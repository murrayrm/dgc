

#include "statefilter.hh"



// dev_reject declarations
int counter = 1;  // how many times have we run?
double buffer[NUM_ELEMENTS]; // buffer to hold elements 
int buff_ptr = 0; // pointer for element in the buffer
double pn, pe; // previous northing and eastings 
double avg, var, dev; // mean, variance, std deviation
double sigma; // Xn - avg


// dev_reject
// This function keeps a running average of the distance between
// gps points that it's fed, and calculates the std. dev. of these points.
// if a received point is outside some specified multiple of this std dev
// it returns 0, otehrwise it returns 1  
int dev_reject(double northing, double easting)
{
  int accept;
  double dn, de, displace; // change in northing/easting 

  if (counter == 1)
    {
      pn = northing;
      pe = easting;
      avg = 0;
      var = 0;
      dev = 0;
      accept = 1;
    }
  else if (counter <= NUM_ELEMENTS)
    {
      // get the deltas
      dn = northing - pn;
      de = easting - pe;
      displace = sqrt(dn * dn + de * de);
      buffer[buff_ptr] = displace;
      
      // get and store the stats
      avg = get_avg();
      var = get_var();
      dev = sqrt(var);
  
      // increment things that need to be incremented
      buff_ptr = (buff_ptr + 1) % NUM_ELEMENTS;
      counter++;
      accept = 1;
      pn = northing;
      pe = easting;
    }
  else
    {
      // get the deltas
      dn = northing - pn;
      de = easting - pe;
      displace = sqrt(dn * dn + de * de);
     
      sigma = abs(displace - avg);

      if (sigma >= dev * SigmaMult)
	{
	  accept = 0;
	  buffer[buff_ptr] = displace;	  
	}else{
	  accept = 1;
	  buffer[buff_ptr] = displace;
	  }
      
      // get and store the stats
      avg = get_avg();
      var = get_var();
      dev = sqrt(var);
      
      buff_ptr = (buff_ptr + 1) % NUM_ELEMENTS;
      counter++;
      pn = northing;
      pe = easting;
    }


  return accept;
}

// run through the buffer and get the average of the elements
double get_avg()
{
  double total=0;
  int limit;

  // sum up only the number of elements that've been put in to buffer
  if (counter <= NUM_ELEMENTS)
    {
      limit = counter;
    }else{
      limit = NUM_ELEMENTS;
    }


  for (int i = 0; i < limit; i++)
    {
      total += buffer[i];
    }

  return total/limit;
}

// gets 1/n * (x - avg)^2
double get_var()
{
  double total = 0;
  int limit;

  // sum up only the number of elements that've been put in to buffer
  if (counter <= NUM_ELEMENTS)
    {
      limit = counter;
    }else{
      limit = NUM_ELEMENTS;
    }


  for (int i = 0; i < limit; i++)
    {
      total += (buffer[i] - avg) * (buffer[i] - avg);
    }

  return total/limit;
}

// reset the filter to initial conditions
void filter_reset()
{
  counter = 1;
  buff_ptr = 0;

  return;
}

/*
// checkjump declarations
double prev_e, prev_n;  // northing and easting from previous run
double prev_vn, prev_ve; // last run's velocities
int first_time = 1;  // flag for running first time through
double last_update_time; // time of last update (for getting delta-t)


// check_jump
// checks if there's been a jump in the GPS data by looking at the heading 
// angle and velocity.  doesn't work very well
int check_jump(gpsDataWrapper gpsdata, double update_time, VState_GetStateMsg
	       vehstate)
{
  double cur_n, cur_e; // new northing and easting
  double vn, ve; // current velocities
  double dn, de; // change in northing, easting, etc
  double dt; // change in time from last reading
  int truth; // return variable
  double speed, displacement;
  double vn_norm, ve_norm; // normalized vectors for vn, ve
  double dn_norm, de_norm; // normalized vectors for change in n, e
  double delta_angle; // difference between heading and displacement angle
  double dot; // dot product betw heading and displacement

  // convert current lat/long to utm
  LatLong getutm(0, 0);
  getutm.set_latlon(gpsdata.data.lat, gpsdata.data.lng);
  getutm.get_UTM(&cur_e, &cur_n); 

  // set current velocities
  vn = vehstate.Vel_N;
  ve = vehstate.Vel_E;

  if (!first_time)
    {
      // first get time difference
      dt = update_time - last_update_time;

      // then get all the other deltas
      dn = cur_n - prev_n;
      de = cur_e - prev_e;

      // get the overall displacement and normalized displacement vector
      displacement = sqrt(dn * dn + de * de);
      dn_norm = dn / displacement;
      de_norm = de / displacement;

      // take dot product between displacement unit vector and heading
      dot = dn_norm * cos(vehstate.Yaw) + de_norm * sin(vehstate.Yaw);

      // calculate the difference in angle
      delta_angle = acos(dot);
     
      // calculate speed and normalized velocity
      speed = sqrt(vn * vn + ve * ve);
 
      // if the angle is too great or the displacement is too great
      // we've made a gps jump
      if ((delta_angle >= ANGLE_CUTOFF) 
	  || (speed * dt * DV_WEIGHT <= displacement))
	{
	  truth = 1;
	}else
	  truth = 0;
      
    }else {
      first_time = 0;
    }

  // set all previous variables to current and return
  prev_n = cur_n;
  prev_e = cur_e;
  prev_vn = vn;
  prev_ve = ve;
  last_update_time = update_time;
  
  return truth;
}*/
