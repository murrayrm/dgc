/*
 * vdrive - vehicle driving program
 *
 * Richard M. Murray
 * 30 Dec 03
 *
 * This program is a simple front end for the vehlib library.  It allows
 * you to control the vehicle through the joystick or remotely and displays
 * the vehicle state.
 *
 * 20 Jan 04, RMM: revised to allow use of new device driver modules.  All
 * old code is ifdef'd out using the name of the package.  As new drivers
 * are integrated, delete the ifdef's and rewrite as needed.
 */

/* Standard header files defined in unix */
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

/*
 * DGC libraries
 *
 * This section includes the definitions for libraries that are part
 * of the DGC code set.  These libraries are *not* part of vehlib and
 * are maintained elsewhere.
 *
 */

#ifdef MTA
/* MTA header files */
#include "MTA/Modules.hh"
#include "MTA/Kernel.hh"
#include <boost/shared_ptr.hpp>

#include "vdmta.hh"
struct VDrive_CmdMotionMsg mtacmd;      /* commanded motion from MTA */

#include "VState.hh"			/* grab message format for VState */
struct VState_GetStateMsg vehstate;	/* current vehicle state */
#endif
int mta_flag = 1;			/* run MTA */

/* Sparrow */
#include "sparrow/display.h"
#include "sparrow/dbglib.h"
#include "sparrow/channel.h"
#include "sparrow/flag.h"
#include "sparrow/keymap.h"

char config_file[FILENAME_MAX] = "vdrive.ini"; 
char dumpfile[FILENAME_MAX] = "vdrive.dat";
DEV_LOOKUP chn_devlut[] = {
  {"virtual", virtual_driver},
  {NULL, NULL}
};
pthread_t dd_thread;			/* display thread */
pthread_t cap_thread;			/* data capture thread */
void *capture_start(void *);		/* capture startup routine */
int display_flag = 1;			/* dynamic display? */

/* 
 * Device driver definitions
 * 
 * For each device driver, we include the header file and define any
 * variables needed in this file to manage the device thread.
 *
 */

#include "vehports.h"			/* default port numbers */

/* Joystick driver */
#ifdef JOYSTICK
#include "joystick.h"

/* Define variables for joystick thread */
pthread_t joystick_thread;		/* Joystick thread */
void *joystick_start(void *);		/* Joystick startup routine */
struct joy_data joy;			/* Joystick data */
#endif
int joy_count = 0;		        /* flag for enabling joystick */
int joystick_flag = 0;		        /* flag for enabling joystick */

/* Steering thread */
#ifdef STEER
#include "steer.h"

int steer_port = STEER_SERIAL_PORT;     /* copy of port number */
pthread_t steer_thread;			/* steering thread */
void *steer_start(void *);		/* steering startup routine */
double str_angle;			/* steering angle offset */
double steer_off = 0;			/* steering angle */
#define STEER_EPS 0.01			/* amount to increment steering */
int steer_inc(long), steer_dec(long);	/* callbacks for changing angle */
#endif
int steer_flag = 0;			/* flag for enabling steering */

/* Brake driver */
#ifdef BRAKE
#include "brake.h"

int brake_port = BRAKE_SERIAL_PORT;     /* copy of port number */
double brk_pos, thr_pos;		/* brake and accelerator position */
int brk_lock = 0;			/* use for memory locking */
double BRAKE_VEL = 0.1;			/* brake velocity during motion */
double BRAKE_ACC = 0.1;			/* brake acceleration during motion */
double MAX_BRAKE=0.25;			/* max brake position for full stop */
int full_stop(long);            	/* callback for emergency full stop */
#endif
int brake_flag = 0;			/* flag for enabling braking */

/* Throttle driver */
#ifdef THROTTLE
#include "throttle.h"

int throttle_port = THROTTLE_PARALLEL_PORT;   /* copy of port number */   //SHOULDNT THIS BE PP_GASBR??????????????????????????????????????????????????
#endif
int throttle_flag = 0;			/* flag for enabling accelerator */

/* Acceleration thread */
#ifdef ACCEL
#include "cruise.h"

pthread_t accel_thread;			/* braking thread */
void *accel_start(void *);		/* braking startup routine */
double ACCEL_EPS = 0.01;		/* amount to increment braking */
int accel_inc(long), accel_dec(long);	/* callbacks for changing accel */
double cruise_gain = 1;			/* gain for cruise control */
double speed_ref = 0;			/* velocity */
double accel_off = 0;			/* acceleration offset */
#endif
int acc_count = 0;			/* keep track of accel writes */
int cruise_flag = 0;			/* cruise control flag */


/*
 * Local definitions
 *
 * Now go thorugh and define all of the functions and variables that
 * are specific to this file.
 *
 */

/* Functions defined in this file */
int user_quit(long);
int user_load_parmtbl(long), user_save_parmtbl(long);

/* Variables used in this file */
int remote_flag = 0;			/* grab data remotely */

/* Vehicle mode */
int vdrive_mode(DD_ACTION, int);
enum veh_mode {
  modeOFF = 'o', modeMANUAL = 'm', modeREMOTE = 'r', modeAUTONOMOUS = 'a'
};
enum veh_mode mode_steer = modeOFF;
enum veh_mode mode_drive = modeOFF;
int mode_manual(long), mode_remote(long), mode_off(long);

/* Usage message */
char *usage = "\
Usage: %s [-v] [options]\n\
  -b	disable braking subsystem\n\
  -h	print this message\n\
  -m	disable MTA subsystem\n\
  -s	disable steering subsystem\n\
  -t	disable throttle subsystem\n\
  -v	turn on verbose error messages\n\
";


/* Display tables; include at end so they have access to local variables */
#include "vddisp.h"

/*
 * vdrive main
 * 
 * This is the main routine for vdrive.  It parses command line arguments,
 * starts up the threads used by vdrive, and starts the MTA loop.
 *
 */

int main(int argc, char **argv)
{
    int c, errflg = 0, error = 0;

    /* Turn on error processing during startup */
    dbg_all = dbg_outf = 1;
    dbg_flag = 0;			/* set with -v if you want output */

    /* 
     * Parse command line arguments
     *
     * Each device should have a flag that can be used to disable that 
     * device/thread from the command line.  Set flag to -1 to indicate
     * that device should not be initialized.
     */

    while ((c = getopt(argc, argv, "vrthbms?")) != EOF)
      switch (c) {
      case 'v':		dbg_flag = 1;			break;
      case 'm':		mta_flag = 0;			break;
      case 'r':	        remote_flag = 1;		break;
      case 's':		steer_flag = -1;		break;
      case 't':		throttle_flag = -1;		break;
      case 'b':		brake_flag = -1;		break;
      case 'h':		errflg++;			break;
      default:		errflg++;			break;
	break;
      }

    /* Print an error message if anything went wrong */
    if (errflg || argc < optind) {
      fprintf(stderr, usage, argv[0]);
      exit(1);
    }

    /*
     * Initialize devices
     *
     * This section of the code initializes each device that vdrive
     * needs to talk to.  If device_flag is set to -1, device has
     * been disabled on command line and should *not* be initialized.
     * Otherwise, attempt to open the device and set flag = 0 or 1 to 
     * indicate whether it was successful.
     *
     */

#   ifdef MTA
    /* If the MTA flag is turned on, automatically turn on the remote flag */
    if (mta_flag) remote_flag = 1;
#   endif

#   ifdef SPARROW
    /* Load initialization file */
    if (dd_tbl_load(config_file, parmtbl) < 0) {
      dbg_error("couldn't load vdrive conifiguration file: %s\n", config_file);
      ++error;
    }
#   endif

#   ifdef JOYSTICK
    /* Initalize anything that might cause an error */
    dbg_info("joystick");
    if (joy_open(JOYSTICK_PORT, &joy) < 0) ++error; else ++joystick_flag;
#   endif

#   ifdef STEERING
    dbg_info("steering: %d", steer_flag);
    if (steer_flag == 0) 
      if (!init_steer()) ++error; else ++steer_flag;
#   endif

#   ifdef THROTTLE
    dbg_info("throttle: %d", throttle_flag);
    if (throttle_flag == 0) {
      if (throttle_open()<0) {
	dbg_error("accelerator initialization failed\n");
	++error; 
      } else 
	throttle_calibrate()
	throttle_flag = 1;;
    }
#   endif

 
#   ifdef BRAKE
    dbg_info("brake: %d", brake_flag);
    if (brake_flag == 0) {
      if (brake_open() < 0) {  //this must also start the brake thread so the brake_write below will work
	++error; 
	dbg_error("brake initialization failed\n");
      }
      else {
	// Move brake to initial position 
	dbg_info("setting brake position to zero...");
	brake_zero();
	brake_flag = 1;
      }
    }
#   endif

#   ifdef SPARROW
    /* Initialize sparrow channel structures */
    if (chn_config("vdrive.dev") < 0) {
      dbg_error("can't open vdrive.dev");
      ++error;
    } else if (chn_init() < 0) {
      dbg_error("error initializing channel interface");
      ++error;
    }
#   endif

    /* Pause if there are any errors or verbose */
    dbg_info("[error = %d], steer = %d, brake = %d, throttle = %d\n", 
	     error, steer_flag, brake_flag, throttle_flag);
    if (dbg_flag || error) {
      if (error) fprintf(stdout, "Errors on startup; continue (y/n)? ");
      if (dbg_flag) fprintf(stderr, "Continue (y/n)? ");
      if (getchar() != 'y') exit(1);
    }

    /* Turn off printf while display is running */
    dbg_all = 0;

    /* Initialize the display package */
    if (dd_open() < 0) exit(1);		/* initialize the database, screen */
    dd_bindkey('Q', user_quit);
    dd_bindkey('M', mode_manual);
    dd_bindkey('R', mode_remote);
    dd_bindkey('O', mode_off);
#   ifdef STEER
    dd_bindkey('j', steer_inc);
    dd_bindkey('k', steer_dec);
#   endif
#   ifdef ACCEL
    dd_bindkey(K_HOME, accel_inc);
    dd_bindkey('m', accel_dec);
#   endif
#   ifdef BRAKE
    dd_bindkey(' ', brake_pause);  //changed from full_stop to brake_pause  where are the arguments for this!!!!!!!!!!!!!!!!!
#   endif
    dd_usetbl(vddisp);		/* set display description table */

    /* Start up threads for execution */
    fprintf(stderr, "starting threads\n");

#   ifdef JOYSTICK
    pthread_create(&joystick_thread, NULL, joystick_start, (void *) NULL);
#   endif

#   ifdef STEER
    pthread_create(&steer_thread, NULL, steer_start, (void *) NULL);
#   endif

#   ifdef ACCEL
    if (pthread_create(&accel_thread, NULL, accel_start, (void *) NULL) < 0) {
      perror("accel");
      exit(1);
    }
#   endif

#   ifdef SPARROW
    pthread_create(&cap_thread, NULL, capture_start, NULL);
#   endif

    /* Run the display manager as a thread */
    fprintf(stderr, "starting dd_loop\n");
    pthread_create(&dd_thread, NULL, dd_loop_thread, (void *) NULL);
#   ifdef MTA
    if (!mta_flag)
#   endif
      /* If we aren't running MTA, wait until dd_loop ends */
      pthread_join(dd_thread, (void **) NULL);
#   ifdef MTA
    else {
      /* Otherwise, pass control to MTA */
      Register(shared_ptr<DGC_MODULE>( new VDrive ));
      StartKernel();
    }
#   endif
    dd_close();				/* clear the screen and free memory */

    /* User cleanup goes here */
}

/* 
 * Process threads 
 *
 * Each function in this section starts up a thread to process data
 * from a device or module.
 *
 */

#ifdef JOYSTICK
/* Joystick thread */
void *joystick_start(void *arg)
{
  /* Thread just reads the joystick port forever */
  while (1) {
    if (joystick_flag) joy_read(&joy);
    ++joy_count;
  }
  return NULL;
}
#endif

#ifdef ACCEL
/* Throttle and brake thread */
void *accel_start(void *arg)
{
  while (1) {
    double cmd_acc;			/* commanded accleration */
    double cmd_vel;			/* commanded velocity */

    switch (mode_drive) {
    case modeMANUAL:
      /* Use the joystick to command motion */
      cmd_acc = (joy.data[1] - joy.offset[1])/joy.scale[1];
      if (cmd_acc > 0) cmd_acc *= cruise_gain;
      break;

    case modeREMOTE:
      /* Use reference acceleration to command motion */
      cmd_acc = accel_off;
      break;

    case modeAUTONOMOUS:
      /* Not yet implemented; need data from MTA */
      cmd_acc = 0;			/* set to zero just in case */  //this needs to be implemented with commands from arbiter?
      break;

    case modeOFF:
      thr_pos = 0;
      brk_pos = 0;
      break;
    }

    /* See if we should run the cruise control loop */
    if (cruise_flag) {
      /* Call the cruise control function (PD controller) and add
	 reference acceleration (offset) */
      cmd_acc = cruise_gain * cruise(speed_ref, kf_vel, kf_acc) +
	accel_off;
    }

    /* Check limits and allocate out the command */
    if (cmd_acc > 1) cmd_acc = 1;
    if (cmd_acc < -1) cmd_acc = -1;
    thr_pos = (cmd_acc < 0) ? 0 : cmd_acc;
    brk_pos = (cmd_acc < 0) ? -cmd_acc : 0;

    /* Send command to the throttle */
    if (throttle_flag == 1) {  //dont we need to check that the throttle lock is not on???????????????????????????????????
      ++acc_count;
      // Ike says that setting the position to zero then desired works
      // Doesn't seem to do all that much better
      //set_accel_abs_pos(0.0);  //removing assuming that increasing sensitivity settings on the controller itself will fix this
      throttle_write(thr_pos);
    }

    /* Send command to the brake */
    if (brake_flag == 1) {
      ++acc_count;
      brk_lock = 1;
      brake_write(brk_pos, BRAKE_VEL, BRAKE_ACC);
      brk_lock = 0;
    }

    /* Sleep for 100 msec to allow everyone to keep up */
    usleep(100000);
  }
  return NULL;
}
#endif

#ifdef STEER
void *steer_start(void *arg)
{
  /* Thread just reads the joystick port forever */
  while (1) {
    switch (mode_steer) {
    case modeMANUAL:
      /* Use the joystick to command the angle */
      str_angle = (joy.data[0] - joy.offset[0])/joy.scale[0];
      if (str_angle < -1) str_angle = -1;
      if (str_angle >  1) str_angle =  1;
      break;

    case modeREMOTE:
      /* Use the reference value to command the position */
      str_angle = steer_off;
      if (str_angle < -1) str_angle = -1;
      if (str_angle >  1) str_angle =  1;
      break;

    case modeAUTONOMOUS:
      /* Get data from MTA */
      str_angle = mtacmd.steer_cmd;
      break;

    case modeOFF:
      str_angle = 0;
      break;
    }

    if (steer_flag == 1) {
      /* Write the data to the steering controller */
      if (steer_heading(str_angle) < 0) {
	/* Print out an error message */
      }

      /* Update the steering state (needed?) */
      steer_state_update(0, 0);

      usleep(10000);
    }
  }
  return NULL;
}
#endif

#ifdef SPARROW
/* Data capture */
void *capture_start(void *arg)
{
  struct timeval tv;
  static int start = 0;

  /* Initialize the starting time */
  gettimeofday(&tv, NULL); start = tv.tv_sec;

  while (1) {
    /* Save the time the data was taken */
    gettimeofday(&tv, NULL);
    chn_data(0) = (double) (tv.tv_sec - start) + ((double) tv.tv_usec) / 1e6;

    /* Fill up the rets of the data array with the data we want to capture */
    /* Note that # of channels must correspond to config.dev */
#   ifdef MTA
    chn_data(1) = vehstate.kf_lat;
    chn_data(2) = vehstate.kf_lng;
    chn_data(3) = vehstate.Pitch;
    chn_data(4) = vehstate.Roll;
    chn_data(5) = vehstate.Yaw;
    chn_data(6) = vehstate.Speed;
    chn_data(7) = vehstate.PitchRate;
    chn_data(8) = vehstate.YawRate;
#   endif MTA
#   ifdef ACCEL
    chn_data(9) = brk_pos;
    chn_data(10) = thr_pos;
#   endif
#   ifdef STEER
    chn_data(11) = str_angle;
#   endif
    chn_data(12) = 0;
    chn_data(13) = 0;
    chn_data(14) = 0;
    chn_data(15) = 0;

    chn_write();			/* write data to memory */
    usleep(10000);			/* take data ~100 Hz */
  }
}
#endif

/*
 * Display callbacks and managers
 *
 * These functions are used by the display routines to control the
 * operation of vdrive.  Each function takes a long integer as an
 * argument (recast as needed) and returns and integer.
 *
 *     mode_*		    set the operating mode
 *     user_cap_toggle	    turn capture off/on
 *     user_load_parmtbl    load parameters from file
 *     user_save_parmtbl    save parameters from file
 *     vdrive_mode	    display manager for alphnumeric mode
 *     user_quit	    terminate vdrive
 */

/* Mode selection commands */
int mode_manual(long arg) { 
  mode_steer = modeMANUAL; 		/* manual steering */
  mode_drive = modeOFF; 		/* brake and throttle via pedals */
  return 0; 
}
int mode_remote(long arg) { mode_steer = mode_drive = modeREMOTE; return 0; }
int mode_off(long arg) { mode_steer = mode_drive = modeOFF; return 0; }

#ifdef ACCEL
/* Increment/decrement throttle and brake */
int accel_inc(long arg) { accel_off += ACCEL_EPS; return 0; }
int accel_dec(long arg) { accel_off -= ACCEL_EPS; return 0; }
#endif

#ifdef STEER
/* Increment/decrement steering angle */
int steer_inc(long arg) { steer_off += STEER_EPS; return 0; }
int steer_dec(long arg) { steer_off -= STEER_EPS; return 0; }
#endif

#ifdef BRAKE
/* Execute a full stop */
//int full_stop(long arg) {
int brake_pause(double pos)  //not sure how to deal with this!!!!!!!!!!!!!!!!!!!!!!!!
  int count = 0;

  /* First, push down the break: will get picked up in accel thread */
  if (brake_flag) {
    /* Check if brake loop is active */
    while (brk_lock and count++ < 10) usleep(10000);

    /* Turn off the brake */
    brake_flag = 0;			/* turn off brake loop */
    //   brk_pos = MAX_BRAKE;		/* just in case loop restarts */
    brk_pos = pos;                    //just for testing purposes
    brake_write(brk_pos, BRAKE_VEL, BRAKE_ACC);
  }

  /* Now turn off the throttle */
if (throttle_flag) throttle_pause(); //changed from thr_pos = 0 to throttle_pause() **************************

  /* Beep so that we know this all happened */
  DD_BEEP();

  return 0;
} 
#endif

/* Toggle capture mode */
int user_cap_toggle(long arg)
{
  return chn_capture_flag ? chn_capture_off() : chn_capture_on();
}

#ifdef SPARROW
/* Save the actuation parameters */
int user_save_parmtbl(long arg)
{
  if (dd_tbl_save(config_file, parmtbl) < 0) {
    dd_text_prompt("could not print data");
    dd_beep((long) 0);
  }
  return 0;
}

/* Save the actuation parameters */
int user_load_parmtbl(long arg)
{
  if (dd_tbl_load(config_file, parmtbl) < 0) {
    dd_text_prompt("could not print data");
    dd_beep((long) 0);
  }
  return 0;
}
#endif

/* Special display manager function for handling the vehicle mode */
int vdrive_mode(DD_ACTION action, int id)
{
    DD_IDENT *dd = ddtbl + id;
    char *value = (char *)dd->value;
    char itmp;
    
    switch (action) {
    case Input:
      if (DD_SCANF("Mode: ", "%c", &itmp) == 1)
	*value = (short) itmp;
      break;
	
    default:			/* default = use dd_short */
      return dd_short(action, id);
    }
    return 0;
}

/* Quit the dfan program */
int user_quit(long arg)
{
  int count = 0;

# ifdef MTA
  /* Tell MTA to shut down */
  ForceKernelShutdown();

  /* Wait for 1 second to make sure it is shut down */
  while (ShuttingDown()) {
    if (count++ > 1000) break;
    usleep(1000);
  }
  if (!ShuttingDown()) fprintf(stderr, "\nMTA Kernel shut down\n");
# endif

  /* Tell dd_loop to abort now */
  return DD_EXIT_LOOP;
}

