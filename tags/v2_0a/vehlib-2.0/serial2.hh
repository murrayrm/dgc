#ifndef SERIAL2_HH
#define SERIAL2_HH

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <pthread.h>

#include "MTA/Misc/Time/Timeval.hh"
#include "MTA/Misc/Pipes/BufferedPipe.hh"
#include "MTA/Misc/Thread/ThreadsForFunctions.hh"

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>


#define SerialPath 	"/dev/ttyS"
#define MaxSerialPorts	10		// suuport ttyS0 to ttyS9
#define MaxRX           8               // max # of bytes to read per "read"
#define MaxTX           8               // max # of bytes to send per "write"
#define SerialBlock	1		
#define SerialNonBlock	0		
#define DefaultBaud	B9600

#define SR_PARITY_DISABLE	1<<0	// disable parity
#define SR_PARITY_EVEN		1<<1	// set even parity
#define SR_PARITY_ODD		1<<2	// set odd parity

#define SR_SOFT_FLOW_DISABLE	1<<3	// disable both software and hardware flow control
#define SR_HARD_FLOW_DISABLE	1<<4	// disable both software and hardware flow control
#define SR_SOFT_FLOW_INPUT	1<<5	// enable software input flow control
#define SR_SOFT_FLOW_OUTPUT	1<<6	// enable software output flow control
#define SR_HARD_FLOW_ENABLE	1<<7	// enable hardware flow control
	
#define SR_ONE_STOP_BIT		1<<8	// enable one stop bit
#define SR_TWO_STOP_BIT		1<<9	// enable two stop but


typedef void (*CallbackFunction) ();

namespace SPStates {
  enum {
    Open,      // serial port is open and running
    NotOpen,   // serial port is not open (closed)
    Error      // serial port had an error (and is not open now)

  };
};

struct SerialPortHandle {

  boost::recursive_mutex mutex;
  BufferedPipe   TX;   // transmit queue
  BufferedPipe   RX;   // receive  queue


  int use;     // Tell the server to open the serial port if it is not.
  int baud;    // baud rate.
  int options; // Serial port options.
  int state;   // State of the port.

  int fd;      // File Descriptor of Serial Port File

  int ec;      // error count 
  
  CallbackFunction callback; // function to call when new data is available

  pid_t wakePID; // Wake up this thread using SIGUSR2.

  // Debugging Info
  int bRead, bWrite;  // # of bytes written and read from serial port.
  int aRead, aWrite;  // # of read and write _attempts_ made on serial port.


  // constructor
  SerialPortHandle() : use(false), state(SPStates::NotOpen), callback(NULL), options(0),
                       fd(-1), bRead(0), bWrite(0), aRead(0), aWrite(0),
                       wakePID(-1), ec(0), baud(-1) {}

};


/*  serial_open_advanced opens a serial port and allow more setting for the serial port.
 *  set options to set parity, flow control, and stop bits. Note: even if you don't set
 *  stop bit, there is still one stop bit, I guess it is default to serial port.
 *  com: serial port you want to open
 *  BaudRate: baud rate you want to set to
 *  options: set parity, flow control, and stop bits.
 *  
 *  available flags for options ( OR these flags to select multiple flags):
 *
 *	SR_PARITY_DISABLE	// disable parity
 *	SR_PARITY_EVEN		// set parity to even	
 *	SR_PARITY_ODD		// set parity to odd
 *	
 *	SR_FLOW_DISABLE		// disable flow control
 *	SR_FLOW_INPUT		// enable input flow control
 *	SR_FLOW_OUTPUT		// enable output flow control
 *
 *	SR_ONE_STOP_BIT		// set one stop bit
 *	SR_TWO_STOP_BIT		// set two stop bit
 */


int serial_open_advanced(int com, int BaudRate, int options, CallbackFunction f=NULL);

/* serial_open: opens a serial port (ttyS0 to ttyS9) for reading and writing.
 *  RETURN: -1 if an error occurs; 0 otherwise.
 *  com: serial port number
 *  BaudRate: can be
 *	      B0, B50, B75, B110, B134, B150, B200, B300, B600, B1200, B1800, B2400, B4800, 
 *	      B9600, B19200, B38400, B57600, B115200, B230400, B460800, B500000, B576000, 
 *	      B921600, B1000000, B1152000, B1500000, B2000000, B2500000, B3000000, B3500000, 
 *	      B4000000
 */
int serial_open(int com, int BaudRate, CallbackFunction f=NULL);

/* serial_read:	read from a com port, (a buffer indeed). If the com port is not open, call serial_open to open the serial port.
 *	 	The length of the returned data is smaller or equal to requested length
 * RETURN:	length of data read from serial, -1 if an error occurs
 */	
int serial_read(int com, char *buffer, int length);
/* serial_read_until:	read from a com port. If the com port is not open, 
 *  		call serial_open to open the serial port. The user has to 
 *		specify a character he or she is looking for, and this
 * 		function won't return until either the character has
 * 		been found or timeout or buffer length is longer than
 *		limitlength. If the character is found, data
 *		in the buffer up to the character will be 
 *		returned to the user. 
 * com   	: number of serial port
 * buffer	: buffer that will be returned with data
 * delimit	: character the user is looking for
 * timeout	: time out in microseconds
 * limitlength	: limit length of the buffer. If serial buffer is longer than this, 
 *		  then return data of limitlength from the buffer
 * RETURN	: length of data read from serial, -1 if an error occurs
 */
int serial_read_until(int com, char *buffer, char delimit, int maxlength, Timeval timeout=Timeval(0,500000));


/* serial_read_blocking: 
 *      Uses calls to serial_read for various buffer lengths to read until all
 *      data comes in or the timeout is reached.  A timeout of 0 means that it
 *      is never reached.  
 * NOTE:        Doesn't actually use serial_block or serial_nonblock
 * RETURN:      length of data read from serial, -1 if an error occurs
 */
int serial_read_blocking(int com, char *orig_buffer, int length, Timeval timeout=Timeval(0, 100000));

/* serial_write: write to a com port. If the com port is not open, 
 *  		 it will return an error.
 * RETURN: length of data written. -1 if an error occurs.
 */
int serial_write(int com, char *buffer, int length);

/* serial_close: closes serial port number com
 * RETURN: -1 if an error occurs; 0 otherwise.
 */
int serial_close(int com);

/* serial_clean_buffer: delete the contents of the buffer for specified com number
 * RETURN: -1 if an error occurs, which can happen only when you pass a com
 *	  number that doesn't exit. 0 if succesfull.
 */
int serial_clean_buffer(int com);


/*  serial_data_length returns the length of the available data in the buffer
 */ 
int serial_data_length(int com);

void print_serial_status(int com = -1);


#endif  



