/*
 * vstate.cc - vehicle state estimtor
 *
 * RMM 31 Dec 03
 *
 * This program is the state estimator for the vehicle.  It provides an
 * on-screen display of the vehicle state and also sets up an MTA mailbox
 * that can be used to query the vehicle state.
 *
 * H 03 Jan 04 
 * Added magnetometer reading 
 * Added statefilter code
 *
 * Alex Fax 15 Jan 04
 * Kalman Filter added, statefilter no longer is run
 *
 * H 16 Jan 04
 * added data logging and display to KF
 * gps valid display
 *
 * H 30 Jan 04
 * Comments added, code cleaned up, all old statefilter stuff now gone
 * 
 * H 08 Feb 04
 * GPS disable implemented
 * GPS nav mode display implemented
 *
 * H 13 Feb 04
 * Added GPS jump finding routine for disconts in gps solution
 * created in new statefilter.cc
 */

/*
  Kalman filtered state estimator
  Structure:  multiple threads are started up, one each for IMU, magnetometer,
  gps, and display. 
  Each thread loops continuously getting data.  The Kalman filter code runs 
  at 400 Hz, triggered by the IMU thread. 
  The KF itself updates at 1 Hz.  Magnetometer is used to initialize heading 
  but not during actual driving.  The vehicle
  MUST be stationary when the program starts, since the IMU is used to derive
  initial pitch and yaw assuming the car is stationary.
  KF takes a couple of seconds to converge to the initial settings, then is 
  updated at 1 Hz from GPS.  
  External dependencies:  
    kfilter.a
    kfilter/Global.h, Kalmano.h, kfilter.h
        
    LatLong.h (for UTM conversion)
*/

/* Logging details
   NOTE: all time stamps are local, i.e. referenced from start of this 
   program, except the Timeval timestamp
   in the vehstate struct, which I think is gotten through MTA
   statelog.dat - logs vehstate details, UTM, vel, etc
   kflog.dat - logs kalman filter details such as biases, residuals, etc
   kplog.dat - logs covariance matrix:
               first logs time, then fills rest of row with zeros, then next 
	       30 rows are Kp
   imulog.dat - imu raw data and timestamp (local)
   gpslog.dat - gps raw data and timestamp (local)
*/

// KNOWN BUGS AND ISSUES:
// 


#include <stdlib.h>
#include <iostream.h>
#include <unistd.h>
#include <pthread.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"
#include "sparrow/channel.h"
#include <string.h>

#include "vehports.h"
#include <fstream.h>

#include "MTA/Modules.hh"
#include "vsmta.hh"
#include "MTA/Kernel.hh"
#include <boost/shared_ptr.hpp>

#include "LatLong.h"

#include "kfilter/kfilter.h"
#include "kfilter/Kalmano.h"
#include "kfilter/Global.h"

#include "statefilter.hh"

/* Functions defined in this file */
static int user_quit(long);
int veh_gps_init(long), veh_gps_process(long);


// Externally defined Kalman Filter variables
extern int NavBufferIndex;
extern TSaveNavData NavBuffer[XRATIO2];

extern double gps_lat, gps_lon, gps_alt;
extern double gps_vn, gps_ve, gps_vu;
extern double gps_time;
extern double imu_time;
extern double mag_hdg;

extern int New_GPS_data_flag;   // flag set when new GPS data comes in
extern int New_Mag_data_flag;

extern double imu_dvx,imu_dvy,imu_dvz; // Delta-v's m/s (should be)
extern double imu_dtx,imu_dty,imu_dtz; // Delta-Thetas,should be rads

extern double kp[NUM_ST+1][NUM_ST+1];  // covariance matrix
extern double kx[NUM_ST+1];            // kalman gains

extern double tot_kabx, tot_kaby, tot_kabz;
extern double tot_ksfx, tot_ksfy, tot_ksfz;
extern double tot_kgbx, tot_kgby, tot_kgbz;
extern double tot_kgsfx;

extern double ExtGPSPosMeas_x, ExtGPSPosMeas_y, ExtGPSPosMeas_z;
extern double ExtGPSVelMeas_x, ExtGPSVelMeas_y, ExtGPSVelMeas_z;

extern double GPS_xy_kr, GPS_h_kr, GPS_v_kr; // errors

/* Variables used in this file */
int mta_flag = 1;			/* run MTA */
struct VState_GetStateMsg vehstate;	/* current vehicle state */


// ********** NOTE: Channel based data-logging no longer used, 
// this is only here because sparrow seems to expect it *******
// channel logging declarations
// guide to log can be found in sreadme
char config_file[FILENAME_MAX] =	/* configuration file */
  "kfconfig.ini"; 
char dumpfile[FILENAME_MAX] = "kfstate.dat";
DEV_LOOKUP chn_devlut[] = {
  {"virtual", virtual_driver},
  {NULL, NULL}
};
struct timeval tv;
struct timeval start;


// note, enable = 0 means device is enabled but not active, enabled = 1 
// means device has been initialized update flags are for statefilter, 
// 0= no new data, 1 = new data received, -1 = currently updating

/* Define variables for GPS thread */
#define GPS
#include "gps.h"
pthread_t gps_thread;			/* GPS thread */
void *gps_start(void *);		/* GPS startup routine */
int veh_gps_com = GPS_SERIAL_PORT;	/* serial port for GPS */
int veh_gps_rate = 10;			/* GPS rate (Hz) */
gpsDataWrapper gpsdata;			/* GPS data */
static char *gps_buffer;		/* buffer for holding GPS data */
int gps_count = 0;			/* keep track of GPS reads */
int new_gps = 0;                        /* gps update flag */ 
double gheading;                        /* gps heading, used to compare gps 
					   heading with magnetometer */
int gps_valid;                          /* valid gps pvt flag */
double gps_local_time;                  // gps timestamp 
int gps_enabled = 0;			/* flag for enabling GPS */
int gps_active = 1;                     // flag for manually deactivating 
                                        // GPS during run
char gps_nav_msg[MAX_MSG_LEN];          // character message explaining 
                                        // nav mode
int gps_nav_mode;                       // current navigational mode 
int ztoggle = 1;                        // take altitude corrections or not
double GPS_ALT_DEFAULT = 0;          // default altitude to feed to kf
int gps_ext_nav;                     // extended nav mode
int gps_jumped=0;                    // flag for gps discontinuities
int gps_invalid_count = 0;           // counter for how long we've been invalid


/* Define variable for IMU thread */
#define IMU
#include "imu.h"
pthread_t imu_thread;			/* IMU thread */
void *imu_start(void *);		/* IMU startup routine */
IMU_DATA imudata;			/* IMU data */
int imu_count = 0;			/* keep track of IMU reads */
int imu_enabled = 0;			/* flag for enabling IMU */
int imu10hz = 0;                        /* int to count down to 10 hz for 
					   IMU */
int imu1hz = 0;                         /* counter for 1hz tasks */

/* Magnetometer definitions */
#define MAGNETOMETER
#include "magnetometer.h"
pthread_t mag_thread;                   /* magnetometer thread */
void *mag_start(void *);                /* magnetometer startup routine */
mag_data magreading;                    /* read magnetometer data */
int veh_mag_com = MAG_SERIAL_PORT;      /* magnetometer serial port */
int mag_count = 0;                      /* keep track of magnetometer reads */
int new_mag = 0;                        /* mag update flag */
int mag_enabled = 0;                    /* magnetometer enable flag */

/* OBD2 definitions */
#include "OBDII.h"
int obdflag = -1;                       // flag for activating obd2
double obd_vel;                        // obd2 velocity

//Data logging declarations
fstream kffile;                        // fstream for logging kf vars
fstream imufile;                       // fstream for logging imu raw output
fstream gpsfile;                       // fstream for logging raw gps data
fstream statefile;                     // fstream for logging state data
fstream kpfile;                        // fstream for logging covariance data
char *statename = "statelog.dat";      // data log file for state
char *imuname = "imulog.dat";          // data log file for imu
char *gpsname = "gpslog.dat";          // data log for gps
char *kfname = "kflog.dat";            // data log for kf
char *kpname = "kplog.dat";            // covariance matrix log
int logflag = 0;                       // enable/disable data logging
void logheaders();                     // write logfile headers to file

/* Usage message */
char *usage = "\
Usage: %s [-v] [options]\n\
  -g    disable GPS subsystem\n\
  -h    print this message\n\
  -i    disable IMU subsystem\n\
  -m    disable MTA subsystem\n\
  -v    turn on verbose error messages\n\
  -a    turns off magnetometer\n\
  -l    turns on logging from start\n\
  -o    enables obd2 corrections of velocity\n\
";

// include display header
// NOTE: these two lines need to be at the bottom of all the 
// declarations in order to ensure that all the desired variables 
// are readable by the display
#include "vsdisp.h"			/* display */
pthread_t dd_thread;			/* display thread */

int main(int argc, char **argv)
{
    int c, errflg = 0, error = 0;
    

    /* Turn on error processing during startup */
    dbg_flag = dbg_all = dbg_outf = 1;

    /* Parse command line arguments */
    while ((c = getopt(argc, argv, "vimghalo?")) != EOF)
      switch (c) {
      case 'v':		dbg_flag = 1;			break;
      case 'i':		imu_enabled = -1;		break;
      case 'm':		mta_flag = -1;			break;
      case 'g':		gps_enabled = -1;		break;
      case 'h':         errflg++;                       break;
      case 'a':         mag_enabled = -1;               break;
      case 'l':         logflag = 1;                    break;
      case 'o':         obdflag = 1;                    break;
      default:		errflg++;			break;
	break;
      }

    /* Print an error message if anything went wrong */
    if (errflg || argc < optind) {
      fprintf(stderr, usage, argv[0]);
      exit(1);
    }

    /*
     * Device Initialization
     *
     * Initialize all of the devices that might causes errors that
     * would stop us from running.
     *
     */

     // initialize headers for data log columns
    if (logflag)
	logheaders();
   

    /* GPS initialization; gps_enabled = -1 if disabled from command line */
    if (gps_enabled != -1) {
      if (gps_open(veh_gps_com) <0) {
	gps_set_nav_rate(veh_gps_rate);
	dbg_error("GPS: initialization failure\n");
	++error;
      } else
	{
	  gps_enabled = 1;
	  strcpy(gps_nav_msg, "Initialized");
	  overview[NAVMODE].initialized = 0;
	}
    } else
      {
	gps_enabled = 0;
	strcpy(gps_nav_msg, "Disabled");
	overview[NAVMODE].initialized = 0; // force display update
      }

    /* Mag initialization; mag_enabled = -1 if disabled from command line */
    if (mag_enabled != -1) {
      if (mm_open(veh_mag_com) != 0){
	dbg_error("Magnetometer error");
	++error;

	// No Mag present, so manually get heading 
	cout<<"Mag not enabled, enter current heading in degrees:";
	cin>>magreading.heading;
    
      }else{
	mag_enabled = 1;
	}
    }else{ 
	mag_enabled = 0;
	//Mag disabled, so manually initialize heading
	cout<<"Mag not enabled, enter current heading in degrees:";
	cin>>magreading.heading;
    }


    /* IMU initialization; imu_enabled = -1 if disabled from command line */
    if (imu_enabled != -1) {
       if (IMU_open() < 0) {
	 dbg_error("IMU error");
	++error; 
      } else {
	imu_enabled = 1;
	DGCNavInit();
      }
    } else
      imu_enabled = 0;

    // OBD2 Initialization
    if (obdflag != -1){
      if (OBD_Init(OBD_SERIAL_PORT) < 0) {
	dbg_error("OBDII error");
	++error;
      }else{
	obdflag = 1;
      }
    }else
	obdflag = 0;

    /* Initialize sparrow channel structures */
    if (chn_config("kfconfig.dev") < 0) {
      dbg_error("can't open kfconfig.dev");
      ++error;
    } else if (chn_init() < 0) {
      dbg_error("error initializing channel interface");
      ++error;
    }

    // initialize starting time 
    gettimeofday(&start, NULL);


    // set flags for sensors active
    vehstate.gps_enabled = gps_enabled;
    vehstate.imu_enabled = imu_enabled;
    vehstate.mag_enabled = mag_enabled;
      
    /* Pause if there are any errors */
    dbg_info("gps_enabled = %d, imu_enabled = %d, mag_enabled = %d\n", 
	     gps_enabled, imu_enabled, mag_enabled);
    if (error) {
      fprintf(stdout, "Errors on startup; continue (y/n)? ");
      if (getchar() != 'y') exit(1);
    }

    /* Turn off printf while display is running */
    dbg_all = 0;

    /* Initialize the display package */
    if (dd_open() < 0) exit(1);		/* initialize the database, screen */
    dd_bindkey('Q', user_quit);         // Q quits
    dd_bindkey('L', user_log_inc);      // L causes the log to increment
    dd_bindkey('G', user_gps_toggle);   // G causes the gps to switch on 
                                        // and off
    dd_usetbl(overview);		/* set display description table */

    /* Start up threads for execution */

    // imu thread
    if (pthread_create(&imu_thread, NULL, imu_start, (void *) NULL) < 0) {
      dbg_error("Can't start IMU thread \n");
      exit(1);
    }


    // gps thread
    if (pthread_create(&gps_thread, NULL, gps_start, (void *) NULL) < 0) {
      dbg_error("Can't start GPS thread \n");
      exit(1);
    }


    // magnetometer thread
    if (pthread_create(&mag_thread, NULL, mag_start, (void *) NULL) < 0) {
      dbg_error("Can't start mag thread \n");
      exit(1);
    }


    /* Run the display manager as a thread */
    pthread_create(&dd_thread, NULL, dd_loop_thread, (void *) NULL);
    if (!mta_flag)
      /* If we aren't running MTA, wait until dd_loop ends */
      pthread_join(dd_thread, (void **) NULL);
    else {
      /* Otherwise, pass control to MTA */
      Register(shared_ptr<DGC_MODULE>( new VState ));
      StartKernel();
    }
    dd_close();	     /* clear the screen and free memory */

    /* User cleanup goes here */
}

/*
 * Process threads 
 *
 * These routines define the process threads used in vstate.  Each of
 * them should be started up in the main code.  Once called, they
 * should loop forever.
 *
 * TBD: should integrate initialization and move these routines to their
 * respective files, so that startup becomes particularly simple.
 *
 */



#ifdef IMU
void *imu_start(void *arg)
{
  LatLong latlong(0 , 0); // latlong conversion
  int row, col;

  /* Thread reads the imu, calls a KF update, and then stores results */
  while (1) 
    if ((imu_enabled > 0)) {
      IMU_read(&imudata);
      ++imu_count;

      //log data
      /* Save the time the data was taken */
      gettimeofday(&tv, NULL);
      
      // pass data to Kalman filter   
      imu_time = (double) (tv.tv_sec - start.tv_sec) + 
	((double) (tv.tv_usec - start.tv_usec)) / 1e6;
      imu_dvx = imudata.dvx;
      imu_dvy = imudata.dvy;
      imu_dvz = imudata.dvz;
      imu_dtx = imudata.dtx;
      imu_dty = imudata.dty;
      imu_dtz = imudata.dtz;
       
      // stores KF data into vehstate
      vehstate.Timestamp = TVNow();
      vehstate.kf_lat = NavBuffer[NavBufferIndex].lat / DEG2RAD;
      vehstate.kf_lng = NavBuffer[NavBufferIndex].lon / DEG2RAD;
      vehstate.Altitude = NavBuffer[NavBufferIndex].alt;
      latlong.set_latlon(vehstate.kf_lat, vehstate.kf_lng);
      latlong.get_UTM(&vehstate.Easting, &vehstate.Northing);
      

      vehstate.Vel_N = NavBuffer[NavBufferIndex].vn;
      vehstate.Vel_E = NavBuffer[NavBufferIndex].ve;
      vehstate.Vel_U = NavBuffer[NavBufferIndex].vd;
      vehstate.Speed = sqrt(vehstate.Vel_N * vehstate.Vel_N + vehstate.Vel_E 
			    * vehstate.Vel_E + vehstate.Vel_U * 
			    vehstate.Vel_U);
      
      
      vehstate.Yaw = NavBuffer[NavBufferIndex].thdg * DEG2RAD;
      vehstate.Pitch = NavBuffer[NavBufferIndex].pitch;
      vehstate.Roll =  NavBuffer[NavBufferIndex].roll;


      
	// log data
	if (logflag)
	  {
	    imufile.precision(10);
	    imufile<<imu_time<<'\t'<<imudata.dvx<<'\t'<<imudata.dvy<<
	      '\t'<<imudata.dvz<<'\t'<<imudata.dtx<<'\t'<<imudata.dty<<
	      '\t'<<imudata.dtz<<endl;

	    // if there's been a Kalman update then save the kf data
	    // counter to slow logging to 10 Hz
	    if ((imu1hz >= 400) && 
		(NavBuffer[NavBufferIndex].KalmanUpdate != 0))
	      {
		statefile.precision(10);
		statefile<<imu_time<<'\t'<<vehstate.Easting<<
		  '\t'<<vehstate.Northing<<'\t'<<vehstate.Altitude<<
		  '\t'<<vehstate.Vel_E<<'\t'<<vehstate.Vel_N<<
		  '\t'<<vehstate.Vel_U<<'\t'<<vehstate.Speed<<
		  '\t'<<vehstate.Accel<<'\t'<<vehstate.Pitch<<
		  '\t'<<vehstate.Roll<<'\t'<<vehstate.Yaw<<
		  '\t'<<vehstate.kf_lat<<'\t'<<vehstate.kf_lng<<endl;

		kffile.precision(10);
		kffile<<imu_time<<
		  '\t'<<NavBuffer[NavBufferIndex].KalmanUpdate<<
		  '\t'<<ExtGPSPosMeas_x<<'\t'<<ExtGPSPosMeas_y<<
		  '\t'<<ExtGPSPosMeas_z<<'\t'<<ExtGPSVelMeas_x<<
		  '\t'<<ExtGPSVelMeas_y<<'\t'<<ExtGPSVelMeas_z<<
		  '\t'<<tot_kabx<<'\t'<<tot_kaby<<'\t'<<tot_kabz<<
		  '\t'<<tot_kgbx<<'\t'<<tot_kgby<<'\t'<<tot_kgbz<<
		  '\t'<<tot_kgsfx<<'\t'<<endl;
	    
	    
		// log covariance matrix, row by row   
		kpfile.precision(10);
		
		// log the time and fill the rest of the row with zeros
		kpfile<<imu_time<<'\t';
		for (col = 1; col <= NUM_ST; col++){
		  kpfile<<0<<'\t';
		}
		kpfile<<endl;
		    
		// log the covariance matrix itself
		for(col = 0; col <= NUM_ST; col++)
		  {
		    for (row = 0; row <= NUM_ST; row++)
		      {
			kpfile<<kp[row][col]<<'\t';
		      }
		    kpfile<<endl;
		  }
	    
		imu1hz = 0;
	      }
	    imu1hz ++;
	  }

	// store raw data into vehstate
      vehstate.imudata = imudata;

      // run kalman filter routines
      DGCNavRun();

      // increment counter to for logging state data
      //imu10hz++;
      
    }
  return NULL;
}
#endif


#ifdef GPS
/* Read messages from the GPS unit and put them into a buffer */
void *gps_start(void *arg)
{
  char *gps_buffer;
  LatLong gps_ll(0,0);
  double gps_east, gps_north;
  double speed;
  
  if ((gps_buffer = (char *) malloc(2000)) == NULL) {
    dbg_panic("GPS: memory allocation error\n");
    return NULL;
  }

  while (1) {
    if (gps_enabled > 0) {
      gps_read(gps_buffer, 1000);  //this is blocking
      switch (gps_msg_type(gps_buffer)) {
      case GPS_MSG_PVT:
	gpsdata.update_gps_data(gps_buffer);
	++gps_count;
	
	// store raw data into MTA
	vehstate.gpsdata = gpsdata.data;
	
	//PREPROCESSING SECTION
	// check for pvt valid flag
	gps_valid = (gpsdata.data.nav_mode & NAV_VALID);  
	gps_nav_mode = (int)gpsdata.data.nav_mode;
	gps_ext_nav = (int)gpsdata.data.extended_nav_mode;

	// get display string for message
	strcpy(gps_nav_msg, gps_mode(gpsdata.data.nav_mode)); 
        overview[NAVMODE].initialized = 0; // force update of string

	// set pvt valid flag in MTA so rest of system knows i
	//f we're getting gps
	vehstate.gps_pvt_valid = gps_valid;
	
	//log data
	/* Save the time the data was taken */
	gettimeofday(&tv, NULL);
        gps_local_time = (double) (tv.tv_sec - start.tv_sec) + 
	  ((double) (tv.tv_usec - start.tv_usec)) / 1e6;

	// transform gps lat/long into utm for logging
	gps_ll.set_latlon(gpsdata.data.lat, gpsdata.data.lng);
	gps_ll.get_UTM(&gps_east, &gps_north);

	//END PREPROCESSING
	

	//DATA PROCESSING SECTION       
	if(gps_valid > 0)
	  { 
	    gps_invalid_count = 0;
	    //gps_jumped = dev_reject(gps_north, gps_east);
	    
	    // if there's been a jump in the gps solution, open up the error
	    if (gps_jumped)
	    {
	      // gps_xy_kr = gps_err_reset(gps_xy_kr);
	    }

	// if KF needs new GPS data, write it over
	if ((New_GPS_data_flag == 0) &&  (gps_active)) {   

	  gps_time = gps_local_time;
          gps_lat = gpsdata.data.lat*DEG2RAD;
          gps_lon = gpsdata.data.lng*DEG2RAD;
	  gps_alt = gpsdata.data.altitude;
	  gps_vu  = gpsdata.data.vel_u;
	  gps_vn  = gpsdata.data.vel_n;
          gps_ve  = gpsdata.data.vel_e;

	  if (obdflag > 0){
	    // OBDII SECTION 
	    obd_vel = Get_Speed(); 
	    speed = sqrt(gpsdata.data.vel_n * gpsdata.data.vel_n +
			 gpsdata.data.vel_e * gpsdata.data.vel_e);

	    // if the speed is low then replace the GPS speed 
	    // and set the kf error for speed measurement
	    // these constants are defined in statefilter. 
	    if (speed <= OBD_CUTOFF){
	      gps_vn = obd_vel * cos(vehstate.Yaw);
	      gps_ve = obd_vel * sin(vehstate.Yaw);
	      GPS_v_kr = OBD_VEL_ERR;
	    }else{
	      GPS_v_kr = GPS_VEL_ERR;
	    }
	  }
          New_GPS_data_flag = 1;
	}
   
	  }else{
	    // processing for when gps is not valid
	    gps_invalid_count++;

	    // if this many seconds have gone by and the gps hasn't renewed
	    if (gps_invalid_count >= INVALID_THRESHOLD)
	    {
		// reset the gps filter
		//filter_reset();

		// do other stuff

	    }
	  }
	
	// log all data after everything is done
	if (logflag){
	  // log gps data
	  gpsfile.precision(10);
	  gpsfile<<gps_local_time<<'\t'<<gps_valid<<
	    '\t'<<New_GPS_data_flag<<'\t'<<gps_nav_mode<<
	    '\t'<<gpsdata.data.lat<<'\t'<<gpsdata.data.lng<<
	    '\t'<<gps_east<<'\t'<<gps_north<<'\t'<<gpsdata.data.altitude<<
	    '\t'<<gpsdata.data.vel_n<<'\t'<<gpsdata.data.vel_e<<
	    '\t'<<gpsdata.data.vel_u<<'\t'<<gps_active<<
	    '\t'<<gps_ext_nav<<'\t'<<gps_jumped<<
	    '\t'<<gpsdata.data.sats_used<<endl; 
	}

      
	break;
    
      default:
	/* print_gps_msg(gps_buffer) */
	break;
      }
    }
  }
  return NULL;
}
#endif

#ifdef MAGNETOMETER
// updates magnetometer data continuously
void *mag_start(void *arg)
{
  while(1){
    if ((mag_enabled > 0) && (new_mag == 0)){
      mm_read(magreading);
      mag_count++;

      // store raw mag data
      vehstate.magreading = magreading;
    }
    
    if (New_Mag_data_flag == 0) {
      mag_hdg = magreading.heading*DEG2RAD;
      New_Mag_data_flag = 1;
    }
    
  }
  return NULL;
}
#endif

/*
 * Callbacks to control operation of the program
 *
 */

/* Quit the dfan program */
int user_quit(long arg)
{
  int count = 0;
  if (logflag)
    {
      imufile.close();
      gpsfile.close();
      kffile.close();
      kpfile.close();
      statefile.close();
    }

  if( mta_flag == 1)
    {
  /* Tell MTA to shut down */
  ForceKernelShutdown();

  /* Wait for 1 second to make sure it is shut down */
  while (ShuttingDown()) {
    if (count++ > 1000) break;
    usleep(1000);
  }

}
    /* Tell dd_loop to abort now */
    return DD_EXIT_LOOP;
}

/* Toggle capture mode */
int user_cap_toggle(long arg)
{
  return chn_capture_flag ? chn_capture_off() : chn_capture_on();
}

// enables or disables data logging
// if data logging is disabled, then enable.  Otherwise disable and close
int user_log_inc(long arg)
{
  if (logflag == 1)
    {
      // disable logging and close log files
      logflag = 0;
      imufile.close();
      gpsfile.close();
      statefile.close();
      kffile.close();
      kpfile.close();
    }
  else
    {
      logflag = 1;
      logheaders();
    }
  return 1;
}

// keeps the KF from getting gps updates, for testing only
int user_gps_toggle(long arg)
{
  if (gps_active)
    gps_active = 0;
  else
    gps_active = 1;
  
  return 1;
}

// keeps the gps updates from updating the KF altitude
int user_z_toggle(long arg)
{
  /*if (ztoggle)
    ztoggle = 0;
  else
    ztoggle = 1;
  */
  return 1;
}

void logheaders()
{
  // imu file logging
	imufile.open(imuname, fstream::out|fstream::app);
        imufile<<"%imu_time"<<'\t'<<"imudata.dvx"<<'\t'<<"imudata.dvy"<<
	  '\t'<<"imudata.dvz"<<'\t'<<"imudata.dtx"<<'\t'<<"imudata.dty"<<
	  '\t'<<"imudata.dtz"<<endl;

	// gps data logging
	gpsfile.open(gpsname, fstream::out|fstream::app);
	gpsfile<<"%gps_local_time"<<'\t'<<"gps_valid"<<
	  '\t'<<"New_GPS_data_flag"<<'\t'<<"GPS Nav Mode"<<
	  '\t'<<"gpsdata.data.lat"<<'\t'<<"gpsdata.data.lng"<<
	  '\t'<<"gps_easts"<<'\t'<<"gps_north"<<
	  '\t'<<"gpsdata.data.altitude"<<'\t'<<"gpsdata.data.vel_n"<<
	  '\t'<<"gpsdata.data.vel_e"<<'\t'<<"gpsdata.data.vel_u"<<
	  '\t'<<"gps_active"<<'\t'<<"gps_ext_nav"<<'\t'<<"gps_jumped"<<
	  '\t'<<"gpsdata.data.sats_used"<<endl; 


	statefile.open(statename, fstream::out|fstream::app);
	statefile<<"%imu_time"<<'\t'<<"vehstate.Easting"<<
	  '\t'<<"vehstate.Northing"<<'\t'<<"vehstate.Altitude"<<
	  '\t'<<"vehstate.Vel_E"<<'\t'<<"vehstate.Vel_N"<<
	  '\t'<<"vehstate.Vel_U"<<'\t'<<"vehstate.Speed"<<
	  '\t'<<"vehstate.Accel"<<'\t'<<"vehstate.Pitch"<<
	  '\t'<<"vehstate.Roll"<<'\t'<<"vehstate.Yaw"<<'\t'<<"kf_lat"<<
	  '\t'<<"kf_lng"<<endl;

	// kf data logging
	kffile.open(kfname, fstream::out|fstream::app);
	kffile<<"%imu_time"<<'\t'<<"KalmanUpdate"<<
	  '\t'<<"ExtGPSPosMeas_x"<<'\t'<<"ExtGPSPosMeas_y"<<
	  '\t'<<"ExtGPSPosMeas_z"<<'\t'<<"ExtGPSVelMeas_x"<<
	  '\t'<<"ExtGPSVelMeas_y"<<'\t'<<"ExtGPSVelMeas_z"<<
	  '\t'<<"tot_kabx"<<'\t'<<"tot_kaby"<<'\t'<<"tot_kabz"<<
	  '\t'<<"tot_kgbx"<<'\t'<<"tot_kgby"<<'\t'<<"tot_kgbz"<<
	  '\t'<<"tot_kgsfx"<<endl;

	//kf covariance matrix logging
	kpfile.open(kpname, fstream::out|fstream::app);

	return;
}
