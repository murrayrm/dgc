#ifndef STATEFILTER_HH
#define STATEFILTER_HH

#include <math.h>

#include "gps.h"
#include "LatLong.h"
#include "VState.hh"


// CONSTANT DECLARATIONS
// obd2 related constant decs
#define OBD_CUTOFF .25 // m/s
#define OBD_VEL_ERR .05 // m/s
#define GPS_VEL_ERR 1 // m/s

//int check_jump(gpsDataWrapper gpsdata, double update_time, VState_GetStateMsg
//	       vehstate);


int dev_reject(double northing, double easting);
double get_avg();
double get_var();
void filter_reset();

// constant definitions
//const double ANGLE_CUTOFF = 45 * 180 / M_PI;
//const double DV_WEIGHT = 1.5;

const int NUM_ELEMENTS = 5;
#define  SigmaMult 4
#define  INVALID_THRESHOLD 10 // 10 seconds

#endif
