/* VState.hh - message format for state data
 *
 * Lars Cremean, Ike Gremmer
 * 2 January 2004
 * 
 * This file defines the format for state data that is requested
 * from VState.  It will probably go in a file with a different name,
 * but for now it is here.
 *
 * HH 3 Jan 04
 * changed attitude angles to radians, added magnetometer
 */

#ifndef VSTATE_HH
#define VSTATE_HH

#include "MTA/Misc/Time/Timeval.hh"
#include "MTA/Misc/Mail/Mail.hh"

#include "imu.h"
#include "gps.h"
#include "magnetometer.h"

// State Estimator internal constant declarations
/* If velocities are less than these values, consider them to be 0. */
#define VEL_N_VALID_CUTOFF 	0.1
#define VEL_E_VALID_CUTOFF 	0.1
#define VEL_U_VALID_CUTOFF 	0.1

/* When the speed of the car is higher than this, then don't use magnetometer */
#define MAGNET_VALID_CUTOFF	1  // in m/s
#define MAG_OFFSET 30 // in degrees

struct VState_GetStateMsg
{
  Timeval Timestamp;   // time stamp when GPS data is received

  double Easting;      // UTM easting in meters
  double Northing;     // UTM northing in meters
  float  Altitude;     // altitude in meters (from what reference?)

  double kf_lat;       // latitude from KF
  double kf_lng;       // longitude from KF

  float  Vel_E;        // east velocity in m/s
  float  Vel_N;        // north velocity in m/s
  float  Vel_U;        // up velocity in m/s

  float  Speed;        // m/s 
  double Accel;	       // m/s^2

  float  Pitch;        // pitch in radians
  float  Roll;         // roll in radians
  float  Yaw;          // heading in radians, north=0 rads 
                       // clockwise increase
  
  // currently not implemented
  float  PitchRate;    
  float  RollRate;
  float  YawRate;

  // flags for sensors, 1 means they're up and running, 0 or -1 means they're inactive
  int gps_enabled;
  int imu_enabled;
  int mag_enabled;

  // gps pvt has a solution or not
  int gps_pvt_valid;

  struct gpsData gpsdata;	// raw GPS data
  struct imu_data imudata;	// raw IMU data
  struct mag_data magreading;   // raw magnetometer data
};

// enumerate all module messages that could be received
namespace VStateMessages {
  enum {
    GetState,			// Read the current state from the file
    NumMessages			// Placeholder (not used)
  };
};

#endif
