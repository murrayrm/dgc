//Code to run the throttle actuator]

#ifndef THROTTLE_H
#define THROTTLE_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include "parallel.h"

//NOTE: throughout this file, variables ending in counts refer to the digipot's native
//counts, 0-127; variables without this ending refer to throttle position from 0-1
//The system design dictates that the counts value of 127 is equivalent to throttle
//position of 0.0, corresponding to a fully closed, or idle, throttle.  The counts value
//of 0 is equivalent to a throttle position of 1, corresponding to fully open, or full
//throttle

/* Constants determined by the minimum and maximum values the digipot can have
 *  The gas actuator has been mechanically adjusted so this represents the full
 *  range of motion of the gas pedal.  Reducing these values will limit the range 
 *  accorgingly.  Note that THROTTLE_MIN_POT_VAL_COUNTS corresponds to full throttle
 *  and vice versa
 *  */
#define THROTTLE_MIN_POT_VAL_COUNTS 0
#define THROTTLE_MAX_POT_VAL_COUNTS 127

/* Constants determining which pins on the digipot correspond to which pins on 
 * the parallel port
 */
//~~  #define ACCEL_CLK PP_PIN07
//~~  #define ACCEL_UD PP_PIN08
#define THROTTLE_CLK PP_PIN01
#define THROTTLE_UD PP_PIN16
#define THROTTLE_CS PP_PIN17

//Functions general to all drivers

//Initialization function for the gas actuator and sets up the parallel port used by the gas, 
//brake pot, transmission, and ignition.
//**********NOTE: throttle_calibrate must be run before the first movement after vdrive starts*************
int throttle_open(int port);

//Calibration function for the gas actuator
int throttle_calibrate();

//Moves the throttle to the zero (throttle fully closed) position
int throttle_zero();

//Simple status checker that returns 0 for ok and -1 for error in throttle function
int throttle_ok();

//Arguments for throttle_status, valued to facilitate using bit masks to determine which data to return
//user must simply pass in the sum of the data elements he wishes returned to the struct
#define THROTTLE_POS = 1
#define THROTTLE_ENABLED = 2
#define THROTTLE_MOVING  = 4
#define THROTTLE_ALL = THROTTLE_POS + THROTTLE_ENABLED + THROTTLE_MOVING 

//The struct for the status return
struct throttle_status_data {
  int status;
};

//Request status report of type request_type to be returned at *throttle_status_data which is zeros except 
//for the data requested.  Note that this function will currently not return a struct.  Call throttle_ok
//to get that info and throttle_read to get a position.
int throttle_status(int request_type, struct throttle_status_type *throttle_status_data);

//Suspend throttle operation upon a pause estop command
int throttle_pause();

//Suspend throttle operation upon a estop disable command
int throttle_disable();

//Recume throttle operation after a pause command
int throttle_resume();

//Closes the port, unallocates memory, stops threads
int throttle_close();

//Functions specific to this driver

//Returns throttle position
double throttle_read();

//Change the gas actuator position based on the 0-1 scale
int throttle_write(double desired_pos);

//Helper functions

//Change the gas actuator position based on an amount to increment or decrement on the 0-127 scale
int throttle_increment(int desired_pos_change_counts);

//Helper function which returns min_val if val<min_val, max_val if val>max_val, or otheriwse returns val
double throttle_in_range(double val, double min_val, double max_val);

#endif









