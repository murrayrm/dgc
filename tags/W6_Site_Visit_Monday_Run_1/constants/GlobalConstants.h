#ifndef GLOBALCONSTANTS_H
#define GLOBALCONSTANTS_H

/*! This file should contain all constants that are independent of which 
 ** platform the code is running on and independent of any specific code
 ** implementation. */

// Conversions for RDDF file
#define METERS_PER_FOOT 0.304800609
#define MPS_PER_MPH     0.447040893

// RDDF utility constants
#define RDDF_FILE "rddf.dat"
#define LAST_WAYPOINT_SPEED 2.0

#warning "This depends on other constants in util/planner/*Defs.h"
#define LAST_WAYPOINT_DIST  39.5
#endif
