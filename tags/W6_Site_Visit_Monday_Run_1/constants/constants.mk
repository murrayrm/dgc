CONSTANTS_PATH = $(DGC)/constants

CONSTANTS_FILES = \
	$(CONSTANTS_PATH)/AliceConstants.h  \
	$(CONSTANTS_PATH)/GlobalConstants.h  \
	$(CONSTANTS_PATH)/ActuatorState.hh \
	$(CONSTANTS_PATH)/VehicleState.hh \
	$(CONSTANTS_PATH)/LadarConstants.h \
	$(CONSTANTS_PATH)/SensorConstants.h	

CONSTANTS_DEPEND = $(CONSTANTS_FILES) \
	$(CONSTANTS_PATH)/Makefile
