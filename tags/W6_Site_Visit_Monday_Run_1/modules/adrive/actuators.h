/*  This is the header file for actuators.c.  
 * actuators.c is the threading interface for the steering code.  
 */

#ifndef _STEER_H
#define _STEER_H

/*! The standadized function call to execute trans command*/
extern int  execute_trans_command( double command );
/*! The standadized function call to execute trans status*/ 
extern int  execute_trans_status();
/*! The standadized function call to execute trans initialization*/
extern void execute_trans_init();

/*! The standadized function call to execute estop command*/
extern int execute_estop_command( double command ); 
/*! The standadized function call to execute estop status*/
extern int execute_estop_status();
/*! The standadized function call to execute estop initialization*/
extern void execute_estop_init();

/*! The standadized function call to execute gas command*/
extern int  execute_gas_command( double command );
/*! The standadized function call to execute gas status*/ 
extern int  execute_gas_status();
/*! The standadized function call to execute gas initialization*/
extern void execute_gas_init();

/*! The standadized function call to execute steer command*/
int  execute_steer_command( double command );
/*! The standadized function call to execute steer status*/
int  execute_steer_status();
/*! The standadized function call to execute steer initialization*/
void execute_steer_init();

/*! The standadized function call to execute brake command*/
int  execute_brake_command( double command );
/*! The standadized function call to execute brake status*/
int  execute_brake_status();
/*! The standadized function call to execute brake initialization*/
void execute_brake_init();

#endif //_STEER_H
