#!/bin/sh -x

cd logs

time1="153346"
time2="151518"

tag1="_20050423_"$time1
tag2="_20050423_"$time2

rm -f state path path2 yerror* p_* i_* d_* vref vact verr* yerrF* yerrR*


paste state$tag1.dat | awk '{print $2, $3}' > state$tag1
paste state$tag2.dat | awk '{print $2, $3}' > state$tag2

paste ../../util/RDDF/bob.dat | awk '{print $3,$2}' > path

paste steps$tag1.dat | awk '{print NR, $1}' > FF$tag1
paste steps$tag2.dat | awk '{print NR, $1}' > FF$tag2

paste steps$tag1.dat | awk '{print NR, $2}' > FB$tag1
paste steps$tag2.dat | awk '{print NR, $2}' > FB$tag2

paste pid$tag1.dat | awk '{if(($3 !=0)&&($2<100)&&($2>-100)) print NR,$1}' > p$tag1
paste pid$tag2.dat | awk '{if(($3 !=0)&&($2<100)&&($2>-100)) print NR,$1}' > p$tag2

paste pid$tag1.dat | awk '{if(($3 !=0)&&($2<100)&&($2>-100)) print NR,$2}' > i$tag1
paste pid$tag2.dat | awk '{if(($3 !=0)&&($2<100)&&($2>-100)) print NR,$2}' > i$tag2

paste pid$tag1.dat | awk '{if(($3 !=0)&&($2<100)&&($2>-100)) print NR,$3}' > d$tag1
paste pid$tag2.dat | awk '{if(($3 !=0)&&($2<100)&&($2>-100)) print NR,$3}' > d$tag2

paste error$tag1.dat | awk '{print NR,$1}' > yerrF$tag1
paste error$tag2.dat | awk '{print NR,$1}' > yerrF$tag2

paste steps$tag1.dat | awk '{print NR, $2}' > FB$tag1
paste steps$tag2.dat | awk '{print NR, $2}' > FB$tag2

paste velocity$tag1.dat | awk '{print NR, $1}' > vref$tag1
paste velocity$tag2.dat | awk '{print NR, $1}' > vref$tag2

paste velocity$tag1.dat | awk '{print NR, $2}' > vact$tag1
paste velocity$tag2.dat | awk '{print NR, $2}' > vact$tag2

paste velocity$tag1.dat | awk '{print NR, $3}' > verr$tag1
paste velocity$tag2.dat | awk '{print NR, $3}' > verr$tag2

#echo "plot 'yerrF' with dots,'yerrR' with dots, 'FF' with dots, 'FB' with dots" | gnuplot -persist
#echo "plot 'vref' with l, 'vact' with l, 'verr' with l" | gnuplot -persist
#echo "plot 'p', 'i', 'd' with dots" | gnuplot -persist

echo "plot 'state$tag2' with dots, 'state$tag1' with dots, 'path'" | gnuplot -persist
echo "plot 'yerrF$tag1', 'yerrF$tag2'" | gnuplot -persist