#ifndef TRAJFOLLOWER_HH
#define TRAJFOLLOWER_HH

#include "StateClient.h"
#include "TrajTalker.h"
#include <pthread.h>
#include <fstream>
using namespace std;

#include "traj.h"
#include "PID_Controller.hh"

class TrajFollower : public CStateClient, public CTrajTalker
{
  /*! The steer command to send to adrive (in SI units). */
  double m_steer_cmd;

  /*! The accel command to send to adrive (in SI units). */
  double m_accel_cmd;

  /*! The trajectory controller. */
  CPID_Controller* m_pPIDcontroller;

  /*! how many times the while loop in the Active function has run.  */
  int m_nActiveCount;

  /*! The number of trajectories we have received. */
  int m_trajCount;

  /*! The current trajectory to follow. */
  CTraj* m_pTraj;

  /*! Time that module started. */
  unsigned long long m_timeBegin; 

  /*! Logs to output the plans and the state */
  ofstream m_outputPlans, m_outputPlanStarts, m_outputState, m_outputTest;

	/*! The skynet socket for the communication with drive */
	int m_drivesocket;

	/*! The mutex to protect the traj we're following */
	pthread_mutex_t		m_trajMutex;

	/*! The skynet socket for receiving trajectories */
	int								m_trajSocket;

public:
  TrajFollower(int sn_key);
  ~TrajFollower();

  void Active();

  void Comm();

  void SparrowDisplayLoop();

  void UpdateSparrowVariablesLoop();
};

#endif
