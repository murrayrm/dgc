//ModuleStater_test.cc: send command do ModuleStarter
#include "ModuleStarter.hh"
#include "sn_msg.hh"
#include "string.h"
#include <unistd.h>

int main(int argc, char** argv){  
  int sn_key;
  char* default_key = getenv("SKYNET_KEY");
  if (2 == argc)
    {
      sn_key = atoi(argv[1]);
    }
  else if (default_key != NULL )
    {
      sn_key = atoi(default_key);
    }
  else
    {
      printf("usage: sn_get_test key\nDefault key is $SKYNET_KEY\n");
      exit(2);
    };
  
  
  modulename myself = SNGuimodstart;
  sn_msg myoutput = SNmodcom;
  modulename other_mod = SNmodstart;   
  skynet Skynet(myself, sn_key);
  printf("My module id is: %u\n", Skynet.get_id());
  int socket = Skynet.get_send_sock(myoutput);

  int c;
  struct command cmd[4];
  while((c = getchar()) != EOF)
    {
      if(c == 'c')
	break;
    }
  cout << "Sending asim";
  strcpy(cmd[1].type,"SNfusionmapper");
  cmd[1].PID = 0;
  cmd[1].action = 's';
  cmd[1].compID = 9;
  Skynet.send_msg(socket,&cmd[1],sizeof(struct command),0);
  sleep(1);
  // now kill it 
  cmd[1].action = 'k';
  while((c = getchar()) != EOF)
    {
      if(c == 'c')
	break;
    }
  cout << "killing asim" << endl;
  Skynet.send_msg(socket,&cmd[1],sizeof(struct command),0);
  sleep(1);
  exit(3);

  while((c = getchar()) != EOF)
    {
      if(c == 'c')
	break;
    }
  strcpy(cmd[0].type,"SNtrajfollower");
  cmd[0].PID=0;
  cmd[0].action = 's';
  cmd[0].compID = 1;
  Skynet.send_msg(socket, &cmd[0], sizeof(struct command), 0);
  sleep(1);
  while((c = getchar()) != EOF)
    {
      if(c == 'c')
	break;
    }
  strcpy(cmd[2].type,"SNModeMan");
  cmd[2].PID = 0;
  cmd[2].action = 'k';
  cmd[2].compID = 6;
  Skynet.send_msg(socket,&cmd[2],sizeof(struct command),0);
  sleep(1);
  while((c = getchar()) != EOF)
    {
      if(c == 'c')
	break;
    }
  cmd[0].action = 'K';
  Skynet.send_msg(socket, &cmd[0], sizeof(struct command), 0);
  sleep(1);
  while((c = getchar()) != EOF)
    {
      if(c == 'c')
	break;
    }
  cmd[0].action = 's';
  Skynet.send_msg(socket, &cmd[0], sizeof(struct command), 0);
  while((c = getchar()) != EOF)
    {
      if(c == 'c')
	break;
    }
  cmd[2].action = 'k';
  Skynet.send_msg(socket, &cmd[2], sizeof(struct command), 0);
  while((c = getchar()) != EOF)
    {
      if(c == 'c')
	break;
    }
  cmd[2].action = 'k';
  Skynet.send_msg(socket, &cmd[2], sizeof(struct command), 0);
  while((c = getchar()) != EOF)
    {
      if(c == 'c')
	break;
    }
  cmd[0].action = 'k';
  Skynet.send_msg(socket, &cmd[0], sizeof(struct command), 0);
  while((c = getchar()) != EOF)
    {
      if(c == 'c')
	break;
    }
  cmd[1].action = 'k';
  Skynet.send_msg(socket, &cmd[1], sizeof(struct command), 0);

  /**
  strcpy(cmd[1].type,SNSIM);
  strcpy(cmd[1].adress, SNSIM_ADRESS);
  cmd[1].PID = 0;
  cmd[1].compID = 6;
  Skynet.send_msg(socket,&cmd[1],sizeof(struct command),0);

  sleep(1);

  strcpy(cmd[2].type,SNRDDFGEN);
  strcpy(cmd[2].adress,SNRDDFGEN_ADRESS);
  cmd[2].PID = 0;
  cmd[2].compID = 5;
  Skynet.send_msg(socket,&cmd[2],sizeof(struct command),0);

  sleep(1);

  strcpy(cmd[3].type,SNMODMAN);
  strcpy(cmd[3].adress,SNMODMAN_ADRESS);
  cmd[3].PID = 0;
  cmd[3].compID = 6;
  Skynet.send_msg(socket,&cmd[3],sizeof(struct command),0);
  */


  /*int CMDSIZE = sizeof(struct command);
  struct command cmd2;
  int sock1 = Skynet.listen(myoutput, other_mod);            //getting message via skynet
  Skynet.get_msg(sock1, &cmd2, CMDSIZE, 0);
  printf("module PID is %d",cmd2.PID);*/
  return 0;
}
