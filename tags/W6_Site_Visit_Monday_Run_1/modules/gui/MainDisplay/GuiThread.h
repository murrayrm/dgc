/**
 * GuiThread.h
 * Revision History:
 * 01/21/2005  hbarnor  Created
 */

#ifndef GUITHREAD_H
#define GUITHREAD_H

#include "gui.h"

using namespace std;

class GuiThread
{
protected:
  //Glib::ustring                  m_APP_NAME;
  /**
   * Reference to athe map config file.
   * This might not be necessary.
   */ 
  MapConfig * m_map_config;
  /**
   * The thread that runs the gui
   *
   */
  Glib::Thread *  m_thread; 
  /**
   * The main method to actual get the gui going. 
   * Initilize GUI.
   */
  void main();
  /**
   * instance of the gui that this thread is running
   */
  Gui::Gui * window;
  Gtk::Main * kit;
public:
  /**
   * An accessor for the map config reference
   */
  MapConfig * get_map_config();
  /**
   * Constructor for the GuiThread
   *
   */
  GuiThread(MapConfig * mc);
  /**
   * Destructor
   */
  ~GuiThread();
  /**
   * Start method for the thread. 
   * spwans a new thread and runs the gui
   */
  void start_thread();
  /**
   * GetGui - returns access to the gui
   * running inside the thread
   */
  Gui* getGui();
};

#endif
