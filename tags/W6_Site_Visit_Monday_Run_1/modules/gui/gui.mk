SKYNETGUI_PATH=$(DGC)/modules/gui

GUI_DEPEND_LIBS = \
	$(ADRIVELIB) \
	$(CMAPLIB) \
	$(CMAPPLIB) \
	$(GGISLIB) \
	$(MAPDISPLAYLIB) \
	$(RDDFLIB) \
	$(SKYNETLIB) \
	$(TRAJLIB) \
	$(MODULEHELPERSLIB) \
	$(ADRIVELIB)

GUI_DEPEND_SOURCES = \
	$(SKYNETGUI_PATH)/MapDisplaySN/MapDisplaySN.cc \
	$(SKYNETGUI_PATH)/MapDisplaySN/MapDisplaySN.hh \
	\
	$(SKYNETGUI_PATH)/MainDisplay/gui.h \
	$(SKYNETGUI_PATH)/MainDisplay/gui.cc \
	\
	$(SKYNETGUI_PATH)/MainDisplay/GuiThread.cc \
	$(SKYNETGUI_PATH)/MainDisplay/GuiThread.h \
	\
	$(SKYNETGUI_PATH)/StartTab/StartTab.cc \
	$(SKYNETGUI_PATH)/StartTab/StartTab.hh \
	\
	$(SKYNETGUI_PATH)/MainDisplay/MainTab.h \
	$(SKYNETGUI_PATH)/MainDisplay/MainTab.cc \
	\
	$(SKYNETGUI_PATH)/main_mde.cc \
	$(SKYNETGUI_PATH)/main.cc \

GUI_DEPEND = $(GUI_DEPEND_SOURCES) $(GUI_DEPEND_LIBS)
