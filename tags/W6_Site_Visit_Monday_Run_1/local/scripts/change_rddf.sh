#!/bin/bash

# script to copy an .rddf file to some race computers.
# NOTE: replace $USER with the desired username on the race computers!
# 
# Created by: Henrik Kjellander 19 Sep 2004


USER=henrik
COMPUTERS="titanium tantalum osmium magnesium iridium cobalt"
BUILDSTRING="cd dgc/bob/RDDF; ./bob2rddf.sh > $1.rddf < $1.bob; rm -f rddf.dat; ln -s $1.rddf rddf.dat; rm -f bob.dat; ln -s $1.bob bob.dat"

if [ ! "$1" ]; then
  echo "Script for changing RDDF (.rddf and .bob) file on multiple computers:"
  echo "Usage: ./change_rddf.sh filename (without the .bob extension)"
  echo "- filename: this must be a filename of a file in .bob format (WITHOUT the .bob extension)."
  echo "NOTE: You must edit this script if you don't want to login with: $USER"
  echo 
  echo "What this script does:"
  echo " 1. Copies the .bob file to these computers: $COMPUTERS"
  echo " 2. Converts the .bob file to a .rddf file on each computer"
  echo " 3. Updates the rddf.dat and bob.dat links"
  exit 0;
fi

# First check if the .bob file exists
if [ ! -f $1.bob ]; then
  echo "$1.bob file not found in current directory. Please make sure your"
  echo "current directory contains your $1.bob file!"
  exit -1
fi

for comp in $COMPUTERS
do
  echo Copying $1.bob to $comp:
  if(scp $1.bob $USER@$comp:dgc/bob/RDDF); then
      echo Successfully copied $1.bob to $comp!
      # Only do the rest if that copying went OK
      xterm -sl 9999 -geometry 100x30-500-300 -e "ssh $USER@$comp '$BUILDSTRING';"
      echo
  else 
      echo Failed copying $1.bob to $comp! Please check the network connections!
      echo
  fi
done

echo finished!

exit 0;
