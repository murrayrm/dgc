// estop drivers for DGC 2005 Alice
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "estop.h"
#include "SDSPort.h"
#include "vehports.h"
#include "constants.h"

int estopPortFlag = FALSE;
SDSPort* estopPort;
extern char simulator_IP[];
int estopSerialPort = -1;
char estop_read[ESTOP_BIG_INPUT_LENGTH];

int estop_open(int port) {
  if (estopPortFlag == FALSE) {
    estopPort = new SDSPort("192.168.0.60", port);
    estopPortFlag = TRUE;
    estopSerialPort = port;
  }
  else estopPort->openPort();

  if (estopPort->isOpen() == 1) {
    estopSerialPort = port;
    return TRUE;
  }
  else return FALSE;
}



void simulator_estop_open(int port) {
  estopPort = new SDSPort(simulator_IP, port);
  estopPortFlag = TRUE;
  estopSerialPort = port;
}
  


int estop_close() {
  if (estopPort->closePort() == 0) return TRUE;
  else return FALSE;
}


int estop_status(void) {
  int read_length;
  read_length = estopPort->read(ESTOP_BIG_INPUT_LENGTH, estop_read, 10000);
  //  printf("estop read is %c\n", estop_read[read_length - 1]);
  if (estop_read[read_length - 1] == RUN_CHAR) return RUN;
  else if (estop_read[read_length - 1] == DISABLE_CHAR) return DISABLE;
  else return PAUSE;
}
