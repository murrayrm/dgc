function result = generateTerrain(min, length, step, type, params)
    result(:,1) = min:step:length;
    switch type
        case 0
            result(:,2) = params(1);
        case 1
            result(:,2) = params(1)*sin(params(2).*result(:,1))+params(3);
        case 2
            result(:,2) = abs(params(1)*sin(params(2).*result(:,1))+params(3));
        case 3
            result(:,2) = params(1);
            startCell = (params(2)-min)/step;
            endCell = startCell+params(3)/step;
            result(startCell:endCell,2) = params(4);
        otherwise
            result(:,2) = 0;
            sprintf('Unknown terrain type - using default of flat')
    end
    
        