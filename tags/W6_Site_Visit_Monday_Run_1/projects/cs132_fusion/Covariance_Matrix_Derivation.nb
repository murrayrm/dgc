(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 5.0'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[     11858,        246]*)
(*NotebookOutlinePosition[     12502,        268]*)
(*  CellTagsIndexPosition[     12458,        264]*)
(*WindowFrame->Normal*)



Notebook[{
Cell[BoxData[
    \(First\ we\ generate\ the\ basic\ vectors\ and\ rotation\ matrices\ we\ \
will\ \(\(need\)\(.\)\)\)], "Input"],

Cell[BoxData[{
    \(\(rawMeas\  = \ {{\[Rho]}, {0}, {0}};\)\), "\[IndentingNewLine]", 
    \(\(rotStoV\  = \ {{Cos[\[Phi]], \ 0, \ Sin[\[Phi]]}, {0, 1, 
            0}, {\(-Sin[\[Phi]]\), 0, 
            Cos[\[Phi]]}};\)\), "\[IndentingNewLine]", 
    \(\(transStoV\  = \ {{xlv}, {0}, {zlv}};\)\), "\[IndentingNewLine]", 
    \(\(rotVtoG\  = \ {{Cos[pvg], \ 0, \ Sin[pvg]}, {0, 1, 
            0}, {\(-Sin[pvg]\), 0, Cos[pvg]}};\)\), "\[IndentingNewLine]", 
    \(\(transVtoG\  = \ {{xvg}, {0}, {zvg}};\)\)}], "Input"],

Cell[BoxData[
    \(Then\ we\ propagate\ the\ noise\ throughout\ the\ coordinate\ \
transformations, \ until\ we\ come\ to\ the\ final\ noisy\ measurement, \ 
    and\ the\ final\ "\<clean\>"\ \(\(measurement\)\(.\)\)\)], "Input"],

Cell[BoxData[{
    \(\(rawMeasClean\  = \ 
        rawMeas /. \[Rho] \[Rule] \[Rho]h;\)\), "\[IndentingNewLine]", 
    \(\(rawMeasNoisy = 
        rawMeas /. \[Rho] \[Rule] \[Rho]\[Epsilon];\)\), \
"\[IndentingNewLine]", 
    \(\(measVNoisy = 
        rotStoV . rawMeasClean + rotStoV . rawMeasNoisy + 
          transStoV;\)\), "\[IndentingNewLine]", 
    \(\(measVClean\  = 
        rotStoV . rawMeasClean + transStoV;\)\), "\[IndentingNewLine]", 
    \(\(transVtoGFull\  = \ \(transVtoG /. 
            xvg \[Rule] xvgh + xvg\[Epsilon]\) /. 
          zvg \[Rule] zvgh + zvg\[Epsilon];\)\), "\[IndentingNewLine]", 
    \(\(rotVtoGFull = \ 
        rotVtoG /. 
          pvg \[Rule] pvgh + pvg\[Epsilon];\)\), "\[IndentingNewLine]", 
    \(\(rotVtoGClean\  = \ 
        rotVtoG /. pvg \[Rule] pvgh;\)\), "\[IndentingNewLine]", 
    \(\(rotVtoGNoisy\  = \ 
        rotVtoGFull\  - \ rotVtoGClean;\)\), "\[IndentingNewLine]", 
    \(\(globalNoisyMeas\  = \ 
        transVtoGFull + rotVtoGClean . measVNoisy + 
          rotVtoGNoisy . measVClean;\)\), "\[IndentingNewLine]", 
    \(\(globalTrueMeas\  = \ \ \(\(\(transVtoG + 
                  rotVtoG . \((transStoV + 
                        rotStoV . rawMeas)\) /. \[Rho] \[Rule] \[Rho]h\) /. 
              pvg \[Rule] pvgh\) /. xvg \[Rule] xvgh\) /. 
          zvg \[Rule] zvgh;\)\)}], "Input"],

Cell[BoxData[
    \(The\ matrix\ representing\ the\ noise\ alone\ is\ then\ the\ \
"\<clean\>"\ measurement\ subtracted\ from\ the\ full\ measurement . \ \ We\ \
apply\ the\ small\ angle\ approximation\ and\ neglect\ higher\ order\ cross - 
      terms\ and\ we\ get\ out\ our\ covariance\ \(\(matrix\)\(.\)\)\)], \
"Input"],

Cell[CellGroupData[{

Cell[BoxData[{
    \(\(globalNoisyMeas\  - \ globalTrueMeas // 
        TrigExpand;\)\), "\[IndentingNewLine]", 
    \(\(globalNoiseMat = \(\(\(\(\(\(\(% /. 
                        Sin[\[Rho]\[Epsilon]] -> \[Rho]\[Epsilon]\) /. 
                      Cos[\[Rho]\[Epsilon]] \[Rule] 1\) /. 
                    Sin[pvg\[Epsilon]] -> pvg\[Epsilon]\) /. 
                  Cos[pvg\[Epsilon]] \[Rule] 1\) /. 
                Sin[xvg\[Epsilon]] -> xvg\[Epsilon]\) /. 
              Cos[xvg\[Epsilon]] \[Rule] 1\) /. 
            Sin[zvg\[Epsilon]] -> zvg\[Epsilon]\) /. 
          Cos[zvg\[Epsilon]] \[Rule] 1;\)\), "\[IndentingNewLine]", 
    \(\(initialCovarMat\  = \ 
        globalNoiseMat . 
          Transpose[globalNoiseMat];\)\), "\[IndentingNewLine]", 
    \(\(\(\(\(\(\(Expand[%] /. xvg\[Epsilon]\ zvg\[Epsilon] \[Rule] 0\) /. 
                pvg\[Epsilon]\ zvg\[Epsilon] \[Rule] 0\) /. 
              pvg\[Epsilon]\ xvg\[Epsilon] \[Rule] 0\) /. 
            zvg\[Epsilon]\ \[Rho]\[Epsilon] \[Rule] 0\) /. 
          xvg\[Epsilon]\ \[Rho]\[Epsilon] \[Rule] 0\) /. 
        pvg\[Epsilon]\ \[Rho]\[Epsilon] \[Rule] 
          0;\)\[IndentingNewLine]\), "\[IndentingNewLine]", 
    \(\(\(\(\(\(\(\(\(\(Collect[%, \ {\[Rho]\[Epsilon]\^2, \ 
                            pvg\[Epsilon]\^2, \ xvg\[Epsilon]\^2, \ 
                            zvg\[Epsilon]\^2}] /. \[Rho]\[Epsilon]\^2 \[Rule] 
                          sensorrangeuncertainty\) /. 
                      pvg\[Epsilon]\^2 \[Rule] 
                        vehicleorientationuncertainty[1]\) /. 
                    xvg\[Epsilon]\^2 \[Rule] vehiclepositionuncertainty[1]\) /. 
                  zvg\[Epsilon]\^2 \[Rule] 
                    vehiclepositionuncertainty[3]\) /. \[Rho]h \[Rule] 
                  corruptrawmeas\) /. 
              pvgh \[Rule] corruptvehiclepitch\) /. \[Phi] \[Rule] 
              sensorpitch\) /. xlv \[Rule] sensoroffset[1]\) /. 
        zlv \[Rule] sensoroffset[3];\)\), "\[IndentingNewLine]", 
    \(\(finalResult\  = %;\)\), "\[IndentingNewLine]", 
    \(MatrixForm[Simplify[finalResult]]\)}], "Input"],

Cell[BoxData[
    TagBox[
      RowBox[{"(", "\[NoBreak]", GridBox[{
            {\(1\/2\ \((sensorrangeuncertainty + 
                    corruptrawmeas\^2\ vehicleorientationuncertainty[1] + 
                    2\ corruptrawmeas\ Cos[sensorpitch]\ sensoroffset[
                        1]\ vehicleorientationuncertainty[1] - 
                    2\ corruptrawmeas\ Cos[
                        2\ corruptvehiclepitch + sensorpitch]\ sensoroffset[
                        1]\ vehicleorientationuncertainty[1] + 
                    sensoroffset[1]\^2\ vehicleorientationuncertainty[1] - 
                    Cos[2\ corruptvehiclepitch]\ sensoroffset[1]\^2\ \
vehicleorientationuncertainty[1] + 
                    sensoroffset[3]\^2\ vehicleorientationuncertainty[1] + 
                    Cos[2\ corruptvehiclepitch]\ sensoroffset[3]\^2\ \
vehicleorientationuncertainty[1] - 
                    2\ sensoroffset[1]\ sensoroffset[3]\ Sin[
                        2\ corruptvehiclepitch]\ \
vehicleorientationuncertainty[1] - 
                    2\ corruptrawmeas\ sensoroffset[3]\ Sin[
                        sensorpitch]\ vehicleorientationuncertainty[1] - 
                    2\ corruptrawmeas\ sensoroffset[3]\ Sin[
                        2\ corruptvehiclepitch + 
                          sensorpitch]\ vehicleorientationuncertainty[1] + 
                    Cos[2\ \((corruptvehiclepitch + 
                              sensorpitch)\)]\ \((sensorrangeuncertainty - 
                          corruptrawmeas\^2\ vehicleorientationuncertainty[
                              1])\) + 2\ vehiclepositionuncertainty[1])\)\), 
              "0", \(1\/2\ \((\((\(-2\)\ corruptrawmeas\ Cos[
                              2\ corruptvehiclepitch + 
                                sensorpitch]\ sensoroffset[3] - 
                          2\ Cos[2\ corruptvehiclepitch]\ sensoroffset[
                              1]\ sensoroffset[3] + 
                          sensoroffset[1]\^2\ Sin[2\ corruptvehiclepitch] - 
                          sensoroffset[3]\^2\ Sin[2\ corruptvehiclepitch] + 
                          2\ corruptrawmeas\ sensoroffset[1]\ Sin[
                              2\ corruptvehiclepitch + 
                                sensorpitch])\)\ \
vehicleorientationuncertainty[1] + 
                    Sin[2\ \((corruptvehiclepitch + 
                              sensorpitch)\)]\ \((\(-sensorrangeuncertainty\) \
+ corruptrawmeas\^2\ vehicleorientationuncertainty[1])\))\)\)},
            {"0", "0", "0"},
            {\(1\/2\ \((\((\(-2\)\ corruptrawmeas\ Cos[
                              2\ corruptvehiclepitch + 
                                sensorpitch]\ sensoroffset[3] - 
                          2\ Cos[2\ corruptvehiclepitch]\ sensoroffset[
                              1]\ sensoroffset[3] + 
                          sensoroffset[1]\^2\ Sin[2\ corruptvehiclepitch] - 
                          sensoroffset[3]\^2\ Sin[2\ corruptvehiclepitch] + 
                          2\ corruptrawmeas\ sensoroffset[1]\ Sin[
                              2\ corruptvehiclepitch + 
                                sensorpitch])\)\ \
vehicleorientationuncertainty[1] + 
                    Sin[2\ \((corruptvehiclepitch + 
                              sensorpitch)\)]\ \((\(-sensorrangeuncertainty\) \
+ corruptrawmeas\^2\ vehicleorientationuncertainty[1])\))\)\), 
              "0", \(1\/2\ \((sensorrangeuncertainty + 
                    corruptrawmeas\^2\ vehicleorientationuncertainty[1] + 
                    2\ corruptrawmeas\ Cos[sensorpitch]\ sensoroffset[
                        1]\ vehicleorientationuncertainty[1] + 
                    2\ corruptrawmeas\ Cos[
                        2\ corruptvehiclepitch + sensorpitch]\ sensoroffset[
                        1]\ vehicleorientationuncertainty[1] + 
                    sensoroffset[1]\^2\ vehicleorientationuncertainty[1] + 
                    Cos[2\ corruptvehiclepitch]\ sensoroffset[1]\^2\ \
vehicleorientationuncertainty[1] + 
                    sensoroffset[3]\^2\ vehicleorientationuncertainty[1] - 
                    Cos[2\ corruptvehiclepitch]\ sensoroffset[3]\^2\ \
vehicleorientationuncertainty[1] + 
                    2\ sensoroffset[1]\ sensoroffset[3]\ Sin[
                        2\ corruptvehiclepitch]\ \
vehicleorientationuncertainty[1] - 
                    2\ corruptrawmeas\ sensoroffset[3]\ Sin[
                        sensorpitch]\ vehicleorientationuncertainty[1] + 
                    2\ corruptrawmeas\ sensoroffset[3]\ Sin[
                        2\ corruptvehiclepitch + 
                          sensorpitch]\ vehicleorientationuncertainty[1] + 
                    Cos[2\ \((corruptvehiclepitch + 
                              sensorpitch)\)]\ \((\(-sensorrangeuncertainty\) \
+ corruptrawmeas\^2\ vehicleorientationuncertainty[1])\) + 
                    2\ vehiclepositionuncertainty[3])\)\)}
            }], "\[NoBreak]", ")"}],
      Function[ BoxForm`e$, 
        MatrixForm[ BoxForm`e$]]]], "Output"]
}, Open  ]],

Cell[BoxData[
    \(\(\(finalResult[\([1]\)]\)[\([1]\)] // InputForm;\)\)], "Input"],

Cell[BoxData[
    \(\(\(finalResult[\([1]\)]\)[\([3]\)] // InputForm;\)\)], "Input"],

Cell[BoxData[
    \(\(\(finalResult[\([3]\)]\)[\([1]\)] // InputForm;\)\)], "Input"],

Cell[BoxData[
    \(\(\(finalResult[\([3]\)]\)[\([3]\)] // InputForm;\)\)], "Input"]
},
FrontEndVersion->"5.0 for Microsoft Windows",
ScreenRectangle->{{0, 1024}, {0, 699}},
WindowSize->{1020, 672},
WindowMargins->{{Automatic, 0}, {Automatic, 0}}
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{
Cell[1754, 51, 129, 2, 30, "Input"],
Cell[1886, 55, 519, 8, 110, "Input"],
Cell[2408, 65, 230, 3, 50, "Input"],
Cell[2641, 70, 1350, 28, 210, "Input"],
Cell[3994, 100, 324, 5, 50, "Input"],

Cell[CellGroupData[{
Cell[4343, 109, 2102, 36, 251, "Input"],
Cell[6448, 147, 5046, 84, 89, "Output"]
}, Open  ]],
Cell[11509, 234, 84, 1, 30, "Input"],
Cell[11596, 237, 84, 1, 30, "Input"],
Cell[11683, 240, 84, 1, 30, "Input"],
Cell[11770, 243, 84, 1, 30, "Input"]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

