//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// CR_Tracker
//----------------------------------------------------------------------------

#ifndef CR_TRACKER_DECS

//----------------------------------------------------------------------------

#define CR_TRACKER_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glut.h>

#include "opencv_utils.hh"

// my stuff 

#include "CR_ParticleFilter.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))

//-------------------------------------------------
// structure
//-------------------------------------------------


//-------------------------------------------------
// functions
//-------------------------------------------------

void draw_tracker(int, int);
void update_tracker();
void write_tracker(FILE *, int);
void get_tracker_position(float *, float *);
void reset_tracker();
void initialize_tracker();
void pf_samp_prior(CR_Vector *);
void pf_samp_state(CR_Vector *, CR_Vector *);
void pf_dyn_state(CR_Vector *, CR_Vector *);
double pf_condprob_zx(void *, CR_Vector *);
double myround(double);
void process_tracker_command_line_flags(int, char **);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
