//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_OnOff.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

float KL_threshold;
int KL_time_window;
float KL_majority_fraction;
float *KL_histogram;
float *KL_history;
float KL_uniform_prob;
int KL_num_vote_levels;

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// MVIV threshold = 0.2 for 160 x 120 images, window = 300, 2/3 majority must be on-road

void initialize_onoff_confidence(float threshold, int time_window, float majority_fraction)
{
  // set globals to remember parameters

  KL_threshold = threshold;
  KL_time_window = time_window;
  KL_majority_fraction = majority_fraction;

  // 256 gray levels in vote function

  KL_num_vote_levels = 256;
  KL_uniform_prob = 1.0 / (float) KL_num_vote_levels;
  KL_histogram = (float *) calloc(KL_num_vote_levels, sizeof(float));
  
  // initialize array to store last time_window KL values

  // < fill this in > 
}

//----------------------------------------------------------------------------

float compute_onoff_confidence(IplImage *im)
{
  float KL;
  int j;

  KL = KL_from_uniform(im);

  // threshold and filter based on past history 

  // < fill this in from preprocess_onoff template, but in the meantime... >

  //  return KL;

  if (KL >= KL_threshold)
    return 1.0;
  else
    return 0.0;
}

//----------------------------------------------------------------------------

// Kullback-Leibler divergence of vote function from uniform distribution

float KL_from_uniform(IplImage *im)
{
  int i, j;
  float total, KL, prob;

  // compute probability of each vote total in this image

  for (i = 0; i < KL_num_vote_levels; i++)
    KL_histogram[i] = 0;

  for (j = 0, total = 0.0; j < im->height; j++)
    for (i = 0; i < im->width; i++) {
      KL_histogram[(int) ((float) (KL_num_vote_levels - 1) * FLOAT_IMXY(im, i, j))]++;
      total++;
    }

  // compute divergence from uniform distribution

  for (i = 0, KL = 0; i < KL_num_vote_levels; i++) {
    prob = KL_histogram[i] / total;  
    if (prob) 
      KL += KL_uniform_prob * (log(prob / KL_uniform_prob));
  }

  return -KL;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
