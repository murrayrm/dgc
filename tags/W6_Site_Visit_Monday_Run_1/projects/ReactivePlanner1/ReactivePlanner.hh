#ifndef REACTIVEPLANNER
#define REACTIVEPLANNER

#include <AliceConstants.h>
#include "PathEvaluation.hh"
double ALPHA = 1;

//Evaluates the cost of each point using the cost function in PathEvaluation, 
//and, if that cost is too high, finds the nearest acceptable point to the North or South,
//then uses StretchPath to make the path pass through that point instead. 
template <class T>
bool CheckPath(CTraj path, int numpoints, double deltadist, CMap &costmap, int layernum, T maxcost);

//Changes path so that the start_index-th point passes through newnorthing and asymptotically approaches the original path to the East and West of it.
//It also adjusts the speeds so that it traverses the trajectory in the same time.
template <class T>
void StretchPath(CTraj path, double newnorthing[], int start_index);

#endif


