#include"PathEvaluation.hh"

template <class T>
T SumCosts(CTraj path, int numpoints, double deltadist, CMap &costmap, double vehLength, double vehWidth, int layernum)
{
  int yaw;
  yaw = atan(path.getNorthingDiff(0,1)/path.getEastingDiff(0,1));
  T cost = GetAreaCost<T>(path.getNorthing(0), path.getEasting(0), yaw, costmap, vehLength, vehWidth);
  for(int i=1; i<numpoints; i++)
    {
      yaw = atan(path.getNorthingDiff(i,1)/path.getEastingDiff(i,1));
      cost += GetAreaCost<T>(path.getNorthing(i), path.getEasting(i), yaw, costmap, vehLength, vehWidth, layernum); 
    }
  return cost;
}

template < class T>
T GetAreaCost(double Northing, double Easting, double Yaw, CMap &costmap, double vehLength, double vehWidth, int layernum)
{
  double deltaside = 0.2; //min(velmap.getRowRes(), velmap.getColRes());
  double hereN, hereE;
  T costhere;
  T maxcost;
  VehFrameToNorthEast(hereN, hereE, -vehWidth/2, 0, Northing, Easting, Yaw);
  costhere = costmap.getDataUTM<T>(layernum, hereN, hereE);
  maxcost = costhere;
  for(double x = deltaside; x <= vehLength; x += deltaside)
    {
      for(double y = deltaside-vehWidth/2; y <= vehWidth/2; y += deltaside)
	{
	  VehFrameToNorthEast(hereN, hereE, y, x, Northing, Easting, Yaw); 
	  costhere = costmap.getDataUTM<T>(layernum, hereN, hereE);
	  if(costhere > maxcost)
	    maxcost = costhere;
	}
    } 
  return maxcost;
}

void VehFrameToNorthEast(double &hereN, double &hereE, double lengthwise, double widthwise, double originN, double originE, double Yaw)
{
  double angle = atan(widthwise/lengthwise)+Yaw;
  hereN = originN+sqrt(lengthwise*lengthwise+widthwise*widthwise)*cos(angle);
  hereE = originE+sqrt(lengthwise*lengthwise+widthwise*widthwise)*sin(angle);
}

