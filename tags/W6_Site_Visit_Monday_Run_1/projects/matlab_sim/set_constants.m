% Setting constants for bicycle model
dt = .005;

global L_r L_f I_VEH MASS A_PHI phi_max phi_min

% steering limits
phi_max = .38;
phi_min = -.38;
A_PHI = 1;

% set state saturations
state_u = [inf; inf; inf; inf; inf; inf; phi_max];
state_l = [-inf; -inf; -inf; -inf; -inf; -inf; phi_min];

% Lever arms
L_r = 1.36;
L_f = 1.50;
L = L_r + L_f;

% mass, inertia & drag
I_VEH = 4951;
MASS = 2200;

% initial conditions
x0 = 0;
y0 = 0;
th0= 0;
thd0 =0;
V_lat0 =0;
V_lon0 =0;
phi0 = 0;

state0 = [thd0; V_lat0; V_lon0; x0; y0; th0; phi0];
