/* FakeState.hh
 *
 * JML 31 Dec 04
 *
 */

#ifndef FAKESTATE_HH
#define FAKESTATE_HH

#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <fstream>

#include "VehicleState.hh"
//#include "FakeStateUpdateThread.cc"
#include "Alice_Const.hh"
#include "sn_msg.hh"
#include "sn_types.h"
#include "DGCutils"

class FakeState
{
    skynet m_skynet;

    int lastIndex;

    // These structs used for messaging purposes
    struct VehicleState vehiclestate;

    // Corresponding mutexes:
    pthread_mutex_t m_VehicleStateMutex;

    // Other mutexes:
    pthread_mutex_t m_UpdateMutex;

    unsigned long long starttime;

    //perpetual UpdateState variables?
    double timediff;

    int broadcast_statesock;

public:
    FakeState(int, int, int, int, int, int);

    void UpdateState();
    void printVehicleState();

    //These all need to get started in Main:
    void VehicleStateMsg_thread();
    void Update_thread();

    void Broadcast();
};

// This is now defined in VehicleState.hh
/* New Clean struct for Vehicle State Messages */

#endif
