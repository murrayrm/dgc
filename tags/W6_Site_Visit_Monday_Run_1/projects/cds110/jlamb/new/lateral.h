
//#ifndef LATERAL_H
//#define LATERAL_H

#include <iostream.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <math.h>
//#include "waypoints.h"

#define NORTHING 0
#define EASTING 1
#define MAX_LATERAL_ERROR 10
#define LATERAL_PROPORTIONAL_GAIN .05
#define HEADING_PROPORTIONAL_GAIN 0
#define LATERAL_INTEGRATOR_GAIN 0
#define HEADING_INTEGRATOR_GAIN 0
#define LATERAL_DERIVATIVE_GAIN 0
#define HEADING_DERIVATIVE_GAIN 0

#define LATERAL_INTEGRATOR_SAT 10
#define HEADING_INTEGRATOR_SAT 10

#define MAX_STEER 1

static double waypoints_lateralq[100][2] = {{3778548, 403819},
					    {3778523, 403849},
					    {3778498, 403892},
					    {3778459, 403995},
					    {3778414, 403996}};
double distPoint_to_Line(double, double, double, double, double, double);
double heading_error(double);
double lateral_controller(double, double, double);
//#endif
