#ifndef STATEFILTER_HH
#define STATEFILTER_HH

#include <math.h>

#include "gps.h"
#include "LatLong.h"
#include "VState.hh"


// CONSTANT DECLARATIONS
// State Estimator internal constant declarations
/* If velocities are less than these values, consider them to be 0. */
#define VEL_N_VALID_CUTOFF 	0.1
#define VEL_E_VALID_CUTOFF 	0.1
#define VEL_U_VALID_CUTOFF 	0.1

/* When the speed of the car is higher than this, then don't use magnetometer */
#define MAGNET_VALID_CUTOFF	1  // in m/s
#define MAG_OFFSET 30 // in degrees
const double mag_pitch_offset = -1.5; // degrees
const double mag_roll_offset = 5; // degrees 

// state validity declarations
const double EQUATOR_N = 50; // if we're within 50m of the equator wait
const double STATE_SPEED_KILL = 50; // above 50 m/s kill
const int STATE_KILL = -1;
const int STATE_WAIT = 0;
const int STATE_VALID = 1;

// obd2 related constant decs
#define OBD_CUTOFF .25 // m/s
#define OBD_VEL_ERR .05 // m/s
#define GPS_VEL_ERR 1 // m/s

// definitions for running without IMU
#define GPS_VEL_CUTOFF .5 // m/s
//const double GPS_SPEED_ZERO = .75;

// constant definitions for jump detection
const double ANGLE_CUTOFF = M_PI/6;
const double ANGLE_TOO_LARGE = M_PI/2;

//const double DV_WEIGHT = 1.5;

// constant declarations for gps error state machine
const int GPS_NORMAL = 0;
const int GPS_JUMPED = 1;

const double ERROR_WEIGHT = 2; // multiply jump by this ratio to get error
const double GPS_DEFAULT_ERROR = 2; // gps position error in meters
const double ERROR_FALLOFF = .75; // multiply error by this amount each loop
const double EARTH_RADIUS = 6378 * 1000; // earth radius in meters

// constant declarations for gps nav state machine
const int GPS_NAV_VALID = 0;
const int GPS_NAV_INVALID = 1;
const int EXT_NAV_INVALID = 2;
const int INVALID_THRESHOLD = 30; // 1 minute before going into extinv mode 
const int JUMP_RESET_THRESHOLD =  5; // 10 seconds to reset jump finder
const int UPDATE_THRESH = 120; // 2 minutes before artificialy updating

// definitions for canceling out pitch/roll of imu mounting
const double IMU_PITCH_OFFSET = .08; // radians
const double IMU_ROLL_OFFSET = 0; // rad
// note these offsets are backwards in sign from the usual convention
// because of the way the transformation is done
const double IMU_X_OFFSET = -1.68;
const double IMU_Y_OFFSET = -.356;
const double IMU_Z_OFFSET = 1.958;


// pitch/roll time constants
const double p_harsh_ratio = .996;
const double r_harsh_ratio = .996;
/*
const int NUM_ELEMENTS = 5;
#define  SigmaMult 4
*/

// detects a jump by looking at the heading, and comparing it to angle
// of displacement, if angle is off by more than 45 degs from the heading
// register a jump
double detect_jump(double cur_n, double cur_e, double update_time, 
	       VState_GetStateMsg vehstate);

// resets the jump detector when gps is lost
void detect_reset();

// 1-d kalman filter that calculates magnetometer error
void magfilter(double magheading, double gpsheading);

// bounds yaw by -pi to pi
double norm_yaw(double yaw);

// sets gps error based on gps state
double gps_err_reset(double gps_err, double gps_jump, double gps_pdop);

// angular rate harshness detection
double p_harsh(double prate);
double r_harsh(double rrate);

int state_validity(VState_GetStateMsg curstate);

/*
// calculates running std. dev of gps 
int dev_reject(double northing, double easting);

// helper funcs for running std. dev
double get_avg();
double get_var();
void filter_reset();
*/


#endif
