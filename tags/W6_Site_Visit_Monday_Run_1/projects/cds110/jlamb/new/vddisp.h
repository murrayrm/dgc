/*
 * vddisp.h - vdrive display screen for Tahoe
 *
 * RMM, 30 Dec 03
 *
 */

/* Local function declarations */
int user_quit(long);
int user_cap_toggle(long);
int brake_calibrate_cb(long);

extern DD_IDENT parmtbl[];	/* Parameter table */
extern int steer_cmdangle;	/* Commanded steering angle (steer.c) */
extern double brk_current_abs_pos_buf, brk_current_acc_buf, brk_current_vel_buf;
extern double brk_current_brake_pot, brk_pot_voltage;
extern double brake_min_pot, brake_max_pot;
extern int brk_case;
extern int brk_count;

extern int steer_state, steer_write_count;

extern bool cruise_value_good; //is the time data valid?
extern double cruise_act_vel; //actual speed
extern double cruise_cmd_vel; //commanded speed
extern double cruise_cmd_out; //what cruise decides to do
extern double vel_error_p; //proportional error
extern double vel_error_i; //integral error
extern double vel_error_d; //derivative error
extern double G_vel_p; //proportional gain
extern double G_vel_i; //integral gain
extern double G_vel_i_pos;
extern double G_vel_i_neg;
extern double G_vel_d; //derivativ gain
extern double time_interval; //
extern double timeouts;

/* Object ID's (offset into table) */
#define QUITB	141

/* Allocate space for buffer storage */
char vddbuf[604];

DD_IDENT vddisp[] = {
{1, 1, (void *)("VDRIVE 2.1c"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 17, (void *)("J:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 27, (void *)("S:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 37, (void *)("A:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 47, (void *)("B:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 57, (void *)("M:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 1, (void *)("D="), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 7, (void *)("S="), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 17, (void *)("T:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 27, (void *)("S:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 37, (void *)("X:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 46, (void *)("VM:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 56, (void *)("VS:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{4, 10, (void *)("Off    Ref   Act   Gain  Cmd"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{4, 47, (void *)("| Joy:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 1, (void *)("Drive"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 47, (void *)("| In Charge:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{6, 1, (void *)("Steer"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{6, 47, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{7, 44, (void *)("|CR OK:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{7, 56, (void *)("CMD:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{7, 66, (void *)("SPD:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 1, (void *)("P:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 8, (void *)("I:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 15, (void *)("D:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 22, (void *)("Ipos:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 32, (void *)("Ineg:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 44, (void *)("|TI:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 53, (void *)("timeouts:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 67, (void *)("OUT:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 44, (void *)("|E: P:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 56, (void *)("I:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 64, (void *)("D:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{10, 1, (void *)("Axis   IMU"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{10, 17, (void *)("GPS"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{10, 26, (void *)("|  KF     Vel     |"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{11, 2, (void *)("X/N"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{11, 26, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{11, 44, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{12, 2, (void *)("Y/E"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{12, 26, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{12, 44, (void *)("| Dump file"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 2, (void *)("Z/U"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 26, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 44, (void *)("| Brake: r="), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 62, (void *)("y="), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 71, (void *)("s="), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{14, 2, (void *)("R"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{14, 17, (void *)("-------  |"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{14, 44, (void *)("|   min="), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{14, 58, (void *)("max="), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{15, 2, (void *)("P"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{15, 17, (void *)("M"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{15, 26, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{15, 44, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{16, 2, (void *)("Y"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{16, 26, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{16, 44, (void *)("| Trans:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{17, 44, (void *)("| Steer: s="), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{17, 62, (void *)("w="), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{18, 1, (void *)("MTA: vel="), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{18, 16, (void *)("str="), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{19, 1, (void *)("Drivers"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{19, 13, (void *)("Flag    Cmd     Cal    Zero"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{19, 42, (void *)("Functions   Flag   Stop"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{20, 3, (void *)("Brake"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{20, 44, (void *)("Cruise"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{21, 3, (void *)("Throttle"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{21, 42, (void *)("Modes"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{22, 3, (void *)("Steer"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{22, 44, (void *)("O = off     M = manual (human)"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{23, 44, (void *)("R = remote  A = autonomous (MTA)"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 20, (void *)(&(joy_count)), dd_short, "%5d", vddbuf+0, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "joy_count", -1},
{1, 30, (void *)(&(steer_count)), dd_short, "%5d", vddbuf+8, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "steer_count", -1},
{1, 40, (void *)(&(acc_count)), dd_short, "%5d", vddbuf+16, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "acc_count", -1},
{1, 60, (void *)(&(mta_counter)), dd_short, "%5d", vddbuf+24, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "mta_counter", -1},
{1, 50, (void *)(&(brk_count)), dd_short, "%5d", vddbuf+32, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "brk_count", -1},
{2, 60, (void *)(&(vstate_count)), dd_short, "%5d", vddbuf+40, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vstate_count", -1},
{2, 50, (void *)(&(vmanage_count)), dd_short, "%5d", vddbuf+48, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vmanage_count", -1},
{5, 9, (void *)(&(accel_off)), dd_double, "%5.2f", vddbuf+56, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "accel_off", -1},
{6, 9, (void *)(&(steer_off)), dd_double, "%5.2f", vddbuf+64, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "steer_off", -1},
{5, 16, (void *)(&(speed_ref)), dd_double, "%5.2f", vddbuf+72, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "speed_ref", -1},
{5, 22, (void *)(&(vehstate.Speed)), dd_double, "%4.2f", vddbuf+80, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.Speed", -1},
{7, 51, (void *)(&(cruise_value_good)), dd_short, "%d", vddbuf+88, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "cruise_value_good", -1},
{7, 70, (void *)(&(cruise_act_vel)), dd_double, "%4.2f", vddbuf+96, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "cruise_act_vel", -1},
{7, 60, (void *)(&(cruise_cmd_vel)), dd_double, "%4.2f", vddbuf+104, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "cruise_cmd_vel", -1},
{9, 50, (void *)(&(vel_error_p)), dd_double, "%4.2f", vddbuf+112, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vel_error_p", -1},
{9, 58, (void *)(&(vel_error_i)), dd_double, "%4.2f", vddbuf+120, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vel_error_i", -1},
{9, 66, (void *)(&(vel_error_d)), dd_double, "%4.2f", vddbuf+128, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vel_error_d", -1},
{8, 3, (void *)(&(G_vel_p)), dd_double, "%4.2f", vddbuf+136, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "G_vel_p", -1},
{8, 10, (void *)(&(G_vel_i)), dd_double, "%4.2f", vddbuf+144, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "G_vel_i", -1},
{8, 27, (void *)(&(G_vel_i_pos)), dd_double, "%4.2f", vddbuf+152, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "G_vel_i_pos", -1},
{8, 37, (void *)(&(G_vel_i_neg)), dd_double, "%4.2f", vddbuf+160, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "G_vel_i_neg", -1},
{8, 17, (void *)(&(G_vel_d)), dd_double, "%4.2f", vddbuf+168, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "G_vel_d", -1},
{8, 48, (void *)(&(time_interval)), dd_double, "%5.4f", vddbuf+176, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "time_interval", -1},
{8, 71, (void *)(&(cruise_cmd_out)), dd_double, "%4.3f", vddbuf+184, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "cruise_cmd_out", -1},
{8, 62, (void *)(&(timeouts)), dd_short, "%d", vddbuf+192, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "timeouts", -1},
{20, 61, (void *)(&(cruise_stop)), dd_double, "%5.2f", vddbuf+200, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "cruise_stop", -1},
{5, 28, (void *)(&(cruise_gain)), dd_double, "%5.2f", vddbuf+208, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "cruise_gain", -1},
{5, 41, (void *)(&(brk_pos)), dd_double, "%5.2f", vddbuf+216, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "brk_pos", -1},
{5, 35, (void *)(&(thr_pos)), dd_double, "%5.2f", vddbuf+224, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "thr_pos", -1},
{6, 35, (void *)(&(str_angle)), dd_double, "%5.2f", vddbuf+232, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "str_angle", -1},
{4, 55, (void *)(&(joy.data[0])), dd_double, "%5.0f", vddbuf+240, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "joy.data[0]", -1},
{4, 60, (void *)(&(joy.data[1])), dd_double, "%5.0f", vddbuf+248, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "joy.data[1]", -1},
{4, 67, (void *)(&(joy.buttons)), dd_short, "%d", vddbuf+256, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "joy.buttons", -1},
{18, 10, (void *)(&(mtacmd.velocity_cmd)), dd_double, "%4.2f", vddbuf+264, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "mtacmd.velocity_cmd", -1},
{18, 20, (void *)(&(mtacmd.steer_cmd)), dd_double, "4.2f", vddbuf+272, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "mtacmd.steer_cmd", -1},
{11, 17, (void *)(&(vehstate.gpsdata.lat)), dd_double, "%7.4f", vddbuf+280, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.gpsdata.lat", -1},
{12, 17, (void *)(&(vehstate.gpsdata.lng)), dd_double, "%7.4f", vddbuf+288, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.gpsdata.lng", -1},
{13, 17, (void *)(&(vehstate.gpsdata.altitude)), dd_double, "%7.4f", vddbuf+296, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.gpsdata.altitude", -1},
{11, 8, (void *)(&(vehstate.imudata.dvx)), dd_double, "%7.4f", vddbuf+304, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.imudata.dvx", -1},
{12, 8, (void *)(&(vehstate.imudata.dvy)), dd_double, "%7.4f", vddbuf+312, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.imudata.dvy", -1},
{13, 8, (void *)(&(vehstate.imudata.dvz)), dd_double, "%7.4f", vddbuf+320, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.imudata.dvz", -1},
{14, 8, (void *)(&(vehstate.imudata.dtx)), dd_double, "%7.4f", vddbuf+328, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.imudata.dtx", -1},
{15, 8, (void *)(&(vehstate.imudata.dty)), dd_double, "%7.4f", vddbuf+336, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.imudata.dty", -1},
{16, 8, (void *)(&(vehstate.imudata.dtz)), dd_double, "%7.4f", vddbuf+344, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.imudata.dtz", -1},
{11, 27, (void *)(&(vehstate.kf_lat)), dd_double, "%7.2f", vddbuf+352, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.kf_lat", -1},
{12, 27, (void *)(&(vehstate.kf_lng)), dd_double, "%7.2f", vddbuf+360, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.kf_lng", -1},
{13, 27, (void *)(&(vehstate.Altitude)), dd_float, "%7.2f", vddbuf+368, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.Altitude", -1},
{11, 36, (void *)(&(vehstate.Vel_N)), dd_float, "%5.2f", vddbuf+376, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.Vel_N", -1},
{12, 36, (void *)(&(vehstate.Vel_E)), dd_float, "%5.2f", vddbuf+384, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.Vel_E", -1},
{13, 36, (void *)(&(vehstate.Vel_U)), dd_float, "%5.2f", vddbuf+392, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.Vel_U", -1},
{14, 27, (void *)(&(vehstate.Roll)), dd_float, "%7.2f", vddbuf+400, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.Roll", -1},
{15, 27, (void *)(&(vehstate.Pitch)), dd_float, "%7.2f", vddbuf+408, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.Pitch", -1},
{16, 27, (void *)(&(vehstate.Yaw)), dd_float, "%7.2f", vddbuf+416, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.Yaw", -1},
{15, 19, (void *)(&(vehstate.magreading.heading)), dd_float, "%5.2f", vddbuf+424, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.magreading.heading", -1},
{15, 46, (void *)("ESTOP:"), dd_label, "NULL", (char *) NULL, 1, user_estop_toggle, (long)(long) 0, 0, 0, Button, "", -1},
{15, 53, (void *)(&(estop_call_flag)), dd_short, "%d", vddbuf+432, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "estop_call_flag", -1},
{20, 13, (void *)(&(brake_flag)), dd_short, "%1d", vddbuf+440, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "brake_flag", -1},
{20, 21, (void *)(&(brk_pos)), dd_double, "%5.2f", vddbuf+448, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "brk_pos", -1},
{20, 29, (void *)("CAL"), dd_label, "NULL", (char *) NULL, 1, brake_calibrate_cb, (long)(long) 0, 0, 0, Button, "", -1},
{20, 36, (void *)("RST"), dd_label, "NULL", (char *) NULL, 1, brake_reset_cbe, (long)(long) 0, 0, 0, Button, "", -1},
{21, 13, (void *)(&(throttle_flag)), dd_short, "%1d", vddbuf+456, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "throttle_flag", -1},
{21, 21, (void *)(&(thr_pos)), dd_double, "%5.2f", vddbuf+464, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "thr_pos", -1},
{20, 54, (void *)(&(cruise_flag)), dd_short, "%1d", vddbuf+472, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "cruise_flag", -1},
{22, 13, (void *)(&(steer_flag)), dd_short, "%1d", vddbuf+480, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "steer_flag", -1},
{22, 21, (void *)(&(str_angle)), dd_double, "%5.2f", vddbuf+488, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "str_angle", -1},
{22, 29, (void *)("CAL"), dd_label, "NULL", (char *) NULL, 1, steer_cal, (long)(long) 0, 0, 0, Button, "", -1},
{22, 36, (void *)("ZERO"), dd_label, "NULL", (char *) NULL, 1, steer_setzero, (long)(long) 0, 0, 0, Button, "", -1},
{12, 60, (void *)(dumpfile), dd_string, "%s", (char *)(sizeof(dumpfile)), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "dumpfile", -1},
{12, 56, (void *)(&(capflag)), dd_short, "[%d]:", vddbuf+496, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "capflag", -1},
{24, 1, (void *)("QUIT"), dd_label, "NULL", (char *) NULL, 1, user_quit, (long)(long) 0, 0, 0, Button, "", -1},
{24, 9, (void *)("PARAM"), dd_label, "NULL", (char *) NULL, 1, dd_usetbl_cb, (long)parmtbl, 0, 0, Button, "", -1},
{24, 17, (void *)("CAPTURE"), dd_label, "NULL", (char *) NULL, 1, user_cap_toggle, (long)(long) 0, 0, 0, Button, "", -1},
{2, 30, (void *)(&(steer_cmdangle)), dd_short, "%4d", vddbuf+504, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "steer_cmdangle", -1},
{2, 20, (void *)(&(brk_current_brake_pot)), dd_short, "%4.2f", vddbuf+512, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "brk_current_brake_pot", -1},
{2, 9, (void *)(&(mode_steer)), vdrive_mode, "%c", vddbuf+520, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "mode_steer", -1},
{2, 3, (void *)(&(mode_drive)), vdrive_mode, "%c", vddbuf+528, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "mode_drive", -1},
{13, 55, (void *)(&(brk_current_abs_pos_buf)), dd_double, "%5.2f", vddbuf+536, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "brk_current_abs_pos_buf", -1},
{13, 64, (void *)(&(brk_current_brake_pot)), dd_double, "%5.2f", vddbuf+544, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "brk_current_brake_pot", -1},
{13, 73, (void *)(&(brk_case)), dd_short, "%d", vddbuf+552, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "brk_case", -1},
{16, 53, (void *)(&(current_gear)), dd_short, "%d", vddbuf+560, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "current_gear", -1},
{14, 52, (void *)(&(brake_min_pot)), dd_double, "%5.2g", vddbuf+568, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "brake_min_pot", -1},
{14, 62, (void *)(&(brake_max_pot)), dd_double, "%5.2g", vddbuf+576, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "brake_max_pot", -1},
{5, 60, (void *)(&(JOYSTICK_IN_CHARGE)), dd_short, "%d", vddbuf+584, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "JOYSTICK_IN_CHARGE", -1},
{17, 55, (void *)(&(steer_state)), dd_short, "%d", vddbuf+592, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "steer_state", -1},
{17, 64, (void *)(&(steer_write_count)), dd_short, "%d", vddbuf+600, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "steer_write_count", -1},
DD_End};
