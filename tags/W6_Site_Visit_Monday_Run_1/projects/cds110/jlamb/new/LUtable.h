#ifndef LUtable_H
#define LUtable_H

#include <string>
#include <iostream>
#include <fstream>
#include <map>

#define PRINT_DEBUG_MESSAGES 0

class LUtable {
 public:
  /* Constructors */
  LUtable();
  LUtable(std::string filename);

  /* Destructors */
  ~LUtable();

  void loadTable(std::string filename);

  double getValue(double key);

 private:
  typedef std::map<double, double> TableType;

  TableType dataTable;
  double min_key;
  double max_key ;

};

#endif
