#include "LadarFinder.hh"

#include "laserdevice.h"

#include "Constants.h" // For serial ports, other constants
#include "frames/frames.hh" // For coordinate definitions.
#include <iostream.h>
#include <fstream.h>

// FIXME: Eventually these should go in an include file.
#define ROOF_LADAR_SCAN_ANGLE 100
#define ROOF_LADAR_RESOLUTION 0.5
#define ROOF_LADAR_UNITS 0.01

// global variables from main file
extern int   SIM;         // get the simulation flag from the main function
extern int   LOG_SCANS;
extern int   QUIT_PRESSED;
extern char* ROOF_TTY;
extern char* TESTNAME;

extern int    PLAYBACK;     // Whether or not to play back a logged file
extern int    STEPTHROUGH;  // Step through scans one at a time
extern FILE*  scanlog;      // Log files for various data
extern FILE*  errorlog;     // Records any errors reported by the scanner
extern FILE*  statelog;     // Synchronized state data 
extern double start_time;   // Time to start reading logs
extern double stop_time;    // Time to stop reading logs

extern int    numLogScanPoints;
extern double scanLogUnits;

int RoofReset = 0;
int RoofResetCount = 0;
int TotalScans = 0;
int TotalErrorScans = 0;

// Coordinate system stuff.
XYZcoord roofLadarOffset(ROOF_LADAR_OFFSET_X, 
                         ROOF_LADAR_OFFSET_Y, 
                         ROOF_LADAR_OFFSET_Z);
frames roofLadarFrame;

// LADAR Driver class
CLaserDevice roofLaser;

/******************************************************************************/
/******************************************************************************/
int LadarFinder::InitLadar() 
{
  int result;
  char * port_name;
  char filename[255];
  char datestamp[255];
  int status;
  unsigned char data[1024];
  bool good_to_go = true;
  
  // Open log file if LOG_SCANS is set
  if( LOG_SCANS ) 
  {
    // filename will be TESTNAME_ddmmyyyy_hhmmss.log
    time_t t = time(NULL);
    tm *local;
    local = localtime(&t);
    int i;
    
    sprintf(datestamp, "%02d%02d%04d_%02d%02d%02d",
            local->tm_mon+1, local->tm_mday, local->tm_year+1900,
            local->tm_hour, local->tm_min, local->tm_sec);
    
    sprintf(filename, "logs/%s_scans_%s.log", TESTNAME, datestamp);
    printf("Logging scans to file: %s\n", filename);
    scanlog=fopen(filename,"w");
    if ( scanlog == NULL ) 
    {
      printf("Unable to open scan log!!!\n");
      exit(-1);
    }
    // print data format information in a header at the top of the scanlog file
    fprintf(scanlog, "%% angle = %d\n", ROOF_LADAR_SCAN_ANGLE);
    fprintf(scanlog, "%% resolution = %lf\n", ROOF_LADAR_RESOLUTION);
    fprintf(scanlog, "%% units = %lf\n", ROOF_LADAR_UNITS);
    fprintf(scanlog, "%% Time[sec] | scan points\n");
    
    sprintf(filename, "logs/%s_state_%s.log", TESTNAME, datestamp);
    printf("Logging state to file: %s\n", filename);
    statelog=fopen(filename,"w");
    if ( statelog == NULL )
    {
      printf("Unable to open state log!!!\n");
      exit(-1);
    }
    // print data format information in a header at the top of the state file
    fprintf(statelog, "%% Time[sec] | Easting[m] | Northing[m] | Altitude[m]");
    fprintf(statelog, " | Vel_E[m/s] | Vel_N[m/s] | Vel_D[m/s]" );
    fprintf(statelog, " | Speed[m/s] | Accel[m/s/s]" );
    fprintf(statelog, " | Pitch[rad] | Roll[rad] | Yaw[rad]" );
    fprintf(statelog, " | PitchRate[rad/s] | RollRate[rad/s] | YawRate[rad/s]");
    fprintf(statelog, "\n");
    
    sprintf(filename, "logs/%s_errors_%s.log", TESTNAME, datestamp);
    printf("Logging errors to file: %s\n", filename);
    errorlog=fopen(filename,"w");
    if ( errorlog == NULL ) 
    {
      printf("Unable to open error log!!!\n");
      exit(-1);
    }
  } // end if( LOG_SCANS )
  else if( PLAYBACK )
  {
    double tmp; // throw away variable for time stamps
    int scan; // throw away variable for scans
    d.log_tstamp = 0; // initialize the log time stamp
    
    printf("Throwing away scans before --start time... ");
    
    // Cue up the correct location in the log files
    // throw away all of the log data before start_time
    while( d.log_tstamp < start_time ) 
    {
      // suck in the state data for a scan
      fscanf(statelog, "%lf %lf %lf %f %f %f %f %lf %f %f %f %f %f %f %f\n",
             &(d.log_tstamp), &(d.SS.Easting), &(d.SS.Northing), &(d.SS.Altitude), 
             &(d.SS.Vel_E), &(d.SS.Vel_N), &(d.SS.Vel_D), &(tmp), &(tmp), 
             &(d.SS.Pitch), &(d.SS.Roll), &(d.SS.Yaw), 
             &(d.SS.PitchRate), &(d.SS.RollRate), &(d.SS.YawRate));
      //printf("Throwing away state at time = %f sec\n", d.log_tstamp );
      
      // throw away all of the log data before start_time
      fscanf(scanlog, "%lf ",&tmp);
      // suck in a scan and save it
      for(int i = 0; i < numLogScanPoints; i++) 
      {
        fscanf(scanlog,"%d ", &scan);
      } 
      fscanf(scanlog,"\n");
      //printf("Throwing away scan, at time = %f sec\n", tmp );
      
      if( d.log_tstamp != tmp ) 
      {
        printf("LadarFinder::InitLadar: Warning: Timestamps don't match!\n");
      }
    }
    
    printf("...done throwing away scans at t = %f.\n", d.log_tstamp);
  }
  
  // initialize the coordinate frames for the LADAR sensor
  roofLadarFrame.initFrames(roofLadarOffset, 
                            ROOF_LADAR_PITCH, 
                            ROOF_LADAR_ROLL, 
                            ROOF_LADAR_YAW);
  
  // Ladar initialization tasks
  if( !SIM ) 
  {
    while( roofLaser.Setup(ROOF_TTY, ROOF_LADAR_SCAN_ANGLE, 
                           ROOF_LADAR_RESOLUTION, 0) != 0) 
    {
      sleep(1);
    }

    // Do a self-test of the roof ladar
    printf("\n\nPERFORMING ROOF LADAR SELF-TEST...IF THIS DOES NOT RETURN\n");
    printf("AFTER A COUPLE OF SECONDS, THEN IT FAILED!!!!\n");
    status = roofLaser.LockNGetData( data );
    if((status & 0xFF) != 0x10) 
    {
      printf("ROOF LADAR ERROR!  STATUS: %X\n", status);
      good_to_go = false;
    }
    if(!good_to_go) exit(-1); 
    return true;
  } 
  else 
  {
    // Do simulation stuff here...
    return true;
  }  
} // end LadarFinder::InitLadar()

/******************************************************************************/
/******************************************************************************/
void LadarFinder::LadarGetMeasurements() 
{
  int i;
  double *roofscan;
  double tstamp;
  double junk; // throw away variable
  double phi, centDist;
  int numRoofScanPoints;
  int roofStatus;
  XYZcoord npoint(0, 0, 0);
  XYZcoord scanpoint(0, 0, 0);
  XYZcoord vehPos(0, 0, 0);
  double roofOffset;
  static int messageCounter = 0;
  
  Timeval tv = TVNow();
  // Starting angle of roof scan (in the sensor frame, the angle/azimuth 
  // of the first scan -- the farthest to the left)
  roofOffset = ((180.0 - ROOF_LADAR_SCAN_ANGLE) / 2.0) * M_PI / 180.0;
  
  if( PLAYBACK ) 
  {
    numRoofScanPoints = numLogScanPoints; // get global variable
  }
  else if(SIM) 
  {
    numRoofScanPoints = 
    (int)(ROOF_LADAR_SCAN_ANGLE / ROOF_LADAR_RESOLUTION + 1);
  } 
  else 
  {
    numRoofScanPoints = roofLaser.getNumDataPoints();
  }
  
  roofscan = (double *) malloc(numRoofScanPoints * sizeof(double));
  
  /* Steps - 
    1. Take a scan from the ladar.
    2. Synchronize it with state data.
    3. Calculate mean and covariance of each measurement.
    4. Send measurements to FusionMapper
    */
  // Read in or log the vehicle state and scans
  // Important!: The PLAYBACK block format should match the LOG_SCANS block.
  if( PLAYBACK ) 
  {
    // suck in the state data for a scan
    fscanf(statelog, "%lf %lf %lf %f %f %f %f %lf %f %f %f %f %f %f %f\n",
           &(d.log_tstamp), &(d.SS.Easting), &(d.SS.Northing), &(d.SS.Altitude), 
           &(d.SS.Vel_E), &(d.SS.Vel_N), &(d.SS.Vel_D), &(junk), &(junk), 
           &(d.SS.Pitch), &(d.SS.Roll), &(d.SS.Yaw), 
           &(d.SS.PitchRate), &(d.SS.RollRate), &(d.SS.YawRate));
  }
  // Important!: The format of this block should match the one above.
  else if(LOG_SCANS) 
  {
    tstamp = d.SS.Timestamp.dbl();
    fprintf(scanlog, "%f ", tstamp);
    
    // Print state      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 
    fprintf(statelog, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",
            tstamp, d.SS.Easting, d.SS.Northing, d.SS.Altitude,
            d.SS.Vel_E, d.SS.Vel_N, d.SS.Vel_D, d.SS.Speed(), d.SS.Accel(),
            d.SS.Pitch, d.SS.Roll, d.SS.Yaw, 
            d.SS.PitchRate, d.SS.RollRate, d.SS.YawRate );
    fflush(statelog);
  }
  
  // Lets update the state right before we take a scan...
  // We'll update state whether it's from simulation or actual.
  if( !PLAYBACK ) 
  {
    UpdateState();
  }

  // Update the XYZcoord for use with the new frames class.
  vehPos.X = d.SS.Northing;
  vehPos.Y = d.SS.Easting;
  vehPos.Z = 0.0;
  //--------------------------------------------------
  //   vehPos.Z = d.SS.Altitude;
  //-------------------------------------------------- 
  
  // Step 1 - Take a scan
  if( !SIM ) 
  { 
    roofStatus = roofLaser.UpdateScan();
    RoofReset = (roofLaser.isResetting() != 0);
    if(RoofReset != 0) RoofResetCount = roofLaser.isResetting();
    TotalScans++;
    if( ((roofStatus & 0x01FF) != 0x0010) )
      TotalErrorScans++;
    if (LOG_SCANS) 
    {
      fprintf(errorlog,"%f 0x%04X\n", tstamp, roofStatus);
      fflush(errorlog);
    }
    // update local data buffer
    for(int i = 0; i < numRoofScanPoints; i++) 
    {
      if (LOG_SCANS) 
      {
        fprintf(scanlog,"%d ", roofLaser.Scan[i]);
      }
      if(roofLaser.isDataError(roofStatus,roofLaser.Scan[i]) || RoofReset) 
        roofscan[i] = -1;
      else
        roofscan[i] = (roofLaser.Scan[i] * ROOF_LADAR_UNITS);
    }
  } 
  else if( PLAYBACK ) 
  {
    int scan;
    
    // keep taking scans until the end of the log or until stop_time
    if( !feof(scanlog) && !feof(statelog) && d.log_tstamp < stop_time )
    {
      fscanf(scanlog, "%lf ",&tstamp);
      // suck in a scan and save it
      for(int i = 0; i < numLogScanPoints; i++) 
      {
        fscanf(scanlog,"%d ", &scan);
        if(scan >= 0x1FF7) 
        {
          roofscan[i] = -1.0; // Bad scan so ignore it
        }
        else roofscan[i] = (scan * scanLogUnits);
      }
      fscanf(scanlog,"\n");
    }
    else
    {
      printf("Finished reading scans at time = %f sec.\n", tstamp );
      QUIT_PRESSED = 1;
    }
  }
  // or fake a scan - TODO: should get this from Gazebo/Player!
  else 
  {
    // the distance of the center scanline to flat ground
    // assumes zero roll and zero yaw for sensor
    // and zero roll and zero pitch for vehicle...
    //printf("Fair notice - NO support for simulating 2 ladars yet!!!\n");
    centDist = ROOF_LADAR_OFFSET_Z/tan(ROOF_LADAR_PITCH);
    for (int i = 0; i < numRoofScanPoints; i++) 
    {
      phi = roofOffset + (i * ROOF_LADAR_RESOLUTION * M_PI) / 180.0;
      roofscan[i] = centDist / sin(phi);
    }
    if (LOG_SCANS) 
    {
      for (int i = 0; i < numRoofScanPoints; i++) 
      {
        fprintf(scanlog,"%d ", (int) (roofscan[i] * 100.0) );
      }
    }
  } // end getting new scan
  if (LOG_SCANS) fprintf(scanlog,"\n");
  if (LOG_SCANS) fflush(scanlog);
  
  /* ***************************************************** */
  /* ***************************************************** */
  // Step 2 - Do some pattern matching to find the flattest spot
  // in the current scan.
 
  

  
  
  /* ***************************************************** */
  /* ***************************************************** */
  // Step 2 - synchronize the scan with state data and calculate
  // the measurement coordinate in 3D space
  
  // update internal state of frames
  // TODO: Determine which elements of SS are actually needed for this 
  // operation, and use the minimal set!
  roofLadarFrame.updateState( vehPos, d.SS.Pitch, d.SS.Roll, d.SS.Yaw );
  
  /* maintain a counter of the valid measurements. */
  int numValidRoofMeasurements = numRoofScanPoints;
  /* Loop once through the scanpoints so that we can send the number of 
    measurements in the MTA message, which doesn't include the invalid 
    scans. */
  for(int i = 0; i < numRoofScanPoints; i++)
  {
    if(roofscan[i] < 0 || roofscan[i] >= ROOF_LADAR_MAX_RANGE)
    {
      // If its < 0 then we got an error from the ladar, so ignore the
      // scanpoint, and dont send it.  If its max range or
      // larger, we either got an error or we got a good scanpoint and
      // there isn't anything there.  so ignore the scanpoint, and
      // dont send the data.
      numValidRoofMeasurements--;
    }
  }
  
  if(!RoofReset) 
  {
    // calculate measurements from the new roof scan
    for(int i = 0; i < numRoofScanPoints; i++) 
    {
      phi = roofOffset + (i * ROOF_LADAR_RESOLUTION * M_PI) / 180.0;
      if(roofscan[i] < 0 || roofscan[i] >= ROOF_LADAR_MAX_RANGE) 
      {
        // If its < 0 then we got an error from the ladar, so ignore the
        // scanpoint, and dont send it.  If its max range or
        // larger, we either got an error or we got a good scanpoint and
        // there isn't anything there.  so ignore the scanpoint, and
        // dont send the data.
        continue;
      }
      
      // convert the range into coordinates in the sensors frame
      // sensor x-axis points straight ahead, y-axis to the left, and
      // z-down, thus the scanline at 90 degrees will have a 0 y-coord.
      // All of the data will have a zero z-value in the sensors frame.
      scanpoint.X = roofscan[i]*sin(phi);
      scanpoint.Y = roofscan[i]*cos(phi);
      scanpoint.Z = 0;
      
      // convert the sensor frame coords into utm coordinates
      npoint = roofLadarFrame.transformS2N(scanpoint);
      
    } // end looping through new roofscan points
    
    if( PLAYBACK ) 
    {
      if( STEPTHROUGH )
      {
        printf("Paused because --step was specified (<Enter> to continue).\n");
        char answer[100];
        fgets(answer,sizeof(answer),stdin);
      }
      //--------------------------------------------------
      //       /* DEBUG */
      //       usleep(10000);
      //-------------------------------------------------- 
    }
  }
  
  /* ***************************************************** */
  /* ***************************************************** */
  
  free(roofscan);
  
  Timeval tv_end = TVNow();
  // as of thursday this takes 420 ms!!!
  //  fprintf(stderr, "%lf seconds elapsed\n", (tv_end.dbl() - tv.dbl()));
  
} // end LadarFinder::LadarGetMeasurements()


void LadarFinder::LadarShutdown() 
{
  fclose(scanlog);
  fclose(statelog);
  roofLaser.LockNShutdown();
}


// end ladar.cc
