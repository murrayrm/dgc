#ifndef PLANNERMODULE_HH
#define PLANNERMODULE_HH

#include <pthread.h>
#include "StateClient.h"
#include "TrajTalker.h"
#include "CMapPlus.hh"
#include "rddf.hh"
#include "reactivePlanner.hh"

#define BLUR_SIZE 1.  //  m

class reactiveModule : public CStateClient, public CTrajTalker
{
  CMapPlus				 	m_map;
  int								m_mapLayer, m_blurLayer;
  RDDF							m_rddf;
	char*							m_pMapDelta;

	/*! Whether at least one delta was received */
	bool							m_bReceivedAtLeastOneDelta;
	pthread_mutex_t		m_deltaReceivedMutex;
	pthread_cond_t		m_deltaReceivedCond;

	/*! The mutex to protect the map we're planning on */
	pthread_mutex_t		m_mapMutex;

	/*! The skynet socket for sending trajectories (to mode managements) */
	int								m_trajSocket;

public:
  reactiveModule(int sn_key);
  ~reactiveModule();

  /*! Method to run in separate thread.  This function runs the dd_loop() 
   *  Sparrow method to update the screen and take keyboard commands. */
  void SparrowDisplayLoop();

  /*! Method to update the variables that are to be displayed in the Sparrow
   *  interface.  This method does not run a loop. */
  void UpdateSparrowVariablesLoop();

	/*! This is the function that continually runs the planner in a loop */
	void planningLoop(void);

	/*! this is the function that continually reads map deltas and updates the
		map */
	void getMapDeltasThread();
	
	void blurMap();
};

#endif
