# DGC Globaly Included Makefile
# Include this at the top of every makefile after setting DGC
.PHONY: redirect folders all
redirect: folders all

# Tool definitions
CC = gcc
CPP=g++
INCLUDE=-I$(INCLUDEDIR)
CFLAGS += -g -Wall $(INCLUDE)
CPPFLAGS += -pthread $(CFLAGS)
LDFLAGS += -g -pthread -L$(LIBDIR)
INSTALL = install
INSTALL_DATA = ln -f
INSTALL_PROGRAM = ln -f 
#INSTALL_DATA = $(INSTALL) -m 644
#INSTALL_PROGRAM = $(INSTALL) -m 754
MAKEDEPEND = makedepend -Y
CDD = $(BINDIR)/cdd

# Rule to make the target folders and copy constants before any rules execute
ifneq (1,${MAKINGFIRST})
MAKEFIRST := $(shell export MAKINGFIRST=1 && $(MAKE) first)
endif

# Directory constants
BINDIR = $(DGC)/bin
LIBDIR = $(DGC)/lib
LOGSDIR = $(DGC)/logs
INCLUDEDIR = $(DGC)/include
CONSTANTSDIR = $(DGC)/constants

# headers library locations
#  fake libraries, don't link with these :)
RPGSEEDERHDR        = $(LIBDIR)/rpgSeederHeaderLib.a

# header include makefiles - must have HEADERNAME_PATH and HEADERNAME_DEPEND 
# defined within
include $(DGC)/modules/rddfPathGen/rpgSeederHeader.mk

# drivers library locations
ALICELIB            = $(LIBDIR)/alice.a
IMU_FASTCOMLIB      = $(LIBDIR)/imu_fastcom.a
SDSLIB              = $(LIBDIR)/SDS.a
SERIALLIB           = $(LIBDIR)/serial.a
SICKLMS221LIB       = $(LIBDIR)/ladarlib.a
STEREOSOURCELIB     = $(LIBDIR)/stereoSource.a
LADARSOURCELIB      = $(LIBDIR)/ladarSource.a

# driver include makefiles - must have DRIVERNAME_PATH and DRIVERNAME_DEPEND 
# defined within
include $(DGC)/drivers/alice/alice.mk
include $(DGC)/drivers/gpsSDS/gps.mk
include $(DGC)/drivers/imu_fastcom/imu_fastcom.mk
include $(DGC)/drivers/SDS/SDS.mk
include $(DGC)/drivers/serial/serial.mk
include $(DGC)/drivers/sicklms221/sicklms221.mk
include $(DGC)/drivers/stereovision/stereoSource.mk
include $(DGC)/drivers/ladar/ladarSource.mk

# Util library locations
CMAPLIB             = $(LIBDIR)/CMap.a
CMAPPLIB            = $(LIBDIR)/CMapPlus.a
CCPAINTLIB          = $(LIBDIR)/CCorridorPainter.a
CCOSTPAINTLIB       = $(LIBDIR)/CCostPainter.a
CPIDLIB             = $(LIBDIR)/libpid.a
DGCUTILS            = $(LIBDIR)/DGCutils.o
FINDFORCELIB        = $(LIBDIR)/findforce.a
FLWINLIB            = $(LIBDIR)/flwin.a
FRAMESLIB           = $(LIBDIR)/frames.a
GGISLIB             = $(LIBDIR)/ggis.a
MAPDISPLAYLIB       = $(LIBDIR)/MapDisplay.a
MODULEHELPERSLIB    = $(LIBDIR)/libmodulehelpers.a 
PIDCONTROLLERLIB    = $(LIBDIR)/PID_Controller.a
PLANNERLIB          = $(LIBDIR)/planner.a
RDDFLIB             = $(LIBDIR)/rddf.a
SKYNETLIB           = $(LIBDIR)/libskynet.a
SPARROWLIB          = $(LIBDIR)/sparrow.a
STEREOPROCESSLIB    = $(LIBDIR)/stereoProcess.a
TRAJLIB             = $(LIBDIR)/traj.a
LADARCLIENTLIB	    = $(LIBDIR)/LadarClient.o

# util include makefiles - must have UTILNAME_PATH and UTILNAME_DEPEND defined 
# within
include $(DGC)/util/cMap/CMap.mk
include $(DGC)/util/corridorPainter/CCorridorPainter.mk
include $(DGC)/util/costPainter/CCostPainter.mk
include $(DGC)/util/pid/Cpid.mk
include $(DGC)/util/moduleHelpers/DGCutils.mk
include $(DGC)/util/controllerUtils/force/find_force.mk
include $(DGC)/util/flwin/flwin.mk
include $(DGC)/util/frames/frames.mk
include $(DGC)/util/latlong/latlong.mk
include $(DGC)/util/mapDisplay/MapDisplay.mk
include $(DGC)/util/moduleHelpers/ModuleHelpers.mk
include $(DGC)/util/pidController/PID_Controller.mk
include $(DGC)/util/planner/planner.mk
include $(DGC)/util/RDDF/Rddf.mk
include $(DGC)/util/skynet/skynet.mk
include $(DGC)/util/sparrow/sparrow.mk
include $(DGC)/util/stereoProcess/stereoProcess.mk
include $(DGC)/util/traj/traj.mk

# Module library locations
ADRIVELIB           = $(LIBDIR)/adrive.a
ASTATELIB           = $(LIBDIR)/astate.a
FUSIONMAPPERLIB     = $(LIBDIR)/fusionMapper.a
SKYNETGUILIB        = $(LIBDIR)/gui.a
LADARFEEDERBIN      = $(BINDIR)/ladarFeeder
MODEMANMODULELIB    = $(LIBDIR)/modeManModule.a
MODULESTARTERLIB    = $(LIBDIR)/moduleStarter.a
PLANNERMODULELIB    = $(LIBDIR)/plannerModule.a
RDDFPATHGENLIB      = $(LIBDIR)/rddfPathGen.a
SIMULATORLIB        = $(LIBDIR)/simulator.a
STEREOFEEDERBIN     = $(BINDIR)/stereoFeeder
TRAJFOLLOWERLIB     = $(LIBDIR)/trajFollower.a

# module include makefiles - must have MODULENAME_PATH and MODULENAME_DEPEND 
# defined within
include $(DGC)/modules/adrive/adrive.mk
include $(DGC)/modules/astate/astate.mk
include $(DGC)/modules/fusionMapper/fusionMapper.mk
include $(DGC)/modules/gui/gui.mk
include $(DGC)/modules/ladarFeeder/ladarFeeder.mk
include $(DGC)/modules/modeManModule/modeManModule.mk
include $(DGC)/modules/moduleStarter/moduleStarter.mk
include $(DGC)/modules/plannerModule/plannerModule.mk #Must come after planner
include $(DGC)/modules/rddfPathGen/rddfPathGen.mk
include $(DGC)/modules/simulator/simulator.mk # This needs to come after adrive.
include $(DGC)/modules/stereoFeeder/stereoFeeder.mk
include $(DGC)/modules/trajFollower/trajFollower.mk
include $(DGC)/util/gazebo/clients/LadarClient/ladarClient.mk

# other library locations
CONSTANTSLIB        = $(LIBDIR)/constants
LOGGERLIB           = $(LIBDIR)/logger.a
REACTIVEPLANNERLIB  = $(LIBDIR)/reactivePlanner.a

# other include makefiles
include $(DGC)/constants/constants.mk
include $(DGC)/projects/logger/Logger.mk
include $(DGC)/projects/reactivePlanner/reactivePlanner.mk


# DGC Global Makefile Rules
first: folders $(CONSTANTSLIB)

folders: $(INCLUDEDIR) $(LIBDIR) $(BINDIR) $(LOGSDIR)

$(CONSTANTSLIB): $(CONSTANTS_DEPEND)
	cd $(CONSTANTS_PATH) && $(MAKE) all && $(MAKE) install

$(LADARFEEDERBIN): $(LADARFEEDER_DEPEND)
	cd $(LADARFEEDER_PATH) && $(MAKE) && $(MAKE) install

$(SICKLMS221LIB): $(SICKLMS221_DEPEND)
	cd $(SICKLMS221_PATH) && $(MAKE) && $(MAKE) install

$(STEREOPROCESSLIB): $(STEREOPROCESS_DEPEND)
	cd $(STEREOPROCESS_PATH) && $(MAKE) && $(MAKE) install

$(STEREOSOURCELIB): $(STEREOSOURCE_DEPEND)
	cd $(STEREOSOURCE_PATH) && $(MAKE) && $(MAKE) install

$(STEREOFEEDERBIN): $(STEREOFEEDER_DEPEND)
	cd $(STEREOFEEDER_PATH) && $(MAKE) && $(MAKE) install

$(GGISLIB): $(LATLONG_DEPEND)
	cd $(LATLONG_PATH) && $(MAKE) && $(MAKE) install

$(FLWINLIB): $(FLWIN_DEPEND)
	cd $(FLWIN_PATH) && $(MAKE) && $(MAKE) install

$(ADRIVELIB): $(ADRIVE_DEPEND)
	cd $(ADRIVE_PATH) && $(MAKE) && $(MAKE) install

$(ASTATELIB): $(ASTATE_DEPEND)
	cd $(ASTATE_PATH) && $(MAKE) && $(MAKE) install

$(IMU_FASTCOMLIB): $(IMU_FASTCOM_DEPEND)
	cd $(IMU_FASTCOM_PATH) && $(MAKE) && $(MAKE) install

$(PLANNERMODULELIB): $(PLANNERMODULE_DEPEND)
	cd $(PLANNERMODULE_PATH) && $(MAKE) && $(MAKE) install

$(PLANNERLIB): $(PLANNER_DEPEND)
	cd $(PLANNER_PATH) && $(MAKE) && $(MAKE) install

$(MODEMANMODULELIB): $(MODEMANMODULE_DEPEND)
	cd $(MODEMANMODULE_PATH) && $(MAKE) && $(MAKE) install

$(SIMULATORLIB): $(SIMULATOR_DEPEND)
	cd $(SIMULATOR_PATH) && $(MAKE) && $(MAKE) install

$(FRAMESLIB): $(FRAMES_DEPEND)
	cd $(FRAMES_PATH) && $(MAKE) && $(MAKE) install

$(SKYNETGUILIB): $(SKYNETGUI_DEPEND)
	cd $(SKYNETGUI_PATH) && $(MAKE) && $(MAKE) install

$(MODULESTARTERLIB): $(MODULESTARTER_DEPEND)
	cd $(MODULESTARTER_PATH) && $(MAKE) && $(MAKE) install

$(MAPDISPLAYLIB): $(MAPDISPLAY_DEPEND)
	cd $(MAPDISPLAY_PATH) && $(MAKE) && $(MAKE) install

$(FUSIONMAPPERLIB): $(FUSIONMAPPER_DEPEND)
	cd $(FUSIONMAPPER_PATH) && $(MAKE) && $(MAKE) install

$(TRAJFOLLOWERLIB): $(TRAJFOLLOWER_DEPEND)
	cd $(TRAJFOLLOWER_PATH) && $(MAKE) && $(MAKE) install

$(CPIDLIB): $(CPID_DEPEND)
	cd $(CPID_PATH) && $(MAKE) && $(MAKE) install

$(PIDCONTROLLERLIB): $(PIDCONTROLLER_DEPEND)
	cd $(PIDCONTROLLER_PATH) && $(MAKE) && $(MAKE) install

$(FINDFORCELIB): $(FINDFORCE_DEPEND)
	cd $(FINDFORCE_PATH) && $(MAKE) && $(MAKE) install

$(CMAPLIB): $(CMAP_DEPEND)
	cd $(CMAP_PATH) && $(MAKE) && $(MAKE) install

$(CMAPPLIB): $(CMAP_DEPEND)
	cd $(CMAP_PATH) && $(MAKE) && $(MAKE) install

$(RDDFLIB): $(RDDF_DEPEND)
	cd $(RDDF_PATH) && $(MAKE) && $(MAKE) install

$(CCPAINTLIB): $(CCORRIDORPAINTER_DEPEND)
	cd $(CCORRIDORPAINTER_PATH) && $(MAKE) && $(MAKE) install

$(CCOSTPAINTLIB): $(CCOSTPAINTER_DEPEND)
	cd $(CCOSTPAINTER_PATH) && $(MAKE) && $(MAKE) install

$(SERIALLIB): $(SERIAL_DEPEND)
	cd $(SERIAL_PATH) && $(MAKE) && $(MAKE) install

$(SDSLIB): $(SDS_DEPEND)
	cd $(SDS_PATH) && $(MAKE) && $(MAKE) install

$(MODULEHELPERSLIB): $(MODULEHELPERS_DEPEND) $(DGCUTILS_DEPEND)
	cd $(MODULEHELPERS_PATH) && $(MAKE) && $(MAKE) install

$(DGCUTILS): $(DGCUTILS_DEPEND) $(MODULEHELPERS_DEPEND)
	cd $(DGCUTILS_PATH) && $(MAKE) install

$(SKYNETLIB): $(SKYNET_DEPEND)
	cd $(SKYNET_PATH) && $(MAKE) install

$(TRAJLIB): $(TRAJ_DEPEND)
	cd  $(TRAJ_PATH) && $(MAKE) && $(MAKE) install

$(RDDFPATHGENLIB): $(RDDFPATHGEN_DEPEND)
	cd $(RDDFPATHGEN_PATH) && $(MAKE) && $(MAKE) install

$(RPGSEEDERHDR): $(RPGSEEDERHDR_DEPEND)
	cd $(RPGSEEDERHDR_PATH) && $(MAKE) headers

$(SPARROWLIB): $(SPARROW_DEPEND)
	cd $(SPARROW_PATH) && $(MAKE) && $(MAKE) install

$(LOGGERLIB): $(LOGGER_DEPEND)
	cd $(LOGGER_PATH) && $(MAKE) install

$(CDD): $(SPARROWLIB)

$(INCLUDEDIR):
	cd $(DGC) && mkdir include

$(LIBDIR):
	cd $(DGC) && mkdir lib

$(BINDIR):
	cd $(DGC) && mkdir bin

$(LOGSDIR):
	cd $(DGC) && mkdir logs

$(ALICELIB): $(ALICE_DEPEND)
	cd $(ALICE_PATH) && $(MAKE) && $(MAKE) install

$(REACTIVEPLANNERLIB): $(REACTIVEPLANNER_DEPEND)
	cd $(REACTIVEPLANNER_PATH) && $(MAKE) && $(MAKE) install

$(LADARSOURCELIB): $(LADARSOURCE_DEPEND)
	cd $(LADARSOURCE_PATH) && $(MAKE) && $(MAKE) install

$(LADARCLIENTLIB): $(LADARCLIENT_DEPEND)
	cd $(LADARCLIENT_PATH) && $(MAKE) && $(MAKE) install
