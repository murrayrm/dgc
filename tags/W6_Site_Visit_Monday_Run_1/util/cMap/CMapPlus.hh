#ifndef __CMAPPLUS_HH__
#define __CMAPPLUS_HH__

#include <iomanip>
#include <fstream>
#include <stdio.h>
#include "CMap.hh"
#include <sys/time.h>
#include <iostream>

#define COMMENT_CHAR '%'


using namespace std;


enum {
  CMP_LOGGING_OFF,
  CMP_LOGGING_ON,
  CMP_LOGGING_PAUSED,

  CMP_LOGGING_NUM
};


enum {
  CMP_DISPLAY_OFF,
  CMP_DISPLAY_ON,
  CMP_DISPLAY_PAUSED,

  CMP_DISPLAY_NUM
};


enum {
  CMP_LOG_MSG_HEADER = 0,
  CMP_LOG_MSG_SHIFT = 1,
  CMP_LOG_MSG_CLEAR = 2,
  CMP_LOG_MSG_CHANGE = 3,

  CMP_LOG_MSG_NUM
};


class CMapPlus : public CMap {
public:
  CMapPlus();
  ~CMapPlus();

  int initLayerLog(int layerNum, char* layerFilename);
  int startLayerLog(int layerNum);
  int pauseLayerLog(int layerNum);
  int closeLayerLog(int layerNum);

  template <class T>
  int saveLayer(int layerNum, char* layerFilenamePrefix);

  template <class T>
  int loadLayer(int layerNum, char* layerFilenamePrefix, bool bRaw = false);

  template <class T>
  int addLayer(T noDataVal, T outsideMapVal);

  template <class T>
  int setDataUTM(int layerNum, double UTMNorthing, double UTMEasting, T cellValue);
  template <class T>
  int setDataWin(int layerNum, int winRow, int winCol, T cellValue);

  int updateVehicleLoc(double UTMNorthing, double UTMEasting);

  int clearMap();
  int clearLayer(int layerNum);

  enum {
    CMP_TYPE_INT,
    CMP_TYPE_DBL,
    
    CMP_TYPE_NUM
  };
  

  template <class T>
  int saveLayerAsImage(int layerNum, char* filename, int type, T min, T max);
  

private:
  char** layerLogFilename;
  ofstream** layerLogFiles;
  int* layerLogStatus;

  int saveLayerShift(int layerNum);
  int saveLayerClear(int layerNum);
  template <class T>
  int saveLayerChange(int layerNum, int winRow, int winCol, T newValue);
};


template <class T>
int CMapPlus::saveLayerAsImage(int layerNum, char* filename, int type, T min, T max) {
  if(type>=CMP_TYPE_NUM) {
    return CM_UNKNOWN_ERROR;
  }
  /*
  IplImage* imageFile;

  CvSize imageSize;

  imageSize = cvSize(getNumCols(), getNumRows());
  imageFile = cvCreateImage(imageSize, IPL_DEPTH_8U, 3);

  double temp_val_dbl;
  unsigned char temp_val;
  double r_val, g_val, b_val;

  for(int r=getNumRows()-1; r>=0; r--) {
    for(int c=0; c<getNumCols(); c++) {
      temp_val_dbl = ((double)(getDataWin<T>(layerNum, r, c)-min))/((double)(max-min));
      if(temp_val_dbl < 0) {
	printf("thresh\n");
	temp_val_dbl = 0;
      }
      if(temp_val_dbl > 1) {
	printf("thres\n");
	temp_val_dbl = 1;
      }
      double h = temp_val_dbl;
      double s = 1;
      //double h = ((double)c)/((double)getNumCols());
      //double s = ((double)r)/((double)getNumRows());
      double v = 1;
      double var_h = 6*h;
      int var_i = (int)floor(var_h);
      double var_1 = v*(1-s);
      double var_2 = v*(1-s*(var_h - ((double)var_i)));
      double var_3 = v*(1-s*(1-(var_h-((double)var_i))));
      switch(var_i) {
      case 0:
	r_val = v;
	g_val = var_3;
	b_val = var_1;
	break;
      case 1:
	r_val = var_2;
	g_val = v;
	b_val = var_1;
	break;
      case 2:
	r_val = var_1;
	g_val = v;
	b_val = var_3;
	break;
      case 3:
	r_val = var_1;
	g_val = var_2;
	b_val = v;
	break;
      case 4:
	r_val = var_3;
	g_val = var_1;
	b_val = v;
	break;
      case 5:
	r_val = v;
	g_val = var_1;
	b_val = var_2;
	break;
      }


      ((unsigned char*)(imageFile->imageData + imageFile->widthStep*r))[3*c] = (unsigned char)(255*r_val);
      ((unsigned char*)(imageFile->imageData + imageFile->widthStep*r))[3*c+1] = (unsigned char)(255*g_val);
      ((unsigned char*)(imageFile->imageData + imageFile->widthStep*r))[3*c+2] = (unsigned char)(255*b_val);
    }
  }
    
  cvSaveImage(filename, imageFile);
  */
  return CM_OK;

}



template <class T>
int CMapPlus::addLayer(T noDataVal, T outsideMapVal) {
  int layerNum;

  layerNum = CMap::addLayer<T>(noDataVal, outsideMapVal);

  layerLogFilename = (char**)realloc(layerLogFilename, getNumLayers()*sizeof(char*));
  layerLogFiles = (ofstream**)realloc(layerLogFiles, getNumLayers()*sizeof(ofstream*));
  layerLogStatus = (int*)realloc(layerLogStatus, getNumLayers()*sizeof(int));
  if(layerLogFilename==NULL ||
     layerLogFiles == NULL ||
     layerLogStatus == NULL) {
    fprintf(stderr, "%s [%d]: Couldn't allocate memory!  Quitting...\n", __FILE__, __LINE__);
    exit(1);
  }

  return layerNum;
}


template <class T>
int CMapPlus::setDataUTM(int layerNum, double UTMNorthing, double UTMEasting, T cellValue) {
  int winRow, winCol;
  UTM2Win(UTMNorthing, UTMEasting, &winRow, &winCol);

  return setDataWin<T>(layerNum, winRow, winCol, cellValue);
}


template <class T>
int CMapPlus::setDataWin(int layerNum, int winRow, int winCol, T cellValue) {
  T& tempRef = CMap::getDataWin<T>(layerNum, winRow, winCol);
  if(tempRef != getLayerOutsideMapVal<T>(layerNum)) {
    tempRef = cellValue;

    saveLayerChange<T>(layerNum, winRow, winCol, cellValue);
  }

  return 0;
}


template <class T>
int CMapPlus::saveLayerChange(int layerNum, int winRow, int winCol, T newValue) {
  if(layerLogStatus[layerNum] == CMP_LOGGING_ON) {
    double timestamp;
    timeval time;
    gettimeofday(&time, NULL);
    timestamp = ((double) time.tv_sec) + (( (double) time.tv_usec) / 1000000.0); 
    *(layerLogFiles[layerNum]) << CMP_LOG_MSG_CHANGE << " " 
			       << layerNum << " " 
			       << fixed << timestamp << " "
			       << winRow << " " 
			       << winCol << " " 
			       << newValue << " "
			       << endl;
  }

  return 0;
}


template <class T>
int CMapPlus::loadLayer(int layerNum, char* layerFilenamePrefix, bool bRaw) {
  char headerFilename[256];
  char dataFilename[256];
  double timestamp, resRows, resCols, newUTMNorthing, newUTMEasting;
  int numRows, numCols;
  long int windowBottomLeftUTMNorthingRowResMultiple;
  long int windowBottomLeftUTMEastingColResMultiple;

  sprintf(headerFilename, "%s.hdr", layerFilenamePrefix);
  sprintf(dataFilename, "%s.dat", layerFilenamePrefix);

  FILE* headerFile = NULL;
  headerFile = fopen(headerFilename, "r");
  if(headerFile != NULL) {
    while(fgetc(headerFile)!='\n');
    while(fgetc(headerFile)!='\n');

    //fscanf(headerFile, "%s\n");
    //fscanf(headerFile, "%s\n");
    //fscanf(headerFile, "%c timestamp       numRows numCols resRows resCols bottomLeftNorthingRowMult bottomLeftEastingColMult\n", COMMENT_CHAR);
    fscanf(headerFile, "%lf %d    %d    %lf  %lf  %ld %ld\n", &timestamp, 
	    &numRows, &numCols, 
	    &resRows, &resCols,
	    &windowBottomLeftUTMNorthingRowResMultiple, &windowBottomLeftUTMEastingColResMultiple);
    fclose(headerFile);

		if(numRows != getNumRows() || numCols != getNumCols() ||
			 resRows != getResRows() || resCols != getResCols())
		{
			cerr << "loaded cmap file doesn't match cmap it's being loaded into. whaaaaaaaaaaaaaaa" << endl;
			exit(-1);
		}
  }
	else
	{
		cerr << "CMapPlus::loadLayer(): Couldn't read header file" << endl;
		exit(1);
	}

  newUTMNorthing = ((double)windowBottomLeftUTMNorthingRowResMultiple)*getResRows() + getNumRows()*getResRows()/2;
  newUTMEasting = ((double)windowBottomLeftUTMEastingColResMultiple)*getResCols() + getNumCols()*getResCols()/2;

  updateVehicleLoc(newUTMNorthing, newUTMEasting);

  ifstream dataFile(dataFilename);

	T cellValue;
	unsigned char dummy;

	if(dataFile) {
		for(int row=0; row < getNumRows(); row++)
		{
			for(int col=0; col < getNumCols(); col++)
			{
				if(bRaw)
				{
					dataFile.read((char*)&dummy,1);
					cellValue = (T)((double)(dummy) / 255.0);
					setDataWin<T>(layerNum, row, col, cellValue);
				}
				else
				{
					dataFile >> cellValue; 
					setDataWin<T>(layerNum, row, col, cellValue);
				}
			}
		}
	}
	else
	{
		cerr << "CMapPlus::loadLayer(): Couldn't read data file" << endl;
		exit(1);
	}

  dataFile.close();

  return 0;
}

template <class T>
int CMapPlus::saveLayer(int layerNum, char* layerFilenamePrefix) {
  char headerFilename[256];
  char dataFilename[256];
  //char datestamp[256];
  double timestamp;
  //time_t date;
  //tm* local;
  timeval time;

  gettimeofday(&time, NULL);
  timestamp = ((double) time.tv_sec) + (( (double) time.tv_usec) / 1000000.0); 

  sprintf(headerFilename, "%s.hdr", layerFilenamePrefix);
  sprintf(dataFilename, "%s.dat", layerFilenamePrefix);
  
  ofstream dataFile(dataFilename);
  FILE* headerFile = NULL;

  headerFile = fopen(headerFilename, "w");
  if(headerFile != NULL) {
    fprintf(headerFile, "%c This is the header file for a instantaneously saved CMap layer\n", COMMENT_CHAR);
    fprintf(headerFile, "%c timestamp       numRows numCols resRows resCols bottomLeftNorthingRowMult bottomLeftEastingColMult\n", COMMENT_CHAR);
    fprintf(headerFile, "%lf %.4d    %.4d    %.4lf  %.4lf  %ld %ld\n", timestamp, 
	    getNumRows(), getNumCols(), 
	    getResRows(), getResCols(),
	    getWindowBottomLeftUTMNorthingRowResMultiple(), getWindowBottomLeftUTMEastingColResMultiple());
    fclose(headerFile);
  }

  if(dataFile) {
    dataFile.precision(10);
    for(int row=0; row < getNumRows(); row++) {
      for(int col=0; col < getNumCols(); col++) {
	dataFile << getDataWin<T>(layerNum, row, col) << " ";
	//cout << getDataWin<T>(layerNum, row, col) << " ";
      }
      //cout <<endl;
      dataFile << endl;
    }
  }
  dataFile.close();

  return 0;
}


#endif //__CMAPPLUS__HH__
