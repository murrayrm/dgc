% script compose_waypoint_file.m
%
% Purpose:
%   This script composes a Bob-format waypoint file.
% 
% Changes:
%
%   2004/03/01, Lars Cremean, created
%   2005/04/13, LBC, resurrected
%
% Inputs:
%   none
%
% Example usage:
%
%   compose_wpfile

% now write the resulting data to another file
file = fopen('waypoints.bob', 'w');

fprintf(file, '# This is a modified Bob format waypoint file that has been\n');
fprintf(file, '# output by the MATLAB script compose_wpfile.m\n');
fprintf(file, '# Format: {char, Easting[m], Northing[m], Offset[m], Speed[m/s]}\n');

type   = 0;
radius = 5; 
speed  = 10;
nort0  = 396260;
east0  = 3777625;

for i = linspace( 0, 2000, 1000 )

  % make the waypoints do a sinusoidal path in the northerly direction
  east = east0 + 100 * sin( i / 7.5 );
  nort = nort0 + 7.5 * i;

  fprintf(file, '%d  %9.2f  %10.2f  %7.2f  %6.2f\n', type, east, nort, radius, speed);

end

fclose(file);
