/**
 * LadarClient.hh
 * Player client for ladars
 * Revision History:
 * 03/22/05 hbarnor created
 */

#ifndef LADARCLIENT_HH
#define LADARCLIENT_HH

/** the name of the config file containing custom host and port */
#define CONFIG_FILENAME "config"
/** the host to connect to the Player Server at (if no config file found) */
#define PLAYER_GAZEBO_HOST "localhost"
/** the port to connect to the Player Server at (if no config file found) */
#define PLAYER_GAZEBO_PORT 6665

#include "playerclient.h"
#include <string>
#include <fstream>
#include <iostream>


using namespace std;

/** 
 * The data mode we use for player communication 
 * We will pull new data when we need it. This is 
 * synonymous with the way the real hardware works. 
 */
const unsigned char DATA_MODE = PLAYER_DATAMODE_PULL_ALL;
class LadarClient
{

public:

  int numScanPoints;
  double *scanRanges;

  /**
   * Constructor
   */
  LadarClient();
  /**
   * Destructor - for memory management
   */
  ~LadarClient();
  /**
   * This method will connect player server based on config file. 
   * localhost if no config file exists.
   * will subscribe to bumperLadar and roofLadar
   * these will have to be defined in the world file
   */   
  int init(int lnum);

  /**
   * readConfig - if config file exist
   * read info on host running player
   * server
   */
  void readConfig();
  /** Tries to connect to the Player server until it succeedes */
  void connect();
  /** Disconnects the player client */
  void disconnect();
  /* Subscribes to the different devices, you must run connect() first */
  void subscribe();
  /* Unsubscribes the different devices, good to do before disconnect() */
  void unsubscribe();
  /* Tests getting data from gazebo */
  void grab();

protected:
  
private:
/**
   * Our client instance
   */
  PlayerClient *alice; 
  /**
   * Our bumper ladar - will need to change when more ladars are added
   */
  LaserProxy *playerLadar;
    /**
     * Our roof ladar
   */
  // LaserProxy *roofLadar;  // not in use right now


  


  /**
   * the host running the player server
   */
  string host;
  /**
   * the port running the player
   * server
   */
  int port;
  /*the number assigned to the ladar in alice.cfg*/
  int ladarnum;
};

#endif
