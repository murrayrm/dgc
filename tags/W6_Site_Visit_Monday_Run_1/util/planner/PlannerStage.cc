#include <math.h>
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

#include "AliceConstants.h"
#include "PlannerStage.h"
#include "CMapPlus.hh"

extern "C" int snopt_(char *, int *, int *, int *,
											int *, int *, int *, int *, int *, 
											double *, char *,
											int (*)(int *, int *, int *, int *, double *x, double *, double *, int *, char *, int *, int *, int *, double *, int *, int),
											int (*)(int *, int *, double *, double *, double *, int *, char *, int *, int *, int *, double *, int *, int),
											double *, int *, 
											int *, double *, double *, char *, int *, 
											double *, double *, double *, int *, int *, 
											int *, int *, int *, int *, double *, 
											double *, char *, int *, int *, int *, double *,
											int *, char *, int *, int *, int *, double *,
											int *, int, int, int, int, int);
extern "C" int snseti_(char *,    int *, int *, int *, int *, char *, int *, int *, int *, double *, int *, int, int);
extern "C" int snsetr_(char *, double *, int *, int *, int *, char *, int *, int *, int *, double *, int *, int, int);
extern "C" int sninit_(int *, int *, char *, int *, int *, int *, double *, int *, int);
extern "C" int snset_ (char *,    			 int *, int *, int *, char *, int *, int *, int *, double *, int *, int, int);

#define DO_BENCHMARK

#ifdef DO_BENCHMARK
#include "DGCutils"
int numinterpblurs = 0;
unsigned long long costtime = 0;
unsigned long long blurtime = 0;
unsigned long long integrateNEtime = 0;
unsigned long long timetotal = 0;
#endif

int SNOPTfuncConstr(int *mode, int *nncon, int *nnjac, 
										int *nejac, double *pSolverState, double *fCon, double *gCon, int *nstate,
										char *cu, int *lencu, int *iu, int *leniu, double *ru, int *lenru, int cu_len)
{
#ifdef DO_BENCHMARK
	unsigned long long t1, t2;
	DGCgettime(t1);
#endif

	CPlannerStage* pPlannerStage = (CPlannerStage*)cu;
	if(memcmp(pPlannerStage->m_pPrevSolverState, pSolverState, (*nnjac)*sizeof(pSolverState[0])) != 0)
	{
		memcpy(pPlannerStage->m_pPrevSolverState, pSolverState, (*nnjac)*sizeof(pSolverState[0]));
		pPlannerStage->makeCollocationData(pSolverState);
		pPlannerStage->funcConstr();
		pPlannerStage->funcCost();
	}

	if(*mode == 2 || *mode == 0)
		memcpy(fCon, pPlannerStage->m_pConstrData, (*nncon)*sizeof(fCon[0]));
	if(*mode == 2 || *mode == 1)
		memcpy(gCon, pPlannerStage->m_pConstrGradData, (*nejac)*sizeof(gCon[0]));

#ifdef DO_BENCHMARK
	DGCgettime(t2);
	costtime += t2-t1;
#endif
	return 0;
}

int SNOPTfuncCost(int *mode, int *nnobj,
									double *pSolverState, double *fObj, double *gObj, int *nstate,
									char *cu, int *lencu, int *iu, int *leniu, double *ru, int *lenru, int cu_len)
{
#ifdef DO_BENCHMARK
	unsigned long long t1, t2;
	DGCgettime(t1);
#endif

	CPlannerStage* pPlannerStage = (CPlannerStage*)cu;
	if(memcmp(pPlannerStage->m_pPrevSolverState, pSolverState, (*nnobj)*sizeof(pSolverState[0])) != 0)
	{
		memcpy(pPlannerStage->m_pPrevSolverState, pSolverState, (*nnobj)*sizeof(pSolverState[0]));
		pPlannerStage->makeCollocationData(pSolverState);
		pPlannerStage->funcConstr();
		pPlannerStage->funcCost();
	}

	if(*mode == 2 || *mode == 0)
		*fObj = pPlannerStage->m_obj;
	if(*mode == 2 || *mode == 1)
		memcpy(gObj, pPlannerStage->m_pCostGradData, (*nnobj)*sizeof(gObj[0]));

#ifdef DO_BENCHMARK
	DGCgettime(t2);
	costtime += t2-t1;
#endif

	return 0;
}

CPlannerStage::CPlannerStage(CMap *pMap, int mapLayerID, RDDF* pRDDF, bool USE_MAXSPEED)
	: m_pRDDF(pRDDF), m_pMap(pMap), m_mapLayer(mapLayerID), m_USE_MAXSPEED(USE_MAXSPEED)
{
	// Allocate memory for the trajectory. Trajectory is 3th order (has up to and
	// including 2nd derivatives)
	m_pTraj		= new CTraj(3);

	// Allocate memory for various arrays and init various SNOPT variables
	strncpy(start, "Cold    ", 8);
	nName = 1;
	objadd = 0.0;
	strncpy(prob,  "planner ", 8);
	inform = 0;

	// Create the map layer that will contain the cached values for the blur. -1.0
	// is the outside-map-value. -2.0 is the no-data value. Thus any positive
	// value indicated valid data (that positive value IS the valid data).
	if(m_pMap != NULL)
		m_blurLayer = m_pMap->addLayer<double>(-2.0, -1.0);
}

void CPlannerStage::initialize(char *pDercoeffsFile)
{
	int i;

	SNOPTn = NPnumVariables;
	nnobj = NPnumVariables;
	nnjac = NPnumVariables;
	// no linear objective
	iobj = 0;

	m_pSpecsFile = new char[128];
	m_splinecoeffsTheta = new double[(NUM_POINTS-1) * 3];
	m_splinecoeffsSpeed = new double[(NUM_POINTS-1) * 3];
	m_PlannerLowerBounds = new double[NUM_LIN_INIT_CONSTR  + NUM_LIN_FINL_CONSTR  + NUM_LIN_TRAJ_CONSTR +
																	NUM_NLIN_INIT_CONSTR + NUM_NLIN_FINL_CONSTR + NUM_NLIN_TRAJ_CONSTR];
	m_PlannerUpperBounds = new double[NUM_LIN_INIT_CONSTR  + NUM_LIN_FINL_CONSTR  + NUM_LIN_TRAJ_CONSTR +
																	NUM_NLIN_INIT_CONSTR + NUM_NLIN_FINL_CONSTR + NUM_NLIN_TRAJ_CONSTR];
	m_valcoeffs = new double[NUM_COLLOCATION_POINTS*NUM_POINTS];
	m_d1coeffs  = new double[NUM_COLLOCATION_POINTS*NUM_POINTS];
	m_d2coeffs  = new double[NUM_COLLOCATION_POINTS*NUM_POINTS];
	m_valcoeffsfine = new double[NUM_COLLOCATION_POINTS_FINE*NUM_POINTS];
	m_d1coeffsfine  = new double[NUM_COLLOCATION_POINTS_FINE*NUM_POINTS];
	m_d2coeffsfine  = new double[NUM_COLLOCATION_POINTS_FINE*NUM_POINTS];

	// the matrix that takes the spline control points and generates the
	// polynomial coefficients of each spline segment. There are NUM_POINTS
	// control points. There are NUM_POINTS-1 spline segments, and since each is
	// quadratic, each is defined by 3 values. Note that since the initial value
	// of the spline (m_initSpeed, m_initTheta) is not included, these will need
	// to be manually added.
	m_dercoeffsmatrix = new double[NUM_POINTS*((NUM_POINTS-1) * 3)];
	m_stateestimatematrix = new double[NUM_POINTS*NUM_COLLOCATION_POINTS_FINE];

	m_pColl_theta       = new double[NUM_COLLOCATION_POINTS];
	m_pColl_dtheta      = new double[NUM_COLLOCATION_POINTS];
	m_pColl_ddtheta     = new double[NUM_COLLOCATION_POINTS];
	m_pCollN            = new double[NUM_COLLOCATION_POINTS];
	m_pCollE            = new double[NUM_COLLOCATION_POINTS];
	m_pCollGradN        = new double[NUM_COLLOCATION_POINTS * NUM_POINTS];
	m_pCollGradE        = new double[NUM_COLLOCATION_POINTS * NUM_POINTS];
	m_pCollVlimit       = new double[NUM_COLLOCATION_POINTS];
	m_pCollDVlimitDN    = new double[NUM_COLLOCATION_POINTS];
	m_pCollDVlimitDE    = new double[NUM_COLLOCATION_POINTS];
	m_pCollDVlimitDTheta= new double[NUM_COLLOCATION_POINTS];

	m_pCollGradVlimit   = new double[NUM_COLLOCATION_POINTS * (NUM_POINTS+1)];

	m_pConstrData       = new double[NPnumNonLinearConstr];
	m_pConstrGradData   = new double[NPnumNonLinearConstr*NPnumVariables];
	m_pCostGradData     = new double[NPnumVariables];

	m_grad_solver = new double*[NUM_NLIN_INIT_CONSTR +
															NUM_NLIN_TRAJ_CONSTR +
															NUM_NLIN_FINL_CONSTR];
	for(i=0;
			i<NUM_NLIN_INIT_CONSTR +
				NUM_NLIN_TRAJ_CONSTR +
				NUM_NLIN_FINL_CONSTR;
			i++)
	{
		m_grad_solver[i] = new double[SIZE_ZP];
	}

	m_pOutputTheta  = new double[NUM_COLLOCATION_POINTS_FINE];
	m_pOutputDTheta = new double[NUM_COLLOCATION_POINTS_FINE];

	m_mapblurcoeffs   = new double[(MAP_BLURINTERP_DIAM_POINTS * MAP_INTERPOLATION_DIAM) * (MAP_BLURINTERP_DIAM_POINTS * MAP_INTERPOLATION_DIAM)];

	ifstream dercoeffs(pDercoeffsFile);
	if(!dercoeffs)
	{
		cerr << "couldn't open derivative coefficients file\n";
		exit(1);
	}
	dercoeffs.read((char*)m_valcoeffs, NUM_COLLOCATION_POINTS*NUM_POINTS*sizeof(double));
	dercoeffs.read((char*)m_d1coeffs,  NUM_COLLOCATION_POINTS*NUM_POINTS*sizeof(double));
	dercoeffs.read((char*)m_d2coeffs,  NUM_COLLOCATION_POINTS*NUM_POINTS*sizeof(double));

	ifstream dercoeffsfine("dercoeffsfine.dat");
	if(!dercoeffsfine)
	{
		cerr << "couldn't open derivative coefficients fine file\n";
		exit(1);
	}
	dercoeffsfine.read((char*)m_valcoeffsfine, NUM_COLLOCATION_POINTS_FINE*NUM_POINTS*sizeof(double));
	dercoeffsfine.read((char*)m_d1coeffsfine,  NUM_COLLOCATION_POINTS_FINE*NUM_POINTS*sizeof(double));
	dercoeffsfine.read((char*)m_d2coeffsfine,  NUM_COLLOCATION_POINTS_FINE*NUM_POINTS*sizeof(double));

	ifstream dercoeffsmatrix("dercoeffsmatrix.dat");
	if(!dercoeffsmatrix)
	{
		cerr << "couldn't open derivative matrix file\n";
		exit(1);
	}

	dercoeffsmatrix.read((char*)m_dercoeffsmatrix, NUM_POINTS*((NUM_POINTS-1) * 3)*sizeof(double));

	ifstream stateestimatematrix("stateestimatematrix.dat");
	if(!stateestimatematrix)
	{
		cerr << "couldn't open state estimate matrix file\n";
		exit(1);
	}

	stateestimatematrix.read((char*)m_stateestimatematrix, NUM_POINTS*NUM_COLLOCATION_POINTS_FINE*sizeof(double));

#warning "not all variables have nonlinear contributions"
	// extra 1 for the cost
	SNOPTm = NPnumNonLinearConstr + NPnumLinearConstr;
	nncon = NPnumNonLinearConstr;
	ne = (NPnumNonLinearConstr + NPnumLinearConstr) * NPnumVariables;

	m_SNOPTmatrix = new double[ne];
	ha = new int[ne];
	ka = new int[SNOPTn+1];
	hs = new int[SNOPTn+SNOPTm];
	m_pSolverState = new double[SNOPTn+SNOPTm];
	m_pPrevSolverState = new double[SNOPTn];
	pi = new double[SNOPTm];
	rc = new double[SNOPTn+SNOPTm];
	m_SNOPTlowerBounds = new double[NPnumVariables + NPnumNonLinearConstr + NPnumLinearConstr];
	m_SNOPTupperBounds = new double[NPnumVariables + NPnumNonLinearConstr + NPnumLinearConstr];

	memset(m_SNOPTmatrix, 0, ne*sizeof(m_SNOPTmatrix[0]));
	for(i=0; i<ne; i++)
	{
		ha[i] = (i % (NPnumNonLinearConstr + NPnumLinearConstr)) + 1;
	}

	for(i=0; i<=SNOPTn; i++)
	{
		ka[i] = (NPnumNonLinearConstr + NPnumLinearConstr) * i + 1;
	}

	memset(hs, 0, (SNOPTn+SNOPTm)*sizeof(hs[0]));
	memset(pi, 0, SNOPTm*sizeof(pi[0]));

	SetUpLinearConstr();
}

void CPlannerStage::setContext(SSNOPTContext* pContext)
{
	cw = pContext->cw;
	iw = pContext->iw;
	rw = pContext->rw;
	lencw = pContext->lencw;
	leniw = pContext->leniw;
	lenrw = pContext->lenrw;
}

CPlannerStage::~CPlannerStage()
{
	int i;

	delete m_pTraj;

	// free memory
	delete m_SNOPTmatrix;
	delete ha;
	delete ka;
	delete hs;
	delete pi;
	delete rc;
	delete m_pSolverState;
	delete m_pPrevSolverState;
	delete m_SNOPTlowerBounds;
	delete m_SNOPTupperBounds;

	delete m_splinecoeffsTheta;
	delete m_splinecoeffsSpeed;
	delete m_PlannerLowerBounds;
	delete m_PlannerUpperBounds;

	delete m_valcoeffs;
	delete m_d1coeffs;
	delete m_d2coeffs;
	delete m_valcoeffsfine;
	delete m_d1coeffsfine;
	delete m_d2coeffsfine;
	delete m_stateestimatematrix;

	delete m_pColl_theta;
	delete m_pColl_dtheta;
	delete m_pColl_ddtheta;
	delete m_pCollN;
	delete m_pCollE;
	delete m_pCollGradN;
	delete m_pCollGradE;
	delete m_pCollVlimit;
	delete m_pCollDVlimitDN;
	delete m_pCollDVlimitDE;
	delete m_pCollDVlimitDTheta;

	delete m_pCollGradVlimit;

	delete m_pConstrData;
	delete m_pConstrGradData;
	delete m_pCostGradData;

	for(i=0;
			i<NUM_NLIN_INIT_CONSTR +
				NUM_NLIN_TRAJ_CONSTR +
				NUM_NLIN_FINL_CONSTR;
			i++)
	{
		delete m_grad_solver[i];
	}
	delete m_grad_solver;
	delete m_dercoeffsmatrix;
	delete m_mapblurcoeffs;

	delete m_pSpecsFile;

	delete m_pOutputTheta;
	delete m_pOutputDTheta;
}

void CPlannerStage::makeSNOPTBoundsArrays(void)
{
	int i, j;

	for(i=0; i<NPnumVariables-1; i++)
	{
		m_SNOPTlowerBounds[i] = -BIGNUMBER;
		m_SNOPTupperBounds[i] = BIGNUMBER;
	}

	// set the bounds for sf
	m_SNOPTlowerBounds[NPnumVariables-1] = MIN_SF;
	m_SNOPTupperBounds[NPnumVariables-1] = MAX_SF;


	// put in the non-linear constraints
	for(i=0; i<NUM_NLIN_INIT_CONSTR; i++)
	{
		m_SNOPTlowerBounds[i+NPnumVariables] = m_PlannerLowerBounds[i];
		m_SNOPTupperBounds[i+NPnumVariables] = m_PlannerUpperBounds[i];
	}
	for(i=0; i<NUM_NLIN_TRAJ_CONSTR; i++)
	{
#warning "surely there's a better way to do this, rather than ignoring the first several traj constraints"
#warning "also, this particular way of specifying this is ugly. these constraints should just go away"
		for(j=0; j<NUM_IGNORED_CONSTRAINTS; j++)
		{
			m_SNOPTlowerBounds[i+j*NUM_NLIN_TRAJ_CONSTR+NPnumVariables+NUM_NLIN_INIT_CONSTR] =
				-BIGNUMBER;
			m_SNOPTupperBounds[i+j*NUM_NLIN_TRAJ_CONSTR+NPnumVariables+NUM_NLIN_INIT_CONSTR] =
				BIGNUMBER;
		}
		for(j=NUM_IGNORED_CONSTRAINTS; j<NUM_COLLOCATION_POINTS; j++)
		{
			m_SNOPTlowerBounds[i+j*NUM_NLIN_TRAJ_CONSTR+NPnumVariables+NUM_NLIN_INIT_CONSTR] =
				m_PlannerLowerBounds[i + NUM_NLIN_INIT_CONSTR];
			m_SNOPTupperBounds[i+j*NUM_NLIN_TRAJ_CONSTR+NPnumVariables+NUM_NLIN_INIT_CONSTR] =
				m_PlannerUpperBounds[i + NUM_NLIN_INIT_CONSTR];
		}
	}
	for(i=0; i<NUM_NLIN_FINL_CONSTR; i++)
	{
		m_SNOPTlowerBounds[i + NPnumVariables + NUM_NLIN_INIT_CONSTR + NUM_COLLOCATION_POINTS*NUM_NLIN_TRAJ_CONSTR] =
			m_PlannerLowerBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + i];
		m_SNOPTupperBounds[i + NPnumVariables + NUM_NLIN_INIT_CONSTR + NUM_COLLOCATION_POINTS*NUM_NLIN_TRAJ_CONSTR] =
			m_PlannerUpperBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + i];
	}

	// put in the linear constraints
	for(i=0; i<NUM_LIN_INIT_CONSTR; i++)
	{
		m_SNOPTlowerBounds[i+NPnumVariables+NPnumNonLinearConstr] = m_PlannerLowerBounds[i + NUM_NLIN_CONSTR];
		m_SNOPTupperBounds[i+NPnumVariables+NPnumNonLinearConstr] = m_PlannerUpperBounds[i + NUM_NLIN_CONSTR];
	}
	for(i=0; i<NUM_LIN_TRAJ_CONSTR; i++)
	{
		for(j=0; j<NUM_COLLOCATION_POINTS; j++)
		{
			m_SNOPTlowerBounds[i*NUM_COLLOCATION_POINTS+j+NPnumVariables+NPnumNonLinearConstr+NUM_LIN_INIT_CONSTR] =
				m_PlannerLowerBounds[i + NUM_NLIN_CONSTR + NUM_LIN_INIT_CONSTR];
			m_SNOPTupperBounds[i*NUM_COLLOCATION_POINTS+j+NPnumVariables+NPnumNonLinearConstr+NUM_LIN_INIT_CONSTR] =
				m_PlannerUpperBounds[i + NUM_NLIN_CONSTR + NUM_LIN_INIT_CONSTR];
		}
	}
	for(i=0; i<NUM_LIN_FINL_CONSTR; i++)
	{
		m_SNOPTlowerBounds[i + NPnumVariables+NPnumNonLinearConstr + NUM_LIN_INIT_CONSTR + NUM_COLLOCATION_POINTS*NUM_LIN_TRAJ_CONSTR] =
			m_PlannerLowerBounds[NUM_NLIN_CONSTR + NUM_LIN_INIT_CONSTR + NUM_LIN_TRAJ_CONSTR + i];
		m_SNOPTupperBounds[i + NPnumVariables+NPnumNonLinearConstr + NUM_LIN_INIT_CONSTR + NUM_COLLOCATION_POINTS*NUM_LIN_TRAJ_CONSTR] =
			m_PlannerUpperBounds[NUM_NLIN_CONSTR + NUM_LIN_INIT_CONSTR + NUM_LIN_TRAJ_CONSTR + i];
	}
}

void CPlannerStage::SetUpLinearConstr(void)
{
	int i,j;

	// this is probably redundant
	memset(m_SNOPTmatrix,0, (NPnumNonLinearConstr + NPnumLinearConstr) * NPnumVariables * sizeof(m_SNOPTmatrix[0])); 

// 	// set up the linear constraints
// 	// traj constraint: theta (measured from the init theta)
// 	for(j=0; j<NUM_COLLOCATION_POINTS; j++)
// 	{
// 		for(i=0; i<NUM_POINTS; i++)
// 		{
// 			m_SNOPTmatrix[i * (NPnumNonLinearConstr + NPnumLinearConstr) + NPnumNonLinearConstr + j] =
// 				SCALEFACTOR_THETA * m_valcoeffs[j + i*NUM_COLLOCATION_POINTS];
// 		}
// 		m_SNOPTmatrix[(NPnumVariables-1) * (NPnumNonLinearConstr + NPnumLinearConstr) + NPnumNonLinearConstr + j] =
// 			0.0;
// 	}

// 	// final constraint: final theta
// 	for(i=0; i<NUM_POINTS; i++)
// 	{
// 		m_SNOPTmatrix[i * (NPnumNonLinearConstr + NPnumLinearConstr) + NPnumNonLinearConstr +
// 									NUM_COLLOCATION_POINTS*NUM_LIN_TRAJ_CONSTR] =
// 			SCALEFACTOR_THETA * m_valcoeffs[(NUM_COLLOCATION_POINTS-1) + i*NUM_COLLOCATION_POINTS];
// 	}
// 	m_SNOPTmatrix[(NPnumVariables-1) * (NPnumNonLinearConstr + NPnumLinearConstr) + NPnumNonLinearConstr +
// 								NUM_COLLOCATION_POINTS*NUM_LIN_TRAJ_CONSTR] =
// 		0.0;
}



void CPlannerStage::SetUpPlannerBounds(VehicleState *pState)
{
	NEcoord coord = m_pRDDF->getPointAlongTrackLine(m_initN, m_initE,
																									PLANNING_TARGET_DIST, &m_targetTheta);
	m_targetN = coord.N - m_initN;
	m_targetE = coord.E - m_initE;

	// adjust m_targetTheta so it wraps around properly
	while((m_targetTheta - m_initTheta) > M_PI)
		m_targetTheta -= 2.0*M_PI;
	while((m_initTheta - m_targetTheta) > M_PI)
		m_targetTheta += 2.0*M_PI;

	if(CARTESIAN_TARGET_SPECIFICATION)
	{
		m_PlannerLowerBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + 0] = SCALEFACTOR_DIST * (- HALF_TARGET_WINDOW);
		m_PlannerUpperBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + 0] = SCALEFACTOR_DIST * (  HALF_TARGET_WINDOW);
		m_PlannerLowerBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + 1] = SCALEFACTOR_DIST * (- HALF_TARGET_WINDOW);
		m_PlannerUpperBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + 1] = SCALEFACTOR_DIST * (  HALF_TARGET_WINDOW);
	}
	else
	{
		m_PlannerLowerBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + 0] = -HALF_TARGET_ERROR;
		m_PlannerUpperBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + 0] =  HALF_TARGET_ERROR;
		m_PlannerLowerBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + 1] = -BIGNUMBER;
		m_PlannerUpperBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + 1] =  BIGNUMBER;
	}
}

bool CPlannerStage::SeedFromTraj(CTraj* pTraj, double dist)
{
	double trajlen = pTraj->getLength();

	// if the distance isn't specified, use the whole length of the traj
	// if the traj isn't long enough to accomodate the seed, return an error
	if(dist == 0.0)
		dist = trajlen;
	else if(trajlen < dist)
		return false;

	double *pThetaVector = new double[NUM_COLLOCATION_POINTS_FINE];

	pTraj->getThetaVector(dist, NUM_COLLOCATION_POINTS_FINE, pThetaVector, m_initTheta);

	char notrans = 'N';
	int rows = NUM_POINTS;
	int cols = NUM_COLLOCATION_POINTS_FINE;
	double alpha = 1.0;
	double beta = 0.0;
	int incr = 1;

	dgemv_(&notrans, &rows, &cols, &alpha, m_stateestimatematrix, &rows,
				 pThetaVector, &incr, &beta, m_pSolverState, &incr);

	m_pSolverState[NPnumVariables-1] = dist;

	setTargetToDistAlongSpline(1.0);

	delete pThetaVector;
	return true;
}

#define min(a,b) ((a)<(b) ? (a) : (b))

void CPlannerStage::setTargetToDistAlongSpline(double distRatio)
{
	int i;
	int finalPoint;

	finalPoint = min(lround(distRatio * (double)NUM_COLLOCATION_POINTS_FINE), NUM_COLLOCATION_POINTS_FINE-1);

	getSplineCoeffsTheta(m_pSolverState);
	m_targetN = m_targetE = 0.0;
	for(i=0;
			i < finalPoint;
			i++)
	{
		integrateNEfine(i, m_targetN, m_targetE);
	}
	m_targetN *= m_pSolverState[NPnumVariables-1];
	m_targetE *= m_pSolverState[NPnumVariables-1];
}

int CPlannerStage::run(VehicleState *pVehState, double* pSolverState)
{
#ifdef DO_BENCHMARK
	unsigned long long t1, t2;
	DGCgettime(t1);
	costtime = 0;
	blurtime = 0;
	numinterpblurs = 0;
	integrateNEtime = 0;
	timetotal = 0;
#endif
	double speedAtRearAxle = hypot(pVehState->Vel_N + VEHICLE_WHEELBASE*pVehState->YawRate*sin(pVehState->Yaw), 
																 pVehState->Vel_E - VEHICLE_WHEELBASE*pVehState->YawRate*cos(pVehState->Yaw));

	m_initSpeed		= fmax(MIN_V, speedAtRearAxle);
	m_initTheta		= pVehState->Yaw;
	m_initN				= pVehState->Northing - VEHICLE_WHEELBASE*cos(pVehState->Yaw);
	m_initE				= pVehState->Easting  - VEHICLE_WHEELBASE*sin(pVehState->Yaw);





// 	m_initN = m_initE = 0.0;
// 	double nlow = 3834756.0;
// 	double elow = 442524.0;
// 	double nhigh= 3834756.0;
// 	double ehigh= 442536.0;
// 	double resl = 0.05;
// 	double n,e,v,dvn,dve;

// 	ofstream sp3("sp3.dat");
// 	sp3 << setprecision(10);
// // 	for(n=nlow; n<=nhigh; n+=3.0*resl)
// // 	{
// 	n = nlow;
// 		for(e=elow;e<=ehigh;e+=3.0*resl)
// 		{
// 			getContinuousMapValueDiff(n,e,&v,&dvn,&dve);
// 			sp3 << e-elow << ' ' << v << ' ' << dve << endl;
// 		}
// 		sp3 << endl;
// // 	}
// 	exit(1);





 	cout << setprecision(20);


	// if we couldn't set up the problem (probably because we were too close to
	// the end, return
	if(!SetUpPlannerBoundsAndSeedCoefficients(pSolverState, pVehState))
		return -2;

	cout << "target: (" << m_targetN << ", " << m_targetE << endl;

	// make the bounds arrays for SNOPT
	makeSNOPTBoundsArrays();

	// zero out the previous solver state, since there is none
	memset(m_pPrevSolverState, 0, (SNOPTn)*sizeof(m_pPrevSolverState[0]));

	// indicate that no blurring data is available
	m_pMap->clearLayer(m_blurLayer);


// 	double asdf[11] = {-3.1526015312158786,-2.8088311304172127,0.093214528023877349,3.0229032627714303,1.5115091178747668,-2.3217007625102601,-1.5600698414849232,0.82443778173860915,1.431700685086517,-0.013708444128843304,66.767714035427019};
// 	memcpy(m_pSolverState, asdf, sizeof(asdf));
// 	outputTraj(m_pTraj);
// 	ofstream seedfil("seedfil");
// 	m_pTraj->print(seedfil);
// 	exit(1);

	CallSolver();

	ifstream inf("doplot");
	int doplot;
	inf >> doplot;
	if(doplot == 1)
	{
		CMapPlus *pMapPlus = (CMapPlus*)m_pMap;
		if(NUM_POINTS+1 == NPnumVariables)
			pMapPlus->saveLayer<double>(m_mapLayer, "map1");
		else
			pMapPlus->saveLayer<double>(m_mapLayer, "map2");
	}

	// 	if(inform == 0)
	{
		outputTraj(m_pTraj);
	}

#ifdef DO_BENCHMARK
	DGCgettime(t2);
	timetotal += t2-t1;

 	cout << setprecision(3);
	cout << "Time integrateNE: " << DGCtimetosec(integrateNEtime) << endl;
	cout << "Time blur: " << DGCtimetosec(blurtime) << endl;
	cout << "Time per intepblur: " << (double) blurtime / (double)numinterpblurs << endl;
	cout << "Time cost: " << DGCtimetosec(costtime) << endl;
	cout << "Time total: " << DGCtimetosec(timetotal) << endl;
 	cout << setprecision(15);
#endif
	return inform;
}

void CPlannerStage::checkDerivatives(void)
{
// 	ofstream derdat("derdat");
// 	derdat << setprecision(20);

// 	int i = 1;
// 	double del = 0.001;
// 	double swing = 1.0;

// 	m_pSolverState[i] -= swing;
// 	for(double j=-swing;j<swing;j+=del)
// 	{
// 		m_pSolverState[i] += del;

// 		makeCollocationData(m_pSolverState);
// 		funcConstr();
// 		funcCost();

// 		derdat << m_pSolverState[i] << ' ' << m_obj << ' ' << m_pCostGradData[i] << endl;
// 	}
// 	exit(1);







// 	int i;
// 	double obj;
// 	double* pCostGradData = new double[NPnumVariables];
// 	double* pCostGradComputed = new double[NPnumVariables];

// 	makeCollocationData(m_pSolverState);
// 	funcConstr();
// 	funcCost();

// 	obj = m_obj;
// 	memcpy(pCostGradData, m_pCostGradData, NPnumVariables*sizeof(pCostGradData[0]));

// 	cout << "Objective gradient check: given, computed, %err" << endl;
// 	cout.setf(ios::showpoint);
// 	cout.precision(4);
// 	for(i=0; i<NPnumVariables; i++)
// 	{
// 		cout.width(10);
// 		cout << pCostGradData[i];
// 	}
// 	cout << endl;

// 	for(i=0; i<NPnumVariables; i++)
// 	{
// 		m_pSolverState[i] += CHECKDERIVATIVES_DELTA;

// 		makeCollocationData(m_pSolverState);
// 		funcConstr();
// 		funcCost();

// 		pCostGradComputed[i] = (m_obj - obj) / CHECKDERIVATIVES_DELTA;
// 		cout.width(10);
// 		cout << pCostGradComputed[i];

// 		m_pSolverState[i] -= CHECKDERIVATIVES_DELTA;
// 	}
// 	cout << endl;

// 	for(i=0; i<NPnumVariables; i++)
// 	{
// 		cout.width(10);
// 		cout << fabs((pCostGradComputed[i] - pCostGradData[i]) / pCostGradData[i]);
// 	}
// 	cout << endl;

// 	delete pCostGradData;
// 	delete pCostGradComputed;
}

void CPlannerStage::CallSolver(void)
{
	memset(hs, 0, (SNOPTn+SNOPTm)*sizeof(hs[0]));
	memset(pi, 0, SNOPTm*sizeof(pi[0]));

	int isumm = 0;
	sninit_(&PRINT_LEVEL, &isumm,
					cw, &lencw, iw, &leniw, rw, &lenrw, 8);

	ifstream specs(m_pSpecsFile);
	string line;
	if(specs)
		while(getline(specs,line))
		{
			if(line == "print6")
				PRINT_LEVEL = 6;
			else if(line == "print0")
				PRINT_LEVEL = 0;
			else
			{
				snset_((char*)line.c_str(), &PRINT_LEVEL, &isumm, &inform,
							 cw, &lencw, iw, &leniw, rw, &lenrw, line.length(), 8);
			}
		}

	// zero out the top half (slacks) of solverstate. This might not be necessary
	memset(m_pSolverState + NPnumVariables, 0, (NPnumNonLinearConstr + NPnumLinearConstr)*sizeof(m_pSolverState[0]));


	snopt_(start, &SNOPTm, &SNOPTn, &ne, &nName, &nncon, &nnobj, &nnjac, &iobj, &objadd,
				 prob, SNOPTfuncConstr, SNOPTfuncCost,
				 m_SNOPTmatrix, ha, ka, m_SNOPTlowerBounds, m_SNOPTupperBounds, NULL, hs, m_pSolverState, pi, rc,
				 &inform, &mincw, &miniw, &minrw, &ns, &ninf, &sinf, &obj,
				 (char*)this, &lencw, NULL, NULL, rw, &lenrw,
				 cw, &lencw, iw, &leniw, rw, &lenrw,
				 8, 8, 8, 8, 8);

	// the following tests whether the gradients are proper
	// 	int iState = 20;
	// 	int iCon;
	// 	int mode = 2;
	// 	double fCon[NPnumNonLinearConstr];
	// 	double gCon[NPnumNonLinearConstr*NPnumVariables];
	// 	ofstream dataout("test.dat");
	//   m_pSolverState[0]=   -0.70204;
	//   m_pSolverState[1]=    0.64412;
	//   m_pSolverState[2]=   -6.06464;
	//   m_pSolverState[3]=    3.17813;
	//   m_pSolverState[4]=   -0.02959;
	//   m_pSolverState[5]=   -0.95691;
	//   m_pSolverState[6]=    1.80859;
	//   m_pSolverState[7]=    0.84552;
	//   m_pSolverState[8]=   -4.46146;
	//   m_pSolverState[9]=    5.67879;
	//   m_pSolverState[10]=    0.0;
	//   m_pSolverState[11]= -16.73304;
	//   m_pSolverState[12]=  -2.15412;
	//   m_pSolverState[13]=  11.57775;
	//   m_pSolverState[14]=   8.61134;
	//   m_pSolverState[15]=   6.07860;
	//   m_pSolverState[16]=   5.00339;
	//   m_pSolverState[17]=   1.33800;
	//   m_pSolverState[18]=  -2.05089;
	//   m_pSolverState[19]=   4.32064;
	//   m_pSolverState[20]=  77.68337;
	// 	for(int i=0; i<10; i++)
	// 		m_pSolverState[i]=0.0;

	// 	for(m_pSolverState[iState] = -0.5;
	// 			m_pSolverState[iState] < 0.5;
	// 			m_pSolverState[iState]+=0.05)
	// 	{
	// 		int nu = NPnumNonLinearConstr;
	// 		SNOPTfuncConstr(&mode, NULL, NULL,  NULL,
	// 										m_pSolverState, fCon, gCon,
	// 										NULL, NULL, NULL, NULL, &COLLOCATION_FACTOR, NULL, NULL, 0);

	// 		dataout << m_pSolverState[iState] << ' ';
	// 		for(iCon = 0; iCon<NPnumNonLinearConstr; iCon++)
	// 		{
	// 			dataout << fCon[iCon] << ' ' << gCon[MATRIX_INDEX(iCon, iState)] << ' ';
	// 		}
	// 		dataout << endl;
	// 	}
}
