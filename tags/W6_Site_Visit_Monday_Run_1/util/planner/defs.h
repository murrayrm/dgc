#ifndef _DEFS_H_
#define _DEFS_H_

#define IDX_THETA   0
#define IDX_DTHETA  1
#define IDX_DDTHETA 2
#define IDX_V       3
#define IDX_DV      4
#define IDX_DDV     5
#define IDX_SF      6
#define IDX_N       7
#define IDX_E       8
#define SIZE_ZP     9

// the scaling factors
/* #define SCALEFACTOR_DIST       (1.0) */
/* #define SCALEFACTOR_THETA      (1.0) */
/* #define SCALEFACTOR_ACCEL      (1.0) */
/* #define SCALEFACTOR_YAWDOT   (1.0) */
/* #define SCALEFACTOR_SPEED      (1.0) */
/* #define SCALEFACTOR_TANPHI   (1.0) */
/* #define SCALEFACTOR_PHIDOT   (1.0) */
/* #define SCALEFACTOR_ROLLOVER (1.0) */

#define SCALEFACTOR_DIST      (1.0 / 1.0)
#define SCALEFACTOR_THETA     (1.0 / M_PI)
#define SCALEFACTOR_ACCEL     (1.0 / VEHICLE_MAX_ACCEL)
#define SCALEFACTOR_YAWDOT    (1.0 / (M_PI / 10.0))
#define SCALEFACTOR_SPEED     (1.0 / MAXSPEED)
#define SCALEFACTOR_TANPHI    (1.0 / VEHICLE_MAX_TAN_AVG_STEER)
#define SCALEFACTOR_PHIDOT    (1.0 / (2.0 * MAX_PHI / MIN_WHEEL_RAIL_TO_RAIL_TIME))
#define SCALEFACTOR_ROLLOVER  (1.0 / (2.0 * 9.8 * VEHICLE_AVERAGE_TRACK / (2.0 * VEHICLE_H) * ROLLOVER_TWEAK))

#define MAX_NUM_POINTS        20

#define NUM_LIN_CONSTR        (NUM_LIN_INIT_CONSTR  + NUM_LIN_TRAJ_CONSTR  + NUM_LIN_FINL_CONSTR)
#define NUM_NLIN_CONSTR       (NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + NUM_NLIN_FINL_CONSTR)

#define NUM_COLLOCATION_POINTS  ((NUM_POINTS-1) * COLLOCATION_FACTOR + 1)
#define NUM_COLLOCATION_POINTS_FINE ((NUM_POINTS-1) * COLLOCATION_FACTOR_FINE + 1)
#define COLLOCATION_FACTOR_FINE 400


#define NPnumLinearConstr       ((NUM_LIN_INIT_CONSTR)  + (NUM_LIN_FINL_CONSTR)  + NUM_COLLOCATION_POINTS * (NUM_LIN_TRAJ_CONSTR))
#define NPnumNonLinearConstr    ((NUM_NLIN_INIT_CONSTR) + (NUM_NLIN_FINL_CONSTR) + NUM_COLLOCATION_POINTS * (NUM_NLIN_TRAJ_CONSTR))

#define MATRIX_INDEX(constr, i) ((constr) + (i)*(NPnumNonLinearConstr))








/* #define INTERPOLATION_SMOOTH */
/* #define INTERPOLATION_GAUSSIAN */
// INTERPOLATION_BILINEAR is the default



// this is for the INTERPOLATION_SMOOTH, INTERPOLATION_GAUSSIAN intepolation methods
#define MAP_INTERP_RAD        2
#define MAP_INTERP_DIAM       (MAP_INTERP_RAD + MAP_INTERP_RAD)
#define MAP_INTERP_EXPCOEFF   (log(0.001)/(2.0 * (double)(MAP_INTERP_RAD*MAP_INTERP_RAD)))




// these are for the INTERPOLATION_BILINEAR interpolation method
// how many CELLS wide the map interpolation window is. must be even
#define MAP_INTERPOLATION_DIAM  2
// how many CELLS wide the window to be blurred, interpolated is
#define MAP_BLURINTERP_DIAM_CELLS     (MAP_BLUR_KERNEL_DIAM + MAP_INTERPOLATION_DIAM - 1)
// how many DATA POINTS wide the window to be blurred, interpolated is. The
// cells are undersampled by a factor of MAP_BLURINTERP_STEP
#define MAP_BLURINTERP_DIAM_POINTS    ((MAP_BLURINTERP_DIAM_CELLS) / (MAP_BLURINTERP_STEP))





#define MIN_SF                (PLANNING_TARGET_DIST / 2.0)
#define MAX_SF                (PLANNING_TARGET_DIST * 2.0)

#define MIN_V                 0.05
#define MAXSPEED              5.0


#define BIGNUMBER             1.0e20

// This variable specifies the margin of error allowed for the nonlinear
// equality constraints. Specified as a fraction of the maximum value. So 0.1
// here will translate into a 10% of maximum acceleration allowed error on the
// initial acceleration term, for example
#define NONLINEAR_MARGIN      0.05

// if the seed says to move straight ahead, add this to the
// heading. this improves numerics
#define HEADINGCORRECTION 0.001
// if the seed turning radius is larger than this, set it to
// this. this improves numerics
#define MAXSEEDRADIUS 1000.0

#define CHECKDERIVATIVES_DELTA  (1e-7)

// max error is (-(ln 2)/(2*APPROXMIN_EXPFACTOR*ln(APPROXMIN_BASE)))
#define APPROXMIN_BASE            5.0
#define APPROXMIN_EXPFACTOR       5.0
#define OBSTACLE_AVOIDANCE_DELTA  0.5
#define OBSTACLE_AVOIDANCE_RAD    0
#define OBSTACLE_AVOIDANCE_DIAM   (OBSTACLE_AVOIDANCE_RAD*2 + 1)

#endif // _DEFS_H_
