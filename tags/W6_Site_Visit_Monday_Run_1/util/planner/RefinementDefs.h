// NUM_POINTS is the number of points in the spline
// COLLOCATION_FACTOR is the number of collocation points in each spline segment
// NUM_COLLOCATION_POINTS is the resulting total number of collocation points.
#define	NUM_POINTS_REFINEMENT								6
#define	COLLOCATION_FACTOR_REFINEMENT				10

#define PRINT_LEVEL_REFINEMENT						  0
#define USE_FINAL_ANGLE_CONSTRAINT_REFINEMENT	false

#define HALF_TARGET_ERROR_REFINEMENT							0.5
#define CARTESIAN_TARGET_SPECIFICATION_REFINEMENT	true
#define HALF_TARGET_WINDOW_REFINEMENT							0.5

#define PLANNING_TARGET_DIST_REFINEMENT						30.0

// how many constraints are ignored at the start of the trajectory
#define NUM_IGNORED_CONSTRAINTS_REFINEMENT				10

//  Which constraints are here
// Present: 1
// Absent:  0
#define VCONSTR					1
#define ACONSTR					1
#define PHICONSTR				1
#define PHIDCONSTR			1
#define ROLLOVERCONSTR	1

#define VCONSTR_IDX					(VCONSTR - 1)
#define ACONSTR_IDX					(VCONSTR_IDX + ACONSTR)
#define PHICONSTR_IDX				(ACONSTR_IDX + PHICONSTR)
#define PHIDCONSTR_IDX			(PHICONSTR_IDX + PHIDCONSTR)
#define ROLLOVERCONSTR_IDX	(PHIDCONSTR_IDX + ROLLOVERCONSTR)

#define MAX_PHI				VEHICLE_MAX_AVG_STEER
#define MAX_TAN_PHI		VEHICLE_MAX_TAN_AVG_STEER

// min time in which wheel can be turned from rail to rail
#define MIN_WHEEL_RAIL_TO_RAIL_TIME		2.0

// This is the tweak parameter for the roll-over constraint. Should be
// 1.0. Smaller values make the system more conservative and force a slower
// speed around obstacles.
#define ROLLOVER_TWEAK				0.1

#define SCALEFACTOR_COST ( MAXSPEED / PLANNING_TARGET_DIST_REFINEMENT / (NUM_POINTS-1) * 2.0)
//#define EXACT_COST

//#define SEED_MAXDECEL
//#define SEED_MAXACCEL
#define SEED_NOACCEL

#define USE_INITIAL_YAWRATE
/* #define USE_INITIAL_ACCEL */

// how many CELLS wide the map blurring kernel is. must be odd
#define MAP_BLUR_KERNEL_DIAM_REFINEMENT		3

// how many map cells per a single data point. used in the map blurring/interpolation
#define MAP_BLURINTERP_STEP_REFINEMENT    1
