#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

#include "CCorridorPainter.hh"
#include "CMapPlus.hh"

extern int EF_ALLOW_MALLOC_0=1;

int main(int argc, char *argv[])
{
	if(argc != 3)
	{
		cerr << "Usage: rddf2gnuplot input.rddf output.dat" << endl;
		return -1;
	}

	CMapPlus map;
	int i;

	double nhigh=-1e20, nlow=1e20, ehigh=-1e20, elow=1e20;
	RDDF rddf(argv[1]);
	if(rddf.getNumTargetPoints() < 1)
	{
		cerr << "something funky about the rddf. Maybe couldn't open file?" << endl;
		return -1;
	}

	for(i=0; i<rddf.getNumTargetPoints(); i++)
	{
		nhigh = fmax(rddf.getWaypointNorthing(i), nhigh);
		nlow  = fmin(rddf.getWaypointNorthing(i), nlow);
		ehigh = fmax(rddf.getWaypointEasting(i),  ehigh);
		elow  = fmin(rddf.getWaypointEasting(i),  elow);
	}
	nhigh += 10.0;
	nlow -= 10.0;
	ehigh += 10.0;
	elow -= 10.0;

	double res = 0.2;
	int nwidth = lround(floor((nhigh-nlow)/res));
	int ewidth = lround(floor((ehigh-elow)/res));
// 	CCorridorPainter painter;
// 	map.initMap(-1e6, -1e6, nwidth, ewidth, res, res, 0);
// 	int layer = map.addLayer<double>(-1.0, -1.01);
// 	painter.initPainter(&map, layer, &rddf);
// 	map.updateVehicleLoc((nhigh+nlow)/2.0, (ehigh+elow)/2.0);
// 	painter.paintChanges();

	map.initMap(-1e6, -1e6, 2500, 1000, res, res, 0);
	int layer = map.addLayer<double>(-3.0, -2.0);
 	map.loadLayer<double>(layer, "cmap", true);



	nlow = map.getWindowBottomLeftUTMNorthing();
	nhigh = map.getWindowTopRightUTMNorthing();
	elow = map.getWindowBottomLeftUTMEasting();
	ehigh = map.getWindowTopRightUTMEasting();

	ofstream fileout(argv[2]);
	if(!fileout)
	{
		cerr << "couldn't open output file" << endl;
		return -1;
	}
 	fileout << setprecision(10);
 	for(double n=nlow; n<=nhigh; n+=5.0*res)
 	{
 		for(double e=elow; e<=ehigh; e+=5.0*res)
 		{
 			if(map.getDataUTM<double>(layer, n, e) <= 0.5)
 				fileout << n << ' ' << e << endl;
 		}
 	}
 	return 0;
}
