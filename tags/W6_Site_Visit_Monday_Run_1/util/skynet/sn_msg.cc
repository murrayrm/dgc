#include "sn_msg.hh"
#include <netinet/in.h>
#include <cassert>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <sys/un.h>
#ifdef MACOSX
#include <sys/uio.h>
#endif

using namespace std;

skynet::skynet (modulename snname, int snkey)
{
	//fprintf(stderr, "skynet::skynet(): KEY = %d\n", snkey);
  key = snkey;
  name = snname;
  if(pthread_mutex_init(&listen_mutex, NULL) != 0)
  {
    fprintf(stderr,"Couldn't create mutex\n");
    exit(1);
  }
  mk_modcom_sock();
  snd_register();
}

skynet::skynet(const skynet &other)
{
  fprintf(stderr, "Error: module attempted to copy a skynet object\n");
  exit(1);
}

void skynet::snd_register()
{
  modcom_hdr reghdr;
  reghdr.name = name;
  reghdr.action = sn_register;
  reghdr.msglen = 0;               /* no ancillary data for registration */
  reghdr.aux_data.msg_type = (sn_msg)0;    /* initialize to appease valgrind */
  send_quant(modcom_sock, &reghdr, sizeof(reghdr), 0);
  recv_quant(modcom_sock, &id, sizeof(id), 0);
}

int skynet::get_large_sock(sn_msg type)
{
  char buf;
  modcom_hdr reghdr;
  reghdr.name = name;
  reghdr.action = get_lsock;
  reghdr.msglen = sizeof(type);
  reghdr.aux_data.msg_type = (sn_msg)0;    /* initialize to appease valgrind */
  if(pthread_mutex_lock(&listen_mutex) != 0)
  {
    fprintf(stderr, "Couldn't lock mutex\n");
  }
#if 0
  {
    char sendbuf[sizeof(reghdr) + sizeof(type)];
    (modcom_hdr)sendbuf = reghdr;
    *((sn_msg*)((char*)sendbuf+sizeof(modcom_hdr))) = type;
    send_quant(modcom_sock, &sendbuf, sizeof(sendbuf), 0);
    fprintf(stderr, "starting to send listen sends\n");
    fprintf(stderr, "finishing sending listen sends\n");
  }
#endif
  send_quant(modcom_sock, &reghdr, sizeof(reghdr), 0);
  send_quant(modcom_sock, &type, sizeof(type), 0);
  if (pthread_mutex_unlock(&listen_mutex) != 0)
  {
    fprintf(stderr, "Couldn't unlock mutex\n");
    exit(1);
  }
  /*now receive the socket*/
  int recvfd;                    /* new socket */
  struct msghdr msg;
  struct iovec iov[1];
  ssize_t n;
  union {
    struct cmsghdr cm;
    char control[CMSG_SPACE(sizeof(int))];
  } control_un;
  struct cmsghdr *cmptr;
                    
  msg.msg_control = control_un.control;
  msg.msg_controllen = sizeof(control_un.control);
  
  msg.msg_name = NULL;
  msg.msg_namelen = 0;
  
  iov[0].iov_base = &buf;
  iov[0].iov_len = 1;
  msg.msg_iov = iov;
  msg.msg_iovlen = 1;
  
  if ( (n = recvmsg(modcom_sock, &msg, 0)) <= 0)
  {
    perror("get_large_sock: recvmsg");
    exit(1);
  }
  if ((cmptr = CMSG_FIRSTHDR(&msg)) !=NULL && 
      cmptr->cmsg_len == CMSG_LEN(sizeof(int)))
  {
    if (cmptr->cmsg_level != SOL_SOCKET)
    {
      fprintf(stderr, "control level != SOL_SOCKET");
      exit(1);
    }
    if (cmptr->cmsg_type != SCM_RIGHTS)
    {
      fprintf(stderr, "control type != SCM_RIGHT");
      exit(1);
    }
    recvfd = *((int*) CMSG_DATA(cmptr));
  }
  else recvfd = -1;

  //printf("get_large_sock: recvfd= %d\n", recvfd);

  return recvfd;
}

uint32_t skynet::get_id()
{
  return id;
}

void skynet::mk_modcom_sock()
{
  char soc_filename[108];
  char str_key[20];
  strcpy(soc_filename, SOC_BASENAME);
  strcat(soc_filename, ".");
  sprintf(str_key, "%d", key);
  strcat(soc_filename, str_key);
  modcom_sock = socket(AF_UNIX, SOCK_STREAM, 0);
  if (-1 == modcom_sock)
  {
    perror("Are you sure you're running skynetd???\nmk_modcom_sock:socket");
		
    exit(1);
  }
  struct sockaddr_un addr;
  addr.sun_family = AF_UNIX;
  strcpy(addr.sun_path, soc_filename);
  if ( -1 == connect(modcom_sock, (struct sockaddr *)&addr, strlen(addr.sun_path) +
                     sizeof(addr.sun_family)))
  {
    perror("mk_modcom_sock:connect");
    exit(1);
  }
}  

skynet::~skynet()
{
  /* deregister with skynetd */
  shutdown(modcom_sock, SHUT_RDWR);
  close(modcom_sock);
}

int skynet::listen (sn_msg mytype, modulename somemodule)
{
  /* 1:   socket
     2:   setsockopt SO_REUSEADDR
     3:   setsockopt IP_ADD_MEMBERSHIP
     4:   bind
     5:   optional: connect, if you only want to listen to a certain modulename
     TODO: step 5 and ID discrimination will require skynetd and registration
     6:   if large messages are expected, ask skynetd to create a unix domain
          socket pair and send one file descriptor.
  */
  struct sockaddr_in localAddr;
  localAddr.sin_family = AF_INET;
  localAddr.sin_port = htons (dg_port);
  bzero (&localAddr.sin_zero, 8);

  if (get_dg_addr (mytype, &localAddr.sin_addr, key) != 0)
  {
    perror ("sn_listen: get_dg_addr");
    exit (1);
  }

  //debugging
  //printf("dg addr %u\n", localAddr.sin_addr.s_addr);


  int sockfd = socket (AF_INET, SOCK_DGRAM, 0);
  if (sockfd == -1)
  {
    perror ("sn_listen: socket");
    exit (1);
  }

  //Allow multiple sockets on same computer to listen to same port
  u_int yes = 1;
  if (setsockopt (sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof (yes)) < 0)
  {
    perror ("Reusing addr failed");
    exit (1);
  }

  struct ip_mreq my_mreq;
  my_mreq.imr_multiaddr = localAddr.sin_addr;
  my_mreq.imr_interface.s_addr = htonl (INADDR_ANY);
  if (setsockopt (sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (void *) &my_mreq,
                  sizeof (my_mreq)))
  {
    perror ("setsockopt");
    exit (1);
  }

  /* trying to bind to the mcast address instead of INADDR_ANY might not work.
     in that case, do localAddr.sin_addr.s_addr=htonl(INADDR_ANY)
  */

  if (bind (sockfd, (struct sockaddr *) &localAddr, sizeof (localAddr)) < 0)
  {
    printf ("%s: cannot bind port %d\n", inet_ntoa (localAddr.sin_addr),
            dg_port);
    exit (1);
  }

  /* TODO connect if you want to, to discriminate against sending modulename and ID */
  
  /*get a large receiving socket.  This should be optional*/
  int recvfd = get_large_sock(mytype);

  /*make the muxsock*/
  assert(sizeof(struct muxsock) == sizeof(int));
  struct muxsock mymuxsock;
  mymuxsock.mcast = (unsigned short int)sockfd;
  mymuxsock.aux.ux = (unsigned short int)recvfd;
  return *((int*)&mymuxsock);
}

int skynet::stream_listen(sn_msg mytype, modulename somemodule)
{
  short unsigned int port = get_stream_port(mytype, key);
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd == -1) {
    perror("socket");
    exit(1);
  }
  struct sockaddr_in my_addr ;
  my_addr.sin_family = AF_INET ;
  my_addr.sin_port = htons(port);
  my_addr.sin_addr.s_addr = INADDR_ANY ;
  bzero(&my_addr.sin_zero, 8);
              
  int bind_succeed =
    bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr));
  if (bind_succeed == -1) {
    perror("bind");
    exit(1);
  }
  /*set backlog to 1 since we only want one connection*/
  int listen_suceed = ::listen(sockfd, 2) ;    
  if ( listen_suceed == -1 ) {
    perror("listen");
    exit(1);
  }
  struct sockaddr_in their_addr ;
  /*int*/socklen_t sin_size = sizeof(struct sockaddr_in);
  int new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);
  if (new_fd == -1) {
    perror("accept");
    exit(1);
  }
  return new_fd;
}

size_t skynet::get_msg (int channel, void *mybuf, size_t bufsize, int options,
                        pthread_mutex_t* pMutex, bool bReleaseMutexWhenDone)
{
  //TODO: implement nonblocking calls
  
  struct muxsock mymuxsock = *((struct muxsock*)&channel);
  fd_set readfds;
  FD_ZERO(&readfds);
  FD_SET((int)mymuxsock.mcast, &readfds);
  FD_SET((int)mymuxsock.aux.ux, &readfds);
  int ret;
  int maxsock = (int)((mymuxsock.mcast > mymuxsock.aux.ux) ? 
                       mymuxsock.mcast : mymuxsock.aux.ux);
  int select_ret;
  do
  {
    select_ret = select(maxsock + 1, &readfds, NULL, NULL, NULL);
    //fprintf(stderr, "select returned %d\n", select_ret);
  }while (errno == EINTR);
  if(select_ret<0)
  {
    perror("get_msg:select()");
    exit(1);
  }
  if(pMutex != NULL)
  {
    if(pthread_mutex_lock(pMutex) != 0)
    {
      perror("Couldn't lock mutex in skynet::get_msg");
    }
  }
  if(FD_ISSET((int)mymuxsock.mcast, &readfds))
  {
    ret = recv((int)mymuxsock.mcast, mybuf, bufsize, 0);
  }
  else
  {
    assert(FD_ISSET((int)mymuxsock.aux.ux, &readfds));
    /*check the size of the message, then get it all*/
    unsigned int datalen;
    //TODO: verify that calling recv_quant twice in a row will be thread safe,
    //or else make it thread safe!
    recv_quant((int)mymuxsock.aux.ux, &datalen, sizeof(datalen), 0);
    datalen = ntohl(datalen);
    assert(bufsize >= datalen);
    recv_quant((int)mymuxsock.aux.ux, mybuf, datalen, 0);
    ret = datalen;
    //ret = recv((int)mymuxsock.aux.ux, mybuf, bufsize, 0);
  }
  if(pMutex != NULL && bReleaseMutexWhenDone)
  {
    if(pthread_mutex_unlock(pMutex) != 0)
    {
      perror("Couldn't unlock mutex in skynet::get_msg");
    }
  }
  return ret;
}

size_t skynet::stream_get_msg(int channel, void *mybuf, size_t bufsize,
    int options, pthread_mutex_t* pMutex, bool bReleaseMutexWhenDone)
{
  size_t ret;
  if(pMutex != NULL)
  {
    if(pthread_mutex_lock(pMutex) != 0)
    {
      perror("Couldn't lock mutex in skynet::get_msg");
    }
  }
  ret = recv(channel, mybuf, bufsize, options);
  if(pMutex != NULL && bReleaseMutexWhenDone)
  {
    if(pthread_mutex_unlock(pMutex) != 0)
    {
      perror("Couldn't unlock mutex in skynet::get_msg");
    }
  }
  return ret;
}

int skynet::get_send_sock (sn_msg type)
{
  /* 1:   socket
     2:   bind? it isn't neccesary.  i don't know what it gets you
     3:   connect
  */
  int sockfd;
  sockfd = socket (AF_INET, SOCK_DGRAM, 0);
  if (sockfd == -1)
  {
    perror ("socket");
    exit (1);
  }

  struct sockaddr_in multi_addr;
  multi_addr.sin_family = AF_INET;
  multi_addr.sin_port = htons (dg_port);
  //inet_aton(MADDR, &(multi_addr.sin_addr));
  bzero (&multi_addr.sin_zero, 8);
  if (get_dg_addr (type, &multi_addr.sin_addr, key) != 0)
  {
    perror ("sn_get_send_sock: get_dg_addr");
    exit (1);
  }
  //printf("dg addr %u\n", multi_addr.sin_addr.s_addr);

  int connect_suceed = connect (sockfd, (struct sockaddr *) &multi_addr,
                                sizeof (struct sockaddr));
  if (connect_suceed == -1)
  {
    perror ("connect");
    exit (1);
  }

  muxsock mymuxsock;
  mymuxsock.mcast = sockfd;
  mymuxsock.aux.type = type;

  return *((int*)&mymuxsock);
}

int skynet::get_stream_sock(sn_msg type, struct in_addr* remote_addr)
{
  int sockfd = socket (AF_INET, SOCK_STREAM, 0);
  if (sockfd == -1)
  {
    perror ("socket");
    exit (1);
  }
  short unsigned int port = get_stream_port(type, key);
  struct sockaddr_in rem_addr;
  rem_addr.sin_family = AF_INET ;
  rem_addr.sin_port = htons(port);
  rem_addr.sin_addr = *remote_addr ;
  bzero(&rem_addr.sin_zero, 8);
  
  int connect_suceed =
    connect(sockfd, (struct sockaddr*)&rem_addr, sizeof(struct sockaddr));
  if ( connect_suceed == -1 ) {
    perror("connect");
    exit(1);
  }
  return sockfd;
}

size_t skynet::send_msg (int channel, void *msg, size_t msgsize, int options,
                         pthread_mutex_t* pMutex, bool bReleaseMutexWhenDone)
{
  //TODO: implement options
  ssize_t bytes_sent;
  struct muxsock mymuxsock = *((struct muxsock*)&channel);
  int mcastsock = mymuxsock.mcast;
  sn_msg type = (sn_msg)mymuxsock.aux.type;
  assert(last_type < 65536);
  if(pMutex != NULL)
  {
    if(pthread_mutex_lock(pMutex) != 0)
    {
      perror("Couldn't lock mutex in skynet::get_msg");
    }
  }
  if(msgsize > MAX_PKT_SIZE)
  {
    //need to send a large message
    modcom_hdr reghdr;
    reghdr.name = name;
    reghdr.action = send_reliable;
    reghdr.msglen = msgsize;
    reghdr.aux_data.msg_type = type;
    //TODO:  eliminate need for buffer by using writev
    void* buffer = malloc(msgsize + sizeof(reghdr));
    memcpy(buffer, &reghdr, sizeof(reghdr));
    memcpy((char*)buffer+sizeof(reghdr), msg, msgsize);
    bytes_sent = msgsize;
    send_quant(modcom_sock, buffer, sizeof(reghdr) + bytes_sent, 0);
    free(buffer);
  }
  else
  {
    //can send a small message
    bytes_sent = send(mcastsock, msg, msgsize, 0);
  }
  if(pMutex != NULL && bReleaseMutexWhenDone)
  {
    if(pthread_mutex_unlock(pMutex) != 0)
    {
      perror("Couldn't unlock mutex in skynet::get_msg");
    }
  }
  return bytes_sent;
}

size_t skynet::stream_send_msg (int channel, void *msg, size_t msgsize, 
        int options, pthread_mutex_t* pMutex, bool bReleaseMutexWhenDone)
{
  size_t bytes_sent;
  if(pMutex != NULL)
  {
    if(pthread_mutex_lock(pMutex) != 0)
    {
      perror("Couldn't lock mutex in skynet::get_msg");
    }
  }
  bytes_sent = send(channel, msg, msgsize, options);
  if(pMutex != NULL && bReleaseMutexWhenDone)
  {
    if(pthread_mutex_unlock(pMutex) != 0)
    {
      perror("Couldn't unlock mutex in skynet::get_msg");
    }
  }
  return bytes_sent;
}

/* these functions have yet to be implemented */
int skynet::heartbeat() { return 0; }
int skynet::pause() { return 0; }
