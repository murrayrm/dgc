#ifndef SN_TYPES_H
#define SN_TYPES_H

#ifdef MACOSX
#include <sys/types.h>
typedef u_int32_t uint32_t; 
#endif

#include <netinet/in.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#define SOC_BASENAME "/tmp/skynetd"  /* TODO: give absolute pathname */
#define NUM_RECIPS 20   //maximum recipients of a reliable multicast message
#define MTU       1500  //number of bytes in a packet
#define EFF_MTU 1500 - sizeof(uint32_t)*3 //number of bytes in payload
#define MAX_PKT_SIZE    1024    /*size of data not including header.  */

/** note: many of these addresses are claimed for specific services.  Sending
   messages with TTL > 1 may hose some of these services and piss people off.
   Conversely, if our router is configured to forward multicast traffic from
   outside, it will piss us off
 */
 #define MIN_ADDR "224.0.2.0"
 #define MAX_ADDR "238.255.255.255"
 #define STREAM_MIN_PORT 4096
 #define STREAM_MAX_PORT 65000
 #if 0
 const int MIN_PORT = 1024;  /* can be opened by non-priveleged process */
 const int MAX_PORT = 65535;
 #endif


 #define dg_port  38475 //random

typedef enum 
{
  SNpointcloud, /** Document me! */
  SNdeltamap,   /**< Document me! */
  SNdrivecmd,   /** This is the message type to send commands to adrive.
                  * The structs is in adrive_skynet_interface_types.h */
  SNdrivecom,   /**This is the message type for returns from adrive to modules.
                 * The struct is in adrive_skynet_interface_types.h */
	SNactuatorstate, // the state of all of the actuators
  SNmodlist,     /** chirps which modules are registered */
  skynetcom,    /** Skynet internal use only */
  rmulti,       /** Skynet internal use only */
  SNtraj,
  SNstate,
  SNgetmetastate,
  SNmetastate,
  SNGuiMsg, /** Feedback to the gui containing 256 char mesg**/
  SNmodcom,  /** comma separated stringg containing commands to be started 
                 and options*/
  SNRDDFtraj,  /** a hack until skynet get_msg knows which module to get
		   message from: traj's sent by RddfPathGen **/
  SNplannertraj,  /** a hack until skynet get_msg knows which module to get
                      message from: traj's sent by PlannerModule **/
  SNmodemantraj,  /** a hack until skynet get_msg knows which module to get
                      message from: traj's sent by ModeManModule **/
	SNtrajPlannerSeed, /** a debugging traj sent by the planner.
												 this is the solver seed */
	SNtrajPlannerInterm, /** a debugging traj sent by the planner.
												 this is the result of the spatial stage */
  SNreactiveTraj,
  SNfusiondeltamap,
  SNladardeltamap,
  SNstereodeltamap,
  SNstaticdeltamap,
  SNroadboundary,
  SNloggerstring,  /** a char (probably of length 256) for use in sending logger info */
  SNmark1,       /** used only for benchmarking*/
  SNmark2,       /** used only for benchmarking*/
  SNladardeltamap_roof,
  SNladardeltamap_bumper,
  last_type /**< do not ever use last_type, it is just a hack to figure out how 
                 many types of msg there are. */
} 
sn_msg;       /** This enum defines all the message types.  Message types should
                * begin with "SN" */


typedef enum
{
  sn_register, add_global, write_global, read_global, listen_global, 
  unlisten_global, remove_global, change_global,
  send_reliable, get_lsock
}
sn_action;

typedef enum 
{
  SNMapDisplay, SNmodstart, SNstereo, SNladar, SNfusionmapper, SNplanner,
  SNadrive, SNadrive_commander, SNadrivelogger, SNGuiAdrive, SNGuimodstart,
  SNasim, SNtrajfollower, SNastate, SNastate_test, SNRddfPathGen, SNModeMan,
  SNtrajtalkertestsend, SNtrajtalkertestrecv, SNladarfeeder, SNroadfinding, 
  MODmark, MODLogger, MODLoggerClient, MODstereofeeder, MODstaticpainter,
  MODreactive, 
  MODladarfeeder_roof,
  MODladarfeeder_bumper,
  ALLMODULES
} 
modulename;   /** This enum defines module names.  To distinguish the constants from
                * message types, module names should start with "MOD" */

typedef struct  /*for communication with skynetd*/
{
  modulename  name;
  sn_action   action;
  int         msglen;
  union{
    sn_msg msg_type;      /*used for send_reliable exchanges*/
  } aux_data;
}
modcom_hdr;

typedef struct            /* basically sn_module without list linking */
{
  uint32_t  id;
  uint32_t  location;
  modulename name;
} ml_elem; 

/* I know they're not types, but these two functions are used by both skynetd
 * and clients */
#ifdef __cplusplus
extern "C" {
#endif
  int recv_quant(int s, void* buf, ssize_t len, int flags);

  int send_quant(int s, void* buf, ssize_t len, int flags);

/** get_dg_addr figures out what multicast address to use for a
 * datagram-like connection.  With multicast, this is a simple
 * function.  Without, we would probably have to consult the SkyNet
 * daemon.  Also, I don't know if multicast is efficient for
 * intracomputer communications.  If not, we may want to use unix
 * sockets for that instead.  Using sn_key from the outer scope may not
 * work if this function is implemented within a dynamically linked
 * library.
 */
  int get_dg_addr(sn_msg type, struct in_addr* my_addr, int key);

/** get_stream_port computes port numbers for tcp connections.  Return value
 *  is the port to use in host byte order. */
  short unsigned int get_stream_port(sn_msg type, int key);

/** SNgettime is a direct copy of DGCgettime.  But I can't use DGCgettime
 *  in skynetd because it's defined in a C++ file
 */
  void SNgettime(long long * DGCtime);
  #ifdef __cplusplus
}
#endif

/* reliable_hdr goes in front of every reliable multicast message.  The sender
 * must send this structure, then send the data.  This won't be set up by the
 * end user of skynet, but by the messaging layer attached to users' modules.
 */

typedef struct {
  uint32_t recip_list[NUM_RECIPS];
} reliable_hdr ;


#endif

