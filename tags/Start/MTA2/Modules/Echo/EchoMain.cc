#include "Modules.hh"
#include "Echo.hh"
#include "Kernel.hh"

#include <boost/shared_ptr.hpp>

using namespace boost;

Kernel K;
Echo DGCM(MODULES::Echo, "Echo", 0);

int main() {

  //  K.Register(shared_ptr<DGC_MODULE>(new Echo(MODULES::Echo, "Echo Server", 0)));
  K.Register();
  K.Start();

  return 0;
}
