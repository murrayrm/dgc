#include "Echo.hh"

#include "Kernel.hh"
#include <time.h>
#include <iostream>

using namespace std;

extern Kernel K;
EchoDatum D;


void Echo::Init() {

  // do your init in here
  // i.e. open serial ports etc,

  cout << ModuleName() << ": Init" << endl;

  // go to active state after initialization
  SetNextState(STATES::ACTIVE);
}



void Echo::Active() {
  while( ContinueInState() ) {  // while stay active
    sleep(1); 
  }
} 


void Echo::Standby() {
  while( ContinueInState() ) {  // while stay in standby
    sleep(1); 
  }
} 


void Echo::Shutdown() {
  // do whatever closing of files, etc
  cout << ModuleName() << ": Shutdown" << endl;

}

void Echo::Restart() {
  // you may want to do something better here, but i'll just call shutdown and init
  cout << ModuleName() <<": Restart" << endl;
  Shutdown();
  Init();
}

string Echo::Status() {
  string x = ModuleName() + ": is in state ";
  switch ( GetCurrentState() ) {
  case STATES::INIT:
    x+= "Init";       break;
  case STATES::ACTIVE:
    x+= "Active";     break;
  case STATES::SHUTDOWN:
    x+= "Shutdown";   break;
  case STATES::STANDBY:
    x+= "Standby";    break;
  case STATES::RESTART:
    x+= "Restart";    break;
  default:
    x+= "Unknown state - (Something is very wrong)";
  }
  return x;
}
 
void Echo::InMailHandler(Mail& ml) {
  cout << "InMail Received" << endl;

  int size = ml.Size();
  char *p = new char[ size ];
  // download the message locally
  ml.DequeueFrom( (void*) p, size );
  Mail reply = Mail(ml.To(), ml.From(), ml.MsgType(), ml.Flags() );
  // now put it back in a new pipe
  reply.QueueTo((void*) p, size);
  delete p;
  // and send it on its way
  K.SendMail(reply);
}


Mail Echo::QueryMailHandler(Mail& ml) {
  cout << "QueryMail Received" << endl;

  int size = ml.Size();
  char *p = new char[ size ];
  ml.DequeueFrom( (void*) p, size );
  Mail reply = Mail(ml.To(), ml.From(), ml.MsgType(), ml.Flags());
  reply.QueueTo((void*) p, size);
  delete p;
  return reply;
}

