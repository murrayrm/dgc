#include "Modules.hh"
#include "TestModule.hh"
#include "Kernel.hh"

#include <boost/shared_ptr.hpp>

using namespace boost;

Kernel K;
TestModule DGCM(MODULES::TestModule, "Test Module", 0);

int main() {

  K.Register();
  //  K.Register(shared_ptr<DGC_MODULE>(new TestModule(MODULES::TestModule, "Test Module", 0)));

  K.Start();

  return 0;
}
