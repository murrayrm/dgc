#ifndef TestModule_HH
#define TestModule_HH

#include "DGC_MODULE.hh"
#include "TestModuleDatum.hh"


typedef DGC_MODULE TestModule;

using namespace std;
using namespace boost;

// state machine states enumerated
// defined in DGC_MODULE.hh

// namespace STATES {
//   enum { INIT, ACTIVE, STANDBY, SHUTDOWN, RESTART, NUM_STATES };
// };

// enumerate all module messages that could be received
namespace TestModuleMessages {
  enum { // in Messages first (messages taken in without responding
         REINIT,   // something where we would invoke reinit state
	 SHUTDOWN, // maybe another process wants us to shutdown
	 PAUSE,    // go to standby state
	 GO,       // go to active state
	 SETCOUNTER, // sets the counter to what is in the message
	 
	 COUNTUP,   // my test module will have a counter that other 
	 COUNTDOWN, // modules can tell to count up or down

	 // now query messages (messages we receive that we respond to)
	 GETCOUNTER, // gets the state of the counter
	 GETSTATE,   // gets what state we are in (i.e. init, active, ...)
	
	 // And a place holder
	 NumMessages
  };
};



#endif
