#ifndef Timeval_HH
#define Timeval_HH

#include <time.h>
#include <sys/time.h>


class Timeval {
public:
  Timeval(long seconds, long microseconds);
  Timeval(const timeval & tv);
  Timeval(const Timeval & tv);

  Timeval operator - (const Timeval &rhs);

private:
  timeval myTV;

};

Timeval TVNow();

#endif
