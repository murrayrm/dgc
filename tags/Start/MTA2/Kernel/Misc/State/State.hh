#ifndef State_HH
#define State_HH

#include <string>

struct State {
  std::string Name;
  std::string Description;
  int         ID;
  void (*f) ();
  unsigned int Flags;
};

namespace StateFlags {
  int QuitAfter = 1; // after exiting this state, shutdown

};


#endif
