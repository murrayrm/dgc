#include "misc/Error/ErrorContext.hh"
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include <map>
#include <string>
#include <vector>
#include <sttdef.h>

ErrorCodes::ErrorCodes() {
  counter = 1;
}

ErrorCodes::~ErrorCodes() {

}

int ErrorCodes::add(std::string str) {
  int r = ints[str] == 0;
  if( r==0 ) {
    // add
    ints[str] = nextEC();
    strings[ints[str]] = str;
  }
  return r;
}

int ErrorCodes::nextEC() {

  boost::recursive_mutex::scoped_lock sl(mutex);
  return counter++;
}

