#include "SharedMemoryContainer.hh"
#include <map>




std::map<char*, int> allocated;

SharedMemoryContainer::SharedMemoryContainer() {

  myPtr = NULL;
  mySize = 0;
  myL = NULL;
  myR = NULL;

  insert();
}

SharedMemoryContainer::SharedMemoryContainer( char * ptr, int sz) {

  myPtr = ptr;
  mySize = sz;
  myL = myPtr;
  myR = myPtr;

  insert();
}

SharedMemoryContainer::SharedMemoryContainer(const SharedMemoryContainer & m) {

  myPtr = m.myPtr;
  mySize = m.mySize;
  myL = myPtr;
  myR = myPtr;

  insert();
}


SharedMemoryContainer::~SharedMemoryContainer() {
  destruct();
}

void SharedMemoryContainer::destruct() {

  allocated[myPtr]--;
  if( allocated[myPtr] == 0 ) {
    delete myPtr;
  }
}

void SharedMemoryContainer::insert() {
  
  if( myPtr != NULL ) {
      allocated[myPtr]++;
  }
}

SharedMemoryContainer& 
  SharedMemoryContainer::operator=(const SharedMemoryContainer & m) {

  destruct();

  myPtr = m.myPtr;
  mySize = m.mySize;
  myL = m.myL;
  myR = m.myR;

  insert();
}



char* SharedMemoryContainer::sliderL() {
  return myL;
}

char* SharedMemoryContainer::sliderR() {
  return myR;
}

char* SharedMemoryContainer::ptr() {
  return myPtr;
}

char* SharedMemoryContainer::read(int r) {
  myL += r;
  return myL;
}

char* SharedMemoryContainer::wrote(int r) {
  myR += r;
  return myR;
}

int SharedMemoryContainer::size() {
  return mySize;
}


int SharedMemoryContainer::leftToWrite() {
  return mySize+myPtr-myR;
}


int SharedMemoryContainer::leftToRead() {
  return myR-myL;
}
