#ifndef SharedMemoryContainer_HH
#define SharedMemoryContainer_HH

class SharedMemoryContainer {
public:
  SharedMemoryContainer();
  SharedMemoryContainer(char * ptr, int sz);
  SharedMemoryContainer(const SharedMemoryContainer& m);
  ~SharedMemoryContainer();

  char* sliderL();
  char* sliderR();
  char* read(int r);
  char* wrote(int l);
  char* ptr();
  int   size();

  int leftToRead();
  int leftToWrite();

  SharedMemoryContainer& operator= (const SharedMemoryContainer & rhs); 

private:
    
  void destruct();
  void insert();

  char* myPtr;
  char* myL;
  char* myR;
  int   mySize;

};





#endif
