#ifndef Socket_HH
#define Socket_HH

#include <string>
#include <unistd.h>
#include <fcntl.h>
#include "../File/File.hh"

unsigned int IPConvert(char* ip);

void FlushWrite(int FD);

class CSocket : public File {
public:
  CSocket();
  CSocket(unsigned int ip, int port);
  CSocket(const CSocket& c);
  ~CSocket();                       // close the file on destruction

  int Open(unsigned int ip, int port);
  /* Inherited from FILE */
  //  int OTRead (void* buffer, int bytes); 
  //  int OTWrite(void* buffer, int bytes); 
  //  int BRead  (void* buffer, int bytes, double timeout=0.1); 
  //  int BWrite (void* buffer, int bytes, double timeout=0.1); 

private:
    
  /* Inherited from FILE */
  //  shared_ptr<FileHandle> myFH;
};
 

class SSocket: public File {
public:
  SSocket();
  ~SSocket();

  int Listen(int port);
  File Accept();
  int Port();

private:
  int M_port;
};




#endif
