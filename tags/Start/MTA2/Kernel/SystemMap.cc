#include "SystemMap.hh"

#include <iomanip>
#include <fstream>
#include <iostream>
#include "Kernel.hh"
#include <time.h>

extern Kernel K;
extern DGC_MODULE DGCM;

KERNEL::SystemMap KD_SystemMap("");

void SYS_MAP_STARTER() {
  KD_SystemMap();
}

using namespace std;
using namespace boost;
boost::recursive_mutex   M_TableOfModulesLock;

namespace KERNEL {
  SystemMap::SystemMap(string textFile) {
    M_TCP.IP = IPConvert("131.215.89.150");
    
    M_TableOfComputers["OLD GRANDCHALLENGE"] = IPConvert("131.215.89.150");
    M_TableOfComputers["NEW GRANDCHALLENGE"] = IPConvert("131.215.133.164");
  }
  
  SystemMap::~SystemMap() {
  }
  
  TCP_ADDRESS SystemMap::LocalAddress() {
    // the IP was set by constructor
    // but need to grab PORT from socket server
    M_TCP.PORT = K.KD_InMessageAcceptor.ServerPort();
    return M_TCP;
  }
  
  MailAddress SystemMap::FindAddress(string moduleName) {
    return FindAddress(M_NameToType[moduleName]);
  }
  
  MailAddress SystemMap::FindAddress(int ModuleType) {
    
    recursive_mutex::scoped_lock  sl (M_TableOfModulesLock);
    // find the module in the table 
    list<ModuleInfo>::iterator x    = M_TableOfModules.begin();
    list<ModuleInfo>::iterator stop = M_TableOfModules.end();
    
    while( x!= stop) {
      if( (*x).ModuleType == ModuleType) {
	// found, return address
	return (*x).MA;
      }
      //      cout << "Find Address Looking for " << ModuleType
      //	   << ", but " << (*x).ModuleType 
      //	   << " is not it." << endl;
      x++;
    }
    
    cout << "Cannot Find Module of Type " << ModuleType << endl;
    cout << "Size of M_TableOfModules = " << M_TableOfModules.size() << endl;
    K.PrintModuleList(M_TableOfModules);
    cout << this;
    // else not found, return "zero"
    MailAddress ret;
    ret.TCP.IP   =  0;
    ret.TCP.PORT =  0;
    ret.MAILBOX  =  -1;

    return ret;
  }
  
  list<ModuleInfo> SystemMap::LocalModuleTypes() {
    list<ModuleInfo> lst;
    ModuleInfo MI;
    MI.MA.TCP = LocalAddress();
    /*    // for each module, need to fill in mailbox and module type

    vector<shared_ptr <ModuleHandler> >::iterator x    = (K.KD_Modules).begin();
    vector<shared_ptr <ModuleHandler> >::iterator stop = (K.KD_Modules).end();
    while( x != stop) {
      MI.MA = (*((*(*x)).p_dm)).MyAddress();
      MI.ModuleType = (*((*(*x)).p_dm)).ModuleType();
      lst.push_back( MI );
      x++;
    }
    //    cout << "SystemMap:LocalModules = " << lst.size() << endl;
    */
    MI.ModuleType = DGCM.ModuleType();
    MI.MA.MAILBOX = DGCM.ModuleNumber();
    lst.push_back(MI);
    return lst;
  }
  
  list<ModuleInfo> SystemMap::AllModuleTypes() {
    recursive_mutex::scoped_lock sl(M_TableOfModulesLock);
    return M_TableOfModules;
  }

  void SystemMap::operator()() {

    // shoud be filled in
    while( !K.ShuttingDown() ) {
      map<string, IP_ADDRESS>::iterator x    = M_TableOfComputers.begin();
      map<string, IP_ADDRESS>::iterator stop = M_TableOfComputers.end();

      list<ModuleInfo> NewModuleTable;
      list<ModuleInfo> TmpModuleTable;
      list<ModuleInfo>::iterator pos;

      while( x != stop ) {
	TmpModuleTable = PortScanIP( (*x).second );
	if( TmpModuleTable.size() > 0) {
	  pos = NewModuleTable.end();
	  NewModuleTable.splice(pos, TmpModuleTable);
	}
	x++;

      }

      if( true) {
	recursive_mutex::scoped_lock sl(M_TableOfModulesLock);
	M_TableOfModules = NewModuleTable;
	//	cout << "Found " << NewModuleTable.size() << " modules." << endl;
      }

      //      cout << "Finished Port Scanning" << endl;
      //      cout << "Found " << M_TableOfModules.size() << " modules." << endl;
      //      cout << this << endl;
      K.PrintModuleList(M_TableOfModules);
      sleep(10);
    }
  }

  list<ModuleInfo> SystemMap::PortScanIP(IP_ADDRESS ip) {
    int i;

    TCP_ADDRESS myAddress = LocalAddress();
    MailAddress KernelAddress;

    KernelAddress.TCP = myAddress;
    KernelAddress.MAILBOX = 0;

    //    cout << "myAddress IP = " << myAddress.IP
    //	 << ", Port= " << myAddress.PORT 
    //	 << ", Trying to scan IP = " << ip << endl;

    list<ModuleInfo> ret;
    for(i=DEF::MinPort; i<(DEF::MinPort + DEF::NumPorts); i++) {

      // do not scan this kernel via sockets, use datum
      if( ip == myAddress.IP && i == myAddress.PORT ) {

	list<ModuleInfo>::iterator pos = ret.end();
	list<ModuleInfo> local = LocalModuleTypes();

	//	cout << "Grabbed " << local.size() << " Local Modules" << endl;
	ret.splice(pos, local);
	continue;
      }

      // otherwise, do a remote scan
      CSocket sock(ip, i); // port is i
      if(sock.FD() < 0) { 
	//	cout << "PORT " << i << " failed to connect." << endl;
	// connection error (probably just no module there)
	// just skip over
	continue;
      }
      
      // we are all good
      // send a query message to ask for remote modules
      MailAddress remoteMailbox;
      remoteMailbox.TCP.IP = ip;
      remoteMailbox.TCP.PORT = i;
      remoteMailbox.MAILBOX = 0; // send to the kernel
      Mail m(KernelAddress, remoteMailbox, KernelMessages::MODULEINFO, MailFlags::QueryMessage);
      
      Mail returnMail = K.SendQuery(m, sock);
      
      if( returnMail.OK() ) {
	//	cout << "returnMail is OK" << endl;
	ModuleInfo MI;
	while(returnMail.Size() >= sizeof(ModuleInfo)) {
	  returnMail.DequeueFrom( (void*) &MI, sizeof(MI));
	  ret.push_back(MI);
	}
      } else cout << "SystemMap:PortScan: Return Mail Failed" << endl;

    }
    return ret;
  }

};
