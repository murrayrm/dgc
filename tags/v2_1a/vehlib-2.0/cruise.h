#ifndef _CRUISE_H_
#define _CRUISE_H_

// Cruise Controller
// version 1.1
// 11/13/03 H Huang
// File creation
// 11/14/03 H Huang
// Added lookup control
// 2/27/2004 J Raycroft
// Rewrote to work with new cruise.
#include "LUtable.h"

//#define CRUISE_OLD

#ifndef CRUISE_OLD
double cruise(double commanded_vel, double fake1, double fake2);
double cruise_terrain();
double cruise_PID(double vel_command);
#endif

#ifdef CRUISE_OLD
double cruise(double vdesired, double vcurrent, double acurrent);
double delta_throttle(double vcurrent, double vdesired);
double Integral_Control(double vcurrent, double vdesired, double current_throttle);
#endif
  
void cruise_init();
void cruise_zero();
double Accel_Lookup(double v_steady);
double Throttle_Lookup(double acceleration);
double Ctr_Throttle_Lookup(double control);
double Vref_Throttle_Lookup(double velRef);

#endif
