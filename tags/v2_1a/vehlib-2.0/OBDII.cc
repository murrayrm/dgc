#include <string>
#include "serial.h"
#include <iostream>

#include "OBDII.h"

using namespace std;

//3 procedures:  OBD_Init, OBD_Speed, OBD_RPM

//OBD Serial Constants
int OBDPort;
const int OBDBaud = 19200;
const double timeout = .1;		// in seconds

const int Timeout = (int) (timeout * 1000000.0);   // timeout in microseconds

//OBD Checking Constants
const char Info_Ret_Correct= 0x1;
const int HW_Instr_Chk_Pos = 2;
const unsigned char Ret_Ctrl_Byte = 0xC0;
const int Seq_Chk_Pos = 8;
const int Req_Length = 10; 			//Same for all commands using Autotap hardware

//  NEVER change these arrays (commands, etc)
//  OBD Initialization Constants
//  NOTE:  OBD_Init_Length is used for both the Sequence and Check b/c they are the same length
unsigned char OBD_Init_Seq[6] = {0x01, 0x02, 0x23, 0x01, 0x00, 0x27};
unsigned char OBD_Init_Chk[6] = {0x01, 0x02, 0xA3, 0x01, 0x0,  0xA7};
const int OBD_Init_Length = 6;

//OBD RPM sensor Constants
unsigned char RPM_Req_Seq[10] = {0x01, 0x01, 0x40, 0x05, 0x68, 0x6A, 0xF1, 0x01, 0x0C, 0x17};
const int RPM_Resp_Length = 12;

//OBD Speed sensor Constants
unsigned char Spd_Req_Seq[10] = {0x01, 0x01, 0x40, 0x05, 0x68, 0x6A, 0xF1, 0x01, 0x0D, 0x18};
const int Spd_Resp_Length = 11;
const float TireComp = 33.0 / 31.0;

//OBD Engine Coolant Temperatur sensor Constants
unsigned char Coolant_Temp_Req_Seq[10] = {0x01, 0x01, 0x40, 0x05, 0x68, 0x6A, 0xF1, 0x01, 0x05, 0x10};
const int Coolant_Resp_Length = 11;

//OBD Intake Manifold Absolute Pressure Constants
unsigned char Intake_Manifold_Pressure_Req_Seq[10] = {0x01, 0x01, 0x40, 0x05, 0x68, 0x6A, 0xF1, 0x01, 0x0B, 0x16};
const int Manifold_Pressure_Resp_Length = 11;

//OBD Ignition Timing Advance for #1 Cylinder Constants
unsigned char Ignition_Timing_Adv_Req_Seq[10] = {0x01, 0x01, 0x40, 0x05, 0x68, 0x6A, 0xF1, 0x01, 0x0E, 0x19};
const int Ignition_Adv_Resp_Length = 11;

//OBD Intake Air Temperature Constants
unsigned char Intake_Air_Temp_Req_Seq[10] = {0x01, 0x01, 0x40, 0x05, 0x68, 0x6A, 0xF1, 0x01, 0x0F, 0x1A};
const int Intake_Air_Temp_Resp_Length = 11;

//OBD AirFlow Rate from Mass Air Flow Sensor Constants
unsigned char AirFlow_Rate_Req_Seq[10] = {0x01, 0x01, 0x40, 0x05, 0x68, 0x6A, 0xF1, 0x01, 0x10, 0x1B};
unsigned int AirFlow_Rate_Resp_Length = 12;

//OBD Absolute Trottle Position Constants
unsigned char Throttle_Pos_Req_Seq[10] = {0x01, 0x01, 0x40, 0x05, 0x68, 0x6A, 0xF1, 0x01, 0x11, 0x1C};
const int Throttle_Pos_Resp_Length = 11;



/*PreCondition:	AutoTap hardware must be plugged in
			Vehicle must be turned on
 PostCondition:	Will initialize the OBDII*/
int OBD_Init(int port){
  OBDPort = port;
  int len;
  int count = 1;
  
  unsigned char Init_Buffer[OBD_Init_Length];
  bool OBD_Init_Flag = false;

  if(serial_open(OBDPort, B19200) != -1) {
    cout << "Serial Port Opened." << endl;
  }
  
  do{
    serial_clean_buffer(OBDPort);
#   ifdef SERIAL2
    serial_write(OBDPort, (char *) OBD_Init_Seq, OBD_Init_Length);
#   else
    serial_write(OBDPort, (char *) OBD_Init_Seq, OBD_Init_Length, SerialBlock);
#   endif

#   ifdef SERIAL2
    if ((len = serial_read_blocking(OBDPort, (char *) Init_Buffer, OBD_Init_Length, Timeval(0, Timeout))) != OBD_Init_Length) {
#   else
    if ((len = serial_read_blocking(OBDPort, (char *) Init_Buffer, OBD_Init_Length, timeout)) != OBD_Init_Length) {
#   endif
//      cout << "OBDII: unmatching Init response: len = " << len << endl;
    }
													      
   
    OBD_Init_Flag = true;
    
    for(int i = 0; i < OBD_Init_Length; ++i){
      if(Init_Buffer[i] != OBD_Init_Chk[i])
        OBD_Init_Flag = false;
    }

    if(!OBD_Init_Flag) {
      usleep(100000);
    }
    count++;
  } while((!OBD_Init_Flag) && (count <= OBD_INIT_WAIT));

      return OBD_Init_Flag;

}


/*PreCondition:	Must have been called
 PostCondition:	Will return the RPM in float form*/
float Get_RPM(){
  unsigned char RPM_Buffer[RPM_Resp_Length];
  float RPM = 1;
  int len;
  serial_clean_buffer(OBDPort);
  
# ifdef SERIAL2
  serial_write(OBDPort, (char *) RPM_Req_Seq, Req_Length);
# else
  serial_write(OBDPort, (char *) RPM_Req_Seq, Req_Length, SerialBlock);
# endif
 
/*  //////////Returned Value Test (Printed to Screen)
      while(1) {
        length = 0;
        while(length == 0) {
            length = serial_read(OBDPort, buffer, 1);
            if(length>0) {
                printf("%X\n", *buffer);
            }
        }
    }
 */ 

#   ifdef SERIAL2
    if (len =serial_read_blocking(OBDPort, (char *) RPM_Buffer, RPM_Resp_Length, Timeval(0, Timeout)) != RPM_Resp_Length) {
#   else
    if (len =serial_read_blocking(OBDPort, (char *) RPM_Buffer, RPM_Resp_Length, timeout) != RPM_Resp_Length) {
#   endif
//      cout << "OBDII: unmatching RPM response length: len = " << len << endl;
     return -1;
    }

    if ((RPM_Buffer[HW_Instr_Chk_Pos] != Ret_Ctrl_Byte) ||
	(RPM_Buffer[Seq_Chk_Pos] != RPM_Req_Seq[Seq_Chk_Pos])){
//      cout<< "OBDII: Error reading back RPM value" << endl;
        return -1;
    }

    RPM = (( (int) RPM_Buffer[9])  * 256 + RPM_Buffer[10]) / 4;
  
  return RPM;
}

/*PreCondition:	Must have been called
 PostCondition:	Will return the Speed in m/s float form*/
float Get_Speed(){
  unsigned char Spd_Buffer[Spd_Resp_Length];
  float Speed = 1;

  serial_clean_buffer(OBDPort);
 
# ifdef SERIAL2
  serial_write(OBDPort, (char *) Spd_Req_Seq, Req_Length);
# else
  serial_write(OBDPort, (char *) Spd_Req_Seq, Req_Length, SerialBlock);
# endif

#   ifdef SERIAL2
    if (serial_read_blocking(OBDPort, (char *) Spd_Buffer, Spd_Resp_Length,
			     Timeval(0, Timeout)) != Spd_Resp_Length) {
#   else
    if (serial_read_blocking(OBDPort, (char *) Spd_Buffer, Spd_Resp_Length, timeout) != Spd_Resp_Length) {
#   endif
//    cout << "OBDII: unmatching Speed response length" << endl;
      return -1;
    }

    if ((Spd_Buffer[HW_Instr_Chk_Pos] != Ret_Ctrl_Byte) ||
            (Spd_Buffer[Seq_Chk_Pos] != Spd_Req_Seq[Seq_Chk_Pos])){
//      cout<< "OBDII: Error reading back Speed value" << endl;
        return -1;
    }
    
//<<<<<<< OBDII.cc
  Speed = (((float) Spd_Buffer[9]) * 1000.0 / 3600.0) * TireComp;
//=======
//  Speed = (((int) Spd_Buffer[9]) * 1000 / 3600) * TireComp;
//>>>>>>> 1.9
  
  return Speed;
}


/*PreCondition:	Must have been called
 PostCondition:	Will return the Engine Coolant Temperature in Celcius*/
float Get_Engine_Coolant_Temp(){
  unsigned char Coolant_Buffer[Coolant_Resp_Length];
  float Coolant_Temp = 1;

  serial_clean_buffer(OBDPort);
 
# ifdef SERIAL2
  serial_write(OBDPort, (char *) Coolant_Temp_Req_Seq, Req_Length);
# else
  serial_write(OBDPort, (char *) Coolant_Temp_Req_Seq, Req_Length, SerialBlock);
# endif

#   ifdef SERIAL2
    if (serial_read_blocking(OBDPort, (char *) Coolant_Buffer, Coolant_Resp_Length, Timeval(0, Timeout)) != Coolant_Resp_Length) {
#   else
    if (serial_read_blocking(OBDPort, (char *) Coolant_Buffer, Coolant_Resp_Length, timeout) != Coolant_Resp_Length) {
#   endif
      cout << "OBDII: unmatching Engine Coolant Temperature response length" << endl;
      return -1;
    }

    if ((Coolant_Buffer[HW_Instr_Chk_Pos] != Ret_Ctrl_Byte) ||
            (Coolant_Buffer[Seq_Chk_Pos] != Coolant_Temp_Req_Seq[Seq_Chk_Pos])){
        cout<< "OBDII: Error reading back Engine Coolant Temperature value" << endl;
        return -1;
    }
    
  Coolant_Temp = ((int) Coolant_Buffer[9]) - 40;
  
  return Coolant_Temp;
}


/*PreCondition:	Must have been called
 PostCondition:	Will return the Intake Manifold Absolute Pressure in kPa*/
float Get_Manifold_Pressure(){
  unsigned char Manifold_Pressure_Buffer[Manifold_Pressure_Resp_Length];
  float Pressure = 1;

  serial_clean_buffer(OBDPort);
 
# ifdef SERIAL2
  serial_write(OBDPort, (char *) Intake_Manifold_Pressure_Req_Seq, Req_Length);
# else
  serial_write(OBDPort, (char *) Intake_Manifold_Pressure_Req_Seq, Req_Length, SerialBlock);
# endif

# ifdef SERIAL2
    if (serial_read_blocking(OBDPort, (char *) Manifold_Pressure_Buffer, Manifold_Pressure_Resp_Length, Timeval(0, Timeout)) != Manifold_Pressure_Resp_Length) {
# else
    if (serial_read_blocking(OBDPort, (char *) Manifold_Pressure_Buffer, Manifold_Pressure_Resp_Length, timeout) != Manifold_Pressure_Resp_Length) {
# endif
      cout << "OBDII: unmatching Intake Manifold Pressure response length" << endl;
      return -1;
    }

    if ((Manifold_Pressure_Buffer[HW_Instr_Chk_Pos] != Ret_Ctrl_Byte) ||
            (Manifold_Pressure_Buffer[Seq_Chk_Pos] != Intake_Manifold_Pressure_Req_Seq[Seq_Chk_Pos])){
        cout<< "OBDII: Error reading back Intake Manifold Pressure value" << endl;
        return -1;
    }
    
  Pressure = Manifold_Pressure_Buffer[9];
  
  return Pressure;
}


/*PreCondition:	Must have been called
 PostCondition:	Will return the Ignitiont Timing Advance for #1 Cylinder in degrees*/
float Get_Ignition_Timing(){
  unsigned char Timing_Buffer[Ignition_Adv_Resp_Length];
  float Advance = 1;

  serial_clean_buffer(OBDPort);
 
# ifdef SERIAL2
  serial_write(OBDPort, (char *) Ignition_Timing_Adv_Req_Seq, Req_Length);
# else
  serial_write(OBDPort, (char *) Ignition_Timing_Adv_Req_Seq, Req_Length, SerialBlock);
# endif

# ifdef SERIAL2
    if (serial_read_blocking(OBDPort, (char *) Timing_Buffer, Ignition_Adv_Resp_Length, Timeval(0, Timeout)) != Ignition_Adv_Resp_Length) {
# else
    if (serial_read_blocking(OBDPort, (char *) Timing_Buffer, Ignition_Adv_Resp_Length, timeout) != Ignition_Adv_Resp_Length) {
# endif
      cout << "OBDII: unmatching Ignition Timing Advance response length" << endl;
      return -1;
    }

    if ((Timing_Buffer[HW_Instr_Chk_Pos] != Ret_Ctrl_Byte) ||
            (Timing_Buffer[Seq_Chk_Pos] != Ignition_Timing_Adv_Req_Seq[Seq_Chk_Pos])){
        cout<< "OBDII: Error reading back Ignition Timing Advance value" << endl;
        return -1;
    }
    
  Advance = ((int)Timing_Buffer[9]) / 2 - 64;
  
  return Advance;
}


/*PreCondition:	Must have been called
 PostCondition:	Will return the Intake Air Temperature in Celcius*/
float Get_Intake_Air_Temp(){
  unsigned char Intake_Air_Temp_Buffer[Intake_Air_Temp_Resp_Length];
  float Temperature = 1;

  serial_clean_buffer(OBDPort);
 
# ifdef SERIAL2
  serial_write(OBDPort, (char *) Intake_Air_Temp_Req_Seq, Req_Length);
# else
  serial_write(OBDPort, (char *) Intake_Air_Temp_Req_Seq, Req_Length, SerialBlock);
# endif

# ifdef SERIAL2
    if (serial_read_blocking(OBDPort, (char *) Intake_Air_Temp_Buffer, Intake_Air_Temp_Resp_Length, Timeval(0, Timeout)) != Intake_Air_Temp_Resp_Length) {
# else
    if (serial_read_blocking(OBDPort, (char *) Intake_Air_Temp_Buffer, Intake_Air_Temp_Resp_Length, timeout) != Intake_Air_Temp_Resp_Length) {
# endif
      cout << "OBDII: unmatching Intake Air Temperature response length" << endl;
      return -1;
    }

    if ((Intake_Air_Temp_Buffer[HW_Instr_Chk_Pos] != Ret_Ctrl_Byte) ||
            (Intake_Air_Temp_Buffer[Seq_Chk_Pos] != Intake_Air_Temp_Req_Seq[Seq_Chk_Pos])){
        cout<< "OBDII: Error reading back Intake Air Temperature value" << endl;
        return -1;
    }
    
  Temperature = ((int) Intake_Air_Temp_Buffer[9]) - 40;
  
  return Temperature;
}


/*PreCondition:	Must have been called
 PostCondition:	Will return the Air Flow Rate in g/s */
float Get_Air_Flow_Rate(){
  unsigned char AirFlow_Rate_Buffer[AirFlow_Rate_Resp_Length];
  float AirFlow_Rate = 1;

  serial_clean_buffer(OBDPort);
 
# ifdef SERIAL2
  serial_write(OBDPort, (char *) AirFlow_Rate_Req_Seq, Req_Length);
# else
  serial_write(OBDPort, (char *) AirFlow_Rate_Req_Seq, Req_Length, SerialBlock);
# endif

# ifdef SERIAL2
    if (serial_read_blocking(OBDPort, (char *) AirFlow_Rate_Buffer, AirFlow_Rate_Resp_Length, Timeval(0, Timeout)) != AirFlow_Rate_Resp_Length) {
# else
    if (serial_read_blocking(OBDPort, (char *) AirFlow_Rate_Buffer, AirFlow_Rate_Resp_Length, timeout) != AirFlow_Rate_Resp_Length) {
# endif
      cout << "OBDII: unmatching AirFlow Rate response length" << endl;
      return -1;
    }

    if ((AirFlow_Rate_Buffer[HW_Instr_Chk_Pos] != Ret_Ctrl_Byte) ||
            (AirFlow_Rate_Buffer[Seq_Chk_Pos] != AirFlow_Rate_Req_Seq[Seq_Chk_Pos])){
        cout<< "OBDII: Error reading back AirFlow Rate value" << endl;
        return -1;
    }
    
  AirFlow_Rate = (((int)AirFlow_Rate_Buffer[9]) * 256 + AirFlow_Rate_Buffer[10]) * 0.1;
  
  return AirFlow_Rate;
}

/*PreCondition: Must have been called
 PostCondition:	Will return the Absolute Throttle Position in a percentage*/
float Get_Throttle_Pos(){
  unsigned char Throttle_Pos_Buffer[Throttle_Pos_Resp_Length];
  float Throttle_Pos = 1;

  serial_clean_buffer(OBDPort);
 
# ifdef SERIAL2
  serial_write(OBDPort, (char *) Throttle_Pos_Req_Seq, Req_Length);
# else
  serial_write(OBDPort, (char *) Throttle_Pos_Req_Seq, Req_Length, SerialBlock);
# endif

# ifdef SERIAL2
    if (serial_read_blocking(OBDPort, (char *) Throttle_Pos_Buffer, Throttle_Pos_Resp_Length, Timeval(0, Timeout)) != Throttle_Pos_Resp_Length) {
# else
    if (serial_read_blocking(OBDPort, (char *) Throttle_Pos_Buffer, Throttle_Pos_Resp_Length, timeout) != Throttle_Pos_Resp_Length) {
# endif
      cout << "OBDII: unmatching Absolute Throttle Position response length" << endl;
      return -1;
    }

    if ((Throttle_Pos_Buffer[HW_Instr_Chk_Pos] != Ret_Ctrl_Byte) ||
            (Throttle_Pos_Buffer[Seq_Chk_Pos] != Throttle_Pos_Req_Seq[Seq_Chk_Pos])){
        cout<< "OBDII: Error reading back Absolute Throttle Position value" << endl;
        return -1;
    }
    
  Throttle_Pos = ((int) Throttle_Pos_Buffer[9]) * 100 / 256;
  
  return Throttle_Pos;
}
