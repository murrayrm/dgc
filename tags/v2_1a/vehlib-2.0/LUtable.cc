#include "LUtable.h"

using namespace std;

LUtable::LUtable() {
  
}

LUtable::LUtable(std::string filename) {
  loadTable(filename);
}

LUtable::~LUtable() {

}

void LUtable::loadTable(std::string filename) {
  double f, v;

  ifstream in_file(filename.c_str(), ios::binary);

  if(in_file) {
    while(!in_file.eof()) {
      f=0;
      v=0;

      in_file >> f;
      in_file >> v;

      TableType::value_type entry(f, v);

      dataTable.insert(entry);

      //      cout << "\nGot f=[" << f << "], v=[" << v << "]\n";
    }
    in_file.close();
  } else {
    cout << "\nError opening look-up table file: " << filename << "\n";
  }

  TableType::iterator iter;

  iter = min_element(dataTable.begin(), dataTable.end());
  min_key = iter->first;

  iter = max_element(dataTable.begin(), dataTable.end());
  max_key = iter->first;

  //  This just outputs some numbers to test proper loading of tables
  //cout << "I am here, where are you?" << endl;
  //cout << "\n Max [" << max_key << "] min[" << min_key << "]\n";

}

double LUtable::getValue(double key) {
  double return_val=0;
  double slope;

  if( PRINT_DEBUG_MESSAGES )
    cout << "Key = " << key << endl;

  TableType::iterator iter = dataTable.find(key);

  if( PRINT_DEBUG_MESSAGES ) {
    cout << "iter->first = " << iter->first
         << ", key = " << key << endl;
  }

  if(iter->first != key ) {
    // wtf?
    //  if(iter->first ==0 && key!=0) {
    // which method to interpolate?
    if(key > max_key) {
      iter = dataTable.find(max_key);
      return_val = iter -> second;
      if( PRINT_DEBUG_MESSAGES ) 
        cout << "Using Max Key" << endl;
    } else if(key < min_key) {
      iter = dataTable.find(min_key);
      return_val = iter -> second;
      if( PRINT_DEBUG_MESSAGES ) 
        cout << "Using Min Key" << endl;
    } else {
      TableType::value_type entry(key, 0);
      TableType::iterator upper_iter = upper_bound(dataTable.begin(), dataTable.end(), entry);
      TableType::iterator lower_iter = max_element(dataTable.begin(), upper_iter);

      slope = (upper_iter->second - lower_iter->second)/(upper_iter->first - lower_iter->first);
      if( PRINT_DEBUG_MESSAGES ) {
        cout << "Slope = (" << upper_iter->second << "-"
   	     << lower_iter->second << ")/(" 
	     << upper_iter->first << "-" 
	     << lower_iter->first << ") = "
	     << slope << endl;
      }

      // H - have you taken algebra 1 yet?  You need to work on your 
      // y = mx+b functions.  There is a nice elementary school across the 
      // street.  BTW, Lars is pissed off.
      //      return_val = slope * (key-lower_iter->second) + lower_iter->second;
      return_val = slope * (key - lower_iter->first) + lower_iter->second;
    }
  } else {
    return_val = iter->second;
  }

  return return_val;
}
