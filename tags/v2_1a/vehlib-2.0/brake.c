#include "vehports.h"
#include "brake2.h"
#include "throttle.h"			/* for THROTTLE_CS */


#include "sparrow/dbglib.h"
#include <iostream>
#include <time.h>

#include "MTA/Misc/Time/Time.hh"

using namespace std;

/* Declare some global variables for use in display */
double brk_current_abs_pos_buf, brk_current_acc_buf, brk_current_vel_buf;
double brk_current_brake_pot, brk_pot_voltage;
int brk_case, brk_count;

/* Constants used in brake_write() function mainly as safety limits. */
/*  The following two variables are used by brake_write and other functions
 * like brake_pause that call it move the brake's absolute position based on the pot.
 *  Although they are initialized to values that will give decent braking
 * characteristics, they should be calibrated using the calib_brake 
 * as soon as the limit switches have been installed.
  *  min_pot & max_pot: max and min position used in brake_write
 *    to scale input position values. [0,1] -> [min_pot, max_pot]. Hence,
 *    these values act as software limits for the brake actuator's movement
 *    range.
 */
double brake_min_pot = 0.38;  //Limits set with regard to hardware configuration
double brake_max_pot = 0.75;  //Limits set with regard to hardware configuration

//double min_counts=0.0;    //no longer used
//double max_counts=-119868;    //no longer used

/* our_max_pos & our_min_pos: 
 *  Modified in calib_brake() with getBrakePot() values, just like min_pot and
 *    max_pot, i.e. (as of 1/17/2004) same as min_pot & max_pot if
 *    calib_brake() is run.
 * Current Usage: set_brake_abs_pos()
 */
//double our_max_pos = .95;  //no longer used
//double our_min_pos = .05;  //no longer used

//****************************************************
//****************************************************
/* Declares the buffer that stores the current commanded brake position,
 * vel and acc values.
 * Current Usage: set_brake_abs_pos_vel(), start_brake_control_loop()
 */
double* brake_abs_pos_buf;
double* brake_vel_buf;
double* brake_acc_buf;

pthread_t brake_thread;                 //braking control loop thread 
int brake_enabled = 0;
double current_brake_pot = 0;
double current_abs_pos_buf = 0;
double current_vel_buf = 0;
double current_acc_buf = 0;

//variables from vdrive that set the brake position for brake_pause, accel, 
//and vel to use for moving the brake
double BRAKE_VEL = 0.3;			/* brake velocity used during motion */  
double BRAKE_ACC = 1;			/* brake acceleration used during motion */
double MAX_BRAKE = 1;			/* max brake position for full stop and user_estop_pause out of 0-1*/


// ****************************************************


//allocates needed memory, opens the serial port, starts the control loop in 
//its own thread.  This must be run after a brake_close is commanded.
int brake_open(int port) {
  char parameters[400];
  brake_enabled = 1;
  //  cout<<"initializing brake"<<endl;
  //***************************************************************
  // **************************************************************
  // allocate memory for the position in start_brake_control_loop
  brake_abs_pos_buf = (double*)malloc(sizeof(double));
  brake_vel_buf = (double*)malloc(sizeof(double));
  brake_acc_buf = (double*)malloc(sizeof(double));
  if((brake_abs_pos_buf == NULL) || (brake_vel_buf == NULL) || (brake_acc_buf == NULL)) {
    printf("Failed to allocate memory for brake_abs_pos_buf, brake_vel_buf, and/or brake_acc_buf.");
    return -1;
  }
  /* Initialize everything to zero */
  //cout << "point 1";
  *brake_abs_pos_buf = *brake_vel_buf = *brake_acc_buf = 0.0;
  //reset the brake memory
  //***************************************************************  
  //cout << "point 2";
  if(serial_open_advanced(port, BRAKE_BAUD, SR_PARITY_DISABLE | SR_SOFT_FLOW_DISABLE | 
			   SR_HARD_FLOW_DISABLE | SR_TWO_STOP_BIT) != 0) {
    dbg_info("\nUnable to open com port %d" , BRAKE_SERIAL_PORT);
    return -1;
  } 
  //cout << "point 3";
  pthread_create(&brake_thread, NULL, brake_start_control_loop,
		 (void *) &brake_enabled);
    // removed an if statement from around the pthread_create
    //{
    //  dbg_info("Brake thread not started\n");
    // return -1;
    // }
  //cout << "point 4\n";
  //  if(brake_reset() < 0) {
  //  return -1;
  //  } //brake_reset returns a dbg_info 
  
  //TBD: figure out whether to do this or not, or maybe just to call brake
  //command 4
  
  //  brake_reset();  
  
  //  else {
  brake_send_command(4, " ", 0); //WNH added 2-14-03, may need to remove
  usleep_for(2000000);
  // cout << "brake OK\n";
  //vdrive returns messages
  return 0;
  //  }
}

//WNH 2-07-04
//everything from here to the next function used to be comments inside init_brake, but have
//been moved down to make reading this code easier

    //If the serial port is initialized, send initialization commands to the brake
    //These are copied directly from the default initialization in the QuickSilver Controls Windows program
    //Note: They probably aren't needed!
    //For information on each command, look up the command ID (first argument in the send_brake_command command) in the QuickSilver manual

    //Format:
    //sprintf(parameters, "parameter format string", param1, param2, ...);
    //send_brake_command(command_num, parameters, strlen(parameters));

    // Because of the following line, this function cannot be called
    // before init_accel() or initBrakeSensors()
    // Get the current zero position: we should probably move the
    // actuator to the absolute zero and then do this... will init_brake()
    // always be followed by calib_brake? 

    //our_min_pos = getBrakePot();

    /*    
    sprintf(parameters, "%d ", 5136);
    send_brake_command(155, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 1);
    send_brake_command(185, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 0);
    send_brake_command(186, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 576);
    send_brake_command(174, parameters, strlen(parameters));

    //ACK Delay
    sprintf(parameters, "%d ", 0);
    send_brake_command(173, parameters, strlen(parameters));
    
    //MCT
    //FLC

    sprintf(parameters, "%d %d %d %d %d %d %d ", 0, 6, 6, 20, 20, 200, 500);
    send_brake_command(148, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 0);
    send_brake_command(237, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 0);
    send_brake_command(184, parameters, strlen(parameters));

    sprintf(parameters, "%d %d %d %d ", 15000, 20000, 6000, 10000);
    send_brake_command(149, parameters, strlen(parameters));

    sprintf(parameters, "%d %d ", 10, 4);
    send_brake_command(150, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 1250);
    send_brake_command(230, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 32767);
    send_brake_command(195, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 10);
    send_brake_command(212, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 52);
    send_brake_command(213, parameters, strlen(parameters));

    //Error Limits (ERL)
    sprintf(parameters, "%d %d %d ", 500, 200, 120);
    send_brake_command(151, parameters, strlen(parameters));

    //Kill Motor Condition (KMC)
    sprintf(parameters, "%d %d ", 128, 0);
    send_brake_command(167, parameters, strlen(parameters));

    sprintf(parameters, "%d %d ", 0, 83);
    send_brake_command(252, parameters, strlen(parameters));
    
    */

    /*
    //Soft Stop Limits
    sprintf(parameters, "%d ", SSL_REGISTER);
    send_brake_command(221, parameters, strlen(parameters));

    //Write Register (of min Soft Stop Limit)
    sprintf(parameters, "%d %d ", SSL_REGISTER, SSL_MIN);
    send_brake_command(11, parameters, strlen(parameters));

    //Write Register (of max Soft Stop Limit)
    sprintf(parameters, "%d %d ", SSL_REGISTER+1, SSL_MAX);
    send_brake_command(11, parameters, strlen(parameters));
    */

    //cout << "end of initialization" << endl;

//sends the brake to zero (no braking) at max vel using the standard code
int brake_zero() {
  brake_write(0, BRAKE_VEL, BRAKE_ACC);
  return 0;
}

int brake_calibrate() {   //TBD: needs to be fixed once the hardware limit stwitched have been built
  /*  float pos;
  int val;
  char comm;
  //Restart the Actuator
  brake_send_command(4, " ", 0);
  //Wait until the actuator is ready again
  while(brake_ok()!=0) {};
  do {
    printf("\nCurrent Position(current setting, counts): %d", get_brake_pos());
    printf("\nCurrent Position(pot reading, 0-1): %f\n", getBrakePot());
    printf("Enter command (c: continue to move actuator z: set zero(full-out?), f: set full-position(all-in?), q: quit): ");
    cin>>comm;
    cout<<endl;
    if(comm=='c'){
	  printf("\nEnter rel pos change (0-1): ");
	  scanf("%f", &pos);
	  if (getBrakePot()+pos <= 0.05 || getBrakePot()+pos >= 1) {
    char cont = 'c';
    while (cont != 'y' && cont != 'n') {
      cout << "This may break the actuator, continue? (y=yes,n=no):";
      cin >> cont;
      cout << endl;
    }
    if (cont == 'y') {
      val = set_brake_rel_pos(pos, 0.02, 0.02);
	  printf("\nVal: %d\n", val);  
    }
     }
     else {
    val = set_brake_rel_pos(pos, 0.05, 0.05);  //orig 0.9
    printf("\nVal: %d\n", val);  
      }
    }
    while(brake_ready()!=0) {
      // cout << "waiting for the brake actuator to finish" << endl;
    };    
    //usleep(1500000);
    //sleep(2);          // wait for the actuator to finish moving?
  } while(comm!='q' && comm!='z' && comm!='f');     
  if(comm=='z') { 
    //Restarting it gets rid of moving error
    //send_brake_command(4, " ", 0);
    //Wait until the actuator is ready again
    //while(brake_ready()!=0) {};
    //set_brake_rel_pos_counts(-10000, 0.06, 0.06);
    //sleep(2);
    //set_brake_rel_pos_counts(ZERO_POS+10000, 0.05, 0.05);
    //Make the current position the 0 position
    our_min_pos = getBrakePot();
    min_pot=getBrakePot();
    min_counts=get_brake_pos();
    cout << "\n*min_pot set to " << min_pot;
    cout << "\n*min_counts set to " << min_counts;
    return send_brake_command(145, " " , 0);
  }
  else if(comm=='f') {
    //Restarting it gets rid of moving error
    //send_brake_command(4, " ", 0);
    //Wait until the actuator is ready again
    //while(brake_ready()!=0) {};
    //sleep(2);
    //Make the current position the 1 position...???
    //return send_brake_command(145, " " , 0);
    our_max_pos = getBrakePot();
    max_pot=getBrakePot();
    max_counts=get_brake_pos();
    cout << "\n*max_pot set to " << max_pot;
    cout << "\n*max_counts set to " << max_counts;
    }*/
  cout << "brake_calibrate called, but code not yet written--nothing done";
  return 0;
}

int brake_ok() {
  char response[50];
  
  brake_receive_command(0, " ", 0, response, 50, 1);
  if(strcmp(response, "# 10 0000 2000\n")==0) {
    return 0;
  }
  /* else if (strcmp(response, "# 10 0000 0100\n") == 0
    || strcmp(response, "# 10 0000 0200\n") == 0) {
    cout << "Brake ERROR~ restarting!" << endl;
    reset_brake();
    return 0;
    } */
  else {
    return -1;
  }
}

//TBD: This will eventually return a struct full of the requested data and zeros elsewhere.
//Doesnt do anything now
int brake_status(int request_type, struct brake_status_type *brake_status_type){
  cout << "brake_status called but code not yet written--nothing done.";
  return 0;
}

//int brake_pause(double pos = MAX_BRAKE){  //should maybe go to MAX_BRAKE??
int brake_pause() {
  brake_write(MAX_BRAKE, BRAKE_VEL, BRAKE_ACC);
  usleep_for(3000000);
  brake_enabled = 0;  //prevents the brake control loop from moving the brake any more
  return 0;
}

//Sends the brake to idle at max vel via the pause function, and disables the brake control loop.
int brake_disable() {
  return brake_pause();
}

//Restarts the brake control loop so that new commands can be issued.  Must be run after
//a brake_pause or brake_disable is commanded.
int brake_resume() {
  brake_enabled = 1;  //allows the brake thread to send move commands again
  return 0;
}

//Closes the serial port, kills the brake control loop and the thread it is running in,
//and unallocates the buffer memory.  Must run brake_open to begin writing to the brake again.
int brake_close() {
  char parameters[400];
  // free memory for the position in start_brake_control_loop
  brake_write(0, BRAKE_VEL, BRAKE_ACC); //moves the brake to zero and shuts the control loop down
  usleep_for(3000000);
  brake_enabled = 0;  //prevents the brake control loop from moving the brake any more
  usleep_for(500000);
  brake_enabled = -1;  //kills the brake control loop thread
  free(brake_abs_pos_buf);
  free(brake_vel_buf);
  free(brake_acc_buf);
    //reset the brake memory
  //***************************************************************  
  return serial_close(BRAKE_SERIAL_PORT);
}

//This function sends the command to reset the brake
//And then waits until the brake is ready before returning
//IT IS A BLOCKING FUNCTION
int brake_reset() {
  bool timed_out = 0;
  timeval before, after;
  double elapsed_time;
  char response[50];
  response[0]='\0';
  int return_value=0;
  gettimeofday(&before, NULL);
  gettimeofday(&after, NULL);
  cout << "rpoint1";
  //Restarting it gets rid of moving error  
  //serial_write(BRAKEPORT, "@16 4 \r", strlen("@16 4 \r"), SerialBlock);
  brake_send_command(4, " ", 0);
  cout << "rpoint2";
  //Wait until the actuator is ready again
#ifdef UNUSED  //wish i knew what the 10 0000 2010 string that i seem to be getting
  //means, but for now, i am going to reset the processor in the actuator without
  //checking to see if it is fixed.
  cout << "rpoint3";
  while((strcmp(response, "# 10 0000 2000\n") != 0) && (timed_out == 0)) {
    //cout << "stop";
    cout << "# 10 0000 2000\n" << response;
    brake_receive_command(0, " ", 0, response, 50, 1);
    gettimeofday(&after, NULL);  //update the time, for the elapsed time calculation
    elapsed_time = after.tv_sec - before.tv_sec;
    if(elapsed_time < 0)
      elapsed_time += 24 * 3600;
    if(elapsed_time > 60) {
      timed_out = 1;
      dbg_info("Brake failed to reset within 1 minute\n");
      return_value = -1;
    }
  }
#endif 
  cout << "rpoint4";
  dbg_info("Brake reset OK");
  //  return return_value; //this is needed for the above code that is commented out.
  cout << "rpoint5";
  return 0;  //WTF wont this return!!!!!!
  // should be calibrate it again?
  // or at least set the min max variables?
  // depends on how stuff is done:
  //  using set_brake_abs_pos_pot 
  //  => either must use hardcoded
  //         or use the first calibrated number and hope the program doesn't crash
  //         or calibrate every time we reset this (using brake pot).


  // Get the elapsed time. 
  //  gettimeofday(&after, NULL);
  //elapsed = after.tv_sec - before.tv_sec;
  //elapsed += ((double) (after.tv_usec - before.tv_usec)) ;
  // Fix Midnight rollover
  //if ( elapsed < 0 ) {
    // Add number of seconds in a day
  //  elapsed += 24 * 3600;
  // }
  //cout << "\nTime to reset in seconds: " << elapsed << endl;
}

int brake_fault_reset() { //WNH 2-21-04 added code that can be called by pushing
  //the rst button in the vdrive-2.0 interface to reset the brake.  Hopefully this
  //will solve the stick problems that the actuator gets into sometimes.
  brake_send_command(4, " ", 0); //WNH 2-21-04 may need to remove
  usleep_for(2000000);
  //Send a polling command
  brake_send_command(0, " ", 0);
  //Clear the polling status word
  brake_send_command(1, "65535 ", strlen("65535 "));
  //Get revision info?
  brake_send_command(5, " ", 0);
  //Clear the program buffer
  brake_send_command(8, " ", 0);
  usleep_for(500000);
  return 0;
}

//returns the curent position on the 0-1 scale as calibrated
double brake_read() {
  return brake_get_pot();
}


#ifdef BRAKEREADFIXED
int brake_read(double *brake_position) {
  if (pp_lock(PP_GASBR)) {  //if we get the lock, we can go
    
    pp_set_bits(PP_GASBR, THROTTLE_CS, 0);     // make sure accel doesn't move
    pp_set_bits(PP_GASBR, 0, BT_OE);        // make sure pot is the only input 
    pp_set_bits(PP_GASBR, 0, BPOT_RC);
    pp_set_bits(PP_GASBR, 0, BPOT_CS);
    usleep(1); // <65ns
    pp_set_bits(PP_GASBR, BPOT_CS, 0);
    usleep(BPOT_TBUSY);    
    pp_set_bits(PP_GASBR, BPOT_RC, 0);
    pp_set_bits(PP_GASBR, 0, BPOT_CS);
    usleep(1); // <83ns
    
    int lowerEight = (int) pp_get_data(PP_GASBR);
    int upperFour = (int) pp_get_bit(PP_GASBR, BPOT_D8) 
      + (int) (pp_get_bit(PP_GASBR, BPOT_D9)<<1)
      + (int) (pp_get_bit(PP_GASBR, BPOT_D10)<<2);
    int sign = (int) pp_get_bit(PP_GASBR, BPOT_D11);
    pp_set_bits(PP_GASBR, BPOT_CS, 0);  
    int potReading = lowerEight + (upperFour<<8);
    
    pp_unlock(PP_GASBR); //lock has been released

    if (sign == 1) {  // negative probably means zero
      //got a negative number.  this is wrong.
      dbg_error("got negative brake pot reading \n");
      return -1; //part of hack below
    }  

    else if (sign == 0) {
      brk_pot_voltage = (potReading/BP_NUM_HIGH - BP_MIN)/(BP_MAX-BP_MIN);
      
      if(brk_pot_voltage == 0) {
	//do nothing;
	dbg_info("pot read zero \n");
	return 0;
      }
      *brake_position=brk_pot_voltage;
      return 0;
    }
    else {
      // cout << "If you're reading this, sign != 0 or 1. Just not possible..." << endl;
      dbg_error("can't read parallel port for brake\n");
      return -1;
    }
  }
  else { //couldn't get the lock
    dbg_info("Couldn't get the lock");
    return -1;
  }
}
#endif




/* This function sets the buffer to the latest commanded brake position, 
 * vel, and acc.  Takes each of these in the 0-1 range, and properly scales them
 * to brake_min_pot, _max_pot, _MIN_VEL, _MAX_VEL, _MIN_ACC, _MAX_ACC
 */
int brake_write(double abs_pos, double vel, double acc) {
  // cout << "set_brake_abs_pos_vel: Setting abs_pos to " << abs_pos << endl;
  *brake_abs_pos_buf = abs_pos;
  *brake_vel_buf = vel;
  *brake_acc_buf = acc;
  return 0;
  //cout<<"done moving with brake_write"<<endl;
}

// starts the loop that monitors the buffer above and moves to the most current value
void *brake_start_control_loop(void *arg) {

  int *brake_enabled_inside = (int *) arg;
  int read_ret_val;
  char parameters[50];
  void *exit_val = 0;  //this is for the pthread_exit() below
  brk_count = 0;  //initialize this
  // Don't bother setting brake position initially
  // cout<<"setting pos to .5"<<endl;
  // set_brake_abs_pos(.5, .5, .5);
  // cout<<"sleeping"<<endl;
  // usleep(2000000);

  //The following is copied from set_brake_abs_pos_limited, might not need them!!

  // The following commands are copied directly from how the QuickSilverMax
  //  Windows program does it. Sadly, no one knows why.
  //Send a polling command
  brake_send_command(0, " ", 0);
  //Clear the polling status word
  brake_send_command(1, "65535 ", strlen("65535 "));
  //Get revision info?
  brake_send_command(5, " ", 0);
  //Clear the program buffer
  brake_send_command(8, " ", 0);

  while (1) {
      // sets up some values to so they only have to be calculated once each loop
    brk_count++;
    current_brake_pot = brake_read();
    //    current_brake_pot = 0.5; // DELETE

    //ensure the variables are scaled and in range properly, also scales desired position 
    //(current_abs_pos_buf) to from 0-1 to brake_min_pot - brake_max_pot 
    current_abs_pos_buf = brake_scale(brake_in_range(*brake_abs_pos_buf, 0, 1), 0, 1, brake_min_pot, brake_max_pot);
    current_vel_buf = brake_scale(brake_in_range(*brake_vel_buf, 0, 1), 0, 1, BRAKE_MIN_VEL, BRAKE_MAX_VEL);
    current_acc_buf = brake_scale(brake_in_range(*brake_acc_buf, 0, 1), 0, 1, BRAKE_MIN_ACC, BRAKE_MAX_ACC);

    //Send a polling command
    //    brake_send_command(0, " ", 0);
    //Clear the polling status word
    // brake_send_command(1, "65535 ", strlen("65535 "));
    //Get revision info?
    // brake_send_command(5, " ", 0);
    //Clear the program buffer
    // brake_send_command(8, " ", 0);


    // checks if brk_enabled from vdrive is high  
    if (*brake_enabled_inside == -1) pthread_exit((exit_val));  //I have no idea how to do this well
    if (*brake_enabled_inside == 1) {

      /*
      Original code uses the following:
        //Limit and scale the velocity, acceleration, and range
        vel=in_range_double(vel, 0, 1);
        vel=scale_double(vel, 0, 1, MIN_VEL, MAX_VEL);

        acc=in_range_double(acc, 0, 1);
        acc=scale_double(acc, 0, 1, MIN_ACC, MAX_ACC);
      */

      // checks to see if break is far enough from the desired point and within acceptable range
      if( //current_brake_pot>current_abs_pos_buf &&
	 //abs(current_brake_pot-current_abs_pos_buf) > BRAKE_POS_TOLERANCE && 
	 //current_brake_pot > brake_min_pot) {
	((current_brake_pot - current_abs_pos_buf) > BRAKE_POS_TOLERANCE) &&
	(current_brake_pot > brake_min_pot)) {  //TBD: dont really need the last
	//because the inout is already scaled
        //send brake a negative velocity move
	sprintf(parameters, "%.0f %.0f %d %d ", current_acc_buf, current_vel_buf, 
		BRAKE_STOP_ENABLE_ABS, BRAKE_STOP_STATE_ABS);
	
	// cout<<"sending brake negative move"<<endl;
        brake_send_command(15, parameters, strlen(parameters));
        // cout<<"current_acc_buf, current_vel_buf, STOP_ENABLE_ABS, STOP_STATE_ABS "
        // <<parameters<<" current_brake_pot "<<current_brake_pot<<" current_abs_pos_buf "
        // <<current_abs_pos_buf<<endl;
	brk_case = 1;
	brk_current_abs_pos_buf = current_abs_pos_buf;
	brk_current_acc_buf = current_acc_buf;
	brk_current_vel_buf = current_vel_buf;
	brk_current_brake_pot = current_brake_pot;
	// cout << "bc=1";
      }
      // checks to see if break is far enough from the desired point and within acceptable range
      else if( //current_abs_pos_buf > current_brake_pot &&
	      //abs(current_brake_pot-current_abs_pos_buf) > BRAKE_POS_TOLERANCE && 
	      //current_brake_pot<brake_max_pot) {
	      ((current_abs_pos_buf - current_brake_pot) > BRAKE_POS_TOLERANCE) &&
	      (current_brake_pot < brake_max_pot)) {
        //send brake a positive move
        sprintf(parameters, "%.0f %.0f %d %d ", current_acc_buf, -current_vel_buf,
		BRAKE_STOP_ENABLE_ABS, BRAKE_STOP_STATE_ABS);
	//          cout<<"sending brake negative move"<<endl;
        brake_send_command(15, parameters, strlen(parameters));
        /*
	  cout<<"Parameters for the brake 
	  command"<<parameters<<current_brake_pot<<current_abs_pos_buf<<endl;
        */
	//        cout<<"current_acc_buf, current_vel_buf, STOP_ENABLE_ABS, STOP_STATE_ABS "
	//        <<parameters<<" current_brake_pot "<<current_brake_pot<<" current_abs_pos_buf "
	//        <<current_abs_pos_buf<<endl;
	brk_case = 2;
	brk_current_abs_pos_buf = current_abs_pos_buf;
	brk_current_acc_buf = current_acc_buf;
	brk_current_vel_buf = current_vel_buf;
	brk_current_brake_pot = current_brake_pot;
	// cout << "bc=2";
      }
      else {
        //send brake stop if it is either close enough or out of range
        sprintf(parameters, "%.0f %.0f %d %d ", current_acc_buf, 0.0,
          BRAKE_STOP_ENABLE_ABS, BRAKE_STOP_STATE_ABS);
	//	cout<<"sending brake stop"<<endl;
        brake_send_command(15, parameters, strlen(parameters));
        /*
        cout<<"Parameters for the brake 
        command"<<parameters<<current_brake_pot<<current_abs_pos_buf<<endl;
        */
	//        cout<<"current_acc_buf, 0-current_vel_buf, STOP_ENABLE_ABS, STOP_STATE_ABS "
	//        <<parameters<<" current_brake_pot "<<current_brake_pot<<" current_abs_pos_buf "
	//        <<current_abs_pos_buf<<endl;
	brk_case = 3;
	brk_current_abs_pos_buf = current_abs_pos_buf;
	brk_current_acc_buf = current_acc_buf;
	brk_current_vel_buf = current_vel_buf;
	brk_current_brake_pot = current_brake_pot;
	// cout << "bc=3";
      }

    } else {
      brk_case = 4;
      //cout << "brake case = 4";
      /* Brake has been disabled; stop moving */
      //sprintf(parameters, "%.0f %.0f %d %d ", current_acc_buf, 0,
      //      STOP_ENABLE_ABS, STOP_STATE_ABS);
      brake_send_command(15, parameters, strlen(parameters));
      dbg_info("Parameters for the brake command--loop disabled %d %d %d\n\r",
	       parameters,current_brake_pot,current_abs_pos_buf);
    }
    usleep_for(10000);
  }
}
//***********************************************************************

int brake_send_command(int command_num, char* parameters, int length) {
  char buffer[50];
  //  cout << "brake sending command";
  //  dbg_info("brake: sending cmd %d\n", command_num);
  return brake_receive_command(command_num, parameters, length, buffer, 50, 1);
}

int brake_receive_command(int command_num, char* parameters, int length, char* buffer, int buf_length, int timeout) {
  char command[400];
  char word[11];  

  //Set the strings to null
  memset(command, 0, strlen(command));
  memset(buffer, 0, buf_length);

  //Prepare the command according to the brake actuator specs
  command[0]='@';
  sprintf(word, "%d ", BRAKE_ID);
  strcat(command, word);
  sprintf(word, "%d ", command_num);
  strcat(command, word);
  strcat(command, parameters);

  // we removed strlen(command)-1 below and removed the \n here 
  // thus doing the same thing
  strcat(command, "\r");// \n");

  //Write the command
  //  printf("sending command %d ", command_num);
#ifndef SERIAL2
  serial_write(BRAKE_SERIAL_PORT, command, strlen(command), SerialBlock);
  //Read the response
  //serial_read_until(BRAKEPORT, buffer, '\r', timeout, buf_length);
  serial_read_until(BRAKE_SERIAL_PORT, buffer, '\n', timeout, buf_length);
#else
  serial_write(BRAKE_SERIAL_PORT, command, strlen(command));
  //Read the response
  //serial_read_until(BRAKEPORT, buffer, '\r', buf_length, timeout);
  serial_read_until(BRAKE_SERIAL_PORT, buffer, '\n', buf_length, Timeval(0, timeout));
#endif

  //If it's a negative acknowledge (it starts with a !)
  if(strncmp(buffer, "!", 1)==0) {
    //Return an error
    return -1;
  } else {
    //Otherwise it's good
    /*  
    for (int i = 0; i < strlen(command); i++) 
      cout << command[i];
    cout << endl;
    
    for (int i = 0; i < strlen(buffer); i++) 
      //printf("%X ", (int)buffer[i]);
      cout << buffer[i];
    //cout << endl;
    */
    return 0;
  }

}

//some general helper functions below

//Helper function for ensuring in range data
double brake_in_range(double val, double min_val, double max_val) {
  if(val<min_val) return min_val;
  if(val>max_val) return max_val;
  return val;
}

//Helper function for scaling data
double brake_scale(double val, double old_min_val, double old_max_val, double new_min_val, double new_max_val) {
  double old_range, new_range;

  old_range=old_max_val-old_min_val;
  new_range=new_max_val-new_min_val;

  return ((new_range*(val-old_min_val)/old_range)+new_min_val);
}

/* TLL.cc - reading the brake force sensor through parallel interface
           - reading the linear pot on the brake actuator for act position
   Revision History: 7/17/2003  Created                      Sue Ann Hong
                     7/18/2003  Finished ver.1               Sue Ann Hong
		     8/17/2003  Added linear pot inteface    Sue Ann Hong
 Note: accuracy ~.005 (for ratio)
       max 1446-1456
 */
//using namespace std;

// remember the parallel port is PP_BTENS (or something like that)

// don't need to call this if init_accel() is called. in fact, one should
// always use init_accel().
//int initBrakeSensors() {
//  return pp_init(PP_GASBR, PP_DIR_IN);  /* data lines: read */
//}


/* getBrakePot
  Return value: (double) position of the brake actuator between 0 and 1
*/
double brake_get_pot() {

  // for testing only...
  //  return 0.5;

  pp_set_bits(PP_GASBR, THROTTLE_CS, 0);     // make sure accel doesn't move
  pp_set_bits(PP_GASBR, 0, BT_OE);        // make sure pot is the only input 
  pp_set_bits(PP_GASBR, 0, BPOT_RC);
  pp_set_bits(PP_GASBR, 0, BPOT_CS);
  usleep(1); // <65ns
  pp_set_bits(PP_GASBR, BPOT_CS, 0);
  usleep(BPOT_TBUSY);    
  pp_set_bits(PP_GASBR, BPOT_RC, 0);
  pp_set_bits(PP_GASBR, 0, BPOT_CS);
  usleep(1); // <83ns
  // read 
  int lowerEight = (int) pp_get_data(PP_GASBR);
  int upperFour = (int) pp_get_bit(PP_GASBR, BPOT_D8) 
    + (int) (pp_get_bit(PP_GASBR, BPOT_D9)<<1)
    + (int) (pp_get_bit(PP_GASBR, BPOT_D10)<<2);
  int sign = (int) pp_get_bit(PP_GASBR, BPOT_D11);
  pp_set_bits(PP_GASBR, BPOT_CS, 0);  
  int potReading = lowerEight + (upperFour<<8);

  if (sign == 1) {  // negative probably means zero
    //    cout << "WARNING: how can the wiper voltage be NEGATIVE???" << endl;
    //cout << "Potreading: " << potReading << endl;
    // return -1;
    // cout << "I'm going to assume that it's zero on return." << endl;
    return current_brake_pot; //this is an ugly hack to go along with the one below
  }  
  else if (sign == 0) {
    //    cout << "Voltage: " 
    //         << (potReading*(BP_VREF_HIGH-BP_VREF_LOW)/BP_NUM_DIV + BP_VREF_LOW) 
    //         << endl;
    brk_pot_voltage = ((double)potReading/BP_NUM_HIGH - BP_MIN)/(BP_MAX-BP_MIN);  //used by sparrow, but 
    //this cant be both the voltage and the value, can it?
    /* Return the value potReading scaled to BP_MAX, BP_MIN range. */

      //WNH 2-07-04 -- TBD: this is a hack to fix the brake pot reading that oscillates
      //between the real value and 0.  The problem is most likely in the brake pot hardware or 
      //software, but this should fix for now since the brake pot should never actually read 0
      //(because it should be limited to something bigger than that by the software) and if it 
      //does, it will simply keep the value it had 1/100th of a second before that, which will
      //be very close to zero.
    if(brk_pot_voltage == 0) {
      brk_pot_voltage = current_brake_pot;
	}
    return brk_pot_voltage;
    //    return (potReading/BP_NUM_HIGH - BP_MIN)/(BP_MAX-BP_MIN);
      }
  else {
    cout << "If you're reading this, sign != 0 or 1. Just not possible..." << endl;
    return -1;
  }
}
