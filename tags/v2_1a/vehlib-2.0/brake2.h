/*
 * brake2.h - header file for brake2.c
 *
 * RMM, 23 Feb 04
 *
 */

#ifndef BRAKE2_HH
#define BRAKE2_HH

#define BRAKE2				/* use new code when necessary */

#include "parallel.h"			/* parallel port bit defs */
#include "throttle.h"			/* define throttle bits */

/* Standard device functions */
int brake_open(int);
int brake_calibrate();
int brake_home(), brake_reset();
int brake_read(double *);
int brake_write(double, double, double);
void *brake_servo(void *);
int brake_status(int *);
int brake_status(int *, int *, int *, int *);
int brake_clrpsw(int);
int brake_pause(), brake_disable(), brake_resume();
int brake_close();

/* Brake specfic device calls */
int brake_setmav(double, double, double);

/* External variables */
extern double brake_pot_current;

/* Brake constants */
#define BRAKE_ID 16
#define BRAKE_BAUD B57600

//absolute max and min values ever used for brake velocity.  they are scaled to BRAKE_VEL, which has a value from 0-1
#define BRAKE_MAXVEL 1000000000		/* ~4000 rpm--this is near the hardware maximum of 2147483647*/
#define BRAKE_MINVEL 1		/* just to keep a 0 out of any division or other math */

//absolute max and min values ever used for brake acceleration.  they are scaled to BRAKE_ACC, which has a value from 0-1
#define BRAKE_MAXACC 5000000	/* this is about half of the presumed hardware max, but higer valuse have given errors before */
#define BRAKE_MINACC 1


//scaling values--these are the ones you should change to change the velocity and acceleration of brake movement
#define BRAKE_VEL 1	           /* value from 0-1 that scales the velocity used between the min and max above */
#define BRAKE_ACC 1                /* value from 0-1 that scales the acceleration between the min and max above */


#define BRAKE_MAXPOS 1		/* max braking position on 0-1 pot scale used for pause_estop and full_stop */
#define BRAKE_MAXPOS_COUNTS 200000		/* full throw of lead screw */
#define BRAKE_HOME 0.35			/* home position for brake */

/* Brake commands */
#define BRAKE_POL	0		/* Poll status word */
#define BRAKE_RIO	21		/* Read I/O status */
#define BRAKE_CKS	20		/* Poll internal status */
#define BRAKE_RST	4		/* Restart */
#define BRAKE_MAV	134		/* Move Absolute, Velocity Based */
#define BRAKE_ZTP	145		/* Zero Target & Position */

// Brake actuator pot control lines
#define BPOT_RC PP_PIN01
#define BPOT_CS PP_PIN16
#define BPOT_D8 PP_PIN10
#define BPOT_D9 PP_PIN11
#define BPOT_D10 PP_PIN12
#define BPOT_D11 PP_PIN13
#define BT_OE PP_PIN14

// Brake act pot constants
#define BPOT_TBUSY 8		// max t_BUSY in us
#define BP_CORR_FACTOR -0.06862	// correction for error in adc
#define BP_NUM_DIV 2047		// 12-bit signed ADC => 11-bit positive
#define BP_VREF_LOW 0		// in V, according to ADC chip spec
#define BP_VREF_HIGH 10         // in V, according to ADC chip spec
#define POT_VHIGH 9.71          // 9.73V according to multimeter
                                // 9.71V (1988,1987) according to s/w
#define BP_NUM_HIGH 1987        // Highest number output by ADC box,
                                //   * Corresponds to POT_VHIGH

/* 
 * BP_MIN & BP_MAX: Used in getBrakePot() to scale brake pot reading. 
 *                  These values provide no safety. If want safety, then should
 *                  adjust min and max values in higher brake functions like 
 *                  set_brake_pos_abs_pot(), etc. (Jan 17, 2004)
 */
#define BP_MIN 0.0
#define BP_MAX 1.0

/*
 * Motion stop bits
 *
 * These codes define the stop bits for the motion commands
 * Currently set to zero => no kill bits
 */
#define BRAKE_STOP_ENABLE_ABS 0
#define BRAKE_STOP_STATE_ABS 0

/* 
 * BRAKE_POS_TOLERANCE
 * BRAKE_CYCLE_TIME
 *
 * this is a desensitized zone that prevents the brake from hunting.
 * note that changing this will require a commensurate change in BRAKE_VEL
 * and BRAKE_ACC in vdrive.
 *
 * BRAKE_CYCLE_TIME is the delay at the end of the servo loop (and in
 * the home command) before looking at the brake position.  This should be
 * small enough that motor doesn't move more than BRAKE_POS_TOLERANCE in
 * that amount of time (otherwise we will hunt).
 */

#define BRAKE_POS_TOLERANCE 0.04	// tolerance (out of 0-1),
#define BRAKE_CYCLE_TIME 10000		/* delay at end of servo loop */
#define BRAKE_RESTART_TIME 1		/* Delay time after a restart (sec) */
#define BRAKE_PAUSE_TIME 3		/* Delay time after a pause (sec) */

/* Redefine some variables so that we don't have to modify vdrive */
//#define BRAKE_VEL BRAKE_MAXVEL  //changed these back to the way they worked in brake.c
//#define BRAKE_ACC BRAKE_MAXACC  //changed these back to the way they worked in brake.c
#define brk_count brake_count
#define brk_current_brake_pot brake_pot_current
#define brk_current_abs_pos_buf brake_cmdpos
#define brk_case brake_case
#define brk_current_vel_buf brake_cmdvel
#define brake_fault_reset brake_reset

#endif /* BRAKE2_HH */
