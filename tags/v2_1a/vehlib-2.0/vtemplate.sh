#!/bin/csh
# 
# VNAME.sh - startup script for VNAME
# RMM, 27 Feb 04
#
# This script starts up VNAME on HOST and restarts it if it exits
# unexpectedly.  This contains an absolute path to VNAME so that it
# can be run from /etc/rc.d.
#
# The script tries to be smart about whether it is running on a terminal
# or not and doesn't request input unless it is running on a terminal
#
# Usage:
#   VNAME flags [&]
#
# Environment variables:
#   DGC		path to DGC executables
#   VFLAG	optional flags to pass to VNAME
#

# Set up process limits to allow core files to be generated
limit coredumpsize unlimited

# Make sure we have a path to the file
if (! $?DGC) set DGC=`pwd`
if (! $?VFLAG) set VFLAG="-v"
if (! -e $DGC/VNAME) then
  echo "FATAL: can't find VNAME in $DGC"
  exit
endif

# Print out a banner so that we know where we are running
echo "VNAME@`hostname`: pwd = `pwd`; VFLAG = $VFLAG";

# Check to make sure we are on the right machine
if (`hostname` != "HOST") then
  echo "WARNING: not running on HOST"
  if ($tty != "") then
      echo -n "Continue (y/n)? ";
      if ($< != "y") exit
  endif
endif

# Check to make sure that VNAME is not already running
set VLIST = "`ps -aux | grep VNAME | grep -v VNAME.sh`"
if ("$VLIST" != "") then
  echo "WARNING: VNAME already running on this machine:"
  echo "  $VLIST"
  if ($tty != "") then
      echo -n "Continue (y/n)? ";
      if ($< != "y") exit
  endif

  # Kill all running copies of VNAME
  if (`killall -9 VNAME` != "") then
    echo "WARNING: couldn't kill all copies of VNAME"
    if ($tty != "") then
      echo -n "Continue (y/n)? ";
      if ($< != "y") exit
    endif
  endif
endif

# Run VNAME and restart it whenever it crashes
while (1) 
  echo "======== starting VNAME -d $VFLAG $argv ========"
  $DGC/VNAME -d $VFLAG $argv
  echo "VNAME aborted"

  # kill anything that is left over
  killall -9 VNAME >& /dev/null

  sleep 2	# sleep for a few seconds to let things die
end
