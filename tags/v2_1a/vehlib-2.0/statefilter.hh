#ifndef STATEFILTER_HH
#define STATEFILTER_HH

#include <math.h>

#include "gps.h"
#include "LatLong.h"
#include "VState.hh"


// CONSTANT DECLARATIONS
// obd2 related constant decs
#define OBD_CUTOFF .25 // m/s
#define OBD_VEL_ERR .05 // m/s
#define GPS_VEL_ERR 1 // m/s

// definitions for running without IMU
#define GPS_VEL_CUTOFF .5 // m/s

// constant definitions for jump detection
const double ANGLE_CUTOFF = M_PI/6;
const double ANGLE_TOO_LARGE = M_PI/2;

//const double DV_WEIGHT = 1.5;

// constant declarations for gps error state machine
const int GPS_NORMAL = 0;
const int GPS_JUMPED = 1;

const double ERROR_WEIGHT = 2; // multiply jump by this ratio to get error
const double GPS_DEFAULT_ERROR = 2; // gps position error in meters
const double ERROR_FALLOFF = .95; // multiply error by this amount each loop
const double EARTH_RADIUS = 6378 * 1000; // earth radius in meters

// constant declarations for gps nav state machine
const int GPS_NAV_VALID = 0;
const int GPS_NAV_INVALID = 1;
const int EXT_NAV_INVALID = 2;
const int INVALID_THRESHOLD = 120; // 2 minutes before going into extinv mode 
const int JUMP_RESET_THRESHOLD =  5; // 10 seconds to reset jump finder

// definitions for canceling out pitch/roll of imu mounting
const double IMU_PITCH_OFFSET = .08; // radians
const double IMU_ROLL_OFFSET = 0; // rad
// note these offsets are backwards in sign from the usual convention
// because of the way the transformation is done
const double IMU_X_OFFSET = -1.68;
const double IMU_Y_OFFSET = -.356;
const double IMU_Z_OFFSET = 1.958;


// pitch/roll time constants
const double p_harsh_ratio = .996;
const double r_harsh_ratio = .996;
/*
const int NUM_ELEMENTS = 5;
#define  SigmaMult 4
*/

// detects a jump by looking at the heading, and comparing it to angle
// of displacement, if angle is off by more than 45 degs from the heading
// register a jump
double detect_jump(double cur_n, double cur_e, double update_time, 
	       VState_GetStateMsg vehstate);

// resets the jump detector when gps is lost
void detect_reset();

// 1-d kalman filter that calculates magnetometer error
void magfilter(double magheading, double gpsheading);

// bounds yaw by -pi to pi
double norm_yaw(double yaw);

// sets gps error based on gps state
double gps_err_reset(double gps_err, double gps_jump, double gps_pdop);

// angular rate harshness detection
double p_harsh(double prate);
double r_harsh(double rrate);

/*
// calculates running std. dev of gps 
int dev_reject(double northing, double easting);

// helper funcs for running std. dev
double get_avg();
double get_var();
void filter_reset();
*/


#endif
