#include "VManage.hh"

#include "vehlib/OBDII.h"

#include <iostream>
using namespace std;

extern int DEBUG_ON;
extern int ERR_OUT;

void VManage::OBDLoop() {

#ifndef OBDII
  return; // for now do nothing.
#endif

  // Initialize OBD
  OBD_Init(d.OBDPort);

  while( !ShuttingDown() ) {

    usleep( 300000 );

    double RPM, Velocity;

    RPM = Get_RPM();
    Velocity = Get_Speed();

    // update the datum
    {
      Lock mylock( d.VMDatumMutex );

      if( RPM >= 0.0 && Velocity >= 0.0) {
	d.s.obd.Velocity = Velocity;
	d.s.obd.RPM      = RPM;
	d.s.obd.OBDUpdateCounter ++;
	d.s.obd.valid = true;
      } else {
	d.s.obd.valid = false;
      }

    }
  }
}


