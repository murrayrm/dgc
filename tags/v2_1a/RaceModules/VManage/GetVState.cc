#include "VManage.hh"

#include <iostream>
using namespace std;

extern int DEBUG_ON;
extern int ERR_OUT;

void VManage::GetVStateLoop() {

  VState_Packet myPacket;

  while( !ShuttingDown() ) {

    int ret = Get_VState_Packet( MyAddress() , myPacket);
    if ( ret >= 0 ) {
      Lock mylock( d.VMDatumMutex );
      
      // update datum
      d.s.vstate = myPacket;
      d.VStateLastUpdate = TVNow();
      d.VStateValid = true;
    }

    usleep( 100000 );
  }
}


