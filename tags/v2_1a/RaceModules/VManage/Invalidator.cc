#include "VManage.hh"

#include <iostream>
using namespace std;

extern int DEBUG_ON;
extern int ERR_OUT;

Timeval OBD_Timeout( 1, 0 );
Timeval VState_Timeout( 1, 0);
Timeval EStop_WaitTime( 6, 0); // wait 6 seconds after estop pause released


void VManage::InvalidatorLoop() {

  // Invalidate old data if timeouts
 
  while( !ShuttingDown() ) {

    {
      Lock mylock( d.VMDatumMutex );

      // check obd data
      if ( (d.OBDLastUpdate - TVNow()) > OBD_Timeout) {
	d.s.obd.Velocity = d.s.obd.RPM = d.s.obd.OBDUpdateCounter = 0;
	d.s.obd.valid = false;
      }
      
      // check VState
      if ( (d.VStateLastUpdate - TVNow()) > VState_Timeout) {
	d.VStateValid = false;
      }
      if( d.VStateValid && ! d.VStateEStopOn ) {
	d.VStateEStopOn = false;
	d.EStopGoTime = TVNow();
      } else {
	d.VStateEStopOn = true;
      }




      // check EStop Go
      if( d.KeyboardEStopOn     || d.VMEStopOn     ||
	  d.TransmissionEStopOn || d.VStateEStopOn ||
	  d.EStopMode == 'p'    || d.EStopMode == 'P' ) {
	d.s.vdrive.estop.EStop = 'p';
      } else if( d.EStopMode == 'd'|| d.EStopMode == 'D') {
	d.s.vdrive.estop.EStop = 'd';
      } else if( ! d.KeyboardEStopOn && 
		 ( d.EStopMode == 'o' || d.EStopMode == 'O') &&
		 (TVNow() - d.EStopGoTime) > EStop_WaitTime) {
	// we can go now
	d.s.vdrive.estop.EStop = 'o';
      }
    }


    usleep( 30000 );
  }
}


