
#include "DFEPlanner.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include <unistd.h>
#include <iostream>

extern int SIM;
extern int DISPLAY;
extern int PRINT_VOTES;


DFEPlanner::DFEPlanner() 
  : DGC_MODULE(MODULES::DFEPlanner, "DFE Planner", 0) {

}

DFEPlanner::~DFEPlanner() {
}


void DFEPlanner::Init() {
  // call the parent init first
  DGC_MODULE::Init();

  InitDFE();
  cout << "Module Init Finished" << endl;
}

void DFEPlanner::Active() {

  cout << "Starting Active Funcion" << endl;
  while( ContinueInState() ) {

    // update the state so we know where the vehicle is at
    UpdateState();

    // Update the votes ... 
    DFEUpdateVotes();
    // and send them to the arbiter
    Mail m = NewOutMessage(MyAddress(), MODULES::Arbiter, ArbiterMessages::Update);
    // Send the arbiter our "ID" and the vote struct.  The weights are taken care of
    // by the arbiter.
    int myID = ArbiterInput::DFE;
    m << myID << d.dfe;
    SendMail(m);

    // and sleep for a bit
    usleep(100000);
  }

  cout << "Finished Active State" << endl;

}


void DFEPlanner::Shutdown() {

  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;
  DFEShutdown();

}



