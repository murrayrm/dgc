#include "ladar_power.h"
#include "LADARMapper.hh"
#include "ladar.hh"
#include "laserdevice.h"
#include "Constants.h" //serial ports, other constants
#include "genMap.hh"
#include "frames/frames.hh"
#include <iostream.h>
#include <fstream.h>

// #define ASCII_MAP

// FIXME: Eventually we want to load these from a config file or something
#define ROOF_LADAR_SCAN_ANGLE 100
#define ROOF_LADAR_RESOLUTION 0.5
#define ROOF_LADAR_UNITS 0.01

#define BUMPER_LADAR_SCAN_ANGLE 180
#define BUMPER_LADAR_RESOLUTION 0.5
#define BUMPER_LADAR_UNITS 0.01

#define LADAR_VEHICLE_LENGTH_MULTIPLIER 1.0
#define LADAR_VEHICLE_WIDTH_MULTIPLIER 1.0


extern int LADAR_POWER_CYCLE;
extern int SIM;         // get the simulation flag from the main function
extern int LOG_SCANS;
extern int RUNTYPE;
extern int USE_BUMPER;  // says whether or not to use the bumper-mounted ladar.
int SAVE_GENMAP = false;
int CLEAR_GENMAP = false;
extern int NOPITCH;
extern int NOROLL;
extern int QUIT_PRESSED;
extern char *ROOF_TTY;
extern char *BUMPER_TTY;
extern char *TESTNAME;
extern double GENMAP_OBS_THRESH;
extern double GENMAP_HEIGHT_THRESH;
extern double GENMAP_VECT_THRESH;
extern double GENMAP_TIRE_THRESH;
int BumperReset = 0;
int RoofReset = 0;

FILE* scanlog;           // Log files for various data
FILE* errorlog;          // Records any errors reported by the scanner
FILE* statelog;
FILE* votelog;

// genMap stuff
XYZcoord roofLadarOffset(ROOF_LADAR_OFFSET_X, ROOF_LADAR_OFFSET_Y, ROOF_LADAR_OFFSET_Z);
XYZcoord bumperLadarOffset(BUMPER_LADAR_OFFSET_X, BUMPER_LADAR_OFFSET_Y, BUMPER_LADAR_OFFSET_Z);
frames roofLadarFrame;
frames bumperLadarFrame;

#ifdef ASCII_MAP
#define DEFAULT_LADARMAP_NUM_ROWS 30 
#define DEFAULT_LADARMAP_NUM_COLS 30
#define DEFAULT_LADARMAP_ROW_RES  10
#define DEFAULT_LADARMAP_COL_RES  10

#endif

// LADAR Driver class
CLaserDevice roofLaser;
CLaserDevice bumperLaser;

/*****************************************************************************/
/*****************************************************************************/
int LADARMapper::InitLadar() {
  int result;
  char * port_name;
  char filename[255];
  char datestamp[255];
  int status;
  unsigned char data[1024];
  bool good_to_go = true;

  printf("genMap num rows: %d\n", DEFAULT_LADARMAP_NUM_ROWS ); 
  printf("genMap num cols: %d\n", DEFAULT_LADARMAP_NUM_COLS ); 

  // Open log file if LOG_SCANS is set
  if( LOG_SCANS ) {
    // filename will be TESTNAME_ddmmyyyy_hhmmss.log
    time_t t = time(NULL);
    tm *local;
    local = localtime(&t);
    int i;

    sprintf(datestamp, "%02d%02d%04d_%02d%02d%02d",
            local->tm_mon+1, local->tm_mday, local->tm_year+1900,
            local->tm_hour, local->tm_min, local->tm_sec);

    sprintf(filename, "testRuns/%s_scans_%s.log", TESTNAME, datestamp);
    printf("Logging scans to file: %s\n", filename);
    scanlog=fopen(filename,"w");
    if ( scanlog == NULL ) {
      printf("Unable to open scan log!!!\n");
      exit(-1);
    }
    // print data format information in a header at the top of the scanlog file
    fprintf(scanlog, "%% angle = %d\n", ROOF_LADAR_SCAN_ANGLE);
    fprintf(scanlog, "%% resolution = %lf\n", ROOF_LADAR_RESOLUTION);
    fprintf(scanlog, "%% units = %lf\n", ROOF_LADAR_UNITS);
    fprintf(scanlog, "%% Time[sec] | scan points\n");

    sprintf(filename, "testRuns/%s_state_%s.log", TESTNAME, datestamp);
    printf("Logging state to file: %s\n", filename);
    statelog=fopen(filename,"w");
    if ( statelog == NULL ) {
      printf("Unable to open state log!!!\n");
      exit(-1);
    }
    // print data format information in a header at the top of the state file
    fprintf(statelog, "%% Time[sec] | Easting[m] | Northing[m] | Altitude[m]");
    fprintf(statelog, " | Vel_E[m/s] | Vel_N[m/s] | Vel_U[m/s]" );
    fprintf(statelog, " | Speed[m/s] | Accel[m/s/s]" );
    fprintf(statelog, " | Pitch[rad] | Roll[rad] | Yaw[rad]" );
    fprintf(statelog, " | PitchRate[rad/s] | RollRate[rad/s] | YawRate[rad/s]");
    fprintf(statelog, "\n");

    sprintf(filename, "testRuns/%s_votes_%s.log", TESTNAME, datestamp);
    printf("Logging votes to file: %s\n", filename);
    votelog=fopen(filename,"w");
    if (votelog == NULL) {
      printf("Unable to open vote log!!!\n");
      exit(-1);
    }

    fprintf(votelog, "%% phi ");
    for( i=0; i<NUMARCS; i++) {
      fprintf(votelog, "%f ", GetPhi(i));
    }
    fprintf(votelog, "\n");
    fprintf(votelog, "%% tstamp (v1,g1) (v2,g2) .... (vn,gn)\n");
    
    sprintf(filename, "testRuns/%s_errors_%s.log", TESTNAME, datestamp);
    printf("Logging errors to file: %s\n", filename);
    errorlog=fopen(filename,"w");
    if ( errorlog == NULL ) {
      printf("Unable to open error log!!!\n");
      exit(-1);
    }
  }

  // genMap init
  d.ladarTerMap.initMap(DEFAULT_LADARMAP_NUM_ROWS,
                        DEFAULT_LADARMAP_NUM_COLS,
                        DEFAULT_LADARMAP_ROW_RES,
                        DEFAULT_LADARMAP_COL_RES, false);

  d.ladarTerMap.initThresholds(GENMAP_OBS_THRESH,
			       GENMAP_HEIGHT_THRESH,
			       GENMAP_TIRE_THRESH,
			       GENMAP_VECT_THRESH,
			       LADAR_VEHICLE_LENGTH_MULTIPLIER,
			       LADAR_VEHICLE_WIDTH_MULTIPLIER);
  
  d.ladarTerMap.initPathEvaluationCriteria(false,false,false,true,false,true,false,true);

  d.ladarTerMap.initVelocityThresholds(LADAR_MAX_SPEED,LADAR_MIN_SPEED,
                                       LADAR_DIST_TO_OBSTACLE_ON_ARC_HALT,
                                       LADAR_DIST_TO_OBSTACLE_ANYWHERE_HALT,
                                       LADAR_DIST_TO_OBSTACLE_ON_ARC_MAX_SPEED,
                                       LADAR_DIST_TO_OBSTACLE_ANYWHERE_MAX_SPEED);

  d.ladarTerMap.displayThresholds();

  roofLadarFrame.initFrames(roofLadarOffset, ROOF_LADAR_PITCH, ROOF_LADAR_ROLL, ROOF_LADAR_YAW);
  bumperLadarFrame.initFrames(bumperLadarOffset, BUMPER_LADAR_PITCH, BUMPER_LADAR_ROLL, BUMPER_LADAR_YAW);
 
  // Turn on LADAR units
  ladar_pp_init();

  if(LADAR_POWER_CYCLE) {
    cout << "POWER CYCLING LADARS....PLEASE WAIT 80 SECONDS" << endl;
    ladar_off(LADAR_BUMPER);
    ladar_off(LADAR_ROOF);
    sleep(5);
    ladar_on(LADAR_BUMPER);
    ladar_on(LADAR_ROOF);
    sleep(80);
  }
 
  // Ladar initialization tasks
  if( !SIM ) {
    while( roofLaser.Setup(ROOF_TTY,ROOF_LADAR_SCAN_ANGLE,ROOF_LADAR_RESOLUTION) != 0 ) {
      sleep(1);
    }
    if ( USE_BUMPER ) {
      while( bumperLaser.Setup(BUMPER_TTY,BUMPER_LADAR_SCAN_ANGLE,BUMPER_LADAR_RESOLUTION) != 0 ) {
	sleep(1);
      }
    }
    // Do a self-test of the roof ladar
    printf("\n\nPERFORMING ROOF LADAR SELF-TEST...IF THIS DOES NOT RETURN\n");
    printf("AFTER A COUPLE OF SECONDS, THEN IT FAILED!!!!\n");
    status = roofLaser.LockNGetData( data );
    if((status & 0xFF) != 0x10) {
      printf("ROOF LADAR ERROR!  STATUS: %X\n", status);
      good_to_go = false;
    }
    if ( USE_BUMPER ) {
      printf("\n\nPERFORMING BUMPER LADAR SELF-TEST...IF THIS DOES NOT RETURN\n");
      printf("AFTER A COUPLE OF SECONDS, THEN IT FAILED!!!!\n");
      status = bumperLaser.LockNGetData( data );
      if((status & 0xFF) != 0x10) {
        printf("BUMPER LADAR ERROR!  STATUS: %X\n", status);
        good_to_go = false;
      }
    }
    if(!good_to_go) exit(-1); 
    return true;
  } 
  else {
    // Do simulation stuff here...
    return true;
  }  
} // end LADARMapper::InitLadar()

/*****************************************************************************/
/*****************************************************************************/
void LADARMapper::LadarUpdateVotes() {
  int i;
  double *roofscan;
  double *bumperscan;
  double tstamp, phi, centDist;
  int numRoofScanPoints, numBumperScanPoints;
  int roofStatus, bumperStatus;
  vector< pair<double, double> > voteList;
  vector<double> steeringAngleList;
  steeringAngleList.reserve(NUMARCS);
  XYZcoord npoint(0, 0, 0), scanpoint(0, 0, 0);
  CellData newCell;
  double roofOffset;
  double bumperOffset;

  Timeval tv = TVNow();
  // Starting angle of roof scan
  roofOffset = ((180.0 - ROOF_LADAR_SCAN_ANGLE) / 2.0) * M_PI / 180.0;
  bumperOffset = ((180.0 - BUMPER_LADAR_SCAN_ANGLE) / 2.0) * M_PI / 180.0;
 
  if(SAVE_GENMAP) {
    SaveGenMap();
    SAVE_GENMAP = false;
  }
  if(CLEAR_GENMAP) {
    ClearGenMap();
    CLEAR_GENMAP = false;
  }
 
  if(SIM) {
    numRoofScanPoints = (int)(ROOF_LADAR_SCAN_ANGLE / ROOF_LADAR_RESOLUTION + 1);
    numBumperScanPoints = (int)(BUMPER_LADAR_SCAN_ANGLE / BUMPER_LADAR_RESOLUTION + 1);
  } else {
    numRoofScanPoints = roofLaser.getNumDataPoints();
    if (USE_BUMPER) numBumperScanPoints = bumperLaser.getNumDataPoints();
  }
  roofscan = (double *) malloc(numRoofScanPoints * sizeof(double));
  bumperscan = (double *) malloc(numBumperScanPoints * sizeof(double));
  
  /* Steps - 1. Take a scan from the ladar.
             2. Fill in genMap
             3. Compute the votes for the various steering angles
  */
  // Log the vehicle state
  if (LOG_SCANS) {
    tstamp = (double)(d.SS.Timestamp.sec()) + d.SS.Timestamp.usec()/1000000.0;
    fprintf(scanlog, "%f ", tstamp);
    // Print state      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 
    fprintf(statelog, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",
	    tstamp, d.SS.Easting, d.SS.Northing, d.SS.Altitude,
	    d.SS.Vel_E, d.SS.Vel_N, d.SS.Vel_U,
	    d.SS.Speed, d.SS.Accel, 
	    d.SS.Pitch, d.SS.Roll, d.SS.Yaw, 
	    d.SS.PitchRate, d.SS.RollRate, d.SS.YawRate );
    fflush(statelog);
  }
  
  // Lets update the state right before we take a scan...
  UpdateState();
  if( NOPITCH ) {
    d.SS.Pitch = 0.0;
  }
  if( NOROLL ) {
    d.SS.Roll = 0.0;
  }

  // Step 1 - Take a scan
  if( !SIM ) { 
    roofStatus = roofLaser.UpdateScan();
    RoofReset = roofLaser.isResetting();
    if ( USE_BUMPER ) {
      bumperStatus = bumperLaser.UpdateScan();
      BumperReset = bumperLaser.isResetting();
    }
    if (LOG_SCANS) {
      fprintf(errorlog,"%f 0x%04X\n", tstamp, roofStatus);
      fflush(errorlog);
    }
    // update local data buffer
    for(int i = 0; i < numRoofScanPoints; i++) {
      if (LOG_SCANS) {
        fprintf(scanlog,"%d ", roofLaser.Scan[i]);
      }
      if(roofLaser.isDataError(roofStatus,roofLaser.Scan[i]) || RoofReset)
        roofscan[i] = -1;
      else
        roofscan[i] = (roofLaser.Scan[i] * ROOF_LADAR_UNITS);
    }
    if ( USE_BUMPER ) {
      for(int i = 0; i < numBumperScanPoints; i++) {
	if(bumperLaser.isDataError(bumperStatus,bumperLaser.Scan[i]) || BumperReset)
	  bumperscan[i] = -1;
	else
	  bumperscan[i] = (bumperLaser.Scan[i] * BUMPER_LADAR_UNITS);
      }
    }
  } 
  // or fake a scan
  else {
    // the distance of the center scanline to flat ground
    // assumes zero roll and zero yaw for sensor
    // and zero roll and zero pitch for vehicle...
//    printf("HEY - YOU ARE USING SIM, THIS DOESN'T SUPPORT SIMULATING 2 LADARS YET!!!");
    centDist = ROOF_LADAR_OFFSET_Z/tan(ROOF_LADAR_PITCH);
    for (int i = 0; i < numRoofScanPoints; i++) {
      phi = roofOffset + (i * ROOF_LADAR_RESOLUTION * M_PI) / 180.0;
      roofscan[i] = centDist / sin(phi);
    }
    if (LOG_SCANS) {
      for (int i = 0; i < numRoofScanPoints; i++) {
        fprintf(scanlog,"%d ", (int) (roofscan[i] * 100.0) );
      }
    }
  } // end getting new scan
  if (LOG_SCANS) fprintf(scanlog,"\n");
  if (LOG_SCANS) fflush(scanlog);
  
  /* ***************************************************** */
  /* ***************************************************** */
  // Step 2 - fill in the map...
  
  // update internal state of frames
  roofLadarFrame.updateState(d.SS);
  bumperLadarFrame.updateState(d.SS);
  // update internal state of the genMap
  d.ladarTerMap.updateFrame(d.SS,true,d.SS.Timestamp.dbl());

  if(!RoofReset) {
    // fill in the map with the new roof scan
    for(int i = 0; i < numRoofScanPoints; i++) {
      phi = roofOffset + (i * ROOF_LADAR_RESOLUTION * M_PI) / 180.0;
      if(roofscan[i] < 0 || roofscan[i] >= ROOF_LADAR_MAX_RANGE) {
        // If its < 0 then we got an error from the ladar, so ignore the
        // scanpoint, and dont stick it in the map.  If its max range or
        // larger, we either got an error or we got a good scanpoint and
        // there isn't anything there.  so ignore the scanpoint, and
        // dont stick it in the map.
        continue;
      }
  
      // convert the range into coordinates in the sensors frame
      // sensor x-axis points straight ahead, y-axis to the left, and
      // z-down, thus the scanline at 90 degrees will have a 0 y-coord.
      // All of the data will have a zero z-value in the sensors frame.
      scanpoint.x = roofscan[i]*sin(phi);
      scanpoint.y = roofscan[i]*cos(phi);
      scanpoint.z = 0;
  
      // convert the sensor frame coords into utm coordinates and put
      // this into the map
      npoint = roofLadarFrame.transformS2N(scanpoint);
      newCell.value = npoint.z;
      // we only overwrite current data if the new data is higher in
      // elevation than the old data...
      // maybe we should only do absolute values, since a big hole is bad too...
      if ( fabs(npoint.z) > fabs(d.ladarTerMap.getCellDataUTM(npoint.x,npoint.y).value) ||
  	 d.ladarTerMap.getCellDataUTM(npoint.x,npoint.y).value == NO_DATA_CELL.value ) {
        d.ladarTerMap.setCellDataUTM(npoint.x,npoint.y,newCell);
      }
    } // end looping through new roofscan points
  }

  // now fill in the map with the new bumper scan
  if ( USE_BUMPER && !BumperReset) {
    for(int i = 0; i < numBumperScanPoints; i++) {
      phi = bumperOffset + (i * BUMPER_LADAR_RESOLUTION * M_PI) / 180.0;
      if(bumperscan[i] < 0 || bumperscan[i] >= BUMPER_LADAR_MAX_RANGE) {
	// If its < 0 then we got an error from the ladar, so ignore the
	// scanpoint, and dont stick it in the map.  If its max range or
	// larger, we either got an error or we got a good scanpoint and
	// there isn't anything there.  so ignore the scanpoint, and
	// dont stick it in the map.
	continue;
      }
      
      // convert the range into coordinates in the sensors frame
      // sensor x-axis points straight ahead, y-axis to the left, and
      // z-down, thus the scanline at 90 degrees will have a 0 y-coord.
      // All of the data will have a zero z-value in the sensors frame.
      scanpoint.x = bumperscan[i]*sin(phi);
      scanpoint.y = bumperscan[i]*cos(phi);
      scanpoint.z = 0;
      
      // convert the sensor frame coords into utm coordinates and put
      // this into the map
      npoint = bumperLadarFrame.transformS2N(scanpoint);
      newCell.value = npoint.z;
      // we only overwrite current data if the new data is higher in
      // elevation than the old data...
      // maybe we should only do absolute values, since a big hole is bad too...
      if ( fabs(npoint.z) > fabs(d.ladarTerMap.getCellDataUTM(npoint.x,npoint.y).value) ||
	   d.ladarTerMap.getCellDataUTM(npoint.x,npoint.y).value == NO_DATA_CELL.value ) {
	d.ladarTerMap.setCellDataUTM(npoint.x,npoint.y,newCell);
      }
    } // end looping through new bumperscan points
  }
  
  /* ***************************************************** */
  /* ***************************************************** */
 
  #ifdef ASCII_MAP
  d.ladarTerMap.display();
  #endif
 
  // Step 3 - figure out the votes...

  if(LOG_SCANS) {
    fprintf(votelog, "%f ", tstamp);
  }
  for( i=0; i<NUMARCS; i++) {
    phi = GetPhi(i);
    steeringAngleList.push_back(phi);
  }
  voteList = d.ladarTerMap.generateArbiterVotes(NUMARCS, steeringAngleList);

  for( i=0; i<NUMARCS; i++) {
    #ifdef ASCII_MAP
    printf("%2.1lf %3.1lf\n", voteList[i].first, voteList[i].second);
    #endif
    d.ladar.Votes[i].Velo     = voteList[i].first; 
    if(BumperReset && RoofReset)
      d.ladar.Votes[i].Goodness = LADAR_RESET;
    else d.ladar.Votes[i].Goodness = voteList[i].second;

    if(LOG_SCANS) {
      fprintf(votelog, "(%lf,%lf) ", d.ladar.Votes[i].Velo, d.ladar.Votes[i].Goodness);
    }
  } // end iterating over votes/arcs
  if(LOG_SCANS) fprintf(votelog, "\n");
  if(LOG_SCANS) fflush(votelog);

  #ifdef ASCII_MAP
  getchar();
  system("clear");
  #endif
 
  // HACK? ADDED THURSDAY NIGHT BEFORE RACE
  // LIMIT TOP SPEED OF VEHICLE USING RAW BUMPER DATA
  // AND SLAM BRAKES (V = 0.0) IF TOO FAST
  // THIS IS BASED ON REACTION TEST PERFORMED THAT SAME NIGHT
  // THAT INDICATES WE CAN STOP IN 25m GOING AT 10 m/s
  // SO ADD 7m DUE TO TIME TRAVELLED WHILE PROCESSING LADAR
  // SCANS...THEN ADD SOME BUFFER SO WE COMMAND TO STOP AT 40m
  // IF GOING 10 m/s

  double minBumperDist = BUMPER_LADAR_MAX_RANGE;
  if(USE_BUMPER && !BumperReset) {
    for(int i = (numBumperScanPoints-1)/2-BUMPER_FIELD_OF_VIEW; i < (numBumperScanPoints-1)/2+BUMPER_FIELD_OF_VIEW; i++) {
      if(bumperscan[i] < minBumperDist) minBumperDist = bumperscan[i];
    }
  }
  double scale = BUMPER_DIST / BUMPER_SPEED;
 
  for( i=0; i<NUMARCS; i++) {
    if(!USE_BUMPER || BumperReset) {
      // LIMIT TOP SPEED
      if(d.ladar.Votes[i].Velo > NO_BUMPER_SAFE_SPEED)
        d.ladar.Votes[i].Velo = NO_BUMPER_SAFE_SPEED;
    } else {
      if((minBumperDist > BUMPER_MIN_DIST)) {
        if(minBumperDist < scale * d.SS.Speed) d.ladar.Votes[i].Velo = 0.0;
        else if(d.ladar.Votes[i].Velo > (minBumperDist / scale))
          d.ladar.Votes[i].Velo = (minBumperDist / scale);
        if(d.ladar.Votes[i].Velo > (BUMPER_MAX_DIST / scale)) 
          d.ladar.Votes[i].Velo = (BUMPER_MAX_DIST / scale);
      }
    }
  }
 
  free(roofscan);
  free(bumperscan);

  Timeval tv_end = TVNow();
  // as of thursday this takes 420 ms!!!
//  fprintf(stderr, "%lf seconds elapsed\n", (tv_end.dbl() - tv.dbl()));
} // end LADARMapper::LadarUpdateVotes()

void LADARMapper::LadarShutdown() {
  fclose(scanlog);
  fclose(statelog);
  roofLaser.LockNShutdown();
  bumperLaser.LockNShutdown();
}

void LADARMapper::SaveGenMap() {
  char datestamp[255],filename[255];

  time_t t = time(NULL);
  tm *local;
  local = localtime(&t);

  sprintf(datestamp, "%02d%02d%04d_%02d%02d%02d",
          local->tm_mon+1, local->tm_mday, local->tm_year+1900,
          local->tm_hour, local->tm_min, local->tm_sec);

  sprintf(filename, "testRuns/%s_%s", TESTNAME, datestamp);

  d.ladarTerMap.saveMATFile(filename,"",TESTNAME);
}

void LADARMapper::ClearGenMap()
{
  d.ladarTerMap.clearMap();
}

// end ladar.cc
