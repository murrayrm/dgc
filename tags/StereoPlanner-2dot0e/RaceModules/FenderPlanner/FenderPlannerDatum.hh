#ifndef FENDERPLANNERDATUM_HH
#define FENDERPLANNERDATUM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "RaceModules/Arbiter/Vote.hh"
#include "RaceModules/Arbiter/SimpleArbiter.hh"
#include "RaceModules/Arbiter/ArbiterModule.hh"
#include "staticFender2rddf.hh"
#include "fenderDDF.hh"
#include "RDDF/rddf.hh"

#include "Constants.h" //for vehicle constants, ie serial ports, etc.

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"
#include "vehlib/VDrive.hh"


using namespace std;

// All data that needs to be shared by common threads
struct FenderPlannerDatum {

  Voter fender; // voter struct for the Arbiter

  // State struct from VState
  boost::recursive_mutex StateStructLock;
  VState_GetStateMsg SS;

  // waypoint data
  RDDF rddf;
  // fenderpoint data
  fenderDDF fenderpoints;

};


#endif
