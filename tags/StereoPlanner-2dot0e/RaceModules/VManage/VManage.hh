#ifndef VDRIVE_HH
#define VDRIVE_HH

#include "MTA/Kernel.hh"
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <sys/types.h>
#include <unistd.h>
#include "vehlib/vehports.h"
#include "vehlib/transmission.h"
#include "vehlib/VState.hh"
#include "vehlib/statefilter.hh"

#include "vehlib/VStarPackets.hh"
#include "vehlib/VStarDevices.hh"
#include "vehlib/VStarMessages.hh"

#include <string>
using namespace std;
typedef boost::recursive_mutex::scoped_lock Lock;


// ---------------------------------------------------------
// "Long Term" things VManage is trying to accomplish
struct VManageGoals {
  

  // Changing Gears, possibly Reverse to Forward or vice versa.
  int SetTransmissionGear;        // --this is what gear we want to set--


  // Slow EStop
  double EStopDecelerationRate; // if we want to slow down if we get an EStop 
                                // instead of slamming on the brakes.

  // default constructor
  VManageGoals() : EStopDecelerationRate(0.0) {}
};



// This datum is the basis for the VManage decision matrix.
// ---------------------------------------------------------
struct VManageDatum {

  // Lock for altering this datum
  boost::recursive_mutex VMDatumMutex;

  // Packets for Hardware Devices
  // all are already defined in the stack
  VManageStack s;

  VManageGoals g;

  // Timeval: VManage - Start Time
  Timeval TimeZero;

  // OBD Stuff
  int OBDPort;             // Serial port for OBD2
  Timeval OBDLastUpdate;

  // Internal EStop Variables (may differ from what the world sees)
  char EStopMode;          // is the physical estop set?
  int  VMEStopOn;          // is VManage internally estopping (to change gears, etc)?
  int  KeyboardEStopOn;    // is a keyboard estop currently set?
  Timeval EStopGoTime;     // when did we find we could go
                           // (i.e. atleast one of the estop variables
                           // above changed from estop pause or 
                           // disable to no estop).


  // More EStop Stuff
  int EStopPort;           // Parallel port for OBD2
  int EStopValid;          // if we are having trouble reading the EStop
                           // this will be set to true and a "pause" will
                           // be sent to VDrive.
  Timeval EStopLastUpdate; // -- Timestamp of last valid read of EStop parallel port 

  // Transmission/Ignition Stuff
  int TransmissionPort;  
  int TransmissionEStopOn;
  int IgnitionPort;
  boost::recursive_mutex ParPortMutex; // Estop, Trans, Ig all on same parallel port, therefore must lock

  // VState stuff
  Timeval VStateLastUpdate; // last good state update
  int VStateValid;          // VState.
  int VStateEStopOn;        // vstate estop flag
  
  // VDrive counter
  int VM_for_VD_Count;

};








// ---------------------------------------------------------
class VManage : public DGC_MODULE {

public:
  VManage(int argc, char **argv);
  ~VManage();
  

  // Init, Active states
  void Init();
  void Active();
  
  // Status string for display.
  string Status();

  // Mail Handlers
  void InMailHandler(Mail& ml);
  Mail QueryMailHandler(Mail& ml);


private:

  int argc;     // command line argument variables
  char ** argv;

  // The Thread Functions ...
  void GoalsLoop();      // the guts of VManage.
  void EStopLoop();         // watches estop and updates it.
  void OBDLoop();           // reads OBD-II continuously.
  void GetVStateLoop();     // gets vstate
  void InvalidatorLoop();   // expires data if we havent read from 
                            // the device in a while (also, controls 
                            // estop delay)
 
  void ConsoleLoop();  // console interface


  VManageDatum d;


  
  // Functions for VManage to fill these packets
  Transmission_Packet Fill_Transmission_Packet();
  Ignition_Packet     Fill_Ignition_Packet();
  EStop_Packet        Fill_EStop_Packet();
  OBD_Packet          Fill_OBD_Packet();

};








#endif
