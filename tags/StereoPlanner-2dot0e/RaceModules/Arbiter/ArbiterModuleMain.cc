#include "ArbiterModule.hh"
#include <getopt.h>

using namespace std;

#define LOG_OPT  10
#define HELP_OPT 11

int   LOG_DATA = 0;     // whether or not to log state and command data
char *TESTNAME;         // id for test to be part of log file name
int noGlobal = 0;       /* whether or not we want the lack of Global to stop
                           Arbiter non-zero commands. */
int noState = 0;        /* Whether we care or not that we have state data.
                         * The arbiter doesn't really care about state (as far
                         * as code goes at least), but other modules will be
                         * unreliable w/o up-to-date & correct state data.
                         */
int noObstacle = 0;     /* whether or not we want the lack of obstacle detection
				   (Stereo/LADAR) to stop Arbiter from sending non-zero
			         commands. */
int reverseOn = 0;      /* whether we want to go into REVERSE mode */


int main(int argc, char **argv) 
{
  //-----------------------------------------------------------------------//
  // Do some argument handling
  int c;

  while (1) {
    int option_index = 0;
    static struct option long_options[] = {
      {"log",          1, 0,             LOG_OPT},
      {"help",         0, 0,             HELP_OPT},
      {"noglobal",     0, &noGlobal,     1},
      {"nostate",      0, &noState,      1},
      {"noobstacle",   0, &noObstacle,   1},
      {"reverse",      0, &reverseOn,   1},
      {0,0,0,0}
    };

    c = getopt_long_only (argc, argv, "", long_options, &option_index);
    if(c == -1) break;

    switch(c) {
      case LOG_OPT:
        LOG_DATA = 1;
        TESTNAME = strdup(optarg);
        break;
      case HELP_OPT:
        cout << endl << "Arbiter Usage:" << endl << endl;
        cout << "Arbiter [-log testname] [-noglobal] [-nostate] [-noobstacle] [reverse]" 
		 << endl;
        cout << "  -log testname  Log state and commands in testRuns subdirectory" << endl;
        cout << "  -help          Displays this help" << endl << endl;
        exit(0);
    }
  } // end while(1)

  cout << endl << "Argument summary:" << endl;
  cout << "  LOG_DATA: " << LOG_DATA;
  if( LOG_DATA ) cout << "  test name: " << TESTNAME;
  cout << endl;
  // Done doing argument handling
  //-----------------------------------------------------------------------//

  
  // Start the MTA By registering a module
  // and then starting the kernel

  Register(shared_ptr<DGC_MODULE>(new Arbiter));
  StartKernel();

  return 0;
}
