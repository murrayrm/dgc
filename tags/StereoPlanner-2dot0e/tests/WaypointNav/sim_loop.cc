/***************************************************************************************/
//FILE: 	sim_loop.cc
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file has the loop that will run in one of the threads
//
/***************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>

#include "WaypointNav.hh"
#include "models/model_functions.h"
#include "latlong/ggis.h"
#include "latlong/contest-info.h"

using namespace std;

double t;
double northing,easting,vel_n,vel_e,vel_u,vel,theta;
double accel,steer;

int cruiseOrder   = 1;
int steeringOrder = 3;
int engineOrder   = 1;
double cruiseVector[1];	// state vector of the cruise control (velocity)
double steeringVector[3];	// state vector of the planar dynamics (x, y, theta)
double engineVector[1];	// state vector of the engine (torque)
double cruiseInput[1], steeringInput[2], engineInput[2];
double throttleOutput, brakeOutput;

void WaypointNav::SimInit() {

  // Initializing
  // TODO: Initialize variables.
  t = 0;
}
  
void WaypointNav::SimUpdate(float deltaT) {

  // Getting the actual state
  northing = d.SS.Northing;
  easting = d.SS.Easting; 
  
  vel_e = d.SS.Vel_E;
  vel_n = d.SS.Vel_N;
  vel_u = d.SS.Vel_U;
  vel = sqrt(vel_n*vel_n + vel_e*vel_e + vel_u*vel_u);
  theta = d.Yaw;
  accel = d.Accel;
  steer = -d.Steer*MAX_STEER; //the convention of the simulator signal is other than the program
  
  // Numerical integration...
  cruiseVector[0] = vel;
  steeringVector[0] = easting;   // x
  steeringVector[1] = northing;  // y
  steeringVector[2] = theta;     // heading
  
  // Getting the acceleration torque
  throttleOutput = throttleActuator(accel);
  
  // Getting the brake torque
  brakeOutput = brakeActuator(accel,vel);
  
  // Passing thru the engine lag
  engineInput[0] = ENGINE_LAG;
  engineInput[1] = throttleOutput;
  runge4(engineOrder, t, engineVector, deltaT,f_lag,engineInput);
  
  // Converting the torques into forces
  cruiseInput[0] = tire(engineVector[0],brakeOutput);
  
  // Computing next state, assuming the actuator as a force
  runge4(cruiseOrder, t, cruiseVector, deltaT,fv,cruiseInput);
  vel = cruiseVector[0];
  
  // Getting the actual stearing position
  // steeringInput[0] = d.steeringDW.data.actuator;
  steeringInput[0] = steer;
  steeringInput[1] = vel;
  
  // Computing next state of the planar movement
  runge4(steeringOrder, t, steeringVector, deltaT,fsteering,steeringInput);
  easting 	= steeringVector[0];
  northing 	= steeringVector[1];
  theta = steeringVector[2];
  
  
  // Updating the next state...
  d.SS.Pitch = 0;
  d.SS.Roll = 0;
  d.SS.Yaw = theta*180/M_PI;
  
  d.SS.Altitude = 0;
  
  d.SS.Vel_U = 0;
  d.SS.Vel_N = vel*sin(theta);
  d.SS.Vel_E = vel*cos(theta);
  d.SS.Speed = vel;
  
  d.SS.Easting = easting;
  d.SS.Northing = northing;
  
  t += deltaT;

} // end SimLoop()
