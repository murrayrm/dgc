// mostly by Jeremy, rewritten by Gunnar to use getpair.h


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string>
#include <iostream.h>
#include <fstream.h>
#include <cstdio>

//OpenCV Includes
#include <cv.h>
#include <highgui.h>
#include <cvaux.h>

#include <stereovision/getpair.h>

#define LEFT_FILE_NAME_PREFIX_DEFAULT    "Default"
#define RIGHT_FILE_NAME_PREFIX_DEFAULT   "Default"
#define LEFT_FILE_NAME_SUFFIX_DEFAULT    "-L"
#define RIGHT_FILE_NAME_SUFFIX_DEFAULT   "-R"
#define FILE_TYPE_DEFAULT "bmp"
#define MAX_FRAMES_DEFAULT          10

int main(int argc, char *argv[]) 
{
  FILE* imagefile;
 
  int i, j, k, max_frames=MAX_FRAMES_DEFAULT, test_num=0, verbose=0, prompt=0, delay=0, which_pair=SHORT_RANGE;
  char left_file_name_prefix[200], right_file_name_prefix[200], left_file_name_suffix[40], right_file_name_suffix[40], file_type[10];
  IplImage* stereoImages[2];
  CvSize imageSize;


  //Set defaults for filenames
  strcpy(left_file_name_prefix, LEFT_FILE_NAME_PREFIX_DEFAULT);
  strcpy(left_file_name_suffix, LEFT_FILE_NAME_SUFFIX_DEFAULT);
  strcpy(right_file_name_prefix, RIGHT_FILE_NAME_PREFIX_DEFAULT);
  strcpy(right_file_name_suffix, RIGHT_FILE_NAME_SUFFIX_DEFAULT);
  strcpy(file_type, FILE_TYPE_DEFAULT);

  //Print usage info
  for(i=1; i<argc; i++) {
    if(strcmp(argv[i], "-help")==0 || strcmp(argv[i], "--help")==0) {
      printf("\nUsage instructions");
      printf("\nThere are four ways to name the logged images files: ");
      printf("\nDefault:                          Default0000-L.pgm,   Default0000-R.pgm");
      printf("\n-lname leftname -rname rightname: leftname0000.pgm,    rightname0000.pgm");
      printf("\n-name somename:                   somename0000-L.pgm, somename0000-R.pgm");
      printf("\n-num 42:                          Test42-0000-L.pgm,   Test42-0000-R.pgm");
      printf("\n-cal:                             cal0000-L.bmp,       cal0000-R.bmp (-c also works)");
      printf("\n-type bmp:                        Logs the images as a .bmp (.jpg also supported, -t also works)");
      printf("\n(-b works as -type bmp, -j works as -type jpg)");
      printf("\nAdditional Commands:");
      printf("\n-max num:   Captures a maximum of num frames (-m also works)");
      printf("\n-delay num: Introduces a wait of delay ms between captures(-d also works)");
      printf("\n-verbose:   Runs in verbose mode (-v also works)");
      printf("\n-prompt:    Prompts the user to get each image pair (-p also works)");
      printf("\n-long:      Use the long-range cameras (-l also works)");
      printf("\n-highres:   Use the old (high-res, Kevin Watson) cameras (-h also works)");
      printf("\n");
      return 0;
    }
    if(strcmp(argv[i], "-max")==0 || strcmp(argv[i], "-m")==0) {
      i++;
      max_frames = atoi(argv[i]);
    }
    if(strcmp(argv[i], "-delay")==0 || strcmp(argv[i], "-d")==0) {
      i++;
      delay = atoi(argv[i])*1000;
    }
    if(strcmp(argv[i], "-prompt")==0 || strcmp(argv[i], "-p")==0) {
      prompt = 1;
    }
    if(strcmp(argv[i], "-verbose")==0 || strcmp(argv[i], "-v")==0) {
      verbose = 1;
    }
    if(strcmp(argv[i], "-type") == 0 || strcmp(argv[i], "-t")==0) {
      i++;
      strcpy(file_type, argv[i]);
    }
    if(strcmp(argv[i], "-b") == 0) {
      strcpy(file_type, "bmp");
    }
    if(strcmp(argv[i], "-j") == 0) {
      strcpy(file_type, "jpg");
    }
    if(strcmp(argv[i], "-long")==0 || strcmp(argv[i], "-l")==0) {
      which_pair = LONG_RANGE;
    }
    if(strcmp(argv[i], "-highres")==0 || strcmp(argv[i], "-h")==0) {
      which_pair = HIGH_RES;
    }
    if(strcmp(argv[i], "-lname")==0) {
      i++;
      strcpy(left_file_name_prefix, argv[i]);
      strcpy(left_file_name_suffix, ".pgm");
    }
    if(strcmp(argv[i], "-rname")==0) {
      i++;
      strcpy(right_file_name_prefix, argv[i]);
      strcpy(right_file_name_suffix, ".pgm");
    }
    if(strcmp(argv[i], "-num")==0) {
      i++;
      test_num = atoi(argv[i]);
      sprintf(left_file_name_prefix, "Test%d-", test_num);
      sprintf(right_file_name_prefix, "Test%d-", test_num);
    }
    if(strcmp(argv[i], "-name")==0) {
      i++;
      sprintf(left_file_name_prefix, "%s", argv[i]);
      sprintf(right_file_name_prefix, "%s", argv[i]);
    }
    if(strcmp(argv[i], "-cal")==0) {
      sprintf(left_file_name_prefix, "cal");
      sprintf(right_file_name_prefix, "cal");
      sprintf(file_type, "bmp");
    }
  }

  dc1394_cameracapture cameras[2];
  raw1394handle_t handle;
  int node2cam[2];

  if (  cameras_start(&handle,node2cam,cameras, which_pair) < 0 ) {
    fprintf( stderr, "Error starting cameras\n" );
    exit(1);
  }


  /*-----------------------------------------------------------------------
   *  Cycle through the number of frames we wish to capture
   *-----------------------------------------------------------------------*/
  for(k = 1; k <= max_frames; k++) {
    usleep(delay);
    if(prompt == 1) {
      printf("Hit enter for next capture");
      getchar();
    }
    //timeval t1, t2;

    
    /*-----------------------------------------------------------------------
     *  capture one frame from each camera
     *-----------------------------------------------------------------------*/
    // do a multi capture - this way it should be synchronized automagically
    if(verbose==1) printf("Grabbing frame %d\n", k);
    //gettimeofday(&t1, NULL);
    if (cameras_snap(handle,cameras)<0)
      {
	fprintf( stderr, "Unable to capture frames\n" );
	cameras_stop( handle,cameras );
	exit(1);
      }
    //gettimeofday(&t2, NULL);

    //printf("Delay: %d sec, %d usec\n", t2.tv_sec-t1.tv_sec, t2.tv_usec-t1.tv_usec);

    /*-----------------------------------------------------------------------
     *  save images under appropriate filenames
     *-----------------------------------------------------------------------*/
    char left_file_name[256];
    char right_file_name[256];

    sprintf(left_file_name, "%s%.4d%s.%s", left_file_name_prefix, k, left_file_name_suffix, file_type);
    sprintf(right_file_name, "%s%.4d%s.%s", right_file_name_prefix, k, right_file_name_suffix, file_type);

    for ( i = 0; i < 2; i++ ) {
      imageSize = cvSize(cameras[i].frame_width, cameras[i].frame_height);    
      stereoImages[i] = cvCreateImage(imageSize, IPL_DEPTH_8U, 1);
      cvSetData(stereoImages[i], (char *)cameras[i].capture_buffer, cameras[i].frame_width);

      switch(node2cam[i])
	{
	case LEFT_CAM :
	  if(strcmp(file_type, "pgm") ==0) {
	    imagefile=fopen(left_file_name, "w");
	    if( imagefile == NULL)
	      {
		fprintf( stderr, "Can't create '%s'", left_file_name);
		continue;
	      }
	    fprintf(imagefile,"P5\n%u %u 255\n", cameras[i].frame_width,
		    cameras[i].frame_height );
	    fwrite((const char *)cameras[i].capture_buffer, 1, 
		   cameras[i].frame_height*cameras[i].frame_width, imagefile);
	    fclose(imagefile);
	    if(verbose == 1) printf("Wrote '%s'\n", left_file_name);
	  } else if(strcmp(file_type, "bmp")==0 || strcmp(file_type, "jpg")==0) {
	    cvSaveImage(left_file_name, stereoImages[i]);
	    if(verbose == 1) printf("Wrote '%s'\n", left_file_name);
	  } else {
	    fprintf(stderr, "Unknown file type '%s'\n", file_type);
	  }
	  break;
	case RIGHT_CAM :
	  if(strcmp(file_type, "pgm") == 0) {
	    imagefile=fopen(right_file_name, "w");
	    if( imagefile == NULL)
	      {
		fprintf( stderr, "Can't create '%s'", right_file_name);
		continue;
	      }
	    fprintf(imagefile,"P5\n%u %u 255\n", cameras[i].frame_width,
		    cameras[i].frame_height );
	    fwrite((const char *)cameras[i].capture_buffer, 1, 
		   cameras[i].frame_height*cameras[i].frame_width, imagefile);
	    fclose(imagefile);
	    if(verbose ==1 ) printf("Wrote '%s'\n", right_file_name);
	  } else if(strcmp(file_type, "bmp")==0 || strcmp(file_type, "jpg")==0) {
	    cvSaveImage(right_file_name, stereoImages[i]);
	    if(verbose ==1 ) printf("Wrote '%s'\n", right_file_name);
	  } else {
	    fprintf(stderr, "Unknown file type '%s'\n", file_type);
	  }
	  break;
	default : 
	  fprintf( stderr, "Can't tell left from right, or couldn't find cameras");
	  break;
	}


      //write_bmp("temp.bmp", cameras[i].frame_height, cameras[i].frame_width, (unsigned char*)cameras[i].capture_buffer, (unsigned char*)cameras[i].capture_buffer, (unsigned char*)cameras[i].capture_buffer);
    }
  }
  // stop data transmission 
  cameras_stop( handle, cameras );

  return 0;
}
