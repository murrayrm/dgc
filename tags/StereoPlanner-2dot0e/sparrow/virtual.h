/*
 * virtual.h - definitions for virtual drivers
 *
 * RMM, 21 Jul 93
 *
 */
 
/* Function declarations */
int virtual_driver(DEVICE *, CHANNEL *, DEV_ACTION);

