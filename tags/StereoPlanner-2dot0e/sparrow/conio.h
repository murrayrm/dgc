/*
 * conio.h - duplicate the functionality of Borland's console interface
 *
 * Ricahrd M. Murray
 * July 27, 1992
 */

/* Structure for returning screen information */
struct text_info {
    int screenheight;
    int screenwidth;
};

/* Cursor types for _setcursor */
#define _NOCURSOR	0
#define _NORMALCURSOR	1

int dd_getch();
void putch(int);
int kbhit();
int clreol();
void clrscr();
void gettextinfo(struct text_info *);
void _setcursortype(int);
int gotoxy(int, int);
void textcolor(int);
void textbackground(int);
