/***************************************************************************************/
//FILE: 	cruise.cpp
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file has the implementation of the methods
//
/***************************************************************************************/

#include "steering.h"
#include <iostream>

using namespace std;

// Default constructor for the steeringDataWrapper class
steeringDataWrapper::steeringDataWrapper() {

}

// Default destructor for the steeringDataWrapper class
steeringDataWrapper::~steeringDataWrapper() {

}

// Prints the data in a steeringDataWrapper object to the screen
void steeringDataWrapper::print_to_screen() {

	// Print 10 decimal places (arbitrary!) to show we have accuracy
	cout << setprecision(10);
	cout << "\nactuator  : " << data.actuator;
}

// This is exactly the same code as the above print_to_screen function, only out_file has replaced cout
void steeringDataWrapper::write_to_file(char *filename) {

	// Open the file for appending
	ofstream out_file(filename, ios::out | ios::app | ios::binary);

	// And then do the exact same stuff as above
	out_file << setprecision(10);
	cout << "\nactuator  : " << data.actuator;
}

// This method gets the values
void steeringDataWrapper::get_data(steeringData &buffer){
  buffer.actuator = data.actuator;
}

// This method updates the data
void steeringDataWrapper::update_data(steeringData buffer){
  data.actuator = buffer.actuator;
}
