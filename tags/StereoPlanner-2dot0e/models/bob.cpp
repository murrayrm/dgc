/***************************************************************************************/
//FILE: 	bob.cpp
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file has the implementation of the methods
//
/***************************************************************************************/

#include "bob.h"
#include <stdlib.h>

using namespace std;

// Default constructor for the bobDataWrapper class
bobDataWrapper::bobDataWrapper() {

}

// Default destructor for the bobDataWrapper class
bobDataWrapper::~bobDataWrapper() {

}

// Prints the data in a gpsDataWrapper object to the screen
void bobDataWrapper::print_to_screen() {

  // Print 10 decimal places (arbitrary!) to show we have accuracy
  cout << setprecision(10);
  cout << "\nx position: " << data.x;
  cout << "\ny position: " << data.y;
  cout << "\nvelocity  : " << data.vel;
}

// This is exactly the same code as the above print_to_screen function, only out_file has replaced cout
void bobDataWrapper::write_to_file(char *filename) {

  // Open the file for appending
  ofstream out_file(filename, ios::out | ios::app | ios::binary);

  // And then do the exact same stuff as above
  cout << setprecision(10);
  out_file << "\nx position: " << data.x;
  out_file << "\ny position: " << data.y;
  out_file << "\nvelocity  : " << data.vel;
  out_file << "\n";
}

// This method gets the values
void bobDataWrapper::get_data(bobData &buffer){
  buffer.x = data.x;
  buffer.y = data.y;
  buffer.phi = data.phi;
  buffer.theta = data.theta;
  buffer.vel = data.vel;
}

// This method updates the data
void bobDataWrapper::update_data(bobData buffer){
  data.x = buffer.x;
  data.y = buffer.y;
  data.phi = buffer.phi;
  data.theta = buffer.theta;
  data.vel = buffer.vel;
}
