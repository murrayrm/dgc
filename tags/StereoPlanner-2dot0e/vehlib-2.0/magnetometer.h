/* This is a program to control TCM2
 */
/*
 * 6/25/2003	Initial Creation
 */

#ifdef SERIAL2
#include "serial2.hh"
#endif


#ifdef __cplusplus
extern "C"{
#endif
  
#ifndef MAGNETOMETER_H
#define MAGNETOMETER_H
  
#include <ctype.h>
#include <math.h>
  //#include "serial/serial.h"
  
#ifndef SERIAL2
#include "serial.h"
#endif
  
  
#define MM_DefaultBaud	B9600		// 9600b/s is the default baud rate of a TCM2 module
#define MM_CR		0x0d		// Carriage Return
  
#define MM_MAX_RETURN_PARAM_NUM	7	// maximum number of returned paremeters by TCM2. The user is advised
					// to declare as many float to accomodate the returned values		 
#define MM_ERROR_CODE_NUM	3	// TCM2 returns 3 error codes
#define MM_TRANSMIT_CHECK_NUM	2	// TCM2 returns 2 trasnmit error checking codes
#define MM_MAX_READ		200	// If we still cannot get anything from serial port after reading 200 times
					// abort the read and return an error
  
  // data structure for passing read magnetometer data
  struct mag_data
  {
    float heading;
    float pitch;
    float roll;
  };
  
  /* configure the device with the following command at initialization.
   * thermometer unit=celcius, compass unit= degree, inclinometer=degree
   * RS232 stand output work format, enable compass data, pitch data, roll data, 
   * magnetometer data, temperature data, distortion detection. magnetometer ouput corrected.
   */
  
  /* These commands configure the TCM2 module when it starts up.
   * MM_SET_TEMP_UNIT	"ut=c"		// temp=celcuis
   * MM_SET_COMP_UNIT	"uc=d"		// compass=degree
   * MM_SET_IN_UNIT	"ui=d"		// inclinometer=degree
   * MM_SET_COMP_DATA	"ec=e"		// enable compass data for output word
   * MM_SET_PITCH_DATA	"ep=e"		// enable pitch data for output word
   * MM_SET_ROLL_DATA	"er=e"		// enable roll data for output word
   * MM_SET_MM_DATA	"em=e"		// enable magenetometer data for output word
   * MM_SET_TEMP_DATA	"et=e"		// enable temperature data for output word
   * MM_SET_DIST_DATA	"ed=e"		// enable distortion data for output word
   * MM_SET_MM_OUTPUT	"ma=c"		// enable magnetometer output correction
   * MM_SET_COMMAND_NUM	10		// number of available init commands
   */
  
  enum mm_set_command	{MM_SET_TEMP_UNIT, MM_SET_COMP_UNIT, MM_SET_IN_UNIT, MM_SET_COMP_DATA, MM_SET_PITCH_DATA,  MM_SET_ROLL_DATA, MM_SET_MM_DATA, MM_SET_TEMP_DATA, MM_SET_DIST_DATA, MM_SET_MM_OUTPUT, MM_SET_COMMAND_NUM};
  
  
  /* There are 7 commands available.
   * MM_COMPASS_UP:  compass update
   * MM_MM_UP: 	   magnetometer update
   * MM_IN_UP: 	   inclinometer update
   * MM_TEMP_UP: 	   temperature update
   * MM_OW_UP: 	   outpute word update
   * MM_LC_SC: 	   last calibration score
   * MM_CR_CA:	   clear calibration data
   * MM_EMP_CA:	   enable multipoint calibration
   * MM_DMP_CA:	   disable multipoint calibration
   * MM_GO:	   enter continuous sampling mode
   * MM_HALT: 	   stop continous sampling
   * MM_WARM_REBOOT: warm reboot
   */
  enum mm_command {MM_COMPASS_UP, MM_MM_UP, MM_IN_UP, MM_TEMP_UP, MM_OW_UP, MM_LC_SC, MM_CR_CA, MM_EMP_CA, MM_DMP_CA, MM_GO, MM_HALT, MM_WARM_REBOOT, MM_COMMAND_NUM};
  
  
  
  /*  mm_open: initialize a com port for TCM2
   */
  int mm_open(int com);
  
  /*  mm_close: uninitialize the com port
   */
  int mm_close(void);
  
  /*  mm_send_command: send a command to TCM2 and return its response
   *  command: one of the command offered by enum mm_command
   *  comm_return: the memory space where results are returned
   *  return_num: number of paremeters you expect to be returned from TCM2/ It is already
   *              in const int mm_command_return_num in magnetometer.c
   */ 
  int mm_write(enum mm_command command, float command_return[], int return_num);
  
  /*  mm_calibration: calibrate TCM2 to compensate for ambient magnetic field
   *  		    After calling this function, you have about 25 secs to calibrate
   */
  int mm_calibrate(void *);
  
  // gets magnetometer data and updates magreading struct
  int mm_read(mag_data &magreading);
  
  static int mm_com;	// com number for magnetometer serial port
  const char mm_init_command[MM_SET_COMMAND_NUM][10]={"ut=c", "uc=d", "ui=d", "ec=e", "ep=e", "er=e", "em=e", "et=e", "ed=e", "ma=c"};
  const char mm_command_list[MM_COMMAND_NUM][8]={"c?", "m?", "i?", "t?","s?", "lc?", "cc", "mpcal=e", "mpcal=d", "go", "h", "ax"};  // command output strings
  const char mm_command_return[MM_COMMAND_NUM][10]={"CE","XYZE", "PRE", "TE", "CPRXYZTE", "HVM", "HVM", "", "", "", "", ""};  // command return strings
  const int mm_command_return_num[MM_COMMAND_NUM]={1, 3, 2, 1, 7, 3, 3, 0, 0, 0, 0, 0};	// number of returned paremters for each command
  
  int mm_pause();
  int mm_resume();
  int mm_disable();
  
#endif 
  
#ifdef __cplusplus
}
#endif  
