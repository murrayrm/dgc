#ifndef ThreadsForFunctions_HH
#define ThreadsForFunctions_HH


#include <iostream>
#include <string.h>

#include "ThreadManager.hh"

using namespace Thread;

// ---------------- Prototypes --------------------------
// ---------------- Prototypes --------------------------
// ---------------- Prototypes --------------------------


// Calls T::m on an existing class that will exist for the
// entire time the thread will be executing
void RunFunctionInNewThreadLoader( void* arg );

void RunFunctionInNewThread( void (*f)(void*), void*arg = NULL );



#endif
