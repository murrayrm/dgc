#include "Computer.hh"
#include "../../Kernel.hh"


void Computer::AutoUpdate() {

  while( ! ShuttingDown() ) {

    list <ModuleInfo> r;
    PortScanIP( IP, r );

    {
      boost::recursive_mutex::scoped_lock sl(Mutex);
      Modules = r;
      LastUpdate = TVNow();
    }

    sleep (5);
  }
}
