#ifndef SharedMemoryContainer_HH
#define SharedMemoryContainer_HH

#include <boost/shared_ptr.hpp>

const int BlockSize = 100;

struct MemoryBlock {
  char B[BlockSize];
};

class SharedMemoryContainer {
public:
  SharedMemoryContainer();
  SharedMemoryContainer(int sz);
  SharedMemoryContainer(const SharedMemoryContainer& m);
  ~SharedMemoryContainer();

  char* sliderL();
  char* sliderR();
  char* read(int r);
  char* wrote(int l);
  char* ptr();
  int   size();

  int leftToRead();
  int leftToWrite();

  //  SharedMemoryContainer& operator= (const SharedMemoryContainer & rhs); 

private:
    
  void destruct();
  void insert();

  boost::shared_ptr<char > myPtr;
  char* myL;
  char* myR;
  int   mySize;

};





#endif
