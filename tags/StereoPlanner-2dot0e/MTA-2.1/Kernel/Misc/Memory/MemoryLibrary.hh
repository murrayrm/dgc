#ifndef MemoryLibrary_HH
#define MemoryLibrary_HH

#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/shared_ptr.hpp>
#include "MemoryHandle.hh"

using namespace std;
using namespace boost;

class MemoryLibrary {

public:
  MemoryLibrary(int blocksize);
  ~MemoryLibrary();

  shared_ptr<MemoryHandle> Checkout();
  void Return(void*);

private:

  boost::recursive_mutex M_TableLock;
  list<void*> M_Table;

  int M_BlockSize;
};




#endif
