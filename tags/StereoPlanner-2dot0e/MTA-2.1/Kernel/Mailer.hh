#ifndef Mailer_HH
#define Mailer_HH


#include "Misc/Socket/Socket.hh"
#include "Misc/File/File.hh"
#include "Misc/Mail/Mail.hh"
#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

using namespace std;
using namespace boost;
  
class ReceiveMailHandler {
  friend class Kernel;
  
public:
  ReceiveMailHandler(File& in);
  ReceiveMailHandler(const ReceiveMailHandler &rhs);
  ~ReceiveMailHandler();
  
  void operator () ();
private:
  File f;
};
  

// To send mail, we have this class.  It will enqueue
// messages to send if they are one way messages
// and they will be sent via another thread.

// Query messages are handled with the same thread that
// called the function because the response is returned.
/*
  class SendMailServer {
  public:
    SendMailServer();
    ~SendMailServer();
    
    void operator()(); // runs the outbox dispatcher

  
    void  SendMail(Mail & ml, File f= File()); // puts mail in a queue to be sent
    Mail  SendQuery(Mail & ml, File f= File()); // sends mail immediately and waits for respons (returned by function)

  private:
    void DeliverMail(Mail& ml, File f = File() ); // deliver mail with file (open file if necessary)    

    list <Mail> M_DeliverQueue;
    boost::recursive_mutex M_DeliverQueueLock;
  };


*/



#endif
