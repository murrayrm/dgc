% plot the gps log changing colors when a jump occurs
gps_jump = gpslog(:,15);
figure
hold on
color = 0
for i = 2:length(gps_jump)
    if gps_jump(i) > 0
        color = color + 1;
        color = mod(color, 2);
    end
    if color == 1 
       plot(gpseasting(i), gpsnorthing(i), 'or')
   else
       plot(gpseasting(i), gpsnorthing(i), 'og')
   end
end
plot(gpseasting, gpsnorthing, 'k-') 