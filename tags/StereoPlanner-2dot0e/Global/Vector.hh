
#ifndef Vector_HH
#define Vector_HH

struct Vector {
  float N;
  float E;
};

float distVector(Vector a, Vector b);
float get_deflection(Vector pos, Vector way, double headAngleDeg);
bool withinWaypoint(Vector pos, Vector way, float threshold);

#endif
