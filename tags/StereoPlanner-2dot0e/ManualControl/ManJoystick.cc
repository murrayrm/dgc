
#include "Msg/dgc_mta.hh"
#include "Msg/dgc_msg.h"
#include "Msg/dgc_socket.h"
#include "Msg/dgc_messages.h"
#include "Msg/dgc_const.h"
#include "Msg/dgc_mta_main.h"
#include "state/state.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
 

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <iostream>
#include <iomanip>
#include <list>
#include <time.h>
#include <fstream>

struct steer_struct {
  float x;
  float y;
  int buttons;
};

const float X_INC = 0.2;
const float Y_INC = 0.05;

int shutdown_flag;
using namespace std;

int main(int argc, char* argv[])
{
  steer_struct mySS;

  mySS.x = 0.0;
  mySS.y = 0.0;
  mySS.buttons = -16;

  shutdown_flag = FALSE;
  dgc_msg_Register(dgcs_JoystickDriver);
  cout << endl << endl << "Registering at time t=" 
       << setprecision(10) << SystemTime() << endl;
  dgc_MsgHeader myMH;
  char myOption;

  while ( shutdown_flag == FALSE ) { 
    
    cout << "Faster (w), Slower (s), " << endl
	 << "Left (a), Right (d), " << endl
	 << "Zero([space]), or Quit (p):" << endl;
    cin >> myOption; // = (char) getc();
    if( myOption == 'w' || myOption == 'W') {
      mySS.y += Y_INC;
      cout << "Setting speed to " << mySS.y << endl;
      // Send a message to ModeManager
      myMH.Destination = 0;
      myMH.DestinationType = dgcs_ModeManager;
      myMH.MsgType = dgcm_JoystickStatePacket;
      myMH.MsgSize = sizeof(steer_struct);
      dgc_SendMessage(myMH, &mySS);

    } else if( myOption == 's' || myOption == 'S') {
      mySS.y -= Y_INC;
      cout << "Setting speed to " << mySS.y << endl;
      // Send a message to ModeManager
      myMH.Destination = 0;
      myMH.DestinationType = dgcs_ModeManager;
      myMH.MsgType = dgcm_JoystickStatePacket;
      myMH.MsgSize = sizeof(steer_struct);
      dgc_SendMessage(myMH, &mySS);


    } else if( myOption == 'a' || myOption == 'A') {
      mySS.x -= X_INC;
      cout << "Setting wheel to " << mySS.x << endl;
      // Send a message to ModeManager
      myMH.Destination = 0;
      myMH.DestinationType = dgcs_ModeManager;
      myMH.MsgType = dgcm_JoystickStatePacket;
      myMH.MsgSize = sizeof(steer_struct);
      dgc_SendMessage(myMH, &mySS);

    } else if( myOption == 'd' || myOption == 'D') {
      mySS.x += X_INC;
      cout << "Setting wheel to " << mySS.x << endl;
      // Send a message to ModeManager
      myMH.Destination = 0;
      myMH.DestinationType = dgcs_ModeManager;
      myMH.MsgType = dgcm_JoystickStatePacket;
      myMH.MsgSize = sizeof(steer_struct);
      dgc_SendMessage(myMH, &mySS);

    } else if( myOption == 'z' || myOption == 'Z') {
      mySS.x = 0.0;
      mySS.y = 0.0;
      cout << "Zeroing car." << endl;
      // Send a message to ModeManager
      myMH.Destination = 0;
      myMH.DestinationType = dgcs_ModeManager;
      myMH.MsgType = dgcm_JoystickStatePacket;
      myMH.MsgSize = sizeof(steer_struct);
      dgc_SendMessage(myMH, &mySS);

    } else if( myOption == 'p' || myOption == 'P') {
      mySS.x = 0.0;
      mySS.y = 0.0;
      cout << "Zeroing car and exiting." << endl;
      // Send a message to ModeManager
      myMH.Destination = 0;
      myMH.DestinationType = dgcs_ModeManager;
      myMH.MsgType = dgcm_JoystickStatePacket;
      myMH.MsgSize = sizeof(steer_struct);
      dgc_SendMessage(myMH, &mySS);
      shutdown_flag = TRUE;  
    } else {
      cout << "Unknown Command" << endl;
    }

  }
  cout << endl << endl << "Unregistering" << endl;
  dgc_msg_Unregister();
    
  return 0;
}














