
#include "Msg/dgc_mta.hh"
#include "Msg/dgc_msg.h"
#include "Msg/dgc_socket.h"
#include "Msg/dgc_messages.h"
#include "Msg/dgc_const.h"
#include "Msg/dgc_mta_main.h"
#include "state/state.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
 

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <iostream>
#include <iomanip>
#include <list>
#include <time.h>
#include <fstream>


int shutdown_flag;
using namespace std;

int main(int argc, char* argv[])
{
  state_struct mySS;

  shutdown_flag = FALSE;
  dgc_msg_Register(dgcs_GlobalRoutePlanner);
  cout << endl << endl << "Registering at time t=" 
       << setprecision(10) << SystemTime() << endl;
  dgc_MsgHeader myMH;
  char myOption;

  while ( shutdown_flag == FALSE ) { 
    
    cout << "Capture (c), Quit (q):" << endl;
    cin >> myOption; // = (char) getc();
    if( myOption == 'c' || myOption == 'C') {
      // Request a message from State
      myMH.Destination = 0;
      myMH.DestinationType = dgcs_StateDriver;
      myMH.MsgType = dgcm_RequestState;
      myMH.MsgSize = 0;
      dgc_SendMessage(myMH, NULL);


    } else if( myOption == 'q' || myOption == 'Q') {
      cout << "Exiting" << endl;
      // read mta and store all messages
      int counter = 0;
      dgc_MsgHeader myMH;
      ofstream outfile ( "../Global/waypoints.gps");//, ios::binary);
      //state_struct mySS;
      while( dgc_PollMessage(myMH) == TRUE ) {
	dgc_GetMessage(myMH, &mySS);
	outfile << mySS.northing << " " << mySS.easting << endl;
	counter ++;
      }
      cout << "Grabbed " << counter << " waypoints." << endl;
      shutdown_flag = TRUE;
    

    } else {
      cout << "Unknown Command" << endl;
    }

  }
  cout << endl << endl << "Unregistering" << endl;
  dgc_msg_Unregister();
    
  return 0;
}














