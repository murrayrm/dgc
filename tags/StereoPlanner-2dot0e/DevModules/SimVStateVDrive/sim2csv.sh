# vim: syntax=awk
#==========sim2csv.sh===========
#18-jan-04	Alan Somers	created

#this script converts a logfile written by SimVStateVDrive to csv E, N format.

gawk 'BEGIN {FS=" "; OFS=", "};
    /^[^%]/ {print $5, $6}' $1
