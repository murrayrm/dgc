#ifndef STEREOIMAGELOGGERDATUM_HH
#define STEREOIMAGELOGGERDATUM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include <stereovision/getpair.h>
#include <stereovision/svsStereoWrapper.hh>

#include "RaceModules/Arbiter/Vote.hh"
#include "RaceModules/Arbiter/SimpleArbiter.hh"
#include "RaceModules/Arbiter/ArbiterModule.hh"

#include "../../include/Constants.h" //for vehicle constants, ie serial ports, etc.
//#include "../../state/imu/rot_matrix.hh"

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"
#include "vehlib/VDrive.hh"

using namespace std;

// All data that needs to be shared by common threads
struct StereoImageLoggerDatum {

  Voter stereo; // voter struct for the Arbiter

  // State struct from VState
  boost::recursive_mutex StateStructLock;
  VState_GetStateMsg SS;

  svsStereoWrapper svsObject;
};


#endif
