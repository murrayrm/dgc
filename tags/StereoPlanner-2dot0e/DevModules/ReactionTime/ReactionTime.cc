
#include "ReactionTime.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include <unistd.h>
#include <iostream>

// These are the states the vehicle can be in for this test
#define INITIAL 0
#define MOVING 1
#define STOPPING 2
#define DONE 3

int QUIT_PRESSED = 0;
int START_VEHICLE = 0;
int FULLSTOP_VEHICLE = 0;
int CRUISESTOP_VEHICLE = 0;

extern unsigned long SLEEP_TIME;
extern int NOSTATE;
extern char *TESTNAME;
extern double DESIRED_VELOCITY;

// When key is pressed to halt vehicle, record the exact time and location here
// This way we can computer elapsed time and distance travelled for display
int StopTimesec = 0;
int StopTimeusec = 0;
double StopX = 0.0;
double StopY = 0.0;
double StopZ = 0.0;

FILE *statelog;
FILE *reactionlog;

// Don't need a separate module for this so we will pretend to be LADAR
ReactionTime::ReactionTime() 
  : DGC_MODULE(MODULES::LADARMapper, "Reaction Timer", 0) {

}

ReactionTime::~ReactionTime() {
}


void ReactionTime::Init() {
  char filename[255];

  // call the parent init first
  DGC_MODULE::Init();

  // Open log files
  // filename will be TESTNAME_type_ddmmyyyy_hhmmss.log
  time_t t = time(NULL);
  tm *local;
  local = localtime(&t);

  sprintf(filename, "testRuns/%s_state_%02d%02d%04d_%02d%02d%02d.log",
          TESTNAME, local->tm_mon+1, local->tm_mday, local->tm_year+1900,
          local->tm_hour, local->tm_min, local->tm_sec);
  printf("Logging state to file: %s\n", filename);
  statelog=fopen(filename,"w");
  if ( statelog == NULL ) {
    printf("Unable to open state log!!!\n");
    exit(-1);
  }

  sprintf(filename, "testRuns/%s_reaction_%02d%02d%04d_%02d%02d%02d.log",
          TESTNAME, local->tm_mon+1, local->tm_mday, local->tm_year+1900,
          local->tm_hour, local->tm_min, local->tm_sec);
  printf("Logging reaction to file: %s\n", filename);
  reactionlog=fopen(filename,"w");
  if ( reactionlog == NULL ) {
    printf("Unable to open reaction log!!!\n");
    exit(-1);
  }

  fprintf(reactionlog, "Desired Velocity: %f\n", DESIRED_VELOCITY);

  cout << "Module Init Finished" << endl;
  RunMethodInNewThread<ReactionTime>( this, &ReactionTime::SparrowDisplayLoop);
  RunMethodInNewThread<ReactionTime>( this, &ReactionTime::UpdateSparrowVariablesLoop);
}

void ReactionTime::Active() {
  int vstate = INITIAL;

  while( ContinueInState() && !QUIT_PRESSED) {

    // update the State Struct
    if(!NOSTATE) {
      UpdateState();
      if(d.SS.Timestamp.sec() == 0) {
        cout << endl << "UNABLE TO COMMUNICATE WITH VSTATE....ABORTING" << endl;
        cout << "RUN WITH -nostate FLAG IF THIS IS OK" << endl << endl;
        exit(-1);
      }
    }

    UpdateSparrowVariablesLoop();

    if(vstate != DONE) {
      fprintf(statelog, "%d %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",
  	    d.SS.Timestamp.sec(), d.SS.Timestamp.usec(), d.SS.Easting,
  	    d.SS.Northing, d.SS.Altitude, d.SS.Vel_E, d.SS.Vel_N,
  	    d.SS.Vel_U, d.SS.Speed, d.SS.Accel, d.SS.Pitch, d.SS.Roll,
  	    d.SS.Yaw, d.SS.PitchRate, d.SS.RollRate, d.SS.YawRate);
    }

    // Send vdrive command to start moving
    if(START_VEHICLE && (vstate == INITIAL)) {
      fprintf(reactionlog, "START %d %d\n", d.SS.Timestamp.sec(),
                                            d.SS.Timestamp.usec());

      // Code yanked from ArbiterModule.cc
      d.toVDrive.Timestamp = TVNow();
      if(DESIRED_VELOCITY < MAX_VEL) d.toVDrive.velocity_cmd = DESIRED_VELOCITY;
      else d.toVDrive.velocity_cmd = MAX_VEL;
      d.toVDrive.steer_cmd = 0.0;
      Mail msg = NewOutMessage( MyAddress(), MODULES::VDrive, VDriveMessages::CmdMotion);
      msg << d.toVDrive;
      SendMail(msg);
      Mail msg2 = NewOutMessage( MyAddress(), MODULES::SimVDrive, VDriveMessages::CmdMotion);
      msg2 << d.toVDrive;
      SendMail(msg2);

      vstate = MOVING;
    }

    if((vstate == MOVING) && !FULLSTOP_VEHICLE && !CRUISESTOP_VEHICLE) 
    {
      Mail msg = NewOutMessage( MyAddress(), MODULES::VDrive, VDriveMessages::CmdMotion);
      msg << d.toVDrive;
      SendMail(msg);
      Mail msg2 = NewOutMessage( MyAddress(), MODULES::SimVDrive, VDriveMessages::CmdMotion);
      msg2 << d.toVDrive;
      SendMail(msg2);
    }

    // Send vdrive command to stop moving
    if(FULLSTOP_VEHICLE && (vstate == MOVING)) {
      StopTimesec = d.SS.Timestamp.sec();
      StopTimeusec = d.SS.Timestamp.usec();
      StopX = d.SS.Easting;
      StopY = d.SS.Northing;
      StopZ = d.SS.Altitude;

      // Code yanked from ArbiterModule.cc
      d.toVDrive.Timestamp = TVNow();
      d.toVDrive.velocity_cmd = 0.0;
      d.toVDrive.steer_cmd = 0.0;
      Mail msg = NewOutMessage( MyAddress(), MODULES::VDrive, VDriveMessages::CmdMotion);
      msg << d.toVDrive;
      SendMail(msg);
      Mail msg2 = NewOutMessage( MyAddress(), MODULES::SimVDrive, VDriveMessages::CmdMotion);
      msg2 << d.toVDrive;
      SendMail(msg2);

      fprintf(reactionlog, "FULLSTOP %d %d\n", d.SS.Timestamp.sec(),
                                           d.SS.Timestamp.usec());
      fprintf(reactionlog, "  X: %f  Y: %f  Z: %f  Speed: %f\n", StopX, StopY, StopZ, d.SS.Speed);

      vstate = STOPPING;
    }
 
    // Send vdrive command to set velocity to 0
    if(CRUISESTOP_VEHICLE && (vstate == MOVING)) {
      StopTimesec = d.SS.Timestamp.sec();
      StopTimeusec = d.SS.Timestamp.usec();
      StopX = d.SS.Easting;
      StopY = d.SS.Northing;
      StopZ = d.SS.Altitude;

      // Code yanked from ArbiterModule.cc
      d.toVDrive.Timestamp = TVNow();
      d.toVDrive.velocity_cmd = 0.0;
      d.toVDrive.steer_cmd = 0.0;
      Mail msg = NewOutMessage( MyAddress(), MODULES::VDrive, VDriveMessages::CmdMotion);
      msg << d.toVDrive;
      SendMail(msg);
      Mail msg2 = NewOutMessage( MyAddress(), MODULES::SimVDrive, VDriveMessages::CmdMotion);
      msg2 << d.toVDrive;
      SendMail(msg2);

      fprintf(reactionlog, "CRUISESTOP %d %d\n", d.SS.Timestamp.sec(),
                                           d.SS.Timestamp.usec());
      fprintf(reactionlog, "  X: %f  Y: %f  Z: %f  Speed: %f\n", StopX, StopY, StopZ, d.SS.Speed);

      vstate = STOPPING;
    } 

    // Has the vehicle come to a complete stop?
    if((vstate == STOPPING) && (fabs(d.SS.Speed) < DELTAV)) {
      long Stop_usec = (StopTimesec * 1000000) + StopTimeusec;
      long Now_usec = (d.SS.Timestamp.sec() * 1000000) + d.SS.Timestamp.usec();
  
      double TimeElapsed = (Now_usec - Stop_usec) / 1000000.0;
  
      double dx = d.SS.Easting - StopX;
      double dy = d.SS.Northing - StopY;
//      double dz = d.SS.Altitude - StopZ;  // I think we can ignore this safely
  
      double DistanceTravelled = sqrt(dx*dx + dy*dy);

      fprintf(reactionlog, "DONE %d %d\n", d.SS.Timestamp.sec(),
                                           d.SS.Timestamp.usec());
      fprintf(reactionlog, "  X: %f  Y: %f  Z: %f  Speed: %f\n", d.SS.Easting, d.SS.Northing, d.SS.Altitude, d.SS.Speed);
      fprintf(reactionlog, "  TIME: %f  DISTANCE: %f\n", TimeElapsed, DistanceTravelled);

      fclose(reactionlog);
      fclose(statelog);
      vstate = DONE;
      exit(0);
    }

    // and sleep for a bit
    usleep(SLEEP_TIME);
  }

  cout << "Finished Active State" << endl;

}


void ReactionTime::Shutdown() {
  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;

}



