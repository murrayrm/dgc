/****************************************************************************
 *
 *  File        : ladtest.c
 *
 *  Project     : WILDCARD
 *
 *  Copyright   : Annapolis Micro Systems Inc., 1999-2000
 *
 ****************************************************************************/
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <string.h>
#if defined(WIN32)
#include <windows.h>
#endif
#include "wcdefs.h"
#include "wc_shared.h"
#include "dimes_v10.h"
#include "LAD_Mem_Bridge_WC.h"

#define GET_IMU {rcimu = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 1, pReadBuffer);\
if ( (((pReadBuffer[0] >>  2) & 0x00000001) == 0x00000001) && (rcimu == WC_SUCCESS) ) {\
rcimu = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 12, pReadBuffer);\
pWriteBuffer[0] = 0x00000000; rcimu = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 1, &pWriteBuffer[0]);\
pWriteBuffer[0] = 0x00000010; rcimu = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 1, &pWriteBuffer[0]);\
pWriteBuffer[0] = 0x00000000; rcimu = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 1, &pWriteBuffer[0]);\
fprintf(fpimu, "%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i\n",\
(pReadBuffer[0] >>  0) & 0x00000001,\
(pReadBuffer[0] >>  1) & 0x00000001,\
(pReadBuffer[0] >>  2) & 0x00000001,\
(pReadBuffer[0] >>  8) & 0x000000ff,\
(pReadBuffer[0] >> 16) & 0x000000ff,\
(pReadBuffer[0] >> 24) & 0x000000ff,\
(pReadBuffer[1] >>  0) & 0x0000ffff,\
(pReadBuffer[1] >> 16) & 0x0000ffff,\
(pReadBuffer[2] >>  0) & 0x0000ffff,\
(pReadBuffer[2] >> 16) & 0x0000ffff,\
(pReadBuffer[3] >>  0) & 0x0000ffff,\
(pReadBuffer[3] >> 16) & 0x0000ffff,\
(pReadBuffer[4] >>  0) & 0x0000ffff,\
(pReadBuffer[4] >> 16) & 0x0000ffff,\
(pReadBuffer[5] >>  0) & 0x0000ffff,\
(pReadBuffer[5] >> 16) & 0x0000ffff,\
(pReadBuffer[6] >>  0) & 0x0000ffff,\
(pReadBuffer[6] >> 16) & 0x0000ffff,\
(pReadBuffer[7] >>  0) & 0x0000ffff,\
(pReadBuffer[7] >> 16) & 0x0000ffff,\
(pReadBuffer[8] >>  0) & 0x000000ff,\
(pReadBuffer[9] >>  0) & 0xffffffff,\
(pReadBuffer[10] >>  0) & 0xffffffff,\
(pReadBuffer[11] >>  0) & 0xffffffff);}}

void wait_delay(void)
{ int i;
    for (i =0; i < 200; i++)
  {
  }
}


/****************************************************************************
 *
 *  Function    : main
 *
 *  Description : Entry point for the WILDCARD
 *                This function is a basic entry point into the test.
 *                It is responcibe for
 *                   1) Parsing the command line parametrs and filling the
 *                      TestInfo struct with those parameters
 *                   2) Opening the WILDCARD(tm) board
 *                   3) Calling the main example procedure
 *                   4) Closing the board when the example completes
 *
 ****************************************************************************/
WC_RetCode
main( int  argc, char *argv [] )
{
  WC_RetCode
    rc = WC_SUCCESS;

  int
    argi;

  WC_TestInfo
    TestInfo;

  char **TestLoc = NULL;

  const char * help_string =
      "Usage: ladtest <list of options>\n"
      "   Options:\n"
      "      -v               Sets verbose mode. Show progress messages.\n"
      "      -i <num>         Sets the number of times to perform the example.\n"
      "                       (default = 1)\n"
      "      -s <num>         Set WILDCARD(tm) device \"slot\" number (default = 0).\n"
      "      -h               Show this help.\n";

  fprintf( stdout, "WILDCARD(tm) Mem_Test Example\n");

  TestInfo.bVerbose    = DEFAULT_VERBOSITY;
  TestInfo.DeviceNum   = DEFAULT_SLOT_NUMBER;
  TestInfo.dIterations = DEFAULT_ITERATIONS;
  TestInfo.fClkFreq    = DEFAULT_FREQUENCY;

  /*  Parse the command line parameters */
  for ( argi = 1; argi < argc; argi++ )
  {
    if ( argv[argi][0] == '-' )
    {
      switch ( toupper(argv[argi][1]) )
      {
        case 'H':  /* Print the help message */
          fprintf ( stdout, "%s\n\n", help_string,1,1 );
          return(WC_SUCCESS);
          break;

        case 'I':  /* Set the number of iterations */
          argi++;
          TestInfo.dIterations =  strtoul( argv [ argi ], TestLoc, 0 );
          /* Error Check the result.
           * The following test will be true only if there was an error
           * in the string conversion above */
          if (TestInfo.dIterations == 0)
          {
            fprintf( stdout, "\nWARNING:  An invalid or missing iteration value\n");
            fprintf( stdout, "          was found after the -i option.\n\n");
            fprintf( stdout, "%s\n\n", help_string );
            return (ERROR_UNKNOWN_SWITCH);
          }

          fprintf( stdout, "Setting the iteration value to %d\n",TestInfo.dIterations);
          break;

        case 'S':  /* Set the device number */
          argi++;
          TestInfo.DeviceNum =  strtoul( argv [ argi ], TestLoc, 0 );
          /* The following tests for a valid slot number */
          if (TestInfo.DeviceNum > WC_MAX_DEVICES)
          {
            fprintf( stdout, "\n  WARNING:  Invalid device number!\n");
            return (ERROR_UNKNOWN_SWITCH);
          }
          else
          {
            fprintf( stdout, "  Setting the device number to %d.\n", TestInfo.DeviceNum);
          }
          break;

        case 'V':  /* Show all Errors & set maximum verbosity */
          TestInfo.bVerbose=TRUE;
          fprintf( stdout, "  Setting Maximum Verbosity.\n");
          break;

        default: /* Unknow switch option */
          fprintf ( stderr, "\n  WARNING:  Unknown option: \"%s\"\n", argv [ argi ] );
          fprintf ( stderr, "%s\n\n", help_string );
          return( ERROR_UNKNOWN_SWITCH );
      }
    }
    else /* Missing the '-' */
    {
      fprintf ( stderr, "\n  WARNING: Unknown option: \"%s\"\n", argv[argi] );
      fprintf ( stderr, "%s\n\n", help_string );
      return(ERROR_UNKNOWN_SWITCH);
    }
  }

  /*  The WILDCARD(tm) MUST be opened before doing any type of *
   *  access to the card.                                      */
  if (TestInfo.bVerbose)
  {
    fprintf(stdout,"\n  Opening Device %d...\n", TestInfo.DeviceNum);
  }
  rc = WC_Open( TestInfo.DeviceNum, 0 );
  DISPLAY_ERROR(rc);

  /* Once the board is successfully opened, the test may be run */
  rc = WC_LADTest_Main( &TestInfo);
  if (rc!=WC_SUCCESS)
  {
    DisplayError(rc);
  }

  /* The WILDCARD(tm) should be closed when the program finishes to *
   * free driver resources                                          */
  rc = WC_Close( TestInfo.DeviceNum );
  DISPLAY_ERROR(rc);
    return(rc);
}

/****************************************************************************
 *
 *  Function    : WC_LADTest_Main
 *
 *  Parameters  : TestInfo - Test Parameters
 *
 *  Description : Initializes the WILDCARD(tm) hardware, and runs the example
 *                TestInfo->dIterations times.
 *
 ****************************************************************************/
WC_RetCode
WC_LADTest_Main (WC_TestInfo *TestInfo)
{
  DWORD
    dIteration,
    dErrorCount;

  WC_RetCode
    rc = WC_SUCCESS;

  /*  Print out a few parameters so we know what we are running */
  if (TestInfo->bVerbose)
  {
    fprintf(stdout,"\n  TEST PARAMETERS:\n");
    fprintf(stdout,"    Clock Frequency = %f\n",TestInfo->fClkFreq);
    fprintf(stdout,"    # of Iterations = %d\n",TestInfo->dIterations);
    fprintf(stdout,"    Device Number   = %d\n",TestInfo->DeviceNum);
    fprintf(stdout,"    Verbose Mode    = %s\n",TestInfo->bVerbose?"TRUE":"FALSE");
  }

  /*  This routine will put the WILDCARD in a known state.  *
   *  We only need to do this before the first iteration.   *
   *  Each additional iteration only needs to reset the     *
   *  PE to initialize the WILCARD to a known state because *
   *  All initialization parameters are kept between resets.*/
  rc = WC_LADTest_Init( TestInfo);
  CHECK_RC(rc)

  /*  Now that the PE is initialized, we run the test       *
   *  TestInfo->dIterations times, counting the number of   *
   *  failures as we go.                                    */
  for (dIteration = 0,dErrorCount = 0; dIteration < TestInfo->dIterations; dIteration++)
  {
    fprintf(stdout,"\n  **** LAD Example Iteration [%d] of [%d] ****\n",dIteration, TestInfo->dIterations);
    rc = WC_LADTest_Run(TestInfo);
    if (rc != WC_SUCCESS)
    {
      DisplayError(rc);
      dErrorCount++;
    }
  }

  /*  Let the user know if the example was a success */
  fprintf(stdout, "\n  Example Complete!  [%d] of [%d] Successful",
          TestInfo->dIterations - dErrorCount, TestInfo->dIterations);

  if (dErrorCount)
  {
    fprintf(stdout, "  ERRORS In Example!\n\n");
  }
  else
  {
    fprintf(stdout, "  Example SUCCESSFUL!\n\n");
  }

  /*  Return SUCCESS if we have made it this far without *
   *  returning.  This means that no fatal errors have   *
   *  occurred.  If any test errors occurred, they have  *
   *  already been printed above after each iteration.   */
  return (WC_SUCCESS);
}

/****************************************************************************
 *
 *  Function    : WC_LADTest_Run
 *
 *  Parameters  : TestInfo - Test Parameters
 *
 *  Description : Runs the Memory Example.  This hardware for this example
 *                contains an image with a LAD_Mux_Block_RAM and a
 *                LAD_Mux_Register_File.
 *
 *                This example will write a random pattern to each of the
 *                components, read it back, and verify that the read and
 *                write contents are equal.
 *
 ****************************************************************************/
WC_RetCode
WC_LADTest_Run( WC_TestInfo *TestInfo )
{
  WC_Mem_Object
    *Left_Memory,
    *Right_Memory;

  DWORD
    dNumDwords,
    dNumDwordsMem,
    *pDMABuffer,
     dNumDwordsRead,
    *pReadBuffer,
    *pReadBufferMemL,
    *pReadBufferMemR,
    *pReadBufferMem,
    *pWriteBuffer;

  unsigned long int
    imu_frame_count_old, imu_frame_count, IMUcounter;

  FILE * wf, *fpimu; 

  unsigned long int  index, counter, MERCAMCommands[100];
  unsigned int i, ii, NCommands;

  unsigned long int Counter40MHz_Old, Counter40MHz;

  char fname1[] = "image_20020626_xxxxxx.raw";
  char fname2[] = "image_20020626_xxxxxx.pgm";
  char fname3[] = "image_20020626_xxxxxx.img";
                // 012345678901234567890123

  char s1[] = "000000";

  WC_RetCode
    rc, rcimu;

  /*  The first step in an application is almost always to reset the PE. *
   *  Although this is not needed for the first iteration of this        *
   *  example because WC_LADTest_Init has already reset the PE, we need  *
   *  to do it again here because subsequent iterations need a fresh PE  *
   *  reset.                                                             *
   *  One assumption made below is that the time between the two         *
   *  WC_PeReset calls is sufficient to reset the PE.  In this example,  *
   *  and in general, this is true.  The reset line need only be high    *
   *  for at most one clock cycle of the longest period clock.           */
  fprintf(stdout, "\n  Resetting PE                        ... ");
  rc=WC_PeReset( TestInfo->DeviceNum, TRUE );
  CHECK_RC(rc);

  rc=WC_PeReset( TestInfo->DeviceNum, FALSE );
  CHECK_RC(rc);
  fprintf(stdout, "DONE\n");

  fprintf( stdout, "  Allocating Buffers                  ... ");

  if (TestInfo->DeviceCfg.MemoryDwords[0] >= TestInfo->DeviceCfg.MemoryDwords[1])
  {
    dNumDwordsMem = TestInfo->DeviceCfg.MemoryDwords[0];
  }
  else
  {
    dNumDwordsMem = TestInfo->DeviceCfg.MemoryDwords[1];
  }

  if (TestInfo->bVerbose) fprintf(stdout, "\n    * Allocating Left Read Buffer          ... ");
  pReadBufferMemL  = malloc(dNumDwordsMem * sizeof(DWORD));
  if (!pReadBufferMemL) return (ERROR_MEMORY_ALLOC);
  memset(pReadBufferMemL, 0, dNumDwordsMem);

  if (TestInfo->bVerbose) fprintf(stdout, "\n    * Allocating Right Read Buffer          ... ");
  pReadBufferMemR  = malloc(dNumDwordsMem * sizeof(DWORD));
  if (!pReadBufferMemR) return (ERROR_MEMORY_ALLOC);
  memset(pReadBufferMemR, 0, dNumDwordsMem);


  fprintf(stdout, "\n    * Allocating Big Read Buffer          ... ");
  pReadBufferMem  = malloc(17*dNumDwordsMem * sizeof(DWORD));
  if (!pReadBufferMem) return (ERROR_MEMORY_ALLOC);
  memset(pReadBufferMem, 0, 17*dNumDwordsMem);



  if (TestInfo->bVerbose) fprintf(stdout,"DONE\n    * Allocating Left Memory Struct   ... ");
  Left_Memory = WC_Mem_Create( TestInfo->DeviceNum, LEFT_MEM_OFFSET, 0 );
  if (!Left_Memory)
    {
      free(pReadBufferMemL);
      free(pReadBufferMemR);
      return (ERROR_MEMORY_ALLOC);
    }
  
  
  if (TestInfo->bVerbose) fprintf(stdout,"DONE\n    * Allocating Right Memory Struct  ... ");
  Right_Memory = WC_Mem_Create( TestInfo->DeviceNum, RIGHT_MEM_OFFSET, 0 );
  if (!Right_Memory)
    {
      free(pReadBufferMemL);
      free(pReadBufferMemR);
      WC_Mem_Release(Left_Memory);
      return (ERROR_MEMORY_ALLOC);
    }
  
  printf ("DONE\n");



  dNumDwords = 30;  // 512*512 = 262144;
  // dNumDwords = 265000; // = 512*512+200
  pDMABuffer  = WC_MemDmaAlloc(TestInfo->DeviceNum, dNumDwords);
  if (!pDMABuffer) return (ERROR_MEMORY_ALLOC);

   /*  The simplest method of buffer verification is to have different   *
    *  buffers for reading and writing. Below we allocate and initialize *
    *  these buffers.                                                    *
    *  The Block RAM is the largest object that we can read and write,   *
    *  so this is size buffer we need.                                   */
  fprintf( stdout, "  Allocating Buffers                  ... ");

  if (TestInfo->bVerbose) fprintf(stdout, "\n    * Allocating Read Buffer          ... ");
  pReadBuffer  = malloc(BRAM_SIZE_DWORDS * sizeof(DWORD));
  if (!pReadBuffer) return (ERROR_MEMORY_ALLOC);
  memset(pReadBuffer, 0, BRAM_SIZE_DWORDS);

  if (TestInfo->bVerbose) fprintf(stdout,"DONE\n    * Allocating Write Buffer         ... ");
  pWriteBuffer = malloc(BRAM_SIZE_DWORDS * sizeof(DWORD));
  if (!pWriteBuffer)
  {
    free(pReadBuffer);
    return (ERROR_MEMORY_ALLOC);
  }

  fprintf(stdout, "DONE\n");

  fprintf(stdout, "sizeof(DWORD) = %i\n", sizeof(DWORD));



  /* READ AND WRITE THE REGISTERS */
  fprintf(stdout, "  Testing Register Data               ... ");

  /****************************************** MER Camera TEST ****************************/

  fpimu = fopen("imu_data.txt", "wb");
  if (fpimu == NULL) {
    fprintf (stderr, "Not able to open  imu_data.txt file\n");
    return 0;
  }


  fprintf(fpimu, "\n\n");
  fprintf(fpimu, "DATA from IMU:\n\n");
  fprintf(fpimu, "imu_rec_busy \timu_crc_error \timu_frame done \timu_frame_error_count \timu_crc_eror_count \timu_frame_count \tdata00 \tdata01 \tdata02 \tdata03 \tdata04 \tdata05 \tdata06 \tdata07 \tdata08 \tdata09 \tdata10 \tdata11 \tdata12 \timu_tp \timu_time_tag \tnew_imu_timetag \timu_fpga_timetag \tcesium_timetag\n");
  fprintf(fpimu, "\n");

  // RESET IMU_DATA_RX by toggling (i_rst_n) (Active reset i_rst_n = high)
  pWriteBuffer[0] = 0x00000000 | 0x10000000;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 1, &pWriteBuffer[0]);  wait_delay();
  pWriteBuffer[0] = 0x00000002 | 0x10000000;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 1, &pWriteBuffer[0]);  wait_delay();
  pWriteBuffer[0] = 0x00000000 | 0x10000000;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 1, &pWriteBuffer[0]);  wait_delay();

  // RESET IMU_rti_update 
  pWriteBuffer[0] = 0x00000000 | 0x10000000;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 1, &pWriteBuffer[0]);  wait_delay();
  pWriteBuffer[0] = 0x00000008 | 0x10000000;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 1, &pWriteBuffer[0]);  wait_delay();
  pWriteBuffer[0] = 0x00000000 | 0x10000000;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 1, &pWriteBuffer[0]);  wait_delay();


  // RESET IMU_rtin_sync 
  pWriteBuffer[0] = 0x00000000 | 0x10000000;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 1, &pWriteBuffer[0]);  wait_delay();
  pWriteBuffer[0] = 0x00000004 | 0x10000000;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 1, &pWriteBuffer[0]);  wait_delay();
  pWriteBuffer[0] = 0x00000000 ;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 1, &pWriteBuffer[0]);  wait_delay();

  // NOW FPGA is receiving DATA from IMU
  imu_frame_count_old = 0;
  IMUcounter = 0;

 /*********************************************************************
  while(TRUE) {

    // Read status register and test it for imu_frame_done
    rcimu = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 1, pReadBuffer);
    if ( (((pReadBuffer[0] >>  2) & 0x00000001) == 0x00000001) && (rcimu == WC_SUCCESS) ) { // imu_frame_done
      rcimu = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 12, pReadBuffer); // Read all data
      // clear imu_frame_done
       pWriteBuffer[0] = 0x00000000;
       rcimu = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 1, &pWriteBuffer[0]);  // wait_delay();
      pWriteBuffer[0] = 0x00000010;
      rcimu = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 1, &pWriteBuffer[0]);  // wait_delay();
      pWriteBuffer[0] = 0x00000000;
      rcimu = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+12+0, 1, &pWriteBuffer[0]);
	  {
	  fprintf(fpimu, "%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i\n",
		(pReadBuffer[0] >>  0) & 0x00000001,  // imu_rec_busy
		(pReadBuffer[0] >>  1) & 0x00000001,  // imu_crc_error
		(pReadBuffer[0] >>  2) & 0x00000001,  // imu_frame done
		(pReadBuffer[0] >>  8) & 0x000000ff,  // imu_frame_error_count
		(pReadBuffer[0] >> 16) & 0x000000ff,  // imu_crc_error_count
		(pReadBuffer[0] >> 24) & 0x000000ff,  // imu_frame_count
		(pReadBuffer[1] >>  0) & 0x0000ffff,  // data 0
		(pReadBuffer[1] >> 16) & 0x0000ffff,  // data 1
		(pReadBuffer[2] >>  0) & 0x0000ffff,  // data 2
		(pReadBuffer[2] >> 16) & 0x0000ffff,  // data 3
		(pReadBuffer[3] >>  0) & 0x0000ffff,  // data 4
		(pReadBuffer[3] >> 16) & 0x0000ffff,  // data 5
		(pReadBuffer[4] >>  0) & 0x0000ffff,  // data 6 
		(pReadBuffer[4] >> 16) & 0x0000ffff,  // data 7
		(pReadBuffer[5] >>  0) & 0x0000ffff,  // data 8
		(pReadBuffer[5] >> 16) & 0x0000ffff,  // data 9 
		(pReadBuffer[6] >>  0) & 0x0000ffff,  // data 10
		(pReadBuffer[6] >> 16) & 0x0000ffff,  // data 11
		(pReadBuffer[7] >>  0) & 0x0000ffff,  // data 12
		(pReadBuffer[7] >> 16) & 0x0000ffff,  // imu_tp
		(pReadBuffer[8] >>  0) & 0x000000ff,  // imu_time_tag
		(pReadBuffer[9] >>  0) & 0xffffffff,  // new imu_timetag
		(pReadBuffer[10] >>  0) & 0xffffffff, // new imu_fpga_timetag
		(pReadBuffer[11] >>  0) & 0xffffffff  // cesium_timetag
		);
	  }
      
    }
  } //End of infinite loop while(true)
 *****************************************************************/




  //  pWriteBuffer[0] = 0x0302017e;
  //  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+index, 1, &pWriteBuffer[index]);
  //  rc = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+index, 1, &pReadBuffer[index]);
  //  fprintf(stdout, "pReadBuffer[%i] = %X\n", index, pReadBuffer[index]);

  fprintf(stdout, "\n\n");
  fprintf(stdout, "DATA from MER Camera:\n\n");
  fprintf(stdout, "\n");
  fprintf(stdout, "\n");


    fprintf(stdout, "Reseting Counters \n");
  // Toggle Reset counters
  pWriteBuffer[0] = 0x00000000;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  wait_delay();
  pWriteBuffer[0] = 0x00000010;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  wait_delay();
  pWriteBuffer[0] = 0x00000000 ;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  wait_delay();
  

    fprintf(stdout, "Waiting 3 ms \n");
  // Wait 3 ms to turn on MCLK
  rc = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+5, 1, &pReadBuffer[0]);
  Counter40MHz_Old = pReadBuffer[0];
  while(TRUE) {
    GET_IMU;
      rc = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+5, 1, &pReadBuffer[0]);
      Counter40MHz = pReadBuffer[0];
      if( Counter40MHz >= (Counter40MHz_Old+120000)) break;  // 40e6MHz * 3e-3 s = 120000 counts
  }
  pWriteBuffer[0] = 0x00000002;   // MCLK_ENBL <= '1'
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  wait_delay();


    fprintf(stdout, "Waiting 6 s\n");
  // Wait 5 s for camera to come up
  rc = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+5, 1, &pReadBuffer[0]);
  Counter40MHz_Old = pReadBuffer[0];
  while(TRUE) {
    GET_IMU;
      rc = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+5, 1, &pReadBuffer[0]);
      Counter40MHz = pReadBuffer[0];
      if( Counter40MHz >= (Counter40MHz_Old+6*40000000)) break;
  }


  // NOW MERCAM is reset and ready for operation
  //counter = 0;


  // Let's send comands to MER Camera

  //
  // Comands to get real image from the camera:
  // 1. Reset
  // 2. Fast flush ( usally 2 flushes)
  // 3. Set Exposure time ( 0.5 sec for the breadboard camera)
  // 4. Take full frame picture
  // For the help with commands see HAL_NCAM_CMD_PUB.H
  //
  //                      / Command
  //                     |    /parity( two high bits)
  //                     vvvv*
  MERCAMCommands[0] =  0x0300c000; // reset

  MERCAMCommands[1] =  0x3f028000; // Set Fast Flush Counter = 2

  MERCAMCommands[2] =  0x4201c000; // Set Exp. Time Low Bite
  MERCAMCommands[3] =  0x45004000; // Set Exp. Time High Bite 0.5 sec
                                   // count = 0.5 sec / 1024 / 5us = 97.65
                                   // = 98 = 6*2^4+2 = 0000 0000 0001 0010 = 0012
 
  MERCAMCommands[4] =  0x54ff4000; // Set Programmable Offset (low byte)
  MERCAMCommands[5] =  0x570f4000; // Set Programmable Offset (high byte)

  // MERCAMCommands[6] =  0x0900c000; // Take full frame picture
  MERCAMCommands[6] =  0x0f00c000; // Take binned image
  NCommands = 7; // total - 5 commands


  /* 
  // Command sequence to get test pattern from the camera
  MERCAMCommands[0] =  0x0300c000; // reset
  MERCAMCommands[1] =  0x12000000; // Read a fixed pattern picture
  NCommands = 2; // total - 2 commands
  */


    fprintf(stdout, "Sending Commands \a\n");
  // Let's send commands to the camera
  for(i=0; i<NCommands; i++){
    pWriteBuffer[0] = MERCAMCommands[i];
    rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+1, 1, &pWriteBuffer[0]);
    // wait until data are sent out through serial port:
    while(TRUE){
      GET_IMU;
      rc = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+1, 1, pReadBuffer);
      if( (pReadBuffer[0] & 0x00100000) == 0) break;  
    }
  } //end for(i)


  fprintf(stdout, "dNumDwordsMem = %i \n", dNumDwordsMem);
  



  /**********************************************************************************/
  // THIS IS HACK!!!!!!!!!!!!!!!!
  // For some reason I need:
  // Reason: dNumDwordsMem is number of Dwords i.e. 2 bytes, I need -- Wrong - memory increased x2 in newer cards
  // number of Qwords i.e. 4 byte words. So:
  dNumDwordsMem = dNumDwordsMem/2;
  // THIS IS HACK!!!!!!!!!!!!!!!!
  /**********************************************************************************/
  // For correct interface to FPGA code dNumDwordsMem = 65536, it
  // is where buffer left-right is swinged in FPGA code  
  dNumDwordsMem = 65536; // Hard code memory size for the smaller memory

  // Let's get data from the memory: Left,right, and so on 16 times:
  rc = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+5, 1, &pReadBuffer[0]);
  Counter40MHz_Old = pReadBuffer[0];

  for(i=0;i<4;i++){
    while(TRUE) {
      GET_IMU;
      rc = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+3, 1, &pReadBuffer[0]);
      if( (pReadBuffer[0] > 0) && (  ((pReadBuffer[0] >> 16)&0x1) == ((i+1)&0x1) ) ) {
	// fprintf(stdout, " Address = %i i = %i \n", pReadBuffer[0], i );
	break;
      }
    }
    if( (i&0x1) == 0){
      // Read Left Mem
      rc = WC_Mem_Read(Left_Memory, MEM_BASE, dNumDwordsMem, (pReadBufferMem+i*dNumDwordsMem));
      if (rc != WC_SUCCESS)
	{
	  free(pReadBufferMemL);
	  free(pReadBufferMemR);
	  WC_Mem_Release(Left_Memory);
	  WC_Mem_Release(Right_Memory);
	  return (rc);
	}      
    } else {
      // Read Right Mem
      rc = WC_Mem_Read(Right_Memory, MEM_BASE, dNumDwordsMem, (pReadBufferMem+i*dNumDwordsMem));
      if (rc != WC_SUCCESS)
	{
	  free(pReadBufferMemL);
	  free(pReadBufferMemR);
	  WC_Mem_Release(Left_Memory);
	  WC_Mem_Release(Right_Memory);
	  return (rc);
	}
    }
  }

  // Wait untill 1.4 sec ended and read Left Mem (Full image 16.5 memories full)
  while(TRUE) {
    GET_IMU;
      rc = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+5, 1, &pReadBuffer[0]);
      Counter40MHz = pReadBuffer[0];
      if( Counter40MHz >= (Counter40MHz_Old+14*4000000)) break;
  }


  fprintf(stdout, "Reading Memory\n");
  // Reading Left Memory
  rc = WC_Mem_Read(Left_Memory, MEM_BASE, dNumDwordsMem, (pReadBufferMem+4*dNumDwordsMem));
  if (rc != WC_SUCCESS)
    {
      free(pReadBufferMemL);
      free(pReadBufferMemR);
      WC_Mem_Release(Left_Memory);
      WC_Mem_Release(Right_Memory);
      return (rc);
    }

  // printout data from memory
  GET_IMU;
  // write_raw_image_file ("new_image.raw", &(pReadBufferMem[NCommands]));
  GET_IMU;
  // write_PGM_image_file ("new_image.pgm", &(pReadBufferMem[NCommands]));
  GET_IMU;
  write_PDS_image_file ("new_image.img", &(pReadBufferMem[NCommands]));
  GET_IMU;

  /*
  wf=fopen("MERCAM_Data.txt","w");
  if (wf==NULL) printf("Not able to open file\n" ) ;
  fprintf(stdout, "Saving data to the file\n");
  for (index =0; index < 17*dNumDwordsMem; index ++) {
     fprintf ( wf , "0x%06x 0x%03x \n" , (pReadBufferMem[index] & 0x003fffff), ((pReadBufferMem[index]>> 23) & 0x000001ff));
    //fprintf ( wf , "0x%06x 0x%02x \n" , ((pReadBufferMem[index]>>1) & 0x0000ffff), ((pReadBufferMem[index]>> 24) & 0x000000ff));
  }
    fclose(wf);
  */

  /*
  wf=fopen("MERCAM_image.dat","w");
  if (wf==NULL) printf("Not able to open file\n" ) ;
  fprintf(stdout, "Saving data to the image.dat file\n");
  for (index =0; index < 1024; index ++) {
    for (counter=0; counter<1056; counter++){
      if (counter == 1055){
	fprintf ( wf, "%d ",  ((pReadBufferMem[(NCommands+1056*index+counter)]>>1) & 0x00000fff) ); // Camera ID - end of row
      } else {
	fprintf ( wf, "%d ",  (((pReadBufferMem[(NCommands+1056*index+counter)]>>1) & 0x00000fff)^0x07ff) );
      }
    // I need ^0x07ff to convert signed image data to unsigned
    }
    fprintf(wf, "\n");
  }
    fclose(wf);
*/


/*
  wf=fopen("MERCAM_tags.dat","w");
  if (wf==NULL) printf("Not able to open file\n" ) ;
  fprintf(stdout, "Saving data to the tags.dat file\n");
  for (index =0; index < 1024; index ++) {
    for (counter=0; counter<1056; counter++){
    fprintf ( wf, "%d ", ((pReadBufferMem[(NCommands+1056*index+counter)]>>24) & 0x00000ff) );
    }
    fprintf(wf, "\n");
  }
    fclose(wf);
*/


/* ********************************************************************************************************* */

  fprintf(stdout, "\nStarting loop\n");

  for(index = 0; index < 1000; index++){

    GET_IMU;

    if(s1[5] < ('0'+9)) s1[5]++;
    else { s1[5]='0'; if(s1[4] < ('0'+9)) s1[4]++; 
    else { s1[4]='0'; if(s1[3] < ('0'+9)) s1[3]++; 
    else { s1[3]='0'; if(s1[2] < ('0'+9)) s1[2]++; 
    else { s1[2]='0'; if(s1[1] < ('0'+9)) s1[1]++; 
    else { s1[1]='0'; if(s1[0] < ('0'+9)) s1[0]++; 
    else { s1[0]='0'; }}}}}}

    fprintf(stdout, "index = %i; s1 = %s \n", index, s1);

    GET_IMU;

    // Toggle Reset counters
    pWriteBuffer[0] = 0x00000002;
    rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  wait_delay();
    pWriteBuffer[0] = 0x00000012;
    rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  wait_delay();
    pWriteBuffer[0] = 0x00000002 ;
    rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  wait_delay();
    
    
    // Let's send commands to the camera
    for(i=0; i<NCommands; i++){
      pWriteBuffer[0] = MERCAMCommands[i];
      rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+1, 1, &pWriteBuffer[0]);
      // wait until data are sent out through serial port:
      while(TRUE){
	GET_IMU;
	rc = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+1, 1, pReadBuffer);
	if( (pReadBuffer[0] & 0x00100000) == 0) break;  
      }
    } //end for(i)
    
    
    // Let's get data from the memory: Left,right, and so on 16 times:
    rc = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+5, 1, &pReadBuffer[0]);
    Counter40MHz_Old = pReadBuffer[0];
    
    for(i=0;i<4;i++){
      while(TRUE) {
	GET_IMU;
	rc = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+3, 1, &pReadBuffer[0]);
	if( (pReadBuffer[0] > 0) && (  ((pReadBuffer[0] >> 16)&0x1) == ((i+1)&0x1) ) ) {
	  // fprintf(stdout, " Address = %i i = %i \n", pReadBuffer[0], i );
	  break;
	}
      }
      if( (i&0x1) == 0){
	// Read Left Mem
	rc = WC_Mem_Read(Left_Memory, MEM_BASE, dNumDwordsMem, (pReadBufferMem+i*dNumDwordsMem));
	if (rc != WC_SUCCESS)
	  {
	    free(pReadBufferMemL);
	    free(pReadBufferMemR);
	    WC_Mem_Release(Left_Memory);
	    WC_Mem_Release(Right_Memory);
	    return (rc);
	  }      
      } else {
	// Read Right Mem
	rc = WC_Mem_Read(Right_Memory, MEM_BASE, dNumDwordsMem, (pReadBufferMem+i*dNumDwordsMem));
	if (rc != WC_SUCCESS)
	  {
	    free(pReadBufferMemL);
	    free(pReadBufferMemR);
	    WC_Mem_Release(Left_Memory);
	    WC_Mem_Release(Right_Memory);
	    return (rc);
	  }
      }
    }
    
    // Wait untill 1.4 sec ended and read Left Mem (Full image 16.5 memories full)
    while(TRUE) {
      GET_IMU;
      rc = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+5, 1, &pReadBuffer[0]);
      Counter40MHz = pReadBuffer[0];
      if( Counter40MHz >= (Counter40MHz_Old+14*4000000)) break;
    }
    
    
    // Reading Left Memory
    rc = WC_Mem_Read(Left_Memory, MEM_BASE, dNumDwordsMem, (pReadBufferMem+4*dNumDwordsMem));
    if (rc != WC_SUCCESS)
      {
	free(pReadBufferMemL);
	free(pReadBufferMemR);
	WC_Mem_Release(Left_Memory);
	WC_Mem_Release(Right_Memory);
	return (rc);
      }
    
    
    // printout data from memory
    ii = index;
    fname1[15] =  s1[0];
    fname1[16] =  s1[1];
    fname1[17] =  s1[2];
    fname1[18] =  s1[3];
    fname1[19] =  s1[4];
    fname1[20] =  s1[5];
    
    fname2[15] = fname1[15];
    fname2[16] = fname1[16];
    fname2[17] = fname1[17];
    fname2[18] = fname1[18];
    fname2[19] = fname1[19];
    fname2[20] = fname1[20];
    
    fname3[15] = fname1[15];
    fname3[16] = fname1[16];
    fname3[17] = fname1[17];
    fname3[18] = fname1[18];
    fname3[19] = fname1[19];
    fname3[20] = fname1[20];
    
    GET_IMU;
    // write_raw_image_file (fname1, &(pReadBufferMem[NCommands]));
    GET_IMU;
    // write_PGM_image_file (fname2, &(pReadBufferMem[NCommands]));
    GET_IMU;
    write_PDS_image_file (fname3, &(pReadBufferMem[NCommands]));
    GET_IMU;
    
  }

  fprintf(stdout, "\nEND of loop\n");

  fclose(fpimu);

/* ********************************************************************************************************* */

  WC_MemDmaFree(TestInfo->DeviceNum,pDMABuffer);
  free(pReadBuffer);
  free(pWriteBuffer);

    printf("\n Reading AddrCounter, and DataCounter... \n");

  rc = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET, 15, pReadBuffer);
  printf("AddrCounter = %d \n", pReadBuffer[3] );
  printf("DataCounter = %d \n", pReadBuffer[2] );


  fprintf(stdout, "DONE\n");


  // Disabling Camera clock
  pWriteBuffer[0] = 0x00000000;   // MCLK_ENBL <= '0'
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);

  /****************************************** END MERCAM TEST ****************************/
  
  return (rc);

}



/****************************************************************************
 *
 *  Function    : DeviceInitialize
 *
 *  Notes       : This function puts the card into a known state before the
 *                test begins.  It is generally a bad idea to assume the
 *                state of the WILDCARD's hardware when a program starts.
 *                Previous programs can leave the hardware in an unknown
 *                state, and it's state on power-on is undefined. If an
 *                application requires a specific state of the hardware,
 *                explicitly set that state.
 *
 *                Before running any application the following steps
 *                should be performed in the order given.
 *
 *                  1) Toggle Power
 *                  2) Assert the processing element reset line
 *                  3) Program the processing element
 *                  4) Set the clock frequency
 *                  5) Configure Interrupts
 *                  6) Deassert the processing element reset line
 *
 ****************************************************************************/
WC_RetCode
WC_LADTest_Init( WC_TestInfo *TestInfo )
{
  WC_RetCode
    rc=WC_SUCCESS;

  /* A great deal of useful information is available from the ID  *
   * PROM on the WILDCARD(tm), including processing element part  *
   * type, memory size, speed grade, etc.  The two API calls,     *
   * WC_DeviceInformation and WC_GetVersion are used to retrieve  *
   * that information.  The procedure DisplayConfiguration,       *
   * defined in wc_shared.c, displays this information to the     *
   * screen.                                                      *
   *                                                              *
   * Below we use the API calls to store the WILDCARD(tm) device  *
   * and version information in the TestInfo struct for use later *
   * in the example, as well as display the information if        *
   * verbosity is on.                                             */

  rc = WC_DeviceInformation( TestInfo->DeviceNum, &(TestInfo->DeviceCfg) );
  CHECK_RC(rc);
  rc = WC_GetVersion( TestInfo->DeviceNum, &(TestInfo->Version));
  CHECK_RC(rc);

  if (TestInfo->bVerbose)
  {
    rc=DisplayConfiguration(TestInfo->DeviceNum);
    CHECK_RC(rc);
  }

  /* It should NOT be assumed that the WILDCARD(tm) processing    *
   * element currently has power.  Below we toggle the power to   *
   * the processing element, leaving it ON for the remainder of   *
   * the example.                                                 */
  if (TestInfo->bVerbose)
  {
    fprintf (stdout, "  Toggling processing element's power...\n");
  }
  rc=WC_PeApplyPower ( TestInfo->DeviceNum, FALSE );
  CHECK_RC(rc);
  rc=WC_PeApplyPower ( TestInfo->DeviceNum, TRUE );
  CHECK_RC(rc);

  if (TestInfo->bVerbose)
    fprintf (stdout, "    PE power turned on.\n");

  /*  The WILDCARD(tm) has a dedicated reset line controlled by *
   *  the WC_PeReset API call.  In general it is advantageous   *
   *  to have the PE in reset when it is being set up. This     *
   *  will prevent the design from starting execution until the *
   *  WILDCARD(tm) has been correcly initialized.               *
   *                                                            *
   *  Below we assert the reset line and keep it asserted       *
   *  until the processing element has been programmed, the     *
   *  clock has been set, and interrupts have been initialized. *
   *
   *  If the Reset_STD_If has been instantiated in the VHDL,    *
   *  this API call will set the signal 'Global_Reset' high.    */

  if (TestInfo->bVerbose)
    fprintf (stdout, "  Asserting PE Reset Line...\n");
  rc=WC_PeReset( TestInfo->DeviceNum, TRUE );
  CHECK_RC(rc);
  if (TestInfo->bVerbose)
    fprintf (stdout, "    PE RESET line asserted.\n");

  /*  As of the creation of this file there are 4 revisions of  *
   *  the WILDCARD(tm) hardware.  (Revs A to D)  Below we use   *
   *  the informaion in TestInfo->Version to determine the      *
   *  revision of the card in this slot.                        */
  rc = WC_GetVersion( TestInfo->DeviceNum, &TestInfo->Version);
  CHECK_RC(rc);

  if (TestInfo->bVerbose)
    fprintf (stdout, "  Loading PE Image...\n");

  if (((TestInfo->Version.Hardware & WC_MAJOR_VER_MASK)>>WC_MAJOR_VER_SHIFT) == 4)
  {
    /*                  REV D WILDCARD(tm)                      *
     *                                                          *
     * The ProgramPeFromFile procedure, found in ws_shared.c,   *
     * will append .\<PART TYPE>\<PACKAGE TYPE>\ to the         *
     * filename, and load that file into the processing element.*
     * For a REV D this path will be                            *
     *    .\XCV300E\PKG_BG352\<IMAGE_FILENAME_REVD>             */
    rc=ProgramPeFromFile( TestInfo->DeviceNum, IMAGE_FILENAME_REVD );
    CHECK_RC(rc);
  }
  else
  {
    /*                  REV A-C WILDCARD(tm)                    *
     *                                                          *
     * The ProgramPeFromFile procedure, found in ws_shared.c,   *
     * will append .\<PART TYPE>\<PACKAGE TYPE>\ to the         *
     * filename, and load that file into the processing element.*
     *                                                          *
     * For a REV C this path will be                            *
     *    .\XCV300E\PKG_BG352\<IMAGE_FILENAME>                  *
     *                                                          *
     * For REVs A or B this path will be                        *
     *    .\XCV300\PKG_BG352\<IMAGE_FILENAME>                   */
    rc=ProgramPeFromFile( TestInfo->DeviceNum, IMAGE_FILENAME );
    CHECK_RC(rc);
  }

  if (TestInfo->bVerbose)
    fprintf (stdout, "    PE Image Loaded.\n");

  /* The WILDCARD(tm) has one on-board programmable oscillator. *
   * WC_ClkSetFrequency sets the frequency of that clock.  We   *
   * always want to set the clock to the appropriate frequency  *
   * before running our application.                            */

  if (TestInfo->bVerbose)
    fprintf(stdout, "  Initializing the clock to %f...\n", TestInfo->fClkFreq);
  rc=WC_ClkSetFrequency ( TestInfo->DeviceNum, TestInfo->fClkFreq );
  CHECK_RC(rc);
  if (TestInfo->bVerbose)
    fprintf(stdout, "    Clock initialized.\n");

  /*  This application does not use the PE interrupt line,  *
   *  so the following code is not needed. It is generally  *
   *  a good practice to set the state of the interrupt     *
   *  line & event, so we'll do it here anyway.             */
  if (TestInfo->bVerbose)
    fprintf (stdout, "  Masking PE Interrupt...\n");
  rc=WC_IntEnable( TestInfo->DeviceNum, FALSE);
  CHECK_RC(rc);
  if (TestInfo->bVerbose)
    fprintf (stdout, "    PE Interrupt Masked.\n");

  /*  The order of mask / reset may be important in some     *
   *  circumstances.  In our case it is not.  We mask,       *
   *  then clear anything that may have been happened before *
   *  the masking operation                                  */
  if (TestInfo->bVerbose)
    fprintf (stdout, "  Resetting PE Interrupt...\n");
  rc=WC_IntReset( TestInfo->DeviceNum );
  CHECK_RC(rc);
  if (TestInfo->bVerbose)
    fprintf (stdout, "    PE Interrupt Reset.\n");


  /*  Lastly, we remove the PE from the RESET state.  When  *
   *  the Reset_STD_IF is instantiated in the VHDL, this    *
   *  will set the VHDL signal 'Global_Reset' low.          */

  if (TestInfo->bVerbose)
    fprintf (stdout, "  De-asserting PE Reset Line...\n");
  rc=WC_PeReset( TestInfo->DeviceNum, FALSE );
  CHECK_RC(rc);
  if (TestInfo->bVerbose)
    fprintf (stdout, "    PE RESET line de-asserted.\n");


  return(rc);
}

/****************************************************************************
 *
 *  Function    : VerifyData
 *
 *  Parameters  : ref  - Reference buffer. (Typically the WriteBuffer)
 *                test - Test Buffer. (Typically the ReadBuffer)
 *                size - Number of DWORDS to test
 *
 *  Description : Compares two buffers and prints the differences if found.
 *
 ****************************************************************************/

WC_RetCode
VerifyData(DWORD ref[], DWORD test[], DWORD size)
{
  DWORD
    memCntr,
    errCount = 0;

  WC_RetCode
    rc = WC_SUCCESS;


  /*  Loop counts off in DWORDS.
   *  Mismatches will stop being counted after MAX_ERR_COUNT errors.
   */

  for ( memCntr=0; (memCntr < size); memCntr++)
  {
    if (ref[memCntr] != test[memCntr])
    {
      rc=ERROR_MEMORY_COMPARE_FAILED;
      if (errCount==0)
      {
        printf( "\n\tERROR: Memory Compare Failed\n");
        printf( "\tDWORD:\tExpected Data:\tActual Data\n");
      }

      printf( "\t%i\t0x%08x\t0x%08x\n",  memCntr, ref[memCntr], test[memCntr]);
      errCount++;
    }
    if ( errCount> MAX_ERR_COUNT )
    {
      printf( "\tMemory compare stopped after %d errors found.\n\n", MAX_ERR_COUNT);
      break;
    }
  }

  if (errCount!=0  && errCount <= MAX_ERR_COUNT )
  {
    printf( "\tNumber of memory errors found: %d.\n\n", errCount);
  }
  return(rc);
}


/****************************************************************************/

#define MAX_ROW 256
#define MAX_COL 1056
#define LINE_PREFIX_BYTES 34
#define LINE_SUFFIX_BYTES 30
    
/****************************************************************************/
int write_raw_image_file (char *file_name, DWORD *buffer)
{
	FILE *fp;
	
	int index,
		counter;
	
	unsigned short int short_pixel;
	
	DWORD buffer_value;
	
	union U {
		unsigned short int two_bytes; 
		char bytes[2];
	} swap; 
	
	
	fp = fopen(file_name,"wb");
	
	if (fp == NULL) {
		fprintf (stderr, "Not able to open \"%s\" file\n", file_name );
		return 0;
	}
	
	for (index = 0; index < MAX_ROW; index ++) {
		for (counter = 0; counter < MAX_COL; counter++){
			
			buffer_value = buffer[MAX_COL*index+counter];
			
			short_pixel = (buffer_value >> 1) & 0x00000fff;
			
			if (counter != (MAX_COL - 1))  // end of row = Camera ID
				short_pixel = short_pixel ^ 0x07ff;
			
			swap.two_bytes = short_pixel;
			
			fwrite ((char *) &(swap.bytes[1]), sizeof (char), 1, fp);
			fwrite ((char *) &(swap.bytes[0]), sizeof (char), 1, fp);
		}
	}
	fclose(fp);
	
	return 1;
}


/****************************************************************************/
int write_PGM_image_file (char *file_name, DWORD *buffer)
{
	FILE *fp;
	
	int index,
		counter;
	
	unsigned short int short_pixel;
	char byte_pixel;
	DWORD buffer_value;
	
	fp = fopen(file_name,"wb");
	
	if (fp == NULL) {
		fprintf(stderr, "Not able to open \"%s\" file\n", file_name );
		return 0;
	}
	
	fprintf (fp, "P5\x0A");
	fprintf (fp, "#Created by MER LAptop GSE\x0A");
	fprintf (fp, "%d %d\x0A", MAX_COL, MAX_ROW);
	fprintf (fp, "255\x0A");
	
	for (index = 0; index < MAX_ROW; index ++) {
		for (counter = 0; counter < MAX_COL; counter++){
			
			buffer_value = buffer[MAX_COL*index + counter];
			
			short_pixel = (buffer_value >> 1) & 0x00000fff;
			
			if (counter == (MAX_COL - 1))
				byte_pixel = short_pixel & 0x00ff; // end of row = Camera ID
			else {
				short_pixel = short_pixel ^ 0x07ff;
				byte_pixel = (short_pixel >> 4) & 0x00ff;
			}
			
			fwrite ((unsigned char *) &byte_pixel, sizeof (byte_pixel), 1, fp);
		}
	}
	fclose(fp);
	
	return 1;
}


/****************************************************************************/
int       write_PDS_header (FILE *fp, char *file_name, int max_row, int max_col, int bpp, double min, double max, double mean, double median, double stddev, double checksum)
{
	int       n,
		i,
		fill,
		rowbytes;
	
	/* How many bytes per pixel */
    rowbytes = ((bpp == 8) ? max_col : 2 * max_col);
    if (rowbytes < 1)
		rowbytes = 1;
	
	/* Determine how many records the header will contain, where each record is the size of a row.
	   Allow at least 1024 bytes so that more header information can be squeezed in after the fact */
    n = (max_row + rowbytes - 1) / rowbytes;
	
    /* Write the header */
    i = 0;
    i += fprintf (fp, "PDS_VERSION_ID     = PDS3\r\n");
    i += fprintf (fp, "RECORD_TYPE        = FIXED_LENGTH\r\n");
    i += fprintf (fp, "RECORD_BYTES       = %d\r\n", rowbytes);
    i += fprintf (fp, "FILE_RECORDS       = %d\r\n", n + max_row);
    i += fprintf (fp, "LABEL_RECORDS      = %d\r\n", n);
    i += fprintf (fp, "^IMAGE             = %d\r\n", n + 1);
    i += fprintf (fp, "OBJECT             = IMAGE\r\n");
    i += fprintf (fp, " LINES             =  %d\r\n", max_row);
    i += fprintf (fp, " LINE_SAMPLES      =  %d\r\n", max_col - (LINE_PREFIX_BYTES / 2 + LINE_SUFFIX_BYTES / 2));
	
    if (bpp == 8) {
		i += fprintf (fp, " SAMPLE_BITS       =  8\r\n");
		i += fprintf (fp, " SAMPLE_TYPE       =  UNSIGNED_INTEGER\r\n");
    } else {
		i += fprintf (fp, " SAMPLE_BITS       =  16\r\n");
		i += fprintf (fp, " SAMPLE_BIT_MASK   =  2#0000111111111111\r\n");
		i += fprintf (fp, " SAMPLE_TYPE       =  MSB_UNSIGNED_INTEGER\r\n");
    }
	
	i += fprintf (fp, " BANDS = 1\r\n");
	i += fprintf (fp, " LINE_PREFIX_BYTES = %d\r\n", LINE_PREFIX_BYTES);
	i += fprintf (fp, " LINE_SUFFIX_BYTES = %d\r\n", LINE_SUFFIX_BYTES);
	
	i += fprintf (fp, " MAXIMUM           = %0.0lf\r\n", max);
	i += fprintf (fp, " MINIMUM           = %0.0lf\r\n", min);
	i += fprintf (fp, " MEAN              = %0.4lf\r\n", mean);
	i += fprintf (fp, " MEDIAN            = %0.0lf\r\n", median);
	i += fprintf (fp, " STANDARD_DEVIATION = %0.4lf\r\n", stddev);
	
	i += fprintf (fp, " CHECKSUM          = %0.0lf\r\n", checksum);
	i += fprintf (fp, " FIRST_LINE        = 1\r\n", median);
	i += fprintf (fp, " FIRST_LINE_SAMPLE = 1\r\n");
	
    i += fprintf (fp, "END_OBJECT         = IMAGE\r\n");
    i += fprintf (fp, "END\r\n");
	
    /* Pad to end of header with blanks */
    fill = n * rowbytes - ftell (fp);
    if (fill < 0) {
		fprintf (stderr, "Header overflow (%d): %s\n", fill, file_name);
		return 0;
    }
	
    while (fill-- > 0)
		fwrite (" ", 1, 1, fp);
	
	return 1;
}


/****************************************************************************/
int calculate_image_statistics (DWORD *buffer, int max_row, int max_col, double *min, double *max, double *mean, double *median, double *stddev, double *checksum)
{
#define SQR(a)          ((a) * (a))
#define MAX(a,b)        (((a) > (b)) ? (a) : (b))
#define MIN(a,b)        (((a) < (b)) ? (a) : (b))
	
	int r,
		c,
		pixel_count = 0;
	
	double	  float_pixel,
		min_pixel = 4096,
		max_pixel = 0,
		sum_pixels = 0,
		sum_squared_pixels = 0;
	
	unsigned short int short_pixel;
	
	DWORD buffer_value;
	
	union U {
		unsigned short int two_bytes; 
		char bytes[2];
	} swap; 
	
	
	/* Initialize the returned parameters */
	*min = *max = *mean = *median = *stddev = *checksum = 0;
	
	/* Run through the image calculating the statistics */
	for (r = 0; r < max_row; r ++) {
		for (c = 0; c < max_col; c++){
			
			buffer_value = buffer[max_col*r+c];
			
			swap.two_bytes = short_pixel = (buffer_value >> 1) & 0x00000fff;
			
			/* Calculate the bytewise checksum */
			*checksum += swap.bytes[0] + swap.bytes[1];
			
			if ((c > LINE_PREFIX_BYTES / 2) && (c < max_col - LINE_SUFFIX_BYTES / 2))	{ 
				/* Convert from 2's complement binary into natural binary */
				short_pixel = swap.two_bytes ^ 0x07ff;
				
				/* Convert from short into float */
				float_pixel = (double) short_pixel;
				
				/* Calculate the intermediate results */
				sum_pixels += float_pixel;
				sum_squared_pixels += SQR(float_pixel);
				max_pixel = MAX (max_pixel, float_pixel);
				min_pixel = MIN (min_pixel, float_pixel);
				
				pixel_count += 1;
			}
		}
	}
	
	*mean = sum_pixels / pixel_count;
    *max = max_pixel;
	*min = min_pixel;
	
    if (pixel_count == 1)
        *stddev = 0;
    else
        *stddev = sqrt ((sum_squared_pixels - SQR (sum_pixels) / pixel_count) / (pixel_count - 1));
	
	*median = 0; /* to do at a future date */
	
	return 1;
}


/****************************************************************************/
int write_PDS_image_file (char *file_name, DWORD *buffer)
{
	FILE *fp;
	
	int r,
		c;
	
	double min, 
		max, 
		mean, 
		median, 
		stddev, 
		checksum;
	
	unsigned short int short_pixel;

	DWORD buffer_value;

	union U {
		unsigned short int two_bytes; 
		char bytes[2];
	} swap; 

	fp = fopen(file_name,"wb");
	
	if (fp == NULL) {
		fprintf(stderr, "Not able to open \"%s\" file\n", file_name );
		return 0;
	}
	
	calculate_image_statistics (buffer, MAX_ROW, MAX_COL, &min, &max, &mean, &median, &stddev, &checksum);
	
	write_PDS_header (fp, file_name, MAX_ROW, MAX_COL, 12, min, max, mean, median, stddev, checksum);
	
	for (r = 0; r < MAX_ROW; r ++) {
		for (c = 0; c < MAX_COL; c++){
			
			buffer_value = buffer[MAX_COL*r + c];
			
			short_pixel = (buffer_value >> 1) & 0x00000fff;
			
			if (c != (MAX_COL - 1))	//  end of row = Camera ID
				short_pixel = short_pixel ^ 0x07ff;
			
			swap.two_bytes = short_pixel;
			
			fwrite ((char *) &(swap.bytes[1]), sizeof (char), 1, fp);
			fwrite ((char *) &(swap.bytes[0]), sizeof (char), 1, fp);
		}
	}
	fclose(fp);
	
	return 1;
}
