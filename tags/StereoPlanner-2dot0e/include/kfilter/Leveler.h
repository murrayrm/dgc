/****************************************************************************
 *
 * INS/GPS, Leveler
 *
 * File: leveler.h
 * Date: 02/16/00
 *
 ****************************************************************************/
#ifndef LevelerH
#define LevelerH

/****************************************************************************
 * Function Definitions
 ****************************************************************************/

void levelerInit(void);
void levelerReEntry(void);
void perform_leveling(void);

void determine_leveler_cutoff(void);
void comp_init_quat_deltas(void);
void perform_first_order_leveler(void);

void levelerQuatLevel(void);

/****************************************************************************
 * End
 ****************************************************************************/
#endif