/*
 * prog2.c - sample shared memory program
 *
 * RMM, 19 Dec 03
 *
 * This program is the companion for prog1.c.  This is almost identical
 * to prog1, except that it *reads* from the shared memory and displays
 * what it finds there.  Hit ^C to exit.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define PRGNAME "P2"

main(int argc, char **argv)
{
  int shmid;				/* Shared memory identifier */
  int size = 1024;		   /* Size of shared memory segment */
  int key = 117;			/* Unique shared memory key */
  int shmflg;				/* Flags to shmget, set below */
  int *p;				/* pointer to shared memory */
  int i;

  /* Create a piece of shared memory */
  shmflg = (IPC_CREAT | 0x1ff);		/* Create R/W segment */
  if ((shmid = shmget(key, size, shmflg)) < 0) {
    fprintf(stderr, "%s: shmget failed\n", PRGNAME);
    perror(PRGNAME);
    exit(0);
  }
  fprintf(stderr, "%s: shmget succeeded\n", PRGNAME);

  /* Now attach to the memory segment */
  p = (int *) shmat(shmid, NULL, 0);

  /* Finally, write out whatever data is present */
  while (1) printf("%d \r", p[0]);

  return 0;
}
