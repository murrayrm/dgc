/*
 * ladar.cc - simple shell for MTA-capable LADAR module
 * RMM, 10 Jan 04
 *
 * This program sends and initialization message to the arbiter and
 * then sends in some votes.  Template for how to implement a voter
 * module in MTA.
 *
 */

/* Header files required for MTA; assumes include path is set up */
#include "MTA/Modules.hh"
#include "MTA/Kernel.hh"
#include <boost/shared_ptr.hpp>

/* Some include files for sparrow, used for debug output */
#include "sparrow/dbglib.h"

/* This is the module definition for the modules we need to talk to */
#include "arbiterMTA.hh"

using namespace std;
using namespace boost;

/* Declare the ladarMTA class based on DGC_MODULE */
class ladarMTA:public DGC_MODULE {
public:
  ladarMTA();
  ~ladarMTA();

  void Init();			// initialization routine
  void Active();		// main routine for module

private:
  // put anything here that you want in this object
  // nothing for now
};

/* Global data structure for the votes */
struct arbiterMTA_InitVoterMsg voter;
struct arbiterMTA_NewVotesMsg voteData;

int main(int argc, char **argv) {
  /* Set up sparrow to print out all debugging statements */
  dbg_flag = 1;				/* enable debugging */
  dbg_all = 1;				/* turn on all modules */
  dbg_outf = 1;				/* print messages to screen */

  /* Register this module in MTA */
  dbg_info("registering ladarMTA module");
  Register(shared_ptr<DGC_MODULE>( new ladarMTA ));

  /* Start the MTA kernel */
  dbg_info("stating the MTA kernel");
  StartKernel();

  return 0;
}

/*
 * MTA MODULE support
 *
 * These are the handling routines for the ladarMTA data type.
 *
 */

using namespace std;
using namespace boost;

/* Constructor and destructor routines */
ladarMTA::ladarMTA() : DGC_MODULE(MODULES::ladarMTA, "LADAR", 0) {}
ladarMTA::~ladarMTA() {}

/* LADAR initialization */
void ladarMTA::Init() {
  Mail msg = NewQueryMessage(MyAddress(), MODULES::arbiterMTA,
			     arbiterMessages::InitVoter);

  /* Send a message to the arbiter saying that I am here */
  voter.voter = 'L';		// let them know I am the LADAR
  msg.QueueTo(&voter, sizeof(voter));
  dbg_info("sending InitVoter to arbiter");
  Mail reply = SendQuery(msg);

  /* If something went wrong, let the user know */
  if (!reply.OK()) {
    dbg_error("problem with reply from arbiter");
    SetNextState(STATES::SHUTDOWN);
  }

  /* Dequeue the angles I should use for my votes */
  reply.DequeueFrom(&voter, sizeof(voter));
  dbg_info("arbiter reply received; angles = %g, %g", 
	   voter.angles[0], voter.angles[1]); 

  SetNextState(STATES::ACTIVE);
}

/* Active state handler */
void ladarMTA::Active() {
  voteData.seqnum = 0;		// sequence number

  while( ContinueInState() ) {  // while stay active
    /* Create an outgoing message that we can use */
    Mail msg = NewOutMessage(MyAddress(), MODULES::arbiterMTA, 
   			     arbiterMessages::NewVotes);

    voteData.voter = 'L';	// let them know I am the LADAR
    voteData.seqnum++;		// increase the message ID
    voteData.votes[0] = 1;	// put in random data for vote
    voteData.votes[1]++;

    /* Send out a message to the arbiter */
    dbg_info("sending message to arbiter");
    msg.QueueTo(&voteData, sizeof(voteData));
    SendMail(msg);

    sleep(2);			// sleep for two seconds
  }
} 
