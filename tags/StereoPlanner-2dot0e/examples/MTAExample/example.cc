// dgc_msg_main.cc 

#include "dgc_mta.hh"
#include "dgc_msg.h"
#include "dgc_socket.h"
#include "dgc_messages.h"
#include "dgc_const.h"
#include "dgc_mta_main.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
 

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <iostream>
#include <list>
#include <time.h>


using namespace std;
int myState;
int myRequestedState;

// should be defined in a header somewhere
struct steer_struct {
  float x;
  float y;
  int buttons;
};


void on_state() {




}

void off_state() {

  int exiting=FALSE;
  steer_struct mySS;
  dgc_MsgHeader myMH;

  // 2 stages
  //  1 - Check if we have a message telling us to start sending joystick commands
  //  2 - Check the joystick to see if we should request control
  

  while( ! exiting ) {
    if( dgc_AnyMessages() == TRUE ) {
      myMH = dgc_PollMessage();
      if ( myMH.MsgID == 0 ) {
	cout << "Error: Retrieving Message" << endl;
      } else {
	switch( myMH.MsgType ) {
	  case JoystickControlState:
	    if ( myMH.MsgSize != sizeof(int)) {
	      cout << "Error: Message size is not what was expected" << endl;
	      // just let the MTA delete the message
	      dgc_RemoveMessage( myMH );
	    } else {
	      // get the message (size of msg is taken from the header)
	      int myNewState;
	      dgc_GetMessage(myMH, & myNewState);
	      switch( myNewState ) {
	        case Joystick_ON:
		  exiting = TRUE;
		  myState = Joystick_ON;
		  break;
	        case Joystick_OFF:
		  // we are already in the off state, do nothing
		  // except reset our request flag
		  myRequestedState = Joystick_OFF;
		  break;
	        default:
		  cout << "Error: unknown state, just ignoring" << endl;
	      }
	    }
	    break;
	  default:
	    // we are not expecting any other messages (ignore them for now)
	    // and just tell the MTA to delete the message
	    dgc_RemoveMessage( myMH );
	}
      }
    }

    // Stage 2 - check steering wheel to see if we should request control
    // If we have already sent a message requesting control, then don't 
    // send another.
    if ( ! exiting && myRequestedState != Joystick_ON ) {
      mySS = getSteeringWheel();
      if (mySS.Buttons == TakeControlState) {
	int msg = Joystick_ON;
	myMH.Destination = 0; // send by type
	myMH.DestinationType = dgcs_SystemMaster; // request steering control from the system master
	myMH.MsgSize = sizeof(int);
	myMH.MsgType = dgcm_JoystickRequestsControl;

	myRequestedState = Joystick_ON;
	dgc_SendMessage(myMH, &msg);

	// in the next iteration of the loop, messages will again be checked
	// and the system manager may allow the joystick to take control.
     }
  }

}




int main(int argc, char* argv[])
{
  int timetoquit = FALSE;

  dgc_msg_Register(dgcs_JoystickDriver);
  
  myState = Joystick_OFF;
  myRequestedState = Joystick_OFF;
  while( ! timetoquit /* Not quitting joystick */ ) {
    if ( myState == Joystick_OFF ) { 
      off_state();
    } else if (myState == Joystick_ON) {
      on_state();
    } else {
      cout << "Error: myState is invalid, exiting" << endl;
      timetoquit = TRUE;
    }
  }

  dgc_msg_Unregister();
  return 0;
}














