#include <vector>
#include <string>
#include <list>

class PermQueue {
 public:
  PermQueue();
  PermQueue(std::string filename);
 
  ~PermQueue();

  void setLogfile(std::string filename);
  void addElement(double element);

  double avgOver(int n);

 private:
  typedef std::list<double> QueueType;

  QueueType dataQueue;

  std::string _logfilename;
};
