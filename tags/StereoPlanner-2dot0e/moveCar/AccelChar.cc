#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <time.h>
#include "../Msg/dgc_time.h"
#include <sys/time.h>
#include <unistd.h>
#include <strstream>

#include "brake.h"
#include "TLL.h"
#include "accel.h"
#include "../gps/gps.h"
#include "../latlong/LatLong.h"
#include "../OBDII/OBDII.h"

const int timeBetweenOBD2 = 100000; // 1 second between obd2 readings


enum {
  FALSE,
  TRUE
};

using namespace std;

struct DriveCommand {
  float accel;
  int seconds;
};

static float brakeVelo = .7;           // relative velocity AND accel of brake
static int gps_com=6;

int main() {

  vector<DriveCommand> queue;
  DriveCommand nextDC;
  string in;
  int bRun = FALSE;
  strstream strStream;

  cout << setprecision(8) << setiosflags(ios::showpoint);

  int testNumber;
  cout << "What test number? ";
  getline(cin, in);
  testNumber = atoi(in.c_str());
  if( testNumber <= 0) {
    cout << "Invalid Test Number" << endl;
    exit(EXIT_SUCCESS);
  }

  string testNumberStr;
  strStream << testNumber;
  strStream >> testNumberStr;

  // get a list of drive commands where each drive command is
  // set accel to ___ for ___ seconds.  once R is pressed, run
  // the set of commands, logging data along the way.

  while( bRun == FALSE ) {
    //initialize to illegal values
    nextDC.accel = -10;
    nextDC.seconds = -1;

    cout << endl 
	 << "Enter an Accel value ( -1 to 1 ) or \"r\" to run." << endl;
    getline(cin, in);
    if( in[0] == 'r' || in[0] == 'R') {
      bRun = TRUE;
    } else {
      nextDC.accel = atof(in.c_str());

      if( nextDC.accel < -1.0 || 1.0 < nextDC.accel) {
	cout << "Invalid Accel Value" << endl;
      } else {
	cout << "Accel set to " << nextDC.accel << endl;
	cout << "For how many seconds: "; 
	getline(cin, in);
	nextDC.seconds = atoi(in.c_str());

	if( nextDC.seconds <= 0) {
	  cout << in << " :Invalid Time (must be greater than zero)" << endl;
	} else {
	  cout << "Time set to " << nextDC.seconds << endl;
	  queue.push_back(nextDC);
	}
      }
    }
  }

  timeval timeZero, accelTV, obdTV;
  gettimeofday(&timeZero, NULL);
  
  double totalTime = 0;

  vector<DriveCommand>::iterator x = queue.begin();
  while( x != queue.end()) {
    totalTime += (double) (*x).seconds;
    cout << "\naccel " << (*x).accel << " for " << (*x).seconds << " seconds";
    x++;
  }
  cout <<endl;

  cout << "Total time " << totalTime << endl;

  // now time to run
  if( fork() == 0) {
    // thread 1 runs the obd2
    float EngineRPM=0, MPH=-1;

    ofstream obdFile( (string("testRuns/obd") + testNumberStr + ".dat").c_str());
    ofstream gpsFile( (string("testRuns/gps") + testNumberStr + ".dat").c_str());

    gpsFile << setprecision(8) << setiosflags(ios::showpoint);
    // Initialize OBDII
    cout << "Initializing OBD-II" << endl;
    OBD_Init(OBDSerialPort);

    // log to file
    gpsFile << "time" << "\t"
	    << "latitude" << "\t"
	    << "longitude" << "\t"
	    << "easting" << "\t"
	    << "northing" << "\t"
	    << "n.velocity" << "\t"
	    << "e.velocity" << endl << endl;
    
    //Code added by Jeremy to record GPS stuff
    gpsDataWrapper data;
    char buffer_start[2000];
    int length, msg_length;
    // buffer_start=(char*)malloc(2000);  // bad Jeremy
    if(init_gps(gps_com, 1)==1) cout << "\nGps initialized - yea! Happy dance!\n";

    LatLong conv(0,0);
    double easting, northing;

    do {
      //timeval curr;
      //gettimeofday(&curr, NULL);    
      //cout << "starting gps: " << timeval_subtract(curr, timeZero) << endl;
      // getting gps info
      get_gps_msg(buffer_start, 1000);   //this is happening at 1Hz or so
                                    // i.e. gps only outputs data every 1 sec
      //gettimeofday(&curr, NULL);    
      //cout << "before update gps: " << timeval_subtract(curr, timeZero) << endl;

      cout <<  "Trying to get GPS" << endl;
      if(get_gps_msg_type(buffer_start)==GPS_MSG_PVT) {
	data.update_gps_data(buffer_start);
      }
      cout << "Got GPS" << endl;

      //gettimeofday(&curr, NULL);    
      //cout << "end gps: " << timeval_subtract(curr, timeZero) << endl;

      // convert from lat&long to UTM
      conv.set_latlon(data.data.lat, data.data.lng);
      conv.get_UTM(&easting, &northing);

      // from OBD-II:
      
      cout << "trying to get OBD-II" << endl;
      EngineRPM = Get_RPM(); //   get EngineRPM
      MPH = Get_Speed();     //   get MPH
      cout << "finished getting OBD-ii" << endl;


      gettimeofday(&obdTV, NULL);    
      obdFile << timeval_subtract(obdTV, timeZero) << "\t" 
       	      << EngineRPM << "\t"
      	      << MPH << endl;

      // log to file
      gpsFile << timeval_subtract(obdTV, timeZero) << "\t"
	      << data.data.lat << "\t"
	      << data.data.lng << "\t"
	      << easting << "\t"
              << northing << "\t"
	      << data.data.vel_n << "\t"
	      << data.data.vel_e << endl;

      usleep(timeBetweenOBD2);
    } while(timeval_subtract(obdTV, timeZero) <= totalTime);


    obdFile.close();
    gpsFile.close();
    // kill the child thread
    exit(EXIT_SUCCESS);
  } else {  // Actuator thread
    vector<DriveCommand>::iterator x = queue.begin();
    ofstream accelFile( (string("testRuns/accel") + testNumberStr + ".dat").c_str());

    //init stuff
    char in='y';
    cout << "Initializing Accel" << endl;
    init_accel();
    cout << "Initializing Brake" << endl;
    init_brake();

    /*
    while (in != 'n') {
      cout<<"Do you want to calibrate the brake actuator? (y or n) "<<endl;
      cin >> in;
      cout << endl;
      if(in=='y' || in=='Y')
	calib_brake();
    }
    */

    // gas/brake actuation according to inputs above
    while( x != queue.end()) {
      gettimeofday(&accelTV, NULL);
      // log to file the accel we are about to set
      accelFile << timeval_subtract(accelTV, timeZero) << "\t"
		<< (*x).accel << endl;
      
      cout << "Setting accel = " << (*x).accel << endl;
      // set accel to (*x).accel
      
      //Code Added by Jeremy for setting acceleration/deceleration
      if( -1.0 <= (*x).accel && (*x).accel <= 0.0) {
	// should put accel to free if braking          
	set_accel_abs_pos( 0.0 );  // all the way out  
	set_brake_abs_pos_pot( (*x).accel *-1.0, brakeVelo, brakeVelo);
	while (brake_ready() != 0) {cout << "stuck?" << endl;}
	cout << "Setting gas to 0.0, brake to " << (*x).accel * -1.0 << endl;
      }
      else if ( 0.0 <= (*x).accel && (*x).accel <= 1.0 ) {
	// should put brake to free if accelerating                            
	set_brake_abs_pos_pot( 0.0, brakeVelo, brakeVelo);
	while (brake_ready() != 0); 
	set_accel_abs_pos( 0.0 );
	set_accel_abs_pos( (*x).accel );
	cout << "Setting brake to 0.0, gas to " << (*x).accel << endl;
      }
      else
	cout << "Bad Accel Data" << endl;
   
      while (brake_ready() != 0); 
    sleep( (*x).seconds);
    x++;
    }
  }

  //cout << "done with the run" << endl;  

  /*
  //First, stop the car!
  set_accel_abs_pos(0.0);
  set_brake_abs_pos_pot(1.0, brakeVelo, brakeVelo);
  while (brake_ready() != 0) {cout << "stuck" << endl;}
  sleep(8);
  cout << "brake position: " << getBrakePot() << endl;

  cout << "Did the car stop?" << endl;
  */

  
  // VERY IMPORTANT -- Set accel back to zero -- gives human control
  set_accel_abs_pos(0.0);
  set_brake_abs_pos_pot(0.0, brakeVelo, brakeVelo);

  cout << "Actuators should be going to zero now" << endl;
  
  // sleep for 2 just in case child thread is still running
  sleep(2);

  //cout << "brake position: " << getBrakePot() << endl;
  //cout << "Simulation Done" << endl;
 
  return 0;
}
