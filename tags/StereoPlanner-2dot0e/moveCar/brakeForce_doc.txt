Braking! Force Sensor, Linear Pot
+ circuit for force sensor, linear pot, gas control
Revision History:  7/14/2003   Sue Ann Hong    Created
                   7/15/2003   Sue Ann Hong    Added force sensor doc
                   7/17/2003   Sue Ann Hong    Almost done
                   7/18/2003   Sue Ann Hong    Added pot & gas
----------------------------------------------------------------------------

CIRCUIT DESIGN FOR PARPORT "PP_BTENS"
  This port reads from the brake force sensor(BT) and the linear pot on the 
brake actuator(BPOT), and also controls the gas actuator.

 * No-latch version:
 PPORT PIN LAYOUT::usage
  PIN02-PIN09 D0-D7 :: data lines for BT & BPOT
  PIN10-PIN13 D8-D11 :: data lines for BPOT
  PIN01 :: [BT]START  [BPOT]RC  [gas]CLK
  PIN14 :: [BT]OE        --        --
  PIN16 :: [BT]ALE    [BPOT]CS  [gas]UD
  PIN17 ::    --         --     [gas]CS
 

 * Multiplexer latch version:
 PPORT PIN LAYOUT::usage
  PIN02-PIN09 D0-D7 :: data lines for BT & BPOT
  PIN10-PIN13 D8-D11 :: data lines for BPOT
  PIN01 :: [BT]OE        --        --
  PIN14 :: [BT]START  [BPOT]RC  [gas]CLK
  PIN16 :: [BT]ALE    [BPOT]CS  [gas]UD
  PIN17 ::    --         --     [gas]CS
 Other ghetto controls:
  [BT][BPOT]multiplexer: 
    pin1  (Select output)  :: BT_OE
    pin15 (/Enable output) :: 
 
Brake Force Sensor (TLL-1K)
Part Number: TLL-1K
Manual/"specs": go to grandchallenge.caltech.edu
hardware: sensor <--> a differential op-amp --> ADC <--> CMOS-TTL
          converting latch <--> parallel <--> magicbox
Software Contact: Sue Ann Hong (sueh@its)
Mechanical Contact: Jeff Lamb

CURRENT STATUS:
 * currently running w/ +/-12VDC. will use +/-15VDC if no problems.
 * need to calibrate the circuit w/ a voltage feeder
 * finalize PCB layout on Protel
 * MTA?
 * documentation: steps 4-6

1. Design
  The force sensor outputs voltage at ~2.3mV/V/1000lbs. Since it requires 10V,
 this means the output voltage of the force sensor is between 0V and 23mV.
 The output voltage is hence amplified then converted to digital so that the
 MagicBox can read it and convert the voltage to force.

   Interface (h/w)
  Sensor -> differential amp (LM324N w/ Rs) 
  -> ADC w/ parallel output (ADC0809) 
  <-> CMOS-TTL converting buffer (CD74HCT244) <-> parallel port <-> MagicBox
  
   Initialization
  None.

   Software Implementation
  Get the parallel input, convert the voltage to force.

2. Hardware Implementation
  - As described in the Interface section. Schematic attached.
  - Amplification introduced a systematic error which is corrected linearly in
    software (getBrakeForce).
  - A/D conversion may introduce the same kind of error which hopefully will
    be adjustable.
  
3. Software Implementation
 in TLL.c
 * float getBrakeForce()
  Reads the "voltage" from the parallel interface board then returns it in lbs.

 * possible other functions: none at the moment

 * currently needed parameters: 

4. Failure Analysis
 * Noise!!!
 * Board shorted/disconnected(joints broken, wire cut, fried chip, etc)/...
 * Connection cable disconnected.
 * Power out
   - Nothing much we can do. It should come back on when power does.
 * 

5. MTA stuff
 Messages to be sent/received from/to (data structures, info)
  - to Diagnostic display? (lots of stuff)
 Control Commands:
  -

6. Finally
 Total bandwidth software uses (comm with the device & comm via MTA.
 Give Ike a paper copy and attach this to README, CVS commit it.


Additional Notes
 * driving logic
   needs - CLARAty decisions of turn angle, velocity
   does - limits velocity according to the angle
          (what if we have to limit the angle cuz we're going way too fast?
           will this never happen since we wont be going that fast?
           either way these actuators need to work with steering)
       --> final velocity and angle, calls the methods above to actually
             drive the car.
       --> needs to set positions of actuators according to the velocity 
           and angle (which is to be converted to the position of actuators
           and movement time/rate (if applicable) when calling setPosition)
   units?
