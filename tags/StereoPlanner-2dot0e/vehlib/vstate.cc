/*
 * vstate.cc - vehicle state estimtor
 *
 * RMM 31 Dec 03
 *
 * This program is the state estimator for the vehicle.  It provides an
 * on-screen display of the vehicle state and also sets up an MTA mailbox
 * that can be used to query the vehicle state.
 *
 * H 03 Jan 04 
 * Added magnetometer reading 
 * Added statefilter code
 */

#include <stdlib.h>
#include <iostream.h>
#include <unistd.h>
#include <pthread.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"
#include "sparrow/channel.h"
#include "gps.h"
#include "imu.h"
#include "magnetometer.h"
#include "frames/rot_matrix.hh"
#include "vehports.h"

#include<fstream.h>

#include "MTA/Modules.hh"
#include "vsmta.hh"
#include "MTA/Kernel.hh"
#include <boost/shared_ptr.hpp>

#include "VState.hh"
#include "statefilter.hh"

/* Functions defined in this file */
static int user_quit(long);
int veh_gps_init(long), veh_gps_process(long);

/* Variables used in this file */
int mta_flag = 1;			/* run MTA */
struct VState_GetStateMsg vehstate;	/* current vehicle state */
int state_good = 0;                      /* flag for MTA to tell that state data is good, 1 is new, 0 not updated, -1 is updating */

// channel logging declarations
// guide to log can be found in sreadme
char config_file[FILENAME_MAX] =	/* configuration file */
  "config.ini"; 
char dumpfile[FILENAME_MAX] = "vstate.dat";
DEV_LOOKUP chn_devlut[] = {
  {"virtual", virtual_driver},
  {NULL, NULL}
};
struct timeval tv;
static int start = 0;

// state internal variables not passed to outside world
/* Variables used in this file */
rot_matrix N2B;

/* Usage message */
char *usage = "\
Usage: %s [-v] [options]\n\
  -g	disable GPS subsystem\n\
  -G rate\n\
	set GPS rate (1, 2, 5, 10 Hz)\n\
  -h	print this message\n\
  -i	disable IMU subsystem\n\
  -m	disable MTA subsystem\n\
  -v	turn on verbose error messages\n\
  -a    turns off magnetometer\n\
  -s turns off statefilter\n\
  -l turns on logging\n\
";


// note, enable = 0 means device is enabled but not active, enabled = 1 means device has been initialized
// update flags are for statefilter, 0= no new data, 1 = new data received, -1 = currently updating
pthread_t gps_thread;			/* GPS thread */
void *gps_start(void *);		/* GPS startup routine */
int veh_gps_com = GPS_SERIAL_PORT;	/* serial port for GPS */
int veh_gps_rate = 10;			/* GPS rate (Hz) */
gpsDataWrapper gpsdata;			/* GPS data */
static char *gps_buffer;		/* buffer for holding GPS data */
int gps_enabled = 0;			/* flag for enabling GPS */
int gps_count = 0;			/* keep track of GPS reads */
int gps_called = 0;			/* keep track of GPS calls */
int new_gps = 0;                        /* gps update flag */ 
double gheading;                        /* gps heading, used to compare gps heading with magnetometer heading */
int gps_valid;                          /* gps solution valid or not */

/* Define variable for IMU thread */
pthread_t imu_thread;			/* IMU thread */
void *imu_start(void *);		/* IMU startup routine */
IMU_DATA imudata;			/* IMU data */
int imu_enabled = 0;			/* flag for enabling IMU */
int imu_count = 0;			/* keep track of IMU reads */
int new_imu = 0;                        /* imu update flag */
double p, r, y;

/* Magnetometer definitions */
pthread_t mag_thread;                   /* magnetometer thread */
void *mag_start(void *);                /* magnetometer startup routine */
mag_data magreading;                    /* read magnetometer data */
int veh_mag_com = MAG_SERIAL_PORT;      /* magnetometer serial port */
int mag_enabled = 0;                    /* magnetometer enable flag */
int mag_count = 0;                      /* keep track of magnetometer reads */
int new_mag = 0;                        /* mag update flag */

// statefilter definitions
pthread_t filter_thread;                /* state filter thread */
void *statefilter(void *);              /* state filter routine */
int statefilter_enable = 1;                 /* flag for enabling/disabling state filter */
int access_flag = 1;                        /* keeps two processes from accessing things at same time */

#include "vsdisp.h"			/* display */
pthread_t dd_thread;			/* display thread */

//********************** Data Logging hack ********************
fstream logfile;                        // temporary fstream for data logging until chn_write stuff is fixed
char *logname = "statelog.dat";
int logflag = 0;                        // enable/disable data logging
double imutime, gpstime, magtime;       // times for data logging

// timing delays
#define MAG_SLEEP  100000             // wait 100 miliseconds between magnetometer readings
#define GPS_SLEEP  50000              // wait 50 milliseconds between gps readings

int main(int argc, char **argv)
{
    int c, errflg = 0, error = 0;

    /* Turn on error processing during startup */
    dbg_flag = dbg_all = dbg_outf = 1;

    /* Parse command line arguments */
    while ((c = getopt(argc, argv, "vimhgaslG:?")) != EOF)
      switch (c) {
      case 'v':		dbg_flag = 1;			break;
      case 'i':		imu_enabled = -1;		break;
      case 'm':		mta_flag = 0;			break;
      case 'g':		gps_enabled = -1;		break;
      case 'h':		errflg++;			break;
      case 'a':         mag_enabled = -1;               break;
      case 's':         statefilter_enable = -1;        break;
      case 'l':         logflag = 1;                    break;
      case 'G':
	/* Set GPS rate */
	if ((veh_gps_rate = atoi(optarg)) <= 0) {
	  dbg_error("invalid GPS rate");
	  ++errflg;
	}
	break;
      default:		errflg++;			break;
	break;
      }

    /* Print an error message if anything went wrong */
    if (errflg || argc < optind) {
      fprintf(stderr, usage, argv[0]);
      exit(1);
    }

    /*
     * Device Initialization
     *
     * Initialize all of the devices that might causes errors that
     * would stop us from running.
     *
     */

    // ***************** Data logging hack ***************
    if (logflag)
      {
	logfile.open(logname, fstream::out|fstream::app);
      }

    /* GPS initialization; gps_enabled = -1 if disabled from command line */
    if (gps_enabled != -1) {
      if (init_gps(veh_gps_com, veh_gps_rate) != 1) {
	dbg_error("GPS: initialization failure\n");
	++error;
      } else
	gps_enabled = 1;
    } else
      gps_enabled = 0;
    
    /* Mag initialization; mag_enabled = -1 if disabled from command line */
    if (mag_enabled != -1) {
      if (mm_init(veh_mag_com) != 0){
	dbg_error("Magnetometer error");
	++error;
      }else{
	mag_enabled = 1;
	// initialize pry data from magnetometer
	get_mag_data(magreading);
	mag_update(magreading, vehstate, N2B);
      }
      }else
	mag_enabled = 0;

    /* IMU initialization; imu_enabled = -1 if disabled from command line */
    if (imu_enabled != -1) {
      if (InitIMU(&imudata) < 0) {
	fprintf(stdout, "IMU error\n");
	++error; 
      } else 
	imu_enabled = 1;
    } else
      imu_enabled = 0;

    /* Initialize sparrow channel structures */
    if (chn_config("vstate.dev") < 0) {
      dbg_error("can't open vstate.dev");
      ++error;
    } else if (chn_init() < 0) {
      dbg_error("error initializing channel interface");
      ++error;
    }

    // initialize starting time 
    gettimeofday(&tv, NULL); start = tv.tv_sec;

    /* Pause if there are any errors */
    dbg_info("gps_enabled = %d, imu_enabled = %d, mag_enabled = %d\n", 
	     gps_enabled, imu_enabled, mag_enabled);
    if (error) {
      fprintf(stdout, "Errors on startup; continue (y/n)? ");
      if (getchar() != 'y') exit(1);
    }

    /* Turn off printf while display is running */
    dbg_all = 0;

    /* Initialize the display package */
    if (dd_open() < 0) exit(1);		/* initialize the database, screen */
    dd_bindkey('Q', user_quit);
    dd_bindkey('c', user_cap_toggle);
    dd_usetbl(overview);		/* set display description table */

    /* Start up threads for execution */
    if(imu_enabled){
    if (pthread_create(&imu_thread, NULL, imu_start, (void *) NULL) < 0) {
      dbg_error("Can't start IMU thread \n");
      exit(1);
    }
    }
    if(gps_enabled){
    if (pthread_create(&gps_thread, NULL, gps_start, (void *) NULL) < 0) {
      dbg_error("Can't start GPS thread \n");
      exit(1);
    }
    }
    if(mag_enabled){
    if (pthread_create(&mag_thread, NULL, mag_start, (void *) NULL) < 0) {
      dbg_error("Can't start mag thread \n");
      exit(1);
    }
    }
    //    if (pthread_create(&filter_thread, NULL, statefilter, (void *) NULL) < 0) {
    //  dbg_error("Can't start statefilter thread \n");
    //  exit(1);
    // }
    /* Run the display manager as a thread */
    pthread_create(&dd_thread, NULL, dd_loop_thread, (void *) NULL);
    if (!mta_flag)
      /* If we aren't running MTA, wait until dd_loop ends */
      pthread_join(dd_thread, (void **) NULL);
    else {
      /* Otherwise, pass control to MTA */
      Register(shared_ptr<DGC_MODULE>( new VState ));
      StartKernel();
    }
    dd_close();				/* clear the screen and free memory */

    /* User cleanup goes here */
}

/*
 * Process threads 
 *
 * These routines define the process threads used in vstate.  Each of
 * them should be started up in the main code.  Once called, they
 * should loop forever.
 *
 * TBD: should integrate initialization and move these routines to their
 * respective files, so that startup becomes particularly simple.
 *
 */

void *imu_start(void *arg)
{
  
  /* Thread just reads the imu forever */
  while (1) 
    if ((imu_enabled > 0) && (new_imu == 0)) {
      IMURead(&imudata);
      ++imu_count;

      //log data
      /* Save the time the data was taken */
      gettimeofday(&tv, NULL);
      imutime = (double) (tv.tv_sec - start) + ((double) tv.tv_usec) / 1e6;
      
      // log raw imu data
      chn_data(0) = imutime;
      chn_data(1) = imudata.dvx;
      chn_data(2) = imudata.dvy;
      chn_data(3) = imudata.dvz;
      chn_data(4) = imudata.dtx;
      chn_data(5) = imudata.dty;
      chn_data(6) = imudata.dtz;

      vehstate.Timestamp = TVNow();

      vehstate.imudata = imudata;
      imu_update(N2B, imudata, vehstate);
      
      
      
	//******************** LOGGING HACK *********************
	if (logflag)
	  {
	    logfile.precision(10);
	    logfile<<imutime<<'\t'<<imudata.dvx<<'\t'<<imudata.dvy<<'\t'<<imudata.dvz<<'\t'<<imudata.dtx<<'\t'<<imudata.dty<<'\t'<<imudata.dtz<<'\t'<<gpstime<<'\t'<<gpsdata.data.lat<<'\t'<<gpsdata.data.lng<<'\t'<<gpsdata.data.vel_n<<'\t'<<gpsdata.data.vel_e<<'\t'<<gpsdata.data.vel_u<<'\t'<<magtime<<'\t'<<magreading.pitch<<'\t'<<magreading.roll<<'\t'<<magreading.heading<<'\t'<<imutime<<'\t'<<vehstate.Northing<<'\t'<<vehstate.Easting<<'\t'<<vehstate.Altitude<<'\t'<<vehstate.Vel_N<<'\t'<<vehstate.Vel_E<<'\t'<<vehstate.Vel_U<<'\t'<<vehstate.Speed<<'\t'<<vehstate.Pitch<<'\t'<<vehstate.Roll<<'\t'<<vehstate.Yaw<<endl;
	  }
      new_imu = 0;
    }
  return NULL;
}

/* Read messages from the GPS unit and put them into a buffer */
void *gps_start(void *arg)
{
  char *gps_buffer;
 
  if ((gps_buffer = (char *) malloc(2000)) == NULL) {
    dbg_panic("GPS: memory allocation error\n");
    return NULL;
  }

  while (1) {
    if ((gps_enabled > 0) && (new_gps ==0)) {
      get_gps_msg(gps_buffer, 1000); ++gps_called;
      switch (get_gps_msg_type(gps_buffer)) {
      case GPS_MSG_PVT:
	gpsdata.update_gps_data(gps_buffer);
	++gps_count;
	gps_valid =( gpsdata.data.nav_mode & NAV_VALID);
	vehstate.gpsdata = gpsdata.data;

	// get gps heading for display
	gheading = atan2(gpsdata.data.vel_n, gpsdata.data.vel_e);

	//log data
	/* Save the time the data was taken */
	gettimeofday(&tv, NULL);
	gpstime = (double) (tv.tv_sec - start) + ((double) tv.tv_usec) / 1e6;
	
	new_gps = 1;  // set flag so gps will be updated

        if (gps_valid > 0)
	  {
	    gps_update(gpsdata, vehstate, N2B);
	  }
	    new_gps = 0;  // set data updated flag       


	/* TODO: moved sleep from here to end of while loop; OK? */
     
	break;
    
      default:
	/* print_gps_msg(gps_buffer) */
	break;
      }
    }
    /* Wait a while until we process the next message */
    /* TODO: need to make sure OK to sleep here */
    //usleep(GPS_SLEEP);
  }
  return NULL;
}

// updates magnetometer data continuously
void *mag_start(void *arg)
{
  while(1){
    if ((mag_enabled > 0) && (new_mag == 0)){
    get_mag_data(magreading);
    mag_count++;
    new_mag = 1;
    vehstate.magreading = magreading;
    

    //log data
    /* Save the time the data was taken */
    gettimeofday(&tv, NULL);
    magtime  = (double) (tv.tv_sec - start) + ((double) tv.tv_usec) / 1e6;
    
    // update data
    mag_update(magreading, vehstate, N2B);
    new_mag = 0;
	
    usleep(MAG_SLEEP);

    }
    
  }
  return NULL;
}

// statefilter
// Handles integration of IMU data as well as merging of all state sensor data into the state struct
// **************************************  
// NOT CURRENTLY IN USE
void *statefilter(void *arg)
{
  if (statefilter_enable ==1)
    {
  while(1){
  
    // if a new imu packet has been received, set the flag so that it won't be updated, process it, then reset flag
    if (new_imu == 1){
      new_imu = -1;
      state_good = 0;
      //      dbg_info("Propagating IMU... ");
      imu_update(N2B, imudata, vehstate);
      //dbg_info("IMU Propagated\n");
      //dbg_info("pitch: %5.2f roll: %5.2f yaw: %5.2f \n", vehstate.Pitch, vehstate.Roll, vehstate.Yaw);
      p = N2B.get_pitch();
      r = N2B.get_roll();
      y = N2B.get_heading();

	//log data
	/* Save the time the data was taken */
	gettimeofday(&tv, NULL);
	chn_data(17) = (double) (tv.tv_sec - start) + ((double) tv.tv_usec) / 1e6;
	chn_data(18) = vehstate.Northing;
	chn_data(19) = vehstate.Easting;
	chn_data(20) = vehstate.Altitude;
	chn_data(21) = vehstate.Vel_N;
	chn_data(22) = vehstate.Vel_E;
	chn_data(23) = vehstate.Vel_U;
	chn_data(24) = vehstate.Speed;
	chn_data(25) = vehstate.Pitch;
	chn_data(26) = vehstate.Roll;
	chn_data(27) = vehstate.Yaw;

	chn_write();

      new_imu = 0;
      state_good = 1;
    }

    // do same with gps data
    if (new_gps == 1){
      new_gps = -1;
      state_good = -1;
      //dbg_info("Updating GPS state... ");
      gps_update(gpsdata, vehstate, N2B);
      
      
	//log data here if IMU is not enabled, otherwise log in imu

      if (imu_enabled != 1){
	/* Save the time the data was taken */
	gettimeofday(&tv, NULL);
	chn_data(17) = (double) (tv.tv_sec - start) + ((double) tv.tv_usec) / 1e6;
	chn_data(18) = vehstate.Northing;
	chn_data(19) = vehstate.Easting;
	chn_data(20) = vehstate.Altitude;
	chn_data(21) = vehstate.Vel_N;
	chn_data(22) = vehstate.Vel_E;
	chn_data(23) = vehstate.Vel_U;
	chn_data(24) = vehstate.Speed;
	chn_data(25) = vehstate.Pitch;
	chn_data(26) = vehstate.Roll;
	chn_data(27) = vehstate.Yaw;
	chn_write();
      }
      //dbg_info("GPS State updated\n");
      new_gps = 0;
      state_good = 1;
    }

    // same thing with magnetometer data
    if (new_mag == 1){
      new_mag = -1;
      state_good = -1;
      //dbg_info("Updating Mag state... ");
      mag_update(magreading, vehstate, N2B);
      //dbg_info("Mag state updated\n");
      //dbg_info("pitch: %f roll: %f yaw: %f \n", vehstate.Pitch, vehstate.Roll, vehstate.Yaw);

      new_mag = 0;
      state_good = 1;
    }
  

  }
  }else{
    while(1)
      {
	if (new_imu == 1)
	  new_imu = 0;
	if (new_gps == 1)
	  new_gps = 0;
	if (new_mag == 1)
	  new_mag = 0;
      }
  }
  return NULL;
}


/*
 * Callbacks to control operation of the program
 *
 */

/* Quit the dfan program */
int user_quit(long arg)
{
  int count = 0;

  if(logflag)
    logfile.close();

  if (mta_flag == 1)
    {
  /* Tell MTA to shut down */
  ForceKernelShutdown();

  /* Wait for 1 second to make sure it is shut down */
  while (ShuttingDown()) {
    if (count++ > 1000) break;
    usleep(1000);
  }

    }
  /* Tell dd_loop to abort now */
  return DD_EXIT_LOOP;
}

/* Toggle capture mode */
int user_cap_toggle(long arg)
{
  return chn_capture_flag ? chn_capture_off() : chn_capture_on();
}
