#include <stdio.h>

#include <fstream>
#include <sstream>
#include <iostream>
#include <string>

#include "steer.h"
#include "serial.h"
#include "sparrow/dbglib.h"

using namespace std;

int steer_negCountLimit;
int steer_posCountLimit;
int STEER_BY_HAND;
int steer_errno;
int steer_cmdangle;		// last commanded steering angle

int verbosity_exec_cmd = 0;


///////////////////////////THIS IS A HACK/////////////////
string lastSteeringCommanded;
//////////////////////////////////////////////////////////


int init_steer (void) 
{
    static int serialPortOpen = FALSE;     // Set to true once we've opened it.
    int justPoweredOn = FALSE;  // Not powered on in *this* function call.
    int status;                 // Receives return values for switch{}'s


    //
    // Do what we can to open the serial port.
    // 

    dbg_info("[steerCar] Trying to initialize the serial port\n");
    
    // Close the serial port first just to be safe.
    if (serialPortOpen == TRUE) {
        printf("[steerCar] Closing serial port\n");
        serial_close(STEER_SERIAL_PORT);
        serialPortOpen = FALSE;
    }

        
    // Open the serial port or die trying.
    if (serial_open_advanced(
                STEER_SERIAL_PORT, 
                B9600, 
                (SR_PARITY_DISABLE | SR_SOFT_FLOW_INPUT | 
		      SR_SOFT_FLOW_OUTPUT | SR_ONE_STOP_BIT)) != 0) 
    {
        dbg_error("Cannot open Serial Port (%d)!\n");
        return ERROR;
    }
    else {
        serialPortOpen = TRUE;
    }

    //
    // Start trying to talk to and initialize controller.
    //

    if (0) {
//// "IF CONTROLLER==OFF" 
        // The Controller is not powered on, so we should do that.
//// "SEND MESSAGE "POWERON CONTROLLER"
        justPoweredOn = TRUE;
        /* Now, we wait to get woken up by the the serial code, but if we 
           timeout, the controller must not have powered up and we should
           return an error */
        if (sleep(POWERON_TIMEOUT) == 0) {
            // We slept and nothing happened, so die.
	  dbg_error("[steerCar] Steering timeout on powerup, unrecoverable.");
        }
    }



    //
    // Test Communication with the controller, assuming serial works.
    //

    if (justPoweredOn == FALSE) {
        /* The controller has power, but we don't know for how long.  We
           have to assume it has just been turned on and wait that whole time
           before trying to send/receive any commands. */
        status = steer_test_communication(TRUE);
    }
    else {
        status = steer_test_communication(FALSE);
    }

    switch (status) {
        case TRUE:
            // Communication works!
            break;
        case FALSE:
        case ERROR:
            // Shit.
            if (justPoweredOn == TRUE) {
                // We're really screwed, since we just restarted the machine.
	      dbg_error("[steerCar] Steering is UNRECOVERABLY DEAD at initialization!!\n");
            }
            else {
                // At least we can power off and try again ...
                

                // !!! for now, RESET should suffice.
                steer_exec_cmd(RESET, verbosity_exec_cmd);
                dbg_error("[steerCar] Steering communication FAILED, rebooting controller");
//// "SEND MESSAGE "POWEROFF CONTROLLER"
//// "WAIT FOR CONFIRMATION"
                return init_steer();
            }
            break;
        default:
            steer_errno = ERR_ILL_RETVAL;
            return ERROR;
            break;
    }
    

    //
    // If the drive cannot enable, load the configuration file and check for
    // success.
    //
    steer_exec_cmd(S_DRIVE1, verbosity_exec_cmd);

    if (steer_exec_cmd(C_DRIVE1, verbosity_exec_cmd) != TRUE) {
//... !!!
      dbg_error("[steerCar] The drive is *not* enabled!!!");
        switch (steer_upload_file( (string) "./textInputFiles/be344ljInit.txt" )) {
            case TRUE:
	      dbg_error("[steerCar] File was loaded successfully");
                // File loaded successfully, continue.
                break;
            case ERROR:
	      dbg_error("[steerCar] We have some error with the upload");
                switch (steer_errno) {
                    case ERR_SERIAL_IO:
                        if (justPoweredOn == TRUE) {
			  dbg_error("[steerCar] Steering has LOST serial communication in the course of initialization, so die.");
                            // !!! This might not be the desired behavior !!!
                            return ERROR;
                        } 
                        else {
//// "TURN POWER OFF AND WAIT"                
                            sleep(POWERON_TIMEOUT);
                            return init_steer();
                        }
                    case ERR_ARG_RANGE:
                    case ERR_ILL_RETVAL:
                    case ERR_FILE_IO:
		      dbg_error("[steerCar] Can't configure drive from file, trying to see if it's already configured.");
                        if (steer_exec_cmd(C_DRIVE0, verbosity_exec_cmd) == TRUE) {
                            // We're in luck, so continue.
                            break;
                        } 
                        else {
                            // We're Screwed.
			  dbg_error("[steerCar] Steering is NOT configurable ... aborting!!");
                            return ERROR;
                        }
                        break;
                    case ERR_NONE:
                    default:
		      dbg_error("[steerCar] Illegal error value");
                        steer_errno = ERR_ILL_RETVAL;
                        // !!! is this the correct behavior?
                        return ERROR;
                        break;
                }
                break;
            case FALSE:
            default:
                steer_errno = ERR_ILL_RETVAL;
                return ERROR;
                break;
        }

        // The file was loaded; RESET establish communication if possible.
        steer_exec_cmd(RESET, verbosity_exec_cmd);
        sleep(POWERON_TIMEOUT);

        // Inspect our communication state.
        switch (steer_test_communication(TRUE)) {
            case TRUE:
	      dbg_info("[steerCar] File loaded and controller reset successfully!");
                break;
            default:
                if (justPoweredOn == TRUE) {
                    // We can't do anything more to reset that powercycle.
		  dbg_error("[steerCar] Could not recover from RESET after power on, aborting!");
                    return ERROR;
                } 
                else {
                    // We can try recycling power as a last resort.
		  dbg_error("[steerCar] Could not recover from RESET, cycling power!");
//// "TURN POWER OFF"
                    return init_steer();
                }
                break;
        }
    } 
    else {
        // Don't need to load configuration file since we can enable drive.
    }
            
//...
//... we should be ready to start just loading commands (communication seems fine), but temperature or other checks might be appropriate here.

    steer_exec_cmd(S_DRIVE1, verbosity_exec_cmd);
    if (steer_exec_cmd(C_DRIVE1, verbosity_exec_cmd) != TRUE) {
      dbg_error("[steerCar] Drive did not enable for some reason");
        steer_errno = ERR_ILL_RETVAL;
        return ERROR;
    }

    //
    // At This point, we assume that we have communication until at least the
    // thing that we try to execute in this function (it would be silly to
    // check after *every* command).
    //
    // We proceed by initializing all controller parameters and settings.
    //

    // 
    // Get the home position.
    //

    // Prepare for actual home command.
    steer_exec_cmd(S_MA1, verbosity_exec_cmd);
    steer_exec_cmd(S_LH3, verbosity_exec_cmd);
    steer_exec_cmd(S_LS0, verbosity_exec_cmd);

#ifdef UNUSED
    // RMM/JCL: put in a movement comment before doing the HOM0
    // This seems to make a difference in manual testing, although
    // we aren't quite sure why
    char buff[100];		// buffer for storing command
    usleep(100000);		// wait for a 1/10th second 
    sprintf(buff, "D5000\n\r");
    serial_write (STEER_SERIAL_PORT, (char *) buff, strlen(buff),
		  SerialBlock);
    steer_exec_cmd(GO, verbosity_exec_cmd);
#endif

    // Find the home position.
    dbg_info("[steerCar] Homing the steering ...");
    steer_exec_cmd(HOM0, verbosity_exec_cmd);

    // Wait until the home position has been reached.
    while (steer_state_moving(TRUE) == TRUE || steer_state_home_complete(TRUE) == FALSE) {
// ... provide some way of breaking out of this and trying to initialize the steering.
      continue;
    }
    dbg_info("[steerCar] Homing procedure finished.");
    
    // 
    // Set the software limits
    //

    // Use slow accelerations and velocities for this... 
    steer_exec_cmd(S_V4, verbosity_exec_cmd);
    steer_exec_cmd(S_A10, verbosity_exec_cmd);

    // Actually get the software limits.
    dbg_info("[steerCar] Trying to set the Software Limits using steer_set_sw_limits()");
    steer_set_sw_limits();

    // Make sure that the wheels aren't moving.
    // ...

    // Now, finish up initialization...
    steer_exec_cmd(S_LS3, verbosity_exec_cmd);
    steer_exec_cmd(S_COMEXC1, verbosity_exec_cmd);
    steer_exec_cmd(S_COMEXL1, verbosity_exec_cmd);
    steer_exec_cmd(C_MA1, verbosity_exec_cmd);
    steer_exec_cmd(S_A20, verbosity_exec_cmd);
    steer_exec_cmd(S_V8, verbosity_exec_cmd);
    //steer_exec_cmd(S_V16, verbosity_exec_cmd);
    steer_heading(0);
    steer_exec_cmd(WAIT_MOVE_DONE, verbosity_exec_cmd);

    // Wait until movement has ceased (this shouldn't be necessary).
    // ...

    dbg_error("[steerCar] End of initialization function reached ... yippee!!!");

    return TRUE;
}

/******************************************************************************
 * steer_exec_cmd
 *
 * Description:
 *      This function executes a command corresponding to the integer passed.
 *      It waits for the response from the servo.  It then compares the return
 *      string with the expected one.
 *
 * Arguments:
 *      verbose         integer that when TRUE causes verbose output to be
 *                      generated.
 *
 * Known Limitations / Bugs
 *      This function is blocking if it cannot write to the serial port.
 *
 * Return Values:
 *      TRUE    returned string matches
 *      FALSE   returned string does not match
 *      ERROR   some error, check the error number
 *****************************************************************************/
int steer_exec_cmd(int cmdIndex, int verbose)
{
    int readLength = 0;                 // Number of chars read from serial.
    char inputBuff[STEER_BUFF_LEN];     // Buffer filled with them chars.
    string inputString;                 // String made from buffer.


    // Check if cmdIndex is within range.
    if (cmdIndex < 0 || cmdIndex >= NUM_STEER_CMDS) {
      dbg_error("[ERROR] %s (%d): Command is out of range (%d)!\n",
                __FILE__, __LINE__, cmdIndex);
        steer_errno = ERR_ARG_RANGE;
        return ERROR;
    }


    if (verbose == TRUE) {
        // Be openly happy that we can read.
      dbg_info("[TRYING] %s", steerCmds[cmdIndex].cmd.c_str());
    }


    //
    // Try to write the command.
    // 

    if (serial_write (
                STEER_SERIAL_PORT, 
                (char *) steerCmds[cmdIndex].cmd.c_str(), 
                steerCmds[cmdIndex].cmd.length(), 
                SerialBlock
                ) 
            != (int) steerCmds[cmdIndex].cmd.length())
    {
        /* Bad things should happen if we exit without writing the wrong length
           string */
        dbg_error("[ERROR] %s (%d): Incomplete serial port write!\n",
                __FILE__, __LINE__);
        steer_errno = ERR_SERIAL_IO;
        return ERROR;
    }

    //
    // We know that the write succeeded, now try to read response if one is
    // expected.
    //
    
    if (steerCmds[cmdIndex].retVal.length() == 0) {
        // We do not expect any return, but read what's there anyway and trash.
        serial_read(STEER_SERIAL_PORT, inputBuff, STEER_BUFF_LEN);
    }
    else {
        // Get the number of characters read and fill buffer with chars.
#if 0
        for (int i = 0; i < STEER_BUFF_LEN-1; i++) {
            usleep(100000);
            if (serial_read (STEER_SERIAL_PORT, inputBuff+i, 1) == 1) {
	      //                cout << i << ": \t" << (int) inputBuff[i] << "\t(" << inputBuff[i] << ")" << endl;
            } 
            else {
                // we don't have anything there anymore, so wait a while to look at stuff.
	      //                cout << "[steerCar] That's all, folks!\n";
                sleep(7);
                break;
            }
            readLength = i+1;
        }
#endif 
        usleep(100000);
        readLength = serial_read_until (STEER_SERIAL_PORT, inputBuff, (char) STEER_EOT, SEC_TIMEOUT, STEER_BUFF_LEN - 1);
        //cout << "readlength is " << readLength << endl;
        // Make this an actual string by appending a terminator.
        inputBuff[readLength] = '\0';               
        // Make string 'cause they are cooler:
        inputString = inputBuff;

        if (readLength <= 0) {
            // Nothing returned and we probably timed out.
	  //            cout << steerCmds[cmdIndex].retVal << endl;
            dbg_error("[ERROR] %s (%d): No Serial port read (trying %s).\n", 
                    __FILE__, __LINE__, steerCmds[cmdIndex].cmd.c_str());
            return ERROR;
        } 
        else {

            /* Check if we have the expected return value.  Null (zero-length)
               string represents no expected return from the device. */
            if (steerCmds[cmdIndex].retVal.compare ( inputString ) == 0
                    || steerCmds[cmdIndex].retVal.length() == 0 )       
            { 
                /* We have the data we wanted, so output if desired (the output
                   string has a newline, but no carriage return. */
                if (verbose == TRUE) {
                    cout << inputString.substr(0, readLength-1) << endl;;
                }
            }
            else {
                // We have something that simply doesn't match exactly.
                dbg_error("[UNEXPECTED] %s (%d): inputBuff (",  __FILE__, __LINE__);
		//                cout << inputString.substr(0, readLength-1)     // Does not have \r.
		//                    << ") does not match expected retVal ("
		//                    << steerCmds[cmdIndex].retVal.substr(0, steerCmds[cmdIndex].retVal.length() - 1) // Does not have \r.
		//                    << ")." <<endl;
                return FALSE;
            }
        }
    }


    return TRUE;
}

/******************************************************************************
 * steer_heading
 *
 * Description:
 *      This function instructs the car to steer the heading, but does not wait
 *      for that heading to actually be achieved until returning.... it assumes
 *      that COMEXC1 has already been issued.
 *
 * Arguments:
 *      float angle     direction: full left is -1, center is 0, full right is
 *                      1.
 *
 * Return Values:
 *      TRUE    in best faith, the code thinks we steered there.
 *      FALSE   [not used]
 *      ERROR   some error, check the error code.
 *****************************************************************************/
int steer_heading (float angle)
{
    char buff[100];

    // Make sure that we aren't out of our limits
// !!! use CONSTANTS instead of {-1,1}
    if (angle < -1 || angle > 1) {
        dbg_error("[%s %d] Illegal steering directive (%f).\n",
                __FILE__, __LINE__, angle); 
        steer_errno = ERR_ARG_RANGE;
        return ERROR;
    }

    /* Negation because the code convention is neg==>left, while for the motor,
       neg==>right */
    angle *= -1;         

    // Make angle be in counts (native controller unit) rather than normalized:
    if (angle < 0) {
        angle *= abs(steer_negCountLimit);
    } else {
        angle *= abs(steer_posCountLimit);
    }

    // Check to see if this is a new command
    if ((int) angle == steer_cmdangle) return TRUE;
    steer_cmdangle = (int) angle;	// save commanded steering angle

    // Now, convert to a string to send to the controller.
    buff[0] = 'D';              // command is of form D<pos>
    sprintf(buff+1, "%.0f\n\r", angle);
    if (serial_write (
                STEER_SERIAL_PORT, 
                (char *) buff,
                strlen(buff),
                SerialBlock
                ) 
            != (int) strlen(buff))
    {
      dbg_error("[%s %d] Not all of string was written.\n", __FILE__, __LINE__); 
        steer_errno = ERR_SERIAL_IO;
        return ERROR;
    }
    if (steer_exec_cmd(GO, FALSE) != TRUE) {
        fprintf(stderr, "[%s %d] steer_exec_cmd() error.\n", __FILE__, __LINE__); 
        return ERROR;
    }
    else {
        // buff has a newline in it.
      //      dbg_info("Steering commanded = %s", buff+1);
    }

    lastSteeringCommanded = (string) (buff+1);

    return TRUE;
}



/******************************************************************************
 * steer_test_communication
 *
 * Description:
 *      This function tests to see whether or not we have communication with 
 *      the controller.  It will either wait the poweron time of the device or 
 *      immediately try to read/write it.
 *
 * Arguments:
 *      int wait        This is either TRUE or FALSE and decides whether or not
 *                      to wait the poweron timeout.
 *
 * Known Limitations / Bugs
 *      The first while loop will hang forever if the drive keeps sending data
 *      forever.
 *
 * Return Values:
 *      TRUE    communication is good.
 *      FALSE   cannot talk to the controller, but I/O works
 *      ERROR   some error, check the error code.
 *****************************************************************************/
int steer_test_communication (int wait)
{
    int status;

    if (wait == TRUE) {
        // Johnson's code should interrupt us here if we get data.
        sleep(POWERON_TIMEOUT);
    }

    // Empty serial buffer to continue in a clear state
    serial_clean_buffer(STEER_SERIAL_PORT);

    /* Set up communications to be the way we like, so that our command 
       execution gets the right strings back on success (':', '\n', and '\r'
       are legal input command delimiters, see p.4).  
       status is the result of all these commands. */
    status = steer_exec_cmd(STOP, verbosity_exec_cmd);
    status |= steer_exec_cmd(S_EOL, verbosity_exec_cmd);
    status |= steer_exec_cmd(S_EOT, verbosity_exec_cmd);
    status |= steer_exec_cmd(S_ECHO0, verbosity_exec_cmd);
    status |= steer_exec_cmd(S_ERRLVL0, verbosity_exec_cmd);

    /* Check that nothing stupid happened with the serial communications. No 
       point in checking for FALSE because the status wouldn't work with the 
       ORs. */
    if ( (status & ERROR) == ERROR ) {
        // Keep the same error number and return.
      dbg_error("Communication failed unexpectedly in test");
        return ERROR;
    }

    /* The status is only TRUE; all good thus far.  Test communciation
       with TREV (could be any other command that returns the same thing,
       regardless of the state of the drive). */
    switch (steer_exec_cmd(C_TREV, verbosity_exec_cmd)) {
        case TRUE:
	  dbg_info("The controller is responding to commands.");
            break;
        case FALSE:
            steer_exec_cmd(C_EOL, verbosity_exec_cmd);
            steer_exec_cmd(C_EOT, verbosity_exec_cmd);
	    dbg_error("Serial communication appears to be working, but controller is not responding.");
            return FALSE;
            break;
        case ERROR:
        default:
            // Shit happened... pass on the error number.
            return ERROR;
	    dbg_error("Communication failed even more unexpectedly in test");
            break;
    }

    // It seems appropriate to upate the controller state here.
    // steer_state_update(TRUE,TRUE);
    
    return TRUE;
}


/******************************************************************************
 * steer_upload_file
 *
 * Description:
 *      This function uploads the given file to the steering controller.
 *
 * Arguments:
 *      string fileName This is the filename... the current directory is
 *                      assumed to be the path.
 *
 * Known Limitations / Bugs:
 *      This is a blocking function if the serial port cannot be written.
 *
 * Return Values:
 *      TRUE    file is uploaded and spotchecked.
 *      FALSE   [not used]
 *      ERROR   some error, check the error code.
 *****************************************************************************/
int steer_upload_file (string fileName)
{
    string line;
    ifstream infile;
    int readLength;
    char inputBuff[STEER_BUFF_LEN];
    infile.open( fileName.c_str() );

    cout << fileName << endl;

    if (infile.good() != true) {
      //        cout << "File " << fileName << " could not be opened, aborting." << endl;
        steer_errno = ERR_FILE_IO;
        return ERROR;
    } else {
      //        cout << "File " << fileName << " was opened successfully." << endl;
    }

    steer_exec_cmd(S_ECHO1, TRUE);
    while( getline(infile, line) ) {
        line += '\n';
        //cout << line;  
        if (serial_write (
                    STEER_SERIAL_PORT, 
                    (char *) line.c_str(),
                    line.length(),
                    SerialNonBlock
                    ) 
                != (int) line.length())
        {
            fprintf(stderr, "[%s %d] Not all of string was written.\n", __FILE__, __LINE__); 
            steer_errno = ERR_SERIAL_IO;
            return ERROR;
        }
    }
    
    // 
    // Now, write a blank line in a blocking fashion so we know that everything
    // has been written.
    //

    // Assume there are no errors.
    serial_write (STEER_SERIAL_PORT, (char *) STEER_EOT, 1, SerialBlock);
    steer_exec_cmd(S_ECHO0, TRUE);
    readLength = serial_read (STEER_SERIAL_PORT, inputBuff, STEER_BUFF_LEN-1);
    inputBuff[readLength] = '\0';
    cout << inputBuff << endl;


    return TRUE;
}



/******************************************************************************
 * steer_set_sw_limits
 *
 * Description:
 *      This function travels to the hardware limits and sets the software
 *      limits inside them.  It assumes that the serial communication is up.
 *      It also sets the limits in this code.
 *
 * Arguments:
 *      [none]
 *
 * Global Variables used:
 *      steer_negCountLimit   Set to the negative sw limit
 *      steer_posCountLimit   Set to the positive sw limit
 *
 * Known Limitations / Bugs:
 *      This is a blocking function if the serial port cannot be written.
 *      lots of magic numbers and little error checking ... actually, pretty
 *      much none.
 *      do NOT call this function when speeds are set high. it might damage
 *      the pow3er steering.
 *
 * Return Values:
 *      TRUE    file is uploaded and spotchecked.
 *      FALSE   [not used]
 *      ERROR   some error, check the error code.
 *****************************************************************************/
int steer_set_sw_limits (void)
{
    string outString;
    stringstream negLimit, posLimit;

    // 
    // Deal with the negative limit first.
    //

    // First, go to the negative limit and then figure out the position.
    outString = (string) "D-500000" + STEER_EOT + "GO" + STEER_EOT;
    serial_write (STEER_SERIAL_PORT, (char *) outString.c_str(), outString.length(), SerialBlock);

    // Wait until we have stopped moving (so we can get the position).
    while ( steer_state_limit_neg(TRUE) == FALSE || steer_state_moving(TRUE) == TRUE) {
// ... A way to break out of here in an emergency to make this loop non-blocking would be nice.
        continue;
    }
    
    // Calculate what we want the software limit to be.
    steer_negCountLimit = steer_state_TPE() + 5000;
    dbg_info("steer_negCountLimit = %d\n", steer_negCountLimit);

    // Set this in the controller as well.
    negLimit << steer_negCountLimit;
    outString = (string) "LSNEG" + negLimit.str() + STEER_EOT;
    serial_write (STEER_SERIAL_PORT, (char *) outString.c_str(), outString.length(), SerialBlock);

    //
    // Deal with the positive limit next.
    //

    // Go to the limit so we can read back the position.
    outString = (string) "D500000" + STEER_EOT + "GO" + STEER_EOT;
    serial_write (STEER_SERIAL_PORT, (char *) outString.c_str(), outString.length(), SerialBlock);

    // Wait until we have stopped moving (so we can get the position).
    while ( steer_state_limit_pos(TRUE) == FALSE || steer_state_moving(TRUE) == TRUE) {
// ... A way to break out of here in an emergency to make this loop non-blocking would be nice.
        continue;
    }
    
    // Calculate the software limit.
    steer_posCountLimit = steer_state_TPE() - 5000;
    dbg_info("steer_posCountLimit = %d\n", steer_posCountLimit);

    // Set this in the controller as well.
    posLimit << steer_posCountLimit;
    outString = (string) "LSPOS" + posLimit.str() + STEER_EOT;
    serial_write (STEER_SERIAL_PORT, (char *) outString.c_str(), outString.length(), SerialBlock);


    cout << "The software limits are now " << steer_posCountLimit << " and " << steer_negCountLimit << "." << endl;

    return TRUE;
}

/*
 * steerState.cc
 */

int steer_state_blocking (int);


extern int steer_negCountLimit;
extern int steer_posCountLimit;
extern int STEER_BY_HAND;
extern int verbosity_exec_cmd;
extern int steer_errno;

///////////////////////////THIS IS A HACK/////////////////
extern string lastSteeringCommanded;
//////////////////////////////////////////////////////////

enum {
    TPE,        // Transfer Encoder position
    TAS,        // Transfer Axis State
    TASX,       // Transfer Extended Axis State
    TSS,        // Transfer System Status
    TER,
    TDTEMP,     // Transfer Drive Temp. (higher of DSP/power block)
    TMTEMP,     // Transfer Motor Temp. (predicted motor winding).
    TDHRS,      // Transfer Operating hours (nearest 1/4 hour).
    NUM_STATE_VARS
};
// The actual saved states/values.
string steerState[NUM_STATE_VARS] = {(string) "1234567890", (string) "1234567890", (string) "1234567890", (string) "1234567890", (string) "1234567890", (string) "1234567890", (string) "1234567890", (string) "1234567890"};
// Timestamps for each of the saved values.
timeval steerStateTimestamps[NUM_STATE_VARS] = {{0,0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}};
// Time in seconds for each datum to expire.  (.3 works, stresses the system A LOT)
double steerStateExpire[NUM_STATE_VARS] = {0.5, 2.6, 6.7, 8.9, 13, 21, 23, 27};
// Strings sent to the steering controller.
string serialStateQuery[NUM_STATE_VARS] = {(string) "TPE", (string) "TAS", (string) "TASX", (string) "TSS", (string) "TER", (string) "TDTEMP", (string) "TMTEMP", (string) "TDHRS"};

/******************************************************************************
 * steer_state_update
 *
 * Description:
 *      Updates the controller state every time called.  Global strings are
 *      written with the values of the commands read, without the echoing of
 *      the command given.  Alas, indexing is one off on the string versus the
 *      manual, because the manual starts with one instead of zero.  
 *      Even though it's annoying, the code writes everything to the controller
 *      and then reads it all back instead of doing things individually since
 *      communication with the controller is the bottleneck.  Also, there are
 *      those stupid underscores to watch out for, so they are removed for
 *      storage in the global variables.
 *
 * Arguments:
 *      log             integer that causes controller state to be logged when 
 *                      TRUE.
 *      updateAll       integer that spefifies that all parameters should be
 *                      recorded when TRUE.  Otherwise, TPE,TAS are updated.
 *
 * Global Variables:
 * [all of the below are written with the command of the same name]
 [...]
 * 
 * Return Values:
 *      TRUE    Data has been updated and/or logged.
 *      FALSE   Returned if function is called to fast (no new data).
 *      ERROR   either file I/O or serial.
 *****************************************************************************/
int steer_state_update (int log, int updateAll)
{
    unsigned int lastSTEER_EOT;  // The last STEER_EOT parsed (for updating variables).
    int readLength;     // Length Read by the serial code.
    int expectedReplies = NUM_STATE_VARS;
    char inputBuff[STEER_BUFF_LEN];  // Buffer serial code can read into.
    string serialInputString;
    timeval timeStamp;
    double elapsed;

//cout << __FILE__ << __LINE__ << endl; fflush(stdout);
    if ( !updateAll ) 
        expectedReplies = 2;    // Magic number follows.


    string fileName;    // Filname we log to.
    ofstream logFile;   // File we log to.
    if (log == TRUE) {
        // http://beta.experts-exchange.com/Programming/Programming_Languages/Cplusplus/Q_20650231.html#8751396
        char Time[20];
        time_t time_now = time(NULL);
        strftime( Time, 20, "%Y%m%d", localtime( &time_now ) );
        fileName = (string) "LOG.steering." + Time + ".dat";
        logFile.open( fileName.c_str(), ios::app );
        if (logFile.good() != true) {
	  //            cout << "File " << fileName << " could not be opened to append." << endl;
            steer_errno = ERR_FILE_IO;
            return ERROR;
        }
    }

    // Clear any data in the buffer.
    serial_clean_buffer(STEER_SERIAL_PORT);


    //
    // Open a big loop to read and write the commands we care about.
    //

    for (int i = 0; i < expectedReplies; i++) {
        //
        // Check how long it's been since the last time we tried to update the
        // variables and decide whether or not to proceed
        //

        gettimeofday(&timeStamp, NULL);
        // Get the elapsed time. 
        elapsed = timeStamp.tv_sec - steerStateTimestamps[i].tv_sec;
        elapsed += ((double) (timeStamp.tv_usec - steerStateTimestamps[i].tv_usec)) / 1000000;
        // Fix Midnight rollover
        if ( elapsed < 0 ) {
            // Add number of seconds in a day
            elapsed += 24 * 3600;
        }

        // Don't get new data if the data hasn't expired.
        if ( elapsed < steerStateExpire[i] ) {         
            continue;
        }


        //
        // Send commands to the controller.
        // 

        if ( -1 == serial_write (
                    STEER_SERIAL_PORT, 
                    (char *) ( (string) "!" + serialStateQuery[i] + (char) STEER_EOT ).c_str(), 
                    serialStateQuery[i].length() + 2, 
                    SerialBlock
                    )
           ) {
            steer_errno = ERR_SERIAL_IO;
            return ERROR;
        }

        // 
        // Read the replies
        //
        readLength = 0;
        readLength = serial_read_until (
                STEER_SERIAL_PORT, 
                inputBuff + readLength, 
                (char) STEER_EOT, 
                SEC_TIMEOUT, 
                STEER_BUFF_LEN - 1
                );
        // Error Checking
        if ( readLength == 0 ) {
	  //            cout << "Nothing read (trying " << serialStateQuery[i] << "), returning " << endl; fflush(stdout);
            logFile << "Nothing read (trying " << serialStateQuery[i] << "), returning " << endl; fflush(stdout);
            steer_errno = ERR_SERIAL_IO;
            return ERROR;
        }
 
        // Make it a string.
        inputBuff[readLength] = '\0';       

//cout << inputBuff;
        if ( updateAll ) {
            cout << inputBuff;
        }
        //
        // Deal with the data.
        //

        // STL is so much cooler.
        serialInputString = (string) inputBuff;
        // Get Rid of underscores
        while ( serialInputString.find('_') != string::npos ) {
            serialInputString.erase(serialInputString.find('_'), 1);
        }

        lastSTEER_EOT = serialInputString.find( (char) STEER_EOT, serialStateQuery[i].length() );
        if ( lastSTEER_EOT == string::npos ) {
            cout << "Parse error in serialInputString (" << serialInputString << ")\n";
            steer_errno = ERR_SERIAL_IO;
            return ERROR;
        }
        steerState[i] = serialInputString.substr(
                serialStateQuery[i].length(), 
                lastSTEER_EOT - serialStateQuery[i].length()
                );
        serialInputString = serialInputString.substr(lastSTEER_EOT + 1);

        // Set the time of update.
        gettimeofday( &(steerStateTimestamps[i]), NULL);

        if ( log ) {
            logFile << "[" 
                << ((string) asctime(localtime(&(steerStateTimestamps[i].tv_sec)))).erase(24)
                << " (+"
                << ((double) steerStateTimestamps[i].tv_usec) / 1000000
                << " sec)] " 
                << serialStateQuery[i] << "\t" << steerState[i] << endl;
            ////////////THIS IS A HACK/////////////
            if (i == TPE) {
                logFile << ((string) asctime(localtime(&(steerStateTimestamps[i].tv_sec)))).erase(24) << "\t" << steerState[i] << "\t" << lastSteeringCommanded;
            }
        }

    }

    if ( log ) {
        logFile.close();
    }

    return TRUE;
}



/******************************************************************************
 * steer_state_limit_pos
 *
 * Description:
 *      Checks if we have reached a positive limit, hardware or software.
 *
 * Global Variables:
 *      steerState      read, not modified
 *
 * Return Values:
 *      TRUE    We are at a limit
 *      FALSE   We are not at a limit
 *****************************************************************************/
int steer_state_limit_pos (int blocking)
{
    if ( blocking ) {
        if (steer_state_blocking (TAS) == ERROR) 
            return ERROR;
    }
    else {
        if (steer_state_update(FALSE, FALSE) == ERROR) 
            return ERROR;
    }

    if ( 
            steerState[TAS][16] == '1' // POS SW
            || steerState[TAS][14] == '1' // POS HW
       ) 
    {
        return TRUE;
    }
    else {
        return FALSE;
    }
}


/******************************************************************************
 * steer_state_limit_neg
 *
 * Description:
 *      Checks if we have reached a negative limit, hardware or software.
 *
 * Global Variables:
 *      steerState[TAS]   read, not modified
 *
 * Return Values:
 *      TRUE    We are at a limit
 *      FALSE   We are not at a limit
 *****************************************************************************/
int steer_state_limit_neg (int blocking)
{
    if ( blocking ) {
        if (steer_state_blocking (TAS) == ERROR) 
            return ERROR;
    }
    else {
        if (steer_state_update(FALSE, FALSE) == ERROR)
            return ERROR;
    }

    if ( 
            steerState[TAS][17] == '1'    // NEG SW
            || steerState[TAS][15] == '1' // NEG HW
            ) 
    {
        return TRUE;
    }
    else {
        return FALSE;
    }
}


/******************************************************************************
 * steer_state_home_complete
 *
 * Description:
 *      Checks if we have completed a home sequence
 *
 * Global Variables:
 *      steerState[TAS]   read, not modified
 *
 * Return Values:
 *      TRUE    We have completed the sequence
 *      FALSE   We have not completed the sequence
 *****************************************************************************/
int steer_state_home_complete (int blocking)
{
    if ( blocking ) {
        if (steer_state_blocking (TAS) == ERROR) 
            return ERROR;
    }
    else {
        if (steer_state_update(FALSE, FALSE) == ERROR) 
            return ERROR;
    }

    if ( steerState[TAS][4] == '1' ) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}


/******************************************************************************
 * steer_state_faulted
 *
 * Description:
 *      Checks if the drive is faulted.
 *
 * Global Variables:
 *      steerState[TAS]   read, not modified
 *
 * Return Values:
 *      TRUE    We are faulted
 *      FALSE   We are not faulted
 *****************************************************************************/
int steer_state_faulted (int blocking)
{
    if ( blocking ) {
        if (steer_state_blocking (TAS) == ERROR) 
            return ERROR;
    }
    else {
        if (steer_state_update(FALSE, FALSE) == ERROR) 
            return ERROR;
    }

    if ( steerState[TAS][13] == '1' ) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}


/******************************************************************************
 * steer_state_moving
 *
 * Description:
 *      Checks if the wheels are moving.
 *
 * Global Variables:
 *      steerState[TAS]   read, not modified
 *
 * Return Values:
 *      TRUE    We are moving
 *      FALSE   We are stopped
 *****************************************************************************/
int steer_state_moving (int blocking)
{
    if ( blocking ) {
        if (steer_state_blocking (TAS) == ERROR) 
            return ERROR;
    }
    else {
        if (steer_state_update(FALSE, FALSE) == ERROR)
            return ERROR;
    }

    if ( steerState[TAS][0] == '1' ) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}


/******************************************************************************
 * steer_state_TPE
 *
 * Description:
 *      Returns the position of the steering.
 *
 * Global Variables:
 *      steerState[TPE]   read, not modified
 *
 * Return Value:
 *      current encoder position.
 *****************************************************************************/
int steer_state_TPE (int blocking)
{
    if ( blocking ) {
        if (steer_state_blocking (TAS) == ERROR) 
            return ERROR;
    }
    else {
        if (steer_state_update(FALSE, FALSE) == ERROR) 
            return ERROR;
    }

    return atoi ( steerState[TPE].c_str() );
}


/******************************************************************************
 * steer_state_blocking
 *
 * Description:
 *      Does not return until the read time of a value is newer than the time
 *      given.
 *
 * Global Variables:
 *      steerState      read, not modified
 *
 * Return Value:
 *      ERROR           Problem with update.
 *      TRUE            Time has elapsed as planned.
 *****************************************************************************/
int steer_state_blocking (int stateIndex)
{
    timeval startTime = {steerStateTimestamps[stateIndex].tv_sec, steerStateTimestamps[stateIndex].tv_usec};

    while ( startTime.tv_sec == steerStateTimestamps[stateIndex].tv_sec
            || startTime.tv_usec == steerStateTimestamps[stateIndex].tv_usec )
    {
        // The updatetime has not changed, so try update again and see.
        if (ERROR == steer_state_update (FALSE, FALSE) ) 
            return ERROR;
    } 
    return TRUE;
}
