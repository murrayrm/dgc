#include "MTAStruct.hh"

Mail& operator << (Mail& ml, const MTAStruct& mta) {
  ml.QueueTo(&mta, sizeof(mta));
  return ml;
}

Mail& operator >> (Mail& ml, MTAStruct& mta) {
  ml.DequeueFrom(&mta, sizeof(mta));
  return ml;
}
