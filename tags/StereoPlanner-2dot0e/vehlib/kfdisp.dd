/*
 * overview.h - overview display screen for Tahoe
 *
 * RMM, 23 Apr 95
 *
 */

/* Local function declarations */
static int user_quit(long);
static int user_cap_toggle(long);

%%
%TITLE	                                            I: %imuc  G: %gpsc  M: %magc
				G: %thr   B: %brk   S: %str   T: %trx   I: %ign

Axis   IMU      GPS/MAG   KF             | Status Indicators
 X     %imdx    %gpx      %imax          | GPS Nav Valid: %gnav  
 Y     %imdy    %gpy      %imay          |      
 Z     %imdz    %gpz      %imaz          |  
 R     %imdr    %magr     %imar          |  
 P     %imdp    %magp     %imap          |   
 Y     %imdw    %magy     %imaw          |   
 V     %imdv    %gpdv     %imadv
lat			  %kflat	
lng			  %kflng

KF/IMU Scale factors, biases, and residuals:			
ABX	%abx    	RPX: %rspx
ABY	%aby		RPY: %rspy
ABZ	%abz		RPZ: %rspz		
GBX	%gbx		RVX: %rsvx
GSFX	%gsfx		RVY: %rsvy
			RVZ: %rsvz

%QUIT

%%
tblname: overview;
bufname: ovwbuf;


label: %TITLE "Tahoe KF State Estimator (v1.0)" -fg=YELLOW;

short: %imuc imu_count "%5d" -ro;
short: %gpsc gps_count "%5d" -ro;
short: %magc mag_count "%5d" -ro;

button:	%QUIT	"QUIT"	user_quit	-idname=QUITB;


float: %magy magreading.heading "%5.2f" -ro;
float: %magp magreading.pitch "%5.2f" -ro;
float: %magr magreading.roll "%5.2f" -ro;

double: %gpx gpsdata.data.lat "%7.4f" -ro;
double: %gpy gpsdata.data.lng "%7.4f" -ro;
double: %gpz gpsdata.data.altitude "%7.4f" -ro;
float: %gpdv vehstate.Speed "%5.2f" -ro;
short: %gnav gps_valid "%d" -ro;

double: %imdx imudata.dvx "%7.4f" -ro;
double: %imdy imudata.dvy "%7.4f" -ro;
double: %imdz imudata.dvz "%7.4f" -ro;
double: %imdr imudata.dtx  "%7.4f" -ro;
double: %imdp imudata.dty  "%7.4f" -ro;
double: %imdw imudata.dtz  "%7.4f" -ro;

double: %imax vehstate.Northing "%7.1f" -ro;
double: %imay vehstate.Easting "%7.1f" -ro;
double: %imaz gpsdata.data.altitude  "%7.1f" -ro;
float: %imap vehstate.Pitch "%5.2f" -ro;
float: %imar vehstate.Roll "%5.2f" -ro;
float: %imaw vehstate.Yaw "%5.2f" -ro;
float: %imadv vehstate.Speed "%5.2f" -ro;
double: %kflat vehstate.kf_lat "%7.4f" -ro;
double: %kflng vehstate.kf_lng "%7.4f" -ro;

double: %abx tot_kabx "%7.6f" -ro;
double: %aby tot_kaby "%7.6f" -ro;
double: %abz tot_kabz "%7.6f" -ro;
double: %gbx tot_kgbx "%7.6f" -ro;
double: %gbx tot_kgby "%7.6f" -ro;
double: %gbx tot_kgbz "%7.6f" -ro;
double: %gsfx tot_kgsfx "%7.6f" -ro;

double: %rspx ExtGPSPosMeas_x "%7.4f" -ro;
double: %rspy ExtGPSPosMeas_y "%7.4f" -ro;
double: %rspz ExtGPSPosMeas_z "%7.4f" -ro;
double: %rsvx ExtGPSVelMeas_x "%7.4f" -ro;
double: %rsvy ExtGPSVelMeas_y "%7.4f" -ro;
double: %rsvz ExtGPSVelMeas_z "%7.4f" -ro;


# string: %ctrlfile ctrlfile "%s" -callback=df_load_ctrl -idname=CTRLFILE;
# string: %trajfile trajfile "%s" -callback=df_load_traj;
# string: %velfile velfile "%s" -callback=df_load_vels;
# string: %mapfile mapfile "%s" -callback=df_load_map;
# string: %dumpfile dumpfile "%s";


# short:	%jy		jy			"%4u"		-ro;
# short:  %jbut		chn_bits(JOYB_CHN)	"%4u"		-ro;

# button: %ctrl	"Control"	ctrl_toggle   -fg=RED -idname=CTRLB;
# button: %joy    "Joystick" 	joy_toggle    -fg=RED -idname=JOYB;
# button: %ff	"FF"	 	ff_toggle     -fg=RED -idname=FFB;
# button: %dcp	"DCP"	 	dcp_toggle    -fg=RED -idname=DCB;
# button: %gs	"GS"	 	gs_toggle     -fg=RED -idname=GSB;
# button: %tbl	"TBL" 		tbl_toggle    -fg=RED -idname=TBLB;

# button: %jyraw	"motor/flap"	joy_raw	      -fg=RED -idname=JRAWB;
# button: %jydir	"f1/f2" 	joy_direct    -fg=RED -idname=JDIRB;
# button: %pitalt "pitch/alt"  	joy_pitchalt  -fg=RED -idname=JPAB;
# button: %xypos  "XY posn" 	joy_XYpos     -fg=RED -idname=JXYB;
