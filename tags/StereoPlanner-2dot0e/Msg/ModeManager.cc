// dgc_msg_main.cc 

#include "dgc_mta.hh"
#include "dgc_msg.h"
#include "dgc_socket.h"
#include "dgc_messages.h"
#include "dgc_const.h"
#include "dgc_mta_main.h"
#include "Log.hh"
#include "dgc_time.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> 

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <iostream>
#include <iomanip>
#include <list>
#include <time.h>
#include <sys/time.h>

struct AccelAndSteering {
  float steering;
  float accel;
};



struct steer_struct {

  AccelAndSteering as;
  int buttons;
};


int shutdown_flag;
using namespace std;

int inControl;
int oldInControl;


const double StateRefreshRate = 1/2; // refresh 2ce a second
const int JOYSTICK_ON_BUTTONS = 15;
const int JOYSTICK_OFF_BUTTONS = 16;

int DriveCommandPacket(dgc_MsgHeader & myMH, steer_struct & mySS);


int main(int argc, char* argv[])
{
  steer_struct mySS;
  int takeover;
  timeval before, after;
  gettimeofday(&before, NULL);


  shutdown_flag = FALSE;
  dgc_msg_Register(dgcs_ModeManager);
  cout << endl << endl << "Registering at time t=" 
       << setprecision(10) << SystemTime() << endl;
  dgc_MsgHeader myMH;

  // let DStar be in control for now
  inControl = dgcs_GlobalRoutePlanner;
	

  while ( TRUE ) { 

    usleep ( 30000 );
    if ( dgc_PollMessage(myMH) == TRUE) {
      switch (myMH.MsgType) {
      case dgcm_DriveCommandPacket:
      case dgcm_JoystickStatePacket:
	// Get the message
	if ( dgc_GetMessage(myMH, &mySS) == TRUE ) {
	  DriveCommandPacket(myMH, mySS);

	  // check for possible takeover from joystick due to buttons pressed
	  if( myMH.SourceType == dgcs_JoystickDriver && 
	      mySS.buttons == JOYSTICK_ON_BUTTONS &&
	      inControl != dgcs_JoystickDriver ) {
	    oldInControl = inControl;
	    inControl = dgcs_JoystickDriver;
	  }else if( myMH.SourceType == dgcs_JoystickDriver && 
		    mySS.buttons == JOYSTICK_OFF_BUTTONS &&
		    inControl == dgcs_JoystickDriver) {
	    inControl = oldInControl;
	  }
	}
	break;
	
      case dgcm_DriverTakeoverPacket:
	if( dgc_GetMessage(myMH, &takeover) == TRUE ) {
	  oldInControl = inControl;
	  inControl = takeover;
	}
	break;
      case dgcm_DriverReleasePacket:
	if( dgc_RemoveMessage(myMH) == TRUE) {
	  inControl = oldInControl;
	}
	break;
      default:
	cout << "Something is wrong, " << __FILE__ << "(" << __LINE__ << ")" << endl;
      }
    }


    // if Global is in charge, and we have not requested state in a while, send a message
    if(inControl == dgcs_GlobalRoutePlanner) {
      gettimeofday(&after, NULL);
      if( timeval_subtract(after, before) >= StateRefreshRate ) {
	before = after;
	myMH.Destination = 0;
	myMH.DestinationType = dgcs_StateDriver;
	myMH.MsgType = dgcm_RequestState;
	myMH.MsgSize = 0;
	dgc_SendMessage(myMH, NULL);
      }
    }

  }
  cout << endl << endl << "Unregistering" << endl;
  dgc_msg_Unregister();

  return 0;
}


int DriveCommandPacket(dgc_MsgHeader & myMH, steer_struct & mySS) {
  if( inControl == myMH.SourceType ) {
    // Forward the gas/brake pedal portion to the gas/brake driver
    myMH.Destination = 0;
    myMH.DestinationType = dgcs_Jeremy;
    myMH.MsgType = dgcm_AccelStatePacket;
    myMH.MsgSize = sizeof(float);
    dgc_SendMessage(myMH, & mySS.as.accel);
    
    // Forward the steering portion to the steering driver 
    myMH.Destination = 0;
    myMH.DestinationType = dgcs_SteeringDriver;
    myMH.MsgType = dgcm_SteeringStatePacket;
    myMH.MsgSize = sizeof(float);
    dgc_SendMessage(myMH, & mySS.as.steering);	    
    
    cout << "Msg Received: x = " << mySS.as.steering << endl;
    cout << "              y = " << mySS.as.accel    << endl;
    cout << "        buttons = " << mySS.buttons     << endl;
    cout << endl;
  }
  return TRUE;
}












