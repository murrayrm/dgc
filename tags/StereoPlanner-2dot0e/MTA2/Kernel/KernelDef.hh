#ifndef KernelDef_HH
#define KernelDef_HH




namespace DEF {

  // constants for socket server and transactions
  const int MinPort  = 13000;
  const int NumPorts = 5;
  const int ReceiveKey = 135324; // just a random number

  // constants for block size of buffers
  const int BlockSize = 1000; // 1kb

};






#endif
