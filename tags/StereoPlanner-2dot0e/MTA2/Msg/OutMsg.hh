#ifndef OutMsg_HH
#define OutMsg_HH

#include "Misc/Mod/Mod.hh"

class OutMsg {
public:
  // OutMsg may only be constructed if given the address of the destination
  OutMsg(const ModAddress & ma);
  ~OutMsg();

  int  BlockSend(Timeval timeout);    // returns negative if send failed
  void NonBlockSend(); // detaches and sends from separate thread

  OutMsg& operator << (const int& RHS);
  OutMsg& operator << (const unsigned int& RHS);
  OutMsg& operator << (const short& RHS);
  OutMsg& operator << (const long& RHS);
  OutMsg& operator << (const char& RHS);
  OutMsg& operator << (const float& RHS);
  OutMsg& operator << (const double& RHS);
  OutMsg& operator << (const Memory& RHS);

private:

  int msgSize;
  ModAddress destination;
  BufferedPipe bpData;

};

#endif
