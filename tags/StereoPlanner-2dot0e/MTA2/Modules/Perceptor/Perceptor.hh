#ifndef Perceptor_HH
#define Perceptor_HH

#include "DGC_MODULE.hh"
#include "PerceptorDatum.hh"


typedef DGC_MODULE Perceptor;

using namespace std;
using namespace boost;

// state machine states enumerated
// defined in DGC_MODULE.hh

// namespace STATES {
//   enum { INIT, ACTIVE, STANDBY, SHUTDOWN, RESTART, NUM_STATES };
// };

// enumerate all module messages that could be received
namespace PerceptorMessages {
  enum { // in Messages first (messages taken in without responding
         REINIT,   // something where we would invoke reinit state
	 SHUTDOWN, // maybe another process wants us to shutdown
	 PAUSE,    // go to standby state
	 GO,       // go to active state
	 
	 // now query messages (messages we receive that we respond to)
	 GETSTATE,   // gets what state we are in (i.e. init, active, ...)
	
	 // And a place holder
	 NumMessages
  };
};



#endif
