//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// CR_Movie
//----------------------------------------------------------------------------

#ifndef CR_MOVIE_DECS

//----------------------------------------------------------------------------

#define CR_MOVIE_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <io.h>

// Windows

#define _WINSOCKAPI_
#include <windows.h>
#include <ddraw.h>

// AVI reading/writing

#include <vfw.h>

// my stuff 

#include "CR_Memory.hh"
#include "CR_Error.hh"
//#include "CR_Window.hh"
//#include "CR_Image.hh"
//#include "CR_Demo3.hh"
//#include "CR_IplImage.hh"
#include "CR_Image.hh"
#include "CR_Win32.hh"
//#include "CR_GPS.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef streamtypeDV
#define streamtypeDV           1937138025
#endif

#define CR_MAX_FILENAME        256

#define CR_UNKNOWN             0
#define CR_MOVIE_AVI           2

//-------------------------------------------------
// structure
//-------------------------------------------------

typedef struct crmovie
{
  char *name;
  int type;
  int width, height;
  //  IplImage *im;       // current image
  CR_Image *im;
  int numframes, currentframe, previousframe, firstframe, lastframe;
  int state;  // playing or stopped?

  int glut_id;        // so we know what window this is playing in, if any

  // AVI stuff

  PAVIFILE AVI_pAviFile;
  PGETFRAME AVI_pgf;
  int interleaved_DV;
  PAVISTREAM AVI_pVideo, compressed_AVI_pVideo;  // for writing
  LPBITMAPINFOHEADER pBmpInfoHeader;             // for writing
  int need_filename;                             // for writing
  
} CR_Movie;

//-------------------------------------------------
// functions
//-------------------------------------------------

CR_Movie *read_ppm_to_CR_Movie(char *);
void get_ppm_frame_CR_Movie(CR_Movie *, int);

CR_Movie *write_avi_initialize_CR_Movie(char *, int, int, int);
void write_avi_addframe_CR_Movie(int, CR_Image *, CR_Movie *);
void write_avi_finish_CR_Movie(CR_Movie *);

CR_Movie *open_CR_Movie(char *);
void close_CR_Movie(CR_Movie *);
int determine_type_CR_Movie(char *);

char *construct_n_digit_filename(int, int, char *, char *);
char *construct_3_digit_filename(int, char *, char *);
void get_frame_CR_Movie(CR_Movie *, int);

// AVI

CR_Movie *read_avi_to_CR_Movie(char *);
void get_avi_frame_CR_Movie(CR_Movie *, int);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif


