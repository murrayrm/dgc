//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_KalmanFilter.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// B = copy_CR_Matrix(A)
// print_CR_Matrix(A)
// free_CR_Matrix(A)
// B = transpose_CR_Matrix(A)
// B = scale_CR_Matrix(a, A)
// free_CR_Matrix_array(A_array, n) 
// B = invert_CR_Matrix(A) ==> B = inverse_CR_Matrix(A)
// ip_transpose_CR_Matrix(A, B) ==> transpose_CR_Matrix(A, B)
// A = make_CR_Matrix(r, c) ==> A = make_CR_Matrix(name, r, c)
// B = make_x_CR_Matrix(x, r, c) ==> make_all_CR_Matrix(name, r, c, x)
// C = multiply_CR_Matrix(A, B) ==> C = product_CR_Matrix(A, B)
// D = multiply3_CR_Matrix(A, B, C) ==> D = product3_CR_Matrix(A, B, C)
// C = add_CR_Matrix(A, B) ==> C = sum_CR_Matrix(A, B)
// D = add3_CR_Matrix(A, B, C) ==> D = sum3_CR_Matrix(A, B, C)
// C = subtract_CR_Matrix(A, B) ==> C = difference_CR_Matrix(A, B)
// B = sum_CR_Matrixs(A_array, n) ==> B = sum_CR_Matrix_array(A_array, n)
// C = evaluate_CRFuncMat_to_CR_Matrix(FuncA, B) ==> C = evaluate_CR_FuncMatrix_to_CR_Matrix(FuncA, B)
// ip_evaluate_CRFuncMat_to_CR_Matrix(FuncA, B, C) ==> C = evaluate_CR_FuncMatrix_to_CR_Matrix(FuncA, B, C)

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// These formulas were taken from "Tracking and Data Association,"
// by Y. Bar-Shalom and T. Fortmann, Academic Press, 1988

// Numbers in parentheses, e.g. "(6-27)," refer to formulas/equations in
// that book

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/*
CR_Kalman *load_CR_Kalman(char *filename)
{
  CR_FuncMatrix *F, *X, *H, *Q, *R, *X0, *P0, *Input;
  FILE *fp;
 
  fp = fopen(filename, "r");

  X = parse_CR_FuncMatrix(fp);
  X0 = parse_CR_FuncMatrix(fp);
  F = parse_CR_FuncMatrix(fp);
  Input = parse_CR_FuncMatrix(fp);
  H = parse_CR_FuncMatrix(fp);
  R = parse_CR_FuncMatrix(fp);
  Q = parse_CR_FuncMatrix(fp);
  P0 = parse_CR_FuncMatrix(fp);

  fclose(fp);

  //  return make_nonlinear_CR_Kalman(X, F, H, Q, R, X0, P0, NULL);
  return make_nonlinear_CR_Kalman(X, F, H, Q, R, X0, P0, Input);
}

//----------------------------------------------------------------------------

// assuming not PDAF

CR_Kalman *copy_CR_Kalman(CR_Kalman *k)
{
  CR_Kalman *k_copy;

  k_copy = (CR_Kalman *) CR_malloc(sizeof(CR_Kalman));

  k_copy->type = k->type;       
  k_copy->nonlinear = k->nonlinear;  
  k_copy->funcmat = k->funcmat;    
  k_copy->iterations = k->iterations; 
  k_copy->updates = 0;

  k_copy->F_nonlinear = k->F_nonlinear;
  k_copy->H_nonlinear = k->H_nonlinear;

  k_copy->F = copy_CR_Matrix(k->F);
  k_copy->H = copy_CR_Matrix(k->H);
  k_copy->FT = copy_CR_Matrix(k->FT);
  k_copy->HT = copy_CR_Matrix(k->HT);
  k_copy->Q = copy_CR_Matrix(k->Q);
  k_copy->R = copy_CR_Matrix(k->R);
  k_copy->P = copy_CR_Matrix(k->P);
  k_copy->P_pred = copy_CR_Matrix(k->P_pred);
  k_copy->P_last = copy_CR_Matrix(k->P_last);
  k_copy->W = copy_CR_Matrix(k->W);
  k_copy->S = copy_CR_Matrix(k->S);
  k_copy->Sinv = copy_CR_Matrix(k->Sinv);
  k_copy->v = copy_CR_Matrix(k->v);
  k_copy->z = copy_CR_Matrix(k->z);
  k_copy->z_pred = copy_CR_Matrix(k->z_pred);
  k_copy->x = copy_CR_Matrix(k->x);
  k_copy->x_pred = copy_CR_Matrix(k->x_pred);
  k_copy->x_last = copy_CR_Matrix(k->x_last);
  k_copy->Input = copy_CR_Matrix(k->Input);

  k_copy->F_X = copy_CR_FuncMatrix(k->F_X);  
  k_copy->F_F = copy_CR_FuncMatrix(k->F_F);  
  k_copy->F_H = copy_CR_FuncMatrix(k->F_H);  
  k_copy->Jac_F_F = copy_CR_FuncMatrix(k->Jac_F_F); 
  k_copy->Jac_F_H = copy_CR_FuncMatrix(k->Jac_F_H); 

  return k_copy;
}

//----------------------------------------------------------------------------

// assuming not PDAF

void free_CR_Kalman(CR_Kalman *k)
{
  free_CR_Matrix(k->F);
  free_CR_Matrix(k->H);
  free_CR_Matrix(k->FT);
  free_CR_Matrix(k->HT);
  free_CR_Matrix(k->Q);
  free_CR_Matrix(k->R);
  free_CR_Matrix(k->P);
  free_CR_Matrix(k->P_pred);
  free_CR_Matrix(k->P_last);
  free_CR_Matrix(k->W);
  free_CR_Matrix(k->S);
  free_CR_Matrix(k->Sinv);
  free_CR_Matrix(k->v);
  free_CR_Matrix(k->z);
  free_CR_Matrix(k->z_pred);
  free_CR_Matrix(k->x);
  free_CR_Matrix(k->x_pred);
  free_CR_Matrix(k->x_last);
  free_CR_Matrix(k->Input);

  free_CR_FuncMatrix(k->F_X);  
  free_CR_FuncMatrix(k->F_F);  
  free_CR_FuncMatrix(k->F_H);  
  free_CR_FuncMatrix(k->Jac_F_F); 
  free_CR_FuncMatrix(k->Jac_F_H); 

  CR_free_malloc(k, sizeof(CR_Kalman));
}

//----------------------------------------------------------------------------

// automatically determines whether to use Extended Kalman Filter
// or basic version -- should replace make_CR_Kalman()
// (except that it expects CR_FuncMatrices instead of CR_Matrix -- that has
// to be taken into account)

// also wants X -- a listing of the state variables

CR_Kalman *make_nonlinear_CR_Kalman(CR_FuncMatrix *stateX, CR_FuncMatrix *F, CR_FuncMatrix *H, CR_FuncMatrix *Q, CR_FuncMatrix *R, CR_FuncMatrix *X0, CR_FuncMatrix *P0, CR_FuncMatrix *Input)
{
  CR_Kalman *k;

  // Input is optional 

  if (!stateX || !F || !H || !Q || !R || !X0 || !P0)
    CR_error("make_nonlinear_CR_Kalman(): at least one of the necessary matrices is NULL\n");

  k = (CR_Kalman *) CR_malloc(sizeof(CR_Kalman));

  k->type = KALMAN;
  k->funcmat = TRUE;    // we're doing things the new way here

  if (!is_state_vector_CR_FuncMatrix(stateX))
    CR_error("make_nonlinear_CR_Kalman(): state vector not of the proper form");
  if (stateX->rows != X0->rows || stateX->cols != X0->cols)
    CR_error("make_nonlinear_CR_Kalman(): state vector / initial state values vector size mismatch");

  //  k->F_X = stateX;      // state variables

  // decide whether we have nonlinearities in dynamics or measurement

  k->F_nonlinear = !is_linear_CR_FuncMatrix(F);     // are dynamics nonlinear?
  k->H_nonlinear = !is_linear_CR_FuncMatrix(H);     // is measurement nonlinear?

  if (k->F_nonlinear || k->H_nonlinear) {
    k->nonlinear = TRUE;
    k->iterations = 1;                          // this should be a variable
  }
  else
    k->nonlinear = FALSE;

  // if dynamics are nonlinear, then linearize them

  if (k->F_nonlinear) {
    rectify_vartables_CR_FuncMatrix(stateX, F);
    k->Jac_F_F = Jacobian_CR_FuncMatrix(F);     // this assures that F is a column vector
    k->F = make_empty_CR_Matrix_from_CR_FuncMatrix(k->Jac_F_F);
    k->FT = make_CR_Matrix("FT", k->F->cols, k->F->rows);
  }
  else {
    k->F = copy_CR_Matrix_from_linear_CR_FuncMatrix(F);
    k->FT = transpose_CR_Matrix(k->F);
  }

  // if measurement process is nonlinear, then linearize it

  if (k->H_nonlinear) {
    
    rectify_vartables_CR_FuncMatrix(stateX, H);
    k->Jac_F_H = Jacobian_CR_FuncMatrix(H);     
    k->H = make_empty_CR_Matrix_from_CR_FuncMatrix(k->Jac_F_H);
    k->HT = make_CR_Matrix("HT", k->H->cols, k->H->rows);
  }
  else {
    k->H = copy_CR_Matrix_from_linear_CR_FuncMatrix(H);
    k->HT = transpose_CR_Matrix(k->H);
  }

  // copy various user-supplied matrices into tracker

  // state variables

  k->F_X = stateX;      

  // dynamics, measurement process

  k->F_F = F;
  k->F_H = H;

  //  dynamics, measurement covariances

  k->Q = copy_CR_Matrix_from_linear_CR_FuncMatrix(Q);
  k->R = copy_CR_Matrix_from_linear_CR_FuncMatrix(R);

  // initial state, initial state covariance

  k->x_last = copy_CR_Matrix_from_linear_CR_FuncMatrix(X0);
  k->P_last = copy_CR_Matrix_from_linear_CR_FuncMatrix(P0);

  // deal with Input

  if (!Input)
    k->Input = NULL;
  else {
    k->Input = copy_CR_Matrix_from_linear_CR_FuncMatrix(Input);
  }

  // finish up

  finish_make_CR_Kalman(k);

  return k;
}

//----------------------------------------------------------------------------

CR_Kalman *make_nonlinear_pdaf_CR_Kalman(CR_FuncMatrix *stateX, CR_FuncMatrix *F, CR_FuncMatrix *H, CR_FuncMatrix *Q, CR_FuncMatrix *R, CR_FuncMatrix *X0, CR_FuncMatrix *P0, float gamma, float pd, float pg, float survol, float lambda)
{
  CR_Kalman *k;

  // make nonlinear plain Kalman part of filter

  k = make_nonlinear_CR_Kalman(stateX, F, H, Q, R, X0, P0, NULL);

  // finish up

  finish_make_pdaf_CR_Kalman(k, gamma, pd, pg, survol, lambda);

  return k;
}

//----------------------------------------------------------------------------

// set PDAF-specific variables

void finish_make_pdaf_CR_Kalman(CR_Kalman *k, float gamma, float pd, float pg, float survol, float lambda)
{
  k->type = PDAF;

  if (pd == 0) 
    CR_error("finish_make_pdaf_CR_Kalman(): detection probability must be non-zero");

  k->P_c = NULL;
  k->P_tilde = NULL;

  k->gamma = gamma;
  k->pd = pd;
  k->pg = pg;
  k->survol = survol;
  k->lambda = lambda;

  k->z_arr = NULL;
  k->n = 0;
  k->last_n = 0;
  k->beta = NULL;
  k->e = NULL;
  k->b = k->e_sum = k->beta_sum = 0;

  k->innov = NULL;
  k->innovT = NULL;

  k->z_dim = k->z_pred->rows;

  k->hypervol = CR_hypersphere_volume(1, k->z_dim);
}
*/

//----------------------------------------------------------------------------

// Kalman Filter equations:

/*
  k->z = z;

  k->x_pred = k->F * k->x_last + k->Input;         // one-step state prediction
  k->P_pred = k->F * k->P_last * k->F + k->Q;      // one-step covariance prediction

  k->S = k->H * k->P_pred * k->H + k->R;           // measurement prediction covariance
  k->W = k->P_pred * k->H * (1 / k->S);            // filter gain
  k->v = k->z - k->H * k->x_pred;                  // innovation

  k->x = k->x_pred + k->W * k->v;                  // state estimate
  k->P = (1.0 - k->W * k->H) * k->P_pred;          // covariance estimate

  k->x_last = k->x;
  k->P_last = k->P;
*/

CR_KalmanFilter *make_CR_KalmanFilter(CR_Matrix *stateX, CR_Matrix *F, CR_Matrix *H, CR_Matrix *Q, CR_Matrix *R, CR_Matrix *X0, CR_Matrix *P0, CR_Matrix *Input)
{
  CR_KalmanFilter *k;

  // Input is optional 

  if (!stateX || !F || !H || !Q || !R || !X0 || !P0)
    CR_error("make_CR_Kalman(): at least one of the necessary matrices is NULL\n");

  k = (CR_KalmanFilter *) CR_malloc(sizeof(CR_KalmanFilter));

  if (stateX->rows != X0->rows || stateX->cols != X0->cols)
    CR_error("make_CR_Kalman(): state vector / initial state values vector size mismatch");

  k->F = copy_CR_Matrix(F);
  k->FT = transpose_CR_Matrix(k->F);

  k->H = copy_CR_Matrix(H);
  k->HT = transpose_CR_Matrix(k->H);

  //  dynamics, measurement covariances

  k->Q = copy_CR_Matrix(Q);
  k->R = copy_CR_Matrix(R);

  // initial state, initial state covariance

  k->x_last = copy_CR_Matrix(X0);
  k->P_last = copy_CR_Matrix(P0);

  // deal with Input

  if (!Input)
    k->Input = NULL;
  else {
    k->Input = copy_CR_Matrix(Input);
  }

  // finish up

  finish_make_CR_KalmanFilter(k);

  return k;
}

//----------------------------------------------------------------------------

void finish_make_CR_KalmanFilter(CR_KalmanFilter *k)
{
  CR_Matrix *temp;

  k->x = NULL;
  k->z = NULL;
  k->P = NULL;
  k->v = NULL;

  k->x_pred = product_CR_Matrix(k->F, k->x_last);             // one-step state prediction

  temp = product3_CR_Matrix(k->F, k->P_last, k->FT);
  k->P_pred = sum_CR_Matrix(temp, k->Q);                     // one-step cov prediction
  free_CR_Matrix(temp);

  k->z_pred = product_CR_Matrix(k->H, k->x_pred);             // measurement prediction

  temp = product3_CR_Matrix(k->H, k->P_pred, k->HT);
  k->S = sum_CR_Matrix(temp, k->R);                      // measurement prediction cov
  free_CR_Matrix(temp);
    
  k->Sinv = inverse_CR_Matrix(k->S);                      // information matrix

  k->W = product3_CR_Matrix(k->P_pred, k->HT, k->Sinv);    // filter gain 
}

//----------------------------------------------------------------------------

// deletes measurement z after use

void update_CR_KalmanFilter(CR_Matrix *z, CR_KalmanFilter *k)
{
  CR_Matrix *temp, *temp2;

  //  printf("update_CR_Kalman()\n");

  //  k->updates++;

  //----------------------------------------------------
  // record measurement
  //----------------------------------------------------
  
  if (k->z)
    free_CR_Matrix(k->z);
  if (!z)
    CR_error("update_CR_Kalman(): no measurement -- and this isn't PDAF");
  k->z = copy_CR_Matrix(z);
  free_CR_Matrix(z);
  
  //---- everything below here is being redone in the first call to update

  //----------------------------------------------------
  // one-step state prediction
  //----------------------------------------------------

  //  printf("x input 1\n");

  free_CR_Matrix(k->x_pred);
  if (!k->Input) 
    k->x_pred = product_CR_Matrix(k->F, k->x_last);               
  else {     // creation, update, and freeing of Input is assumed to take place elsewhere
    temp = product_CR_Matrix(k->F, k->x_last);
    k->x_pred = sum_CR_Matrix(temp, k->Input);
    free_CR_Matrix(temp);
    printf("input 1\n");
  }

  //----------------------------------------------------
  // one-step cov prediction
  //----------------------------------------------------
  
  temp = product3_CR_Matrix(k->F, k->P_last, k->FT);
  free_CR_Matrix(k->P_pred);
  k->P_pred = sum_CR_Matrix(temp, k->Q);                     
  free_CR_Matrix(temp);

  //----------------------------------------------------
  // measurement prediction
  //----------------------------------------------------
  
  free_CR_Matrix(k->z_pred);
  k->z_pred = product_CR_Matrix(k->H, k->x_pred);               

  //----------------------------------------------------
  // measurement prediction cov
  //----------------------------------------------------
 
  temp = product3_CR_Matrix(k->H, k->P_pred, k->HT);
  free_CR_Matrix(k->S);
  k->S = sum_CR_Matrix(temp, k->R);                      
  free_CR_Matrix(temp);

  //----------------------------------------------------
  // information matrix
  //----------------------------------------------------
  
  free_CR_Matrix(k->Sinv);
  k->Sinv = inverse_CR_Matrix(k->S);                      

  //----------------------------------------------------
  // filter gain 
  //----------------------------------------------------
  
  free_CR_Matrix(k->W);
  k->W = product3_CR_Matrix(k->P_pred, k->HT, k->Sinv);    
  
  //---- everything above here is being redone in the first call to update
 
  //----------------------------------------------------
  // innovation
  //----------------------------------------------------
 
  if (k->v)
    free_CR_Matrix(k->v);
  k->v = difference_CR_Matrix(k->z, k->z_pred);                
 
  //----------------------------------------------------
  // state estimate
  //----------------------------------------------------

  temp = product_CR_Matrix(k->W, k->v);
  if (k->x)
    free_CR_Matrix(k->x);
  k->x = sum_CR_Matrix(k->x_pred, temp);                     
  free_CR_Matrix(temp);

  //----------------------------------------------------
  // cov estimate
  //----------------------------------------------------

  temp = product3_CR_Matrix(k->W, k->H, k->P_pred);
  if (k->P)
    free_CR_Matrix(k->P);
  k->P = difference_CR_Matrix(k->P_pred, temp);                
  free_CR_Matrix(temp);
  
  //----------------------------------------------------
  // make currents into lasts
  //----------------------------------------------------

  free_CR_Matrix(k->x_last);
  free_CR_Matrix(k->P_last);
  k->x_last = copy_CR_Matrix(k->x);
  k->P_last = copy_CR_Matrix(k->P);
}

//----------------------------------------------------------------------------

/*
void compute_combined_innovation_pdaf_CR_Kalman(CR_Kalman *k)
{
  int i;
  CR_Matrix **temp_arr;
  CR_Matrix *tempmat;

  //----------------------------------------------------
  // PDAF: Calculate probabilities of association between 
  // measurements and target beta_i(t+1) (6-42, 6-43)
  //----------------------------------------------------

  if (k->beta)
    CR_free_calloc(k->beta, k->last_n + 1, sizeof(float));
  k->beta = (float *) CR_calloc(k->n + 1, sizeof(float));

  //----------------------------------------------------

  if (k->type == PDAF) {

    if (k->e)
      CR_free_calloc(k->e, k->last_n + 1, sizeof(float));
    k->e = (float *) CR_calloc(k->n + 1, sizeof(float));
    
    if (k->n)  // if we have at least one measurement 
      k->b = (float) k->n * pow(2 * PI / k->gamma, k->z_dim / 2) * k->hypervol * (1 - k->pd * k->pg) / k->pd;
    else       // this is to make beta[0] = 1 when there are no measurements
      k->b = k->lambda * k->survol * pow(2 * PI / k->gamma, k->z_dim / 2) * k->hypervol * (1 - k->pd * k->pg) / k->pd;

    for (i = 1, k->e_sum = 0; i < k->n + 1; i++) {
      k->e[i] = exp(-0.5 * CR_Kalman_likelihood(k->z_arr[i - 1], k));

      k->e_sum += k->e[i];
    }
    
    k->beta[0] = k->b / (k->b + k->e_sum);

    for (i = 1; i < k->n + 1; i++) 
      k->beta[i] = k->e[i] / (k->b + k->e_sum);
      
    for (i = 0, k->beta_sum = 0; i < k->n + 1; i++)  
      k->beta_sum += k->beta[i];
  }

  //----------------------------------------------------

  else 
    CR_error("compute_combined_innovation_pdaf_CR_Kalman(): unsupported type");

  //----------------------------------------------------
  // PDAF: individual innovations v_i(t+1) (6-23)
  //----------------------------------------------------

  if (k->last_n) {
    if (k->innov)
      free_CR_Matrix_array(k->innov, k->last_n);
    if (k->innovT)
      free_CR_Matrix_array(k->innovT, k->last_n);
  }

  if (k->n) {
    
    k->innov = make_CR_Matrix_array(k->n);
    k->innovT = make_CR_Matrix_array(k->n);
    
    temp_arr = make_CR_Matrix_array(k->n);
    
    for (i = 1; i < k->n + 1; i++) {
      k->innov[i - 1] = difference_CR_Matrix(k->z_arr[i - 1], k->z_pred);
      k->innovT[i - 1] = transpose_CR_Matrix(k->innov[i - 1]);
      temp_arr[i - 1] = scale_CR_Matrix(k->beta[i], k->innov[i - 1]) ;
    }
  }

  //----------------------------------------------------
  // PDAF: combined innovation v(t) (6-26)
  // (replaces innovation (2-228))
  //----------------------------------------------------

  if (k->v)
    free_CR_Matrix(k->v);

  if (k->n)
    k->v = sum_CR_Matrix_array(temp_arr, k->n);   // this frees temp_arr 
  else 
    k->v = make_all_CR_Matrix("v", k->z_pred->rows, k->z_pred->cols, 0);
}

//----------------------------------------------------------------------------

void compute_measurement_covariance_pdaf_CR_Kalman(CR_Kalman *k)
{
  int i;
  CR_Matrix *temp, *temp2, *temp3;
  CR_Matrix **temp_arr;

  //----------------------------------------------------
  // PDAF: correct measurement cov estimate  
  // P_c(t+1|t+1)  (2-231)
  // (same as Kalman cov estimate (2-231))
  //----------------------------------------------------
  
  temp = product3_CR_Matrix(k->W, k->H, k->P_pred);
  if (k->P_c)
    free_CR_Matrix(k->P_c);
  k->P_c = difference_CR_Matrix(k->P_pred, temp);                
  free_CR_Matrix(temp);

  //----------------------------------------------------
  // PDAF: P_tilde P~(t+1) (6-28)
  //----------------------------------------------------

  if (k->n) {
    temp_arr = make_CR_Matrix_array(k->n);
    temp = transpose_CR_Matrix(k->v);
    temp3 = product_CR_Matrix(k->v, temp);
    free_CR_Matrix(temp);
    
    for (i = 1; i < k->n + 1; i++) {
      temp = product_CR_Matrix(k->innov[i - 1], k->innovT[i - 1]);
      temp_arr[i - 1] = scale_CR_Matrix(k->beta[i], temp);
      free_CR_Matrix(temp);
    }
    temp = sum_CR_Matrix_array(temp_arr, k->n);
    temp2 = difference_CR_Matrix(temp, temp3);   
    free_CR_Matrix(temp);                      
    free_CR_Matrix(temp3);                     
    temp3 = transpose_CR_Matrix(k->W);
    if (k->P_tilde)
      free_CR_Matrix(k->P_tilde);
    k->P_tilde = product3_CR_Matrix(k->W, temp2, temp3);
    free_CR_Matrix(temp2);
    free_CR_Matrix(temp3);
  }
  else {
    if (k->P_tilde)
      free_CR_Matrix(k->P_tilde);
    k->P_tilde = make_all_CR_Matrix("P_tilde", k->P_last->rows, k->P_last->cols, 0);
  }
  
  //----------------------------------------------------
  // PDAF: cov estimate P(t+1|t+1) (6-27)
  // (replaces Kalman cov estimate (2-231))
  //----------------------------------------------------
  
  temp = scale_CR_Matrix(k->beta[0], k->P_pred);
  temp2 = scale_CR_Matrix(1 - k->beta[0], k->P_c);
  if (k->P)
    free_CR_Matrix(k->P);
  k->P = sum3_CR_Matrix(temp, temp2, k->P_tilde);       
  free_CR_Matrix(temp);
  free_CR_Matrix(temp2);
}

//----------------------------------------------------------------------------

// the do-it-all update() function -- handles regular Kalman and PDAF,
// both linear and nonlinear.  and stays sharp, too!

// replaces update_nnsf_CR_Kalman() and update_pdaf_CR_Kalman()

void update_nonlinear_CR_Kalman(CR_Matrix **z_array, int n, CR_Kalman *k)
{
  int i;
  CR_Matrix *temp, *temp2;
  CR_Matrix *z;
  CR_Matrix **newz_array, **tempz_array;

  k->updates++;

  //----------------------------------------------------
  // Record measurements (NN and PDAF)
  //----------------------------------------------------

  // Kalman-style

  if (k->type == KALMAN) {

    z = CR_Kalman_nearest_neighbor(z_array, n, k);

    if (k->z)
      free_CR_Matrix(k->z);
    if (!z)
      CR_error("update_nonlinear_CR_Kalman(): no measurement -- and this isn't PDAF");
    k->z = copy_CR_Matrix(z);
    free_CR_Matrix(z);
  }

  // PDAF-style

  else if (k->type == PDAF) {

    free_CR_Matrix_array(k->z_arr, k->n);

    k->n = n;
    if (k->n) 
      k->z_arr = make_CR_Matrix_array(n);
    else
      k->z_arr = NULL;
    for (i = 0; i < k->n; i++)
      k->z_arr[i] = copy_CR_Matrix(z_array[i]);

  }
  else
    CR_error("update_nonlinear_CR_Kalman(): not yet ready for non-KALMAN, non-PDAF");

  //---- everything below here is being redone in the first call to update

  //----------------------------------------------------
  // one-step state prediction  x(t+1|t)  (2-219)
  // measurement prediction     z(t+1|t)  (2-222)
  //----------------------------------------------------

  // if nonlinear, linearize

  if (k->nonlinear) {

    // state prediction

    free_CR_Matrix(k->x_pred);

    if (k->F_nonlinear) {
      k->x_pred = evaluate_CR_FuncMatrix_to_CR_Matrix(k->F_F, k->x_last);        // one-step state prediction (not in place) = k->F_F(k->x_last)
      // add Input here
      if (k->Input)
	sum_in_place_CR_Matrix(k->x_pred, k->Input);

      evaluate_CR_FuncMatrix_to_CR_Matrix(k->Jac_F_F, k->x_last, k->F);   // compute F (in place) = k->Jac_F_F(k->x_last)
      transpose_CR_Matrix(k->F, k->FT);                          // compute FT (in place)
    }
    else 
      k->x_pred = product_CR_Matrix(k->F, k->x_last);             

    // measurement prediction

    free_CR_Matrix(k->z_pred);

    if (k->H_nonlinear) {
       k->z_pred = evaluate_CR_FuncMatrix_to_CR_Matrix(k->F_H, k->x_pred);        // measurement prediction (not in place) = k->F_H(k->x_pred)

      evaluate_CR_FuncMatrix_to_CR_Matrix(k->Jac_F_H, k->x_pred, k->H);   // compute H (in place) = k->Jac_F_H(k->x_pred)
      transpose_CR_Matrix(k->H, k->HT);                          // compute HT (in place)
    }
    else 
      k->z_pred = product_CR_Matrix(k->H, k->x_pred);             
  }

  // linear, so proceed normally

  else {

    printf("x input 2\n");

    // state prediction

    free_CR_Matrix(k->x_pred);
    if (!k->Input) 
      k->x_pred = product_CR_Matrix(k->F, k->x_last);               
    else {     // creation, update, and freeing of Input is assumed to take place elsewhere
      temp = product_CR_Matrix(k->F, k->x_last);
      k->x_pred = sum_CR_Matrix(temp, k->Input);
      free_CR_Matrix(temp);
      printf("input 2\n");
    }

    // measurement prediction

    free_CR_Matrix(k->z_pred);
    k->z_pred = product_CR_Matrix(k->H, k->x_pred);               

  }

  //----------------------------------------------------
  // one-step cov prediction  P(t+1|t)   (2-221)
  //----------------------------------------------------
  
  temp = product3_CR_Matrix(k->F, k->P_last, k->FT);
  free_CR_Matrix(k->P_pred);
  k->P_pred = sum_CR_Matrix(temp, k->Q);                     
  free_CR_Matrix(temp);

  //----------------------------------------------------
  // measurement prediction cov  z(t+1|t)  (2-222)
  //----------------------------------------------------
 
  temp = product3_CR_Matrix(k->H, k->P_pred, k->HT);
  free_CR_Matrix(k->S);
  k->S = sum_CR_Matrix(temp, k->R);                      
  free_CR_Matrix(temp);

  //----------------------------------------------------
  // information matrix  S^-1(t+1)
  //----------------------------------------------------
  
  free_CR_Matrix(k->Sinv);
  k->Sinv = inverse_CR_Matrix(k->S);                      

  //----------------------------------------------------
  // filter gain   W(k+1)  (2-226)
  //----------------------------------------------------
  
  free_CR_Matrix(k->W);
  k->W = product3_CR_Matrix(k->P_pred, k->HT, k->Sinv);    
  
  //---- everything above here is being redone in the first call to update
 
  //----------------------------------------------------
  // Innovation (NN and PDAF)
  //----------------------------------------------------
 
  // Kalman-style

  if (k->type == KALMAN) {
    if (k->v)
      free_CR_Matrix(k->v);
    k->v = difference_CR_Matrix(k->z, k->z_pred);                 
  }

  // PDAF-style

  else if (k->type == PDAF) 
    compute_combined_innovation_pdaf_CR_Kalman(k);

  else
    CR_error("update_nonlinear_CR_Kalman(): not yet ready for non-KALMAN, non-PDAF");

  //----------------------------------------------------
  // state estimate  x(t+1|t+1) (2-227, 6-25)
  //----------------------------------------------------

  temp = product_CR_Matrix(k->W, k->v);
  if (k->x)
    free_CR_Matrix(k->x);
  k->x = sum_CR_Matrix(k->x_pred, temp);                     
  free_CR_Matrix(temp);

  //----------------------------------------------------
  // cov estimate  P(t+1|t+1) (2-231, 6-27)
  //----------------------------------------------------

  // Kalman-style

  if (k->type == KALMAN) {

    temp = product3_CR_Matrix(k->W, k->H, k->P_pred);
    if (k->P)
      free_CR_Matrix(k->P);
    k->P = difference_CR_Matrix(k->P_pred, temp);                
    free_CR_Matrix(temp);

  }

  // PDAF-style

  else if (k->type == PDAF) 
    compute_measurement_covariance_pdaf_CR_Kalman(k);

  else
    CR_error("update_nonlinear_CR_Kalman(): not yet ready for non-KALMAN, non-PDAF");

  //----------------------------------------------------
  // clean up
  //----------------------------------------------------

  if (k->type == PDAF) {  
    if (k->n) 
      free_CR_Matrix_array(z_array, n);
  }

  //----------------------------------------------------
  // make currents into lasts
  //----------------------------------------------------

  k->last_n = k->n;

  free_CR_Matrix(k->x_last);
  free_CR_Matrix(k->P_last);
  k->x_last = copy_CR_Matrix(k->x);
  k->P_last = copy_CR_Matrix(k->P);
}

//----------------------------------------------------------------------------

// non-parametric probabilistic data association filter
// z is measurements in validation gate

// remember that e[i], beta[i] correspond to z[i - 1]
// => e[0] doesn't exist and beta[0] is not associated with z

// what if n = 0?

void update_pdaf_CR_Kalman(CR_Matrix **z, int n, CR_Kalman *k)
{
  int i;
  CR_Matrix *temp, *temp2, *temp3;
  CR_Matrix **v, **v_t, **temp_arr;

  k->updates++;

  printf("update pdaf kalman\n");

  //----------------------------------------------------
  // PDAF: record measurements
  //----------------------------------------------------
  
  free_CR_Matrix_array(k->z_arr, k->n);

  k->n = n;
  if (k->n) 
    k->z_arr = make_CR_Matrix_array(n);
  else
    k->z_arr = NULL;
  for (i = 0; i < k->n; i++)
    k->z_arr[i] = copy_CR_Matrix(z[i]);

  //----------------------------------------------------
  // one-step state prediction  x(t+1|t)  (2-219)
  //----------------------------------------------------

  printf("x input 3\n");

  free_CR_Matrix(k->x_pred);
  if (!k->Input)
    k->x_pred = product_CR_Matrix(k->F, k->x_last);               
  else {     // creation, update, and freeing of Input is assumed to take place elsewhere
    temp = product_CR_Matrix(k->F, k->x_last);
    k->x_pred = sum_CR_Matrix(temp, k->Input);
    free_CR_Matrix(temp);
    printf("input 3\n");
  }

  //----------------------------------------------------
  // one-step cov prediction  P(t+1|t)   (2-221)
  //----------------------------------------------------

  temp = product3_CR_Matrix(k->F, k->P_last, k->FT);
  free_CR_Matrix(k->P_pred);
  k->P_pred = sum_CR_Matrix(temp, k->Q);                     
  free_CR_Matrix(temp);

  //----------------------------------------------------
  // measurement prediction  z(t+1|t)  (2-222)
  //----------------------------------------------------

  free_CR_Matrix(k->z_pred);
  k->z_pred = product_CR_Matrix(k->H, k->x_pred);               

  //----------------------------------------------------
  // measurement prediction cov  S(t+1)  (2-224)
  //----------------------------------------------------
 
  temp = product3_CR_Matrix(k->H, k->P_pred, k->HT);
  free_CR_Matrix(k->S);
  k->S = sum_CR_Matrix(temp, k->R);                      
  free_CR_Matrix(temp);

  //----------------------------------------------------
  // information matrix  S^-1(t+1)
  //----------------------------------------------------

  free_CR_Matrix(k->Sinv);
  k->Sinv = inverse_CR_Matrix(k->S);                      

  //----------------------------------------------------
  // filter gain  W(k+1)  (2-226)
  //----------------------------------------------------
  
  free_CR_Matrix(k->W);
  k->W = product3_CR_Matrix(k->P_pred, k->HT, k->Sinv);    
  
  //---- everything above here is being redone in the first call to update
 
  //----------------------------------------------------
  // PDAF: Calculate probabilities of association between 
  // measurements and target beta_i(t+1) (6-42, 6-43)
  //----------------------------------------------------

  if (k->beta)
    //    free(k->beta);
    CR_free_calloc(k->beta, k->last_n + 1, sizeof(float));
  k->last_n = n;
  k->beta = (float *) CR_calloc(n + 1, sizeof(float));

  //----------------------------------------------------

  if (k->type == PDAF) {

    if (k->e)
      //      free(k->e);
      CR_free_calloc(k->e, k->last_n + 1, sizeof(float));
    k->e = (float *) CR_calloc(n + 1, sizeof(float));
    
    if (k->n)  // if we have at least one measurement 
      k->b = (float) n * pow(2 * PI / k->gamma, k->z_dim / 2) * k->hypervol * (1 - k->pd * k->pg) / k->pd;
    else       // this is to make beta[0] = 1 when there are no measurements
      k->b = k->lambda * k->survol * pow(2 * PI / k->gamma, k->z_dim / 2) * k->hypervol * (1 - k->pd * k->pg) / k->pd;

    for (i = 1, k->e_sum = 0; i < n + 1; i++) { 
      k->e[i] = exp(-0.5 * CR_Kalman_likelihood(z[i - 1], k));
      k->e_sum += k->e[i];
    }
    
    k->beta[0] = k->b / (k->b + k->e_sum);

    k->maxbeta = k->beta[0];
    k->maxbetaindex = 0;
    k->nonzerobeta = 1 - k->beta[0];

    for (i = 1; i < n + 1; i++) {  
      k->beta[i] = k->e[i] / (k->b + k->e_sum);
      
      if (k->beta[i] > k->maxbeta) {
	k->maxbeta = k->beta[i];
	k->maxbetaindex = i;
      }
    }

    for (i = 0, k->beta_sum = 0; i < n + 1; i++)  
      k->beta_sum += k->beta[i];
  }

  //----------------------------------------------------

  else 
    CR_error("unknown type");

  //----------------------------------------------------
  // PDAF: individual innovations v_i(t+1) (6-23)
  //----------------------------------------------------

  if (k->n) {
    v = make_CR_Matrix_array(n);
    v_t = make_CR_Matrix_array(n);
    temp_arr = make_CR_Matrix_array(n);
    
    for (i = 1; i < n + 1; i++) {
      v[i - 1] = difference_CR_Matrix(z[i - 1], k->z_pred);
      v_t[i - 1] = transpose_CR_Matrix(v[i - 1]);
      temp_arr[i - 1] = scale_CR_Matrix(k->beta[i], v[i - 1]) ;
    }
  }

  //----------------------------------------------------
  // PDAF: combined innovation v(t) (6-26)
  // (replaces innovation (2-228))
  //----------------------------------------------------

  if (k->v)
    free_CR_Matrix(k->v);
  if (k->n)
    k->v = sum_CR_Matrix_array(temp_arr, n);                     
  else 
    k->v = make_all_CR_Matrix("v", k->z_pred->rows, k->z_pred->cols, 0);
  
  //----------------------------------------------------
  // PDAF: state estimate x(t+1|t+1) (6-25)
  // (same as Kalman state estimate (2-227))
  //----------------------------------------------------

  temp = product_CR_Matrix(k->W, k->v);
  if (k->x)
    free_CR_Matrix(k->x);
  k->x = sum_CR_Matrix(k->x_pred, temp);                     
  free_CR_Matrix(temp);

  //----------------------------------------------------
  // PDAF: correct measurement cov estimate  
  // P_c(t+1|t+1)  (2-231)
  // (same as Kalman cov estimate (2-231))
  //----------------------------------------------------

  temp = product3_CR_Matrix(k->W, k->H, k->P_pred);
  if (k->P_c)
    free_CR_Matrix(k->P_c);
  k->P_c = difference_CR_Matrix(k->P_pred, temp);                
  free_CR_Matrix(temp);

  //----------------------------------------------------
  // PDAF: P_tilde P~(t+1) (6-28)
  //----------------------------------------------------

  if (k->n) {
    temp_arr = make_CR_Matrix_array(n);
    temp = transpose_CR_Matrix(k->v);
    temp3 = product_CR_Matrix(k->v, temp);
    free_CR_Matrix(temp);
    
    for (i = 1; i < n + 1; i++) {
      temp = product_CR_Matrix(v[i - 1], v_t[i - 1]);
      temp_arr[i - 1] = scale_CR_Matrix(k->beta[i], temp);
      free_CR_Matrix(temp);
    }
    temp = sum_CR_Matrix_array(temp_arr, n);
    temp2 = difference_CR_Matrix(temp, temp3);   
    free_CR_Matrix(temp);                      
    free_CR_Matrix(temp3);                     
    temp3 = transpose_CR_Matrix(k->W);
    if (k->P_tilde)
      free_CR_Matrix(k->P_tilde);
    k->P_tilde = product3_CR_Matrix(k->W, temp2, temp3);
    free_CR_Matrix(temp2);
    free_CR_Matrix(temp3);
  }
  else {
    if (k->P_tilde)
      free_CR_Matrix(k->P_tilde);
    k->P_tilde = make_all_CR_Matrix("P_tilde", k->P_last->rows, k->P_last->cols, 0);
  }

  //----------------------------------------------------
  // PDAF: cov estimate P(t+1|t+1) (6-27)
  // (replaces Kalman cov estimate (2-231))
  //----------------------------------------------------

  temp = scale_CR_Matrix(k->beta[0], k->P_pred);
  temp2 = scale_CR_Matrix(1 - k->beta[0], k->P_c);
  if (k->P)
    free_CR_Matrix(k->P);
  k->P = sum3_CR_Matrix(temp, temp2, k->P_tilde);       
  free_CR_Matrix(temp);
  free_CR_Matrix(temp2);

  //----------------------------------------------------
  // clean up
  //----------------------------------------------------

  if (k->n) {
    free_CR_Matrix_array(z, n);
    free_CR_Matrix_array(v, n);
    free_CR_Matrix_array(v_t, n);
  }

  //----------------------------------------------------
  // make currents into lasts
  //----------------------------------------------------

  free_CR_Matrix(k->x_last);
  free_CR_Matrix(k->P_last);
  k->x_last = copy_CR_Matrix(k->x);
  k->P_last = copy_CR_Matrix(k->P);
}

//----------------------------------------------------------------------------

// nearest neighbor standard filter
// z is measurements in validation gate

void update_nnsf_CR_Kalman(CR_Matrix **z, int n, CR_Kalman *k)
{
  CR_Matrix *targz;

  targz = CR_Kalman_nearest_neighbor(z, n, k);

  update_CR_Kalman(targz, k);    
}

// z is an array of validated measurements, 
// n is how many there are.
// z is destroyed after use

CR_Matrix *CR_Kalman_nearest_neighbor(CR_Matrix **z, int n, CR_Kalman *k)
{
  int i, lowi;
  float lowlike, like;
  CR_Matrix *lowz;
  

  if (n > 0) {
    lowlike = CR_Kalman_likelihood(z[0], k);
    lowi = 0;

    
    for (i = 1; i < n; i++) {
      like = CR_Kalman_likelihood(z[i], k);
      if (like < lowlike) {
	lowlike = like;
	lowi = i;
      }
    }


    lowz = copy_CR_Matrix(z[lowi]);
    
    return lowz;
  }
  else 
    return NULL;
}

//----------------------------------------------------------------------------

// does _NOT_ delete measurement z after use

float CR_Kalman_likelihood(CR_Matrix *z, CR_Kalman *k)
{
  CR_Matrix *v, *v_t, *l;
  float like;

  v = difference_CR_Matrix(z, k->z_pred);

  v_t = transpose_CR_Matrix(v);

  l = product3_CR_Matrix(v_t, k->Sinv, v);

  like = MATRC(l, 0, 0);
  free_CR_Matrix(l);
  free_CR_Matrix(v);
  free_CR_Matrix(v_t);

  return like;
}

//----------------------------------------------------------------------------

void print_CR_Kalman(CR_Kalman *k)
{
  int i;

  if (!k) {
    printf("NULL kalman\n");
    return;
  }

  printf("**********\n");
  printf("%i\n", k->updates);
  printf("**********\n");

  print_CR_Matrix(k->x);

  print_CR_Matrix(k->F);
  print_CR_Matrix(k->H);
  //  print_CR_Matrix(k->x_pred);
  //  print_CR_Matrix(k->P_pred);
  //  print_CR_Matrix(k->z_pred);
  //  print_CR_Matrix(k->S);
  //  print_CR_Matrix(k->Sinv);
  //  print_CR_Matrix(k->W);
  //  print_CR_Matrix(k->v);

  if (k->type == PDAF) {
    print_CR_Matrix(k->P_tilde);
    print_CR_Matrix(k->P_c);
  }

  //  print_CR_Matrix(k->P);

  if (k->type == PDAF) {

    if (k->type == PDAF) {
      if (k->e) {
	for (i = 1; i < k->n + 1; i++) 
	  printf("*e[%i] = %f*\n", i, k->e[i]);
	printf("\n");
      }
      else 
	printf("k->e empty\n");

      printf("*e_sum = %f*\n", k->e_sum);
      printf("*b = %f*\n", k->b);
      printf("\n");
    }

    if (k->beta) {
      for (i = 0; i < k->n + 1; i++) 
	printf("*beta[%i] = %f*\n", i, k->beta[i]);
    }
    else 
      printf("k->beta empty\n");

    printf("\n*beta_sum = %f*\n", k->beta_sum);

    //    printf("*max beta = %f*\n", k->maxbeta);
    //    printf("*max beta index = %i*\n", k->maxbetaindex);

    if (k->type == PDAF) {
      printf("\n");
      printf("measurements = %i\n\n", k->n);
      if (k->z_arr) {
	for (i = 0; i < k->n; i++) {
	  printf("*z_arr[%i]*\n", i + 1);    // calling 0th measurement the 1st, etc.
	  print_CR_Matrix(k->z_arr[i]);
	}
      }
      else
	printf("z_arr empty\n");
    }
  }
  else 
    print_CR_Matrix(k->z);

  //  printf("*****************\n");
}

//----------------------------------------------------------------------------

// r = radius, n = dimensions

// Vol(r, n) = [Pi^(n/2) r^n] / Gamma(1 + n/2)

// Gamma(1 + n/2) = (n/2)!  for n even
//                = [(2m - 1)!! sqrt(Pi)] / 2^m

double CR_hypersphere_volume(double r, int n)
{
  double num, denom;
  int m;

  printf("incorrect hypersphere volume for r = %f, n = %i\n", r, n);
  return PI;

  // num

  num = pow(PI, (double) n / 2) * pow(r, n);

  // denom

  // n odd

  if (n % 2) {   
    m = (int) floor((double) n / 2);
    denom = ((double) CR_double_factorial(2 * m - 1) * sqrt(PI)) / pow(2, m);
  }

  // n even

  else 
    denom = CR_factorial(n / 2);

  printf("r = %f, n = %i: num = %f, denom = %f, result = %f\n", r, n, num, denom, num / denom);

  // return 

  return num / denom;
}

//----------------------------------------------------------------------------

int CR_double_factorial(int n)
{
  int i, prod;

  // n odd

  if (n % 2) {

    for (i = 1, prod = 1; i <= n; i += 2)
      prod *= i;

  }

  // n even

  else {

    for (i = 2, prod = 1; i <= n; i += 2)
      prod *= i;

  }

  // return

  return prod;
}

//----------------------------------------------------------------------------

int CR_factorial(int n)
{
  int i, prod;

  for (i = 2, prod = 1; i <= n; i++)
    prod *= i;

  return prod;
}
*/
     
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------






