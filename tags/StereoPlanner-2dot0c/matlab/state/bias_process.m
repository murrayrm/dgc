% Process file for looking at the biases and scale factors recorded in
% kflog

load kflog.dat;

abx = kflog(:,9);
aby = kflog(:,10);
abz = kflog(:,11);
gbx = kflog(:,12);
gby = kflog(:,13);
gbz = kflog(:,14);
gsfx = kflog(:,15);

figure
plot(abx)
title('abx')
figure
plot(aby)
title('aby')
figure
plot(abz)
title('abz')

% figure
% plot(gbx)
% title('gbx')
% figure
% plot(gby)
% title('gby')
% figure
% plot(gbz)
% title('gbz')
% 
% figure
% plot(gsfx)
% title('gsfx')