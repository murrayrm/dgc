% Generates a plot of the errors between KF output and raw GPS 

% scroll down kf ouputs and find each gps output closest in time
% then get position error

kfpos = [kftime kfnorthing kfeasting];
gpstemp = [gpstime gpsnorthing gpseasting gpsactive];
gpspos = [];

% generate a gps matrix of same dimensions of the kfpos matrix with time
% indices matched up (approx)
for i = 1:length(kftime)
     k = find(gpstime(:, 1) >= kftime(i));
     j = k(1); % get first instance gps time is greater
     % check to see which element is closer in time, before or after
     if (j == 1) | ( j == length(gpstime))
         gpspos = [gpspos; gpstemp(j, :)];
     elseif (abs(gpstime(j-1) - kftime(i)) < abs(gpstime(j) - kftime(i)))
         gpspos = [gpspos; gpstemp(j-1, :)];
     else
         gpspos = [gpspos; gpstemp(j, :)];
     end
 end
 
 % calculate errors in orthing and easting and total error
 dn = abs(kfnorthing - gpspos(:,2));
 de = abs(kfeasting - gpspos(:,3));
 dp = sqrt(dn .* dn + de .* de);
 
 % plot error as a function of time
 % reference times to start
 t0 = kftime(1);
 time = kftime - t0;
 
 figure
 plot(time, dp);
 title('Absolute Error in IMU/KF Solution vs. GPS');
 xlabel('time (s)')
 ylabel('error (m)')
 
 % plot error as a function of change in heading
 % generate a vector of the differences in heading from one data point to
 % the next 
 kheading2 = [kfheading(2:end); 0];
 dheading = abs(kfheading - kheading2);
 totdelta = []; % total change in heading since the start
 for i = 1:length(dheading)
     totdelta = [totdelta sum(dheading(1:i))];
 end
 figure;
 plot(time, totdelta);
 title('Time vs. Total Change in Heading');
 xlabel('time(s)')
 ylabel('dheading (rad)')
 
 figure;
 plot(totdelta, dp);
 title('dheading vs. err')
 xlabel('dheading (rad)')
 ylabel('error (m)')
 
 
 