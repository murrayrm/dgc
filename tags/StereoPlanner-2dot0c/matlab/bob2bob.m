function bob2bob( bobfile, E_offset, N_offset, Yaw )
%
% function bob2bob( bobfile, E_offset, N_offset, Yaw )
%
% This function takes a Bob format waypoint file and a specification
% to translate and rotate the waypoints in it.  It writes a modified
% waypoint file with the waypoints translated and rotated as 
% specified
%
% Changes:
%
%   2004/02/21, Lars Cremean, created
%
% Inputs:
%
%   bobfile: A string that specifies the Bob format file in the current 
%   directory to read
%   N_offset: The Northing offset of the first waypoint, in meters
%   E_offset: The Easting offset of the first waypoint, in meters
%   Yaw: The angle about which to rotate the waypoints, measured 
%   CLOCKWISE.  The waypoints will rotate about waypoint 0.
%
% Example usage:
%
%   bob2bob( 'qid_original.bob', 376000, 3777000, pi/2 )

% read in the data from the Bob format file
[types, eastings, northings, radii, vees] = ...
textread( bobfile, '%c%f%f%f%f', 'commentstyle', 'shell');

wp0_orig = [eastings(1) northings(1)];

% translate the waypoint set so that the first waypoint is the origin
eastings  = eastings  - wp0_orig(1);
northings = northings - wp0_orig(2);

% rotate each of the waypoints about the first waypoint
% recall that Yaw is defined positive clockwise
R = [cos(Yaw) sin(Yaw); -sin(Yaw) cos(Yaw)];

for i=1:size(eastings)

    % assign the new eastings and northings
    % this will be a [2xN] array
    newen(:,i) = R * [eastings(i); northings(i)];
    
end

% then translate back to the original location plus the
% specified offset
newen(1,:) = newen(1,:) + wp0_orig(1) + E_offset;
newen(2,:) = newen(2,:) + wp0_orig(2) + N_offset;

% now write the resulting data to another file
file = fopen([bobfile,'_shift'], 'w');

fprintf(file, '# This is a modified Bob format waypoint file that has been\n');
fprintf(file, '# output by the MATLAB function bob2bob.m\n');
fprintf(file, '# Format: {char, Easting[m], Northing[m], Offset[m], Speed[m/s]}\n');
fprintf(file, '# The original file is %s\n', bobfile);

for i=1:size(eastings)
    
    fprintf(file, '%c  %9.2f  %10.2f  %7.2f  %6.2f\n', ...
    types(i), newen(1,i), newen(2,i), radii(i), vees(i));
    
end

fclose(file);
