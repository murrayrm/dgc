/* 
 * joystick.c - routines to read the joystick port
 *
 * RMM, 29 Dec 03
 *
 * This file is based on code written by Johnson Liu.  Cleaned things up
 * a bit, mainly.
 *
 * 31 Dec 03, RMM: modified code to handle scaling, centering data
 *   * Added scale and offset to joystick data structure
 *   * Got rid of state variables that did the same thing
 */

#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include "joystick.h"
#include "parallel.h"			/* DGC parallel port defs */
#include "sparrow/dbglib.h"

int joy_port;			// parallel port number for joystick
static long  count_per_usec;	// counts system can increment per usec
static long  STEER_INPUT_DELAY_COUNT; // use for loop to count to this number as delay

/*
 * Joystick constants
 *
 * These are the various constants that are used to define the interface
 * to the joystick.  Most of these look like they were measured.  Should
 * use a calibration routine instead (RMM).
 *
 */

#define STEER_DEFAULT_Y_CENTER	98	// y-axis center
#define STEER_DEFAULT_X_CENTER	98	// x-axis center
#define STEER_RIGHT_END		187	// right limit of travel
#define STEER_LEFT_END		10	// left limit of travel
#define BUSY_LOOP_NUM		30	// loop count for average delay
#define BUSY_LOOP_COUNT		500000 	// use to measure delay in system
#define STEER_INPUT_DELAY	4000	// # usec of output delay for uC

// Pin definitions for polling X, Y, Button
#define	STEER_X_PIN		PP_PIN14	
#define	STEER_Y_PIN		PP_PIN16	
#define	STEER_BUTTON_PIN	PP_PIN17	

/* Initialize the joystick port */
int joy_open(int port, struct joy_data *joydata)
{
   int ret_val;
   long loop_counter;
   int loop_index;
   struct timeval init, end;
   long   elapsed=0, total_elapsed=0;

   /* Intialize variables that we will need later */
   /* TBD: need to put this information in a configuration file */
   joy_port = port;
   joydata->offset[0] = STEER_DEFAULT_X_CENTER;
   joydata->offset[1] = STEER_DEFAULT_Y_CENTER;
   joydata->scale[0] = (STEER_RIGHT_END - STEER_LEFT_END)/2;
   joydata->scale[1] = -(STEER_RIGHT_END - STEER_LEFT_END)/2;
    
   /* execute several loops to benchmark the system*/ 
   for(loop_index=0; loop_index<BUSY_LOOP_NUM; loop_index++)
   {
	//printf("loop_index:%d", loop_index);fflush(stdout);
    	gettimeofday(&init, NULL);
      	for(loop_counter=0; loop_counter<BUSY_LOOP_COUNT; loop_counter++);
    	gettimeofday(&end, NULL);
	elapsed= (end.tv_sec-init.tv_sec)*1000000+(end.tv_usec-init.tv_usec);
	//printf(" elapsed: %ld	", elapsed);
	if (elapsed == 0) 
	  dbg_error("BUSY_LOOP_COUNT not big enough!!!\n");
	total_elapsed+=elapsed;
   }
   
   // calculate how many counts the system can increment per microsecond
   count_per_usec= (long) BUSY_LOOP_COUNT/total_elapsed*BUSY_LOOP_NUM;
   //printf("count_per_usec %ld\n", count_per_usec);
   STEER_INPUT_DELAY_COUNT = STEER_INPUT_DELAY * count_per_usec;

   ret_val=pp_init(joy_port, PP_DIR_IN); // initialize parallel port
   return   ret_val;
}

/* Read the joystick data */
int joy_read (struct joy_data *st)
{
   long i;

   /* Pole the x-axis to get the position */
   pp_set_bits(joy_port, STEER_X_PIN, STEER_Y_PIN | STEER_BUTTON_PIN);

   /* Wait for a bit for the joystick to respond */
   /* RMM: this should be a #define and should be based on CPU speed ?? */
   usleep(50);

   /* Read the data from the parallel port */
   st->data[0]=(float) pp_get_data(joy_port);
   dbg_info("joy_read: %3d ", st->data[0]);   		

   /* RMM: not sure why this is here.  Delete? */
   pp_set_bits(joy_port, 0, STEER_X_PIN | STEER_Y_PIN | STEER_BUTTON_PIN);

   /* Now read the y-axis; same protocol */
   pp_set_bits(joy_port, STEER_Y_PIN, STEER_X_PIN | STEER_BUTTON_PIN);
   usleep(50);

   st->data[1]= (float) pp_get_data(joy_port);
   dbg_info("joy_read", "y: %3d ", st->data[1]);   		

   pp_set_bits(joy_port, 0, STEER_X_PIN | STEER_Y_PIN | STEER_BUTTON_PIN);
   //usleep(STEER_WAIT);

   /* Now read the buttons */
   pp_set_bits(joy_port, STEER_BUTTON_PIN, STEER_X_PIN | STEER_Y_PIN);
   usleep(50);
   st->buttons=~pp_get_data(joy_port);   		

   return 0;
}


/* Clean up joystick interface */
int joy_close()
{
   return   pp_end(joy_port);
}

/* these need to be fleshed out */
int joy_pause(void) {return 0;}
int joy_disable(void) {return 0;}
int joy_resume(void) {return 0;}
