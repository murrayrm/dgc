#include "ladar_power.h"

int ladar_pp_init(){
  //set up the parallel port with data output
  //  pp_end(LADAR_PPORT);
  
  if((pp_init(LADAR_PPORT, PP_DIR_OUT))){ //if we can open the port
    ladar_on(LADAR_BUMPER); //turn on both the ladars
    ladar_on(LADAR_ROOF);
    return 0;
  }
  
  else 
    return -1;
}

int ladar_on(int ladar_number){
  //put ladar_number low -> pin is low, output of box is high, relay is on
  return(pp_set_bits(LADAR_PPORT, 0, ladar_number));
}

int ladar_off(int ladar_number){
  //put ladar_number high-> pin high... relay off
  return(pp_set_bits(LADAR_PPORT, ladar_number, 0));
}
