#include "MTA/Modules.hh"
#include "vsmta.hh"
#include "MTA/Kernel.hh"
#include <boost/shared_ptr.hpp>
#include <stdlib.h>
#include <math.h>
#include <iostream.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <fstream.h>

#include "VState.hh"
#include "LatLong.h"
#include "kfilter/kfilter.h"
#include "kfilter/Kalmano.h"
#include "kfilter/Global.h"
#include "statefilter.hh"
#include "imu.h"
#include "gps.h"
#include "magnetometer.h"
#include <pthread.h>



// Externally defined Kalman Filter variables                                 
extern int NavBufferIndex;
extern TSaveNavData NavBuffer[XRATIO2];

extern double gps_lat, gps_lon, gps_alt;
extern double gps_vn, gps_ve, gps_vu;
extern double gps_time;
extern double imu_time;
extern double mag_hdg;

extern int New_GPS_data_flag;   // flag set when new GPS data comes in          
extern int New_Mag_data_flag;

extern double imu_dvx,imu_dvy,imu_dvz; // Delta-v's m/s (should be)             
extern double imu_dtx,imu_dty,imu_dtz; // Delta-Thetas,should be rads           

extern double kp[NUM_ST+1][NUM_ST+1];  // covariance matrix                     
extern double kx[NUM_ST+1];            // kalman gains                          

extern double tot_kabx, tot_kaby, tot_kabz;
extern double tot_ksfx, tot_ksfy, tot_ksfz;
extern double tot_kgbx, tot_kgby, tot_kgbz;
extern double tot_kgsfx;

extern double ExtGPSPosMeas_x, ExtGPSPosMeas_y, ExtGPSPosMeas_z;
extern double ExtGPSVelMeas_x, ExtGPSVelMeas_y, ExtGPSVelMeas_z;

extern double GPS_xy_kr, GPS_h_kr, GPS_v_kr; // errors                          

// Data reading and writing declarations
ifstream imuraw;  // raw imu data read in
ifstream gpsraw;  // raw gps data read in
ifstream gpsdop; // gps dop data
ofstream outfile; // output state estimator data

char imuname[] = "./sim/imulog.dat";
char gpsname[] = "./sim/gpslog.dat";
char output[]  = "./sim/statelog.dat";
char dopname[] = "./sim/dopfile.dat";

struct VState_GetStateMsg vehstate;

int use_gps = 1;

int main()
{
  string blank; // string used to dump first line of each readfile into
  double initheading; // initial heading to start with
  LatLong latlong(0,0);
  double imutime = 0;
  double gpstime = 0;
  double imu1hz = 0;
  gpsDataWrapper gpsdata;
  int gps_valid, gps_nav_mode, dat_flag, gps_ext_nav, gps_active;
  int gps_usable = 0; // whether the gps is usable or not (within time)
  double gps_north, gps_east;
  int first_time = 1;
  double gps_jumped = 0;
  double gps_error = 3;
  int gps_invalid_count = 0;
  
  double pfom, gdop, pdop, hdop, vdop, tdop, tfom;

  cout<<"Opening files...";
  // open file streams
  imuraw.open(imuname);
  getline(imuraw, blank);
  
  cout << "imu header = " << blank << endl;

  gpsraw.open(gpsname);
  getline(gpsraw, blank);

  cout << "gps header = " << blank << endl;

  gpsdop.open(dopname);
  getline(gpsdop, blank);
  cout<<"dop header = " << blank << endl;

  outfile.open(output);
  outfile<<"%Warning:  SIMULATED FILE"<<endl;
  outfile<<"%imu_time"<<'\t'<<"vehstate.Easting"<<
    '\t'<<"vehstate.Northing"<<'\t'<<"vehstate.Altitude"<<
    '\t'<<"vehstate.Vel_E"<<'\t'<<"vehstate.Vel_N"<<
    '\t'<<"vehstate.Vel_U"<<'\t'<<"vehstate.Speed"<<
    '\t'<<"vehstate.Accel"<<'\t'<<"vehstate.Pitch"<<
    '\t'<<"vehstate.Roll"<<'\t'<<"vehstate.Yaw"<<'\t'<<"kf_lat"<<
    '\t'<<"kf_lng"<<'\t'<<"gps error"<<endl;
  cout<<"done"<<endl;

  cout<<"Initializing KF...";
  // initialize KF
  DGCNavInit();
  cout<<"done"<<endl;
 
  // loop through reading imu data and inputting into KF
  while(!imuraw.eof())
  {
    // IMU THREAD ACTIONS
    // read imu data
    cout<<"reading imu...";
    imuraw>>imu_time>>imu_dvx>>imu_dvy>>imu_dvz>>imu_dtx>>imu_dty>>imu_dtz;
    imutime = imu_time; // set local imu time
    cout<<"done"<<endl;

    // read in KF state
    vehstate.kf_lat = NavBuffer[NavBufferIndex].lat / DEG2RAD;
    vehstate.kf_lng = NavBuffer[NavBufferIndex].lon / DEG2RAD;
    vehstate.Altitude = -NavBuffer[NavBufferIndex].alt;
    latlong.set_latlon(vehstate.kf_lat, vehstate.kf_lng);
    latlong.get_UTM(&vehstate.Easting, &vehstate.Northing);

    vehstate.Vel_N = NavBuffer[NavBufferIndex].vn;
    vehstate.Vel_E = NavBuffer[NavBufferIndex].ve;
    vehstate.Vel_U = NavBuffer[NavBufferIndex].vd;
    vehstate.Speed = sqrt(vehstate.Vel_N * vehstate.Vel_N + vehstate.Vel_E
                              * vehstate.Vel_E + vehstate.Vel_U *
			  vehstate.Vel_U);


    vehstate.Yaw = NavBuffer[NavBufferIndex].thdg * DEG2RAD;
    vehstate.Pitch = NavBuffer[NavBufferIndex].pitch;
    vehstate.Roll =  NavBuffer[NavBufferIndex].roll;
    vehstate.Altitude = 0;

    // log the state
    if (imu1hz >= 400){
    outfile.precision(10);
    outfile<<imutime<<'\t'<<vehstate.Easting<<
      '\t'<<vehstate.Northing<<'\t'<<vehstate.Altitude<<
      '\t'<<vehstate.Vel_E<<'\t'<<vehstate.Vel_N<<
      '\t'<<vehstate.Vel_U<<'\t'<<vehstate.Speed<<
      '\t'<<vehstate.Accel<<'\t'<<vehstate.Pitch<<
      '\t'<<vehstate.Roll<<'\t'<<vehstate.Yaw<<
      '\t'<<vehstate.kf_lat<<'\t'<<vehstate.kf_lng<<
      '\t'<<sqrt(GPS_xy_kr) * EARTH_RADIUS<<
      '\t'<<gps_jumped<<endl;
    
    imu1hz = 0;
    }
    imu1hz++;

    // run KF
    DGCNavRun();
    // END IMU THREAD ACTIONS

    // MAGNETOMETER THREAD ACTIONS
    if (New_Mag_data_flag == 0) {
      cout<<"Enter Initial heading";
      cin>>initheading;
      mag_hdg = initheading;
      New_Mag_data_flag = 1;
    }
    // END MAGNETOMETER THREAD ACTIONS

    // GPS THREAD ACTIONS
    // read gps to next time greater than current time
    while(((first_time) || (gpstime < imutime)) && !gpsraw.eof())
      {
	//cout<<"Reading gps...";
	gpsraw>>gpstime>>gps_valid>>
	  dat_flag>>gps_nav_mode>>
	  gpsdata.data.lat>>gpsdata.data.lng>>
	  gps_east>>gps_north>>gpsdata.data.altitude>>
	  gpsdata.data.vel_n>>gpsdata.data.vel_e>>
	  gpsdata.data.vel_u>>gps_active>>
	  gps_ext_nav>>gps_jumped>>
	  gpsdata.data.sats_used;//>>gps_error;
	//cout<<"done"<<endl;
	
	// read in gps dops
	gpsdop>>pfom>>gdop>>pdop>>hdop>>vdop>>tdop>>tfom;

	gps_usable = 0;
	first_time = 0;
	if (gps_valid >0 ){
	  // set gps error based on jump status
	  gps_jumped = detect_jump(gps_north, gps_east, gpstime, vehstate);
	  GPS_xy_kr = gps_err_reset(GPS_xy_kr, gps_jumped, gdop);
	  gps_invalid_count = 0;
	}else{
	  gps_invalid_count++;
	  if (gps_invalid_count >= 5){
	    detect_reset();
	  }
	}
	//GPS_xy_kr = pow(6 / EARTH_RADIUS, 2);
      }
    
    if (imutime < gpstime && gpstime - imutime < .15)
      gps_usable = 1;
    	
    // use current gps if flag is set and use gps (set in jump detect) 
    if ((New_GPS_data_flag == 0) && (gps_valid > 0)&& (gps_usable))
      {	
	gps_time = gpstime;
	gps_lat = gpsdata.data.lat*DEG2RAD;
	gps_lon = gpsdata.data.lng*DEG2RAD;
	gps_alt = gpsdata.data.altitude;
	gps_vu  = gpsdata.data.vel_u;
	gps_vn  = gpsdata.data.vel_n;
	gps_ve  = gpsdata.data.vel_e;
        //use_gps = 0;
	New_GPS_data_flag = 1;
      }
  }
  
  cout << "I'm done." << endl;

  imuraw.close();
  gpsraw.close();
  outfile.close();

  return 0;
}
  
