/*
 * vehports.h - default port assignments for Tahoe
 *
 * RMM, 2 Jan 04
 *
 * This is the same information that was getting put into Constants.h
 * Renamed the file to someting more description and moved into the 
 * vehicle library directory.
 *
 * Sue Ann Hong, 17 Jan 04
 *  Moved parallel port definitions from parallel.h and deleted 
 *  ACCEL_PARALLEL_PORT.
 */

#ifndef VEHPORTS_H
#define VEHPORTS_H

// state sensor port definitions
// NOTE:  These port definitions are for Magnesium, not titanium
#define IMU_IP_PORT 9000		/* IP port number */
#define GPS_SERIAL_PORT 4		/* serial port */
#define OBD_SERIAL_PORT 0               // OBDII serial port
#define MAG_SERIAL_PORT 6       /* magneotmer serial port */

#define BRAKE_SERIAL_PORT 6		/* serial port */
#define STEER_SERIAL_PORT 0     /* steering serial port */


/* Parallel ports */
#define JOYSTICK_PORT 0			/* parallel port */
#define TRANS_PORT 0           //parallel port on VManage computer (magnesium)
#define ESTOP_PORT 1           //parallel port on VManage computer (magnesium)

enum {
  PP_STEER,           /* The steering wheel / joystick */
  PP_NULL,
  PP_GASBR,           /* gas, brake sensors */
  PP_TRANS,           /* Transmission controller */
  PP_NUM_PORTS
};

#endif   //  VEHPORTS_H
