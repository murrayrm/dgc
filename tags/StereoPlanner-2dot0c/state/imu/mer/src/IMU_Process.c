// IMU_Process.cpp : Converts raw IMU data to decimal values
// Created 12/5/03 Haomiao Huang

//#include <iostream>
//#include <iomanip>
#include <math.h>
#include "IMU_Process.h"

//using namespace std;

//int main()
//{
//	// Debug/test code for accel and gyro processing
//	short blah=0;
//	double acc;
//
//	while(blah !=-999)
//	{
//		cout<< "Enter Raw Accel value:";
//		cin>>blah;
//
//		acc = process_accel(blah);
//		cout<< "Delta-v is: " <<setprecision(5)<< acc <<endl;
//
//		cout<< "Enter Raw gyro value:";
//		cin>>blah;
//
//		acc = process_theta(blah);
//		cout<< "Delta-theta is: " <<setprecision(5)<< acc <<endl;
//	}
//
//	return 1;
//}



// process_theta
// Converts B-4.19 encoded dtheta raw data into decimal numbers
// B-4.19 - 
// binary encoding
// binary point is 4 digits to left of highest digit after sign
// digit, so 15th digit starts as 2^-4 power
// argument t is a 16-bit signed int
double process_theta(short t)
{
	return ((double) t)/pow(2, 19);
}

// process_accel
// Converts B1.14 encoded Accel raw data into decimal numbers
// B1.14 -
// binary encoding
// 1st digit after sign digit is the >1 portion
// 14 digits after are the fractional portion
// binary point is between 15th and 14th digits
// argument a is a 16-bit signed int
double process_accel(short a)
{
 return a/pow(2, 14);
}