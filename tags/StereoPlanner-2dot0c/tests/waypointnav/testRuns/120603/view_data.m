function [] = view_data(test_file, waypoint_file);
% 
% function view_data.m( test_file, waypoint_file )
%
% Author: Luis Goncalves
%
% Date: 12/06/03

data = load(test_file); % logged data file, typically named  test33.dat
waypoints = load(waypoint_file); % waypoint files, typicalliy waypoints33.dat


% define first waypoint to be the origin
e_offset = waypoints(1,2);
n_offset = waypoints(1,1);


% format of test_file
%  1          2         3                  4            5           6     7            8        9          10
% Time(s)	Accel	Steering ([-1,1])	Position	NorthVel	EastVel	Northing	Easting	OBD Speed	OBD RPM

time = data(:,1);
northing = data(:,7) - n_offset;
easting = data(:,8) - e_offset;

vel_n = data(:,5);
vel_e = data(:,6);

% format of waypoint_file
% northing easting
waypoints(:,1) = waypoints(:,1) - n_offset;
waypoints(:,2) = waypoints(:,2) - e_offset;

% draw circles around waypoints
Radius = 5;
theta = [0:360]/180*pi;
circle_e = Radius*cos(theta);
circle_n = Radius*sin(theta);

figure(100); clf; hold on;
    for i=1:size(waypoints,1);
        plot(circle_e + waypoints(i,2), circle_n + waypoints(i,1),'r');
        text(waypoints(i,2), waypoints(i,1), num2str(i));
        % plot(waypoints(i,2), waypoints(i,1), 'ro');
    end
    plot(easting, northing);
    hold on
    quiver(easting(1:10:end), northing(1:10:end), vel_e(1:10:end), vel_n(1:10:end), 1);
    plot(easting(1), northing(1), 'rs');

    xlabel('relative easting [m]', 'FontSize', 18);
    ylabel('relative northing [m]', 'FontSize', 18);
    title([test_file '    ' waypoint_file], 'FontSize', 24);
    axis equal;
        


