#!/bin/bash

# Changes: 
#   01/18/2004 - Lars Cremean - Created.
#
# This file automates a quick fix to the problem in which MTA modules of
# different users who are testing all see each other.  If you change the
# "receive_key" in MTA-2.1/Kernel/KernelDef.hh, then only the modules
# that have the same receive key will see each other.
#
# This file automates the process of remaking MTA after changing to a 
# unique receive key in KernelDef.hh
#
# Usage: Manually copy MTA-2.1/Kernel/KernelDef.hh to team/ (and have this
# file in the same directory).  Change to a unique receive key and run
# ./kernel_def_copy.sh
#

echo
echo "Diffing KernelDef.hh..."
diff KernelDef.hh MTA-2.1/Kernel/KernelDef.hh
echo

echo
echo "Copying KernelDef.hh to MTA-2.1/Kernel/"
cp KernelDef.hh MTA-2.1/Kernel/
echo

echo
echo "Doing a make clean all in Kernel/..."
cd MTA-2.1/Kernel/
make clean all
cd ../..
echo

echo
echo "Doing a make install in MTA-2.1/..."
cd MTA-2.1/
make install
echo


