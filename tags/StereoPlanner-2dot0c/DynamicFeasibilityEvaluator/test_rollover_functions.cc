
#include "vehicle_constants.h"
#include "yaw_pitch_roll_2_matrix.h"
#include "rollover_functions.h"
#include <math.h>
#include <stdio.h>

#define EPS 1e-5

#define g 9.8 // m/s^2, acceleration due to gravity


/* the functions to test:
    double steering_angle_2_arc_radius( double s_angle )

    void gravity_components_in_vehicle_ref_frame(const double yaw, const double pitch, const double roll, 
            double *g_z, double *g_y, double *g_x)

    void min_max_lateral_acceleration_for_rollover_safety(const double g_z, const double g_y, const double T_roll,
            double *a_R, double *a_L)

    double rollover_coefficient(const double a_T, const double g_z, const double g_y)

    void min_max_velocity_for_rollover_safety(const double a_R, const double a_L, const double arc_radius, 
            double *min_v, double *max_v)
*/            

int main(int argc, const char *argv[])
{
    double c_roll;
    double c_roll_correct;

    double a_R, a_L;
    double a_R_correct, a_L_correct;

    double yaw, pitch, roll;
    double g_z, g_y, g_x;

    double T_roll;

    double min_v, max_v;
    double min_v_correct, max_v_correct;

    double arc_radius;

    bool all_ok = true;

    /* *************************** */
    /* test   rollover_coefficient */
    /* *************************** */

        fprintf(stdout, "\n\n*** Test rollover_coefficient ***\n\n");

        // double rollover_coefficient(const double a_T, const double g_z, const double g_y)

        fprintf(stdout, "forward on flat ground: ");
            c_roll = rollover_coefficient( 0, g, 0);
            c_roll_correct = 0;
            if (fabs(c_roll - c_roll_correct) > EPS)
            {
                fprintf(stdout,"Failed, c_roll = %f instead of %f\n", c_roll, c_roll_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK\n");

        fprintf(stdout, "hard right on flat ground: ");
            c_roll = rollover_coefficient( g, g, 0);
            c_roll_correct = 1;
            if (fabs(c_roll - c_roll_correct) > EPS)
            {
                fprintf(stdout,"Failed, c_roll = %f instead of %f\n", c_roll, c_roll_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK\n");

        fprintf(stdout, "hard left on flat ground: ");
            c_roll = rollover_coefficient( -g, g, 0);
            c_roll_correct = -1;
            if (fabs(c_roll - c_roll_correct) > EPS)
            {
                fprintf(stdout,"Failed, c_roll = %f instead of %f\n", c_roll, c_roll_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK\n");


        double angle = 45.0 / 180.0 * 355.0 / 113.0;

        fprintf(stdout, "forward on 45 degree rightward embankment: ");
            c_roll = rollover_coefficient( 0, g*cos(angle), g*sin(angle));
            c_roll_correct = -1;
            if (fabs(c_roll - c_roll_correct) > EPS)
            {
                fprintf(stdout,"Failed, c_roll = %f instead of %f\n", c_roll, c_roll_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK\n");


    /* ***************************************** */
    /* test   min_max_lateral_accel_for_rollover */
    /* ***************************************** */

        fprintf(stdout, "\n\n*** Test min_max_lateral_acceleration_for_rollover_safety ***\n\n");

        fprintf(stdout, "On flat ground: ");
            yaw = 0; pitch = 0; roll = 0;
            gravity_components_in_vehicle_ref_frame( yaw, pitch, roll, &g_z, &g_y, &g_x );
            T_roll = 1;
            min_max_lateral_acceleration_for_rollover_safety(g_z, g_y, T_roll, &a_R, &a_L);
            a_R_correct = -g;
            a_L_correct = g;
            if (fabs(a_R -  a_R_correct) > EPS)
            {
                fprintf(stdout,"Failed, a_R = %f instead of %f..", a_R, a_R_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK...");
            if (fabs(a_L -  a_L_correct) > EPS)
            {
                fprintf(stdout,"Failed, a_R = %f instead of %f\n", a_L, a_L_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK\n");


        fprintf(stdout, "On 45 degree rightward embankment: ");
            yaw = 0; pitch = 0; roll = 45.0 / 180.0 * (355.0/113.0);
            gravity_components_in_vehicle_ref_frame( yaw, pitch, roll, &g_z, &g_y, &g_x );
            T_roll = 1;
            min_max_lateral_acceleration_for_rollover_safety(g_z, g_y, T_roll, &a_R, &a_L);
            a_R_correct = 0;
            a_L_correct = g_z + g_y;
            if (fabs(a_R -  a_R_correct) > EPS)
            {
                fprintf(stdout,"Failed, a_R = %f instead of %f..", a_R, a_R_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK...");
            if (fabs(a_L -  a_L_correct) > EPS)
            {
                fprintf(stdout,"Failed, a_R = %f instead of %f\n", a_L, a_L_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK\n");

        fprintf(stdout, "On 45 degree leftward embankment: ");
            yaw = 0; pitch = 0; roll = -45.0 / 180.0 * (355.0/113.0);
            gravity_components_in_vehicle_ref_frame( yaw, pitch, roll, &g_z, &g_y, &g_x );
            T_roll = 1;
            min_max_lateral_acceleration_for_rollover_safety(g_z, g_y, T_roll, &a_R, &a_L);
            a_R_correct = -g_z + g_y;
            a_L_correct = 0;
            if (fabs(a_R -  a_R_correct) > EPS)
            {
                fprintf(stdout,"Failed, a_R = %f instead of %f..", a_R, a_R_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK...");
            if (fabs(a_L -  a_L_correct) > EPS)
            {
                fprintf(stdout,"Failed, a_R = %f instead of %f\n", a_L, a_L_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK\n");

    /* ******************************************* */
    /* test   min_max_velocity_for_rollover_safety */
    /* ******************************************* */

        fprintf(stdout, "\n\n*** Test min_max_velocity_for_rollover_safety ***\n\n");

        fprintf(stdout, "moving straight ahead on flat ground: ");
            yaw = 0; pitch = 0; roll = 0;
            gravity_components_in_vehicle_ref_frame( yaw, pitch, roll, &g_z, &g_y, &g_x );
            T_roll = 1;
            min_max_lateral_acceleration_for_rollover_safety(g_z, g_y, T_roll, &a_R, &a_L);
            arc_radius = 0;
            min_max_velocity_for_rollover_safety(a_R, a_L, arc_radius, &min_v, &max_v);
            min_v_correct = 0;
            max_v_correct = VEHICLE_MAX_V;
            if (fabs(min_v - min_v_correct) > EPS)
            {
                fprintf(stdout,"Failed, min_v = %f instead of %f..", min_v, min_v_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK...");
            if (fabs(max_v - max_v_correct) > EPS)
            {
                fprintf(stdout,"Failed, max_v = %f instead of %f\n", max_v, max_v_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK\n");

        fprintf(stdout, "hard right on flat ground: ");
            yaw = 0; pitch = 0; roll = 0;
            gravity_components_in_vehicle_ref_frame( yaw, pitch, roll, &g_z, &g_y, &g_x );
            T_roll = 1;
            min_max_lateral_acceleration_for_rollover_safety(g_z, g_y, T_roll, &a_R, &a_L);
            arc_radius = 1; // a = v^2/r, r = a/v^2
            min_max_velocity_for_rollover_safety(a_R, a_L, arc_radius, &min_v, &max_v);
            min_v_correct = 0;
            max_v_correct = sqrt(g);
            if (fabs(min_v - min_v_correct) > EPS)
            {
                fprintf(stdout,"Failed, min_v = %f instead of %f..", min_v, min_v_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK...");
            if (fabs(max_v - max_v_correct) > EPS)
            {
                fprintf(stdout,"Failed, max_v = %f instead of %f\n", max_v, max_v_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK\n");

        fprintf(stdout, "hard left on flat ground: ");
            yaw = 0; pitch = 0; roll = 0;
            gravity_components_in_vehicle_ref_frame( yaw, pitch, roll, &g_z, &g_y, &g_x );
            T_roll = 1;
            min_max_lateral_acceleration_for_rollover_safety(g_z, g_y, T_roll, &a_R, &a_L);
            arc_radius = -1; // a = v^2/r, r = a/v^2
            min_max_velocity_for_rollover_safety(a_R, a_L, arc_radius, &min_v, &max_v);
            min_v_correct = 0;
            max_v_correct = sqrt(g);
            if (fabs(min_v - min_v_correct) > EPS)
            {
                fprintf(stdout,"Failed, min_v = %f instead of %f..", min_v, min_v_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK...");
            if (fabs(max_v - max_v_correct) > EPS)
            {
                fprintf(stdout,"Failed, max_v = %f instead of %f\n", max_v, max_v_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK\n");

        fprintf(stdout, "moving straight ahead on 45 degree rightward embankment: ");
            yaw = 0; pitch = 0; roll = 45.0 / 180.0 * (355.0/113.0);
            gravity_components_in_vehicle_ref_frame( yaw, pitch, roll, &g_z, &g_y, &g_x );
            T_roll = 1;
            min_max_lateral_acceleration_for_rollover_safety(g_z, g_y, T_roll, &a_R, &a_L);
            arc_radius = 0;
            min_max_velocity_for_rollover_safety(a_R, a_L, arc_radius, &min_v, &max_v);
            min_v_correct = 0;
            max_v_correct = 0;
            if (fabs(min_v - min_v_correct) > EPS)
            {
                fprintf(stdout,"Failed, min_v = %f instead of %f..", min_v, min_v_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK...");
            if (fabs(max_v - max_v_correct) > EPS)
            {
                fprintf(stdout,"Failed, max_v = %f instead of %f\n", max_v, max_v_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK\n");

        fprintf(stdout, "hard left on 45 degree rightward embankment: ");
            yaw = 0; pitch = 0; roll = 45.0 / 180.0 * (355.0/113.0);
            gravity_components_in_vehicle_ref_frame( yaw, pitch, roll, &g_z, &g_y, &g_x );
            T_roll = 1;
            min_max_lateral_acceleration_for_rollover_safety(g_z, g_y, T_roll, &a_R, &a_L);
            arc_radius = -1;
            min_max_velocity_for_rollover_safety(a_R, a_L, arc_radius, &min_v, &max_v);
            min_v_correct = 0;
            max_v_correct = 0;
            if (fabs(min_v - min_v_correct) > EPS)
            {
                fprintf(stdout,"Failed, min_v = %f instead of %f..", min_v, min_v_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK...");
            if (fabs(max_v - max_v_correct) > EPS)
            {
                fprintf(stdout,"Failed, max_v = %f instead of %f\n", max_v, max_v_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK\n");

        fprintf(stdout, "hard right on 60 degree rightward embankment: ");
            yaw = 0; pitch = 0; roll = 60.0 / 180.0 * (355.0/113.0);
            gravity_components_in_vehicle_ref_frame( yaw, pitch, roll, &g_z, &g_y, &g_x );
            T_roll = 1;
            min_max_lateral_acceleration_for_rollover_safety(g_z, g_y, T_roll, &a_R, &a_L);
            a_R_correct = -g_z + g_y;
            a_L_correct = g_z + g_y;
            arc_radius = 1;
            min_max_velocity_for_rollover_safety(a_R, a_L, arc_radius, &min_v, &max_v);
            min_v_correct = sqrt(a_R_correct);
            max_v_correct = sqrt(a_L_correct);
            if (fabs(min_v - min_v_correct) > EPS)
            {
                fprintf(stdout,"Failed, min_v = %f instead of %f..", min_v, min_v_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK...");
            if (fabs(max_v - max_v_correct) > EPS)
            {
                fprintf(stdout,"Failed, max_v = %f instead of %f\n", max_v, max_v_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK\n");

        fprintf(stdout, "hard left on 60 degree leftward embankment: ");
            yaw = 0; pitch = 0; roll = -60.0 / 180.0 * (355.0/113.0);
            gravity_components_in_vehicle_ref_frame( yaw, pitch, roll, &g_z, &g_y, &g_x );
            T_roll = 1;
            min_max_lateral_acceleration_for_rollover_safety(g_z, g_y, T_roll, &a_R, &a_L);
            a_R_correct = -g_z + g_y;
            a_L_correct = g_z + g_y;
            arc_radius = 1;
            min_max_velocity_for_rollover_safety(a_R, a_L, arc_radius, &min_v, &max_v);
            min_v_correct = sqrt(a_L_correct);
            max_v_correct = sqrt(a_R_correct);
            if (fabs(min_v - min_v_correct) > EPS)
            {
                fprintf(stdout,"Failed, min_v = %f instead of %f..", min_v, min_v_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK...");
            if (fabs(max_v - max_v_correct) > EPS)
            {
                fprintf(stdout,"Failed, max_v = %f instead of %f\n", max_v, max_v_correct);
                all_ok = false;
            }
            else
                fprintf(stdout,"OK\n");


    // Final Answer ?
        if (all_ok)
        {
            fprintf(stdout, "\n\nAll checks OK !!!\n");
        }
        else
        {
            fprintf(stdout, "\n\n*** Some checks have failed !!! ***\n");
            fprintf(stdout, "Have you enabled USE_DEBUG_VEHICLE_CONSTANTS in vehicle_constants.h ???\n");
        }
}

