@node vdrive,,,Top
@chapter VDrive

The @code{vdrive} program and MTA module are used to control the
motion of the vehicle.  Along with @code{vstate}, it represents the
main embedded systems code, linking commanded motion to the physical
hardware.

@section Starting @code{vdrive}

The following command line options are supported in version 0.9:

@table @code

@item -v
Turn on verbose error messages

@item -s
Turn off control of the steering wheel.

@item -m
Turn on MTA message handling (for receiving commands)

@item -t
Turn off the throttle

@item -l logfile
Send error messages to a log file

@item -b
Turn off the brake
@end table

When @code{vdrive} starts up, it will look for two files:

@table @file
@item vdrive.dev
This is a file that is distributed with @code{vdrive} and contains the
channel definitions required to allow saving data.  If you modify the
@code{vdrive} code and add more data channels, you will need to update
this file.  See sparrow documentation for more information on the
content and format of this file.  If this file does not exist, no data
will be saved when data capture is turned on.

@item vdrive.ini
This file contains values for parameters that should be initalized on
startup.  The only parameters that are currently defined are those
that are included in the @code{ACTDEV} table, available off of the
main sparrow library.  If this file does not exist, the hard coded
values of the parameters will be used.
@end table

If the @code{-v} flag is turned on, @code{vdrive} will print out
status and error messages as it starts up.  If there are any errors,
@code{vdrive} will ask for confirmation before continuing.  (TODO:
need to make it so that error messages still print even if @code{-v}
is not set.)  Once the initialization is complete, @code{vdrive} will
enter the main display routine.

@section Running @code{vdrive}

Once @code{vdrive} is up and running, the display manager takes over
control of the screen.  The standard Sparrow key bindings can be used
to move around the screen, press buttons and enter new values.  The
screen is formatted as follows:

@example
VDrive 1.0	        J: <1>    S: <2>    A: <3>    M: <4>
Mode: D=<5>  S=<6>	T: <7>    B: <8>    S: <9>    X: <10>

         Off    Ref   Act   Gain   Cmd	      | Joy:  %jx  %jy    %jbut
Drive   <11>   <13>  <14>   <16>  <18>  <19>  |   %jyraw	  %jydir
Steer   <12>         <15>   <17>  <20>        |   %pitalt	  %xypos

Axis   IMU      GPS       KF     Vel     | Configuration files:
 X     %imdx    %gpx      %kfx   %kfvx   |   Chn config: vdrive.dev
 Y     %imdy    %gpy      %kfy   %kfvy   |   Dump file:  %dumpfile
 Z     %imdz    %gpz      %kfz   %kfvz   |   Brake: pos: %brkpos vel: %brkvel 
 R     %imdr    -------   %kfr           |   	    cur: %brkcur cas: %brkcas
 P     %imdp    M %magn   %kfp           |   estop: %estopval
 Y     %imdw    G %gpsy   %kfw           |   

                    <SPACE> emergency stop

Accel: %acce	IMU: %imue      Cruise: %crue	KF: %kfe
Brake: %brke	GPS: %gpse      %STRBT %stre

%QUIT %ACDV	%CAPTURE %DUMP
@end example

@table @code
@item <1>-<4>
The upper row
@end table

Key bindings:
@table @code
@item M
Put the vehicle into manual control mode.

@item R
Put the vehicle into remote control mode.

@item <SPC>
(space bar) Bring the vehicle to a rapid stop.
@end table


