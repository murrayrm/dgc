#ifndef VDMTA_HH
#define VDMTA_HH

#include "MTA/DGC_MODULE.hh"
#include "MTA/Misc/Time/Timeval.hh"
#include "VDrive.hh"

struct VDriveDatum {
  Timeval Before;
  Timeval After;

  int MyCounter;
};

extern int mta_counter;

using namespace std;
using namespace boost;

class VDrive : public DGC_MODULE {
public:
  VDrive();
  ~VDrive();

  //  void Init();
  void Active();
  // void Standby();
  // void Shutdown();
  // void Restart();

  // string Status();

  void InMailHandler(Mail & ml);

private:
  VDriveDatum D;
};

#endif
