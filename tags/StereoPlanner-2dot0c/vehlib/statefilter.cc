/* statefilter.cc
 *
 * HH 03 Jan 04 
 * Provides imu angular updates and merges gps and magnetometer data
 */

#include <math.h>
#include "VState.hh"
#include "LatLong.h"
#include "frames/rot_matrix.hh"
#include "sparrow/dbglib.h"
#include "sparrow/display.h"
#include "statefilter.hh"

// counters to slow down updates of attitude by other sensors
// only reset if update occurs, so basically they wait until the time is up (roughly 1hz)
// then waits until the appropriate speed conditions are met before updating
int gps1hz = 0;
int mag1hz = 0;
int firstmag = 1;

// number of times to count before update attitude from either GPS or mag
#define GPS_UPDATE_HZ 10
#define MAG_UPDATE_HZ 10 

// propagate rotation matrix and back out angles with received deltathetas
void imu_update(rot_matrix &nb, IMU_DATA imu, VState_GetStateMsg &vstate)
{
  // propagate rotation matrix by new delta thetas
  nb.propagate(imu.dtx, imu.dty, imu.dtz);

  // update new euler angles (in radians)
  vstate.Pitch = nb.get_pitch();
  vstate.Roll = nb.get_roll();
  vstate.Yaw = nb.get_heading();

  //  dbg_info("P: %5.2f R: %5.2f Y: %5.2f \n", vstate.Pitch, vstate.Roll, vstate.Yaw);
  return;
}

// updates gps position, velocity, and heading
// also updates heading for imu rotation matrix
void gps_update(gpsDataWrapper &gps, VState_GetStateMsg &vstate, rot_matrix &nb)
{
  LatLong latlong(0, 0);

  // perform UTM conversion
  latlong.set_latlon(gps.data.lat, gps.data.lng);
  latlong.get_UTM(&vstate.Easting, &vstate.Northing);
  vstate.Altitude = -gps.data.altitude;

  // set speeds to zero if they're below the noise ceiling
  if (fabs(gps.data.vel_n) >= VEL_N_VALID_CUTOFF) {
    vstate.Vel_N = gps.data.vel_n;
  }else{
    vstate.Vel_N = 0;
  }
  if (fabs(gps.data.vel_e) >= VEL_E_VALID_CUTOFF) {
    vstate.Vel_E = gps.data.vel_e;
  }else{
    vstate.Vel_U = 0;
  }
  if (fabs(gps.data.vel_u) >= VEL_U_VALID_CUTOFF) {
    vstate.Vel_U = gps.data.vel_u;
  }else{
    vstate.Vel_U = 0;
  }

  // get 2d velocity magnitude
  vstate.Speed = sqrt(vstate.Vel_N * vstate.Vel_N + vstate.Vel_E * vstate.Vel_E);

  // get new yaw and reset IMU rotation matrix if speed is high enough
  //  if ((vstate.Speed >= GPS_VALID_CUTOFF)&&(gps1hz >= GPS_UPDATE_HZ)) {
  //vstate.Yaw = atan2(gps.data.vel_n, gps.data.vel_e);
  //nb.setheading(vstate.Yaw);
  //gps1hz=0; // reste 1hz counter
  //}
      gps1hz++;
  return;
}


// updates magnetometer data if velocity is above a certain limit
void mag_update(mag_data mag, VState_GetStateMsg &vstate, rot_matrix &nb)
{
  // mag has 100ms sleeps so it read the mag at roughly 10 hz
  // counter slows it down to 10 hz, but also checks for velocity
  if ((fabs(vstate.Speed) <= MAGNET_VALID_CUTOFF)&&(firstmag))// && mag1hz >= MAG_UPDATE_HZ) 
    {
      mag.heading += MAG_OFFSET;
      if (mag.heading >= 360)
	{
	  mag.heading -= 360;
	}
      vstate.Yaw = mag.heading * M_PI / 180;
      if (vstate.Yaw > M_PI)
	{
	  vstate.Yaw -= 2 * M_PI;
	}
      vstate.Pitch = mag.pitch * M_PI / 180;
      vstate.Roll = mag.roll * M_PI / 180;
      
      nb.setattitude(vstate.Pitch, vstate.Roll, vstate.Yaw);
      //nb.setattitude(mag.pitch, mag.roll, mag.heading);
      //    dbg_info("pitch: %f roll: %f yaw: %f \n", nb.get_pitch(), nb.get_roll(), nb.get_heading());
      mag1hz=0;
      firstmag == 0;
    }
  mag1hz++;
  return;
}
