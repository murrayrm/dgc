README for vehlib
RMM, 2 Jan 04

The vehlib directory contains the code required to run the Tahoe,
including the driving program (vdrive) and state estimator (vstate).
Most of the code here came from the team directory, where it was split
up into subdirectories.  The convention here is the opposite: all
hadware specific files needed to run vdrive, vstate, etc are contained
in this directory.

Besides source code, the following files are here:

* INSTALL: information on how to install this libraries and programs

* Changes: detailed list of changes that have been made to the code

* Pending: list of things that still need to be done

* vehlib.txi: programmers reference for vehlib + user documentation
  for vdrive and vstate.  This compiles into vehlib.{info,html}.

The vehlib code will not be updated any more except for critical bug fixes
because the embedded team is concentrating on getting a set of more 
robust, organized, and better working drivers out in vehlib-2.0 to
support the planning team's testing (our highest priority).  This has 
a few problems with making the file which are addressed below.

To compile vdrive, you will need to do the following:
Delete the team/lib and team/include library in your distribution.
Do a "cvs update -d" in the team library.  
Re-run the makefile (type make install) in team (to reinstall everything).


Coding conventions
------------------

Since functions in this directory have to run in a real-time,
non-supervised manner, there are a couple of conventions that have
been enforced:

* Printing: all code should use dbg_{warn,info,error,panic} for print
  out messages.  These functions log error messages to files as well
  as interface with the sparrow display library.

* Header files: the files in this library should not access any
  directory specific code (eg, ../serial/serial.h).  Instead, the
  source code should be placed in this directory and linked into the
  vehlib.a library.  Include files will go in $(DGC)/include/vehlib.
  This will allow portable use of the library if we change the file
  structure for the rest of the code.

* Changes: in addition to giving summary comments when committing
  changes to CVS, the file 'Changes' in this directory should be used
  to document at a higher level of detail.  Comments can (and should)
  also be placed at the tops of files that were changed, but the
  Changes file provides a convenient way to see all changes made to
  the library in a single location.

Steering motor controller troubleshooting (LBC, 18 Jan 04)
-----------------------------------------

When the steering motor fails, it is sometimes necessary to
reinitialize it manually.  To create a connection to the motor, use the
following commands (very cheap terminal emulator in unix):

cat < /dev/ttyS0 &
cat > /dev/ttyS0

Once you are talking to the motor, the following commands can be used

tasxf <-- tells us status

reset <-- reset motor controller
tasxf <-- status
drive1 <-- should re-enable drive

(iterate starting with reset)
(try pausing between iterations)

d5000
tpe
tpc
go

(iterate starting with reset)

