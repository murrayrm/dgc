#ifndef __COMPUTER__
#define __COMPUTER__

#include <string>
#include "IP.hh"
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include "../Time/Timeval.hh"
#include "ModuleInfo.hh"
#include "MailAddress.hh"


using namespace std;

struct Computer {

  string Name;
  IP_ADDRESS IP;

  boost::recursive_mutex Mutex;
  list<ModuleInfo> Modules;

  // Port Scan IP function
  void AutoUpdate();

  // When did we last update this computer?
  Timeval LastUpdate;

  Computer( string name, IP_ADDRESS ip ) 
    : Name(name), IP(ip), LastUpdate( TVNow() - Timeval(1000000,0)) {}

};


#endif
