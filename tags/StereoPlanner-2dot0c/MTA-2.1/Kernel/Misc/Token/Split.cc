#include <string>

#include "Split.hh"

using namespace std;

// Will trim spaces off of both ends of a string 
// and return the resulting string.
string& Trim(string in) {
  while(in[0] == ' ') {
    in = in.substr(1);
  }
  while(in[in.size()-1] == ' ') {
    in = in.substr(0, in.size()-1);
  }
  return in;
}


list<std::string> SplitAndTrim(std::string str, std::string split) {     
  list<std::string> ret;
  string x = str.find(split);
  
  // This function will add all of the left pieces of a string
  // for example, if str= "A: b:c" and split=":", this will add
  // "A" and "b".  Notice the trim. 
  while (x > 0) {
    left = trim(str.substr(0, x-1));
    str  = str.substr(x+split.size());

    x = str.find(split);
    ret.push_back(left);
  }

  // Now Tack on the Right End of the String
  ret.push_back( trim(str));
}
