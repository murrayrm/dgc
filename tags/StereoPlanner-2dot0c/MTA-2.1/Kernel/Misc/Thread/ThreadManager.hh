#ifndef ThreadManager_HH
#define ThreadManager_HH

namespace Thread {
  typedef void (*VoidFunctionPointer)(void *);
  
  struct ThreadReconstructPointers {
    VoidFunctionPointer p_fun;
    void *p_arg;
  };

  void EnqueueFunctionToExecute(ThreadReconstructPointers & t);


  void* ChildThreadMainLoop(void* arg);


  void StartNewThread();
  void TerminateThread(void*);

  void MaintainThreadCount(int tc);
  void Join();
  void ForceShutdown();
};

#endif
