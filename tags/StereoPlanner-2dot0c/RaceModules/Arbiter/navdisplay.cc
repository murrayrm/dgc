#include "ArbiterModule.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

// Added for display 1/31/04 
VState_GetStateMsg dispSS;
double dispSS_decimaltime;

double PitchDeg, RollDeg, YawDeg;
double PitchRateDeg, RollRateDeg, YawRateDeg;

double steerCommand;
double speedCommand;

double PHI[NUMARCS];
Voter LADAR_;
Voter GLOBAL_;
Voter STEREO_;
Voter STEREOLR_;
Voter DFE_;
Voter ARB_;
Voter PATH_; // path evaluation test module
Voter CAE_;
Voter FENDER_;
Voter ROAD_;

int UpdateCount = 0;
int LadarUpdateCount = 0;
int GlobalUpdateCount = 0;
int StereoUpdateCount = 0;
int StereoLRUpdateCount = 0;
int DFEUpdateCount = 0;
int PathEvaluationUpdateCount = 0;
int CorrArcEvalUpdateCount = 0;
int FenderPlannerUpdateCount = 0;
int RoadFollowerUpdateCount = 0;
int ArbiterUpdateCount = 0;

#include "vddtable.h"
int user_quit(long arg);

void Arbiter::UpdateSparrowVariablesLoop() {

  for(int i=0; i<NUMARCS; i++) PHI[i] = GetPhi(i);

  while( !ShuttingDown() ) {

    dispSS = d.SS;
    dispSS_decimaltime = (d.SS.Timestamp.sec()+d.SS.Timestamp.usec()/1000000.0);

    PitchDeg     = d.SS.Pitch     * 180.0 /M_PI;
    RollDeg      = d.SS.Roll      * 180.0 /M_PI;
    YawDeg       = d.SS.Yaw       * 180.0 /M_PI;
    PitchRateDeg = d.SS.PitchRate * 180.0 /M_PI;
    RollRateDeg  = d.SS.RollRate  * 180.0 /M_PI;
    YawRateDeg   = d.SS.YawRate   * 180.0 /M_PI;

    steerCommand = d.toVDrive.steer_cmd; 
    speedCommand = d.toVDrive.velocity_cmd;
    
    LADAR_   = d.allVoters[ArbiterInput::LADAR];
    GLOBAL_  = d.allVoters[ArbiterInput::Global];
    STEREO_  = d.allVoters[ArbiterInput::Stereo];
    STEREOLR_  = d.allVoters[ArbiterInput::StereoLR];
    DFE_     = d.allVoters[ArbiterInput::DFE];
    CAE_     = d.allVoters[ArbiterInput::CorrArcEval];
    FENDER_  = d.allVoters[ArbiterInput::FenderPlanner];
    ROAD_    = d.allVoters[ArbiterInput::RoadFollower];
    // added for path evaluator test module
    PATH_    = d.allVoters[ArbiterInput::PathEvaluation]; 
    ARB_     = d.combined;

    LadarUpdateCount          = d.UpdateCount[ArbiterInput::LADAR];
    StereoUpdateCount         = d.UpdateCount[ArbiterInput::Stereo];
    StereoLRUpdateCount         = d.UpdateCount[ArbiterInput::StereoLR];
    GlobalUpdateCount         = d.UpdateCount[ArbiterInput::Global];
    DFEUpdateCount            = d.UpdateCount[ArbiterInput::DFE];
    CorrArcEvalUpdateCount    = d.UpdateCount[ArbiterInput::CorrArcEval];
    FenderPlannerUpdateCount  = d.UpdateCount[ArbiterInput::FenderPlanner];
    RoadFollowerUpdateCount   = d.UpdateCount[ArbiterInput::RoadFollower];
    PathEvaluationUpdateCount = d.UpdateCount[ArbiterInput::PathEvaluation];
    ArbiterUpdateCount        = d.CombinedUpdateCount;

    UpdateCount ++;

    usleep(500000); // Wait a bit, because other threads will print some stuff out
  }

}

void Arbiter::SparrowDisplayLoop() {

  dbg_all = 0;

  if (dd_open() < 0) exit(1);
  dd_bindkey('Q', user_quit);

  dd_usetbl(vddtable);

  usleep(500000); // Wait a bit, because other threads will print some stuff out
  dd_loop();
  dd_close();

}

// TODO: Set the shutdown flag in the quit function
int user_quit(long arg)
{
  return DD_EXIT_LOOP;
}
