#include <sys/time.h>
#include <iomanip>
#include <algorithm>  // for min
#include <stdio.h>

#include "FenderPlanner.hh"

#include "fender.hh"

using namespace std;

// Reads in fender points from the specified file (hardcoded now)
// Fender point file format is:  Northing  Easting
int FenderPlanner::InitFender() 
{
  cout << "[InitFender] Starting InitFender()" << endl;
  cout << "[InitFender] d.fenderNum = "        << ArbiterInput::FenderPlanner << endl;

 // Make sure we update our state first 
  while( d.SS.Northing < 100000.0 ) 
  {
    printf("[InitFender] Stuck trying to update state...\n");
    UpdateState();
    // chill out.  
    usleep(500000);
  }

  RDDF rddf("myrddf.dat");
  sleep(1);

  return true;

}

void FenderPlanner::FenderShutdown() 
{

}
