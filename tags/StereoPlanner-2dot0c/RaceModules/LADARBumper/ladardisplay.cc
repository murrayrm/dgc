#include "LADARBumper.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

double PHI[NUMARCS];

extern int SIM;
extern int SEND_VOTES;
extern int GROW_OBSTACLES;
extern unsigned long SLEEP_TIME;
extern int LOG_SCANS;
extern int HALT_VEHICLE;
extern int QUIT_PRESSED;

Voter bumper;
Voter roof;

int UpdateCount = 0;
int TimeSec = 0;
int TimeUSec = 0;

double state_pitch,state_yaw,state_roll;
double state_e,state_n,state_a;
double state_speed;

#include "vddtable.h"
int user_quit(long arg);

void LADARBumper :: UpdateSparrowVariablesLoop() {
  for(int i = 0; i < NUMARCS; i++) PHI[i] = GetPhi(i);

  bumper = d.ladar;
  roof = d.ladar;

  TimeSec = d.SS.Timestamp.sec();
  TimeUSec = d.SS.Timestamp.usec();

  state_pitch = (d.SS.Pitch / (2 * M_PI)) * 360.0;
  state_yaw = (d.SS.Yaw / (2 * M_PI)) * 360.0;
  state_roll = (d.SS.Roll / (2 * M_PI)) * 360.0;
  state_n = d.SS.Northing;
  state_e = d.SS.Easting;
  state_a = d.SS.Altitude;
  state_speed = d.SS.Speed;

  UpdateCount ++;
}

void LADARBumper :: SparrowDisplayLoop() {

  dbg_all = 0;

  if (dd_open() < 0) exit(1);
  dd_bindkey('Q', user_quit);

  dd_usetbl(vddtable);

  cout << "Sparrow display should show up in 1 sec" << endl;
  sleep(1); // Wait a bit, because other threads will print some stuff out
  dd_loop();
  dd_close();
  QUIT_PRESSED = 1;
}

// TODO: Set the shutdown flag in the quit function
int user_quit(long arg)
{
  return DD_EXIT_LOOP;
}
