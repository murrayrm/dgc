% LG040123
%
% compute arc distance to obstacles
%
% This code shows how to compute arc distance to an obstacle
%
% Inputs for calculation:
%   - position of vehicle [UTM]
%   - yaw of vehicle      [clockwise from Northing]
%   - turning radius      [+ve for right-hand (clockwise) turn, -ve for left-hand turn]
%   - position of an obstacle [UTM - assumed to be computed by intersecting arc path with obstacle data]
%
% Outputs of calculation:
%   - arc distance traveled by vehicle till collision with obstacle
%
% Algorithm:
%
%   First a change of coordinates is made to a ref frame where
%   the calculations are simple.
%
%       This reference frame is that of the center of rotation of the arc,
%       with the vehicle along the positive x axis.
%
%       (x and y axii are defined such that Easting direction is that of x axis,
%        and Northing direction is that of y axis)
%
%       Note that for rightward turns, this means that in the new reference frame
%       the vehicle is pointing in the direction of the negative y axis,
%       and for leftward turns, it points in the direction of the postive y axis.
%
%       This coordinate transformation is done by
%           - converting UTM coords (Northing, Easting) to an x-y ref frame,
%               where +ve x is along Easting, and +ve y is along Northing
%           - translate coordinates so that the center of rotation is at the origin
%           - rotate by an amount that will put the vehicle along the +ve x axis
%
%   Next, the arc angle to the obstacle is calculated
%       - compute angle from x axis 
%           - for leftward turns, make sure angles lie in [0 to 2*pi] range
%           - for rightward turns, make sure angles lie in [-2*pi to 0] range
%               (since the vehicle is pointing along the -ve y axis and moves
%                counter-clockwise)
%       - abs(angle) is the arc angle of path traveled to collide with obstacle
%       - abs(angle) * turning radius is arc distance till collision.

% turning radius
% +ve for rightward turn
% -ve for leftward turn
TURN_RADIUS = -100;

% setup figure
    figure(100); clf;
    axis([0 600 0 400]);
    axis equal;
    hold on;
    xlabel('Easting');
    ylabel('Northing');
    title('UTM COORDS');

% get input from user
    fprintf('click on the vehicle position\n');
    vv = ginput(1);
    % vv coords read from screen as (Easting, Northing))
    pos.N = vv(2);
    pos.E = vv(1);
    plot(pos.E, pos.N, 'ro');

    yaw_degrees = input('Enter vehicle yaw in degrees: ');
    yaw = yaw_degrees*pi/180;

    % yaw_vector is the vector according to yaw
    % (ie, the unit vector of the direction the vehicle is pointing in
    yaw_vector.N = cos(yaw);
    yaw_vector.E = sin(yaw);

% plot vehicle on screen
    plot(pos.E, pos.N, 'ro');
    quiver(pos.E, pos.N, yaw_vector.E, yaw_vector.N,30);
    if TURN_RADIUS > 0
        fprintf('Vehicle is making a rightward turn of radius %f\n', TURN_RADIUS);
    else
        fprintf('Vehicle is making a leftward turn of radius %f\n', abs(TURN_RADIUS));
    end

% Compute position of turn center in UTM
    if TURN_RADIUS > 0
        % rightward turn
        yaw_of_turn_center = yaw + pi/2;
    else
        % leftward turn
        yaw_of_turn_center = yaw - pi/2;
    end

    vector_to_turn_center.N = cos(yaw_of_turn_center);
    vector_to_turn_center.E = sin(yaw_of_turn_center);

    turn_center.N = pos.N + abs(TURN_RADIUS)*vector_to_turn_center.N;
    turn_center.E = pos.E + abs(TURN_RADIUS)*vector_to_turn_center.E;

% plot turn center
    plot(turn_center.E, turn_center.N,'ks');

% plot complete arc (not in particular order)
    arc.N = turn_center.N + abs(TURN_RADIUS)*cos(0:.01:2*pi);
    arc.E = turn_center.E + abs(TURN_RADIUS)*sin(0:.01:2*pi);

    plot(arc.E, arc.N,'c')


% get collision points
% NOTE: right now, user has to click close to arc, 
    fprintf('Click on two collision points\n')

    oo1 = ginput(1);

    obstacle_1.N = oo1(2);
    obstacle_1.E = oo1(1);
    plot(obstacle_1.E, obstacle_1.N, 'r*');

    oo2 = ginput(1);

    obstacle_2.N = oo2(2);
    obstacle_2.E = oo2(1);
    plot(obstacle_2.E, obstacle_2.N, 'b*');

% Now compute coords in turn center ref frame
% We want to translate everthing by T so that turning center is at origin
% And then rotate around turning center origin by theta so that 
% the vehicle is on the x axis (different cases for left and rightward turns)
    % first, go from (Northing, Easting) to an (x,y) ref frame
        pos_xy = [pos.E; pos.N];
        turn_center_xy = [turn_center.E; turn_center.N];
        obstacle_1_xy = [obstacle_1.E; obstacle_1.N];
        obstacle_2_xy = [obstacle_2.E; obstacle_2.N];
        
    % Compute necessary translation
        T = - turn_center_xy;

    % Compute necesary rotation
        if TURN_RADIUS > 0 
            % rightward turn
            theta = -yaw + pi;
        else
            theta = -yaw;
        end

        theta = -theta;
        R = [ cos(theta) -sin(theta);
              sin(theta)  cos(theta)];

    % apply transformation
        pos_O = R*(pos_xy + T);
        obstacle_1_O = R*(obstacle_1_xy + T);
        obstacle_2_O = R*(obstacle_2_xy + T);

    % As sanity check, compute also the yaw_vector of the vehicle in the new ref frame
        % first compute yaw in new ref frame
            % first compute yaw in (x,y) coordinates from (Northing, Easting)
            % this means change the handedness of the angle
            % (+ve counter-clockwise instead of clockwise)
            % and add pi/2 since N direction is the y axis
            yaw_xy = -yaw + pi/2;

        % now apply same rotation as to all the points
            yaw_O = yaw_xy + theta;


% plot in new turning center ref frame
    figure(200); clf; hold on;

    plot(pos_O(1), pos_O(2), 'ro');
    quiver(pos_O(1), pos_O(2), cos(yaw_O), sin(yaw_O), 30);

    plot(obstacle_1_O(1), obstacle_1_O(2), 'r*');
    plot(obstacle_2_O(1),obstacle_2_O(2), 'b*');
    plot(0,0,'ks');

    arc_x = TURN_RADIUS*cos(0:.01:2*pi);
    arc_y = TURN_RADIUS*sin(0:.01:2*pi);
    plot(arc_x, arc_y, 'c');

    axis equal;
    title('Turn Center Ref Frame');

% compute arc angles (and arc distance) to obstacles, and find first collision
    obstacle_arc_angle_1 = atan2(obstacle_1_O(2), obstacle_1_O(1));
    obstacle_arc_angle_2 = atan2(obstacle_2_O(2), obstacle_2_O(1));



    % First, fix angles to be either in [0 2*pi] or [-2*pi 0] range
    if TURN_RADIUS > 0
        % rightward turn
        % that means make positive angles negative
        if (obstacle_arc_angle_1 > 0)
            obstacle_arc_angle_1 = obstacle_arc_angle_1 - 2*pi;
        end
        if (obstacle_arc_angle_2 > 0)
            obstacle_arc_angle_2 = obstacle_arc_angle_2 - 2*pi;
        end
    else
        % leftward turn
        % make negative angles positive
        if (obstacle_arc_angle_1 < 0)
            obstacle_arc_angle_1 = obstacle_arc_angle_1 + 2*pi;
        end
        if (obstacle_arc_angle_2 < 0)
            obstacle_arc_angle_2 = obstacle_arc_angle_2 + 2*pi;
        end
    end

    obstacle_dist_1 = abs(obstacle_arc_angle_1)*abs(TURN_RADIUS);
    obstacle_dist_2 = abs(obstacle_arc_angle_2)*abs(TURN_RADIUS);

    fprintf('Distance to obstacle 1 (red  *) is %f\n', obstacle_dist_1);
    fprintf('Distance to obstacle 2 (blue *) is %f\n', obstacle_dist_2);

    % find closet point! plot a green circle around it
    if (abs(obstacle_arc_angle_2) > abs(obstacle_arc_angle_1))
        % obstacle 1 is closest!
        plot(obstacle_1_O(1),obstacle_1_O(2),'go','MarkerSize',10);
        fprintf('Obstacle 1 is closest!\n');
    else
        plot(obstacle_2_O(1),obstacle_2_O(2),'go','MarkerSize',10);
        fprintf('Obstacle 2 is closest!\n');
    end
