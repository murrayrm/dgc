#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "global_map.hh"
#include "global_functions.cc"
//horrible, horrible hardcoded hack for SIZE.  will fix later
#define SIZEX 1629  
#define SIZEY 2395

/*global_map.cc
18-jan-04   Alan Somers     creation*/

/*This is presently just a test program for the following few functions.
It will send an ASCII pgm file to stdout.  Proper usage is:
./global_map_test  myfile.pgm
*/

inline bool pix_is_eq(pixel A, pixel B) {return (A.Y == B.Y) && (A.X == B.X);}

pixel Vec2pix(Vector x) {
    pixel pix;
    pix.Y = (int) x.N;
    pix.X = (int) x.E;
    return (pix); }

int fill_map(unsigned char map[SIZEX*SIZEY],  unsigned char color){
        memset(&map[0], color, SIZEX*SIZEY); 
    return(0);}    

 int draw_circle(unsigned char map[SIZEX*SIZEY], pixel center, int  r,
     unsigned char color){
    for (int i = center.Y - r; i <= center.Y + r; i++){
        int w = (int)sqrt((float) (r*r - (center.Y - i)*(center.Y - i)));
        memset(&map[i*SIZEX] + center.X - w, color, 2 * w + 1); } 
    return(0); }
            
/*here is a picture of my "rectangle", actually a convex quadrilateral
             1              
            /a\             ------>
           /aaa\            |    X
          4aaaaa\           |
           \bbbbb\          V  Y
            \bbbbb2
             \ccc/
              \c/
               3
The points may be specified in any order, so long as they are clockwise.

this code should work for any parallelogram, and some other quadrilaterals
it will even work for some concave quadilaterals.  it will not work for a
quadrilateral in which opposing points are not at the extents of the figure.
for example, if "2" were below "3" in the above figure, this function would
not work*/
int draw_rectangle(unsigned char map[SIZEX*SIZEY], pixel q1, pixel q2, 
    pixel q3, pixel q4, unsigned char color) {
    //rotate point definitions
    pixel mh, ml;
    pixel top = (q1.Y <= q2.Y ? q1 : q2);
    top = (top.Y <= q3.Y ? top : q3);
    top = (top.Y <= q4.Y ? top : q4);
    if (pix_is_eq(top, q2)) {
            top = q1;   //now top is being used as a temp variable
            q1 = q2; q2 = q3; q3 = q4; q4 = top; }
    else if (pix_is_eq(top, q3)) {
            top = q1;  //top is temp
            q1 = q3;
            q3 = q2;   //q3 is a temp variable
            q2 = q4;
            q4 = q3;   //this is really the old q2 
            q3 = top; }
    else if (pix_is_eq(top, q4)) { 
            top = q1;   //top is temp
            q1=q4; q4=q3; q3=q2; q2=top; }
    if (q4.Y <= q2.Y) {
        mh = q4; ml = q2;}
    else {
        mh = q2; ml = q4;} 
    //draw region a
    for (int y=q1.Y; y<mh.Y; y++) {
        int left_edge=(int)((float)(y-q4.Y)*(float)(q1.X-q4.X)/
            (float) (q1.Y-q4.Y)+(float)q4.X);
        int right_edge=(int)((float)(y-q1.Y)*(float)(q1.X-q2.X)/
            (float)(q1.Y-q2.Y)+(float)q1.X);
        memset(&map[y*SIZEY]+left_edge, color, right_edge-left_edge); }
    //draw region b
    if (q4.Y <= q2.Y ) {
        for (int y=q4.Y; y<q2.Y; y++) {
            int left_edge=(int)((float)(y-q3.Y)*(float)(q4.X-q3.X)/
                (float) (q4.Y-q3.Y)+(float)q3.X);
            int right_edge=(int)((float)(y-q1.Y)*(float)(q1.X-q2.X)/
                (float)(q1.Y-q2.Y)+(float)q1.X);
            memset(&map[y*SIZEY]+left_edge, color, right_edge-left_edge); }}
    else {
        for (int y=q2.Y; y<q4.Y; y++) {
           int left_edge=(int)((float)(y-q4.Y)*(float)(q1.X-q4.X)/
                (float) (q1.Y-q4.Y)+(float)q4.X);
           int right_edge=(int)((float)(y-q2.Y)*(float)(q2.X-q3.X)/
                (float)(q2.Y-q3.Y)+(float)q2.X);
           memset(&map[y*SIZEY]+left_edge, color, right_edge-left_edge); }}
    //draw region c
    for (int y=ml.Y; y<q3.Y; y++) {
        int left_edge=(int)((float)(y-q3.Y)*(float)(q4.X-q3.X)/
            (float) (q4.Y-q3.Y)+(float)q3.X);
        int right_edge=(int)((float)(y-q2.Y)*(float)(q2.X-q3.X)/
            (float)(q2.Y-q3.Y)+(float)q2.X);
        memset(&map[y*SIZEY]+left_edge, color, right_edge-left_edge); }
    }

#if 0
int main(){
    ofstream outfile("corridor.pgm");
    unsigned char* map ;
    map=(unsigned char*)malloc(SIZEX*SIZEY);
    long int minSize = SIZEX < SIZEY ? SIZEX : SIZEY;
    pixel q1, q2, q3, q4, center;
    q1.Y=SIZEY/10;      q1.X = SIZEX/2;
    q2.Y=(SIZEY*1)/4;   q2.X = (SIZEX*3)/4;
    q3.Y=(SIZEY*9)/10;  q3.X = (SIZEX*3)/5;
    q4.Y=(SIZEY*3)/4;       q4.X = SIZEX/4;
    center.Y = SIZEY/3; center.X = SIZEX/3;
    fill_map(map, 100);
    draw_circle(map, center, minSize/4, 0);
    draw_rectangle(map, q2, q3, q4, q1, 50); 
    q1.Y=(SIZEY/4);     q1.X = SIZEX/4;
    q2.Y=SIZEY/4;       q2.X = (SIZEX*3)/4;
    q3.Y=(SIZEY*3)/4;   q3.X = (SIZEX*3)/4;
    q4.Y=(SIZEY*3)/4;   q4.X = (SIZEX) /4;
    draw_rectangle(map, q1, q2, q3, q4, 150);
    outfile << "P2\n" << SIZEX << " " << SIZEY << "\n255\n";
    for (int i=0; i<SIZEY; i++) {
        for (int j=0; j<SIZEX; j++) {
            outfile << int(map[i*SIZEY + j]) << " ";}
        outfile << "\n"; }
    free(map);
    return(0); }
#endif  
