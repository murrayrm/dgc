[============GLOBAL PATH PLANNER============]
[Alan Somers         somers@its             ]
[Rocky Velez         rocky@its              ]
[Les White           lawhite@jpl.nasa.gov   ]
[===========================================]

This file explains the operation of the Global Path Planner code.

PURPOSE:
    The Global Path Planner is responsible for keeping the vehicle within the
corridor and headed toward the finish.  It is responsible for maintaining the
speed limits.  If we ever care about passing through the phase lines on time,
Global Planner will do that (or at least try).  The Global Path Planner is
responsible for processing static maps, and using that data to choose the best path over the next few km that stays within the corridor.

METHOD:

--------------GLOBAL PATH PLANNER-------------------
This MTA module works off the vehicle state and the waypoint definition file
to keep the vehicle on track.  Eventually, it will make its votes based on
the decisions of the Global Map Evaluator.

-------------GLOBAL MAP EVALUATOR-------------------
This [unimplemented] MTA module uses the current vehicle state and the static
maps to plan a long-term path for the vehicle.  It should consider at least
the next several km.  It will use the D* algorithm, and run at full speed on
its own computer.

--------------GlobalPlannerMain.cc-------------------
    This file runs first when GlobalPlanner is executed.  It does some very
simple input checking and then starts the MTA module.

--------------GlobalPlanner.cc-----------------------
    This file contains the functions called by the MTA.  The MTA functions are
Init()      calls InitGlobal in global.cc
Active()    gets vehicle state from the MTA and sends votes back to the MTA.
            Also, this function sleeps for 100ms to set the timing of the
            module.  
            Eventually, this function will recieve updated paths from the
            Global Map Evaluator, which will be a separate module.
Shutdown()  Stops the Global Path Planner nicely.

--------------global.cc----------------------------
    This file contains most functions called by GlobalPlanner.cc
InitGlobal()    This function reads in the waypoint file and sets up the
                waypoint class.  Eventually it will do any other needed
                initialization, such as created stub Global Map Evaluator
                data.
GlobalShutdown()    Does just what it sounds like

-------------GlobalUpdateVotes.cc------------------
    This file actually calculates the goodness of the various arcs.  It
contains just one function, GlobalUpdateVotes().  Based on the vehicle state,
It decides which waypoint to shoot for and calculates goodnesses and speeds.
When the Final waypoint is reached, it shuts down the MTA Kernel.
 02/08/2004  Modified to evaluate withinWaypoint using the front of the vehicle
             as opposed to the rear axle. (Sue Ann Hong)

-------------GlobalUpdateVotes2.cc----------------
    This file does all that the previous does, but it also includes corridor
intelligence.  It will compile to a different target, GlobalPlanner2.

-------------GlobalUpdateVotes3.cc----------------
    This file does all that the previous one does, but it also includes Global
Map Evaluator output in its decisions.  It will compile to the target
GlobalPlanner3

-------------global_functions.cc-------------------
    This file contains several, mainly mathematical functions.
calc_steer_command      This calculates the best steering angle, from a
                        strictly waypoint following point of view.  The
                        calling function, probably GlobalUpdateVotes(), will
                        use this info to generate goodnesses for other arcs.
calc_speed_command      This functions determines the maximum speed the
                        vehicle should take to achieve good route tracking.
                        It is arc independent.
few other minor funcs   Self Evident.

-------------waypoints.cc--------------------------
    This file implements the waypoints class.  It includes code to read in
a waypoints file saved on disk, as well as code to manage this list of
waypoints.  Presently is uses BOB format, but should be switched to RDDF.
