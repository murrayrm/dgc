/*
** get_vstate.cc
**
**  
** 
**
*/

#include "LADARMapper.hh" // standard includes, arbiter, datum, ...

void LADARMapper::UpdateState()
{ 
  Mail msg = NewQueryMessage( MyAddress(), MODULES::VState, VStateMessages::GetState);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> d.SS; // download the new state
  }

} // end WaypointNav::UpdateState() 
