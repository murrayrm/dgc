#include "VManage.hh"

#include <iostream>
using namespace std;

extern int DEBUG_ON;
extern int ERR_OUT;

string Prompt = "Enter a command or press \'h\' for help.";

string HelpMenu = 
string( "-----VManage Console Options-----\n") + 
string( "h: Print Help Screen\n" ) +
string( "z: Print VManage Status String\n" ) +
string( "p: Set Keyboard EStop Pause\n" ) +
string( "g: Set Keyboard EStop Go (i.e. Remove Keyboard EStop)\n" ) +
string( "d: Set EStop Deceleration Rate: (0 for do not use this feature)\n")+
string( "s: Change Steer Mode\n" )+
string( "a: Change Accel Mode\n" )+
string( "t: Set Transmission Gear\n" )+
string( "q: quit\n") +
string( "--------------------------------------------------");


void VManage::ConsoleLoop() {

  char cmd;

  char myChar;
  int myInt;
  double myDouble;

  sleep ( 1 );


  while( !ShuttingDown() ) {
    cout << endl << Prompt << endl;
    cin >> cmd;
    
    switch ( cmd ) {
    case 'h': case 'H':  cout << HelpMenu << endl;  break;
    case 'z': case 'Z':  cout << Status() << endl;  break;
    case 'p': case 'P':
      cout << "Setting Keyboard EStop Pause" << endl;
      d.KeyboardEStopOn = true;
      //      d.s.vdrive.estop.EStop = 'p';
      break;
    case 'g': case 'G':
      cout << "Setting Keyboard EStop Go" << endl;
      if( d.KeyboardEStopOn == true) {
	d.KeyboardEStopOn = false;
	d.EStopGoTime = TVNow();
      }
      break;
    case 'd': case 'D':
      cout << "Current Deceleration Rate = " 
	   << setprecision( 4 )
	   << d.g.EStopDecelerationRate << " m/s."
	   << " Please choose a new value (0 for do not use this feature:" 
	   << endl;
      cin >> myDouble;
      d.g.EStopDecelerationRate = myDouble;
      break;
    case 's': case 'S':
      cout << "Enter o,a,m,r (off, auto, manual, remote) to change steer mode." << endl;
      cin >> myChar;
      switch (myChar) {
      case 'o': case 'O': case 'a': case 'A': 
      case 'm': case 'M': case 'r': case 'R':
	d.s.vdrive.smode = myChar;
	break;
      default:
	cout << "Invalid Option (" << myChar << ")" << endl; break;
      }
      break;
    case 'a': case 'A':
      cout << "Enter o,a,m,r (off, auto, manual, remote) to change accel mode." << endl;
      cin >> myChar;
      switch (myChar) {
      case 'o': case 'O': case 'a': case 'A': 
      case 'm': case 'M': case 'r': case 'R':
	d.s.vdrive.amode = myChar;
	break;
      default:
	cout << "Invalid Option (" << myChar << ")" << endl; break;
      }
      break;

    case 't': case 'T':
      cout << "Enter -1 for reverse or 3 for 3rd gear." << endl;
      myInt  = 0;
      cin >> myInt;
      if ( myInt == -1 || myInt == 3 ) {
	d.g.SetTransmissionGear = myInt;
      } else cout << "Invalid Gear" << endl;
      break;
 
    case 'q': 
      exit(0); 
      break;
    default:
      cout << "Invalid option (" << cmd << ")" << endl;
    }
  }
}


