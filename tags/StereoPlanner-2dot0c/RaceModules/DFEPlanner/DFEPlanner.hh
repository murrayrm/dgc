#ifndef DFEPLANNER_HH
#define DFEPLANNER_HH


#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "DFEPlannerDatum.hh"


using namespace std;
using namespace boost;


class DFEPlanner : public DGC_MODULE {
public:
  // and change the constructors
  DFEPlanner();
  ~DFEPlanner();

  // The states in the state machine.
  // Uncomment these if you wish to define these functions.

  void Init();
  void Active();
  //  void Standby();
  void Shutdown();
  //  void Restart();

  // The message handlers. 
  //  void InMailHandler(Mail & ml);
  //  Mail QueryMailHandler(Mail & ml);

  // the status function.
  //string Status();


  // Any functions which run separate threads
  
  // Method protocols
  int  InitDFE();
  void DFEUpdateVotes();
  void DFEShutdown();

  void UpdateState(); // grabs from VState and stores in datum.

private:
  // and a datum named d.
  DFEPlannerDatum d;
};



// enumerate all module messages that could be received
namespace DFEMessages {
  enum {
    // we are not yet receiving messages.
    // A place holder
    NumMessages
  };
};






#endif
