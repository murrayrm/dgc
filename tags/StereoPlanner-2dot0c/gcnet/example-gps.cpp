#include "example-gps.h"
#include "GcnetConnection.hpp"
#include <math.h>
#include <assert.h>
#include <unistd.h>		/* for sleep() */
#include <string.h>

#define USE_RELIABLE_TRANSPORT	false

// Technically, deriving from GcnetConnection is unnecessary for
// transmit-only things.  However, if you ever want the GPS to receive data
// then this will be useful.
struct GPSConnection : public GcnetConnection
{
};

int main(int argc, char **argv)
{
  bool use_reliable = false;

  for (int i = 1; i < argc; i++)
    {
      if (strcmp (argv[i], "--reliable") == 0)
	use_reliable = true;
    }

  // TODO: interface with the GPS unit.  For now, fabricate a circular
  // walk around the origin.
  double theta = 0.0;
  GPSConnection connection;
  assert(connection.connect(ARBITER_HOSTNAME, GPS_PORT));
  for (;;)
    {
      GPSReading reading;
      reading.n = sin(theta);
      reading.e = cos(theta);
      reading.speed = 3;
      reading.heading = theta;  // WRONG
      connection.transmit((void*)&reading, sizeof(reading), use_reliable);
      sleep(1);
      theta += 0.01;
    }
  return 1;
}
