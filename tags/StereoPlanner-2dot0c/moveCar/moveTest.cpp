//compile: g++ moveTest.cpp ../parallel/parallel.c accel.c -o blah
#include <iostream>
#include "../parallel/parallel.h"
#include "accel.h"
#include "TLL.cc"

using namespace std;

int main() {
  char cont = 0;
  float disp = 0;

  //if(initBrakeSensors() != 0) {
  if (init_accel() != 1) {
    cout << "Error opening parallel port/initializing" << endl;
    return -1;
  }

  // enable latch OE -- no more cuz it's gnd'd
  //pp_set_bits(tllpport, 0, PP_PIN17);

  do {
    cout << "1 : brake force sensor" << endl;
    cout << "t : brake force sensor, log" << endl;
    cout << "2 : linear pot" << endl;
    cout << "p : linear pot, log" << endl;
    cout << "3 : move gas actuator" << endl;
    cout << "q : quit" << endl;
    cin >> cont;
    
    if (cont == '1') 
      cout << getBrakeForce() << "lbs" << endl;
    else if (cont == 't') { 
      FILE *log = fopen("tlllog.txt", "a+");
      fprintf(log, "%f lbs\n", getBrakeForce);
      fclose(log);
      cout << "logging..." << endl;
    }
    else if (cont == '2')
      cout << getBrakePot() << endl;
    else if (cont == 'p') {
      FILE *plog = fopen("potlog.txt", "a+");
      fprintf(plog, "proportional position: %f\n", getBrakePot());
      fclose(plog);
      cout << "logging..." << endl;
    }
    else if (cont == '3') { 
      cout << "desired absolute position: ";
      cin >> disp;
      cout << endl;
      set_accel_abs_pos(disp);
    }
  } while (cont != 'q');

  pp_set_bits(tllpport, PP_PIN17, 0);      // disable latch OE
  pp_end(tllpport);
  return 0;
}
