
#include "genMapStruct.hh"


Mail& operator << (Mail& ml, genMapStruct& rhs) // send the struct
{  
  // redirect the struct to the mail message
  // this should be in the same order as below
  ml << rhs.row_res << rhs.col_res 
     << rhs.leftBottomUTM_Northing << rhs.leftBottomUTM_Easting 
     << rhs.leftBottomRow << rhs.leftBottomCol
     << rhs.numRows << rhs.numCols;


  int i, j;
  for(i=0; i<rhs.numRows; i++ ) {
    for( j=0; j<rhs.numCols; j++) {
      ml << (*rhs.cell(i,j));
    }
  }


  return ml;
}


Mail& operator >> (Mail& ml, genMapStruct& rhs) // recover the struct
{
  // redirect the struct to the genMapStruct
  // this should be in the same order as above
  ml >> rhs.row_res >> rhs.col_res 
     >> rhs.leftBottomUTM_Northing >> rhs.leftBottomUTM_Easting 
     >> rhs.leftBottomRow >> rhs.leftBottomCol
     >> rhs.numRows >> rhs.numCols;

  //  printf(" in >> operator of genMapStruct: rhs.numRows = %d\n", rhs.numRows );
  
  rhs.Allocate( rhs.numRows, rhs.numCols );

  int i, j;
  for(i=0; i<rhs.numRows; i++ ) {
    for( j=0; j<rhs.numCols; j++) {
      ml >> (*rhs.cell(i,j));
    }
  }

  //  printf(" in >> operator of genMapStruct: rhs.numRows = %d\n", rhs.numRows );

  return ml;
}



void ConvertGenMapToStruct( genMap& gm, genMapStruct& gms ) 
{
  // Must allocate the data for the genMap struct first
  // numRows and numCols get set in this step
  // You should be asking "Where does this get deallocated?"
  gms.Allocate(gm.getNumRows(), gm.getNumCols());

  //printf("gm.getRowRes() = %f\n", gm.getRowRes() );

  // copy the easy variables
  gms.row_res = gm.getRowRes();
  gms.col_res = gm.getColRes();
  gms.leftBottomUTM_Northing = gm.getLeftBottomUTM_Northing();
  gms.leftBottomUTM_Easting  = gm.getLeftBottomUTM_Easting();
  gms.leftBottomRow = gm.getLeftBottomRow();
  gms.leftBottomCol = gm.getLeftBottomCol();
	
  // copy the genMap data to the struct
  for( int i=0; i < gm.getNumRows(); i++ ) {
    for( int j=0; j < gm.getNumCols(); j++ ) {
      (*gms.cell(i,j)) = gm.getCellDataRC(i, j).value;
    }
  }
}

