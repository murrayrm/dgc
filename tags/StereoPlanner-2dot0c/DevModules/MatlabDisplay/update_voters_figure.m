% LG040220
% Script used by MatlabDisplay.cc 
% to update the figure that plots
% all the votes that the arbiter receives
% (see also initialize_voters_figure.m)

% Let's let  whatever calls this decide what axis handle to put it in. -Lars
% figure(VOTES_FIGURE);

for i = 1:NUM_VOTERS
    set(h_voters_goodness(i), 'YData', voter_goodness(i,:));
    set(h_voters_velocity(i), 'YData', voter_velocity(i,:));
end

set(h_combined_goodness, 'YData', combined_goodness);
set(h_combined_velocity, 'YData', combined_velocity);

% set(h_best_goodness(i), 'XData', arbiter_best_arc, 'YData', voter_goodness(end, arbiter_best_arc));
% set(h_best_velocity(i), 'XData', arbiter_best_arc, 'YData', voter_velocity(end, arbiter_best_arc));

drawnow;
