/*
** get_vstate.cc
**
**  
** 
**
*/

#include "ReactionTime.hh" // standard includes, arbiter, datum, ...

void ReactionTime::UpdateState()
{ 
  Mail msg = NewQueryMessage( MyAddress(), MODULES::VState, VStateMessages::GetState);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> d.SS; // download the new state
  }

} // end ReactionTime::UpdateState() 
