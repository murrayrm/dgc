#include "StereoImageLogger.hh"
#include "stereo.hh"

using namespace std;

int SIM;         // this is a global variable that modules use to keep track of 
                 //  whether the code should be run in simulation mode
int DISPLAY;     // this is a switch that determines whether we're using 
                 //  ncurses display mode
int PRINT_VOTES; // whether each of the voters will call printVotes
                 // functionality not yet implemented              

char left_file_name_prefix[200], right_file_name_prefix[200], left_file_name_suffix[40], right_file_name_suffix[40], file_type[10], state_file_name[256];
int max_frames=MAX_FRAMES_DEFAULT, test_num, verbose=0, prompt=0, delay=0, forever=0, which_pair=SHORT_RANGE;

int main(int argc, char **argv) {


  //Set defaults for filenames
  strcpy(left_file_name_prefix, LEFT_FILE_NAME_PREFIX_DEFAULT);
  strcpy(left_file_name_suffix, LEFT_FILE_NAME_SUFFIX_DEFAULT);
  strcpy(right_file_name_prefix, RIGHT_FILE_NAME_PREFIX_DEFAULT);
  strcpy(right_file_name_suffix, RIGHT_FILE_NAME_SUFFIX_DEFAULT);
  strcpy(file_type, FILE_TYPE_DEFAULT);


  cout << "argc = " << argc << endl;
   
  SIM = 0;
  DISPLAY = 0;
  PRINT_VOTES = 0;
  ////////////////////////////
  // do some argument checking
  if( argc > 1 ) { // if we have arguments
  
    for( int i=1; i < argc; i++ ) { 
      // go through each of the arguments and check...
      cout << "argv[ " << i << "] = " << argv[i] << endl;

      // if the "-sim" argument was specified
      if( strcmp(argv[i], "-sim") == 0 ) {
        cout << "!!! -sim flag acknowledged, using simulation environment !!!" << endl << endl;
        SIM = 1;
      }
      // if the "-pv" argument was specified
      if( strcmp(argv[i], "-pv") == 0 ) {
        cout << "!!! -pv flag acknowledged, will print votes slowly !!!" << endl << endl;
        PRINT_VOTES = 1;
      }
      // if the "-disp" argument was specified
      if( strcmp(argv[i], "-disp") == 0 ) {
        cout << "!!! -disp flag acknowledged, about to use curses display environment !!!" << endl << endl;
        DISPLAY = 1;
      }

    if(strcmp(argv[i], "-help")==0 || strcmp(argv[i], "--help")==0) {
      printf("\nUsage instructions");
      printf("\nThere are four ways to name the logged images files: ");
      printf("\nDefault:                          Default0000-L.pgm,   Default0000-R.pgm");
      printf("\n-lname leftname -rname rightname: leftname0000.pgm,    rightname0000.pgm");
      printf("\n-name somename:                   somename0000-L.pgm, somename0000-R.pgm");
      printf("\n-num 42:                          Test42-0000-L.pgm,   Test42-0000-R.pgm");
      printf("\n-cal:                             cal0000-L.bmp,       cal0000-R.bmp (-c also works)");
      printf("\n-type bmp:                        Logs the images as a .bmp, -t also works)");
      printf("\n(-b works as -type bmp, to log jpgs, see command below)");
      printf("\n-compress                         Logs the images as .jpeg");
      printf("\nAdditional Commands:");
      printf("\n-max num:   Captures a maximum of num frames (-m also works)");
      printf("\n-delay num: Introduces a wait of delay ms between captures(-d also works)");
      printf("\n-forever:   Runs the capture forever");
      printf("\n-verbose:   Runs in verbose mode (-v also works)");
      printf("\n-prompt:    Prompts the user to get each image pair (-p also works)");
      printf("\n-long:      Use the long-range cameras (-l also works)");
      printf("\n-highres:   Use the old (high-res, Kevin Watson) cameras (-h also works)");
      printf("\n");
      return 0;
    }
    if(strcmp(argv[i], "-max")==0 || strcmp(argv[i], "-m")==0) {
      i++;
      max_frames = atoi(argv[i]);
    }
    if(strcmp(argv[i], "-delay")==0 || strcmp(argv[i], "-d")==0) {
      i++;
      delay = atoi(argv[i])*1000;
    }
    if(strcmp(argv[i], "-prompt")==0 || strcmp(argv[i], "-p")==0) {
      prompt = 1;
    }
    if(strcmp(argv[i], "-verbose")==0 || strcmp(argv[i], "-v")==0) {
      verbose = 1;
    }
    if(strcmp(argv[i], "-forever")==0 || strcmp(argv[i], "-f")==0) {
      forever = 1;
    }
    if(strcmp(argv[i], "-type") == 0 || strcmp(argv[i], "-t")==0) {
      i++;
      if(strcmp("jpg", argv[i]) == 0) {
	printf("\nJPG logging has been disabled since it does not create images suitable for later processing");
	printf("\n(Compressed images introduce major artifacts into processing.)");
	printf("\nTo log JPGs for non-processing purposes, use -compress\n");
	return 0;
      }
      strcpy(file_type, argv[i]);
    }
    if(strcmp(argv[i], "-b") == 0) {
      strcpy(file_type, "bmp");
    }
    if(strcmp(argv[i], "-j") == 0) {
      //strcpy(file_type, "jpg");
      printf("\nQuick JPEG logging has been disabled");
      return 0;
    }
    if(strcmp(argv[i], "-long")==0 || strcmp(argv[i], "-l")==0) {
      which_pair = LONG_RANGE;
    }
    if(strcmp(argv[i], "-highres")==0 || strcmp(argv[i], "-h")==0) {
      which_pair = HIGH_RES;
    }
    if(strcmp(argv[i], "-compress") == 0) {
      strcpy(file_type, "jpg");
    }
    if(strcmp(argv[i], "-lname")==0) {
      i++;
      strcpy(left_file_name_prefix, argv[i]);
      strcpy(left_file_name_suffix, ".pgm");
    }
    if(strcmp(argv[i], "-rname")==0) {
      i++;
      strcpy(right_file_name_prefix, argv[i]);
      strcpy(right_file_name_suffix, ".pgm");
    }
    if(strcmp(argv[i], "-num")==0) {
      i++;
      test_num = atoi(argv[i]);
      sprintf(left_file_name_prefix, "Test%d-", test_num);
      sprintf(right_file_name_prefix, "Test%d-", test_num);
    }
    if(strcmp(argv[i], "-name")==0) {
      i++;
      sprintf(left_file_name_prefix, "%s", argv[i]);
      sprintf(right_file_name_prefix, "%s", argv[i]);
    }
    if(strcmp(argv[i], "-cal")==0) {
      sprintf(left_file_name_prefix, "cal");
      sprintf(right_file_name_prefix, "cal");
      sprintf(file_type, "bmp");
    }
    }
  }
    
  // Display a message if we don't see some options set  
  if( SIM == 0 ) {
    cout << endl;
    cout << "!!! No -sim flag detected, using real environment !!!" << endl;
    cout << "    You can Ctrl-c here if you forgot to use -sim " << endl 
	 << endl;
  }
  if( DISPLAY == 0 ) {
    cout << endl;
    cout << "!!! No -disp flag detected, using stdout for display !!!" 
	 << endl;
    cout << "    You can Ctrl-c here if you forgot to use -disp " << endl 
	 << endl;
    DISPLAY = 0;
  }
  // Done doing argument checking.
  ////////////////////////////////
  
  
  // Start the MTA By registering a module
  // and then starting the kernel
  
  Register(shared_ptr<DGC_MODULE>(new StereoImageLogger));
  StartKernel();
  
  return 0;
}

