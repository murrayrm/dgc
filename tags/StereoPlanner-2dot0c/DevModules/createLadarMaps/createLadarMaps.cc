#include "genMap.hh"
#include "frames/frames.hh"
//all constants come from here, sensor offsets and angles, serial
//ports, you name it...
#include "Constants.h"
#include <float.h>
#include <getopt.h>

FILE* scanlog;           // Log files for various data
FILE* statelog;

VState_GetStateMsg SS;

frames ladarFrame;
genMap ladarTerMap;

void UpdateMap(int,double,double,double);
int addedScans = 0;

// COMMAND LINE OPTIONS
char *file,*testname,*timestamp,*matlab;
double start = 0.0,stop = DBL_MAX;
int map_rows = DEFAULT_LADARMAP_NUM_ROWS;
int map_cols = DEFAULT_LADARMAP_NUM_COLS;
double map_row_res = DEFAULT_LADARMAP_ROW_RES;
double map_col_res = DEFAULT_LADARMAP_COL_RES;

double offsetx = ROOF_LADAR_OFFSET_X;
double offsety = ROOF_LADAR_OFFSET_Y;
double offsetz = ROOF_LADAR_OFFSET_Z;
double pitch = ROOF_LADAR_PITCH;
double roll = ROOF_LADAR_ROLL;
double yaw = ROOF_LADAR_YAW;

bool ignstate = false;

// OPTARG stuff
#define FILE_OPT      0
#define OUT_OPT       1
#define START_OPT     2
#define STOP_OPT      3
#define ROW_OPT       4
#define COL_OPT       5
#define ROW_RES_OPT   6
#define COL_RES_OPT   7
#define OFFSETX_OPT   8
#define OFFSETY_OPT   9
#define OFFSETZ_OPT   10
#define PITCH_OPT     11
#define ROLL_OPT      12
#define YAW_OPT       13
#define HELP_OPT      14
#define IGNSTATE_OPT  15

/*****************************************************************************/
/*****************************************************************************/
int main(int argc, char **argv) {
  int c, digit_optind = 0; // getopt variables
  int angle;
  double resolution,units;
  char filename[255],tmp[1000];
  char *p;

  while (1) {
    int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    static struct option long_options[] = {
      {"file",             1, 0, FILE_OPT},
      {"out",              1, 0, OUT_OPT},
      {"start",            1, 0, START_OPT},
      {"stop",             1, 0, STOP_OPT},
      {"rows",             1, 0, ROW_OPT},
      {"cols",             1, 0, COL_OPT},
      {"rres",             1, 0, ROW_RES_OPT},
      {"cres",             1, 0, COL_RES_OPT},
      {"x",                1, 0, OFFSETX_OPT},
      {"y",                1, 0, OFFSETY_OPT},
      {"z",                1, 0, OFFSETZ_OPT},
      {"pitch",            1, 0, PITCH_OPT},
      {"roll",             1, 0, ROLL_OPT},
      {"yaw",              1, 0, YAW_OPT},
      {"ignorestate",      0, 0, IGNSTATE_OPT},
      {"help",             0, 0, HELP_OPT},
      {0,0,0,0}
    };

    c = getopt_long_only (argc, argv, "", long_options, &option_index);
    if(c == -1) break;

    if(c != '?') {
      switch(c) {
        case IGNSTATE_OPT:
          ignstate = true;
          break;
        case START_OPT:
          start = atof(optarg);
          break;
        case STOP_OPT:
          stop = atof(optarg);
          break;
        case ROW_OPT:
          map_rows = atoi(optarg);
          break;
        case COL_OPT:
          map_cols = atoi(optarg);
          break;
        case ROW_RES_OPT:
          map_row_res = atof(optarg);
          break;
        case COL_RES_OPT:
          map_col_res = atof(optarg);
          break;
        case OFFSETX_OPT:
          offsetx = atof(optarg);
          break;
        case OFFSETY_OPT:
          offsety = atof(optarg);
          break;
        case OFFSETZ_OPT:
          offsetz = atof(optarg);
          break;
        case PITCH_OPT:
          pitch = -(atof(optarg) * 2.0 * M_PI / 360.0);
          break;
        case ROLL_OPT:
          roll = -(atof(optarg) * 2.0 * M_PI / 360.0);
          break;
        case YAW_OPT:
          yaw = -(atof(optarg) * 2.0 * M_PI / 360.0);
          break;
        case FILE_OPT:
          file = strdup(optarg);
          break;
        case OUT_OPT:
          matlab = strdup(optarg);
          break;
        case HELP_OPT:
          break;
      }
    }
  }

  if((file == NULL) || (matlab == NULL)) {
    fprintf(stderr, "ERROR!  Must specify both -file and -out switches\n");
    exit(-1);
  }

  printf("file: %s\n", file);

  p = strstr(file, "_scans_");
  timestamp = strdup(p+7);
  *p = 0;
  testname = strdup(file);
  p = strstr(timestamp,".log");
  *p = 0;

  printf("testname: %s\n", testname);
  printf("timestamp: %s\n", timestamp);

  printf("Converting scan points from (%lf,%lf) to genMap\n", start, stop);

  sprintf(filename, "%s_scans_%s.log", testname, timestamp );
  printf("Reading scans from file: %s\n", filename);
  scanlog=fopen(filename,"r");
  if ( scanlog == NULL ) {
    printf("Unable to open scan log!!!\n");
    exit(-1);
  }
  fscanf(scanlog, "%% angle = %d\n", &angle);
  fscanf(scanlog, "%% resolution = %lf\n", &resolution);
  fscanf(scanlog, "%% units = %lf\n", &units);
  
  fgets(tmp, 1000, scanlog); // Read and discard header

  sprintf(filename, "%s_state_%s.log", testname, timestamp );
  printf("Reading state from file: %s\n", filename);
  statelog=fopen(filename,"r");
  if ( statelog == NULL ) {
    printf("Unable to open state log!!!\n");
    exit(-1);
  }
  fgets(tmp, 1000, statelog); // Read and discard header

  printf("LADAR scanning angle: %d degrees\n", angle);
  printf("LADAR scanning resolution: %lf degrees\n", resolution);
  printf("LADAR units: %lf m\n", units);
  printf("Number of scan points: %d\n", (int) ((double)angle/resolution) + 1);

  ladarTerMap.initMap(map_rows,map_cols,map_row_res,map_col_res,false);

  printf("OFFSETZ: %lf\n", offsetz);
  XYZcoord ladarOffset(offsetx, offsety, offsetz);
  ladarFrame.initFrames(ladarOffset, pitch, roll, yaw);

  while(!feof(scanlog) && !feof(statelog)) {
    UpdateMap(angle,resolution,units,offsetz);
  }

  if(addedScans) {
    printf("Found %d scans\n", addedScans);
    printf("Saving matlab UTM map to \"%s.m\"...\n",matlab);
    ladarTerMap.saveMATFile(matlab,"entered in UTM",matlab);
  } 
  else {
    printf("No scans were found in the range (%lf,%lf)\n", start, stop);
  }

  fclose(scanlog);
  fclose(statelog);
  exit(0);
}

/*****************************************************************************/
/*****************************************************************************/
void UpdateMap(int angle,double resolution,double units,double lh) {
  double *laserdata;
  XYZcoord npoint(0, 0, 0), scanpoint(0, 0, 0);
  CellData newCell;
  int i; 
  double tstamp;
  int scan;
  double phi,offset;
  int numScanPoints;

  numScanPoints = (int) ((double)angle/resolution) + 1;
  laserdata = new double[numScanPoints];

  // suck in the state data for a scan
  fscanf(scanlog, "%lf ",&tstamp);
  fscanf(statelog, "%lf %lf %lf %f %f %f %f %lf %f %f %f %f %f %f %f\n",
    &tstamp, &(SS.Easting), &(SS.Northing), &(SS.Altitude), 
    &(SS.Vel_E), &(SS.Vel_N), &(SS.Vel_U), 
    &(SS.Speed), &(SS.Accel), 
    &(SS.Pitch), &(SS.Roll), &(SS.Yaw), 
    &(SS.PitchRate), &(SS.RollRate), &(SS.YawRate));

  // suck in a scan
  for(int i = 0; i < numScanPoints; i++) {
    fscanf(scanlog,"%d ", &scan);
    if(scan >= 0x1FF7) laserdata[i] = -1.0; // Bad scan
    else laserdata[i] = (scan * units);
  }
  fscanf(scanlog,"\n");
  
  /* ***************************************************** */
  // Step 2 - fill in the map...
 
  if(ignstate) {
    SS.Pitch = 0.0;
    SS.Roll = 0.0;
  }
 
  // update internal state of frames
  ladarFrame.updateState(SS);
  // update internal state of the genMap
  ladarTerMap.updateFrame(SS,true);
  offset = ((180.0 - angle) / 2.0) * M_PI / 180.0;  // Starting angle
  if((tstamp >= start) && (tstamp <= stop)) {
    addedScans++;
    for(int i = 0; i < numScanPoints; i++) {
      if(laserdata[i] >= 0 && laserdata[i] < ROOF_LADAR_MAX_RANGE) {
        phi = offset + (i * resolution * M_PI) / 180.0;
    
        // convert the range into coordinates in the sensors frame to be
        // consistent with everything else, sensor x-axis points straight
        // ahead, y-axis to the left, and z-down, thus the scanline at 90
        // degrees will have a 0 y-coord.  All of the data will have a
        // zero z-value in the sensors frame.
        scanpoint.x = laserdata[i]*sin(phi);
        scanpoint.y = laserdata[i]*cos(phi);
        scanpoint.z = 0;
    
        // convert the sensor frame coords into utm coordinates
        // and put this into the map
	// FIXME - THIS WILL OVERWRITE WHATEVER IS CURRENTLY IS IN THE
	// CELL, IF ANYTHING
        npoint = ladarFrame.transformS2N(scanpoint);
        newCell.value = npoint.z;
        ladarTerMap.setCellDataUTM(npoint.x,npoint.y,newCell);
      }
    }
  }
 // end UpdateMap()
}
