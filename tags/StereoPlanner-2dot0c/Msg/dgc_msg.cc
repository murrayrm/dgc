#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <sys/time.h>

#include <fstream>
#include <unistd.h>
#include <fcntl.h>

#include "dgc_const.h"
#include "dgc_socket.h"
#include "dgc_mta.hh"
#include "dgc_msg.h"
#include "dgc_messages.h"
#include "dgc_time.h"

int dgc_mta_ServerPort;
int dgc_mta_NumServerPorts;

int dgc_mta_MyID;
int dgc_mta_MyType;
timeval sys0;



using namespace std;

int dgc_msg_Register(int systype) {

  dgc_mta_MyType = -1;
  //  cout << "dgc_msg_Register: server port = " << MTA_LOCAL_PORT << endl;

  dgc_mta_ServerPort = MTA_LOCAL_PORT;
  dgc_mta_NumServerPorts = MTA_NUM_LOCAL_PORTS;

  int myFD = dgc_OpenCSocket(dgc_mta_ServerPort);

  if (myFD < 0) {
    cout << "dgc_msg_Register: OpenCSocket Failure" << endl;
    return ERROR;
  }


  /* Communication in 4 steps:
     1) Write the size of the message to the socket.
     2) Write the command to the socket.
     3) Read the size of the return message.
     4) Read the return message.
  */
  int myCommand = dgcc_RegisterModule;
  int sz = sizeof(myCommand) + sizeof(systype);

  if ( dgc_WriteSocket(myFD,  &sz,  sizeof(sz) ) == ERROR) {
    cout << "dgc_msg_Register: WriteSocket Message Size Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  //  cout << "dgc_msg_Register: wrote " << sizeof(sz) << " bytes." << endl;

  if ( dgc_WriteSocket(myFD, &myCommand, sizeof(myCommand)) == ERROR) {
    cout << "dgc_msg_Register: WriteSocket My Command Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  //  cout << "dgc_msg_Register: wrote " << sizeof(myCommand) << " bytes." << endl;

  if ( dgc_WriteSocket(myFD, &systype, sizeof(systype)) == ERROR) {
    cout << "dgc_msg_Register: WriteSocket SysType Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  /* Time to shutdown the write half of the socket and wait for the data
     to return.
  */
  if (shutdown(myFD, 1) < 0) {
    cout << "dgc_msg_Register: shutdown error" << endl;
  }

  int returnMsgSize;  
  if ( dgc_ReadSocket(myFD, &returnMsgSize, sizeof(returnMsgSize), 100) == ERROR) {
    cout << "dgc_msg_Register: ReadSocket Message Size Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }
   
  
  if ( dgc_ReadSocket(myFD, &dgc_mta_MyID,  sizeof(dgc_mta_MyID), 100) == ERROR) {
    cout << "dgc_msg_Register: ReadSocket Message Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }
  
  if ( dgc_ReadSocket(myFD, &sys0, sizeof(sys0), 100) == ERROR) {
    cout << "dgc_msg_Register: ReadSocket Message Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  
  //  cout << "dgc_msg_Register: ID returned = " << dgc_mta_MyID << endl;

  if (dgc_mta_MyID > 0){
    dgc_CloseSocket(myFD);
    dgc_mta_MyType = systype;
    return TRUE;
  }
  else {
    dgc_CloseSocket(myFD);
    return ERROR;
  }
}



int dgc_msg_Unregister() {

  //  cout << "dgc_msg_Unregister: server port = " << MTA_LOCAL_PORT << endl;

  dgc_mta_ServerPort = MTA_LOCAL_PORT;
  dgc_mta_NumServerPorts = MTA_NUM_LOCAL_PORTS;

  int myFD = dgc_OpenCSocket(dgc_mta_ServerPort);

  if (myFD < 0) {
    cout << "dgc_msg_Unregister: OpenCSocket Failure" << endl;
    return ERROR;
  }


  /* 1) Write the size of the message to the socket.
     2) Write the command to the socket.
  */

  int myCommand = dgcc_UnregisterModule;
  int sz = sizeof(myCommand) + sizeof(dgc_mta_MyID);

  if ( dgc_WriteSocket(myFD,  &sz,  sizeof(sz) ) == ERROR) {
    cout << "dgc_msg_Unegister: WriteSocket Message Size Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  // cout << "dgc_msg_Unregister: wrote " << sizeof(sz) << " bytes." << endl;

  if ( dgc_WriteSocket(myFD, &myCommand, sizeof(myCommand)) == ERROR) {
    cout << "dgc_msg_Unregister: WriteSocket My Command Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  //  cout << "dgc_msg_Unregister: wrote " << sizeof(myCommand) << " bytes." << endl;

  if ( dgc_WriteSocket(myFD, &dgc_mta_MyID, sizeof(dgc_mta_MyID)) == ERROR) {
    cout << "dgc_msg_Unregister: WriteSocket MyID Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  /* Time to shutdown the write half of the socket and wait for the data
     to return.
  */
  if (shutdown(myFD, 1) < 0) {
    cout << "dgc_msg_Unregister: shutdown error" << endl;
  }

  dgc_CloseSocket(myFD);
  return TRUE;
}



int dgc_AnyMessages(int ms, int mk) {

  int myFD = dgc_OpenCSocket(dgc_mta_ServerPort);

  if (myFD < 0) {
    cout << "dgc_msg_AnyMessages: OpenCSocket Failure" << endl;
    return ERROR;
  }

  /* Communication in 4 steps:
     1) Write the size of the message to the socket.
     2) Write the command to the socket.
     3) Read the size of the return message.
     4) Read the return message.
  */

  int myCommand = dgcc_AnyMessages;
  int sz = sizeof(myCommand) + sizeof(dgc_mta_MyID) + sizeof(ms) + sizeof(mk);

  /* Write the size of the command */ 
  if ( dgc_WriteSocket(myFD, &sz, sizeof(sz)) == FALSE) {
    cout << "dgc_msg_AnyMessages: WriteSocket MyCommand Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  /* Write the command itself */
  if ( dgc_WriteSocket(myFD, &myCommand, sizeof(myCommand)) == ERROR) {
    cout << "dgc_msg_AnyMessages: WriteSocket MS Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }
  /* Write the command itself */
  if ( dgc_WriteSocket(myFD, &dgc_mta_MyID, sizeof(dgc_mta_MyID)) == ERROR) {
    cout << "dgc_msg_AnyMessages: WriteSocket MS Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  /* Write the lower boundary */
  if ( dgc_WriteSocket(myFD, &ms, sizeof(ms)) == ERROR) {
    cout << "dgc_msg_AnyMessages: WriteSocket MS Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }
  /* Write the upper boundary */
  if ( dgc_WriteSocket(myFD, &mk, sizeof(mk)) == ERROR) {
    cout << "dgc_msg_AnyMessages: WriteSocket MK Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }
 
  int returnMsgSize;  
  if ( dgc_ReadSocket(myFD, &returnMsgSize, sizeof(returnMsgSize)) == ERROR) {
    cout << "dgc_msg_AnyMessages: ReadSocket Message Size Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  int returnVal;
  if ( sizeof(returnVal) == returnMsgSize) {
    if ( dgc_ReadSocket(myFD, &returnVal, returnMsgSize) == ERROR) {
      cout << "dgc_msg_AnyMessages: ReadSocket Message Failure" << endl;
      dgc_CloseSocket(myFD);
      return ERROR;
    }
  }
  else {
    cout << "dgc_msg_AnyMessages: Return Message Size Incorrect" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  dgc_CloseSocket(myFD);
  return returnVal;
}


/* ****************************************************************************** 
*********************************************************************************
******************************************************************************* */

/* Poll Message Functions */
int dgc_PollMessage(dgc_MsgHeader& fillme, int ms, int mk) {


  //  dgc_MsgHeader msgh_error;
  //  msgh_error.MsgID = 0; 

  int myFD = dgc_OpenCSocket(dgc_mta_ServerPort);
  int ret;

  if (myFD < 0) {
    cout << "dgc_msg_PollMessage: OpenCSocket Failure" << endl;
    return ERROR; //msgh_error;
  }

  /* Communication in 4 steps:
     1) Write the size of the message to the socket.
     2) Write the command to the socket.
     3) Read the size of the return message.
     4) Read the return message.
  */

  int myCommand = dgcc_PollMessage;
  int sz = sizeof(myCommand) + sizeof(dgc_mta_MyID) + sizeof(ms) + sizeof(mk);

  /* Write the size of the command */ 
  if ( dgc_WriteSocket(myFD, &sz, sizeof(sz)) == ERROR) {
    cout << "dgc_msg_PollMessage: WriteSocket MyCommand Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR; //msgh_error;
  }

  /* Write the command itself */
  if ( dgc_WriteSocket(myFD, &myCommand, sizeof(myCommand)) == ERROR) {
    cout << "dgc_msg_PollMessage: WriteSocket MS Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR; //msgh_error;
  }
  /* Write my ID number */
  if ( dgc_WriteSocket(myFD, &dgc_mta_MyID, sizeof(dgc_mta_MyID)) == ERROR) {
    cout << "dgc_msg_PollMessage: WriteSocket MS Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR; //msgh_error;
  }


  /* Write the lower bound */
  if ( dgc_WriteSocket(myFD, &ms, sizeof(ms)) == ERROR) {
    cout << "dgc_msg_PollMessage: WriteSocket MS Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR; //msgh_error;
  }
  /* Write the upper bound */
  if ( dgc_WriteSocket(myFD, &mk, sizeof(mk)) == ERROR) {
    cout << "dgc_msg_PollMessage: WriteSocket MK Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR; //msgh_error;
  }
 
  int returnMsgSize;  
  if ( dgc_ReadSocket(myFD, &returnMsgSize, sizeof(returnMsgSize)) == ERROR) {
    cout << "dgc_msg_PollMessage: ReadSocket Message Size Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR; //msgh_error;
  }

  //  dgc_MsgHeader returnVal;
  if ( sizeof(int) == returnMsgSize ) {
    if ( dgc_ReadSocket(myFD, &ret, returnMsgSize) == ERROR) {
      cout << "dgc_msg_PollMessage: ReadSocket Message Failure" << endl;
      dgc_CloseSocket(myFD);
      return ERROR; //msgh_error;
    } else {
      dgc_CloseSocket(myFD);
      return ret;
    }
  } else if ( sizeof(dgc_MsgHeader) == returnMsgSize) {
    if ( dgc_ReadSocket(myFD, &fillme, returnMsgSize) == ERROR) {
      cout << "dgc_msg_PollMessage: ReadSocket Message Failure" << endl;
      dgc_CloseSocket(myFD);
      return ERROR; //msgh_error;
    }
  } else {
    cout << "dgc_msg_PollMessage: Return Message Size Incorrect" << endl;
    dgc_CloseSocket(myFD);
    return ERROR; //msgh_error;
  }

  dgc_CloseSocket(myFD);
  return TRUE;
}






/* ****************************************************************************** 
*********************************************************************************
******************************************************************************* */




/* Get Message Function */

int dgc_GetMessage(dgc_MsgHeader &msgh, void * buffer) {

  int myFD = dgc_OpenCSocket(dgc_mta_ServerPort);
  int ret;

  if (myFD < 0) {
    cout << "dgc_msg_GetMessage: OpenCSocket Failure" << endl;
    return ERROR;
  }

  /* Communication in 4 steps:
     1) Write the size of the message to the socket.
     2) Write the command to the socket.
     3) Read the size of the return message.
     4) Read the return message.
  */

  int myCommand = dgcc_GetMessage;
  int sz = sizeof(myCommand) + sizeof(msgh);

  /* Write the size of the command */ 
  if ( dgc_WriteSocket(myFD, &sz, sizeof(sz)) == ERROR) {
    cout << "dgc_msg_GetMessage: WriteSocket MyCommand Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  /* Write the command itself */
  if ( dgc_WriteSocket(myFD, &myCommand, sizeof(myCommand)) == ERROR) {
    cout << "dgc_msg_GetMessage: WriteSocket MSGH Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  /* Write the header */
  if ( dgc_WriteSocket(myFD, &msgh, sizeof(msgh)) == ERROR) {
    cout << "dgc_msg_GetMessage: WriteSocket MSGH Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }
 
  int returnMsgSize;  
  if ( dgc_ReadSocket(myFD, &returnMsgSize, sizeof(returnMsgSize)) == ERROR) {
    cout << "dgc_msg_GetMessage: ReadSocket Message Size Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  if ( returnMsgSize == sizeof(ret) ) {
    if ( dgc_ReadSocket(myFD, &ret, returnMsgSize ) == ERROR) {
      cout << "dgc_msg_GetMessage: ReadSocket Message Failure" << endl;
      dgc_CloseSocket(myFD);
      return ERROR;
    } else {
      dgc_CloseSocket(myFD);
      return ret;
    }
  } else if ( returnMsgSize == sizeof(ret) + msgh.MsgSize ) {
    if ( dgc_ReadSocket(myFD, &ret,   sizeof(ret) ) == ERROR ||
         dgc_ReadSocket(myFD, buffer, msgh.MsgSize ) == ERROR) {
      cout << "dgc_msg_GetMessage: ReadSocket Message Failure" << endl;
      dgc_CloseSocket(myFD);
      return ERROR;
    }
  } else {
    cout << "dgc_msg_GetMessage: Return Message Size Incorrect" << endl;
    cout << "                    Size reported as " << returnMsgSize << " bytes." << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }
  dgc_CloseSocket(myFD);
  return TRUE;
}



/* ****************************************************************************** 
*********************************************************************************
******************************************************************************* */

/* Get Last Message Function */

int dgc_GetLastMessage(void * buffer, int ms, int mk) {

  int myFD = dgc_OpenCSocket(dgc_mta_ServerPort);
  int ret;

  if (myFD < 0) {
    cout << "dgc_msg_GetLastMessage: OpenCSocket Failure" << endl;
    return ERROR;
  }

  /* Communication in 4 steps:
     1) Write the size of the message to the socket.
     2) Write the command to the socket.
     3) Read the size of the return message.
     4) Read the return message.
  */

  int myCommand = dgcc_GetLastMessage;
  int sz = sizeof(myCommand) + sizeof(dgc_mta_MyID) + sizeof(ms) + sizeof(mk);

  /* Write the size of the command */ 
  if ( dgc_WriteSocket(myFD, &sz, sizeof(sz)) == FALSE) {
    cout << "dgc_msg_GetLastMessage: WriteSocket MyCommand Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  /* Write the command itself */
  if ( dgc_WriteSocket(myFD, &myCommand, sizeof(myCommand)) == ERROR) {
    cout << "dgc_msg_GetLastMessage: WriteSocket MS Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }
  /* Write my ID */
  if ( dgc_WriteSocket(myFD, &dgc_mta_MyID, sizeof(dgc_mta_MyID)) == ERROR) {
    cout << "dgc_msg_GetLastMessage: WriteSocket MS Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  /* Write the lower boundary */
  if ( dgc_WriteSocket(myFD, &ms, sizeof(ms)) == ERROR) {
    cout << "dgc_msg_GetLastMessage: WriteSocket MS Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }
  /* Write the upper boundary */
  if ( dgc_WriteSocket(myFD, &mk, sizeof(mk)) == ERROR) {
    cout << "dgc_msg_GetLastMessage: WriteSocket MK Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }
 
  int returnMsgSize;  
  if ( dgc_ReadSocket(myFD, &returnMsgSize, sizeof(returnMsgSize)) == ERROR) {
    cout << "dgc_msg_GetLastMessage: ReadSocket Message Size Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  if ( returnMsgSize == sizeof(ret) ) {
    if ( dgc_ReadSocket(myFD, &ret, returnMsgSize ) == ERROR) {
      cout << "dgc_msg_GetLastMessage: ReadSocket Message Failure" << endl;
      dgc_CloseSocket(myFD);
      return ERROR;
    } else {
      dgc_CloseSocket(myFD);
      return ret;
    }
  } else if ( returnMsgSize > sizeof(ret)) {
    if ( dgc_ReadSocket(myFD, &ret,   sizeof(ret) ) == ERROR ||
         dgc_ReadSocket(myFD, buffer, returnMsgSize-sizeof(ret) ) == ERROR) {
      cout << "dgc_msg_GetLastMessage: ReadSocket Message Failure" << endl;
      dgc_CloseSocket(myFD);
      return ERROR;
    }
  } else {
    cout << "dgc_msg_GetLastMessage: ReadSocket Message Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }
  dgc_CloseSocket(myFD);
  return TRUE;
}




/* Remove Message Function */
/* Same as get msg except it doesnt return anything */
int dgc_RemoveMessage(dgc_MsgHeader &msgh) {

  int myFD = dgc_OpenCSocket(dgc_mta_ServerPort);

  if (myFD < 0) {
    cout << "dgc_msg_GetMessage: OpenCSocket Failure" << endl;
    return ERROR;
  }

  /* Communication in 4 steps:
     1) Write the size of the message to the socket.
     2) Write the command to the socket.
     3) Read the size of the return message.
     4) Read the return message.
  */

  int myCommand = dgcc_RemoveMessage;
  int sz = sizeof(myCommand) + sizeof(msgh);

  /* Write the size of the command */ 
  if ( dgc_WriteSocket(myFD, &sz, sizeof(sz)) == ERROR) {
    cout << "dgc_msg_RemoveMessage: WriteSocket MyCommand Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  /* Write the command itself */
  if ( dgc_WriteSocket(myFD, &myCommand, sizeof(myCommand)) == ERROR) {
    cout << "dgc_msg_RemoveMessage: WriteSocket MSGH Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  /* Write the header */
  if ( dgc_WriteSocket(myFD, &msgh, sizeof(msgh)) == ERROR) {
    cout << "dgc_msg_RemoveMessage: WriteSocket MSGH Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }
 
  // and return
  dgc_CloseSocket(myFD);
  return TRUE;
}






/* Send Message Function */

int dgc_SendMessage(dgc_MsgHeader &msgh, void * msgData, int flags) {

  int myFD = dgc_OpenCSocket(dgc_mta_ServerPort);

  if (myFD < 0) {
    cout << "dgc_msg_SendMessage: OpenCSocket Failure" << endl;
    return ERROR;
  }

  /* Prepare the Msg header */
  msgh.SourceID = dgc_mta_MyID;
  msgh.SourceType = dgc_mta_MyType;



  /* Communication in 4 steps:
     1) Write the size of the message to the socket.
     2) Write the command to the socket.
     3) Read the size of the return message.
     4) Read the return message.
  */

  int myCommand = dgcc_SendMessage;
  int sz = sizeof(myCommand) + sizeof(msgh) + msgh.MsgSize;

  /* Write the size of the command */ 
  if ( dgc_WriteSocket(myFD, &sz, sizeof(sz)) == ERROR) {
    cout << "dgc_msg_SendMessage: WriteSocket Size Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  /* Write the command itself */
  if ( dgc_WriteSocket(myFD, &myCommand, sizeof(myCommand)) == ERROR) {
    cout << "dgc_msg_SendMessage: WriteSocket MyCommand Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  /* Write the header */
  if ( dgc_WriteSocket(myFD, &msgh, sizeof(msgh)) == ERROR) {
    cout << "dgc_msg_SendMessage: WriteSocket MSGH Failure" << endl;
    dgc_CloseSocket(myFD);
    return ERROR;
  }

  if( msgh.MsgSize > 0) {
    /* Write the data */
    if ( dgc_WriteSocket(myFD, msgData, msgh.MsgSize) == ERROR) {
      cout << "dgc_msg_SendMessage: WriteSocket Data Failure" << endl;
      dgc_CloseSocket(myFD);
      return ERROR;
    }
  }

  dgc_CloseSocket(myFD);
  return TRUE;
}



double SystemTime() {
  timeval x;
  gettimeofday(&x, NULL);
  cout << "sys0 = " << sys0.tv_sec << "sec, " << sys0.tv_usec << endl;
  return timeval_subtract(x, sys0);
}
