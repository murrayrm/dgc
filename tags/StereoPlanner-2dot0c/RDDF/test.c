#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <iostream.h>
#include "rddf.hh"

int main (int argc,char* argv[]){
  system("clear");
  int ind, total,ret;
  RDDF rddf("rddf.dat");
  RDDFVector targetPoints;
  char * bobfile;
 
  /*************************************************************************/
  /********** DECLARING VARIABLES ******************************************/
  /*************************************************************************/
  // Variales used to get parameters from command line
  int opt, outOfMemory;
  char *argMethod;
  int method,param;
  

  /*************************************************************************
   ********** GETTING THE PARAMETERS FROM COMMAND LINE			
   **********  CAUTION:						  
   **********    - optind,opterr:  extern variables, do not declare a
   **********	         local variable with any of these names	     
   **********	 - optind: indice of the argv where to find the argument 
   **********		  desired					    
   *************************************************************************/

  // Defining the characters used as delimiter of a command line argument  
  param = 0;
  outOfMemory = 0;
  while ((opt = getopt (argc, argv, "m:")) != -1){
    switch ((unsigned char)opt){
    case 'm':
      param = 1;
      if(optarg != NULL){
	argMethod = (char*) malloc(strlen(optarg)+1);
	if(argMethod == NULL)
	  outOfMemory = 1;
	else
	  strcpy(argMethod,optarg);
      }
      break;
    case '?':
    default:
      system("clear");
      cout << "usage: [-m method]" << endl << endl
           << "1 : load RDDF file test" << endl
           << "2 : create BOB file" << endl << endl;
      return(-1);
    }    
  }
  
  // If any allocation failed
  if(outOfMemory){
    if(argMethod != NULL) free(argMethod);
    system("clear");
    cout << "Problems with memory management" << endl;
    return(-1);
  }
  
  // Showing the choosen test
  if(param)
    method = atoi(argMethod);
  else
    method = 1;

  switch(method){
  case 1:

    //**********************************************//
    // Testing constructor
    //**********************************************//

    targetPoints = rddf.getTargetPoints();
    total = rddf.getNumTargetPoints();
    for(ind=0; ind<total; ind++){
      targetPoints[ind].display();
    }

    break;

  case 2:

    //**********************************************//
    // create BOB file
    //**********************************************//

    targetPoints = rddf.getTargetPoints();
    total = rddf.getNumTargetPoints();
    ret = rddf.createBobFile("rddf.bob");

    break;

  default:
    cout << "Wrong choice !!" << endl;
  }
  
  return(0);
}
