/***************************************************************************************/
//FILE: 	simulator.cc
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file contains the implementation of the methods from the simulator 
//              class
//
/***************************************************************************************/

#include "simulator.hh"
#include "VehicleConstants.h" // get VEHICLE_AXLE_DISTANCE from here

//***********PRIVATE FUNCTIONS***********//

// This function gives back the next state after applying the runge-kutta 4 method
//	order	: the number of differential equations
//	x	: derive the state over x
//	y	: state space
//	ss	: function that evfaluates x' = f(x,t)
//	input	: input of the system like a force or a steering position,
//		  it's a vector to  allow more than one input
void Simulator::diffSolver(int order, double x, double *y, double step, int switchFnc, double *input){

  
  int i;
  double h=step/2.0;
  double k1[MAX_ORDER], k2[MAX_ORDER], k3[MAX_ORDER], k4[MAX_ORDER];
  double Yaux[MAX_ORDER];

  // HACK TO SWITCH BETWEEN FUNCTIONS
  // I COULDN'T FIND A WAY TO ASSIGN A POINTER OF A METHOD TO A VARIABLE
  // PAY ATTENTION ON THE SWITCHS AND THE SWITCHFNC PASSED

  // First step
  switch(switchFnc){
  case LAG_KEY:    ss_lag(x,y,k1,input);    break;
  case CRUISE_KEY: ss_cruise(x,y,k1,input); break;
  case PLANAR_KEY: ss_planar_rear(x,y,k1,input); break;
  } 

  for(i = 0;i < order; i++){
    k1[i]  = step*k1[i];
  }

  // Second step
  for(i = 0;i < order; i++){
    Yaux[i] = y[i] + k1[i]/2;
  }
  switch(switchFnc){
  case LAG_KEY:    ss_lag(x+h,Yaux,k2,input);    break;
  case CRUISE_KEY: ss_cruise(x+h,Yaux,k2,input); break;
  case PLANAR_KEY: ss_planar_rear(x+h,Yaux,k2,input); break;
  } 

  for(i = 0;i < order; i++){
    k2[i]  = step*k2[i];
  }

  // Third step
  for(i = 0;i < order; i++){
    Yaux[i] = y[i] + k2[i]/2;
  }
  switch(switchFnc){
  case LAG_KEY:    ss_lag(x+h,Yaux,k3,input);    break;
  case CRUISE_KEY: ss_cruise(x+h,Yaux,k3,input); break;
  case PLANAR_KEY: ss_planar_rear(x+h,Yaux,k3,input); break;
  } 

  for(i = 0;i < order; i++){
    k3[i]  = step*k3[i];
  }

  // Fourth step
  for(i = 0;i < order; i++){
    Yaux[i] = y[i] + k3[i];
  }
  switch(switchFnc){
  case LAG_KEY:    ss_lag(x+step,Yaux,k4,input);    break;
  case CRUISE_KEY: ss_cruise(x+step,Yaux,k4,input); break;
  case PLANAR_KEY: ss_planar_rear(x+step,Yaux,k4,input); break;
  }

  for(i = 0;i < order; i++){
    k4[i]  = step*k4[i];
  }

  // y for the next step of time
  for(i = 0;i < order; i++){
    y[i] = y[i]+(k1[i]+2*k2[i]+2*k3[i]+k4[i])/6.0;
    //cout << "y[" << i << "]=" << y[i] << " ";
  }
}

// dz/dt = f(z,u)
// z = [v]
// This function evaluates the state space of the cruise control
//	dv/dt = (1/m)*(-b*v + F)
//	v	: velocity
//	b	: drag coeficient
//	F	: Force of brakes or throttle
//	input	: the input is one dymensional
void Simulator::ss_cruise(double x, double *y, double *k, double*force){
  k[0] = (double)((double)1/VEHICLE_MASS)*(-DRAG*y[0] + force[0]);
}

// dz/dt = f(z,u)
// z = [x, y, theta]
// This function evaluates the state space of the planar position
// where the origin is at the center of the rear axle
//	dx/dt = v*cos(theta)
//	dy/dt = v*sin(theta)
//	dtheta/dt = (v/L)*tan(phi)
//      v       : speed of center of rear axle
//	x	: x position
//	y	: y position
//	theta	: heading
//	phi	: steering angle
//	L	: distance between the front and rear axles
//      input   : {steering angle, vehicle speed}
void Simulator::ss_planar_rear(double x, double *y, double *k, double*input){
  k[0] = input[1]*sin(y[2]);
  k[1] = input[1]*cos(y[2]);
  k[2] = (double)((double)input[1]/VEHICLE_AXLE_DISTANCE)*tan(input[0]);
}

// dz/dt = f(z,u)
// z = [x, y, theta]
// This function evaluates the state space of the planar position
// where the origin is at the center of the front axle
//	dx/dt = v*cos(theta + phi)
//	dy/dt = v*sin(theta + phi)
//	dtheta/dt = (v/L)*sin(phi)
//      v       : speed of center of front axle
//	x	: x position
//	y	: y position
//	theta	: heading
//	phi	: steering angle
//	L	: distance between the front and rear axles
void Simulator::ss_planar_front(double x, double *y, double *k, double*input){
  k[0] = input[1]*sin(y[2] + input[0]);
  k[1] = input[1]*cos(y[2] + input[0]);
  k[2] = (double)((double)input[1]/VEHICLE_AXLE_DISTANCE)*sin(input[0]);
}

// dz/dt = f(z,u)
// z = [tau]
// This function evaluates the state space of a first order lag
//	dTau/dt = (lag)*(Tau + u)
//	Tau	: engine torque
//	lag	: delay parameter
//	input	: the input is two dymensional, one to the lag parameter and the other to the reference
void Simulator::ss_lag(double x, double *y, double *k, double*input){
  k[0] = (input[0])*(-y[0] + input[1]);
}

// This functions convert from throttle [0 1] into torque
// y = k*u + b
double Simulator::map_acc(double u){
  double k1 = 1200;
  double b1 = 0;
  double k2 = 5350/4;
  double b2 = -321;
  double y,differential;

  differential = 10;
  if(u < 0){
    // It's a brake actuation
    y = 0;
  }
  else{  
    if(u < 0.2){
      y = k1*u + b1;
    }
    else
    {
      y = k2*u + b2;
    }
  }
  return(y*differential);
}

// This functions convert from brake [-1 0] into torque
// y = k*u + b
double Simulator::map_brake(double u, double vel){
  // Brake parameters 
  double k1 = 0.096;
  double b1 = 0;
  double k2 = 1.41;
  double b2 = -0.657;
  double fmax = 3000;

  double y;
  double in;
  
  in = -u;
  
  if(in < 0){
    // If its a throttle actuation
    y = 0;
  }
  else{
    if(in < 0.5){
      y = k1*in + b1;
    }
    else
    {
      y = k2*in + b2;
    }
  }
  
  // The direction depends on the velocity
  if(vel > 0){
    y = -y*fmax;
  }
  if(vel < 0){
    y = y*fmax;
  }
  if(vel == 0){
    y = 0;
  }
  return(y);  
}

// This functions converts the torque into force
double Simulator::map_tire(double accTorque,double brakeTorque){
  double force;
  
  force = (accTorque + brakeTorque)*VEHICLE_TIRE_RADIUS;
  return(force);
}

//***********PUBLIC FUNCTIONS***********//

// Default Constructor
Simulator::Simulator(){
  simData newSimData;
  setState(newSimData);
  step = 0;
  time = 0;
  steer = 0;
  accel = 0;
}

// Default destructor
Simulator::~Simulator(){

}

// Constructor
Simulator::Simulator(simData oldData,double newStep){
  setState(oldData);
  step = newStep;
  time = 0;
  steer = 0;
  accel = 0;
  engineState[0] = 0;
  steeringState[0] = 0;
  cruiseState[0] = 0;
  planarState[0] = 0;
  planarState[1] = 0;
  planarState[2] = 0;
}

// Initializing the state
void Simulator::initState(simData firstData){
  setState(firstData);
}

// Updating time, steer and accel
void Simulator::setSim(double pTime, double pAccel, double pSteer){
  time = pTime;
  steer = pSteer;
  accel = pAccel;
}

// Updating with the old state, yaw and vel is not necessary (evaluated inside from e_vel and n_vel!!!!)
void Simulator::setState(simData newState){
  state.x = newState.x;
  state.y = newState.y;
  state.n_vel = newState.n_vel;
  state.e_vel = newState.e_vel;
  state.u_vel = newState.u_vel;
  state.yaw = newState.yaw;
  state.phi = newState.phi;
  state.vel = newState.vel;
}

// Evaluation of the integration
void Simulator::simulate(){
  simData newState;
  double x, y, yaw, vel, phi;

  double cruiseInput[1], planarInput[2], engineInput[2], steeringInput[2];
  double throttleOutput, brakeOutput;

  // Getting actual state
  cruiseState[0] = state.vel;
  planarState[0] = state.x;
  planarState[1] = state.y;
  planarState[2] = state.yaw;

  // Getting the acceleration torque
  throttleOutput = map_acc(accel);
      
  // Getting the brake torque
  brakeOutput = map_brake(accel,state.vel);
      
  // Passing thru the engine lag
  engineInput[0] = ENGINE_LAG;      // lag 
  engineInput[1] = throttleOutput;  // input of the system
  diffSolver(LAG_ORDER, time, engineState, step,LAG_KEY, engineInput);

  // Converting the torques into forces
  cruiseInput[0] = map_tire(engineState[0],brakeOutput);
  
  // Computing next velocity
  diffSolver(CRUISE_ORDER, time, cruiseState, step, CRUISE_KEY, cruiseInput);
  vel = cruiseState[0];
  if(vel < 0) vel = 0;

  // Passing thru the steering wheel lag
  steeringInput[0] = STEERING_LAG;
  steeringInput[1] = SIM_MAX_STEER*steer;
  diffSolver(LAG_ORDER, time, steeringState, step,LAG_KEY, steeringInput);
  phi = steeringState[0];
  
  // Computing next state of the planar movement
  planarInput[0] = phi;
  planarInput[1] = vel;
  diffSolver(PLANAR_ORDER, time, planarState,step, PLANAR_KEY, planarInput);
  x   = planarState[0];
  y   = planarState[1];
  yaw = planarState[2];

  // Updating state
  newState.x     = x;
  newState.y     = y;
  newState.vel   = vel;
  newState.n_vel = vel*cos(yaw);
  newState.e_vel = vel*sin(yaw);
  newState.u_vel = 0;
  newState.yaw   = yaw;
  newState.phi   = phi;
  setState(newState);
}

// Set the time step
void Simulator::setStep(double pStep){
  step = pStep;
}

// Returns the state
simData Simulator::getState(){
  return(state);
}
