/***************************************************************************************/
//FILE: 	main.cc
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file is responsible to create the multiply threads in order to run
//		the model, the cruise control and the heading control in each of them
//
/***************************************************************************************/

#include <unistd.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>


#include "main.hh"

using namespace std;

// Variable where all the shared data can be found
DATUM d;

// Used for starting threads
boost::recursive_mutex StarterMutex;
int StarterCount;
void Starter();

int main() {
  cout << "Starting Main" << endl;

  // Spawn threads
  boost::thread_group thrds;

  StarterCount = 0;
  thrds.create_thread(Starter); // Bob thread
  thrds.create_thread(Starter); // Cruise Control thread
  thrds.create_thread(Starter); // Heading Control thread


  thrds.join_all();
  cout << "Shutting Down" << endl;

  return 0;
}


// This function os responsible for creating threads
// It updates a counter and keeps track of each thread with a number
// Each time it starts a different thread
void Starter() {

	int myThread; // The thread ID

	// This piece of block is to creaate another scope so as to have the variable destroied when it ends
	if( TRUE ) {
		boost::recursive_mutex::scoped_lock sl(StarterMutex);
		myThread = StarterCount;
		StarterCount ++;
	}

	switch( myThread ) {
	case 0:
		cout << "Starting GrabBob Loop: " << StarterCount <<  endl;
		GrabBobLoop(d);
		break;
	case 1:
		cout << "Starting GrabCruise Loop: " << StarterCount << endl;
		GrabCruiseLoop(d);
		break;
	case 2:
		cout << "Starting GrabSteering Loop: " << StarterCount << endl;
		GrabSteeringLoop(d);
		break;
	default:
		cout << "Starter error, default reached" << endl;
  }
}
