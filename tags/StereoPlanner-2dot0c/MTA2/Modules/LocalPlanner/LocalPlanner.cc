#include "LocalPlanner.hh"

#include "Kernel.hh"
#include <time.h>
#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define LOCAL_KEY 111
#define LOCAL_SIZE 50000
//Data will be updated in the shared memory every SEND_RATE microseconds
//#define LOCAL_SEND_RATE 1000*1000

using namespace std;

extern Kernel K;
LocalPlannerDatum D;
//extern LocalPlanner testmodule;


void LocalPlanner::Init() {

  // do your init in here
  // i.e. open serial ports etc,

  cout << ModuleName() << ": Init" << endl;

  D.map_data = NULL;
  D.size = LOCAL_SIZE;
  D.key = LOCAL_KEY;

  D.shmflg = (IPC_CREAT | 0x1ff);
  if ((D.shmid = shmget(D.key, D.size, D.shmflg)) < 0) {
    cout << ModuleName() << ": DID NOT INIT PROPERLY - SHARED MEMORY ERROR";
    SetNextState(STATES::SHUTDOWN);
  } else {
    D.map_data = (void*)shmat(D.shmid, NULL, 0);

  // go to active state after initialization
  SetNextState(STATES::ACTIVE);
  }
}



void LocalPlanner::Active() {
  while( ContinueInState() ) {  // while stay active
    /*
    sleep(1); // increment the counter every second

    D.MyCounter ++;
    cout << ModuleName() << ": Active - Incrementing Counter = " << D.MyCounter << endl;
    //cout << "Sending New Out Message" << endl;
    Mail m = NewOutMessage(MODULES::Echo, LocalPlannerMessages::PAUSE);
    K.SendMail(m);
    */
  }
} 


void LocalPlanner::Standby() {
  while( ContinueInState() ) {  // while stay active
    /*
    sleep(1); // dont increment the counter while in standby
    localCounter++;
    if(localCounter % 10 == 9) {
      //      cout << "Trying To Send Mail" << endl;
      Mail m = NewOutMessage(MODULES::Echo, LocalPlannerMessages::GO);
      K.SendMail(m);
    }

    //    D.MyCounter ++;
    cout << ModuleName() << ": Standby - _Not_ Incrementing Counter = " << D.MyCounter << endl;
    */
  }
} 


void LocalPlanner::Shutdown() {
  // do whatever closing of files, etc
  cout << ModuleName() << ": Shutdown" << endl;

}

void LocalPlanner::Restart() {
  // you may want to do something better here, but i'll just call shutdown and init
  cout << ModuleName() <<": Restart" << endl;
  Shutdown();
  Init();
}

string LocalPlanner::Status() {
  string x = ModuleName() + ": is in state ";
  switch ( GetCurrentState() ) {
  case STATES::INIT:
    x+= "Init";       break;
  case STATES::ACTIVE:
    x+= "Active";     break;
  case STATES::SHUTDOWN:
    x+= "Shutdown";   break;
  case STATES::STANDBY:
    x+= "Standby";    break;
  case STATES::RESTART:
    x+= "Restart";    break;
  default:
    x+= "Unknown state - (Something is very wrong)";
  }
  return x;
}
 
void LocalPlanner::InMailHandler(Mail& ml) {

  switch(ml.MsgType()) {  // for mail functions check out Misc/Mail/Mail.hh

  case LocalPlannerMessages::REINIT:
    SetNextState(STATES::RESTART);
    break;

  case LocalPlannerMessages::SHUTDOWN:
    // force the Kernel to shutdown
    K.ForceShutdown();
    break;

  case LocalPlannerMessages::PAUSE:
    SetNextState(STATES::STANDBY);
    break;

  case LocalPlannerMessages::GO:
    SetNextState(STATES::ACTIVE);
    break;

    /*
  case LocalPlannerMessages::SETCOUNTER:
    // check to see that an int was attached to the message
    if( ml.Size() >= sizeof(int)) {
      int x;
      ml >> x;
      D.MyCounter = x;
    }
    break;
    */

  case LocalPlannerMessages::MAPDATA:
    cout << "\nGot map data";
    ml.DequeueFrom(D.map_data, D.size);
    break;

  default:
    cout << ModuleName() << ": Unknown InMessage" << endl;
  }
}


Mail LocalPlanner::QueryMailHandler(Mail& msg) {
  // figure out what we are responding to

  Mail reply = msg; // A hack !!! dont do this :)
  int x;

  switch(msg.MsgType()) {  

    /*
  case LocalPlannerMessages::GETCOUNTER:
    reply = ReplyToQuery(msg);
    reply << D.MyCounter;
    break;
    */

  case LocalPlannerMessages::GETSTATE:
    reply = ReplyToQuery(msg);
    x = GetCurrentState();
    reply << x;
    break;

  default:
    // send back a "bad Message"
    reply = Mail( msg.From(), msg.To(), 0, MailFlags::BadMessage);
  }

  return reply;
}


