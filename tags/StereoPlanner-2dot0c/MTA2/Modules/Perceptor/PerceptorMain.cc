#include "Modules.hh"
#include "Perceptor.hh"
#include "Kernel.hh"

#include <boost/shared_ptr.hpp>

using namespace boost;

Kernel K;
Perceptor DGCM(MODULES::Perceptor, "Test Module", 0);

int main() {

  K.Register();
  //  K.Register(shared_ptr<DGC_MODULE>(new Perceptor(MODULES::Perceptor, "Test Module", 0)));

  K.Start();

  return 0;
}
