#include "Modules.hh"
#include "StateReceiver.hh"
#include "Kernel.hh"

#include <boost/shared_ptr.hpp>

using namespace boost;

Kernel K;
StateReceiver DGCM(MODULES::StateReceiver, "State Receiver Module", 0);

int main() {

  K.Register();
  //  K.Register(shared_ptr<DGC_MODULE>(new StateReceiver(MODULES::StateReceiver, "Test Module", 0)));

  K.Start();

  return 0;
}
