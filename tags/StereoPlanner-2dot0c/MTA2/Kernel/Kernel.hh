#ifndef Kernel_HH
#define Kernel_HH

#include "Modules.hh"
#include "DGC_MODULE.hh"
#include "SystemMap.hh"
#include "Time.hh"
#include "KernelDef.hh"
#include "ModuleHandler.hh"
#include "InMessage.hh"

#include <boost/shared_ptr.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

using namespace std;






class Kernel {
  
  friend class KERNEL::SystemMap;
  friend class KERNEL::InMessageHandler;
public:
  Kernel();
  ~Kernel();

  void Register();
  //  void Register(shared_ptr<DGC_MODULE> dgc);

  void Start(); // start of program, allow module to operate
  void ForceShutdown(); // Flags for shutdown
  void Activate();

  int ShuttingDown(); // returns ShutdownFlag
  int Active(); //
  
  Timeval Now(); // returns current time
  TCP_ADDRESS MyTCPAddress(); // returns TCP Address of current module
  MailAddress FindAddress(int type);


  void  SendMail(Mail & ml, File f= File()); // puts mail in a queue to be sent
  Mail  SendQuery(Mail & ml, File f= File()); // sends mail immediately and waits for respons (returned by function)
  
  
  list<ModuleInfo> LocalModules();
  list<ModuleInfo> AllModules();
  void PrintModuleList(list<ModuleInfo> l);
  
private:
  void DeliverMail(Mail& ml, File f = File() ); // deliver mail with file (open file if necessary)
  
  Mail QueryMailHandler(Mail& ml);
  void InMailHandler(Mail& ml);
  
  

  int KD_ShutdownFlag;
  int KD_ActiveFlag;
  
  int KD_ModulesCounter;
  boost::recursive_mutex KD_ModulesCounterLock;
  
  vector<shared_ptr<ModuleHandler> > KD_Modules;
  
  vector<Mail>                       KD_OutMail;
  
  // and the threads that will run everything
  KERNEL::InMessageAcceptor    KD_InMessageAcceptor;
  //  KERNEL::SystemMap            KD_SystemMap;
  KERNEL::Time                 KD_Time;
  
  
};


namespace KernelMessages {
  enum {  
          ECHO,       // just respond with whatever was sent

    // IN-MESSAGES
          SHUTDOWN,   // Message Declaring Shutdown
	  

    // QUERY-MESSAGES

	  MODULEINFO, // list what modules are 



	  NumKernelMessages
  };

};


#endif
