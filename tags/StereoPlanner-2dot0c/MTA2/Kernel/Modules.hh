#ifndef Modules_HH
#define Modules_HH

namespace MODULES {
  enum {
    Echo,
    SystemMaster,
    FrameGrabber,
    VisionProcessor,
    StateEstimator,
    LocalPlanner,
    GlobalPlanner,
    LADARPlanner,

    Arbiter,
    ModeSwitch,
    DriveActuator,

    TIControl,

    Joystick,
    CameraControl,
    EStop,
    
    Actuators,

    Perceptor,
    StateGiver,
    StateReceiver,

    TestModule,

    NumberOfModules
  };
};

#endif
