// 2/8/2004
// 5 pm
// Christopher Rasmussen

The ud_road_misc directory should be renamed "misc" and placed in ud_road 
for compilation.  (Only core and fftw are in the ud_road module in CVS since
misc contains ~200 MB of libraries and header files).

To compile, type "make" in the ud_road/core directory.

There are currently two modes controlled by #defines in core/main.cpp: 
SOURCE_AVI and SOURCE_DIRECTORY.  SOURCE_AVI takes input from an
AVI movie whose location is specified by "avi_filename".  SOURCE_DIRECTORY
takes input from a series of still images in PPM (ASCII) format in the
directory specified by "in_filedir".  The images are treated as independent--
not a sequence.

The default output of the system is currently a visual representation of
the "likelihood" of the vanishing point location as shown by the distribution
of yellow particles, with the estimate state overlaid as a green square.
 
Uncomment "#define CONNECT_TO_TITANIUM" to try to connect to the MTA shell
on titanium and create threads for writing votes and reading state, genmap
information.
 
Outstanding issues

* Firewire camera input (vs. AVI/still images).  I have code for this,
  but we are waiting on the Pt. Gray color camera to finalize it.
* Get camera calibration and camera-to-vehicle transformation in 
  order to formulate a physically plausible correspondence between road 
  parameters in image space and steering angle.
* Speed up framerate further (it's about 4-5 fps for downsampled 160 x 120
  input images).
* Improve methods for robustly estimating road width, curvature (in 
  addition to vanishing point).  There are miscellaneous functions for this
  in the code--in particular in CR_Segmenter.cpp--but these are not yet 
  race-ready.