//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// CR_PARTICLEFILTER
//----------------------------------------------------------------------------

#ifndef CR_PARTICLEFILTER_DECS

//----------------------------------------------------------------------------

#define CR_PARTICLEFILTER_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "CR_Memory.hh"
#include "CR_Error.hh"
#include "CR_Random.hh"
#include "CR_Matrix.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

typedef double (*CondP_ZX) (void *, CR_Vector *);            // measures "fitness" of each state
typedef void (*SamplePrior) (CR_Vector *);                   // generates initial distribution of states
typedef void (*SampleState) (CR_Vector *, CR_Vector *);      // stochastic update of state
typedef void (*DynamicsState) (CR_Vector *, CR_Vector *);    // deterministic update of state

typedef struct crcondensation 
{
  int updates;
  int n;                 // number of samples

  CR_Vector **s;         // samples from prior p(x)
  CR_Vector **s_new;     // resampled and updated samples
  CR_Vector **pi_s;      // weighted samples (for state estimation)
  CR_Vector *x;          // estimated state (sum of pi_s array)
  CR_Vector *sdet;       // sample after deterministic motion

  CR_Matrix *F;          // (deterministic) coefficient in process equation

  CR_Matrix *M, *MT, *C;
  CR_Vector *mean;

  //  CR_Matrix *Q, *Qsqrt;  // covariance of process noise and its square root
  //  double Qalpha;         // coefficient of Gaussian process noise

  CR_Vector *pi;         // weights on samples (computed from p(z|x)) 
  double pi_sum;         // sum of p(z|x) over n samples
  CR_Vector *c;          // cumulative sum of pi's: c_1 = pi_1; for i > i, c_i = c_{i-1} + pi_i 

  //  CR_Vector *mean;       // mean of Gaussian that initially describes prior p(x)
  CR_Matrix *Cov, *Covsqrt;// covariance of Gaussian that initially describes p(x) and its square root
  double Covalpha;       // coefficient of Gaussian prior

  //  int presample;         // have samples already been generated going into update function?
  int whichsamp;         // index (into array s) of current state being sampled; set before calling cond_prob_zx()
  CR_Vector *xmed;       // estimated state (median of s array)

  CondP_ZX condprob_zx;
  SamplePrior samp_prior;
  SampleState samp_state;
  DynamicsState dyn_state;

} CR_Condensation;

//-------------------------------------------------
// functions
//-------------------------------------------------

void reset_CR_XCondensation(CR_Condensation *);
CR_Condensation *make_CR_XCondensation(int, CR_Vector *, CR_Matrix *, CondP_ZX, SamplePrior, SampleState, DynamicsState);
void update_CR_XCondensation(void *, CR_Condensation *);

//void presample_CR_Condensation(CR_Condensation *);
CR_Condensation *make_CR_Condensation(int, CR_Matrix *, CR_Matrix *, CR_Vector *, CR_Matrix *, CondP_ZX, SamplePrior, SampleState);
int interval_membership(int, int, CR_Vector *, double);
//void old_update_CR_Condensation(CR_Condensation *);
//double old_cond_prob_zx(CR_Matrix *x, CR_Condensation *);
void update_CR_Condensation(void *, CR_Condensation *);
double test_cond_prob_zx(CR_Vector **z, int n, CR_Vector *x, CR_Condensation *);
void print_CR_Condensation(CR_Condensation *);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif
