//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_Memory.hh"

long int bytes_allocated = 0;

//----------------------------------------------------------------------------

void *CR_malloc(size_t size)
{
  bytes_allocated += size;

  return malloc(size);
}

//----------------------------------------------------------------------------

void *CR_calloc(size_t nmemb, size_t size)
{
  bytes_allocated += nmemb * size;

  return calloc(nmemb, size);
}

//----------------------------------------------------------------------------

void CR_free_malloc(void *ptr, size_t size)
{
  bytes_allocated -= size;

  free(ptr);
}

//----------------------------------------------------------------------------

void CR_free_calloc(void *ptr, size_t nmemb, size_t size)
{
  bytes_allocated -= nmemb * size;

  free(ptr);
}

//----------------------------------------------------------------------------

long int CR_allocated_bytes()
{
  return bytes_allocated;
}
