//Author: Lyudmil Antonov 
//Contact lantonov@vt.edu
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string>
#include <stdarg.h>
#include <limits.h>
#include <sys/wait.h>



#define MAX_BUF_SIZE 1024
#define MAX_ARGS 50
//#define LS_DIR "/var/run/dgc"
//#define RUN_DIR /var/dgc/run
/*
 * The run directory where we store run information. 
 */
static const std::string RUN_DIR("/var/dgc/run");
static const time_t sleep_secs = 30;

static const long sleep_nsecs = 0;

#define LOG_FILE "/var/dgc/log/monitor.log"
#define LOG_EXT ".pid"
struct stat f_stat;
char full_fname[MAX_BUF_SIZE];


char mon_strbuf[MAX_BUF_SIZE];
char time_buf[MAX_BUF_SIZE];
char key_buf[MAX_BUF_SIZE];
char bindir_buf[PATH_MAX];
void printem(char * my_Argv[])
{
  int k = 0;
  while(my_Argv[k] != NULL)
    {      
      printf( "[%d] : %s ", k, my_Argv[k]);
      k++;
    }
  printf("\n");
}

/**
 * convertToArgV - takes a pointer to char and converts into a pointer to char[]
 * @param input - the input to be converted 
 * @return the pointer to the array
 */
void convertToArgv(char * input, char * mon_argv[])
{
  int k = 0;
  mon_argv[k++] = "DGCKeepAlive";
  mon_argv[k] = strtok(input," ");
  do
    {          
      k++;
    }
  while((mon_argv[k] = strtok(NULL," ")) != NULL);
}
//---------------------------------------------------------------------------
//Add time tagged msg to log file
//
void TimeTagMsg( int argc, ...) {
	FILE* flog = fopen(LOG_FILE,"a");
	time_t curr_time;
	int i;
	va_list ap;
	
	curr_time = time(NULL);
	ctime_r(&curr_time, time_buf);
	time_buf[strlen(time_buf)-1] = '\0';
	fprintf(flog, "[ %s ] ",  time_buf);

    va_start(ap, argc);
	for ( i = 0 ; i < argc ; i++) 
		fprintf(flog, "%s", va_arg(ap,char*));
	va_end(ap);
	
	fprintf(flog,"\n");
	
	fclose(flog);
}

//---------------------------------------------------------------------------
//Check if a pid exists 
//
int pidexists(pid_t pid) {
	if ((kill(pid,0) == -1) && (errno == ESRCH))
		return 0;
	
	return 1;
}



//---------------------------------------------------------------------------
//Execute process with command line params
//
void ExecProcess(char *cmdLine, char * binDir, char * envKey) {	
  TimeTagMsg(2, "Module died.Executing -- ", cmdLine);
  pid_t pid;
  if((pid =fork()) < 0) 
    {
      perror("fork error");
    }
  else
    {
      if(pid == 0)
	{
	  char * argv[MAX_ARGS];
	  convertToArgv(cmdLine, argv);
	  printem(argv);
	  // only for the child process
	  if(chdir(binDir) != -1)
	    {
	      int status = execv(argv[0],argv);
	      perror("Exec failed");
	      exit(status);
	    }
	  else
	    {
	      perror("Chdir error");
	    }
	}
      wait(NULL);
    }

  //	std::stringstream command;
  //	command << "cd " << binDir <<";export SKYNET_KEY=" << envKey << ";./DGCKeepAlive " << cmdLine << "&";
	//printf("%s",command.str().c_str());
  //	system( command.str().c_str());
	
  //	TimeTagMsg(2, "Module died.Executing -- ", command.str().c_str());
}


//---------------------------------------------------------------------------
//Parse a simple 2 line config file containing pid on first line and 
//executable with cmd line options on the second line 
//
void ParseConfig(char *fname) {
	FILE * log =fopen(fname, "r");
	pid_t pid;
	//char *b_start;
	fscanf(log, "%d",&pid);
	fgets(mon_strbuf, MAX_BUF_SIZE, log); // read line 1
	fgets(mon_strbuf, MAX_BUF_SIZE, log); // read the command
	fgets(bindir_buf, PATH_MAX, log); // read the bin directory
	fgets(key_buf, MAX_BUF_SIZE, log); // read the skynet key
	fclose(log);
	// delete  \n in all these line
	mon_strbuf[strlen(mon_strbuf)-1] = '\0';
	bindir_buf[strlen(bindir_buf)-1] = '\0';
	key_buf[strlen(key_buf)-1] = '\0';
	
	if (!pidexists(pid)){
		if (remove(fname) == -1) {
			perror("remove_file");
		}		
		
		ExecProcess(mon_strbuf, bindir_buf, key_buf);
	}

}


//---------------------------------------------------------------------------
//check if a dir entry is a file ending on .pid and if so parse it 
//
int chfile(const struct dirent *entry) {
  sprintf(full_fname,"%s/%s\0",RUN_DIR.c_str(),entry->d_name); 
	lstat(full_fname, &f_stat);
	
	if (S_ISDIR(f_stat.st_mode) == 0){
		if ((strlen(full_fname)>strlen(LOG_EXT)) && (strcmp(full_fname+strlen(full_fname)-strlen(LOG_EXT),".pid") == 0))
			ParseConfig(full_fname);
	}
	return 0;
}

//---------------------------------------------------------------------------
//Parse the log dir, checking each .pid file for existing pid and restarting 
//dead processes as needed
//
void ParseDir(){
           struct dirent **namelist;
           int n;

           n = scandir(RUN_DIR.c_str(), &namelist, chfile, alphasort);
		   free(namelist);
}


int main(int argc,char * argv[]) {
  struct timespec sleepTime;
  struct timespec remTime;
  sleepTime.tv_sec = sleep_secs;
  sleepTime.tv_nsec = sleep_nsecs;
  pid_t pid;
  //daemonizing
  if ( (pid = fork()) < 0)
    {
      return -1;
    }
  else
    {
      if(pid != 0)
	{
	  //parent exits leaving child as daemon
	  exit(0);
	}
    }	  
  while (true) {
    nanosleep(&sleepTime, &remTime);
    ParseDir();
    
  }
  return 0;

}

