/**
 * Process Manager module
 * Blackbox Implementation 7/22/05
 * Revision by Lusann Yang
 */

#ifndef PROCESSMAN_HH
#define PROCESSMAN_HH

#include <iostream>
#include "sn_msg.hh"

using namespace std;

/** 
 * Put comments in here
 */
class ProcessMan
{
  class aModule 
  {
    string command;
    int computer;
    int* status;
  };
 private:
  struct aModule m_sc2pm;
  struct aModule m_pm2sc;

 public:
  /**
   * Default constructor
   * Put comments in here
   */
  ProcessMan (int snKey);

  /**
   * ProcessMan destructor
   * Put comments in here
   */
  ~ProcessMan();

 private:
  /**
   * Our skynet object used for receiving mesages.
   *
   */
  skynet m_pmSkynet;
    /**
   * mesgs i recive, this is the
   * moduleStarter status message
   */
  sn_msg myInput;
  
  /**
   * sender of chirping messages5B
   */
  modulename sender;

  /**
   * GenList() 
   * 
   */
  void genList();

  /**
   * Broadcast() sends information out to SuperCon
   * (will it also send it out to the other modules?)
   */
  void broadcast();

  /**
   * Listen() listens for heartbeats
   */
  void listen();

  /**
   * ReadConfig() reads in initial configuration
   * fill of all modules ever to be run
   */
  void readConfig();

};
#endif
