#ifndef _PID_H_
#define _PID_H_

/* This is the PID controller class.  It implements a Proportional, 
   Integral, Derivative controller on an input error signal and provides
   the control output = Kp * e + Ki * \int{e} + Kd * \dot{e}. */

#define ERROR_SIGNAL_LENGTH 20
#define PARAM_READ_INTERVAL 10

/** @brief The generic PID controller class. */
class Cpid
{
	/** parameter file, if we're using one. NULL if we're not */
	char *m_pParamFile;

	/** the counter to read in the params every PARAM_READ_INTERVAL updates */
	int m_paramReadCounter;

  /** The proportional, integral and derivative gains. */
  double m_Kp, m_Ki, m_Kd;
  
  /** The value of the derivative (pre-gain). */
  double  m_derivativeError;

  /** The value of the integral (pre-gain). */
  double  m_integralError;

  /** The absolute saturation value for the integral (assumed positive). */
  double  m_integralLimit;
 
  /** A history of the error signal.  The latest discrete error signal value
      is kept in index 0, and the oldest one is at ERROR_SIGNAL_LENGTH-1. */
  double m_errors[ERROR_SIGNAL_LENGTH];

  /** The time stamps corresponding to the error signal updates. */
  unsigned long long m_stamps[ERROR_SIGNAL_LENGTH];

 public:

  /** The constructor allocates memory for the data and sets the gains. */
  Cpid(double Kp, double Ki, double Kd, double sat);

	/** This constructor sets up the controller to read the tuning parameters in
			from a file every PARAM_READ_INTERVAL updates */
	Cpid(char* pParamFile);
  
  /** Reset the proportional gain to some value. */
  void set_Kp( double new_Kp ) { m_Kp = new_Kp; }
                  
  /** Reset the integral gain to some value. */
  void set_Ki( double new_Ki ) { m_Ki = new_Ki; }
                  
  /** Reset the derivative gain to some value. */
  void set_Kd( double new_Kd ) { m_Kd = new_Kd; }
  
  /** Access the proportional gain */
  double get_Kp () { return m_Kp; }

  /** Access the integral gain */
  double get_Ki () { return m_Ki; }

  /** Access the derivative gain */
  double get_Kd () { return m_Kd; }
             
  /** This function sets the integral term saturation. */
  void set_integral_limit(double new_iL ) { m_integralLimit = new_iL; }

  /** This function zeros the integral term. */
  void reset_integral(void) { m_integralError = 0.0; }

  /** This function completely resets the controller (integral and derivative
      terms. */
  void reset(void);

  /** PID control calculation.  Takes an error value and computes the sum of
      the proportional, integral and derivative terms. Returns the control 
      output = Kp * e + Ki * \int{e} + Kd * \dot{e} (input to plant). */
  double step_forward(double error);

  /** returns current proportional error */
  double getP();

  /** returns current integral error */
  double getI();

  /** sets current integral error, for use in bumpless transfer */
  void setI(double newI) {m_integralError = newI;}

  /** returns current derivative error */
  double getD();

  /** Destructor deallocates memory. */
  ~Cpid();

};

#endif // _PID_H_
