#include "sn_msg.hh"
#include <stdio.h>
#include <stdlib.h>
#define BUFSIZE 1024

int main(int argc, char** argv){
  int sn_key;
  char* default_key = getenv("SKYNET_KEY");
  if (2 == argc)
  {
    sn_key  = atoi(argv[1]);
  }
  else if (default_key != NULL )
  {
    sn_key = atoi(default_key);
  }
  else
  {
    printf("usage: sn_get_test [key]\nDefault key is $SKYNET_KEY\n");
    exit(2);
  }
  
  modulename myself = SNfusionmapper;
  modulename sender = ALLMODULES;
  skynet Skynet(myself, sn_key);
  printf("My module ID: %u\n", Skynet.get_id());
  sn_msg myinput = SNpointcloud;
  
  int sock1 = Skynet.stream_listen(myinput, sender);
  char* buffer[BUFSIZE];
  Skynet.stream_get_msg(sock1, buffer, BUFSIZE, 0);
  printf("%s\n", (char*)buffer);
  return 0;
  }
