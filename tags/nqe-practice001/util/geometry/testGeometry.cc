#include "geometry/Geometry.hh"
#include <iostream>

using namespace std;

int main() {
  int numRows = 10;
  int numCols = 3;

  double inputMatrix[3*4];
  /*
  for(int i=0; i<2*100; i++) {
    inputMatrix[i] = ((double)rand())/((double)RAND_MAX)*10.0 - 5.0;
  }
  inputMatrix[0] = 5;
  
  double A=1.0;
  double B=2.0;
  double C=3.0;

  double weightVector[100];
  for(int i=0; i<100; i++) {
    weightVector[i] = 1.0;
  }

  double results[3];

  for(int i=0; i<100; i++) {
    inputMatrix[200+i] = -A*inputMatrix[i]/C +
      -B*inputMatrix[100+i]/C +
      1.0/C + 0.5*((double)rand())/((double)RAND_MAX) - 0.25 ;
  }

  //pseudoInv(inputMatrix, numCols, numRows, outputMatrix);
  for(int i=0; i<30; i++) {
    //cout << inputMatrix[i] << endl;
  }
  //pseudoInv(inputMatrix, numCols, numRows, outputMatrix);
  
  cout << "Calculate that plane fit!" << endl;
  calculateWeightedPlaneFit(inputMatrix, weightVector,
			    100, results);

  for(int i=0; i<3; i++)
    cout << results[i] << endl;

  cout << "Done" << endl;
*/
  double A=1.0;
  double B=2.0;
  double C=3.0;

  for(int i=0; i<4; i++) {
    inputMatrix[3*i + 0] = ((double)rand())/((double)RAND_MAX)*10.0 - 5.0;
    inputMatrix[3*i + 1] = ((double)rand())/((double)RAND_MAX)*10.0 - 5.0;

    //inputMatrix[i + 0] = 3*i + 0;
    //inputMatrix[i + 4] = 3*i + 1;
    inputMatrix[3*i + 2] = -A*inputMatrix[3*i + 0]/C +
      -B*inputMatrix[3*i + 1]/C +
      1.0/C + 0.5*((double)rand())/((double)RAND_MAX) - 0.25 ;
    cout << inputMatrix[3*i+0] << " " 
	 << inputMatrix[3*i + 1] << " " 
	 << inputMatrix[3*i + 2] << endl;
  }
  

  double weightVector[4];
  for(int i=0; i<4; i++) {
    weightVector[i] = 1.0;
  }

  double results[3];

  for(int i=0; i<100; i++) {
  }

  //pseudoInv(inputMatrix, numCols, numRows, outputMatrix);
  for(int i=0; i<30; i++) {
    //cout << inputMatrix[i] << endl;
  }
  //pseudoInv(inputMatrix, numCols, numRows, outputMatrix);
  
  cout << "Calculate that plane fit!" << endl;
  calculateWeightedPlaneFitAndResidual(inputMatrix, weightVector,
			    4, results);

  for(int i=0; i<3; i++)
    cout << results[i] << endl;

  cout << "Done" << endl;


}
