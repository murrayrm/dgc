#ifndef __CMAPLAYER_HH__
#define __CMAPLAYER_HH__


enum {
  CML_OK,
  CML_UNKNOWN_ERROR,

  CML_STATUS_COUNT
};


class BaseLayer {
public:
  BaseLayer() {};
  virtual ~BaseLayer() {};

  virtual void* getDataMem(int memRow, int memCol) {return NULL;};
  virtual int clearMemRow(int memRow) {return CML_UNKNOWN_ERROR;};
  virtual int clearMemCol(int memCol) {return CML_UNKNOWN_ERROR;};

  int _numRows;
  int _numCols;

  virtual void* getDataAddr() {return NULL;};
  virtual void* getNoDataVal() {return NULL;};
  virtual void* getOutsideMapVal() {return NULL;};

private:

};



template <class T>
class CLayer : public BaseLayer
{
public:
  CLayer(int numRows, int numCols, T noDataVal, T outsideMapVal);
  ~CLayer();

  void* getDataMem(int memRow, int memCol);
  int clearMemRow(int memRow);
  int clearMemCol(int memCol);

  void* getDataAddr() {return (void*)dataAddr;};
  inline void* getNoDataVal() {return &_noDataVal;};
  void* getOutsideMapVal() {return &_outsideMapVal;};

private:
  T* dataAddr;
  T _noDataVal;
  T _outsideMapVal;
};


template <class T>
CLayer<T>::CLayer(int numRows, int numCols, T noDataVal, T outsideMapVal) 
{
  _numRows = numRows;
  _numCols = numCols;
  _noDataVal = noDataVal;
  _outsideMapVal = outsideMapVal;

  dataAddr = new T[_numRows * _numCols];

  //Check to make sure malloc was successful
  if(dataAddr == NULL) 
	{
		printf("%s [%d]: Fatal error while attempting to allocate memory for a layer!\n", __FILE__, __LINE__);
		printf("%s [%d]: The program cannot continue!  Quitting!\n", __FILE__, __LINE__);
		exit(1);
	} 
}


template <class T>
CLayer<T>::~CLayer()
{
	delete dataAddr;
}

template <class T>
void* CLayer<T>::getDataMem(int memRow, int memCol) {
  if(memRow < 0 || memRow >= _numRows ||
     memCol < 0 || memCol >= _numCols) {
    return (void*)&_outsideMapVal;
  } else {
    return (void*)&(dataAddr[memRow*_numCols + memCol]);
  }
}


template <class T>
int CLayer<T>::clearMemRow(int memRow) {
  for(int memCol=0; memCol < _numCols; memCol++) {
    dataAddr[memRow*_numCols + memCol] = _noDataVal;
  }
  return CML_OK;
}


template <class T>
int CLayer<T>::clearMemCol(int memCol) {
  for(int memRow=0; memRow < _numRows; memRow++) {
    dataAddr[memRow*_numCols + memCol] = _noDataVal;
  }
  return CML_OK;
}


#endif
