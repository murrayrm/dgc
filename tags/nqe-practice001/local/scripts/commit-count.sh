#! /bin/sh
if (( $#!=1 ))
  then echo "usage: commit-count.sh repos-name"
  echo "prints a sorted list of svn commits by engineer"
  exit 2
fi
tmpfile=`mktemp /tmp/svnlog-temp.XXXXXX`
#svn log file:///dgc/subversion/system | egrep '^r[0-9]+ ' > $tmpfile 
svn log svn+ssh://grandchallenge.caltech.edu/dgc/subversion/$1 | egrep '^r[0-9]+ ' > $tmpfile 
{ for i in `ls /home`
  do echo -ne $i\\t;
    grep $i $tmpfile | wc -l;
  done;
} | sort -nr -k 2
