#ifndef SIMMODELSIMPLE_HH
#define SIMMODELSIMPLE_HH

using namespace std;
#include <math.h>
#include <iostream>
#include <string>

#include "AliceConstants.h"

class SimModelSimple
{
	StateReport m_report;

  double m_phi;   // current steering command
  double m_acc;   // current pedal command
  double m_trans; // current transmission command

public:
  // Constructors/Destructor
  SimModelSimple(){};
  ~SimModelSimple(){};
  
  // Initialization
  void Init(StateReport& initial, double iTime)
	{
		memcpy(&m_report, &initial, sizeof(m_report));
	}

  void set_front_state(double var)
  {
  }
  
  // Gives the simulator the time delta  since last update and then
  // solves the diff eqs forward to arrive at the new state.  
  void RunSim(double step)
	{
		double vold = m_trans*hypot(m_report.nd, m_report.ed);
		double vnew = vold + m_trans*step * m_acc;

		double oldnd = m_report.nd;
		double olded = m_report.ed;

		m_report.phi  = m_phi;
		m_report.yawd = vnew / VEHICLE_WHEELBASE * sin(m_report.phi);
		m_report.yaw += step * m_report.yawd;
		m_report.nd   = vnew * cos(m_report.yaw + m_report.phi);
		m_report.ed   = vnew * sin(m_report.yaw + m_report.phi);
		m_report.n   += step * m_report.nd;
		m_report.e   += step * m_report.ed;
		m_report.ndd  = (m_report.nd - oldnd) / step;
		m_report.edd  = (m_report.ed - olded) / step;
	}

  // Sets the current actuator commands
  void SetCommands(double SteerCmd, double PedalCmd, double TransCmd)
	{
	  m_phi = VEHICLE_MAX_AVG_STEER * SteerCmd;
	  m_acc = PedalCmd;
	  m_trans = TransCmd;
	}

  // Returns the state of the center front axle
  StateReport GetFrontState()
	{
		return m_report;
	}
};

#endif
