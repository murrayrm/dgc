#include "stdlib.h"
#include "time.h"
#include "math.h"
#include "stdio.h"
#include "string.h"

double urand();

int main(int argc, char **argv) {
  // parse arguments
  if (argc != 3) {
    printf("Syntax: autotuner exponentialDecayExponent tempFractionCutoff\n");
    exit(0);
  }
  
  double alpha = atof(argv[1]);
  double tempFractionCutoff = atof(argv[2]);
  double kfX[10];

  const double meanWeights[10] = {0.08, 0.08, 0.04, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
  const double jumpWeights[10] = {0.32, 0.32, 0.16, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
  const double maxWeights[10] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
 
  // mean specs currently from gps docs of performance specs or from guesses.  Update!
  const double meanSpecs[10] = {2.3518e-8, 2.3518e-8, 0.3, 0.01, 0.01, 0.01, 1e-13, 1e-13, 1e-13, 1e-3};
  const double jumpSpecs[10] = {7.8393e-9, 7.8393e-9, 0.05, 0.005, 0.005, 0.005, 1e-17, 1e-17, 1e-17, 1e-4};
  const double maxSpecs[10] = {3.1357e-8, 3.1357e-8, 0.2, 0.02, 0.02, 0.02, 1e-12, 1e-12, 1e-12, 1e-2};

  double oldCost = 0;
  double oldpVals[10];
  double oldqVals[10];
  double oldpError;

  double cost = 0;
  double pVals[10];
  double qVals[10];
  double pError;

  double initialCost;

  // Read in kalman.config
  FILE *cf = fopen("config/kalman.config", "r");
  char line[50];
  char errorCov[50];
  double tempCov;
  int i;

  // Read the entire file.
  if(cf != NULL) {
    while(fgets(line, sizeof(line), cf)) {
      if(sscanf(line, "%s\n", errorCov) == 1) {
	if(strcmp(errorCov, "P:") == 0) {
	  for(i = 0; i < 10; i++) {
	    fgets(line, sizeof(line), cf);
	    if(sscanf(line, "%lf\n", &tempCov) == 1) {
	      oldpVals[i] = tempCov;
	    } else {
	      printf("Error reading pVals[%d] in kalman.config.\n", i);
	      exit(1);
	    }
	  }
	} else if(strcmp(errorCov, "Q:") == 0) {
	  for(i = 0; i < 10; i++) {
	    fgets(line, sizeof(line), cf);
	    if(sscanf(line, "%lf\n", &tempCov) == 1) {
	      oldqVals[i] = tempCov;
	    } else {
	      printf("Error reading qVals[%d] in kalman.config.\n", i);
	      exit(1);
	    }	    
	  }
	} else if(strcmp(errorCov, "pError:") == 0) {
	  fgets(line, sizeof(line), cf);
	  if(sscanf(line, "%lf\n", &tempCov) == 1) {
	    oldpError = tempCov;
	  } else {
	    printf("Error reading pError in kalman.config.\n");
	    exit(1);
	  }
	} else {
	  // Nothing to do.
	}
      }
    }
  }
  fclose(cf);

  // Initialize values to be uses for a cost indicator.
  double meanError[10] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
  double maxError[10] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

  int count = 0;

  // Read in autotune.dat for initial analysis
  system("./astate -f RoseBowlTest -wxa");
  cf = fopen("autotune.dat", "r");
  char kfXline[1000];
 
  if (cf != NULL) {
    while(fgets(kfXline, sizeof(kfXline), cf)) {
      if (sscanf(kfXline, "%le %le %le %le %le %le %le %le %le %le \n", &kfX[0], &kfX[1], &kfX[2], &kfX[3], &kfX[4], &kfX[5], &kfX[6], &kfX[7], &kfX[8], &kfX[9]) == 10) {
	count++;
	meanError[0] += fabs(kfX[0]);
	meanError[1] += fabs(kfX[1]);
	meanError[2] += fabs(kfX[2]);
	meanError[3] += fabs(kfX[3]);
	meanError[4] += fabs(kfX[4]);
	meanError[5] += fabs(kfX[5]);
	meanError[6] += fabs(kfX[6]);
	meanError[7] += fabs(kfX[7]);
	meanError[8] += fabs(kfX[8]);
	meanError[9] += fabs(kfX[9]);
	
	maxError[0] = (fabs(kfX[0]) > maxError[0] ? fabs(kfX[0]) : maxError[0]);
	maxError[1] = (fabs(kfX[1]) > maxError[1] ? fabs(kfX[1]) : maxError[1]);
	maxError[2] = (fabs(kfX[2]) > maxError[2] ? fabs(kfX[2]) : maxError[2]);
	maxError[3] = (fabs(kfX[3]) > maxError[3] ? fabs(kfX[3]) : maxError[3]);
	maxError[4] = (fabs(kfX[4]) > maxError[4] ? fabs(kfX[4]) : maxError[4]);
	maxError[5] = (fabs(kfX[5]) > maxError[5] ? fabs(kfX[5]) : maxError[5]);
	maxError[6] = (fabs(kfX[6]) > maxError[6] ? fabs(kfX[6]) : maxError[6]);
	maxError[7] = (fabs(kfX[7]) > maxError[7] ? fabs(kfX[7]) : maxError[7]);
	maxError[8] = (fabs(kfX[8]) > maxError[8] ? fabs(kfX[8]) : maxError[8]);
	maxError[9] = (fabs(kfX[9]) > maxError[9] ? fabs(kfX[9]) : maxError[9]);
      }
    }
  }
  fclose(cf);

  for(i = 0; i < 10; i++) {
    meanError[i] /= (double)count;
    oldCost += meanError[i]/meanSpecs[i]*meanWeights[i];
    if (i == 0 || i == 1 || i == 2 || i == 9) {
      oldCost += oldpError*meanError[i]/jumpSpecs[i]*jumpWeights[i];
    } else {
      oldCost += meanError[i]/jumpSpecs[i]*jumpWeights[i];
    }
    oldCost += maxError[i]/maxSpecs[i]*maxWeights[i];
  }

  initialCost = oldCost;

  printf("Initial Cost: %lf\n", initialCost);

  // Start with a high temperature (changes are ~0.01*cost)
  double temperature = initialCost*0.1;
  double initialTemperature = temperature;
  double prob;
  
  srand(time(NULL));

  do {
    // add random errors; (2*urand - 1) is uniform from 0 to 1
    for(i = 0; i < 10; i++) {
      if(urand() > 0.80) {
	pVals[i] = oldpVals[i]*(1.0 + (2.0*urand() - 1.0)*0.0625);
      } else {
	pVals[i] = oldpVals[i];
      }
      if(urand() > 0.80) {
	qVals[i] = oldqVals[i]*(1.0 + (2.0*urand() - 1.0)*0.0625);
      } else {
	qVals[i] = oldqVals[i];
      }
    }
    if(urand() > 0.80) {
      pError = oldpError*(1.0 + (2.0*urand() - 1.0)*0.0625);
    } else {
      pError = oldpError;
    }

    // write to file

    cf = fopen("config/kalman.config", "w");
    
    fprintf(cf, "pError:\n");
    fprintf(cf, "%.8le\n", pError);
    fprintf(cf, "\nP:\n");
    for (i = 0; i < 10; i++) {
      fprintf(cf, "%.8le\n", pVals[i]);
    }
    fprintf(cf, "\nQ:\n");
    for (i = 0; i < 10; i++) {
      fprintf(cf, "%.8le\n", qVals[i]);
    }

    printf("pError:\n");
    printf("%.8le\n", pError);
    printf("\nP:\n");
    for (i = 0; i < 10; i++) {
      printf("%.8le\n", pVals[i]);
    }
    printf("\nQ:\n");
    for (i = 0; i < 10; i++) {
      printf("%.8le\n", qVals[i]);
    }


    fclose(cf);

    // run astate

    system("./astate -f RoseBowlTest -wxa");

    // repeat above steps

    cf = fopen("autotune.dat", "r");
    char kfXline[1000];
    
    count = 0;

    for(i = 0; i < 10; i++) {
      meanError[i] = 0.0;
      maxError[i] = 0.0;
    }

    if (cf != NULL) {
      while(fgets(kfXline, sizeof(kfXline), cf)) {
	if(sscanf(kfXline, "%le %le %le %le %le %le %le %le %le %le \n", &kfX[0], &kfX[1], &kfX[2], &kfX[3], &kfX[4], &kfX[5], &kfX[6], &kfX[7], &kfX[8], &kfX[9]) == 10) {
	  count++;
	  meanError[0] += fabs(kfX[0]);
	  meanError[1] += fabs(kfX[1]);
	  meanError[2] += fabs(kfX[2]);
	  meanError[3] += fabs(kfX[3]);
	  meanError[4] += fabs(kfX[4]);
	  meanError[5] += fabs(kfX[5]);
	  meanError[6] += fabs(kfX[6]);
	  meanError[7] += fabs(kfX[7]);
	  meanError[8] += fabs(kfX[8]);
	  meanError[9] += fabs(kfX[9]);
	  
	  maxError[0] = (fabs(kfX[0]) > maxError[0] ? fabs(kfX[0]) : maxError[0]);
	  maxError[1] = (fabs(kfX[1]) > maxError[1] ? fabs(kfX[1]) : maxError[1]);
	  maxError[2] = (fabs(kfX[2]) > maxError[2] ? fabs(kfX[2]) : maxError[2]);
	  maxError[3] = (fabs(kfX[3]) > maxError[3] ? fabs(kfX[3]) : maxError[3]);
	  maxError[4] = (fabs(kfX[4]) > maxError[4] ? fabs(kfX[4]) : maxError[4]);
	  maxError[5] = (fabs(kfX[5]) > maxError[5] ? fabs(kfX[5]) : maxError[5]);
	  maxError[6] = (fabs(kfX[6]) > maxError[6] ? fabs(kfX[6]) : maxError[6]);
	  maxError[7] = (fabs(kfX[7]) > maxError[7] ? fabs(kfX[7]) : maxError[7]);
	  maxError[8] = (fabs(kfX[8]) > maxError[8] ? fabs(kfX[8]) : maxError[8]);
	  maxError[9] = (fabs(kfX[9]) > maxError[9] ? fabs(kfX[9]) : maxError[9]);
	}
      }
    }

    fclose(cf);

    cost = 0;

    for(i = 0; i < 10; i++) {
      meanError[i] /= (double)count;
      cost += meanError[i]/meanSpecs[i]*meanWeights[i];
      if (i == 0 || i == 1 || i == 2 || i == 9) {
	cost += pError*meanError[i]/jumpSpecs[i]*jumpWeights[i];
      } else {
	cost += meanError[i]/jumpSpecs[i]*jumpWeights[i];
      }
      cost += maxError[i]/maxSpecs[i]*maxWeights[i];
    }

    // evaluate with classical SA: always allow down, sometimes allow up.
    if (cost <= oldCost) {
      for (i = 0; i < 10; i++) {
	oldpVals[i] = pVals[i];
	oldqVals[i] = qVals[i];
      }
      oldpError = pError;
      oldCost = cost;
    } else {
      prob = exp(-(cost - oldCost)/temperature);
      printf("Prob: %le, %le\n", cost, temperature);
      if (urand() < prob) {
	for (i = 0; i < 10; i++) {
	  oldpVals[i] = pVals[i];
	  oldqVals[i] = qVals[i];
	}
	oldpError = pError;
	oldCost = cost;
      }
    }

    // Decay temperature exponentially;
    temperature *= (1.0 - alpha);

    printf("CurrentCost: %.8le, tempFraction: %.3lf\n", oldCost, temperature/initialTemperature);

  } while (temperature > initialTemperature*tempFractionCutoff);

  // Write final configuration

  printf("OldCost: %.8le\n", initialCost);
  printf("NewCost: %.8le\n", oldCost);

  cf = fopen("config/kalman.config", "w");
  
  fprintf(cf, "pError:\n");
  fprintf(cf, "%.8le\n", oldpError);
  fprintf(cf, "\nP:\n");
  for (i = 0; i < 10; i++) {
    fprintf(cf, "%.8le\n", oldpVals[i]);
  }
  fprintf(cf, "\nQ:\n");
  for (i = 0; i < 10; i++) {
    fprintf(cf, "%.8le\n", oldqVals[i]);
  }
  
  fclose(cf);

  // Clean up after spread.
  system("rm core.*");  
  
  return 0;
}

double urand() {
  return (double)rand()/(RAND_MAX + 1.0);
}

