 
/* SerialDevice.hpp file.  For initiating serial port and sending packets */
/* to and from port*/

//Written by Sam Pfister 7/25/04


#ifndef SERIAL_HPP
#define SERIAL_HPP


//#include <sys/types.h>
//#include <sys/signal.h>
//#include <sys/stat.h>

#include <fcntl.h>
#include <termios.h>
//#include <stdio.h>
//#include <unistd.h>
#include <string>
#include <iostream>
#include <fstream>
//#include <time.h>
#include <cassert>

using namespace std;


#define DEBUG false
#define DEBUG_INPUT false
#define DEBUG_OUTPUT false


#define SERIAL_MAX_STRING_SIZE 25  // maximum 
#define TIMER 50000			// wait time for port read

#define DEFAULT_MAXREADBUFF = 20;



//////////////////////////////////////////////////////
// SERIAL PORT CLASS DEFINITION

class SerialDevice {
public:
  SerialDevice() ;
	
  ~SerialDevice()  ;

  SerialDevice(const SerialDevice& s) ;
  SerialDevice& operator= (const SerialDevice& s) ;
  void set_debug_input(bool val) {_debug_input = val;};
  void set_debug_output(bool val) {_debug_input = val;};

	int initport(const string& device, int baud)  ;
  void closeport() ;
  string readport(int insize = 15) ;
  void writeport(const string & outstring) ;
  
  
  
private:
  
  int _fd;  //port label
  int _readsleeptime;
  int _readtrytime;
	bool _debug_input;
	bool _debug_output;
  struct termios _oldtio;


};


#endif
























