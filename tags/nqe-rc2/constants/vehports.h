/*
 * vehports.h - default port assignments for Alice
 *
 * JCL, 2 Dec 04
 *
 */

#ifndef VEHPORTS_H
#define VEHPORTS_H

// state sensor port definitions
// NOTE:  These port definitions are for Magnesium, not titanium
#define IMU_IP_PORT 9000		/* IP port number */
#define GPS_SERIAL_PORT 4		/* serial port */
#define OBD_SERIAL_PORT 0               // OBDII serial port
#define MAG_SERIAL_PORT 6       /* magneotmer serial port */

#define BRAKE_SERIAL_PORT 2		/* serial port */
#define STEER_SERIAL_PORT 1     /* steering serial port */
#define THROTTLE_SERIAL_PORT 3     //temporary throttle port JCL 12/2/04

/* Parallel ports */
#define JOYSTICK_PORT 0			/* parallel port */
#define TRANS_PORT 0           //parallel port on VManage computer (magnesium)
#define ESTOP_PORT 1           //parallel port on VManage computer (magnesium)

enum {
  PP_STEER,           /* The steering wheel / joystick */
  PP_NULL,
  PP_GASBR,           /* gas, brake sensors */
  PP_TRANS,           /* Transmission controller */
  PP_NUM_PORTS
};

#endif   //  VEHPORTS_H
