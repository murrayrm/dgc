//SUPERCON STRATEGY TITLE: Unseen-Obstacle - ANYTIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyUnseenObstacle.hh"
#include "StrategyHelpers.hh"

bool CStrategyUnseenObstacle::leave_unseenObstacle_conditions( bool checkBool, CStrategyInterface *pStrategyInterface, const SCdiagnostic *pdiag, const char* StrategySpecificStr ) {
  
  if( checkBool == false ) {
    checkBool = trans_EstopPausedNOTsuperCon( (*pdiag), StrategySpecificStr );
  }
  //Don't check for Pln->!NFP, as this is unseen-obstacle
  return checkBool;
}


void CStrategyUnseenObstacle::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE
  currentStageName = unseenobstacle_stage_names_asString( unseenobstacle_stage_names(stgItr.nextStage()) );
  
  switch( stgItr.nextStage() ) {

  case s_unseenobstacle::estop_pause: 
    //superCon e-stop pause Alice to prepare for a gear change as it has
    //been determined that there is an unseen obstacle in front of Alice's
    //current position
    {

      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = leave_unseenObstacle_conditions( skipStageOps, m_pStrategyInterface, m_pdiag, strprintf( "UnseenObstacle: %s", currentStageName.c_str() ) );
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */      

      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_superConEstop(estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "UnseenObstacle - Stage - %s", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;
    
  case s_unseenobstacle::add_map_obst: 
    //Wait for Alice to be stationary, and then paint a layer of superCon
    //obstacles into the map in front of Alice
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = leave_unseenObstacle_conditions( skipStageOps, m_pStrategyInterface, m_pdiag, strprintf( "UnseenObstacle: %s", currentStageName.c_str() ) );
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->superConPaused == false ) {
	//NOT superCon paused - need to be paused & stationary prior to modifying
	//the superCon layer in the map

	SuperConLog().log( STR, WARNING_MSG, "UnseenObstacle: superCon e-stop pause not yet in place -> will poll");
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );
      }
      
      if ( m_pdiag->aliceStationary == false ) {
	SuperConLog().log( STR, WARNING_MSG, "UnseenObstacle: Alice not yet stationary won't update superCon map yet -> will poll");
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_UnseenObstWaitForStationary, stgItr.currentStageCount() );
      }

      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	//add a superCon obstacle to the superCon map layer
	m_pStrategyInterface->stage_addForwardUnseenObstacle();
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "UnseenObstacle - Stage - %s", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;

  case s_unseenobstacle::obst_wait: 
    //wait for PLN->NFP through a superCon obstacle (verifies that the map updates have
    //been applied), then transition to the LturnReverse strategy
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = leave_unseenObstacle_conditions( skipStageOps, m_pStrategyInterface, m_pdiag, strprintf( "UnseenObstacle: %s", currentStageName.c_str() ) );
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      if( m_pdiag->plnNFP_SuperC == false ) {
	//map updates not yet applied - wait
 	SuperConLog().log( STR, WARNING_MSG, "UnseenObstacle: PLN !-> NFP through superCon obstacle - map updates not applied, will poll");
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_mapdeltasapplied, stgItr.currentStageCount() );
      }
      
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	transitionStrategy(StrategyLturnReverse, "UnseenObstacle: added detected superCon obstacle to the map - transition to LturnReverse");
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "UnseenObstacle: added detected superCon obstacle to the map - transition to LturnReverse");
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "UnseenObstacle - Stage - %s", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyUnseenObstacle::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyUnseenObstacle::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: UnseenObstacle - default stage reached!");
    }
  }
}


void CStrategyUnseenObstacle::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "UnseenObstacle - Exiting");
}

void CStrategyUnseenObstacle::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset();
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "UnseenObstacle - Entering");
}
