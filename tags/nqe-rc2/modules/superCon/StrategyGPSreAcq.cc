//SUPERCON STRATEGY TITLE: GPS re-acquisition - ANYTIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyGPSreAcq.hh"
#include "StrategyHelpers.hh"

//Check for ANYTIME EXIT condition - used by astate to 'cancel' an 'active'
//GPSreAcq request.
bool CStrategyGPSreAcq::checkAstateMoveForwardNoClear( bool checkBool, const SCdiagnostic *pdiag ) {
  
  if( !checkBool ) {
    if( pdiag->astate_move_without_clear == true && pdiag->astNeedToIncludeGPSafterReAcq == false) {
      checkBool = true;
      transitionStrategy( StrategyNominal, "GPSreAcq: astate was not able to include the new GPS signal --> NOMINAL to proceed" );
      SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "GPSreAcq: astate was not able to include the new GPS signal --> NOMINAL to proceed" );
    }
  }  
  return checkBool;
}

void CStrategyGPSreAcq::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE (this MUST remain here)
  currentStageName = gpsreacq_stage_names_asString( gpsreacq_stage_names(stgItr.nextStage()) );

  switch( stgItr.nextStage() ) {
    
  case s_gpsreacq::estop_pause: 
    //astate has requested superCon ready the system for re-inclusion of the
    //re-acquired GPS signal - hence superCon e-stop pause Alice
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
#warning Need to determine what checks are required in GPS re-acq strategy at a strategy level
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAstateMoveForwardNoClear( skipStageOps, m_pdiag );
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;
    
  case s_gpsreacq::confirm_pause_to_astate: 
    //confirm that Alice is paused, and stationary and then signal to
    //astate that it should include the GPS signal (as it is safe
    //for our state estimate to jump
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */      
      /*** SEE #WARNING ABOVE, SHOULD GPS RE-ACQ BE AN ANYTIME BLOCKING STRATEGY ***/
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAstateMoveForwardNoClear( skipStageOps, m_pdiag );
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->superConPaused == false ) {
	//NOT yet superCon paused - adrive has not yet processed the command
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "WARN: Alice not yet paused, cannot re-acq GPS - will poll");
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );
      }

      if( m_pdiag->aliceOBD2stationary == false ) {
	
	skipStageOps == true;
	SuperConLog().log( STR, WARNING_MSG, "WARN: Alice not yet stationary & braking (using OBD2) - will poll");
	checkForPersistLoop( this, persist_loop_waitstationary, stgItr.currentStageCount() );	
      }

      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_signalToAstate(sc_interface::ok_to_add_gps);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;    

  case s_gpsreacq::clear_cmap: 
    //send a message to fusionMapper to clear the current cost-map as astate
    //has successfully completed a GPS re-acquisition jump
    {

      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      //NO strategy conditional trans/term checks should be made at this
      //point, as the quality/accuracy of astate's output at this point is
      //highly uncertain
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAstateMoveForwardNoClear( skipStageOps, m_pdiag );      

      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->astate_move_with_clear == false ) {
	//astate not yet determined what action to take
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "WARN: GPSreAcq - astate NOT yet confirmed cost-map clearing required - will poll");

	if( checkForPersistLoop( this, persist_loop_astateResponse, stgItr.currentStageCount() ) ) {
	  //timed-out waiting for astate
	  m_pStrategyInterface->stage_clearMaps( sc_interface::clear_all_maps_everywhere );
	  m_pStrategyInterface->stage_signalToAstate( sc_interface::timed_out_waiting_for_astate );
	  SuperConLog().log( STR, WARNING_MSG, "WARNING: timed-out waiting for astate to confirm move with/without clear -> NOMINAL" );
	}	
      }
      
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	//signal to fusionMapper to clear the current map, as the GPS re-acq
	//is complete, and it is assumed that it resulted in a state jump
	m_pStrategyInterface->stage_clearMaps( sc_interface::clear_all_maps_everywhere );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }            
    }
    break;

  case s_gpsreacq::trans_nominal: 
    //Sent command to clear map to fusionMapper - hence there should
    //NOT be any artifacts in the map resulting from state jumps
    //so return to NOMINAL
    { 
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	//un-pause Alice as the GPS re-acquisition process has been
	//completed --> NOMINAL
	transitionStrategy( StrategyNominal, "GPSreAcq - Strategy complete --> nominal" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;


    /* END OF STRATEGY */

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyGPSreAcq::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyGPSreAcq::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: GPSreAcq - default stage reached!");
    }
  }
}


void CStrategyGPSreAcq::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "GPSreAcq - Exiting");
}

void CStrategyGPSreAcq::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset(); //reset the stage iterator
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "GPSreAcq - Entering");
}


