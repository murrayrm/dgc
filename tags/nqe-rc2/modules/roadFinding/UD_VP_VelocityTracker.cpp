//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_VP_VelocityTracker.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// wrapper around vanishing point POSITION + VELOCITY tracker (4-D) class constructor.  
/// sets up some variables necessary for the particle filter (from which the 
/// VP tracker class is derived) to be initialized properly--particularly 
/// in the initial distribution of particles.   uses defaults for a priori 
/// position grid spacing, sampling covariance

UD_VP_VelocityTracker *initialize_UD_VP_VelocityTracker(UD_VanishingPoint *vp)
{
  UD_VP_VelocityTracker *vt;
  int i, num_samps;
  float fw;
  UD_Vector *initstate, *dstate, *covdiag;

  // spacing of initial grid of particles (in units of pixels)

  dstate = make_UD_Vector("pf_dstate", 4);
  dstate->x[0] = 5;
  dstate->x[1] = 5;
  dstate->x[2] = 0;
  dstate->x[3] = 0;
  
  // first position in initial grid (all initial velocities are 0)

  initstate = make_UD_Vector("pf_state", 4);
  initstate->x[0] = 0.5 * dstate->x[0];
  initstate->x[1] = 0.5 * dstate->x[1];
  initstate->x[2] = 0;
  initstate->x[3] = 0;

  // compute and report number of grid points (one particle per)

  num_samps = vp->cw * vp->ch / (dstate->x[0] * dstate->x[1]);
  printf("num samps = %i (w = %i, h = %i, dx = %.1f, dy = %.1f)\n", num_samps, vp->cw, vp->ch, dstate->x[0], dstate->x[1]);

  // sampling covariance

  // var = sigma^2, 95% within 2*sigma
  // horizontal: 2*sigma = pf_w / 40 => sigma = pf_w / 80 
  // vertical: 2*sigma = pf_w / 57 => sigma = pf_w / 114

  fw = (float) vp->cw;
  
  covdiag = make_UD_Vector("covdiag", 4);
  covdiag->x[0] = SQUARE(fw / 80.0);
  covdiag->x[1] = SQUARE(fw / 114.0);
//   covdiag->x[0] = .01;
//   covdiag->x[1] = .001;

  // variance of velocity terms 

  covdiag->x[2] = 0.1;
  covdiag->x[3] = 0.01;
  
  // instantiate

  vt = new UD_VP_VelocityTracker(vp, num_samps, initstate, covdiag, dstate);

  return vt;
}

//----------------------------------------------------------------------------

/// constructor for vanishing point POSITION + VELOCITY tracker class derived from 
/// vanishing point tracker class

UD_VP_VelocityTracker::UD_VP_VelocityTracker(UD_VanishingPoint *van, int num_particles, UD_Vector *meanstate, UD_Vector *covdiag, UD_Vector *delta) : UD_VP_Tracker(van, num_particles, meanstate, covdiag, delta)
{
  max_velocity = 0.1 * (float) vp->cw;

  // distribute initial samples (must be here instead of in UD_VP_Tracker constructor
  // because pf_samp_prior() is called, which is not defined by UD_VP_Tracker)
  
  initialize_prior();
}

//----------------------------------------------------------------------------

/// put particles in their a priori state--distribute POSITIONS over regular grid 
/// covering candidate region.  state vector is used here as counter variable to
/// tell where we are in the grid; samp is where the current grid position
/// is put

void UD_VP_VelocityTracker::pf_samp_prior(UD_Vector *samp)
{
  if (state->x[0] >= vp->cw) {
    state->x[0] = 0.5 * dstate->x[0];
    state->x[1] += dstate->x[1];
  }

  samp->x[0] = state->x[0];
  samp->x[1] = state->x[1];

  // set initial velocity to constant

  samp->x[2] = initstate->x[2];
  samp->x[3] = initstate->x[3];

  state->x[0] += dstate->x[0];
}

//----------------------------------------------------------------------------

/// apply deterministic motion model to particles: new position = old position
/// + velocity (velocity is implicitly in units of pixels per algorithm iteration).
/// acceleration is not estimated, so velocity itself just random walks

void UD_VP_VelocityTracker::pf_dyn_state(UD_Vector *old_samp, UD_Vector *new_samp)
{
  copy_UD_Vector(old_samp, new_samp);
  new_samp->x[0] += vp_vx(old_samp);
  new_samp->x[1] += vp_vy(old_samp);
}

//----------------------------------------------------------------------------

/// random diffusion: add Gaussian noise to old state sample 

void UD_VP_VelocityTracker::pf_samp_state(UD_Vector *old_samp, UD_Vector *new_samp)
{
  sample_gaussian_diagonal_covariance(new_samp, old_samp, covdiagsqrt);
}

//----------------------------------------------------------------------------

/// evaluate likelihood of one particle given current image.  if position is 
/// outside candidate region, likelihood = 0.  if inside, just return number of
/// votes of nearest bucket in candidate region.  also makes sure that velocity
/// is not too high 

double UD_VP_VelocityTracker::pf_condprob_zx(UD_Vector *samp)
{
  int ix, iy;

  // gate position to stay in candidate region

  if (candidate_vp_x(samp) < 0 || candidate_vp_x(samp) >= vp->cw || candidate_vp_y(samp) < 0 || candidate_vp_y(samp) >= vp->ch)
    return 0;

  // gate magnitude of velocity

  if (hypot(vp_vx(samp), vp_vy(samp)) > max_velocity)
    return 0;

  // bilinear interpolation would be a little better...

  ix = myround(candidate_vp_x(samp));
  iy = myround(candidate_vp_y(samp));

  return FLOAT_IMXY(vp->candidate_image, ix, iy);
}

//----------------------------------------------------------------------------

/// write current estimate of vanishing point VELOCITY (in image coordinates)
/// to file (terminal string is assumed to be newline)

void UD_VP_VelocityTracker::write(FILE *fp)
{
  write(fp, "\n");
}

//----------------------------------------------------------------------------

/// write current estimate of vanishing point VELOCITY (in image coordinates)
/// to file with an argument specifying terminal string (i.e., newline, 
/// comma-space, etc.)

void UD_VP_VelocityTracker::write(FILE *fp, char *terminal_string)
{
  fprintf(fp, "%.2f, %.2f, %.2f, %.2f%s", vp_x(), vp_y(), vp_vx(), vp_vy(), terminal_string);
  fflush(fp);
}

//----------------------------------------------------------------------------

/// draw road curvature tracker parameters, namely the POSITION & VELOCITY of 
/// the current road vanishing point.  assuming window dimensions are same as
/// candidate region

void UD_VP_VelocityTracker::draw(int r, int g, int b)
{
  int i, win_h, win_w;

  win_w = vp->cw;
  win_h = vp->ch;

  // each particles is a red dot with green arrow indicating velocity

  if (show_particles) {

    glColor3ub(0, 255, 0);
    glLineWidth(1);

    glBegin(GL_LINES);

    for (i = 0; i < num_samples; i++) {
      glVertex2f(candidate_vp_x(sample[i]), win_h - candidate_vp_y(sample[i]));
      glVertex2f(candidate_vp_x(sample[i]) + vp_vx(sample[i]), win_h - candidate_vp_y(sample[i]) - vp_vy(sample[i]));  // don't quite under-
                                                                                                                       // stand necessity 
                                                                                                                       // of last negation
    }

    glEnd();

    glColor3ub(255, 0, 0);
    glPointSize(1);

    glBegin(GL_POINTS);

    for (i = 0; i < num_samples; i++) 
      glVertex2f(candidate_vp_x(sample[i]), win_h - candidate_vp_y(sample[i]));

    glEnd();

  }

  // horizon is red line

  glColor3ub(255, 0, 0);
  glLineWidth(1);

  glBegin(GL_LINES);
  
  glVertex2f(0, win_h - candidate_vp_y());
  glVertex2f(win_w, win_h - candidate_vp_y());

  glEnd();

  // state is big (r, g, b) dot with green line indicating velocity

  glColor3ub(r, g, b);
  glPointSize(5);

  glBegin(GL_POINTS);
  glVertex2f(candidate_vp_x(), win_h - candidate_vp_y());
  glEnd();

  glColor3ub(0, 255, 0);
  glLineWidth(2);

  glBegin(GL_LINES);
  glVertex2f(candidate_vp_x(), win_h - candidate_vp_y());
  glVertex2f(candidate_vp_x() + vp_vx(), win_h - candidate_vp_y() + vp_vy());
  glEnd();
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
