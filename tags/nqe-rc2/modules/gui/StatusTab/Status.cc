/**
 * Status.hh
 * Revision History:
 * 05/22/2005  hbarnor  Created
 */

#include "Status.hh"

#include <iostream>

Status::Status(const Glib::ustring &label, string host, bool mnemonic)
  : m_host(host),
    name(label),
    active(-1),
    oldState(-1),
    counter(countdown)
{
  myLabel.add_label(label,mnemonic);
  init();
}

Status::Status(const Glib::ustring &label, Gtk::AlignmentEnum xalign, Gtk::AlignmentEnum yalign, string host, bool mnemonic)
  : m_host(host),
    name(label),
    active(-1),
    oldState(-1),
    counter(countdown)
{
  myLabel.add_label(label,mnemonic, xalign, yalign);
  init();
}

void Status::init()
{  
  GdkColor color; 
  //GdkColor fcolor; 
  gdk_color_parse("#ffff00", &color);
  myLabel.modify_bg(Gtk::STATE_NORMAL,Gdk::Color(&color));
  add(myLabel);
  //gdk_color_parse("red", &fcolor);
  //Glib::RefPtr<Gtk::RcStyle> style = get_modifier_style();
  //style->set_bg(Gtk::STATE_NORMAL,Gdk::Color(&color));
  //
  //style->set_fg(Gtk::STATE_NORMAL,Gdk::Color(&fcolor));
  //modify_style(style);
  show_all_children();
}

Status::~Status()
{
  //nothing yet
}

int Status::getActive()
{
  return active;
}

void Status::setActive(int newState)
{
  if(newState != -1)
    {
      oldState = newState;
    }
  cout << "setting active to " << newState << endl;
  if((oldState != -1) && (newState == -1))
    {
      return;
    }
  else
    {
      active = newState;
    }

}

bool Status::on_timeout(int count)
{
  string display(name);
  //display << " " << active;
  if(counter != 0)
    {
      --counter;
      //cout << counter << endl;
    }
  else
    {
      if((active >= 5) || (active == -10))
	{
	  //cout << "Not active " << endl;
	  GdkColor color; 
	  gdk_color_parse("red", &color);       	
	  myLabel.modify_bg(Gtk::STATE_NORMAL,Gdk::Color(&color));
	  //	  add(myLabel);
	}
      else
	{
	  GdkColor color; 
	  switch(active)
	    {
	    case 0:
	      gdk_color_parse("green", &color);
	      break;
	    case 1:
	      gdk_color_parse("#2f4f4f", &color);
	      break;
	    case 2:
	      gdk_color_parse("#ff8C00", &color);
	      break;
	    case 3:
	      gdk_color_parse("#daa520", &color);
	      break;
	    case 4:
	      gdk_color_parse("#800000", &color);
	      break;
	    default:	      
	      gdk_color_parse("#ffff00", &color);
	      break;
	    }
	  //cout << "Setting active false" << endl;
	  active = -10;	  
	  //myLabel.add_label(display);
	  myLabel.modify_bg(Gtk::STATE_NORMAL,Gdk::Color(&color));
	}
      counter = countdown;
    }
  return true;
}
