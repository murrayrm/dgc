#include "SkynetContainer.h"
#include "DGCutils"
#include <pthread.h>
#include "DGCPlotData.hh"
#include "TabStruct.h"

#define TAB_UPDATE_PERIOD 1000000

#define CModuleTabClientDummy2(x) C##x##TabClient
#define CModuleTabClientDummy1(x) CModuleTabClientDummy2(x)
#define CModuleTabClient CModuleTabClientDummy1(TABNAME)

class CModuleTabClient : virtual public CSkynetContainer
{
	bool                  m_bIAllocatedMemory;

protected:
	SModuleTabInput *     m_pTabInput;
	SModuleTabOutput*     m_pTabOutput;

public:
  CModuleTabClient()
	{
		m_pTabInput  = new SModuleTabInput;
		m_pTabOutput = new SModuleTabOutput;
		m_bIAllocatedMemory = true;

		DGCstartMemberFunctionThread(this, &CModuleTabClient::getTabData);
		DGCstartMemberFunctionThread(this, &CModuleTabClient::sendTabData);
	}

	CModuleTabClient(SModuleTabInput* pTabInput, SModuleTabOutput* pTabOutput)
		: m_pTabInput(pTabInput), m_pTabOutput(pTabOutput)
	{
		m_bIAllocatedMemory = false;

		DGCstartMemberFunctionThread(this, &CModuleTabClient::getTabData);
		DGCstartMemberFunctionThread(this, &CModuleTabClient::sendTabData);
	}

  ~CModuleTabClient()
	{
		if(m_bIAllocatedMemory)
		{
			delete m_pTabInput;
			delete m_pTabOutput;
		}
	}

	void getTabData(void)
	{
		int tabSocket = m_skynet.listen(SNModuleTabOutput, ALLMODULES);

		while(true)
			m_skynet.get_msg(tabSocket, m_pTabOutput, sizeof(*m_pTabOutput), 0);
	}

	void sendTabData(void)
	{
		int tabSocket = m_skynet.get_send_sock(SNModuleTabInput);

		while(true)
		{
			m_skynet.send_msg(tabSocket, m_pTabInput, sizeof(*m_pTabInput), 0);
			DGCusleep(TAB_UPDATE_PERIOD);
		}
	}
};

