#ifndef DISTANCES_HH
#define DISTANCES_HH


#include<iostream>
using namespace std;

//Calculates a bunch of distances between a line segment and a point
// and relative distances from a line to a point relative to line width


/*
**
** Distance calculator, calcs shortest squared distance from line to point
**
*/
class RoadDistance2
{
 public:
  RoadDistance2(double N1, double E1, double N2, double E2) {
    set(N1,E1, N2,E2);
  }

  void set(double N1, double E1, double N2, double E2) {
    m_N[0] = N1;
    m_E[0] = E1;
    m_N[1] = N2;
    m_E[1] = E2;

    m_v1_2 = (m_N[1]-m_N[0])*(m_N[1]-m_N[0]) + (m_E[1]-m_E[0])*(m_E[1]-m_E[0]);
    m_norm_N = (m_N[1]-m_N[0])/m_v1_2;
    m_norm_E = (m_E[1]-m_E[0])/m_v1_2;
  }

  double operator()(double N, double E) {
    //shortest distance from the point to the line is
    //calculated by the orthogonal projection of the point
    //onto the line
    //
    //v1 = (N[1]-N[0], E[1]-E[0])
    //v2 = (N-N[0], E-E[0])
    //
    //            v1 dot v2
    // ort = v2 - --------- * v1
    //              |v1|^2
    //
    // Where the bottom right part is pre-calculated ( v1 / |v1|^2 )

    double dot_prod = (m_N[1]-m_N[0])*(N-m_N[0]) + (m_E[1]-m_E[0])*(E-m_E[0]);

    if(dot_prod<0) {  //Point is behind line
      return (m_N[0]-N)*(m_N[0]-N) + (m_E[0]-E)*(m_E[0]-E);
    }
    if(dot_prod>m_v1_2) {  //Point is in front of line
      return (m_N[1]-N)*(m_N[1]-N) + (m_E[1]-E)*(m_E[1]-E);
    }

    //Orthanogal vector
    double pN = (N - m_N[0]) - dot_prod*m_norm_N;
    double pE = (E - m_E[0]) - dot_prod*m_norm_E;

    return pN*pN + pE*pE;
  }

 protected:
  double m_N[2];
  double m_E[2];

  double m_norm_N;    //Not really normalized, but almost...
  double m_norm_E;
  double m_v1_2;      //squared length of v1
};

/*
**
** Distance calculator, calcs shortest squared distance from line to point
** relative to the road width. i.e 0 is center line, 1 is border line
*/
class RoadDistanceRel2
{
 public:
  RoadDistanceRel2(double N1, double E1, double w1, double N2, double E2, double w2) {
    set(N1,E1,w1, N2,E2,w2);
  }

  void set(double N1, double E1, double w1, double N2, double E2, double w2) {
    m_N[0] = N1;
    m_E[0] = E1;
    m_r[0] = w1/2;
    m_N[1] = N2;
    m_E[1] = E2;
    m_r[1] = w2/2;

    m_v1_2 = (m_N[1]-m_N[0])*(m_N[1]-m_N[0]) + (m_E[1]-m_E[0])*(m_E[1]-m_E[0]);
    m_v1_2_inv_times_dr = 1 / m_v1_2;

    m_norm_N = (m_N[1]-m_N[0]) * m_v1_2_inv_times_dr;
    m_norm_E = (m_E[1]-m_E[0]) * m_v1_2_inv_times_dr;

    m_v1_2_inv_times_dr *= m_r[1]-m_r[0];
  }

  double operator()(double N, double E) {
    //shortest distance from the point to the line is
    //calculated by the orthogonal projection of the point
    //onto the line
    //
    //v1 = (N[1]-N[0], E[1]-E[0])
    //v2 = (N-N[0], E-E[0])
    //
    //            v1 dot v2
    // ort = v2 - --------- * v1
    //              |v1|^2
    //
    // Where the bottom right part is pre-calculated ( v1 / |v1|^2 )
    //
    // The relative position along the line where the projection hits is
    //     v1 dot v2
    // r = ---------
    //       |v1|^2
    //
    // Thus the radius at this point is
    //
    // rad = r1 + r*(r2-r1)
    double dot_prod = (m_N[1]-m_N[0])*(N-m_N[0]) + (m_E[1]-m_E[0])*(E-m_E[0]);

    double r = m_r[0] + dot_prod*m_v1_2_inv_times_dr;

    if(dot_prod<0) {  //Point is behind line
      double d = (m_N[0]-N)*(m_N[0]-N) + (m_E[0]-E)*(m_E[0]-E);
      if(d>m_r[0]*m_r[0])
	return 1.0;
      return d/(m_r[0]*m_r[0]);
    }
    if(dot_prod>m_v1_2) {  //Point is in front of line
      double d = (m_N[1]-N)*(m_N[1]-N) + (m_E[1]-E)*(m_E[1]-E);
      if(d>m_r[1]*m_r[1])
	return 1.0;
      return d/(m_r[1]*m_r[1]);
    }
    
    //Orthanogal vector
    double pN = (N - m_N[0]) - dot_prod*m_norm_N;
    double pE = (E - m_E[0]) - dot_prod*m_norm_E;
    
    double d2 = (pN*pN + pE*pE) / (r*r);
    if(d2>1.0)
      return 1.0;
    return d2;
  }

 protected:
  double m_N[2];
  double m_E[2];
  double m_r[2];

  double m_norm_N;    //Not really normalized, but almost...
  double m_norm_E;
  
  double m_v1_2;
  //Squared length of line vector inverted times (w2-w1)
  double m_v1_2_inv_times_dr;
};

#endif
