#ifndef __CMAPDELTA_HH__
#define __CMAPDELTA_HH__


template <class T>
struct deltaCell{
  double UTMNorthing, UTMEasting;
  T cellValue;
} __attribute__((packed));

struct deltaHeader {
  int numCells;
  double vehicleLocUTMNorthing, vehicleLocUTMEasting;
  double resRows, resCols;
  int numRows, numCols;
  unsigned long long timestamp;
} __attribute__((packed));


class CDeltaList {
public:
  int numDeltas;
  void** deltaList;
  int* deltaSizes;
  deltaHeader* deltaHeaders;
  
  CDeltaList() {
    numDeltas = 0;
    deltaList=(void**)malloc(0);
    deltaSizes=(int*)malloc(0);
    deltaHeaders=(deltaHeader*)malloc(0);
  };
  ~CDeltaList() {
    free(deltaList);
    free(deltaSizes);
    free(deltaHeaders);
  };

  bool isEmpty() {
    if(numDeltas==0) {
      return true;
    } else {
      return false;
    }
  };

  bool isShiftOnly() {
    if(numDeltas==1 && deltaSizes[0]==0) {
      return true;
    } else {
      return false;
    }
  };

  void clearList() {
    deltaList = (void**)realloc(deltaList, 0);
    deltaSizes = (int*)realloc(deltaSizes, 0);
    deltaHeaders = (deltaHeader*)realloc(deltaHeaders, 0);
    numDeltas = 0;
  };

  void addDelta(deltaHeader tempHeader, void* deltaData, int deltaByteSize) {
    numDeltas++;
    deltaSizes = (int*)realloc(deltaSizes, sizeof(int)*(numDeltas));
    deltaList = (void**)realloc(deltaList, sizeof(void*)*(numDeltas));
    deltaHeaders = (deltaHeader*)realloc(deltaHeaders, sizeof(deltaHeader)*(numDeltas));

    deltaSizes[numDeltas-1] = deltaByteSize;
    deltaList[numDeltas-1] = deltaData;
    deltaHeaders[numDeltas-1] = tempHeader;
  };
};


#endif
