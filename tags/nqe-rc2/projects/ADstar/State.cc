//Implementations of the key struct (part of the AD* algorithm's implementation

#include "State.h"
#include <stdlib.h>

using namespace std;

State::key::key(double first, double second)
{
  k1 = first;
  k2 = second;
}

double State::key::getK1()
{
  return k1;
}

double State::key::getK2()
{
  return k2;
}

void State::key::setK1(double newK1)
{
  k1 = newK1;
}

void State::key::setK2(double newK2)
{
  k2 = newK2;
}

bool State::key::operator < (key other) //This operator compares the priority of two key objects (returns true if *this has lower priority than other)
{
  if(getK1() < other.getK1()) //These keys are prioritized lexicographically: K1 is more important than K2.  Therefore, if K1 of *this is less than K2 of other, then *this has a lower priority
    {
      return true;
    }

  else if((getK1() == other.getK1()) && (getK2() < other.getK2())) //If K1 of *this == K1 of other, then *this has lower priority than other iff K2 of *this < K2 of other
    {
      return true;
    }
  else //If we're here, then K1 *this > K1 other of K1 *this == K1 other && K2 *this > K2 other; either way, *this has a higher priority than other, so return false
    {
      return false;
    }
}

State::key::~key()
{
  //All data fields in this class are primitive, so nothing to do here
}

//End key struct implementation






//Part of the State struct (part of the AD* algorithm's implemenatation)

State::State()
{
  northing = 0;
  easting = 0;
  g = 0;
  rhs = 0;
  state_key = NULL;
  alreadyVisited = false;
}

State::State(double initNorthing, double initEasting, double gVal, double rhsVal, double key1, double key2)
{
  northing = initNorthing;
  easting = initEasting;
  g = gVal;
  rhs = rhsVal;
  state_key = new key(key1, key2);
  alreadyVisited = false;
}

State::State(double initNorthing, double initEasting, double gVal, double rhsVal, key* k)
{
  northing = initNorthing;
  easting = initEasting;
  g = gVal;
  rhs = rhsVal;
  state_key = k;
  alreadyVisited = false;
}

State::State(double initNorthing, double initEasting, key* k)
{
  northing = initNorthing;
  easting = initEasting;
  g = INFINITE;
  rhs = INFINITE;
  state_key = k;
  alreadyVisited = false;
}

State::State(State* s)
{
  northing = s->getNorthing();
  easting = s->getEasting();
  g = s->getg();
  rhs = s->getrhs();
  state_key = s->getKey();
  alreadyVisited = s->getAlreadyVisited();
}

State* State::operator = ( State* s)
{
  if (this != NULL)
    {
      northing = s->getNorthing();
      easting = s->getEasting();
      g = s->getg();
      rhs = s->getrhs();
      setKey(s->getKey());
      setAlreadyVisited(s -> getAlreadyVisited());
    }
  else
    {
      *this = new State(s->getNorthing(), s->getEasting(), s->getg(), s->getrhs(),s->getKey());
      this -> setAlreadyVisited(s -> getAlreadyVisited());
    }
  return this;
}

bool State:: operator < (State* s)
{
  return ( (*getKey()) < ( *(s->getKey()) ) );
}

void State::setNorthing(double newNorthing)
{
  northing = newNorthing;
}

void State::setEasting(double newEasting)
{
  easting = newEasting;
}

void State::setg(double newG)
{
  g = newG;
}

void State::setrhs(double newRHS)
{
  rhs = newRHS;
}

void State::setAlreadyVisited(bool tf)
{
  alreadyVisited = tf;
}

void State::setKey(key* k)
{
  if(getKey() != NULL)
    {
      key* temp = state_key;
      state_key = k;
      delete temp;
    }
  else
    {
      state_key = k;
    }
}

void State::setKey(double key1, double key2)
{
  setKey(new key(key1, key2));
}

State::key* State::getKey()
{
  return state_key;
}

const double State::getg() const
{
  return g;
}

const double State::getrhs() const
{
  return rhs;
}

const double State::getNorthing() const
{
  return northing;
}

const double State::getEasting() const
{
  return easting;
}

const bool State::getAlreadyVisited() const
{
  return alreadyVisited;
}

void State::print()
{
  cout<<"Northing = "<<getNorthing()<<endl;
  cout<<"Easting = "<<getEasting()<<endl;
  cout<<"g = "<<getg()<<endl;
  cout<<"rhs = "<<getrhs()<<endl;
  if(getKey() != NULL)
    {
      cout<<"k1 = "<<getKey()->getK1()<<endl;
      cout<<"k2 = "<<getKey()->getK2()<<endl;
    }
  else
    {
      cout<<"This State has a null key"<<endl;
    }
  cout<<endl;
}

State::~State()
{
  if(state_key != NULL)
    {
      delete state_key;
    }
}
