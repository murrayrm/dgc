//Implementation of AD* planning algorithm

#include "ADstar.h"
#include <math.h>

double e;  //current epsilon value for this iteration of the plan.

bool DontNeedToReplan = true; //Keeps track of whether we need to generate a new plan from scratch
bool NoNewMapUpdates = true;


using namespace std;

//Here's the implemenatation of the actual algorithm itself:

//Creates a CMapPlus object
ADstar::ADstar(int sn_key, bool bWaitForStateFill, double epsilon, double decrement)
  :CSkynetContainer(SNplanner, sn_key), rddf("rddf.dat"), CStateClient(bWaitForStateFill), receivedAtLeastOneDelta(false), haveMap(false), EPSILON_INIT(epsilon), EPSILON_DECREMENT(decrement) //Create a new link to skynet, load the RDDF file "rddf.dat", get ready to receive state data, initilize some bool variables.

{
  //Create a new buffer for map deltas
  mapDelta = new char[MAX_DELTA_SIZE];
  mapRequestSocket = m_skynet.get_send_sock(SNfullmaprequest);
  mapDeltaSocket = m_skynet.listen(SNfusiondeltamap, SNfusionmapper);
  CStateClient::UpdateState();  //Get and store vehicle state info
  vehicleUTMNorthing = m_state.Northing_rear();
  vehicleUTMEasting = m_state.Easting_rear();
  setgoal();  //Set the coordinates of our initial goal.
 
  //Initialize the map
  m_map.initMap(vehicleUTMNorthing, vehicleUTMEasting, FUSIONMAPPER_NUMROWSCOLS, FUSIONMAPPER_NUMROWSCOLS, FUSIONMAPPER_RESROWSCOLS, FUSIONMAPPER_RESROWSCOLS, 0);

  #warning: "This should be changed to take in the max speed from the RDDF corridor that we're currently in."
  speedLayer = m_map.addLayer<double>((double) SPEED_NO_DATA, 0.01);

  StateLayer = m_map.addLayer<State*>(NULL, NULL);
  mapRows = FUSIONMAPPER_NUMROWSCOLS;
  mapCols = FUSIONMAPPER_NUMROWSCOLS;
  cellSizeN = FUSIONMAPPER_RESROWSCOLS;
  cellSizeE = FUSIONMAPPER_RESROWSCOLS;
  topLeftUTMNorthing = m_map.getWindowTopRightUTMNorthing();
  topLeftUTMEasting = m_map.getWindowBottomLeftUTMEasting();
  bool bRequestMap = true;
  m_skynet.send_msg(mapRequestSocket, &bRequestMap, sizeof(bool), 0);  //Request and then receive initial map info.
  mapDeltaSocket = m_skynet.listen(SNfusiondeltamap, SNfusionmapper);
  if(mapDeltaSocket < 0)
    {
      cerr << "ADstar:: getMapDeltas(): skynet listen returned error." << endl;
    }
  
  
  int deltasize;
  RecvMapdelta(mapDeltaSocket, mapDelta, &deltasize);
  
  //DGClockMutex(&m_mapMutex);
  
  m_map.applyDelta<double>(speedLayer, mapDelta, deltasize);
  
  //DGCunlockMutex(&m_mapMutex);
  cerr << "Applied mapdelta" << endl;
  

  //Initialize the State layer
  for(int r = 0; r < mapRows; r++)  //For all states in StateLayer
    {
      for(int c = 0; c < mapCols; c++)
	{
	  double  n,  e,  rhs;
	  m_map.Win2UTM(r, c, &n, &e);  //Calculate and store the northing and easting values of the state.

	  #warning: "This should be changed to accept the max speed value in the current RDDF corridor."
	  rhs = sqrt(pow( (goalUTMNorthing - vehicleUTMNorthing), 2) + pow( (goalUTMEasting - vehicleUTMEasting), 2) ) / SPEED_NO_DATA;  //Set the rhs value to the time it would take to traverse a straightline path to the goal at max allowable speed.
	  m_map.setDataWin(StateLayer, r, c, new State(n, e, rhs, rhs));  //Initialize the state at row r, column c in the map with its northing, easting, rhs, and g values.
	}
    }

  receivedAtLeastOneDelta = true;
}


//Primary AD* algorithms

void ADstar::key(State* s)
{
  double stateG = s -> getg();
  double stateRHS = s -> getrhs();

  if(stateG > stateRHS)
    {
      s -> setKey(stateRHS + e * h(getCurrentState(), s), stateRHS);
    }

  else
    {
      s -> setKey(stateG + h(getCurrentState(), s), stateG);
    }
}  




void ADstar::UpdateState(State* s)
{
  if(!(s -> getAlreadyVisited() ) )  //We haven't visited this state yet
    {
      s -> setg(INFINITE);
    }

  if( s != getgoal() )
    {
      s -> setrhs( calcrhs(s) );
    }

  OPEN.remove(s);

  if( (s -> getg() ) != ( s -> getrhs() ) )
    {
      //Find out if s is an element of CLOSED
      list<State*>:: iterator pointer = CLOSED.getQueueIterator();
      int size = CLOSED.size();
      int pos = 1;
      
      while( (s != (*pointer) ) && (pos <= size) )  //We haven't found a pointer to s yet and we haven't reached the end of the list
	{
	  pointer++;
	  pos++;
	}

      if(s != (*pointer) )  //If this is true upon while loop termination, then the pointer to s wasn't in the queue CLOSED, so s wasn't in CLOSED
	{
	  key(s);
	  OPEN.push(s);
	}

      else
	{
	  INCONS.push(s);
	}
  
  }
}




void ADstar::ComputeOrImprovePath()
{
  while( (  *(OPEN.peek() -> getKey())  <  *(getCurrentState() -> getKey())  ) || ( (getCurrentState() -> getrhs() ) !=  (getCurrentState() -> getg() ) ) )
    {
      State* s = OPEN.pop();

      if( (s -> getg() ) > (s -> getrhs() ) )
	{
	  s -> setg( s-> getrhs() );
	  CLOSED.push(s);

	  //This next block of code calls UpdateState on each of the predecessors of s
	  int size;
	  State** temp = getPred(s, &size);

	  for(int i = 0; i < size; i++)
	    {
	      UpdateState( temp[i] );
	    }

	  delete [] temp;  //Don't forget to delete memory!

	}

      else
	{
	  s -> setg(INFINITE);

	  //This next block of code calls UpdateState on each of the predecessors of s
	  int size;
	  State** temp = getPred(s, &size);

	  for(int i = 0; i < size; i++)
	    {
	      UpdateState( temp[i] );
	    }

	  delete [] temp;  //Don't forget to delete memory!

	  UpdateState(s);

	}
    }
}





//Main planning loop, i.e., THE BIG ONE.  This r0xx0rs my s0xx0rs 'cause it's super 31337 :-).
void ADstar::plan()
{
  while(true)
    {
      DontNeedToReplan = true;
      (getCurrentState()) -> setg(INFINITE);
      (getCurrentState()) -> setrhs(INFINITE);
      (getgoal() ) -> setg(INFINITE);
      (getgoal() ) -> setrhs(0);
      
      e = EPSILON_INIT; //initialize the epsilon value;
      
      OPEN.clear();
      CLOSED.clear();
      INCONS.clear();
      
      key(getgoal());  //Set the goal key
      OPEN.push(getgoal());
      
      ComputeOrImprovePath();
      
      #warning: "Need to implement some form of solution publishing here."
      //Publish this solution
      
      while(DontNeedToReplan) //Refinement stage of the planning loop, i.e., we've generated an initial solution to the path segment that we're working on
	{
	  int tempsize;  //General int used to keep track of the sizes of things

	  State** temp = getMapDeltas(&tempsize);
	  
	  if((tempsize != 0) || (e != 1)) //We have cost changes detected or we haven't yet generated an optimal plan, so do some more planning.  Otherwise, just chillax
	    {
	      if(tempsize > 0) //Cost changes detected
		{
		  for(int i = 0; i < tempsize; i++) //For each State with a change in its cost
		    {
		      int neighborsize;
		      State** neighbors = getPred(temp[i], &neighborsize); //Get all cells whose edge costs are affected by the change in this cell (i.e., all predecessors to this cell)
		      
		      for(int j = 0; j < neighborsize; j++)
			{
			  UpdateState(neighbors [j]); //Update the state of each of the neighbors of cells with cost changes
			}
		      
		      delete [] neighbors;
		    }
		}
	     
		  delete [] temp;
	      
	      if(e > 1)
		{
		  e -= EPSILON_DECREMENT;
		}
	      
	      //Move states from INCONS into OPEN
	      int INCONSsize = INCONS.size();
	      
	      while(INCONSsize > 0)
		{
		  OPEN.push(INCONS.pop());
		  INCONSsize--;
		}
	      
	      //Update the priorities for all states in OPEN according to Key()
	      int OPENsize = OPEN.size();
	      int pos = 1; 
	      
	      list<State*>::iterator pointer = OPEN.getQueueIterator();
	      
	      while(pos <= OPENsize)
		{
		  key( (*pointer)); //Update the state key according to Key()
		  pointer++;
		  pos++;
		}
	      
	      OPEN.sort();
	      
	      CLOSED.clear();
	      
	      ComputeOrImprovePath();
	      
#warning: "Need some way of publishing solutions here."
	      
	      //Publish current solution
	      
	    }


	} //End of refinement stage

    }  //End of planning loop

}  //End of planning method
			   




//Helper methods


void ADstar::resetMap()
{
  DontNeedToReplan = false;  //If we're resetting the map, we need to replan from scratch
  deleteAllStateObjects();

  //Shift the map
  m_map.updateVehicleLoc(vehicleUTMNorthing, vehicleUTMEasting);
  
  //Update map constants
  topLeftUTMNorthing = m_map.getWindowTopRightUTMNorthing();
  topLeftUTMEasting = m_map.getWindowBottomLeftUTMEasting();

  //Reinitialize the State layer.
  for(int r = 0; r < mapRows; r++)  //For all states in StateLayer
    {
      for(int c = 0; c < mapCols; c++)
	{
	  double  n,  e, rhs;
	  m_map.Win2UTM(r, c, &n, &e);  //Calculate and store the northing and easting values of the state.
	  
	  #warning: "This should be changed to take in the max allowable speed for the given RDDF corridor."
	  rhs = sqrt(pow( (goalUTMNorthing - vehicleUTMNorthing), 2) + pow( (goalUTMEasting - vehicleUTMEasting), 2) ) / SPEED_NO_DATA;  //Set the rhs value to the time it would take to traverse a straightline path to the goal at max allowable speed.

	  m_map.setDataWin(StateLayer, r, c, new State(n, e, rhs, rhs));  //Initialize the state at row r, column c in the map with its northing, easting, rhs, and g values.
	}
    }

  //Might also need to update the goal (the target state) and replan.

  if(sqrt(pow( (goalUTMNorthing - vehicleUTMNorthing), 2) + pow( (goalUTMEasting - vehicleUTMEasting), 2) ) < REPLAN_DIST)  //We're too close to the goal - replan
    {
      setgoal();
      //Plan from scratch
    }
}
      
      
      
State** ADstar::getMapDeltas(int *size)
{
  CStateClient::UpdateState();  //Get vehicle state update.
  vehicleUTMNorthing = m_state.Northing_rear();
  vehicleUTMEasting = m_state.Easting_rear();
  
  //May also need to update goal and goal State
  NEcoord loc(vehicleUTMNorthing, vehicleUTMEasting);
  NEcoord goal(goalUTMNorthing, goalUTMEasting);
  if((loc - goal).norm() < REPLAN_DIST)  //We're too close to the goal - replan
    {
     
      *size = 0;
      resetMap();
      setgoal();
      return NULL;
    }

  //If we're too close to the top or bottom edges of the map
  if((vehicleUTMNorthing - m_map.getWindowBottomLeftUTMNorthing() < MIN_DIST_TO_EDGE_OF_MAP) || (m_map.getWindowTopRightUTMNorthing() -  vehicleUTMNorthing < MIN_DIST_TO_EDGE_OF_MAP)) 
    {
      resetMap();
      *size = 0;
      return NULL;
    }
  
  else if((vehicleUTMEasting - m_map.getWindowBottomLeftUTMEasting() < MIN_DIST_TO_EDGE_OF_MAP) || (m_map.getWindowTopRightUTMEasting() -  vehicleUTMEasting < MIN_DIST_TO_EDGE_OF_MAP)) //If we're too close to the left or right edges of the map
    {
      resetMap();
      *size = 0;
      return NULL;
    }  
  else //We're not too close, so just go ahead and receive the map deltas;
    {
      double n, e, newCellVal;
      State** changes;

      mapDeltaSocket = m_skynet.listen(SNfusiondeltamap, SNfusionmapper);
      if(mapDeltaSocket < 0)
	{
	  cerr << "ADstar:: getMapDeltas(): skynet listen returned error." << endl;
	}
      
      int deltasize;
      RecvMapdelta(mapDeltaSocket, mapDelta, &deltasize);
      

      changes = new State*[deltasize];

      *size = deltasize; //Send out the size of the State** array
      //DGClockMutex(&m_mapMutex);
      
      //Apply the map delta
      for(int k = 0; k < deltasize; k++)
      {
	newCellVal = m_map.getDeltaVal<double>(k, speedLayer, mapDelta, &n, &e);  //Get the new val, and the cell's (Northing, Easting coords);
	
	m_map.setDataUTM(speedLayer, n, e, newCellVal);  //Set the new value for the cell in the speed layer.

	changes[k] = m_map.getDataUTM<State*>(StateLayer, n, e); //Add a new State* to the changes array to record that the cell at these coordinates has changed - will need to update the State object here later.
	  
      }
      
      //DGCunlockMutex(&m_mapMutex);
      cerr << "Applied mapdelta" << endl;

      return changes;
    }
}




void ADstar::deleteAllStateObjects()
{
  for(int r = 0;  r < mapRows; r++)
    for(int c = 0; c < mapCols; c++)
      {
	State* temp = m_map.getDataWin<State*>(StateLayer, r, c);
	delete temp;
      }
}


void ADstar::setgoal()
{
  NEcoord goalCoords = rddf.getPointAlongTrackLine(vehicleUTMNorthing, vehicleUTMEasting, PLAN_AHEAD_DIST);
  goalUTMNorthing = goalCoords.N;
  goalUTMEasting = goalCoords.E;
}




State* ADstar:: getgoal()
{
  return m_map.getDataUTM<State*>(StateLayer, goalUTMNorthing, goalUTMEasting);
}



State* ADstar:: getCurrentState()
{
  return m_map.getDataUTM<State*>(StateLayer, vehicleUTMNorthing, vehicleUTMEasting);
}



void ADstar::setState(int r, int c, State* s)
{
  m_map.setDataWin(StateLayer, r, c, s);
}



State* ADstar::getState(int r, int c)
{
  return m_map.getDataWin<State*>(StateLayer, r, c);
}




double ADstar:: calcg(State* s) //Used to calculate the g-value for a state
{
  #warning: "This should be changed to accept the speed limit in the map RDDF for this region."
  return sqrt(pow( (goalUTMNorthing - vehicleUTMNorthing), 2) + pow( (goalUTMEasting - vehicleUTMEasting), 2) ) / SPEED_NO_DATA;
}




double ADstar:: calcrhs(State* s)
{
  double newRhs;

  if(s == getgoal()) //If this is the goal state, rhs = 0
    {
      newRhs = 0;
    }

  else  //This algorithm returns 0 if s == goalState, or min( c(s, t) + g(t)) where t is an element of Succ(s)
    {
      int length;
      State** neighbors = getSucc(s, &length);  //get the successors to the current state.
      double result;
      newRhs = (neighbors[0] -> getg()) + c(s, neighbors[0]);  //Initialize the fist result; this is OK because each cell is guaranteed to have at least 3 neighbors.

      for(int i = 1; i < length; i++)  //Find min( c(s, t) + g(t)) where t is an element of Succ(s) 
	{
	  result = (neighbors[i] -> getg()) + c(s, neighbors[i]);
	  if(result < newRhs)
	    {
	      newRhs = result;
	    }
	}

      delete [] neighbors;  //Don't forget to clean up after ourselves.
    }
  
  return newRhs;
}

double ADstar:: c(State* s, State* t)
{
  double distance = sqrt( pow( (s->getNorthing() - t->getNorthing() ), 2) + pow( ( s->getEasting() - t->getEasting()), 2) );  //Calculate the distance between the adjacent cells
  return distance / m_map.getDataUTM<double>(speedLayer, t->getNorthing(), t->getEasting() ); //Return the distance between the cells divided by the speed limit in the cell into which we are moving (i.e., the time to travel from the current cell into the new cell.
}

double ADstar:: h(State* s, State* t)
{
  double distance = sqrt( pow( (s->getNorthing() - t->getNorthing() ), 2) + pow( ( s->getEasting() - t->getEasting()), 2) ); //Calculate the distance between the cells.
  
  #warning: "This should be changed to account for the max allowable speed in the corridor."
  return distance / SPEED_NO_DATA;  //Return the distance between the cells divided by the max speed allowable in the corridor.
}


//Yay for complicated control switching!
State** ADstar::getPred(const State* s, int *length)
{
  State** neighbors;
  int row, col;
  m_map.UTM2Win(s->getNorthing(), s->getEasting(), &row, &col);

  if((0 < row) && (row < mapRows - 1) && (0 < col) && (col < mapCols - 1)) //We're not on any of the edges of the map.
    {
      neighbors = new State*[8];
      neighbors[0] = m_map.getDataWin<State*>(StateLayer, row - 1, col - 1);  //Get the cell above and to the left
      neighbors[1] = m_map.getDataWin<State*>(StateLayer, row - 1, col);  //Get the cell above
      neighbors[2] = m_map.getDataWin<State*>(StateLayer, row - 1, col + 1);  //Get the cell above and to the right
      neighbors[3] = m_map.getDataWin<State*>(StateLayer, row , col - 1);  //Get the cell to the left
      neighbors[4] = m_map.getDataWin<State*>(StateLayer, row,  col + 1);  //Get the cell to the right
      neighbors[5] = m_map.getDataWin<State*>(StateLayer, row + 1, col - 1);  //Get the cell below and to the left
      neighbors[6] = m_map.getDataWin<State*>(StateLayer, row + 1, col);  //Get the cell below
      neighbors[7] = m_map.getDataWin<State*>(StateLayer, row + 1, col + 1);  //Get the cell below and to the right.
    }

  else if(row == 0) //We're on the top edge
    {
      if( (0 < col) && (col < mapCols - 1) ) //We're not in a corner
	{
	  neighbors = new State*[5];
	  neighbors[0] = m_map.getDataWin<State*>(StateLayer, row, col - 1);  //Get the cell to the left
	  neighbors[1] = m_map.getDataWin<State*>(StateLayer, row, col + 1);  //Get the cell to the right
	  neighbors[2] = m_map.getDataWin<State*>(StateLayer, row + 1, col - 1);  //Get the cell below and to the left
	  neighbors[3] = m_map.getDataWin<State*>(StateLayer, row + 1, col);  //Get the cell below
	  neighbors[4] = m_map.getDataWin<State*>(StateLayer, row + 1, col + 1);  //Get the cell below and to the right.
	}

      else if(0 == col) //We're in the top left-hand corner
	{
	  neighbors = new State*[3];
	  neighbors[0] = m_map.getDataWin<State*>(StateLayer, row , col + 1);  //Get the cell to the right
	  neighbors[1] = m_map.getDataWin<State*>(StateLayer, row + 1, col);  //Get the cell below
	  neighbors[2] = m_map.getDataWin<State*>(StateLayer, row + 1, col + 1);  //Get the cell below  and to the right
	}

      else //We're in the top right-hand corner
	{
	  neighbors = new State*[3];
	  neighbors[0] = m_map.getDataWin<State*>(StateLayer, row , col - 1);  //Get the cell to the left
	  neighbors[1] = m_map.getDataWin<State*>(StateLayer, row + 1, col);  //Get the cell below
	  neighbors[2] = m_map.getDataWin<State*>(StateLayer, row + 1, col - 1);  //Get the cell below and to the left
	}
    }

  else if(row == mapRows - 1) //We're on the bottom edge
    {
      if( (0 < col) && (col < mapCols - 1) ) //We're not in a corner
	{
	  neighbors = new State*[5];
	  neighbors[0] = m_map.getDataWin<State*>(StateLayer, row, col - 1);  //Get the cell to the left
	  neighbors[1] = m_map.getDataWin<State*>(StateLayer, row, col + 1);  //Get the cell to the right
	  neighbors[2] = m_map.getDataWin<State*>(StateLayer, row + 1, col - 1);  //Get the cell below and to the left
	  neighbors[3] = m_map.getDataWin<State*>(StateLayer, row + 1, col);  //Get the cell below
	  neighbors[4] = m_map.getDataWin<State*>(StateLayer, row + 1, col + 1);  //Get the cell below and to the right.
	}

      else if(0 == col) //We're in the bottom left-hand corner
	{
	  neighbors = new State*[3];
	  neighbors[0] = m_map.getDataWin<State*>(StateLayer, row , col + 1);  //Get the cell to the right
	  neighbors[1] = m_map.getDataWin<State*>(StateLayer, row - 1, col);  //Get the cell above
	  neighbors[2] = m_map.getDataWin<State*>(StateLayer, row - 1, col + 1);  //Get the cell above and to the right
	}

      else //We're in the bottom right-hand corner
	{
	  neighbors = new State*[3];
	  neighbors[0] = m_map.getDataWin<State*>(StateLayer, row , col - 1);  //Get the cell to the left
	  neighbors[1] = m_map.getDataWin<State*>(StateLayer, row - 1, col);  //Get the cell above
	  neighbors[2] = m_map.getDataWin<State*>(StateLayer, row - 1, col - 1);  //Get the cell above and to the left
	}
    }

  else if(col == 0)  //We're on the left edge, but not in a corner
    {  
      neighbors = new State*[5];
      neighbors[0] = m_map.getDataWin<State*>(StateLayer, row - 1, col);  //Get the cell above
      neighbors[1] = m_map.getDataWin<State*>(StateLayer, row - 1, col + 1);  //Get the cell above and to the right
      neighbors[2] = m_map.getDataWin<State*>(StateLayer, row , col + 1);  //Get the cell to the right
      neighbors[3] = m_map.getDataWin<State*>(StateLayer, row + 1, col);  //Get the cell below
      neighbors[4] = m_map.getDataWin<State*>(StateLayer, row + 1, col + 1);  //Get the cell below and to the right.
    }

  else //We're on the right edge, but not in a corner
    {
      neighbors = new State*[5];
      neighbors[0] = m_map.getDataWin<State*>(StateLayer, row - 1, col );  //Get the cell above
      neighbors[1] = m_map.getDataWin<State*>(StateLayer, row - 1, col - 1);  //Get the cell above and to the left
      neighbors[2] = m_map.getDataWin<State*>(StateLayer, row , col - 1);  //Get the cell to the left
      neighbors[3] = m_map.getDataWin<State*>(StateLayer, row + 1, col);  //Get the cell below
      neighbors[4] = m_map.getDataWin<State*>(StateLayer, row + 1, col - 1);  //Get the cell below and to the left.
    }

  return neighbors;
}




//Yay for complicated control switching!
State** ADstar::getSucc(const State* s, int *length)
{
  State** neighbors;
  int row, col;
  m_map.UTM2Win(s->getNorthing(), s->getEasting(), &row, &col);

  if((0 < row) && (row < mapRows - 1) && (0 < col) && (col < mapCols - 1)) //We're not on any of the edges of the map.
    {
      neighbors = new State*[8];
      neighbors[0] = m_map.getDataWin<State*>(StateLayer, row - 1, col - 1);  //Get the cell above and to the left
      neighbors[1] = m_map.getDataWin<State*>(StateLayer, row - 1, col);  //Get the cell above
      neighbors[2] = m_map.getDataWin<State*>(StateLayer, row - 1, col + 1);  //Get the cell above and to the right
      neighbors[3] = m_map.getDataWin<State*>(StateLayer, row , col - 1);  //Get the cell to the left
      neighbors[4] = m_map.getDataWin<State*>(StateLayer, row,  col + 1);  //Get the cell to the right
      neighbors[5] = m_map.getDataWin<State*>(StateLayer, row + 1, col - 1);  //Get the cell below and to the left
      neighbors[6] = m_map.getDataWin<State*>(StateLayer, row + 1, col);  //Get the cell below
      neighbors[7] = m_map.getDataWin<State*>(StateLayer, row + 1, col + 1);  //Get the cell below and to the right.
    }

  else if(row == 0) //We're on the top edge
    {
      if( (0 < col) && (col < mapCols - 1) ) //We're not in a corner
	{
	  neighbors = new State*[5];
	  neighbors[0] = m_map.getDataWin<State*>(StateLayer, row, col - 1);  //Get the cell to the left
	  neighbors[1] = m_map.getDataWin<State*>(StateLayer, row, col + 1);  //Get the cell to the right
	  neighbors[2] = m_map.getDataWin<State*>(StateLayer, row + 1, col - 1);  //Get the cell below and to the left
	  neighbors[3] = m_map.getDataWin<State*>(StateLayer, row + 1, col);  //Get the cell below
	  neighbors[4] = m_map.getDataWin<State*>(StateLayer, row + 1, col + 1);  //Get the cell below and to the right.
	}

      else if(0 == col) //We're in the top left-hand corner
	{
	  neighbors = new State*[3];
	  neighbors[0] = m_map.getDataWin<State*>(StateLayer, row , col + 1);  //Get the cell to the right
	  neighbors[1] = m_map.getDataWin<State*>(StateLayer, row + 1, col);  //Get the cell below
	  neighbors[2] = m_map.getDataWin<State*>(StateLayer, row + 1, col + 1);  //Get the cell below  and to the right
	}

      else //We're in the top right-hand corner
	{
	  neighbors = new State*[3];
	  neighbors[0] = m_map.getDataWin<State*>(StateLayer, row , col - 1);  //Get the cell to the left
	  neighbors[1] = m_map.getDataWin<State*>(StateLayer, row + 1, col);  //Get the cell below
	  neighbors[2] = m_map.getDataWin<State*>(StateLayer, row + 1, col - 1);  //Get the cell below and to the left
	}
    }

  else if(row == mapRows - 1) //We're on the bottom edge
    {
      if( (0 < col) && (col < mapCols - 1) ) //We're not in a corner
	{
	  neighbors = new State*[5];
	  neighbors[0] = m_map.getDataWin<State*>(StateLayer, row, col - 1);  //Get the cell to the left
	  neighbors[1] = m_map.getDataWin<State*>(StateLayer, row, col + 1);  //Get the cell to the right
	  neighbors[2] = m_map.getDataWin<State*>(StateLayer, row + 1, col - 1);  //Get the cell below and to the left
	  neighbors[3] = m_map.getDataWin<State*>(StateLayer, row + 1, col);  //Get the cell below
	  neighbors[4] = m_map.getDataWin<State*>(StateLayer, row + 1, col + 1);  //Get the cell below and to the right.
	}

      else if(0 == col) //We're in the bottom left-hand corner
	{
	  neighbors = new State*[3];
	  neighbors[0] = m_map.getDataWin<State*>(StateLayer, row , col + 1);  //Get the cell to the right
	  neighbors[1] = m_map.getDataWin<State*>(StateLayer, row - 1, col);  //Get the cell above
	  neighbors[2] = m_map.getDataWin<State*>(StateLayer, row - 1, col + 1);  //Get the cell above and to the right
	}

      else //We're in the bottom right-hand corner
	{
	  neighbors = new State*[3];
	  neighbors[0] = m_map.getDataWin<State*>(StateLayer, row , col - 1);  //Get the cell to the left
	  neighbors[1] = m_map.getDataWin<State*>(StateLayer, row - 1, col);  //Get the cell above
	  neighbors[2] = m_map.getDataWin<State*>(StateLayer, row - 1, col - 1);  //Get the cell above and to the left
	}
    }

  else if(col == 0)  //We're on the left edge, but not in a corner
    {  
      neighbors = new State*[5];
      neighbors[0] = m_map.getDataWin<State*>(StateLayer, row - 1, col);  //Get the cell above
      neighbors[1] = m_map.getDataWin<State*>(StateLayer, row - 1, col + 1);  //Get the cell above and to the right
      neighbors[2] = m_map.getDataWin<State*>(StateLayer, row , col + 1);  //Get the cell to the right
      neighbors[3] = m_map.getDataWin<State*>(StateLayer, row + 1, col);  //Get the cell below
      neighbors[4] = m_map.getDataWin<State*>(StateLayer, row + 1, col + 1);  //Get the cell below and to the right.
    }

  else //We're on the right edge, but not in a corner
    {
      neighbors = new State*[5];
      neighbors[0] = m_map.getDataWin<State*>(StateLayer, row - 1, col );  //Get the cell above
      neighbors[1] = m_map.getDataWin<State*>(StateLayer, row - 1, col - 1);  //Get the cell above and to the left
      neighbors[2] = m_map.getDataWin<State*>(StateLayer, row , col - 1);  //Get the cell to the left
      neighbors[3] = m_map.getDataWin<State*>(StateLayer, row + 1, col);  //Get the cell below
      neighbors[4] = m_map.getDataWin<State*>(StateLayer, row + 1, col - 1);  //Get the cell below and to the left.
    }

  return neighbors;
}


ADstar:: ~ADstar()
{
  delete [] mapDelta;
}
