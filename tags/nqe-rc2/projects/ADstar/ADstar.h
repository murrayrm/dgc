//Header file for new ADstar seederModule

//NOTE:  All functions and data field names below are defined in accordance with the paper "Anytime Dynamic A*: An Anytime, Replanning Algorithm", by Likhachev, Ferguson, Gordon, Stentz, and Thrun.  Also note that, unless SPECIFICALLY NOTED AS SUCH, all coordinates in this algorithm will be UTM coordinates.

#include <queue>
#include "MapdeltaTalker.h"
#include "rddf.hh"
#include "StateClient.h"
#include "CMapPlus.hh"
#include "State.h"
#include "StateQueue.h"

using namespace std;

class ADstar: public CMapdeltaTalker, public CStateClient
{

  //Private helper methods

  private:

  //Data fields for the algorithm


  //State variables.
  
  double vehicleUTMNorthing, vehicleUTMEasting;  //UTM northing and easting coordinates of the vehicle.

  double goalUTMNorthing, goalUTMEasting;   //UTM northing and easting coordinates of the goal state.






  //CMap and related variables

  CMapPlus m_map; //The map of the current area around the vehicle and related variables.

  int mapRows, mapCols;  //The number of rows/cols in the CMap

  double cellSizeN, cellSizeE; //Northing/Easting dimensions of a cell

  double topLeftUTMNorthing, topLeftUTMEasting;  //Northing/Easting coordinates of one of the map cells.

  int speedLayer;  //Reference for the speed limit layer.

  int StateLayer; //Reference for the State* layer.

  int mapRequestSocket;  //Reference w/ fusionMapper to request full map data.

  int mapDeltaSocket;  //Reference w/ fusionMapper to listen for map deltas.

  char* mapDelta;

  bool receivedAtLeastOneDelta;

  bool haveMap;  //Variable that keeps track of whether or not we already have a valid costMap.

#define MAX_DELTA_SIZE 1000000
#define MIN_DIST_TO_EDGE_OF_MAP 50  //Distance in meters between current vehicle location and the edge of the map at which a new map and plan is generated.

  RDDF rddf;

#define PLAN_AHEAD_DIST 300 //Distance that the ADstar algorithm will plan ahead.
#define REPLAN_DIST  50 //Distance at which a new plan is generated.

  const static double SPEED_NO_DATA = 15; //Assume that we can go 1 m/s (about 3 miles/hr) if we don't know anything about a given piece of terrain yet.





  //Skynet socket for sending trajs

  int trajSocket;

  //These priority queues are implemented such that the States stored in them
  //will be extracted in increasing priority, as stated in the AD* paper


  double EPSILON_INIT;
  double EPSILON_DECREMENT;

  StateQueue OPEN;
  StateQueue CLOSED;
  StateQueue INCONS;

  //AD* primary algorithms (as defined in the AD* paper)


  //Sets the key for s according to the key algorithm in the paper.
  void key(State* s);

  void UpdateState(State* s);

  void ComputeOrImprovePath();

  void plan();







  //Helper methods

  //Generates a new map centered on the vehicle's current location.  Also updates the objective (the goal state).
  void resetMap();

  //Gets new map information.  If the vehicle's location is too close to the edge of the map (as defined by MIN_DIST_TO_EDGE_OF_MAP) then this will call resetMap() to create a new map centered on the vehicle.  Returns an array of the pointers to State objects of  all map cells that were updated
  State** getMapDeltas(int *size);

  //Deletes all state objects in the StateLayer map layer.
  void deleteAllStateObjects();
  
  void setgoal();

  State* getgoal();

  void setCurrentState();

  State* getCurrentState();

  void setState(int r, int c, State* s);

  State* getState(int r, int c);

  double calcg(State* s); //Function for estimating the cost from state s to the goal.

  double calcrhs(State* s);  //Lookahead cost algorithm

  double c(State* s, State* t);  //Calculates the exact edge cost between adjacent States s and t associated with moving FROM s INTO t.

  double h(State* s, State* t); //Heuristic to calculate approximate cost between State s and t

  // returns a pointer to an array of State*'s ( the neighbors of s), sets length to the length of the State* array.
  State** getPred(const State* s, int *length);

  State** getSucc(const State* s, int *length);



  //Public methods
 public:


  //Initializes vehicle location, creates a CostMap, and fills it in.
  ADstar(int sn_key, bool bWaitForStateFill, double e, double decrement);

  //CTraj plan();

  ~ADstar();
};
