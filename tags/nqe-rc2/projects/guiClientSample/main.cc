#include "sample.h"

int main(int argc, char *argv[])
{
  int skynet_key;
  char* p_skynet_key = getenv("SKYNET_KEY");
  if ( p_skynet_key == NULL )
    {
      cout << "You need to set a skynet key!" << endl;
      return 1;
    }
  else
    {
      skynet_key = atoi(p_skynet_key);
      cout << "Using skynet key: " << skynet_key << endl;
    }
  
  CSampleClient sampleClient(skynet_key);
  
  sampleClient.activeLoop();
}
