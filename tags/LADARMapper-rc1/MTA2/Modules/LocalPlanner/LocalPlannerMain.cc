#include "Modules.hh"
#include "LocalPlanner.hh"
#include "Kernel.hh"

#include <boost/shared_ptr.hpp>

using namespace boost;

Kernel K;
LocalPlanner DGCM(MODULES::LocalPlanner, "Test Module", 0);

int main() {

  K.Register();
  //  K.Register(shared_ptr<DGC_MODULE>(new LocalPlanner(MODULES::LocalPlanner, "Test Module", 0)));

  K.Start();

  return 0;
}
