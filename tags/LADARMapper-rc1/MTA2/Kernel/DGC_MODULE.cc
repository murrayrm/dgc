#include "DGC_MODULE.hh"
#include "Kernel.hh"
#include <iostream>

extern Kernel K;
extern DGC_MODULE DGCM;

using namespace boost;
using namespace std;

void DGC_MODULE_STARTER() {

  DGCM();
}

DGC_MODULE::DGC_MODULE(int modtype, string modname, int modflags ) {

  M_ModuleType             = modtype;
  M_ModuleName             = modname;
  M_ModuleFlags            = modflags;

  M_CurrentState           = -1;
  M_NextState              = STATES::INIT; 

  M_MyThreads              = 0; // now user defined threads started.
  M_StateMachineLock       = shared_ptr<recursive_mutex> (new recursive_mutex);

};

DGC_MODULE::~DGC_MODULE() {

}

void DGC_MODULE::operator ()() {
  // Loads State Loop
  cout << "Running State Machine Loop" << endl;
  cout << this << endl;
  StateMachineLoop();
};

/*
void DGC_MODULE::Init() {
  cout << "DGC_MODULE Default Init function got called.  You should define Init()" << endl;
}
void DGC_MODULE::Active() {
  cout << "DGC_MODULE Default Active function got called.  You should define your own." << endl;
}
void DGC_MODULE::Standby() {
  cout << "DGC_MODULE Default Standby function got called.  You should define your own." << endl;
}
void DGC_MODULE::Shutdown() {
  cout << "DGC_MODULE Default Shutdown function got called.  You should define your own." << endl;
}

void DGC_MODULE::Restart() {
  cout << "DGC_MODULE Default Standby function got called.  You should define your own." << endl;
}



string DGC_MODULE::Status() {
  string x = "Status function of " + M_ModuleName +
    " has not been defined.  State = ";

  switch(M_CurrentState) {
  case STATES::INIT: 
    x += "Init";           break;
  case STATES::ACTIVE:
    x += "Active";         break;
  case STATES::STANDBY:
    x += "Standby";        break;
  case STATES::SHUTDOWN:
    x += "Shutdown";       break;
  case STATES::RESTART:
    x += "Restart";        break;
  default:
    x += "Unknown [Something is very wrong].";
  }

  return x;
}

void DGC_MODULE::InMailHandler(Mail& msg) {
  cout << "In Message Received but function not defined - ignoring message" << endl;
}

Mail DGC_MODULE::QueryMailHandler(Mail& msg) {
  cout << "Query Mail Received but function not defined - returning bad message" << endl;
  return Mail(msg.To(), msg.From(), 0, MailFlags::BadMessage);
}
*/

void DGC_MODULE::SetModuleNumber(int m) {
  M_ModuleNumber = m;
}

int  DGC_MODULE::ModuleNumber() {
  return M_ModuleNumber;
}

MailAddress DGC_MODULE::MyAddress() {
  MailAddress x;
  x.TCP = K.MyTCPAddress();
  x.MAILBOX = M_ModuleNumber;

  return x;
}

int DGC_MODULE::ModuleType() {
  return M_ModuleType;
}
string DGC_MODULE::ModuleName() {
  return M_ModuleName;
}

int DGC_MODULE::SetNextState( int stateID ) {
  boost::recursive_mutex::scoped_lock sl(*M_StateMachineLock);

  if( !((M_NextState    == STATES::SHUTDOWN) ||
	(M_CurrentState == STATES::SHUTDOWN))	) {
    if( stateID < 0 || stateID > STATES::NUM_STATES ) {
      return -1;  // invalid state number (who's the idiot?)
    } else {
      M_NextState = stateID;
    }

  } else {
    // return shutdown state, since a shutdown has been issued previously
    return STATES::SHUTDOWN;
  }
} 

int DGC_MODULE::GetCurrentState() {
  return M_CurrentState;
}

int DGC_MODULE::ContinueInState() {
  if( K.ShuttingDown() || M_NextState >=0) {
    return false;
  } else { 
    return true;
  }
}

void DGC_MODULE::StateMachineLoop() {
  while( M_NextState >= 0 && M_NextState < STATES::NUM_STATES) {
    M_CurrentState = M_NextState;
    M_NextState    = -1; // clear the next state before we execute
    ExecuteCurrentState();

    if( M_CurrentState == STATES::SHUTDOWN) {
      // if the shutdown state finished...
      M_NextState = -1;  // kill the program
    } else if(M_NextState == -1) {
      // Next State was never set...might as well 
      // shutdown for lack of something better to do
      SetNextState(STATES::SHUTDOWN);
    }
  }
}

void DGC_MODULE::ExecuteCurrentState() {
  switch( M_CurrentState ) {
  case STATES::INIT:
    Init(); break;
  case STATES::ACTIVE:
    Active(); break;
  case STATES::STANDBY:
    Standby(); break;
  case STATES::SHUTDOWN:
    Shutdown(); break;
  case STATES::RESTART:
    Restart(); break;
  default:
    cout << M_ModuleName << ": WTF? - State gone wacko" << endl;
  }
}


Mail DGC_MODULE::NewQueryMessage(int modtype, int messagetype) {

  return Mail( MyAddress(), K.FindAddress(modtype), messagetype, MailFlags::QueryMessage);
}

Mail DGC_MODULE::NewOutMessage(int modtype, int messagetype) {

  MailAddress m = K.FindAddress(modtype);
  //  cout << "Find Address of " << modtype
  //       << " = " << m.TCP.IP
  //       << ":" << m.TCP.PORT
  //       << ":" << m.MAILBOX;
  return Mail( MyAddress(), K.FindAddress(modtype), messagetype, 0);
}
