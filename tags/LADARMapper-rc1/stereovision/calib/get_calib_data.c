/**************************************************************************
**       Title: get_calib_data.c
**    
**    Author: S. Waydo <waydo@cds.caltech.edu>
**
**    Get some images for calibration
**
**************************************************************************/

#include <stdio.h>
#include <libraw1394/raw1394.h>
#include <libdc1394/dc1394_control.h>
#include <stdlib.h>

#include <unistd.h>

// OpenCV stuff
#include <cv.h>
#include <highgui.h>
#include <cvaux.h>

#define MAX_CAMERAS  2
#define LEFT_CAM     0
#define RIGHT_CAM    1
#define UNKNOWN_CAM -1

#define NUM_CALIB_IMAGES 5

/*-----------------------------------------------------------------------
 *  set up cameras
 *-----------------------------------------------------------------------*/
int init_cameras( raw1394handle_t * handle_ptr,
                  int * numCameras_ptr,
                  dc1394_camerainfo info[],
                  quadlet_t cam_ids[],
                  int node2cam[],
                  dc1394_cameracapture cameras[] );

/*-----------------------------------------------------------------------
 *  have the cameras start sending us data
 *-----------------------------------------------------------------------*/
int start_cameras( raw1394handle_t handle,
                   int numCameras,
                   dc1394_cameracapture cameras[] );

/*-----------------------------------------------------------------------
 *  Rectify and resize images
 *-----------------------------------------------------------------------*/
int process_images( int numCameras, 
	            dc1394_cameracapture cameras[],
	            int node2cam[],
	            IplImage* stereoImages[] );

/*-----------------------------------------------------------------------
 *  Stop data transmission
 *-----------------------------------------------------------------------*/
int stop_cameras( raw1394handle_t handle,
		  int numCameras,
		  dc1394_cameracapture cameras[] );

/*-----------------------------------------------------------------------
 *  Shut down cameras
 *-----------------------------------------------------------------------*/
int close_cameras( raw1394handle_t handle,
		   int numCameras,
		   dc1394_cameracapture cameras[] );

int main(int argc, char *argv[]) 
{
  dc1394_cameracapture cameras[MAX_CAMERAS]; // array of cameras so we can do multi-grab
  dc1394_camerainfo info[MAX_CAMERAS];
  quadlet_t cam_ids[MAX_CAMERAS];
  int numCameras;
  raw1394handle_t handle;
  int i, j;
  int dummy;


  // lookup of node to camera number
  // defined LEFT_CAM to be 0, RIGHT_CAM to be 1 for now
  int node2cam[MAX_CAMERAS];

  // openCV stereo vision stuff
  IplImage* stereoImages[MAX_CAMERAS];

  char filename[32];

  // initialize cameras
  if ( init_cameras( &handle, &numCameras, info, cam_ids, node2cam, cameras ) < 0 ) {
    fprintf( stderr, "Error initializing cameras\n" );
    exit(1);
  }

  // start data transmission
  if ( start_cameras( handle, numCameras, cameras ) < 0 ) {
    fprintf( stderr, "Error starting cameras\n" );
    exit(1);
  }

  int k;
  for ( k = 1; k <= NUM_CALIB_IMAGES; k++ ) {

    printf( "pausing...\n" );
    sleep(3);
    printf( "k = %d\n", k );

    /*-----------------------------------------------------------------------
     *  capture one frame from each camera
     *-----------------------------------------------------------------------*/
    // do a multi capture - this way it should be synchronized automagically
    if ( dc1394_multi_capture(handle,cameras,numCameras)!=DC1394_SUCCESS)
      {
        fprintf( stderr, "Unable to capture frames\n" );
        close_cameras( handle, numCameras, cameras );
        exit(1);
      }

    process_images( numCameras, cameras, node2cam, stereoImages );
  
    /*-----------------------------------------------------------------------
     *  save images under appropriate filenames
     *-----------------------------------------------------------------------*/
    for ( i = 0; i < numCameras; i++ ) {

      switch(node2cam[i])
        {
        case LEFT_CAM :
          sprintf( filename, "left_%d.jpg", k );
	  break;
        case RIGHT_CAM :
          sprintf( filename, "right_%d.jpg", k );
	  break;
        default : 
          sprintf( filename, "unknown_%d.jpg", k );
	  break;
        }
      cvSaveImage( filename, stereoImages[i] );
    }

    // clean up
    for ( i = 0; i < MAX_CAMERAS; i++ ) cvReleaseImage( &stereoImages[i] );

  }


  // stop data transmission 
  stop_cameras( handle, numCameras, cameras );

  // clean up
  for ( i = 0; i < MAX_CAMERAS; i++ ) cvReleaseImage( &stereoImages[i] );
  close_cameras( handle, numCameras, cameras );
  return 0;
}


/*-----------------------------------------------------------------------
 *  HELPER FUNCTIONS
 *-----------------------------------------------------------------------*/

/*-----------------------------------------------------------------------
 *  set up cameras
 *-----------------------------------------------------------------------*/
int init_cameras( raw1394handle_t * handle_ptr,
                  int * numCameras_ptr,
                  dc1394_camerainfo info[],
                  quadlet_t cam_ids[],
                  int node2cam[],
                  dc1394_cameracapture cameras[] ) {

  int numNodes;
  int i, j;
  char idstr[9];

  nodeid_t * camera_nodes;

  /*-----------------------------------------------------------------------
   *  Open ohci and asign handle to it
   *-----------------------------------------------------------------------*/
  *handle_ptr = dc1394_create_handle(0);
  if (*handle_ptr==NULL)
  {
    fprintf( stderr, "Unable to aquire a raw1394 handle\n\n"
             "Please check \n"
	     "  - if the kernel modules `ieee1394',`raw1394' and `ohci1394' are loaded \n"
	     "  - if you have read/write access to /dev/raw1394\n\n");
    return(-1);
    }

  /*-----------------------------------------------------------------------
   *  get the camera nodes and describe them as we find them
   *-----------------------------------------------------------------------*/
  numNodes = raw1394_get_nodecount(*handle_ptr);
  camera_nodes = dc1394_get_camera_nodes(*handle_ptr, numCameras_ptr, 0);
  for ( i = 0; i < *numCameras_ptr; i++ ) {
    dc1394_get_camera_info(*handle_ptr, camera_nodes[i], &info[i]);
    cam_ids[i] = info[i].euid_64 & 0xffffffff;
    sprintf( idstr, "0x%08X",cam_ids[i] );
   if ( strcmp( idstr, "0x002F51E9" ) == 0 ) {
      node2cam[i] = LEFT_CAM;
      printf( "Left!\n" );
    } else if ( strcmp( idstr, "0x002F51EA" ) == 0 ) {
      node2cam[i] = RIGHT_CAM;
      printf( "Right!\n" );
    } else {
      node2cam[i] = UNKNOWN_CAM;
      printf( "Unknown camera found!\n" );
    }
  }


  fflush(stdout);
  if (*numCameras_ptr<1)
  {
    fprintf( stderr, "no cameras found :(\n");
    dc1394_destroy_handle(*handle_ptr);
    return(-1);
  }

 /*-----------------------------------------------------------------------
   *  to prevent the iso-transfer bug from raw1394 system, check if
   *  camera is highest node. For details see 
   *  http://linux1394.sourceforge.net/faq.html#DCbusmgmt
   *  and
   *  http://sourceforge.net/tracker/index.php?func=detail&aid=435107&group_id=8157&atid=108157
   *-----------------------------------------------------------------------*/
  // TODO: Figure out what this bug means and if we need to be worried about it!!!
  //  if( camera_nodes[0] == numNodes-1 )
  /* if( camera_nodes[numCameras-1] == numNodes-1 )
  {
        fprintf( stderr, "\n"
             "Sorry, your camera is the highest numbered node\n"
             "of the bus, and has therefore become the root node.\n"
             "The root node is responsible for maintaining \n"
             "the timing of isochronous transactions on the IEEE \n"
             "1394 bus.  However, if the root node is not cycle master \n"
             "capable (it doesn't have to be), then isochronous \n"
             "transactions will not work.  The host controller card is \n"
             "cycle master capable, however, most cameras are not.\n"
             "\n"
             "The quick solution is to add the parameter \n"
             "attempt_root=1 when loading the OHCI driver as a \n"
             "module.  So please do (as root):\n"
             "\n"
             "   rmmod ohci1394\n"
             "   insmod ohci1394 attempt_root=1\n"
             "\n"
             "for more information see the FAQ at \n"
             "http://linux1394.sourceforge.net/faq.html#DCbusmgmt\n"
             "\n"); 
    dc1394_destroy_handle(handle);
    return(-1);
  }*/

  /*-----------------------------------------------------------------------
   *  setup capture
   *-----------------------------------------------------------------------*/
  for ( i = 0; i < *numCameras_ptr; i++ ) {
    if ( dc1394_setup_capture(*handle_ptr, camera_nodes[i],
                              i, // channel
                              FORMAT_SVGA_NONCOMPRESSED_1,
                              MODE_1024x768_MONO,
                              SPEED_400,
                              FRAMERATE_15,
                              &cameras[i])!=DC1394_SUCCESS) {
    fprintf( stderr,"unable to setup camera-\n"
             "check line %d of %s to make sure\n"
             "that the video mode,framerate and format are\n"
             "supported by your camera\n",
             __LINE__,__FILE__);
    close_cameras( *handle_ptr, *numCameras_ptr, cameras );
    return(-1);
    }
  }

  // set trigger mode - I'm not exactly sure what this does or if it is
  // strictly necessary
  for ( i = 0; i < *numCameras_ptr; i++ ) {
    if( dc1394_set_trigger_mode(*handle_ptr, cameras[i].node, TRIGGER_MODE_0)
        != DC1394_SUCCESS)
    {
      fprintf( stderr, "unable to set camera %d trigger mode\n", i);
    }
  }

  return 0;

}

/*-----------------------------------------------------------------------
 *  have the cameras start sending us data
 *-----------------------------------------------------------------------*/
int start_cameras( raw1394handle_t handle,
                   int numCameras,
                   dc1394_cameracapture cameras[] ) {

  int i, j;

  for ( i = 0; i < numCameras; i++ ) {
    if ( dc1394_start_iso_transmission(handle,cameras[i].node)
	 !=DC1394_SUCCESS)
      {
	fprintf( stderr, "Unable to start camera %d iso transmission\n", i);
        close_cameras( handle, numCameras, cameras );
	return(-1);
      }
  }

  return 0;
}

/*-----------------------------------------------------------------------
 *  Resize images and put into OpenCV format
 *-----------------------------------------------------------------------*/
int process_images( int numCameras, 
	            dc1394_cameracapture cameras[],
	            int node2cam[],
	            IplImage* stereoImages[] ) {

  IplImage* tempImage;
  CvSize rawSize, smallSize;

  int i;

  for ( i = 0; i < numCameras; i++ ) {
    // find size of image and put it in the expected format
    rawSize = cvSize( cameras[i].frame_width, cameras[i].frame_height );
    // create a temporary spot to put the raw data
    tempImage = cvCreateImage( rawSize, IPL_DEPTH_8U, 1 );
    cvSetData( tempImage, (char *)cameras[i].capture_buffer, 
               cameras[i].frame_width );
    smallSize = cvSize( rawSize.width, rawSize.height );
    stereoImages[node2cam[i]] = cvCreateImage( smallSize, IPL_DEPTH_8U, 1 );
    cvResize( tempImage, stereoImages[node2cam[i]], CV_INTER_LINEAR );
  }

  return 0;

}



/*-----------------------------------------------------------------------
 *  Stop data transmission
 *-----------------------------------------------------------------------*/
int stop_cameras( raw1394handle_t handle,
		  int numCameras,
		  dc1394_cameracapture cameras[] ) {

  int i;

  for ( i = 0; i < numCameras; i++ ) {
    if (dc1394_stop_iso_transmission(handle,cameras[i].node)!=DC1394_SUCCESS)
      {
	fprintf( stderr, "Couldn't stop camera%d?\n", i);
      }
  }

  return 0;

}

/*-----------------------------------------------------------------------
 *  Shut down cameras
 *-----------------------------------------------------------------------*/
int close_cameras( raw1394handle_t handle,
		   int numCameras,
		   dc1394_cameracapture cameras[] ) {

  int i;

  for ( i = 0; i < numCameras; i++ ) dc1394_release_camera(handle,&cameras[i]);
  dc1394_destroy_handle(handle);

  return 0;

}
