/*
 *
 * display.h - Display structures and definitions 
 *
 * Richard M. Murray
 * November 4, 1987
 *
 */

#ifndef __DISPLAY_INCLUDED__
#define __DISPLAY_INCLUDED__

/* Display actions */
typedef enum {
    Update, Refresh, Input, Save, Load
} DD_ACTION;

/* Display type */
enum display_type {
  Data=0, Label, Button, KeyBinding, String
};

/* Display entry structure */
typedef struct display_entry {
    int row, col;
    void *value;
    int (*function)(DD_ACTION, int);
    char *format;
    char *current;
    int selectable;		/* Allow entry to be selected */
    int (*callback)(long);	/* fcn called after selection */
    long userarg;		/* user argument passed to callback */

    /* Optional parameters */
    int foreground;
    int background;
    enum display_type type;
    char varname[32];
    int length;			/* maximum allowed length of field */

    /* Parameters not normally initialized */
    int lastlen;		/* length of last value displayed */
    int up, down, left, right;
    unsigned initialized: 1;
    unsigned reverse: 1;

    /* Offsets to adjacent (selectable) entries */
    /* Initialization done in dd_usetbl() */

} DD_IDENT;

#define DD_Label(r, c, s)  	    {r, c, s, dd_label, "NULL", NULL, 0, NULL, (long)NULL, "", -1}
#define DD_Button(r, c, s, fcn)     {r, c, s, dd_label, NULL, NULL, 1, fcn, (long)NULL, "", -1}
#define DD_End			    {0, 0, NULL, NULL, NULL, NULL, 0}
#define DD_EXIT_LOOP		    (dd_exit_loop((long) 0))
#define DD_BEEP()		    (dd_beep((long) 0))

/* Macros for calling redefineable functions */
#define DD_CLS	(*dd_cls_fcn)	     /* clear screen function macro */
#define DD_PROMPT (*dd_prompt_fcn)  	/* prompt function macro */
#define DD_SCANF (*dd_scanf_fcn) 	/* scan function macro */

/* Global variables */
extern DD_IDENT *ddtbl;	        /* current display description table */
extern DD_IDENT *ddprv;	        /* previous display description table */
extern int dd_rows;		/* number of available rows on screen */
extern int dd_cols;		/* number of available columns */
extern int dd_modef;	        /* text/graphics flag */
extern int dd_cur;		/* current entry */
extern int dd_keycode;		/* last keypress code */
extern int dd_debug;		/* turn debugging info on */
extern char dd_save_string[];	/* string for saving and loading variables */

/* Declare variables which are used to hold function pointers */
extern void (*dd_cls_fcn)(long);
extern void (*dd_prompt_fcn)(char *);
extern int (*dd_scanf_fcn)(char *, char *, void *); 

/* Display functions */
extern int dd_short(DD_ACTION, int), dd_byte(DD_ACTION, int);
extern int dd_float(DD_ACTION, int), dd_label(DD_ACTION, int);
extern int dd_double(DD_ACTION, int), dd_ubyte(DD_ACTION, int);
extern int dd_long(DD_ACTION, int), dd_string(DD_ACTION, int);
extern int dd_nilmgr(DD_ACTION, int), dd_nilcbk(long);

/* Screen attributes */
#define Normal  0x0
#define Reverse 0x1

/* Colors (ANSI) */
#define BLACK	     0
#define RED	     1
#define GREEN	     2
#define YELLOW	     3
#define BLUE	     4
#define MAGENTA	     5
#define CYAN	     6
#define LIGHTGREY    7

#define DD_DEFFG     LIGHTGREY
#define DD_DEFBG     BLACK

/* Library functions */
extern int dd_open(void);
extern int dd_usetbl(DD_IDENT *tbl);
extern int dd_usetbl_cb(long);
extern int dd_prvtbl();
extern int dd_prvtbl_cb(long);
extern DD_IDENT *dd_gettbl(void);
extern int dd_update(void);
extern int dd_redraw(long);
extern void dd_refresh(int offset);
extern int dd_bindkey(int key, int (*fcn)(long));
extern int dd_setcolor(int offset, int bg, int fg);
extern int dd_setlabel(int offset, char *s);
extern int dd_loop(void);
extern void *dd_loop_thread(void *);
extern int dd_save(char *filename);
extern int dd_tbl_save(char *filename, DD_IDENT *tbl);
extern int dd_load(char *filename);
extern int dd_tbl_load(char *filename, DD_IDENT *tbl);
extern int dd_select(int);
extern int dd_beep(long);

extern int dd_find_up(int entry, int *up);
extern int dd_find_right(int entry, int *right);
extern int dd_find_down(int entry, int *down);
extern int dd_find_left(int entry, int *left);

/* Terminal interface */
extern void dd_prompt(char *);
extern int dd_getc(void);
extern void dd_text_prompt(char *s);
extern void dd_text_cls(long);
extern int dd_text_scanf(char *prompt, char *fmt, void *ptr);
extern void dd_close(void);
#define DD_PUTS dd_puts
extern void dd_puts(DD_IDENT *dd, char *s);
extern char *dd_cgets(char *buffer);
extern int dd_read(char *prompt, char *address, int length);

/* Display actions (usually bound to keys) */
extern int dd_exit_loop(long);
extern int dd_unbound(long);
extern int dd_exec_callback(long);
extern int dd_input(long);

/* Item selection (can also be bound to keys) */
extern int dd_next(long);
extern int dd_prev(long);
extern int dd_left(long);
extern int dd_right(long);
extern int dd_up(long);
extern int dd_down(long);

/* ddprintf */
#include <stdarg.h>
int dd_sprintf(char *, char *, int, ...);
int dd_vsprintf(char *, char *, int, va_list); 

/* chngettok */
#include <stdio.h>
int chn_gettok(FILE * fp, char *string, int length, char *delimiters, int *line);


#endif /* __DISPLAY_INCLUDED__ */
