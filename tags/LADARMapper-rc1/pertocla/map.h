#ifndef __MAP_H__
#define __MAP_H__

#define MIN_STEREO_RANGE        2.0	/* meters */

// Size of sensor map
#define MAP_WIDTH		20.0            /* meters, was 20.0 */
#define MAP_HEIGHT		20.0            /* meters, was 20.0 */
#define MAP_X_RES		0.2             /* meters */
#define MAP_Y_RES		0.2             /* meters */
#define MAP_Z_RES		0.02            /* meters */
#define LOW_HI_MAP_RATIO	5
#define MAP_X_RES_LOW		(MAP_X_RES*LOW_HI_MAP_RATIO)	/* meters */
#define MAP_Y_RES_LOW		(MAP_Y_RES*LOW_HI_MAP_RATIO)	/* meters */
#define MAP_ROWS		((int)(MAP_HEIGHT/MAP_Y_RES))
#define MAP_COLS		((int)(MAP_WIDTH/MAP_X_RES))
#define MAP_ROWS_LOW		((int)(MAP_HEIGHT/MAP_Y_RES_LOW))
#define MAP_COLS_LOW		((int)(MAP_WIDTH/MAP_X_RES_LOW))

// Size of GDRS world map
#define GDRSMAP_GRID_SIZE       (0.2)
#define GDRSMAP_NUM_CELLS       (251)
#define GDRSMAP_MAX_ROWS        500

// Size of jplmap diagnostic map
#define LARGE_DIAGNOSTIC_SENSOR_MAP	1

//Colors used to display map images in remdisp
#define BLACK                   0
#define GREEN                   1
#define GRAY                    2
#define YELLOW                  3
#define BLUE                    4
#define RED                     5
#define WHITE                   6
#define ORANGE                  7

#define MIN_GRAY                10      // grayscale fcolor 0-127
#define MAX_GRAY                117

// Vehicle dimensions for remdisp display
#define VEHICLE_WIDTH           1.0     // meters
#define VEHICLE_LENGTH          1.5     // meters

// For grayscale display of elevation in remdisp
#define MIN_HEIGHT              (-5.0)  // meters
#define MAX_HEIGHT              5.0     // meters

#endif
