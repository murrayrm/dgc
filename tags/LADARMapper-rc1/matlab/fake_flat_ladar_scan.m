% this little script produces a log file that is identical to those
% produced by the actual ladar hardware.
% 
% Important parameters:
% LADAR_HEIGHT = z-offset of sensor
% LADAR_PITCH = pitch angle of sensor
% numscans = how many scans to fake
% angle_res = angular resolution between scans, i.e. .5 degrees
% angle_range = angular range, i.e. 100 or 180 deg
% units = units of sensor, in m.

numscans = 58;
filename = 'simflat_scans_02202004_143445.log';
LADAR_HEIGHT = 2; %offset height in meters
LADAR_PITCH = deg2rad(10.4);
angle_res = .5;
angle_range = 100; %degrees
units = .01; %lms220 returns data in cm (usually)

lo = LADAR_HEIGHT/tan(LADAR_PITCH);

start_angle = (180 - angle_range) / 2;
range=[];
for i = start_angle:angle_res:start_angle+angle_range
  tmp = lo / sin(deg2rad(i));
  range = [range tmp];
end
range = range/units;
disp(sprintf('Saving %d scans to file %s',numscans,filename));
fid=fopen(filename,'w');
% stuff that appears at the top of the logfile
fprintf(fid, '%% angle = %d\n', angle_range);
fprintf(fid, '%% resolution = %f\n', angle_res);
fprintf(fid, '%% units = %f\n', units);
fprintf(fid, '%% Time[sec] | scan points\n');

for i = 1:numscans
  % the timestamp
  fprintf(fid, '0.000000 ');
  % print the ranges, to the correct number of digits
  fprintf(fid,'%4.0f ',range);
  fprintf(fid,'\n');
end
fclose(fid);