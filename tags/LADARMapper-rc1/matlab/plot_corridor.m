function original = plot_corridor( varargin );
% 
% function plot_corridor( waypoint_file )
%
% Changes: 
%   02/02/04, Lars Cremean, created  
%
% Function:
%   This function reads a Bob format waypoint specification and displays a 
%   plot of the associated corridor.
%
% Usage example:
%   plot_corridor('waypoints.bob')
%   The waypoints file must be in Bob format!
%

hold on;

% set the defaults for the optional arguments 

if( nargin < 1 )
	error('Need an argument (waypoint_file)');
end
if( nargin >= 1 )
	
	waypoint_file = varargin{1};
        
	% Handle waypoints file.
	% Can't use 'load' command with Bob format.
	[type,eastings,northings,radii,vees] = ...
	  textread( waypoint_file, '%s%f%f%f%f', 'commentstyle', 'shell');

	% waypoints array has columns {easting, northing}
	waypoints(:,1) = eastings;
	waypoints(:,2) = northings;

        % return the original waypoints
	original = [eastings northings];
end
if( nargin >= 2 )
	offset = varargin{2};
        e_offset = offset(1);
	n_offset = offset(2);
else
	e_offset = eastings(1);
	n_offset = waypoints(1,2);
end
if( nargin >= 3 )
	error('Too many arguments!');
end

   
% format of waypoint_file
% easting northing GRRR
% make all the measurements relative to first waypoint
waypoints(:,1) = waypoints(:,1) - e_offset;
waypoints(:,2) = waypoints(:,2) - n_offset;

% draw circles around waypoints
theta = [0:360]/180*pi;

h1(1) = text(waypoints(1,1), waypoints(1,2), num2str(0));

for i=1:size(waypoints,1)-1;

    % draw a circle around the current waypoint
    circle_e = radii(i)*cos(theta);
    circle_n = radii(i)*sin(theta);
    h2(i) = plot(circle_e + waypoints(i,1), circle_n + waypoints(i,2),'r');

    % draw the same circle around the next waypoint
    plot(circle_e + waypoints(i+1,1), circle_n + waypoints(i+1,2),'r');
    h1(i+1) = text(waypoints(i+1,1), waypoints(i+1,2), num2str(i));

    % draw the lateral offset lines 
    % get the angle to the next waypoint
    point = atan2( waypoints(i+1,2) - waypoints(i,2), ...
                   waypoints(i+1,1) - waypoints(i,1) );

    % draw the left lateral offset line
    fir = waypoints(i,:) + radii(i) * [cos(point+pi/2) sin(point+pi/2)];
    nex = waypoints(i+1,:)+radii(i) * [cos(point+pi/2) sin(point+pi/2)];
    h3(i) = plot( [fir(1) nex(1)], [fir(2) nex(2)], 'r' );
    % draw the right lateral offset line
    fir = waypoints(i,:) + radii(i) * [cos(point-pi/2) sin(point-pi/2)];
    nex = waypoints(i+1,:)+radii(i) * [cos(point-pi/2) sin(point-pi/2)];
    h4(i) = plot( [fir(1) nex(1)], [fir(2) nex(2)], 'r' );
    
end

axis tight
axis equal
hold on
grid on
