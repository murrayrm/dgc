function [original, origin] = fplot_corridor(waypoint_file, xref, yref);
% 
% function fplot_corridor( waypoint_file, xref, yref )
%
% Changes: 
%   02/02/04, Lars Cremean, created  
%   03/05/04, H Huang modified to generate fender points
% Function:
%   This function reads a Bob format waypoint specification and displays a 
%   plot of the associated corridor.
%
% Usage example:
%   plot_corridor('waypoints.bob')
%   The waypoints file must be in Bob format!
%
% This will plot the waypoints and the corridor and a reference mark to be
% used by get_fenders.  Zoom to where you can see the reference clearly,
% and then export as a bitmap.   Then draw on the bitmap the fender points
% you want and save the file as a 16-color bitmap.  The bigger the x and y
% references the better the accuracy of the fender points, since we get a
% better baseline to get a pixels-per-meter count.  Be sure that
% fplot_corridor and get_fenders use the same x and y refs.   The origin
% that fplot_corridor returns (second return variable) is then fed into
% get_fenders to correctly set the origin of the fender points.

hold on;

% size of reference line for image
% xref = 1;
% yref = 1;

% Handle waypoints file.

% 12/30/2003 (lars): waypointfile is no longer a friendly file to MATLAB's
% 'load' command
[type,eastings,northings,radii,vees] = ...
    textread( waypoint_file, '%s%f%f%f%f', 'commentstyle', 'shell');

original = [eastings northings];
    
% waypoints array has columns {easting, northing}
waypoints(:,1) = eastings;
waypoints(:,2) = northings;

% define first waypoint to be the origin
e_offset = waypoints(1,1);
n_offset = waypoints(1,2);

origin = [waypoints(1,1) waypoints(1,2)];

% format of waypoint_file
% easting northing GRRR
% make all the measurements relative to first waypoint
waypoints(:,1) = waypoints(:,1) - e_offset;
waypoints(:,2) = waypoints(:,2) - n_offset;

% draw circles around waypoints
theta = [0:360]/180*pi;

%h1(1) = text(waypoints(1,1), waypoints(1,2), num2str(0));

for i=1:size(waypoints,1)-1;

    % draw a circle around the current waypoint
    circle_e = radii(i)*cos(theta);
    circle_n = radii(i)*sin(theta);
    h2(i) = plot(circle_e + waypoints(i,1), circle_n + waypoints(i,2),'r');

    % draw the same circle around the next waypoint
    plot(circle_e + waypoints(i+1,1), circle_n + waypoints(i+1,2),'r');
    %h1(i+1) = text(waypoints(i+1,1), waypoints(i+1,2), num2str(i));

    % draw the lateral offset lines 
    % get the angle to the next waypoint
    point = atan2( waypoints(i+1,2) - waypoints(i,2), ...
                   waypoints(i+1,1) - waypoints(i,1) );

    % draw the left lateral offset line
    fir = waypoints(i,:) + radii(i) * [cos(point+pi/2) sin(point+pi/2)];
    nex = waypoints(i+1,:)+radii(i) * [cos(point+pi/2) sin(point+pi/2)];
    h3(i) = plot( [fir(1) nex(1)], [fir(2) nex(2)], 'r' );
    % draw the right lateral offset line
    fir = waypoints(i,:) + radii(i) * [cos(point-pi/2) sin(point-pi/2)];
    nex = waypoints(i+1,:)+radii(i) * [cos(point-pi/2) sin(point-pi/2)];
    h4(i) = plot( [fir(1) nex(1)], [fir(2) nex(2)], 'r' );
    
end

%axis tight
axis equal
hold on
grid off

% draw one meter lines north and south at origin
plot([0 xref], [0 0], 'b');
plot([0 0], [0 yref], 'b');
axis equal
