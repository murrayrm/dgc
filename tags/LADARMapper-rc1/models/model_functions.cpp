/***************************************************************************************/
//FILE: 	functions.cpp
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file has some functions of global use. the prototypes are at
//		functions.h as well as information about them
//
/***************************************************************************************/

#include "model_functions.h"

using namespace std;

void runge4(int order, double x, double *y, double step, void f(double,double*,double*,double*),double *input){
  int i;
  double h=step/2.0;
  double k1[MAX_ORDER], k2[MAX_ORDER], k3[MAX_ORDER], k4[MAX_ORDER];
  double Yaux[MAX_ORDER];

  // First step
  f(x,y,k1,input);
  for(i = 0;i < order; i++){
    k1[i]  = step*k1[i];
  }

  // Second step
  for(i = 0;i < order; i++){
    Yaux[i] = y[i] + k1[i]/2;
  }
  f(x+h,Yaux,k2,input);
  for(i = 0;i < order; i++){
    k2[i]  = step*k2[i];
  }

  // Third step
  for(i = 0;i < order; i++){
    Yaux[i] = y[i] + k2[i]/2;
  }
  f(x+h,Yaux,k3,input);
  for(i = 0;i < order; i++){
    k3[i]  = step*k3[i];
  }

  // Fourth step
  for(i = 0;i < order; i++){
    Yaux[i] = y[i] + k3[i];
  }
  f(x+step,Yaux,k4,input);
  for(i = 0;i < order; i++){
    k4[i]  = step*k4[i];
  }

  // y for the next step of time
  for(i = 0;i < order; i++)
    y[i] = y[i]+(k1[i]+2*k2[i]+2*k3[i]+k4[i])/6.0;
  //if(order > 2)
  //  cout << "RK y=" << y[2] << "; k1=" << k1[2] << "; k2=" << k2[2] << "; k3=" << k3[2] << "; k4=" << k4[2] << endl;
}

void  fv(double x, double *y, double *k, double *force){
  k[0] = (double)((double)1/MASS)*(-DRAG*y[0] + force[0]);
}

void  fsteering(double x, double *y, double *k, double *input)
{
  k[0] = input[1]*cos(y[2] + input[0]);
  k[1] = input[1]*sin(y[2] + input[0]);
  k[2] = (double)((double)input[1]/VEHICLE_AXLE_DISTANCE)*sin(input[0]);
}

// input is {steering angle, vehicle speed}
void  fsteering2(double x, double *y, double *k, double *input)
{
  k[0] = input[1]*cos(y[2]);
  k[1] = input[1]*sin(y[2]);
  // Why do we cast input[1] to a double?
  k[2] = (double)((double)input[1]/VEHICLE_AXLE_DISTANCE)*tan(input[0]);
}

void  f_lag(double x, double *y, double *k, double *param){
  k[0] = (param[0])*(-y[0] + param[1]);
}

double throttleActuator(double u){
  double k1 = 600;
  double b1 = 0;
  double k2 = 5350;
  double b2 = -285;
  double y,differential;


  differential = 10; //between 1 and 2 gear position
  if(u < 0){
    // It's a brake actuation
    y = 0;
  }
  else{  
    if(u < 0.06){
      y = k1*u + b1;
    }
    else
    {
      y = k2*u + b2;
    }
  }
  return(y*differential);
}

double brakeActuator(double u, double vel){
  // Brake parameters 
  double k1 = 0.096;
  double b1 = 0;
  double k2 = 1.41;
  double b2 = -0.657;
  double fmax = 400;

  double y;
  double in;
  
  in = -u;
  
  if(in < 0){
    // If its a throttle actuation
    y = 0;
  }
  else{
    if(in < 0.5){
      y = k1*in + b1;
    }
    else
    {
      y = k2*in + b2;
    }
  }
  
  // The direction depends on the velocity
  if(vel > 0){
    y = -y*fmax;
  }
  if(vel < 0){
    y = y*fmax;
  }
  if(vel = 0){
    y = 0;
  }
  return(y);  
}

// This functions converts the torque into force
double tire(double accTorque, double brakeTorque){
  double force;
  
  force = (accTorque + brakeTorque)*TIRE_RADIUS;
  return(force);
}
