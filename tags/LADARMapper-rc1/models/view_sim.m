function runge(filename,i)

rKutta = load(filename);

tR = rKutta(:,1);
xR = rKutta(:,2);
yR = rKutta(:,3);
vR = rKutta(:,4);
thetaR = rKutta(:,5);

figure(i);
subplot(3,2,1:2), plot(tR,vR);
title('Speed');
ylabel('V');

subplot(3,2,3), plot(tR,thetaR);
ylabel('\theta');

subplot(3,2,4), plot(xR,yR);
ylabel('Y');

subplot(3,2,5), plot(tR,xR);
ylabel('X');
xlabel('t');

subplot(3,2,6), plot(tR,yR);
ylabel('y');
xlabel('t');
