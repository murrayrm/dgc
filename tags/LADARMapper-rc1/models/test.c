/* A test program for the Simulator class's methods
 * Author: Thyago Consort
 * Revision History:  Created  Thyago C.  1/17/2004
 */

#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <iostream.h>
#include <fstream.h>
#include "simulator.hh"

#define DELTA_T 0.01

int main (int argc,char* argv[]){

  /*************************************************************************/
  /********** DECLARING VARIABLES ******************************************/
  /*************************************************************************/
  // Variales used to get parameters from command line
  int opt, outOfMemory;
  char *argMethod;
  int method,param;
  
  system("clear");

  /*************************************************************************
   ********** GETTING THE PARAMETERS FROM COMMAND LINE			
   **********  CAUTION:						  
   **********    - optind,opterr:  extern variables, do not declare a
   **********	         local variable with any of these names	     
   **********	 - optind: indice of the argv where to find the argument 
   **********		  desired					    
   *************************************************************************/

  // Defining the characters used as delimiter of a command line argument  
  param = 0;
  outOfMemory = 0;
  while ((opt = getopt (argc, argv, "m:")) != -1){
    switch ((unsigned char)opt){
    case 'm':
      param = 1;
      if(optarg != NULL){
	argMethod = (char*) malloc(strlen(optarg)+1);
	if(argMethod == NULL)
	  outOfMemory = 1;
	else
	  strcpy(argMethod,optarg);
      }
      break;
    case '?':
    default:
      system("clear");
      cout << "usage: [-m method]" << endl << endl
           << "1: simData struct and constructor" << endl
           << "2: Simulator class getState/setState methods" << endl
           << "3: Simulate going north" << endl
           << "4: Simulate going east" << endl
           << "5: Simulate with positive steering wheel" << endl
           << "6: Simulate with negative steering wheel" << endl << endl;
      return(-1);
    }    
  }
  
  // If any allocation failed
  if(outOfMemory){
    if(argMethod != NULL) free(argMethod);
    system("clear");
    cout << "Problems with memory management" << endl;
    return(-1);
  }
  
  // Showing the choosen test
  if(param)
    method = atoi(argMethod);
  else
    method = 1;

  simData example;
  simData newData;
  Simulator engine(example,DELTA_T);
  double t; // time
  double accel,steer;

  // Open the file for appending
  ofstream out_file;

  switch(method){
  case 1:

    // Testing constructor
    cout << "Testing the constructor" << endl << endl;
    cout << "[x=" << example.x << "][y=" << example.y << "][vel=" << example.vel << "][n_vel=" << example.n_vel << "][e_vel=" << example.e_vel << "][u_vel=" << example.u_vel << "][yaw=" << example.yaw << "]" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // Testing the copy constructor
    cout << "Testing the copy constructor (1,2,3,4,5,6,7,8,9,0 expected)" << endl << endl;
    newData.x = 2;
    newData.y = 3;
    newData.vel = 4;
    newData.n_vel = 5;
    newData.e_vel = 6;
    newData.u_vel = 7;
    newData.yaw = 8;
    example = newData;

    newData.x = 1;
    newData.y = 1;
    newData.vel = 1;
    newData.n_vel = 1;
    newData.e_vel = 1;
    newData.u_vel = 1;
    newData.yaw = 1;

    cout << "[x=" << example.x << "][y=" << example.y << "][vel=" << example.vel << "][n_vel=" << example.n_vel << "][e_vel=" << example.e_vel << "][u_vel=" << example.u_vel << "][yaw=" << example.yaw << "]" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    break;
  case 2:
    // Testing constructor
    cout << "Testing the setState and getState methods" << endl << endl;
    newData.x = 2;
    newData.y = 3;
    newData.vel = 8888;
    newData.yaw = 9999;

    example = engine.getState();
    cout << "[x=" << example.x << "][y=" << example.y << "][vel=" << example.vel << "][n_vel=" << example.n_vel << "][e_vel=" << example.e_vel << "][u_vel=" << example.u_vel << "][yaw=" << example.yaw << "]" << endl;

    engine.setState(newData);
    example = engine.getState();
    cout << "(1,2,3,0,0,0,0,0,9,1) expected" << endl;
    cout << "[x=" << example.x << "][y=" << example.y << "][vel=" << example.vel << "][n_vel=" << example.n_vel << "][e_vel=" << example.e_vel << "][u_vel=" << example.u_vel << "][yaw=" << example.yaw << "]" << endl;

    newData.n_vel = 3;
    newData.e_vel = 4;
    newData.u_vel = 0;
    engine.setState(newData);
    example = engine.getState();
    cout << "(1,2,3,5,3,4,0,0.927,9,1) expected" << endl;
    cout << "[x=" << example.x << "][y=" << example.y << "][vel=" << example.vel << "][n_vel=" << example.n_vel << "][e_vel=" << example.e_vel << "][u_vel=" << example.u_vel << "][yaw=" << example.yaw << "]" << endl;

    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    break;

  case 3:

    t = 0;
    accel = 0.1;
    steer = 0;
    out_file.open("north.dat", ios::out | ios::binary);
    while(1){

      // Giving commands to the simulator
      engine.setSim(t,accel,steer);

      // Simulating
      engine.simulate();

      // Getting new state
      newData = engine.getState();
      
      // t - x - y - v - theta - phi
      out_file << t << "\t" << newData.x << "\t" << newData.y << "\t" << newData.vel << "\t" << newData.yaw << "\t" << newData.phi << endl;

      cout << "Bob (t=" << t << ") (phi=" << newData.phi << ") (vel=" << newData.vel << ")" << "(x=" << newData.x << ")" << "(y=" << newData.y << ")" << "(yaw=" << newData.yaw << ") (accel=" << accel << ") (steer=" << steer << ")" << endl;

      // Incrementing time
      t += DELTA_T;
      //usleep(5000);
      //getchar();
    }
    break;

  case 4:

    t = 0;
    accel = 0.1;
    steer = 0;
    out_file.open("east.dat", ios::out | ios::binary);
    while(1){

      // Giving commands to the simulator
      engine.setSim(t,accel,steer);
      newData.yaw = M_PI/2;
      engine.setState(newData);

      // Simulating
      engine.simulate();

      // Getting new state
      newData = engine.getState();
      
      // t - x - y - v - theta - phi
      out_file << t << "\t" << newData.x << "\t" << newData.y << "\t" << newData.vel << "\t" << newData.yaw << "\t" << newData.phi << endl;

      cout << "Bob (t=" << t << ") (phi=" << newData.phi << ") (vel=" << newData.vel << ")" << "(x=" << newData.x << ")" << "(y=" << newData.y << ")" << "(yaw=" << newData.yaw << ") (accel=" << accel << ") (steer=" << steer << ")" << endl;

      // Incrementing time
      t += DELTA_T;
      //usleep(5000);
      //getchar();
    }
    break;

  case 5:

    t = 0;
    accel = 0.1;
    steer = 1;
    cout << sin(25*M_PI/180) << endl;
    out_file.open("right.dat", ios::out | ios::binary);
    while(1){

      // Giving commands to the simulator
      engine.setSim(t,accel,steer);

      // Simulating
      engine.simulate();

      // Getting new state
      newData = engine.getState();
      
      // t - x - y - v - theta - phi
      out_file << t << "\t" << newData.x << "\t" << newData.y << "\t" << newData.vel << "\t" << newData.yaw << "\t" << newData.phi << endl;

      cout << "Bob (t=" << t << ") (phi=" << newData.phi << ") (vel=" << newData.vel << ")" << "(x=" << newData.x << ")" << "(y=" << newData.y << ")" << "(yaw=" << newData.yaw << ") (accel=" << accel << ") (steer=" << steer << ")" << endl;

      // Incrementing time
      t += DELTA_T;
      usleep(5000);
      //getchar();
    }
    break;

  case 6:

    t = 0;
    accel = 0.1;
    steer = -0.2;
    out_file.open("left.dat", ios::out | ios::binary);
    while(1){

      // Giving commands to the simulator
      engine.setSim(t,accel,steer);

      // Simulating
      engine.simulate();

      // Getting new state
      newData = engine.getState();
      
      // t - x - y - v - theta - phi
      out_file << t << "\t" << newData.x << "\t" << newData.y << "\t" << newData.vel << "\t" << newData.yaw << "\t" << newData.phi << endl;

      cout << "Bob (t=" << t << ") (phi=" << newData.phi << ") (vel=" << newData.vel << ")" << "(x=" << newData.x << ")" << "(y=" << newData.y << ")" << "(yaw=" << newData.yaw << ") (accel=" << accel << ") (steer=" << steer << ")" << endl;

      // Incrementing time
      t += DELTA_T;
      //usleep(5000);
      //getchar();
    }
    break;

  default:
    cout << "Wrong choice !!" << endl;
  }
  
  return(0);
}
