//ArbiterModule.cc

#include "ArbiterModule.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include <iostream>
using namespace std;

// Variables for command-line arguments/flags
extern int LOG_DATA;    // whether or not the -log option was detected
extern char * TESTNAME; // the string identifier for the log file
extern int noGlobal;    /* whether or not we want the lack of Global to stop
                           Arbiter non-zero commands. */
extern int noState;     /* Whether we care or not that we have state data.
                         * The arbiter doesn't really care about state (as far
                         * as code goes at least), but other modules will be
                         * unreliable w/o up-to-date & correct state data.
	       		 */
extern int noObstacle;  /* whether or not we want the lack of obstacle 
			   detection (Stereo/LADAR) to stop Arbiter from 
			   sending non-zero commands. */
extern int reverseOn;   /* whether we want to go into REVERSE mode */
FILE * logfile;         // logfile is for state and command logs

// These files are for vote logging
vector<FILE *> voteLogFileVector;


Arbiter::Arbiter()
  : DGC_MODULE(MODULES::Arbiter, "Arbiter", 0) {}

Arbiter::~Arbiter() {}

void Arbiter::Init() 
{
  // call the parent init first
  DGC_MODULE::Init();


  //------------------Logging Initialization--------------------------//
  char filename[255]; // output log file name 

  // set up logging file if option was set
  if( LOG_DATA )
  {
    // create a log of the state and arbiter commands
    // filename will be TESTNAME_ddmmyyyy_hhmmss.log
    time_t t = time(NULL);
    tm *local;
    local = localtime(&t);

    sprintf(filename, "testRuns/%s_arbiter_%02d%02d%04d_%02d%02d%02d.log",
            TESTNAME, local->tm_mon+1, local->tm_mday, local->tm_year+1900,
            local->tm_hour, local->tm_min, local->tm_sec);
    printf("Logging data to file: %s\n", filename);
    logfile = fopen(filename,"w");
    if ( logfile == NULL )
    {
      printf("Unable to open log file!!!\n");
      exit(-1);
    }
    // print data format information in a header at the top of the file
    fprintf(logfile, "%% Time[sec] | Easting[m] | Northing[m] | Altitude[m]");
    fprintf(logfile, " | Vel_E[m/s] | Vel_N[m/s] | Vel_U[m/s]" );
    fprintf(logfile, " | Speed[m/s] | Accel[m/s/s]" );
    fprintf(logfile, " | Pitch[rad] | Roll[rad] | Yaw[rad]" );
    fprintf(logfile, " | PitchRate[rad/s] | RollRate[rad/s] | YawRate[rad/s]");
    fprintf(logfile, " | PhiDes[rad] | VelDes[m/s]");
    fprintf(logfile, "\n");

    // reserve the space for our 
    voteLogFileVector.reserve(ArbiterInput::Count);
    
    // additionally, create a log file for each voter 
    // LM = LADAR [LADARMapper],                SV = Stereo [StereoPlanner], 
    // GP = Global [GlobalPlanner],             DF = DFE [DFEPlanner],
    // CE = CorrArcEval [CorridorArcEvaluator], RF = RoadFollower [RoadFollower]
    // PE = PathEvaluation [PathEvaluation]

    for( int i = 0; i<ArbiterInput::Count; i++) 
    {
      // create the name of the file and open it for writing
      sprintf(filename, "testRuns/%s_votes%02d_%02d%02d%04d_%02d%02d%02d.log",
		      TESTNAME, i, local->tm_mon+1, local->tm_mday, 
		      local->tm_year+1900, local->tm_hour, local->tm_min, 
		      local->tm_sec);
      printf("Logging data to file: %s\n", filename);
      voteLogFileVector.push_back( fopen(filename,"w") );
      if ( voteLogFileVector[i] == NULL ) 
      {
	printf("Unable to open vote log file %02d!!!\n", i);
	exit(-1);
      }
      // print data format information in a header at the top of the file
      fprintf(voteLogFileVector[i], "%% Vote log file %02d\n", i );
      fprintf(voteLogFileVector[i], 
	      "%% Votes are goodnesses and then velocities for each\n");
      fprintf(voteLogFileVector[i], "%% arc from full left to full right.\n");
      fprintf(voteLogFileVector[i], "%% NUMARCS = %d\n", NUMARCS );
    }
  } // end if(LOG_DATA)


  //------------------Arbiter Datum Initialization--------------------------//
  // initialize the voter weight that we will use to that defined in
  // ArbiterModuleDatum
  for( int i = 0; i < ArbiterInput::Count; i++)
  {
    d.allVoters[i].VoterWeight = InputWeights[i];
  }
  d.ArbiterMode = ArbiterModes::NORMAL;
  d.currCommandIndex = 0;
  d.lastCommand[d.currCommandIndex].steer_cmd = 0;
  d.lastCommand[d.currCommandIndex].velocity_cmd = 0;

  cout << ModuleName() << "Init Finished" << endl;

  // start Sparrow Display
  RunMethodInNewThread<Arbiter>( this, &Arbiter::SparrowDisplayLoop);
  RunMethodInNewThread<Arbiter>( this, &Arbiter::UpdateSparrowVariablesLoop);
}

void Arbiter::Active()
{
  int i, j;

  // timestamp start of demo, CURRENTLY NOT USED ANYWHERE
  d.TimeZero = TVNow();

  // reset the update counters
  for( i= 0; i< ArbiterInput::Count; i++) { d.UpdateCount[i] = 0; }
  d.UpdateCountState = 0;
  d.CombinedUpdateCount = 0;


  //------------------------------------------------------------------------//
  //----------------------The main command loop-----------------------------//
  // EVENTUALLY code inside the loop below should be a method/methods called 
  // in Update message handler, with no sleep (for synchronization with Voters)
  // or for faster processing with LADAR (if optimized). If not, well...
  while( ContinueInState() ) {

    // for logging of state and command data 01/31/04
    UpdateState();

    // For testing purposes mainly, we have arguments that can enable
    // running without certain modules.
    // Without these flags, we run according to ARBITER_LOGIC.txt.
    //////////////////////////////////////////////////////////////////
    // TODO: GET RID OF THEM FOR QID?????????????????????????????????????
    if ( (noState == 0) &&  d.UpdateCountState < 1) {
      // ForceShutdown();     // Will be usable with MTA-2.2
      cout << "No state detected. Will only send zero votes." << endl;
      usleep(SLEEP_WAIT_FOR_MISSING_MODULES);
    }
    if ( (noGlobal == 0) && d.UpdateCount[ArbiterInput::Global] < 1 )
	 //	 && d.UpdateCount[ArbiterInput::CorrArcEval] < 1) {
    {
      // ForceShutdown();     // Will be usable with MTA-2.2
      cout << "No Global detected. Will only send zero votes." << endl;
      usleep(SLEEP_WAIT_FOR_MISSING_MODULES);
    }
    if ( (noObstacle == 0) && 
	 d.UpdateCount[ArbiterInput::Stereo] < 1 && 
	 d.UpdateCount[ArbiterInput::StereoLR] < 1 && 
         d.UpdateCount[ArbiterInput::LADAR] < 1) {
      // ForceShutdown();     // Will be usable with MTA-2.2
      cout << "No obstacle avoidance detected." 
	   << "Will try to inch forward slowly." << endl;
      // No sleeping, since we want obstacle avoidance ASAP.
    }


    //----------------------Updating Votes from Voters-----------------------//
    Timeval now = TVNow();
    list<Voter> arbInput;
    { // lock the vote sources
      boost::recursive_mutex::scoped_lock s(d.voterLock);

      // Update Voter attributes for this cycle
      for( i = 0; i<ArbiterInput::Count; i++) {
	
        if( (now - d.lastUpdate[i]) > InputTimeout) {
	  // It has been too long since this input has been updated, : ignore
          // Reinitialize this Voter's votes and weight, mostly for display
          // purposes.
          d.allVoters[i].VoterWeight = 0.0;
	  for( int phiInd=0; phiInd<NUMARCS; phiInd++)
	  {
            d.allVoters[i].Votes[phiInd].Goodness = 0.0;
	    d.allVoters[i].Votes[phiInd].Velo = 0.0;
	  }
	}
	else // Input not timed-out
	{
	  if (//(!d.SS.imu_enabled && d.allVoters[i].ID == ArbiterInput::LADAR)
	       // IMU is out and this Voter's LADAR, so we don't want it.
	       // || 
	      d.allVoters[i].Votes[NUMARCS/2].Goodness == LADAR_RESET)
	       // This Voter's LADAR and it's resetting, so it's pretty much
 	       // non-existent.
	  {
	    d.allVoters[i].VoterWeight = 0.0;
	    // But we don't change arc goodness/velocity values so we can
	    // see the resetting in display.
	  }
	  else {
	    // A valid Voter.
	    // Set the weight properly and add it to the list of valid inputs
	    d.allVoters[i].VoterWeight = InputWeights[i];
	    d.allVoters[i].ID = i;
	    arbInput.push_back( d.allVoters[i] );
	  }
	}
      } // end of for(arbiter inputs)
    } // Finish locking Voters.


    //----------------------Calculating command-----------------------------//
    ArbiterBestResult toSend;

    // Only Decide on Arcs if Global input is valid or we don't care,
    //                 AND if State input is valid or we don't care.
    ///////////////////////////////////////////////////////////////////////
    // TODO: REMEMBER TO GET RID OF noGlobal noState option when not testing?
    if( ((now - d.lastUpdate[ArbiterInput::Global]) < InputTimeout || 
	 // (now - d.lastUpdate[ArbiterInput::CorrArcEval]) < InputTimeout || 
	 noGlobal)
        && ((now - d.lastUpdateState) < InputTimeout || noState)
        && !arbInput.empty()) 
    {
      if ( (now - d.lastUpdate[ArbiterInput::Stereo]) < InputTimeout ||
	   (now - d.lastUpdate[ArbiterInput::StereoLR]) < InputTimeout ||
	   (now - d.lastUpdate[ArbiterInput::LADAR]) < InputTimeout || 
	   noObstacle) 
      {
	// Voters in the list are copies of d.allVoters, so we shouldn't
	// need to lock Voters for evaluation.
	// boost::recursive_mutex::scoped_lock s(d.voterLock);
	toSend = PickBest(arbInput, 
	          d.lastCommand[d.currCommandIndex].steer_cmd * MAX_STEER);

	// Update the combined votes field in the datum for display
	d.combined = toSend.combined;
	
	// Checking if we've hit a deadend/moving obstacle/going too fast.
	//////////////////////////////////////////////////////////////////
	////////////////NEED SIMPLEARBITER TO HANDLE NO_CONFIDENCE///////////
	////////////////OR OUT_OF_FIELD_OF_VIEW///////////////////////////
	////////////////OR JUST ASSUME OBSTACLE AVOIDANCE FROM OTHERS/////
	if (toSend.cmd.vel <= 0 && reverseOn && !noObstacle) {
	  switch (d.ArbiterMode)
	  {
	    case ArbiterModes::NORMAL :
	    {

	      // Going forward and see obstacles. Stop and wait.
	      toSend.cmd.vel = 0;
	      toSend.cmd.phi = 0;
	      // update the combined votes field in the datum for display
	      for(int i=0; i<NUMARCS; i++ ) {
		d.combined.Votes[i].Goodness = 0.0;
		d.combined.Votes[i].Velo     = 0.0;
	      }
	      
	      // We now go into the PAUSE mode, waiting to see if obstacles in
	      // front will clear out (moving obstacles).
	      d.ArbiterMode = ArbiterModes::PAUSE;
	      d.pauseCounter = 0;
	    }
	    break;
	    case ArbiterModes::PAUSE :
	    {
	      // We were in PAUSE mode, which means we were waiting to see if
	      // an obstacle in front would move, but we still can't move 
	      // forward, so now go into the REVERSE mode, until we see a 
	      // clear path in front.
	      d.pauseCounter++;
	      if (d.pauseCounter >= MAX_PAUSE_COUNT) {
		d.ArbiterMode = ArbiterModes::REVERSE;
		// Set REVERSE begin time.
		d.reverseBeginPeriodTime = TVNow();
	      }
	    }
	    break;
	    case ArbiterModes::REVERSE :
	    {
	      // Still can't move forward. Stay in REVERSE.
	      // TODO: DO WE WANT STEREO/LADAR TO EVALUATE OVER GENMAPS WITH 
	      // OLD DATA, GOING BACKWARD?? OR EVEN JUST FOLLOWING THE PATH
	      // IT TOOK TO GET HERE. -- NOT FOR QID, 2004.
	      // Currently, we're creeping backwards slowly until we get good
	      // votes forward. Steering angle is either straight or the same
	      // as previous.
	      // However, we do a reverse-stop-reverse-stop mechanism to keep
	      // the vehicle from moving backwards too fast, dangerously.
	      if (TVNow() - d.reverseBeginPeriodTime < REVERSE_ONE_PERIOD) 
	      {
	        toSend.cmd.vel = REVERSE_VELO;
// #define USE_STRAIGHT_REVERSE
#ifdef USE_STRAIGHT_REVERSE    // Basic, going back straight
 	        toSend.cmd.phi = 0;
#else  // Command the last steer
	        // Preserve the steering angle
	        // -- Due to steering delay, this may not be accurate, though.
	        toSend.cmd.phi = d.lastCommand[d.currCommandIndex].steer_cmd 
		                 * MAX_STEER;
#endif
	      }
	      else if (TVNow() - d.reverseBeginPeriodTime < REVERSE_STOP_TIME)
	      {
		toSend.cmd.vel = 0;
		// Preserve the steering angle
		toSend.cmd.phi = d.lastCommand[d.currCommandIndex].steer_cmd 
		               * MAX_STEER;
	      }
	      else {
		////////////////////////////TEST//////////////////////////
		cout << "One reverse-stop cycle over. Car should go in reverse again" << endl;
		d.reverseBeginPeriodTime = TVNow();
	      }
	      // update the combined votes field in the datum for display
	      for(int i=0; i<NUMARCS; i++ ) {
		d.combined.Votes[i].Goodness = 0.0;
		d.combined.Votes[i].Velo     = 0.0;
	      }
	    }
	    break;
	    default : ;
	      // It shouldn't ever get here. Do nothing.
	  } // end of switch(mode)
	} // end of REVERSE handling
	else {  // Not a 'deadend'/moving obstacle
	  d.ArbiterMode = ArbiterModes::NORMAL;
	}
      }
      else 
      {
	// If no obstacle avoidance, inch forward until it comes back or 
	// something happens and we stop somehow.	 
	// At this point, we don't have obstacle avoidance, but CAE 
	// may want us to stop, so we take the minimum.
	toSend = PickBest( arbInput,
		 d.lastCommand[d.currCommandIndex].steer_cmd * MAX_STEER);
	toSend.cmd.vel = min(toSend.cmd.vel, NO_OBSTAVOID_VELO);

	// update the combined votes field in the datum for display
	// Note that displayed velocities aren't related to the command.
	d.combined = toSend.combined;
      }
    }
    else // No Global or state.
    {
      // No matter which state we're in, we should stop and wait for a bit.
      // We may actually be done, if Global's not sending Votes.
      // (An immediate stop)
      toSend.cmd.vel = 0;
      toSend.cmd.phi = 0;
      // Update the combined votes field in the datum for display
      for(int i=0; i<NUMARCS; i++ ) {
	d.combined.Votes[i].Goodness = 0.0;
	d.combined.Votes[i].Velo     = 0.0;
      }
      // Wait and see if we get any inputs.
      usleep(SLEEP_WAIT_FOR_MISSING_MODULES);
    }
    
    
    //-------- Sending commands to Vdrive---------------------------------//
    // Only send a command if the command is different from the previous one
    // or it's been a while since we sent the last command.
    if ( TVNow()-d.lastCommand[d.currCommandIndex].Timestamp
           > CommandTimeout ||
         fabs(d.lastCommand[d.currCommandIndex].velocity_cmd
	      - toSend.cmd.vel) >= THRESHOLD_VELO_DIFF ||
         fabs(d.lastCommand[d.currCommandIndex].steer_cmd 
	      * MAX_STEER - toSend.cmd.phi) >= THRESHOLD_STEER_DIFF )
    {
      // update the counter
      d.CombinedUpdateCount++;

      // Store into datum
      // TODO: Change to system time instead of local
      // Currently, we only use the clock on Arbiter's computer. Unless we want
      // to make sure other modules are sending the latest data, or Vdrive is
      // using the universal system clock to check if arbiter's commands are
      // recent, we have no reason to store the universal system time.
      // Is the universal system time even being used anywhere?
      d.toVDrive.Timestamp = TVNow();
      d.toVDrive.velocity_cmd = toSend.cmd.vel;
      // steer_cmd in [-1,1]
      d.toVDrive.steer_cmd = toSend.cmd.phi / MAX_STEER;  

      ////////////DEBUG--BECAUSE THIS SHOULDN'T HAPPEN//////////////////////
      if (fabs(d.toVDrive.steer_cmd) > 1.0) {
	cout << "Steer command out of range: " << d.toVDrive.steer_cmd << endl;
	if (d.toVDrive.steer_cmd < 0) {
	  d.toVDrive.steer_cmd = -1.0;
	}
	else {
	  d.toVDrive.steer_cmd = 1.0;
	}
      }

      // Store the command into history - to be used for arc-picking & REVERSE
      d.currCommandIndex = (d.currCommandIndex + 1) % NUM2REMEMBER;
      d.lastCommand[d.currCommandIndex].Timestamp = d.toVDrive.Timestamp;
      d.lastCommand[d.currCommandIndex].velocity_cmd = d.toVDrive.velocity_cmd;
      d.lastCommand[d.currCommandIndex].steer_cmd = d.toVDrive.steer_cmd;

      // And ship off to VDrive
      Mail msg = 
       NewOutMessage( MyAddress(), MODULES::VDrive, VDriveMessages::CmdMotion);
      msg << d.toVDrive;
      SendMail(msg);

      //////////////////////////////////////////
      // TODO: FOR NOW SEND TO SIMVDRIVE AS WELL
      Mail msg2 = NewOutMessage( MyAddress(), MODULES::SimVDrive,   
				 VDriveMessages::CmdMotion);
      msg2 << d.toVDrive;
      SendMail(msg2);
    }

    // Sleeping because we share CPU with Vdrive, DFEPlanner, GlobalPlanner
    //////////////////////////////////////////////////
    // TODO: Currently hitting 10Hz, may want to sleep less to make quicker
    // decisions.
    usleep(SLEEP_ACTIVE_LOOP);


    //------------------Logging Data---------------------------------------//
    // log data if the switch was set
    if (LOG_DATA) {
      fprintf(logfile, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",
            (double)(d.SS.Timestamp.sec()+d.SS.Timestamp.usec()/1000000.0), 
            d.SS.Easting, d.SS.Northing, d.SS.Altitude, d.SS.Vel_E, d.SS.Vel_N,
            d.SS.Vel_U, d.SS.Speed, d.SS.Accel, d.SS.Pitch, d.SS.Roll,
            d.SS.Yaw, d.SS.PitchRate, d.SS.RollRate, d.SS.YawRate,
            toSend.cmd.phi, toSend.cmd.vel );
      fflush(logfile);

      for( int i = 0; i<ArbiterInput::Count; i++)
      {
	// write the goodness votes to the file
	for( int j = 0; j<NUMARCS; j++ ) {
	  fprintf(voteLogFileVector[i], "%5.1f\t", 
		  d.allVoters[i].Votes[j].Goodness );
	}
	// write the speed votes to the file
	for( int j = 0; j<NUMARCS; j++ ) {
	  fprintf(voteLogFileVector[i], "%5.2f\t", 
		  d.allVoters[i].Votes[j].Velo );
	}
	fprintf(voteLogFileVector[i], "\n");
      }
    }

  } // end of while( ContinueInState() )
  //------------------------------------------------------------------------//

  // stop the car at the end of a demo
  d.toVDrive.Timestamp = TVNow();
  d.toVDrive.velocity_cmd = 0;
  d.toVDrive.steer_cmd = 0;
  Mail msg = 
    NewOutMessage( MyAddress(), MODULES::VDrive, VDriveMessages::CmdMotion);
  msg << d.toVDrive;
  SendMail(msg);
}

void Arbiter::Standby() {

  while( ContinueInState() ) {
    usleep(100000); // just wait until we are ready to go
  }

}

void Arbiter::Shutdown() {

  // close the log file if we opened it
  if( LOG_DATA ) {
    fclose(logfile);
    for( int i = 0; i<ArbiterInput::Count; i++) {
      fclose( voteLogFileVector[i] );
    }
  }

  // if we get to here a break has been detected 
  cout << "----------------Arbiter: Shutting Down----------------" << endl;
}

Mail Arbiter::QueryMailHandler(Mail& msg) {
  Mail reply;			// allocate space for the reply

  // figure out what we are responding to
  switch(msg.MsgType()) {
  case ArbiterMessages::GetVotes:
    reply = ReplyToQuery(msg);
    {
      struct Voter ms;
      for(int i=0; i<ArbiterInput::Count; i++) {
        ms = d.allVoters[i];
        reply << ms;
      }
      reply << d.combined;

      reply << d.toVDrive;
      /*
       printf(" Sending d.toVDrive.velocity_cmd = %f\n", 
                d.toVDrive.velocity_cmd );
       printf(" Sending d.toVDrive.steer_cmd = %f\n", d.toVDrive.steer_cmd );
      */
    }
  break;
  default:
    reply = DGC_MODULE::QueryMailHandler(msg);
  }
  return reply;
}

void Arbiter::InMailHandler(Mail & msg) {

  switch (msg.MsgType()) {
  case ArbiterMessages::Start:
    {
      // download the test number that we are supposed to start
      // and then set the zero time, finally switching to active
      // state allowing the execution of the demo

      msg >> d.TestNumber;
      strstream strStream;
      strStream << d.TestNumber;
      strStream >> d.TestNumberStr;

      d.TimeZero = TVNow();

      SetNextState(STATES::ACTIVE);
    }
    break;

  case ArbiterMessages::Stop:
    // just switch to standby state (shutdown at end of active state)
    SetNextState(STATES::STANDBY);
    break;

  case ArbiterMessages::Update:
    // Update the votes for an input source
    {
      int src;
      Voter vtr;
      msg >> src >> vtr;
      // Check Timestamp for safety
      if( 0 <= src && src < ArbiterInput::Count) {
	// update the datum
	//cout << "Update Message Received From Src = " << src << endl;
	boost::recursive_mutex::scoped_lock s(d.voterLock);
	d.UpdateCount[src]++;

	// This was the original way to handle arbiter vote update messages
	// It overwrites the weight that we are using for our voter (bad.)
	//d.allVoters[src] = vtr;
	// Here's a new way that only overwrites the votes 
	for( int i = 0; i < NUMARCS; i++ ) {
	  d.allVoters[src].Votes[i] = vtr.Votes[i];
	}
        // Can't hurt to set the ID as well
	d.allVoters[src].ID = vtr.ID;

	// Currently, we only use the clock on Arbiter's computer
	d.lastUpdate[src] = TVNow();
      }
    }
    break;
  default:
    // let the parent mail handler handle it
    DGC_MODULE::InMailHandler(msg);
    break;
  }
}
