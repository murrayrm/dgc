For info on how the road-following system works, scroll down

Quick Explanation of the Files in this Folder

Makefile - Pretty self-explanatory

RoadFollower.cc - Contains all the actual road-following code.  More specifically, code to communicate with the road-following computer and pass along votes and maps should be contained in this file, as well as code to initialize and shutdown the road follower

RoadFollower.hh - Header for RoadFollower.cc

RoadFollowerDatum.hh - Should contain any variables that need to be accessed by different threads

RoadFollowerMain.cc - Simply  the main program that runs and starts up the MTA module

get_vstate.cc - Contains a function to get state data

HOW THE ROAD-FOLLOWING SYSTEM IS SETUP
Currently, the system architecture is to have one color road-following camera on the vehicle, connected to a WinXP computer.  This WinXP computer will run Christopher Rasmussen's road-following code, than communicate with the RoadFollower module via a simple socket connection (kind of like the IMU does).  The RoadFollower module will then pass along the votes to the arbiter as a normal module would, as well as potentially receiving maps from SV and LADAR, which it could pass along to the WinXP road-following code  (which could then use the maps for more accurate road-following.)
