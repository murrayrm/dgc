#ifndef STEREOPLANNER_HH
#define STEREOPLANNER_HH

//Use military time, so 730 corresponds to 7:30AM, 2:57PM corresponds to 1457
#define EARLY_MIL_TIME_LIMIT 400
#define LATE_MIL_TIME_LIMIT 730
//At 6:00 it is 93 degrees, so subtract 45 degrees and use radians
#define LOWER_YAW_LIMIT 0.838
//At 7:30 it is 106 degrees, so add 45 degrees and use radians
#define UPPER_YAW_LIMIT 2.635

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include "StereoPlannerDatum.hh"


using namespace std;
using namespace boost;

typedef boost::recursive_mutex::scoped_lock Lock;

bool StereoNotPointedAtSun(double Yaw);

class StereoPlanner : public DGC_MODULE {
public:
  // and change the constructors
  StereoPlanner();
  ~StereoPlanner();

  // The states in the state machine.
  // Uncomment these if you wish to define these functions.

  void Init();
  void Active();
  //  void Standby();
  void Shutdown();
  //  void Restart();

  // The message handlers. 
  //  void InMailHandler(Mail & ml);
  //  Mail QueryMailHandler(Mail & ml);

  // the status function.
  //string Status();


  // Any functions which run separate threads
  
  // Method protocols
  int  InitStereo();
  void StereoUpdateVotes();
  void StereoShutdown();

  void UpdateState(); // grabs from VState and stores in datum.

  void StereoCaptureThread();

private:
  // and a datum named d.
  StereoPlannerDatum d;
};



// enumerate all module messages that could be received
namespace StereoMessages {
  enum {
    // we are not yet receiving messages.
    // A place holder
    NumMessages
  };
};






#endif
