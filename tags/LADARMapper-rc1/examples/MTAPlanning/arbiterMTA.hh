/* arbiterMTA.hh - arbiter MTA module header
 *
 * Richard Murray
 * 10 January 2004
 * 
 * This file defines the format for the arbiter messages.  It will be
 * include by all modules that talk to the arbiter.
 *
 */

#ifndef ARBITERMTA_HH
#define ARBITERMTA_HH

#include "MTA/DGC_MODULE.hh"
#include "MTA/Misc/Time/Timeval.hh"
#include "MTA/Misc/Mail/Mail.hh"

struct arbiterMTA_InitVoterMsg
{
  Timeval Timestamp;		// time stamp when data was sent
  char voter;			// voter source (one character code)
  double angles[2];		// angles to vote at (return)
};

struct arbiterMTA_NewVotesMsg
{
  Timeval Timestamp;		// time stamp when data was sent
  char voter;			// voter source (one character code)
  int seqnum;			// sequence number
  double votes[2];		// votes (two doubles for now)
};

// enumerate all module messages that could be received
namespace arbiterMessages {
  enum {
    InitVoter,			// Announced that a new voter is here
    NewVotes,			// new set of voting data
    NumMessages			// Placeholder (not used)
  };
};

#endif
