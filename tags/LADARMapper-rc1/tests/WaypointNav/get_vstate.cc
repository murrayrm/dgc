/*
** get_vstate.cc
**
**  
** 
**
*/

#include "WaypointNav.hh" // standard includes, arbiter, datum, ...

extern int SIM; // get the simulation flag from the main function
extern int PRINT_VOTES; // get the print_votes flag from the main function

int SimUpdatePeriod = 10000;  // microseconds

void WaypointNav::GrabVStateLoop()
{ 
  if( SIM ) {
    SimInit();
    while( ! ShuttingDown() ) {

      SimUpdate( (float) SimUpdatePeriod / 1000000.0 );
      usleep(SimUpdatePeriod);
    }
  } 
  else {

    while( ! ShuttingDown() ) {

      Mail msg = NewQueryMessage( MyAddress(), MODULES::VState, VStateMessages::GetState);
      Mail reply = SendQuery(msg);
      
      reply.DequeueFrom( (void*) & (d.SS), sizeof(d.SS));
    
      usleep(10000);
    }
  }

} // end WaypointNav::GrabVStateLoop() 

// end get_vstate.cc
