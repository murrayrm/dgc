#include "magnetometer.h"

extern int mm_command_return_num[MM_COMMAND_NUM]; // defined in magnetometer.c

int main()
{
	float a[MM_MAX_RETURN_PARAM_NUM];
	int error;

	mm_init(0);
	//sleep(1);
	//sleep(2);
	if((error=mm_send_command(MM_LC_SC, a, mm_command_return_num[MM_LC_SC]))<0)
		fprintf(stderr, "Error reading last calibration score\n");
	else printf("last calibration score: H:%f V:%f M:%f\n", a[0], a[1], a[2]);

	/*if((error=mm_send_command(MM_CR_CA, a, mm_command_return_num[MM_CR_CA]))<0)
		fprintf(stderr, "Error clearing calibration\n");
	else printf("clear calibration score: H:%f V:%f M:%f\n", a[0], a[1], a[2]);*/
   	
	
	printf("before calling compass\n"); fflush(stdout);
	if((error=mm_send_command(MM_COMPASS_UP, a, mm_command_return_num[MM_COMPASS_UP]))<0)
		fprintf(stderr, "Error reading commpass update\n");
	else printf("compass: Error: %d %f\n", error, a[0]);

	if((error=mm_send_command(MM_MM_UP, a, mm_command_return_num[MM_MM_UP]))<0)
		fprintf(stderr, "Error reading magnetometer update\n");
	else printf("magnetometer:Error: %d X:%f Y:%f Z:%f\n", error, a[0], a[1], a[2]);
	if((error=mm_send_command(MM_IN_UP, a, mm_command_return_num[MM_IN_UP]))<0)
		fprintf(stderr, "Error reading inclinometer update\n");
	else printf("inclinometer: Error: %d P:%f R:%f \n", error, a[0], a[1]);
	if((error=mm_send_command(MM_TEMP_UP, a, mm_command_return_num[MM_TEMP_UP]))<0)
		fprintf(stderr, "Error reading tempertature update\n");
	else printf("magnetometer: Error: %d T:%f \n", error, a[0] );
	if((error=mm_send_command(MM_OW_UP, a, mm_command_return_num[MM_OW_UP]))<0)
		fprintf(stderr, "Error output word update\n");
	else printf("magnetometer: Error: %d C:%f P:%f R:%f X:%f Y:%f Z:%f T:%f \n", error, a[0], a[1], a[2], a[3], a[4], a[5], a[6] );
	
	if((error=mm_send_command(MM_LC_SC, a, mm_command_return_num[MM_LC_SC]))<0)
		fprintf(stderr, "Error reading last calibration score\n");
	else printf("last calibration score: H:%f V:%f M:%f\n", a[0], a[1], a[2]);

	mm_uninit();

	return 0;
}
