/*
 * vdrive - vehicle driving program
 *
 * Richard M. Murray
 * 30 Dec 03
 *
 * This program is a simple front end for the vehlib library.  It allows
 * you to control the vehicle through the joystick or remotely and displays
 * the vehicle state.
 *
 * 20 Jan 04, RMM: revised to allow use of new device driver modules.  All
 * old code is ifdef'd out using the name of the package.  As new drivers
 * are integrated, delete the ifdef's and rewrite as needed.


 * 01 Feb 04  Added ifdefs for CAPTURE and CRUISE to allow them to be turned
 * off during debugging.

 */

/* Standard header files defined in unix */


#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <iostream.h>
#include <getopt.h>
#include <math.h>
#include "vdrive.h"			/* vdrive functions */

/*
 * DGC libraries
 *
 * This section includes the definitions for libraries that are part
 * of the DGC code set.  These libraries are *not* part of vehlib and
 * are maintained elsewhere.
 *
 */

#define BRAKE      //thrns on the brake components
#define THROTTLE   //turns on the throttle components
#define ACCEL      //turns on the accel components
#define STEER
#define MTA
#define JOYSTICK
#define CRUISE
#define CAPTURE
#define VMANAGE				/* turn on remote interface */

#ifdef MTA
/* MTA header files */
#include "MTA/Modules.hh"
#include "MTA/Kernel.hh"
#include <boost/shared_ptr.hpp>
#include "MTA/Misc/Time/Time.hh"        //needed for usleep_for()

#include "vdmta.hh"
struct VDrive_CmdMotionMsg mtacmd;      /* commanded motion from MTA */

#include "VState.hh"			/* grab message format for VState */
struct VState_GetStateMsg vehstate;	/* current vehicle state */

extern int MTA_VDRIVE_VALID;
#endif
int mta_flag = 1;			/* run MTA */

/* Sparrow */
#include "sparrow/display.h"
#include "sparrow/dbglib.h"
#include "sparrow/flag.h"
#include "sparrow/keymap.h"

char config_file[FILENAME_MAX] = "vdrive.ini"; 
pthread_t dd_thread;			/* display thread */
int display_flag = 1;			/* dynamic display? */

char dumpfile[FILENAME_MAX] = "vdrive.dat";
static FILE *capfile = NULL;
int capflag = 0;			/* turn data capture on/off */
int caplock = 0;			/* locking variable for capture */
pthread_t cap_thread;			/* data capture thread */
void *capture_start(void *);		/* capture startup routine */
#define CAP_SLEEP 10000			/* set capture rate ~100 Hz */

/* 
 * Device driver definitions
 * 
 * For each device driver, we include the header file and define any
 * variables needed in this file to manage the device thread.
 *
 */

#include "vehports.h"			/* default port numbers */



// ---------------------------------------------------------------------
#ifdef VMANAGE
#include "VStarPackets.hh"

void *vmanage_start(void*);
pthread_t vmanage_thread;
double    VM_Max_Velocity=0;
#endif
// ---------------------------------------------------------------------



/* Joystick driver */
#ifdef JOYSTICK
#include "joystick.h"  /*this is outside of joystick only to compile without joystick, 
/* Define variables for joystick thread */
pthread_t joystick_thread;		/* Joystick thread */
void *joystick_start(void *);		/* Joystick startup routine */
struct joy_data joy;			/* Joystick data */

#endif
int JOYSTICK_IN_CHARGE = false;         // joystick can take charge by pressing buttons
int joy_count = 0;		        /* flag for enabling joystick */
int joystick_flag = 0;		        /* flag for enabling joystick */

/* transmission driver */
#include "transmission.h"
int current_gear = 3;		/* keeps track of gear -1 = reverse, 0 = neutral, 1 and 3 = 1st and 3rd, 10 = in transition
//-- TBD: assume starting in 3rd for now*/
int trans_gear = 3;

/* Steering thread */
#ifdef STEER
#include "steer.h"

int steer_port = STEER_SERIAL_PORT;     /* copy of port number */
pthread_t steer_thread;			/* steering thread */
void *steer_start(void *);		/* steering startup routine */
double str_angle;			/* steering angle offset */
double steer_off = -0.12;		/* steering angle */
int steer_count = 0;
#define STEER_EPS 0.01			/* amount to increment steering */
#define STEER_CYCLE_USEC 10000		/* delay time for steering servo */
int steer_inc(long), steer_dec(long);	/* callbacks for changing angle */
int steer_cal(long);			/* calibrate steering */
int steer_setzero(long);                   /* manually set steering zero point*/
int steer_position = -1;
int steer_position_counter = -1;
int steer_last_position = -1;
#endif
int steer_flag = 0;			/* flag for enabling steering */

/* Brake driver */
#ifdef BRAKE
#include "brake2.h"
double brk_pos, thr_pos;		/* brake and accelerator position */
int brake_lock = 0;		/* use for memory locking */  //moved this definition from brake.h
pthread_t brake_thread;			/* brake thread */

//changed the following from defines to doubles
#ifndef BRAKE_ACC
extern double BRAKE_ACC, BRAKE_VEL; //deleted MAX_BRAKE because it is not used any more
#endif
#define SPEED_MIN 0.001			/* speed limit for assuming zero */
double cruise_stop = 0.75;         //cmd_acc value that cruise control loop uses when speed_ref == 0
//can to any safe, positive value since it is negated before passing to the brake/throttle allocator
//TBD: do these need to be caps or lowercase??  Not sure if the distinction is usage or definition

int full_stop(long);            	/* callback for emergency full stop */
int brake_reset_cbe(long);              //callback for brake reset
int brake_flag = 0;			/* flag for enabling braking */
double init_pos = 0;  //position in 0-1 ro init to
#endif


/* Throttle driver */
//note that throughout the vehlib-2.0 architecture, accel means throttle + brake 
#ifdef THROTTLE
#include "throttle.h"

int throttle_port = PP_GASBR;   /* copy of port number */   //changed from THROTTLE_PARALLEL_PORT
#endif
int throttle_flag = 0;			/* flag for enabling accelerator */
int throttle_par_port_flag =0;               //to let the brake know if it can use the port, 
                                        //even if the actuator fails to initialize

/* Acceleration thread */
//note that throughout the vehlib-2.0 architecture, accel means throttle + brake
#ifdef ACCEL
#include "cruise.h"

pthread_t accel_thread;			/* braking thread */
void *accel_start(void *);		/* braking startup routine */
double ACCEL_EPS = 0.01;		/* amount to increment braking */
int accel_inc(long), accel_dec(long);	/* callbacks for changing accel */
double cruise_gain = 1;			/* gain for cruise control */
double speed_ref = 0;			/* velocity */
double accel_off = 0;			/* acceleration offset */
#endif

int acc_count = 0;			/* keep track of accel writes */
int cruise_flag = 0;			/* cruise control flag */

//vdrive test functions
int user_estop_call(long);
int user_estop_pause();                    //callback for user_estop_pause testing function
int user_estop_resume();

int estop_call_flag = 0;
int estop_flag = 1;			/* enable/disable estop */

int throttle_flag_condition = 0;
int brake_flag_condition = 0;
int steer_flag_condition = 0;
int magnetometer_flag_condition = 0;
int gps_flag_condition = 0;
int IMU_flag_condition = 0;
int joystick_flag_condition = 0;
//int cruise_flag_condition = 0;

//test function constants
double accel_test_scale_value = .5;  //this scales the amplitude of the accel test output
double accel_test_omega = .02;        //this scales the time dependence of the accel 
                                     //test output--be careful setting accel_test_omega
                                     //!= steer_test_omega
double steer_test_scale_value = 1;   //this scales the amplitude of the steer test output
double steer_test_omega = .02;        //this scales the time dependence of the steer
                                     //test output--be careful setting accel_test_omega
                                     //!= steer_test_omega

// A Vdrive module for MTA
#ifdef MTA
VDrive *p_myVDrive = new VDrive;
#endif

/*
 * Local definitions
 *
 * Now go thorugh and define all of the functions and variables that
 * are specific to this file.
 *
 */

/* Functions defined in this file */
int user_quit(long);
int user_load_parmtbl(long), user_save_parmtbl(long);
int trans_setposition_mta(int gear);

/* Vehicle mode */
int vdrive_mode(DD_ACTION, int);
enum veh_mode {
  modeOFF = 'o', modeMANUAL = 'm', modeREMOTE = 'r', modeAUTONOMOUS = 'a', modeTEST = 't'
};
enum veh_mode mode_steer = modeOFF;
enum veh_mode mode_drive = modeOFF;

/* Options and usage message */
static char *short_options = "vrdtehbjmsl:?";
static struct option long_options[] = {
  {"help", no_argument, NULL, 'h'},
  {"log", required_argument, NULL, 'l'},
  {"sim", no_argument, NULL, 'S'},
  {NULL, 0, NULL, 0}
};
static char *usage = "\
Usage: %s [-v] [options]\n\
  -b	disable braking subsystem\n\
  -d	disable display (control via vremote, vmanage) \n\
  -e	disable vmanage error message for estop\n\
  -h	print this message\n\
  -j    disable joystick\n\
  -l file \n\
	log debugging messages to a file\n\
  -m	disable MTA subsystem\n\
  -s	disable steering subsystem\n\
  -t	disable throttle subsystem\n\
  -v	turn on verbose error messages\n\
  --sim simulation mode (equivalent to -b -s -t -m -j)\n\
";


/* Display tables; include at end so they have access to local variables */
#include "vddisp.h"
#include "vdparm.h"

/*
 * vdrive main
 * 
 * This is the main routine for vdrive.  It parses command line arguments,
 * starts up the threads used by vdrive, and starts the MTA loop.
 *
 */

int main(int argc, char **argv)
{
    int c, errflg = 0, error = 0;

    /* Turn on error processing during startup */
    dbg_all = dbg_outf = 1;
    dbg_flag = 0;			/* set with -v if you want output */

    /* 
     * Parse command line arguments
     *
     * Each device should have a flag that can be used to disable that 
     * device/thread from the command line.  Set flag to -1 to indicate
     * that device should not be initialized.
     */

    while ((c = getopt_long(argc, argv, short_options, long_options, NULL)) 
	   != EOF)
      switch (c) {
      case 'v':		dbg_flag = 1;			break;
      case 'm':		mta_flag = 0;			break;
      case 'e':		estop_flag = 0;			break;
      case 's':		steer_flag = -1;		break;
      case 't':		throttle_flag = -1;		break;
      case 'b':		brake_flag = -1;		break;
      case 'j':		joystick_flag = -1;		break;
      case 'd':		display_flag = 0;		break;
      case 'h':		errflg++;			break;

      case 'S':				/* disable all devices */
	mta_flag = 0;
	steer_flag = throttle_flag = brake_flag = -1;
	joystick_flag = -1;
	break;

      case 'l':				/* open a log file */
	dbg_openlog(optarg, "a");
	break;
	
      default:		errflg++;			break;
	break;
      }

    /* Print an error message if anything went wrong */
    if (errflg || argc < optind) {
      fprintf(stderr, usage, argv[0]);
      exit(1);
    }

    /*
     * Initialize devices
     *
     * This section of the code initializes each device that vdrive
     * needs to talk to.  If device_flag is set to -1, device has
     * been disabled on command line and should *not* be initialized.
     * Otherwise, attempt to open the device and set flag = 0 or 1 to 
     * indicate whether it was successful.
     *
     */

    /* Load initialization file */
    if (dd_tbl_load(config_file, parmtbl) < 0) {
      dbg_error("couldn't load vdrive conifiguration file: %s\n", config_file);
      ++error;
    }

#   ifdef JOYSTICK
    /* Initalize anything that might cause an error */
    dbg_info("joystick");
    if (joystick_flag == 0) {
      if (joy_open(JOYSTICK_PORT, &joy) < 0) ++error; else joystick_flag = 1;
    }
#   endif

#   ifdef STEER
    dbg_info("steering: %d", steer_flag);
    if (steer_flag == 0) { 
      if (steer_open(steer_port) != 1) {
	++error; 
	dbg_info("steering open failed");
	if(steer_calibrate() != 1) ++error; else ++steer_flag;
      }
      else ++steer_flag;
    }
#   endif

    // note that the throttle must be opened as below before the
    // brake actuator can be used since the brake pot communicates
    // over the throttle parallel port
#   ifdef THROTTLE
    dbg_info("Initializing--throttle: %d\n", throttle_flag);
    if (throttle_flag == 0) {
      if (throttle_open(PP_GASBR) < 0) {  //WNH 1-31-04: previously init_throttle
	dbg_error("Throttle initialization failed\n");
	++error; 
      } 
      else if(throttle_calibrate() < 0){
	dbg_error("Throttle calibration failed\n");
	++error;
	throttle_par_port_flag=1;  //can use the parallel port for the brak, but
	                      //the actuator failed to initialize
      }  //moves throttle to 0 and calibrates
      else {
	throttle_par_port_flag=1;  //can use the brake
	throttle_flag = 1;    //and the actuator
	//	cout << "Throttle OK";
	dbg_info("Throttle OK\n");
      }
    }
#   endif

    // note that the throttle must be opened as below before the brake
    // actuator can be used since the brake pot communicates over the
    // throttle parallel port 
#   ifdef BRAKE
    dbg_info("Initializing--brake: %d\n", brake_flag);
    if (brake_flag == 0) {
      if (throttle_par_port_flag != 1) {
	dbg_error("Can't run brake without throttle\n");
	++error;

      } else if (brake_open(BRAKE_SERIAL_PORT) < 0) {  
	++error; 
	dbg_error("Brake initialization failed\n");

      } else {
	// Move brake to initial position 
	dbg_info("Setting brake position to zero...\n");
	//brake_calibrate();  //turn this on to perform a brake calibration to the limit switches
#       ifndef BRAKE_OLD
	if (brake_home() >= 0) {
	  brake_flag = 1;
	  dbg_info("Brake OK\n");
	} else
	  dbg_error("Brake homing failed");
#       else
	brake_write(init_pos, BRAKE_VEL, BRAKE_ACC); 
	usleep_for(2000000);
	brake_flag = 1;
	dbg_info("Brake OK\n");
#       endif
      }
    }
#   endif

    /* Pause if there are any errors or verbose */
    dbg_info("[error = %d], steer = %d, brake = %d, throttle = %d\n", 
	     error, steer_flag, brake_flag, throttle_flag);
    if (dbg_flag || error) {
      if (error) fprintf(stdout, "Errors on startup; %s",
			 display_flag ? "continue (y/n)? " : "continuing\n");
      else if (dbg_flag && display_flag) fprintf(stderr, "Continue (y/n)? ");
      if (display_flag && getchar() != 'y') exit(1);
    }

    /* Turn off printf while display is running */
    /* dbg_all = 0; */
    // dbg_add_module("brake2.c");
    // dbg_add_module("serial2.c");

    /* Initialize the display package */
    if (display_flag) {
      dbg_info("initializing dynamic display\n");
      if (dd_open() < 0) exit(1);	/* initialize the database, screen */
      dbg_info("binding keys\n");
      dd_bindkey('Q', user_quit);
      dd_bindkey('M', mode_manual);
      dd_bindkey('A', mode_autonomous);
      dd_bindkey('R', mode_remote);
      dd_bindkey('O', mode_off);
      dd_bindkey(20, mode_test);  //WNH, 2-12-04 added a simple test mode 
      //for steer/accel bound to ctrl+t  make sure you know what it does before using it!
      dd_bindkey('c', user_cap_toggle);

      dd_bindkey(150, user_estop_toggle);  //WNH, 2-12-04 added an estop function for testing purposes
      //make sure you know what this does before using it!  Bound to ctrl+F10

#   ifdef STEER
      dd_bindkey('k', steer_inc);
      dd_bindkey('j', steer_dec);
#   endif
#   ifdef ACCEL
      dd_bindkey('i', accel_inc);
      dd_bindkey('m', accel_dec);
#   endif
#   ifdef BRAKE
      dd_bindkey(' ', full_stop);  //going to continue to use the full_stop command 
      //in order to be able to call throttle_pause and brake_pause
      //this is the code from where it is defined in diplay.c in sparrow:
      //extern int dd_bindkey(int key, int (*fcn)(long));
#   endif
      dbg_info("setting display to main display\n");
      dd_usetbl(vddisp);		/* set display description table */
    }

    /* Start up threads for execution */
    fprintf(stderr, "starting threads\n");

#   ifdef JOYSTICK
    dbg_info("starting joystick thread");
    pthread_create(&joystick_thread, NULL, joystick_start, (void *) NULL);
#   endif

#   ifdef STEER
    dbg_info("starting steer thread");
    pthread_create(&steer_thread, NULL, steer_start, (void *) NULL);
#   endif

#   ifdef ACCEL
    dbg_info("starting accel thread");
    if (pthread_create(&accel_thread, NULL, accel_start, (void *) NULL) < 0) {
      perror("accel");
      exit(1);
    }
#   endif

#   ifdef BRAKE
    dbg_info("starting brake thread");
    if (pthread_create(&brake_thread, NULL, brake_servo,
		       (void *) &brake_flag) < 0) {
      perror("brake");
      exit(1);
    }
#   endif

#   ifdef DO_NOT_USE //CAPTURE
    if (pthread_create(&cap_thread, NULL, capture_start, NULL) < 0) {
      perror("capture");
      exit(1);
    }
#   endif

    /* Run the display manager as a thread */
    fprintf(stderr, "starting dd_loop\n");
    dbg_all = 0;			/* turn off general debugging */
    if (display_flag)
      pthread_create(&dd_thread, NULL, dd_loop_thread, (void *) NULL);

#   ifdef MTA
    if (!mta_flag)
#   endif
      /* If we aren't running MTA, wait until dd_loop ends */
      pthread_join(dd_thread, (void **) NULL);
#   ifdef MTA
    else {
      /* Otherwise, pass control to MTA */

      Register(shared_ptr<DGC_MODULE>( p_myVDrive ));
#      ifdef VMANAGE
       if (estop_flag == 1 && pthread_create(&vmanage_thread, NULL, vmanage_start, (void *) NULL) < 0) {
	 perror("vmanage");
	 exit(1);
       }
#      endif
       StartKernel();
    }
#   endif
    dd_close();				/* clear the screen and free memory */

    /* User cleanup goes here */
}














/* 
 * Process threads 
 *
 * Each function in this section starts up a thread to process data
 * from a device or module.
 *
 */

#ifdef VMANAGE

/* vmanage thread */
void *vmanage_start(void *arg)
{
  VM_for_VD_Packet vd_packet;
  char estopVal = 0;

  // Set velocity at which we should simply stomp on the brakes
  const double max_velo_threshold = MAX_VEL_THRESHOLD; 

  VM_Max_Velocity = 0.0; // dont let the car go anywhere yet.
  sleep(2); // give things a chance to initialize

  Timeval lastVMUpdate = TVNow() - Timeval(100, 0); // make old time

  // wait till vdrive mta is ready
  while( ! MTA_VDRIVE_VALID ) usleep(100000);

  //  Make sure that we know vmanage has been found
  //  for( int i =0; i<100; i++) cout << "VMANAGE IS A GO" << endl;

  while (1) {
    //    cout << "Insanity check" << endl;

    // If joystick controls the vehicle then do not do much
    // (other than not get in it's way)
    if( JOYSTICK_IN_CHARGE ) {
      mode_steer = modeMANUAL;
      mode_drive = modeOFF;
      user_estop_resume();
      
      //      cout << "JOYSTICK MODE" << endl;
      // cout << "VMANAGE_THREAD: JOYSTICK IS IN CHARGE" << endl;

    } else {

      //      cout << "Checking if more than a second " << endl;

      // if it has been more than 1 second since we've talked with VManage
      if( TVNow() - lastVMUpdate > Timeval( 1, 0)) { 
	// set estop pause until we do
	//	cout << "Cannot find VManage, EStop Pause Set Until it is found" << endl;
	user_estop_pause();

	sleep(1);
      }
      
      //      cout << "Trying to get VManage packet " << endl;
      
      int vm_ret = Get_VM_for_VD_Packet( p_myVDrive->MyAddress(), vd_packet);
      if( vm_ret < 0 ) {
	continue; // try again
	vmanage_count = -1;
      } else {
	lastVMUpdate = TVNow(); // else we got a good update.
	vmanage_count++;
      }

      
      // Now set all of the appropriate options.
      // first check EStop, once again, only apply estop if below threshold
      if( vd_packet.max_velo < max_velo_threshold &&
	  (vd_packet.estop.EStop == 'p' ||
	   vd_packet.estop.EStop == 'P' ||
	   vd_packet.estop.EStop == 'd' ||
	   vd_packet.estop.EStop == 'D' )) { 
	
	user_estop_pause();
	VM_Max_Velocity = 0.0; // dont let the car go anywhere.
      } else {
	
	// check the modes for steer and accel
	switch ( vd_packet.smode ) {
	case 'a': case 'A': mode_steer = modeAUTONOMOUS;        break;
	case 'o': case 'O': mode_steer = modeOFF;               break;
	case 'm': case 'M': mode_steer = modeMANUAL;            break;
	case 'r': case 'R': mode_steer = modeREMOTE;            break;
	case 't': case 'T': mode_steer = modeTEST;              break;
	default: 
	  cout << "VManage Thread: Invalid Steer Input "
	       << vd_packet.smode << ", Setting To Autonomous" << endl;
	  mode_steer = modeAUTONOMOUS;
	}
	
	switch ( vd_packet.amode ) {
	case 'a': case 'A': 
	  mode_drive = modeAUTONOMOUS;	/* set mode to autonomous */
	  cruise_flag = 1;		/* turn on cruise control */
	  break;
	case 'o': case 'O': mode_drive = modeOFF;               break;
	case 'm': case 'M': mode_drive = modeMANUAL;            break;
	case 'r': case 'R':
	  if (mode_drive != modeREMOTE) {
	    cruise_flag = 0;		/* require cruise to be set from kbd */
	  }
	  mode_drive = modeREMOTE;
	  break;
	case 't': case 'T': mode_drive = modeTEST;              break;
	default: 
	  cout << "VManage Thread: Invalid Accel Input " 
	       << vd_packet.amode << ", Setting To Autonomous" << endl;
	  mode_drive = modeAUTONOMOUS;

	  //RAYCROFT  hey ike, how do you get back into autonomous, with cruise enabled?
	  //we really really need to make sure that cruise_flag gets set to 1 when we want to go again.

	}
	
	// Now cap the velocity that the cruise control can use
	VM_Max_Velocity = vd_packet.max_velo;
	user_estop_resume();
      }
    }

    /* Sleep for a bit */
    //    cout << "VMANAGE ONE LOOP" << endl;
    usleep(100000);			/* 100 msec */
  }
  return NULL;
}
#endif





















#ifdef JOYSTICK
/* Joystick thread */
void *joystick_start(void *arg)
{
  const int B_TOP_TRIGGER = 8;// 0x1000; // code for top button on joystick
  const int B_BOT_TRIGGER = 4;// 0x100;  // code for trigger finger on handle
  const int B_BOT_INSIDE  = 2;// 0x10;   // inside 2 buttons share this code
  const int B_BOT_OUTSIDE = 1;// 0x1;    // outside 2 buttons

  /* Thread just reads the joystick port forever */
  while (1) {
    //    if (joystick_flag == 1) joy_read(&joy);
    // always read joystick
    joy_read(&joy);

    joy.buttons = abs(joy.buttons); // make positive.
    
    if ( joy.buttons != 16 ) dbg_info("Buttons = &d", joy.buttons);

    // check the buttons 
    if( joy.buttons == (16 -( B_TOP_TRIGGER + B_BOT_TRIGGER))) {
      // joystick takes control (manual mode)
      JOYSTICK_IN_CHARGE = true;
    } else if ( joy.buttons == (16 -( B_BOT_OUTSIDE + B_BOT_INSIDE))) {
      // joystick puts vehicle back in normal mode
      JOYSTICK_IN_CHARGE = false;
    }

    ++joy_count;

    usleep(10000);
  }
  return NULL;
}
#endif










#ifdef ACCEL

#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>

// The famous failure of the Ike-One-Line-Function 
void EMPTY_SIG_HANDLER(int sig){  signal(sig, &EMPTY_SIG_HANDLER);}

int ACCEL_PID = -1;

/* Throttle and brake thread */
void *accel_start(void *arg)
{
  signal(SIGUSR1, &EMPTY_SIG_HANDLER);
  ACCEL_PID = getpid();


  double cmd_acc;  /* commanded accleration */ 
  double cmd_vel;  /* commanded velocity */    //TBD: i think this never got used
  //probably what speed_ref does now

  while (1) {

    switch (mode_drive) {

    case modeMANUAL:
      /* Use the joystick to command motion */
      speed_ref = 10*(joy.data[1] - joy.offset[1])/joy.scale[1]; //the 5 scales us from 
      if (speed_ref<0.0) speed_ref=(speed_ref/5.0);   //5m/s forward, 1 m/s reverse.
      //-5 m/s to +5 m/s.
      break;
      
    case modeREMOTE:      
      //if cruise is running we get speedref from sparrow
      break;
      
    case modeAUTONOMOUS:
      speed_ref = mtacmd.velocity_cmd; //WNH 2-14-04, gets speed data from MTA
      //truncate to maximum safe velocity according to VManage
      if (estop_flag ==1) {
	if ( speed_ref >= 0.0 ) speed_ref = fmin( speed_ref, VM_Max_Velocity ); 
	else                    speed_ref = fmax( speed_ref, -VM_Max_Velocity );
      }
      break;
      
    case modeTEST:
      //simple test function to drive the accel and steer in sinusoids
      cmd_acc = accel_test_scale_value * sin(acc_count * accel_test_omega);
      break;

    case modeOFF:
      //thr_pos = 0;  //WNH 2-15-04
      //brk_pos = 0;
      cmd_acc = 0;
      break;
    }

    if (mode_drive==modeAUTONOMOUS || mode_drive==modeREMOTE || mode_drive==modeMANUAL) {
      if (cruise_flag) {
	if(fabs(speed_ref) < SPEED_MIN) {
	  cmd_acc = -cruise_stop; //need to use some safe maximum braking value here	  
	}
	else if(speed_ref > 0.0) {
	  trans_setposition_mta(THIRD); //switch the gear--this might need to call an MTA command to vmanage
	  current_gear = 3; //record the gear
	  cmd_acc = cruise_gain * cruise(speed_ref) + accel_off;	  
	} 
	else if( speed_ref < 0.0) {
	  trans_setposition_mta(REVERSE); //switch the gear--this might need to call an MTA command to vmanage
	  current_gear = -1; //record the gear
	  cmd_acc = cruise_gain * cruise(speed_ref) + accel_off;
	}
      } 
      else cmd_acc = accel_off;
    }
    
    
    /* Check limits and allocate out the command */
    if (cmd_acc > 1) cmd_acc = 1;
    if (cmd_acc < -1) cmd_acc = -1;
    thr_pos = (cmd_acc < 0) ? 0 : cmd_acc;
    brk_pos = (cmd_acc < 0) ? -cmd_acc : 0;

    /* Send command to the throttle */
    if (throttle_flag == 1) {  //TBD: do we need a throttle lock here like we use on the brake?
      ++acc_count;
      throttle_write(thr_pos);
    }

    /* Send command to the brake */
    if (brake_flag == 1) {
      ++acc_count;           //TBD: should this first check to see if break_lock is on?
      brake_lock = 1;
      brake_write(brk_pos, BRAKE_VEL, BRAKE_ACC);
      brake_lock = 0;
    }

    /* Sleep for 100 msec to allow everyone to keep up */
    usleep_for(100000);  //TBD: this thread is not running at 100Hz already, so leaving as usleep(), OK?
  }
  return NULL;
}
#endif





















#ifdef STEER

int STEER_PID = -1;

void *steer_start(void *arg)
{
  static double old_angle = 0;		/* save MTA commanded angle */

  signal(SIGUSR1, &EMPTY_SIG_HANDLER);
  STEER_PID = getpid();

  /* Thread just reads the joystick port forever */
  while (1) {
    switch (mode_steer) {


#ifdef JOYSTICK
    case modeMANUAL:
      /* Use the joystick to command the angle */
      str_angle = (joy.data[0] - joy.offset[0])/joy.scale[0];
      //if (str_angle < -1) str_angle = -1; //added in below
      //if (str_angle >  1) str_angle =  1;
      break;
#endif //commented for debugging JCL 02/07/04


    case modeREMOTE:
      /* Use the reference value to command the position */
      str_angle = steer_off;
      //if (str_angle < -1) str_angle = -1;  //added in below
      //if (str_angle >  1) str_angle =  1;
      break;


#ifdef MTA
    case modeAUTONOMOUS:
      /* Get data from MTA */
      old_angle = str_angle = mtacmd.steer_cmd;
      str_angle += steer_off;
      break;
#endif


    case modeTEST:
      //simple test function to drive the accel and steer in sinusoids
      str_angle = steer_test_scale_value * cos(acc_count * steer_test_omega);
      break;


    case modeOFF:
      //      str_angle = 0; //WNH 2-15-04 - as per RMM, this should not change
      //the steering position, but just leave it where is was before
      /* TODO: we should disable steering here */
      break;
    }

    /* Limit the steering angle if commanded out of range */
    if (str_angle <= -1) str_angle = -0.99;
    if (str_angle >=  1) str_angle =  0.99;

    if (steer_flag == 1) {
      
      /* Write the data to the steering controller */
      if (steer_heading(str_angle) < 0) {
	/* Print out an error message */
	steer_count = -1;   //this is not very robust.  TBD: This kind of error 
	//checking needs to be done in steer.c so that the process can be restarted.  
      } 
	
      else
	++steer_count;
      
      /* TBD: Update the steering state (needed?) */
      /* Note: this could generate lots of messages across serial */
      /* steer_state_update(0, 0); */
    }

#ifdef UNUSED
/*********************************************************88
\\the following is a hack ... it will increment a counter everytime the steer loop is called
\\when that counter reaches a certain value, it will check the actual steering motor position
\\if the motor position is equal to the last motor position, then command a small move left
\\of current position and a small move right.  then command the commanded angle */
//    if (steer_position_counter > 25) {
//      steer_last_position = steer_position;
//      steer_position = steer_getposition();
//      steer_position_counter = 0;
//      if(steer_position == steer_last_position) {
//	steer_setposition(steer_position - 100);
//	steer_setposition(steer_position + 100);
//\	steer_heading (str_angle);
//      }
//
//    }
//    /*******************************************************/
//    steer_position_counter++;
#endif

    /* Wait for a bit, but looking for new commands */
#   define STEER_CYCLE_DIVS 10
    for (int count = 0; count < STEER_CYCLE_DIVS; ++count) {
      /* Check to see if we have received a new command */
      /* This is intended to speed up our response */
      if (mode_steer == modeAUTONOMOUS && old_angle != mtacmd.steer_cmd) break;
      usleep(STEER_CYCLE_USEC/STEER_CYCLE_DIVS);
    }
  }
  return NULL;
}
#endif

#ifdef CAPTURE

/* Data capture */
void *capture_start(void *arg)
{
  struct timeval tv;
  static int start = 0;

  /* Initialize the starting time */
  gettimeofday(&tv, NULL); start = tv.tv_sec;

  while (1) {
    if (!capflag) continue;
    caplock = 1;			/* lock access */

    /* Save the time the data was taken */
    gettimeofday(&tv, NULL);
    fprintf(capfile, "%g\t", (tv.tv_sec - start) + 
	    ((double) tv.tv_usec) / 1e6);

    /* Fill up the rets of the data array with the data we want to capture */
    /* Order should be the same as used in the simulation module */
#   ifdef MTA
    fprintf(capfile, "%g %g %g ", vehstate.Northing, 
	    vehstate.Easting, vehstate.Altitude);
    fprintf(capfile, "%g %g %g ", vehstate.Vel_U, vehstate.Speed, 
	    vehstate.Accel);
    fprintf(capfile, "%g %g %g ", vehstate.Pitch, vehstate.Roll, vehstate.Yaw);
    fprintf(capfile, "%g %g %g ", vehstate.PitchRate, vehstate.RollRate,
	    vehstate.YawRate);

#   endif 
#   ifdef ACCEL
    fprintf(capfile, "%g %g ", brk_pos, thr_pos);
#   endif
#   ifdef STEER
    fprintf(capfile, "%g ", str_angle);
#   endif

    fprintf(capfile, "\n");
    caplock = 0;			/* turn off lock variable */
    usleep(CAP_SLEEP);			/* take data ~100 Hz */
    //    usleep(100000);
  }
}
#endif

/*
 * MTA data managers
 *
 * These functions are used to copy data into the VDriveStack for
 * transfer to VRemote.
 *
 */

int Set_VDriveStack( VDriveStack &vd)
{
  vd.mode_steer = (int) mode_steer; vd.mode_drive = (int) mode_drive;
  Set_Brake_Packet( vd.brake );
  Set_Steer_Packet( vd.steer );
  Set_Throttle_Packet( vd.throttle );
  Set_Joystick_Packet( vd.joystick );
  return 0;
}

int Set_Brake_Packet( Brake_Packet &brake)
{
  brake.Position = brk_pos;
  brake.BrakeState = brake_flag;
  brake.BrakeCase = brake_case;
  return 0;
}

int Set_Throttle_Packet( Throttle_Packet &throttle)
{
  throttle.Position = thr_pos;
  throttle.ThrottleState = throttle_flag;
  return 0;
}

int Set_Joystick_Packet( Joystick_Packet &joystick)
{
  joystick.X = (int) joy.data[0];
  joystick.Y = (int) joy.data[1];
  return 0;
}

int Set_Steer_Packet( Steer_Packet &steer)
{
  steer.Steer = str_angle;
  steer.SteerState = steer_flag;
  return 0;
}

/*
 * Display callbacks and managers
 *
 * These functions are used by the display routines to control the
 * operation of vdrive.  Each function takes a long integer as an
 * argument (recast as needed) and returns and integer.
 *
 *     mode_*		    set the operating mode
 *     user_cap_toggle	    turn capture off/on
 *     user_load_parmtbl    load parameters from file
 *     user_save_parmtbl    save parameters from file
 *     user_cal_steer	    calibrate steering
 *     vdrive_mode	    display manager for alphnumeric mode
 *     user_quit	    terminate vdrive
 */

/* Mode selection commands */
int mode_manual(long arg) { 
  mode_steer = modeMANUAL; 		/* manual steering */
  mode_drive = modeOFF; 		/* brake and throttle via pedals */
  return 0; 
}
int mode_remote(long arg) { mode_steer = mode_drive = modeREMOTE; return 0; }
int mode_off(long arg) { mode_steer = mode_drive = modeOFF; return 0; }
int mode_autonomous(long arg) { mode_steer = mode_drive = modeAUTONOMOUS; 
 if (cruise_flag != -1) {
   cruise_flag = 1; //WNH 2-20-04, turn on cruise by default in autonomous mode.  Can be turned
 }; //off manually by entering a 0 in the cruise flag spot on the sparrow interface.
 return 0; }
int mode_test(long arg) { mode_steer = mode_drive = modeTEST; return 0; } 
//added test mode 


#ifdef ACCEL
/* Increment/decrement throttle and brake */
int accel_inc(long arg) { accel_off += ACCEL_EPS; return 0; }
int accel_dec(long arg) { accel_off -= ACCEL_EPS; return 0; }
#endif

#ifdef STEER
/* Increment/decrement steering angle */
int steer_inc(long arg) { steer_off += STEER_EPS; return 0; }
int steer_dec(long arg) { steer_off -= STEER_EPS; return 0; }
int steer_setzero(long arg) {
  extern int setraw(), unsetraw();
  int strflag = steer_flag;
  steer_flag = 0;  //disable steering
  steer_setzero();
  steer_flag = strflag;  //return to initial state
  return 0;
}

int steer_cal(long arg) {
  extern int setraw(), unsetraw();
  int strflag = steer_flag;

  /* Disable steering so that we don't write to anything */
  steer_flag = 0;

  /* Set things up so that we can see debugging messages */
  unsetraw();
  DD_PROMPT("running steering calibration\n");
  dbg_add_module("steer.cc");

  steer_calibrate();

  /* Reset the screen and turn off debugging */
  setraw();
  DD_PROMPT("done");
  dbg_delete_module("steer.cc");
  dd_redraw((long) 0);

  steer_flag = strflag;
  return 0;
}
#endif

#ifdef BRAKE
/* Execute a full stop */  //note that this command, unlike user_estop, will return
//with the actuators turned off.  They will need to be reset to run again.
int full_stop(long arg) {
  int count = 0;

  /* First, push down the break: will get picked up in accel thread */
  if (brake_flag) {
    /* Check if brake loop is active */
    while (brake_lock and count++ < 10) usleep(10000); //TBD: leaving as usleep?

    /* Turn off the brake */
    brake_flag = 0;			/* turn off brake loop */
    //   brk_pos = MAX_BRAKE;		/* just in case loop restarts */
    // brk_pos = MAX_BRAKE;                //just for testing, see below
    brake_pause();               //as defined in the standards, dev_pause functions should
    //not take arguments, but for the Feb 1 test, we need to be able to set the brake position 
    //used in an estop pause easily, so an argument is used here.  The brake_pause function will
    //use the MAX_BRAKE vaule if no argument is specified.
  }

  /* Now turn off the throttle */
  if (throttle_flag) {
    throttle_flag = 0;
    throttle_pause(); //WNH 1-31-04 changed from thr_pos = 0 to throttle_pause()
    
  usleep_for(3000000); // give time for the brake to move
    /* Beep so that we know this all happened */
  DD_BEEP();
  cout << "Full stop done";  
  return 0;
  } 
#endif
}




int brake_reset_cbe(long arg) {
  int count = 0;
  if (brake_flag == 1) {  //only does anything if the brake is already on
    brake_flag = 0;
    while (brake_lock && count++ < 10) usleep(10000);
    brake_reset();
    brake_flag = 1;
  }  
  DD_PROMPT("Brake reset done");
  return 0;
}







/* Recalibrate the brake */
int brake_calibrate_cb(long arg) {
  brake_calibrate();
  DD_PROMPT("Brake calibrate done");
  return 0;
}



int user_estop_toggle(long arg) {
  return estop_call_flag ? user_estop_resume() : user_estop_pause();
} 

//estop command
int user_estop_pause() {
  int count = 0;

  /* Check to see if we have already been called */
  if (estop_call_flag++ > 0) return 1;

  //set a variable so we know what to turn back on in user_estop_resume()
  throttle_flag_condition = throttle_flag;
  brake_flag_condition = brake_flag;
  //see comment below
  //  magnetometer_flag_condition = magnetometer_flag;
  //  gps_flag_condition = gps_flag;
  //  IMU_flag_condition = IMU_flag;
  joystick_flag_condition = joystick_flag;
  //cruise_flag_condition = cruise_flag;

#ifdef UNUSED
  /* If we are driving above the velocity threshold, slow down first */
  if (vehstate.Speed > MAX_VEL_THRESHOLD) {
    int count = 0;
    while (vehstate.Speed > MAX_VEL_THRESHOLD && count++ < 100) {
      /* Set the reference speed to zero */
      speed_ref = 0;
      usleep(100000);			/* wait for 100 msec */
    }
  }
#endif

  //call all the _pause code
  throttle_flag = 0;
  brake_flag = 0;//has to be done in this order to make it work since
  //the commands executed here in this thread, are almost immediately 
  //overwritten by the cruise thread that writes a stop.
  joystick_flag = 0;
  cout << "Waiting on brake " <<endl;
  while (brake_lock && count++ < 10) usleep(10000); 
  cout << "Throttle Pause " << endl;
  throttle_pause();
  cout << "Brake Pause" << endl;
  brake_pause();
  cout <<"Steer Pause" << endl;
  steer_pause();
  //  mm_pause();
  //  gps_pause();
  //  IMU_pause();
  //  joy_pause();

  //  usleep_for(1000000); //let the brake move

  //  brake_flag = 0;
  // throttle_flag = 0;  
  //dont set steer_flag to 0 because we want to retain steering control
  //  magnetometer_flag = 0;
  //  gps_flag = 0;
  //  IMU_flag = 0;
  // cruise_flag = 0;

  cout << "Estop pause done\n"; //eventualy need to link this to the display
  estop_call_flag = 1;

  return 0;
}


//estop command
int user_estop_resume() {

  /* Check to make sure we haven't already been called */
  if (estop_call_flag == 0) return 0;

  /*
   * TBD (Will): need to think about whether this logic always works.
   * It may turn out that we call estop from multiple locations (eg,
   * VM and keyboard), in which case this could get lost doing the
   * wrong thing
   *
   */

  //call all the _resume code
  throttle_resume();
  brake_resume();
  steer_resume();
  mm_resume();
  gps_resume();
  IMU_resume();
  joy_resume();

   //set the _flag variables back to their original conditions
  throttle_flag = throttle_flag_condition;
  brake_flag = brake_flag_condition;
  //see comment above in user_estop_pause
  //  magnetometer_flag = magnetometer_flag_condition;
  //  gps_flag = gps_flag_condition;
  //  IMU_flag = IMU_flag_condition;
  joystick_flag = joystick_flag_condition;
  //cruise_flag = cruise_flag_condition;

  cout << "Estop resume done"; //eventualy need to link this to the display
  estop_call_flag = 0;

  return 0;
}


/* Toggle capture mode */
int user_cap_toggle(long arg)
{
  /* If we are turning capture on, open the logging file */
  if (!capflag) {
    /* Close capture file and reopen if needed */
    if ((capfile = fopen(dumpfile, "w+")) == NULL) return -1;

    time_t now = time(NULL);
    fprintf(capfile, "# vdrive capture file %s", ctime(&now));
    capflag = 1;

  } else {
    int i;

    /* Wait until any data capturing is done */
    capflag = 0;
    for (i = 0; i < 10 && caplock; ++i) usleep(CAP_SLEEP/10);

    /* Make sure that data is flushed and then close file */
    if (capfile != NULL) fclose(capfile);
    capfile = NULL;
  }
  return capflag;
}

/* Save the actuation parameters */
int user_save_parmtbl(long arg)
{
  if (dd_tbl_save(config_file, parmtbl) < 0) {
    dd_text_prompt("could not print data");
    dd_beep((long) 0);
  }
  return 0;
}

/* Save the actuation parameters */
int user_load_parmtbl(long arg)
{
  if (dd_tbl_load(config_file, parmtbl) < 0) {
    dd_text_prompt("could not print data");
    dd_beep((long) 0);
  }
  return 0;
}

/* Special display manager function for handling the vehicle mode */
int vdrive_mode(DD_ACTION action, int id)
{
    DD_IDENT *dd = ddtbl + id;
    char *value = (char *)dd->value;
    char itmp;
    
    switch (action) {
    case Input:
      if (DD_SCANF("Mode: ", "%c", &itmp) == 1)
	*value = (short) itmp;
      break;
	
    default:			/* default = use dd_short */
      return dd_short(action, id);
    }
    return 0;
}

/* Quit the dfan program */
int user_quit(long arg)
{
  int count = 0;

# ifdef MTA
  /* Tell MTA to shut down */
  ForceKernelShutdown();

  /* Wait for 1 second to make sure it is shut down */
  while (ShuttingDown()) {
    if (count++ > 1000) break;
    usleep(1000);
  }
  if (!ShuttingDown()) fprintf(stderr, "\nMTA Kernel shut down\n");
# endif

  /* Tell dd_loop to abort now */
  return DD_EXIT_LOOP;
}

/*
 * trans_setposition_mta(gear)
 *
 * Function to send a message to VManage requesting a change in the 
 * gear.
 *
 */

int trans_setposition_mta(int gear)
{
  // make sure mta is ready
  while( ! MTA_VDRIVE_VALID ) usleep( 100000 );

  Transmission_Packet tp;
  tp.Gear = gear;
  DD_PROMPT("sending transmission package");
  return Set_Transmission(p_myVDrive->MyAddress(), tp);
}
