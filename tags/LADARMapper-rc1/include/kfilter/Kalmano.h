/****************************************************************************
 *
 * INS/GPS, Kalman Filter Observation Data Generation
 *
 * File: kalmano.h
 * Date: 04/18/00
 *
 ****************************************************************************/

/****************************************************************************
                        [REVISION HISTORY]

  Date     Engineer             Description
==============================================================================
 02/09/00  N. Datta             Add koGenerateQuaternionData
 02/09/00  N. Datta             Add koAcceptBORL
 02/09/00  N. Datta             Add koAcceptDbs
 02/09/00  N. Datta             Add include Imu.h
 02/09/00  N. Datta             Add types TPhi, TQupdate
 02/23/00  T. Varty             Add Direction Cosine and Lever Arm types
                                Add kalmanOReinit
 03/20/00  T. Varty             Add Ins_Gps.h
 03/31/00  T. Varty             Move TDirCos definition to Imu.h
 04/03/00  T. Varty             Remove pack(0)
 04/18/00  T. Varty             Add koAcceptLauncherGeometry
 10/26/00  T. Varty             Add koAcceptGpsLeverArms
*****************************************************************************/

#ifndef KalmanOH
#define KalmanOH

//#include "Ins_Gps.h"
#include "Imu.h"

/****************************************************************************
 * Constants
 ****************************************************************************/

#define QUPD_BUF_LEN        4           // length of update data buffer

/****************************************************************************
 * Phi Type Definition
 ****************************************************************************/

typedef struct{
  double phix, phiy, phiz;
} TPhi;

/****************************************************************************
 * Quaternion Type Definition
 ****************************************************************************/

typedef struct{
  TPhi         phi;
  TQuaternion  qmid;
  int          validQuatData;
} TQupdate;  

/****************************************************************************
 * Lever Arm Type Definition
 ****************************************************************************/

typedef struct {
  double x, y, z;
} TLeverArm;


typedef struct {
  double         V1, V2, V3;
} T3dimVector;

/***************************************************************************
 * Nav Data Buffering
 ***************************************************************************/

typedef struct {
  unsigned int    tftm;
  TDirCos         bnDc;
  double          vx, vy, vz;
  double          vn, ve, vd;
  double          lat, lon, alt;
  double          calpha, salpha;
  double          xApplyDpx, xApplyDpy, xApplyDh;
  double          xApplyDvx, xApplyDvy, xApplyDvz;
  double          time;
  double          roll,pitch,yaw;
  double          thdg;
  int             KalmanUpdate;
} TSaveNavData;


/****************************************************************************
 * Function Definitions
 ****************************************************************************/

void kalmanOInit(void);
void kalmanOReinit(void);
void koGenerateWagaData(void);
void koGenerateZeroVelData(void);
void koGenMotionDetData(void);
void koGenerateBaroData(void);
//void koGenerateLosData(GpsLosChlData_t *los, int numChannels);
void koGenerateQuaternionData(int Num, TQbuf *QBuf);
//void koSaveNavData(void);
void koAcceptDbs(double x, double y, double z);
void koAcceptBORL(TQuaternion qLB);
//void koAcceptGpsLvrArms(double x, double y, double z, int lvrArmB);
//void koAcceptLauncherGeometry(double LevArm1, double LevArm2,
//                              double LevArm3, double LevArm4);
void koConvertGeoToECEF(double *posECEF, double lat, double lon, double alt);
void koGenerateExtGPSPosData( void );
void koBufferNavData( void );
void koGPSDataCheck( void );
void koGenerateExtGPSVelData( void );
void koBufferCopy( TSaveNavData *Buff1, TSaveNavData *Buff2);
void koDirCosSum( TDirCos *dc1, TDirCos *dc2, double k1, double k2, TDirCos *dc3 );

/****************************************************************************
 * End
 ****************************************************************************/
#endif
