

#ifndef StatusMessage_HH
#define StatusMessage_HH

// Each module has a status function which
// returns a string decribing how it is 
// doing, such as "IMU WORING, etc..."

// A status message is simply a string
// describing the state of the module
// so instead of declaring a struct StatusMessage,
// we can simply typedef

#include <string>
typedef std::string StatusMessage;




#endif
