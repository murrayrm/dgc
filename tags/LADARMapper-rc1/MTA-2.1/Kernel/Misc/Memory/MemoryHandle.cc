#include "MemoryHandle.hh"
#include "MemoryLibrary.hh"


MemoryHandle::MemoryHandle(void* p_ML, void* pdata, int sz) {

  p_MemoryLibrary = p_ML;
  ptr = pdata;
  size = sz;

}

MemoryHandle::~MemoryHandle() {

  MemoryLibrary * pml = (MemoryLibrary*) p_MemoryLibrary;

  pml->Return(ptr);

}

void* MemoryHandle::operator * () {
  return ptr;
}
