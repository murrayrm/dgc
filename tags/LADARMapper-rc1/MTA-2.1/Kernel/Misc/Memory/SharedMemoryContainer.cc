#include "SharedMemoryContainer.hh"
#include <map>

#include <boost/shared_ptr.hpp>

#include "../Time/Timeval.hh"
#include <iostream>

using namespace std;
using namespace boost;

// int smccount = 0;


SharedMemoryContainer::SharedMemoryContainer() {
  
  myPtr = shared_ptr<char> (new char[BlockSize] ) ;
  mySize = BlockSize;
  myL = (char*) & (*myPtr);
  myR = (char*) & (*myPtr);

  // smccount ++;
}

SharedMemoryContainer::SharedMemoryContainer(int sz) {

  myPtr = shared_ptr<char> (new char[sz] ) ;
  mySize = sz;
  myL = (char*) & (*myPtr);
  myR = (char*) & (*myPtr);

  // smccount++;
}


SharedMemoryContainer::SharedMemoryContainer(const SharedMemoryContainer & m) {

  myPtr = m.myPtr;
  mySize = m.mySize;
  myL = m.myL; //myPtr;
  myR = m.myR; //myPtr;

  //  smccount ++;
}


SharedMemoryContainer::~SharedMemoryContainer() {
  //  destruct();

  
}
/*
void SharedMemoryContainer::destruct() {

  allocated[myPtr]--;
  if( allocated[myPtr] == 0 ) {
    delete myPtr;
  }
}

void SharedMemoryContainer::insert() {
  
  if( myPtr != NULL ) {
      allocated[myPtr]++;
  }
}

*/
/*
SharedMemoryContainer& 
  SharedMemoryContainer::operator=(const SharedMemoryContainer & m) {

  destruct();

  myPtr = m.myPtr;
  mySize = m.mySize;
  myL = m.myL;
  myR = m.myR;

  insert();
}

*/

char* SharedMemoryContainer::sliderL() {
  return myL;
}

char* SharedMemoryContainer::sliderR() {
  return myR;
}

char* SharedMemoryContainer::ptr() {
  return (char*) &(*myPtr);
}

char* SharedMemoryContainer::read(int r) {
  myL += r;
  return myL;
}

char* SharedMemoryContainer::wrote(int r) {
  myR += r;
  return myR;
}

int SharedMemoryContainer::size() {
  return mySize;
}


int SharedMemoryContainer::leftToWrite() {
  return  mySize + ( (char*) &(*myPtr) - myR) ;
}


int SharedMemoryContainer::leftToRead() {
  return myR-myL;
}
