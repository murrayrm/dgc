#ifndef MATLABDISPLAYDEATUM_HH
#define MATLABDISPLAYDEATUM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "../../RaceModules/Arbiter/Vote.hh"
#include "../../RaceModules/Arbiter/SimpleArbiter.hh"
#include "../../RaceModules/Arbiter/ArbiterModule.hh"

#include "../../include/Constants.h" //for vehicle constants, ie serial ports, etc.

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"
#include "vehlib/VDrive.hh"

// The genMap struct sent via MTA
#include "genMapStruct.hh"

// Dirty way to get CorridorArcEvaluator messages definition
#include "../../RaceModules/CorridorArcEvaluator/CorridorArcEvaluator.hh"

using namespace std;

// All data that needs to be shared by common threads
struct MatlabDisplayDatum
{
    Voter matlabdisplay; // voter struct for the Arbiter

    // State struct from VState
    boost::recursive_mutex StateStructLock;
    VState_GetStateMsg SS;

    // genMap struct from CorridorArcEvaluator
    genMapStruct CAE_genMapStruct;

  // Arbiter voters
  Voter arbiterVoters[ArbiterInput::Count];

  Voter combinedVote;

  VDrive_CmdMotionMsg arbiter_cmd;

};


#endif
