#include "genMap.hh"
#include "frames/frames.hh"
#include "Constants.h"
#include <float.h>
#include <getopt.h>
#include <math.h>
#include "../../RaceModules/Arbiter/SimpleArbiter.hh"

VState_GetStateMsg SS;
frames ladarFrame;
genMap ladarTerMap;
// COMMAND LINE OPTIONS
char *file;

int map_rows = DEFAULT_LADARMAP_NUM_ROWS;
int map_cols = DEFAULT_LADARMAP_NUM_COLS;
double map_row_res = DEFAULT_LADARMAP_ROW_RES;
double map_col_res = DEFAULT_LADARMAP_COL_RES;

// OPTARG stuff
#define FILE_OPT      0
#define HELP_OPT      14

int main(int argc, char **argv) {
  int c, digit_optind = 0; // getopt variables
  int angle;
  double resolution,units;
  char filename[255],tmp[1000];
  char *p;
  vector< pair<double, double> > voteList;
  vector<double> steeringAngleList;
  steeringAngleList.reserve(NUMARCS);
  double phi;

  while (1) {
    int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    static struct option long_options[] = {
      {"file",             1, 0, FILE_OPT},
      {"help",             0, 0, HELP_OPT},
      {0,0,0,0}
    };

    c = getopt_long_only (argc, argv, "", long_options, &option_index);
    if(c == -1) break;

    if(c != '?') {
      switch(c) {
        case FILE_OPT:
          file = strdup(optarg);
          break;
        case HELP_OPT:
          break;
      }
    }
  }

  if((file == NULL)) {
    fprintf(stderr, "ERROR!  Must specify -file switch\n");
    exit(-1);
  }

  printf("file: %s\n", file);

  ladarTerMap.loadMATFile(file);
  ladarTerMap.initPathEvaluationCriteria(false,false,false,true,false);
  ladarTerMap.initThresholds(70,1,0,0,1,1);

// narrow_corridor.m
  SS.Easting = 403505;
  SS.Northing = 3777453;
  SS.Yaw = -M_PI/2.0 + 0.06;

  SS.Altitude = 0.0;
  SS.Pitch = 0.0;
  SS.Roll = 0.0;

  for(int i = 0; i < NUMARCS; i++) {
    phi = GetPhi(i);
    steeringAngleList.push_back(phi);
  }

  ladarTerMap.updateFrame(SS,true);
  voteList = ladarTerMap.generateArbiterVotes(NUMARCS, steeringAngleList);

  printf("vehicle (row,col): (%d,%d)\n",ladarTerMap.getVehRow(),ladarTerMap.getVehCol());

  for(int i = 0; i < NUMARCS; i++) {
    printf("%2.2lf %2.1lf %3.1lf\n", GetPhi(i), voteList[i].first, voteList[i].second);
  }

  exit(0);
}

