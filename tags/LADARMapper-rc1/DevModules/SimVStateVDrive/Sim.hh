#ifndef SIM_HH
#define SIM_HH

#define LOG_FILE_DIR "testRuns/"
#define LOG_FILE_NAME "sim_"
#define LOG_FILE_EXT ".dat"

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

// Cruise controller for simulator
#include "vehlib-2.0/cruise.h"

// Arbiter contains constants such as MIN/MAX Velocity
#include "RaceModules/Arbiter/ArbiterModule.hh"

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"
#include "vehlib/VDrive.hh"

using namespace std;
using namespace boost;

// to be shared between sim vdrive and vstate.
struct SIM_DATUM {

  boost::recursive_mutex lock;
  VState_GetStateMsg SS;
  VDrive_CmdMotionMsg cmd;

  float Steer;
  float Accel;

  Timeval lastUpdate;
  Timeval startTime;

  int ServedState;
  int CmdMotionCount;

};

class SimVDrive : public DGC_MODULE {
public:
  SimVDrive();

  void Active();
  void InMailHandler(Mail & ml);

};

class SimVState : public DGC_MODULE {
public:
  SimVState();
  
  void Init();
  void Active();
  Mail QueryMailHandler(Mail & ml);

  void SimInit();
  void SimUpdate(float deltaT);

  void SparrowDisplayLoop();
  void UpdateSparrowVariablesLoop();

private:
  // This probably shouldn't be here
  //  SIM_DATUM d;

};

#endif
