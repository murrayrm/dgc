
#ifndef __DYNAMIC_FEASIBILITY_EVALUATOR_H__
#define __DYNAMIC_FEASIBILITY_EVALUATOR_H__

void dynamic_feasibility_evaluator( const double v, 
        const double yaw, const double pitch, const double roll, 
        const double T_roll,
        const double *steering_angle, const unsigned int n_steering_angles,
        double *c_roll, double *min_v, double *max_v)
/*
         * Inputs:
         *  - v         : [m/s] forward velocity
         *
         *  - yaw, pitch, roll  : [radians] angles defining orientation of vehicle to inertial reference frame
         *                          (i.e., from these we know the slope of the terrain)
         *
         *  - steering_angle    : [radians] array of steering angles defining the arcs to be evaluated
         *
         *  - n_steering_angles : number of entries in the array
         *
         *  - T_roll    : [unitless] rollover safety threshold, bounded from 0 to 1. At a value of 1, the vehicle
         *
         *  Outputs:
         *  - c_roll    : [unitless] array of rollover coefficients for the evalutated arcs
         *                  for the current forward velocity.
         *                  c_roll = 
         *                      0 if vehicle is perfectly stable
         *                      magnitude 1 if on the verge of rolling over
         *                      magnitue > 1 if there is a rollover torque present
         *                      c_roll < 0 if leaning to the right
         *                      c_roll > 0 if leaning to the left
         *                      so c_roll < -1 => rolling rightward
         *
         * - min_v      : [m/s] array of minimum velocity for each arc such that the rollover coefficient is 
         *                  within the rollover safety threshold T_roll
         *
         * - max_v      : [m/s] array of maximum velocity for each arc such that the rollover coefficient is 
         *                  within the rollover safety threshold T_roll
*/

#endif
