using namespace std;

#include "waypoints.h"

int main(int argc, char ** argv)
{
  Waypoints wps("waypoints.gps");
  waypoint wp;
  for (wps.get_start(); wps.get_current().num < wps.get_size(); wps.get_next())
    { 
      cout << "Type = ";
      switch (wps.get_current().t )
	{
	case START:
	  cout << "START";
	  break;
	case NORMAL:
	  cout << "NORMAL";
	  break;
	case CHECKPOINT:
	  cout << "CHECKPT";
	  break;
	case END:
	  cout << "END";
	  break;
	}
      cout << "\tE = " << (long)wps.get_current().easting 
	   << "\tN = " << (long)wps.get_current().northing 
	   << "\tRadius = " << wps.get_current().radius 
	   << "\tVelocity = " << wps.get_current().max_velocity 
	   << endl;
    }



  return 0;
}
