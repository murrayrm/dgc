#include "moveCar/accel.h"

#include <time.h>
#include <stdio.h>
#include <iostream>

using namespace std;

int main() {
  float pos;
  int digi_pos;

  //  init_brake();
  init_accel();

  while(true) {
    printf("\nEnter pos: ");
    scanf("%d", &digi_pos);
    change_digipot_by(digi_pos);
  }
  
  return 0;

}

