#include <time.h>
#include <stdio.h>
#include <iostream>
#include "gps/gps.h"

/*Basic program for logging GPS data for testing purposes
revision history
 * Jeremy Gillula, jeremy@its.caltech,edu
 * 16-SEP-2003 modified code to use new valid_pvt_msg function to only
 * report valid data
 * Alan Somers, somers@its.caltech.edu
 * 13-AUG-2003 modified code to take port number as a command line argument
 * began revision history
*/
int GPS_COM;
enum choice {nothing, screen, file, both};
char* filename;

int main(int argc, char** argv) {
  //run-time help
  if ((argc < 2) || (argc > 4)){
    printf("Usage:\tgps_data_logging OPTION serial-device-number [logfile]\n");
    printf("\nGet data from the NAVCOM gps reciever and print it to the screen or a file\n");
    printf("\nUse 0 for ttyS0, 1 for ttyS1, etc.\n");
    printf("Use 32 for ttyUSB0, 33 for ttyUSB1, etc\n");
    printf("ttyS32 must be hardlinked to ttyUSB0, etc\n");
    printf("to make, use: \"ln /dev/ttyUSB0 /dev/ttyS33\"\n");
    printf("  +s\tprints log data to the screen\n");
    printf("  -s\tsuppresses screen output\n\n");
  exit(0);}

  //Some variables...
  choice mychoice;
  if (argc == 4){
    if (strcmp(argv[1] , "+s")) {
      mychoice = file;}
    else {mychoice = both;}}
  else {if (strcmp(argv[1] , "+s") ){
      mychoice = nothing;}
    else {mychoice = screen;}}
 
  GPS_COM = atoi(argv[2]);
  if (argc == 4)  filename = argv[3];

  /*cout << nothing << " " << screen << " " << file << " " << both << "\n";
  cout << argv[1] << "\t" << (argv[1] == "+s") << "\t" << argc << "\t" << mychoice << "\t" << filename << endl;
*/
  gpsDataWrapper data;
//  int choice=0;

  char *buffer_start;
  //char* filename;//[100];
  int length, msg_length;
  buffer_start=(char*)malloc(2000);
  
  if(init_gps(GPS_COM,1) == 1) {
    printf("\nGPS Initialized.");
    //Make the user choose
/*    while(choice==0) {
      printf("\nOptions:");
      printf("\n1 Logging to File Only");
      printf("\n2 Printing to Screen Only");
      printf("\n3 Both!");
      printf("\nPlease Make Your Selection: ");
      scanf("%d", &choice);
  */
      
      switch(mychoice) {
      case nothing:
        printf("\nDoing nothing\n");
        break;
      case file:
	//Logging to file only
	/*printf("\nEnter a filename: ");
	scanf("%s", filename);*/
	printf("\nlogging data to ", filename, "\nHit Ctrl+C to Quit");
	fflush(stdout);
	while(true) {
	  //Get the gps message...
	  get_gps_msg(buffer_start, 1000);
	  //If it's a PVT message...
	  if(get_gps_msg_type(buffer_start)==GPS_MSG_PVT) {
	      //Update the data based on it
	      data.update_gps_data(buffer_start);
	      //And write it to filename
	      data.write_to_file(filename);
	  }
	}
	break;
      case screen:
        //Printing to screen only
	printf("\noutputting data to the screen");
        while(true) {
	  //Get the gps message...
	  get_gps_msg(buffer_start, 1000);
	  //If it's a PVT message...
	  if(get_gps_msg_type(buffer_start)==GPS_MSG_PVT) {
	    //Update the data based on it
	    data.update_gps_data(buffer_start);
	    //And print it to the screen
	    data.print_to_screen();
	    if(valid_pvt_msg(buffer_start)) {
	      printf("\nData valid.");
	    } else {
	      printf("\nInvalid data.");
	    }
	    printf("\nHit Ctrl+C to Quit");
	  }
	}
	break;
      case both:
	//Both
	/*printf("\nEnter a filename: ");
	scanf("%s", filename);*/
        printf("\nprinting to the screen and to ", filename);
	while(true) {
	  //Get the gps message...
	  get_gps_msg(buffer_start, 1000);
	  //If it's a PVT message...
	  if(get_gps_msg_type(buffer_start)==GPS_MSG_PVT) {
	    //Update the data based on it
	    data.update_gps_data(buffer_start);
	    //And write it to filename
	    data.write_to_file(filename);
	    //And print it to the screen
	    data.print_to_screen();
	    if(valid_pvt_msg(buffer_start)) {
	      printf("\nData valid.");
	    } else {
	      printf("\nInvalid data.");
	    }
	    printf("\nHit Ctrl+C to Quit");
	  }
	}		    
	break;
      /*default:
	printf("\nPlease select 1-3");
	choice=0;
      }*/
    }
  } else {
    printf("\nGPS NOT initialized.\n");
  }
  
  return 0;
}
