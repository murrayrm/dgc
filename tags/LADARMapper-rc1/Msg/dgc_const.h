#ifndef dgc_const_H
#define dgc_const_H


#define TRUE 1
#define FALSE 0
#define ERROR -1
#define NOT_FOUND -1

#define MTA_LOCAL_PORT 13131
#define MTA_NUM_LOCAL_PORTS 1

#define MTA_REMOTE_PORT 13125
#define MTA_NUM_REMOTE_PORTS 1

#define str_localhost "127.0.0.1"

#endif
