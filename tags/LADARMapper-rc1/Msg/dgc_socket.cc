#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <sys/time.h>

#include <fstream>
#include <unistd.h>
#include <fcntl.h>

#include "dgc_const.h"
#include "dgc_socket.h"
#include "dgc_time.h"



using namespace std;

int dgc_OpenCSocket(int portToConnectTo)
{
  //  cout << "Opening Client Socket, port " << portToConnectTo << endl;

  sockaddr_in ctrl_sa;
  socklen_t   ctrl_sl;
 
  int ctrl_sd = socket(PF_INET, SOCK_STREAM, 0);
  if (ctrl_sd < 0) {
    cout << "  OpenCSocket: Error - Creating Socket" << endl;
    return ERROR;
  } 

  memset(&ctrl_sa, 0, sizeof(ctrl_sa));

  ctrl_sa.sin_family = AF_INET;
  ctrl_sa.sin_port = htons(portToConnectTo);

  /*  if(!(inet_aton(str_localhost, &ctrl_sa.sin_addr))) {
    cout << "  OpenCSocket: Error - Inet Translation" << endl;
    return ERROR;
   }
  */ 

  if(!(inet_aton("0", &ctrl_sa.sin_addr))) {
    cout << "  OpenCSocket: Error - Inet Translation" << endl;
    dgc_CloseSocket(ctrl_sd);
    return ERROR;
  }

  ctrl_sl = sizeof(ctrl_sa);
  if( connect(ctrl_sd, (sockaddr *)&ctrl_sa, ctrl_sl) ) {
    cout << "  OpenCSocket: Error - Connecting Socket" << endl;
    perror("connect");
    dgc_CloseSocket(ctrl_sd);
    return ERROR;
  }

  return ctrl_sd;
}




int dgc_OpenSSocket(int portToBindTo)
{
  //  cout << "Opening Server Socket" << endl;

  int sockfd;
  sockaddr_in srv;
  socklen_t   socklen;
  //  int i=1;

  sockfd = socket(PF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
    cout << "  OpenSSocket: Error - Creating Socket" << endl;
    return ERROR;
  } 

  //  setsockopt(sockfd, SOL_SOCKET, 0, &i, sizeof(i));
  memset(&srv, 0, sizeof(srv));

  srv.sin_family = AF_INET;
  srv.sin_port = htons(portToBindTo);
  socklen = sizeof(srv);

  if((bind(sockfd, ((sockaddr *) &srv), socklen))) {
    cout << "  OpenSSocket: Error - Bind" << endl;
    return ERROR;
  }

  if(listen(sockfd, 5) < 0) {
    cout << "  OpenSSocket: Error - Listen" << endl;
    return ERROR;
  }

  return sockfd;
}

int dgc_AcceptSSocket(int sockfd, sockaddr & sa, socklen_t & socklen) {

  //  cout << "dgc_AcceptSSocket: waiting for connection, FD=" << sockfd << endl;
  int ret = accept(sockfd, &sa, &socklen);
  if( ret >= 0) {

    //cout << "dgc_AcceptSSocket: socket connected, FD=" << ret << endl; 
    return ret;
  }
  else {
    perror("accept");
    return ERROR;
  }
}



int dgc_CloseSocket(int fd) {
  if( fd >= 0) {
    if ( close(fd) == 0) return TRUE;
    else return ERROR;
  }
  return ERROR;
}









int dgc_ReadSocket(int fd, void * buffer, int sz, int timeout_ms )
{
  double timeout_seconds = ((double) timeout_ms) / 1000.0;
  timeval before, after;
  gettimeofday( &before, NULL );
  //  cout << "dgc_ReadSocket: Trying to read " << sz << " bytes." << endl;
  //  cout << "                into buffer " << buffer << endl;
  if (fd >= 0) {
    int bytesRead=0;
    int nb;

    while(bytesRead < sz) {
    
      nb = recv(fd , buffer, sz-bytesRead, 0);
      //      cout << "                Read " << nb << " bytes." << endl;

      if (nb < 0) {
	cout << "dgc_ReadSocket: nb < 0" << endl;
	perror("recv");
	return ERROR;
      }
      else if (nb == 0) {
	usleep(100);
	if ( timeout_seconds != 0 ) { 
	  gettimeofday( &after, NULL);
	  double t = timeval_subtract(after, before);
	  //cout << "dgc_ReadSocket: t= " << t * 1000000.0 << endl;
	  if ( t > timeout_seconds ) { // timeout message if no update in timeout milliseconds
	    	    cout << "dgc_ReadSocket: Timeout - " << t * 1000000.0 << " microseconds." << endl;
	    return ERROR;
	  }
	}
      } else {
	/*	gettimeofday( &after, NULL);
	double t = timeval_subtract(after, before);
	cout << "dgc_ReadSocket: t = " << t << endl;
	*/

	gettimeofday( &before, NULL );
	bytesRead += nb;
	buffer     = (void *) ( ((char *) buffer) + nb);
	//	cout << "dgc_ReadSocket: bytesRead = " << bytesRead << endl;
	//	cout << "                buffer = " << buffer << endl; 
	//      cout << endl;
      }
    }
    return TRUE;
  }
  return ERROR;
}


int dgc_WriteSocket(int fd, void * buffer, int sz, int timeout) {

  if (fd >= 0) {

    int nb = send(fd , buffer, sz, 0);
    if (nb <= 0) return ERROR; // error writing file
    int bytesWritten = nb;
    buffer     = (void *) ( ((char *) buffer) + nb);


    while(bytesWritten < sz) {
      usleep(3);
      nb = send(fd , buffer, sz-bytesWritten, 0);
      if (nb <= 0) return ERROR; // error writing file
      bytesWritten += nb;
      buffer     = (void *) ( ((char *) buffer) + nb);
    }
    //    cout << "dgc_WriteSocket: Wrote " << sz << " bytes." << endl;
    return TRUE;
  }
  return ERROR;
}
