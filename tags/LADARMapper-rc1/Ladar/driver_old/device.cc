/*********************************************************************************/
/* SICK LASER DRIVER V0.9                                                        */
/* Copyright (C) 2000 - Kasper Stoy - USC Robotics Labs (See README for details) */
/*********************************************************************************/
#include<pthread.h>
#include<stdio.h>

#include"device.h"

CDevice::CDevice() {
  /* the mutex's are defined to be default type (NULL) which is fast */
  pthread_mutex_init( &dataAccessMutex, NULL );
  pthread_mutex_init( &setupMutex, NULL );

  /* lock setup mutex */
  pthread_mutex_lock( &setupMutex );
  firstdata = true;
}

CDevice::~CDevice() {
  pthread_mutex_destroy( &dataAccessMutex );
  pthread_mutex_destroy( &setupMutex );
}

int CDevice::LockNShutdown() {
  if ( firstdata == false ) {
    // this means that the setup went okay and the call to lock will not be blocked
    pthread_mutex_lock( &setupMutex );
  }
  firstdata = true;
  return( Shutdown() );
}

void CDevice::LockNGetData( unsigned char *dest ) {
  pthread_mutex_lock( &setupMutex );
  pthread_mutex_lock( &dataAccessMutex );
  GetData(dest);
  pthread_mutex_unlock( &dataAccessMutex );
  pthread_mutex_unlock( &setupMutex );
}

void CDevice::LockNGetData() {
  //      printf("in CDevice: lockngetdata/n");
  pthread_mutex_lock( &setupMutex );
  pthread_mutex_lock( &dataAccessMutex );
  GetData();
  pthread_mutex_unlock( &dataAccessMutex );
  pthread_mutex_unlock( &setupMutex );
}

void CDevice::LockNPutData( unsigned char *dest ) {
  pthread_mutex_lock( &dataAccessMutex );
  PutData(dest);
  pthread_mutex_unlock( &dataAccessMutex );
  if (firstdata) pthread_mutex_unlock( &setupMutex );
  firstdata=false;
}
