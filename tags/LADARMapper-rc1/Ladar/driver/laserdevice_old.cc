/*********************************************************************************/
/* SICK LASER DRIVER V0.9                                                        */
/* Copyright (C) 2000 - Kasper Stoy - USC Robotics Labs (See README for details) */
/*********************************************************************************/
#include <fcntl.h>
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
#include <unistd.h>
#include <sys/times.h>

#include "laserdevice_old.h"
#define MKSHORT(a,b) ((unsigned short)(a)|((unsigned short)(b)<<8))

#define CRC16_GEN_POL 0x8005

const unsigned char startString[] = { 0x02, 0x80, 0xd6, 0x02, 0xb0, 0x69, 0x01 };
const unsigned char ACK[] = { 0x02, 0x80, 0x03, 0x00, 0xa0 };
const unsigned char NACK[] = { 0x02, 0x80, 0x03, 0x00, 0x92 };
const unsigned char STX = 0x02;

CLaserDevice::CLaserDevice(void) {
  puts("CLaserDevice Constructor called.");
  debuglevel=0;
  bzero(data, 1024 );
  puts("SICK LASER DRIVER V0.9, COPYRIGHT (C) 2000");
  puts("KASPER STOY - USC ROBOTICS LABS");
}

unsigned short CLaserDevice::CreateCRC( unsigned char* commData, unsigned int uLen )
{
  unsigned short uCrc16;
  unsigned char abData[2];
  
  uCrc16 = 0;
  abData[0] = 0;
  
  while(uLen-- ) {
    abData[1] = abData[0];
    abData[0] = *commData++;
    
    if( uCrc16 & 0x8000 ) {
      uCrc16 = (uCrc16 & 0x7fff) << 1;
      uCrc16 ^= CRC16_GEN_POL;
    }
    else {
      uCrc16 <<= 1;
    }
    uCrc16 ^= MKSHORT (abData[0],abData[1]);
  }
  return( uCrc16); 
}

ssize_t CLaserDevice::WriteToLaser( unsigned char *data, ssize_t len ) {
  unsigned char *datagram;
  unsigned short crc;
  ssize_t sz;

  datagram = new unsigned char[len+6];

  datagram[0] = STX; /* start byte */
  datagram[1] = 0x00; /* LMS address */
  datagram[2] = 0x0F & len; /* LSB - number of bytes to follow excluding crc */
  datagram[3] = 0xF0 & len; /* MSB - number of bytes to follow excluding crc */

  memcpy( (void *) &datagram[4], (void *) data, len );

  /* insert CRC */
  len += 4;
  crc = CreateCRC( datagram, len );
  datagram[len] = crc & 0x00FF;
  datagram[len+1] = (crc & 0xFF00) >> 8;
  len += 2;

  if (debuglevel>1) {
    printf("\nSending: ");
    for(int i=0; i<len; i++) {
      printf("%.2xh/", datagram[i]);
    }
    printf("\n");
  }

  sz = write( laser_fd, datagram, len);

  delete datagram;
  return (sz);
}

int CLaserDevice::isError(int status) {
  if((status & 0x00FF) != 0x0010) return true;
  if( (status >> 6) & 0x01 ) return true;
  if( (status >> 7) & 0x01 ) return true;
  return false;
}

void CLaserDevice::DecodeStatusByte( unsigned char byte ) {
  unsigned short code;

  /* print laser status */
  fprintf(stderr,"Laser Staus: ");
  code = byte & 0x07;
  switch(code) {
  case 0:
    fprintf(stderr,"no error ");
    break;
  case 1:
    fprintf(stderr,"info ");
    break;
  case 2:
    fprintf(stderr,"warning ");
    break;
  case 3:
    fprintf(stderr,"error ");
    break;
  case 4:
    fprintf(stderr,"fatal error ");
    break;
  default:
    fprintf(stderr,"unknown code ");
    break;
  }
 
  code = (byte >> 3) & 0x03;
  switch(code) {
  case 0:
    fprintf(stderr,"LMS -xx1 to -xx4 ");
    break;
  case 1:
    fprintf(stderr,"LMI ");
    break;
  case 2:
    fprintf(stderr,"LMS -xx6 ");
    break;
  case 3:
    fprintf(stderr,"reserved ");
    break;
  default:
    fprintf(stderr,"unknown device ");
    break;
  }

  fprintf(stderr,"restart:%d ", (byte >> 5) & 0x01);

  if ( ( byte >> 6 ) & 0x01 ) 
    fprintf(stderr,"Implausible measured value ");

  if ( ( byte >> 7 ) & 0x01 ) 
    fprintf(stderr,"Pollution ");

  fprintf(stderr,"\n");
}

bool CLaserDevice::CheckDatagram( unsigned char *datagram ) {
  unsigned short crc;

  if (debuglevel>1) {
    printf("\nReceived: ");
    for(int i=0; i<9; i++) {
      printf("%.2xh/", datagram[i]); fflush(stdout);
    }
    printf("\n");fflush(stdout);
  }

  /* check CRC */
  crc = CreateCRC( &datagram[0], 7 );
  if ( datagram[7] != (crc & 0x00FF) || datagram[8] != ((crc & 0xFF00) >> 8) ) {
    if (debuglevel>1) {
      printf("\ncrc incorrect: expected 0x%.2x 0x%.2x got 0x%.2x 0x%.2x\n",
	     ((crc & 0xFF00) >> 8), (crc & 0x00FF), datagram[7], datagram[8] );
    }
    return(false);
  }

  /* print status information */
  if (debuglevel>1) {
    DecodeStatusByte( datagram[6] );
  }

  /* decode message */
  if (strncmp( (const char *) datagram, (const char *) ACK, 5 ) == 0 ) {
    switch( datagram[5] ) {
    case 0x00: 
      puts("ok");
      break;
    case 0x01:
      puts("request denied - incorrect password");
      break;
    case 0x02:
      puts("request denied - LMI fault");
      break;
    }
  }
  else if (strncmp( (const char *) datagram, (const char *) NACK, 5 ) == 0 ) {
    puts("not acknowledged");
  }

  return(true);
} 

ssize_t CLaserDevice::RecieveAck() {
  unsigned char datagram[9];
  ssize_t sz;

  /* laser sends acknowledge within 60ms therefore 
     with 70ms we are on the safe side */
  usleep(700000);
  
  while(1) {
    if ( (sz = read( laser_fd, &datagram[8], 1 )) < 0 ) {
      puts("no acknowledge received");
      return(sz);
    }
    
    if (datagram[0]==STX && CheckDatagram( datagram ) )
      break;
    else
      for(int i=0;i<8;i++) datagram[i]=datagram[i+1];
  }

  return (sz);
}


int CLaserDevice::ChangeMode(  )
{ 
  ssize_t len;
  unsigned char request[20];

  request[0] = 0x20; /* mode change command */
  request[1] = 0x00; /* configuration mode */
  request[2] = 0x53; // S - the password 
  request[3] = 0x49; // I
  request[4] = 0x43; // C
  request[5] = 0x4B; // K
  request[6] = 0x5F; // _
  request[7] = 0x4C; // L
  request[8] = 0x4D; // M
  request[9] = 0x53; // S

  len = 10;
  
  printf("Sending configuration mode request to laser.."); fflush(stdout);
  if ( (len = WriteToLaser( request, len)) < 0 ) {
    perror("ChangeMode");
    return(1);
  }

  if ( ( len = RecieveAck() ) < 0 ) {
    perror("ChangeMode");
    return(1);
  }

  return 0;
}

int CLaserDevice::Request38k()
{ 
  ssize_t len;
  unsigned char request[20];

  request[0] = 0x20; /* mode change command */
  request[1] = 0x40; /* 38k */

  len = 2;
  
  printf("Sending 38k request to laser.."); fflush(stdout);
  if ( (len = WriteToLaser( request, len)) < 0 ) {
    perror("Request38k");
    return(1);
  }

  if ( ( len = RecieveAck() ) < 0 ) {
    perror("Request38k");
    return(1);
  }

  return 0;
}

int CLaserDevice::RequestData()
{ 
  ssize_t len;
  unsigned char request[20];

  request[0] = 0x20; /* mode change command */
  request[1] = 0x24; /* request data */

  len = 2;
  
  printf("Sending data request to laser.."); fflush(stdout);
  if ( (len = WriteToLaser( request, len)) < 0 ) {
    perror("RequestData");
    return(1);
  }

  if ( ( len = RecieveAck() ) < 0 ) {
    perror("RequestData");
    return(1);
  }

  return 0;
}

int CLaserDevice::Setup(char *port) {
  struct termios term;

  printf("Attempting to open port %s\n", port);
  strcpy( LASER_SERIAL_PORT, port );

  if( (laser_fd = open( LASER_SERIAL_PORT, O_RDWR | O_SYNC | O_NONBLOCK , S_IRUSR | S_IWUSR )) < 0 ) {
    perror("CLaserDevice:Setup");
    return(1);
  }  
 
  // set the serial port speed to 9600 to match the laser
  // later we can ramp the speed up to the SICK's 38K
  if( tcgetattr( laser_fd, &term ) < 0 )
    printf( "get attributes error\n" );
  
  cfmakeraw( &term );
  cfsetispeed( &term, B9600 );
  cfsetospeed( &term, B9600 );
  
  if( tcsetattr( laser_fd, TCSAFLUSH, &term ) < 0 )
    printf( "set attributes error\n" );

  if( ChangeMode() != 0 ) {
    puts("Change mode failed most likely because the laser");
    puts("is running 38K..switching terminal to 38k");
  }
  else if( Request38k() != 0 ) {
    puts("CLaserDevice:Setup: Couldn't change laser speed to 38k");
    return( 1 );
  }

  // set serial port speed to 38k
  if( tcgetattr( laser_fd, &term ) < 0 )
    printf( "get attributes error\n" );
  cfmakeraw( &term );
  cfsetispeed( &term, B38400 );
  cfsetospeed( &term, B38400 );
  if( tcsetattr( laser_fd, TCSAFLUSH, &term ) < 0 )
    printf( "set attributes error\n" );

  if( RequestData() != 0 ) {
    printf("Couldn't request data most likely because the laser\nis not connected or if connected not to %s\n", LASER_SERIAL_PORT);
    return( 1 );
  }

  puts("Laser ready");
  fflush(stdout);
  /* success: start the "Run" thread and report success */

  close(laser_fd);

  // 1/4/03 this function is causing a segfault in our code! 
  // (it overwrites the DATUM d of WaypointNav.hh) -Lars
  // crc incorrect: expected 0x05 0x6f got 0x39 0x0f - ignoring scan
  // crc incorrect: expected 0x3d 0x0d got 0x0b 0xf1 - ignoring scan
  Run();

  return(0);

} // end CLaserDevice::Setup()

int CLaserDevice::Shutdown() {
  /* shutdown laser device */
  close(laser_fd);
  pthread_cancel( thread );
  puts("Laser has been shutdown");

  return(0);
}

void *RunThread( void *laserdevice ) {
  unsigned char data[1024];
  int n,c;
  int bytes = 0;
  unsigned short crc;
  int status = 0;

  CLaserDevice *ld = (CLaserDevice *) laserdevice;

  pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, NULL);
  memset( data, 0, 7 );

  if( (ld->laser_fd = open( ld->LASER_SERIAL_PORT, O_RDWR | O_SYNC , S_IRUSR | S_IWUSR )) < 0 ) {
    perror("CLaserDevice:RunThread");
    pthread_exit(0);
  }  

  while(1) {
    /* test if we are supposed to cancel */
    pthread_testcancel();

    /* get laser scans one at a time */

    // read the stream of bytes one by one until we see the message header

    // move the first 7 bytes in the data buffer one place left
    for( n=0; n<6; n++ ) {
      data[n] = data[n+1];
    }

    bytes += read( ld->laser_fd, &data[6], 1 );

    if( strncmp( (char *) data, (char *) startString, 7 ) == 0 ) // ok we've got the start of the data 
      {
	// now read the measured values (361*2 bytes) plus status (1 byte) and crc (2 bytes)
	bytes = 0;
	while(bytes<725) {
	  bytes += read( ld->laser_fd, &data[bytes+7], 725-bytes );
	}

	crc = ld->CreateCRC( data, 730 );

	if ( data[730] != (crc & 0x00FF) || data[731] != ((crc & 0xFF00) >> 8) ) {
	  printf("crc incorrect: expected 0x%.2x 0x%.2x got 0x%.2x 0x%.2x - ignoring scan\n",
		 ((crc & 0xFF00) >> 8), (crc & 0x00FF), data[730], data[731] );
          status |= STATUS_CRC_ERROR;
//	  continue;
	}

	for( c=7; c<729; c+=2 ) {
	  // check to see if laser is dazzled
	  if ( (data[c+1] & 0x20) == 0x20 ) {
	    puts("Laser dazzled - ignoring scan");
            status |= STATUS_DAZZLED;
//	    continue;
	  }

	  // mask b to strip off the status bits 13-15
	  data[c+1] &= 0x1F;     

// FIXME: Check for invalid values here

	}
	/* test if we are supported to cancel */
	pthread_testcancel();

        status |= data[729]; // Status byte returned by scanner

	ld->LockNPutData( data, status );
      }
  }
}

void CLaserDevice::Run() {

  pthread_attr_t attr;
  
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
  pthread_create( &thread, &attr, &RunThread, this );
}

int CLaserDevice::GetData( unsigned char *dest ) {
  memcpy( dest, data, 723 );  
  return data_status;
}

int CLaserDevice::GetData() {
  // memcpy( dest, data, 723 );
  int j= 0;
  for(int i=1;i<723;i+=2) {
    //      cout <<"ingetdata "<< i << " " << j << endl;
    Scan[j] =(unsigned short int)data[i] +
      ((unsigned short int)data[i+1] << 8) ;
    //cout <<"ingetdata "<< Scan[j]<< endl;
    j++;
  }
  return data_status;
}

int CLaserDevice::GetPoint()
{
  int point = Scan[Index];
  Index++;
  if (Index >360)
    Index = 0;
  return (point);

}

void CLaserDevice::PutData( unsigned char *src, int status ) {
  data[0]='l';
  memcpy( &data[1], &src[7], 722 );
  data_status = status;
}


CDevice::CDevice() {
  /* the mutex's are defined to be default type (NULL) which is fast */
  pthread_mutex_init( &dataAccessMutex, NULL );
  pthread_mutex_init( &setupMutex, NULL );

  /* lock setup mutex */
  pthread_mutex_lock( &setupMutex );
  firstdata = true;
}

CDevice::~CDevice() {
  pthread_mutex_destroy( &dataAccessMutex );
  pthread_mutex_destroy( &setupMutex );
}

int CDevice::LockNShutdown() {
  if ( firstdata == false ) {
    // this means that the setup went okay and the call to lock will not be blocked
    pthread_mutex_lock( &setupMutex );
  }
  firstdata = true;
  return( Shutdown() );
}

int CDevice::LockNGetData( unsigned char *dest ) {
  int status = 0;
  pthread_mutex_lock( &setupMutex );
  pthread_mutex_lock( &dataAccessMutex );
  status = GetData(dest);
  pthread_mutex_unlock( &dataAccessMutex );
  pthread_mutex_unlock( &setupMutex );
  return status;
}

int CDevice::LockNGetData() {
  int status = 0;
  pthread_mutex_lock( &setupMutex );
  pthread_mutex_lock( &dataAccessMutex );
  status = GetData();
  pthread_mutex_unlock( &dataAccessMutex );
  pthread_mutex_unlock( &setupMutex );
  return status;
}

void CDevice::LockNPutData( unsigned char *dest, int status ) {
  pthread_mutex_lock( &dataAccessMutex );
  PutData(dest,status);
  pthread_mutex_unlock( &dataAccessMutex );
  if (firstdata) pthread_mutex_unlock( &setupMutex );
  firstdata=false;
}
