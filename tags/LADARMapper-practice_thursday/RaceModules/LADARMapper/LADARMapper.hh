#ifndef LADARPLANNER_HH
#define LADARPLANNER_HH


#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "LADARMapperDatum.hh"


using namespace std;
using namespace boost;


class LADARMapper : public DGC_MODULE {
public:
  // and change the constructors
  LADARMapper();
  ~LADARMapper();

  // The states in the state machine.
  // Uncomment these if you wish to define these functions.

  void Init();
  void Active();
  //  void Standby();
  void Shutdown();
  //  void Restart();

  // The message handlers. 
  //  void InMailHandler(Mail & ml);
  //  Mail QueryMailHandler(Mail & ml);

  // the status function.
  //string Status();


  // Any functions which run separate threads
  
  // Method protocols
  int  InitLadar();
  void LadarUpdateVotes();
  void LadarShutdown();

  void UpdateState();

  void SparrowDisplayLoop();
  void UpdateSparrowVariablesLoop();

  void SaveGenMap();
  void ClearGenMap();

private:
  // and a datum named d.
  LADARMapperDatum d;
};



// enumerate all module messages that could be received
namespace WaypointNavMessages {
  enum {
    // we are not yet receiving messages.
    // A place holder
    NumMessages
  };
};






#endif
