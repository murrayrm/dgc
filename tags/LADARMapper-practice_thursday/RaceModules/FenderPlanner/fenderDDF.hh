/* 
   fenderDDF.hh - class to read the fenderpoint rddf-like file and return the fenderpoints
   it's a lot like rddf.hh, actually...
   created by Rocky Velez, March 3, 2004
*/

#ifndef _FENDERDDF_HH
#define _FENDERDDF_HH

#include <stdio.h>
#include <string.h>
#include <iostream.h>
#include <fstream.h>
#include <math.h>
#include <vector>
#include "Constants.h"
#include "ggis.h"
#include "staticFender2rddf.hh"
#include "global_functions.hh"

#define SEGMENTDATA_DELIMITER ' '
#define FENDERPT_DELIMITER ','
#define SEGMENT_DELIMITER '\n'

using namespace std;

typedef struct fenderpoint {
  double FPnorthing;  // from Fender Data
  double FPeasting;   // from Fender Data

  double DistToWP;    // meters, from fender point
  double Speed;       // m/s, optimal speed at which to travel through fenderpoint

 };

//fenderpoint* fenderpointVector;  // stores information about each fenderpoint

// fenderpoints
typedef struct segmentData {
  int segmentNumber;
  int numFenderpoints;
  double WPnorthing;
  double WPeasting;  
  double WPradius;
  fenderpoint* fplist;

};

//segmentData* segmentDataVector;  
// stores information about each segment and the fenderpoints 
// located in that segment

class fenderDDF {
public:
 // Constructors
  fenderDDF();
  fenderDDF(char *pFileName);

  // Destructor
  ~fenderDDF();

  // Accessors
  Vector findCentroid(int segmentNum, Vector pos);
  /* fenderpointsFOV
   * this determines whether there are any fenderpoints in front of the
   * vehicle, and within the current segment
   */

  bool fenderpointsFOV(int segmentNum, Vector pos);

  int getNumSegments();

private:
  //bool fenderpointsInSegment(int segmentNum);
  int readFDDF(char *); // reads the fenderDDF
  double distBOB2WP(const double &WPnorthing, const double &WPeasting, const Vector &pos);

  int numWP;
  int numSegments;          //num segments, NOT waypoints
  segmentData* segmentList;  //formerly segmentFPdata
  
  // array of all the segments and the fenderpoints within each
  // segment
};




#endif
