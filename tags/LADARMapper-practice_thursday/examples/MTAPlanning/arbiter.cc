/*
 * arbiter.c - simple shell for MTA-capable arbiter module
 * RMM, 10 Jan 04
 *
 * This program accepts initiatlization messages from voter sources and
 * then receives the votes themselves.  It illustrates how the arbiter
 * can be implemented in MTA.
 *
 */

/* Header files required for MTA; assumes include path is set up */
#include "MTA/Modules.hh"
#include "MTA/Kernel.hh"
#include <boost/shared_ptr.hpp>

/* Some include files for sparrow, used for debug output */
#include "sparrow/dbglib.h"

/* This is the module definition for the modules we need to talk to */
#include "arbiterMTA.hh"

using namespace std;
using namespace boost;

class arbiterMTA : public DGC_MODULE {
public:
  arbiterMTA();
  ~arbiterMTA();

  void Active();
  void InMailHandler(Mail & ml);
  Mail QueryMailHandler(Mail & ml);

private:
  // put data structure here
};

/* Declare some global structures to hold vote data */
double ladar_votes[2];		// LADAR votes
double stereo_votes[2];		// Stereo votes

int main(int argc, char **argv) {
  /* Set up sparrow to print out all debugging statements */
  dbg_flag = 1;				/* enable debugging */
  dbg_all = 1;				/* turn on all modules */
  dbg_outf = 1;				/* print messages to screen */

  /* Register this module in MTA */
  dbg_info("registering arbiterMTA module");
  Register(shared_ptr<DGC_MODULE>( new arbiterMTA ));

  /* Start the MTA kernel */
  dbg_info("stating the MTA kernel");
  StartKernel();

  return 0;
}

/*
 * MTA MODULE support
 *
 * These are the handling routines for the arbiterMTA data type.
 *
 */

using namespace std;
using namespace boost;

/* Constructor and destructor routines */
arbiterMTA::arbiterMTA() : DGC_MODULE(MODULES::arbiterMTA, "Arbiter", 0) {}
arbiterMTA::~arbiterMTA() {}

/* Active state handler */
void arbiterMTA::Active() {
  while( ContinueInState() ) {  // while stay active

    /* Print out the current votes that we have */
    printf("LADAR votes: %g, %g\n", ladar_votes[0], ladar_votes[1]);
    printf("Stereo votes: %g, %g\n", stereo_votes[0], stereo_votes[0]);

    sleep(5);			// sleep for a while
  }
} 

/* Handler for receiving messages */
void arbiterMTA::InMailHandler(Mail& msg) {
  struct arbiterMTA_NewVotesMsg newdata;	// new vote data

  switch (msg.MsgType()) {
  case arbiterMessages::InitVoter:
    dbg_error("recieved InitVoter as OutMail");
    break;

  case arbiterMessages::NewVotes:
    msg.DequeueFrom(&newdata, sizeof(newdata));
    dbg_info("received new votes; voter = %c, seq = %d, vote = %g, %g", 
	     newdata.voter, newdata.seqnum,
	     newdata.votes[0], newdata.votes[1]);

    /* Now copy the data into place based on voter type */
    /* Should use process locks here since this is multi-threaded */
    switch (newdata.voter) {
      int i;

    case 'L':	
      for (i = 0; i < 2; ++i) ladar_votes[i] = newdata.votes[i];
      break;

    case 'S':
      for (i = 0; i < 2; ++i) stereo_votes[i] = newdata.votes[i];
      break;
    }
    break;

  default:
    // let the parent mail handler handle it
    break;
  }
}

/* Handler for responding to messages */
Mail arbiterMTA::QueryMailHandler(Mail& msg) {
  Mail reply;					// reply message
  struct arbiterMTA_InitVoterMsg newvoter;	// new voter data

  switch (msg.MsgType()) {
  case arbiterMessages::InitVoter:
    reply = ReplyToQuery(msg);		// create reply message

    msg.DequeueFrom(&newvoter, sizeof(newvoter));
    dbg_info("received voter initialization; voter = %c", newvoter.voter);

    /* Send back the angles we want votes for (dummy data) */
    newvoter.angles[0] = newvoter.voter + 0.5;
    newvoter.angles[1] = 0;
    reply.QueueTo(&newvoter, sizeof(newvoter));

    break;

  case arbiterMessages::NewVotes:
    dbg_error("recieved NewVotes as QueryMail");
    break;

  default:
    // let the parent mail handler handle it.
    reply = DGC_MODULE::QueryMailHandler(msg);
    break;
  }

  return reply;
}
