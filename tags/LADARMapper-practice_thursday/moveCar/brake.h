#ifndef BRAKE_H
#define BRAKE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include "../serial/serial.h"
#include "../include/Constants.h"

//Brake Constants
#define BRAKE_ID 16
#define BRAKEBAUD B57600
// TODO: THIS IS SLIGHTLY DIRTY.  but we want to keep all constants,
// etc in Constants.h
#define BRAKEPORT BRAKE_SERIAL_PORT
// serial port defined in include/Constants.h

//Brake Message Constants
#define BRAKE_ACK "* 10\n"

//Brake Movement Constant Values
#define STOP_ENABLE_ABS 0
#define STOP_STATE_ABS 0
#define MAX_ACC 1073741.0
#define MIN_ACC 1.0
#define MAX_VEL 2147483647.0
#define MIN_VEL 1.0
#define ZERO_POS -120000
#define MAX_POS -230000
#define MIN_POS 0
#define MAX_MOV_TEN 13440   //4.2"/sec * 32000 steps/in / 10Hz =max movement in 10Hz control loop

/* send_brake_command()
 * Description: Sends the brake a command of command_num with parameters of
 *  length
 */
int send_brake_command(int command_num, char *parameters, int length);

/* receive_brake_command()
 * Return value: The response from sending a brake command
 * Description: Sends the brake a command of command_num with parameters of
 *  length, and then stores the response in buffer of length buf_length (or
 *  times out in timeout seconds)
 */
int receive_brake_command(int command_num, char *parameters, int length,
			  char *buffer, int buf_length, int timeout);

/* brake_ready()
 * Return values: 0, if the brake is ready to receive a command
 *                -1, otherwise
 * Description: Sends a polling command to see if the brake is ready
 *  after a command has been sent. Also handles holding/moving error.
 */
int brake_ready();

/* Init_brake()
 * Initializes the brake by simply initializing its serial port. If wish to
 *  use the brake pot or force meter, must call initBrakeSensors() in TLL.c.
 */
int init_brake();

/* reset_brake()
 * Description: Resets the brake by sending the reset command
 *  and then waits until the brake is back up and running again
 *  before returning.
 * Note: THIS IS A BLOCKING FUNCTION.
 */
int reset_brake();

/* calib_brake()
 * Description: User can calibrates the brake using this function. It prompts a
 *  few options so the user can keep moving the brake actuator, set the zero
 *  position or set the full position. We use set_brake_rel_pos in calib_brake,
 *  so we do not need the brake pot.
 * Notes: "zero position" = full-out?
 *        "full position" = fully retracted?
 */
int calib_brake();

/* set_brake_abs_pos()
 * Inputs: abs_pos   0 = calibrated zero (no braking)
 *                   1 = fully retracted (full braking)
 *         vel, acc  0 to 1
 * Description: Sets the brake actuator, taking the first argument as the
 *  desired position in the absolute scale. The absolute scale is defined by 
 *  constants set in calib_brake().
*/
int set_brake_abs_pos(double abs_pos, double vel, double acc);

/* set_brake_abs_pos_limited()
 * this function is identical to set_brake_abs_pos(), except that 
 * it will never move more than MAX_MOV_TEN, to insure that it will return
 * quickly, making it suitable for a 10hz control loop.
 * an integrator is used to compensate for the limiting, though i may
 * eliminate the integrator in a few hours.*/
int set_brake_abs_pos_limited(double abs_pos, double vel, double acc);

/* set_brake_abs_pos_pot()
 * Inputs: abs_pos   0 = calibrated zero (no braking)
 *                   1 = fully retracted (full braking)
 *         vel, acc  0 to 1
 * Description: Sets the brake actuator, taking the first argument as the
 *  desired position in the absolute scale, defined by constants set in 
 *  calib_brake(). 'abs_pos' is on the 0-1 scale, and goes from no braking to
 *  full braking (It does NOT go from actuator fully out to fully in, unless
 *  that's how the actuator was calibrated.)
 */

int set_brake_abs_pos_pot(double abs_pos, double vel, double acc);

/* set_brake_abs_pos_pot_limited()
 * this function is identical to set_brake_abs_pos_pot, except that it is
 * limited in the same way as set_brake_abs_pos_limited.
 * no integrator is used*/
int set_brake_abs_pos_pot_limited(double abs_pos, double vel, double acc);

/* set_brake_rel_pos()
 * Arguments: rel_pos  -1 to 1 (negative to move out)
 *            vel, acc  0 to 1
 * Description: Sets the brake actuator, taking the first argument as the
 *  desired displacement from the current position. 
 */
int set_brake_rel_pos(double rel_pos, double vel, double acc);

/* set_brake_rel_pos_counts()
 * Arguments: rel_pos  -1 to 1 (negative to move out)
 *            vel, acc  0 to 1
 * Description: Sets the brake actuator, taking the first argument as the
 *  desired displacement from the current position. This one doesn't depend
 *  on calibrated values. Just moves the brake with the count values
 */
int set_brake_rel_pos_counts(double rel_pos, double vel, double acc);

/* get_brake_pos()
 * Description: Returns the current brake position in counts,
 *  which it gets from the brake controller,
 *  which keeps the last commanded value?? (how is it different from the pot?)
 */
int get_brake_pos();


//Helper functions for scaling and limiting numbers
double in_range_double(double val, double min_val, double max_val);
double scale_double(double val, double old_min_val, double old_max_val, double new_min_val, double new_max_val);

#endif        /* BRAKE_H */
