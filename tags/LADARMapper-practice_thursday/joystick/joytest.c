#include <stdio.h>

#include "sw.h"
#include <unistd.h>

#define ST_PORT 0

int main(void)
{
   struct steer_struct st;
   int i=1;

   st.x=0;
   st.y=0;
   st.buttons=0;
   
   if(init_steering(ST_PORT)!=0)
	fprintf(stderr, "Can't open port for steering wheel!!!");
	
   while(1)
   {   
	if(i==1000) i=0;
   	printf("%3d ", i++);
	get_steering(&st);
	printf("Relative X: %0.3f -- Y:%0.3f", st.x, st.y); 

	if (st.buttons & STEER_BUTTON_A) printf("A ");
	if (st.buttons & STEER_BUTTON_B) printf("B ");
	if (st.buttons & STEER_BUTTON_C) printf("C ");
	if (st.buttons & STEER_BUTTON_D) printf("D ");
        printf("\n");
        fflush(stdout);
	usleep(30000);
    }
	
    end_steering();
    return 0;
}
