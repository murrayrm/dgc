/****************************************************************************
 *
 *  File        : ladtest.c
 *
 *  Project     : WILDCARD
 *
 *  Copyright   : Annapolis Micro Systems Inc., 1999-2000
 *
 ****************************************************************************/
#include <stdio.h>
#include <time.h>
#include <conio.h>
#include <math.h>
#include <string.h>
#if defined(WIN32)
#include <windows.h>
#include <windowsx.h>
#else
#include <sys/socket.h>
#include <netinet/in.h>		/* for sockaddr_in */
#include <netdb.h>		/* for gethostbyname() */
#include <unistd.h>		/* for read(), write() */
#endif
#include <stdlib.h>
#include <io.h>
#include <fcntl.h>

#include "wcdefs.h"
#include "wc_shared.h"
#include "MER_IMU_LN200_Interface_v15.h"

// Caltech includes
#include "IMU_Process.h"
#include "imu_net.h"

// Networking includes
#include <sys/types.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include "crc32.h"

// ***********************************************************
// Networking related code and declarations
// note that titianium's IP address is currently hard coded 
#ifndef WIN32
#define SOCKET int
#define closesocket close
#endif

// port and destination IP address declarations
static unsigned portnum = 9000;
// unsigned char ip_address[4] = {131, 215, 133, 164}; // grandchallenge
unsigned char ip_address[4] = {192, 168, 0, 11}; // palladium
//unsigned char ip_address[4] = {131,215,89,150}; // old grandchallenge

static int
lookup_addr (struct sockaddr *addr,
	     unsigned port)
{
  struct sockaddr_in addr_in;
  memset (&addr_in, 0, sizeof (addr_in));
  addr_in.sin_family = AF_INET;
  addr_in.sin_port = htons (port);
  memcpy(&addr_in.sin_addr, ip_address, 4);
  memcpy (addr, &addr_in, sizeof (struct sockaddr_in));
  return 1;
}

// *****************************************************************************


void wait_delay(void)
{ int i;
    for (i =0; i < 200; i++)
  {
  }
}


/****************************************************************************
 *
 *  Function    : main
 *
 *  Description : Entry point for the WILDCARD
 *                This function is a basic entry point into the test.
 *                It is responcibe for
 *                   1) Parsing the command line parametrs and filling the
 *                      TestInfo struct with those parameters
 *                   2) Opening the WILDCARD(tm) board
 *                   3) Calling the main example procedure
 *                   4) Closing the board when the example completes
 *
 ****************************************************************************/
WC_RetCode
main( int  argc, char *argv [] )
{
  WC_RetCode
    rc = WC_SUCCESS;

  int
    argi;

  WC_TestInfo
    TestInfo;

  char **TestLoc = NULL;

  const char * help_string =
      "Usage: ladtest <list of options>\n"
      "   Options:\n"
      "      -v               Sets verbose mode. Show progress messages.\n"
      "      -i <num>         Sets the number of times to perform the example.\n"
      "                       (default = 1)\n"
      "      -s <num>         Set WILDCARD(tm) device \"slot\" number (default = 0).\n"
      "      -h               Show this help.\n";struct sockaddr addr;
  

  fprintf( stdout, "%% WILDCARD(tm) Mem_Test Example\n");

  TestInfo.bVerbose    = DEFAULT_VERBOSITY;
  TestInfo.DeviceNum   = DEFAULT_SLOT_NUMBER;
  TestInfo.dIterations = DEFAULT_ITERATIONS;
  TestInfo.fClkFreq    = DEFAULT_FREQUENCY;
  // TestInfo.fClkFreq    = 12.0;

  /*  Parse the command line parameters */
  for ( argi = 1; argi < argc; argi++ )
  {
    if ( argv[argi][0] == '-' )
    {
      switch ( toupper(argv[argi][1]) )
      {
        case 'H':  /* Print the help message */
          fprintf ( stdout, "%s\n\n", help_string,1,1 );
          return(WC_SUCCESS);
          break;

        case 'I':  /* Set the number of iterations */
          argi++;
          TestInfo.dIterations =  strtoul( argv [ argi ], TestLoc, 0 );
          /* Error Check the result.
           * The following test will be true only if there was an error
           * in the string conversion above */
          if (TestInfo.dIterations == 0)
          {
            fprintf( stdout, "\nWARNING:  An invalid or missing iteration value\n");
            fprintf( stdout, "          was found after the -i option.\n\n");
            fprintf( stdout, "%s\n\n", help_string );
            return (ERROR_UNKNOWN_SWITCH);
          }

          fprintf( stdout, "Setting the iteration value to %d\n",TestInfo.dIterations);
          break;

        case 'S':  /* Set the device number */
          argi++;
          TestInfo.DeviceNum =  strtoul( argv [ argi ], TestLoc, 0 );
          /* The following tests for a valid slot number */
          if (TestInfo.DeviceNum > WC_MAX_DEVICES)
          {
            fprintf( stdout, "\n  WARNING:  Invalid device number!\n");
            return (ERROR_UNKNOWN_SWITCH);
          }
          else
          {
            fprintf( stdout, "  Setting the device number to %d.\n", TestInfo.DeviceNum);
          }
          break;

        case 'V':  /* Show all Errors & set maximum verbosity */
          TestInfo.bVerbose=TRUE;
          fprintf( stdout, "  Setting Maximum Verbosity.\n");
          break;

        default: /* Unknow switch option */
          fprintf ( stderr, "\n  WARNING:  Unknown option: \"%s\"\n", argv [ argi ] );
          fprintf ( stderr, "%s\n\n", help_string );
          return( ERROR_UNKNOWN_SWITCH );
      }
    }
    else /* Missing the '-' */
    {
      fprintf ( stderr, "\n  WARNING: Unknown option: \"%s\"\n", argv[argi] );
      fprintf ( stderr, "%s\n\n", help_string );
      return(ERROR_UNKNOWN_SWITCH);
    }
  }

  /*  The WILDCARD(tm) MUST be opened before doing any type of *
   *  access to the card.                                      */
  if (TestInfo.bVerbose)
  {
    fprintf(stdout,"\n  Opening Device %d...\n", TestInfo.DeviceNum);
  }
  rc = WC_Open( TestInfo.DeviceNum, 0 );
  DISPLAY_ERROR(rc);

  /* Once the board is successfully opened, the test may be run */
  rc = WC_LADTest_Main( &TestInfo);
  if (rc!=WC_SUCCESS)
  {
    DisplayError(rc);
  }

  /* The WILDCARD(tm) should be closed when the program finishes to *
   * free driver resources                                          */
  rc = WC_Close( TestInfo.DeviceNum );
  DISPLAY_ERROR(rc);
    return(rc);
}

/****************************************************************************
 *
 *  Function    : WC_LADTest_Main
 *
 *  Parameters  : TestInfo - Test Parameters
 *
 *  Description : Initializes the WILDCARD(tm) hardware, and runs the example
 *                TestInfo->dIterations times.
 *
 ****************************************************************************/
WC_RetCode
WC_LADTest_Main (WC_TestInfo *TestInfo)
{
  DWORD
    dIteration,
    dErrorCount;

  WC_RetCode
    rc = WC_SUCCESS;

  /*  Print out a few parameters so we know what we are running */
  if (TestInfo->bVerbose)
  {
    fprintf(stdout,"\n  TEST PARAMETERS:\n");
    fprintf(stdout,"    Clock Frequency = %f\n",TestInfo->fClkFreq);
    fprintf(stdout,"    # of Iterations = %d\n",TestInfo->dIterations);
    fprintf(stdout,"    Device Number   = %d\n",TestInfo->DeviceNum);
    fprintf(stdout,"    Verbose Mode    = %s\n",TestInfo->bVerbose?"TRUE":"FALSE");
  }

  /*  This routine will put the WILDCARD in a known state.  *
   *  We only need to do this before the first iteration.   *
   *  Each additional iteration only needs to reset the     *
   *  PE to initialize the WILCARD to a known state because *
   *  All initialization parameters are kept between resets.*/
  rc = WC_LADTest_Init( TestInfo);
  CHECK_RC(rc)

  /*  Now that the PE is initialized, we run the test       *
   *  TestInfo->dIterations times, counting the number of   *
   *  failures as we go.                                    */
  for (dIteration = 0,dErrorCount = 0; dIteration < TestInfo->dIterations; dIteration++)
  {
    fprintf(stdout,"\n  %% **** LAD Example Iteration [%d] of [%d] ****\n",dIteration, TestInfo->dIterations);
    rc = WC_LADTest_Run(TestInfo);
    if (rc != WC_SUCCESS)
    {
      DisplayError(rc);
      dErrorCount++;
    }
  }

  /*  Let the user know if the example was a success */
  fprintf(stdout, "\n  Example Complete!  [%d] of [%d] Successful",
          TestInfo->dIterations - dErrorCount, TestInfo->dIterations);

  if (dErrorCount)
  {
    fprintf(stdout, "  ERRORS In Example!\n\n");
  }
  else
  {
    fprintf(stdout, "  Example SUCCESSFUL!\n\n");
  }

  /*  Return SUCCESS if we have made it this far without *
   *  returning.  This means that no fatal errors have   *
   *  occurred.  If any test errors occurred, they have  *
   *  already been printed above after each iteration.   */
  return (WC_SUCCESS);
}

/****************************************************************************
 *
 *  Function    : WC_LADTest_Run
 *
 *  Parameters  : TestInfo - Test Parameters
 *
 *  Description : Runs the Memory Example.  This hardware for this example
 *                contains an image with a LAD_Mux_Block_RAM and a
 *                LAD_Mux_Register_File.
 *
 *                This example will write a random pattern to each of the
 *                components, read it back, and verify that the read and
 *                write contents are equal.
 *
 ****************************************************************************/
WC_RetCode
WC_LADTest_Run( WC_TestInfo *TestInfo )
{

		double theta_x=0;
	double theta_y=0;
	double theta_z=0;
	int old_frame=0;



  DWORD
    *pReadBuffer,
    *pWriteBuffer,
    index, index2;

  unsigned long int
    imu_frame_count_old, imu_frame_count, counter;

  WC_RetCode
    rc;

  unsigned int test_data[32];  // Storage for the IMU test data

  // added DGC declarations
  short convert;
  IMUReading current;
  //int filenum;
  //FILE *imufile;

   // ****************************************************
  // Networking declarations/initializations
  struct sockaddr addr;
  SOCKET sock;

      char buf[sizeof(current) + 4];
      unsigned len;
	
	WORD wVersionRequested;
	WSADATA wsaData;
	int err;
	 
	wVersionRequested = MAKEWORD( 2, 2 );
	 
	err = WSAStartup( wVersionRequested, &wsaData );
	if ( err != 0 ) {
		/* Tell the user that we could not find a usable */
		/* WinSock DLL.                                  */
		return;
	}

	sock = socket(AF_INET, SOCK_DGRAM, 0);

	if (!lookup_addr (&addr,portnum))
	{
			return 1;
	}

//	fprintf(stderr, "Enter test number: ");
//	scanf("%d", &filenum);
//	imufile = fopen("test%d.dat", filenum, "a+b");

  // *******************************************************
  
  
  
  /*  The first step in an application is almost always to reset the PE. *
   *  Although this is not needed for the first iteration of this        *
   *  example because WC_LADTest_Init has already reset the PE, we need  *
   *  to do it again here because subsequent iterations need a fresh PE  *
   *  reset.                                                             *
   *  One assumption made below is that the time between the two         *
   *  WC_PeReset calls is sufficient to reset the PE.  In this example,  *
   *  and in general, this is true.  The reset line need only be high    *
   *  for at most one clock cycle of the longest period clock.           */
  fprintf(stdout, "\n %% Resetting PE                        ... ");
  rc=WC_PeReset( TestInfo->DeviceNum, TRUE );
  CHECK_RC(rc);

  rc=WC_PeReset( TestInfo->DeviceNum, FALSE );
  CHECK_RC(rc);
  fprintf(stdout, "DONE\n");

   /*  The simplest method of buffer verification is to have different   *
    *  buffers for reading and writing. Below we allocate and initialize *
    *  these buffers.                                                    *
    *  The Block RAM is the largest object that we can read and write,   *
    *  so this is size buffer we need.                                   */
  fprintf( stdout, "%%  Allocating Buffers                  ... ");

  if (TestInfo->bVerbose) fprintf(stdout, "\n    * Allocating Read Buffer          ... ");
  pReadBuffer  = malloc(BRAM_SIZE_DWORDS * sizeof(DWORD));
  if (!pReadBuffer) return (ERROR_MEMORY_ALLOC);
  memset(pReadBuffer, 0, BRAM_SIZE_DWORDS);

  if (TestInfo->bVerbose) fprintf(stdout,"DONE\n    * Allocating Write Buffer         ... ");
  pWriteBuffer = malloc(BRAM_SIZE_DWORDS * sizeof(DWORD));
  if (!pWriteBuffer)
  {
    free(pReadBuffer);
    return (ERROR_MEMORY_ALLOC);
  }

  fprintf(stdout, "DONE\n");

  /* READ AND WRITE THE REGISTERS */
  fprintf(stdout, "%%  Testing Register Data               ... ");

  /****************************************** IMU TEST ****************************/

  //  pWriteBuffer[0] = 0x0302017e;
  //  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+index, 1, &pWriteBuffer[index]);
  //  rc = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+index, 1, &pReadBuffer[index]);
  //  fprintf(stdout, "pReadBuffer[%i] = %X\n", index, pReadBuffer[index]);

  // output to screen
  fprintf(stdout, "\n\n");
  fprintf(stdout, "%% Transmitting Data from IMU:\n\n");
  fprintf(stdout, "%% imu_rec_busy \timu_crc_error \timu_frame done \timu_frame_error_count \timu_crc_eror_count \timu_frame_count \tdata00 \tdata01 \tdata02 \tdata03 \tdata04 \tdata05 \tdata06 \tdata07 \tdata08 \tdata09 \tdata10 \tdata11 \tdata12 \tx_vel \ty_vel \tz_vel \tx_phi \ty_phi \tz_phi \tx_phi_sum \ty_phi_sum \tz_phi_sum \tframe_diff");
	  //\timu_tp \timu_time_tag \tnew_imu_timetag \timu_fpga_timetag \tcesium_timetag\n");
  fprintf(stdout, "\n"); 

  // output to file


  // RESET IMU_DATA_RX by toggling (i_rst_n) (Active reset i_rst_n = high)
  pWriteBuffer[0] = 0x00000000 | 0x10000000;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  wait_delay();
  pWriteBuffer[0] = 0x00000002 | 0x10000000;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  wait_delay();
  pWriteBuffer[0] = 0x00000000 | 0x10000000;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  wait_delay();

  // RESET IMU_rti_update 
  pWriteBuffer[0] = 0x00000000 | 0x10000000;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  wait_delay();
  pWriteBuffer[0] = 0x00000008 | 0x10000000;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  wait_delay();
  pWriteBuffer[0] = 0x00000000 | 0x10000000;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  wait_delay();


  // RESET IMU_rtin_sync 
  pWriteBuffer[0] = 0x00000000 | 0x10000000;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  wait_delay();
  pWriteBuffer[0] = 0x00000004 | 0x10000000;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  wait_delay();
  pWriteBuffer[0] = 0x00000000 ;
  rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  wait_delay();

  // NOW FPGA is receiving DATA from IMU
  imu_frame_count_old = 0;
  counter = 0;


  while(TRUE) {    // wait for the frame_done signal
    // Testing "LEDs" on J6 J7, J8, J9
    // counter = counter + 1;
    // pWriteBuffer[0] = 0x00000000 | (( counter & 0x0f ) << 5);   
    // fprintf(stdout, "%4x\n", pWriteBuffer[0]);

    // rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);
    //

    // Read status register and test it for imu_frame_done
    rc = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, pReadBuffer);
    if ( (((pReadBuffer[0] >>  2) & 0x00000001) == 0x00000001) && (rc == WC_SUCCESS) ) { // imu_frame_done
      rc = WC_PeRegRead(TestInfo->DeviceNum, REGISTER_OFFSET+0, 12, pReadBuffer); // Read all data
      // clear imu_frame_done
      pWriteBuffer[0] = 0x00000000;
      // pWriteBuffer[0] = 0x00000000 | (( counter & 0x0f ) << 5);        
      rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  // wait_delay();
      pWriteBuffer[0] = 0x00000010;
      // pWriteBuffer[0] = 0x00000010 | (( counter & 0x0f ) << 5);
      rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);  // wait_delay();
      pWriteBuffer[0] = 0x00000000;
      // pWriteBuffer[0] = 0x00000000 | (( counter & 0x0f ) << 5);
      rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+0, 1, &pWriteBuffer[0]);
	  // rc = WC_PeRegWrite(TestInfo->DeviceNum, REGISTER_OFFSET+12, 1, &pWriteBuffer[0]);
      
      imu_frame_count = (pReadBuffer[0] >> 24) & 0x000000ff;
      
     // if ( imu_frame_count != imu_frame_count_old ) {          // New frame is received
	// fprintf(stdout, "%1x \t%1x \t%1x \t%4X \t%4X \t%4X \t%8X \t%8X \t%8X \t%8X \t%8X \t%8X \t%8X \t%8X \t%8X \t%8X \t%8X \t%8X \t%8X \t%8X \t%4X \t%8X \t%8X \t%8X\n",
	  {
		  //*******************************************************
	  // Convert raw IMU data and transmit to main computer
	   // get frame count
		convert = (pReadBuffer[0] >> 24) & 0x000000ff;
		current.frame = convert;
		
		// deltavs and delta thetas
		convert = (pReadBuffer[1] >>  0) & 0x0000ffff;  // data 0 - dvx
		current.dvx = process_accel(convert);

		convert = (pReadBuffer[1] >> 16) & 0x0000ffff;  // data 1 - dvy
		current.dvy = process_accel(convert);	
		
		convert = (pReadBuffer[2] >>  0) & 0x0000ffff;  // data 2 - dvz
		current.dvz = process_accel(convert);

		convert = (pReadBuffer[2] >> 16) & 0x0000ffff;  // data 3 - dtx
		current.dtx = process_theta(convert);

		convert = (pReadBuffer[3] >>  0) & 0x0000ffff;  // data 4 - dty
		current.dty = process_theta(convert);

		convert = (pReadBuffer[3] >> 16) & 0x0000ffff;  // data 5 - dtz
		current.dtz = process_theta(convert);


		theta_x += current.dtx;
		theta_y += current.dty;
		theta_z += current.dtz;


		//// transmit code here

		memcpy (buf + 4, &current, sizeof (current));
		len = sizeof (current);
		//fprintf(stderr, "Packet size: %d \n", len);
		crc32_big_endian (buf + 4, len, (unsigned char*)buf);
		if (sendto(sock, buf, len + 4, 0, &addr, sizeof(addr)) != len+4)
		{
			fprintf(stderr, "sendto failed\n");
		}

		//********************************************************
	
#ifdef UNUSED
	  fprintf(stdout, "%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%i \t%f \t%f \t%i \t%i \t%i \t%i \t%i ", 
		//  pReadBuffer[0] & 0x00000007,  // Status bits: imu_frame done imu_crc_error imu_rec_busy
		(pReadBuffer[0] >>  0) & 0x00000001,  // imu_rec_busy
		(pReadBuffer[0] >>  1) & 0x00000001,  // imu_crc_error
		(pReadBuffer[0] >>  2) & 0x00000001,  // imu_frame done
		(pReadBuffer[0] >>  8) & 0x000000ff,  // imu_frame_error_count
		(pReadBuffer[0] >> 16) & 0x000000ff,  // imu_crc_error_count
		(pReadBuffer[0] >> 24) & 0x000000ff,  // imu_frame_count
		(pReadBuffer[1] >>  0) & 0x0000ffff,  // data 0
		(pReadBuffer[1] >> 16) & 0x0000ffff,  // data 1
		(pReadBuffer[2] >>  0) & 0x0000ffff,  // data 2
		(pReadBuffer[2] >> 16) & 0x0000ffff,  // data 3
		(pReadBuffer[3] >>  0) & 0x0000ffff,  // data 4
		(pReadBuffer[3] >> 16) & 0x0000ffff,  // data 5
		(pReadBuffer[4] >>  0) & 0x0000ffff,  // data 6 
		(pReadBuffer[4] >> 16) & 0x0000ffff,  // data 7
		(pReadBuffer[5] >>  0) & 0x0000ffff,  // data 8
		(pReadBuffer[5] >> 16) & 0x0000ffff,  // data 9 
		(pReadBuffer[6] >>  0) & 0x0000ffff,  // data 10
		(pReadBuffer[6] >> 16) & 0x0000ffff,  // data 11
		(pReadBuffer[7] >>  0) & 0x0000ffff  // data 12
		);
#endif

	  fprintf(stdout, "\rframe %d, omega: %5.2f %5.2f %5.2f     angle: %5.2f %5.2f %5.2f",
		current.frame-old_frame,
		current.dtx*1000,
		current.dty*1000,
		current.dtz*1000,
		theta_x,
		theta_y,
		theta_z	
); 
/*		
		(pReadBuffer[7] >> 16) & 0x0000ffff,  // imu_tp
		(pReadBuffer[8] >>  0) & 0x000000ff,  // imu_time_tag
		(pReadBuffer[9] >>  0) & 0xffffffff,  // new imu_timetag
		(pReadBuffer[10] >>  0) & 0xffffffff, // new imu_fpga_timetag
		(pReadBuffer[11] >>  0) & 0xffffffff  // cesium_timetag
//		(pReadBuffer[12] >>  0) & 0x0000ffff,  // Read Counter
//		(pReadBuffer[12] >> 16) & 0x0000ffff  // Write Counter
*/



	  		old_frame = current.frame;
	  imu_frame_count_old = imu_frame_count;
      }
      
    }
  } //End of infinite loop while(true)


  /****************************************** END IMU TEST ****************************/

  free(pReadBuffer);
  free(pWriteBuffer);

  
  fprintf(stdout, "DONE\n");

  return (rc);

}


/****************************************************************************
 *
 *  Function    : DeviceInitialize
 *
 *  Notes       : This function puts the card into a known state before the
 *                test begins.  It is generally a bad idea to assume the
 *                state of the WILDCARD's hardware when a program starts.
 *                Previous programs can leave the hardware in an unknown
 *                state, and it's state on power-on is undefined. If an
 *                application requires a specific state of the hardware,
 *                explicitly set that state.
 *
 *                Before running any application the following steps
 *                should be performed in the order given.
 *
 *                  1) Toggle Power
 *                  2) Assert the processing element reset line
 *                  3) Program the processing element
 *                  4) Set the clock frequency
 *                  5) Configure Interrupts
 *                  6) Deassert the processing element reset line
 *
 ****************************************************************************/
WC_RetCode
WC_LADTest_Init( WC_TestInfo *TestInfo )
{
  WC_RetCode
    rc=WC_SUCCESS;

  /* A great deal of useful information is available from the ID  *
   * PROM on the WILDCARD(tm), including processing element part  *
   * type, memory size, speed grade, etc.  The two API calls,     *
   * WC_DeviceInformation and WC_GetVersion are used to retrieve  *
   * that information.  The procedure DisplayConfiguration,       *
   * defined in wc_shared.c, displays this information to the     *
   * screen.                                                      *
   *                                                              *
   * Below we use the API calls to store the WILDCARD(tm) device  *
   * and version information in the TestInfo struct for use later *
   * in the example, as well as display the information if        *
   * verbosity is on.                                             */

  rc = WC_DeviceInformation( TestInfo->DeviceNum, &(TestInfo->DeviceCfg) );
  CHECK_RC(rc);
  rc = WC_GetVersion( TestInfo->DeviceNum, &(TestInfo->Version));
  CHECK_RC(rc);

  if (TestInfo->bVerbose)
  {
    rc=DisplayConfiguration(TestInfo->DeviceNum);
    CHECK_RC(rc);
  }

  /* It should NOT be assumed that the WILDCARD(tm) processing    *
   * element currently has power.  Below we toggle the power to   *
   * the processing element, leaving it ON for the remainder of   *
   * the example.                                                 */
  if (TestInfo->bVerbose)
  {
    fprintf (stdout, "  Toggling processing element's power...\n");
  }
  rc=WC_PeApplyPower ( TestInfo->DeviceNum, FALSE );
  CHECK_RC(rc);
  rc=WC_PeApplyPower ( TestInfo->DeviceNum, TRUE );
  CHECK_RC(rc);

  if (TestInfo->bVerbose)
    fprintf (stdout, "    PE power turned on.\n");

  /*  The WILDCARD(tm) has a dedicated reset line controlled by *
   *  the WC_PeReset API call.  In general it is advantageous   *
   *  to have the PE in reset when it is being set up. This     *
   *  will prevent the design from starting execution until the *
   *  WILDCARD(tm) has been correcly initialized.               *
   *                                                            *
   *  Below we assert the reset line and keep it asserted       *
   *  until the processing element has been programmed, the     *
   *  clock has been set, and interrupts have been initialized. *
   *
   *  If the Reset_STD_If has been instantiated in the VHDL,    *
   *  this API call will set the signal 'Global_Reset' high.    */

  if (TestInfo->bVerbose)
    fprintf (stdout, "  Asserting PE Reset Line...\n");
  rc=WC_PeReset( TestInfo->DeviceNum, TRUE );
  CHECK_RC(rc);
  if (TestInfo->bVerbose)
    fprintf (stdout, "    PE RESET line asserted.\n");

  /*  As of the creation of this file there are 4 revisions of  *
   *  the WILDCARD(tm) hardware.  (Revs A to D)  Below we use   *
   *  the informaion in TestInfo->Version to determine the      *
   *  revision of the card in this slot.                        */
  rc = WC_GetVersion( TestInfo->DeviceNum, &TestInfo->Version);
  CHECK_RC(rc);

  if (TestInfo->bVerbose)
    fprintf (stdout, "  Loading PE Image...\n");

  if (((TestInfo->Version.Hardware & WC_MAJOR_VER_MASK)>>WC_MAJOR_VER_SHIFT) == 4)
  {
    /*                  REV D WILDCARD(tm)                      *
     *                                                          *
     * The ProgramPeFromFile procedure, found in ws_shared.c,   *
     * will append .\<PART TYPE>\<PACKAGE TYPE>\ to the         *
     * filename, and load that file into the processing element.*
     * For a REV D this path will be                            *
     *    .\XCV300E\PKG_BG352\<IMAGE_FILENAME_REVD>             */
    rc=ProgramPeFromFile( TestInfo->DeviceNum, IMAGE_FILENAME_REVD );
    CHECK_RC(rc);
  }
  else
  {
    /*                  REV A-C WILDCARD(tm)                    *
     *                                                          *
     * The ProgramPeFromFile procedure, found in ws_shared.c,   *
     * will append .\<PART TYPE>\<PACKAGE TYPE>\ to the         *
     * filename, and load that file into the processing element.*
     *                                                          *
     * For a REV C this path will be                            *
     *    .\XCV300E\PKG_BG352\<IMAGE_FILENAME>                  *
     *                                                          *
     * For REVs A or B this path will be                        *
     *    .\XCV300\PKG_BG352\<IMAGE_FILENAME>                   */
    rc=ProgramPeFromFile( TestInfo->DeviceNum, IMAGE_FILENAME );
    CHECK_RC(rc);
  }

  if (TestInfo->bVerbose)
    fprintf (stdout, "    PE Image Loaded.\n");

  /* The WILDCARD(tm) has one on-board programmable oscillator. *
   * WC_ClkSetFrequency sets the frequency of that clock.  We   *
   * always want to set the clock to the appropriate frequency  *
   * before running our application.                            */

  if (TestInfo->bVerbose)
    fprintf(stdout, "  Initializing the clock to %f...\n", TestInfo->fClkFreq);
  rc=WC_ClkSetFrequency ( TestInfo->DeviceNum, TestInfo->fClkFreq );
  CHECK_RC(rc);
  if (TestInfo->bVerbose)
    fprintf(stdout, "    Clock initialized.\n");

  /*  This application does not use the PE interrupt line,  *
   *  so the following code is not needed. It is generally  *
   *  a good practice to set the state of the interrupt     *
   *  line & event, so we'll do it here anyway.             */
  if (TestInfo->bVerbose)
    fprintf (stdout, "  Masking PE Interrupt...\n");
  rc=WC_IntEnable( TestInfo->DeviceNum, FALSE);
  CHECK_RC(rc);
  if (TestInfo->bVerbose)
    fprintf (stdout, "    PE Interrupt Masked.\n");

  /*  The order of mask / reset may be important in some     *
   *  circumstances.  In our case it is not.  We mask,       *
   *  then clear anything that may have been happened before *
   *  the masking operation                                  */
  if (TestInfo->bVerbose)
    fprintf (stdout, "  Resetting PE Interrupt...\n");
  rc=WC_IntReset( TestInfo->DeviceNum );
  CHECK_RC(rc);
  if (TestInfo->bVerbose)
    fprintf (stdout, "    PE Interrupt Reset.\n");


  /*  Lastly, we remove the PE from the RESET state.  When  *
   *  the Reset_STD_IF is instantiated in the VHDL, this    *
   *  will set the VHDL signal 'Global_Reset' low.          */

  if (TestInfo->bVerbose)
    fprintf (stdout, "  De-asserting PE Reset Line...\n");
  rc=WC_PeReset( TestInfo->DeviceNum, FALSE );
  CHECK_RC(rc);
  if (TestInfo->bVerbose)
    fprintf (stdout, "    PE RESET line de-asserted.\n");


  return(rc);
}

/****************************************************************************
 *
 *  Function    : VerifyData
 *
 *  Parameters  : ref  - Reference buffer. (Typically the WriteBuffer)
 *                test - Test Buffer. (Typically the ReadBuffer)
 *                size - Number of DWORDS to test
 *
 *  Description : Compares two buffers and prints the differences if found.
 *
 ****************************************************************************/

WC_RetCode
VerifyData(DWORD ref[], DWORD test[], DWORD size)
{
  DWORD
    memCntr,
    errCount = 0;

  WC_RetCode
    rc = WC_SUCCESS;


  /*  Loop counts off in DWORDS.
   *  Mismatches will stop being counted after MAX_ERR_COUNT errors.
   */

  for ( memCntr=0; (memCntr < size); memCntr++)
  {
    if (ref[memCntr] != test[memCntr])
    {
      rc=ERROR_MEMORY_COMPARE_FAILED;
      if (errCount==0)
      {
        printf( "\n\tERROR: Memory Compare Failed\n");
        printf( "\tDWORD:\tExpected Data:\tActual Data\n");
      }

      printf( "\t%i\t0x%08x\t0x%08x\n",  memCntr, ref[memCntr], test[memCntr]);
      errCount++;
    }
    if ( errCount> MAX_ERR_COUNT )
    {
      printf( "\tMemory compare stopped after %d errors found.\n\n", MAX_ERR_COUNT);
      break;
    }
  }

  if (errCount!=0  && errCount <= MAX_ERR_COUNT )
  {
    printf( "\tNumber of memory errors found: %d.\n\n", errCount);
  }
  return(rc);
}
