/***************************************************************************************/
//FILE: 	bob.h
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file has the data that bob returns to the application such as
//		gps velocity and position
//
/***************************************************************************************/

#ifndef BOB_H
#define BOB_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <fstream>


// Data from the model
// x    : bob's x position
// y    : bob's y position
// theta: bob's heading
// phi  : bob's steering angle
// vel  : bob's velocity
struct bobData {
  double x;
  double y;
  double theta;
  double phi;
  double vel;
};

// Class to access the data
class bobDataWrapper {

 public:
  // Constructors and Destructors
  bobDataWrapper();
  ~bobDataWrapper();

  // Prints all the data in the object to the screen
  void print_to_screen();

  // Appends all the data in the object to filename
  void write_to_file(char *filename);

  // Get the values
  void get_data(bobData &buffer);

  // Updates the values
  void update_data(bobData buffer);

  // Data
  bobData data;
};
#endif
