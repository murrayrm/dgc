#ifndef ModuleHandler_HH
#define ModuleHandler_HH


#include "Misc/Mail/Mail.hh"
#include "DGC_MODULE.hh"
#include <boost/shared_ptr.hpp>

class ModuleHandler {

public:
  ModuleHandler(shared_ptr<DGC_MODULE> dm, int ModNum);

  void InMail(Mail & ml);
  Mail QueryMail(Mail & ml);

  int ModuleNumber();

  shared_ptr<DGC_MODULE> p_dm;

private:
  //  Mailbox mb;


  boost::recursive_mutex MailMutex;

  int M_ModuleNumber;

};




#endif
