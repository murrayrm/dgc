// Mailbox.cc

#include "Mailbox.hh"
#include "Misc/Mail/Mail.hh"

Mailbox::Mailbox(int mn) : M_MailboxNumber(mn){

}


Mailbox::Mailbox~() {

}

Mail Mailbox::Pop() {
  boost::recursive_mutex::scoped_lock sl(MailboxLock);
  Mail r;
  if( Q.size() > 0 ) {
    r = Q.front();
    Q.pop_front();
  } else {
    r.Flags(MailFlags::BadMail);
  }
  return r;
}

void Mailbox::Push(Mail& msg) {
  boost::recursive_mutex::scoped_lock sl(MailboxLock);
  Q.push_back(msg);
}
