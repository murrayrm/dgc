#ifndef PerceptorDatum_HH
#define PerceptorDatum_HH

struct PerceptorDatum {

  //Shared Memory Variables
  int shmid;
  int size;
  int key;
  int shmflg;
  void *map_data;
};

#endif
