% Reads in KF output state data and GPS state data 
% and places files in column vectors 

% read in k-filtered state data and store the northing and easting
load statelog.dat;
kfnorthing = statelog(:,3);

% go down until we find that the KF has started using GPS locations to
% generate data (start with first non-zero kfnorthing)
i = find(kfnorthing > 10);
statelog = statelog(i(1):end, :);
kftime = statelog(:,1);
kfeasting = statelog(:,2);
kfnorthing = statelog(:,3);
kfaltitude = statelog(:,4);
kfve = statelog(:,5);
kfvn = statelog(:,6);
kfvu = statelog(:,7);
kfspeed = statelog(:,8);
kfpitch = statelog(:,10);
kfroll = statelog(:,11);
kfheading = statelog(:,12);

load gpslog.dat
gpstime = gpslog(:,1);
i = find(gpstime > kftime(1));  % only use points starting from same time as kf
gpslog = gpslog(i:end, :);

% now fill in all the columns with values
gpstime = gpslog(:,1);
gpsvalid = gpslog(:,2);
gpsnewgps = gpslog(:,3);
gpsmoderaw = gpslog(:,4);
gpseasting = gpslog(:,7);
gpsnorthing = gpslog(:,8);
gpsaltitude = gpslog(:,9);
gpsvn = gpslog(:,10);
gpsve = gpslog(:,11);
gpsvu = gpslog(:,12);
gpsactive = gpslog(:,13);
gpsextnav = gpslog(:,14);
