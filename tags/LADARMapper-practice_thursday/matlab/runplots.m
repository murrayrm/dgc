% this file sets up the variables to plot the ladar scans.  All you
% need to do is change logdir to be the directory where the scan/state
% files are kept, and change scans and states to be the filenames.

% clear everything so we don't have any old crap lying around
clear all;
%logdir = '/home/klk/test_logs/20040210_tennislot/LADAR_data/';
logdir = '/local/scratch/klk/';
scans = 'simflat_scans_02202004_143445.log';
%scans = 'simpleloops_scans_02162004_160842.log';
states = 'simflat_state_02202004_143445.log';
%scans = 'collect_scans_02102004_172341.log';
%states = 'collect_state_02102004_172341.log';
%logdir = '/home/klk/test_logs/20040201_caltechlot/ladar_data_roof_figureeights/';
%scans = 'ladar_data_roof_eights_scans_02012004_155246.log';
%states = 'ladar_data_roof_eights_state_02012004_155246.log';

scanfile = strcat(logdir,scans);
statefile = strcat(logdir,states);

% turn off an annoying but maybe useful warning...  happens because
% when we fit the data to a grid, we have multiple scanpoints for a
% single grid pixel.  I think the default is that it averages
% evertying, probably want to change that.
warning off MATLAB:griddata:DuplicateDataPoints

% plot the scans
plot_ladar_scans;



%  old but maybe useful stuff...
%
%stateformat=strcat(repmat('%*d',1,2),... %timestamps
%		   repmat('%f',1,3),... %easting,northing,alt
%		   repmat('%*f',1,5),...
%		   repmat('%f',1,3),... %pitch,roll,yaw
%		   repmat('%*s',1,35));
%stateformat=strcat(repmat('%*d',1,2),... %timestamps
%		   repmat('%f',1,3),... %easting,northing,alt
%		   repmat('%*f',1,5),...
%		   repmat('%f',1,3),... %pitch,roll,yaw
%		   repmat('%*s',1,6)...
%		   repmat('%f',1,2)... %lat lng
%		   repmat('%*s',1,27));
%[statedata(:,1),statedata(:,2),statedata(:,3),...
% statedata(:,4),statedata(:,5),statedata(:,6)]...
%    = textread(statefile,stateformat);
%[state(:,1),state(:,2),state(:,3),state(:,4),state(:,5),state(:,6)]...
%    = textread(statefile,stateformat);
