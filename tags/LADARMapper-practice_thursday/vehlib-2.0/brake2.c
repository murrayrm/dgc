 /*
 * brake2.c - brake actuator interface
 *
 * RMM, 23 Feb 04
 *
 * Functions defined in this file
 *
 *   brake_open		initialize connection to brake
 *   brake_close	close connection to brake
 *   brake_calibrate	calibrate brake interface
 *   brake_setposition	set absolute position of brake
 */



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//    SHARED


////////////////////////////////////////////////////////////////////////////////
// Libraries

#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>
#include "brake2.h"
#include "serial.h"
#include "vehports.h"
#include "sparrow/dbglib.h"
#include "MTA/Misc/Time/Time.hh"

////////////////////////////////////////////////////////////////////////////////
// Global Declares

static int brake_port = -1;		/* brake serial port number */
static int brake_enabled = 1;		/* estop enable/disable */
double brake_timeout = 2;		/* timeout for reading response */
double brake_minpos_counts = 0;		/* brake count limits */
double brake_maxpos_counts = 0;
double brake_min_pot = 0.25;		/* brake pot limits */
double brake_max_pot = 0.75;
double brake_cmdpos = 0;		/* commanded position for servo */
double brake_cmdvel = 0;		/* commanded velocity for motor */
double brake_cmdacc = 0;		/* commanded acceleration for motor */
double brake_maxvel = 0;		/* maximum velocity for motor */
double brake_maxacc = 0;		/* maximum acceleration for motor */
double brake_pot_current = 0;		/* current brake pot reading */
int brake_count = 0;			/* servo loop counter */
int brake_case = 0;			/* internal case for debugging */


////////////////////////////////////////////////////////////////////////////////
// Global Declares that shouldn't be set from outside

double buf_cmdpos, buf_cmdvel, buf_cmdacc;
int buf_lock = 0;			/* locking variable for buffer */


////////////////////////////////////////////////////////////////////////////////
// Helper Function Declarations

static double brake_in_range(double, double, double);
static double brake_scale(double, double, double, double, double);
int brake_read(double *curpos);


////////////////////////////////////////////////////////////////////////////////
// Functions

/////////////////////////////////////////////////////////////////////
// brake_write(pos, vel, acc)
//
// Command the motion of the brake to a given position.  This uses the 
// servo loop to actually move the brake (using velocity commands)
//
int brake_write(double pos, double vel, double acc)
{
  int count = 0;

  /* Wait until servo loop is done accessing variables */
  while (buf_lock && count++ < 10) usleep_for(BRAKE_CYCLE_USEC/10);

  /* Store values for use in servo loop */
  buf_cmdpos = pos;
  buf_cmdvel = vel;
  buf_cmdacc = acc;

  return -1;
}

/////////////////////////////////////////////////////////////////////
// brake_close()
//
// Terminate the connection to the brake.  This function makes sure 
// that the brake is no longer moving, but does not return the position
// of the brake to any particular position.
//
int brake_close()
{
  /* Close the port if it has been opened */
  if (brake_port != -1) serial_close(brake_port);
  brake_port = -1;
  return 0;
}


///////////////////////////////////////////////////////////////////// 
// brake_pause
//
int brake_pause() {
  brake_write(BRAKE_MAXPOS, BRAKE_VEL, BRAKE_ACC);
  //WNH  fixed a counts vs 0-1 value problem here
  /*! TODO: Magic number + should use usleep_for in threaded environment !*/
  /*! Better yet, wait until brake gets to destination !*/
  usleep(BRAKE_PAUSE_USEC);	       /* wait for brake to depress */
  brake_enabled = 0;    // prevents brake control loop from moving further
  return 0;
}


///////////////////////////////////////////////////////////////////// 
// brake_disable
//
int brake_disable() {
  return brake_pause();
}

///////////////////////////////////////////////////////////////////// 
// brake_resume
//
int brake_resume() {
  brake_enabled = 1;  // allows the brake thread to send move commands again
  return 0;
}

////////////////////////////////////////////////////////////////////////////////
//helper functions


//////////////////////////////////////////////////////////////////////
// brake_scale
//
// Utility function to scale a command between max and min values.
//
static double brake_scale(double val, 
		   double old_min_val, double old_max_val, 
		   double new_min_val, double new_max_val) {
  double old_range, new_range;

  old_range=old_max_val-old_min_val;
  new_range=new_max_val-new_min_val;

  return ((new_range*(val-old_min_val)/old_range)+new_min_val);
}


//////////////////////////////////////////////////////////////////////
// brake_in_range
//
// Utility function to truncate a command between max and min values
//
static double brake_in_range(double val, double min_val, double max_val) {
  if(val<min_val) return min_val;
  if(val>max_val) return max_val;
  return val;
}

/////////////////////////////////////////////////////////////////////
/*
 * brake_read()
 *
 * Read the current position of the brake, as indicated by the brake
 * pot.  Note that this function requires access to the throttle parallel
 * port, which is assumed to be opened.
 *
 */
int brake_read(double *curpos) 
{
  int count = 0;			/* counter for brake lock */
  double lastpos = 0;			/* last good reading */

  /* Set up the read on the parallel port */
  /* TODO: port number should not be hard coded */
  /* TODO: decide if we need this command; turns code to spaghetti */

  /* Get a lock on the parallel port */
  while (!pp_lock(PP_GASBR) && count++ < 10) usleep_for(BRAKE_CYCLE_USEC/10);
  if (count == 10) return -1;		/* couldn't access parallel port */

  pp_set_bits(PP_GASBR, THROTTLE_CS, 0); // make sure accel doesn't move
  pp_set_bits(PP_GASBR, 0, BT_OE);	 // make sure pot is the only input 
  pp_set_bits(PP_GASBR, 0, BPOT_RC);
  pp_set_bits(PP_GASBR, 0, BPOT_CS);
  usleep(10); // <65ns
  pp_set_bits(PP_GASBR, BPOT_CS, 0);
  usleep(BPOT_TBUSY);    
  pp_set_bits(PP_GASBR, BPOT_RC, 0);
  pp_set_bits(PP_GASBR, 0, BPOT_CS);
  usleep(10); // <83ns

  /* Read from the actual pot */
  int lowerEight = (int) pp_get_data(PP_GASBR);
  int upperFour = (int) pp_get_bit(PP_GASBR, BPOT_D8) 
    + (int) (pp_get_bit(PP_GASBR, BPOT_D9)<<1)
    + (int) (pp_get_bit(PP_GASBR, BPOT_D10)<<2);
  int sign = (int) pp_get_bit(PP_GASBR, BPOT_D11);
  pp_set_bits(PP_GASBR, BPOT_CS, 0);  
  int potReading = lowerEight + (upperFour<<8);

  pp_unlock(PP_GASBR);

  switch (sign) {
  case 0:				/* positive reading */
    /* Return the value potReading scaled to BP_MAX, BP_MIN range. */
    brake_pot_current =
      ((double) potReading/BP_NUM_HIGH - BP_MIN) / (BP_MAX-BP_MIN);

    // WNH 2-07-04 -- TBD: this is a hack to fix the brake pot reading
    // that oscillates between the real value and 0.  The
    // problem  is  most  likely  in  the brake  pot  hardware  or
    // software, but this  should fix for now since  the brake pot
    // should never actually read  0 (because it should be limited
    // to something  bigger than that  by the software) and  if it
    // does, it  will simply  keep the value  it had 1/100th  of a
    // second before that, which will be very close to zero.

    if (brake_pot_current == 0) brake_pot_current = lastpos;
    lastpos = *curpos = brake_pot_current;
    return 0;

  case 1:				/* negative reading */
    /* Something is wrong here; return previous value */
    dbg_error("brake pot returned negative value");
    *curpos = lastpos;
    return -1;

  default:				/* parallel port error */
    /* Something is wrong here; return previous value */
    dbg_error("brake pot parallel port error");
    *curpos = lastpos;
    return -1;
  }
  return -1;				/* keep compiler happy */
}




////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//     OLD BRAKE ACTUATOR ONLY

#ifdef BRAKE_OLD


////////////////////////////////////////////////////////////////////////////////
// Helper Function Declarations

int brake_sendcmd(int, char *, ...), brake_sendack(int, char *, ...);
char *brake_response(), *brake_sendresp(int, char *, ...);


////////////////////////////////////////////////////////////////////////////////
// Function Definitions

//////////////////////////////////////////////////////////////////////
/*
 * brake_open(port)
 *
 * Initialized the connection to the brake actuator across the specified
 * serial port.  The brake also makes use of information that is passed
 * via the throttle parallel port, which is assumed to be properly 
 * initialized.
 *
 */
int brake_open(int port)
{
  char *response;

  /* Close the current brake port if open */
  if (brake_port != -1) brake_close();

  /* Open the serial device for the brake */
  if (serial_open_advanced(port, BRAKE_BAUD,
			   SR_PARITY_DISABLE | SR_SOFT_FLOW_DISABLE | 
			   SR_HARD_FLOW_DISABLE | SR_TWO_STOP_BIT) != 0) {
    dbg_info("Unable to open com port %d" , port);
    return -1;
  } 
  brake_port = port;

  /* Brake initialization: set up parameters */
  brake_minpos_counts = 0; brake_maxpos_counts = BRAKE_MAXPOS_COUNTS;
  //WNH added the counts suffix to eliminate confusion
  brake_cmdvel = brake_maxvel = BRAKE_MAXVEL;
  brake_maxacc = BRAKE_MAXACC;

# ifdef UNUSED
  /* Send the polling command to make sure the brake is there */
  if (brake_sendack(BRAKE_POL, NULL) < 0) {
    dbg_error("brake failed to initialize");

    /* Try reseting the brake to see if that helps */
    brake_reset();
    if (brake_sendack(BRAKE_POL, NULL) < 0) return -1;

    /* Send the brake home, so it is at a known location */
    brake_home();
  }
# endif

  return 0;
}

/////////////////////////////////////////////////////////////////////
/*
 * brake_calibrate()
 *
 * Calibrate the brake actuator by initializing the servo and moving the
 * brake until the brake pot is at the minimum position.  This is not
 * really a true calibration since it doesn't do anything to make sure
 * that the brake pot is reading correctly (and we have reason to believe
 * that it drifts over time, but nothing critical).
 *
 */
int brake_calibrate()
{
  /* Send the brake a reset command and wait for a bit */
  brake_sendcmd(BRAKE_RST, NULL); usleep_for(BRAKE_RESTART_USEC);

  /* Now reinitialize the controller */
  brake_sendack(155, "5136");		/* Identify: Unit 16, Group 20 */
  brake_sendack(185, "1");		/* Protocol: 8-bit ASCII */
  brake_sendack(186, "0");		/* Serial Interface: RS-232 */
  brake_sendack(174, "576");		/* Baud rate: 57600 */
  brake_sendack(173, "0");		/* Ack delay = 0 */
  brake_sendack(148,			/* Control Constants */
	"0 6 6 20 20 200 500");		/* see p28 in user manual for order */
  brake_sendack(237, "0");		/* Gravity offset contanst = 0 */
  brake_sendack(184, "0");		/* Direction */
  brake_sendack(149,			/* Torque limits */
	"30000 30000 30000 30000");
  brake_sendack(150, "10 4");		/* Anti-hunt constants */
  brake_sendack(230, "1250");		/* Anti-hunt delay */
  brake_sendack(195, "32767");		/* S-curve factor */
  brake_sendack(212, "10");		/* Low voltage trip */
  brake_sendack(213, "52");		/* Over voltage trip */
  brake_sendack(252, "0 83");		/* Digital input filter */

  /* Original values for parameters */
  brake_sendack(151, "500 200 120");
  brake_sendack(167, "128 0");

# ifdef UNUSED
  /* Set up motor conditions to allow continuous operation */
  brake_sendack(151, "20000 20000 100");/* move/hold error limits [big] */
  brake_sendack(225, NULL);		/* Enabled multi-tasking */
  brake_sendack(167, "0 0");		/* Kill motor conditions = none */
  brake_sendack(182, NULL);		/* Kill enabled driver */
# endif

  /* Note: default program will just blink the LED */
  /* TODO: enter a program that does something better than this */

  /* Home the brake */
  brake_home();

  /* Reset the internal position counter to zero */
  brake_sendack(BRAKE_ZTP, NULL);

  return 0;
}



/////////////////////////////////////////////////////////////////////
/*
 * brake_reset
 * 
 * Reset the brake after a failure 
 *
 */
int brake_reset() {
  brake_sendcmd(BRAKE_RST, NULL); usleep_for(BRAKE_RESTART_USEC);
  dbg_info("Brake reset OK");
  return 0;
}


int brake_zero(){
  return brake_home();
}


/////////////////////////////////////////////////////////////////////
/*
 * brake_home()
 *
 * Using the digital pot, move the actuator until it is in the home
 * position.  This home position is defined by the constant BRAKE_HOME,
 * which should indicate the value of the pot when we are at the desired
 * location.  We assume that this function is called in a thread safe
 * way.
 *
 */
int brake_home()
{
  int tries = 0;
  double curpos;

  /* Read the current position of the brake */
  if (brake_read(&curpos) < 0) return -1;
  dbg_info("brake pos = %g", curpos);

  if (curpos < BRAKE_HOME) {
    /* Set the velocity of the brake to a positive value */
    brake_sendack(15, "%0.f %0.f 0 0", -brake_maxacc, -0.1*brake_maxvel);
    while (brake_read(&curpos) >= 0 && curpos < BRAKE_HOME && tries++ < 100) {
      usleep_for(BRAKE_CYCLE_USEC);	/* let the brake move a bit */
    }
  } else {
    /* Set the velocity of the brake to a positive value */
    brake_sendack(15, "%0.f %0.f 0 0", brake_maxacc, 0.1*brake_maxvel);
    while (brake_read(&curpos) >= 0 && curpos > BRAKE_HOME && tries++ < 100) {
      usleep_for(BRAKE_CYCLE_USEC);	/* let the brake move a bit */
    }
  }
  /* Stop the actuator and check to make sure things are working */
  if (brake_sendack(15, "%0.f 0 0 0", brake_maxacc) < 0 || tries >= 100)
    return -1;

  /* Mark this position as zero */
  brake_sendack(BRAKE_ZTP, NULL);

  return 0;
}






/*
 * brake_setmav(abs_pos, maxvel, maxacc)
 *
 * Set the position of the brake using a trapezoidal profile and the
 * internal counter.  This function directly commands the brakes
 * positions by sending the appropriate commands to the actuator.  The
 * position, velocity, and acceleration are scaled from 0 to 1.
 * 
 */
int brake_setmav(double pos, double vel, double acc)
{
  double cmdpos, maxvel, maxacc;
  char cmdstr[256];			/* buffer for commands */

  /* Convert brake position to absolute counts */
  cmdpos = (pos * ((double) (brake_maxpos_counts - brake_minpos_counts))) + brake_minpos_counts;
  //WNH 2-28-04 fixed math error that would result in cmdpos > brake_maxpos_counts for pos = 1
  //also added the _counts subscript to eliminate confusion 
  maxvel = vel * ((double) brake_maxvel);
  maxacc = acc * ((double) brake_maxacc);

  /* Send out the command */
  dbg_info("MAV pos=%.0f, vel=%.0f, acc=%.0f", cmdpos, maxvel, maxacc);
  brake_sendack(BRAKE_MAV, "%.0f %.0f %.0f 0 0", cmdpos, maxacc, maxvel);
  
  /* Ignore return status from brake driver for now */
  return 0;
}

/*
 * brake_servo(enablep)
 * 
 * This function is designed to be called as a thread that will 
 * continuously control the velocity of the brake until it reaches
 * a desired position.
 *
 */

void *brake_servo(void *flag) {
  int *brake_flag = (int *) flag;
  int status, statcount = 0;
  void *exit_val = 0;			/* return status from thread */
  double curpos;			/* current brake position */

  brake_count = 0;			/* initialize brake counter */

  /* Check the brake status to see if we are in an error condition */
  if (brake_port < 0 || brake_status(&status) < 0) {
    dbg_error("brake status error");
    brake_count = -1;

    /* See if we can reset the condition */
    if (brake_port < 0) {
      /* User should open the port before calling this function */
      dbg_panic("brake port not open");
      return NULL;
    }

    /* Reset the brake and make sure it took */
    brake_sendcmd(BRAKE_RST, NULL); sleep(BRAKE_RESTART_USEC);
    if (brake_status(&status) < 0) {
      /* Something is still wrong; abort the thread */
      /* TODO: need logic in vdrive to try restarting more aggressively */
      return NULL;
    }
  } 

  /*
   * Main servo loop
   *
   * This loop takes the commands stored in the buf_* variables (set
   * by brake_write), scales them into proper units, and then commands
   * the motion of the actuator.  To minimize writes to the actuator,
   * we use the variable brake_case to keep track of what we did in
   * the last iteration:
   *
   *    brake_case = -2			brake loop disabled
   *    brake_case = -1			moving backwards
   *    brake_case = 0			stopped
   *    brake_case = 1			moving forwards
   *    brake_case = 2			re-send brake command
   *
   */
  double last_cmdvel, last_cmdacc;	/* keep track of vel, acc changes */
  brake_case = 2;			/* initialize brake_state */

  while (1) {
    /* Check to make sure we should still be running */
    if (*brake_flag == -1) pthread_exit(exit_val);

    /* Copy buffered variables into actual variables */
    buf_lock = 1;			/* insure exclusive access */
    brake_cmdpos = brake_scale(brake_in_range(buf_cmdpos, 0, 1), 
			       0, 1, brake_min_pot, brake_max_pot);
    brake_cmdvel = brake_scale(brake_in_range(buf_cmdvel, 0, 1), 
			       0, 1, BRAKE_MINVEL, brake_maxvel);
    brake_cmdacc = brake_scale(brake_in_range(buf_cmdacc, 0, 1), 
			       0, 1, BRAKE_MINACC, brake_maxacc);

    buf_lock = 0;			/* release exclusive access */

    statcount = 0;			/* reset status counter */
    if (brake_read(&curpos) < 0) {
      /* Can't read the brake position; something is wrong */
      dbg_panic("can't read brake position");
      brake_count = -2;

      /* TODO: figure out what do do in this case */
      /* For now, just try 100 times, then abort */
      if (++statcount < 100) {
	usleep_for(BRAKE_CYCLE_USEC);	/* sleep for 100 ms */
	continue;
      } else 
	*brake_flag = 0;		/* disable brake loop */
    }

    /* Check to see if acceleration or velocity commands have changed */
    if (last_cmdvel != brake_cmdvel || last_cmdacc != brake_cmdacc)
      brake_case = 2;			/* re-command motion */
    last_cmdvel = brake_cmdvel; last_cmdacc = brake_cmdacc;

    /* Re-enable the driver, just in case it is tripped */
    if (brake_case == 2) brake_sendack(227, NULL);

    if (brake_enabled) {

      /* Check to see if we need to move and in what direction */
      if((brake_pot_current - brake_cmdpos) > BRAKE_POS_TOLERANCE &&
	 (brake_pot_current > brake_min_pot)) {  
	// TBD: dont really need the last because the inout is already scaled
	/* send brake a negative velocity move, unless it is already moving */
	if (brake_case != 1) {
	  status = brake_sendack(15, "%.0f %.0f %d %d ",
				 brake_cmdacc, brake_cmdvel,
				 BRAKE_STOP_ENABLE_ABS, BRAKE_STOP_STATE_ABS);
	  if (status < 0) { brake_count = -3; *brake_flag = 0; continue; }
	}
	brake_case = 1;			/* keep track of direction of motion */

      } else if (((brake_cmdpos - brake_pot_current) > BRAKE_POS_TOLERANCE) && 
		 (brake_pot_current < brake_max_pot)) {
        // send brake a positive move, unless it is already moving
	if (brake_case != -1) {
	  status = brake_sendack(15, "%.0f %.0f %d %d ", brake_cmdacc,
				 -brake_cmdvel, BRAKE_STOP_ENABLE_ABS, 
				 BRAKE_STOP_STATE_ABS);

	  if (status < 0) { brake_count = -3; *brake_flag = 0; continue; }
	}
	brake_case = -1;		/* keep track of direction of motion */

      } else {
        //send brake stop if it is either close enough or out of range
	if (brake_case != 0) {
	  status = brake_sendack(15, "%.0f %.0f %d %d ", brake_cmdacc, 0.0,
				 BRAKE_STOP_ENABLE_ABS, BRAKE_STOP_STATE_ABS);
	  
	  if (status < 0) { brake_count = -3; *brake_flag = 0; continue; }
	}
	brake_case = 0;			/* keep track of direction of motion */
      }

    } else {
      /* Brake has been disabled; stop moving */
	  status = brake_sendack(15, "%.0f %.0f %d %d ", brake_cmdacc, 0.0,
				 BRAKE_STOP_ENABLE_ABS, BRAKE_STOP_STATE_ABS);
      brake_case = -2;			/* tracking flag */
    }
    ++brake_count;
    /* TODO: change this to usleep_for */
    usleep_for(BRAKE_CYCLE_USEC);			/* sleep for 10 msec */
  }
  return NULL;
}

/*
 * brake_status(&status)
 *
 * Read the brake status.  Returns 0 if all conditions OK, -1 otherwise.
 * If the argument is non-null, it stores the full status word there.
 *
 */
static int brake_procstat(char *, int *);

int brake_status(int *psw) { return brake_status(psw, NULL, NULL, NULL); }
int brake_status(int *psw, int *rio, int *cks, int *pos)
{
  char *response;
  int errflag = 0;

  /* Check the polling status word */
  response = brake_sendresp(BRAKE_POL, NULL);
  errflag += (brake_procstat(response, psw) < 0 ? 1 : 0);

  /* Check the I/O status word */
  response = brake_sendresp(BRAKE_RIO, NULL);
  errflag += (brake_procstat(response, rio) < 0 ? 1 : 0);

  /* Check the internal status word */
  response = brake_sendresp(BRAKE_CKS, NULL);
  errflag += (brake_procstat(response, cks) < 0 ? 1 : 0);

  /* Get the brake position */
  response = brake_sendresp(12, "1");
  errflag += (brake_procstat(response, pos) < 0 ? 1 : 0);

  return errflag < 0 ? -1 : 0;
}

/* Utility function for processing status commands */
static int brake_procstat(char *response, int *status)
{
  int ibuf;

  if (response == NULL) return -1;	/* error for brake_sendresp */

  switch (response[0]) {
  case '*':				/* status is all zero */
    ibuf = 0;
    break;

  case '#':				/* status returned as response */
    sscanf(response, "# 10 %*x %x", &ibuf);
    break;

  case '!':
  default:
    return -1;				/* something went wrong */
  }

  if (status != NULL) *status = ibuf;
  return 0;
}

/*
 * brake_clrpsw(mask)
 * 
 * Clear the polling status register.
 *
 */

int brake_clrpsw(int mask)
{
  return brake_sendack(1, "%d", mask);
}


/*
 * Utility functions
 * 
 * These functions are used by the driver to access the brake and carry
 * out routine computations.
 *
 * brake_sendcmd(cmd, fmt, ...)	send a command to the brake (no response)
 * brake_sendack(cmd, fmt, ...)	send command and wait for acknowledge
 * brake_sendrsp(cmd, fmt, ...)	send command and wait for response
 * brake_response()		get response from brake
 *
 * Note: these functions are NOT thread aware.  This should be OK since
 * we shouldn't have more than one thread trying to control the actuator
 * anyway (since the serial port is not thread aware...).
 *
 */

static char parameters[1024];		/* buffer for parameters */
static char command[1024];		/* buffer for command */
static char response[1024];		/* responds from brake */

/* Send a command to the brake and don't wait for return code */
int brake_sendcmd(int cmd, char *fmt, ...)
{
  va_list arg_ptr;

  /* Check to make sure things are initialized */
  if (brake_port < 0) return -1;

  /* Generate the parameter string */
  if (fmt != NULL) {
    va_start(arg_ptr, fmt);
    vsprintf(parameters, fmt, arg_ptr);
    va_end(arg_ptr);
  } else
    parameters[0] = '\0';		/* no arguments */

  /* Generate the command string and send it to the brake */
  sprintf(command, "@%d %d %s\r", BRAKE_ID, cmd, parameters);
  dbg_info("sendcmding: %s", command);
# ifdef SERIAL2
  return serial_write(brake_port, command, strlen(command));
# else
  return serial_write(brake_port, command, strlen(command), SerialBlock);
# endif
}

/* Send a command to the brake and wait for ACK */
int brake_sendack(int cmd, char *fmt, ...)
{
  va_list arg_ptr;
  int status;

  /* Check to make sure things are initialized */
  if (brake_port < 0) return -1;

  /* Generate the parameter string */
  if (fmt != NULL) {
    va_start(arg_ptr, fmt);
    vsprintf(parameters, fmt, arg_ptr);
    va_end(arg_ptr);
  } else
    parameters[0] = '\0';		/* no arguments */

  /* Generate the command string and send it to the brake */
  sprintf(command, "@%d %d %s\r", BRAKE_ID, cmd, parameters);
  dbg_info("sendacking: %s", command);
# ifdef SERIAL2
  status = serial_write(brake_port, command, strlen(command));
# else
  status = serial_write(brake_port, command, strlen(command), SerialBlock);
# endif
  if (status < 0) return -1;

  /* Get the response and make sure it looks OK */
  brake_response();
  switch (response[0]) {
  case '*':				/* ACK */
    break;
    /* Check to make sure the next character is correct */
    if (strncmp(response+2, "10", 2) != 0) {
      dbg_error("didn't get ack; received %s", response);
      return -1;
    }
    break;

  case '#':				/* data return */
  case '!':				/* NACK */
  default:				/* something wrong */
    dbg_error("bad response code: received %s", response);
    return -1;
  }
  return 0;
}

char *brake_sendresp(int cmd, char *fmt, ...)
{
  va_list arg_ptr;
  int status;

  /* Check to make sure things are initialized */
  if (brake_port < 0) return NULL;

  /* Generate the parameter string */
  if (fmt != NULL) {
    va_start(arg_ptr, fmt);
    vsprintf(parameters, fmt, arg_ptr);
    va_end(arg_ptr);
  } else
    parameters[0] = '\0';		/* no arguments */

  /* Generate the command string and send it to the brake */
  sprintf(command, "@%d %d %s\r", BRAKE_ID, cmd, parameters);
  dbg_info("sendresping: %s", command);
# ifdef SERIAL2
  status = serial_write(brake_port, command, strlen(command));
# else
  status = serial_write(brake_port, command, strlen(command), SerialBlock);
# endif
  if (status < 0) return NULL;

  /* Get the response and make sure it looks OK */
  return brake_response();
}

/*
 * brake_response()
 *
 * Return the latest response from the brake.
 *
 */
char *brake_response() { 
  int len;

  /* Reset the response buffer in case we timeout */
  response[0] = '\0';

  /* Read from the serial port */
# ifdef SERIAL2
  len = serial_read_until(brake_port, response, '\n', sizeof(response),
			  Timeval((int) brake_timeout, 0));
# else
  len = serial_read_until(brake_port, response, '\n', brake_timeout, 
		    sizeof(response));
# endif

  /* Get rid of trailing spaces */
  while (len > 0 && isspace(response[len-1])) response[--len] = '\0';

  dbg_info("response [%d]: %s", len, response);
  return response;
}


#endif












////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//   NEW BRAKE ACTUATOR

#ifndef BRAKE_OLD

////////////////////////////////////////////////////////////////////////////////
//  Helper Function Declarations

int brake_sendcmd(char *);
int brake_response();
int brake_sendresp(char *);
int brake_move_vel(double);

////////////////////////////////////////////////////////////////////////////////
//  Function Definitions


//////////////////////////////////////////////////////////////////////
/*
 * brake_open(port)
 *
 * Initialized the connection to the brake actuator across the specified
 * serial port.  The brake also makes use of information that is passed
 * via the throttle parallel port, which is assumed to be properly 
 * initialized.
 *
 */
int brake_open(int port)
{
  /* Close the current brake port if open */
  if (brake_port != -1) brake_close();

  /* Open the serial device for the brake */
  if (serial_open_advanced(port, BRAKE_BAUD1,
			   SR_PARITY_DISABLE | SR_SOFT_FLOW_DISABLE | 
			   SR_HARD_FLOW_DISABLE | SR_ONE_STOP_BIT) != 0) {
    dbg_info("Unable to open com port %d" , port);
    return -1;
  } 
  brake_port = port;

  //see if we have communication with the actuator
  if ( brake_sendresp("RS") == -1 ){
    dbg_info("No response from brake controller");
    return -1;
  }
  
#ifdef HIGH_SPEED
  dbg_info("Setting Brake Serial Baud to 38400");
  brake_sendcmd("BAUD38400"); //Makes the actuator run at 38400 baud.  Note that if open 
  //has already been called (say before another brake_close), the brake may already be
  //running at 38400 baud.  However, this is not a problem because it will accept commands 
  //from a lower baud port.

  if (serial_close(port) != 0) { // need to close port to open again at 38400 baud
    dbg_info("Unable to close com port %d" , port);
    return -1; 
  }

  dbg_info("Brake port colsed sucessfully");
  /* Open the serial device for the brake again at the final speed*/
  if (serial_open_advanced(port, BRAKE_BAUD2,
                           SR_PARITY_DISABLE | SR_SOFT_FLOW_DISABLE |
                           SR_HARD_FLOW_DISABLE | SR_ONE_STOP_BIT) != 0) {
    dbg_info("Unable to open com port %d" , port);
    return -1;
  }

  //see if we have communication with the actuator
  if ( brake_sendresp("RS") == -1 ){
    dbg_info("No response from brake controller");
    return -1;
  }
  
  dbg_info("Brake comm. restored");
#endif    /* HIGH_SPEED */
  
  //  dbg_info("Setting limit switches to directional mode");
  //brake_sendcmd("LIMD"); //makes the limit switches directional, ie if you hit a limit switch, you can 
  //move back the other direction, but not further that way
  dbg_info("Changing brake moving error maximum value");
  brake_sendcmd("E=32000"); //sets the moving error to its maximum value so that we dont fault out
  //moving as fast as we do

  /* Brake initialization: set up parameters */
  brake_minpos_counts = 0; brake_maxpos_counts = BRAKE_MAXPOS_COUNTS;
  //WNH added the counts suffix to eliminate confusion
  brake_cmdvel = brake_maxvel = BRAKE_MAXVEL;

  dbg_info("setting brake_maxacc to %d", BRAKE_MAXACC);
  brake_maxacc = BRAKE_MAXACC;
  dbg_info("brake_maxacc: %f", brake_maxacc);

  dbg_info("Switching to velocity control mode");
  brake_sendcmd("MV"); //puts the brake in velocity move mode
  
  char accelstr[128];  //sets the acceleration to use for moves
  dbg_info("Setting max acceleration for brake actuator");
  sprintf(accelstr, "A=%d",(int) brake_maxacc); 
  brake_sendcmd(accelstr);

  //  dbg_info("Sending brake to zero position");
  //brake_zero();

  return 0;
}


//////////////////////////////////////////////////////////////////////
// brake_zero
//
// moves the brake to the extended limit switch, and then sets the
// minimum pot value after a read, in effect calibrating the lower
// limit for the brake.
//
int brake_zero(){

  //  brake_move_vel(-brake_maxvel);

  //  usleep_for(3000000);  //wait for brake to home
  
  // brake_read(&brake_min_pot);  //calibrate as the new minimum value
  //to send the brake to.  
  
  return 0;
}

int brake_home(){
  return brake_zero();
};

//////////////////////////////////////////////////////////////////////
/*
 * brake_calibrate()
 *
 * Calibrate the brake actuator by initializing the servo and moving the
 * brake until the brake pot is at the minimum position.  This is not
 * really a true calibration since it doesn't do anything to make sure
 * that the brake pot is reading correctly (and we have reason to believe
 * that it drifts over time, but nothing critical).
 *
 */
int brake_calibrate()
{
 
  //  char velstr[128];

 
  //retract the brake until limit switch
  //  brake_move_vel(brake_maxvel);
  //  usleep_for(3000000);
  //  brake_read(&brake_max_pot);  //calibrate as the new maximum value
  //to send the brake to.  
  
  
  //extend brake until limit switch
  //brake_move_vel(brake_maxvel);
  //usleep_for(3000000);
  //brake_read(&brake_min_pot);  //calibrate as the new minimum value
  //to send the brake to.  

  return 0;
}


//////////////////////////////////////////////////////////////////////
//brake_reset
//
// Reset the brake after a failure.. doesn't do anything.  nothing to
// reset.  Just send a new command and it does it automatically
//
int brake_reset() {
  return 0;
}




//////////////////////////////////////////////////////////////////////
/*
 * brake_servo(enablep)
 * 
 * This function is designed to be called as a thread that will 
 * continuously control the velocity of the brake until it reaches
 * a desired position.
 *
 */

void *brake_servo(void *flag) {
  int *brake_flag = (int *) flag;
  int status, statcount = 0;
  void *exit_val = 0;			/* return status from thread */
  double curpos;			/* current brake position */

  brake_count = 0;			/* initialize brake counter */

  /* Check the brake status to see if we are in an error condition */
    /* See if we can reset the condition */
  if (brake_port < 0) {
    /* User should open the port before calling this function */
    dbg_panic("brake port not open");
    return NULL;
  }
  
  
  /*
   * Main servo loop
   *
   * This loop takes the commands stored in the buf_* variables (set
   * by brake_write), scales them into proper units, and then commands
   * the motion of the actuator.  To minimize writes to the actuator,
   * we use the variable brake_case to keep track of what we did in
   * the last iteration:
   *
   *    brake_case = -2			brake loop disabled
   *    brake_case = -1			moving backwards
   *    brake_case = 0			stopped
   *    brake_case = 1			moving forwards
   *    brake_case = 2			re-send brake command
   *
   */
  
  double last_cmdvel, last_cmdacc;	/* keep track of vel, acc changes */
  brake_case = 2;			/* initialize brake_state */
  
  while (1) {
    /* Check to make sure we should still be running */
    if (*brake_flag == -1) pthread_exit(exit_val);
    
    /* Copy buffered variables into actual variables */
    buf_lock = 1;			/* insure exclusive access */
    brake_cmdpos = brake_scale(brake_in_range(buf_cmdpos, 0, 1), 
			       0, 1, brake_min_pot, brake_max_pot);
    brake_cmdvel = brake_scale(brake_in_range(buf_cmdvel, 0, 1), 
			       0, 1, BRAKE_MINVEL, brake_maxvel);
    brake_cmdacc = brake_scale(brake_in_range(buf_cmdacc, 0, 1), 
			       0, 1, BRAKE_MINACC, brake_maxacc);

    buf_lock = 0;			/* release exclusive access */

    statcount = 0;			/* reset status counter */

    if (brake_read(&curpos) < 0) {
      /* Can't read the brake position; something is wrong */
      dbg_panic("can't read brake position");
      brake_count = -2;

      /* TODO: figure out what do do in this case */
      /* For now, just try 100 times, then abort */
      if (++statcount < 100) {
	usleep_for(BRAKE_CYCLE_USEC);	/* sleep for 100 ms */
	continue;
      } else 
	*brake_flag = 0;		/* disable brake loop */
    }
    
    /* Check to see if acceleration or velocity commands have changed */
    if (last_cmdvel != brake_cmdvel || last_cmdacc != brake_cmdacc){
      brake_case = 2;			/* re-command motion */
    }
    
    last_cmdvel = brake_cmdvel; last_cmdacc = brake_cmdacc;
    

    if (brake_enabled) {

      /* Check to see if we need to move and in what direction */

      if((brake_pot_current - brake_cmdpos) > BRAKE_POS_TOLERANCE) {  
	// send brake a negative velocity move, unless it is already moving 
	if (brake_case != 1) brake_move_vel(-brake_cmdvel);
	brake_case = 1;			/* keep track of direction of motion */
      } 
      
      else if ((brake_cmdpos - brake_pot_current) > BRAKE_POS_TOLERANCE) {
        // send brake a positive move, unless it is already moving
	if (brake_case != -1) brake_move_vel(brake_cmdvel);
	brake_case = -1;		/* keep track of direction of motion */
      } 

      else {
        //send brake stop if it is either close enough or out of range
	if (brake_case != 0) brake_move_vel(0);
	brake_case = 0;			/* keep track of direction of motion */
      }

    } 
    
    else {  /* Brake has been disabled; stop moving */
      brake_move_vel(0);
      brake_case = -2;			/* tracking flag */
    }
    ++brake_count;
    /* TODO: change this to usleep_for */
    usleep_for(BRAKE_CYCLE_USEC);			/* sleep for 10 msec */
  }
  return NULL;
}


////////////////////////////////////////////////////////////////////////////////
//Helper functions

/*
 * Utility functions
 * 
 * These functions are used by the driver to access the brake and carry
 * out routine computations.
 *
 * brake_sendcmd(fmt, ...)	send a command to the brake (no response)
 * brake_sendack(fmt, ...)	send command and wait for acknowledge
 * brake_sendrsp(fmt, ...)	send command and wait for response
 * brake_response()		get response from brake
 *
 * Note: these functions are NOT thread aware.  This should be OK since
 * we shouldn't have more than one thread trying to control the actuator
 * anyway (since the serial port is not thread aware...).
 *
 */

static char command[1024];		/* buffer for command */
static char response[1024];		/* responds from brake */


//////////////////////////////////////////////////////////////////////
// brake_sendcmd
//
// sends a command string to the brake motor
//
int brake_sendcmd(char *input)
{

  /* Check to make sure things are initialized */
  if (brake_port < 0) return -1;

  /* Generate the command string and send it to the brake */
  sprintf(command, "%s\n", input);
  dbg_info("sendcmding: %s", command);
# ifdef SERIAL2
  return serial_write(brake_port, command, strlen(command));
# else
  return serial_write(brake_port, command, strlen(command), SerialBlock);
# endif
}


/////////////////////////////////////////////////////////////////////
// brake_sendresp
//
// 
int brake_sendresp(char *command_str)
{
  if (brake_port < 0) return -1;

  // serial_clean_buffer(brake_port);  //make sure we have a clean buffer

  dbg_info("Sending command %s to brake", command_str);
  brake_sendcmd(command_str); //send the command
  dbg_info("Command sent, waiting for response");
  return brake_response();  //data is in response, return  >=0 if we
  //got a value, else, -1 if bad.
}


/////////////////////////////////////////////////////////////////////
// brake_response()
//
// Return the latest response from the brake.
//
int brake_response() { 
  int len;
  response[0] = '\0';   /* Reset the buffer in case timeout */
  
# ifdef SERIAL2   /* Read from the serial port */
  dbg_info("Reading");
  len = serial_read_until(brake_port, response, '\n', sizeof(response),
		      Timeval((int) brake_timeout, 0));
# else
  len = serial_read_blocking(brake_port, response, sizeof(response), brake_timeout);
# endif
  /* Get rid of trailing spaces */
  while (len > 0 && isspace(response[len-1])) response[--len] = '\0';  
  dbg_info("response [%d]: %s", len, response);
  return len;
}

/////////////////////////////////////////////////////////////////////
// brake_move_vel
//
int brake_move_vel(double velocity){
  char velstr[128];
  sprintf(velstr, "V=%d",(int)velocity);
  brake_sendcmd(velstr);
  brake_sendcmd("G");
}

#endif
