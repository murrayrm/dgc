// Frame Transformation Class
// Used to transform coordinates from a body-fixed frame to
// a fixed reference (nav) frame
// 
// Revision History
// H Huang 25 Jan 04
// Created

#include <math.h>
#include "frames.hh"

using namespace std;

// constructors
frames::frames()
{
  // since all XYZcoord and rot_matrix initialize to identity when
  // declared, nothing needs to be done
}

// frames(sensor offsets)
// creates frame transform already initialized with sensor offset
// rotation matrix for S2B and offset are initialized, vehicle origin is not 
frames:: frames(XYZcoord sensor_offset, double sensor_pitch, double sensor_roll, double sensor_yaw)
{
  // initialize rotation matrix with proper attitude angles
  S2B.set_b2n_attitude(sensor_pitch, sensor_roll, sensor_yaw);
  
  // offsets are what must be added to sensor coordinates to get body coords
  offset.x = sensor_offset.x;
  offset.y = sensor_offset.y;
  offset.z = sensor_offset.z;
}

// initFrames( sensor offsets)
// same thing as constructor above, essentially
void frames::initFrames(XYZcoord sensor_offset, double sensor_pitch, double sensor_roll, double sensor_yaw)
{
  // initialize rotation matrix with proper attitude angles
  S2B.set_b2n_attitude(sensor_pitch, sensor_roll, sensor_yaw);

  // offsets are what must be added to sensor coordinates to get body coords
  offset.x = sensor_offset.x;
  offset.y = sensor_offset.y;
  offset.z = sensor_offset.z;
  return;
}

// sets the body state of the vehicle to be used in transformations
// from vehicle's body-fixed frame to UTM frame
void frames::updateState(VState_GetStateMsg curstate)
{
  // intialize body to nav rotation matrix
  B2N.set_b2n_attitude(curstate.Pitch, curstate.Roll, curstate.Yaw);

  // initialize UTM offsets
  UTMoffset.x = curstate.Northing;
  UTMoffset.y = curstate.Easting;
  UTMoffset.z = curstate.Altitude;

  return;
}

// transform a given point in the sensor's coordinate frame to UTM frame
XYZcoord frames::transformS2N(XYZcoord point)
{
  XYZcoord result;
  
  // get point coordinates in body frame first
  result = transformS2B(point);

  // now convert from body to nav frame
  result = transformB2N(result);
  
  return result;
}

// transform a given point in the nav frame (UTM) to body frame
XYZcoord frames::transformB2N(XYZcoord point)
{
  XYZcoord result;

  // perform r otation out of body frame
  result = B2N * point;

  // now add UTM offset to get UTM coordinates
  result.x += UTMoffset.x;
  result.y += UTMoffset.y;
  result.z += UTMoffset.z;

  return result;
}

// transforms a given point in the sensor's view frame to the body frame
XYZcoord frames::transformS2B(XYZcoord point)
{
  XYZcoord result;
  //cout << " frames.cc: point: " << point.x << " " << point.y << " " 
  //    << point.z << endl;

  // first rotate the point out of the local frame
  result = S2B * point;
  //cout << " frames.cc: result before offset: " << result.x << " " 
  //     << result.y << " " << result.z << endl;

  //cout << " frames.cc: S2B matrix: ";
  //S2B.display();

  // now add offsets to get body frame coordinates 
  result.x += offset.x;
  result.y += offset.y;
  result.z += offset.z;
  
  return result;
}
