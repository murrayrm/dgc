//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// CR_OpenGL
//----------------------------------------------------------------------------

#ifndef CR_OPENGL_DECS

//----------------------------------------------------------------------------

#define CR_OPENGL_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <GL/glut.h>

#include "glui.h"

#include "CR_Matrix.hh"

// extern "C" {
// #include "CR_Fresnel.h"
// }

//-------------------------------------------------
// defines
//-------------------------------------------------


//-------------------------------------------------
// functions
//-------------------------------------------------

void initialize_opengl();

void print_opengl_info();

void ogl_draw_parametric_quadratic(double, double, double, double, double, double, double, double, double, double, double, int, int, int, int);
//void ogl_draw_clothoid(double, double, double, double, double, double, double, int, int, int, int);
//void ogl_draw_clothoid2(double, double, double, double, double, double, double, int, int, int, int);
//void ogl_draw_approx_clothoid(double, double, double, double, double, double, int, int, int, int);
//void ogl_draw_fresnel_spiral(double, double, double, int, double, double, int, int, int);
void ogl_draw_scaled_CR_Matrix(int, int, int, int, CR_Matrix *);
void ogl_draw_CR_Matrix(int, int, int, CR_Matrix *);
void ogl_draw_CR_Matrix(int, int, int, CR_Matrix *, int, int, int, int, int, int);
void ogl_draw_points_d(double *, double *, int, int, int, int, int);
void ogl_draw_line_loop_d(double *, double *, int, int, int, int);
void ogl_draw_thick_line_loop_d(double *, double *, int, int, int, int, int);
void ogl_draw_line_strip_d(double *, double *, int, int, int, int);
void ogl_draw_thick_line_strip_d(double *, double *, int, int, int, int, int);
void ogl_draw_lines_d(double *, double *, int, int, int, int);
void ogl_draw_line(int, int, int, int, int, int, int);
void ogl_draw_thick_line(int, int, int, int, int, int, int, int);
void ogl_draw_string(char *, int, int, int, int, int);
void ogl_draw_string2(char *, int, int, int, int, int);
void ogl_draw_rectangle(int, int, int, int, int, int, int);
void ogl_draw_quadrilateral(int, int, int, int, int, int, int, int, int, int, int);
void ogl_draw_cross(int, int, int, int, int, int);
void ogl_draw_line_strip(int *, int *, int, int, int, int);
void ogl_draw_intensity_points(int *, int *, int *, int, int, double);
void ogl_draw_point(int, int, int, int, int, int);
void ogl_dummy();

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif


