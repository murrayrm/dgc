//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_VanishingPoint.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

int do_planx_strip;
int do_opengl_voting;

//CR_Image *planx_im;      // decimated size (320 x 240 or 360 x 240)
CR_Image *planx_big_im;  // original size (640 x 480 or 720 x 480)
CR_Image *planx_scratch_im;

CR_Image *planx_voter_im; 
CR_Image *planx_voter_scratch_im;

CR_Matrix **planx_voter_dlm;

int planx_do_orientation_interpolation;
int planx_do_anisotropy;
int planx_do_strength;
int planx_do_point_line_dist;
int planx_do_no_inv_dist;
int planx_do_halfsize;  // decimate image for speed?  (720x480 -> 360x240)

int **planx_voter_max;  // for each wavelength, index of dominant orientation (DO) at pixel (x, y)
int **planx_voter_max2;  // for each wavelength, index of runner-up for dominant orientation (DO) at pixel (x, y)
CR_Matrix **planx_voter_interp_nx;
CR_Matrix **planx_voter_interp_ny;
CR_Matrix **planx_voter_strength;  // for each wavelength, filter response at DO at pixel (x, y), normalized by filter area
CR_Matrix **planx_voter_anisotropy;  // for each wavelength, fraction of total Gabor power of DO at pixel (x, y)
CR_Matrix **planx_voter_inv_strength;  
CR_Matrix **planx_voter_inv_anisotropy;  
double *planx_nx;     // components of unit vector pointing in direction of orientation
double *planx_ny;
double *planx_filter_area;

int planx_num_orientations;    // denoted "theta"
double planx_start_theta;      // first orientation
double planx_delta_theta;      // orientation interval (in degrees)

int planx_num_wavelengths;     // denoted "lambda"
double planx_start_lambda;     // first wavelength
double planx_delta_lambda;     // wavelength interval (1 = 1 octave)

int planx_num_filters;         // number of Gabor wavelet odd/even "pairs"
int planx_num_kernels;         // 2 x planx_num_filters

// DO(x,y) = dominant orientation at a pixel (x,y)
// VP(x,y) = direction to vanishing point from pixel (x,y)

extern int planx_w, planx_h;                  // raw image dimensions
extern float fplanx_w, fplanx_h;

int planx_voter_w, planx_voter_h;          // dimensions of subsampled image for voters
int planx_voter_left, planx_voter_right, planx_voter_top, planx_voter_bottom;
int planx_voter_dx, planx_voter_dy;
int planx_voter_strip_height;
int planx_voter_current_top;
CR_Matrix **planx_wavelength_voters;  // one entry for every voter
CR_Matrix *planx_vp_locations;   // rows are images, each pair of cols is (r, c) of vanishing point pixel (y=r-1,x=c-1) for a wavelength
CR_Matrix *planx_voter_slices;  // what angle relative to VP did voters come from?  
                                // rows=wavelengths, col pairs=slices: (1) yes votes in this slice, (2)total voters in this slice
int planx_num_voter_slices;   // how many slices of 180 degrees will voters be divided into?
double planx_voter_slices_delta_theta;  // thickness of each slice

int planx_candidate_w, planx_candidate_h;       // dimensions of subsampled image for VP candidates
int planx_candidate_left, planx_candidate_right, planx_candidate_top, planx_candidate_bottom;
int planx_candidate_dx, planx_candidate_dy;
int pf_candidate_delta;
double planx_prior_x;
double planx_prior_y;
CR_Matrix *planx_candidate_votes;
CR_Image *planx_candidate_votes_im;
CR_Matrix **planx_candidate_wavelength_votes;
CR_Image **planx_candidate_wavelength_votes_im;

double planx_cos_threshold;            // cosine over this number indicates angle b/t DO(x,y) & VP(x,y) less than angular resolution of filters

CR_Image *planx_gray_im, *planx_gray_fp_im, *planx_gray_fft_fp_im, *planx_odd_gabor_fp_im, *planx_even_gabor_fp_im;
CR_Image *planx_squared_odd_gabor_fp_im, *planx_squared_even_gabor_fp_im, *planx_complex_gabor_fp_im;
CR_Matrix **planx_gabor_kernel_matrix;
CR_Matrix **planx_gabor_fft_kernel_matrix;
CR_Matrix *planx_gabor_kernel_info;
CR_Image_Kernel **planx_gabor_kernel;
IplConvKernelFP ***planx_gabor_filter;
CR_Image ***planx_gabor_filter_fp_im;
CR_Image ***planx_gabor_filter_fft_fp_im;
double ***planx_gabor_filter_fft_in;
fftw_complex ***planx_gabor_filter_fft_out;
fftw_complex ***planx_gabor_filter_inv_fft_in;
double ***planx_gabor_filter_inv_fft_out;
fftw_plan **planx_gabor_filter_fft_plan;
fftw_plan **planx_gabor_filter_inv_fft_plan;
fftw_complex *many_planx_gabor_filter_inv_fft_in;
double *many_planx_gabor_filter_inv_fft_out;
fftw_plan many_inv_fft_plan;
CR_Image *planx_gabor_kernel_im;
char *planx_kernel_name;

CR_Image **planx_pyr_im, **planx_pyr_dx_im, **planx_pyr_dy_im, **planx_pyr_smoothed_im, **planx_pyr_littlesmoothed_im;
int *planx_pyr_w, *planx_pyr_h;
int planx_pyr_levels;
int planx_pyr_blocksize, planx_pyr_blockframe, planx_pyr_blockarea;
int planx_pyr_svd_dx, planx_pyr_svd_dy;
double planx_pyr_voter_R_value_threshold;
CR_Matrix **planx_pyr_voter_dx, **planx_pyr_voter_dy, **planx_pyr_voter_R_value, **planx_pyr_voter_gradient_magnitude;
CR_Matrix *planx_pyr_M, *planx_pyr_U, *planx_pyr_VT;
double *planx_pyr_S;

CR_Condensation *planx_con;
CR_Matrix *planx_cov;
CR_Matrix *planx_con_samps;

double *image_fft_in, *image_fft_inv_out, *image_fft_inv_out2;
fftw_complex *image_fft_out, *image_fft_out2;
fftw_plan image_fft_plan, image_fft_inv_plan, image_fft_inv_plan2;
int fft_planx_w;   // the limit used for the complex domain

//int gabor_filters_initialized = FALSE;

float *vp_votes;
float vote_weight;
int vote_line_length;
float *planx_nx_vote_line;     // line extending across image in direction of orientation
float *planx_ny_vote_line;

int planx_max_x, planx_max_y;
float planx_max;

extern int vehicle_steering_vote;
extern double vehicle_steering_goodness;

//----------------------------------------------------------------------------

//  glClear(GL_COLOR_BUFFER_BIT);

// optimize computations in inner loop
//     how fast is this with a vertex array?
// don't actually draw to screen (no swapbuffers, window-opening)
//     a grayscale image buffer would probably be faster (1 float instead of 3 per pixel)
//     in the meantime, try overlaying on raw image to verify correctnesss
// read buffer into accessible memory and find max/access with particle filter condprob
// make sure we don't saturate: colors > 1.0 get clamped (is there a way to avoid the clamping?)

void compute_vp_votes(CR_Image *im)
{
  int i, r, c, i_max, x, y;
  float vx, vy, hvy;
  float nx, ny, x1, y1, x2, y2;

  glClear(GL_COLOR_BUFFER_BIT);
  glEnable(GL_BLEND);

  // the vote totals are all zero if the color components are < 0.004...what the f**k???

  //  glColor3f(vote_weight, vote_weight, vote_weight);
  //  glColor3f(0.004, 0.004, 0.004);
  glColor3f(0.003925, 0.003925, 0.003925);
  //  glColor3f(0.005, 0.005, 0.005);
  //glColor3f(0.0025, 0.0025, 0.0025);
  glPointSize(1);

  i = 0;

  glBegin(GL_LINES);

  for (vy = planx_voter_top, r = 0; vy < planx_voter_bottom; vy += planx_voter_dy, r++)
    for (vx = planx_voter_left, c = 0; vx < planx_voter_right; vx += planx_voter_dx, c++) {

      i_max = planx_voter_max[0][i++];

      // change the orientation set-up to get rid of this
      if (i_max > 1 && i_max < planx_num_orientations - 2) {

	if (!do_opengl_voting) {
	  votes_midpoint_line(vx, fplanx_h - vy, vx + planx_nx_vote_line[i_max], fplanx_h - vy + planx_ny_vote_line[i_max], 
			      0.004, vp_votes, planx_w, planx_h);
	}
	else {
	  glVertex2f(vx, fplanx_h - vy);
	  glVertex2f(vx + planx_nx_vote_line[i_max], fplanx_h - vy + planx_ny_vote_line[i_max]);
	}
      }

    }

  glEnd();


  // read it back

  if (do_opengl_voting)
    glReadPixels(0, 0, im->w, im->h, GL_RED, GL_FLOAT, vp_votes);

  //  for (i = 0; i < im->w * im->h; i++)
  for (y = 0, i = 0, planx_max = 0; y < im->h; y++)
    for (x = 0; x < im->w; x++) {
      if (vp_votes[i] > planx_max) {
	planx_max = vp_votes[i];
	planx_max_x = x;
	planx_max_y = y;
      }
      i++;
    }
  //    printf("%f\n", vp_votes[i]);
  //  printf("%f at (%i, %i)\n", planx_max, planx_max_x, planx_max_y);

  glDisable(GL_BLEND);
}

//----------------------------------------------------------------------------

// combine void strip_pf_where_is_the_VP_PlanX_grab_CR_CG(CR_Image *im)
// and void strip_pf_tracking_movie_PlanX_grab_CR_CG(CR_Image *im)
// divide base strip height by shrink factor

// implement moving strip version--after first strip

void compute_moving_vp_votes(int current_voter_top, CR_Image *im)
{
  int i, r, c, i_max;
  float vx, vy, hvy;
  float nx, ny, x1, y1, x2, y2;

  glClear(GL_COLOR_BUFFER_BIT);
  glEnable(GL_BLEND);

  // the vote totals are all zero if the color components are < 0.004...what the f**k???

  glColor3f(0.004, 0.004, 0.004);
  glPointSize(1);

  i = 0;

  glBegin(GL_LINES);

  // if current top == strip height, do the whole rectangle
  // else: just subtract votes of bottom row of last voter strip 
  // and add votes of top row of new rect

  for (vy = current_voter_top, r = 0; vy < current_voter_top + planx_voter_strip_height; vy += planx_voter_dy, r++)
    for (vx = planx_voter_left, c = 0; vx < planx_voter_right; vx += planx_voter_dx, c++) {

      i_max = planx_voter_max[0][i++];

      // change the orientation set-up to get rid of this
      if (i_max > 1 && i_max < planx_num_orientations - 2) {
	glVertex2f(vx, fplanx_h - vy);
	glVertex2f(vx + planx_nx_vote_line[i_max], fplanx_h - vy + planx_ny_vote_line[i_max]);
      }

    }

  glEnd();

  // read it back

  glReadPixels(0, 0, im->w, im->h, GL_RED, GL_FLOAT, vp_votes);

  glDisable(GL_BLEND);
}

//----------------------------------------------------------------------------

// should have some notion of confidence--variance of particle distribution?

void draw_vp_particlefilter(CR_Image *im)
{
  int i;

  // samples

  glColor3ub(255, 255, 0);
  glPointSize(2);

  glBegin(GL_POINTS);

  for (i = 0; i < planx_con->n; i++) 
    glVertex2f(planx_con->s[i]->x[0], planx_con->s[i]->x[1]);

  glEnd();

  // state

  glColor3ub(0, 0, 0);
  glPointSize(7);

  glBegin(GL_POINTS);
  
  glVertex2f(planx_con->x->x[0], planx_con->x->x[1]);

  glEnd();

  glColor3ub(0, 255, 0);
  glPointSize(5);

  glBegin(GL_POINTS);
  
  glVertex2f(planx_con->x->x[0], planx_con->x->x[1]);

  glEnd();



  // max
  /*
  glColor3ub(0, 0, 0);
  glPointSize(7);

  glBegin(GL_POINTS);
  
  glVertex2f(planx_max_x, planx_max_y);

  glEnd();

  glColor3ub(0, 0, 255);
  glPointSize(5);

  glBegin(GL_POINTS);
  
  glVertex2f(planx_max_x, planx_max_y);

  glEnd();
  */

}

//----------------------------------------------------------------------------

void update_vp_particlefilter()
{
  int i;
  double diff;

  // update particle filter
  
  update_CR_XCondensation(NULL, planx_con);

  // linearly map x <= 0 to vote = 0 and x >= planx_w - 1 to vote = 24
 
  vehicle_steering_vote = myround(24.0 * (planx_con->x->x[0] / fplanx_w));

  // empirically, < 5.0 is good for a 160-pixel wide image, > 10 is not so good
  for (i = 0, diff = 0; i < planx_con->n; i++) 
    diff += planx_con->pi->x[i] * SQUARE(planx_con->s[i]->x[0] - planx_con->x->x[0]);
  diff = sqrt(diff);
  vehicle_steering_goodness = 100.0 * exp(-10.0 * diff / fplanx_w);  // should be proportional to spread of particle distribution

  //  printf("goodness = %lf (diff = %lf)\n", vehicle_steering_goodness, diff);

  /*    
  for (i = 0; i < planx_con->n; i++) {
    MATRC(planx_con_samps, 0, 3*i) = planx_con->s[i]->x[0];
    MATRC(planx_con_samps, 0, 3*i+1) = planx_con->s[i]->x[1];
    MATRC(planx_con_samps, 0, 3*i+2) = planx_con->pi->x[i];
  }
  */

  //  write_dlm_CR_Matrix(fp, planx_con_samps);
  //  fflush(fp);

  // end particle filter
}

//----------------------------------------------------------------------------

void reset_vp_particlefilter()
{
  planx_prior_x = 0.5 * (double) pf_candidate_delta;
  planx_prior_y = 0.5 * (double) pf_candidate_delta;

  reset_CR_XCondensation(planx_con);
}

//----------------------------------------------------------------------------


void initialize_vp_particlefilter()
{
  FILE *fp;
  int i, num_samps;

  // initialize particle filter

  pf_candidate_delta = 10;

  num_samps = planx_candidate_w * planx_candidate_h / (SQUARE(pf_candidate_delta));
  printf("num samps = %i (w = %i, h = %i, del = %i)\n", num_samps, planx_candidate_w, planx_candidate_h, pf_candidate_delta);

  // var = sigma^2, 95% within 2*sigma
  // horizontal: 2*sigma = planx_w / 40 => sigma = planx_w / 80 
  // vertical: 2*sigma = planx_w / 57 => sigma = planx_w / 114

  planx_cov = make_all_CR_Matrix("planx_cov", 2, 2, 0.0);
  MATRC(planx_cov, 0, 0) = SQUARE(planx_w / 80);
  MATRC(planx_cov, 1, 1) = SQUARE(planx_w / 114);
  //  planx_cov = make_diagonal_all_CR_Matrix("planx_cov", 2, 10.0);
  // planx_cov = init_diagonal_CR_Matrix("planx_cov", 10.0, 10.0);

  planx_prior_x = 0.5 * (double) pf_candidate_delta;
  planx_prior_y = 0.5 * (double) pf_candidate_delta;

  planx_con = make_CR_XCondensation(num_samps, make_CR_Vector("planx_state", 2), planx_cov,
				    PlanX_condprob_zx, PlanX_samp_prior, PlanX_samp_state, PlanX_dyn_state);
  planx_con_samps = make_CR_Matrix("con_samps", 1, 3*planx_con->n);


  //  fp = fopen(planx_avi_pf_samples_filename, "w");

  for (i = 0; i < planx_con->n; i++) {
    MATRC(planx_con_samps, 0, 3*i) = planx_con->s[i]->x[0];
    MATRC(planx_con_samps, 0, 3*i+1) = planx_con->s[i]->x[1];
    MATRC(planx_con_samps, 0, 3*i+2) = 1.0/(double)planx_con->n;
  }
  //  write_dlm_CR_Matrix(fp, planx_con_samps);
  //  fflush(fp);

  // end initialize particle filter

}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// double planx_prior_x = 5;
// double planx_prior_y = 5;

// where the particles start

void PlanX_samp_prior(CR_Vector *samp)
{
  double x, y, delta;

  delta = (double) pf_candidate_delta;

  //  printf("%lf %lf\n", planx_prior_x, planx_prior_y);
  //exit(1);

  // put in uniform distribution over image

  if (planx_prior_x >= planx_candidate_w) {
    planx_prior_x = delta/2;
    planx_prior_y += delta;
  }

  samp->x[0] = planx_prior_x;
  samp->x[1] = planx_prior_y;

  planx_prior_x += delta;

    /*
  for (y = delta/2; y < h; y += delta)
    for (x = delta/2; x < w; x += delta) {
      samp->x[0] = x;
      samp->x[1] = y;
    }
    */
  //  printf("leaving samp prior\n");
  //  fflush(stdout);
}

//----------------------------------------------------------------------------

// random diffusion

void PlanX_samp_state(CR_Vector *old_samp, CR_Vector *new_samp)
{
  // nuthin' fancy...

  sample_gaussian_CR_Matrix(planx_con->Covalpha, new_samp, old_samp, planx_con->Covsqrt);
}

//----------------------------------------------------------------------------

// deterministic drift

void PlanX_dyn_state(CR_Vector *old_samp, CR_Vector *new_samp)
{
  // identity transform for now

  copy_CR_Vector(old_samp, new_samp);
}

//----------------------------------------------------------------------------

// bigger is better :)

//int counter = 0;

double PlanX_condprob_zx(void *Z, CR_Vector *samp)
{
  //  printf("%i: %lf %lf\n", counter++, samp->x[0], samp->x[1]);

  //  return pf_PlanX_vote(samp->x[0], samp->x[1], planx_voter_top, planx_voter_bottom, 2);  // last argument is wavelength number--more funky tricks later :)

  //  return pf_PlanX_vote(samp->x[0], samp->x[1], planx_voter_top, planx_voter_bottom, planx_num_wavelengths - 1);  // last argument is wavelength number--more funky tricks later :)

  return gl_pf_PlanX_vote(samp->x[0], samp->x[1], planx_voter_top, planx_voter_bottom, planx_num_wavelengths - 1);  // last argument is wavelength number--more funky tricks later :)
}

//----------------------------------------------------------------------------

// bigger is better :)

//int counter = 0;

double PlanX_strip_condprob_zx(void *Z, CR_Vector *samp)
{
  return pf_PlanX_vote(samp->x[0], samp->x[1], planx_voter_current_top, planx_voter_current_top + planx_voter_strip_height, 2);  
  //  return 
  //   pf_PlanX_vote(samp->x[0], samp->x[1], planx_voter_current_top, planx_voter_current_top + planx_voter_strip_height, 1) +
  //   pf_PlanX_vote(samp->x[0], samp->x[1], planx_voter_current_top, planx_voter_current_top + planx_voter_strip_height, 2);  
}

//----------------------------------------------------------------------------

double myround(double x)
{
  if (x - floor(x) >= 0.5)
    return ceil(x);
  else
    return floor(x);
}

//----------------------------------------------------------------------------

// particle filtering version that uses opengl-computed votes

double gl_pf_PlanX_vote(double x, double y, int voter_top, int voter_bottom, int i_lambda)
{
  int ix, iy;

  if (x < planx_candidate_left || x >= planx_candidate_right || y < planx_candidate_top || y >= planx_candidate_bottom)
    return 0;

  ix = myround(x);
  iy = myround(y);

  // bilinear interpolation would be a little better

  return vp_votes[ix + planx_w * iy];
}

//----------------------------------------------------------------------------

// particle filtering version

double pf_PlanX_vote(double x, double y, int voter_top, int voter_bottom, int i_lambda)
{
  int r, c, r_start, i_max;
  double votes, vx, vy, vy_start, vpx, vpy, vpmag, inv_vpmag, vp_cos;

  if (x < planx_candidate_left || x >= planx_candidate_right || y < planx_candidate_top || y >= planx_candidate_bottom)
    return 0;

  vy_start = MAX2(floor(y + 1), voter_top);
  if (vy_start > voter_bottom)
    return 0;
  r_start = (vy_start - planx_voter_top) / planx_voter_dy;

  votes = 0;

  for (vy = vy_start, r = r_start; vy < voter_bottom; vy += planx_voter_dy, r++)
    for (vx = planx_voter_left, c = 0; vx < planx_voter_right; vx += planx_voter_dx, c++) {

      vpx = vx - x;
      vpy = vy - y;
      vpmag = sqrt(SQUARE(vpx) + SQUARE(vpy));
      inv_vpmag = 1.0 / vpmag;
      vpx *= inv_vpmag;
      vpy *= inv_vpmag;
      
      i_max = planx_voter_max[i_lambda][c + planx_voter_w * r];
	
      // still don't know why the minus sign below is necessary
      vp_cos = -vpx * planx_nx[i_max] + vpy * planx_ny[i_max];  

      if (fabs(vp_cos) >= planx_cos_threshold) 
	votes++;
    }

  return votes;
}

//----------------------------------------------------------------------------

// strip version

void PlanX_vote_DIST(double x, double y, int cand_r, int cand_c, int voter_top, int voter_bottom)
{
  int r, c, r_start, i_max, i_lambda;
  double votes, vx, vy, vy_start, vpx, vpy, vpmag, inv_vpmag, vp_cos;
  CR_Matrix *mat;

  vy_start = MAX2(floor(y + 1), voter_top);
  if (vy_start > voter_bottom)
    return;
  r_start = (vy_start - planx_voter_top) / planx_voter_dy;

  if (x == 100) 
    printf("y = %.1lf, voter_top = %i, vy_start = %.1lf, r_start = %i\n", y, voter_top, vy_start, r_start);

  for (vy = vy_start, r = r_start; vy < voter_bottom; vy += planx_voter_dy, r++)
    for (vx = planx_voter_left, c = 0; vx < planx_voter_right; vx += planx_voter_dx, c++) {

      vpx = vx - x;
      vpy = vy - y;
      vpmag = sqrt(SQUARE(vpx) + SQUARE(vpy));
      inv_vpmag = 1.0 / vpmag;
      vpx *= inv_vpmag;
      vpy *= inv_vpmag;
      
      for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++) {    // iterate over scales

	// assuming no orientation interpolation
	i_max = planx_voter_max[i_lambda][c + planx_voter_w * r];
	
	// still don't know why the minus sign below is necessary
	vp_cos = -vpx * planx_nx[i_max] + vpy * planx_ny[i_max];  

	if (fabs(vp_cos) >= planx_cos_threshold) { 
	  
	  // assuming no inv dist
	  MATRC(planx_candidate_wavelength_votes[i_lambda], cand_r, cand_c) = MATRC(planx_candidate_wavelength_votes[i_lambda], cand_r, cand_c) + 1;
	  MATRC(planx_candidate_votes, cand_r, cand_c) = MATRC(planx_candidate_votes, cand_r, cand_c) + 1;
	}
      }
    }
}

//----------------------------------------------------------------------------

// full version

void PlanX_vote_DIST(double x, double y, int cand_r, int cand_c)
{
  int r, c, r_start, i_max, i_lambda;
  double votes, vx, vy, vy_start, vpx, vpy, vpmag, inv_vpmag, vp_cos;
  CR_Matrix *mat;

  vy_start = MAX2(floor(y + 1), planx_voter_top);
  if (vy_start > planx_voter_bottom)
    return;
  r_start = (vy_start - planx_voter_top) / planx_voter_dy;

  if (x == 100) 
    printf("y = %.1lf, voter_top = %i, vy_start = %.1lf, r_start = %i\n", y, planx_voter_top, vy_start, r_start);

  for (vy = vy_start, r = r_start; vy < planx_voter_bottom; vy += planx_voter_dy, r++)
    for (vx = planx_voter_left, c = 0; vx < planx_voter_right; vx += planx_voter_dx, c++) {

      vpx = vx - x;
      vpy = vy - y;
      vpmag = sqrt(SQUARE(vpx) + SQUARE(vpy));
      inv_vpmag = 1.0 / vpmag;
      vpx *= inv_vpmag;
      vpy *= inv_vpmag;
      
      for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++) {    // iterate over scales

	if (planx_do_orientation_interpolation) {
	  // still don't know why the minus sign below is necessary
	  vp_cos = -vpx * MATRC(planx_voter_interp_nx[i_lambda], r, c) + vpy * MATRC(planx_voter_interp_ny[i_lambda], r, c);
	}
	else {
	  i_max = planx_voter_max[i_lambda][c + planx_voter_w * r];
	
	  // still don't know why the minus sign below is necessary
	  vp_cos = -vpx * planx_nx[i_max] + vpy * planx_ny[i_max];  
	}

	if (fabs(vp_cos) >= planx_cos_threshold) { 

	  // assuming no inv dist
	  inv_vpmag = 1.0;
	  if (planx_do_anisotropy)
	    inv_vpmag *= MATRC(planx_voter_anisotropy[i_lambda], r, c);
	  if (planx_do_strength)
	    inv_vpmag *= MATRC(planx_voter_strength[i_lambda], r, c);

	  MATRC(planx_candidate_wavelength_votes[i_lambda], cand_r, cand_c) = MATRC(planx_candidate_wavelength_votes[i_lambda], cand_r, cand_c) + inv_vpmag;
	}
      }
    }
}

//----------------------------------------------------------------------------

void tally_votes(CR_Image *im)
{
  int r, c, x, y, i_lambda;
  double maxval;

  //-------------------------------------------------
  // exhaustive method
  //-------------------------------------------------

  /*
  // reset vote arrays
      
  for (r = 0; r < planx_candidate_h; r++) 
    for (c = 0; c < planx_candidate_w; c++) {
      MATRC(planx_candidate_votes, r, c) = 0.0;
      for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++)     // iterate over scales
	MATRC(planx_candidate_wavelength_votes[i_lambda], r, c) = 0.0;
    }

  // tally votes and see who won

  for (r = 0, y = planx_candidate_top; r < planx_candidate_h; r++, y += planx_candidate_dy) 
    for (c = 0, x = planx_candidate_left; c < planx_candidate_w; c++, x += planx_candidate_dx) 
      PlanX_vote_DIST(x, y, r, c, planx_voter_top, planx_voter_bottom);

  max_CR_Matrix(planx_candidate_votes, &maxval, &r, &c);
  // tally individual wavelength votes?
  */

  //-------------------------------------------------
  // opengl method for counting votes
  //-------------------------------------------------

  if (!do_planx_strip) 
    compute_vp_votes(im);   // stores results in vp_votes array
  else 
    compute_moving_vp_votes(planx_voter_current_top, im); 

  //-------------------------------------------------
  // particle filtering method for tracking max vote-getter
  //-------------------------------------------------

  update_vp_particlefilter();   // reads vp_votes 

}

//----------------------------------------------------------------------------

// mono float image
// assume blend dst = dst + val
// assume (xa, ya) is in the image, but not necessarily (xb, yb) (and then we can assume yb <= ya => yb < im_h)

void votes_midpoint_line(float xa, float ya, float xb, float yb, float val, float *im, int im_w, int im_h)
{
  int xl, yl, xr, yr, dx, dy, xtemp, ytemp, x, y, d, incrE, incrNE;
  float clipped_xb, clipped_yb;

  CR_error("this is broken right now");

  // clip against left, right, and top sides

  //  CR_flush_printf("%f %f %f %f\n", xa, ya, xb, yb);

  if (xb >= im_w) {
    clipped_yb = myround(yb - (xb - (float) (im_w - 1)) * (yb - ya) / (xb - xa));
    clipped_xb = (float) (im_w - 1);
  }
  else if (xb < 0) {
    clipped_yb = myround(yb - xb * (yb - ya) / (xb - xa));
    clipped_xb = 0;
  }
  else if (yb < 0) {
    clipped_xb = myround(xb - yb * (xb - xa) / (yb - ya));
    clipped_yb = 0;
  }

  // bounds checking (shouldn't be necessary if clipping has occurred)

  if  (!(xa >= 0 && clipped_xb >= 0 && ya >= 0 && clipped_yb >= 0 && xa < im_w && clipped_xb < im_w && ya < im_h && clipped_yb < im_h))
    return;

  //  CR_flush_printf("%f %f %f %f (%f %f)\n", xa, ya, clipped_xb, clipped_yb, xb, yb);

  //  CR_flush_printf("here\n");

  // assign xl, yl to leftmost point (and xr, yr accordingly)

  if (xa < clipped_xb || (xa == clipped_xb && ya >= clipped_yb)) {
    xl = myround(xa);
    yl = myround(ya);
    xr = myround(clipped_xb);
    yr = myround(clipped_yb);
  }
  else if (xa > clipped_xb || (xa == clipped_xb && ya < clipped_yb)) {
    xl = myround(clipped_xb);
    yl = myround(clipped_yb);
    xr = myround(xa);
    yr = myround(ya);
  }

  // get down to business

  dx = xr - xl;
  dy = yr - yl;

  if (dx > 0 && dy >= 0) {

    // m <= 1

    if (dx >= dy) {

      d = 2 * dy - dx;
      incrE = 2 * dy;
      incrNE = 2 * (dy - dx);

      for (x = xl, y = yl; x <= xr; x++) {
	if (!(x >= 0 && y >= 0 && x < im_w && y < im_h))
	  CR_flush_printf("A: %f %f %f %f -> clipped %f %f -> %i %i [%i %i %i %i]\n", xa, ya, xb, yb, clipped_xb, clipped_yb, x, y, xl, yl, xr, yr);
	else
	  im[y * im_w + x] += val;
	if (d <= 0) 
	  d += incrE;
	else {
	  d += incrNE;
	  y++;
	}      
      }
    }

    // m > 1: swap x and y

    else {

      d = 2 * dx - dy;       
      incrE = 2 * dx;
      incrNE = 2 * (dx - dy);

      for (x = xl, y = yl; y <= yr; y++) {
	if (!(x >= 0 && y >= 0 && x < im_w && y < im_h))
	  CR_flush_printf("B: %f %f %f %f -> clipped %f %f -> %i %i [%i %i %i %i]\n", xa, ya, xb, yb, clipped_xb, clipped_yb, x, y, xl, yl, xr, yr);
	else
	  im[y * im_w + x] += val;
	if (d <= 0) 
	  d += incrE;
	else {
	  d += incrNE;
	  x++;
	}      
      }
    }
  }
  else {

    dy = -dy;

    // m >= -1

    if (dx >= dy) {


      d = 2 * dy - dx;
      incrE = 2 * dy;
      incrNE = 2 * (dy - dx);

      for (x = xl, y = yl; x <= xr; x++) {
	if (!(x >= 0 && y >= 0 && x < im_w && y < im_h))
	  CR_flush_printf("C: %f %f %f %f -> clipped %f %f -> %i %i [%i %i %i %i]\n", xa, ya, xb, yb, clipped_xb, clipped_yb, x, y, xl, yl, xr, yr);
	else
	  im[y * im_w + x] += val;
	if (d <= 0) 
	  d += incrE;
	else {
	  d += incrNE;
	  y++;
	}      
      }


    }

    // m < -1: swap x and y

    else {


      d = 2 * dx - dy;
      incrE = 2 * dx;
      incrNE = 2 * (dx - dy);

      for (x = xl, y = yl; y <= 2 * yl - yr; y++) {          // reflecting yr about yl
	if (!(x >= 0 && y >= 0 && x < im_w && y < im_h))
	  CR_flush_printf("D: %f %f %f %f -> clipped %f %f -> %i %i [%i %i %i %i]\n", xa, ya, xb, yb, clipped_xb, clipped_yb, x, y, xl, yl, xr, yr);
	else
	  im[y * im_w + x] += val;
	if (d <= 0) 
	  d += incrE;
	else {
	  d += incrNE;
	  x++;
	}      
      }

    }
  }
}

//----------------------------------------------------------------------------

void init_candidate_and_voter_regions(int voter_top, int voter_bottom, int voter_left, int voter_right, int voter_strip_height,
				      int voter_dx, int voter_dy,
				      int candidate_top, int candidate_bottom, int candidate_left, int candidate_right, 
				      int candidate_dx, int candidate_dy)
{
  int i, j, t, x, y;

  // voters

  planx_voter_left = voter_left;
  planx_voter_right = voter_right;
  planx_voter_top = voter_top;
  planx_voter_bottom = voter_bottom;
  planx_voter_strip_height = voter_strip_height;
  planx_voter_dx = voter_dx;  
  planx_voter_dy = voter_dy;  

  planx_voter_current_top = planx_voter_bottom - planx_voter_strip_height; 

  for (y = voter_top, planx_voter_h = 0; y < voter_bottom; y += voter_dy, planx_voter_h++)
    for (x = voter_left, planx_voter_w = 0; x < voter_right; x += voter_dx, planx_voter_w++)
      ;

  // candidates

  planx_candidate_left = candidate_left;
  planx_candidate_right = candidate_right;
  planx_candidate_top = candidate_top;
  planx_candidate_bottom = candidate_bottom;
  planx_candidate_dx = candidate_dx;  
  planx_candidate_dy = candidate_dy;  
  
  for (y = candidate_top, planx_candidate_h = 0; y < candidate_bottom; y += candidate_dy, planx_candidate_h++)
    for (x = candidate_left, planx_candidate_w = 0; x < candidate_right; x += candidate_dx, planx_candidate_w++)
      ;

}

//----------------------------------------------------------------------------

/*
void initialize_voting(int voter_top, int voter_bottom, int voter_left, int voter_right, 
		       int voter_dx, int voter_dy,
		       int candidate_top, int candidate_bottom, int candidate_left, int candidate_right, 
		       int candidate_dx, int candidate_dy)
*/
void initialize_voting()
{
  int i, j, t, x, y;
  double theta;

  // the opengl method for voting
  //  glEnable(GL_BLEND);
  glBlendFunc(GL_ONE, GL_ONE);
  vp_votes = (float *) CR_calloc(planx_w * planx_h, sizeof(float));
  vote_weight = 1 / (float) (planx_w * planx_h);
  printf("vote weight = %f\n", vote_weight);
  vote_line_length = 2*planx_w;  // each line must be sufficiently long that it will reach the image edge
                                 // no matter where in the image it starts or what its orientation

  // subsampled filter responses (i.e., "voters") will go here

  planx_voter_dlm = (CR_Matrix **) CR_calloc(planx_num_filters, sizeof(CR_Matrix *));
  for (t = 0; t < planx_num_filters; t++) 
    planx_voter_dlm[t] = make_CR_Matrix("planx_voter_dlm", planx_voter_h, planx_voter_w);
  
  planx_candidate_votes = make_CR_Matrix("votes", planx_candidate_h, planx_candidate_w);
  planx_candidate_votes_im = make_CR_Image(planx_candidate_w, planx_candidate_h, 0);
  planx_candidate_wavelength_votes = (CR_Matrix **) calloc(planx_num_wavelengths, sizeof(CR_Matrix *));
  planx_candidate_wavelength_votes_im = (CR_Image **) calloc(planx_num_wavelengths, sizeof(CR_Image *));
  for (t = 0; t < planx_num_wavelengths; t++) {
    planx_candidate_wavelength_votes[t] = make_CR_Matrix("candidate wavelength_votes", planx_candidate_h, planx_candidate_w);
    planx_candidate_wavelength_votes_im[t] = make_CR_Image(planx_candidate_w, planx_candidate_h, 0);
  }
  
  //-------------------------------------------------
  // set up dominant orientation directions
  //-------------------------------------------------

  planx_cos_threshold = cos(0.5 * DEG2RAD(planx_delta_theta));

  planx_nx = (double *) calloc(planx_num_orientations, sizeof(double));
  planx_ny = (double *) calloc(planx_num_orientations, sizeof(double));

  planx_nx_vote_line = (float *) calloc(planx_num_orientations, sizeof(float));
  planx_ny_vote_line = (float *) calloc(planx_num_orientations, sizeof(float));

  for (i = 0; i < planx_num_orientations; i++) {
    theta = DEG2RAD(planx_delta_theta) * (double) i;
    planx_nx[i] = cos(theta);
    planx_ny[i] = sin(theta);

    planx_nx_vote_line[i] = planx_nx[i] * vote_line_length;
    planx_ny_vote_line[i] = planx_ny[i] * vote_line_length;

  }

  planx_voter_max = (int **) calloc(planx_num_wavelengths, sizeof(int *));
  for (t = 0; t < planx_num_wavelengths; t++) 
    planx_voter_max[t] = (int *) calloc(planx_voter_w * planx_voter_h, sizeof(int));

}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void compute_dominant_orientations()
{
  int i, j, i_lambda, i_theta, i_max, r, c, x, y;
  double s_max, s;
  CR_Matrix *mat;
  double lambda, orientation, even, size;

  for (j = 0; j < planx_voter_h; j++)
    for (i = 0; i < planx_voter_w; i++) 
      for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++) {                // iterate over scales
	for (i_theta = 0, s_max = -1.0; i_theta < planx_num_orientations; i_theta++) {  // iterate over orientations
	  mat = planx_voter_dlm[i_theta + planx_num_orientations * i_lambda];
	  s = MATRC(mat, j, i);
	  if (s > s_max) {
	    s_max = s;
	    i_max = i_theta;
	  }
	}
	planx_voter_max[i_lambda][i + planx_voter_w * j] = i_max;
      }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// planx_im is raw 24-bit color

void fft_run_gabor_filters(CR_Image *planx_im)
{
  int i, j, t, x, y, r, c, cols, size, comp_size, double_comp_size;
  double ac, bd, ab_cd, test;
  double *g_out, *im_out, *g_inv_in, *g_inv_out0, *g_inv_out1;
  double *out, *out2;

  // convert to usable form

  iplColorToGray(planx_im->iplim, planx_gray_im->iplim);
  iplScaleFP(planx_gray_im->iplim, planx_gray_fp_im->iplim, 0, 255);
 
  // transfer planx_gray_fp_im to image_fft_in (float to double conversion) & execute forward fft

  for (y = 0; y < planx_h; y++)
    for (x = 0; x < planx_w; x++)
      image_fft_in[y * planx_w + x] = (double) GRAYFP_IMXY(planx_gray_fp_im, x, y);
  fftw_execute(image_fft_plan);
  //  fftw_execute(image_fft_inv_plan);

  // convolve image with each gabor filter

  size = planx_h * planx_w;
  comp_size = planx_h * fft_planx_w;
  double_comp_size = 2 * comp_size;
  cols = planx_voter_dlm[0]->cols;
  
  im_out = (double *) image_fft_out;

  /*
  for (y = 0, t = 0; y < planx_h; y++)
    for (x = 0; x < fft_planx_w; x++) {
      GRAYFP_IMXY(planx_complex_gabor_fp_im, x, y) = im_out[t];
      t += 2;
    }
  
  sprintf(planx_kernel_name, "C:\\Documents\\software\\grandchallenge\\images\\filtered\\im_out.pgm", i);
  write_CR_Image(planx_kernel_name, planx_complex_gabor_fp_im);
  exit(1);
  */

  /*
  for (t = 0; t < comp_size; t++) {
    ((double *) image_fft_out2[t])[0] = ((double *) image_fft_out[t])[0];
    ((double *) image_fft_out2[t])[1] = ((double *) image_fft_out[t])[1];
  }
  */

  // start working part

  /*
  out = (double *) image_fft_out;
  out2 = (double *) image_fft_out2;

  g_out = (double *) planx_gabor_filter_fft_out[0][0];

  for (t = 0; t < double_comp_size; t += 2) {
	ac = g_out[t] * out[t];
	bd = g_out[t + 1] * out[t + 1];
	ab_cd = (g_out[t] + g_out[t + 1]) * (out[t] + out[t + 1]);
	out2[t] = ac - bd;
	out2[t + 1] = ab_cd - ac - bd;
  }

  fftw_execute(image_fft_inv_plan2);


  //  fftw_execute(image_fft_inv_plan);
 
  double *tout = (double *) CR_calloc(size, sizeof(double));
  memcpy(tout, image_fft_inv_out2, size * sizeof(double));

  g_out = (double *) planx_gabor_filter_fft_out[0][1];

  for (t = 0; t < double_comp_size; t += 2) {
	ac = g_out[t] * out[t];
	bd = g_out[t + 1] * out[t + 1];
	ab_cd = (g_out[t] + g_out[t + 1]) * (out[t] + out[t + 1]);
	out2[t] = ac - bd;
	out2[t + 1] = ab_cd - ac - bd;
  }

  fftw_execute(image_fft_inv_plan2);

  for (y = 0, t = 0; y < planx_h; y++)
    for (x = 0; x < planx_w; x++) {
      GRAYFP_IMXY(planx_complex_gabor_fp_im, x, y) = sqrt(SQUARE(tout[t]) + SQUARE(image_fft_inv_out2[t]));
      t++;
    }
  
  sprintf(planx_kernel_name, "C:\\Documents\\software\\grandchallenge\\images\\filtered\\out%i.pgm", i);
  write_CR_Image(planx_kernel_name, planx_complex_gabor_fp_im);
  exit(1);
  */

  // end working part


  for (i = 0; i < planx_num_filters; i++) {
    //for (i = 0; i < 5; i++) {
    for (j = 0; j < 2; j++) {

      // componentwise multiplication in complex domain between planx_gabor_filter_fft_out and image_fft_out

      // (1) apparently same speed as (2)

      g_out = (double *) planx_gabor_filter_fft_out[i][j];
      g_inv_in = (double *) planx_gabor_filter_inv_fft_in[i][j];

      for (t = 0; t < double_comp_size; t += 2) {
	ac = g_out[t] * im_out[t];
	bd = g_out[t + 1] * im_out[t + 1];
	ab_cd = (g_out[t] + g_out[t + 1]) * (im_out[t] + im_out[t + 1]);
	g_inv_in[t] = ac - bd;
	g_inv_in[t + 1] = ab_cd - ac - bd;
      }
      
      // (2) simplify loop--apparently faster than (3)

      /*
      for (t = 0; t < comp_size; t++) {
	ac = ((double *) planx_gabor_filter_fft_out[i][j][t])[0] * ((double *) image_fft_out[t])[0];
	bd = ((double *) planx_gabor_filter_fft_out[i][j][t])[1] * ((double *) image_fft_out[t])[1];
	ab_cd = 
	  (((double *) planx_gabor_filter_fft_out[i][j][t])[0] + ((double *) planx_gabor_filter_fft_out[i][j][t])[1]) * 
	  (((double *) image_fft_out[t])[0] * ((double *) image_fft_out[t])[1]);
	((double *) planx_gabor_filter_inv_fft_in[i][j][t])[0] = ac;
	((double *) planx_gabor_filter_inv_fft_in[i][j][t])[1] = ab_cd - ac - bd;
      }
      */

      // (3) 

      /*
      for (y = 0; y < planx_h; y++)
	for (x = 0; x < fft_planx_w; x++) {    // NOTE the difference from planx_w here
	  t = y * fft_planx_w + x; 
	  ac = ((double *) planx_gabor_filter_fft_out[i][j][t])[0] * ((double *) image_fft_out[t])[0];
	  bd = ((double *) planx_gabor_filter_fft_out[i][j][t])[1] * ((double *) image_fft_out[t])[1];
	  ab_cd = 
	    (((double *) planx_gabor_filter_fft_out[i][j][t])[0] + ((double *) planx_gabor_filter_fft_out[i][j][t])[1]) * 
	    (((double *) image_fft_out[t])[0] * ((double *) image_fft_out[t])[1]);
	  ((double *) planx_gabor_filter_inv_fft_in[i][j][t])[0] = ac;
	  ((double *) planx_gabor_filter_inv_fft_in[i][j][t])[1] = ab_cd - ac - bd;
	}
      */

      // invert fft 

      fftw_execute(planx_gabor_filter_inv_fft_plan[i][j]);
  
    }

    g_inv_out0 = planx_gabor_filter_inv_fft_out[i][0];
    g_inv_out1 = planx_gabor_filter_inv_fft_out[i][1];
    
    for (y = planx_voter_top, r = 0; y < planx_voter_bottom; y += planx_voter_dy, r++)
      for (x = planx_voter_left, c = 0; x < planx_voter_right; x += planx_voter_dx, c++) {
	t = x + y * planx_w;
 	planx_voter_dlm[i]->x[c + r * cols] = sqrt(SQUARE(g_inv_out0[t]) + SQUARE(g_inv_out1[t]));
      }

    // if you really need to see an image

    /*
    for (y = 0, t = 0; y < planx_h; y++)
      for (x = 0; x < planx_w; x++) {
	GRAYFP_IMXY(planx_complex_gabor_fp_im, x, y) = sqrt(SQUARE(g_inv_out0[t]) + SQUARE(g_inv_out1[t]));
	t++;
	}
    */
    //    sprintf(planx_kernel_name, "C:\\Documents\\software\\grandchallenge\\images\\filtered\\nout%03i.pgm", i);
    //    write_CR_Image(planx_kernel_name, planx_complex_gabor_fp_im);

    //    sprintf(kernel_name, "C:\\Documents\\tex\\planx\\images\\filtered_dlm\\unsurf_straight_%03i_%.2lf_%.2lf_complex_%.0lf.dlm", 2 * i, lambda, orientation, size);
    //    write_dlm_CR_Image(kernel_name, planx_complex_gabor_fp_im);
  }

  //  exit(1);
  //  fftw_execute(many_inv_fft_plan);

}

//----------------------------------------------------------------------------

// planx_im is raw 24-bit color

void PlanX_run_gabor_filters(CR_Image *planx_im)
{
  int i, j, i_lambda, i_theta, i_max, i_max2, r, c, x, y;
  double s_max, s_max2, s;
  CR_Matrix *mat;
  double lambda, orientation, even, size, nx, ny, nmag;
  double time_elapsed, total_time;
  CR_Image *im;
  
  // convert to usable form

  iplColorToGray(planx_im->iplim, planx_gray_im->iplim);
  iplScaleFP(planx_gray_im->iplim, planx_gray_fp_im->iplim, 0, 255);
  
  // run Gabor filters on image and histogram
  
  //  CR_difftime();
  //  total_time = 0;
  
  // abbreviated way to get complex response/power modulus (haven't checked for accuracy, but speed is apparently the same)

  //  for (i = 0; i < planx_num_filters; i++) 
  //  for (i = 0; i <= 0; i++) {
  for (i = 36; i <= 36; i++) {
    iplConvolve2DFP(planx_gray_fp_im->iplim, planx_complex_gabor_fp_im->iplim, planx_gabor_filter[i], 2, IPL_SUMSQROOT);

  /*
  //  for (i = 0; i < planx_num_kernels / 2; i++) {
  for (i = 72 * 2; i < 72 * 2 + 1; i++) {
   
//     lambda = MATRC(planx_gabor_kernel_info, 2 * i, 0); 
//     orientation = MATRC(planx_gabor_kernel_info, 2 * i, 1); 
//     even = MATRC(planx_gabor_kernel_info, 2 * i, 2);
//     size = MATRC(planx_gabor_kernel_info, 2 * i, 3);

    //    printf("%i", i);

    //    printf("%i: convolving with %.2lf %.2lf %.0lf %.0lf\n", 2 * i, lambda, orientation, even, size);
    //    printf("%i: convolving with %.2lf %.2lf %.0lf %.0lf \n", 2 * i + 1, lambda, orientation, even + 1, size);
    
    // simple odd, even responses

    convolve_CR_Image(planx_gabor_kernel[2 * i], planx_gray_fp_im, planx_odd_gabor_fp_im);
    convolve_CR_Image(planx_gabor_kernel[2 * i + 1], planx_gray_fp_im, planx_even_gabor_fp_im);
    
    // complex response/power modulus
    
    iplSquare(planx_odd_gabor_fp_im->iplim, planx_squared_odd_gabor_fp_im->iplim);
    iplSquare(planx_even_gabor_fp_im->iplim, planx_squared_even_gabor_fp_im->iplim);
    iplAdd(planx_squared_odd_gabor_fp_im->iplim, planx_squared_even_gabor_fp_im->iplim, planx_complex_gabor_fp_im->iplim);
 
    
    for (y = 0; y < planx_h; y++)
      for (x = 0; x < planx_w; x++)
	GRAYFP_IMXY(planx_complex_gabor_fp_im, x, y) = sqrt(GRAYFP_IMXY(planx_complex_gabor_fp_im, x, y));
  */

    //    time_elapsed = CR_difftime();
    //    total_time += time_elapsed;
    //    printf("time = %lf / %lf\n", time_elapsed, total_time);
    
    // write filter responses
    
//     im = planx_complex_gabor_fp_im;
//     mat = planx_voter_dlm[i];
//     
//     for (y = planx_voter_top, r = 0; y < planx_voter_bottom; y += planx_voter_dy, r++)
//       for (x = planx_voter_left, c = 0; x < planx_voter_right; x += planx_voter_dx, c++)
// 	MATRC(mat, r, c) = ((float *) &(im->iplim->imageData[y * im->iplim->widthStep + 4 * x]))[0];


    sprintf(planx_kernel_name, "C:\\Documents\\software\\grandchallenge\\images\\filtered\\nonfft_complex_36.pgm", i);
    write_CR_Image(planx_kernel_name, planx_complex_gabor_fp_im);
    exit(1);

//      sprintf(kernel_name, "C:\\Documents\\tex\\planx\\images\\filtered\\unsurf_straight_%03i_%.2lf_%.2lf_complex_%.0lf.pgm", 2 * i, lambda, orientation, size);
//      write_CR_Image(kernel_name, planx_complex_gabor_fp_im);
//      sprintf(kernel_name, "C:\\Documents\\tex\\planx\\images\\filtered_dlm\\unsurf_straight_%03i_%.2lf_%.2lf_complex_%.0lf.dlm", 2 * i, lambda, orientation, size);
//      write_dlm_CR_Image(kernel_name, planx_complex_gabor_fp_im);
  } 
  
  //  printf("\n");
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// initialize voting
// takes voter limits, candidate limits
// remove these from gabor init function below

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// assuming "new way" of CR_Source_CG.cpp version

// start_theta, delta_theta, num_orientations, start_lambda, delta_lambda, num_wavelengths, voter_h, voter_w, planx_w, planx_h

void PlanX_initialize_gabor_filters(double start_lambda, double delta_lambda, int num_wavelengths, 
				    double start_theta, double delta_theta, int num_orientations, 
				    CR_Image *im)
{
  char *filename, *full_name, *voter_name;
  int i, j, r, c, t, x, y;
  char *kernel_name;
  double lambda, orientation, even, size, theta;

  if (num_wavelengths != 1)
    CR_error("PlanX_initialize_gabor_filters(): array organization of filters doesn't handle multiple wavelengths right now");

  planx_gray_im = make_CR_Image(im->w, im->h, IPL_TYPE_GRAY_8U);

  planx_start_lambda = start_lambda;
  planx_delta_lambda = delta_lambda;
  planx_num_wavelengths = num_wavelengths;

  planx_start_theta = start_theta;
  planx_delta_theta = delta_theta;
  planx_num_orientations = num_orientations;

  //  planx_w = im->w;
  //  planx_h = im->h;
  
  planx_num_filters = planx_num_orientations * planx_num_wavelengths;

  planx_num_kernels = planx_num_filters * 2;
  
  printf("making kernels...\n");
  
  planx_kernel_name = (char *) CR_calloc(256, sizeof(char));
  planx_gabor_kernel_info = make_CR_Matrix("kernel_info", planx_num_kernels, 4); // wavelength, orientation, phase, size  
  planx_gabor_kernel_matrix = make_gabor_filter_bank_CR_Matrix_array(planx_start_lambda, planx_delta_lambda, planx_num_wavelengths, 
								     planx_start_theta, planx_delta_theta, planx_num_orientations, 
								     planx_gabor_kernel_info);

  //-------------------------------------------------
  // compute convolution kernels
  //-------------------------------------------------

  planx_filter_area = (double *) calloc(planx_num_wavelengths, sizeof(double));

  for (i = 0; i < planx_num_wavelengths; i++) 
    planx_filter_area[i] = SQUARE(planx_gabor_kernel_matrix[2 * planx_num_orientations * i]->rows);
  
  planx_gabor_kernel = (CR_Image_Kernel **) CR_calloc(planx_num_kernels, sizeof(CR_Image_Kernel *));

  kernel_name = (char *) CR_calloc(256, sizeof(char));

  for (i = 0; i < planx_num_kernels; i++) { 
    
    planx_gabor_kernel[i] = make_CR_Image_Kernel(planx_gabor_kernel_matrix[i]);
    
//     lambda = MATRC(planx_gabor_kernel_info, i, 0); 
//     orientation = MATRC(planx_gabor_kernel_info, i, 1); 
//     even = MATRC(planx_gabor_kernel_info, i, 2);
//     size = MATRC(planx_gabor_kernel_info, i, 3);

//     planx_gabor_kernel_im = make_CR_Image(planx_gabor_kernel_matrix[i]->cols, planx_gabor_kernel_matrix[i]->rows, 0);
//     interp_draw_CR_Matrix_CR_Image(planx_gabor_kernel_matrix[i], planx_gabor_kernel_im);
//     sprintf(kernel_name, "C:\\Documents\\software\\grandchallenge\\images\\%03i_%.2lf_%.2lf_%.0lf_%.0lf.ppm", i, lambda, orientation, even, size);
//     write_CR_Image(kernel_name, planx_gabor_kernel_im);
//     free_CR_Image(planx_gabor_kernel_im);
  
//     planx_gabor_kernel_im = make_CR_Image(planx_gabor_fft_kernel_matrix[i]->cols, planx_gabor_fft_kernel_matrix[i]->rows, 0);
//     interp_draw_CR_Matrix_CR_Image(planx_gabor_fft_kernel_matrix[i], planx_gabor_kernel_im);
//     sprintf(kernel_name, "C:\\Documents\\software\\grandchallenge\\images\\fft_%03i_%.2lf_%.2lf_%.0lf_%i_%i.ppm", i, lambda, orientation, even, planx_h, planx_w);
//     write_CR_Image(kernel_name, planx_gabor_kernel_im);
//     free_CR_Image(planx_gabor_kernel_im);
   
    //    write_pfm_CR_Matrix(kernel_name, planx_gabor_kernel_matrix[i]);
  }

  // need this for abbreviated way to get complex response/power modulus used in PlanX_run_gabor_filters()

  planx_gabor_filter = (IplConvKernelFP ***) CR_calloc(planx_num_filters, sizeof(IplConvKernelFP **));

  for (i = 0; i < planx_num_filters; i++) {
    planx_gabor_filter[i] = (IplConvKernelFP **) CR_calloc(2, sizeof(IplConvKernelFP *));
    for (j = 0; j < 2; j++) 
      planx_gabor_filter[i][j] = planx_gabor_kernel[i + planx_num_orientations * (i / planx_num_orientations) + j * planx_num_orientations]->k;
  }

  // fft stuff: general

  planx_gabor_fft_kernel_matrix = make_gabor_filter_bank_CR_Matrix_array(planx_start_lambda, planx_delta_lambda, planx_num_wavelengths, 
									 planx_start_theta, planx_delta_theta, planx_num_orientations, 
									 planx_gabor_kernel_info,
									 planx_h, planx_w);   

  // fft stuff: Intel

  /*
  planx_gabor_filter_fp_im = (CR_Image ***) CR_calloc(planx_num_filters, sizeof(CR_Image **));
  planx_gabor_filter_fft_fp_im = (CR_Image ***) CR_calloc(planx_num_filters, sizeof(CR_Image **));

  for (i = 0; i < planx_num_filters; i++) {
    planx_gabor_filter_fp_im[i] = (CR_Image **) CR_calloc(2, sizeof(CR_Image *));
    planx_gabor_filter_fft_fp_im[i] = (CR_Image **) CR_calloc(2, sizeof(CR_Image *));
    for (j = 0; j < 2; j++) {
      // make fp images
      planx_gabor_filter_fp_im[i][j] = make_CR_Image(planx_w, planx_h, IPL_TYPE_GRAY_32F);
      planx_gabor_filter_fft_fp_im[i][j] = make_CR_Image(planx_w, planx_h, IPL_TYPE_GRAY_32F);
      // copy data over
      //      planx_gabor_filter_fft_fp_im[i][j] = planx_gabor_kernel[i + planx_num_orientations * (i / planx_num_orientations) + j * planx_num_orientations]->k;
      // compute ffts
      iplRealFft2D(planx_gabor_filter_fp_im[i][j]->iplim, planx_gabor_filter_fft_fp_im[i][j]->iplim, IPL_FFT_Forw);

    }
  }
  */

  // fft stuff: FFTW

  fft_planx_w = planx_w / 2 + 1;

  planx_gabor_filter_fft_in = (double ***) CR_calloc(planx_num_filters, sizeof(double **));
  planx_gabor_filter_fft_out = (fftw_complex ***) CR_calloc(planx_num_filters, sizeof(fftw_complex **));
  planx_gabor_filter_fft_plan = (fftw_plan **) CR_calloc(planx_num_filters, sizeof(fftw_plan *));

  planx_gabor_filter_inv_fft_out = (double ***) CR_calloc(planx_num_filters, sizeof(double **));
  planx_gabor_filter_inv_fft_in = (fftw_complex ***) CR_calloc(planx_num_filters, sizeof(fftw_complex **));
  planx_gabor_filter_inv_fft_plan = (fftw_plan **) CR_calloc(planx_num_filters, sizeof(fftw_plan *));

  // the advanced interface for running many ffts doesn't seem to speed things up at all

  /*
  many_planx_gabor_filter_inv_fft_out = (double *) fftw_malloc(sizeof(double) * planx_w * planx_h * planx_num_filters * 2);
  many_planx_gabor_filter_inv_fft_in = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * planx_w * planx_h * planx_num_filters * 2);

  const int n[2] = {planx_h, planx_w};

  CR_flush_printf("many fft: planning...");
  many_inv_fft_plan = fftw_plan_many_dft_c2r(2, n, planx_num_filters * 2,
					     many_planx_gabor_filter_inv_fft_in, NULL,
					     1, planx_w * planx_h,
					     many_planx_gabor_filter_inv_fft_out, NULL,
					     1, planx_w * planx_h,
					     FFTW_MEASURE);
  CR_flush_printf("done\n");
  */

  /*
  fftw_plan fftw_plan_many_dft_c2r(int rank, const int *n, int howmany,
				   fftw_complex *in, const int *inembed,
				   int istride, int idist,
				   double *out, const int *onembed,
				   int ostride, int odist,
				   unsigned flags);
  */

  for (i = 0; i < planx_num_filters; i++) {

    printf("preparing ffts for gabor pair %i\n", i);

    planx_gabor_filter_fft_in[i] = (double **) CR_calloc(2, sizeof(double *));
    planx_gabor_filter_fft_out[i] = (fftw_complex **) CR_calloc(2, sizeof(fftw_complex *));
    planx_gabor_filter_fft_plan[i] = (fftw_plan *) CR_calloc(2, sizeof(fftw_plan));

    planx_gabor_filter_inv_fft_out[i] = (double **) CR_calloc(2, sizeof(double *));
    planx_gabor_filter_inv_fft_in[i] = (fftw_complex **) CR_calloc(2, sizeof(fftw_complex *));
    planx_gabor_filter_inv_fft_plan[i] = (fftw_plan *) CR_calloc(2, sizeof(fftw_plan));

    for (j = 0; j < 2; j++) {

      planx_gabor_filter_fft_in[i][j] = (double *) fftw_malloc(sizeof(double) * planx_w * planx_h);
      planx_gabor_filter_fft_out[i][j] = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * fft_planx_w * planx_h);
      planx_gabor_filter_fft_plan[i][j] = fftw_plan_dft_r2c_2d(planx_h, planx_w, planx_gabor_filter_fft_in[i][j], planx_gabor_filter_fft_out[i][j], FFTW_ESTIMATE);

      planx_gabor_filter_inv_fft_out[i][j] = (double *) fftw_malloc(sizeof(double) * planx_w * planx_h);
      planx_gabor_filter_inv_fft_in[i][j] = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * fft_planx_w * planx_h);
      planx_gabor_filter_inv_fft_plan[i][j] = fftw_plan_dft_c2r_2d(planx_h, planx_w, planx_gabor_filter_inv_fft_in[i][j], planx_gabor_filter_inv_fft_out[i][j], FFTW_MEASURE);  

      // put data into "in" array and execute
      //     t = i + planx_num_orientations * (i / planx_num_orientations) + j * planx_num_orientations;
      t = 2 * i + j;
      for (r = 0; r < planx_h; r++)
	for (c = 0; c < planx_w; c++)
	  planx_gabor_filter_fft_in[i][j][r * planx_w + c] = MATRC(planx_gabor_fft_kernel_matrix[t], r, c);
      free_CR_Matrix(planx_gabor_fft_kernel_matrix[t]);
      fftw_execute(planx_gabor_filter_fft_plan[i][j]);
    }
  }

  image_fft_in = (double *) fftw_malloc(sizeof(double) * planx_w * planx_h);
  image_fft_out = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * fft_planx_w * planx_h);
  image_fft_out2 = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * fft_planx_w * planx_h);
  image_fft_inv_out = (double *) fftw_malloc(sizeof(double) * planx_w * planx_h);
  image_fft_inv_out2 = (double *) fftw_malloc(sizeof(double) * planx_w * planx_h);
  CR_flush_printf("image fft: planning...");
  image_fft_plan = fftw_plan_dft_r2c_2d(planx_h, planx_w, image_fft_in, image_fft_out, FFTW_PATIENT); 
  image_fft_inv_plan = fftw_plan_dft_c2r_2d(planx_h, planx_w, image_fft_out, image_fft_inv_out, FFTW_ESTIMATE); 
  image_fft_inv_plan2 = fftw_plan_dft_c2r_2d(planx_h, planx_w, image_fft_out2, image_fft_inv_out2, FFTW_ESTIMATE); 
  CR_flush_printf("done\n");

  printf("done\n");

  //-------------------------------------------------
  // prepare storage for convolution results
  //-------------------------------------------------
  
  // initialize images for filter stages
  
  planx_gray_fp_im = make_CR_Image(planx_w, planx_h, IPL_TYPE_GRAY_32F);
  planx_gray_fft_fp_im = make_CR_Image(planx_w, planx_h, IPL_TYPE_GRAY_32F);

  planx_odd_gabor_fp_im = make_CR_Image(planx_w, planx_h, IPL_TYPE_GRAY_32F);
  planx_even_gabor_fp_im = make_CR_Image(planx_w, planx_h, IPL_TYPE_GRAY_32F);
  planx_squared_odd_gabor_fp_im = make_CR_Image(planx_w, planx_h, IPL_TYPE_GRAY_32F);
  planx_squared_even_gabor_fp_im = make_CR_Image(planx_w, planx_h, IPL_TYPE_GRAY_32F);
  planx_complex_gabor_fp_im = make_CR_Image(planx_w, planx_h, IPL_TYPE_GRAY_32F);

  //  gabor_filters_initialized = TRUE;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------


