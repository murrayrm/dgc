#ifndef Debug_HH
#define Debug_HH

#include "Misc/Structures/ModuleInfo.hh"

list<ModuleInfo> LocalModuleTypes();
list<ModuleInfo> AllModuleTypes();
void PrintModuleList(list<ModuleInfo> l);


#endif
