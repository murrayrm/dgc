#include "DGC_MODULE.hh"
#include "Kernel.hh"
#include <iostream>


using namespace boost;
using namespace std;

extern int MTA_KERNEL_COUT;

DGC_MODULE::DGC_MODULE(int modtype, string modname, int modflags ) {

  M_ModuleType             = modtype;
  M_ModuleName             = modname;
  M_ModuleFlags            = modflags;

  M_CurrentState           = -1;
  M_NextState              = STATES::INIT; 

  // M_MyThreads              = 0; // now user defined threads started.

};

DGC_MODULE::~DGC_MODULE() {

}

void DGC_MODULE::operator ()() {
  // Loads State Loop
  cout << "Running State Machine Loop" << endl;
  StateMachineLoop();
};

void DGC_MODULE::ForceShutdown() {

  cout << ModuleName() << " Shutdown called" << endl;
  SetNextState(STATES::SHUTDOWN);
}

void DGC_MODULE::Init() {
  cout << ModuleName() << ":[Default] Init." << endl;
  SetNextState(STATES::ACTIVE);
}
void DGC_MODULE::Active() {
  cout << ModuleName() << ":[Default] Active State." << endl;
}
void DGC_MODULE::Standby() {
  cout << ModuleName() << ":[Default] Standby State." << endl;
}
void DGC_MODULE::Shutdown() {
  cout << ModuleName() << ":[Default] Shutdown State." << endl;
}
void DGC_MODULE::Restart() {
  cout << ModuleName() << ":[Default] Standby State." << endl;
}


string DGC_MODULE::Status() {
  string x = ModuleName() + ": is in state ";
  switch ( GetCurrentState() ) {
  case STATES::INIT:
    x+= "Init";       break;
  case STATES::ACTIVE:
    x+= "Active";     break;
  case STATES::SHUTDOWN:
    x+= "Shutdown";   break;
  case STATES::STANDBY:
    x+= "Standby";    break;
  case STATES::RESTART:
    x+= "Restart";    break;
  default:
    x+= "Unknown state - (Something is very wrong)";
  }
  return x;
}



void DGC_MODULE::InMailHandler(Mail& msg) {
  cout << ModuleName() << ":[Default] InMessageHandler - Ignoring Message Received" << endl;
}

Mail DGC_MODULE::QueryMailHandler(Mail& msg) {
  cout << ModuleName() << ":[Default] QueryMailHandler - Replying With Bad Message" << endl;
  return Mail(msg.To(), msg.From(), 0, MailFlags::BadMessage);
}


int  DGC_MODULE::ModuleNumber() {
  return M_ModuleNumber;
}

MailAddress DGC_MODULE::MyAddress() {
  MailAddress x;
  x.TCP = MyTCPAddress();
  x.MAILBOX = M_ModuleNumber;

  return x;
}


ModuleInfo DGC_MODULE::MyInfo() {
  ModuleInfo x;
  x.MA = MyAddress();
  x.ModuleType = M_ModuleType;
  return x;
}

int DGC_MODULE::ModuleType() {
  return M_ModuleType;
}
string DGC_MODULE::ModuleName() {
  return M_ModuleName;
}

int DGC_MODULE::SetNextState( int stateID ) {

  if( MTA_KERNEL_COUT ) cout << "SetNextState Called" <<endl;
  boost::recursive_mutex::scoped_lock sl(M_StateMachineLock);

  if( !((M_NextState    == STATES::SHUTDOWN) ||
	(M_CurrentState == STATES::SHUTDOWN))	) {
    if( stateID < 0 || stateID > STATES::NUM_STATES ) {
      return -1;  // invalid state number (who's the idiot?)
    } else {
      M_NextState = stateID;
    }

  } else {
    // return shutdown state, since a shutdown has been issued previously
    return STATES::SHUTDOWN;
  }
} 

int DGC_MODULE::GetCurrentState() {
  return M_CurrentState;
}

int DGC_MODULE::ContinueInState() {
  if( ShuttingDown() || M_NextState >=0) {
    return false;
  } else { 
    return true;
  }
}

void DGC_MODULE::StateMachineLoop() {
  while( M_NextState >= 0 && M_NextState < STATES::NUM_STATES) {
    M_CurrentState = M_NextState;
    M_NextState    = -1; // clear the next state before we execute
    ExecuteCurrentState();

    if( M_CurrentState == STATES::SHUTDOWN) {
      // if the shutdown state finished...
      M_NextState = -1;  // kill the program
    } else if(M_NextState == -1) {
      // Next State was never set...might as well 
      // shutdown for lack of something better to do
      SetNextState(STATES::SHUTDOWN);
    }
  }
}

void DGC_MODULE::ExecuteCurrentState() {
  switch( M_CurrentState ) {
  case STATES::INIT:
    Init(); break;
  case STATES::ACTIVE:
    Active(); break;
  case STATES::STANDBY:
    Standby(); break;
  case STATES::SHUTDOWN:
    Shutdown(); break;
  case STATES::RESTART:
    Restart(); break;
  default:
    cout << M_ModuleName << ": WTF? - State gone wacko" << endl;
  }
}


Mail NewQueryMessage(MailAddress from, int modtype, int messagetype) {

  return Mail( from, FindModuleAddress(modtype), messagetype, MailFlags::QueryMessage);
}

Mail NewOutMessage(MailAddress from, int modtype, int messagetype) {

  return Mail( from, FindModuleAddress(modtype), messagetype, 0);
}
