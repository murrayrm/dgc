#include "Kernel.hh"

#include <iomanip>
#include <fstream>
#include <iostream>
#include "Kernel.hh"
#include <time.h>
#include "Misc/Socket/Socket.hh"
#include "Misc/Thread/ThreadsForClasses.hpp"
#include <signal.h>
#include "Debug.hh"

using namespace std;
using namespace boost;
extern Kernel K;
extern int MTA_KERNEL_COUT;

pid_t PID_SYSTEM_MAP = -1;

// Internal Functions
int PortScanIP(IP_ADDRESS ip, list<ModuleInfo>&ret);
list<ModuleInfo> PortScanTableOfComputers( map<string, IP_ADDRESS>& a );

int Kernel::SystemMapInit(string textFile) {

  // -------OLD WORKING BLOCK OF CODE----------
  //  M_LOCAL_TCP.IP = IPConvert("127.0.0.1");
  //  M_TableOfComputers["LOCALHOST"] = IPConvert("127.0.0.1");
  //  M_LOCAL_TCP.PORT = M_ServerSocket.Port();  
  // -------OLD WORKING BLOCK OF CODE----------
  

  // ------- START OF NEW VERSION --------------------------
  string myip;
  {
    //first grab myIP from file
    ifstream fip("/etc/ip");
    fip >> myip;
  
    cout << "SystemMapInit: Grabbed MyIP = " << myip << " from file /etc/ip"
	 << " which converts to " << IPConvert(myip.c_str()) << endl;

    if( IPConvert(myip.c_str()) == 0 ) {
      // bad IP Table lookup
      // quit here
      cout << "--------------------------------------" << endl;
      cout << "--------------------------------------" << endl;
      cout << "--------------------------------------" << endl;
      cout << "MISSING IP FILE /etc/ip -- Ask Ike or Alan To Provide This File" << endl;

      boost::recursive_mutex::scoped_lock sl(M_TableOfComputersLock);

      M_LOCAL_TCP.IP = IPConvert("127.0.0.1");
      M_TableOfComputers["LOCALHOST"] = IPConvert("127.0.0.1");
      M_LOCAL_TCP.PORT = M_ServerSocket.Port();  


    } else {
      // it appears to be ok.
      
      M_LOCAL_TCP.IP = IPConvert(myip.c_str());    
      M_LOCAL_TCP.PORT = M_ServerSocket.Port();  
      
      // Now grab the grid of computers out of another file
      ifstream fgrid("/etc/grid");
      string comp; string ip;

      while( ! fgrid.eof()) {

	fgrid >> comp;
	if( ! fgrid.eof()) {
	  fgrid >> ip;

	  boost::recursive_mutex::scoped_lock sl(M_TableOfComputersLock);
	  //	  M_TableOfComputers[comp] = IPConvert( ip.c_str());
	  
	  Computer * p_computer = new Computer( comp, IPConvert( ip.c_str()));

	  M_Grid.push_back( shared_ptr<Computer> (p_computer) );
	  RunMethodInNewThread<Computer>( p_computer, &Computer::AutoUpdate);

	}
      }

      //      M_TableOfComputers["Grandchallenge"] = IPConvert("131.215.133.164");
      //      M_TableOfComputers["Titanium"] = IPConvert("192.168.0.4");
      //      M_TableOfComputers["Magnesium"] = IPConvert("192.168.0.7");
      //      M_TableOfComputers["Iridium"] = IPConvert("192.168.0.8");
      //      M_TableOfComputers["Palladium"] = IPConvert("192.168.0.11");

      
      
      //      cout << "We have " << M_TableOfComputers.size() << " computers." << endl;
      cout << "We have " << M_Grid.size() << " computers." << endl;

    }
  }

  // ------- END OF NEW VERSION -------------------------- 


  //  boost::recursive_mutex::scoped_lock sl(M_TableOfComputersLock);
  if( MTA_KERNEL_COUT ) cout << "SystemMapInit: Trying To Scan Table Of Computers" << endl;
  int ok_to_go;
  list< shared_ptr<Computer> >::iterator x;

  do {
    ok_to_go = true;
    for( x = M_Grid.begin(); x != M_Grid.end(); x++) {
      boost::recursive_mutex::scoped_lock s( (*(*x)).Mutex );
      // if we have not scanned this computer then we are not ready yet
      if( TVNow() - (*(*x)).LastUpdate > Timeval( 10, 0)) ok_to_go = false; 
    }
    usleep( 100000 );
  } while( ! ok_to_go );

  //  M_TableOfModules = PortScanTableOfComputers(M_TableOfComputers);
  if( MTA_KERNEL_COUT ) cout << "SystemMapInit: Finished Scanning Table Of Computers" << endl;


  return true;
}

void Kernel::SystemMapThread() {


  /* Not Needed Anymore
  PID_SYSTEM_MAP = getpid();

  // shoud be filled in
  while( ! ShuttingDown() ) {

    list<ModuleInfo> NewModuleTable;
    {
      boost::recursive_mutex::scoped_lock sl(M_TableOfComputersLock);
      NewModuleTable = PortScanTableOfComputers(M_TableOfComputers);
    }

    if( true) {
      recursive_mutex::scoped_lock sl(M_TableOfModulesLock);
      M_TableOfModules = NewModuleTable;
    }

    //PrintModuleList(M_TableOfModules);
    sleep(10);
  }
  */
}

void Kernel::SystemMapShutdown() {


}
  
TCP_ADDRESS MyTCPAddress() {
  return K.M_LOCAL_TCP;
}

DGC_MODULE* FindLocalModule(int mbox) {

  recursive_mutex::scoped_lock s(K.M_LocalModulesLock);
  list<shared_ptr <DGC_MODULE> >::iterator x    = (K.M_LocalModules).begin();
  list<shared_ptr <DGC_MODULE> >::iterator stop = (K.M_LocalModules).end();
  while( x != stop) {
    if( (*(*x)).ModuleNumber() == mbox ) {
      // found the right module
      return & (*(*x)); 
    } else {
      x++;
    }
  }

  return NULL;
}


MailAddress FindModuleAddress(string moduleName) {
  return FindModuleAddress(K.M_ModuleNameToType[moduleName]);
}
  
MailAddress FindModuleAddress(int ModuleType) {

  list<ModuleInfo>::iterator x;
  list< shared_ptr<Computer> >::iterator y;

  for( y = K.M_Grid.begin(); y != K.M_Grid.end(); y++) {
    recursive_mutex::scoped_lock  sl( (*(*y)).Mutex );      
    for( x = (*(*y)).Modules.begin(); x != (*(*y)).Modules.end(); x++) {
      if( (*x).ModuleType == ModuleType) {
	// found, return address
	return (*x).MA;
      }
    }
  }
 
    
  if( MTA_KERNEL_COUT ) cout << "Cannot Find Module of Type " << ModuleType << ", "
       << "Size of M_TableOfModules = " << K.M_TableOfModules.size() << endl;

  if( MTA_KERNEL_COUT ) PrintModuleList( AllModules() );
  
  // else not found, return "zero"
  MailAddress ret;
  ret.TCP.IP   =  0;
  ret.TCP.PORT =  0;
  ret.MAILBOX  =  -1;
  
  return ret;
}

  
list<ModuleInfo> LocalModules() {
  //  cout << "Local Modules - Trying to obtain Lock " << endl;
  recursive_mutex::scoped_lock s(K.M_LocalModulesLock);
  //  cout << "Local Modules - Lock Obtained" << endl;
  list<ModuleInfo> lst;
  ModuleInfo MI;

  // for each module, need to fill in mailbox and module type
  list<shared_ptr <DGC_MODULE> >::iterator x    = (K.M_LocalModules).begin();
  list<shared_ptr <DGC_MODULE> >::iterator stop = (K.M_LocalModules).end();
  while( x != stop) {
    MI.MA = (*(*x)).MyAddress();
    MI.ModuleType = (*(*x)).ModuleType();
    lst.push_back( MI );
    x++;
  }
  //    cout << "SystemMap:LocalModules = " << lst.size() << endl;
  
  //cout << "LocalModules Function Finished" <<endl;
  return lst;
}
  
list<ModuleInfo> AllModules() {

  list<ModuleInfo> ret;

  list<ModuleInfo>::iterator x;
  list< shared_ptr<Computer> >::iterator y;

  for( y = K.M_Grid.begin(); y != K.M_Grid.end(); y++) {
    recursive_mutex::scoped_lock  sl( (*(*y)).Mutex );      
    for( x = (*(*y)).Modules.begin(); x != (*(*y)).Modules.end(); x++) {
      ret.push_back( (*x) );
    }
  }

  return ret;
}
  

int PortScanIP(IP_ADDRESS ip, list<ModuleInfo>&ret) {
  int i;

  if( MTA_KERNEL_COUT ) cout << "SystemMap: PortScanIP: Scanning IP " << ip << endl;  
  TCP_ADDRESS myAddress = MyTCPAddress();
  MailAddress myKernelAddress;
  
  myKernelAddress.TCP = myAddress;
  myKernelAddress.MAILBOX = 0;
  
  //    cout << "myAddress IP = " << myAddress.IP
  //	 << ", Port= " << myAddress.PORT 
  //	 << ", Trying to scan IP = " << ip << endl;
  
  //  list<ModuleInfo> ret;
  for(i=DEF::MinPort; i<(DEF::MinPort + DEF::NumPorts); i++) {
    
    // do not scan this kernel via sockets, use datum
    if( ip == myAddress.IP && i == myAddress.PORT ) {
      
      list<ModuleInfo>::iterator pos = ret.end();
      list<ModuleInfo> local = LocalModules();
      if( MTA_KERNEL_COUT ) cout << "                     : Grabbed " << local.size()
				 << " Local Modules" << endl;        
      //	cout << "Grabbed " << local.size() << " Local Modules" << endl;
      ret.splice(pos, local);
      continue;
    }
    
    // otherwise, do a remote scan
    if( MTA_KERNEL_COUT ) cout << "                     : Scanning Port " << i << endl;
  
    // make sure we can proceed in reasonable time.
    Timeval before = TVNow();

    CSocket sock(ip, i); // port is i
    if(sock.FD() < 0) { 
      //	cout << "PORT " << i << " failed to connect." << endl;
      // connection error (probably just no module there)
      // just skip over

      // TODO: Make the timeout value a #define
      if( (TVNow() - before) > Timeval(0, 800000)) return -1; // this computer probably doesnt exist.
      
      continue;
    }
    
    // we are all good
    // send a query message to ask for remote modules
    MailAddress remoteMailbox;
    remoteMailbox.TCP.IP = ip;
    remoteMailbox.TCP.PORT = i;
    remoteMailbox.MAILBOX = 0; // send to the kernel
    if( MTA_KERNEL_COUT ) cout << "                     : By Sending Query Message " << endl;
    Mail m(myKernelAddress, remoteMailbox, KernelMessages::MODULEINFO, MailFlags::QueryMessage);   
    Mail returnMail = SendQuery(m, sock);
    
    if( MTA_KERNEL_COUT ) cout << "                     : SendQuery Returned ..." << endl;

    if( returnMail.OK() ) {
      //	cout << "returnMail is OK" << endl;
      if( MTA_KERNEL_COUT ) cout << "                     : Success " << i << endl;
      ModuleInfo MI;
      while(returnMail.Size() >= sizeof(ModuleInfo)) {
	returnMail.DequeueFrom( (void*) &MI, sizeof(MI));

	// use the IP and port we used to find the module,
	// not what it thinks it is
	MI.MA.TCP.IP = ip;
	MI.MA.TCP.PORT = i;
	ret.push_back(MI);
      }
    } else if( MTA_KERNEL_COUT ) cout << "                     : Failure " << i << endl;
    
  }
  return ret.size();
}


list<ModuleInfo> PortScanTableOfComputers( map<string, IP_ADDRESS>& myMap ) {
  map<string, IP_ADDRESS>::iterator x    = myMap.begin();
  map<string, IP_ADDRESS>::iterator stop = myMap.end();
  
  list<ModuleInfo> NewModuleTable;
  list<ModuleInfo> TmpModuleTable;
  list<ModuleInfo>::iterator pos;

  
  list < map<string, IP_ADDRESS>::iterator > toRemove;
  list < map<string, IP_ADDRESS>::iterator >::iterator y;

  
  while( x != stop ) {
    TmpModuleTable.clear();
    //    cout << "Scanning " << (*x).first << endl;

    if( PortScanIP( (*x).second, TmpModuleTable ) >= 0) {
      pos = NewModuleTable.end();
      NewModuleTable.splice(pos, TmpModuleTable);
    } else {
      // An error occurred, stop using this IP
      cout << "Cannot Find Computer - " << (*x).first
	   << ", Deleting it from the table." << endl;
      toRemove.push_back(x);
    }
    x++;
  }

  // now remove all bad connections  
  for(y = toRemove.begin(); y != toRemove.end(); y++) {
    myMap.erase( (*y) );
  }


  return NewModuleTable;
}


