#include "Memory.hh"
#include <iostream>

using namespace std;

int main() {

  
  MemoryPointer x ( new char[100], 100);
  MemoryPointer y ( new char[99], 99);
  
  MemoryPointer r;
  MemoryPointer z = x;

  MemoryPointer w = y;

  x = y;
  z = y;
  

  MemoryContainer mc ( 100);

  return 0;
}
