#include "misc/Memory/Memory.hh"
#include <map>
#include <iostream>

using namespace std;

std::map<char*, int> allocated;


MemoryPointer::MemoryPointer() {

  myPtr = NULL;
  mySize = 0;
  mySlider = myPtr;

}

MemoryPointer::MemoryPointer( char * ptr, int sz) {

  myPtr = ptr;
  mySize = sz;
  mySlider = myPtr;

  insert();
}

MemoryPointer::MemoryPointer( const MemoryPointer & m) {

  myPtr = m.myPtr;
  mySize = m.mySize;
  mySlider = m.mySlider;

  insert();
}


MemoryPointer::~MemoryPointer() {
  destruct();
}

void MemoryPointer::destruct() {

  allocated[myPtr]--;
  if( allocated[myPtr] == 0 ) {
    delete myPtr;
    cout << "deallocated" << endl;
  }
}

void MemoryPointer::insert() {
  
  if( myPtr != NULL ) {
      if( allocated[myPtr] > 0) {
	cout << "copied" << endl;
      } else {
	cout << "allocated" << endl;
      }

      allocated[myPtr]++;
  }
}

MemoryPointer& MemoryPointer::operator=(const MemoryPointer & m) {

  destruct();

  myPtr = m.myPtr;
  mySize = m.mySize;
  mySlider = m.mySlider;

  insert();
}



char* MemoryPointer::slider() {
  return mySlider;
}


char* MemoryPointer::ptr() {
  return myPtr;
}

char* MemoryPointer::shr(int r) {
  mySlider += r;
  return mySlider;
}

char* MemoryPointer::shl(int l) {
  mySlider -= l;
  return mySlider;
}

int MemoryPointer::size() {
  return mySize;
}


int MemoryPointer::used() {
  return mySlider-myPtr;
}


int MemoryPointer::left() {
  return myPtr+mySize-mySlider;
}
/* ************************************************************* *
 * ************************************************************* */

MemoryContainer::MemoryContainer(int alloc_inc) {

  //  myMP = MemoryPointer(new char[initial_alloc], initial_alloc);
  myBlockSize = alloc_inc;
  myHead = 0;
}
  
MemoryContainer::~MemoryContainer() {
  destruct();
}

void MemoryContainer::destruct() {

}

int push(char* pdata, int sz) {
  if(myMP.left() < sz) {
    // must reallocate
    int alloc_amt = (((myMP.size() + sz) / myBlockSize)+1)*myBlockSize;
    MemoryPointer temp(new char[alloc_amt], alloc_amt);
    memcpy(temp.ptr(), myMP(), myMP.used());    
    temp.shr(myMP.used());
    myMP = temp;
  }

  memcpy(myMP.slider(), pdata, sz);
  myMP.shr(sz);
}

//// NEED 2 sliders 1 for end of existing data, one for where we are as far as popping data.
