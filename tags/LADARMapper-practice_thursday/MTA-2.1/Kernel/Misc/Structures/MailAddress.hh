#ifndef MailAddress_HH
#define MailAddress_HH

#include "IP.hh"



// A unique destination is IP Address, port, and mailbox number 
// (same module may have multiple mailboxes)
struct MailAddress {
  TCP_ADDRESS TCP;
  int MAILBOX;
};

#endif
