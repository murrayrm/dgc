#ifndef File_HH
#define File_HH

#include <boost/shared_ptr.hpp>
#include "../Time/Timeval.hh"

class FileHandle {
private:
  int my_fd;
  int IAMOwner;
public:
  FileHandle(int fd, int owner=true);
  ~FileHandle();
  int FD();
};


class File {
public:
  File();
  File(const File&);
  //  File(std::string filename, int flags= O_CREAT | O_TRUNC);
  File(int fd, int owner=true); // if owner is true, this will automatically 
  ~File();                       // close the file on destruction

  int OTRead (void* buffer, int bytes, Timeval timeout= Timeval(0, 100000)); // one time read up to 'bytes' bytes
  int OTWrite(void* buffer, int bytes, Timeval timeout= Timeval(0, 100000)); // one time write, returns bytes written
  int BRead  (void* buffer, int bytes, Timeval timeout= Timeval(1, 0)); // blocking read with timeout
  int BWrite (void* buffer, int bytes, Timeval timeout= Timeval(1, 0)); // timeout in seconds.

  int FD(); // returns file descriptor

protected: // so that derived classes may use these
    

  boost::shared_ptr<FileHandle> myFH;

};
 


#endif
