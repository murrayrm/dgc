#include <stdio.h>
#include "File.hh"
#include <time.h>
#include <sys/time.h>
#include <sys/poll.h>
#include <unistd.h>
#include "../Time/Time.hh"
#include "../Time/Timeval.hh"


#include <sys/types.h>
#include <stropts.h>

#include <iostream>

using namespace std;
using namespace boost;

FileHandle::FileHandle(int fd, int owner) {
  my_fd = fd;
  IAMOwner = owner;

  //  if( my_fd >= 0 ) {
  //    cout << "FD " << my_fd << " opened. IAMOwner = " << IAMOwner << endl;
  //  }
}

FileHandle::~FileHandle() {
  if (my_fd >= 0 && IAMOwner) {
    //    cout << "FD " << my_fd << " closed." << endl;
    close(my_fd);
  }// else if(! IAMOwner && my_fd>0 ) {
   // cout << "FD " << my_fd << " not closed because I am not the owner." << endl;
  //}
}

int FileHandle::FD() {
  return my_fd;
}


File::File() {
  myFH = shared_ptr<FileHandle>(new FileHandle(-1, false));
}

/*
File::File(string filename, int flags) {
  IAmOwner = TRUE;
  myFD = open(filename.c_str(), flags);
  insert();
}
*/

File::File(int fd, int owner) {
  myFH = shared_ptr<FileHandle>(new FileHandle(fd, owner));
}

File::File(const File& f) {
  myFH = f.myFH;
}

File::~File() {
}

int File::OTRead(void* buffer, int bytes, Timeval timeout) {
  int myFD = FD();
  //  cout << "OTRead Trying to read " << bytes  << " bytes." << endl;
  int bytesread;
  int retval;

  if( bytes >= 0 && myFD >=0) {
    int i_timeout = (timeout.sec()*1000) + (timeout.usec() / 1000); // timeout in milliseconds
    pollfd pfd;
    pfd.fd = myFD;
    pfd.events = POLLIN;
    // peek to see if there is any data there without getting locked up.
    retval = poll(&pfd, 1, i_timeout); 
    if( retval == 0 ) {
      return 0;
      // Else if the read flag was successfully returned.
    } else if( retval > 0 && (pfd.revents & POLLIN) ) {
      bytesread = read(myFD, buffer, bytes);
      return bytesread;
    }
  } 
  
  return -1;
}


int File::OTWrite(void* buffer, int bytes, Timeval timeout) {
  int myFD = FD();
  int retval;
  int byteswritten;

  if( bytes >= 0 && myFD >=0) {
    int i_timeout = (timeout.sec()*1000) + (timeout.usec() / 1000); // timeout in milliseconds
    pollfd pfd;
    pfd.fd = myFD;
    pfd.events = POLLOUT;
    // peek to see if writing will get locked up.
    retval = poll(&pfd, 1, i_timeout); 
    if( retval == 0 ) {
      return 0;
      // Else if the write flag was successfully returned.
    } else if( retval > 0 && (pfd.revents & POLLOUT) ) {
      byteswritten = write(myFD, buffer, bytes);
      return byteswritten;
    }
  } else {
    return -1;
  }
}





int File::BRead(void* buf, int bytes, Timeval timeout) {

  //  cout << "File:BRead trying to read " << bytes
  //       << " bytes.  Timeout = " << timeout
  //       << " and FD = " << FD() << endl;
  char* buffer = (char*) buf;
  Timeval before, after;
  before = TVNow();

  int bytes_read=0;
  int r;
  while( bytes_read < bytes) {
    r = OTRead(buffer+bytes_read, bytes-bytes_read);
    //    cout << "File Bytes Read = " << r << endl;
    if ( r<0) return r; //bytes_read;
    else if(r == 0) {
      after = TVNow();
      if( (after - before) > timeout) {
	return bytes_read;
      }
    } else {
      before = TVNow();
      bytes_read += r;
    }
  }
  return bytes_read;
}

int File::BWrite(void* buf, int bytes, Timeval timeout) {
  char* buffer = (char*) buf;
  Timeval before, after;
  before = TVNow();

  int written=0;
  int r;
  while( written < bytes) {
    r = OTWrite(buffer+written, bytes-written);
    if ( r<0) return r; //read;
    else if(r == 0) {
      after = TVNow();
      if( (after - before) > timeout) {
	return written; //read;
      }
    } else {
      before = TVNow();
      written += r;
    }
  }
  return written;
}

int File::FD() {
  return (*myFH).FD();
}
