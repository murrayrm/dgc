
// replace with the name of your header file
#include "Template.hh"



// Use the form given below, changing Template to the name
// of your module. (only need to change function prototypes,
// the function code will stay the same).

  // to add Template struct to a message
Mail& operator << (Mail& ml, const Template& rhs) {

  // see Kernel/Misc/Mail/Mail.hh for QueueTo
  // definition (just memory copies)
  ml.QueueTo( &rhs, sizeof(rhs));
  return ml;
}

  // to read back Template struct from a message
Mail& operator >> (Mail& ml, Template& rhs) {

  // see Kernel/Misc/Mail/Mail.hh for DequeueFrom
  // definition (just memory copies)
  ml.DequeueFrom( &rhs, sizeof(rhs));
  return ml;
}
