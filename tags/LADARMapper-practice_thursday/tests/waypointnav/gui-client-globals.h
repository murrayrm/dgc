/* Constants */

/* size and character of goodness maps */
#define MAX_GOODNESS			255
#define GOODNESS_COUNT			101
#define GOODNESS_ANGULAR_SPREAD		0.6	/* radians */

/* TCP port offsets from the GUI_PORT_BASE */
#define LADAR_PORT_OFFSET		1
#define STEREOVISION_PORT_OFFSET	2
#define ARBITER_PORT_OFFSET		3
#define STATE_ESTIMATOR_PORT_OFFSET	4
#define GLOBAL_PLANNER_PORT_OFFSET	5
#define CRUISE_CONTROL_PORT_OFFSET	6

#define PORT_NAME_TO_OFFSET_MAP					\
	{ "ladar", LADAR_PORT_OFFSET },				\
	{ "stereovision", STEREOVISION_PORT_OFFSET },		\
	{ "arbiter", ARBITER_PORT_OFFSET },			\
	{ "state-estimator", STATE_ESTIMATOR_PORT_OFFSET },	\
	{ "global-planner", GLOBAL_PLANNER_PORT_OFFSET  }, 	\
	{ "cruise-control", CRUISE_CONTROL_PORT_OFFSET  }


/* physical constants */
#define FEET_TO_METERS 0.3048

#include "contest-info.h"
