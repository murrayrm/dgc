// obd.cc

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>


#include "waypointnav.hh"

// RMM, 19 Dec 03: commented out next line; defined in OBDI.h
// const int OBDSerialPort = 1;  - defined in OBDII.h

extern int SIM;

int InitOBD(DATUM &d) {
  int tmp;
  if(!SIM){
    cout << "Initializing OBD-II" << endl;
    OBD_Init(OBDSerialPort);
 
    cout << "OBD-II Init Response" << endl;
  }

  d.obdValid = FALSE;
  return TRUE; // success
}

void GrabOBDLoop(DATUM &d) {
  int i=0;
  float RPM, Speed;

  while(d.shutdown_flag == FALSE) {
    if( i == 0 ) {
      // grab Speed
      if(!SIM){
        Speed = Get_Speed();
      }
      else{
        Speed = d.SS.speed;
      }
      if(Speed > 0.0) {
	boost::recursive_mutex::scoped_lock lock(d.obdLock);
	d.obdSpeed = Speed;
	d.obdValid = TRUE;
	d.obdNewData = TRUE;
      }
    } else if( i == 1) {
      // grab RPM
      if(!SIM){
        RPM = Get_RPM();
      }
      else{
        RPM = 0;
      }
      if(RPM > 0.0) {
	boost::recursive_mutex::scoped_lock lock(d.obdLock);
	d.obdRPM = RPM;
	d.obdValid = TRUE;
	d.obdNewData = TRUE;
      }
    }
    i = (i+1) % 2;
  }

}
