//
// global.cc
//
// Author: Rich Petras
// Date: 
//
//

#include "WaypointNav.hh" // standard includes, arbiter, datum, ...

extern int SIM; // get the simulation flag from the main function
extern int PRINT_VOTES; // get the print_votes flag from the main function

// ADDED BY LARS CREMEAN for Sparrow display 
extern double waydisp_global_p[];
extern double waydisp_global_v[];
extern int waydisp_global_g[];

// Return value: steering angle in -1(left) to 1(right)
double f_Steering(Vector curPos, Vector curWay, double curHeadDeg) {
  // Find the angle between curWay and curHead, set it as the new steering
  double steer = -get_deflection(curPos, curWay, curHeadDeg);

  // we're squaring the magnitude of the steering here for some reason
  //steer = steer * fabs(steer);

  // Saturate the steering command to be in [-1, 1]
  if (fabs(steer) > MAX_STEER) {
    if (steer > 0)
      return 1.0;
    else
      return -1.0;
  }
  else {
    return steer/MAX_STEER;
  }

} // end f_Steering(Vector curPos, Vector curWay, Vector curHead)


////////////////////////////////////////////////////
// Return value: desired velocity at this point
// Needs a body that calculates a good velocity given state & possibly steering
double f_Velo(double curVelo, Vector curPos, Vector curWay)
{  
  double des_velo;

  // Check to see what the max speed possible is
  des_velo = 20;  // m/s

  return des_velo;
}
///////////////////////////////////////////////////

void WaypointNav::GlobalLoop() {

  pair<double, double> result;
  Vector currentWaypoint;
  // Now the state has a heading in degrees
  Vector pos;
  double heading;
  Timeval tv;
  double posE, posN, curSpeed=0;

  while( !ShuttingDown() ) {

    if (d.wp.get_current().num != d.wp.get_size()) 
    {
      currentWaypoint.N = (double) d.wp.get_current().northing;
      currentWaypoint.E = (double) d.wp.get_current().easting;
      pos.N = d.SS.Northing;
      pos.E = d.SS.Easting;

      // Are we at the waypoint?
      if (withinWaypoint(currentWaypoint, pos, d.wp.get_current().radius)) 
      {
	d.wp.get_next();   // Jump to the nexts wp
	currentWaypoint.N = (double) d.wp.get_current().northing;
	currentWaypoint.E = (double) d.wp.get_current().easting;
      }

      result.first = f_Velo(curSpeed, pos, currentWaypoint);   // Velocity
      result.second = f_Steering(pos, currentWaypoint, d.SS.Yaw);  // Steering

      if( 0 ) { // suppress these printouts
	printf(" Waypoint %d: N = %10.3f, E = %9.3f\n", 
			d.wp.get_current().num, currentWaypoint.N, currentWaypoint.E);
	printf("--GlobalLoop-- Current Position: N = %10.3f, E = %9.3f\n", pos.N, pos.E);
	printf("--GlobalLoop-- Relative Position: N = %10.3f, E = %9.3f\n", 
			currentWaypoint.N-pos.N, currentWaypoint.E-pos.E);
	printf("--GlobalLoop-- result.first (vel) = %10.3f, result.second (steer) = %9.3f\n", 
			result.first, result.second );
	printf("--GlobalLoop-- Heading(deg): %10.3f\n", d.SS.Yaw);
      }
    }
    else 
    {
      cout << "[GlobalLoop] Waypoint following done" << endl;
      cout << "[GlobalLoop] Calling ForceKernelShutdown()!" << endl;
      ForceKernelShutdown();
      // Reset the brake so we are guaranteed to stop the vehicle
      result.first = 0;               // Velocity
      result.second = 0;              // Steering
    }
    usleep_for(100000);      // Evaluation rate

    // lock DFE (??????)
    boost::recursive_mutex::scoped_lock slDFE(d.DFELock);
    int goodness = 0;
    double timestamp = 0;        // Dummy for now
    Voter::voteListType wayPt = d.voterArray[d.globalNum].getVotes();

    // Now wayPt should contain all the right arcs to be evaluated.
    // So evaluate them.
    int i = 0;
    for (Voter::voteListType::iterator iter = wayPt.begin();
		    iter != wayPt.end(); ++iter,++i) {

      // Calculate goodness by looking at how close to the desired steering
      // the angle is:       
      // this one scales the steering linearly between 0 and MAX_GOODNESS
      // for evaluating, respectively, exactly where we want to steer and 
      // MAX_STEER when we want to steer to -MAX_STEER (or vice versa)
      /*      goodness = max( 0, (int)( round(MAX_GOODNESS 
	      - fabs( (iter->phi)/MAX_STEER - result.second) * MAX_GOODNESS) ) );
	      */      // we'll have to think smarter about how the arbiter evaluates these
      // votes.  we don't want to give a zero vote just because we want to 
      // turn the other way 
      // let's try to make the same linear scaling, but between some
      // parameter (e.g. 100) and MAX_GOODNESS
      int ggparam = 100;
      goodness = max( ggparam, (int)( round(MAX_GOODNESS
		 - fabs( (iter->phi)/MAX_STEER - result.second)/2.0 *
	         (MAX_GOODNESS - ggparam) ) ) );
      // assign the actual vote goodness
      iter->goodness = goodness;

      // iter->velo = veloLookupTable(iter->phi);   // doesn't exist yet
      // cout << "global.cc -- ideal heading velocity: " << result.first << endl;
      float diff = fabs( (iter->phi)/MAX_STEER - 0 ); // [0,1]
      //      iter->velo = (MAX_VELO - MIN_VELO) * ( 1 - diff) + MIN_VELO;
      iter->velo = (MAX_VELO - MIN_VELO) * pow(1 - diff, 2) + MIN_VELO;
      //    cout << "global.cc -- velocity: " << iter->velo << endl; 

      // ADDED BY MIKE THIELMAN for Sparrow display 
      waydisp_global_v[i] = iter->velo;
      waydisp_global_p[i] = iter->phi * 180.0 / M_PI;
      waydisp_global_g[i] = goodness;
    }

    // Update this evaluator's Voter's voteList.
    d.voterArray[d.globalNum].updateVotes(wayPt, timestamp);

  }  // end of while(shutdown == false)

  cout << "--GlobalLoop: done--" << endl;
} // end GlobalLoop(DATUM &d)



// Reads in waypoints from the specified file (hardcoded now)
// Waypoint file format is:  Northing  Easting
void WaypointNav::InitGlobal(){

  cout << "[InitGlobal] Starting InitGlobal()" << endl;
  cout << "[InitGlobal] d.globalNum = " << d.globalNum << endl;
  cout << "[InitGlobal] NUMARCS = " << NUMARCS << endl;
  cout << "[InitGlobal] LEFTPHI = " << LEFTPHI << endl;
  cout << "[InitGlobal] RIGHTPHI = " << RIGHTPHI << endl;

  // Add a global Voter in d.demoArb
  d.voterArray[d.globalNum] = Voter(NUMARCS, LEFTPHI, RIGHTPHI);
  //  Voter blah(NUMARCS, LEFTPHI, RIGHTPHI);
  d.globalWeight = GW;
  cout << "Adding Global voter" << endl;
  d.demoArb.addVoter(&(d.voterArray[d.globalNum]), d.globalWeight);
  cout << "Added Global Voter" << endl;

  d.wp.read("waypoints.gps");
  cout << " Done reading waypoint data" << endl;

}
