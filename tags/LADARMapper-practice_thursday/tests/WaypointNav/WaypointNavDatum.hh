#ifndef WAYPOINTNAVDATUM_HH
#define WAYPOINTNAVDATUM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "vehlib/cruise.h"

#include "../../Arbiter/Voter.h"
#include "../../Arbiter/Arbiter.h"
#include "global.hh"
#include "../../include/Constants.h" //for vehicle constants, ie serial ports, etc.

#include "../../Ladar/Ladar.h" // for LADAR_NUM_SCANPOINTS, ...

#include "../../state/imu/rot_matrix.hh"
#include "../../Global/waypoints.h"

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"
#include "vehlib/VDrive.hh"

using namespace std;

#define NUM_VOTERS 3

const double MAX_VELO  = 15.0;      // 12/6/2003
const double MIN_VELO  = 10.0;      // 12/6/2003
const double MAX_SPEED = 15.0;      // m/s

const int GW = 0;                           // global voter weight constant 
const int DFEW = 0;                         // DFE voter weight constant
const int LOCAL_W = 0;                      // Ladar voter weight constant
const int LADAR_W = 1;                      // Ladar voter weight constant




struct WaypointNavDatum {
  
  // for data logging 
  int TestNumber; 
  string TestNumberStr;

  // Time zero
  Timeval TimeZero;

  // Accel Data
  float TargetVelocity;  // in m/s
  float Accel; // normalized value in [-1,1], maps to throttle/brake

  // Steering Data
  double Steer; // command to the steering actuator
  double Yaw; // actual heading

  // Arbiter Data
  Arbiter demoArb;
  // Voters - change Arbiter to use an array of Voters???, maybe a Vector
  // TODO: Straighten this stuff out
  int numVoters;
  Voter * voterArray;
  //Voter voterArray[NUM_VOTERS];

  // State struct from VState
  boost::recursive_mutex StateStructLock;
  VState_GetStateMsg SS;

  Waypoints wp;

  // Global Data
  boost::recursive_mutex globalLock;
  int globalNum;
  int globalWeight;

  // DFE Data
  boost::recursive_mutex DFELock;
  int DFEnum;
  int DFEweight;
  
  // Ladar Data
  //int LadarNum;
  //int LadarWeight;
  
  // Local Data
  //boost::recursive_mutex LocalLock;
  //int LocalNum;
  //int LocalWeight;

};


#endif
