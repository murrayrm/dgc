#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>


#include "cruisemain.hh"



int InitMag(DATUM &d) {
  int tmp;
  cout << "Initializing Magnetometer" << endl;
  tmp = mm_init(DEFAULT_MAGNETOMETER_SERIAL_PORT);
  cout << "Mag Init Response = " << tmp << endl;

  d.magValid = FALSE;
  if( tmp == 0 ) {
    return TRUE; // success
  } else {
    return FALSE;
  }
}

void GrabMagLoop(DATUM &d) {
  int i;
  float localMagBuffer[7];

  while(d.shutdown_flag == FALSE) {
    // Read Magnetometer
    //cout << "Getting Magnetometer" << endl;
    // if something goes wrong, just start the loop over 
    if((mm_send_command(MM_LC_SC, localMagBuffer, mm_command_return_num[MM_LC_SC]))<0) {
      fprintf(stderr, "Magnetometer: Error reading last calibration score\n");
      continue;
    }

    if((mm_send_command(MM_OW_UP, localMagBuffer, mm_command_return_num[MM_OW_UP]))<0) {
      fprintf(stderr, "Magnetometer: Error output word update\n");
      continue;
    }

    /*cout << "Heading = " << localMagBuffer[0] 
           << ", Pitch = " << localMagBuffer[1]
            << ", Roll = " << localMagBuffer[2] << endl; */
    //ss.heading = localMagBuffer[0];
    //ss.pitch   = localMagBuffer[1];
    //ss.roll    = localMagBuffer[2];
    //cout<<"Got Mag"<<endl;

    // if we get here then we successfully read from the magnetometer
    // so we can update the datum

    // lock the magnetometer datum first
    boost::recursive_mutex::scoped_lock lock(d.magLock);
    for(i=0; i<7; i++) {
      d.magBuffer[i] = localMagBuffer[i];
    }
    d.magValid = TRUE;
    d.magNewData = TRUE;
    // and do it all again
  }
}
