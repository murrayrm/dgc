
/* This structure should be modified to contain any IMU data
 * that you want transmitted.  (needs to be less that a few kilobytes
 * due to UDP packet length restrictions).
 */
typedef struct 
{
  unsigned n, e;
} IMUReading;
