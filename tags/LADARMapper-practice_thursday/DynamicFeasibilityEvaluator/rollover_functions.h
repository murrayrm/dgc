
#ifndef __ROLLOVER_FUNCTIONS_H__
#define __ROLLOVER_FUNCTIONS_H__

double steering_angle_2_arc_radius( double s_angle );


void gravity_components_in_vehicle_ref_frame(const double yaw, const double pitch, const double roll, 
        double *g_z, double *g_y, double *g_z);
/* Given yaw, pitch, roll, figure out vertical and lateral components of gravity in the 
 * vehicle reference frame */


void min_max_lateral_acceleration_for_rollover_safety(const double g_z, const double g_y, const double T_roll,
        double *a_R, double *a_L);
/* Given rollover safety threshold compute min and max lateral accelerations */
    /* Inputs:
     *  g_z : [m/s^2] downward component of gravity (in vehicle ref frame)
     *  g_y : [m/s^2] lateral  component of gravity (in vehicle ref frame)
     *  T_roll: [unitless] rollover safety threshold
     *
     *  Outputs:
     *  a_R : [m/s^2] acceleration for which rollover safety threshold is achieved for a rightward roll
     *  a_L : [m/s^2] acceleration for which rollover safety threshold is achieved for a leftward roll
     *
     *  NOTE: the safe lateral accelerations, a_safe are in the range
     *          a_R <= a_safe <= a_L
     */


double rollover_coefficient(const double a_T, const double g_z, const double g_y);
// given the current lateral acceleration (derived from current velocity and arc radius),
// compute the rollover coefficient
/* 
 * Inputs:
     *  a_T : [m/s^2] lateral acceleration of vehicle due to following specified arc path at specified velocity
     *  g_z : [m/s^2] downward component of gravity (in vehicle ref frame)
     *  g_y : [m/s^2] lateral  component of gravity (in vehicle ref frame)
     *
 * Outputs:
     *  c_roll : [unitless] rollover safety coefficient.
     *                  c_roll = 
     *                      0 if vehicle is perfectly stable
     *                      magnitude 1 if on the verge of rolling over
     *                      magnitue > 1 if there is a rollover torque present
     *                      c_roll < 0 if leaning to the right
     *                      c_roll > 0 if leaning to the left
     *                      so c_roll < -1 => rolling rightward
     *
 */



void min_max_velocity_for_rollover_safety(const double a_R, const double a_L, const double arc_radius, 
        double *min_v, double *max_v);
// Given the current arc and the lateral acceleration limits to stay within some rollover safety threshold,
// compute the corresponding min and max forward velocities
/* 
 * Inputs:
     *  a_R : [m/s^2] acceleration for which rollover safety threshold is achieved for a rightward roll
     *  a_L : [m/s^2] acceleration for which rollover safety threshold is achieved for a leftward roll
     *  arc_radius : [m] the radius of the arc being considered
     *                  NOTE: arc_radius == 0 signifies moving straight ahead
     *                          (more simple representation than arc_radius = infinity)
     *
 * Outputs:
     *  min_v : [m/s] minimum forward velocity to be within the rollover safety threshold
     *  max_v : [m/s] maximum forward velocity to be within the rollover safety threshold
 */

#endif
