% LG040116.2347
% plot 'realistic' curves of rollover coefficient and path goodness
% in order to figure out how to tune goodness function 

% define constants
    VEHICLE_H = 1.95/2;        % [m], height of Center of Mass from ground
                            % TODO: NOTE: this is the worst case scenario, since cannot know from spec sheet
                            %
    VEHICLE_L = 1.651/2.0;     % [m], distance of left wheel to projection of CM down on axle
    VEHICLE_R = 1.651/2.0;     % [m], distance of right wheel to projection of CM down on axle

    VEHICLE_AXLE_DISTANCE = 2.946;     % [m] distance between front and rear axles

    GRAVITY = 9.8;  % [m/s^2]

    MAX_GOODNESS = 100.0;
    MAX_SPEED = 15.0; % [m/s]

    EPSILON = 10e-5;

    Thresh_roll = 1; % the rollover safety threshold, to compare to abs(c_roll)

N_Votes = 25;
MAX_STEER_ANGLE = 25; % [deg]
phi = [-MAX_STEER_ANGLE:(2*MAX_STEER_ANGLE/(N_Votes-1)):MAX_STEER_ANGLE] / 180*pi; % [rad]
% phi = -0.44:.04:0.44; % radians, steering angle

current_velocity = 15.0; % [m/s]

% assume planar surface, gravity straight downwards
yaw = 0; pitch = 0; roll = 0;

% % on a 45 deg slope going down rightwards
% yaw = 0; pitch = 0; roll = 45/180*pi;
% 
% % on a 45 deg slope going down lefwards
% yaw = 0; pitch = 0; roll = -45/180*pi;
% 
% % on a 10 deg slope going down lefwards
% yaw = 0; pitch = 0; roll = -10/180*pi;

% compute components of gravity in vehicle reference frame
    % g_z : down, g_y : lateral (right) g_x : forward
    R = yaw_pitch_roll_2_matrix(yaw, pitch, roll);
        %  yaw_pitch_roll_2_matrix(yaw, pitch, roll, x, y, z);
        % x, y, z define the rotation matrix from vehicle ref frame to inertial ref frame;
        % R_vw = [x y z], v = vehicle, w = world (inertial)
        % 
        % so R_wv = R_vw' = [x'; y'; z']
        % 
        % gravity vector in inertial ref frame is 
        % 
        %  g_w = [0; 0; 1] * GRAVITY
        %  
        % so gravity vector in vehicle ref frame is
        % 
        %  g_v = R_wv g_w = R_vw' g_w = GRAVITY * [x[2]; y[2]; z[2]]
    

    g_z = GRAVITY * R(3,3); % z[2]; % vertical (in vehicle ref frame) component of gravity
    g_y = GRAVITY * R(3,2); % y[2]; % lateral  (in vehicle ref frame) component of gravity
    g_x = GRAVITY * R(3,1); % x[2]; % forward  (in vehicle ref frame) component of gravity

% compute min and max lateral acceleration for rollover safety
    a_L = ((VEHICLE_L+VEHICLE_R)/(2*VEHICLE_H)*(1+Thresh_roll) - VEHICLE_R/VEHICLE_H)*g_z + g_y;
    % a_L is minimum acceleration permissible before rightward roll exceeds rollover threshold
    
    a_R = ((VEHICLE_L+VEHICLE_R)/(2*VEHICLE_H)*(1-Thresh_roll) - VEHICLE_R/VEHICLE_H)*g_z + g_y;
    % a_R is maximum acceleration permissible before leftward roll exceeds rollover threshold

for i = 1:length(phi)
    % compute arc radius
        if (abs(phi(i)) < EPSILON )
            arc_radius(i) = 0;
        else
            arc_radius(i) =  VEHICLE_AXLE_DISTANCE / tan(phi(i));
        end

        % arc_radius is positive for rightward turns, negative for leftwards turns,
        % and ZERO for moving straigh ahead (more useful than infinity)
      
    % compute total resulting lateral acceleration, a_T(i), given arc path
        if (arc_radius(i) == 0)
            % special case of moving straight ahead, total lateral acceleration is zero
            a_T(i) = 0;
        else
            a_T(i) = current_velocity*current_velocity/arc_radius(i);
        end

    % compute rollover coefficient, c_roll
        c_roll(i) = ((VEHICLE_L-VEHICLE_R)*g_z + 2*VEHICLE_H*(a_T(i) - g_y)) / ((VEHICLE_L+VEHICLE_R)*g_z);

    % compute min and max safe velocities
        if (arc_radius(i) ==0)
            % moving straight ahead, we can have any forward velocity
            % and not risk rollover
            % UNLESS one of the max accelerations is actually ZERO
            % TODO: may want to replace the return value with something that
            %          signals "no limit"

            % NOTE: TODO: ??? will this make moving straight ahead look too good on a dangerous slope ???
            if (abs(a_R)<1e-3 | abs(a_L)<1e-3)
                min_safe_velocity(i) = 0;
                max_safe_velocity(i) = 0;
            else
                min_safe_velocity(i) = 0;
                max_safe_velocity(i) = MAX_SPEED; % NOTE: TODO: ??? will this make moving straight ahead look too good on a dangerous slope ???
            end
        else
            if (arc_radius(i) > 0)
                % min velocity
                v_squared = a_R * arc_radius(i);
                if (v_squared < 0)
                    min_safe_velocity(i) = 0;
                else
                    min_safe_velocity(i) = sqrt(v_squared);
                end
                
                % max velocity
                v_squared = a_L * arc_radius(i);
                if (v_squared < 0)
                    max_safe_velocity(i) = 0;
                else
                    max_safe_velocity(i) = sqrt(v_squared);
                end
            else
                % min velocity
                v_squared = a_L * arc_radius(i);
                if (v_squared < 0)
                    min_safe_velocity(i) = 0;
                else
                    min_safe_velocity(i) = sqrt(v_squared);
                end

                % max velocity
                v_squared = a_R * arc_radius(i);
                if (v_squared < 0)
                    max_safe_velocity(i) = 0;
                else
                    max_safe_velocity(i) = sqrt(v_squared);
                end
            end
        end

    % compute velocity vote
        if (max_safe_velocity(i) > MAX_SPEED) 
            velocity_vote(i) = MAX_SPEED;
        else    
            velocity_vote(i) = max_safe_velocity(i);
        end

    % compute goodness vote
        if (abs(c_roll(i)) > Thresh_roll) 
            goodness_vote(i) = 0;
        else 
            goodness_vote(i) = MAX_GOODNESS * (1.0 - abs(c_roll(i))/Thresh_roll);
        end
end

% plot some results
    % figure(101); clf
        % subplot(5,1,1);
        % plot(180/pi*phi,'.-');
        % title(['phi [deg] for speed = ' num2str(current_velocity) ' m/s']);

        % subplot(5,1,2);
        % plot(c_roll,'.-');
        % title('c_{roll}');

        % subplot(5,1,3);
        % plot(a_T,'.-');
        % title('a_T');

        % subplot(5,1,4);
        % plot(goodness_vote,'.-');
        % title('goodness vote');

        % subplot(5,1,5);
        % plot(velocity_vote,'.-');
        % title('velocity vote');

    figure(102); clf
        subplot(4,1,1);
        plot(180/pi*phi,c_roll,'.-');
        title(['c_{roll} versus phi (degrees) for speed = ' num2str(current_velocity) ' m/s']);
        ylabel('c_{roll}');

        subplot(4,1,2);
        plot(180/pi*phi,a_T,'.-');
        title('a_T versus phi ');
        ylabel('a_T [m/s^2]');

        subplot(4,1,3);
        plot(180/pi*phi,goodness_vote,'.-');
        title('goodness vote versus phi ');
        ylabel('goodness vote')

        subplot(4,1,4);
        plot(180/pi*phi,velocity_vote,'.-');
        title('velocity vote versus phi ');
        ylabel('velocity vote [m/s]');
        xlabel('steering angle phi [degrees]')

    figure(104); clf; name('arc radius versus steering angle');
        plot(phi*180/pi, arc_radius,'.-');
        title('arc radius versus steering angle');
        xlabel('steering angle [deg]');
        ylabel('arc radius [m]');

