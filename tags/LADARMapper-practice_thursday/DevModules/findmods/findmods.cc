#include "MTA/Kernel.hh"

class findmods : public DGC_MODULE {

 public:
  findmods() : DGC_MODULE( MODULES::Passive, "findmods", 0) {}

  void Init() { exit(0); }
};


int main(int argc, char **argv) {

  Register(shared_ptr<DGC_MODULE>(new findmods));
  StartKernel();

  return 0;
}
