#include "MatlabDisplay.hh"

using namespace std;

const int LOG_OPT = 10;
const int HELP_OPT = 11;
int loggedDataRun = 0;        // Whether we're viewing logged data.
                              // Default is viewing real-time data.
char *logFileInfo;


int main(int argc, char **argv) {

  //---Argument Checking---------------------------------------------------//
  int c;

  while (1) {
    int option_index = 0;
    static struct option long_options[] = {
      {"logged",       1, 0,             LOG_OPT},
      {"help",         0, 0,             HELP_OPT},
      {0,0,0,0}
    };

    c = getopt_long_only (argc, argv, "", long_options, &option_index);
    if(c == -1) break;

    switch(c) {
      case LOG_OPT:
        loggedDataRun = 1;
	// logFileInfo should be the full filename of one of the 
	// logs in the data set
	logFileInfo = strdup(optarg);
        break;
      case HELP_OPT:
        cout << endl << "MatlabDisplay Usage:" << endl << endl;
        cout << "MatlabDisplay [-logged]" << endl;
        cout << "  -logged  View logged data, given one of the log file names"
	     << endl;
        cout << "  -help          Displays this help" << endl << endl;
        exit(0);
    }
  } // end while(1)

  //--Start the MTA By registering a module--------------------------------//
  // and then starting the kernel

  Register(shared_ptr<DGC_MODULE>(new MatlabDisplay));
  StartKernel();

  return 0;
}
