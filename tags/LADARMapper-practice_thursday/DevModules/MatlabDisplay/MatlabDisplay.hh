#ifndef MATLABDISPLAY_HH
#define MATLABDISPLAY_HH

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "MatlabDisplayDatum.hh"

#include "engine.h" // The MATLAB engine

#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include <unistd.h>
#include <iostream>
#include <getopt.h>

#define  BUFSIZE  1024  // Buffer for MATLAB output
#define  GENMAP_DISPLAY_DEV

using namespace std;
using namespace boost;

class MatlabDisplay : public DGC_MODULE 
{
    public:

        // and change the constructors
        MatlabDisplay();
        ~MatlabDisplay();

        // The states in the state machine.
        // Uncomment these if you wish to define these functions.

        void Init();
        void Active();
        //  void Standby();
        void Shutdown();
        //  void Restart();

        // The message handlers. 
        //  void InMailHandler(Mail & ml);
        //  Mail QueryMailHandler(Mail & ml);

        // the status function.
        //string Status();


        // Any functions which run separate threads

        // Method protocols
        int  InitMatlabDisplay();
        void MatlabDisplayUpdateVotes();
        void MatlabDisplayShutdown();

        void UpdateState();    // grabs from VState and stores in datum.
	void UpdateArbiterVoters();
        void ReceiveCAEgenMap();  // grabs from CorridorArcEvaluator
        void UpdateCAEgenMap();  // grabs from CorridorArcEvaluator
	void PlotGenMap(); // gets and plots a new genMap 

	void SparrowDisplayLoop();
	void UpdateSparrowVariablesLoop();

    private:

        // Hook to Matlab Engine
        Engine *_ep;

	// Variable for MATLAB output
	char buffer[BUFSIZE];

        // ADD CODE HERE: Define variables to interface with Matlab
	// these are the matlab variables where we will store plotting data
        // example variables
        mxArray *_votes_M; 
        mxArray *_vel_M;
	// special array approximately matches the state struct.
	// Easting, Northing, Altitude, Vel_E, Vel_N, Vel_U (1-6)
	// Pitch, Roll, Yaw, PitchRate, RollRate, YawRate   (7-12)
	// Speed                                            (13)
        mxArray *_currSS_M; 

	// these are the C counterparts; they point inside the mxArray to where
	// the array of doubles is actually stored (mxArray has additional 
	// wrapper info, such as variable type (double, complex, uint8), 
	// as well as the current dimensions of the variable)
        // example variables
        double *_votes_C; 
        double *_vel_C;   	
	// pointer to special state array
        double *_currSS_C;

        // counter for going through the logfiles
	mxArray *_counter_M;
	double *_counter_C;
	        
        // This variable is not related to the above variables.
	// GET RID OF this variable in the code when you don't want to produce the silly fake data anymore
        int _counter; // counter variable to produce fake data (since we haven't programmed message passing yet)

   
        // array that represents the data in the genMap that is sent
	// across MTA from the CorridorArcEvaluator
	mxArray *_mapData_M;
	mxArray *_mapNumRows_M;
	mxArray *_mapNumCols_M;
	mxArray *_mapRowRes_M;
	mxArray *_mapColRes_M;
	mxArray *_mapLeftBottomRow_M;
	mxArray *_mapLeftBottomCol_M;
	mxArray *_mapLB_Northing_M;
	mxArray *_mapLB_Easting_M;

        // pointer to map data
	double *_mapData_C;
	double *_mapNumRows_C;
	double *_mapNumCols_C;
        double *_mapRowRes_C;
	double *_mapColRes_C;
	double *_mapLeftBottomRow_C;
	double *_mapLeftBottomCol_C;
	double *_mapLB_Northing_C;
	double *_mapLB_Easting_C;

        //and a datum named d.
        MatlabDisplayDatum _d;

	// Matlab variables for displaying votes
        mxArray *_voters_goodness_M;
        mxArray *_voters_velocity_M;

        mxArray *_combined_goodness_M;
        mxArray *_combined_velocity_M;

	// zero oriented index of best arc chosen by arbiter
        mxArray *_arbiter_best_arc_M;

    // C counterpart pointing to the array inside the Matlab mxArray structures
        double *_voters_goodness_C;
        double *_voters_velocity_C;

        double *_combined_goodness_C;
        double *_combined_velocity_C;

        double *_arbiter_best_arc_C;

};


// enumerate all module messages that could be received
namespace MatlabDisplayMessages
{
    enum {
        // we are not yet receiving messages.
        // A place holder
        NumMessages
    };
};

#endif
