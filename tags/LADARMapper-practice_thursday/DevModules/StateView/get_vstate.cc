/*
** get_vstate.cc
**
**  
** 
**
*/
#include "StateView.hh"
void StateView::UpdateState()
{ 
  Mail msg = NewQueryMessage( MyAddress(), MODULES::VState, VStateMessages::GetState);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> SS; // download the new state
  }

} // end WaypointNav::UpdateState() 
