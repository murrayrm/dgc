#include "GlobalPlanner.hh"

using namespace std;

int SIM;         // this is a global variable that modules use to keep track of 
                 //  whether the code should be run in simulation mode
int DISPLAY;     // this is a switch that determines whether we're using 
                 //  ncurses display mode
int PRINT_VOTES; // whether each of the voters will call printVotes
                 // functionality not yet implemented              
 
int GROW_OBSTACLES = 0; // do not grow obstacles


int main(int argc, char **argv) {
  cout << setprecision(10);
  cout << "argc = " << argc << endl;
   
  SIM = 0;
  DISPLAY = 0;
  PRINT_VOTES = 0;
  ////////////////////////////
  // do some argument checking
  if( argc > 1 ) { // if we have arguments
  
    for( int i=1; i < argc; i++ ) { 
      // go through each of the arguments and check...
      cout << "argv[ " << i << "] = " << argv[i] << endl;

      // if the "-sim" argument was specified
      if( strcmp(argv[i], "-sim") == 0 ) {
        cout << "!!! -sim flag acknowledged, using simulation environment !!!" << endl << endl;
        SIM = 1;
      }
      // if the "-pv" argument was specified
      if( strcmp(argv[i], "-pv") == 0 ) {
        cout << "!!! -pv flag acknowledged, will print votes slowly !!!" << endl << endl;
        PRINT_VOTES = 1;
      }
      // if the "-disp" argument was specified
      if( strcmp(argv[i], "-disp") == 0 ) {
        cout << "!!! -disp flag acknowledged, about to use curses display environment !!!" << endl << endl;
        DISPLAY = 1;
      }
    }
  }
  // Display a message if we don't see some options set  
  if( SIM == 0 ) {
    cout << endl;
    cout << "!!! No -sim flag detected, using real environment !!!" << endl;
    cout << "    You can Ctrl-c here if you forgot to use -sim " << endl 
	 << endl;
  }
  if( DISPLAY == 0 ) {
      cout << endl;
      cout << "!!! No -disp flag detected, using stdout for display !!!" 
	   << endl;
      cout << "    You can Ctrl-c here if you forgot to use -disp " << endl 
	   << endl;
      DISPLAY = 0;
  }
  // Done doing argument checking.
  ////////////////////////////////

  
  // Start the MTA By registering a module
  // and then starting the kernel

  Register(shared_ptr<DGC_MODULE>(new GlobalPlanner));
  StartKernel();

  return 0;
}
