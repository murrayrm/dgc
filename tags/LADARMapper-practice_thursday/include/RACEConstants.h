#ifndef QIDCONSTANTS_H
#define QIDCONSTANTS_H

// All speeds given in meters per second

// Global Constants

// MAX_SPEED is the highest speed the vehicle is ever allowed to go 
#define MAX_SPEED 10.0
// MIN_SPEED is the lowest speed the vehicle can reliably be commanded to go
#define MIN_SPEED 1.5

/************************************************************************/

// SR Camera Constants
#define SR_MAX_SPEED                            2.50      // Assuming a 10m range
#define SR_MIN_SPEED                            MIN_SPEED
#define SR_DIST_TO_OBSTACLE_ON_ARC_HALT         2.5       // Halt if obs within 2.5m
#define SR_DIST_TO_OBSTACLE_ANYWHERE_HALT       -1.0      // Ignore
#define SR_DIST_TO_OBSTACLE_ON_ARC_MAX_SPEED    10.0      // Start slowing down when obs withing 10m
#define SR_DIST_TO_OBSTACLE_ANYWHERE_MAX_SPEED  -1.0      // Ignore

// LR Camera Constants
#define LR_MAX_SPEED                            5.0       // Assuming a 20m range
#define LR_MIN_SPEED                            MIN_SPEED
#define LR_DIST_TO_OBSTACLE_ON_ARC_HALT         2.5       // Halt if obs within 2.5m
#define LR_DIST_TO_OBSTACLE_ANYWHERE_HALT       -1.0      // Ignore
#define LR_DIST_TO_OBSTACLE_ON_ARC_MAX_SPEED    20.0      // Start slowing down when obs within 20m
#define LR_DIST_TO_OBSTACLE_ANYWHERE_MAX_SPEED  -1.0      // Ignore

// LADARMapper Constants
#define LADAR_MAX_SPEED                           12.5      // Assuming a 50m range (thats our genMap size)
#define LADAR_MIN_SPEED                           MIN_SPEED 
#define LADAR_DIST_TO_OBSTACLE_ON_ARC_HALT        2.5       // Halt if obs within 2.5m
#define LADAR_DIST_TO_OBSTACLE_ANYWHERE_HALT      -1.0      // Ignore
#define LADAR_DIST_TO_OBSTACLE_ON_ARC_MAX_SPEED   50.0      // Start slowing down when obs within 50m
#define LADAR_DIST_TO_OBSTACLE_ANYWHERE_MAX_SPEED -1.0      // Ignore

/************************************************************************/

// LADAR Bumper Constants
#define BUMPER_MAX_DIST                           80.0 // Don't increase allowed velocity beyond this point
#define BUMPER_MIN_DIST                           20.0 // Don't adjust velocity if distance less than this 
#define BUMPER_SPEED                              10.0 // A point on the curve
#define BUMPER_DIST                               35.0 // A point on the curve
#define BUMPER_FIELD_OF_VIEW                        10 // Number of ladar scans to either side of center to look at
#define NO_BUMPER_SAFE_SPEED                       2.0 // Command no higher than this if bumper is not in use or disabled

// MISC Constants

// CORRIDOR_SAFE_SPEED is the speed we will go at if we are outside the
// corridor, going the wrong direction, or facing the edge of the corridor
#define CORRIDOR_SAFE_SPEED 2.0

// This is the maximum speed the DFE will allow us to go
// It's used in RaceModules/DFEPlanner/DFE.cc
// Arbiter reduces this by 1.0 for whatever reason
#define  DFE_MAX_SPEED  11.0 // m/s


#endif
