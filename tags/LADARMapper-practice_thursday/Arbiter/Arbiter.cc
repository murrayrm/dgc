/* Arbiter.cc
   Author: J.D. Salazar
   Created: 8-25-03
   Revision History: 11/10/2003 Sue Ann Hong  added updateNextArcs
   The Arbiter to be used by Team Caltech in the 2004 Darpa Grand Challenge */

#include "Arbiter.h"

#include <iostream>
using namespace std;


void Arbiter::addVoter(Voter *v, int weight)
{
  voterPairType p(v, weight);
  voterList_.push_back(p);
}


void Arbiter::updateNextArcs(double leftPhi, double rightPhi, int numArcs) {
  for(voterListType::iterator iter = voterList_.begin(); 
      iter != voterList_.end(); ++iter)
    {
      // Need a timestamp?
      iter->first->updateArcs(leftPhi, rightPhi, numArcs);
    }
}


void Arbiter::getVotes()
{
  // Need to add a feature so that a new vote list is taken only if it is
  // newer, or at least some kind of time checking.
  voteList_.clear();
  for(voterListType::iterator iter = voterList_.begin(); iter != voterList_.end();
      ++iter)
    {
      voteList_.push_back( iter->first->getVotes() );
    }
}

void Arbiter::printCombinedVotes()
{
  cout << "COMBINED VOTES" << endl;
  for(combinedVoteListType::iterator iter = combinedVoteList_.begin(); iter != combinedVoteList_.end();
      ++iter)
    {
      cout << "(Phi, Velo, Good) = (" << iter->phi << ", " << iter->velo 
	      << ", " << iter->goodness << ")" << endl;
    }
}


void Arbiter::combineVotes()
{
  combinedVoteList_.clear();

  // Why do we do this instead of using the iterator below?  -Lars
  voterListType::iterator voter = voterList_.begin();
	  
  // From the voter we can get the weight
  double voterWeight;
  double sumWeight = 0;

  //cout << "======COMBINING VOTES======" << endl;
  // iterate over the VOTER list (i here represents each of the voters in turn)
  for( voterVoteListType::iterator i = voteList_.begin(); i != voteList_.end();
	 ++i )
  {
    voterWeight = (double) voter->second;
    sumWeight += (double) voterWeight;
    //cout << "Voter Weight = " << voterWeight << endl;

    // If this is the first VOTER, create all the votes in combinedVoteList,
    // and set them by default to be equal to the first voter's vote list.
    if(i == voteList_.begin())
    {
      // j here represents the elements in a VOTE list (NOT a voter list)
      for( Voter::voteListType::iterator j = i->begin(); j != i->end(); ++j)
      {
	Vote auxVote;
        // note: we don't anymore modify the goodness according to the voter's 
	// weight.  the effect of the weight will come into the combining of the
	// votes
	auxVote.goodness = (int)j->goodness;
        auxVote.phi = j->phi;
        auxVote.velo = j->velo;
	combinedVoteList_.push_back( auxVote );
      }
    }
    else {
      // {iter} here represents a VOTE list (NOT a voter list) of COMBINED votes
      combinedVoteListType::iterator iter = combinedVoteList_.begin();

      // j here represents the elements in each voter's VOTE list (NOT voter list)
      // "for each of the votes in voter i's (not the first voter) list"...
      for( Voter::voteListType::iterator j = i->begin(); j != i->end(); ++j )
      {
	// TODO: need to add features to make sure the same arcs are being
	// compared.
	
	// make this arc's goodness a weighted average of the
	// goodnesses from each voter	
	// TODO: rewrite to not mix integer and floating point calculations 
	// (if you want to use average of votes).  Can we just make goodness
	// a double?? -Lars
	//iter->goodness += (int) (voterWeight * j->goodness / sumWeight);

	// combine the vote goodnesses and velocities 
	if( voterWeight ) { 
	  // make this arc's goodness a minimum of the voters' goodnesses
	  iter->goodness = min( iter->goodness, j->goodness ); 
	  // just take the minimum velocity to be safe 
	  iter->velo = min(iter->velo, j->velo);
	}
	++iter; // increment to the next vote for the combined vote list

      } // end iterating over votes {j}
      
    } // end else

    // Not sure why we don't just use {i}  -Lars
    voter++;
    
  } // end iterating over voters {i}

  //cout << "Sum  Weight = " << sumWeight << endl;
  //Making the average of the goodness
  // TODO: If still want to use average of goodnesses, find a better way to
  // implement this (or don't), so we don't have to comment it out every time
  // we switch from that method
  /*
  for( combinedVoteListType::iterator iter= combinedVoteList_.begin(); 
		  iter != combinedVoteList_.end (); ++iter ) {
    iter->goodness /= (int) sumWeight;
  }
  */

  
} // end Arbiter::combineVotes()


void Arbiter::pickBestArc()
{
  // This function would be better if it found the best subset of arcs above
  //  some certain threshold and picked the one in the middle.
  // Currently this algorithm is biased towards left turns.
  int maxVote = -9999;
  double bestArc = 0;
  double velocity = 0;                        

  //  cout << "--Arbiter::pickBestArc--" << endl;

  for( combinedVoteListType::iterator iter = combinedVoteList_.begin();
       iter != combinedVoteList_.end();
       ++iter )
    {
      if( maxVote < iter->goodness )
        {
          maxVote = iter->goodness;
          bestArc = iter->phi;
	  velocity = iter->velo;              
        }
    }

  actCommand_.velo = velocity;                
  actCommand_.phi = bestArc;
  actCommand_.goodness = maxVote;
}

//
// end Arbiter.cc
