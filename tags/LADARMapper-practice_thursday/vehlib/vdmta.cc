 /*
 * vdmta.cc - MTA interface for vdrive
 * 
 * RMM, 1 Jan 04
 *
 * This file contains all of the MTA interface routines required for 
 * vdrive.  For right now, this is a just a copy of Ike's test example,
 * to see if I can get everything to compile and run.
 */

#include <time.h>
#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>

#include "MTA/Kernel.hh"
#include "MTA/Misc/Time/Time.hh"

#include "sparrow/dbglib.h"
#include "vdmta.hh"
#include "VState.hh"

extern struct VState_GetStateMsg vehstate; // vehicle state
extern struct VDrive_CmdMotionMsg mtacmd; // commanded motion from vdrive.c

int mta_counter = 0;

using namespace std;

VDrive::VDrive() : DGC_MODULE(MODULES::VDrive, "Vehicle Drive", 0) {}
VDrive::~VDrive() {}
void VDrive::Active() {
  D.MyCounter=0;

  while( ContinueInState() ) {  // while stay active
    D.MyCounter++;
    dbg_info("VDrive active; count = %d", D.MyCounter);

    /* Send a message requesting position from VState */
    Mail msg = NewQueryMessage(MyAddress(), MODULES::VState, 
   			     VStateMessages::GetState);
    Mail reply = SendQuery(msg);
    reply.DequeueFrom(&vehstate, sizeof(vehstate));
    if (reply.OK()) ++mta_counter; else mta_counter = -1;
    usleep(10000);
  }
} 

void VDrive::InMailHandler(Mail& msg) {
  switch (msg.MsgType()) {
  case VDriveMessages::CmdMotion:
    /* Get the motion data and store it locally */
    msg.DequeueFrom(&mtacmd, sizeof(mtacmd));
    break;

  default:
    // let the parent mail handler handle it
    break;
  }
}
