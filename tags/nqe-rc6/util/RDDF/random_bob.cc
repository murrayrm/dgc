#include <iostream.h>
#include <fstream.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip.h>
#define rand_scale  80   //meters
#define rand_offset 
#define min_lb 1.524

int main(){
    const double velocity = 25;
    ofstream outfile("random_bob.dat");
    outfile << "#random, long distance rddf\n";
    outfile << "#type   easting    northing    radius  velocity\n";
    double northing=0, easting=0;
    double radius = 5;
    srandom(574857684);         //just an arbitrary seed I mashed in
    double sumE=0;
    double sumN=0;
    outfile << setprecision(10);
    for (int i=0; i < 5000; i++){
        outfile << "N\t" << easting << "\t" << northing << "\t" << radius << "\t"<< velocity << endl;
        long int rand_east = random();
        long int rand_north = random();    
        double new_northing = rand_scale * log((exp(1)-1)*rand_north/RAND_MAX+1) - rand_scale/4;
        double new_easting = rand_scale * log((exp(1) -1)*rand_east/RAND_MAX+1) - rand_scale/4;
        northing += new_northing;
        easting += new_easting;
        sumN += northing;
        sumE += easting;
        radius = 4*hypot(new_northing, new_easting) / rand_scale + min_lb; 
        }
}
