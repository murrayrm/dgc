#include <iostream>
using namespace std;

#include <stdlib.h>
#include <math.h>
#include "CMapPlus.hh"
#include "defs.h"
#include "RefinementStage.h"
#include "MapAccess.h"

#ifdef DO_BENCHMARK
#include "DGCutils"
extern unsigned long long integrateNEtime;
#endif


// #define NEW_INTEGRATE_NE

// for simpler access
// NOTE: n,e here are not scaled by Sf
#define theta						m_pColl_theta  [collIndex]
#define dtheta					m_pColl_dtheta [collIndex]
#define ddtheta					m_pColl_ddtheta[collIndex]
#define v               m_pColl_speed  [collIndex]
#define dv              m_pColl_dspeed [collIndex]
#define sf							m_collSF
#define n								m_pCollN[collIndex]
#define e								m_pCollE[collIndex]
#define vlimit          m_pCollVlimit[collIndex]
#define dvldN           m_pCollDVlimitDN[collIndex]
#define dvldE           m_pCollDVlimitDE[collIndex]
#define dvldtheta       m_pCollDVlimitDTheta[collIndex]

extern "C" void dgemv_(char*, int*, int*, double*, double*, int*, double*, int*, double*, double*, int*);
extern "C" void dgemm_(char*, char*, int*, int*, int*, double*, double*, int*, double*, int*, double*, double*, int*);
extern "C" double ddot_(int*, double*, int*, double*, int*);
extern "C" void daxpy_(int*, double*, double*, int*, double*, int*);


void CRefinementStage::makeCollocationData(double* pSolverState)
{
	int i;

	getSplineCoeffsTheta(pSolverState);

	char notrans = 'N';
	int rows = NUM_COLLOCATION_POINTS;
	int cols = NUM_POINTS;
	double alpha = 1.0;
	double beta = 0.0;
	int incr1 = 1;
	int incr0 = 0;

	// make theta, dtheta, ddtheta
	dgemv_(&notrans, &rows, &cols, &alpha, m_valcoeffs, &rows,
				 pSolverState, &incr1, &beta, m_pColl_theta, &incr1);
	dgemv_(&notrans, &rows, &cols, &alpha, m_d1coeffs, &rows,
				 pSolverState, &incr1, &beta, m_pColl_dtheta, &incr1);
	dgemv_(&notrans, &rows, &cols, &alpha, m_d2coeffs, &rows,
				 pSolverState, &incr1, &beta, m_pColl_ddtheta, &incr1);

	for(i=0; i<NUM_COLLOCATION_POINTS; i++)
		m_pColl_theta[i] += m_initTheta;


	m_collSF = pSolverState[NPnumVariables-1];

	// integrate through to get N,E and their gradients, as well as
	// vlimit(N,E) and its gradients. First, set up the variables for
	// the integration

	int collIndex = 0;

	n = e = 0.0;
	memset(m_pCollGradN, 0, NUM_POINTS*sizeof(m_pCollGradN[0]));
	memset(m_pCollGradE, 0, NUM_POINTS*sizeof(m_pCollGradE[0]));

	int gradVlimitIndex = 0;
	for(;
			collIndex<NUM_COLLOCATION_POINTS;
			collIndex++)
	{
		getContinuousMapValueDiffGrown(m_kernelWidth,
																	 m_pMap, m_mapLayer, collIndex>m_pointsToNodataUnpassableCutoff,
																	 m_initSpeed,
																	 n*sf + m_initN, e*sf + m_initE,
																	 theta, &vlimit, &dvldN, &dvldE, &dvldtheta);

		// integrate the (N,E) values, unless we're already at the end
		if(collIndex != NUM_COLLOCATION_POINTS-1)
			integrateNE(collIndex);
	}

	cols = NUM_SPEED_SEGMENTS;
	dgemv_(&notrans, &rows, &cols, &alpha, m_valcoeffs_speed, &rows,
				 pSolverState+NUM_POINTS, &incr1, &beta, m_pColl_speed, &incr1);
	dgemv_(&notrans, &rows, &cols, &alpha, m_d1coeffs_speed, &rows,
				 pSolverState+NUM_POINTS, &incr1, &beta, m_pColl_dspeed, &incr1);

	// 	daxpy_(&rows, &alpha, &m_initSpeed, &incr0, m_pColl_speed, &incr1);
	for(int i=0; i<NUM_COLLOCATION_POINTS; i++)
	{
		m_pColl_speed[i] += m_initSpeed;
	}
}

void CRefinementStage::NonLinearInitConstrFunc(void)
{
	const int collIndex = 0;

	m_pConstrData[0] = SCALEFACTOR_ACCEL  * m_initSpeed*dv    /sf;
	m_pConstrData[1] = SCALEFACTOR_YAWDOT * m_initSpeed*dtheta/sf;

	m_grad_solver[0][IDX_THETA]		= 0.0;
	m_grad_solver[0][IDX_DTHETA]	= 0.0;
	m_grad_solver[0][IDX_DDTHETA]	= 0.0;
	m_grad_solver[0][IDX_V]				= 0.0;
	m_grad_solver[0][IDX_DV]			= SCALEFACTOR_ACCEL * m_initSpeed/sf;
	m_grad_solver[0][IDX_SF]			= SCALEFACTOR_ACCEL * (-m_initSpeed*dv/sf/sf);
	m_grad_solver[0][IDX_N]				= 0.0;
	m_grad_solver[0][IDX_E]				= 0.0;

	m_grad_solver[1][IDX_THETA]		= 0.0;
	m_grad_solver[1][IDX_DTHETA]	= SCALEFACTOR_YAWDOT * m_initSpeed/sf;
	m_grad_solver[1][IDX_DDTHETA]	= 0.0;
	m_grad_solver[1][IDX_V]				= 0.0;
	m_grad_solver[1][IDX_DV]			= 0.0;
	m_grad_solver[1][IDX_SF]			= SCALEFACTOR_YAWDOT * (-m_initSpeed*dtheta/sf/sf);
	m_grad_solver[1][IDX_N]				= 0.0;
	m_grad_solver[1][IDX_E]				= 0.0;
}

void CRefinementStage::NonLinearTrajConstrFunc(int constr_index_snopt, int collIndex)
{
#if VCONSTR
	m_pConstrData[constr_index_snopt + VCONSTR_IDX] = SCALEFACTOR_SPEED * (vlimit - v);
#endif
#if ACONSTR
	m_pConstrData[constr_index_snopt + ACONSTR_IDX]		= SCALEFACTOR_ACCEL * v*dv/sf;
#endif
#if PHICONSTR
	// This is actually tan(phi)
	m_pConstrData[constr_index_snopt + PHICONSTR_IDX]	= SCALEFACTOR_TANPHI * VEHICLE_WHEELBASE / sf * dtheta;
#endif
#if PHIDCONSTR
	m_pConstrData[constr_index_snopt + PHIDCONSTR_IDX]	= SCALEFACTOR_PHIDOT * VEHICLE_WHEELBASE * v * ddtheta
		/sf/sf/( 1.0 + m_pConstrData[constr_index_snopt + PHICONSTR_IDX]*m_pConstrData[constr_index_snopt + PHICONSTR_IDX] );
#endif
#if ROLLOVERCONSTR
	m_pConstrData[constr_index_snopt + ROLLOVERCONSTR_IDX]	= SCALEFACTOR_ROLLOVER * v*v * dtheta / sf;
#endif


#if VCONSTR
	m_grad_solver[VCONSTR_IDX][IDX_THETA]		= SCALEFACTOR_SPEED * dvldtheta;
	m_grad_solver[VCONSTR_IDX][IDX_DTHETA]	= 0.0;
	m_grad_solver[VCONSTR_IDX][IDX_DDTHETA]	= 0.0;
	m_grad_solver[VCONSTR_IDX][IDX_V]				= -SCALEFACTOR_SPEED;
	m_grad_solver[VCONSTR_IDX][IDX_DV]			= 0.0;
	m_grad_solver[VCONSTR_IDX][IDX_SF]			= SCALEFACTOR_SPEED * (dvldN*n + dvldE*e);
	m_grad_solver[VCONSTR_IDX][IDX_N]				= SCALEFACTOR_SPEED * dvldN*sf;
	m_grad_solver[VCONSTR_IDX][IDX_E]				= SCALEFACTOR_SPEED * dvldE*sf;
#endif
#if ACONSTR
	m_grad_solver[ACONSTR_IDX][IDX_THETA]		= 0.0;
	m_grad_solver[ACONSTR_IDX][IDX_DTHETA]	= 0.0;
	m_grad_solver[ACONSTR_IDX][IDX_DDTHETA]	= 0.0;
	m_grad_solver[ACONSTR_IDX][IDX_V]				= SCALEFACTOR_ACCEL * dv/sf;
	m_grad_solver[ACONSTR_IDX][IDX_DV]			= SCALEFACTOR_ACCEL * v/sf;
	m_grad_solver[ACONSTR_IDX][IDX_SF]			= SCALEFACTOR_ACCEL * (-v*dv/sf/sf);
	m_grad_solver[ACONSTR_IDX][IDX_N]				= 0.0;
	m_grad_solver[ACONSTR_IDX][IDX_E]				= 0.0;
#endif
#if PHICONSTR
	m_grad_solver[PHICONSTR_IDX][IDX_THETA]		= 0.0;
	m_grad_solver[PHICONSTR_IDX][IDX_DTHETA]	= SCALEFACTOR_TANPHI * (VEHICLE_WHEELBASE / sf);
	m_grad_solver[PHICONSTR_IDX][IDX_DDTHETA]	= 0.0;
	m_grad_solver[PHICONSTR_IDX][IDX_V]				= 0.0;
	m_grad_solver[PHICONSTR_IDX][IDX_DV]			= 0.0;
	m_grad_solver[PHICONSTR_IDX][IDX_SF]			= SCALEFACTOR_TANPHI * (-VEHICLE_WHEELBASE * dtheta /sf/sf);
	m_grad_solver[PHICONSTR_IDX][IDX_N]				= 0.0;
	m_grad_solver[PHICONSTR_IDX][IDX_E]				= 0.0;
#endif
#if PHIDCONSTR
	m_grad_solver[PHIDCONSTR_IDX][IDX_THETA]	= 0.0;
	m_grad_solver[PHIDCONSTR_IDX][IDX_DTHETA]	= m_grad_solver[PHICONSTR_IDX][IDX_DTHETA] * m_pConstrData[constr_index_snopt + PHIDCONSTR_IDX] *
		(-2.0 * m_pConstrData[constr_index_snopt + PHICONSTR_IDX] / ( 1.0 + m_pConstrData[constr_index_snopt + PHICONSTR_IDX]*m_pConstrData[constr_index_snopt + PHICONSTR_IDX] ));
	m_grad_solver[PHIDCONSTR_IDX][IDX_DDTHETA]= m_pConstrData[constr_index_snopt + PHIDCONSTR_IDX] / ddtheta;
	m_grad_solver[PHIDCONSTR_IDX][IDX_V]			= m_pConstrData[constr_index_snopt + PHIDCONSTR_IDX] / v;
	m_grad_solver[PHIDCONSTR_IDX][IDX_DV]			= 0.0;
	m_grad_solver[PHIDCONSTR_IDX][IDX_SF]			= (-2.0*m_pConstrData[constr_index_snopt + PHIDCONSTR_IDX]) *
		(1.0 / sf + m_pConstrData[constr_index_snopt + PHICONSTR_IDX] / ( 1.0 + m_pConstrData[constr_index_snopt + PHICONSTR_IDX]*m_pConstrData[constr_index_snopt + PHICONSTR_IDX] ) * m_grad_solver[PHICONSTR_IDX][IDX_SF]);
	m_grad_solver[PHIDCONSTR_IDX][IDX_N]			= 0.0;
	m_grad_solver[PHIDCONSTR_IDX][IDX_E]			= 0.0;
#endif
#if ROLLOVERCONSTR
	m_grad_solver[ROLLOVERCONSTR_IDX][IDX_THETA]	= 0.0;
	m_grad_solver[ROLLOVERCONSTR_IDX][IDX_DTHETA]	= SCALEFACTOR_ROLLOVER * v*v/sf;
	m_grad_solver[ROLLOVERCONSTR_IDX][IDX_DDTHETA]= 0.0;
	m_grad_solver[ROLLOVERCONSTR_IDX][IDX_V]			= SCALEFACTOR_ROLLOVER * 2.0*v*dtheta/sf;
	m_grad_solver[ROLLOVERCONSTR_IDX][IDX_DV]			= 0.0;
	m_grad_solver[ROLLOVERCONSTR_IDX][IDX_SF]			= SCALEFACTOR_ROLLOVER * (-v*v*dtheta/sf/sf);
	m_grad_solver[ROLLOVERCONSTR_IDX][IDX_N]			= 0.0;
	m_grad_solver[ROLLOVERCONSTR_IDX][IDX_E]			= 0.0;
#endif
}

void CRefinementStage::NonLinearFinlConstrFunc(void)
{
	double sin_m_targetTheta = sin(m_targetTheta);
	double cos_m_targetTheta = cos(m_targetTheta);

	const int constr_index_snopt = NUM_NLIN_INIT_CONSTR + NUM_COLLOCATION_POINTS*NUM_NLIN_TRAJ_CONSTR;
	const int collIndex = NUM_COLLOCATION_POINTS-1;

	// This constraint reorients the final point to the trackline at the target,
	// and constrains the longitudinal and lateral distances
	m_pConstrData[constr_index_snopt + 0] = SCALEFACTOR_DIST * ((n*sf - m_targetN)*cos_m_targetTheta + (e*sf - m_targetE)*sin_m_targetTheta);
	m_pConstrData[constr_index_snopt + 1] = SCALEFACTOR_DIST * ((n*sf - m_targetN)*sin_m_targetTheta - (e*sf - m_targetE)*cos_m_targetTheta);


	m_grad_solver[0][IDX_THETA]		= 0.0;
	m_grad_solver[0][IDX_DTHETA]	= 0.0;
	m_grad_solver[0][IDX_DDTHETA]	= 0.0;
	m_grad_solver[0][IDX_SF]			= SCALEFACTOR_DIST * (n *cos_m_targetTheta + e*sin_m_targetTheta);
	m_grad_solver[0][IDX_N]				= SCALEFACTOR_DIST * ( sf*cos_m_targetTheta );
	m_grad_solver[0][IDX_E]				= SCALEFACTOR_DIST * ( sf*sin_m_targetTheta );

	m_grad_solver[1][IDX_THETA]		= 0.0;
	m_grad_solver[1][IDX_DTHETA]	= 0.0;
	m_grad_solver[1][IDX_DDTHETA]	= 0.0;
	m_grad_solver[1][IDX_SF]			= SCALEFACTOR_DIST * (n *sin_m_targetTheta - e*cos_m_targetTheta);
	m_grad_solver[1][IDX_N]				= SCALEFACTOR_DIST * ( sf*sin_m_targetTheta );
	m_grad_solver[1][IDX_E]				= SCALEFACTOR_DIST * (-sf*cos_m_targetTheta );

	m_grad_solver[0][IDX_V]				= 0.0;
	m_grad_solver[0][IDX_DV]			= 0.0;
	m_grad_solver[1][IDX_V]				= 0.0;
	m_grad_solver[1][IDX_DV]			= 0.0;
}

void CRefinementStage::getSplineCoeffsTheta(double* pSolverState)
{
	char notrans = 'N';
	int rows = (NUM_POINTS-1) * 3; // number of polynomial coefficients
	int cols = NUM_POINTS;         // number of spline-defining variables
	double alpha = 1.0;
	double beta = 0.0;
	int incr1 = 1;
// 	int incr0 = 0;
// 	int incr3 = 3;
// 	int numSegments = NUM_POINTS-1;

	dgemv_(&notrans, &rows, &cols, &alpha, m_dercoeffsmatrix, &rows,
				 pSolverState, &incr1, &beta, m_splinecoeffsTheta, &incr1);
// 	daxpy_(&numSegments, &alpha, &m_initTheta, &incr0,
// 				 m_splinecoeffsTheta, &incr3);
	for(int i=0; i<(NUM_POINTS-1); i++)
		m_splinecoeffsTheta[3*i] += m_initTheta;
}

void CRefinementStage::funcConstr(double* pSolverState)
{
	int collIndex, constr_index_snopt, constr_index_planner;

	int varNUM_COLLOCATION_POINTS = NUM_COLLOCATION_POINTS;
	int varNPnumNonLinearConstr = NPnumNonLinearConstr;
	int varNUM_POINTS = NUM_POINTS;
	int varNUM_SPEED_SEGMENTS = NUM_SPEED_SEGMENTS;

	int one = 1;

	// zero out the gradient matrix
	memset(m_pConstrGradData, 0, NPnumNonLinearConstr*NPnumVariables*sizeof(m_pConstrGradData[0]));

	constr_index_snopt = 0;

	NonLinearInitConstrFunc();
	for(constr_index_planner=0;
			constr_index_planner < NUM_NLIN_INIT_CONSTR;
			constr_index_planner++)
	{
		// compute
		// 		m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, ALL)]  +=
		// 			m_grad_solver[constr_index_planner][IDX_THETA] * m_valcoeffs[0 + ALL*NUM_COLLOCATION_POINTS];
		// etc, etc
		// prev code was
// 		for(i=0; i<NUM_POINTS; i++)
// 		{
// 			// This is the initial constraint, so there's no dependence on (N,E), since it's (0,0) here
// 			gCon[MATRIX_INDEX(constr_index_snopt, i)]  = m_valcoeffs[0*(NUM_POINTS+1) + i]*m_grad_solver[constr_index_planner][IDX_THETA];
// 			gCon[MATRIX_INDEX(constr_index_snopt, i)] += m_d1coeffs [0*(NUM_POINTS+1) + i]*m_grad_solver[constr_index_planner][IDX_DTHETA];
// 			gCon[MATRIX_INDEX(constr_index_snopt, i)] += m_d2coeffs [0*(NUM_POINTS+1) + i]*m_grad_solver[constr_index_planner][IDX_DDTHETA];
// 			gCon[MATRIX_INDEX(constr_index_snopt, NUM_POINTS+i)]  = m_valcoeffs[0*(NUM_POINTS+1) + i]*m_grad_solver[constr_index_planner][IDX_V];
// 			gCon[MATRIX_INDEX(constr_index_snopt, NUM_POINTS+i)] += m_d1coeffs [0*(NUM_POINTS+1) + i]*m_grad_solver[constr_index_planner][IDX_DV];
// 		}
		daxpy_(&varNUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_THETA],
					 m_valcoeffs, &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&varNUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_DTHETA],
					 m_d1coeffs, &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&varNUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_DDTHETA],
					 m_d2coeffs, &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&varNUM_SPEED_SEGMENTS,
					 &m_grad_solver[constr_index_planner][IDX_V],
					 m_valcoeffs_speed, &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NUM_POINTS)], &varNPnumNonLinearConstr);
		daxpy_(&varNUM_SPEED_SEGMENTS,
					 &m_grad_solver[constr_index_planner][IDX_DV],
					 m_d1coeffs_speed, &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NUM_POINTS)], &varNPnumNonLinearConstr);
		// Sf derivatives
		m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NPnumVariables-1)] = m_grad_solver[constr_index_planner][IDX_SF];

		constr_index_snopt++;
	}

	for(collIndex=0; collIndex<NUM_COLLOCATION_POINTS; collIndex++)
	{
		NonLinearTrajConstrFunc(constr_index_snopt, collIndex);

		for(constr_index_planner=0;
				constr_index_planner < NUM_NLIN_TRAJ_CONSTR;
				constr_index_planner++)
		{
			daxpy_(&varNUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_N],
						 &m_pCollGradN[NUM_POINTS*collIndex], &one,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
			daxpy_(&varNUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_E],
						 &m_pCollGradE[NUM_POINTS*collIndex], &one,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
			daxpy_(&varNUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_THETA],
						 &m_valcoeffs[collIndex], &varNUM_COLLOCATION_POINTS,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
			daxpy_(&varNUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_DTHETA],
						 &m_d1coeffs[collIndex], &varNUM_COLLOCATION_POINTS,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
			daxpy_(&varNUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_DDTHETA],
						 &m_d2coeffs[collIndex], &varNUM_COLLOCATION_POINTS,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
			daxpy_(&varNUM_SPEED_SEGMENTS,
						 &m_grad_solver[constr_index_planner][IDX_V],
						 &m_valcoeffs_speed[collIndex], &varNUM_COLLOCATION_POINTS,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NUM_POINTS)], &varNPnumNonLinearConstr);
			daxpy_(&varNUM_SPEED_SEGMENTS,
						 &m_grad_solver[constr_index_planner][IDX_DV],
						 &m_d1coeffs_speed[collIndex], &varNUM_COLLOCATION_POINTS,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NUM_POINTS)], &varNPnumNonLinearConstr);

			// Sf derivatives
			m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NPnumVariables-1)] = m_grad_solver[constr_index_planner][IDX_SF];
			constr_index_snopt++;
		}
	}

	// compute the constr, gradients from the user
	NonLinearFinlConstrFunc();
	for(constr_index_planner=0;
			constr_index_planner < NUM_NLIN_FINL_CONSTR;
			constr_index_planner++)
	{
		daxpy_(&varNUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_N],
					 &m_pCollGradN[NUM_POINTS*(NUM_COLLOCATION_POINTS-1)], &one,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&varNUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_E],
					 &m_pCollGradE[NUM_POINTS*(NUM_COLLOCATION_POINTS-1)], &one,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&varNUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_THETA],
					 &m_valcoeffs[(NUM_COLLOCATION_POINTS-1)], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&varNUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_DTHETA],
					 &m_d1coeffs[(NUM_COLLOCATION_POINTS-1)], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&varNUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_DDTHETA],
					 &m_d2coeffs[(NUM_COLLOCATION_POINTS-1)], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&varNUM_SPEED_SEGMENTS,
					 &m_grad_solver[constr_index_planner][IDX_V],
					 &m_valcoeffs_speed[(NUM_COLLOCATION_POINTS-1)], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NUM_POINTS)], &varNPnumNonLinearConstr);
		daxpy_(&varNUM_SPEED_SEGMENTS,
					 &m_grad_solver[constr_index_planner][IDX_DV],
					 &m_d1coeffs_speed[(NUM_COLLOCATION_POINTS-1)], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NUM_POINTS)], &varNPnumNonLinearConstr);

		// Sf derivatives
		m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NPnumVariables-1)] = m_grad_solver[constr_index_planner][IDX_SF];
		constr_index_snopt++;
	}
}


void CRefinementStage::funcCost(double* pSolverState)
{
	m_obj = 0.0;
	memset(m_pCostGradData, 0, NPnumVariables*sizeof(m_pCostGradData[0]));

	double v0, v1;
	double dv0, dv1;
	double dobj;
	double sum;

	double v0_2, v0_3, v0_4, v1_2;
	double sum_5;
	v0  = m_initSpeed;
	for(int i=0; i<NUM_SPEED_SEGMENTS; i++)
	{
		v1 = pSolverState[NUM_POINTS + i] + m_initSpeed;

		sum = v0 + v1;
		sum_5 = sum*sum*sum*sum*sum;
		v0_2 = v0  * v0;
		v0_3 = v0_2* v0;
		v0_4 = v0_3* v0;
		v1_2 = v1*v1;

		// dobj = integral from 0 to 1 of the 5th order taylor series expansion of 1/(v0 + (v1-v0)*s) around s=0.5
		dobj = ( v1*( v1 *(v1 *(v1 + 2.0*v0) + 4.0*v0_2) + 2.0*v0_3) + v0_4 )  / sum_5;
		m_obj += dobj;

		dv0 = -(v1_2 * 3.0 * (v1_2 + 2.0*v0_2) +     v0_4) /sum_5/sum;
		dv1 = -(v1_2 *       (v1_2 + 6.0*v0_2) + 3.0*v0_4) /sum_5/sum;

		m_pCostGradData[NUM_POINTS + i  ] += dv1 * m_collSF * SCALEFACTOR_TIME;

		if(i != 0)
			m_pCostGradData[NUM_POINTS + i-1] += dv0 * m_collSF * SCALEFACTOR_TIME;

		v0 = v1;
	}
	m_obj *= m_collSF * SCALEFACTOR_TIME;
	m_pCostGradData[NPnumVariables-1] = m_obj/m_collSF;



	int constr_index_snopt;
	int varNUM_COLLOCATION_POINTS = NUM_COLLOCATION_POINTS;
	int varNUM_NLIN_TRAJ_CONSTR   = NUM_NLIN_TRAJ_CONSTR;
	double alpha;
	int j;
	// steering control effort
#if PHIDCONSTR
	constr_index_snopt = NUM_NLIN_INIT_CONSTR + PHIDCONSTR_IDX;
	alpha = EXTRAFACTOR_STEER_EFFORT/SCALEFACTOR_PHIDOT/SCALEFACTOR_PHIDOT;

	m_obj += alpha *
		ddot_(&varNUM_COLLOCATION_POINTS,	
					&m_pConstrData[constr_index_snopt], &varNUM_NLIN_TRAJ_CONSTR,
					&m_pConstrData[constr_index_snopt], &varNUM_NLIN_TRAJ_CONSTR);

	for(j=0; j<NPnumVariables; j++)
	{
		m_pCostGradData[j] += alpha * 2.0 *
			ddot_(&varNUM_COLLOCATION_POINTS,
						&m_pConstrData[constr_index_snopt], &varNUM_NLIN_TRAJ_CONSTR,
						&m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, j)], &varNUM_NLIN_TRAJ_CONSTR);
	}

	// Above is the BLAS version of this:
// 	for(int i=0; i<NUM_COLLOCATION_POINTS; i++)
// 	{
// 		m_obj += EXTRAFACTOR_STEER_EFFORT/SCALEFACTOR_PHIDOT/SCALEFACTOR_PHIDOT *
// 			m_pConstrData[constr_index_snopt] *
// 			m_pConstrData[constr_index_snopt];

// 		for(int j=0; j<NPnumVariables; j++)
// 			m_pCostGradData[j] += EXTRAFACTOR_STEER_EFFORT/SCALEFACTOR_PHIDOT/SCALEFACTOR_PHIDOT *
// 				2.0 * m_pConstrData[constr_index_snopt] *
// 				m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, j)];

// 		constr_index_snopt += NUM_NLIN_TRAJ_CONSTR;
// 	}
#endif

	// accel control effort
	// the accel effort code is very similar to the steering effort code
#if ACONSTR
	if(USE_ACCEL_EFFORT)
	{
		constr_index_snopt = NUM_NLIN_INIT_CONSTR + ACONSTR_IDX;
		alpha = EXTRAFACTOR_ACCEL_EFFORT/SCALEFACTOR_ACCEL/SCALEFACTOR_ACCEL;

		m_obj += alpha *
			ddot_(&varNUM_COLLOCATION_POINTS,	
						&m_pConstrData[constr_index_snopt], &varNUM_NLIN_TRAJ_CONSTR,
						&m_pConstrData[constr_index_snopt], &varNUM_NLIN_TRAJ_CONSTR);

		for(int j=0; j<NPnumVariables; j++)
		{
			m_pCostGradData[j] += alpha * 2.0 *
				ddot_(&varNUM_COLLOCATION_POINTS,
							&m_pConstrData[constr_index_snopt], &varNUM_NLIN_TRAJ_CONSTR,
							&m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, j)], &varNUM_NLIN_TRAJ_CONSTR);
		}
	}
#endif

	// longitudinal control effort
	// this code is very similar to the steering effort code
#if ROLLOVERCONSTR
	if(USE_ROLLOVER_EFFORT)
	{
		constr_index_snopt = NUM_NLIN_INIT_CONSTR + ROLLOVERCONSTR_IDX;
		alpha = EXTRAFACTOR_ROLLOVER_EFFORT/SCALEFACTOR_ROLLOVER/SCALEFACTOR_ROLLOVER;

		m_obj += alpha *
			ddot_(&varNUM_COLLOCATION_POINTS,	
						&m_pConstrData[constr_index_snopt], &varNUM_NLIN_TRAJ_CONSTR,
						&m_pConstrData[constr_index_snopt], &varNUM_NLIN_TRAJ_CONSTR);

		for(int j=0; j<NPnumVariables; j++)
		{
			m_pCostGradData[j] += alpha * 2.0 *
				ddot_(&varNUM_COLLOCATION_POINTS,
							&m_pConstrData[constr_index_snopt], &varNUM_NLIN_TRAJ_CONSTR,
							&m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, j)], &varNUM_NLIN_TRAJ_CONSTR);
		}
	}
#endif
}

void CRefinementStage::integrateNE(int collIndex)
{
#ifdef DO_BENCHMARK
	unsigned long long t1, t2;
	DGCgettime(t1);
#endif

#ifdef NEW_INTEGRATE_NE
	int i;
	int splinecoeffsstart = 3 * (collIndex / COLLOCATION_FACTOR);

	double a  = m_splinecoeffsTheta[splinecoeffsstart];
	double b  = m_splinecoeffsTheta[splinecoeffsstart + 1];
	double c  = m_splinecoeffsTheta[splinecoeffsstart + 2];

	double s0 = (double)(collIndex % COLLOCATION_FACTOR)/(double)(COLLOCATION_FACTOR);
	double di = 1.0 / (double)(COLLOCATION_FACTOR);
	double dicubed = di*di*di;
	double s1 = s0 + di;

	double x0_plus_a = 0.5 * (s0*(b+c*s0) + s1*(b+c*s1)) + a;
	double cos_x0_plus_a = cos(x0_plus_a);
	double sin_x0_plus_a = sin(x0_plus_a);

	double val = 7.0 * (s0*s0 + s1*s1) + 6.0*s0*s1;
	double small = c * dicubed / 6.0;
	double big   = 1.0 - dicubed/120.0 * ( c*c* val + 5.0*b* (2.0*c*(s1 + s0) + b) );

	double dsmall_dc =   dicubed/6.0;
	double dbig_db   = - dicubed/12.0 * (c*(s1 + s0) + b );
	double dbig_dc   = - dicubed/60.0 * (c*val + 5.0*b*(s1 + s0) );
	double dx0_plus_a_da = 1.0;
	double dx0_plus_a_db = 0.5*(s0 + s1);
	double dx0_plus_a_dc = 0.5*(s0*s0 + s1*s1);



	double delN = (cos_x0_plus_a*big + sin_x0_plus_a*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));
	double delE = (sin_x0_plus_a*big - cos_x0_plus_a*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));

	double ddelN_da = (                        - sin_x0_plus_a*big +
										                           cos_x0_plus_a*small              ) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));
	double ddelN_db = (cos_x0_plus_a*dbig_db   - sin_x0_plus_a*dx0_plus_a_db*big +
										                           cos_x0_plus_a*dx0_plus_a_db*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));
	double ddelN_dc = (cos_x0_plus_a*dbig_dc   - sin_x0_plus_a*dx0_plus_a_dc*big +
										 sin_x0_plus_a*dsmall_dc + cos_x0_plus_a*dx0_plus_a_dc*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));

	double ddelE_da = (                        + cos_x0_plus_a*big +
										                           sin_x0_plus_a*small              ) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));
	double ddelE_db = (sin_x0_plus_a*dbig_db   + cos_x0_plus_a*dx0_plus_a_db*big +
										                           sin_x0_plus_a*dx0_plus_a_db*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));
	double ddelE_dc = (sin_x0_plus_a*dbig_dc   + cos_x0_plus_a*dx0_plus_a_dc*big -
										 cos_x0_plus_a*dsmall_dc + sin_x0_plus_a*dx0_plus_a_dc*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));

	m_pCollN[collIndex+1] = n + delN;
	m_pCollE[collIndex+1] = e + delE;

	double ddelN_dcoeff, ddelE_dcoeff;
	int collGradNEindex = NUM_POINTS*collIndex;
	for(i=0; i<NUM_POINTS; i++)
	{
		ddelN_dcoeff = 
			ddelN_da * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3    ] +
			ddelN_db * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 1] +
			ddelN_dc * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 2];
		ddelE_dcoeff = 
			ddelE_da * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3    ] +
			ddelE_db * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 1] +
			ddelE_dc * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 2];

		m_pCollGradN[collGradNEindex+NUM_POINTS] = m_pCollGradN[collGradNEindex] + ddelN_dcoeff;
		m_pCollGradE[collGradNEindex+NUM_POINTS] = m_pCollGradE[collGradNEindex] + ddelE_dcoeff;
		collGradNEindex++;
	}

#else

	int i;
	int splinecoeffsstart = 3 * (collIndex / COLLOCATION_FACTOR);

	double a = m_splinecoeffsTheta[splinecoeffsstart + 0];
	double b = m_splinecoeffsTheta[splinecoeffsstart + 1];
	double c = m_splinecoeffsTheta[splinecoeffsstart + 2];
	double s1 = (double)(collIndex % COLLOCATION_FACTOR)/(double)(COLLOCATION_FACTOR);
	double diff = 1.0 / (double)(COLLOCATION_FACTOR);
	double sum = 2.0*s1 + diff;
	double smid = s1 + diff/2.0;

	double ce = smid*(smid*c + b) + a;

	double cos_ce = cos(ce);
	double sin_ce = sin(ce);

	double ce_minus_a = ce - a;

	double sumsq = sum*sum;
	double diffsq = diff*diff;
	double sumsqplusdiffsq = sumsq + diffsq;
	double binom = (sqrt(5.0)*sumsq + (sqrt(5.0) - 2.0) * diffsq) * (sqrt(5.0)*sumsq + (sqrt(5.0) + 2.0)*diffsq);

	double small_ce_minus_a_0 = -diff*(3.0*c* (c*binom + 20.0*b*sum*sumsqplusdiffsq) +
																		20.0*b*b*(3.0*sumsq+diffsq)-480.0) / 480.0;
	double small_ce_minus_a_1 = diff * (3.0*sum * (c*sum + 2.0*b) + c*diffsq) / 12.0;
	double small_ce_minus_a_2 = -diff / 2.0;

	double big_ce_minus_a_0 =
		(
		 sum * 35.0 *( sum *( sum *( sum *( sum * c*c * (c*sum +
																										 6.0*b) +
																				c * (5.0*c*c*diffsq + 12.0*b*b)) +
																 4.0*b*(5.0*c*c*diffsq + 2.0*b*b)) +
													3.0 * c*(diffsq*(c*c*diffsq+8.0*b*b)-32.0)) +
									 2.0 * b*(diffsq*(3.0*c*c*diffsq+4.0*b*b)-96.0)) +
		 c*diffsq*(diffsq*(5.0*c*c*diffsq+84.0*b*b)-1120.0)
		) * diff / 13440.0;

	// these are hardcoded
// 	double big_ce_minus_a_1 = small_ce_minus_a_0;
// 	double big_ce_minus_a_2 = small_ce_minus_a_1 / 2.0;
// 	double big_ce_minus_a_3 = small_ce_minus_a_2 / 3.0;

	double big =
		ce_minus_a *( ce_minus_a *( ce_minus_a*small_ce_minus_a_2 / 3.0 +
																small_ce_minus_a_1 / 2.0) +
									small_ce_minus_a_0) +
		big_ce_minus_a_0;

	double small =
		ce_minus_a *( ce_minus_a * small_ce_minus_a_2 +
									small_ce_minus_a_1 ) +
		small_ce_minus_a_0;

	m_pCollN[collIndex+1] = n + (cos_ce*small + sin_ce*big) / (double)(NUM_POINTS-1);
	m_pCollE[collIndex+1] = e + (sin_ce*small - cos_ce*big) / (double)(NUM_POINTS-1);


	double dsmall_ce_minus_a_0_db = -diff * (1.5*c*sum*sumsqplusdiffsq + b*(3.0*sumsq+diffsq)) / 12.0;
	double dsmall_ce_minus_a_0_dc = -diff / 80.0 * (10.0*b*sum*sumsqplusdiffsq + binom*c);
	double dsmall_ce_minus_a_1_db = 0.5*diff*sum;
	double dsmall_ce_minus_a_1_dc = diff*(3.0*sumsq + diffsq) / 12.0;
	double dbig_ce_minus_a_0_db = diff / 13440.0 * 
		(35.0*sum*(sum*(sum*(6.0*sum*c*(c*sum + 4.0*b) +
												 20.0*(c*c*diffsq + 1.2*b*b)) +
										48.0*b*c*diffsq) +
							 2.0*(diffsq*12.0*(0.25*c*c*diffsq + b*b) - 96.0)) +
		 168.0*b*c*diffsq*diffsq);
	double dbig_ce_minus_a_0_dc = diff / 13440.0 *
			  (35.0*sum*(sum*(sum*(sum*3.0*(c*sum*(c*sum + 4.0*b) +
																	5.0*c*c*diffsq + 4.0*b*b) +
														 40.0*b*c*diffsq) +
												3.0 * diffsq * (3.0*c*c*diffsq + 8.0*b*b) - 96.0) +
									 12.0*b*c*diffsq*diffsq) +
				 diffsq * (diffsq * (diffsq*15.0*c*c + 84.0*b*b) - 1120.0));


	double dsmall_da = -small_ce_minus_a_1 - 2.0*ce_minus_a*small_ce_minus_a_2;
	double dsmall_db = ce_minus_a*dsmall_ce_minus_a_1_db + dsmall_ce_minus_a_0_db;
	double dsmall_dc = ce_minus_a*dsmall_ce_minus_a_1_dc + dsmall_ce_minus_a_0_dc;
	double dbig_da   = -small_ce_minus_a_0 - ce_minus_a*small_ce_minus_a_1 - small_ce_minus_a_2*ce_minus_a*ce_minus_a;
	double dbig_db   =
		ce_minus_a *( ce_minus_a * dsmall_ce_minus_a_1_db / 2.0 +
									dsmall_ce_minus_a_0_db) +
		dbig_ce_minus_a_0_db;
	double dbig_dc   =
		ce_minus_a *( ce_minus_a * dsmall_ce_minus_a_1_dc / 2.0 +
									dsmall_ce_minus_a_0_dc) +
		dbig_ce_minus_a_0_dc;

	double dsmall_dcoeff;
	double dbig_dcoeff;


	int collGradNEindex = NUM_POINTS*collIndex;
	for(i=0; i<NUM_POINTS; i++)
	{
		dsmall_dcoeff =
			dsmall_da * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3] +
			dsmall_db * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 1] +
			dsmall_dc * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 2];

		dbig_dcoeff =
			dbig_da * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3] +
			dbig_db * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 1] +
			dbig_dc * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 2];

		m_pCollGradN[collGradNEindex+NUM_POINTS] = m_pCollGradN[collGradNEindex] +
			(cos_ce * dsmall_dcoeff + sin_ce*dbig_dcoeff) /
			(double)(NUM_POINTS-1);
		m_pCollGradE[collGradNEindex+NUM_POINTS] = m_pCollGradE[collGradNEindex] +
			(sin_ce * dsmall_dcoeff - cos_ce*dbig_dcoeff) /
			(double)(NUM_POINTS-1);
		collGradNEindex++;
	}
#endif


#ifdef DO_BENCHMARK
	DGCgettime(t2);
	integrateNEtime += t2-t1;
#endif
}

#warning "This function really shouldn't be mostly a copy of integrateNE"
void CRefinementStage::integrateNEfine(int collIndex, double& outN, double& outE)
{
#ifdef NEW_INTEGRATE_NE

	int i;
	int splinecoeffsstart = 3 * (collIndex / COLLOCATION_FACTOR_FINE);

	double a  = m_splinecoeffsTheta[splinecoeffsstart];
	double b  = m_splinecoeffsTheta[splinecoeffsstart + 1];
	double c  = m_splinecoeffsTheta[splinecoeffsstart + 2];

	double s0 = (double)(collIndex % COLLOCATION_FACTOR_FINE)/(double)(COLLOCATION_FACTOR_FINE);
	double di = 1.0 / (double)(COLLOCATION_FACTOR_FINE);
	double dicubed = di*di*di;
	double s1 = s0 + di;

	double x0_plus_a = 0.5 * (s0*(b+c*s0) + s1*(b+c*s1)) + a;
	double cos_x0_plus_a = cos(x0_plus_a);
	double sin_x0_plus_a = sin(x0_plus_a);

	double val = 7.0 * (s0*s0 + s1*s1) + 6.0*s0*s1;
	double small = c * dicubed / 6.0;
	double big   = 1.0 - dicubed/120.0 * ( c*c* val + 5.0*b* (2.0*c*(s1 + s0) + b) );

	double delN = (cos_x0_plus_a*big + sin_x0_plus_a*small) / (double)((NUM_POINTS-1)*COLLOCATION_FACTOR_FINE);
	double delE = (sin_x0_plus_a*big - cos_x0_plus_a*small) / (double)((NUM_POINTS-1)*COLLOCATION_FACTOR_FINE);
	outN += delN;
	outE += delE;

#else

	int splinecoeffsstart = 3 * (collIndex / COLLOCATION_FACTOR_FINE);

	double a = m_splinecoeffsTheta[splinecoeffsstart];
	double b = m_splinecoeffsTheta[splinecoeffsstart + 1];
	double c = m_splinecoeffsTheta[splinecoeffsstart + 2];
	double s1 = (double)(collIndex % COLLOCATION_FACTOR_FINE)/(double)(COLLOCATION_FACTOR_FINE);
	double diff = 1.0 / (double)(COLLOCATION_FACTOR_FINE);
	double smid = s1 + diff/2.0;

	double ce = smid*(smid*c + b) + a;
	double ce_minus_a = ce - a;
	double sum = 2.0*s1 + diff;

	double sumsq = sum*sum;
	double diffsq = diff*diff;
	double sumsqplusdiffsq = sumsq + diffsq;
	double binom = (sqrt(5.0)*sumsq + (sqrt(5.0) - 2.0) * diffsq) * (sqrt(5.0)*sumsq + (sqrt(5.0) + 2.0)*diffsq);

	double small_ce_minus_a_0 = -diff*(3.0*c* (c*binom + 20.0*b*sum*sumsqplusdiffsq) +
																		 20.0*b*b*(3.0*sumsq+diffsq)-480.0) / 480.0;
	double small_ce_minus_a_1 = diff * (3.0*sum * (c*sum + 2.0*b) + c*diffsq) / 12.0;
	double small_ce_minus_a_2 = -diff / 2.0;

	double big_ce_minus_a_0 =
		(
		 sum * 35.0 *( sum *( sum *( sum *( sum * c*c * (c*sum +
																										 6.0*b) +
																				c * (5.0*c*c*diffsq + 12.0*b*b)) +
																 4.0*b*(5.0*c*c*diffsq + 2.0*b*b)) +
													3.0 * c*(diffsq*(c*c*diffsq+8.0*b*b)-32.0)) +
									 2.0 * b*(diffsq*(3.0*c*c*diffsq+4.0*b*b)-96.0)) +
		 c*diffsq*(diffsq*(5.0*c*c*diffsq+84.0*b*b)-1120.0)
		 ) * diff / 13440.0;

	// these are hardcoded
	// double big_ce_minus_a_1 = small_ce_minus_a_0;
	// double big_ce_minus_a_2 = small_ce_minus_a_1 / 2.0;
	// double big_ce_minus_a_3 = small_ce_minus_a_2 / 3.0;

	double big =
		ce_minus_a *( ce_minus_a *( ce_minus_a*small_ce_minus_a_2 / 3.0 +
																small_ce_minus_a_1 / 2.0) +
									small_ce_minus_a_0) +
		big_ce_minus_a_0;

	double small =
		ce_minus_a *( ce_minus_a * small_ce_minus_a_2 +
									small_ce_minus_a_1 ) +
		small_ce_minus_a_0;

	outN += (cos(ce)*small + sin(ce)*big) / (double)(NUM_POINTS-1);
	outE += (sin(ce)*small - cos(ce)*big) / (double)(NUM_POINTS-1);
#endif
}
