/* 
 * Simple gnuplot_i.c usage.
 */

#include <stdio.h>
//#include <stdlib.h>

#include "gnuplot_i.h"

int main(int argc, char *argv[])
{
  gnuplot_ctrl* h1;
  printf("*** simple example of opening a gnuplot handle from C ***\n");
  h1 = gnuplot_init();

  return 0;
}
