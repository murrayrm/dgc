#include <stdio.h>
#include <stdlib.h>

#include "stereovision/stereoSource.hh"
#include "stereovision/stereoProcess.hh"


int main(int argc, char *argv[]) {
  frames myFrame;
  XYZcoord offset(0, 0, -2);
  XYZcoord start_point(1.4142136, 0, 0);
  XYZcoord end_point(0, 0, 0);
  double pitch=-0.78539816, roll=0, yaw=0;
  VehicleState state;

  myFrame.initFrames(offset, pitch, roll, yaw);

  state.Easting = state.Northing = state.Altitude = state.Pitch = state.Roll = state.Yaw = 0;

  XYZcoord state_coord(state.Northing, state.Easting, state.Altitude);

  myFrame.updateState(state_coord, state.Roll, state.Pitch, state.Yaw);

  end_point = myFrame.transformS2B(start_point);

  printf("Offset: (%lf, %lf, %lf)\n", offset.X, offset.Y, offset.Z);
  printf("PRY: (%lf, %lf, %lf)\n", pitch, roll, yaw);
  printf("State: %lf, %lf, %lf, %lf %lf %lf\n", state.Easting, state.Northing, state.Altitude, state.Pitch, state.Roll, state.Yaw);
  printf("Start: (%lf, %lf, %lf)\n", start_point.X, start_point.Y, start_point.Z);
  printf("End: (%lf, %lf, %lf)\n", end_point.X, end_point.Y, end_point.Z);


  stereoSource mySource;
  stereoProcess myProcess;
  VehicleState currentState;

  mySource.init(0, "../../drivers/stereovision/config/stereovision/CamID.ini.short", "temp", "bmp");
  myProcess.init(0, "../../drivers/stereovision/config/stereovision/SVSCal.ini.short",
		 "../../drivers/stereovision/config/stereovision/SVSParams.ini.short",
		 "../../drivers/stereovision/config/stereovision/CamCal.ini.short",
		 "temp",
		 "bmp");

  for(int i=0; i<50; i++) {
    mySource.grab();
    mySource.save();
    myProcess.loadPair(mySource.pair(), currentState);
    myProcess.calcRect();
    //myProcess.saveRect();
    myProcess.calcDisp();
    myProcess.saveDisp();
    myProcess.calc3D();

    int numPoints = myProcess.numPoints();


    XYZcoord UTMPoint;
    cout << "scanning " << numPoints << " points" << endl;
    for(int j=0; j<numPoints; j++) {
      UTMPoint = myProcess.UTMPoint(j);
      if(UTMPoint!=XYZcoord(0,0,0)) {
	cout << "got it ";
      }
    }

    /*
    for(int y=myProcess.subWindow.y; y<myProcess.subWindow.y+myProcess.subWindow.height; y++) {
      for(int x=myProcess.subWindow.x; x<myProcess.subWindow.x+myProcess.subWindow.width; x++) {
	//DEBUG("inside the for loops, x is " << x << " and y is " << y)
	//DEBUG(myProcess.SinglePoint(x,y,&UTMPoint))
	if(myProcess.SinglePoint(x, y, &UTMPoint)) {
	  cout << "passed first if ";
	}
      }
    }
    */
    cout << endl;
  }

  mySource.stop();
  
  return 0;
}

