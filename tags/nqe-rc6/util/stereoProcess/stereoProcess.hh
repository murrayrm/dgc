#ifndef __STEREOPROCESS_H__
#define __STEREOPROCESS_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <set>
#include <deque>

#include <opencv/cv.h>
#include <opencv/cvaux.h>
#include <opencv/highgui.h>

#include <svs/src/svsclass.h>
#include <svs/samples/flwin.h>

#include "stereovision/stereoSource.hh"
//#include "frames/frames.hh"
#include "Matrix.hh"
#include "VehicleState.hh"

#include "DGCutils"

enum {
  stereoProcess_OK,
  stereoProcess_NO_FILE,
  stereoProcess_UNKNOWN_ERROR,

  stereoProcess_COUNT
};


/*-----------------------------------------------------------------------------*/
/**
  @brief A class to wrap around stereovision processing engines.

  stereoProcess was designed to wrap around any stereovision processing engine
  we create.  It has functionality to load a stereovision pair, rectify it,
  calculate the disparity image, and then transform the resulting points into
  UTM coordinates.  Currently it uses the SVS (Small Vision System) stereovision
  engine to perform stereo processing - it could be replaced if we wanted.

  Possible future improvements (please remove these as they are completed):
  - Enabling online calibration.
  - Cleanup of code to make sure there are no memory leaks, uninitialized
  variables, segfaults, etc.
  - Cleanup of code to unify error messages.
*/
/*-----------------------------------------------------------------------------*/
class stereoProcess {
public:
  //! A basic constructor, initializes various internal variables
  stereoProcess();

  //! A basic destructor - doesn't do any clean up yet!
  ~stereoProcess();

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Initializes a stereoProcess object
    @param  verboseLevel      Verbosity level (not yet implemented)
    @param  SVSCalFilename    Filename of the SVS calibration file
    @param  SVSParamsFilename Filename of the SVS parameter file
    @param  CamParamsFilename Filename of the camera parameter file
    @param  baseFilename      Base filename to log to
    @param  baseFileType      Base filetype to log to
    @param  num               Frame number to start out with
    @param  sunParamsFilename Filename of the sun detection parameter file

    This method initializes a stereoProcess object, and MUST be called before
    you do anything else with the stereoProcess object.  It takes a verbosity
    level, which in theory would tell the object what level of error or status
    messages to report.  (This isn't yet implemented, though.)  It also takes
    the following files:
    - SVSCalfilename - This is the calibration file SVS creates.
    - SVSParamsFilename - This is a file that contains parameters for the SVS
    software to use.  
    - CamParamsFilename - This is a file that contains the XYZ and RPY
    information of the camera pair with respect to the vehicle origin.
    Examples of these files can be found in
    trunk/drivers/stereovision/calibration.  There is also documentation about
    them on the wiki, linked to from the stereovision component page.
    
    The method also takes a few optional arguments: the baseFilename and
    baseFileType, which are used for automatic logging.  The baseFilename is a
    prefix the software uses when logging data (see the various save() methods
    for more information), and the baseFileType specifies what image formats
    images should be saved as (bmp, jpg, etc.).  num indicates what frame number
    to start logging from (the default is 0).

  */
  /*---------------------------------------------------------------------------*/
  int init(int verboseLevel, char* SVSCalFilename, char* SVSParamsFilename, 
	   char* CamParamsFilename, char *baseFilename = "", 
	   char *baseFileType = "", int num = 0, char* sunParamsFilename = "", char* cutOffsParamsFilename = "");

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Loads a stereo pair and vehicle state object for processing
    @param  stereoPair A stereoPair object to process
    @param  state      The state structure to use when transforming to UTM
    @return Standard stereoSource status code
    
    This will load an image pair into the memory of the stereoProcess object.
  */
  /*---------------------------------------------------------------------------*/
  int loadPair(stereoPair pair, VehicleState state);

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Loads two images and vehicle state for processing
    @param  left  Pointer to the left image in memory
    @param  right Pointer to the right image in memory
    @param  state The state structure to use when transforming to UTM
    @return  Standard stereoSource status code

    This will load to images from memory into the stereoProcess object.
  */
  /*---------------------------------------------------------------------------*/
  int loadPair(unsigned char *left, unsigned char *right, VehicleState state);

  /*---------------------------------------------------------------------------*/
  /**
    @brief Rectifies the image pair in the stereoProcess object
    @return Standard stereoSource status code

    This function performs the rectification step of stereovision processing on
    the image pair currently loaded into the object, making the scanlines
    parallel.  It does this using the various calibration parameters loaded in
    from file when the init function was called.
  */
  /*---------------------------------------------------------------------------*/
  int calcRect();

  /*---------------------------------------------------------------------------*/
  /**
    @brief Performs the disparity calculation step on the rectified images
    @return Standard stereoSource status code

    This function uses the stereovision engine embedded into the stereoProcess
    object to perform the disparity calculation step of the stereovision
    processing.
  */
  /*---------------------------------------------------------------------------*/
  int calcDisp();

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Calculates the 3D location of every pixel in the disparity image
    @return FIXME

    This method calculates the 3D location of every pixel in the disparity image
    relative to the left camera.  The resulting data can be accessed via the
    methods: numPoints(), validPoint(int), UTMPoint(int), Point(int).  You would
    access it as follows (for more detailed information, see the documentation
    for each method):

    @code
    myObject.calc3D();
    for(int i=0; i<myObject.numPoints(); i++) {
      if(myObject.validPoint(i)) {
        XYZcoord tempPoint = myObject.UTMPoint(i);
	... process the point here ...
      } else {
      //Ignore - bad point
      }
    }
    @endcode

  */
  /*---------------------------------------------------------------------------*/
  bool calc3D();
  

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Saves the rectified images and the disparity image to a file
    @return Standard stereoSource status code

    This method quick-saves the current disparity and rectified image to a file,
    using the filenames that were given as optional arguments to the init
    method.  If no optional arguments were given, then the behavior is
    undefined.  For more information on the filename format it saves to, just
    try using it.
  */
  /*---------------------------------------------------------------------------*/
  int save();

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Saves the rectified and disparity images to a file
    @param  baseFilename Base filename to use to save the images
    @param  baseFileType Filetype to save the images as (only BMP is supported)
    @param  num          Frame number to use while saving
    @return Standard stereoProcess status code
    
    Explicitly saves the current disparity and rectified images to files, using
    the given parameters.  For more explanation of the filename format when
    saving, see the documentation for the save() method.
  */
  /*---------------------------------------------------------------------------*/
  int save(char *baseFilename, char *baseFileType, int num);

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Saves the disparity image to a file
    @return Standard stereoProcess status code

    Saves the current disparity image to a file using the optional arguments
    given to the init method.  If no arguments were given, then the behavior is
    undefined.  For more information on the filename format, see the
    documentation for the save() method.
  */
  /*---------------------------------------------------------------------------*/
  int saveDisp();

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Saves the disparity image to an explicit file
    @param  baseFilename Base filename to use to save the images
    @param  baseFileType Filetype to save the images as (only BMP is supported)
    @param  num          Frame number to use while saving
    @return Standard stereoProcess status code

    Explicitly saves the current disparity image to a file, using the given
    parameters.  For more explanation of the filename format used when saving,
    see the documentation for the save() method.
  */
  /*---------------------------------------------------------------------------*/
  int saveDisp(char *baseFilename, char *baseFileType, int num);

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Saves the rectificied images to a file
    @return Standard stereoProcess status code

    Saves the current rectified images to files using the optional arguments
    given to the init method.  If no arguments were given, then the behavior is
    undefined.  For more information on the filename format, see the
    documentation for the save() method.
  */
  /*---------------------------------------------------------------------------*/
  int saveRect();

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Saves the rectified images to files
    @param  baseFilename Base filename to use to save the images
    @param  baseFileType Filetype to save the images as (only BMP is supported)
    @param  num          Frame number to use while saving
    @return Standard stereoProcess status code

    Saves the current rectified images to a file using the optional arguments
    given to the init method.  If no arguments were given, then the behavior is
    undefined.  For more information on the filename format, see the
    documentation for the save() method.
  */
  /*---------------------------------------------------------------------------*/
  int saveRect(char *baseFilename, char *baseFileType, int num);


  /*---------------------------------------------------------------------------*/
  /**
    @brief  Displays 3 X windows, 1for each rectified image and 1 for disparity
    @param  width  Width of the window
    @param  height Height of the window
    @return Standard stereoProcess status code

    If you're running X, then SVS has the ability to show in real time both the
    rectified images, as well as the disparity image.  If you give these methods
    their optional arguments, you can choose the size of the window to show -
    otherwise they will show the subwindow being used for processing by default.
  */
  /*---------------------------------------------------------------------------*/
  int show(int width=0, int height=0);

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Displays the disparity image in an X window
    @param  width  Width of the window
    @param  height Height of the window
    @return Standard stereoProcess status code

    Shows a green disparity image - useful for debugging!  For more information,
    see the show(int, int) method.
  */
  /*---------------------------------------------------------------------------*/
  int showDisp(int width=0, int height=0);

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Displays the rectified image in two X windows
    @param  width  Width of the window
    @param  height Height of the window
    @return Standard stereoProcess status code

    Shows the rectified images in two windows - useful for debugging!  For more
    information, see the show(int, int) method.
  */
  /*---------------------------------------------------------------------------*/
  int showRect(int width=0, int height=0);

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Displays the left rectified image in an X window
    @param  width  Width of the window
    @param  height Height of the window
    @return Standard stereoProcess status code

    Shows the rectified left image in an X window - useful for debugging!  For
    more information, see the show(int, int) method.
  */
  /*---------------------------------------------------------------------------*/
  int showRectLeft(int width=0, int height=0);

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Displays the right rectified image in an X window
    @param  width  Width of the window
    @param  height Height of the window
    @return Standard stereoProcess status code

    Shows the rectified right images in an X window - useful for debugging!  For
    more information, see the show(int, int) method.
  */
  /*---------------------------------------------------------------------------*/
  int showRectRight(int width=0, int height=0);

  
  //! Not yet implemented!
   unsigned char* disp();

  //! Not yet implemented!
  unsigned char* rectLeft();

  //! Not yet implemented!
  unsigned char* rectRight();

  //! Not yet implemented!
  stereoPair rectPair();

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Accessor for the number of points for which 3D coords were found
    @return The number of points for which 3D coordinates were calculated

    This method returns the number of 3D points found after calc3D() was
    called.  Note that not all the points are valid.  For an example of how to
    use this method, please see the documentation for the calc3D() method.
  */
  /*---------------------------------------------------------------------------*/
  int numPoints();

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Accessor for whether or not a point is valid
    @param  i The index of the point to check for validity
    @return Whether or not the point is valid
    
    This method returns whether or not the ith point is valid.  A point might be
    invalid if no disparity could be calculated for it (due to lack of texture
    in the image) or if the calculated disparity fell below a confidence
    threshold.  For an example of how to use this method, please see the
    documentation for the calc3D() method.

  */
  /*---------------------------------------------------------------------------*/
  bool validPoint(int i);

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Accessor for the UTM coordinate of the ith point in the point cloud
    @param  i The index of the point to get the UTM coordinate for
    @return The UTM coordinate of the ith point in the point cloud

    This method returns the UTM coordinate of the ith point.  For an example of
    how to use this method, please see the documentation for the calc3D()
    method.  Note that if the point is not valid, it will return the coordinate
    (0, 0, 0).
  */
  /*---------------------------------------------------------------------------*/
  NEDcoord UTMPoint(int i);

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Accessor for the relative coordinate of the ith point in the cloud
    @param  i The index of the point to get the relative coordinates of
    @return The coordinate of the ith point relative to the left camera

    This method returns the relative coordinate of the ith point in the point
    cloud.  Per the SVS documentation, the relative coordinate system this
    method uses has its origin at the left camera, and the orientation +Z away
    from the camera, +X to the camera's right, and +Y down beneath the
    camera. Also note that the XYZ coordinates here are in millimeters.
    This method should be used in the same way as the UTMPoint(int) method.
  */
  /*---------------------------------------------------------------------------*/
  XYZcoord Point(int i);

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Method to individually calculate the UTM coordinate of a given pixel
    @param  x        The x image coordinate of the pixel
    @param  y        The y image coordinate of the pixel
    @param  XYZCoord A pointer to an XYZcoord in which to store the result
    @return Whether or not the pixel is valid

    This method is useful if you do not want to use calc3D() and calculate the
    3D coordinates of every pixel in the image - it allows you to pick and choose
    which pixels to calculate 3D coordinates for.  You give it the x and y
    coordinates (in the image) of the pixel, as well as a pointer to an XYZcoord
    in which to store the resulting 3D location (which is in UTM).  If the pixel
    is valid, the method will return true and the XYZcoord will be useful.  If
    the pixel turned out to be invalid (i.e. if no disparity could be
    calculated, or it fell below a threshold) then the method will return false
    and the XYZcoord will be (0, 0, 0).
  */
  /*---------------------------------------------------------------------------*/
  bool SinglePoint(int x, int y, NEDcoord* resultPoint);

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Method to individually calculate the relative coordinate of a pixel
    @param  x        The x image coordinate of the pixel
    @param  y        The y image coordinate of the pixel
    @param  XYZCoord A pointer to an XYZcoord in which to store the result
    @return Whether or not the pixel is valid
    
    This method is identical to SinglePoint(int, int, XYZcoord*), except that it
    returns its result in the relative coordinate system, where the vehicle
    origin is the origin, +X is forward in front of the vehicle, +Y is to the
    right of the vehicle, and +Z is down into the ground.  For more information
    on the arguments this method takes and how to use it, see the documentation
    for the SinglePoint(int, int, XYZcoord*) method, which is identical in terms
    of inputs and behavior.
  */
  /*---------------------------------------------------------------------------*/
  bool SingleRelativePoint(int x, int y, XYZcoord* resultPoint);

  //! Returns the current frame number
  int pairIndex();  

  //! Resets the current frame number to 0
  int resetPairIndex();

  //! Accessor to return the current state the object is using
  VehicleState currentState();

  int numMinPoints;

  CvSize imageSize;
  CvRect subWindow;
 
  /*---------------------------------------------------------------------------*/
  /**
    @brief Accessor for the size of the correlation window in pixels
    @return The width/height of the correlation window SVS uses in pixels

    When calculating disparity, some stereovision algorithms (such as the one
    SVS uses) compare windows of pixels for similarities.  This returns the size
    of the window (which is a square) in pixels.  Generally this ranges from 3
    pixels up to as many as 13 (or higher).

    For more information, see the SVS documentation.
  */
  /*---------------------------------------------------------------------------*/
  int corrsize();

  /*---------------------------------------------------------------------------*/
  /**
    @brief Accessor for the confidence threshold
    @return The confidence threshold SVS uses, in the range [0,40]

    When calculating disparity, some stereovision algorithms (such as the one
    SVS uses) generate a confidence measure for each pixel.  I have no idea how
    SVS calculates confidence, but it rejects pixels below this confidence
    threshold.  (0 is low confidence, 40 is the highest confidence.)

    For more information, see the SVS documentation.
  */
  /*---------------------------------------------------------------------------*/
  int thresh();

  /*---------------------------------------------------------------------------*/
  /**
    @brief Accessor for the number of disparities to search
    @return The number of disparities to search, in the range FIXME JHG!

    When calculating disparity, some stereovision algorithms (such as the one
    SVS uses) do a search over a fixed number of possible disparities to
    determine which one is the most likely.  This number indicates how many
    disparities SVS is searching.

    For more information, see the SVS documentation.
  */
  /*---------------------------------------------------------------------------*/
  int ndisp();

  /*---------------------------------------------------------------------------*/
  /**
    @brief Accessor for the horopter
    @return The horopter SVS uses (in pixels)

    When calculating disparity, some stereovision algorithms (such as the one
    SVS uses) do a search over a fixed number of possible disparities to
    determine which one is the most likely.  This number indicates the offset of
    how many pixels SVS skips before it starts searching - this is useful if you
    know no objects will be within a certain distance range.

    For more information, see the SVS documentation.
  */
  /*---------------------------------------------------------------------------*/
  int offx();

  /*---------------------------------------------------------------------------*/
  /**
    @brief Accessor for the unique parameter value.

    The unique parameter sets a threshold for the difference between the best and the second
    best correlation score. If the difference is below the threshold, the disparity is not included
    in the disparity image. See SVS documentation for details.

  */
  /*---------------------------------------------------------------------------*/
  int svsUnique();

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Accessor for whether or not multiscale disparity processing is enabled
    @return The status of the multiscale disparity flag (0 or 1)

    When calculating disparity, SVS provides a multiscale feature, which
    increases the reliability of the disparity information by calculating
    disparity based on images shrunk by 1/2 in each dimension.  If this feature
    is enabled, this method returns 1.  If it is not, this will return a 0.
  */
  /*---------------------------------------------------------------------------*/
  int multi();

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Mutator to set the various parameters SVS takes
    @param  corrsize         The correlation window size to use
    @param  thresh           The confidence threshold to use
    @param  ndisp            Number of disparities to search
    @param  offx             The horopter
    @param  svsUnique        The unique parameter value
    @param  multi            Whether or not to enable multiscale processing
    @param  subwindow_x_off  The x coordinate of the subwindow
    @param  subwindow_y_off  The y coordinate of the subwindow
    @param  subwindow_width  The width of the subwindow to process
    @param  subwindow_height The height of the subwindow to process 
    @return Standard stereoProcess status code
    
    This method sets the SVS parameters that SVS uses.  For more information on
    a given parameter, see its accessor method.
  */
  /*---------------------------------------------------------------------------*/
  int setSVSParams(int corrsize, int thresh, int ndisp, int offx, int svsUnique, int multi, int subwindow_x_off, int subwindow_y_off, int subwindow_width, int subwindow_height);

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Accessor for whether or not the blobfilter is active.
    @return 0 if blobfilter is inactive, otherwise 1

  */
  /*---------------------------------------------------------------------------*/
  int blobActive();

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Accessor for the treshold parameter of the blob filter.
    @return the treshold parameter

  */
  /*---------------------------------------------------------------------------*/
  int blobTresh();

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Accessor for the disparity tolerance parameter of the blob filter.
    @return the disparity tolerance parameter

  */
  /*---------------------------------------------------------------------------*/
  int blobDispTol();

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Mutator to set the parameters of the blob filter
    @param  blobActive           Determines whether the blob filter is active
    @param  blobThresh           Blobs occupying less pixels than this value will be removed
    @param  blobDispTol          If the relative disparity difference of adjacent pixels is greater than this value, the pixels are regarded as not connected.       
    @return Standard stereoProcess status code
    
    This method sets the parameters of the blob filter.
  */
  /*---------------------------------------------------------------------------*/
  int setBlobParams(int blobActive, int blobThresh, int blobDispTol);

  /**
     @brief Accessor for if sun detection is active or not
     @return 1 if active, 0 if not active
  */
  /*---------------------------------------------------------------------------*/
  int sunDetection();
  
    /*---------------------------------------------------------------------------*/
  /**
     @brief Accessor for half the horizontal width of which we should paint black around the sun
     @return the width in degrees
  */
  /*---------------------------------------------------------------------------*/
  int aspectWidth();


  /*---------------------------------------------------------------------------*/
  /**
     @brief Accessor for half the vertical width of which we should paint black around the sun
     @return the width in degrees
  */
  /*---------------------------------------------------------------------------*/
  int tiltWidth();
  

  /*---------------------------------------------------------------------------*/
  /**
     @brief  Mutator to set the parameters of the sun detection
     @param  sunDetection         Determines whether the sun detection is active
     @param  aspectWidth          How much to remove from the image around the sun, horizontally
     @param  tiltWidth            How much to remove from the image around the sun, vertically       
     @return Standard stereoProcess status code
     
     This method sets the parameters of the sun detection.
  */
  /*---------------------------------------------------------------------------*/
  int setSunDetectionParams(int sunDetection, int aspectWidth, int tiltWidth);


  /*---------------------------------------------------------------------------*/

 /**
     @brief Calculates a new subwindow depending on the pitch state.

     This method changes the subwindow depending on the pitch state of the vehicle.
  */
  /*---------------------------------------------------------------------------*/ 
 void pitchControl();

  /**
    @brief  Accessor for whether or not the pitch control is active.
    @return 0 if pitch control is inactive, otherwise 1

  */
   /*---------------------------------------------------------------------------*/
  int getPitchActive();

   /**
    @brief  Mutator to set if pitch control active or not
  
    @return Standard stereoProcess status code
    
  */
  /*---------------------------------------------------------------------------*/
  int setPitchActive(int pitchActive);  

  /**
     @brief Accessor for the y offset of the subwindow.
     @return y offset of the subwindow
  */
  /*---------------------------------------------------------------------------*/
  int getSubwindowYOff();

  int getNormalYOff();
  int setNormalYOff(int y_off);

  /**
     @brief Accessor for the y offset of the subwindow used by exposure control.
     @return y offset of the subwindow for exposure control.
  */
  /*---------------------------------------------------------------------------*/
  int getExposureYOff();

  /**
     @brief  generates a pointcloud in UTM coordinates
     @return true if successful, false otherwise
     
  */
  /*---------------------------------------------------------------------------*/
  bool stereoProcess::generateUTMPointCloud();


  /*---------------------------------------------------------------------------*/
  /**
     @brief  accessor function for the next point in the point cloud
     @return true if a new point is returned, false if there are no more points
     
  */
  /*---------------------------------------------------------------------------*/
  bool stereoProcess::getNextUTMPoint(NEDcoord* UTMPoint);
  
  /*---------------------------------------------------------------------------*/
  /**
     @brief  mutator function for cutoff parameters
     @return standard OK message.
     
  */
  /*---------------------------------------------------------------------------*/
  int stereoProcess::setCutoffParams(double MinRange, double MaxRange, double MaxRelativeAltitude, double MinRelativeAltitude);
  

  /*---------------------------------------------------------------------------*/
  /**
     @brief  accessor function for the minimum allowed range of 3D points
     @return guess     
  */
  /*---------------------------------------------------------------------------*/
  double stereoProcess::getMinRange();


  /*---------------------------------------------------------------------------*/
  /**
     @brief  accessor function for the maximum allowed range of 3D points
     @return guess     
  */
  /*---------------------------------------------------------------------------*/
  double stereoProcess::getMaxRange();

  /*---------------------------------------------------------------------------*/
  /**
     @brief  accessor function for the maximum allowed Z difference of 3D points (relative to vehicle)
     @return guess     
  */
  /*---------------------------------------------------------------------------*/
  double stereoProcess::getMaxRelativeAltitude();

  /*---------------------------------------------------------------------------*/
  /**
     @brief  accessor function for the minimum allowed Z difference of 3D points (relative to vehicle)
     @return guess     
  */
  /*---------------------------------------------------------------------------*/
  double stereoProcess::getMinRelativeAltitude();

private:
  
  IplImage* stereoImages[MAX_CAMERAS];
  IplImage* tempImages[MAX_CAMERAS];

  //SVS variables
  svsStoredImages *videoObject;
  svsMultiProcess *processObject;
  svsStereoImage *imageObject;

  // for subwindowing
  svsStoredImages *videoObject_subw;
  svsStereoImage *imageObject_subw;
  short* disparityMemory;

  //Transformation variables
  // frames cameraFrame;
  Matrix camera2Body;
  Matrix body2Nav;
  Matrix camera2Nav;
  Matrix m_PointCloud;
  Matrix m_UTMPointCloud;
  int m_nbrUTMPoints;
  int m_UTMPointCounter; 

  VehicleState _currentState;

  char _currentFilename[256];
  char _currentFileType[10];

  int _currentSourceType;
  int _pairIndex;

  int _verboseLevel;


  svsWindow *rectLeftWindow;
  svsWindow *rectRightWindow;
  svsWindow *dispWindow;

  int showingDisp;
  int showingRectLeft;
  int showingRectRight;

  pthread_mutex_t _paramsMutex;

  //blobfilter stuff
  short *dispIm;
  int m_blobActive, m_blobTresh, m_blobDispTol;
  int c[480*640]; // color matrix
  void flood(int pixel, int k, short* dispIm);
  int nbrPixels;
  int lastPixelRow;

  // sun detection
  int m_sunDetection, m_aspectWidth, m_tiltWidth;
  int fov_h, fov_v;        //field of view

  int m_pitchActive;
  int exp_off_add;
  int y_off_add;
  int normal_y_off;

  // point cutoffs
  double m_PointMinRange, m_PointMaxRange, m_PointMaxRelativeAltitude, m_PointMinRelativeAltitude;
};

#endif  //__STEREOPROCESS_H__
