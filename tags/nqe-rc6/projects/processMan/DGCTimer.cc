
/**
 * DGCTimer.cc
 * Revision History:
 * 25/08/2005 hbarnor created
 * $Id$
 * Watchdog timer class.
 */

#include "DGCTimer.hh"

DGCTimer::DGCTimer(double timeOut, sigc::signal<void> * action)
  : resetReceived(false),
    enable(false),
    m_timeOut(timeOut)  
{
  DGCcreateMutex(&resetMutex);
  m_action = action;
  long nsec = (long) (m_timeOut * MILLITONANO);
  timeVal.tv_sec = 0;
  timeVal.tv_nsec = nsec;
  DGCstartMemberFunctionThread(this,&DGCTimer::run);
}


DGCTimer::~DGCTimer()
{
  cout << "Timer Destructor" << endl;
}

void DGCTimer::start()
{
  enable = true;
}

void DGCTimer::run()
{
  while(enable)
    {	
      nanosleep(&timeVal, &remValue);
      if(!resetReceived)
	{
	  m_action->emit();
	}
      DGClockMutex(&resetMutex);
      resetReceived = false;
      DGCunlockMutex(&resetMutex);      
    }
}

void DGCTimer::stop()
{
  enable = false;
}

void DGCTimer::reset()
{
#ifdef DEBUG 
  cerr << " Reset called" << endl;
#endif  
  DGClockMutex(&resetMutex);
  resetReceived = true;
  DGCunlockMutex(&resetMutex);      
}
