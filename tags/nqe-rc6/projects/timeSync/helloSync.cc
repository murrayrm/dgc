#include <iostream>
#include <string>
#include <fstream>
#include <octave/oct.h>

using namespace std;

int main(int argc, char** argv) 
{
  if (argc != 3)
    {
      cout << "Error... need 2 files to operate on!" << endl;
      return 1;
    }

  string one_filename(argv[2]);
  fstream one_stream(argv[2], fstream::out);

  string two_filename(argv[3]);
  fstream two_stream(argv[3], fstream::out);



   int sz=20;
   Matrix m = Matrix(sz,sz);
   for (int r=0;r<sz;r++) 
   {
     for (int c=0;c<sz;c++) 
     {
       m(r,c)=r*c;
     }
   }
   cout << "Hello world! " << endl;
   cout << m;

   return 0;
}
