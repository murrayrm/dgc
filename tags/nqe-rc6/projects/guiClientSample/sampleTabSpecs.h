#define TABNAME Sample

// fields: type, name, default value, widgetType, xLabel, yLabel, xVal, yVal
// everything is TAB-RELATIVE. Thus input is FROM the client INTO the tab
#define TABINPUTS(_) \
  _(unsigned,SAMPLE_INPUT_UNSIGNED,   0, Entry, 0,0, 1,0 )	\
	_(double  ,SAMPLE_INPUT_DOUBLE  , 1.0, Entry, 0,1, 1,1 ) \
_(double, SAMPLE_SINGLE_ENTRY, 1.0, SEntry, 10, 10, 1,4) \
_(double, SAMPLE_SINGLE_LABEL, 1.0, SLabel, 10, 10, 2,4) 

#define TABOUTPUTS(_) \
  _(int, SAMPLE_OUTPUT_INT, -1, Entry, 0 , 3, 1,3  )	\
  _(int, THIS_IS_A_LABEL  ,  0, Label, 0,5, 1,5  )

// fields: are plotTitle, xLabel, yLabel, xMin, xMax, majorXTick,
// numMinorXticks, yMin, yMax, majorYTick, numMinorYTicks, autoscale,
// legend 
#define TABPLOTS(_) \
 _(TestPlot, Time(s), Random, 0.0, 20.0, 2, 1, 0.0, 1.0,0.1, 1, false, Rand)

//#undef TABNAME
