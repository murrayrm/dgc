/**
 * DGCTab.cc
 * Revision History:
 * 07/07/2005  hbarnor  Created
 * $Id: DGCTab.cc 8530 2005-07-11 06:26:58Z hbarnor $
 */

#include <iostream>

#include "DGCTab.hh"
/**
DGCTab::DGCTab()
  : NUM_ROWS(MAXROWS),
    NUM_COLS(MAXCOLS),
    m_userTable(NUM_ROWS, NUM_COLS, false),
    m_sendBtn("send")
{
  pack_start(m_userTable);
  m_hBtnBox.pack_start(m_sendBtn);
  pack_end(m_hBtnBox);
  init();
  show_all_children();
}
*/

DGCTab::DGCTab(skynet* messenger)
  : NUM_ROWS(MAXROWS),
    NUM_COLS(MAXCOLS),
    m_userTable(NUM_ROWS, NUM_COLS, false),
    m_sendBtn("send")
{
  m_skynet = messenger;
  pack_start(m_userTable);
  m_hBtnBox.pack_start(m_sendBtn);
  m_sendBtn.signal_clicked().connect(sigc::mem_fun(this, &DGCTab::updateClient));
  pack_end(m_hBtnBox);
  init();
  show_all_children();
}
DGCTab::~DGCTab()
{
  delete m_plotArray;
}
void DGCTab::init()
{
  //currently empty
}

void DGCTab::addLabelWidget(string name, string defValue,guint col1, guint row1, guint col2, guint row2)
{
  //cout << "Adding label: Specs: " << col1 << " , " << row1 << " , " << col2 << " , " << row2 << endl;
  DGCLabel * tempLabel = manage(new DGCLabel(name)); 
  addToTable(tempLabel, col1, row1);
  addToShadow(tempLabel, col1, row1);
  tempLabel = manage(new DGCLabel(defValue)); 
  addToTable(tempLabel, col2, row2);
  addToShadow(tempLabel, col2, row2);
}

void DGCTab::addSLabelWidget(string name, string defValue,guint col1, guint row1, guint col2, guint row2)
{
  //cout << "Adding label: Specs: " << col1 << " , " << row1 << " , " << col2 << " , " << row2 << endl;
  DGCLabel * tempLabel = manage(new DGCLabel(defValue)); 
  addToTable(tempLabel, col2, row2);
  addToShadow(tempLabel, col2, row2);
}

void DGCTab::addButtonWidget(string name, string defValue,guint col1, guint row1, guint col2, guint row2)
{
  //cout << "Adding button: Specs: " << col1 << " , " << row1 << " , " << col2 << " , " << row2 << endl;
  DGCButton* tempBtn = manage(new DGCButton(name));
  addToTable(tempBtn, col2, row2);
  addToShadow(tempBtn, col2, row2);
}

void DGCTab::addEntryWidget(string name, string defValue, guint col1, guint row1, guint col2, guint row2)
{
  //cout << "Adding entry: Specs: " << col1 << " , " << row1 << " , " << col2 << " , " << row2 << endl;
  DGCLabel * tempLabel = manage(new DGCLabel(name)); 
  addToTable(tempLabel, col1, row1);
  addToShadow(tempLabel, col1, row1);
  DGCEntry * tempEntry = manage(new DGCEntry(defValue)); 
  addToTable(tempEntry, col2, row2);
  addToShadow(tempEntry, col2, row2);
}

void DGCTab::addSEntryWidget(string name, string defValue, guint col1, guint row1, guint col2, guint row2)
{
  //cout << "Adding entry: Specs: " << col1 << " , " << row1 << " , " << col2 << " , " << row2 << endl;
  DGCEntry * tempEntry = manage(new DGCEntry(defValue)); 
  addToTable(tempEntry, col2, row2);
  addToShadow(tempEntry, col2, row2);
}

void DGCTab::addToTable(Widget* widget, guint posX, guint posY)
{  
  //cout << " at (" << posX  << "," << posY  <<") " << endl;
  // error checking 
  if(posX+1 <= NUM_COLS && posY+1 <= NUM_ROWS)
    {  
      m_userTable.attach(*widget, posX, posX+1, posY, posY+1, Gtk::AttachOptions(),Gtk::AttachOptions(),5,5);
      show_all_children();
    }
  else
    {
      string s = "negative or too big cell specification";
      cout << s << endl;
      throw s;      
    }
}

void DGCTab::addToShadow(DGCWidget* widget, guint posX, guint posY)
{  
  // error checking 
  if(posX+1 <= NUM_COLS && posY+1 <= NUM_ROWS)
    {  
      m_shadowTable[posX][posY] = widget;
    }
  else
    {
      string s = "negative or too big cell specification";
      cout << s << endl;
      //throw s;      
    }
}

void DGCTab::setWidgetValue(stringstream& newValue, guint col, guint row)
{
  Glib::ustring temp = Glib::ustring(newValue.str());
  if( col+1 <= NUM_COLS && row+1 <= NUM_ROWS)
    { 
      m_shadowTable[col][row]->setText(temp);     
    }
}

void DGCTab::getWidgetValue(stringstream& newValue, guint col, guint row)
{
    if( col+1 <= NUM_COLS && row+1 <= NUM_ROWS)
    {
      newValue << m_shadowTable[col][row]->getText();
    }
}

void DGCTab::addPlot(string title, string xLabel, string yLabel, double xMin, double xMax, double majorXTick, int numMinorXTicks, double yMin, double yMax, double majorYTick, int numMinorYTicks, bool autoscale, string legend)
{

  Gtk::VBox  *tBox = manage(new Gtk::VBox());
  m_plotArray = new DGCPlot((GtkWidget*)tBox->gobj());
  m_plotArray->setTitle(title);
  m_plotArray->setXLabel(xLabel);
  m_plotArray->setYLabel(yLabel);
  m_plotArray->setXRange(xMin, xMax, majorXTick, numMinorXTicks);
  m_plotArray->setYRange(yMin, yMax, majorYTick, numMinorYTicks);
  m_plotArray->setAutoScale(autoscale);
  if(legend == "NULL")
    {
      m_plotArray->setLegend(legend);
      m_plotArray->displayLegend(true);
    }
  pack_start(*tBox);
  show_all_children();
}
