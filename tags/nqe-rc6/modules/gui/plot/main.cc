/**
 * example main code. 
 * $Id$
 */

#include <gtkmm.h>
#include "DGCPlot.hh"
#include <iostream>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

//DGCPlot * myFix;


//using namespace std;
int main(int argc, char *argv[])
{
  Gtk::Window * window;
  Gtk::Main * kit;
  kit = new Gtk::Main(argc, argv);
  window = new Gtk::Window();
  window->show_all_children();
  
  Gtk::VBox * box = new Gtk::VBox(FALSE, 0);
  window->add(*box);
  //myFix= new DGCPlot((GtkWidget*)window->gobj());
  
  DGCPlot myFix(GTK_WIDGET(box->gobj()));
  myFix.setTitle("Example 1");
  myFix.setXLabel("Time (s)");
  myFix.setYLabel("value (H)");
  //myFix.setAutoScale(true);
  myFix.setXRange(0.0, 20.0, 2, 1);
  myFix.setYRange(0.0, 1, 0.1, 1);
  myFix.setLegend("Blah");
  myFix.displayLegend(true);
  //gtk_plot_set_range(GTK_PLOT(plotInstance), 0. ,20., 0., 1.);
  //gtk_plot_set_ticks(GTK_PLOT(plotInstance), GTK_PLOT_AXIS_X, 2, 1);
  //gtk_plot_set_ticks(GTK_PLOT(plotInstance), GTK_PLOT_AXIS_Y, .1, 1);

  //Glib::thread_init();  
  //box.show_all_children();
  window->show_all_children();

  //Glib::Thread * m_thread = Glib::Thread::create( sigc::ptr_fun(&update), false);
  kit->run(*window); //Shows the window and returns when it is closed.    
  
  //delete window;
  //delete kit;
  return 0;
}




