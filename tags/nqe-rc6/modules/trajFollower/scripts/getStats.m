function output=getStats(data)

# returns MSE and max yerror
##these are basic performance requirements
# MSE change in command, max change in command
##hopefully will catch oscillations
# MSE difference between cmd and actual, max diff between cmd and actual
## possible way to tell how hard on the vehicle this was

     rows = size(data)(1)
     cols = size(data)(2)

     mseYerr=mean(data(:,3).*data(:,3));
     maxYerr=max(abs(data(:,3)));
avgYerr=mean(abs(data(:,3)));

mseDeltaCmd=mean((data(:,cols)-data(:,14)).*(data(:,cols)-data(:,14)));
maxDeltaCmd=max(abs(data(:,cols)-data(:,14)));

mseDiffCmd=mean((data(:,40)-data(:,14)).*(data(:,49)-data(:,14)));
maxDiffCmd=max(abs(data(:,40)-data(:,14)));



     output = [mseYerr,maxYerr,avgYerr,mseDeltaCmd,maxDeltaCmd,mseDiffCmd,maxDiffCmd];

end
