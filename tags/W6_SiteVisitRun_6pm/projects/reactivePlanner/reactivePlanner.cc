#include <math.h>
#include <sys/time.h>
#include "frames/coords.hh"
#include "rddf.hh"
#include "CCorridorPainter.hh"
#include "AliceConstants.h"
#include "CMap.hh"
#include "reactivePlanner.hh"
using namespace std;

reactivePlanner::reactivePlanner()
{
  traj = new CTraj(3);
  m_arcs = new double[4 * POINTS_PER_ARC * LINES_DEEP * POINTS_WIDE * (2 -
      POINTS_WIDE * (LINES_DEEP - 2 * LINES_LONG + 1)) / 2];
}

reactivePlanner::~reactivePlanner()
{
  delete traj;
    delete [] m_arcs;
}

CTraj *reactivePlanner::plan(RDDF *rddf, CMap *map, int layer,
          NEcoord &initialPoint, double initialTheta, double initialSpeed)
{
  m_rddf = rddf;
  m_layer = layer;
  m_map = map;
  points[0] = initialPoint;
  theta[0] = initialTheta;
  m_speed0 = initialSpeed;

  pickPoints();

  makeBundles();

  pickArcs();

  makeTrajFromArcs();
  
  return traj;
}

/*Produces lines of points perpendicular to the RDDF track line.
  Also gives the heading of points in each line*/
void reactivePlanner::pickPoints()
{
  int i, j, p, q;
  double corRad, dF, dB, tF, tB, dtF, dtB;
  NEcoord pos, vec;

  for(i = 0; i < LINES_LONG; i++)
  {   
    pos = m_rddf->getPointAlongTrackLine(points[0], DISTANCE_BETWEEN_LINES *
        (i + 1), &theta[i + 1], &corRad);
    //Rotate trackline yaw if near waypoint to make smoother turns.
    p = m_rddf->getNumTargetPoints();
    q = m_rddf->getCurrentWaypointNumber(pos);
    if(q > 0) dB = (pos - m_rddf->getWaypoint(q)).norm();
    else dB = 1e10;
    if(q < p) dF = (pos - m_rddf->getWaypoint(q + 1)).norm();
    else dF = 1e10;
    if(dB < corRad && dF < corRad)
    {
      tB = m_rddf->getTrackLineYaw(q - 1);
      tF = m_rddf->getTrackLineYaw(q + 1);
      dtB = fmod(theta[i + 1] - tB, 2 * M_PI);
      if(dtB <= -M_PI) dtB  += 2 * M_PI;
      else if(dtB > M_PI) dtB  -= 2 * M_PI;
      dtF = fmod(tF - theta[i + 1], 2 * M_PI);
      if(dtF <= -M_PI) dtF  += 2 * M_PI;
      else if(dtF > M_PI) dtF  -= 2 * M_PI;
      theta[i + 1] += (dB * dtF - dF * dtB) / (2 * (dB + dF));
    }
    else if(dB < corRad)
    {
      tB = m_rddf->getTrackLineYaw(q - 1);
      dtB = fmod(theta[i + 1] - tB, 2 * M_PI);
      if(dtB <= -M_PI) dtB  += 2 * M_PI;
      else if(dtB > M_PI) dtB  -= 2 * M_PI;
      theta[i + 1] -= (corRad - dB) * dtB / (2 * corRad);
    }
    else if(dF < corRad)
    {
      tF = m_rddf->getTrackLineYaw(q + 1);
      dtF = fmod(tF - theta[i + 1], 2 * M_PI);
      if(dtF <= -M_PI) dtF  += 2 * M_PI;
      else if(dtF > M_PI) dtF  -= 2 * M_PI;
      theta[i + 1] += (corRad - dF) * dtF / (2 * corRad);
    }
    //Convert corridor radius to effective corridor radius.
    if(corRad > RDDF_VEHWIDTH_EFF) corRad -= RDDF_VEHWIDTH_EFF;
    else corRad = 0;
    if(corRad > MAX_WIDTH) corRad = MAX_WIDTH;
    //Vector from pos to right corridor boundary.
    vec = NEcoord(-corRad * sin(theta[i + 1]), corRad * cos(theta[i + 1]));
    //Points are evenly spaced between the effective corridor boundaries.
    #warning Need to worry about wide corridors.
    for(j=0; j<POINTS_WIDE; j++)
      points[INDEX2(i, j)] = pos + vec * (2 * (double)j /
          (POINTS_WIDE - 1) - 1);
  }
}

void reactivePlanner::makeBundles()
{
  int i, j, k, f;

  for(f = 0; f < LINES_DEEP; f++)
  {
    for(k = 0; k < POINTS_WIDE; k++)
    {
      makeArc(INDEX2a(f, k), points[0], theta[0], points[INDEX2(f, k)],
          theta[f + 1]);
      for(i = 0; i < LINES_LONG - f - 1; i++)
        for(j = 0; j < POINTS_WIDE; j++)
          makeArc(INDEX4(i, j, f, k), points[INDEX2(i, j)], theta[i + 1],
              points[INDEX2(i + f + 1, k)], theta[i + f + 2]);
    }
  }
}

/*Returns a pointer to a traj made up of 2 circular arcs that smoothly connect
  two unit vectors.  Also, gives the radius of the arcs and the arc length
  between points.*/
void reactivePlanner::makeArc(int index, NEcoord p0, double theta0,
    NEcoord p1, double theta1)
{
  makeArc(index, p0.N, p0.E, theta0, p1.N, p1.E, theta1);
}

/*Returns a pointer to a traj made up of 2 circular arcs that smoothly connect
  two unit vectors.  Also, gives the radius of the arcs and the arc length
  between points.*/
void reactivePlanner::makeArc(int index, double x0, double y0,
    double theta0, double x1, double y1, double theta1)
{
  int i, m, n, p;
  double x, y, theta, r, ra, rb, d, ang0, ang1, turn0, turn1, turn0a, turn1a,
          turn0b, turn1b, angD, l, la, lb,
          ct0, st0, ct, st, c0, c1, c2, cn0, ce0, cn1, ce1, ctt;

  ct0 = cos(theta0);
  st0 = sin(theta0);
  //Translate and rotate coordinate system to simplify equations.
  x = (x1 - x0) * ct0 + (y1 - y0) * st0;
  y = (y1 - y0) * ct0 + (x0 - x1) * st0;
  theta = fmod(theta1 - theta0, 2 * M_PI);
  if(theta < 0) theta += 2 * M_PI;
  if(0 <= theta && theta < 0.00001) theta = 0.00001;  //can't div by 0
  if(2 * M_PI - 0.00001 < theta && theta <= 2 * M_PI)
    theta = 2 * M_PI - 0.00001;
  //Thus spake Mathematica: (see makeArc.nb)
  ct = cos(theta);
  st = sin(theta);
  c0 = y + y * ct - x * st;
  c1 = sqrt(3 * x * x + 4 * y * y - (x * ct + y * st) * 
        (x * (2 + ct) + y * st));
  c2 = -2 + 2 * ct;

  ra = (c0 - c1) / c2;
  if(ra >= 0)
  {
    turn0a = fmod(atan2(x + ra * st, ra - (y - ra * ct)), 2 * M_PI);
    turn1a = fmod(turn0a - theta, 2 * M_PI);
  }
  else
  {
    turn0a = fmod(atan2(x + ra * st, y - ra * ct - ra), 2 * M_PI);
    turn1a = fmod(turn0a + theta, 2 * M_PI);
  }
  if(turn0a < 0) turn0a += 2 * M_PI;
  if(turn1a < 0) turn1a += 2 * M_PI;
  la = fabs(ra) * (turn0a + turn1a);

  rb = (c0 + c1) / c2;
  if(rb >= 0)
  {
    turn0b = fmod(atan2(x + rb * st, rb - (y - rb * ct)), 2 * M_PI);
    turn1b = fmod(turn0b - theta, 2 * M_PI);
  }
  else
  {
    turn0b = fmod(atan2(x + rb * st, y - rb * ct - rb), 2 * M_PI);
    turn1b = fmod(turn0b + theta, 2 * M_PI);
  }
  if(turn0b < 0) turn0b += 2 * M_PI;
  if(turn1b < 0) turn1b += 2 * M_PI;
  lb = fabs(rb) * (turn0b + turn1b);

  if(la <= lb) { r = ra; turn0 = turn0a; turn1 = turn1a; l = la; }
  else { r = rb; turn0 = turn0b; turn1 = turn1b; l = lb; }

  p = min((int)(l / INTERNAL_TRAJ_SPACING + 1), POINTS_PER_ARC);
  angD = (turn0 + turn1) / p;
  d = l / p;
  m = (int)(turn0 / angD) + 1;
  n = p - m;

  cn0 = x0 - r * st0;
  ce0 = y0 + r * ct0;
  if(r >= 0) ctt = theta0 + turn0;
  else
  {
    angD = -angD;
    ctt = theta0 - turn0;
  }
  cn1 = cn0 + 2 * r * sin(ctt);
  ce1 = ce0 - 2 * r * cos(ctt);
  for(i = 0; i < m; i++)
  {
    ang0 = i * angD + theta0;
    m_arcs[INDEX3(index, i, 2)] = cos(ang0);
    m_arcs[INDEX3(index, i, 3)] = sin(ang0);
    m_arcs[INDEX3(index, i, 0)] = cn0 + r * m_arcs[INDEX3(index, i, 3)];
    m_arcs[INDEX3(index, i, 1)] = ce0 - r * m_arcs[INDEX3(index, i, 2)];
  }
  angD = -angD;
  for(; i < p; i++)
  {
    ang1 = (i - m + 1) * angD + ctt;
    m_arcs[INDEX3(index, i, 2)] = cos(ang1);
    m_arcs[INDEX3(index, i, 3)] = sin(ang1);
    m_arcs[INDEX3(index, i, 0)] = cn1 - r * m_arcs[INDEX3(index, i, 3)];
    m_arcs[INDEX3(index, i, 1)] = ce1 + r * m_arcs[INDEX3(index, i, 2)];
  }
  
  m_r[index] = r; m_d[index] = d; m_p[index] = p; m_m[index] = m;
  m_n[index] = n;
}

void reactivePlanner::pickArcs()
{
  int i, j, k, f, a[(LINES_LONG - 1) * POINTS_WIDE],
      a1[(LINES_LONG - 1) * POINTS_WIDE], a2[LINES_LONG], a3[LINES_LONG - 1];
  double cost[LINES_LONG * POINTS_WIDE], temp;
  
  for(k = 0; k < POINTS_WIDE; k++)
  {
    if(fabs(m_r[k]) >= TURNING_RADIUS && !isTrajObstructed(k, m_map, m_layer))
    {
      cost[k] = evalCost(k, m_map, m_layer);
    }
    else
    {
      cost[k] = evalCost(k, m_map, m_layer) * 1.e10;
    }
  }
  for(i = 0; i < LINES_LONG - 1; i++)
  {
    for(k = 0; k < POINTS_WIDE; k++)
    {
      a[INDEX2a(i, k)] = 0;
      a1[INDEX2a(i, k)] = 0;
      if(fabs(m_r[INDEX4(i, 0, 0, k)]) >= TURNING_RADIUS &&
          !isTrajObstructed(INDEX4(i, 0, 0, k), m_map, m_layer))
      {
        cost[INDEX2a(i + 1, k)] = cost[INDEX2a(i, 0)] + evalCost(INDEX4(i, 0, 0,
            k), m_map, m_layer);
      }
      else
      {
        cost[INDEX2a(i + 1, k)] = cost[INDEX2a(i, 0)] + evalCost(INDEX4(i, 0, 0,
            k), m_map, m_layer) * 1.e10;
      }
      for(j = 1; j < POINTS_WIDE; j++)
      {
        if(fabs(m_r[INDEX4(i, j, 0, k)]) >= TURNING_RADIUS &&
            !isTrajObstructed(INDEX4(i, j, 0, k), m_map, m_layer))
        {
          temp = cost[INDEX2a(i, j)] + evalCost(INDEX4(i, j, 0, k), m_map,
              m_layer);
        }
        else
        {
          temp = cost[INDEX2a(i, j)] + evalCost(INDEX4(i, j, 0, k), m_map,
              m_layer) * 1.e10;
        }
        if(temp < cost[INDEX2a(i + 1, k)])
        {
          a[INDEX2a(i, k)] = j;
          cost[INDEX2a(i + 1, k)] = temp;
        }
      }
      if(i < LINES_DEEP - 1)
      {
        if(fabs(m_r[INDEX2a(i + 1, k)]) >= TURNING_RADIUS &&
            !isTrajObstructed(INDEX2a(i + 1, k), m_map, m_layer))
        {
          temp = evalCost(INDEX2a(i + 1, k), m_map, m_layer);
        }
        else
        {
          temp = evalCost(INDEX2a(i + 1, k), m_map, m_layer) * 1.e10;
        }
        if(temp < cost[INDEX2a(i + 1, k)])
        {
          a1[INDEX2a(i, k)] = i + 1;
          cost[INDEX2a(i + 1, k)] = temp;
        }
        for(f = 1; f < i; f++)
        {
          for(j = 0; j < POINTS_WIDE; j++)
          {
            if(fabs(m_r[INDEX4(i - f, j, f, k)]) >= TURNING_RADIUS &&
                !isTrajObstructed(INDEX4(i - f, j, f, k), m_map, m_layer))
            {
              temp = cost[INDEX2a(i - f, j)] + evalCost(INDEX4(i - f, j, f, k),
                  m_map, m_layer);
            }
            else
            {
              temp = cost[INDEX2a(i - f, j)] + evalCost(INDEX4(i - f, j, f, k),
                  m_map, m_layer) * 1.e10;
            }
            if(temp < cost[INDEX2a(i + 1, k)])
            {
              a[INDEX2a(i, k)] = j;
              a1[INDEX2a(i, k)] = f;
              cost[INDEX2a(i + 1, k)] = temp;
            }
          }
        }
      }
      else
      {
        for(f = 1; f < LINES_DEEP; f++)
        {
          for(j = 0; j < POINTS_WIDE; j++)
          {
            if(fabs(m_r[INDEX4(i - f, j, f, k)]) >= TURNING_RADIUS &&
                !isTrajObstructed(INDEX4(i - f, j, f, k), m_map, m_layer))
            {
              temp = cost[INDEX2a(i - f, j)] + evalCost(INDEX4(i - f, j, f, k),
                  m_map, m_layer);
            }
            else
            {
              temp = cost[INDEX2a(i - f, j)] + evalCost(INDEX4(i - f, j, f, k),
                  m_map, m_layer) * 1.e10;
            }
            if(temp < cost[INDEX2a(i + 1, k)])
            {
              a[INDEX2a(i, k)] = j;
              a1[INDEX2a(i, k)] = f;
              cost[INDEX2a(i + 1, k)] = temp;
            }
          }
        }
      }
    }
  }
  a2[0] = 0;
  temp = cost[INDEX2a(LINES_LONG - 1, 0)];
  for(k = 1; k < POINTS_WIDE; k++)
  {
    if(cost[INDEX2a(LINES_LONG - 1, k)] < temp)
    {
      a2[0] = k;
      temp = cost[INDEX2a(LINES_LONG - 1, k)];
    }
  }
  i = LINES_LONG - 2;
  m_g = 1;
  while(i >= 0 && i - a1[INDEX2a(i, a2[m_g - 1])] >= 0)
  {
    a2[m_g] = a[INDEX2a(i, a2[m_g - 1])];
    i -= a1[INDEX2a(i, a2[m_g - 1])] + 1;
    a3[m_g - 1] = i + 1;
    m_g++;
  }
  b[0] = INDEX2a(a3[m_g - 2], a2[m_g - 1]);
  for(i = 1; i < m_g - 1; i++)
  {
    b[i] = INDEX4(a3[m_g - i - 1], a2[m_g - i], a3[m_g - i - 2] -
        a3[m_g - i - 1] - 1, a2[m_g - i - 1]);
  }
  b[m_g - 1] = INDEX4(a3[0], a2[1], LINES_LONG - a3[0] - 2, a2[0]);
}

void reactivePlanner::makeTrajFromArcs()
{
  int i, j, p, q, c;
  double maxV, speed, nextSpeed, speedF[TRAJ_MAX_LEN], speedB[TRAJ_MAX_LEN],
          speedI, speedD, northing[3], easting[3], accel;

  traj->startDataInput();
  p = 0;
  speedF[0] = fmin(m_speed0, m_map->getDataUTM<double>(m_layer,
      m_arcs[INDEX3(b[0], 0, 0)], m_arcs[INDEX3(b[0], 0, 1)]));
  speedI = 2 * MAX_ACCEL * m_d[b[0]];
  maxV = sqrt(fabs(MAX_LAT_ACCEL * m_r[b[0]]));
  q = m_p[b[0]];
  for(j = 1; j < q; j++)
  {
    speedF[j] = fmin(fmin(sqrt(speedF[j - 1] * speedF[j - 1] + speedI), maxV),
        m_map->getDataUTM<double>(m_layer, m_arcs[INDEX3(b[0], j, 0)],
        m_arcs[INDEX3(b[0], j, 1)]));
  }
  p += q;
  for(i = 1; i < m_g; i++)
  {
    speedI = 2 * MAX_ACCEL * m_d[b[i]];
    maxV = sqrt(fabs(MAX_LAT_ACCEL * m_r[b[i]]));
    q = m_p[b[i]];
    for(j = 0; j < q; j++)
    {
      speedF[p + j] = fmin(fmin(sqrt(speedF[p + j - 1] * speedF[p + j - 1] +
          speedI), maxV), m_map->getDataUTM<double>(m_layer,
          m_arcs[INDEX3(b[i], j, 0)], m_arcs[INDEX3(b[i], j, 1)]));
    }
    p += q;
  }

  q = m_p[b[m_g - 1]];
  speedB[p - 1] = m_map->getDataUTM<double>(m_layer, m_arcs[INDEX3(b[m_g - 1],
      q - 1, 0)], m_arcs[INDEX3(b[m_g - 1], q - 1, 1)]);
  speedD = -2 * MAX_DECEL * m_d[b[m_g - 1]];
  maxV = sqrt(fabs(MAX_LAT_ACCEL * m_r[b[m_g - 1]]));
  for(j = - 2; j >= -q; j--)
  {
    speedB[p + j] = fmin(fmin(sqrt(speedB[p + j + 1] * speedB[p + j + 1] +
        speedD), maxV), m_map->getDataUTM<double>(m_layer, m_arcs[INDEX3(b[m_g -
        1], q + j, 0)], m_arcs[INDEX3(b[m_g - 1], q + j, 1)]));
  }
  p -= q;
  for(i = m_g - 2; i >= 0; i--)
  {
    speedD = -2 * MAX_DECEL * m_d[b[i]];
    maxV = sqrt(fabs(MAX_LAT_ACCEL * m_r[b[i]]));
    q = m_p[b[i]];
    for(j = -1; j >= -q; j--)
    {
      speedB[p + j] = fmin(fmin(sqrt(speedB[p + j + 1] * speedB[p + j + 1] +
          speedD), maxV), m_map->getDataUTM<double>(m_layer, m_arcs[INDEX3(b[i],
          q + j, 0)], m_arcs[INDEX3(b[i], q + j, 1)]));
    }
    p -= q;
  }

  nextSpeed = fmax(fmin(speedF[0], speedB[0]), MIN_SPEED);
  for(i = 0; i < m_g - 1; i++)
  {
    for(j = 0; j < m_m[b[i]]; j++)
    {
      speed = nextSpeed;
      nextSpeed = fmax(fmin(speedF[p + j + 1], speedB[p + j + 1]), MIN_SPEED);
      accel = (nextSpeed * nextSpeed - speed * speed) / (2 * m_d[b[i]]);
      northing[0] = m_arcs[INDEX3(b[i], j, 0)];
      easting[0] = m_arcs[INDEX3(b[i], j, 1)];
      northing[1] = m_arcs[INDEX3(b[i], j, 2)] * speed;
      easting[1] = m_arcs[INDEX3(b[i], j, 3)] * speed;
      northing[2] = -easting[1] * speed / m_r[b[i]] + accel *
          m_arcs[INDEX3(b[i], j, 2)];
      easting[2] = northing[1] * speed / m_r[b[i]] + accel *
          m_arcs[INDEX3(b[i], j, 3)];
      traj->inputWithDiffs(northing, easting);
    }
    p += m_m[b[i]];
    q = m_m[b[i]];
    for(j = 0; j < m_n[b[i]]; j++)
    {
      speed = nextSpeed;
      nextSpeed = fmax(fmin(speedF[p + j + 1], speedB[p + j + 1]), MIN_SPEED);
      accel = (nextSpeed * nextSpeed - speed * speed) / (2 * m_d[b[i]]);
      northing[0] = m_arcs[INDEX3(b[i], q + j, 0)];
      easting[0] = m_arcs[INDEX3(b[i], q + j, 1)];
      northing[1] = m_arcs[INDEX3(b[i], q + j, 2)] * speed;
      easting[1] = m_arcs[INDEX3(b[i], q + j, 3)] * speed;
      northing[2] = easting[1] * speed / m_r[b[i]] + accel *
          m_arcs[INDEX3(b[i], q + j, 2)];
      easting[2] = -northing[1] * speed / m_r[b[i]] + accel *
          m_arcs[INDEX3(b[i], q + j, 3)];
      traj->inputWithDiffs(northing, easting);
    }
    p += m_n[b[i]];
  }
  if(m_n[b[m_g - 1]] <= 0)
  {
    c = m_m[b[m_g - 1]] - 1;
  }
  else
  {
    c = m_m[b[m_g - 1]];
  }
  for(j = 0; j < c; j++)
  {
    speed = nextSpeed;
    nextSpeed = fmax(fmin(speedF[p + j + 1], speedB[p + j + 1]), MIN_SPEED);
    accel = (nextSpeed * nextSpeed - speed * speed) / (2 * m_d[b[m_g -1]]);
    northing[0] = m_arcs[INDEX3(b[m_g - 1], j, 0)];
    easting[0] = m_arcs[INDEX3(b[m_g - 1], j, 1)];
    northing[1] = m_arcs[INDEX3(b[m_g - 1], j, 2)] * speed;
    easting[1] = m_arcs[INDEX3(b[m_g - 1], j, 3)] * speed;
    northing[2] = -easting[1] * speed / m_r[b[m_g - 1]] + accel *
        m_arcs[INDEX3(b[m_g - 1], j, 2)];
    easting[2] = northing[1] * speed / m_r[b[m_g - 1]] + accel *
        m_arcs[INDEX3(b[m_g - 1], j, 3)];
    traj->inputWithDiffs(northing, easting);
  }
  p += m_m[b[m_g - 1]];
  q = m_m[b[i]];
  for(j = 0; j < m_n[b[m_g - 1]] - 1; j++)
  {
    speed = nextSpeed;
    nextSpeed = fmax(fmin(speedF[p + j + 1], speedB[p + j + 1]), MIN_SPEED);
    accel = (nextSpeed * nextSpeed - speed * speed) / (2 * m_d[b[m_g - 1]]);
    northing[0] = m_arcs[INDEX3(b[m_g - 1], q + j, 0)];
    easting[0] = m_arcs[INDEX3(b[m_g - 1], q + j, 1)];
    northing[1] = m_arcs[INDEX3(b[m_g - 1], q + j, 2)] * speed;
    easting[1] = m_arcs[INDEX3(b[m_g - 1], q + j, 3)] * speed;
    northing[2] = easting[1] * speed / m_r[b[m_g - 1]] + accel *
        m_arcs[INDEX3(b[m_g - 1], q + j, 2)];
    easting[2] = -northing[1] * speed / m_r[b[m_g - 1]] + accel *
        m_arcs[INDEX3(b[m_g - 1], q + j, 3)];
    traj->inputWithDiffs(northing, easting);
  }
  q = m_p[b[m_g - 1]] - 1;
  speed = nextSpeed;
  northing[0] = m_arcs[INDEX3(b[m_g - 1], q, 0)];
  easting[0] = m_arcs[INDEX3(b[m_g - 1], q, 1)];
  northing[1] = m_arcs[INDEX3(b[m_g - 1], q, 2)] * speed;
  easting[1] = m_arcs[INDEX3(b[m_g - 1], q, 3)] * speed;
  if(m_n[b[m_g - 1]] > 0)
  {
    northing[2] = easting[1] * speed / m_r[b[m_g - 1]];
    easting[2] = -northing[1] * speed / m_r[b[m_g - 1]];
  }
  else
  {
    northing[2] = -easting[1] * speed / m_r[b[m_g - 1]];
    easting[2] = northing[1] * speed / m_r[b[m_g - 1]];
  }
  traj->inputWithDiffs(northing, easting);
}

/*Evaluates traj through a CMap layer and returns 1 if any point in the traj
  lies in a cell with goodness below the cutoff level.  Currently is only
  defined for a layer of doubles.  Default cutoff is 0.1 m/s*/
bool reactivePlanner::isTrajObstructed(int index, CMap * map, int layer)
{
  for(int i = 0; i < m_p[index]; i++)
    if(map->getDataUTM<double>(layer, m_arcs[INDEX3(index, i, 0)],
        m_arcs[INDEX3(index, i, 1)]) <= CUTOFF)
      return 1;
  return 0;
}

/*Evaluates traj through a CMap layer and returns the time it would take to
  complete the traj if at every point in the traj the vehicle traveled at the
  speed specified by the map cell that point falls within.  Only defined for a
  layer of doubles.*/
double reactivePlanner::evalCost(int index, CMap * map, int layer)
{
  int i;
  double cost = 0;
  for(i = 0; i < m_p[index]; i++)
    cost += m_d[index] / map->getDataUTM<double>(layer,
        m_arcs[INDEX3(index, i, 0)], m_arcs[INDEX3(index, i, 1)]);
  return cost;
}
