x = state.signals.values(:,4);
y = state.signals.values(:,5);
vlon = state.signals.values(:,3);
vlat = state.signals.values(:,2);
time = state.time;
yaw = state.signals.values(:,6);

figure
quiver(y, x, sin(yaw), cos(yaw), .25, 'r')
title('easting, northing')

figure
plot(time, vlon)
title('Vlon')

figure
plot(time, vlat)
title('Vlat')