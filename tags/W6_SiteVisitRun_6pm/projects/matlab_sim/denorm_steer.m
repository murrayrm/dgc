function phicmd = denorm_steer(cmd)
% This function denormalizes the commanded steering from -1 to 1 to actual
% steering angle in rad.  Allowal is made for the steering range to be asymmetric
% about the 0 point

global phi_max phi_min

% do some tricky stuff to avoid if statements (slow)
phicmd = (sign(cmd) - 1)/2 *cmd * phi_min + (1+sign(cmd))/2 * cmd * phi_max;
