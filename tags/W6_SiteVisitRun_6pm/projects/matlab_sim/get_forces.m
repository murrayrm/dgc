function [F_lon, F_lat] = get_forces(state, throttle)
% This gives the longitudinal and lateral forces based on current state
% and commands

[thd, vlat, vlon, x, y, theta, phi] = set_states(state);

F_lon = [0 ; 0];

% get sideslip angles and calculate lateral forces
% note that both alpha and F_lat are 2x1 vectors 
alpha = wheelslip(state);
F_lat = pacejka(alpha);

f = engine(vlon, throttle);

% assume only rear wheel drive for now
%F_lon(1) = f / 2;
%F_lon(2) = f / 2;
F_lon(2) = f;
%f_lon(2) = 4000;