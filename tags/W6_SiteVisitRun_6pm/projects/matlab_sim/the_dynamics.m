function dstate = the_dynamics(state, steercmd, throttle)
% This is the big wrapper function for calculating the vehicle's dynamics
% get forces
[flon, flat] =  get_forces(state, throttle);

% denormalize the steering
steercmd = denorm_steer(steercmd);

% get planar dynamics
dstate = planar_dynamics(state, flon, flat, steercmd);