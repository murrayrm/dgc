#ifndef INDIVIDUAL_HPP
#define INDIVIDUAL_HPP

/** An individual class. This works as a superclass to be able easily implement
 * different individual classes to solve different kinds of problems with only
 * minimum coding required.
 * $Id$ 
 */
class Individual
{
public:
  /**
   * Evaluates the fitness for this individual. Must be done before calling 
   * getFitness() every time this individal has been changed (mutated or cross-over)
   */
  virtual void evaluate() = 0;

  /**
   * Returns the fitness for this individual
   * @return the fitness
   */
  virtual double getFitness() const = 0;

  /**
   * Returns a complete copy of this individual (must be deleted later)
   * @return the Individual copy 
   */
  virtual Individual* clone() = 0;

  /**
   * Mutates the properties of this individual
   */
  virtual void mutate() = 0;

  /**
   * Makes a cross-over change of the properties of this individual
   * from the current properties and properties from and another.
   * @param theOtherParent the other individual to perform the crossover with
   */
  virtual void crossOver(Individual* theOtherParent) = 0;

  /**
   * Prints the individual
   */
  virtual void print() = 0;

protected:
  /** the current fitness of this individual */
  double fitness; 
};

#endif


