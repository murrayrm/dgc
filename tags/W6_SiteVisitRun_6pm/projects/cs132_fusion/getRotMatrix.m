function result = getRotMatrix(p, h, r)
    result = [cos(p)*cos(h), -cos(r)*sin(h)+cos(h)*sin(p)*sin(r), cos(h)*cos(r)*sin(p)+sin(h)*sin(r);...
              cos(p)*sin(h), cos(h)*cos(r)+sin(h)*sin(p)*sin(r),  cos(r)*sin(h)*sin(p)-cos(h)*sin(r);...
              -sin(p),       cos(p)*sin(r),                       cos(p)*cos(r)];
