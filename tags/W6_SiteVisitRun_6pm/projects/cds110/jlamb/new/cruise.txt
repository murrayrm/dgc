Cruise Controller v. 2.0

Overview:
        Cruise control is implemented as a PID controller around a feed-forward model of the system.  The controller cruise_PID,  returns a control signal (scaled from -1 to 1) based on the commanded velocity, the current velocity, the velocity of the vehicle at the last time cruise was called, and the time elapsed since the function was last called.  Commanded velocity is passed as a double, and if the VehStateGetMsg struct vehstate is defined, no more arguments need to be passed, and cruise will read the value for velocity directly.  Alternately, cruise can be called with three arguments: commanded velocity, actual velocity and the current acceleration (used for backward compatability with cruise 1.0.) Elapsed time is stored internally, making use of the timeval struct and the gettimeofday function.
	cruise_optimised modifies the PID loop such that positive and negative errors can have different constants inside of the integration step, so that the integrator will accumulate error at different rates depending if the vehicle must accelerate or decelerate.
        Cruise v1.0 is still available, and cruise changes functionality if CRUISE_OLD is defined.  If CRUISE_OLD is defined, the original cruise controller, which utilizes lookup tables and a PD controller will be used.  If it is not defined, a PID controller is used.
        Cruise 2.0 includes a function to wrap another loop around the cruise controller, which models the bob/terrain plant as a system that takes commanded velocity as an input, with pitch, yaw, and roll rates as outputs.  An error signal (acceptable rates - actual rates), and corresponding limit on velocity is generated using cruise_terrain.  This value can be used to limit the commanded velocity, inside the cruise function, before cruise_PID or cruise_optimised are called.  Currently, there is no interface or filter between raw yaw, pitch, and roll rates, so cruise terrain will not limit the commanded velocity.  Pitch, yaw, and roll rates are available from the vehstate struct, and will need to be filtered before being passed through cruise_terrain.  It has not been determined if the system will be stable with this additional feedback loop, and should be modeled and used with care.  (It's meant to be used on rough terrain, so it will need to be tested on rough terrain at some point.  Don't flip Bob!)  A simulink model of the engine dynamics of bob is available at: team/models/modify_current_throttle.mdl.  A model for terrain and the new cruise controller must still be created.


Defines:
	cruise.h:	CRUISE_OLD	causes cruise to compile with the old cruise controller
	cruise.h:	CRUISE_DIFF_INTS	enables use of different constants in integrator for positive and negative errors.
	cruise.h:	CRUISE_OPTIMIZED	enables some optimizations, includes:  integrator reset after long pause, commanded vel = 0 causes cruise to stop as fast as possible.


Functions:
	cruise(commanded_vel)	wrapper for cruise controller, attempts to keep vehicle at commanded vel in m/s.  gets value for actual speed from the vehstate structure.
	cruise(commanded_vel, act_vel, act_acc)	Similar, only actual speed is passed in.  act_acc is ignored in cruise_new.
	cruise_terrain()	returns maximum safe velocity based on current terrain conditions.
	cruise_PID(vel_command, vel_actual) PID calculations and lookup carried out here.
	cruise_optimised	a PID controller with a few tweaks.  see code for differences.

Tables:
	v_to_a_lookup	used in cruise_old
	v_to_t_lookup	feed-forward throttle position based on steady state
	a_to_t_lookup	change in throttle to produce the desired acceleration used in CRUISE_OLD
	ctr_to_t_PID	gain from control signal to throttle position (to achieve more linear scale)
	ctr_to_t_lookup	same as above, but, used in CRUISE_OLD (different gain parameters, so a different table is required)

External Data:
	struct VState_GetStateMsg vehstate	contains all information pertaining to the state of the vehicle.

Constants:
	vel_cruise_max	maximum velocity that the cruise controller will ever attempt to command

	g_vel_p		gain for proportional term of velocity error

	velocity_timeout	number of seconds after which cruise resets the integral and derivative terms, after not being called.
	g_vel_d		gain for derivative term
	g_vel_i		gain for integral term
	g_vel_i_pos	if CRUISE_DIFF_INTS is defined, use this multiplier when calculating the integral when error is positive
	g_vel_i_neg	...if the error is negative
	integrator_sat	saturation for integral term in PID controller

	G_roll_p	proportional gain on error in terrain roughness
	G_roll_i	integral...
	roll_harsh_thresh  amount of terrain "harshness" (as determined by some yet to be designed filter) the vehicle can tolerate.

	G_pitch_p	proportional gain on error in terrain roughness
	G_pitch_i	integral...
	roll_harsh_thresh  amount of terrain "harshness" (as determined by some yet to be designed filter) the vehicle can tolerate.

	throttle_maximum	maximum throttle value to command
	throttle_minimum	minimum throttle value to command




Future work:
	1. Need to decide if terrain will be handled by planning, or if terrain loop should be built into cruise function
	1a. If cruise is the place, need to decide on limits of the vehicle, and how to maintain stability.

	2. Make sure that PID loop has accurate feed forward data.  Also, make sure that control to t is reasonable.  The feed forward data should be easy to take:  manually take the truck to a stable speed, wait, go faster, wait, go faster, wait, until enough data is collected to determine the steady state speed at any given throttle position.  For ctr_to_t, the process is more difficult.  The goal of our controller is to produce and acceleration equal to some gain times the value of the PID control value.  So, we need to find throttle based on acceleration.  One possibility would be to work backwards:  for example, get the vehicle to 20 m/s.  set throttle to .5, record acceleration vs. velocity.  set repeat, only, set throttle to .4, repeat at .3, .2. .1 0, -.1... -1.  Working back, a table can be constructed of throttle vs. dv/dt vs velocity.  A major drawback of the current system is that acceleration is not linear w.r.t. velocity, and so as speed increaces, the control signal assumes a more negative value (acceleration is not as positive as it should be because of drag and engine dynamics).  So, another advance would be to include a two dimensional array of values with inputs of desired acceleration and current velocity, with output of desired throttle position.  