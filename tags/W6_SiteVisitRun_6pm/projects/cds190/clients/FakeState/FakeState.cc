/* FakeState.cc - new vehicle state estimator
 *
 * JML 31 Dec 04
 *
 * */

#include "FakeState.hh"

FakeState::FakeState(int skynet_key, int k_switch = 1, int imu_switch = 1, int gps_switch = 1, 
		int mag_switch = 1, int verbose_switch = -1)
	: m_skynet(SNastate, skynet_key)
{
	//FInd startting time;
	DGCgettime(starttime);

	//Create mutexes
	DGCcreateMutex(&m_VehicleStateMutex);
	DGCcreateMutex(&m_UpdateMutex);

	lastIndex = 0;

	//Initialize structs apropriately:
	
	//	DGCgettime(vehiclestate.Timestamp);
	vehiclestate.Northing = 0;
	vehiclestate.Easting = 0;
	vehiclestate.Altitude = 0;
	vehiclestate.Vel_N = 0;
	vehiclestate.Vel_E = 0;
	vehiclestate.Vel_D = 0;
	vehiclestate.Acc_N = 0;
	vehiclestate.Acc_E = 0;
	vehiclestate.Acc_D = 0;
	vehiclestate.Roll = 0;
	vehiclestate.Pitch = 0;
	vehiclestate.Yaw = 0;
	vehiclestate.RollAcc = 0;
	vehiclestate.PitchAcc = 0;
	vehiclestate.YawAcc = 0;
	vehiclestate.Timestamp = 0;

	broadcast_statesock = m_skynet.get_send_sock(SNstate);
	if (broadcast_statesock < 0)
		cerr << "FakeState::FakeState(): get_send_sock returned error" << endl;
}

void FakeState::Broadcast()
{
	if(m_skynet.send_msg(broadcast_statesock,
				&vehiclestate,
                                sizeof(vehiclestate),
                                0,
                                &m_VehicleStateMutex)
                        != sizeof(vehiclestate))
           {
                cerr << "FakeState::Broadcast(): didn't send right size state message" << endl;
	   }
}

void FakeState::printVehicleState()
{
	cout << "Vehicle State:" << endl;
}

// VehicleState messaging thread:
void FakeState::VehicleStateMsg_thread()
{
	//Get sockets for receiving/sending data:
	int getstatesock = m_skynet.listen(SNreqstate, ALLMODULES);
	if(getstatesock < 0)
		cerr << "FakeState::VehicleStateMsg_thread(): skynet listen returned error" << endl;

	int statesock = m_skynet.get_send_sock(SNstate);
	if(statesock < 0)
		cerr << "FakeState::VehicleStateMsg_thread(): skynet get_send_sock returned error" << endl;

	char getstatedummy;

	//Loop listening for messages and then sending:
	while(true)
	{
		cout << "Starting to listen for reqstate" << endl;
		if(m_skynet.get_msg(getstatesock,
					&getstatedummy, 
					sizeof(getstatedummy), 0)
				!= sizeof(getstatedummy))
		{
			cerr << "FakeState::VehicleStateMsg_thread(): didn't received right size getstate command" << endl;
			continue;
		}
		cout << "Starting to broadcast reqstate" << endl;
		if(m_skynet.send_msg(statesock,
					&vehiclestate, 
					sizeof(vehiclestate), 
					0, 
					&m_VehicleStateMutex) 
				!= sizeof(vehiclestate))
		{
			cerr << "FakeState::VehicleStateMsg_thread(): didn't send right size state message" << endl;
		}
	}
}
