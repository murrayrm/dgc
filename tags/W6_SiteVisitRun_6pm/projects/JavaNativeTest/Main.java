/*
 * Created on Oct 11, 2004
 * $Id$
 */

import javax.swing.JFrame;

/**
 * This is a test program for creating a CMap and using native methods (C++) 
 * to modify it, then using Java for displaying it!
 * It does:
 * - Creates an image (that could represent the map as a byte array)
 * - Enters native code and creates a CMap object and initalizes it
 * - Reads the byte array from Java into the native C++ code and alters it
 *  
 * @author Henrik Kjellander (henrik.kjellander@home.se)
 */
public class Main {
	public static void main(String[] args) {
		/** main just creates and shows a CMapCanvas JFrame */
		CMapTest f = new CMapTest();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.pack();
		f.setVisible(true);
		
		// run the test
		byte bytes[] = f.getCMap();
		System.out.println("bytes length: " + bytes.length);
		f.test(bytes);
		

		/*
		// ByteArray test just to see that it's actually working to
		// access arrays from the native C++ code
		ByteArray p = new ByteArray();
	    byte arr[] = new byte [10];
	    for (byte i = 0; i < 10; i++)
	      arr[i] = i;
	    int sum = p.sumArray(arr);
	    System.out.println("sum = " + sum);
		*/
		
        }
}
