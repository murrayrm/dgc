#include <sn_msg.hh>
#include <sys/time.h>
#include <iostream>

#define us_p_s  1000000 // 4294967296

using namespace std;
int main(int argc, char** argv)
{
  int key; 
  unsigned int msg_size;
  //double msg_rate;  //Bytes per second
  int testlen;
  if (argc < 4)
  {
    cerr << "This program should only be called from snm-main.sh" << endl;
    cerr << "usage: single-latency-master key msg_size time" << endl;
    cerr << "key is the skynet key, ";
    cerr << "msg_size is in bytes, ";
    cerr << "rate is bytes per second, time is test time in seconds" << endl;
      
    exit(2);
  }
  else
  {
    key = atoi(argv[1]);
    msg_size = atoi(argv[2]);
    testlen = atoi(argv[3]); 
    //cout << key << " " << msg_size << " " << msg_rate << " " << testlen << endl ;
  }
  long long int starttime, mytime;
  SNgettime(&starttime);
  skynet Skynet(MODmark, key);
  int send_chan = Skynet.get_send_sock(SNmark1);
  int recv_chan = Skynet.listen(SNmark2, ALLMODULES);
  char buffer[msg_size];
  buffer[0] = 0;                            //continue character
  long long int newmytime;
  long long int echotime;
  
  double sum, sumsq, mean, stddev, max;
  sum = 0;
  sumsq = 0;
  max = 0;
  
  unsigned int n = 0;
  do
  {
    SNgettime(&mytime);
    Skynet.send_msg(send_chan, buffer, msg_size, 0); //send ping
    Skynet.get_msg(recv_chan, buffer, msg_size, 0);  //get echo
    SNgettime(&newmytime);
    echotime = newmytime - mytime;              //latency
    
    sum += echotime; 
    sumsq += echotime*echotime; 
    max = (echotime > max ? echotime  : max );
    
    n++;
  } while(newmytime - starttime < testlen * 1000000);
  mean = sum/n;
  stddev = sqrt((sumsq - n * mean*mean) / (n - 1));       //divide by n-1 gives unbiased estimator
  double totaltime = (double)(newmytime - starttime)/1000000;
  //now kill the slave.  Send multiple kill signals
  buffer[0] = 1;                            //kill character
  int m;
  for(m=0; m<=25; m++)
  {
    usleep(1000);                             //wait a while
    Skynet.send_msg(send_chan, buffer, 1, 0); //send 1 char
  }
  //cerr << "messages= " << n << "\ttime= " << totaltime ;
  double real_rate = (double)(n * msg_size) / totaltime;
  //cerr << "\trate= " << real_rate << endl;
  cout << n << " " << real_rate << " " << totaltime << " " << mean << " " << stddev << " " << max << endl; 
  return 0;
}
