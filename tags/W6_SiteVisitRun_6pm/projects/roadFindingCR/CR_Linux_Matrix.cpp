//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_Linux_Matrix.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// General math and miscellaneous
//----------------------------------------------------------------------------

void convert_CR_Vector_array_to_CR_Matrix(CR_Vector **arr, CR_Matrix *M)
{
  int r, c;

  for (r = 0; r < M->rows; r++)
    for (c = 0; c < M->cols; c++)
      MATRC(M, r, c) = arr[r]->x[c];
}

//----------------------------------------------------------------------------

void colsum_CR_Matrix(CR_Matrix *M, CR_Vector *V)
{
  int r, c;

  for (c = 0; c < V->rows; c++)
    V->x[c] = 0.0;

  for (c = 0; c < V->rows; c++)
    for (r = 0; r < M->rows; r++)
      V->x[c] += MATRC(M, r, c);
}

//----------------------------------------------------------------------------

// calculate mean and covariance of n d-dimensional samples, where each 
// sample is a row of M

void mean_cov_CR_Matrix(CR_Matrix *M, CR_Matrix *MT, CR_Vector *mean, CR_Matrix *C)
{
  double n, d;
  int r, c;

  d = M->cols;
  n = M->rows;

  // compute mean & subtract it off

  colsum_CR_Matrix(M, mean);
  scale_CR_Vector(1 / n, mean);

  for (r = 0; r < M->rows; r++)
    for (c = 0; c < M->cols; c++)
      MATRC(M, r, c) = MATRC(M, r, c) - mean->x[c];

  // form product

  transpose_CR_Matrix(M, MT);
  product_CR_Matrix(MT, M, C);

  //  print_CR_Matrix(C);
  //  exit(1);
}

//----------------------------------------------------------------------------

// value, row, and column of largest element in matrix M

void max_CR_Matrix(CR_Matrix *M, double *maxval, int *max_r, int *max_c)
{
  int r, c;

  *max_r = 0;
  *max_c = 0;
  *maxval = MATRC(M, 0, 0);
  for (r = 0; r < M->rows; r++)
    for (c = 0; c < M->cols; c++)
      if (MATRC(M, r, c) > *maxval) {
	*maxval = MATRC(M, r, c);
	*max_r = r;
	*max_c = c;
      }
}

//----------------------------------------------------------------------------

// value, row, and column of smallest element in matrix M

void min_CR_Matrix(CR_Matrix *M, double *minval, int *min_r, int *min_c)
{
  int r, c;

  *min_r = 0;
  *min_c = 0;
  *minval = MATRC(M, 0, 0);
  for (r = 0; r < M->rows; r++)
    for (c = 0; c < M->cols; c++)
      if (MATRC(M, r, c) < *minval) {
	*minval = MATRC(M, r, c);
	*min_r = r;
	*min_c = c;
      }
}

//----------------------------------------------------------------------------

double log2(double x)
{
  return INV_LN2 * log(x);
}

//----------------------------------------------------------------------------

// popular transfer function used by Matlab neural networks -- just the hyperbolic tangent function

double tansig(double x)
{
  return -1 + 2 / (1 + exp(-2 * x));
}

//----------------------------------------------------------------------------

int count_lines(FILE *fp)
{
  char c;
  int lines;

  //  rewind(fp);
  lines = 0;
  
  while ((c = fgetc(fp)) != EOF)
    if (c == '\n')
      lines++;
  rewind(fp);
  //  lines++;

  return lines;
}

//----------------------------------------------------------------------------

CR_Matrix **make_CR_Matrix_array(int n)
{
  return (CR_Matrix **) CR_calloc(n, sizeof(CR_Matrix *));
}

//----------------------------------------------------------------------------

void free_CR_Matrix_array(CR_Matrix **M_array, int n)
{
  int i;

  for (i = 0; i < n; i++)
    free_CR_Matrix(M_array[i]);

  CR_free_calloc(M_array, n, sizeof(CR_Matrix *));
}

//----------------------------------------------------------------------------

// Sum of matrices in M_array; assume n >= 1, all matrices are same size

CR_Matrix *sum_CR_Matrix_array(CR_Matrix **M_array, int n)
{
  CR_error("1 removed on Linux -- no MKL");

  /*
  CR_Matrix *Sum;
  int i;

  Sum = copy_CR_Matrix(M_array[0]);

  for (i = 1; i < n; i++)
    cblas_daxpy(Sum->rows * Sum->cols, 1, M_array[i]->x, 1, Sum->x, 1);

  return Sum;
  */
}

//----------------------------------------------------------------------------

// octave_interval should be > 0

CR_Matrix **make_gabor_bank_CR_Matrix_array(double start_wavelength, double octave_interval, int num_wavelengths,
						   double start_orientation, int num_orientations, int *n, int size)
{
  CR_Matrix **gabor_bank;
  int i, lambda_num;
  double lambda, orientation, even, octave_coeff;

  octave_coeff = pow((double) 2, octave_interval); 
  *n = num_orientations * num_wavelengths * 2;
  gabor_bank = make_CR_Matrix_array(*n);

  for (lambda = start_wavelength, lambda_num = 0, i = 0; lambda_num < num_wavelengths; lambda *= octave_coeff, lambda_num++) 
    for (orientation = start_orientation; orientation < start_orientation + 360.0; orientation += 360 / (double) num_orientations) 
      for (even = 0.0; even <= 1.0; even += 1.0) {
	gabor_bank[i] = make_gabor_kernel_CR_Matrix(lambda, orientation, even, size);
	printf("%i: wavelength = %.2lf (%i), orientation = %.2lf, even = %.0lf (%i x %i)\n", i, lambda, lambda_num, orientation, even, gabor_bank[i]->rows, gabor_bank[i]->rows);
	i++;
      }
    
  return gabor_bank;
}

//----------------------------------------------------------------------------

/*
  union
  {
    float f;
    unsigned char b[4];
  } dat1, dat2;
  float littlef;

  dat1.f = bigf;
  
  dat2.b[0] = dat1.b[3];
  dat2.b[1] = dat1.b[2];
  dat2.b[2] = dat1.b[1];
  dat2.b[3] = dat1.b[0];
    
  littlef = dat2.f;

  return littlef;     
*/

void write_pfm_CR_Matrix(char *filename, CR_Matrix *M)
{
  int i, r, c;
  FILE *fp;
  float *f;
  union
  {
    float f;
    unsigned char b[4];
  } intensity;
  //  float intensity;
//   float sign;       // bit  1
//   float exponent;   // bits 2-9 (next 8)
//   float mantissa;   // bits 10-32 (next 23)
//   float whole_component, fractional_component;


//   f = (float *) calloc(M->rows * M->cols, sizeof(float));
//   for (r = 0, i = 0; r < M->rows; r++) 
//     for (c = 0; c < M->cols; c++) 
//       f[i++] = (float) MATRC(M, r, c); 

  fp = fopen(filename, "w");
// 
  fprintf(fp, "PF\n%d %d\n1\n", M->cols, M->rows);
//   fclose(fp);

  // fp = fopen(filename, "wb");

  //  fwrite(f, sizeof(float), M->rows * M->cols, fp);

  for (r = 0; r < M->rows; r++) 
    for (c = 0; c < M->cols; c++) {
      intensity.f = (float) MATRC(M, r, c); 

      fprintf(fp, "%c%c%c%c%c%c%c%c%c%c%c%c", 
	      intensity.b[0], intensity.b[1], intensity.b[2], intensity.b[3],
	      intensity.b[0], intensity.b[1], intensity.b[2], intensity.b[3],
	      intensity.b[0], intensity.b[1], intensity.b[2], intensity.b[3]);

//       fprintf(fp, "%b%b%b%b%b%b%b%b%b%b%b%b", 
// 	      intensity.b[3], intensity.b[2], intensity.b[1], intensity.b[0],
// 	      intensity.b[3], intensity.b[2], intensity.b[1], intensity.b[0],
// 	      intensity.b[3], intensity.b[2], intensity.b[1], intensity.b[0]);

//       if (intensity > 0)
// 	sign = 0;
//       else
// 	sign = 1;
//       whole_component = floor(intensity);
//       fractional_component = intensity - whole_component;

//      fprintf(fp, "%f%f%f", intensity, intensity, intensity);
    }
  
  fclose(fp); 
}

//----------------------------------------------------------------------------

// octave_interval should be > 0

// info columns are wavelength, orientation, phase, size 

CR_Matrix **make_gabor_bank_CR_Matrix_array(double start_wavelength, double octave_interval, int num_wavelengths,
						   double start_orientation, double orientation_interval, int num_orientations, 
						   CR_Matrix *info)
{
  CR_Matrix **gabor_bank;
  int i, lambda_num, orientation_num;
  double lambda, orientation, even, octave_coeff;

  octave_coeff = pow((double) 2, octave_interval); 
  gabor_bank = make_CR_Matrix_array(info->rows);

  for (lambda = start_wavelength, lambda_num = 0, i = 0; lambda_num < num_wavelengths; lambda *= octave_coeff, lambda_num++) 
    for (orientation = start_orientation, orientation_num = 0; orientation_num < num_orientations; orientation += orientation_interval, orientation_num++) 
      for (even = 0.0; even <= 1.0; even += 1.0) {
	gabor_bank[i] = make_gabor_kernel_CR_Matrix(lambda, orientation, even);   // xxx
	// GPU convolution debugging
	//	print_CR_Matrix(gabor_bank[i]);
	// 	printf("lambda = %lf, orientation = %lf, even = %lf\n", lambda, orientation, even);
// 	exit(1);
// 	//	gabor_bank[i] = make_gabor_kernel_CR_Matrix(lambda, orientation, even, 240);   // xxx
	MATRC(info, i, 0) = lambda;
	MATRC(info, i, 1) = orientation;
	MATRC(info, i, 2) = even;
	MATRC(info, i, 3) = gabor_bank[i]->rows;
	//	printf("%i: wavelength = %.2lf (%i), orientation = %.2lf, even = %.0lf (%i x %i)\n", i, lambda, lambda_num, orientation, even, gabor_bank[i]->rows, gabor_bank[i]->rows);
	i++;
      }
    
  return gabor_bank;
}

//----------------------------------------------------------------------------
// xxx this is the one!!!!
//----------------------------------------------------------------------------

// octave_interval should be > 0

// info columns are wavelength, orientation, phase, size 

CR_Matrix **make_gabor_bank_CR_Matrix_array(double start_wavelength, double octave_interval, int num_wavelengths,
						   double start_orientation, double orientation_interval, int num_orientations, 
						   CR_Matrix *info,
						   int rows, int cols)
{
  CR_Matrix **gabor_bank;
  int i, lambda_num, orientation_num;
  double lambda, orientation, even, octave_coeff;

  octave_coeff = pow((double) 2, octave_interval); 
  gabor_bank = make_CR_Matrix_array(info->rows);

  for (lambda = start_wavelength, lambda_num = 0, i = 0; lambda_num < num_wavelengths; lambda *= octave_coeff, lambda_num++) 
    for (orientation = start_orientation, orientation_num = 0; orientation_num < num_orientations; orientation += orientation_interval, orientation_num++) 
      for (even = 0.0; even <= 1.0; even += 1.0) {
	gabor_bank[i] = make_gabor_kernel_CR_Matrix(lambda, orientation, even, rows, cols);   // xxx
	//	gabor_bank[i] = make_gabor_kernel_CR_Matrix(lambda, orientation, even, 240);   // xxx
	MATRC(info, i, 0) = lambda;
	MATRC(info, i, 1) = orientation;
	MATRC(info, i, 2) = even;
	MATRC(info, i, 3) = gabor_bank[i]->rows;
	printf("%i: wavelength = %.2lf (%i), orientation = %.2lf, even = %.0lf (%i x %i)\n", i, lambda, lambda_num, orientation, even, gabor_bank[i]->rows, gabor_bank[i]->cols);
	i++;
      }
    
  return gabor_bank;
}

//----------------------------------------------------------------------------

// octave_interval should be > 0

CR_Matrix **make_gabor_bank_CR_Matrix_array(double start_wavelength, double octave_interval, int num_wavelengths,
						   double start_orientation, int num_orientations, int *n)
{
  CR_Matrix **gabor_bank;
  int i, lambda_num;
  double lambda, orientation, even, octave_coeff;

  octave_coeff = pow((double) 2, octave_interval); 
  *n = num_orientations * num_wavelengths * 2;
  gabor_bank = make_CR_Matrix_array(*n);

  for (lambda = start_wavelength, lambda_num = 0, i = 0; lambda_num < num_wavelengths; lambda *= octave_coeff, lambda_num++) 
    for (orientation = start_orientation; orientation < start_orientation + 360.0; orientation += 360 / (double) num_orientations) 
      for (even = 0.0; even <= 1.0; even += 1.0) {
	gabor_bank[i] = make_gabor_kernel_CR_Matrix(lambda, orientation, even);
	printf("%i: wavelength = %.2lf (%i), orientation = %.2lf, even = %.0lf (%i x %i)\n", i, lambda, lambda_num, orientation, even, gabor_bank[i]->rows, gabor_bank[i]->rows);
	i++;
      }
    
  return gabor_bank;
}

//----------------------------------------------------------------------------

// generates a gabor wavelet of lambda (wavelength), orientation (degrees)
// and either even or odd symmetric in phase (even = 1 or 0).

// More details in T. S. Lee, "Image Representation Using 2-D Gabor Wavelets",
// PAMI, Vol. 18, No. 10, October, 1996

// selects a good size for the wavelength indicated

CR_Matrix *make_gabor_kernel_CR_Matrix(double lambda, double orientation, double even)
{
  int size;

  size = (int) (15.0 * lambda / (1.5 * PI));

  return make_gabor_kernel_CR_Matrix(lambda, orientation, even, size);
}

//----------------------------------------------------------------------------

// generates a gabor wavelet of lambda (wavelength), orientation (degrees)
// and either even or odd symmetric in phase (even = 1 or 0).

// More details in T. S. Lee, "Image Representation Using 2-D Gabor Wavelets",
// PAMI, Vol. 18, No. 10, October, 1996

CR_Matrix *make_gabor_kernel_CR_Matrix(double lambda, double orientation, double even, int size)
{
  int i, j;
  double c, sigma, f, k, u, v, theta, ai, aj, norm_f, sum, mean;
  CR_Matrix *gabor_f, *gaussian, *filt_ga;

  //  printf("size = %i\n", size);
  sigma = (double) size / 9;   // spread of Gaussian

  f = (2 * PI) / lambda;
  theta = DEG2RAD(orientation);

  u = f * cos(theta);
  v = f * sin(theta);
  c = (double) size / 2;
  
  gabor_f = make_all_CR_Matrix("gabor_f", size, size, 1.0);
  gaussian = make_all_CR_Matrix("gaussian", size, size, 0.0);
  filt_ga = make_CR_Matrix("filt_ga", size, size);

  for (i = 1, sum = 0; i <= size; i++) 
    for (j = 1; j <= size; j++) { 
      ai = (i - c) * cos(theta) + (j - c) * sin(theta); 
      aj = (i - c) * -sin(theta) + (j - c) * cos(theta); 
      MATRC(gaussian, i - 1, j - 1) = exp(-(1 / (8 * sigma * sigma)) * (4 * ai * ai + aj * aj)); 
      MATRC(gabor_f, i - 1, j - 1) = MATRC(gaussian, i - 1, j - 1) * sin(u * (i - c) + v * (j - c) + (even * PI_2));
      sum += MATRC(gabor_f, i - 1, j - 1);
    }

  //  print_CR_Matrix(gaussian);

  // remove dc_component  

  mean = sum / (double) (size * size);
  for (i = 0; i < size; i++) 
    for (j = 0; j < size; j++)  
      MATRC(gabor_f, i, j) = MATRC(gabor_f, i, j) - mean;

  // L2 normalized
  norm_f = 0;
  for (i = 0; i < size; i++) 
    for (j = 0; j < size; j++)  
      norm_f += MATRC(gabor_f, i, j) * MATRC(gabor_f, i, j);
  norm_f = sqrt(norm_f);

  for (i = 0; i < size; i++) 
    for (j = 0; j < size; j++)  
      MATRC(filt_ga, i, j) = MATRC(gabor_f, i, j) / norm_f; 
 
  // clean up

  free_CR_Matrix(gabor_f);
  free_CR_Matrix(gaussian);

  // finish

  //  print_CR_Matrix(filt_ga);

  return filt_ga;

// % this is just for showing your gabor filter. 
// % you should comment them out when you call this function. 
// I = uint8(255*((gabor_f - min(min(gabor_f)))/(max(max(gabor_f))- ... 
// 					      min(min(gabor_f))))); 
// imshow(I) 
}

//----------------------------------------------------------------------------

// makes custom-sized non-square kernel (generally for FFT)

// generates a gabor wavelet of lambda (wavelength), orientation (degrees)
// and either even or odd symmetric in phase (even = 1 or 0).

// More details in T. S. Lee, "Image Representation Using 2-D Gabor Wavelets",
// PAMI, Vol. 18, No. 10, October, 1996

CR_Matrix *make_gabor_kernel_CR_Matrix(double lambda, double orientation, double even, int rows, int cols)
{
  int i, j, size, row, col, center_row, center_col, myr, myc, rr, cc;
  double r, c, sigma, f, k, u, v, theta, ai, aj, norm_f, sum, mean;
  CR_Matrix *gabor_f, *gaussian, *filt_ga, *filt_ga_temp;
  
  size = (int) (15.0 * lambda / (1.5 * PI));

  filt_ga_temp = make_gabor_kernel_CR_Matrix(lambda, orientation, even, size);
  filt_ga = make_CR_Matrix("filt_ga", rows, cols);
  for (row = 0; row < filt_ga->rows; row++)
    for (col = 0; col < filt_ga->cols; col++)
      MATRC(filt_ga, row, col) = 0.0;

  // now copy filt_ga_temp into the center of filt_ga

  center_row = rows / 2;
  center_col = cols / 2;
  
  rr = filt_ga_temp->rows;
  cc = filt_ga_temp->cols;

  /*
  for (row = 0; row < filt_ga_temp->rows; row++)
    for (col = 0; col < filt_ga_temp->cols; col++)
      MATRC(filt_ga, center_row - size / 2 + row, center_col - size / 2 + col) = MATRC(filt_ga_temp, row, col);
  */
  for (row = -rr/2; row < rr/2; row++)
    for (col = -cc/2; col < cc/2; col++) {
      myr = row % rows;
      if (myr < 0)
	myr += rows;
      myc = col % cols;
      if (myc < 0)
	myc += cols;
      MATRC(filt_ga, myr, myc) = MATRC(filt_ga_temp, row + rr/2, col + cc/2);
    }


  free_CR_Matrix(filt_ga_temp);

  /*
  //  printf("size = %i\n", size);
  sigma = (double) size / 9;   // spread of Gaussian

  f = (2 * PI) / lambda;
  theta = DEG2RAD(orientation);

  u = f * cos(theta);
  v = f * sin(theta);
  r = (double) rows / 2;
  c = (double) cols / 2;
  
  gabor_f = make_all_CR_Matrix("gabor_f", rows, cols, 1.0);
  gaussian = make_all_CR_Matrix("gaussian", rows, cols, 0.0);
  filt_ga = make_CR_Matrix("filt_ga", rows, cols);

  for (i = 1, sum = 0; i <= rows; i++) 
    for (j = 1; j <= cols; j++) { 
      ai = (i - r) * cos(theta) + (j - c) * sin(theta); 
      aj = (i - r) * -sin(theta) + (j - c) * cos(theta); 
      MATRC(gaussian, i - 1, j - 1) = exp(-(1 / (8 * sigma * sigma)) * (4 * ai * ai + aj * aj)); 
      MATRC(gabor_f, i - 1, j - 1) = MATRC(gaussian, i - 1, j - 1) * sin(u * (i - r) + v * (j - c) + (even * PI_2));
      sum += MATRC(gabor_f, i - 1, j - 1);
    }

  //  print_CR_Matrix(gaussian);

  // remove dc_component  

  mean = sum / (double) (rows * cols);
  for (i = 0; i < rows; i++) 
    for (j = 0; j < cols; j++)  
      MATRC(gabor_f, i, j) = MATRC(gabor_f, i, j) - mean;

  // L2 normalized
  norm_f = 0;
  for (i = 0; i < rows; i++) 
    for (j = 0; j < cols; j++)  
      norm_f += MATRC(gabor_f, i, j) * MATRC(gabor_f, i, j);
  norm_f = sqrt(norm_f);

  for (i = 0; i < rows; i++) 
    for (j = 0; j < cols; j++)  
      MATRC(filt_ga, i, j) = MATRC(gabor_f, i, j) / norm_f; 
 
  // clean up

  free_CR_Matrix(gabor_f);
  free_CR_Matrix(gaussian);
  */

  // finish

  //  print_CR_Matrix(filt_ga);

  return filt_ga;

// % this is just for showing your gabor filter. 
// % you should comment them out when you call this function. 
// I = uint8(255*((gabor_f - min(min(gabor_f)))/(max(max(gabor_f))- ... 
// 					      min(min(gabor_f))))); 
// imshow(I) 
}

//----------------------------------------------------------------------------

// assuming A consists of 1's and 0's; fills B with 0's where
// there are 0's in A and labels of connected components where
// there are 1's in A (1, 2, 3, etc.)

// if fourconn is TRUE, uses four-connectedness criterion; else
// uses eight-connectedness

// returns number of components

int connected_components_CR_Matrix(int fourconn, CR_Matrix *A, CR_Matrix *B)
{
  int label, x, y, l1, l2, n, m, num_ccs;
  CR_Vector *equiv, *newequiv;
  double current_cc;

  equiv = make_CR_Vector("equiv", A->rows * A->cols);
  label = 0;

  // clean out B

  set_all_CR_Matrix(0.0, B);

  // label pixel in upper-left corner

  if (MATRC(A, 0, 0)) {
    label++;
    MATRC(B, 0, 0) = label;
    equiv->x[label] = label;
    //    printf("[%i] = %i (upper left)\n", label, label);
  }

  // label pixels in the first row

  for (x = 1; x < A->cols; x++) {
    if (MATRC(A, 0, x)) {
      if (MATRC(A, 0, x) == MATRC(A, 0, x - 1))
	MATRC(B, 0, x) = MATRC(B, 0, x - 1);
      else {
	label++;
	MATRC(B, 0, x) = label;
	equiv->x[label] = label;
	//	printf("[%i] = %i (first row)\n", label, label);
      }
    }
  }

  // label remaining rows

  for (y = 1; y < A->rows; y++) {

    // label first pixel in row

    if (MATRC(A, y, 0) == MATRC(A, y - 1, 0))
      MATRC(B, y, 0) = MATRC(B, y - 1, 0);
    if (MATRC(A, y, 0) && MATRC(A, y, 0) != MATRC(A, y - 1, 0)) {
      label++;
      MATRC(B, y, 0) = label;
      equiv->x[label] = label;
      //      printf("[%i] = %i (first column)\n", label, label);
    }

    // label rest of columns

    for (x = 1; x < A->cols; x++) {
      if (MATRC(A, y, x)) {
	if (MATRC(A, y, x) == MATRC(A, y, x - 1) && MATRC(A, y, x) != MATRC(A, y - 1, x))
	  MATRC(B, y, x) = MATRC(B, y, x - 1);
	if (MATRC(A, y, x) != MATRC(A, y, x - 1) && MATRC(A, y, x) == MATRC(A, y - 1, x))
	  MATRC(B, y, x) = MATRC(B, y - 1, x);
	if (MATRC(A, y, x) == MATRC(A, y, x - 1) && MATRC(A, y, x) == MATRC(A, y - 1, x)) {
	  if (MATRC(B, y, x - 1) == MATRC(B, y - 1, x))
	    MATRC(B, y, x) = MATRC(B, y, x - 1);
	  // new equivalence of classes
	  else {
	    l1 = (int) MIN2(MATRC(B, y, x - 1), MATRC(B, y - 1, x));   // find the smaller class label
	    l2 = (int) MAX2(MATRC(B, y, x - 1), MATRC(B, y - 1, x));   // find the larger class label
	    MATRC(B, y, x) = l1;        // set the current pixel class to the smaller
	    equiv->x[l2] = l1;          // note the equivalency in the array
	    //	    printf("[%i] = %i (comparison)\n", l2, l1);
	  }
	}
	if (MATRC(A, y, x) != MATRC(A, y, x - 1) && MATRC(A, y, x) != MATRC(A, y - 1, x)) {
	  label++;
	  MATRC(B, y, x) = label;
	  equiv->x[label] = label;
	  //	  printf("[%i] = %i (interior)\n", label, label);
	}
      }
    }
  }

//   printf("\n");
//   printf("before\n");
//   for (m = 0; m <= label; m++)
//     printf("%lf\n", equiv->x[m]);
//   printf("\n");

  // first pass through image is done.  now resolve class equivalencies

  for (m = label; m >= 2; m--) {
    n = m;
    while (1) { 
      if (equiv->x[n] < n)
	n = (int) equiv->x[n];
      else 
	break;
    }
    equiv->x[m] = n;
  }

//   printf("after\n");
//   for (m = 0; m <= label; m++)
//     printf("%lf\n", equiv->x[m]);
//   printf("\n");

//   for (m = 1, num_ccs = label; m < label; m++) 
//     for (n = m + 1; n <= label; n++)
//       if (equiv->x[m] == equiv->x[n])
// 	num_ccs--;

  newequiv = make_all_CR_Vector("newequiv", label + 1, 0.0);
  for (m = 1; m <= label; m++) 
    newequiv->x[(int) equiv->x[m]] = 1.0;
  num_ccs = (int) sum_of_terms_CR_Vector(newequiv);

  for (m = 2; m <= label; m++) {
    for (y = 0; y < A->rows; y++)
      for (x = 0; x < A->cols; x++)
	if (MATRC(B, y, x) == (double) m)
	  MATRC(B, y, x) = equiv->x[m];
  }

  // post-process so that labels run from 1 to num_ccs with
  // biggest area first, then next biggest, and so on
  
  //  print_CR_Matrix(B);

  for (m = 1, current_cc = 1.0; m <= label; m++) 
    if (newequiv->x[m]) {
      for (y = 0; y < A->rows; y++)
	for (x = 0; x < A->cols; x++)
	  if (MATRC(B, y, x) == (double) m)
	    MATRC(B, y, x) = current_cc;
      current_cc++;
    }

  //  printf("num_ccs = %i, highest label = %i\n", num_ccs, label);

  //  print_CR_Matrix(B);

  // clean up

  free_CR_Vector(equiv);
  free_CR_Vector(newequiv);

  // return

  return num_ccs;
}

//----------------------------------------------------------------------------

void biggest_connected_component_CR_Matrix(int fourconn, CR_Matrix *A, CR_Matrix *B)
{
  int num_ccs, i, biggest_cc;
  CR_Vector *area;

  num_ccs = connected_components_CR_Matrix(fourconn, A, B);
  area = make_all_CR_Vector("area", num_ccs + 1, 0.0);

  // calculate areas, keep track of maximal area and associated label

  for (i = 0; i < B->rows * B->cols; i++)  
    if (B->x[i])                              // effectively treating area of "blank" region as 0
      area->x[(int) B->x[i]]++;

  //  print_CR_Vector(area);

  biggest_cc = max_index_CR_Vector(area);
  //  printf("biggest_cc = %i\n", biggest_cc);

  // set all pixels not in biggest area to zero; set pixels in biggest area to one

  for (i = 0; i < B->rows * B->cols; i++)
    if (B->x[i] == (double) biggest_cc)
      B->x[i] = 1.0;
    else
      B->x[i] = 0.0;

  // clean up

  free_CR_Vector(area);
}

//----------------------------------------------------------------------------
// Level 3 BLAS and LAPACK
//----------------------------------------------------------------------------

CR_Matrix *convert_rc_matrix_to_CR_Matrix(int rows, int cols, double **rc)
{
  CR_Matrix *M;
  int i, j;

  M = make_CR_Matrix("M", rows, cols);
  
  for (j = 0; j < rows; j++)
    for (i = 0; i < cols; i++)
      MATRC(M, j, i) = rc[j][i];

  return M;
}

// Make a matrix

CR_Matrix *make_CR_Matrix(char *name, int rows, int cols)
{
  CR_Matrix *M;

  M = (CR_Matrix *) CR_malloc(sizeof(CR_Matrix));

  M->name = (char *) CR_calloc(CR_MATRIX_MAX_NAME, sizeof(char));
  strncpy(M->name, name, CR_MATRIX_MAX_NAME);

  M->rows = rows;
  M->cols = cols;

  M->x = (double *) CR_calloc(rows * cols, sizeof(double));

  M->buffsize = rows * cols * sizeof(double);

  return M;
}

//----------------------------------------------------------------------------

// read matrix in matlab format (A = [ a11, a12, ..., a1n ;
//                                     a21, a22, ..., a2n ;
//                                                .
//                                                .
//                                                .
//                                     am1, am2, ..., amn ];

CR_Matrix *read_CR_Matrix(char *filename)
{
  CR_Matrix *M;
  char *Mname;
  FILE *fp;
  int rows, cols, i, j;
  double temp;
  char c;

  Mname = (char *) CR_calloc(128, sizeof(char));

  fp = fopen(filename, "r");
  if (!fp) {
    //    CR_error("read_CR_Matrix(): no such file");
    CR_flush_printf("read_CR_Matrix(): no such file %s", filename);
    exit(1);
  }

  // figure out name of matrix plus number of rows, columns

  rows = count_lines(fp) - 1;   // - 1 added for TechSketch because roadpoly names are first line of file

  fscanf(fp, "%s = [", Mname);

  cols = 0;
  do {
    fscanf(fp, " %lf", &temp);
    c = fgetc(fp);
    cols++;
  } while (c == ',');

  // allocated the matrix

  M = make_CR_Matrix(Mname, rows, cols);

  // fill it in

  rewind(fp);

  fscanf(fp, "%s = [", Mname);

  for (j = 0; j < rows - 1; j++) {
    for (i = 0; i < cols - 1; i++) {
      fscanf(fp, "%lf, ", &temp);
      MATRC(M, j, i) = temp;
    }
    fscanf(fp, "%lf ;\n", &temp);
    MATRC(M, j, i) = temp;
  }

  for (i = 0; i < cols - 1; i++) {
    fscanf(fp, "%lf, ", &temp);
    MATRC(M, j, i) = temp;
  }
  fscanf(fp, "%lf", &temp);
  MATRC(M, j, i) = temp;

#ifdef CR_MATRIX_DEBUG
  print_CR_Matrix(M);
#endif

  // finish up

  fclose(fp);

  return M;
}

//----------------------------------------------------------------------------

// read matrix in Matlab's dlm format (a11,a12,...,a1n
//                                     a21,a22,...,a2n
//                                              .
//                                              .
//                                              .
//                                     am1,am2,...,amn 

// must know dimensions

CR_Matrix *read_dlm_CR_Matrix(char *filename, int rows, int cols)
{
  CR_Matrix *M;
  FILE *fp;
  double temp;
  int i, j;

  fp = fopen(filename, "r");
  if (!fp)
    CR_error("read_dlm_CR_Matrix(): no such file\n");

  M = make_CR_Matrix("M", rows, cols);

  for (j = 0; j < rows; j++) {
    for (i = 0; i < cols - 1; i++) {
      fscanf(fp, "%lf, ", &temp);
      //      fscanf(fp, "%lf,", &temp);
      MATRC(M, j, i) = temp;
    }
    fscanf(fp, "%lf\n", &temp);
    MATRC(M, j, i) = temp;
  }

  fclose(fp);

  return M;
}

// for already-allocated matrices

void read_dlm_CR_Matrix(char *filename, CR_Matrix *M)
{
  FILE *fp;
  double temp;
  int i, j, numrows;
  
  fp = fopen(filename, "r");
  numrows = count_lines(fp);
  printf("numrows = %i\n", numrows);

  //  for (j = 0; j < M->rows; j++) {
  for (j = 0; j < numrows; j++) {
    for (i = 0; i < M->cols - 1; i++) {
      fscanf(fp, "%lf, ", &temp);
      MATRC(M, j, i) = temp;
    }
    fscanf(fp, "%lf\n", &temp);
    MATRC(M, j, i) = temp;
  }
  M->rows = numrows;

  fclose(fp);
}

// for already-allocated matrices

void read_dlm_CR_Matrix(char *filename, CR_Matrix *M, int startrow)
{
  FILE *fp;
  double temp;
  int i, j, numrows;
  
  fp = fopen(filename, "r");
  numrows = count_lines(fp);
  printf("numrows = %i\n", numrows);
  if ((startrow + numrows) > M->rows)
    CR_error("read_dlm_CR_Matrix(): row overflow");

  //  for (j = 0; j < M->rows; j++) {
  for (j = 0; j < numrows; j++) {
    for (i = 0; i < M->cols - 1; i++) {
      fscanf(fp, "%lf, ", &temp);
      MATRC(M, j + startrow, i) = temp;
    }
    fscanf(fp, "%lf\n", &temp);
    MATRC(M, j + startrow, i) = temp;
  }
  //  M->rows = numrows;

  fclose(fp);
}

//----------------------------------------------------------------------------

// Make a matrix and initialize its values

CR_Matrix *init_CR_Matrix(char *name, int rows, int cols, ...)
{
  int r, c;
  CR_Matrix *M;
  va_list args;

  //  if (cols == 1)
  //    CR_error("make_CR_Matrix(): 1-column matrices should be vectors");

  M = make_CR_Matrix(name, rows, cols);

  /*
  M = (CR_Matrix *) CR_malloc(sizeof(CR_Matrix));

  M->name = (char *) CR_calloc(CR_MATRIX_MAX_NAME, sizeof(char));
  strncpy(M->name, name, CR_MATRIX_MAX_NAME);

  M->rows = rows;
  M->cols = cols;

  M->x = (double *) CR_calloc(rows * cols, sizeof(double));

  M->buffsize = rows * cols * sizeof(double);
  */

  // initialize to 0 by default

//   for (r = 0; r < M->rows; r++)
//     for (c = 0; c < M->cols; c++)
//       MATRC(M, r, c) = 0;

  // fill in provided values if they exist

  va_start(args, cols);

  for (r = 0; r < M->rows; r++)
    for (c = 0; c < M->cols; c++)
      //      M->x[r * M->rows + c] = va_arg(args, double);
      MATRC(M, r, c) = va_arg(args, double);

  va_end(args);

  return M;
}

//----------------------------------------------------------------------------

// Make a matrix and initialize its diagonal values, setting off-diagonal
// elements to zero

CR_Matrix *init_diagonal_CR_Matrix(char *name, int rows, ...)
{
  int i;
  CR_Matrix *M;
  va_list args;

  M = make_CR_Matrix(name, rows, rows);

  // initialize to 0 by default
 
  memset(M->x, 0, (int) M->buffsize);

  // fill in provided values if they exist

  va_start(args, rows);

  for (i = 0; i < M->rows; i++)
      MATRC(M, i, i) = va_arg(args, double);

  va_end(args);

  return M;
}

//----------------------------------------------------------------------------

// Make a matrix and initialize its diagonal values, setting off-diagonal
// elements to zero

CR_Matrix *make_diagonal_all_CR_Matrix(char *name, int rows, double x)
{
  int i;
  CR_Matrix *M;

  M = make_CR_Matrix(name, rows, rows);

  // initialize to 0 by default
 
  memset(M->x, 0, M->buffsize);

  // fill in provided values if they exist

  for (i = 0; i < M->rows; i++)
      MATRC(M, i, i) = x;

  return M;
}

//----------------------------------------------------------------------------

// Free matrix

void free_CR_Matrix(CR_Matrix *M)
{
  if (M) {
    CR_free_malloc(M->x, M->buffsize);
    CR_free_malloc(M, sizeof(CR_Matrix));
  }
}

//----------------------------------------------------------------------------

// Set elements of a matrix

void set_CR_Matrix(CR_Matrix *M, ...)
{
  int r, c;
  va_list args;

  // set values

  va_start(args, M);

  for (r = 0; r < M->rows; r++)
    for (c = 0; c < M->cols; c++)
      //      M->x[r * M->rows + c] = va_arg(args, double);
      MATRC(M, r, c) = va_arg(args, double);

  va_end(args);
}

//----------------------------------------------------------------------------

// Make a matrix with all of its elements set to x

CR_Matrix *make_all_CR_Matrix(char *name, int rows, int cols, double x)
{
  CR_Matrix *M;

  M = make_CR_Matrix(name, rows, cols);
  set_all_CR_Matrix(x, M);

  return M;
}

//----------------------------------------------------------------------------

// Print matrix

void print_CR_Matrix(CR_Matrix *M)
{
  int r, c, i, len;

  printf("%s(%i,%i) = ", M->name, M->rows, M->cols);
  len = strlen(M->name) + 5;
  printf("[ ");
  for (r = 0; r < M->rows - 1; r++) {
    for (c = 0; c < M->cols - 1; c++)
      printf("%6.5lf, ", MATRC(M, r, c));
    printf("%6.5lf ;\n", MATRC(M, r, c));
    for (i = 0; i < len; i++)
      printf(" ");
  }
  for (c = 0; c < M->cols - 1; c++)
    printf("%6.5lf, ", MATRC(M, r, c));
  printf("%6.5lf ];\n", MATRC(M, r, c));    
  printf("\n");

  /*
  printf("%s = ", M->name);
  len = strlen(M->name) + 5;
  printf("[ ");
  for (r = 0; r < M->rows - 1; r++) {
    for (c = 0; c < M->cols - 1; c++)
      printf("%6.3lf, ", M->x[r * M->rows + c]);
    printf("%6.3lf;\n", M->x[r * M->rows + c]);
    for (i = 0; i < len; i++)
      printf(" ");
  }
  for (c = 0; c < M->cols - 1; c++)
    printf("%6.3lf, ", M->x[r * M->rows + c]);
  printf("%6.3lf ];\n", M->x[r * M->rows + c]);    
  printf("\n");
  */
}

//----------------------------------------------------------------------------

// Print matrix in column-major format

void CM_print_CR_Matrix(CR_Matrix *M)
{
  int r, c, i, len;

  printf("%s = ", M->name);
  len = strlen(M->name) + 5;
  printf("[ ");
  for (r = 0; r < M->rows - 1; r++) {
    for (c = 0; c < M->cols - 1; c++)
      printf("%6.3lf, ", CM_MATRC(M, r, c));
    printf("%6.3lf ;\n", CM_MATRC(M, r, c));
    for (i = 0; i < len; i++)
      printf(" ");
  }
  for (c = 0; c < M->cols - 1; c++)
    printf("%6.3lf, ", CM_MATRC(M, r, c));
  printf("%6.3lf ];\n", CM_MATRC(M, r, c));    
  printf("\n");
}

//----------------------------------------------------------------------------

// Print an array of doubles like it's a matrix

void print_as_CR_Matrix(char *name, double *x, int rows, int cols)
{
  int r, c, i, len;

  printf("%s = ", name);
  len = strlen(name) + 5;
  printf("[ ");
  for (r = 0; r < rows - 1; r++) {
    for (c = 0; c < cols - 1; c++)
      printf("%6.3lf, ", x[c + cols * r]);
    printf("%6.3lf ;\n", x[c + cols * r]);
    for (i = 0; i < len; i++)
      printf(" ");
  }
  for (c = 0; c < cols - 1; c++)
    printf("%6.3lf, ", x[c + cols * r]);
  printf("%6.3lf ];\n", x[c + cols * r]);    
  printf("\n");
}

//----------------------------------------------------------------------------

// Print matrix to a file (that's already open)

void fprint_CR_Matrix(FILE *fp, CR_Matrix *M)
{
  int r, c, i, len;

  fprintf(fp, "%s = ", M->name);
  len = strlen(M->name) + 5;
  fprintf(fp, "[ ");
  for (r = 0; r < M->rows - 1; r++) {
    for (c = 0; c < M->cols - 1; c++)
      fprintf(fp, "%6.3lf, ", MATRC(M, r, c));
    fprintf(fp, "%6.3lf ;\n", MATRC(M, r, c));
    for (i = 0; i < len; i++)
      fprintf(fp, " ");
  }
  for (c = 0; c < M->cols - 1; c++)
    fprintf(fp, "%6.3lf, ", MATRC(M, r, c));
  fprintf(fp, "%6.3lf ];\n", MATRC(M, r, c));    
  fprintf(fp, "\n");
}

//----------------------------------------------------------------------------

// assume first column is [0,1] label

void write_svmlight_CR_Matrix(char *filename, CR_Matrix *M, int startcol, int stopcol)
{
  int r, c, i;
  FILE *fp;

  fp = fopen(filename, "w");

  for (r = 0; r < M->rows; r++) {
    
    // print label
    //      fprintf(fp, "%f ",MATRC(M, r,0));
     if (MATRC(M, r, 0))
       fprintf(fp, "+1 ");
     else
       fprintf(fp, "-1 ");

    for (c = startcol; c <= stopcol; c++) {
      fprintf(fp, "%i:%lf ", c, MATRC(M, r, c));
//       if (MATRC(M,r,c)>0)
// 	fprintf(fp, "%i:1.0 ", c);
//       else
// 	fprintf(fp, "%i:0.0 ", c);
    }
    fprintf(fp, "\n");
  }

  fclose(fp);
}

//----------------------------------------------------------------------------

// Print matrix to a file in dlm format

void write_dlm_CR_Matrix(char *filename, CR_Matrix *M)
{
  int r, c, i;
  FILE *fp;

  fp = fopen(filename, "w");

  for (r = 0; r < M->rows; r++) {
    for (c = 0; c < M->cols - 1; c++)
      fprintf(fp, "%lf, ", MATRC(M, r, c));
    fprintf(fp, "%lf\n", MATRC(M, r, c));
  }

  fclose(fp);
}

//----------------------------------------------------------------------------

// Print matrix to an already-open file in dlm format

void write_dlm_CR_Matrix(FILE *fp, CR_Matrix *M)
{
  int r, c, i;

  for (r = 0; r < M->rows; r++) {
    for (c = 0; c < M->cols - 1; c++)
      fprintf(fp, "%lf, ", MATRC(M, r, c));
    fprintf(fp, "%lf\n", MATRC(M, r, c));
  }
}

//----------------------------------------------------------------------------

// Copy matrix, including contents

CR_Matrix *copy_CR_Matrix(CR_Matrix *M)
{
  if (M) {
    CR_Matrix *M_copy = copy_header_CR_Matrix(M);
    
    memcpy(M_copy->x, M->x, M->buffsize);

    return M_copy;
  }
  else
    return NULL;
}

//--------------------------------------

// Copy matrix, including contents (overwrites old matrix---oooh, overloaded function!)
// (assumes M1, M2 match sizewise)

// M1 is src, M2 is dst

void copy_CR_Matrix(CR_Matrix *M1, CR_Matrix *M2)
{
  memcpy(M2->x, M1->x, M1->buffsize);
}

//----------------------------------------------------------------------------

// Copy matrix without duplicating contents

CR_Matrix *copy_header_CR_Matrix(CR_Matrix *M)
{
  CR_Matrix *M_copy = make_CR_Matrix("M_copy", M->rows, M->cols);

  return M_copy;
}

//----------------------------------------------------------------------------

// Set all elements of matrix to same value

void set_all_CR_Matrix(double x, CR_Matrix *M)
{
  int i;

  for (i = 0; i < M->rows * M->cols; i++)
    M->x[i] = x;
}

//----------------------------------------------------------------------------

// Is a point inside a polygon?

// from comp.graphics.algorithms FAQ...
// see http://www.ecse.rpi.edu/Homepages/wrf/geom/pnpoly.html

// returns TRUE if the point is inside the polygon and FALSE if it is outside
// if the point is on an edge, either value may be returned

// expects n x 2 matrix, where first column is x and second column is y

int point_in_poly_CR_Matrix(double x, double y, CR_Matrix *poly)
{
  int i, j, c;

  c = FALSE;

  for (i = 0, j = poly->rows - 1; i < poly->rows; j = i++) {

    if ((((MATRC(poly, i, 1) <= y) && (y < MATRC(poly, j, 1))) || 
	 ((MATRC(poly, j, 1) <= y) && (y < MATRC(poly, i, 1)))) &&
	(x < (MATRC(poly, j, 0) - MATRC(poly, i, 0)) * (y - MATRC(poly, i, 1)) / 
	 (MATRC(poly, j, 1) - MATRC(poly, i, 1)) + MATRC(poly, i, 0)))
      
      c = !c;
  }

  return c;
}

//----------------------------------------------------------------------------

void transpose_CR_Matrix(CR_Matrix *M, CR_Matrix *transposedM)
{
  int i, j;

  for (i = 0; i < M->rows; i++)
    for (j = 0; j < M->cols; j++)
      MATRC(transposedM, j, i) = MATRC(M, i, j);
}

//--------------------------------------

CR_Matrix *transpose_CR_Matrix(CR_Matrix *M)
{
  CR_Matrix *transposedM = make_CR_Matrix("M'", M->cols, M->rows);
  int i, j;

  transpose_CR_Matrix(M, transposedM);
  
  return transposedM;
}

//----------------------------------------------------------------------------

// M3 = M1 * M2

void product_CR_Matrix(CR_Matrix *M1, CR_Matrix *M2, CR_Matrix *M3)
{
#ifdef CR_MATRIX_DEBUG
  if (M1->cols != M2->rows) 
    CR_error("product_CR_Matrix(): # columns of M1 (%i) != # rows of M2 (%i)", M1->cols, M2->rows);
  else if (M1->rows != M3->rows || M2->cols != M3->cols)
    CR_error("product_CR_Matrix(): # rows of M1 (%i) != # rows of M3 (%i) or # cols of M2 (%i) != # cols of M3 (%i)", M1->rows, M3->rows, M2->cols, M3->cols);
#endif

  //  print_CR_Matrix(M1);
  //  print_CR_Matrix(M2);

  my_product_CR_Matrix(M1, M2, M3);
  //  cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, M1->rows, M2->cols, M1->cols, 1.0, M1->x, M1->rows, M2->x, M1->cols, 0.0, M3->x, M1->rows);

  //  print_CR_Matrix(M3);
}

//--------------------------------------

CR_Matrix *product_CR_Matrix(CR_Matrix *M1, CR_Matrix *M2)
{
  CR_Matrix *M3;

#ifdef CR_MATRIX_DEBUG
  if (M1->cols != M2->rows) 
    CR_error("product_CR_Matrix(): # columns of M1 (%i) != # rows of M2 (%i)", M1->cols, M2->rows);
#endif

  M3 = make_CR_Matrix("M3", M1->rows, M2->cols);
  product_CR_Matrix(M1, M2, M3);

  return M3;
}

//----------------------------------------------------------------------------

// D = A * B * C

CR_Matrix *product3_CR_Matrix(CR_Matrix *A, CR_Matrix *B, CR_Matrix *C)
{
  CR_Matrix *AB, *D;

  AB = product_CR_Matrix(A, B);
  D = product_CR_Matrix(AB, C);

  free_CR_Matrix(AB);

  return D;
}

//----------------------------------------------------------------------------

// non-BLAS version of M3 = M1 * M2

void my_product_CR_Matrix(CR_Matrix *M1, CR_Matrix *M2, CR_Matrix *M3)
{
#ifdef CR_MATRIX_DEBUG
  if (M1->cols != M2->rows) 
    CR_error("product_CR_Matrix(): # columns of M1 (%i) != # rows of M2 (%i)", M1->cols, M2->rows);
  else if (M1->rows != M3->rows || M2->cols != M3->cols)
    CR_error("product_CR_Matrix(): # rows of M1 (%i) != # rows of M3 (%i) or # cols of M2 (%i) != # cols of M3 (%i)", M1->rows, M3->rows, M2->cols, M3->cols);
#endif

  int i, j, k;

  /*
  for (i = 0; i < M1->rows; i++) 
    for (j = 0; j < M2->cols; j++) {
      MATRC(M3, i, j) = 0.0;
      for (k = 0; k < M1->cols; k++)  
	MATRC(M3, i, j) += MATRC(M1, i, k) * MATRC(M2, k, j);
    }
  */

  for (i = 0; i < M1->rows; i++) 
    for (j = 0; j < M2->cols; j++) {
      CM_MATRC(M3, i, j) = 0.0;
      for (k = 0; k < M1->cols; k++)  
	CM_MATRC(M3, i, j) += CM_MATRC(M1, i, k) * CM_MATRC(M2, k, j);
    }

}

//----------------------------------------------------------------------------

// Scale matrix (non-destructive)

CR_Matrix *scale_CR_Matrix(double a, CR_Matrix *M)
{
  CR_error("2 removed on Linux -- no MKL");

  /*
  CR_Matrix *Mscale;

  Mscale = copy_CR_Matrix(M);
  cblas_dscal(Mscale->rows * Mscale->cols, a, Mscale->x, 1);

  return Mscale;
  */
}

//----------------------------------------------------------------------------

// C = A + B

void sum_CR_Matrix(CR_Matrix *A, CR_Matrix *B, CR_Matrix *C)
{
  CR_error("3 removed on Linux -- no MKL");

  /*
  copy_CR_Matrix(A, C);
  cblas_daxpy(A->rows * A->cols, 1, B->x, 1, C->x, 1);
  */
}

//--------------------------------------

// C = A + B

CR_Matrix *sum_CR_Matrix(CR_Matrix *A, CR_Matrix *B)
{
  CR_error("4 removed on Linux -- no MKL");

  /*
  CR_Matrix *C;

  C = copy_CR_Matrix(A);
  cblas_daxpy(A->rows * A->cols, 1, B->x, 1, C->x, 1);

  return C;
  */
}

//--------------------------------------

// A += B

void sum_in_place_CR_Matrix(CR_Matrix *A, CR_Matrix *B)
{
  CR_error("5 removed on Linux -- no MKL");

  /*
  cblas_daxpy(A->rows * A->cols, 1, B->x, 1, A->x, 1);
  */
}

//----------------------------------------------------------------------------

// D = A + B + C

CR_Matrix *sum3_CR_Matrix(CR_Matrix *A, CR_Matrix *B, CR_Matrix *C)
{
  CR_error("6 removed on Linux -- no MKL");

  /*
  CR_Matrix *AB, *D;

  AB = sum_CR_Matrix(A, B);
  D = sum_CR_Matrix(AB, C);

  free_CR_Matrix(AB);

  return D;
  */
}

//----------------------------------------------------------------------------

// C = A - B

CR_Matrix *difference_CR_Matrix(CR_Matrix *A, CR_Matrix *B)
{
  CR_error("7 removed on Linux -- no MKL");

  /*
  CR_Matrix *C;

  C = copy_CR_Matrix(A);
  cblas_daxpy(A->rows * A->cols, -1, B->x, 1, C->x, 1);

  return C;
  */
}

//----------------------------------------------------------------------------

// Moore-Penrose pseudoinverse of a matrix

CR_Matrix *pseudoinverse_CR_Matrix(CR_Matrix *A)
{
  CR_Matrix *AT, *APlus;
  //  CR_Matrix *AAT, *AATInv;
  CR_Matrix *ATA, *ATAInv;

  AT = transpose_CR_Matrix(A);
  /*
  AAT = make_CR_Matrix("AAT", A->rows, A->rows);
  product_CR_Matrix(A, AT, AAT);
  AATInv = inverse_CR_Matrix(AAT);
  APlus = make_CR_Matrix("APlus", A->cols, A->rows);
  product_CR_Matrix(AT, AATInv, APlus);
  */

  ATA = product_CR_Matrix(AT, A);
  ATAInv = inverse_CR_Matrix(ATA);
  APlus = product_CR_Matrix(ATAInv, AT);

  free_CR_Matrix(AT);
  free_CR_Matrix(ATA);
  free_CR_Matrix(ATAInv);

  /*  
  free_CR_Matrix(AAT);
  free_CR_Matrix(AATInv);
  */

  return APlus;
}

//----------------------------------------------------------------------------

#define NR_END 1
#define FREE_ARG char*


void nrerror(char error_text[])
  /* Numerical Recipes standard error handler */
{
  fprintf(stderr,"Numerical Recipes run-time error...\n");
  fprintf(stderr,"%s\n",error_text);
  fprintf(stderr,"...now exiting to system...\n");
  exit(1);
}

float *vector(long nl, long nh)
  /* allocate a float vector with subscript range v[nl..nh] */
{
  float *v;
  v=(float *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(float)));
  if (!v) nrerror("allocation failure in vector()");
  return v-nl+NR_END;
}

void free_vector(float *v, long nl, long nh)
/* free a float vector allocated with vector() */
{
  free((FREE_ARG) (v+nl-NR_END));
}

float **matrix(long nrl, long nrh, long ncl, long nch)
/* allocate a float matrix with subscript range m[nrl..nrh][ncl..nch] */
{
  long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
  float **m;
  /* allocate pointers to rows */
  m=(float **) malloc((size_t)((nrow+NR_END)*sizeof(float*)));
  
  if (!m) nrerror("allocation failure 1 in matrix()");
  m += NR_END;
  m -= nrl;
  /* allocate rows and set pointers to them */
  m[nrl]=(float *) malloc((size_t)((nrow*ncol+NR_END)*sizeof(float)));
  if (!m[nrl]) nrerror("allocation failure 2 in matrix()");
  m[nrl] += NR_END;
  m[nrl] -= ncl;
  for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;
  /* return pointer to array of pointers to rows */
  return m;
}

//Given a matrix a[1..m][1..n], this routine computes its singular value decomposition, 
//A = U�W�V T. Thematrix U replaces a on output. The diagonal matrix of singular values W is output
//as a vector w[1..n]. The matrix V (not the transpose V T ) is output as v[1..n][1..n].

float *rv1;

void init_svdcmp(int n)
{
  //  rv1=vector(1,n); 
}

int svdcmp(float **a, int m, int n, float w[], float **v)
{
  float pythag(float a, float b);
  int flag,i,its,j,jj,k,l,nm;
  float anorm,c,f,g,h,s,scale,x,y,z,*rv1;
  //float anorm,c,f,g,h,s,scale,x,y,z;

  rv1=vector(1,n);
  g=scale=anorm=0.0; // Householder reduction to bidiagonal form.
  for (i=1;i<=n;i++) {
    l=i+1;
    rv1[i]=scale*g;
    g=s=scale=0.0;
    if (i <= m) {
      for (k=i;k<=m;k++) scale += fabs(a[k][i]);
      if (scale) {
	for (k=i;k<=m;k++) {
	  a[k][i] /= scale;
	  s += a[k][i]*a[k][i];
	}
	f=a[i][i];
	g = -SIGN(sqrt(s),f);
	h=f*g-s;
	a[i][i]=f-g;
	for (j=l;j<=n;j++) {
	  for (s=0.0,k=i;k<=m;k++) s += a[k][i]*a[k][j];
	  f=s/h;
	  for (k=i;k<=m;k++) a[k][j] += f*a[k][i];
	}
	for (k=i;k<=m;k++) a[k][i] *= scale;
      }
    }
    w[i]=scale *g;
    g=s=scale=0.0;
    if (i <= m && i != n) {
      for (k=l;k<=n;k++) scale += fabs(a[i][k]);
      if (scale) {
	
	for (k=l;k<=n;k++) {
	  a[i][k] /= scale;
	  s += a[i][k]*a[i][k];
	}
	f=a[i][l];
	g = -SIGN(sqrt(s),f);
	h=f*g-s;
	a[i][l]=f-g;
	for (k=l;k<=n;k++) rv1[k]=a[i][k]/h;
	for (j=l;j<=m;j++) {
	  for (s=0.0,k=l;k<=n;k++) s += a[j][k]*a[i][k];
	  for (k=l;k<=n;k++) a[j][k] += s*rv1[k];
	}
	for (k=l;k<=n;k++) a[i][k] *= scale;
      }
    }
    anorm=FMAX(anorm,(fabs(w[i])+fabs(rv1[i])));
  }

  for (i=n;i>=1;i--) { // Accumulation of right-hand transformations.
    if (i < n) {
      if (g) {
	for (j=l;j<=n;j++) // Double division to avoid possible underflow.
	  v[j][i]=(a[i][j]/a[i][l])/g;
	for (j=l;j<=n;j++) {
	  for (s=0.0,k=l;k<=n;k++) s += a[i][k]*v[k][j];
	  for (k=l;k<=n;k++) v[k][j] += s*v[k][i];
	}
      }
      for (j=l;j<=n;j++) v[i][j]=v[j][i]=0.0;
    }
    v[i][i]=1.0;
    g=rv1[i];
    l=i;
  }
  for (i=IMIN(m,n);i>=1;i--) { // Accumulation of left-hand transformations.
    l=i+1;
    g=w[i];
    for (j=l;j<=n;j++) a[i][j]=0.0;
    if (g) {
      g=1.0/g;
      for (j=l;j<=n;j++) {
	for (s=0.0,k=l;k<=m;k++) s += a[k][i]*a[k][j];
	f=(s/a[i][i])*g;
	for (k=i;k<=m;k++) a[k][j] += f*a[k][i];
      }
      for (j=i;j<=m;j++) a[j][i] *= g;
    } else for (j=i;j<=m;j++) a[j][i]=0.0;
    ++a[i][i];
  }
  for (k=n;k>=1;k--) { // Diagonalization of the bidiagonal form: Loop over
    // singular values, and over allowed iterations. 
    for (its=1;its<=30;its++) {
      flag=1;
      for (l=k;l>=1;l--) { // Test for splitting.
	nm=l-1; // Note that rv1[1] is always zero.
	if ((float)(fabs(rv1[l])+anorm) == anorm) {
	  flag=0;
	  break;
	}
	if ((float)(fabs(w[nm])+anorm) == anorm) break;
      }
      if (flag) {
	c=0.0; // Cancellation of rv1[l], if l > 1.
	s=1.0;
	for (i=l;i<=k;i++) {
	  
	  f=s*rv1[i];
	  rv1[i]=c*rv1[i];
	  if ((float)(fabs(f)+anorm) == anorm) break;
	  g=w[i];
	  h=pythag(f,g);
	  w[i]=h;
	  h=1.0/h;
	  c=g*h;
	  s = -f*h;
	  for (j=1;j<=m;j++) {
	    y=a[j][nm];
	    z=a[j][i];
	    a[j][nm]=y*c+z*s;
	    a[j][i]=z*c-y*s;
	  }
	}
      }
      z=w[k];
      if (l == k) { // Convergence.
	if (z < 0.0) { // Singular value is made nonnegative.
	  w[k] = -z;
	  for (j=1;j<=n;j++) v[j][k] = -v[j][k];
	}
	break;
      }
      if (its == 30) 
	return 0;
      //nrerror("no convergence in 30 svdcmp iterations");
      x=w[l]; // Shift from bottom 2-by-2 minor.
      nm=k-1;
      y=w[nm];
      g=rv1[nm];
      h=rv1[k];
      f=((y-z)*(y+z)+(g-h)*(g+h))/(2.0*h*y);
      g=pythag(f,1.0);
      f=((x-z)*(x+z)+h*((y/(f+SIGN(g,f)))-h))/x;
      c=s=1.0; // Next QR transformation:
      for (j=l;j<=nm;j++) {
	i=j+1;
	g=rv1[i];
	y=w[i];
	h=s*g;
	g=c*g;
	z=pythag(f,h);
	rv1[j]=z;
	c=f/z;
	s=h/z;
	f=x*c+g*s;
	g = g*c-x*s;
	h=y*s;
	y *= c;
	for (jj=1;jj<=n;jj++) {
	  x=v[jj][j];
	  z=v[jj][i];
	  v[jj][j]=x*c+z*s;
	  v[jj][i]=z*c-x*s;
	}
	z=pythag(f,h);
	w[j]=z; // Rotation can be arbitrary if z = 0.
	if (z) {
	  z=1.0/z;
	  c=f*z;
	  s=h*z;
	}
	f=c*g+s*y;
	x=c*y-s*g;

	for (jj=1;jj<=m;jj++) {
	  y=a[jj][j];
	  z=a[jj][i];
	  a[jj][j]=y*c+z*s;
	  a[jj][i]=z*c-y*s;
	}
      }
      rv1[l]=0.0;
      rv1[k]=f;
      w[k]=x;
    }
  }

  free_vector(rv1,1,n);
  return 1;
}

//Computes (a2 + b2)1/2 without destructive underflow or overflow.

float pythag(float a, float b)
{
  float absa,absb;
  absa=fabs(a);
  absb=fabs(b);
  if (absa > absb) return absa*sqrt(1.0+SQR(absb/absa));
  else return (absb == 0.0 ? 0.0 : absb*sqrt(1.0+SQR(absa/absb)));
}

//----------------------------------------------------------------------------

int svd_lwork;
double *svd_work;
char svd_jobu, svd_jobvt;
int svd_info;

void init_svd(int rows)
{
  svd_lwork = SQUARE(rows);
  svd_work = (double *) CR_calloc(svd_lwork, sizeof(double));
  
  svd_jobu = 'A';
  svd_jobvt = 'A';
}

// assuming rows > cols

// remember that CLAPACK is expecting column-major format

void clapack_svd_CR_Matrix(CR_Matrix *M, CR_Matrix *U, double *S, CR_Matrix *VT)
{
  CR_error("8 removed on Linux -- no MKL");

  /*
  //  printf("%.1lf %.1lf\n", CM_MATRC(M, 0, 0), CM_MATRC(M, 0, 1));

  DGESVD(&svd_jobu, &svd_jobvt, &M->rows, &M->cols, M->x, &M->rows, S, U->x, &M->rows, VT->x, &M->cols, svd_work, &svd_lwork, &svd_info);

//   for (j = 0; j < M->rows; j++)
//     for (i = 0; i < M->cols; i++)
//       CM_MATRC(S, j, i) = 0.0;

//   for (i = 0; i < M->cols; i++)
//     CM_MATRC(S, i, i) = singular[i];

  //  printf("S: %.1lf %.1lf\n", CM_MATRC(S, 0, 0), CM_MATRC(S, 1, 1));

  //  CM_print_CR_Matrix(U);
  //  CM_print_CR_Matrix(S);
  //  CM_print_CR_Matrix(VT);

  //  CM_print_CR_Matrix(product3_CR_Matrix(U, S, VT));
  */
}

//----------------------------------------------------------------------------

// Compute singular value decomposition

// [U S VT] = svd(M)

// M can be non-square (assume rows (A) >= cols (B))
// M = A x B, U = A x A, S = A x B, VT = B x B

void svd_CR_Matrix(CR_Matrix *M, CR_Matrix *U, CR_Matrix *S, CR_Matrix *VT)
{
  CR_error("9 removed on Linux -- no MKL");

  /*
  double *M_diag, *M_offdiag, *tauq, *taup, *work, *temp;
  double *S_copy;
  int info, lwork, ldc, ncc, i, j;
  char uplo, vect;
  CR_Matrix *B, *Q, *PT, *QB, *QBPT;
  double *MM1, *MM2;

#ifdef CR_MATRIX_DEBUG
  if (M->rows != M->cols)
    CR_error("svd_CR_Matrix(): not yet ready for non-square M");
#endif

  MM1 = (double *) CR_calloc(M->rows * M->rows, sizeof(double));
  MM2 = (double *) CR_calloc(M->rows * M->rows, sizeof(double));
  //  S_copy = (double *) CR_malloc(S->buffsize);
  M_diag = (double *) CR_calloc(M->cols, sizeof(double));
  M_offdiag = (double *) CR_calloc(M->cols - 1, sizeof(double));
  tauq = (double *) CR_calloc(M->cols, sizeof(double));
  taup = (double *) CR_calloc(M->cols, sizeof(double));
  lwork = 64 * (M->rows + M->cols);
  work = (double *) CR_calloc(lwork, sizeof(double));

  // reduce M to bidiagonal form s.t. M = Q * B * P' 

  // transpose because fortran sucks

  for (j = 0; j < M->rows; j++)
    for (i = 0; i < M->cols; i++)
      MM1[j + M->rows * i] = MATRC(M, j, i);

  //  for (i = 0; i < M->rows * M->cols; i++)
  //    printf("%lf ", M->x[i]);
  //  printf("\n");

  //  print_as_CR_Matrix("MM1", MM1, M->rows, M->rows);

  //  print_CR_Matrix(M);
   //  print_as_CR_Matrix("MM1", MM1, M->rows, M->rows);

  //  memcpy(S->x, M->x, M->buffsize);
  //  DGEBRD(&M->rows, &M->cols, S->x, &M->rows, M_diag, M_offdiag, tauq, taup, work, &lwork, &info);
  DGEBRD(&M->rows, &M->cols, MM1, &M->rows, M_diag, M_offdiag, tauq, taup, work, &lwork, &info);

  //  DGEBRD(&M->cols, &M->rows, M->x, &M->cols, M_diag, M_offdiag, tauq, taup, work, &lwork, &info);
  if (info)
    CR_error("svd_CR_Matrix(): bad reduction to bidiagonal form");

#ifdef CR_MATRIX_DEBUG
  print_as_CR_Vector("B diag", M_diag, M->cols);
  print_as_CR_Vector("B upper diag", M_offdiag, M->cols - 1);
#endif

  //  print_as_CR_Vector("tauq", tauq, M->cols);
  //  print_as_CR_Vector("taup", taup, M->cols);

  //  print_CR_Matrix(M);

  //  print_as_CR_Matrix("MM1", MM1, M->rows, M->rows);

  // generate Q

  vect = 'Q';
  //  memcpy(S_copy, S->x, S->buffsize); // save B; Q goes in S
  //  DORGBR(&vect, &M->rows, &M->rows, &M->cols, S->x, &M->rows, tauq, work, &lwork, &info);
  memcpy(MM2, MM1, M->rows * M->rows * sizeof(double));
  DORGBR(&vect, &M->rows, &M->rows, &M->cols, MM1, &M->rows, tauq, work, &lwork, &info);
  if (info)
    CR_error("svd_CR_Matrix(): bad generation of Q");

//   MM1[0 + M->rows * 0] = -0.776;
//   MM1[1 + M->rows * 0] = -0.3110;
//   MM1[2 + M->rows * 0] = -0.5430;
//   MM1[3 + M->rows * 0] = -0.7760;
//   MM1[0 + M->rows * 1] = -0.833;
//   MM1[1 + M->rows * 1] = -0.451;
//   MM1[2 + M->rows * 1] = -0.069;
//   MM1[3 + M->rows * 1] = 0.312;
//   MM1[0 + M->rows * 2] = 0.392;
//   MM1[1 + M->rows * 2] = -0.238;
//   MM1[2 + M->rows * 2] = 0.701;
//   MM1[3 + M->rows * 2] = 0.547;
//   MM1[0 + M->rows * 3] = -0.383;
//   MM1[1 + M->rows * 3] = 0.802;
//   MM1[2 + M->rows * 3] = -0.457;
//   MM1[3 + M->rows * 3] = 0.037;

  // generate P'

  vect = 'P';
  //  DORGBR(&vect, &M->rows, &M->cols, &M->rows, VT->x, &M->rows, taup, work, &lwork, &info);
  //  DORGBR(&vect, &M->cols, &M->cols, &M->rows, S_copy, &M->rows, taup, work, &lwork, &info);  // S_copy becomes P'
  DORGBR(&vect, &M->cols, &M->cols, &M->rows, MM2, &M->rows, taup, work, &lwork, &info); 
  if (info)
    CR_error("svd_CR_Matrix(): bad generation of P'");

  //  print_as_CR_Matrix("Q", MM1, M->rows, M->rows);
  //  print_as_CR_Matrix("P'", MM2, M->rows, M->rows);

  B = make_CR_Matrix("B", M->rows, M->cols);
  memset(B->x, 0, B->buffsize);
  for (i = 0; i < M->cols; i++) {
    MATRC(B, i, i) = M_diag[i];
    if (i < M->cols + 1)
      MATRC(B, i, i + 1) = M_offdiag[i];
  }

  Q = make_CR_Matrix("Q", M->rows, M->rows);
  PT = make_CR_Matrix("PT", M->cols, M->cols);
  for (j = 0; j < M->rows; j++) 
    for (i = 0; i < M->rows; i++) {
      MATRC(Q, i, j) = MM1[i + M->rows * j];
      if (j < M->cols && i < M->cols)
	MATRC(PT, j, i) = MM2[i + M->rows * j];
    }
  //  memcpy(PT->x, MM2, PT->buffsize);

  
  QB = make_CR_Matrix("QB", M->rows, M->cols);
  QBPT = make_CR_Matrix("QBPT", M->rows, M->cols);


#ifdef CR_MATRIX_DEBUG
  print_CR_Matrix(Q);
  print_CR_Matrix(B);
  print_CR_Matrix(PT);
#endif

  CR_Matrix *QT, *P;

//   QT = transpose_CR_Matrix(Q);
//   P = transpose_CR_Matrix(PT);

  product_CR_Matrix(Q, B, QB);
  product_CR_Matrix(QB, PT, QBPT);

#ifdef CR_MATRIX_DEBUG
  print_CR_Matrix(QB);
  print_CR_Matrix(QBPT);
#endif

  //  CR_exit(1);


//   product_CR_Matrix(QT, B, QB);
//   product_CR_Matrix(QB, P, QBPT);
// 
//   printf("QTB\n");
//   print_CR_Matrix(QB);
//   printf("QTBP\n");
//   print_CR_Matrix(QBPT);

  // compute SVD of B -- singular values wind up in M_diag

  uplo = 'U';
  ldc = 1;
  ncc = 0;
  // the 2nd arg is B's order... is this right?  maybe MAX2(M->rows, M->cols)?  or M->rows * M->cols? 
  //  DBDSQR(&uplo, &M->rows, &VT->cols, &U->rows, &ncc, M_diag, M_offdiag, VT->x, &VT->rows, U->x, &U->rows, NULL, &ldc, work, &info);
  //  DBDSQR(&uplo, &M->rows, &VT->cols, &U->rows, &ncc, M_diag, M_offdiag, S_copy, &VT->rows, S->x, &U->rows, NULL, &ldc, work, &info);
  //  DBDSQR(&uplo, &M->cols, &VT->cols, &U->rows, &ncc, M_diag, M_offdiag, MM2, &VT->rows, MM1, &U->rows, NULL, &ldc, work, &info);
  DBDSQR(&uplo, &M->cols, &M->rows, &U->rows, &ncc, M_diag, M_offdiag, MM2, &M->rows, MM1, &U->rows, NULL, &ldc, work, &info);
  if (info)
    CR_error("svd_CR_Matrix(): bad SVD");

#ifdef CR_MATRIX_DEBUG
  print_as_CR_Vector("singular values", M_diag, M->cols);
  print_as_CR_Matrix("U", MM1, M->rows, M->rows);
  print_as_CR_Matrix("VT", MM2, M->rows, M->rows);
#endif

  for (j = 0; j < M->rows; j++) 
    for (i = 0; i < M->rows; i++) {
      MATRC(U, i, j) = MM1[i + M->rows * j];
      if (j < M->cols && i < M->cols)
	//	MATRC(VT, j, i) = MM2[i + M->rows * j];
	MATRC(VT, i, j) = MM2[i + M->rows * j];
    }

//   for (i = 0; i < S->rows * S->cols; i++)
//     S->x[i] = 0;
//   print_CR_Matrix(S);
  memset(S->x, 0, S->buffsize);
  for (i = 0; i < S->cols; i++) 
    S->x[i * (S->cols + 1)] = M_diag[i];


#ifdef CR_MATRIX_DEBUG
  print_CR_Matrix(U);
  print_CR_Matrix(VT);
  print_CR_Matrix(S);
#endif

  CR_Matrix *US, *USVT;

  US = make_CR_Matrix("US", U->rows, S->cols);
  USVT = make_CR_Matrix("USVT", U->rows, VT->cols);

  product_CR_Matrix(U, S, US);

  product_CR_Matrix(US, VT, USVT);

#ifdef CR_MATRIX_DEBUG
  print_CR_Matrix(US);
  print_CR_Matrix(USVT);
#endif

  //  CR_exit(1);

  // swap VT and U because they are transposed versions of what we want (b/c we're doing svd of
  // A rather than B, if i'm understanding the documentation for BDSQR correctly)

//   temp = VT->x;
//   VT->x = U->x;
//   U->x = temp;

  //  memcpy(VT->x, S_copy, VT->buffsize);
  //  memcpy(U->x, S->x, U->buffsize);

  // transfer singular values to diagonal of S

  //  memset(S->x, 0, M->buffsize);

  // clean up

  CR_free_calloc(MM1, M->rows * M->rows, sizeof(double));
  CR_free_calloc(MM2, M->rows * M->rows, sizeof(double));
  //  CR_free_malloc(S_copy, S->buffsize);
  CR_free_calloc(M_diag, M->rows, sizeof(double));
  CR_free_calloc(M_offdiag, M->rows, sizeof(double));  
  CR_free_calloc(tauq, M->rows, sizeof(double));
  CR_free_calloc(taup, M->rows, sizeof(double));
  CR_free_calloc(work, lwork, sizeof(double));

//   product_CR_Matrix(S, VT, SVT);
//   print_CR_Matrix(SVT);
// 
//   product_CR_Matrix(U, SVT, USVT);

  //  CR_exit(1);
*/
}

//----------------------------------------------------------------------------

// Compute singular value decomposition

// [U S VT] = svd(M)

// assuming M is square

void square_svd_CR_Matrix(CR_Matrix *M, CR_Matrix *U, CR_Matrix *S, CR_Matrix *VT)
{
  //  CR_error("10 removed on Linux -- no MKL");

  double *M_diag, *M_offdiag, *tauq, *taup, *work, *temp;
  int info, lwork, ldc, ncc, i, j, result;
  char uplo, vect;
  
#ifdef CR_MATRIX_DEBUG
  if (M->rows != M->cols)
    CR_error("svd_CR_Matrix(): not yet ready for non-square M");
#endif

  M_diag = (double *) CR_calloc(M->rows, sizeof(double));
  M_offdiag = (double *) CR_calloc(M->rows - 1, sizeof(double));
  tauq = (double *) CR_calloc(M->rows, sizeof(double));
  taup = (double *) CR_calloc(M->rows, sizeof(double));
  lwork = 64 * (M->rows + M->cols);
  work = (double *) CR_calloc(lwork, sizeof(double));

  // reduce M to bidiagonal form s.t. M = Q * B * P' (storing B in U for now) 

  memcpy(U->x, M->x, M->buffsize);
  //xxx  DGEBRD(&M->rows, &M->cols, U->x, &M->rows, M_diag, M_offdiag, tauq, taup, work, &lwork, &info);

  dgebrd_((integer *) &M->rows, (integer *) &M->cols, U->x, (integer *) &M->rows, M_diag, M_offdiag, tauq, taup, work, (integer *) &lwork, (integer *) &info);

  if (info)
    CR_error("svd_CR_Matrix(): bad reduction to bidiagonal form");

  // generate Q

  vect = 'Q';
  memcpy(VT->x, U->x, M->buffsize); // save B in VT; Q goes in U
  //xxx  DORGBR(&vect, &M->rows, &M->cols, &M->cols, U->x, &M->rows, tauq, work, &lwork, &info);
  dorgbr_(&vect, (integer *) &M->rows, (integer *) &M->cols, (integer *) &M->cols, U->x, (integer *) &M->rows, tauq, work, (integer *) &lwork, (integer *) &info);
  if (info)
    CR_error("svd_CR_Matrix(): bad generation of Q");

  // generate P'

  vect = 'P';
  //xxx  DORGBR(&vect, &M->rows, &M->cols, &M->rows, VT->x, &M->rows, taup, work, &lwork, &info);
  dorgbr_(&vect, (integer *) &M->rows, (integer *) &M->cols, (integer *) &M->rows, VT->x, (integer *) &M->rows, taup, work, (integer *) &lwork, (integer *) &info);
  if (info)
    CR_error("svd_CR_Matrix(): bad generation of P'");

  // compute SVD of B -- singular values wind up in M_diag

  uplo = 'U';
  ldc = 1;
  ncc = 0;
  // the 2nd arg is B's order... is this right?  maybe MAX2(M->rows, M->cols)?  or M->rows * M->cols? 
  //  DBDSQR(&uplo, &M->rows, &VT->cols, &U->rows, &ncc, M_diag, M_offdiag, VT->x, &VT->rows, U->x, &U->rows, NULL, &ldc, work, &info);
  dbdsqr_(&uplo, (integer *) &M->rows, (integer *) &VT->cols, (integer *) &U->rows, (integer *) &ncc, M_diag, M_offdiag, VT->x, (integer *) &VT->rows, U->x, (integer *) &U->rows, NULL, (integer *) &ldc, work, (integer *) &info);
  if (info)
    CR_error("svd_CR_Matrix(): bad SVD");

  // swap VT and U because they are transposed versions of what we want (b/c we're doing svd of
  // A rather than B, if i'm understanding the documentation for BDSQR correctly)

  temp = VT->x;
  VT->x = U->x;
  U->x = temp;

  // transfer singular values to diagonal of S

  memset(S->x, 0, M->buffsize);
  for (i = 0, j = 0; i < S->rows * S->cols; i += (S->cols + 1), j++)
    S->x[i] = M_diag[j];

  // clean up

  CR_free_calloc(M_diag, M->rows, sizeof(double));
  CR_free_calloc(M_offdiag, M->rows, sizeof(double));  
  CR_free_calloc(tauq, M->rows, sizeof(double));
  CR_free_calloc(taup, M->rows, sizeof(double));
  CR_free_calloc(work, lwork, sizeof(double));
}

//----------------------------------------------------------------------------

// Inverse of a matrix using SVD

// M should be square

// don't take 1 / 0 of diagonal elements, and result
// is V * (1 / S) * UT

CR_Matrix *svd_inverse_CR_Matrix(CR_Matrix *M)
{
  int i;
  CR_Matrix *U, *S, *VT;
  CR_Matrix *MInv;

  MInv = make_CR_Matrix("MInv", M->rows, M->cols);

  // compute svd of M = U * S * V'
  
  U = make_CR_Matrix("U", M->rows, M->cols);
  S = make_CR_Matrix("S", M->rows, M->cols);
  VT = make_CR_Matrix("VT", M->rows, M->cols);

  //  square_svd_CR_Matrix(M, U, S, VT);
  svd_CR_Matrix(M, U, S, VT);

  print_CR_Matrix(U);
  print_CR_Matrix(S);
  print_CR_Matrix(VT);
  //  CR_exit(1);

  // take inverse of S's diagonal elements
  // (would it be more efficient to do this with VML on M_diag in SVD before copying?)

  for (i = 0; i < M->rows * M->cols; i += (M->cols + 1))
    S->x[i] = 1 / S->x[i];

  // set MInv = U * (1 / S) * V'

  product_CR_Matrix(U, S, MInv);
  memcpy(S->x, MInv->x, M->buffsize);
  product_CR_Matrix(S, VT, MInv);

  // clean up

  free_CR_Matrix(U);
  free_CR_Matrix(S);
  free_CR_Matrix(VT);

  return MInv;
}

//----------------------------------------------------------------------------

// Square root of M (non-destructive)

// M should be square

void square_root_CR_Matrix(CR_Matrix *M, CR_Matrix *Msqrt)
{
  int i;
  CR_Matrix *U, *S, *VT;

  // compute svd of M = U * S * V'
  
  U = make_CR_Matrix("U", M->rows, M->cols);
  S = make_CR_Matrix("S", M->rows, M->cols);
  VT = make_CR_Matrix("VT", M->rows, M->cols);

  square_svd_CR_Matrix(M, U, S, VT);

  // take square root of S's diagonal elements
  // (would it be more efficient to do this with VML on M_diag in SVD before copying?)

  for (i = 0; i < M->rows * M->cols; i += (M->cols + 1))
    S->x[i] = sqrt(S->x[i]);

  // set Msqrt = U * sqrt(S) * V'

  product_CR_Matrix(U, S, Msqrt);
  memcpy(S->x, Msqrt->x, M->buffsize);
  product_CR_Matrix(S, VT, Msqrt);

  // clean up

  free_CR_Matrix(U);
  free_CR_Matrix(S);
  free_CR_Matrix(VT);
}

//----------------------------------------------------------------------------

// |M| (non-destructive)
// returns TRUE if successful, FALSE otherwise

double determinant_CR_Matrix(CR_Matrix *M)
{
  int *ipiv;
  double *M_copy;
  int info;
  double det;
  int i;

  // set up

  ipiv = (int *) CR_calloc(M->rows, sizeof(int));
  M_copy = (double *) CR_malloc(M->buffsize);

  // copy M so that it's not destroyed

  memcpy(M_copy, M->x, M->buffsize);

  // in-place LU factorization

  //xxx  DGETRF(&M->rows, &M->cols, M_copy, &M->rows, ipiv, &info);
  dgetrf_((integer *) &M->rows, (integer *) &M->cols, M_copy, (integer *) &M->rows, (integer *) ipiv, (integer *) &info);
  if (info)
    CR_error("determinant_CR_Matrix(): bad LU factorization");

  // determinant is product of diagonal elements

  for (i = 0, det = 1.0; i < M->rows * M->cols; i += (M->cols + 1))
    det *= M_copy[i];

  // clean up

  CR_free_calloc(ipiv, M->rows, sizeof(int));
  CR_free_malloc(M_copy, M->buffsize);
 
  return det;
}

//----------------------------------------------------------------------------

// M2 = M1^-1 (non-destructive)
// returns TRUE if successful, FALSE otherwise

int inverse_CR_Matrix(CR_Matrix *M1, CR_Matrix *M2)
{
  //  CR_error("11 removed on Linux -- no MKL");

  int *ipiv;
  double *work;
  int lwork;
  int info;

  // set up

  ipiv = (int *) CR_calloc(M1->rows, sizeof(int));
  lwork = 64 * M1->rows;  // 16, 32, or 64 are recommended coefficients here--64 most generous
  work = (double *) CR_calloc(lwork, sizeof(double));

  // copy M1 into M2 so that M1 is not destroyed

  memcpy(M2->x, M1->x, M1->buffsize);

  // in-place LU factorization

  dgetrf_((integer *) &M2->rows, (integer *) &M2->cols, M2->x, (integer *) &M2->rows, (integer *) ipiv, (integer *) &info);
  if (info)
    return FALSE;

  // inverse of LU-factored matrix

  dgetri_((integer *) &M2->rows, M2->x, (integer *) &M2->rows, (integer *) ipiv, work, (integer *) &lwork, (integer *) &info);
  if (info)
    return FALSE;

  // clean up

  CR_free_calloc(ipiv, M1->rows, sizeof(int));
  CR_free_calloc(work, lwork, sizeof(double));

  return TRUE;
}

CR_Matrix *inverse_CR_Matrix(CR_Matrix *M)
{
  CR_Matrix *InvM;

  InvM = copy_header_CR_Matrix(M);
  inverse_CR_Matrix(M, InvM);

  return InvM;
  /*
  InvM = svd_inverse_CR_Matrix(M);

  print_CR_Matrix(M);
  print_CR_Matrix(InvM);
  */
  return InvM;
}

//----------------------------------------------------------------------------

// N-dimensional Gaussian evaluation coefficient

// calculate alpha = (2 * PI)^(-N/2) * det(MCov)^(-1/2) 

double gaussian_alpha_CR_Matrix(CR_Matrix *MCov)
{
  return pow(TWOPI, -0.5 * (double) MCov->rows) / sqrt(determinant_CR_Matrix(MCov));
  //return pow(TWOPI, -0.5 * (double) MCov->rows) / 1.0;
}

//----------------------------------------------------------------------------

// Compute all eigenvalues and eigenvectors of a symmetric matrix

void eig_CR_Matrix(CR_Matrix *M, CR_Matrix *Evals, CR_Matrix *Evects)
{
//   char job, uplo;
//   int lwork, k;
//   double *work;
// 
//   k = log(M->rows);
//   lwork = 3 * SQUARE(M->rows) + (5 * 2 * k) * M->rows + 1;
//   work = (double *) CR_calloc(lwork, sizeof(double));
//   memcpy(Evects->x, M->x, M->buffsize);
// 
//   job = 'V';
//   uplo = 'U';
//   DSYEVD(&job, &uplo, M->rows, Evects->x, M->rows, Evals->x, );
// 
//   // clean up
// 
//   CR_free_calloc(work, lwork, sizeof(double));
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// Polak-Ribiere (pr == TRUE) or Fletcher-Reeves (pr == FALSE) conjugate gradient

// Given a starting point p[0, ..., n - 1], Fletcher-Reeves-Polak-Ribiere 
// conjugate gradient minimization is performed on a function func, using its 
// gradient as calculated by a routine dfunc.  The convergence tolerance on 
// the function value is input as ftol.  Returned quantities are p (the 
// location of the minimum), iter (the number of iterations that were performed), 
// and fret (the minimum value of the function).  The routine linmin is called 
// to perform line minimization.

// (from Numerical Recipes in C)

void frprmn_CR_Matrix(int pr, CR_Vector *p, double ftol, int *iter, double *fret, double (*func)(CR_Vector *), void (*dfunc)(CR_Vector *, CR_Vector *))
{
  int j, its, n;
  double gg, gam, fp, dgg;
  CR_Vector *g, *h, *xi;

  n = p->rows;
  g = make_CR_Vector("g", n);
  h = make_CR_Vector("h", n);
  xi = make_CR_Vector("xi", n);
  fp = (*func)(p);
  (*dfunc)(p, xi);
  //  CR_exit(1);
  for (j = 0; j < n; j++) {
    g->x[j] = -xi->x[j];
    xi->x[j] = h->x[j] = g->x[j];
  }
  for (its = 1; its <= ITMAX; its++) {
    printf("here\n");
    *iter = its;
    linmin_CR_Vector(p, xi, fret, func);
    printf("done\n");
    if (2.0 * fabs(*fret - fp) <= ftol * (fabs(*fret) + fabs(fp) + EPS)) {
      CR_FREEALL
	return;
    }
    fp = (*func)(p);
    (*dfunc)(p, xi);
    dgg = gg = 0.0;
    for (j = 0; j < n; j++) {
      gg += g->x[j] * g->x[j];  
      if (pr)
	dgg += (xi->x[j] + g->x[j]) * xi->x[j];  // Polak-Ribiere 
      else
	dgg += xi->x[j] * xi->x[j]; // Fletcher-Reeves
    }
    if (gg == 0.0) {
      CR_FREEALL
	return;
    }
    gam = dgg / gg;
    for (j = 0; j < n; j++) {
      g->x[j] = -xi->x[j];
      xi->x[j] = h->x[j] = g->x[j] + gam * h->x[j];
    }
  }
  CR_error("frprmn_CR_Matrix(): too many iterations");
}

//----------------------------------------------------------------------------

// Minimization of a function func of n variables.  Input consists of an initial
// starting point p[0, ..., n - 1], an initial matrix xi[0, ..., n - 1][0, ..., n - 1],
// whose columns contain the initial set of directions (usually the n unit vectors);
// and ftol, the fractional tolerance in the function value such that failure to decrease
// by more than this amount on one iteration signals doneness.  On output, p is set to the
// best point found, xi is the then-current direction set, fret is the returned function 
// value at p, and iter is the number of iterations taken.  

// (from Numerical Recipes in C)

void powell_CR_Matrix(CR_Vector *p, CR_Matrix *xi, double ftol, int *iter, double *fret, double (*func)(CR_Vector *))
{
  int i, ibig, j, n;
  double del, fp, fptt, t;
  CR_Vector *pt, *ptt, *xit;

  //  CR_flush_printf("powell\n");

  n = p->rows;
  pt = make_CR_Vector("pt", n);
  ptt = make_CR_Vector("ptt", n);
  xit = make_CR_Vector("xit", n);
  *fret = (*func)(p);
  for (j = 0; j < n; j++) 
    pt->x[j] = p->x[j];
  for (*iter = 1; ; ++(*iter)) {
    //    printf("iter = %i\n", *iter);
    fp = (*fret);
    ibig = 0;
    del = 0.0;
    for (i = 0; i < n; i++) {
      for (j = 0; j < n; j++) 
	xit->x[j] = MATRC(xi, j, i);
      fptt = (*fret);
      //      printf("i = %i\n", i);
      linmin_CR_Vector(p, xit, fret, func);
      //      printf("after\n");
      if (fabs(fptt - (*fret)) > del) {
	del = fabs(fptt - (*fret));
	ibig = i;
      }
    }
    if (2.0 * fabs(fp - (*fret)) <= ftol * (fabs(fp) + fabs(*fret))) {
      free_CR_Vector(xit);
      free_CR_Vector(ptt);
      free_CR_Vector(pt);
      return;
    }
    if (*iter == ITMAX) 
      CR_error("powell_CR_Matrix: maximum iterations exceeded");
    for (j = 0; j < n; j++) {
      ptt->x[j] = 2.0 * p->x[j] - pt->x[j];
      xit->x[j] = p->x[j] - pt->x[j];
      pt->x[j] = p->x[j];
    }
    fptt = (*func)(ptt);
    if (fptt < fp) {
      t = 2.0 * (fp - 2.0 * (*fret) + fptt) * SQR(fp - (*fret) - del) - del * SQR(fp - fptt);
      if (t < 0.0) {
	linmin_CR_Vector(p, xit, fret, func);
	for (j = 0; j < n; j++) {
	  MATRC(xi, j, ibig) = MATRC(xi, j, n - 1);
	  MATRC(xi, j, n - 1) = xit->x[j];
	}
      }
    }
  }
}

//----------------------------------------------------------------------------

int mncom;
CR_Vector *mpcom, *mxicom; 
double (*mnrfunc)(CR_Vector *);

void linmin_CR_Vector(CR_Vector *p, CR_Vector *xi, double *fret, double (*func)(CR_Vector *))
{
  int j;
  double xx,xmin,fx,fb,fa,bx,ax;
  
  //  CR_flush_printf("linmin\n");

  mncom = p->rows;
  mpcom = make_CR_Vector("mpcom", p->rows);
  mxicom = make_CR_Vector("mxicom", p->rows);
  mnrfunc = func;
  for (j = 0; j < p->rows; j++) {
    mpcom->x[j] = p->x[j];
    mxicom->x[j] = xi->x[j];
  }
  ax = 0.0;
  xx = 1.0;
  mnbrak(&ax, &xx, &bx, &fa, &fx, &fb, f1dim_CR_Vector);
  *fret = brent(ax, xx, bx, f1dim_CR_Vector, TOL, &xmin);
  for (j = 0; j < p->rows; j++) {
    xi->x[j] *= xmin;
    p->x[j] += xi->x[j];
  }
  free_CR_Vector(mxicom);
  free_CR_Vector(mpcom);
}

//----------------------------------------------------------------------------

// Given a function func and distinct initial points ax and bx, this routine searches in 
// the downhill direction (defined by the function as evaluated at the initial points) and
// returns new points ax, bx, cx that bracket a minimum of the function.  Also returned are the 
// function values at the three points, fa, fb, and fc.

// (from Numerical Recipes in C)

void mnbrak(double *ax, double *bx, double *cx, double *fa, double *fb, double *fc, double (*func)(double))
{
  double ulim,u,r,q,fu,dum;

  //  CR_flush_printf("mnbrak\n");

  *fa=(*func)(*ax);
  //  CR_flush_printf("mnbrak 2\n");
  *fb=(*func)(*bx);
  //  CR_flush_printf("mnbrak 3\n");

  if (*fb > *fa) {
    SHFT(dum,*ax,*bx,dum)
      SHFT(dum,*fb,*fa,dum)
      }
  *cx=(*bx)+GOLD*(*bx-*ax);
  *fc=(*func)(*cx);
  while (*fb > *fc) {
    //    printf("aaargh!\n");
    r=(*bx-*ax)*(*fb-*fc);
    q=(*bx-*cx)*(*fb-*fa);
    u=(*bx)-((*bx-*cx)*q-(*bx-*ax)*r)/
      (2.0*SIGN(FMAX(fabs(q-r),TINY),q-r));
    ulim=(*bx)+GLIMIT*(*cx-*bx);
    if ((*bx-u)*(u-*cx) > 0.0) {
      fu=(*func)(u);
      if (fu < *fc) {
	*ax=(*bx);
	*bx=u;
	*fa=(*fb);
	*fb=fu;
	return;
      } else if (fu > *fb) {
	*cx=u;
	*fc=fu;
	return;
      }
      u=(*cx)+GOLD*(*cx-*bx);
      fu=(*func)(u);
    } else if ((*cx-u)*(u-ulim) > 0.0) {
      fu=(*func)(u);
      if (fu < *fc) {
	SHFT(*bx,*cx,u,*cx+GOLD*(*cx-*bx))
	  SHFT(*fb,*fc,fu,(*func)(u))
	  }
    } else if ((u-ulim)*(ulim-*cx) >= 0.0) {
      u=ulim;
      fu=(*func)(u);
    } else {
      u=(*cx)+GOLD*(*cx-*bx);
      fu=(*func)(u);
    }
    SHFT(*ax,*bx,*cx,u)
      SHFT(*fa,*fb,*fc,fu)
      }
}

//----------------------------------------------------------------------------

// uses globals ncom, pcom, xicom, nrfunc

double f1dim_CR_Vector(double x)
{
  int j;
  double f; 
  CR_Vector *xt;
  
  xt = make_CR_Vector("xt", mncom);
  for (j = 0; j < mncom; j++) 
    xt->x[j]= mpcom->x[j] + x * mxicom->x[j];
  f = (*mnrfunc)(xt);
  free_CR_Vector(xt);

  return f;
}

//----------------------------------------------------------------------------

// Given a function f and a bracketing triplet of abscissas ax, bx, cx, this routine
// isolates the minimum to a fractional precision of about tol using Brent's method.
// The abscissa of the minimum is returned as xmin and the minimum value of f is returned
// as the function value

// (from Numerical Recipes in C)

double brent(double ax, double bx, double cx, double (*f)(double), double tol, double *xmin)
{
  int iter;
  double a,b,d,etemp,fu,fv,fw,fx,p,q,r,tol1,tol2,u,v,w,x,xm;
  double e=0.0;
  
  a=(ax < cx ? ax : cx);
  b=(ax > cx ? ax : cx);
  x=w=v=bx;
  fw=fv=fx=(*f)(x);
  for (iter=1;iter<=ITMAX;iter++) {
    xm=0.5*(a+b);
    tol2=2.0*(tol1=tol*fabs(x)+ZEPS);
    if (fabs(x-xm) <= (tol2-0.5*(b-a))) {
      *xmin=x;
      return fx;
    }
    if (fabs(e) > tol1) {
      r=(x-w)*(fx-fv);
      q=(x-v)*(fx-fw);
      p=(x-v)*q-(x-w)*r;
      q=2.0*(q-r);
      if (q > 0.0) p = -p;
      q=fabs(q);
      etemp=e;
      e=d;
      if (fabs(p) >= fabs(0.5*q*etemp) || p <= q*(a-x) || p >= q*(b-x))
	d=CGOLD*(e=(x >= xm ? a-x : b-x));
      else {
	d=p/q;
	u=x+d;
	if (u-a < tol2 || b-u < tol2)
	  d=SIGN(tol1,xm-x);
      }
    } else {
      d=CGOLD*(e=(x >= xm ? a-x : b-x));
    }
    u=(fabs(d) >= tol1 ? x+d : x+SIGN(tol1,d));
    fu=(*f)(u);
    if (fu <= fx) {
      if (u >= x) a=x; else b=x;
      SHFT(v,w,x,u)
	SHFT(fv,fw,fx,fu)
	} else {
	  if (u < x) a=u; else b=u;
	  if (fu <= fw || w == x) {
	    v=w;
	    w=u;
	    fv=fw;
	    fw=fu;
	  } else if (fu <= fv || v == x || v == w) {
	    v=u;
	    fv=fu;
	  }
	}
  }
  CR_error("brent(): Too many iterations");
  *xmin=x;
  return fx;
}

//----------------------------------------------------------------------------
// Level 2 BLAS and LAPACK
//----------------------------------------------------------------------------

// Fits n-degree polynomial to data points

// Y = [y1, y2, ... , yN]', X = [x1, x2, ... , xN]',
// A = [a0, a1, ... , an]', and
//
// T = [1, x1, x1^2, ... , x1^n ; 
//      1, x2, x2^2, ... , x2^n ; 
//                    .
//                    .
//                    .
//      1, xN, xN^2, ... , xN^n ]

// gets least-squares fit to A of Y = TA

// n = 1 ==> linear, n = 2 ==> quadratic, n = 3 ==> cubic, etc.

CR_Vector *polyfit_CR_Matrix(int n, CR_Vector *X, CR_Vector *Y)
{
  int i, j;
  CR_Matrix *T, *TPlus;
  CR_Vector *A;

  // make T and fill it in

  T = make_CR_Matrix("T", X->rows, n + 1);

  for (j = 0; j < T->rows; j++)
    for (i = 0; i < T->cols; i++)
      MATRC(T, j, i) = pow(X->x[j], (double) i);

  // do the heavy lifting

  TPlus = pseudoinverse_CR_Matrix(T);

  A = make_CR_Vector("A", n + 1);

  product_sum_CR_Matrix(1.0, TPlus, Y, 0.0, NULL, A);

  // clean up

  free_CR_Matrix(T);
  free_CR_Matrix(TPlus);

  // return 

  return A;
}

//----------------------------------------------------------------------------

// this assumes matrices are in row-major form, of course

void vectorize_CR_Matrix(CR_Matrix *M, CR_Vector *V)
{
  memcpy(V->x, M->x, M->buffsize);
}

//----------------------------------------------------------------------------

// y = F(x, z) = A x^2 + B x + C z^2 + D z + E x z + F

void quadratic_surface_fit_CR_Matrix(CR_Vector *X, CR_Vector *Y, CR_Vector *Z, CR_Vector *A)
{
  int i, j;
  CR_Matrix *T, *TPlus;

  // make T and fill it in

  T = make_CR_Matrix("T", X->rows, 6);

  for (j = 0; j < T->rows; j++) {
    MATRC(T, j, 0) = X->x[j] * X->x[j];
    MATRC(T, j, 1) = X->x[j];
    MATRC(T, j, 2) = Z->x[j] * Z->x[j];
    MATRC(T, j, 3) = Z->x[j];
    MATRC(T, j, 4) = X->x[j] * Z->x[j];
    MATRC(T, j, 5) = 1;
  }

  // do the heavy lifting

  TPlus = pseudoinverse_CR_Matrix(T);

  product_sum_CR_Matrix(1.0, TPlus, Y, 0.0, NULL, A);

  // clean up

  free_CR_Matrix(T);
  free_CR_Matrix(TPlus);
}

//----------------------------------------------------------------------------

// Z = A * X

void matrix_vector_product_CR_Matrix(CR_Matrix *A, CR_Vector *X, CR_Vector *Z)
{
  product_sum_CR_Matrix(1.0, A, X, 0.0, NULL, Z);
}

//----------------------------------------------------------------------------

// Z = alpha * A * X + beta * Y

void product_sum_CR_Matrix(double alpha, CR_Matrix *A, CR_Vector *X, double beta, CR_Vector *Y, CR_Vector *Z)
{
  CR_Matrix *AT;

  my_product_sum_CR_Matrix(alpha, A, X, beta, Y, Z);
  return;

  //  CR_error("removed on Linux -- no MKL");

  /*
  CR_error("product_sum_CR_Matrix(): check for correctness, especially with non-square matrices");

  if (Y)
    memcpy(Z->x, Y->x, Y->buffsize);
  AT = transpose_CR_Matrix(A);
  cblas_dgemv(CblasRowMajor, CblasNoTrans, A->rows, A->cols, alpha, AT->x, A->rows, X->x, 1, beta, Z->x, 1);
  //  cblas_dgemv(CblasRowMajor, CblasNoTrans, A->rows, A->cols, alpha, A->x, A->rows, X->x, 1, beta, Z->x, 1);
  free_CR_Matrix(AT);
  */
}

//----------------------------------------------------------------------------

void my_product_sum_CR_Matrix(double alpha, CR_Matrix *A, CR_Vector *X, double beta, CR_Vector *Y, CR_Vector *Z)
{
  int i, j;
  double sum;

  memset(Z->x, 0, Z->buffsize);
  
  if (Y) {
    for (j = 0; j < A->rows; j++) {
      for (i = 0, sum = 0; i < A->cols; i++)
	sum += MATRC(A, j, i) * X->x[i];
      Z->x[j] = alpha * sum + beta * Y->x[j];
    }
  }
  else {
    for (j = 0; j < A->rows; j++) {
      for (i = 0, sum = 0; i < A->cols; i++)
	sum += MATRC(A, j, i) * X->x[i];
      Z->x[j] = alpha * sum;
    }
  }
}

//----------------------------------------------------------------------------

// Mahalanobis distance

// dist^2 = (V - VMean)' * MInvCov * (V - VMean) (non-destructive)

// note that a square root is taken at the end, which is not the case in
// the exponent of a Gaussian evaluation

double mahalanobis_CR_Matrix(CR_Vector *V, CR_Vector *VMean, CR_Matrix *MInvCov)
{
  CR_error("12 removed on Linux -- no MKL");

  /*
  double *V_copy, *VMean_copy;
  double result;

  // copy vectors

  V_copy = (double *) CR_malloc(V->buffsize);
  VMean_copy = (double *) CR_malloc(VMean->buffsize);

  memcpy(V_copy, V->x, V->buffsize);
  memcpy(VMean_copy, VMean->x, VMean->buffsize);

  // put (V - VMean) into V_copy

  cblas_daxpy(VMean->rows, -1.0, VMean_copy, 1, V_copy, 1);

  // multiply MInvCov by (V - VMean) -- put result into VMean_copy

  cblas_dgemv(CblasRowMajor, CblasNoTrans, MInvCov->rows, MInvCov->cols, 1.0, MInvCov->x, MInvCov->rows, V_copy, 1, 0.0, VMean_copy, 1);

  // answer = sqrt of dot product of V_copy (V - VMean) and VMean_copy (MInvCov * (V - VMean))

  result = sqrt(cblas_ddot(V->rows, V_copy, 1, VMean_copy, 1));

  // clean up and return

  CR_free_malloc(V_copy, V->buffsize);
  CR_free_malloc(VMean_copy, VMean->buffsize);

  return result;
  */
}

//----------------------------------------------------------------------------

// N-dimensional Gaussian evaluation

// result = alpha * exp(-0.5 * (V - VMean)' * MInvCov * (V - VMean)),
// where alpha = (2 * PI)^(-N/2) * det(MCov)^(-1/2) 

double gaussian_CR_Matrix(double alpha, CR_Vector *V, CR_Vector *VMean, CR_Matrix *MInvCov)
{
  CR_error("13 removed on Linux -- no MKL");

  /*
  double *V_copy, *VMean_copy;
  double result;

  // copy vectors

  V_copy = (double *) CR_malloc(V->buffsize);
  VMean_copy = (double *) CR_malloc(VMean->buffsize);

  memcpy(V_copy, V->x, V->buffsize);
  memcpy(VMean_copy, VMean->x, VMean->buffsize);

  // put (V - VMean) into V_copy

  cblas_daxpy(VMean->rows, -1.0, VMean_copy, 1, V_copy, 1);

  // multiply MInvCov by (V - VMean) -- put result into VMean_copy

  cblas_dgemv(CblasRowMajor, CblasNoTrans, MInvCov->rows, MInvCov->cols, 1.0, MInvCov->x, MInvCov->rows, V_copy, 1, 0.0, VMean_copy, 1);

  // answer = sqrt of dot product of V_copy (V - VMean) and VMean_copy (MInvCov * (V - VMean))

  result = cblas_ddot(V->rows, V_copy, 1, VMean_copy, 1);

  // clean up and return

  CR_free_malloc(V_copy, V->buffsize);
  CR_free_malloc(VMean_copy, VMean->buffsize);

  return alpha * exp(-0.5 * result);
  */
}

//----------------------------------------------------------------------------

// Generate N-dimensional sample from Gaussian distribution

void sample_gaussian_CR_Matrix(double alpha, CR_Vector *V, CR_Vector *VMean, CR_Matrix *MCovSqrt)
{

// Z = alpha * A * X + beta * Y

  CR_Vector *Unit;
  int i;

  Unit = make_CR_Vector("Unit", V->rows);
  for (i = 0; i < V->rows; i++) 
    Unit->x[i] = normal_CR_Random();
  product_sum_CR_Matrix(1.0, MCovSqrt, Unit, 1.0, VMean, V);
  free_CR_Vector(Unit);

  //  CR_error("THIS ONE ACTUALLY GETS USED... removed on Linux -- no MKL");

  /*
  int i;
  double *unit;

  unit = (double *) CR_malloc(V->buffsize);

  for (i = 0; i < V->rows; i++) 
    unit[i] = normal_CR_Random();

  // copy vector

  memcpy(V->x, VMean->x, VMean->buffsize);

  // multiply MCovSqrt by unit and add to V (copy of VMean) -- result is put in V

  cblas_dgemv(CblasRowMajor, CblasNoTrans, MCovSqrt->rows, MCovSqrt->cols, 1.0, MCovSqrt->x, MCovSqrt->rows, unit, 1, 1.0, V->x, 1);

  // clean up

  CR_free_malloc(unit, V->buffsize);
  */
}

//----------------------------------------------------------------------------

// Mahalanobis distance

// specialized for 3 x 3 covariance, 3 x 1 mean -- i.e., RGB space, XYZ space
// (would it be more efficient to not do the sqrt here in the assumption that
// this function is getting called repeatedly to fill up a vector somewhere,
// then use VML to sqrt everything in that vector once it's full?)

double mahalanobis_3x3_CR_Matrix(double *V, CR_Vector *VMean, CR_Matrix *MInvCov)
{
  double diff1, diff2, diff3, a1, a2, a3;
  double *MInvCov_x;
  double result;

  MInvCov_x = MInvCov->x;

  diff1 = V[0] - VMean->x[0];
  diff2 = V[1] - VMean->x[1];
  diff3 = V[2] - VMean->x[2];
  
  a1 = diff1 * MInvCov_x[0] + diff2 * MInvCov_x[3] + diff3 * MInvCov_x[6];
  a2 = diff1 * MInvCov_x[1] + diff2 * MInvCov_x[4] + diff3 * MInvCov_x[7];
  a3 = diff1 * MInvCov_x[2] + diff2 * MInvCov_x[5] + diff3 * MInvCov_x[8];

  return sqrt(a1 * diff1 + a2 * diff2 + a3 * diff3);
}

//----------------------------------------------------------------------------

// Covariance matrix

// specialized for 3x3 covariance matrix
// variables are columns, trials are rows 

void mean_and_covariance_nx3_CR_Matrix(CR_Vector *V0, CR_Vector *V1, CR_Vector *V2, CR_Vector *Mean, CR_Matrix *Cov)
{
  int i;
  CR_Vector *Var0, *Var1, *Var2;
  double colvar0, colvar1, colvar2, sum0, sum1, sum2, dot01, dot02, dot12;

#ifdef CR_MATRIX_DEBUG
  if (V0->rows <= 1)
    CR_error("mean_and_covariance_nx3_CR_Matrix(): variance undefined for only 1 data point");
  if (Cov->rows != 3 || Cov->cols != 3)
    CR_error("mean_and_covariance_nx3_CR_Matrix(): Cov must have same number of 3 rows, cols");
#endif

//   print_CR_Vector(V0);
//   print_CR_Vector(V1);
//   print_CR_Vector(V2);
//   print_CR_Vector(Mean);
//   print_CR_Matrix(Cov);

  sum0 = sum_of_terms_CR_Vector(V0);
  sum1 = sum_of_terms_CR_Vector(V1);
  sum2 = sum_of_terms_CR_Vector(V2);
  Mean->x[0] = sum0 / (double) V0->rows;
  Mean->x[1] = sum1 / (double) V0->rows;
  Mean->x[2] = sum2 / (double) V0->rows;

  Var0 = make_CR_Vector("Var0", V0->rows);
  Var1 = make_CR_Vector("Var1", V0->rows);
  Var2 = make_CR_Vector("Var2", V0->rows);

  for (i = 0; i < V0->rows; i++) {
    Var0->x[i] = (Mean->x[0] - V0->x[i]) * (Mean->x[0] - V0->x[i]);
    Var1->x[i] = (Mean->x[1] - V1->x[i]) * (Mean->x[1] - V1->x[i]);
    Var2->x[i] = (Mean->x[2] - V2->x[i]) * (Mean->x[2] - V2->x[i]);
  }

  colvar0 = sum_of_terms_CR_Vector(Var0) / (double) (V0->rows - 1);
  colvar1 = sum_of_terms_CR_Vector(Var1) / (double) (V0->rows - 1);
  colvar2 = sum_of_terms_CR_Vector(Var2) / (double) (V0->rows - 1);

  // fill diagonal with variances

  MATRC(Cov, 0, 0) = colvar0;
  MATRC(Cov, 1, 1) = colvar1;
  MATRC(Cov, 2, 2) = colvar2;

  // fill in off-diagonal elements

  dot01 = dotproduct_CR_Vector(V0, V1);
  dot02 = dotproduct_CR_Vector(V0, V2);
  dot12 = dotproduct_CR_Vector(V1, V2);

  MATRC(Cov, 0, 1) = (((double) V0->rows) * dot01 - sum0 * sum1) / ((double) (V0->rows * (V0->rows - 1)));
  MATRC(Cov, 0, 2) = (((double) V0->rows) * dot02 - sum0 * sum2) / ((double) (V0->rows * (V0->rows - 1)));
  MATRC(Cov, 1, 2) = (((double) V0->rows) * dot12 - sum1 * sum2) / ((double) (V0->rows * (V0->rows - 1)));
  MATRC(Cov, 1, 0) = MATRC(Cov, 0, 1);
  MATRC(Cov, 2, 0) = MATRC(Cov, 0, 2);
  MATRC(Cov, 2, 1) = MATRC(Cov, 1, 2);

  // clean up

  free_CR_Vector(Var0);
  free_CR_Vector(Var1);
  free_CR_Vector(Var2);
}

//----------------------------------------------------------------------------
// Level 1 BLAS and VML
//----------------------------------------------------------------------------

// Subtract one vector from another: V3 = V1 - V2

void difference_CR_Vector(CR_Vector *V1, CR_Vector *V2, CR_Vector *V3)
{
  CR_error("removed on Linux -- no MKL");

  /*
  copy_CR_Vector(V1, V3);
  cblas_daxpy(V1->rows, -1, V2->x, 1, V3->x, 1);
  */
}

//----------------------------------------------------------------------------

void sum_CR_Vector(CR_Vector *V1, CR_Vector *V2, CR_Vector *V3)
{
  CR_error("removed on Linux -- no MKL");

  /*
  copy_CR_Vector(V1, V3);
  cblas_daxpy(V1->rows, 1, V2->x, 1, V3->x, 1);
  */
}

//----------------------------------------------------------------------------

void sum_CR_Vector_array(CR_Vector **V_array, int n, CR_Vector *Sum)
{
  int i, j;

  for (j = 0; j < Sum->rows; j++) {
    Sum->x[j] = 0;
    for (i = 0; i < n; i++)
      Sum->x[j] += V_array[i]->x[j];
  }

  //  CR_error("THIS ONE ACTUALLY GETS USED...removed on Linux -- no MKL");

  /*
  int i;

  copy_CR_Vector(V_array[0], Sum);

  for (i = 1; i < n; i++)
    cblas_daxpy(Sum->rows, 1, V_array[i]->x, 1, Sum->x, 1);
  */
}

//----------------------------------------------------------------------------

// Index of element of V with maximum absolute value

int max_index_CR_Vector(CR_Vector *V)
{
  CR_error("removed on Linux -- no MKL");

  /*
  return cblas_idamax(V->rows, V->x, 1) - 1;
  */
}

//----------------------------------------------------------------------------

// Index of element of V with minimum absolute value

int min_index_CR_Vector(CR_Vector *V)
{
  CR_error("removed on Linux -- no MKL");

  /*
  return cblas_idamin(V->rows, V->x, 1) - 1;
  */
}

//----------------------------------------------------------------------------

// Sum of absolute values of vector elements

double sum_of_terms_CR_Vector(CR_Vector *V)
{
  double sum;
  int i;

  for (i = 0, sum = 0.0; i < V->rows; i++)
    sum += fabs(V->x[i]);
  return sum;

  //  return cblas_dasum(V->rows, V->x, 1);
}

//----------------------------------------------------------------------------

// Scale vector (destructive)

void scale_CR_Vector(double a, CR_Vector *V)
{
  int i;

  for (i = 0; i < V->rows; i++)
    V->x[i] *= a;

  //  cblas_dscal(V->rows, a, V->x, 1);
}

//----------------------------------------------------------------------------

// Divide elements of one vector by corresponding elements of another (non-destructive)

void divide_CR_Vector(CR_Vector *V1, CR_Vector *V2, CR_Vector *V3)
{
  CR_error("removed on Linux -- no MKL");

  /*
  //  int i;

  //#ifdef BARNEY
  vdDiv(V1->rows, V1->x, V2->x, V3->x);
  //#else
  //  vdDiv(&V1->rows, V1->x, V2->x, V3->x);
  //#endif

  //  for (i = 0; i < V1->rows; i++)
  //    V3->x[i] = V1->x[i] / V2->x[i];
  */
}

//----------------------------------------------------------------------------

// Hyperbolic tangent of every element in a vector (non-destructive)

void tanh_CR_Vector(CR_Vector *V1, CR_Vector *V2)
{
  CR_error("removed on Linux -- no MKL");

  /*
  vdtanh(&V1->rows, V1->x, V2->x);
  */
}

//----------------------------------------------------------------------------

// Natural log of every element in a vector (non-destructive)

void log_CR_Vector(CR_Vector *V1, CR_Vector *V2)
{
  CR_error("removed on Linux -- no MKL");

  /*
  //  int i;

  //#ifdef BARNEY
  vdLn(V1->rows, V1->x, V2->x);
  //#else
  //  vdLn(&V1->rows, V1->x, V2->x);
  //#endif

  //  for (i = 0; i < V1->rows; i++)
  //    V2->x[i] = log(V1->x[i]);
  */
}

//----------------------------------------------------------------------------

// Log base 2 of every element in a vector (non-destructive)

void log2_CR_Vector(CR_Vector *V1, CR_Vector *V2)
{
  CR_error("removed on Linux -- no MKL");

  /*
  //  int i;

  //#ifdef BARNEY
  vdLn(V1->rows, V1->x, V2->x);
  //#else
  //  vdLn(&V1->rows, V1->x, V2->x);
  //#endif
  //  scale_CR_Vector(INV_LN2, V2);

  //  for (i = 0; i < V1->rows; i++)
  //    V2->x[i] = log(V1->x[i]);
  */
}

//----------------------------------------------------------------------------

// Compute square root of every element in a vector (non-destructive)

void sqrt_CR_Vector(CR_Vector *V1, CR_Vector *V2)
{
  CR_error("removed on Linux -- no MKL");

  /*
  //  int i;

  // for testing
  //vmlGetMode();

  //#ifdef BARNEY
  vdSqrt(V1->rows, V1->x, V2->x); 
  //#else
  //  vdSqrt(&V1->rows, V1->x, V2->x); 
  //#endif

  //  for (i = 0; i < V1->rows; i++)
  //    V2->x[i] = sqrt(V1->x[i]);
  */
}

//----------------------------------------------------------------------------

// Raise every element in a vector to the same power (non-destructive)

// Note: there's a VML function that will raise each of V1's elements
// to a power indicated by the corresponding element in V2 and put the
// result in V3, but filling V2 with the same number seems like it might
// slow that routine down below this method.  Should probably check, though.

void pow_CR_Vector(double a, CR_Vector *V1, CR_Vector *V2)
{
  int i;

  for (i = 0; i < V1->rows; i++)
    V2->x[i] = pow(V1->x[i], a);
}

//----------------------------------------------------------------------------

// Raise every element in a vector to the same power (non-destructive)

// VML version of pow_CR_Vector()

void pow2_CR_Vector(double a, CR_Vector *V1, CR_Vector *V2)
{
  CR_error("removed on Linux -- no MKL");

  /*
  double *V_pow;
  int i;

  // set up

  V_pow = (double *) CR_malloc(V1->buffsize);
  for (i = 0; i < V1->rows; i++)
    V_pow[i] = a;

  // do it

  //#ifdef BARNEY
  vdPow(V1->rows, V1->x, V_pow, V2->x); 
  //#else
  //  vdPow(&V1->rows, V1->x, V_pow, V2->x); 
  //#endif

  // clean up

  CR_free_malloc(V_pow, V1->buffsize);
  */
}

//----------------------------------------------------------------------------

// Make a vector

CR_Vector *make_CR_Vector(char *name, int rows)
{
  CR_Vector *V;

  V = (CR_Vector *) CR_malloc(sizeof(CR_Vector));

  V->name = (char *) CR_calloc(CR_MATRIX_MAX_NAME, sizeof(char));
  strncpy(V->name, name, CR_MATRIX_MAX_NAME);

  V->rows = rows;

  V->x = (double *) CR_calloc(rows, sizeof(double));

  V->buffsize = rows * sizeof(double);

  return V;
}

//----------------------------------------------------------------------------

// Make a vector and initialize its values

CR_Vector *init_CR_Vector(char *name, int rows, ...)
{
  int i;
  CR_Vector *V;
  va_list args;

  V = make_CR_Vector(name, rows);
  /*
  V = (CR_Vector *) CR_malloc(sizeof(CR_Vector));

  V->name = (char *) CR_calloc(CR_MATRIX_MAX_NAME, sizeof(char));
  strncpy(V->name, name, CR_MATRIX_MAX_NAME);

  V->rows = rows;

  V->x = (double *) CR_calloc(rows, sizeof(double));

  V->buffsize = rows * sizeof(double);
  */

  // initialize to 0 by default

//   for (i = 0; i < V->rows; i++)
//     V->x[i] = 0;

  // fill in provided values if they exist

  va_start(args, rows);

  for (i = 0; i < V->rows; i++)
    V->x[i] = va_arg(args, double);

  va_end(args);

  return V;
}

//----------------------------------------------------------------------------

// Free vector

void free_CR_Vector(CR_Vector *V)
{
  if (V) {
    CR_free_malloc(V->name, CR_MATRIX_MAX_NAME);
    CR_free_malloc(V->x, V->buffsize);
    CR_free_malloc(V, sizeof(CR_Vector));
  }
}

//----------------------------------------------------------------------------

// Set elements of a vector

void set_CR_Vector(CR_Vector *V, ...)
{
  int i;
  va_list args;

  // initialize

  va_start(args, V);

  for (i = 0; i < V->rows; i++)
    V->x[i] = va_arg(args, double);

  va_end(args);
}

//----------------------------------------------------------------------------

// Set all elements of vector to same value

void set_all_CR_Vector(double x, CR_Vector *V)
{
  int i;

  for (i = 0; i < V->rows; i++)
    V->x[i] = x;
}

//----------------------------------------------------------------------------

// Make a vector with all of its elements set to x

CR_Vector *make_all_CR_Vector(char *name, int rows, double x)
{
  CR_Vector *V;

  V = make_CR_Vector(name, rows);
  set_all_CR_Vector(x, V);

  return V;
}

//----------------------------------------------------------------------------

// Copy vector, including contents (creates new vector)

CR_Vector *copy_CR_Vector(CR_Vector *V)
{
  CR_Vector *V_copy = copy_header_CR_Vector(V);

  memcpy(V_copy->x, V->x, V->buffsize);

  return V_copy;
}

//--------------------------------------

// Copy vector, including contents (overwrites old vector)
// (assumes V1, V2 match sizewise)
// V1 -> V2 (V2 is overwritten)

void copy_CR_Vector(CR_Vector *V1, CR_Vector *V2)
{
  memcpy(V2->x, V1->x, V1->buffsize);
}

//----------------------------------------------------------------------------

// Copy vector without duplicating contents

CR_Vector *copy_header_CR_Vector(CR_Vector *V)
{
  CR_Vector *V_copy;

  V_copy = make_CR_Vector("V_copy", V->rows);

  return V_copy;
}

//----------------------------------------------------------------------------

// Print a vector

void print_CR_Vector(CR_Vector *V)
{
  int r;

  printf("%s = ", V->name);
  printf("[ ");
  for (r = 0; r < V->rows - 1; r++)
    printf("%6.3lf, ", V->x[r]);
  printf("%6.3lf ];\n", V->x[r]);
  //  printf("\n");
}

//----------------------------------------------------------------------------

// Print array of doubles as a vector

void print_as_CR_Vector(char *name, double *x, int rows)
{
  int r;

  printf("%s = ", name);
  printf("[ ");
  for (r = 0; r < rows - 1; r++)
    printf("%6.3lf, ", x[r]);
  printf("%6.3lf ];\n", x[r]);
  printf("\n");
}

//----------------------------------------------------------------------------

void write_excel_CR_Vector(char *filename, CR_Vector *V)
{
  int i;
  FILE *fp;

  fp = fopen(filename, "w");
  for (i = 0; i < V->rows; i++)
    fprintf(fp, "%i\t%lf\n", i, V->x[i]);
  fclose(fp);
}

//----------------------------------------------------------------------------

// Print a vector to a file (that's already open)

void fprint_CR_Vector(FILE *fp, CR_Vector *V)
{
  int r;

  fprintf(fp, "%s = ", V->name);
  fprintf(fp, "[ ");
  for (r = 0; r < V->rows - 1; r++)
    fprintf(fp, "%6.3lf, ", V->x[r]);
  fprintf(fp, "%6.3lf ];\n", V->x[r]);
  fprintf(fp, "\n");
}

//----------------------------------------------------------------------------

// read vector of known size in already open file into already allocated vector

void fread_CR_Vector(FILE *fp, CR_Vector *V)
{
  char Mname[128];
  int i;
  double temp;

  fscanf(fp, "%s = [ ", Mname);
  for (i = 0; i < V->rows - 1; i++) {
    fscanf(fp, "%lf, ", &temp);
    V->x[i] = temp;
  }
  fscanf(fp, "%lf ];\n", &temp);
  V->x[V->rows - 1] = temp;
}

//----------------------------------------------------------------------------

void write_dlm_CR_Vector(char *filename, CR_Vector *V)
{
  FILE *fp;
  int j;

  fp = fopen(filename, "w");

  for (j = 0; j < V->rows; j++) 
    fprintf(fp, "%lf\n", V->x[j]);

  fclose(fp);
}

//----------------------------------------------------------------------------

// read vector in Matlab's dlm format: a1
//                                     a2
//                                     .
//                                     .
//                                     .
//                                     am

// must know dimensions

CR_Vector *read_dlm_CR_Vector(char *filename, int rows)
{
  CR_Vector *V;
  FILE *fp;
  double temp;
  int j;

  fp = fopen(filename, "r");

  V = make_CR_Vector("V", rows);

  for (j = 0; j < rows; j++) {
    fscanf(fp, "%lf\n", &temp);
    V->x[j] = temp;
  }

  fclose(fp);

  return V;
}

void read_dlm_CR_Vector(char *filename, CR_Vector *V)
{
  FILE *fp;
  double temp;
  int j;

  fp = fopen(filename, "r");

  for (j = 0; j < V->rows - 1; j++) {
    fscanf(fp, "%lf,", &temp);
    V->x[j] = temp;
  }
  fscanf(fp, "%lf", &temp);
  V->x[V->rows - 1] = temp;

  fclose(fp);
}

//----------------------------------------------------------------------------

double magnitude_CR_Vector(CR_Vector *V)
{
  CR_error("removed on Linux -- no MKL");

  /*
  CR_error("magnitude_CR_Vector(): make sure this is doing what you expect--it may not be");
  return cblas_dnrm2(V->rows, V->x, 1);
  */
}

//----------------------------------------------------------------------------

// assuming their sizes match

double dotproduct_CR_Vector(CR_Vector *V1, CR_Vector *V2)
{
  int i;
  double sum;

  for (i = 0, sum = 0.0; i < V1->rows; i++)
    sum += V1->x[i] * V2->x[i];
  return sum;

  //  return cblas_ddot(V1->rows, V1->x, 1, V2->x, 1);
}

//----------------------------------------------------------------------------

void randperm_CR_Vector(CR_Vector *V)
{
  int i, j;
  double temp;

  for (i = 0; i < V->rows; i++)
    V->x[i] = (double) i;

  for (i = 0; i < V->rows; i++) {
    j = ranged_uniform_int_CR_Random(i, V->rows - 1);
    temp = V->x[i];
    V->x[i] = V->x[j];
    V->x[j] = temp;
  }
}

//----------------------------------------------------------------------------

int sort_cmp(const void *left, const void *right)
{
  double leftval, rightval;

  leftval = ((double *) left)[0];
  rightval = ((double *) right)[0];

  if (leftval < rightval)
    return -1;
  else if (leftval > rightval)
    return 1;
  else 
    return 0;
}

void sort_CR_Vector(CR_Vector *V)
{
  qsort((void *) V->x, V->rows, sizeof(double), sort_cmp);
}

//----------------------------------------------------------------------------

double median_CR_Vector(CR_Vector *V)
{
//   printf("before\n");
//   print_CR_Vector(V);

  sort_CR_Vector(V);

//   printf("after\n");
//   print_CR_Vector(V);

  // odd

  if (!(V->rows % 2)) {
    //    printf("even\n");
    return 0.5 * (V->x[V->rows / 2 - 1] + V->x[V->rows / 2]);
  }

  // even 

  else {
    //    printf("odd\n");
    return V->x[(V->rows - 1) / 2];
  }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------


