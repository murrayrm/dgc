//----------------------------------------------------------------------------
//
//  Linux robot library
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "firewireCamera.hh"
 
//----------------------------------------------------------------------------

// Digital Camera spec information (libdc1394)

int numPorts = MAX_PORTS;
raw1394handle_t handles[MAX_CAMERAS];
dc1394_cameracapture cameras[MAX_CAMERAS];
nodeid_t *camera_nodes;
dc1394_feature_set features;
int numCams = 0;

int width, height;

//int fps = FRAMERATE_30;
int fps = FRAMERATE_15;
int camera_mode;

const char *device_name = "/dev/video1394/0";

int camCount;

//----------------------------------------------------------------------------

void clear_channel(void)
{
  for (int channel = 0; channel < MAX_CAMERAS; channel++) 
    ioctl(open(device_name,O_RDONLY),VIDEO1394_IOC_UNLISTEN_CHANNEL,&channel);
}

//----------------------------------------------------------------------------

// returns number of cameras attached; 0 indicates failure (possibly because
// there are no cameras)

int initialize_capture_firewire(int *w, int *h, int mode)
{
  unsigned int channel;
  unsigned int speed;
  raw1394handle_t raw_handle;
  struct raw1394_portinfo ports[MAX_PORTS];
  int p, i;

  camera_mode = mode;

  if (mode == MODE_640x480_MONO || mode == MODE_640x480_YUV422) {
    width = 640;
    height = 480;
  }
  else if (mode == MODE_320x240_YUV422) {
    width = 320;
    height = 240;
  }

  *w = width;
  *h = height;

  
  clear_channel();

  // get the number of ports (cards) 
  
  raw_handle = raw1394_new_handle();
  if (raw_handle==NULL) {
    printf("Unable to aquire a raw1394 handle\n");
    printf("did you load the drivers?\n");
    return 0;
  }
  numPorts = raw1394_get_port_info(raw_handle, ports, numPorts);
  raw1394_destroy_handle(raw_handle);
  
  printf("number of firewire ports = %d\n", numPorts);

  ////////////////////////////////////////////////
  //
  // * Get dc1394 handle to each port
  // * For each camera found, set up for capture by 
  //    * Snagging the ISO channel/speed
  //    * Configuring the channel for DMA x-fer
  //    * Start ISO x-fers
  //

  for (p = 0; p < numPorts; p++) {
            
    // Get the camera nodes and describe them as we find them 
    
    raw_handle = raw1394_new_handle();
    raw1394_set_port( raw_handle, p );
    camera_nodes = dc1394_get_camera_nodes(raw_handle, &camCount, 0);
    raw1394_destroy_handle(raw_handle);
    
    printf("Number of cameras = %i on port %i\n", camCount, p);
    
    // setup cameras for capture 

    for (i = 0; i < camCount; i++) {	

      printf("setting up %i\n", numCams);

      handles[numCams] = dc1394_create_handle(p);
      if (handles[numCams]==NULL) {
	printf("Unable to aquire a raw1394 handle\n");
	cleanup_capture_firewire();
	return 0;
      }
      
      cameras[numCams].node = camera_nodes[i];
      
      if(dc1394_get_camera_feature_set(handles[numCams], 
				       cameras[numCams].node, 
				       &features) !=DC1394_SUCCESS) {
	printf("unable to get feature set\n");
	cleanup_capture_firewire();
	return 0;
      }
      
      //      dc1394_print_feature_set(&features);
      
      if (dc1394_get_iso_channel_and_speed(handles[numCams], 
					   cameras[numCams].node,
					   &channel, &speed) 
	  != DC1394_SUCCESS) {
	printf("unable to get the iso channel number\n");
	cleanup_capture_firewire();
	return 0;
      }

      if (dc1394_dma_setup_capture(handles[numCams], 
				   cameras[numCams].node, 
				   i+1, // channel
 				   FORMAT_VGA_NONCOMPRESSED, 
				   mode,
  				   SPEED_400, 
				   fps, 
				   NUM_BUFFERS, 
				   //				   0, 
				   DROP_FRAMES,
  				   device_name, 
				   &cameras[numCams]) 
	  != DC1394_SUCCESS)  {
 	printf("unable to setup camera- check line %d of %s to make sure\n", __LINE__,__FILE__);
	printf("that the video mode,framerate and format are supported is one supported by your camera\n");
	cleanup_capture_firewire();
	return 0;
      }
      
      // Have the camera start sending us data

      if (dc1394_start_iso_transmission(handles[numCams], cameras[numCams].node) !=DC1394_SUCCESS) {
	printf("unable to start camera iso transmission\n");
	cleanup_capture_firewire();
	return 0;
      }

      numCams++;
    }

    dc1394_free_camera_nodes(camera_nodes);
  }

  //  return camCount;
  return numCams;
}

//----------------------------------------------------------------------------

// call once at end of program

void cleanup_capture_firewire()
{
  int i;

  for (i=0; i < numCams; i++) {
    dc1394_dma_unlisten( handles[i], &cameras[i] );
    dc1394_dma_release_camera( handles[i], &cameras[i]);
    dc1394_destroy_handle(handles[i]);
  }

  //  exit(1);
}

//----------------------------------------------------------------------------

// captures into buffer without YUV conversion

int capture_raw_firewire_image(char *buffer, int size, int camNum)
{
  if (camNum < 0 || camNum >= camCount) {
    printf("invalid camera number\n");
    return 0;
  }

  // captures all attached cameras with one call

  //  dc1394_dma_multi_capture(cameras, numCams);

  // capture just one specific camera

  //dc1394_dma_single_capture(&cameras[camNum]);

  // assuming YUV422 is capture format

  memcpy((unsigned char *) buffer, (unsigned char *)cameras[camNum].capture_buffer, size); 

  // unlock the capture buffer

  if (dc1394_dma_done_with_buffer(&cameras[camNum]) < 0)
    perror("DWB failed:");

  return 1;

}

//----------------------------------------------------------------------------

// captures and converts from YUV to RGB

int capture_firewire_image(IplImage *im, int camNum)
{
  printf("trying to capture %i (%i)\n", camNum, numCams);
  fflush(stdout);

  if (camNum < 0 || camNum >= camCount) {
    printf("invalid camera number\n");
    return 0;
  }

  // captures all attached cameras with one call

  dc1394_dma_multi_capture(cameras, numCams);

  // capture just one specific camera

  //dc1394_dma_single_capture(&cameras[camNum]);

  printf("a\n");
  fflush(stdout);

  // assuming YUV422 is capture format

  uyvy2rgb ((unsigned char *)cameras[camNum].capture_buffer, (unsigned char *) im->imageData, im->width * im->height);

  printf("b\n");
  fflush(stdout);

  // unlock the capture buffer

  if (dc1394_dma_done_with_buffer(&cameras[camNum]) < 0)
    perror("DWB failed:");

  printf("leaving capture\n");
  fflush(stdout);

  return 1;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
