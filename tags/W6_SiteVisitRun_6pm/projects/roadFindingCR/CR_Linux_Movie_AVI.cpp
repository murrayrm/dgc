//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_Linux_Movie_AVI.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Movie *open_CR_Movie(char *filename)
{
  int movie_type;
  CR_Movie *mov;

  //  printf("%s\n", filename);

  // get filename

  movie_type = determine_type_CR_Movie(filename);
  if (movie_type == CR_MOVIE_AVI)
    mov = read_avi_to_CR_Movie(filename);
  else 
    CR_error("can't yet handle that kind of movie");

  printf("opened %s; w = %i, h = %i\n", filename, mov->width, mov->height);

  // set up

  mov->name = (char *) CR_calloc(CR_MAX_FILENAME, sizeof(char));
  strcpy(mov->name, filename);

  mov->currentframe = 0;

  // finish

  return mov;
}

//----------------------------------------------------------------------------

// free resources and close file

void close_CR_Movie(CR_Movie *mov)
{
  int len;

  CR_error("no close");

//xxx   if (mov->type == CR_MOVIE_AVI) {
//xxx     AVIFileRelease(mov->AVI_pAviFile);    
//xxx   }

  cvReleaseImage(&mov->im);
  //  free_CR_Image(mov->im);
  CR_free_calloc(mov->name, CR_MAX_FILENAME, sizeof(char));
  CR_free_malloc(mov, sizeof(CR_Movie));
}

//----------------------------------------------------------------------------

int determine_type_CR_Movie(char *filename)
{
  char suffix[4];
  int len;

  len = strlen(filename);
  suffix[0] = filename[len - 3];
  suffix[1] = filename[len - 2];
  suffix[2] = filename[len - 1];
  suffix[3] = '\0';

  if (!strcmp(suffix, "avi") || !strcmp(suffix, "AVI"))
    return CR_MOVIE_AVI;

  // Unknown

  else 
    return CR_UNKNOWN;
}

/*
//----------------------------------------------------------------------------

// rate is frames per second

CR_Movie *write_avi_initialize_CR_Movie(char *filename, int width, int height, int rate)
{
  CR_Movie *mov;
  AVISTREAMINFO streaminfo;
  int buffsize;
  AVICOMPRESSOPTIONS opts;
  AVICOMPRESSOPTIONS FAR *aopts[1] = {&opts};

  // initialize movie

  mov = (CR_Movie *) CR_malloc(sizeof(CR_Movie));
  mov->type = CR_MOVIE_AVI;
  mov->interleaved_DV = FALSE;

  mov->AVI_pAviFile = NULL;
  mov->AVI_pVideo = NULL;
  mov->compressed_AVI_pVideo = NULL;

  // allocate space for an image-sized DIB

  buffsize = 3 * width * height;

  mov->pBmpInfoHeader = (LPBITMAPINFOHEADER) CR_malloc(sizeof(BITMAPINFOHEADER) + buffsize);

  mov->pBmpInfoHeader->biSize = sizeof(BITMAPINFOHEADER);
  mov->pBmpInfoHeader->biWidth = width;
  mov->pBmpInfoHeader->biHeight = height;
  mov->pBmpInfoHeader->biPlanes = 1;
  mov->pBmpInfoHeader->biBitCount = 24;
  mov->pBmpInfoHeader->biCompression = 0;  // same as BI_RGB?
  mov->pBmpInfoHeader->biSizeImage = buffsize;
  mov->pBmpInfoHeader->biClrUsed = 0;
  mov->pBmpInfoHeader->biClrImportant = 0;

  // initialize AVI file

  AVIFileInit();

  //  if (AVIFileOpen(&mov->AVI_pAviFile, filename, OF_WRITE | OF_CREATE, NULL))

  if (!filename) {  // get filename from user later; use temporary name for now
    if (AVIFileOpen(&mov->AVI_pAviFile, "temp.avi", OF_CREATE, NULL))
      CR_error("write_avi_initialize_CR_Movie(): error opening avi file");
    mov->need_filename = TRUE;
  }
  else {
    if (AVIFileOpen(&mov->AVI_pAviFile, filename, OF_CREATE, NULL))
      CR_error("write_avi_initialize_CR_Movie(): error opening avi file");
    mov->need_filename = FALSE;
  }

  // start video stream

  memset(&streaminfo, 0, sizeof(AVISTREAMINFO));
  streaminfo.fccType = streamtypeVIDEO;
  streaminfo.fccHandler = 0;
  streaminfo.dwScale = 1;
  streaminfo.dwRate = rate;
  streaminfo.dwSuggestedBufferSize = buffsize;
  SetRect(&streaminfo.rcFrame, 0, 0, width, height);

  if (AVIFileCreateStream(mov->AVI_pAviFile, &mov->AVI_pVideo, &streaminfo))
    CR_error("write_avi_initialize_CR_Movie(): error initializing video stream");

  // write video stream info (including compression)

  memset(&opts, 0, sizeof(opts));

  // keep putting up compression dialog box until user picks a codec
  //  while (!AVISaveOptions(NULL, 0, 1, &mov->AVI_pVideo, (LPAVICOMPRESSOPTIONS FAR *) &aopts));

  // automatically choose no compression at all (since other codecs seem not to work)

  memset(&opts, 0, sizeof(AVICOMPRESSOPTIONS));
  opts.dwFlags = 8;

  if (AVIMakeCompressedStream(&mov->compressed_AVI_pVideo, mov->AVI_pVideo, &opts, NULL))
    CR_error("write_avi_initialize_CR_Movie(): error making compressed stream");

  if (AVIStreamSetFormat(mov->compressed_AVI_pVideo, 0, mov->pBmpInfoHeader, mov->pBmpInfoHeader->biSize))
    CR_error("write_avi_initialize_CR_Movie(): error setting stream format");

  return mov;
}

//----------------------------------------------------------------------------

void write_avi_addframe_CR_Movie(int framenumber, CR_Image *im, CR_Movie *mov)
{
  // convert to CR_Image
  
  //  fprintf((FILE *) mov->AVI_pVideo, "hello");

  ip_convert_CR_Image_to_DIB(im, mov->pBmpInfoHeader);
//   ip_convert_DIB_to_CR_Image(mov->pBmpInfoHeader, im);
//   write_CR_Image("xxx.ppm", im);
//   CR_exit(1);

  // write it
  
  //  if (AVIStreamWrite(mov->AVI_pVideo, framenumber, 1, (unsigned char *) (mov->pBmpInfoHeader + mov->pBmpInfoHeader->biSize), mov->pBmpInfoHeader->biSizeImage, AVIIF_KEYFRAME, NULL, NULL))
  if (AVIStreamWrite(mov->AVI_pVideo, framenumber, 1, (LPBYTE) mov->pBmpInfoHeader + mov->pBmpInfoHeader->biSize, mov->pBmpInfoHeader->biSizeImage, AVIIF_KEYFRAME, NULL, NULL))
    CR_error("write_avi_addframe_CR_Movie(): error writing to video stream");
}

//----------------------------------------------------------------------------

void write_avi_finish_CR_Movie(CR_Movie *mov)
{
  char *filename;
  char mvstr[128];
  char tempfilename[128] = "//C/Documents/software/fusion/temp.avi";

  AVIStreamClose(mov->AVI_pVideo);
  AVIStreamClose(mov->compressed_AVI_pVideo);
  AVIFileClose(mov->AVI_pAviFile);

  // maybe do this before we start recording?  it would be so much easier
  if (mov->need_filename) {
//     filename = choose_save_file_dialog_win32("Microsoft AVI File (*.avi)\0\0");
//     printf("filename = %s\n", filename);
//     sprintf(mvstr, "mv %s %s\n", tempfilename, filename);
//     printf("%s\n", mvstr);
//     //    system(mvstr);
  }
}
*/

//----------------------------------------------------------------------------

CR_Movie *read_avi_to_CR_Movie(char *filename)
{
  CR_Movie *mov;

  // initialize movie

  mov = (CR_Movie *) CR_malloc(sizeof(CR_Movie));
  mov->type = CR_MOVIE_AVI;
  mov->interleaved_DV = FALSE;

  mov->AviFile = CreateIAviReadFile(filename);
  mov->AviStream = mov->AviFile->GetStream(0, AviStream::Video);

  mov->info = mov->AviStream->GetStreamInfo();

  // allocate space for current image

  mov->AviStream->GetVideoFormat(&mov->bh, sizeof(mov->bh));

  //  printf("w = %i, h = %i\n", mov->bh.biWidth, mov->bh.biHeight);
  printf("w = %i, h = %i, n = %i\n", mov->info->GetVideoWidth(), mov->info->GetVideoHeight(), mov->info->GetStreamFrames());
  
  mov->width = mov->info->GetVideoWidth();
  mov->height = mov->info->GetVideoHeight();
  mov->im = cvCreateImage(cvSize(mov->width, mov->height), IPL_DEPTH_8U, 3);
  mov->temp_im = cvCreateImage(cvSize(mov->width, mov->height), IPL_DEPTH_8U, 3);

  // length of movie?

  mov->numframes = mov->info->GetStreamFrames();
  mov->currentframe = mov->firstframe = 0;
  mov->lastframe = mov->numframes - 1;

  mov->AviStream->StartStreaming();

  /*
  int i, j, w, h;
  LONG lSize;
  unsigned char *pChunk;
  AVIFILEINFO info;
  PAVISTREAM AVI_pVideo;
  PAVISTREAM AVI_pAudio;
  int res;
  PAVISTREAM AVI_pUnknown;
  AVISTREAMINFO streaminfo;
  int videostream_gotten = FALSE;
  union
  {
    unsigned long i;
    unsigned char c[4];
  } streamtype;

  //  streamtype stype;


  // this stuff should eventually get moved out

//   mov->lasermov = NULL;
//   mov->cameramov = NULL;
//  mov->lasermovframe = 0;

  // initialize AVI file

  AVIFileInit();

  if (AVIFileOpen(&mov->AVI_pAviFile, filename, OF_READ, NULL)) {
    //    CR_error("read_avi_to_CR_Movie(): error opening avi file");
    CR_flush_printf("read_avi_to_CR_Movie(): error opening avi file %s\n", filename);
    exit(1);
  }

  if (AVIFileInfo(mov->AVI_pAviFile, &info, sizeof(info)))
    CR_error("read_avi_to_CR_Movie(): error getting avi file info");

  if (AVIFileGetStream(mov->AVI_pAviFile, &AVI_pUnknown, 0, 0))
    CR_error("read_avi_to_CR_Movie(): Error opening generic stream");

  AVIStreamInfo(AVI_pUnknown, &streaminfo, sizeof(AVISTREAMINFO));

  if (streaminfo.fccType == streamtypeDV) 
    mov->interleaved_DV = TRUE;
  else if (streaminfo.fccType == streamtypeAUDIO) {
    // ignore audio, now read video
    CR_flush_printf("got audio\n");
  }
  else if (streaminfo.fccType == streamtypeVIDEO) {
    //    CR_flush_printf("got video\n");
    AVI_pVideo = AVI_pUnknown;
    videostream_gotten = TRUE;
  } 
  else {
    CR_error("read_avi_to_CR_Movie(): don't know what to do with first stream type read");    
  }

  //  if (AVIFileGetStream(mov->AVI_pAviFile, &AVI_pAudio, streamtypeAUDIO, 0)) 
  //    CR_error("read_avi_to_CR_Movie(): Error opening the audio stream");

  if (!mov->interleaved_DV) {

    if (!videostream_gotten)
      if (AVIFileGetStream(mov->AVI_pAviFile, &AVI_pVideo, streamtypeVIDEO, 0)) 
	CR_error("read_avi_to_CR_Movie(): Error opening the video stream");

    if (AVIStreamReadFormat(AVI_pVideo, AVIStreamStart(AVI_pVideo), NULL, &lSize)) 
      CR_error("read_avi_to_CR_Movie(): Error reading format size");

    pChunk = (unsigned char *) calloc(lSize, sizeof(unsigned char));

    if (AVIStreamReadFormat(AVI_pVideo, AVIStreamStart(AVI_pVideo), pChunk, &lSize)) 
      CR_error("read_avi_to_CR_Movie(): Error reading format");

    LPBITMAPINFO pInfo = (LPBITMAPINFO)pChunk;
    DWORD biSize = pInfo->bmiHeader.biSize;
    //    printf("bisize = %i\n", biSize);

    mov->AVI_pgf = AVIStreamGetFrameOpen(AVI_pVideo, NULL);
    
    if(!mov->AVI_pgf) 
      CR_error("read_avi_to_CR_Movie(): Error opening stream");

    mov->currentframe = mov->firstframe = AVIStreamStart(AVI_pVideo);
    mov->lastframe = AVIStreamEnd(AVI_pVideo);
    mov->numframes = 1 + mov->lastframe - mov->firstframe;

    // allocate space for current image

    mov->width = info.dwWidth;
    mov->height = info.dwHeight;
    mov->im = make_CR_Image(mov->width, mov->height, 0);
  }
  else {
    CR_error("read_avi_to_CR_Movie(): don't know what to do with DV interleaved stream");
  }
  */

  // load first image in

  get_avi_frame_CR_Movie(mov, mov->currentframe);

  // return

  return mov;
}

//----------------------------------------------------------------------------

void get_avi_frame_CR_Movie(CR_Movie *mov, int framenumber)
{
  CImage *cim;

  //  printf("reading\n");
  cim = mov->AviStream->GetFrame(true);
  //  mov->avistream->ReadFrames(mov->im->data, mov->im->buffsize, 1);
//   if (cim)
//     printf("%i\n", cim->Width());
//   else
//     printf("no cim!\n");
//  printf("w = %i, h = %i, depth = %i, type = %i, bpp = %i, bpl = %i, direction = %i\n", cim->Width(), cim->Height(), cim->Depth(), cim->Format(), cim->Bpp(), cim->Bpl(), cim->Direction());


//   if (cim->Format() != 24)    // I think this means the image is RGB, so swap to keep OpenCV happy
  cim->ByteSwap();

  // I believe right now the images are coming in RGB, so color-specific OpenCV functions (which assume BGR) may break

  if (cim->Direction()) {    // this means the image is upside-down
    mov->temp_im->imageData = (char *) cim->m_pPlane[0];
    cvConvertImage(mov->temp_im, mov->im, CV_CVTIMG_FLIP);
  }
  else  
    mov->im->imageData = (char *) cim->m_pPlane[0];

  // only do this for CR_Linux_Image, not CR_OpenCV_Image
  /*

  if (cim->Format() == 24)    // this means the image is BGR
    cim->ByteSwap();

  mov->im->data = cim->m_pPlane[0];
  if (cim->Direction())
    vflip_CR_Image(mov->im);  // flip mov->im vertically
  */

  mov->currentframe++;

  //  CR_error("no get");

  /*
  static LPBITMAPINFOHEADER pBmpInfoHeader;

  if (mov->interleaved_DV)
    CR_error("get_avi_frame_CR_Movie(): can't handle DV yet");

  // extract frame

  pBmpInfoHeader = (LPBITMAPINFOHEADER) AVIStreamGetFrame(mov->AVI_pgf, framenumber);
  
  //   printf("biSize = %i, biPlanes = %i, biCompression = %i, biSizeImage = %i, biClrUsed = %i, biClrImportant = %i, biWidth = %i, biHeight = %i, biBitCount = %i, biXPelsPerMeter = %i, biYPelsPerMeter = %i, lpheadersize = %i, headersize = %i\n", pBmpInfoHeader->biSize, pBmpInfoHeader->biPlanes, pBmpInfoHeader->biCompression, pBmpInfoHeader->biSizeImage, pBmpInfoHeader->biClrUsed, pBmpInfoHeader->biClrImportant, pBmpInfoHeader->biWidth, pBmpInfoHeader->biHeight, pBmpInfoHeader->biBitCount, pBmpInfoHeader->biXPelsPerMeter, pBmpInfoHeader->biYPelsPerMeter, (int) sizeof(LPBITMAPINFOHEADER), (int) sizeof(BITMAPINFOHEADER));

  if (!pBmpInfoHeader)
    CR_error("Error getting frame from stream");

  //  if (pBmpInfoHeader->biBitCount != 24) 
  //    CR_error("AVI must be 24-bit");

  if (pBmpInfoHeader->biHeight < 0) 
    CR_error("expecting origin to be bottom-left corner");

  // convert to IplImage
  //  iplConvertFromDIB(pBmpInfoHeader, mov->im);
  
  // convert to CR_Image
  ip_convert_DIB_to_CR_Image(pBmpInfoHeader, mov->im);

  mov->currentframe = framenumber;

  //  free(pBmpInfoHeader);
  */
}

//----------------------------------------------------------------------------

void get_frame_CR_Movie(CR_Movie *mov, int framenumber)
{
  if (mov->type == CR_MOVIE_AVI)
    get_avi_frame_CR_Movie(mov, framenumber);
  else
    CR_error("get_frame_CR_Movie(): unknown movie type");
}

//----------------------------------------------------------------------------

// constructs a filename of the form "<prefix>num<suffix>", where num
// is formatted to occupy n digits, padded with leading 0's if necessary.
// num must be positive

char *construct_n_digit_filename(int numdigits, int num, char *prefix, char *suffix)
{
  char *result, *zeroes;
  int numzeroes, totlen, i;

  if (num < 0 || num > pow(10, numdigits) - 1) {
    CR_flush_printf("%i\n", num);
    CR_error("construct_n_digit_filename(): number out of range");
  }

  //  printf("pre len = %i, suf len = %i\n", strlen(prefix), strlen(suffix));

  totlen = strlen(prefix) + strlen(suffix) + numdigits + 1;
  result = (char *) CR_calloc(totlen, sizeof(char));
  numzeroes = numdigits - (int) floor(log10((float) num)) - 1;

  if (numzeroes) {
    zeroes = (char *) CR_calloc(numzeroes, sizeof(char));
    for (i = 0; i < numzeroes; i++)
      zeroes[i] = '0';

    sprintf(result, "%s%s%i%s", prefix, zeroes, num, suffix);

    CR_free_calloc(zeroes, numzeroes, sizeof(char));
  }
  else {

    //  CR_flush_printf("prefix = %s, num = %i, suffix = %s, totlen = %i\n", prefix, num, suffix, totlen);
  //  exit(1);

    sprintf(result, "%s%i%s", prefix, num, suffix);
  }

  //  CR_flush_printf("result = %s\n", result);
  //  exit(1);

  //  CR_free_calloc(result, totlen, sizeof(char));

  return result;
}

//----------------------------------------------------------------------------

// constructs a filename of the form "<prefix>num<suffix>", where num
// is formatted to occupy 3 digits, padded with leading 0's if necessary

char *construct_3_digit_filename(int num, char *prefix, char *suffix)
{
  char *result;

  if (num < 0 || num > 999) {
    CR_flush_printf("%i\n", num);
    CR_error("construct_3_digit_filename(): number out of range");
  }

  //  printf("pre len = %i, suf len = %i\n", strlen(prefix), strlen(suffix));

  result = (char *) CR_calloc(strlen(prefix) + strlen(suffix) + 3, sizeof(char));

  if (num < 10)
    sprintf(result, "%s00%i%s", prefix, num, suffix);
  else if (num < 100)
    sprintf(result, "%s0%i%s", prefix, num, suffix);
  else
    sprintf(result, "%s%i%s", prefix, num, suffix);

  return result;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
           
