#include "TrajTalker.h"
#include "DGCutils"

#include <iostream>
using namespace std;

CTrajTalker::CTrajTalker()
{
	m_pDataBuffer = new char[CTraj::getHeaderSize() + TRAJ_MAX_LEN*3*2*sizeof(double)];
}

CTrajTalker::~CTrajTalker()
{
	delete m_pDataBuffer;
}

bool CTrajTalker::SendTraj(int trajSocket, CTraj* pTraj, pthread_mutex_t* pMutex)
{
	int						bytesToSend;
	int						bytesSent;
	int           i;
	char* pBuffer = m_pDataBuffer;

	if(pTraj->getOrder() != 3)
	{
		cerr << "Error: CTrajTalker::SendTraj(): order != 3" << endl;
		return false;
	}

	memcpy(pBuffer, pTraj, CTraj::getHeaderSize());
	pBuffer += CTraj::getHeaderSize();

	for(i=0; i<pTraj->getOrder(); i++)
	{
		memcpy(pBuffer, pTraj->getNdiffarray(i), pTraj->getNumPoints() * sizeof(double));
		pBuffer += pTraj->getNumPoints() * sizeof(double);
		memcpy(pBuffer, pTraj->getEdiffarray(i), pTraj->getNumPoints() * sizeof(double));
		pBuffer += pTraj->getNumPoints() * sizeof(double);
	}

	bytesToSend = 2 * pTraj->getOrder() * pTraj->getNumPoints() * sizeof(double) + CTraj::getHeaderSize();

// 	cerr << "TrajTalker: about to send " << bytesToSend << " bytes" << endl;

	bytesSent = m_skynet.send_msg(trajSocket, m_pDataBuffer, bytesToSend, 0, pMutex);

	if(bytesSent != bytesToSend)
	{
		cerr << "CTrajTalker::SendTraj(): sent " << bytesSent << " bytes while expected to send " << bytesToSend << " bytes" << endl;
		return false;
	}

	return true;
}

bool CTrajTalker::RecvTraj(int trajSocket, CTraj* pTraj, pthread_mutex_t* pMutex)
{
	int						bytesToReceive;
	int						bytesReceived;
	int						i;
	char* pBuffer = m_pDataBuffer;

	if(pTraj->getOrder() != 3)
	{
		cerr << "Error: CTrajTalker::RecvTraj(): order != 3" << endl;
		return false;
	}

	bytesToReceive = CTraj::getHeaderSize() + TRAJ_MAX_LEN*3*2*sizeof(double);
	bytesReceived = m_skynet.get_msg(trajSocket, m_pDataBuffer, bytesToReceive, 0, pMutex);
	if(bytesReceived <= 0)
	{
		cerr << "CTrajTalker::RecvTraj(): skynet error" << endl;
		return false;
	}

// 	cerr << "TrajTalker: received " << bytesReceived << " bytes" << endl;

	memcpy(pTraj, pBuffer, CTraj::getHeaderSize());
	pBuffer += CTraj::getHeaderSize();

	for(i=0; i<pTraj->getOrder(); i++)
	{
		memcpy(pTraj->getNdiffarray(i), pBuffer, pTraj->getNumPoints() * sizeof(double));
		pBuffer += pTraj->getNumPoints() * sizeof(double);
		memcpy(pTraj->getEdiffarray(i), pBuffer, pTraj->getNumPoints() * sizeof(double));
		pBuffer += pTraj->getNumPoints() * sizeof(double);
	}

	return true;
}
