#ifndef __LADARFEEDER_HH__
#define __LADARFEEDER_HH__

//System include files
#include <unistd.h>

//DGC Include files
#include <StateClient.h>
#include <CMapPlus.hh>
#include "frames/frames.hh"
#include <DGCutils>
#include <ladar/ladarSource.hh>


using namespace std;

//Sparrow Display Functions
int user_quit(long arg);
int user_pause(long arg);
int user_step(long arg);
int user_reset(long arg);
int user_change(long arg);

int log_all(long arg);
int log_base(long arg);
int log_quickdebug(long arg);
int log_alldebug(long arg);
int log_none(long arg);


struct LadarFeederOptions {
  char sourceFilenamePrefix[256];
  char logFilenamePrefix[256];
  char ladarString[256];
  int optSNKey, optPause;
  int optCom, optState, optLadar;
  int optLog, logTime, logMaps;
  ladarSourceLoggingOptions loggingOpts;
  int optMaxScans, optSource, optFile, optVerbose, optSim;
  double optDelay, optRate;
  double optThresh;
};


class LadarFeeder : public CStateClient {
public:
  LadarFeeder(modulename ladarType, int skynetKey, LadarFeederOptions ladarFeederOptsArg);
  ~LadarFeeder();
  
  void ActiveLoop();
  
  void ReceiveDataThread();

  void SparrowDisplayLoop();

  void UpdateSparrowVariablesLoop();

  enum {
    LADAR_BUMPER,
    LADAR_ROOF,

    LADAR_UNKNOWN
  };


private:
  CMapPlus ladarMap;

  ladarSource ladarObject;

  LadarFeederOptions ladarOpts;

  XYZcoord ladarPositionOffset;
  RPYangle ladarAngleOffset;

  int _QUIT;
  int _PAUSE;
  int _STEP;

  int _numDeltas;
  
  int layerID_ladarElev;
  double noDataVal_ladarElev;
  double outsideMapVal_ladarElev;
};

#endif
