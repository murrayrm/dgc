/*! This is the header file to allow other functions to call skynet_main.  
 * Which is the skynet thread for adrive.*/

#ifndef ASKYNET_MONITER_H
#define ASKYNET_MONITER_H

void init_vehicle_struct(vehicle_t & new_vehicle);

struct skynet_main_args
{
	struct vehicle_t *pVehicle;
	int* pShutdown_flag;
	int* pRun_skynet_thread;
	void* pSkynet;
};

void* skynet_main(void *arg);

#endif // ASKYNET_MONITER_H
