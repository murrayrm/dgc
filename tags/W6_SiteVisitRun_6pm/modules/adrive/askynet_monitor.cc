#include <stdio.h>
#include <pthread.h>


#include "adrive.h"
#include "adrive_skynet_interface_types.h"
#include "askynet_monitor.hh"
#include "actuators.h"
#include "sn_msg.hh"
#include <string.h>
#include "ActuatorState.hh"

#include <iostream>
using namespace std;

#define BUFSIZE 1024

// send the actuator state at 100hz
#define ACTUATOR_STATE_BROADCAST_INTERVAL 10000

char * actuator_names[NUM_ACTUATORS] = { "BRAKE",
                                         "GAS",
                                         "TRANS",
                                         "STEER",
                                         "ESTOP" };
actuator_command_function actuator_command_functions[NUM_ACTUATORS] = { execute_brake_command,
                                                                        execute_gas_command,
                                                                        execute_trans_command,
                                                                        execute_steer_command,
                                                                        execute_estop_command };
actuator_status_function actuator_status_functions[NUM_ACTUATORS] = { execute_brake_status,
                                                                      execute_gas_status,
                                                                      execute_trans_status,
                                                                      execute_steer_status,
                                                                      execute_estop_status };
actuator_init_function actuator_init_functions[NUM_ACTUATORS] = { execute_brake_init,
                                                                  execute_gas_init,
                                                                  execute_trans_init,
                                                                  execute_steer_init,
                                                                  execute_estop_init };

void parse_status_request(char buffer[BUFSIZE]);
void parse_command(char buffer[BUFSIZE]);
void sn_broadcast_position(int actuator);
void* actuatorStateBroadcastThread(void *pArg);

/*!** THe function which allocates the necessary  *****
 *  memory for all the vehicle srructures */
void init_vehicle_struct(vehicle_t & new_vehicle)
{
  int i;

  //  1.  Initialize Mutexes

  // Initialize acutator mutexes
  for(i=0; i<NUM_ACTUATORS; i++) 
  {
    actuator_t * p_actuator = & (new_vehicle.actuator[i]);

    pthread_mutex_init(&p_actuator->mutex, NULL);
    pthread_cond_init(&p_actuator->cond_flag, NULL);
    p_actuator->start_bit = 0;
    p_actuator->status_enabled  = true;
    p_actuator->command_enabled = true;
    p_actuator->status   = OFF;
    p_actuator->command  = 0;
    p_actuator->position = 0;
    p_actuator->pressure = 0;
    p_actuator->error    = NO_ERROR;      
    p_actuator->status_loop_counter = 0;
    p_actuator->command_loop_counter = 0;
    strcpy( p_actuator->name, actuator_names[i] );
      
    p_actuator->execute_command = actuator_command_functions[i];
    p_actuator->execute_status  = actuator_status_functions[i];
    p_actuator->execute_init    = actuator_init_functions[i];

  }
  
  //Initialize the steering calibration conditional.  
  pthread_cond_init(&steer_calibrated_flag, NULL);
         
}   /* ENd of build_vehicle() */

/*! This is the thread that will monitor skynet for adrive commands and 
 * cause them to execute.  It will pull the skynet key from the environment
 * variable.  */
void* skynet_main(void *pArg)
{
  struct skynet_main_args* pSNmainArgs = (struct skynet_main_args*)pArg;

  struct vehicle_t* pVehicle = pSNmainArgs->pVehicle;
  int& shutdown_flag = *pSNmainArgs->pShutdown_flag;
  int& run_skynet_thread = *pSNmainArgs->pRun_skynet_thread;


  char* default_key = getenv("SKYNET_KEY");
  int sn_key = atoi(default_key);
  pVehicle->skynet_key = sn_key;
  modulename myself = SNadrive;
  modulename other_mod = SNadrive_commander;
  skynet Skynet(myself, sn_key);


  // spawn the thread that broadcasts the actoator state
  pSNmainArgs->pSkynet = (void*)&Skynet;
  pthread_t actuatorStateBroadcastThreadID;
  pthread_create( &actuatorStateBroadcastThreadID, NULL, &actuatorStateBroadcastThread, pArg);




  //printf("My module ID: %u\n My key is %d\n", Skynet.get_id(),sn_key);
  sn_msg myinput = SNdrivecmd;
  sn_msg myoutput = SNdrivecom;

  
  int socket_input = Skynet.listen(myinput, other_mod);
  int socket_output = Skynet.get_send_sock(myoutput);
  drivecmd_t  my_command;
  drivecom_t  my_return;

  while (shutdown_flag == false && run_skynet_thread == true)
  {
    // Listen for an incoming sknet message.  
    Skynet.get_msg(socket_input,&my_command, sizeof(my_command), 0);
    // Parse the command and execute.  
    switch (my_command.my_actuator){
      // These cases came from adrive_skynet_interface_types.h
      // This structure assumes that the actions for each actuator
      // will be different.  If they are determined to be homogeneous, 
      // this case structure can be serialized.  
  
    case steer: 
      // record that we recieved a skynet message to steering.  
      pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_STEER].mutex );
      pVehicle->actuator[ACTUATOR_STEER].command_loop_counter ++;
      pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_STEER].mutex );
       
      //Do something with it.
      switch (my_command.my_command_type){
      case get_position:
        my_return.my_actuator = steer;
        my_return.my_return_type = return_position;
    
        pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_STEER].mutex );
        my_return.number_arg = pVehicle->actuator[ACTUATOR_STEER].position;
        pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_STEER].mutex );
          
        Skynet.send_msg(socket_output, &my_return, sizeof(my_return), 0);
        break;
      case set_position:
        pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_STEER].mutex );
        pVehicle->actuator[ACTUATOR_STEER].command = my_command.number_arg; 
        pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_STEER].mutex );
        pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_STEER].cond_flag );
        break;      
      }
      break;
    case gas:
      // record that we recieved a skynet message to gas.  
      pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_GAS].mutex );
      pVehicle->actuator[ACTUATOR_GAS].command_loop_counter ++;
      pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_GAS].mutex );

      //Do something with it.       
      switch (my_command.my_command_type){
      case set_position:
        pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_GAS].mutex );
        pVehicle->actuator[ACTUATOR_GAS].command = my_command.number_arg;
        pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_GAS].mutex );
        pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_GAS].cond_flag );
        break;
      case get_position:
        my_return.my_actuator = gas;
        my_return.my_return_type = return_position;

        pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_GAS].mutex );
        my_return.number_arg = pVehicle->actuator[ACTUATOR_GAS].position;
        pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_GAS].mutex );
    
        Skynet.send_msg(socket_output, &my_return, sizeof(my_return), 0);
        break;
      }
      break;
    case brake:
      // record that we recieved a skynet message to brake.  
      pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_BRAKE].mutex );
      pVehicle->actuator[ACTUATOR_BRAKE].command_loop_counter ++;
      pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_BRAKE].mutex );

      //Do something with it.
      switch (my_command.my_command_type){
      case get_position:
        my_return.my_actuator = brake;
        my_return.my_return_type = return_position;
    
        pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_BRAKE].mutex );
        my_return.number_arg = pVehicle->actuator[ACTUATOR_BRAKE].position;
        pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_BRAKE].mutex );
    
        Skynet.send_msg(socket_output, &my_return, sizeof(my_return), 0);
        break;
      case get_pressure:
        my_return.my_actuator = brake;
        my_return.my_return_type = return_pressure;
    
        pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_BRAKE].mutex );
        my_return.number_arg = pVehicle->actuator[ACTUATOR_BRAKE].pressure;
        pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_BRAKE].mutex );
    
        Skynet.send_msg(socket_output, &my_return, sizeof(my_return), 0);
        break; 
      case set_position:
        pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_BRAKE].mutex );
        pVehicle->actuator[ACTUATOR_BRAKE].command = my_command.number_arg; 
        pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_BRAKE].mutex );
        pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_BRAKE].cond_flag );
        break;
      }
      break;
  
      /* This is the case that should be used during the race.  It combines the accel and brake
       * preventing commanding brake and accel to be both commanded at once.  */
    case accel:
      // record that we recieved a skynet message to brake and gas.  
      pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_BRAKE].mutex );
      pVehicle->actuator[ACTUATOR_BRAKE].command_loop_counter ++;
      pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_BRAKE].mutex );
      pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_GAS].mutex );
      pVehicle->actuator[ACTUATOR_GAS].command_loop_counter ++;
      pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_GAS].mutex );
  
      //Do something with it.
      switch (my_command.my_command_type){
      case set_position:
        //Check the gas
        pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_GAS].mutex );
        if(my_command.number_arg >= 0 && my_command.number_arg <= 1.0)
          // Check that the command is in the right range for gas and execute.  
        {
          pVehicle->actuator[ACTUATOR_GAS].command = my_command.number_arg;
        }
        else //if (my_command.number_arg > -1 && my_command.number_arg <= 0)
          // if it is outside the range set gas to zero.  
        {
          pVehicle->actuator[ACTUATOR_GAS].command = 0.0;
        }
        pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_GAS].mutex );
        pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_GAS].cond_flag );


        //The brake
        pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_BRAKE].mutex );
        if (my_command.number_arg > 0.0 && my_command.number_arg <= 1.0)
          // If the 
        {
          pVehicle->actuator[ACTUATOR_BRAKE].command = 0.0; 
        }
        else if (my_command.number_arg >= -1.0 && my_command.number_arg <= 0.0)
        {
          // Negate this when passed to the brake for it takes 0 to 1.
          // This is so that the actuator takes 0 to 1 from the commanded 
          // general accel. 
          pVehicle->actuator[ACTUATOR_BRAKE].command = -1.0 * my_command.number_arg; 
        }
        pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_BRAKE].mutex );
        pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_BRAKE].cond_flag );
        break;

      case get_position:
        printf("You really shouldn't be asking for accel position");
        break;
      }
      break;

    case estop:
      // Only one command which is to return condition. 
      my_return.my_actuator = estop;
      my_return.my_return_type = return_position;

      pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_ESTOP].mutex);
      my_return.number_arg = pVehicle->actuator[ACTUATOR_ESTOP].position;
      pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_ESTOP].mutex);
      break;
		}
  }
  
  pthread_exit(0); 
}

void* actuatorStateBroadcastThread(void *pArg)
{
  struct skynet_main_args* pSNmainArgs = (struct skynet_main_args*)pArg;

  struct vehicle_t* pVehicle = pSNmainArgs->pVehicle;
  int& shutdown_flag = *pSNmainArgs->pShutdown_flag;
  int& run_skynet_thread = *pSNmainArgs->pRun_skynet_thread;
  skynet* pSkynet = (skynet*)pSNmainArgs->pSkynet;

  ActuatorState state;

  int socket = pSkynet->get_send_sock(SNactuatorstate);

  while (shutdown_flag == false && run_skynet_thread == true)
  {
		pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_STEER].mutex );
		state.m_steerpos = pVehicle->actuator[ACTUATOR_STEER].position;
		state.m_steercmd = pVehicle->actuator[ACTUATOR_STEER].command;
		pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_STEER].mutex );

		pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_GAS].mutex );
		state.m_gaspos = pVehicle->actuator[ACTUATOR_GAS].position;
		state.m_gascmd = pVehicle->actuator[ACTUATOR_GAS].command;
		pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_GAS].mutex );

		pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_BRAKE].mutex );
		state.m_brakepos = pVehicle->actuator[ACTUATOR_BRAKE].position;
		state.m_brakecmd = pVehicle->actuator[ACTUATOR_BRAKE].command;
		pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_BRAKE].mutex );

		pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_BRAKE].mutex );
		state.m_brakepressure = pVehicle->actuator[ACTUATOR_BRAKE].pressure;
		pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_BRAKE].mutex );

		pthread_mutex_lock(&pVehicle->actuator[ACTUATOR_ESTOP].mutex );
		state.m_estoppos = pVehicle->actuator[ACTUATOR_ESTOP].position;
		pthread_mutex_unlock(&pVehicle->actuator[ACTUATOR_ESTOP].mutex );


		if(pSkynet->send_msg(socket, &state, sizeof(state), 0) != sizeof(state))
		{
			cerr << "actuatorStateBroadcastThread didn't send the right number of bytes!!!!!" << endl;
		}

		usleep(ACTUATOR_STATE_BROADCAST_INTERVAL);
  }

	return NULL;
}
