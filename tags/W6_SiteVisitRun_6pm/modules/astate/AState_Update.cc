/* AState.cc - new vehicle state estimator
 *
 * JML 31 Dec 04
 *
 * */

#include "AState.hh"

// Externally defined Kalman Filter variables
extern int NavBufferIndex;
extern TSaveNavData NavBuffer[XRATIO2];

extern double gps_lat, gps_lon, gps_alt;
extern double gps_vn, gps_ve, gps_vu;
extern double gps_time;
extern double imu_time;
extern double mag_hdg;

extern int New_GPS_data_flag;   // flag set when new GPS data comes in
extern int New_Mag_data_flag;

extern double imu_dvx,imu_dvy,imu_dvz; // Delta-v's m/s (should be)
extern double imu_dtx,imu_dty,imu_dtz; // Delta-Thetas,should be rads

extern double kp[NUM_ST+1][NUM_ST+1];  // covariance matrix
extern double kx[NUM_ST+1];            // kalman gains

extern double tot_kabx, tot_kaby, tot_kabz;
extern double tot_ksfx, tot_ksfy, tot_ksfz;
extern double tot_kgbx, tot_kgby, tot_kgbz;
extern double tot_kgsfx;

extern double ExtGPSPosMeas_x, ExtGPSPosMeas_y, ExtGPSPosMeas_z;
extern double ExtGPSVelMeas_x, ExtGPSVelMeas_y, ExtGPSVelMeas_z;

extern double GPS_xy_kr, GPS_h_kr, GPS_v_kr; // errors

extern int inhib_vert_vel; 

void AState::UpdateState()
{
	// Load in GPS data:

	if (kfilter_enabled == 1)
	{
		//Fill our external variables
		
		// Lock the IMUData Mutex:
		DGClockMutex(&m_IMUDataMutex);
		imu_time = my_imu_time / 1e6;
      		imu_dvx = imudata.dvx;
      		imu_dvy = imudata.dvy;
      		imu_dvz = imudata.dvz;
      		imu_dtx = imudata.dtx;
      		imu_dty = imudata.dty;
      		imu_dtz = imudata.dtz;
		DGCunlockMutex(&m_IMUDataMutex);

		if (New_Mag_data_flag == 0) {
				New_Mag_data_flag = 1;
				mag_count++;
		}
		//cout << "GPS_valid? " << gps_valid << endl;
		
		if ((New_GPS_data_flag == 0) && (gps_valid)) {
			// Lock the GPSData Mutex:
			DGClockMutex(&m_GPSDataMutex);
	  		gps_time = my_gps_time / 1e6;
			
	  		gps_lat = gpsdata.data.lat*DEG2RAD;
	  		gps_lon = gpsdata.data.lng*DEG2RAD;
	  		gps_alt = gpsdata.data.altitude;

	    		gps_vu  = gpsdata.data.vel_u;
	    		gps_vn  = gpsdata.data.vel_n;
	    		gps_ve  = gpsdata.data.vel_e;
	  		New_GPS_data_flag = 1;
			DGCunlockMutex(&m_GPSDataMutex);
		}
		inhib_vert_vel = 1;

		DGCNavRun(); // Run Kalman Filter

        if (NavBufferIndex != lastIndex)
        {
		// Lock the vehiclestate mutex
		DGClockMutex(&m_VehicleStateMutex);
		timediff = imu_time - (vehiclestate.Timestamp / 1e6);

		// Accelerations (done first since referencing last time)
#warning "Acc values should be read directly from IMU"
    		vehiclestate.Acc_N = (NavBuffer[NavBufferIndex].vn - vehiclestate.Vel_N) / timediff;
    		vehiclestate.Acc_E = (NavBuffer[NavBufferIndex].ve - vehiclestate.Vel_E) / timediff;
    		vehiclestate.Acc_D = (NavBuffer[NavBufferIndex].vd - vehiclestate.Vel_D) / timediff;
    		vehiclestate.RollAcc = (vehiclestate.RollRate - imu_dtx) / timediff;
    		vehiclestate.PitchAcc = (vehiclestate.PitchRate - imu_dty) / timediff;
    		vehiclestate.YawAcc = (vehiclestate.YawRate - imu_dtz) / timediff;

		// Velocities
        	vehiclestate.Vel_N = NavBuffer[NavBufferIndex].vn;
		vehiclestate.Vel_E = NavBuffer[NavBufferIndex].ve;
#warning "vd might be z-up?"
		vehiclestate.Vel_D = NavBuffer[NavBufferIndex].vd;

#warning "Pitch/Roll/Yaw Rates may need to be changed to reflect IMU orientation"
		vehiclestate.RollRate = imudata.dtx * 400;
		vehiclestate.PitchRate = imudata.dty * -400;
		vehiclestate.YawRate = imudata.dtz * -400;

		// Evil transformation...
		
		//cout << "Lat: " << NavBuffer[NavBufferIndex].lat << ", ";
		//cout << "Lng: " << NavBuffer[NavBufferIndex].lon << endl;
		//
#warning Roll Pitch and Yaw need to corrected for IMU offset
		vehiclestate.Roll =  NavBuffer[NavBufferIndex].roll - ROLL_IMU;
		vehiclestate.Pitch = NavBuffer[NavBufferIndex].pitch - PITCH_IMU;
		vehiclestate.Yaw = NavBuffer[NavBufferIndex].thdg * DEG2RAD - YAW_IMU; // Yaw comes out in degrees -- ask NGC not me

		latlong.set_latlon(NavBuffer[NavBufferIndex].lat / DEG2RAD, NavBuffer[NavBufferIndex].lon / DEG2RAD);
		latlong.get_UTM(&vehiclestate.Easting, &vehiclestate.Northing);
		vehiclestate.Altitude = -NavBuffer[NavBufferIndex].alt; //We use z-down, GPS and NGC use z-up

  		XYZcoord vehPos(vehiclestate.Northing, vehiclestate.Easting, vehiclestate.Altitude);

  		imu_vector.updateState(vehPos, vehiclestate.Pitch, vehiclestate.Roll, vehiclestate.Yaw);

		imu_correct = imu_vector.transformS2N(blank); 

		vehiclestate.Northing = imu_correct.X;
		vehiclestate.Easting = imu_correct.Y;
		vehiclestate.Altitude = imu_correct.Z;

  //	if (vehiclestate.Speed2() > 1)
	//		vehiclestate.Yaw = atan2(vehiclestate.Vel_E, vehiclestate.Vel_N);

        vehiclestate.Timestamp = my_imu_time;

		DGCunlockMutex(&m_VehicleStateMutex);
        lastIndex = NavBufferIndex;
        //Broadcast the state data:
	if (++broadcastCounter == 5) {
        	Broadcast();
		broadcastCounter = 0;
	}
        }
	} else
	{
		DGClockMutex(&m_VehicleStateMutex);
		DGClockMutex(&m_GPSDataMutex);
		vehiclestate.Timestamp = my_gps_time;
		vehiclestate.Roll = 0;
		vehiclestate.Pitch = 0;
		vehiclestate.RollRate = 0;
		vehiclestate.PitchRate = 0;
		vehiclestate.YawRate = 0;
		latlong.set_latlon(gpsdata.data.lat, gpsdata.data.lng);
		latlong.get_UTM(&vehiclestate.Easting, &vehiclestate.Northing);
		vehiclestate.Altitude = 0;
    		vehiclestate.Acc_N = (vehiclestate.Vel_N - gpsdata.data.vel_n) / timediff;
    		vehiclestate.Acc_E = (vehiclestate.Vel_E - gpsdata.data.vel_e) / timediff;
	    	vehiclestate.Vel_N  = gpsdata.data.vel_n;
	    	vehiclestate.Vel_E  = gpsdata.data.vel_e;
		vehiclestate.Vel_D = 0;
		if (vehiclestate.Speed2() > 1)
			vehiclestate.Yaw = atan2(gpsdata.data.vel_e, gpsdata.data.vel_n);
		DGCunlockMutex(&m_VehicleStateMutex);
		DGCunlockMutex(&m_GPSDataMutex);
	}
}
