/****************************************************************************
 *
 * INS/GPS, Kalman Filter
 *
 * File: kalman.h
 * Date: 10/01/99
 *
 ****************************************************************************/
#ifndef KalmanH
#define KalmanH

#ifdef __BORLANDC__
#pragma option -b
#endif

/****************************************************************************
 * System Error Estimator States
 ****************************************************************************/

typedef enum {
  NOT_LEVELED,
  PRE_LEVELED,
  LEVELED,
  COARSE_ALIGN,
  FINE_ALIGN
} SeeState;

/****************************************************************************
 * Kalman Align Modes
 ****************************************************************************/

typedef enum {
  KALMAN_STARTUP,
  KALMAN_COARSE,
  KALMAN_COARSE_FINE,
  KALMAN_FINE
//KALMAN_REVERSION
} KAlignMode;

/****************************************************************************
 * Function Definitions
 ****************************************************************************/

void kalmanInit(void);
void kControlKalmanFilter(void);
void kApplySystemCorr(void);

//double compute_sigma_ratio(double, double, double[]);

void kEraseKp(void);
void kEraseKx(void);
void kEraseKq(void);
void kEraseKh(void);

/****************************************************************************
 * End
 ****************************************************************************/
#endif