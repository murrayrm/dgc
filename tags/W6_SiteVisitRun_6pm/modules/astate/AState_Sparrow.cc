#include "AState.hh"
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

extern int QUIT_PRESSED;

extern AState *ss;

struct AState_Info
{

	char   gps_stat[10];
	double gps_err;
	double gps_n;
	double gps_e;
	double gps_a;
	double gps_vn;
	double gps_ve;

	char   imu_stat[10];
	double imu_dvx;
	double imu_dvy;
	double imu_dvz;
	double imu_dtx;
	double imu_dty;
	double imu_dtz;

	char   mag_stat[10];
	double mag_head;

	char   kf_stat[10];
	int    imu_count;
	int    gps_count;
	int    mag_count;

	int    Timestamp;
	VehicleState vstate;

	int    snkey;

    int stat;

    double ibx;
    double iby;
    double ibz;
};

extern double tot_kabx;
extern double tot_kaby;
extern double tot_kabz;

AState_Info display_vars;

int toggle_stationary(long);

#include "asdisp.h"

void AState::UpdateSparrow_thread() 
{
	while( QUIT_PRESSED != 1 )
	{
			if (gps_valid == 1)
			{
				strcpy(display_vars.gps_stat,"valid");
			} else {
				strcpy(display_vars.gps_stat,"invalid");
			}

		display_vars.gps_err = gps_err;
		display_vars.gps_n = gps_north;
		display_vars.gps_e = gps_east;
		display_vars.gps_a = gpsdata.data.altitude;
		display_vars.gps_vn = gpsdata.data.vel_n;
		display_vars.gps_ve = gpsdata.data.vel_e;

		if (imu_enabled == 1) {
			strcpy(display_vars.imu_stat,"active");
		} else {
			strcpy(display_vars.imu_stat,"inactive");
		}
		display_vars.imu_dvx = imudata.dvx;
		display_vars.imu_dvy = imudata.dvy;
		display_vars.imu_dvz = imudata.dvz;
		display_vars.imu_dtx = imudata.dtx;
		display_vars.imu_dty = imudata.dty;
		display_vars.imu_dtz = imudata.dtz;

		if (mag_enabled == 1) {
			strcpy(display_vars.mag_stat,"active");
		} else {
			strcpy(display_vars.mag_stat,"inactive");
		}

		display_vars.mag_head = mag_heading;

		if (kfilter_enabled == 1) {
			strcpy(display_vars.kf_stat,"active");
		} else {
			strcpy(display_vars.kf_stat,"inactive");
		}

		display_vars.imu_count = imu_count;
		display_vars.gps_count = gps_count;
		display_vars.mag_count = mag_count;

		display_vars.Timestamp = vehiclestate.Timestamp - starttime;
		display_vars.vstate = vehiclestate;

		display_vars.snkey = snkey;

        display_vars.stat = stationary;

        display_vars.ibx = tot_kabx;
        display_vars.iby = tot_kaby;
        display_vars.ibz = tot_kabz;

		usleep(100000);
	}

}

void AState::SparrowDisplayLoop() {
	dbg_all = 0;

	usleep(1000000);

	if (dd_open() < 0) exit(1);

	dd_usetbl(asdisp);

	dd_loop();
	dd_close();
	QUIT_PRESSED = 1;
}

int toggle_stationary(long arg) {
    if ((*ss).stationary == 1) {
        (*ss).stationary = 0;
    } else {
        (*ss).stationary = 1;
    }
}


