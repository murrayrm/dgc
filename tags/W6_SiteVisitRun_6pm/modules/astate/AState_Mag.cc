/* AState_IMU.cc - new vehicle state estimator
 *
 * JML 31 Dec 04
 *
 * */

#include "AState.hh"

struct raw_MAG
{
    double mag_heading;
};

extern double mag_hdg;
extern int New_Mag_data_flag;

void AState::Mag_init(double MAG_HDG = 0)
{
    if (replay == 1) {
        raw_MAG mag_in;
        mag_replay_stream.read((char*)&mag_in, sizeof(raw_MAG));
        mag_hdg = mag_in.mag_heading;
    }
    else if (mag_enabled == 0) {
        mag_heading = MAG_HDG*DEG2RAD;
        mag_hdg = mag_heading;
    } else {
        mag_enabled = -1;
    }
    if (mag_enabled == -1)
    {
	    double tmp = 0.0;
	    cout << "Mag could not be started, enter current heading in degrees:";
	    cin >> tmp;
	    cin.clear();
	    mag_heading = tmp*DEG2RAD;
	    mag_hdg = mag_heading;
    }
	New_Mag_data_flag = 1;
    if (log_raw == 1)
    {
        raw_MAG mag_out;
        mag_out.mag_heading = mag_hdg;
        mag_log_stream.write((char*)&mag_out, sizeof(raw_MAG));
    }
}

void AState::Mag_thread()
{
	if (mag_enabled == -1)
	{
		cerr << "AState::Mag_thread() unable to start" << endl;
	    return;
	}
}
