#ifndef FUSIONMAPPER_HH
#define FUSIONMAPPER_HH

#include <unistd.h>
#include <pthread.h>

#include "StateClient.h"
#include "DGCutils"

#include "CMapPlus.hh"
#include "rddf.hh"
#include "CCorridorPainter.hh"
#include "CCostPainter.hh"
//#include "RoadFinding.hh"


using namespace std;


struct FusionMapperOptions {
  int optLadar, optStereo;
  int optSNKey;
  int optAverage;
};


class FusionMapper : public CStateClient {
public:
  FusionMapper(int skynetKey, FusionMapperOptions mapperOptsArg);
  ~FusionMapper();
  
  void ActiveLoop();
  
  void ReceiveDataThread_LadarRoof();

  void ReceiveDataThread_LadarBumper();

  void ReceiveRoadDataThread();

  void UpdateSparrowVariablesLoop();

  void SparrowDisplayLoop();

private:

  FusionMapperOptions mapperOpts;

  char* m_pMapDelta;
  double* m_pRoadInfo;

  pthread_mutex_t m_mapMutex;

  CMapPlus fusionMap;
  
  RDDF fusionRDDF;
  
  CCorridorPainter fusionCorridorPainter;
  CCostPainter fusionCostPainter;
  CCorridorPainter fusionCorridorReference;
  
  int layerID_ladarElev;
  double noDataVal_ladarElev;
  double outsideMapVal_ladarElev;
  
  int layerID_cost;
  double noDataVal_cost;
  double outsideMapVal_cost;

  int layerID_number;
  long long noDataVal_number;
  long long outsideMapVal_number;
 
  int layerID_corridor;
  
  double rddf_insideVal;
  double rddf_outsideVal;
  double rddf_tracklineVal;
  double rddf_edgeVal;

  // Temporary way to debug
  ofstream delta_log;

};

#endif
