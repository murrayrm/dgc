
#ifndef _DEADBABY_HH_
#define _DEADBABY_HH_

//Threshold values beyond which the speed is decreased.
#define ROLL_THRESH 0.2       //rad
#define ROLL_RATE_THRESH 0.01   //rad/s
#define PITCH_RATE_THRESH 0.02  //rad/s
#define Z_ACEL_THRESH 5.       //m/s^2
/*Scale factors are necessary so values can be summed.
  The greater the factors, the more the speed will be decreased*/
#define ROLL_FACTOR 2.        //1/rad
#define ROLL_RATE_FACTOR 10.  //s/rad
#define PITCH_RATE_FACTOR 50. //s/rad
#define Z_ACEL_FACTOR 0.5     //s^2/m

#endif
