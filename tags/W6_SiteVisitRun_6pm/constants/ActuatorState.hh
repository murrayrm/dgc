/* ActuatorState.hh - platform-independent actuator state struct
 *
 * Dima
 */

#ifndef ACTUATORSTATE_HH
#define ACTUATORSTATE_HH

#include <string.h>

struct ActuatorState
{
  /** Default constructor */
  ActuatorState()
	{
	  // initialize the state to zeros to appease the memory profilers
	  memset(this, 0, sizeof(*this));
	}
  
  // These are all in the [0,1] range
  double m_steerpos;
  double m_gaspos;
  double m_brakepos;
  double m_brakepressure;
  double m_estoppos;
  
  double m_steercmd;
  double m_gascmd;
  double m_brakecmd;
};

#endif
