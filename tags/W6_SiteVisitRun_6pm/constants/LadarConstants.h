#ifndef LADARCONSTANTS_H
#define LADARCONSTANTS_H

// LADAR mounting parameters are in SensorConstants.h

#define ROOF_LADAR_MAX_RANGE 80    //[m] should be better characterized!!
#define BUMPER_LADAR_MAX_RANGE 50  //[m]

#define LADAR_BAD_SCANPOINT -1 // number to assign to a bad scanpoint

#define DEFAULT_LADARMAP_NUM_ROWS 500
#define DEFAULT_LADARMAP_NUM_COLS 500
#define DEFAULT_LADARMAP_ROW_RES  0.2
#define DEFAULT_LADARMAP_COL_RES  0.2

#define LADAR_PITCH_EPSILON  2.0 * (2.0 * M_PI / 360.0)
#define LADAR_ROLL_EPSILON   2.0 * (2.0 * M_PI / 360.0)

#endif
