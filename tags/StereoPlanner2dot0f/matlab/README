team/matlab
===========

Changes:
  2004/02/02, Lars Cremean, created
  2004/02/09, Lars Cremean, added changes section 

This directory is for housing MATLAB scripts and functions for real-time 
monitoring (eventually) and post-process viewing of logged data.

Files in this directory can be linked to by adding a rule in the Makefile
above any logfile directory.  For example, in RaceModules/Arbiter/Makefile,
we can add the rule:

matlab: ../GlobalPlanner/waypoints.gps
        ( cd ../GlobalPlanner; rddf2bob.sh < waypoints.gps > waypoints.bob )
	( ln -fs ../../GlobalPlanner/waypoints.bob testRuns/ )
	( ln -fs ../$(DGC)/matlab/view_data_0p1.m testRuns/view_data.m )

The first line converts the waypoints file in GlobalPlanner to a new file in
Bob format (by convention, *.bob).  The second line creates a symbolic link 
to that file in the testRuns directory.  The third line creates a symbolic 
link to the MATLAB function to view the data and waypoints.

scripts and functions
=====================

bob2bob.m - Script that takes a Bob format waypoint file and outputs a 
new Bob format file that is translated and rotated according to input arguments

view_data.m - Function to show the waypoint corridor and the path of the 
vehicle.  Uses calls to plot_path and plot_corridor

plot_path.m - Length of arrow represents the vehicle speed, and a cartoon of 
the vehicle is drawn, in animation, using draw_vehicle.m

plot_corridor.m - Function, given a Bob-format waypoint file, which shows 
a plot of the defined corridor

draw_vehicle.m - Function that draws a vehicle given a UTM location, Yaw,
and steering angle
