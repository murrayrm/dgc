#include "BufferedPipe.hh"
#include <iostream>

using namespace std;

BufferedPipe myPipe(500000);

int main() {

  int x = 5, y;
  for(x=0; x<500000; x++) {
    myPipe.push(&x, sizeof(int));
  }
  x = 12345;

  for(y=0; y<500001; y++) {
    if( myPipe.pop(&x, sizeof(int)) == sizeof(int) ) {
      cout << "X = " << x << endl;
    } else {
      cout << "No data available" << endl;
    }
  }
  return 0;
}
