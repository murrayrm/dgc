#include <pthread.h>
#include <iostream>
//#include <string.h>
#include <list>
#include <boost/shared_ptr.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include "ThreadManager.hh"

#include <signal.h>
#include <unistd.h>
#include <stdio.h>

extern int MTA_KERNEL_COUT;

using namespace std;
using namespace boost;

// used to catch generic wake-up signals
void catch_sigusr1(int sgnl) {

  signal(sgnl, &catch_sigusr1);
}


namespace Thread {
  boost::recursive_mutex           LoadQueueMutex;
  list <ThreadReconstructPointers> LoadQueue;

  boost::recursive_mutex           ThreadsMutex;
  int                              OkToAllocateThreads = true;
  list <pthread_t>                 ThreadsToJoin;
  int                              ThreadsAllocatedCount = 0;
  int                              TotalThreadsStartedEver = 0;
  list <pid_t>                     IdleThreads;


  void* ChildThreadMainLoop(void* arg)
  { 
    // make sure we can wake the thread up by 
    // sending a sigusr1 or an alarm
    signal(SIGUSR1, &catch_sigusr1);
    signal(SIGALRM, &catch_sigusr1);
    signal(SIGPIPE, &catch_sigusr1);

    ThreadReconstructPointers next;

    int newjob = false;
    int JustBecameIdle = true;

    if( MTA_KERNEL_COUT) cout << "ThreadManager: Starting New Child Thread, PID = " 
			      << getpid() << endl;

    while(true) {
      {
	boost::recursive_mutex::scoped_lock sl(LoadQueueMutex);
	if(LoadQueue.size() > 0) {
	  if( MTA_KERNEL_COUT) cout << "ThreadManager: Dequeueing new function to execute - queue size = " 
				    << LoadQueue.size() << endl;
	  next = LoadQueue.front();
	  LoadQueue.pop_front();

	  newjob = true;

	  { // must remove ourself from idle threads if we exist
	    boost::recursive_mutex::scoped_lock sl(ThreadsMutex);
	    list<pid_t>::iterator x;
	    pid_t mypid = getpid();

	    //x=IdleThreads.begin();
	    //while( x != IdleThreads.end() ) {
	    //  if( (*x) == mypid ) { IdleThreads.erase(x);} 
	    //  else { x++; }
	    //}
	    
	    for( x=IdleThreads.begin(); x != IdleThreads.end(); x++) {
	      if( (*x) == mypid) { IdleThreads.erase(x); break; }
	    }
	  }

	} else {
	  newjob = false;
	}
      }

      if( newjob ) {
 	if( MTA_KERNEL_COUT) cout << "ThreadManager: Starting New Job, PID = " 
				  << getpid() << endl;
	next.p_fun(next.p_arg);
	JustBecameIdle = true;
      } else {
	// enqueue in idle threads queue
	{
	  boost::recursive_mutex::scoped_lock sl(ThreadsMutex);
	  if( JustBecameIdle ) { 
	    IdleThreads.push_back( getpid() );
	    if( MTA_KERNEL_COUT) cout << "ThreadManager: Enquing in Idle Threads, PID = " 
				      << getpid() << endl;
	    JustBecameIdle = false;
	  }
	}

	// sleep for a bit - ideally this should be a PAUSE
	sleep(2);

      }
    }
  }

  void EnqueueFunctionToExecute(ThreadReconstructPointers & toEnqueue) {
    {
      boost::recursive_mutex::scoped_lock sl(LoadQueueMutex);
      
      if( MTA_KERNEL_COUT) cout << "ThreadManager: Enqueueing new function to execute - queue size = " 
				<< LoadQueue.size() << endl;

      LoadQueue.push_back( toEnqueue );
  
      boost::recursive_mutex::scoped_lock sl2(ThreadsMutex);
      if( IdleThreads.size() > 0 ) {
	pid_t pt = IdleThreads.front();
	IdleThreads.pop_front();
	kill(pt, SIGUSR1);
      } else if( TotalThreadsStartedEver < 20) {
	StartNewThread();
      } else {
	//cout << "Too many threads already started" << endl;
      }
    }
  }

  void StartNewThread() {
    pthread_t hThread;
    int ret;
    boost::recursive_mutex::scoped_lock  sl(ThreadsMutex);
    // now locked
    if( OkToAllocateThreads ) {
      ret=pthread_create(&hThread,NULL, ChildThreadMainLoop,NULL);  // Create Thread
      if(ret<0) { 
	printf("******************** Thread Creation Failed*********************\n");  
      } else {
	ThreadsToJoin.push_back(hThread);
	ThreadsAllocatedCount ++;
	TotalThreadsStartedEver ++;
      }
    } else {
      cout << "Not Starting Thread Because We Are Shutting Down" << endl;
    }
  }


  void TerminateThread(void*) {
    pthread_exit(0);    
  }


  void MaintainThreadCount(int tc) {
    boost::recursive_mutex::scoped_lock sl(ThreadsMutex);

    if( ThreadsAllocatedCount > tc ) {
      // too many allocated, delete a few
      for( ; ThreadsAllocatedCount > tc; ThreadsAllocatedCount--) {
	ThreadReconstructPointers trp;
	trp.p_fun  = &TerminateThread;
	trp.p_arg  = NULL;
	EnqueueFunctionToExecute(trp);
      } 
    } else if( ThreadsAllocatedCount < tc && OkToAllocateThreads ) {
      // not enough allocated, add a few threads
      for( ; ThreadsAllocatedCount < tc; ThreadsAllocatedCount++) {
	StartNewThread();
      }
    } else {
      // The Correct Number are allocated, do nothing.
    }
  }

  void Join() {
    int quit = false;
    pthread_t myPT;
    
    {  
      boost::recursive_mutex::scoped_lock sl(ThreadsMutex);
      if( ThreadsToJoin.size() == 0 ) {
	quit = true;
      } else {
	myPT = ThreadsToJoin.front();
	ThreadsToJoin.pop_front();
      }
    }

    while( !quit ) {
      pthread_join(myPT, NULL);
      { 
	boost::recursive_mutex::scoped_lock sl(ThreadsMutex);
	if( ThreadsToJoin.size() == 0 ) { quit = true; } 
	else {
	  myPT = ThreadsToJoin.front();
	  ThreadsToJoin.pop_front();
	}
      }
    }
  }


  void ForceShutdown() {
    boost::recursive_mutex::scoped_lock sl(ThreadsMutex);
    OkToAllocateThreads = false;
    MaintainThreadCount(0);
  }
};
