#ifndef Pipe_HH
#define Pipe_HH

#include <stddef.h>

class Pipe {
  
public:
  Pipe();
  ~Pipe();
  virtual size_t size() =0;

private:
  virtual void destruct() =0;
  virtual int pop(void* buffer, const size_t bytes) =0;
  virtual int push(const void* buffer, const size_t bytes)=0;
};

#endif
