#include "BufferedPipe.hh"
#include <iostream>
#include <unistd.h>


using namespace std;


const int trials = 100000;
BufferedPipe myPipe;

int main() {
  int count=0;

  int x = 5, y;
  while (1) {
    for(x=0; x<trials; x++) {
      myPipe.push(&x, sizeof(int));
    }
    
    cout << "Number of blocks = " << myPipe.blocks() << endl;

    x = 12345;
    
    for(y=0; y<trials; y++) {
      myPipe.pop(&x, sizeof(int));

      if( x != y ) cout << "Error" << endl;

    }
    count ++;
    cout << "Count = " << count << endl;
    sleep(1);
  }
  return 0;
}
