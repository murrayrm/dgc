#include "Socket.hh"
#include "../Time/Time.hh"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <fcntl.h>

#include <time.h>
#include <sys/time.h>
#include <fcntl.h>

#include <boost/shared_ptr.hpp>


using namespace std;
using namespace boost;

extern int MTA_KERNEL_COUT;

unsigned int IPConvert(const char* ip) {
  unsigned int ret;
  inet_aton(ip, (in_addr*) &ret);
  return ret;
}

int set_nonblock(int fd) {

  int flags = fcntl(fd, F_GETFL, 0);
  if (flags == -1) {
    return -1;
  } else {
    return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
  }


}




CSocket::CSocket() {
  myFH = shared_ptr<FileHandle> (new FileHandle(-1, false));
}


CSocket::CSocket(unsigned int ip, int port) {
  myFH = shared_ptr<FileHandle> (new FileHandle(-1, false));
  Open(ip, port);
}

int CSocket::Open(unsigned int ip, int port) {

  sockaddr_in ctrl_sa;
  socklen_t   ctrl_sl;

  int myFD  = socket(PF_INET, SOCK_STREAM, 0);
  myFH = shared_ptr<FileHandle>(new FileHandle(myFD, true));

  if( myFD >= 0) {
    memset(&ctrl_sa, 0, sizeof(ctrl_sa));
    ctrl_sa.sin_family = AF_INET;
    ctrl_sa.sin_port   = htons(port);
    ctrl_sa.sin_addr.s_addr = ip;
    ctrl_sl = sizeof(ctrl_sa);
    
    if( connect(myFD, (sockaddr*) &ctrl_sa, ctrl_sl)) {
      //      cout << "Socket Connect Error" << endl;
      myFH = shared_ptr<FileHandle>(new FileHandle(-1, false));;
      return false;
    } else {
      // we are successfully connected

      //      set_nonblock(myFD); // declare non-blocking I/O
      return myFD;
    }
  } else {
    return false;
  }
}
      
CSocket::CSocket(const CSocket& f) {
  myFH = f.myFH;
}

CSocket::~CSocket() {

}


SSocket::SSocket() {
  myFH = shared_ptr<FileHandle> (new FileHandle(-1, false));
}

SSocket::~SSocket() {

}


int SSocket::Listen(int port) {

  // set an alarm to make sure we abort if there was an error
  alarm(1);

  //  cout << "Opening Server Socket" << endl;

  if( MTA_KERNEL_COUT ) cout << "SSocket: Trying to listen on port: " 
			     << port << endl;
  M_port = port;

  int sockfd;
  sockaddr_in srv;
  socklen_t   socklen;

  sockfd = socket(PF_INET, SOCK_STREAM, 0);

  //  cout << "Server Socket: FD = " << sockfd << endl;

  if (sockfd < 0) {
    if( MTA_KERNEL_COUT ) cout << "       : Error - Creating Socket" << endl;
    alarm(0); // clear the alarm
    return false;
  } else {
      myFH = shared_ptr<FileHandle> (new FileHandle(sockfd, false));
  }

  // cout << "Server Socket: FD = " << (*myFH).FD() << endl;

  //  setsockopt(sockfd, SOL_SOCKET, 0, &i, sizeof(i));
  memset(&srv, 0, sizeof(srv));

  srv.sin_family = AF_INET;
  srv.sin_port = htons(port);
  socklen = sizeof(srv);

  if( MTA_KERNEL_COUT ) cout   << "       : Trying To Bind" << endl;

  if((bind(sockfd, ((sockaddr *) &srv), socklen))) {
    if( MTA_KERNEL_COUT ) cout << "       : Error - Bind" << endl;
    alarm(0); // clear the alarm
    return false;
  }

  if( MTA_KERNEL_COUT ) cout   << "       : Trying To Listen" << endl;

  if(listen(sockfd, 5) < 0) {
    if( MTA_KERNEL_COUT ) cout << "       : Error - Listen" << endl;
    alarm(0); // clear the alarm
    return false;
  }

  if( MTA_KERNEL_COUT ) cout   << "       : Listen Successful" << endl;

  alarm(0); // clear the alarm
  return sockfd;
}


File SSocket::Accept() {
  int sockfd = (*myFH).FD();
  sockaddr sa;
  socklen_t socklen;

  int ret = accept(sockfd, &sa, &socklen);

  if( ret >= 0) {
    return File(ret, true);
  }
  else {
    perror("accept");
    return File(-1, false);
  }
}

int SSocket::Port() {
  return M_port;
}
