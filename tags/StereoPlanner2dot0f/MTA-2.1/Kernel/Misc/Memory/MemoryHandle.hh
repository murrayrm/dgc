#ifndef MemoryHandle_HH
#define MemoryHandle_HH



class MemoryHandle {
public:
  MemoryHandle(void* p_MemoryLibrary, void* ptr, int size);
  ~MemoryHandle();

  void* operator * ();

private:

  void* p_MemoryLibrary;
  void* ptr;
  int size;

};

#endif
