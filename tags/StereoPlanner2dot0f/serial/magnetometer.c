#include "magnetometer.h"

static int mm_com;	// com number for magnetometer serial port
const char mm_init_command[MM_SET_COMMAND_NUM][10]={"ut=c", "uc=d", "ui=d", "ec=e", "ep=e", "er=e", "em=e", "et=e", "ed=e", "ma=c"};
const char mm_command_list[MM_COMMAND_NUM][8]={"c?", "m?", "i?", "t?","s?", "lc?", "cc", "mpcal=e", "mpcal=d", "go", "h", "ax"};  // command output strings
const char mm_command_return[MM_COMMAND_NUM][10]={"CE","XYZE", "PRE", "TE", "CPRXYZTE", "HVM", "HVM", "", "", "", "", ""};  // command return strings
const int mm_command_return_num[MM_COMMAND_NUM]={1, 3, 2, 1, 7, 3, 3, 0, 0, 0, 0, 0};	// number of returned paremters for each command


int mm_init(int com)
{
   char setup[10]={0};
   int i, end, max_read=MM_MAX_READ;

   if(serial_open(com, MM_DefaultBaud)<0)
   {
	fprintf(stderr, "Unable to open serial port for magnetometer\n");
	return -1;
   }
   mm_com=com;
   mm_send_command(MM_HALT, NULL, 0);
   for(i=0; i<MM_SET_COMMAND_NUM; i++)	// enter all the init command
   {
	end= strlen(mm_init_command[i]);
	memcpy(setup, mm_init_command[i], end);
	setup[end]=MM_CR;
   	if(serial_write(mm_com, setup, end+1, SerialBlock)!=(end+1))
           fprintf(stderr, "init_mm: unable to send initialization command.\n");
	while((serial_read(0, setup, 80)<=0) && (max_read>=0))
        {
	   usleep(50000);
	   max_read--;
	}
	if(max_read<0) return -1;
	max_read=MM_MAX_READ;	
   }
   return 0;		
   
}

int mm_uninit(void)
{
	return serial_close(mm_com);
}
   
int mm_send_command(enum mm_command command, float command_return[], int return_num)
{
   char buffer[60]={0};
   char *pt1=NULL, *pt2=NULL;
   int  end;
   int i, temp=0, read_len=0,total_len=0;
   int error=0, terror_check=0, xor_val=0, max_read=MM_MAX_READ;

   memcpy(buffer,  mm_command_list[command], strlen(mm_command_list[command]));
   end=strlen(mm_command_list[command]);
   buffer[end]=	MM_CR;	//CR

   if(serial_write(mm_com, buffer, end+1, SerialBlock)!=(end+1))
   {	
      fprintf(stderr, "send_mm_command: serial_write error\n");
      return -1;
   }
   switch(command)
   {
      case MM_COMPASS_UP:	// these commands generate error code and checksum
      case MM_MM_UP:
      case MM_IN_UP:
      case MM_TEMP_UP:
      case MM_OW_UP:
      {
	do
	{
	   usleep(50000);	// sleep for a while and wait for a serial signal to wake us up
	   if((read_len=serial_read(mm_com, buffer+total_len, 50-total_len))>=0)
	   {
		total_len+=read_len;
	        pt2=strchr(buffer, '*');
		max_read--;
	   }
	}while((pt2==NULL || (pt2-buffer+2)>=total_len) && max_read>=0);	// keep reading until getting the full response
							// the end of the response is *nn, an asterisk and two numbers
	if(max_read<0) return -1;// if the serial port is read max_read times, there is an error.

	   
	if((pt1=strchr(buffer, '$'))==NULL)
	{
	   fprintf(stderr, "mm_send_command: Didn't get $\n");
	   return -1;
	}
	for(pt1++; pt1<pt2; pt1++)	// calculate the xored value for the characters between $ and *
	{
		xor_val= (*pt1) ^ xor_val;
	}
	if(pt2++!=NULL)			// look for beginning of transmit error checking code
	   for(i=0; i<MM_TRANSMIT_CHECK_NUM; i++)	// convert error checking code into an integer
	   {
		if(isdigit(pt2[i]))
		   temp=pt2[i]-48;	// convert ASCII digit to real number
		else if(isupper(pt2[i]))	
		   temp=pt2[i]-55;	// convert ASCII letter to real number
		else fprintf(stderr, "send_mm_command: Error code error\n");
		terror_check = terror_check + temp*pow(16, MM_TRANSMIT_CHECK_NUM-i-1);
	   }
	if(terror_check!=xor_val)	// if the error correcting code and the xored value are different
	{				// then there is a transmit error
           fprintf(stderr, "mm_send_command: transmit error\n");
	   return -1;
	}
	if((pt1=strchr(buffer, 'E'))!=NULL)	// look for beginning of error code
	{
	   if(pt1<pt2)	// make sure we didn't read E from checksum
	      for(i=1; i<=MM_ERROR_CODE_NUM; i++)
	      {
	   	if(isdigit(pt1[i]))
		   temp=pt1[i]-48;	// convert ASCII digit to real number
		else if(isupper(pt1[i]))	
		   temp=pt1[i]-55;	// convert ASCII letter to real number
		else fprintf(stderr, "send_mm_command: Error code error\n");
		error = error | (temp<<((3-i)*4));
	      }
	}
	else error=0;
	if(pt1!=NULL) pt1[0]='\0';  // get rid of 'E' so that atof wouldn't think E001 stands for 10^001 
	for(i=0; (i<strlen(mm_command_return[command])-1) || i<return_num;i++)	// interpret the results
	   if((pt1=strchr(buffer, mm_command_return[command][i]))!=NULL)
		command_return[i]=atof(pt1+1);
	return error;
	break;
      }
      case MM_LC_SC:	// these commands don't generate error code
      case MM_CR_CA:	
      {
	do
	{
	   usleep(50000);	// sleep for a while and wait for a serial signal to wake us up
	   if((read_len=serial_read(mm_com, buffer+total_len, 50-total_len))>=0)
	   {
		total_len+=read_len;
		pt2=strchr(buffer, 'M');	// look for M in the string
		max_read--;
	        //printf("LC: %s", buffer);
	   }
	}while((pt2==NULL || (pt2-buffer+4)>=total_len) && max_read>=0);  // keep reading until the end of the
				// response is read or the serial port has been read max_read times.

	if(max_read<0) return -1;// if the serial port is read max_read times, there is an error.

	for(i=0; (i<strlen(mm_command_return[command])-1) || i<return_num;i++)	// if not error, interpret the results
	   if((pt1=strchr(buffer, mm_command_return[command][i]))!=NULL)
		command_return[i]=atof(pt1+1);
	return error;
	break;
      }
      case MM_EMP_CA:
      case MM_DMP_CA:
      case MM_GO:
      {
	while((serial_read(0, buffer, 60)<=0)&& max_read>0)
        {
	   usleep(50000);
	   max_read--;
	}	
         return 0;
	 break;
      }
      case MM_WARM_REBOOT:
      case MM_HALT:
      default:
      {
	while((serial_read(0, buffer, 60)<=0) && (max_read>=0))
        {
	   usleep(50000);
	   max_read--;
	}
	if(max_read<0) return -1;
	return 0;
	break;
      }
   }
   return -1;		// shouldn't get here
}

int mm_calibration(void)
{
   long int i, j, l;
   float a[10];
   char buffer[10];
   int  max_read=200, max_halt=200;	// max_read and max_halt are used to make sure read and halt are performed
			// that many times so that the system doesn't lock up

   mm_send_command(MM_LC_SC, a, 3);
   mm_send_command(MM_CR_CA, a, 3);

   printf("clear calibration score: H:%f V:%f M:%f\n", a[0], a[1], a[2]);

   mm_send_command(MM_EMP_CA, a, 0); 
   mm_send_command(MM_GO, a, 0);

   for(i=0; i<10000; i++)
	for(j=0; j<10000; j++) 
	   for(l=0; l<300; l++) ;

   mm_send_command(MM_HALT, NULL, 0);

   while(max_read>0 && max_halt>0)		// use this loop to make sure the module is halted
   {
	if(serial_read(mm_com, buffer, 10)==0)	
	{
	   usleep(10000);
	   max_read--;
	}
	else
	{
	   mm_send_command(MM_HALT, NULL, 0);
	   max_halt--;
	}
   }
   usleep(10000);
   mm_send_command(MM_LC_SC, a, 3);
   mm_send_command(MM_DMP_CA, a, 0);
   printf("calibration score: %f %f %f\n", a[0], a[1], a[2]);
   return 0;
}	
