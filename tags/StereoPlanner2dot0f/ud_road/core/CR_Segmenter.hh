//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// CR_Segmenter
//----------------------------------------------------------------------------

#ifndef CR_SEGMENTER_DECS

//----------------------------------------------------------------------------

#define CR_SEGMENTER_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>


#include "CR_Matrix.hh"
#include "CR_Image.hh"
#include "CR_Memory.hh"
#include "CR_ParticleFilter.hh"
#include "CR_OpenGL.hh"
#include "CR_KalmanFilter.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))
#define MAX3(A, B, C)         (((A) > (B)) ? MAX2((A), (C)) : MAX2((B), (C)))
#define MIN3(A, B, C)         (((A) < (B)) ? MIN2((A), (C)) : MIN2((B), (C)))

typedef struct crroadregion
{
  double vp_x, vp_y;
  double theta_center, theta_width;

} CR_RoadRegion;

//-------------------------------------------------
// functions
//-------------------------------------------------

void ransac_fish(CR_Vector *, CR_Matrix *, int);
void init_seg_kalman();
void draw_fish_segmenter();
int fit_right_step_edge(int, CR_Image *);
int fit_left_step_edge(int, CR_Image *);
void init_seg_voter_region(int, int, int, int, int, int);
void hist_segment_image(CR_Image *);
void draw_hist_segmenter(CR_Image *);
void update_segmenter(CR_Image *);
void update_seg_particlefilter();
void draw_seg_particlefilter();
void initialize_seg_particlefilter();
void seg_samp_prior(CR_Vector *);
void seg_samp_state(CR_Vector *, CR_Vector *);
void seg_dyn_state(CR_Vector *, CR_Vector *);
double seg_condprob_zx(void *, CR_Vector *);
void init_seg_fisher(CR_Matrix *, CR_Matrix *, CR_Vector *);
void seg_fisher(CR_Matrix *, CR_Matrix *, CR_Vector *);
void initialize_segmenter();
void compute_discriminants(CR_Image *);
int legal_roadregion(CR_Vector *);
int inside_roadregion(double, double, CR_Vector *);
int below_horizon(double, double, CR_Vector *);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif
