//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_Image.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

int num_image_processors = 7;

int done_calibration = FALSE;

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// from file

CR_Calibration *initialize_CR_Calibration(char *filename)
{
  CR_Vector *cv;
  
  cv = read_dlm_CR_Vector(filename, 8);
  return initialize_CR_Calibration(cv->x[0], cv->x[1], cv->x[2], cv->x[3], cv->x[4], cv->x[5], cv->x[6], cv->x[7]);
}

//----------------------------------------------------------------------------

// Bougouet's camera calibration parameters

// assuming skew = 0, kc_5 = 0

CR_Calibration *initialize_CR_Calibration(double fc_1, double fc_2, double cc_1, double cc_2, double kc_1, double kc_2, double kc_3, double kc_4)
{
  CR_Calibration *C;

  C = (CR_Calibration *) CR_malloc(sizeof(CR_Calibration));

  C->KK = init_CR_Matrix("KK", 3, 3, fc_1, 0.0, cc_1, 0.0, fc_2, cc_2, 0.0, 0.0, 1.0);
  C->KK_inv = inverse_CR_Matrix(C->KK);
  C->KC = init_CR_Vector("KC", 4, kc_1, kc_2, kc_3, kc_4);
  C->X = init_CR_Vector("X", 3, 0.0, 0.0, 1.0);
  C->Y = init_CR_Vector("Y", 3, 0.0, 0.0, 1.0);

  C->LUT_x = NULL;
  C->LUT_y = NULL;

  return C;
}

//----------------------------------------------------------------------------

// takes distorted point (xd, yd) -> undistorted point (xu, yu)

void undistort_image_CR_Calibration(CR_Image *im, CR_Image *imdist, CR_Calibration *C)
{
  int v, u;

  /*
  C->X->x[0] = xs;
  C->X->x[1] = ys;

  matrix_vector_product_CR_Matrix(C->KK_inv, C->X, C->Y);
  */

  if (!C->LUT_x) {

    C->LUT_x = (int **) CR_calloc(im->h, sizeof(int *));
    C->LUT_y = (int **) CR_calloc(im->h, sizeof(int *));
    for (v = 0; v < im->h; v++) {
      C->LUT_x[v] = (int *) CR_calloc(im->w, sizeof(int));
      C->LUT_y[v] = (int *) CR_calloc(im->w, sizeof(int));
    }

    double a1, b1, u0, v0, k1, k2, p1, p2, p22;
    int ud, vd;
    double dv, y, y1, y2, y3, du, x, x1, x2, x3, r2, bx, by;
    
    a1 = 1.f / MATRC(C->KK, 0, 0);
    b1 = 1.f / MATRC(C->KK, 1, 1);
    u0 = MATRC(C->KK, 0, 2);
    v0 = MATRC(C->KK, 1, 2);
    k1 = C->KC->x[0];
    k2 = C->KC->x[1];
    p1 = C->KC->x[2];
    p2 = C->KC->x[3];
    
    p22 = 2.f * p2;
    
    for (v = 0; v < im->h; v++) {
      
      dv = v - v0;
      y = b1 * dv;
      y1 = p1 / y;
      y2 = y * y;
      y3 = 2.f * p1 * y;
      
      for (u = 0; u < im->w; u++) {
	
	du = u - u0;
	x = a1 * du;
	x1 = p2 / x;
	x2 = x * x;
	x3 = p22 * x;
	r2 = x2 + y2;
	bx = r2 * (k1 + r2 * k2) + x3 + y3;
	by = bx + r2 * y1;
	ud = u, vd = v;
	
	bx += r2 * x1;
	ud += CR_round(bx * du);
	vd += CR_round(by * dv);

	if (ud < 0 || ud >= im->w || vd < 0 || vd >= im->h) {
	  C->LUT_x[v][u] = -1;
	  C->LUT_y[v][u] = -1;
	}
	else {
	  C->LUT_x[v][u] = ud;
	  C->LUT_y[v][u] = vd;
	}
      }
    }
  }

  for (v = 0; v < im->h; v++)
    for (u = 0; u < im->w; u++) {
      //      printf("x = %i, y = %i, distorted x = %i, y = %i\n", u, v, C->LUT_x[v][u], C->LUT_y[v][u]);
// 
      //      if (v > 0)
      //       	exit(1);

      if (C->LUT_x[v][u] == -1) {
	IMXY_R(im, u, v) = (unsigned char) 0;
	IMXY_G(im, u, v) = (unsigned char) 0;
	IMXY_B(im, u, v) = (unsigned char) 0;
      }
      else {
	IMXY_R(im, u, v) = (unsigned char) IMXY_R(imdist, C->LUT_x[v][u], C->LUT_y[v][u]);
	IMXY_G(im, u, v) = (unsigned char) IMXY_G(imdist, C->LUT_x[v][u], C->LUT_y[v][u]);
	IMXY_B(im, u, v) = (unsigned char) IMXY_B(imdist, C->LUT_x[v][u], C->LUT_y[v][u]);   
      }   
    }

  //  exit(1);

}

//----------------------------------------------------------------------------

// draw src on top of dst with src's upper-left hand corner located at (x, y) in dst
// handles clipping issues

void composite_CR_Image(int x, int y, CR_Image *src, CR_Image *dst)
{
  int i, j, u, v;

  for (i = x, u = 0; i < x + src->w; i++, u++)
    for (j = y, v = 0; j < y + src->h; j++, v++) 
      if (IN_IMAGE(dst, i, j)) {
	IMXY_R(dst, i, j) = (unsigned char) IMXY_R(src, u, v);
	IMXY_G(dst, i, j) = (unsigned char) IMXY_G(src, u, v);
	IMXY_B(dst, i, j) = (unsigned char) IMXY_B(src, u, v);
      }
}

//----------------------------------------------------------------------------

// chooses anchor point nearest to center

CR_Image_Kernel *make_CR_Image_Kernel(CR_Matrix *kernel_matrix)
{
  CR_Image_Kernel *kernel;
  int i, j, index;

  kernel = (CR_Image_Kernel *) CR_malloc(sizeof(CR_Image_Kernel));
  kernel->x = 1 + (kernel_matrix->cols - 1) / 2;
  kernel->y = 1 + (kernel_matrix->rows - 1) / 2;
  kernel->w = kernel_matrix->cols;
  kernel->h = kernel_matrix->rows;
  kernel->values = (float *) CR_calloc(kernel_matrix->rows * kernel_matrix->cols, sizeof(float));

  for (j = 0, index = 0; j < kernel_matrix->rows; j++)
    for (i = 0; i < kernel_matrix->cols; i++)
      kernel->values[index++] = MATRC(kernel_matrix, j, i);

  kernel->k = iplCreateConvKernelFP(kernel->w, kernel->h, kernel->x, kernel->y, kernel->values);

  return kernel;
}

//----------------------------------------------------------------------------

void convolve_CR_Image(CR_Image_Kernel *kernel, CR_Image *src_im, CR_Image *dst_im)
{
  iplConvolve2DFP(src_im->iplim, dst_im->iplim, &kernel->k, 1, IPL_SUM);
}

//----------------------------------------------------------------------------

// w and h should be odd

// values may be 8-bit or 32-bit floating point

void histogram_gray_CR_Image(int x, int y, int w, int h, CR_Vector *histogram, double hist_lower, double hist_upper, CR_Image *im)
{
  int i, j, xrad, yrad;
  double hist_interval, inverse_area, val;

  if (im->type != IPL_TYPE_GRAY_32F) 
    CR_error("histogram_gray_CR_Image(): only floating point data supported at this time");

  // initialize

  xrad = (w - 1) / 2;
  yrad = (h - 1) / 2;
  inverse_area = 1 / (double) (w * h);

  hist_interval = (hist_upper - hist_lower) / (double) histogram->rows;

  // set count to zero

  set_all_CR_Vector(0.0, histogram);
    
  // count

  for (j = y - yrad; j <= y + yrad; j++)
    for (i = x - xrad; i <= x + xrad; i++) {
      val = GRAYFP_IMXY(im, i, j);
      if (val < hist_lower) {
	//	printf("val %lf => bin %i (bins = %i, lower = %lf, upper = %lf, interval = %lf)\n", val, 0, histogram->rows, hist_lower, hist_upper, hist_interval);
	histogram->x[0]++;
      }
      else if (val > hist_upper) {
	//	printf("val %lf => bin %i (bins = %i, lower = %lf, upper = %lf, interval = %lf)\n", val, histogram->rows - 1, histogram->rows, hist_lower, hist_upper, hist_interval);
	histogram->x[histogram->rows - 1]++;
      }
      else {
	//	printf("val %lf => bin %i (bins = %i, lower = %lf, upper = %lf, interval = %lf)\n", val, (int) ((val - hist_lower) / hist_interval), histogram->rows, hist_lower, hist_upper, hist_interval);
	histogram->x[(int) ((val - hist_lower) / hist_interval)]++;
      }
    }

  // normalize

  scale_CR_Vector(inverse_area, histogram);

  //  print_CR_Vector(histogram);
  //  exit(1);

}

//----------------------------------------------------------------------------

// values must be 8-bit at this point

// histogram color channels independently over subimage of size (w, h) centered at (x, y)

void joint_histogram_color_CR_Image(int x, int y, int w, int h, CR_Vector *joint_rgb_histogram, int num_bins_per_color, double hist_lower, double hist_upper, CR_Image *im)
{
  int i, j, xrad, yrad, r_coord, g_coord, b_coord;
  double hist_interval, inverse_area, r, g, b;

  if (im->type != IPL_TYPE_COLOR_8U) 
    CR_error("joint_histogram_color_CR_Image(): only 8-bit unsigned data supported at this time");

  // initialize

  xrad = (w - 1) / 2;
  yrad = (h - 1) / 2;
  inverse_area = 1 / (double) (w * h);

  hist_interval = (hist_upper - hist_lower) / (double) num_bins_per_color;

  // set count to zero

  set_all_CR_Vector(0.0, joint_rgb_histogram);

  // count

  for (j = y - yrad; j <= y + yrad; j++)
    for (i = x - xrad; i <= x + xrad; i++) {

      //      printf("i = %i, j = %i\n", i, j);

      r = (double) ((unsigned char) IMXY_R(im, i, j));
      g = (double) ((unsigned char) IMXY_G(im, i, j));
      b = (double) ((unsigned char) IMXY_B(im, i, j));

      if (r < hist_lower)
	r_coord = 0;
      else if (r > hist_upper)
	r_coord = num_bins_per_color - 1;
      else
	r_coord = (int) ((r - hist_lower) / hist_interval);

      if (g < hist_lower)
	g_coord = 0;
      else if (g > hist_upper)
	g_coord = num_bins_per_color - 1;
      else
	g_coord = (int) ((g - hist_lower) / hist_interval);

      if (b < hist_lower)
	b_coord = 0;
      else if (b > hist_upper)
	b_coord = num_bins_per_color - 1;
      else
	b_coord = (int) ((b - hist_lower) / hist_interval);

      joint_rgb_histogram->x[r_coord * num_bins_per_color * num_bins_per_color + g_coord * num_bins_per_color + b_coord]++;
    }

  // normalize

  scale_CR_Vector(inverse_area, joint_rgb_histogram);

  //  print_CR_Vector(joint_rgb_histogram);
}

//----------------------------------------------------------------------------

// values must be 8-bit at this point

// histogram color channels independently over subimage of size (w, h) centered at (x, y)

//void histogram_trapezoid_color_CR_Image(int x, int y_top, int y_bottom, float slope, CR_Matrix *uv_histogram, double hist_lower, double hist_upper, CR_Image *im)
void histogram_trapezoid_color_CR_Image(int x, int y_top, int y_bottom, int w_bottom, double slope, CR_Vector *r_histogram, CR_Vector *g_histogram, CR_Vector *b_histogram, CR_Image *im)
{
  int i, j, xrad, yrad;
  double hist_interval, hist_range, inverse_area, r, g, b, area, inverse_slope;
  double xoff;

  if (im->type != IPL_TYPE_COLOR_8U) 
    CR_error("histogram_color_CR_Image(): only 8-bit unsigned data supported at this time");

  // initialize

  hist_range = 255.0;    // can assume hist_lower = 0 and hist_upper = 255 with 8-bit images
  hist_interval = hist_range / (double) r_histogram->rows;

  // set count to zero

  set_all_CR_Vector(0.0, r_histogram);
  set_all_CR_Vector(0.0, g_histogram);
  set_all_CR_Vector(0.0, b_histogram);
    
  // count

  area = 0;
  inverse_slope = 1.0/slope;
  xoff = 0.5 * (double) w_bottom;
  for (j = y_bottom; j >= y_top; j--, xoff -= inverse_slope) {
    for (i = x - CR_round(xoff); i < x + CR_round(xoff); i++) {

      area++;

      r = (double) ((unsigned char) IMXY_R(im, i, j));
      g = (double) ((unsigned char) IMXY_G(im, i, j));
      b = (double) ((unsigned char) IMXY_B(im, i, j));

      r_histogram->x[(int) (r / hist_interval)]++;
      g_histogram->x[(int) (g / hist_interval)]++;
      b_histogram->x[(int) (b / hist_interval)]++;

      /*
      r = (double) ((unsigned char) IMXY_R(im, i, j));
      if (r < hist_lower)
	r_histogram->x[0]++;
      else if (r > hist_upper)
	r_histogram->x[r_histogram->rows - 1]++;
      else
	r_histogram->x[(int) ((r - hist_lower) / hist_interval)]++;

      g = (double) ((unsigned char) IMXY_G(im, i, j));
      if (g < hist_lower)
	g_histogram->x[0]++;
      else if (g > hist_upper)
	g_histogram->x[g_histogram->rows - 1]++;
      else
	g_histogram->x[(int) ((g - hist_lower) / hist_interval)]++;

      b = (double) ((unsigned char) IMXY_B(im, i, j));
      if (b < hist_lower)
	b_histogram->x[0]++;
      else if (b > hist_upper)
	b_histogram->x[b_histogram->rows - 1]++;
      else
	b_histogram->x[(int) ((b - hist_lower) / hist_interval)]++;
      */

    }
  }

  inverse_area = 1/area;

  // normalize

  scale_CR_Vector(inverse_area, r_histogram);
  scale_CR_Vector(inverse_area, g_histogram);
  scale_CR_Vector(inverse_area, b_histogram);

//   print_CR_Vector(r_histogram);
//   print_CR_Vector(g_histogram);
//   print_CR_Vector(b_histogram);
}

//----------------------------------------------------------------------------

// values must be 8-bit at this point

// histogram color channels independently over subimage of size (w, h) centered at (x, y)

void histogram_color_CR_Image(int x, int y, int w, int h, CR_Vector *r_histogram, CR_Vector *g_histogram, CR_Vector *b_histogram, double hist_lower, double hist_upper, CR_Image *im)
{
  int i, j, xrad, yrad;
  double hist_interval, inverse_area, r, g, b;

  if (im->type != IPL_TYPE_COLOR_8U) 
    CR_error("histogram_color_CR_Image(): only 8-bit unsigned data supported at this time");

  // initialize

  xrad = (w - 1) / 2;
  yrad = (h - 1) / 2;
  inverse_area = 1 / (double) (w * h);

  hist_interval = (hist_upper - hist_lower) / (double) r_histogram->rows;

  // set count to zero

  set_all_CR_Vector(0.0, r_histogram);
  set_all_CR_Vector(0.0, g_histogram);
  set_all_CR_Vector(0.0, b_histogram);
    
  // count

  for (j = y - yrad; j <= y + yrad; j++)
    for (i = x - xrad; i <= x + xrad; i++) {

      //      printf("i = %i, j = %i\n", i, j);

      r = (double) ((unsigned char) IMXY_R(im, i, j));
      if (r < hist_lower)
	r_histogram->x[0]++;
      else if (r > hist_upper)
	r_histogram->x[r_histogram->rows - 1]++;
      else
	r_histogram->x[(int) ((r - hist_lower) / hist_interval)]++;

      g = (double) ((unsigned char) IMXY_G(im, i, j));
      if (g < hist_lower)
	g_histogram->x[0]++;
      else if (g > hist_upper)
	g_histogram->x[g_histogram->rows - 1]++;
      else
	g_histogram->x[(int) ((g - hist_lower) / hist_interval)]++;

      b = (double) ((unsigned char) IMXY_B(im, i, j));
      if (b < hist_lower)
	b_histogram->x[0]++;
      else if (b > hist_upper)
	b_histogram->x[b_histogram->rows - 1]++;
      else
	b_histogram->x[(int) ((b - hist_lower) / hist_interval)]++;
    }

  // normalize

  scale_CR_Vector(inverse_area, r_histogram);
  scale_CR_Vector(inverse_area, g_histogram);
  scale_CR_Vector(inverse_area, b_histogram);

//   print_CR_Vector(r_histogram);
//   print_CR_Vector(g_histogram);
//   print_CR_Vector(b_histogram);

}

//----------------------------------------------------------------------------

void sobel_CR_Image(int threshold, CR_Image *im, CR_Image *edgeim)
{
  int i, j, temp;

  for (j = 0; j < im->h; j++)
    for (i = 0; i < im->w; i++) {
      
      temp = thresholded_point_sobel_3x3(i, j, threshold, im);

      /*      
      if (temp > 255)
	temp = 0;
      else
	temp = 255 - temp;
      */

      IMXY_R(edgeim, i, j) = (unsigned char) temp;
      IMXY_G(edgeim, i, j) = (unsigned char) temp;
      IMXY_B(edgeim, i, j) = (unsigned char) temp;

      //      IMXY_PIX(edgeim, i, j, temp, temp, temp);
    }
}
    
//----------------------------------------------------------------------------

int thresholded_point_sobel_3x3(int i, int j, int threshold, CR_Image *im)
{
  int x_edge, y_edge, intensity;

  // make sure we're trying to do this with the real image, and not outside or too
  // close to the edge

  //  if (!(i >= 1 && i < im->w - 1 && j >= 1 && j < im->h - 1))
  //    return 0;

  x_edge = 
    -IMXY_I(im, i - 1, j - 1) +     IMXY_I(im, i + 1, j - 1) +
    -2 * IMXY_I(im, i - 1, j)     + 2 * IMXY_I(im, i + 1, j) +
    -IMXY_I(im, i - 1, j + 1) +     IMXY_I(im, i + 1, j + 1);
  y_edge = 
    IMXY_I(im, i - 1, j - 1) + 2 * IMXY_I(im, i, j - 1) + IMXY_I(im, i + 1, j - 1) +
    -IMXY_I(im, i - 1, j + 1) - 2 * IMXY_I(im, i, j + 1) - IMXY_I(im, i + 1, j + 1);

  intensity = (int) CR_round(sqrt((double) (x_edge * x_edge + y_edge * y_edge)));

  //  printf("theta = %lf, y = %i, x = %i\n", atan2(y_edge, x_edge), y_edge, x_edge);

  if (intensity >= threshold) {
    return intensity;
    //    printf("intensity = %i, thresh = %i\n", intensity, threshold);
  }
  else
    return 0;
}

//----------------------------------------------------------------------------

int oriented_thresholded_point_sobel_3x3(int i, int j, double theta, int threshold, CR_Image *im)
{
  int x_edge, y_edge, intensity;
  double edge_theta, magnitude, theta_diff;

  // make sure we're trying to do this with the real image, and not outside or too
  // close to the edge

  //  if (!(i >= 1 && i < im->w - 1 && j >= 1 && j < im->h - 1))
  //    return 0;

  x_edge = 
    -IMXY_I(im, i - 1, j - 1) +     IMXY_I(im, i + 1, j - 1) +
    -2 * IMXY_I(im, i - 1, j)     + 2 * IMXY_I(im, i + 1, j) +
    -IMXY_I(im, i - 1, j + 1) +     IMXY_I(im, i + 1, j + 1);
  y_edge = 
    IMXY_I(im, i - 1, j - 1) + 2 * IMXY_I(im, i, j - 1) + IMXY_I(im, i + 1, j - 1) +
    -IMXY_I(im, i - 1, j + 1) - 2 * IMXY_I(im, i, j + 1) - IMXY_I(im, i + 1, j + 1);

  magnitude = sqrt((double) (x_edge * x_edge + y_edge * y_edge));
  edge_theta = atan2((float) y_edge, (float) x_edge);
  intensity = (int) CR_round(cos(fabs(edge_theta - theta)) * magnitude);

//   printf("edge theta = %lf, y = %i, x = %i\n", edge_theta, y_edge, x_edge);
//   printf("abs theta diff = %lf, factor = %lf\n", fabs(edge_theta - theta), cos(fabs(edge_theta - theta)));
//   printf("old intensity = %i, new intensity = %i, threshold = %i\n", (int) CR_round(magnitude), intensity, threshold);
//   printf("----------------------------------------------------------------\n");

  if (intensity >= threshold) {
    return intensity;
    //    printf("intensity = %i, thresh = %i\n", intensity, threshold);
  }
  else
    return 0;
}

//----------------------------------------------------------------------------

// compute mean gradient magnitude, angular difference along line 

//     grad_x = IMXY_I(im, x + 1, y) - IMXY_I(im, x, y);
//     grad_y = IMXY_I(im, x, y + 1) - IMXY_I(im, x, y);
// 
//     grad_mag = sqrt((double) (grad_x * grad_x + grad_y * grad_y));
//     grad_theta = atan2(grad_y, grad_x);

void grad_diff_CR_Image(int x, int y, double theta, double *mag, double *theta_diff, double *n, CR_Image *im)
{
  int grad_x, grad_y;
  double grad_theta;

  grad_x = IMXY_I(im, x + 1, y) - IMXY_I(im, x, y);
  grad_y = IMXY_I(im, x, y + 1) - IMXY_I(im, x, y);

  *mag += sqrt((double) (grad_x * grad_x + grad_y * grad_y));
  grad_theta = atan2((float) grad_y, (float) grad_x);
  *theta_diff += fabs(theta - grad_theta + PI / 2);
  (*n)++;
}

void mean_gradient_along_line_CR_Image(int x1, int y1, int x2, int y2, CR_Image *im,
				       double theta, double *mean_theta_diff, double *mean_mag)
{
  int dx, dy, bx, by, xsign, ysign, p, const1, const2, sign, temp;
  double n;

  *mean_theta_diff = 0.0;
  *mean_mag = 0.0;
  n = 0.0;

  bx = x1;
  by = y1;
  dx = (x2 - x1);
  dy = (y2 - y1);

  if (dx == 0 && dy == 0) {
    grad_diff_CR_Image(bx, by, theta, mean_mag, mean_theta_diff, &n, im);
  }
  else if (dy == 0) {                            // have a horizontal line 
    xsign = dx / abs(dx);
    grad_diff_CR_Image(bx, by, theta, mean_mag, mean_theta_diff, &n, im);
    while (bx != x2) {
      bx += xsign;
      grad_diff_CR_Image(bx, by, theta, mean_mag, mean_theta_diff, &n, im);
    }
  }
  else if (dx == 0) {                        // have vertical line 
    ysign = dy / abs(dy);
    grad_diff_CR_Image(bx, by, theta, mean_mag, mean_theta_diff, &n, im);
    while (by != y2) {
      by += ysign;
      grad_diff_CR_Image(bx, by, theta, mean_mag, mean_theta_diff, &n, im);
    }
  }
  else {                                      // use Bresenham algorithm
    xsign = dx / abs(dx);
    ysign = dy / abs(dy);
    dx = abs(dx);
    dy = abs(dy);
    grad_diff_CR_Image(bx, by, theta, mean_mag, mean_theta_diff, &n, im);
    if (dx < dy) {                       // line more vertical than horizontal 
      p = 2 * dx - dy;
      const1 = 2 * dx;
      const2 = 2 * (dx - dy);
      while (by != y2) {
	by = by + ysign;
	if (p < 0) 
	  p = p + const1;
	else {
	  p = p + const2;
	  bx = bx + xsign;
	}
	grad_diff_CR_Image(bx, by, theta, mean_mag, mean_theta_diff, &n, im);
      }
    }
    else {                               // line more horizontal than vertical 
      p = 2 * dy - dx;
      const2 = 2 * (dy - dx);
      const1 = 2 * dy;
      while (bx != x2) {
	bx = bx + xsign;
	if (p < 0) 
	  p = p + const1;
	else {
	  p = p + const2;
	  by = by + ysign;
	}
	grad_diff_CR_Image(bx, by, theta, mean_mag, mean_theta_diff, &n, im);
      }
    }
  }

  *mean_theta_diff /= n;
  *mean_mag /= n;
  //  printf("n = %i\n", (int) n);
}

//----------------------------------------------------------------------------

// best edge along line...assumed that line has been clipped to desired bounding
// box already

void best_sobel_edge_along_line_CR_Image(int x1, int y1, int x2, int y2, CR_Image *im,
					 int *curmax, int *x_max, int *y_max, int edge_thresh)
{
  int dx, dy, bx, by, xsign, ysign, p, const1, const2, sign, temp;
  double theta;

//   theta = atan2(y2 - y1, x2 - x1);
//   printf("theta = %lf, y2 - y1 = %i, x2 - x1 = %i, x1 = %i, y1 = %i, x2 = %i, y2 = %i\n", theta, y2 - y1, x2 - x1, x1, y1, x2, y2);
  theta = atan2((float) (y1 - y2), (float) (x2 - x1));
  //  printf("theta = %lf, y1 - y2 = %i, x2 - x1 = %i, x1 = %i, y1 = %i, x2 = %i, y2 = %i\n", theta, y1 - y2, x2 - x1, x1, y1, x2, y2);

  //  *curmax = thresholded_point_sobel_3x3(x1, y1, edge_thresh, im);
  *curmax = oriented_thresholded_point_sobel_3x3(x1, y1, theta, edge_thresh, im);
  *x_max = x1;
  *y_max = y1;

  bx = x1;
  by = y1;
  dx = (x2 - x1);
  dy = (y2 - y1);

  if (dx == 0 && dy == 0) {
    //    if ((temp = thresholded_point_sobel_3x3(bx, by, edge_thresh, im)) > *curmax) {
    if ((temp = oriented_thresholded_point_sobel_3x3(bx, by, theta, edge_thresh, im)) > *curmax) {
      *curmax = temp;
      *x_max = bx;
      *y_max = by;
    }
  }
  else if (dy == 0) {                            // have a horizontal line 
    xsign = dx / abs(dx);
    //    if ((temp = thresholded_point_sobel_3x3(bx, by, edge_thresh, im)) > *curmax) {
    if ((temp = oriented_thresholded_point_sobel_3x3(bx, by, theta, edge_thresh, im)) > *curmax) {
      *curmax = temp;
      *x_max = bx;
      *y_max = by;
    }
    while (bx != x2) {
      bx += xsign;
      //      if ((temp = thresholded_point_sobel_3x3(bx, by, edge_thresh, im)) > *curmax) {
      if ((temp = oriented_thresholded_point_sobel_3x3(bx, by, theta, edge_thresh, im)) > *curmax) {
	*curmax = temp;
	*x_max = bx;
	*y_max = by;
      }
    }
  }
  else if (dx == 0) {                        // have vertical line 
    ysign = dy / abs(dy);
    //    if ((temp = thresholded_point_sobel_3x3(bx, by, edge_thresh, im)) > *curmax) {
    if ((temp = oriented_thresholded_point_sobel_3x3(bx, by, theta, edge_thresh, im)) > *curmax) {
      *curmax = temp;
      *x_max = bx;
      *y_max = by;
    }
    while (by != y2) {
      by += ysign;
      //      if ((temp = thresholded_point_sobel_3x3(bx, by, edge_thresh, im)) > *curmax) {
      if ((temp = oriented_thresholded_point_sobel_3x3(bx, by, theta, edge_thresh, im)) > *curmax) {
	*curmax = temp;
	*x_max = bx;
	*y_max = by;
      }
    }
  }
  else {                                      // use Bresenham algorithm
    xsign = dx / abs(dx);
    ysign = dy / abs(dy);
    dx = abs(dx);
    dy = abs(dy);
    //    if ((temp = thresholded_point_sobel_3x3(bx, by, edge_thresh, im)) > *curmax) {
    if ((temp = oriented_thresholded_point_sobel_3x3(bx, by, theta, edge_thresh, im)) > *curmax) {
      *curmax = temp;
      *x_max = bx;
      *y_max = by;
    }
    if (dx < dy) {                       // line more vertical than horizontal 
      p = 2 * dx - dy;
      const1 = 2 * dx;
      const2 = 2 * (dx - dy);
      while (by != y2) {
	by = by + ysign;
	if (p < 0) 
	  p = p + const1;
	else {
	  p = p + const2;
	  bx = bx + xsign;
	}
	//	if ((temp = thresholded_point_sobel_3x3(bx, by, edge_thresh, im)) > *curmax) {
	if ((temp = oriented_thresholded_point_sobel_3x3(bx, by, theta, edge_thresh, im)) > *curmax) {
	  *curmax = temp;
	  *x_max = bx;
	  *y_max = by;
	}
      }
    }
    else {                               // line more horizontal than vertical 
      p = 2 * dy - dx;
      const2 = 2 * (dy - dx);
      const1 = 2 * dy;
      while (bx != x2) {
	bx = bx + xsign;
	if (p < 0) 
	  p = p + const1;
	else {
	  p = p + const2;
	  by = by + ysign;
	}
	//	if ((temp = thresholded_point_sobel_3x3(bx, by, edge_thresh, im)) > *curmax) {
	if ((temp = oriented_thresholded_point_sobel_3x3(bx, by, theta, edge_thresh, im)) > *curmax) {
	  *curmax = temp;
	  *x_max = bx;
	  *y_max = by;
	}
      }
    }
  }
//   printf("----------------------------------------------------------------\n");
//   printf("----------------------------------------------------------------\n");
}

//----------------------------------------------------------------------------

// if line L = (x1, y1)->(x2, y2) is completely outside of rectangle
// R = (rect_x1, rect_y1)->(rect_x2, rect_y2), returns FALSE.
// else returns TRUE, and ...
// (1) if L is completely inside R, does nothing
// (2) if, WLOG, (x1, y1) is outside of R and (x2, y2) is inside,
//     changes (x1, y1) to point on R where L intersects it

// uses Cohen-Sutherland algorithm

// define the coding of end points

typedef unsigned int code;
enum {TOP = 0x1, BOTTOM = 0x2, RIGHT = 0x4, LEFT = 0x8};

// compute the code of a point relative to a rectangle

code ComputeCode(double x, double y, int xleft, int ytop, int xright, int ybottom)
{
  code c = 0;

  if (y < ytop)
    c |= TOP;
  else if (y > ybottom)
    c |= BOTTOM;
  if (x > xright)
    c |= RIGHT;
  else if (x < xleft)
    c |= LEFT;

  return c;
}

int cohen_sutherland_clip(double *x1, double *y1, double *x2, double *y2, int xleft, int ytop, int xright, int ybottom)
{
  code C0, C1, C;
  double x, y;
	
  C0 = ComputeCode(*x1, *y1, xleft, ytop, xright, ybottom);
  C1 = ComputeCode(*x2, *y2, xleft, ytop, xright, ybottom);
	
  for (;;) {

    // trivial accept: both ends in rectangle 

    if ((C0 | C1) == 0) 
      return TRUE;
    
    // trivial reject: both ends on the external side of the rectangle 

    if ((C0 & C1) != 0)
      return FALSE;
    
    // normal case: clip end outside rectangle 

    C = C0 ? C0 : C1;

    if (C & TOP) {
      x = *x1 + (*x2 - *x1) * (ytop - *y1) / (*y2 - *y1);
      //x = *x1 + (*x2 - *x1) * (*y1 - ytop) / (*y2 - *y1);
      y = ytop;
    } 
    else if (C & BOTTOM) {
      x = *x1 + (*x2 - *x1) * (ybottom - *y1) / (*y2 - *y1);
      //x = *x1 + (*x2 - *x1) * (*y1 - ybottom) / (*y2 - *y1);
      y = ybottom;
    } 
    else if (C & RIGHT) {
      x = xright;
      y = *y1 + (*y2 - *y1) * (xright - *x1) / (*x2 - *x1);
    } 
    else {
      x = xleft;
      y = *y1 + (*y2 - *y1) * (xleft - *x1) / (*x2 - *x1);
    }
    
    // set new end point and iterate 

    if (C == C0) {
      *x1 = x; 
      *y1 = y;
      C0 = ComputeCode(*x1, *y1, xleft, ytop, xright, ybottom);
    } 
    else {
      *x2 = x; 
      *y2 = y;
      C1 = ComputeCode(*x1, *y2, xleft, ytop, xright, ybottom);
    }
  }
  // not reached 
}

int clip_line_to_rect_CR_Image(int *x1, int *y1, int *x2, int *y2, int xleft, int ytop, int xright, int ybottom)
{
  double dx1, dy1, dx2, dy2;
  int result;

  dx1 = (double) *x1;
  dy1 = (double) *y1;
  dx2 = (double) *x2;
  dy2 = (double) *y2;

  result = cohen_sutherland_clip(&dx1, &dy1, &dx2, &dy2, xleft, ytop, xright, ybottom);

  *x1 = (int) CR_round(dx1);
  *y1 = (int) CR_round(dy1);
  *x2 = (int) CR_round(dx2);
  *y2 = (int) CR_round(dy2);

  return result;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// run filter on image divided into 2 regions: inside the area specified by the
// polygon Region, and everything else...then write responses to file

// add link to this function to image processing submenu

void write_region_filter_responses_CR_Image(char *filename, CR_Image_Filter *im_filter, CR_Matrix *Region, CR_Image *im)
{
  int strpos, i, j, k, x, xsub, ysub, data_points;
  FILE *fp;
  CR_Matrix *all_responses;
  CR_Vector *response;

  if (!Region) {
    printf("write_region_filter_responses_CR_Image(): no region defined\n");
    return;
  }

  // cutting down on the data a bit

  xsub = 5;
  ysub = 5;

  // open file and print filter name, parameters in comments (in file)

  fp = fopen(filename, "w");

  for (strpos = strlen(filename) - 1; strpos >= 0; strpos--)
    if (filename[strpos] == '\\')
      break;
  filename[strlen(filename)-2] = '\0';

  data_points = 0;
  for (j = im_filter->h_margin; j < im->h - im_filter->h_margin; j += ysub)
    for (i = im_filter->w_margin; i < im->w - im_filter->w_margin; i += xsub) 
      data_points++;

  //  data_points = ((im->w - 2 * im_filter->w_margin) * (im->h - 2 * im_filter->h_margin)) / (xsub * ysub);
  //  CR_flush_printf("data = %i, pred data = %i\n", data_points, ((im->w - 2 * im_filter->w_margin) * (im->h - 2 * im_filter->h_margin)) / (xsub * ysub));
  //  CR_exit(1);

  fprintf(fp, "%% %s\n", &filename[strpos + 1]);
  fprintf(fp, "%% Subsampling: x = %i, y = %i\n", xsub, ysub);
  fprintf(fp, "%% Margin: w = %i, h = %i\n", im_filter->w_margin, im_filter->h_margin);
  fprintf(fp, "%% Number of data points = %i\n\n", data_points);

  // run filter over image; for each pixel, fill in its  x, y position, the pixel label (road/off-road),
  //    and the filter response (raw -- we'll quantize the responses later) as
  //    "x, y, label, response1, response2, ..., responseN"

  response = make_CR_Vector("response", im_filter->response_dimension);
  all_responses = make_CR_Matrix(&filename[strpos + 1], data_points, 3 + response->rows);

  x = 0;
  for (j = im_filter->h_margin; j < im->h - im_filter->h_margin; j += ysub)
    for (i = im_filter->w_margin; i < im->w - im_filter->w_margin; i += xsub) {   

      CR_flush_printf("i = %i, j = %i\n", i, j);

      CR_error("filter_pixel() function taken out for now");
      //      filter_pixel_CR_Image_Filter(i, j, im_filter, im, response);
      //      print_CR_Vector(response);
      MATRC(all_responses, x, 0) = i;   // x coordinate
      MATRC(all_responses, x, 1) = j;   // y coordinate
      if (point_in_poly_CR_Matrix(i, j, Region))
	MATRC(all_responses, x, 2) = 1;
      else
	MATRC(all_responses, x, 2) = 0;
      for (k = 0; k < response->rows; k++)
	MATRC(all_responses, x, 3 + k) = response->x[k];
      x++;
    }

  fprint_CR_Matrix(fp, all_responses);
  CR_flush_printf("done\n");

  free_CR_Matrix(all_responses);
  free_CR_Vector(response);

  // close file

  fclose(fp);
}

//----------------------------------------------------------------------------

/*
void filter_pixel_CR_Image_Filter(int x, int y, CR_Image_Filter *im_filter, CR_Image *im, CR_Vector *response)
{
  int r, g, b, rleft, rright, rup, rdown, gleft, gright, gup, gdown, bleft, bright, bup, bdown;

  r = (unsigned char) IMXY_R(im, x, y);
  g = (unsigned char) IMXY_G(im, x, y);
  b = (unsigned char) IMXY_B(im, x, y);
 
  if (im_filter->type == CR_IMFILTER_NORM_COLOR) {
    
    response->x[0] = (double) r / (double) (r + g + b + 1);
    response->x[1] = (double) g / (double) (r + g + b + 1);
  }
  else if (im_filter->type == CR_IMFILTER_CCCI) {
 
    // skipping 2 horizontally vs. only 1 vertically because vertical dimension is already half what it should be

    rleft = (unsigned char) IMXY_R(im, x - 2, y);
    gleft = (unsigned char) IMXY_G(im, x - 2, y);
    bleft = (unsigned char) IMXY_B(im, x - 2, y);

    rright = (unsigned char) IMXY_R(im, x + 2, y);
    gright = (unsigned char) IMXY_G(im, x + 2, y);
    bright = (unsigned char) IMXY_B(im, x + 2, y);

    rup = (unsigned char) IMXY_R(im, x, y - 1);
    gup = (unsigned char) IMXY_G(im, x, y - 1);
    bup = (unsigned char) IMXY_B(im, x, y - 1);

    rdown = (unsigned char) IMXY_R(im, x, y + 1);
    gdown = (unsigned char) IMXY_G(im, x, y + 1);
    bdown = (unsigned char) IMXY_B(im, x, y + 1);
    
    response->x[0] = log(rleft + 1) + log(rright + 1) + log(rup + 1) + log(rdown + 1) - 4 * log(r + 1);
    response->x[1] = log(gleft + 1) + log(gright + 1) + log(gup + 1) + log(gdown + 1) - 4 * log(g + 1);
    response->x[2] = log(bleft + 1) + log(bright + 1) + log(bup + 1) + log(bdown + 1) - 4 * log(b + 1);
  }
  else if (im_filter->type == CR_IMFILTER_RGB) {

    response->x[0] = r;
    response->x[1] = g;
    response->x[2] = b;
  }
  else if (im_filter->type == CR_IMFILTER_RGB_3x3) {

    response->x[0] = (unsigned char) IMXY_R(im, x-1, y-1);
    response->x[1] = (unsigned char) IMXY_G(im, x-1, y-1);
    response->x[2] = (unsigned char) IMXY_B(im, x-1, y-1);

    response->x[3] = (unsigned char) IMXY_R(im, x, y-1);
    response->x[4] = (unsigned char) IMXY_G(im, x, y-1);
    response->x[5] = (unsigned char) IMXY_B(im, x, y-1);

    response->x[6] = (unsigned char) IMXY_R(im, x+1, y-1);
    response->x[7] = (unsigned char) IMXY_G(im, x+1, y-1);
    response->x[8] = (unsigned char) IMXY_B(im, x+1, y-1);

    response->x[9] = (unsigned char) IMXY_R(im, x-1, y);
    response->x[10] = (unsigned char) IMXY_G(im, x-1, y);
    response->x[11] = (unsigned char) IMXY_B(im, x-1, y);

    response->x[12] = (unsigned char) IMXY_R(im, x, y);
    response->x[13] = (unsigned char) IMXY_G(im, x, y);
    response->x[14] = (unsigned char) IMXY_B(im, x, y);

    response->x[15] = (unsigned char) IMXY_R(im, x+1, y);
    response->x[16] = (unsigned char) IMXY_G(im, x+1, y);
    response->x[17] = (unsigned char) IMXY_B(im, x+1, y);

    response->x[18] = (unsigned char) IMXY_R(im, x-1, y+1);
    response->x[19] = (unsigned char) IMXY_G(im, x-1, y+1);
    response->x[20] = (unsigned char) IMXY_B(im, x-1, y+1);

    response->x[21] = (unsigned char) IMXY_R(im, x, y+1);
    response->x[22] = (unsigned char) IMXY_G(im, x, y+1);
    response->x[23] = (unsigned char) IMXY_B(im, x, y+1);

    response->x[24] = (unsigned char) IMXY_R(im, x+1, y+1);
    response->x[25] = (unsigned char) IMXY_G(im, x+1, y+1);
    response->x[26] = (unsigned char) IMXY_B(im, x+1, y+1);
  }
  else
    CR_error("filter_pixel_CR_Image_Filter(): unknown filter type");
}
*/

//----------------------------------------------------------------------------

// the last filter defined in this function is the default filter unless 
// the type is subsequently changed

CR_Image_Filter *make_CR_Image_Filter(CR_Image *im)
{
  CR_Image_Filter *im_filter;

  im_filter = (CR_Image_Filter *) CR_malloc(sizeof(CR_Image_Filter));
  im_filter->name = (char *) CR_calloc(64, sizeof(char));

  // SVD color model

  im_filter->type = CR_IMFILTER_SVD_COLOR;
  sprintf(im_filter->name, "SVDColor");
  im_filter->response_dimension = 1;
  im_filter->w_margin = im_filter->h_margin = 0;

  im_filter->svd_color_model = FALSE;
  im_filter->Dist = make_CR_Matrix("Dist", im->h, im->w);
  im_filter->Mean = make_CR_Vector("Mean", 3);
  im_filter->Cov = make_CR_Matrix("Cov", 3, 3);
  im_filter->InvCov = make_CR_Matrix("InvCov", 3, 3);

  // Normalized color model

  im_filter->type = CR_IMFILTER_NORM_COLOR;
  sprintf(im_filter->name, "NormColor");
  im_filter->response_dimension = 2;
  im_filter->w_margin = im_filter->h_margin = 0;

  // Funt's Color Constant Color Indexing

  /*
  im_filter->type = CR_IMFILTER_CCCI;
  sprintf(im_filter->name, "CCCI");
  im_filter->response_dimension = 3;
  im_filter->w_margin = 2;
  im_filter->h_margin = 1;
  */

  // Just the colors themselves

  im_filter->type = CR_IMFILTER_RGB;
  sprintf(im_filter->name, "RGB");
  im_filter->response_dimension = 3;
  im_filter->w_margin = im_filter->h_margin = 0;

  // the 3x3 neighborhood of colors 

  im_filter->type = CR_IMFILTER_RGB_3x3;
  sprintf(im_filter->name, "RGB_3x3");
  im_filter->response_dimension = 27;
  //  im_filter->w_margin = im_filter->h_margin = 1;
  im_filter->w_margin = im_filter->h_margin = 1;

  // return

  return im_filter;
}

//----------------------------------------------------------------------------

void free_CR_Image_Filter(CR_Image_Filter *im_filter)
{
  // SVD color model

  free_CR_Matrix(im_filter->Dist);
  free_CR_Vector(im_filter->Mean);
  free_CR_Matrix(im_filter->Cov);
  free_CR_Matrix(im_filter->InvCov);

  // finish

  CR_free_malloc(im_filter, sizeof(CR_Image_Filter));
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// vertically flip image im in place

void vflip_CR_Image(CR_Image *src, CR_Image *dst)
{
  iplMirror(src->iplim, dst->iplim, 0);   // last arg should be 1 for a vertical axis, -1 for both
}

//----------------------------------------------------------------------------

// this is just a shell for IplImage at the moment

CR_Image *make_CR_Image(int w, int h, int type)
{
  CR_Image *im;

  im = (CR_Image *) CR_malloc(sizeof(CR_Image));
  im->type = type;

  im->iplim = make_IplImage(w, h, type);

  if (type == IPL_TYPE_COLOR_8U_YUV) {
    im->iplim->colorModel[0] = 'Y';
    im->iplim->colorModel[1] = 'U';
    im->iplim->colorModel[2] = 'V';

    im->iplim->channelSeq[0] = 'Y';
    im->iplim->channelSeq[1] = 'U';
    im->iplim->channelSeq[2] = 'V';
    //    printf("%s\n", im->iplim->channelSeq);
  }

  // im->type ???
  im->w = im->iplim->width;
  im->h = im->iplim->height;
  im->wstep = im->iplim->widthStep;
  im->data = im->iplim->imageData;
  set_roi_CR_Image(0, 0, im->w, im->h, im);

  im->poly_region = NULL;
 
  im->buffsize = w * h * 3;   // this depends on the type---assuming RGB24 for now

  return im;
}

//----------------------------------------------------------------------------

// this is just a shell for IplImage at the moment

CR_Image *make_FP_CR_Image(int w, int h, int type)
{
  CR_Image *im;

  im = (CR_Image *) CR_malloc(sizeof(CR_Image));

  im->iplim = make_FP_IplImage(w, h, type);

  // im->type ???
  im->w = im->iplim->width;
  im->h = im->iplim->height;
  im->wstep = im->iplim->widthStep;
  im->data = im->iplim->imageData;
  set_roi_CR_Image(0, 0, im->w, im->h, im);

  im->poly_region = NULL;
 
  return im;
}

//----------------------------------------------------------------------------

void free_CR_Image(CR_Image *im)
{
  free_IplImage(im->iplim);
  CR_free_malloc(im, sizeof(CR_Image));
}

//----------------------------------------------------------------------------

// set whole image without regard for ROI

void set_CR_Image(int value, CR_Image *im)
{
  int x, y, w, h;

  // save old ROI

  get_roi_CR_Image(&x, &y, &w, &h, im);

  // set ROI to whole image for setting

  set_roi_CR_Image(0, 0, im->w, im->h, im);

  iplSet(im->iplim, value);

  // restore old ROI

  set_roi_CR_Image(x, y, w, h, im);
}

//----------------------------------------------------------------------------

// copy without regard for ROIs

void ip_copy_CR_Image(CR_Image *src, CR_Image *dst)
{
  int src_x, src_y, src_w, src_h;
  int dst_x, dst_y, dst_w, dst_h;

  // save old ROIs

  get_roi_CR_Image(&src_x, &src_y, &src_w, &src_h, src);
  get_roi_CR_Image(&dst_x, &dst_y, &dst_w, &dst_h, dst);

  // set ROIs to whole image for copying

  set_roi_CR_Image(0, 0, src->w, src->h, src);
  set_roi_CR_Image(0, 0, dst->w, dst->h, dst);

  iplCopy(src->iplim, dst->iplim);

  // restore old ROIs

  set_roi_CR_Image(src_x, src_y, src_w, src_h, src);
  set_roi_CR_Image(dst_x, dst_y, dst_w, dst_h, dst);
}

//----------------------------------------------------------------------------

CR_Image *copy_CR_Image(CR_Image *im)
{
  CR_Image *im_copy;

  im_copy = (CR_Image *) CR_malloc(sizeof(CR_Image));

  im_copy->iplim = iplCloneImage(im->iplim);

  // im_copy->type ???
  im_copy->w = im_copy->iplim->width;
  im_copy->h = im_copy->iplim->height;
  im_copy->wstep = im_copy->iplim->widthStep;
  im_copy->data = im_copy->iplim->imageData;
  im_copy->iplim->roi->xOffset = im->iplim->roi->xOffset;
  im_copy->iplim->roi->yOffset = im->iplim->roi->yOffset;
  im_copy->iplim->roi->width = im->iplim->roi->width;
  im_copy->iplim->roi->height = im->iplim->roi->height;

  return im_copy;
}

//----------------------------------------------------------------------------

// given a string, a number, and a filename extension (such as ".ppm"), 
// returns string with number (in string form) appended to it

// assumes number is no more than four digits

char *num_suffix_plus_extension(char *s, int n, char *ext)
{
  char *numstring = (char *) CR_calloc(4, sizeof(char));
  char *suf_string = (char *) CR_calloc(256, sizeof(char));

  sprintf(numstring, "%i", n);
  //  return strcat(strcpy(suf_string, s), numstring);
  strcat(strcpy(suf_string, s), numstring);
  sprintf(&suf_string[strlen(suf_string)], "%s", ext);
 
  //  free(numstring);
  CR_free_calloc(numstring, 4, sizeof(char));

  return suf_string;
}

//----------------------------------------------------------------------------

// figures out desired image format from filename extension

void write_CR_Image(char *filename, CR_Image *im)
{
  int image_type;

  image_type = determine_type_CR_Image(filename);

  if (image_type == CR_IMAGE_PPM)
    write_IplImage_to_PPM(filename, im->iplim);
  else if (image_type == CR_IMAGE_PFM)
    write_IplImage_to_PFM(filename, im->iplim);
  else 
    CR_error("write_CR_Image(): can not handle that kind of image right now");
}

//----------------------------------------------------------------------------

CR_Image *read_CR_Image(char *filename)
{
  CR_Image *im;
  int image_type;

  image_type = determine_type_CR_Image(filename);

  if (image_type == CR_IMAGE_PPM)
    im = read_PPM_to_CR_Image(filename);
  else if (image_type == CR_IMAGE_JPEG)
    im = read_JPEG_to_CR_Image(filename);
  else 
    CR_error("read_CR_Image(): can only handle ppm, rif images");

  // may not be true in the future, but it is now

  im->type = IPL_TYPE_COLOR_8U;  

  return im;
}

//----------------------------------------------------------------------------

CR_Image *read_JPEG_to_CR_Image(char *filename)
{
  CR_Image *im;

  im = (CR_Image *) CR_malloc(sizeof(CR_Image));

  im->iplim = CreateImageFromJPEG(filename);

  // im->type ???
  im->w = im->iplim->width;
  im->h = im->iplim->height;
  im->wstep = im->iplim->widthStep;
  im->data = im->iplim->imageData;

  return im;
}

//----------------------------------------------------------------------------

void read_PPM_to_CR_Image(char *filename, CR_Image *im)
{
  read_PPM_to_IplImage(filename, im->iplim);
}

//----------------------------------------------------------------------------

CR_Image *read_PPM_to_CR_Image(char *filename)
{
  CR_Image *im;

  im = (CR_Image *) CR_malloc(sizeof(CR_Image));

  im->iplim = read_PPM_to_IplImage(filename);

  // im->type ???
  im->w = im->iplim->width;
  im->h = im->iplim->height;
  im->wstep = im->iplim->widthStep;
  im->data = im->iplim->imageData;

  return im;
}

//----------------------------------------------------------------------------

// just use suffix

int determine_type_CR_Image(char *filename)
{
  char suffix[4];
  int len;

  len = strlen(filename);
  suffix[0] = filename[len - 3];
  suffix[1] = filename[len - 2];
  suffix[2] = filename[len - 1];
  suffix[3] = '\0';

  //  printf("suffix = %s\n", suffix);

  // Images

  if (!strcmp(suffix, "ppm") || !strcmp(suffix, "PPM"))
    return CR_IMAGE_PPM;
  else if (!strcmp(suffix, "pgm") || !strcmp(suffix, "PGM"))
    return CR_IMAGE_PPM;
  else if (!strcmp(suffix, "pfm") || !strcmp(suffix, "PFM"))
    return CR_IMAGE_PFM;
  else if (!strcmp(suffix, "jpg") || !strcmp(suffix, "JPG"))
    return CR_IMAGE_JPEG;
  else if (!strcmp(suffix, "bmp") || !strcmp(suffix, "BMP"))
    return CR_IMAGE_BITMAP;
  else if (!strcmp(suffix, "rif") || !strcmp(suffix, "RIF"))
    return CR_IMAGE_DEMO3_RIF;
  else if (!strcmp(suffix, "ps") || !strcmp(suffix, "PS") || !strcmp(suffix, "eps") || !strcmp(suffix, "EPS"))
    return CR_IMAGE_EPS;

  // Unknown

  else 
    return CR_UNKNOWN;
}

//----------------------------------------------------------------------------

void get_roi_CR_Image(int *x, int *y, int *w, int *h, CR_Image *im)
{
  *x = im->iplim->roi->xOffset;
  *y = im->iplim->roi->yOffset;
  *w = im->iplim->roi->width;
  *h = im->iplim->roi->height;
}

//----------------------------------------------------------------------------

void set_roi_CR_Image(int x, int y, int w, int h, CR_Image *im)
{
  im->iplim->roi->xOffset = im->roi_x = x;
  im->iplim->roi->yOffset = im->roi_y = y;
  im->iplim->roi->width = im->roi_w = w;
  im->iplim->roi->height = im->roi_h = h;
}

//----------------------------------------------------------------------------

void ip_convert_DIB_to_CR_Image(LPBITMAPINFOHEADER pBmpInfoHeader, CR_Image *im)
{
  iplConvertFromDIB(pBmpInfoHeader, im->iplim);
}

//----------------------------------------------------------------------------

void ip_convert_CR_Image_to_DIB(CR_Image *im, LPBITMAPINFOHEADER pBmpInfoHeader)
{
  iplConvertToDIB(im->iplim, pBmpInfoHeader, IPL_DITHER_NONE, IPL_PALCONV_NONE);
}

//----------------------------------------------------------------------------

void set_pixel_CR_Image(int x, int y, unsigned char *pixel, CR_Image *im)
{
  iplPutPixel(im->iplim, x, y, pixel);
}

//----------------------------------------------------------------------------

void set_pixel_rgb_CR_Image(int x, int y, int r, int g, int b, CR_Image *im)
{
  static unsigned char pixel[3];

  pixel[IPL_RED] = (unsigned char) r;
  pixel[IPL_GREEN] = (unsigned char) g;
  pixel[IPL_BLUE] = (unsigned char) b;

  set_pixel_CR_Image(x, y, pixel, im);
}

//----------------------------------------------------------------------------

// fit a Gaussian to the RGB distribution of the pixels within a rectangular
// region of interest in image im, leaving out pixels which are regarded as 
// outliers according to the definition specified by outlier_type

int rgb_mean_and_covariance_CR_Image(CR_Image *im, int outlier_def, CR_Vector *Mean, CR_Matrix *Cov)
{
  int cur, i, j;
  int nonsats;              // number of unsaturated pixels in color sample
  CR_Vector *clean_r;       // array of red pixel values from color sample with outliers removed 
  CR_Vector *clean_g;       // array of green pixel values from color sample with outliers removed 
  CR_Vector *clean_b;       // array of blue pixel values from color sample with outliers removed 
  int x, y, w, h;
  double covdet;

  // check bounds

  if (!im->iplim || !im->iplim->roi)
    CR_error("rgb_mean_and_covariance_CR_Image(): ROI must be set");

  x = im->roi_x;
  y = im->roi_y;
  w = im->roi_w;
  h = im->roi_h;

  if (x < 0 || x >= im->w || x + w > im->w ||
      y < 0 || y >= im->h || y + h > im->h)
    CR_error("rgb_mean_and_covariance_CR_Image(): ROI not completely within the image");

  // count unsaturated pixels in sample image (or all pixels if we don't care)

  if (outlier_def == OUTLIER_SATURATED) {

    for (j = y, nonsats = 0; j < y + h; j++)
      for (i = x; i < x + w; i++) {	
	if ((unsigned char) IMXY_R(im, i, j) != (COLOR_LEVELS - 1) && 
	    (unsigned char) IMXY_G(im, i, j) != (COLOR_LEVELS - 1) && 
	    (unsigned char) IMXY_B(im, i, j) != (COLOR_LEVELS - 1))
	  nonsats++;
      }
  }
  else 
    nonsats = w * h;

  if (!nonsats) {
    printf("All sample pixels were saturated\n");
    return FALSE;
  }

  // transfer pixels from sample image to matrix form

  clean_r = make_CR_Vector("clean_r", nonsats); 
  clean_g = make_CR_Vector("clean_g", nonsats); 
  clean_b = make_CR_Vector("clean_b", nonsats); 
  for (j = y, cur = 0; j < y + h; j++)
    for (i = x; i < x + w; i++) {	
      if (outlier_def != OUTLIER_SATURATED ||
	  ((unsigned char) IMXY_R(im, i, j) != (COLOR_LEVELS - 1) && 
	   (unsigned char) IMXY_G(im, i, j) != (COLOR_LEVELS - 1) && 
	   (unsigned char) IMXY_B(im, i, j) != (COLOR_LEVELS - 1))) {
	clean_r->x[cur] = (unsigned char) IMXY_R(im, i, j);
	clean_g->x[cur] = (unsigned char) IMXY_G(im, i, j);
	clean_b->x[cur] = (unsigned char) IMXY_B(im, i, j);
	cur++;
      }
    }

  // calculate mean and covariance of what's left

  mean_and_covariance_nx3_CR_Matrix(clean_r, clean_g, clean_b, Mean, Cov);

  covdet = determinant_CR_Matrix(Cov);
  if (covdet < 0.00001) {
    printf("Poorly scaled covariance\n");
    return FALSE;
  }
    
  // clean up
  
  free_CR_Vector(clean_r);
  free_CR_Vector(clean_g);
  free_CR_Vector(clean_b);

  // finish

  return TRUE;
}

//----------------------------------------------------------------------------

void rgb_mahalanobis_distance_CR_Image(CR_Image *im, CR_Matrix *Dist, CR_Vector *Mean, CR_Matrix *InvCov)
{
  int i, j;
  static double V[3];

  for (j = im->roi_y; j < im->roi_y + im->roi_h; j++)
    for (i = im->roi_x; i < im->roi_x + im->roi_w; i++) {
      V[0] = (unsigned char) IMXY_R(im, i, j);
      V[1] = (unsigned char) IMXY_G(im, i, j);
      V[2] = (unsigned char) IMXY_B(im, i, j);
      MATRC(Dist, j, i) = mahalanobis_3x3_CR_Matrix(V, Mean, InvCov);
    }
}

//----------------------------------------------------------------------------

// upper left, upper right corner

void draw_rect_CR_Image(int x, int y, int w, int h, int r, int g, int b, CR_Image *im)
{
  int i;

  // top and bottom

  for (i = x; i < x + w; i++) {

    IMXY_R(im, i, y) = (unsigned char) r;
    IMXY_G(im, i, y) = (unsigned char) g;
    IMXY_B(im, i, y) = (unsigned char) b;

    IMXY_R(im, i, y + h - 1) = (unsigned char) r;
    IMXY_G(im, i, y + h - 1) = (unsigned char) g;
    IMXY_B(im, i, y + h - 1) = (unsigned char) b;
  }

  // left and right

  for (i = y; i < y + h; i++) {

    IMXY_R(im, x, i) = (unsigned char) r;
    IMXY_G(im, x, i) = (unsigned char) g;
    IMXY_B(im, x, i) = (unsigned char) b;

    IMXY_R(im, x + w - 1, i) = (unsigned char) r;
    IMXY_G(im, x + w - 1, i) = (unsigned char) g;
    IMXY_B(im, x + w - 1, i) = (unsigned char) b;
  }
}

//----------------------------------------------------------------------------

// I = uint8(255*((gabor_f - min(min(gabor_f)))/(max(max(gabor_f))- ... 
// 					      min(min(gabor_f))))); 


void interp_draw_CR_Matrix_CR_Image(CR_Matrix *M, CR_Image *im)
{
  int r, c, temp;
  double matmin, matmax, interval;

  matmin = matmax = MATRC(M, 0, 0);

  for (r = 0; r < M->rows; r++)
    for (c = 0; c < M->cols; c++) {
      if (MATRC(M, r, c) < matmin)
	matmin = MATRC(M, r, c);
      if (MATRC(M, r, c) > matmax)
	matmax = MATRC(M, r, c);
    }

  //  printf("max = %lf, min = %lf\n", matmax, matmin);

  interval = matmax - matmin;
  //  printf("%lf %lf %lf\n", matmin, matmax, interval);

  for (r = 0; r < M->rows; r++)
    for (c = 0; c < M->cols; c++) {
      temp = (int) CR_round(255.0 * (MATRC(M, r, c) - matmin) / interval);
      IMXY_R(im, c, r) = (unsigned char) temp;
      IMXY_G(im, c, r) = (unsigned char) temp;
      IMXY_B(im, c, r) = (unsigned char) temp;
    }
}

//----------------------------------------------------------------------------

// if zerobright = TRUE, display 0 as pure white and anything >= blackthresh as
// pure black, linearly interpolating intensity in between.  this is useful when 
// matrix mat contains #'s of standard deviations--sigmas--and thus values in it 
// can range from 0 to positive infinity.

// if zerobright = FALSE, display 0 as pure black and anything >= blackthresh as
// pure white, linearly interpolating intensity in between

void draw_CR_Matrix_CR_Image(double blackthresh, CR_Matrix *M, CR_Image *im, int zerobright)
{
  int i, j, temp;

  for (i = 0; i < M->rows; i++)
    for (j = 0; j < M->cols; j++) {
      if (zerobright) {
	if (MATRC(M, i, j) > blackthresh) {
	  IMXY_R(im, j, i) = (unsigned char) 0;
	  IMXY_G(im, j, i) = (unsigned char) 0;
	  IMXY_B(im, j, i) = (unsigned char) 0;
	}
	else {
	  temp = (int) CR_round(255.0 * (blackthresh - MATRC(M, i, j)) / blackthresh);
	  IMXY_R(im, j, i) = (unsigned char) temp;
	  IMXY_G(im, j, i) = (unsigned char) temp;
	  IMXY_B(im, j, i) = (unsigned char) temp;
	}
      }
      else {
	if (MATRC(M, i, j) > blackthresh) {
	  IMXY_R(im, j, i) = (unsigned char) 0;
	  IMXY_G(im, j, i) = (unsigned char) 0;
	  IMXY_B(im, j, i) = (unsigned char) 0;
	}
	else {
	  temp = (int) CR_round(255.0 - 255.0 * (blackthresh - MATRC(M, i, j)) / blackthresh);
	  IMXY_R(im, j, i) = (unsigned char) temp;
	  IMXY_G(im, j, i) = (unsigned char) temp;
	  IMXY_B(im, j, i) = (unsigned char) temp;
	}
      }
    }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------


