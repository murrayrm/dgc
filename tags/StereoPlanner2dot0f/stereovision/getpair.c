#include "getpair.h"

int cameras_start(raw1394handle_t * handle_ptr,
                  int node2cam[],
                  dc1394_cameracapture cameras[],
		  int which_pair) {

  int numNodes;
  int i;
  char idstr[9];
  int numCameras;
  quadlet_t cam_ids[2];
  dc1394_camerainfo info[2];   
  nodeid_t * camera_nodes;

  // Open ohci and asign handle to it
  *handle_ptr = dc1394_create_handle(0);
  if (*handle_ptr==NULL)
  {
    fprintf( stderr, "Unable to aquire a raw1394 handle\n\n"
             "Please check \n"
	     "  - if the kernel modules `ieee1394',`raw1394' and `ohci1394' are loaded \n"
	     "  - if you have read/write access to /dev/raw1394\n\n");
    return(-1);
    }

  //  get the camera nodes
  numNodes = raw1394_get_nodecount(*handle_ptr);
  camera_nodes = dc1394_get_camera_nodes(*handle_ptr, &numCameras, 0);

  if(numCameras!=2) {
    fprintf( stderr, "%d cameras found, not 2\n",numCameras);
    dc1394_destroy_handle(*handle_ptr);
    return(-1);
  }

  for ( i = 0; i < 2; i++ ) {
    dc1394_get_camera_info(*handle_ptr, camera_nodes[i], &info[i]);
    cam_ids[i] = info[i].euid_64 & 0xffffffff;
    sprintf( idstr, "0x%08X",cam_ids[i] );
    switch(which_pair) {
    case HIGH_RES:
      if(strcmp(idstr, LEFT_CAMERA_STRING)==0) {
	node2cam[i] = LEFT_CAM;
      } else if ( strcmp( idstr, RIGHT_CAMERA_STRING)==0) {
	node2cam[i] = RIGHT_CAM;
      } else {
	fprintf( stderr, "camera not known as left (%s) or right (%s)\n", LEFT_CAMERA_STRING, RIGHT_CAMERA_STRING);
	fprintf(stderr, "PRINTING CAMERA INFO!\n");
	cameras_info();
	dc1394_destroy_handle(*handle_ptr);
	return(-1);
      }
      break;
    case SHORT_RANGE:
      if(strcmp(idstr, SHORT_LEFT_CAMERA_STRING)==0) {
	node2cam[i] = LEFT_CAM;
      } else if ( strcmp( idstr, SHORT_RIGHT_CAMERA_STRING)==0) {
	node2cam[i] = RIGHT_CAM;
      } else {
	fprintf( stderr, "camera not known as short-left (%s) or short-right (%s)\n", SHORT_LEFT_CAMERA_STRING, SHORT_RIGHT_CAMERA_STRING);
	fprintf(stderr, "PRINTING CAMERA INFO!\n");
	cameras_info();
	dc1394_destroy_handle(*handle_ptr);
	return(-1);
      }
      break;
    case LONG_RANGE:
      if(strcmp(idstr, LONG_LEFT_CAMERA_STRING)==0) {
	node2cam[i] = LEFT_CAM;
      } else if ( strcmp( idstr, LONG_RIGHT_CAMERA_STRING)==0) {
	node2cam[i] = RIGHT_CAM;
      } else {
	fprintf( stderr, "camera (%s) not known as long-left (%s) or long-right (%s)\n", idstr, LONG_LEFT_CAMERA_STRING, LONG_RIGHT_CAMERA_STRING);
	fprintf(stderr, "PRINTING CAMERA INFO!\n");
	cameras_info();
	dc1394_destroy_handle(*handle_ptr);
	return(-1);
      }
      break;
    default:
      fprintf(stderr, "unknown camera pair\n");
      fprintf(stderr, "PRINTING CAMERA INFO!\n");
      cameras_info();
      dc1394_destroy_handle(*handle_ptr);
      return(-1);
      break;
    }

  }

  //  fflush(stdout);// Waydo has this - why?

  // setup capture
  for ( i = 0; i < numCameras; i++ ) {
    switch(which_pair) {
    case HIGH_RES:
      if ( dc1394_setup_capture(*handle_ptr, camera_nodes[i],
				i, // channel
				FORMAT_SVGA_NONCOMPRESSED_1,
				MODE_1024x768_MONO,
				SPEED_400,
				FRAMERATE_15,
				&cameras[i])!=DC1394_SUCCESS) {
	fprintf( stderr,"unable to setup camera-\n"
		 "check line %d of %s to make sure\n"
		 "that the video mode,framerate and format are\n"
		 "supported by your camera\n",
		 __LINE__,__FILE__);
	
	cameras_stop(*handle_ptr, cameras );
	return(-1);
      }
      break;
    case SHORT_RANGE:
      if ( dc1394_setup_capture(*handle_ptr, camera_nodes[i],
				i, // channel
				FORMAT_VGA_NONCOMPRESSED,
				MODE_640x480_MONO, 
				SPEED_400,
				FRAMERATE_30,
				&cameras[i])!=DC1394_SUCCESS) {
	fprintf( stderr,"unable to setup camera-\n"
		 "check line %d of %s to make sure\n"
		 "that the video mode,framerate and format are\n"
		 "supported by your camera\n",
		 __LINE__,__FILE__);
	
	cameras_stop(*handle_ptr, cameras );
	return(-1);
      }
      break;
    case LONG_RANGE:
      if ( dc1394_setup_capture(*handle_ptr, camera_nodes[i],
				i, // channel
				FORMAT_VGA_NONCOMPRESSED,
				MODE_640x480_MONO, 
				SPEED_400,
				FRAMERATE_30,
				&cameras[i])!=DC1394_SUCCESS) {
	fprintf( stderr,"unable to setup camera-\n"
		 "check line %d of %s to make sure\n"
		 "that the video mode,framerate and format are\n"
		 "supported by your camera\n",
		 __LINE__,__FILE__);
	
	cameras_stop(*handle_ptr, cameras );
	return(-1);
      }
      break;
    default:
      fprintf(stderr, "unknown camera pair\n");
      fprintf(stderr, "PRINTING CAMERA INFO!\n");
      cameras_info();
      cameras_stop(*handle_ptr, cameras );
      return(-1);
      break;
    }
  }

  // set trigger mode - I'm not exactly sure what this does or if it is
  // strictly necessary
  for ( i = 0; i < numCameras; i++ ) {
    if( dc1394_set_trigger_mode(*handle_ptr, cameras[i].node, TRIGGER_MODE_0)
        != DC1394_SUCCESS)
    {
      fprintf( stderr, "unable to set camera %d trigger mode\n", i);
    }
  }

  // having the cameras start sending data

  for ( i = 0; i < 2; i++ ) {
    if ( dc1394_start_iso_transmission(*handle_ptr,cameras[i].node)
	 !=DC1394_SUCCESS)
      {
	fprintf( stderr, "Unable to start camera %d iso transmission\n", i);
        cameras_stop(*handle_ptr, cameras );
	return(-1);
      }
  }

  return 0;
}


int cameras_stop( raw1394handle_t handle,
		  dc1394_cameracapture cameras[] )
{
  int i;

  // stop data transmission
  for ( i = 0; i < 2; i++ ) {
    if (dc1394_stop_iso_transmission(handle,cameras[i].node)!=DC1394_SUCCESS)
      {
	fprintf( stderr, "Couldn't stop camera%d\n", i);
      }
  }

  // close cameras
  for ( i = 0; i < 2; i++ ) dc1394_release_camera(handle,&cameras[i]);
  dc1394_destroy_handle(handle);

  return 0;
}

int cameras_snap(raw1394handle_t handle, dc1394_cameracapture cameras[])
{
  if ( dc1394_multi_capture(handle,cameras,2)!=DC1394_SUCCESS)
    {
      fprintf( stderr, "Unable to capture frames\n" );
      cameras_stop( handle, cameras );
      return(-1);
    }
  return 0;
}


int cameras_info()
{
  int numNodes;
  int i;
  char idstr[9];
  int numCameras;
  nodeid_t * camera_nodes;
  raw1394handle_t handle;

  // Open ohci and asign handle to it

  handle = dc1394_create_handle(0);
  if (handle==NULL)
  {
    fprintf( stderr, "Unable to aquire a raw1394 handle\n\n"
             "Please check \n"
	     "  - if the kernel modules `ieee1394',`raw1394' and `ohci1394' are loaded \n"
	     "  - if you have read/write access to /dev/raw1394\n\n");
    return(-1);
    }

  //  get the camera nodes and describe them as we find them
  numNodes = raw1394_get_nodecount(handle);
  camera_nodes = dc1394_get_camera_nodes(handle, &numCameras, 1);

  printf("%d cameras found\n",numCameras);
  quadlet_t cam_ids[numCameras];
  dc1394_camerainfo info[numCameras];   

  for ( i = 0; i < numCameras; i++ ) {
    dc1394_get_camera_info(handle, camera_nodes[i], &info[i]);
    cam_ids[i] = info[i].euid_64 & 0xffffffff;
    sprintf( idstr, "0x%08X",cam_ids[i] );
    if ( strcmp( idstr, LEFT_CAMERA_STRING)==0) {
      printf("%s: known as left\n",idstr);
    } else if ( strcmp( idstr, RIGHT_CAMERA_STRING)==0) {
      printf("%s: known as right\n", idstr);
    } else if ( strcmp( idstr, SHORT_LEFT_CAMERA_STRING)==0) {
      printf("%s: known as short-left\n", idstr);
    } else if ( strcmp( idstr, SHORT_RIGHT_CAMERA_STRING)==0) {
      printf("%s: known as short-right\n", idstr);
    } else if ( strcmp( idstr, LONG_LEFT_CAMERA_STRING)==0) {
      printf("%s: known as long-left\n", idstr);
    } else if ( strcmp( idstr, LONG_RIGHT_CAMERA_STRING)==0) {
      printf("%s: known as long-right\n", idstr);
    } else {
      printf("%s: not known\n", idstr);
    }
  }
  dc1394_destroy_handle(handle);
}
 
