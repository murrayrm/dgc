#ifndef StateReceiverDatum_HH
#define StateReceiverDatum_HH

#include "../../../state/state.h"

struct StateReceiverDatum {
  //State Data
  double timestamp;
  double northing;
  double easting;
  float altitude;
  float vel_n;
  float vel_e;
  float vel_u;
  float heading;
  float pitch;
  float roll;
  float speed;
  float heading_n;
  float heading_e;
  double phi;

  //Shared Memory Variables
  int shmid;
  int size;
  int key;
  int shmflg;
  char *state_string;
};

#endif
