// StateManager.cc
#include "StateManager.hh"

State st_Init;
State st_Shutdown;


StateManger::StateManager() {
  m.StateCtr = 0;
  m.CurrentState = -1;
  m.NextState = -1;
  m.ModuleName = ModuleName;


  // Set up Initialization and Shutdown States
  st_Init.Name = m.ModuleName + ":Init";
  st_Init.Description = "---------------------------";
  st_Init.f = STATE_INIT;

  st_Shutdown.Name = m.ModuleName + ":Shutdown";
  st_Shutdown.Description = "---------------------------";
  st_Shutdown.f = STATE_SHUTDOWN;

  st_Init.ID = AddState(st_Init);
  st_Shutdown.ID = AddState(st_Shutdown);

  SetNextStateInit();
}

StateManager::StateManager~() {

}

int StateManager::AddState(State& newState) {
  if( newState.f != NULL ) {
    boost::recursive_mutex::scoped_lock s(m.stateLock);
    newState.ID = m.StateCtr++;
    m.StateTable.push_back(newState);
    return newState.ID;
  } else {
    return -1;
  }
}

int StateManager::SetNextState(State& st) {
  return SetNextState(st.ID);
}

int StateManager::SetNextState(int stID) {
  // if next state is the shutdown state, do not allow 
  // this to be changed (i.e. force shutdown)

  boost::recursive_mutex::scoped_lock sl(m.stateLock);
  if( !(NextState == st_Shutdown.id) ) {
    
    // make sure the state is in our state table
    list<State>::iterator x    = m.StateTable.begin();
    list<State>::iterator stop = m.StateTable.end();
    
    while( x!= stop) {
      if( (*x).ID == stID) {
	// states match, so allow this to be next state
	m.NextState = stID;
	return st.ID;
      }
      x++;
    }
    
    // did not find this state in the table
    return -1;
  } else {
    // Cannot escape shutdown
    return -1;
  }
}


int StateManager::GetNextState() {
  return m.NextState;
}

int StateManager::GetCurrentState() {
  return m.CurrentState;
}

int StateManager::SetNextStateInit() {
  return SetNextState(st_Init);
}

int StateManager::SetNextStateShutdown() {
  return SetNextState(st_Shutdown);
}


State StateManger::StateInfo(int ID) {
  // find the state that matches ID 
  boost::recursive_mutex::scoped_lock sl(m.stateLock);
  // if ID == -1 (default), then get info for current state;
  if( ID == -1 ) {
    ID = m.CurrentState;
  }

  list<State>::iterator x    = m.StateTable.begin();
  list<State>::iterator stop = m.StateTable.end();
  while( x!= stop) {
    if( (*x).ID == m.NextState) {
      return (*x);
    }
    x++;
  }
  // did not find this state in the table
  // return a negative acknoledgement
  State ret;
  ret.ID = -1;
  return ret;
}

void StateManager::operator()() {
  while( m.NextState >= 0 ) {
    m.CurrentState = m.NextState;
    ExecuteCurrentState();
    if(m.CurrentState == st_Shutdown.ID) {
      // finished executing shutdown -> set next state to -1
      m.NextState = -1;
    } else if {
      // if there is no "next state", shutdown for 
      // lack of something better to do.
      cout << "StateManager: Shutting down for lack of Next State" << endl;
      SetNextStateShutdown();
    }
  } 
}

void StateManager::ExecuteCurrentState() {
  State cs = StateInfo(m.CurrentState);
  if(cs.ID < 0 || cs.f == NULL) {
    return;
  } else {
    cs.f();
  }
}
