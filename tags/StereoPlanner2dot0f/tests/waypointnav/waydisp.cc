#include "waypointnav.hh"
#include "sparrow/display.h"
#include "sparrow/dbglib.h"
#include "vddtable.h"

#include "waydisp.hh"

int user_quit(long arg);

void WayDisp(DATUM &d) {
	dbg_all = 0;
 
	waydisp_arbiter_phi   = 0.0;
	waydisp_arbiter_velo  = 0.0;
	waydisp_arbiter_good  = 0;
	waydisp_arbiter_yaw   = 0.0;
	waydisp_arbiter_count = 0;

	for(int i = 0; i < NUMARCS; i++) {
		waydisp_ladar_p[i] = 0.0;
		waydisp_ladar_v[i] = 0.0;
		waydisp_ladar_g[i] = 0;

		waydisp_global_p[i] = 0.0;
		waydisp_global_v[i] = 0.0;
		waydisp_global_g[i] = 0;

		waydisp_stereo_p[i] = 0.0;
		waydisp_stereo_v[i] = 0.0;
		waydisp_stereo_g[i] = 0;

		waydisp_dfe_p[i] = 0.0;
		waydisp_dfe_v[i] = 0.0;
		waydisp_dfe_g[i] = 0;

		waydisp_arbiter_p[i] = 0.0;
		waydisp_arbiter_v[i] = 0.0;
		waydisp_arbiter_g[i] = 0;
	}

	if (dd_open() < 0) exit(1);
	dd_bindkey('Q', user_quit);

	dd_usetbl(vddtable);

	sleep(4); // Wait a bit, because other threads will print some stuff out
	dd_loop();
	dd_close();
}

// TODO: Set the shutdown flag 
int user_quit(long arg)
{
	return DD_EXIT_LOOP;
}
