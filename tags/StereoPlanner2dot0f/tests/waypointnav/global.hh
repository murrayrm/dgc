#ifndef GLOBAL_HH
#define GLOBAL_HH

//#include "../../state/state.h"
#include "../../Global/Vector.hh"
#include "../../latlong/LatLong.h"
#include "../../Msg/dgc_time.h"
#include <fstream>
#include <unistd.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

using namespace std;

//const double waypointRadius = 10.0;      // meters around waypoint
const double waypointRadius = 3.0;      // meters around waypoint

//const double MAX_VELO = 5.0;    // 12/6/03 max velocity in m/s
//const double MIN_VELO = 0.0;    // 12/6/03 not using brakes, so idle

// Variables considered in f_Steering and f_Velo
//const double MAX_STEER = .5;          // currently defined in waypointnav.hh
//const double DESIRED_VELOCITY = 5 * 1600 / 3600;   // desired velocity in m/s
//const double VELO_RANGE = 5 * 1600 / 3600;
//const double ACCEL_TOOMUCH = .1;
//const double TOO_FAST = 2 * 1600 / 3600;
//const double MIN_ACCEL_GAS   =   0.00;
//const double MAX_ACCEL_GAS   =   0.10;
//const double MIN_ACCEL_BRAKE =  -0.40;
//const double MAX_ACCEL_BRAKE =  -0.90;
//const double SCALE_ACCEL_GAS   =  1.0; // scales m/s to accel
//const double SCALE_ACCEL_BRAKE =  1.0;

typedef struct WaypointFollowing {
 public:
  int    thrdCounter;
  ofstream stateFile;
  ofstream driveFile;
  timeval timeZero;
  list<Vector> waypoints;

  WaypointFollowing() {
    thrdCounter = 0;
    gettimeofday(&timeZero, NULL);
    //stateFile.open("state.dat");
    driveFile.open("drive.dat");
    //  stateFile << setiosflags(ios::fixed) << setprecision(5);
    driveFile << setiosflags(ios::fixed) << setprecision(5);
  }
};

double f_Steering(Vector curPos, Vector curWay, double curHeadDeg);
double f_Velo(double curVelo, Vector curPos, Vector curWay);

#endif
