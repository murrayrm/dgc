#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"


class GPSLog : public DGC_MODULE {
public:
  GPSLog();

  void Active();

};

GPSLog::GPSLog(): DGC_MODULE( MODULES::Passive, "Passive GPS Capture Program", 0){}

void GPSLog::Active() {

  //  string TestNumber;
  //  cout << "What test number? " << endl;
  //  cin >> TestNumber;
  //  // echo the test number entered
  //  cout << endl << TestNumber << endl;  


  VState_GetStateMsg mySS;

  int counter = 0;  
  int shutdown_flag = false;
  char myOption;
  ofstream outfile ( "gps.dat", ios::app);

  outfile << setiosflags(ios::fixed);

  outfile << endl << endl << endl;
  outfile << "# File Format -- " << endl;
  outfile << "# Number"  << "\t"
	  << "Time"      << "\t"
	  << "Easting"   << "\t"
	  << "Northing"  << "\t"
	  << "East Vel"  << "\t"
	  << "North Vel" << "\t"
	  << "Up Vel"    << "\t"
	  << "Speed"     << "\t"
	  << "Yaw"       << "\t"
	  << endl;

  time_t st = time(NULL);
  outfile << "Start Time: " << ctime(&st) << endl;

  Timeval startTime = TVNow();

  while ( true ) { 
    
    // Request a message from State
    Mail msg = NewQueryMessage(MyAddress(), MODULES::VState, VStateMessages::GetState);
    Mail reply = SendQuery(msg);
    reply >> mySS;
    
    outfile << counter << "\t"
	    << setprecision(2) << (TVNow() - startTime)
            << "\t"  << mySS.Easting << "\t" << mySS.Northing 
	    << setprecision(2) 
	    << "\t"  << mySS.Vel_E << "\t" << mySS.Vel_N << "\t" << mySS.Vel_U
	    << "\t"  << mySS.Speed << "\t" 
	    << setprecision (3) << mySS.Yaw 
	    << endl;
    cout << setprecision(8);
    cout << "Press Ctrl-C to Quit: " << counter << ", Grabbed E = " << mySS.Easting << ", N = " << mySS.Northing << endl;
    counter ++; 
    sleep(1);
  }

  ForceKernelShutdown();
}

int main(int argc, char* argv[])
{
  
  Register( shared_ptr<DGC_MODULE> ( new GPSLog));
  StartKernel();

  return 0;
}
