#include "Sim.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

// Added for display 2/24/04 
VState_GetStateMsg dispSS;
double dispSS_decimaltime;

double steerCommand;
double speedCommand;

int UpdateCount   = 0;
int ServedCount   = 0;
int ReceivedCount = 0;

extern int QUIT_PRESSED;
extern int PAUSED;

extern SIM_DATUM d;

#include "vddtable.h"
int user_quit(long arg);
int pause_simulation(long arg);
int unpause_simulation(long arg);

void SimVState::UpdateSparrowVariablesLoop() 
{
  while( !ShuttingDown() ) {

    // this is the same as in the arbiter display 
    dispSS = d.SS;
    dispSS_decimaltime = (d.SS.Timestamp.sec()+d.SS.Timestamp.usec()/1000000.0);

    steerCommand = d.cmd.steer_cmd; 
    speedCommand = d.cmd.velocity_cmd;

    UpdateCount ++;
    ServedCount   = d.ServedState;
    ReceivedCount = d.CmdMotionCount;
    
    usleep(500000); // Wait a bit, because other threads will print some stuff out
  }

}

void SimVState::SparrowDisplayLoop() 
{
  dbg_all = 0;

  if (dd_open() < 0) exit(1);

  dd_bindkey('Q', user_quit);
  dd_bindkey('P', pause_simulation);
  dd_bindkey('U', unpause_simulation);

  dd_usetbl(vddtable);

  usleep(500000); // Wait a bit, because other threads will print some stuff out
  dd_loop();
  dd_close();
  QUIT_PRESSED = 1; // Following LADARMapper's lead
}

// TODO: Set the shutdown flag in the quit function
int user_quit(long arg)
{
  return DD_EXIT_LOOP;
}

// Pause the simulation
int pause_simulation(long arg)
{
  PAUSED = 1;
}

// Unpause the simulation
int unpause_simulation(long arg)
{
  PAUSED = 0;
}
