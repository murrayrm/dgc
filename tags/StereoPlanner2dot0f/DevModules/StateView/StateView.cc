#include "StateView.hh"
#include <boost/shared_ptr.hpp>

void StateView::Active() {

  cout << "Starting Active Funcion" << endl;
  while( ContinueInState() ) {

    // update the state so we know where the vehicle is at
    UpdateState();

    cout << setprecision(10);
    cout << "N " << SS.Northing 
	 << ", E " << SS.Easting 
	 << ", N VEL " << SS.Vel_N
	 << ", E VEL " << SS.Vel_E
	 << endl;
    

    // and sleep for a bit
    usleep(2000000);
  }

  cout << "Finished Active State" << endl;

}


int main(int argc, char **argv) {
   
  Register(shared_ptr<DGC_MODULE>(new StateView));
  StartKernel();

  return 0;
}
