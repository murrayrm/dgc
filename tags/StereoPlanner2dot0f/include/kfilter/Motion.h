/*------------------------------------------------------------------------
--                                                                      --
--                        U N C L A S S I F I E D                       --
--                                                                      --
--                  L I T T O N   P R O P R I E T A R Y                 --
--                                                                      --
-- Copyright (c) 1999, Guidance & Control Systems Division,             --
--                     Litton Systems, Inc.                             --
--                                                                      --
--------------------------------------------------------------------------
--                                                                      --
--                       RESTRICTED RIGHTS LEGEND                       --
--                                                                      --
-- Use, duplication or disclosure is subject to restrictions stated in  --
-- Contract No. (see Software Version Description Document)             --
-- with Litton Systems, Inc., Guidance and Control Systems Division.    --
--                                                                      --
-----------------------------------------------------------------*/
/****************************************************************************
 *
 * INS/GPS, Motion Detect
 *
 * File: motion.h
 * Date: 10/01/99
 *
 ****************************************************************************/


/****************************************************************************
 * Motion Detect Status
 ****************************************************************************/

typedef enum {
  STATIONARY, TAXIING, FLYING
} MDStatus;

/****************************************************************************
 * Function Definitions
 ****************************************************************************/

void motionDetectInit(void);
void mdAcceptErrorEst(double x_vel_corr, double y_vel_corr, double z_vel_corr,
                                 double x_tilt, double y_tilt, double z_tilt);
void mdAcceptVel(double vx, double vy, double vz);
void mdAcceptAngRates(double pitch_rate, double roll_rate, double yaw_rate);
void mdAcceptAttitude(double pitch, double roll, double yaw);
void motionDetect(void);

/****************************************************************************
 * End
 ****************************************************************************/

