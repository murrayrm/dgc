#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include "laserdevice.h"

int main(int argc, char *argv[]) {
  unsigned char data[1024];
  int result;
  int status;

  if(argc < 2) {
    printf("Serial device must be specified on cmd line (ex /dev/ttyS9)\n");
    exit(-1);
  }

  printf("Attempting to communicated with LADAR on %s\n",argv[1]);

  CLaserDevice laserDevice; /* change this */

  result = laserDevice.Setup(argv[1], 180, 0.50);

  if (result==0) { // success
    printf("Laser initialized successfully - no need to reset\n");
//    laserDevice.Reset(true);
    laserDevice.Reset(false);
    laserDevice.Reset(true);
  } else {
    printf("Attempting to reset laser...\n");
    laserDevice.Reset(true);
  }
  laserDevice.Shutdown();

  return 0;
}
