/*****************************************************************************/
/* SICK LADAR Calibration routines                                           */
/* Copyright (C) 2004 - Mike Thielman                                        */
/*****************************************************************************/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <pthread.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

#define START_SCAN 60

// To calibrate, vehicle (LADAR) must be on a level plane. 
// The calibration routines need to know the height of the ladar scanner.
// The pitch is calibrated by cos(theta) = adjacent / hypotenuse
//   where adjacent = height of ladar, hypotenuse = distance of center scan

// The roll is calibrated by ...
// In an attempt to make it more accurate, every pair of angles will be
// measured and the average taken.

#define ANGLE 100
#define RES 0.50

#include "laserdevice.h"

#include "vddtable.h"
int user_quit(long arg);
pthread_t dd_thread;
void SparrowDisplayLoop();
void UpdateSparrowVariablesLoop();
int npitch = 0,nroll = 0;
double pitch,pitchtotal,roll,rolltotal,pitchmax,pitchmin,rollmax,rollmin;
double pitchavg,rollavg;

int npitchall = 0,nrollall = 0;
double pitchall,pitchalltotal,rollall,rollalltotal,pitchallmax,pitchallmin,rollallmax,rollallmin;
double pitchavgall,rollavgall;

int main(int argc, char *argv[]) {
  double x,x1,x2;
  int result;
  int status;
  int height;
  double phi,offset;
  int n;

  pitchtotal = 0.0;
  rolltotal = 0.0;  
  pitchmax = DBL_MIN;
  pitchmin = DBL_MAX;
  rollmax = DBL_MIN;
  rollmin = DBL_MAX;

  pitchalltotal = 0.0;
  rollalltotal = 0.0;  
  pitchallmax = DBL_MIN;
  pitchallmin = DBL_MAX;
  rollallmax = DBL_MIN;
  rollallmin = DBL_MAX;

  if(argc < 3) {
    printf("Usage: calibrate <tty> <height>\n\n");
    printf("                  tty - ex /dev/ttyS8\n");
    printf("                  height - height of ladar in centimeters\n");
    exit(-1);
  }

  height = atoi(argv[2]);
  printf("Attempting to communicate with LADAR device %s\n",argv[1]);
  printf("Mounted at %d cm\n", height);

  offset = ((180.0 - ANGLE) / 2.0) * M_PI / 180.0;

  CLaserDevice laserDevice; /* change this */

  result = laserDevice.Setup(argv[1], ANGLE, RES);

  dbg_all = 0;
  if (dd_open() < 0) exit(1);
  dd_usetbl(vddtable);
  pthread_create(&dd_thread, NULL, dd_loop_thread, (void *) NULL);

  n = laserDevice.getNumDataPoints();
  printf("numDataPoints(): %d\n", n);

  for( ; ; uc++) {
  status = laserDevice.UpdateScan();

    // Determine PITCH
    for(int i = 0; i < n; i++) {
      x = laserDevice.Scan[i];
      if(!laserDevice.isDataError(status,laserDevice.Scan[i])) {
        if(height > x) {
          printf("Height is incorrect or LADAR is not on level surface\n");
          printf("Or there is an obstacle in the way\n");
          exit(-1);
        }
          pitch = (M_PI / 2.0) - acos((double) height / x);
          pitchalltotal += pitch;
          if(pitch > pitchallmax) pitchallmax = pitch;
          if(pitch < pitchallmin) pitchallmin = pitch;
          npitchall++;
        if((i >= START_SCAN) && (i < (n-START_SCAN))) {
          pitchtotal += pitch;
          if(pitch > pitchmax) pitchmax = pitch;
          if(pitch < pitchmin) pitchmin = pitch;
          npitch++;
        }
      }
    }

    // Determine ROLL
    for(int i = START_SCAN; i < (n-1)/2; i++) {
      x1 = laserDevice.Scan[i];
      x2 = laserDevice.Scan[n-i];

      if(laserDevice.isDataError(status,laserDevice.Scan[i]) || laserDevice.isDataError(status,laserDevice.Scan[n-i])) continue;

      x = (x1+x2) / 2;
      phi = offset + (i * RES * M_PI) / 180.0;

      // NOT SURE THIS IS RIGHT
      roll = atan((x1-x) / (x * sin(phi)));
        rollalltotal += roll;
        if(roll > rollallmax) rollallmax = roll;
        if(roll < rollallmin) rollallmin = roll;
        nrollall++;
      if(i >= START_SCAN) {
        rolltotal += roll;
        if(roll > rollmax) rollmax = roll;
        if(roll < rollmin) rollmin = roll;
        nroll++;
      }
    }
    UpdateSparrowVariablesLoop();
    usleep(100000);
  }

  laserDevice.LockNShutdown();
  return(0);
}
void UpdateSparrowVariablesLoop() {
  minpitchrad = pitchmin;
  minpitchdeg = pitchmin * 360.0 / (2.0 * M_PI);
  maxpitchrad = pitchmax;
  maxpitchdeg = pitchmax * 360.0 / (2.0 * M_PI);

  avgpitchrad = pitchtotal / (double) npitch;
  avgpitchdeg = avgpitchrad * 360.0 / (2.0 * M_PI);

  minrollrad = rollmin;
  minrolldeg = rollmin * 360.0 / (2.0 * M_PI);
  maxrollrad = rollmax;
  maxrolldeg = rollmax * 360.0 / (2.0 * M_PI);

  avgrollrad = rolltotal / (double) nroll;
  avgrolldeg = avgrollrad * 360.0 / (2.0 * M_PI);

  minpitchallrad = pitchallmin;
  minpitchalldeg = pitchallmin * 360.0 / (2.0 * M_PI);
  maxpitchallrad = pitchallmax;
  maxpitchalldeg = pitchallmax * 360.0 / (2.0 * M_PI);

  avgpitchallrad = pitchalltotal / (double) npitchall;
  avgpitchalldeg = avgpitchallrad * 360.0 / (2.0 * M_PI);

  minrollallrad = rollallmin;
  minrollalldeg = rollallmin * 360.0 / (2.0 * M_PI);
  maxrollallrad = rollallmax;
  maxrollalldeg = rollallmax * 360.0 / (2.0 * M_PI);

  avgrollallrad = rollalltotal / (double) nrollall;
  avgrollalldeg = avgrollallrad * 360.0 / (2.0 * M_PI);
}
int user_quit(long arg)
{
  return DD_EXIT_LOOP;
}
