# Makefile for sparrow library
# RMM, 30 Dec 03

CC = g++
CFLAGS = -g -I. -DCOLOR
DGC = ..

PGMS = cdd sparrow.a dispexmp sparrow.html
INCS = display.h debug.h dbglib.h channel.h flag.h keymap.h

all: $(PGMS) TAGS
install: $(PGMS) $(DGC)/bin $(DGC)/lib $(DGC)/include/sparrow
	install -s cdd $(DGC)/bin
	install -m 644 sparrow.a $(DGC)/lib/sparrow.a
	install -m 644 $(INCS) $(DGC)/include/sparrow

$(DGC)/bin:; mkdir $@
$(DGC)/lib:; mkdir $@
$(DGC)/include/sparrow:; mkdir -p $@

#
# Display compiler
#
CDD_SRC = cdd.c parse.c
CDD_OBJS = $(CDD_SRC:.c=.o)
cdd: $(CDD_OBJS)
	$(CC) $(CFLAGS) -o $@ $(CDD_OBJS)

parse.c: parse.y
	yacc parse.y
	@cp -p y.tab.c parse.c

#
# Display library
#
LIB_SRC = display.c keymap.c tclib.c cgets.c conio.c termio.c flag.c \
	ddtypes.c hook.c debug.c ddthread.c ddsave.c capture.c channel.c \
	chnconf.c virtual.c fcn_gen.c matlab.c matrix.c loadmat.c \
        chngettok.c devlut.c
LIB_OBJS = $(LIB_SRC:.c=.o)

sparrow.a: $(LIB_OBJS)
	ar r $@ $?

fcn_gen.o: fcn_tbl.h
fcn_tbl.h: fcn_tbl.dd cdd;	cdd -o $@ fcn_tbl.dd

#
# Example program (under unix)
#
dispexmp: dispexmp.o sparrow.a
	$(CC) $(CFLAGS) -o $@ dispexmp.o sparrow.a -ltermcap -lcurses

dispexmp.o: dispexmp.h
dispexmp.h: dispexmp.dd cdd;	cdd -o $@ dispexmp.dd

#
# schntest program - simple version of chntest
#
schntest: schntest.o sparrow.a
	$(CC) $(CFLAGS) -o $@ schntest.o sparrow.a -ltermcap -lcurses

schntest.o: schntest.h
schntest.h: schntest.dd cdd;	cdd -o $@ schntest.dd

#
# Documentation
#
sparrow.html: sparrow.txi # dispexmp.cxi dispexmp.ddi
	makeinfo --html --no-split -o $@ sparrow.txi

# html version does not include code
#
# dispexmp.cxi: dispexmp.c
#	sed -e "s/{/@{/" -e "s/}/@}/" $? | expand > $@
#
# dispexmp.ddi: dispexmp.dd
#	expand dispexmp.dd > dispexmp.ddi


# Generate a TAGS file
TAGS: *.c
	etags *.[ch]

tidy:;	/bin/rm -f *.o *~ parse.tab.* y.tab.c
clean: tidy;	
	/bin/rm -f parse.c $(PGMS) $(TBLS) *.html *.cxi *.ddi

depend:
	makedepend -Y -- $(CFLAGS) -- *.c

# DO NOT DELETE THIS LINE - makedep depends on it

capture.o: channel.h flag.h hook.h
cdd.o: cdd.h
cgets.o: conio.h
channel.o: channel.h hook.h
chnconf.o: channel.h display.h matlab.h matrix.h
chngettok.o: display.h
conio.o: conio.h tclib.h termio.h
ddsave.o: display.h
ddthread.o: display.h
ddtypes.o: display.h
debug.o: dbglib.h conio.h tclib.h
devlut.o: channel.h
dispexmp.o: display.h dispexmp.h
display.o: display.h keymap.h ddkeymap.h hook.h flag.h
fcn_gen.o: channel.h servo.h display.h fcn_gen.h fcn_tbl.h
flag.o: flag.h
hook.o: hook.h
keymap.o: keymap.h
matlab.o: matlab.h matrix.h
matrix.o: matrix.h
parse.o: cdd.h
schntest.o: display.h keymap.h channel.h servo.h
tclib.o: tclib.h keymap.h termio.h
virtual.o: channel.h virtual.h
y.tab.o: cdd.h
