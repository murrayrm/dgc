#ifndef LOG_HH
#define LOG_HH

#define Log LOG(__FILE__, __LINE__)

#include <string>
#include <iomanip>
#include <strstream>
#include <iostream>

namespace MSG {

  // constants
  enum LOG_LEVEL {
    LOG_DEBUG,
    LOG_NORMAL,
    LOG_ERROR,
    LOG_CRITICAL_ERROR
  };



  class LOG {
  public:
    LOG(char *, int line );
    ~LOG();

    LOG & operator()(LOG_LEVEL loglevel, int logoutput=-1);

    LOG & operator << (const std::ostrstream & rhs);
    LOG & operator << (const int & rhs);
    LOG & operator << (const long & rhs);
    LOG & operator << (const float & rhs);
    LOG & operator << (const double & rhs);
    LOG & operator << (const std::string & rhs);
    LOG & operator << (const char * & rhs);
    

  private:
    std::ostrstream x;
    LOG_LEVEL myLogLevel;
    int logoutput;
    int fd;

  };

}


#endif
