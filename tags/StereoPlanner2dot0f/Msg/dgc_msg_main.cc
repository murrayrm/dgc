// dgc_msg_main.cc 

#include "dgc_mta.hh"
#include "dgc_msg.h"
#include "dgc_socket.h"
#include "dgc_messages.h"
#include "dgc_const.h"
#include "dgc_mta_main.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
 

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <iostream>
#include <list>
#include <time.h>


int shutdown_flag;
using namespace std;

extern int dgc_mta_MyID;

const int myType=15;

int main(int argc, char* argv[])
{

  shutdown_flag = FALSE;
  cout << endl << endl << "Registering" << endl;
  dgc_msg_Register(myType);

  int ret;
  int myMsg = 454234;

  //  sleep(1);
  cout << "Checking for messages" << endl;
  cout << "Any Messages = " << dgc_AnyMessages() << endl;

  dgc_MsgHeader myMH;

  cout << endl << "Checking for messages" << endl;
  cout << "Poll Message = " << dgc_PollMessage(myMH) << endl;

  cout << endl << "Getting the message" << endl;
  cout << "Get Message = " <<  dgc_GetMessage(myMH, &ret) << endl;
  cout << "MyMsg = " << myMsg << ", ret = " << ret << endl;

  myMH.Destination = 0;
  myMH.DestinationType = myType;
  myMH.MsgType = 25;
  myMH.MsgSize = sizeof(int);


  cout << endl << "Sending a msg." << endl;
  dgc_SendMessage(myMH, &myMsg);
  


  cout << endl << "Checking for messages" << endl;
  cout << "Any Messages = " << dgc_AnyMessages() << endl;

  cout << endl << "Checking for messages" << endl;
  cout << "Poll Message = " << dgc_PollMessage(myMH) << endl;

  cout << "myMH.Source = " << myMH.Source << ", myMH.SourceID = " << myMH.SourceID
       << ", myMH.MsgSize = " << myMH.MsgSize << "." << endl;

  cout << endl << "Getting the message" << endl;
  cout << "Get Message = " <<  dgc_GetMessage(myMH, &ret) << endl;
  cout << "MyMsg = " << myMsg << ", ret = " << ret << endl;




  cout << endl << endl << "Checking for messages" << endl;
  cout << "Any Messages = " << dgc_AnyMessages() << endl;

  cout << endl << endl << "Checking for messages" << endl;
  cout << "Poll Message = " << dgc_PollMessage(myMH) << endl;

  cout << "myMH.Source = " << myMH.Source << ", myMH.SourceID = " << myMH.SourceID
       << ", myMH.MsgSize = " << myMH.MsgSize << "." << endl;

  cout << endl << endl << "Getting the message" << endl;
  cout << "Get Message = " <<  dgc_GetMessage(myMH, &ret) << endl;
  cout << "MyMsg = " << myMsg << ", ret = " << ret << endl;

  //sleep(1);
  cout << endl << endl << "Unregistering" << endl;
  dgc_msg_Unregister();


  return 0;
}














