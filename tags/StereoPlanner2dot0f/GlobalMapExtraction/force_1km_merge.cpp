/* This code makes the assumption that present searches that have found that
 * a picture's origin will always be at a unit UTM value (rather than some
 * fraction or decimal) will continue to hold.  This code will not work if that
 * should break, because it assumes that it is merging various bitmaps into a
 * bitmap that is known to be 1000x1000.
 */

#include <list>
#include <string>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <fcntl.h>

using namespace std;

class bot_left_pix {
public:
  int x;
  int y;
};

union buf_four {
  // Named in_long anachronistically to copy-pasted code, this assumption
  // isn't necessary.
  unsigned long in_long;
  char buf[4];
};

union buf_two {
  // Named in_long anachronistically to copy-pasted code, this assumption
  // isn't necessary.
  unsigned int in_short;
  char buf[2];
};

void print_as_bin(const unsigned long input) {
  unsigned long n = input;
  string byte_string("");
  string byte;
  for (int i = 0; i < 4; i++) {
    byte = "";
    for (int j = 0; j < 8; j++) {
      if (n % 2 == 0) {
        byte = string("0") + byte;
      }
      else {
        byte = string("1") + byte;
      }
      n /= 2;
    }
    byte += " ";
    byte_string = byte + byte_string;
  }

  cout << byte_string << "\n";
  return;
}



bool add_bmp(string in_name, bot_left_pix pixel, ofstream& bmp) {
  ifstream in;
  buf_four in_four;
  int fnum;
  if ((fnum = open(in_name.c_str(), O_RDONLY)) < 0) {
    cout << "CRITICAL: Error opening file " << in_name << "to read image "
	 << "data\n    Very unexpected failure (this opened before).\n";
    return false;
  }

  if (!bmp.is_open()) {
    cout << "CRITICAL: Output file closed entering subfunction\n";
    in.close();
    return false;
  }
  
  //in.seekg(18, ios::beg);
  //in.read(in_four.buf, 4);
  lseek(fnum, 18, SEEK_SET);
  read(fnum, in_four.buf, 4);
  int frag_width = in_four.in_long;
  //in.read(in_four.buf, 4);
  read(fnum, in_four.buf, 4);
  int frag_height = in_four.in_long;
  int padding = 0;
  while ((frag_width + padding) % 4 != 0) {
    padding++;
  }
  char* buffer = (char*) malloc(frag_width);
  if (buffer == NULL) {
    cout << "Error allocating memory for buffer to transfer image data\n";
    close(fnum);
    return false;
  }
  //in.seekg(1078, ios::beg);
  lseek(fnum, 1078, SEEK_SET);
  for (int r = 0; r < frag_height; r++) {
    bmp.seekp(1078 + ((999 - pixel.y) + r)*1000 + pixel.x, ios::beg);
    read(fnum, buffer, frag_width);
    for (int i = 0; i < frag_width; i++) {
      bmp.put(buffer[i]);
    }
    lseek(fnum, padding, SEEK_CUR);
  }

  free(buffer);
  close(fnum);
  return true;
}


// Takes inputs ordered by the size of the image, from smallest to largest.
// Writes all 0s to the image data first, then overwrites each picture in turn.
// Larger consecutive picture segments will overwrite smaller ones in an effort
// to reduce error.  Calling this function will erase any file with the given
// output name, so this should only be called after a check has been made that
// no such file already exists (or that it should be erased if so).  This file
// expects that the input names have already been tested to open safely.  This
// will repeat the test, but will immediately stop and return an error if the
// test fails, since it would have expected to receive valid names.
bool merge_bitmaps(list<string> in_names,
		   list<bot_left_pix> in_starts, string out_name) {
  assert(in_names.size() == in_starts.size());

  //if (const int bmp_num = open(out_name.c_str(), O_WRONLY | O_CREAT) < 0) {
  //  cout << "Error opening bitmap file \"" << out_name << "\"\n";
  //  return false;
  //} 

  ofstream bmp;
  bmp.open(out_name.c_str(), ios::out | ios::trunc | ios::binary);
  if (!bmp.is_open()) {
    cout << "Error opening bitmap file \"" << out_name << "\"\n";
    return false;
  }

  buf_four sbuf_four;
  buf_two sbuf_two;

  /// Write the header for the file.
  //Indicate that this is a bitmap
  bmp.put('B');
  bmp.put('M');
  //Write the bitmap size
  sbuf_four.in_long = (unsigned long)(1078 + 1000*1000);
  bmp.write(sbuf_four.buf, 4);
  //Write the reserved bits
  sbuf_four.in_long = 0;
  bmp.write(sbuf_four.buf, 4);
  //Write the offset from the beginning of the file to the data
  sbuf_four.in_long = 1078;
  bmp.write(sbuf_four.buf, 4);
  //Write the size of the bitmap header
  sbuf_four.in_long = 40;
  bmp.write(sbuf_four.buf, 4);
  //Write the width of the image in pixels
  // HARD CODED TO 1000 FOR THIS APPLICATION
  sbuf_four.in_long = 1000;
  bmp.write(sbuf_four.buf, 4);
  //Write the height of the image in pixels
  // HARD CODED TO 1000 FOR THIS APPLICATION
  sbuf_four.in_long = 1000;
  bmp.write(sbuf_four.buf, 4);
  //Write the number of target planes
  sbuf_two.in_short = 1;
  bmp.write(sbuf_two.buf, 2);
  //Write the number of bits per pixel
  sbuf_two.in_short = 8;
  bmp.write(sbuf_two.buf, 2);
  //Write the type of compression
  sbuf_four.in_long = 0;
  bmp.write(sbuf_four.buf, 4);
  //Write the size of the image data in bytes
  sbuf_four.in_long = 1000 * 1000;
  bmp.write(sbuf_four.buf, 4);
  //Write the horizontal conversion from pixels to meters
  sbuf_four.in_long = 0;
  bmp.write(sbuf_four.buf, 4);
  //Write the vertical conversion from pixels to meters
  sbuf_four.in_long = 0;
  bmp.write(sbuf_four.buf, 4);
  //Write the number of colors used
  sbuf_four.in_long = 256;
  bmp.write(sbuf_four.buf, 4);
  //Write the number of important colors used
  sbuf_four.in_long = 0;
  bmp.write(sbuf_four.buf, 4);

  //Write the color table.
  unsigned char ch = 0;
  for (int index = 0; index < 256; ch++, index++) {
    bmp.put(ch);
    bmp.put(ch);
    bmp.put(ch);
    bmp.put(0);
  }

  // Input default 0 value for all pixels.  Row byte sizes must be padded to
  // be divisible by 4, but with a 1000 by 1000 image, this is already taken
  // into account.  Just write 1,000,000 0 value pixels.
  for (long int i = 0; i < 1000000; i++) {
    bmp.put(0);
  }

  // Now, overwrite with any and all appropriate pixel values.
  bool success = true;
  list<string>::const_iterator n;
  list<bot_left_pix>::const_iterator p;
  for (n = in_names.begin(), p = in_starts.begin(); n != in_names.end() &&
	 success; n++, p++) {
    success = add_bmp(*n, *p, bmp);
    if (!success) {
      cout << "Error adding in the bitmap \"" << *n << "\" to the output"
	   << "   bitmap \"" << out_name << "\"\n";
    }
  }
  
  // Return whether or not all the bitmaps were returned successfully.
  bmp.close();
  return success;
}



int main(int argc, char *argv[]) {
  if (argc != 2) {
    printf("Usage: %s [filename] - convert tiff [filename] to a text file\n",
           argv[0]);
    return 0;
  }

  string in_name(argv[1]);
  if (in_name.substr(in_name.size() - 4, 4) != ".bmp") {
    cout << "Input \"" << in_name << "\" not labeled a bitmap.  Giving up.\n";
    return 0;
  }

  // Identifying marks are added onto fragment names right before the
  // extension.  _L indicates that the fragment is incomplete to its left, _T
  // to its top, _R to its right, _B beneath it.  In corner cases, two of these
  // will be stacked one after the other.  Find out which tag the input file
  // has or quit if there is none.  (These tags were added on creation by the
  // code I wrote to extract the bitmaps to begin with).
  string frag_id = in_name.substr(in_name.size() - 8, 4);
  cout << "frag_id = " << frag_id << "\n";

  if (frag_id.substr(0, 2) != "_T" && frag_id.substr(0, 2) != "_B" &&
      frag_id.substr(0, 2) != "_L" && frag_id.substr(0, 2) != "_R") {
    // There are not two tags appended one after the other.  Remove the junk.
    frag_id = frag_id.substr(2, 2);
  }

  
  if (frag_id.substr(0, 2) != "_T" && frag_id.substr(0, 2) != "_B" &&
      frag_id.substr(0, 2) != "_L" && frag_id.substr(0, 2) != "_R") {
    // There is no tag whatsoever.  Report and quit.
    cout << "Input \"" << in_name << "\" not labeled as a fragment to merge.\n"
	 << "  Giving up.\n";
    return 0;
  }

  string base_name(in_name.substr(0, in_name.size() - (4 + frag_id.size()))); 

  // Check and see if a file can be opened as input that would have the name
  // of a completed, merged map having these coordinates.  If such a map
  // already exists, for now, give up.

  string test_name = base_name + ".bmp";
  ifstream test;
  /*
  test.open(test_name.c_str(), ios::in | ios::binary);
  cout << "DEBUG: test_name is \"" << test_name << "\"\n";
  if (test.is_open()) {
    cout << "Bitmap fragment \"" << in_name << "\" appears to already have\n"
	 << "   an existing merged map \"" << test_name << "\".  Giving up\n";
    test.close();
    return 0;
  }
  */

  // Check that input name is a valid file that can be opened.
  test.open(in_name.c_str(), ios::in | ios::binary);
  if (!test.is_open()) {
    cout << "Unable to open input file \"" << in_name << "\".  Giving up\n";
    return 0;
  }
  test.close();

  list<string> tests(0);
  if (frag_id.size() == 4) {
    tests.push_back(base_name + "_L_T.bmp");
    tests.push_back(base_name + "_R_T.bmp");
    tests.push_back(base_name + "_L_B.bmp");
    tests.push_back(base_name + "_R_B.bmp");
    tests.push_back(base_name + ".bmp");
  }
  else if (frag_id == "_L" || frag_id == "_R") {
    tests.push_back(base_name + "_L.bmp");
    tests.push_back(base_name + "_R.bmp");
    tests.push_back(base_name + ".bmp");
  }
  else if (frag_id == "_T" || frag_id == "_B") {
    tests.push_back(base_name + "_B.bmp");
    tests.push_back(base_name + "_T.bmp");
    tests.push_back(base_name + ".bmp");
  }

  // Find which of the possible contributing bitmaps can be opened, and of
  // those, which ones cover a larger area.  The ones that conver a larger area
  // will be written to the map last so that the largest continuous area from
  // an original file possible will be input unoverwritten.  This is an effort
  // to make the merged maps as reliable as possible.
  list<string> file_names(0);
  list<bot_left_pix> pixels(0);
  list<int> areas;
  
  list<string>::iterator n;
  list<bot_left_pix>::iterator p;
  list<int>::iterator a;

  buf_four in_four;
  
  unsigned long x;
  int y;
  bot_left_pix pixel;
  int area;
  string this_frag;
  for (list<string>::const_iterator i = tests.begin(); i != tests.end(); i++) {
    cout << "Opening file " << *i << "\n";
    test.open(i->c_str(), ios::binary);
    if (test.is_open()) {
      // This file exists and can be opened safely.  Determine where it will
      // fit into the merged picture and record the pertinent info into the
      // lists.
      test.close();
      int fnum = open(i->c_str(), O_RDONLY);
      //test.seekg(18, ios::beg);
      //cout << "before the read x is " << x << " and y is " << y << "\n";
      //cout << "\nx is \n";
      //print_as_bin(x);
      //cout << "\ny is \n";
      //print_as_bin(y);
      //cout << "\n1000 is \n";
      //print_as_bin(1000);
      //test.read(in_four.buf, 4);
      //x = in_four.in_long;

      lseek(fnum, 18, SEEK_SET);
      read(fnum, in_four.buf, 4);
      x = in_four.in_long;
      read(fnum, in_four.buf, 4);
      y = in_four.in_long;
      //cout << "\n";
      //print_as_bin(in_four.in_long);
      //cout << "\n";
      //print_as_bin(x);
      //test.read(in_four.buf, 4);
      //y = in_four.in_long;
      //cout << "\nx is \n";
      //print_as_bin(x);
      //cout << "\ny is \n";
      //print_as_bin(y);

      cout << "DEBUG: width is " << x << " and height is " << y << "\n";
      this_frag = i->substr(in_name.size() -
			    (4 + frag_id.size()), frag_id.size());

      // Error check the input size.
      if ((this_frag == "_L" || this_frag == "_R") && y != 1000) {
	cout << "ERROR: file \"" << *i << "\" has an image height " << y
	     << "\n   that should instead be 1000.  Give up\n";
	close(fnum);
	return 0;
      }
      if ((this_frag == "_T" || this_frag == "_B") && x != 1000) {
	cout << "ERROR: file \"" << *i << "\" has an image width " << x
	     << "\n   that should instead be 1000.  Give up\n";
	close(fnum);
	return 0;
      }

      if (x > 1000 || y > 1000) {
	cout << "ERROR: improper dimensions larger than image to merge into.\n"
	     << "  width is " << x << " and height is " << y << "\n";
	close(fnum);
	return 0;
      }

      // Note the area and the pixel.  Also not the location where the fragment
      // will appear in the merged image.  Because the dimensions can be
      // rechecked when the file is opened to extract the image data, it is
      // sufficient to identify the location to simply cite the bottom left
      // pixel in the 1000x1000 bitmap that will contain info from this one.
      area = x * y;
      

      if (this_frag != "_L_B" && this_frag != "_L_T" && this_frag != "_R_B" &&
	  this_frag != "_R_T" && this_frag != "_L" && this_frag != "_R" &&
	  this_frag != "_B" && this_frag != "_T") {
	pixel.x = 0;
	pixel.y = 999;
      }

      if (this_frag == "_T" || this_frag == "_R" || this_frag == "_R_T") {
	pixel.x = 0;
	pixel.y = 999;
      }
      
      if (this_frag == "_L" || this_frag == "_L_T") {
	pixel.y = 999;
      }
      
      if (this_frag == "_B" || this_frag == "_R_B") {
	pixel.x = 0;
      }
      
      if (this_frag == "_B" || this_frag == "_R_B" || this_frag == "_L_B") {
	pixel.y = y - 1;
      }
      
      if (this_frag == "_L" || this_frag == "_L_B" || this_frag == "_L_T") {
	pixel.x = 1000 - x;
      }

      n = file_names.begin();
      p = pixels.begin();
      a = areas.begin();
      while (a != areas.end() && area < *a) {
	n++;
	p++;
	a++;
      }

      file_names.insert(n, *i);
      pixels.insert(p, pixel);
      areas.insert(a, area);
    }
  }

  // Quick debugging check that none of the three lists became imballanced.
  if (file_names.size() != pixels.size() || pixels.size() != areas.size()) {
    assert(1);
  }

  // Although the functions that follow do not have a problem with this
  // condition, for now, don't proceed further if there is an incomplete
  // coverage of the concerned area.  This is done since more maps may be
  // downloaded in the future, and it would be annoying to selectively find
  // and replace the fragments that surround those maps if they are labeled
  // as merged maps rather than as fragments.  By happy conincidence, the
  // following condition works perfectly.
  // THIS CONDITION ALSO HANDLES THE CASE WHERE TWO MAPS HAVE ONLY CORNER
  // COVERAGE OF A REGION, BUT THE OTHER TWO NEARBY MAPS HAVE AN ENTIRE
  // SIDE COVERED (AND HENCE DO NOT SHOW AS TWO OTHER CORNERS, BUT AS TWO OTHER
  // SIDES, WHICH THE CORNER TEST DOESN'T LOOK FOR.
  /*
  if (file_names.size() != frag_id.size()) {
    for (list<string>::const_iterator j = file_names.begin(); j!= file_names.end(); j++) {
      cout << "next j is " << *j << "\n";
    }
    cout << "frag_id is " << frag_id << "\n";
    cout << "frag_id.size() is " << frag_id.size() << "\n";
    cout << "Incomplete fragment cover of the area to be merged into.\n"
	 << "   Current default is to not proceed\n";
    return 0;
  }
  */

  string bmp_name = base_name + "_fm.bmp";

  if (!merge_bitmaps(file_names, pixels, bmp_name)) {
    cout << "Fragements read and processed, but merging process failed.\n";
  }

  return 0;
}
