
                STEERING MESSAGES SPECIFICATION 


SUMMARY
        This document outlines all the messages that the steering needs to
        function as part of the DGC application, the direction of communication,
        and any other vital details.  Revision history is documented in CVS.

        NOTE:   Right now, these messages do not have types or sizes or actual
                constants/names and are not implemented.



MESSAGE DESCRIPTIONS


'power cycle controller' -OR- 
'poweroff controller' and 'poweron controller'
        The controller has to be able to be turned on and off somehow.  Since it
        is assumed to be on when the code is executed, all that is really
        necessary is for a power-cycling command that is smart enough to just
        turn on power if it wasn't off before.  The only trick with this is that
        the power loss needs to be for ___XXX___ seconds so that the controller
        actually shuts down.  It may be easier to implement this as power-on and
        power-off commands with a sleep in between.  The latter approach carries
        the advantage that all the steering-related timing stays with the
        steering code.  On the other hand, this approach may be dangerous if the
        steering glitches and transmits serial data on shutdown (never actually
        observed), in which case the serial code would awake the sleep, not
        allowing the requisite down time to reset the controller.
        NOTE:   There need not be a message to tell the steering code to power
                cycle the controller, since (a) the steering can't do itself
                anyway and (b) the steering will attempt to cycle power on
                initialization if necessary.

        Which ever communication(s) are chosen need to be bi-directional and
        confirmed, since the steering actuation needs to wait until the device
        has or is correctly power cycling before continuing operation.



'is the vehicle on' -OR-
'is the car jacked up' -OR-
'???'
        This is important because the steering code should wait for power
        steering in the vehicle before trying to calibrate, since the
        calibration procedure relies on being able to move the wheels.

        This communication is one way, with on/off messages being sent to the
        steering when power cycles happen for the vehicle.



'direction input' -OR-
'steer heading'
        This is a number ranging from [-1,1] for [left,right].

        Currently, this is a one-way communication, i.e. there are no
        communications returning to the requester that the steering has been
        undertaken.  A more bullet-proof system would have this, although it
        would generate a lot of uneccessary traffic.



'request steering limits'
        This will return the software and hardware limits on the steering to
        the requestor.  Units are in RADIANS.

        Communication directionality is obvious to the casual observer.



'request steering heading'
        This will return the current steering heading in RADIANS to the
        requestor.

        Communication directionality is obvious to the casual observer.

        

'reinitializing'
        Sent by the steering code to the controlling code ... a reply is not
        waited for since this is only a notification.

        One way communication to the controller.
        


'want to reinitialize'
        Signal to the control structure that the steering would like to (and
        will) reinitialize.  Initialization (see 'reinitializing') will occur
        either when an ok is received from the controller or when a timeout is
        achieved (we don't ask to reinitialize without it being necessary).
        The waiting behavior is motivated by a desire to let the vehicle slow
        down before reinitialization is attempted and the wheels swerve to
        there maxima (safety risk depending on speed). 

        Definitely one-way communication, including the time the request was
        sent.  Optionally replied to, including the request time (so you know
        if the current reinitialization is being authorized, instead of a
        delayed delivery of a request that timed out).



'reinitialize'
        This command tells the steering to reinitialize from whereever it is
        in code and is followed by a 'want to reinitialize' message, which is
        then followed by a 'reinitializing' and the actual reinitialization.
        This whole set of messages is to ensure that if anyone wants to
        reinitialize the steering, the system is notified that it is happening
        (in an ideal world, only the control system would command
        reinitialization and thus not need this, but this is safer).

        This message is sent to the steering my some other system and nothing
        is returned directly, although more reinitialization communications
        ensue.



'get current heading'
        This returns the most current heading, and slows down the steering
        response time since we have to wait for the system's reply.  The unit
        is RADIANS.

        This communication is bi-directional and a heading is returned.


