// #include "moveCar/brake.h"
#include "brake.h"
#include "TLL.h"
#include <iostream>
#include <time.h>

using namespace std;

double our_max_pos = .95;
double our_min_pos = .05;

//The following four variables are used by set_brake_abs_pos_pot to
//move the brake's absolute position based on the pot
//Although they initialize below to values that will give decent braking
//characteristics most of the time, it is better to calibrate them using the
//calib_brake function
//If we use the values below as static var's we need to a large set of 
//counts that we can average on.
// Limits set by hardware at Santa Anita, 9/12/2003
double min_pot=0.37;
double max_pot=0.90;
double min_counts=0.0;
double max_counts=-119868;
/*
double min_pot=0.3;
double max_pot=0.90;
double min_counts=0;
double max_counts=-135700;
*/

//The following variable is used to integrate movements called with
//set_brake_abs_pos and set_brake_abs_pos_pot.  The purpose is for the command
//to always return within 1/10 second, even if the requested movement is
//incomplete, and yet to make up for lost movement the next time the function
//is called
double remainder_pos = 0;


//Makes use of the receive_brake_command to reduce the need for extra code
//Just uses a dummy buffer
int send_brake_command(int command_num, char* parameters, int length) {
  char buffer[50];

  return receive_brake_command(command_num, parameters, length, buffer, 50, 1);
}

//Returns the response from sending a brake command
int receive_brake_command(int command_num, char* parameters, int length, char* buffer, int buf_length, int timeout) {
  char command[400];
  char word[11];  

  //Set the strings to null
  memset(command, 0, strlen(command));
  memset(buffer, 0, buf_length);

  //Prepare the command according to the brake actuator specs
  command[0]='@';
  sprintf(word, "%d ", BRAKE_ID);
  strcat(command, word);
  sprintf(word, "%d ", command_num);
  strcat(command, word);
  strcat(command, parameters);
  strcat(command, "\r\n");

  //Write the command
  serial_write(BRAKEPORT, command, strlen(command)-1, SerialBlock);
  
  //Read the response
  //serial_read_until(BRAKEPORT, buffer, '\r', timeout, buf_length);
  serial_read_until(BRAKEPORT, buffer, '\n', timeout, buf_length);

  //If it's a negative acknowledge (it starts with a !)
  if(strncmp(buffer, "!", 1)==0) {
    //Return an error
    return -1;
  } else {
    //Otherwise it's good
    /*  
    for (int i = 0; i < strlen(command); i++) 
      cout << command[i];
    cout << endl;
    
    for (int i = 0; i < strlen(buffer); i++) 
      //printf("%X ", (int)buffer[i]);
      cout << buffer[i];
    //cout << endl;
    */
    return 0;
  }

}

//Sends a polling command to see if the brake is ready
// after a command has been sent.
// Also handles holding/moving error.
int brake_ready() {
  char response[50];
  
  receive_brake_command(0, " ", 0, response, 50, 1);
  if(strcmp(response, "# 10 0000 2000\n")==0) {
    return 0;
  }
  else if (strcmp(response, "# 10 0000 0100\n") == 0
        || strcmp(response, "# 10 0000 0200\n") == 0) {
    cout << "Brake ERROR~ restarting!" << endl;
    reset_brake();
    return 0;
  } 
  else {
    return -1;
  }
}

//This function send the command to reset the brake
//And then waits until the brake is ready before returning
//IT IS A BLOCKING FUNCTION
int reset_brake() {
  double time;
  timeval before, after;
  double elapsed;
  char response[50];

  gettimeofday(&before, NULL);

  //Restarting it gets rid of moving error  
  //serial_write(BRAKEPORT, "@16 4 \r", strlen("@16 4 \r"), SerialBlock);
  send_brake_command(4, " ", 0);

  //Wait until the actuator is ready again
  while(strcmp(response, "# 10 0000 2000\n")!=0) {
    //cout << "stop";
    receive_brake_command(0, " ", 0, response, 50, 1);
  }

  // should be calibrate it again?
  // or at least set the min max variables?
  // depends on how stuff is done:
  //  using set_brake_abs_pos_pot 
  //  => either must use hardcoded
  //         or use the first calibrated number and hope the program doesn't crash
  //         or calibrate every time we reset this (using brake pot).


  gettimeofday(&after, NULL);
  // Get the elapsed time. 
  elapsed = after.tv_sec - before.tv_sec;
  elapsed += ((double) (after.tv_usec - before.tv_usec)) / 1000000;
  // Fix Midnight rollover
  if ( elapsed < 0 ) {
    // Add number of seconds in a day
    elapsed += 24 * 3600;
  }
  //cout << "\nTime to reset in seconds: " << elapsed << endl;

  return 0;
}

//Initializes the brake
int init_brake() {
  char parameters[400];

  if (serial_open_advanced(BRAKEPORT, BRAKEBAUD, SR_PARITY_DISABLE | SR_SOFT_FLOW_DISABLE | SR_HARD_FLOW_DISABLE | SR_TWO_STOP_BIT) != 0) {
    printf("Unable to open com port %d \n", BRAKEPORT);
    return -1;
  } else {
    //If the serial port is initialized, send initialization commands to the brake
    //These are copied directly from the default initialization in the QuickSilver Controls Windows program
    //Note: They probably aren't needed!
    //For information on each command, look up the command ID (first argument in the send_brake_command command) in the QuickSilver manual

    //Format:
    //sprintf(parameters, "parameter format string", param1, param2, ...);
    //send_brake_command(command_num, parameters, strlen(parameters));

    // Because of the following line, this function cannot be called
    // before init_accel() or initBrakeSensors()
    // Get the current zero position: we should probably move the
    // actuator to the absolute zero and then do this... will init_brake()
    // always be followed by calib_brake? 

    //our_min_pos = getBrakePot();

    /*    
    sprintf(parameters, "%d ", 5136);
    send_brake_command(155, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 1);
    send_brake_command(185, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 0);
    send_brake_command(186, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 576);
    send_brake_command(174, parameters, strlen(parameters));

    //ACK Delay
    sprintf(parameters, "%d ", 0);
    send_brake_command(173, parameters, strlen(parameters));
    
    //MCT
    //FLC

    sprintf(parameters, "%d %d %d %d %d %d %d ", 0, 6, 6, 20, 20, 200, 500);
    send_brake_command(148, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 0);
    send_brake_command(237, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 0);
    send_brake_command(184, parameters, strlen(parameters));

    sprintf(parameters, "%d %d %d %d ", 15000, 20000, 6000, 10000);
    send_brake_command(149, parameters, strlen(parameters));

    sprintf(parameters, "%d %d ", 10, 4);
    send_brake_command(150, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 1250);
    send_brake_command(230, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 32767);
    send_brake_command(195, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 10);
    send_brake_command(212, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 52);
    send_brake_command(213, parameters, strlen(parameters));

    //Error Limits (ERL)
    sprintf(parameters, "%d %d %d ", 500, 200, 120);
    send_brake_command(151, parameters, strlen(parameters));

    //Kill Motor Condition (KMC)
    sprintf(parameters, "%d %d ", 128, 0);
    send_brake_command(167, parameters, strlen(parameters));

    sprintf(parameters, "%d %d ", 0, 83);
    send_brake_command(252, parameters, strlen(parameters));
    
    */

    /*
    //Soft Stop Limits
    sprintf(parameters, "%d ", SSL_REGISTER);
    send_brake_command(221, parameters, strlen(parameters));

    //Write Register (of min Soft Stop Limit)
    sprintf(parameters, "%d %d ", SSL_REGISTER, SSL_MIN);
    send_brake_command(11, parameters, strlen(parameters));

    //Write Register (of max Soft Stop Limit)
    sprintf(parameters, "%d %d ", SSL_REGISTER+1, SSL_MAX);
    send_brake_command(11, parameters, strlen(parameters));
    */

    cout << "end of init" << endl;
    return 0;
  }
}

// Sets the brake's absolute position in 0-1 units
// 0: calibrated zero, 1:fully retracted
int set_brake_abs_pos(double abs_pos, double vel, double acc) {
  char parameters[50];
  char response[50];

  //Limit and scale the velocity, acceleration, and range
  vel=in_range_double(vel, 0, 1);
  vel=scale_double(vel, 0, 1, MIN_VEL, MAX_VEL);

  acc=in_range_double(acc, 0, 1);
  acc=scale_double(acc, 0, 1, MIN_ACC, MAX_ACC);

  // absolute position goes from 0 to 1, and this spans the length
  // from the zero position to the full position set in calibration.
  // in calibration, zero position should be the "fully" extended 
  // and the full position should be the "fully" retracted position.
  abs_pos=in_range_double(abs_pos, 0, 1);
  abs_pos=in_range_double(abs_pos + remainder, 0, 1);
  //abs_pos=scale_double(abs_pos, 0, 1, MIN_POS, MAX_POS);
  real_min_pos= MIN_POS*(our_max_pos-our_min_pos);
  real_max_pos= MAX_POS*(our_max_pos-our_min_pos);
  abs_pos=scale_double(abs_pos, 0, 1, real_min_pos, real_max_pos);
  abs_pos = in_range_double(abs_pos + remainder, real_min_pos, real_max_pos);
  current_pos = get_brake_pos();
  limited_abs_pos = in_range_double(abs_pos, current_pos - MAX_MOV_TEN, current_pos + MAX_MOV_TEN);
  remainder = abs_pos - limited_abs_pos;
  /*
  send_brake_command(5, " ", 0);
  send_brake_command(0, " ", 0);
  send_brake_command(1, "65535 ", strlen("65535 "));
  send_brake_command(3, "500000 ", strlen("500000 "));
  */

  //The following commands are copied directly from how the QuickSilverMax WIndows program does it
  //Send a polling command
  send_brake_command(0, " ", 0);
  //Clear the polling status word
  send_brake_command(1, "65535 ", strlen("65535 "));
  //Get revision info?
  send_brake_command(5, " ", 0);
  //Clear the program buffer
  send_brake_command(8, " ", 0);
  //Start the program download
  send_brake_command(9, " ", 0);

  //Move Absolute, Velocity Based (MAV)
  sprintf(parameters, "%.0f %.0f %.0f %d %d ", limited_abs_pos, acc, vel, STOP_ENABLE_ABS, STOP_STATE_ABS);
  send_brake_command(134, parameters, strlen(parameters));

  //End the program download
  send_brake_command(128, " ", 0);
  //Run the program
  receive_brake_command(10, " ", 0, response, 50, 1);

  if(strcmp(response, BRAKE_ACK)==0) {
    return 0;
  } else {
    return -1;
  }
}

//Sets the brake's position based on current pot value and calibration values
//abs_pos is on the 0-1 scale as follows
// 0: calibrated zero (no braking), 1:fully retracted (full-braking)
//Where these points are set during calibration
int set_brake_abs_pos_pot(double abs_pos, double vel, double acc) {
  double abs_pos_pot, rel_pos_pot, rel_pos_counts;

  if (abs_pos < 0.0 || abs_pos > 1.0)
    return -1;

  cout << "-set_brake_abs_pos_pot-Current brake position: " << getBrakePot() 
       << endl;
  cout << "-set_brake_abs_pos_pot-Setting the brake to: " << abs_pos << endl;

  //First, figure out what pot value the desired abs_pos would have
  abs_pos_pot=scale_double(abs_pos, 0, 1, min_pot, max_pot);
  //Then, figure out what the relative position change is in pot units
  rel_pos_pot=getBrakePot()-abs_pos_pot;
  if (abs(rel_pos_pot) < .02)         // 1% error margin
    rel_pos_counts = 0;
  else
    //Then, figure out how many counts the desired change in pot units corresponds to
    rel_pos_counts=scale_double(rel_pos_pot, -max_pot+min_pot, max_pot-min_pot, max_counts-min_counts, -max_counts+min_counts);

  cout << "-set_brake_abs_pos_pot- relative change: " << rel_pos_pot << endl;

  /*
  cout << "\n*Min Pot, Max Pot: " << min_pot << ", " << max_pot;
  cout << "\n*Min Count, Max Count: " << min_counts << ", " << max_counts;
  cout << "\n*abs_pos_pot: " << abs_pos_pot << endl;
  cout << "\n*current pot val: " << getBrakePot();
  cout << "\n*pot change: " << rel_pos_pot;
  cout << "\n*rel_pos_counts: " << rel_pos_counts << endl;
  fflush(stdout);
  */

  //Then move that many counts
  return set_brake_rel_pos_counts(rel_pos_counts, vel, acc);
}

// Sets the brake's relative position in 0-1 units
// still in the full-scale units. i.e. calibrated zero position doesn't matter.
//  if we change this, we'd need to change calib_brake.
int set_brake_rel_pos(double rel_pos, double vel, double acc) {
  char parameters[50];
  char response[50];

  //Limit and scale the velocity, acceleration, and range
  vel=in_range_double(vel, 0, 1);
  vel=scale_double(vel, 0, 1, MIN_VEL, MAX_VEL);

  acc=in_range_double(acc, 0, 1);
  acc=scale_double(acc, 0, 1, MIN_ACC, MAX_ACC);

  // leaving relative position in absolute scale (i.e. 1 means the full
  // length of the actuator itself.
  rel_pos=in_range_double(rel_pos, -1, 1);
  //rel_pos=in_range_double(rel_pos, our_min_pos-getBrakePot(), our_max_pos-getBrakePot());
  rel_pos=scale_double(rel_pos, -1, 1, -MAX_POS, MAX_POS);
  //rel_pos=scale_double(rel_pos, -1, 1, -MAX_POS*(our_max_pos-our_min_pos), MAX_POS*(our_max_pos-our_min_pos));

  //send_brake_command(5, " ", 0);
  //send_brake_command(0, " ", 0);
  //send_brake_command(1, "65535 ", strlen("65535 "));
  //send_brake_command(3, "500000 ", strlen("500000 "));
  //Send a polling command
  send_brake_command(0, " ", 0);
  //Clear the polling status word
  send_brake_command(1, "65535 ", strlen("65535 "));
  //Get revision info?
  send_brake_command(5, " ", 0);
  //Clear the program buffer
  send_brake_command(8, " ", 0);
  //Start the program download
  send_brake_command(9, " ", 0);

  //Move Relative, Velocity Based (MRV)
  sprintf(parameters, "%.0f %.0f %.0f %d %d ", rel_pos, acc, vel, STOP_ENABLE_ABS, STOP_STATE_ABS);
  send_brake_command(135, parameters, strlen(parameters));

  //End the program download
  send_brake_command(128, " ", 0);
  //Run the program
  receive_brake_command(10, " ", 0, response, 50, 1);

  if(strcmp(response, BRAKE_ACK)==0) {
    return 0;
  } else {
    return -1;
  }
}

int set_brake_rel_pos_counts(double rel_pos, double vel, double acc) {
  char parameters[50];
  char response[50];

  //Limit and scale the velocity and acceleration
  vel=in_range_double(vel, 0, 1);
  vel=scale_double(vel, 0, 1, MIN_VEL, MAX_VEL);

  acc=in_range_double(acc, 0, 1);
  acc=scale_double(acc, 0, 1, MIN_ACC, MAX_ACC);

  //rel_pos=in_range_double(rel_pos, -MAX_POS*(our_min_pos-getBrakePot()), MAX_POS*(our_max_pos-getBrakePot()));

  //send_brake_command(5, " ", 0);
  //send_brake_command(0, " ", 0);
  //send_brake_command(1, "65535 ", strlen("65535 "));
  //send_brake_command(3, "500000 ", strlen("500000 "));
  //Send a polling command
  send_brake_command(0, " ", 0);
  //Clear the polling status word
  send_brake_command(1, "65535 ", strlen("65535 "));
  //Get revision info?
  send_brake_command(5, " ", 0);
  //Clear the program buffer
  send_brake_command(8, " ", 0);
  //Start the program download
  send_brake_command(9, " ", 0);

  //Move Relative, Velocity Based (MRV)
  sprintf(parameters, "%.0f %.0f %.0f %d %d ", rel_pos, acc, vel, STOP_ENABLE_ABS, STOP_STATE_ABS);
  send_brake_command(135, parameters, strlen(parameters));

  //End the program download
  send_brake_command(128, " ", 0);
  //Run the program
  receive_brake_command(10, " ", 0, response, 50, 1);

  if(strcmp(response, BRAKE_ACK)==0) {
    return 0;
  } else {
    return -1;
  }
}

//Get the actuator position in counts
int get_brake_pos() {
  char buffer[50];
  int command, upper_word, lower_word, position;

  receive_brake_command(12, "1 ", strlen("1 "), buffer, 50, 1);  

  sscanf(buffer, "# 10 %X %X %X", &command, &upper_word, &lower_word);

  upper_word <<= 16;
  position=lower_word+upper_word;

  if(command == 12) {
    return position;
  } else {
    return -1;
  }
}


// need to set both the zero and the full position -- read manual!!!
int calib_brake() {
  float pos;
  int val;
  char comm;
  //Restart the Actuator
  send_brake_command(4, " ", 0);
  //Wait until the actuator is ready again
  while(brake_ready()!=0) {};
  //Send the brake all the way out
  //set_brake_rel_pos_counts(250000, 1, 1);  // BADDDDD - breaks brake
   do {
          printf("\nCurrent Position(current setting, counts): %d", get_brake_pos());
          printf("\nCurrent Position(pot reading, 0-1): %f\n", getBrakePot());
	  printf("Enter command (c: continue to move actuator z: set zero(full-out?), f: set full-position(all-in?), q: quit): ");
	  cin>>comm;
	  cout<<endl;
	  if(comm=='c')
          {
	    printf("\nEnter rel pos change (0-1): ");
	    scanf("%f", &pos);
	    if (getBrakePot()+pos <= 0.05 || getBrakePot()+pos >= 1) {
	      char cont = 'c';
	      while (cont != 'y' && cont != 'n') {
		cout << "This may break the actuator, continue? (y=yes,n=no):";
		cin >> cont;
		cout << endl;
	      }
	      if (cont == 'y') {
		val = set_brake_rel_pos(pos, 0.02, 0.02);
		printf("\nVal: %d\n", val);  
	      }
	    }
	    else {
	      val = set_brake_rel_pos(pos, 0.05, 0.05);  //orig 0.9
	      printf("\nVal: %d\n", val);  
	    }
	  }
          while(brake_ready()!=0) {
	    //	    cout << "waiting for the brake actuator to finish" << endl;
	  };    
          //usleep(1500000);
          //sleep(2);          // wait for the actuator to finish moving?
    } while(comm!='q' && comm!='z' && comm!='f');     
   if(comm=='z') { 
     //Restarting it gets rid of moving error
     //send_brake_command(4, " ", 0);
     //Wait until the actuator is ready again
     //while(brake_ready()!=0) {};
     //set_brake_rel_pos_counts(-10000, 0.06, 0.06);
     //sleep(2);
     //set_brake_rel_pos_counts(ZERO_POS+10000, 0.05, 0.05);
     //Make the current position the 0 position
     our_min_pos = getBrakePot();
     min_pot=getBrakePot();
     min_counts=get_brake_pos();
     cout << "\n*min_pot set to " << min_pot;
     cout << "\n*min_counts set to " << min_counts;
     return send_brake_command(145, " " , 0);
   }
   else if(comm=='f') {
     //Restarting it gets rid of moving error
     //send_brake_command(4, " ", 0);
     //Wait until the actuator is ready again
     //while(brake_ready()!=0) {};
     //sleep(2);
     //Make the current position the 1 position...???
     //return send_brake_command(145, " " , 0);
     our_max_pos = getBrakePot();
     max_pot=getBrakePot();
     max_counts=get_brake_pos();
     cout << "\n*max_pot set to " << max_pot;
     cout << "\n*max_counts set to " << max_counts;
   }
   return 0;
}


//Helper functions for limiting and scaling numbers
double in_range_double(double val, double min_val, double max_val) {
  if(val<min_val) return min_val;
  if(val>max_val) return max_val;
  return val;
}

double scale_double(double val, double old_min_val, double old_max_val, double new_min_val, double new_max_val) {
  double old_range, new_range;

  old_range=old_max_val-old_min_val;
  new_range=new_max_val-new_min_val;

  return ((new_range*(val-old_min_val)/old_range)+new_min_val);
}
