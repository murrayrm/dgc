//#include "moveCar/brake.h"
#include "brake.h"
#include "TLL.h"

#include <time.h>
#include <stdio.h>
#include <iostream>

using namespace std;

int main() {
  float pos;
  int val;
  cout << "initializing..." << endl;
  initBrakeSensors();
  // currently, init_brake() must come after initBrakeASensors
  if (init_brake() != 0)  
    cout << "Error initializing brake actuator." << endl;
  //  init_accel();
  
  char calib;
  bool calibdone = false;
  while (!calibdone) {
    cout << "calibration? (y or n): ";
    cin >> calib;
    cout << endl;
    if (calib == 'y' || calib == 'Y')
      calib_brake();
    else if (calib == 'n' || calib == 'N') {
      calibdone = true;
      cout << "no calib" << endl;
    }
  }

  // now move it around a bit to see how it behaves
  while(true) {
    char absrel;
    double disp;
    cout << "current position (potreading): " << getBrakePot() << endl;
    cout << "absolute, relative, or pot? (a or r or p): ";
    cin >> absrel;
    cout << endl;
    cout << "Enter position change (-1 to 1): ";
    cin >> disp;
    if (absrel == 'a') {
      cout << absrel << endl;
      val = set_brake_abs_pos(disp, 0.05, 0.05);
    }
    else if (absrel == 'r') {
      cout << absrel << endl;
      val = set_brake_rel_pos(disp, 0.05, 0.05);
    }
    else if (absrel =='p') {
      cout << absrel << endl;
      val = set_brake_abs_pos_pot(disp, 0.05, 0.05);
    }
    while(brake_ready()!=0) {};    
    cout << endl;
    cout << "val = " << val << endl;
    //sleep(2);
  }

  /* For any kind of brake movement, must use the calib_brake() limits.
  //for(int x=0; x<2; x++) {
  //pos=(float)x/10.0;
  printf("\nCurrent Position: %d", get_brake_pos());
  printf("\nEnter rel pos change: ");
  scanf("%f", &pos);
  val = set_brake_rel_pos_counts(pos, 0.9, 0.9);
  //cout << "Trying " << pos << endl;
  //val = set_brake_abs_pos(pos, 0.9, 0.9);
  while(brake_ready()!=0) {};    
  printf("\nVal: %d\n", val);  
  //usleep(1500000);
  sleep(2);
  //}    
  */
      
  return 0;
}

