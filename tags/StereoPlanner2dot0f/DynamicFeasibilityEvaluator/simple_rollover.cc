
#include "simple_rollover.h"

void simple_rollover( const double H, const double L, const double R, const double m, const double a_tot, const double g_z, const double g_y,
                    double *c_roll, double *F_L_z, double *F_R_z)
{                    
    /*
    % LG031122
    %
    % Inputs:
    %   - H     : [m], height of Center of Mass from ground
    %   - L     : [m], distance of left wheel to projection of CM down on axle
    %   - R     : [m], distance of right wheel to projection of CM down on axle
    %               NOTE: both L, R are postive numbers; they are distances
    %
    %   - m     : [kg], mass of the vehicle
    %
    %   - a_tot : [m/s^2], the resultant acceleration of the vehicle;
    %               the centripetal acceleration due to the arc path,
    %               a purely lateral acceleration (along vehicle y axis)
    %               (+ve for rightward turns, -ve for leftward turns)
    %
    %   - g_z   : [m/s^2], the downward component of gravity, in the vehicle ref frame
    %   - g_y   : [m/s^2], the lateral component of gravity, in the vehicle ref frame
    %
    %   NOTE: for now we ignore the forward component of gravity (vehicle ref frame)
    %           because we have a simple model of the lateral dynamics only.
    %
    %   NOTE: if the vehicle were traveling on a level groundplane, g_z = g, g_y = 0;
    %         if the vehicle were rolled 90 degrees (driving on a 90 degree embankment),
    %         g_z = 0, g_y = g (or -g, depending on the direction of the embankment)
    %
    %   Outputs:
    %   - c_roll : [unitless] roll coefficient
    %               0 if vehicle is perfectly stable
    %               magnitude 1 if on the verge of rolling over
    %               magnitue > 1 if there is a rollover torque present
    %               c_roll > 0 if leaning to the right
    %               c_roll < 0 if leaning to the left
    %               so c_roll > 1 => rolling rightward
    %
    %   - F_L_z : [N], vertical (in vehicle ref frame) component of left wheel ground reaction force
    %   - F_R_z : [N], vertical (in vehicle ref frame) component of right wheel ground reaction force

    % Vehicle coordinate system:
    %   x forward
    %   y right
    %   z down


    % work in 2-D
    %
    % See notes_031123_3.jpg and _4.jpg for model and equations
    % Model is that only outer wheel has lateral ground force reaction.
    %
    %   Assumptions:
    %       - ignore effect of suspension in shifting COM of vehicle
    %       - calculating only the instantaneous effect of a particular arc,
    %           as if we switch steering angles instantaneously, ignoring the
    %           steering dynamics.
    %       - assuming instantaneous veheicle velocity is tangent to instantaneous arc,
    %           ignoring dynamics of switching from one arc to another (ie, ignoring
    %           conservation of angular momentum)
    %       - also ignoring effect of throttle acceleration or braking
    %           NOTE: if we do want to include this, we have to distinguish
    %               between accel/decel due to engine torque/brakes and due
    %               to gravity. One is a force on the center of gravity, the
    %               others are torques around the rear axle.
    %       - assuming flat surface
    %       - assuming steering angle does not affect contact point of wheels
    %           (this seems reasonable)
    %       - assuming no slipping when computing roll torque. ? Does it make
    %           a difference ?
    */

    // total force is the centripetal force due to traveling on the arc trajectory
    double F_T = m * a_tot; 

    // evaluate vertical and lateral components of gravity force
    double F_g_y = m * g_y;
    double F_g_z = m * g_z;

    /* equations describing total force as combination of gravity and ground forces
        F_T = F_g + F_L + F_R             (vector form)
        F_T_y = F_g_y + F_L_y + F_R_y     (lateral component)
          0   = F_g_z + F_L_z + F_R_z     (vertical component)
    */

    // compute vertical component of wheel reaction forces

    *F_R_z = ( L * F_g_z - H * (F_T - F_g_y) ) / ( L + R );

    *F_L_z = ( R * F_g_z + H * (F_T - F_g_y) ) / ( L + R );

    // compute rollover coefficient
    // as defined in "Vehicle_dynamics_control_for_rollover_avoidance.pdf"

    *c_roll = ( *F_R_z - *F_L_z ) / (*F_R_z + *F_L_z);

    /*
    %      = ( (L-R) F_g - 2 H F_T ) / (L+R) F_g
    % equivalent to pdf formula when L = R 

    % compute approximate rollover coefficient
    % from "Vehicle_dynamics_control_for_rollover_avoidance.pdf"
    % to double check.
    %
    % c_roll_check = 2 * H / (L + R) * F_T / F_g;
    */
}

