LADARMapper usage:

To make, just run "make", there is no make install

Run ./LADARMapper -help for info on the available options.  It is
configured that for race day it should be run with no options.  We may
optionally want to have logging turned on for race day, in which case
use -log switch.

LADARMapper needs to be able to talk to vstate.  It will abort with an
error if it is unable to communicate with vstate.  use -nostate option
to not abort.

LADARMapper will also try to send votes to the arbiter.  It will not
detect if the arbiter is unable to receive the votes, so that should
be specifically looked for.

For a nice debugging feature, try uncommenting the #define ASCII_MAP line
towards the top, then be sure to run WITHOUT the -disp switch.
======================================================================

This file explains the ladar logging file format. there are 3 files
for each run.  One contains state data, one contains scan data, and
the last contains vote data.

--
The votes log has the following format:

time_sec time_usec vote_index velocity goodness

e.g. vote_index goes from 0 to 24 since we have 25 angles to vote on.
So each set of 25 lines constitutes a single set of votes.

--
The scan log has the following line format:

time_sec time_usec range_i

where range_i is the range of the i_th scanline, and there are 361
ranges.  so a single line in the scan log has 363 numbers on it : 2
numbers for the timestamp, and then the 361 ranges.  The ladar scans
from right to left, i.e. 0 degrees is on the units right and 180
degrees is on the units left.  The ranges are in cm.

--
The state log has the following line format:

time_sec time_usec easting northing altitude vel_e vel_n vel_u speed
accel pitch roll yaw pitchrate rollrate yawrate gps_week
gps_ms_in_week gps_sats_used gps_lat gps_lng gps_nav_mode
gps_ellipsoidal_height gps_altitude gps_vel_n gps_vel_e gps_vel_u
gps_position_fom gps_gdop gps_pdop gps_hdop gps_vdop gps_tdop gps_tfom
gps_max_dgps_corr_age gps_dgsp_config gps_extended_nav_mode
gps_antenna_height_adj gps_adjust_v_height imu_dvx imu_dvy imu_dvz
imu_dtx imu_dty imu_dtz mag_heading mag_pitch mag_roll

This is all in one single line.  Note that a few of the values are
chars, so a few of them may look kind of funny.  I don't know for sure
what some of the gps stuff is, but I think we are logging everything
we can get our hands on.

