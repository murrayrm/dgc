Design Specification for the Arbiter module

--------------------------------------------

Revision History:

  2/1/04   Sue Ann Hong    Created in team/RaceModules/Arbiter,
                           Modified from a previous version

--------------------------------------------

 . Current version (implemented)

  - Structs & Classes
   + struct Vote (Vote.hh)
    double goodness;
     The full span is from 0 to 100. Here are the goodness values each Planner
     should use (at least currently). For now, let's just deal with a few
     values, since it's hard to fully use this scale without each module being
     biased depending on the implementer. 
     Goodness values are determined by the path evaluation methods in genMap
     for most of the planner modules, so one need not worry so much about
     the consistency of goodness values. May need to make sure DFE follows
     the same lines. 
       0   NEVER			      ex// a big tree, a concrete wall
      20   May be feasible
      40   Barely feasible
      60   A bit tough, but can do it
      80   Pretty nice
     100   No problem
     100+  Reserved
      102  NO_CONFIDENCE (SimpleArbiter.hh) : The Voter cannot say anything
            about the arc, lacking info.
    double velo;
     The velocity at which the vehicle can travel the arc safely. If the arc is
     not traversable, this should be zero.
    Note: Doesn't contain the steering angle.
 
   + struct Voter (SimpleArbiter.hh)
    = Each Planner is a Voter to the Arbiter and has a weight that determines
      how must influence the Voter/Planner has on the final command.
    Attr:
     double VoterWeight;
     Vote Votes[NUMARCS];
     int ID;

   + MTA module Arbiter's datum d (ArbiterModuleDatum.hh)
    The Arbiter MTA module defines a set of Voters and keeps them updated
    with data from Update messages. Following are variables it keeps inside
    the datum d to do so.
     Voter allVoters[];  - List of Voters
     Voter combined;     - The Ultimate Voter that has combined info from
			   all valid Voters.
     Timeval lastUpdate[];  - Keeps track of when a Voter was last updated
     ...
     Other logging/timestamp/locking variables


  - Logic & methods

   + Communication and action at the MTA level

   Messages:
    Incoming:
     (from all Planners) Update(Voter) : Votes from each planner
    Outgoing:
     (to Vdrive) CmdMotion(velociy_cmd, steer_cmd, Timestamp) 
     (to State) UpdateState() : 
       State query (get_vstate.cc). Only for logging and displaying purposes.

   Arbiter at the level of MTA/main (ArbiterModule.cc)
     [Planner Modules: LADAR, Stereo, Global, DFE]
    Active():
     Loop {
      1. Go through the array of Voters representing all known Voters and
         update info from Update messages that came in in the last cycle. If
         it's been too long since the Voter was updated, then do not add the
         Voter to the list of Voters to be considered for choosing the best
         arc. Otherwise, add the Voter to the list.
      (Decision maker)
      2. If the Global Voter is valid*, then Calculate the ultimate
         goodness for arcs and pick the best arc (PickBest(), 
	 SimpleArbiter.cc,.hh)'. Send the resulting command.
         Else (i.e. no Global/no modules) stop the car.
         Else send (v, phi) msg (VDriveMessages::CmdMotion) to module vdrive.
      4. Sleep for 10 seconds to wait for new planner Update messages.
     }

    * Note: The Arbiter is only "active" until when receives signals from
    global. When Global finishes the arbiter will deactivate.  Therefore if all
    goes well when the global module reaches the last waypoint the vehicle is
    immediately brought to a stop. (Ike Gremmer)

    ' Calculating the ultimate goodness for arcs and picking the best arc:
      an angle's Goodness = min(goodness from all Voters for this angle)
      an angle's Velocity = min(Velocity from all Voters for this angle)

    MsgHandlers:
     Start()
     Stop()
     Update(Voter updates)
     1. Get MTA messages as soon as they come in
     2. Update the corresponding Voter


   + Arbiter methods

   double GetPhi( int arcnum )
   Voter CombineVotes(list<Voter> voter_inputs);
   ActuatorCommand PickBestCombinedArc(Voter& combined);
   ArbiterBestResult PickBest(list<Voter> voter_inputs)
   Voter CombineVotes(list<Voter> voters)
   ActuatorCommand PickBestCombinedArc(Voter& combined)
   
-------------------------------------------------------------------------------

 . Design in progress

  Messages
   Incoming:
    (all Planners) Update(Voter, double leftPhi, double rightPhi, 
		          double Timestamp).
    (Vmanage?) Start
    (Vmanage?) Stop
    (from Vmanage) ModeSwitch (int newMode)
   Outgoing:
    (to Vmanage? and planners - DFE, LADAR?) Switch camera pairs
    (to Vmanage?) No modules running

  Modes - The whole system can be in different modes to react to different
          situtations. 
   Vmanage-defined modes:
    FULL : fully operational.
    NOREVERSE : transmission stuck. No reverse possible.
    DEGRADED: actuator not fully functional? HW defunct?
    STOP: Must stop to calibrate HW/cool engine down/E-stop/etc.
   Planning-defined modes:
    REVERSE : going reverse
    STOP: No planner alive/obstacle close ahead/etc.

  Arbiter at the level of MTA/main (ArbiterModule.cc):
    [Planner Modules: LADAR, Stereo, Global, DFE]
  Active():
   Loop {     
     switch(mode)
     // Enable msg handling (and hence decision making) while in here.
   }

  MsgHandlers:
   Update (Voter updates)
    1. Get MTA messages as soon as they come in
    2. Update the corresponding Voter
    3. Call the decision maker (trigger)
   ModeSwitch (int newMode) : mode = mode
    
  Other functions:
   Decision maker  (SimpleArbiter.hh,.cc)
    1. Calculate the ultimate goodness for arcs and pick the best arc*
    2. If (none good/no modules) 
        // CONTINGENCY MANAGEMENT
        then stop the car and display/log. (or switch mode to rear cameras)
	// At this point, should pause for a bit, try evaluating again,
	// if we get Votes, proceed from there.
	// if the obstacle is gone, then it was a moving object, so proceed
	// if we don't get anything or the obstacle persists, go to REVERSE
       Else if (command not too different from the last one and 
                && much time has passed) 
        then send nothing.
       Else 
        send (v, phi) msg (VDriveMessages::CmdMotion) to module vdrive.
   transitionToReverse() & transitionToFull()
    1. Inform Vdrive (to change the gear), stereo/LADAR (switch camera/LADAR?),
       and Vmanage.
    2. mode = REVERSE; or mode = FULL;

  * Calculating the ultimate goodness for arcs and picking the best arc
     - Dependent on the current mode?

-------------------------------------------------------------------------------

 . Status
  - More work on logic/contingency management and implementing the Arbiter's
    start/stop/restart and algorithms for the active state that takes the
    current mode into account.

-------------------------------------------------------------------------------

 . Planners (some notes for now)

 > Arc generator must be uniform for all planners
  - genMap handles this in the path evaluator methods
  - Currently assumes constant velocity

 > Each estimator should generate arcs based on the arcs indicated by the
  Arbiter, evaluate them, and send the result to the Arbiter (Voter, Timestamp)
  as fast as possible. The timestamp should probably be the one from the state
  struct with which the planner evaluated the arcs.

 > Waypoint & Corridor handler
  - one uniform function to be used in Global and some local modules, that puts
 down corridor information as obstacles or a special value. We need to treat 
 "out of bounds" areas as obstacles for D* to create targetpoints, but in the
 Global planner we may need to get rid of those obstacles, since it may lead us
 to dead end due to the coarse resolution at the global level. We could have a
 CorridorPlanner module that only deals with corridor following in the local
 20cm x 20cm scale, using the same path evaluation function as stereo and
 LADAR. This would better ensure that we stay in bounds, because it's more
 explicit and also is on a better resolution.
