#ifndef STEREOPLANNERDATUM_HH
#define STEREOPLANNERDATUM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "../Arbiter/Vote.hh"
#include "../Arbiter/SimpleArbiter.hh"
#include "../Arbiter/ArbiterModule.hh"

#include "../../include/Constants.h" //for vehicle constants, ie serial ports, etc.

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"
#include "vehlib/VDrive.hh"

#include <stereovision/svsStereoWrapper.hh>
#include <frames/frames.hh>
#include <genMap.hh>

using namespace std;

// All data that needs to be shared by common threads
struct StereoPlannerDatum {

  Voter stereo; // voter struct for the Arbiter

  // State struct from VState
  boost::recursive_mutex StateStructLock;
  VState_GetStateMsg SS;
  VState_GetStateMsg tempState;

  boost::recursive_mutex CameraLock;
  bool *stopCapture;
  svsStereoWrapper svsObject;

  genMap svMap;
};


#endif
