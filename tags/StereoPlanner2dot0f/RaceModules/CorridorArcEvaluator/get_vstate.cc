/*
** get_vstate.cc
**
**  
** 
**
*/

#include "CorridorArcEvaluator.hh" // standard includes, arbiter, datum, ...

void CorridorArcEvaluator::UpdateState()
{ 
  Mail msg = NewQueryMessage( MyAddress(), MODULES::VState, VStateMessages::GetState);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> d.SS; // download the new state
  }

} // end CorridorArcEvaluator::UpdateState() 
