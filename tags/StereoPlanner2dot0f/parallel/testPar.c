#include "parallel.h"
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#define SOME_PORT_NUM   1

int main() {

    /* Example writing data */
    /*
    printf("pp_init(write): %d\n", pp_init(SOME_PORT_NUM, PP_DIR_OUT));
    printf("pp_set_data: %d\n", pp_set_data(SOME_PORT_NUM, 0xF0));
    printf("pp_set_bits: %d\n", pp_set_bits(SOME_PORT_NUM, PP_PIN01 | PP_PIN14, PP_PIN16 | PP_PIN17));
    pp_end(SOME_PORT_NUM);
    */
    
    /* Example reading data at one-second intervals */

    /* Initialize the port (SOME_PORT_NUM) */
    if (pp_init(SOME_PORT_NUM, PP_DIR_IN) != -1) 
    {
        printf("pp_init(read) of port %d was successful !\n", SOME_PORT_NUM);
    } 
    else 
    { 
        fprintf(stderr, "%s (%d): pp_init(read) of port %d failed with error \"%s\" ... aborting.\n", 
                __FILE__, __LINE__, SOME_PORT_NUM, strerror(errno));
        return -1;
    }
    /* Begin infinite loop. */
    while(1) {
        printf("pp_get_data: %x\n", pp_get_data(SOME_PORT_NUM));
        printf("pp_get_bit (PIN15): %d\n", pp_get_bit(SOME_PORT_NUM, PP_PIN15));
        printf("pp_get_bit (PIN13): %d\n", pp_get_bit(SOME_PORT_NUM, PP_PIN13));
        printf("pp_get_bit (PIN12): %d\n", pp_get_bit(SOME_PORT_NUM, PP_PIN12));
        printf("pp_get_bit (PIN10): %d\n", pp_get_bit(SOME_PORT_NUM, PP_PIN10));
        printf("pp_get_bit (PIN11): %d\n", pp_get_bit(SOME_PORT_NUM, PP_PIN11));
        sleep(1);
    }
    /* Not like we'll ever get here ... */
    pp_end(SOME_PORT_NUM);

  return 0;
}
