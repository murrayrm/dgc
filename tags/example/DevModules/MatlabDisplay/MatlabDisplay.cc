
#include "MatlabDisplay.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include <unistd.h>
#include <iostream>

MatlabDisplay::MatlabDisplay() : DGC_MODULE(MODULES::MatlabDisplay, "Matlab Display", 0)
{
}

MatlabDisplay::~MatlabDisplay()
{
}


void MatlabDisplay::Init() {
    // call the parent init first
    DGC_MODULE::Init();

    //Insert code to initialize MatlabDisplay here

    // LG: code from DFEPlanner_Matlab/DFEPlanner.cc
    // Initialize the Matlab engine

    _counter = 0; // setup our fake data generator, remove once message passing with real data is available

    // start the Matlab Engine !!!
    // NOTE: see the Matlab External Interfaces Guide for more details.
    //       It is available online at: 
    //          External Interfaces / API:
    //          http://www.mathworks.com/access/helpdesk/help/techdoc/matlab_external/matlab_external.shtml
    //          External Interfaces / API Reference
    //          http://www.mathworks.com/access/helpdesk/help/techdoc/apiref/apiref.shtml
    {
        fprintf(stderr,"Starting the matlab engine...\n");
        if (!(_ep = engOpen("\0"))) 
        {
            fprintf(stderr, "\nCan't start MATLAB engine\n");
            return; // EXIT_FAILURE;
        }
        fprintf(stderr,"Matlab engine started.\n");
    }

    // ADD CODE HERE: Set up plot figures with graphics handles
    {
        // open figure and setup for quick plotting
        /*      Ordinarily, you would just plot data with a 
                    plot(x,y) command.
                However, if you keep track of the plotted element's handle
                    h_line = plot(x,y);
                Then you can replot new data much more quickly by doing
                    set(h_line, 'XData', x_new, 'YData', y_new);
                    drawnow;
        */

        engEvalString(_ep, " figure(100);                        ");

        engEvalString(_ep, " subplot(2,1,1)                      ");
        // h_votes is a handle to the line plot of votes
        // later we can efficiently replot it by replacing the
        // data that h_votes uses. 
        engEvalString(_ep, " h_votes = plot(zeros(1,25),'.-');   ");
        engEvalString(_ep, " xlabel('votes')                     ");

        engEvalString(_ep, " subplot(2,1,2)                      ");
        // likewise, h_vel is a handle to the line plot of velocities
        engEvalString(_ep, " h_vel = plot(zeros(1,25),'.-');     ");
        engEvalString(_ep, " xlabel('vel')                       ");

        engEvalString(_ep, " drawnow;                            "); // force rendering of plots

    }

    // ADD CODE HERE: Initialize the matlab variables
    _votes_M = mxCreateDoubleMatrix(1, NUMARCS, mxREAL);
    _vel_M = mxCreateDoubleMatrix(1, NUMARCS, mxREAL);

    // ADD CODE HERE: Initialize the C counterparts that point to the actual array of data
    _votes_C = mxGetPr(_votes_M); // mxGetPr means "Get 'P'ointer to 'r'eal (as opposed to complex) data
    _vel_C = mxGetPr(_vel_M);

    cout << "Module Init Finished" << endl;

    fprintf(stderr, "Sleeping for 5 seconds... this is what the plots look like after initialization\n");
    usleep(5000000);
}

void MatlabDisplay::Active()
{
    int i;
    double steerangle;

    cout << "Starting Active Funcion" << endl;
    while( ContinueInState() ) 
    {

        /****
            This was code from RoadFollower

        // update the state so we know where the vehicle is at
        UpdateState();

        // Update the votes ... 
        for(i = 0; i < NUMARCS; i++) 
        {
            steerangle = GetPhi(i);
            d.roadfollower.Votes[i].Goodness = MAX_GOODNESS/2;
            d.roadfollower.Votes[i].Velo = MAX_SPEED;
        }


        // and send them to the arbiter
        Mail m = NewOutMessage(MyAddress(), MODULES::Arbiter, ArbiterMessages::Update);
        // Send the arbiter our "ID" and the vote struct.  The weights are taken care of
        // by the arbiter.
        int myID = ArbiterInput::MatlabDisplay;
        m << myID << d.roadfollower;
        SendMail(m);

        ****/

        // ADD CODE HERE: Get messages from other modules

        // For now we just make up some data
        for(int i=0; i<NUMARCS; i++) 
        {

            _d.matlabdisplay.Votes[i].Goodness = 15* sin( (_counter + i)*3.14/180.0 );
            _d.matlabdisplay.Votes[i].Velo = 15* cos( (_counter + i)*3.14/180.0 );

            _counter += 1;

        }


        // ADD CODE HERE: Copy variables for matlab display
        // NOTE that since _votes_C points inside _votes_M, we've copied the
        // data to the matlab variable.

        for(int i=0; i<NUMARCS; i++) 
        {
            _votes_C[i] = _d.matlabdisplay.Votes[i].Goodness;
            _vel_C[i] = _d.matlabdisplay.Votes[i].Velo;
        }


        // ADD CODE HERE: Place the matlab variables into the MATLAB workspace.

        engPutVariable(_ep, "votes", _votes_M);
        engPutVariable(_ep, "vel", _vel_M);


        // ADD CODE HERE: Call matlab plot commands
        // for the fastest replotting, we just reset the 
        // Y-axis data that each line plot handle uses
        // (same number of elements, in vector, same XData)

        engEvalString(_ep, " set(h_votes,'YData',votes);         ");
        engEvalString(_ep, " set(h_vel  ,'YData',vel  );         ");

        engEvalString(_ep, " drawnow;                            "); // force rendering of plots

        // and sleep for a bit
        //usleep(100000);
    }

    cout << "Finished Active State" << endl;
}


void MatlabDisplay::Shutdown()
{

    // if we get to here a break has been detected 
    cout << "Shutting Down" << endl;

    //Insert code to shut down MatlabDisplay here

    // ADD CODE HERE: release matlab variables
    mxDestroyArray(_votes_M);
    mxDestroyArray(_vel_M);

    // close the Matlab Engine
    engClose(_ep);

}



