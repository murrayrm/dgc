#ifndef MATLABDISPLAY_HH
#define MATLABDISPLAY_HH

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "MatlabDisplayDatum.hh"

#include "engine.h"

using namespace std;
using namespace boost;


class MatlabDisplay : public DGC_MODULE 
{
    public:

        // and change the constructors
        MatlabDisplay();
        ~MatlabDisplay();

        // The states in the state machine.
        // Uncomment these if you wish to define these functions.

        void Init();
        void Active();
        //  void Standby();
        void Shutdown();
        //  void Restart();

        // The message handlers. 
        //  void InMailHandler(Mail & ml);
        //  Mail QueryMailHandler(Mail & ml);

        // the status function.
        //string Status();


        // Any functions which run separate threads

        // Method protocols
        int  InitMatlabDisplay();
        void MatlabDisplayUpdateVotes();
        void MatlabDisplayShutdown();

        void UpdateState(); // grabs from VState and stores in datum.

    private:

        // Hook to Matlab Engine
        Engine *_ep;

        // ADD CODE HERE: Define variables to interface with Matlab

        mxArray *_votes_M; // these are the matlab variables where we will store plotting data
        mxArray *_vel_M;

        double *_votes_C; // these are the C counterparts; they point inside the mxArray to where
        double *_vel_C;   // the array of doubles is actually stored
                          // (mxArray has additional wrapper info, such as variable type (double, complex, uint8,
                          //  as well as the current dimensions of the variable)

        // ADD CODE HERE: I suppose this is the variable to be used for message passing ???
        //and a datum named d.
        MatlabDisplayDatum _d;

        // GET RID OF this variable in the code when you don't want to produce the silly fake data anymore
        int _counter; // counter variable to produce fake data (since we haven't programmed message passing yet)

};


// enumerate all module messages that could be received
namespace MatlabDisplayMessages
{
    enum {
        // we are not yet receiving messages.
        // A place holder
        NumMessages
    };
};

#endif
