#include "MatlabDisplay.hh"

using namespace std;

int main(int argc, char **argv) {

  ////////////////////////////
  // do some argument checking

  // Done doing argument checking.
  ////////////////////////////////

  // Start the MTA By registering a module
  // and then starting the kernel

  Register(shared_ptr<DGC_MODULE>(new MatlabDisplay));
  StartKernel();

  return 0;
}
