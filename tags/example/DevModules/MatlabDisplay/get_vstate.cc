/*
** get_vstate.cc
**
**  
** 
**
*/

#include "MatlabDisplay.hh" // standard includes, arbiter, datum, ...

void MatlabDisplay::UpdateState()
{ 
  Mail msg = NewQueryMessage( MyAddress(), MODULES::VState, VStateMessages::GetState);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> _d.SS; // download the new state
  }

} // end WaypointNav::UpdateState() 
