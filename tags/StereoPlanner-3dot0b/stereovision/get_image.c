/**************************************************************************
**       Title: grab a stereo pair using libdc1394
**
**    Get one gray stereo image pair using libdc1394 and store it as portable gray map
**    (pgm). Based on 'samplegrab' from Chris Urmson 
**
**************************************************************************/

/*********************************************
 * Original code heavily modified by S. Waydo
 * to get images from multiple
 * cameras, identify cameras, etc.
 *********************************************/

#include <stdio.h>
#include <libraw1394/raw1394.h>
#include <libdc1394/dc1394_control.h>
#include <stdlib.h>
#include <getopt.h>

#define LEFT_FILE_NAME    "LeftImage.pgm"
#define RIGHT_FILE_NAME   "RightImage.pgm"
#define UNKNOWN_FILE_NAME "UnknownImage.pgm"

#define MAX_CAMERAS  2
#define LEFT_CAM     0
#define RIGHT_CAM    1
#define UNKNOWN_CAM -1

/*-----------------------------------------------------------------------
 *  set up cameras
 *-----------------------------------------------------------------------*/
int init_cameras( raw1394handle_t * handle_ptr,
                  int * numCameras_ptr,
                  dc1394_camerainfo info[],
                  quadlet_t cam_ids[],
                  int node2cam[],
                  dc1394_cameracapture cameras[] );

/*-----------------------------------------------------------------------
 *  have the cameras start sending us data
 *-----------------------------------------------------------------------*/
int start_cameras( raw1394handle_t handle,
                   int numCameras,
                   dc1394_cameracapture cameras[] );

/*-----------------------------------------------------------------------
 *  Stop data transmission
 *-----------------------------------------------------------------------*/
int stop_cameras( raw1394handle_t handle,
		  int numCameras,
		  dc1394_cameracapture cameras[] );

/*-----------------------------------------------------------------------
 *  Shut down cameras
 *-----------------------------------------------------------------------*/
int close_cameras( raw1394handle_t handle,
		   int numCameras,
		   dc1394_cameracapture cameras[] );

int main(int argc, char *argv[]) 
{
  FILE* imagefile;
  dc1394_cameracapture cameras[MAX_CAMERAS]; // array of cameras so we can do multi-grab
  dc1394_camerainfo info[MAX_CAMERAS];
  quadlet_t cam_ids[MAX_CAMERAS];
  int numCameras;
  raw1394handle_t handle;
  int i, j;

  while(1) {
    int this_option_optind = optind ? optind : 1;
    int option_index=0;
    static struct option long_options[] = {
      {"name", 1, 0, NAME_OPT},
      {"sim",  0, &SIM, 1},
      {0, 0, 0, 0}
    };
  }
       

  // lookup of node to camera number
  // defined LEFT_CAM to be 0, RIGHT_CAM to be 1 for now
  int node2cam[MAX_CAMERAS];

  // initialize cameras
  if ( init_cameras( &handle, &numCameras, info, cam_ids, node2cam, cameras ) < 0 ) {
    fprintf( stderr, "Error initializing cameras\n" );
    exit(1);
  }

  // start data transmission
  if ( start_cameras( handle, numCameras, cameras ) < 0 ) {
    fprintf( stderr, "Error starting cameras\n" );
    exit(1);
  }

  /*-----------------------------------------------------------------------
   *  capture one frame from each camera
   *-----------------------------------------------------------------------*/
  // do a multi capture - this way it should be synchronized automagically
  if ( dc1394_multi_capture(handle,cameras,numCameras)!=DC1394_SUCCESS)
    {
      fprintf( stderr, "Unable to capture frames\n" );
      close_cameras( handle, numCameras, cameras );
      exit(1);
    }

  // stop data transmission 
  stop_cameras( handle, numCameras, cameras );

  /*-----------------------------------------------------------------------
   *  save images under appropriate filenames
   *-----------------------------------------------------------------------*/
  for ( i = 0; i < numCameras; i++ ) {
    switch(node2cam[i])
      {
      case LEFT_CAM :
	imagefile=fopen(LEFT_FILE_NAME, "w");
	if( imagefile == NULL)
	  {
	    fprintf( stderr, "Can't create '" LEFT_FILE_NAME "'");
	    continue;
	  }
	break;
      case RIGHT_CAM :
	imagefile=fopen(RIGHT_FILE_NAME, "w");
	if( imagefile == NULL)
	  {
	    fprintf( stderr, "Can't create '" RIGHT_FILE_NAME "'");
	    continue;
	  }
	break;
      default : 
	imagefile=fopen(UNKNOWN_FILE_NAME, "w");
	if( imagefile == NULL)
	  {
	    fprintf( stderr, "Can't create '" UNKNOWN_FILE_NAME "'");
	    continue;
	  }
	break;
      }
      fprintf(imagefile,"P5\n%u %u 255\n", cameras[i].frame_width,
              cameras[i].frame_height );
      fwrite((const char *)cameras[i].capture_buffer, 1, 
             cameras[i].frame_height*cameras[i].frame_width, imagefile);
      fclose(imagefile);
  }

  // clean up
  close_cameras( handle, numCameras, cameras );
  return 0;
}


/*-----------------------------------------------------------------------
 *  HELPER FUNCTIONS
 *-----------------------------------------------------------------------*/

/*-----------------------------------------------------------------------
 *  set up cameras
 *-----------------------------------------------------------------------*/
int init_cameras( raw1394handle_t * handle_ptr,
                  int * numCameras_ptr,
                  dc1394_camerainfo info[],
                  quadlet_t cam_ids[],
                  int node2cam[],
                  dc1394_cameracapture cameras[] ) {

  int numNodes;
  int i, j;
  char idstr[9];

  nodeid_t * camera_nodes;

  /*-----------------------------------------------------------------------
   *  Open ohci and asign handle to it
   *-----------------------------------------------------------------------*/
  *handle_ptr = dc1394_create_handle(0);
  if (*handle_ptr==NULL)
  {
    fprintf( stderr, "Unable to aquire a raw1394 handle\n\n"
             "Please check \n"
	     "  - if the kernel modules `ieee1394',`raw1394' and `ohci1394' are loaded \n"
	     "  - if you have read/write access to /dev/raw1394\n\n");
    return(-1);
    }

  /*-----------------------------------------------------------------------
   *  get the camera nodes and describe them as we find them
   *-----------------------------------------------------------------------*/
  numNodes = raw1394_get_nodecount(*handle_ptr);
  camera_nodes = dc1394_get_camera_nodes(*handle_ptr, numCameras_ptr, 1);
  for ( i = 0; i < *numCameras_ptr; i++ ) {
    dc1394_get_camera_info(*handle_ptr, camera_nodes[i], &info[i]);
    cam_ids[i] = info[i].euid_64 & 0xffffffff;
    sprintf( idstr, "0x%08X",cam_ids[i] );
   if ( strcmp( idstr, "0x002F51E9" ) == 0 ) {
      node2cam[i] = LEFT_CAM;
      printf( "Left!\n" );
    } else if ( strcmp( idstr, "0x002F51EA" ) == 0 ) {
      node2cam[i] = RIGHT_CAM;
      printf( "Right!\n" );
    } else {
      node2cam[i] = UNKNOWN_CAM;
      printf( "Unknown camera found!\n" );
    }
  }


  fflush(stdout);
  if (*numCameras_ptr<1)
  {
    fprintf( stderr, "no cameras found :(\n");
    dc1394_destroy_handle(*handle_ptr);
    return(-1);
  }

 /*-----------------------------------------------------------------------
   *  to prevent the iso-transfer bug from raw1394 system, check if
   *  camera is highest node. For details see 
   *  http://linux1394.sourceforge.net/faq.html#DCbusmgmt
   *  and
   *  http://sourceforge.net/tracker/index.php?func=detail&aid=435107&group_id=8157&atid=108157
   *-----------------------------------------------------------------------*/
  // TODO: Figure out what this bug means and if we need to be worried about it!!!
  //  if( camera_nodes[0] == numNodes-1 )
  /* if( camera_nodes[numCameras-1] == numNodes-1 )
  {
        fprintf( stderr, "\n"
             "Sorry, your camera is the highest numbered node\n"
             "of the bus, and has therefore become the root node.\n"
             "The root node is responsible for maintaining \n"
             "the timing of isochronous transactions on the IEEE \n"
             "1394 bus.  However, if the root node is not cycle master \n"
             "capable (it doesn't have to be), then isochronous \n"
             "transactions will not work.  The host controller card is \n"
             "cycle master capable, however, most cameras are not.\n"
             "\n"
             "The quick solution is to add the parameter \n"
             "attempt_root=1 when loading the OHCI driver as a \n"
             "module.  So please do (as root):\n"
             "\n"
             "   rmmod ohci1394\n"
             "   insmod ohci1394 attempt_root=1\n"
             "\n"
             "for more information see the FAQ at \n"
             "http://linux1394.sourceforge.net/faq.html#DCbusmgmt\n"
             "\n"); 
    dc1394_destroy_handle(handle);
    return(-1);
  }*/

  /*-----------------------------------------------------------------------
   *  setup capture
   *-----------------------------------------------------------------------*/
  for ( i = 0; i < *numCameras_ptr; i++ ) {
    if ( dc1394_setup_capture(*handle_ptr, camera_nodes[i],
                              i, // channel
                              FORMAT_SVGA_NONCOMPRESSED_1,
                              MODE_1024x768_MONO,
                              SPEED_400,
                              FRAMERATE_15,
                              &cameras[i])!=DC1394_SUCCESS) {
    fprintf( stderr,"unable to setup camera-\n"
             "check line %d of %s to make sure\n"
             "that the video mode,framerate and format are\n"
             "supported by your camera\n",
             __LINE__,__FILE__);
    close_cameras( *handle_ptr, *numCameras_ptr, cameras );
    return(-1);
    }
  }

  // set trigger mode - I'm not exactly sure what this does or if it is
  // strictly necessary
  for ( i = 0; i < *numCameras_ptr; i++ ) {
    if( dc1394_set_trigger_mode(*handle_ptr, cameras[i].node, TRIGGER_MODE_0)
        != DC1394_SUCCESS)
    {
      fprintf( stderr, "unable to set camera %d trigger mode\n", i);
    }
  }

  return 0;

}

/*-----------------------------------------------------------------------
 *  have the cameras start sending us data
 *-----------------------------------------------------------------------*/
int start_cameras( raw1394handle_t handle,
                   int numCameras,
                   dc1394_cameracapture cameras[] ) {

  int i, j;

  for ( i = 0; i < numCameras; i++ ) {
    if ( dc1394_start_iso_transmission(handle,cameras[i].node)
	 !=DC1394_SUCCESS)
      {
	fprintf( stderr, "Unable to start camera %d iso transmission\n", i);
        close_cameras( handle, numCameras, cameras );
	return(-1);
      }
  }

  return 0;
}


/*-----------------------------------------------------------------------
 *  Stop data transmission
 *-----------------------------------------------------------------------*/
int stop_cameras( raw1394handle_t handle,
		  int numCameras,
		  dc1394_cameracapture cameras[] ) {

  int i;

  for ( i = 0; i < numCameras; i++ ) {
    if (dc1394_stop_iso_transmission(handle,cameras[i].node)!=DC1394_SUCCESS)
      {
	fprintf( stderr, "Couldn't stop camera%d?\n", i);
      }
  }

  return 0;

}

/*-----------------------------------------------------------------------
 *  Shut down cameras
 *-----------------------------------------------------------------------*/
int close_cameras( raw1394handle_t handle,
		   int numCameras,
		   dc1394_cameracapture cameras[] ) {

  int i;

  for ( i = 0; i < numCameras; i++ ) dc1394_release_camera(handle,&cameras[i]);
  dc1394_destroy_handle(handle);

  return 0;

}
