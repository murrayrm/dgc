stereovision CVS module

This module contains the stereovision utilities to be used by StereoPlanner.

IF STEREOVISION CODE DOES NOT COMPILE OR RUN, READ BELOW:
If stereovision code does not compile, make sure that you have the proper libraries (IEEE 1394 and OpenCV) installed on your system properly - for more information on how to install them, read /doc/linux/firewire-howto
Also, make sure you are linking to them properly, and your makefile is correct.

If stereovision code does not run and you get the following error:
./SomeStereoProgram: error while loading shared libraries: libsvs.so: cannot
open shared object file: No such file or directory
then you need to set the LD_LIBRARY_PATH environment variable in your shell to the location of team/lib.
Depending on your shell, that means executing one of the following commands at the prompt
$> setenv LD_LIBRARY_PATH "/team/lib"
or
$> export LD_LIBRARY_PATH="/team/lib"
(The directory path you set may be different depending on where you keep your copy of the code locally.)

getpair.h - grabs a pair of synced images, see below

log_image.c - logs images in various naming schemes: run log_image -help for more info


HOW TO TAKE PICTURES WITH THE 1394 CAMERAS

How to take synced picture with the 1394 pictures:
getpair.h provides the interface to take pictures from the firewire cameras.  It includes getpair.c - the whole interface is written in C for speed, but may be adapted to C++ in some future version for elegance and ease of use.  Make sure that the raw1394 module is included (as root run "modprobe ra1394").  First include "getpair.h".  To scan the firewire bus and display information on any found cameras, call cameras_info().  This will return diagnostic information about any cameras as well as an identifying string.  This ID string is unique to each camera and will look something like "0x002F51E9".  Once you have figured out which camera is which (by unplugging cables and running cameras_info(), modify getpair.h to include the correct strings:

#define LEFT_CAMERA_STRING "0x002F51E9"
#define RIGHT_CAMERA_STRING "0x002F51EA"

Now you're ready to take pictures.  Create the following variables:

raw1394handle_t handle;
int note2cam[2];
dc1394_cameracapture cameras[2];

Start the cameras:

cameras_start(&handle,node2cam,cameras);

This sets up the cameras and gets them sending data.

Take two synced images:

cameras_snap(handle,cameras);

Now the images will be in cameras[i].capture_buffer.

To tell left from right, use node2cam[]- for example if node2cam[0]==LEFT_CAM, then left camera is camera[0].  Again, this clunky interface will be eliminated if I make another version.  camera[i].capture_buffer can be fed right into cv functions or SVS:

  bool ret = videoObject->Load(cameras[0].frame_width, cameras[0].frame_height, 
                               (unsigned char *) camera[0].capture_buffer, 
                               (unsigned char *) cameras[1].capture_buffer, 
                               NULL, NULL,
                               FALSE, FALSE);
                               
Running cameras_snap will snap another shot.  When you're done, please shutdown the cameras with cameras_stop(handle,cameras).  cameras_start, cameras_snap, and cameras_stop return -1 if there is an error and you should not proceed and 0 if everything is cool, for example:

  if (  cameras_start(&handle,node2cam,cameras) < 0 ) {
    fprintf( stderr, "Error starting cameras\n" );
    exit(1);
  }

Please send any question to gunnar@its.
