function original = plot_fender(varargin);
% 
% function plot_fender( varargin )
%
% Changes: 
%   03/04/04, Alan Somers, created  
%
% Function:
%   This function reads a matlab-formatted fender point list and displays them 
%   The optional arguments are for a starting offset
%
% Usage example:
%   plot_fender('fenderDDF-matlab.dat')
%   plot_fender('fenderDDF-matlab.dat', northing, easting)
% The northing and easting  should correspond to waypoint 0 of the RDDF.

%Handle arguments
if (nargin < 1)
    error('Need at least one argument (filename)');
end
if (nargin == 2)
    error('Need exactly 0 or 2 offset arguments');
end
if (nargin > 3)
    error('Too many arguments');
end

[northings, eastings]=textread(varargin{1});
if (nargin == 3)
    northings = northings - varargin{2};
    eastings = eastings - varargin{3};
end

plot(eastings,northings,'.');
