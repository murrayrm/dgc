% switch colors in plot if mode changes

% color codes:
% 1,2 change in both ext mode  

color = 1;

figure;
hold on
for i=2:length(gpsmode)
    if gpsmode(i) ~= gpsmode(i - 1)
        if gps_ext_mode(i) ~= gps_ext_mode(i - 1)
            if color == 1
                color = 2;
            else
                color = 1;
            end
        else
            if color == 3 
                color = 4;
            else 
                color = 3;
            end
        end
    elseif gps_ext_mode(i) ~= gps_ext_mode(i - 1)
        if color == 1
            color = 2;
        else
            color = 1;
        end
    end
    if color == 1
        plot(gpseasting(i), gpsnorthing(i), '.b')
    elseif color == 2
        plot(gpseasting(i), gpsnorthing(i), '.k')
    elseif color == 3
        plot(gpseasting(i), gpsnorthing(i), '.r')
    else
        plot(gpseasting(i), gpsnorthing(i), '.g')
    end
end
    