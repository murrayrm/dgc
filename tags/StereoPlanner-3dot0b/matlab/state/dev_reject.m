function result = dev_reject(data) 
% moving average/standard dev noise rejection
% calculate the avg. of the difference between the last n samples
% use that to generate the std. dev, then look at the n+1 sample
% and if that's outside a certain range then tag it as a reject
% data argument is a nx2 vector (northing easting) 

buff = [];
n = 5;
result = [];
buff_ptr = 1;
sigMult = 4; % how far out is the bound before we throw it out?

% go down each row of data and process
for i = 1:length(data(:,1))
    if i == 1
      prev = data(i,:);
      avg = 0;
      var = 0;
      dev = 0;
      result = 1;
  else
      if length(buff) < n
          % get distance from this point to last 
          dn = data(i, 1) - prev(1);
          de = data(i, 2) - prev(2);
          delta = sqrt(dn^2 + de^2);
          buff = [buff delta];
          
          % now calculate avgs and std devs
          avg = MEAN(buff);
          var = MEAN((buff - avg).^2);
          dev = sqrt(var);
          
          % flag as okay
          result = [result 1];
          
          % set prev
          prev = data(i,:);  
      else
          % get distance from this point to last 
          dn = data(i, 1) - prev(1);
          de = data(i, 2) - prev(2);
          delta = sqrt(dn^2 + de^2);
          
          prev = data(i,:);
          
          % check if outside bound
          if abs(delta - avg) >= dev*sigMult
              result = [result 0]; % throw it out
              buff(buff_ptr) = delta;
              avg = MEAN(buff);
              var = MEAN((buff - avg).^2);
              dev = sqrt(var);
              
              % increment the buff pointer 
              buff_ptr = mod(buff_ptr + 1, n);
              if buff_ptr == 0
                  buff_ptr = 1;
           end
          else
              % otherwise accept this and put into buff
              result = [result 1];
              buff(buff_ptr) = delta;
              avg = MEAN(buff);
              var = MEAN((buff - avg).^2);
              dev = sqrt(var);
              
              % increment the buff pointer 
              buff_ptr = mod(buff_ptr + 1, n);
              if buff_ptr == 0
                  buff_ptr = 1;
              end
          end
      end
  end
end
result = result';
               