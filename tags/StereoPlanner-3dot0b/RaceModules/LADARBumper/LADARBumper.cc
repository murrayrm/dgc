#include "LADARBumper.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include <unistd.h>
#include <iostream>

int QUIT_PRESSED = 0;

extern int SEND_VOTES;
extern unsigned long SLEEP_TIME;
extern int SPARROW_DISP;
extern int NOSTATE;

LADARBumper::LADARBumper() 
  : DGC_MODULE(MODULES::LADARBumper, "LADAR Bumper", 0) {

}

LADARBumper::~LADARBumper() {
}


void LADARBumper::Init() {
  // call the parent init first
  DGC_MODULE::Init();

  InitLadar();
  cout << "Module Init Finished" << endl;

  cout << "About to start Sparrow display" << endl;
  if(SPARROW_DISP) {
    cout << "Starting Sparrow display" << endl;
    RunMethodInNewThread<LADARBumper>( this, &LADARBumper::SparrowDisplayLoop);
    RunMethodInNewThread<LADARBumper>( this, &LADARBumper::UpdateSparrowVariablesLoop);
  }
}

void LADARBumper::Active() {

  cout << "Starting Active Function" << endl;
  while( ContinueInState() && !QUIT_PRESSED) {
    // update the State Struct
    if(!NOSTATE) {
      UpdateState();
      if(d.SS.Northing == 0) {
        cout << endl << "UNABLE TO COMMUNICATE WITH VSTATE" << endl;
	cout << "NORTHING IS ZERO, PLEASE CHECK OR" << endl;
        cout << "RUN WITH -nostate SWITCH IF THIS IS OK" << endl << endl;
        exit(-1);
      }
    }
    // Update the votes and send them to the arbiter
    LadarUpdateVotes();

    if(SEND_VOTES) {
      Mail m = NewOutMessage(MyAddress(), 
			     MODULES::Arbiter, 
			     ArbiterMessages::Update);
      // Send the arbiter our "ID" and the vote struct.  The weights
      // are taken care of by the arbiter.
      int myID = ArbiterInput::LADAR;
      m << myID << d.ladar;
      SendMail(m);
    }

    if(SPARROW_DISP) UpdateSparrowVariablesLoop();

    // and sleep for a bit
    usleep_for(SLEEP_TIME);
  }

  cout << "Finished Active State" << endl;

}


void LADARBumper::Shutdown() {
  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;
  LadarShutdown();

}



