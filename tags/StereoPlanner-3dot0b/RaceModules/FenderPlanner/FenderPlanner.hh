#ifndef FENDERPLANNER_HH
#define FENDERPLANNER_HH


#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "FenderPlannerDatum.hh"
#include "fenderDDF.hh"

using namespace std;
using namespace boost;

class FenderPlanner : public DGC_MODULE {
public:
  // and change the constructors
  FenderPlanner();
  ~FenderPlanner();

  // The states in the state machine.
  void Init();
  void Active();
  void Shutdown();

  // Method protocols
  int  InitFender();
  void FenderUpdateVotes();
  void FenderShutdown();

  void UpdateState(); // grabs from VState and stores in datum.

private:
  // and a datum named d.
  FenderPlannerDatum d;
};


// enumerate all module messages that could be received
namespace FenderPlannerMessages {
  enum {
    // we are not yet receiving messages.
    // We can set up the sending of a message by defining it here
    //    Get_genMapStruct,  // MatlabDisplay queries for this 
    NumMessages        // A place holder
  };
};


#endif
