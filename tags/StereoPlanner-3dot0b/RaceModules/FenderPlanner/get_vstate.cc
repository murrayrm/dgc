/*
** get_vstate.cc
**
**  
** 
**
*/

#include "FenderPlanner.hh" // standard includes, arbiter, datum, ...

void FenderPlanner::UpdateState()
{ 
  Mail msg = NewQueryMessage( MyAddress(), MODULES::VState, VStateMessages::GetState);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> d.SS; // download the new state
  }

} // end FenderPlanner::UpdateState() 
