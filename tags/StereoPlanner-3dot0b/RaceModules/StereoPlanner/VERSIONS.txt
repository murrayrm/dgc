StereoPlanner Version Info:
StereoPlanner-1c - ran during the successful QID run on Wednesday, March 10, 2004
StereoPlanner-2.0a - Never tested besides tests in the garage while the vehicle is still.  Used faster velocities and longer stopping distances, but seemed to give much worse votes.
StereoPlanner-2.0b - Uses smaller stopping distances than 2.0a, but the same velocities.  Tested in the shop while still, gave good-looking votes
StereoPlanner-2.0c - Only change between 2.0b and 2.0c is that c doesn't send votes when pointed toward the sun
StereoPlanner-2.0d - Only change between 2.0c and 2.0d is that d uses faster speeds thanks to Mike - but still sometimes hits stuff
StereoPlanner-2.0e - Only change between 2.0d and 2.0e is that e uses a 4m stopping distance instead of 2.5
StereoPlanner-2.0f - Only change between 2.0e and 2.0f is that f uses velocity when determining stopping distance
StereoPlanner-3.0a - Only change between 2.0f and 3.0a is that 3.0a rejects points within 20m for long range cameras