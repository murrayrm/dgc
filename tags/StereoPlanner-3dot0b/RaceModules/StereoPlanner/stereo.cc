#include <sys/time.h>
#include <iomanip>
#include <algorithm>  // for min
#include <stdio.h>

#include "StereoPlanner.hh"
#include "stereo.hh"

//svMap Default Parameters
#define DEFAULT_SVMAP_NUM_ROWS 250
#define DEFAULT_SVMAP_NUM_COLS 250
#define DEFAULT_SVMAP_ROW_RES  0.2
#define DEFAULT_SVMAP_COL_RES  0.2

#define LONG_DEFAULT_SVMAP_NUM_ROWS 250
#define LONG_DEFAULT_SVMAP_NUM_COLS 250
#define LONG_DEFAULT_SVMAP_ROW_RES  0.4
#define LONG_DEFAULT_SVMAP_COL_RES  0.4

#define X_LIMIT DEFAULT_SVMAP_NUM_ROWS*DEFAULT_SVMAP_ROW_RES*0.5
#define Y_LIMIT DEFAULT_SVMAP_NUM_COLS*DEFAULT_SVMAP_COL_RES*0.5
#define Z_LIMIT 1000

#define LONG_X_LIMIT LONG_DEFAULT_SVMAP_NUM_ROWS*DEFAULT_SVMAP_ROW_RES*0.5
#define LONG_Y_LIMIT LONG_DEFAULT_SVMAP_NUM_COLS*DEFAULT_SVMAP_COL_RES*0.5
#define LONG_Z_LIMIT 1000

#define DEFAULT_SVSCAL_FILE_NAME    "../../calibration/SVSCal.ini.short"
#define DEFAULT_SVSPARAMS_FILE_NAME "../../calibration/SVSParams.ini.short"
#define DEFAULT_CAMCAL_FILE_NAME    "../../calibration/CamCal.ini.short"

#define LONG_DEFAULT_SVSCAL_FILE_NAME    "../../calibration/SVSCal.ini.long"
#define LONG_DEFAULT_SVSPARAMS_FILE_NAME "../../calibration/SVSParams.ini.long"
#define LONG_DEFAULT_CAMCAL_FILE_NAME    "../../calibration/CamCal.ini.long"

#define OVERPASS_HEIGHT -3.3

#define LR_MIN_DISTANCE 20.0

using namespace std;

extern int QUIT;
extern int PRINT_VOTES; // get the print_votes flag from the main function
extern int debug, verbose, current_frame, log_images, which_pair, sim, print_time;

extern char left_file_name_prefix[200], right_file_name_prefix[200];
extern char state_file_name[256];
extern char vote_file_name[256];
extern char debug_file_name_prefix[200];

double maxx, maxy, maxz, minx, miny, minz;

FILE *pointFile, *utmPointFile, *statelog, *votelog;

double msMisc1=0, msCapture=0, msMisc2=0, msProcess=0, msFill=0, msEvaluate=0, msMisc3=0, msVotes=0, msTotal=0;

int StereoPlanner::InitStereo() {
  //Initialize the cameras
  if(verbose) printf("Initializing cameras...\n");
  if(which_pair == SHORT_RANGE) {
    if(svsSW_OK != d.svsObject.init(which_pair, DEFAULT_SVSCAL_FILE_NAME, DEFAULT_SVSPARAMS_FILE_NAME, DEFAULT_CAMCAL_FILE_NAME, TRUE, verbose)) {
      printf("Error initializing svsStereoWrapper object at line %d in %s!  Quitting...\n", __LINE__, __FILE__);
      return -1;
    }
  } else if(which_pair == LONG_RANGE) {
    if(svsSW_OK != d.svsObject.init(which_pair, LONG_DEFAULT_SVSCAL_FILE_NAME, LONG_DEFAULT_SVSPARAMS_FILE_NAME, LONG_DEFAULT_CAMCAL_FILE_NAME, TRUE, verbose)) {
      printf("Error initializing svsStereoWrapper object at line %d in %s!  Quitting...\n", __LINE__, __FILE__);
      return -1;
    }
  } else {
    printf("Not set to use long or short at line %d in %s!  Quitting...\n", __LINE__, __FILE__);
    return -1;
  }

  //Initialize the map
  if(verbose) printf("Initializing genMap...\n");
  if(which_pair == SHORT_RANGE) {
    d.svMap.initMap(DEFAULT_SVMAP_NUM_ROWS, DEFAULT_SVMAP_NUM_COLS, DEFAULT_SVMAP_ROW_RES, DEFAULT_SVMAP_COL_RES, FALSE); 
  } else if(which_pair == LONG_RANGE) {
    d.svMap.initMap(LONG_DEFAULT_SVMAP_NUM_ROWS, LONG_DEFAULT_SVMAP_NUM_COLS, LONG_DEFAULT_SVMAP_ROW_RES, LONG_DEFAULT_SVMAP_COL_RES, FALSE); 
  }
  d.svMap.initPathEvaluationCriteria(false,false,false, false,false, true, true);
  //d.svMap.initPathEvaluationCriteria(false,false,false, true,false, true, false, false);
  d.svMap.initThresholds(70, 1, TIRE_OFF_THRESHOLD, VECTOR_PROD_THRESHOLD, GROW_VEHICLE_LENGTH, 1.2);

// FIXME: THIS is needed to work with the new genMap...
//   Be sure to create a new version of StereoPlanner and indicate that
//   it is using the new velocity controls.

  if(which_pair == SHORT_RANGE) {
    d.svMap.initVelocityThresholds(SR_MAX_SPEED,SR_MIN_SPEED,
				   SR_DIST_TO_OBSTACLE_ON_ARC_HALT,
				   SR_DIST_TO_OBSTACLE_ANYWHERE_HALT,
				   SR_DIST_TO_OBSTACLE_ON_ARC_MAX_SPEED,
				   SR_DIST_TO_OBSTACLE_ANYWHERE_MAX_SPEED);
  } else if(which_pair == LONG_RANGE) {
    d.svMap.initVelocityThresholds(LR_MAX_SPEED,LR_MIN_SPEED,
				   LR_DIST_TO_OBSTACLE_ON_ARC_HALT,
				   LR_DIST_TO_OBSTACLE_ANYWHERE_HALT,
				   LR_DIST_TO_OBSTACLE_ON_ARC_MAX_SPEED,
				   LR_DIST_TO_OBSTACLE_ANYWHERE_MAX_SPEED);
  }    


  //Initialize state log, if needed
  statelog = NULL;
  if(log_images) {
    if(verbose) printf("Initializing state log...\n");
    statelog=fopen(state_file_name, "w");
    if( statelog == NULL)
      {
	fprintf( stderr, "Can't create '%s'", state_file_name);
      } else {
	// print data format information in a header at the top of the state file
	fprintf(statelog, "% Time[sec] | Easting[m] | Northing[m] | Altitude[m]");
	fprintf(statelog, " | Vel_E[m/s] | Vel_N[m/s] | Vel_U[m/s]" );
	fprintf(statelog, " | Speed[m/s] | Accel[m/s/s]" );
	fprintf(statelog, " | Pitch[rad] | Roll[rad] | Yaw[rad]" );
	fprintf(statelog, " | PitchRate[rad/s] | RollRate[rad/s] | YawRate[rad/s]");
	fprintf(statelog, "\n");
	fclose(statelog);
      }
  }

  votelog = NULL;
  if(debug) {
    if(verbose) printf("Initializing vote log...\n");
    votelog = fopen(vote_file_name, "w");
    if(votelog == NULL) {
      fprintf(stderr, "Can't create '%s'", vote_file_name);
    } else {
      fprintf(votelog, "% ");
      fprintf(votelog, "Arc[rad]");
      for(int i=0; i < NUMARCS; i++) {
	fprintf(votelog, " | %lf", GetPhi(i));
      }
      fprintf(votelog, "\n");
      fclose(votelog);
    }
  }

  if(verbose) printf("Initialization complete\n");
  return TRUE;
}

void StereoPlanner::StereoUpdateVotes() {
  Timeval start, end, result, startTotal, endTotal;
  startTotal = TVNow();
  start = TVNow();

  int numPoints;
  double steerAngle;
  XYZcoord UTMPoint, Point;
  char left_file_name[256], right_file_name[256];
  char pts_file_name[256];
  char utmpts_file_name[256];
  char map_file_name[256];
  char size_file_name[256];
  char img_file_name[256];
  char disp_file_name[256];
  char single_map_file_name[256];
  double maxx, maxy, maxz, minx, miny, minz;
  double xUpLimit, yUpLimit, zUpLimit;
  double xLowLimit, yLowLimit, zLowLimit;

  vector< pair<double, double> > voteList;
  vector<double> steeringAngleList;
  steeringAngleList.reserve(NUMARCS);
  voteList.reserve(NUMARCS);
    
  current_frame++;

  end = TVNow();
  result = end - start;
  msMisc1+=(((double)result.sec())/1000000 + (double)result.usec())/1000;
  start = TVNow();

  //Grab the images
  if(sim) {
    sprintf(left_file_name, "%s%.4d-L.bmp", left_file_name_prefix, current_frame);
    sprintf(right_file_name, "%s%.4d-R.bmp", right_file_name_prefix, current_frame);
    if(svsSW_OK != d.svsObject.grabFilePair(left_file_name, right_file_name, state_file_name, current_frame)) {
      printf("Error while grabbing images from file at line %d in %s!  Quitting...\n", __LINE__, __FILE__);
      QUIT = 1;
      return;
    } else if(verbose) {
      printf("Grabbed pair %d from file OK\n", current_frame);
    }    
  } else {
    Lock mylock(d.CameraLock);
    //StereoCaptureThread();
    if(svsSW_OK != d.svsObject.loadCameraPair(d.tempState)) {
      //if(svsSW_OK != d.svsObject.grabCameraPair(d.tempState)) {
      printf("Error while grabbing images from cameras at line %d in %s!  Quitting...\n", __LINE__, __FILE__);
      return;
    } else if(verbose) {
      printf("Grabbed pair %d OK\n", current_frame);
    }
  }
  d.SS = d.svsObject.SS;

  end = TVNow();
  result = end - start;
  msCapture+=(((double)result.sec())/1000000 + (double)result.usec())/1000;
  start = TVNow();

  if(log_images) {
    sprintf(left_file_name, "%s%.4d-L.bmp", left_file_name_prefix, current_frame);
    sprintf(right_file_name, "%s%.4d-R.bmp", right_file_name_prefix, current_frame);
    if(svsSW_OK != d.svsObject.saveStereoPair(left_file_name, right_file_name)) {
      printf("Error while saving pair (%s, %s)\n", left_file_name, right_file_name);
    } else {
      //Save the state log
      statelog=fopen(state_file_name, "a");
      if(statelog != NULL) {
	// Print state      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 
	fprintf(statelog, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",
		(double)(d.SS.Timestamp.sec()+d.SS.Timestamp.usec()/1000000.0),
		d.SS.Easting, d.SS.Northing, d.SS.Altitude, 
		d.SS.Vel_E, d.SS.Vel_N, d.SS.Vel_U, 
		d.SS.Speed, d.SS.Accel, 
		d.SS.Pitch, d.SS.Roll, d.SS.Yaw, 
		d.SS.PitchRate, d.SS.RollRate, d.SS.YawRate );
	fclose(statelog);
	if(verbose) printf("Wrote state\n");
      } else {
	printf("Couldn't open %s to write state\n", state_file_name);
      }
    }
  }

  if(debug) {
    sprintf(map_file_name, "%s%.4dMap", debug_file_name_prefix, current_frame);
    sprintf(size_file_name, "%s%.4dSize", debug_file_name_prefix, current_frame);
    sprintf(single_map_file_name, "%s%.4dSingleMap", debug_file_name_prefix, current_frame);
    sprintf(img_file_name, "%s%.4dMap.pgm", debug_file_name_prefix, current_frame);
    sprintf(pts_file_name, "%s%.4dPts.mat", left_file_name_prefix, current_frame);
    sprintf(utmpts_file_name, "%s%.4dUTMPts.mat", debug_file_name_prefix, current_frame);
    sprintf(disp_file_name, "%s%.4dDisp.bmp", debug_file_name_prefix, current_frame);
  }

  end = TVNow();
  result = end - start;
  msMisc2+=(((double)result.sec())/1000000 + (double)result.usec())/1000;
  start = TVNow();

  //Process the images
  if(svsSW_OK != d.svsObject.processPair()) {
    printf("Error while processing image pair at line %d in %s!  Quitting...\n", __LINE__, __FILE__);
    return;
  } else if(verbose) {
    printf("Processed pair %d OK\n", current_frame);
  }

  end = TVNow();
  result = end - start;
  msProcess+=(((double)result.sec())/1000000 + (double)result.usec())/1000;
  start = TVNow();

  //Fill the map
  if(verbose) printf("Filling the map...");
  d.svMap.updateFrame(d.SS, TRUE, d.SS.Timestamp.dbl());
  //d.svMap.updateFrame(d.SS);
  if(debug) {
    utmPointFile = fopen(utmpts_file_name, "w"); 
    pointFile = fopen(pts_file_name, "w");
  } else {
    utmPointFile = NULL ;
    pointFile = NULL;
  }
  if(which_pair == SHORT_RANGE) {
    xUpLimit = X_LIMIT + d.SS.Northing;
    yUpLimit = Y_LIMIT + d.SS.Easting;
    zUpLimit = Z_LIMIT;
    xLowLimit = d.SS.Northing - X_LIMIT;
    yLowLimit = d.SS.Easting -Y_LIMIT;
    zLowLimit = -Z_LIMIT;
  } else if(which_pair == LONG_RANGE) {
    xUpLimit = LONG_X_LIMIT + d.SS.Northing;
    yUpLimit = LONG_Y_LIMIT + d.SS.Easting;
    zUpLimit = LONG_Z_LIMIT;
    xLowLimit = d.SS.Northing - LONG_X_LIMIT;
    yLowLimit = d.SS.Easting -LONG_Y_LIMIT;
    zLowLimit = -LONG_Z_LIMIT;
  }

  XYZcoord RelativePoint;

  //d.svMap.clearMap();
  for(int y=d.svsObject.subWindow.y; y<d.svsObject.subWindow.height; y++) {
    for(int x=d.svsObject.subWindow.x; x<d.svsObject.subWindow.width; x++) {
      if(d.svsObject.SinglePoint(x, y, &UTMPoint)) {
	//printf("%d %d %d %d\n", d.svsObject.subWindow.x, d.svsObject.subWindow.y, d.svsObject.subWindow.width, d.svsObject.subWindow.height);
	//printf("Got point (%lf, %lf, %lf)\n", UTMPoint.x, UTMPoint.y, UTMPoint.z);
	//if(UTMPoint.x < xUpLimit && UTMPoint.y < yUpLimit && UTMPoint.z < zUpLimit &&
	// UTMPoint.x > xLowLimit && UTMPoint.y > yLowLimit && UTMPoint.z > zLowLimit) {
	if(which_pair == LONG_RANGE) {
	  d.svsObject.SingleRelativePoint(x, y, &RelativePoint);
	  if(RelativePoint.x > LR_MIN_DISTANCE) {
	    d.svMap.setCellListDataUTM(UTMPoint.x, UTMPoint.y, UTMPoint.z);
	  }
	} else if(which_pair == SHORT_RANGE) {
	  d.svMap.setCellListDataUTM(UTMPoint.x, UTMPoint.y, UTMPoint.z);
	}
	  //}
      }
    }
  }
  /*
  for(int i=0; i < numPoints; i++) {
    if(d.svsObject.validPoint(i)) {
      if(debug) Point = d.svsObject.Point(i);
      UTMPoint = d.svsObject.UTMPoint(i);
      if(utmPointFile != NULL) fprintf(utmPointFile, "%lf %lf %lf\n", UTMPoint.x, UTMPoint.y, UTMPoint.z);
      if(pointFile != NULL) fprintf(pointFile, "%lf %lf %lf\n", Point.x, Point.y, Point.z);
      //printf("Point (X, Y, Z): (%lf, %lf, %lf)...", UTMPoint.x-d.SS.Northing, UTMPoint.y-d.SS.Easting, UTMPoint.z);
      //printf("Point %d: (X, Y, Z): (%lf, %lf, %lf)...", i, Point.x, Point.y, Point.z);
      if(UTMPoint.x < xUpLimit && UTMPoint.y < yUpLimit && UTMPoint.z < zUpLimit &&
	 UTMPoint.x > xLowLimit && UTMPoint.y > yLowLimit && UTMPoint.z > zLowLimit) {
	d.svMap.setCellListDataUTM(UTMPoint.x, UTMPoint.y, UTMPoint.z);
	//printf("Accepted\n");
      } else {
	//printf("Rejected\n");
      }
    }
  }
  */

  if(verbose) printf("done\n");
  if(utmPointFile != NULL) fclose(utmPointFile);
  if(pointFile != NULL) fclose(pointFile);

  end = TVNow();
  result = end - start;
  msFill+=(((double)result.sec())/1000000 + (double)result.usec())/1000;
  start = TVNow();

  //Evaluate the map
  if(verbose) printf("Evaluating map (threshold=%d)...\n", d.svsObject.numMinPoints);
  if(debug) d.svMap.saveSIZEFile(size_file_name, "StereoVision cell size data", "SVmapSize");
  d.svMap.evaluateCellsSV(d.svsObject.numMinPoints, OVERPASS_HEIGHT);

  end = TVNow();
  result = end - start;
  msEvaluate+=(((double)result.sec())/1000000 + (double)result.usec())/1000;
  start = TVNow();

  if(debug) {
    //printf("Max (X,Y,Z)=(%lf, %lf, %lf)\n", maxx, maxy, maxz);
    //printf("Min (X,Y,Z)=(%lf, %lf, %lf)\n", minx, miny, minz);
    d.svsObject.saveDispImage(disp_file_name);
    d.svMap.saveMATFile(map_file_name, "StereoVision genMap data", "SVmap");
    //    singleMap.saveMATFile(single_map_file_name, "StereoVision single genMap data", "SVmap");
    //d.svMap.saveIMGFile(img_file_name);
  }
  
  end = TVNow();
  result = end - start;
  msMisc3+=(((double)result.sec())/1000000 + (double)result.usec())/1000;
  start = TVNow();

  //Get the votes
  for(int i = 0; i < NUMARCS; i++) {
    steerAngle = GetPhi(i);
    steeringAngleList.push_back(steerAngle);
  }

  voteList = d.svMap.generateArbiterVotes(NUMARCS, steeringAngleList);

  if(verbose) printf("We have %d arcs:\n", NUMARCS);
  if(debug) { 
    votelog = fopen(vote_file_name, "a");
  } else {
    votelog = NULL ;
  } 
 for(int i=0; i < NUMARCS; i++) {
    if(verbose) printf("Vote %d (angle rad, angle deg): (good, vel): (%lf, %lf): (%lf, %lf)\n", 
		       i,
		       GetPhi(i), GetPhi(i)*180/M_PI,
		       voteList[i].second, voteList[i].first);
    d.stereo.Votes[i].Goodness = voteList[i].second;
    d.stereo.Votes[i].Velo = voteList[i].first;
    if(votelog != NULL) fprintf(votelog, "%lf ", d.stereo.Votes[i].Goodness);
  }
  if(votelog != NULL) {
    fprintf(votelog, "\n");
    fclose(votelog);
  }

  /*
  d.stereo.Votes[0].Goodness = NO_CONFIDENCE;
  d.stereo.Votes[0].Velo = 5;
  d.stereo.Votes[NUMARCS-1].Goodness = NO_CONFIDENCE;
  d.stereo.Votes[NUMARCS-1].Velo = 5;
  */

  end = TVNow();
  result = end - start;
  msVotes+=(((double)result.sec())/1000000 + (double)result.usec())/1000;

  endTotal = TVNow();
  result = endTotal-startTotal;
  msTotal+=(((double)result.sec())/1000000 + (double)result.usec())/1000;

  if(print_time) printf("M1 %.3lf, C %.3lf, M2 %.3lf, P %.3lf, F %.3lf, E %.3lf, M3: %.3lf, V %.3lf, Tot %.3lf, Sum %.3lf\n",
	 msMisc1/(double)current_frame, 
	 msCapture/(double)current_frame, 
	 msMisc2/(double)current_frame, 
	 msProcess/(double)current_frame, 
	 msFill/(double)current_frame, 
	 msEvaluate/(double)current_frame, 
	 msMisc3/(double)current_frame,
	 msVotes/(double)current_frame, 
	 msTotal/(double)current_frame,
	 (msMisc1 + msCapture + msMisc2 + msProcess + msFill + msEvaluate + msMisc3 + msVotes)/(double)current_frame);

  printf("SP-3.0b Update votes complete: %d\n", current_frame);    
} // end StereoUpdateVotes


void StereoPlanner::StereoShutdown() {


}

void StereoPlanner::StereoCaptureThread() {
  while(true) {
    {
      Lock mylock(d.CameraLock);
      d.svsObject.snapCameraPair();
    }
    UpdateState();
    usleep(100);
  }
}
