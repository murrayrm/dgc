#include "StereoPlanner.hh"

using namespace std;

int QUIT = 0;
int DISPLAY;     // this is a switch that determines whether we're using 
                 //  ncurses display mode
int PRINT_VOTES; // whether each of the voters will call printVotes
                 // functionality not yet implemented              

int debug = 0, verbose = 0, current_frame = 0, log_images = 0, no_vote = 0, which_pair = SHORT_RANGE, sim = 0, print_time = 0; 

char left_file_name_prefix[200]="", right_file_name_prefix[200]="";
char state_file_name[256];
char vote_file_name[256];
char debug_file_name_prefix[200]="";

int main(int argc, char **argv) {

  cout << "argc = " << argc << endl;
   
  DISPLAY = 0;
  PRINT_VOTES = 0;
  ////////////////////////////
  // do some argument checking
  if( argc > 1 ) { // if we have arguments
  
    for( int i=1; i < argc; i++ ) { 
      // go through each of the arguments and check...
      cout << "argv[ " << i << "] = " << argv[i] << endl;

      // if the "-sim" argument was specified
      if( strcmp(argv[i], "-sim") == 0 ) {
        sim = 1;
	i++;
	sprintf(left_file_name_prefix, "%s", argv[i]);
	sprintf(right_file_name_prefix, "%s", argv[i]);
	sprintf(state_file_name, "%s.log", argv[i]);
      }
      // if the "-pv" argument was specified
      if( strcmp(argv[i], "-pv") == 0 ) {
        cout << "!!! -pv flag acknowledged, will print votes slowly !!!" << endl << endl;
        PRINT_VOTES = 1;
      }
      // if the "-disp" argument was specified
      if( strcmp(argv[i], "-disp") == 0 ) {
        cout << "!!! -disp flag acknowledged, about to use curses display environment !!!" << endl << endl;
        DISPLAY = 1;
      }
      if(strcmp(argv[i], "-time")==0 || strcmp(argv[i], "-t")==0) {
	print_time = 1;
      }
      if(strcmp(argv[i], "-debug")==0 || strcmp(argv[i], "-d")==0) {
	debug = 1;
	i++;
	sprintf(debug_file_name_prefix, "%s", argv[i]);
	sprintf(vote_file_name, "%s.votes", argv[i]);
      }
      if(strcmp(argv[i], "-log")==0 || strcmp(argv[i], "-l")==0) {
	log_images = 1;
	i++;
	sprintf(left_file_name_prefix, "%s", argv[i]);
	sprintf(right_file_name_prefix, "%s", argv[i]);
	sprintf(state_file_name, "%s.log", argv[i]);
      }
      if(strcmp(argv[i], "-long")==0) {
	which_pair = LONG_RANGE;
      }
      if(strcmp(argv[i], "-highres")==0) {
	which_pair = HIGH_RES;
      }
      if(strcmp(argv[i], "-ld")==0 || strcmp(argv[i], "-dl")==0) {
	debug = 1;
	log_images = 1;
	i++;
	sprintf(left_file_name_prefix, "%s", argv[i]);
	sprintf(right_file_name_prefix, "%s", argv[i]);
	sprintf(state_file_name, "%s.log", argv[i]);
	sprintf(vote_file_name, "%s.votes", argv[i]);
	sprintf(debug_file_name_prefix, "%s", argv[i]);
      }
      if(strcmp(argv[i], "-verbose")==0 || strcmp(argv[i], "-v")==0) {
	verbose = 1;
      }
      if(strcmp(argv[i], "-novote")==0) {
	no_vote = 1;
      }
    }
  }

  current_frame = 0;

  // Display a message if we don't see some options set  
  if( DISPLAY == 0 ) {
      cout << endl;
      cout << "!!! No -disp flag detected, using stdout for display !!!" 
	   << endl;
      cout << "    You can Ctrl-c here if you forgot to use -disp " << endl 
	   << endl;
      DISPLAY = 0;
  }
  // Done doing argument checking.
  ////////////////////////////////

  
  // Start the MTA By registering a module
  // and then starting the kernel
  StereoPlanner *p_sp = new StereoPlanner;
  cout << endl << "Initializing Stereo" << endl;
  p_sp->InitStereo();
  sleep(1);
  Register(shared_ptr<DGC_MODULE>(p_sp));//(new StereoPlanner));
  StartKernel();

  return 0;
}
