#include "global.hh"

// first version of GlobalUpdateVotes that doesn't take corridor following into account

void GlobalPlanner::GlobalUpdateVotes() 
{
  double steer_cmd; // commanded steering angle (rad)
  double speed_cmd; // commanded speed (m/s)

  Vector currentWaypoint; // the current waypoint
  Vector nextWaypoint;    // the next waypoint
  Vector pos;             // our current UTM position
  Vector vehFrontPos;
  int currentWPnum;

  pos.N = d.SS.Northing;
  pos.E = d.SS.Easting;
  currentWaypoint.N = (double) d.wp.get_current().northing;
  currentWaypoint.E = (double) d.wp.get_current().easting;
  currentWPnum = d.wp.get_current().num;
  // The following should only be the case if GlobalPlanner dies and restarts 
  // for some reason and finds that it's already at the finishing waypoint.

  // Are we at the waypoint?
  /* Determine if waypoint achieved by comparing the position of the front of
   * the truck instead of the rear axle. We may change calculating steer, speed
   * and corridor using the front of the vehicle also.
   */
  // Note! This gave nan before!! 
  //Vector heading(d.SS.Vel_N, d.SS.Vel_E);
  //Vector vehFrontPos = addVector(pos, scaleVector(heading, VEHICLE_LENGTH));
  vehFrontPos.E = pos.E + VEHICLE_LENGTH * cos(M_PI_2 - d.SS.Yaw);
  vehFrontPos.N = pos.N + VEHICLE_LENGTH * sin(M_PI_2 - d.SS.Yaw);
  
  if (d.wp.get_current().num == d.wp.get_size() - 1 && !d.done)
  {
    cout << "Heading for the LAST WAYPOINT! not checking withinCorridor" 
	 << endl;
    if(withinWaypoint(vehFrontPos, currentWaypoint, d.wp.get_current().radius))
    {
      cout << "Last Waypoint Achieved!!" <<endl;
      cout << "waypoint number: " << d.wp.get_current().num << endl;
      cout << "number of waypoints: " << d.wp.get_size() << endl;
      d.done = true;
    }
  }
  else if (!d.done) // && not going to the last waypoint
  {
    nextWaypoint.N = (double) d.wp.peek_next().northing;
    nextWaypoint.E = (double) d.wp.peek_next().easting;
    if (withinCorridor(vehFrontPos, currentWaypoint, nextWaypoint, 
		       d.wp.get_current().radius))
    {
      cout << "Waypoint Achieved" <<endl;
      d.wp.get_next();   // Jump to the nexts wp
      cout << " -----SHOOTING FOR WAYPOINT --- (" 
	   << d.wp.get_current().easting << ", "
	   << d.wp.get_current().northing << ")" << endl;
    }
  }

  if (!d.done)
  {
    currentWaypoint.N = (double) d.wp.get_current().northing;
    currentWaypoint.E = (double) d.wp.get_current().easting;
   
    // HACK! 01/25/04 manual offset in Yaw
    /* d.SS.Yaw = d.SS.Yaw + 20.0*M_PI/180.0; */
    // don't be so sure that we're getting Yaw between [-pi, pi]
    // Normalize the desired heading to be in [-pi,pi]
    //if( d.SS.Yaw > M_PI ) d.SS.Yaw = d.SS.Yaw - 2.0*M_PI;
    //if( d.SS.Yaw < -M_PI ) d.SS.Yaw = d.SS.Yaw + 2.0*M_PI;
    d.SS.Yaw = atan2( sin(d.SS.Yaw), cos(d.SS.Yaw) ); 

    // calculate the desired steering angle (in radians) and speed    
    steer_cmd = calc_steer_command(pos, currentWaypoint, M_PI_2 - d.SS.Yaw);  
    speed_cmd = calc_speed_command(d.SS.Yaw, pos, currentWaypoint,
				   nextWaypoint, d.wp.get_current().radius);  

    printf("current waypoint num = %d\n", currentWPnum );
    printf("Current position  = %10.3f m Easting, %10.3f m Northing\n", 
	   d.SS.Easting, d.SS.Northing );
    printf("Waypoint position = %10.3f m Easting, %10.3f m Northing,\n",
	   currentWaypoint.E  , currentWaypoint.N); 
    printf("  %.3f m Radius, %.3f m/s Speed Limit\n", 
	   d.wp.get_current().radius, d.wp.get_current().max_velocity);
    printf("Vehicle front position = %10.3f m Easting, %10.3f m Northing\n", 
      vehFrontPos.E  , vehFrontPos.N );
    printf("Relative position = %10.3f m Easting, %10.3f m Northing\n", 
      currentWaypoint.E - d.SS.Easting , currentWaypoint.N - d.SS.Northing );
    printf("Rel. pos. from front = %10.3f m Easting, %10.3f m Northing\n", 
      currentWaypoint.E - vehFrontPos.E, currentWaypoint.N - vehFrontPos.N );
    printf("yaw = %f [%.1f degrees], steer_cmd = %f\n", d.SS.Yaw,
		    d.SS.Yaw*180.0/M_PI, steer_cmd );

    //-----Assigning global Voter's Vote goodness & velocity values--------//
    double goodness = 0;
  
    // Now wayPt should contain all the right arcs to be evaluated.
    // So evaluate them.
    int i = 0;
    
    for(i=0; i<NUMARCS; i++) 
    {
      // Calculate goodness by looking at how close to the desired steering
      // the angle is:       
      // this one scales the steering linearly between 0 and MAX_GOODNESS
      // for evaluating, respectively, exactly where we want to steer and 
      // MAX_STEER when we want to steer to -MAX_STEER (or vice versa)
      /*      goodness = max( 0, (int)( round(MAX_GOODNESS 
	      - fabs( (iter->phi)/MAX_STEER - steer_cmd ) * MAX_GOODNESS) ) );
      */      
      // we'll have to think smarter about how the arbiter evaluates these
      // votes.  we don't want to give a zero vote just because we want to 
      // turn the other way 
      // let's try to make the same linear scaling, but between some
      // parameter (e.g. 100) and MAX_GOODNESS
      // TODO: We may want to change the slope of the goodness curve depending
      //  on how far the waypoint is and how wide the corridor is. If we need
      //  to turn to the waypoint very very soon, then the sloope should be
      //  steeper and also if the radius of the corridor is small, then we 
      //  should make the slope steeper to avoid turning the wrong way.

      // global_min_goodness is the goodness reported for the full left arc
      // when we want to turn full right, and for the full right arc when we
      // want to turn full left
      double global_min_goodness = 40.0;
      goodness = fmax( global_min_goodness, MAX_GOODNESS - 
		       fabs( GetPhi(i) - steer_cmd )/(2 * USE_MAX_STEER) * 
		       (MAX_GOODNESS - global_min_goodness) );
      
      //cout << "i= " << i << ", phi= " << GetPhi(i) << ", DesRelYaw= "
      //	 << steer_cmd << ", goodness= " << goodness << endl;
      
      // assign the actual vote goodness
      d.global.Votes[i].Goodness = goodness;
      
      // assign the velocity to the vote
      // this code assigns the velocity assuming that going straight is best, 
      // it actually mimics the functionality of the DFE in some 
      // unsophisticated way.
      // When run with the DFE and this code the vehicle DOES NOT TURN to the
      // next waypoint!
      //float diff = fabs( GetPhi(i) / MAX_STEER - 0 ); // [0,1]
      // What we really want to do is...
      //  1. modify our speed according to our error in Yaw, and
      //  2. anticipate ahead of time when we need to turn to the next waypoint
      // The first one is now implemented in calc_speed_command, so (for now),
      // we just set that speed for all votes
      // 3/6/04 Sue Ann Hong -- Changed to take in the RDDF speed limit and to
      // abide by it. In fact, we may want to go a little slower, but this
      // should be OK?
      d.global.Votes[i].Velo = min(speed_cmd, 
				   d.wp.peek_previous().max_velocity);
    } // end of for (each phi)
  } // end of if (not done)
  else  // We're done
  {
    cout << "[GlobalLoop] Waypoint following done" << endl;
    cout << "Total Number of WPs: " << d.wp.get_size();
    cout << "Current WP number: " << d.wp.get_current().num;
    cout << "Done: " << d.done << endl;
    cout << "[GlobalLoop] Calling ForceKernelShutdown()!" << endl;
    
    // Tell the arbiter to stop the demo since we are at the
    // end of the waypoints - this will stop the vehicle
    for(int i=0; i<NUMARCS; i++) {
      d.global.Votes[i].Goodness = 0.0;
      d.global.Votes[i].Velo = 0.0;
    }
    Mail m = NewOutMessage(MyAddress(), MODULES::Arbiter, 
			   ArbiterMessages::Update);
    sleep(1); // wait a second

    // Quit to signal that we're done with waypoint following.
    ForceKernelShutdown();
  } // end of else //done
  
  //usleep(10000); // here so that global doesn't hog all the power 


} // end GlobalUpdateVotes
