#include "VDrive.hh"
#include "vehlib/steer.h"

extern int DEBUG_ON;
extern int ERR_OUT;


/* IN THIS FILE 

   void VDrive::SteerSetup() ----- 
     // 0.  LOCK THE DEVICE (TO PREVENT OTHER THREADS FROM USING IT)
     // 1.  CLEAR DEVICE OF ERROR (CLOSE DEVICE IF IN ERROR STATE)
     // 2.  OPEN DEVICE IF WE ARE SUPPOSED TO
     // 3.  CALIBRATE DEVICE IF WE ARE SUPPOSED TO
     // 4.  CLOSE THE DEVICE IF WE ARE SUPPOSED TO
     // 5.  SET STATE VARIABLE

   void VDrive::SteerLoop() -- Thread that controls gas and brake.

     
*/

void VDrive::SteerSetup() {

  DeviceHandle *steer = & d.s.deviceHandles[DEVICES::STEER];
  
  // STEPS
  // 0.  LOCK THE DEVICE (TO PREVENT OTHER THREADS FROM USING IT)
  // 1.  CLEAR DEVICE OF ERROR (CLOSE DEVICE IF IN ERROR STATE)
  // 2.  OPEN DEVICE IF WE ARE SUPPOSED TO
  // 3.  CALIBRATE DEVICE IF WE ARE SUPPOSED TO
  // 4.  CLOSE THE DEVICE IF WE ARE SUPPOSED TO
  // 5.  SET STATE VARIABLE
  
  // lock the steer (will unlock when we exit this block)
  // Step 0.
  Lock myLock(d.DeviceLock[DEVICES::STEER]);
  
  // Step 1. Clear device of error
  if( steer->state == DeviceStates::Error ) {

    if( DEBUG_ON ) cout << DeviceStrings[DEVICES::STEER] 
			<< " - Clearing Device Error (Setup Step 1)." << endl;

    steer_close();
    steer->is_open = false;
    steer->state = DeviceStates::InActive;
  }
  
  // Step 2. Open device 
  // If the steer is not open, and we are supposed to be
  // using it, then OPEN THE DEVICE
  if ( ! steer->is_open && steer->use ) {
    
    steer->state = DeviceStates::Init;
    
    if( DEBUG_ON ) cout << DeviceStrings[DEVICES::STEER] 
			<< " - Opening Device (Setup Step 2)." << endl;
    
    if( steer_open(steer->port) < 0 ) {
      // Opening Failed => set error state
      if( ERR_OUT ) cout << DeviceStrings[DEVICES::STEER] 
			 << " - Failed Opening Device (Setup Step 2)." << endl;

      steer->state = DeviceStates::Error;	
      steer->is_open = false;
    } else {
      steer->is_open = true;
           
    }
  }
  
  // Step 3. Calibrate the device 
  // If the steer is open, and we are supposed to be
  // using it, and it is flagged to be calibrated
  // then CALIBRATE THE DEVICE
  if ( ! steer->is_open && 
         steer->use && 
         steer->needs_calibration ) {
    
    steer->state = DeviceStates::Calibrate;

    if( DEBUG_ON ) cout << DeviceStrings[DEVICES::STEER] 
			<< " - Calibrating Device (Setup Step 3)." << endl;
    
    if( steer_calibrate() < 0 ) {
      // Calibrating Failed => set error state	
      if( ERR_OUT ) cout << DeviceStrings[DEVICES::STEER] 
			 << " - Failed Calibrating Device (Setup Step 3)." << endl;
      steer->state = DeviceStates::Error;
    } 
    steer->needs_calibration = false;
  }
  
  // Step 4. Close the device 
  // If the steer is open, and we are not
  // using it anymore, or it is flagged to be closed,
  // the CLOSE THE DEVICE
  if ( !steer->use ) {

    if( steer->is_open ) {    
      steer_close();
      steer->is_open = false;
      if( DEBUG_ON ) cout << DeviceStrings[DEVICES::STEER] 
			  << " - Closing [Not Using] Device (Setup Step 4)." << endl;    }
    steer->state   = DeviceStates::InActive;
  }
  
  
  // Step 5. Set the state Varible 
  // If there is no error and we are open and in use, then set active
  if( steer->is_open &&
      steer->state != DeviceStates::Error &&
      steer->use) {
    
    steer->state = DeviceStates::Active;
  } 
  // other states were set above so we are ok
  
}


