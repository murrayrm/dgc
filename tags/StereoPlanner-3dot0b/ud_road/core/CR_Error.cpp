//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_Error.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// print message then exit program.  tacks a newline on in console version
// (newline not expected so that popping up a window for the message 
// will look right)

void CR_error(char *message, ...)
{
  va_list args;
  
#ifdef CR_CONSOLE

  int message_length;
  char *new_message;

  message_length = strlen(message);
  new_message = (char *) calloc(message_length + 1, sizeof(char));
  strcpy(new_message, message);
  strcat(new_message, "\n");

  va_start(args, new_message);

  vprintf(new_message, args);

  va_end(args);

#endif

#ifdef CR_WINDOWED

  printf("no windowed CR_Error() at this time\n");
  exit(1);

#endif

  //  printf("%s\n", message);

  // force a seg fault so that we can do a backtrace in gdb
  //  CR_Matrix *M = NULL;
  //  printf("%f\n", M->rc[3][5]);

  //while (1);

  CR_exit(1);
  //  exit(1);
}

//----------------------------------------------------------------------------

void CR_exit(int exit_code)
{
  // force a seg fault so that we can do a backtrace in gdb
  //  CR_Matrix *M = NULL;
  //  printf("%f\n", M->rc[3][5]);

  exit(exit_code);
}

//----------------------------------------------------------------------------

// print to stdout and flush for immediate display

void CR_flush_printf(char *fmt, ...)
{
  va_list args;

  va_start(args, fmt);

  vprintf(fmt, args);

  va_end(args);

  //  printf("%s\n", fmt);

  fflush(stdout);
}

//----------------------------------------------------------------------------

// print to stdout and echo to file fp if fp != NULL

void CR_log_printf(FILE *fp, char *fmt, ...)
{
  va_list args;

  va_start(args, fmt);

  vprintf(fmt, args);

  if (fp) {
    vfprintf(fp, fmt, args);
    fflush(fp);
  }

  va_end(args);
}

//----------------------------------------------------------------------------

// same behavior as printf

void CR_printf(char *fmt, ...)
{
  va_list args;

  va_start(args, fmt);

  vprintf(fmt, args);

  va_end(args);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
