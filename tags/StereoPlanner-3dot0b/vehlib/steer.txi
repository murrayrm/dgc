@c -*- texinfo -*-

@node steer,,,Top
@chapter Steering Interface

The @code{steer} device driver is used to control the steering
actuator on the truck.

@menu
* intro: steer/intro.		Overview of the steer system
@end menu

@node steer/intro,,,steer
@section Introduction

The steer subsytem uses a Parker Automation Gemini GV6 Series Digital
Servo Controller and Drive (GV6 for short).  The GV6 has its own
internal programming language and can be set up to do very
sophisticated motion control, including motion profiles, hardware and
software end of travel limits, error processing, etc.

For the Tahoe, the GV6 is set up to be run in ``continuous execution''
mode, where position commands are sent repeatedly to the controller.
This allows the angle of the wheels to be controlled in a smooth
fasion.

@section Configuration and calibration

The @code{steer_calibrate} function is used to calibrate the drive.  It
does a full calibration, including homing the motor and setting the
software limits.  It takes approximately 30 seconds to run.  (Note:
this routine has not worked well in the field, including generating
segmentation faults.  Someone should rewrite this, keeping in mind
that in a race environment, intermittent communication failures are
possible, so to write the routine carefully.)

Here is the sequence of operations and the corresponding GV6 command
used to implement it.

@enumerate
@item Test communcation with controller.
The first action of the calibration routine is to make sure that it
can talk to the controller.  If it is not known whether the drive is
powered on, then the controller will wait for @code{POWERON_TIMEOUT}
seconds (9 sec) before trying to talk to the controller across the
serial port.  The communication test is done by first issuing a motor stop
command (@code{!S}) and resetting the communcation parameters (no
echo, no error limits).  The drive is then asked to return its
revision level (@code{TREV}), which is checked to make sure it returns
the correct value.  If the first communication test fails, a reset
command (@code{RESET}) is sent to the controller and we (recursively)
try again.

@item Load configuration file.
One communications has been established, the drive is enabled
(@code{DRIVE1}) and a 
configuration file (default: @file{textInputFiles/be344ljInit.txt}) is
downloaded to the drive.  This file is assumed to exist.  (Note: often
it does not, which just means that the parameters are not reloaded.
The parameters are stored in EEPROM, so this is not a big deal.)

@item Reset the drive.
At this point the drive is reset and communications are once again
tested.  (RMM note: there is yet another recursive call to steer_init
here; this seems like it could cause lots of problems.)

@item Home the drive.
The drive is prepared for homing by putting it into absolute mode
(@code{MA1}), enabling the hard limits (@code{LH3}) and disabling the
soft limits (@code{LS0}).  The drive is commanded home in the positive
direction (@code{HOM0}), which will cause it to search for the home
position.  This is controlled by the GV6 software, which drives the
motor around looking for the (hardware) home switches to be activited.

@item Set the software limits.
The software limits are set by setting the velocity and acceleration
of the drive (@code{V4}, @code{A10}) and then commanding the drive to
move to its negative and positive limits.  The location of the drive
is computed at the end of each motion (determine by the hardware
limits, which turns off the drive motion) and the software limits are
set to 5000 counts less than the hardware limits.  The hardware and
software limits are stored in the motor controller and the steer
module.

@item Place motor into operating mode.
Finally, the motor is sent a sequence of commands that get it ready
for accepting drive commands.  The software limits are enabled
(@code{LS3}) and continuous execution mode is turned on
(@code{COMEXC1}).  The motor controller is told not to stop processing
commands if a limit is hit (@code{COMEXL1}).  The motor is put into
absolute mode (@code{MA1}) and the acceleration and velocity values
are set (@code{A20}, @code{V8}).  Finally, the motor is commanded back
to zero and the calibration routine waits until the motion is completed.

@end enumerate
