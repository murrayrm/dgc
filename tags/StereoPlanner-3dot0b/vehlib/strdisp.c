/*
 * strdisp.c - Callback functions for steering display 
 *
 * RMM, 17 Jan 04
 * 
 * This file contains functions that are needed to implement the display
 * contained in strdisp.dd.  Initially empty.
 */

#include "steer.h"
#include "sparrow/display.h"
#include "strdisp.h"			/* include the display table */

/* Call the steering calibration routine */
int strdisp_calib(long arg)
{
  init_steer();
  return 0;
}

/* Get the drive status */
int strdisp_status(long arg)
{
  steer_state_update(0, 1);		/* get all states */
}
