Changes for vdrive package
RMM, 31 Dec 03

***** v1_0e *****

4 Feb 04, RMM: release 1.0e fixes
  * Commented out estop in vdrive so that -e flag is not required
  * Put revision number into display (need to remember to update)
  * Added release tag to Makefile to install release in /usr/local
      + we should use this for installing revisions in a fixed place
      + current version won't work because data files won't be found
  * fixed small bug in steering initialization (needed to check < 0)
  * commented out IMU and GPS code
  * fixed up vddrive.dd to display vehstate IMU, GPS data

***** v1_0d *****

1 Feb 04, RMM: changes to support 1 Feb testing
  * Changed display to show VState data that is being sent
  * Got rid of kf code; now uses vehdata for cruise
  * Added 'A' key binding to put system in autonomous mode
  * Updated drive code to work in autonomous mode

***** v1_0c *****

25 Jan 04, RMM: changes to get GPS working
  * GPS thread taking up all processor time on titanium; reading *way* too fast
  * Got rid of some screen display items that were not used by vstate
  * Changed serial_read at top of GPS loop to blocking read
      + no reason not to block at this point; just wasting CPU cycles
  * Put usleep at different point in loop so that delays happen after 
    every attempt to read GPS (need to make sure this is OK)
  * Added flags and display vars to track GPS progress; looks OK

24 Jan 04, RMM: small fix for estop
  * Worked with Jeff to install estop code
  * Had to add -e flag to allow estop to be disabled during testing

***** v1_0b ******

18 Jan 04, RMM: field changes at El Mirage
  * Fixed vdrive -b bug: wasn't checking == 1 before calling brake control loop
  * Bound 'c' to toggle capture for easier data capture
  * Changed data begin captured (through chn_data assignments)
  * Moved brk_lock from vdrive.c to brake.c; use by full_stop
  * brake.c: added simple locking using brk_lock in send_brake_command
  * got rid of cout throughout brake.c; replaced with dbg_error calls

17 Jan 04, RMM: worked on brake code with Will H
  * moved variables defined in brake.h to brake.c to avoid linking problems
  * fixed problems in control logic for deadband
  * still need to do some parameter tuning (max_vel, max_acc, tolerance)

17 Jan 04, RMM: changes to steering driver
  * Added a check for new position before writing to drive
  * Got rid of state/position update in vdrive steering loop
  * Tried putting in a D5000 before homing in steer_init; didn't work (??)

17 Jan 04, RMM: actuator interface for steering (for debuggin)
  * added strdisp.{dd,c} to create steering interface table
  * set up low level commands that we can use to test steering
  * very rudimentary setup for now; need to improve later

14 Jan 04, RMM: updates in preparation for El Mirage test
  * added usage message to vdrive and vstate.  Use -h to see it
  * remote mode is now turned on automatically when MTA is turned on
  * created default vdrive.ini-def and vdrive.dev-def files
  * changed "acc_" to "thr_" throughout vdrive (and A to T on display)
  * MTA mode is now on by default in vdrive and vstate
  * Added some code to shut down kernel, but not enabled in MTA?  (bug #37)
  * Cruise control mode now adds accel offset (ref_accel) to command 

13 Jan 04, RMM: clean up of vdrive v0.9 for use by vehicle team
  * bound 'O' key to put vehicle into "off" mode

**** v1_0a ****

4 Jan 04, RMM: changes for vehicle testing
  * added configuration read at start of vdrive to load parameter file
  * added two modes for vdrive, along drive and steer axes

4 Jan 04, RMM: initial capture code
  * compiled in sparrow capture routines
  * set up thread to take data at roughly 100 Hz
  * captures data at about 50 Hz on grandchallenge

3 Jan 04, HH/RMM: state filter code
  * H: summarize your changes here
  * Moved non-global defs out of VState.hh and into statefilter.hh

3 Jan 04, RMM: MTA interface to vdrive
  * Got rid of StateStructMsg.cc: pipes weren't working anyway
  * Created VDrive.hh to hold information needed to command motion

3 Jan 04, RMM: integration of cruise control code
  * copied curise.cc from tests/waypointnav
  * also grabbed lookup table code (rename LUtable.{cc,h})
  * added to Makefile and fixed linking errors

3 Jan 04, HH: integration of magnetometer into vstate
  * copied magnetometer code from steerCar
  * integrated and working

3 Jan 04, RMM: vdrive changes
  * Fixed problems in joystick returns; ints instead of chars
  * Set up joystick input for brake/throttle
  * Added brake initialization on startup

3 Jan 04, RMM: continued work on MTA version of vstate
  * Put in code to copy GPS data across MTA channel
  * Inserted GPS thread into vstate (had been left out)

2 Jan 04, RMM: modifications for MTA version of vstate
  * Created StateStructuMsg.hh that defines the state space structure
  * Put imudata directly into state struct
  * Implemented vdrive query to vstate server: working!
  * Added some simple instructions to INSTALL

2 Jan 04, Tim Chung: updates to brake code
  * Put vel and acc commands into the display so that we can test things
  * Created emergency stop interface, bound to space bar, to quickly brake

2 Jan 04, RMM: local modes for titanium
  * removed TAGS, v[ds]disp.h, vehlib.html
  * these are created from existing source => don't CVS
  * Removed these from grandchallenge as well; shouldn't have been there

2 Jan 04, RMM/IG: MTA code
  * Ike worked through the first round of MTA code
  * Set vstate up as a mailbox server which will return the vehicle angle
  * Initially set up vdrive as server (oops!); changed to vstate

2 Jan 04, RMM: local mods for titanium
  * added printfs to steer.cc to print limitts (will need later)
  * added debugging information to brake code, display
  * basic brake functionality working (!)

2 Jan 04, RMM: integration of brake code
  * copied brake.[ch] from moveCar
  * got rid of references outside this directory
  * created vehports.h to have list of default port assignments
  * created accel.h with just ACCEL_CS; will put rest in later
  * initial version compiling OK

2 Jan 04, RMM: minor fixes discovered when installing on palladium
  * Fixed ../serial reference in gps.h (changed to ./)
  * Eliminated links to Constants and Msgs from steering actuator
  * Updated dependencies in Makefile; was missing program source

1 Jan 04, RMM: MTA prep
  * Started to put in changes for MTA; need to run on grandchallenge
  * Rudimentary version of Ike's TestModule running in vdmta.cc

31 Dec 03, RMM: minor updates
  * Included dbglib in serial.c to turn off messages
  * Converted dbg_printf calls to dbg_{info,warn,etc}
  * Renamed overview.{dd,c} to vddisp.{dd,c}
  * Reconfigured makefile to create vehlib.a + added installation routines

31 Dec 03, RMM: initial working versions
  * vdrive - command motion (and read input)
  * vstate - read state input (eventually filter)
