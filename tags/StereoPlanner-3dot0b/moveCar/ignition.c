//#include "moveCar/ignition.h"
#include "ignition.h"
#include <stdio.h>
#include "../OBDII/OBDII.h"
#include <unistd.h>

/* ignition controls the run pin, but ignition action hasn't been defined.
 * probably need some sort of feedback for ignition.
 * Action available:
 * PULL_RUN_HIGH	: pull run pin high
 * PULL_RUN_LOW		: pull run pin low
 */

int initIgTrans() {
  return pp_init(IGPPORT, PP_DIR_OUT); 
}

/* call when there's a cooling "emergency" in the car */
int AC_ON () {
  // car is not under heavy load
  // not hot -- check w/ temperature sensor?
  pp_set_bits(IGPPORT, AC_CONTROL, 0);
  return 0;      /* how do we know if it's actually on? */
}

/* do/can we control the level of AC? */

int AC_OFF() {
  pp_set_bits(IGPPORT, 0, AC_CONTROL);
  return 0;
}

int startCar() {
  // stop OBDII module (software)
  pp_set_bits(IGPPORT, RUN_PIN, 0);      // set CAR Run high
  // start OBDII module (software)
  pp_set_bits(IGPPORT, IGN_PIN, 0);   // ignition on
  while (Get_RPM() <= 0);  // wait for the motors to start turning
  //sleep(2);
  pp_set_bits(IGPPORT, 0, IGN_PIN);   // ignition done
  return 0;
}


void ignition(int action)
{
   switch(action)
   {
	case PULL_RUN_HIGH:
	   pp_set_bits(IGPPORT, RUN_PIN, 0);
	   break;

	case PULL_RUN_LOW:
	   pp_set_bits(IGPPORT, 0, RUN_PIN);
	   break;

	case IGNITE_CAR:
	   fprintf(stderr, "Igniting the car is not definied yet\n");
	   break;
	
	default:
	   fprintf(stderr, "ignition error: unknown action\n");
   }
}
