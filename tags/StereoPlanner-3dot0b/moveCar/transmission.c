#include <stdio.h>

//#include "moveCar/transmission.h"
#include "transmission.h"
#include <unistd.h>
#include "../OBDII/OBDII.h"

/* trans_shift_gear shifts the gear of the car. You can also select the gear to be
 * controlled by the driver. 
 * Available gear:
 * TRANS_NEUTRAL	: neutral gear
 * TRANS_FREE		: free float
 * TRANS_FIRST		: first gear
 * TRANS_SECOND		: second gear
 * TRANS_THIRD		: third gear
 * TRANS_REVERSE	: reverse gear
 * TRANS_HUMAN		: human control
 *
 */

/*
#ifdef PP_ACCEL
#undef PP_ACCEL
#define PP_ACCEL 0
#endif
*/

void trans_shift_gear(int gear)
{
   switch(gear)
   {
      	case TRANS_NEUTRAL:
	   pp_set_bits(TRANSPPORT, TRANS_NEUTRAL_PIN_HIGH, TRANS_NEUTRAL_PIN_LOW);
	   break;

	case TRANS_FREE:
	   pp_set_bits(TRANSPPORT, TRANS_FREE_PIN_HIGH, TRANS_FREE_PIN_LOW);
	   break;

	case TRANS_FIRST:
	   pp_set_bits(TRANSPPORT, TRANS_FIRST_PIN_HIGH, TRANS_FIRST_PIN_LOW);
	   break;

	case TRANS_SECOND:
	   pp_set_bits(TRANSPPORT, TRANS_SECOND_PIN_HIGH, TRANS_SECOND_PIN_LOW);
	   break;

	case TRANS_THIRD:
	   pp_set_bits(TRANSPPORT, TRANS_THIRD_PIN_HIGH, TRANS_THIRD_PIN_LOW);
	   break;

	case TRANS_REVERSE:
	   pp_set_bits(TRANSPPORT, TRANS_REVERSE_PIN_HIGH, TRANS_REVERSE_PIN_LOW);
	   break;

	case TRANS_HUMAN:
	  /* (stop gas pump, push brake, chk w/ gps&imu), but for now... */
          /* OBDII version */
          /* set_accel_abs_pos(0.0);
	     set_brake_abs_pos_pot(1.0, 0.2, 0.2);
             while (Get_Speed() > 0);
	     set_brake_abs_pos_pot(0.0, 0.2, 0.2);
	     while (brake_ready != 0);
	  */
          /* super-hack version */
	  /* First, shift to neutral and wait for the car to stopish. */
           pp_set_bits(TRANSPPORT, TRANS_NEUTRAL_PIN_HIGH, TRANS_NEUTRAL_PIN_LOW);
	   sleep(5);
	   pp_set_bits(TRANSPPORT, TRANS_HUMAN_PIN_HIGH, TRANS_HUMAN_PIN_LOW);
	   break;

	default:
	   fprintf(stderr,"trans_shift_gear() error: unknown gear\n");
   }
}			
