Braking! Actuator & Force Sensor
Revision History:  7/14/2003   Sue Ann Hong    Created
                   7/15/2003   Sue Ann Hong    Added force sensor doc
                   7/17/2003   Sue Ann Hong    Almost done
----------------------------------------------------------------------------

Braking Actuator
Part Name: Ultramotion Digit Linear Actuator
Part Number: D-2B.125-QS23-7.2-P-BR/4
Manual/Specs: 
   http://www.ultramotion.com/products/actuator_info.php?ac=1&mt=19&ls=5&st=3&nm=12&tm=5&sw=0&pt=1 Jul 10 
   http://www.qcontrol.com/manuals.htm 
Software Contact: Sue Ann Hong (sueh@its)
Mechanical Contact: Will Heltsley, Scott Fleming (scottf@its)

CURRENT STATUS: 
 * written initBrake, need to add more initializations as specs are decided
   (see the last item for more). Also, should 'struct'ize the commands.
 * written getBrakePosAbs/Rel
 * (when the actuator gets here) need to find the relationship between
   what we send and what the vehicle does, so that we can set appropriate
   parameters and units like 'in what unit we send the velocity and 
   acceleration to braking functions', 'what's the maximum acceleration and
   velocity that the braking pedal can handle', etc. From this we can finish
   the setPosAbs, setPosRel (the conversion part isn't done).   
 * need to test all of that (actuator not here yet)
 * MTA
 * documentation: Steps 4-6 not complete
 * there's a bunch of fancy stuff we can do with this motor, especially
   initializing some parameters so that it shuts down when things become
   dangerous, or come back on when it's safe. until we decide what the driving
   logic is, we can ignore these and just set them to default values. 

1. Design
  The braking actuator pushes/pulls the brake pedal and is controlled by the 
 MagicBox through serial connection.
   
   Interface
  RS232 (also can use RS485 but the current implementation uses RS232).
 Need a noise-blocking mechanism which I believe the mechanical group
 is already taking care of. 

   Initialization
  There's a long list of parameters to be initialized for the actuator
 (see the command manual). See the initialization function in the Software
 Implementation for more detail. The commands (9-bit binary form) are of the 
 form:
  Actuator-ID(1byte) LengthOfCommand=#ofParameters+1forCommandItself(1) 
  Command(1) Parameters(?) CheckSum(1). 
   
   Software Implementation
  An initialization sequence must be impelemented along with the braking 
 function. Also, the actuator has a kill mechanism that shuts the motor
 down according to the conditions given. This may be implemented according
 to the needs.

2. Hardware Implementation
  None except an RS232 cable. If we want/can, we can use 7 digital I/O lines
  to control KillMotor (shuts down motor immediately), stop in the middle of
  a movement (Stop Enable in MoveAbs/MoveRel), etc.

3. Software Implementation
 * int checkSum(char *comm, int len);
    A helper function to calculate the check sum which is sent as the last
    byte of the command to confirm that all the bytes were transferred 
    correctly.

 * initBrake(/* int bRate */ )
    Initializes the brake actuator to operate with the PC104 with the settings
    specified by the mechanical needs of the DGC project.
   Input: (bRate, baud rate?)
   Output: 
   Parameters to be initialized:
      Protocol - options: 8-bit Ascii or 9-bit binary 
               - currently using 9-bit binary for speed
      Baud Rate - currently using 57600 (default for the actuator)
      Ack Delay - delay time in the actuator between receiving a command
                  and sending an acknowledgement response. 
                - currently using 0ms (recommended for RS232).
      Maximum Temperature - temp @which the actuator shuts down
                          - currently using 70C (a lil below real maxtemp)
   Other parameters: (* = probably need it; ? = need this?; ?! = should do it)
     SilverMax ID (default = 16)
     Control Loop Mode (default = position)
     Digital input filter
     Done bit (default = disabled)
     Direction (default = +for+ -for-)
     Dual/single Loop Control (default = single loop control)
 *   Disable/enable Motor Driver
 *   Multitasking (default = disabled) - may want it for
        Hard Stop Move, Stop, or Halt, or End.. or Kill Motor
 *   Error limits (moving error = 0, holding error = 0,
                 delay to holding = 12ms)
     Gravity Offset Constants (default = 0)
     Kill Disable Driver (default = set)
        => when Kill Motor Condition met, motor driver disabled
 *   Kill Enable Driver (default = unset)
 *   or Kill Motor Conditions (should be set soon?) need I/O 1, 2, or 3
        and Kill Motor Recovery (should be set soon?)
 applicable? Low Voltage Processor Trip
             Power Low Recovery
 *   Motor Constants (default = factory set voltage)
 ?   Open Loop Phase
 *   Over Voltage Trip (default = 52V)
     Phase Advance Constants (default = factory optimal)
     S-Curve Factor - acceleration curve (default = trapezoid)
 *   Serial Interface (default = RS-232)
 ?!  Torque Limits (should set this?)
    & Torque Ramp Up
   Note: Need more specs from the mech group.

** int setBrakeAbs(float absPos, float velocity, float accel);
   Input: relative position of the brake pedal (+ to push, - to release)
          (maximum?) speed and acceleration of the actuator movement
   Output: [physical] the actuator moves the brake pedal (push/release)
           success? final velocity?
   Mechanism: need current state? - no.
              command (w/ time) - 0xB0
              feedback - ACK only
                       - no feedback of the final position
              
** int setBrakeRel(float relPos, float velocity, float accel);
   Input: relative position of the brake pedal (+ to push, - to release)
          (maximum?) speed and acceleration of the actuator movement
   Output: [physical] the actuator moves the brake pedal (push/release)
           success? final velocity?
   Mechanism: need current state? - no.
              command - 0xB1
              feedback - only ACK
                       - no feedback of the final position

 * float getBrakePos()
   Input: none
   Output: position of the brake pedal with a timestamp?
   Mechanism: need current state? - gives the current state(position)
              command - 0x02(read register) 0x01(position register)
              feedback - returns the position of the actuator

 * possible other functions: stopBrake() - end/stop/pause?
                             
 * currently undetermined parameters: 
    - maximum velocity of the actuator
    - inch-to-count conversion (though we may not need it if we just use the 
      count as the unit of 'position')
    - com port 

~~~~~    
4. Failure Analysis
 * Power out -> actuator stays in place
  - mech solution?
 * Too much force -> braking pedal (almost) breaks
 * 

5. MTA stuff
 Messages to be sent/received from/to (data structures, info)
  - to Diagnostic display? (lots of stuff)
 Control Commands:
  - 

6. Finally
 Total bandwidth software uses (comm with the device & comm via MTA.
 Give Ike a paper copy and attach this to README, CVS commit it.
~~~~~


Brake Force Sensor (TLL-1K)
Part Number: TLL-1K
Manual/"specs": go to grandchallenge.caltech.edu
hardware: sensor <--> a differential op-amp --> ADC <--> CMOS-TTL
converting latch <--> parallel <--> magicbox
General idea: shucks.~
Software Contact: Sue Ann Hong (sueh@its)
Mechanical Contact: eh.. -_-;;

CURRENT STATUS:
 * need to test parallel port I/O code, then test getBrakeForce
 * need to test the circuit w/ a voltage feeder
  - if it doesn't work:
    ~ adjust the frequency of the 555 chip (now at ~48KHz). we'll use a 
      clockbox when we know which frequency to use.
    ~ change the connection between parallel and the latch to the alternate
      one (i'll have this on the schematic)
    ~ ummm... debug...
   then test with the sensor: how would i apply force to the tension sensor?
 * if the test is successful, get a solderboard and make sure it really works?
   make PCB layout on Protel (can be done laaater)
 * need to write parallel port I/O code, then finish getBrakeForce
 * MTA
 * documentation: steps 4-6

1. Design
  The force sensor outputs voltage at ~2.3mV/V/1000lbs. Since it requires 10V,
 this means the output voltage of the force sensor is between 0V and 23mV.
 The output voltage is hence amplified then converted to digital so that the
 MagicBox can read it and convert the voltage to force.

   Interface (h/w)
  Sensor -> differential amp (LM324N w/ Rs) 
  -> ADC w/ parallel output (ADC0809) 
  <-> CMOS-TTL converting buffer (CD74HCT244) <-> parallel port <-> MagicBox
  
   Initialization
  None.

   Software Implementation
  Get the parallel input, convert the voltage to force.

2. Hardware Implementation
  - As described in the Interface section. Schematic attached.
  - Amplification introduced a systematic error which is corrected linearly in
    software (getBrakeForce).
  - A/D conversion may introduce the same kind of error which hopefully will
    be adjustable.
  
3. Software Implementation
 in TLL.c
 * float getBrakeForce()
  Reads the "voltage" from the parallel interface board then returns it in lbs.

 * possible other functions: none at the moment

 * currently needed parameters: 

4. Failure Analysis
 * Noise!!!
 * Board shorted/disconnected(joints broken, wire cut, fried chip, etc)/...
 * Connection cable disconnected.
 * Power out
   - Nothing much we can do. It should come back on when power does.
 * 

5. MTA stuff
 Messages to be sent/received from/to (data structures, info)
  - to Diagnostic display? (lots of stuff)
 Control Commands:
  -

6. Finally
 Total bandwidth software uses (comm with the device & comm via MTA.
 Give Ike a paper copy and attach this to README, CVS commit it.


Additional Notes
 * driving logic
   needs - CLARAty decisions of turn angle, velocity
   does - limits velocity according to the angle
          (what if we have to limit the angle cuz we're going way too fast?
           will this never happen since we wont be going that fast?
           either way these actuators need to work with steering)
       --> final velocity and angle, calls the methods above to actually
             drive the car.
       --> needs to set positions of actuators according to the velocity 
           and angle (which is to be converted to the position of actuators
           and movement time/rate (if applicable) when calling setPosition)
   units?
