/* CellData.cc
   Author: J.D. Salazar
   Created: 7-16-03
   
   This class contains the class that stores data in the map structure for the local obstacle avoidance mechanism 
   to be used by Team Caltech in the 2004 Darpa Grand Challenge. */

#include "CellData.h"

// Default constructor creates a cell containing NO_DATA for every entry in the cell.
CellData::CellData()
{
  setElevation(NO_DATA);
  setElevationConfidence(NO_DATA);

  setClassification(NO_DATA);
  setClassificationConfidence(NO_DATA);

  setObstacleData(NO_DATA);
  setObstacleDataConfidence(NO_DATA);

  setRoughness(NO_DATA);
  setRoughnessConfidence(NO_DATA);

  setGoodness(NO_DATA);

  setTimeLastModified(NO_DATA);
}

// Constructor used to set values for all the data.  Currently roughness confidence is not used as the perceptor maps don't give it.
CellData::CellData(int elevation, int elevationConfidence, 
		   int classification, int classificationConfidence,
		   int obstacleData, int obstacleDataConfidence,
		   int roughness, int roughnessConfidence,
		   int timeLastModified)
{
  setElevation(elevation);
  setElevationConfidence(elevationConfidence);

  setClassification(classification);
  setClassificationConfidence(classificationConfidence);

  setObstacleData(obstacleData);
  setObstacleDataConfidence(obstacleDataConfidence);

  setRoughness(roughness);
  setRoughnessConfidence(roughnessConfidence);

  if(obstacleData!=NO_DATA) {
    setGoodness(250*(1-obstacleData));
  } else {
    setGoodness(NO_DATA);
  }

  setTimeLastModified(timeLastModified);
}

// Constructor given data encoded in the compressed perceptor format.
CellData::CellData(int cellData, int timeLastModified)
{
  setCellData(cellData);
  setTimeLastModified(timeLastModified);

  if(getObstacleData() == NO_DATA) {
    setGoodness(NO_DATA); 
  } else {
    setGoodness(250*(1-getObstacleData()));
  }
}

// Assignment Operator
CellData & CellData::operator= (const CellData &other)
{
  // Check for self assignment
  if( this == &other ) return *this;
  
  destroy();
  copy(other);
  
  return *this;
} 

// Comparison Operators
bool CellData::operator== (const CellData &other) const
{
  return getElevation() == other.getElevation() &&
    // As there is no NO_DATA flag for confidences, if the Elevation contains NO_DATA the cells are equal
    // regardless of the confidence (this goes for other confidences as well).
    (getElevationConfidence() == other.getElevationConfidence() || getElevation() == NO_DATA) &&
    getClassification() == other.getClassification() &&
    (getClassificationConfidence() == other.getClassificationConfidence() || getClassification() == NO_DATA) &&
    getObstacleData() == other.getObstacleData() &&
    (getObstacleDataConfidence() == other.getObstacleDataConfidence() || getObstacleData() == NO_DATA) &&
    getRoughness() == other.getRoughness() &&
    (getRoughnessConfidence() == other.getRoughnessConfidence() || getRoughness() == NO_DATA) &&
    getGoodness() == other.getGoodness() &&
    // If the cell contains no data whatsoever, than the time last modified is irrelevant when comparing cells.
    (getTimeLastModified() == other.getTimeLastModified() || (getElevation() == NO_DATA &&
							      getClassification() == NO_DATA &&
							      getObstacleData() == NO_DATA &&
							      getRoughness() == NO_DATA));
} 

bool CellData::operator!= (const CellData &other) const 
{
  return !(*this == other);
}

// Mutators
void CellData::setTimeLastModified(int time) 
{ 
  _timeLastModified = time; 
}

void CellData::setGoodness(int goodness)
{
  _goodness = goodness;
}

void CellData::setCellData(int cellData)
{
  _cellData = cellData;
}

void CellData::setElevation(int elevationData) 
{
  // Set the specific no data flag if the generic one is given.
  if(elevationData == NO_DATA)
    {
      elevationData = NO_ELEVATION_DATA_FLAG;
      encodeData(elevationData, ELEVATION_DATA_SHIFT, ELEVATION_DATA_MASK);
    }
  else
    {
      // Set elevation sign bit.
      int elevationSign = (elevationData < 0) ? 1 : 0;
      encodeData(elevationSign, ELEVATION_SIGN_SHIFT, ELEVATION_SIGN_MASK);
      
      // Set elevation data bits.
      elevationData = std::min(abs(elevationData), MAX_ELEV_DATA);
      encodeData(elevationData, ELEVATION_DATA_SHIFT, ELEVATION_DATA_MASK);
    }
}

void CellData::setElevationConfidence(int elevationConfidence)
{ 
  // If the generic NO_DATA flag is set, give a confidence of 0 (as there isn't a no data flag for confidences in the
  // perceptor code).
  if(elevationConfidence == NO_DATA || elevationConfidence < 0)
    elevationConfidence = 0;
  // Make sure confidence is not too large and if it is cap it.
  else
    elevationConfidence = std::min(elevationConfidence, MAX_ELEV_CONF);

  encodeData(elevationConfidence, ELEVATION_CONFIDENCE_SHIFT, ELEVATION_CONFIDENCE_MASK);
}

void CellData::setClassification(int classificationData)
{
  // Set the specific no data flag if the generic one is given.
  if(classificationData == NO_DATA)
    classificationData = NO_CLASSIFICATION_DATA;
  // Make sure the data is in the appropriate range.
  else if(classificationData < 0 || classificationData  > MAX_CLASS_DATA)
     classificationData = NO_CLASSIFICATION_DATA;

  encodeData(classificationData, CLASSIFICATION_DATA_SHIFT, CLASSIFICATION_DATA_MASK);
}

void CellData::setClassificationConfidence(int classificationConfidence)
{ 
  // If the generic NO_DATA flag is set, give a confidence of 0 (as there isn't a no data flag for confidences in the
  // perceptor code).
  if(classificationConfidence == NO_DATA || classificationConfidence < 0)
    classificationConfidence = 0;
  // Make sure confidence is not too large and if it is cap it.
  else
    classificationConfidence = std::min(classificationConfidence, MAX_CLASS_CONF);

  encodeData(classificationConfidence, CLASSIFICATION_CONFIDENCE_SHIFT, CLASSIFICATION_CONFIDENCE_MASK);
}

void CellData::setObstacleData(int obstacleData)
{
  // Set the specific no data flag if the generic one is given.
  if(obstacleData == NO_DATA) 
    obstacleData = NO_OBSTACLE_DATA;
  // Make sure the data is in the appropriate range.
  else if(obstacleData < 0 || obstacleData  > MAX_OBJECT_DATA)
     obstacleData = NO_OBSTACLE_DATA;

  if(obstacleData == NO_OBSTACLE_DATA) {
    setGoodness(NO_DATA);
  } else {
    setGoodness(250*(1-obstacleData));
    //cout << "\nset to " << 250*(1-obstacleData);
  }

  encodeData(obstacleData, OBJECT_DATA_SHIFT, OBJECT_DATA_MASK);
}


void CellData::setObstacleDataConfidence(int obstacleDataConfidence)
{ 
  // If the generic NO_DATA flag is set, give a confidence of 0 (as there isn't a no data flag for confidences in the
  // perceptor code).
  if(obstacleDataConfidence == NO_DATA || obstacleDataConfidence < 0)
    obstacleDataConfidence = 0;
  else
    // Make sure confidence is not too large and if it is cap it.
    obstacleDataConfidence = std::min(obstacleDataConfidence, MAX_OBJECT_CONF);

  encodeData(obstacleDataConfidence, OBJECT_CONFIDENCE_SHIFT, OBJECT_CONFIDENCE_MASK);
}
  
void CellData::setRoughness(int roughness)
{
  // Set the specific no data flag if the generic one is given.
  if(roughness == NO_DATA)
    roughness = NO_ROUGHNESS_DATA;
  // Make sure the data is in the appropriate range.
  else if(roughness < 0)
    roughness = NO_ROUGHNESS_DATA;
  else
    roughness = std::min(roughness, MAX_ROUGH_DATA);

  encodeData(roughness, ROUGHNESS_DATA_SHIFT, ROUGHNESS_DATA_MASK);
}


void CellData::setRoughnessConfidence(int roughnessConfidence)
{ 
  // If the generic NO_DATA flag is set, give a confidence of 0 (as there isn't a no data flag for confidences in the
  // perceptor code).
  if(roughnessConfidence == NO_DATA || roughnessConfidence < 0)
    roughnessConfidence = 0;
  // Make sure confidence is not too large and if it is cap it.
  else
    roughnessConfidence = std::min(roughnessConfidence, MAX_ROUGH_CONF);

  encodeData(roughnessConfidence, ROUGHNESS_CONFIDENCE_SHIFT, ROUGHNESS_CONFIDENCE_MASK);
}



// Accessors
int CellData::getElevation() const 
{ 

  /* elevation sign bit is 1 for negative elevation */
  /* elevation sign bit is 0 for positive elevation */
  int elevationSignBit = decodeData(ELEVATION_SIGN_MASK, ELEVATION_DATA_SHIFT);
  int elevationSign = elevationSignBit ? -1 : 1;

  int elevationData = decodeData(ELEVATION_DATA_MASK, ELEVATION_DATA_SHIFT);

  // If there is no elevation data return the generic NO_DATA flag.
  if(elevationData == NO_ELEVATION_DATA_FLAG)
    {
      elevationData = NO_DATA;
      elevationSign = 1;
    }

  return elevationSign * elevationData; 
 }

int CellData::getElevationConfidence() const 
{
  return decodeData(ELEVATION_CONFIDENCE_MASK, ELEVATION_CONFIDENCE_SHIFT);
}

int CellData::getClassification() const
{
  int classificationData = decodeData(CLASSIFICATION_DATA_MASK, CLASSIFICATION_DATA_SHIFT);

  // If there is no classification data return the NO_DATA flag.
  if(classificationData == NO_CLASSIFICATION_DATA)
    classificationData = NO_DATA;

  return classificationData;
}

int CellData::getClassificationConfidence() const
{
  return decodeData(CLASSIFICATION_CONFIDENCE_MASK, CLASSIFICATION_CONFIDENCE_SHIFT);
}

int CellData::getObstacleData() const
{
  int obstacleData = decodeData(OBJECT_DATA_MASK, OBJECT_DATA_SHIFT);

  // If there is no classification data return the NO_DATA flag.
  if(obstacleData == NO_OBSTACLE_DATA)
    obstacleData = NO_DATA;

  return obstacleData;
}

int CellData::getObstacleDataConfidence() const
{
  return decodeData(OBJECT_CONFIDENCE_MASK, OBJECT_CONFIDENCE_SHIFT);
}

int CellData::getRoughness() const
{
  int roughness = decodeData(ROUGHNESS_DATA_MASK, ROUGHNESS_DATA_SHIFT);

  // If there is no classification data return the NO_DATA flag.
  if(roughness == NO_ROUGHNESS_DATA)
    roughness = NO_DATA;

  return roughness;
}

int CellData::getRoughnessConfidence() const
{
  return decodeData(ROUGHNESS_CONFIDENCE_MASK, ROUGHNESS_CONFIDENCE_SHIFT);
}

int CellData::getTimeLastModified() const 
{ 
  return _timeLastModified; 
}

int CellData::getGoodness() const
{
  return _goodness;
}

int CellData::getCellData() const
{
  return _cellData;
}


// Encodes n into '_cellData' using 'mask' and 'shift'.
void CellData::encodeData(int n, unsigned int shift, unsigned int mask)
{
  setCellData( (( ~mask & getCellData() )) | ((n << shift) & mask) );
}

// Returns the data stored in '_cellData' with given 'mask' and 'shift'.
int CellData::decodeData(unsigned int mask, unsigned int shift) const
{
  int temp = ((getCellData() & mask) >> shift);
  return temp;
}

// Takes the data from 'other' and combines it with the data already in the cell.  The data is combined using
// a weighted average based on the confidence for elevation and roughness, and is given the value
// with the highest confidence for obstacles and classification.  This function should be hand tweaked to produce
// the best results (i.e. weighted average may not be the best system, etc.)
void CellData::updateData(const CellData &other)
{
  // Expire both this cell and 'other' data to the time of the newest cell.
  int newestTime = std::max(getTimeLastModified(), other.getTimeLastModified());
  CellData expiredThis = expireData(newestTime);
  CellData expiredOther = other.expireData(newestTime);

  // Udate the 'timeLastModified' so it contains the value of the newest of the two cells.
  setTimeLastModified(newestTime);

  // Update the elevation
  // If only one cell has elevation data than use that cells data/confidence.
  if( expiredThis.getElevation() == NO_DATA )
    {
      setElevation( expiredOther.getElevation() );
      setElevationConfidence( expiredOther.getElevationConfidence() );
    }
  else if( expiredOther.getElevation() == NO_DATA )
    {
      setElevation( expiredThis.getElevation() );
      setElevationConfidence( expiredThis.getElevationConfidence() );
    }
  // Otherwise, find the new elevation as a weighted average based on confidence.
  else
    {
      double thisConfidenceRatio = 
	expiredThis.getElevationConfidence() / ( expiredThis.getElevationConfidence() + expiredOther.getElevationConfidence() );
      double otherConfidenceRatio = 
	expiredOther.getElevationConfidence() / ( expiredThis.getElevationConfidence() + expiredOther.getElevationConfidence() );
      int newElevation = 
	int(expiredThis.getElevation() * thisConfidenceRatio + expiredOther.getElevation() * otherConfidenceRatio);

      setElevation( std::min(newElevation, MAX_ELEV_DATA) );

      // Find the new elevation confidence.  This should be higher than the max of the other two confidence's but
      // it is not clear how to combine the two confidences.  For now just add them (making sure not to exceed the maximum).
      setElevationConfidence( std::min(getElevationConfidence() + other.getElevationConfidence(), 
				       MAX_ELEV_CONF) );
    } 

  // Update the classification
  // If only one cell has classification data than use that cells data/confidence
  if( expiredThis.getClassification() == NO_DATA )
    {
      setClassification( expiredOther.getClassification() );
      setClassificationConfidence( expiredOther.getClassificationConfidence() );
    }
  else if( expiredOther.getClassification() == NO_DATA )
    {
      setClassification( expiredThis.getClassification() );
      setClassificationConfidence( expiredThis.getClassificationConfidence() );
    }
  // Otherwise use the classification data from the cell with the higher confidence.
  else
    {
      setClassification( ( expiredThis.getClassificationConfidence() > expiredOther.getClassificationConfidence() ) 
			 ? expiredThis.getClassification()
			 : expiredOther.getClassification() );
	
      // If they disagreed on classifcation than the new confidence should be lower than both of the previous confidences
      // and if they agreed it should be higher.  
      // It is not clear how best to combine the data.  For now just add 1 to the highest confidence or subtract 1 from 
      // the lowest appropriately.
      setClassificationConfidence( ( expiredThis.getClassification() == expiredOther.getClassification() )
				   ? std::max( std::max(expiredThis.getClassificationConfidence() + 1, 
							expiredOther.getClassificationConfidence() + 1),
					       MAX_CLASS_CONF )
				   : std::min( std::min(expiredThis.getClassificationConfidence() - 1,
							expiredOther.getClassificationConfidence() - 1),
					       0 ));
    }

  // Update the obstacle data.
  // If only one cell has obstacle data than use that cells data/confidence
  if( expiredThis.getObstacleData() == NO_DATA )
    {
      setObstacleData( expiredOther.getObstacleData() );
      setObstacleDataConfidence( expiredOther.getObstacleDataConfidence() );
    }
  else if( expiredOther.getObstacleData() == NO_DATA )
    {
      setObstacleData( expiredThis.getObstacleData() );
      setObstacleDataConfidence( expiredThis.getObstacleDataConfidence() );
    }
  else
    {
      // If either cell registers an obstacle take the safe approach and say there is an obstacle regardless of confidence.
      if( expiredThis.getObstacleData() == POSITIVE_OBSTACLE ||
	  expiredOther.getObstacleData() == POSITIVE_OBSTACLE )
	setObstacleData( POSITIVE_OBSTACLE );
      else if( expiredThis.getObstacleData() == NEGATIVE_OBSTACLE ||
	       expiredOther.getObstacleData() == NEGATIVE_OBSTACLE )
	setObstacleData( NEGATIVE_OBSTACLE );
      else
	setObstacleData( NO_OBSTACLE );

      // If they disagreed on obstacle than the new confidence should be lower than both of the previous confidences
      // and if they agreed it should be higher.  
      // It is not clear how best to combine the data.  For now just add 1 to the highest confidence or subtract 1 from 
      // the lowest appropriately.
      setObstacleDataConfidence( ( expiredThis.getObstacleData() == expiredOther.getObstacleData() )
				 ? std::max( std::max(expiredThis.getObstacleDataConfidence() + 1, 
						      expiredOther.getObstacleDataConfidence() + 1),
					     MAX_OBJECT_CONF )
				 : std::min( std::min(expiredThis.getObstacleDataConfidence() - 1,
						      expiredOther.getObstacleDataConfidence() - 1),
					     0 ));
					     
    }

  // Update the roughness data
  // If only one cell has roughness data than use that cells data/confidence
  if( expiredThis.getRoughness() == NO_DATA )
    {
      setRoughness( expiredOther.getRoughness() );
      setRoughnessConfidence( expiredOther.getRoughnessConfidence() );
    }
  else if( expiredOther.getRoughness() == NO_DATA )
    {
      setRoughness( expiredThis.getRoughness() );
      setRoughnessConfidence( expiredOther.getRoughnessConfidence() );
    }
  // Otherwise, since there is currently no confidence stored for roughness, just average the two values of roughness.
  else
    {
      setRoughness( ( expiredThis.getRoughness() + expiredOther.getRoughness() ) / 2 );
    } 

  //Finally, the resulting goodness must've changed, so set the goodness to unknown
  if(getObstacleData()!=NO_DATA) {
    setGoodness(250*(1-getObstacleData()));
  } else {
    setGoodness(NO_DATA);
  }
}

// As data gets older, it's confidence should decrease.  This function returns a new 'CellData' object with all data the same
// except a lower confidence and a new time last modified.
// The function is currently written so 'timeLastModified' and 'newTime' should be given in ?? units.
CellData CellData::expireData(int newTime) const
{
  // Expire confidences, this currenlty works by decreasing confidence by one every 5 seconds.
  int newElevationConfidence =  getElevationConfidence(); //std::max(getElevationConfidence() - int( (getTimeLastModified() - newTime) / 5000 ),
  //				-1);
  int newClassificationConfidence = getClassificationConfidence();
  //std::max(getClassificationConfidence() - int( (getTimeLastModified() - newTime) / 5000 ),
  //					     -1);
  int newObstacleDataConfidence = getObstacleDataConfidence();
  //std::max(getObstacleDataConfidence() - int( (getTimeLastModified() - newTime) / 5000 ),
  //				   -1);
  int newRoughnessConfidence = getRoughnessConfidence();
  //std::max(getRoughnessConfidence() - int( (getTimeLastModified() - newTime) / 5000 ),
  //				-1);


  // Return a cell with the new confidencess.
  CellData newCell( ( newElevationConfidence == -1 ? NO_DATA : getElevation() ), 
		    std::max(0,newElevationConfidence), 
		    ( newClassificationConfidence == -1 ? NO_DATA : getClassification() ), 
		    std::max(0,newClassificationConfidence),
		    ( newObstacleDataConfidence == -1 ? NO_DATA : getObstacleData() ), 
		    std::max(0,newObstacleDataConfidence),
		    ( newRoughnessConfidence == -1 ? NO_DATA : getRoughness()), 
		    std::max(0,newRoughnessConfidence),
		    newTime );
  return newCell;
}

// Copies other to this.
void CellData::copy(const CellData &other)
{
  setCellData( other.getCellData() );
  setTimeLastModified( other.getTimeLastModified() );
  setGoodness( other.getGoodness() );
}

// Deallocates any memory and resets all values of this. 
void CellData::destroy()
{
  setElevation(NO_DATA);
  setElevationConfidence(NO_DATA);

  setClassification(NO_DATA);
  setClassificationConfidence(NO_DATA);

  setObstacleData(NO_DATA);
  setObstacleDataConfidence(NO_DATA);

  setRoughness(NO_DATA);
  setRoughnessConfidence(NO_DATA);

  setGoodness(NO_DATA);

  setTimeLastModified(NO_DATA);
}
