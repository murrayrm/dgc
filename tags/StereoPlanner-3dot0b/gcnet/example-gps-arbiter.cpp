#include "example-gps.h"
#include "GcnetConnection.hpp"
#include <assert.h>
#include <unistd.h>	/* for sleep() */
#include <iostream>

class GPSArbiterConnection : public GcnetConnection {
    // Handle an incoming GPS reading.
    virtual void handle_incoming(const void *data, unsigned length);
};

void GPSArbiterConnection::handle_incoming(const void *data, unsigned length)
{
  assert(length == sizeof(GPSReading));
  const GPSReading *reading = (const GPSReading *) data;

  // TODO: Pass data along to arbiter.
  std::cerr << "GPS: n=" << reading->n << ", e=" << reading->e << std::endl;
}

int main (int argc, char **argv)
{
  GPSArbiterConnection *connection;
  for (int i = 1; i < argc; i++)
    {
      std::cerr << "unexpected argument '" << argv[i] << "'" << std::endl;
      return 1;
    }
  connection = new GPSArbiterConnection();
  assert(connection->listen(GPS_PORT));
  for (;;)
    sleep(100000);
  return 0;
}
