#include "ReactionTime.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

double PHI[NUMARCS];

// These are command line options
extern int SIM;
extern unsigned long SLEEP_TIME;
extern double DESIRED_VELOCITY;
extern char *TESTNAME;

// These flags are set when the appropriate button is pressed
extern int FULLSTOP_VEHICLE;
extern int CRUISESTOP_VEHICLE;
extern int START_VEHICLE;
extern int QUIT_PRESSED;

// These are saved when the vehicle is told to stop
extern int StopTimesec;
extern int StopTimeusec;
extern double StopX;
extern double StopY;
extern double StopZ;

// These are variables used to store data for sparrow display
Voter reaction;

int UpdateCount = 0;
int TimeSec = 0;
int TimeUSec = 0;

double cmd_vel = 0.0;
double BrakeLevel = 0.0;
double VelocityX = 0.0;
double VelocityY = 0.0;
double VelocityZ = 0.0;
double Speed = 0.0;
double DistanceTravelled = 0.0;
double TimeElapsed = 0.0;

#include "vddtable.h"
int user_quit(long arg);
int start_vehicle(long arg);
int fullstop_vehicle(long arg);
int cruisestop_vehicle(long arg);

void ReactionTime :: UpdateSparrowVariablesLoop() {
  for(int i = 0; i < NUMARCS; i++) PHI[i] = GetPhi(i);

  reaction = d.reaction;

  TimeSec = d.SS.Timestamp.sec();
  TimeUSec = d.SS.Timestamp.usec();

  cmd_vel = d.toVDrive.velocity_cmd;

  VelocityX = d.SS.Vel_E;
  VelocityY = d.SS.Vel_N;
  VelocityZ = d.SS.Vel_U;
  Speed     = d.SS.Speed;

  if((FULLSTOP_VEHICLE == 0) && (CRUISESTOP_VEHICLE == 0)) {
  } else {
    if(fabs(d.SS.Speed) > DELTAV) {
      long Stop_usec = (StopTimesec * 1000000) + StopTimeusec;
      long Now_usec = (d.SS.Timestamp.sec() * 1000000) + d.SS.Timestamp.usec();
  
      TimeElapsed = (Now_usec - Stop_usec) / 1000000.0;
  
      double dx = d.SS.Easting - StopX;
      double dy = d.SS.Northing - StopY;
  //    double dz = d.SS.Altitude - StopZ;  // I think we can ignore this safely
  
      DistanceTravelled = sqrt(dx*dx + dy*dy);
    }
  }

  UpdateCount ++;
}

void ReactionTime :: SparrowDisplayLoop() {

  dbg_all = 0;

  if (dd_open() < 0) exit(1);
  dd_bindkey('Q', user_quit);
  dd_bindkey('S', start_vehicle);
  dd_bindkey('C', cruisestop_vehicle);


  dd_usetbl(vddtable);

  sleep(1); // Wait a bit, because other threads will print some stuff out
  dd_loop();
  dd_close();
  QUIT_PRESSED = 1;
}

int user_quit(long arg)
{
  return DD_EXIT_LOOP;
}
int start_vehicle(long arg)
{
  START_VEHICLE = 1;
}
int fullstop_vehicle(long arg)
{
  FULLSTOP_VEHICLE = 1;
}
int cruisestop_vehicle(long arg)
{
  CRUISESTOP_VEHICLE = 1;
}
