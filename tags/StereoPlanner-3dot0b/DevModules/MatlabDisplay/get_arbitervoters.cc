/*
** get_arbitervoters.cc
**
**  
** 
**
*/

#include "MatlabDisplay.hh" // standard includes, arbiter, datum, ...

void MatlabDisplay::UpdateArbiterVoters()
{ 
  Mail msg = NewQueryMessage( MyAddress(), MODULES::Arbiter, ArbiterMessages::GetVotes);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    Voter tmp; 
    for(int i=0; i< ArbiterInput::Count; i++) {
      reply >> _d.arbiterVoters[i];
    }

    // read in combined vote
    reply >> _d.combinedVote;

    // read in arbiter's command
    reply >> _d.arbiter_cmd;

    //printf(" Receiving d.toVDrive.velocity_cmd = %f\n", _d.arbiter_cmd.velocity_cmd );
    //printf(" Receiving d.toVDrive.steer_cmd = %f\n", _d.arbiter_cmd.steer_cmd );

  }

} // end UpdateState() 
