/*
 * schntest.c - simplest possible sparrow program (to check dependencies)
 *
 * Richard M. Murray
 * 9 August 1992
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "display.h"		/* dynamic display package */
#include "keymap.h"
#include "channel.h"
#include "servo.h"

/* Declare global variables for this module */
int channel_read_button(long), channel_write_button(long);
int channel_servo_button(long), channel_init_button(long);
int channel_capture_button(long), channel_dump_button(long);
int chntest_fcn_config(long), user_quit(long);

static int isr_rate= 200;   	/* servo interrupt rate */
static char dumpfile[20] = "chntest.dat";
static char cfgfile[20] = "config.dev";
double enc1, enc2;		/* encoder variables */

int chntest_chnid; /* variable for testing fcn_gen */
double chntest_frequency; /* variable for testing fcn_gen */
char dummystring[30];

/* Function declarations */
int channel_read_button(long);
int channel_write_button(long);
int channel_init_button(long);
int channel_capture_button(long);
int channel_dump_button(long);
int chntest_fcn_config(long);
int channel_servo_button();
int chntest_fcn_test();

#include "schntest.h"		/* menu definition */

/* Device driver lookup table */
extern int virtual_driver(DEV_ACTION, ...);
extern int fcn_driver(DEV_ACTION, ...);

DEV_LOOKUP chn_devlut[] = {
    {"virtual", virtual_driver},		/* virtual channels */
    {"function-gen", fcn_driver},		/* fcn_gen.c */
};

int main(int argc, char **argv)
{
  freopen("error.msg", "w", stderr); /* write errors to a file */

  /* Start up the display package and run everything from there */
  if (dd_open() < 0) {
    fprintf(stderr, "diag: can't initialize display\n");
    exit(1);
  }
  
  /* Define a few custom keys */
  dd_bindkey('Q', user_quit);
  dd_bindkey(K_F1, channel_servo_button);
  dd_bindkey(K_F2, channel_capture_button);
  dd_bindkey(K_F3, channel_dump_button);
  dd_bindkey(K_F4, channel_init_button);
  dd_bindkey('r', dd_redraw);
  dd_usetbl(chnmenu);		/* start off on the main menu */
  dd_loop();			/* start the display manager */
  dd_close();			/* close up the screen */
  
  printf("turning off servo\n");
  
  /* Reset the servo handler if it is running */
  servo_stop();
  return 0;
}

/* Globally defined callbacks */
int user_quit(long arg) { return DD_EXIT_LOOP; }

/*
 * Servo code
 *
 * This servo loop just reads and writes channel data
 *
 */
 
static void isr()
{
    chn_read();
    chn_write();
}

/*
 * Display callbacks
 *
 * These functions are all called by pressing buttons on the display
 *
 */

/* Dummy functions (should be defined in channel interface) */
int channel_read_button(long arg) { chn_read(); return 0; }
int channel_write_button(long arg) { chn_write(); return 0; }
int channel_init_button(long arg) { chn_config("config.dev"); return 0; }
int channel_capture_button(long arg) { chn_capture_on(); return 0; }
int channel_dump_button(long arg) { chn_capture_dump("diag.dat"); return 0; }
int chntest_fcn_config(long arg) { return 0; }


/* Start up a servo loop to read/write values */
int channel_servo_button(long arg)
{
    static int init = 0, running = 0;
    int status;

    /* Initialize the channel data if this is the first call */
    if (!init) { chn_config(cfgfile); init = 1; }

    /* Toggle the status of the servo routine */
    if (!running) {
	/* servo_sethandler(isr_fast_handler); */
        status = servo_setup(isr, isr_rate, SERVO_OVFL_ABORT);
	if (status <= 0) DD_PROMPT("chntest: couldn't start servo");

        status = servo_enable();
	if (status <= 0) DD_PROMPT("chntest: couldn't enable servo");
    } else {
        servo_disable();
    }

    running = !running;
    return 0;
}

int chntest_fcn_test(){
  return 0;
}
