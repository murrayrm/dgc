
#ifndef __YAW_PITCH_ROLL_2_MATRIX_H__
#define __YAW_PITCH_ROLL_2_MATRIX_H__


void yaw_pitch_roll_2_matrix(const double yaw, const double pitch, const double roll, double *x, double *y, double *z);

#endif
