#include "StateGiver.hh"
#include "../StateReceiver/StateReceiver.hh"
#include "Kernel.hh"
#include <time.h>
#include <iostream>

using namespace std;

extern Kernel K;
StateGiverDatum D;
//extern StateGiver testmodule;


void StateGiver::Init() {

  // do your init in here
  // i.e. open serial ports etc,

  cout << ModuleName() << ": Init" << endl;

  //D.MyCounter = 0;

  // go to active state after initialization
  SetNextState(STATES::ACTIVE);
}



void StateGiver::Active() {
  while( ContinueInState() ) {  // while stay active
    
    //get state data from shared memory
    
    Mail m = NewOutMessage(MODULES::StateReceiver, StateReceiverMessages::DATA);
    /*
    m << D.northing << D.easting << D.altitude 
      << D.vel_n << D.vel_e << D.vel_u 
      << D.heading << D.pitch << D.roll
      << D.speed << D.heading_n << D.heading_e
      << D.phi;
    */
    m << 0 << 0 << 0 
      << 0 << 0 << 0 
      << 0 << 0 << 0 
      << 0;
    K.SendMail(m);

  /*
    sleep(1); // increment the counter every second

    //D.MyCounter ++;
    //cout << ModuleName() << ": Active - Incrementing Counter = " << D.MyCounter << endl;
    //cout << "Sending New Out Message" << endl;
    Mail m = NewOutMessage(MODULES::Echo, StateGiverMessages::PAUSE);
    K.SendMail(m);
  */
  }
} 


void StateGiver::Standby() {
  /*
  int localCounter = 0;
  while( ContinueInState() ) {  // while stay active
    sleep(1); // dont increment the counter while in standby
    localCounter++;
    if(localCounter % 10 == 9) {
      //      cout << "Trying To Send Mail" << endl;
      Mail m = NewOutMessage(MODULES::Echo, StateGiverMessages::GO);
      K.SendMail(m);
    }

    //    D.MyCounter ++;
    //cout << ModuleName() << ": Standby - _Not_ Incrementing Counter = " << D.MyCounter << endl;
  }
  */
  SetNextState(STATES::ACTIVE);  
} 


void StateGiver::Shutdown() {
  // do whatever closing of files, etc
  cout << ModuleName() << ": Shutdown" << endl;

}

void StateGiver::Restart() {
  // you may want to do something better here, but i'll just call shutdown and init
  cout << ModuleName() <<": Restart" << endl;
  Shutdown();
  Init();
}

string StateGiver::Status() {
  string x = ModuleName() + ": is in state ";
  switch ( GetCurrentState() ) {
  case STATES::INIT:
    x+= "Init";       break;
  case STATES::ACTIVE:
    x+= "Active";     break;
  case STATES::SHUTDOWN:
    x+= "Shutdown";   break;
  case STATES::STANDBY:
    x+= "Standby";    break;
  case STATES::RESTART:
    x+= "Restart";    break;
  default:
    x+= "Unknown state - (Something is very wrong)";
  }
  return x;
}
 
void StateGiver::InMailHandler(Mail& ml) {

  switch(ml.MsgType()) {  // for mail functions check out Misc/Mail/Mail.hh

  case StateGiverMessages::REINIT:
    SetNextState(STATES::RESTART);
    break;

  case StateGiverMessages::SHUTDOWN:
    // force the Kernel to shutdown
    K.ForceShutdown();
    break;

  case StateGiverMessages::PAUSE:
    SetNextState(STATES::STANDBY);
    break;

  case StateGiverMessages::GO:
    SetNextState(STATES::ACTIVE);
    break;

   /*
  case StateGiverMessages::SETCOUNTER:
    // check to see that an int was attached to the message
    if( ml.Size() >= sizeof(int)) {
      int x;
      ml >> x;
      D.MyCounter = x;
    }
    break;
   */
  default:
    cout << ModuleName() << ": Unknown InMessage" << endl;
  }
}


Mail StateGiver::QueryMailHandler(Mail& msg) {
  // figure out what we are responding to

  Mail reply = msg; // A hack !!! dont do this :)
  int x;

  switch(msg.MsgType()) {  

  case StateGiverMessages::GETSTATE:
    reply = ReplyToQuery(msg);
    x = GetCurrentState();
    reply << x;
    break;

  default:
    // send back a "bad Message"
    reply = Mail( msg.From(), msg.To(), 0, MailFlags::BadMessage);
  }

  return reply;
}


