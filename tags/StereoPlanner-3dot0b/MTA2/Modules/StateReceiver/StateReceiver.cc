#include "StateReceiver.hh"

#include "Kernel.hh"
#include <time.h>
#include <iostream>

#include <stdlib.h>
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define STATE_KEY 117
#define STATE_SIZE 1024

using namespace std;

extern Kernel K;
StateReceiverDatum D;
//extern StateReceiver testmodule;


void StateReceiver::Init() {

  // do your init in here
  // i.e. open serial ports etc,

  cout << ModuleName() << ": Init" << endl;

  D.state_string = NULL;
  D.timestamp = 0.0;
  D.easting = 0.0;
  D.northing = 0.0;
  D.altitude = 0.0;
  D.roll = 0.0;
  D.pitch = 0.0;
  D.heading = 0.0;

  D.size = STATE_SIZE;
  D.key = STATE_KEY;

  D.shmflg = (IPC_CREAT | 0x1ff);
  if ((D.shmid = shmget(D.key, D.size, D.shmflg)) < 0) {
    cout << ModuleName() << ": DID NOT INIT PROPERLY - SHARED MEMORY ERROR";
    SetNextState(STATES::SHUTDOWN);
  } else {
    D.state_string = (char *) shmat(D.shmid, NULL, 0);

  //D.MyCounter = 0;

  // go to active state after initialization
  SetNextState(STATES::ACTIVE);
  }
}


void StateReceiver::Active() {
  double gpstime = 0.0;
  double pos_x = 0.0, pos_y = 0.0, pos_z = 0.0;
  double roll = 0.0, pitch = 0.0, yaw = 0.0;

  while( ContinueInState() ) {  // while stay active
    
    gpstime++;
    //gpstime = D.timestamp;
    pos_x = D.easting;
    pos_y = D.northing;
    pos_z = D.altitude;
    roll = D.roll;
    pitch = D.pitch;
    yaw = D.heading;
    
    sprintf(D.state_string,
	    "Pan: 0\n"
	    "Tilt: 0\n"
	    "gpstime %lf\n"
	    "rel.x %lf\n"
	    "rel.y %lf\n"
	    "rel.z %lf\n"
	    "rpy.r %lf\n"
	    "rpy.p %lf\n"
	    "rpy.y %lf\n", 
	    gpstime,
	    pos_x, pos_y, pos_z,
	    roll, pitch, yaw);
    printf(D.state_string);
    usleep(1000*1000);
  }
} 


void StateReceiver::Standby() {
  /*
  int localCounter = 0;
  while( ContinueInState() ) {  // while stay active
    sleep(1); // dont increment the counter while in standby
    localCounter++;
    if(localCounter % 10 == 9) {
      //      cout << "Trying To Send Mail" << endl;
      Mail m = NewOutMessage(MODULES::Echo, StateReceiverMessages::GO);
      K.SendMail(m);
    }

    //    D.MyCounter ++;
    cout << ModuleName() << ": Standby - _Not_ Incrementing Counter = " << D.MyCounter << endl;
  }
  */
} 


void StateReceiver::Shutdown() {
  // do whatever closing of files, etc
  cout << ModuleName() << ": Shutdown" << endl;

}

void StateReceiver::Restart() {
  // you may want to do something better here, but i'll just call shutdown and init
  cout << ModuleName() <<": Restart" << endl;
  Shutdown();
  Init();
}

string StateReceiver::Status() {
  string x = ModuleName() + ": is in state ";
  switch ( GetCurrentState() ) {
  case STATES::INIT:
    x+= "Init";       break;
  case STATES::ACTIVE:
    x+= "Active";     break;
  case STATES::SHUTDOWN:
    x+= "Shutdown";   break;
  case STATES::STANDBY:
    x+= "Standby";    break;
  case STATES::RESTART:
    x+= "Restart";    break;
  default:
    x+= "Unknown state - (Something is very wrong)";
  }
  return x;
}
 
void StateReceiver::InMailHandler(Mail& ml) {
  switch(ml.MsgType()) {  // for mail functions check out Misc/Mail/Mail.hh

  case StateReceiverMessages::REINIT:
    SetNextState(STATES::RESTART);
    break;

  case StateReceiverMessages::SHUTDOWN:
    // force the Kernel to shutdown
    K.ForceShutdown();
    break;

  case StateReceiverMessages::PAUSE:
    SetNextState(STATES::STANDBY);
    break;

  case StateReceiverMessages::GO:
    SetNextState(STATES::ACTIVE);
    break;

    /*
  case StateReceiverMessages::SETCOUNTER:
    // check to see that an int was attached to the message
    if( ml.Size() >= sizeof(int)) {
      int x;
      ml >> x;
      D.MyCounter = x;
    }
    break; 
    */
  case StateReceiverMessages::DATA: 
    ml >> D.timestamp
       >> D.northing >> D.easting >> D.altitude
       >> D.roll >> D.pitch >> D.heading;
    break;

  default:
    cout << ModuleName() << ": Unknown InMessage" << endl;
  }
}


Mail StateReceiver::QueryMailHandler(Mail& msg) {
  // figure out what we are responding to

  Mail reply = msg; // A hack !!! dont do this :)
  int x;

  switch(msg.MsgType()) {  

    /*
    case StateReceiverMessages::GETCOUNTER:
    reply = ReplyToQuery(msg);
    reply << D.MyCounter;
    break;
    */

  case StateReceiverMessages::GETSTATE:
    reply = ReplyToQuery(msg);
    x = GetCurrentState();
    reply << x;
    break;

  default:
    // send back a "bad Message"
    reply = Mail( msg.From(), msg.To(), 0, MailFlags::BadMessage);
  }

  return reply;
}


