#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream.h>
#include "serial.h"

int main(int argc, char **argv) {
  int serial_num=0;
  char *buffer_ptr;
  buffer_ptr=(char*)malloc(256);
  bool read_error=false;

  for(int i=0; i<argc; i++) {
    if(strncmp(argv[i], "-com=", 5)==0) sscanf(argv[i], "-com=%d", &serial_num);
  }

  if(serial_open(serial_num, B57600) != -1) {
    //Test blocking read
    cout << "\nReading...";
    serial_read_blocking(serial_num, buffer_ptr, 256);
    cout << "\nRead: ";
    for(int j=0; j<256; j++) {
      //Check to make sure all possible char byte values are there
      if(j!=(int)*(unsigned char*)(buffer_ptr+j)) {
	cout << j << ":" << (int)*(unsigned char*)(buffer_ptr+j) << ", ";
	read_error = true;
      }
    }

    serial_close(serial_num);

    if(read_error) {
      cout << "\nOutput Errors Above.\n";
    } else {
      cout << "\nInput successful.\n";
    }
  } else {
    cout << "\nError opening com " << serial_num << ".\n";
  }

  return 0;
}
