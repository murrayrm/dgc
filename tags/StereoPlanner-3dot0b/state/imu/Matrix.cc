/********************
Haomiao Huang
matrix class
Modify for use to generate rotation matrices
********************/

#include "Matrix.hh"
#include <iostream>
#include <cassert>

using namespace std;

// Constructors
Matrix::Matrix()  // create empty matrix
{
  rows = 0;
  cols = 0;
  data = NULL;
}

Matrix::Matrix(int r, int c)  // create r x c matrix
{
  rows = r;
  cols = c;
  
  int row = 0;
  int col = 0;

  assert(r >= 0); // check for negative
  assert(c >= 0);

  data = new double *[rows];  // point data at an array of pointers
  for (row= 0; row < rows; row++)
    {
      data[row] = new double [cols];
    }

  for (row = 0; row < rows; row++)
    {
      for (col = 0; col < cols; col++)
	{
	  data[row][col] = 0; // copy zeroes into data
	}
    }
}

Matrix::Matrix(const Matrix &matrix2) // copy constructor
{
  // make new matrix same size as matrix2
  rows = matrix2.getrows();
  cols = matrix2.getcols();
  
  int row, col;  // initialize counter variables

  data = new double *[rows];  // point data at an array of pointers
  for (row= 0; row < rows; row++)
    { 
      data[row]= new double [cols];
    }

  for (row= 0; row < rows; row++)
    {
      for (col = 0; col < cols; col++)
	{
	  data[row][col]= matrix2.getelem(row, col); // copy elements into data
	}
    }
}

// Mutators
// sets the rowth colth element the matrix to value
void Matrix::setelem(int row, int col, double value)  
{
  assert((row >= 0)&&(row < rows)); // check for negative and too large
  assert((col >= 0)&&(col < cols));
  data[row][col]= value; // set value
}

// Accessors
int Matrix::getrows() const  // returns the number of rows
{
  return rows;
}

int Matrix::getcols() const  // returns the number of columns
{
  return cols;
}

// returns the rowth, colth element of the matrix
double Matrix::getelem(int row, int col) const  
{
  assert((row >= 0)&&(row < rows)); // check for negative and too large
  assert((col >= 0)&&(col < cols));
  return data[row][col];
}

// operators
// compares with matrix2
bool Matrix::operator==(const Matrix &matrix2) const 
{
  int row, col;
  bool equal = true;
  
  // check if matrices are the same size
  if ((matrix2.getrows() == rows)&&(matrix2.getcols() == cols))
    {
      for (row= 0; row < rows; row++)
	{
	  for (col = 0; col < cols; col++)
	    {
	      if (data[row][col]!= matrix2.getelem(row, col))
		equal = false;  // if element is not equal return false
	    }
	}
    }
  else
    equal = false;

  return equal;
}

Matrix Matrix::operator+(const Matrix& matrix2) const
{
  assert((matrix2.getrows() == rows)&&(matrix2.getcols() == cols));
  // only add if matrices are the same size
  Matrix result; // create a copy of the current matrix
  result = *this;
  result += matrix2;  // add matrix2 to the copy
  return result;
}

Matrix Matrix::operator-(const Matrix &matrix2) const
{
  assert((matrix2.getrows() == rows)&&(matrix2.getcols() == cols));
  // only add if matrices are the same size
  Matrix result; // create a copy of the current matrix
  result = *this;
  result -= matrix2;  // subtract matrix2 from the copy
  return result;
}

Matrix& Matrix::operator+=(const Matrix &matrix2)
{
  int row, col;
  assert((matrix2.getrows() == rows)&&(matrix2.getcols() == cols));
  // only add if matrices are the same size
  for (row= 0; row < rows; row++)
    {
      for (col = 0; col < cols; col++)
	{
	  // add elements from matrix2
	  data[row][col]+= matrix2.getelem(row, col);  
	}
    }
  return *this;
}

Matrix& Matrix::operator-=(const Matrix &matrix2)
{
  int row, col;
  assert((matrix2.getrows() == rows)&&(matrix2.getcols() == cols));
  // only subtract if matrices are the same size
  
  for (row= 0; row < rows; row++)
    {
      for (col = 0; col < cols; col++)
	{
	  // subtract elements from matrix2
	  data[row][col]-= matrix2.getelem(row, col);
	}
    }

  return *this;
}

Matrix Matrix::operator*(const Matrix &matrix2) const
{
  /* Matrix multiplication:
  multiply ith row by jth column and sum results
  n x m * i x n = n x i */

  assert(cols == matrix2.getrows());
  // only multiply when you can

  Matrix result;  // create a copy of the matrix
  result = *this;
  result *= matrix2;  // multiply by matrix 2 and return the result
  return result;
}

Matrix& Matrix::operator*=(const Matrix &matrix2)
{
  /* Matrix multiplication:
  multiply ith row by jth column and sum results
  n x m * i x n = n x i */
  
  int r2 = matrix2.getrows();
  int c2 = matrix2.getcols();
  int row, col, col2; 
  double entry=0;

  // create result matrix n x i
  Matrix result(rows, c2); 

  assert(cols == r2); // only multiply when you can
  
  // run through each column of matrix2
  for(col2 = 0; col2 < c2; col2++)
    {
      // multiply by each row of the original
      for (row = 0; row < rows; row++)
	{
	  // within each i x j multiply each entry
	  for(col = 0; col < cols; col++)
	    {
	      entry += data[row][col] * matrix2.getelem(col, col2);
	    }
	  result.setelem(row, col2, entry); // set value
	  entry = 0;
	}
    }

  *this = result;
  
  return *this;
}

bool Matrix::operator!=(const Matrix &matrix2) 
{
  return !(*this == matrix2); // check for not equal
}

Matrix& Matrix::operator=(const Matrix &matrix2)
{
  int row, col;
  if (*this == matrix2)
    return *this;

  // clear out data pointer first
  for (row= 0; row < rows; row++)
    delete [] data[row];

  delete [] data;

  // make array same size as new array
  rows = matrix2.rows;
  cols = matrix2.cols;

  data = new double *[rows];  // point data at an array of pointers
  for (row= 0; row < rows; row++)
    {
      data[row]= new double [cols];
    }

  for (row= 0; row < rows; row++)
    {
      for (col = 0; col < cols; col++)
	{
	  data[row][col]= matrix2.getelem(row, col); // copy elements into data
	}
    }

  return *this;
}

// Destructors
Matrix::~Matrix()
{
  for (int row= 0; row < rows; row++)
    {
      delete [] data[row];
    }
  delete [] data;
}
