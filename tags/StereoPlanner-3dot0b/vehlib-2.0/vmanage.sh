#!/bin/csh
# 
# vmanage.sh - startup script for vmanage
# RMM, 27 Feb 04
#
# This script starts up vmanage on magnesium and restarts it if it exits
# unexpectedly.  This contains an absolute path to vmanage so that it
# can be run from /etc/rc.d.
#
# The script tries to be smart about whether it is running on a terminal
# or not and doesn't request input unless it is running on a terminal
#
# Usage:
#   vmanage flags [&]
#
# Environment variables:
#   DGC		path to DGC executables
#   VMFLAGS	optional flags to pass to vmanage
#

# Set up process limits to allow core files to be generated
limit coredumpsize unlimited

# Make sure we have a path to the file
if (! $?DGC) set DGC=`pwd`
if (! $?VMFLAGS) set VMFLAGS=""
if (! -e $DGC/vmanage) then
  echo "FATAL: can't find vmanage in $DGC"
  exit
endif

# Print out a banner so that we know where we are running
echo "vmanage@`hostname`: pwd = `pwd`; VMFLAGS = $VMFLAGS";

# Check to make sure we are on the right machine
if (`hostname` != "magnesium") then
  echo "WARNING: not running on magnesium"
  if ($tty != "") then
      echo -n "Continue (y/n)? ";
      if ($< != "y") exit
  endif
endif

# Check to make sure that vmanage is not already running
set VLIST = "`ps -aux | grep vmanage | grep -v vmanage.sh`"
if ("$VLIST" != "") then
  echo "WARNING: vmanage already running on this machine:"
  echo "  $VLIST"
  if ($tty != "") then
      echo -n "Kill/Continue/Abort (k/c/a)? ";
      set response = $<;
      if ($response == "a") exit
  endif

  # Kill all running copies of vmanage
  if ($response == "k") then
    if (`killall -9 vmanage` != "") then
      echo "WARNING: couldn't kill all copies of vmanage"
      if ($tty != "") then
        echo -n "Continue (y/n)? ";
        if ($< != "y") exit
      endif
    endif
  endif
endif

# Run vmanage and restart it whenever it crashes
while (1) 
  echo "======== starting vmanage $VMFLAGS $argv ========"
  $DGC/vmanage $VMFLAGS $argv
  echo "vmanage aborted"

  # kill anything that is left over
  killall -9 vmanage >& /dev/null

  sleep 2	# sleep for a few seconds to let things die
end
