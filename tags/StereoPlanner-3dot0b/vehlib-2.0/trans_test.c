#include <iostream.h>
#include <stdio.h>
#include <time.h>
#include <fstream.h>
#include <sys/time.h>
#include <unistd.h>
#include "vdrive.h"
#include "vehports.h"
#include "transmission.h"

int main(void) {
  cout << "Transmission and ignition test program, press any key to continue";
  getchar();
  trans_open(TRANS_PORT);
  trans_setposition(NEUTRAL);
  cout << "NEUTRAL, press a key";
  getchar();
  //  pause(2000);
  trans_setposition(FIRST);
  cout << "FIRST, press a key";
  getchar();
  //  pause(2000);
  trans_setposition(THIRD);
  cout << "THIRD, press a key";
  getchar();
  //  pause(2000);
  trans_setposition(REVERSE);
  cout << "REVERSE, press a key";
  getchar();
  //  pause(2000);
  return 0;
}
