#include "ladar_power.h"

int ladar_pp_init(){
  //set up the parallel port with data output
  //  pp_end(LADAR_PPORT); //do we need to do this?  will it die if 
  //we kill it and it is not already open?
  
  if( pp_init(LADAR_PPORT, PP_DIR_OUT) == 0 ){ //if we can open the port 
    ladar_on(LADAR_BUMPER);
    ladar_on(LADAR_ROOF);
    return 0;
  }
  else return -1;
}

int ladar_on(int ladar_number){
  //put ladar_number low -> pin is low, output of box is high, relay is on
  return(pp_set_bits(LADAR_PPORT, ladar_number, 0));
}

int ladar_off(int ladar_number){
  //put ladar_number high-> pin high... relay off
  return(pp_set_bits(LADAR_PPORT, 0, ladar_number));
}
