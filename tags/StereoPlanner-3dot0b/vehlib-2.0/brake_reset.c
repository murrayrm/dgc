/*
 * brake_reset.c - program to reset the gas spring
 *
 * RMM, 7 Mar 04
 *
 * This program is based on braketest.  Use it to reset the gas spring
 * position.
 */
#include <stdlib.h>
#include <pthread.h>
#include "brake2.h"
#include "vehports.h"
#include "parallel.h"
#include "sparrow/dbglib.h"

pthread_t brake_thread;			/* brake thread */
int brake_flag = 0;			/* enabled flag */
extern int brake_case;

main(int argc, char **argv)
{
  char inpbuf[100];
  double pos;

  /* Turn on debugging */
  dbg_flag = dbg_outf = dbg_all = 1;

  /* Open up brake/throttle parallel port */
  pp_init(PP_GASBR, PP_DIR_IN);

# ifdef BRAKE_OLD
  brake_open(5);
# else
  brake_open(6);
# endif
  /* brake_calibrate(); */

      /* 
       * Reset gas spring 
       *
       * This command uses the brakepot to reset the gas spring.  It
       * commands a slow velocity and then monitors the pot until that 
       * position is reached.  Basically the same functionality as 
       * brake_home(), except using different pot settings.
       */
#     define MAX_RESET_COUNT 100
#     define RESET_CYCLE_USEC 100000
#     define BRAKE_RESET_POS 0.75
#     define BRAKE_RESET_VEL 1000000
      int count = 0;			/* counter to avoid hanging */
      double curpos;

      brake_read(&curpos); printf("Current position = %g\n", curpos);
      printf("Move brake to %g (y/n)? ", BRAKE_RESET_POS);
      scanf("%s", inpbuf);
      if (inpbuf[0] != 'y') exit(0);

      while (count++ < MAX_RESET_COUNT) {
	int tries = 0;
	if (brake_read(&curpos) < 0) { printf("can't read position"); break; }

	if (curpos < BRAKE_RESET_POS) {
	  /* Set the velocity of the brake to a positive value */
	  brake_move_vel(BRAKE_RESET_VEL);
	  while (brake_read(&curpos) >= 0 && curpos < BRAKE_RESET_POS &&
		 tries++ < 100) {
	    usleep(BRAKE_CYCLE_USEC);	/* let the brake move a bit */
	  }
	  break;			/* done moving */
	} else {
	  brake_move_vel(-BRAKE_RESET_VEL);
	  /* Set the velocity of the brake to a positive value */
	  while (brake_read(&curpos) >= 0 && curpos > BRAKE_RESET_POS &&
		 tries++ < 100) {
	    usleep(BRAKE_CYCLE_USEC);	/* let the brake move a bit */
	  }
	  break;			/* done moving */
	}
      }
      /* Set the velocity to zero */
      brake_move_vel(0);

      /* Wait util the spring is deployed and move back */
      printf("Brake at %g; release (y/n)? ", curpos);
      scanf("%s", inpbuf);
      if (inpbuf[0] != 'y') exit(0);

      /* Do a simple move */
      brake_move_vel(-1000000);
      sleep(1);
      brake_move_vel(0);

      printf("Brake at %g; press return to exit\n", curpos);
      scanf("%s", inpbuf);

  brake_close();
  return 0;
}
