#ifndef IMU_H
#define IMU_H

typedef struct imu_data {
  double dvx;
  double dvy;
  double dvz;
  double dtx;
  double dty;
  double dtz;
} IMU_DATA;

int IMU_open();
int IMU_close();
int IMU_ok();
int IMU_zero();
int IMU_pause();
int IMU_resume();
int IMU_disable();
int IMU_init();
int IMU_read(IMU_DATA *);


#endif
