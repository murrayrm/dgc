/*
 * serial2.cc - serial port access
 *
 * Johnson Liu, 18 Jun 03
 * Jeremy Gilula, Will Coulter, Sue Ann Hong, Richard Murray
 *
 * This file containes the serial port interface routines for the vehicle
 * library.  This functions are used by all drives that communicate via
 * serial ports.  The following functions are defined:
 *
 * serial_open		 open up a serial port for reading and writing
 * serial_open_advanced  advanced version w/ additional input options
 * serial_read		 read data from a serial port (non-blocking)
 * serial_read_blocking	 wait until data is available before returning
 * serial_read_until	 read until a specified character is read
 * serial_write		 write data to a serial port
 * serial_close		 close serial port
 *
 * Change history
 * --------------
 * 18 Jun 03, JL: Initial Creation 
 *
 * 19 Jun 03, JL: Got the code working on /dev/ttyS0
 *
 * 20 Jun 03, JL: Got the code working on /dev/ttyS1, doesn't work 
 *  simultaneously on ttyS0 and ttyS1
 *
 * 23 Jun 03, JG: 
 *   * work simulatneously on ttyS0 and ttyS1. Finish README. 
 *
 * 18 Jul 03, JG?
 *   * Eliminated 8th-bit cutoff error, added a serial_read_blocking function
 *
 * Aug 03?, WC
 *   * This is wholly not-up-to-date. Look at the CVS revision log
 *
 * 13 Sep 03, WC:  serial_read_blocking now takes a timeout factor
 *
 * 13 Sep 03, SAH: 
 *   * changed serial_read_until to use timeval for better precision in time
 *
 * 14 Sep 03 , SAH:
 *   * serial_read_until tested and works. segfaulted once.
 *   * won't do it anymore so can't debug. tell me if it happens again.
 *
 * 29 Dec 03, RMM: conversion for vehlib library
 *   * removed text output; replace with dbg_* calls
 * 
 */

#ifdef SERIAL2

#include <time.h>
#include <iostream>
#include "serial2.hh"
#include <string>
#include <strstream>
#include <signal.h>
#include <unistd.h>
#include <sys/poll.h>


#include "MTA/Kernel.hh"

using namespace std;
typedef boost::recursive_mutex::scoped_lock Lock;

SerialPortHandle sp[MaxSerialPorts];

boost::recursive_mutex SerialDaemonLock;
int SPDaemonStarted = false;

// Serial Daemon Function Prototypes (should never be called by user)
int DaemonOpenPort(int com, int BaudRate, int options);
int DaemonClosePort(int com);

// Serial Daemon Prototypes
void RXThread(void*);      // Send and receive threads.
void TXThread(void*); 
void SerialManager(void*); // opens and closes serial ports

// PIDs of Serial Port Daemons
pid_t RXPID = -1;
pid_t TXPID = -1;
pid_t ManagerPID = -1;


int SERIAL_DEBUG = false;

// Serial Daemon Starter
void StartSPDaemons() {

  Lock localLock(SerialDaemonLock);
  if( SPDaemonStarted == false ) {
    RunFunctionInNewThread( RXThread, NULL);
    RunFunctionInNewThread( TXThread, NULL);
    RunFunctionInNewThread( SerialManager, NULL);
    SPDaemonStarted = true;
  }
}



// Signal handler -- does nothing but helps us wake up threads
void null_signal_handler( int sig ) {
  signal( sig, &null_signal_handler);
} 



/*  serial_open: opens a serial port (ttyS0 to ttyS9) for reading and writing.
 *  RETURN: -1 if an error occurs; 0 otherwise.
 *  com: serial port number
 *  BaudRate: see serial.h for baud rates
 */
int serial_open(int com, int BaudRate, CallbackFunction f)
{
  return serial_open_advanced(com, BaudRate, 0, f); // options = 0
}


/* serial_write: write to a com port. If the com port is not open,
 * it will return an error.  RETURN: length of data
 * written. -1 if an error occurs. 
 */
int serial_write(int com, char *buffer, int length)
{

  //  if( SERIAL_DEBUG ) cout << "Serial Write Called With Com = " << com << endl;

  // Step 1. Check Arguments -- COM Port, buffer and Arg Length 
  // Make sure we aren't passed garbage 
  if (com < 0 || com >= MaxSerialPorts ||
      buffer == NULL ||
      length < 0) {
    cout << "Serial Write: Argument Error: Com=" << com 
	 << ", Buffer=" << buffer << ", length=" << length << endl;
    return -1;
  }
  
  // Step 2.  Make sure the serial port is open and we are using it
  Lock myLock(sp[com].mutex); // lock the serial port handle

  if(sp[com].state != SPStates::Open ||
     sp[com].use == false) {
    cout << "Serial Write: Serial Port Error: State=" << sp[com].state 
	 << ", use=" << sp[com].use << endl;
    return -1;
  }

  // otherwise, just enqueue the data to send.
  sp[com].TX.push(buffer, length);
  // and send a kill signal to the sending thread
  Lock tl(SerialDaemonLock);
  if( TXPID >= 0 ) kill(TXPID, SIGUSR2);

  return length;
}


/* serial_close: closes serial port number com
 * RETURN: -1 if an error occurs; 0 otherwise.
 */

int serial_close(int com)
{
  if( SERIAL_DEBUG ) cout << "Serial Close Called With Com = " << com << endl;

  // Step 1. Check Arguments -- COM Port 
  if (com < 0 || com >= MaxSerialPorts) {
    cout << "Serial Close: Argument Error: Com=" << com << endl;
    return -1;
  }

  // else, flag the serial port to be closed.
  Lock mylock( sp[com].mutex);
  // tell the serial daemons to stop using the serial port
  sp[com].use = false;  

  return 0;
}

/* Delete the contents of the buffer for specified com number */
int serial_clean_buffer(int com)
{

  if( SERIAL_DEBUG ) cout << "Serial Clean Buffer Called With Com = " << com << endl;

  // Step 1. Check Arguments -- COM Port 
  if (com < 0 || com >= MaxSerialPorts) {
    cout << "Serial Clean Buffer: Argument Error: Com=" << com << endl;
    return -1;
  }

  // else, lock the handle and dump the buffer.
  Lock mylock( sp[com].mutex);
  sp[com].RX.dump();  

  return 0;
}


/* return the length of the available data in the buffer */ 
int serial_data_length(int com)
{
  if( SERIAL_DEBUG ) cout << "Serial Data Length Called With Com = " << com << endl;

  // Step 1. Check Arguments -- COM Port 
  if (com < 0 || com >= MaxSerialPorts) {
    cout << "Serial Data Length: Argument Error: Com=" << com << endl;
    return -1;
  }

  // else, lock the handle and check the buffer.
  Lock mylock( sp[com].mutex);
  return sp[com].RX.size();  

}










/*
 *  serial_open_advanced
 *
 *  Opens a serial port and allow more setting for the serial port.
 *  Options include: set parity, flow control, and stop bits. 
 *  Note: Even if you don't set stop bit, there is still one stop bit.
 *
 *
 *  com: serial port you want to open
 *  BaudRate: baud rate 
 *  options: set parity, flow control, and stop bits. 
 *           See serial.h for valid options.
 */

int serial_open_advanced(int com, int BaudRate, int options, CallbackFunction f)
{
  if( SERIAL_DEBUG ) cout << "Serial Open Advanced Called With Com = " << com << endl;

  // DO LOTS OF ERROR CHECKING FIRST
  // 1. Make sure COM is OK
  // 2. Check Parity Settings
  // 3. Check Stop Bits
  // 4. Check Hardware Flow Control
  // 5. Check Software Flow Control
  // 6. Make Sure the Serial Port Threads are started

  // ********* THEN ********
  // 7. Pass the Open Port Info to the serial port daemons
  // 8. Wait till the Server daemons respond saying everything is OK or an error occurred.



  // Step 1. Check COM Port
  /* Make sure we aren't passed garbage */
  if (com < 0 || com >= MaxSerialPorts) {
    cout << "Serial Open Advanced: Error Com port out of range: " << com << endl;
    return -1;
  }

  // Step 2.
  /* Check Parity Options */ 
  if((options & SR_PARITY_DISABLE) && ((options & SR_PARITY_EVEN) || (options & SR_PARITY_ODD)))
  {
    cout << "Serial Open Advanced: Cannot disable and enable parity at the same time.\n";
    return -1;
  }
  else if ((options & SR_PARITY_EVEN) && (options & SR_PARITY_ODD))
  {
    cout << "Serial Open Advanced: Cannot set parity to even and odd at the same time.\n";
    return -1;
  }

  // Step 3. Check Stop Bits
  if((options & SR_ONE_STOP_BIT) && (options & SR_TWO_STOP_BIT))
  {
    cout << "Serial Open Advanced: Cannot set stop bit to one and two at the same time.\n";
    return -1;
  }

  // Step 4.
  /* Check hardware flow control*/
  if((options & SR_HARD_FLOW_DISABLE) && (options & SR_HARD_FLOW_ENABLE))
  {
    cout << "Serial Open Advanced: Cannot disable and enable hardware flow control at the same time\n";
    return -1;
  }
  
  // Step 5.
  /* check software flow control*/
  /* Caution : if you use flow control, you won't receive bytes with values of 17 and 19*/
  if((options & SR_SOFT_FLOW_DISABLE) && ((options & SR_SOFT_FLOW_INPUT) || (options & SR_SOFT_FLOW_OUTPUT)))
  {
    cout << "Serial Open Advanced: Cannot disable and enable software flow control at the same time\n";
    return -1;
  }


  // Step 6. Start the Serial Port Server Threads (this will only start them if they havent already)
  StartSPDaemons(); 
  
  // Step 7. Pass the open command to the serial port daemons
  {
    Lock myLock(sp[com].mutex); // lock the serial port so we can change it's options
    sp[com].use     = true;     // Set the flags using the handle.
    sp[com].baud    = BaudRate;
    sp[com].options = options; 

    signal( SIGUSR2, null_signal_handler );
    sp[com].wakePID = getpid();
    sp[com].callback= f;
  } // serial port released here
  
  // Step 8. Wait for a return value (via the handle).
  int returnValue = 1;
  while(1) {
    sleep(1);
    {
      Lock myLock(sp[com].mutex); // lock the serial port so we can check it's status
      if( sp[com].state == SPStates::Open ) return 0;
      else if( sp[com].state == SPStates::Error ) return -1;
    } // serial port unlocked automatically here
  }
  
}







/* serial_read:	Read from receive queue. If the com port is not open, 
 *  		it will return an error.
 * RETURN:	Length of data read from serial, -1 if an error occurs
 */
int serial_read(int com, char *buffer, int length)
{

  // if( SERIAL_DEBUG ) cout << "Serial Read Called With Com = " << com << endl;

  /* Make sure we aren't passed garbage */
  if (com < 0 || com >= MaxSerialPorts) {
    cout << "Serial Read: Error Com port out of range: " << com << endl;
    return -1;
  }
  if ( length < 0) {
    cout << "Serial Read: Length to read is negative " << length << endl;
    return -1;
  }


  // else, lock the port handle
  Lock myLock (sp[com].mutex);

  // see if the serial port is actually open 
  if( sp[com].state != SPStates::Open ) {
    cout << "Serial Read: Error - Trying to read from closed com port" << com << endl;
    return -1;
  }
  if( ! sp[com].use ) {
    cout << "Serial Read: Error - Trying to read from closing com port" << com << endl;
    return -1;
  }

  // If all that is good we are ready to read data.
  return sp[com].RX.pop( buffer, length );

}


/* serial_read_blocking: 
 *      Uses calls to serial_read for various buffer lengths to read until all
 *      data comes in or the timeout is reached.  A timeout of 0 means that the
 *      function returns the contents of the buffer and returns immediately.
 * RETURN:	length of data read from serial, -1 if an error occurs
 */
int serial_read_blocking(int com, char *buffer, int length, Timeval timeout)
{

  if( SERIAL_DEBUG ) cout << "Serial Read Blocking Called With Com = " << com << endl;

  int leftToRead = length;  // how much do we have left?
  int justRead;

  // Initialize Timevals so we know when to quit.

  // get start time
  Timeval startTime = TVNow();

  while(true) {
    // Try to read all the data.
    justRead = sp[com].RX.pop( buffer, leftToRead );
    // serial_read( com, buffer, leftToRead);
    
    if( justRead >= 0 ) {
      leftToRead -= justRead; // decrement counter
      buffer     += justRead; // shift the buffer
    } else {
      // an error reading, return bytes read already
      return length-leftToRead;
    }
    // If we got all the data we are good.
    // If not we should sleep _if_ we still have time to wait, 
    // otherwise just return now. Note: there is no reason to wait
    // less than 1ms.  If we are within 1ms of a timeout we will just
    // return now.
    if( leftToRead == 0 || 
	(TVNow() - startTime) > (timeout - Timeval(0, 1000))) {
      return length - leftToRead;
    } else {
      {
	// register as a callback 
	// (have the serial daemon wake me up if new data arrives)
	Lock myLock (sp[com].mutex);
	signal ( SIGUSR2, null_signal_handler );
	sp[com].wakePID = getpid();
      }
      // and sleep for a bit
      //      Sleep ( timeout - (TVNow() - startTime) );
      usleep(8000);
    }
  } 
}



/* serial_read_until (until the correct character was read):
 *              If the com port is not open return an error. 
 *
 *              The user has to specify a character he or she is looking
 *              for, and this function won't return until either the character has
 * 		been found, or the function times out, or buffer length is longer than
 *		limitlength. If the character is found, data in the buffer up to the 
 *              character will be returned to the user. 
 *
 * com   	: number of serial port
 * buffer	: buffer that will be returned with data
 * delimit	: character the user is looking for
 * timeout	: time out in microseconds
 * limitlength	: limit length of the buffer. If serial buffer is longer than this, 
 *		  then return data of limitlength from the buffer
 * RETURN	: length of data read from serial:
 *                if an error occurs, then the # of bytes read before the error occured
 * NOTE         : it's segfaulted once, but i can't debug it because it won't do it
 *                anymore. maybe it'll happen in the future. watch out for that.
 */
int serial_read_until(int com, char *buffer, char delimit, int maxlength, Timeval timeout)
{

  if( SERIAL_DEBUG ) cout << "Serial Read Until Called With Com = " << com << endl;

  int leftToRead = maxlength;  // how much do we have left that we can read before giving up?
  int ret;

  // Initialize Timevals so we know when to quit.
  Timeval startTime = TVNow();

  while(true) {
    // Try to read a byte
    ret = sp[com].RX.pop( buffer, 1 );
    // serial_read( com, buffer, 1 );
    if( ret > 0 ) {
      // If we got a new one, update the read counter,
      // and check to see if it the new character 
      // is what we are looking for.
      leftToRead -= ret; // decrement counter      
      if( buffer[0] == delimit || leftToRead == 0 ) { 
	// if we found our character or we reached the maximum
	// allowed number of bytes, quit now
	return maxlength - leftToRead;
      } else {
	buffer += ret; // shift the buffer
      }
    } else if ( ret < 0) {
      // an error, return #of bytes we have already read
      return maxlength-leftToRead;
    } else {
      // We did not get a character on this pass.
      // If we still have time we can wait, then sleep.
      // otherwise just return now. Note: there is no reason to wait
      // less than 1ms.  If we are within 1ms of a timeout we will just
      // return now.
      if( (TVNow() - startTime) > (timeout - Timeval(0, 1000))) {
	return maxlength - leftToRead;
      } else {
	{
	  // register as a callback 
	  // (have the serial daemon wake me up if new data arrives)
	  Lock myLock (sp[com].mutex);
	  signal(SIGUSR2, null_signal_handler);
	  sp[com].wakePID = getpid();
	}
	// and sleep for a bit
	//	Sleep ( timeout - (TVNow() - startTime) );
	usleep(8000);
      }
    }
  }
}



// This function allows the Daemon thread to close the serial port
// if it wants.

int DaemonClosePort(int com) {

  if( SERIAL_DEBUG ) cout << "Daemon Close Port Called With Com = " << com << endl;

  // Step 1. Check COM Port
  /* Make sure we aren't passed garbage */
  if (com < 0 || com >= MaxSerialPorts) {
    cout << "Daemon Close Port: Error Com port out of range: " << com << endl;
    return -1;
  }

  Lock mylock(sp[com].mutex);

  // Step 2. Check File Descriptor
  /* Make sure we aren't passed garbage */
  if (sp[com].fd < 0 ) {
    cout << "Daemon Close Port: Error FD out of range: " << sp[com].fd << endl;
    return -1;
  }


  // else

  close ( sp[com].fd );

  sp[com].use = false;
  sp[com].fd  = -1;
  sp[com].state = SPStates::NotOpen;

  // Added 3/4/04 -- make sure data is dropped when files are closed
  sp[com].TX.dump();
  sp[com].RX.dump();

  return 0;
}


// This function allows the Daemon thread to open the serial port
// if it wants.

int DaemonOpenPort(int com, int BaudRate, int options) {

  if( SERIAL_DEBUG ) cout << "Daemon Open Port Called With Com = " << com << endl;

  // DO LOTS OF ERROR CHECKING FIRST
  // 1. Make sure COM is OK
  // 2. Check Parity Settings
  // 3. Check Stop Bits
  // 4. Check Hardware Flow Control
  // 5. Check Software Flow Control

  // ********* THEN ********
  // 
  // 


  // Step 1. Check COM Port
  /* Make sure we aren't passed garbage */
  if (com < 0 || com >= MaxSerialPorts) {
    cout << "Daemon Open Port: Error Com port out of range: " << com << endl;
    return -1;
  }
  {
    Lock myLock (sp[com].mutex);
    if( sp[com].state == SPStates::Open) {
      cout << "Daemon Open Port: Error Com port already open: " << com << endl;
      return -1;
    } else {
      sp[com].state = SPStates::NotOpen; // reset an error condition
    }
  }

  // Step 2.
  /* Check Parity Options */ 
  if((options & SR_PARITY_DISABLE) && ((options & SR_PARITY_EVEN) || (options & SR_PARITY_ODD)))
  {
    cout << "Daemon Open Port: Cannot disable and enable parity at the same time.\n";
    return -1;
  }
  else if ((options & SR_PARITY_EVEN) && (options & SR_PARITY_ODD))
  {
    cout << "Daemon Open Port: Cannot set parity to even and odd at the same time.\n";
    return -1;
  }

  // Step 3. Check Stop Bits
  if((options & SR_ONE_STOP_BIT) && (options & SR_TWO_STOP_BIT))
  {
    cout << "Daemon Open Port: Cannot set stop bit to one and two at the same time.\n";
    return -1;
  }

  // Step 4.
  /* Check hardware flow control*/
  if((options & SR_HARD_FLOW_DISABLE) && (options & SR_HARD_FLOW_ENABLE))
  {
    cout << "Daemon Open Port: Cannot disable and enable hardware flow control at the same time\n";
    return -1;
  }
  
  // Step 5.
  /* check software flow control*/
  /* Caution : if you use flow control, you won't receive bytes with values of 17 and 19*/
  if((options & SR_SOFT_FLOW_DISABLE) && ((options & SR_SOFT_FLOW_INPUT) || (options & SR_SOFT_FLOW_OUTPUT)))
  {
    cout << "Daemon Open Port: Cannot disable and enable software flow control at the same time\n";
    return -1;
  }


  strstream fn;
  fn << ((string) SerialPath) << com;
  string filename(fn.str());

  cout << "Opening Serial Port: " << filename.c_str() << endl;

  int fd;
  if((fd = open(filename.c_str(), O_RDWR | O_NOCTTY /*| O_NDELAY*/))<0) {	
    // initialize to blocking state
    cout <<"Daemon Open Port: Open Error" << endl;

    //set error state
    Lock myLock( sp[com].mutex);    
    sp[com].state = SPStates::Error;
    sp[com].use = false;

    return -1;
  } 

  struct termios term;
  int oflags;


  /* Get the current options for the port */
  if(tcgetattr(fd, &term)<0) {
    cout << "Daemon Open Port: tcgetattr error\n";
    close( fd );
    // Yikes !!, reset the FD
    Lock myLock( sp[com].mutex);
    sp[com].fd = -1;
    sp[com].state = SPStates::Error;
    sp[com].use = false;

    return -1;
  }

  cfsetispeed(&term, BaudRate);	/* Set the baud rates */
  cfsetospeed(&term, BaudRate);
  tcflush(fd, TCIFLUSH);

  /* Enable the receiver and set local mode */ 
  if (options & SR_PARITY_EVEN) term.c_cflag = PARENB;                // enable even parity	
  else if (options & SR_PARITY_ODD)  term.c_cflag = PARENB | PARODD;  // enable odd parity
  

  /* Check stop bits */
  if (options & SR_TWO_STOP_BIT) term.c_cflag = term.c_cflag | CSTOPB;
  // default is one stop bit, so don't worry about it
  
  /* Check hardware flow control*/
  if(options & SR_HARD_FLOW_ENABLE)  	term.c_cflag = term.c_cflag | CRTSCTS;// enable hard 
  
  term.c_cflag = term.c_cflag | // set the parity ,stop bit, and hardware flow control
    BaudRate |                  // set the baurd rate
    CS8 |                       // 8-bit data
    CLOCAL |                    // enable receiver
    CREAD;                      // ignore modem status line
  
  term.c_oflag &= ~OPOST;	// turn off post processing
  
  /* check software flow control*/
  if(options & SR_SOFT_FLOW_INPUT)   	term.c_iflag = term.c_iflag | IXOFF;	// enable soft output 
  if(options & SR_SOFT_FLOW_OUTPUT)  	term.c_iflag = term.c_iflag | IXON;	// enable soft input
  
  term.c_iflag = term.c_iflag | IGNBRK;    // ignore breaks
  term.c_lflag =0;                         // everything off
  



  /* Set the new options for the port */
  if(tcsetattr(fd, TCSANOW, &term)<0)
  {
    Lock myLock( sp[com].mutex);
    sp[com].fd = -1;
    sp[com].state = SPStates::Error;
    sp[com].use = false;
    cout << "Daemon Open Port: tcsetattr error, line" << endl ;
    return -1;
  }
    
  Lock myLock( sp[com].mutex);
  sp[com].use = true;
  sp[com].fd = fd;
  sp[com].state = SPStates::Open;

  if( sp[com].wakePID >= 0 ) kill(sp[com].wakePID, SIGUSR2);
  sp[com].wakePID = -1;
  
  return 0;
}



// Daemon in charge of opening and closing serial ports.
void SerialManager(void*) {

  if( SERIAL_DEBUG ) cout << "Serial Manager Thread Started" << endl;

  // save my PID
  {
    Lock myLock(SerialDaemonLock);
    ManagerPID = getpid();
    signal( SIGUSR2, &null_signal_handler );
  }

  int com, baud, options;


  while( ! ShuttingDown() ) {

    for ( com = 0; com < MaxSerialPorts; com++) {
      // check all the coms  
      Lock comlock(sp[com].mutex);
      if( sp[com].use &&
	  sp[com].state != SPStates::Open ) {
	// open the serial port
	DaemonOpenPort( com, sp[com].baud, sp[com].options);

	if( RXPID >= 0 ) kill(RXPID, SIGUSR2);
	if( TXPID >= 0 ) kill(TXPID, SIGUSR2);

      } else if ( sp[com].use == false &&
		  sp[com].state == SPStates::Open) {
	// close the serial port
	DaemonClosePort( com );
      }
      
      if( sp[com].wakePID >= 0 ) kill(sp[com].wakePID, SIGUSR2);
      sp[com].wakePID = -1;
    }

    sleep(1);
  }

}


// RX Thread sits in a loop reading serial data
void RXThread(void*) {

  if( SERIAL_DEBUG ) cout << "RXThread Started" << endl;

  // save my PID
  {
    Lock myLock(SerialDaemonLock);
    RXPID = getpid();
    signal( SIGUSR2, &null_signal_handler );
  }


  pollfd pfd[MaxSerialPorts]; // polling file descriptors
  int openSP, com, retval;

  const int timeout = 1000; // milliseconds --> 1 second.

  while ( !ShuttingDown() ) {
    
    // Steps
    // 1. Create a list of all open/good serial ports.
    // 2. Poll to see if data is available.
    // 3. For each serial port go through and read data if available,
    //    and enqueue it in the RX buffer.

    // Step 1. -- list all open serial ports.
    openSP = 0; 
    for( com = 0; com < MaxSerialPorts; com++) {
      Lock mylock( sp[com].mutex);
      if( sp[com].use &&
	  sp[com].state == SPStates::Open &&
	  sp[com].fd > 0 ) {
	
	pfd[openSP].fd = sp[com].fd;
	pfd[openSP].events = POLLIN;
	openSP++;
      }
    }

    // if no serial ports are open then do not bother
    // trying to read
    if ( openSP == 0 ) {
      usleep( 100000);
    } else {

      // Step 2.  Poll the serial ports (if any are open)
      retval = poll( &(pfd[0]), openSP, timeout);

      // Step 3.  Read data from all serial ports 
      if( retval > 0 ) {
	// go through the serial ports and read data
	// read this serial port
	char buf[MaxRX];
	int ret, com, i;
	for(i=0; i<openSP; i++) {
	  if( pfd[i].revents & POLLIN) {
	    ret = read(pfd[i].fd, buf, MaxRX);
	    // find which com port this fd goes to
	    for( com = 0; com < MaxSerialPorts; com++) {
	      if( sp[com].fd == pfd[i].fd ) {
		Lock s(sp[com].mutex);
		sp[com].aRead ++; // we attempted to read

		// Now if read > 0
		if( ret >=0) {
		  sp[com].bRead += ret; // we read this many bytes
		  sp[com].RX.push(buf, ret);

		  if( sp[com].wakePID >= 0 ) kill(sp[com].wakePID, SIGUSR2);
		  sp[com].wakePID = -1;
		}
		break;
	      }
	    }
	  }
	}
      }
    }
  }
}



void TXThread(void*) {

  if( SERIAL_DEBUG ) cout << "TXThread Started" << endl;

  // save my PID
  {
    Lock myLock(SerialDaemonLock);
    TXPID = getpid();
    signal( SIGUSR2, &null_signal_handler );
  }


  pollfd pfd[MaxSerialPorts]; // polling file descriptors
  int openSP, com, retval;

  const int timeout = 1000; // milliseconds --> 1 second.

  while ( !ShuttingDown() ) {
    
    // Steps
    // 1. Create a list of all open/good serial ports.
    // 2. Poll to see if data is can be sent.
    // 3. For each serial port go through and send data if available,
    //    and dequeue it in the RX buffer.

    // Step 1. -- list all open serial ports.
    openSP = 0; 
    for( com = 0; com < MaxSerialPorts; com++) {
      Lock mylock( sp[com].mutex);
      if( sp[com].state == SPStates::Open && // serial port is open
	  sp[com].use                     && // we are using the serial port (as opposed to closing it)
	  sp[com].fd > 0                  && // fd is reasonable
	  sp[com].TX.size() > 0           ){ // we have something to transmit
	
	pfd[openSP].fd = sp[com].fd;
	pfd[openSP].events = POLLOUT;
	openSP++;
      }
    }

    // if no serial ports are open then do not bother
    // trying to write
    if ( openSP == 0 ) {
      usleep( 50000);
    } else {

      // Step 2.  Poll the serial ports (if any are open)
      retval = poll( &(pfd[0]), openSP, timeout);

      // Step 3.  Write data to all serial ports 
      if( retval > 0 ) {
	// go through the serial ports and write data
	char buff[MaxTX];
	int ret, com, i, bytes;
	for(i=0; i<openSP; i++) {
	  if( pfd[i].revents & POLLOUT) {
	    // find which com port this fd goes to
	    for( com = 0; com < MaxSerialPorts; com++) {
	      if( sp[com].fd == pfd[i].fd ) {
		Lock s(sp[com].mutex);

		// we are a go for write
		// copy data from buffer into transmit buffer
		// but do NOT dequeue it yet (just in case 
		// write fails).
		bytes = sp[com].TX.peek(buff, MaxTX);
		
		// do the writing
		ret = write( sp[com].fd, buff, bytes);
		sp[com].aWrite ++; // we attemted a write;
		
		// Now if write worked
		if( ret >=0 ) {
		  sp[com].bWrite += ret;     // we wrote this many bytes
		  sp[com].TX.pop(buff, ret);  // now pop the data.
		}
		break;
	      }
	    }
	  }
	}
      }
    }
  }
}



void print_serial_status(int port) {

  if( port < 0 || port >= MaxSerialPorts) {
    // print status of all serial ports
    for ( int com=0; com < MaxSerialPorts; com ++) {
      cout << "Serial Port: " << com << ", Device State=" << sp[com].state 
	   << ", Error Count (Currently Not Used)=" << sp[com].ec << endl
	   << "Use=" << sp[com].use << ", fd=" << sp[com].fd 
	   << ", baud=" << ", options=" << sp[com].options << endl
	   << "Using Read Callback " << sp[com].callback << endl
	   << "Bytes Read: "  << sp[com].bRead << " in " << sp[com].aRead << " attempts." << endl
	   << "Bytes Written: " << sp[com].bWrite << " in " << sp[com].aWrite << " attempts." << endl << endl;
    }
  } else {
    int com = port;
    // ONLY print status of requested serial port.
    cout << "Serial Port: " << com << ", Device State=" << sp[com].state 
	 << ", Error Count (Currently Not Used)=" << sp[com].ec << endl
	 << "Use=" << sp[com].use << ", fd=" << sp[com].fd 
	 << ", baud=" << ", options=" << sp[com].options << endl
	 << "Using Read Callback " << sp[com].callback << endl
	 << "Bytes Read: "  << sp[com].bRead << " in " << sp[com].aRead << " attempts." << endl
	 << "Bytes Written: " << sp[com].bWrite << " in " << sp[com].aWrite << " attempts." << endl << endl;    
  }
  
}



#endif // serial2
