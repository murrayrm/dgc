#include <iostream>
#include "genMap.hh"

//Tests map class window shifting and cell data updating functions by 
//running a simulation that declares a map and feeds it a new set of data 
//and new vehicle position at each time step.

void State_Store(VState_GetStateMsg &state, double x, double y, double vx, double vy, double ax, double ay, double t);

int main()
{
	const int NUMROWS = 10, NUMCOLS = 10;
	const double ROW_RES = 0.001, COL_RES = 0.001;
	int r, c;
	double x = 0.001, vx = 0.01, ax = 0.000, input_x, display_x;
	double y = 0.001, vy = 0.01, ay = 0.000, input_y, display_y;
	double t = 0.0, dt = 0.1;
	CellData z, display_z;
	const double Time_Length = 1.0;
	VState_GetStateMsg state_now;
	State_Store(state_now, x, y, vx, vy, ax, ay, t);
	genMap testmap(state_now, NUMROWS, NUMCOLS, ROW_RES, COL_RES, false);

	for(t = 0; t < Time_Length; t += dt)
	{
	  x = x + vx*dt;
	  y = y + vy*dt;
	  z.value = 1.0;
	  vx = vx + ax*dt;
	  vy = vy + ay*dt;
	  State_Store(state_now, x, y, vx, vy, ax, ay, t);
	  testmap.updateFrame(state_now, true, 0, 0);
	  input_y = 0.0;
	  for(r = 0; r < NUMROWS*10; r++)
	    {
	      input_x = 0.0;
	      for(c = 0; c < NUMCOLS*10; c++)
		{
		  if((input_x > x)&&(input_y > y))
		    z.value = input_x*input_y*1000000.0;
		  else
		    z.value = 0.0;
		  testmap.setCellDataUTM(input_x, input_y, z);
		  input_x += COL_RES;
		}
	      input_y += ROW_RES;
	    }
	  testmap.display();
	  //display_y = 0.0;
	  //cout << "-----------------------------------------------------------\n";
	  //	  for(r = 0; r < NUMROWS; r++)
	  //{
	  //  display_x = 0.0;
	  //  for(c = 0; c < NUMCOLS; c++)
	  //{
	  //  display_z = testmap.getCellDataUTM(display_x, display_y);
	  //  cout << display_z.value << "\t";
	  //  display_x = display_x + COL_RES;
	  //}
	  //  cout << endl;
	  //  display_y = display_y + ROW_RES;
	  //} 
	}
	return 0;
}

void State_Store(VState_GetStateMsg &state, double x, double y, double vx, double vy, double ax, double ay, double t)
{
	//state.Timestamp.sec = t;
	state.Easting = x;
	state.Northing = y;
	state.Vel_E = vx;
	state.Vel_N = vy;
	state.Speed = sqrt(vx*vx + vy*vy);
	state.Accel = sqrt(ax*ax + ay*ay);
}
