#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <iostream.h>
#include "genMap.hh"

#define PI 3.1415

int main()
{
  genMap saved_map;
  int status;
  status = saved_map.loadMATFile("MATsample2.m");
  if(status)
    {
      cout << "Error occurred while loading map from MATFile.\n";
    }
  else
    {
      cout << "We have successfully loaded the map from the MATFile.\n";
    }
  status = saved_map.saveMATFile("MATsample3", "This MATFile is the output of MATFile_test", "sample_map");
  if(status)
    {
      cout << "Error ocurred while saving map to MATFile.\n";
    }
  else
    {
      cout << "We have successfully saved the map to a MATFile.\n";
    }
  return 0;
}
