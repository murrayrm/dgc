#include "Modules.hh"
#include "TestModule.hh"
#include "Kernel.hh"

#include <boost/shared_ptr.hpp>

using namespace boost;

int main() {

  Register(shared_ptr<DGC_MODULE>( new TestModule ));
  StartKernel();

  return 0;
}
