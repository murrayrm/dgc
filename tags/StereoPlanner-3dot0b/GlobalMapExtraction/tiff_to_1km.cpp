#include <list>
#include <unistd.h>
#include <fcntl.h>
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <vector>


using namespace std;

bool I_am_big_endian();
void* my_read(void* data, int byte_size);
void print_as_bin(const unsigned long input);
int read_entry(int tifn, const int tiff_end);
bool stuff_data(const char* tiff_name, const char* bmp_name,
		const unsigned int top, const unsigned int bottom,
		const unsigned int left, const unsigned int right,
		const unsigned int x_len, const unsigned int y_len);
int read_header(const string header_name, double* x_origin, double* y_origin,
		int* x_len, int* y_len);
bool process_image(const int tifn, const char* bmp_name,
		   const unsigned int top, const unsigned int bottom,
		   const unsigned int left, const unsigned int right,
		   const int tiff_end);



enum entry_tags
{ImageWidth = 256, ImageLength, BitsPerSample, Compression,
 PhotometricInterpretation = 262, StripOffsets = 273, SamplesPerPixel = 277,
 RowsPerStrip, StripByteCounts, XResolution = 282, YResolution,
 ResolutionUnit = 296
};
enum types {BYTE = 1, ASCII, SHORT, LONG, RATIONAL};
enum {SUCCESS, UN_OPENED, ERROR};

union buffer_one {
  unsigned char buf[1];
  unsigned char in_char;
};

union buffer_two {
  unsigned char buf[2];
  unsigned short in_short;
};

union buffer_four {
  unsigned char buf[4];
  unsigned long in_long;
};

union sbuffer_two {
  char buf[2];
  unsigned short in_short;
};

union sbuffer_four {
  char buf[4];
  unsigned long in_long;
};


bool me_big_endian;
bool tiff_big_endian;
buffer_one buf_one;
buffer_two buf_two;
buffer_four buf_four;
sbuffer_two sbuf_two;
sbuffer_four sbuf_four;
/*
char in_one[1];
char in_two[2];
char in_four[4];
unsigned char in_char;
unsigned short in_short;
unsigned long in_long;
*/
unsigned long tiff_col_num;
unsigned long tiff_row_num;
unsigned long rows_per_strip;
list<unsigned long> strip_offsets;
list<unsigned long> strip_byte_counts;
int is_RGB;
bool black_is_zero;


// For now, average pixel values to convert them to grayscale.  A change may
// occur in the future to have it instead simply return the green value of the
// RGB values.
inline unsigned char conv_to_grayscale(unsigned char value1,
				       unsigned char value2,
				       unsigned char value3) {
  return (value1 + value2 + value3)/3;
}



bool I_am_big_endian() {
  buf_four.in_long = 1;
  if (buf_four.buf[0] == 1) {
    return false;
  }
  else if (buf_four.buf[3] == 1) {
    return true;
  }
  else {
    cout << "Unexpected data storage method\n";
    assert(1);
    return false;
  }
}


void print_as_bin(const unsigned long input) {
  unsigned long n = input;
  string byte_string("");
  string byte;
  for (int i = 0; i < 4; i++) {
    byte = "";
    for (int j = 0; j < 8; j++) {
      if (n % 2 == 0) {
	byte = string("0") + byte;
      }
      else {
	byte = string("1") + byte;
      }
      n /= 2;
    }
    byte += " ";
    byte_string = byte + byte_string;
  }

  cout << byte_string << "\n";
  return;
}
      
  



void* my_read(void* data, int byte_size) {
  if (me_big_endian != tiff_big_endian) {
    // There is an inconsistency between how the machine would read the data
    // and how it is stored in the tiff file.  The bytes in this buffer must
    // be reversed.
    unsigned char temp;
    unsigned char* front_it = (unsigned char*)data;
    unsigned char* back_it = (unsigned char*)data;
    back_it += (byte_size - 1);
    // Swap the bits until the file is half read or only a center bit remains.
    for (int i = 0; i < byte_size / 2; i++) {
      temp = *front_it;
      *front_it = *back_it;
      *back_it = temp;
      front_it++;
      back_it--;
    }
  }

  // Now either the data has been reversed to be compatible, or the data was
  // good to begin with.  Return the input pointer.
  return data;
}




/* Grabs the data from the tiff file bounded by the input region and stores it
 * into a bmp of the given name, creating it if it doesn't exist.  This
 * function also takes as an input which pixel in the .bmp maps to the upper
 * left pixel of the .tif file in case this function is only instantiating part
 * of the entire .bmp file.  These default to 0, 0, and so don't have to be
 * worried with in the base case of the entire .bmp coming from the same tiff.
 * When a .bmp is composed of information from multiple .tifs, THIS FUNCTION
 * SHOULD ALWAYS BE CALLED FIRST ON THE MOST LOWWER RIGHT INPUT.  When the
 * function is writing a new file that has not previously existed, it will
 * infer the size of the .bmp from the width and height of the .tif information
 * it is transfering and the offset into the .bmp that it is given.
 *
 * If this function is in an application that is being debugged, it sends any
 * errors it encounters to cout as they are encountered.  These are also stored
 * in a string as the function runs, with non-terminal errors or warnings
 * accumulating until the function ends or a terminal error occurs.  External
 * code can consult this by specifying the final input, a string pointer, to
 * be non-NULL, in which case the error log will be returned as well as
 * forwarded to cout.
 */

bool stuff_data(const char* tiff_name, const char* bmp_name,
		const unsigned int top, const unsigned int bottom,
		const unsigned int left, const unsigned int right,
		const unsigned int x_len, const unsigned int y_len) {
  bool no_errors = true;
  
  /// Determine that the filenames are as expected.
  
  string tiff_str(tiff_name);
  string bmp_str(bmp_name);
  if (
      (
       ((tiff_str.substr(tiff_str.size() - 4, 4) != ".tif" &&
	 tiff_str.substr(tiff_str.size() - 4, 4) != ".Tif") &&
	tiff_str.substr(tiff_str.size() - 4, 4) != ".TIF") &&

       ((tiff_str.substr(tiff_str.size() - 5, 4) != ".tiff" &&
	 tiff_str.substr(tiff_str.size() - 5, 4) != ".Tiff") &&
	tiff_str.substr(tiff_str.size() - 5, 4) != ".TIFF")) &&
      
      ((bmp_str.substr(bmp_str.size() - 4, 4) != ".bmp" &&
	bmp_str.substr(bmp_str.size() - 4, 4) != ".Bmp") &&
       bmp_str.substr(bmp_str.size() - 4, 4) != ".BMP")) {
    
    // Any of these is terminal.  Log and quit.
    printf("Improper filenames.  Tiff filename is \"%s\"\n", tiff_name);
    printf("and BMP filename is \"%s\"\n", bmp_name);
    printf("File extensions were \"%s\" and \"%s\"\n",
	   tiff_str.substr(tiff_str.size() - 4, 4).c_str(),
	   bmp_str.substr(bmp_str.size() - 4, 4).c_str());
	   
    return false;
  }

  // Assign initial values to data used to read image information that should
  // be initialized in the IDF.
  tiff_col_num = 0;
  tiff_row_num = 0;
  rows_per_strip = 0;
  strip_offsets.clear();
  strip_byte_counts.clear();
  is_RGB = -1;  

  me_big_endian = I_am_big_endian();

  /// Open the file and note the value given at the end of the file to check
  /// against the offsets read from the file.
  
  int tifn;
  if ((tifn = open(tiff_name, O_RDONLY)) < 0) {
    // Terminal error, log and return.
    printf("Error oppening tiff \"%s\"\n", tiff_name);
    return false;
  }

  // Note the end of the file to error-check offsets.
  const int tiff_end = lseek(tifn, 0, SEEK_END);
  
  
  /// Prepare buffers to read in data, constants, bools, etc. that will be
  /// used.

  unsigned long ifd_offset;
  unsigned long next_offset;
  
  /// Read the tiff header and log errors.  The header is always the first
  /// 8 bytes.  Don't fail until the entire header has been read.
  
  // Store whether data is read in big_endian or not.

  lseek(tifn, 0, SEEK_SET);
  read(tifn, buf_two.buf, 2);
  if (buf_two.in_short == 0x4D4D) {
    tiff_big_endian = true;
    // me_big_endian = true;
  }
  else if (buf_two.in_short == 0x4949) {
    tiff_big_endian = false;
    // me_big_endian = true;
  }
  else if (buf_two.in_short == 0xD4D4) {
    tiff_big_endian = true;
    // me_big_endian = false;
  }
  else if (buf_two.in_short == 0x9494) {
    tiff_big_endian = false;
    // me_big_endian = false;
  }
  else {
    // One of these must be the first two bytes in the file.  Terminal error.
    printf("TIFF reader expects first two bytes to specify big or little\n");
    printf("endian format, instead recieved %X\n", buf_two.in_short);
    no_errors = false;
  }

  // All tiff files have (no kidding) 42 in the next byte.

  lseek(tifn, 2, SEEK_SET);
  read(tifn, buf_two.buf, 2);

  my_read(buf_two.buf, 2);
  if (buf_two.in_short != 42) {
    printf("TIFF identifying mark, 42 in decimal, was not found in the\n");
    printf("header.  Hex of bytes 3 and 4 is instad %X\n", buf_two.in_short);
    no_errors = false;
  }

  // Find where in the tif the IFD is.

  lseek(tifn, 4, SEEK_SET);
  read(tifn, buf_four.buf, 4);
  my_read(buf_four.buf, 4);
  ifd_offset = buf_four.in_long;

  // Go ahead and move the file pointer to the IFD to prepare to read it.  This
  // allows the last header value to be error checked before closing by
  // verifying that the offset remains within the file.
  if (lseek(tifn, ifd_offset, SEEK_SET) > tiff_end) {
    printf("Hex file offset to IDF, \"%lX\" leads out of the file\n",
	   ifd_offset);
    no_errors = false;
  }

  if (!no_errors) {
    printf("Errors in header, closing file and giving up\n");
    close(tifn);
    return false;
  }

  
  /// Read the IFD info and determine whether the file meets the expected
  /// parameters.

  // Find the number of entries in the IFD.  The pointer is already at its
  // beginning.
  read(tifn, buf_two.buf, 2);
  int entry_count = *(unsigned short*) my_read(buf_two.buf, 2);

  // Read each entry.

  int prev_id = 0;
  int cur_id = 0;
  next_offset = ifd_offset + 2;
  
  for (int index = 0; index < entry_count; index++) {
    if (lseek(tifn, next_offset, SEEK_SET) > tiff_end) {
      printf("Hex file offset of the current entry, \"%lX\" leads out of\n",
	     ifd_offset);
      printf("the file.  The invalid entry is the %dth, coming after the\n",
	     index);
      printf("entry labeled %d\n", prev_id);
      printf("Futile to proceed, closing file and giving up\n");
      close(tifn);
      return false;
    }
    
    // This function updates any global values as needed from the info in the
    // entry, ignores irrelvant entries, and positions the pointer back to the
    // beginning of the entry again when it is done.
    cur_id = read_entry(tifn, tiff_end);
    if (cur_id < 0) {
      // read_entry could not handle the entry, log the catch and bail.
      printf("Error reading the %dth entry, tag %d\n", index, -cur_id);
      printf("Will close and quit after attempting to read all entrys\n");
      no_errors = false;
    }
    if (cur_id <= prev_id && cur_id > 0) {
      // The entry IDs are supposed to be sorted in increasing order.  This
      // might indicate an error with the tiff file.  Still try and read it,
      // but log the failure.
      printf("WARNING: IFD in file \"%s\" stores entries in a non-\n",
	     tiff_name);
      printf("ascending order as required by tiff convention\n");
      printf("Entry tag %d preceeded tag %d\n", prev_id, cur_id);
      printf("Attempting to salvage file read\n");
    }
    // Need to move the pointer as well as increment index.  Each 'entry' is
    // 12 bytes.  Also, set the prior id to the current one.
    next_offset += 12;
    prev_id = cur_id;
  }
  
  if (!no_errors) {
    printf("Failed to read or ignore entries correctly.  Close and quit\n");
    close(tifn);
    return false;
  }

  // The pointer is now addressed to the last value in an IFD, which is the
  // offset of the next IFD.  For the purposes of the maps this reader is
  // intended for, I really, really hope that this value is NULL and there is
  // only one image.  If it isn't, it logs a warning and pretends that it were
  // (meaning it ignores it and does not at any point follow the offset to read
  // the next IFD).
  read(tifn, buf_four.buf, 4);
  my_read(buf_four.buf, 4);
  if (buf_four.in_long != 0) {
    printf("WARNING: multiple IFDs in file \"%s\"\n", tiff_name);
    printf("Attempting to salvage file read\n");
  }
  
  // Assign initial values to data used to read image information that should
  // be initialized in the IDF.
  if (tiff_col_num == 0 || tiff_row_num == 0 || rows_per_strip == 0 ||
      strip_offsets.empty() || strip_byte_counts.empty() ||  is_RGB == -1
      || strip_offsets.size() != strip_byte_counts.size()) {
    printf("Data for image read improper or absent.  Claimed image width\n");
    printf("is %ld, image height is %ld, rows per image strip are %ld.\n",
	   tiff_col_num, tiff_row_num, rows_per_strip);
    printf("There are %d offsets for strips and %d byte counts for strips\n",
	   strip_offsets.size(), strip_byte_counts.size());
    printf("The is_RGB value determined is %d\n", is_RGB);
    printf("Closing and quiting\n");
    close(tifn);
    return false;
  }

  // Make sure data found in image file matches expectation from header file
  if (tiff_col_num != x_len || tiff_row_num != y_len) {
    printf("WARNING: Image data did not match input data from header file.\n");
    printf("  header indicated %d was the number of columns, while the \n",
	   x_len);
    printf("    file actually contains %ld columns.\n", tiff_col_num);
    printf("  header indicated %d was the number of rows, while the \n",
	   y_len);
    printf("    file actually contains %ld rows.\n", tiff_row_num);
    printf("Closing and quiting\n");
    close(tifn);
    return false;
  }
    
  /// Have all the values needed, get the image data.

  // Check the input values for tiff retrieval against the values that were
  // input.
  if (top > bottom) {
    printf("Top bound %d exceeds bottom bound %d.  Invalid input.\n", top,
	   bottom);
    no_errors = false;
  }
  if (bottom > tiff_row_num) {
    printf("Bottom bound %d exceeds number of rows %ld.  Invalid input\n",
	   bottom, tiff_row_num);
    no_errors = false;
  }
  if (left > right) {
    printf("Left bound %d exceeds left bound %d.  Invalid input.\n", left,
	   right);
    no_errors = false;
  }
  if (right > tiff_col_num) {
    printf("Right bound %d exceeds number of columns %ld.  Invalid input\n",
	   right, tiff_col_num);
    no_errors = false;
  }

  if (!no_errors) {
    printf("Bounds for data extraction exceed bounds of image size.\n");
    printf("Closing and quitting\n");
    close(tifn);
    return false;
  }
  
  // Run the processing and check for errors during it.
  if (!process_image(tifn, bmp_name, top, bottom, left, right, tiff_end)) {
    printf("Error reading or outputting image data.\n");
    printf("Closing and quitting\n");
    close(tifn);
    return false;
  }
  
  close(tifn);
  return true;
  }


int read_entry(int tifn, const int tiff_end) {
  // Read in the data from the buffer.
  read(tifn, buf_two.buf, 2);
  my_read(buf_two.buf, 2);
  unsigned short cur_id = buf_two.in_short;
  
  read(tifn, buf_two.buf, 2);
  my_read(buf_two.buf, 2);
  unsigned short type = buf_two.in_short;

  
  read(tifn, buf_four.buf, 4);
  my_read(buf_four.buf, 4);
  unsigned long count = buf_four.in_long;

  unsigned long byte_len = 0;
  unsigned long offset = 0;
  unsigned int size = 0;

  /// Determine whether to leave the file pointer at the data value, or if that
  /// is an offset that will send it elsewhere.
  
  // The type of data is not known in advance.  Use NULL pointer.
  void* data = NULL;

  // Find the size of the data type, whether it is stored locally or at an
  // offset, and retrieve the data.  Do this only if the data type is
  // a recognized and supported one.  These form a superset of all types that
  // are demanded for future calls to data, so data will be retrieved here
  // if it is to be accessed later.
  switch(type) {
  case BYTE:
  // This is the same as for ascii, do nothing and don't break
  case ASCII:
    size = 1;
    byte_len = count * size;
    (unsigned char *)data = (unsigned char *) malloc(byte_len);
    if (data == NULL) {
      printf("Unable to assign memory to store data in entry %d\n", cur_id);
      printf("Futile to proceed with tag, returning failure\n");
      return -cur_id;
    }
    break;

  case SHORT:
    size = 2;
    byte_len = count * size;
    (unsigned short *)data = (unsigned short *) malloc(byte_len);
    if (data == NULL) {
      printf("Unable to assign memory to store data in entry %d\n", cur_id);
      printf("Futile to proceed with tag, returning failure\n");
      return -cur_id;
    }
    break;

  case LONG:
    size = 4;
    byte_len = count * size;
    (unsigned long *)data = (unsigned long *) malloc(byte_len);
    if (data == NULL) {
      printf("Unable to assign memory to store data in entry %d\n", cur_id);
      printf("Futile to proceed with tag, returning failure\n");
      return -cur_id;
    }
    break;
  }


  

  // File pointer is currently to the 4 byte end of the entry.  If the data
  // won't fit into that region, redirect it to the offset before reading
  // into the data buffer.
  if (byte_len > 4) {
    read(tifn, buf_four.buf, 4);
    my_read(buf_four.buf, 4);
    offset = buf_four.in_long;
    if (lseek(tifn, offset, SEEK_SET) > tiff_end) {
      printf("Offset to the data of the entry tagged %d\n", cur_id);
      printf("leads to the hex location %lX, which is out of the %s",
	     offset, "file\n");
      printf("Hex of data type was %X and decimal of count was %ld\n", type,
	     count);
      printf("Futile to proceed with tag, returning failure\n");
      free(data);
      return -cur_id;
    }
  }
  
  if (data && read(tifn, data, byte_len) < 0) {
    printf("Error reading large tag data, byte size was %ld\n", byte_len);
    printf("Futile to proceed with tag, returning failure\n");
    free(data);
    return -cur_id;
  }

  
  /// Now have the data, need to determine if and how to use it.  This is done
  /// based in the id value of the entry.
  void* location = data;
  unsigned short in_short;
  unsigned long in_long;
  switch(cur_id) {
    
  case ImageWidth:
    if (count != 1 || (type != SHORT && type != LONG)) {
      printf("Tag %d had a hex type %X and decimal count %ld\n%s", cur_id,
	     type, count, "These values violate the tiff specification\n");
      printf("Futile to proceed with tag, returning failure\n");
      in_long = *(unsigned long*) my_read(data, 4);
      printf("For reference, in_long is %ld\nor:\n", in_long);
      print_as_bin(in_long);
      free(data);
      return -cur_id;
    }
    if (tiff_col_num != 0) {
      printf("Repeated reading of the width of the tiff image in tag %d\n",
	     cur_id);
      printf("Existing value is %ld\n", tiff_col_num);
      printf("Stoping tag reading, returning failure\n");
      free(data);
      return -cur_id;
    }
    if (type == SHORT) {
      in_short = *(unsigned short*) my_read(data, 2);
      tiff_col_num = in_short;
    }
    else {
      in_long = *(unsigned long*) my_read(data, 4);
      tiff_col_num = in_long;
    }
    break;
    
  case ImageLength:
    if (count != 1 || (type != SHORT && type != LONG)) {
      printf("Tag %d had a hex type %X and decimal count %ld\n%s", cur_id,
	     type, count, "These values violate the tiff specification\n");
      printf("Futile to proceed with tag, returning failure\n");
      free(data);
      return -cur_id;
    }
    if (tiff_row_num != 0) {
      printf("Repeated reading of the height of the tiff image in tag %d\n",
	     cur_id);
      printf("Existing value is %ld\n", tiff_row_num);
      printf("Stoping tag reading, returning failure\n");
      free(data);
      return -cur_id;
    }
    if (type == SHORT) {
      in_short = *(unsigned short*) my_read(data, 2);
      tiff_row_num = in_short;
    }
    else {
      in_long = *(unsigned long*) my_read(data, 4);
      tiff_row_num = in_long;
    }
    break;
    
  case Compression:
    if (count != 1 || type != SHORT) {
      printf("Tag %d had a hex type %X and decimal count %ld\n%s", cur_id,
	     type, count, "These values violate the tiff specification\n");
      printf("Futile to proceed with tag, returning failure\n");
      free(data);
      return -cur_id;
    }
    in_short = *(unsigned short*) my_read(data, 2);
    if (in_short != 1) {
      printf("TAG %d INDICATES COMPRESSION USED!\n", cur_id);
      printf("Compression hex data is %X\n", in_short);
      printf("Futile to proceed with tag, returning failure\n");
      free(data);
      return -cur_id;
    }
    break;
    
  case PhotometricInterpretation:
    if (count != 1 || type != SHORT)  {
      printf("Tag %d had a hex type %X and decimal count %ld\n%s", cur_id,
	     type, count, "These values violate the tiff specification\n");
      printf("Futile to proceed with tag, returning failure\n");
      free(data);
      return -cur_id;
    }
    in_short = *(unsigned short*) my_read(data, 2);
    if (in_short > 2) {
      printf("Tag %d had a hex data %X indicating an unexpected form of\n%s",
	     cur_id, in_short, "photometric interpretation\n");
      printf("Futile to proceed with tag, returning failure\n");
      free(data);
      return -cur_id;
    }
    if (in_short == 2) {
      is_RGB = 1;
      black_is_zero = true;
    }
    else {
      is_RGB = 0;
      if (in_short == 1) {
	black_is_zero = true;
      }
      else {
	black_is_zero = false;
      }
    }
    break;
    
  case RowsPerStrip:
    if (count != 1 || (type != SHORT && type != LONG))  {
      printf("Tag %d had a hex type %X and decimal count %ld\n%s", cur_id,
	     type, count, "These values violate the tiff specification\n");
      printf("Futile to proceed with tag, returning failure\n");
      free(data);
      return -cur_id;
    }
    if (rows_per_strip != 0) {
      printf("Repeated reading of the rows per image data chunk in tag %d\n",
	     cur_id);
      printf("Existing value is %ld\n", rows_per_strip);
      printf("Stoping tag reading, returning failure\n");
      free(data);
      return -cur_id;
    }
    if (type == SHORT) {
      in_short = *(unsigned short*) my_read(data, 2);
      rows_per_strip = in_short;
    }
    else {
      in_long = *(unsigned long*) my_read(data, 4);
      rows_per_strip = in_long;
    }    
    break;
    
  case StripOffsets:
    if (type != SHORT && type != LONG)  {
      printf("Tag %d had a hex type %X and decimal count %ld\n%s", cur_id,
	     type, count, "These values violate the tiff specification\n");
      printf("Futile to proceed with tag, returning failure\n");
      free(data);
      return -cur_id;
    }
    if (!strip_offsets.empty()) {
      printf("Repeated reading of the image data offsets in tag %d\n", cur_id);
      printf("Stoping tag reading, returning failure\n");
      free(data);
      return -cur_id;
    }
    if (type == SHORT) {
      for (unsigned int i = 0; i < count; i++) {
	in_short = *(short *) my_read(location, 2);
	strip_offsets.push_back(in_short);
	((short *)location)++;
      }
    }
    else {
      for (unsigned int i = 0; i < count; i++) {
	in_long = *(long *) my_read(location, 4);
	strip_offsets.push_back(in_long);
	((long *)location)++;
      }
    }
    break;
    
  case StripByteCounts:
    if (type != SHORT && type != LONG)  {
      printf("Tag %d had a hex type %X and decimal count %ld\n%s", cur_id,
	     type, count, "These values violate the tiff specification\n");
      printf("Futile to proceed with tag, returning failure\n");
      free(data);
      return -cur_id;
    }
    if (!strip_byte_counts.empty()) {
      printf("Repeated reading of the image data byte lengths in tag %d\n",
	     cur_id);
      printf("Stoping tag reading, returning failure\n");
      free(data);
      return -cur_id;
    }
    if (type == SHORT) {
      for (unsigned int i = 0; i < count; i++) {
	in_short = *(short *) my_read(location, 2);
	strip_byte_counts.push_back(in_short);
	((short *)location)++;
      }
    }
    else {
      for (unsigned int i = 0; i < count; i++) {
	in_long = *(long *) my_read(location, 4);
	strip_byte_counts.push_back(in_long);
	((long *)location)++;
      }
    }
    break;
    
  case BitsPerSample:
    if (type != SHORT || (count != 1 && count != 3)) {
      printf("Tag %d had a hex type %X and decimal count %ld\n", cur_id, type,
	     count);
      printf("These values violate the tiff specification or were not\n%s",
	     "forseen for this implementation (if count value not 1 or 3)\n");
      printf("Futile to proceed with tag, returning failure\n");
      free(data);
      return -cur_id;
    }
    for (unsigned int i = 0; i < count; i++) {
      in_short = *(short *) my_read(location, 2);
      if (in_short != 8) {
	printf("Tag %d indicates that image data is not a byte a sample,\n",
	       cur_id);
	printf("as expected.  Size in bits is instead %d\n", in_short);
	printf("Don't proceed with tag read, returning failure\n");
	free(data);
	return -cur_id;
      }
    }
    if (count == 3 && is_RGB == 0) {
      printf("Tag %d indicates contradictory grayscale vs RGB data\n", cur_id);
      printf("Existing value of is_RGB was %d, while the tag\n", is_RGB);
      printf("holds data for %ld samples for a pixel\n", count);
      printf("Don't proceed with tag read, returning failure\n");
      free(data);
      return -cur_id;
    }
    if (count == 1 && is_RGB == 1) {
      printf("Tag %d indicates contradictory grayscale vs RGB data\n", cur_id);
      printf("Existing value of is_RGB was %d, while the tag\n", is_RGB);
      printf("holds data for %ld samples for a pixel\n", count);
      printf("Don't proceed with tag read, returning failure\n");
      free(data);
      return -cur_id;
    }
    break;
    
  case SamplesPerPixel:
    if (count != 1 || type != SHORT) {
      printf("Tag %d had a hex type %X and decimal count %ld\n%s", cur_id,
	     type, count, "These values violate the tiff specification\n");
      printf("Futile to proceed with tag, returning failure\n");
      free(data);
      return -cur_id;
    }
    in_short = *(short *) my_read(data, 2);
    if (in_short != 1 && in_short != 3) {
      printf("Tag %d had a indicates %d samples per pixel\n%s", cur_id,
	     in_short, "A value not foreseen by this implementation\n");
      printf("Futile to proceed with tag, returning failure\n");
      free(data);
      return -cur_id;
    }
    if (in_short == 3 && is_RGB == 0) {
      printf("Tag %d indicates contradictory grayscale vs RGB data\n", cur_id);
      printf("Existing value of is_RGB was %d, while the tag\n", is_RGB);
      printf("holds data for %d samples for a pixel\n", in_short);
      printf("Don't proceed with tag read, returning failure\n");
      free(data);
      return -cur_id;
    }
    if (in_short == 1 && is_RGB == 1) {
      printf("Tag %d indicates contradictory grayscale vs RGB data\n", cur_id);
      printf("Existing value of is_RGB was %d, while the tag\n", is_RGB);
      printf("holds data for %d samples for a pixel\n", in_short);
      printf("Don't proceed with tag read, returning failure\n");
      free(data);
      return -cur_id;
    }
    break;
  }  
  
  
  free(data);
  return cur_id;
}



bool process_image(const int tifn, const char* bmp_name,
		   const unsigned int top, const unsigned int bottom,
		   const unsigned int left, const unsigned int right,
		   const int tiff_end) {

  printf("Beginning processing image\n");
  // Define the buffer that will be used to store image data.
  unsigned int size_bound = 3000;
  
  unsigned int bmp_rows = (bottom - top) + 1;
  unsigned int bmp_cols = (right - left) + 1;

  unsigned char* const data = (unsigned char*) malloc(size_bound);
  if (data == NULL) {
    printf("Failed to allocate memory for variable data in process_image %s",
	   "subfunction.\n");
    printf("Futile to continue.  Return failure\n");
    return false;
  }

  unsigned char* const grays = (unsigned char*) malloc(bmp_cols);
  if (grays == NULL) {
    printf("Failed to allocate memory for variable greens in process_image %s",
	   "subfunction.\n");
    printf("Futile to continue.  Return failure\n");
    free(data);
    return false;
  }

  // The length of each row must be a multiple of 4 bytes long, regardless
  // of the number of pixels, so find that length
  int row_length = bmp_cols;
  while((row_length)%4!=0) {
    row_length++;
  }
  fstream bmp;

  // First, attempt to open an existing file.
  bmp.open(bmp_name, ios::in | ios::out | ios::binary | ios::trunc);
  if (!bmp.is_open()) {
    printf("Failed to open file to store bmp file in\n");
    printf("Futile to continue.  Return failure\n");
    free(data);
    free(grays);
    return false;
  }
  // Write the BMP's header
  
  //Indicate that this is a bitmap
  bmp.put('B');
  bmp.put('M');
  //Write the bitmap size
  sbuf_four.in_long = (unsigned long)(1078 + bmp_rows*row_length);
  bmp.write(sbuf_four.buf, 4);
  //Write the reserved bits
  sbuf_four.in_long = 0;
  bmp.write(sbuf_four.buf, 4);
  //Write the offset from the beginning of the file to the data
  sbuf_four.in_long = 1078;
  bmp.write(sbuf_four.buf, 4);
  //Write the size of the bitmap header
  sbuf_four.in_long = 40;
  bmp.write(sbuf_four.buf, 4);
  //Write the width of the image in pixels
  sbuf_four.in_long = bmp_cols;
  bmp.write(sbuf_four.buf, 4);
  //Write the height of the image in pixels
  sbuf_four.in_long = bmp_rows;
  bmp.write(sbuf_four.buf, 4);
  //Write the number of target planes
  sbuf_two.in_short = 1;
  bmp.write(sbuf_two.buf, 2);
  //Write the number of bits per pixel
  sbuf_two.in_short = 8;
  bmp.write(sbuf_two.buf, 2);
  //Write the type of compression
  sbuf_four.in_long = 0;
  bmp.write(sbuf_four.buf, 4);
  //Write the size of the image data in bytes
  sbuf_four.in_long = bmp_rows * row_length;
  bmp.write(sbuf_four.buf, 4);
  //Write the horizontal conversion from pixels to meters
  sbuf_four.in_long = 0;
  bmp.write(sbuf_four.buf, 4);
  //Write the vertical conversion from pixels to meters
  sbuf_four.in_long = 0;
  bmp.write(sbuf_four.buf, 4);
  //Write the number of colors used
  sbuf_four.in_long = 256;
  bmp.write(sbuf_four.buf, 4);
  //Write the number of important colors used
  sbuf_four.in_long = 0;
  bmp.write(sbuf_four.buf, 4);

  //Write the color table.
  unsigned char ch = 0;
  for (int index = 0; index < 256; ch++, index++) {
    bmp.put(ch);
    bmp.put(ch);
    bmp.put(ch);
    bmp.put(0);
  }

  
  unsigned long unread_data = 0;
  unsigned char* location = data;
  unsigned long unread_strip = 0;
  unsigned long strip_offset = 0;
  unsigned long strip_count = 0;
  int last_read_size = 0;
  
  list<unsigned long>::const_iterator strip_loc;
  list<unsigned long>::const_iterator strip_size;

  unsigned char value1;
  unsigned char value2;
  unsigned char value3;

  unsigned int r;
  unsigned int c;
  
  unsigned char* gray_ptr = grays;

  // Recurse through the rows.
  for (r = 0; r < tiff_row_num; r++) {
    // Reset the buffer pointers to the front of the row for the bitmap
    gray_ptr = grays;
    
    // Determine if the image data is now located in a new strip.
    if (r % rows_per_strip == 0) {
      strip_count++;
      if (unread_strip != 0 || unread_data != 0) {
	printf("Warning: Unexpected behavior.  Row %d is divisible by the %s",
	       r, "number of\n");
	printf("  rows in a strip, %ld, yet the amount of data unread in %s",
	       rows_per_strip, "the\n");
	printf("  strip is %ld and in the local buffer is %ld\n", unread_strip,
	       unread_data);
	printf("  Occured when loading the %ldth strip (indexed from 1)\n",
	       strip_count);
      }
      if (r == 0) {
	// If the process is just beginning, initialize the data to the first
	// strip of image data.
	strip_loc = strip_offsets.begin();
	strip_size = strip_byte_counts.begin();
      }
      else {
	// If it is instead a new strip location, iterate further through the
	// list of their locations and size.
	strip_loc++;
	strip_size++;
	if (strip_loc == strip_offsets.end() ||
	    strip_size == strip_byte_counts.end()) {
	  printf("ERROR: Reading row %d of %ld, on loading the %ldth strip\n",
		 r, rows_per_strip, strip_count - 1);
	  printf("  (all indexed from 0) the list of the sizes and/or %s",
		 "locations of the strips\n");
	  printf("  lacked data for more strips.  There are %d strip sizes\n",
		 strip_byte_counts.size());
	  printf("  and %d strip locations.  If these are equal and too\n%s%s",
		 strip_offsets.size(),
		 "  small, check for output indicating a second IFD, an\n",
		 "  advanced functionality not implemented this version\n");
	  printf("Futile to continue.  Return failure\n");
	  free(data);
	  free(grays);
	  bmp.close();
	  return false;	  
	}
      }

      unread_strip = *strip_size;
      strip_offset = *strip_loc;
      if (lseek(tifn, strip_offset, SEEK_SET) > tiff_end) {
	printf("Failed updating the buffer opening a new data strip.\n");
	printf("In processing image data, file pointer sent past file %s",
	       "end.\n");
	printf("Error occured evaluating the %dth row\n", r);
	printf("Futile to continue.  Return failure\n");
	free(data);
	free(grays);
	bmp.close();
	return false;
      }
    }
    
    for (c = 0; c < tiff_col_num; c++) {

      // Determine if the buffer needs to be updated and if so to what.
      if (unread_data == 0) {
	
	if (unread_strip == 0) {
	  printf("No data left in strip in the middle of reading a row.\n");
	  printf("  There should be %ld rows and %ld cells in each row\n",
		 tiff_row_num, tiff_col_num);
	  printf("  There are %ld bytes of data in this strip.\n",
		 *strip_size);
	  printf("  The offsets indicate %d strips, and the size %d strips\n",
		 strip_offsets.size(), strip_byte_counts.size());
	  printf("  Strip should contain full rows of data\n");
	  printf("  Error occured evaluating row %d and column %d.\n", r, c);
	  printf("  Futile to continue.  Return failure\n");
	  free(data);
	  free(grays);
	  bmp.close();
	  return false;
	}
	
	// Read in new amount of data into the buffer.  Store the size and set
	// this size to the amount of unread data in the buffer.
	last_read_size = read(tifn, data, (unread_strip > size_bound ?
					   size_bound : unread_strip));
	
	// Update the variables that consider the data left in the buffer and
	// the strip.
	unread_data = last_read_size;
	unread_strip -= unread_data;
	location = data;
      }
    
      // Store the data for a single pixel if it meets the location criteria
      // within the picture.  Increment the pointer and unread buffer data
      // regardless.

      if (r >= top && r <= bottom && c >= left && c <= right) {
	// BOOGABOOGA  Every first instance of a variable above must be changed
	// to r or c to truly capture rectangular subset selection criteria.
	// Also, but below must be changed to output to a .bmp, not to a text
	// file for debugging.
	
	if (is_RGB == 1) {
	  // Color data, 3 pixels will be stored.
	  value1 = *location;
	  location++;
	  value2 = *location;
	  location++;
	  value3 = *location;
	  location++;
	  
	  if (!black_is_zero) {
	    value1 = 255 - value1;
	    value2 = 255 - value2;
	    value3 = 255 - value3;
	  }

	  // Zero is a reserved data value.  Set any zero values to instead
	  // be 1.
	  if (value1 == 0) {
	    value1 = 1;
	  }
	  if (value2 == 0) {
	    value2 = 1;
	  }
	  if (value3 == 0) {
	    value3 = 1;
	  }

	  *gray_ptr = conv_to_grayscale(value1, value2, value3);
	  gray_ptr++;
	  unread_data -= 3;
	}
	else {
	  // Greyscale data, 1 pixel will be stored.
	  value1 = *location;

	  if (!black_is_zero) {
	    value1 = 255 - value1;
	  }

	  // Zero is a reserved data value.  Set any zero values to instead
	  // be 1.
	  if (value1 == 0) {
	    value1 = 1;
	  }

	  location++;

	  *gray_ptr = value1;
	  gray_ptr++;
	  
	  unread_data--;
	}
      }
      else {
	// Still need to move in the buffer and update the unread info logs.
	if (is_RGB == 1) {
	  location += 3;
	  unread_data -= 3;
	}
	else {
	  location++;
	  unread_data--;
	}
      }
    }

    /// Have just read an entire row.  Need to dump the red, green, and blue
    /// arrays into the .bmp

    // Determine if the current row maps to a row that exists in the .bmp file
    if (r >= top && r <= bottom) {
      if (((bmp_rows - 1) - (r - top)) < 0 ||
	  ((bmp_rows - 1) - (r - top)) >= bmp_rows) {
	printf("Error: Pointer into the bmp for the %dth row points outside\n",
	       r);
	printf("  the expected range.  The .bmp should have %d rows and %d\n",
	       bmp_rows, ((bmp_rows - 1) - (r - top)));
	printf("  would be the row offset (indexed from 0) that the .bmp %s\n",
	       "would be pointed");
	printf("  to the beginning of.\n");
	printf("  Futile to continue.  Return failure\n");	
	free(data);
	free(grays);
	return false;
      }
      bmp.seekp(1078 + ((bmp_rows - 1) - (r - top)) * row_length, ios::beg);
      
      // Now write the pixel data to the bitmap.
      for (unsigned int i = 0; i < bmp_cols; i++) {
	bmp.put(*(grays + i));
      }
    }	
  }

  printf("Done processing image.  Reached row %d and column %d.\n", r, c);

  free(data);
  free(grays);
  bmp.close();
  return true;
}




int read_header(const string header_name, double* x_origin, double* y_origin,
		int* x_len, int* y_len) {

  if (header_name.size() <= 4 ||
      ((header_name.substr(header_name.size() - 4, 4) != ".HDR" &&
	(header_name.substr(header_name.size() - 4, 4) != ".Hdr" &&
	 header_name.substr(header_name.size() - 4, 4) != ".hdr")) &&
       (header_name.substr(header_name.size() - 4, 4) != ".TXT" &&
	(header_name.substr(header_name.size() - 4, 4) != ".Txt" &&
	 header_name.substr(header_name.size() - 4, 4) != ".txt"))) ) {
    return UN_OPENED;
  }
       
  ifstream in;

  long double rmse;
  long double horiz_res;

  in.open(header_name.c_str());

  if (!in.is_open()) {
    cout << "CRITICAL: Unable to open the header \"" << header_name << "\"\n";
    return UN_OPENED;
  }

  string data_id("");
  string goal;
  string to_seek;


  to_seek = string("RASTER_ORDER");
  
  while (!in.eof() && data_id.substr(0, to_seek.size()) != to_seek) {
    in >> data_id;
  }
  if (in.eof()) {
    cout << "CRITICAL: Unable to open value " << to_seek << "\n";
    return ERROR;
  }

  goal = string("LEFT_RIGHT/TOP_BOTTOM");
  in >> data_id;
  if(data_id != goal) {
    cout << "ERROR: Expected value for " << to_seek << " was not found.\n"
	 << "  Expected \"" << goal << "\" and got \"" << data_id << "\"\n";
  }



  to_seek = string("SAMPLES_AND_LINES");
  
  while (!in.eof() && data_id.substr(0, to_seek.size()) != to_seek) {
    in >> data_id;
  }
  if (in.eof()) {
    cout << "CRITICAL: Unable to open value " << to_seek << "\n";
    return ERROR;
  }

  in >> *x_len;
  in >> *y_len;
  


  to_seek = string("HORIZONTAL_DATUM");
  
  while (!in.eof() && data_id.substr(0, to_seek.size()) != to_seek) {
    in >> data_id;
  }
  if (in.eof()) {
    cout << "CRITICAL: Unable to open value " << to_seek << "\n";
    return ERROR;
  }

  goal = string("NAD83");
  in >> data_id;
  if(data_id != goal) {
    cout << "ERROR: Expected value for " << to_seek << " was not found.\n"
	 << "  Expected \"" << goal << "\" and got \"" << data_id << "\"\n";
  }



  to_seek = string("HORIZONTAL_COORDINATE_SYSTEM");
  
  while (!in.eof() && data_id.substr(0, to_seek.size()) != to_seek) {
    in >> data_id;
  }
  if (in.eof()) {
    cout << "CRITICAL: Unable to open value " << to_seek << "\n";
    return ERROR;
  }

  goal = string("UTM");
  in >> data_id;
  if(data_id != goal) {
    cout << "ERROR: Expected value for " << to_seek << " was not found.\n"
	 << "  Expected \"" << goal << "\" and got \"" << data_id << "\"\n";
  }



  to_seek = string("COORDINATE_ZONE");
  
  while (!in.eof() && data_id.substr(0, to_seek.size()) != to_seek) {
    in >> data_id;
  }
  if (in.eof()) {
    cout << "CRITICAL: Unable to open value " << to_seek << "\n";
    return ERROR;
  }
  
  goal = string("11");
  in >> data_id;
  if(data_id != goal) {
    cout << "ERROR: Expected value for " << to_seek << " was not found.\n"
	 << "  Expected \"" << goal << "\" and got \"" << data_id << "\"\n";
  }


  to_seek = string("HORIZONTAL_UNITS");
  
  while (!in.eof() && data_id.substr(0, to_seek.size()) != to_seek) {
    in >> data_id;
  }
  if (in.eof()) {
    cout << "CRITICAL: Unable to open value " << to_seek << "\n";
    return ERROR;
  }
  
  goal = string("METERS");
  in >> data_id;
  if(data_id != goal) {
    cout << "ERROR: Expected value for " << to_seek << " was not found.\n"
	 << "  Expected \"" << goal << "\" and got \"" << data_id << "\"\n";
  }


  to_seek = string("HORIZONTAL_RESOLUTION");
  
  while (!in.eof() && data_id.substr(0, to_seek.size()) != to_seek) {
    in >> data_id;
  }
  if (in.eof()) {
    cout << "CRITICAL: Unable to open value " << to_seek << "\n";
    return ERROR;
  }

  in >> horiz_res;
  if (horiz_res != 1.000000) {
    printf("WARNING: Horizontal resolution is %Lf\n", horiz_res);
  }


  to_seek = string("XY_ORIGIN");

  
  while (!in.eof() && data_id.substr(0, to_seek.size()) != to_seek) {
    in >> data_id;
  }
  if (in.eof()) {
    cout << "CRITICAL: Unable to open value " << to_seek << "\n";
    return ERROR;
  }

  in >> *x_origin;
  in >> *y_origin;
  
  if (fmod(*x_origin, 1.000000) != (long double)0.0) {
    printf("WARNING: X origin has non-unit value %f\n", *x_origin);
  }
  if (fmod(*y_origin, 1.000000) != (long double)0.0) {
    printf("WARNING: Y origin has non-unit value %f\n", *y_origin);
  }


  to_seek = string("RMSE_XY");

  
  while (!in.eof() && data_id.substr(0, to_seek.size()) != to_seek) {
    in >> data_id;
  }
  if (in.eof()) {
    cout << "CRITICAL: Unable to open value " << to_seek << "\n";
    return ERROR;
  }

  in >> rmse;

  cout << "Successfully read file \"" << header_name << "\"\n";
  printf("x_origin is %.2f y_origin is %.2f and res is %.4Lf\n", *x_origin,
	 *y_origin, horiz_res);

  return SUCCESS;
}




int main(int argc, char *argv[]) {
  if (argc != 2) {
    printf("Usage: %s [filename] - convert tiff [filename] to a text file\n",
	   argv[0]);
    return 0;
  }

  string header_name(argv[1]);
  string tiff_name = header_name.substr(0, header_name.size() - 4);
  
  if (header_name.substr(header_name.size() - 4, 4) == string(".hdr") ||
      header_name.substr(header_name.size() - 4, 4) == string(".txt")) {
    // Assume extension for tiff is also lowwer caps.
    tiff_name += string(".tif");
  }
  else if (header_name.substr(header_name.size() - 4, 4) == string(".HDR") ||
	   header_name.substr(header_name.size() - 4, 4) == string(".TXT")) {
    // Assume extension for tif also upper caps
    tiff_name += string(".TIF");
  }

  double x_origin = 0;
  double y_origin = 0;
  int x_len = 0;
  int y_len = 0;
  
  if (read_header(header_name, &x_origin, &y_origin, &x_len, &y_len) !=
      SUCCESS) {
    cout << "FAILURE: Failed to read header file \"" << header_name << "\"\n";
    return 0;
  }

  if (x_origin <= 0 || y_origin <= 0 || x_len <= 0 || y_len <= 0 ||
      fmod(x_origin, 1.0) != 0 || fmod(y_origin, 1.0) != 0) {
    cout << "FAILURE: Unexpected data read from header file \"" << header_name
	 << "\"\n";
  }

  /// Store lists of all the x and y pixel offsets that correspond to a UTM 
  /// northing or easting divisible by 1000 into lists.

  vector<int> x_pixel(0);
  vector<int> y_pixel(0);
  int pixel;  // THIS MUST BE SIGNED.  IT CAN BE NEGATIVE.
  bool unbounded_top = true;
  bool unbounded_bottom = false;
  bool unbounded_left = true;
  bool unbounded_right = false;
  
  pixel = 0;
  if (fmod(x_origin + pixel, 1000.0) == 0) {
    // The very left edge of the map has a pixel divisible by 1000.  This map
    // will stand alone and not need to be merged with a map to the left.
    unbounded_left = false;
    pixel += 1000;
  }

  x_pixel.push_back(pixel);

  // Special case that trips a bool.  Set the bool and increment past it.
  // No matter what, this value must be added to the list.
  while (fmod(x_origin + pixel, 1000.0) != 0) {
    pixel++;
  }
  while (pixel <= x_len) {
    x_pixel.push_back(pixel);
    pixel += 1000;
  }
  
  // Add in pixels to allow creation of partial maps of the 1-km partitions
  // that extend beyond the DOQ range.
  if (x_pixel.back() != x_len) {
    x_pixel.push_back(x_len);
    unbounded_right = true;
  }

  // Now do the y pixels.  Unlike the x pixels, the y_origin value defines the
  // maximal northing (rather than the minimal easting for the x_origin).  We
  // will need to know if either the bottom pixel, or the pixel just above the
  // top row is a bound.
  pixel = -1;
  if (fmod(y_origin - pixel, 1000.0) == 0) {
    // The very left edge of the map has a pixel divisible by 1000.  This map
    // will stand alone and not need to be merged with a map to the left.
    unbounded_top = false;
    pixel += 1000;
  }

  y_pixel.push_back(pixel);
  
  while (fmod((y_origin - pixel), 1000.0) != 0) {
    pixel++;
  }
  while (pixel <= y_len - 1) {
    y_pixel.push_back(pixel);
    pixel += 1000;
  }
  
  // Add in pixels to allow creation of partial maps of the 1-km partitions
  // that extend beyond the DOQ range.
  if (y_pixel.back() != y_len - 1) {
    y_pixel.push_back(y_len - 1);
    unbounded_bottom = true;
  }

  char x_buf[7];
  char y_buf[7];
  int temp;
  string bmp_name;

  cout << "x_pixel = {";
  for (unsigned int i = 0; i < x_pixel.size(); i++) {
    cout << x_pixel[i] << " ";
  }
  cout << "}\n";
  cout << "y_pixel = {";
  for (unsigned int i = 0; i < y_pixel.size(); i++) {
    cout << y_pixel[i] << " ";
  }
  cout << "}\n";

  cout << "unbounded_top is " << unbounded_top << "\n";
  cout << "unbounded_bottom is " << unbounded_bottom << "\n";
  cout << "unbounded_left is " << unbounded_left << "\n";
  cout << "unbounded_right is " << unbounded_right << "\n";
  


  for(unsigned int i = 1; i < x_pixel.size(); i++) {
    if (i == 1 && unbounded_left) {
      // On the far left in a square km that extends further left.  The name
      // for the eastings of this zone must come from the UTM of the next zone
      // over, rather than the leftmost pixel.
      temp = sprintf(x_buf, "%d", (((int)x_origin + x_pixel[i]) / 1000) - 1);
      x_buf[temp] = 0;
    }
    else {
      // Get the easting name contribution from the UTM of the leftmost pixel
      // in the region.
      temp = sprintf(x_buf, "%d", ((int)x_origin + x_pixel[i - 1]) / 1000);
      x_buf[temp] = 0;
    }

    for(unsigned int j = 1; j < y_pixel.size(); j++) {
      if (j == (y_pixel.size() - 1) && unbounded_bottom) {
	// On the far bottom in a square km that extends further left.  The
	// name for the eastings of this zone must come from the UTM of the
	// next zone over, rather than the leftmost pixel.
	temp = sprintf(y_buf, "%d",
		       (((int)y_origin - y_pixel[j - 1]) / 1000) - 1);
	y_buf[temp] = 0;
      }
      else {
	// Get the easting name contribution from the UTM of the leftmost pixel
	// in the region.
	temp = sprintf(y_buf, "%d", ((int)y_origin - y_pixel[j]) / 1000);
	y_buf[temp] = 0;
      }

    
      /// Compose the name of the .bmp output file.
      bmp_name = string("");

      // Input the name of the UTM Zone.  For now, this is held to be constant.
      bmp_name += string("11Z_");
      // Add the identification for the Northings.
      bmp_name += string(y_buf) + string("N_");
      // Add the identification for the Eastings.
      bmp_name += string(x_buf) + string("E");
      // Add identifiers if the image is only a fragment.
      if (i == 1 && unbounded_left) {
	bmp_name += string("_L");
      }
      if (i == x_pixel.size() - 1 && unbounded_right) {
	bmp_name += string("_R");
      }
      if (j == 1 && unbounded_top) {
	bmp_name += string("_T");
      }
      if (j == y_pixel.size() - 1 && unbounded_bottom) {
	bmp_name += string("_B");
      }
      // Add the extension.
      bmp_name += string(".bmp");
      
      /*
      cout << "The tiff has origin (" << fixed << x_origin << ", " << fixed
	   << y_origin << ")\n" << "   it would make a bitmap \"" << bmp_name
	   << "\" within pixels\n" << "   (" << x_pixel[i - 1] << ", "
	   << y_pixel[j - 1] + 1 << ")  (" << x_pixel[i] - 1 << ", "
	   << y_pixel[j] << ")\n\n";
      */

      if (!stuff_data(tiff_name.c_str(), bmp_name.c_str(), y_pixel[j - 1] + 1,
		      y_pixel[j], x_pixel[i - 1], x_pixel[i] - 1, x_len,
		      y_len)) {
	cout << "FAILURE: Unable to make bitmap " << bmp_name << " from the\n"
	     << "  tiff file " << tiff_name << ".  The following were inputs\n"
	     << "    top pixel offset: " << y_pixel[j - 1] + 1 << "\n"
	     << "    bottom pixel offset: " << y_pixel[j] << "\n"
	     << "    left pixel offset: " << x_pixel[i - 1] + 1 << "\n"
	     << "    right pixel offset: " << x_pixel[i] - 1 << "\n"
	     << "    x axis length: " << x_len << "\n"
	     << "    y axis length: " << y_len << "\n";
      }
    }
  }
  


  //  return 0;
  
  /*

  cout << "\nBINARY/HEX TEST\n";

  cout << "sizeof(unsigned short) = " << sizeof(unsigned short) << "\n";
  cout << "sizeof(unsigned long) = " << sizeof(unsigned long) << "\n";
  cout << "sizeof(char) = " << sizeof(char) << "\n";
  cout << "sizeof(unsigned char) = " << sizeof(unsigned char) << "\n\n";
  
  buf_four.in_long = 337;

  cout << "buf_four.in_long = " << buf_four.in_long << "\n";
  cout << "&(buf_four.in_long) = " << &(buf_four.in_long) << "\n";
  cout << "buf_four.buf = " << buf_four.buf << "\n";
  cout << "&(buf_four.buf) = " << &(buf_four.buf) << "\n";

  for (int i = 0; i < 4; i++) {
    cout << "(int)buf_four.buf[" << i << "] = " << hex
	 << (int)buf_four.buf[i] << "\n";
  }

  unsigned char* c = buf_four.buf;
  cout << "hex (int)c = " << hex << (int)c << "\n";
  cout << "hex (int)*c = " << hex << (int)*c << "\n";
  c++;
  cout << "c++\n";
  cout << "hex (int)c = " << hex << (int)c << "\n";
  cout << "hex (int)*c = " << hex << (int)*c << "\n";
  c++;
  cout << "c++\n";
  cout << "hex (int)c = " << hex << (int)c << "\n";
  cout << "hex (int)*c = " << hex << (int)*c << "\n";
  c++;
  cout << "c++\n";
  cout << "hex (int)c = " << hex << (int)c << "\n";
  cout << "hex (int)*c = " << hex << (int)*c << "\n";
  c--;
  cout << "c--\n";
  unsigned char cc(1);
  *c = cc;
  cout << "*c = unsigned char(1)\n";
  cout << "hex c = " << hex << c << "\n";
  cout << "hex *c = " << hex << *c << "\n";
  cout << "buf_four.in_long = " << dec << buf_four.in_long << "\n";
  
  for (int i = 0; i < 4; i++) {
    cout << "(int)buf_four.buf[" << i << "] = " << hex
	 << (int)buf_four.buf[i] << "\n";
  }

  cout << "print_as_bin(337)" << "\n";
  print_as_bin(337);
  print_as_bin(55528);
  print_as_bin(16454);
  print_as_bin(buf_four.in_long);
  cout << "char(0x49) is " << char(0x49) << "\n";
  cout << "char(0x94) is " << char(0x94) << "\n";
  cout << "char(0x4D) is " << char(0x4D) << "\n";
  cout << "char(0xD4) is " << char(0xD4) << "\n";
  */
  return 0;
}
