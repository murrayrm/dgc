#include <iostream>
#include <string>

#include <stdio.h>

// need the serial port interface to write...
#include "../serial/serial.h"
#include "steerCar.h"



using namespace std;

#define COUNTS_PER_REV  8000
#define COM_PORT        0
#define BUFF_LEN        3000
#define STEER_EOL       '\n'    // End of the line for us.
#define PROMPT_LEN      2       // Length (in characters) of the prompt that is expected.

// Strings to use (look at http://www.linuxselfhelp.com )


int main () {

    char inputBuff[BUFF_LEN];
    string outString = (string) "";
    int readLength;             // Length we read from serial (returned by serial_read_*)
    int readLines;              // Number of lines that have been read following a command.
    char c;
    printf("a;lksdfj;asd: %s\n\n\n", (steerCmds[0]).cmd.c_str());

    // Initialize the serial port
    if (serial_open_advanced(
                COM_PORT, 
                B9600, 
                (SR_PARITY_DISABLE | SR_SOFT_FLOW_INPUT | SR_SOFT_FLOW_OUTPUT | SR_ONE_STOP_BIT)
                ) != 0) 
    {
        fprintf(stderr, "Can't open serial port\n");
        return 0;
    }
#if 0
    if (serial_write(COM_PORT, (char *) trev.cmd.c_str(), trev.cmd.length(), SerialBlock) != (int) trev.cmd.length()) {
        /* Note that serial_write does not change trev.c_str() */
        fprintf(stderr, "Not everything was written\n");
        return 0;
    }
#endif 

    while (1) {
        readLines = 0; 
        //while (readLines < trev.numLines || serial_data_length(COM_PORT) > 0) 
        //sleep(SEC_TIMEOUT);           // time to wait for response from servo (can ignore and keep inputting data)
        while (serial_data_length(COM_PORT) > 0) 
        {
        //usleep(5000);
            if (
                    (
                     (readLength = serial_read_until(COM_PORT, inputBuff, (char) STEER_EOL, SEC_TIMEOUT, BUFF_LEN - 1)) >= MIN_LINE_LEN
                     && readLength <= MAX_LINE_LEN
                     ) 
                    || 
                    (readLength = serial_read(COM_PORT, inputBuff, BUFF_LEN - 1)) >= PROMPT_LEN
               ) 
            {
                inputBuff[readLength] = '\0';   // Make it a string!
                printf("%s", inputBuff);
                fflush(stdout);
            } 
            else {
                // shit happened.
                fprintf(stderr, "Bollocks\n");
                return 0;
            }
            readLines++;
            //printf("readLines: %d\n", readLines);
        }
        fflush(stdout);


        outString = (string) "";
        while ((c = (char) getchar()) != '+')
        {
            outString += c;
            if (c == '\n') {
                //printf("%s", outString.c_str());
                if (serial_write(COM_PORT, (char *) outString.c_str(), outString.length(), SerialBlock) != (int) outString.length()) {
                    /* Note that serial_write does not change trev.c_str() */
                    fprintf(stderr, "Not everything was written\n");
                    return 0;
                }
                break;
            }
        }
        if (c == '+') {
            return 0;
        }

    }

    // Shouldn't ever get here.
    return 0;

}
