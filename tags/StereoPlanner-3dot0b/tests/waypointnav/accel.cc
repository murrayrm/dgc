#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
// DATUM & cruise function protocols??
#include "waypointnav.hh"
// Driving
#include "../../moveCar/brake.h"
#include "../../moveCar/accel.h"
#include "../../steerCar/steerCar.h"
// Added the following for waypoint following
#include "../../latlong/LatLong.h"
#include "../../Arbiter/Arbiter.h"
#include "gui-client.h"

#define VISUALIZATION_GUI 1

const float brakeVelo = 0.7;

extern int SIM;
extern int PRINT_VOTES;

// ADDED BY MIKE THIELMAN for Sparrow display
extern double waydisp_arbiter_p[];
extern double waydisp_arbiter_v[];
extern int    waydisp_arbiter_g[];

// add final votes to Sparrow display
extern double waydisp_arbiter_phi;
extern double waydisp_arbiter_velo;
extern int    waydisp_arbiter_good;
extern double waydisp_arbiter_yaw;
extern int    waydisp_arbiter_count;

int InitAccel(DATUM &d) {

  d.accelValid = FALSE;
  d.drivingValid = TRUE;

  if (!SIM) {
    cout << "Initializing brake"<< endl;
    init_brake();
    cout << "Initializing gas" << endl;
    init_accel();
  }

  cruise_init();
  return TRUE; // success

} // end InitAccel()


// This function takes in the state data of the vehicle and commands
// actuators. In case of waypoint following (added 11/30/2003) the function
// will commannd both steering and throttle/brake actuators. 
void AccelLoop(DATUM &d) {

  // File used by the gui client
  FILE* log_fp;

  timeval now;
  float accel, curSpeed; 
  float steer = 100;
  ofstream accelFile( (string("testRuns/test") 
		       + d.TestNumberStr 
		       + ".dat").c_str());
  ofstream gpsLogFile( "testRuns/logged_waypoints.gps" );

  bool MAKE_NICE_DATA_FOR_ALEX = false;
  ofstream imuLogFile( (string("testRuns/imu") 
			  + d.TestNumberStr 
			  + ".dat").c_str());
  bool runArbiter = true;
  int arbiter_count = 0;

  accelFile << setiosflags(ios::showpoint) << setprecision(10);
  accelFile << "% Target Velocity (m/s) = " << d.targetVelocity << endl;
  accelFile << "% Time(s)\taccel ([-1,1])\tsteer ([-1,1])\tSpeed(m/s)" 
	    << "\tEasting\tNorthing\tAltitude\tEastVel\tNorthVel\tVertVel" 
	    << "\tOBD Speed\tOBD RPM" << endl;
  gpsLogFile << setiosflags(ios::showpoint) << setprecision(10);
  if( MAKE_NICE_DATA_FOR_ALEX ) {
    imuLogFile << setiosflags(ios::showpoint) << setprecision(10);
    imuLogFile << "% time(sec)\ttime(usec)\tdvx\tdvy\tdvz\tdtx\tdty\tdtz\n";
  }
  if(VISUALIZATION_GUI){
    log_fp = gui_client_create_log_file("state-estimator");
  }

  // for IMU data logging
  long last_sec = 0;
  long last_usec = 0;
  int hz_count = 0;

  // for vote display in Sparrow
  Arbiter::combinedVoteListType combinedVotes;
  
  cout << setiosflags(ios::showpoint) << setprecision(4);
  while(d.shutdown_flag == FALSE) {

//    if( (d.stateValidFromIMU) && (d.stateValidFromGPS) ) {
//      if( (d.stateNewDataFromIMU) && (d.stateNewDataFromGPS) ) {
	if (runArbiter) {
	//if(0){
	  // lock evaluators --???????????????????????????? unlock?
	  boost::recursive_mutex::scoped_lock slGLOBAL(d.globalLock);
	  boost::recursive_mutex::scoped_lock slDFE(d.DFELock);
	  
	  // Run arbiter with the current Voter values: do what runArb() does
	  (d.demoArb).getVotes();
	  (d.demoArb).combineVotes();
	  (d.demoArb).pickBestArc();

          // ADDED BY LARS CREMEAN for Sparrow display
          // VERIFIED that these are the same as the votes that come from
	  // printCombinedVotes
	  int i=0; 
	  combinedVotes = (d.demoArb).getCombinedVoteList(); 
          for( Arbiter::combinedVoteListType::iterator 
			  iter = combinedVotes.begin(); 
			  iter != combinedVotes.end(); ++iter, ++i ) {
	    waydisp_arbiter_p[i] = iter->phi*180.0/M_PI;
	    waydisp_arbiter_v[i] = iter->velo;
	    waydisp_arbiter_g[i] = iter->goodness;
	  }
	  
	  // TODO want to be able to log all votes
          //(d.demoArb).logAllVotes(votesFile);

	  if(PRINT_VOTES){
	    (d.demoArb).printCombinedVotes();
            // sleep a while so that you can read the output
            cout << "--accel.cc: arbiter-- Sleeping so you can read my output." << endl;  
            sleep(2);
          }

	  // Finally update command values of this method
	  steer = ( (d.demoArb).getBestPhi() ) / MAX_STEER;
	  d.targetVelocity = (d.demoArb).getBestVelo();
          
	}
	else {           // No Arbiter
	  steer = 0.5;   
          d.targetVelocity = 5;    // m/s just some default value
	  // -------------------------------------
	  // OPEN LOOP TEST
	  //accel = 0.25;	  
	  // -------------------------------------
	}
        // Update the best vote in the Sparrow display
        waydisp_arbiter_phi = steer;
        waydisp_arbiter_velo = d.targetVelocity;
        waydisp_arbiter_good = (d.demoArb).getBestGoodness();
        waydisp_arbiter_yaw = d.SS.yaw;
        waydisp_arbiter_count = arbiter_count;
	arbiter_count++;

	// Run cruise controller
	accel = cruise(d.targetVelocity, d.SS.speed, d.accel);          
        
        //cout << "STEER = " << steer << " ACCEL = " << accel << endl;
        
	// Now output the result assuming we are still allowed 
	// to change the accel (i.e., we are not shutting down)
	boost::recursive_mutex::scoped_lock slDRIVING(d.drivingLock);
	if(d.drivingValid) {
	  gettimeofday(&now, NULL);
	  /*
	  cout << "Time=" << timeval_subtract(now, d.TimeZero) << ", ";
	  cout << "GPS Speed=" << d.SS.speed << ", ";
	  cout << "OBD Speed=" << d.obdSpeed << ", ";
	  cout << "RPM=" << d.obdRPM << ", ";
	  cout << "Accel=" << accel << endl;
          cout << "Steer=" << steer << endl;
	  */

	  if (!SIM) { 
 	  //if (0) { // cheat and don't connect to hardware

	    //cout << "Steering Command = " << steer << endl;	    
	    // Send command to steering actuator
	    if( -1.0 <= steer && steer <= 1.0 ) {
	      //cout << "Setting steering to " << steer << endl;
	      steer_heading(steer);
	    } else {
	      cout << "Bad Steering Command = " << steer << endl;
	    }
	  
	    // Send commands to throttle/brake actuators
	    d.accel = accel;
	    //cout << "Gas Command = " << d.accel << endl;
	    if(-1.0 <= accel && accel <= 0.0) {
	      // cout << "Setting brake to " << accel << endl; 
	      set_accel_abs_pos(0.0);
	      // set_brake_abs_pos_pot( accel, brakeVelo, brakeVelo );
	      
	    } else if(0.0 <= accel && accel <= 1.0) {
	      // cout << "Setting throttle to " << accel << endl;
	      set_accel_abs_pos(0);
	      set_accel_abs_pos(accel);
	      // set_brake_abs_pos_pot( 0.0, brakeVelo, brakeVelo );
	    } else {
	      cout << "Bad Accel Command = " << accel << endl;
	    }
	  } // end if(!SIM)
	  else {   // Simulation

            // d.steer and d.accel will be used by the simulator thread
            d.steer = steer;
            d.accel = accel;
	    //cout << "Steering ( " << steer << " ) Accel( " << d.accel << " )" << endl;
	  }
	  
	} // end if(runArbiter)

        // compare to global
//        printf( " ---accel.cc--- d.SS.easting = %10.3f\n", d.SS.easting );
//        printf( " ---accel.cc--- d.SS.northing = %10.3f\n", d.SS.northing );

      if( last_usec != d.imu_usec ) {
        last_usec = d.imu_usec;
        
        hz_count++;        

        //if( last_sec != d.imu_sec ) {
        if( 0 ) {
          last_sec = d.imu_sec;
          cerr << "hz_count = " << hz_count << endl;
          hz_count = 0;
        } 
	// Log state data
        // This is the main logfile
	gettimeofday(&now, NULL);
	accelFile << timeval_subtract(now, d.TimeZero) << "\t"
		  << d.accel << "\t"
	          << d.steer << "\t"
		  << d.SS.speed << "\t"
		  << d.SS.easting << "\t"
		  << d.SS.northing << "\t"
		  << d.SS.altitude << "\t"
		  << d.SS.vel_e << "\t"
		  << d.SS.vel_n << "\t"
		  << d.SS.vel_u << "\t"
		  << d.obdSpeed << "\t"
		  << d.obdRPM << "\t"
		  << endl;
        // we did this to get a waypoint file
	gpsLogFile << d.SS.northing << "\t"
	           << d.SS.easting << "\t";

  if( MAKE_NICE_DATA_FOR_ALEX ) {
	// log imu data for Alex 12/29/03
        imuLogFile << d.imu_sec << "\t" 
                  << d.imu_usec << "\t"
	          << d.imu_dvx << "\t"
	          << d.imu_dvy << "\t"
	          << d.imu_dvz << "\t"
	          << d.imu_dtx << "\t"
	          << d.imu_dty << "\t"
	          << d.imu_dtz << "\t"
	           << endl;
  }
        if(VISUALIZATION_GUI){
          if(log_fp == NULL){
            fprintf(stderr,"error in the gui file of state estimator");
          }
          fprintf(log_fp,"GPS: %11f %6f %6f\n",timeval_subtract(now, d.TimeZero),100*d.SS.northing,100*d.SS.easting);
          fflush(log_fp);
        }
      } // end if(usec stuff)

 //     } // end if(d.stateNewData)

      // Sleep for a bit and do it again -- controls actuator rate
      if( !MAKE_NICE_DATA_FOR_ALEX ) {
        usleep_for(100000);
      }

//    } else {      // state not valid (else for if(d.stateValid))
      // PROBABLY WANT THIS!
      //cout << "accel.cc -- state not valid. sleeping." << endl;
//      usleep_for(100000);    // wait to do anything until we have data
//    }

  }  // end while(d.shutdown_flag == FALSE)

  if(!SIM) {
    // Main program done, set throttle to none and stop the car
    set_accel_abs_pos(0);
    //set_brake_bas_pos_pot(1.0, brakeVelo, brakeVelo);
  }

} // end AccelLoop()

//
// end accel.cc
