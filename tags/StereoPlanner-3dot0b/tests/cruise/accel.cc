#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

// DATUM
#include "cruisemain.hh"

// Driving
#include "../../moveCar/brake.h"
#include "../../moveCar/accel.h"
#include "../../steerCar/steerCar.h"

// Added the following for waypoint following
#include "WaypointFollowing.hh"
#include "../../latlong/LatLong.h"
#include "../../Arbiter/Vote.h"
#include "../../Arbiter/Voter.h"
#include "../../Arbiter/Arbiter.h"
#include "../../Arbiter/runArb.h"

int InitAccel(DATUM &d) {

  d.accelValid = FALSE;
  d.drivingValid = TRUE;
  cout << "Initializing brake"<< endl;
  init_brake();
  cout << "Initializing gas" << endl;
  init_accel();
  cruise_init();
  return TRUE; // success
}

// This function takes in the state data of the vehicle and commands
// actuators. In case of waypoint following (added 11/30/2003) the function
// will commannd both steering and throttle/brake actuators. 
void AccelLoop(DATUM &d) {

  timeval now;
  float accel, curSpeed; 
  float steer = 100;
  ofstream accelFile( (string("testRuns/test") 
		       + d.TestNumberStr 
		       + ".dat").c_str());
  bool wayptFollowing = true;         // flag to dis/enable waypt following

  accelFile << setiosflags(ios::showpoint) << setprecision(10);
  accelFile << "% Target Velocity (m/s) = " << d.targetVelocity << endl;
  accelFile << "% Time(s)\tAccel Position\tSpeed(m/s)" 
	    << "\tNorthVel\tEastVel\tNorthing\tEasting" 
	    << "\tOBD Speed\tOBD RPM" << endl;

  cout << setiosflags(ios::showpoint) << setprecision(4);

  while(d.shutdown_flag == FALSE) {
    if(d.stateValid) {
      if(d.stateNewData) {
	// new data -> change output
	// lock state
	boost::recursive_mutex::scoped_lock slSTATE(d.stateLock);

        // Added for 12/02/2003 test.
	// calculate what to do
        // The following does waypoint following if we're using waypoint
        if (wayptFollowing) {

	  // Fake the velocity for now (fakes the heading)
	  //	  d.gpsDW.data.vel_n = -1.0;
	  //	  d.gpsDW.data.vel_e = 0;

          // Get state data from GPS
	  Voter::voteListType wayPt;
	  Vector head, pos;
	  double posE, posN;
	  // heading - from velocity
	  head.N = d.gpsDW.data.vel_n ; 
	  head.E = d.gpsDW.data.vel_e ; 
	  // position - eating/northing
	  LatLong latlong(0,0);
	  latlong.set_latlon(d.gpsDW.data.lat, d.gpsDW.data.lng); 
	  latlong.get_UTM(&posE, &posN);   // conver to UTM
	  pos.E = posE;
	  pos.N = posN;
	  // speed - from velocity
	  curSpeed = sqrt(d.gpsDW.data.vel_n * d.gpsDW.data.vel_n
			  + d.gpsDW.data.vel_e * d.gpsDW.data.vel_e);

	  // Calculate new steering & velocity using state data & waypts
	  // First get commands from global estimator
	  pair<double, double> globalCommand = globalEst(head, pos, 
							 curSpeed, d.WF);

	  // Now put the command in Vote form to put into Arbiter
	  Vote wayPtVote(globalCommand.second, 250, globalCommand.first);  
	  wayPt.push_back(wayPtVote);      
	  // Values needed to call runArb
	  gettimeofday(&now, NULL);  
	  int sign = 0;       // for fcvt
	  int dec = 5;        // for fcvt
	  pair<double, double> command = runArbiter(wayPt, d.TestNumberStr, 
		    fcvt(timeval_subtract(now, d.TimeZero), 7, &dec, &sign)); 

	  // Finally update command values of this method
	  steer = command.second;              
	  d.targetVelocity = command.first;    
	}
	else {           // No waypoint following
	  steer = 100;   // high value so that the steer_heading isn't called
	}
	
	//accel = 0; // for human driving & waypoint collecting 
		accel = cruise(d.targetVelocity, d.SS.speed, d.accel);          
	
	// -------------------------------------
	// OPEN LOOP TEST
	//accel = 0.25;

	// -------------------------------------

	// now output the result assuming we are still allowed 
	// to change the accel (i.e., we are not shutting down)
	boost::recursive_mutex::scoped_lock slDRIVING(d.drivingLock);
	if(d.drivingValid) {
	  gettimeofday(&now, NULL);
	  cout << "Time=" << timeval_subtract(now, d.TimeZero) << ", ";
	  cout << "GPS Speed=" << d.SS.speed << ", ";
	  cout << "OBD Speed=" << d.obdSpeed << ", ";
	  cout << "RPM=" << d.obdRPM << ", ";
	  cout << "Accel=" << accel << endl;
          cout << "Steer=" << steer << endl;

	  // Send command to steering actuator
	  if( -1.0 <= steer && steer <= 1.0 ) {
	    //cout << "Setting steering to " << steer << endl;
	    steer_heading(steer);
	  } else {
	    cout << "Bad Steering Command = " << steer << endl;
	  }

	  // Send commands to throttle/brake actuators
	  d.accel = accel;
	  if(-1.0 <= accel && accel <= 0.0) {
	    // cout << "Setting brake to " << accel << endl; 
	    set_accel_abs_pos(0.0);
	    //set_brake_abs_pos_pot( accel, brakeVelo, brakeVelo );

	  } else if(0.0 <= accel && accel <= 1.0) {
	    // cout << "Setting throttle to " << accel << endl;
	    //set_brake_abs_pos_pot( 0.0, brakeVelo, brakeVelo );
	    set_accel_abs_pos(0);
	    set_accel_abs_pos(accel);
	  } else {
	    cout << "Bad Accel Command = " << accel << endl;
	  }
	}

	// Log state data
	gettimeofday(&now, NULL);
	accelFile << timeval_subtract(now, d.TimeZero) << "\t"
		  << accel << "\t"
	          << steer << "\t"
		  << d.SS.speed << "\t"
		  << d.SS.vel_n << "\t"
		  << d.SS.vel_e << "\t"
		  << d.SS.northing << "\t"
		  << d.SS.easting << "\t"
		  << d.obdSpeed << "\t"
		  << d.obdRPM << "\t"
		  << endl;

      }
      // sleep for a bit and do it again

      //      cout << "--accel.cc: dont with one cycle. sleeping--" << endl;
      usleep_for(100000);      // This controls the rate at which steering/gas command is sent
    } else {      // state not valid
      cout << "--accel.cc: STATE not valid.--" << endl;
      usleep_for(100000); // wait to do anything until we have data
    }
  }  // while(active)

  // Main program done, set throttle to none
  set_accel_abs_pos(0);
}

