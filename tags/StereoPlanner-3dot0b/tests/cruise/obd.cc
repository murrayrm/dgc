// obd.cc

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>


#include "cruisemain.hh"

// RMM, 19 Dec 03: this is now defined in OBDII.hh
// const int OBDSerialPort = 1;


int InitOBD(DATUM &d) {
  int tmp;
  cout << "Initializing OBD-II" << endl;
  OBD_Init(OBDSerialPort);
 
  cout << "OBD-II Init Response" << endl;

  d.obdValid = FALSE;
  return TRUE; // success
}

void GrabOBDLoop(DATUM &d) {
  int i=0;
  float RPM, Speed;

  while(d.shutdown_flag == FALSE) {
    if( i == 0 ) {
      // grab Speed
      Speed = Get_Speed();
      if(Speed > 0.0) {
	boost::recursive_mutex::scoped_lock lock(d.obdLock);
	d.obdSpeed = Speed;
	d.obdValid = TRUE;
	d.obdNewData = TRUE;
      }
    } else if( i == 1) {
      // grab RPM
      RPM = Get_RPM();
      if(RPM > 0.0) {
	boost::recursive_mutex::scoped_lock lock(d.obdLock);
	d.obdRPM = RPM;
	d.obdValid = TRUE;
	d.obdNewData = TRUE;
      }
    }
    i = (i+1) % 2;
  }

}
