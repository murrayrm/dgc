//SUPERCON STRATEGY TITLE: estop pause NOT (caused by) superCon - BLOCKING-ANYTIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyEstopPausedNOTsuperCon.hh"
#include "StrategyHelpers.hh"

void CStrategyEstopPausedNOTsuperCon::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE
  currentStageName = estopPauseNotsuperCon_stage_names_asString( estopPauseNotsuperCon_stage_names(stgItr.nextStage()) );    

  switch( estopPauseNotsuperCon_stage_names(stgItr.nextStage()) ) {

  case s_estopPauseNotsuperCon::a_e_stop_waitstate: 
    //A NON-superCon (adrive/DARPA) estop pause has occurred - hence
    //WAIT in this strategy & stage until the pause is removed (->RUN)
    //when pause is removed, transition to NOMINAL strategy, the
    //exception to this is if a DARPA estop pause is detected
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = trans_GPSreAcq( (*m_pdiag), strprintf( "GPSreAcq: %s", currentStageName.c_str() ) );
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->pausedNOTsuperCon == true ) {
	//still paused --> don't transition to nominal strategy yet
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "WARN: EstopPausedNOTsuperCon - Still externally paused, will poll" );
      }

      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {
	
	transitionStrategy(StrategyNominal, "EstopPausedNOTsuperCon - Finished Stage 1 (no longer adrive/DARPA estop paused)");	
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "EstopPausedNOTsuperCon: stage - %s completed - NO longer adrive/DARPA estop paused --> Nominal", currentStageName.c_str() );
	//DO NOT update stgItr.nextStage() here - this strategy should continually loop
	//in this stage until the !superCon estop -> RUN
      }
    }
    break;
    
    /* END OF STRATEGY */

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: EstopPausedNOTsuperCon::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: EstopPausedNOTsuperCon::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: CStrategyEstopPausedNOTsuperCon - default stage reached!");
    }

  }
  
}


void CStrategyEstopPausedNOTsuperCon::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "EstopPausedNOTsuperCon - Exiting");
}

void CStrategyEstopPausedNOTsuperCon::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset();
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "EstopPausedNOTsuperCon - Entering");
}
