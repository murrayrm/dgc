#include"SuperConClient.hh"

#include "DGCutils"

#include<time.h>
#include<stdlib.h>

CSuperConClient::CSuperConClient(string module_name)
  : m_module_name(module_name)
{
  DGCcreateMutex(&m_sc_mutex);

  m_listen_socket = m_skynet.listen(SNsuperconResume, MODsupercon);
  m_send_socket = m_skynet.get_send_sock(SNsuperconMessage);
}

CSuperConClient::~CSuperConClient()
{
}

///Main throw function
void CSuperConClient::scDoTheMessage(int rev, const char *file, int line, sc_msg_type type, int msgID, supercon_msg_cmd &cmd)
{
  //Send message over skynet to SuperCon, first fill up the cmd struct
  strncpy(m_cmd.module_id, m_module_name.c_str(), 3);
#warning process_id hack in supercon
  m_cmd.process_id = clock();
  m_cmd.type = type;
  m_cmd.msg_id = msgID;
  m_cmd.revision = rev;
  strncpy(m_cmd.file, file, SUPERCON_PATH_MAX);
  m_cmd.line = line;
  if(m_cmd.user_bytes>SUPERCON_MAX_USER_BYTES)
    m_cmd.user_bytes = SUPERCON_MAX_USER_BYTES;

  //Only send the user bytes required to be sent
  m_skynet.send_msg(m_send_socket, &cmd, SUPERCON_MSG_CMD_CORE+cmd.user_bytes, 0);

  //Depending on type, wait for skynet etc...
  if(type==scMessage)  //Nothing more to do
    return;

  if(type==scWarning)  //Nothing more to do
    return;

  if(type==scError) {
    //Wait for a SuperCon reply signifying the problem has been fixed
    supercon_resume_cmd resume_cmd;

    int start_time = clock();
    bool found_msg = false;

#warning Timeout checking not correctly implemented, get_msg locks...

    while(clock()-start_time < SUPERCONCLIENT_TIMEOUT*CLOCKS_PER_SEC) {  //While correct message has not been received and no timeout has accured
      int nbytes = m_skynet.get_msg(m_listen_socket, &resume_cmd, sizeof(supercon_resume_cmd), 0);
      if(nbytes==sizeof(supercon_resume_cmd) && resume_cmd.process_id==m_cmd.process_id) {
	found_msg = true;
	break;
      }
    }

    if(found_msg && resume_cmd.resume=='y')   //A correct resume was received
      return;

    //No resume, or timeout.... assert!!!
  }

  //Assume assert.... either specified as assert, or incorrect parameter value
  exit(1);
}
