#include "RFCommunications.hh"
#include "rddf.hh"
#include "DGCutils"
#include "Road.hh"
#include "sparrowhawk.hh"
#include <iostream>

using namespace std;

//#define DEBUG_PRINT

RFCommunications::RFCommunications(int skynetkey, bool waitstate)
  : CSkynetContainer(SNroadfinding, skynetkey),
    CStateClient(waitstate), m_traj(3)
{
  m_running = true;

  m_road_speed = 20.0;
  m_extend_backward=10.0;
  m_road_width=-1;
  m_lat = 0;
  m_width_threshold = 1.0;         //Width of less then 1m => cut of road
  m_turnoff_angle = 90.0;          //RDDF turn angel at which to turn of road
  m_turnoff_angle_lookahead=10.0;  //How far to look ahead
  m_rddf_angle = 0.0;

  m_send_road = true;
  m_send_traj = true;
  m_send_every_x_road = 5;

  //Create sending sockets
  m_traj_socket = m_skynet.get_send_sock(SNroadtraj);
  m_road_socket = m_skynet.get_send_sock(SNroad2map);

  //Initialize ladar info
  m_ladar_info[0].numPoints = 0;
  m_use_ladar[0]=true;
  m_ladar_socket[0] = m_skynet.listen(SNladarmeas_bumper, MODladarfeeder_bumper);
  DGCcreateMutex(&m_ladar_mutex[0]);
  DGCstartMemberFunctionThreadWithArg(this, &RFCommunications::readLadarThread, (void*)0);

  m_ladar_info[1].numPoints = 0;
  m_use_ladar[1]=true;
  m_ladar_socket[1] = m_skynet.listen(SNladarmeas_roof, MODladarfeeder_roof);
  DGCcreateMutex(&m_ladar_mutex[1]);
  DGCstartMemberFunctionThreadWithArg(this, &RFCommunications::readLadarThread, (void*)1);


  //Set sparrow bindings
  SparrowHawk().rebind("RoadSpeed", &m_road_speed);
  SparrowHawk().rebind("ExtendBackwards", &m_extend_backward);
  SparrowHawk().rebind("RoadWidth", &m_road_width);
  SparrowHawk().rebind("WidthCutOff", &m_width_threshold);
  SparrowHawk().rebind("AngleCutOff", &m_turnoff_angle);
  SparrowHawk().rebind("AngleCutOffLookAhead", &m_turnoff_angle_lookahead);
  SparrowHawk().rebind("RDDFAngle", &m_rddf_angle);
  SparrowHawk().set_readonly("RDDFAngle");
  SparrowHawk().rebind("SendEveryXRoad", &m_send_every_x_road );
  SparrowHawk().rebind("SendRoad", &m_send_road);
  SparrowHawk().rebind("SendTraj", &m_send_traj);
  SparrowHawk().rebind("UseLadarBumper", (bool*)&m_use_ladar[Bumper]);
  SparrowHawk().rebind("UseLadarRoof", (bool*)&m_use_ladar[Roof]);

  SparrowHawk().rebind("LatDisp", &m_lat);

  SparrowHawk().rebind("TrajSendCnt", &m_traj_cnt.cnt);
  SparrowHawk().set_readonly("TrajSendCnt");
  SparrowHawk().rebind("TrajSendHz", &m_traj_cnt.freq);
  SparrowHawk().set_readonly("TrajSendHz");
  SparrowHawk().rebind("RoadSendCnt", &m_road_cnt.cnt);
  SparrowHawk().set_readonly("RoadSendCnt");
  SparrowHawk().rebind("RoadSendHz", &m_road_cnt.freq);
  SparrowHawk().set_readonly("RoadSendHz");

  SparrowHawk().rebind("LadarBumperCnt", &m_ladar_cnt[0].cnt);
  SparrowHawk().set_readonly("LadarBumperCnt");
  SparrowHawk().rebind("LadarBumperHz", &m_ladar_cnt[0].freq);
  SparrowHawk().set_readonly("LadarBumperHz");
  SparrowHawk().rebind("LadarRoofCnt", &m_ladar_cnt[1].cnt);
  SparrowHawk().set_readonly("LadarRoofCnt");
  SparrowHawk().rebind("LadarRoofHz", &m_ladar_cnt[1].freq);
  SparrowHawk().set_readonly("LadarRoofHz");
}

RFCommunications::~RFCommunications()
{
  m_running = false;

  DGCdeleteMutex(&m_ladar_mutex[0]);
  DGCdeleteMutex(&m_ladar_mutex[1]);
}


/*
**
** Both these functions eventually result in roads and trajs getting sent
**
*/

void RFCommunications::RFSendTraj(double dir, double lat)
{
  //Convert to point set and use SendRoad
  double x[100];
  double y[100];
  double w[100];

  double c = cos(dir);
  double s = sin(dir);
  double d = 0.0;
  
  //Generate trajectory points
  for(int i=0; i<(int)sizeof(x); ++i, d+=.25) {
    x[i] = c*d;
    y[i] = lat + s*d;
    w[i] = 5;
  }
  
  RFSendRoad(x, y, w, sizeof(x));
  
  return;
}


//Send roads and trajectories to fusion mapper and traj follower
//  x: meters in front
//  y: meters to the right
void RFCommunications::RFSendRoad(double *x, double *y, double *width, int size)
{
  if(size<2)    //Have to have atleast 2 points
    return;

  static int cnt = 0;

  ++cnt;

  if(cnt< m_send_every_x_road)
    return;

  cnt=0;

  bool is_road=false;

  m_lat=y[0];

  //Place holders for road and trajectory
  Road road;
  road.length=0;
  m_traj.setNumPoints(0);

  //Transform and add points to trajectory
  UpdateState();

  //Coordinat transformation, so far dont consider pitch and roll
  frames f;
  f.updateState( NEDcoord(m_state.Northing, m_state.Easting, 0), 0,0, m_state.Yaw);

  m_traj.startDataInput();

  //Add backward extension
  XYZcoord pt(x[0],y[0],0);
  NEDcoord pt2;
  pt.X -= m_extend_backward;
  pt2 = f.transformB2N(pt);
  m_traj.inputNoDiffs(pt2.N, pt2.E);
  road[road.length].N = pt2.N;
  road[road.length].E = pt2.E;
  road[road.length].speed = m_road_speed;
  road[road.length].width = width[0];
  ++road.length;

  //Copy main part of the road to trajectory and road structure
  if(size>ROAD_MAX_LENGTH-1)
    size = ROAD_MAX_LENGTH-1;
  for(int i=0; i<size; ++i) {
    if(width[i]<m_width_threshold)
      break;
    if(width[i]>0)
      is_road=true;
    pt.X = x[i];
    pt.Y = y[i];
    pt2 = f.transformB2N(pt);
    m_traj.inputNoDiffs(pt2.N, pt2.E);
    road[road.length].N = pt2.N;
    road[road.length].E = pt2.E;
    road[road.length].speed = m_road_speed;
    road[road.length].width = width[i];
    ++road.length;
  }

  if(m_road_width!=-1) {
    for(int i=0; i<road.length; ++i) {
      road[i].width = m_road_width;
    }
  }
  /*
  //Print road
  for(int i=0; i<road.length; ++i) {
    SparrowHawk().log("Road part %i: %lf %lf, %lf", i, road[i].N, road[i].E, road[i].width);
  }

  for(int i=0; i<size; ++i) {
    SparrowHawk().log("Road part %i: %lf %lf, %lf", i, x[i], y[i], width[i]);
  }
  */

  //Check road curvature
  m_rddf.updateState(m_state);

  int i = m_rddf.getCurrentPoint();
  if(i>=0 && i<m_rddf.getNumTargetPoints()) 
  {
    double dist = 0.0;
    double angle = 0.0;
   
    int j=0;
    for(; j<100 && i+j<m_rddf.getNumTargetPoints() && 
                    dist<m_turnoff_angle_lookahead; ++j) {
      dist += m_rddf.getTrackLineDist(i+j);
    }
    if(j!=0)
      --j;   //Step one back so we get last item before distance was to great

    angle = (m_rddf.getTrackLineYaw(i+j) - m_rddf.getTrackLineYaw(i)) * 180/3.14159265;

    if(fabs(angle) > m_turnoff_angle)
      is_road = false;

    m_rddf_angle = angle;
  } 
  else
  {
    m_rddf_angle = -1000.0;
  }

  //Set trajectory speed
  for(i=0; i<m_traj.getNumPoints()-1; ++i) {
    double vn = m_traj.getNorthing(i+1) - m_traj.getNorthing(i);
    double ve = m_traj.getEasting(i+1) - m_traj.getEasting(i);

    double scale = m_road_speed / sqrt(vn*vn + ve*ve);
    (m_traj.getNdiffarray(1))[i] = vn*scale;
    (m_traj.getEdiffarray(1))[i] = ve*scale;
    (m_traj.getNdiffarray(2))[i] = 0;
    (m_traj.getEdiffarray(2))[i] = 0;
  }
  (m_traj.getNdiffarray(1))[i] = 0;
  (m_traj.getEdiffarray(1))[i] = 0;
  (m_traj.getNdiffarray(2))[i] = 0;
  (m_traj.getEdiffarray(2))[i] = 0;


  //Send the road
  if(m_road_socket && m_send_road) {
    if(!is_road)
      road.length=0;
    m_skynet.send_msg(m_road_socket, (void*)&road, road.data_size());
    ++m_road_cnt;
  }

  //Send the trajectory
  if(m_traj_socket && m_send_traj) {
    SendTraj(m_traj_socket, &m_traj);
    ++m_traj_cnt;
  }
}

/*
**
** Return ladar scans and maximal number of points in ladar scan,
** angle is in radians with 0 to the right
**
*/
int RFCommunications::RFGetMaxScanPoints()
{
  return MAX_SCAN_POINTS;
}

int RFCommunications::RFGetLadarScan(Ladar ladar, double *angle, double *range, unsigned long long &tStamp)
{
  if(ladar!=Bumper && ladar!=Roof)
    return 0;

  DGClockMutex(&m_ladar_mutex[ladar]);

  //Transfer the scan data
  for(int i = 0; i < m_ladar_info[ladar].numPoints; i++) {
    range[i] = m_ladar_info[ladar].data[i].ranges;
    angle[i] = m_ladar_info[ladar].data[i].angles;
  }
  tStamp = m_ladar_info[ladar].timeStamp;
  int nPoints = m_ladar_info[ladar].numPoints;

  DGCunlockMutex(&m_ladar_mutex[ladar]);

  return nPoints;
}

/*
**
** Force a state update and read of current position
**
*/
void RFCommunications::RFUpdateState()
{
  UpdateState();
}

double RFCommunications::RFNorthing()
{
  return m_state.Northing;
}

double RFCommunications::RFEasting()
{
  return m_state.Easting;
}

double RFCommunications::RFYaw()
{
  return m_state.Yaw;
}

double RFCommunications::RFPitch()
{
  return m_state.Pitch;
}

/*
**
** Ladar reader thread
**
*/
void RFCommunications::readLadarThread(void *ladar_nr)
{
  int ladar = (int)ladar_nr;
  ladarMeasurementStruct ladarInfo;    //One for each ladar
  
  while(m_running) {
    m_skynet.get_msg(m_ladar_socket[ladar], &ladarInfo, sizeof(ladarMeasurementStruct), 0);
    
    if(!m_use_ladar[ladar])
      continue;
    
    ++m_ladar_cnt[ladar];
    
    //lock mutex
    DGClockMutex(&m_ladar_mutex[ladar]);
    
    if(ladarInfo.numPoints > MAX_SCAN_POINTS) {
      //To many points, so dont report anything at all
      m_ladar_info[ladar].numPoints = 0;
      m_ladar_info[ladar].timeStamp = ladarInfo.timeStamp;
    } else {
      //Copy the ladar data to the correct container
      m_ladar_info[ladar].numPoints = ladarInfo.numPoints;
      m_ladar_info[ladar].timeStamp = ladarInfo.timeStamp;
      for(int i=0; i<ladarInfo.numPoints; ++i) {
	m_ladar_info[ladar].data[i].ranges = ladarInfo.data[i].ranges;
	m_ladar_info[ladar].data[i].angles = ladarInfo.data[i].angles;
      }
    }
    DGCunlockMutex(&m_ladar_mutex[ladar]);
  }
}


