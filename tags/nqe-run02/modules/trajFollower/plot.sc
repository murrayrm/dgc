#!/bin/sh -x

cd logs

rm -f state path path2 yerror vref vact verror

#state is the places we've been, path is the desired trajectory, path2 is the
# part of the trajectory that sn_TrajFollower has come closest to

paste state.dat | awk '{print $2, $3}' > state
#paste state2.dat| awk '{if($4 != 0)print $4, $5}' > path2
#paste ../rddf.dat | awk '{print $1, $4}' > path
paste ../../util/RDDF/bob.dat | awk '{print $3,$2}' > path
paste steps.dat | awk '{print NR, $1}' > FF
paste steps.dat | awk '{print NR, $2}' > FB

paste pid.dat | awk '{if(($3 !=0)&&($2<100)&&($2>-100)) print NR,$1}' > p
paste pid.dat | awk '{if(($3 !=0)&&($2<100)&&($2>-100)) print NR,$2}' > i
paste pid.dat | awk '{if(($3 !=0)&&($2<100)&&($2>-100)) print NR,$3}' > d


echo "plot 'path', 'state' with dots" | gnuplot -persist
echo "plot 'p', 'i', 'd' with dots" | gnuplot -persist

# plotting the perpendicular y-error as a function of some arbitrary time
# variable...

paste error.dat | awk '{print NR,$1}' > yerrF
paste error.dat | awk '{print NR,$3}' > yerrR
echo "plot 'yerrF' with dots,'yerrR' with dots, 'FF' with dots, 'FB' with dots" | gnuplot -persist

#plotting the desired vs actual velocities
paste velocity.dat | awk '{print NR, $1}' > vref
paste velocity.dat | awk '{print NR, $2}' > vact
paste velocity.dat | awk '{print NR, $3}' > verr

echo "plot 'vref' with l, 'vact' with l, 'verr' with l" | gnuplot -persist

#plotting the velocity error (as an arbitrary function of time...)

#paste velocity.dat | awk '{print NR, $3}' > verror
#echo "plot 'verror' " | gnuplot -persist
