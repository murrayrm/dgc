#include "ggis.h"
#include "contest-info.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Convert a UTM coordinate to a LatLon, using the routines in ggis.c */

int main (int argc, char **argv)
{
  GisCoordUTM utm;
  GisCoordLatLon latlon;
  char *end;
  if (argc != 5)
    {
      fprintf (stderr, "usage: %s ZONE-NUMBER ZONE-LETTER EASTING NORTHING\n"
	       "\n"
	       "Take a coordinate in UTM and convert it to latitude/longitude.\n"
	       "\n"
	       "In UTM, the world is carved into 60 vertical strips, each 6 degrees,\n"
	       "which are assigned a number from 1 to 60.\n"
	       "It is also carved into 20 lettered zones,\n"
	       "From 84N to 80S are the zones:  CDEFGHJKLMNPQRSTUVWX.\n"
	       , argv[0]);
      return 1;
    }

  utm.zone = strtoul (argv[1], NULL, 10);
  if (utm.zone < 1 || utm.zone > 60)
    {
      fprintf (stderr, "Invalid zone number: must be 1..60.\n");
      exit (1);
    }
  utm.letter = argv[2][0];

  /* Note: the following check also permits utm.letter==0
     (which can be passed in as '')
     That seems to be allowed. */
  if (strchr ("CDEFGHJKLMNPQRSTUVWX", utm.letter) == NULL)
    {
      fprintf (stderr, "Invalid zone number: must be one of CDEFGHJKLMNPQRSTUVWX.\n");
      exit (1);
    }
  utm.e = strtod (argv[3], &end);
  if (end == argv[3])
    {
      fprintf (stderr, "Error parsing easting.\n");
      exit (1);
    }
  utm.n = strtod (argv[4], &end);
  if (end == argv[4])
    {
      fprintf (stderr, "Error parsing northing.\n");
      exit (1);
    }

  if (!gis_coord_utm_to_latlon (&utm, &latlon, GEODETIC_MODEL))
    {
      fprintf (stderr, "Error converting coordinate to latlon.\n");
      exit (1);
    }

  printf ("Easting (meters) = %.3f\nNorthing (meters) = %.3f\n", utm.e, utm.n);
  printf ("UTM Zone = %d\nUTM Letter = %c\n", utm.zone, utm.letter );
  printf ("Latitude (deg) = %.13f\n", latlon.latitude );
  printf ("Longitude (deg) = %.13f\n", latlon.longitude );

  return 0;
}
