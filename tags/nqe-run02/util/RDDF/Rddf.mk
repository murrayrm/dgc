RDDF_PATH = $(DGC)/util/RDDF
RDDF_NAME = rddf.dat

RDDF_DEPEND = \
	$(CONSTANTSLIB) \
	$(RDDF_PATH)/rddf.hh \
	$(RDDF_PATH)/rddf.cc \
	$(RDDF_PATH)/herman.hh \
	$(RDDF_PATH)/herman.cc \
	$(RDDF_PATH)/raid.hh \
	$(RDDF_PATH)/raid.cc \
	$(RDDF_PATH)/Makefile
