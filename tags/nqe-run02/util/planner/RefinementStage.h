#ifndef _REFINEMENTSTAGE_H_
#define _REFINEMENTSTAGE_H_


#include "VehicleState.hh"
#include "rddf.hh"
#include "CMap.hh"
#include "traj.h"
#include "defs.h"
#include "AliceConstants.h"

extern "C" void dgemv_(char*, int*, int*, double*, double*, int*, double*, int*, double*, double*, int*);
extern "C" void dgemm_(char*, char*, int*, int*, int*, double*, double*, int*, double*, int*, double*, double*, int*);
extern "C" void dgesv_(int*, int* , double *, int* , int *, double *, int*, int*);
extern "C" double ddot_(int*, double*, int*, double*, int*);
extern "C" void dgesvd_(char*, char*, int*, int*, double*, int*, double*, double*, int*, double*, int*, double*, int*, int* );
extern "C" double dscal_(int*, double*, double*, int*);

#define DO_BENCHMARK

class CRefinementStage
{
	RDDF*			m_pRDDF;
	CTraj*		m_pTraj;
	CMap*			m_pMap;
	int				m_mapLayer;

	// The order of the bounds is non-linear {init, traj, final}, linear {init,
	// traj, final}
	double*		m_solverLowerBounds;
	double*		m_solverUpperBounds;
	double*		m_PlannerLowerBounds;
	double*		m_PlannerUpperBounds;

	double *m_pSolverState;
	int inform;

	// Internal SNOPT crap
	char start[8];
	int SNOPTm, SNOPTn;
	int ne;
	int nName;
	int nnobj, nncon, mincw, miniw, minrw;
	int nout, nnjac;
	int iobj, ninf;
	double objadd;
	char prob[8*5];
	double *m_SNOPTmatrix;
	int *ha, *ka;
	double *rc;
	double *pi;
	int *hs, ns;
	double obj, sinf;
	char *cw;
	int *iw;
	double *rw;
	int lencw, leniw, lenrw;
	int PRINT_LEVEL;

	// Everything in the computation is shifted by this many units. Used to move
	// the absolute values of the computation to reasonable values
	double		m_initN, m_initE;
	double		m_initTheta;
	double    m_initYawRate;
	double		m_initSpeed;
	double    m_initAcc;
	double    m_nodataUnpassableCutoffDist;
	int       m_pointsToNodataUnpassableCutoff;

	double		m_targetN, m_targetE;
	double		m_targetTheta;

	double*		m_pColl_theta;
	double*		m_pColl_dtheta;
	double*		m_pColl_ddtheta;
	double*   m_pColl_speed;
	double*   m_pColl_dspeed;
	double*   m_pCollN;
	double*   m_pCollE;
	double*		m_pCollGradN;
	double*		m_pCollGradE;
	double*   m_pCollVlimit;
	double*   m_pCollDVlimitDN;
	double*   m_pCollDVlimitDE;
	double*   m_pCollDVlimitDTheta;

	double    m_collSF;

	double**	m_grad_solver;

	double*		m_valcoeffs, *m_d1coeffs, *m_d2coeffs;
	double*		m_valcoeffsfine, *m_d1coeffsfine, *m_d2coeffsfine;
	double*		m_valcoeffs_speed, *m_d1coeffs_speed;
	double*		m_valcoeffsfine_speed, *m_d1coeffsfine_speed;

	double*		m_dercoeffsmatrix;
	double*   m_stateestimatematrix;
	double*   m_speedestimatematrixfine;
	double*   m_speedestimatematrix;

	double*		m_splinecoeffsTheta;

	double*   m_pOutputTheta;
	double*   m_pOutputDTheta;
	double*   m_pOutputSpeed;
	double*   m_pOutputDSpeed;

	// these are used to store the theta and speed vectors for seeding. since
	// these two are seeded separately, we can share the memory space
	union
	{
		double*   m_pThetaVector;
		double*   m_pSpeedVector;
	};

	CTraj *m_pSeedTraj;

	// the nominal planning distance
	double PLANNING_TARGET_DIST;

	// the length of the SNOPT state vector
	int NPnumVariables;

	// numbers of constraints
	int	NUM_LIN_INIT_CONSTR, NUM_LIN_TRAJ_CONSTR, NUM_LIN_FINL_CONSTR,
		NUM_NLIN_INIT_CONSTR, NUM_NLIN_TRAJ_CONSTR, NUM_NLIN_FINL_CONSTR;

	string m_specsName;
	bool m_USE_MAXSPEED;

	// whether we want to actuallty run the solver or to just take the seed
	bool m_bOnlySeed;

	int  m_kernelWidth;
	int  m_failureCount;
	bool m_bDropKernel;

	void outputTraj(CTraj* pTraj);
	void makeSolverBoundsArrays(void);
	bool SetUpPlannerBoundsAndSeedCoefficients(CTraj* pSeedTraj);

	void getSplineCoeffsTheta(double* pSolverState);
	void integrateNE(int collIndex);
	void integrateNEfine(int collIndex, double& outN, double& outE);
	void initialize(char* pDercoeffsFile);
	void SetCollocVars(void);
	void SetUpLinearConstr(void);
	void SetUpPlannerBounds(void);
	void CallSolver(void);

	bool SeedFromTraj(CTraj* pTraj, double dist = 0.0);
	void setTargetToDistAlongSpline(double distRatio);

	void NonLinearInitConstrFunc(void);
	void NonLinearTrajConstrFunc(int constr_index_snopt, int collIndex);
	void NonLinearFinlConstrFunc(void);


public:
	bool			m_bGetVProfile;

	int run(VehicleState* pVehState, CTraj* pSeedTraj = NULL, bool bIsStateFromAstate = true);
	void checkDerivatives();

	CTraj* getTraj(void)
	{
		return m_pTraj;
	}
	double getObjective(void)
	{
		return m_obj;
	}

	void funcConstr(double* pSolverState);
	void funcCost  (double* pSolverState);
	double *m_pPrevSolverState;
	double*   m_pConstrData;
	double*   m_pConstrGradData;
	double    m_obj;
	double*   m_pCostGradData;

	// this function takes in a traj and performs a least-squares fit to make it C^2
	void MakeTrajC2(CTraj* pTraj);

	// this function evaluates the spatial part of the current solver state
	// through the map to seed the temporal part with a least squares fit of the
	// map-based speeds. This function is similar to CPlannerStage::SeedFromTraj
	void SeedSpeedFromState(void);

	CRefinementStage(CMap *pMap = NULL, int mapLayerID = 0, RDDF* pRDDF = NULL, bool USE_MAXSPEED = false, bool bGetVProfile = false);
	~CRefinementStage();

	double getLength(void)
	{
		return m_pSolverState[NPnumVariables-1];
	}

	CTraj* getSeedTraj(void);

	void makeCollocationData(double* pSolverState);
};

#endif // _REFINEMENTSTAGE_H_
