#include <iostream>
#include <fstream>
using namespace std;

#include <stdlib.h>

#include "sn_msg.hh"
#include "DGCutils"

#define MAX_BUFFER_SIZE 1000000

struct SContext
{
	skynet*          pSkynet;
	sn_msg           msgtype;
	ostream*         pStream;
	pthread_mutex_t* pMutex;
};

void* listenToType(void *pArg)
{
	SContext *pContext = (SContext*)pArg;
  int sock = pContext->pSkynet->listen(pContext->msgtype, ALLMODULES);

	char* pBuffer = new char[MAX_BUFFER_SIZE];

	while(true)
	{
		int msgsize = pContext->pSkynet->get_msg (sock, pBuffer, MAX_BUFFER_SIZE, 0);
		unsigned long long stamp;
		DGCgettime(stamp);
		
		DGClockMutex(pContext->pMutex);
		pContext->pStream->write((char*)&stamp, sizeof(stamp));
		pContext->pStream->write((char*)&pContext->msgtype, sizeof(pContext->msgtype));
		pContext->pStream->write((char*)&msgsize, sizeof(msgsize));
		pContext->pStream->write((char*)pBuffer, msgsize);
		DGCunlockMutex(pContext->pMutex);
	}
	delete pBuffer;
}


int main(int argc, char *argv[])
{
	int i;

	ofstream logfile;
	ostream* pLogstream = &logfile;
	if(argc == 1)
	{
		logfile.open("/tmp/snlog");
	}
	else
	{
		if(strcmp(argv[1], "-") == 0)
			pLogstream = &cout;
		else
			logfile.open(argv[1]);
	}

	int sn_key;
	cerr << "Searching for skynet KEY " << endl;
	char* pSkynetkey = getenv("SKYNET_KEY");
	if( pSkynetkey == NULL )
	{
		cerr << "SKYNET_KEY environment variable isn't set" << endl;
		return -1;
	}
	else {
		sn_key = atoi(pSkynetkey);
		cout << "Construting skynet with key " << sn_key << endl;
	}
	skynet skynetobject(SNguilogwriter, sn_key);

	pthread_mutex_t filemutex;
	DGCcreateMutex(&filemutex);

	SContext  contexts[last_type];
	pthread_t thread_id[last_type];

	for(i=0; i<last_type; i++)
	{
		contexts[i].pSkynet = &skynetobject;
		contexts[i].msgtype = (sn_msg)i;
		contexts[i].pStream = pLogstream;
		contexts[i].pMutex  = &filemutex;
		
		pthread_create(&thread_id[i], NULL, &listenToType, (void*)&contexts[i]);
	}
	pthread_join(thread_id[0], NULL);

	DGCdeleteMutex(&filemutex);
}
