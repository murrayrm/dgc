#ifndef STRATEGY_UNSEENOBSTACLE_HH
#define STRATEGY_UNSEENOBSTACLE_HH

//SUPERCON STRATEGY TITLE: Unseen-Obstacle - ANYTIME STRATEGY
//HEADER FILE (.hh)
//DESCRIPTION: This strategy updates the superCon layer in the map to reflect
//obstacles whose existence has been confirmed through investigation, i.e.
//Alice tried to drive forwards through an area, and her progress was
//impeded by an obstacle.  Note that this strategy is NOT responsible for
//identifying that there is an intraversible obstacle in front of Alice - 
//which is handled in the strategy-transition conditions (evaluated prior
//to executing any stepForward(...) method)

#include "Strategy.hh"
#include "sc_specs.h"

namespace s_unseenobstacle {

#define UNSEENOBSTACLE_STAGE_LIST(_) \
  _(estop_pause, = 1) /*first entry must = 1 (as this is referenced against NEXTstage */ \
  _(add_map_obst, ) \
  _(obst_wait, )
DEFINE_ENUM(unseenobstacle_stage_names, UNSEENOBSTACLE_STAGE_LIST)

}

using namespace std;
using namespace s_unseenobstacle;

class CStrategyUnseenObstacle : public CStrategy
{

/*** METHODS ***/
public:
  
  /*** METHODS ***/

  /** CONSTRUCTORS **/
  CStrategyUnseenObstacle() : CStrategy() {}  

  /** DESTRUCTOR **/
  ~CStrategyUnseenObstacle() {}

  /** MUTATORS **/
  void stepForward(const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface);
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

private:
  //conditional tests for leaving unseen-obstacle
  bool leave_unseenObstacle_conditions( bool checkBool, CStrategyInterface *pStrategyInterface, const SCdiagnostic *pdiag, const char* StrategySpecificStr );

};

#endif
