#ifndef SUPERCONHELPERS_HH
#define SUPERCONHELPERS_HH

#include"sparrowhawk.hh"
#include<iostream>
#include<iomanip>
using namespace std;

//Define helper functions for supercon

//Defines used to create state and diagnosis structs
//These are used to ensure initialization is done correctly
//and so that print functions and gui associations are done
//automaticaly


/*
**
** State defines
**
*/

#define STATE_ITEM(typet, item, default, desc)	\
  typet item;

#define STATE_LOCKITEM(typet, item, default, desc)	\
  bool lock_##item;

#define STATE_RESET(typet, item, default, desc)	\
  item = default;				\
  lock_##item = false;

#define STATE_COUNT(typet, item, default, desc)	+1

#define STATE_SPARROW(typet, item, default, desc)			\
  SparrowHawk().make_label(de++, row, cols[0], "| " #item ":");		\
  SparrowHawk().make_edit(de, row, cols[1], &item);			\
  SparrowHawk().set_notify(de++, this, &SCstate::set_lock, &lock_##item); \
  SparrowHawk().make_edit(de++, row, cols[2], &lock_##item);		\
  SparrowHawk().make_label(de++, row, 80, "|");				\
  ++row;

#define STATE_LOG_HEADER(typet, item, default, desc)	\
  os<<" " #item;

#define STATE_LOG_WRITE(typet, item, default, desc)	\
  os<<setprecision(20)<<item<<" ";

#define STATE_LOG_READ(typet, item, default, desc)	\
  is>>item;

#define STATE_ASSIGMENT(typet, item, default, desc)	\
  if(!lock_##item)					\
    item = state.item;

//Use C++ cast to reset scThrows
#define STATE_RESET_SCTHROWS(typet, item, default, desc)	\
  item = typet(item);

#define BEGIN_DEFINE_STATES(classname, list)		\
  struct classname {					\
    classname() { reset(); }				\
							\
    classname &operator=(const classname &state) {	\
      list(STATE_ASSIGMENT);				\
      return *this;					\
    }							\
    							\
    list(STATE_ITEM);					\
    							\
    void reset() {					\
      list(STATE_RESET);				\
    }							\
							\
    void log_format(ostream &os) {			\
      list(STATE_LOG_HEADER);				\
    }							\
    void log_write(ostream &os)	{			\
      list(STATE_LOG_WRITE);				\
    }							\
    void log_read(istream &is)	{			\
      list(STATE_LOG_READ);				\
    }							\
    							\
    void set_lock(bool *p) {				\
      *p=true;						\
    }							\
							\
    int sparrow_items() const {						\
      int cnt = (0 + list(STATE_COUNT));				\
      return 4*cnt + 2;							\
    }									\
    void set_sparrow(display_entry *de, int row) {			\
      int cols[3] = {1, 50, 72};					\
									\
      SparrowHawk().make_label(de++, row, 1, "+------------------------------------------------------------------------------+"); \
      ++row;								\
									\
      list(STATE_SPARROW);						\
									\
      SparrowHawk().make_label(de++, row, 1, "+------------------------------------------------------------------------------+"); \
      ++row;								\
    }									\
  private:								\
    list(STATE_LOCKITEM);

#define END_DEFINE_STATES(classname, list)			\
  public:							\
  void reset_scThrows() {					\
    list(STATE_RESET_SCTHROWS);					\
  }								\
  };


/*
**
** Diagnostic defines
**
*/

#define DIAGNOSTIC_ITEM(item, desc)		\
  bool_counter item;


#define DIAGNOSTIC_COUNT(item, desc)	+1

#define DIAGNOSTIC_SPARROW(item, desc)					\
  SparrowHawk().make_label(de++, row, cols[0], "| " #item ":");		\
  SparrowHawk().make_edit(de, row, cols[1], &dummy_b);			\
  SparrowHawk().set_setter(de, &item, &bool_counter::setter);		\
  SparrowHawk().set_getter(de++, &item, &bool_counter::getter);		\
  SparrowHawk().make_edit(de, row, cols[2], &dummy_i, "(%i)");		\
  SparrowHawk().set_setter(de, &item, &bool_counter::set_count);	\
  SparrowHawk().set_getter(de++, &item, &bool_counter::get_count);	\
									\
  SparrowHawk().make_label(de++, row, 80, "|");				\
  ++row;

#define DEFINE_DIAGNOSTICS(classname, list)		\
  struct classname {					\
							\
    list(DIAGNOSTIC_ITEM);				\
    							\
    int sparrow_items() const {						\
      int cnt = (0 + list(DIAGNOSTIC_COUNT));				\
      return 4*cnt + 2;							\
    }									\
    void set_sparrow(display_entry *de, int row) {			\
      int cols[3] = {1, 50, 56};					\
      static bool dummy_b;						\
      static int dummy_i;						\
									\
      SparrowHawk().make_label(de++, row, 1, "+------------------------------------------------------------------------------+"); \
      ++row;								\
									\
      list(DIAGNOSTIC_SPARROW);						\
									\
      SparrowHawk().make_label(de++, row, 1, "+------------------------------------------------------------------------------+"); \
      ++row;								\
    }									\
  };


#endif
