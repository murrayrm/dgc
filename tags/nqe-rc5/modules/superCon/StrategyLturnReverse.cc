//SUPERCON STRATEGY TITLE: L-turn Reverse - SOMETIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyLturnReverse.hh"
#include "StrategyHelpers.hh"

void CStrategyLturnReverse::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE
  currentStageName = lturnreverse_stage_names_asString( lturnreverse_stage_names(stgItr.nextStage()) );
  
  switch( stgItr.nextStage() ) {

  case s_lturnreverse::estop_pause: 
    //superCon e-stop pause Alice prior to shifting into reverse
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "LturnReverse: %s", currentStageName.c_str() ) );
      skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->validAliceStop == false ) {
	SuperConLog().log( STR, WARNING_MSG, "WARNING: LturnReverse called when not in a valid stop" );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LturnReverse - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_lturnreverse::gear_reverse: //send-gear change command to adrive ?-->reverse
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "LturnReverse: %s", currentStageName.c_str() ) );
      skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->superConPaused == false ) {
	//NOT superCon paused - need to be to change gear, don't send gear change
	//command until Alice is paused
	SuperConLog().log( STR, WARNING_MSG, "LturnReverse: superCon e-stop pause not yet in place -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );	
      }
      
      if( m_pdiag->safeGearChangeConds == false ) {
	//conditions NOT yet safe for Alice to change gear -> don't send gear change command yet

	skipStageOps = true;	
	SuperConLog().log( STR, WARNING_MSG, "LturnReverse: alice not yet stationary (safeGearChangeConds == false), can't change gear -> will poll");
	checkForPersistLoop( this, persist_loop_waitstationary, stgItr.currentStageCount() );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	m_pStrategyInterface->stage_changeGear(sc_interface::reverse_gear);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LturnReverse - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_lturnreverse::trajF_reverse: //send change-mode command to TrajFollower ?-->reverse-MODE
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "LturnReverse: %s", currentStageName.c_str() ) );
      skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->adriveGearReverse == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "LturnReverse - Alice not yet in reverse gear - will poll" );
	checkForPersistLoop( this, persist_loop_gearChange, stgItr.currentStageCount() );	
      }

      if( m_pdiag->superConPaused == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, ERROR_MSG, "ERROR: LturnReverse - not yet paused (will NOT change TFmode yet) - will poll" );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_changeTFmode( tf_reverse, LTURN_REVERSE_DISTANCE_METERS );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LturnReverse - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;
    
  case s_lturnreverse::estop_run: //superCon e-stop run Alice now that she is ready to REVERSE
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "LturnReverse: %s", currentStageName.c_str() ) );
      skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->trajFReverse == false ) {
	SuperConLog().log( STR, WARNING_MSG, "LturnReverse: TrajFollower not yet in reverse mode" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_trajfModeChange, stgItr.currentStageCount() );
      }
      
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_run);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LturnReverse - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_lturnreverse::reversing_wait: //wait until Alice has reversed through LTURN_REVERSE_DISTANCE_METERS (assuming PLN !-> FP)
          //and then transition to LoneRanger IFF PLN -> NFP through TERRAIN obstacles, IFF PLN->NFP
          //through SUPERCON obstacles, then add an obstacle into the map in front of Alice &&
          //transition to the NOMINAL strategy
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "LturnReverse: %s", currentStageName.c_str() ) );
      skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      if( m_pdiag->tfFinishedReversing == false ) {
	//not finished reversing
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_trajfReverseFinished, stgItr.currentStageCount() );
      }

      if( m_pdiag->tfFinishedReversing == true && m_pdiag->transLturnReverse == true ) {
	//Add a superCon (intraversible) obstacle in front of Alice's current position
	//PRIOR to reversing again to block-off the area in front of Alice, as it has
	//all been 'checked' using the LoneRanger, and no way through has been found
	//hence this area should be marked as BAD so that we can reverse for a distance
	// > the planning distance, by blocking off at least part of the route
	//along which we came into the area.
	m_pStrategyInterface->stage_addForwardUnseenObstacle();
	transitionStrategy( StrategyNominal, "LturnReverse reversed & PLN still ->NFP through SuperCon obstacle transition to NOMINAL to invoke another reversing action" );
	SuperConLog().log( STR, WARNING_MSG, "WARNING: LturnReverse reversed & PLN still ->NFP through SuperCon obstacle transition to NOMINAL to invoke another reversing action" );
	skipStageOps = true;
      }
      
      if( m_pdiag->tfFinishedReversing == false || m_pdiag->plnNFP_Terrain == false || m_pdiag->plnNFP_SuperC == true ) {
	//This is a check to ensure that we only transition to the lone ranger when
	//we have finished reversing && the planner is NFP through a TEERAIN obstacle ONLY
	skipStageOps = true;
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	transitionStrategy( StrategyLoneRanger, "reversing complete and PLN->NFP through TERRAIN obstacles ONLY -> LoneRanger" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "reversing complete and PLN->NFP through TERRAIN obstacles ONLY" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LturnReverse - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }      
    }
    break;

    /** END OF STRATEGY **/
    
  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyLturnReverse::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyLturnReverse::stepForward - default case in nextStep switch() reached" );
      transitionStrategy(StrategyNominal, "ERROR: LturnReverse - default stage reached!" );
    }
  }
}


void CStrategyLturnReverse::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "LturnReverse - Exiting" );
}

void CStrategyLturnReverse::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset();
  skipStageOps.reset(); //resets to FALSE 
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "LturnReverse - Entering" );
}


