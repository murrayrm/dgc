//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_VanishingPoint.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for VanishingPoint base class.  takes dominant orientations object
/// to parametrize image size, number of orientations, etc.  sets up voter region
/// and candidate region boundaries and allocates memory for them.  lateral_expansion
/// and top_expansion are fractions of the image width and height, respectively,
/// with which to expand the candidate region on both sides and on the top side,
/// respectively

UD_VanishingPoint::UD_VanishingPoint(UD_DominantOrientations *domorient, float lateral_expansion, float top_expansion)
{
  int i, vx, vy;
  float orientation;

  // local names for window dimensions
  
  dom = domorient;
  w = dom->w;
  h = dom->h;
  candidate_lateral_margin = (int) (-lateral_expansion * (float) w);
  candidate_top_margin = (int) (-top_expansion * (float) h);

  // default values

  first_compute_call                    = TRUE;
  show                                  = FALSE; 
  compute_max_candidate                 = FALSE;

  voter_sky_fraction                    = 0.0; 
  candidate_scale                       = 1.0; 

  // candidates

  candidate_bottom_margin = 0;

  inv_candidate_scale = 1 / candidate_scale;

  cw = (int) candidate_scale * (w - 2 * candidate_lateral_margin);
  ch = (int) candidate_scale * (h - candidate_top_margin - candidate_bottom_margin);

  candidate_image = cvCreateImage(cvSize(cw, ch), IPL_DEPTH_32F, 1);

  // voters

  voter_top_margin = (int) ceil(MAX2(voter_sky_fraction * (float) h, dom->gabor_size / 2 + 1));
  voter_bottom_margin = dom->gabor_size / 2 + 1;
  voter_lateral_margin = dom->gabor_size / 2 + 1;

  // endpoints of voter ray lines

  voter_ray_x = cvCreateImage(cvSize(w, h), IPL_DEPTH_32F, 1);
  voter_ray_y = cvCreateImage(cvSize(w, h), IPL_DEPTH_32F, 1);

  // fixed endpoint (in candidate coordinates)

  for (vy = 0; vy < h; vy++)
    for (vx = 0; vx < w; vx++) {
      FLOAT_IMXY(voter_ray_x, vx, vy) = V2C_X(vx);
      FLOAT_IMXY(voter_ray_y, vx, vy) = V2C_Y(vy);
    }

  // each line must be sufficiently long that it will reach across the candidate region
  // no matter where in the voter region it starts or what its orientation.
  // use distance from bottom left of voter region to top right of candidate region as max
  // (will get clipped if too long)

  float max_dx, max_dy;

  max_dx = C2V_X(cw) - 0;
  max_dy = C2V_Y(0) - ch;
  voter_ray_length = hypot(max_dx, max_dy);
  //  voter_ray_length = sqrt(SQUARE(max_dx) + SQUARE(max_dy));

  // initialize arrays of x, y ray components  (0 deg. = pointing horizontal right, 90 = vertical up, 180 = horizontal left
  // (in candidate coordinate system)

  voter_ray_dx = (float *) calloc(dom->gabor_num_orientations, sizeof(float));
  voter_ray_dy = (float *) calloc(dom->gabor_num_orientations, sizeof(float));

  for (i = 0, orientation = dom->gabor_start_orientation; 
       i < dom->gabor_num_orientations; 
       orientation += dom->gabor_delta_orientation, i++) {

    voter_ray_dx[i] = V2C_X(cos(DEG2RAD(orientation)) * voter_ray_length);
    voter_ray_dy[i] = -V2C_Y(sin(DEG2RAD(orientation)) * voter_ray_length); 
  }
}

//----------------------------------------------------------------------------

/// set up opengl so that we can use line-drawing and blending
/// to implement voting

void UD_VanishingPoint::initialize_opengl()
{
  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT);
  glColor3f(1, 1, 1);
  glLineWidth(1);
  glBlendFunc(GL_ONE, GL_ONE);

#ifdef HAVE_GEFORCE6
  init_fp_blend();
#endif
}

//----------------------------------------------------------------------------

/// draw rectangle outlining pixels that are voting for VP candidates

void UD_VanishingPoint::draw_voter_region()
{
  glColor3f(0,0,1);
  glLineWidth(1);
  glBegin(GL_LINE_LOOP);
  glVertex2f(im2can_x(voter_lateral_margin), ch - im2can_y(voter_top_margin));
  glVertex2f(im2can_x(w - voter_lateral_margin), ch - im2can_y(voter_top_margin));
  glVertex2f(im2can_x(w - voter_lateral_margin), ch - im2can_y(h - voter_bottom_margin));
  glVertex2f(im2can_x(voter_lateral_margin), ch - im2can_y(h - voter_bottom_margin));
  glEnd();
}

//----------------------------------------------------------------------------

/// draw maximum vote-getting location

void UD_VanishingPoint::draw_max()
{
  if (compute_max_candidate) {
    glPointSize(5);
    glColor3f(1, 0, 0);
    glBegin(GL_POINTS);
    glVertex2f(pmax.x, ch - pmax.y);
    glEnd();
  }
}

//----------------------------------------------------------------------------

/// calculate how many votes each location in candidate region received from
/// voters in voting region using Hough transform on lines drawn for all
/// dominant orientation rays.  maximum of 256 votes per candidate without 
/// HAVE_GEFORCE6 (max of 2048 with)

void UD_VanishingPoint::compute_linear_opengl(IplImage *DO_max_response_index)
{
  int vx, vy, i_max;
  float cx, cy, val_max;

  // set color somewhere before here

  // vote

  glClear(GL_COLOR_BUFFER_BIT);

#ifdef HAVE_GEFORCE6
  enter_fp_blend(cw, ch);
#endif

  glEnable(GL_BLEND);

  glColor3f(ONE_OVER_256, ONE_OVER_256, ONE_OVER_256);
//  glColor3f(1,1,1);

  glBegin(GL_LINES);

  for (vy = voter_top_margin; vy < h - voter_bottom_margin; vy++)
    for (vx = voter_lateral_margin; vx < w - voter_lateral_margin; vx++) {

      i_max = UCHAR_IMXY(DO_max_response_index, vx, vy);

      // exclude near-horizontal lines
      if (i_max > 1 && i_max < dom->gabor_num_orientations - 2) {	

	cx = FLOAT_IMXY(voter_ray_x, vx, vy);
	cy = FLOAT_IMXY(voter_ray_y, vx, vy);
	
	glVertex2f(cx, cy);
	glVertex2f(cx + voter_ray_dx[i_max], cy + voter_ray_dy[i_max]);
       
      }
  }

  glEnd();

  glDisable(GL_BLEND);

#ifdef HAVE_GEFORCE6
  exit_fp_blend();
#endif

  // read it back

  glRasterPos2i(0, 0);
  glReadPixels(0, 0, cw, ch, GL_RED, GL_FLOAT, candidate_image->imageData);

  // count votes for all candidates (not necessary for particle filtering)

  if (compute_max_candidate) {
    double maxval, minval;
    cvMinMaxLoc(candidate_image, &minval, &maxval, &pmin, &pmax);
    printf("max val %lf at (%i, %i)\n", maxval, pmax.x, pmax.y);
  }
}

//----------------------------------------------------------------------------

/// generic "compute vanishing point function" which is implemented here
/// by compute_linear_opengl().  should be a pure virtual function
/// so that various classes can be derived that compute the vanishing point
/// as well as curves differently

void UD_VanishingPoint::compute(IplImage *DO_max_response_index)
{
  if (first_compute_call) {
    initialize_opengl();
    first_compute_call = FALSE;
  }

  compute_linear_opengl(DO_max_response_index);
}

//----------------------------------------------------------------------------

/// process command-line flags related to vanishing point calculation

void UD_VanishingPoint::process_command_line_flags(int argc, char **argv) 
{
  int i;

  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i],                             "-sky"))
      voter_sky_fraction = atof(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-computemax"))
      compute_max_candidate = TRUE;
  }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#ifdef HAVE_GEFORCE6

// Enabling a higher vote total per candidate with 16-bit floating point 
// blending

//----------------------------------------------------------------------------

/// initialize buffers for 11-bit (2048 votes max) voting using 
/// Nvidia GeForce 6800 hardware-acclerated floating-point blending

void UD_VanishingPoint::init_fp_blend()
{
  int i, n;

  // initialize pbuffer

  ogl_disp = glXGetCurrentDisplay();
  ogl_draw = glXGetCurrentDrawable();
  ogl_oldcontext = glXGetCurrentContext();

  int pfAttribList[] = {
    GLX_RED_SIZE,               16,
    GLX_GREEN_SIZE,             16,
    GLX_BLUE_SIZE,              16,
    GLX_ALPHA_SIZE,             16,
    GLX_FLOAT_COMPONENTS_NV,    true,
    GLX_RENDER_TYPE_SGIX,
    GLX_RGBA_BIT_SGIX,
    GLX_DRAWABLE_TYPE_SGIX,
    GLX_PBUFFER_BIT_SGIX,
    0,
  };

  GLXFBConfig *glxConfig = glXChooseFBConfigSGIX(ogl_disp, 0, pfAttribList, &n);
  if (!glxConfig)
    UD_error("pbuffer creation error: glXChooseFBConfigSGIX() failed\n");

  int pbAttribList[] = {
    GLX_LARGEST_PBUFFER, true,
    GLX_PRESERVED_CONTENTS, true,
    0,
  };
    
  ogl_pbuffer = glXCreateGLXPbufferSGIX(ogl_disp, glxConfig[0], cw, ch, pbAttribList);  
  if (!ogl_pbuffer)
    UD_error("pbuffer creation error:  glXCreatePbufferSGIX() failed\n");

  ogl_context = glXCreateContextWithConfigSGIX(ogl_disp, glxConfig[0], GLX_RGBA_TYPE, ogl_oldcontext, true);
  if (!ogl_context)
    UD_error("pbuffer creation error:  glXCreateContextWithConfigSGIX() failed\n");
}

//----------------------------------------------------------------------------

/// activate GeForce blending buffer as drawing surface

void UD_VanishingPoint::enter_fp_blend()
{
  // activate pbuffer

  glXMakeCurrent(ogl_disp, ogl_pbuffer, ogl_context);

  // draw on it

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, cw, 0, ch);

  glClearColor(0.0, 0.0, 0.0, 0.0);
  glClear(GL_COLOR_BUFFER_BIT);

  glColor3f(1, 1, 1);
  glPointSize(1);
  glLineWidth(1);

  glBlendFunc(GL_ONE, GL_ONE);
}

//----------------------------------------------------------------------------

/// deactivate GeForce blending buffer as drawing surface

void UD_VanishingPoint::exit_fp_blend()
{
  // deactivate pbuffer

  //  glXMakeCurrent(ogl_disp, ogl_draw, ogl_oldcontext);
}

//----------------------------------------------------------------------------

/// end-to-end test to verify that GeForce offscreen drawing is working
/// (not necessary for vanishing point computation)

void UD_VanishingPoint::fp_blend_test()
{
  int width, i, j;

  //  print_opengl_info();
  
  //  half hbuffer;
  //  HALF_FLOAT_NV h;
  //  GLhalfNV h;

  width = 160;
  height = 120;

  float *testbuff = (float *) calloc(width * height, sizeof(float));

  init_fp_blend(width, height);
  enter_fp_blend(width, height);

  for (j = 0; j < 10; j++) {

  glClear(GL_COLOR_BUFFER_BIT);

  glEnable(GL_BLEND);

  glBegin(GL_POINTS);
  for (i = 0; i < 536; i++) 
  glVertex2f(0, 0);
  glEnd();

  glDisable(GL_BLEND);


  glReadPixels(0, 0, width, height, GL_RED, GL_FLOAT, testbuff);
  for (i = 0; i < width * height; i++)
    if (testbuff[i])
      printf("%f\n", testbuff[i]);
  }

  // deactivate pbuffer

  //  glXMakeCurrent(ogl_disp, ogl_draw, ogl_oldcontext);

  // end here

  exit(1);
}

#endif

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

