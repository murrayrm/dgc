//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_RoadFollower
//----------------------------------------------------------------------------

#ifndef UD_ROADFOLLOWER_DECS

//----------------------------------------------------------------------------

#define UD_ROADFOLLOWER_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glut.h>

#include "opencv_utils.hh"

// my stuff 

#include "UD_VanishingPoint.hh"
#include "UD_VP_PositionTracker.hh"
#include "UD_VP_VelocityTracker.hh"
#include "UD_VP_MultiTracker.hh"
#include "UD_DominantOrientations.hh"
#include "UD_ImageSource.hh"
#include "UD_LadarSource.hh"
#include "UD_Ladar_GapTracker.hh"
#include "UD_OnOff.hh"
#include "UD_Aerial_RoadTracer.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))

// camera calibration data; in degrees

#define DEFAULT_HFOV        45.0
#define PIX2RAD(x, w, hfov) ((hfov)*(x)/(w))     // can also use y, h, vfov

//-------------------------------------------------
// class
//-------------------------------------------------

class UD_Aerial_RoadTracer;

class UD_RoadFollower
{
 private:
  bool use_sunglare;
  bool sunglare_value;
  bool use_shadow;
  bool shadow_value;

 public:

  // variables

  int cum_iterations;

  int num_lookahead_pts;
  double *lookahead_x, *lookahead_z, *lookahead_radius, *lookahead_speed;
  double delta_lookahead;
  double max_lookahead;

  int have_state;
  double northing, easting, heading;
  double northing_velocity, easting_velocity;
  double northing_acceleration, easting_acceleration;
  double last_northing, last_easting;
  double last_northing_velocity, last_easting_velocity;

  UD_CameraCalibration *cal;

  UD_LadarSourceArray *ladarray;                       ///< pointer to array of ladar sources, if any
  UD_Ladar_GapTracker *gap_tracker;                    ///< filter that keeps track of clear space ahead
  UD_Aerial_RoadTracer *rtracer;

  int w, h;                                            ///< dimensions of source image
  int cw, ch;                                          ///< dimensions of VP candidate search region

  UD_VanishingPoint *vp;                               ///< vanishing point object associated with road tracker
  UD_VP_PositionTracker *straight_tracker;                ///< filter that keeps track of road direction
  //  UD_VP_VelocityTracker *straight_tracker;                        ///< filter that keeps track of road direction, curvature
  //UD_VP_MultiTracker *straight_tracker;                        ///< filter that keeps track of road direction, curvature
  UD_OnOff *oo;                                        ///< on/off road object associated with road tracker

  int num_curve_pts;
  UD_VP_PositionTracker **curve_tracker; 

  // "support_" variables all have to do with segmenting the road on the basis of where the 
  // estimated vanishing point's votes are coming from

  int support_num_orientations;                        ///< number of rays to shoot downward from vanishing point to identify road region
  float support_ray_length;                            ///< max distance traveled along each ray (will be clipped to actual image)
  float support_ray_min_length;                        ///< how many pixels of ray must be in image to count it
  float support_start_orientation;                     ///< positive angle below horizontal (in radians) of first ray to shoot on the
                                                       ///< right-hand side of VP (the end orientation is assumed symmetric)
  float support_delta_orientation;                     ///< angular interval between successive rays (radians)
  float *support_ray_dx;                               ///< x component of support ray direction 
  float *support_ray_dy;                               ///< y component of support ray direction 
  float *support_ray_vx;                               ///< x coordinates of support rays' clipped VP locations (image coords.)
  float *support_ray_vy;                               ///< y coordinates of support rays' clipped VP locations (image coords.)
  float *support_ray_ex;                               ///< x coordinate of support rays' clipped endpoint locations (image coords.)
  float *support_ray_ey;                               ///< y coordinate of support rays' clipped endpoint locations (image coords.)
  int *support_ray_long_enough;                        ///< true if ray >= min_length, false otherwise

  float support_angle_diff_thresh;                     ///< max mean angular discrepancy along ray from VP (in radians) to count as "support"
  float *support_angle_diff;                           ///< actual mean angular discrepancies along all rays from VP
  float support_mean_dx, support_mean_dy;              ///< unit vector in direction of mean support ray (i.e., instantaneous 
                                                       ///< measurement of road centerline)
  float support_cur_dx, support_cur_dy;                ///< temporally filtered version of support_mean--the centerline estimate we use  
  float support_alpha;                                 ///< blending coefficient for combining last support_cur with current support_mean
  float support_lat_dx, support_lat_dy;                ///< longer version of support_cur for drawing & finding intersection with im. bottom
  int show_support;                                    ///< draw support, non-support rays?
  int show_centerline;                                 ///< draw road centerline estimate (aka support_lat)?

  // key output variables

  float direction_error;                               ///< difference between vehicle heading and estimated road heading, in radians 
                                                       ///< (vehicle heading too far left = +, too far right = -)

  float lateral_x;                                     ///< intersection of estimated centerline of road with bottom of image, in pixels
  float lateral_error;                                 ///< lateral_x, converted to radians with intrinsic camera calibration info. 
  float lateral_offset;                                ///< lateral_error angle converted to meters using extrinsic camera calibration
  float laser_offset;                                  ///< gap tracker's estimated offset to middle of safe area

  // functions

  UD_RoadFollower(UD_VanishingPoint *, UD_OnOff *, UD_LadarSourceArray *);

  int is_synced();
  void get_state();

  void estimate_curvature();

  void run();
  int iterate();
  void draw(UD_ImageSource *);
  void cycle_draw_mode_image();
  void cycle_draw_mode_overlay();

  void reset();
  void write(FILE *);

  void process_command_line_flags(int, char **);

  void compute_direction_error(UD_CameraCalibration *);
  void print_output_to_controller();
  void check_onroad();

  void send_road_width();
  void initialize_road_width();
  void estimate_road_width();

  void initialize_support(float, float);
  float compute_support();   
  void draw_support();           
  float centerline_angle_diff(CvPoint, CvPoint, float *, float *);
  float compute_centerline();
  void draw_centerline();
  int clipcode(float, float, float, float, float, float);
  float clip_support_ray(float, float, float, float, float *, float *, float *, float *, float, float, float, float);

  void draw_vehicle_shadow();

};

UD_RoadFollower *initialize_position_UD_RoadFollower(UD_VanishingPoint *, UD_OnOff *);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
