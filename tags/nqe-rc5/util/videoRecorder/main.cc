#include<SDL/SDL.h>

#include"videoRecorder.hh"
#include"videoPlayer.hh"
#include"smart_counter.hh"

#include"sparrowhawk.hh"

//#include<highgui.h>

#include<getopt.h>

void print_surf(SDL_Surface *p)
{
  printf("Flags: %i\n", p->flags);
  printf("Format bits per pixel: %i\n", p->format->BitsPerPixel);
  printf("Format bytes per pixel: %i\n", p->format->BytesPerPixel);
  printf("Format masks: r:%x  g:%x  b:%x  a:%x\n", p->format->Rmask, p->format->Gmask, p->format->Bmask, p->format->Amask);
  printf("dim -  w:%i  h:%i\n", p->w, p->h);
  printf("pitch: %i\n", p->pitch);
}

void print_help()
{
  printf("Usage: videoRecorder [OPTION]\n");
  printf("  --nosparrow             Prevents the sparrow inteface from starting\n");
  printf("  --skynetkey key         Run on supplied skynet key\n");
  printf("  --snkey key             Run on supplied skynet key\n");
  printf("  --display               Displays video feed on startup\n");
  printf("  --playback              Playback video from timber\n");
}

struct CmdOpt
{
  int sparrow;
  int skynet_key;
  int display;
  int playback;
};

bool parse_cmd(int argc, char *argv[], CmdOpt &opt)
{
  //Set default options
  opt.sparrow=1;
  opt.skynet_key=-1;
  opt.display=0;
  opt.playback=0;

  //Parse env.
  char *pkey = getenv("SKYNET_KEY");
  if(pkey)
    opt.skynet_key = atoi(pkey);

  //Parse cmd
  const int OPT_HELP=10;
  const int OPT_SNKEY=11;

  static struct option long_options[] = {
    //Options that don't require arguments
    {"nosparrow", no_argument, &opt.sparrow, 1},
    {"help",      no_argument, 0, OPT_HELP},

    {"snkey", required_argument, 0, OPT_SNKEY},
    {"skynetkey", required_argument, 0, OPT_SNKEY},
    {"display", no_argument, &opt.display, 1},
    {"playback", no_argument, &opt.playback, 1},

    {0,0,0,0}
  };


  while (1) {
    int option_index = 0;
    
    int c = getopt_long(argc, argv, "", long_options, &option_index);
    if(c == -1) break;

    switch(c) {
    case OPT_HELP:
      print_help();
      return false;
    case OPT_SNKEY:
      opt.skynet_key = atoi(optarg);
      break;
    default:
      if(c!=0) {
	printf("Unknown option %d!\n", c);
	print_help();
	return false;
      }
    }
  }

  if(opt.skynet_key==-1) {
    printf("You have not specified a skynet key as a command-line argument (using --snkey)\n");
    return false;
  }

  return true;
}

int main(int argc, char *argv[])
{
  CmdOpt opt;
  if(!parse_cmd(argc, argv, opt))
    return 0;

  if(!opt.playback) {
    CVideoRecorder vr(opt.skynet_key, opt.sparrow==1);
    
    vr.setCamera(0);
  
    if(opt.display)
      vr.setDisplay(true);
  
    SparrowHawk().run();

    while(!vr.terminated())
      usleep(10000);

  } else {
    
    CVideoPlayer vp(opt.skynet_key, false);

    vp.playback();
  }

  return 0;
}
