#include"videoPlayer.hh"
#include"sparrowhawk.hh"
#include"sn_types.h"

#include<SDL/SDL.h>
#include <cv.h>
#include <highgui.h>

#include<sys/stat.h>
#include<unistd.h>

#include<iostream>

CVideoPlayer::CVideoPlayer(int skynet_key, bool sparrow)
  : CSkynetContainer(MODvideoRecorder, skynet_key),
    CTimberClient(timber_types::videoRecorder),
    m_log_index_file(0), m_display_inited(false),
    m_screen(0), m_surf(0)
{
  m_terminate=false;
  m_bytes_read.clear();

  DGCcreateMutex(&m_mutex);

  setDisplay(true);
}

CVideoPlayer::~CVideoPlayer()
{
  setDisplay(false);

  DGCdeleteMutex(&m_mutex);
}

/*
**
** Playback
**
*/
void CVideoPlayer::playback(bool threaded) {
  if(threaded) {
    DGCstartMemberFunctionThread(this, &CVideoPlayer::playerThread);
  } else {
    playerThread();
  }
}

/*
**
** Accessors
**
*/
bool CVideoPlayer::terminated() const
{
  return m_terminate;
}

double CVideoPlayer::displayFramerate() const
{
  return m_display_cnt.freq;
}

long long CVideoPlayer::bytesRead() const
{
  return m_bytes_read.cnt;
}

double CVideoPlayer::bytesReadRate() const
{
  return m_bytes_read.freq;
}

/*
**
** Setters
**
*/

//Used internally when already locked
void CVideoPlayer::setDisplay(bool display)
{
  DGClockMutex(&m_mutex);

  if(display && !m_display_inited) {   //Initialize SDL
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_NOPARACHUTE) < 0) {
      DGCunlockMutex(&m_mutex);
      return;
    }
    atexit(SDL_Quit);
    m_display_inited=true;
  }

  if(m_surf) {   //Delete display surfaces
    SDL_FreeSurface(m_surf);
    m_surf=0;
  }

  if(display) {    //Create new display surfaces
    m_screen = SDL_SetVideoMode(320, 240, 
				24, SDL_DOUBLEBUF | SDL_ANYFORMAT);
    if(!m_screen) {
      DGCunlockMutex(&m_mutex);
      return;
    }
    SDL_WM_SetCaption("Video Player", NULL);

    m_surf = SDL_CreateRGBSurface(SDL_SWSURFACE, 320, 240, 24,
                 m_screen->format->Rmask, m_screen->format->Gmask,
                 m_screen->format->Bmask, m_screen->format->Amask);
    if(!m_surf) {
      SDL_Quit();
      m_screen=0;
      DGCunlockMutex(&m_mutex);
      return;
    }
  } else {
    if(m_screen)
      SDL_Quit();
    m_screen=0;
  }
  DGCunlockMutex(&m_mutex);
}


/*
**
** Helper functions
**
*/

//Thread reading from camera, exposure controlling and saving to file
void CVideoPlayer::playerThread()
{
  unsigned long long current_time;
  unsigned long long image_time;

  char buf[1024];
  char filename[1024];

  cerr<<"Entered player thread"<<endl;
  while(!m_terminate) {
    usleep(1000);   //Leave time when all mutexes are unlocked 

    //Get the next image
    string dir;
    dir = getPlaybackDir();
    //If new directory or time has moved backwards
    if(checkNewPlaybackDirAndReset() || current_time>getPlaybackTime()) {
      if(m_log_index_file)
	fclose(m_log_index_file);
      
      string file = dir + "index.log";
      m_log_index_file = fopen(file.c_str(), "r");
      if(!m_log_index_file) {
	cerr<<"Error opening log index file "<<file<<endl;
	m_terminate=true;
	continue;
      }

      cerr<<"Loaded log index file"<<endl;
    }

    //Read from index file to get next filename
    if(!m_log_index_file)
      continue;

    current_time = getPlaybackTime();
    image_time = 0;

    bool breakout=false;
    do {
      
      do {  //Find next - none commented - line
	if(!fgets(buf, sizeof(buf), m_log_index_file)) {
	  if(feof(m_log_index_file)) {
	    //End of file
	    usleep(10000);
	    breakout=true;
	    break;
	  }
	  cerr<<"Error reading line from index file"<<endl;
	  breakout=true;
	  break;
	}
      } while(buf[0]=='#' && !breakout);
      
      if(breakout)
	break;

      sscanf(buf, "%lli %s", &image_time, filename);
    } while(image_time < current_time);
    if(!breakout) {
      string file = dir + filename;

      IplImage *image = cvLoadImage(file.c_str());
      if(m_screen && image) {
	displayImage(image);
      }
      
      if(image) {  //Delete the image
	cvReleaseImage(&image);
      }
    }

    do {
      //Check if we've got input through SDL
      SDL_Event event;
      while(SDL_PollEvent(&event)) {
	switch(event.type) {
	case SDL_QUIT:
	  m_terminate=true;
	  break;
	case SDL_KEYDOWN:
	case SDL_KEYUP:
	  if(event.key.keysym.sym==27) {
	    m_terminate=true;
	    break;
	  }
	  if((event.key.keysym.mod & (KMOD_LCTRL | KMOD_RCTRL))!=0
	     && (event.key.keysym.sym=='c' || event.key.keysym.sym=='C')) {
	    m_terminate=true;
	    break;
	  }
	  break;
	}
      }

      usleep(10000);
    } while(image_time > getPlaybackTime() && 
	    current_time <= getPlaybackTime() &&
	    !m_terminate);
  }
}

void CVideoPlayer::displayImage(IplImage *image)
{
  //Update screen
  if(SDL_LockSurface(m_surf)==0) {
    //cerr<<"Surface locked"<<endl;
    memcpy(m_surf->pixels, image->imageData, image->width*image->height*3);
    SDL_UnlockSurface(m_surf);
    
    SDL_Rect rc;
    rc.x=0;
    rc.y=0;
    rc.w = 320;
    rc.h = 240;
    if(SDL_BlitSurface(m_surf, &rc, m_screen, &rc) == 0) {
      SDL_Flip(m_screen);
      //cerr<<"Flipped"<<endl;
    }
    
    ++m_display_cnt;
  }
}


/*
**
** Sparrow interface functions
**
*/
void CVideoPlayer::set_read_bytes()
{
  long long b = bytesRead();
  static char buf[100];

  if(b<1000) {  //b
    sprintf(buf, "%i b    ", (int)b);
  } else if(b<1000000) {       //kb
    sprintf(buf, "%.1f kb    ", b/1000.0);
  } else if(b<1000000000) {    //Mb
    sprintf(buf, "%.1f Mb    ", b/1000000.0);
  } else if(b<1000000000000ll) { //Gb
    sprintf(buf, "%.1f Gb    ", b/1000000000.0);
  } else { //Tb
    sprintf(buf, "%.1f Tb   ", b/1000000000000.0);
  }
  //SparrowHawk().set_string("StatLogedData", buf);
}

void CVideoPlayer::set_read_rate()
{
  double b = bytesReadRate();
  static char buf[100];

  if(b<1000) {  //b
    sprintf(buf, "%i b/s   ", (int)b);
  } else if(b<1000000) {       //kb
    sprintf(buf, "%.1f kb/s    ", b/1000.0);
  } else if(b<1000000000) {    //Mb
    sprintf(buf, "%.1f Mb/s    ", b/1000000.0);
  } else if(b<1000000000000ll) { //Gb
    sprintf(buf, "%.1f Gb/s    ", b/1000000000.0);
  } else { //Tb
    sprintf(buf, "%.1f Tb/s   ", b/1000000000000.0);
  }
  //SparrowHawk().set_string("StatLogRate", buf);
}
