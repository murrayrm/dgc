#include <iostream>
#include <fstream>
#include <iomanip>
#include <unistd.h>
#include <string>
#include <sstream>

#include "PID_Controller.hh"
#include "VehicleState.hh"

using namespace std;

int main(void)
{

  double phi, accel;

  char* loadFilename = "default.traj";
  ifstream infile(loadFilename);

  CTraj* m_traj;
  m_traj   = new CTraj(3, infile);

  VehicleState state;
  ActuatorState actuator_state;
  state.Northing = 0;
  state.Easting = 0;
  state.Vel_E = 0;
  state.Vel_N =0;
  state.Vel_D = 0;
  state.Yaw = 0;
  state.YawRate	= 0.0;

  CPID_Controller* m_follow = new CPID_Controller;
  m_follow->getControlVariables(&state, &actuator_state, &accel, &phi, 0 , "", 0);

  cout<<"Commanded accel: "<<accel<<endl;
  cout<<"Commanded phi:   "<<phi<<endl;

  m_follow->SetNewPath(m_traj, &state);

  m_follow->getControlVariables(&state, &actuator_state, &accel, &phi, 0, "", 0);
  cout<<"set new path, just testing that it isn't broken"<<endl;
  cout<<"new accel: "<<accel<<endl;
  cout<<"new phi:   "<<phi<<endl;

  return 0;
  /*
read in path from file
initialize controller w/ that path and fake state
run getcontrol vars
print out internal variables
accessor functions (for realtime diagnostics--use more member variables)
	these will be public functions that return internal variables

use setnewpath
  */
  }
