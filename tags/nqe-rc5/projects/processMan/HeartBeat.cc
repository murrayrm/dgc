/**
 * HeartBeat.cc
 * Revision History:
 * 20/08/2005  hbarnor  Created
 * $Id$
 */

#include "HeartBeat.hh"

HeartBeat::HeartBeat(string localIP, struct pmRmMesg * heartBeat)
  : m_localIP(localIP),
    newConnection(false), 
    globalConnfd(NO_FILE_DESC) 
{
  // install the signal handler for sigpipe
  signal(SIGPIPE,HeartBeat::handleSigPipe);
  memcpy(&m_heartBeat,heartBeat, sizeof(m_heartBeat));
  //p_myHeartBeat = heartBeat;
  DGCcreateMutex(&newConnMutex);
  DGCstartMemberFunctionThread(this, &HeartBeat::heartBeat);
  DGCstartMemberFunctionThread(this, &HeartBeat::server);
}


HeartBeat::~HeartBeat()
{
  cout << " Killing heartbeat" << endl;
  close(globalConnfd);
}


void HeartBeat::server()
{
  int listenfd;
  int localConnfd;
  struct sockaddr_in servAddr;
  struct sockaddr_in cliAddr;
  socklen_t cliLen; 
 // get a tcp socket 
  listenfd = socket(AF_INET, SOCK_STREAM, 0);
  // clear the address structure
  bzero(&servAddr, sizeof(servAddr));
  servAddr.sin_family = AF_INET;
  servAddr.sin_port = htons(HEARTBEAT_PORT);
  int err = inet_pton(AF_INET, m_localIP.c_str(), &servAddr.sin_addr);
  if(err != 1)
    {
      cerr << "Error starting server on localIP. " << endl;
    }
  //bind to IP and port 
  err = bind(listenfd, (struct sockaddr *) &servAddr, sizeof(servAddr));
  if(err != 0)
    {
      cerr << "Error on heart beat server bind " << endl; 
    }
  // make socket a listen socket with backlog = LISTENQ
  listen(listenfd, LISTENQ);
  while(true)
    {
      cliLen = sizeof(cliAddr);
      localConnfd = accept(listenfd, (struct sockaddr *) &cliAddr, &cliLen);
      if(localConnfd < 0)
	{
	  cerr << "Failed to accept connection. \n Continueing ... " << endl;
	}
      else
	{
	  globalConnfd = localConnfd;
	  DGClockMutex(&newConnMutex);
	  newConnection = true;
	  DGCunlockMutex(&newConnMutex);
	}
    }
  cout << " This line cannot be achieved. \n Exiting ....." << endl;
  exit(-1);
}

void HeartBeat::heartBeat()
{
 int currentfd = globalConnfd;
  while(true)
    {
      if(newConnection)
	{
	  close(currentfd);
	  currentfd = globalConnfd;
	  DGClockMutex(&newConnMutex);
	  newConnection = false;
	  DGCunlockMutex(&newConnMutex);
	}
      ssize_t i = writeData(currentfd, &m_heartBeat, sizeof( struct pmRmMesg ) );
      if(i < 0)
	{
	  switch(i)
	    {
	    case -1: 
	      cerr << "Failed to write to network " << endl;
	      if(errno == EPIPE)
		{
		  cerr << "Socket was closed" << endl;
		  close(currentfd);
		  currentfd = NO_FILE_DESC;
		}
	      break;
	    case -2:
#ifdef DEBUG
	      cerr << "No connection on server. " << endl;
#endif
	      break;
	    default:
	      cerr << "Write to network - unknown error " << endl;
	    }
	}
      usleep(HEART_PERIOD);
    }
}


ssize_t HeartBeat::writeData(int filedes, const void * data, size_t nbytes)
{
  //internal persistent memory
  if(filedes != NO_FILE_DESC )
    {
      size_t nleft = nbytes;
      ssize_t nwritten;
      const char * ptr = (char *)data;
      while(nleft > 0)
	{
	  if( (nwritten = write(filedes, ptr, nleft)) <= 0)
	    {
	      if(errno == EINTR)
		{
		  nwritten = 0;
		}
	      else
		{
		  return -1;
		}
	    }
	  nleft -= nwritten;
	  ptr += nwritten;
	}
      return nbytes;
    }
  else
    {
      return -2;
    }
}

Sigfunc * HeartBeat::signal(int signo, Sigfunc *func)
{
  struct sigaction act, oact;
  act.sa_handler = func;
  sigemptyset(&act.sa_mask);
  act.sa_flags = 0;
  if(signo == SIGALRM)
    {
      act.sa_flags |= SA_INTERRUPT;      
    }
  else
    {
      act.sa_flags |= SA_RESTART;
    }
  
  if(signo == SIGPIPE)
    {
      act.sa_handler = SIG_IGN;
    }
  
  if(sigaction(signo, &act, &oact) < 0)
    {
      return SIG_ERR;
    }
  return oact.sa_handler;
}

void HeartBeat::handleSigPipe(int signo)
{
  cerr << "received sigpipe" << endl;
}
