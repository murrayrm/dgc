#include <iostream>
#include <octave/oct.h>

using namespace std;

main() 
{
   int sz=20;
   Matrix m = Matrix(sz,sz);
   for (int r=0;r<sz;r++) 
   {
     for (int c=0;c<sz;c++) 
     {
       m(r,c)=r*c;
     }
   }
   cout << "Hello world! " << endl;
   cout << m;
}
