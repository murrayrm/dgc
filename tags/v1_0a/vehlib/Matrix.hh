/*********************************** 
Haomiao Huang
3 x 3 matrix class
converted to turn into rotation matrices
***********************************/

  const int rows = 3;
  const int cols = 3;


class Matrix
{
private:
  double data[rows][cols];

public:
  // Constructors
  Matrix();  // create empty matrix
  Matrix(const Matrix &matrix2); // copy constructor

  // Mutators
  void setelem(int row, int col, double value);  
  // sets the rowth colth element the matrix to value

  // Accessors
  int getrows() const;  // returns the number of rows
  int getcols() const;  // returns the number of columns
  double getelem(int row, int col) const;  
  // returns the rowth, colth element of the matrix

  // Operators
  // compares with matrix2
  bool operator==(const Matrix &matrix2) const;  
  
  // add another r x c matrix
  Matrix operator+(const Matrix &matrix2) const;  
  
  // subtract another r x c matrix
  Matrix operator-(const Matrix &matrix) const;  

  // check for not equal
  bool Matrix::operator!=(const Matrix &matrix2); 

  // add another r x c matrix to self
  Matrix& operator+=(const Matrix &matrix2);
  
  // subtract another r x c matrix from self
  Matrix& operator-=(const Matrix &matrix);

  // multiply by another matrix
  Matrix operator*(const Matrix &matrix2) const;  
  
  // multiply self by another matrix
  Matrix& operator*=(const Matrix &matrix);  
  
  // set one matrix equal to another
  Matrix& operator=(const Matrix &matrix2);

  // Destructors
  ~Matrix();  
};
