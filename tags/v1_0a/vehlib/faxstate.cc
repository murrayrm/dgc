/*
 * faxstate.cc - vehicle state estimtor
 *
 * RMM 31 Dec 03
 *
 * This program is the state estimator for the vehicle.  It provides an
 * on-screen display of the vehicle state and also sets up an MTA mailbox
 * that can be used to query the vehicle state.
 *
 * H 03 Jan 04 
 * Added magnetometer reading 
 * Added statefilter code
 *
 * Alex Fax 15 Jan 04
 * Kalman Filter added, statefilter no longer is run
 *
 * H 16 Jan 04
 * added data logging and display to KF
 * gps valid display
 * 
 */

#include <stdlib.h>
#include <iostream.h>
#include <unistd.h>
#include <pthread.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"
#include "sparrow/channel.h"
#include "gps.h"
#include "imu.h"
#include "magnetometer.h"
#include "rot_matrix.hh"
#include "vehports.h"
#include <fstream.h>

#include "MTA/Modules.hh"
#include "vsmta.hh"
#include "MTA/Kernel.hh"
#include <boost/shared_ptr.hpp>

#include "VState.hh"
#include "statefilter.hh"
#include "LatLong.h"

#include "kfilter/kfilter.h"
#include "kfilter/Kalmano.h"
#include "kfilter/Global.h"

/* Functions defined in this file */
static int user_quit(long);
int veh_gps_init(long), veh_gps_process(long);

extern int NavBufferIndex;
extern TSaveNavData NavBuffer[XRATIO2];

extern double gps_lat, gps_lon, gps_alt;
extern double gps_vn, gps_ve, gps_vu;
extern double gps_time;
extern double imu_time;
extern double mag_hdg;

extern int New_GPS_data_flag;   // flag set when new GPS data comes in
extern int New_Mag_data_flag;

extern double imu_dvx,imu_dvy,imu_dvz; // Delta-v's m/s (should be)
extern double imu_dtx,imu_dty,imu_dtz; // Delta-Thetas,should be rads

extern double kp[NUM_ST+1][NUM_ST+1];
extern double kx[NUM_ST+1];

extern double tot_kabx, tot_kaby, tot_kabz;
extern double tot_ksfx, tot_ksfy, tot_ksfz;
extern double tot_kgbx, tot_kgby, tot_kgbz;
extern double tot_kgsfx;

extern double ExtGPSPosMeas_x, ExtGPSPosMeas_y, ExtGPSPosMeas_z;
extern double ExtGPSVelMeas_x, ExtGPSVelMeas_y, ExtGPSVelMeas_z;



/* Variables used in this file */
int mta_flag = 1;			/* run MTA */
struct VState_GetStateMsg vehstate;	/* current vehicle state */
int state_good = 0;                      /* flag for MTA to tell that state data is good, 1 is new, 0 not updated, -1 is updating */

// channel logging declarations
// guide to log can be found in sreadme
char config_file[FILENAME_MAX] =	/* configuration file */
  "kfconfig.ini"; 
char dumpfile[FILENAME_MAX] = "kfstate.dat";
DEV_LOOKUP chn_devlut[] = {
  {"virtual", virtual_driver},
  {NULL, NULL}
};
struct timeval tv;
static int start = 0;

// state internal variables not passed to outside world
/* Variables used in this file */
rot_matrix N2B;

// note, enable = 0 means device is enabled but not active, enabled = 1 means device has been initialized
// update flags are for statefilter, 0= no new data, 1 = new data received, -1 = currently updating
pthread_t gps_thread;			/* GPS thread */
void *gps_start(void *);		/* GPS startup routine */
int veh_gps_com = GPS_SERIAL_PORT;	/* serial port for GPS */
int veh_gps_rate = 10;			/* GPS rate (Hz) */
gpsDataWrapper gpsdata;			/* GPS data */
static char *gps_buffer;		/* buffer for holding GPS data */
int gps_enabled = 0;			/* flag for enabling GPS */
int gps_count = 0;			/* keep track of GPS reads */
int new_gps = 0;                        /* gps update flag */ 
double gheading;                        /* gps heading, used to compare gps heading with magnetometer heading */
int gps_valid;                          /* valid gps pvt flag */

/* Define variable for IMU thread */
pthread_t imu_thread;			/* IMU thread */
void *imu_start(void *);		/* IMU startup routine */
IMU_DATA imudata;			/* IMU data */
int imu_enabled = 0;			/* flag for enabling IMU */
int imu_count = 0;			/* keep track of IMU reads */
int new_imu = 0;                        /* imu update flag */
double p, r, y;

/* Magnetometer definitions */
pthread_t mag_thread;                   /* magnetometer thread */
void *mag_start(void *);                /* magnetometer startup routine */
mag_data magreading;                    /* read magnetometer data */
int veh_mag_com = MAG_SERIAL_PORT;      /* magnetometer serial port */
int mag_enabled = 0;                    /* magnetometer enable flag */
int mag_count = 0;                      /* keep track of magnetometer reads */
int new_mag = 0;                        /* mag update flag */

//********************* Data logging hack *************************
fstream logfile;                        // temporary fstream for data logging until chn_write stuff is fixed
char *logname = "statelog.dat";
int logflag = 0;                        // enable/disable data logging
double gps_local_time;

/* Usage message */
char *usage = "\
Usage: %s [-v] [options]\n\
  -g    disable GPS subsystem\n\
  -h    print this message\n\
  -i    disable IMU subsystem\n\
  -m    disable MTA subsystem\n\
  -v    turn on verbose error messages\n\
  -a    turns off magnetometer\n\
  -l    turns on data logging\n\
";


// statefilter definitions
pthread_t filter_thread;                /* state filter thread */
void *statefilter(void *);              /* state filter routine */

#include "kfdisp.h"			/* display */
pthread_t dd_thread;			/* display thread */

int main(int argc, char **argv)
{
    int c, errflg = 0, error = 0;

    /* Turn on error processing during startup */
    dbg_flag = dbg_all = dbg_outf = 1;

    /* Parse command line arguments */
    while ((c = getopt(argc, argv, "vimghal?")) != EOF)
      switch (c) {
      case 'v':		dbg_flag = 1;			break;
      case 'i':		imu_enabled = -1;		break;
      case 'm':		mta_flag = -1;			break;
      case 'g':		gps_enabled = -1;		break;
      case 'h':         errflg++;                      break;
      case 'a':         mag_enabled = -1;               break;
      case 'l':         logflag = 1;                    break;
      default:		errflg++;			break;
	break;
      }

    /* Print an error message if anything went wrong */
    if (errflg || argc < optind) {
      fprintf(stderr, "usage: %s [-v]\n", argv[0]);
      exit(1);
    }


    /*
     * Device Initialization
     *
     * Initialize all of the devices that might causes errors that
     * would stop us from running.
     *
     */

     // ***************** Data logging hack ***************
    if (logflag)
      {
	logfile.open(logname, fstream::out|fstream::app);
        logfile<<"%imu_time"<<'\t'<<"imudata.dvx"<<'\t'<<"imudata.dvy"<<'\t'<<"imudata.dvz"<<'\t'<<"imudata.dtx"<<'\t'<<"imudata.dty"<<'\t'<<"imudata.dtz"<<'\t'<<"gps_local_time"<<'\t'<<"gps_valid"<<'\t'<<"New_GPS_data_flag"<<'\t'<<"gpsdata.data.lat"<<'\t'<<"gpsdata.data.lng"<<'\t'<<"gpsdata.data.vel_n"<<'\t'<<"gpsdata.data.vel_e"<<'\t'<<"gpsdata.data.vel_u"<<'\t'<<"imu_time"<<'\t'<<"vehstate.Northing"<<'\t'<<"vehstate.Easting"<<'\t'<<"vehstate.kf_lat"<<'\t'<<"vehstate.kf_lng"<<'\t'<<"vehstate.Altitude"<<'\t'<<"vehstate.Vel_N"<<'\t'<<"vehstate.Vel_E"<<'\t'<<"vehstate.Vel_U"<<'\t'<<"vehstate.Speed"<<'\t'<<"vehstate.Pitch"<<'\t'<<"vehstate.Roll"<<'\t'<<"vehstate.Yaw"<<'\t'<<"ExtGPSPosMeas_x"<<'\t'<<"ExtGPSPosMeas_y"<<'\t'<<"ExtGPSPosMeas_z"<<'\t'<<"ExtGPSVelMeas_x"<<'\t'<<"ExtGPSVelMeas_y"<<'\t'<<"ExtGPSVelMeas_z"<<endl;
      }


    /* GPS initialization; gps_enabled = -1 if disabled from command line */
    if (gps_enabled != -1) {
      if (init_gps(veh_gps_com, veh_gps_rate) != 1) {
	dbg_error("GPS: initialization failure\n");
	++error;
      } else
	gps_enabled = 1;
    } else
      gps_enabled = 0;
    
    /* Mag initialization; mag_enabled = -1 if disabled from command line */
    if (mag_enabled != -1) {
      if (mm_init(veh_mag_com) != 0){
	dbg_error("Magnetometer error");
	++error;
      }else{
	mag_enabled = 1;
	// initialize pry data from magnetometer
	get_mag_data(magreading);
	mag_update(magreading, vehstate, N2B);
      }
      }else
	mag_enabled = 0;

    /* IMU initialization; imu_enabled = -1 if disabled from command line */
    if (imu_enabled != -1) {
      if (InitIMU(&imudata) < 0) {
	fprintf(stdout, "IMU error\n");
	++error; 
      } else {
	imu_enabled = 1;
        DGCNavInit();
      }
    } else
      imu_enabled = 0;

    /* Initialize sparrow channel structures */
    if (chn_config("kfconfig.dev") < 0) {
      dbg_error("can't open kfconfig.dev");
      ++error;
    } else if (chn_init() < 0) {
      dbg_error("error initializing channel interface");
      ++error;
    }

    // initialize starting time 
    gettimeofday(&tv, NULL); start = tv.tv_sec;

    /* Pause if there are any errors */
    dbg_info("gps_enabled = %d, imu_enabled = %d, mag_enabled = %d\n", 
	     gps_enabled, imu_enabled, mag_enabled);
    if (error) {
      fprintf(stdout, "Errors on startup; continue (y/n)? ");
      if (getchar() != 'y') exit(1);
    }

    /* Turn off printf while display is running */
    dbg_all = 0;

    /* Initialize the display package */
    if (dd_open() < 0) exit(1);		/* initialize the database, screen */
    dd_bindkey('Q', user_quit);
    dd_usetbl(overview);		/* set display description table */

    /* Start up threads for execution */
    if (pthread_create(&imu_thread, NULL, imu_start, (void *) NULL) < 0) {
      dbg_error("Can't start IMU thread \n");
      exit(1);
    }
    if (pthread_create(&gps_thread, NULL, gps_start, (void *) NULL) < 0) {
      dbg_error("Can't start GPS thread \n");
      exit(1);
    }
    if (pthread_create(&mag_thread, NULL, mag_start, (void *) NULL) < 0) {
      dbg_error("Can't start mag thread \n");
      exit(1);
    }
    //    if (pthread_create(&filter_thread, NULL, statefilter, (void *) NULL) < 0) {
    //      dbg_error("Can't start statefilter thread \n");
    //      exit(1);
    //    }
    /* Run the display manager as a thread */
    pthread_create(&dd_thread, NULL, dd_loop_thread, (void *) NULL);
    if (!mta_flag)
      /* If we aren't running MTA, wait until dd_loop ends */
      pthread_join(dd_thread, (void **) NULL);
    else {
      /* Otherwise, pass control to MTA */
      Register(shared_ptr<DGC_MODULE>( new VState ));
      StartKernel();
    }
    dd_close();				/* clear the screen and free memory */

    /* User cleanup goes here */
}

/*
 * Process threads 
 *
 * These routines define the process threads used in vstate.  Each of
 * them should be started up in the main code.  Once called, they
 * should loop forever.
 *
 * TBD: should integrate initialization and move these routines to their
 * respective files, so that startup becomes particularly simple.
 *
 */

void *imu_start(void *arg)
{
  LatLong latlong(0 , 0); // latlong conversion

  /* Thread reads the imu, calls a KF update, and then stores results from KF */
  while (1) 
    if ((imu_enabled > 0)) {
      IMURead(&imudata);
      ++imu_count;

      //log data
      /* Save the time the data was taken */
      gettimeofday(&tv, NULL);
      
      // pass data to Kalman filter   
      imu_time = (double) (tv.tv_sec - start) + ((double) tv.tv_usec) / 1e6;
      imu_dvx = imudata.dvx;
      imu_dvy = imudata.dvy;
      imu_dvz = imudata.dvz;
      imu_dtx = imudata.dtx;
      imu_dty = imudata.dty;
      imu_dtz = imudata.dtz;
      
      // log raw imu data
      chn_data(0) = imu_time;
      chn_data(1) = imudata.dvx;
      chn_data(2) = imudata.dvy;
      chn_data(3) = imudata.dvz;
      chn_data(4) = imudata.dtx;
      chn_data(5) = imudata.dty;
      chn_data(6) = imudata.dtz;

      
      chn_data(7) = imu_time;
      chn_data(8) = NavBuffer[NavBufferIndex].lat / DEG2RAD;
      chn_data(9) = NavBuffer[NavBufferIndex].lon / DEG2RAD;
      chn_data(10) = NavBuffer[NavBufferIndex].vn;
      chn_data(11) = NavBuffer[NavBufferIndex].ve;
      chn_data(12) = NavBuffer[NavBufferIndex].vd;
      chn_data(13) = NavBuffer[NavBufferIndex].thdg;
      chn_data(14) = NavBuffer[NavBufferIndex].pitch;
      chn_data(15) = NavBuffer[NavBufferIndex].roll;
      chn_data(16) = NavBuffer[NavBufferIndex].yaw;
     
      chn_data(23) = ExtGPSPosMeas_x;
      chn_data(24) = ExtGPSPosMeas_y;
      chn_data(25) = ExtGPSPosMeas_z;
      chn_data(26) = ExtGPSVelMeas_x;
      chn_data(27) = ExtGPSVelMeas_y;
      chn_data(28) = ExtGPSVelMeas_z;

      
      // stores KF data into vehstate
      vehstate.Timestamp = TVNow();
      vehstate.kf_lat = NavBuffer[NavBufferIndex].lat / DEG2RAD;
      vehstate.kf_lng = NavBuffer[NavBufferIndex].lon / DEG2RAD;
      vehstate.Altitude = NavBuffer[NavBufferIndex].alt;
      latlong.set_latlon(vehstate.kf_lat, vehstate.kf_lng);
      latlong.get_UTM(&vehstate.Easting, &vehstate.Northing);
      

      vehstate.Vel_N = NavBuffer[NavBufferIndex].vn;
      vehstate.Vel_E = NavBuffer[NavBufferIndex].ve;
      vehstate.Vel_U = NavBuffer[NavBufferIndex].vd;
      vehstate.Speed = sqrt(vehstate.Vel_N * vehstate.Vel_N + vehstate.Vel_E * vehstate.Vel_E + vehstate.Vel_U * vehstate.Vel_U);
      
      
      vehstate.Yaw = NavBuffer[NavBufferIndex].thdg * DEG2RAD;
      vehstate.Pitch = NavBuffer[NavBufferIndex].pitch * DEG2RAD;
      vehstate.Roll =  NavBuffer[NavBufferIndex].roll * DEG2RAD;


      //****************************
      //TEMPORARY HACK FOR ALTITUDE
      vehstate.Altitude = 0;
      //***************************

	//******************** LOGGING HACK *********************
	if (logflag)
	  {
	    logfile.precision(10);
	    logfile<<imu_time<<'\t'<<imudata.dvx<<'\t'<<imudata.dvy<<'\t'<<imudata.dvz<<'\t'<<imudata.dtx<<'\t'<<imudata.dty<<'\t'<<imudata.dtz<<'\t'<<gps_local_time<<'\t'<<gps_valid<<'\t'<<New_GPS_data_flag<<'\t'<<gpsdata.data.lat<<'\t'<<gpsdata.data.lng<<'\t'<<gpsdata.data.vel_n<<'\t'<<gpsdata.data.vel_e<<'\t'<<gpsdata.data.vel_u<<'\t'<<imu_time<<'\t'<<vehstate.Northing<<'\t'<<vehstate.Easting<<'\t'<<vehstate.kf_lat<<'\t'<<vehstate.kf_lng<<'\t'<<vehstate.Altitude<<'\t'<<vehstate.Vel_N<<'\t'<<vehstate.Vel_E<<'\t'<<vehstate.Vel_U<<'\t'<<vehstate.Speed<<'\t'<<vehstate.Pitch<<'\t'<<vehstate.Roll<<'\t'<<vehstate.Yaw<<'\t'<<ExtGPSPosMeas_x<<'\t'<<ExtGPSPosMeas_y<<'\t'<<ExtGPSPosMeas_z<<'\t'<<ExtGPSVelMeas_x<<'\t'<<ExtGPSVelMeas_y<<'\t'<<ExtGPSVelMeas_z<<endl;
	  }



      vehstate.imudata = imudata;

      new_imu = 1;

      DGCNavRun();
      chn_write(); // write data to file if enabled 
}
  return NULL;
}

/* Read messages from the GPS unit and put them into a buffer */
void *gps_start(void *arg)
{
  char *gps_buffer;
  

  if ((gps_buffer = (char *) malloc(2000)) == NULL) {
    dbg_panic("GPS: memory allocation error\n");
    return NULL;
  }

  while (1) {
    if (gps_enabled > 0) {
      get_gps_msg(gps_buffer, 1000);
      switch (get_gps_msg_type(gps_buffer)) {
      case GPS_MSG_PVT:
	gpsdata.update_gps_data(gps_buffer);
	++gps_count;
	gps_valid = (gpsdata.data.nav_mode & NAV_VALID);

	// get gps heading for display
	gheading = atan2(gpsdata.data.vel_n, gpsdata.data.vel_e);

	//log data
	/* Save the time the data was taken */
	gettimeofday(&tv, NULL);
        gps_local_time = (double) (tv.tv_sec - start) + ((double) tv.tv_usec) / 1e6;

        if (New_GPS_data_flag == 0) {    // if KF needs new GPS data, write it over
	  gps_time = gps_local_time;
          gps_lat = gpsdata.data.lat*DEG2RAD;
          gps_lon = gpsdata.data.lng*DEG2RAD;
          gps_alt = gpsdata.data.altitude;
          gps_vn  = gpsdata.data.vel_n;
          gps_ve  = gpsdata.data.vel_e;
          gps_vu  = gpsdata.data.vel_u;
          New_GPS_data_flag = 1;
        }
   
	chn_data(17) = gps_local_time;
	chn_data(18) = gpsdata.data.lat;
	chn_data(19) = gpsdata.data.lng;
	chn_data(20) = gpsdata.data.vel_n;
	chn_data(21) = gpsdata.data.vel_e;
	chn_data(22) = gpsdata.data.vel_u;  

	// log here if imu is disabled, otherwise data will be written in IMU loop
	//	if (imu_enabled != 1)
	//  chn_write();

	vehstate.gpsdata = gpsdata.data;

	//new_gps = 1;  // set data updated flag for statefilter
	break;
    
      default:
	/* print_gps_msg(gps_buffer) */
	break;
      }
    }
  }
  return NULL;
}

// updates magnetometer data continuously
void *mag_start(void *arg)
{
  while(1){
    if ((mag_enabled > 0) && (new_mag == 0)){
    get_mag_data(magreading);
    mag_count++;

    //log data
    /* Save the time the data was taken */
    gettimeofday(&tv, NULL);
    /* chn_data(13) = (double) (tv.tv_sec - start) + ((double) tv.tv_usec) / 1e6;
    chn_data(14) = magreading.pitch;
    chn_data(15) = magreading.roll;
    chn_data(16) = magreading.heading; */

    if (New_Mag_data_flag == 0) {
      mag_hdg = magreading.heading*DEG2RAD;
      New_Mag_data_flag = 1;
    }
 
    // log here if imu and gps are disabled, otherwise logfile will be written in imu or gps loop
    //    if ((imu_enabled != 1) && (gps_enabled !=1))
    // chn_write();

    vehstate.magreading = magreading;

    //new_mag = 1; // set flag for statefilter
    }
    
  }
  return NULL;
}

// statefilter
// Handles integration of IMU data as well as merging of all state sensor data into the state struct
void *statefilter(void *arg)
{
  while(1){
  
    // if a new imu packet has been received, set the flag so that it won't be updated, process it, then reset flag
    if (new_imu == 1){
      new_imu = -1;
      state_good = 0;
      //      dbg_info("Propagating IMU... ");
      imu_update(N2B, imudata, vehstate);
      //dbg_info("IMU Propagated\n");
      //dbg_info("pitch: %5.2f roll: %5.2f yaw: %5.2f \n", vehstate.Pitch, vehstate.Roll, vehstate.Yaw);
      p = N2B.get_pitch();
      r = N2B.get_roll();
      y = N2B.get_heading();

	//log data
	/* Save the time the data was taken */
	gettimeofday(&tv, NULL);
	chn_data(17) = (double) (tv.tv_sec - start) + ((double) tv.tv_usec) / 1e6;
	chn_data(18) = vehstate.Northing;
	chn_data(19) = vehstate.Easting;
	chn_data(20) = vehstate.Altitude;
	chn_data(21) = vehstate.Vel_N;
	chn_data(22) = vehstate.Vel_E;
	chn_data(23) = vehstate.Vel_U;
	chn_data(24) = vehstate.Speed;
	chn_data(25) = vehstate.Pitch;
	chn_data(26) = vehstate.Roll;
	chn_data(27) = vehstate.Yaw;

	chn_write();

      new_imu = 0;
      state_good = 1;
    }

    // do same with gps data
    if (new_gps == 1){
      new_gps = -1;
      state_good = -1;
      //dbg_info("Updating GPS state... ");
      gps_update(gpsdata, vehstate, N2B);
      
      
	//log data here if IMU is not enabled, otherwise log in imu

      if (imu_enabled != 1){
	/* Save the time the data was taken */
	gettimeofday(&tv, NULL);
	chn_data(17) = (double) (tv.tv_sec - start) + ((double) tv.tv_usec) / 1e6;
	chn_data(18) = vehstate.Northing;
	chn_data(19) = vehstate.Easting;
	chn_data(20) = vehstate.Altitude;
	chn_data(21) = vehstate.Vel_N;
	chn_data(22) = vehstate.Vel_E;
	chn_data(23) = vehstate.Vel_U;
	chn_data(24) = vehstate.Speed;
	chn_data(25) = vehstate.Pitch;
	chn_data(26) = vehstate.Roll;
	chn_data(27) = vehstate.Yaw;
	chn_write();
      }
      //dbg_info("GPS State updated\n");
      new_gps = 0;
      state_good = 1;
    }

    // same thing with magnetometer data
    if (new_mag == 1){
      new_mag = -1;
      state_good = -1;
      //dbg_info("Updating Mag state... ");
      mag_update(magreading, vehstate, N2B);
      //dbg_info("Mag state updated\n");
      //dbg_info("pitch: %f roll: %f yaw: %f \n", vehstate.Pitch, vehstate.Roll, vehstate.Yaw);

      new_mag = 0;
      state_good = 1;
    }
  

  }

  return NULL;
}


/*
 * Callbacks to control operation of the program
 *
 */

/* Quit the dfan program */
int user_quit(long arg)
{
  int count = 0;
  if (logflag)
    logfile.close();

  if( mta_flag == 1)
    {
  /* Tell MTA to shut down */
  ForceKernelShutdown();

  /* Wait for 1 second to make sure it is shut down */
  while (ShuttingDown()) {
    if (count++ > 1000) break;
    usleep(1000);
  }

}
    /* Tell dd_loop to abort now */
    return DD_EXIT_LOOP;
}

/* Toggle capture mode */
int user_cap_toggle(long arg)
{
  return chn_capture_flag ? chn_capture_off() : chn_capture_on();
}
