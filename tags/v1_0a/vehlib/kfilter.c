/*----------------------------------------------------------------------------
--                                                                          --
--                        U N C L A S S I F I E D                           --
--                                                                          --
--       N O R T H R O P   G R U M M A N   P R O P R I E T A R Y            --
--                                                                          --
-- Copyright (c) 2002 Northrop Grumman Corp.                                --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------
--                                                                          --
--    The code contained herein is proprietary to Northrop Grumman Corp.    --
--    and is subject to any non-disclosure agreements and proprietary       --
--    information exchange agreements between Northrop Grumman and the      --
--    and the receiving party.  The code contained herein should be         --
--    considered technical data and is subject to all relevant export       --
--    control regulations.                                                  --
--                                                                          --
----------------------------------------------------------------------------*/

/* File: kfilter.c
 * Date: 12/29/03
 */

#include <math.h>
#include <stdio.h>
#include "Global.h"
/* #include "Gps.h"  */
#include "Imu.h"
#include "Nav.h"
#include "Leveler.h"
/*#include "Motion.h" */
#include "Kalman.h"
#include "Kalmanp.h"
#include "Kalmano.h" 
#include "kfilter.h"
#include "Modectl.h"
#include "Mathlib.h"

extern double nmlat,nmlon,nfaltf;
extern double roll,pitch,yaw;
extern double nmalpha;
extern double dt_imu, dt_lvl, dt_navf, dt_navm, dt_navs;

extern int en_gps_pos_upd;
extern int en_gps_vel_upd;

double imu_dvx,imu_dvy,imu_dvz; // Delta-v's m/s (should be)
double imu_dtx,imu_dty,imu_dtz; // Delta-Thetas,should be rads

double DGC_gps_lat, DGC_gps_lon, DGC_gps_alt; // for initialization
double DGC_mag_hdg;

double gps_lat, gps_lon, gps_alt;
double gps_vn, gps_ve, gps_vu;
double gps_time;
double imu_time;
double mag_hdg;
 
double GPS_xy_kr, GPS_h_kr, GPS_v_kr;

extern double tot_kabx_i,tot_kaby_i,tot_kabz_i;
extern double tot_kasfx_i,tot_kasfy_i,tot_kasfz_i;
extern double tot_kgbx_i,tot_kgby_i,tot_kgbz_i,tot_kgsfx_i;

int New_GPS_data_flag;   // flag set when new GPS data comes in
int New_Mag_data_flag;

extern T3dimVector ins_gps_lvr_B;

int xdue500a;                        // 500 Hz (A) tasks due time
int xdue500b;                        // 500 Hz (B) tasks due time
int xdue500c;                        // 500 Hz (C) tasks due time
int xdue100;                         // 100 Hz tasks due time
int xdue10;                          // 10  Hz tasks due time
int xdue1;                           // 1 Hz tasks due time
double imu_update_rate;                 // Rate of IMU data output, Hz
int xImuSyncCnt;            // executive sync cnt (0:1:199)
int xNavfSyncCnt;           // executive sync cnt (0:2:198)

void DGCNavInit(void)
{

     dt_imu = 1.0 / IMU_HZ;  
     dt_lvl = 1.0 * XREP500 / IMU_HZ;
     dt_navf = 1.0 * XREP500 / IMU_HZ;
     dt_navm = 1.0 * XREP100 / IMU_HZ;
     dt_navs = 1.0 * XREP1 / IMU_HZ;
 
     ins_gps_lvr_B.V1 = -3.0;      // lever arm from ins to GPS in body frame.
     ins_gps_lvr_B.V2 =  0.3;
     ins_gps_lvr_B.V3 =  0.0;
 
     // measurement residuals

     GPS_xy_kr = 9.0;  
     GPS_h_kr = fsquare(3.0/6378.0/1000);   // 3.0m divided by earth radius.
     GPS_v_kr = 0.04;

     // initial instrument errors
     tot_kabx_i = 0.0;
     tot_kaby_i = 0.0;
     tot_kabz_i = 0.0;
     tot_kasfx_i = 0.0;
     tot_kasfy_i = 0.0;
     tot_kasfz_i = 0.0;
     tot_kgbx_i = 0.0;
     tot_kgby_i = 0.0;
     tot_kgbz_i = 0.0;
     tot_kgsfx_i = 0.0;

    xdue500a   = XOFFSET500A;           // delta velocity cycle
    xdue500b   = XOFFSET500B;           // delta theta cycle
    xdue500c   = XOFFSET500C;           // apply kalman corrections to system
    xdue100    = XOFFSET100;
    xdue10     = XOFFSET10;
    xdue1      = XOFFSET1;

/* Initialize INS/GPS *******************************************************/

  imuInit();                            // initialize imu
  navInit();                            // initialize nav
  imuInitQuaternion();                  // initialize ON quaternion
  levelerInit();                        // initialize leveler
  modeCtlInit();                        // initialize mode control
  //  motionDetectInit();                   // initialize motion detector
  kalmanInit();                         // initialize kalman filter

  New_GPS_data_flag = 0;
  New_Mag_data_flag = 0;

  }


void DGCNavRun(void)
{   
    imuDataSummation();                 // Sum IMU data from 1 KHz to 500 Hz

    if (xdue500a == 0) {            // Delta velocity cycle
      xdue500a = XREP500;
         if (xdue1 == 0) { 
        if (modeCtlRestart() == TRUE) { // reinitialize executive ?
          DGCNavInit();                    // y.
	  } 
	} 
      imuCompAccelData();               // Latch 500 Hz DV, Kalman compensation
      imuDeltaVelTransform();           // Transform dv from body to nav frame
      navVelocityInt();                 // Integrate nav frame velocities
      perform_leveling();               // Perform leveling
      generate_a_mtx();                 // Generate Kalman A matrix data 

    }
   

    if (xdue500b == 0) {            // Delta theta cycle

      xdue500b = XREP500;
      imuCompGyroData();                // Latch 500 Hz delta theta, Kalman com
      navCompTiltCorrs();               // Compute tilt corrections
      imuQuaternionInt();               // Quaternion integration
      imuQuatRotate();                  // Compute body to geodetic quaternion
      imuQuatBuf();                     // Buffer body to geodetic quaternion
      imuDirCosines();                  // Compute the direction cosines 
      imuEulerAngles();                 // Compute euler angles
      koBufferNavData();
      koGPSDataCheck(); 
     }


    if (xdue100 == 0) {             // 100 Hz navigation tasks
      xdue100 = XREP100;
      navMaintEarthToNavDC();           // Update earth to nav DC
      navCompEarthRates();              // Compute earth rates
      navCompInvEarthRad();             // Compute inverse earth radius terms
      navCompCoriolis();                // Compute Coriolis accels    
      kApplySystemCorr();               // Apply kalman corrections  
     }

    if (xdue10 == 0) {
      xdue10 = XREP10;
      navCompGravity();                 // compute gravity
      modeCtl();                        // mode control
    }


    if (xdue1 == 0) {               // N.000
      xdue1 = XREP1;
      kControlKalmanFilter();
       }

    /* Decrement all counters */

    xdue500a -= 1;
    xdue500b -= 1;
    xdue500c -= 1;
    xdue100  -= 1;
    xdue10   -= 1;
    xdue1    -= 1;
}
