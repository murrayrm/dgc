/* VDrive.hh - header file for communicating with vdrive MTA interface
 *
 * Richard Murray
 * 3 January 2004
 * 
 * This file defines the format for motion command that is sent
 * to Vdrive.
 *
 */

#ifndef VDRIVE_HH
#define VDRIVE_HH

#include "MTA/Misc/Time/Timeval.hh"
#include "MTA/Misc/Mail/Mail.hh"

struct VDrive_CmdMotionMsg
{
  Timeval Timestamp;		// time stamp when data was sent
  double velocity_cmd;		// desired velocity
  double steer_cmd;		// commanded steering angle
};

// enumerate all module messages that could be received
namespace VDriveMessages {
  enum {
    CmdMotion,			// Command a given motion
    NumMessages			// Placeholder (not used)
  };
};

#endif
