#ifndef __STEREOSOURCE_H__
#define __STEREOSOURCE_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libraw1394/raw1394.h>
#include <libdc1394/dc1394_control.h>

#include <opencv/cv.h>
#include <opencv/cvaux.h>
#include <opencv/highgui.h>

typedef struct{
  unsigned char* left;
  unsigned char* right;
} stereoPair;


enum {
  stereoSource_OK,
  stereoSource_UNKNOWN_SOURCE,
  stereoSource_NO_FILE,
  stereoSource_UNKNOWN_ERROR,
  stereoSource_NOT_IMPLEMENTED,

  stereoSource_COUNT
};


enum {
  SOURCE_UNASSIGNED,
  SOURCE_CAMERAS,
  SOURCE_FILES,

  NUM_SOURCE_TYPES
};


enum {
  LEFT_CAM,
  RIGHT_CAM,

  MAX_CAMERAS
};

class stereoSource {
public:
  stereoSource();
  ~stereoSource();

  int init(int verboseLevel, char *camIDFilename, char *logFilename = "", char *logFileType = "");
  int init(int verboseLevel, int width, int height, char *baseFilename, char *baseFileType, int start = 1);

  int stop();

  int grab();
  int grab(char *baseFilename, char *baseFileType, int num);
  
  int save();
  int save(char *baseFilename, char *baseFileType, int num);

  unsigned char* left();
  unsigned char* right();
  stereoPair pair();

  int currentSourceType();
  int pairIndex();

  int width();
  int height();

  int resetPairIndex();

  int startExposureControl(CvRect imageROI);
  int stopExposureControl();

  int setFeatureEnableAutoShutter(int status);
  int setFeatureEnableAutoExposure(int status);
  int setFeatureEnableAutoGain(int status);

  int setFeatureValShutter(double scale);
  int setFeatureValExposure(double scale);
  int setFeatureValGain(double scale);

  double getFeatureValShutter();
  double getFeatureValExposure();
  double getFeatureValGain();

  double getAvgBrightness(int camera, int x_start, int x_stop, int y_start, int y_stop);

  double getAvgROIBrightness();

  int getExposureControlStatus() {return _exposureControlStatus;};

private:
  dc1394_cameracapture cameras[MAX_CAMERAS];
  raw1394handle_t handle;
  int node2cam[MAX_CAMERAS];

  IplImage* _images[MAX_CAMERAS];
  IplImage* _colorImages[MAX_CAMERAS];
  IplImage* _temp[MAX_CAMERAS];
  CvSize pairSize;

  CvRect _imageROI;

  char _currentFilename[256];
  char _currentFileType[10];

  int _currentSourceType;
  int _pairIndex;

  int _verboseLevel;

  int _exposureControlStatus;
};

#endif  //__STEREOSOURCE_H__
