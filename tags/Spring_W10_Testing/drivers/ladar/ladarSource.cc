#include "ladarSource.hh"

#define PFIXME printf("%s [%d] fixme\n", __FILE__, __LINE__);

double ladarSource::getCenterDist() {
  #warning JEREMY G:  This( ladarSource::getCenterDist()) needs to be updated for non-sick units if you use it in other spots.  This function now returns -1.0 so that it can be used with gazebo (easy fix, but not sure what it is needed for at this point)

  return -1.0;
  //  return _sickLadar.Scan[101]*_ladarUnits;
}


ladarSource::ladarSource() { 
  _currentSourceType = SOURCE_UNASSIGNED;
  _scanIndex = 0;
  //_currentFilename = NULL;
  memset(_currentFilename, 0, 256);
  _loggingEnabled = 0;
  _loggingPaused = 0;
  _loggingOpts.logRaw = 1;
  _loggingOpts.logState = 1;
  _loggingOpts.logRanges = 0;
  _loggingOpts.logRelative = 0;
  _loggingOpts.logUTM = 0;

  sprintf(_errorMessage, "None.");
}


ladarSource::~ladarSource() {
  stopLogging();
}


int ladarSource::init(int sourceType, char *ladarIDFilename, char *logFilename, int loggingEnabled, int optladar) {
  char filenameHeader[256];
  char filenameRaw[256];
  char filenameState[256];

  char tempBuffer[2048];

  _currentSourceType = sourceType;
  int returnStatus = OK;
  if(strcmp(logFilename, "")!=0) {
    strcpy(_currentFilename, logFilename);
  }
  strcpy(_configFilename, ladarIDFilename);
  PFIXME;
  parseConfigFile(ladarIDFilename);
  

  switch(sourceType) {
  case SOURCE_LADAR:
    switch(_currentLadarType) {
    case TYPE_SICK_LMS_221:
      while(_sickLadar.Setup(_serialPort, _scanAngle, _resolution)!=0) {
	sleep(1);
	sprintf(_errorMessage, "LADAR not responding - retrying!");
      }      
      break;
    default:
      PFIXME
      break;
    }
    
    break;

  case SOURCE_FILES:
    if(strcmp(_currentFilename, "")==0) {
      PFIXME;
      return UNKNOWN_ERROR;
    }
    sprintf(filenameHeader, "%s%s", _currentFilename, ".header");
    sprintf(filenameRaw, "%s%s", _currentFilename, ".raw");
    sprintf(filenameState, "%s%s", _currentFilename, ".state");
    struct stat buffer;
    if(!stat(filenameHeader, &buffer)) {
      parseConfigFile(filenameHeader);
      _logFileRaw = fopen(filenameRaw, "r");
      fgets(tempBuffer, 2048, _logFileRaw);
      _logFileState = fopen(filenameState, "r");
      fgets(tempBuffer, 2048, _logFileRaw);
    } else {
      PFIXME;
      return UNKNOWN_ERROR;      
    }
    break;

  case SOURCE_SIM:
#ifdef _PLAYER_
    // initialize any player stuff.
    _gazeboLadar.init(optladar);
    //initPlayer();
#else
    PFIXME;
#endif
    break;
  default:
    cerr << __FILE__ << " [" << __LINE__ << "]: Unknown source type, couldn't open!" << endl;
    return UNKNOWN_SOURCE;
    break;
  }

  if(loggingEnabled) {
    startLogging(_loggingOpts);
  }

  return UNKNOWN_ERROR;
}


int ladarSource::stop() {
  return UNKNOWN_ERROR;
}

unsigned long long ladarSource::scan() {
  int tempInt;
  int latestScanTimestamp;


  int numGoodPoints = 0;
  int status = 0;
  int numScanPoints;

  switch(_currentSourceType) {
  case SOURCE_LADAR:
    switch(_currentLadarType) {
    case TYPE_SICK_LMS_221:
      numScanPoints = _sickLadar.getNumDataPoints();
      status = _sickLadar.UpdateScan();
      DGCgettime(_latestScanTimestamp);
      if(_logFileRaw!=NULL) {
	fprintf(_logFileRaw, "%llu ", _latestScanTimestamp);
      }
      if(_logFileRaw!=NULL) {
	fprintf(_logFileRaw, "%d ", numScanPoints);
      }
      _latestScanTimestamp-=142857;

      for(int i=0; i< numScanPoints; i++) {
	if(_logFileRaw!=NULL) {
	  fprintf(_logFileRaw, "%d ", _sickLadar.Scan[i]);
	}
	if(_sickLadar.isDataError(status, _sickLadar.Scan[i]) == LADAR_DATA_BAD || _sickLadar.Scan[i]>=8191) {
	  //do nothing - bad scan
	} else {
	  _ranges[numGoodPoints] = _sickLadar.Scan[i] * _ladarUnits;
	  _angles[numGoodPoints] = _scanAngleOffset + (i*_resolution*M_PI)/180.0;
	  numGoodPoints++;
	}
      }
      if(_logFileRaw!=NULL) {
	fprintf(_logFileRaw, "\n");
      }
      _numPoints = numGoodPoints;
      break;
    default:
      break;
    }
    break;
  case SOURCE_FILES:
    if(!feof(_logFileState)) {
      fscanf(_logFileState, "%llu %llu %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
	     latestScanTimestamp,
	    &_currentState.Timestamp, 
	    &_currentState.Northing, &_currentState.Easting, &_currentState.Altitude,
	    &_currentState.Vel_N, &_currentState.Vel_E, &_currentState.Vel_D,
	    &_currentState.Acc_N, &_currentState.Acc_E, &_currentState.Acc_D,
	    &_currentState.Roll, &_currentState.Pitch, &_currentState.Yaw,
	    &_currentState.RollRate, &_currentState.PitchRate, &_currentState.YawRate,
	    &_currentState.RollAcc, &_currentState.PitchAcc, &_currentState.YawAcc);

    } else {
      //PFIXME;
      return UNKNOWN_ERROR;
    }

    if(!feof(_logFileRaw)) {
      fscanf(_logFileRaw, "%llu ", &_latestScanTimestamp);
      fscanf(_logFileRaw, "%d ", &numScanPoints);
      for(int i=0; i<numScanPoints; i++) {
	fscanf(_logFileRaw, "%d ", &tempInt);
#warning "magic number here"
	if(tempInt>=0x1FF7) {
	  //bad scan - do nothing
	} else {
	  _ranges[numGoodPoints] = tempInt * _ladarUnits;
	  _angles[numGoodPoints] = _scanAngleOffset + (i*_resolution*M_PI)/180.0;
	  numGoodPoints++;
	}
	fscanf(_logFileRaw, "\n");
      }
    } else {
      //PFIXME;
      return UNKNOWN_ERROR;
    }
    _numPoints = numGoodPoints;

    break;
  case SOURCE_SIM:
#ifdef _PLAYER_
    // Read from player
    // after this we will have a roofPlayerLadar.scanRanges[] array
    _gazeboLadar.grab();
    numScanPoints = _gazeboLadar.numScanPoints;
    DGCgettime(_latestScanTimestamp);
    if(_logFileRaw!=NULL) {
      fprintf(_logFileRaw, "%llu ", _latestScanTimestamp);
    }
    
    for(int i=0; i< numScanPoints; i++) {
      if(_logFileRaw!=NULL) {
	fprintf(_logFileRaw, "%d ", _gazeboLadar.scanRanges[i]/100.0);
      }
      if(_gazeboLadar.scanRanges[i]>=81.91) {
	//do nothing - bad scan
      } else {
	_ranges[numGoodPoints] = _gazeboLadar.scanRanges[i];
	_angles[numGoodPoints] = (i*_resolution*2*M_PI)/180.0;//_scanAngleOffset + (i*_resolution*M_PI)/180.0;//this is a hack
	numGoodPoints++;
      }
    }
    if(_logFileRaw!=NULL) {
      fprintf(_logFileRaw, "\n");
    }
    _numPoints = numGoodPoints;
#else
    sprintf(_errorMessage, "Gazebo support not compiled!");
    fprintf(stderr, "%s [%d]: Gazebo support not compiled - please define the PLAYER environment variable and remake.\n", __FILE__, __LINE__);
    exit(1);
#endif
    break;
 default:
    PFIXME
    break;
  }
  
  _scanIndex++;

  return _latestScanTimestamp;
}


int ladarSource::scan(char *logFilename, int num) {
  return UNKNOWN_ERROR;
}


int ladarSource::startLogging(ladarSourceLoggingOptions loggingOpts, char *baseFilename) {
  char filenameHeader[256];
  char filenameRaw[256];
  char filenameState[256];
  char filenameRanges[256];
  char filenameRelative[256];
  char filenameUTM[256];
  if(baseFilename!=NULL && strcmp(baseFilename, "")!=0) {
    strcpy(_currentFilename, baseFilename);
  }

  if(_loggingEnabled==1 || _currentSourceType==SOURCE_FILES) {
    PFIXME;
    return UNKNOWN_ERROR;
  } else {
    _loggingOpts = loggingOpts;
      
    //Check to see if the filename exists
    if(_currentFilename==NULL || strcmp(_currentFilename, "")==0) {
      //the filename doesn't exist - make it
      time_t t = time(NULL);
      tm *local;
      local = localtime(&t);
      char datestamp[255];

      sprintf(datestamp, "%02d%02d%04d_%02d%02d%02d",
	      local->tm_mon+1, local->tm_mday, local->tm_year+1900,
	      local->tm_hour, local->tm_min, local->tm_sec);
      
      sprintf(_currentFilename, "logs/ladar_%s", datestamp);
    }
    
    sprintf(filenameHeader, "%s%s", _currentFilename, ".header");

    struct stat buffer;
    if(!stat(filenameHeader, &buffer)) {
      PFIXME;
      _loggingEnabled = 0;
      return UNKNOWN_ERROR;
    } else {
      sprintf(filenameRaw, "%s%s", _currentFilename, ".raw");
      sprintf(filenameState, "%s%s", _currentFilename, ".state");
      sprintf(filenameRanges, "%s%s", _currentFilename, ".ranges");
      sprintf(filenameRelative, "%s%s", _currentFilename, ".relative");
      sprintf(filenameUTM, "%s%s", _currentFilename, "UTM");
      
      FILE* logFileHeader = NULL;
      logFileHeader = fopen(filenameHeader, "w");
      if(logFileHeader==NULL) {
	fprintf(stderr, "%s [%d]: Error opening file '%s', quitting...\n", __FILE__, __LINE__, filenameHeader);
	exit(1);
      }

      fprintf(logFileHeader, "%% Header for ladar logs\n");
      fprintf(logFileHeader, "config_file: %s\n", _configFilename);
      fprintf(logFileHeader, "x: %lf\n", _ladarPosOffset.X);
      fprintf(logFileHeader, "y: %lf\n", _ladarPosOffset.Y);
      fprintf(logFileHeader, "z: %lf\n", _ladarPosOffset.Z);
      fprintf(logFileHeader, "pitch: %lf\n", _ladarAngleOffset.P);
      fprintf(logFileHeader, "roll: %lf\n", _ladarAngleOffset.R);
      fprintf(logFileHeader, "yaw: %lf\n", _ladarAngleOffset.Y);
      fprintf(logFileHeader, "type: %s\n", getLadarType(_currentLadarType));
      fprintf(logFileHeader, "scan_angle: %d\n", _scanAngle);
      fprintf(logFileHeader, "resolution: %lf\n", _resolution);
      fprintf(logFileHeader, "units: %lf\n", _ladarUnits);
      fprintf(logFileHeader, "tty: %s\n", _serialPort);
      fclose(logFileHeader);
      
      if(_loggingOpts.logRaw) {
	_logFileRaw = fopen(filenameRaw, "w");
	if(_logFileRaw==NULL) {
	  fprintf(stderr, "%s [%d]: Error opening file '%s', quitting...\n", __FILE__, __LINE__, filenameRaw);
	  exit(1);
	}
	fprintf(_logFileRaw, "%% Time [usec] | Scan Points\n");
      }

      if(_loggingOpts.logState) {
	_logFileState = fopen(filenameState, "w");
	if(_logFileState==NULL) {
	  fprintf(stderr, "%s [%d]: Error opening file '%s', quitting...\n", __FILE__, __LINE__, filenameState);
	  exit(1);
	}
	fprintf(_logFileState, "%% LADARTime [usec] | StateTime[usec] | Northing[m] | Easting [m] Altitude[m]");
	fprintf(_logFileState, " | Vel_N[m/s] | Vel_E[m/s] | Vel_U[m/s]" );
	fprintf(_logFileState, " | Acc_N[m/s^2] | Acc_E[m/s^2] | Acc_U[m/s^2]" );
	fprintf(_logFileState, " | Roll[rad] | Pitch[rad] | Yaw[rad]" );
	fprintf(_logFileState, " | RollRate[rad/s] | PitchRate[rad/s] | YawRate[rad/s]");
	fprintf(_logFileState, " | RollAcc[rad/s^2] | PitchAcc[rad/s^2] | YawAcc[rad/s^2]");
	fprintf(_logFileState, "\n");     
      }

      _loggingEnabled = 1;    
      return OK;
    }    
  }
    return UNKNOWN_ERROR;
}


int ladarSource::stopLogging() {
  if(_logFileRaw!=NULL) fclose(_logFileRaw);
  if(_logFileState!=NULL) fclose(_logFileState);
  if(_logFileRanges!=NULL) fclose(_logFileRanges);
  if(_logFileRelative!=NULL) fclose(_logFileRelative);
  if(_logFileUTM!=NULL) fclose(_logFileUTM);

  _loggingEnabled = 0;
  _loggingPaused = 0;

  return OK;
}


int ladarSource::resetScanIndex() {
  _scanIndex = 0;
  return UNKNOWN_ERROR;
}


int ladarSource::parseConfigFile(char* configFilename) {
  //Get info about our ladar from the ID file
  FILE* configFile = NULL;
  char fieldBuffer[512];
  char fieldName[256];
  char fieldValue[256];

  configFile = fopen(configFilename, "r");
  if(configFile != NULL) {
    while(!feof(configFile)) {
      fgets(fieldBuffer, 512, configFile);
      if(strncmp(fieldBuffer, "%", 1)!=0) {
	sscanf(fieldBuffer, "%s %s\n", fieldName, fieldValue);
	if(strcmp(fieldName, "type:")==0) {
	  _currentLadarType = getLadarType(fieldValue);
	} else if(strcmp(fieldName, "scan_angle:")==0) {
	  _scanAngle = atoi(fieldValue);
	} else if(strcmp(fieldName, "resolution:")==0) {
	  _resolution = atof(fieldValue);
	} else if(strcmp(fieldName, "tty:")==0) {
	  strcpy(_serialPort, fieldValue);
	} else if(strcmp(fieldName, "units:")==0) {
	  _ladarUnits = atof(fieldValue);
	} else if(strcmp(fieldName, "x:")==0) {
	  _ladarPosOffset.X = atof(fieldValue);
	} else if(strcmp(fieldName, "y:")==0) {
	  _ladarPosOffset.Y = atof(fieldValue);
	} else if(strcmp(fieldName, "z:")==0) {
	  _ladarPosOffset.Z = atof(fieldValue);
	} else if(strcmp(fieldName, "roll:")==0) {
	  _ladarAngleOffset.R = atof(fieldValue);
	} else if(strcmp(fieldName, "pitch:")==0) {
	  _ladarAngleOffset.P = atof(fieldValue);
	} else if(strcmp(fieldName, "yaw:")==0) {
	  _ladarAngleOffset.Y = atof(fieldValue);
	} else {
	  PFIXME;
	  printf("Unknown %s %s\n", fieldName, fieldValue);
	}
      }
    }
    _scanAngleOffset = ((180.0 - _scanAngle)/2.0) * M_PI/180.0;
    setLadarFrame(_ladarPosOffset, _ladarAngleOffset);

    return OK;
  } else {
#warning fixme
    return UNKNOWN_ERROR;
  }
      
  return UNKNOWN_ERROR;
}


int ladarSource::setLadarFrame(XYZcoord ladarPosOffset, RPYangle ladarAngleOffset) {
  _ladarFrame.initFrames(ladarPosOffset, 
			 ladarAngleOffset.P,
			 ladarAngleOffset.R,
			 ladarAngleOffset.Y);

  return OK;
}


int ladarSource::updateState(VehicleState state) {
  _currentState = state;
  
  _ladarFrame.updateState(XYZcoord(_currentState.Northing, _currentState.Easting, _currentState.Altitude), 
			  _currentState.Pitch, _currentState.Roll, _currentState.Yaw);
  
  if(_logFileState!=NULL) {
    fprintf(_logFileState, "%llu %llu %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
	    _latestScanTimestamp,
	    _currentState.Timestamp, 
	    _currentState.Northing, _currentState.Easting, _currentState.Altitude,
	    _currentState.Vel_N, _currentState.Vel_E, _currentState.Vel_D,
	    _currentState.Acc_N, _currentState.Acc_E, _currentState.Acc_D,
	    _currentState.Roll, _currentState.Pitch, _currentState.Yaw,
	    _currentState.RollRate, _currentState.PitchRate, _currentState.YawRate,
	    _currentState.RollAcc, _currentState.PitchAcc, _currentState.YawAcc);
    fflush(_logFileState);
  }

  return OK;
}

double ladarSource::range(int i) {
  return _ranges[i];
}


double ladarSource::angle(int i) {
  return _angles[i];
}


XYZcoord ladarSource::ladarPoint(int i) {
  //printf("%d x: %f y: %f angle: %f range: %f\n", i,_ranges[i]*sin(_angles[i]),_ranges[i]*cos(_angles[i]), _angles[i], _ranges[i]);
  return XYZcoord(_ranges[i]*sin(_angles[i]), _ranges[i]*cos(_angles[i]), 0.0);
}


XYZcoord ladarSource::UTMPoint(int i) {
  return _ladarFrame.transformS2N(ladarPoint(i));
}



int ladarSource::toggleLogging() {
  _loggingPaused = !_loggingPaused;

  return !_loggingPaused;
}


int ladarSource::getLadarType(char* ladarString) {
  if(strcmp(ladarString, "SICK_LMS_221")==0) {
    return TYPE_SICK_LMS_221;
  } else {
#warning fixme
    fprintf(stderr, "%s [%d]: FIXME %s\n", __FILE__, __LINE__, ladarString);
    return TYPE_UNKNOWN;
  }
  
  return TYPE_UNKNOWN;
}


char* ladarSource::getLadarType(int ladarInt) {
  switch(ladarInt) {
  case TYPE_SICK_LMS_221:
    return "SICK_LMS_221";
    break;
  default:
    return NULL;
    break;
  }

  return NULL;
}
