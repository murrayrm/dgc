/* Ryan Farmer
 * Riegl LMS-Q120 Driver
 * rieglClient.c
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/ioctl.h>

#define SUCCESS 0
#define ERROR   1

#define MAX_MSG_LEN 256
#define INCOMING_CON_QUEUE 10


int main(int argc, char *argv[])
{
  /* Declarations */
  struct hostent *serverHostInfo;

  struct sockaddr_in localAddr, serverAddr;

  int servPort;        // User specified server port (> 1024 as user)
  int listenerSocket;
  int i, tempInt;
  int recdBytes;
  int maxfd;
  char buffer[MAX_MSG_LEN];
  fd_set master;
  fd_set tempMaster;

  /* Read command line parameters */
  if (argc != 3)
  {
      // 192.168.0.234 - port 20001 is data port
      printf("Usage:  %s <Server hostname> <Server port>\n", argv[0]);
      return ERROR;
  }

  servPort = atoi(argv[2]);
  
  if (servPort == 0)
  {
      printf("Usage:  %s <Server hostname> <Server port>\n", argv[0]);
      exit(ERROR);
  }

  printf("Attempting to connect to server. . . .\n");

  serverHostInfo = gethostbyname(argv[1]);

  if (serverHostInfo == NULL)
  {
    perror("Error in host name lookup");
    exit(ERROR);
  }


  serverAddr.sin_family = serverHostInfo->h_addrtype;
  memcpy((char *) &serverAddr.sin_addr.s_addr, serverHostInfo->h_addr_list[0], serverHostInfo->h_length);
  serverAddr.sin_port = htons(servPort);

  // Create socket
  listenerSocket = socket(AF_INET, SOCK_STREAM, 0);
  
  if (listenerSocket < 0)
  {
    perror("Cannot open socket");
    exit(ERROR);
  }

  // Bind any port number
  localAddr.sin_family = AF_INET;
  localAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  localAddr.sin_port = htons(0);

  memset(&(localAddr.sin_zero), '\0', 8);

  tempInt = bind(listenerSocket, (struct sockaddr *) &localAddr,
		 sizeof(localAddr));

  if (tempInt < 0)
  {
    perror("Cannot bind local address to port");
    exit(ERROR);
  }

  // connect to server
  tempInt = connect(listenerSocket, (struct sockaddr *) &serverAddr,
		    sizeof(serverAddr));

  if (tempInt < 0)
  {
    perror("Cannot connect");
    exit(ERROR);
  }

  printf("Successful connection established\n");

  // prepare fd lists for use with select
  FD_ZERO(&master);
  FD_ZERO(&tempMaster);

  maxfd = listenerSocket;

  // we only care about two fds:  --now one
//  FD_SET(0, &master);  // Std in input
  FD_SET(listenerSocket, &master);  // server messages

  // send msg
  tempInt = send(listenerSocket, "GET /\n", 6, 0);


  while (1)
  {
    tempMaster = master;
    // wait for data
    recdBytes = select(maxfd + 1, &tempMaster, NULL, NULL, NULL);

    if (recdBytes < 0)
    {
      perror("select");
      exit(ERROR);
    }

    if (recdBytes == 0)
    {
      // time out occurred--this will not happen when the 5th arg is null...
    }

    /**** STDIN Code
    if (FD_ISSET(0, &tempMaster))
    {
      // Match from standard in - send to server
      // read in length from stdin
      ioctl(0, FIONREAD, &tempInt);
      // buffer over flow protection - will send the rest of it
      //  since select will allow the loop to run until no data
     if (tempInt > sizeof(buffer) - 1)
	tempInt = sizeof(buffer) - 1;

      tempInt = read(0, buffer, tempInt);
      // the client's receive function should add the null char to the
      // string--does not need to be sent.  this is for further use on
      // this side (debug, output, etc)
      buffer[tempInt] = '\0';

      tempInt = send(listenerSocket, buffer, tempInt, 0);
      if (tempInt < 0)
	perror("error - send");

    }

    else if (FD_ISSET...) stuff here

    **/
    if (FD_ISSET(listenerSocket, &tempMaster))
    {
      // received information from the server - one less element for null char
      recdBytes = recv(listenerSocket, buffer, sizeof(buffer) - 1, 0);
      if (recdBytes == 0)
      {
	// connection was closed by server
	close(listenerSocket);
	printf("Disconnected (timed out or connection lost).  Shutting down.\n");
	exit(ERROR);
      }
      else if (recdBytes < 0)
      {
	perror("Error - recv");
	exit(ERROR);
      }
      else
      {


	// add the null char for output to screen
	buffer[recdBytes + 1] = '\0';

	printf("%s", buffer);  // output to the screen
      }
    }
  }

  return 0;  // shouldn't get here . . . compiler likes
}
