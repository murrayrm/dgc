#ifndef LADARFEEDERDATUM_HH
#define LADARFEEDERDATUM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "Constants.h" //vehicle constants- serial ports, ladar consts,etc

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// Definition of state messages
#include "vehlib/VState.hh"
// Platform-independent state struct
#include "VehicleState.hh"

using namespace std;

// All data that needs to be shared by common threads
struct LadarFinderDatum 
{
  /// log_tstamp is the last time stamp that was read in from the log files
  /// (the timestamps should be synchronized between state and scan logs
  double log_tstamp;
  
  VehicleState SS; // the state space struct
};


#endif
