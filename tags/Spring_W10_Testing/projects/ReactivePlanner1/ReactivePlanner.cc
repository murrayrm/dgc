#include "ReactivePlanner.hh"

template <class T>
bool PlanReactively(CTraj path, int numpoints, double deltadist, CMap &costmap, int layernum, T maxcost)
{
  double VehLength = VEHICLE_LENGTH;
  double VehWidth = VEHICLE_WIDTH;
  bool obstacle_present = 0;
  double northing[2], easting[2], yaw, northingshift = 0, northnorthing, southnorthing;
  yaw = arctan(path.getNorthingDiff(0,1)/path.getEastingDiff(0,1));
  T cost = GetAreaCost<T>(path.getNorthing(0), path.getEasting(0), yaw, costmap, VEHICLE_LENGTH, VEHICLE_WIDTH);
  T northcost, southcost;
  for(int i=1; i<numpoints; i++)
    {
      yaw = arctan(path.getNorthingDiff(i,1)/path.getEastingDiff(i,1));
      cost = GetAreaCost<T>(path.getNorthing(i), path.getEasting(i), yaw, costmap, VEHICLE_LENGTH, VEHICLE_WIDTH, layernum);
      if(cost > maxcost)
        {
          obstacle_present = 1;
          northing[0] = path.getNorthing(i);
          northing[1] = path.getNorthingDiff(i,1);
          easting[0] = path.getEasting(i);
          easting[1] = path.getEastingDiff(i,1);
          northnorthing = path.getNorthing(i);
          southnorthing = path.getNorthing(i);
          northingshift = 0;
          while(cost > maxcost)
            {
              northnorthing+=deltadist;
              southnorthing-=deltadist;
              northcost = GetAreaCost<T>(northnorthing, easting[0], yaw, costmap, VEHICLE_LENGTH, VEHICLE_WIDTH, layernum);
              southcost = GetAreaCost<T>(southnorthing, easting[0], yaw, costmap, VEHICLE_LENGTH, VEHICLE_WIDTH, layernum);
              if(southcost > northcost)
                {
                  northing[0] = southnorthing;
                  cost = southcost;
                }
              else
                {
                  northing[0] = northnorthing;
                  cost = northcost;
                }
            }
          StretchPath(path, northing, i);
        }
    }
  return obstacle_present;
}
