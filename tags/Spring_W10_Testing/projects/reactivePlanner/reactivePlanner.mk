REACTIVEPLANNER_PATH = $(DGC)/projects/reactivePlanner

REACTIVEPLANNER_DEPEND_LIBS = \
	$(SKYNETLIB) \
	$(TRAJLIB) \
	$(RDDFLIB) \
	$(GGISLIB) \
	$(CMAPLIB) \
	$(CMAPPLIB) \
	$(SPARROWLIB) \
	$(CCPAINTLIB) \
	$(MODULEHELPERSLIB) \
	$(LOGGERLIB)

REACTIVEPLANNER_DEPEND_SOURCES = \
	$(REACTIVEPLANNER_PATH)/reactivePlanner.cc \
	$(REACTIVEPLANNER_PATH)/reactivePlanner.hh \
	$(REACTIVEPLANNER_PATH)/reactiveModule.cc \
	$(REACTIVEPLANNER_PATH)/reactiveModule.hh \
	$(REACTIVEPLANNER_PATH)/reactiveMain.cc \
	$(REACTIVEPLANNER_PATH)/test.cc \
	$(REACTIVEPLANNER_PATH)/reactivePlanner.mk \
	$(REACTIVEPLANNER_PATH)/Makefile

REACTIVEPLANNER_DEPEND = $(REACTIVEPLANNER_DEPEND_LIBS) $(REACTIVEPLANNER_DEPEND_SOURCES)
