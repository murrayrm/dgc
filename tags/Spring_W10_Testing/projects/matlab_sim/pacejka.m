function Fy = pacejka(a)
% Pacejka's Magic Formula for determining tire forces
% numbers courtesy of STI, for bob
D = 10675;
C = 1.1344;
B = 7.685;
E = -1.552;

Fy = D * sin(C * atan(B * a - E *(B * a - atan(B * a))));

