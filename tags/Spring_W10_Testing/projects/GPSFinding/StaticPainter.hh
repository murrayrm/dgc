#include <cstdio>
#include <cmath>
#include <CMap.hh>
#include <rddf.hh>  
#define FALSE 0
#define TRUE 1

/** RDDFnode is a linked list of RDDF files, which can be loaded and 
    unloaded as required to improve performance. These RDDF files are 
    capture files, meaning the waypoints were actually driven, and the
    speed variable corresponds to the speed the driver was going. Don't
    load regular RDDFs. Capture files are labeled as:
    gps_yyyymmdd_hhmmss.dat. */
typedef struct RDDFnode {
  /** The RDDF data--i.e. the waypoint Northing, Easting, Speed.  Other 
      RDDF features aren't used but included for file compatibility. */
  RDDF *data;

  /** Unique index assigned to each loaded RDDF.  This allows it to be
      identified for unloading. */
  int index;

  /** The largest northing value in the captured RDDF.  Used with other 
      largest/smallest values, to quickly evaluate whether an RDDF is 
      applicable or not to a given area.  */
  double maxNorthing;
  
  /** The smallest northing value in the RDDF. */
  double minNorthing;

  /** The largest easting value in the RDDF. */
  double maxEasting;

  /** The smallest easting value in the RDDF. */
  double minEasting;

  /** A pointer to the next loaded RDDF, if any exists. */
  struct RDDFnode *next;
};

/** StaticPainter takes previously captured RDDF files from drives in the
    desert, and writes them to map deltas, which fusionMapper can then use
    for the planner.  Values in the map are the speed the driver was going 
    at the time of capture, which is assumed to be a reasonable path. */

class StaticPainter {
public:
  // Constructors
  /** Creates a new StaticPainter object. No arguments taken; will require
      Initialize(CMap, layer) and at least one instance of LoadRDDF(&rddf)
      before it becomes useful. */
  StaticPainter();

  /** Destroys a StaticPainter object. */
  // Destructor
  ~StaticPainter();
  
  // Mutators
  /** Loads the map and map layer into StaticPainter. Returns 0 if all went
      well. */
  int Initialize(CMap *map, int layer);
  
  /** Repaints the entire map inside the box.  Not implemented yet. */
  int Repaint();

  /** Paints the trace of the driven path into two map deltas: one for the
      rows exposed by the last movement, and another for the columns exposed.
      Returns 0 if all went well. */
  int PaintDelta();

  /** Loads a captured RDDF into the StaticPainter object, so that if any 
      part of it appears on the map, it will be drawn in. It is possible 
      to have multiple RDDFs loaded at a time.  Each is assigned a unique 
      index at loading; this can be used to unload them as well. Returns
      the index of the loaded RDDF.  

      Paths to RDDFs to be loaded are stored in rddflist.dat, in the 
      GPSFinding directory.*/
  int LoadRDDF(RDDF *newGPStrace);

  /** Unloads a captures RDDF by index.  Not implemented yet. */
  int UnloadRDDF(int oldIndex);

private:
  // Misc
  inline int StaticPainter::between(double middle, double left, double right) {
    return ((middle > left) && (middle < right)) ? TRUE : FALSE;
  }
  
#warning Should template inlines later.  
  inline double StaticPainter::max(double a, double b) {
    return (a > b) ? a : b;
  }
  
  inline double StaticPainter::min(double a, double b) {
    return (a < b) ? a : b;
  }

  inline double StaticPainter::sign(double a) {
    return (a > 0) ? a : -a;
  }  

  inline double StaticPainter::distance(NEcoord a, NEcoord b) {
    return sqrt((a.N-b.N)*(a.N-b.N) + (a.E-b.E)*(a.E-b.E));
  }
  
  inline double StaticPainter::distance(NEcoord a, double N, double E) {
    return sqrt((a.N-N)*(a.N-N) + (a.E-E)*(a.E-E));
  }

  int LineCross(NEcoord map1, NEcoord map2,
		NEcoord traj1, NEcoord traj2);
  int DrawSegment(NEcoord map1, NEcoord map2, 
		  NEcoord traj1, NEcoord traj2, double speed);
  int isPointInBox(NEcoord point, NEcoord map1, NEcoord map2);

  CMap *GPSmap;
  int GPSlayer;
  RDDFnode *GPStrace;

  double leftMapCoord;
  double rightMapCoord;
  double topMapCoord;
  double bottomMapCoord;
  double vehLocN;
  double vehLocE;
  double numRows;
  double numCols;
  double resRows;
  double resCols;
};

// Linked list of RDDFs?  Allow swapping in and out to save memory, maybe...
