#include <fstream>
#include <iostream>
#include <sys/time.h>
#include "frames/coords.hh"
#include "CMap.hh"
#include "rddf.hh"
#include "StaticPainter.hh"
#define CM_OK 0
#warning need to get real CM_OK in here.

int main() {
  timeval t0, t1, t2, t3, t4;
  int i;

  system("rm testpoints");
  system("rm output.traj");
  system("rm outputRDDF.traj");
  FILE *pFile;
  pFile = fopen("outputRDDF.traj", "w");

  gettimeofday(&t0, NULL);

  int layer;
  RDDF rddf("rddf.dat");
  NEcoord initialPoint(rddf.getWaypointNorthing(0), rddf.getWaypointEasting(0));
  CMap map;
  StaticPainter paint;

  map.initMap(0., 0., 200, 200, 1., 1., 0);
  layer = map.addLayer<double>(1e-3, 1e-4);
  paint.LoadRDDF(&rddf);  
  paint.Initialize(&map, layer);

  int updateLoc = map.updateVehicleLoc(initialPoint.N-100, initialPoint.E-100);
  for(i = 0; i < rddf.getNumTargetPoints(); i++) {
    fprintf(pFile, "%10lf %10lf\n",rddf.getWaypointNorthing(i),rddf.getWaypointEasting(i));

    updateLoc = map.updateVehicleLoc(rddf.getWaypointNorthing(i), rddf.getWaypointEasting(i));
    if (updateLoc != CM_OK) {
      cerr << "PaintDelta failed to update GPSmap at (" 
	   << rddf.getWaypointNorthing(i) << ", " 
	   << rddf.getWaypointEasting(i) << ")." << endl;
      exit(1);
    } else {
      paint.PaintDelta();
    }
  }
  gettimeofday(&t1, NULL);
  //  fprintf(pFile, "%10lf %10lf\n",rddf.getWaypointNorthing(i),rddf.getWaypointEasting(i));
}


// Notes: RDDF format:
// (have to write some functions for myself, I think)
// RDDFdata: Northing Easting maxSpeed offset radius(=offset), other stuff
// 

