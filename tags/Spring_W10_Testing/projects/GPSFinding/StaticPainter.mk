STATICPAINTER_PATH = $(DGC)/projects/GPSFinding

STATICPAINTER_DEPEND_LIBS = \
	$(SPARROWLIB) \
	$(RDDFLIB) \
	$(GGISLIB) \
	$(CMAPLIB) \
	$(CMAPPLIB) 

STATICPAINTER_DEPEND_SOURCES = \
	$(STATICPAINTER_PATH)/StaticPainter.hh \
	$(STATICPAINTER_PATH)/StaticPainter.cc

STATICPAINTER_DEPEND = \
	$(STATICPAINTER_DEPEND_LIBS) \
	$(STATICPAINTER_DEPEND_SOURCES)
