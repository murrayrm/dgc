#include <StaticPainter.hh>
#warning Remove/adjust/whatever this \#define
#define CM_OK 0
#define RDDF_LIST "rddflist.dat"
#define MAX_RDDF_FILE_PATH_CHARS 256

StaticPainter::StaticPainter() {
  GPSmap = NULL;
  GPStrace = NULL;
}

StaticPainter::~StaticPainter() {
  // Destruct stuff here.
  RDDFnode *temptrace;
  while(GPStrace != NULL) {
    temptrace = GPStrace->next;
    free(GPStrace);
    GPStrace = temptrace;
  }
  cout << "This module will self-destruct...well, now." << endl;
}

int StaticPainter::Initialize(CMap *cmap, int layer) {
  GPSmap = cmap;
  GPSlayer = layer;
  cout << "GPSlayer: " << GPSlayer << endl;
  /* GPStrace->data = trace->data;
  GPStrace->index = trace->index;
  GPStrace->maxNorthing = trace->maxNorthing;
  GPStrace->minNorthing = trace->minNorthing;
  GPStrace->maxEasting = trace->maxEasting;
  GPStrace->minEasting = trace->minEasting;
  GPStrace->next = trace->next;*/

  leftMapCoord = GPSmap->getWindowBottomLeftUTMNorthing();
  cout << "leftMapCoord: " << leftMapCoord << endl;
  rightMapCoord = GPSmap->getWindowTopRightUTMNorthing();
  cout << "rightMapCoord: " << rightMapCoord << endl;
  topMapCoord = GPSmap->getWindowTopRightUTMEasting();
  cout << "topMapCoord: " << topMapCoord << endl;
  bottomMapCoord = GPSmap->getWindowBottomLeftUTMEasting();
  cout << "bottomMapCoord: " << bottomMapCoord << endl;
  vehLocN = GPSmap->getVehLocUTMNorthing();
  cout << "vehLocN: " << vehLocN << endl;
  vehLocE = GPSmap->getVehLocUTMEasting();
  cout << "vehLocE: " << vehLocE << endl;
  numRows = GPSmap->getNumRows();
  cout << "numRows: " << numRows << endl;
  numCols = GPSmap->getNumCols();
  cout << "numCols: " << numCols << endl;
  resRows = GPSmap->getResRows();
  cout << "resRows: " << resRows << endl;
  resCols = GPSmap->getResCols();
  cout << "resCols: " << resCols << endl;

  char rddffile[MAX_RDDF_FILE_PATH_CHARS];
  ifstream file(RDDF_LIST);
  while(!file.eof()) {
    file.getline(rddffile, MAX_RDDF_FILE_PATH_CHARS);
    static RDDF rddf(rddffile);
    LoadRDDF(&rddf);
  }
  return CM_OK;
}

int StaticPainter::Repaint() {
  cout << "Repaint called." << endl;
}

int StaticPainter::PaintDelta() {
  // Assume this called _after_ vehloc updated elsewhere
  int i,j;
  int row, col;
  NEcoord map1, map2;
  NEcoord traj1, traj2;

  FILE *pFile;
  pFile = fopen("output.traj","a");

  NEcoord deltaBox[2][4];
  NEcoord tempDelta[4];
  GPSmap->getExposedRowBoxUTM(tempDelta);
  for(i = 0; i < 4; i++) {
    deltaBox[0][i] = tempDelta[i];
  }
  GPSmap->getExposedColBoxUTM(tempDelta);
  for(i = 0; i < 4; i++) {
    deltaBox[1][i] = tempDelta[i];
  }

  // Run search to see if trajectories fall in
  // modification necessary for variable names
  // for map, 1 is bottom left, 2 is top right
  // i = box number.
  for (i = 0; i < 2; i++) {
    map1.N = min(deltaBox[i][0].N, deltaBox[i][2].N);
    map1.E = min(deltaBox[i][0].E, deltaBox[i][2].E);
    map2.N = max(deltaBox[i][0].N, deltaBox[i][2].N);
    map2.E = max(deltaBox[i][0].E, deltaBox[i][2].E);
    

    //    cout << "Map "  << map1.N << " " << map1.E << " " << map2.N << " " << map2.E << endl;

    for (RDDFnode *currentPath = GPStrace; currentPath != NULL; currentPath = currentPath->next) {
      if (((map1.N < GPStrace->minNorthing) && (map2.N < GPStrace->minNorthing)) ||
	  ((map1.N > GPStrace->maxNorthing) && (map2.N > GPStrace->maxNorthing)) ||
	  ((map1.E < GPStrace->minEasting) && (map2.E < GPStrace->minEasting)) ||
	  ((map1.E > GPStrace->maxEasting) && (map2.E > GPStrace->maxEasting))) {
	//	cout << "out of box, missed completely." << endl;
	continue;
      } else {
	
	// Run through each segment of each RDDF.
	int temp = currentPath->data->getNumTargetPoints();
	for(j = 0; j < (temp-1); j++) {
	  traj1.N = currentPath->data->getWaypointNorthing(j);
	  traj1.E = currentPath->data->getWaypointEasting(j);
	  traj2.N = currentPath->data->getWaypointNorthing(j+1);
	  traj2.E = currentPath->data->getWaypointEasting(j+1);	      
	
	  // Eliminate segments not nearby first.
	  if (((traj1.N < map1.N) && (traj2.N < map1.N)) ||
	      ((traj1.N > map2.N) && (traj2.N > map2.N)) ||
	      ((traj1.E < map1.E) && (traj2.E < map1.E)) ||
	      ((traj1.E > map2.E) && (traj2.E > map2.E))) { 
	    continue;
	  }
	  
	  // Next, eliminate ones with at least one end inside.
	  if (((between(traj1.N, map1.N, map2.N))&&(between(traj1.E, map1.E, map2.E))) ||
	      ((between(traj2.N, map1.N, map2.N))&&(between(traj2.E, map1.E, map2.E)))) {
	    DrawSegment(map1, map2, traj1, traj2, 
			currentPath->data->getWaypointSpeed(j));
	    fprintf(pFile, "%10lf %10lf\n",traj1.N,traj1.E);
	    fprintf(pFile, "%10lf %10lf\n",traj2.N,traj2.E);
	    continue;
	  }
	  
	  // Lastly, check the difficult ones.
	  if (LineCross(map1, map2, traj1, traj2) == TRUE) {
	    DrawSegment(map1, map2, traj1, traj2, 
			currentPath->data->getWaypointSpeed(j));
	  }
	} // End segment runthrough
      } // End RDDF runthrough
    }
  } // End all-box runthrough
  fclose(pFile);
  return CM_OK;
}

int StaticPainter::isPointInBox(NEcoord point, NEcoord map1, NEcoord map2) {
  if(((point.N > map1.N) && (point.N > map2.N)) ||
     ((point.N < map1.N) && (point.N < map2.N)) ||
     ((point.E > map1.E) && (point.E > map2.E)) ||
     ((point.E < map1.E) && (point.E < map2.E))) {
    return 0;
  }
  return 1;
}

int StaticPainter::LoadRDDF(RDDF *myRDDF) {
  // Build 'em and give 'em sequential indices.
  cout << "LoadRDDF called." << endl;
  
  RDDFnode *newRDDFnode = new RDDFnode;
  newRDDFnode->data = myRDDF;
  newRDDFnode->next = NULL;
  
  // Preload important information about the RDDF.
  int pts = newRDDFnode->data->getNumTargetPoints();
  double maxN = newRDDFnode->data->getWaypointNorthing(0);
  double minN = maxN;
  double maxE = newRDDFnode->data->getWaypointEasting(0);
  double minE = maxE;
  for (int i = 1; i < pts; i++) {
    double testN = newRDDFnode->data->getWaypointNorthing(i);
    maxN = max(testN, maxN);
    minN = min(testN, minN);
    double testE = newRDDFnode->data->getWaypointEasting(i);
    maxE = max(testE, maxE);
    minE = min(testE, minE);
  }
  newRDDFnode->maxNorthing = maxN;
  newRDDFnode->minNorthing = minN;
  newRDDFnode->maxEasting = maxE;
  newRDDFnode->minEasting = minE;

  if (GPStrace == NULL) {
    newRDDFnode->index = 1; 
    GPStrace = newRDDFnode;
  }  else {
    RDDFnode *temp = GPStrace;
    while(temp->next != NULL) {
      temp = temp->next;
    }
    newRDDFnode->index = temp->index + 1;
    temp->next = newRDDFnode;
  }
  cout << "First GPS point: (" << GPStrace->data->getWaypointNorthing(0)
       << ", " << GPStrace->data->getWaypointEasting(0) << ")." << endl;

  return newRDDFnode->index;
}

#warning don't call UnloadRDDF--it's not ready.
int StaticPainter::UnloadRDDF(int index) {
  RDDFnode *tempnode;
  for(tempnode = GPStrace; tempnode != NULL; tempnode = tempnode->next) {
    if (1) {
    }
  }
  /* Stuff goes here. */  
}

int StaticPainter:: LineCross(NEcoord map1, NEcoord map2,
			     NEcoord traj1, NEcoord traj2) {
  // This algorithm is used to determine whether a line with *both* ends 
  // outside a box intersects that box.  Simply, it intersects the box 
  // iff it intersects the diagonals of the box.  Thus, we take an 
  // algorithm that determines if two lines intersect, and run it twice.

  // Parameterize each line segment as a line, with the parameters s and t
  // varying from 0 to 1 inside the segment boundaries and other values
  // outside.
  // Algorithm found at vb-helper.com, adapted.  Thanks, Google!
  
  /*  cout << "LineCross was called for map corners (" << map1.N << "[N], "
       << map1.E << "[E]) and (" << map2.N << "[N], " << map2.E
       << "[E])." << endl;
  cout << "LineCross was called for traj ends (" << traj1.N << "[N], "
       << traj1.E << "[E]) and (" << traj2.N << "[N], " << traj2.E
       << "[E])." << endl;*/
  
  double dMapN = map2.N-map1.N;
  double dMapE = map2.E-map1.E;
  double dTrajN = traj2.N-traj1.N;
  double dTrajE = traj2.E-traj1.E;
  double swwd = dMapN*dTrajE-dTrajN*dMapE; // Slope Written Without Divisions

  // Do the first diagonal, from the bottom left to the top right.
  
  if (swwd != 0) { // parallel lines
    double s = (dTrajN*(map1.E-traj1.E) - dTrajE*(map1.N-traj1.N))/swwd;
    double t = (dMapN*(map1.E-traj1.E) - dMapE*(map1.N-traj1.N))/swwd;
    if ((s >= 0) && (s <= 1) && (t >= 0) && (t <= 1)) {
      return true;
    }
  }

  // Do the second diagonal, from the top left to the bottom right.
  
  double temp = map2.E;
  map2.E = map1.E;
  map1.E = temp;

  temp = map2.N;
  map2.N = map1.N;
  map1.N = temp;

  dMapN = map2.N-map1.N;
  dMapE = map2.E-map1.E;
  dTrajN = traj2.N-traj1.N;
  dTrajE = traj2.E-traj1.E;
  swwd = dMapN*dTrajE-dTrajN*dMapE;
  
  if (swwd != 0) { // parallel lines
    double s = (dTrajN*(map1.E-traj1.E) - dTrajE*(map1.N-traj1.N))/swwd;
    double t = (dMapN*(map1.E-traj1.E) - dMapE*(map1.N-traj1.N))/swwd;
    if ((s >= 0) && (s <= 1) && (t >= 0) && (t <= 1)) {
      return true;
    }
  }

  // No intersections found, then we..
  return false;
}

int StaticPainter::DrawSegment(NEcoord map1, NEcoord map2,
			       NEcoord traj1, NEcoord traj2, double speed) {
#warning Define MAX_PRECISION as 1/max(double).
#define MAX_PRECISION 1e-8
  
  FILE *pFile;
  pFile = fopen ("testpoints","a");

  // Define window coordinates for each NEcoord.
  int mapN1, mapN2, mapE1, mapE2;
  int trajN1, trajN2, trajE1, trajE2;
  GPSmap->UTM2Win(map1.N, map1.E, &mapN1, &mapE1);
  GPSmap->UTM2Win(map2.N, map2.E, &mapN2, &mapE2);
  GPSmap->UTM2Win(traj1.N, traj1.E, &trajN1, &trajE1);
  GPSmap->UTM2Win(traj2.N, traj2.E, &trajN2, &trajE2);
  
  // Prepare parts of slope.
  double dy = traj2.N-traj1.N;
  double dx = traj2.E-traj1.E;
  
  // Case 1: Horizontal segment.
  if (fabs(dy) < MAX_PRECISION) {
    cout << "Case 1" << endl;
    return CM_OK;
    if (trajE2 > trajE1) {
      for (int i = trajE1; i <= trajE2; i++) {
	if(between(i, mapE1, mapE2) || i == mapE1 || i == mapE2) {
	  GPSmap->setDataWin(GPSlayer, trajN1, i, speed);
	}
      } 
    }
    else {
      for (int i = trajE2; i <= trajE1; i++) {
	if(between(i, mapE1, mapE2) || i == mapE1 || i == mapE2) {
	  GPSmap->setDataWin(GPSlayer, trajN1, i, speed);
	}
      }
    }
    return CM_OK;
  }

  // Case 2: Vertical segment
  if (fabs(dx) < MAX_PRECISION) {    
    cout << "Case 2" << endl;
    return CM_OK;
    
    if (trajN2 > trajN1) {
      for (int i = trajN1; i <= trajN2; i++) {
	if(between(i, mapN1, mapN2) || i == mapN1 || i == mapN2) {
	  GPSmap->setDataWin(GPSlayer, i, trajE1, speed);
	}
      } 
    }
    else {
      for (int i = trajN2; i <= trajN1; i++) {
	if(between(i, mapN1, mapN2) || i == mapN1 || i == mapN2) {
	  GPSmap->setDataWin(GPSlayer, i, trajE1, speed);
	}
      }
    }
    return CM_OK;
  }
  
  // Case 3: Other slant
  // (This is the general case.)
 double m = dy/dx;
 /* double xShift = sign(dx)*resCols/abs(m);
    double yShift = sign(dy)*resRows*abs(m);*/
 
 double xShift = resCols/m;
 double yShift = resRows*m;
 double signN = (traj1.N>traj2.N? -1: 1);
 double signE = (traj1.E>traj2.E? -1: 1);

 
 // Find intersection point with 1st row and column--these determine the
 // offset that is combined with the shift above.
 NEcoord firstNIntercept, firstEIntercept;
  
 GPSmap->roundUTMToCellBottomLeft(traj1.N, traj1.E, &firstNIntercept.N, &firstEIntercept.E);
 firstNIntercept.N += (dy > 0 ? resRows : 0);
 firstEIntercept.E += (dx > 0 ? resCols : 0);
 firstNIntercept.E = (firstNIntercept.N - traj1.N)/m + traj1.E;
 firstEIntercept.N = m*(firstEIntercept.E - traj1.E) + traj1.N;

 int xCounter = 0, yCounter = 0;
 double t, tx, ty;
 NEcoord xPoint, yPoint;
 for(t = 0; t <= distance(traj1, traj2);) {
   /*xPoint.N = firstEIntercept.N + xCounter*resRows;
   xPoint.E = firstEIntercept.E + xCounter*xShift;
   yPoint.N = firstNIntercept.N + yCounter*yShift;
   yPoint.E = firstNIntercept.E + yCounter*resCols;*/
   xPoint.N = firstEIntercept.N + signE*xCounter*yShift;
   xPoint.E = firstEIntercept.E + signE*xCounter*resCols;
   yPoint.N = firstNIntercept.N + signN*yCounter*resRows;
   yPoint.E = firstNIntercept.E + signN*yCounter*xShift;
   tx = distance(traj1, xPoint);
   ty = distance(traj1, yPoint);
   if (tx < ty) {
     t = tx;
     if(isPointInBox(xPoint, map1, map2)) {
       fprintf(pFile, "%10lf %10lf\n",(xPoint.N + resRows/2),xPoint.E);
       GPSmap->setDataUTM(GPSlayer, (xPoint.N + resRows/2), xPoint.E, speed);
     }
     xCounter++;
   } else {
     t = ty;
     if(isPointInBox(yPoint, map1, map2)) {
       fprintf(pFile, "%10lf %10lf\n",yPoint.N,(yPoint.E + resCols/2));
       GPSmap->setDataUTM(GPSlayer, yPoint.N, (yPoint.E + resCols/2), speed);
     }
     yCounter++;
   }
 }   
 fclose(pFile);
 return CM_OK;
}  
