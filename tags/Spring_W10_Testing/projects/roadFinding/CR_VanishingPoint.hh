//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// CR_VanishingPoint
//----------------------------------------------------------------------------

#ifndef CR_VANISHING_DECS

//----------------------------------------------------------------------------

#define CR_VANISHING_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glut.h>

#include "opencv_utils.hh"

// my stuff 

#include "CR_Memory.hh"
#include "CR_Error.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

// uncomment this line if an Nvidia GeForce6-series
// graphics card with 16-bit floating point blending
// is installed
//#define HAVE_GEFORCE6

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))

// voter to candidate coordinate transformation

#define V2C_X(vx)        (candidate_scale * ((vx) - candidate_x_offset))
#define V2C_Y(vy)        (candidate_scale * ((vy) - candidate_y_offset))

// candidate to voter coordinate transformation

#define C2V_X(cx)        (inv_candidate_scale * (candidate_x_offset - (cx)))
#define C2V_Y(cy)        (inv_candidate_scale * (candidate_y_offset - (cy)))

#define ONE_OVER_256     0.00390625

#define MAX_ANGLE_DIFF   3.14159

//-------------------------------------------------
// structure
//-------------------------------------------------


//-------------------------------------------------
// functions
//-------------------------------------------------

void initialize_vanishing_point_estimation(int, int);
void compute_vanishing_point(int, int);
void process_vanishing_point_command_line_flags(int argc, char **argv);
void opengl_linear_vanishing_point_estimation(int, int);
void draw_vanishing_point(int, int);
void compute_ground_to_camera_transform(float, float, float, float, CvMat *);
void initialize_vanishing_point_support(int, int);
float compute_vanishing_point_support(float, float);
void draw_vanishing_point_support(float, float, int, int);

void init_fp_blend(int, int);
void enter_fp_blend(int, int);
void exit_fp_blend();
void fp_blend_test();

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
