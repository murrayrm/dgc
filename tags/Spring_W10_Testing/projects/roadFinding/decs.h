//----------------------------------------------------------------------------
//  Road follower module
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------------

#ifndef DECS_DECS

//----------------------------------------------------------------------------

#define DECS_DECS

// General

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Fast Fourier transform

#include <fftw3.h>

// OpenCV includes

// #include "cv.h"
// #include "highgui.h"

// OpenGL/GLUT includes

#include <GL/glut.h>
#include "glx.h"
#include "glext.h"

#include "opencv_utils.hh"

// My stuff

#include "CR_Memory.hh"
#include "CR_Random.hh"
#include "CR_Linux_Time.hh"
#include "CR_Error.hh"
//#include "CR_OpenCV_Image.hh"
//#include "CR_Linux_Image.hh"
#include "CR_Linux_Matrix.hh"
#include "CR_Linux_Movie_AVI.hh"
#include "CR_DominantOrientations.hh"
#include "CR_VanishingPoint.hh"
//#include "CR_CaptureFirewire.hh"
#include "firewireCamera.hh"
#include "CR_Tracker.hh"
#include "CR_OnOff.hh"

//----------------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------------

// general 

#ifndef MAXSTRLEN
#define MAXSTRLEN  256
#endif

#ifndef PI
#define PI      3.14159265358979323846
#endif

#ifndef PI_2
#define PI_2    1.57079632679489661923
#endif

#ifndef TWOPI
#define TWOPI   (PI * 2.0)
#endif

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

// camera calibration data; in degrees

#define HORIZ_FOV_DEGS      45.0
#define HORIZ_PIX2DEG(x, w) ((x)*HORIZ_FOV_DEGS/(w))
#define HORIZ_PIX2RAD(x, w) (DEG2RAD(PIX2DEG((x), (w))))

// be CAREFUL with sending functions to macros, because
// they'll probably get evaluated more times than you 
// want.  In particular, don't send rand() in the x and
// y slots, or the i slot, because it'll be a goddamn mess.
// assign the result of the function call to a variable,
// then pass it along.

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))
#define MAX3(A, B, C)         (((A) > (B)) ? MAX2((A), (C)) : MAX2((B), (C)))
#define MIN3(A, B, C)         (((A) < (B)) ? MIN2((A), (C)) : MIN2((B), (C)))


#define SOURCE_TYPE_IMAGE  1
#define SOURCE_TYPE_AVI    2
#define SOURCE_TYPE_LIVE   3
#define SOURCE_TYPE_RAW    4

//----------------------------------------------------------------------------

void finish_run();
int read_next_image(FILE *, IplImage **);
void initialize_live_logger();
void write_live_logger();

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif
