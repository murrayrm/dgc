//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_OnOff.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

float KL_threshold               = 0.1;    // minimum KL divergence to think this image is good
int KL_time_window               = 300;    // how many past images to take into account for filtering 
float KL_majority_fraction       = 0.667;  // what fraction of time window must be good for filtering
int print_onoff                  = FALSE;
int do_onoff                     = TRUE;

int KL_index;
float *KL_histogram;
float *KL_history;
int KL_history_full;
float KL_uniform_prob;
int KL_num_vote_levels;
	
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// MVIV threshold = 0.2 for 160 x 120 images, window = 300, 2/3 majority must be on-road

void initialize_onoff_confidence()
{
  // 256 gray levels in vote function

  KL_num_vote_levels = 256;
  KL_uniform_prob = 1.0 / (float) KL_num_vote_levels;
  KL_histogram = (float *) calloc(KL_num_vote_levels, sizeof(float));
  
  // initialize array to store last time_window KL values
  // this wraps around, so keep track of current index and whether we've wrapped
  KL_history = (float *) calloc(KL_time_window, sizeof(float));
  KL_history_full = FALSE;
  KL_index = 0;
}

//----------------------------------------------------------------------------

// returns fraction of last KL_time_window KL measurements
// that were over KL_threshold (i.e., it's up to caller to 
// threshold this on KL_majority_fraction or use directly as confidence)

float compute_onoff_confidence(IplImage *im)
{
  int i, num_on;
  float confidence;
 

  if (!do_onoff)
    return 1.0;
				
  // record current value in history buffer

  KL_history[KL_index] = KL_from_uniform(im);
  if (KL_index < KL_time_window - 1)
    KL_index++;
  else {
    if (!KL_history_full)
      KL_history_full = TRUE;
    KL_index = 0;
  }

  // threshold and filter based on past history 

  if (!KL_history_full) {
    for (i = 0, num_on = 0; i < KL_index; i++)
      if (KL_history[i] > KL_threshold)
	num_on++;
    confidence = (float) num_on / (float) KL_index;
  }
  else {
    for (i = 0, num_on = 0; i < KL_time_window; i++)
      if (KL_history[(i + KL_index) % KL_time_window] > KL_threshold)
	num_on++;
    confidence = (float) num_on / (float) KL_time_window;
  }
  
  if (print_onoff)
    printf("index %i full %i, on %i conf %.2f KL %.2f\n", KL_index, KL_history_full, num_on, confidence, KL_history[KL_index]);

  return confidence;
}

//----------------------------------------------------------------------------

// Kullback-Leibler divergence of vote function from uniform distribution

float KL_from_uniform(IplImage *im)
{
  int i, j;
  float total, KL, prob;

  // compute probability of each vote total in this image

  for (i = 0; i < KL_num_vote_levels; i++)
    KL_histogram[i] = 0;

  for (j = 0, total = 0.0; j < im->height; j++)
    for (i = 0; i < im->width; i++) {
      KL_histogram[(int) ((float) (KL_num_vote_levels - 1) * FLOAT_IMXY(im, i, j))]++;
      total++;
    }

  // compute divergence from uniform distribution

  for (i = 0, KL = 0; i < KL_num_vote_levels; i++) {
    prob = KL_histogram[i] / total;  	
//    if (print_onoff)
//       printf("%i %f\n", i, prob);
    if (prob) 
      KL += KL_uniform_prob * (log(prob / KL_uniform_prob));
  }

  return -KL;
}

//----------------------------------------------------------------------------

void process_onoff_command_line_flags(int argc, char **argv) 
{
  int i;

  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i],                             "-klthresh"))
      KL_threshold = atof(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-klthreshold"))
      KL_threshold = atof(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-klwindow"))
      KL_time_window = atoi(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-klmajority"))
      KL_majority_fraction = atof(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-noonoff"))
      do_onoff = FALSE;
    else if (!strcmp(argv[i],                        "-printonoff"))
      print_onoff = TRUE;
  }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
