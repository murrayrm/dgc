function result = plotMap(map, plot_error_bars, plot_options)
    if map.type==0
        plot_error_bars=0;
    end
    if plot_error_bars==0
        plot(map.vals(:,1), map.vals(:,2), plot_options);
    else
        errorbar(map.vals(:,1), map.vals(:,2), map.vals(:,3), map.vals(:,3), plot_options);
    end