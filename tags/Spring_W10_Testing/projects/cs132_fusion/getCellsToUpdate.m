function result = getCellsToUpdate(map, point, covar, max_range)
    cellNum = getCellNum(map, point);
    total_prob = 0;
    if 0<cellNum & cellNum<length(map.vals)
        if isempty(find(eig(covar)<=0))
            mvnpdfval = mvnpdf([map.vals(cellNum,1) point(3)]', point([1 3]), covar)*map.res;
            total_prob = total_prob+mvnpdfval;
        else
            result = [];
            return;
        end
    else
        result = [];
        return;
    end
    j = 1;
    while total_prob<0.95 & j<max_range
        for i=[cellNum-j, cellNum+j]
            if 0<i & i<length(map.vals)
                mvnpdfval = mvnpdf([map.vals(i,1) point(3)]', point([1 3]), covar)*map.res;
                total_prob = total_prob + mvnpdfval;
            else
                result = (cellNum-j+1):(cellNum+j-1);
                return;
            end
        end
        j = j + 1;
    end
    result = (cellNum-j):(cellNum+j);
            