;----------------------------------------
; extra functions
;----------------------------------------

; insert date and time for notetaking stuff
(defun insert-date-time ()
   "Inserts human readable date and time string." (interactive)
     (insert (format-time-string "[%Y-%m-%d %l:%M:%S %P] ")))

(fset 'goto-other-buffer "\C-xb\C-m")


;----------------------------------------
; key bindings
;----------------------------------------

(global-set-key [(control b)] 'goto-other-buffer)
(global-set-key [backtab] 'dabbrev-expand)
(global-set-key [(control ?c) (?r)] 'insert-date-time)
(global-set-key [(control v)] 'other-window)









