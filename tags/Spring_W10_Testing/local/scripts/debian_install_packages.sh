#!/bin/sh

# Definition of packages to install:
# NOTE: If you add a new line, make sure to comment it and add it to the PACKAGES variable!

COMPILE="make g++ gcc g77 automake1.7 autoconf m4 libtool libtool-doc doxygen"
DGC_LIBS="libncurses5-dev blas-dev" 
#BOOST_LIBS="libboost-dev=1.31.0-9 libboost-thread1.31.0 libboost-thread-dev=1.31.0-9 libboost-doc"
EDITORS="vim vim-gtk vim-doc qemacs xemacs21 emacs21 kdevelop"
DEVELOPMENT="$COMPILE $DGC_LIBS $EDITORS"

MATH="gnuplot octave2.1 octave2.1-headers octave-forge f2c libg2c0-dev"
SYSADMIN="sudo ssh subversion rpm alien apmd aptitude nis"
KERNEL="kernel-headers-2.4.26-1-386 kernel-package"
BASIC_UTILS="wget zip unzip less bison gawk texinfo cvs xterm"
IMAGE_UTILS="imagemagick" # availability of xv and mplayer?
MORE_UTILS="wireless-tools xpdf k3b ifplugd mozilla-firefox"
ACROREAD="acroread"
BASIC="$MATH $SYSADMIN $KERNEL $BASIC_UTILS $IMAGE_UTILS $MORE_UTILS $ACROREAD"

GDAL="gdal-bin libgdal1 libgdal1-dev libgdal-doc python-gdal"
# Compile ode from source for terrain modeling support (do not use libode-dev)
#ODE="libode-dev"
OPENGL="xlibmesa-dev glutg3 glutg3-dev glut-doc"
PYTHON="python-opengl python-dev swig swig-doc libwxgtk2.4-python"
PROJ='proj proj-ps-doc'
LIB3DS="view3ds lib3ds-dev"
XML="libxml1 libxml-dev libxml2 libxml2-dev"
LIBXMU="libxmu-dev"
GAZEBO="$GDAL $ODE $OPENGL $PYTHON $PROJ $XML $LIBXMU $LIB3DS"

# Removed libgtk2.0-dbg because it caused headaches on Holmium
GTK2='libgtk2.0-dev libgtk2.0-doc'
GTKMM24='libgtkmm-2.4-dev libgtkmm-2.4-doc'
GTKGLEXT='libgtkglext1 libgtkglext1-dev libgtkglext1-doc'
# Maybe will need to install gtkglextmm-1.1.0 manually
#GTKGLEXTMM='libgtkglextmm1 libgtkglextmm1-dev' 
MAPDISPLAY="$GTK2 $GTKMM24 $GTKGLEXT $GTKGLEXTMM"

#OPENCV="libopencv-dev libopencv-doc libhighgui-dev libcvaux-dev"
CAM="libraw1394-5 libraw1394-dev libdc1394-10 libdc1394-10-dev libdc1394-examples"
CANVAS="libgnomecanvas2-0 libgnomecanvas2-dev libgnomecanvas2-doc"
JPEG="libjpeg62 libjpeg62-dev"
GSL="libgsl0 gsl-bin"
PLAYER="$OPENCV $CAM $CANVAS $JPEG $GSL"

PACKAGES="$DEVELOPMENT $BASIC $GAZEBO $MAPDISPLAY $PLAYER"

if [ "$UID" != "0" ]; then
        echo "You must be root to run this script!"
        exit 0
fi

if [ ! -f sources.list ]; then
	echo "You must be in the same directory as sources.list to run this script!"
	exit 0
fi

# name of the apt sources list:
CUR_FILE=/etc/apt/sources.list
NEW_FILE=sources.list

if [[ ! -f $NEW_FILE ]]; then
  echo You must stand in dgc/loca/scripts to run this script!
  exit 0
fi

DIFF=$(diff $CUR_FILE $NEW_FILE)

if [[ $DIFF ]]; then
  echo Your old and your the new sources.list file differ!
  echo If you chose to overwrite with the new one
  echo \(the old one will be echo saved as sources.list.BAK\)
  read -s -n1 -p "Overwrite? (Y/n)" input
  echo $input
  if [[ ! $input || "$input" == "Y" || "$input" == "y" ]]; then
    cp $CUR_FILE $CUR_FILE.BAK
    cp $NEW_FILE $CUR_FILE
  fi
fi

# update the apt cache
echo
echo Running apt-get update...
apt-get update

# this is the standard set of packages to install for a development laptop
echo
echo Installing Team Caltech packages: $PACKAGES
echo
apt-get install --fix-missing $PACKAGES 

echo
echo Team Caltech installation script finished!
echo "(Don't forget to check for errors)."
echo
#echo If you got any error about 404 file not found, you may need to
#echo remove toughguy.caltech.edu from sources.list and run this script again.

exit 0

