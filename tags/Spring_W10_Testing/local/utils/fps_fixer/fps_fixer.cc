#include <vector>
#include <fstream> // ifstream
#include <string>
#include <boost/tokenizer.hpp>
using namespace std;

/**
 * fps_fixer:
 * This program was created to be able getting 25fps movies in almost accurate
 * real time from the images logged by stereo vision cameras.
 * It like this:
 * - You give the recorded fps and the desired output fps as arguments
 * - This program will list all your .ppm files (yes you must have converted
 *   your .bmp files to .ppm since the video encoding applications in linux
 *   will never support a windows format like BMP)
 * - It will then duplicate some of the images to make the resulting image
 *   sequence fit the desired fps.
 * 
 * Making a accurate real time movie must be done this way, since the encoders 
 * can only encode in standard fps'es (like 24, 25, 30).
 * Author: Henrik Kjellander
 * Created: 7 Oct 2004
 */
int main(int argc, char **argv)
{
  if(argc != 3)
  {
    printf("Program for duplicating images in a sequence to gain a realistic speed\n");
    printf("for playback when encoded with a standard fps rate.\n");
    printf("\n");
    printf("Usage: %s state_log desired_fps\n", argv[0]);
    printf("\n");
    printf("state_log      the filename of the .state file\n");
    printf("desired_fps    the fps we want to encode the movie with\n");
    printf("\n");
    printf("Example:\n");
    printf("  %s field_test.state 25 \n", argv[0]);
    printf("  For stereovision images logged with .state file and we wish to\n");
    printf("  encode in standard PAL (25fps)\n");
    exit(0);
  }

  // save the arguments
  char* state_filename = argv[1];
  int desired_fps = atoi(argv[2]);
  
  // parse the .state file to find out the average recorded fps
  printf("fps_fixer: Reading .state file: '%s'\n", state_filename);
  ifstream statelog(state_filename);
  if(!statelog.is_open())
  {
    printf("fps_fixer: Cannot read file '%s'\n", state_filename);
    exit(1);
  }

  // parse the first time, which is the second element on the first row
  // This is soooo wierd, I have to use statelog.getline() method
  // because when I use getline(statelog, line) I only get empty lines.
  // Maybe there's something strange with the .state file format????
  string line;
  char buf[1024];
  statelog.getline(buf, 1023);
  line = buf;
  //printf("line: %s\n", line.c_str());
  
  typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
  boost::char_separator<char> sep(" \t"); // spaces or tabs
  tokenizer tok(line, sep);
  tokenizer::iterator tok_iter = tok.begin();
  tok_iter++; // now we have the second element
  double start_time = atof((*tok_iter).c_str());
  
  // now keep going to the last row
  int row_counter = 1;
  char lastbuf[1024];
  while(statelog.getline(buf, 1023))
  {
    row_counter++;
    strcpy(lastbuf, buf);
  }  
  line = lastbuf;
  //printf("line: %s\n", line.c_str());
  
  // at the last row, lets to the same thing as above to find the last time
  tok.assign(line, sep);
  tok_iter = tok.begin();
  tok_iter++; // now we have the second element
  double end_time = atof((*tok_iter).c_str());
  
  double time_elapsed = end_time - start_time;
  
  int recorded_fps = (int)(row_counter / time_elapsed);
  
  printf("Parsed recorded fps: %d\n", recorded_fps);
  statelog.close();
  
    
  // better hope that noone uses a file called ppm_list.txt since it's
  // about to be overwritten now :)
  char* list_filename = "ppm_list.txt";
  strcpy(buf, "ls *.ppm > ");
  strcat(buf, list_filename);
  
  // create a list of the files
  printf("executing: %s\n", buf);
  system(buf);
  
  // create an output directory
  strcpy(buf, "mkdir -p output");
  printf("executing: %s\n", buf);
  system(buf);
  
  // lets read the file we created!
  printf("fps_fixer: parsing file list (%s)\n", list_filename);
  ifstream listfile(list_filename);
  if(!listfile.is_open())
  {
    printf("Error reading file %s\n", list_filename);
    exit(-1);
  }
  
  // vectors and strings are nice and easy to handle
  vector<string> image_files;
  
  while(getline(listfile, line))
  {
     image_files.push_back(line);
  }   
  
  // Calculate the ratio
  double ratio = desired_fps / (double)recorded_fps;
  printf("ratio: %lf\n", ratio);
  if(ratio < 1.0)
  {
    printf("ratio is < 1.0, which means there's no point duplicating images\n");
    printf("you need to select a higher encoder fps than %d or delete images\n", desired_fps);
    printf("(that feature is not supported by this program)\n");
    exit(-1);
  }
  
  // Now duplicate frames at the proper places so we get a sequence with the
  // desired fps
  double counter = 0.0;
  string outfile;      // filename for the outfile
  string copy_outfile; // for making link copies
  string output_prefix = "img_";
  string output_extension = ".ppm";
  int output_number = 1; // for the output filenames
  char output_number_buf[128];
  string copy_cmd;
  int created_counter = 0;
      
  for(int i=0; i < image_files.size() ;i++)
  {
    sprintf(output_number_buf, "%06d", output_number++);
    outfile = output_prefix + output_number_buf + output_extension;
    copy_cmd = "mv " + image_files[i] + " output/" + outfile;
    printf("executing: %s\n", copy_cmd.c_str());
    system(copy_cmd.c_str());
    
    counter += 1.0;
    while(counter > ratio)
    {
      // create one/more duplicates as links
      sprintf(output_number_buf, "%06d", output_number++);
      copy_outfile = output_prefix + output_number_buf + output_extension;
      copy_cmd = "ln -s " + outfile + " output/" + copy_outfile;
      printf("executing: %s\n", copy_cmd.c_str());      
      system(copy_cmd.c_str());
      created_counter++;
      counter -= ratio;
    }
  }
  listfile.close();
  
  // Lets move all the files from output back again:
  strcpy(buf, "mv output/*.ppm .");
  printf("executing: %s\n", buf);
  system(buf);
  
  // Here comes the housekeeping ;)
  strcpy(buf, "rm ");
  strcat(buf, list_filename);
  printf("executing: %s\n", buf);
  system(buf);
  // ...and the directory
  strcpy(buf, "rmdir output");
  printf("executing: %s\n", buf);
  system(buf);
  
  printf("Summary:\n");
  printf(" desired fps: %d\n", desired_fps);
  printf(" recorded average fps: %d\n", recorded_fps);
  printf(" ratio (desired/recorded): %lf\n", ratio);
  printf(" number of extra images created (links): %d\n", created_counter);
  printf("Done!\n");
  exit (0);
}
