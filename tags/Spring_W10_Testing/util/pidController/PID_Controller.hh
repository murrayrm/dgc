/*
 *CPID_Controller: PID Lateral & Longitudinal Trajectory Follower implemented using
 *the wrapper classes PathFollower (MTA) or sn_TrajFollower (Skynet)
 */

#ifndef PID_CONTROLLER_HH
#define PID_CONTROLLER_HH

#define DEBUG_LEVEL 0

// Only define this if you want to use compute_SteerCmd_simple();
#warning "New option here for doing simple feedback (not tuned)"
//#define SIMPLE_LATERAL_CONTROL
//#define PURE_PURSUIT_LATERAL_CONTROL
// Subset of above #define to just use compute_SteerFF_simple();
//#define SIMPLE_STEER_FEEDFORWARD
// Whether to limit the steering angle based on maximum lateral acceleration
//#define USE_DFE

#include "VehicleState.hh"  // for VehicleState
#include "ActuatorState.hh"

// The CTraj trajectory container class and teh CPid pid controller wrapper
// class
#include "traj.h"
#include "pid.hh"

#define LatGainFilename				"LatGains.dat"
#define LongGainFilename			"LongGains.dat"

//Distance in meters from the path (perpendicular distance) outside of  which the controller
//will point Alice directly towards (perpendicular to) the path
#define PERP_THETA_DIST				20.0

//alpha and beta gains for the combined error controller
#define ALPHA_GAIN						0.4
#define BETA_GAIN							(2.0*ALPHA_GAIN*PERP_THETA_DIST/(M_PI - M_PI*ALPHA_GAIN))

//value of combined error that causes the modified
//commanded speed (which has a max value = v_traj
//although its current value is scaled by a negative
//gradient linear slope which has a y-intercept of
//v_traj, and a calculated -ve gradient, until the
//saturation point at a small speed, and a relatively
//large combined error
#define C_ERROR_SAT   2.0

//Lookahead *as a multiple of the wheelbase* used when determining the lateral controller feed-forward 
//term - the position to which the closest point on the trajectory is used to calculate the feed-forward
//term is calculated by moving along a straight line drawn between the two axles by a distance equal to
//(wheelbase*LOOKAHEAD_FF_LAT_REFPOS), note that LOOKAHEAD_FF_LAT_REFPOS can be EITHER POSITIVE OR NEGATIVE
#define LOOKAHEAD_FF_LAT_REFPOS  0.0


//This is the 'small speed' at which the reference
//speed is set when the combined error is greater
//than the C_ERROR_SAT
#define V_LAT_ERROR_SAT 4.0


// indices into the error arrays
#define FRONT 0
#define REAR  1
#define YAW   2

// minimum speed. anything lower than this is bumped up
#define MINSPEED 0.01

//IF TrajFollower reaches a point on the 'current' trajectory
//where there are only this many traj-points until the end of the
//trajectory then it will assume that something has gone wrong
//(e.g. the planner has died) and that it is no-longer receiving
//trajectories - when this occurs it will set the accel cmd to -5
//to command a hard-stop
#define NUM_TRAJ_POINTS_TO_END_OF_TRAJ_TO_START_STOPPING  100

enum EYerrorside {YERR_FRONT, YERR_BACK};
enum EAerrorside {AERR_FRONT, AERR_BACK, AERR_YAW};

class CPID_Controller 
{
	// the PID controllers
	Cpid* m_pSpeedPID;
  Cpid* m_pLateralPID;
 
  // the reference trajectory we're following
  CTraj m_traj;

	// these are the indices into the desired Yerror and Aerror arrays
	int m_YerrorIndex, m_AerrorIndex;

	// whether we're feeding back on yaw error
	bool m_AerrorYaw;

	// whether we want to use the lateral feed forward term
	bool m_bUseLateralFF;

	// m_refIndex stores the index of the closest (spatially) point on the
	// reference traj (reference traj is REAR-based) to the rear axle of our
	// vehicle.
  int m_refIndex;

	// These contain the state of the front and rear axles of the VEHICLE when
	// asked for control inputs. Angle is either the direction the point in
	// question is moving (if front or rear) or the yaw. Thus arg(angle) at the
	// back is yaw and arg(angle) at the front is yaw+steeringangle
	double m_vehN[2];
	double m_vehE[2];
	double m_vehDN[2];
	double m_vehDE[2];
	double m_vehAngle[2];
	double m_vehYaw;
	double m_vehPhi;
	double m_yawdot;
        
        //The timestamp for the current state-struct being used by the follower
        double trajF_current_state_Timestamp;

	// These contain the state of the front and rear axles of the REFERENCE when
	// asked for control inputs. Angle is either the direction the point in
	// question is moving (if front or rear) or the yaw. Thus arg(angle) at the
	// back is yaw and arg(angle) at the front is yaw+steeringangle
	double m_refN[2];
	double m_refE[2];
	double m_refDN[2];
	double m_refDE[2];
	double m_refDDN_REAR;
	double m_refDDE_REAR;
	double m_refAngle[2];


  double m_Yerror[2];   // y-error (meters)
  double m_Aerror[2];   // heading-error (rad)
  double m_Cerror;      // combined-error
  double m_Verror;      // speed-error (m/s)

  double m_steerCmd;    // steering command (rad)
  double m_accelCmd;    // acceleration command (m/s^2)
  double m_oldSteerCmd; // previous steering command (rad)

  // Feedback and feedforward terms for lateral and speed control
  double m_SteerFB; // steering control signal from feedback component of controller (rad)
  double m_SteerFF; // steering control signal from feedforward component of controller (rad)

  double m_AccelFB; // acceleration control signal from feedback component of controller (m/s^2)
  double m_AccelFF; // acceleration control signal from feedforward component of controller (m/s^2)

  // the reference and actual speeds
	double m_refSpeed;
	double m_trajSpeed;
	double m_actSpeed;

	// random files
  //outputs file in format: yerror, aerror, cerror (adds line every time 
  //compute_CError is called)
  ofstream m_outputError;
  ofstream m_outputVelocity;
  //outputs file in format: n_act, e_act, index, n_des, e_des
  ofstream m_outputState2;
  ofstream m_outputVars;
  //outputs file in format: steerCmd, accelCmd
  ofstream m_outputCommands;
  //output file meant for testing purposes. it's content will change
  ofstream m_outputTest;
  ofstream m_outputSteps;
  ofstream m_outputPID;

  void setup_Output();

  /** Calculate the index of the point on the trajectory that is closest to 
   * some NEcoord.  This NEcoord is "frac" fraction away from the NEcoord of 
   * the rear of the vehicle to the NEcoord of the front of the vehicle, where 
   * frac can be any real value (negative and > 1 values are OK). This function 
   * returns the integer index of the closest point on the trajectory. This 
   * function formerly set the member variable m_refIndex, but no longer does 
   * so.  */
  int getClosestIndex(double frac);

  void compute_FrontAndRearState_veh(VehicleState* pState);
  void compute_FrontAndRearState_ref();


  void compute_YError();
  void compute_AError();
  void compute_CError();

  void compute_SteerCmd();
  void compute_SteerFF();
  
  /** This function computes a simple steering feedforward term equal to the 
   * Yaw error.  The principle is that you want your steering wheels to point 
   * in the direction of the path you want to follow.  It does not set any 
   * member variables, but rather returns a steering FF value.*/
  double compute_SteerFF_simple();

  /** Function to compute a commanded steering by using a simple feedforward 
   * term that steers the wheels in the direction of the trajectory and a 
   * simple nonlinear feedback term on y_error.  It does not return a value, 
   * but rather sets the member variables associated with steering command. */
  void compute_SteerCmd_simple();

  /** Function to calculate the steering command by pure pursuit. This means 
   * that the steering commanded is that which points the steering wheels ahead 
   * on the trajectory by some lookahead distance from the front axle. */
  void compute_SteerCmd_purepursuit(double lookahead);
    
  double DFE(double cmd, double old);


  void compute_trajSpeed();
  void compute_actSpeed();
  void compute_refSpeed();

  void compute_AccFF();


public:
  CPID_Controller(EYerrorside yerrorside = YERR_FRONT,
									EAerrorside aerrorside = AERR_YAW,
									bool bUseLateralFF = true);

  ~CPID_Controller();

  // set control values
  void getControlVariables(VehicleState *state, double phicurrent, double *accel, double *phi);

  // sets new path
  void SetNewPath(CTraj* path);

  /** Use this to check the internal variable corresponding to error in heading */
  double access_AError()    { return m_Aerror[m_AerrorIndex]; }

  /** Use this to check the internal variable corresponding to perpendicular error from path */
  double access_YError()    { return m_Yerror[m_YerrorIndex]; }


  //NOTE: This code is not ideal, as the function declaration is actually in this file, which is the
  //header file, however there are issues with having these functions accessible
  //to sparrow (which uses C) - hence this appears to be the best option.

  //Use this to check the internal variable corresponding to a double which represents whether the
  //FRONT or the REAR axle (or some other reference position) are being used to calculate the Y-ERROR
  //in the lateral feed-back component of the controller.
  //OUTPUT: double corresponding to the factor multiplying the wheelbase (which is then added to the
  //center of the REAR axle (distance always taken along the line joining the center of the two axles
  //together; HENCE 0 = REAR AXLE, 1 = FRONT AXLE & 0.5 = HALF-WAY between the axles

  double access_YErrorRefPos() {

    //m_YerrorIndex variable - inverted before being output so that the y-error reference
    //positions are: 0 = REAR & 1 = FRONT (i.e. the SAME as the feed-forward lookahead in that they
    //represesent a distance in units of wheelbase along the line of the vehicle FROM the REAR AXLE
    if (m_YerrorIndex==1) {
    
      //in the OLD representation this corresponds to the REAR, in the NEW representation it correponds
      //to the FRONT, hence invert, i.e. make = ZERO before returning
      return double(m_YerrorIndex - 1);

    }
    if (m_YerrorIndex==0) {
 
      //in the OLD representation this corresponds to the FRONT, in the NEW representation it corresponds
      //to the REAR, hence invert, i.e. make = ONE before returning
      return double(m_YerrorIndex + 1);

    }

  }

  //Use this to check the internal variable corresponding to a double which represents whether the
  //FRONT or the REAR axle (or some other reference position) are being used to calculate the A-ERROR
  //in the lateral feed-back component of the controller.
  //OUTPUT: double corresponding to the factor multiplying the wheelbase (which is then added to the
  //center of the REAR axle (distance always taken along the line joining the center of the two axles
  //together; HENCE 0 = REAR AXLE, 1 = FRONT AXLE & 0.5 = HALF-WAY between the axles

  double access_AErrorRefPos() { 

    //m_AerrorIndex variable - inverted before being output so that the a-error reference
    //positions are: 0 = REAR & 1 = FRONT (i.e. the SAME as the feed-forward lookahead in that they
    //represesent a distance in units of wheelbase along the line of the vehicle FROM the REAR AXLE
    if (m_AerrorIndex==1) {
    
      //in the OLD representation this corresponds to the REAR, in the NEW representation it correponds
      //to the FRONT, hence invert, i.e. make = ZERO before returning
      return double(m_AerrorIndex - 1);

    }
    if (m_AerrorIndex==0) {
 
      //in the OLD representation this corresponds to the FRONT, in the NEW representation it corresponds
      //to the REAR, hence invert, i.e. make = ONE before returning
      return double(m_AerrorIndex + 1);

    }

  }

  /** Use this to check the internal variable corresponding to a double which represents whether the
      FRONT or the REAR axle (or some other reference position) are being used to calculate the
      lateral FEED-FORWARD component of the controller
      OUTPUT: double corresponding to the factor multiplying the wheelbase (which is then added to the
      center of the REAR axle (distance always taken along the line joining the center of the two axles
      together; HENCE 0 = REAR AXLE, 1 = FRONT AXLE & 0.5 = HALF-WAY between the axles
  **/
  double access_AFFRefPos() { return double(LOOKAHEAD_FF_LAT_REFPOS); }


  /** Use this to check the internal variable corresponding to combined error.  */
  double access_CError()    { return m_Cerror; }

  /** Use this to check the internal variable corresponding to speed error. */
  double access_VError()    { return m_Verror; }

  double access_AccelFF()   { return m_AccelFF;}
  double access_AccelFB()   { return m_AccelFB;}
  double access_AccelCmd()  { return m_accelCmd;}
  
  double access_SteerFF()   { return m_SteerFF;}
  double access_SteerFB()   { return m_SteerFB;}
  double access_SteerCmd()  { return m_steerCmd;}

  double access_VRef()      { return m_refSpeed; }
  double access_VTraj()     { return m_trajSpeed; }

  double access_Timestamp() { return trajF_current_state_Timestamp; }

};


#endif
