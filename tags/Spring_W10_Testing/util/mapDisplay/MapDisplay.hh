#ifndef __MAP_DISPLAY_HH__
#define __MAP_DISPLAY_HH__


#include <GL/gl.h>
#include <GL/glu.h>

#include <gtkmm.h>
#include <gtkglmm.h>

#include "OpenGLWidget.hh"

#include <iostream>
#include <math.h>

#include "CMap.hh"


#include "MapConfig.hh"


using namespace std;


// Forward declaration and prototypes

class CMap;
void CMapToPixbuf(MapWidget * cmap_widget);




// CLASS MAP_WIDGET ----------------------------------------

class MapWidget : public Gtk::HBox {

protected:
  // The map config class
  MapConfig                       * m_config;
  sigc::signal1<void, MapWidget*>   m_cmap_to_pixbuf;

protected: // GTK Display Stuff
  
  // For displaying the CMap
  FramedOpenGLWidget         m_cmap_draw_area;
  Glib::RefPtr<Gdk::Pixbuf>  m_cmap_pixbuf;
  Gtk::Label                 m_label_cmap_info;

  // For displaying the Colormap Legend
  FramedOpenGLWidget         m_colormap_draw_area;
  Glib::RefPtr<Gdk::Pixbuf>  m_colormap_pixbuf;
  Gtk::Label                 m_label_colormap_maxvalue;
  Gtk::Label                 m_label_colormap_midvalue;
  Gtk::Label                 m_label_colormap_minvalue;

  // Color Chart Options 
  Gtk::Frame                 m_frame_colormap_options;
  Gtk::VBox                  m_vbox_colormap_options;
  Gtk::Label                 m_label_colormap_center;
  Gtk::SpinButton            m_spin_colormap_center;
  Gtk::Label                 m_label_colormap_range;
  Gtk::SpinButton            m_spin_colormap_range;
  Gtk::CheckButton           m_check_colormap_log_scale;
  Gtk::CheckButton           m_check_colormap_auto_range;
  Gtk::Label                 m_label_colormap_min_range;
  Gtk::SpinButton            m_spin_colormap_min_range;
  Gtk::CheckButton           m_check_colormap_auto_center;

  // CMap Options
  Gtk::Frame                 m_frame_cmap_options;
  Gtk::VBox                  m_vbox_cmap_options;

  Gtk::Frame                 m_frame_cmap;
  Gtk::VBox                  m_vbox_cmap;
  Gtk::CheckButton           m_check_draw_cmap;
  Gtk::Label                 m_label_cmap_layer;
  Gtk::SpinButton            m_spin_cmap_layer;
  Gtk::CheckButton           m_check_draw_cmap_border;
  Gtk::Button                m_button_clear_cmap;

  Gtk::CheckButton           m_check_draw_gridlines;
  Gtk::CheckButton           m_check_draw_sensors;
  Gtk::CheckButton           m_check_draw_paths;
  Gtk::CheckButton           m_check_draw_rddf;
  Gtk::CheckButton           m_check_draw_waypoints;
  Gtk::CheckButton           m_check_draw_info;
  Gtk::Label                 m_label_zoom_scale;
  Gtk::SpinButton            m_spin_zoom_scale;
  Gtk::Label                 m_label_gridlines_spacing;

  // Container Widgets
  Gtk::VBox                  m_options_box;
  Gtk::VBox                  m_colormap_box;
  Gtk::VBox                  m_cmap_box;

  // Map data 
  bool m_use_anchor;

  // Vars for rendering text
  GLuint                     m_Font12ListBase;
  int                        m_Font12Height;
  double                     m_Font12HalfHeight;

  GLuint                     m_Font36ListBase;
  int                        m_Font36Height;
  double                     m_Font36HalfHeight;

  // Copy of important frames and maps
  RECTANGLE_MAP<double, double> map_utm_to_screen;
  RECTANGLE<int>                whitespace;

protected:
  bool   _gdk_event(GdkEventButton * event)
  { 
    notify_update();
    return false;
  }

  // Functions for redrawing the CMap
  void   _cmap_init();
  bool   _cmap_button(int x, int y, int button, int type)
  {
    //    cout << "Button Press Event " << endl;

    if( button == 3 ) 
      {
	m_config->m_drop_anchor = false;
      }
    //else if( button == 1 && (state & GDK_SHIFT_MASK)) 
    else if(button==1 && (type == GDK_2BUTTON_PRESS))
      {
	m_config->m_anchor_pos = map_utm_to_screen.reverse_map_point(POINT2D<double>( x+whitespace.left, y+whitespace.bottom ) );
	m_config->m_drop_anchor = true;
      }
    else if( button == 1 )
      {
	cout << setprecision(10);
	UTM_POINT tempPoint = map_utm_to_screen.reverse_map_point(POINT2D<double>( x+whitespace.left, y+whitespace.bottom ) );
	double cellVal = get_cmap()->getDataUTM<double>(get_layer_num(), tempPoint.N, tempPoint.E);
	char noteBuffer[20];
	if(cellVal==get_cmap()->getLayerNoDataVal<double>(get_layer_num())) {
	  snprintf(noteBuffer, 20, "No Data");
	} else if (cellVal==get_cmap()->getLayerOutsideMapVal<double>(get_layer_num())) {
	  snprintf(noteBuffer, 20, "Outside Map");
	} else {
	  snprintf(noteBuffer, 20, "Real Data");
	}
	char infoBuffer[256];
	snprintf(infoBuffer, 256, "(%.10g, %.10g) = %.10g (%s)", tempPoint.N, tempPoint.E, cellVal, noteBuffer);
	m_label_cmap_info.set_text(infoBuffer);
      }
    return false;
  }
  void   _cmap_reshape(int w, int h);
  void   _cmap_draw(int width, int height);
  void   _colormap_init();
  bool   _colormap_button( int , int , int , int) 
  {
    return false;
  }
  void   _colormap_reshape(int w, int h);
  void   _colormap_draw(int width, int height);
  void   _clear_current_layer() ;

public: // Structors
  MapWidget(MapConfig * mc);
  ~MapWidget() {}
  
public:

  void notify_cmap_pointer_change();

  void notify_update();

public: // Accessors

  CMap *                     get_cmap()               { return m_config->get_cmap(); }
  Glib::RefPtr<Gdk::Pixbuf>  get_pixbuf()             { return m_cmap_pixbuf; }
  Glib::RefPtr<Gdk::Pixbuf>  get_colormap()           { return m_colormap_pixbuf; }
  int                        get_width()              { return m_cmap_pixbuf->get_width(); }
  int                        get_height()             { return m_cmap_pixbuf->get_height(); }
  double                     get_no_data_val() 
  { 
    if( m_config->get_cmap() ) return m_config->get_cmap()->getLayerNoDataVal<double>( m_spin_cmap_layer.get_value_as_int() );
    else return 0;
  }

  // Colormap options and values
  bool                       get_colormap_use_log_scale()      { return m_check_colormap_log_scale.get_active(); }
  bool                       get_colormap_use_auto_range()     { return m_check_colormap_auto_range.get_active(); }
  bool                       get_colormap_use_auto_center()    { return m_check_colormap_auto_center.get_active(); }
  double                     get_colormap_center()             { return m_spin_colormap_center.get_value(); }
  void                       set_colormap_center(double d)     {        m_spin_colormap_center.set_value(d); }
  double                     get_colormap_range()              { return m_spin_colormap_range.get_value(); }
  void                       set_colormap_range(double d)      {        m_spin_colormap_range.set_value(d); }
  double                     get_colormap_min_range()          { return m_spin_colormap_min_range.get_value(); }
  void                       set_colormap_min_range(double d)  {        m_spin_colormap_min_range.set_value(d); }
  int                        get_layer_num()                   { return m_spin_cmap_layer.get_value_as_int(); }
  MapConfig *                get_map_config()                  { return m_config; }
};






class MapDisplayThread
{
protected:
  Glib::ustring                  m_APP_NAME;
  MapConfig                   *  m_map_config;
  Glib::Thread                *  m_thread; 

public:

  MapConfig                   *  get_map_config()
  {
    return m_map_config;
  }

  MapDisplayThread(Glib::ustring app_name, MapConfig * mc)
    : m_APP_NAME(app_name), m_map_config(mc)
  {  

  }
protected:
  void main() {

    cout << "Starting Map Thread" << endl;

    Gtk::Main kit(0, NULL);
    Gtk::GL::init(0, NULL);  

    Gtk::Window        m_window;
    MapWidget          m_MapWidget(m_map_config);

    m_window.set_title(m_APP_NAME);  
    m_window.add(m_MapWidget);  
    //m_window.resize(640, 480); //width, height
    m_window.show_all();

    cout << "About to run application" << endl;

    kit.run(m_window);
  }
public:
  void start_thread() 
  {
    cout << "Initializing glib threads" << endl;

    Glib::thread_init();

    cout << "Spawning Map Thread" << endl;

    m_thread = Glib::Thread::create( sigc::mem_fun(*this, & MapDisplayThread::main), false);

   
  }
  void notify_update()
  {
    if( m_map_config ) m_map_config->notify_update();
  }
};

#endif
