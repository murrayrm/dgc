#ifndef SKYNETD_H
#define SKYNETD_H

#include <stddef.h>
#include "sn_types.h"
#include <sys/time.h>

#define MODCOM_BUFSIZE  1024
#define BACKLOG 20
#define RMULTIIN_TIMEOUT 500000 //microseconds

/* store modules in a doubly linked list.  sort the list by location,
 * increasing.  If we need to scale up, pick a better data type.  a hash table
 * would be good if there are many module types.
 */

#define ULLONG_MAX  18446744073709551615ULL //not defined in c without C99

//#define NUM_MODS 20
#define MAX_HOSTNAME_LEN  35
#define FALSE             0
#define TRUE              1
struct sn_module {
  uint32_t    id;
  uint32_t    location;     /* ip address of computer the module is on */
  modulename  name;
  struct      sn_module* prev;
  struct      sn_module* next;
} ;

/* every socket needs a purpose so skynetd knows what to do with it */
typedef enum {global_var, listening, module_com, rmulti_listen, rmultiin, rmultiout, large_msg, chirp_modlist_type } sn_sock_type;


/* store a list of all open sockets.  An array has very fast access, but hard
 * to expand or add elements to.  Use the fact that the kernel assigns sockets
 * starting at 0 and counting up.  For now, use an array where the index is the
 * socket.  Later, maybe use a binary tree when skynet has many sockets to look
 * up?
 */


/* reliable multicast stuff */
typedef struct {
  short int present;                    //temporary
  void* data;
  ssize_t datalen;
  sn_msg  msg_type;
  uint32_t recip_list[NUM_RECIPS]; //list of module ids.  Must initialize to 0
  short int got_list[NUM_RECIPS];
  uint32_t msg_id;                 //unique code for all rmultimsg
} rmultimsgo;

/* Linked list of incoming rmulti buffers */
struct rmultimsgi {
  short int present;                    //temporary
  void* data;
  sn_msg msg_type;
  ssize_t datalen;
  int* pkt_list;              
  int num_pkts;
  uint32_t msg_id;                 //unique code for all rmultimsg
  long long int last_time;         //timestamp of last activity
  struct rmultimsgi* prev;
  struct rmultimsgi* next;
} ;

#define NUM_SOCKS 50
typedef struct {
  sn_sock_type  type;
  char          present;
  unsigned long long checktime;    /*do something at this time.  0 means never*/
  union{
    struct sn_module* module; /* relevant for types module_com and large_msg*/
    rmultimsgo* rmultimsgo; 
    struct rmultimsgi* rmultimsgi;
        /* any data relevant for only global_var goes here */
    } data;
  sn_msg  sn_msg_type;      /* relevant for large message sockets */
} sn_sock;

typedef struct {                  //normally in network byte order
  int msg_id;
  int pkt;
  int num_pkts;
} rpkthdr; 

int insert_reliable(int s, modcom_hdr* hdr);

int remove_rmultiin(struct rmultimsgi* rmultibuf);

int insert_rmultiin(int s, rpkthdr* hdr, struct rmultimsgi** rmultibuf_ptr);

int process_rmultiin(int s, struct rmultimsgi* rmulmsgbufi);

int handle_rmultiin(int s);     /*receives and reassembles large messages*/

int handle_rmultiout(int s);    /*fragments and sends large messages*/

int dispatch_rmultiin(struct rmultimsgi* rmulmsgbufi);

/* drop_rmultiin should be run periodically.  It will purge all rmultiin
 * buffers that have been inactive
 */
void drop_rmultiin();

/*end reliable multicast stuff */

void chirp_modlist();

//int mk_modcom_sock();

void init_data(int argc, char** argv);

int accept_module_com(int s);

int handle_module_com(int s);

int remove_module(int s);

int remove_sock(int s);

int mk_lsock(int s, modcom_hdr* hdr);

void handle_register(int s, modcom_hdr* hdr);

//void recv_quant(int s, void* buf, ssize_t len, int flags);

//void send_quant(int s, void* buf, ssize_t len, int flags);

uint32_t get_my_ip();

void set_checktime(int sock, unsigned int usec);

int dump_sock_list();

#endif
