SKYNET_PATH = $(DGC)/util/skynet

SKYNET_DEPEND = \
	$(SKYNET_PATH)/large_test.cc \
	$(SKYNET_PATH)/skynetd.c \
	$(SKYNET_PATH)/skynetd.h \
	$(SKYNET_PATH)/skynetd_main.c \
	$(SKYNET_PATH)/skynetd_static.c \
	$(SKYNET_PATH)/skynetd_static.h \
	$(SKYNET_PATH)/sn_get_reliable.cc \
	$(SKYNET_PATH)/sn_get_test.c \
	$(SKYNET_PATH)/sn_get_test.cc \
	$(SKYNET_PATH)/sn_global.h \
	$(SKYNET_PATH)/sn_msg.c \
	$(SKYNET_PATH)/sn_msg.cc \
	$(SKYNET_PATH)/sn_msg.h \
	$(SKYNET_PATH)/sn_msg.hh \
	$(SKYNET_PATH)/sn_send_reliable.cc \
	$(SKYNET_PATH)/sn_send_test.c \
	$(SKYNET_PATH)/sn_send_test.cc \
	$(SKYNET_PATH)/sn_types.h \
	$(SKYNET_PATH)/sn_utils.c
