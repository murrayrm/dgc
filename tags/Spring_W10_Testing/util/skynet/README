USAGE INSTRUCTIONS:
http://gc.caltech.edu/wiki/index.php/Component:_Embedded_skynet
 
FILES:
   
sn_types.h
sn_utils.c

  These files contain code needed by both skynetd and modules.

sn_msg.h
sn_msg.c
sn_global.h

  These files contain the c implementation of messaging.  It is currently broken, but can be fixed.

sn_msg.hh
sn_msg.cc

  These files contain the c++ implementation of messaging.  If you are a module writer, this is all you care about.

skynetd.c
skynetd_main.c
skynetd.h
skynetd_static.c

  These are the code for skynetd.

sn_get_test.cc
sn_send_test.cc
sn_get_reliable.cc
large_test.cc

  These are test and example code

Makefile
env-setup.sh

  env-setup.sh modifies your shell startup files to define $SKYNET_KEY

NOTES ON FUTURE CODING:

* for large message, semi-reliable multicast is neccessary.  That is, break a
message into multiple fragments.  If a client gets at least one fragment,
then it can rerequest missing fragments.  But if a client never gets any
fragment, it's in the dark.

* to synchronize skynetd among different computers, some kind of reliable
multicast would be neccessary.  So if 1 skynetd receives a registration on the
local computer, it needs to tell other skynetd's about it.  This would have to
be totally reliable.  On the other hand, we could simply have every skynetd
"chirp" a couple hundred bytes once per second that would be enough to encode
it's status - all module registrations at least.  This would work well for
registration, but not global variables.  Global variables must be implemented
either by totally reliable multicast, by an n^2 web of tcp connections, or
something more complicated: 

* each skynetd maintains a table of all global variables, their sizes, and last
update times.  It has one table for all global variables, and one for global
variables written by a local module.  It chirps the latter once per second, and
any skynetd that listens to the chirp compares that table to it's own master
table.  If it's own table is behind on any timestamp, it requests a
transmission from the skynetd that sent the chirp.

The static arrays I used to store data about sockets and modules are presently:
* not expandable
* order n to search through, where n is the number of entries
* order m in size, where m is the highest numbered module id in the cluster
* it is not easy to distinguish whether a module is local vs remote
* sorted
* if two modules register simultaneously on separate computers, they will get
  the same id.

To solve the last problem, I think we should use a formula for module ids where
id = (Last 8 bits of host's ip)*256 + a sequential number.  However, this would
horribly bloat the table size.  A sorted linked list would be optimal in size,
order n to search, and order n in size.  A sorted binary tree would be order
log(n) to search, and order n in size.  I'm not sure whether it's worth it to
make the binary trees.  This list would have to be doubly linked for speedy
insertions.
