/**********************************************************************
 **********************************************************************
 * +FILE:        rddf.hh 
 *    
 * +DESCRIPTION: class to read the rddf file and return the waypoints
 *
 * +AUTHOR     : Thyago Consort 
 **********************************************************************
 *********************************************************************/

#ifndef __RDDF_HH__
#define __RDDF_HH__

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>
#include "ggis.h"
#include "GlobalConstants.h" // for RDDF_FILE and unit conversions
#include "frames/coords.hh" // for NEcoord

#define RDDF_DELIMITER ','
#define BAD_BOB_FILE -1
// Tack an additional waypoint onto the end with slow speed.
// This is a hack so that planner will behave.  Talk to Dima.
#define TACK_ONE_ON

using namespace std;


/** Waypoints are defined by the RDDF, and translated to a vector of RDDFData 
  structs when RDDF::loadFile() is called.  Even though the RDDF is defined in 
  terms of latitude, longitude, mph, and feed, SI units are used from this 
  point on in all of our code. */
typedef struct RDDFData
{
  /** number of the waypoint.  The corridor segment defined by waypoint i goes 
    from waypoint i to waypoint i+1.  DARPA RDDFs are indexed from waypoint 1, 
    which corresponds to index 0 in our code. */
  int number;

  /** Northing (in meters, UTM zone 11 S) of waypoint <number>. */
  double Northing;
  /** Easting (in meters, UTM zone 11 S) of waypoint <number>. */
  double Easting;

  /** speed limit (in m/s) for the given corridor segment. */
  double maxSpeed;  
  /** boundary offset (in meters) of the corridor to the next waypoint. */  
  double offset;    
  /** radius (in meters) is actually identical to offset, and defines the 
    boundaries of a given corridor segment.  Each race vehicle must stay within 
    the union of all of the corridor segments. */
  double radius;

  /** phase line - hour.  Not used for 2004 race, although kept in the RDDF 
    specification provided by DARPA. */
  double hour;
  /** phase line - minute.  Not used for 2004 race, although kept in the RDDF 
    specification provided by DARPA. */
  double min;
  /** phase line - second.  Not used for 2004 race, although kept in the RDDF 
    specification provided by DARPA. */
  double sec;

  /** Display function. */
  void display()
  {
    printf("N=%d North=%10.3f East=%10.3f VEL=%.3f Off= %.2f R=%.2f hour=%d min=%d sec=%d\n",
		    number,Northing,Easting,maxSpeed,offset,radius,(int)hour,(int)min,(int)sec);
  }
 
  /** Accessor function to get an NEcoord format of a given waypoint. */
  NEcoord ne_coord() 
  {
    NEcoord ret;
    ret.N = Northing;
    ret.E = Easting;
    return ret;
  }
};

/** RDDFVector is a standard template vector of waypoint data defined by 
  RDDFData. */
typedef std::vector<RDDFData> RDDFVector;

class RDDF 
{
private:

  int numTargetPoints;
  RDDFVector targetPoints;

public:

  // Constructors
  RDDF();
  RDDF(char*pFileName);
  RDDF(const RDDF &other) 
  {
    numTargetPoints = other.getNumTargetPoints(); 
    targetPoints = other.getTargetPoints(); 
  }
  
  // Destructor
  ~RDDF();

  RDDF &operator=(const RDDF &other);

  // Accessors

  /** Accessor function to get the number of waypoints. */
  int getNumTargetPoints() const { return numTargetPoints; }

  /** Accessor function to get a vector of all the waypoints.  RDDFVector is a 
   * standard template vector of RDDFData structs, which contain all essential 
   * data for a waypoint (number, Northing, Easting, maxSpeed, offset). */
  RDDFVector getTargetPoints() const { return targetPoints; }

  /** Return waypoint as NEcoord. */
  NEcoord getWaypoint(int waypointNum)
  {
    return targetPoints[waypointNum].ne_coord();
  };    

  /** Get the Northing value of a given waypoint. */
  double getWaypointNorthing(int waypointNum) 
  { 
    return targetPoints[waypointNum].Northing; 
  };

  /** Get the Easting value of a given waypoint. */ 
  double getWaypointEasting(int waypointNum) 
  { 
    return targetPoints[waypointNum].Easting;
  };


  double getWaypointSpeed(int waypointNum)
  {
    return targetPoints[waypointNum].maxSpeed;
  }

  /** Load data from file (RDDF format file). */
  int loadFile(char*);

  /** Get the waypoint number for the given location, i.e. the index of the 
    corridor segment that contains the point.  Note that corridor segment i is 
    the segment between waypoints i and i+1.  If the point is not within the 
    corridor (the sum of corridor segments), then this function returns the 
    index of the closest waypoint. */
  int getCurrentWaypointNumber( NEcoord& pos );
  int getCurrentWaypointNumber( double Northing, double Easting );

  /** Get the number of the next waypoint that we will pass. */
  int getNextWaypointNumber( NEcoord& pos );
  int getNextWaypointNumber( double Northing, double Easting );

  /** Get the angle (in radians) from the specified waypoint to the next, 
   * defined as positive clockwise from north.  Waypoints are indexed from 0.  
   * */
  double getTrackLineYaw( int i );
  
  /** Get the distance (in meters) from the specified waypoint to the next. */
  double getTrackLineDist( int i );

  /** Boolean function to return whether a given point is inside a specified 
   * corridor segment (the maximal rectangle or the circles of waypoint i's 
   * radius at either end).  Used by getCurrentWaypointNumber and other fine 
   * functions. */
  int isPointInCorridor(int waypointNum, double Northing, double Easting);

  /** Gets the location along the trackline a given distance away from the 
   * specified point.  This function projects the position to the current 
   * waypoint's trackline, and then traces forward along the trackline from 
   * there by the specified distance.  It returns this coordinate.
	 * Also, if thetaAtTarget isn't NULL, it'll get filled in with the
	 * orientation the vehicle would have if it was at the returned XYcoord,
	 * facing down the trackline. */
  NEcoord getPointAlongTrackLine( NEcoord& pos, double distance,
    double* thetaAtTarget = NULL, double* corRad = NULL );
	NEcoord getPointAlongTrackLine( double n, double e, double distance,
    double* thetaAtTarget = NULL, double* corRad = NULL );

  /** Save the file at the bob format (returns 0 if OK) */
  int createBobFile(char* fileName);

  /** Returns the lateral distance from the given point to the nearest 
   * trackline segment */
  double getDistToTrackline(NEcoord pointCoords);

  /** Returns the ratio of (distance from given point to the nearest trackline
   * segment) over (lateral boundary offset, i.e. the width of the corridor
   * in that segment of the RDDF) */
  double getRatioToTrackline(NEcoord pointCoords);

  /** Ask Jeremy Gillula what the heck this function does (and put that 
    information here). */
  int getCorridorSegmentBoundingBoxUTM(int waypointNum, 
				       double winBottomLeftUTMNorthing, double winBottomLeftUTMEasting,
				       double winTopRightUTMNorthing, double winTopRightUTMEasting,
				       double& BottomLeftUTMNorthing, double& BottomLeftUTMEasting, 
				       double& TopRightUTMNorthing, double& TopRightUTMEasting);

  /** Ask Jeremy Gillula what the heck this function does (and put that 
    information here). */
  int getWaypointNumAheadDist(double UTMNorthing, double UTMEasting, 
                              double distance);

  /** Ask Jeremy Gillula what the heck this function does (and put that 
    information here). */
  int getWaypointNumBehindDist(double UTMNorthing, double UTMEasting, 
                               double distance);

};

#endif //__RDDF_HH__
