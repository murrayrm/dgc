#ifndef _PLANNER_H_
#define _PLANNER_H_

#include "VehicleState.hh"
#include "rddf.hh"
#include "CMap.hh"
#include "traj.h"
#include "SpacialStage.h"
#include "RefinementStage.h"

/* #define RTP_VPROFILE_DISPLAY */

#ifdef RTP_VPROFILE_DISPLAY
#include "pstream.h"
#endif

class CPlanner
{
	CSpacialStage			m_spacialStage;
	CRefinementStage	m_refinementStage;

#ifndef USE_NPSOL
	SSNOPTContext			m_SNOPTContext;
	SSNOPTContext			m_SNOPTContext2;
#endif

	ofstream					m_interm;
	ofstream					m_after;

#ifdef RTP_VPROFILE_DISPLAY
	redi::pstream     m_rtpPipe;
#endif

public:
	CPlanner(CMap *pMap,  int mapLayerID,
					 RDDF* pRDDF, bool USE_MAXSPEED = false,
					 bool bGetVProfile = false);
	~CPlanner();

	int plan(VehicleState *pState);
	double getVProfile(CTraj* pTraj);

	CTraj* getTraj(void);
	CTraj* getSeedTraj(void);
	CTraj* getIntermTraj(void);

	double getLength(void);
};

#endif // _PLANNER_H_
