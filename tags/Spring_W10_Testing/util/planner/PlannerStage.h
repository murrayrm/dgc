#ifndef _PLANNERSTAGE_H_
#define _PLANNERSTAGE_H_

#include "VehicleState.hh"
#include "rddf.hh"
#include "CMap.hh"
#include "traj.h"
#include "defs.h"

extern "C" void dgemv_(char*, int*, int*, double*, double*, int*, double*, int*, double*, double*, int*);
extern "C" void dgemm_(char*, char*, int*, int*, int*, double*, double*, int*, double*, int*, double*, double*, int*);
extern "C" void dgesv_(int*, int* , double *, int* , int *, double *, int*, int*);
extern "C" double ddot_(int*, double*, int*, double*, int*);
extern "C" void dgesvd_(char*, char*, int*, int*, double*, int*, double*, double*, int*, double*, int*, double*, int*, int* );
extern "C" double dscal_(int*, double*, double*, int*);


#ifndef USE_NPSOL
struct SSNOPTContext
{
	char *cw;
	int *iw;
	double *rw;
	int lencw, leniw, lenrw;
};
#endif

class CPlannerStage
{
protected:
	RDDF*			m_pRDDF;
	CTraj*		m_pTraj;
	CMap*			m_pMap;
	int				m_mapLayer;
	int       m_blurLayer;

	// The order of the bounds is non-linear {init, traj, final}, linear {init,
	// traj, final}
	double*		m_solverLowerBounds;
	double*		m_solverUpperBounds;
	double*		m_PlannerLowerBounds;
	double*		m_PlannerUpperBounds;

	double *m_pSolverState;
	int inform;

#ifdef USE_NPSOL
	// Internal NPSOL crap
	int* istate;
	double* nonLinearConstrVals;
	double* cjac;
	double* clambda;
	double* gobj;
	double* Rmatrix;
	int leniw, lenw;
	int* iw;
	double* w;
#else
	// Internal SNOPT crap
	char start[8];
	int SNOPTm, SNOPTn;
	int ne;
	int nName;
	int nnobj, nncon, mincw, miniw, minrw;
	int nout, nnjac;
	int iobj, ninf;
	double objadd;
	char prob[8];
	double *m_SNOPTmatrix;
	int *ha, *ka;
	double *rc;
	double *pi;
	int *hs, ns;
	double obj, sinf;
	char *cw;
	int *iw;
	double *rw;
	int lencw, leniw, lenrw;

	int PRINT_LEVEL;
#endif

	// Everything in the computation is shifted by this many units. Used to move
	// the absolute values of the computation to reasonable values
	double		m_initN, m_initE;
	double		m_initTheta;
	double		m_initSpeed;
	double		m_targetN, m_targetE;
	double		m_targetTheta;

	double*		m_pColl_theta;
	double*		m_pColl_dtheta;
	double*		m_pColl_ddtheta;
	double*   m_pCollN;
	double*   m_pCollE;
	double*		m_pCollGradN;
	double*		m_pCollGradE;
	double*   m_pCollVlimit;
	double*   m_pCollDVlimitDN;
	double*   m_pCollDVlimitDE;
	double*   m_pCollDVlimitDTheta;

	double*   m_pCollGradVlimit;
	double    m_collSF;

	double**	m_grad_solver;

	double*		m_valcoeffs, *m_d1coeffs, *m_d2coeffs;
	double*		m_valcoeffsfine, *m_d1coeffsfine, *m_d2coeffsfine;
	double*		m_dercoeffsmatrix;
	double*   m_stateestimatematrix;
	double*		m_mapblurcoeffs;

	double*		m_splinecoeffsTheta;
	double*		m_splinecoeffsSpeed;

	double*   m_pOutputTheta;
	double*   m_pOutputDTheta;

	double*   m_pThetaVector;


	// the length of the SNOPT state vector
	int NPnumVariables;

	// numbers of constraints
	int	NUM_LIN_INIT_CONSTR, NUM_LIN_TRAJ_CONSTR, NUM_LIN_FINL_CONSTR,
		NUM_NLIN_INIT_CONSTR, NUM_NLIN_TRAJ_CONSTR, NUM_NLIN_FINL_CONSTR;

	// NUM_POINTS is the number of points in the spline
	// COLLOCATION_FACTOR is the number of collocation points in each spline segment
	// NUM_COLLOCATION_POINTS is the resulting total number of collocation points.
	int NUM_POINTS, COLLOCATION_FACTOR;

	char *m_pSpecsFile;
	bool USE_FINAL_ANGLE_CONSTRAINT;
	bool m_USE_MAXSPEED;

	double HALF_TARGET_ERROR;
	bool CARTESIAN_TARGET_SPECIFICATION;
	double HALF_TARGET_WINDOW;

	double PLANNING_TARGET_DIST;
	int NUM_IGNORED_CONSTRAINTS;
	int MAP_BLUR_KERNEL_DIAM;

	virtual void outputTraj(CTraj* pTraj) = 0;
	virtual void makeSolverBoundsArrays(void);
	virtual bool SetUpPlannerBoundsAndSeedCoefficients(double* pPrevState, VehicleState* pState) = 0;

	void getSplineCoeffsTheta(double* pSolverState);
	void integrateNE(int collIndex);
	void integrateNEfine(int collIndex, double& outN, double& outE);
	void initialize(char* pDercoeffsFile);
	void SetCollocFactor1(void);
	void SetCollocFactor2(void);
	void SetCollocVars(void);
	virtual void SetUpLinearConstr(void);
	virtual void SetUpPlannerBounds(VehicleState *pState);
	void CallSolver(void);

	double approxMin(double* f, double* dmin_df);
	void getContinuousMapValueDiffGrown(double zeroedNorthing, double zeroedEasting, double yaw,
																			double *f, double *dfdN, double *dfdE, double *dfdyaw);
	void getContinuousMapValueDiff(double UTMNorthing, double UTMEasting,
																 double *f, double *dfdN, double *dfdE);
	virtual void NonLinearFinlConstrFunc(void);
	bool SeedFromTraj(CTraj* pTraj, double dist = 0.0);
	void setTargetToDistAlongSpline(double distRatio);

public:
	CPlannerStage(CMap *pMap = NULL, int mapLayerID = 0, RDDF* pRDDF = NULL, bool USE_MAXSPEED = false);
	virtual ~CPlannerStage();

#ifndef USE_NPSOL
	void setContext(SSNOPTContext* pContext);
#endif

	int run(VehicleState* pState, double* pSolverState = NULL);
	void checkDerivatives();

	CTraj* getTraj(void)
	{
		return m_pTraj;
	}
	double* getSolverState(void)
	{
		return m_pSolverState;
	}
	double getObjective(void)
	{
		return m_obj;
	}

	virtual void funcConstr(void) = 0;
	virtual void funcCost  (void) = 0;
	double *m_pPrevSolverState;
	double*   m_pConstrData;
	double*   m_pConstrGradData;
	double    m_obj;
	double*   m_pCostGradData;
	virtual void makeCollocationData(double* pSolverState);
};

#endif // _PLANNERSTAGE_H_
