#include "PlannerStage.h"

// #define NEW_INTEGRATE_NE

// for simpler access
// NOTE: n,e here are not scaled by Sf
#define theta						m_pColl_theta  [collIndex]
#define dtheta					m_pColl_dtheta [collIndex]
#define ddtheta					m_pColl_ddtheta[collIndex]
#define sf							m_collSF
#define n								m_pCollN[collIndex]
#define e								m_pCollE[collIndex]
#define vlimit          m_pCollVlimit[collIndex]
#define dvldN           m_pCollDVlimitDN[collIndex]
#define dvldE           m_pCollDVlimitDE[collIndex]
#define dvldtheta       m_pCollDVlimitDTheta[collIndex]

#define DO_BENCHMARK

#ifdef DO_BENCHMARK
#include "DGCutils"
extern int numinterpblurs;
extern unsigned long long integrateNEtime;
extern unsigned long long blurtime;
#endif

void CPlannerStage::makeCollocationData(double* pSolverState)
{
	int i;

	getSplineCoeffsTheta(pSolverState);

	char notrans = 'N';
	int rows = NUM_COLLOCATION_POINTS;
	int cols = NUM_POINTS;
	double alpha = 1.0;
	double beta = 0.0;
	int incr = 1;

	// make theta, dtheta, ddtheta
#warning "do we need ALL the derivatives?"
	dgemv_(&notrans, &rows, &cols, &alpha, m_valcoeffs, &rows,
				 pSolverState, &incr, &beta, m_pColl_theta, &incr);
	dgemv_(&notrans, &rows, &cols, &alpha, m_d1coeffs, &rows,
				 pSolverState, &incr, &beta, m_pColl_dtheta, &incr);
	dgemv_(&notrans, &rows, &cols, &alpha, m_d2coeffs, &rows,
				 pSolverState, &incr, &beta, m_pColl_ddtheta, &incr);

	for(i=0; i<NUM_COLLOCATION_POINTS; i++)
		m_pColl_theta[i] += m_initTheta;


	m_collSF = pSolverState[NPnumVariables-1];

	// integrate through to get N,E and their gradients, as well as
	// vlimit(N,E) and its gradients. First, set up the variables for
	// the integration

	int collIndex = 0;

#warning "these should be hardcoded"
	n = e = 0.0;
	memset(m_pCollGradN, 0, NUM_POINTS*sizeof(m_pCollGradN[0]));
	memset(m_pCollGradE, 0, NUM_POINTS*sizeof(m_pCollGradE[0]));

	int gradVlimitIndex = 0;
	for(;
			collIndex<NUM_COLLOCATION_POINTS;
			collIndex++)
	{
		getContinuousMapValueDiffGrown(n*sf, e*sf, theta, &vlimit, &dvldN, &dvldE, &dvldtheta);

		for(i=0; i<NUM_POINTS; i++)
		{
			m_pCollGradVlimit[gradVlimitIndex++] =
				sf * (dvldN * m_pCollGradN[i + NUM_POINTS*collIndex] +
							dvldE * m_pCollGradE[i + NUM_POINTS*collIndex])+
				dvldtheta * m_valcoeffs[collIndex + i*NUM_COLLOCATION_POINTS];
		}
		m_pCollGradVlimit[gradVlimitIndex++] = dvldN*n + dvldE*e;

		// integrate the (N,E) values, unless we're already at the end
		if(collIndex != NUM_COLLOCATION_POINTS-1)
			integrateNE(collIndex);
	}
}

double CPlannerStage::approxMin(double* f, double* dmin_df)
{
	double val = 0.0;
	for(int i=0; i<OBSTACLE_AVOIDANCE_DIAM; i++)
	{
		val += f[i];
		dmin_df[i] = 1.0/(double)OBSTACLE_AVOIDANCE_DIAM;
	}
	return val/(double)OBSTACLE_AVOIDANCE_DIAM;


		
// 	double& f1 = f[0];
// 	double& f2 = f[1];
// 	double& f3 = f[2];

// 	double val =
// 		pow(APPROXMIN_BASE,2.0*APPROXMIN_EXPFACTOR*(f1+f2)) +
// 		pow(APPROXMIN_BASE,2.0*APPROXMIN_EXPFACTOR*(f1+f3)) +
// 		pow(APPROXMIN_BASE,2.0*APPROXMIN_EXPFACTOR*(f2+f3));
// 	double minval = f1 + f2 + f3 -
// 		1.0/(2.0*APPROXMIN_EXPFACTOR) * log(val) / log(APPROXMIN_BASE);


// 	dmin_df[0] = pow(APPROXMIN_BASE, 2.0*APPROXMIN_EXPFACTOR*(f2+f3)) / val;
// 	dmin_df[1] = pow(APPROXMIN_BASE, 2.0*APPROXMIN_EXPFACTOR*(f1+f3)) / val;
// 	dmin_df[2] = pow(APPROXMIN_BASE, 2.0*APPROXMIN_EXPFACTOR*(f1+f2)) / val;
// 	return minval;



// 	double small0 = 1e10;
// 	double small1 = 1e10;
// 	int    small0i, small1i;

// 	int i;

// 	for(i=0; i<OBSTACLE_AVOIDANCE_DIAM; i++)
// 	{
// 		if(f[i] < small0)
// 		{
// 			small1  = small0;
// 			small1i = small0i;
// 			small0  = f[i];
// 			small0i = i;
// 		}
// 		else if(f[i] < small1)
// 		{
// 			small1 = f[i];
// 			small1i= i;
// 		}
// 	}

// 	memset(dmin_df, 0, OBSTACLE_AVOIDANCE_DIAM*sizeof(double));
// 	double minval = (-log(pow(APPROXMIN_BASE, (f[small0i]-f[small1i])*APPROXMIN_EXPFACTOR) +
// 												pow(APPROXMIN_BASE, (f[small1i]-f[small0i])*APPROXMIN_EXPFACTOR)) / (log(APPROXMIN_BASE)*APPROXMIN_EXPFACTOR) +f[small0i]+f[small1i]) / 2.0;
// 	dmin_df[small0i] = 1.0 / (1.0 + pow(APPROXMIN_BASE, 2.0*APPROXMIN_EXPFACTOR*(f[small0i] - f[small1i])));
// 	dmin_df[small1i] = 1.0 / (1.0 + pow(APPROXMIN_BASE, 2.0*APPROXMIN_EXPFACTOR*(f[small1i] - f[small0i])));

// 	return minval;
}

// void CPlannerStage::getContinuousMapValueDiffGrown(double zeroedNorthing, double zeroedEasting, double yaw,
// 																									 double *fval, double *dfdN, double *dfdE, double *dfdyaw)
// {
// 	double f[OBSTACLE_AVOIDANCE_DIAM];
// 	double dfdNindividual[OBSTACLE_AVOIDANCE_DIAM];
// 	double dfdEindividual[OBSTACLE_AVOIDANCE_DIAM];
// 	double dmin_df[OBSTACLE_AVOIDANCE_DIAM];

// 	// r cycles through the radii away from the middle. pointindex cycles through
// 	// the point we're looking at. This is why the for loop below looks funny
// 	int r, pointindex = 0;

// 	getContinuousMapValueDiff(zeroedNorthing + m_initN,
// 														zeroedEasting  + m_initE,
// 														&f[pointindex], &dfdNindividual[pointindex], &dfdEindividual[pointindex]);

// 	pointindex++;
// 	for(r=1; pointindex<OBSTACLE_AVOIDANCE_DIAM; r++)
// 	{
// 		getContinuousMapValueDiff(zeroedNorthing + m_initN + ((double)r)*OBSTACLE_AVOIDANCE_DELTA*sin(yaw),
// 															zeroedEasting  + m_initE - ((double)r)*OBSTACLE_AVOIDANCE_DELTA*cos(yaw),
// 															&f[pointindex], &dfdNindividual[pointindex], &dfdEindividual[pointindex]);
// 		pointindex++;
// 		getContinuousMapValueDiff(zeroedNorthing + m_initN - ((double)r)*OBSTACLE_AVOIDANCE_DELTA*sin(yaw),
// 															zeroedEasting  + m_initE + ((double)r)*OBSTACLE_AVOIDANCE_DELTA*cos(yaw),
// 															&f[pointindex], &dfdNindividual[pointindex], &dfdEindividual[pointindex]);
// 		pointindex++;
// 	}

// 	*fval = approxMin(f, dmin_df);

// 	*dfdN = 0.0;
// 	*dfdE = 0.0;
// 	for(pointindex=0; pointindex<OBSTACLE_AVOIDANCE_DIAM; pointindex++)
// 	{
// 		*dfdN += dmin_df[pointindex]*dfdNindividual[pointindex];
// 		*dfdE += dmin_df[pointindex]*dfdEindividual[pointindex];
// 	}


// 	*dfdyaw = 0.0;
// 	pointindex = 1;
// 	for(r=1; pointindex<OBSTACLE_AVOIDANCE_DIAM; r++)
// 	{
// 		*dfdyaw += OBSTACLE_AVOIDANCE_DELTA * ((double)r) * (dmin_df[pointindex  ]* (dfdNindividual[pointindex  ]*cos(yaw) + dfdEindividual[pointindex  ]*sin(yaw)) -
// 																												 dmin_df[pointindex+1]* (dfdNindividual[pointindex+1]*cos(yaw) + dfdEindividual[pointindex+1]*sin(yaw)));
// 		pointindex += 2;
// 	}
// }

// void CPlannerStage::getContinuousMapValueDiffGrown(double zeroedNorthing, double zeroedEasting, double yaw,
// 																									 double *fval, double *dfdN, double *dfdE, double *dfdyaw)
// {
// 	double f[OBSTACLE_AVOIDANCE_DIAM];
// 	double dfdNindividual[OBSTACLE_AVOIDANCE_DIAM];
// 	double dfdEindividual[OBSTACLE_AVOIDANCE_DIAM];
// 	double dmin_df[OBSTACLE_AVOIDANCE_DIAM];

// 	// r cycles through the radii away from the middle. pointindex cycles through
// 	// the point we're looking at. This is why the for loop below looks funny
// 	int r, pointindex = 0;

// 	getContinuousMapValueDiff(zeroedNorthing + m_initN,
// 														zeroedEasting  + m_initE,
// 														&f[pointindex], &dfdNindividual[pointindex], &dfdEindividual[pointindex]);

// 	pointindex++;
// 	for(r=1; pointindex<OBSTACLE_AVOIDANCE_DIAM; r++)
// 	{
// 		getContinuousMapValueDiff(zeroedNorthing + m_initN + ((double)r)*OBSTACLE_AVOIDANCE_DELTA*sin(yaw),
// 															zeroedEasting  + m_initE - ((double)r)*OBSTACLE_AVOIDANCE_DELTA*cos(yaw),
// 															&f[pointindex], &dfdNindividual[pointindex], &dfdEindividual[pointindex]);
// 		pointindex++;
// 		getContinuousMapValueDiff(zeroedNorthing + m_initN - ((double)r)*OBSTACLE_AVOIDANCE_DELTA*sin(yaw),
// 															zeroedEasting  + m_initE + ((double)r)*OBSTACLE_AVOIDANCE_DELTA*cos(yaw),
// 															&f[pointindex], &dfdNindividual[pointindex], &dfdEindividual[pointindex]);
// 		pointindex++;
// 	}

// 	*fval = approxMin(f, dmin_df);

// 	*dfdN = 0.0;
// 	*dfdE = 0.0;
// 	for(pointindex=0; pointindex<OBSTACLE_AVOIDANCE_DIAM; pointindex++)
// 	{
// 		*dfdN += dmin_df[pointindex]*dfdNindividual[pointindex];
// 		*dfdE += dmin_df[pointindex]*dfdEindividual[pointindex];
// 	}


// 	*dfdyaw = 0.0;
// 	pointindex = 1;
// 	for(r=1; pointindex<OBSTACLE_AVOIDANCE_DIAM; r++)
// 	{
// 		*dfdyaw +=
// 			dmin_df[pointindex  ]*
// 			(dfdNindividual[pointindex  ]* ( cos(yaw)*OBSTACLE_AVOIDANCE_DELTA * ((double)r)) +
// 			 dfdEindividual[pointindex  ]* ( sin(yaw)*OBSTACLE_AVOIDANCE_DELTA * ((double)r)));
// 		*dfdyaw +=
// 			dmin_df[pointindex+1]*
// 			(dfdNindividual[pointindex+1]* (-cos(yaw)*OBSTACLE_AVOIDANCE_DELTA * ((double)r)) +
// 			 dfdEindividual[pointindex+1]* (-sin(yaw)*OBSTACLE_AVOIDANCE_DELTA * ((double)r)));

// 		pointindex += 2;
// 	}
// }

void CPlannerStage::getContinuousMapValueDiffGrown(double zeroedNorthing, double zeroedEasting, double yaw,
																									 double *fval, double *dfdN, double *dfdE, double *dfdyaw)
{
	double f[OBSTACLE_AVOIDANCE_DIAM];
	double dfdNindividual[OBSTACLE_AVOIDANCE_DIAM];
	double dfdEindividual[OBSTACLE_AVOIDANCE_DIAM];
	double dmin_df[OBSTACLE_AVOIDANCE_DIAM];

	// r cycles through the radii away from the middle. pointindex cycles through
	// the point we're looking at. This is why the for loop below looks funny
	int r, pointindex = 0;

	getContinuousMapValueDiff(zeroedNorthing + m_initN + VEHICLE_WHEELBASE*cos(yaw),
														zeroedEasting  + m_initE + VEHICLE_WHEELBASE*sin(yaw),
														&f[pointindex], &dfdNindividual[pointindex], &dfdEindividual[pointindex]);

	pointindex++;
	for(r=1; pointindex<OBSTACLE_AVOIDANCE_DIAM; r++)
	{
		getContinuousMapValueDiff(zeroedNorthing + m_initN + ((double)r)*OBSTACLE_AVOIDANCE_DELTA*sin(yaw) + VEHICLE_WHEELBASE*cos(yaw),
															zeroedEasting  + m_initE - ((double)r)*OBSTACLE_AVOIDANCE_DELTA*cos(yaw) + VEHICLE_WHEELBASE*sin(yaw),
															&f[pointindex], &dfdNindividual[pointindex], &dfdEindividual[pointindex]);
		pointindex++;
		getContinuousMapValueDiff(zeroedNorthing + m_initN - ((double)r)*OBSTACLE_AVOIDANCE_DELTA*sin(yaw) + VEHICLE_WHEELBASE*cos(yaw),
															zeroedEasting  + m_initE + ((double)r)*OBSTACLE_AVOIDANCE_DELTA*cos(yaw) + VEHICLE_WHEELBASE*sin(yaw),
															&f[pointindex], &dfdNindividual[pointindex], &dfdEindividual[pointindex]);
		pointindex++;
	}

	*fval = approxMin(f, dmin_df);

	*dfdN = 0.0;
	*dfdE = 0.0;
	for(pointindex=0; pointindex<OBSTACLE_AVOIDANCE_DIAM; pointindex++)
	{
		*dfdN += dmin_df[pointindex]*dfdNindividual[pointindex];
		*dfdE += dmin_df[pointindex]*dfdEindividual[pointindex];
	}


	pointindex = 1;
	*dfdyaw =
		dmin_df[0]*
		(dfdNindividual[0]* (-sin(yaw)*VEHICLE_WHEELBASE) +
		 dfdEindividual[0]* ( cos(yaw)*VEHICLE_WHEELBASE));

	for(r=1; pointindex<OBSTACLE_AVOIDANCE_DIAM; r++)
	{
		*dfdyaw +=
			dmin_df[pointindex  ]*
			(dfdNindividual[pointindex  ]* ( cos(yaw)*OBSTACLE_AVOIDANCE_DELTA * ((double)r) - sin(yaw)*VEHICLE_WHEELBASE) +
			 dfdEindividual[pointindex  ]* ( sin(yaw)*OBSTACLE_AVOIDANCE_DELTA * ((double)r) + cos(yaw)*VEHICLE_WHEELBASE));
		*dfdyaw +=
			dmin_df[pointindex+1]*
			(dfdNindividual[pointindex+1]* (-cos(yaw)*OBSTACLE_AVOIDANCE_DELTA * ((double)r) - sin(yaw)*VEHICLE_WHEELBASE) +
			 dfdEindividual[pointindex+1]* (-sin(yaw)*OBSTACLE_AVOIDANCE_DELTA * ((double)r) + cos(yaw)*VEHICLE_WHEELBASE));

		pointindex += 2;
	}
}

void CPlannerStage::getContinuousMapValueDiff(double UTMNorthing, double UTMEasting,
																							double *f, double *dfdN, double *dfdE)
{
	int nearestSWCornerRow, nearestSWCornerCol;
	int row, col, matrixidx;

	// Find the R/C coords of the cell whose SW corner is nearest to the given (N,E)
	m_pMap->UTM2Win(UTMNorthing + m_pMap->getResRows() / 2.0,
									UTMEasting  + m_pMap->getResCols() / 2.0,
									&nearestSWCornerRow, &nearestSWCornerCol);

	// middleCellN, middleCellE are the UTM coords of the corner nearest to UTMNorthing, UTMEasting
	// (rescaledN, rescaledE) is now [0,1] from the mid of the SW cell to the mid of the NE cell
	double middleCellN, middleCellE;
	m_pMap->Win2UTM(nearestSWCornerRow, nearestSWCornerCol, &middleCellN, &middleCellE);
	double rescaledN = ((UTMNorthing - middleCellN) + m_pMap->getResRows()/2.0) / m_pMap->getResRows();
	double rescaledE = ((UTMEasting  - middleCellE) + m_pMap->getResCols()/2.0) / m_pMap->getResCols();

#ifdef DO_BENCHMARK
	unsigned long long t1, t2;
	DGCgettime(t1);
#endif





#ifdef INTERPOLATION_SMOOTH
 	int i;


 	char notrans = 'N';
 	char trans   = 'T';
 	double alpha  =  1.0;
 	double beta   =  0.0;
	double minone = -1.0;
	double posone =  1.0;

 	int incr = 1;
 	int numcoeffs = 3;
 	int numpoints = MAP_INTERP_DIAM*MAP_INTERP_DIAM;

	// backup of WM is necessary because the svd routine destroys the contents
 	double WM[MAP_INTERP_DIAM*MAP_INTERP_DIAM * 3];
 	double WMbackup[MAP_INTERP_DIAM*MAP_INTERP_DIAM * 3];

	double windowW[MAP_INTERP_DIAM*MAP_INTERP_DIAM];
 	double delr, delc;

	double KN[MAP_INTERP_DIAM*MAP_INTERP_DIAM];
	double KE[MAP_INTERP_DIAM*MAP_INTERP_DIAM];

 	matrixidx = 0;
 	for(col = -MAP_INTERP_RAD;
 			col < +MAP_INTERP_RAD;
 			col ++)
 	{
		delc = (double)(col+1) - rescaledE;

 		for(row = -MAP_INTERP_RAD;
 				row < +MAP_INTERP_RAD;
 				row ++)
 		{
			double w;

 			delr = (double)(row+1) - rescaledN;
 			w = exp( MAP_INTERP_EXPCOEFF * (delr*delr + delc*delc) );
 			windowW [matrixidx] = w*m_pMap->getDataWin<double>(m_mapLayer, row+nearestSWCornerRow, col+nearestSWCornerCol);

 			WM[matrixidx + numpoints*0] = w;
 			WM[matrixidx + numpoints*1] = w*(double)row;
 			WM[matrixidx + numpoints*2] = w*(double)col;

			KN[matrixidx] = -4.0*MAP_INTERP_EXPCOEFF*delr / m_pMap->getResRows();
			KE[matrixidx] = -4.0*MAP_INTERP_EXPCOEFF*delc / m_pMap->getResCols();
 			matrixidx++;
 		}
 	}

	// make a backup of the WM matrix (because the svd routine destroys the data).
	memcpy(WMbackup, WM, MAP_INTERP_DIAM*MAP_INTERP_DIAM * 3*sizeof(double));

	double pinvWM[MAP_INTERP_DIAM*MAP_INTERP_DIAM * 3];
 	double coeffs[3];
	double coeffsdN[3];
	double coeffsdE[3];

	// Compute the SVD of the weighted M matrix
	char svdReturnWhatsImportant = 'S';
	double svdSigma[3];
	double svdU[MAP_INTERP_DIAM*MAP_INTERP_DIAM * 3];
	double svdVT[3*3];
	double svdWork[256];
	int    svdWorkSize = 256;
	int    svdInfo;
	dgesvd_(&svdReturnWhatsImportant, &svdReturnWhatsImportant, &numpoints, &numcoeffs, WM,
					&numpoints, svdSigma, svdU, &numpoints, svdVT, &numcoeffs,
					svdWork, &svdWorkSize, &svdInfo);

	// Now compute the pseudoinverse of WM: WM=U S VT -> WM+ = V inv(S) UT
	// where inv(S) is ST with the diagonal elements reciprocated.
	// inv(S) :
	svdSigma[0] = 1.0/svdSigma[0];
	svdSigma[1] = 1.0/svdSigma[1];
	svdSigma[2] = 1.0/svdSigma[2];
	// V*inv(S)
	dscal_(&numcoeffs, &svdSigma[0], svdVT  , &numcoeffs);
	dscal_(&numcoeffs, &svdSigma[1], svdVT+1, &numcoeffs);
	dscal_(&numcoeffs, &svdSigma[2], svdVT+2, &numcoeffs);
	// V*inv(S)*UT
	dgemm_(&trans, &trans, &numcoeffs, &numpoints, &numcoeffs, &alpha,
				svdVT, &numcoeffs, svdU, &numpoints, &beta, pinvWM, &numcoeffs);


	// Now compute the solution vector into coeffs:
 	dgemv_(&notrans, &numcoeffs, &numpoints, &alpha, pinvWM, &numcoeffs,
 				 windowW, &incr, &beta, coeffs, &incr);

	// Store the output!
 	*f    = coeffs[0] + (rescaledN-1.0)*coeffs[1] + (rescaledE-1.0)*coeffs[2];


	// Now compute the derivatives:
	// First, compute the error: (windowW - WMx) -> windowW
	dgemv_(&notrans, &numpoints, &numcoeffs, &minone, WMbackup, &numpoints, coeffs, &incr,
				 &posone, windowW, &incr);
	// Then compute K*error
	for(i=0; i<numpoints; i++)
	{
		KN[i] *= windowW[i];
		KE[i] *= windowW[i];
	}
	// Now multiply the K*error to get the two derivative vectors (one for
	// easting, one for northing)
 	dgemv_(&notrans, &numcoeffs, &numpoints, &alpha, pinvWM, &numcoeffs,
 				 KN, &incr, &beta, coeffsdN, &incr);
 	dgemv_(&notrans, &numcoeffs, &numpoints, &alpha, pinvWM, &numcoeffs,
 				 KE, &incr, &beta, coeffsdE, &incr);
 	*dfdN = coeffsdN[0] + coeffs[1]/m_pMap->getResRows() + (rescaledN-1.0)*coeffsdN[1] + (rescaledE-1.0)*coeffsdN[2];
 	*dfdE = coeffsdE[0] + coeffs[2]/m_pMap->getResCols() + (rescaledE-1.0)*coeffsdE[2] + (rescaledN-1.0)*coeffsdE[1];;


#elif defined INTERPOLATION_GAUSSIAN

	*f    = 0.0;
 	*dfdN = 0.0;
 	*dfdE = 0.0;

	double delr, delc;
	double sumWeights                = 0.0;
	double sumWeightedMap            = 0.0;
	double sumMapDerivativesN        = 0.0;
	double sumDerivativesN           = 0.0;
	double sumMapDerivativesE        = 0.0;
	double sumDerivativesE           = 0.0;

 	for(col = -MAP_INTERP_RAD;
 			col < +MAP_INTERP_RAD;
 			col ++)
 	{
		delc = (double)(col+1) - rescaledE;

 		for(row = -MAP_INTERP_RAD;
 				row < +MAP_INTERP_RAD;
 				row ++)
 		{
			double w;
			double dwN, dwE;
			double m;
 			delr = (double)(row+1) - rescaledN;

 			w  = exp( MAP_INTERP_EXPCOEFF * (delr*delr + delc*delc) );
			dwN = -2.0*MAP_INTERP_EXPCOEFF * w * delr / m_pMap->getResRows();
			dwE = -2.0*MAP_INTERP_EXPCOEFF * w * delc / m_pMap->getResCols();
			m  = m_pMap->getDataWin<double>(m_mapLayer, row+nearestSWCornerRow, col+nearestSWCornerCol);

			sumWeights                += w;
 			sumWeightedMap            += w * m;
			sumMapDerivativesN        += m * dwN;
			sumDerivativesN           += dwN;
			sumMapDerivativesE        += m * dwE;
			sumDerivativesE           += dwE;
 		}
 	}

	*f    = sumWeightedMap/sumWeights;
	*dfdN = sumMapDerivativesN/sumWeights - sumWeightedMap*sumDerivativesN/sumWeights/sumWeights;
	*dfdE = sumMapDerivativesE/sumWeights - sumWeightedMap*sumDerivativesE/sumWeights/sumWeights;

#else



	// At this point we sample the data in a window around (nearestSWCornerRow,
	// nearestSWCornerCol). Then we apply a gaussian blur to this window 4 times
	// to get the blurred value of the map at the 4 cells around our point (the
	// blur is done with a matrix multiplication). We then apply a bilinear
	// interpolation to compute the actual value of the map here. The
	// sampling/blurring (mostly sampling) of the map is a computational
	// bottleneck. We thus cache precomputed sampling/blurring results and use
	// those if we can.
	double& cachedBlurSW = m_pMap->getDataWin<double>(m_blurLayer, nearestSWCornerRow-1, nearestSWCornerCol-1);
	double& cachedBlurS  = m_pMap->getDataWin<double>(m_blurLayer, nearestSWCornerRow-1, nearestSWCornerCol  );
	double& cachedBlurW  = m_pMap->getDataWin<double>(m_blurLayer, nearestSWCornerRow  , nearestSWCornerCol-1);
	double& cachedBlur   = m_pMap->getDataWin<double>(m_blurLayer, nearestSWCornerRow  , nearestSWCornerCol  );

	double blurredwindow[4] = {0.0, 0.0, 0.0, 0.0};

	// if any one of the corners we need has invalid data, compute everything
	if(cachedBlurSW < 0.0 || cachedBlurS < 0.0 || cachedBlurW < 0.0 || cachedBlur < 0.0)
	{
		// zero out the blurring result
		memset(blurredwindow, 0, 4*sizeof(blurredwindow[0]));

		// these double loops can't be trivially optimized, since the map could wrap in memory (it's a torus).
		matrixidx = 0;
		for(col = nearestSWCornerCol-MAP_BLURINTERP_DIAM_CELLS/2;
				col < nearestSWCornerCol+MAP_BLURINTERP_DIAM_CELLS/2;
				col ++)
		{
			for(row = nearestSWCornerRow-MAP_BLURINTERP_DIAM_CELLS/2;
					row < nearestSWCornerRow+MAP_BLURINTERP_DIAM_CELLS/2;
					row ++)
			{
				double mapval = m_pMap->getDataWin<double>(m_mapLayer, row, col);
				blurredwindow[0] += mapval * m_mapblurcoeffs[matrixidx*4    ];
				blurredwindow[1] += mapval * m_mapblurcoeffs[matrixidx*4 + 1];
				blurredwindow[2] += mapval * m_mapblurcoeffs[matrixidx*4 + 2];
				blurredwindow[3] += mapval * m_mapblurcoeffs[matrixidx*4 + 3];
				matrixidx++;
			}
		}

		// if the cached values were NODATA, set them
		if(cachedBlurSW == -2.0) cachedBlurSW = blurredwindow[0];
		if(cachedBlurW  == -2.0) cachedBlurW  = blurredwindow[1];
		if(cachedBlurS  == -2.0) cachedBlurS  = blurredwindow[2];
		if(cachedBlur   == -2.0) cachedBlur   = blurredwindow[3];
	}
	else
	{
		// read in the cached values
		blurredwindow[0] = cachedBlurSW;
		blurredwindow[1] = cachedBlurW;
		blurredwindow[2] = cachedBlurS;
		blurredwindow[3] = cachedBlur;
	}

	*f =
		blurredwindow[0] * ((1.0 - rescaledN) * (1.0 - rescaledE)) +
		blurredwindow[1] * (rescaledN * (1.0 - rescaledE)) +
		blurredwindow[2] * ((1.0 - rescaledN) * rescaledE) +
		blurredwindow[3] * (rescaledN * rescaledE);

	*dfdN =
		(blurredwindow[0] * (rescaledE - 1.0) +
		 blurredwindow[1] * (1.0 - rescaledE) +
		 blurredwindow[2] * (-rescaledE) +
		 blurredwindow[3] * (rescaledE)) / m_pMap->getResRows();
	*dfdE =
		(blurredwindow[0] * (rescaledN - 1.0) +
		 blurredwindow[1] * (-rescaledN) +
		 blurredwindow[2] * (1.0 - rescaledN) +
		 blurredwindow[3] * (rescaledN)) / m_pMap->getResCols();

#endif

#ifdef DO_BENCHMARK
	DGCgettime(t2);
	blurtime += t2-t1;
	numinterpblurs++;
#endif

	if(m_USE_MAXSPEED)
	{
		if(f != NULL)
			*f = (*f) * (MAXSPEED - MIN_V) + MIN_V;
		if(dfdN != NULL)
		{
			*dfdN *= MAXSPEED - MIN_V;
			*dfdE *= MAXSPEED - MIN_V;
		}
	}
}

void CPlannerStage::getSplineCoeffsTheta(double* pSolverState)
{
	char notrans = 'N';
	int rows = (NUM_POINTS-1) * 3; // number of polynomial coefficients
	int cols = NUM_POINTS;         // number of spline-defining variables
	double alpha = 1.0;
	double beta = 0.0;
	int incr = 1;

	dgemv_(&notrans, &rows, &cols, &alpha, m_dercoeffsmatrix, &rows,
				 pSolverState, &incr, &beta, m_splinecoeffsTheta, &incr);

	for(int i=0; i<(NUM_POINTS-1); i++)
		m_splinecoeffsTheta[3*i] += m_initTheta;
}

void CPlannerStage::integrateNE(int collIndex)
{
#ifdef DO_BENCHMARK
	unsigned long long t1, t2;
	DGCgettime(t1);
#endif

#ifdef NEW_INTEGRATE_NE
	int i;
	int splinecoeffsstart = 3 * (collIndex / COLLOCATION_FACTOR);

	double a  = m_splinecoeffsTheta[splinecoeffsstart];
	double b  = m_splinecoeffsTheta[splinecoeffsstart + 1];
	double c  = m_splinecoeffsTheta[splinecoeffsstart + 2];

	double s0 = (double)(collIndex % COLLOCATION_FACTOR)/(double)(COLLOCATION_FACTOR);
	double di = 1.0 / (double)(COLLOCATION_FACTOR);
	double dicubed = di*di*di;
	double s1 = s0 + di;

	double x0_plus_a = 0.5 * (s0*(b+c*s0) + s1*(b+c*s1)) + a;
	double cos_x0_plus_a = cos(x0_plus_a);
	double sin_x0_plus_a = sin(x0_plus_a);

	double val = 7.0 * (s0*s0 + s1*s1) + 6.0*s0*s1;
	double small = c * dicubed / 6.0;
	double big   = 1.0 - dicubed/120.0 * ( c*c* val + 5.0*b* (2.0*c*(s1 + s0) + b) );

	double dsmall_dc =   dicubed/6.0;
	double dbig_db   = - dicubed/12.0 * (c*(s1 + s0) + b );
	double dbig_dc   = - dicubed/60.0 * (c*val + 5.0*b*(s1 + s0) );
	double dx0_plus_a_da = 1.0;
	double dx0_plus_a_db = 0.5*(s0 + s1);
	double dx0_plus_a_dc = 0.5*(s0*s0 + s1*s1);



	double delN = (cos_x0_plus_a*big + sin_x0_plus_a*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));
	double delE = (sin_x0_plus_a*big - cos_x0_plus_a*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));

	double ddelN_da = (                        - sin_x0_plus_a*big +
										                           cos_x0_plus_a*small              ) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));
	double ddelN_db = (cos_x0_plus_a*dbig_db   - sin_x0_plus_a*dx0_plus_a_db*big +
										                           cos_x0_plus_a*dx0_plus_a_db*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));
	double ddelN_dc = (cos_x0_plus_a*dbig_dc   - sin_x0_plus_a*dx0_plus_a_dc*big +
										 sin_x0_plus_a*dsmall_dc + cos_x0_plus_a*dx0_plus_a_dc*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));

	double ddelE_da = (                        + cos_x0_plus_a*big +
										                           sin_x0_plus_a*small              ) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));
	double ddelE_db = (sin_x0_plus_a*dbig_db   + cos_x0_plus_a*dx0_plus_a_db*big +
										                           sin_x0_plus_a*dx0_plus_a_db*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));
	double ddelE_dc = (sin_x0_plus_a*dbig_dc   + cos_x0_plus_a*dx0_plus_a_dc*big -
										 cos_x0_plus_a*dsmall_dc + sin_x0_plus_a*dx0_plus_a_dc*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));

	m_pCollN[collIndex+1] = n + delN;
	m_pCollE[collIndex+1] = e + delE;

	double ddelN_dcoeff, ddelE_dcoeff;
	for(i=0; i<NUM_POINTS; i++)
	{
		ddelN_dcoeff = 
			ddelN_da * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3    ] +
			ddelN_db * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 1] +
			ddelN_dc * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 2];
		ddelE_dcoeff = 
			ddelE_da * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3    ] +
			ddelE_db * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 1] +
			ddelE_dc * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 2];

#warning "this could be done with a static matrixindex int"
		m_pCollGradN[i + NUM_POINTS*(collIndex+1)] = m_pCollGradN[i + NUM_POINTS*collIndex] + ddelN_dcoeff;
		m_pCollGradE[i + NUM_POINTS*(collIndex+1)] = m_pCollGradE[i + NUM_POINTS*collIndex] + ddelE_dcoeff;
	}

#else

	int i;
	int splinecoeffsstart = 3 * (collIndex / COLLOCATION_FACTOR);

	double a = m_splinecoeffsTheta[splinecoeffsstart + 0];
	double b = m_splinecoeffsTheta[splinecoeffsstart + 1];
	double c = m_splinecoeffsTheta[splinecoeffsstart + 2];
	double s1 = (double)(collIndex % COLLOCATION_FACTOR)/(double)(COLLOCATION_FACTOR);
	double diff = 1.0 / (double)(COLLOCATION_FACTOR);
	double sum = 2.0*s1 + diff;
	double smid = s1 + diff/2.0;

	double ce = smid*(smid*c + b) + a;

	double cos_ce = cos(ce);
	double sin_ce = sin(ce);

#warning ce_minus_a could be 0 if we choose ce as such
	double ce_minus_a = ce - a;

	double sumsq = sum*sum;
	double diffsq = diff*diff;
	double sumsqplusdiffsq = sumsq + diffsq;
	double binom = (sqrt(5.0)*sumsq + (sqrt(5.0) - 2.0) * diffsq) * (sqrt(5.0)*sumsq + (sqrt(5.0) + 2.0)*diffsq);

	double small_ce_minus_a_0 = -diff*(3.0*c* (c*binom + 20.0*b*sum*sumsqplusdiffsq) +
																		20.0*b*b*(3.0*sumsq+diffsq)-480.0) / 480.0;
	double small_ce_minus_a_1 = diff * (3.0*sum * (c*sum + 2.0*b) + c*diffsq) / 12.0;
	double small_ce_minus_a_2 = -diff / 2.0;

	double big_ce_minus_a_0 =
		(
		 sum * 35.0 *( sum *( sum *( sum *( sum * c*c * (c*sum +
																										 6.0*b) +
																				c * (5.0*c*c*diffsq + 12.0*b*b)) +
																 4.0*b*(5.0*c*c*diffsq + 2.0*b*b)) +
													3.0 * c*(diffsq*(c*c*diffsq+8.0*b*b)-32.0)) +
									 2.0 * b*(diffsq*(3.0*c*c*diffsq+4.0*b*b)-96.0)) +
		 c*diffsq*(diffsq*(5.0*c*c*diffsq+84.0*b*b)-1120.0)
		) * diff / 13440.0;

	// these are hardcoded
// 	double big_ce_minus_a_1 = small_ce_minus_a_0;
// 	double big_ce_minus_a_2 = small_ce_minus_a_1 / 2.0;
// 	double big_ce_minus_a_3 = small_ce_minus_a_2 / 3.0;

	double big =
		ce_minus_a *( ce_minus_a *( ce_minus_a*small_ce_minus_a_2 / 3.0 +
																small_ce_minus_a_1 / 2.0) +
									small_ce_minus_a_0) +
		big_ce_minus_a_0;

	double small =
		ce_minus_a *( ce_minus_a * small_ce_minus_a_2 +
									small_ce_minus_a_1 ) +
		small_ce_minus_a_0;

	m_pCollN[collIndex+1] = n + (cos_ce*small + sin_ce*big) / (double)(NUM_POINTS-1);
	m_pCollE[collIndex+1] = e + (sin_ce*small - cos_ce*big) / (double)(NUM_POINTS-1);


	double dsmall_ce_minus_a_0_db = -diff * (1.5*c*sum*sumsqplusdiffsq + b*(3.0*sumsq+diffsq)) / 12.0;
	double dsmall_ce_minus_a_0_dc = -diff / 80.0 * (10.0*b*sum*sumsqplusdiffsq + binom*c);
	double dsmall_ce_minus_a_1_db = 0.5*diff*sum;
	double dsmall_ce_minus_a_1_dc = diff*(3.0*sumsq + diffsq) / 12.0;
	double dbig_ce_minus_a_0_db = diff / 13440.0 * 
		(35.0*sum*(sum*(sum*(6.0*sum*c*(c*sum + 4.0*b) +
												 20.0*(c*c*diffsq + 1.2*b*b)) +
										48.0*b*c*diffsq) +
							 2.0*(diffsq*12.0*(0.25*c*c*diffsq + b*b) - 96.0)) +
		 168.0*b*c*diffsq*diffsq);
	double dbig_ce_minus_a_0_dc = diff / 13440.0 *
			  (35.0*sum*(sum*(sum*(sum*3.0*(c*sum*(c*sum + 4.0*b) +
																	5.0*c*c*diffsq + 4.0*b*b) +
														 40.0*b*c*diffsq) +
												3.0 * diffsq * (3.0*c*c*diffsq + 8.0*b*b) - 96.0) +
									 12.0*b*c*diffsq*diffsq) +
				 diffsq * (diffsq * (diffsq*15.0*c*c + 84.0*b*b) - 1120.0));


	double dsmall_da = -small_ce_minus_a_1 - 2.0*ce_minus_a*small_ce_minus_a_2;
	double dsmall_db = ce_minus_a*dsmall_ce_minus_a_1_db + dsmall_ce_minus_a_0_db;
	double dsmall_dc = ce_minus_a*dsmall_ce_minus_a_1_dc + dsmall_ce_minus_a_0_dc;
	double dbig_da   = -small_ce_minus_a_0 - ce_minus_a*small_ce_minus_a_1 - small_ce_minus_a_2*ce_minus_a*ce_minus_a;
	double dbig_db   =
		ce_minus_a *( ce_minus_a * dsmall_ce_minus_a_1_db / 2.0 +
									dsmall_ce_minus_a_0_db) +
		dbig_ce_minus_a_0_db;
	double dbig_dc   =
		ce_minus_a *( ce_minus_a * dsmall_ce_minus_a_1_dc / 2.0 +
									dsmall_ce_minus_a_0_dc) +
		dbig_ce_minus_a_0_dc;

	double dsmall_dcoeff;
	double dbig_dcoeff;
#warning "BLAS-ify this"
	for(i=0; i<NUM_POINTS; i++)
	{
		dsmall_dcoeff =
			dsmall_da * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3] +
			dsmall_db * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 1] +
			dsmall_dc * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 2];

		dbig_dcoeff =
			dbig_da * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3] +
			dbig_db * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 1] +
			dbig_dc * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 2];

#warning "this could be done with a static matrixindex int"
		m_pCollGradN[i + NUM_POINTS*(collIndex+1)] = m_pCollGradN[i + NUM_POINTS*collIndex] +
			(cos_ce * dsmall_dcoeff + sin_ce*dbig_dcoeff) /
			(double)(NUM_POINTS-1);
		m_pCollGradE[i + NUM_POINTS*(collIndex+1)] = m_pCollGradE[i + NUM_POINTS*collIndex] +
			(sin_ce * dsmall_dcoeff - cos_ce*dbig_dcoeff) /
			(double)(NUM_POINTS-1);
	}
#endif


#ifdef DO_BENCHMARK
	DGCgettime(t2);
	integrateNEtime += t2-t1;
#endif
}


void CPlannerStage::NonLinearFinlConstrFunc(void)
{
	const int constr_index_snopt = NUM_NLIN_INIT_CONSTR + NUM_COLLOCATION_POINTS*NUM_NLIN_TRAJ_CONSTR;
	const int collIndex = NUM_COLLOCATION_POINTS-1;

	if(CARTESIAN_TARGET_SPECIFICATION)
	{
		m_pConstrData[constr_index_snopt + 0] = SCALEFACTOR_DIST * (n*sf - m_targetN);
		m_pConstrData[constr_index_snopt + 1] = SCALEFACTOR_DIST * (e*sf - m_targetE);

		m_grad_solver[0][IDX_THETA]		= 0.0;
		m_grad_solver[0][IDX_DTHETA]	= 0.0;
		m_grad_solver[0][IDX_DDTHETA]	= 0.0;
		m_grad_solver[0][IDX_SF]			= SCALEFACTOR_DIST * n;
		m_grad_solver[0][IDX_N]				= SCALEFACTOR_DIST * sf;
		m_grad_solver[0][IDX_E]				= 0.0;

		m_grad_solver[1][IDX_THETA]		= 0.0;
		m_grad_solver[1][IDX_DTHETA]	= 0.0;
		m_grad_solver[1][IDX_DDTHETA]	= 0.0;
		m_grad_solver[1][IDX_SF]			= SCALEFACTOR_DIST * e;
		m_grad_solver[1][IDX_N]				= 0.0;
		m_grad_solver[1][IDX_E]				= SCALEFACTOR_DIST * sf;
	}
	else
	{
		// This constraint projects the final point to the trackline at the target,
		// and constrains the projected distance to the target.
		m_pConstrData[constr_index_snopt + 0] = SCALEFACTOR_DIST * ((n*sf - m_targetN)*cos(m_targetTheta) + (e*sf - m_targetE)*sin(m_targetTheta));
		m_pConstrData[constr_index_snopt + 1] = 0.0;


		m_grad_solver[0][IDX_THETA]		= 0.0;
		m_grad_solver[0][IDX_DTHETA]	= 0.0;
		m_grad_solver[0][IDX_DDTHETA]	= 0.0;
		m_grad_solver[0][IDX_SF]			= SCALEFACTOR_DIST * (n *cos(m_targetTheta) + e*sin(m_targetTheta));
		m_grad_solver[0][IDX_N]				= SCALEFACTOR_DIST *  sf*cos(m_targetTheta);
		m_grad_solver[0][IDX_E]				= SCALEFACTOR_DIST *  sf*sin(m_targetTheta);

		m_grad_solver[1][IDX_THETA]		= 0.0;
		m_grad_solver[1][IDX_DTHETA]	= 0.0;
		m_grad_solver[1][IDX_DDTHETA]	= 0.0;
		m_grad_solver[1][IDX_SF]			= 0.0;
		m_grad_solver[1][IDX_N]				= 0.0;
		m_grad_solver[1][IDX_E]				= 0.0;
	}
}


#warning "This function really shouldn't be mostly a copy of integrateNE"
void CPlannerStage::integrateNEfine(int collIndex, double& outN, double& outE)
{
#ifdef NEW_INTEGRATE_NE

	int i;
	int splinecoeffsstart = 3 * (collIndex / COLLOCATION_FACTOR_FINE);

	double a  = m_splinecoeffsTheta[splinecoeffsstart];
	double b  = m_splinecoeffsTheta[splinecoeffsstart + 1];
	double c  = m_splinecoeffsTheta[splinecoeffsstart + 2];

	double s0 = (double)(collIndex % COLLOCATION_FACTOR_FINE)/(double)(COLLOCATION_FACTOR_FINE);
	double di = 1.0 / (double)(COLLOCATION_FACTOR_FINE);
	double dicubed = di*di*di;
	double s1 = s0 + di;

	double x0_plus_a = 0.5 * (s0*(b+c*s0) + s1*(b+c*s1)) + a;
	double cos_x0_plus_a = cos(x0_plus_a);
	double sin_x0_plus_a = sin(x0_plus_a);

	double val = 7.0 * (s0*s0 + s1*s1) + 6.0*s0*s1;
	double small = c * dicubed / 6.0;
	double big   = 1.0 - dicubed/120.0 * ( c*c* val + 5.0*b* (2.0*c*(s1 + s0) + b) );

	double delN = (cos_x0_plus_a*big + sin_x0_plus_a*small) / (double)((NUM_POINTS-1)*COLLOCATION_FACTOR_FINE);
	double delE = (sin_x0_plus_a*big - cos_x0_plus_a*small) / (double)((NUM_POINTS-1)*COLLOCATION_FACTOR_FINE);
	outN += delN;
	outE += delE;

#else

	int splinecoeffsstart = 3 * (collIndex / COLLOCATION_FACTOR_FINE);

	double a = m_splinecoeffsTheta[splinecoeffsstart];
	double b = m_splinecoeffsTheta[splinecoeffsstart + 1];
	double c = m_splinecoeffsTheta[splinecoeffsstart + 2];
	double s1 = (double)(collIndex % COLLOCATION_FACTOR_FINE)/(double)(COLLOCATION_FACTOR_FINE);
	double diff = 1.0 / (double)(COLLOCATION_FACTOR_FINE);
	double smid = s1 + diff/2.0;

	double ce = smid*(smid*c + b) + a;
	double ce_minus_a = ce - a;
	double sum = 2.0*s1 + diff;

	double sumsq = sum*sum;
	double diffsq = diff*diff;
	double sumsqplusdiffsq = sumsq + diffsq;
	double binom = (sqrt(5.0)*sumsq + (sqrt(5.0) - 2.0) * diffsq) * (sqrt(5.0)*sumsq + (sqrt(5.0) + 2.0)*diffsq);

	double small_ce_minus_a_0 = -diff*(3.0*c* (c*binom + 20.0*b*sum*sumsqplusdiffsq) +
																		 20.0*b*b*(3.0*sumsq+diffsq)-480.0) / 480.0;
	double small_ce_minus_a_1 = diff * (3.0*sum * (c*sum + 2.0*b) + c*diffsq) / 12.0;
	double small_ce_minus_a_2 = -diff / 2.0;

	double big_ce_minus_a_0 =
		(
		 sum * 35.0 *( sum *( sum *( sum *( sum * c*c * (c*sum +
																										 6.0*b) +
																				c * (5.0*c*c*diffsq + 12.0*b*b)) +
																 4.0*b*(5.0*c*c*diffsq + 2.0*b*b)) +
													3.0 * c*(diffsq*(c*c*diffsq+8.0*b*b)-32.0)) +
									 2.0 * b*(diffsq*(3.0*c*c*diffsq+4.0*b*b)-96.0)) +
		 c*diffsq*(diffsq*(5.0*c*c*diffsq+84.0*b*b)-1120.0)
		 ) * diff / 13440.0;

	// these are hardcoded
	// double big_ce_minus_a_1 = small_ce_minus_a_0;
	// double big_ce_minus_a_2 = small_ce_minus_a_1 / 2.0;
	// double big_ce_minus_a_3 = small_ce_minus_a_2 / 3.0;

	double big =
		ce_minus_a *( ce_minus_a *( ce_minus_a*small_ce_minus_a_2 / 3.0 +
																small_ce_minus_a_1 / 2.0) +
									small_ce_minus_a_0) +
		big_ce_minus_a_0;

	double small =
		ce_minus_a *( ce_minus_a * small_ce_minus_a_2 +
									small_ce_minus_a_1 ) +
		small_ce_minus_a_0;

	outN += (cos(ce)*small + sin(ce)*big) / (double)(NUM_POINTS-1);
	outE += (sin(ce)*small - cos(ce)*big) / (double)(NUM_POINTS-1);
#endif
}
