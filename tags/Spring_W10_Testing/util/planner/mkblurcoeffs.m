N = 2; # i want only the 4 neighboring points: 2x2
GAUSSIAN_BLUR_FACTOR = 2.5;

MAP_BLUR_KERNEL_DIAM = [ 7 7];

filenames = ["mapblurcoeffs1.dat"; "mapblurcoeffs2.dat"];

for index=1:2
	H = MAP_BLUR_KERNEL_DIAM(index);
	fil = fopen(filenames(index, :), "w+b", "ieee-le");

	h = (H-1) / 2;

  if H == 1
		xx = yy = 0;
  else
	  [xx,yy] = meshgrid(linspace(-1.0, 1.0, H));
  endif

	w = exp(-GAUSSIAN_BLUR_FACTOR * (xx.^2 + yy.^2));
	w /= sum(sum(w));

	M_blur = [];

	for j=1:N
		for i=1:N
			wrowsbefore = i-1;
			wrowsafter  = N+2*h - wrowsbefore - H;
			wcolsbefore = j-1;
			wcolsafter  = N+2*h - wcolsbefore - H;

			wmounted = [zeros(wrowsbefore, N+2*h);
									zeros(H, wcolsbefore) w zeros(H, wcolsafter);
									zeros(wrowsafter, N+2*h)];

			M_blur = [M_blur; vec(wmounted)'];
		end
	end

	fwrite(fil, M_blur , "double");
  fclose(fil);
end

