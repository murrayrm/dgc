#include <iostream>
using namespace std;

#include <stdlib.h>
#include <math.h>
#include "CMapPlus.hh"
#include "defs.h"
#include "SpacialStage.h"
#include "SpacialDefs.h"

// for simpler access
// NOTE: n,e here are not scaled by Sf
#define theta						m_pColl_theta  [collIndex]
#define dtheta					m_pColl_dtheta [collIndex]
#define ddtheta					m_pColl_ddtheta[collIndex]
#define sf							m_collSF
#define n								m_pCollN[collIndex]
#define e								m_pCollE[collIndex]
#define vlimit          m_pCollVlimit[collIndex]
#define dvldN           m_pCollDVlimitDN[collIndex]
#define dvldE           m_pCollDVlimitDE[collIndex]
#define dvldtheta       m_pCollDVlimitDTheta[collIndex]

#warning "some more things should use these functions. maybe makezp. search for valcoeffs, search for for(i=0; i<NUM_POINTS; i++)"
extern "C" void dgemv_(char*, int*, int*, double*, double*, int*, double*, int*, double*, double*, int*);
extern "C" void dgemm_(char*, char*, int*, int*, int*, double*, double*, int*, double*, int*, double*, double*, int*);
extern "C" double ddot_(int*, double*, int*, double*, int*);
extern "C" void daxpy_(int*, double*, double*, int*, double*, int*);

void CSpacialStage::NonLinearInitConstrFunc(void)
{
	const int collIndex = 0;

#warning "at slow speeds, this might be wrong since m_initSpeed was clipped up"
	m_pConstrData[0] = SCALEFACTOR_YAWDOT * m_initSpeed*dtheta/sf;

	m_grad_solver[0][IDX_THETA]		= 0.0;
	m_grad_solver[0][IDX_DTHETA]	= SCALEFACTOR_YAWDOT * m_initSpeed/sf;
	m_grad_solver[0][IDX_DDTHETA]	= 0.0;
	m_grad_solver[0][IDX_SF]			= SCALEFACTOR_YAWDOT * (-m_initSpeed*dtheta/sf/sf);
	m_grad_solver[0][IDX_N]				= 0.0;
	m_grad_solver[0][IDX_E]				= 0.0;
}

void CSpacialStage::NonLinearTrajConstrFunc(int constr_index_snopt, int collIndex)
{
#if PHICONSTR
	// This is actually tan(phi)
	m_pConstrData[constr_index_snopt + PHICONSTR_IDX]	=
		SCALEFACTOR_TANPHI * VEHICLE_WHEELBASE / sf * dtheta;

	m_grad_solver[PHICONSTR_IDX][IDX_THETA]		= 0.0;
	m_grad_solver[PHICONSTR_IDX][IDX_DTHETA]	= SCALEFACTOR_TANPHI * VEHICLE_WHEELBASE / sf;
	m_grad_solver[PHICONSTR_IDX][IDX_DDTHETA]	= 0.0;
	m_grad_solver[PHICONSTR_IDX][IDX_SF]			= SCALEFACTOR_TANPHI * (-VEHICLE_WHEELBASE * dtheta /sf/sf);
	m_grad_solver[PHICONSTR_IDX][IDX_N]				= 0.0;
	m_grad_solver[PHICONSTR_IDX][IDX_E]				= 0.0;
#endif

#warning "dvldN and friends shouldn't really be used"
	m_pConstrData[constr_index_snopt + PHICONSTR_IDX + 1] =
		SCALEFACTOR_SPEED * vlimit;

	m_grad_solver[PHICONSTR_IDX + 1][IDX_THETA]		= SCALEFACTOR_SPEED * dvldtheta;
	m_grad_solver[PHICONSTR_IDX + 1][IDX_DTHETA]	= 0.0;
	m_grad_solver[PHICONSTR_IDX + 1][IDX_DDTHETA]	= 0.0;
	m_grad_solver[PHICONSTR_IDX + 1][IDX_SF]			= SCALEFACTOR_SPEED * (dvldN*n + dvldE*e);
	m_grad_solver[PHICONSTR_IDX + 1][IDX_N]				= SCALEFACTOR_SPEED * dvldN*sf;
	m_grad_solver[PHICONSTR_IDX + 1][IDX_E]				= SCALEFACTOR_SPEED * dvldE*sf;
}

void CSpacialStage::funcConstr(void)
{
	int collIndex, constr_index_snopt, constr_index_planner;

	int varNUM_COLLOCATION_POINTS = NUM_COLLOCATION_POINTS;
	int varNPnumNonLinearConstr = NPnumNonLinearConstr;
	int one = 1;

	// zero out the gradient matrix
	memset(m_pConstrGradData, 0, NPnumNonLinearConstr*NPnumVariables*sizeof(m_pConstrGradData[0]));

	constr_index_snopt = 0;

	NonLinearInitConstrFunc();
	for(constr_index_planner=0;
			constr_index_planner < NUM_NLIN_INIT_CONSTR;
			constr_index_planner++)
	{
		// compute
		// 		m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, ALL)]  +=
		// 			m_grad_solver[constr_index_planner][IDX_THETA] * m_valcoeffs[0 + ALL*NUM_COLLOCATION_POINTS];
		// etc, etc
		// prev code was
// 		for(i=0; i<NUM_POINTS; i++)
// 		{
// 			// This is the initial constraint, so there's no dependence on (N,E), since it's (0,0) here
// 			m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, i)]  = m_valcoeffs[0 + i*NUM_COLLOCATION_POINTS]*m_grad_solver[constr_index_planner][IDX_THETA];
// 			m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, i)] += m_d1coeffs [0 + i*NUM_COLLOCATION_POINTS]*m_grad_solver[constr_index_planner][IDX_DTHETA];
// 			m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, i)] += m_d2coeffs [0 + i*NUM_COLLOCATION_POINTS]*m_grad_solver[constr_index_planner][IDX_DDTHETA];
// 		}
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_THETA],
					 &m_valcoeffs[0], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_DTHETA],
					 &m_d1coeffs[0], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_DDTHETA],
					 &m_d2coeffs[0], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		// Sf derivatives
		m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NPnumVariables-1)] = m_grad_solver[constr_index_planner][IDX_SF];

		constr_index_snopt++;
	}

	for(collIndex=0; collIndex<NUM_COLLOCATION_POINTS; collIndex++)
	{
		NonLinearTrajConstrFunc(constr_index_snopt, collIndex);

		for(constr_index_planner=0;
				constr_index_planner < NUM_NLIN_TRAJ_CONSTR;
				constr_index_planner++)
		{
			daxpy_(&NUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_N],
						 &m_pCollGradN[NUM_POINTS*collIndex], &one,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
			daxpy_(&NUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_E],
						 &m_pCollGradE[NUM_POINTS*collIndex], &one,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
			daxpy_(&NUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_THETA],
						 &m_valcoeffs[collIndex], &varNUM_COLLOCATION_POINTS,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
			daxpy_(&NUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_DTHETA],
						 &m_d1coeffs[collIndex], &varNUM_COLLOCATION_POINTS,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
			daxpy_(&NUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_DDTHETA],
						 &m_d2coeffs[collIndex], &varNUM_COLLOCATION_POINTS,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);

			// Sf derivatives
			m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NPnumVariables-1)] = m_grad_solver[constr_index_planner][IDX_SF];
			constr_index_snopt++;
		}
	}

	// compute the constr, gradients from the user
	NonLinearFinlConstrFunc();
	for(constr_index_planner=0;
			constr_index_planner < NUM_NLIN_FINL_CONSTR;
			constr_index_planner++)
	{
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_N],
					 &m_pCollGradN[NUM_POINTS*(NUM_COLLOCATION_POINTS-1)], &one,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_E],
					 &m_pCollGradE[NUM_POINTS*(NUM_COLLOCATION_POINTS-1)], &one,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_THETA],
					 &m_valcoeffs[(NUM_COLLOCATION_POINTS-1)], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_DTHETA],
					 &m_d1coeffs[(NUM_COLLOCATION_POINTS-1)], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_DDTHETA],
					 &m_d2coeffs[(NUM_COLLOCATION_POINTS-1)], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);

		// Sf derivatives
		m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NPnumVariables-1)] = m_grad_solver[constr_index_planner][IDX_SF];
		constr_index_snopt++;
	}
}


void CSpacialStage::funcCost(void)
{
// 	ofstream outtrajfil("out.dat");
// 	CTraj outtraj;
// 	outputTraj(&outtraj);
// 	outtraj.print(outtrajfil);



	int i,collIndex;

#warning "m_pblahdata is too long? NPnumVariables vs. NUM_POINTS"
	m_obj = 0.0;
	memset(m_pCostGradData, 0, NPnumVariables*sizeof(m_pCostGradData[0]));

	for(collIndex=0; collIndex<NUM_COLLOCATION_POINTS; collIndex++)
	{
		m_obj += (sf / vlimit) * SCALEFACTOR_COST;
		for(i=0; i<NUM_POINTS; i++)
		{
			m_pCostGradData[i] += -sf * m_pCollGradVlimit[collIndex*(NUM_POINTS+1) + i] /vlimit/vlimit * SCALEFACTOR_COST;

		}
		m_pCostGradData[NPnumVariables-1] += (vlimit - sf*m_pCollGradVlimit[collIndex*(NUM_POINTS+1) + NUM_POINTS]) /vlimit/vlimit * SCALEFACTOR_COST;
	}
}

int CSpacialStage::whereInfeasible(void)
{
	int i;

	for(i=0; i<NUM_COLLOCATION_POINTS; i++)
	{
		if(m_pConstrData[NUM_NLIN_INIT_CONSTR + i*NUM_NLIN_TRAJ_CONSTR + (NUM_NLIN_TRAJ_CONSTR-1)] < m_solverLowerBounds[NPnumVariables + NUM_NLIN_INIT_CONSTR + i*NUM_NLIN_TRAJ_CONSTR + (NUM_NLIN_TRAJ_CONSTR-1)] ||
			 m_pConstrData[NUM_NLIN_INIT_CONSTR + i*NUM_NLIN_TRAJ_CONSTR + (NUM_NLIN_TRAJ_CONSTR-1)] > m_solverUpperBounds[NPnumVariables + NUM_NLIN_INIT_CONSTR + i*NUM_NLIN_TRAJ_CONSTR + (NUM_NLIN_TRAJ_CONSTR-1)])
		{
			cout << "going through obstacle in collocation point " << i << endl;
			return i;
		}
	}
	return -1;
}
