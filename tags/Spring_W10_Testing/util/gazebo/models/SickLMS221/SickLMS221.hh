#ifndef SICKLMS221_HH
#define SICKLMS221_HH

#include <gazebo/Body.hh>
#include <gazebo/Model.hh>

// Forward declarations
class RayProximity;

/**
 * SickLMS221 - Simulate SICKLMS221 scanning laser range finder
 * A slight modification from the SickLMS200 that Andrew Howard wrote
 *
 * Installation
 * -# Edit the Makefile so GAZEBO_SRC_DIR points at your Gazebo 0.5.0 source tree
 * -# Run 'make' to compile
 * -# See /dgc/gazebo/worlds/tahoe_demo.world for usage of the SickLMS221 model.
 * @author Henrik Kjellander
 * $Id$
 */
class SickLMS221 : public Model
{
  /// @brief Constructor
  public: SickLMS221( World *world );

  /// @brief Destructor
  public: virtual ~SickLMS221();
  
  /// @brief Load the model
  public: virtual int Load( WorldFile *file, WorldFileNode *node );

  /// @brief Initialize the model
  public: virtual int Init( WorldFile *file, WorldFileNode *node );

  /// @brief Finalize the model
  public: virtual int Fini();

  /// @brief Update the model state
  public: virtual void Update( double step );

  /// @brief Load ODE stuff
  private: int OdeLoad( WorldFile *file, WorldFileNode *node );

  /// @brief Load rays
  private: int RayLoad( WorldFile *file, WorldFileNode *node );

  /// @brief Update the data in the laser interface
  private: void PutLaserData();

  /// @brief Update the data in the fiducial interface
  private: void PutFiducialData();

  // Our body
  private: Body *body;
  
  // Ray sensor
  private: RayProximity *sensor;
  
  // External interfaces
  private: gz_laser_t *laser_iface;
  private: gz_fiducial_t *fiducial_iface;

  // Laser settings
  private: int rayCount;
  private: int rangeCount;
  private: double laserMinRange, laserMaxRange;
  private: double laserPeriod;
  private: double laserTime;

  private: double scanAngle; // M_PI or (110*M_PI/180.0) radians
};


#endif
