#include "CCostPainter.hh"

CCostPainter::CCostPainter() {
  _inputMap = NULL;

  _costLayerNum = 0;
  _elevLayerNum = 0;
  _corridorLayerNum = 0;
}


CCostPainter::~CCostPainter() {

}


int CCostPainter::initPainter(CMap* inputMap, int costLayerNum, int elevLayerNum, int corridorLayerNum, int nonBinary, int growLayerNum, double radius, int conservativeGrowing) {
  _inputMap = inputMap;
  _nonBinary = nonBinary;
  _costLayerNum = costLayerNum;
  _elevLayerNum = elevLayerNum;
  _corridorLayerNum = corridorLayerNum;
  _growLayerNum = growLayerNum;

  _numRows = _inputMap->getNumRows();
  _numCols = _inputMap->getNumCols();

  _radius = radius;

  _conservativeGrowing = conservativeGrowing;

  _numMaskPts = 0;

  double rowRes = _inputMap->getResRows();
  double colRes = _inputMap->getResCols();

  int minRow, minCol, maxRow, maxCol;

  minRow = -radius/rowRes;
  minCol = -radius/colRes;
  maxRow = radius/rowRes;
  maxCol = radius/colRes;

  /*
  for(int row=minRow; row<maxRow; row++) {
    for(int col=minCol; col<maxCol; col++) {
      if(conservativeGrowing) {
	
      } else {
	if(
      }
      _numMaskPts++;
    }
  }
  */

  return 0;
}

int CCostPainter::paintChanges(double UTMNorthing, double UTMEasting, int useGrad) {
  if(_inputMap->checkBoundsUTM(UTMNorthing, UTMEasting) == CMap::CM_OK) {
    double maxVelo, currentVelo, newVelo;
    double maxGradient;
    double corridorReference;
    int maxLocalRow, maxLocalCol;
    int minLocalRow, minLocalCol;
  //  int maxGlobalRow, maxGlobalCol;
   // int minGlobalRow, minGlobalCol;
    int cellRow, cellCol;
    //printf("%s [%d]: Painin' us some changes y'all\n", __FILE__, __LINE__);

    //First, set us some limits
    _inputMap->UTM2Win(UTMNorthing, UTMEasting, &cellRow, &cellCol);
    if(cellRow < _numRows-1) {
      maxLocalRow = cellRow+1;
    } else {
      maxLocalRow = cellRow;
    }

    if(cellCol < _numCols-1) {
      maxLocalCol = cellCol+1;
    } else {
      maxLocalCol = cellCol;
    }

    if(cellRow>0) {
      minLocalRow = cellRow-1;
    } else {
      minLocalRow = cellRow;
    }

    if(cellCol > 0) {
      minLocalCol = cellCol-1;
    } else {
      minLocalCol = cellCol;
    }

    double currentElev, northElev, southElev, eastElev, westElev;    

    currentElev = _inputMap->getDataWin<double>(_elevLayerNum, cellRow, cellCol);
    northElev = _inputMap->getDataWin<double>(_elevLayerNum, maxLocalRow, cellCol);
    southElev = _inputMap->getDataWin<double>(_elevLayerNum, minLocalRow, cellCol);
    eastElev = _inputMap->getDataWin<double>(_elevLayerNum, cellRow, maxLocalCol);
    westElev = _inputMap->getDataWin<double>(_elevLayerNum, cellRow, minLocalCol);
	double noData = _inputMap->getLayerNoDataVal<double>(_elevLayerNum);
	if(northElev !=  noData)
	{
		northElev = fabs( northElev-currentElev);
	}
	else
	{
		northElev = 0;
	}
	
	if(southElev != noData )
	{
		southElev = fabs( southElev-currentElev);
	}
	else
	{
		southElev = 0;
	}
	if(eastElev != noData )
	{
		eastElev = fabs( eastElev-currentElev);
	}
	else
	{
		eastElev = 0;
	}
	if(westElev != noData )
	{
		westElev = fabs( westElev-currentElev);
	}
	else
	{
		westElev = 0;
	}



    maxGradient = maxGradientCalc(northElev,
				  southElev,
				  eastElev,
				  westElev);

    //maxVelo = generateVeloFromGrad(maxGradient);

    //int r = cellRow;
    //int c= cellCol;
    for(int r=minLocalRow; r<=maxLocalRow; r++) {
      for(int c=minLocalCol; c<=maxLocalCol; c++) {
	currentVelo = _inputMap->getDataWin<double>(_costLayerNum, r, c);
	corridorReference = _inputMap->getDataWin<double>(_corridorLayerNum, r, c);


	if(_nonBinary == 1)
	{
		maxVelo = generateVeloFromGrad(maxGradient);
		//printf("%s [%d]: nonbinary, maxvelo is %lf\n", __FILE__, __LINE__, maxVelo);
	}
	else{
	  
		if(maxGradient>0.3) {
		  maxVelo=0.001;
		} else {
		  maxVelo = corridorReference;
		}
		//printf("%s [%d]: binary, maxvelo is %lf, corrref is %lf\n", __FILE__, __LINE__, maxVelo, corridorReference);
	}
	if(useGrad == 0)
	{
	  //printf("%s [%d]: Whoah there pardner, useGrad==0!\n", __FILE__, __LINE__);
	  
		maxVelo = 0.001;
	}
	
//   if(corridorReference != _inputMap->getLayerNoDataVal<double>(_corridorLayerNum) || corridorReference != _inputMap->getLayerOutsideMapVal<double>(_corridorLayerNum)){
	if(currentVelo != maxVelo){
	  /*
	  printf("%s [%d]: maxVelo is %lf, corrRef is %lf, maxgrad is %lf, pt is (%lf, %lf)\n", 
		 __FILE__, __LINE__, maxVelo, corridorReference, maxGradient, UTMNorthing, UTMEasting);
	  */
	  _inputMap->setDataWin_Delta<double>(_costLayerNum, r, c,min(maxVelo, corridorReference));
	  //         }
	}
      }
    }
/*
    double finalVelo = 0;
    double b = 1;

    const int gaussianWidth = 5;
    int windowSize=(gaussianWidth-1)/2;
    double kernel[gaussianWidth][gaussianWidth];
    double gaussianSum = 0;
    for(int i=0; i<gaussianWidth; i++) {
      for(int j=0; j<gaussianWidth; j++) {
	kernel[i][j] = exp(-b)*(pow((double)(i-windowSize),2)+pow((double)(j-windowSize),2));
	gaussianSum+=kernel[i][j];
      }
    }

    for(int i=0; i<gaussianWidth; i++) {
      for(int j=0; j<gaussianWidth; j++) {
	kernel[i][j] = kernel[i][j]/gaussianSum;
      }
    }


    minGlobalRow = cellRow-windowSize;
    minGlobalCol = cellCol-windowSize;
    maxGlobalRow = cellRow+windowSize;
    maxGlobalCol = cellCol+windowSize;

    if(minGlobalRow < 0) minGlobalRow = 0;
    if(minGlobalCol < 0) minGlobalCol = 0;
    if(maxGlobalRow > _numRows-1) maxGlobalRow = _numRows-1;
    if(maxGlobalCol > _numCols-1) maxGlobalCol = _numCols-1;
    

    for(int row = minGlobalRow; row<=maxGlobalRow; row++) {
      for(int col = minGlobalCol; col<=maxGlobalCol; col++) {
	minLocalRow = row-windowSize;
	minLocalCol = col-windowSize;
	maxLocalRow = row+windowSize;
	maxLocalCol = col+windowSize;
	
	if(minLocalRow < 0) minLocalRow = 0;
	if(minLocalCol < 0) minLocalCol = 0;
	if(maxLocalRow > _numRows-1) maxLocalRow = _numRows-1;
	if(maxLocalCol > _numCols-1) maxLocalCol = _numCols-1;
	
	finalVelo = 0;

	for(int r=minLocalRow; r<=maxLocalRow; r++) {
	  for(int c=minLocalCol; c<=maxLocalCol; c++) {
	    finalVelo += kernel[row][col]*_inputMap->getDataWin<double>(_costLayerNum, row, col);
	  }
	}
	_inputMap->setDataWin_Delta<double>(_blurredLayerNum, row, col, finalVelo);
      }
    }

*/
    //printf("Out of bounds: (%lf, %lf)!\n", UTMNorthing, UTMEasting);
  }
  return 0;
}


double CCostPainter::generateVeloFromGrad(double gradient) {
  double temp = fabs(gradient);
  /*
  double someConst = 1;
  double maxSpeedAt30 = 5;
  double firstLimit = 0.1;
  double secondLimit = 0.3;
  if( temp < firstLimit)
    return 55-temp/((55-maxSpeedAt30)/firstLimit);
  else  if( temp >= firstLimit && temp <= secondLimit)
    return maxSpeedAt30- pow((temp-firstLimit), 2)/someConst;
  else if( temp > secondLimit)
    return 0.00001;
  */

  double maxObsHeight = 0.3;
  double maxSpeed = 5.0;
  if(temp < maxObsHeight) {
    return (maxObsHeight-temp)*maxSpeed/maxObsHeight;
  } else {
    return 0.0001;
  }
}


double CCostPainter::maxGradientCalc(double g1, double g2, double g3, double g4) {
  double temp1, temp2;
  temp1 = max(g1,g2);
  temp2 = max(g3,g4);
  return max(temp1, temp2);
}


int CCostPainter::growChanges(double UTMNorthing, double UTMEasting) {
  double rowRes = _inputMap->getResRows();
  double colRes = _inputMap->getResCols();

  int minRow, minCol, maxRow, maxCol;

  minRow = (int)-1*_radius/rowRes - 1;
  minCol = (int)-1*_radius/colRes - 1;
  maxRow = (int)_radius/rowRes + 1;
  maxCol = (int)_radius/colRes + 1;

  int origRow, origCol;

  int localRow, localCol;

  _inputMap->UTM2Win(UTMNorthing, UTMEasting, &origRow, &origCol);

  double blNorthing, blEasting, tlNorthing, tlEasting;
  double trNorthing, trEasting, brNorthing, brEasting;

  double blDist, tlDist, trDist, brDist;

  int* rowMask;
  int* colMask;

  rowMask = new int[(maxRow-minRow)*(maxCol-minCol)];
  colMask = new int[(maxRow-minRow)*(maxCol-minCol)];

  int numPoints = 0;

  double minCost = 999.0;

  //minCost = ((double)(rand()%100))/-100.0*6.0;

  double localCost;

  double noData = _inputMap->getLayerNoDataVal<double>(_costLayerNum);

  for(int row=minRow; row<=maxRow; row++) {
    for(int col=minCol; col<=maxCol; col++) {
      localRow = row + origRow;
      localCol = col + origCol;

      localCost = _inputMap->getDataWin<double>(_costLayerNum, localRow, localCol);

      if(localCost != _inputMap->getLayerOutsideMapVal<double>(_growLayerNum)) {
	_inputMap->Win2UTM(localRow, localCol, &blNorthing, &blEasting);
	tlNorthing = blNorthing + rowRes;
	trNorthing = blNorthing + rowRes;
	brNorthing = blNorthing;

	brEasting = blEasting + colRes;
	trEasting = blEasting + colRes;
	tlEasting = blEasting;

	blDist = sqrt(pow(blNorthing-UTMNorthing,2) + pow(blEasting-UTMEasting,2));
	brDist = sqrt(pow(brNorthing-UTMNorthing,2) + pow(brEasting-UTMEasting,2));
	tlDist = sqrt(pow(tlNorthing-UTMNorthing,2) + pow(tlEasting-UTMEasting,2));
	trDist = sqrt(pow(trNorthing-UTMNorthing,2) + pow(trEasting-UTMEasting,2));

	if(_conservativeGrowing) {
	  if(blDist<_radius || brDist < _radius ||
	     tlDist<_radius || trDist < _radius) {
	    rowMask[numPoints] = localRow;
	    colMask[numPoints] = localCol;
	    if(localCost < minCost && localCost!=noData) minCost = localCost;
	    numPoints++;
	  }
	} else {
	  if(blDist<_radius && brDist < _radius &&
	     tlDist<_radius && trDist < _radius) {
	    rowMask[numPoints] = localRow;
	    colMask[numPoints] = localCol;
	    if(localCost < minCost && localCost!=noData) minCost = localCost;
	    numPoints++;
	  }

	}

      }
    }
  }

  for(int i=0; i<numPoints; i++) {
    if(_inputMap->getDataWin<double>(_growLayerNum, rowMask[i], colMask[i]) > minCost) {
      _inputMap->setDataWin_Delta<double>(_growLayerNum, rowMask[i], colMask[i],
					  minCost);
    }
  }
  return 1;
}


int CCostPainter::growChanges(NEcoord exposedArea[]) {
  int minRow, maxRow, minCol, maxCol;

  _inputMap->UTM2Win(exposedArea[0].N, exposedArea[0].E,
		     &minRow, &minCol);
  _inputMap->UTM2Win(exposedArea[2].N, exposedArea[2].E,
		     &maxRow, &maxCol);
  
  // force the values into range
  minRow = max(minRow-2, 0);
  minCol = max(minCol-2, 0);
  maxRow   = min(maxRow+2, _inputMap->getNumRows()-1);
  maxCol   = min(maxCol+2, _inputMap->getNumCols()-1);
  
  double tempN, tempE;

  for(int row=minRow; row<=maxRow; row++) {
    for(int col=minCol; col<=maxCol; col++) {
      _inputMap->Win2UTM(row, col, &tempN, &tempE);
      growChanges(tempN, tempE);
    }
  }

  return 0;
}
