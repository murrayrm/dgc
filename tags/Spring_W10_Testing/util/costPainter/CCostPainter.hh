#ifndef __CCOSTPAINTER_HH__
#define __CCOSTPAINTER_HH__


#include <stdlib.h>
#include <CMap.hh>
#include <CCorridorPainter.hh>
//#include "../../MTA/Kernel/Misc/Time/Timeval.hh"


class CCostPainter {
public:
  CCostPainter();
  ~CCostPainter();

  int initPainter(CMap* inputMap, int costLayerNum, int elevLayerNum, int corridorLayerNum, int nonBinary, int growLayerNum, double radius, int conservativeGrowing);
  int paintChanges(double UTMNorthing, double UTMEasting, int useGrad);

  double generateVeloFromGrad(double gradient);


  int growChanges(double UTMNorthing, double UTMEasting);
  
  int growChanges(NEcoord exposedArea[]);

  enum {
    CCOSTPAINTER_NOT_PAINTED=0,
    CCOSTPAINTER_OUTSIDE_MAP=-1,
    
    CCOSTPAINTER_NUM_TYPES
  };


private:
  CMap* _inputMap;
  int _nonBinary;
  int _costLayerNum;
  int _elevLayerNum;
  int _corridorLayerNum;
  int _growLayerNum;

  int _numRows;
  int _numCols;

  int _numMaskPts;

  double _radius;
  int _conservativeGrowing;

  int* _maskRow;
  int* _maskCol;

  double maxGradientCalc(double g1, double g2, double g3, double g4);
};


#endif //__CCOSTPAINTER_HH__
