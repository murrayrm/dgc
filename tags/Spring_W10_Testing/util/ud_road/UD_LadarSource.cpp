//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_LadarSource.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for ladar calibration class.  takes intrinsic and extrinsic 
/// (relative to vehicle coordinate system) parameters

UD_LadarCalibration::UD_LadarCalibration(int num, float first_theta, float delta_theta, float x, float y, float z, float pitch)
{
  int i;

  // local names

  num_rays = num;
  first_ray_theta = first_theta;
  ray_delta_theta = delta_theta;
  dx = x;
  dy = y;
  dz = z;
  pitch_angle = pitch;

  // body

  sin_pan_angle = (float *) calloc(num_rays, sizeof(float));
  cos_pan_angle = (float *) calloc(num_rays, sizeof(float));

  float pan_angle;

  for (i = 0; i < num_rays; i++) {
    pan_angle = DEG2RAD(first_ray_theta + ray_delta_theta * (float) i);
    sin_pan_angle[i] = sin(pan_angle);
    cos_pan_angle[i] = cos(pan_angle);
  }

  last_ray_theta = RAD2DEG(pan_angle);
  max_range = 100.0;

  sin_pitch_angle = sin(pitch_angle);
  cos_pitch_angle = cos(pitch_angle);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for UD_LadarSource base class

UD_LadarSource::UD_LadarSource(UD_LadarCalibration *ladarcal)
{
  if (!ladarcal) {
    printf("ladar source requires calibration information\n");
    exit(1);
  }

  // local names

  cal = ladarcal;
  num_rays = cal->num_rays;

  win_dx = win_dy = 0;
}

//----------------------------------------------------------------------------

/// draw ladar hit points projected onto image source.  redness is 
/// proportional to departure from planarity (positive or negative)

void UD_LadarSource::draw(UD_CameraCalibration *cal)
{
  int i;
  float r, g;

  if (!cal) {
    printf("no camera calibration defined\n");
    exit(1);
  }

  if (diff < max_diff) {

    compute_image_positions(cal);

    glPointSize(2);
    glBegin(GL_POINTS);
    for (i = 0; i < num_rays; i++) {
      r = 5.0*fabs(vehicle_y[i]);
      g = 5.0*(1 - fabs(vehicle_y[i]));
      glColor3f(MIN2(r,1), MAX2(g,0),0);
      glVertex2f(image_x[i] + win_dx, image_y[i] + win_dy);
    }
    glEnd();
  }
}

//----------------------------------------------------------------------------

/// project all ladar hit points onto image source with
/// particular calibration

void UD_LadarSource::compute_image_positions(UD_CameraCalibration *cal)
{
  int i;
  float cx, cy, cz;
  
  if (!cal) {
    printf("no camera calibration defined\n");
    exit(1);
  }


  /*
  ladar2vehicle(i, range[i], &vx, &vy, &vz);
  cal->vehicle2camera(vx, vy, vz, &cx, &cy, &cz);
  cal->camera2image(cx, cy, cz, x, y);      
  *h = vy;
  */

  // put ladar source or at least ladar points into road follower class

  for (i = 0; i < num_rays; i++) {
    //    ladar2vehicle(i, range[i], &vehicle_x[i], &vehicle_y[i], &vehicle_z[i]);
    cal->vehicle2camera(vehicle_x[i], vehicle_y[i], vehicle_z[i], &cx, &cy, &cz);
    cal->camera2image(cx, cy, cz, &image_x[i], &image_y[i]);      
  }

//   for (i = 0; i < num_rays; i++)
//     ladar2image(i, range[i], &image_x[i], &image_y[i], &vehicle_y[i], cal);
}

//----------------------------------------------------------------------------

/// transform one ladar hit point in ladar coordinates to vehicle coordinates

void UD_LadarSource::ladar2vehicle(int index, float range, float *vx, float *vy, float *vz)
{
  // spherical to rectangular coordinate transformation
  
  *vx = range * cal->sin_pan_angle[index];
  *vy = range * cal->cos_pan_angle[index] * cal->sin_pitch_angle;
  *vz = range * cal->cos_pan_angle[index] * cal->cos_pitch_angle;

  // translation from ladar position to vehicle origin

  *vx -= cal->dx;
  *vy -= cal->dy;
  *vz -= cal->dz;
}

//----------------------------------------------------------------------------

/// transform one ladar hit point in ladar coordinates to image coordinates.
/// h = vertical distance from ground plane in meters

void UD_LadarSource::ladar2image(int index, float range, float *x, float *y, float *h, UD_CameraCalibration *cal)
{
  float vx, vy, vz, cx, cy, cz;

  ladar2vehicle(index, range, &vx, &vy, &vz);
  cal->vehicle2camera(vx, vy, vz, &cx, &cy, &cz);
  cal->camera2image(cx, cy, cz, x, y);      
  *h = vy;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// load next ladar scan from log file.  assumes file pointer is at beginning of a scan;
/// returns 0 if no more scans left

int UD_LadarSource_Scans::read_scan(FILE *fp)
{
  int i, result;

  result = fscanf(scans_fp, "%lf ", &timestamp);
  if (!result)
    return 0;

  for (i = 0; i < num_rays; i++) {
    result = fscanf(scans_fp, "%f ", &(range[i]));
    if (!result)
      return 0;
    // convert to meters (assuming raw numbers are in cm and max is 8191)
    range[i] /= 100.0;
  }
  fscanf(scans_fp, "\n");

  return 1;
}

//----------------------------------------------------------------------------

/// look ahead at next ladar scan's timestamp.  do this so that we can
/// tell whether we would get better time synchronization
/// by using it instead of the current scan

int UD_LadarSource_Scans::peek_next_scan_timestamp(FILE *fp)
{
  long pos;
  int result;

  pos = ftell(fp);
  result = fscanf(scans_fp, "%lf ", &timestamp);
  if (!result)
    return 0;
  fseek(fp, pos, SEEK_SET);

  return 1;
}

//----------------------------------------------------------------------------

/// capture the ladar scan closest in time to the current image source frame.
/// this is designed to work with an image_list source which has a log file
/// containing timestamps

void UD_LadarSource_Scans::capture(UD_ImageSource *imsrc)
{
  if (imsrc)
    capture(imsrc->timestamp);
}

//----------------------------------------------------------------------------

/// get ladar scan in log file closest to timestamp.   return time "error"
/// or discrepancy--how close could we get?

void UD_LadarSource_Scans::capture(double target_timestamp)
{
  int i, result, scan_num;
  float last_diff;

  // "capture" data
  
  rewind(scans_fp);
  eat_comments(scans_fp);
  
  scan_num = 0;
  
  result = read_scan(scans_fp);
  if (!result) 
    diff = NONE;
  else {
    last_diff = fabs(timestamp - target_timestamp);
//     printf("scan 0: %lf -> %lf (%lf)\n", timestamp, last_diff, target_timestamp);
//     fflush(stdout);
    
    do {
      result = peek_next_scan_timestamp(scans_fp);
      if (result && fabs(timestamp - target_timestamp) < last_diff) {
	read_scan(scans_fp);   // assume if target_timestamp is there then whole scan is there
	last_diff = fabs(timestamp - target_timestamp);
// 	printf("scan %i: %lf -> %lf (%lf)\n", ++scan_num, timestamp, last_diff, target_timestamp);
// 	fflush(stdout);
      }
      else
	break;
    } while (result);
      
    // finish up

    diff = last_diff;
  }

  for (i = 0; i < num_rays; i++)
    ladar2vehicle(i, range[i], &vehicle_x[i], &vehicle_y[i], &vehicle_z[i]);
}

//----------------------------------------------------------------------------

/// simply read next ladar scan from file

void UD_LadarSource_Scans::capture()
{
  int i;
  float f;

  // "capture" data
      
  fscanf(scans_fp, "%lf ", &timestamp);
  printf("ladar: %lf\n", timestamp);
  fflush(stdout);
  for (i = 0; i < num_rays; i++) {
    fscanf(scans_fp, "%f ", &(range[i]));
    // convert to meters
    range[i] /= 100.0;
    //	printf("%.2f ", range[i]);
  }
  fscanf(scans_fp, "\n");
  //      printf("\n");

  for (i = 0; i < num_rays; i++)
    ladar2vehicle(i, range[i], &vehicle_x[i], &vehicle_y[i], &vehicle_z[i]);
}

//----------------------------------------------------------------------------

/// skip comments at beginning of Caltech ladar scan file

void UD_LadarSource_Scans::eat_comments(FILE *fp)
{
  char c;
  char str[80];

  while (1) {
    c = fgetc(fp);
    ungetc(c, fp);
    if (c == '%') {
      fgets (str, 79, fp);
    }
    else
      break;
  }
}

//----------------------------------------------------------------------------

/// constructor for derived Scans LadarSource class.  opens the Caltech
/// scans log file specified in source_path and use calibration parameters
/// in ladarcal

UD_LadarSource_Scans::UD_LadarSource_Scans(char *source_path, UD_LadarCalibration *ladarcal) : UD_LadarSource(ladarcal)
{
  scans_fp = fopen(source_path, "r");
  eat_comments(scans_fp);
    
  range = (float *) calloc(num_rays, sizeof(float));

  vehicle_x = (float *) calloc(num_rays, sizeof(float));
  vehicle_y = (float *) calloc(num_rays, sizeof(float));
  vehicle_z = (float *) calloc(num_rays, sizeof(float));

  image_x = (float *) calloc(num_rays, sizeof(float));
  image_y = (float *) calloc(num_rays, sizeof(float));
}

//----------------------------------------------------------------------------

/// look for and process any ladar-specific command-line flags

void UD_LadarSource_Scans::process_command_line_flags(int argc, char **argv) 
{
  int i;

  for (i = 1; i < argc; i++) {
  }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
