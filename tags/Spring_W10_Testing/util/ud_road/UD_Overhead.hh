//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_Overhead
//----------------------------------------------------------------------------

#ifndef UD_OVERHEAD_DECS

//----------------------------------------------------------------------------

#define UD_OVERHEAD_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glut.h>

#include "opencv_utils.hh"

#include "UD_RoadFollower.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))

//-------------------------------------------------
// class
//-------------------------------------------------

class UD_Overhead
{
  
 public:

  int window_id;                             ///< GLUT identifier for this window 
  int width, height;                         ///< pixel dimensions of window

  UD_RoadFollower *rf;                       ///< road follower object containing current curvature and boundary estimates

  float map_width;                           ///< lateral limits in vehicle coords. (meters) of local map to draw (from -mw/2 to +mw/2)
  float map_depth;                           ///< how far ahead in vehicle coords. (meters) to draw local map (from 0 to md)
  float vehicle_offset;                      ///< how many meters to add to map_depth to include representation of vehicle in map
  float pix_per_meter;                       ///< conversion factor between meters of local map and pixels of overhead window

  float vehicle_width;
  float vehicle_front_axle_to_bumper;
  float vehicle_rear_axle_to_bumper;
  float vehicle_wheelbase;

  GLUquadricObj *glcircle;

  // functions

  UD_Overhead(float, float, float, UD_RoadFollower *);

  float v2o_x(float x) { return (x + 0.5 * map_width) * pix_per_meter; }
  float v2o_z(float z) { return (z + vehicle_offset) * pix_per_meter; }

  void initialize_display(int, int);

  static void sdisplay() { overhead->display(); }
  static void skeyboard(unsigned char key, int x, int y) { overhead->keyboard(key, x, y); }
  static void smouse(int button, int state, int x, int y) { overhead->mouse(button, state, x, y); }

  void draw_vehicle();
  void draw_road();
  void draw_ladar(UD_LadarSource *);
  void draw_gap();
  void draw_circle(float, float, float, int, int, int);

  void display();
  void keyboard(unsigned char, int, int);
  void mouse(int, int, int, int);

  static UD_Overhead *overhead;

};

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
