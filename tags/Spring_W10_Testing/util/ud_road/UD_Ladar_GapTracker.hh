//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_Ladar_GapTracker
//----------------------------------------------------------------------------

#ifndef UD_LADAR_GAPTRACKER_DECS

//----------------------------------------------------------------------------

#define UD_LADAR_GAPTRACKER_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glut.h>

#include "opencv_utils.hh"

// my stuff 

#include "UD_ParticleFilter.hh"
#include "UD_LadarSource.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))

// camera calibration data; in degrees

#define DEFAULT_HFOV        45.0
#define PIX2RAD(x, w, hfov) ((hfov)*(x)/(w))     // can also use y, h, vfov

//-------------------------------------------------
// class
//-------------------------------------------------

class UD_Ladar_GapTracker : public UD_ParticleFilter
{
  
 public:

  // variables

  UD_LadarSource *ladsrc;                              ///< ladar source containing hit information
  float *ladar_hist;                                   ///< absolute value of largest divergence from flatness for this x value
  float ladar_hist_range;                              ///< area to search for road gap (in meters) on either side of center
  int ladar_hist_bins;                                 ///< divide diameter by num bins to get bin width
  float ladar_hist_bin_width;
  float gapwidth;                                      ///< assumed minimum width of road

  float direction_error;

  int w, h;                                            ///< dimensions of source image
  int show_particles;                                  ///< draw particle filter representations?

  // functions

  UD_Ladar_GapTracker(UD_LadarSource *, int, UD_Vector *, UD_Vector *);

  void pf_samp_prior(UD_Vector *);
  void pf_samp_state(UD_Vector *, UD_Vector *);
  void pf_dyn_state(UD_Vector *, UD_Vector *);
  double pf_condprob_zx(UD_Vector *);

  int get_bin_x(int i) { return (float) (i - ladar_hist_bins / 2) * ladar_hist_bin_width; }
  int get_bin(float x) { return ladar_hist_bins / 2 + (int) floor(x / ladar_hist_bin_width); }
  int get_bin_val(float x) { return ladar_hist[get_bin(x)]; }
  float bin_project(float x, float z) { return (x - z * tan(direction_error)); }
  int get_bin(float x, float z) { return get_bin(bin_project(x, z)); }

  void draw();
  void write(FILE *);
  void write(FILE *, char *);
  void iterate();

};

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
