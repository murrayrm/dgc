//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_DominantOrientations.hh"

//----------------------------------------------------------------------------
// minimal subset of UD_Linux_Matrix that UD_DominantOrientations needs
//----------------------------------------------------------------------------

/// make a matrix (1-D array of doubles representing 2-D matrix in 
/// row-major format)

UD_Matrix *make_UD_Matrix(char *name, int rows, int cols)
{
  UD_Matrix *M;

  M = (UD_Matrix *) malloc(sizeof(UD_Matrix));

  M->name = (char *) calloc(MAXSTRLEN, sizeof(char));
  strncpy(M->name, name, MAXSTRLEN);

  M->rows = rows;
  M->cols = cols;

  M->x = (double *) calloc(rows * cols, sizeof(double));

  M->buffsize = rows * cols * sizeof(double);

  return M;
}

//----------------------------------------------------------------------------

/// make a matrix with all of its elements set to x

UD_Matrix *make_all_UD_Matrix(char *name, int rows, int cols, double x)
{
  UD_Matrix *M;

  M = make_UD_Matrix(name, rows, cols);
  set_all_UD_Matrix(x, M);

  return M;
}

//----------------------------------------------------------------------------

/// set all elements of matrix to same value

void set_all_UD_Matrix(double x, UD_Matrix *M)
{
  int i;

  for (i = 0; i < M->rows * M->cols; i++)
    M->x[i] = x;
}

//----------------------------------------------------------------------------

/// free matrix's allocated memory

void free_UD_Matrix(UD_Matrix *M)
{
  if (M) {
    free(M->x);
    free(M);
  }
}

//----------------------------------------------------------------------------

/// generates a gabor wavelet kernel of wavelength lambda, angle given by "orientation" 
/// (in degrees), and either even or odd symmetric in phase (even = 1 or 0).
/// taken from derivation in T. S. Lee, "Image Representation Using 2-D Gabor Wavelets",
/// IEEE Trans. PAMI, Vol. 18, No. 10, October, 1996

UD_Matrix *make_gabor_kernel_UD_Matrix(double lambda, double orientation, double even, int size)
{
  int i, j;
  double c, sigma, f, k, u, v, theta, ai, aj, norm_f, sum, mean;
  UD_Matrix *gabor_f, *gaussian, *filt_ga;

  sigma = (double) size / 9;   // spread of Gaussian

  f = (2 * PI) / lambda;
  theta = DEG2RAD(orientation);

  u = f * cos(theta);
  v = f * sin(theta);
  c = (double) size / 2;
  
  gabor_f = make_all_UD_Matrix("gabor_f", size, size, 1.0);
  gaussian = make_all_UD_Matrix("gaussian", size, size, 0.0);
  filt_ga = make_UD_Matrix("filt_ga", size, size);

  for (i = 1, sum = 0; i <= size; i++) 
    for (j = 1; j <= size; j++) { 
      ai = (i - c) * cos(theta) + (j - c) * sin(theta); 
      aj = (i - c) * -sin(theta) + (j - c) * cos(theta); 
      MATRC(gaussian, i - 1, j - 1) = exp(-(1 / (8 * sigma * sigma)) * (4 * ai * ai + aj * aj)); 
      MATRC(gabor_f, i - 1, j - 1) = MATRC(gaussian, i - 1, j - 1) * sin(u * (i - c) + v * (j - c) + (even * PI_2));
      sum += MATRC(gabor_f, i - 1, j - 1);
    }

  // remove dc_component  

  mean = sum / (double) (size * size);
  for (i = 0; i < size; i++) 
    for (j = 0; j < size; j++)  
      MATRC(gabor_f, i, j) = MATRC(gabor_f, i, j) - mean;

  // L2-normalize

  norm_f = 0;
  for (i = 0; i < size; i++) 
    for (j = 0; j < size; j++)  
      norm_f += MATRC(gabor_f, i, j) * MATRC(gabor_f, i, j);
  norm_f = sqrt(norm_f);

  for (i = 0; i < size; i++) 
    for (j = 0; j < size; j++)  
      MATRC(filt_ga, i, j) = MATRC(gabor_f, i, j) / norm_f; 
 
  // clean up

  free_UD_Matrix(gabor_f);
  free_UD_Matrix(gaussian);

  // finish

  return filt_ga;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for dominant orientation base class.  allocates and calculates
/// bank of oriented Gabor kernels and support structures for carrying out
/// convolutions using FFTW Fast Fourier Transform library (http://www.fftw.org)

UD_DominantOrientations::UD_DominantOrientations(int width, int height, int num_orientations, 
						 float start_orientation, float end_orientation, float wavelength)
{
  int i;

  // local names

  w = width;
  h = height;

  gabor_num_orientations                = num_orientations;     
  gabor_start_orientation               = start_orientation;   // in degrees
  gabor_end_orientation                 = end_orientation;     // in degrees  
  gabor_wavelength                      = wavelength;   

  // default values

  show                                  = FALSE;
    
  // body

  gabor_delta_orientation  = (gabor_end_orientation - gabor_start_orientation) / (float) gabor_num_orientations;

  make_gabor_bank_fft_float();
  initialize_gabor_fft_float();
}

//----------------------------------------------------------------------------

/// compute dominant orientations over one image.  calculate response 
/// of all oriented Gabor filters to image and determine which filter 
/// elicits maximum response at each pixel

void UD_DominantOrientations::compute(IplImage *im)
{
  run_gabor_fft_float(im);

  find_gabor_max_responses();
  //  filter_out_weak_responses();
}

//----------------------------------------------------------------------------

/// make one custom image-sized kernel for FFT by shifting, padding conventional
/// square kernel 

float *UD_DominantOrientations::make_gabor_kernel_fft_float(float orientation, float even, float **kernel, int *size)
{
  int i, j, row, col, myr, myc, rr, cc;
  float r, c, f, u, v;
  UD_Matrix *filt_ga_temp;
  float *filt_ga;

  *size = (int) (15.0 * gabor_wavelength / (1.5 * PI));

  filt_ga_temp = make_gabor_kernel_UD_Matrix(gabor_wavelength, orientation, even, *size);

  filt_ga = (float *) fftwf_malloc(sizeof(float) * h * w);
  for (i = 0; i < h * w; i++)
    filt_ga[i] = 0.0;

  // now copy filt_ga_temp into filt_ga just so... (still not sure I'm doing this entirely correctly)

  rr = filt_ga_temp->rows;
  cc = filt_ga_temp->cols;

  for (row = -rr/2; row < rr/2; row++)
    for (col = -cc/2; col < cc/2; col++) {
      myr = row % h;
      if (myr < 0)
	myr += h;
      myc = col % w;
      if (myc < 0)
	myc += w;
      filt_ga[w * myr + myc] = MATRC(filt_ga_temp, row + rr/2, col + cc/2);
    }

  *kernel = (float *) malloc(*size * *size * sizeof(float));
  for (i = 0; i < *size * *size; i++)
    (*kernel)[i] = filt_ga_temp->x[i];

  free_UD_Matrix(filt_ga_temp);

  return filt_ga;
}

//----------------------------------------------------------------------------

/// create bank of FFTW-specialized Gabor kernels with dimensions 
/// num_orientations x 2 (odd and even).  note that
/// every kernel is the size of the image and all have the same wavelength
 
void UD_DominantOrientations::make_gabor_bank_fft_float()
{
  int i, orientation_num, even;
  float orientation;

  gabor_bank = (float ***) calloc(gabor_num_orientations, sizeof(float **));
  for (i = 0; i < gabor_num_orientations; i++) 
    gabor_bank[i] = (float **) calloc(2, sizeof(float *));

  gabor_bank_fft_float = (float ***) calloc(gabor_num_orientations, sizeof(float **));
  for (i = 0; i < gabor_num_orientations; i++) 
    gabor_bank_fft_float[i] = (float **) calloc(2, sizeof(float *));

  for (i = 0, orientation = gabor_start_orientation, orientation_num = 0; 
       orientation_num < gabor_num_orientations; 
       orientation += gabor_delta_orientation, orientation_num++) {
    for (even = 0; even <= 1; even++) {
      gabor_bank_fft_float[i][even] = make_gabor_kernel_fft_float(orientation, even, &gabor_bank[i][even], &gabor_size);  
      printf("%i: orientation = %.2lf, even = %i, wavelength = %.2lf (%i x %i)\n", i, orientation, even, gabor_wavelength, h, w);
    }
    i++;
  }
}

//----------------------------------------------------------------------------

/// make IPL_DEPTH_32F image whose data buffer is properly byte-aligned for 
/// use with FFTW.  another way to do this would involve using 
/// cvSetMemoryManager to make fftw_malloc get called for images automatically

IplImage *UD_DominantOrientations::make_fft_float_IplImage(int w, int h)
{
  IplImage *im;

  im = cvCreateImage(cvSize(w, h), IPL_DEPTH_32F, 1);
  cvFree((void **) &im->imageData);
  im->imageData = (char *) fftwf_malloc(sizeof(float) * w * h);

  return im;
}

//----------------------------------------------------------------------------

/// create auxiliary data structures for convolving Gabor kernels
/// with source image using FFTW library, as well as determining
/// which orientation produced maximum response at each pixel

void UD_DominantOrientations::initialize_gabor_fft_float()
{
  int i, j, t, u, v, double_comp_size;
  int fft_width;                    // the limit used for the complex domain
  fftwf_plan **gabor_fft_float_plan;       
  float *g_out;

  fft_width = w / 2 + 1;
  double_comp_size = 2 * h * fft_width;

  // input image

  gabor_max_response_index = cvCreateImage(cvSize(w, h), IPL_DEPTH_8U, 1);
  gabor_max_response_intensity = cvCreateImage(cvSize(w, h), IPL_DEPTH_8U, 1);
  gabor_new_max_mask = cvCreateImage(cvSize(w, h), IPL_DEPTH_8U, 1);
  image_gray_char = cvCreateImage(cvSize(w, h), IPL_DEPTH_8U, 1);
  image_gray_float = make_fft_float_IplImage(w, h);
  image_fft_float_out = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fft_width * h);
  image_fft_float_out_sum = (float *) fftwf_malloc(sizeof(float) * fft_width * h);
  UD_flush_printf("image fft: planning...");
  image_fft_float_plan = fftwf_plan_dft_r2c_2d(h, w, (float *) image_gray_float->imageData, image_fft_float_out, FFTW_PATIENT); 

  UD_flush_printf("done\n");

  // gabor filters

  gabor_fft_float_out = (fftwf_complex ***) calloc(gabor_num_orientations, sizeof(fftwf_complex **));
  gabor_fft_float_plan = (fftwf_plan **) calloc(gabor_num_orientations, sizeof(fftwf_plan *));
  gabor_fft_float_out_sum = (float ***) calloc(gabor_num_orientations, sizeof(float **));

  // products of gabor filters and image

  product_inv_fft_float_in = (fftwf_complex ***) calloc(gabor_num_orientations, sizeof(fftwf_complex **));
  conv_image = (IplImage ***) calloc(gabor_num_orientations, sizeof(IplImage **));
  gabor_response = (IplImage **) calloc(gabor_num_orientations, sizeof(IplImage *));
  product_inv_fft_float_plan = (fftwf_plan **) calloc(gabor_num_orientations, sizeof(fftwf_plan *));

  for (i = 0; i < gabor_num_orientations; i++) {

    //    printf("preparing ffts for gabor pair %i\n", i);

    gabor_fft_float_out[i] = (fftwf_complex **) calloc(2, sizeof(fftwf_complex *));
    gabor_fft_float_plan[i] = (fftwf_plan *) calloc(2, sizeof(fftwf_plan));
    gabor_fft_float_out_sum[i] = (float **) calloc(2, sizeof(float *));

    product_inv_fft_float_in[i] = (fftwf_complex **) calloc(2, sizeof(fftwf_complex *));
    conv_image[i] = (IplImage **) calloc(2, sizeof(IplImage *));
    gabor_response[i] = make_fft_float_IplImage(w, h);
    product_inv_fft_float_plan[i] = (fftwf_plan *) calloc(2, sizeof(fftwf_plan));

    for (j = 0; j < 2; j++) {

      gabor_fft_float_out[i][j] = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fft_width * h);
      gabor_fft_float_plan[i][j] = fftwf_plan_dft_r2c_2d(h, w, gabor_bank_fft_float[i][j], gabor_fft_float_out[i][j], FFTW_ESTIMATE);
      gabor_fft_float_out_sum[i][j] = (float *) fftwf_malloc(sizeof(float) * fft_width * h);
      fftwf_execute(gabor_fft_float_plan[i][j]);

      g_out = (float *) gabor_fft_float_out[i][j];
      for (t = 0, u = 1, v = 0; t < double_comp_size; t += 2, u += 2) 
	gabor_fft_float_out_sum[i][j][v++] = g_out[t] + g_out[u];

      // don't need these any more

      fftwf_free(gabor_bank_fft_float[i][j]);
      fftwf_free(gabor_fft_float_plan[i][j]);

      // product

      product_inv_fft_float_in[i][j] = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fft_width * h);
      conv_image[i][j] = make_fft_float_IplImage(w, h);
      product_inv_fft_float_plan[i][j] = fftwf_plan_dft_c2r_2d(h, w, product_inv_fft_float_in[i][j], (float *) conv_image[i][j]->imageData, FFTW_PATIENT);  // FFTW_MEASURE
    }

    free(gabor_bank_fft_float[i]);
    free(gabor_fft_float_plan[i]);
  }

  // finish freeing

  free(gabor_bank_fft_float);
  free(gabor_fft_float_plan);
}

//----------------------------------------------------------------------------

/// carry out convolution of source image with bank of Gabor filters
/// using FFTW library.  this simply creates an array of convolved images,
/// one for each orientation--we do not yet know which orientation got
/// a maximal response at each image location

void UD_DominantOrientations::run_gabor_fft_float(IplImage *source_im)
{
  int i, j, t, u, v, x, y, r, c;
  int h, w, double_comp_size;
  int fft_width;
  float fft_normalization_factor;

  h = source_im->height;
  w = source_im->width;
  fft_width = w / 2 + 1;
  fft_normalization_factor = 1.0 / (float) (h * w);

  // convert raw image to FFT domain

  cvConvertImage(source_im, image_gray_char);    // color to gray
  cvConvert(image_gray_char, image_gray_float);  
  fftwf_execute(image_fft_float_plan);

  // convolve image with each gabor filter

  float ac, bd;
  float *g_out, *g_inv_in, *im_out;

  double_comp_size = 2 * h * fft_width;

  im_out = (float *) image_fft_float_out;
  for (t = 0, u = 1, v = 0; t < double_comp_size; t += 2, u += 2) 
    image_fft_float_out_sum[v++] = im_out[t] + im_out[u];
  
  for (i = 0; i < gabor_num_orientations; i++) {
    for (j = 0; j < 2; j++) {
	
      g_out = (float *) gabor_fft_float_out[i][j];
      g_inv_in = (float *) product_inv_fft_float_in[i][j];
	
      for (t = 0, u = 1, v = 0; t < double_comp_size; t += 2, u += 2, v++) {
	ac = g_out[t] * im_out[t];
	bd = g_out[u] * im_out[u];
	g_inv_in[t] = ac - bd;
	g_inv_in[u] = gabor_fft_float_out_sum[i][j][v] * image_fft_float_out_sum[v] - ac - bd;
      }

      // invert fft 
      
      fftwf_execute(product_inv_fft_float_plan[i][j]);
    }

    // odd^2 + even^2
    
    cvScale(conv_image[i][0], conv_image[i][0], fft_normalization_factor);
    cvScale(conv_image[i][1], conv_image[i][1], fft_normalization_factor);

    cvMul(conv_image[i][0], conv_image[i][0], conv_image[i][0]);
    cvMul(conv_image[i][1], conv_image[i][1], conv_image[i][1]);
    cvAdd(conv_image[i][0], conv_image[i][1], gabor_response[i]);
  }

  // two other possibilities for complex multiplication:

  // (1) cvMulSpectrums--not exactly sure about complex number format, but theoretically it does what I want
  // (2) this:
  //       cvMul(gabor_real[i][j], im_real, real_prod);
  //       cvMul(gabor_imag[i][j], im_imag, imag_prod);
  //       cvSub(real_prod, imag_prod, real_part);
  //       cvMul(gabor_sum[i][j], im_sum, sum_prod);
  //       cvSub(sum_prod, real_prod, sum_real_diff);
  //       cvSub(sum_real_diff, imag_prod, imag_part);

  // interleave real_part and imag_part into product_inv_fft_float_in
  //   --could use cvMerge with 2-channel float image as destination
}

//----------------------------------------------------------------------------

/// given a set of Gabor filter responses to the source image, figure
/// out which orientation got maximum reponse at each image pixel.  
/// for display, also creates grayscale image indicating "winning"
/// orientation index by pixel intensity

void UD_DominantOrientations::find_gabor_max_responses()
{ 
  int i, scale_factor;

  // keep max values in gabor_response[0] to avoid copy
  gabor_max_response_value = gabor_response[0];

  cvSetZero(gabor_max_response_index);

  for (i = 1; i < gabor_num_orientations; i++) {
    cvCmp(gabor_response[i], gabor_max_response_value, gabor_new_max_mask, CV_CMP_GT);      
    cvSet(gabor_max_response_index, cvScalarAll(i), gabor_new_max_mask);                  // for actual indexing
    cvMax(gabor_response[i], gabor_max_response_value, gabor_max_response_value);         // update max. values
  }  

  if (show) {
    if (gabor_num_orientations > 1)
      scale_factor = 255 / (gabor_num_orientations - 1);
    else
      scale_factor = 0;
    cvScale(gabor_max_response_index, gabor_max_response_intensity, scale_factor);        // for grayscale display
  }
}

//----------------------------------------------------------------------------

/// suppress image locations where highest filter response was not high enough
/// [not using this right now]

void UD_DominantOrientations::filter_out_weak_responses()
{
  CvPoint pmin, pmax;
  double maxval, minval;
  CvScalar avgval;

  cvMinMaxLoc(gabor_max_response_value, &minval, &maxval, &pmin, &pmax);
  avgval = cvAvg(gabor_max_response_value);

  cvCmpS(gabor_max_response_value, avgval.val[0], gabor_new_max_mask, CV_CMP_LT);      
  cvSet(gabor_max_response_index, cvScalarAll(0), gabor_new_max_mask); 
  
  printf("max response = %lf, min = %lf, avg = %lf\n", maxval, minval, avgval.val[0]);
}

//----------------------------------------------------------------------------

/// look for and execute command-line flags related to dominant orientation
/// calculations

void UD_DominantOrientations::process_command_line_flags(int argc, char **argv) 
{
  int i;

  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i],                             "-showdom")) 
      show = TRUE;
  }
}

//----------------------------------------------------------------------------


