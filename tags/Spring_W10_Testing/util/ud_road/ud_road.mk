UDROAD_PATH = $(DGC)/util/ud_road

UDROAD_DEPEND_LIBS = 

UDROAD_DEPEND_SOURCES = \
    $(UDROAD_PATH)/Makefile \
    $(UDROAD_PATH)/Makefile.UD \
    $(UDROAD_PATH)/UD_DominantOrientations.cpp \
    $(UDROAD_PATH)/UD_DominantOrientations.hh \
    $(UDROAD_PATH)/UD_Error.cpp \
    $(UDROAD_PATH)/UD_Error.hh \
    $(UDROAD_PATH)/UD_FirewireCamera.cpp \
    $(UDROAD_PATH)/UD_FirewireCamera.hh \
    $(UDROAD_PATH)/UD_ImageSource.cpp \
    $(UDROAD_PATH)/UD_ImageSource.hh \
    $(UDROAD_PATH)/UD_LadarSource.cpp \
    $(UDROAD_PATH)/UD_LadarSource.hh \
    $(UDROAD_PATH)/UD_Ladar_GapTracker.cpp \
    $(UDROAD_PATH)/UD_Ladar_GapTracker.hh \
    $(UDROAD_PATH)/UD_Linux_Movie_AVI.cpp \
    $(UDROAD_PATH)/UD_Linux_Movie_AVI.hh \
    $(UDROAD_PATH)/UD_Linux_Time.cpp \
    $(UDROAD_PATH)/UD_Linux_Time.hh \
    $(UDROAD_PATH)/UD_Logger.cpp \
    $(UDROAD_PATH)/UD_Logger.hh \
    $(UDROAD_PATH)/UD_OnOff.cpp \
    $(UDROAD_PATH)/UD_OnOff.hh \
    $(UDROAD_PATH)/UD_Overhead.cpp \
    $(UDROAD_PATH)/UD_Overhead.hh \
    $(UDROAD_PATH)/UD_ParticleFilter.cpp \
    $(UDROAD_PATH)/UD_ParticleFilter.hh \
    $(UDROAD_PATH)/UD_Random.cpp \
    $(UDROAD_PATH)/UD_Random.hh \
    $(UDROAD_PATH)/UD_RoadFollower.cpp \
    $(UDROAD_PATH)/UD_RoadFollower.hh \
    $(UDROAD_PATH)/UD_VP_MultiTracker.cpp \
    $(UDROAD_PATH)/UD_VP_MultiTracker.hh \
    $(UDROAD_PATH)/UD_VP_PositionTracker.cpp \
    $(UDROAD_PATH)/UD_VP_PositionTracker.hh \
    $(UDROAD_PATH)/UD_VP_Tracker.cpp \
    $(UDROAD_PATH)/UD_VP_Tracker.hh \
    $(UDROAD_PATH)/UD_VP_VelocityTracker.cpp \
    $(UDROAD_PATH)/UD_VP_VelocityTracker.hh \
    $(UDROAD_PATH)/UD_VanishingPoint.cpp \
    $(UDROAD_PATH)/UD_VanishingPoint.hh \
    $(UDROAD_PATH)/conversions.cpp \
    $(UDROAD_PATH)/conversions.h \
    $(UDROAD_PATH)/decs.h \
    $(UDROAD_PATH)/main.cpp \
    $(UDROAD_PATH)/opencv_utils.hh 

UDROAD_DEPEND = \
	$(UDROAD_DEPEND_LIBS) \
	$(UDROAD_DEPEND_SOURCES)

# TODO: Get rid of this by installing standard libraries
UDROAD_LIBINCDIR=/usr/local/road_libs_and_incs


##### FFT include and library information ###################################

FFTW_LIBS 	= -lfftw3 -lfftw3f
FFTW_INCDIRS 	= -I$(UDROAD_LIBINCDIR)/fftw
FFTW_LIBDIRS 	= -L$(UDROAD_LIBINCDIR)/fftw

##### AVI include and library information ###################################

AVI_LIBS 	= -laviplay
AVI_INCDIRS 	= -I$(UDROAD_LIBINCDIR)/avi/include
AVI_LIBDIRS 	= -L$(UDROAD_LIBINCDIR)/avi/lib

##### OpenCV include and library information ################################

OPENCV_LIBS = -lcv -lcxcore -lhighgui
OPENCV_INCDIRS 	= -I$(UDROAD_LIBINCDIR)/opencv/include -I/usr/local/include/opencv
OPENCV_LIBDIRS 	= -L$(UDROAD_LIBINCDIR)/opencv/lib -L/usr/local/lib/

##### OPENGL include and library information ################################

OPENGL_LIBS = -lGL -lGLU -lglut -lXmu 
OPENGL_INCDIRS 	= -I$(UDROAD_LIBINCDIR)/include -I/usr/include/GL 
OPENGL_LIBDIRS 	= -L/usr/X11R6/lib -L/usr/lib -L$(UDROAD_LIBINCDIR)/lib 

##### Firewire include and library information ##############################

FIREWIRE_LIBS = -ldc1394_control -lraw1394
FIREWIRE_INCDIRS 	= -I/usr/src/linux/drivers/ieee1394/
FIREWIRE_LIBDIRS 	= -L/usr/lib -L/usr/local/lib

##### Include and linking flags ############################################

UDROAD_INCLUDE = $(FFTW_INCDIRS) \
                 $(AVI_INCDIRS) \
                 $(OPENCV_INCDIRS) \
                 $(OPENGL_INCDIRS) \
                 $(FIREWIRE_INCDIRS)

UDROAD_LDFLAGS = $(FFTW_LIBDIRS) \
                 $(AVI_LIBDIRS) \
                 $(OPENCV_LIBDIRS) \
                 $(OPENGL_LIBDIRS) \
                 $(FFTW_LIBS) \
                 $(ATLAS_LIBS) \
                 $(AVI_LIBS) \
                 $(OPENCV_LIBS) \
                 $(OPENGL_LIBS) \
                 $(FIREWIRE_LIBS) \
                 $(FIREWIRE_LIBDIRS) \
                 -Wl,-rpath,$(UDROAD_LIBINCDIR),-rpath,$(UDROAD_LIBINCDIR) -lm

