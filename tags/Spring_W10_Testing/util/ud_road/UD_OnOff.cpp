//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_OnOff.hh"
	
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for on/off road decision class.  threshold = 0.2 is 
/// tuned for 160 x 120 images, hence check that image dimensions match--
/// remove if further tuning done for other size.  also assumes max
/// vote level of 256, which would not be correct if HAVE_GEFORCE6 is defined

UD_OnOff::UD_OnOff(UD_ImageSource *imsrc, float threshold, int time_window, float majority_fraction)
{
  // local names

  KL_threshold = threshold;                
  KL_time_window = time_window;            
  KL_majority_fraction = majority_fraction;
  
  // default values

  print_onoff                      = FALSE;
  do_onoff                         = TRUE;
  onroad_state                     = TRUE;

  // body

  // threshold only valid for 160 x 120 images or 180 x 120 images

  if (imsrc->im->height != 120 || (imsrc->im->width != 160 && imsrc->im->width != 180)) {
    printf("not using on/off road test -- image size must be 160 x 120 or 180 x 120\n");
    do_onoff = FALSE;
  }

  // to redden display when we think we're off road

  redim = cvCreateImage(cvSize(imsrc->im->width, imsrc->im->height), IPL_DEPTH_8U, 3);
  cvSet(redim, cvScalar(1, 0, 0));

#ifdef HAVE_GEFORCE6
  printf("GeForce: different number of gray levels in effect\n");
  exit(1);
#endif

  // assume 256 gray levels in vote function

  KL_num_vote_levels = 256;
  KL_uniform_prob = 1.0 / (float) KL_num_vote_levels;
  KL_histogram = (float *) calloc(KL_num_vote_levels, sizeof(float));
  
  // initialize array to store last time_window KL values
  // this wraps around, so keep track of current index and whether we've wrapped

  KL_history = (float *) calloc(KL_time_window, sizeof(float));
  KL_history_full = FALSE;
  KL_index = 0;
}

//----------------------------------------------------------------------------

/// return fraction of last KL_time_window KL measurements
/// that were over KL_threshold.  it's up to caller to 
/// threshold this on KL_majority_fraction or use directly as confidence

void UD_OnOff::compute_confidence(IplImage *im)
{
  int i, num_on;
 
  if (!do_onoff) {
    onroad_confidence = 1.0;
    onroad_state = TRUE;
    return;
  }
				
  // record current value in history buffer (which wraps around)

  KL_history[KL_index] = KL_from_uniform(im);

  if (print_onoff)
    printf("%i %.2f ", KL_index, KL_history[KL_index]);
 
  if (KL_index < KL_time_window - 1)
    KL_index++;
  else {
    KL_history_full = TRUE;
    KL_index = 0;
  }

  // threshold and filter based on past history 

  if (!KL_history_full) {
    for (i = 0, num_on = 0; i < KL_index; i++)
      if (KL_history[i] > KL_threshold)
	num_on++;
    onroad_confidence = (float) num_on / (float) KL_index;
  }
  else {
    for (i = 0, num_on = 0; i < KL_time_window; i++)
      if (KL_history[(i + KL_index) % KL_time_window] > KL_threshold)
	num_on++;
    onroad_confidence = (float) num_on / (float) KL_time_window;
  }
  
  if (print_onoff)
    printf("index %i full %i, on %i conf %.2f KL %.2f\n", KL_index, KL_history_full, num_on, onroad_confidence, KL_history[KL_index-1]);

  onroad_state = onroad_confidence >= KL_majority_fraction;   
}

//----------------------------------------------------------------------------

/// compute Kullback-Leibler divergence of vote function from uniform distribution

float UD_OnOff::KL_from_uniform(IplImage *im)
{
  int i, j;
  float total, KL, prob;

  // compute probability of each vote total in this image

  for (i = 0; i < KL_num_vote_levels; i++)
    KL_histogram[i] = 0;

  for (j = 0, total = 0.0; j < im->height; j++)
    for (i = 0; i < im->width; i++) {
      KL_histogram[(int) ((float) (KL_num_vote_levels - 1) * FLOAT_IMXY(im, i, j))]++;
      total++;
    }

  // compute divergence from uniform distribution

  for (i = 0, KL = 0; i < KL_num_vote_levels; i++) {
    prob = KL_histogram[i] / total;  	
    //    if (print_onoff)
    //      printf("%i %f\n", i, prob);
    if (prob) 
      KL += KL_uniform_prob * (log(prob / KL_uniform_prob));
  }

  return -KL;
}

//----------------------------------------------------------------------------

/// process command-line flags relevant to on/off road decision calculations

void UD_OnOff::process_command_line_flags(int argc, char **argv) 
{
  int i;

  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i],                             "-klthresh"))
      KL_threshold = atof(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-klthreshold"))
      KL_threshold = atof(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-klmajority"))
      KL_majority_fraction = atof(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-printonoff"))
      print_onoff = TRUE;
    else if (!strcmp(argv[i],                        "-noonoff"))
      do_onoff = FALSE;
  }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
