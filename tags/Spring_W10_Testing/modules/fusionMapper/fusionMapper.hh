#ifndef FUSIONMAPPER_HH
#define FUSIONMAPPER_HH

#include <unistd.h>
#include <pthread.h>

#include "StateClient.h"
#include "DGCutils"

#include "CMapPlus.hh"
#include "rddf.hh"
#include "CCorridorPainter.hh"
#include "CCostPainter.hh"
#include "StaticPainter.hh"

using namespace std;

//Sparrow Display Functions
int user_quit(long arg);
int user_pause(long arg);
int user_reset(long arg);
int user_change(long arg);

class StereoAvgCell {
public:
  int numPoints;
  double cellVal;

  StereoAvgCell() {
    numPoints = 0;
    cellVal = 0.0;
  };
  ~StereoAvgCell() {};

  bool operator!=(const StereoAvgCell &rhs) {
    if(this->numPoints==rhs.numPoints) {
      return true;
    }
    return false;
  };
};


struct FusionMapperOptions {
  int optLadar, optStereo;
  int optSNKey;
  int optAverage;
  int optMax;
  double optObsGrowingSize;
  double optRDDFScaling;
  double optMaxSpeed;
  int optNonBinary;
};

class FusionMapperStatusInfo {
private:
  unsigned long long startProcessTime;
  unsigned long long endProcessTime;

public:
  int numMsgs;
  int msgSize;
  double msgSizeAvg;
  double processTime;
  double processTimeAvg;

  FusionMapperStatusInfo() {reset();};
  ~FusionMapperStatusInfo() {};

  void startProcessTimer() {
    DGCgettime(startProcessTime);
  };

  void endProcessTimer(int newMsgSize) {
    DGCgettime(endProcessTime);
    msgSize = newMsgSize;
    msgSizeAvg = (msgSize + numMsgs*msgSizeAvg)/(numMsgs+1);
    processTime = ((double)(endProcessTime-startProcessTime))/(1000.0);
    processTimeAvg = (processTime + processTimeAvg*numMsgs)/(numMsgs+1);
    numMsgs++;
  };

  void reset() {
    numMsgs=0;
    msgSize=0;
    msgSizeAvg=0.0;
    processTime=0.0;
    processTimeAvg=0.0;
  };
};
  

class FusionMapper : public CStateClient {
public:
  FusionMapper(int skynetKey, FusionMapperOptions mapperOptsArg);
  ~FusionMapper();
  
  void ActiveLoop();
  
  void ReceiveDataThread_LadarRoof();

  void ReceiveDataThread_LadarBumper();

  void ReceiveDataThread_Stereo();

  void ReceiveRoadDataThread();

  void UpdateSparrowVariablesLoop();

  void PlannerListenerThread();
  
  void SparrowDisplayLoop();

private:

  FusionMapperOptions mapperOpts;

  //Info Structs for each module we receive from
  FusionMapperStatusInfo _infoLadarFeederRoof;
  FusionMapperStatusInfo _infoLadarFeederBumper;
  FusionMapperStatusInfo _infoStereoFeeder;

  //And one for output info
  FusionMapperStatusInfo _infoFusionMapper;

  char* m_pLadarRoofMapDelta;
  char* m_pLadarBumperMapDelta;
  char* m_pStereoMapDelta;
  double* m_pRoadInfo;


  //double* m_pRoadInfo;

  double* m_pRoadReceive;

  double* m_pRoadX;
  double* m_pRoadY;
  double* m_pRoadW;
  int m_pRoadPoints;

  pthread_mutex_t m_mapMutex;

  CMap fusionMap;
  
  RDDF fusionRDDF;
  
  CCorridorPainter fusionCorridorPainter;
  CCostPainter fusionCostPainter;
  CCorridorPainter fusionCorridorReference;
  StaticPainter fusionStaticPainter;

  CCorridorPainter fusionGrowPainter;
	
  int layerID_stereoElev;

  int layerID_stereoAvg;

  int layerID_static;

  int layerID_grown;
  
  int layerID_ladarElev;
  double noDataVal_ladarElev;
  double outsideMapVal_ladarElev;
  
  int layerID_cost;
  double noDataVal_cost;
  double outsideMapVal_cost;

  int layerID_number;
  long long noDataVal_number;
  long long outsideMapVal_number;
 
  int layerID_corridor;
  
  double rddf_insideVal;
  double rddf_outsideVal;
  double rddf_tracklineVal;
  double rddf_edgeVal;

  // Temporary way to debug
  ofstream delta_log;

  int _QUIT;
  int _PAUSE;
};

#endif
