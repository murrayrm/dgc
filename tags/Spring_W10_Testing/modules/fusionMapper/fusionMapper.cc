#include "fusionMapper.hh"

#define MAX_DELTA_SIZE 999999
#define MAX_ROAD_SIZE 50

FusionMapper::FusionMapper(int skynetKey, FusionMapperOptions mapperOptsArg) 
  : CSkynetContainer(SNfusionmapper, skynetKey), delta_log("logs/delta.fm.log") {
  printf("Welcome to fusionmapper init\n");
  _QUIT = 0;
  _PAUSE = 0;

  mapperOpts = mapperOptsArg;

  delta_log << "# fusionMapper produced this.  Time then size." << endl;
    
  //m_pMapDelta = new char[MAX_DELTA_SIZE];
  // static allocated for memory efficiency, if larger than 100 pts, change this
  m_pRoadReceive = new double[MAX_ROAD_SIZE * 3 + 1];
  m_pRoadX = new double[MAX_ROAD_SIZE];
  m_pRoadY = new double[MAX_ROAD_SIZE];
  m_pRoadW = new double[MAX_ROAD_SIZE];
  m_pRoadPoints = 0;

  // Constants in GlobalConstants.h
  fusionMap.initMap(0.0, 0.0, 
		  FUSIONMAPPER_NUMROWSCOLS, FUSIONMAPPER_NUMROWSCOLS,
		  FUSIONMAPPER_RESROWSCOLS, FUSIONMAPPER_RESROWSCOLS,
		  0);

#warning "magic constants here. need comments/explication. wrong constants will stop the rddf from being painted in"
  layerID_ladarElev = fusionMap.addLayer<double>(0, -1);
  layerID_cost    = fusionMap.addLayer<double>(0.5e-3, 1e-4);
  layerID_number = fusionMap.addLayer<long long>(0, -1);
  layerID_corridor = fusionMap.addLayer<double> (0, -1);
  layerID_static = fusionMap.addLayer<double>(0, -1);

  //layerID_grown = fusionMap.addLayer<double>(0.5e-3, 1e-4);

  StereoAvgCell noData, outsideMap;
  noData.numPoints = 0;
  outsideMap.numPoints = -1;

  layerID_stereoAvg = fusionMap.addLayer<StereoAvgCell>(noData, outsideMap);
  layerID_stereoElev = fusionMap.addLayer<double>(0, -1);
  
  fusionCorridorPainter.initPainter(&(fusionMap), 
				    layerID_cost, 
				    &(fusionRDDF),
				    1.0,
				    1e-3,
				    5.0,
				    4.5,
				    0.5,
				    1);
  /*
  fusionGrowPainter.initPainter(&(fusionMap), 
				    layerID_grown, 
				    &(fusionRDDF),
				    1.0,
				    1e-3,
				    5.0,
				    4.5,
				    0.5,
				    1);
  */
  fusionCorridorReference.initPainter(&(fusionMap), layerID_corridor, &(fusionRDDF), 1.0, 1e-3, 5, 4.5, .5, 0);
  

  fusionCostPainter.initPainter(&(fusionMap),
				layerID_cost,
				layerID_ladarElev,
				layerID_corridor, mapperOpts.optNonBinary,
				layerID_grown, 1.5, 0
				);  

  fusionStaticPainter.Initialize(&(fusionMap), layerID_static);
  
  DGCcreateMutex(&m_mapMutex);
}


FusionMapper::~FusionMapper() {
  //FIX ME
  DGCdeleteMutex(&m_mapMutex);
  printf("Thank you for using the fusionmapper destructor\n");
}


void FusionMapper::ActiveLoop() {
  //TODO - Give this a sparrow/sngui display
  printf("Entering FusionMapper active-loop\n");
  printf("Press Ctrl+C to quit\n");

  int socket_num = m_skynet.get_send_sock(SNfusiondeltamap);

  void* deltaPtr = NULL;
  int deltaSize = 0;

  double UTMNorthing;
  double UTMEasting;
  int int_northing[4];
  int int_easting[4];
  
  double cellval;

  NEcoord exposedRow[4], exposedCol[4];

    while(!_QUIT) {
    _infoFusionMapper.startProcessTimer();
    UpdateState();


#warning "Should change updateVehicleLoc to be updateMapCenter"
    DGClockMutex(&m_mapMutex);
    fusionMap.updateVehicleLoc(m_state.Northing, m_state.Easting);
    fusionMap.getExposedRowBoxUTM(exposedRow);
    fusionMap.getExposedColBoxUTM(exposedCol);
    fusionCorridorPainter.paintChanges(exposedRow, exposedCol);    
    fusionCorridorReference.paintChanges(exposedRow, exposedCol);
    //fusionGrowPainter.paintChanges(exposedRow, exposedCol);
    //fusionCostPainter.growChanges(exposedRow);
    //fusionCostPainter.growChanges(exposedCol);
    
    fusionStaticPainter.PaintDelta();
    //deltaPtr = fusionMap.serializeDelta<double>(layerID_grown, deltaSize);
    deltaPtr = fusionMap.serializeDelta<double>(layerID_cost, deltaSize);
    DGCunlockMutex(&m_mapMutex);    


    int sent_OK;

    //code to send delta
    if(deltaSize>20) {
      sent_OK = m_skynet.send_msg(socket_num, deltaPtr, deltaSize, 0);
      //fusionMap.resetDelta(layerID_grown);
      fusionMap.resetDelta(layerID_cost);
      _infoFusionMapper.endProcessTimer(sent_OK);
      unsigned long long now;
      DGCgettime(now);
      delta_log << now << " " << sent_OK << endl;
    }

    usleep(10000);
  }
}


void FusionMapper::ReceiveDataThread_LadarRoof() {
  int mapDeltaSocket = m_skynet.listen(SNladardeltamap_roof, MODladarfeeder_roof);
  double UTMNorthing;
  double UTMEasting;

  double cellval;
  int count=0;

  long long num_cell_updates;
  double old_val;
  //printf("%s [%d]: \n", __FILE__, __LINE__);

  m_pLadarRoofMapDelta = new char[MAX_DELTA_SIZE];
  while(!_QUIT) { 
    //printf("%s [%d]: \n", __FILE__, __LINE__);
    if(!_PAUSE) {
      //printf("%s [%d]: \n", __FILE__, __LINE__);
      _infoLadarFeederRoof.startProcessTimer();
      if(mapperOpts.optLadar) {
	int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pLadarRoofMapDelta, MAX_DELTA_SIZE, 0);
	//printf("%s [%d]: \n", __FILE__, __LINE__);
	if(numreceived>0) {
	  //printf("%s [%d]: \n", __FILE__, __LINE__);
	  DGClockMutex(&m_mapMutex);
	  int cellsize = fusionMap.getDeltaSize(m_pLadarRoofMapDelta);
	  //printf("%s [%d]: \n", __FILE__, __LINE__);
	  for(int i=0; i<cellsize; i++) {
	    cellval = fusionMap.getDeltaVal<double>(i, layerID_ladarElev, m_pLadarRoofMapDelta, &UTMNorthing, &UTMEasting);
	    if(mapperOpts.optAverage) {
	      num_cell_updates = fusionMap.getDataUTM<long long>(layerID_number, UTMNorthing, UTMEasting);
	      fusionMap.setDataUTM<long long>(layerID_number, UTMNorthing, UTMEasting, num_cell_updates+1);
	      old_val = fusionMap.getDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting);
	      fusionMap.setDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting, 
					   ((old_val*num_cell_updates)+cellval)/(num_cell_updates+1));
	    } else if(mapperOpts.optMax) {
	      old_val = fusionMap.getDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting);
	      if(cellval < old_val || 
		 old_val==fusionMap.getLayerNoDataVal<double>(layerID_ladarElev)) {
		fusionMap.setDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting, cellval);	
	      }
	    } else {
	      fusionMap.setDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting, cellval);	
	    }
	    //printf("%s [%d]: calling paintchanges\n", __FILE__, __LINE__);	    
	    fusionCostPainter.paintChanges(UTMNorthing, UTMEasting, 1);
	    //fusionCostPainter.growChanges(UTMNorthing, UTMEasting);
	  }
	  DGCunlockMutex(&m_mapMutex);
	  _infoLadarFeederRoof.endProcessTimer(numreceived);
	}     
      }
    }
    usleep(100);
  }
  delete m_pLadarRoofMapDelta; 
}


void FusionMapper::ReceiveDataThread_LadarBumper() {
  int mapDeltaSocket = m_skynet.listen(SNladardeltamap_bumper, MODladarfeeder_bumper);
  double UTMNorthing;
  double UTMEasting;

  double cellval;
  int count=0;

  long long num_cell_updates;
  double old_val;

  m_pLadarBumperMapDelta = new char[MAX_DELTA_SIZE];
  while(!_QUIT) { 
    if(!_PAUSE) {
      _infoLadarFeederBumper.startProcessTimer();
      if(mapperOpts.optLadar) {
	int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pLadarBumperMapDelta, MAX_DELTA_SIZE, 0);
	if(numreceived>0) {
	  DGClockMutex(&m_mapMutex);
	  int cellsize = fusionMap.getDeltaSize(m_pLadarBumperMapDelta);
	  for(int i=0; i<cellsize; i++) {
	    cellval = fusionMap.getDeltaVal<double>(i, layerID_ladarElev, m_pLadarBumperMapDelta, &UTMNorthing, &UTMEasting);
	    
	    if(mapperOpts.optAverage) {
	      num_cell_updates = fusionMap.getDataUTM<long long>(layerID_number, UTMNorthing, UTMEasting);
	      fusionMap.setDataUTM<long long>(layerID_number, UTMNorthing, UTMEasting, num_cell_updates+1);
	      old_val = fusionMap.getDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting);
	      fusionMap.setDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting, 
					   ((old_val*num_cell_updates)+cellval)/(num_cell_updates+1));
	    } else if(mapperOpts.optMax) {
	      old_val = fusionMap.getDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting);
	      if(cellval < old_val || 
		 old_val==fusionMap.getLayerNoDataVal<double>(layerID_ladarElev)) {
		fusionMap.setDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting, cellval);	
	      }
	    } else {
	      fusionMap.setDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting, cellval);	
	    }
	    //printf("%s [%d]: calling paintchanges\n", __FILE__, __LINE__);
	    fusionCostPainter.paintChanges(UTMNorthing, UTMEasting, 0);
	  }
	  DGCunlockMutex(&m_mapMutex);
	  _infoLadarFeederBumper.endProcessTimer(numreceived);
	}     
      }
    }
    usleep(100);
  }
  delete m_pLadarBumperMapDelta; 
}


void FusionMapper::ReceiveDataThread_Stereo() {
  int mapDeltaSocket = m_skynet.listen(SNstereodeltamap, MODstereofeeder);
  double UTMNorthing;
  double UTMEasting;

  StereoAvgCell cellval;
  int count=0;

  long long num_cell_updates;
  StereoAvgCell old_val;
  StereoAvgCell new_val;

  m_pStereoMapDelta = new char[MAX_DELTA_SIZE];
  while(!_QUIT) { 
    if(!_PAUSE) {
      _infoStereoFeeder.startProcessTimer();
      if(mapperOpts.optStereo) {
	int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pStereoMapDelta, MAX_DELTA_SIZE, 0);
	if(numreceived>0) {
	  DGClockMutex(&m_mapMutex);
	  int cellsize = fusionMap.getDeltaSize(m_pStereoMapDelta);
	  for(int i=0; i<cellsize; i++) {
	    cellval = fusionMap.getDeltaVal<StereoAvgCell>(i, layerID_stereoAvg, m_pStereoMapDelta, &UTMNorthing, &UTMEasting);
	    old_val = fusionMap.getDataUTM<StereoAvgCell>(layerID_stereoAvg, UTMNorthing, UTMEasting);
	    new_val.cellVal = (((double)cellval.numPoints)*cellval.cellVal + ((double)old_val.numPoints)*old_val.cellVal)/((double)(cellval.numPoints+old_val.numPoints));
	    new_val.numPoints = cellval.numPoints + old_val.numPoints;
	    fusionMap.setDataUTM<StereoAvgCell>(layerID_stereoAvg, UTMNorthing, UTMEasting, new_val);
	    fusionMap.setDataUTM<double>(layerID_stereoElev, UTMNorthing, UTMEasting, new_val.cellVal);
	    /*	    
	    if(mapperOpts.optAverage) {
	      num_cell_updates = fusionMap.getDataUTM<long long>(layerID_number, UTMNorthing, UTMEasting);
	      fusionMap.setDataUTM<long long>(layerID_number, UTMNorthing, UTMEasting, num_cell_updates+1);
	      old_val = fusionMap.getDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting);
	      fusionMap.setDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting, 
					   ((old_val*num_cell_updates)+cellval)/(num_cell_updates+1));
	    } else if(mapperOpts.optMax) {
	      old_val = fusionMap.getDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting);
	      if(cellval < old_val || 
		 old_val==fusionMap.getLayerNoDataVal<double>(layerID_ladarElev)) {
		fusionMap.setDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting, cellval);	
	      }
	    } else {
	      fusionMap.setDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting, cellval);	
	    }
	    */
	    fusionCostPainter.paintChanges(UTMNorthing, UTMEasting, 0);
	  }
	  DGCunlockMutex(&m_mapMutex);
	  _infoStereoFeeder.endProcessTimer(numreceived);
	}     
      }

    }
    usleep(100);
  }
  delete m_pStereoMapDelta; 
}





void FusionMapper::ReceiveRoadDataThread() {
  /***** UNCOMMENT THIS WHEN READY -- CODE SHOULD WORK
  int roadInfoSocket = m_skynet.listen(SNroadboundary, SNroadfinding);

  if(roadInfoSocket < 0)
    cerr << "error - FIXME" << endl;

  while(!_QUIT) {
    printf("Attempting to get road info\n");
    int numreceived = m_skynet.get_msg(roadInfoSocket, m_pRoadReceive, MAX_ROAD_SIZE * 3 + 1, 0);		  
    if(numreceived>0)
      {
	int numRoadPoints = m_pRoadReceive[0];
	if(numreceived != sizeof(double)*(numRoadPoints*3+1) || numRoadPoints > MAX_ROAD_SIZE) {
	  cerr << "error -FIXME" << endl;
	} else {
	  
	  m_pRoadPoints = numRoadPoints;
	  for(int i = 0; i < numRoadPoints; i++)
	  {
	    m_pRoadX[i] = m_pRoadReceive[1 + i];
	    m_pRoadY[i] = m_pRoadReceive[1 + i + numRoadPoints];
	    m_pRoadW[i] = m_pRoadReceive[1 + i + 2 * numRoadPoints];


	    //do stuff with the road array here
	    //like put it in the map -jhg
	    // ryanf:  maybe you want to do that after the for loop
	  }


	}
      }

  }		
  // update map at 10hz
  usleep(10000);

  ********/

}


void FusionMapper::PlannerListenerThread() { 
	int mapDeltaSocket = m_skynet.listen(SNfullmaprequest, SNplanner);
          double UTMNorthing;
          double UTMEasting;
          int socket_num = m_skynet.get_send_sock(SNfusiondeltamap);
          int deltaSize = 0;
          int sent_OK;
          int i,j;
	void * deltaPtr = NULL;
	 bool *  m_pRequestMap = new bool;
           while(true) {
            int numreceived =  m_skynet.get_msg(mapDeltaSocket, m_pRequestMap, MAX_DELTA_SIZE, 0);
            cout << "Received "  << numreceived << " Messages"  << endl; 
	    if(numreceived>0) {
                  DGClockMutex(&m_mapMutex);
             if(*m_pRequestMap == true){
	        for(i=0; i < fusionMap.getNumRows() ; i++){
       		   for(j=0; j < fusionMap.getNumCols(); j++){
   			  fusionMap.setDataWin_Delta<double>(layerID_cost, i,j,fusionMap.getDataWin<double>(layerID_cost,i, j));
			  }				
		}
		deltaPtr = fusionMap.serializeDelta<double>(layerID_cost, deltaSize); 
		sent_OK = m_skynet.send_msg(socket_num, deltaPtr, deltaSize, 0);
     		  fusionMap.resetDelta(layerID_cost);
     		  printf("Sent was %d\n", sent_OK);
     	  } 
       DGCunlockMutex(&m_mapMutex);
       }
      usleep(20000);
    }
  delete m_pRequestMap;
}



