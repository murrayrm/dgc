#include "fusionMapper.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

#include <iostream>


using namespace std;

int QUIT;
int PAUSE;
int RESET;
int CHANGE;

int sparrowSNKey;

double sparrowAggrTuningObstacleGrowing;
double sparrowAggrTuningRDDFScaling;
double sparrowAggrTuningMaxSpeed;

int sparrowStatusLadarFeederRoof;
int sparrowNumMsgsLadarFeederRoof;
int sparrowMsgSizeLadarFeederRoof;
double sparrowMsgSizeAvgLadarFeederRoof;
double sparrowTimeLadarFeederRoof;
double sparrowTimeAvgLadarFeederRoof;

int sparrowStatusLadarFeederBumper;
int sparrowNumMsgsLadarFeederBumper;
int sparrowMsgSizeLadarFeederBumper;
double sparrowMsgSizeAvgLadarFeederBumper;
double sparrowTimeLadarFeederBumper;
double sparrowTimeAvgLadarFeederBumper;

int sparrowStatusStereoFeeder;
int sparrowNumMsgsStereoFeeder;
int sparrowMsgSizeStereoFeeder;
double sparrowMsgSizeAvgStereoFeeder;
double sparrowTimeStereoFeeder;
double sparrowTimeAvgStereoFeeder;

int sparrowStatusGPSFinder;
int sparrowNumMsgsGPSFinder;
int sparrowMsgSizeGPSFinder;
double sparrowMsgSizeAvgGPSFinder;
double sparrowTimeGPSFinder;
double sparrowTimeAvgGPSFinder;

int sparrowStatusRoadFinder;
int sparrowNumMsgsRoadFinder;
int sparrowMsgSizeRoadFinder;
double sparrowMsgSizeAvgRoadFinder;
double sparrowTimeRoadFinder;
double sparrowTimeAvgRoadFinder;

int sparrowStatusFusionMapper;
int sparrowNumMsgsFusionMapper;
int sparrowMsgSizeFusionMapper;
double sparrowMsgSizeAvgFusionMapper;
double sparrowTimeFusionMapper;
double sparrowTimeAvgFusionMapper;

#include "vddtable.h"

void FusionMapper::UpdateSparrowVariablesLoop() {
  sparrowSNKey = mapperOpts.optSNKey;

  sparrowStatusLadarFeederRoof = mapperOpts.optLadar;
  sparrowStatusLadarFeederBumper = mapperOpts.optLadar;
  sparrowStatusStereoFeeder = mapperOpts.optStereo;
  sparrowStatusFusionMapper = 1;

  sparrowAggrTuningObstacleGrowing = mapperOpts.optObsGrowingSize;
  sparrowAggrTuningRDDFScaling = mapperOpts.optRDDFScaling;
  sparrowAggrTuningMaxSpeed = mapperOpts.optMaxSpeed;
  
  while(!_QUIT) {
    //Update variables that changed in the display
    if(CHANGE) {
      CHANGE = 0;

      mapperOpts.optObsGrowingSize = sparrowAggrTuningObstacleGrowing;
      mapperOpts.optRDDFScaling = sparrowAggrTuningRDDFScaling;
      mapperOpts.optMaxSpeed = sparrowAggrTuningMaxSpeed;
    }
    //Update read-only variables
    sparrowNumMsgsLadarFeederRoof = _infoLadarFeederRoof.numMsgs;
    sparrowMsgSizeLadarFeederRoof = _infoLadarFeederRoof.msgSize;
    sparrowMsgSizeAvgLadarFeederRoof = _infoLadarFeederRoof.msgSizeAvg;
    sparrowTimeLadarFeederRoof = _infoLadarFeederRoof.processTime;
    sparrowTimeAvgLadarFeederRoof = _infoLadarFeederRoof.processTimeAvg;

    sparrowNumMsgsLadarFeederBumper = _infoLadarFeederBumper.numMsgs;
    sparrowMsgSizeLadarFeederBumper = _infoLadarFeederBumper.msgSize;
    sparrowMsgSizeAvgLadarFeederBumper = _infoLadarFeederBumper.msgSizeAvg;
    sparrowTimeLadarFeederBumper = _infoLadarFeederBumper.processTime;
    sparrowTimeAvgLadarFeederBumper = _infoLadarFeederBumper.processTimeAvg;

    sparrowNumMsgsFusionMapper = _infoFusionMapper.numMsgs;
    sparrowMsgSizeFusionMapper = _infoFusionMapper.msgSize;
    sparrowMsgSizeAvgFusionMapper = _infoFusionMapper.msgSizeAvg;
    sparrowTimeFusionMapper = _infoFusionMapper.processTime;
    sparrowTimeAvgFusionMapper = _infoFusionMapper.processTimeAvg;
    
    _PAUSE = PAUSE;
    _QUIT = QUIT;
    if(RESET) {
      _infoLadarFeederRoof.reset();
      _infoLadarFeederBumper.reset();
      _infoFusionMapper.reset();
      RESET = 0;
    }

    usleep(10000);
  }

}

void FusionMapper::SparrowDisplayLoop() 
{

  dbg_all = 0;

  if (dd_open() < 0) exit(1);

  dd_bindkey('Q', user_quit);
  dd_bindkey('q', user_quit);
  dd_bindkey('P', user_pause);
  dd_bindkey('p', user_pause);
  dd_bindkey('R', user_reset);
  dd_bindkey('r', user_reset);

  dd_usetbl(vddtable);

  usleep(500000); // Wait a bit, because other threads will print some stuff out
  dd_loop();	
  dd_close();
  _QUIT = 1;
  sleep(1);
}

int user_quit(long arg)
{
  QUIT = 1;
  return DD_EXIT_LOOP;
}


int user_reset(long arg)
{
  RESET = 1;
  return 0;
}

int user_pause(long arg)
{
  if(PAUSE) {
    PAUSE=0;
  } else {
    PAUSE=1;
  }
  return 0;
}

int user_change(long arg) {
  CHANGE=1;
  return 0;
}

