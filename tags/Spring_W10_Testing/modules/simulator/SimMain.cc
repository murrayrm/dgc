#include "Sim.hh"
#include "askynet_monitor.hh"

#include "DGCutils"

using namespace std;

int main(int argc, char **argv)
{
	// defaults
	bool bSimpleModel = false;
	double steerLag = 0.0;
	double steerRateLimit = 0.699;

  // process command line options
  for (int i = 1; i < argc; ++i)
	{
		string arg(argv[i]);

    if (arg == "-h" || arg == "--help")
    {
      /* User has requested usage information. Print it to standard
         output, and exit with exit code zero. */
      cout << "asim command line options:\n"
           << "--------------------------\n"
           << "-h, --help:       Prints this usage\n"
           << "--simple:         Uses the simple (kinematic integrator) vehicle mode.\n"
           << "                  No sliding, steering lags, etc.\n"
           << "--lag val:        Sets the steering first order lag timeconstant to val (s)\n"
           << "                  Default: 0.0 sec\n"
           << "--ratelimit val:  Sets the steering rate limit to val (rad/sec)\n"
           << "                  Default: 0.699 rad/sec\n"
           << "\n"
           << "Status:\n"
           << "---------------------\n"
           << "Current RDDF:         ";
      cout.flush();
			system("readlink -f rddf.dat");
			cout << "Current SKYNET_KEY:   ";
			cout.flush();
			system("echo $SKYNET_KEY");
			cout << endl;
			return 0;
		}
		else if(arg == "--simple")
		{
			bSimpleModel = true;
		}
		else if(arg == "--lag")
		{
			if(i == argc-1)
			{
				cout << "Ignoring argument " << arg << " because it needs a value" << endl;
				break;
			}

			steerLag = atoi(argv[++i]);
		}
		else if(arg == "--ratelimit")
		{
			if(i == argc-1)
			{
				cout << "Ignoring argument " << arg << " because it needs a value" << endl;
				break;
			}

			steerRateLimit = atoi(argv[++i]);
		}
		else
		{
			cout << "Ignoring unknown argument " << arg << endl;
		}
	}

  int sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if( pSkynetkey == NULL )
	{
		cerr << "SKYNET_KEY environment variable isn't set" << endl;
	}
  else
    sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;

  asim sim(sn_key, bSimpleModel, steerLag, steerRateLimit);

  DGCstartMemberFunctionThread(&sim, &asim::SparrowDisplayLoop);
  DGCstartMemberFunctionThread(&sim, &asim::UpdateSparrowVariablesLoop);

	// adrive stuff
	int shutdown_flag = false;
	int run_skynet_thread = true;
	struct skynet_main_args args;
	pthread_t adriveThread;
	args.pVehicle = sim.getADriveVehicle();
	args.pShutdown_flag = &shutdown_flag;
	args.pRun_skynet_thread = &run_skynet_thread;
	init_vehicle_struct(*args.pVehicle);
	pthread_create( &adriveThread, NULL, &skynet_main, &args );

  sim.Active();

  return 0;
}
