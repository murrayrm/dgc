#include "gpsAutoCap.hh"
#include "rddf.hh"
#include "ggis.h"
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <fstream>
#include <pthread.h>

using namespace std;

/** gpsAutoCap
 * Eric Cady, 6/1/05
 * A logging utility for static data cribbed shamelessly from gpscap. Once
 * started, takes state data at a given frequency (12.5 Hz) and stores it
 * as an RDDF.  Allows paths to be created automatically while driving, for
 * GPS finding purposes.  (These paths form a layer in the cost map.)
 * 
 * Needs to have bob2rddf.sh run on the output; later version will hopefully 
 * do this automatically.
 */


gpsAutoCap::gpsAutoCap(int skynet_key) 	: CSkynetContainer(SNastate, skynet_key) 
{
  cout<<"before DGCgettime"<<endl;	
  DGCgettime(starttime);
  cout<<"after DGCgettime"<<endl;
  
  char gpsFileName[256];
  time_t t = time(NULL);
  tm *local;
  local = localtime(&t);
  
  sprintf(gpsFileName, "logs/gps_%04d%02d%02d_%02d%02d%02d.dat", 
	  local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	  local->tm_hour, local->tm_min, local->tm_sec);
  
  m_outputGPS.open(gpsFileName);
  
  m_outputGPS << setprecision(20);

}

void gpsAutoCap::Active() {

  char cmd[99];
  double speed;
  GisCoordLatLon latlon;
  GisCoordUTM utm;
  int index = 0;
  utm.zone = 11;
  utm.letter = 'S';

  cout<<endl<<"This function now assumes a default width of 4m."<<endl;
  cout<<"You may change it manually (in emacs, meta-shift-5 is find and replace)"<<endl<<endl;
  cout<<"Data is taken at 12.5 Hz; this may be modifed in source, but no commandline option exists for it yet." << endl << endl;
  
  cout << "Type c and hit enter to begin capture; hit ctrl-c to quit." << endl;
  scanf("%99s",cmd);
  cout << "Recording..." << endl; 
  
  while(1) 
    {
      UpdateState();
      
      utm.n = m_state.Northing;
      utm.e = m_state.Easting;

      if(!gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL)) {
	cerr << "Coordinate transform failed at (E, N) = (" << utm.e << ", " << utm.n << ")." 
	     << endl; 
	exit(1);
      }

      speed = sqrt(m_state.Vel_N*m_state.Vel_N + m_state.Vel_E*m_state.Vel_E + m_state.Vel_D*m_state.Vel_D);
      m_outputGPS << index << RDDF_DELIMITER << latlon.latitude << RDDF_DELIMITER 
		  << latlon.longitude << RDDF_DELIMITER << 4.0/METERS_PER_FOOT << RDDF_DELIMITER 
		  << speed/MPS_PER_MPH << RDDF_DELIMITER << "####" << RDDF_DELIMITER
		  << "####" << RDDF_DELIMITER << "####" << endl;
      
      index++;
      usleep(80000); //12.5 Hz => 2m spacing @ 25 m/s
    }
}


int main(int argc, char **argv) {
  
  int sn_key = 0;
  
  cout<<"Run this when driving desert roads..."<<endl;
  char* pSkynetkey = getenv("SKYNET_KEY");
  cout<<"got skynet key"<<endl;
  if ( pSkynetkey == NULL)
    {
      cerr << "SKYNET_KEY environment variable isn't set." << endl;
    } else
      {
	sn_key = atoi(pSkynetkey);
      }
  cerr << "Constructing skynet with KEY = " << sn_key << endl;
  gpsAutoCap ast(sn_key);
  cout<<"about to call active"<<endl;
  ast.Active();
  return 0;
}


