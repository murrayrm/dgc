#ifndef ASTATE_TEST_HH
#define ASTATE_TEST_HH

#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <fstream>

#include "VehicleState.hh"
#include "sn_msg.hh"
#include "DGCutils"
#include "StateClient.h"
#include "rddf.hh"
#include "GlobalConstants.h"
#include "ggis.h"


class gpsAutoCap : public CStateClient {

  ofstream m_outputGPS;

  unsigned long long starttime;

  double timecalc;

public:
  gpsAutoCap(int);
  void Active();


};

#endif
