/*! This is the simple way to test adrive for properly listening over skynet
 * for commands.  Run this program and follow the prompts.  It can simply 
 * send steer, brake, and gas commands over skynet.  */

#include "sn_msg.hh"
#include "string.h"
#include <unistd.h>
#define  SLEEP_TIME  1000000 //in microsec.
#define MAX_LINE_LENGTH 99  //characters
#include "adrive_skynet_interface_types.h"

double get_command_from_prompt(void);
void send_skynet_command(drivecmd_t &my_command_to_send, 
			 sn_msg my_output_to_send, int sn_key);

int main(int argc, char** argv){
  //  modulename myself = SNadrive_commander;
  sn_msg myoutput = SNdrivecmd;
  int sn_key;
  char* default_key = getenv("SKYNET_KEY");
  if (2 == argc)
  {
    sn_key  = atoi(argv[1]);
  }
  else if (default_key != NULL )
  {
    sn_key = atoi(default_key);
  }
  else
  {
    printf("usage: sn_send_test key\nDefault key is $SKYNET_KEY\n");
    exit(2);
  }

  //skynet Skynet(myself, sn_key);
  //  printf("my module id: %d\n", Skynet.get_id());
  while(1)
    {
      //      int socket;
      char inputstring[MAX_LINE_LENGTH];
      drivecmd_t my_command;
      my_command.my_command_type = set_position;
      printf("Enter a for accel, s for steering, b for brake, g for gas:");
      scanf("%99s", inputstring);
      switch (inputstring[0])
	{
	case 's':
	  my_command.my_actuator = steer;
	  my_command.number_arg = get_command_from_prompt();

	  break;
	case 'b':
	  my_command.my_actuator = brake;
	  my_command.number_arg = get_command_from_prompt();
	  break;
	case 'g':
	  my_command.my_actuator = gas;
	  my_command.number_arg = get_command_from_prompt();
	  break;
	case 'a':
	  my_command.my_actuator = accel;
	  my_command.number_arg = get_command_from_prompt();
	  break;
	  
	default:
	  my_command.my_actuator = estop;
	  my_command.number_arg = get_command_from_prompt();
	  break;
	}
      
      send_skynet_command(my_command, myoutput, sn_key);

      //       socket = Skynet.get_send_sock(myoutput);
      //Skynet.send_msg(socket, (void*)&my_command, sizeof(my_command), 0);
      //printf("sent message %d %g\n", my_command.my_actuator, my_command.number_arg);
      usleep(SLEEP_TIME);      
    }
  
  return 0;
}
double get_command_from_prompt(void)
{
  char inputstring[100];
     printf("\nEnter a commanded postion(gas brake: [0-1] steer accel: [-1,1]):");
     scanf("%99s", inputstring);
     return strtod(inputstring, NULL);
}      

void send_skynet_command(drivecmd_t &my_command_to_send, sn_msg myoutput_to_send, int sn_key)
{
  modulename myself = SNadrive_commander;
  skynet Skynet(myself, sn_key); 
  printf("my module id: %d\n", Skynet.get_id());
  int socket = Skynet.get_send_sock(myoutput_to_send);
  Skynet.send_msg(socket, (void*)&my_command_to_send, sizeof(my_command_to_send), 0);
  printf("sent message %d %g\n", my_command_to_send.my_actuator, my_command_to_send.number_arg);
}
