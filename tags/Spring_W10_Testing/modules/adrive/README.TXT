adrive.config readme
Tully Foote

These are the possible startup parameters.  
The numbers of the ports must be set for 
the current configuration. 

All the threads default to on adding any of the below
lines to the config file disables that thread.  


throttle_port 0
brake_port 1
estop_port 2
trans_port 3
steer_port 4

disable_steer_command
disable_brake_command
disable_trans_command
disable_estop_command
disable_gas_command

disable_steer_status
disable_brake_status
disable_trans_status
disable_estop_status
disable_gas_status

disable_sparrow
disable_skynet
disable_logging
 

#enable_simulation 131.215.42.45
