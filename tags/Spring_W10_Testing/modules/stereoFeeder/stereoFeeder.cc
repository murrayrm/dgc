#include "stereoFeeder.hh"
#include <DGCutils>

int QUIT=0;
int PAUSE=0;
int STEP=0;

double time_lock;
double time_cap;
double time_rect;
double time_disp;
double time_fill;
double time_eval;
double time_vote;
double time_total;
double time_debug;
double time_full;

extern int optSparrow, optVote, optPair, optExpCtrl;
extern double optDelay, optRate;


#define STEREO_DOUBLE_NO_DATA 0.0
#define STEREO_HEIGHT_THRESHOLD -4.0

#define DEFAULT_FUSIONMAP_NUM_ROWS 150
#define DEFAULT_FUSIONMAP_NUM_COLS 150
#define DEFAULT_FUSIONMAP_ROW_RES 1
#define DEFAULT_FUSIONMAP_COL_RES 1


//stereoMap Default Parameters
#define DEFAULT_SVMAP_NUM_ROWS 250
#define DEFAULT_SVMAP_NUM_COLS 250
#define DEFAULT_SVMAP_ROW_RES  0.2
#define DEFAULT_SVMAP_COL_RES  0.2

#define LONG_DEFAULT_SVMAP_NUM_ROWS 250
#define LONG_DEFAULT_SVMAP_NUM_COLS 250
#define LONG_DEFAULT_SVMAP_ROW_RES  0.4
#define LONG_DEFAULT_SVMAP_COL_RES  0.4

#define X_LIMIT DEFAULT_SVMAP_NUM_ROWS*DEFAULT_SVMAP_ROW_RES*0.5
#define Y_LIMIT DEFAULT_SVMAP_NUM_COLS*DEFAULT_SVMAP_COL_RES*0.5
#define Z_LIMIT 1000

#define LONG_X_LIMIT LONG_DEFAULT_SVMAP_NUM_ROWS*DEFAULT_SVMAP_ROW_RES*0.5
#define LONG_Y_LIMIT LONG_DEFAULT_SVMAP_NUM_COLS*DEFAULT_SVMAP_COL_RES*0.5
#define LONG_Z_LIMIT 1000

#define CALIB_PATH_PREFIX "../../drivers/stereovision/calibration/"

#define CALIB_SVSCAL_NAME    "SVSCal.ini"
#define CALIB_SVSPARAMS_NAME "SVSParams.ini"
#define CALIB_CAMCAL_NAME    "CamCal.ini"
#define CALIB_CAMID_NAME     "CamID.ini"
#define CALIB_GENMAP_NAME    "GenMapParams.ini"

#define CALIB_SHORT_RANGE_SUFFIX ".short"
#define CALIB_LONG_RANGE_SUFFIX  ".long"
#define CALIB_SHORT_COLOR_SUFFIX ".short_color"
#define CALIB_HOMER_SUFFIX       ".homer"

#define OVERPASS_HEIGHT -3.3

#define LR_MIN_DISTANCE 20.0

#define which_pair SHORT_RANGE

using namespace std;

extern int QUIT;
extern int PAUSE;

extern double time_lock;
extern double time_cap;
extern double time_rect;
extern double time_disp;
extern double time_fill;
extern double time_eval;
extern double time_vote;
extern double time_total;
extern double time_debug;
extern double time_full;

extern char debugFilenamePrefix[200];
extern char logFilenamePrefix[200];
extern char sourceFilenamePrefix[200];
extern char optFormat[10];
extern int logFrames, logRect, logDisp, logState, logMaps, logTime, logVotes;
extern int optDisplay, optPair, optUseCameras, optSim, optFile, optSparrow, optMaxFrames, optMaxLoops;

double time_ms = 0;
double rate = 0;

int pairIndex;

int lastPair;

StereoFeeder::StereoFeeder(int skynetKey) 
  : CSkynetContainer(MODstereofeeder, skynetKey) {
  //Initialize our calibration file stuff to defined defaults
  sprintf(calibFileObject[SHORT_RANGE].SVSCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSCAL_NAME, CALIB_SHORT_RANGE_SUFFIX);
  sprintf(calibFileObject[SHORT_RANGE].SVSParamFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSPARAMS_NAME, CALIB_SHORT_RANGE_SUFFIX);
  sprintf(calibFileObject[SHORT_RANGE].CamCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMCAL_NAME, CALIB_SHORT_RANGE_SUFFIX);
  sprintf(calibFileObject[SHORT_RANGE].CamIDFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMID_NAME, CALIB_SHORT_RANGE_SUFFIX);
  sprintf(calibFileObject[SHORT_RANGE].GenMapFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_GENMAP_NAME, CALIB_SHORT_RANGE_SUFFIX);

  sprintf(calibFileObject[LONG_RANGE].SVSCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSCAL_NAME, CALIB_LONG_RANGE_SUFFIX);
  sprintf(calibFileObject[LONG_RANGE].SVSParamFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSPARAMS_NAME, CALIB_LONG_RANGE_SUFFIX);
  sprintf(calibFileObject[LONG_RANGE].CamCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMCAL_NAME, CALIB_LONG_RANGE_SUFFIX);
  sprintf(calibFileObject[LONG_RANGE].CamIDFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMID_NAME, CALIB_LONG_RANGE_SUFFIX);
  sprintf(calibFileObject[LONG_RANGE].GenMapFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_GENMAP_NAME, CALIB_LONG_RANGE_SUFFIX);

  sprintf(calibFileObject[SHORT_COLOR].SVSCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSCAL_NAME, CALIB_SHORT_COLOR_SUFFIX);
  sprintf(calibFileObject[SHORT_COLOR].SVSParamFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSPARAMS_NAME, CALIB_SHORT_COLOR_SUFFIX);
  sprintf(calibFileObject[SHORT_COLOR].CamCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMCAL_NAME, CALIB_SHORT_COLOR_SUFFIX);
  sprintf(calibFileObject[SHORT_COLOR].CamIDFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMID_NAME, CALIB_SHORT_COLOR_SUFFIX);
  sprintf(calibFileObject[SHORT_COLOR].GenMapFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_GENMAP_NAME, CALIB_SHORT_COLOR_SUFFIX);

  sprintf(calibFileObject[HOMER].SVSCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSCAL_NAME, CALIB_HOMER_SUFFIX);
  sprintf(calibFileObject[HOMER].SVSParamFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSPARAMS_NAME, CALIB_HOMER_SUFFIX);
  sprintf(calibFileObject[HOMER].CamCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMCAL_NAME, CALIB_HOMER_SUFFIX);
  sprintf(calibFileObject[HOMER].CamIDFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMID_NAME, CALIB_HOMER_SUFFIX);
  sprintf(calibFileObject[HOMER].GenMapFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_GENMAP_NAME, CALIB_HOMER_SUFFIX);

  if(optUseCameras) {
    if(sourceObject.init(0, calibFileObject[optPair].CamIDFilename, logFilenamePrefix, optFormat)!=stereoSource_OK) {
      fprintf(stderr, "Error while initializing cameras!  Quitting...\n");
      exit(1);
    }
  }

  processObject.init(0,
		     calibFileObject[optPair].SVSCalFilename,
		     calibFileObject[optPair].SVSParamFilename,
		     calibFileObject[optPair].CamCalFilename,
		     logFilenamePrefix,
		     optFormat);

  if(optSim || optFile) {
    //sourceObject.stop();
    sourceObject.init(0, processObject.imageSize.width, processObject.imageSize.height, sourceFilenamePrefix, optFormat, 1);
  }
  stereoMap.initMap(0, 0, 
		  DEFAULT_FUSIONMAP_NUM_ROWS, DEFAULT_FUSIONMAP_NUM_COLS,
		  DEFAULT_FUSIONMAP_ROW_RES, DEFAULT_FUSIONMAP_COL_RES,
		  0);

  noDataVal_stereoAvg.numPoints = 0;
  outsideMapVal_stereoAvg.numPoints = -1;

  layerID_stereoAvg = stereoMap.addLayer<StereoAvgCell>(noDataVal_stereoAvg, outsideMapVal_stereoAvg);
  //layerID_stereoElev = stereoMap.addLayer<double>(STEREO_DOUBLE_NO_DATA, CM_DOUBLE_OUTSIDE_MAP);
  //layerID_stereoCount = stereoMap.addLayer<int>(0, -1);
  //countLayerNum = stereoMap.addLayer<int>(0, -1);

  /*
  //Initialize the map
  if(optPair == SHORT_RANGE || optPair==SHORT_COLOR || optPair == HOMER) {
    stereoMap.initMap(DEFAULT_SVMAP_NUM_ROWS, DEFAULT_SVMAP_NUM_COLS, DEFAULT_SVMAP_ROW_RES, DEFAULT_SVMAP_COL_RES, FALSE); 
  } else if(optPair == LONG_RANGE) {
    stereoMap.initMap(LONG_DEFAULT_SVMAP_NUM_ROWS, LONG_DEFAULT_SVMAP_NUM_COLS, LONG_DEFAULT_SVMAP_ROW_RES, LONG_DEFAULT_SVMAP_COL_RES, FALSE); 
  }
  stereoMap.initPathEvaluationCriteria(false,false,false, false,false, true, true);
  //stereoMap.initPathEvaluationCriteria(false,false,false, true,false, true, false, false);
  stereoMap.initThresholds(70, 1, TIRE_OFF_THRESHOLD, VECTOR_PROD_THRESHOLD, GROW_VEHICLE_LENGTH, 1.2);

  svQuickMap.initMap(tempState.Northing, tempState.Easting, DEFAULT_SVMAP_NUM_ROWS, DEFAULT_SVMAP_NUM_COLS, DEFAULT_SVMAP_ROW_RES, DEFAULT_SVMAP_COL_RES, 0);
  */

  if(optDisplay) {
    processObject.show();
  }

  if(optExpCtrl) {
    sourceObject.startExposureControl(processObject.subWindow);
  }
}


StereoFeeder::~StereoFeeder() {
  //FIX ME
  printf("Thank you for using the stereoFeeder destructor\n");
}


void StereoFeeder::ActiveLoop() {
  int socket_num = m_skynet.get_send_sock(SNstereodeltamap);

  void* deltaPtr = NULL;
  int deltaSize = 0;


  while(!QUIT) {
    if(optSparrow) {
      UpdateSparrowVariablesLoop();
    }
    usleep(10000);


  if(!PAUSE) {
    if(optMaxLoops == 0) {
      if(optSparrow) {
	user_quit(0);
	sleep(1);
      }
      QUIT = 1;
      return;
    }

    if(optMaxLoops>0) optMaxLoops--;

    unsigned long long startFull, endFull, resultFull;
    DGCgettime(startFull);

    double steerAngle;
    double xUpLimit, yUpLimit, zUpLimit;
    double xLowLimit, yLowLimit, zLowLimit;
    XYZcoord UTMPoint, Point;
    unsigned long long start, end, result;
    unsigned long long startTotal, endTotal, resultTotal;
    
    //start = TVNow();
    DGCgettime(startTotal);
    {
      Lock mylock(CameraLock);

      if(optFile) {
	if(sourceObject.grab() == stereoSource_OK) {
	  FILE *stateLogFile = NULL;
	  char stateLogFilename[256];
	  sprintf(stateLogFilename, "%s.state", sourceFilenamePrefix);
	  stateLogFile = fopen(stateLogFilename, "r");
	  if(stateLogFile != NULL) {
	    int lineNum=-1;
	    double doubleTime;
	    long int sec, usec;
	    unsigned long long grabTimestamp;
	    while(lineNum != sourceObject.pairIndex()-1) {
	      double temp;
	      fscanf(stateLogFile, "%d: %lld %lld %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
		     &lineNum,
		     &grabTimestamp,
		     &m_state.Timestamp, 
		     &m_state.Northing, &m_state.Easting, &m_state.Altitude,
		     &m_state.Vel_N, &m_state.Vel_E, &m_state.Vel_D,
		     &m_state.Acc_N, &m_state.Acc_E, &m_state.Acc_D,
		     &m_state.Roll, &m_state.Pitch, &m_state.Yaw,
		     &m_state.RollRate, &m_state.PitchRate, &m_state.YawRate,
		     &m_state.RollAcc, &m_state.PitchAcc, &m_state.YawAcc);
	    }
	    /*
	    sec = (int)doubleTime;
	    usec = (int)((doubleTime - floor(doubleTime)) * 1000000);
	    timeval timevalTime(sec, usec);
	    //tempState.Timestamp = timevalTime;
	    tempState.Timestamp = (unsigned long long)doubleTime*1000000;
	    */
	    fclose(stateLogFile);
	  }
	} else {
	  if(optSparrow) {
	    user_quit(0);
	    sleep(1);
	  }
	  QUIT = 1;
	  return;
	}
      }
      
      /*
      end = TVNow();
      result = end-start;
      time_lock = result.dbl()*1000;
      start = TVNow();
      */
      if(stereoProcess_OK != processObject.loadPair(sourceObject.pair(), m_state)) {
	printf("Error while grabbing images from cameras at line %d in %s!  Quitting...\n", __LINE__, __FILE__);
	return;
      }
      pairIndex = sourceObject.pairIndex();
    }
    
    tempState = processObject.currentState();
    /*
    end = TVNow();
    result=end-start;
    time_cap = result.dbl()*1000;
    //printf("C %.3lf, ", result.dbl()*1000);
    start = TVNow();
    */

    processObject.calcRect();
    /*
    end = TVNow();
    result=end-start;
    time_rect = result.dbl()*1000;
    //printf("R %.3lf, ", result.dbl()*1000);
    start = TVNow();
    */
    
    processObject.calcDisp();
    /*
    end = TVNow();
    result=end-start;
    time_disp = result.dbl()*1000;
    //printf("D %.3lf, ", result.dbl()*1000);
    start = TVNow();
    */
    
    XYZcoord RelativePoint;
    double *oldVal;
    int numPoints;
    //processObject.calc3D();
    //numPoints = processObject.numPoints();

    stereoMap.updateVehicleLoc(tempState.Northing, tempState.Easting);
    stereoMap.clearMap();

    //printf("points: %d\n", numPoints);
    /*
    for(int b=0; b<numPoints; b++) {
      UTMPoint = processObject.UTMPoint(b);
      
      oldVal = (double*)svQuickMap.getDataUTM(layerID_stereoElev, UTMPoint.X, UTMPoint.Y);
      if(oldVal != NULL) {
	if(*oldVal > UTMPoint.Z) {
    // *oldVal = UTMPoi
	  //oldVal = (double*)svQuickMap.getDataUTM(layerID_stereoElev, UTMPoint.X, UTMPoint.Y);
	  //svQuickMap.setDataUTM(layerID_stereoElev, UTMPoint.X, UTMPoint.Y, &UTMPoint.Z);
	}
      } 
      
    }
    */
    double min = 0;
    double max = 0;

    for(int y=processObject.subWindow.y; y<processObject.subWindow.height; y++) {
      for(int x=processObject.subWindow.x; x<processObject.subWindow.width; x++) {
	//if(0) {
	if(processObject.SinglePoint(x, y, &UTMPoint)) {
	  //if(UTMPoint.Z > STEREO_HEIGHT_THRESHOLD) {
	    //double& oldVal = stereoMap.getDataUTM<double>(layerID_stereoElev, UTMPoint.X, UTMPoint.Y);
	    //double oldVal = stereoMap.getDataUTM<double>(layerID_stereoElev, UTMPoint.X, UTMPoint.Y);
	    //if(UTMPoint.Z < oldVal) {
	      //oldVal = UTMPoint.Z;
	  //printf("The veh is at %lf %lf\n", m_state.Northing, m_state.Easting);
	  //printf("We think a point is at %lf, %lf\n", UTMPoint.X, UTMPoint.Y);
	  //UTMPoint.Z = -UTMPoint.Z;
	  int numInCell = (stereoMap.getDataUTM<StereoAvgCell>(layerID_stereoAvg, UTMPoint.X, UTMPoint.Y)).numPoints;
	  double numInCellDbl = (double)numInCell;
	  if(numInCell!=-1 && fabs(UTMPoint.Z - tempState.Altitude)<2) {
	    //if(numInCell!=-1 && fabs(UTMPoint.Z)<2) {
	    StereoAvgCell newVal;
	    double oldVal = (stereoMap.getDataUTM<StereoAvgCell>(layerID_stereoAvg, UTMPoint.X, UTMPoint.Y)).cellVal;
	    newVal.cellVal = (double)(((double)UTMPoint.Z)+(((double)numInCell)*oldVal))/((double)(numInCell+1));
	    newVal.numPoints = numInCell+1;
	    stereoMap.setDataUTM_Delta<StereoAvgCell>(layerID_stereoAvg, UTMPoint.X, UTMPoint.Y, newVal);
	    //stereoMap.setDataUTM<int>(layerID_stereoCount, UTMPoint.X, UTMPoint.Y, numInCell+1);
	    //if(numInCell==0) {
	      //printf("setting to %lf (%lf), from %lf, %d\n", UTMPoint.Z+numInCellDbl*oldVal, UTMPoint.Z, oldVal, numInCell);
	      //printf("or %lf\n", UTMPoint.Z);
	    //}
	    //	    if(newVal<0) {
	    //printf("less than 0 %lf\n", newVal);
	    //}
	  }
	      //int& oldCount = stereoMap.getDataUTM<int>(countLayerNum, UTMPoint.X, UTMPoint.Y);
	      //oldCount++;
	      //int oldCount = stereoMap.getDataUTM<int>(countLayerNum, UTMPoint.X, UTMPoint.Y);
	      //stereoMap.setDataUTM<int>(countLayerNum, UTMPoint.X, UTMPoint.Y, oldCount+1);
	      //}
	      //}
	}
      }
    }


    //SKYNETY
    deltaPtr = stereoMap.serializeDelta<double>(layerID_stereoAvg, deltaSize);
    //code to send delta
    if(deltaSize>20)
      {
	int sentnum = m_skynet.send_msg(socket_num, deltaPtr, deltaSize, 0);
	printf("Sent was %d\n", sentnum);
      }
    else
      {
	// 			printf("deltaSize <= 20. Didn't send\n");
      }

    stereoMap.resetDelta(layerID_stereoAvg);






    //stereoMap.refreshLayerDisplay<double>(layerID_stereoElev);
    /*
    end = TVNow();
    result=end-start;
    time_fill = result.dbl()*1000;
    //printf("F %.3lf, ", result.dbl()*1000);
    start = TVNow();
    */
    /*
    Mail m = NewOutMessage(MyAddress(), MODULES::FusionMapper,
			   FusionMapperMessages::StereoMeasurements);
    int numCells = 0;

    long int windowBottomLeftRowOffset =stereoMap.getWindowBottomLeftUTMNorthingRowResMultiple();
    long int windowBottomLeftColOffset = stereoMap.getWindowBottomLeftUTMEastingColResMultiple();

    for(int row=0; row < stereoMap.getNumRows(); row++) {
      for(int col=0; col < stereoMap.getNumCols(); col++) {
	if(stereoMap.getDataWin<double>(layerID_stereoElev, row, col) != STEREO_DOUBLE_NO_DATA) {
	  numCells++;
	}
      }
    }

    m << numCells;
    m << m_state.Northing;
    m << m_state.Easting;
    m << windowBottomLeftRowOffset;
    m << windowBottomLeftColOffset;

    for(int row=0; row < stereoMap.getNumRows(); row++) {
      for(int col=0; col < stereoMap.getNumCols(); col++) {
	int tempCount = stereoMap.getDataWin<int>(countLayerNum, row, col);
	if(tempCount != 0) {
	  double tempVal = stereoMap.getDataWin<double>(layerID_stereoElev, row, col);
	  m << row;
	  m << col;
	  m << tempCount;
	  m << tempVal;
	}
      }
    }


    SendMail(m);
    */
    /*
    end = TVNow();
    result=end-start;
    time_eval = result.dbl()*1000;
    //printf("E %.3lf, ", result.dbl()*1000);
    start = TVNow();
    */
    char mapname[256];

    int imageRow=0;
    int imageCol=0;
    int winRow, winCol;
    double minDouble, maxDouble, valueDouble;
    char valueChar;
    minDouble = 0;
    maxDouble = 0;
    double dptr;

    /*
    //int _numRows = svQuickMap.getNumRows();
    //int _numCols = svQuickMap.getNumCols();

    char *imageBuffer;

    imageBuffer = (char*)malloc(svQuickMap.getNumRows()*svQuickMap.getNumCols());

    for(winRow = 0; winRow < _numRows; winRow++) {
      for(winCol = 0; winCol < _numCols; winCol++) {
	dptr = svQuickMap.getDataWin<double>(layerID_stereoElev, winRow, winCol);
	if(dptr != CM_DOUBLE_OUTSIDE_MAP) {
	  valueDouble = dptr;
	  if(valueDouble < minDouble) minDouble = valueDouble;
	  if(valueDouble > maxDouble) maxDouble = valueDouble;
	}
      }
    }

    printf("MaxDouble: %lf, MinDouble: %lf\n", maxDouble, minDouble);
    double newValueDouble;
    for(winRow = 0; winRow < _numRows; winRow++) {
      imageCol = 0;
      for(winCol = 0; winCol < _numCols; winCol++) {
	valueDouble = 0;
	dptr = svQuickMap.getDataWin<double>(layerID_stereoElev, winRow, winCol);
	if(dptr != CM_DOUBLE_OUTSIDE_MAP) valueDouble = dptr;
	newValueDouble = (((valueDouble-minDouble)*255/(maxDouble-minDouble)));
	valueChar = (char)(int)newValueDouble;
	imageBuffer[imageRow*_numCols + imageCol] = valueChar;
	imageCol++;
      }
      imageRow++;
    }


    FILE* imagefile;
    imagefile=fopen(mapname, "w");
    if( imagefile == NULL) {
      fprintf( stderr, "Can't create '%s'", mapname);
    } else {
      fprintf(imagefile,"P5\n%u %u 255\n", _numRows, _numCols);
      fwrite((const char *)imageBuffer, 1, _numRows*_numCols, imagefile);
      fclose(imagefile);
    }

    */

    //stereoMap.saveIMGFile("temp.pgm");

    /*
    FILE* ptsFile;
    char ptsFilename[256];
    sprintf(ptsFilename, "%s%.4pts", logFilenamePrefix, pairIndex-1);
    ptsFile = fopen(ptsFilename, "w");

    for(int y=processObject.subWindow.Y; y<processObject.subWindow.height; y++) {
      if(processObject.SingleRelativePoint(320, y, &RelativePoint))
	fprintf(ptsFile, "%lf %lf %d\n", RelativePoint.X, RelativePoint.Z*-1, y);
    }

    fclose(ptsFile);
    */

    /*
    //Get the votes
    for(int i = 0; i < NUMARCS; i++) {
      steerAngle = GetPhi(i);
      steeringAngleList.push_back(steerAngle);
    }
    
    voteList = stereoMap.generateArbiterVotes(NUMARCS, steeringAngleList);
    
    for(int i=0; i < NUMARCS; i++) {
      stereo.Votes[i].Goodness = voteList[i].second;
      stereo.Votes[i].Velo = voteList[i].first;
    }
    */
    /*
    end = TVNow();
    result=end-start;
    time_vote = result.dbl()*1000;
    //printf("V %.3lf, ", result.dbl()*1000);
    start = TVNow();
    */

    DGCgettime(endTotal);
    resultTotal = endTotal-startTotal;  
    time_total = DGCtimetosec(resultTotal)*1000;

    if(logRect) processObject.saveRect(logFilenamePrefix, optFormat, pairIndex);
    if(logDisp) processObject.saveDisp(logFilenamePrefix, optFormat, pairIndex);
    if(logMaps) {
      char logMapsFilename[256];
      sprintf(logMapsFilename, "%s%.6d", logFilenamePrefix, pairIndex);
      //stereoMap.saveMATFile(logMapsFilename, "StereoVision genMap data", "SVmap");
    }
    
    /*
    if(logVotes) {
      FILE *voteLogFile = NULL;
      char voteLogFilename[256];
      sprintf(voteLogFilename, "%s.votes", logFilenamePrefix);
      
      voteLogFile = fopen(voteLogFilename, "a");
      if(voteLogFile) {
	fprintf(voteLogFile, "%.6d ", pairIndex);
	for(int i=0; i < NUMARCS; i++) {
	  fprintf(voteLogFile, "%lf ", stereo.Votes[i].Goodness);
	}
	fprintf(voteLogFile, "\n");
	fclose(voteLogFile);
      }
    }
    */

    
    time_ms = DGCtimetosec(resultTotal)*1000;
    rate = 1/DGCtimetosec(resultTotal);
    /* 
    end = TVNow();
    result = end-start;
    time_debug = result.dbl()*1000;
    */
    DGCgettime(endFull);
    resultFull = endFull-startFull;
    time_full = DGCtimetosec(resultFull)*1000;
    //printf("FT %.3lf\n", resultFull.dbl()*1000);

    if(logTime) {
      FILE *timeLogFile = NULL;
      char timeLogFilename[256];
      sprintf(timeLogFilename, "%s.time", logFilenamePrefix);
      
      timeLogFile = fopen(timeLogFilename, "a");
      if(timeLogFile) {
	fprintf(timeLogFile, "%.6d %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
		pairIndex,
		time_lock,
		time_cap,
		time_rect,
		time_disp,
		time_fill,
		time_eval,
		time_vote,
		time_total,
		time_debug,
		time_full);
	fclose(timeLogFile);
      }
    }
  }


  }
}


void StereoFeeder::ImageCaptureThread() {
  unsigned long long grabTimestamp;

  while(true && !QUIT) {
    if(!PAUSE && (optUseCameras || optSim)) {
      if(optMaxFrames == 0) {
	if(optSparrow) {
	  user_quit(0);
	  sleep(1);
	}
	QUIT = 1;
	return;
      }
      
      if(optMaxFrames>0) optMaxFrames--;
      
      {
	Lock mylock(CameraLock);
	if(optUseCameras) {
	  sourceObject.grab();
	  DGCgettime(grabTimestamp);
	  UpdateState();
	  //m_state.Altitude = 0;
	} else if(optSim) {
	  usleep(33000);
	  if(sourceObject.grab() == stereoSource_OK) {
	    FILE *stateLogFile = NULL;
	    char stateLogFilename[256];
	    sprintf(stateLogFilename, "%s.state", sourceFilenamePrefix);
	    stateLogFile = fopen(stateLogFilename, "r");
	    if(stateLogFile != NULL) {
	      int lineNum=-1;
	      double doubleTime;
	      long int sec, usec;
	      while(lineNum != sourceObject.pairIndex()-1) {
		double temp;
		fscanf(stateLogFile, "%d: %lld %lld %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
		       &lineNum,
		       &grabTimestamp,
		       &m_state.Timestamp, 
		       &m_state.Northing, &m_state.Easting, &m_state.Altitude,
		       &m_state.Vel_N, &m_state.Vel_E, &m_state.Vel_D,
		       &m_state.Acc_N, &m_state.Acc_E, &m_state.Acc_D,
		       &m_state.Roll, &m_state.Pitch, &m_state.Yaw,
		       &m_state.RollRate, &m_state.PitchRate, &m_state.YawRate,
		       &m_state.RollAcc, &m_state.PitchAcc, &m_state.YawAcc);
	      }
	      /*
	      sec = (int)doubleTime;
	      usec = (int)((doubleTime - floor(doubleTime)) * 1000000);
	      timeval timevalTime(sec, usec);
	      //m_state.Timestamp = timevalTime;
	      m_state.Timestamp = (unsigned long long)doubleTime*1000000;
	      */
	      fclose(stateLogFile);
	    }
	  } else {
	    if(optSparrow) {
	      user_quit(0);
	      sleep(1);
	    }
	    QUIT = 1;
	    return;
	  }
	}
      }

	if(logFrames) sourceObject.save(logFilenamePrefix, optFormat, sourceObject.pairIndex());
	if(logState) {
	  FILE *stateLogFile = NULL;
	  char stateLogFilename[256];
	  sprintf(stateLogFilename, "%s.state", logFilenamePrefix);

	  stateLogFile = fopen(stateLogFilename, "a");
	  if(stateLogFile) {
	    fprintf(stateLogFile, "%.6d %lld %lld %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
		    sourceObject.pairIndex(),
		    grabTimestamp,
		    m_state.Timestamp, 
		    m_state.Northing, m_state.Easting, m_state.Altitude,
		    m_state.Vel_N, m_state.Vel_E, m_state.Vel_D,
		    m_state.Acc_N, m_state.Acc_E, m_state.Acc_D,
		    m_state.Roll, m_state.Pitch, m_state.Yaw,
		    m_state.RollRate, m_state.PitchRate, m_state.YawRate,
		    m_state.RollAcc, m_state.PitchAcc, m_state.YawAcc);
	    fclose(stateLogFile);
	  }
	}
      usleep(100);
      if(optDelay!=-1) {
	//printf("Delaying for %lf\n", optDelay*1e6);
	usleep(optDelay*1e6);
      }
      if(optRate!=-1) {
	//printf("Rating for %lf\n", (1/optRate)*1e6);
	usleep((1/optRate)*1e6);
      }
    }
  }

}
