#include "ladarFeeder.hh"

#define CONFIG_PATH_PREFIX "./LadarCal.ini."
#define CONFIG_BUMPER_SUFFIX "bumper"
#define CONFIG_ROOF_SUFFIX "roof"

#define PFIXME fprintf(stderr, "%s [%d]: FIXME\n", __FILE__, __LINE__)

LadarFeeder::LadarFeeder(modulename ladarType, int skynetKey, LadarFeederOptions ladarOptsVal) 
  : CSkynetContainer(ladarType, skynetKey) {
  ladarOpts = ladarOptsVal;
  _QUIT = 0;
  _PAUSE = 0;
  _STEP = 0;
  _numDeltas = 0;

  if(ladarOptsVal.optPause) {
    _PAUSE = 1;
  }

  char configFilename[256];
  switch(ladarOpts.optLadar) {
  case LADAR_BUMPER:
    sprintf(configFilename, "%s%s", CONFIG_PATH_PREFIX, CONFIG_BUMPER_SUFFIX);
    break;
  case LADAR_ROOF:
    sprintf(configFilename, "%s%s", CONFIG_PATH_PREFIX, CONFIG_ROOF_SUFFIX);
    break;
  default:
    PFIXME;
      break;
  }


  int numRows = 150;
  int numCols = 150;
  double resRows = 1.0;
  double resCols = 1.0;

  ladarMap.initMap(0,0, 
		   numRows, numCols,
		   resRows, resCols,
		   0);
  
  noDataVal_ladarElev = -99;
  outsideMapVal_ladarElev = -100;  layerID_ladarElev = ladarMap.addLayer<double>(noDataVal_ladarElev,
						 outsideMapVal_ladarElev);
  
  ladarObject.init(ladarOpts.optSource, configFilename, ladarOpts.logFilenamePrefix, ladarOpts.optLog, ladarOpts.optLadar);  

  ladarPositionOffset = ladarObject.getLadarPosition();
  ladarAngleOffset = ladarObject.getLadarAngle();
}


LadarFeeder::~LadarFeeder() {
  //FIX ME
  PFIXME;
  printf("Thank you for using the ladarmapper destructor\n");
}


void LadarFeeder::ActiveLoop() {
  int socket_num = 0;
  int socket_num_ladar = 0;

  if(ladarOpts.optLadar==LadarFeeder::LADAR_ROOF) {
    socket_num = m_skynet.get_send_sock(SNladardeltamap_roof);
    socket_num_ladar = m_skynet.get_send_sock(SNladarmeas_roof);
  }
  if(ladarOpts.optLadar==LadarFeeder::LADAR_BUMPER) {
    socket_num = m_skynet.get_send_sock(SNladardeltamap_bumper);
  }
  void* deltaPtr = NULL;
  int deltaSize = 0;
  XYZcoord UTMPoint;
  int sentNum;

  ofstream interp_log("interp.log");

  
  interp_log << "# LF time [usec] | state time [usec] | ladar center scan [m] | state pitch [rad]" << endl;


  unsigned long long scan_timestamp;

  bool inObs = false;

  double scanval;

  ladarMeasurementStruct myMeasurement;

  while(!_QUIT) {
    if(!_PAUSE || _STEP) {
      _STEP = 0;
      if(ladarObject.scanIndex() >= ladarOpts.optMaxScans && ladarOpts.optMaxScans>=0) {
	return;
      }
      //ladarObject.updateState(m_state) must come after ladarObject.scan()
      scan_timestamp = ladarObject.scan();
      
      scanval = ladarObject.getCenterDist();
      
      //usleep(10000);
      UpdateState(scan_timestamp);
      interp_log << scan_timestamp << " " << m_state.Timestamp << " ";
      interp_log << scanval << " " << m_state.Pitch << endl;
      //UpdateState();
      m_state.Altitude = 0;
      ladarObject.updateState(m_state);
      //PFIXME;
      
      //m_state.Pitch = 0;
      if(ladarOpts.optState==0) {
	m_state = VehicleState();
      }
      ladarMap.updateVehicleLoc(m_state.Northing, m_state.Easting);
      //printf("New loop\n");
      inObs = false;

      double minAngleRes = ladarObject.getResolutionRads();

      myMeasurement.numPoints = ladarObject.numPoints();

      for(int i=1; i<ladarObject.numPoints(); i++) {
	myMeasurement.ranges[i] = ladarObject.range(i);
	myMeasurement.angles[i] = ladarObject.angle(i);
	/*
	if(ladarObject.range(i-1)-ladarObject.range(i) > ladarOpts.optThresh && inObs==false) {
	  printf("Starting an obstacle between %d and %d since %lf>%lf\n", 
		 i-1, i,
		 ladarObject.range(i-1)-ladarObject.range(i),
		 ladarOpts.optThresh);
	  inObs = true;
	}
	if(ladarObject.range(i)-ladarObject.range(i-1) > ladarOpts.optThresh && inObs==true) {
	  inObs=false;
	  printf("Ending an obstacle between %d and %d since %lf>%lf\n", 
		 i-1, i,
		 ladarObject.range(i)-ladarObject.range(i-1),
		 ladarOpts.optThresh);
	}
	*/

	/*
	//if(inObs) {
	if(ladarOpts.optLadar==LADAR_ROOF) {
	  if(fabs(ladarObject.range(i)-ladarObject.range(i-1)) > ladarOpts.optThresh && 
	     fabs(ladarObject.angle(i)-ladarObject.angle(i-1)) <= minAngleRes) {
	    UTMPoint = ladarObject.UTMPoint(i);
	    ladarMap.setDataUTM_Delta<double>(layerID_ladarElev, UTMPoint.X, UTMPoint.Y, 123.456);
	  }
	}
	if(ladarOpts.optLadar==LADAR_BUMPER) {
	  UTMPoint = ladarObject.UTMPoint(i);
	  ladarMap.setDataUTM_Delta<double>(layerID_ladarElev, UTMPoint.X, UTMPoint.Y, 123.456);
	}
	*/
	  UTMPoint = ladarObject.UTMPoint(i);
	  ladarMap.setDataUTM_Delta<double>(layerID_ladarElev, UTMPoint.X, UTMPoint.Y, UTMPoint.Z);	
      }

      if(ladarOpts.optLadar==LadarFeeder::LADAR_ROOF) {
	m_skynet.send_msg(socket_num_ladar, ((void*)&myMeasurement), 
			  sizeof(ladarMeasurementStruct),0);
      }
      
      deltaPtr = ladarMap.serializeDelta<double>(layerID_ladarElev, deltaSize);
      if(deltaSize>20 && ladarOpts.optCom==1) {
	sentNum = m_skynet.send_msg(socket_num, deltaPtr, deltaSize, 0);
	_numDeltas++;
      }
      ladarMap.resetDelta(layerID_ladarElev);    
      
      usleep(140000);
      if(ladarOpts.optDelay!=-1) {
	usleep(ladarOpts.optDelay*1e6);
      }
      if(ladarOpts.optRate!=-1) {
	usleep((1/ladarOpts.optRate)*1e6);
      } 
    }
  }
}


void LadarFeeder::ReceiveDataThread() {
  printf("Data receiving not yet");
}
