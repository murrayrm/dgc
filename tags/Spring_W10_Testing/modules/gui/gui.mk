GUI_PATH=$(DGC)/modules/gui

GUI_DEPEND_LIBS = \
	$(ADRIVELIB) \
	$(CMAPLIB) \
	$(CMAPPLIB) \
	$(GGISLIB) \
	$(MAPDISPLAYLIB) \
	$(RDDFLIB) \
	$(SKYNETLIB) \
	$(TRAJLIB) \
	$(MODULEHELPERSLIB)

GUI_DEPEND_SOURCES = \
	$(GUI_PATH)/MapDisplaySN/MapDisplaySN.cc \
	$(GUI_PATH)/MapDisplaySN/MapDisplaySN.hh \
	\
	$(GUI_PATH)/MainDisplay/gui.h \
	$(GUI_PATH)/MainDisplay/gui.cc \
	\
	$(GUI_PATH)/MainDisplay/GuiThread.cc \
	$(GUI_PATH)/MainDisplay/GuiThread.h \
	\
	$(GUI_PATH)/StartTab/StartTab.cc \
	$(GUI_PATH)/StartTab/StartTab.hh \
	\
	$(GUI_PATH)/StatusTab/StatusTab.cc \
	$(GUI_PATH)/StatusTab/StatusTab.hh \
	\
	$(GUI_PATH)/StatusTab/Status.cc \
	$(GUI_PATH)/StatusTab/Status.hh \
	\
	$(GUI_PATH)/MainDisplay/MainTab.h \
	$(GUI_PATH)/MainDisplay/MainTab.cc \
	\
	$(GUI_PATH)/main.cc 

GUI_DEPEND = $(GUI_DEPEND_SOURCES) $(GUI_DEPEND_LIBS)
