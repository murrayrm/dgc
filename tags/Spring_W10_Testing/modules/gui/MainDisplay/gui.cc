/**
 * gui.cc
 * Revision History:
 * 01/07/2005  hbarnor  Created
 */

#include <iostream>
#include "gui.h"

Gui::Gui()
{
  map_config = new MapConfig();
  m_mapWidget= new MapWidget(map_config);
  updateSNKey();
  startTab = new StartTab(sn_key);
  statusTab = new StatusTab(sn_key);
  initGui();  
  show_all_children();
}
 
Gui::Gui(MapConfig * mapConfig)
  : map_config(mapConfig)
{
  m_mapWidget= new MapWidget(map_config);
  updateSNKey();
  startTab = new StartTab(sn_key);
  statusTab = new StatusTab(sn_key);
}
Gui::~Gui()
{
  // do some memory clean up
  delete startTab;
  delete statusTab;
  delete m_mapWidget;
}

void Gui::initGui()
{
  set_title("SkyNet"); // set the title of the window
  set_border_width(10); // set the border size 
#warning "Hard-coded window size not good"
  set_default_size(600,400); // default size of the 
  notebook.set_border_width(5); // set the border width of the tabbed panes
  //initMapConfig();
  notebook.append_page(*m_mapWidget,"MapDisplay"); // add the mapdisplay tab  
  notebook.append_page(*startTab,"Module Starter"); // add the adrive tab
  notebook.append_page(*statusTab,"StatusTab"); // add the main tab
  //notebook.append_page(globalPTab,"Global Planner"); // add the global planner tab
  //notebook.append_page(ladarTab, "Ladar"); // add the ladar tab
  //notebook.append_page(stereoTab, "Stereo"); // add the stereo tab
  add(notebook);
  show_all_children();
}

void Gui::initMapConfig()
{

}

void Gui::updateSNKey()
{
  /** 
   * Read skynet key from environment
   */
  sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if( pSkynetkey == NULL )
    {
      cerr << "SKYNET_KEY environment variable isn't set" << endl;
    }
  else
    {
      sn_key = atoi(pSkynetkey);
    }
  cout << "Constructing skynet with KEY = " << sn_key << endl;
}

void Gui::startThreads()
{
  //cout << "Spawning Gui sub-Thread" << endl;
  status_thread = Glib::Thread::create( sigc::mem_fun(startTab, &StartTab::statusDisplay), false);
  status_thread = Glib::Thread::create( sigc::mem_fun(statusTab, &StatusTab::listen), false);
  ///DGCstartMemberFunctionThread(startTab,&StartTab::statusDisplay);
}

void Gui::buildStartTab(string configFile)
{
  startTab->readFile(configFile);
  startTab->show_all_children();
}
