/**
 * StatusTab.cc
 * Revision History:
 * 01/07/2005  hbarnor  Created
 */

#include <iostream>

#include "StatusTab.hh"

StatusTab::StatusTab(int sn_key)
  : statusFrm("Status Table"),
    m_skynet(SNGuimodstart, sn_key),
    labelCount(0),
    timeOutValue(1500) //start counting labels from zero
{
  myInput = SNmodlist;
  sender = ALLMODULES;
  /**
  Gtk::Frame *frame1 = manage(new MultiFrame("Test 1"));
  Gtk::Frame *myLabel = manage(new Status("temp1", 2));
  Gtk::Frame *myLabel2 = manage(new Status("temp2", 2));
  Gtk::Frame *myLabel3 = manage(new Status("temp3", 2));
  Gtk::Frame *myLabel4 = manage(new Status("temp4", 2));
  Gtk::Frame *myLabel5 = manage(new Status("temp5", 2));
  Gtk::Frame *myLabel6 = manage(new Status("temp6", 2));
  frame1->add(*myLabel);
  frame1->add(*myLabel2);
  frame1->add(*myLabel3);
  frame1->add(*myLabel4);
  frame1->add(*myLabel5);
  frame1->add(*myLabel6);
  add(*frame1);
  */
  show_all_children();
}

StatusTab::~StatusTab()
{

}


void StatusTab::listen()
{
  int listenSocket = m_skynet.listen(myInput,sender);
  char* buffer[BUFSIZE];
  while(1)
  {
    int size = m_skynet.get_msg(listenSocket, buffer, BUFSIZE, 0);
    unsigned int i;
    for(i=0; i<size/sizeof(ml_elem) ;i++)
      {
	ml_elem* mod = ((ml_elem*)buffer + i);
	struct in_addr addr;
	addr.s_addr = (in_addr_t)htonl((mod->location));
	//printf("%s\t%s\t%u\t\n", modulename_asString(mod->name), inet_ntoa(addr), mod->id); 
	intelliAdd(mod->location, mod->name);
	//ignore id
	// location is an ip address 
	// name is enum
      }
    //printf("\n");
  }
}

void StatusTab::intelliAdd(unsigned location, modulename name)
{
  //find map for this location
  ComputerMap::iterator myIter = myComps.find(location);  
  StatusMap::iterator module;
  if(myIter == myComps.end())
    {
      cerr << "Did not find computer" << endl;
      //create map and insert status if 
      // it does not already exist
      Status *newModule = manage(new Status(Glib::ustring(modulename_asString(name)),location));
      StatusMap *newSMap = new StatusMap();
      newSMap->insert(make_pair(name,newModule));
      myComps.insert(make_pair(location, newSMap));
      addLabel(location, newModule);
    }
  else
    {
      // map for location exists
      // get map of id's and locate the id
      module = myIter->second->find(name);
      if(module == myIter->second->end())
	{
	  // insert module if it does not aleady 
	  // exist
	  Status *newModule = manage(new Status(modulename_asString(name),location));
	  myIter->second->insert(make_pair(name,newModule));
	  addLabel(location,newModule);
	}
      else
	{
	  module->second->setActive(true);
	  //cout << "setting as active" << endl;
	}
    }
}

void StatusTab::addLabel(unsigned location, Status *label)
{
  //create a slot for the label in the main display thread 
  sigc::slot<bool> my_slot = sigc::bind(sigc::mem_fun(label,&Status::on_timeout),timeOutValue);
  //connect slot to Glib::signal_imeout()
  sigc::connection conn = Glib::signal_timeout().connect(my_slot,timeOutValue);
  struct in_addr addr;
  addr.s_addr = (in_addr_t)htonl((location));
  std::map< unsigned, Gtk::Frame* >::iterator frameIter = myFrames.find(location);
  if(frameIter == myFrames.end())
    {
      frameIter= myFrames.insert(make_pair(location,manage(new MultiFrame(Glib::ustring(inet_ntoa(addr)))))).first;      
    } 
  frameIter->second->add(*label);
  add(*(frameIter->second));
  show_all_children();
}
