/**
 * Status.hh
 * Revision History:
 * 05/22/2005  hbarnor  Created
 */

#include "Status.hh"

#include <iostream>

Status::Status(const Glib::ustring &label, int location, bool mnemonic)
  : location(location),
    name(label),
    active(true),
    counter(countdown)
{
  myLabel.add_label(label,mnemonic);
  init();
}

Status::Status(const Glib::ustring &label, Gtk::AlignmentEnum xalign, Gtk::AlignmentEnum yalign, int location, bool mnemonic)
  : location(location),
    name(label),
    active(true),
    counter(countdown)
{
  myLabel.add_label(label,mnemonic, xalign, yalign);
  init();
}

void Status::init()
{  
  GdkColor color; 
  //GdkColor fcolor; 
  gdk_color_parse("green", &color);
  myLabel.modify_bg(Gtk::STATE_NORMAL,Gdk::Color(&color));
  add(myLabel);
  //gdk_color_parse("red", &fcolor);
  //Glib::RefPtr<Gtk::RcStyle> style = get_modifier_style();
  //style->set_bg(Gtk::STATE_NORMAL,Gdk::Color(&color));
  //
  //style->set_fg(Gtk::STATE_NORMAL,Gdk::Color(&fcolor));
  //modify_style(style);
  show_all_children();
}

Status::~Status()
{
  //nothing yet
}

bool Status::getActive()
{
  return active;
}

void Status::setActive(bool newState)
{
  active = newState;
}

bool Status::on_timeout(int count)
{
  if(counter != 0)
    {
      --counter;
      //cout << counter << endl;
    }
  else
    {
      if(!active)
	{
	  //cout << "Not active " << endl;
	  GdkColor color; 
	  gdk_color_parse("red", &color);
	  myLabel.modify_bg(Gtk::STATE_NORMAL,Gdk::Color(&color));
	  //	  add(myLabel);
	}
      else
	{
	  //cout << "Setting active false" << endl;
	  active = false;
	  GdkColor color; 
	  gdk_color_parse("green", &color);
	  myLabel.modify_bg(Gtk::STATE_NORMAL,Gdk::Color(&color));
	}
      counter = countdown;
    }
  return true;
}
