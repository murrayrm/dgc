/**
 * Status.hh
 * Revision History:
 * 05/21/2005  hbarnor  Created
 */

#ifndef STATUS_HH
#define STATUS_HH



#include <gtkmm/eventbox.h>
#include <gtkmm/frame.h>
//#include <gtkmm/rc.h>
//#include <glibmm/ustring.h>
#define countdown 2
using namespace std;

/**
 * Status - This is a colored Gtk Frame with a label for displaying 
 * the status of a skynet module. Its green when the module is active 
 * and red otherwise. It also encapsulates the data about the module
 * like its location(computer) and its name(modulename).
 */
class Status : public Gtk::Frame
{

public:
  /**
   * Constructor with string
   *
   */
  Status(const Glib::ustring &label, int location, bool mnemonic=true);
  /**
   * constructor with alignment
   *
   */
  Status(const Glib::ustring &label, Gtk::AlignmentEnum xalign, Gtk::AlignmentEnum yalign, int location, bool mnemonic=true);
  /**
   * Destructor
   */
  ~Status();
  /**
   *
   * accessor methods for the active flags
   */
  void setActive(bool newState);
  bool getActive();
  /**
   * Counts n-timeouts then,if active makes non-active 
   * without changing colors. If it shows up as not-active
   * then changes color
   */
  bool on_timeout(int count);
private:
  /**
   * do initialization stuff for 
   * label like set colors
   */
  void init();
  /**
   *The label instance inside the fame
   *
   */
  Gtk::EventBox myLabel;
  /**
   * holds the nol IP address of the modules location
   */
  int location;
  /**
   * holds the moduleName
   */
  Glib::ustring name;
  /**
   * flag for updating te color of the 
   * label. True is green and false is red
   */
  bool active;
  /**
   * variable to remember state, last time 
   * it was updated
   */
  bool oldState;
  /**
   * counter for number of time sto timeout
   */
  int counter;
};

#endif
