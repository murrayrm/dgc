/**
 * StatusTab.h
 * Revision History:
 * 05/19/2005  hbarnor  Created
 */

#ifndef STATUSTAB_HH
#define STATUSTAB_HH

#include <netinet/in.h>
#include <arpa/inet.h>

#include <map>
#include <gtkmm/box.h>
#include <gtkmm/frame.h>

#include "sn_msg.hh"
#include "Status.hh"
#include "MultiFrame.hh"

using namespace std;

#define DEBUG false
#define BUFSIZE 1024


/**
 * Create a map with the module id as key 
 * and the Status label as the object
 */
typedef map<modulename,Status*> StatusMap;
/**
 * Create a map with the location as key 
 * and the StatusMap as the object
 */
typedef map<unsigned, StatusMap*> ComputerMap;

/**
 * StatusTab class. StatusTab provides an overview and summary.
 * Displays the status of all modules. Controls for the 
 * for the various modules can also be found here. In 
 * addition displays a scrollable log of all past actions.
 * $Id$ 
 */


class StatusTab : public Gtk::VBox
{

 public:

  /**
   * Default constructor.
   * Contains a status table, command buttons
   * and a log.
   */
  StatusTab(int sn_key);
  /**
   * StatusTab Destructor - for taking care of dynamically 
   * allocated memory. 
   */
  ~StatusTab();
  /**
   * listen - listens for skynet module list  chirps(messages)
   * receives chirps and calls rebuild to display status information
   *
   */
  void listen();
private:
  /**
   * Adds the label to the appropriate frame
   *
   */
  void addLabel(unsigned location, Status *label);
  /**
   * intelligently adds a label for each module 
   * reported. There is only one label for each 
   * module
   */
  void intelliAdd(unsigned location, modulename name);
  /**
   * Declare the computer map
   * that will be used to 
   * hold the data
   */
  ComputerMap myComps;
  /**
   * Module Status
   * Container for displaying the status of the 
   * various modules.
   */
  Gtk::Frame statusFrm;
  /**
   * a skynet client
   */
  skynet m_skynet;
  /**
   * mesgs i recive, this is the
   * moduleStarter status message
   */
  sn_msg myInput;
  /**
   * sender of chirping messages
   *
   */
  modulename sender;
  /**
   * Map of location to frames
   * the key is the location 
   */
  std::map< unsigned,Gtk::Frame* > myFrames;
  /**
   * int to keep track of te number of labels/modules 
   * we have. Also used in creating slot 
   */
  int labelCount;
  int timeOutValue;
};

#endif
