/* AState.hh
 *
 * JML 31 Dec 04
 *
 */

#ifndef ASTATE_HH
#define ASTATE_HH

#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <fstream>

#include "VehicleState.hh"

#include "LatLong.h"
#include "frames/frames.hh"
#include "frames/rot_matrix.hh"
#include "gps.hh"
#include "imu_fastcom.h"
#include "sn_msg.hh"
#include "sn_types.h"

#include "DGCutils"

#include "kfilter/kfilter.h"
#include "kfilter/Kalmano.h"
#include "kfilter/Global.h"

#include "SensorConstants.h"

/*!  MetaState struct.  This is the struct that stores higher level information about
 *   the status of various components of the stateserver.
 */
struct MetaState
{
    unsigned long long Timestamp;    

    int gps_enabled;
    int imu_enabled;
    int mag_enabled;

    int gps_active;
    int imu_active;
    int mag_active;
    
    int gps_pvt_valid;
    int ext_jump_flag;  
};

/*! AState Class.  AState is the State Estimation class for Alice.
 *  GPS, and IMU information are combined through the use of a NGC Kalman filter.
 *  There are separate threads to manage each source of input data, and a separate
 *  function that gets called to update the state.  A final thread takes care of
 *  messaging with other modules via Skynet.
 *  \brief This is the class that implements the state server.
 */
class AState
{
    public:
    /*! The skynet class that takes care of messaing */
    skynet m_skynet;

    /*! Flag for verbose messaging */
    int verbose;

    /*! Flag listing the status of the K-filter.  1 = enabled.  0 = disabled. -1 = Couldn't Start. */
    int kfilter_enabled;
    /*! Flag listing the status of the IMU.  1 = enabled.  0 = disabled. -1 = Couldn't Start. */
    int imu_enabled;
    /*! Flag listing the status of the GPS.  1 = enabled.  0 = disabled. -1 = Couldn't Start. */
    int gps_enabled; 
    /*! Flag listing the status of the Mag.  1 = enabled.  0 = disabled. -1 = Couldn't Start. */
    int mag_enabled;

    /*! Index into NavBuffer from last time state was updated.  This is used to prevent double-updates and acceleration zeroing. */
    int lastIndex;

    int broadcastCounter;

    /*! Struct to store vehicle state. */
    struct VehicleState vehiclestate;
    /*! Struct to store meta state. */
    struct MetaState	metastate;

    /*! Mutex for vehicle state. */
    pthread_mutex_t m_VehicleStateMutex;
    /*! Mutex for meta state. */
    pthread_mutex_t m_MetaStateMutex;

    /*! Mutex for manipulation of IMU data. */
    pthread_mutex_t m_IMUDataMutex;
    /*! Mutex for manipulation of GPS data. */
    pthread_mutex_t m_GPSDataMutex;
    /*! Mutex for manipulation of Mag data. */
    pthread_mutex_t m_MagDataMutex;

    pthread_mutex_t m_KalmanMutex;

    int broadcast_statesock;

    /*! Time when AState was started. */
    unsigned long long starttime;

    /*! Imu data class that incoming data is read into. */
    IMU_DATA imudata;
    /*! Number of times IMU has been read from. */
    int imu_count;
    /*! Time that the IMU was last read from. */
    unsigned long long my_imu_time;

    unsigned long long imu_log_start;

    /*! GPS data class that incoming data is read into. */
    gpsDataWrapper gpsdata;
    /*! Number of times GPS has been read from. */
    int gps_count;
    /*! Time that the GPS was last read from. */
    unsigned long long my_gps_time;

    unsigned long long gps_log_start;
    /*! Whether or not the GPS signal is currently valid. */
    int gps_valid;

    double mag_heading;

    int mag_count;

    int stationary;

    /*! Variable used in calculation of crude accelerations. */
    double timediff;
    /*! Class used for lat-long / UTM conversions */
    LatLong latlong;
    /*! Part of frames code used in reference frame transformations */
    XYZcoord blank;
    /*! Part of frames code used in reference frame transformations */
    XYZcoord imu_offset;
    /*! Part of frames code used in reference frame transformations */
    frames imu_vector;
    /*! Part of frames code used in reference frame transformations */
    XYZcoord imu_correct;

    double gps_err;

    double gps_north;

    double gps_east;

    int snkey;

    int log_raw;
    int replay;
    fstream imu_log_stream;
    fstream imu_replay_stream;
    fstream gps_log_stream;
    fstream gps_replay_stream;
    fstream mag_log_stream;
    fstream mag_replay_stream;
    char imu_log_file[104];
    char imu_replay_file[104];
    char gps_log_file[104];
    char gps_replay_file[104];
    char mag_log_file[104];
    char mag_replay_file[104];


//public:
    /*! AState Constructor */
    AState(int skynet_key, int USE_KF, int USE_IMU, int USE_GPS, int USE_MAG, int USE_VERBOSE, 
            double MAG_HDG, int LOG_RAW, char* LOG_FILE, int REPLAY, char* REPLAY_FILE);

    ~AState();

    /*! Function that initializes the IMU */
    void IMU_init();
    /*! Function that initializes the GPS */
    void GPS_init();
    /*! Function that initializes the Mag */
    void Mag_init(double MAG_HDG);
    /*! Function that updates the vehicle_state struct */
    void UpdateState();
    /*! Function to print out the vehicle state */
    void printVehicleState();
    /*! Function to print out the meta state */
    void printMetaState();

    /*! Function to broadcast state data */
    void Broadcast();
    /*! Thread to read from the IMU.  Note: this is where UpdateState normally gets called since it's the fastest thread.*/
    void IMU_thread(); 
    /*! Thread to read from the GPS.*/
    void GPS_thread();
    /*! Thread to read from the Mag.*/
    void Mag_thread();

    void UpdateSparrow_thread();

    void SparrowDisplayLoop();

    void Restart();
};

#endif
