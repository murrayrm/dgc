/****************************************************************************
 *
 * INS/GPS, Kalman Filter Updates
 *
 * File: kalmanu.h
 * Date: 04/17/00
 *
 ****************************************************************************/
/****************************************************************************
                        [REVISION HISTORY]

  Date     Engineer             Description
==============================================================================
 02/09/00  N. Datta             Add kuQuaternionUpdate
 04/17/00  T. Varty             Move kalman_update and compute_sigma_ratio
                                definitions to kalmanu.c
*****************************************************************************/

#ifndef KalmanUH
#define KalmanUH

/****************************************************************************
 * Function Definitions
 ****************************************************************************/

void kalmanUInit(void);
void kalmanUReinit(void);
void kuWaZeroVelUpdate(void);
void kuZeroHorVelUpdate(int update_request);
void kuZeroVertVelUpdate(int update_request);
//void kuGpsRngUpdate(void);
//void kuGpsDRngUpdate(void);
void kuBaroUpdate(void);
void kuQuaternionUpdate(TPhi phi, TQuaternion qmid);
void kuExtGPSPosUpdate(void);
void kuExtGPSVelUpdate(void);

/****************************************************************************
 * End
 ****************************************************************************/
#endif
