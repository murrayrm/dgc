/* Modeman.cc 
 * Core functions of mode management without skynet or MTA shell.
 * Revision History:
 *  2/25/2005     Sue Ann Hong     Created from ModeManModule.cc
 *  3/20/2005     Sue Ann Hong     Back to being a part of ModeManModule class
 *     Ugly, but it's the easiest way to get state to ModeMan core functions
 *     and deadbaby functions. 
 *  3/22/2005     Sue Ann Hong     Starting to complete modeswitching
 *     Fault management will change the mode switch grid, but it's about time
 *     to put in all modes in the mode switch grid to prepare for that time
 *     when we actually get skynet messages with source names (so we can handle
 *     multiple sources of trajectories).
 *
 * Search for
 *  TODO  to find incomplete stuff
 *  PARAM to find variable parameters (part of incomplete stuff, I'd say)
 */

#include <iostream>
#include <fstream>
using namespace std;

#include "ModeManModule.hh"
#include "CMapPlus.hh"
#include "trajMap.hh"
#include "DGCutils"

// Sparrow e-stop pause signal. ugly, but blame sparrow.
extern bool sparrowEStopPauseSignal;
extern int NODISPLAY;
extern int NODRIVE;

/** InitModeMan:
 * Mostly initializes modeman core variables. Why can't this be in
 * ModeMan.cc? **/
void ModeManModule::InitModeMan(bool noDisplay) 
{
  cout << "ModeManModule: Starting Init" << endl;

  this->noDisplay = noDisplay;
 
  //------------------private member Initialization--------------------------//
  /* Mode-switch grid:
   *  Each mode has a different priority list for switching.
   */
  /* TODO
   * 3/21/2005     For the spring field test, we only need to switch between 
   *   PASSTHROUGH and STOP. Hence this matrix prefers PASSTHROUGH to
   *   STOP because we switch from PASSTHROUGH to STOP only on e-stop
   *   pause signals, and we want to switch from STOP to PASSTHROUGH
   *   more than STOP to STOP when we're done with "emergency" or
   *   e-stop pause situations.
   *  It is important to change this matrix given the situation.
   *  The full implementation of ModeMan switching mechanism may
   *  require a different scheme, but desirably not, because this
   *  is nice and pretty. The full version will hopefully come
   *  soon.
   */
  for (int i = 0; i < ModeManModes::COUNT; i++) {
    for (int j = 0; j < ModeManModes::COUNT; j++) {
      switchPList[i][j] = j;
    }
  }

  // Initialize update counters
  for(int i= 0; i< PathInput::COUNT; i++) { UpdateCount[i] = 0; }
  CommandSentCount = 0;

  /* paths */
  for( int i = 0; i < PathInput::COUNT; i++)
  {
    new_path[i] = new CTrajMap(PATH_ORDER);   // just arrived from modules///
    newPathPresent[i] = false;
    lastUpdate[i] = TimeZero;           // a timestamp for all path sources

    path[i] = new CTrajMap(PATH_ORDER);       // paths we're evaluating on///
    safety[i] = 0;     // safety of each path
    present[i] = false;                    // whether each module is present
    pathUpdateCount[i] = 0;
  }
  numNewPaths = 0;
  numPresent = 0;

  // curPath is just a pointer, not a full object. Set it to NULL while
  // there are no valid input paths.
  curPath = NULL;
  // TODO: curSafety should be frequently updated by deadbaby sensing
  curSafety = 0;                    // safety of the curPath at the moment
  curPathModule = PathInput::COUNT;
  // PARAM: For now it is
  ModeManMode = ModeManModes::PASSTHROUGH; 
  global_speedlimit = DEFAULT_GLOBAL_SPEEDLIMIT;
  needToStop = false;
  speedLimitAge=0;

  // Sparrow Variables
  pathOutCount = 0;
  updateCountState = 0;
  PassThroughModule = -1;    // -1 is for auto (i.e.picks whatever's available)

  // add'l parameters (too advanced for now 12/26/2004 SAH)
  //paranoid = DEFAULT_PARANOID;
  //resourceLevel = DEFAULT_RESOURCE_LEVEL;

  // Create locks
  for (int i = 0; i < PathInput::COUNT; i++) {
    DGCcreateMutex(&newPathMutex[i]);
  }
  // start Sparrow Display -- in ModeManModuleMain.cc

  cout << "ModeManModule: Init Finished" << endl;

}

/* Specifies under what circumstances one can request a new path.
 * Currently, the condition is if there's a new path from a module
 * or if there's an emergency .
 */
bool ModeManModule::needToSendPath() {
  return ( numNewPaths || needToStop );
}


/* Checks for validity of each new_path, path, and updates path */
void ModeManModule::UpdateModeManPaths( ) 
{
  // Update paths for this cycle
  for(int i = 0; i < PathInput::COUNT; i++) {
    
    // TODO: NEED A MORE SOPHISTICATED IntpuTimeout value (1sec 12/29/04)
    // It shoudl depend on displacement? (i.e. how much we've moved) 
    //  -- it doesn't count for obstacle detection though (moving obstacles,
    //     orientation)
    unsigned long long now;
    DGCgettime(now);
    // TODO: it seems like present[i] isn't getting updated??? (sparrow)
    if( (now - lastUpdate[i]) > INPUT_TIMEOUT) {
      // It has been too long since this input has been updated, : ignore
      present[i] = false;      // whether each module is present
    }

    /* 
     * Don't need to lock path unless we touch it in another thread
     * which we don't at the moment (only new_path gets touched by 
     * another thread that takes in skynet CTraj messages) 
     */
    DGClockMutex(&newPathMutex[i]);
    if (newPathPresent[i]) {
      // Input has a new path so update its path with new_path
      *(path[i]) = *(new_path[i]);   //// CTraj operator= copy
      present[i] = true;   // with this we can keep track of valid paths
      numPresent++;
      pathUpdateCount[i]++;
    }
    DGCunlockMutex(&newPathMutex[i]);

  } // end of for(ModeMan inputs)
  
  numNewPaths = 0;
}

void ModeManModule::UpdateModeManFaults()
{
  /* For now, we take e-stop signals from sparrow
   * Later on, they'll come as a skynet message and the var will be modified
   * in the wrapper 
   */
  if(!NODRIVE)
    needToStop = sparrowEStopPauseSignal || m_actuatorState.m_estoppos != 2 ||
        chkInputPathSafety();
  else
    needToStop = sparrowEStopPauseSignal || chkInputPathSafety();
  // TODO: curSafety should be frequently updated by deadbaby sensing
  //curSafety = 0;                    // safety of the curPath at the moment

  //UpdateDBS();
  //check_global_speedlimit();

  // add'l parameters (too advanced for now 12/26/2004 SAH)
  //paranoid = DEFAULT_PARANOID;
  //resourceLevel = DEFAULT_RESOURCE_LEVEL;
}

/* fault management - emergency var's */
void ModeManModule::ManageFaults() {
  /* For now, we take e-stop signals from sparrow
   * Later on, they'll come as a skynet message and the var will be modified
   * in the wrapper 
   */
  //apply_global_speedlimit();
} // end of ManageFaults


/* Given the current paths and conditions, pick a path to send to path 
   follower */
int ModeManModule::PickNewMode() {

  //----------------------Finding the new mode-----------------------------//
  // The big switch statement of modes. the contents should follow the 
  // mode-switch grid.
  if (!needToStop) {
    int tryMode = ModeManMode;
    for (int i = 0; i < ModeManModes::COUNT; i++) {
      tryMode = switchPList[ModeManMode][i];
      if (canSwitchTo(tryMode)) {
        if (noDisplay) {
          cout << "ModeManModule: switching from mode " << ModeManMode 
               << "to mode " << tryMode << endl;
          }
        switchTo(tryMode);
	    break;	 
      }
    }
  }
  else switchTo(ModeManModes::STOP);
  
  if (noDisplay) cout << "ModeManModule: Mode = " << ModeManMode << endl;
  return 0;
}

CTraj * ModeManModule::getcurPath() {
  //----------------------Now Choose Path-----------------------------//
  if (ModeManMode == ModeManModes::STOP) {
    //Places a speedlimit of 0 to curPath
    // Note that if curPath is null then even if there was an e-stop
    // pause signal we wouldn't get here.
      getStopPath();
  }
  else if (ModeManMode == ModeManModes::PASSTHROUGH) {  
    int next;  
    if (PassThroughModule == -1) {
      /* in AUTO mode, the module we're following. for now, we just take the 
       * first module that's present, and we have no smooth switching
       * mechanisms, so we must only have 1 module feeding paths. 
       */
      for (int i = 0; i < PathInput::COUNT; i++) {
        if (present[i]) {
          next = i;
          break;
        }
      }
    }
    else {
      next = PassThroughModule;
    }

    // don't need to lock since curPath is handled in only one thread
    curPath = path[next];                 // point to path[next]
    curPathModule = next;
    
    // TODO: Update timestamp of the curpath (not in datum yet 12/26/2004)
    CommandSentCount++;    // update the counter
  }
  // TODO: implement path-getting for other modes
  else {
    cout << "ModeMan: invalid mode: " << ModeManMode << endl;
    return NULL;
  }
  return curPath;
}


// Helper functions (mostly used in Active)

/* Return 0 if there exists no safe path */
int ModeManModule::chkInputPathSafety()   
{
  if(curPath != NULL) return curPath->isTrajObstructed(&m_map, m_layer);
  else return 0;
}

// DecFT: basic version: the only possible switch is from passthrough to stop
/**
 * Note that this is entirely unnecessary if there are no emergency situations
 * involved in mode switching.
 **/
int ModeManModule::canSwitchTo(int tryMode)
{
  switch (tryMode) {
    case ModeManModes::PASSTHROUGH: return 1; break;
    case ModeManModes::STOP: return 1; break;
    case ModeManModes::FAST: break;
    case ModeManModes::NORMAL: break;
    case ModeManModes::SLOW: break;
    case ModeManModes::REVERSE: break;
  }
  return 0;
} // end of canSwitchTo

/**
 *  Switching from a mode to another may require more than just changing
 * the variable ModeManMode.
 **/
int ModeManModule::switchTo(int newMode)
{
  ModeManMode = newMode;
  // other stuff when changing mode
  return 0;
}

/**
 * If we have a curPath, modifies curPath to a stoppping path and returns it.
 * If there's no curPath (i.e. NULL), then returns NULL.
 **/
CTraj *ModeManModule::getStopPath()
{
  if (curPath == NULL) {
    if (m_state.Speed2() > .0001) {
      /* We'd be here if we were driving manually (i.e. no traj available)
	     * and got e-stop paused. In that case, we don't send any path to
	     * path follower since path follower shouldn't already have a path 
	     * that's of non-zero speed.
	     **/
      cerr << "ModeManModule::getStopPath() "
        << "- curPath is null and we're moving." << endl;
    }
    return NULL;
  }
  else {
    NEcoord current_position(m_state.Northing_rear(), m_state.Easting_rear());
    
    ///////////////// DEBUG: print path
    //ofstream outfile("debug.traj");
    //curPath->print(outfile);
    
    /* Not locking curPath because this shouldn't be called in parallel with
       path[] accessing functions anyway. */
    curPath->speed_adjust(0.,&current_position, m_state.Speed2());
    ///////////////// DEBUG: print path
    //curPath->print(outfile);
    return curPath;
  } // end of if curpath is null else ...

} // end of getStopPath()

/**
 * Intended to incorporate inputs from external modules (static, rddf, etc),
 * but it seems unlikely we'll do this at this point. The only real speedlimit
 * we need to impose so far is from deadbaby sensing 
 * (ModeManModule::UpdateDBS).
 **/
int ModeManModule::check_global_speedlimit()
{
  if(dbsLimit>global_speedlimit)
  {
    global_speedlimit = dbsLimit;
    speedLimitAge=0;
  }
  else
  {
    speedLimitAge++;
  }
  if(speedLimitAge*SLEEP_ACTIVE_LOOP>SPEEDLIMIT_RESET_AGE)
    global_speedlimit=DEFAULT_GLOBAL_SPEEDLIMIT;
  return 0;
}

/* Modifies curPath to the given global speedlimit. */
int ModeManModule::apply_global_speedlimit()
{
  if (curPath == NULL) {
    if (m_state.Speed2() > .0001) {
      /* We'd be here if we were driving manually (i.e. no traj available)
	     * and got e-stop paused. In that case, we don't send any path to
	     * path follower since path follower shouldn't already have a path 
	     * that's of non-zero speed.
	     **/
      cerr << "ModeManModule::getStopPath() "
        << "- curPath is null and we're moving." << endl;
    }
    return 1;
  }
  else {
    NEcoord current_position(m_state.Northing_rear(), m_state.Easting_rear());
    // Not locking curPath because this shouldn't be called in parallel with
    // path[] accessing functions anyway.
    curPath->speed_adjust(global_speedlimit,&current_position, m_state.Speed2());
    // modify path given to global speedlimit and and broadcast to other modules 
    return 0;
  }
}
