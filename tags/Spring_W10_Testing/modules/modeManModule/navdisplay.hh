#ifndef NAVDISPLAY_HH
#define NAVDISPLAY_HH

static int user_quit(long arg);
static int user_estop_pause(long);
static int user_estop_unpause(long);

// Sparrow e-stop pause signal. ugly, but blame sparrow.
bool sparrowEStopPauseSignal = false;

// Added for display 12/29/04 

/* TODO: want all input paths */

// State struct vars
double Easting = 0, Northing = 0;
double Altitude = 0;
double Vel_N = 0, Vel_E = 0, Vel_D = 0;
double Acc_N = 0, Acc_E = 0, Acc_D = 0;
double Roll = 0, Pitch = 0, Yaw = 0;
double RollRate = 0, PitchRate = 0, YawRate = 0;
//double RollAcc = 0, PitchAcc = 0, YawAcc = 0;
double dispSS_decimaltime = 0;    // for Timestamp

// Converted vars
double speed = 0;
double PitchDeg, RollDeg, YawDeg;
double PitchRateDeg, RollRateDeg, YawRateDeg;

// Other vars
int StateUpdateCount = 0;
int SparrowUpdateCount = 0;
int InputUpdateCount[PathInput::COUNT];
int NewPathPresent[PathInput::COUNT];
int PathPresent[PathInput::COUNT];
int CurPathModule = PathInput::COUNT;
int CurMode = ModeManModes::COUNT;
int PathOutCount = 0;
int spPassThroughModule = -1;


/*
Voter * GLOBAL_;
Voter * LADAR_;
Voter * STEREO_;
Voter * DFE_;
Voter * ARB_;

Voter vtr;
*/



#endif
