#ifndef RFCOMMUNICATIONS_HH
#define RFCOMMUNICATIONS_HH

#include "StateClient.h"
#include "VehicleState.hh"
#include "SkynetContainer.h"
#include "sn_msg.hh"
#include "TrajTalker.h"
#include <math.h>

class RFCommunications : public CStateClient, public CTrajTalker
{

private:

  void RFCalculateTraj(double dir, double lat);

public:

  sn_msg m_roadFinding;
  int rf_socket;
  int RFSkyNetKey;
  int RFskynetguiSocket;
  int RFtrajSocket;
  int RFtrajLength;

  //bools for sending traj and gui or not
  int RFsendtraj;
  int RFsendgui;

  double RFNorthing;
  double RFEasting;
  double RFRoll;
  double RFPitch;
  double RFYaw;
  double RFAltitude;

  //Used to fill traj
  double RFDestination[2]; //Northing, Easting destination
  double RFVelocity[2]; //Northing, Easting velocities
  double RFAcceleration[2]; //Northing, Easting accelerations

  RFCommunications(int skynetkey, int traj, int gui);
  ~RFCommunications();

  void RFUpdateState();
  void RFSendTraj(double dir, double lat);

  void RFSendToMap(double dir, double lat);

  void RFSendRoad(double x[], double y[], double width[], int size);
};

#endif
