//----------------------------------------------------------------------------
//  Road follower module
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//----------------------------------------------------------------------------

#include "decs.h"

//----------------------------------------------------------------------------

UD_CameraCalibration *camcal          = NULL;
UD_ImageSource *imsrc                 = NULL;
UD_LadarCalibration *ladcal           = NULL;
UD_LadarSource *ladsrc                = NULL;
UD_DominantOrientations *dom          = NULL;
UD_VanishingPoint *vp                 = NULL;
UD_RoadFollower *rf                   = NULL;
UD_OnOff *oo                          = NULL;

UD_Reporter *reporter                 = NULL;
UD_Logger *live_logger                = NULL;

#define SKYNET

#ifdef SKYNET

#include "RFCommunications.hh"
#include <iostream>
#include <iomanip>

using namespace std;

int skynet = 0;
int sendtraj = 0;
int sendgui = 0;
RFCommunications *RFComm;

#endif

//----------------------------------------------------------------------------

int do_computations                   = TRUE;    // run algorithm? (vs. just displaying input)
int do_alice                          = FALSE;   // are we connected to the vehicle?
int input_paused                      = FALSE;   // are we "frozen" on the current image?
int main_window_id;

//----------------------------------------------------------------------------

void display()
{
  // raw source

  if (!do_computations)
    glDrawPixels(imsrc->im->width, imsrc->im->height, GL_RGB, GL_UNSIGNED_BYTE, imsrc->im->imageData);

  // current image, tracker state, etc.

  else 
    rf->draw(imsrc);  

  // ladar projection
  
  if (ladsrc) 
    ladsrc->draw(camcal);

  glutSwapBuffers();
}

//----------------------------------------------------------------------------

void keyboard(unsigned char key, int x, int y)
{
  // pause/unpause current image frame (just a freeze frame if live input--does not buffer)

  if (key == 'p')
    input_paused = !input_paused;
}

//----------------------------------------------------------------------------

void mouse(int button, int state, int x, int y)
{
  if (do_computations) {

    // cycle between raw image view, dominant orientation view, and vote function view
    
    if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) 
      rf->cycle_draw_mode_image();
    
    // cycle between overlaying nothing, tracker particles, or support rays
    
    else if (button == GLUT_RIGHT_BUTTON && state == GLUT_UP) 
      rf->cycle_draw_mode_overlay();
  }

  // save whatever the display is currently showing to file

  if (button == GLUT_MIDDLE_BUTTON && state == GLUT_UP) {
    glReadPixels(0, 0, imsrc->im->width, imsrc->im->height, GL_RGB, GL_UNSIGNED_BYTE, imsrc->im->imageData);
    sprintf(imsrc->frame_filename, "../road_data/saved_%05i.png", imsrc->relative_frame);
    cvConvertImage(imsrc->im, imsrc->im, CV_CVTIMG_SWAP_RB | CV_CVTIMG_FLIP);  // RGB->BGR	
    cvSaveImage(imsrc->frame_filename, imsrc->im);
  }
}

//----------------------------------------------------------------------------

void process_image()
{
  // calculate dominant orientations
  
  dom->compute(imsrc->im);
  
  // find vanishing point
  
  vp->compute(dom->gabor_max_response_index);
  
  // track vanishing point, centerline, make on/off road decision, etc.

  rf->run();
  
  // talk to vehicle controller
  
  if (do_alice) {
    rf->send_output_to_controller(camcal);
#ifdef SKYNET
    if (sendtraj)
      RFComm->RFSendTraj(rf->direction_error, rf->lateral_error);
#endif
  }
}

//----------------------------------------------------------------------------

void compute_display_loop()
{
  glutSetWindow(main_window_id);

  // start timing (of _iteration_, of which there may be multiple per image frame)

  reporter->start_timer(imsrc);

  // perform image processing for current image (frame 0 already "captured" by constructor)

  if (do_computations)
    process_image();

  // draw current image and algorithm output in camera window

  glutPostRedisplay();

  // draw in overhead view window 

  if (do_computations) {
    glutSetWindow(UD_Overhead::overhead->window_id);
    glutPostRedisplay();
  }

  // ready for new sensor data?

  if (!input_paused && (!do_computations || rf->iterate())) {

    // handle logging

    if (live_logger)
      live_logger->write(imsrc, rf);                                      

    // capture 

    imsrc->capture();    
    if (ladsrc)
      ladsrc->capture(imsrc);
  }

  // end timing

  reporter->end_timer(imsrc);
}

//----------------------------------------------------------------------------

/// register callbacks and create one display window

void initialize_display(int width, int height, int argc, char **argv)
{
  int x, y;

  x = 100;
  y = 100;

  if (do_computations) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    UD_Overhead::overhead->initialize_display(x + width + 10, y);
  }

  glutInitWindowSize(width, height); 
  glutInitWindowPosition(x, y);
  main_window_id = glutCreateWindow("road");

  glutIdleFunc(compute_display_loop);
  
  glutDisplayFunc(display); 
  glutMouseFunc(mouse); 
  glutKeyboardFunc(keyboard);

  if (!do_computations) {
    glPixelZoom(1, -1);
    glRasterPos2i(-1, 1);
  }

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, width-1, 0, height-1);
}


//----------------------------------------------------------------------------

void init_per_run(int argc, char **argv)
{
#ifdef SKYNET
  if (skynet) {
    int sn_key = 0;
    char *RFSkyNetKey = getenv("SKYNET_KEY");
    if (RFSkyNetKey == NULL) {
      printf("SKYNET_KEY environment variable is not set!!\n");
      exit(1);
    }
    else
      sn_key = atoi(RFSkyNetKey);
    printf("sn key = %i\n", sn_key);
    RFComm = new RFCommunications(sn_key, sendtraj, sendgui);
  }
#endif

  //-------------------------------------------------
  // reporting (command-line flags can modify)
  //-------------------------------------------------

  reporter = new UD_Reporter();
  reporter->process_command_line_flags(argc, argv);   

  //-------------------------------------------------
  // command-line flags (imsrc initialized after this)
  //-------------------------------------------------

  process_general_command_line_flags(argc, argv);

  //-------------------------------------------------
  // short circuit rest of init sequence?
  //-------------------------------------------------

  if (!do_computations) {
    initialize_display(imsrc->im->width, imsrc->im->height, argc, argv);
    glutMainLoop();
    return;
  }

  //-------------------------------------------------
  // random number generation
  //-------------------------------------------------

  initialize_UD_Random();

  //-------------------------------------------------
  // dominant orientation estimation
  //-------------------------------------------------

  dom = new UD_DominantOrientations(imsrc->im->width, imsrc->im->height, 36, 0.0, 180.0, 4.0);
  dom->process_command_line_flags(argc, argv);

  //-------------------------------------------------
  // vanishing point finding
  //-------------------------------------------------

  vp = new UD_VanishingPoint(dom, 0.2, 0.0);
  vp->process_command_line_flags(argc, argv);
  if (ladsrc) {
    ladsrc->win_dx = (float) -vp->candidate_lateral_margin; 
    ladsrc->win_dy = (float) -vp->candidate_top_margin;    
  }
 
  //-------------------------------------------------
  // on vs. off road confidence
  //-------------------------------------------------

  oo = new UD_OnOff(imsrc, 0.1, 300, 0.667);
  oo->process_command_line_flags(argc, argv);

  //-------------------------------------------------
  // tracking vanishing point
  //-------------------------------------------------

  rf = new UD_RoadFollower(vp, oo, ladsrc);  
  rf->process_command_line_flags(argc, argv);

  //-------------------------------------------------
  // overhead view (constructed in UD_Overhead.cpp)
  //-------------------------------------------------

  UD_Overhead::overhead->rf = rf;

  //-------------------------------------------------
  // start OpenGL GLUT
  //-------------------------------------------------

  initialize_display(vp->cw, vp->ch, argc, argv);
  glutMainLoop();
}

//----------------------------------------------------------------------------

void process_general_command_line_flags(int argc, char **argv)
{
  int i, mode;

  for (i = 1; i < argc; i++) {

#ifdef SKYNET
    if (!strcmp(argv[i], "-sendtraj"))
      sendtraj = 1;
    else if (!strcmp(argv[i], "-sendgui"))
      sendgui = 1;
    else if (!strcmp(argv[i], "-skynet"))
      skynet = 1;
#endif

    // path to image file listing, start frame (by place in file list), end frame, target image width (180 recommended)

    if (!strcmp(argv[i],                             "-image_list")) 
      imsrc = new UD_ImageSource_Image(camcal, argv[i + 1], atoi(argv[i + 2]), atoi(argv[i + 3]), atoi(argv[i + 4]));

    // path to AVI file, start frame, end frame (-1 = last frame of movie), target image width (180 recommended)

    else if (!strcmp(argv[i],                        "-image_avi"))
      imsrc = new UD_ImageSource_AVI(camcal, argv[i + 1], atoi(argv[i + 2]), atoi(argv[i + 3]), atoi(argv[i + 4]));

      // camera number, mode, end frame (-1 = NONE), target image width (180 recommended)

    else if (!strcmp(argv[i],                        "-image_live")) {
      if (!strcmp(argv[i + 2],      "MODE_640x480_MONO"))
	mode = MODE_640x480_MONO;
      else if (!strcmp(argv[i + 2], "MODE_640x480_YUV422"))
	mode = MODE_640x480_YUV422;
      else if (!strcmp(argv[i + 2], "MODE_320x240_YUV422"))
	mode = MODE_320x240_YUV422;
      else {
	printf("unsupported firewire camera mode\n");
	exit(1);
      }

      imsrc = new UD_ImageSource_Live(camcal, atoi(argv[i + 1]), mode, atoi(argv[i + 3]), atoi(argv[i + 4]));
    }

    // path to scans, path to image timestamp logs, maximum timestamp difference

    else if (!strcmp(argv[i],                        "-ladar_scans")) {  // use calibration for cab ladar at Stoddard Wells

      if (!imsrc) {
	printf("you must put the -image_* specifier before the -ladar_scans flag on the command line\n");
	exit(1);
      }

      ladsrc = new UD_LadarSource_Scans(argv[i+1], ladcal);
      imsrc->setup_timestamp(argv[i + 2]);
      ladsrc->max_diff = atof(argv[i + 3]);
    }
    else if (!strcmp(argv[i],                        "-nocompute")) 
      do_computations = FALSE;

    // path, save image interval

    else if (!strcmp(argv[i],                        "-log")) 
      live_logger = new UD_Logger(argv[i + 1], atoi(argv[i + 2]));

    // try to connect to skynet...

    else if (!strcmp(argv[i],                        "-alice"))  
      do_alice = TRUE;

    // ladar's mount point, FOV

    else if (!strcmp(argv[i],                        "-sw_cabladar"))   // use calibration for cab-mounted ladar at Stoddard Wells 
      ladcal = new UD_LadarCalibration(201, 50.0, -0.5, 0.0, -2.387, -0.477, -0.2);

    // see here for information (e.g., FOV) on Caltech's stereo cameras: http://team.caltech.edu/members/

    else if (!strcmp(argv[i],                        "-sw_rightcam"))   // use calibration for right-hand stereo camera at Stoddard Wells 
      camcal = new UD_CameraCalibration(640, 480, DEG2RAD(94.3), 380.94, 379.83, 344.58, 243.72, 0.226, -2.088, -0.468, -0.1);  // k1 = -0.266, k2 = 0.0877
    else if (!strcmp(argv[i],                        "-sw_leftcam"))   // use calibration for left-hand stereo camera at Stoddard Wells 
      camcal = new UD_CameraCalibration(640, 480, DEG2RAD(94.3), 380.94, 379.83, 344.58, 243.72, -0.248, -2.089, -0.464, -0.1); 
  }
}

//----------------------------------------------------------------------------

int main(int argc, char **argv)
{
  init_per_run(argc, argv);

  return 1;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
