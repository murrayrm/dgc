#include "RFCommunications.hh"
#include <iostream>

using namespace std;

RFCommunications::RFCommunications(int skynetkey, int traj, int gui) : CSkynetContainer(SNroadfinding, skynetkey)
{
  RFtrajLength = 100; //meters
  RFSkyNetKey = skynetkey;
  RFsendtraj = traj;
  RFsendgui = gui;

  //Prepare socket for sending road boundary data
  m_roadFinding = SNroadboundary;
  rf_socket = m_skynet.get_send_sock(m_roadFinding);

  //Prepare sockets to send traj and gui data
  if (RFsendgui && RFsendtraj)
    {
      RFskynetguiSocket = m_skynet.get_send_sock(SNtraj);
      RFtrajSocket = m_skynet.get_send_sock(SNRDDFtraj);
    }  
  else if (RFsendtraj)
    {
      RFtrajSocket = m_skynet.get_send_sock(SNtraj);
    }
  else if (RFsendgui)
    {
      RFskynetguiSocket = m_skynet.get_send_sock(SNtraj);
    }

  if (rf_socket < 0)
    {
      cerr << "Error connecting to Skynet!!" << endl;
    }
}

RFCommunications::~RFCommunications()
{
}

void RFCommunications::RFUpdateState()
{
  UpdateState();
  
  RFNorthing = m_state.Northing;
  RFEasting = m_state.Easting;
  RFRoll = m_state.Roll;
  RFPitch = m_state.Pitch;
  RFYaw = m_state.Yaw;
  RFAltitude = m_state.Altitude;
}

void RFCommunications::RFCalculateTraj(double dir, double lat)
{
  RFUpdateState();

  double adjustedTheta = RFYaw + dir / 180 * M_PI;

  if (adjustedTheta > 2 * M_PI)
    adjustedTheta -= 2 * M_PI;

  RFDestination[0] = cos(adjustedTheta) * RFtrajLength + RFNorthing;
  RFDestination[1] = sin(adjustedTheta) * RFtrajLength + RFEasting;

  RFVelocity[0] = 5;
  RFVelocity[1] = 5;

  RFAcceleration[0] = 0;
  RFAcceleration[1] = 0;
}

void RFCommunications::RFSendTraj(double dir, double lat)
{
  double n[3], e[3];
  CTraj *RFtraj = new CTraj(3);

  RFCalculateTraj(dir, lat);

  RFtraj->startDataInput();

  n[0] = RFNorthing;
  n[1] = RFVelocity[0];
  n[2] = RFAcceleration[0];
  e[0] = RFEasting;
  e[1] = RFVelocity[1];
  e[2] = RFAcceleration[1];
  RFtraj->inputWithDiffs(n, e);

  n[0] = RFDestination[0];
  n[1] = RFVelocity[0];
  n[2] = RFAcceleration[0];
  e[0] = RFDestination[1];
  e[1] = RFVelocity[1];
  e[2] = RFAcceleration[1];
  RFtraj->inputWithDiffs(n, e);

  if (RFsendtraj)
    {
      cout << "SENDING TRAJ" << endl;
      SendTraj(RFtrajSocket, RFtraj);
    }

  if (RFsendgui)
    {
      SendTraj(RFskynetguiSocket, RFtraj);
    }

  cout << "Current Location N:" << RFNorthing << " E:" << RFEasting <<endl;
  cout << "Destination Loca N:" << RFDestination[0] << " E:" << RFDestination[1] <<endl;

  delete RFtraj;
}



// Sends a road structure to fusion mapper.  The road is defined as follows:
//  x:  Meters in front of Alice, relative to the middle of the front axle.
//       In front is positive.
//  y:  Meters to the right of Alice (right hand rule, z-axis down)
//       To the right is positive
//  width:  Radius, in meters, surrounding the corresponding point above
//       (where the road is defined)
// Example:  x[0] = 0, y[0] = 0, width[0] = 5 corresponds to a road circle 5m in radius surrounding the front bumper
// size:  number of points passed
void RFCommunications::RFSendRoad(double x[], double y[], double width[], int size)
{
  // skynet message format:  int:size|size*sizeof(double)*3
  // {[number of road points == n], [n x-points, double], [n y-points, double], [n width values, double]}

  int sizeSendArray = 1 + 3 * size;

  if (sizeSendArray == 1)
    return;

  double sendArray[sizeSendArray];

  sendArray[0] = size;

  for (int i = 0; i < size; i++)
  {
    sendArray[i + 1] = x[i];
    sendArray[i + size + 1] = y[i];
    sendArray[i + 2 * size + 1] = width[i];
  }

  m_skynet.send_msg(rf_socket, (void*)sendArray, sizeof(sendArray), 0);
}
