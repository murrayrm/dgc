#ifndef GLOBALCONSTANTS_H
#define GLOBALCONSTANTS_H

/*! This file should contain all constants that are independent of which 
 ** platform the code is running on and independent of any specific code
 ** implementation. */

// Conversions for RDDF file
/* FYI: According to the Geodetic Glossary of the National Geodetic Survey
(page 82), a survey foot is a unit of length defined as 1/3 of a yard and
equal in the US, since 1866, to exactly 1200/3937 (0.304800609) of a
metre. The terms US foot and survey foot are used interchangeably in North
America.

The International foot is exactly 1/3 of the international yard, or exactly
0.3048 metres. The International foot was defined, for scientific purposes by
the 1959 agreement between the US NBS and similar organisations in other
countries.

US foot = 0.3048006096 metres (to ten decimal places) 
International foot = 0.3048 metres

WE ARE USING INTERNATIONAL UNITS
*/
#define METERS_PER_FOOT 0.3048
#define MPS_PER_MPH     0.44704

// RDDF utility constants
#define RDDF_FILE "rddf.dat"
#define LAST_WAYPOINT_SPEED 2.0

#warning "This depends on other constants in util/planner/*Defs.h"
#define LAST_WAYPOINT_DIST  39.5

// Mapper constants
//#define FUSIONMAPPER_NUMROWSCOLS 200
//#define FUSIONMAPPER_RESROWSCOLS 1.0 // [m]
#define FUSIONMAPPER_NUMROWSCOLS 400
#define FUSIONMAPPER_RESROWSCOLS 0.5 // [m]


#endif
