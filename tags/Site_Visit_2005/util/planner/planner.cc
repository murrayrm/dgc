#include "planner.h"
#include <iomanip>

extern "C" int sninit_(int *, int *, char *, int *, int *, int *, double *, int *, int);
extern "C" int snset_ (char *,int *, int *, int *, char *, int *, int *, int *, double *, int *, int, int);

#define USE_REFINEMENT

CPlanner::CPlanner(CMap *pMap,  int mapLayerID, RDDF* pRDDF, bool USE_MAXSPEED, bool bGetVProfile)
	: m_spacialStage   (pMap, mapLayerID, pRDDF, USE_MAXSPEED),
		m_refinementStage(pMap, mapLayerID, pRDDF, USE_MAXSPEED, bGetVProfile),
		m_interm("interm"),
		m_after("after")
{
#ifdef RTP_VPROFILE_DISPLAY
	m_rtpPipe.open("./rtp");
#endif

	m_interm << setprecision(20);
	m_after  << setprecision(20);

	int maxSNOPTm = (int)(100.0/0.2) + 10;
	int maxSNOPTn = 2*MAX_NUM_POINTS+1;

	m_SNOPTContext.lencw = 10000;
	m_SNOPTContext.leniw = 2000*(maxSNOPTm+maxSNOPTn);
	m_SNOPTContext.lenrw = 4000*(maxSNOPTm+maxSNOPTn);
	m_SNOPTContext.cw = new char[8*m_SNOPTContext.lencw];	
	m_SNOPTContext.iw = new int[m_SNOPTContext.leniw];
	m_SNOPTContext.rw = new double[m_SNOPTContext.lenrw];

	m_SNOPTContext2.lencw = 10000;
	m_SNOPTContext2.leniw = 2000*(maxSNOPTm+maxSNOPTn);
	m_SNOPTContext2.lenrw = 4000*(maxSNOPTm+maxSNOPTn);
	m_SNOPTContext2.cw = new char[8*m_SNOPTContext2.lencw];	
	m_SNOPTContext2.iw = new int[m_SNOPTContext2.leniw];
	m_SNOPTContext2.rw = new double[m_SNOPTContext2.lenrw];

#warning "see if contexts should be shared"
	m_spacialStage   .setContext(&m_SNOPTContext);
	m_refinementStage.setContext(&m_SNOPTContext2);
}

CPlanner::~CPlanner()
{
	delete m_SNOPTContext.cw;
	delete m_SNOPTContext.iw;
	delete m_SNOPTContext.rw;

	delete m_SNOPTContext2.cw;
	delete m_SNOPTContext2.iw;
	delete m_SNOPTContext2.rw;
}

int CPlanner::plan(VehicleState *pState)
{
	// copy the state so we can modify it if we need
	VehicleState stateCopy = *pState;

	cout << "CPlanner::plan(VehicleState *pState)" << endl;
	cout << '{';
 	cout << stateCopy.Northing << ", " << stateCopy.Vel_N << ", " << stateCopy.Acc_N << "," << endl;
 	cout << stateCopy.Easting  << ", " << stateCopy.Vel_E << ", " << stateCopy.Acc_E << "," << endl;
 	cout << stateCopy.Yaw << ", " << stateCopy.YawRate << "}" << endl;




#if 0
	double ststate[]=
{3834759.08956516, -0.799384642641056, -0.393576922009346,
442528.265039379, 0.234311262182145, 0.114672860827129,
2.85910613699925, -0.000624620176999015}
;
	stateCopy.Northing = ststate[0];
	stateCopy.Easting  = ststate[3];
	stateCopy.Vel_N = ststate[1];
	stateCopy.Vel_E = ststate[4];
	stateCopy.Acc_N = ststate[2];
	stateCopy.Acc_E = ststate[5];
	stateCopy.Yaw = atan2(stateCopy.Vel_E, stateCopy.Vel_N);
	stateCopy.YawRate = (stateCopy.Vel_N*stateCopy.Acc_E - stateCopy.Acc_N*stateCopy.Vel_E) / (stateCopy.Vel_N*stateCopy.Vel_N + stateCopy.Vel_E*stateCopy.Vel_E);
	stateCopy.Vel_D = 0.0;
#endif


	int res = m_spacialStage.run(&stateCopy, m_refinementStage.getSolverState());

// 	m_spacialStage.getTraj()->print(m_interm);
// 	m_interm << endl;
#ifdef USE_REFINEMENT
	cout << "intermediate stage: " << res << endl;

	// feasibility check. If it's infeasible set the error condition
	if(m_spacialStage.whereInfeasible() != -1)
		res = -1;

	if(res == 0 || res == 4 || res == 9)
	{
// 		ifstream instate("intermstate");
// 		double intermstate[6];
// 		instate.read((char*)intermstate, 48);
// 		res = m_refinementStage.run(&stateCopy, intermstate);

		res = m_refinementStage.run(&stateCopy, m_spacialStage.getSolverState());
	}
	else
		return res;

#else
	*m_refinementStage.getTraj() = *m_spacialStage.getTraj();
#endif	

// 	m_refinementStage.getTraj()->print(m_after);
// 	m_after << endl;

#ifdef RTP_VPROFILE_DISPLAY
	m_rtpPipe << "0" << endl;
	m_refinementStage.getTraj()->printSpeedProfile(m_rtpPipe);
#endif

	return res;
}

double CPlanner::getVProfile(CTraj* pTraj)
{
	m_refinementStage.m_bGetVProfile = true;

	*(m_refinementStage.getTraj()) = *pTraj;
	VehicleState vehstate(pTraj->getNorthingDiff(0, 0), pTraj->getNorthingDiff(0, 1), pTraj->getNorthingDiff(0, 2),
												pTraj->getEastingDiff (0, 0), pTraj->getEastingDiff (0, 1), pTraj->getEastingDiff (0, 2));
	m_refinementStage.run(&vehstate);

	m_refinementStage.m_bGetVProfile = false;

	return m_refinementStage.getObjective();
}

CTraj* CPlanner::getTraj(void)
{
	return m_refinementStage.getTraj();
}

CTraj* CPlanner::getSeedTraj(void)
{
	return m_spacialStage.getSeedTraj();
}

CTraj* CPlanner::getIntermTraj(void)
{
	return m_spacialStage.getTraj();
}

double CPlanner::getLength(void)
{
	return m_refinementStage.getLength();
}
