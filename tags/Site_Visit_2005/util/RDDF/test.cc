//#include <stdlib.h>
//#include <unistd.h>
//#include <ctype.h>
//#include <string.h>
//#include <iostream.h>
#include "rddf.hh"
//#include "../../MTA/Kernel/Misc/Time/Timeval.hh"

int main (int argc,char* argv[])
{
  RDDF rddf("rddf.dat");
  RDDFVector waypoints;

  waypoints = rddf.getTargetPoints();

  int num = rddf.getNumTargetPoints();
  printf("Number of waypoints = %d\n", num);
  
  for( int i=0; i < num; i++ )
  {
    waypoints[i].display();
  }

  return 0;
}
