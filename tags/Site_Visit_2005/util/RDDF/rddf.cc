/**********************************************************************
 **********************************************************************
 * +FILE:        rddf.cc
 *    
 * +DESCRIPTION: rddf class methods
 *
 * 
 **********************************************************************
 *********************************************************************/

#include "rddf.hh"

// Constructor
RDDF::RDDF()
{
  loadFile(RDDF_FILE);
}
RDDF::RDDF(char *pFileName)
{
  loadFile(pFileName);
}
// Destructor
RDDF::~RDDF()
{
}

RDDF &RDDF::operator=(const RDDF &other)
{
  numTargetPoints = other.getNumTargetPoints();
  targetPoints = other.getTargetPoints();
  return *this;
}

// API comments in header
int RDDF::loadFile(char* fileName)
{
  ifstream file;
  string line;
  RDDFData eachWaypoint;

  numTargetPoints = 0;
  
  file.open(fileName,ios::in);
  if(!file)
  {
    cout << "UNABLE TO OPEN THE FILE /team/RDDF/rddf.dat" << endl;
    cout << "PLEASE ADD IN YOUR MAKEFILE:" << endl;
    cout << "( ln -fs $(DGC)/RDDF/rddf.dat $(DGC)/YOUR_DIRECTORY/rddf.dat)" << endl;
    exit(1);
  }

  while(!file.eof())
  {
    GisCoordLatLon latlon;
    GisCoordUTM utm;

    // Number
    getline(file,line,RDDF_DELIMITER);
    eachWaypoint.number = atoi(line.c_str());
    // Latitude
    getline(file,line,RDDF_DELIMITER);
    latlon.latitude = atof(line.c_str());
    
    // Longitude
    getline(file,line,RDDF_DELIMITER);
    latlon.longitude = atof(line.c_str());

    // Converting to UTM
    if(!gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL))
    { 
      cerr << "Error converting coordinate " << eachWaypoint.number << " to utm\n";
      exit(1); 
    }
    eachWaypoint.Northing = utm.n;
    eachWaypoint.Easting = utm.e;

    // Boundary offset and radius
    getline(file,line,RDDF_DELIMITER);
    eachWaypoint.offset = atof(line.c_str())*METERS_PER_FOOT;
    eachWaypoint.radius = eachWaypoint.offset;
    // Speed limit
    getline(file,line,RDDF_DELIMITER);
    eachWaypoint.maxSpeed = atof(line.c_str())*MPS_PER_MPH;
    // Hour
    getline(file,line,RDDF_DELIMITER);
    if(line.find("#") != string::npos)
    {
      eachWaypoint.hour = 0;
    }
    else
    {
      eachWaypoint.hour = atoi(line.c_str());
    }
    // Minute
    getline(file,line,RDDF_DELIMITER);
    if(line.find("#") != string::npos)
    {
      eachWaypoint.min = 0;
    }
    else
    {
      eachWaypoint.min = atoi(line.c_str());
    }
    // Second
    file >> line;
    if(line.find("#") != string::npos)
    {
      eachWaypoint.sec = 0;
    }
    else
    {
      eachWaypoint.sec = atoi(line.c_str());
    }

    if(numTargetPoints <= eachWaypoint.number)
    {
      targetPoints.push_back(eachWaypoint);
      numTargetPoints++;
    }
    else
    {
      break;
    }
  }

#ifdef TACK_ONE_ON
  eachWaypoint = targetPoints[numTargetPoints-1];

  double yaw = getTrackLineYaw(numTargetPoints-2);
  double dist = LAST_WAYPOINT_DIST;

  // Set the new waypoint to be far away and have zero speed
  eachWaypoint.Northing = eachWaypoint.Northing + dist*cos(yaw);
  eachWaypoint.Easting = eachWaypoint.Easting + dist*sin(yaw);

  // Set the speed to be something small 
  targetPoints[numTargetPoints-1].maxSpeed = LAST_WAYPOINT_SPEED;
  eachWaypoint.maxSpeed = LAST_WAYPOINT_SPEED;
  eachWaypoint.number = eachWaypoint.number+1;

  // Tack on this additional waypoint
  targetPoints.push_back(eachWaypoint);
  numTargetPoints++;
#endif

  // debug
  // cout << "RDDF: " << numTargetPoints << " target points read !!!" << endl;
  return(0);
}

// API comments in header file.
double RDDF::getTrackLineYaw( int i )
{
  // Return the Yaw of waypoint i's trackline.
  return atan2( targetPoints[i+1].Easting - targetPoints[i].Easting, 
                targetPoints[i+1].Northing - targetPoints[i].Northing );
}

// API comments in header file.
double RDDF::getTrackLineDist( int i )
{
  // Return the distance between waypoints i and i+1
  return sqrt( pow(targetPoints[i+1].Easting - targetPoints[i].Easting, 2) + 
               pow(targetPoints[i+1].Northing - targetPoints[i].Northing, 2) );
}

// API comments in header file.
NEcoord RDDF::getPointAlongTrackLine( double n, double e, double distance,
  double* thetaAtTarget, double* corRad)
{
	NEcoord pos(n,e);
	return getPointAlongTrackLine(pos, distance, thetaAtTarget);
}

NEcoord RDDF::getPointAlongTrackLine( NEcoord& pos, double distance,
  double* thetaAtTarget, double* corRad)
{
  // i is the index of the corridor segment we are in
  int i = getCurrentWaypointNumber( pos.N, pos.E );

  double remaining = distance; // remaining distance
  double frac; // fraction to next waypoint

  // we either have a projection to the trackline segment or we don't
  // this calculates the normalized distance from waypoint i to i+1 of the 
  // projection to the trackline.  0 <= frac <= 1 from i to i+1.
  frac = ( (pos - targetPoints[i].ne_coord()) * 
    (targetPoints[i+1].ne_coord() - targetPoints[i].ne_coord()) )
    / pow(getTrackLineDist(i), 2);
  // saturate frac to be constrained to the line segment
  //frac = fmax( fmin( frac, 1.0 ), 0.0 );

  // Now, from the start coordinate, move along the trackline the specified 
  // distance (initialized by stepping back to previous waypoint and adding 
  // this distance to the remaining).
  remaining += frac * getTrackLineDist(i);
  double dist = getTrackLineDist(i);
  
  while( remaining > dist ) 
  {
    // advance to the next waypoint
    remaining -= dist;
    i++;
    dist = getTrackLineDist(i);
  }

	// targetCorridor is the vector pointing from the start to the end of the
	// corridor our final point will be in
	NEcoord targetCorridor = targetPoints[i+1].ne_coord() - targetPoints[i].ne_coord();

	// if we're asking for the target orientation to be computed, fill it in with
	// the argument of the targetCorridor
	if(thetaAtTarget != NULL)
		*thetaAtTarget = atan2(targetCorridor.E, targetCorridor.N);

  if(corRad != NULL) *corRad = targetPoints[i].radius;

	return targetPoints[i].ne_coord() + 
    targetCorridor * remaining * (1.0 / getTrackLineDist(i));
}


// API comments in header
int RDDF::getCurrentWaypointNumber( NEcoord& pos )
{
  return getCurrentWaypointNumber( pos.N, pos.E );
}


// API comments in header
int RDDF::getCurrentWaypointNumber( double Northing, double Easting )
{
  // set the current position vector
  NEcoord curPos( Northing, Easting );
  // declare waypoint vectors
  NEcoord curWay, nextWay;

  // minimum found distance to waypoint
  double minDist = 123456789;
  int minDistWay = 0;
  
  // cycle through all of the waypoints
  for( int i = 0; i < numTargetPoints-1; i++ ) 
  {
    curWay.N = targetPoints[i].Northing;
    curWay.E = targetPoints[i].Easting;
    nextWay.N = targetPoints[i+1].Northing;
    nextWay.E = targetPoints[i+1].Easting;
 
    // check to see if we are within the corridor segment
    // if we are, just return with the index of the segment
    //if( withinCorridor( curPos, curWay, nextWay, targetPoints[i].radius ) ) 
    if( isPointInCorridor( i, curPos.N, curPos.E ) )
    {
      //printf("------ I am within corridor segment %d -------\n", i ); 
      return i;
    }

    // if this waypoint is closer than any other we've found,
    // then remember so 
    if( (curPos - curWay).norm() < minDist ) 
    {
      minDist = (curPos - curWay).norm(); 
      minDistWay = i;
    }
  } 
  //printf("------ I am NOT within the corridor -------\n" ); 
  
  // if we get to this point in the function, it means that we are
  // **not** within the corridor.  In this case, return the index of
  // the waypoint that is closest to us
  return minDistWay;
}


// API comments in header
int RDDF::getNextWaypointNumber( NEcoord& pos )
{
  return getCurrentWaypointNumber( pos.N, pos.E ) + 1;
}


// API comments in header
int RDDF::getNextWaypointNumber( double Northing, double Easting )
{
  return getCurrentWaypointNumber( Northing, Easting ) + 1;
}


int RDDF::createBobFile(char* fileName)
{
  ofstream file;
  int ind=0;
  file.open(fileName);
  if(!file)
  {
    cout << "UNABLE TO OPEN THE FILE" << endl;
    return(BAD_BOB_FILE);
  }
  file << "# This is a Bob format waypoint specification file.\n";
  file << "# The column format here is: \n";
  file << "# 1. character [ignored]\n";
  file << "# 2. Easting[m]\n";
  file << "# 3. Northing[m] \n";
  file << "# 4. lateral_offset[m] \n";
  file << "# 5. speed_limit [m/s] \n";
  file << "# NOTE THAT THESE ARE IN SI AS OPPOSED TO THE RDDF SPEC.\n#\n";
  for(ind=0;ind < numTargetPoints;ind++)
  {

    file << "0   " << targetPoints[ind].Northing << " " << targetPoints[ind].Easting 
         << " " << targetPoints[ind].offset << " " << targetPoints[ind].maxSpeed << "\n"; 
  }
  return(0);
}


int RDDF::isPointInCorridor(int waypointNum, double Northing, double Easting) 
{
  if(waypointNum<0 || waypointNum>=(numTargetPoints-1)) {
    return 0;
  }

  NEcoord cur_pos(Northing, Easting);
  NEcoord cur_way(targetPoints[waypointNum].Northing, 
                  targetPoints[waypointNum].Easting);
  NEcoord next_way(targetPoints[waypointNum+1].Northing, 
                   targetPoints[waypointNum+1].Easting);
  double next_corr_rad = targetPoints[waypointNum].radius;


  // Check if within the circular part of the corridor    
  // Within the circle around the current waypoint
  // -or- within the circle around the next waypoint
  if ((cur_pos - cur_way).norm() < next_corr_rad ||
      (cur_pos - next_way).norm() < next_corr_rad) 
  {
    return 1;

    //   cout << "~~~~~~~~~WITHIN THE CIRCULAR PART~~~~~~~~~" << endl;
  }

  // Check if within the rectangular part of the corridor
  // We consider the four vertices of the rectangle
  // v[1]------------------------v[2]               
  // |                              |                 
  // * <-- cur_way                  * <-- next_way  
  // |                              |               
  // v[0]------------------------v[3]               
    
  // v is an array for the vertices of the rectangle
  NEcoord v[4];     
  // way2way is the vector from the current to next waypoint
  NEcoord way2way = next_way - cur_way;
  // offset is the vector perpendicular to way2way, rotated 
  // counter-clockwise by pi/2
  NEcoord offset(way2way.E, -way2way.N);
  // Now scale the offset to be the vector from the waypoint to the
  // vertex of the rectangle on the waypoint radius
  // TODO: This will break if the successive waypoints are the same!
  offset = offset * (next_corr_rad / offset.norm());

  // assign the vectors from the waypoints to the corresponding vertices
  v[0] = cur_way - offset;
  v[1] = cur_way + offset;
  v[2] = next_way + offset;
  v[3] = next_way - offset;

  // Now find the angles formed by the lines from the vehicle position
  // to adjacent vertices vpv = "vertex-position-vertex"
  double vpv[4];          
  
  for (int i = 0; i < 4; i++) 
  {
    vpv[i] = atan2((v[i%4] - cur_pos).N, (v[i%4] - cur_pos).E) - 
             atan2((v[(i+1)%4] - cur_pos).N, (v[(i+1)%4] - cur_pos).E);
    // normalize and get the acute angle absolute value
    vpv[i] = atan2( sin(vpv[i]), cos(vpv[i]) ); 
    vpv[i] = fabs( vpv[i] );
  }
  // check to see if less than some very small number to get around
  // floating-point precision concerns about this calculation
  if (fabs((2*M_PI - (vpv[0] + vpv[1] + vpv[2] + vpv[3]))) < 0.0001)
  {
    return 1;

    //    cout << "$$$$$$$$$WITHIN THE RECTANGLE$$$$$$$$$" << endl;
  }
  
  return 0;
}


int RDDF::getCorridorSegmentBoundingBoxUTM(int waypointNum, 
					   double winBottomLeftUTMNorthing, double winBottomLeftUTMEasting,
					   double winTopRightUTMNorthing, double winTopRightUTMEasting,
					   double& BottomLeftUTMNorthing, double& BottomLeftUTMEasting, 
					   double& TopRightUTMNorthing, double& TopRightUTMEasting) 
{
  //int corType;
  int bottomOut = 0;
  int topOut = 0;
  double firstRadius, secondRadius, radius;
  firstRadius = targetPoints[waypointNum].radius;
  secondRadius = targetPoints[waypointNum+1].radius;
  radius = max(firstRadius, secondRadius);
  
  BottomLeftUTMNorthing = min(targetPoints[waypointNum].Northing-radius,
			      targetPoints[waypointNum+1].Northing-radius);
  TopRightUTMNorthing = max(targetPoints[waypointNum].Northing+radius,
			    targetPoints[waypointNum+1].Northing+radius);
  BottomLeftUTMEasting = min(targetPoints[waypointNum].Easting-radius,
			      targetPoints[waypointNum+1].Easting-radius);
  TopRightUTMEasting = max(targetPoints[waypointNum].Easting+radius,
			    targetPoints[waypointNum+1].Easting+radius);


  /*
  if((targetPoints[waypointNum].Northing - radius) <
     (targetPoints[waypointNum+1].Northing + radius))
  {
    printf("%s [%d]: \n", __FILE__, __LINE__);
    BottomLeftUTMNorthing = targetPoints[waypointNum].Northing - radius;
    TopRightUTMNorthing = targetPoints[waypointNum+1].Northing + radius;
    //printf("n=%lf, r=%lf\n", targetPoints[waypointNum+1].Northing, targetPoints[waypointNum+1].radius);
  } 
  else 
  {
    printf("%s [%d]: \n", __FILE__, __LINE__);
    BottomLeftUTMNorthing = targetPoints[waypointNum+1].Northing - radius;
    TopRightUTMNorthing = targetPoints[waypointNum].Northing + radius;
    //BottomLeftUTMNorthing = targetPoints[waypointNum+1].Northing - targetPoints[waypointNum+1].radius;
    //TopRightUTMNorthing = targetPoints[waypointNum].Northing + targetPoints[waypointNum].radius;
  }

  if((targetPoints[waypointNum].Easting - radius) <
     (targetPoints[waypointNum+1].Easting + radius))
    //  if((targetPoints[waypointNum].Easting - targetPoints[waypointNum].radius) <
    //(targetPoints[waypointNum+1].Easting + targetPoints[waypointNum+1].radius)) 
  {
     printf("%s [%d]: \n", __FILE__, __LINE__);
     BottomLeftUTMEasting = targetPoints[waypointNum].Easting - radius;
     TopRightUTMEasting = targetPoints[waypointNum+1].Easting + radius;
     //BottomLeftUTMEasting = targetPoints[waypointNum].Easting - targetPoints[waypointNum].radius;
   //TopRightUTMEasting = targetPoints[waypointNum+1].Easting + targetPoints[waypointNum+1].radius;
  } 
  else 
  {
    printf("%s [%d]: \n", __FILE__, __LINE__);
    BottomLeftUTMEasting = targetPoints[waypointNum+1].Easting - radius;
    TopRightUTMEasting = targetPoints[waypointNum].Easting + radius;
    //BottomLeftUTMEasting = targetPoints[waypointNum+1].Easting - targetPoints[waypointNum+1].radius;
    //TopRightUTMEasting = targetPoints[waypointNum].Easting + targetPoints[waypointNum].radius;
  }
  */
  if(BottomLeftUTMNorthing < winBottomLeftUTMNorthing) 
  {
    //printf("%s [%d]: \n", __FILE__, __LINE__);
    BottomLeftUTMNorthing = winBottomLeftUTMNorthing;
  }
  if(BottomLeftUTMEasting < winBottomLeftUTMEasting) 
  {
    //printf("%s [%d]: \n", __FILE__, __LINE__);
    BottomLeftUTMEasting = winBottomLeftUTMEasting;
  }
  if(TopRightUTMNorthing > winTopRightUTMNorthing) 
  {
    //printf("%s [%d]: \n", __FILE__, __LINE__);
    TopRightUTMNorthing = winTopRightUTMNorthing;
  }
  if(TopRightUTMEasting > winTopRightUTMEasting) 
  {
    //printf("%s [%d]: \n", __FILE__, __LINE__);
    TopRightUTMEasting = winTopRightUTMEasting;
  }

  if(BottomLeftUTMNorthing > winTopRightUTMNorthing ||
     BottomLeftUTMEasting > winTopRightUTMEasting ||
     TopRightUTMNorthing < winBottomLeftUTMNorthing ||
     TopRightUTMEasting < winBottomLeftUTMEasting) 
  {
    return -1;
  }

  if(targetPoints[waypointNum+1].Northing - radius> winTopRightUTMNorthing ||
     targetPoints[waypointNum+1].Northing + radius< winBottomLeftUTMNorthing || 
     targetPoints[waypointNum+1].Easting - radius > winTopRightUTMEasting ||
     targetPoints[waypointNum+1].Easting + radius < winBottomLeftUTMEasting) 
    /*
  if(targetPoints[waypointNum+1].Northing - targetPoints[waypointNum+1].radius> winTopRightUTMNorthing ||
     targetPoints[waypointNum+1].Northing + targetPoints[waypointNum+1].radius< winBottomLeftUTMNorthing || 
     targetPoints[waypointNum+1].Easting - targetPoints[waypointNum+1].radius > winTopRightUTMEasting ||
     targetPoints[waypointNum+1].Easting + targetPoints[waypointNum+1].radius < winBottomLeftUTMEasting) 
    */
  {
    topOut=1;
  }

  if(targetPoints[waypointNum].Northing - targetPoints[waypointNum].radius> winTopRightUTMNorthing ||
     targetPoints[waypointNum].Northing + targetPoints[waypointNum].radius< winBottomLeftUTMNorthing || 
     targetPoints[waypointNum].Easting - targetPoints[waypointNum].radius > winTopRightUTMEasting ||
     targetPoints[waypointNum].Easting + targetPoints[waypointNum].radius < winBottomLeftUTMEasting) 
    /*
  if(targetPoints[waypointNum].Northing - targetPoints[waypointNum].radius> winTopRightUTMNorthing ||
     targetPoints[waypointNum].Northing + targetPoints[waypointNum].radius< winBottomLeftUTMNorthing || 
     targetPoints[waypointNum].Easting - targetPoints[waypointNum].radius > winTopRightUTMEasting ||
     targetPoints[waypointNum].Easting + targetPoints[waypointNum].radius < winBottomLeftUTMEasting) 
    */
  {
    bottomOut = 1;
  }

  if(bottomOut == 1 && topOut == 1) return 3;
  if(bottomOut == 1 && topOut == 0) return 2;
  if(bottomOut == 0 && topOut == 1) return 1;

  return 0;
}


int RDDF::getWaypointNumAheadDist(double UTMNorthing, double UTMEasting, double distance) 
{
  int seed = getCurrentWaypointNumber(UTMNorthing, UTMEasting);
  NEcoord curPos(UTMNorthing, UTMEasting);
  NEcoord wayPos(targetPoints[seed].Northing, targetPoints[seed].Easting);
  if((curPos - wayPos).norm() > distance) 
  {
    return (seed+1 > numTargetPoints-1) ? numTargetPoints-1 : seed + 1;
  }
  for(int i=seed; i<numTargetPoints-1; i++) 
  {
    wayPos.N = targetPoints[i].Northing;
    wayPos.E = targetPoints[i].Easting;
    if((curPos - wayPos).norm() > distance) 
    {
      return i;
    }
  }

  return numTargetPoints-1;
}


int RDDF::getWaypointNumBehindDist(double UTMNorthing, double UTMEasting, double distance) 
{
  int seed = getCurrentWaypointNumber(UTMNorthing, UTMEasting);
  NEcoord curPos(UTMNorthing, UTMEasting);
  NEcoord wayPos(targetPoints[seed].Northing, targetPoints[seed].Easting);
  if((curPos - wayPos).norm() > distance) return (seed-1 < 0) ? 0 : seed-1;

  for(int i=seed; i>0; i--) 
  {
    wayPos.N = targetPoints[i].Northing;
    wayPos.E = targetPoints[i].Easting;
    if((curPos - wayPos).norm() > distance) 
    {
      return i;
    }
  }

  return 0;
}


double RDDF::getDistToTrackline(NEcoord pointCoords) 
{
  //Get the nearest waypoint
  int wayptNum = getCurrentWaypointNumber(pointCoords.N, pointCoords.E);

  //We're making a right-triangle - we want to find one of the legs
  //So first we find the length of the other leg
  double distAlongTrackline = pow((NEcoord(pointCoords.N, pointCoords.E) - targetPoints[wayptNum].ne_coord()) * 
    (targetPoints[wayptNum+1].ne_coord() - targetPoints[wayptNum].ne_coord()), 2)/pow(getTrackLineDist(wayptNum), 2);
  //Then the length of the hypoteneuse
  double hypoteneuse = (NEcoord(pointCoords.N, pointCoords.E) - targetPoints[wayptNum].ne_coord()) * 
    (NEcoord(pointCoords.N, pointCoords.E) - targetPoints[wayptNum].ne_coord());

  //And return the result of the pythagorean theorem
  return sqrt(hypoteneuse - distAlongTrackline);
  //DO NOT COMMIT YET - FINISH TESTING
}

double RDDF::getRatioToTrackline(NEcoord pointCoords) 
{
  //Get the distance to 
  double dist = getDistToTrackline(pointCoords);

  int wayptNum = getCurrentWaypointNumber(pointCoords.N, pointCoords.E);
 
  return dist/targetPoints[wayptNum].offset;
}
