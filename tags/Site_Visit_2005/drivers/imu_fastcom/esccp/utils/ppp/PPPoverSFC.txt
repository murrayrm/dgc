Connecting the SuperFastcom up to an IP network using PPP.

Prereqs:

Compile PPP support into the kernel (or as modules), select (at a minimum?)

'PPP (point-to-point protocol) Support'
'PPP Support for async serial ports'
'PPP BSD Deflate compression'
'PPP Deflate compression'

the resulting modules that are needed are:

slhc
ppp_generic
ppp_async
bsd_comp
ppp_deflate

with this in place, you can build the escc  module:
uncompress the escc sources:

tar -xzvf esccp_4_2_2004.tar.gz

change into the module source directory:

cd esccp

create the esccp.o module:

make

load the esccp.o module:

insmod esccp.o

create the device nodes for the superfastcom:

mknod /dev/escc0 c 254 0
mknod /dev/escc1 c 254 1

(if you need to chmod these to acceptable permissions, all of this was done assuming you are root, but it probably isn't entirely necessary)

change into the example code directory:

cd utils

build the examples:

make
make install

Configure the card/port:
cd bin
./esccclock 0 1000000
./esccpsettings 0 hdlcset


Execute the pty wedge program:

./pty /dev/escc0

This will return something like '/dev/pts/0', substitute the result into the following line for '/dev/pts/x'
(also substitute any valid parameters to pppd here including the IP's that you really want)

pppd -detach local 192.168.2.50:192.168.2.51 /dev/pts/x &

(do the same thing on the other side swapping the ip's and wait for IP)
ifconfig should show ppp0 with IP's
ping 192.168.2.51 should now work, as should any other IP related utility (ie setup routing, gateways etc, etc)

this was tested with a stock redhat 9.0 install.


