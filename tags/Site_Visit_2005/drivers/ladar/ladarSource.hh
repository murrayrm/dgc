#ifndef __LADARSOURCE_H__
#define __LADARSOURCE_H__


//System Include Files
#include <iostream>
#include <fstream>
#include <sys/stat.h>

//DGC Include Files
#include <laserdevice.h>
#include <frames/frames.hh>
#include <VehicleState.hh>
#include <DGCutils>

#ifdef _PLAYER_
#include "playerclient.h"
#include "LadarClient.hh"

// Player/Gazebo stuff
/*! the name of the config file */
#define CONFIG_FILENAME "config"
/*! the host to connect to the Player Server at (if no config file found) */
#define PLAYER_GAZEBO_HOST "localhost"
/*! the port to connect to the Player Server at (if no config file found) */
#define PLAYER_GAZEBO_PORT 6665
#endif

#define MAX_SCAN_POINTS 1000

using namespace std;

struct ladarSourceLoggingOptions {
  int logRaw;
  int logState;
  int logRanges;
  int logRelative;
  int logUTM;
};

/*---------------------------------------------------------------------------*/
/**
  @brief A LADAR driver class

  ladarSource is the LADAR equivalent of stereoSource, and is a replacement for
  the myriad LADAR drivers that existed before.  (Actually, it technically
  combines them into a single driver.)  The basic idea with ladarSource is that
  when you initialize it, you tell it to read from file, simulation, or an
  actual ladar.  You also give it a path to a configuration file, which tells
  ladarSource what type of LADAR you're using (SICK, Riegl, etc.) as well as the
  physical constants associated with the LADAR (XYZ, RPY, scan resolution, etc).

  Then, all you have to do is scan, update the LADAR's state (so it can
  transform the scan points into UTM coordinates), and tell the ladarSource to
  log if you like.

*/
/*---------------------------------------------------------------------------*/
class ladarSource {
public:
  //! A basic constructor, initializes variables to 0 and such
  ladarSource();
  //! A basic destructor - doesn't do any cleanup yet!
  ~ladarSource();
  
  /*-------------------------------------------------------------------------*/
  /**
     @brief   Initialize the ladarSource
     @param   sourceType       Use files, sim, or actual ladar
     @param   ladarIDFilename  Path/filename to the config file for the ladar
     @param   logFilename      Prefix for log files (optional)
     @param   loggingEnabled   Flag to disable logging (optional)
     @return  Standard ladarSource status code

     This initializes the ladarSource object with the given parameters.  For
     example, to create a ladarSource object that reads from files, you would
     do:
     
     @code
     myLadar.init(SOURCE_FILES, "config/Ladar.ini.roof", "prefix_to_read_from", 0);
     @code

     init should only be called once per object.  If it is called more than once
     on a ladarSource object, further behavior of that object is undefined.

  */
  /*-------------------------------------------------------------------------*/
  int init(int sourceType, char *ladarIDFilename, char *logFilename = "", int loggingEnabled = 1, int optladar=0);

  //! Returns the XYZ coordinates of the LADAR relative to vehicle origin
  XYZcoord getLadarPosition() {return _ladarPosOffset;};

  //! Returns the RPY angle of the LADAR relative to the vehicle frame
  RPYangle getLadarAngle() {return _ladarAngleOffset;};
  
  /*-------------------------------------------------------------------------*/
  /**
     @brief   Sets the ladar's position and angle for coord transforms
     @param   ladarPosOffset    XYZ of the LADAR wrt the vehicle origin
     @param   ladarAngleOffset  RPY of the LADAR wrt the vehicle frame
     @return  Standard ladarSource status code

     This sets the XYZ and RPY of the LADAR with respect to the vehicle frame.
     This overrides what the ladarSource object read from the configuration file
     passed to it on initialization.

  */
  /*-------------------------------------------------------------------------*/
  int setLadarFrame(XYZcoord ladarPosOffset, RPYangle ladarAngleOffset);

  int stop();


  /*-------------------------------------------------------------------------*/
  /**
     @brief   Tells the ladarSource object to scan
     @return  Local timestamp of when the scan took place

     This performs a scan, reading and processing the raw data so it is ready
     for use, and returns a timestamp of the local time of the computer as close
     to the time when the actual scan was taken as possible.

  */
  /*-------------------------------------------------------------------------*/
  unsigned long long scan();
  int scan(char *logFilename, int num);
  
  ladarSourceLoggingOptions getLoggingOptions() {return _loggingOpts;};
  char* getLogFilename() {return _currentFilename;};
  int startLogging(ladarSourceLoggingOptions loggingOpts, char *baseFilename="");
  int toggleLogging();
  int stopLogging();

  int currentSourceType() {return _currentSourceType;};
  int scanIndex() {return _scanIndex;};

  int resetScanIndex();

  int updateState(VehicleState state);

  //! Returns the number of valid points in the latest scan
  int numPoints() {return _numPoints;};

  //! Returns the range (in meters) of the ith point of the latest scan
  double range(int i);

  //! Returns the angle (in radians) of the ith point of the latest scan
  double angle(int i);

  //! Returns the XYZ coords (relative to LADAR) of the ith point of the latest scan
  XYZcoord ladarPoint(int i);

  //! Returns the XYZ coords (in UTM) of the ith point of the latest scan
  XYZcoord UTMPoint(int i);
  VehicleState currentState() {return _currentState;};

  char* getErrorMessage() {return _errorMessage;};

  double getResolutionRads() {return _resolution*M_PI/180.0;};

  double getCenterDist();

  enum {
    OK,
    UNKNOWN_SOURCE,
    NO_FILE,
    UNKNOWN_ERROR,
    NOT_IMPLEMENTED,    

    ERROR_COUNT
  };

  enum {
    SOURCE_UNASSIGNED,
    SOURCE_LADAR,
    SOURCE_FILES,
    SOURCE_SIM,
    
    SOURCE_UNKNOWN
  };

  enum {
    TYPE_SICK_LMS_221,

    TYPE_UNKNOWN
  };

private:
  int parseConfigFile(char* configFilename);
  int getLadarType(char* ladarString);
  char* getLadarType(int ladarInt);

  
  ladarSourceLoggingOptions _loggingOpts;

  frames _ladarFrame;
  XYZcoord _ladarPosOffset;
  RPYangle _ladarAngleOffset;
  
  VehicleState _currentState;

  //Actual Ladars
  CLaserDevice _sickLadar;
  
#ifdef _PLAYER_
  // Simulation Members
  PlayerClient *alice;
  // host running the player server
  string host;
  // port running the player server
  int port;

  // Simulation Ladars
  LadarClient _gazeboLadar;

  // End Simulation Members
#endif

  char _currentFilename[256];
  char _configFilename[256];
  FILE* _logFileRaw;  
  FILE* _logFileState;  
  FILE* _logFileRelative;  
  FILE* _logFileRanges;  
  FILE* _logFileUTM;  

  int _loggingEnabled;
  int _loggingPaused;

  int _currentSourceType;
  int _currentLadarType;
  int _numPoints;
  int _scanIndex;
  double _ranges[MAX_SCAN_POINTS];
  double _angles[MAX_SCAN_POINTS];

  unsigned long long _latestScanTimestamp;

  int _scanAngle;
  double _resolution;
  double _ladarUnits;
  double _scanAngleOffset;
  char _serialPort[256];


  char _errorMessage[1024];
};

#endif  //__LADARSOURCE_H__
