//#include "buildStartTab.hh"

#include <iostream>
#include <stdio.h>
#include <string>
#include <fstream>

/* GTK stuff */
#include <gtkmm.h>

class ExampleWindow : public Gtk::Window
{
public:
  ExampleWindow();
  virtual ~ExampleWindow();

protected:

  //Child widgets:
  Gtk::HBox m_HBox;
  Gtk::VBox m_VBox, m_VBox2;
  Gtk::Frame m_Frame_Normal, m_Frame_Multi, m_Frame_Left, m_Frame_Right,
    m_Frame_LineWrapped, m_Frame_FilledWrapped, m_Frame_Underlined;
  Gtk::Label m_Label_Normal, m_Label_Multi, m_Label_Left, m_Label_Right,
    m_Label_LineWrapped, m_Label_FilledWrapped, m_Label_Underlined;
};

ExampleWindow::ExampleWindow()
  :
  m_HBox(false, 5),
  m_VBox(false, 5),
  m_Frame_Normal("Normal Label"),
  m_Frame_Multi("Multi-line Label"),
  m_Frame_Left("Left Justified Label"),
  m_Frame_Right("Right Justified Label"),
  m_Frame_LineWrapped("Line wrapped label"),
  m_Frame_FilledWrapped("Filled, wrapped label"),
  m_Frame_Underlined("Underlined label"),
  m_Label_Normal("_This is a Normal label", true),
  m_Label_Multi("This is a Multi-line label.\nSecond line\nThird line"),
  m_Label_Left("This is a Left-Justified\nMulti-line label.\nThird line"),
  m_Label_Right("This is a Right-Justified\nMulti-line label.\nFourth line, (j/k)"),
  m_Label_Underlined("This label is underlined!\nThis one is underlined in quite a funky fashion")
{
  set_title("Label");
  set_border_width(5);

  add(m_HBox);

  m_HBox.pack_start(m_VBox, Gtk::PACK_SHRINK);

  m_Frame_Normal.add(m_Label_Normal);
  m_VBox.pack_start(m_Frame_Normal, Gtk::PACK_SHRINK);

  m_Frame_Multi.add(m_Label_Multi);
  m_VBox.pack_start(m_Frame_Multi, Gtk::PACK_SHRINK);

  m_Label_Left.set_justify(Gtk::JUSTIFY_LEFT);
  m_Frame_Left.add(m_Label_Left);
  m_VBox.pack_start(m_Frame_Left, Gtk::PACK_SHRINK);

  m_Label_Right.set_justify(Gtk::JUSTIFY_RIGHT);
  m_Frame_Right.add(m_Label_Right);
  m_VBox.pack_start(m_Frame_Right, Gtk::PACK_SHRINK);

  m_HBox.pack_start(m_VBox2, Gtk::PACK_SHRINK);

  m_Label_LineWrapped.set_text("This is an example of a line-wrapped label.  It " \
			       "should not be taking up the entire             " /* big space to test spacing */\
    "width allocated to it, but automatically " \
    "wraps the words to fit.  " \
    "The time has come, for all good men, to come to " \
    "the aid of their party.  " \
    "The sixth sheik's six sheep's sick.\n" \
    "     It supports multiple paragraphs correctly, " \
    "and  correctly   adds "\
			       "many          extra  spaces. ");
  m_Label_LineWrapped.set_line_wrap();
  m_Frame_LineWrapped.add(m_Label_LineWrapped);
  m_VBox2.pack_start(m_Frame_LineWrapped, Gtk::PACK_SHRINK);

  m_Label_FilledWrapped.set_text("This is an example of a line-wrapped, filled label.  " \
    "It should be taking " \
    "up the entire              width allocated to it.  " \
    "Here is a sentence to prove "\
    "my point.  Here is another sentence. "\
    "Here comes the sun, do de do de do.\n"\
    "    This is a new paragraph.\n"\
    "    This is another newer, longer, better " \
    "paragraph.  It is coming to an end, "\
				 "unfortunately.");
  m_Label_FilledWrapped.set_justify(Gtk::JUSTIFY_FILL);
  m_Label_FilledWrapped.set_line_wrap();
  m_Frame_FilledWrapped.add(m_Label_FilledWrapped);
  m_VBox2.pack_start(m_Frame_FilledWrapped, Gtk::PACK_SHRINK);

  m_Label_Underlined.set_justify(Gtk::JUSTIFY_LEFT);
  m_Label_Underlined.set_pattern ("_________________________ _ _________ _ ______     __ _______ ___");
  m_Frame_Underlined.add(m_Label_Underlined);
  m_VBox2.pack_start(m_Frame_Underlined, Gtk::PACK_SHRINK);

  show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

/*end GTK stuff */


//#include "StartTab.hh"
using namespace std;

#define debug

//line format definitions for module starter config file
static string _module = "Module:";

void readFile(string fileName)
{
  // read in config file passed in as param
  // format of config file will be 
  // first line of config file is a number 
  // specifying the number of modules 
  // read that and call the function  
  // setMaxModule(int n)
  // Moule:snModName 0/1 
  // snModName is the synet module name
  // 0/1 - means either a 0 or a 1 
  // where 1 means start it up automatically and
  // 0 means show it with a non-checked checkbox 
  // but don't start it up 
  // for each line in the config file call the function
  // addModule(String snModName, bool start)

  //char buffer[256];
  //int numLines = 0;
  string snModName, line;
  unsigned int pos, i;
  bool start;

  ifstream configfile(fileName.c_str() );
    if (! configfile.is_open())
      { cout << "Error opening file"; exit (1); }
    while (! configfile.eof() )
      {
	//cout << "\nreading line..." << endl;
	getline(configfile, line);
	if(! line.empty() )
	  if(line[0] != '#') // # at beginning of line denotes comment
	    {	    
	      //parse string
	      if( (pos = line.find(" ")) != string::npos)
		{
		  if(line.substr(0,pos) == _module)
		    {
		      //module listing
		      line.erase(0,pos);
		      for(i = 0;line[i] == ' ';++i);//elim whitepsace
		      line.erase(0,i);
		      //cout << "module listing length: " << pos << endl;
		      //cout << "line is now: " << line << endl;
		      if( (pos = line.find(" ")) != string::npos)
			{
			  snModName = line.substr(0,pos); //module name
			  line.erase(0,pos);
			  for(i = 0;line[i] == ' ';++i);//elim whitepsace
			  line.erase(0,i);
			  //cout << "module name length: " << pos << endl;
			  //cout << "line is now: " << line << endl;
			  switch (line[0])
			    {
			    case '0':
			      start = 0;
			      break;
			    case '1':
			      start = 1;
			      break;
			    default:
			      //cout << "invalid boolean specifier: " << line[0] <<  endl;
			      //cout << "in line: " << line << endl;
			      exit (1);
			    }
			}
		    }
		}
	    }
      }
    configfile.close();
    //addModule(snModName, start); //add module to skynetGUI
}


void updateConfig(const string fileName, const string snModName, const bool start)
{
  // updates the config file start-up value
  string s;  //input string read from file
  char start_char = (start ? 1 : 0) + '0';
  fstream configfile(fileName.c_str(), fstream::in | fstream::out);

  if (! configfile.is_open() )
    { cout << "Error opening file"; exit (1); }
  while (! configfile.eof() )
    {
      configfile >> s;
      if(s[0] != '#')
	{
	  if(s == _module) // module listing
	    {
	      configfile >> s;
	      if (s == snModName)
		{
		  cout << "module name: " << s << endl;
		  cout << "snModName: " << snModName << endl;
		  configfile.get();
		  configfile.put(start_char);
		}
	      else
		{
		  getline(configfile,s);
		  cout << "Instead of module name we have: " << s << endl;
		  cout << "string in: " << s << endl;
		}
	    }
	  else
	    {
	      cout << "Instead of Module: we have: " << s << endl;
	      //get rid of the rest of the line
	      getline(configfile, s);
	      cout << "string out: " << s << endl;
	    }
	}
      else
	{
	  getline(configfile, s); //clear commented line
	  cout << "commented line... " << s << endl;
	}
    }
  configfile.close();  
}


#ifdef debug
/* main */

#include <gtkmm/main.h>

int main(int argc, char *argv[])
    {
      Gtk::Main kit(argc, argv);

      ExampleWindow window;
      Gtk::Main::run(window); //Shows the window and returns when it is closed.
  return 0;
}

/* end main */
#endif
