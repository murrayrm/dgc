/* AState.cc - new vehicle state estimator
 *
 * JML 31 Dec 04
 *
 * */

#include "AState.hh"

extern double GPS_xy_kr;
extern double GPS_v_kr;

// Sparrow input variables
extern int gps_time_offset;	// microseconds time offset (from AState.c)
extern double gps_err_const;
extern double gps_v_kr_const;
extern int gps_xy_err_mode;
extern int gps_v_err_mode;

extern int QUIT_PRESSED;

struct raw_GPS
{
    unsigned long long gps_time;
    int gps_valid;
    double lat;
    double lng;
    double alt;
    double vel_e;
    double vel_n;
    double vel_u;
};

void AState::GPS_init()
{
	if (gps_enabled == 0) {
		return;
	}
    if (replay == 1) {
             raw_GPS gps_in;
             gps_replay_stream.read((char *)&gps_in, sizeof(raw_GPS));

             my_gps_time = gps_in.gps_time;
             gps_valid = gps_in.gps_valid;
             gpsdata.data.lat = gps_in.lat;
             gpsdata.data.lng = gps_in.lng;
             gpsdata.data.altitude = gps_in.alt;
             gpsdata.data.vel_e = gps_in.vel_e;
             gpsdata.data.vel_n = gps_in.vel_n;
             gpsdata.data.vel_u = gps_in.vel_u;

             gps_log_start = my_gps_time;
    } else {
	    gps_enabled = gps_open(4); // GPS_open needs to return -1 on failure!
	    if(gps_enabled == -1)
	    {
		    cerr << "AState::GPS_init(): failed to open GPS" << endl;
	    } else
	    {
		    gps_set_nav_rate(10);
		    gps_set_msg_rate(GPS_MSG_PVT,-3);
	    }
    }
}


void AState::GPS_thread()
{
  char *gps_buffer;
  unsigned long long gps_local_time;
  if ((gps_buffer = (char *) malloc(2000)) == NULL)
    {
      cerr << "AState::GPS_thread(): memory allocation error" << endl;
    }

  LatLong gps_ll(0,0);
  double dn;
  double de;
  double product;
  double magnitude;
  double angle;
  double jump;

  unsigned long long nowtime;

  raw_GPS gps_in;
  raw_GPS gps_out;
  unsigned long long raw_gps_time;
	
  while ( QUIT_PRESSED != 1) {
    if (replay == 1) {
      gps_buffer[3] = GPS_MSG_PVT;

      gps_replay_stream.read((char *)&gps_in, sizeof(raw_GPS));
      raw_gps_time = gps_in.gps_time - imu_log_start + starttime;

      DGCgettime(nowtime);
      while (raw_gps_time > nowtime) {
	usleep(1);
	DGCgettime(nowtime);
      }
    } else {
      if (gps_enabled == 1) {
	gps_read(gps_buffer, 1000);  //Need to determine if this is blocking...
	DGCgettime(gps_local_time); // Time stamp as soon as data read.
	gps_local_time -= gps_time_offset;
      } else {
	gps_buffer[3] = GPS_MSG_PVT;
	usleep(500000);
	DGCgettime(gps_local_time); // Time stamp as soon as data read.
      }
    }

    switch (gps_msg_type(gps_buffer)) {
    case GPS_MSG_PVT:

      // LOCK THE MUTEX!
      DGClockMutex(&m_GPSDataMutex);
      if (replay == 1) {

	gps_valid = gps_in.gps_valid;
	gpsdata.data.lat = gps_in.lat;
	gpsdata.data.lng = gps_in.lng;
	gpsdata.data.altitude = gps_in.alt;
	gpsdata.data.vel_e = gps_in.vel_e;
	gpsdata.data.vel_n = gps_in.vel_n;
	gpsdata.data.vel_u = gps_in.vel_u;

	my_gps_time = raw_gps_time;

                        
      } else {

	if (gps_enabled == 1) {

	  if (gpsdata.update_gps_data(gps_buffer) == -1) {
	    //Ignored invalid PVT
	    DGCunlockMutex(&m_GPSDataMutex);
	    continue;
	  }
	  my_gps_time = gps_local_time;
	  gps_valid = (gpsdata.data.nav_mode & NAV_VALID) && 1;
	} else {
	  gps_valid = 1;
	  gpsdata.data.lat = 34;
	  gpsdata.data.lng = -118;
	  gpsdata.data.altitude = 0;
	  gpsdata.data.vel_e = 0;
	  gpsdata.data.vel_n = 0;
	  gpsdata.data.vel_u = 0;
	  my_gps_time = gps_local_time;
	}
      }

      if (log_raw == 1) {
	gps_out.gps_time = my_gps_time;
	gps_out.gps_valid = gps_valid;
	gps_out.lat = gpsdata.data.lat;
	gps_out.lng = gpsdata.data.lng;
	gps_out.alt = gpsdata.data.altitude;
	gps_out.vel_e = gpsdata.data.vel_e;
	gps_out.vel_n = gpsdata.data.vel_n;
	gps_out.vel_u = gpsdata.data.vel_u;
            
	gps_log_stream.write((char *)&gps_out, sizeof(raw_GPS));
      }
      //gps_valid = 1;
      if (gps_valid) {
	gps_ll.set_latlon(gpsdata.data.lat, gpsdata.data.lng);
	gps_ll.get_UTM(&gps_east, &gps_north);

	if (gps_xy_err_mode == 1) {

		/* 
	 	* Set the covariance according to the error in data measurements.  
	 	*
	 	* In addition, if we detect a large jump in GPS yaw
	 	* then set the covariance higher so that we don't trust 
	 	* the measurement as much.
	 	*/
	
		/* Compute the differences in current and previous position */
		de = gps_east - vehiclestate.Easting;
		dn = gps_north - vehiclestate.Northing;
		magnitude = sqrt( dn * dn + de * de );
	
		/* See how this compares to the current yaw angle */
		product = (dn * cos(vehiclestate.Yaw) + de * sin(vehiclestate.Yaw)) / magnitude;
		angle = acos(product);
	
		/* Initialize the GPS error to the current value */
		gps_err = sqrt(GPS_xy_kr) * EARTH_RADIUS;
	
		/* See if we have encountered a jump (in yaw) */
		if (angle >= ANGLE_CUTOFF) {
	  	jump = magnitude;
		} else {
	  	jump = 0;
		}
	
		/* If the jump is bigger than current covariance estimate
	 	* then recompute the gps error
	 	*/
		if (jump > gps_err) {   
	  	gps_err = magnitude * ERROR_WEIGHT;
		} else {
	  	/* Decrease the GPS error */
	  	gps_err *= ERROR_FALLOFF;
		}
	
		/* Put a lower bound on the size of the GPS error */
		if (gps_err < .2) {
	  	gps_err = .2;
		}  

		/* Now convert the GPS error to the units wanted by KF */
		GPS_xy_kr = pow(gps_err / EARTH_RADIUS, 2);

	} else if (gps_xy_err_mode == 2) {

		/* This code attempts to use the actual GPS error
	 	* as reported by the Navcom (plus a multiplicative
	 	* fudge factor)
	 	*/
		gps_err = (double)gpsdata.data.position_fom / 100.0;
		gps_err *= 1.0;

		/* Now convert the GPS error to the units wanted by KF */
		GPS_xy_kr = pow(gps_err / EARTH_RADIUS, 2);

	} else {
		gps_err = gps_err_const;

		/* Now convert the GPS error to the units wanted by KF */
		GPS_xy_kr = pow(gps_err / EARTH_RADIUS, 2);
	}


	/*
	 * The following code is used when we are at rest to allow the Kalman
	 * filter to rapidly converge on the IMU biases and other parameters
	 * 
	 * The basic idea is that if we are at rest, then we set the covariance
	 * on the GPS velocity to a small number and set the velocities to 
	 * identically zero.
	 */
	if (stationary == 1) {
	  GPS_v_kr = .001;
	  gpsdata.data.vel_e = 0;
	  gpsdata.data.vel_n = 0;
	  gpsdata.data.vel_u = 0;
	} else {
	  /* The default case: if moving, set the covariance appropriately */
	  GPS_v_kr = gps_v_kr_const;
	}

	++gps_count;
      }
      DGCunlockMutex(&m_GPSDataMutex);
    }
    if (imu_enabled == 0 && kfilter_enabled == 0) {
      UpdateState();
    }
  }
  free(gps_buffer);
}
