/*
 * Changes:
 * 
 * Lars Cremean, 7 Sep 04, created (based on Arbiter2/navdisplay.dd)
 *
 */

#include "TrajFollower.hh"

static int user_quit(long);

%%
    ============================= TrajFollower ================================
00 +---------------------------+-----------------------------------------------+
01 | Time     [s]: %tim        | SteerCommand [rad]  :  %steer_cmd_rad         |
02 | Northing [m]: ---         | AccelCommand [m/s/s]:  %accel_cmd             |
03 | Easting  [m]: ---         | Pitch [rad]: %pit   [deg] %pitd               |
04 | Altitude [m]: %alt        | Roll  [rad]: %rol   [deg] %rold               |
05 | Vel_E  [m/s]: %v_eas      | Yaw   [rad]: ---    [deg] %yawd               |
06 | Vel_N  [m/s]: %v_nor      | PitchRate [rad/s]: %v_pit   [deg/s] %v_pitd   |
07 | Vel_U  [m/s]: %v_alt      | RollRate  [rad/s]: %v_rol   [deg/s] %v_rold   |
08 | Speed  [m/s]: ---         | YawRate   [rad/s]: %v_yaw   [deg/s] %v_yawd   |
09 +---------------------------+-----------------------------------------------+
10 | Traj count:  %trct          phiCmd    [rad]: %phi                         |
11 | Ref. index:  %refi          accelCmd  [m/s/s]: %accl                      |
12 +---------------------------------------------------------------------------+
13 |        Accel   Phi             Err          Des          Act              |
14 |                                                                           |
15 |      [ %k0     %k1     ]  Nor                            %nor             |
16 |      [ %k2     %k3     ]  Eas                            %eas             |
17 | pK = [ %k4     %k5     ]  Spd                            %speed           |
18 |      [ %k6     %k7     ]  Yaw                            %yaw             |
19 |                                                                           |
20 |                                                                           |
21 | yError: %yerr             Ref Position W-Base (0=REAR, 1=FRONT):%yERef    |
22 | aError: %aerr             Ref Position W-Base (0=REAR, 1=FRONT):%aERef    |
23 | cError: %cerr                                                             |
24 |                                                                           |
25 | AccelFF:  %accff          VRef: %vre    			               |
26 | AccelFB:  %accfb          VTraj:%vtr      			               |
27 | AccelCmd: %acccmd                                 		               |
28 |                                                                           |
29 | SteerFF:  %strff          Ref Position W-Base (0=REAR, 1=FRONT): %aFRef   |
30 | SteerFB:  %strfb                                                          |
31 | SteerCmd: %strcmd                                                         |
   +---------------------------------------------------------------------------+
    Display Update Count: %uc
    CTRL-C to exit
%% 
tblname: vddtable;
bufname: vddbuf;

int:    %trct RecvTrajCount   "%d";
int:    %refi TrajIndex       "%d";
double: %vre  vRef        "%.3f";
double: %vtr  vTraj       "%.3f";

double: %accff  accelFF       "%.3f";
double: %accfb  accelFB       "%.3f";
double: %acccmd accelCmd      "%.3f";

double: %strff  steerFF       "%.3f";
double: %strfb  steerFB       "%.3f";
double: %strcmd steerCmd      "%.3f";

double: %aerr aError        "%.3f";
double: %yerr yError        "%.3f";
double: %cerr cError        "%.3f";

double: %yERef   yErrRef         "%.3f";
double: %aERef   aErrRef         "%.3f";
double: %aFRef   aFFref          "%.3f";

double: %phi    dphiCmd                         "%.3f";
double: %accl   daccelCmd                       "%.3f"; 

double: %steer_cmd_rad  steerCommandRad         "%.3f";
double: %accel_cmd      accelCommand            "%.3f";

double: %k0    K0                           "%6.4f";
double: %k1    K1                           "%6.4f";
double: %k2    K2                           "%6.4f";
double: %k3    K3                           "%6.4f";
double: %k4    K4                           "%6.4f";
double: %k5    K5                           "%6.4f";
double: %k6    K6                           "%6.4f";
double: %k7    K7                           "%6.4f";


double: %tim    d_decimaltime              "%10.1f";
double:  %speed  d_speed                    "%.3f";

double:  %nor    dispSS.Northing                 "%10.3f";
double:  %eas    dispSS.Easting                  "%10.3f";
double:  %alt    dispSS.Altitude                 "%.3f";
double:  %v_nor  dispSS.Vel_N                    "%.3f";
double:  %v_eas  dispSS.Vel_E                    "%.3f";
double:  %v_alt  dispSS.Vel_D                    "%.3f";
double:  %pit    dispSS.Pitch                    "%.3f";
double:  %rol    dispSS.Roll                     "%.3f";
double:  %yaw    dispSS.Yaw                      "%.4f";
double:  %v_pit  dispSS.PitchRate                "%.3f";
double:  %v_rol  dispSS.RollRate                 "%.3f";
double:  %v_yaw  dispSS.YawRate                  "%.3f";

double:  %pitd   PitchDeg                        "%.3f";
double:  %rold   RollDeg                         "%.3f";
double:  %yawd   YawDeg                          "%.3f";
double:  %v_pitd PitchRateDeg                    "%.3f";
double:  %v_rold RollRateDeg                     "%.3f";
double:  %v_yawd YawRateDeg                      "%.3f";

short:  %uc SparrowUpdateCount     "%5d";

