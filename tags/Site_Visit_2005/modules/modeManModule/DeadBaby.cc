/**
 * DeadBaby.cc
 *  Dead baby sensing based purely on state data.
 *
 * 3/21/2005      Sue Ann Hong         Created
 *   This version merely looks at pitch/roll rate and adjusts the speed
 *  accordingly. This is really a shell and H will fill in more sophisticated 
 *  schemes later.
 **/

#include <math.h>
#include "DeadBaby.hh"
#include "ModeManModule.hh"

/**
 * UpdateDBS()
 *  Sets the global_speedlimit with maximum speed OK for the current roll rate 
 * and pitch rate, if global_speedlimit isn't already low enough.
 **/
void ModeManModule::UpdateDBS()
{
  dbsLimit = m_state.Speed2()/(1. +
    max(fabs(m_state.Roll) - ROLL_THRESH,0.) * ROLL_FACTOR +
    max(fabs(m_state.RollRate) - ROLL_RATE_THRESH,0.) * ROLL_RATE_FACTOR +
    max(fabs(m_state.PitchRate) - PITCH_RATE_THRESH,0.) * PITCH_RATE_FACTOR +
    max(fabs(m_state.Acc_D) - Z_ACEL_THRESH,0.) * Z_ACEL_FACTOR);
}
