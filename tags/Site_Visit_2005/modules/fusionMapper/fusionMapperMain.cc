#include <stdlib.h>
#include <stdio.h>

#include "fusionMapper.hh"
#include <DGCutils>

#include <getopt.h>

enum {
  OPT_NONE,
  OPT_HELP,
  OPT_SPARROW,
  OPT_WAIT,
  OPT_LADAR,
  OPT_STEREO,
  OPT_SNKEY,
  OPT_AVERAGE,
  OPT_MAX, 

  NUM_OPTS
};


void printFusionMapperHelp();  

int main(int argc, char** argv) {
  //Initialize options to be passed to the FusionMapper object
  FusionMapperOptions mapperOpts;
  mapperOpts.optLadar = 1;
  mapperOpts.optStereo = 0;
  mapperOpts.optSNKey = -1;
  mapperOpts.optAverage = 0;
  mapperOpts.optMax = 0;

  //Initialize local options
  int optSparrow = 0;
  int optWait = -1;

  int c;
  int digit_optind = 0;


  static struct option long_options[] = {
    //Options that don't require arguments
    {"nosparrow", no_argument, &optSparrow, 0},
    {"help",      no_argument, 0, OPT_HELP},
    {"nowait", no_argument, &optWait, 0},
    {"noladar", no_argument, &mapperOpts.optLadar, 0},
    {"nostereo", no_argument, &mapperOpts.optStereo, 0},    
    //Options that require arguments
    {"snkey", required_argument, 0, OPT_SNKEY},
    {"skynetkey", required_argument, 0, OPT_SNKEY},
    //Options that have optional arguments
    {"wait", optional_argument, 0, OPT_WAIT},
    {"ladar", optional_argument, 0, OPT_LADAR},
    {"stereo", optional_argument, 0, OPT_STEREO},
    {"sparrow", optional_argument, 0, OPT_SPARROW},
    {"average", optional_argument, 0, OPT_AVERAGE},
    {"max", optional_argument, 0, OPT_MAX},
    {0,0,0,0}
  };


  while (1) {
    int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    
    c = getopt_long_only(argc, argv, "", long_options, &option_index);
    if(c == -1) break;

    switch(c) {
    case '?':
    case OPT_HELP:
      printFusionMapperHelp();
      exit(1);
      break;
    case OPT_SNKEY:
      mapperOpts.optSNKey = atoi(optarg);
      break;
    case OPT_WAIT:
      if(optarg != NULL) {
	optWait=atoi(optarg);
      } else {
	optWait = 7;
      }
      break;
    case OPT_LADAR:
      if(optarg!=NULL) {
	mapperOpts.optLadar = atoi(optarg);
      } else {
	mapperOpts.optLadar = 1;
      }
      break;
    case OPT_STEREO:
      if(optarg!=NULL) {
	mapperOpts.optStereo = atoi(optarg);
      } else {
	mapperOpts.optStereo = 1;
      }
      break;
    case OPT_AVERAGE:
      if(optarg!=NULL) {
	mapperOpts.optAverage = atoi(optarg);
      } else {
	mapperOpts.optAverage = 1;
      }
      break;
    case OPT_MAX:
      if(optarg!=NULL) {
	mapperOpts.optMax = atoi(optarg);
      } else {
	mapperOpts.optMax = 1;
      }
      break;
    default:
      if(c!=0) {
	printf("Unknown option %d!\n", c);
	printFusionMapperHelp();
	exit(1);
      }
    }
  }

  char* ptrSkynetKey = getenv("SKYNET_KEY");
  
  if(ptrSkynetKey == NULL && mapperOpts.optSNKey==-1) {
    printf("You have not specified a skynet key as a command-line argument (using --snkey)\n");
    printf("or as an environment variable.  To set a skynet ket, try doing:\n");
    printf("$> export SKYNET_KEY=X\n");
    printf("where X is some number.  To check your skynet key, do:\n");
    printf("$> echo $SKYNET_KEY\n");
    return 0;
  } else {
    printf("Welcome to FusionMapper\n");
    if(mapperOpts.optSNKey==-1) {
      mapperOpts.optSNKey = atoi(ptrSkynetKey);
    }
   
    printf("Option Summary - General:\n");
    printf("SkynetKey      - %d\n", mapperOpts.optSNKey);
    printf("Sparrow        - %d\n", optSparrow);
    printf("Using Ladar    - %d\n", mapperOpts.optLadar);
    printf("Using Stereo   - %d\n", mapperOpts.optStereo);
    printf("Averaging      - %d\n", mapperOpts.optAverage);
    printf("Max            - %d\n", mapperOpts.optMax);

    printf(" == HIT CTRL+C TO CANCEL ==\n");
    char temp;
    if(optWait>=0) {
      for(int i=0; i<optWait; i++) {
	printf("Program will start in %d seconds...\r", optWait-i);
	fflush(stdout);
	sleep(1);
      }
    } else if(optWait==-1) {
      printf("Please review the options you have set.  Do you want to continue (y/n)? ");      
      temp = getchar();
      if(temp!='y' && temp!='Y') {
	printf("You have entered something other than 'y' - quitting...\n");
	return 0;
      }
    }


    FusionMapper fusionMapperObj(mapperOpts.optSNKey, mapperOpts);

    DGCstartMemberFunctionThread(&fusionMapperObj, &FusionMapper::ReceiveDataThread_LadarRoof);

    DGCstartMemberFunctionThread(&fusionMapperObj, &FusionMapper::ReceiveDataThread_LadarBumper);

    fusionMapperObj.ActiveLoop();
  }

  return 0;
}


void printFusionMapperHelp() {
  printf("Usage: fusionMapper [OPTION]\n");
  printf("Runs the Team Caltech DGC fusionMapper software\n\n");

  printf("  --sparrow[=N]       Sets whether or not to show the sparrow display (w/out argument shows)\n");
  printf("  --nosparrow         Does not show the sparrow display\n");
  printf("  --help              Prints this help message\n");
  printf("  --wait[=N]          Sets how long to wait before starting (w/out argument is 7 sec)\n");
  printf("  --nowait            Disables a wait before the program begins\n");
  printf("  --ladar             Sets whether or not to use ladar (w/out argument is true)\n");
  printf("                      fusionMapper runs with ladar by default\n");
  printf("  --noladar           Disables using ladar\n");
  printf("  --stereo            Sets whether or not to use stereo (w/out argument is true)\n");
  printf("                      fusionMapper runs without stereo by default\n");
  printf("  --nostereo          Disables using stereo\n");
  printf("\n");
}
