#include "fusionMapper.hh"

#define MAX_DELTA_SIZE 999999
#define MAX_ROAD_SIZE 1000

FusionMapper::FusionMapper(int skynetKey, FusionMapperOptions mapperOptsArg) 
  : CSkynetContainer(SNfusionmapper, skynetKey), delta_log("logs/delta.fm.log") {
  printf("Welcome to fusionmapper init\n");

  mapperOpts = mapperOptsArg;

  delta_log << "# fusionMapper produced this.  Time then size." << endl;
    
  //m_pMapDelta = new char[MAX_DELTA_SIZE];
  m_pRoadInfo = new double[MAX_ROAD_SIZE];

  int numRows = 150;
  int numCols = 150;
  double resRows = 1;
  double resCols = 1;
  
  fusionMap.initMap(0,0, 
		  numRows, numCols,
		  resRows, resCols,
		  0);

#warning "magic constants here. need comments/explication. wrong constants will stop the rddf from being painted in"
  layerID_ladarElev = fusionMap.addLayer<double>(0, -1);
  layerID_cost    = fusionMap.addLayer<double>(0.5e-3, 1e-4);
  layerID_number = fusionMap.addLayer<long long>(0, -1);
  layerID_corridor = fusionMap.addLayer<double> (0, -1);
  
  fusionCorridorPainter.initPainter(&(fusionMap), 
				    layerID_cost, 
				    &(fusionRDDF),
				    1.0,
				    1e-3,
				    5.0,
				    4.5,
				    0.5,
				    1);

fusionCorridorReference.initPainter(&(fusionMap), layerID_corridor, &(fusionRDDF), 1.0, 1e-3, 5, 4.5, .5, 0);
  

  fusionCostPainter.initPainter(&(fusionMap),
				layerID_cost,
				layerID_ladarElev,
                layerID_corridor
				);  

  DGCcreateMutex(&m_mapMutex);
}


FusionMapper::~FusionMapper() {
  //FIX ME
  DGCdeleteMutex(&m_mapMutex);
  printf("Thank you for using the fusionmapper destructor\n");
}


void FusionMapper::ActiveLoop() {
  //TODO - Give this a sparrow/sngui display
  printf("Entering FusionMapper active-loop\n");
  printf("Press Ctrl+C to quit\n");

  int socket_num = m_skynet.get_send_sock(SNfusiondeltamap);

  void* deltaPtr = NULL;
  int deltaSize = 0;

  double UTMNorthing;
  double UTMEasting;
  int int_northing[4];
  int int_easting[4];
  
  double cellval;

  NEcoord exposedRow[4], exposedCol[4];

  
  while(true) {
    UpdateState();


#warning "Should change updateVehicleLoc to be updateMapCenter"
    DGClockMutex(&m_mapMutex);
    fusionMap.updateVehicleLoc(m_state.Northing, m_state.Easting);
    fusionMap.getExposedRowBoxUTM(exposedRow);
    fusionMap.getExposedColBoxUTM(exposedCol);
    fusionCorridorPainter.paintChanges(exposedRow, exposedCol);    
    fusionCorridorReference.paintChanges(exposedRow, exposedCol);
    deltaPtr = fusionMap.serializeDelta<double>(layerID_cost, deltaSize);
    DGCunlockMutex(&m_mapMutex);    


    int sent_OK;

    //code to send delta
    if(deltaSize>20) {
      sent_OK = m_skynet.send_msg(socket_num, deltaPtr, deltaSize, 0);
      fusionMap.resetDelta(layerID_cost);
      printf("Sent was %d\n", sent_OK);
      unsigned long long now;
      DGCgettime(now);
      delta_log << now << " " << sent_OK << endl;
    } else {
      //printf("deltaSize <= 20. Didn't send\n");
    }

    usleep(10000);
  }
}


void FusionMapper::ReceiveDataThread_LadarRoof() {
  int mapDeltaSocket = m_skynet.listen(SNladardeltamap_roof, MODladarfeeder_roof);
  double UTMNorthing;
  double UTMEasting;

  double cellval;
  int count=0;

  long long num_cell_updates;
  double old_val;

  m_pMapDelta = new char[MAX_DELTA_SIZE];
  while(true) { 
    if(mapperOpts.optLadar || mapperOpts.optStereo) {
      int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pMapDelta, MAX_DELTA_SIZE, 0);
      if(numreceived>0) {
	printf("Got a roof!\n");
	DGClockMutex(&m_mapMutex);
	int cellsize = fusionMap.getDeltaSize(m_pMapDelta);
	for(int i=0; i<cellsize; i++) {
	  cellval = fusionMap.getDeltaVal<double>(i, layerID_ladarElev, m_pMapDelta, &UTMNorthing, &UTMEasting);
	  if(mapperOpts.optAverage) {
	    num_cell_updates = fusionMap.getDataUTM<long long>(layerID_number, UTMNorthing, UTMEasting);
	    fusionMap.setDataUTM<long long>(layerID_number, UTMNorthing, UTMEasting, num_cell_updates+1);
	    old_val = fusionMap.getDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting);
	    fusionMap.setDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting, 
					 ((old_val*num_cell_updates)+cellval)/(num_cell_updates+1));
	  } else if(mapperOpts.optMax) {
	    old_val = fusionMap.getDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting);
	    if(cellval < old_val || 
	       old_val==fusionMap.getLayerNoDataVal<double>(layerID_ladarElev)) {
	      fusionMap.setDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting, cellval);	
	    }
	  } else {
	    fusionMap.setDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting, cellval);	
	  }
	  fusionCostPainter.paintChanges(UTMNorthing, UTMEasting, 1);
	}
	DGCunlockMutex(&m_mapMutex);
      }     
    }
    usleep(100);
  }
  delete m_pMapDelta; 
}


void FusionMapper::ReceiveDataThread_LadarBumper() {
  int mapDeltaSocket = m_skynet.listen(SNladardeltamap_bumper, MODladarfeeder_bumper);
  double UTMNorthing;
  double UTMEasting;

  double cellval;
  int count=0;

  long long num_cell_updates;
  double old_val;

  m_pMapDelta = new char[MAX_DELTA_SIZE];
  while(true) { 
    if(mapperOpts.optLadar || mapperOpts.optStereo) {
      int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pMapDelta, MAX_DELTA_SIZE, 0);
      if(numreceived>0) {
	printf("Got a bumper!\n");
	DGClockMutex(&m_mapMutex);
	int cellsize = fusionMap.getDeltaSize(m_pMapDelta);
	for(int i=0; i<cellsize; i++) {
	  cellval = fusionMap.getDeltaVal<double>(i, layerID_ladarElev, m_pMapDelta, &UTMNorthing, &UTMEasting);

	  if(mapperOpts.optAverage) {
	    num_cell_updates = fusionMap.getDataUTM<long long>(layerID_number, UTMNorthing, UTMEasting);
	    fusionMap.setDataUTM<long long>(layerID_number, UTMNorthing, UTMEasting, num_cell_updates+1);
	    old_val = fusionMap.getDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting);
	    fusionMap.setDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting, 
					 ((old_val*num_cell_updates)+cellval)/(num_cell_updates+1));
	  } else if(mapperOpts.optMax) {
	    old_val = fusionMap.getDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting);
	    if(cellval < old_val || 
	       old_val==fusionMap.getLayerNoDataVal<double>(layerID_ladarElev)) {
	      fusionMap.setDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting, cellval);	
	    }
	  } else {
	    fusionMap.setDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting, cellval);	
	  }

	  fusionCostPainter.paintChanges(UTMNorthing, UTMEasting, 0);
	}
	DGCunlockMutex(&m_mapMutex);
      }     
    }
    usleep(100);
  }
  delete m_pMapDelta; 
}


void FusionMapper::ReceiveRoadDataThread() {
  /*
  int roadInfoSocket = m_skynet.listen(SNroadboundary, SNroadfinding);

  if(roadInfoSocket < 0)
    cerr << "error - FIXME" << endl;

  while(true) {
    printf("Attempting to get road info\n");
    int numreceived = m_skynet.get_msg(roadInfoSocket, m_pRoadInfo, MAX_ROAD_SIZE, 0);		  
    if(numreceived>0)
      {
	int numScanLines = m_pRoadInfo[0];
	if(numreceived != sizeof(double)*(numScanLines*6+1)) {
	  cerr << "error -FIXME" << endl;
	} else {
	  RoadBoundary* roadArray = new RoadBoundary[numScanLines];
	  for(int i=0; i<numScanLines; i++) {
	    roadArray[i].left = new XYZcoord(m_pRoadInfo[i*6+1], 
					     m_pRoadInfo[i*6+2], 
					     m_pRoadInfo[i*6+3]);
	    roadArray[i].right = new XYZcoord(m_pRoadInfo[i*6+4], 
					      m_pRoadInfo[i*6+5], 
					      m_pRoadInfo[i*6+6]);
	    //do stuff with the road array here
	    //like put it in the map
	  }
	}
      }

  }		
  // update map at 10hz
  usleep(10000);
  */
}

