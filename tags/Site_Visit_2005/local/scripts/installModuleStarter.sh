#!/bin/bash

# This script downloads/updates the current 
# repository and runs the necessary coompilations 
# to run the benchmarks
#
# Created: Apr 29, 2005
# Author: Henry Barnor
#TODO


# first go to user home directory
cd ~/
#check if dgc directory exits and update else ceckout
if [ -f ~/dgc ]; then 
    cd ~/dgc
    svn up 
else
    svn co svn+ssh://team@gc.caltech.edu/dgc/subversion/dgc/trunk dgc
    cd dgc
fi
#run make in dgc
make uninstall
make
#now copy skynetd executable
if [ ! -e /usr/local/skynet ]; then     
    mkdir /usr/local/skynet
fi

cp bin/skynetd /usr/local/skynet/.    
cp bin/moduleStarter /usr/local/skynet/.
cp bin/ModuleStarter.config /usr/local/skynet/.