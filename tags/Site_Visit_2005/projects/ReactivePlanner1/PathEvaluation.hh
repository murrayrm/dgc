#ifndef PATHEVALUATION
#define PATHEVALUATION

#include <math.h>
#include "CMap.hh"
#include "traj.h"

//Sums the costs of all points on path evaluated as evaluated with GetAreaCost and returns sum. 
//'+' and '>' must be defined for type T. 
template <class T>
T SumCosts(CTraj path, int numpoints, double deltadist, CMap &costmap, double vehLength, double vehWidth, int layernum);

//Should return the cost of the highest-cost point under the vehicle. '>' must be defined for type T. 
template < class T>
T GetAreaCost(double Northing, double Easting, double Yaw, CMap &costmap, double vehLength, double vehWidth, int layernum);

//Should convert from Northing/Easting to vehicle cootdinates.
void VehFrameToNorthEast(double &hereN, double &hereE, double lendthwise, double widthwise, double originN, double originE, double Yaw);

#endif
