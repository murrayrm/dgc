/**
 * RoadFinding.hh
 * Primary Road finding module

 * Revision History
 * 3/3/05 - Ryan Farmer (ryanf)
 */

#ifndef ROADFINDING_HH
#define ROADFINDING_HH


#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

/* fix the includes below when the makefile has been set up */
// #include "projects/ModuleHelpers/SkynetContainer.h"
// #include "../../frames/coords.hh"
// #include "snmsg.hh"


using namespace std;

/** RoadBoundary structure:
 * Description:  Represents a road from the RoadFinding module
 *  Each point is the end of a road in 3-space, each pair (left and right) define the left and right edges of the road.
 */
struct RoadBoundary
{
  XYZcoord *left;
  XYZcoord *right;

  RoadBoundary()
  {
    left = NULL;
    right = NULL;
  }

  RoadBoundary(double x1, double y1, double z1, double x2, double y2, double z2)
  {
    // uses the DGC defn of xyz space coordinates
    left = new XYZcoord(x1, y1, z1);
    right = new XYZcoord(x2, y2, z2);
  }

  ~RoadBoundary()
  {
    if (left != NULL)
      delete left;

    if (right != NULL)
      delete right;
  }
};

/**RoadFinding Class
 * Uses the stereo pair to obstain an image, runs the road finding software, and uses that stereo information to compute the edge locations in space.
 * It then sends the information via Skynet to FusionMapper 
 * 
 * Note:  Currently, this sends garbage data--no stereo or road detection software is implemented here.
 * TODO:  Add serialize and deserialize functions (functionality already present here and in FusionMapper)
 */
class RoadFinding : CSkynetContainter
{
public:
  /* Constructor */
  RoadFinding(int skynetKey);

  /* Deconstructor */
  ~RoadFinding();

  /* returns the vanishing point direction in degrees (0 being straight ahead, -180 being to the left, 180 being to the right) */
  double GetVanishingPointDirection();

  /* Gets the current road area polygon */
  RoadBoundary *GetRoadArea();

  /* Gets how many scanlines we are using to define the road polygon */ 
  int GetRoadCount();

  /* Sends the current road area to FusionMapper via skynet */
  void SendToSkynet();

  /* Connects to skynet to send road area data to FusionMapper */
  void ConnectToSkynet();

protected:
  /* height of the image we are doing processing on */
  int m_imgWidth;

  /* height of the image we are doing processing on */
  int m_imgHeight;

  /* Vanishing Point XY coordinates */
  int m_vPoint_X;
  int m_vPoint_Y;

  /* Current road boundary (from last run) */
  RoadBoundary *m_RoadBounds;

  /* Number of scan lines on the road to add to the road structure (number of vertical points that we scan at--up to the vanishingpoint) */
  int m_RoadCount;

  /* skynet variables */
  /* these are provided by the parent class */
  //  skynet m_Skynet;
  //   modulename m_myself;

  /* skynet message output */
  sn_msg m_skyOutput;

  int m_socket;
  bool m_SkynetConnected;

private:

};

#endif 
