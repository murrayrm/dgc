//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_Tracker.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

extern IplImage *candidate_image;

extern int candidate_width;
extern int candidate_height;
extern int candidate_lateral_margin;
extern int candidate_top_margin;
extern int candidate_bottom_margin;

extern float support_cur_dx, support_cur_dy;   

//----------------------------------------------------------------------------

CR_ParticleFilter *pf_tracker;           // vanishing point tracker

int show_particles = FALSE;

int pf_candidate_delta = 5;      // initial grid spacing of particles (in pixels)

double pf_prior_x, pf_prior_y;
int pf_candidate_left, pf_candidate_right, pf_candidate_top, pf_candidate_bottom;

CR_Matrix *pf_cov;
CR_Matrix *pf_samps;

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void get_tracker_position(float *x, float *y)
{
  *x = (float) pf_tracker->x->x[0];
  *y = (float) pf_tracker->x->x[1];
}

//----------------------------------------------------------------------------

void write_tracker(FILE *fp, int num)
{
  fprintf(fp, "%i, %f, %f\n", num, pf_tracker->x->x[0], pf_tracker->x->x[1]);
  fflush(fp);
}

//----------------------------------------------------------------------------

// should have some notion of confidence--variance of particle distribution?

void draw_tracker(int win_w, int win_h)
{
  int i;

  // samples are yellow

  if (show_particles) {
    glColor3ub(255, 255, 0);
    glPointSize(2);

    glBegin(GL_POINTS);

    for (i = 0; i < pf_tracker->n; i++) 
      glVertex2f(pf_tracker->s[i]->x[0], win_h - pf_tracker->s[i]->x[1]);

    glEnd();
  }

  // horizon line is red

  glColor3ub(255, 0, 0);
  glLineWidth(1);

  glBegin(GL_LINES);
  
  glVertex2f(0, win_h - pf_tracker->x->x[1]);
  glVertex2f(win_w, win_h - pf_tracker->x->x[1]);

  glEnd();

  // state is blue

  glColor3ub(0, 0, 255);
  glPointSize(5);

  glBegin(GL_POINTS);
  
  glVertex2f(myround(pf_tracker->x->x[0]), win_h - myround(pf_tracker->x->x[1]));

  glEnd();
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// execute another iteration of particle filter

void update_tracker()
{
  update_CR_ParticleFilter(NULL, pf_tracker);
}

//----------------------------------------------------------------------------

// distribute particles uniformly across image (pf_prior_x and _y are initialized
// to upper-left corner of grid here)

void reset_tracker()
{
  pf_prior_x = pf_candidate_left + 0.5 * (double) pf_candidate_delta;
  pf_prior_y = pf_candidate_top + 0.5 * (double) pf_candidate_delta;
  
  support_cur_dx = 0;
  support_cur_dy = 1;   

  reset_CR_ParticleFilter(pf_tracker);
}

//----------------------------------------------------------------------------

// number of samples is determined by image dimensions and candidate_delta, 
// which indicates inter-sample spacing of grid

void initialize_tracker()
{
  FILE *fp;
  int i, num_samps;
  float w;

  support_cur_dx = 0;
  support_cur_dy = 1;   

  pf_candidate_left = candidate_lateral_margin;
  pf_candidate_right = candidate_lateral_margin + candidate_width;
  pf_candidate_top = candidate_top_margin;
  pf_candidate_bottom = candidate_top_margin + candidate_height;

  num_samps = candidate_width * candidate_height / (SQUARE(pf_candidate_delta));
  printf("num samps = %i (w = %i, h = %i, del = %i)\n", num_samps, candidate_width, candidate_height, pf_candidate_delta);

  // var = sigma^2, 95% within 2*sigma
  // horizontal: 2*sigma = pf_w / 40 => sigma = pf_w / 80 
  // vertical: 2*sigma = pf_w / 57 => sigma = pf_w / 114

  w = (float) candidate_width;

  pf_cov = make_all_CR_Matrix("pf_cov", 2, 2, 0.0);
  MATRC(pf_cov, 0, 0) = SQUARE(w / 80.0);
  MATRC(pf_cov, 1, 1) = SQUARE(w / 114.0);

  pf_prior_x = 0.5 * (double) pf_candidate_delta;
  pf_prior_y = 0.5 * (double) pf_candidate_delta;

  pf_tracker = make_CR_ParticleFilter(num_samps, 
				      make_CR_Vector("pf_state", 2), 
				      pf_cov,
				      pf_condprob_zx, 
				      pf_samp_prior, 
				      pf_samp_state, 
				      pf_dyn_state);

  pf_samps = make_CR_Matrix("samps", 1, 3*pf_tracker->n);

  for (i = 0; i < pf_tracker->n; i++) {
    MATRC(pf_samps, 0, 3*i) = pf_tracker->s[i]->x[0];
    MATRC(pf_samps, 0, 3*i+1) = pf_tracker->s[i]->x[1];
    MATRC(pf_samps, 0, 3*i+2) = 1.0/(double)pf_tracker->n;
  }
}

//----------------------------------------------------------------------------

// where the particles start

void pf_samp_prior(CR_Vector *samp)
{
  double x, y, delta;

  delta = (double) pf_candidate_delta;

  // put in uniform distribution over image

  if (pf_prior_x >= pf_candidate_right) {
    pf_prior_x = delta/2;
    pf_prior_y += delta;
  }

  samp->x[0] = pf_prior_x;
  samp->x[1] = pf_prior_y;

  pf_prior_x += delta;
}

//----------------------------------------------------------------------------

// random diffusion

void pf_samp_state(CR_Vector *old_samp, CR_Vector *new_samp)
{
  sample_gaussian_CR_Matrix(pf_tracker->Covalpha, new_samp, old_samp, pf_tracker->Covsqrt);
}

//----------------------------------------------------------------------------

// deterministic drift -- static for now

void pf_dyn_state(CR_Vector *old_samp, CR_Vector *new_samp)
{
  copy_CR_Vector(old_samp, new_samp);
}

//----------------------------------------------------------------------------

// states are all in voter space

double pf_condprob_zx(void *Z, CR_Vector *samp)
{
  // using opengl-computed votes

  int ix, iy;

  if (samp->x[0] < pf_candidate_left || samp->x[0] >= pf_candidate_right || samp->x[1] < pf_candidate_top || samp->x[1] >= pf_candidate_bottom)
    return 0;

  ix = myround(samp->x[0]);
  iy = myround(samp->x[1]);

  // bilinear interpolation would be a little better

  return FLOAT_IMXY(candidate_image, ix, iy);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

double myround(double x)
{
  if (x - floor(x) >= 0.5)
    return ceil(x);
  else
    return floor(x);
}

//----------------------------------------------------------------------------

void process_tracker_command_line_flags(int argc, char **argv) 
{
  int i;

  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i],                             "-delta"))
      pf_candidate_delta = atoi(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-showpart"))
      show_particles = TRUE;
  }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
