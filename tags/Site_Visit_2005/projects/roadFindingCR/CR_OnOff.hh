//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// CR_OnOff
//----------------------------------------------------------------------------

#ifndef CR_ONOFF_DECS

//----------------------------------------------------------------------------

#define CR_ONOFF_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "opencv_utils.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))

//-------------------------------------------------
// structure
//-------------------------------------------------


//-------------------------------------------------
// functions
//-------------------------------------------------

void initialize_onoff_confidence(float, int, float);
float compute_onoff_confidence(IplImage *);
float KL_from_uniform(IplImage *);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
