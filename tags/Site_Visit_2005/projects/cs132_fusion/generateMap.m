function result = generateMap(min, max, step, type, params)
    result.vals(:,1) = min:step:max;
    result.vals(:,2) = params(1);
    result.vals(:,3) = params(2);
    result.type = type;
    result.res = step;