function result = generateSensor(offset, orientation, type, params)
    result.type = type;
    result.offset = offset;
    result.orientation = orientation;
    switch type
        case 0
            result.min_range = 0;
            result.max_range = params(1);
            result.range_uncertainty = params(2);
        case 1
            result.fov = params(1);
            result.num_pixels = params(2);
            result.focal_length = params(3);
            result.baseline = params(4);
            result.num_disp = params(5);
            result.min_range = result.baseline*result.focal_length/result.num_disp;            
            result.max_range = result.baseline*result.focal_length;            
        otherwise
            sprintf('Unknown sensor type')
    end