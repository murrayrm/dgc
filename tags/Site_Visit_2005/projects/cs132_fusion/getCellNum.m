function result = getCellNum(map, point)
    x_coord = point(1);
    temp = find(map.vals(:,1)>x_coord);
    result = -1;
    if length(temp)>0
        result = temp(1)-1;
    end
    if result<=0
        result = -1;
    end
    