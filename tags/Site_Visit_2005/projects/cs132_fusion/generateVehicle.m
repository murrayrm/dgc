function result = generateVehicle(position, orientation, position_uncertainty, orientation_uncertainty, sensors, map)
    result.position = position;
    result.orientation = orientation;
    result.position_uncertainty = position_uncertainty;
    result.orientation_uncertainty = orientation_uncertainty;
    result.sensors = sensors;
    result.map = map;