#include <math.h>
#include <sys/time.h>
#include "frames/coords.hh"
#include "trajMap.hh"
#include "rddf.hh"
#include "CMap.hh"
#include "CCorridorPainter.hh"
#include "AliceConstants.h"
using namespace std;

#ifndef _REACTIVEPLANNER_HH_
#define _REACTIVEPLANNER_HH_

#define INTERNAL_TRAJ_SPACING 0.5 //  m
#define POINTS_PER_ARC 256
#define POINTS_WIDE 7
#define LINES_LONG 6
#define LINES_DEEP 3
#define DISTANCE_BETWEEN_LINES 12.  //  m/s
#define MIN_SPEED 1.
#define MAX_WIDTH 15. //  m
#define CUTOFF 0.1 //  m/s

#define MAX_ACCEL 2.      //  m/s^2
#define MAX_DECEL (-3.)   //  m/s^2
#define MAX_LAT_ACCEL 3.  //  m/s^2
#define TURNING_RADIUS 8. //  m

#define INDEX2(i, j) (POINTS_WIDE * (i) + (j) + 1)
#define INDEX2a(i, j) (POINTS_WIDE * (i) + (j))
#define INDEX3(i, j, k) (4 * POINTS_PER_ARC * (i) + 4 * (j) + (k))
#define INDEX4(i, j, f, k) (POINTS_WIDE * POINTS_WIDE * (f) * (2 *\
    LINES_LONG - (f) - 1) / 2 + POINTS_WIDE * POINTS_WIDE * (i) + POINTS_WIDE *\
    (j) + (k) + LINES_DEEP * POINTS_WIDE)

class reactivePlanner
{
  private:
    int m_m[LINES_DEEP * POINTS_WIDE * (2 - POINTS_WIDE * (LINES_DEEP - 2 *
        LINES_LONG + 1)) / 2],
        m_n[LINES_DEEP * POINTS_WIDE * (2 - POINTS_WIDE * (LINES_DEEP - 2 *
        LINES_LONG + 1)) / 2],
        m_p[LINES_DEEP * POINTS_WIDE * (2 - POINTS_WIDE * (LINES_DEEP - 2 *
        LINES_LONG + 1)) / 2], b[LINES_LONG], m_layer, m_g;
    NEcoord points[LINES_LONG * POINTS_WIDE + 1];
    double m_speed0, theta[LINES_LONG + 1], *m_arcs,
        m_r[LINES_DEEP * POINTS_WIDE * (2 - POINTS_WIDE * (LINES_DEEP - 2 *
        LINES_LONG + 1)) / 2],
        m_d[LINES_DEEP * POINTS_WIDE * (2 - POINTS_WIDE * (LINES_DEEP - 2 *
        LINES_LONG + 1)) / 2];
    CTraj *traj;
    RDDF *m_rddf;
    CMap *m_map;

    /*! Produces lines of points perpendicular to the RDDF track line.
        Also gives the heading of points in each line*/
    void pickPoints();

    void makeBundles();
    /*! Returns a pointer to a traj made up of 2 circular arcs that smoothly
        connect two unit vectors.  Also, gives the radius of the arcs and the
        arc length between points.*/
    void makeArc(int index, NEcoord p0, double theta0, NEcoord p1,
        double theta1);

    /*! Returns a pointer to a traj made up of 2 circular arcs that smoothly
        connect two unit vectors.  Also, gives the radius of the arcs and the
        arc length between points.*/
    void makeArc(int index, double x0, double y0, double theta0, double x1,
        double y1, double theta1);

    void pickArcs();

    void makeTrajFromArcs();
    
    void blurMap();
    
    /*! Evaluates traj through a CMap layer and returns 1 if any point in the
        traj lies in a cell with goodness below the cutoff level.  Currently is
        only defined for a layer of doubles.*/
    bool    isTrajObstructed(int index, CMap * map, int layer);
  
    /*! Evaluates traj through a CMap layer and returns the time it would take
        to complete the traj if at every point in the traj the vehicle traveled
        at the speed specified by the map cell that point falls within.  Only
        defined for a layer of doubles.*/
    double  evalCost(int index, CMap * map, int layer);
  public:
    reactivePlanner();

    ~reactivePlanner();

    CTraj *reactivePlanner::plan(RDDF *rddf, CMap *map, int layer,
        NEcoord &initialPoint, double initialTheta, double initialSpeed);
};

#endif
