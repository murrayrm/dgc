
#include <iostream>
#include "statefilter.hh"
#include <math.h>
using namespace std;

// detectjump declarations
int first_time = 1;  // flag for running first time through
double last_update_time; // time of last update (for getting delta-t)
double n_last, e_last; // previous northing/easting
//extern int use_gps; // flag for throwing out gps 

// state machine declarations
int gps_err_state = GPS_NORMAL;

// magfilter definitions
double mag_yaw_err = .43;  // magnetometer yaw error
double mag_pk = 0;       // magnetometer  mean sq err
double mag_pk1=0;     // mean sq err coeff
double mag_bk;       // magnetometer kalman gain
double yk;  // current mag heading

// magnetometer heading mean sq. err (est)
double mag_sigma = pow(3 * M_PI / 180, 2);
// gps heading mean sq. err (est)
double gps_sigma = pow(.5 * M_PI / 180, 2);    

// harshness decs
double prate_max = 0;
double rrate_max = 0;

/*
// dev_reject declarations
int counter = 1;  // how many times have we run?
double buffer[NUM_ELEMENTS]; // buffer to hold elements 
int buff_ptr = 0; // pointer for element in the buffer
double pn, pe; // previous northing and eastings 
double avg, var, dev; // mean, variance, std deviation
double sigma; // Xn - avg
*/

// magfilter
// runs a single state kalman filter to estimate the error in the 
// mag heading reading
void magfilter(double magheading, double gpsheading)
{
  yk = gpsheading - magheading;

  // bound yaw error
  yk = norm_yaw(yk);

  // get mean sq err coeff
  mag_pk1 = mag_pk + mag_sigma;
  
  // update gain from mean sq err
  mag_bk = mag_pk / (mag_pk + gps_sigma);

  // get mean sq err
  mag_pk = mag_pk1 - mag_bk * mag_pk1;

  // get next estimate of the error
  mag_yaw_err = mag_yaw_err + mag_bk * (yk - mag_yaw_err);

  return;
}

// norm_yaw
// bounds yaw by -pi to pi
double norm_yaw(double yaw)
{
  while ((yaw > M_PI) || (yaw < - M_PI)){
    if (yaw > M_PI)
      yaw -= M_PI * 2;
    else if (yaw < - M_PI)
      yaw += M_PI * 2;
  }
 
  return yaw;
}

// detect_jump
// checks if there's been a jump in the GPS data by looking at the heading 
// angle and comparing it to the displacement angle.  If there's been a 
// jump, then return the magnitude of that jump, otherwise return 0
double detect_jump(double cur_n, double cur_e, double update_time, 
	       VState_GetStateMsg vehstate)
{
  double angle; // angle between heading and the displacement vector
  double magnitude; // magnitude of the displacement vector
  double jump; // answer to return
  double dn, de; // displacement between last point and this one
  double product; // the dot product between the heading and the displacement

  if (first_time){
    first_time = 0;
    }
  else{
    dn = cur_n - n_last;
    de = cur_e - e_last;

    // normalize displacement vector
    magnitude = sqrt(dn * dn + de * de);

    // take dot product between this displacement and the heading
    product = (dn * cos(vehstate.Yaw) + de * sin(vehstate.Yaw)) / magnitude;
    angle = acos(product);
   
    if (angle >= ANGLE_CUTOFF)
      {
	jump = magnitude;
	//use_gps = 1;
      }
    else{
      //use_gps = 1;
      jump = 0;
    }
  }

  n_last = cur_n;
  e_last = cur_e;
  last_update_time = update_time;

  return jump;
}

// detect_reset
// resets the jump detection so that it doesn't think we've jumped horribly
// between gps jumps
void detect_reset()
{
  first_time = 1;

  return;
}


// gps_err_reset
// If there's been a jump, the gps error value in the kfilter is reset higher
// and then allowed to come down to a low defalt value as an exponential curve
double gps_err_reset(double gps_err, double gps_jump, double gps_pdop)
{
  gps_err =sqrt(gps_err) * EARTH_RADIUS; // convert gps error back into meters 
  
  double e_weight = gps_pdop / 1.5;
  e_weight = ERROR_WEIGHT;

  if (gps_jump > gps_err) {
    gps_err = gps_jump * e_weight;
  }else{
    gps_err *= ERROR_FALLOFF;
  }

  if (gps_err < GPS_DEFAULT_ERROR){
    gps_err = GPS_DEFAULT_ERROR;
  }
    
    

  gps_err = pow(gps_err / EARTH_RADIUS, 2);

  return gps_err;
}

// angular rate harshness detection
double p_harsh(double prate)
{
  if (prate >= prate_max)
    prate_max = prate;
  else
    prate_max *= p_harsh_ratio;

  return prate_max;
}

double r_harsh(double rrate)
{
  if (rrate >= rrate_max)
    rrate_max = rrate;
  else
    rrate_max *= r_harsh_ratio;

  return rrate_max;
}

int state_validity(VState_GetStateMsg curstate)
{
  int validity = STATE_VALID;
  
  int notnum = (isnan(curstate.Northing) || isnan(curstate.Easting) 
		|| isnan(curstate.Yaw) || isnan(curstate.Speed)
		|| isnan(curstate.Pitch) || isnan(curstate.Roll));

  // wrap everything in a not a number check
  if ((curstate.Speed >= STATE_SPEED_KILL) ||notnum) {
    validity = STATE_KILL;
  }else{
    if ((curstate.Northing <= EQUATOR_N) || (curstate.Easting <= 0)){
      validity = STATE_WAIT;
    }
  }

  return validity;
}
