//this begins the transmission header file
#define TRUE 1
#define FALSE 0
#define ERROR -1

using namespace std;
//transmission gears to pass to trans_setposition(int)
#define NEUTRAL 0
//#define FREE   //no longer used
#define FIRST 1
//#define SECOND //no longer used
#define THIRD 3
#define REVERSE -1

//ignition states to pass to ignition_setposition(int)
#define RUN 2
#define START 1
#define OFF 0

int trans_open(int);
int trans_setposition(int);
int trans_close(void);
int ignition_setposition(int);

