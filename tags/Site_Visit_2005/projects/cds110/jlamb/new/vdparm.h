/*
 * vdparm.dd - vdrive parameter definitions
 *
 * RMM, 3 Dec 04
 *
 * This page should show as much as it can about the parameters that
 * define vdrive and the actuation subsystems.  The table is both used
 * to dynamically vary the parameters in testing and also to generate the
 * vdrive initialization file.
 *
 */

extern int steer_negCountLimit;
extern int steer_posCountLimit;
extern int acc_commanded;

/* Object ID's (offset into table) */

/* Allocate space for buffer storage */
/* (none required) */

DD_IDENT parmtbl[] = {
{1, 1, (void *)("Actuation Device Display"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 45, (void *)("J:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 55, (void *)("I:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 65, (void *)("A:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 1, (void *)("Mode:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 25, (void *)("T:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 39, (void *)("B:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 49, (void *)("S:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 59, (void *)("X:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 69, (void *)("I:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 3, (void *)("Position:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 29, (void *)("Position:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 54, (void *)("Position:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{6, 3, (void *)("Port:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{6, 29, (void *)("Port:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{6, 54, (void *)("Port:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{7, 3, (void *)("Max acc:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{7, 54, (void *)("Neg lim:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 3, (void *)("Max vel:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 54, (void *)("Pos lim:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 3, (void *)("Full stop:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{12, 3, (void *)("Position:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{12, 29, (void *)("Gear:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 3, (void *)("Port:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 29, (void *)("Port:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{15, 1, (void *)("Config file:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{15, 14, (void *)(config_file), dd_string, "%s", (char *)(sizeof(config_file)), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "config_file", -1},
{17, 1, (void *)("BACK"), dd_label, "NULL", (char *) NULL, 1, dd_prvtbl_cb, (long)(long) 0, 0, 0, Button, "", -1},
{17, 7, (void *)("QUIT"), dd_label, "NULL", (char *) NULL, 1, user_quit, (long)(long) 0, 0, 0, Button, "", -1},
{17, 13, (void *)("SAVE"), dd_label, "NULL", (char *) NULL, 1, user_save_parmtbl, (long)(long) 0, 0, 0, Button, "", -1},
{17, 19, (void *)("LOAD"), dd_label, "NULL", (char *) NULL, 1, user_load_parmtbl, (long)(long) 0, 0, 0, Button, "", -1},
DD_End};
