#ifndef LOGGER_HH
#define LOGGER_HH

#include "sn_msg.hh"
#include <iostream>
#include <iomanip>
#include <math.h>
#include <string>
#include <fstream>
#include <set>
#include "DGCutils"
#include "CLoggerTalker.hh"
#include "LoggerConfig.hh"


using namespace std;

class CLogger : public CLoggerTalker
{
public:
  CLogger(int des_skynet_key);
  ~CLogger();

  void LoggerActiveLoop();

  /** Accessors */
  string getTimestamp();
  int getDebugLevel();

  /** Mutators */
  void setDebugLevel(int value);

  int sendDebug();

private:
  void RecvThread();
  void SendThread();
  void CLogger::getCommand(char* command, char* message);
  void CLogger::catDGCDirFromFile(char* dest);

  set<string> todoSet;
  int skynet_key;

  bool logging_on;
  pthread_mutex_t logging_on_mutex;

  bool shutdown;
  pthread_mutex_t shutdown_mutex;

  int debug_level;
  pthread_mutex_t debug_level_mutex;

  ofstream todoListFile;
  pthread_mutex_t todoListFile_mutex;

  char* session_timestamp;
  pthread_mutex_t session_timestamp_mutex;

};

#endif  // LOGGER_HH
