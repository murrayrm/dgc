LOGGER_PATH = $(DGC)/projects/logger

LOGGER_DEPEND_MODULES = $(SKYNETLIB) $(DGCUTILS)

LOGGER_DEPEND_SOURCES = \
	$(LOGGER_PATH)/CLogger.hh \
	$(LOGGER_PATH)/CLogger.cc \
	$(LOGGER_PATH)/CLoggerClient.hh \
	$(LOGGER_PATH)/CLoggerClient.cc \
	$(LOGGER_PATH)/CLoggerTalker.hh \
	$(LOGGER_PATH)/CLoggerTalker.cc \
	$(LOGGER_PATH)/LoggerMain.cc \
        $(LOGGER_PATH)/LoggerConfig.hh

LOGGER_DEPEND = \
	$(LOGGER_DEPEND_MODULES) \
	$(LOGGER_DEPEND_SOURCES)
