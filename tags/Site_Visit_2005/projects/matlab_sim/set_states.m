function [thd, vlat, vlon, x, y, theta, phi] = set_states(state)
% Sets the named states appropriately given the state vector
% this saves having to reference things by indicies all the time
thd = state(1);
vlat = state(2);
vlon = state(3);
x = state(4);
y =  state(5);
theta = state(6);
phi = state(7);