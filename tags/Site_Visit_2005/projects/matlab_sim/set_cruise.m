% set control constants for cruise controller
v_look_in = 0:2:16;
v_throt_out = [-1 .075 .11 .13 .14 .155 .165 .2 .25];
V_ref = 3;
Kp_v = .5;
Kd_v = 1;
