function dstate = planar_dynamics(state, F_lon, F_lat, steercmd)
% This function contains the dynamics of the bicycle model that is being
% used to model the motion of the vehicle.  The state it uses is a
% 6-vector:
% (thd, v_lon, v_lat, x, y, theta)
% The forces of the vehicle are precalculated based on the current vehicle
% state, as is the steering angle (this allows us to use different models
% for the steering and longitudinal dynamics)

global L_f L_r I_VEH MASS  

dstate = zeros(7, 1);

[thd, vlat, vlon, x, y, theta, phi] = set_states(state);

% thetad
dstate(1) = (L_f  * F_lon(1) * phi  + L_f * F_lat(1) - L_r * F_lat(2)) / I_VEH;
%dstate(1) = (L_f * F_lat(1) - L_r * F_lat(2)) / I_VEH;

% v_lat
dstate(2) = (F_lon(1) * phi + F_lat(1) + F_lat(2)) / MASS - thd * vlon;

% v_lon
dstate(3) = (F_lon(1) + F_lon(2) - F_lat(1) * phi) / MASS + vlat * thd;
%dstate(3) = (F_lon(1) + F_lon(2)) / MASS;

% x
dstate(4) = vlon * cos(theta) - vlat * sin(theta);

% y
dstate(5) = vlon * sin(theta) + vlat * cos(theta);

% theta
dstate(6) = thd;

% phi
dstate(7) = steering(phi, steercmd);