#include <unistd.h>
#include <iostream>
#include <fstream>
#include "DGCutils"
#include "CTimberClient.hh"
#include "GlobalConstants.h"
#include "RefinementStage.h"
#include "reactiveTabSpecs.hh"
#include "reactivePlanner.hh"
#include "reactiveModule.hh"
using namespace std;

int QUIT_PRESSED = 0;

extern int           NO_SPARROW_DISP;
extern int           NOSTATE;
extern int           NOPLAN;
extern int           NOMM;
extern int           SIMPLEUPDATE;
extern unsigned long SLEEP_TIME;
extern int					 STATICMAP;

//This is the size of the buffer allocated for receiving map-deltas
//(used as the buffer size in the call to skynet.get_msg(...) when
//receiving map deltas over skynet)
//NOTE: MAX_DELTA_SIZE is a defined const in several places in fusion
//mapper and the associated feeder modules, and its value is DIFFERENT
//in each place, as it is really used as the skynet message receive buffer
//size in the mapper related code - this has been raised as a bug
#define MAX_DELTA_SIZE				1000000
#define MIN_PLANNING_CYCLETIME_US		500000
#define PLANNING_LOOKAHEAD					0.
#define PROXIMITY_TO_TRAJ_TO_LOOKAHEAD 10

//#define NO_LOOKAHEAD

reactiveModule::reactiveModule(int sn_key)
  : CSkynetContainer(MODreactive, sn_key), m_rddf("rddf.dat"),
        m_bReceivedAtLeastOneDelta(false), CTimberClient(timber_types::reactivePlanner),
        CModuleTabClient(&m_input, &m_output)
{
	m_pMapDelta = new char[MAX_DELTA_SIZE];

#warning "these shouldn't be hard coded"
	if(STATICMAP == 1)
	{
		m_map.initMap(0.0 ,0.0, 2500, 1000, 0.2, 0.2, 0 );
		// Add the speed limit layer to the map
		m_mapLayer = m_map.addLayer<double>(-3.0, -2.0);
		m_map.loadLayer<double>(m_mapLayer, "cmap", true);
		m_blurLayer = m_map.addLayer<double>(-3.0, -2.0);
	}
	else
	{    // constants in GlobalConstants.h
	  m_map.initMap(CONFIG_FILE_DEFAULT_MAP);
	  // Add the speed limit layer to the map
	  m_mapLayer = m_map.addLayer<double>(CONFIG_FILE_COST);
	  m_blurLayer = m_map.addLayer<double>(CONFIG_FILE_COST);
	}
	DGCcreateMutex(&m_mapMutex);
	DGCcreateMutex(&m_deltaReceivedMutex);
	DGCcreateCondition(&m_deltaReceivedCond);
}

reactiveModule::~reactiveModule() 
{
	delete m_pMapDelta;
	DGCdeleteMutex(&m_mapMutex);
	DGCdeleteMutex(&m_deltaReceivedMutex);
	DGCdeleteCondition(&m_deltaReceivedCond);
  if (outfile.is_open())
    outfile.close();
}

void reactiveModule::getMapDeltasThread()
{
  // The skynet socket for receiving map deltas (from fusionmapper)
	int mapDeltaSocket = m_skynet.listen(SNfusiondeltamap, SNfusionmapper);
	if(mapDeltaSocket < 0)
		cerr << "CPlannerModule::getMapDeltasThread(): skynet listen returned error" << endl;

	while(true)
	{
		int deltasize;
		RecvMapdelta(mapDeltaSocket, m_pMapDelta, &deltasize);
    cerr << "Received mapdelta number: " << ++m_input.numDeltas << endl;
		m_input.cumulativeDeltaSize += deltasize;

 		DGClockMutex(&m_mapMutex);
		m_map.applyDelta<double>(m_mapLayer, m_pMapDelta, deltasize);
		cerr << "Applied mapdelta" << endl;
		blurMap();
		cerr << "Blurred map." << endl;
 		DGCunlockMutex(&m_mapMutex);

		// set the condition to signal that the first delta was received
		if(!m_bReceivedAtLeastOneDelta)
      DGCSetConditionTrue(m_bReceivedAtLeastOneDelta, m_deltaReceivedCond,
        m_deltaReceivedMutex);
	}
}

void reactiveModule::planningLoop(void) 
{
#warning "same map used here, even though two physical ones exist"
  reactivePlanner rP;
	int trajSocket;
	CTraj *traj = new CTraj(3);
	NEcoord pos;
	unsigned long long time1, time2;

	DGCgettime(time1);

	bool bSentTraj = false;

	if (NOMM)
		trajSocket     = m_skynet.get_send_sock(SNtraj);
	else
		trajSocket     = m_skynet.get_send_sock(SNreactiveTraj);

  cout << "Waiting for a delta map" << endl;

  // don't try to plan until at least a single mapDelta is received
  DGCWaitForConditionTrue(m_bReceivedAtLeastOneDelta, m_deltaReceivedCond,
      m_deltaReceivedMutex);


  while( !QUIT_PRESSED )
	{
	  bool logging_enabled = getLoggingEnabled();
	  if (logging_enabled)
    {
      string logging_location = getLogDir() +  (string) "reactiveLog";
      if (checkNewDirAndReset())
	    {
	      if (outfile.is_open())
	        outfile.close();
  	    outfile.open(logging_location.c_str());
      }
  	}
	
		if(!NOSTATE) 
		{
		  UpdateState();
		}

#ifdef NO_LOOKAHEAD
		DGClockMutex(&m_mapMutex);
		cout << "starting to plan" << endl;
		pos = NEcoord(m_state.Northing_rear(), m_state.Easting_rear());
		traj = rP.plan(&m_rddf, &m_map, m_blurLayer, pos, m_state.Yaw, m_state.Speed2());
		cout << "finished plan" << endl;
		DGCunlockMutex(&m_mapMutex);
#else
		if(!bSentTraj)
		{
			DGClockMutex(&m_mapMutex);
			cout << "starting to plan" << endl;
		  pos = NEcoord(m_state.Northing_rear(), m_state.Easting_rear());
		  traj = rP.plan(&m_rddf, &m_map, m_blurLayer, pos, m_state.Yaw, m_state.Speed2());
			cout << "finished plan" << endl;
			DGCunlockMutex(&m_mapMutex);
		}
		else
		{
			VehicleState lookaheadState = m_state;
			double lookaheadDist = PLANNING_LOOKAHEAD * m_state.Speed2();
			int closestpoint = traj->getClosestPoint(m_state.Northing_rear(), m_state.Easting_rear());
			if( hypot(traj->getNorthing(closestpoint) - m_state.Northing,
								traj->getEasting(closestpoint) - m_state.Easting) <
								PROXIMITY_TO_TRAJ_TO_LOOKAHEAD)
			{
				closestpoint += lround( lookaheadDist / (LINES_LONG * 
				    DISTANCE_BETWEEN_LINES) * (double)traj->getNumPoints());

				if(closestpoint >= traj->getNumPoints())
            closestpoint = traj->getNumPoints()-1;

				lookaheadState.Northing = traj->getNorthingDiff(closestpoint, 0);
				lookaheadState.Easting  = traj->getEastingDiff (closestpoint, 0);
				lookaheadState.Vel_N    = traj->getNorthingDiff(closestpoint, 1);
				lookaheadState.Vel_E    = traj->getEastingDiff (closestpoint, 1);
				lookaheadState.Acc_N    = traj->getNorthingDiff(closestpoint, 2);
				lookaheadState.Acc_E    = traj->getEastingDiff (closestpoint, 2);
				lookaheadState.Yaw      = atan2(lookaheadState.Vel_E, lookaheadState.Vel_N);
				lookaheadState.YawRate  = (lookaheadState.Vel_N*lookaheadState.Acc_E -
																	 lookaheadState.Acc_N*lookaheadState.Vel_E) /
																	 lookaheadState.Speed2()/lookaheadState.Speed2();
			}

			DGClockMutex(&m_mapMutex);
			cout << "starting to plan" << endl;
		  pos = NEcoord(lookaheadState.Northing, lookaheadState.Easting);
		  traj = rP.plan(&m_rddf, &m_map, m_blurLayer, pos, lookaheadState.Yaw,
		      lookaheadState.Speed2());
			cout << "finished plan" << endl;
			DGCunlockMutex(&m_mapMutex);
		}
#endif
      //Use Dima's function to make the output traj C2
      rS.MakeTrajC2(traj);

			SendTraj(trajSocket, traj);
			cerr << "Sent traj number: " << ++m_input.numTrajsSent << endl;
      if (getLoggingEnabled())
        {
          traj->print(outfile);
        }

			double n   = traj->getNorthingDiff(0, 0);
			double nd  = traj->getNorthingDiff(0, 1);
			double ndd = traj->getNorthingDiff(0, 2);
			double e   = traj->getEastingDiff (0, 0);
			double ed  = traj->getEastingDiff (0, 1);
			double edd = traj->getEastingDiff (0, 2);
			cerr <<   "desired a: "   << (nd*ndd + ed*edd)/hypot(nd,ed)
					 << ", desired phi: " << asin( VEHICLE_WHEELBASE*(nd*edd - ndd*ed)/
					                               pow(nd*nd+ed*ed, 1.5) ) << endl;

			bSentTraj = true;

			DGCgettime(time2);
      if((m_input.runtimeUs = time2 - time1) < MIN_PLANNING_CYCLETIME_US)
				usleep(MIN_PLANNING_CYCLETIME_US - (time2 - time1));
			DGCgettime(time1);

	}
}

/*Grows obsticles in a square.  This function will probably be depreciated once
  growing is implemented in fusionMapper.*/
void reactiveModule::blurMap()
{
  int i, j, k, f, r, c, n, e;

  n = (int)(BLUR_SIZE / m_map.getResRows() + 0.5);
  e = (int)(BLUR_SIZE / m_map.getResCols() + 0.5);
  /*if(n > 0 || e > 0)
  {*/
    double goodness;

    r = m_map.getNumRows() - n;
    c = m_map.getNumCols() - e;

    for(i = n; i < r; i++)
      for(j = e; j < c; j++)
      {
        goodness = m_map.getDataWin<double>(m_mapLayer, i, j);
        if(goodness > 0.1)
          for(k = i - n; k <= i + n; k++)
            for(f = j - e; f <= j + e; f++)
              goodness = fmin(goodness, m_map.getDataWin<double>(m_mapLayer, k,
                  f));
        m_map.setDataWin<double>(m_blurLayer, i, j, goodness);
      }
  /*}
  else
    m_map.copyLayer<double>(&m_map, m_mapLayer, m_blurLayer, 0);*/
}
