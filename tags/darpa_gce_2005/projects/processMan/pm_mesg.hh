/**
 * pm_mesg.hh
 * Contains structs of the various message type that will be sent over
 * for the monitoring.
 * $Id$
 */

#ifndef PM_MESG_HH
#define PM_MESG_HH

enum pm_mtype
  {
    WIL = 1,  // who is leader
    IAL,      // i am leader 
    HEARTBEAT, // heartbeat 
    EXPIRED,
    END_OF_MTYPE
  };


/**
 * struct representing messages sent by the RingMonitor section of
 * process management. The mesgType determines what kind of message is
 * being sent. The data contained in address is dependent on the
 * message type.   
 */
struct pmRmMesg
{
  pm_mtype mesgType;
  struct sockaddr_in addr;
};


#endif  // PM_MESG_HH
