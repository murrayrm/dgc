#! /bin/bash

# set the bob file
FILE=../../util/RDDF/routes/DARPA_RACE_RDDF_ZERO_BASED.bob

echo "Analyzes velocities in last year's race RDDF"
echo " * First plot is max speed (in m/s), as specified in the RDDF, versus distance"
echo "   along the corridor (in m) for last year's race."
echo " * Second plot is how long it would take to complete last year's race, if we at"
echo "   all times drove at the min of (rddf max speed, our vehicle's max speed),"
echo "   versus our vehicles max speed."

# in gawk script:
# n is northing
# e is easting
# s is speed (m/s)
# d is distance from this pt to the last one
# tot is sum of d over all lines
cat $FILE | gawk 'BEGIN {OFS="  \t"} {if (s != 0) {d=(($2-e)^2 + ($3-n)^2)^.5; tot+=d; print tot,d,s;} e=$2; n=$3; s=$5;}' > proc
echo "set xlabel 'distance along corridor (m)'; set ylabel 'speed (m/s)'; plot 'proc' using 1:3 with lines title '$FILE'" | gnuplot -persist

{
for speed in `seq 5 1 30`; do
    echo -ne "$speed  \t"
    export max=$speed
    cat proc | gawk 'BEGIN {maxs=ENVIRON["max"]; OFS="  \t"} {if ($3 > maxs) s=maxs; else s=$3; tot+=$2/s} END {print tot/60/60}'
done;
} > times

echo "set yrange [0:15]; set xrange [0:30]; set xlabel 'vehicles max speed (m/s)'; set ylabel 'time (hours)'; plot 'times' with lines title '$FILE'" | gnuplot -persist

