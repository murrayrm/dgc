#!/bin/bash

# This script switches the grid file between containing only
# the local IP and containing also the race computers IPs
# NOTE: This script will create the ~/local dir and copy the 
#       necessary files from dgc/local/ into that directory if it 
#       didn't already existed.
cd ~

if [ ! -d dgc ]; then 
  echo Error, you must have the DGC code repository checked out to ~/dgc
  echo Correct this and then run this script again.
  exit 0
fi

if [[ ! -d local || ! -f local/ip || ! -f local/grid ]]; then
  echo You didn\'t have a local directory in $HOME or at least no ip and grid
  echo files in it. Setting up your grid and ip files now...
  mkdir -p local
  cp dgc/local/ip local
  cp dgc/local/grid* local 
  echo Done setting up your ip and grid files for MTA. Defaulting to 
  echo the configuration with the race computers included in the grid files 
  echo \(linking grid to grid_shop\).
  cd local
  ln -s grid_shop grid
  echo Finished setting up your local dir. Exiting...
  exit 0
fi

# And if this is not the first time this script is executed. Keep going

cd local

# Calculate the number of words in grid file and assume, if they're more
# than two, that it's the shop grid file
NBR=$(cat grid | wc | awk '{ print $1 }')

# if the grid file contains more than two words, it must be the one
# with all the race computer IPs in, then we want to switch to the local one
if [[ $NBR -gt 2 ]]; then 
  echo Setting MTA configuration to LOCAL ONLY
  rm -f grid
  ln -s grid_local grid
else
  # otherwise, we have the local one and wants to use the one for the shop now
  echo Setting MTA configuration to WITH RACE COMPUTERS
  rm -f grid
  ln -s grid_shop grid
fi

echo Done!
exit 0

