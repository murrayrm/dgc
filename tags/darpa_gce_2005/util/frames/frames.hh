// Frame Transformation Class
// Used to transform coordinates from a body-fixed frame to
// a fixed reference (nav) frame
// 
// Revision History
// H Huang 25 Jan 04
// Created

#ifndef FRAMES_HH
#define FRAMES_HH

#include "rot_matrix.hh"

class frames
{
private:
  rot_matrix B2N;  // body to nav rotation matrix used to rotate out of body frame into nav frame
  XYZcoord offset;  // offset of sensor viewpoint to vehicle origin 
  rot_matrix S2B; // rotation matrix from sensor viewing angle to vehicle body frame
  NEDcoord UTMoffset; // UTM coordinate of vehicle origin

public:
  // constructors
  
  // creates identity transformation (no rotations, no translations,
  // sensor origin is coincident with vehicle origin which is also
  // coincident with UTM origin
  frames(); 

  // creates frame transform already initialized with sensor offset
  // vehicle to UTM transformation is still assumed to be identity
  // (must set state) NOTE: REMEMBER THAT THE COORDINATE FRAMES ARE
  // X-FORWARD, Y-RIGHT, Z-DOWN this means that if the sensor is 2m
  // above, 3m to the front of, and 1m to the left of the vehicle
  // origin it's offset would be (3, -1, -2)
  frames(XYZcoord sensor_offset, double sensor_pitch, double sensor_roll, double sensor_yaw); 

  // initialize the sensor's point of view relative to the body frame
  // only used if you didn't initialize the frames on declaration,
  // same caveats apply as for constructor
  void initFrames(XYZcoord sensor_offset, double sensor_pitch, double sensor_roll, double sensor_yaw);

  // sets the body state of the vehicle to be used in transformations
  // from vehicle's body-fixed frame to UTM frame.
  void updateState(NEDcoord vehPos, float Pitch, float Roll, float Yaw); 
  
  // transformations
  // All transformations are conducted as follows: first the point is
  // rotated out of the local frame to a frame offset from the desired
  // frame but with parallel axes, then the offset is added to produce
  // coordinates in the desired frame

  // transform a given point in the sensor's coordinate frame to UTM
  // frame
  NEDcoord transformS2N(XYZcoord point);

  // transform a given point in the body frame to the nav frame
  NEDcoord transformB2N(XYZcoord point);

  // transform a given point in the sensor's coordinate frame to the
  // body frame
  XYZcoord transformS2B(XYZcoord point);

};

#endif
