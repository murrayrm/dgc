#ifndef SPARROWHWAK_FUNCTORS_HH
#define SPARROWHWAK_FUNCTORS_HH

/*
**
** SparrowHawk abstract base callback holder
**
*/
class CSparrowHawkCallback
{
 public:
  CSparrowHawkCallback() {}
  virtual ~CSparrowHawkCallback() {}

  virtual void operator()() { }
};

template<class T>
class CSparrowHawkCallbackFnc : public CSparrowHawkCallback
{
 public:
  CSparrowHawkCallbackFnc(T t) : m_t(t) {}
  virtual void operator()() { m_t(); }
 private:
  T m_t;
};

template<class T, class A1>
class CSparrowHawkCallbackFnc1 : public CSparrowHawkCallback
{
 public:
  CSparrowHawkCallbackFnc1(T t, A1 a1) : m_t(t), m_a1(a1) {}
  virtual void operator()() { m_t(m_a1); }
 private:
  T m_t;
  A1 m_a1;
};

template<class T>
class CSparrowHawkCallbackT : public CSparrowHawkCallback
{
 public:
  CSparrowHawkCallbackT(T *t, void (T::*fnc)()) : m_t(t), m_fnc(fnc) {}
  virtual void operator()() { (m_t->*m_fnc)(); }
 private:
  T *m_t;
  void (T::*m_fnc)();
};

template<class T, class A1>
class CSparrowHawkCallbackT1 : public CSparrowHawkCallback
{
 public:
  CSparrowHawkCallbackT1(T *t, void (T::*fnc)(A1), A1 a1)
    : m_t(t), m_fnc(fnc), m_a1(a1) {}
  virtual void operator()() { (m_t->*m_fnc)(m_a1); }
 private:
  T *m_t;
  void (T::*m_fnc)(A1);
  A1 m_a1;
};

/*
**
** SparrowHawk abstract base setter callback holder
**
*/
class CSparrowHawkSetter
{
 public:
  CSparrowHawkSetter() {}
  virtual ~CSparrowHawkSetter() {}
  
  virtual void operator()(void *pVariable, void *pNewValue) =0;
};

template<class BT>
class CSparrowHawkSetterBT : public CSparrowHawkSetter
{
 public:
  CSparrowHawkSetterBT() {}
  
  virtual void operator()(void *pVariable, void *pNewValue) =0;
};

template<class BT, class T>
class CSparrowHawkSetterFnc : public CSparrowHawkSetterBT<BT>
{
 public:
  CSparrowHawkSetterFnc(T t) : m_t(t) {}
  virtual void operator()(void *pVariable, void *pNewValue) {
    m_t((BT*)pVariable, *(BT*)pNewValue);
  }
 private:
  T m_t;
};

template<class BT, class T>
class CSparrowHawkSetterT : public CSparrowHawkSetterBT<BT>
{
 public:
  CSparrowHawkSetterT(T *t, void (T::*fnc)(BT *pVariable, BT new_value))
    : m_t(t), m_fnc(fnc) {}
  virtual void operator()(void *pVariable, void *pNewValue) {
    (m_t->*m_fnc)((BT*)pVariable, *(BT*)pNewValue);
  }
 private:
  T *m_t;
  void (T::*m_fnc)(BT*, BT);
};

template<class BT, class T, class A1>
class CSparrowHawkSetterT1 : public CSparrowHawkSetterBT<BT>
{
 public:
  CSparrowHawkSetterT1(T *t, void (T::*fnc)(BT *pVariable, BT new_value, A1), A1 a1)
    : m_t(t), m_fnc(fnc), m_a1(a1) {}
  virtual void operator()(void *pVariable, void *pNewValue) {
    (m_t->*m_fnc)((BT*)pVariable, *(BT*)pNewValue, m_a1);
  }
 private:
  T *m_t;
  void (T::*m_fnc)(BT*, BT, A1);
  A1 m_a1;
};

/*
**
** SparrowHawk abstract base getter
**
*/
class CSparrowHawkGetter
{
 public:
  CSparrowHawkGetter() {}
  virtual ~CSparrowHawkGetter() {}
  
  virtual void operator()(void *pVariable) =0;
};

template<class BT>
class CSparrowHawkGetterBT : public CSparrowHawkGetter
{
 public:
  CSparrowHawkGetterBT() {}
  
  virtual void operator()(void *pVariable) { }
};

template<class BT, class T>
class CSparrowHawkGetterFnc : public CSparrowHawkGetterBT<BT>
{
 public:
  CSparrowHawkGetterFnc(T t) : m_t(t) {}
  virtual void operator()(void *pVariable) {
    *(BT*)pVariable = m_t();
  }
 private:
  T m_t;
};

template<class BT, class T>
class CSparrowHawkGetterT : public CSparrowHawkGetterBT<BT>
{
 public:
  CSparrowHawkGetterT(T *t, BT (T::*fnc)())
    : m_t(t), m_fnc(fnc) {}
  virtual void operator()(void *pVariable) {
    *(BT*)pVariable = (m_t->*m_fnc)();
  }
 private:
  T *m_t;
  BT (T::*m_fnc)();
};

template<class BT, class T, class A1>
class CSparrowHawkGetterT1 : public CSparrowHawkGetterBT<BT>
{
 public:
  CSparrowHawkGetterT1(T *t, BT (T::*fnc)(A1), A1 a1)
    : m_t(t), m_fnc(fnc), m_a1(a1) {}
  virtual void operator()(void *pVariable) {
    *(BT*)pVariable = (m_t->*m_fnc)(m_a1);
  }
 private:
  T *m_t;
  BT (T::*m_fnc)(A1);
  A1 m_a1;
};


#endif //SPARROWHWAK_FUNCTORS_HH
