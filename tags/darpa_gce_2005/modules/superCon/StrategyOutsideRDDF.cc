//SUPERCON STRATEGY TITLE: Outside RDDF - ANYTIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyOutsideRDDF.hh"
#include "StrategyHelpers.hh"

void CStrategyOutsideRDDF::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE (this MUST remain here)
  currentStageName = outside_rddf_stage_names_asString( outside_rddf_stage_names(stgItr.nextStage()) );

  switch( stgItr.nextStage() ) {
    
  case s_outsiderddf::estop_pause: 
    //we are currently outside the RDDF - estop pause before taking action
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      //THERE SHOULD BE NO CHECKSFOR OTHER STRATEGIES AT THIS POINT - COULD
      //POTENTIALLY CAUSE UNDESIRED BEHAVIOUR
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "OutsideRDDF - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;
    
  case s_outsiderddf::broaden_rddf: 
    //wait until Alice is stationary, and then broaden the RDDF so that she
    //can return to the 'real' RDDF (as she will no longer be confused/trapped
    //outside of the real RDDF, she will think she is actually still inside
    //the RDDF)
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */      
      //THERE SHOULD BE NO CHECKSFOR OTHER STRATEGIES AT THIS POINT - COULD
      //POTENTIALLY CAUSE UNDESIRED BEHAVIOUR
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->superConPaused == false ) {
	//NOT yet superCon paused - adrive has not yet processed the command
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "WARN: Alice not yet paused, cannot broaden RDDF - will poll");
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );
      }

      if( m_pdiag->aliceStationary == false ) {
	
	skipStageOps == true;
	SuperConLog().log( STR, WARNING_MSG, "WARN: Alice not yet stationary - will poll");
	checkForPersistLoop( this, persist_loop_waitstationary, stgItr.currentStageCount() );	
      }
	
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_expandRDDFwidth();
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "OutsideRDDF - RDDF broadened as Alice was outside the RDDF" );	
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "OutsideRDDF - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;    

    
  case s_outsiderddf::trans_nominal: 
    //sent command to fusionMapper to broaden the RDDF, wait for confirmation
    //that RDDF broadening is complete, and then transition to the nominal
    //strategy
    { 
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      if( m_pdiag->broaden_rddf_complete == false ) {
	
	skipStageOps = true;
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "OutsideRDDF - confirmation of RDDF-broadening NOT yet received from Fmapper -> will poll" );
	checkForPersistLoop( this, persist_loop_broadenRDDF, stgItr.currentStageCount() );	
      }
      
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	//un-pause Alice as the GPS re-acquisition process has been
	//completed --> NOMINAL
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - confirmation of map-clearing received from fmapper" );
	transitionStrategy( StrategyNominal, "GPSreAcq - Strategy complete --> nominal" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;    

    /* END OF STRATEGY */

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyOutsideRDDF::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyOutsideRDDF::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: OutsideRDDF - default stage reached!");
    }
  }
}


void CStrategyOutsideRDDF::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "OutsideRDDF - Exiting");
}

void CStrategyOutsideRDDF::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset(); //reset the stage iterator
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "OutsideRDDF - Entering");
}
