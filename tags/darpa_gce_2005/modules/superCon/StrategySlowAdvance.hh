#ifndef STRATEGY_SLOWADVANCE_HH
#define STRATEGY_SLOWADVANCE_HH

//SUPERCON STRATEGY TITLE: Slow Advance - SOMETIME STRATEGY
//HEADER FILE (.hh)
//DESCRIPTION: 

#include "Strategy.hh"
#include "sc_specs.h"

namespace s_slowadvance {

#define SLOWADVANCE_STAGE_LIST(_) \
  _(max_speedcap, = 1) /*first entry must = 1 (as this is referenced against NEXTstage */ \
  _(slowadvance_wait, )
DEFINE_ENUM(slowadvance_stage_names, SLOWADVANCE_STAGE_LIST)

}

using namespace std;
using namespace s_slowadvance;

class CStrategySlowAdvance : public CStrategy
{

public:

  /** CONSTRUCTORS **/
  CStrategySlowAdvance() : CStrategy() {}  

  /** DESTRUCTOR **/
  ~CStrategySlowAdvance() {}


  /** MUTATORS **/
  void stepForward(const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface);
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

  /** HELPER METHODS **/
  bool checkPlnFailedNoTraj( bool checkBool, const SCdiagnostic *pdiag, const char* StrategySpecificStr );
};

#endif
