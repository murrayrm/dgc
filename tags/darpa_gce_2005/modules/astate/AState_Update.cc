/* AState.cc - new vehicle state estimator
 *
 * JML 09 Jun 05
 *
 * */

#warning Earth Radius is based on Latitude -- we should really use a dynamically changing variable for most of the places we refer to it.  This is not critical and is large enough change that we probably dont want to mess with it.

#include "AState.hh"
#include "Matrix.hh"

#define square(x) ((x)*(x))

double filter(double *oldVals, double accel);

void AState::updateStateThread()
{
  unsigned long long stateTime = 0;

  //IMU data read in
  unsigned long long imuTime;
  Matrix fbSum(3);
  Matrix wb_ibSum(3);
  double dtSum = 0.0;
  int newIMU = 0;


  //GPS data read in
  double gpsLat;
  double gpsLon;
  unsigned long long gpsTime;
  Matrix gpsVg(3);
  Matrix gpsVn(3);
  int gps3D;
  int newGPS = 0;
  GisCoordLatLon latsForGPS;
  GisCoordUTM utmForGPS;

  //GPS nov data read in
  double novLat;
  double novLon;
  unsigned long long novTime;
  Matrix novVg(3);
  Matrix novVn(3);
  int newNovP = 0;
  int newNovV = 0;
  GisCoordLatLon latsForNov;
  GisCoordUTM utmForNov;
  int isNovMsgPos;
  double novVelLatency;

  //OBD data read in
  unsigned long long obdTime;
  int newObd = 0;
  Matrix obdVb(3);
  Matrix obdVn(3);
 
  //Internal state estimate
  double lat = 0.0;
  double lon = 0.0;
  double height = 0.0;
  Matrix vn(3);
  double roll = 0.0;
  double pitch = 0.0;
  double yaw = 0.0;
  alpha = 0.0;
  Matrix Cne(3,3);
  Matrix Cnb(3,3);
  Matrix Cgn(3,3);
  Matrix Cbi(3,3);
  makeNav2Geo(Cgn,alpha);

  makeBody2Nav(Cbi, (ROLL_IMU), (PITCH_IMU), (YAW_IMU));

  //Variables necessary for initialization
  double gpsLatSum = 0.0;
  double gpsLonSum = 0.0;
  double gpsHeightSum = 0.0;
  int goodGpsCnt = 0;
  double novLatSum = 0.0;
  double novLonSum = 0.0;
  double novHeightSum = 0.0;
  int goodNovCnt = 0;
  double gNorm;
  
  // Expected errors in the Navcom
  const double gpsHorErr = 0.15;
  const double gpsVerErr = 0.30;
  const double gpsVelErr = 0.01;

  // These are temporary variables used for calculating dXdt
  double vnx;
  double vny;
  double Rmeridian;
  double ca;
  double sa;
  double ca2;
  double sa2;
  double Rnormal;
  double rhox;
  double rhoy;
  double vx;
  double vy;
  double vz;
  double px;
  double py;
  double pz;
  double wx;
  double wy;
  double wz;
  double ox;
  double oy;
  double oz;
  double fx;
  double fy;
  double fz;
  double magg;
  double g0;

  //Defined Earth Rate.
  Matrix we_ie(3);
  we_ie.setelem(2, EARTH_RATE);

  //Variables used for finding nav solutions
  Matrix gn(3);
  Matrix fn(3);
  Matrix wn_ib(3);
  Matrix wn_ie(3);
  Matrix wn_in(3);
  Matrix wn_bn(3);
  Matrix wn_en(3);

  //Varibales used for applying corrections
  Matrix CneMeas(3,3);
  Matrix dTheta(3);
  Matrix dPhi(3);
  Matrix vdot(3);
  double dH;
  Matrix dVn(3);
  Matrix oldWn_bn(3);
  Matrix oldWb_ibSum(3);

  int ind;

  // is-vehicle-stationary flag
  int stationary = 0;
  Matrix stationaryFbSum(3);
  Matrix stationaryWb_ibSum(3);
  int stationaryIMUCount = 0;
  const int recalcOrientationCutoff = 10000;

  //Definition of gpsOffset in body frame
  Matrix gpsOffset(3);
  gpsOffset.setelem(0, (X_IMU)-(X_GPS));
  gpsOffset.setelem(1, (Y_IMU)-(Y_GPS));
  gpsOffset.setelem(2, (Z_IMU)-(Z_GPS));

  //gpsOffset transformed into Nav Frame
  Matrix gpsNavOffset(3);
  Matrix gpsVelOffset(3);

  //Definition of novOffset in body frame
  Matrix novOffset(3);
  novOffset.setelem(0, (X_IMU)-(X_NOVATEL));
  novOffset.setelem(1, (Y_IMU)-(Y_NOVATEL));
  novOffset.setelem(2, (Z_IMU)-(Z_NOVATEL));

  //novOffset transformed into Nav Frame
  Matrix novNavOffset(3);
  Matrix novVelOffset(3);

  //Offsets from IMU to Vehicle
  Matrix vehicleOffsets(3);
  vehicleOffsets.setelem(0, -X_IMU);
  vehicleOffsets.setelem(1, -Y_IMU);
  vehicleOffsets.setelem(2, -Z_IMU);
  Matrix obdVelOffset(3);

  //Variables used for converting to UTM / Latlon
  GisCoordLatLon latsForVehState;
  GisCoordUTM utmForVehState;

  // Variables for filtering
  double oldAcc_N[4];
  double oldAcc_E[4];
  double oldAcc_D[4];
  double oldRollRate[4];
  double oldPitchRate[4];
  double oldYawRate[4];
  double oldRollAcc[4];
  double oldPitchAcc[4];
  double oldYawAcc[4];

  for(int i = 0; i < 4; i++) {
    oldAcc_N[i] = 0.0;
    oldAcc_E[i] = 0.0;
    oldAcc_D[i] = 0.0;
    oldRollRate[i] = 0.0;
    oldPitchRate[i] = 0.0;
    oldYawRate[i] = 0.0;
    oldRollAcc[i] = 0.0;
    oldPitchAcc[i] = 0.0;
    oldYawAcc[i] = 0.0;
  }

  //Counter that increments every loop (used to limit broadcast rate)
  int count = 0;

  double sizeJump;
  double navConf;
  double sizeJumpVel;
  Matrix tmpVel(3);

  Matrix fbtmp(3);
  Matrix wbtmp(3);

  while (!quitPressed) {





    /*Load IMU Data*/

    DGClockMutex(&m_IMUDataMutex);
    if (imuBufferReadInd > imuBufferLastInd) {
      DGCSetConditionTrue(imuBufferFree);

      imuBufferLastInd++;

      ind = imuBufferLastInd % IMU_BUFFER_SIZE;
      
      fbtmp.setelem(0, -imuBuffer[ind].data.dvx);
      fbtmp.setelem(1,  imuBuffer[ind].data.dvy);
      fbtmp.setelem(2, -imuBuffer[ind].data.dvz);

      wbtmp.setelem(0, -imuBuffer[ind].data.dtx);
      wbtmp.setelem(1,  imuBuffer[ind].data.dty);
      wbtmp.setelem(2, -imuBuffer[ind].data.dtz);

      fbSum = fbSum + Cbi * fbtmp;
      wb_ibSum = wb_ibSum + Cbi * wbtmp;


      dtSum += 0.0025; // 1/400 --> IMU works at 400 Hz
      
      gyroError = ((imuBuffer[ind].debug.status_word >> 2) & 1);
      accelError = ((imuBuffer[ind].debug.status_word >> 3) & 1);
      gyroTmp = ((imuBuffer[ind].debug.status_word >> 5) & 1);
      accelTmp = ((imuBuffer[ind].debug.status_word >> 6) & 1);
      powerError = ((imuBuffer[ind].debug.status_word >> 7) & 1);

      switch (imuBuffer[ind].debug.mux_id) {
      case 0:
	xGyroTmp = imuBuffer[ind].debug.value / pow(2.0,8.0);
	break;
      case 1:
	yGyroTmp = imuBuffer[ind].debug.value / pow(2.0,8.0);
	break;
      case 2:
	zGyroTmp = imuBuffer[ind].debug.value / pow(2.0,8.0);
	break;
      case 3:
	xAccelTmp = imuBuffer[ind].debug.value / pow(2.0,8.0);
	break;
      case 4:
	yAccelTmp = imuBuffer[ind].debug.value / pow(2.0,8.0);
	break;
      case 5:
	zAccelTmp = imuBuffer[ind].debug.value / pow(2.0,8.0);
	break;
      case 6:
	diodeTmp = imuBuffer[ind].debug.value / pow(2.0,8.0);
	break;
      case 7:
	receivTmp = imuBuffer[ind].debug.value / pow(2.0,8.0);
	break;
      case 8:
        bfsTmp = imuBuffer[ind].debug.value / pow(2.0,8.0);
	break;
      case 9:
	minus5mon = imuBuffer[ind].debug.value / pow(2.0,15.0) * 7.0;
	break;
      case 10:
	plus5mon = imuBuffer[ind].debug.value / pow(2.0,15.0) * 7.0;
	break;
      case 11:
	minus15mon = imuBuffer[ind].debug.value / pow(2.0,15.0) * 20.0;
	break;
      case 12:
	plus15mon = imuBuffer[ind].debug.value / pow(2.0,15.0) * 20.0;
	break;
      case 13:
	analogGnd = imuBuffer[ind].debug.value / pow(2.0,15.0) * 3.0;
      }

      imuTime = imuBuffer[ind].time;

      if(stateTime < imuTime) {
	stateTime = imuTime;
      }

      newIMU = 1;
      imuCount++;

    }
    DGCunlockMutex(&m_IMUDataMutex);





    /*Load GPS data*/

    DGClockMutex(&m_GPSDataMutex);
    if (gpsBufferReadInd > gpsBufferLastInd) {
      if ((gpsBuffer[(gpsBufferLastInd + 1) % GPS_BUFFER_SIZE].time < stateTime) || newIMU == 0) {

	DGCSetConditionTrue(gpsBufferFree);

	gpsBufferLastInd++;

	ind = gpsBufferLastInd % GPS_BUFFER_SIZE;

	gps3D =  (gpsBuffer[ind].data.nav_mode & NAV_3D) && 1;
      
	if (stateMode != INIT) {
	  gpsNavOffset = Cgn * Cnb * gpsOffset;
	  gpsLat = gpsBuffer[ind].data.lat*M_PI/180 + gpsNavOffset.getelem(0)/EARTH_RADIUS;
	  gpsLon = gpsBuffer[ind].data.lng*M_PI/180 + gpsNavOffset.getelem(1)/EARTH_RADIUS;
	} else {
	  gpsLat = gpsBuffer[ind].data.lat*M_PI/180;
	  gpsLon = gpsBuffer[ind].data.lng*M_PI/180;
	}
	gpsHeight = - gpsBuffer[ind].data.ellipsoidal_height;

	gpsVelOffset = matrixCross(oldWn_bn) * Cgn.transpose() * gpsNavOffset;
	gpsVg.setelem(0,gpsBuffer[ind].data.vel_n);
	gpsVg.setelem(1,gpsBuffer[ind].data.vel_e);
	gpsVg.setelem(2,-gpsBuffer[ind].data.vel_u);
	gpsVn = Cgn.transpose() * gpsVg - gpsVelOffset;
	gpsFOM = gpsBuffer[ind].data.position_fom;
	gpsDOP = gpsBuffer[ind].data.gdop;
	gpsSats = gpsBuffer[ind].data.sats_used;

	gpsTime = gpsBuffer[ind].time;

	if (stateTime < gpsTime) {
	  stateTime = gpsTime;
	}

	latsForGPS.latitude = gpsLat*(180/M_PI);
	latsForGPS.longitude = gpsLon*(180/M_PI);
	gis_coord_latlon_to_utm(&latsForGPS, &utmForGPS, GIS_GEODETIC_MODEL_WGS_84);

	gpsNorth = utmForGPS.n;
	gpsEast = utmForGPS.e;

	newGPS = 1;

	gpsCount++;
      }
    }
    DGCunlockMutex(&m_GPSDataMutex);




    /*Load Nov Data*/

    DGClockMutex(&m_NovDataMutex);
    if (novBufferReadInd > novBufferLastInd) {
      if ((novBuffer[(novBufferLastInd + 1) % NOV_BUFFER_SIZE].time < stateTime) || newIMU == 0) {
	DGCSetConditionTrue(novBufferFree);

	ind = novBufferLastInd % NOV_BUFFER_SIZE;

	novBufferLastInd++;

	if(novBuffer[ind].data.msg_type == BESTPOS) {
	  isNovMsgPos = 1;
	  if (stateMode != INIT) {
	    novNavOffset = Cgn * Cnb * novOffset;
	    novLat = novBuffer[ind].data.lat*M_PI/180 + novNavOffset.getelem(0)/EARTH_RADIUS;
	    novLon = novBuffer[ind].data.lon*M_PI/180 + novNavOffset.getelem(1)/EARTH_RADIUS;
	  } else {
	    novLat = novBuffer[ind].data.lat*M_PI/180;
	    novLon = novBuffer[ind].data.lon*M_PI/180;
	  }	  
	  novHeight = - novBuffer[ind].data.hgt
	    - novBuffer[ind].data.undulation;
	

	  latsForNov.latitude = novLat*(180/M_PI);
	  latsForNov.longitude = novLon*(180/M_PI);
	  gis_coord_latlon_to_utm(&latsForNov, &utmForNov, GIS_GEODETIC_MODEL_WGS_84);
	  
	  novNorth = utmForNov.n;
	  novEast = utmForNov.e;

	  novNorthStd = novBuffer[ind].data.lat_std;
	  novEastStd = novBuffer[ind].data.lon_std;
	  novHgtStd = novBuffer[ind].data.hgt_std;


	  newNovP = 1;	  
	  

	} else if(novBuffer[ind].data.msg_type == BESTVEL) {
	  isNovMsgPos = 0;
	  novNavOffset = Cnb * novOffset;
	  novVelOffset = matrixCross(oldWn_bn) * novNavOffset;
	  double tmpAngle = novBuffer[ind].data.trk_gnd;
	  novVg.setelem(0,novBuffer[ind].data.hor_spd*cos(tmpAngle*M_PI/180));
	  novVg.setelem(1,novBuffer[ind].data.hor_spd*sin(tmpAngle*M_PI/180));
	  novVg.setelem(2,-novBuffer[ind].data.vert_spd);
	  novVn = Cgn.transpose() * novVg - novVelOffset;
#warning add latency velocity effects -- requires substantial change in code
	  novVelLatency = novBuffer[ind].data.latency;

	  newNovV = 1;
	}
	
	novTime = novBuffer[ind].time;
	if (stateTime < novTime) {
	  stateTime = novTime;
	}
		
	novCount++;

      }
    }
    DGCunlockMutex(&m_NovDataMutex);
      
    



    /*Load OBD data*/

    DGClockMutex(&m_OBDDataMutex);
    if (obdBufferReadInd > obdBufferLastInd) {
      if(obdBuffer[(obdBufferLastInd + 1) % OBD_BUFFER_SIZE].time < stateTime || newIMU == 0) {
	DGCSetConditionTrue(obdBufferFree);

	obdBufferLastInd++;

	ind = obdBufferLastInd % OBD_BUFFER_SIZE;

	obdTime = obdBuffer[ind].time;
	obdSteer = obdBuffer[ind].data.m_steerpos * obdSteerScaleFactor;
	obdBrake = obdBuffer[ind].data.m_brakepos;
	obdBrakeCmd = obdBuffer[ind].data.m_brakecmd;
	obdVel = obdBuffer[ind].data.m_VehicleWheelSpeed * obdVelScaleFactor;
	obdVb.clear();
	obdVb.setelem(0, obdVel);
	obdVelOffset = matrixCross(oldWn_bn) * Cnb * vehicleOffsets;
	obdVn = Cnb * obdVb - obdVelOffset;

	if (obdBrake > .5 && obdBrakeCmd > .5 && fabs(obdVel) <  0.001) {
	  newObd = 1;
	} else {
	  newObd = 0;
	}
	
	if (stateTime < obdTime) {
	  stateTime = obdTime;
	}

	obdCount++;

      }
    }
    DGCunlockMutex(&m_OBDDataMutex);



    /*Wait for new data if there is none*/
    
    if (newGPS == 0 && newIMU == 0 && newObd == 0 && newNovP == 0 && newNovV == 0) {
      newData.bCond = false;
      DGCWaitForConditionTrue(newData);
    }



    /* Beginning of Logic for State */

    if (stateMode == PREINIT) { 
      //Essentially just make sure everything is cleared
      fbSum.clear();
      wb_ibSum.clear();
      dtSum = 0;
           
      goodGpsCnt = 0;
      gpsLatSum = 0;
      gpsLonSum = 0;
      gpsHeightSum = 0;

      newGPS = 0;
      newIMU = 0;
      newNovP = 0;
      newNovV = 0;
      newObd = 0;
  
      if (imuCount > 4000) {
	
	stateMode = INIT;
	
      }

    } else if(stateMode == INIT) {

      if (newGPS) {
 	gpsLatSum += gpsLat;
	gpsLonSum += gpsLon;
	gpsHeightSum += gpsHeight;
	goodGpsCnt++;
	newGPS = 0;
      }
      
      if (newNovP) {
	if(isNovMsgPos) {
	  novLatSum += novLat;
	  novLonSum += novLon;
	  novHeightSum += novHeight;
	  goodNovCnt++;
	}
	newNovP = 0;
      } 
      
      if (newIMU) {
	newIMU = 0; //IMU is implicitly summed in the IMU read in
      }

      int loadedGps = 0;

      if (dtSum > 20 && _metaState.novPvtValid == 0 && _metaState.gpsPvtValid == 0 && loadedGps == 0) {
	goodGpsCnt = 21;

	gpsBackupFile.open("gpsBackupFile", fstream::in);
	
	double gpsTmpLat;
	double gpsTmpLon;

	gpsBackupFile >> gpsTmpLat;
	gpsBackupFile >> gpsTmpLon;

	gpsBackupFile.close();

	gpsLatSum = 21 * gpsTmpLat;
	gpsLonSum = 21 * gpsTmpLon;

	loadedGps = 1;
      }

      //If we have enough data to initialize:
      if (dtSum > 20 && (goodGpsCnt > 20 || goodNovCnt > 20)) {
	//Find mag of gravity...	
	gNorm = fbSum.norm();
	
	//Work backwards to find roll, pitch, yaw from these.
	pitch = asin(fbSum.getelem(0) / gNorm);
	roll = asin(-fbSum.getelem(1) / (gNorm*cos(pitch)));
	
	//Wtmp = Earth rotation rate translated into current nav frame.
	Matrix ctmp(3,3);
	makeBody2Nav(ctmp,roll,pitch,0);
	Matrix wtmp = ctmp * wb_ibSum;
	
	yaw = -alpha + -atan2(wtmp.getelem(1), wtmp.getelem(0));
	
	makeBody2Nav(Cnb, roll, pitch, yaw);
	

	if (goodNovCnt == 0) {
	  
	  lat = gpsLatSum / goodGpsCnt;
	  lon = gpsLonSum / goodGpsCnt;
	  height = gpsHeightSum / goodGpsCnt;
	  
	  gpsNavOffset = Cnb * gpsOffset;
	  lat += gpsNavOffset.getelem(0)/EARTH_RADIUS;
	  lon += gpsNavOffset.getelem(1)/EARTH_RADIUS;
	  height += gpsNavOffset.getelem(2);
	  
	} else if (goodGpsCnt == 0) {
	  
	  lat = novLatSum / goodNovCnt;
	  lon = novLonSum / goodNovCnt;
	  height = novHeightSum / goodNovCnt;
	  
	  novNavOffset = Cnb * novOffset;
	  lat += novNavOffset.getelem(0)/EARTH_RADIUS;
	  lon += novNavOffset.getelem(1)/EARTH_RADIUS;
	  height += novNavOffset.getelem(2);
	  
	} else {
	  // Apply offsets and take the weighted average
	  double gLat = gpsLatSum / goodGpsCnt;
	  double gLon = gpsLonSum / goodGpsCnt;
	  double gHeight = gpsHeightSum / goodGpsCnt;
	  
	  gpsNavOffset = Cnb * gpsOffset;
	  gLat += gpsNavOffset.getelem(0)/EARTH_RADIUS;
	  gLon += gpsNavOffset.getelem(1)/EARTH_RADIUS;
	  gHeight += gpsNavOffset.getelem(2);
	  
	  double nLat = novLatSum / goodNovCnt;
	  double nLon = novLonSum / goodNovCnt;
	  double nHeight = novHeightSum / goodNovCnt;
	  
	  novNavOffset = Cnb * novOffset;
	  nLat += novNavOffset.getelem(0)/EARTH_RADIUS;
	  nLon += novNavOffset.getelem(1)/EARTH_RADIUS;
	  nHeight += novNavOffset.getelem(2);
	  
	  lat = (gLat*goodGpsCnt + nLat*goodNovCnt)/(goodGpsCnt + goodNovCnt);
	  lon = (gLon*goodGpsCnt + nLon*goodNovCnt)/(goodGpsCnt + goodNovCnt);
	  height = (gHeight*goodGpsCnt + nHeight*goodNovCnt)/(goodGpsCnt + goodNovCnt);
	}
	
	makeEarth2Nav(Cne, lat, lon, alpha);
	makeNav2Geo(Cgn, alpha);

	gpsLatSum = 0;
	gpsLonSum = 0;
	gpsHeightSum = 0;
	fbSum.clear();
	wb_ibSum.clear();
	dtSum = 0.0;
		
	stateMode = NAV;

	kfInit();

	lat = asin( -Cne.getelem(2,2) );
	lon =  atan2( -Cne.getelem(2,1), -Cne.getelem(2,0) );
	alpha = atan2( -Cne.getelem(1,2), Cne.getelem(0,2) );
	pitch = asin( - Cnb.getelem(2,0));
	yaw = atan2( Cnb.getelem(1,0), Cnb.getelem(0,0) );
	roll = atan2( Cnb.getelem(2,1), Cnb.getelem(2,2) );

	newGPS = 0;
	newIMU = 0;
	newNovP = 0;
	newNovV = 0;

      }
     
    } else if (stateMode == NAV) {

      if(newIMU == 1) {
	//Do actual nav calculation...
	
	//Rotate by half the roation...

	wn_ie = Cne * we_ie;

	wn_ib = Cnb * (wb_ibSum / dtSum);

	wn_ie = Cne * we_ie;

	wn_in = wn_ie + wn_en;

	wn_bn = wn_in - wn_ib;

	Cnb = matrixDiffEqStep(wn_bn * dtSum / 2) * Cnb;



	//Move according to accelerations...

	g0 = GWGS0*(1+GWGS1*pow(sin(lat),2))/sqrt(1 - EPSILON2*pow(sin(lat),2));
	gn.setelem(2,g0*(1+2*height/EARTH_RADIUS));

	fn = Cnb * (fbSum / dtSum);

	vdot = fn - matrixCross(wn_en + wn_ie * 2)*vn + gn;

	vnx = vn.getelem(0) + vdot.getelem(0) * dtSum / 2;
	vny = vn.getelem(1) + vdot.getelem(1) * dtSum / 2;
	Rmeridian = EARTH_RADIUS*(1-EPSILON2)/pow((1-EPSILON2*pow(sin(lat),2)),1.5);
	ca = cos(alpha);
	sa = sin(alpha);
	ca2 = pow(ca,2);
	sa2 = pow(sa,2);
	Rnormal = EARTH_RADIUS/sqrt(1-EPSILON2*pow(sin(lat),2));
	rhox = vny*(ca2/(Rnormal - height) + sa2/(Rmeridian - height)) - vnx*sa*ca*(1/(Rmeridian - height) - 1/(Rnormal - height));
	rhoy = -vnx*(sa2/(Rnormal - height) + ca2/(Rmeridian - height)) + vny*sa*ca*(1/(Rmeridian - height) - 1/(Rnormal - height));
	wn_en.setelem(0,rhox);
	wn_en.setelem(1,rhoy);

	vn = vn + vdot * dtSum;

	height = height + dtSum * vn.getelem(2);


	Cne = matrixDiffEqStep(wn_en * dtSum) * Cne;


	//Finish Rotation...

	wn_ie = Cne * we_ie;

	wn_ib = Cnb * (wb_ibSum / dtSum);

	wn_ie = Cne * we_ie;

	wn_in = wn_ie + wn_en;

	wn_bn = wn_in - wn_ib;

	Cnb = matrixDiffEqStep(wn_bn * dtSum / 2) * Cnb;

	if (count % 1000 == 0) {
	
	  makeEarth2Nav(Cne, lat, lon, alpha);
	  makeBody2Nav(Cnb, roll, pitch, yaw);

	}


	//Correctly set all internal state values

	lat = asin( -Cne.getelem(2,2) );
	lon =  atan2( -Cne.getelem(2,1), -Cne.getelem(2,0) );
	alpha = atan2( -Cne.getelem(1,2), Cne.getelem(0,2) );
	pitch = asin( - Cnb.getelem(2,0));
	yaw = atan2( Cnb.getelem(1,0), Cnb.getelem(0,0) );
	roll = atan2( Cnb.getelem(2,1), Cnb.getelem(2,2) );
	makeNav2Geo(Cgn, alpha);


	//Propagate errors according to Nav movement

	//Setup for calculation of kfdXdt

	vx = vn.getelem(0);
	vy = vn.getelem(1);
	vz = vn.getelem(2);
	px = wn_en.getelem(0);
	py = wn_en.getelem(1);
	pz = wn_en.getelem(2);
	wx = wn_in.getelem(0);
	wy = wn_in.getelem(1);
	wz = wn_in.getelem(2);
	ox = wn_ie.getelem(0);
	oy = wn_ie.getelem(1);
	oz = wn_ie.getelem(2);
	fx = fn.getelem(0);
	fy = fn.getelem(1);
	fz = fn.getelem(2);
	magg = gn.norm();

	double tempprop[100] = {0, 0, -vy/square(EARTH_RADIUS), 0, 1/EARTH_RADIUS, 0, 0, 0, 0, -py,
				0, 0, vx/square(EARTH_RADIUS), -1/EARTH_RADIUS, 0, 0, 0, 0, 0, px,
				0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
				-2*(vy*oy + vz*oz), 2*vy*ox, -vz*vx/square(EARTH_RADIUS), vz/EARTH_RADIUS, 2*oz, -(py + 2*oy), 0, -fz, fy, 2*vz*ox,
				2*vx*oy, -2*(vx*ox + vz*oz), -vy*vz/square(EARTH_RADIUS), -2*oz, vz/EARTH_RADIUS, (px + 2*ox), fz, 0, -fx, 2*vz*oy,
				2*vx*oz, 2*vy*oz, (vx*vx + vy*vy)/square(EARTH_RADIUS)+2*magg/EARTH_RADIUS, 2*(py + oy), -2*(px + ox), 0, -fy, fx, 0, -2*(vx*ox + vy*oy),
				0, -oz, -vy/square(EARTH_RADIUS), 0, 1/EARTH_RADIUS, 0, 0, wz, -wy, oy,
				oz, 0, vx/square(EARTH_RADIUS), -1/EARTH_RADIUS, 0, 0, -wz, 0, wx, -ox,
				-oy, ox, 0, 0, 0, 0, wy, -wx, 0, 0,
				py, -px, 0, 0, 0, 0, 0, 0, 0, 0};


	kfdXdt.setelems(tempprop);

	kfA = kfA.eye() + kfdXdt*dtSum;

	kfX = kfA*kfX;

	kfP = (kfA*kfP*kfA.transpose() + kfQ);

	fbSum.clear();

	newIMU = 0;

      }

      
      //Make GPS Measurement

      if (newGPS == 1) {

	//Do we want to incorporate this measurement or not?
	if (_metaState.novPvtValid == 0) {
	
	  sizeJump = sqrt( pow(gpsLat - lat, 2.0) + pow(gpsLon - lon, 2.0) );
	  navConf = 10*sqrt(kfP.getelem(0,0) + kfP.getelem(1,1));
	  
	  if ((sizeJump < JUMP_THRESHOLD || stateJump == JUMP_OK)) {
	    
	    //Create measurement according to gps latitude and longitude
	    makeEarth2Nav(CneMeas, gpsLat, gpsLon, alpha);
	    dTheta = unmatrixCross(Matrix(3,3).eye() - (Cne * CneMeas.transpose()));
	    dVn = vn - gpsVn;
	    dH = height - gpsHeight;
	    
	    Matrix tmp;
	    
	    if(gps3D) {
	      
	      kfZ3d.setelem(0,dTheta.getelem(0));
	      kfZ3d.setelem(1,dTheta.getelem(1));
	      kfZ3d.setelem(2,dH);
	      kfZ3d.setelem(3,dVn.getelem(0));
	      kfZ3d.setelem(4,dVn.getelem(1));
	      kfZ3d.setelem(5,dVn.getelem(2));
	      
	      kfR3d.setelem(0, 0, square(gpsHorErr/2.0/sqrt(2.0)*gpsDOP/EARTH_RADIUS));
	      kfR3d.setelem(1, 1, square(gpsHorErr/2.0/sqrt(2.0)*gpsDOP/EARTH_RADIUS));
	      kfR3d.setelem(2, 2, square(gpsVerErr/2.0*gpsDOP));
	      kfR3d.setelem(3, 3, square(gpsVelErr*gpsDOP));
	      kfR3d.setelem(4, 4, square(gpsVelErr*gpsDOP));
	      kfR3d.setelem(5, 5, square(gpsVelErr*gpsDOP));
	      
	      tmp = (kfH3d*kfP*kfH3d.transpose() + kfR3d);
	      
	      kfK = kfP*kfH3d.transpose()*tmp.inverse();
	      
	      kfX = kfX + kfK*(kfZ3d - kfH3d*kfX);
	      
	      kfP = (kfP.eye() - kfK*kfH3d)*kfP;
	      /*  
	      if (stateJump == JUMP_OK && sizeJump < JUMP_THRESHOLD*0.5) {
		DGClockMutex(&m_jumpMutex);
		stateJump = JUMP_DONE;
		DGCunlockMutex(&m_jumpMutex);
	      }
	      */
	    } else {
	      kfZ2d.setelem(0,dTheta.getelem(0));
	      kfZ2d.setelem(1,dTheta.getelem(1));
	      kfZ2d.setelem(2,dVn.getelem(0));
	      kfZ2d.setelem(3,dVn.getelem(1));
	      
	      kfR2d.setelem(0, 0, square(gpsHorErr/2.0/sqrt(2.0)*gpsDOP/EARTH_RADIUS));
	      kfR2d.setelem(1, 1, square(gpsHorErr/2.0/sqrt(2.0)*gpsDOP/EARTH_RADIUS));
	      kfR2d.setelem(2, 2, square(gpsVelErr*gpsDOP));
	      kfR2d.setelem(3, 3, square(gpsVelErr*gpsDOP));
	      
	      tmp = (kfH2d*kfP*kfH2d.transpose() + kfR2d);
	      
	      kfK = kfP*kfH2d.transpose()*tmp.inverse();
	      
	      kfX = kfX + kfK*(kfZ2d - kfH2d*kfX);
	      
	      kfP = (kfP.eye() - kfK*kfH2d)*kfP;
	      
	    }
	  } else if ((stateJump == JUMP_NONE) && navConf > JUMP_THRESHOLD) {

	    DGClockMutex(&m_jumpMutex);
	    stateJump = JUMP_REQUESTED;
	    DGCunlockMutex(&m_jumpMutex);

	  } 

	}
		
	newGPS = 0;
      }

      //Make Novatel Measurement

      if (newNovP == 1) {

	//Do we want to incorporate these values or not?

	sizeJump = sqrt( pow(novLat - lat, 2.0) + pow(novLon - lon, 2.0) );
	navConf = 10*sqrt(kfP.getelem(0,0) + kfP.getelem(1,1));

	if (sizeJump < JUMP_THRESHOLD || stateJump == JUMP_OK) {

	//Create measurement according to gps latitude and longitude
	  makeEarth2Nav(CneMeas, novLat, novLon, alpha);
	  dTheta = unmatrixCross(Matrix(3,3).eye() - (Cne * CneMeas.transpose()));
	  dH = height - novHeight;
	  
	  Matrix tmp;
	  
	  kfZnovP.setelem(0,dTheta.getelem(0));
	  kfZnovP.setelem(1,dTheta.getelem(1));
	  kfZnovP.setelem(2,dH);
	  
	  kfRnovP.setelem(0, 0, square(novNorthStd / EARTH_RADIUS));
	  kfRnovP.setelem(1, 1, square(novEastStd / EARTH_RADIUS));
	  kfRnovP.setelem(2, 2, square(novHgtStd));
	  
	  tmp = (kfHnovP*kfP*kfHnovP.transpose() + kfRnovP);
	  
	  kfK = kfP*kfHnovP.transpose()*tmp.inverse();
	  
	  kfX = kfX + kfK*(kfZnovP - kfHnovP*kfX);
	  
	  kfP = (kfP.eye() - kfK*kfHnovP)*kfP;
	  /*
	  if (stateJump == JUMP_OK && sizeJump < JUMP_THRESHOLD*0.5) {
	    DGClockMutex(&m_jumpMutex);
	    stateJump = JUMP_DONE;
	    DGCunlockMutex(&m_jumpMutex);
	  }
	  */
	} else if ((stateJump == JUMP_NONE) && navConf > JUMP_THRESHOLD) {
	  DGClockMutex(&m_jumpMutex);
	  stateJump = JUMP_REQUESTED;
	  DGCunlockMutex(&m_jumpMutex);
	}
	newNovP = 0;

      }

      if (newNovV == 1) {


	tmpVel = vn - novVn;
	sizeJumpVel = tmpVel.norm();

	navConf = 10*sqrt(kfP.getelem(0,0) + kfP.getelem(1,1));
	
	if (sizeJumpVel < JUMP_THRESHOLD_VEL || stateJump == JUMP_OK) {
	  
	  dVn = vn - novVn;
	  
	  Matrix tmp;
	  
	  
	  kfZnovV.setelem(0,dVn.getelem(0));
	  kfZnovV.setelem(1,dVn.getelem(1));
	  kfZnovV.setelem(2,dVn.getelem(2));
	  
#warning kfRnovV values are bullshit but work so far
	  kfRnovV.setelem(0, 0, square(novNorthStd));
	  kfRnovV.setelem(1, 1, square(novEastStd));
	  kfRnovV.setelem(2, 2, square(novHgtStd));
	  
	  tmp = (kfHnovV*kfP*kfHnovV.transpose() + kfRnovV);
	  
	  kfK = kfP*kfHnovV.transpose()*tmp.inverse();
	  
	  kfX = kfX + kfK*(kfZnovV - kfHnovV*kfX);
	  
	  kfP = (kfP.eye() - kfK*kfHnovV)*kfP;

	  newNovV = 0;

	}
		
	
      }

      


      //Apply OBD measurement

      if (newObd == 1) {

	tmpVel = vn;
	sizeJumpVel = tmpVel.norm();
	
	navConf = 10*sqrt(kfP.getelem(0,0) + kfP.getelem(1,1));
	
	if (sizeJumpVel < JUMP_THRESHOLD_ZERO_VEL || stateJump == JUMP_OK) {

#warning Would be nice to use a bicycle model for real-time velocity updates rather than just stationary mode
	  
	  // KF on the obdii speed
	  //	Matrix tempV(3);
	  //tempV.setelem(0, obdVel);
	  
	  //	kfZobd = vn - obdVn;//Cnb*tempV;
	  kfZobd = vn; //Makes velocity zero
	  
	  kfRobd.setelem(0, 0, pow(0.01, 2.));
	  kfRobd.setelem(1, 1, pow(0.01, 2.));
	  kfRobd.setelem(2, 2, pow(0.01, 2.));
	  Matrix tmp;
	  
	  tmp = (kfHobd*kfP*kfHobd.transpose() + kfRobd);
	  
	  kfK = kfP*kfHobd.transpose()*tmp.inverse();
	  
	  kfX = kfX + kfK*(kfZobd - kfHobd*kfX);
	  
	  kfP = (kfP.eye() - kfK*kfHobd)*kfP;
	  
	  newObd = 0;
	  /*
	  if (stateJump == JUMP_OK && sizeJumpVel < JUMP_THRESHOLD_ZERO_VEL*0.5 &&
	      _metaState.gpsPvtValid == 0 && _metaState.novPvtValid == 0) {
	    DGClockMutex(&m_jumpMutex);
	    stateJump = JUMP_DONE;
	    DGCunlockMutex(&m_jumpMutex);
	  }
	  */
	} else if (stateJump == JUMP_NONE) {
	  DGClockMutex(&m_jumpMutex);
	  stateJump = JUMP_REQUESTED;
	  DGCunlockMutex(&m_jumpMutex);
	}
	
      }


      //Apply Correction:

      dTheta.setelem(0,kfX.getelem(0));
      dTheta.setelem(1,kfX.getelem(1));
      dTheta.setelem(2,kfX.getelem(9));
      dPhi.setelem(0,kfX.getelem(6));
      dPhi.setelem(1,kfX.getelem(7));
      dPhi.setelem(2,kfX.getelem(8));
      dH = kfX.getelem(2);
      dVn.setelem(0,kfX.getelem(3));
      dVn.setelem(1,kfX.getelem(4));
      dVn.setelem(2,kfX.getelem(5));
	
      Cne = matrixDiffEqStep(dTheta * -pError)*Cne;
	
      Cnb = matrixDiffEqStep(dPhi * -1.0)*Cnb;
	
      height = height - pError*dH;
	
      vn = vn - dVn;
	  
      lat = asin( -Cne.getelem(2,2) );
      lon =  atan2( -Cne.getelem(2,1), -Cne.getelem(2,0) );
      alpha = atan2( -Cne.getelem(1,2), Cne.getelem(0,2) );
      pitch = asin( - Cnb.getelem(2,0));
      yaw = atan2( Cnb.getelem(1,0), Cnb.getelem(0,0) );
      roll = atan2( Cnb.getelem(2,1), Cnb.getelem(2,2) );
      makeNav2Geo(Cgn, alpha);

      if (_astateOpts.useAutotune) {
	for(int tmp = 0; tmp < kfX.getrows(); tmp++){
	  autotuneStream.setf(ios::scientific);
	  autotuneStream.unsetf(ios::fixed);
	  autotuneStream << kfX.getelem(tmp) << " ";
	}
	autotuneStream << endl;
      }
	  
      // Reset error vector with leftovers from smoothing, if any.
      kfX.clear();
      kfX.setelem(0, (1-pError)*dTheta.getelem(0));
      kfX.setelem(1, (1-pError)*dTheta.getelem(1));
      kfX.setelem(2, (1-pError)*dH);
      kfX.setelem(9, (1-pError)*dTheta.getelem(2));



      trackErr = sqrt( pow(dTheta.getelem(0)*EARTH_RADIUS, 2.0) + pow(dTheta.getelem(1)*EARTH_RADIUS, 2.0) );



      // Update vehicle state struct

      DGClockMutex(&m_VehicleStateMutex);
	
      latsForVehState.latitude = lat*(180/M_PI);
      latsForVehState.longitude = lon*(180/M_PI);	
	
      gis_coord_latlon_to_utm(&latsForVehState, &utmForVehState, GIS_GEODETIC_MODEL_WGS_84);
	
      _vehicleState.Timestamp = imuTime;

      Matrix rtmp = Cgn*Cnb*vehicleOffsets;
      _vehicleState.Northing = utmForVehState.n + rtmp.getelem(0); 
      _vehicleState.Easting = utmForVehState.e + rtmp.getelem(1);
      _vehicleState.Altitude = height + rtmp.getelem(2);

      Matrix rrtmp = matrixCross(wn_bn)*rtmp;
      _vehicleState.Vel_N = vn.getelem(0) - rrtmp.getelem(0);
      _vehicleState.Vel_E = vn.getelem(1) - rrtmp.getelem(1);
      _vehicleState.Vel_D = vn.getelem(2) - rrtmp.getelem(2);

      if (dtSum != 0) {
	Matrix rrrtmp = matrixCross((wn_bn - oldWn_bn)/dtSum)*rtmp - matrixCross(wn_bn)*rrtmp;
	_vehicleState.Acc_N = filter(oldAcc_N, vdot.getelem(0) - rrrtmp.getelem(0));
	_vehicleState.Acc_E = filter(oldAcc_E, vdot.getelem(1) - rrrtmp.getelem(1));
	_vehicleState.Acc_D = filter(oldAcc_D, vdot.getelem(2) - rrrtmp.getelem(2));
	_vehicleState.RollAcc = filter(oldRollAcc,(wb_ibSum.getelem(0) - oldWb_ibSum.getelem(0))/dtSum);
	_vehicleState.PitchAcc = filter(oldPitchAcc,(wb_ibSum.getelem(1) - oldWb_ibSum.getelem(1))/dtSum);
	_vehicleState.YawAcc = filter(oldYawAcc,(wb_ibSum.getelem(2) - oldWb_ibSum.getelem(2))/dtSum);
	_vehicleState.RollRate = filter(oldRollRate, wb_ibSum.getelem(0)/dtSum);
	_vehicleState.PitchRate = filter(oldPitchRate,wb_ibSum.getelem(1)/dtSum);
	_vehicleState.YawRate = filter(oldYawRate,wb_ibSum.getelem(2)/dtSum);
      } else {
	_vehicleState.Acc_N = filter(oldAcc_N,vdot.getelem(0));
	_vehicleState.Acc_E = filter(oldAcc_E,vdot.getelem(1));
	_vehicleState.Acc_D = filter(oldAcc_D,vdot.getelem(2));
	_vehicleState.RollAcc = filter(oldRollAcc,0);
	_vehicleState.PitchAcc = filter(oldPitchAcc,0);
	_vehicleState.YawAcc = filter(oldYawAcc,0);
	_vehicleState.RollRate = filter(oldRollRate,0);
	_vehicleState.PitchRate = filter(oldPitchRate,0);
	_vehicleState.YawRate = filter(oldYawRate,0);
      }

      _vehicleState.Roll = roll;
      _vehicleState.Pitch = pitch;
      _vehicleState.Yaw = yaw + alpha;	

      //Confidences are calculated as standard deviations...

      _vehicleState.NorthConf = 10*sqrt(kfP.getelem(0,0))*EARTH_RADIUS;
      _vehicleState.EastConf = 10*sqrt(kfP.getelem(1,1))*EARTH_RADIUS;
      _vehicleState.HeightConf = 10*sqrt(kfP.getelem(2,2));
      _vehicleState.RollConf = sqrt(kfP.getelem(6,6));
      _vehicleState.PitchConf = sqrt(kfP.getelem(7,7));
      _vehicleState.YawConf = sqrt(kfP.getelem(8,8));
	
      oldWn_bn = wn_bn;
      oldWb_ibSum = wb_ibSum;
      double tmpHead = atan2(_vehicleState.Vel_E_rear(), _vehicleState.Vel_N_rear());
      headErr = (atan2(sin(_vehicleState.Yaw), cos(_vehicleState.Yaw)) - atan2(sin(tmpHead), cos(tmpHead))) * 180/M_PI;

      DGCunlockMutex(&m_VehicleStateMutex);
	
      wb_ibSum.clear();
      dtSum = 0.0;
	
      if (count++ % 8 == 0) {
	
	if (_astateOpts.debug == 1) {
	  cout.precision(20);
	  cout << lat << '\t' << lon << '\t' << novLat << '\t' << novLon << '\t' << roll << '\t' << pitch << '\t' << yaw;
	  cout << '\t' << gpsFOM << '\t'  << height << '\t' << novHeight << '\t' << vn.getelem(0) << '\t' << vn.getelem(1);
	  cout << '\t' << vn.getelem(2) << '\t' << novVn.getelem(0) << '\t' << novVn.getelem(1) << '\t' << novVn.getelem(2);
	  cout << '\t' << stationary << '\t' << novVn.norm() << '\t' << alpha << endl;
	}

	//All Your Network Traffic Are Belong To Us
	if (_astateOpts.fastMode == 0) {
	  broadcast();

	}

      }

      if (count % 1000 == 0) {
	gpsBackupFile.open("gpsBackupFile", fstream::out);
	gpsBackupFile.precision(20);
	gpsBackupFile << lat << '\t' << lon << endl;
	gpsBackupFile.close();
      }


    } else if (stateMode == FALLBACK) {
      
      if(newGPS && _metaState.novPvtValid == 0) {
	DGClockMutex(&m_VehicleStateMutex);

        _vehicleState.Timestamp = gpsTime;

        latsForVehState.latitude = gpsLat*(180/M_PI);
	lat = gpsLat;
        latsForVehState.longitude = gpsLon*(180/M_PI);
	lon = gpsLon;
        gis_coord_latlon_to_utm(&latsForVehState, &utmForVehState, GIS_GEODETIC_MODEL_WGS_84);

	// Estimate initially
        _vehicleState.Northing = utmForVehState.n;
        _vehicleState.Easting = utmForVehState.e;
        _vehicleState.Altitude = gpsHeight;
	height = gpsHeight;

	_vehicleState.Vel_N = gpsVn.getelem(0);
        _vehicleState.Vel_E = gpsVn.getelem(1);
        _vehicleState.Vel_D = gpsVn.getelem(2);

	// Use these to estimate RPY
	 if (gpsVn.norm() != 0.0) {
	   _vehicleState.Yaw = atan2(gpsVn.getelem(1), gpsVn.getelem(0));
	   _vehicleState.Roll = 0; // Can't back it out of the matrix, as V in the body-frame is roll-independent.
	   _vehicleState.Pitch = asin(-gpsVn.getelem(2)/gpsVn.norm());
	 } else { // use previous ones if we have nothing to base it on.
	   _vehicleState.Yaw = yaw;
	   _vehicleState.Roll = roll;
	   _vehicleState.Pitch = pitch;
	 }

	// No second or third derivative corrections, though, without IMU input.

        _vehicleState.Acc_N = filter(oldAcc_N,(vn.getelem(0) - gpsVn.getelem(0))/5.0);  // Updates at 5Hz
        _vehicleState.Acc_E = filter(oldAcc_E,(vn.getelem(1) - gpsVn.getelem(1))/5.0);  // Updates at 5Hz
        _vehicleState.Acc_D = filter(oldAcc_D,(vn.getelem(2) - gpsVn.getelem(2))/5.0);  // Updates at 5Hz

	vn = gpsVn;

        _vehicleState.RollRate = filter(oldRollRate,(roll - _vehicleState.Roll)/5.0);
        _vehicleState.PitchRate = filter(oldPitchRate,(pitch - _vehicleState.Pitch)/5.0);
        _vehicleState.YawRate = filter(oldYawRate,(yaw - _vehicleState.Yaw)/5.0);

        _vehicleState.RollAcc = filter(oldRollAcc,_vehicleState.RollRate/5.0);
        _vehicleState.PitchAcc = filter(oldPitchAcc,_vehicleState.PitchRate/5.0);
        _vehicleState.YawAcc = filter(oldYawAcc,_vehicleState.YawRate/5.0);

        yaw = _vehicleState.Yaw;
	pitch = _vehicleState.Pitch;
	roll = _vehicleState.Roll;

	makeBody2Nav(Cnb, roll, pitch, yaw);

        Matrix rtmp = Cgn*Cnb*vehicleOffsets;
        _vehicleState.Northing += rtmp.getelem(0);
        _vehicleState.Easting += rtmp.getelem(1);
        _vehicleState.Altitude += rtmp.getelem(2);

	// Let alpha remain ~zero.

        // Confidences are mostly meaningless--there's not really any
	// good way to do RPY confidence.

        _vehicleState.NorthConf = gpsDOP*0.15;
        _vehicleState.EastConf = gpsDOP*0.15;
        _vehicleState.HeightConf = gpsDOP*0.30;
        _vehicleState.RollConf = _vehicleState.RollRate;
        _vehicleState.PitchConf = _vehicleState.PitchRate;
        _vehicleState.YawConf = _vehicleState.YawRate;

        DGCunlockMutex(&m_VehicleStateMutex);

	if (_astateOpts.debug == 1) {
	  cout.precision(20);
	  cout << lat << '\t' << lon << '\t' << gpsLat << '\t' << gpsLon << '\t' << roll << '\t' << pitch << '\t' << yaw + alpha << '\t' << gpsFOM << '\t'  << height << '\t' << gpsHeight << '\t' << vn.getelem(0) << '\t' << vn.getelem(1) << '\t' << vn.getelem(2) << '\t' << novVn.getelem(0) << '\t' << novVn.getelem(1) << '\t' << novVn.getelem(2) << '\t' << stationary << '\t' << novVn.norm() << endl;
	}
      }

      // Note: if this ever gets adjusted in the future, the optimal way to combine 
      // these two would be with a second KF on lat, lon, vel, etc. rather than on
      // their errors, and use that with these inputs.  Not happening, I'm afraid.

      // Update with the Novatel if available
      if(newNovP || newNovV) {
	if(newNovP) {
	  latsForVehState.latitude = novLat*(180/M_PI);
	  lat = novLat;
	  latsForVehState.longitude = novLon*(180/M_PI);
	  lon = novLon;
	  gis_coord_latlon_to_utm(&latsForVehState, &utmForVehState, GIS_GEODETIC_MODEL_WGS_84);
	  
	  // Estimate initially
	  _vehicleState.Northing = utmForVehState.n;
	  _vehicleState.Easting = utmForVehState.e;
	  _vehicleState.Altitude = novHeight;
	  height = novHeight;
	  
	  _vehicleState.NorthConf = novNorthStd;
	  _vehicleState.EastConf = novEastStd;
	  _vehicleState.HeightConf = novHgtStd;

	} else {
	  _vehicleState.Vel_N = novVn.getelem(0);
	  _vehicleState.Vel_E = novVn.getelem(1);
	  _vehicleState.Vel_D = novVn.getelem(2);
	  
	  // Use these to estimate RPY
	  if (novVn.norm() > 1.0) {
	    _vehicleState.Yaw = atan2(novVn.getelem(1), novVn.getelem(0));
	    _vehicleState.Roll = 0; // Can't back it out of the matrix, as V in the body-frame is roll-independent.
	    _vehicleState.Pitch = asin(-novVn.getelem(2)/novVn.norm());
	  } else { // use previous ones if we have nothing to base it on.
	    _vehicleState.Yaw = yaw;
	    _vehicleState.Roll = roll;
	    _vehicleState.Pitch = pitch;
	  }
	  
	  // No second or third derivative corrections, though, without IMU input.
	  
	  _vehicleState.Acc_N = filter(oldAcc_N,(vn.getelem(0) - novVn.getelem(0))/5.0);  // Updates at 5Hz
	  _vehicleState.Acc_E = filter(oldAcc_E,(vn.getelem(1) - novVn.getelem(1))/5.0);  // Updates at 5Hz
	  _vehicleState.Acc_D = filter(oldAcc_D,(vn.getelem(2) - novVn.getelem(2))/5.0);  // Updates at 5Hz
	  
	  vn = novVn;
	  
	  _vehicleState.RollRate = filter(oldRollRate,(roll - _vehicleState.Roll)/5.0);
	  _vehicleState.PitchRate = filter(oldPitchRate,(pitch - _vehicleState.Pitch)/5.0);
	  _vehicleState.YawRate = filter(oldYawRate,(yaw - _vehicleState.Yaw)/5.0);
	  
	  _vehicleState.RollAcc = filter(oldRollAcc,_vehicleState.RollRate/5.0);
	  _vehicleState.PitchAcc = filter(oldPitchAcc,_vehicleState.PitchRate/5.0);
	  _vehicleState.YawAcc = filter(oldYawAcc,_vehicleState.YawRate/5.0);
	  
	  yaw = _vehicleState.Yaw;
	  pitch = _vehicleState.Pitch;
	  roll = _vehicleState.Roll;
	  
	  makeBody2Nav(Cnb, roll, pitch, yaw);
	  
	  Matrix rtmp = Cgn*Cnb*vehicleOffsets;
	  _vehicleState.Northing += rtmp.getelem(0);
	  _vehicleState.Easting += rtmp.getelem(1);
	  _vehicleState.Altitude += rtmp.getelem(2);
	  
	  _vehicleState.RollConf = _vehicleState.RollRate;
	  _vehicleState.PitchConf = _vehicleState.PitchRate;
	  _vehicleState.YawConf = _vehicleState.YawRate;	  
	}
      }


      if(newGPS || newNovP || newNovV) {
	//All Your Network Traffic Are Belong To Us
	if (_astateOpts.fastMode == 0) {
	  broadcast(); 
	}
	
	newGPS = 0;
	newNovP = 0;
	newNovV = 0;

      }

    } else {
      stateMode = PREINIT; 
    }
  }
}

// 4th order Butterworth filter is suitable for our needs.
double filter(double *oldVals, double accel) {
  // Initialize filter
  double b[5] = {0.10625987851426e-6,
		       0.42503951405704e-6,
		       0.63755927108555e-6,
		       0.42503951405704e-6,
		       0.10625987851426e-6};
  
  double a[5] = {1.00000000000000,
		 -3.90452133611599,
		 5.71809570332490,
		 -3.72250242317483,
		 0.90892975612397};

  double fAccel = b[0]*accel+oldVals[0]; 
  oldVals[0] = b[1]*accel+oldVals[1]-a[1]*fAccel;
  oldVals[1] = b[2]*accel+oldVals[2]-a[2]*fAccel;
  oldVals[2] = b[3]*accel+oldVals[3]-a[3]*fAccel;
  oldVals[3] = b[4]*accel-a[4]*fAccel;   

  return fAccel;
}
