#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <termios.h>
#include <unistd.h>
#include <netinet/in.h>  /* for struct sockaddr_in, htons(3) */
#include <sys/ioctl.h>
#include <math.h>
#include <sstream>
#include <fstream>
#include <linux/serial.h>

#include <DGCutils>

#include "ladar_buffer.hh"

#define HAVE_HI_SPEED_SERIAL

#define DEFAULT_LASER_PORT "/dev/ttyS1"
#define DEFAULT_LASER_PORT_RATE 38400
#define DEFAULT_LASER_RETRIES 3
#define MAX_LADAR_SCAN 401
#define MAX_CONFIG_FILE_LINE 1024
#define SIZE_TIMESTAMP_BUFFER 100

using namespace std;

struct laser_data_t {
  uint16_t ranges[MAX_LADAR_SCAN];
  uint16_t intensity[MAX_LADAR_SCAN];
	unsigned long long timestamps[MAX_LADAR_SCAN];
  int min_angle;
  int max_angle;
  int resolution;
  int range_count;
  float range_res;
  unsigned long long ladar_timestamp;
};

// The laser device class.
class SickLMS200 {
public:
    
    SickLMS200();
    ~SickLMS200();

    int Setup(char *filename, bool setconfigfile = true);
    bool Reset();
    bool DumpErrors(void);

    CLadarDataBuffer m_laserData;

    int getNumDataPoints();
    
    int startLogging(char* filename);
    int stopLogging();

    bool isLogging();

    int SetupFromFile(char *configFilename, char* logFilename);

    unsigned long long Scan(unsigned long long timestampThresh);
    int GetScanFrame(double* ranges, double* angles, unsigned long long* latestScanTimestamp,
										 unsigned long long* timestamps,
		     char* errorMessage);
		void MainThread(void);

    // Request data from the laser. Returns true on success
    bool RequestLaserData(void);

    // Request data from the laser to stop. Returns true on success
    bool RequestLaserDataStop(void);

private:
		// for detecting a new scan condition
		bool            m_bHaveNewScan;
		pthread_cond_t  m_newScanCondition;
		pthread_mutex_t m_newScanCondMutex;

		pthread_mutex_t m_laserDataMutex;
		DGCcondition    m_requestDataCondition;

    FILE* _logFileRaw;
		FILE* _errorFile;
    char _logFilename[2048];
    char _errorFilename[2048];
    unsigned long long lastTime;

		// the timestamp buffer, the index into it, whether the buffer is full,
		// and the sum of all the timestamps
		unsigned long long m_timestamps[SIZE_TIMESTAMP_BUFFER];
		int                m_timestampBufferIndex;
		bool               m_bTimestampBufferFull;
		unsigned long long m_timestampsSum;

    // Compute the start and end scan segments based on the current resolution and
    // scan angles.  Returns 0 if the configuration is valid.
    bool CheckScanConfig();
    
    // Open the terminal
    // Returns 0 on success
    int OpenTerm();

		// sets the serial device to either block or not block. These return true on success.
		bool SetBlockingMode();
		bool SetNonBlockingMode();

    // Close the terminal
    // Returns 0 on success
    int CloseTerm();
    
    // Set the terminal speed
    // Valid values are 9600 and 38400
    // Returns 0 on success
    int ChangeTermSpeed(int speed);

    int CheckStatus(uint8_t status);

    // Get the laser type
    bool GetLaserType(char *buffer, size_t bufflen);

    // Set the laser data rate
    // Valid values are 9600 and 38400
    // Returns true on success
    bool SetLaserSpeed(int speed);

    // Put the laser into configuration mode
    bool EnterInstallationMode();

    // Set the laser configuration
    // Returns true on success
    bool SetLaserConfig(bool intensity);

    // Change the resolution of the laser
    bool SetLaserRes(int angle, int res);
    
    // Read range data from laser
    bool ReadScanFromLaser(uint16_t *data, size_t datalen);

    // Write newest data to the data struct.
    void PutData(laser_data_t *pFrom);

private:
    void getTTYport(char* pTTY);

    // Write a packet to the laser
    ssize_t WriteToLaser(uint8_t *data, ssize_t len); 
    
    // Read a packet from the laser
    ssize_t ReadFromLaser(uint8_t *data, ssize_t maxlen, bool ack = false, int timeout = -1,
			  bool checkStatus = true);

    // Calculates CRC for a telegram
    unsigned short CreateCRC(uint8_t *data, ssize_t len);

  int ParseConfigFile(char *filename);

  int CheckErrors();

	int DumpConfig();

  //  protected:
 public:

    // Laser pose in robot cs.
    double pose[3];
    double size[2];
    
    // Name of device used to communicate with the laser
    char device_name[MAX_CONFIG_FILE_LINE];
    
    // laser device file descriptor
    int laser_fd;           

    // Starup delay
    int startup_delay;
  
    // Scan width and resolution.
    int scan_width, scan_res;

    // Start and end scan angles (for restricted scan).  These are in
    // units of 0.01 degrees.
    int min_angle, max_angle;
    
    // Start and end scan segments (for restricted scan).  These are
    // the values used by the laser.
    int scan_min_segment, scan_max_segment;
		int scan_num_segments;

    // Range resolution (in mm)
    int range_res;

		// for compatibility with ladarSource. Somebody fix this. Please.
		double _resolution, _ladarUnits, _scanAngleOffset;
		int _scanAngle;

    // Turn intensity data on/off
    bool intensity;

    bool can_do_hi_speed;
    int port_rate;
    int current_rate;  

    enum LADAR_AUTHORITY_TYPE {
      NEITHER = 0,

      MASTER = 1,
      SLAVE = 2
    };

		char _configFilename[256];

    LADAR_AUTHORITY_TYPE _ladarAuthority;

    string _errorMessage;

#ifdef HAVE_HI_SPEED_SERIAL
  struct serial_struct old_serial;
#endif
};

////////////////////////////////////////////////////////////////////////////////
// Device codes

#define STX     '\x02'
#define ACK     0xA0
#define NACK    0x92
#define CRC16_GEN_POL 0x8005


////////////////////////////////////////////////////////////////////////////////
// Error macros
#define RETURN_ERROR(erc, m) {return erc;}
 
////////////////////////////////////////////////////////////////////////////////
