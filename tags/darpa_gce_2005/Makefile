DGC = .
include $(DGC)/global.mk

# This is the list of subdirectories to be cleaned
SUBDIRS = drivers modules util

# append .clean to directory names to clean
# this prevents "make modules" from cleaning modules
CLEAN_RULES = $(SUBDIRS:=.clean)

.PHONY: all clean uninstall wipe astate stereo ladar util tags
.PHONY: $(CLEAN_RULES) $(DGC_BIN_INFO)

all:  nogui

world: all simulator gui stereo road 

64: planner mapper $(RDDFPREPBIN) $(DGC_BIN_INFO)

32: $(DGC_BIN_INFO) \
    $(ASTATEBIN) \
    $(LADARFEEDERBIN) \
    $(STEREOFEEDERBIN) \
    $(TRAJFOLLOWERBIN) \
    $(ADRIVEBIN) \
    $(SUPERCONBIN) \
    $(DBSBIN) \
    $(RDDFPREPBIN) \
    $(DISTRDDFBIN) \
    $(GUIBIN) \
    $(ROADFINDINGBIN) \
    $(LOGPLAYERBIN) \
    $(SIMULATORBIN)

nogui: $(DGC_BIN_INFO) \
    $(TRAJFOLLOWERBIN) \
    $(FUSIONMAPPERBIN) \
    $(PLANNERMODULEBIN) \
    $(ADRIVEBIN) \
    $(LADARFEEDERBIN) \
    $(ASTATEBIN) \
    $(LOGPLAYERBIN) \
    $(VIDEORECORDERBIN) \
    $(SUPERCONBIN) \
    $(DBSBIN) \
    $(RDDFPREPBIN) \
    $(DISTRDDFBIN)

gui: $(GUIBIN)

adrive: $(ADRIVEBIN) 

stereo: $(STEREOFEEDERBIN)

superCon: $(SUPERCONBIN)

ladar: $(LADARFEEDERBIN)

fusion: $(FUSIONMAPPERBIN)

mapper: fusion

planner: $(PLANNERMODULEBIN)

state: $(ASTATEBIN)

timber: $(TIMBERBIN)

road: $(ROADFINDINGBIN)

simulator: $(SIMULATORBIN)

sim: simulator

util:
	make -C util install

gazebo:
	make -C util/gazebo/ install

uninstall: $(CLEAN_RULES) wipe

clean: $(CLEAN_RULES)
	@echo
	@echo '*** NOTE: By popular demand, "make clean" no longer removes the include, lib, and bin directories. Use "make uninstall" to do this. ***'

wipe:
	(cat $(BINDIR)/todo_timber >> $(DGC)/todo_timber 2>/dev/null; true)
	rm -rf lib bin

info: $(DGC_BIN_INFO)

$(CLEAN_RULES):
	$(MAKE) -C $(@:.clean=) clean

$(DGC_BIN_INFO):
	echo -ne "hostname  : " > $@ ; hostname >> $@
	echo -ne "\ndate      : " >> $@ ; date >> $@
	echo -ne "\npwd       : " >> $@ ; pwd >> $@
	echo -ne "\nsvn info  :\n\n" >> $@ ; svn info >> $@

tags:
	find . -path '*/.svn' -prune -o -print | grep -E \\.h\$$\|\\.hh\$$\|\\.c\$$\|\\.cc\$$\|\\.cpp\$$ | grep -v "#" | etags --declarations --defines --globals -l c -l c++ --members -
