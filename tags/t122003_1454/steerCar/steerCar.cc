#include <stdio.h>

#include <fstream>
#include <sstream>
#include <iostream>
#include <string>

#include "../Msg/dgc_const.h"           // TRUE, FALSE, ERROR
#include "steerCar.h"
#include "../serial/serial.h"

using namespace std;

extern int negCountLimit;
extern int posCountLimit;
extern int STEER_BY_HAND;
extern int verbosity_exec_cmd;
extern int steer_errno;

///////////////////////////THIS IS A HACK/////////////////
extern string lastSteeringCommanded;
//////////////////////////////////////////////////////////


int init_steer (void) 
{
    static int serialPortOpen = FALSE;     // Set to true once we've opened it.
    int justPoweredOn = FALSE;  // Not powered on in *this* function call.
    int status;                 // Receives return values for switch{}'s


    //
    // Do what we can to open the serial port.
    // 

    cout << "Trying to initialize the serial port"  << endl;
    
    // Close the serial port first just to be safe.
    if (serialPortOpen == TRUE) {
        cout << " Closing serial port\n";
        serial_close(STEER_SERIAL_PORT);
        serialPortOpen = FALSE;
    }

        
    // Open the serial port or die trying.
    if (serial_open_advanced(
                STEER_SERIAL_PORT, 
                B9600, 
                (SR_PARITY_DISABLE | SR_SOFT_FLOW_INPUT | SR_SOFT_FLOW_OUTPUT | SR_ONE_STOP_BIT)
                ) != 0) 
    {
        fprintf(stderr, "[ERROR] %s (%d): Cannot open Serial Port (%d)!\n",
                __FILE__, __LINE__, STEER_SERIAL_PORT);
        return ERROR;
    }
    else {
        serialPortOpen = TRUE;
    }

    //
    // Start trying to talk to and initialize controller.
    //

    if (0) {
//// "IF CONTROLLER==OFF" 
        // The Controller is not powered on, so we should do that.
//// "SEND MESSAGE "POWERON CONTROLLER"
        justPoweredOn = TRUE;
        /* Now, we wait to get woken up by the the serial code, but if we 
           timeout, the controller must not have powered up and we should
           return an error */
        if (sleep(POWERON_TIMEOUT) == 0) {
            // We slept and nothing happened, so die.
            cout << "Steering timeout on powerup, unrecoverable." << endl;
        }
    }



    //
    // Test Communication with the controller, assuming serial works.
    //

    if (justPoweredOn == FALSE) {
        /* The controller has power, but we don't know for how long.  We
           have to assume it has just been turned on and wait that whole time
           before trying to send/receive any commands. */
        status = steer_test_communication(TRUE);
    }
    else {
        status = steer_test_communication(FALSE);
    }

    switch (status) {
        case TRUE:
            // Communication works!
            break;
        case FALSE:
        case ERROR:
            // Shit.
            if (justPoweredOn == TRUE) {
                // We're really screwed, since we just restarted the machine.
                cout << "Steering is UNRECOVERABLY DEAD at initialization!!" << endl;
            }
            else {
                // At least we can power off and try again ...
                

                // !!! for now, RESET should suffice.
                steer_exec_cmd(RESET, verbosity_exec_cmd);
                cout << "Steering communication FAILED, rebooting controller" << endl;
//// "SEND MESSAGE "POWEROFF CONTROLLER"
//// "WAIT FOR CONFIRMATION"
                return init_steer();
            }
            break;
        default:
            steer_errno = ERR_ILL_RETVAL;
            return ERROR;
            break;
    }
    

    //
    // If the drive cannot enable, load the configuration file and check for
    // success.
    //
    steer_exec_cmd(S_DRIVE1, verbosity_exec_cmd);

    if (steer_exec_cmd(C_DRIVE1, verbosity_exec_cmd) != TRUE) {
//... !!!
        cout << "The drive is *not* enabled!!!" << endl;
        switch (steer_upload_file( (string) "./textInputFiles/be344ljInit.txt" )) {
            case TRUE:
                cout << "File was loaded successfully" << endl;
                // File loaded successfully, continue.
                break;
            case ERROR:
                cout << "We have some error with the upload" << endl;
                switch (steer_errno) {
                    case ERR_SERIAL_IO:
                        if (justPoweredOn == TRUE) {
                            cout << "Steering has LOST serial communication in the course of initialization, so die." << endl;
                            // !!! This might not be the desired behavior !!!
                            return ERROR;
                        } 
                        else {
//// "TURN POWER OFF AND WAIT"                
                            sleep(POWERON_TIMEOUT);
                            return init_steer();
                        }
                    case ERR_ARG_RANGE:
                    case ERR_ILL_RETVAL:
                    case ERR_FILE_IO:
                        cout << "Can't configure drive from file, trying to see if it's already configured." << endl;
                        if (steer_exec_cmd(C_DRIVE0, verbosity_exec_cmd) == TRUE) {
                            // We're in luck, so continue.
                            break;
                        } 
                        else {
                            // We're Screwed.
                            cout << "Steering is NOT configurable ... aborting!!" << endl;
                            return ERROR;
                        }
                        break;
                    case ERR_NONE:
                    default:
                        cout << "Illegal error value" << endl;
                        steer_errno = ERR_ILL_RETVAL;
                        // !!! is this the correct behavior?
                        return ERROR;
                        break;
                }
                break;
            case FALSE:
            default:
                steer_errno = ERR_ILL_RETVAL;
                return ERROR;
                break;
        }

        // The file was loaded; RESET establish communication if possible.
        steer_exec_cmd(RESET, verbosity_exec_cmd);
        sleep(POWERON_TIMEOUT);

        // Inspect our communication state.
        switch (steer_test_communication(TRUE)) {
            case TRUE:
                cout << "File loaded and controller reset successfully!" << endl;
                break;
            default:
                if (justPoweredOn == TRUE) {
                    // We can't do anything more to reset that powercycle.
                    cout << "Could not recover from RESET after power on, aborting!" << endl;
                    return ERROR;
                } 
                else {
                    // We can try recycling power as a last resort.
                    cout << "Could not recover from RESET, cycling power!" << endl;
//// "TURN POWER OFF"
                    return init_steer();
                }
                break;
        }
    } 
    else {
        // Don't need to load configuration file since we can enable drive.
    }
            
//...
//... we should be ready to start just loading commands (communication seems fine), but temperature or other checks might be appropriate here.

    steer_exec_cmd(S_DRIVE1, verbosity_exec_cmd);
    if (steer_exec_cmd(C_DRIVE1, verbosity_exec_cmd) != TRUE) {
        cout << "Drive did not enable for some reason" << endl;
        steer_errno = ERR_ILL_RETVAL;
        return ERROR;
    }

    //
    // At This point, we assume that we have communication until at least the
    // thing that we try to execute in this function (it would be silly to
    // check after *every* command).
    //
    // We proceed by initializing all controller parameters and settings.
    //

    // 
    // Get the home position.
    //

    // Prepare for actual home command.
    steer_exec_cmd(S_MA1, verbosity_exec_cmd);
    steer_exec_cmd(S_LH3, verbosity_exec_cmd);
    steer_exec_cmd(S_LS0, verbosity_exec_cmd);

    // Find the home position.
    cout << "Homing the steering ..." << endl;
    steer_exec_cmd(HOM0, verbosity_exec_cmd);

    // Wait until the home position has been reached.
    while (steer_state_moving(TRUE) == TRUE || steer_state_home_complete(TRUE) == FALSE) {
// ... provide some way of breaking out of this and trying to initialize the steering.
      continue;
    }
    cout << "Homing procedure finished." << endl; fflush(stdout);
    
    // 
    // Set the software limits
    //

    // Use slow accelerations and velocities for this... 
    steer_exec_cmd(S_V4, verbosity_exec_cmd);
    steer_exec_cmd(S_A10, verbosity_exec_cmd);

    // Actually get the software limits.
    cout << "Trying to set the Software Limits using steer_set_sw_limits()" << endl;
    steer_set_sw_limits();

    // Make sure that the wheels aren't moving.
    // ...

    // Now, finish up initialization...
    steer_exec_cmd(S_LS3, verbosity_exec_cmd);
    steer_exec_cmd(S_COMEXC1, verbosity_exec_cmd);
    steer_exec_cmd(S_COMEXL1, verbosity_exec_cmd);
    steer_exec_cmd(C_MA1, verbosity_exec_cmd);
    steer_exec_cmd(S_A20, verbosity_exec_cmd);
    steer_exec_cmd(S_V8, verbosity_exec_cmd);
    //steer_exec_cmd(S_V16, verbosity_exec_cmd);
    steer_heading(0);
    steer_exec_cmd(WAIT_MOVE_DONE, verbosity_exec_cmd);

    // Wait until movement has ceased (this shouldn't be necessary).
    // ...

    cout << "End of initialization function reached ... yippee!!!" << endl;

    return TRUE;
}

/******************************************************************************
 * steer_exec_cmd
 *
 * Description:
 *      This function executes a command corresponding to the integer passed.
 *      It waits for the response from the servo.  It then compares the return
 *      string with the expected one.
 *
 * Arguments:
 *      verbose         integer that when TRUE causes verbose output to be
 *                      generated.
 *
 * Known Limitations / Bugs
 *      This function is blocking if it cannot write to the serial port.
 *
 * Return Values:
 *      TRUE    returned string matches
 *      FALSE   returned string does not match
 *      ERROR   some error, check the error number
 *****************************************************************************/
int steer_exec_cmd(int cmdIndex, int verbose)
{
    int readLength = 0;                 // Number of chars read from serial.
    char inputBuff[STEER_BUFF_LEN];     // Buffer filled with them chars.
    string inputString;                 // String made from buffer.


    // Check if cmdIndex is within range.
    if (cmdIndex < 0 || cmdIndex >= NUM_STEER_CMDS) {
        fprintf(stderr, "[ERROR] %s (%d): Command is out of range (%d)!\n",
                __FILE__, __LINE__, cmdIndex);
        steer_errno = ERR_ARG_RANGE;
        return ERROR;
    }


    if (verbose == TRUE) {
        // Be openly happy that we can read.
        fprintf(stdout, "[TRYING] %s", steerCmds[cmdIndex].cmd.c_str());
    }


    //
    // Try to write the command.
    // 

    if (serial_write (
                STEER_SERIAL_PORT, 
                (char *) steerCmds[cmdIndex].cmd.c_str(), 
                steerCmds[cmdIndex].cmd.length(), 
                SerialBlock
                ) 
            != (int) steerCmds[cmdIndex].cmd.length())
    {
        /* Bad things should happen if we exit without writing the wrong length
           string */
        fprintf(stderr, "[ERROR] %s (%d): Incomplete serial port write!\n",
                __FILE__, __LINE__);
        steer_errno = ERR_SERIAL_IO;
        return ERROR;
    }

    //
    // We know that the write succeeded, now try to read response if one is
    // expected.
    //
    
    if (steerCmds[cmdIndex].retVal.length() == 0) {
        // We do not expect any return, but read what's there anyway and trash.
        serial_read(STEER_SERIAL_PORT, inputBuff, STEER_BUFF_LEN);
    }
    else {
        // Get the number of characters read and fill buffer with chars.
#if 0
        for (int i = 0; i < STEER_BUFF_LEN-1; i++) {
            usleep(100000);
            if (serial_read (STEER_SERIAL_PORT, inputBuff+i, 1) == 1) {
                cout << i << ": \t" << (int) inputBuff[i] << "\t(" << inputBuff[i] << ")" << endl;
            } 
            else {
                // we don't have anything there anymore, so wait a while to look at stuff.
                cout << "That's all, folks!\n";
                sleep(7);
                break;
            }
            readLength = i+1;
        }
#endif 
        usleep(100000);
        readLength = serial_read_until (STEER_SERIAL_PORT, inputBuff, (char) STEER_EOT, SEC_TIMEOUT, STEER_BUFF_LEN - 1);
        //cout << "readlength is " << readLength << endl;
        // Make this an actual string by appending a terminator.
        inputBuff[readLength] = '\0';               
        // Make string 'cause they are cooler:
        inputString = inputBuff;

        if (readLength <= 0) {
            // Nothing returned and we probably timed out.
            cout << steerCmds[cmdIndex].retVal << endl;
            fprintf(stderr, "[ERROR] %s (%d): No Serial port read (trying %s).\n", 
                    __FILE__, __LINE__, steerCmds[cmdIndex].cmd.c_str());
            return ERROR;
        } 
        else {

            /* Check if we have the expected return value.  Null (zero-length)
               string represents no expected return from the device. */
            if (steerCmds[cmdIndex].retVal.compare ( inputString ) == 0
                    || steerCmds[cmdIndex].retVal.length() == 0 )       
            { 
                /* We have the data we wanted, so output if desired (the output
                   string has a newline, but no carriage return. */
                if (verbose == TRUE) {
                    cout << inputString.substr(0, readLength-1) << endl;;
                }
            }
            else {
                // We have something that simply doesn't match exactly.
                fprintf(stdout, "[UNEXPECTED] %s (%d): inputBuff (",  __FILE__, __LINE__);
                cout << inputString.substr(0, readLength-1)     // Does not have \r.
                    << ") does not match expected retVal ("
                    << steerCmds[cmdIndex].retVal.substr(0, steerCmds[cmdIndex].retVal.length() - 1) // Does not have \r.
                    << ")." <<endl;
                return FALSE;
            }
        }
    }


    return TRUE;
}

/******************************************************************************
 * steer_heading
 *
 * Description:
 *      This function instructs the car to steer the heading, but does not wait
 *      for that heading to actually be achieved until returning.... it assumes
 *      that COMEXC1 has already been issued.
 *
 * Arguments:
 *      float angle     direction: full left is -1, center is 0, full right is
 *                      1.
 *
 * Return Values:
 *      TRUE    in best faith, the code thinks we steered there.
 *      FALSE   [not used]
 *      ERROR   some error, check the error code.
 *****************************************************************************/
int steer_heading (float angle)
{
    char buff[100];

    // Make sure that we aren't out of our limits
// !!! use CONSTANTS instead of {-1,1}
    if (angle < -1 || angle > 1) {
        fprintf(stderr, "[%s %d] Illegal steering directive (%f).\n",
                __FILE__, __LINE__, angle); 
        steer_errno = ERR_ARG_RANGE;
        return ERROR;
    }

    /* Negation because the code convention is neg==>left, while for the motor,
       neg==>right */
    angle *= -1;         

    // Make angle be in counts (native controller unit) rather than normalized:
    if (angle < 0) {
        angle *= abs(negCountLimit);
    } else {
        angle *= abs(posCountLimit);
    }

    // Now, convert to a string to send to the controller.
    buff[0] = 'D';              // command is of form D<pos>
    sprintf(buff+1, "%.0f\n\r", angle);
    if (serial_write (
                STEER_SERIAL_PORT, 
                (char *) buff,
                strlen(buff),
                SerialBlock
                ) 
            != (int) strlen(buff))
    {
        fprintf(stderr, "[%s %d] Not all of string was written.\n", __FILE__, __LINE__); 
        steer_errno = ERR_SERIAL_IO;
        return ERROR;
    }
    if (steer_exec_cmd(GO, FALSE) != TRUE) {
        fprintf(stderr, "[%s %d] steer_exec_cmd() error.\n", __FILE__, __LINE__); 
        return ERROR;
    }
    else {
        // buff has a newline in it.
        printf("Steering commanded = %s", buff+1);
    }

    lastSteeringCommanded = (string) (buff+1);
    return TRUE;
}



/******************************************************************************
 * steer_test_communication
 *
 * Description:
 *      This function tests to see whether or not we have communication with 
 *      the controller.  It will either wait the poweron time of the device or 
 *      immediately try to read/write it.
 *
 * Arguments:
 *      int wait        This is either TRUE or FALSE and decides whether or not
 *                      to wait the poweron timeout.
 *
 * Known Limitations / Bugs
 *      The first while loop will hang forever if the drive keeps sending data
 *      forever.
 *
 * Return Values:
 *      TRUE    communication is good.
 *      FALSE   cannot talk to the controller, but I/O works
 *      ERROR   some error, check the error code.
 *****************************************************************************/
int steer_test_communication (int wait)
{
    int status;

    if (wait == TRUE) {
        // Johnson's code should interrupt us here if we get data.
        sleep(POWERON_TIMEOUT);
    }

    // Empty serial buffer to continue in a clear state
    serial_clean_buffer(STEER_SERIAL_PORT);

    /* Set up communications to be the way we like, so that our command 
       execution gets the right strings back on success (':', '\n', and '\r'
       are legal input command delimiters, see p.4).  
       status is the result of all these commands. */
    status = steer_exec_cmd(STOP, verbosity_exec_cmd);
    status |= steer_exec_cmd(S_EOL, verbosity_exec_cmd);
    status |= steer_exec_cmd(S_EOT, verbosity_exec_cmd);
    status |= steer_exec_cmd(S_ECHO0, verbosity_exec_cmd);
    status |= steer_exec_cmd(S_ERRLVL0, verbosity_exec_cmd);

    /* Check that nothing stupid happened with the serial communications. No 
       point in checking for FALSE because the status wouldn't work with the 
       ORs. */
    if ( (status & ERROR) == ERROR ) {
        // Keep the same error number and return.
        cout << "Communication failed unexpectedly in test" << endl;
        return ERROR;
    }

    /* The status is only TRUE; all good thus far.  Test communciation
       with TREV (could be any other command that returns the same thing,
       regardless of the state of the drive). */
    switch (steer_exec_cmd(C_TREV, verbosity_exec_cmd)) {
        case TRUE:
            cout << "The controller is responding to commands." << endl;
            break;
        case FALSE:
            steer_exec_cmd(C_EOL, verbosity_exec_cmd);
            steer_exec_cmd(C_EOT, verbosity_exec_cmd);
            cout << "Serial communication appears to be working, but controller is not responding." << endl;
            return FALSE;
            break;
        case ERROR:
        default:
            // Shit happened... pass on the error number.
            return ERROR;
            cout << "Communication failed even more unexpectedly in test" << endl;
            break;
    }

    // It seems appropriate to upate the controller state here.
    // steer_state_update(TRUE,TRUE);
    
    return TRUE;
}


/******************************************************************************
 * steer_upload_file
 *
 * Description:
 *      This function uploads the given file to the steering controller.
 *
 * Arguments:
 *      string fileName This is the filename... the current directory is
 *                      assumed to be the path.
 *
 * Known Limitations / Bugs:
 *      This is a blocking function if the serial port cannot be written.
 *
 * Return Values:
 *      TRUE    file is uploaded and spotchecked.
 *      FALSE   [not used]
 *      ERROR   some error, check the error code.
 *****************************************************************************/
int steer_upload_file (string fileName)
{
    string line;
    ifstream infile;
    int readLength;
    char inputBuff[STEER_BUFF_LEN];
    infile.open( fileName.c_str() );

    cout << fileName << endl;

    if (infile.good() != true) {
        cout << "File " << fileName << " could not be opened, aborting." << endl;
        steer_errno = ERR_FILE_IO;
        return ERROR;
    } else {
        cout << "File " << fileName << " was opened successfully." << endl;
    }

    steer_exec_cmd(S_ECHO1, TRUE);
    while( getline(infile, line) ) {
        line += '\n';
        //cout << line;  
        if (serial_write (
                    STEER_SERIAL_PORT, 
                    (char *) line.c_str(),
                    line.length(),
                    SerialNonBlock
                    ) 
                != (int) line.length())
        {
            fprintf(stderr, "[%s %d] Not all of string was written.\n", __FILE__, __LINE__); 
            steer_errno = ERR_SERIAL_IO;
            return ERROR;
        }
    }
    
    // 
    // Now, write a blank line in a blocking fashion so we know that everything
    // has been written.
    //

    // Assume there are no errors.
    serial_write (STEER_SERIAL_PORT, (char *) STEER_EOT, 1, SerialBlock);
    steer_exec_cmd(S_ECHO0, TRUE);
    readLength = serial_read (STEER_SERIAL_PORT, inputBuff, STEER_BUFF_LEN-1);
    inputBuff[readLength] = '\0';
    cout << inputBuff << endl;


    return TRUE;
}



/******************************************************************************
 * steer_set_sw_limits
 *
 * Description:
 *      This function travels to the hardware limits and sets the software
 *      limits inside them.  It assumes that the serial communication is up.
 *      It also sets the limits in this code.
 *
 * Arguments:
 *      [none]
 *
 * Global Variables used:
 *      negCountLimit   Set to the negative sw limit
 *      posCountLimit   Set to the positive sw limit
 *
 * Known Limitations / Bugs:
 *      This is a blocking function if the serial port cannot be written.
 *      lots of magic numbers and little error checking ... actually, pretty
 *      much none.
 *      do NOT call this function when speeds are set high. it might damage
 *      the pow3er steering.
 *
 * Return Values:
 *      TRUE    file is uploaded and spotchecked.
 *      FALSE   [not used]
 *      ERROR   some error, check the error code.
 *****************************************************************************/
int steer_set_sw_limits (void)
{
    string outString;
    stringstream negLimit, posLimit;

    // 
    // Deal with the negative limit first.
    //

    // First, go to the negative limit and then figure out the position.
    outString = (string) "D-500000" + STEER_EOT + "GO" + STEER_EOT;
    serial_write (STEER_SERIAL_PORT, (char *) outString.c_str(), outString.length(), SerialBlock);

    // Wait until we have stopped moving (so we can get the position).
    while ( steer_state_limit_neg(TRUE) == FALSE || steer_state_moving(TRUE) == TRUE) {
// ... A way to break out of here in an emergency to make this loop non-blocking would be nice.
        continue;
    }
    
    // Calculate what we want the software limit to be.
    negCountLimit = steer_state_TPE() + 5000;

    // Set this in the controller as well.
    negLimit << negCountLimit;
    outString = (string) "LSNEG" + negLimit.str() + STEER_EOT;
    serial_write (STEER_SERIAL_PORT, (char *) outString.c_str(), outString.length(), SerialBlock);

    //
    // Deal with the positive limit next.
    //

    // Go to the limit so we can read back the position.
    outString = (string) "D500000" + STEER_EOT + "GO" + STEER_EOT;
    serial_write (STEER_SERIAL_PORT, (char *) outString.c_str(), outString.length(), SerialBlock);

    // Wait until we have stopped moving (so we can get the position).
    while ( steer_state_limit_pos(TRUE) == FALSE || steer_state_moving(TRUE) == TRUE) {
// ... A way to break out of here in an emergency to make this loop non-blocking would be nice.
        continue;
    }
    
    // Calculate the software limit.
    posCountLimit = steer_state_TPE() - 5000;

    // Set this in the controller as well.
    posLimit << posCountLimit;
    outString = (string) "LSPOS" + posLimit.str() + STEER_EOT;
    serial_write (STEER_SERIAL_PORT, (char *) outString.c_str(), outString.length(), SerialBlock);


    cout << "The software limits are now " << posCountLimit << " and " << negCountLimit << "." << endl;

    return TRUE;
}

