MODULE TRANSMISSION
TITLE  'Transmission jerry-rig'

"DEVICE:    GAL20v10

"Description:    The rotary switch for manual control of the transmission
"                is break-before-make.  This means that for a fraction of 
"                a second when changing gears, the transmission will try
"                to drift to neutral.  To prevent this problem, this PAL
"                is used to simulate a break-simultaneous-make switch.
"                Additionally, the GAL is used to decode the 3 bit computer
"                input and switch between computer vs human control. 		 

"Revision History:
"1-aug-2003      Alan Somers    Created

"Pins

CLOCK    pin 1;                      "1MHZ clk box
DB0      pin 2;
DB1      pin 3;
DB2      pin 4;
!swN     pin 5;
!swF     pin 6;
!sw1     pin 7;
!sw2     pin 8;
!sw3     pin 9;
"GND     pin 10
"!OE      pin 11;
Q1       pin   ISTYPE 'reg';
Q2       pin   ISTYPE 'reg';
Q3       pin   ISTYPE 'reg';
"error    pin 17  ISTYPE 'com';
sol      pin 20  ISTYPE 'com';
white    pin 21  ISTYPE 'com';
green    pin 22  ISTYPE 'com';
red      pin 23  ISTYPE 'com';
"VCC      pin 20

data =    [DB2, DB1, DB0];            "input
gstate =  [Q3, Q2, Q1];               "state bits
neutral = [0,  0,  0];
free    = [0,  0,  1];
first   = [0,  1,  0];
second  = [0,  1,  1];
third   = [1,  0,  0];
reverse = [1,  0,  1];
human   = [1,  1,  1];
gear    = [sol, green, white, red];   "output
outN    = [0,      0,     0,   0];
outF    = [0,      0,     0,   1];
out1    = [1,      0,     0,   1];
out2    = [1,      0,     1,   1];
out3    = [1,      1,     0,   1];
outR    = [1,      1,     1,   1];
switch  = [sw3,  sw2,   sw1, swF, swN];    "input
manNR   = [  0,    0,     0,   0,   1];
manF    = [  0,    0,     0,   1,   0];
man1    = [  0,    0,     1,   0,   0];
man2    = [  0,    1,     0,   0,   0];
man3    = [  1,    0,     0,   0,   0];
mansw   = [  0,    0,     0,   0,   0];


EQUATIONS

"error.OE    = OE;
"gear.OE     = 1;
"gstate.OE   = 1;

Q1.CLK       = CLOCK;
Q2.CLK       = CLOCK;
Q3.CLK       = CLOCK;


"because our rotary switch is fucking 2P5T with 10 mechanical positions
"instead of 1P10T like it should be, we have to use state bits 
"also, when the box is first powered on, the transmission switch must be set
"to free (pos2) so the solenoids are off and there is no ambiguity

TRUTH_TABLE
([data, switch, gstate] :> gstate -> gear);
"computer controlled mode
[neutral,  .X., neutral] :> neutral ->  outN;
[neutral,  .X., free   ] :>    free ->  outN;
[neutral,  .X., first  ] :>   first ->  outN;
[neutral,  .X., second ] :>  second ->  outN;
[neutral,  .X., third  ] :>   third ->  outN;
[neutral,  .X., reverse] :> reverse ->  outN;
[   free,  .X., neutral] :> neutral ->  outF;
[   free,  .X.,    free] :>    free ->  outF;
[   free,  .X.,   first] :>   first ->  outF;
[   free,  .X.,  second] :>  second ->  outF;
[   free,  .X.,   third] :>  third  ->  outF;
[   free,  .X., reverse] :>  reverse -> outF;
[  first,  .X., neutral] :>  neutral -> out1;
[  first,  .X.,    free] :>  free   ->  out1;
[  first,  .X.,   first] :>  first  ->  out1;
[  first,  .X.,  second] :>  second ->  out1;
[  first,  .X.,   third] :>  third  ->  out1;
[  first,  .X., reverse] :>  reverse -> out1;
[ second,  .X., neutral] :>  neutral -> out2;
[ second,  .X.,    free] :>  free    -> out2;
[ second,  .X.,   first] :>  first  ->  out2;
[ second,  .X.,  second] :>  second ->  out2;
[ second,  .X.,   third] :>  third  ->  out2;
[ second,  .X., reverse] :>  reverse -> out2;
[  third,  .X., neutral] :>  neutral -> out3;
[  third,  .X.,    free] :>  free    -> out3;
[  third,  .X.,   first] :>  first  ->  out3;
[  third,  .X.,  second] :>  second ->  out3;
[  third,  .X.,   third] :>  third  ->  out3;
[  third,  .X., reverse] :>  reverse -> out3;
[reverse,  .X., neutral] :>  neutral -> outR;
[reverse,  .X.,    free] :>  free    -> outR;
[reverse,  .X.,   first] :>  first   -> outR;
[reverse,  .X.,  second] :>  second  -> outR;
[reverse,  .X.,   third] :>  third   -> outR;
[reverse,  .X., reverse] :>  reverse -> outR;

"Human controlled mode
[  human, manF,     .X.] :> free   -> outF;
[  human, man1,     .X.] :> first  -> out1;
[  human, man2,     .X.] :> second -> out2;
[  human, man3,     .X.] :> third -> out3;

[  human, manNR, neutral] :> neutral -> outN;
[  human, manNR, free] :> neutral -> outN;
[  human, manNR, reverse] :> reverse -> outR;
[  human, manNR, third] :> reverse-> outR;

[  human, mansw,  third] :> third -> out3;
[  human, mansw,  second] :> second -> out2;
[  human, mansw,  first] :> first -> out1;
[  human, mansw,  free] :> free -> outF;
[  human, mansw,  neutral] :> neutral -> outN;
[  human, mansw,  reverse] :> reverse -> outR;



END TRANSMISSION
