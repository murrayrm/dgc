#ifndef LocalPlannerDatum_HH
#define LocalPlannerDatum_HH

struct LocalPlannerDatum {

  //Shared Memory Variables
  int shmid;
  int size;
  int key;
  int shmflg;
  void *map_data;
};

#endif
