#ifndef StateManager_HH
#define StateManager_HH

#include <list>

#include <Misc/State/State.hh>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

// The following functions must be defined by the designer
// of a DGC module.  Their prototypes are necessary here
// so that the kernel can call them.
void STATE_INIT();
void STATE_SHUTDOWN();



class StateManager {
public:
  StateManager();
  ~StateManager();

  // add all states
  int AddState(State& newState);

  int SetNextState(State& st);
  int SetNextState(int stID);

  int SetNextStateInit();
  int SetNextStateShutdown();

  int GetCurrentState();
  int GetNextState();

  void operator()(); // runs the state machine

  // get information about a specific state. If no state
  // is specified, the current state is assumed.
  State StateInfo(int ID = -1);
  
private:

  void ExecuteCurrentState();

  struct {
    std::string ModuleName;
    boost::recursive_mutex stateLock;

    int CurrentState;
    int NextState;
    std::list<State> StateTable;
    int StateCtr;
  } m;
};


#endif
