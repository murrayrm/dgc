#include "LRToken.hh"
#include <map>
#include <string>
#include <fstream>
#include <stdio.h>
#include <iostream>

using namespace std;

string& trim(string in) {

  while(in[0] == ' ') {
    in = in.substr(1);
  }

  while(in[in.size()-1] == ' ') {
    in = in.substr(0, in.size()-1);
  }

  return in;
}

map<string, string> LRTokenize(string filename) {

  ifstream infile(filename.c_str());
  string ln;
  int x,y;
  map <string, string> ret;  
  string left, right;

  while(!infile.eof()) {
    getline(infile, ln);
    // first get rid of comments
    x = ln.find('#');
    if( x > 0) {
      ln = ln.substr(0, x);
      cout << ln << endl;    
    }
    
    x = ln.find('=');
    if( x < 0 ) // just ignore this line
      {}
    else {
      left  = trim(ln.substr(0, x-1));
      right = trim(ln.substr(x+1));
      //      cout << "Left: \"" << left  << "\"" << endl;
      //      cout << "Right: \"" << right << "\""<< endl;
      //
      //      cout << "Trimmed" << endl;
      //      cout << "Left: \"" << trim(left)  << "\"" << endl;
      //      cout << "Right: \"" << trim(right) << "\""<< endl;
      ret[left] = right;
    }
  }


  return ret;
}

