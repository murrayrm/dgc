#ifndef IP_HH
#define IP_HH


typedef unsigned int IP_ADDRESS;

struct TCP_ADDRESS {
  unsigned int IP;
  unsigned short PORT;
};

#endif
