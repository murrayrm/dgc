#ifndef Message_HH
#define Message_HH

#include <string>
#include "../Memory/ToMemory.hh"
#include "../Memory/FromMemory.hh"

struct MessageHeader {
  TCP_ADDRESS To, From;
  int Size;
  int Type;
};


class Message {
public:
  Message();
  Message(const Message& rhs);
  ~Message();

  void From(ModAddress f);
  void To(ModAddress t);

  void TimeStamp();
  void Flags(unsigned int f);

  // operators to append data to a message
  Message& operator << (const float& RHS);
  Message& operator << (const double& RHS);
  Message& operator << (const int& RHS);
  Message& operator << (const unsigned int& RHS);
  Message& operator << (const short& RHS);
  Message& operator << (const unsigned short& RHS);
  Message& operator << (const long& RHS);
  Message& operator << (const unsigned long& RHS);
  //  Message& operator << (const std::string& RHS);
  //  Message& operator << (const FromMemory& RHS);
  //  Message& operator << (const FromFile& RHS);

  // Functions to read back data from message
  Message& operator >> (float& RHS);
  Message& operator >> (double& RHS);
  Message& operator >> (int& RHS);
  Message& operator >> (unsigned int& RHS);
  Message& operator >> (short& RHS);
  Message& operator >> (unsigned short& RHS);
  Message& operator >> (long& RHS);
  Message& operator >> (unsigned long& RHS);
  //  Message& operator >> (std::string& RHS);
  //Message& operator >> (ToMemory& RHS);
  //Message& operator >> (ToFile& RHS);

private:
  struct {
    MessageHeader mh;
    unsigned int flags;
    unsigned int status;
    Stream data;
  } m;
};


namespace MessageFlags {
  const int BadMessage = 1;
  const int QueryMessage = 2;
  const int RetQueryMessage = 4;
  const int KernelMessage = 8;

  const int MessageUndeliverable;
  const int 
}; 

#endif
