#include "Mail.hh"
#include <iostream>

using namespace std;

//friend int operator >> (File  fd, Mail & ml) {

Mail::Mail(File& fd) {
  // download:
  // : To, From
  // : Type of msg
  // : Flags
  // : size of message
  // : the message
  M_Data_In  = shared_ptr<BufferedPipe> (new BufferedPipe(DEF::BlockSize));
  M_Data_Out = shared_ptr<BufferedPipe> (new BufferedPipe(DEF::BlockSize));

  unsigned int sizeofmsg;
  int bytesUnread;

  //  cout << "Trying to read in mail" << endl;

  if( fd.BRead((void*) &M_To, sizeof(MailAddress))) {
    // keep going if read properly
    //    cout << "to successful" << endl;
    if( fd.BRead((void*) &M_From, sizeof(MailAddress))) {
      //      cout << "from successful" << endl;
      if(fd.BRead((void*) &M_MsgType, sizeof(int))) {
	//	cout << "type successful = " << M_MsgType << endl;
	if(fd.BRead((void*) &M_Flags, sizeof(unsigned int))) {
	  //	  cout << "flags successful = " << M_Flags << endl;
	  if(fd.BRead((void*) &sizeofmsg, sizeof(sizeofmsg))) {
	    //	    cout << "sizeofmsg = " << sizeofmsg << endl;
	    
	    if( (*M_Data_In).getFromFile(fd.FD(), sizeofmsg) == sizeofmsg) {
	      
	      /*
	      cout << "Mail: Reading File " << fd.FD()
		   << ": From:" << M_From.TCP.IP
		   << ":"  << M_From.TCP.PORT 
		   << ":"  << M_From.MAILBOX
		   << ", " << M_MsgType 
		   << ", " << M_Flags
		   << ", " << sizeofmsg << endl;
	      */
	      // we are all good
	      M_Status = true;
	      return;
	    }
	  }
	}
      }
    }
  }
  // else set error
  M_Status = false;
  
}

int Mail::ToFile(File& fd) {
  // upload:
  // : To, From
  // : Type of msg
  // : Flags
  // : size of message
  // : the message
  unsigned int sizeofmsg = (*M_Data_Out).size();
  /*
  cout << "Mail: Flushing File " << fd.FD()
       << ": To:" << M_To.TCP.IP
       << ":"  << M_To.TCP.PORT 
       << ":"  << M_To.MAILBOX
       << ", " << M_MsgType 
       << ", " << M_Flags
       << ", " << sizeofmsg << endl;
  */
  fd.BWrite((void*) &M_To,      sizeof(MailAddress));
  fd.BWrite((void*) &M_From,    sizeof(MailAddress)); 
  fd.BWrite((void*) &M_MsgType, sizeof(int)); 
  fd.BWrite((void*) &M_Flags,   sizeof(unsigned int));
  fd.BWrite((void*) &sizeofmsg, sizeof(sizeofmsg));
	    
  (*M_Data_Out).flushToFile(  fd.FD()  );

  //  FlushWrite(fd.FD());
  // we are all good
  M_Status = false;

  return true;
}


Mail::Mail(MailAddress From, MailAddress To, int msgtype, int flags) {
  
  M_Data_In  = shared_ptr<BufferedPipe> (new BufferedPipe(DEF::BlockSize));
  M_Data_Out = shared_ptr<BufferedPipe> (new BufferedPipe(DEF::BlockSize));
  
  M_From = From;
  M_To   = To;
  M_MsgType = msgtype;

  M_Flags = flags;

  if( M_Flags & (int) MailFlags::BadMessage ) {
    M_Status = false;
  } else {
    M_Status = true;
  }


}

Mail::Mail(const Mail& rhs) {
  M_Data_In   = rhs.M_Data_In;
  M_Data_Out  = rhs.M_Data_Out;

  M_From      = rhs.M_From;
  M_To        = rhs.M_To;

  M_Flags     = rhs.M_Flags;
  M_Status    = rhs.M_Status;
  M_MsgType   = rhs.M_MsgType;

}

Mail::~Mail() {

}  

MailAddress Mail::From() {
  return M_From;
}

MailAddress Mail::To() {
  return M_To;
}

unsigned int Mail::Flags() {
  return M_Flags;
}

int Mail::MsgType() {
  return M_MsgType;
}

unsigned int Mail::Size() {
  return (*M_Data_In).size();
}

int Mail::OK() {
  return M_Status;
}

int Mail::QueueTo (const void * ptr, int size) {
  return (*M_Data_Out).push(ptr, size);
}

int Mail::DequeueFrom( void* ptr, int size) {
  return (*M_Data_In).pop(ptr, size);
}


// operators to append data to a message
Mail& Mail::operator << (const float& RHS) {
  QueueTo( &RHS, sizeof(RHS));
  return (*this);
}
Mail& Mail::operator << (const double& RHS) {
  QueueTo( &RHS, sizeof(RHS));
  return (*this);
}
Mail& Mail::operator << (const int& RHS) {
  QueueTo( &RHS, sizeof(RHS));
  return (*this);
}
Mail& Mail::operator << (const unsigned int& RHS) {
  QueueTo( &RHS, sizeof(RHS));
  return (*this);
}
Mail& Mail::operator << (const short& RHS) {
  QueueTo( &RHS, sizeof(RHS));
  return (*this);
}
Mail& Mail::operator << (const unsigned short& RHS) {
  QueueTo( &RHS, sizeof(RHS));
  return (*this);
}
Mail& Mail::operator << (const long& RHS) {
  QueueTo( &RHS, sizeof(RHS));
  return (*this);
}
Mail& Mail::operator << (const unsigned long& RHS) {
  QueueTo( &RHS, sizeof(RHS));
  return (*this);
}

// Functions to read back data from message
Mail& Mail::operator >> (float& RHS) {
  DequeueFrom( &RHS, sizeof(RHS));
  return (*this);
}
Mail& Mail::operator >> (double& RHS) {
  DequeueFrom( &RHS, sizeof(RHS));
  return (*this);
}
Mail& Mail::operator >> (int& RHS) {
  DequeueFrom( &RHS, sizeof(RHS));
  return (*this);
}
Mail& Mail::operator >> (unsigned int& RHS) {
  DequeueFrom( &RHS, sizeof(RHS));
  return (*this);
}
Mail& Mail::operator >> (short& RHS) {
  DequeueFrom( &RHS, sizeof(RHS));
  return (*this);
}
Mail& Mail::operator >> (unsigned short& RHS) {
  DequeueFrom( &RHS, sizeof(RHS));
  return (*this);
}
Mail& Mail::operator >> (long& RHS) { 
  DequeueFrom( &RHS, sizeof(RHS));
  return (*this);
}
Mail& Mail::operator >> (unsigned long& RHS) {
  DequeueFrom( &RHS, sizeof(RHS));
  return (*this);
}


Mail ReplyToQuery(Mail& ml, int flags) {
  return Mail( ml.To(), ml.From(), 0 /* msg type doesnt matter */, flags);
}

