#ifndef Mail_HH
#define Mail_HH

#include <boost/shared_ptr.hpp>
#include <string>
#include "../../KernelDef.hh"
#include "../File/File.hh"
#include "../Pipes/BufferedPipe.hh"
#include "../Structures/IP.hh"
#include "../Structures/MailAddress.hh"



using namespace std;
using namespace boost; //::shared_ptr;




class Mail {
public:

  Mail(File& in);    // reads in from socket
  int ToFile(File& out); // flushes to file


  Mail(MailAddress From, MailAddress To, int msgtype, int flags = 0);
  Mail(const Mail& rhs);
  ~Mail();

  MailAddress From();
  MailAddress To();
  unsigned int Flags();
  int MsgType();
  unsigned int Size();
  int OK();

  int QueueTo    ( const void* ptr, int size);
  int DequeueFrom( void* ptr, int size);

  /*
  struct x {
    int y; int z;
    float q;
  };
  x d;
  ml << d.y << d.z << d.q; // send 
  ml >> d.y >> d.z >> d.q; // receive
  */
  // operators to append data to a message
  Mail& operator << (const float& RHS);
  Mail& operator << (const double& RHS);
  Mail& operator << (const int& RHS);
  Mail& operator << (const unsigned int& RHS);
  Mail& operator << (const short& RHS);
  Mail& operator << (const unsigned short& RHS);
  Mail& operator << (const long& RHS);
  Mail& operator << (const unsigned long& RHS);

  // Functions to read back data from message
  Mail& operator >> (float& RHS);
  Mail& operator >> (double& RHS);
  Mail& operator >> (int& RHS);
  Mail& operator >> (unsigned int& RHS);
  Mail& operator >> (short& RHS);
  Mail& operator >> (unsigned short& RHS);
  Mail& operator >> (long& RHS);
  Mail& operator >> (unsigned long& RHS);

private:
  MailAddress  M_From, M_To;
  unsigned int M_Flags;
  unsigned int M_Status;
  int          M_MsgType;
  shared_ptr<BufferedPipe> M_Data_In;
  shared_ptr<BufferedPipe> M_Data_Out;
};


// just generic functions 
Mail ReplyToQuery(Mail& ml, int MsgFlags = 0);



namespace MailFlags {
  const int BadMessage = 1;
  const int QueryMessage = 2;

  // const int MessageUndeliverable = 16;
  //  const int 
}; 

#endif
