#include "BufferedPipe.hh"
#include "../Data/Constants.hh"
#include "../File/File.hh"
#include "../Time/Time.hh"
#include <time.h>
#include <sys/time.h>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>


#include <iostream>
#include <list>
#include <cstring>

using namespace std;

BufferedPipe::BufferedPipe(int block_size) {
  myBlockSize = block_size;
  mySize = 0;
}

BufferedPipe::~BufferedPipe() {
  destruct();
}

void BufferedPipe::destruct() {
  while( myData.size() > 0 ) {
    myData.pop_front();
  }
}

int BufferedPipe::size() {
  return mySize;
}

int BufferedPipe::getFromFile(File & FD, int bytes) {
  return getFromFile( FD.FD(), bytes);

}
int BufferedPipe::getFromFile(int FD, int bytes) {
  File f(FD, FALSE); // we are not the owner of the file
  boost::recursive_mutex::scoped_lock sl(mutex);

  timeval before, after;
  gettimeofday(&before, NULL);

  char* buffer;
  SharedMemoryContainer*  p_smc;  
  int toWrite;
  int bytes_read;
  int bytes_to_write = bytes;

  while( bytes > 0) {
    if( myData.size() == 0 || myData.back().leftToWrite() <=0) {
      NewSMC(); // allocate a new buffer
    }
    p_smc = &myData.back();
    toWrite = (*p_smc).leftToWrite();
    toWrite = (toWrite < bytes)? toWrite: bytes;
    bytes_read = f.BRead( p_smc->sliderR(), toWrite);
    //    cout << "Bytes read = " << bytes_read << endl;
    if( bytes_read < 0 ) {
      cout << "Buffered Pipe File Read Error" << endl;
      return ERROR;
    } else if(bytes_read == 0) {
      gettimeofday(&after, NULL);
      if( timeval_subtract(after, before) > 0.1) {
	return bytes_to_write - bytes;
      }


    } else { 
      (*p_smc).wrote(bytes_read);
      bytes  -= bytes_read;
      buffer += bytes_read;
      mySize += bytes_read;
      gettimeofday(&before, NULL);
    }
  }
  //  cout << "Buffered Pipe Returns " << bytes_to_write << " bytes." << endl;
  return bytes_to_write - bytes;
}


int BufferedPipe::flushToFile(File &FD, int bytes) {
  //  boost::recursive_mutex::scoped_lock sl(mutex);
  return flushToFile(FD.FD(), bytes);
}


int BufferedPipe::flushToFile(int FD, int bytesToWrite) {
  if (bytesToWrite < 0) bytesToWrite = mySize;

  File f(FD, FALSE); // we are not the owner of the file
  boost::recursive_mutex::scoped_lock sl(mutex);

  SharedMemoryContainer*  p_smc;  
  size_t toWrite;
  int bytes_written, empty=FALSE;
  size_t totalBytes = bytesToWrite;

  while( bytesToWrite > 0 && empty == FALSE) {
    if( myData.empty()) {
      empty = TRUE;
    } else {

      p_smc = &myData.front();
      if( p_smc->leftToRead() == 0 && p_smc->leftToWrite() == 0) {
	myData.pop_front();

      } else if(p_smc->leftToRead() == 0) {
	empty = TRUE;
      } else {
	
	toWrite = p_smc->leftToRead();
	toWrite = (toWrite < bytesToWrite)? toWrite: bytesToWrite;

	bytes_written = f.BWrite(p_smc->sliderL(), toWrite);
	p_smc->read(bytes_written);
	bytesToWrite -= bytes_written;
	mySize       -= bytes_written;
      }
    }
  }

  return totalBytes-bytesToWrite;
}


int BufferedPipe::pop(void* buf, int bytes) {

  char* buffer = (char*) buf;
  SharedMemoryContainer *smc;  
  size_t toWrite;
  int bytesToWrite = bytes, empty = FALSE;

  boost::recursive_mutex::scoped_lock sl(mutex);
  while( bytesToWrite > 0 && empty == FALSE) {
    if( myData.empty()) {
      empty = TRUE;
    } else {

      smc = &myData.front();
      if( smc->leftToRead() == 0 && smc->leftToWrite() == 0) {
	myData.pop_front();

      } else if(smc->leftToRead() == 0) {
	empty = TRUE;
      } else {
	
	toWrite = smc->leftToRead();
	toWrite = (toWrite < bytesToWrite)? toWrite: bytesToWrite;

	memcpy( buffer, smc->sliderL(), toWrite);
	smc->read(toWrite);
	bytesToWrite -= toWrite;
	buffer       += toWrite;
	mySize       -= toWrite;
      }
    }
  }

  return bytes-bytesToWrite;
}

int BufferedPipe::push(const void* mem, int bytes) {

  char* buffer = (char*) mem;
  SharedMemoryContainer*  p_smc;  
  size_t toWrite;
  boost::recursive_mutex::scoped_lock sl(mutex);
  while( bytes > 0) {
    if( myData.size() == 0 || myData.back().leftToWrite() <=0) {
      NewSMC(); // allocate a new buffer
    }
    p_smc = &myData.back();
    toWrite = (*p_smc).leftToWrite();
    toWrite = (toWrite < bytes)? toWrite: bytes;
    memcpy( (*p_smc).sliderR(), buffer, toWrite);
    (*p_smc).wrote(toWrite);
    bytes  -= toWrite;
    buffer += toWrite;
    mySize += toWrite;
  }
  return TRUE;
}


int BufferedPipe::NewSMC() {
  myData.push_back( SharedMemoryContainer( new char[myBlockSize], myBlockSize));

  return TRUE;
}
