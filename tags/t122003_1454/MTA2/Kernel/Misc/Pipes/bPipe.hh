//#include "misc/Memory/SharedMemoryContainer.hh"
#include <list>

class bPipe {
  
public:
  bPipe( unsigned int block_size = 1000 );
  ~bPipe();
  void destruct();

  unsigned int size();
  int flushToFile(int FD, int bytes=-1);
  int readFromFile(int FD, int bytes);
  
  bPipe& operator << (const int& RHS);
  bPipe& operator << (const unsigned int& RHS);
  bPipe& operator << (const short& RHS);
  bPipe& operator << (const long& RHS);
  bPipe& operator << (const char& RHS);
  bPipe& operator << (const float& RHS);
  bPipe& operator << (const double& RHS);
  bPipe& operator << (const std::string& RHS);

  
  bPipe& operator >> (int& RHS);
  bPipe& operator >> (unsigned int& RHS);
  bPipe& operator >> (short& RHS);
  bPipe& operator >> (long& RHS);
  bPipe& operator >> (char& RHS);
  bPipe& operator >> (float& RHS);
  bPipe& operator >> (double& RHS);
  bPipe& operator >> (std::string& RHS);


private:
  //  std::list<SharedMemoryContainer> dataQueue;
  unsigned long bytesAllocated;
  unsigned int  blockSize;

  int pop(const void* buffer, const int bytes);
  int push(const void* buffer, const int bytes);
};

