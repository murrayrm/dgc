#ifndef BufferedPipe_HH
#define BufferedPipe_HH

#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include <list>
#include <stddef.h>
#include "Pipe.hh"
#include "../Memory/SharedMemoryContainer.hh"
#include "../File/File.hh"


class BufferedPipe { //:Pipe  {
  
public:
  BufferedPipe(int myBlockSize=100);
  ~BufferedPipe();
  void destruct();
  int size();
  int getFromFile(int FD,   int bytes);    // returns the # of bytes read or -1
  int getFromFile(File& FD, int bytes);    // for error
                                           
  int flushToFile(int FD,   int bytes=-1); // returns # of bytes remaining
  int flushToFile(File& FD, int bytes=-1); // after flushing "bytes" bytes. If 
                                           // bytes<0 it will attempt to flush all.

  int pop(void* buffer, const int bytes);
  int push(const void* buffer, const int bytes);

private:
  std::list<SharedMemoryContainer> myData;
  int NewSMC();
  size_t mySize;
  size_t myBlockSize;
  boost::recursive_mutex mutex;
};

#endif
