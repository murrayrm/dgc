#ifndef ErrorContext_HH
#define ErrorContext_HH

#include <string>
#include <map>
#include <vector>
#include <stddef.h>


class ErrorCodes {
public:
  ErrorCodes();
  ~ErrorCodes();

  int add(std::string);
  int get(int code, std::string& fill);

private:
  int nextEC();

  boost::recursive_mutex mutex;
  int counter;
  std::map<std::string, int> ints;
  std::vector<std::string> strings;
};




class ErrorContext {
  
public:
  ErrorContext(int code);
  ~ErrorContext();

  int getCode();
  std::string getString();
  void* getPtr();

private:

  std::string myString;
  int myCode;
  void* myPtr;

};

#endif
