#ifndef ToMemory_HH
#define ToMemory_HH

#include "FromMemory.hh"

// from and to memory simply contain a pointer 
// with a size parameter attached, so just alias
// one to the other
typedef FromMemory ToMemory;
