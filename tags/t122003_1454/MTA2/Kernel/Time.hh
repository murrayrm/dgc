#ifndef Time_HH
#define Time_HH

#include "Misc/Time/Timeval.hh"


namespace KERNEL {
  class Time {
  public:
    Time();
    ~Time();

    void operator ()();
    Timeval Now();

  private:
    Timeval M_TimeZero;

  };

};
#endif
