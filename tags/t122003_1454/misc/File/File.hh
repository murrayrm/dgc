#ifndef File_HH
#define File_HH

#include <boost/shared_ptr.hpp>

class FileHandle {
private:
  int my_fd;
  int IAMOwner;
public:
  FileHandle(int fd, int owner=true);
  ~FileHandle();
  int FD();
};


class File {
public:
  File();
  File(const File&);
  //  File(std::string filename, int flags= O_CREAT | O_TRUNC);
  File(int fd, int owner=true); // if owner is true, this will automatically 
  ~File();                       // close the file on destruction

  int OTRead (void* buffer, int bytes); // one time read up to 'bytes' bytes
  int OTWrite(void* buffer, int bytes); // one time write, returns bytes written
  int BRead  (void* buffer, int bytes, double timeout=0.1); // blocking read with timeout
  int BWrite (void* buffer, int bytes, double timeout=0.1); // timeout in seconds.

  int FD(); // returns file descriptor

  //  friend int operator >> (File& fl, Mail & ml);

protected: // so that derived classes may use these
    

  boost::shared_ptr<FileHandle> myFH;

};
 


#endif
