#include <stdio.h>
#include "File.hh"
#include <time.h>
#include <sys/time.h>
#include "../Time/Time.hh"

using namespace std;
using namespace boost;

FileHandle::FileHandle(int fd, int owner) {
  my_fd = fd;
  IAMOwner = owner;
}

FileHandle::~FileHandle() {
  if (my_fd >= 0 && IAMOwner) {
    close(my_fd);
  }
}

int FileHandle::FD() {
  return my_fd;
}

File::File() {
  myFH = shared_ptr<FileHandle>(new FileHandle(-1, false));
}

/*
File::File(string filename, int flags) {
  IAmOwner = TRUE;
  myFD = open(filename.c_str(), flags);
  insert();
}
*/

File::File(int fd, int owner) {
  myFH = shared_ptr<FileHandle>(new FileHandle(fd, owner));
}

File::File(const File& f) {
  myFH = f.myFH;
}

File::~File() {
}

int File::OTRead(void* buffer, int bytes) {
  int myFD = FD();

  if( bytes >= 0 && myFD >=0) {
    return read(myFD, buffer, bytes);
  } else {
    return -1;
  }
}


int File::OTWrite(void* buffer, int bytes) {
  int myFD = FD();

  if( bytes >= 0 && myFD >=0) {
    return write(myFD, buffer, bytes);
  } else {
    return -1;
  }
}





int File::BRead(void* buf, int bytes, double timeout) {
  char* buffer = (char*) buf;
  timeval before, after;
  gettimeofday(&before, NULL);
  int bytes_read=0;
  int r;
  while( bytes_read < bytes) {
    r = OTRead(buffer+bytes_read, bytes-bytes_read);
    if ( r<0) return r; //bytes_read;
    else if(r == 0) {
      gettimeofday(&after, NULL);
      if( timeval_subtract(after, before) > timeout) {
	return bytes_read;
      }
    } else {
      gettimeofday(&before, NULL);
      bytes_read += r;
    }
  }
}

int File::BWrite(void* buf, int bytes, double timeout) {
  char* buffer = (char*) buf;
  timeval before, after;
  gettimeofday(&before, NULL);
  int written=0;
  int r;
  while( written < bytes) {
    r = OTWrite(buffer+written, bytes-written);
    if ( r<0) return r; //read;
    else if(r == 0) {
      gettimeofday(&after, NULL);
      if( timeval_subtract(after, before) > timeout) {
	return written; //read;
      }
    } else {
      gettimeofday(&before, NULL);
      written += r;
    }
  }
}

int File::FD() {
  return (*myFH).FD();
}
