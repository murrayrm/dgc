#ifndef BufferedPipe_HH
#define BufferedPipe_HH

#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include <list>
#include <stddef.h>
#include "Pipe.hh"
#include "../Memory/SharedMemoryContainer.hh"
#include "../File/File.hh"


class BufferedPipe { //:Pipe  {
  
public:
  BufferedPipe(size_t myBlockSize=100);
  ~BufferedPipe();
  void destruct();
  size_t size();
  int getFromFile(int FD,   size_t bytes); // returns the # of bytes read or -1
  int getFromFile(File& FD, size_t bytes); // for error
                                           
  int flushToFile(int FD,   int bytes=-1); // returns # of bytes remaining
  int flushToFile(File& FD, int bytes=-1); // after flushing "bytes" bytes. If 
                                           // bytes<0 it will attempt to flush all.

  int pop(void* buffer, const size_t bytes);
  int push(const void* buffer, const size_t bytes);

private:
  std::list<SharedMemoryContainer> myData;
  int NewSMC();
  size_t mySize;
  size_t myBlockSize;
  boost::recursive_mutex mutex;
};

#endif
