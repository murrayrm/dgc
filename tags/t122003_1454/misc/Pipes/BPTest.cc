#include "BufferedPipe.hh"
#include <iostream>

using namespace std;

BufferedPipe myPipe(50000);

int main() {
  int count=0;

  int x = 5, y;
  while (1) {
    for(x=0; x<500000; x++) {
      myPipe.push(&x, sizeof(int));
    }
    x = 12345;
    
    for(y=0; y<500000; y++) {
      myPipe.pop(&x, sizeof(int));
      
    }
    count ++;
    cout << "Count = " << count << endl;
  }
  return 0;
}
