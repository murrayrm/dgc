#include "Socket.hh"
#include "../Time/Time.hh"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <fcntl.h>

#include <time.h>
#include <sys/time.h>

#include <boost/shared_ptr.hpp>

using namespace std;
using namespace boost;

unsigned int IPConvert(char* ip) {
  unsigned int ret;
  inet_aton(ip, (in_addr*) &ret);
  return ret;
}

CSocket::CSocket() {
  myFH = shared_ptr<FileHandle> (new FileHandle(-1, false));
}


CSocket::CSocket(unsigned int ip, int port) {
  Open(ip, port);
}

int CSocket::Open(unsigned int ip, int port) {

  sockaddr_in ctrl_sa;
  socklen_t   ctrl_sl;

  int myFD  = socket(PF_INET, SOCK_STREAM, 0);

  if( myFD >= 0) {
    myFH = shared_ptr<FileHandle>(new FileHandle(myFD, true));

    memset(&ctrl_sa, 0, sizeof(ctrl_sa));
    ctrl_sa.sin_family = AF_INET;
    ctrl_sa.sin_port   = htons(port);
    ctrl_sa.sin_addr.s_addr = ip;
    ctrl_sl = sizeof(ctrl_sa);
    
    if( connect(myFD, (sockaddr*) &ctrl_sa, ctrl_sl)) {
      cout << "Socket Connect Error" << endl;
      myFH = shared_ptr<FileHandle>(new FileHandle(-1, false));;
      return false;
    } else {
      // we are successfully connected
      return myFD;
    }
  } else {
    return false;
  }
}
      
CSocket::CSocket(const CSocket& f) {
  myFH = f.myFH;
}

CSocket::~CSocket() {
}


SSocket::SSocket() {
  myFH = shared_ptr<FileHandle> (new FileHandle(-1, false));
}

SSocket::~SSocket() {

}


int SSocket::Listen(int port) {
  //  cout << "Opening Server Socket" << endl;
  M_port = port;

  int sockfd;
  sockaddr_in srv;
  socklen_t   socklen;

  sockfd = socket(PF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
    cout << "  OpenSSocket: Error - Creating Socket" << endl;
    return false;
  } else {
      myFH = shared_ptr<FileHandle> (new FileHandle(sockfd, false));
  }

  //  setsockopt(sockfd, SOL_SOCKET, 0, &i, sizeof(i));
  memset(&srv, 0, sizeof(srv));

  srv.sin_family = AF_INET;
  srv.sin_port = htons(port);
  socklen = sizeof(srv);

  if((bind(sockfd, ((sockaddr *) &srv), socklen))) {
    cout << "  OpenSSocket: Error - Bind" << endl;
    return false;
  }

  if(listen(sockfd, 5) < 0) {
    cout << "  OpenSSocket: Error - Listen" << endl;
    return false;
  }

  return sockfd;
}


File SSocket::Accept() {
  int sockfd = (*myFH).FD();
  sockaddr sa;
  socklen_t socklen;

  int ret = accept(sockfd, &sa, &socklen);
  if( ret >= 0) {
    return File(sockfd, true);
  }
  else {
    perror("accept");
    return File(-1, false);
  }
}

int SSocket::Port() {
  return M_port;
}
