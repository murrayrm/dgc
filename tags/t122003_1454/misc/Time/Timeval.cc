#include "Timeval.hh"

#include <time.h>
#include <sys/time.h>

Timeval::Timeval(long seconds, long microseconds) {

  myTV.tv_sec = seconds;
  myTV.tv_usec = microseconds;

}

Timeval::Timeval(const timeval & tv2) {
  myTV.tv_sec  = tv2.tv_sec;
  myTV.tv_usec = tv2.tv_usec;
}

Timeval::Timeval(const Timeval& rhs) {

  myTV.tv_sec  = rhs.myTV.tv_sec;
  myTV.tv_usec = rhs.myTV.tv_usec;

}

Timeval Timeval::operator - (const Timeval& rhs) {
  Timeval t = (*this);
  
  if( t.myTV.tv_usec < rhs.myTV.tv_usec ) {
    // borrow from seconds
    t.myTV.tv_usec += 1000000;
    t.myTV.tv_sec -= 1;
  }

  t.myTV.tv_sec  = t.myTV.tv_sec  - rhs.myTV.tv_sec;
  t.myTV.tv_usec = t.myTV.tv_usec - rhs.myTV.tv_usec;

  return t;
}

Timeval TVNow() {
  timeval t;
  gettimeofday(&t, NULL);
  return Timeval(t.tv_sec, t.tv_usec);
};
