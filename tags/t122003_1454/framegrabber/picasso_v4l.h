//$Log: picasso_v4l.h,v $
//Revision 1.1  2003/09/06 00:12:52  chihhao
//framegrabber include files
//
//Revision 1.9  2002/11/01 08:24:34  rini
//diverse kleine veranderingen
//Latency timer toegevoegd
//
//Revision 1.8  2002/05/27 12:24:40  rini
//*** empty log message ***
//
//Revision 1.7  2001/08/31 14:32:19  rini
//sync voor oude 3C toegevoegd en VREF+ - in te stellen
//
//Revision 1.6  2001/04/25 08:27:10  rini
//mogelijkheid tot capturen van velden toegevoegd aan 2sq fam.
//bug uit verticale schaling 2sq gehaald
//
//Revision 1.5  2001/03/28 10:25:20  rini
//enkele wijzigingen ivm kernel 2.4.x. Nog niet werkend !
//
//Revision 1.4  2001/03/23 14:47:42  rini
//Diverse kleine veranderingen
//
//Revision 1.3  2001/03/14 10:01:19  rini
//General Purpose toegevoegd.
//Burstlength threshold toegevoegd
//Debitransfers vervangen door Read/Write Mach register
//
//Revision 1.2  2001/03/13 07:46:56  rini
//cameralink picasso toegevoegd aan rt-driver
//documentatie aangepast
//
//Revision 1.1  2001/03/09 13:32:36  rini
//picasso_v4l.h verplaats naar include directory
//
//Revision 1.2  2001/03/08 09:15:36  rini
//pci_fi toegevoegd
//pci_ls toegevoegd
//uart gedeelte pci_cl toegevoegd
//MaakInstallVersietoegevoegd.
//diverse kleine veranderingen
//
//Revision 1.1.1.1  2001/02/13 07:47:11  rini
//Eerste import
//


#ifndef PICASSO_V4L_H
#define PICASSO_V4L_H

//additional video formats
#define VIDEO_PALETTE_8 100
#define VIDEO_PALETTE_16 108
#define VIDEO_PALETTE_RGBa555	109
#define VIDEO_PALETTE_RGB55a5	110

/* additonal ioctl's */
#define VIDIO_PICSETMODE	        _IOW('v',  BASE_VIDIOCPRIVATE+1, int)

#define VIDIO_PICSETIO  	        _IOW('v',  BASE_VIDIOCPRIVATE+2, int)
#define VIDIO_PICSETEXPOSURE        _IOW('v',  BASE_VIDIOCPRIVATE+3, int)
#define VIDIO_PICSETEXPOSURETIME	_IOW('v',  BASE_VIDIOCPRIVATE+4, int)

#define VIDIO_PICSETRANDOMTRIGGER	_IOW('v',  BASE_VIDIOCPRIVATE+6, int)
#define VIDIO_PICSETLVDSGPOUT		_IOW('v',  BASE_VIDIOCPRIVATE+7, int)
#define VIDIO_PICGETLVDSGPIN		_IOR('v',  BASE_VIDIOCPRIVATE+8, int)
#define VIDIO_PICSETSYNCINPUT		_IOW('v',  BASE_VIDIOCPRIVATE+9, int)

#define VIDIO_PICUARTSELECT			_IOW('v',  BASE_VIDIOCPRIVATE+10, int)
#define VIDIO_PICUARTINIT			_IOW('v',  BASE_VIDIOCPRIVATE+11, uartstruct)
#define VIDIO_PICUARTWRITE			_IOW('v',  BASE_VIDIOCPRIVATE+12, char)
#define VIDIO_PICUARTREAD			_IOR('v',  BASE_VIDIOCPRIVATE+13, char)

#define VIDIO_PICGETGP0				_IOW('v',  BASE_VIDIOCPRIVATE+14, int)
#define VIDIO_PICGETGP1				_IOW('v',  BASE_VIDIOCPRIVATE+15, int)
#define VIDIO_PICSETGP0				_IOR('v',  BASE_VIDIOCPRIVATE+16, int)
#define VIDIO_PICSETGP1				_IOR('v',  BASE_VIDIOCPRIVATE+17, int)

#define VIDIO_PICSETGP0_IRQ			_IOW('v',  BASE_VIDIOCPRIVATE+18, int)
#define VIDIO_PICSETGP1_IRQ			_IOW('v',  BASE_VIDIOCPRIVATE+19, int)

#define VIDIO_PICWAITGP0_IRQ		_IOW('v',  BASE_VIDIOCPRIVATE+20, int)
#define VIDIO_PICWAITGP1_IRQ		_IOW('v',  BASE_VIDIOCPRIVATE+21, int)

#define VIDIO_PICSETBURSTLENGTH		_IOW('v',  BASE_VIDIOCPRIVATE+22, int)
#define VIDIO_PICSETTHRESHOLD		_IOW('v',  BASE_VIDIOCPRIVATE+23, int)
#define VIDIO_PICSETTRIGGERMODE		_IOW('v',  BASE_VIDIOCPRIVATE+24, TriggerModeStruct)

#define VIDIO_PICSETFIELDMODE		_IOW('v',  BASE_VIDIOCPRIVATE+25, FieldModeStruct)

#define VIDIO_PICSETVREFS			_IOW('v',  BASE_VIDIOCPRIVATE+26, VRefStruct)
#define VIDIO_PICSETLATENCYTIMER	_IOW('v',  BASE_VIDIOCPRIVATE+27, unsigned char)

//NORMS
#define PAL 0
#define NTSC 1
#define SECAM 2
#define AUTO 3
#define Progressive_Scan_PAL 4
#define Progressive_Scan_NTSC 5

//SYNC inputs
#define SYNC_R		0
#define SYNC_G		1
#define SYNC_B		2
#define SYNC_EXTERN	3

typedef struct fieldmodestructtag
{
	int Buffer;
	int Mode;
//modes
#define VIDEO_CAPTURE_ODD		0
#define VIDEO_CAPTURE_EVEN		1
#define VIDEO_CAPTURE_ODDEVEN	2
#define VIDEO_CAPTURE_EVENODD	3
#define VIDEO_CAPTURE_SEPERATE	4
}FieldModeStruct;


typedef struct VRefStructtag
{

	unsigned char Vref_R_plus;
	unsigned char Vref_R_min;
	unsigned char Vref_G_plus;
	unsigned char Vref_G_min;
	unsigned char Vref_B_plus;
	unsigned char Vref_B_min;

}VRefStruct;


//UART Defines
#define EXTERNAL_UART_0		0
#define EXTERNAL_UART_1		1
#define ONBOARD_UART		2
#define ONBOARD_UART_RS232	3

#define NO_PARITY	0
#define EVEN_PARITY	1
#define ODD_PARITY	2

#define BR_110		0
#define BR_300		1
#define BR_1200		2
#define BR_2400		3
#define BR_4800		4
#define BR_9600		5
#define BR_19200	6
#define BR_38400	7
#define BR_57600	8
#define BR_115200	9

typedef struct uartstructtag
{
	int Parity;
	int nBitsPerCharacter;//5,6,7,8
	int nStopBits;//1,2
	int Baudrate;//9600

}uartstruct;

#define GP_NO_IRQ					0
#define GP_RISING_IRQ				1
#define GP_FALLING_IRQ				2
#define GP_RISING_AND_FALLING_IRQ	3

#define BURST_LENGTH_1		0
#define BURST_LENGTH_2		1
#define BURST_LENGTH_4		2
#define BURST_LENGTH_8		3	
#define BURST_LENGTH_16		4
#define BURST_LENGTH_32		5
#define BURST_LENGTH_64		6
#define BURST_LENGTH_128	7

#define THRESHOLD_4			0
#define THRESHOLD_8			1
#define THRESHOLD_16		2
#define THRESHOLD_32		3


typedef struct TriggerModetag
{
	int Buffer;
	int Mode;
}TriggerModeStruct;

#define START_TRIGGER_NONE			0
#define START_TRIGGER_GP0_HIGH		1
#define START_TRIGGER_GP1_HIGH		2
#define START_TRIGGER_GP0_LOW		3
#define START_TRIGGER_GP1_LOW		4
#define START_TRIGGER_GP0_RISING	5
#define START_TRIGGER_GP1_RISING	6
#define START_TRIGGER_GP0_FALLING	7
#define START_TRIGGER_GP1_FALLING	8
#define START_TRIGGER_SDI_FALLING	9
#define START_TRIGGER_SDI_RISING	10

#ifdef __RTL__
extern void rt_video_unregister_device(struct video_device *vfd);
extern int rt_video_register_device(struct video_device *vfd, int type);
#endif

#endif //PICASSO_V4L_H
