#include "fg.h"

int fgfd[FRAME_GRABBER_NUM];
int Fieldmode[FRAME_GRABBER_NUM];
int Format[FRAME_GRABBER_NUM];
/* fg_init initializes the ARVOO framegrabber specified by num to default seeting
 *
 * num: number of ARVOO framegrabber.
 */

void fg_init(int num)
{
    char DeviceName[128];
    struct video_channel chan;
    struct video_capture vc;
    int sync;
    
 
    if (num>=FRAME_GRABBER_NUM) 
    {
	fprintf(stderr, "Frame grabber number is wrong!!\n");
	return;
    }

    sprintf(DeviceName,"/dev/video%d",num);
    if((fgfd[num] = open(DeviceName, O_RDWR))<=0)
	fprintf(stderr, "Unable to open %s\n", DeviceName);

    //set input channel nummer and norm (PAL/NTSC etc)
    chan.channel = FG_DEFAULT_PORT;
    chan.norm = FG_DEFAULT_NORM;

    if(ioctl(fgfd[num], VIDIOCSCHAN, &chan) < 0)
    {
		perror("VIDIOCSCHAN");
		return ;
    }
    
    sync = FG_DEFAULT_SYNC; 	
    if(ioctl(fgfd[num], VIDIO_PICSETSYNCINPUT, &sync) < 0)
    {
		perror("VIDIO_PICSETSYNCINPUT");
		return ;
    }

    vc.x = FG_DEFAULT_X_OFFSET;
    vc.y = FG_DEFAULT_Y_OFFSET;
    vc.width = FG_DEFAULT_FULL_FRAME_WIDTH;
    vc.height = FG_DEFAULT_FULL_FRAME_HEIGHT / 2;	//Field height
    if(ioctl(fgfd[num], VIDIOCSCAPTURE, &vc) < 0)
    {
		perror("VIDIOCSCAPTURE\n");
		return ;
    }

    fg_change_field(num);
    Format[num]=VIDEO_PALETTE_RGB24;
    return ;
}

/* fg_change_field changes even buffer to contain even picture, and odd buffer to contain
 * odd picture. More options could be added in the future.
 */
void fg_change_field(int num)
{
   int i;
   FieldModeStruct fm;

   for (i=0;i<2;i++)
   {
   	fm.Buffer = (i*2) + 0; // change buffer 1 and 3 to contain odd pictures
   	fm.Mode = VIDEO_CAPTURE_ODD;
   	if(ioctl(fgfd[num], VIDIO_PICSETFIELDMODE, &fm) < 0)
   	{
   	   perror("VIDIO_PICSETFIELDMODE");
   	   return ;
   	}
   	fm.Buffer = (i*2) + 1; 
   	fm.Mode = VIDEO_CAPTURE_EVEN;
   	if(ioctl(fgfd[num], VIDIO_PICSETFIELDMODE, &fm) < 0)
   	{
   	   perror("VIDIO_PICSETFIELDMODE");
   	   return ;
   	}
   }
   Fieldmode[num] = VIDEO_CAPTURE_SEPERATE;
}

void CopyToOneBuffer(BYTE* RGB,BYTE* RG,BYTE *B,int nPixels)
{
	int i;

    for (i = 0; i < nPixels; i++)
    {
        RGB[i*3 + 0] = B[i];  //Blue
        //RGB[i*3 + 0] = 0;  //Blue
        RGB[i*3 + 1] = RG[i*2 + 1]; //Green
        //RGB[i*3 + 0] = 0;
        RGB[i*3 + 2] = RG[i*2 + 0]; //Red
        //RGB[i*3 + 0] = 0;
    }
}
void SaveToBitmap(char* FileName, BYTE* mem,int width, int height, int Format)
{
	DWORD PageSize; // The size of the pixelfield after adjusting
	DWORD Pitch; // The number of bytes/line. For Bitmaps each scan line must be padded with zeroes to end on a LONG data-type boundary
	int fd, y;//, x,y;
	int bmi_size;
	int Bpp;

	BITMAPFILEHEADER bmfHdr;   // Header for Bitmap file

	BITMAPINFO *bmi;


	switch (Format)
	{
	case VIDEO_PALETTE_RGB32:
		Bpp=4;
		break;
	case VIDEO_PALETTE_RGB24:
		Bpp = 3;
		break;
	case VIDEO_PALETTE_RGB565:
	case VIDEO_PALETTE_RGBa555:
	case VIDEO_PALETTE_RGB55a5:
		Bpp = 2;
		break;
	case VIDEO_PALETTE_GREY:
	case VIDEO_PALETTE_8:
		Bpp = 1;
		break;
	default:
		printf("main.c: Unknown video format, not saved\n");
		return;
	}
	//calculate the bmi size
	bmi_size = sizeof(BITMAPINFOHEADER);

	if ( Bpp == 1 )
	{
		// gray bitmap, 256 gray-values
		bmi_size += sizeof( RGBQUAD ) * 256;
	}
	else if ( Bpp == 2 )
	{
		bmi_size += sizeof( RGBQUAD ) * 3;
	}

	bmi = (BITMAPINFO*) malloc( bmi_size ); 

	
	//Pitch is the width * number of bytes/pixel rounden to a multiple of 4
	//Pitch=(((width * Bpp)+ (Bpp-1))/4)*4; 
	Pitch=(((width * Bpp)+ (3))/4)*4;

	printf("width = %d, Bpp = %d, pitch = %ld\n",width,Bpp,Pitch);

	PageSize = width * height * Bpp ;
	PageSize = Pitch * height;
	
	//Open the file
	fd = open( FileName  ,O_CREAT | O_WRONLY | O_TRUNC,0644);
	if (fd < 0)
	{
		printf ("File open error ,%d !\n",fd);
		return;
	}
	
	//check for valid mempointer
	if (mem == NULL)
	{
		// error!
		printf("mem == NULL\n");
		return ;
	}
	
	// If necessary make color pallette
	if ( Bpp == 1 )
	{
		unsigned		i;
		for ( i = 0; i < 256; i++ )
		{
			// make gray colors
			bmi->bmiColors[i].rgbBlue = i;
			bmi->bmiColors[i].rgbGreen = i;
			bmi->bmiColors[i].rgbRed = i;
			bmi->bmiColors[i].rgbReserved = i;
		}
	}
	else if ( Bpp == 2 )
	{
		// create color mask
		DWORD	bmask=0, gmask=0, rmask = 0;
		switch( Format )
		{
		case VIDEO_PALETTE_RGB565:      // 2 bytes per pixel, R5, G6, B5  
			bmask = 0x001f;
			gmask = 0x07e0;
			rmask = 0xf800;
			break;
		case VIDEO_PALETTE_RGBa555:		// 2 bytes per pixel, Alpha, R5, G5, B5
			bmask = 0x001f;
			gmask = 0x03e0;
			rmask = 0x7c00;
			break;
		case VIDEO_PALETTE_RGB55a5:	   // 2 bytes per pixel, R5, G5, Alpha, B5
									// in Win9x not showable!
			bmask = 0x001f;
			gmask = 0x07c0;	
			rmask = 0xf800;
			break;
		}
		//printf("Format = %d, rmask = %x\n",Format,rmask);

		(*(DWORD *)(bmi->bmiColors)) = rmask;
		(*(DWORD *)(bmi->bmiColors + 1)) = gmask;
		(*(DWORD *)(bmi->bmiColors + 2)) = bmask;
	}


	//fill BitmapFilestructure
	bmfHdr.bfType = 0x4d42; //represents a (int)'BM'
	bmfHdr.bfSize = sizeof(BITMAPFILEHEADER) +
		bmi_size +	PageSize;
	bmfHdr.bfReserved1=0;
	bmfHdr.bfReserved2=0;
	
	bmfHdr.bfOffBits= bmi_size + sizeof(BITMAPFILEHEADER);
	bmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);;
	bmi->bmiHeader.biWidth = width;
	bmi->bmiHeader.biHeight = height;
	bmi->bmiHeader.biPlanes = 1;
	bmi->bmiHeader.biBitCount = Bpp * 8;
	
	if ( Bpp== 2 )
		bmi->bmiHeader.biCompression = BI_BITFIELDS;
	else
		bmi->bmiHeader.biCompression = BI_RGB;

	bmi->bmiHeader.biSizeImage = 0;
	bmi->bmiHeader.biXPelsPerMeter = 0;
	bmi->bmiHeader.biYPelsPerMeter = 0;
	bmi->bmiHeader.biClrUsed = 0;
	bmi->bmiHeader.biClrImportant = 0;
	
		
	write( fd, &bmfHdr, sizeof(BITMAPFILEHEADER));
	write( fd, bmi, bmi_size);

	//write the pixelinfo to the file
	for (y = height-1; y >=0; y--)
	{
		write( fd, (mem + y * width * Bpp), Pitch);

	}//for y

	close( fd );
	
	printf ("Bitmap '%s' saved ! \n",FileName);
	return;
	
}

void SaveToRGB24_PCI_3C_REV2A(int num, int buffnum, char *outputname)
{
	//RGB 24 picture is split in two seperate buffer by the PCI-3C-REV2 framegrabber
	BYTE *origin;
	BYTE *RG, *B, *RGB;

	struct video_mbuf mbuf;


	//Get the buffer props
	if(ioctl(fgfd[num], VIDIOCGMBUF, &mbuf)<0)
    	{
		perror("VIDIOCGMBUF");
		return;
	}
	
	//map the buffers
	origin = (BYTE*)mmap(0, mbuf.size, PROT_READ|PROT_WRITE, MAP_SHARED, fgfd[num], 0);
	RG = origin + mbuf.offsets[buffnum];
	B = origin + mbuf.offsets[buffnum+4];

	//Copy the RG and B buffer to one RGB Buffer
	RGB = malloc(FG_DEFAULT_FULL_FRAME_WIDTH * FG_DEFAULT_FULL_FRAME_HEIGHT *3);
	if (RGB ==NULL) return;

	if ( (Fieldmode[num] == VIDEO_CAPTURE_ODD) || (Fieldmode[num] == VIDEO_CAPTURE_EVEN) || (Fieldmode[num] == VIDEO_CAPTURE_SEPERATE))
	{
		CopyToOneBuffer(RGB,RG,B,FG_DEFAULT_FULL_FRAME_WIDTH*FG_DEFAULT_FULL_FRAME_HEIGHT/2);
		SaveToBitmap(outputname,RGB,FG_DEFAULT_FULL_FRAME_WIDTH,FG_DEFAULT_FULL_FRAME_HEIGHT/2,Format[num]);
	}
	else
	{
		CopyToOneBuffer(RGB,RG,B,FG_DEFAULT_FULL_FRAME_WIDTH*FG_DEFAULT_FULL_FRAME_HEIGHT);
		SaveToBitmap(outputname,RGB,FG_DEFAULT_FULL_FRAME_WIDTH,FG_DEFAULT_FULL_FRAME_HEIGHT,Format[num]);
	}


	if (munmap(origin,mbuf.size) <0)
		perror("munmap");

	free(RGB);

}

/* fg_capture_to_file capture a specifed number of pictures and output them to a 
 * bitmap file. 
 *
 * num: 		number of frame grabber to capture from
 * fileprefix: 		prefix of the output file name
 * file_start_index:	after file name prefix, the is an underscore (_) then followed
 * file_end_index:	by this file index. The pictures will start with file_start_index
 *			and end with file_end_index. Pictures with odd index are odd scanne
 *			lines, and the same goes for even index.
 *
 * example: fileprefix="TEST" file_start_index=50, file_end_index=100, output file names 
 * are TEST_50.bmp to TEST_100.bmp.
 */

void fg_capture_to_file(int num, char *fileprefix, long file_start_index, long file_end_index)
{
   struct video_mmap mm;
   struct video_channel chan;
   struct video_capture vc;
   long i;
   int sync;
   //struct timeval img_time[4];
   char outputfilename[128];

   if(fgfd[num]==0) 
   {
	fprintf(stderr, "Need to initialize first!!!\n");
	return ;
   }	

    chan.channel = FG_DEFAULT_PORT;
    chan.norm = FG_DEFAULT_NORM;

    if(ioctl(fgfd[num], VIDIOCSCHAN, &chan) < 0)
    {
		perror("VIDIOCSCHAN");
		return ;
    }
    
    sync = FG_DEFAULT_SYNC; 	
    if(ioctl(fgfd[num], VIDIO_PICSETSYNCINPUT, &sync) < 0)
    {
		perror("VIDIO_PICSETSYNCINPUT");
		return ;
    }

    vc.x = FG_DEFAULT_X_OFFSET;
    vc.y = FG_DEFAULT_Y_OFFSET;
    vc.width = FG_DEFAULT_FULL_FRAME_WIDTH;
    vc.height = FG_DEFAULT_FULL_FRAME_HEIGHT / 2;	//Field height
    if(ioctl(fgfd[num], VIDIOCSCAPTURE, &vc) < 0)
    {
		perror("VIDIOCSCAPTURE\n");
		return ;
    }


	// Set output properties and start capture
   mm.width	= FG_DEFAULT_FULL_FRAME_WIDTH;
   mm.height	= FG_DEFAULT_FULL_FRAME_HEIGHT;
   mm.format	= FG_DEFAULT_FORMAT;

   for (i=file_start_index; i <=file_end_index+3; i++)  // capture the first 4 files
   {
	mm.frame = i%4;
	if(ioctl(fgfd[num], VIDIOCMCAPTURE, &mm)<0)
	{
	    perror("VIDIOCMCAPTURE");
	    return ;
	}
   }
   for (i=file_start_index+4; i <=file_end_index; i++) // then capture the rest and write them
   {
	mm.frame = i%4;
	if(ioctl(fgfd[num], VIDIOCSYNC, &mm.frame)<0)
        {
	   perror("VIDIOCSYNC");
	   return ;
	}

        sprintf(outputfilename,"%s_%ld.bmp", fileprefix, i-4);
	SaveToRGB24_PCI_3C_REV2A(num, i%4, outputfilename);

	if(ioctl(fgfd[num], VIDIOCMCAPTURE, &mm)<0)
	{
	    perror("VIDIOCMCAPTURE");
	    return ;
	}
	//gettimeofday(&img_time[i], NULL);

   }
   for (i=file_end_index-3; i <=file_end_index; i++)  // write the last 4 files
   {
	mm.frame = i%4;
	if(ioctl(fgfd[num], VIDIOCSYNC, &mm.frame)<0)
        {
	   perror("VIDIOCSYNC");
	   return ;
	}

        sprintf(outputfilename,"%s_%ld.bmp", fileprefix, i);
	SaveToRGB24_PCI_3C_REV2A(num, i%4, outputfilename);

	//gettimeofday(&img_time[i], NULL);

   }

}

void fg_uninit(int num)
{
   close(fgfd[num]);
}
