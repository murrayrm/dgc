#ifdef __cplusplus
extern "C" {
#endif

#ifndef FG_H
#define FG_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <linux/videodev.h>

#include "picasso_v4l.h"


#define FRAME_GRABBER_NUM	2	// support 2 framegrabbers
#define FG_DEFAULT_PATH		"/dev/video"
#define FG_DEFAULT_PORT		0
#define FG_DEFAULT_NORM		NTSC
#define FG_DEFAULT_SYNC		SYNC_G
#define FG_DEFAULT_X_OFFSET	20 
#define FG_DEFAULT_Y_OFFSET	20 
#define FG_DEFAULT_FULL_FRAME_WIDTH	640
#define FG_DEFAULT_FULL_FRAME_HEIGHT	480
#define FG_DEFAULT_FORMAT	VIDEO_PALETTE_RGB24

#define BYTE unsigned char
#define WORD unsigned short
#define DWORD unsigned long

#define BI_RGB        0L
#define BI_BITFIELDS  3L


typedef struct
{ //use __attribute__ ((packed)); for right struct size
	WORD    bfType __attribute__ ((packed));
	DWORD   bfSize __attribute__ ((packed));
	WORD    bfReserved1 __attribute__ ((packed));
	WORD    bfReserved2 __attribute__ ((packed));
	DWORD   bfOffBits __attribute__ ((packed)); 
} BITMAPFILEHEADER;  

typedef struct
{
	DWORD  biSize;
	long   biWidth;
	long   biHeight;
	WORD   biPlanes;
	WORD   biBitCount;
	DWORD  biCompression;
	DWORD  biSizeImage;
	long   biXPelsPerMeter;
	long   biYPelsPerMeter;
	DWORD  biClrUsed;
	DWORD  biClrImportant; 
} BITMAPINFOHEADER; 

typedef struct 
{
	BYTE    rgbBlue;
	BYTE    rgbGreen; 
	BYTE    rgbRed;
	BYTE    rgbReserved; 
} RGBQUAD; 

typedef struct 
{
	BITMAPINFOHEADER bmiHeader; 
	RGBQUAD          bmiColors[1]; 
} BITMAPINFO; 

void fg_init(int num);
void fg_change_field(int num);
void fg_capture_to_file(int num, char *fileprefix, long file_start_index, long file_end_index);
void fg_uninit(int num);

#endif

#ifdef __cplusplus
}
#endif
