/***************************************************************************************/
//FILE: 	steering.cc
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file has the loop that will run in one of the threads
//
/***************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>

#include "functions.h"

using namespace std;

void GrabSteeringLoop(DATUM &d) {

  // steering command structure
  steeringData steering;

  // set the commanded steering value
  steering.actuator = 0.5236; // => pi/6

  while(!d.shutdown_flag){

    // STEERING SCOPE
    if(TRUE){
      // Locking the access
      // boost::recursive_mutex::scoped_lock sl(d.steeringLock);

      // Updating the reference
      // d.steeringDW.update_data(steering);
    }

    // Getting the actual erros and reference
    // d.steeringDW.get_data(steering);
    d.steer = 0.5236;

    sleep(1);
  }
}
