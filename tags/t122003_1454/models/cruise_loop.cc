/***************************************************************************************/
//FILE: 	cruise.cc
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file has the loop that will run in one of the threads
//
/***************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>

#include "functions.h"

using namespace std;

void GrabCruiseLoop(DATUM &d) {

  cruiseData cruise;
 
  // hard code the values for now
  // but what are the units?? - Lars
  cruise.err = 0;
  cruise.derr = 0;
  cruise.refVel = 0;
  cruise.actuator = 0.08;

  while(!d.shutdown_flag){

    // CRUISE SCOPE
    if(TRUE){
      // Locking the access
      // boost::recursive_mutex::scoped_lock sl(d.cruiseLock);

      // Updating the next erros and reference
      // d.cruiseDW.update_data(cruise);
    }

    // Getting the actual erros and reference
    // d.cruiseDW.get_data(cruise);
    d.accel = 0.08;

    sleep(1);
  }
}
