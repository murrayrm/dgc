/***************************************************************************************/
//FILE: 	bob_loop.cc
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file has the loop that will run in one of the threads
//
/***************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>

#include "../tests/waypointnav/waypointnav.hh"
#include "functions.h"

using namespace std;

void GrabBobLoop(DATUM &d) {

  double t;
  bobData bob;

  int cruiseOrder   = 1;
  int steeringOrder = 3;
  int engineOrder   = 1;
  double cruiseVector[1];	// state vector of the cruise control (velocity)
  double steeringVector[3];	// state vector of the planar dynamics (x, y, theta)
  double engineVector[1];	// state vector of the engine (torque)
  double cruiseInput[1], steeringInput[2], engineInput[2];
  double throttleOutput, brakeOutput;

  bob.x = 0;
  bob.y = 0;
  bob.phi = 0;
  bob.theta = 0;
  bob.vel = 0;

  // Open the file for appending
  ofstream out_file("planar.dat", ios::out | ios::binary);

  t = 0;
  while(!d.shutdown_flag){
    // t - x - y - v - theta - phi
    out_file << t << "\t" << bob.x << "\t" << bob.y << "\t" << bob.vel << "\t" << bob.theta << "\t" << bob.phi << endl;

    cout << "Bob thread (t=" << t << ") (ster=" << bob.phi << ") (vel=" << bob.vel << ")" << "(x=" << bob.x << ")" << "(y=" << bob.y << ")" << "(theta=" << bob.theta << ")" << endl;

    // BOB SCOPE
    if (TRUE){

      // Locking the access
      // boost::recursive_mutex::scoped_lock sl_bob(d.bobLock);

      // Getting the actual state
      // d.bobDW.get_data(bob);
      cruiseVector[0] = bob.vel;
      steeringVector[0] = bob.x;
      steeringVector[1] = bob.y;
      steeringVector[2] = bob.theta;

      // Getting the acceleration torque
      throttleOutput = throttleActuator(d.accel);
      
      // Getting the brake torque
      brakeOutput = brakeActuator(d.accel,bob.vel);
      
      // Passing thru the engine lag
      engineInput[0] = ENGINE_LAG;
      engineInput[1] = throttleOutput;
      runge4(engineOrder, t, engineVector, GPS_DELTAT,f_lag,engineInput);

      // Converting the torques into forces
      cruiseInput[0] = tire(engineVector[0],brakeOutput);
            
      // Computing next state, assuming the actuator as a force
      runge4(cruiseOrder, t, cruiseVector, GPS_DELTAT,fv,cruiseInput);
      bob.vel = cruiseVector[0];

      // Getting the actual stearing position
      // steeringInput[0] = d.steeringDW.data.actuator;
      steeringInput[0] = 0;
      steeringInput[1] = bob.vel;

      // Computing next state of the planar movement
      runge4(steeringOrder, t, steeringVector, GPS_DELTAT,fsteering,steeringInput);
      bob.x 	= steeringVector[0];
      bob.y 	= steeringVector[1];
      bob.theta = steeringVector[2];
      bob.phi	= d.steer;

      // Updating the next state -- THIS IS WHERE THE VEHICLE STATE IS ACTUALLY UPDATED
      // d.bobDW.update_data(bob);
    }
    t += GPS_DELTAT;
    //sleep(1);
  }
}
