/***************************************************************************************/
//FILE: 	steering.h
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file has the data that the steering control block generates
//
/***************************************************************************************/

#ifndef __STEERING_H
#define __STEERING_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <fstream>

// Data from the model
// actuator: command to be sent to bob
struct steeringData {
  float actuator;
};

// Class to access the data
class steeringDataWrapper {

  public:
  // Constructors and Destructors
  steeringDataWrapper();
  ~steeringDataWrapper();

  // Prints all the data in the object to the screen
  void print_to_screen();

  // Appends all the data in the object to filename
  void write_to_file(char *filename);

  // Get the values
  void get_data(steeringData &buffer);

  // Updates the values
  void update_data(steeringData buffer);

  // Data
  steeringData data;
};
#endif
