/***************************************************************************************/
//FILE: 	functions.h
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file has prototypes and defines of functions.cpp
//
/***************************************************************************************/

#include "main.hh"
#include "bob.h"
#include "cruise.h"
#include "steering.h"

#define TRUE 1
#define FALSE 0
#define MASS 2000
#define CAR_WIDTH 2
#define DRAG 63
#define GPS_DELTAT 0.11
#define ENGINE_LAG 0.2
#define TIRE_RADIUS 0.4216

#define MAX_ORDER 5 //Maximum of differential equations, just to simplify the programm

// This function gives back the next state after applying the runge-kutta method
//	order	: the number of differential equations
//	x	: derive the state over x
//	y	: state space
//	f	: function that evfaluates f(x,y)
//	input	: input of the system like a force or a steering position,
//		  it's a vector to  allow more than one input
void runge4(int order, double x, double *y, double step, void f(double,double*,double*,double*),double *input);

// dz/dt = fv(z,u)
// z = [v]
// This function evaluates the state space of the cruise control
//	dv/dt = (1/m)*(-b*v + F)
//	v	: velocity
//	b	: drag coeficient
//	F	: Force of brakes or throttle
//	inpuy	: the input is one dymensional
void fv(double x, double *y, double *k, double*input);

// dz/dt = fsteering(z,u)
// z = [x, y, theta]
// This function evaluates the state space of the planar position
//	dx/dt = v*cos(theta + phi)
//	dy/dt = v*sin(theta + phi)
//	dtheta/dt = (v/L)*sin(phi)
//	x	: x position
//	y	: y position
//	theta	: heading
//	phi	: steering angle
//	L	: width of the frontal axle that turns the wheels
void fsteering(double x, double *y, double *k, double*input);

// dz/dt = f_lag(z,u)
// z = [tau]
// This function evaluates the state space of a first order lag
//	dTau/dt = (lag)*(Tau + u)
//	Tau	: engine torque
//	lag	: delay parameter
//	input	: the input is two dymensional, one to the lag parameter and the other to the reference
void f_lag(double x, double *y, double *k, double*input);

// This functions convert from throttle [0 1] into torque
// y = k*u + b
double throttleActuator(double u);

// This functions convert from brake [-1 0] into torque
// y = k*u + b
double brakeActuator(double u, double vel);

// This functions converts the torque into force
double tire(double accTorque, double brakeTorque);

// Functions for the loop in the threads
void GrabBobLoop(DATUM &d);
void GrabCruiseLoop(DATUM &d);
void GrabSteeringLoop(DATUM &d);
