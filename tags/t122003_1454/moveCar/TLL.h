/* TLL.h - header file for TLL.cc (brake force meter, linear pot)
   Revision History: 8/29/2003  Created                      Sue Ann Hong
 */

//#include "parallel.h"
//#include "../team/parallel/parallel.h"
#include "../parallel/parallel.h"        // compile with ../parallel/parallel.c

#ifndef TLL_H
#define TLL_H

// remember the parallel port is PP_BTENS
/* Parallel Port set-up
 Gas:
   Data lines (in): none
   Control lines (out):
  pin14  not used
  pin01  digipot CLK
  pin16  digipot direction (UD)
  pin17  digipot CS [pin only used by this mode]
*/
// constants 
// Brake actuator pot control lines
static int BPOT_RC = PP_PIN01;
static int BPOT_CS = PP_PIN16;
static int BPOT_D8 = PP_PIN10;
static int BPOT_D9 = PP_PIN11;
static int BPOT_D10 = PP_PIN12;
static int BPOT_D11 = PP_PIN13;
// Brake act pot constants
static int BPOT_TBUSY = 8;               // max t_BUSY in us
static double BP_CORR_FACTOR = -0.06862; // correction for error in adc
static double BP_VREF_HIGH = 10;         // V
static double BP_VREF_LOW = 0;           // V
static double BP_NUM_DIV = 2047;        // 12-bit signed ADC => 11-bit positive
static double POT_VHIGH = 9.71;         // 9.73V according to multimeter
                                        // 9.71V (1988,1987) according to s/w
static double BP_NUM_HIGH = 1997;
//static double BP_MIN = .059;
//static double BP_MAX = .810;
static double BP_MIN = .06;
static double BP_MAX = .800;

// TLL-1K Force Sensor control lines
static int BT_OE = PP_PIN14;
static int BT_START = PP_PIN01;
static int BT_ALE = PP_PIN16;// TLL-1K Force Sensor constants
static double BT_CORR_FACTOR = -0.06862; /* correction for noise in amp, adc */
static double BT_VREF_HIGH = 2.5;
static double BT_VREF_LOW = 0;
static double BT_NUM_DIV = 255;                    // 8-bit ADC
static double BT_MAX_OUTPUT = 2.33*12*100/1000;   // 2.33mV/V*Vin*amp*1V/1000mV
static double BT_MAX_FORCE = 1000;

static int tllpport = 2;//PP_GASBR;
//static int tllpport = 0;

/* Reading the tension sensor & linear pot: parallel interface */
int initBrakeSensors();

/* getBrakeForce()
  Return value: (double) force on the brake pedal
*/
double getBrakeForce();

/* getBrakePot
  Return value: (double) position of the brake actuator between 0 and 1
*/
double getBrakePot();

#endif 
