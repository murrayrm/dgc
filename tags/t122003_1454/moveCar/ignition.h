#ifdef __cplusplus
extern "C" {
#endif

#ifndef IGNITION_H
#define IGNITION_H

#include "../parallel/parallel.h"

#define IGPPORT PP_TRANS

/* Pin definition for run and ignition pins on parallel port*/
/* old numbers */
/*#define	RUN_PIN		PP_PIN05
#define IGN_PIN		PP_PIN06
*/
  /* new numbers */
#define AC_CONTROL      PP_PIN01    
#define	RUN_PIN		PP_PIN02    
#define IGN_PIN		PP_PIN03    


/* Action for ignition() */
#define PULL_RUN_HIGH	0
#define PULL_RUN_LOW	1
#define IGNITE_CAR	2

int initIgTrans();
int AC_ON();
int AC_OFF();
int startCar();
  void ignition(int action);

#endif

#ifdef __cplusplus
}
#endif

