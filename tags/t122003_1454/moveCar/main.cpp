#include "moveCar/accel.h"
#include "moveCar/brake.h"

#include "Msg/dgc_msg.h"
#include "Msg/dgc_socket.h"
#include "Msg/dgc_messages.h"
#include "Msg/dgc_time.h"
#include "Msg/dgc_const.h"
#include "Msg/dgc_mta.hh"

#include <time.h>
#include <stdio.h>
#include <iostream>

using namespace std;

int main() {

  init_brake();
  init_accel();
  dgc_MsgHeader myMH;

  float accel;

  dgc_msg_Register(dgcs_Jeremy);

  while( TRUE ) {

    if( dgc_PollMessage(myMH) == TRUE) {
      switch (myMH.MsgType) {
      case dgcm_AccelStatePacket:
	if( dgc_GetMessage(myMH, &accel) == TRUE ) {
	  if ( -1.0 <= accel && accel <= 0.0 ) {
	    set_brake_abs_pos( accel *-1.0, 0.8, 0.8);
	    set_accel_abs_pos( 0.0 );
	  } else if ( 0.0 < accel && accel <= 1.0 ) {
	    set_brake_abs_pos( 0.0, 0.8, 0.8);
	    set_accel_abs_pos( accel );
	  } else {
	    cout << "Bad Accel Data" << endl;
	  } 
	  cout << "Accel = " << accel << endl;
	} 
	else {
	  cout << "Something is wrong" <<endl;
	}
	break;
	
      case dgcm_TransmissionStatePacket:
	cout << "We received a transmission packet but are not doing anything" << endl;
	break;
	
      case dgcm_IgnitionStatePacket:
	cout << "We received an ignition packet but are not doing anything" << endl;
	
	break;
	
      default:
	cout << "Unknown command " << endl;
      }
    }
    
    usleep( 10000 );
  }
  
  dgc_msg_Unregister();
  return 0;

}

