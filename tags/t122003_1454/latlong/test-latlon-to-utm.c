#include "ggis.h"
#include "contest-info.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Convert a LatLon coordinate to UTM, using the routines in ggis.c */

int main (int argc, char **argv)
{
  GisCoordLatLon latlon;
  GisCoordUTM utm;
  char *end;
  if (argc != 3)
    {
      fprintf (stderr, "usage: %s LATITUDE LONGITUDE\n"
	       "Convert Lat/Lon to UTM, using the WGS 84 Datum.\n",
	       argv[0]);
      exit (1);
    }
  latlon.latitude = strtod (argv[1], &end);
  if (end == argv[1])
    {
      fprintf (stderr, "error parsing latitude.\n");
      exit (1);
    }
  latlon.longitude = strtod (argv[2], &end);
  if (end == argv[2])
    {
      fprintf (stderr, "error parsing longitude.\n");
      exit (1);
    }
  if (!gis_coord_latlon_to_utm (&latlon, &utm, GEODETIC_MODEL))
    {
      fprintf (stderr, "Error converting coordinate to utm.\n");
      exit (1);
    }

  printf ("%d %c %.4f %.4f\n", utm.zone, utm.letter, utm.e, utm.n);

  return 0;
}
