/* This code was taken from the GGIS project,
   which was written by Dave Benson.
   Some code lifted from usenet postings; see the .c file.
   See http://ggis.sf.net/.
 */

#ifndef __GGIS_H_
#define __GGIS_H_

typedef enum
{
  GIS_COORD_SYSTEM_UTM,
  GIS_COORD_SYSTEM_LATLON
} GisCoordSystem;

typedef struct {
  double longitude;	/* degrees e/w of greenwich [-180 .. 180] */
  double latitude;	/* degrees n/s of the equator [-90 .. 90] */
} GisCoordLatLon;

typedef struct {
  unsigned short zone;
  char letter;		/* may be 0 to indicate north america */
  double n;		/* meters north */
  double e;		/* meters east */
} GisCoordUTM;

typedef struct _GisGeodeticEllipsoid GisGeodeticEllipsoid;

typedef enum
{
  GIS_GEODETIC_MODEL_AIRY,
  GIS_GEODETIC_MODEL_AUSTRALIAN_NATIONAL,
  GIS_GEODETIC_MODEL_BESSEL_1841,
  GIS_GEODETIC_MODEL_BESSEL_1841_NAMBIA,
  GIS_GEODETIC_MODEL_CLARKE_1866,
  GIS_GEODETIC_MODEL_CLARKE_1880,
  GIS_GEODETIC_MODEL_EVEREST,
  GIS_GEODETIC_MODEL_FISCHER_1960_MERCURY,
  GIS_GEODETIC_MODEL_FISCHER_1968,
  GIS_GEODETIC_MODEL_GRS_1967,
  GIS_GEODETIC_MODEL_GRS_1980,
  GIS_GEODETIC_MODEL_HELMERT_1906,
  GIS_GEODETIC_MODEL_HOUGH,
  GIS_GEODETIC_MODEL_INTERNATIONAL,
  GIS_GEODETIC_MODEL_KRASSOVSKY,
  GIS_GEODETIC_MODEL_MODIFIED_AIRY,
  GIS_GEODETIC_MODEL_MODIFIED_EVEREST,
  GIS_GEODETIC_MODEL_MODIFIED_FISCHER_1960,
  GIS_GEODETIC_MODEL_SOUTH_AMERICAN_1969,
  GIS_GEODETIC_MODEL_WGS_60,
  GIS_GEODETIC_MODEL_WGS_66,
  GIS_GEODETIC_MODEL_WGS_72,
  GIS_GEODETIC_MODEL_WGS_84,
  GIS_GEODETIC_MODEL_DEFAULT = GIS_GEODETIC_MODEL_WGS_84
} GisGeodeticModel;

/* Returns whether the conversion succeeded. */
int      gis_coord_latlon_to_utm(const GisCoordLatLon *input,
			         GisCoordUTM      *output,
			         GisGeodeticModel  model);
int      gis_coord_utm_to_latlon(const GisCoordUTM *input,
			         GisCoordLatLon   *output,
			         GisGeodeticModel  model);

/* --- geodetic model internals --- */
struct _GisGeodeticEllipsoid
{
  GisGeodeticModel model;
  const char *name;
  double equatorial_radius;
  double eccentricity_squared;
};

const GisGeodeticEllipsoid *
gis_geodetic_ellipsoid_lookup (GisGeodeticModel model);

#endif
