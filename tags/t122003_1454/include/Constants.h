#define GPS_SERIAL_PORT 4
#define OBD_SERIAL_PORT 6
#define MAG_SERIAL_PORT 8
#define LADAR_SERIAL_PORT 9
#define STEER_SERIAL_PORT 0
#define BRAKE_SERIAL_PORT 5

// useful for tests
#define DEFAULT_VELOCITY 4.5 // 4.5 m/s = 10.07 mph
