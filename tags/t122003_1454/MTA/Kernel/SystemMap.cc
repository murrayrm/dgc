#include "SystemMap.hh"

#include <iomanip>
#include <fstream>
#include <iostream>
#include "Kernel.hh"

extern Kernel K;
using namespace std;

namespace KERNEL {
  SystemMap::SystemMap(string textFile) {
    M_TCP.IP = IPConvert("131.215.89.150");
    
    M_TableOfComputers["OLD GRANDCHALLENGE"] = IPConvert("131.215.89.150");
    M_TableOfComputers["NEW GRANDCHALLENGE"] = IPConvert("131.215.133.164");
  }
  
  SystemMap::~SystemMap() {
  }
  
  TCP_ADDRESS SystemMap::LocalAddress() {
    return M_TCP;
  }
  
  MailAddress SystemMap::FindAddress(string moduleName) {
    return FindAddress(M_NameToType[moduleName]);
  }
  
  MailAddress SystemMap::FindAddress(int ModuleType) {
    
    
    // find the module in the table 
    list<ModuleInfo>::iterator x    = M_TableOfModules.begin();
    list<ModuleInfo>::iterator stop = M_TableOfModules.end();
    
    while( x!= stop) {
      if( (*x).ModuleType == ModuleType) {
	// found, return address
	return (*x).MA;
      }
      x++;
    }
    
    // else not found, return "zero"
    MailAddress ret;
    ret.TCP.IP   =  0;
    ret.TCP.PORT =  0;
    ret.MAILBOX  =  -1;
    
    return ret;
  }
  
  vector<int> SystemMap::LocalModuleTypes() {
    vector<int> vec;
    vector<shared_ptr <ModuleHandler> >::iterator x    = (K.KD_Modules).begin();
    vector<shared_ptr <ModuleHandler> >::iterator stop = (K.KD_Modules).end();
    while( x != stop) {
      vec.push_back( (*((*(*x)).p_dm)).ModuleType() );
      x++;
    }
    return vec;
  }
  

  void SystemMap::operator()() {

    // shoud be filled in

  }

};
