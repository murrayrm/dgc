#ifndef InMessage_HH
#define InMessage_HH

#include "Misc/Socket/Socket.hh"
#include "Misc/File/File.hh"

/* Classes defined in this file
   InMessageAcceptor: The Socket Server 
   InMessageHandler: Spawned to act upon a message received 
*/
namespace KERNEL {
  class InMessageAcceptor {
  public:
    InMessageAcceptor();
    ~InMessageAcceptor();
    
    void operator()(); // runs the inbox message server

    int ServerPort();
  private:
    
    SSocket M_ss;
    int M_ActiveFlag;
    
  };
  
  
  class InMessageHandler {
  public:
    InMessageHandler();
    ~InMessageHandler();
    
    void operator () (File& in);
    friend class Kernel;
  };
  
};
#endif
