#include "Kernel.hh"


#include <iostream>
using namespace std;


boost::thread_group KD_ThreadGroup;
//extern Kernel *K;

void SignalHandler(int signal) {


} 

Kernel::Kernel(): KD_SystemMap("") {
  KD_ShutdownFlag = false;
  KD_ActiveFlag = false;

  KD_ModulesCounter = 0;
}

Kernel::~Kernel() {
}

void Kernel::Register(shared_ptr<DGC_MODULE> dm) {

  boost::recursive_mutex::scoped_lock sl(KD_ModulesCounterLock);
  int x = (++KD_ModulesCounter);
  
  cout << "Registering New Module (" << (*dm).ModuleName()  
       << ") of type =" << (*dm).ModuleType() << ", given Mailbox = " << x << endl;
  
  KD_Modules.push_back( shared_ptr<ModuleHandler> (new ModuleHandler(dm, x)));

}

void testfunc() {}

void Kernel::Start() {
  // Setup catching of signals

  cout << "Kernel Starting " << endl;

  KD_ThreadGroup.create_thread(KD_InMessageAcceptor);
  KD_ThreadGroup.create_thread(KD_SystemMap);
  KD_ThreadGroup.create_thread(KD_Time);

  // sleep for a second before starting the modules
  sleep(1);
  // now start all modules.
  vector<shared_ptr<ModuleHandler> >::iterator x    = KD_Modules.begin();
  vector<shared_ptr<ModuleHandler> >::iterator stop = KD_Modules.end();
  while( x != stop ) {
    cout << "Starting " << (*((*(*x)).p_dm)).ModuleName() << endl;
    KD_ThreadGroup.create_thread( *((*(*x)).p_dm) );
    x++;
  }

  

  // Wait till we are ready to shut down
  while(!ShuttingDown()) {
    sleep(1);
  }
  // Once we are ready, we must release the server 
  // socket by issuing a local shutdown command.

  //  ...code goes here...
  

  KD_ThreadGroup.join_all();
}


Timeval Kernel::Now() {
  return KD_Time.Now();
}


TCP_ADDRESS Kernel::MyTCPAddress() {
  return KD_SystemMap.LocalAddress();
}


int Kernel::ShuttingDown() {
  return KD_ShutdownFlag;
}

int Kernel::Active() {
  return KD_ActiveFlag;
}

void Kernel::Activate() {
  if( ! ShuttingDown() ) {
    KD_ActiveFlag = true;
  }
}

void Kernel::ForceShutdown() {
  KD_ShutdownFlag = true;
  KD_ActiveFlag = false;
}


void Kernel::SendMail(Mail& ml) {
  KD_OutMail.push_back(ml);

  cout << "Adding mail to send queue" << endl;
}

void DeliverMail(Mail &ml, File f) {
  // if fd<0 open a socket
  if( f.FD() < 0) {
    f = CSocket (ml.To().TCP.IP, ml.To().TCP.PORT);
  }

  // if still < 0 just give up
  if( f.FD() < 0) {
    return;
  }
  
  // send the key to verify the message
  int myKey = DEF::ReceiveKey;
  f.BWrite((void*) &myKey, sizeof(myKey));
  ml.ToFile(f);

}

Mail Kernel::SendQuery(Mail& ml) {
  CSocket mySocket(ml.To().TCP.IP, ml.To().TCP.PORT);

  if(mySocket.FD() < 0) {
    // error opening socket 
    // just return a "bad" message
    return Mail(ml.From(), ml.To(), ml.MsgType(), MailFlags::BadMessage);
  } else {

    // send the key to verify the message
    int myKey = DEF::ReceiveKey;
    mySocket.BWrite((void*) &myKey, sizeof(myKey));
    ml.ToFile(mySocket);
    
    // now read back the response
    
    // read the key to verify the message
    myKey = 0;
    mySocket.BRead((void*) &myKey, sizeof(myKey));
    if( myKey != DEF::ReceiveKey ) {
      // error - bad message
      // just return a "bad" message
      return Mail(ml.From(), ml.To(), ml.MsgType(), MailFlags::BadMessage);
    } else {
      // let the mailer download the message
      return Mail(mySocket);
    }
  }
}
  


Mail Kernel::QueryMailHandler(Mail& ml) {
  cout << "Query Mail Arrived" << endl;
  return Mail(ml.To(), ml.From(), 1, 0);
}

void Kernel::InMailHandler(Mail& ml) {
  cout << "In Mail Arrived" << endl;
}


