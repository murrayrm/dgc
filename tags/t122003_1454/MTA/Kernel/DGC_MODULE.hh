#ifndef DGC_MODULE_HH
#define DGC_MODULE_HH

#include <string>

#include "Misc/Mail/Mail.hh"
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/shared_ptr.hpp>



using namespace std;
using namespace boost;

class DGC_MODULE {

public:
  DGC_MODULE(int ModType, string ModName, int ModFlags);
  ~DGC_MODULE();
  
  // where it all starts...
  void operator ()();

  // States in state machine
  virtual void Init()    ;// = 0;
  virtual void Active()  ;// = 0;
  virtual void Standby() ;// = 0; 
  virtual void Shutdown();// = 0;
  virtual void Restart() ;// = 0;


  // returns "world readable status" of module as a string
  virtual string Status();
  

  // Function called when new mail has arrived
  virtual void InMailHandler(Mail & msg) ;//=0;
  virtual Mail QueryMailHandler(Mail & msg) ;//=0;

  void SetModuleNumber(int m);
  MailAddress MyAddress();

  int ModuleType();
  string ModuleName();


  // Functions to control state machine
protected:
  int  SetNextState( int stateID );
  int  GetCurrentState();
  int  ContinueInState();
private:
  void StateMachineLoop();
  void ExecuteCurrentState();


  // Module Low-Level Control
  int M_MyThreads;
  int M_ModuleNumber;

  // Module Definition
  int    M_ModuleType;
  string M_ModuleName;
  int    M_ModuleFlags;
  
  //  Mailbox M_Mailbox;
    
  
  // State Machine Variables
  int M_CurrentState;
  int M_NextState;
  shared_ptr<recursive_mutex> M_StateMachineLock;
  
};



// state machine indices enumerated
namespace STATES {
  enum { INIT, ACTIVE, STANDBY, SHUTDOWN, RESTART, NUM_STATES };
};

#endif
