#ifndef SystemMap_HH
#define SystemMap_HH

#include <vector>
#include <string>
#include <map>
#include <list>

#include "Misc/Structures/IP.hh"
#include "Misc/Structures/MailAddress.hh"

using namespace std;

struct ModuleInfo {
  MailAddress MA;
  int ModuleType;
};

namespace KERNEL { 

  class SystemMap {
    friend class Kernel;
  public:
    SystemMap(std::string sysMapTextFile);
    ~SystemMap();
    
    TCP_ADDRESS LocalAddress();

    MailAddress FindAddress(std::string moduleName);
    MailAddress FindAddress(int ModuleType); 
    
    vector<int> LocalModuleTypes();
    
    void operator()();
    
  private:
    TCP_ADDRESS M_TCP;  // ip and port of this server 
    map<string, IP_ADDRESS>  M_TableOfComputers;
    list<ModuleInfo>         M_TableOfModules;
    
    map<string, int>         M_NameToType;
    
  };

};
#endif
