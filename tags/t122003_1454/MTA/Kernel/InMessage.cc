#include "InMessage.hh"
#include "Misc/Mail/Mail.hh"
#include "Kernel.hh"

#include <iostream>
using namespace std;

extern Kernel K;

namespace KERNEL { 
  
  InMessageAcceptor::InMessageAcceptor() {
    //  TCP_ADDRESS tcp;// = K->MyTCPAddress();
    
    int port = DEF::MinPort;
    M_ActiveFlag = false;
    while( ! M_ss.Listen(port) ) {
      port ++;
      if (port > (DEF::MinPort+DEF::NumPorts)) {
	cout << "Error Creating Socket Server, Trying Again" << endl;
	port = DEF::MinPort;
      }
    }
  }
  
  InMessageAcceptor::~InMessageAcceptor() {}
  
  void InMessageAcceptor::operator ()() {
    int ActiveMsgs = 0;
    
    int msgType;
    InMessageHandler imh;
    M_ActiveFlag = true;
    
    while(K.ShuttingDown() == false) {
      File in = M_ss.Accept();
      
      imh(in);
      // in will close automatically
    }
    
    // shutting down now
  //  M_ss.Close();
    M_ActiveFlag = false;
  }
  
  int InMessageAcceptor::ServerPort() {
    return M_ss.Port();
  }

  
  InMessageHandler::InMessageHandler()  {}
  InMessageHandler::~InMessageHandler() {}
  
  void InMessageHandler::operator() (File& in) {
    
    // read in the key to verify the message
    int myKey = 0, flags;
    in.BRead((void*) &myKey, sizeof(myKey));
    if( myKey != DEF::ReceiveKey ) return;
    
    // if that was successful, then the rest of 
    // the data is the message
    Mail newMessage(in);
    
    if(  newMessage.OK() ) {
      // now we need to determine what type of message
      // and who it goes to
      
      // first, find the recipient
      int mbox = newMessage.To().MAILBOX;
      flags = newMessage.Flags();

      if ( mbox == 0 ) { // for the kernel
	if(flags & MailFlags::QueryMessage) {
	  // it's a query message
	  Mail returnMessage = K.QueryMailHandler(newMessage);
	  // so send it on its way.
	  K.SendMail(returnMessage);
	} else {
	  // just deliver
	  K.InMailHandler(newMessage);
	}
	
      } else { // find the module the message is for
	vector<shared_ptr <ModuleHandler> >::iterator x    = (K.KD_Modules).begin();
	vector<shared_ptr <ModuleHandler> >::iterator stop = (K.KD_Modules).end();
	while( x != stop) {
	  if( (*(*x)).ModuleNumber() == mbox ) {
	    // found the right module
	    // now check if inmessage or query message

	    if(flags & MailFlags::QueryMessage) {
	      // it's a query message
	      Mail returnMessage = (*(*x)).QueryMail(newMessage);
	      // so send it on its way.
	      K.SendMail(returnMessage);
	    } else {
	      // just deliver
	      (*(*x)).InMail(newMessage);
	    }
	  }
	  x++;
	}
      }
    }
  }
  
  
  
};
