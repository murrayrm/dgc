#ifndef Modules_HH
#define Modules_HH

namespace MODULES {
  enum {
    SystemMaster,
    FrameGrabber,
    VisionProcessor,
    StateEstimator,
    LocalPlanner,
    GlobalPlanner,
    LADARPlanner,

    Arbiter,
    ModeSwitch,
    DriveActuator,

    TIControl,

    Joystick,
    CameraControl,
    EStop,
    
    Actuators,

    TestModule,

    NumberOfModules
  };
};

#endif
