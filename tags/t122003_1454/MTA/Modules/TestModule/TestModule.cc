#include "TestModule.hh"

#include "Kernel.hh"
#include <time.h>
#include <iostream>

using namespace std;

extern Kernel K;
TestModuleDatum D;
//extern TestModule testmodule;


void TestModule::Init() {

  // do your init in here
  // i.e. open serial ports etc,

  cout << ModuleName() << ": Init" << endl;

  D.MyCounter = 0;

  // go to active state after initialization
  SetNextState(STATES::ACTIVE);
}



void TestModule::Active() {
  while( ContinueInState() ) {  // while stay active
    sleep(1); // increment the counter every second

    D.MyCounter ++;
    cout << ModuleName() << ": Active - Incrementing Counter = " << D.MyCounter << endl;
  }
} 


void TestModule::Standby() {
  while( ContinueInState() ) {  // while stay active
    sleep(1); // dont increment the counter while in standby

    //    D.MyCounter ++;
    cout << ModuleName() << ": Standby - _Not_ Incrementing Counter = " << D.MyCounter << endl;
  }
} 


void TestModule::Shutdown() {
  // do whatever closing of files, etc
  cout << ModuleName() << ": Shutdown" << endl;

}

void TestModule::Restart() {
  // you may want to do something better here, but i'll just call shutdown and init
  cout << ModuleName() <<": Restart" << endl;
  Shutdown();
  Init();
}

string TestModule::Status() {
  string x = ModuleName() + ": is in state ";
  switch ( GetCurrentState() ) {
  case STATES::INIT:
    x+= "Init";       break;
  case STATES::ACTIVE:
    x+= "Active";     break;
  case STATES::SHUTDOWN:
    x+= "Shutdown";   break;
  case STATES::STANDBY:
    x+= "Standby";    break;
  case STATES::RESTART:
    x+= "Restart";    break;
  default:
    x+= "Unknown state - (Something is very wrong)";
  }
  return x;
}
 
void TestModule::InMailHandler(Mail& ml) {

  switch(ml.MsgType()) {  // for mail functions check out Misc/Mail/Mail.hh

  case TestModuleMessages::REINIT:
    SetNextState(STATES::RESTART);
    break;

  case TestModuleMessages::SHUTDOWN:
    // force the Kernel to shutdown
    K.ForceShutdown();
    break;

  case TestModuleMessages::PAUSE:
    SetNextState(STATES::STANDBY);
    break;

  case TestModuleMessages::GO:
    SetNextState(STATES::ACTIVE);
    break;

  case TestModuleMessages::SETCOUNTER:
    // check to see that an int was attached to the message
    if( ml.Size() >= sizeof(int)) {
      int x;
      ml >> x;
      D.MyCounter = x;
    }
    break;

  default:
    cout << ModuleName() << ": Unknown InMessage" << endl;
  }
}


Mail TestModule::QueryMailHandler(Mail& msg) {
  // figure out what we are responding to

  Mail reply = msg; // A hack !!! dont do this :)
  int x;

  switch(msg.MsgType()) {  

  case TestModuleMessages::GETCOUNTER:
    reply = ReplyTo(msg);
    reply << D.MyCounter;
    break;

  case TestModuleMessages::GETSTATE:
    reply = ReplyTo(msg);
    x = GetCurrentState();
    reply << x;
    break;

  default:
    // send back a "bad Message"
    reply = Mail( msg.From(), msg.To(), 0, MailFlags::BadMessage);
  }

  return reply;
}



/*
TestModule::TestModule(): 
  DGC_MODULE(MODULES::TestModule, "Test Module", 
	     0, // Flags, none used at the moment
	     StartTestModule ) {

  // do not do anything in the constructor
  // do all initialization in INIT Function

}

TestModule::~TestModule() {
  
  // whatever, try to do shudown stuff in SHUTDOWN Function
}
*/
