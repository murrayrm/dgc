#ifndef OBDII_H
#define OBDII_H

//#include "Msg/dgc_time.h"
#include "../Msg/dgc_time.h"
#include "../include/Constants.h"

// RMM, 19 Dec 03: added definition of default serial port
// Consider changing this to a global variable with this as default (??)

//defined in Constants.h
#define OBDSerialPort OBD_SERIAL_PORT // serial port # for OBD

void OBD_Init(int port);
float Get_RPM();
float Get_Speed();
float Get_Engine_Coolant_Temp();
float Get_Manifold_Pressure();
float Get_Ignition_Timing();
float Get_Intake_Air_Temp();
float Get_Air_Flow_Rate();
float Get_Throttle_Pos();

#endif  //OBDII_H
