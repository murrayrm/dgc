#include "serial.h"

int main()
{
	char a[4][30]={"^^abcdefg^^", "**johnson's test**", "@@Today is Friday@@", "$$Linux serial Darpa$$"} ;
	char b[4][30]={"^^another test^^", "**Ruddock**", "@@Tomorrow is Saturday@", "$$written by johnson$$"} ;
	char buffer[3000];
	int i, counta=0, countb=0, len; 
	long int j, k, l;

	/*for(i=0; i<4; i++)
	   printf("strlen(a[%d])=%d \n", i, strlen(a[i]));*/
	printf("\n");
	if(serial_open(0, B9600) < 0)
		fprintf(stderr, "Unable to open com port %d\n", 0);
        // B4000000 is not defined in termios.h for all operating systems, so use 38400
	if(serial_open_advanced(1, B38400, SR_PARITY_EVEN | SR_SOFT_FLOW_INPUT | SR_SOFT_FLOW_OUTPUT | SR_HARD_FLOW_ENABLE | SR_TWO_STOP_BIT) < 0)
		fprintf(stderr, "Unable to open com port %d\n", 0);

	printf("ttyS0 blocking write, ttyS1 blocking write\n");
	for(i=0; i<300; i++)
	{
	   counta=counta+serial_write(0, a[i%4], strlen(a[i%4])+1, SerialBlock);
	   //for(i=0; i<300; i++)
	   countb=countb+serial_write(1, b[i%4], strlen(b[i%4])+1, SerialBlock);
	}
	printf("Tranmist /dev/ttyS0:%d bytes,  /dev/ttyS1: %d bytes\n", counta, countb);

	counta=countb=0;
	printf("ttyS0 blocking write, ttyS1 blocking write\n");
	for(i=0; i<400; i++)
	{   counta=counta+serial_write(0, b[i%4], strlen(a[i%4])+1, SerialBlock);
		//for(i=0; i<400; i++)
	   countb=countb+serial_write(1, a[i%4], strlen(b[i%4])+1, SerialBlock);
	}
	printf("Tranmist /dev/ttyS0:%d bytes,  /dev/ttyS1: %d bytes\n", counta, countb);

	for(i=0; i<10; i++)
	{
		if((len=serial_read(0, buffer, 3000))>0)
		{
			printf("\n!!GOT %d BYTES from /dev/ttyS0!!\n", len);
			for(j=0; j<len; j++)
				printf("%c", buffer[j]);
			printf("\n");
		}
		else	printf("GOT NOTHING from /dev/ttyS0\n");

		for(j=0; j<30000; j++)	// delay to do something else
		   for(k=0; k<30000; k++)
			l=0;

		if((len=serial_read(1, buffer, 3000))>0)
		{
			printf("\n!!GOT %d BYTES from /dev/ttyS1!!\n", len);
			for(j=0; j<len; j++)
				printf("%c", buffer[j]);
			printf("\n");
		}
		else	printf("GOT NOTHING from /dev/ttyS1\n");

	}
	serial_close(0);
	serial_close(1);
	return 0;
}


