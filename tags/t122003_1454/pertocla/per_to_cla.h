#ifndef __PER_TO_CLA_H__
#define __PER_TO_CLA_H__

#include <cstdio>
#include <string>
#include <iostream>
#include <fstream>

//#include "../../perceptor/MapGen/map.h"
// DIRTY HACK SO EVERYONE DOESN'T HAVE TO CHECKOUT PERCEPTOR
// FIX ME OR YOU WILL PAY
#include "map.h"
#include "../moveCar/accel.h"

extern "C" 
{
  // ANOTHER DIRTY HACK AS ABOVE.  FIX ME TOO OR ILL GET YOUR KITTY CAT
#include "mapinterface.h"
  //#include "../../perceptor/Utils/mapinterface.h"
}

//Size of everything except the map itself in bytes
#define MAP_HEADER_SIZE sizeof(char)+3*sizeof(int)+sizeof(unsigned long)+3*sizeof(float)+10*sizeof(double)
#define MAX_ROWS MAP_ROWS
#define MAX_COLS MAP_COLS
#define MAX_TOTAL_MAP_SIZE MAP_HEADER_SIZE+MAX_ROWS*(MAX_COLS+2)*4

#define MIN_GOODNESS 0
#define MAX_GOODNESS 255

#define DEFAULT_UNKNOWN_GOODNESS 0
#define DEFAULT_UNKNOWN_CERTAINTY 0

class serialMap {
	public:
		serialMap(char map_type_arg, unsigned long map_size_arg, int map_time_arg, int num_rows_arg, int num_cols_arg, float map_x_res_arg, float map_y_res_arg, float map_z_res_arg, double rot_x_arg, double rot_y_arg, double rot_z_arg, double rot_s_arg, double tranRel_x_arg, double tranRel_y_arg, double tranRel_z_arg, double origin_x_arg, double origin_y_arg, double origin_z_arg, int* map_map_arg);
		serialMap();
		~serialMap();
		void print_map();

		int updateMap(char map_type_arg, unsigned long map_size_arg, int map_time_arg, int num_rows_arg, int num_cols_arg, float map_x_res_arg, float map_y_res_arg, float map_z_res_arg, double rot_x_arg, double rot_y_arg, double rot_z_arg, double rot_s_arg, double tranRel_x_arg, double tranRel_y_arg, double tranRel_z_arg, double origin_x_arg, double origin_y_arg, double origin_z_arg, int* map_map_arg);

		//Perceptor Side Routines
		//Writes a serialMap to filename
		int writeMap(char* filename);
		//Sends a serialMap to Claraty
		int sendMap();
		//Writes a serialized map to a certain spot in memory
		int writeMemoryMap(void *outptr);

		//Claraty Side Routines
		//Reads a serielMap from a filename
		int readMap(const char* filename);
		//Receives a serialMap from Claraty
		int getMap();

		//If you add any variables here, make sure to adjust the MAP_HEADER_SIZE, and change various other encoding and decoding functions
		char map_type; //s=short range, l=long range, r=rear view
		unsigned long map_size; //in total number of cells; to get map_size in bytes, use map_size*sizeof(int)
		int map_time;
		int num_rows; //in cells
		int num_cols; //in cells
		float map_x_res; //in meters
		float map_y_res; //in meters
		float map_z_res; //in meters
		//Vehicle orientation vector (can be used to get roll, pitch, yaw)
		double rot_x;
		double rot_y;
		double rot_z;
		double rot_s;
		//Vehicle position in UTM coordinates
		double tranRel_x;
		double tranRel_y;
		double tranRel_z;
		//Map origin (lower-left) in UTM coordinates
		double origin_x;
		double origin_y;
		double origin_z;

		int* map_map; //The terrain map.  Format described in documentation terrain_map_specs.txt

		// private:
		//Serializes a serial map into a serial stream
		void* serializeMap();

		//Deserializes a serial map
		void deserializeMap(void* serializedMap);
};

//Turns a serialMap into a claratyMap
void makeClaratyMap(serialMap perMap, int num/*, claratyMap &claMap*/);

//Helper routines for making the Claraty map
int in_range_ptc(int val, int min_val, int max_val);
int int_scale(int val, int old_min_val, int old_max_val, int new_min_val, int new_max_val);
int round(float num);

//Function for writing a set of r,g,b values to a file
int write_bmp(const char* filename, int rows, int cols, unsigned char* r, unsigned char* g, unsigned char* b);

#endif
