#ifndef func_HH
#define func_HH

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <time.h>
#include "../../Msg/dgc_time.h"

// Driving
#include "../../moveCar/brake.h"
#include "../../moveCar/TLL.h"
#include "../../moveCar/accel.h"
#include "../../steerCar/steerCar.h"

#include <string>
using namespace std;

void SteerSet(float steer);
void AccelSet(float accel);
void BrakeSet(float brake);


#endif
