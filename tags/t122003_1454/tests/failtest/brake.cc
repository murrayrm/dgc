#include "func.hh"



int main() {

 init_brake();
 initBrakeSensors();

 int i;
 float x;

 for (i=0; i< 10; i++) {
   x = -(i%2) * 0.7;
   cout << "-brake.cc-Setting Brake To: " << x << endl;
   BrakeSet( x );
   sleep_for(5);
 }

 return 0;
}
