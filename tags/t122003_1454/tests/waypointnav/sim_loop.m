clear all;
rKutta = load('planar.dat');

steer = rKutta(:,1);
accel = rKutta(:,2);
time = rKutta(:,3);
vel = rKutta(:,4);
lat = rKutta(:,5);
lng = rKutta(:,6);
theta = rKutta(:,7);

figure(1);
subplot(3,2,1), plot(time,accel);
ylabel('accel');

subplot(3,2,2), plot(time,vel);
ylabel('vel');

subplot(3,2,3), plot(time,lat);
ylabel('lat');

subplot(3,2,4), plot(time,lng);
ylabel('lng');

subplot(3,2,5), plot(time,steer);
ylabel('steer');

subplot(3,2,6), plot(lat,lng);
ylabel('planar');
