#include "waypointnav.hh"
#include "../../display/DisplayFcns.h"

// return a pointer to the gps window
void DisplayAll( DATUM &d ) {

  // wait for all of the initialization prints to print
  sleep(1);

  init_display();
  
  // get the generic window
  WINDOW * genw;
  genw = get_gen_window();

  wmove(genw, 1, 1);

  // get the same file as set by sim_loop
  ifstream mycin_sim("dispfile_sim");
  char linestr_sim[1000];

  // keep printing everything output to mycout
  while(d.shutdown_flag == FALSE) {

    // write my pipe to linestr
    mycin_sim >> linestr_sim;
    // and print the string to my curses window
    wprintw( genw, linestr_sim );
    wrefresh( genw );
    usleep(10);

  }
  // make sure we close the pipes
  mycin_sim.close();

} // end DisplayAll()

