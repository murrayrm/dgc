#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
// DATUM & cruise function protocols??
#include "waypointnav.hh"
// Driving
#include "../../moveCar/brake.h"
#include "../../moveCar/accel.h"
#include "../../steerCar/steerCar.h"
// Added the following for waypoint following
#include "../../latlong/LatLong.h"
#include "../../Arbiter/Arbiter.h"

const float brakeVelo = 0.7;

extern int SIM;
extern int PRINT_VOTES;

int InitAccel(DATUM &d) {
  d.accelValid = FALSE;
  d.drivingValid = TRUE;

  if (!SIM) {
    cout << "Initializing brake"<< endl;
    init_brake();
    cout << "Initializing gas" << endl;
    init_accel();
  }

  cruise_init();
  return TRUE; // success

} // end InitAccel()


// This function takes in the state data of the vehicle and commands
// actuators. In case of waypoint following (added 11/30/2003) the function
// will commannd both steering and throttle/brake actuators. 
void AccelLoop(DATUM &d) {

  timeval now;
  float accel, curSpeed; 
  float steer = 100;
  ofstream accelFile( (string("testRuns/test") 
		       + d.TestNumberStr 
		       + ".dat").c_str());
  ofstream gpsLogFile( "../../Global/Gone_waypoints.gps" );
  bool runArbiter = true;

  accelFile << setiosflags(ios::showpoint) << setprecision(10);
  accelFile << "% Target Velocity (m/s) = " << d.targetVelocity << endl;
  accelFile << "% Time(s)\taccel ([-1,1])\tsteer ([-1,1])\tSpeed(m/s)" 
	    << "\tNorthVel\tEastVel\tNorthing\tEasting" 
	    << "\tOBD Speed\tOBD RPM" << endl;
  gpsLogFile << setiosflags(ios::showpoint) << setprecision(10);

  cout << setiosflags(ios::showpoint) << setprecision(4);
  while(d.shutdown_flag == FALSE) {
    if( (d.stateValidFromIMU) && (d.stateValidFromGPS) ) {
      if( (d.stateNewDataFromIMU) && (d.stateNewDataFromGPS) ) {
	if (runArbiter) {
	//if(0){
	  // lock evaluators --???????????????????????????? unlock?
	  boost::recursive_mutex::scoped_lock slGLOBAL(d.globalLock);
	  boost::recursive_mutex::scoped_lock slDFE(d.DFELock);
	  
	  // Run arbiter with the current Voter values: do what runArb() does
	  (d.demoArb).getVotes();
	  (d.demoArb).combineVotes();
	  (d.demoArb).pickBestArc();
          if(PRINT_VOTES){
            (d.demoArb).printCombinedVotes();
            cout << "--accel.cc: arbiter-- Sleeping so you can read my output." << endl;  
            sleep(2);
          }
          // SLEEP FOR A WHILE HERE SO THAT WE CAN SEE THE COUTS

	  // Finally update command values of this method
	  steer = ( (d.demoArb).getBestPhi() ) / MAX_STEER;
	  d.targetVelocity = (d.demoArb).getBestVelo();
          
	}
	else {           // No Arbiter
	  steer = 0.5;   
          d.targetVelocity = 5;    // m/s just some default value
	  // -------------------------------------
	  // OPEN LOOP TEST
	  //accel = 0.25;	  
	  // -------------------------------------
	}
	// Run cruise controller
	accel = cruise(d.targetVelocity, d.SS.speed, d.accel);          
        //cout << "STEER = " << steer << " ACCEL = " << accel << endl;
        
	// Now output the result assuming we are still allowed 
	// to change the accel (i.e., we are not shutting down)
	boost::recursive_mutex::scoped_lock slDRIVING(d.drivingLock);
	if(d.drivingValid) {
	  gettimeofday(&now, NULL);
	  /*
	  cout << "Time=" << timeval_subtract(now, d.TimeZero) << ", ";
	  cout << "GPS Speed=" << d.SS.speed << ", ";
	  cout << "OBD Speed=" << d.obdSpeed << ", ";
	  cout << "RPM=" << d.obdRPM << ", ";
	  cout << "Accel=" << accel << endl;
          cout << "Steer=" << steer << endl;
	  */

	  if (!SIM) {
	    cout << "Steering Command = " << steer << endl;	    
	    // Send command to steering actuator
	    if( -1.0 <= steer && steer <= 1.0 ) {
	      //cout << "Setting steering to " << steer << endl;
	      steer_heading(steer);
	    } else {
	      cout << "Bad Steering Command = " << steer << endl;
	    }
	    
	    // Send commands to throttle/brake actuators
	    d.accel = accel;
	    cout << "Gas Command = " << d.accel << endl;
	    if(-1.0 <= accel && accel <= 0.0) {
	      // cout << "Setting brake to " << accel << endl; 
	      set_accel_abs_pos(0.0);
	      // set_brake_abs_pos_pot( accel, brakeVelo, brakeVelo );
	      
	    } else if(0.0 <= accel && accel <= 1.0) {
	      // cout << "Setting throttle to " << accel << endl;
	      set_accel_abs_pos(0);
	      set_accel_abs_pos(accel);
	      // set_brake_abs_pos_pot( 0.0, brakeVelo, brakeVelo );
	    } else {
	      cout << "Bad Accel Command = " << accel << endl;
	    }
	  } // end if(!SIM)
	  else {   // Simulation

            // d.steer and d.accel will be used by the simulator thread
            d.steer = steer;
            d.accel = accel;
	    //cout << "Steering (" << steer << ") Accel(" << d.accel << ")" << endl;
	  }
	  
	} // end if(runArbiter)

	// Log state data
        // This is the main logfile
	gettimeofday(&now, NULL);
	accelFile << timeval_subtract(now, d.TimeZero) << "\t"
		  << d.accel << "\t"
	          << d.steer << "\t"
		  << d.SS.speed << "\t"
		  << d.SS.vel_n << "\t"
		  << d.SS.vel_e << "\t"
		  << d.SS.northing << "\t"
		  << d.SS.easting << "\t"
		  << d.obdSpeed << "\t"
		  << d.obdRPM << "\t"
		  << endl;
        // we did this to get a waypoint file
	gpsLogFile << d.SS.northing << "\t"
	           << d.SS.easting << "\t"
	           << endl;

      } // end if(d.stateNewData)

      // Sleep for a bit and do it again -- controls actuator rate
      usleep_for(100000);

    } else {      // state not valid (else for if(d.stateValid))
      // PROBABLY WANT THIS!
      //cout << "accel.cc -- state not valid. sleeping." << endl;
      usleep_for(100000);    // wait to do anything until we have data
    }

  }  // end while(d.shutdown_flag == FALSE)

  if(!SIM) {
    // Main program done, set throttle to none and stop the car
    set_accel_abs_pos(0);
    //set_brake_bas_pos_pot(1.0, brakeVelo, brakeVelo);
  }

} // end AccelLoop()

//
// end accel.cc
