#include "global.hh"
#include "waypointnav.hh"

#include "../../Arbiter/Voter.h"
#include "../../Arbiter/Arbiter.h"

extern int SIM; // get the simulation flag from the main function
extern int PRINT_VOTES; // get the print_votes flag from the main function

// Return value: steering angle in -1(left) to 1(right)
double f_Steering(Vector curPos, Vector curWay, Vector curHead) {
  // Find the angle between curWay and curHead, set it as the new steering
  double steer = -get_deflection(curPos, curWay, curHead);

  // we're squaring the magnitude of the steering here for some reason
  // what is the explanation???  
  //steer = steer * fabs(steer);

  // Saturate the steering command to be in [-1, 1]
  if (fabs(steer) > MAX_STEER) {
    if (steer > 0)
      return 1.0;
    else
      return -1.0;
  }
  else {
    return steer/MAX_STEER;
  }
} // end f_Steering


////////////////////////////////////////////////////
// Return value: desired velocity at this point
// Needs a body that calculates a good velocity given state & possibly steering
double f_Velo(double curVelo, Vector curPos, Vector curWay)
{  
  double des_velo;

  // Check to see what the max speed possible is
  des_velo = 1.5;  // m/s

  return des_velo;
}
///////////////////////////////////////////////////

void GlobalLoop(DATUM &d)
{
  pair<double, double> result;
  Vector currentWaypoint;
  Vector pos, head;
  timeval tv;
  double posE, posN, curSpeed;

  while(d.shutdown_flag == FALSE) {

    //if(!SIM){
    //system("clear");
    //}

    if( d.WF.waypoints.size() > 0) {

      if (TRUE) {
	// lock state
	boost::recursive_mutex::scoped_lock slSTATE(d.stateLock);
	currentWaypoint = d.WF.waypoints.front();

	// Update state info: position & heading
        // need to assign pos[.E/.N], curSpeed, head[.E/.N]
        // get the state data from the state filter (the state filter
        // assigns these to the DATUM d)
        pos.N = d.SS.northing;
        pos.E = d.SS.easting;
        curSpeed = d.SS.speed;
        head.N = d.SS.heading_n;
        head.E = d.SS.heading_e;

        // print out some position information
  	if(!SIM){
          printf(" Waypoint Position: N = %10.3f, E = %9.3f\n", currentWaypoint.N, currentWaypoint.E);
	  printf(" Current Position: N = %10.3f, E = %9.3f\n", pos.N, pos.E);
	  printf(" Current Heading: N = %10.3f, E = %9.3f\n", head.N, head.E);
	}
      } // state lock should end here

      if( withinWaypoint(pos, currentWaypoint, waypointRadius) ) {
	cout << "!!!!!!!!!!!!!!!!!!!!!!Waypoint Acheieved!!!!!!!!!!!!!!!!!!!" << endl;
	d.WF.waypoints.pop_front();
      } 
      else {   // Gotta get to the next waypoint
	//cout << endl; // stupid spacing printout
	result.second = f_Steering(pos, currentWaypoint, head);  // Steering
	result.first = f_Velo(curSpeed, pos, currentWaypoint);   // Velocity

        // Log driving decisions
	gettimeofday(&tv, NULL);
	d.WF.driveFile << timeval_subtract(tv, d.WF.timeZero) << "\t"
		       << pos.E << "\t"
		       << pos.N << "\t"
		       << head.E  << "\t"
		       << head.N  << "\t"
		       << curSpeed   << "\t"
		       << result.second << "\t"
		       << result.first  << endl;
	usleep_for(100000);      // Evaluation rate
      }
    } 
    else {
      // Done with this set of waypoint following
      cout << "Waypoint following done" << endl;
      d.shutdown_flag = TRUE;
      // Reset the brake so we are guaranteed to stop the vehicle
      result.first = 0;               // Velocity
      result.second = 0;              // Steering
    } // end of if(waypoints.size > 0) else ...


    // WHY ARE WE DEALING WITH DFE IN GLOBAL.CC?  --LARS
    // lock DFE (??????)
    boost::recursive_mutex::scoped_lock slDFE(d.DFELock);
    int goodness = 0;
    double timestamp = 0;        // Dummy for now
    Voter::voteListType wayPt = d.voterArray[d.globalNum].getVotes();

    // make sure your couts are explicit!
    // i thought this came from the arbiter!
    //cout << "--global.cc-- best steer command: " << result.second << endl;
    //cout << "--global.cc-- best velo  command: " << result.first << endl;

    // Now wayPt should contain all the right arcs to be evaluated.
    // So evaluate them.
    for (Voter::voteListType::iterator iter = wayPt.begin(); 
	 iter != wayPt.end(); ++iter) {

      // Calculate goodness by looking at how close to the desired steering
      // the angle is:       
      goodness = max(0, (int)( round(MAX_GOODNESS 
	  - fabs( (iter->phi)/MAX_STEER - result.second) * MAX_GOODNESS)));
      iter->goodness = goodness;

      // iter->velo = veloLookupTable(iter->phi);   // doesn't exist yet
    // cout << "global.cc -- ideal heading velocity: " << result.first << endl;
      float diff = fabs( (iter->phi)/MAX_STEER - 0 ); // [0,1]
      //      iter->velo = (MAX_VELO - MIN_VELO) * ( 1 - diff) + MIN_VELO;
      iter->velo = (MAX_VELO - MIN_VELO) * pow(1 - diff, 2) + MIN_VELO;
      //    cout << "global.cc -- velocity: " << iter->velo << endl; 
    }

    // Update this evaluator's Voter's voteList.
    d.voterArray[d.globalNum].updateVotes(wayPt, timestamp);
    if( PRINT_VOTES ) {
      //print the arbiter votes 
      cout << "--GlobalLoop-- GLOBAL votes" << endl;
      d.voterArray[d.globalNum].printVotes();   /////////////////////////
      cout << "--global.cc-- Sleeping so you can read my output." << endl;
      sleep(2);
    }
  }  // end of while(shutdown == false)

  cout << "--GlobalLoop: done--" << endl;
}


// Reads in waypoints from the specified file (hardcoded now)
// Waypoint file format is:  Northing  Easting
void InitGlobal(DATUM &d){

  // Add a global Voter in d.demoArb
  d.voterArray[d.globalNum] = Voter(NUMARCS, LEFTPHI, RIGHTPHI);
  //  Voter blah(NUMARCS, LEFTPHI, RIGHTPHI);
  d.globalWeight = GW;
  d.demoArb.addVoter(&(d.voterArray[d.globalNum]), d.globalWeight);
  cout << "Added Global Voter" << endl;

  Vector currentWaypt;
  // Read in all waypoints
  ifstream waypointFile("../../Global/waypoints.gps");
  d.WF.waypoints.clear();
  while(!waypointFile.eof()) {
    waypointFile >> currentWaypt.N >> currentWaypt.E;
    cout << "Waypoint North = " << currentWaypt.N << ", East = " << currentWaypt.E << endl;
    d.WF.waypoints.push_back(currentWaypt);
  }

  // close waypoints file
  waypointFile.close();
}
