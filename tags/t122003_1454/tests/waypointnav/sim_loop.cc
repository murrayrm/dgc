/***************************************************************************************/
//FILE: 	sim.cc
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file has the loop that will run in one of the threads
//
/***************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>

#include "waypointnav.hh"
#include "models/model_functions.h"
#include "display/DisplayFcns.h"
#include "latlong/ggis.h"
#include "latlong/contest-info.h"

using namespace std;

extern int DISPLAY;
void SimLoop(DATUM &d) {

  double t;
  double northing,easting,vel_n,vel_e,vel_u,vel,theta;
  double accel,steer;

  /* from ggis.h */
  //typedef struct {
  //  double longitude;     /* degrees e/w of greenwich [-180 .. 180] */
  //  double latitude;      /* degrees n/s of the equator [-90 .. 90] */
  //} GisCoordLatLon;
  //
  //typedef struct {
  //  unsigned short zone;
  //  char letter;          /* may be 0 to indicate north america */
  //  double n;             /* meters north */
  //  double e;             /* meters east */
  //} GisCoordUTM;
  GisCoordUTM utm;
  GisCoordLatLon latlon;

  int cruiseOrder   = 1;
  int steeringOrder = 3;
  int engineOrder   = 1;
  double cruiseVector[1];	// state vector of the cruise control (velocity)
  double steeringVector[3];	// state vector of the planar dynamics (x, y, theta)
  double engineVector[1];	// state vector of the engine (torque)
  double cruiseInput[1], steeringInput[2], engineInput[2];
  double throttleOutput, brakeOutput;

  // Open the file for appending
  ofstream out_file("planar.dat", ios::out | ios::binary);
  //out_file << "steer \t\t accel  \t\t time \t\t vel \t\t lat \t\t lon \t\t theta" << endl;

  //WINDOW * arbw;
  //arbw = get_arb_window();

  // Initializing
  if(TRUE){
    boost::recursive_mutex::scoped_lock sl_gps(d.gpsLock);
    d.gpsDW.data.lat = 34.14;
    d.gpsDW.data.lng = -118.04; //coordinates from Santa Anita Parking lot
    d.gpsDW.data.vel_n = 0;
    d.gpsDW.data.vel_e = 0;
    d.gpsDW.data.vel_u = 0;
    d.theta = 0;
  }

  t = 0;
  while(!d.shutdown_flag){

    if( DISPLAY ) {
      ofstream mycout("dispfile_sim");
      //    mycout << "steer \t\t accel  \t\t time \t\t vel \t\t lat \t\t lon \t\t theta" << endl;
      // WHY DOESN'T THIS WORK??????????????????
      //wprintw( arbw, "boogabooga ");
      //wrefresh( arbw );
    }

    // BOB SCOPE
    if (TRUE){

      // Getting the actual state
      // we want to make the simulator as transparent as possible, so we're 
      // operating mostly on latitude and longitude data
      latlon.latitude = d.gpsDW.data.lat;
      latlon.longitude = d.gpsDW.data.lng;
      //cout << "LAT(" << latlon.latitude << ") LON(" << latlon.longitude << ")" << endl;
      // convert to UTM
      if (!gis_coord_latlon_to_utm (&latlon, &utm, GEODETIC_MODEL))
      {
        fprintf (stderr, "Error converting coordinate to utm.\n");
        exit(-1);
      } 
      // set the northing and easting variables that we use here       
      northing = utm.n;
      easting = utm.e;
      //cout << "UTM_E(" << easting << ") UTM_N(" << northing << ")" << endl;
 
      vel_n = d.gpsDW.data.vel_n;
      vel_e = d.gpsDW.data.vel_e;
      vel_u = d.gpsDW.data.vel_u;
      vel = sqrt(vel_n*vel_n + vel_e*vel_e + vel_u*vel_u);
      theta = d.theta;
      accel = d.accel;
      steer = -d.steer*MAX_STEER; //the convention of the simulator signal is other than the program
    
      // accel - t - v

      out_file.precision(10);
      out_file << steer << "\t\t" << accel << "\t\t" << t << "\t\t" << vel<< "\t\t" << easting << "\t\t" << northing << "\t\t" << theta << endl;
      //cout << steer << "\t\t" << accel << "\t\t" << vel<< "\t\t" << easting << "\t\t" << northing << endl;
      if( DISPLAY ) {
//  mycout << steer << "\t\t" << accel << "\t\t" << vel<< "\t\t" << easting << "\t\t" << northing << endl;
}
      cruiseVector[0] = vel;
      steeringVector[0] = easting;   // x
      steeringVector[1] = northing;  // y
      steeringVector[2] = theta;     // heading

      // Getting the acceleration torque
      throttleOutput = throttleActuator(accel);
       
      // Getting the brake torque
      brakeOutput = brakeActuator(accel,vel);
      
      // Passing thru the engine lag
      engineInput[0] = ENGINE_LAG;
      engineInput[1] = throttleOutput;
      runge4(engineOrder, t, engineVector, GPS_DELTAT,f_lag,engineInput);

      // Converting the torques into forces
      cruiseInput[0] = tire(engineVector[0],brakeOutput);
            
      // Computing next state, assuming the actuator as a force
      runge4(cruiseOrder, t, cruiseVector, GPS_DELTAT,fv,cruiseInput);
      vel = cruiseVector[0];

      // Getting the actual stearing position
      // steeringInput[0] = d.steeringDW.data.actuator;
      steeringInput[0] = steer;
      steeringInput[1] = vel;

      // Computing next state of the planar movement
      runge4(steeringOrder, t, steeringVector, GPS_DELTAT,fsteering,steeringInput);
      easting 	= steeringVector[0];
      northing 	= steeringVector[1];
      theta = steeringVector[2];

      // change our new easting and northing back to lat/lon
      if (!gis_coord_utm_to_latlon (&utm, &latlon, GEODETIC_MODEL))
      {
        fprintf (stderr, "Error converting coordinate to latlon.\n");
        exit (1);
      }

      // Updating the next state -- THIS IS WHERE THE VEHICLE STATE IS ACTUALLY UPDATED

      // GPS
      boost::recursive_mutex::scoped_lock sl_gps(d.gpsLock);
      d.gpsDW.data.lat = latlon.latitude;
      d.gpsDW.data.lng = latlon.longitude;
      d.gpsDW.data.vel_n = vel*sin(theta);
      d.gpsDW.data.vel_e = vel*cos(theta);
      d.gpsDW.data.vel_u = vel_u;
      d.theta = theta;
      d.gpsValid = TRUE;
      d.gpsNewData = TRUE;

      // Magnetometer
      boost::recursive_mutex::scoped_lock sl_mag(d.magLock);
      for (int i=0;i < 7;i++){
        d.magBuffer[i] = 0;
      }
      d.magValid = TRUE;
      d.magNewData = TRUE;

      // IMU
      boost::recursive_mutex::scoped_lock sl_imu(d.imuLock);      
      d.gotimu = 1;
      d.stateValidFromIMU = 1;
      d.stateNewDataFromIMU = 1;

      // State
      boost::recursive_mutex::scoped_lock sl_state(d.stateLock);
      d.SS.pitch = 0;
      d.SS.roll = 0;
      d.SS.altitude = 0;
      d.SS.heading = 0;
      d.SS.vel_u = 0;
      d.SS.vel_n = vel*sin(theta);;
      d.SS.vel_e = vel*cos(theta);;
      d.SS.speed = vel;
      d.SS.easting = easting;
      d.SS.northing = northing;
      d.stateValidFromGPS = 1;
      d.stateNewDataFromGPS = 1;

      printf("STEER(%f) ACCEL(%f) VEL(%10.2f) X(%10.2f) Y(%10.2f)\n", steer, accel, vel, easting, northing);
    }

    t += GPS_DELTAT;
    usleep_for(100);

  } // end while(...) 

  // make sure we close the pipes
  // WHAT'S THE RIGHT WAY TO DO THIS?  I KEEP GETTING BROKEN PIPES! --LARS
//  ofstream mycout("dispfile_sim");
//  mycout.close();
  
} // end SimLoop()
