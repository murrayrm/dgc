#ifndef WAYPOINTNAV_HH
#define WAYPOINTNAV_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>
//#include "../../state/state.h"
#include "../../gps/gps.h"
//#include "../../latlong/LatLong.h"  // Currently in other headers
#include "../../magnetometer/magnetometer.h"
#include "../../moveCar/brake.h"
#include "../../moveCar/accel.h"
//#include "../../moveCar/TLL.h"
#include "../../moveCar/cruise.h"
#include "../../Msg/dgc_time.h"
//#include "Msg/dgc_const.h"
#include "../../steerCar/steerCar.h"
#include "../../steerCar/steerState.hh"
#include "../../OBDII/OBDII.h"
#include "../../Arbiter/Arbiter.h"
#include "global.hh"
#include "../../include/Constants.h" //for vehicle constants, ie serial ports, etc.
#include "../../display/DisplayFcns.h" // for ncurses display

#include "../../state/imu/rot_matrix.hh"

using namespace std;

// Oh-so-bad, but I'm hardcoding the number of Voters anyway.
// PLEASE FIXE ME!

//static int numVoters = 4;
#define NUM_VOTERS 3

static int NUMARCS = 25;
const double MAX_STEER = 25*M_PI/180;      // maximum steering angle in radians

const double MAX_VELO = 5.0;      // 12/6/2003
const double MIN_VELO = 2.0;      // 12/6/2003

const double MAX_SPEED = 2;              // m/s
static double LEFTPHI = -1 * MAX_STEER;
static double RIGHTPHI = MAX_STEER;
static int MAX_GOODNESS = 250;           // Max goodness of a Vote
static int GW = 1;                       // global voter weight constant 
static int DFEW = 0;                     // DFE voter weight constant
static int LOCAL_W = 0;                      // Ladar voter weight constant
static int LADAR_W = 0;                      // Ladar voter weight constant

// DEBUG
//static bool SIM = false;
//static bool SIM = true;


struct state_struct
{
   double timestamp;	// time stamp when GPS data is received
   double northing;	// UTM northing
   double easting;	// UTM easting
   float  altitude;	// guess it
   float  vel_n;	// north velocity
   float  vel_e;	// east velocity
   float  vel_u;	// up velocity
   float  heading;	// heading in degrees, straight from the magnetometer. north=0 degree, clockwise increase
   float  pitch;	// pitch in degrees
   float  roll; 	// roll in degrees
   float speed;         // magnitude of velocity in m/s
   float heading_n;     // north vector of heading
   float heading_e;     // east vector of heading
};


// state declarations

#define DEFAULT_MAGNETOMETER_SERIAL_PORT	7
#define DEFAULT_GPS_SERIAL_PORT			6

#define STATE_POLL_DELAY	400000
#define CHECK_MESSAGE_DELAY     50000

/* If velocities are less than these values, consider them to be 0. */
#define VEL_N_VALID_CUTOFF 	0.1
#define VEL_E_VALID_CUTOFF 	0.1
#define VEL_U_VALID_CUTOFF 	0.1

/* When the speed of the car is higher than this, then don't use magnetometer */
#define MAGNET_VALID_CUTOFF	1  // in m/s

// initial heading, pitch, and roll
// car has a natural 3 degree down pitch
// assumes starting facing north, as soon as we start moving it'll figure out which way 
// we're actually pointed
const double p0 = -.052;
const double r0 = 0;
const double h0 = 0;

struct DATUM {
  
  // for data logging 
  int TestNumber; 
  string TestNumberStr;

  // Time zero
  timeval TimeZero;

  // Module Level stuff
  int shutdown_flag;

  // Magnetometer stuff
  boost::recursive_mutex magLock;
  int magValid; // flag to determine if mag is valid (new data)
  int magNewData;
  float magBuffer[7];
  timeval magTS;

  // GPS Stuff 
  boost::recursive_mutex gpsLock;
  int gpsValid;
  int gpsNewData;
  gpsDataWrapper gpsDW;
  timeval gpsTS;

  // OBD-II Stuff
  boost::recursive_mutex obdLock;
  int obdValid;
  int obdNewData;
  float obdSpeed;
  float obdRPM;

  // State Struct for driving code to use
  boost::recursive_mutex stateLock;
  state_struct SS;
  int stateNewDataFromIMU;
  int stateValidFromIMU;
  int stateNewDataFromGPS;
  int stateValidFromGPS;

  // Accel Data
  float targetVelocity;  // in m/s
  float accel; // normalized value in [-1,1], maps to throttle/brake
  int accelValid;

  // Stearing Data
  double steer; // command to the steering actuator
  double theta; // actual heading

  // Arbiter Data
  Arbiter demoArb;
  // Voters - change Arbiter to use an array of Voters???, maybe a Vector
  //  Voter voterArray[NUM_VOTERS];
  int numVoters;
  Voter *voterArray;

  // Global Data
  boost::recursive_mutex globalLock;
  struct WaypointFollowing WF;
  //  WaypointFollowing WF;
  int globalNum;
  int globalWeight;

  // DFE Data
  boost::recursive_mutex DFELock;
  int DFEnum;
  int DFEweight;
  
  // Ladar Data
  int LadarNum;
  int LadarWeight;
  
  // Local Data
  boost::recursive_mutex LocalLock;
  int LocalNum;
  int LocalWeight;

  // IMU data 
  boost::recursive_mutex imuLock;
  int gotimu;
  rot_matrix N2B;

  // simulation loop data
  // MAYBE WE DON'T NEED THIS(?) --LARS
  // NEEDED ONLY IF SIM ACCESSES VARIABLES IN DATUM --SUE ANN
  //boost::recursive_mutex SimLock;

  // A lock for actually allowing the software to drive the car
  boost::recursive_mutex drivingLock;
  int drivingValid;
};

// Method protocols
void InitGlobal(DATUM &d);
void GlobalLoop(DATUM &d);

int InitDFE(DATUM &d);
void DFELoop(DATUM &d);


int  InitMag(DATUM &d); 
void GrabMagLoop(DATUM &d);

int  InitOBD(DATUM &d); 
void GrabOBDLoop(DATUM &d);

//int  InitStateFilter(DATUM &d);
//void StateFilterLoop(DATUM &d);

int  InitAccel(DATUM &d);
void AccelLoop(DATUM &d);

int InitLadar(DATUM &d);
void LadarLoop(DATUM &d);

int InitLocal(DATUM &d);
void LocalLoop(DATUM &d);

void DisplayAll(DATUM &d);

void SimLoop(DATUM &d);

float cruise(float targetVelo, float curVelo, float curAccel);

int InitIMU(DATUM &state_dat);
void GrabIMULoop(DATUM &state_dat);

int  InitGPS(DATUM &d);
void GrabGPSLoop(DATUM &d);
void GPSsimstub(DATUM &d);

#endif 
