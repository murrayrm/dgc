//#include "../../state/state.h"
//#include "../../Global/Vector.hh"
//#include "../../latlong/LatLong.h"
//#include "../../Msg/dgc_time.h"
#include <fstream>
#include <unistd.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <algorithm>  // for min
#include "../../Arbiter/Arbiter.h"

#include "waypointnav.hh"

#include "DynamicFeasibilityEvaluator/vehicle_constants.h"
#include "DynamicFeasibilityEvaluator/rollover_functions.h"

using namespace std;

extern int SIM; // get the simulation flag from the main function
extern int PRINT_VOTES; // get the print_votes flag from the main function

int InitDFE(DATUM &d) {

  d.voterArray[d.DFEnum] = Voter(NUMARCS, LEFTPHI, RIGHTPHI);
  d.DFEweight = DFEW;
  cout << "adding DFE voter to demoArb." << endl;
  d.demoArb.addVoter(&(d.voterArray[d.DFEnum]), d.DFEweight);
  cout << "DFE Voter Added" << endl;
  // Other initialization tasks

  return 0;
}

void DFELoop(DATUM &d) {

    // d.SS.speed is the vehicle velocity, from GPS
    double current_velocity = d.SS.speed;

    // TODO: HACK: NOTE:  d should also have magnetometer data (with yaw, pitch roll), but it's not available yet
    // so for now, set yaw, pitch, roll to ZERO
    double yaw = 0;
    double pitch =0;
    double roll = 0;

    /* Given yaw, pitch, roll, figure out vertical and lateral components of gravity in the 
     * vehicle reference frame */
    double g_z, g_y, g_x;
    gravity_components_in_vehicle_ref_frame(yaw, pitch, roll, &g_z, &g_y, &g_z);

    // TODO: HACK: NOTE: rollover safety threshold should be an input to the DFE, given
    //                   by Arbiter (or the race strategist)
    // for now, set it's value here.
    double T_roll = 0.2;

    /* Given rollover safety threshold compute min and max lateral accelerations */
    double a_R, a_L;
    min_max_lateral_acceleration_for_rollover_safety(g_z, g_y, T_roll, &a_R, &a_L);

    double a_T; // [m/s^2] the total lateral acceleration of the vehicle, due to following the specified trajectory
                // at the specified speed

  while(d.shutdown_flag == FALSE) {

    // lock state if need to
    //    boost::recursive_mutex::scoped_lock slSTATE(d.stateLock);
    // Should take in velocity data and deal with it, but for now this is
    // a very stupid evaluator

    // Evalute arcs here
    int goodness = 0;
    double timestamp = 0;        // Dummy for now

    Voter::voteListType wayPt = d.voterArray[d.DFEnum].getVotes();

    // Now wayPt should contain all the rigth arcs to be evaluated.
    // So evaluate them.
    for (Voter::voteListType::iterator iter = wayPt.begin(); iter != wayPt.end(); ++iter) 
    {
        // // iter->velo = veloLookupTable(iter->phi);
        // iter->velo = max(0, (int)(round(MAX_SPEED - fabs(iter->phi) 
        //   		      * MAX_SPEED / MAX_STEER)));
        // // An arc is good if we can travel at more than 0      
        // iter->goodness = min( MAX_GOODNESS, MAX_GOODNESS * (int)(iter->velo == 0) );

        
        // use code from DynamicFeasibilityEstimator/dynamic_feasibility_evaluator.cc
        
        double arc_steering_angle = iter->phi;
        
        double arc_radius = steering_angle_2_arc_radius( arc_steering_angle );
        // arc_radius is positive for rightward turns, negative for leftwards turns,
        // and ZERO for moving straigh ahead (more useful than infinity)
        
        if (arc_steering_angle == 0)
        {
            // special case of moving straight ahead, total lateral acceleration is zero
            a_T = 0;
        }
        else
        {
            a_T = current_velocity*current_velocity/arc_radius;
        }
        
        // given the current velocity and arc (i.e.; current lateral acceleration)
        // compute the rollover coefficient
        double c_roll = rollover_coefficient(a_T, g_z, g_y);
        
        double min_safe_velocity, max_safe_velocity;
        min_max_velocity_for_rollover_safety(a_R, a_L, arc_radius, &min_safe_velocity, &max_safe_velocity);

        // set the vote goodness and max velocity
        // TODO: NOTE: I think the DFE should return more info than just the
        //             goodness of an arc at the current velocity, 
        //             it should evaluate all combinations of arcs and velocities
        //             and take into account the dynamics of changing the velocity as well!!!

        if (max_safe_velocity > MAX_SPEED)
        {
            iter->velo = MAX_SPEED;
        }
        else
        {   
            iter->velo = max_safe_velocity;
        }

        // convert rollover coefficient for arc @ current velocity into 
        // a 'goodness'
        // goodness = 0   : very bad    if c_roll >= T_roll
        // goodness = 250 : very good   if c_roll == 0
        // TODO: NOTE: we may want to have the range T_roll < c_roll < 1 not
        //             all be mapped to zero goodness, since they are still
        //             feasible, but just not very desireable

        if (c_roll > T_roll)
        {
            iter->goodness = 0;
        }
        else
        {
            iter->goodness = (int) round( MAX_GOODNESS * (1.0 - c_roll/T_roll) );
        }
    
    } // end iterating over arcs to vote on

    // lock Global?
    //    boost::recursive_mutex::scoped_lock slGLOBAL(d.globalLock);
    // Update this evaluator's Voter's voteList.
    d.voterArray[d.DFEnum].updateVotes(wayPt, timestamp);
    if( PRINT_VOTES ) {
      // print the votes we're sending to the arbiter
      //cout << "--DFELoop-- DFE votes" << endl;
      //d.voterArray[d.DFEnum].printVotes();   /////////////////////////
      //cout << "--DFE.cc-- Sleeping so you can read my output." << endl;
      //sleep(2);
    }
    usleep_for(10000);      // Evaluation rate

  } // end while(d.shutdown_flag == FALSE)

} // end DFELoop(DATUM &d)
