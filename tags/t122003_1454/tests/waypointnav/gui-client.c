/* gui-client.c

   This is a standalone implementation of gui_client_create_log_file().

   It creates a logfile, and spawns a child process to pipe the
   log to the GUI.

   NOTE: we use the 'Log' directive here because this file
   will probably be cut-n-pasted into other people's projects.

   $Log: gui-client.c,v $
   Revision 1.1  2003/12/13 07:19:21  lars
   dave benson's gui stuff. from Santa Anita test on 12/06/03.

   Revision 1.4  2003/11/21 00:36:25  daveb
   *** empty log message ***

   Revision 1.3  2003/11/21 00:31:30  daveb
   Fixes numerous bugs in the image-sequence parser.

   Revision 1.2  2003/11/20 23:29:57  daveb
   *** empty log message ***

   Revision 1.1.1.1  2003/11/20 21:40:23  daveb
   Initial import of the GrandChallenge visualization gui,
   by Dave Benson.
 */

#include "gui-client-globals.h"
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/poll.h>
#include <signal.h>
#include <assert.h>

#define gboolean int
#define TRUE	1
#define FALSE	0

#define IS_DEBUGGING	0

/* --------------------------------------------------------------------- */
/* Constants and Tables */

/* What types of log-file scanning the child process knows how to do. */

/* table of module-name => port-offset */
/* Define: services[], n_services */
static struct
{
  const char *name;
  unsigned port_offset;
} services[] = {
  PORT_NAME_TO_OFFSET_MAP
};
#define n_services 	(sizeof(services)/sizeof(services[0]))

#define MIN(a,b)	(((a) < (b)) ? (a) : (b))

/* --------------------------------------------------------------------- */
/* Function prototypes. */
typedef struct _Scanner Scanner;

/* the function which runs in the child process until
   the parent process dies. */
static void child_process (int         pipe_read_end,
			   const char *log_filename,
			   const char *hostname,
			   int         port_number,
			   Scanner    *scanner);

/* connect to a server */
static int         my_connect_tcp    (const char* host,
				      int         port,
				      int         verbose);

/* scanner definition */

static Scanner *scanner_new_line_based (void);
static Scanner *scanner_new_image_based (void);

/* Returns the first byte offset of a record's start,
   or -1 if no record start was found. */
static int scanner_scan_data (Scanner *scanner, unsigned char *data, unsigned length);

static void scanner_reset (Scanner *scanner);

static void scanner_destroy (Scanner *);



/* --------------------------------------------------------------------- */
/* Public interface. */

/* Begin logging for a particular module. */
FILE *gui_client_create_log_file (const char *module_name)
{
  char filename[2048];
  FILE *out;

  const char *hostname = getenv ("GUI_HOST");
  const char *port_base_str = getenv ("GUI_PORT_BASE");
  const char *log_directory = getenv ("GUI_LOG_DIRECTORY");
  const char *suppress_logging_str = getenv ("GUI_SUPPRESS_LOGGING");
  const char *suppress_connecting_str = getenv ("GUI_SUPPRESS_CONNECTING");

  int suppress_logging = suppress_logging_str ? atoi (suppress_logging_str) : 0;
  int suppress_connecting = suppress_connecting_str ? atoi (suppress_connecting_str) : 0;
  int port_base = port_base_str ? atoi (port_base_str) : 11000;
  int pid = getpid ();
  int port;

  int pipe_fds[2];
  unsigned i;
  int fork_rv;

  signal (SIGPIPE, SIG_IGN);

  if (log_directory == NULL)
    log_directory = ".";
  if (hostname == NULL)
    hostname = "localhost";

  if (suppress_logging)
    return fopen ("/dev/null", "wb");

  snprintf (filename, sizeof (filename), "%s/%s-%d.log", log_directory, module_name, pid);
  out = fopen (filename, "wb");
  if (out == NULL)
    return NULL;

  if (suppress_connecting)
    return out;

  /* Fork a process to watch the logfile.
   * We also pass the process the read-end of a pipe,
   * which will be closed when this process dies.
   */
  if (pipe (pipe_fds) < 0)
    {
      fprintf (stderr, "error launching gui client! Error creating pipe: %s.\n",
	       strerror (errno));
      return out;
    }

  for (i = 0; i < n_services; i++)
    if (strcmp (services[i].name, module_name) == 0)
      break;
  if (i == n_services)
    {
      fprintf (stderr, "error: module '%s' is not known.  (At least it is missing from the PORT_NAME_TO_OFFSET_MAP macro...)\n",
	       module_name);
      close (pipe_fds[0]);
      close (pipe_fds[1]);
      return out;
    }

  port = port_base + services[i].port_offset;

  fork_rv = fork ();
  if (fork_rv == -1)
    {
      fprintf (stderr, "error forking child process: %s.\n", strerror (errno));
      return out;
    }

  if (fork_rv == 0)
    {
      Scanner *scanner;
      sleep(5);
      if (strcmp (module_name, "stereovision") == 0)
	scanner = scanner_new_image_based ();
      else
	scanner = scanner_new_line_based ();
      signal (SIGPIPE, SIG_IGN);
      child_process (pipe_fds[0], filename, hostname, port, scanner);
      scanner_destroy (scanner);
      exit (0);
    }
  close (pipe_fds[0]);

  /* pipe_fds[1] will be closed when the parent process terminates.
     at the point the child process will get notified that pipe_fds[0]
     is ready-to-read (via select(2)). */

  return out;
}


/* --------------------------------------------------------------------- */
/* The child process which watches the logfile.

   If it is not connected to the GUI it will periodically try to reconnect.

   It tails along the logfile.
 */
static int subtract_timeouts_millis (const struct timeval *a, const struct timeval *b)
{
  return (a->tv_sec - b->tv_sec) * 1000
       + (a->tv_usec - b->tv_usec) / 1000;
}

static void
add_millis_to_timeval (struct timeval *tv, unsigned ms)
{
  tv->tv_usec += (ms%1000) * 1000;
  if (tv->tv_usec >= 1000*1000)
    {
      tv->tv_sec++;
      tv->tv_usec -= 1000 * 1000;
    }
  tv->tv_sec += ms / 1000;
}

static void child_process (int         pipe_read_end,
			   const char *log_filename,
			   const char *hostname,
			   int         port_number,
			   Scanner    *scanner)
{
  struct pollfd pollfds[2];
  struct timeval next_connection_time;
  struct timeval next_tail_wakeup;
  int gui_fd = -1;
  int log_fd = open (log_filename, O_RDONLY);
  int at_eof = 0;
  struct timeval cur_time;
  int new_client = 1;

  /* state */
  gettimeofday (&cur_time, NULL);
  next_tail_wakeup = next_connection_time = cur_time;

  while (pipe_read_end >= 0 || gui_fd >= 0)
    {
      unsigned n_pollfds = 0;
      int timeout = -1;
      unsigned i;

restart_loop:

      if (pipe_read_end >= 0)
	{
	  pollfds[n_pollfds].fd = pipe_read_end;
	  pollfds[n_pollfds].events = POLLIN|POLLHUP;
	  n_pollfds++;
	}

      if (!at_eof && gui_fd >= 0)
	{
	  pollfds[n_pollfds].fd = gui_fd;
	  pollfds[n_pollfds].events = POLLOUT;
	  n_pollfds++;
	}

      /* compute timeout */
      if (at_eof)
	{
	  timeout = subtract_timeouts_millis (&next_tail_wakeup, &cur_time);
	  if (timeout < 0)
	    timeout = 0;
	}
      if (gui_fd <= 0)
	{
	  int tmp_timeout = subtract_timeouts_millis (&next_connection_time, &cur_time);
	  if (tmp_timeout < 0)
	    tmp_timeout = 0;
	  if (timeout < 0 || tmp_timeout < timeout)
	    timeout = tmp_timeout;
	}

      /* do polling */
      if (poll (pollfds, n_pollfds, timeout) < 0)
	{
	  if (errno == EINTR)
	    goto restart_loop;

	  fprintf (stderr, "client gui child process: error running poll(2): %s.\n",
		   strerror (errno));
	  return;
	}

      gettimeofday (&cur_time, NULL);

      for (i = 0; i < n_pollfds; i++)
	{
	  if (pollfds[i].fd == pipe_read_end
	   && (pollfds[i].revents & (POLLIN|POLLHUP)) != 0)
	    {
	      close (pipe_read_end);
	      pipe_read_end = -1;
	      if (at_eof)
		{
		  gui_fd = -1;
		  close (gui_fd);
		}
	    }
	  else if (pollfds[i].fd == gui_fd
	        && (pollfds[i].revents & (POLLHUP|POLLERR)) != 0)
	    {
	      close (gui_fd);
	      gui_fd = -1;
	      next_connection_time = cur_time;
	      add_millis_to_timeval (&next_connection_time, 5000);
	    }
	  else if (pollfds[i].fd == gui_fd
	        && (pollfds[i].revents & POLLOUT) == POLLOUT)
	    {
	      char buf[4096];
	      if (new_client)
		{
		  /* scan for the next boundary.
		     once found, reset the scanner,
		     and clear the new_client flag.
		     if not found, raise at_eof. */
		  for (;;)
		    {
		      int n_read = read (log_fd, buf, sizeof (buf));
		      int start_offset;
		      if (n_read < 0)
			{
			  if (errno == EINTR)
			    continue;
			  fprintf (stderr, "Error reading logfile: %s.\n", strerror (errno));
			}
		      if (n_read == 0)
			{
			  /* hm, got to eof with no boundary. */
			  at_eof = 1;
			  next_tail_wakeup = cur_time;
			  add_millis_to_timeval (&next_tail_wakeup, 300);
			  goto restart_loop;
			}
		      start_offset = scanner_scan_data (scanner, buf, n_read);
		      if (start_offset >= 0)
			{
			  lseek (log_fd, -(n_read - start_offset), 1);
			  scanner_reset (scanner);
			  new_client = 0;
			  break;
			}
		      /* continue scanning... */
		    }
		}
	      assert (!new_client);

	      /* try writing data to the gui_fd */
	      int n_read = read (log_fd, buf, sizeof (buf));
	      at_eof = (n_read == 0);
	      if (n_read < 0)
		{
		  fprintf (stderr, "error reading logfile: %s\n", strerror (errno));
		  return;
		}
	      if (!at_eof)
		{
		  int n_written = write (gui_fd, buf, n_read);
		  if (n_written < 0)
		    {
		      if (errno == EPIPE)
			{
			  /* shut down... */
			  close (gui_fd);
			  gui_fd = -1;
			  next_connection_time = cur_time;
			  add_millis_to_timeval (&next_connection_time, 5000);
			  n_written = 0;
#if IS_DEBUGGING
			  fprintf (stderr, "got EPIPE\n");
#endif
			}
		      else if (errno == EINTR || errno == EAGAIN)
			n_written = 0;
		      else
			{
			  fprintf (stderr, "error writing to gui-fd: %s.\n", strerror(errno));
			  return;
			}
		    }
		  if (n_written > 0)
		    /* We are just copying all data... we don't really care where
		       the record boundaries happen to fall... that's for the GUI itself
		       to worry about. */
		    (void) scanner_scan_data (scanner, buf, n_written);
		  if (n_read != n_written)
		    {
		      lseek (log_fd, -(n_read - n_written), 1);
		    }
		  
		}

              /* if at_eof, reset next_tail_wakeup */
              next_tail_wakeup = cur_time;
	      add_millis_to_timeval (&next_tail_wakeup, 300);
	    }
	}

      /* if it's time, try connecting again */
      if (gui_fd == -1 && subtract_timeouts_millis (&next_connection_time, &cur_time) <= 0)
	{
#if IS_DEBUGGING
	  fprintf(stderr, "trying to connect to %s:%u\n", hostname, port_number);
#endif
	  gui_fd = my_connect_tcp (hostname, port_number, IS_DEBUGGING);
	  if (gui_fd < 0)
	    {
#if IS_DEBUGGING
	      fprintf(stderr, "failed... will retry.\n");
#endif
	      next_connection_time = cur_time;
	      add_millis_to_timeval (&next_connection_time, 5000);
	    }
	  else
	    {
	      /* make gui_fd nonblocking */
	      fcntl (gui_fd, F_SETFL, fcntl (gui_fd, F_GETFL) | O_NONBLOCK);
#if IS_DEBUGGING
	      fprintf (stderr, "gui child: successfully connected.\n");
#endif
	    }
	}

      /* if it's time, reset at_eof */
      if (at_eof && subtract_timeouts_millis (&next_tail_wakeup, &cur_time) <= 0)
	{
	  /* reset the EOF flag in the kernel */
	  lseek (log_fd, 0, 1);

	  at_eof = 0;
	}

      gettimeofday (&cur_time, NULL);
    }
#if IS_DEBUGGING
  fprintf(stderr, "gui child: done... returning.\n");
#endif
}

/* --------------------------------------------------------------------- */
/* --- Scanner implementation --- */

struct _Scanner
{
  /* Returns the first byte offset of a record's start,
     or -1 if no record start was found. */
  int  (*scan) (Scanner       *scanner,
		unsigned char *data,
		unsigned       length);

  void (*reset) (Scanner *scanner);

  void (*destroy) (Scanner    *scanner);
};

static int scanner_scan_data (Scanner *scanner, unsigned char *data, unsigned length)
{
  return (*scanner->scan) (scanner, data, length);
}

static void scanner_reset (Scanner *scanner)
{
  return (*scanner->reset) (scanner);
}

static void scanner_destroy (Scanner *scanner)
{
  return (*scanner->destroy) (scanner);
}

/* --- Line-Based Scanner implementation --- */
typedef struct _LineBasedScanner LineBasedScanner;
struct _LineBasedScanner
{
  Scanner base_instance;
  int is_at_line_start;
};

static int  line_based_scanner_scan (Scanner       *scanner,
				     unsigned char *data,
				     unsigned       length)
{
  LineBasedScanner *lb_scanner = (LineBasedScanner *) scanner;
  unsigned char *nl;
  int rv = -1;
  if (lb_scanner->is_at_line_start)
    rv = 0;
  else if ((nl = memchr (data, '\n', length - 1)) != NULL)
    {
      rv = nl + 1 - data;
    }
  lb_scanner->is_at_line_start = (data[length - 1] == '\n');
  return rv;
}

static void line_based_scanner_reset (Scanner    *scanner)
{
  LineBasedScanner *lb_scanner = (LineBasedScanner *) scanner;
  lb_scanner->is_at_line_start = 1;
}

static void line_based_scanner_destroy (Scanner    *scanner)
{
  free (scanner);
}

static Scanner *scanner_new_line_based (void)
{
  LineBasedScanner *s = (LineBasedScanner *) malloc (sizeof (LineBasedScanner));
  if (s == NULL)
    return NULL;
  s->base_instance.scan = line_based_scanner_scan;
  s->base_instance.reset = line_based_scanner_reset;
  s->base_instance.destroy = line_based_scanner_destroy;
  s->is_at_line_start = 1;
  return &s->base_instance;
}

/* --- image-based scanner --- */
typedef struct _ImageBasedScanner ImageBasedScanner;
struct _ImageBasedScanner
{
  Scanner base_instance;

  int next_is_record_bdy;

  /* bytes_remaining_in_image == 0, then we are processing the header,
     and will be until header_newline_count==2.
     at that point we must parse the header lines (description / width,height,depth)
     at set bytes_remaining_in_image accordingly. */
  unsigned char *header_buffer;
  unsigned header_buffer_len;
  unsigned header_buffer_alloced;
  unsigned header_newline_count;

  unsigned bytes_remaining_in_image;
};

static int  image_based_scanner_scan (Scanner       *scanner,
				      unsigned char *data,
				      unsigned       length)
{
  ImageBasedScanner *ib_scanner = (ImageBasedScanner *) scanner;
  unsigned char *orig_data = data;
  int rv = -1;
  if (ib_scanner->next_is_record_bdy)
    rv = 0;
  ib_scanner->next_is_record_bdy = 0;
  while (length > 0)
    {
      if (ib_scanner->bytes_remaining_in_image == 0)
	{
	  unsigned char c;
	  if (ib_scanner->header_buffer_len == ib_scanner->header_buffer_alloced)
	    {
	      unsigned char *new_buffer = (unsigned char *)
		                          realloc (ib_scanner->header_buffer,
						   ib_scanner->header_buffer_alloced * 2);
	      if (new_buffer == NULL)
		{
		  fprintf(stderr, "out-of-memory!!!!\n");
		  return -1;
		}
	      ib_scanner->header_buffer = new_buffer;
	      ib_scanner->header_buffer_alloced *= 2;
	    }
	  c = *data++;
	  length--;
	  ib_scanner->header_buffer[ib_scanner->header_buffer_len] = c;
	  ib_scanner->header_buffer_len++;
	  if (c == '\n')
	    {
	      ib_scanner->header_newline_count++;
	      if (ib_scanner->header_newline_count == 2)
		{
		  /* Parse header. */
		  unsigned char *desc_end = memchr (ib_scanner->header_buffer, '\n', ib_scanner->header_buffer_len);
		  char *wh_line = (char*)desc_end + 1;
		  unsigned width, height, depth;
		  char *at = wh_line;
		  char *end;
		  int packed = 0;
		  unsigned size;
		  ib_scanner->header_buffer[ib_scanner->header_buffer_len-1] = 0;
		  width = strtoul (at, &end, 10);
		  if (at == end)
		    {
		      fprintf(stderr, "error parsing width from image header.\n");
		      return -1;
		    }
		  at = end;
		  height = strtoul (at, &end, 10);
		  if (at == end)
		    {
		      fprintf(stderr, "error parsing height from image header.\n");
		      return -1;
		    }
		  at = end;
		  depth = strtoul (at, &end, 10);
		  if (depth == 1 && (*end == 'p' || *end == 'P'))
		    packed = 1;
		  switch (depth)
		    {
		    case 0:
		    case 8:
		      size = width * height;
		      break;
		    case 1: 
		      if (packed)
			size = (width * height + 7) / 8;
		      else
			size = (width + 7) / 8 * height;
		      break;
		    case 24:
		      size = width * height * 3;
		      break;
		    default:
		      {
			fprintf(stderr, "error parsing depth %u: unknown\n", depth);
			return -1;
		      }
		    }

		  ib_scanner->header_buffer_len = 0;
		  ib_scanner->header_newline_count = 0;
		  if (size == 0)
		    fprintf (stderr, "warning: got 0 length image\n");
		  ib_scanner->bytes_remaining_in_image = size;
		}
	    }
	}
      else
	{
	  unsigned transfer = MIN (length, ib_scanner->bytes_remaining_in_image);
	  ib_scanner->bytes_remaining_in_image -= transfer;
	  length -= transfer;
	  data += transfer;
	  if (ib_scanner->bytes_remaining_in_image == 0)
	    {
	      if (length == 0)
		ib_scanner->next_is_record_bdy = 1;
	      else if (rv == -1)
		rv = data - orig_data;
	    }
	}
    }
  return rv;
}

static void image_based_scanner_reset (Scanner *scanner)
{
  ImageBasedScanner *ib_scanner = (ImageBasedScanner *) scanner;
  ib_scanner->bytes_remaining_in_image = 0;
  ib_scanner->header_buffer_len = 0;
  ib_scanner->header_newline_count = 0;
}

static void image_based_scanner_destroy (Scanner *scanner)
{
  ImageBasedScanner *ib_scanner = (ImageBasedScanner *) scanner;
  free (ib_scanner->header_buffer);
  free (ib_scanner);
}

static Scanner *scanner_new_image_based (void)
{
  ImageBasedScanner *s = (ImageBasedScanner *) malloc (sizeof (ImageBasedScanner));
  if (s == NULL)
    return NULL;
  s->base_instance.scan = image_based_scanner_scan;
  s->base_instance.reset = image_based_scanner_reset;
  s->base_instance.destroy = image_based_scanner_destroy;
  s->header_buffer = malloc (1024);
  s->header_buffer_len = 0;
  s->header_buffer_alloced = 1024;
  s->header_newline_count = 0;
  s->bytes_remaining_in_image = 0;
  return &s->base_instance;
}

/* --------------------------------------------------------------------- */
/* --- my_connect_* implementations --- */
/*
    GDAM - Geoff & Dave's Audio Mixer
    Copyright (C) 1999    Dave Benson, Geoff Matters.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

    Contact:
        daveb@ffem.org <Dave Benson>
        geoff@ugcs.caltech.edu <Geoff Matters>
*/
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <netdb.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#ifndef MAX_UNIX_PATH
#define MAX_UNIX_PATH		108
#endif

/*
 * Lookup a computer's ip address from its host name.
 */
static gboolean gdam_host_name_lookup(const char* host, void* ip_addr_out)
{
  struct hostent *host_ent;
  host_ent = gethostbyname (host);
  if (!host_ent)
    {
      fprintf (stderr, "error doing dns lookup of %s: %s.\n",
		 host, hstrerror (h_errno));
      return FALSE;
    }
  /* IPv4 requirement... */
  assert (host_ent->h_length == 4);
  memcpy (ip_addr_out, host_ent->h_addr_list[0], 4);
  return TRUE;
}
/*
 * Pack a `struct sockaddr_in' from the host and port.
 */
static gboolean gdam_assemble_tcp_socket_addr(const char*         host,
                                              int                 port,
					      struct sockaddr_in* addr)
{
  memset (addr, 0, sizeof (struct sockaddr_in));
  if (!gdam_host_name_lookup (host, &addr->sin_addr))
    return FALSE;
  addr->sin_family = AF_INET;
  addr->sin_port = htons ((unsigned short) port);
  return TRUE;
}

/*
 * Perform a connection to an arbitrary sockaddr,
 * which may be unix or tcp style.
 *
 * If *pending is set, the connect must be polled and "gdam_connect_finished".
 */
static int
gdam_connect_sockaddr(struct sockaddr* inaddr, 
		      gboolean verbose)
{
  int fd;
  int protocol_family;
  int size;
  if (inaddr->sa_family == AF_INET)
    {
      protocol_family = PF_INET;
      size = sizeof (struct sockaddr_in);
    }
  else if (inaddr->sa_family == AF_UNIX)
    {
      protocol_family = PF_UNIX;
      size = sizeof (struct sockaddr_un);
    }
  else
    {
      protocol_family = inaddr->sa_family;
      size = sizeof (struct sockaddr);
    }
  fd = socket (protocol_family, SOCK_STREAM, 0);
  if (fd < 0)
    {
      if (verbose)
	fprintf (stderr, "error creating socket: %s\n", strerror (errno));
      return -1;
    }
retry_connect:
  if (connect (fd, inaddr, size) < 0)
    {
      if (errno == EINTR)
	goto retry_connect;
      if (errno != EINPROGRESS && errno != EWOULDBLOCK)
	{
	  if (verbose)
	    fprintf (stderr, "error connecting:  %s\n", strerror (errno));
	  close (fd);
	  return -1;
	}
    }
  return fd;
}

/*
 * Connect to a host/port pair.
 */
int my_connect_tcp(const char* host,
				   int port, 
				   gboolean verbose)
{
  struct sockaddr_in inaddr;
  if (!gdam_assemble_tcp_socket_addr (host, port, &inaddr))
    return -1;
  return gdam_connect_sockaddr ((struct sockaddr *) &inaddr, verbose);
}
