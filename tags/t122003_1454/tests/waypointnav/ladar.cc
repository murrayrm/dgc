#include "waypointnav.hh"
#include "Ladar/driver_klk/laserdevice.h"
#include "Ladar/Ladar.h"
#include "DynamicFeasibilityEvaluator/rollover_functions.h"

extern int SIM; // get the simulation flag from the main function
extern int PRINT_VOTES; // get the print_votes flag from the main function

// THIS IS NASTY BUT IT'LL WORK FOR NOW...
// TODO: fix this nastiness
CLaserDevice laser("/dev/ttyS9"); // NEW WAY
//CLaserDevice *laser;

int InitLadar(DATUM &d) {
  
  int result;
  char * port_name;
  d.voterArray[d.LadarNum] = Voter(NUMARCS, LEFTPHI, RIGHTPHI);
  d.LadarWeight = LADAR_W;
  d.demoArb.addVoter(&(d.voterArray[d.LadarNum]), d.LadarWeight);
  cout << "Added Ladar Voter" << endl;
  // Ladar initialization tasks
  if( 1 ) { // use this if you want to use the LADAR unit
  //if( !SIM ) { // use this if you truly want to simulate
    sprintf(port_name,"/dev/ttyS%d",LADAR_SERIAL_PORT);
    // THIS WAY DIDN'T WORK FOR ME (LARS) 12/20/03
    // BUT I DON'T THINK THIS WAS THE PROBLEM
    //CLaserDevice laser(port_name);  // NEW WAY
    //laser = new CLaserDevice(port_name); // change this to appropriate serial port
    result = laser.Setup();
    //result = laser->Setup();
    
    if ( result == 0 ) {
      return TRUE;
    } 
    else {
      return FALSE;
    }
  } 
  else {
    // Do simulation stuff here...
    return TRUE;
  }  
  
}

void LadarLoop(DATUM &d) {
   
  double laserdata[361];
  
  while(d.shutdown_flag == FALSE) {

  if( 1 ) { // use this if you want to use the LADAR unit
  //if( !SIM ) { // use this if you truly want to simulate
      // Take a scan
      laser.UpdateScan(); // NEW WAY
      //laser->UpdateScan();

      // update local data buffer
      for(int i = 0; i < 361; i++) {
	laserdata[i] = (laser.Scan[i]) / 1000; //note: laser returns data in mm.
	//laserdata[i] = (laser->Scan[i]) / 1000; //note: laser returns data in mm.
      }
    } 
    else {
      // fake the laser scan here...
      for (int i = 0; i < 361; i++) {
	laserdata[i] = LADAR_MAX_RANGE; 
      }

      for (int i = 160; i < 200; i++) {
	//laserdata[i] = LADAR_MAX_RANGE * .8;
      }
    }  

    //figure out the votes...
    int goodness = 0;
    double timestamp = 0;  //just a dummy timestamp
    Voter::voteListType wayPt = d.voterArray[d.LadarNum].getVotes();
    
    // Now wayPt should contain all the right arcs to be evaluated.
    // So evaluate them.
    for (Voter::voteListType::iterator iter = wayPt.begin();
	 iter != wayPt.end(); ++iter) {
      iter->velo = DEFAULT_VELOCITY;     // in meters/sec
      //TODO: actually set this to something useful
      double arc_steering_angle = iter->phi;
      double arc_radius = steering_angle_2_arc_radius( arc_steering_angle );
      // arc_radius is positive for right turns, neg for left turns,
      // and ZERO straight ahead
      
      double scanline_angle;
      double arc_scan_intersection;
      double arc_drive_dist;

      // TODO: note - some of the numbers below depend on what mode
      // the ladar is in, ie the number of scanlines depends on if you
      // are in .5deg mode, 1deg mode, or .25 deg mode.  these should
      // probably be #defined somewhere, but for now they'll just he
      // hardcoded in here.
      
      // TODO: need to take into account that the car is not a point.
      // this could fit in easly in the arc_scan_intersection <=
      // laserdata comparison.

      if ( arc_radius == 0 ) {
	// straight ahead, special case
	iter->goodness = MAX_GOODNESS;
	if ( laserdata[180] < LADAR_MAX_RANGE ) {
	  arc_drive_dist = laserdata[180];
	  iter->goodness = (int)(arc_drive_dist * MAX_GOODNESS / 
	                     (LADAR_MAX_RANGE * M_PI_2));
	}
      }
      else if ( arc_radius > 0 ) {
	// right turn, so only use scan lines on the right side scan
	// line # 181 is the "middle" -> index 180 since arrays start
	// at zero

	// set goodnes to max, and we will reduce it appropriately if
	// we can't drive the whole arc
	iter->goodness = MAX_GOODNESS;
	for ( int i = 179; i >= 0; i-- ) {
	  // start and middle and work outwards so as soon as we find
	  // a point that is too close, we can break
	  
	  // if scanline is max_range, just skip it.
	  if ( laserdata[i] >= LADAR_MAX_RANGE )
	    continue;
	  
	  // find the distance at which the scan line intersects the
	  // steering arc
	  scanline_angle = i * LADAR_ANGLE_RESOLUTION;
	  arc_scan_intersection = 2 * arc_radius * cos(scanline_angle);
	  
	  // the length of the arc we could drive along before
	  // intersecting this scan line
	  arc_drive_dist = arc_radius * ( M_PI - 2 * scanline_angle );

	  if ( arc_scan_intersection >= laserdata[i] ) {
	    // there's something between us and where the arc takes us
	    // modify the goodness appropriately...
	    
	    // just scale it (sort of) linearly for now...
	    // this was a crappy way to do it.
	    //iter->goodness = MAX_GOODNESS / 180 * (180 - i);
	    // this seems better.
	    iter->goodness = (int)(arc_drive_dist * MAX_GOODNESS / 
	                     (LADAR_MAX_RANGE * M_PI_2));
	    break;
	  }
	} //end looping thru right half scan lines
      }
      else {
	// left turn, so only use scan lines on the left side
	// note, arc_radius will be negative here.
	
	// set goodnes to max, and we will reduce it appropriately if
	// we can't drive the whole arc
	iter->goodness = MAX_GOODNESS;
	for ( int i = 181; i < 361; i++ ) {
	  // same as above basically, start at middle and work
	  // outwards with the scanlines.
	
	  // if scanline is max_range, just skip it.
	  if ( laserdata[i] >= LADAR_MAX_RANGE )
	    continue;
	  
	  scanline_angle = i * LADAR_ANGLE_RESOLUTION;
	  arc_scan_intersection = 2 * arc_radius * cos(scanline_angle); //negatives cancel out here
	  
	  // distance we could drive along the arc...
	  arc_drive_dist = arc_radius * ( M_PI - 2 * scanline_angle ); //negatives ok here too
	  if ( arc_scan_intersection >= laserdata[i] ) {
	    // there's something between us and where the arc takes us
	    // modify the goodness appropriately...

	    // just scale it (sort of) linearly for now...
	    //iter->goodness = MAX_GOODNESS/180 * (i - 180);
	    // as said above, this way makes more sense.
	    iter->goodness = (int)(arc_drive_dist * MAX_GOODNESS / 
	                     (LADAR_MAX_RANGE * M_PI_2));
	    break;
	  }
	} //end looping thru left half scan lines
      } //end the if arc_radius =/<=/>= checks
      //cout << iter->phi << ", " << arc_drive_dist << "  \n";
    }
    //cout << "\n";
    // I HAVE NO IDEA IF ANYTHING NEEDS TO BE LOCKED HERE...
    d.voterArray[d.LadarNum].updateVotes(wayPt, timestamp);
    //if( 1 ) { // cheat and print votes anyway
    if( PRINT_VOTES ) {
      // print the arbiter votes
      //cout << "--LadarLoop-- LADAR votes" << endl;
      //d.voterArray[d.LadarNum].printVotes();
      //cout << "--ladar.cc-- Sleeping so you can read my output." << endl;
      //sleep(2);
    }
    usleep_for(10000);    // Evaluation rate

  } // end while(d.shutdown_flag == FALSE)

  // copying from main.cc in Ladar (Lars)
  laser.LockNShutdown();  
    
} // end LadarLoop(DATUM &d)
