#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "cruisemain.hh"
#include "../../latlong/LatLong.h"

int InitGPS(DATUM &d) {
  int tmp;
  // init GPS and MAG
  cout << "Initializing GPS";
  // init_gps( comm port, rate [Hz])
  tmp = init_gps(4, 10);
  cout << "GPS Init Response = " << tmp << endl;

  d.gpsValid = FALSE;
  if( tmp == 1 ) {
    return TRUE; // success
  } else {
    return FALSE;
  }
}

void GrabGPSLoop(DATUM &d) {
  int i;
  char gps_buffer[1000];
  LatLong latlong(0, 0);

  while(d.shutdown_flag == FALSE) {

    // wait until PVT message is received
    cout << "Trying to get GPS" << endl;
    get_gps_pvt_msg(gps_buffer, 1000);
    cout << "Got GPS" <<endl;

    if (!valid_pvt_msg(gps_buffer)) {
      //      cout<<"WARNING: GPS data invalid"<<endl;
      cout << "GPS Invalid PVT" << endl;
      continue; // just try again
    }
  

    // lock the gps datum and update it
    boost::recursive_mutex::scoped_lock sl(d.gpsLock);
    d.gpsDW.update_gps_data(gps_buffer);
    d.gpsValid = TRUE;
    d.gpsNewData = TRUE;

    // cout<<"got GPS"<<endl;
    // and do it all again
    usleep(100000);
  }
}
