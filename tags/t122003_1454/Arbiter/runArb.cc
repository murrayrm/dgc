#include "Vote.h"
#include "Voter.h"
#include "Arbiter.h"
#include <string>
#include <time.h>
#include <iostream>
#include <fstream>
#include <sys/time.h>   // gettimeofday()
#include <cstdlib>
#include <algorithm>   // min

using namespace std;

static const int MAX_GOODNESS = 250;
static const double MAX_VELOCITY = 25.0;     // 25m/s ~= 50mph
// Sample parameters
static const double leftPhi = -1.3;
static const double rightPhi = 1.3;
static const int numArcs = 25;

pair<double, double> runArbiter(Voter::voteListType &inVotes, 
                                string testNumStr, string timeStr)
{
  pair<double, double> result;    // <velocity, steering angle>

  // Test logging variables
  ofstream commandLog( (string("test/ArbTest") + testNumStr + ".dat").c_str());
  ofstream arcsLog((string("test/Arcs") + testNumStr + ".dat").c_str());

  // Create an arbiter & a sample voter
  Arbiter testArb;
  Voter sample(numArcs, leftPhi, rightPhi);     // one voter for this test
  testArb.addVoter(&sample, 1);

  // Update arcs to be evaluated
  //testArb.updateNextArcs(leftPhi, rightPhi, numArcs);
  
  // Update voter votes _not_ using MTA
  sample.updateVotes(inVotes, (double)(atof(timeStr.c_str())));
  // Log arcs w/ timestamp
  arcsLog << timeStr << endl;                       // Timestamp
  Arbiter::voterListType logVoters = testArb.getVoterList(); 
  for(Arbiter::voterListType::iterator iter = logVoters.begin();
      iter != logVoters.end(); ++iter) {

    Voter::voteListType logVotes = iter->first->getVotes();
    for (Voter::voteListType::iterator viter = logVotes.begin();
	 viter != logVotes.end(); ++viter) {
      arcsLog << viter->phi << "\t"           // Steering Angle
	      << viter->velo << "\t"          // Velocity
	      << viter->goodness << endl;     // Goodness should be zero
    }
  }
  
  // Check if arcs have been updated right
  //cout << "--testMain: Arcs updated--" << endl;    
  
  // Calculate best arc & output
  testArb.getVotes();
  // check
  testArb.combineVotes();
  // check
  testArb.pickBestArc();
  // Log result w/ timestamp: Timestamp       Velocity        Steering_Angle
  commandLog << timeStr << "\t"                       // Timestamp
	     << testArb.getBestVelo() << "\t"           // Velocity
	     << testArb.getBestPhi() << endl;           // Steering Angle
  //cout << "--testMain--Best (Phi, Velo, goodness): (" 
  //     << testArb.getBestPhi() << ", "
  //     << testArb.getBestVelo() << ", " << testArb.getBestGoodness() << ")"
  //     << endl;
  // Result:
  result.first = testArb.getBestVelo();
  result.second = testArb.getBestPhi();

  commandLog.close();
  arcsLog.close();
    
  return result;
}


/*
int main() {

  timeval now, TimeZero;
  gettimeofday(&TimeZero, NULL);

  Voter::voteListType wayPt;            //
  pair<double, double> globalCommand;
  globalCommand.first = 10;
  globalCommand.second = .3;
  Vote wayPtVote(globalCommand.second, 250, globalCommand.first);  //
  wayPt.push_back(wayPtVote);           //
  gettimeofday(&now, NULL);             //
  pair<double, double> command = runArbiter(wayPt, "1", "12000");  

 cout << "--testing runArb--" << endl;
 cout << "velocity: " << command.first 
      << ", Steering: " << command.second << endl;

 return 0;
}
*/
