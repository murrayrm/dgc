/* Arbiter.cc
   Author: J.D. Salazar
   Created: 8-25-03
   Revision History: 11/10/2003 Sue Ann Hong  added updateNextArcs
   The Arbiter to be used by Team Caltech in the 2004 Darpa Grand Challenge */

#include "Arbiter.h"

#include <iostream>
using namespace std;


void Arbiter::addVoter(Voter *v, int weight)
{
  voterPairType p(v, weight);
  voterList_.push_back(p);
}


void Arbiter::updateNextArcs(double leftPhi, double rightPhi, int numArcs) {
  for(voterListType::iterator iter = voterList_.begin(); 
      iter != voterList_.end(); ++iter)
    {
      // Need a timestamp?
      iter->first->updateArcs(leftPhi, rightPhi, numArcs);
    }
}


void Arbiter::getVotes()
{
  // Need to add a feature so that a new vote list is taken only if it is
  // newer, or at least some kind of time checking.
  voteList_.clear();
  for(voterListType::iterator iter = voterList_.begin(); iter != voterList_.end();
      ++iter)
    {
      voteList_.push_back( iter->first->getVotes() );
    }
}

void Arbiter::printCombinedVotes()
{
  cout << "COMBINED VOTES" << endl;
  for(combinedVoteListType::iterator iter = combinedVoteList_.begin(); iter != combinedVoteList_.end();
      ++iter)
    {
      cout << "(Phi, Velo, Good) = (" << iter->phi << ", " << iter->velo << ", " << iter->goodness << ")" << endl;
    }
}


void Arbiter::combineVotes()
{
  combinedVoteList_.clear();

  // From the voter we can get the weight
  voterListType::iterator voter = voterList_.begin();
  double sumWeight = 0;
  double voterWeight;

  //cout << "======COMBINING VOTES======" << endl;
  for( voterVoteListType::iterator i = voteList_.begin(); i != voteList_.end();
	 ++i )
  {
    voterWeight = (double) voter->second;
    sumWeight += (double) voterWeight;
    //cout << "Voter Weight = " << voterWeight << endl;

    // If this is the first voter, create all the votes in combinedVoteList.
    if(i == voteList_.begin())
    {
      for( Voter::voteListType::iterator j = i->begin(); j != i->end(); ++j)
      {
        // I still have to update the goodness multiplying by the weight ...
	Vote auxVote;
        auxVote.goodness = (int) voterWeight*j->goodness;
        auxVote.phi = j->phi;
        auxVote.velo = j->velo;
	combinedVoteList_.push_back( auxVote );
      }
    }
    else {
      combinedVoteListType::iterator iter = combinedVoteList_.begin();
      for( Voter::voteListType::iterator j = i->begin(); j != i->end(); ++j )
      {
	// Scaling based on Voter weight needs to be added.
	// Also need to add features to make sure the same arcs are being
	//  compared.
	  iter->goodness += (int) voterWeight*j->goodness;
	  if(voterWeight){// the smallest speed, more safe
            iter->velo = min(iter->velo, j->velo);
          }
          else{ //descart the vote the weight is zero
            iter->velo = iter->velo;
          }
	  ++iter;
      }
    }
    voter++;
  }
  //cout << "Sum  Weight = " << sumWeight << endl;
  //Making the average of the goodness
  for( combinedVoteListType::iterator iter= combinedVoteList_.begin(); iter != combinedVoteList_.end(); ++iter ){
    iter->goodness /= (int) sumWeight;
  }
}


void Arbiter::pickBestArc()
{
  // This function would be better if it found the best subset of arcs above
  //  some certain threshold and picked the one in the middle.
  // Currently this algorithm is biased towards left turns.
  int maxVote = -9999;
  double bestArc = 0;
  double velocity = 0;                        

  //  cout << "--Arbiter::pickBestArc--" << endl;

  for( combinedVoteListType::iterator iter = combinedVoteList_.begin();
       iter != combinedVoteList_.end();
       ++iter )
    {
      if( maxVote < iter->goodness )
        {
          maxVote = iter->goodness;
          bestArc = iter->phi;
	  velocity = iter->velo;              
        }
    }

  actCommand_.velo = velocity;                
  actCommand_.phi = bestArc;
  actCommand_.goodness = maxVote;
}

//
// end Arbiter.cc
