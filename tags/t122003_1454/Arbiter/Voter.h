#ifndef __VOTER_H__
#define __VOTER_H__

/* Voter.h
   Author: J.D. Salazar
   Created: 8-25-03
   Revision History:  11/15/2003  Sue Ann Hong   
                      12/05/2003  Sue Ann Hong   Added copy constructor

   A class representing something that evaluates steering arcs for the Arbiter 
   to be used by Team Caltech in the 2004 Darpa Grand Challenge */

#include "Vote.h"
#include <list>

class Voter
{
 public:
  // Constructor
  Voter(unsigned int numberOfArcs = DEFAULT_NUMBER_OF_ARCS,
	//        unsigned int maxVote = DEFAULT_MAX_VOTE,
        double leftPhi = DEFAULT_LEFT_PHI,
        double rightPhi = DEFAULT_RIGHT_PHI)
    : numberOfArcs_(numberOfArcs)
    //    , maxVote_(maxVote)
    , leftPhi_(leftPhi)
    , rightPhi_(rightPhi)
    {  updateArcs(leftPhi_, rightPhi_, numberOfArcs_); }

  // Copy Constructor
  Voter(const Voter &other) { copy(other); }

  Voter &operator= (const Voter &other) {
    // Check for self assignment
    if ( this == &other ) return *this;

    //    destroy();  // Currently we've got nothing to destroy
    copy(other);
    return *this;
  }

  void copy(const Voter &other) {
    leftPhi_ = other.leftPhi_;
    rightPhi_ = other.rightPhi_;
    numberOfArcs_ = other.numberOfArcs_;
    // MAY NEED TO CHANGE IF UPDATEVOTES CHANGES N DOESN'T DESTROY votes_ FIRST
    voteListType copyVotes = other.votes_;
    updateVotes(copyVotes, other.getTime());
  }

  // Destroy oneself
  void destroy() {};

  // Destructor
  virtual ~Voter() {};

  // Votes are currently stored using the STL 'list'
  typedef std::list<Vote> voteListType;

  // Accessors
  int getNumberOfArcs() const { return numberOfArcs_; }
  double getLeftPhi() const { return leftPhi_; }
  double getRightPhi() const { return rightPhi_; }
  // int getMaxVote() const { return maxVote_; }
  double getTime() const { return time_; }
  /* getVotes()
   * Description: Returns this.votes_ -- because list is a std type, this
   *   should return a copy. Calling this function multiple times results in
   *   an intersting error. Should call this function and store votes_ values
   *   into a new voteListType, if wish to use the values in votes_.
   *   Should only be used for logging/debugging purposes. Otherwise it may be
   *   dangerous.
   */
  voteListType getVotes();
  void printVotes();

  // Mutators
  void setNumberOfArcs(int n) { numberOfArcs_ = std::max( n, 1 ); }
  void setLeftPhi(double n) { leftPhi_ = n; }
  void setRightPhi(double n) { rightPhi_ = n; }
  //  void setMaxVote(int n) { maxVote_ = n; }
  /* updateArcs()
   * Description: Updates leftPhi_, rightPhi_, numArcs_, and votes_
   *  vote_ gets filled with steering angle (phi) values calculated from
   *  rightPhi, leftPhi, numArcs. The previous vote_ is erased.
   */
  void updateArcs(double leftPhi, double rightPhi, int numArcs);

  // Default values
  static const int DEFAULT_NUMBER_OF_ARCS = 25;
  static const int DEFAULT_MAX_VOTE = 250;
  static const double DEFAULT_LEFT_PHI = -3.14159;
  static const double DEFAULT_RIGHT_PHI = 3.14159;

  /* updateVotes()
   * Description: Should be called after updateArcs and sending process msg to
   *  the module. Updates the goodness values in votes_. 
   * (as of) 12/20/2003: erases the existing votes_ and copies the passed 
   *                     in 'votes' into votes_.
   */
  void updateVotes(voteListType &votes, double timestamp);

protected:
  voteListType votes_;   // The list of the current votes on the steering arcs
  double time_;          // The timestamp of the last vote.
  int numberOfArcs_;     // The number of steering arcs to evaluate;
  //  int maxVote_;          // The maximum value that a vote can have
  // The minimum and maximum possible steering angles in radians.
  double leftPhi_, rightPhi_;
};

#endif
