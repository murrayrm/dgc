#include "Vote.h"
#include "Voter.h"
#include "Arbiter.h"
#include <string>
#include <time.h>
#include <iostream>
#include <fstream>
#include <sys/time.h>   // gettimeofday()
#include <cstdlib>
#include <algorithm>   // min

using namespace std;

static const int MAX_GOODNESS = 250;
static const double MAX_VELOCITY = 25.0;     // 25m/s ~= 50mph
// Sample parameters
static const double leftPhi = -1.3;
static const double rightPhi = 1.3;
static const int numArcs = 25;
int runArbiter(Voter::voteListType &inVotes, string testNumStr, 
               string timeStr);

int main(int argc, char **argv) {

  if (argc != 2) {
    cout << "Usage -- ./testMain <test number>" << endl;
    exit(1);
  }
 
  string testNumStr = argv[1];
  Voter::voteListType inputVotes;
  // timestamp: in seconds, since the beginning of this arbiter operation
  timeval time, begin;                    // timeval for timestamp
  double beginTimestamp;
  double timestamp = 0;
  beginTimestamp = begin.tv_sec + (begin.tv_usec/1000000.0);
  gettimeofday(&begin, NULL);

  // Generate the votes list
  inputVotes.clear();
  // Same code as Voter::updateArcs
  // Fill 'votes_' structure w/ angles and zero velo, zero goodness
  double increment;
  if( numArcs > 1 && rightPhi != leftPhi )
    increment = (rightPhi - leftPhi) / (numArcs - 1);
  else
    increment = 1;

  inputVotes.clear();
  double i;
  int j = 0;
  for( i = leftPhi; i <= rightPhi; i += increment )
  {
    // Create a vote to put into the list
    srand(j);
    Vote v((double)(j*.5), min(rand()/10000000, 250), (double)(j*2));
    inputVotes.push_back(v);
    //    cout << i << " " << v.velo << v.phi << v.goodness << endl;
    j++;
  }

  gettimeofday(&time, NULL);
  timestamp = beginTimestamp - (time.tv_sec + (time.tv_usec/1000000.0));
  int dec = 5;
  int sign = 0;
  // watch-out: fcvt not ANSI-C
  runArbiter(inputVotes, testNumStr, fcvt(timestamp, 7, &dec, &sign));

  return 0;
}

int runArbiter(Voter::voteListType &inVotes, string testNumStr, string timeStr)
{
  // Test logging variables
  ofstream commandLog( (string("test/ArbTest") + testNumStr + ".dat").c_str());
  ofstream arcsLog((string("test/Arcs") + testNumStr + ".dat").c_str());

  // Create an arbiter & a sample voter
  Arbiter testArb;
  Voter sample(numArcs, leftPhi, rightPhi);     // one voter for this test
  testArb.addVoter(&sample, 1);

  //  while (cycle) {
    // Update arcs to be evaluated
    //testArb.updateNextArcs(leftPhi, rightPhi, numArcs);

    // Update voter votes _not_ using MTA
    sample.updateVotes(inVotes, (double)(atof(timeStr.c_str())));
    // Log arcs w/ timestamp
    arcsLog << timeStr << endl;                       // Timestamp
    Arbiter::voterListType logVoters = testArb.getVoterList(); 
    for(Arbiter::voterListType::iterator iter = logVoters.begin();
	iter != logVoters.end(); ++iter) {

        Voter::voteListType logVotes = iter->first->getVotes();
        for (Voter::voteListType::iterator viter = logVotes.begin();
             viter != logVotes.end(); ++viter) {
 	  arcsLog << viter->phi << "\t"           // Steering Angle
	          << viter->velo << "\t"          // Velocity
                  << viter->goodness << endl;     // Goodness should be zero
	}
    }

    // Check if arcs have been updated right
      cout << "--testMain: Arcs updated--" << endl;    

    // Calculate best arc & output
    testArb.getVotes();
    // check
    testArb.combineVotes();
    // check
    testArb.pickBestArc();
    // Log result w/ timestamp: Timestamp       Velocity        Steering_Angle
    commandLog << timeStr << "\t"                       // Timestamp
               << testArb.getBestVelo() << "\t"           // Velocity
               << testArb.getBestPhi() << endl;           // Steering Angle
    cout << "--testMain--Best (Phi, Velo, goodness): (" 
         << testArb.getBestPhi() << ", "
         << testArb.getBestVelo() << ", " << testArb.getBestGoodness() << ")"
         << endl;
    //  }

    commandLog.close();
    arcsLog.close();

    return 0;
}
