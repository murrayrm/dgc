#ifndef __ARBITER_H__
#define __ARBITER_H__

/* Arbiter.h
   Author: J.D. Salazar
   Created: 8-25-03
   Revision History:  11/10/2003  Sue Ann Hong  Added updateNextArcs
                                                Added actCommand_ & accessors
   The Arbiter to be used by Team Caltech in the 2004 Darpa Grand Challenge */

#include "Voter.h"
#include <list>
#include <utility>  // For pair

class Arbiter
{
 public:
  // Constructor
  Arbiter() { actCommand_ = Vote(0, 0, 0); };

  // Destructor
  ~Arbiter() {};

  typedef std::pair<Voter *, int> voterPairType;
  typedef std::list< voterPairType > voterListType;
  typedef std::list< Voter::voteListType > voterVoteListType;
  typedef std::list<Vote> combinedVoteListType;

  // Accessors
  double getBestPhi() { return actCommand_.phi; }
  double getBestVelo() { return actCommand_.velo; }
  int getBestGoodness() { return actCommand_.goodness; }
  /* getVoterList()
   * Description: Returns this.voterList_ -- because list is a std type, this
   *   should return a copy. Calling this function multiple times results in
   *   an intersting error. Should call this function and store voterList_
   *   values into a new voterListType, if wish to use the values in 
   *   voterList_. Should only be used for logging/debugging purposes.
   *   Otherwise it may be dangerous.
   */
  voterListType getVoterList() { return voterList_; };

  /* addVoter()
   * Description: Add a voter to the arbiter.  'Voter's are structures which
   *  evaluate steering arcs and are derived from the class 'Voter'.  'weight'
   *  is an integer value representing how important this voter is; a higher
   *  weight means the Voter has more influence.
   */
  void addVoter(Voter *v, int weight);

  /* updateNextArcs()
   * Description: Updates the arcs to be evaluated for all voters
   *  by calling Voter::updateArcs() for each Voter in this.voterList_.
   */
  void updateNextArcs(double leftPhi, double rightPhi, int numArcs);

  /* getVotes()
   * Description: Update voteList_ with votes_ values from all Voters.
   */
  void getVotes();

  /* combineVotes()
   * Description: Combine the votes of each individual voter into one vote
   *  using the weights. The result is put into combinedVoteList_. This should
   *  only be called after 'getVotes' which makes sure all values in voteList_
   *  are up-to-date. 
   */
  void combineVotes();

  /*printCombinedVotes
   *Print the combined votes as phi, velo and goodness
   */
  void printCombinedVotes();

  /* pickBestArc()
   * Description: Updates actCommand_ by picking the best arc out of the
   *  combinedVoteList_. This should only be called after 'combineVotes' to
   *  make sure we pick the best arc with up-to-date info.
   */
  void pickBestArc();

 private:
  Vote actCommand_;

  // A list of Voters to be used in the arbiter.
  voterListType voterList_;

  // A list of vote lists ('voteListType's): it's a list of vote lists (votes_)
  //  from all voters. 
  voterVoteListType voteList_;

  // The list of votes containing the ultimate goodness value for each arc,
  //  obtained by combining goodness values in 'voteListType's in voteList_.
  combinedVoteListType combinedVoteList_;
};

#endif
