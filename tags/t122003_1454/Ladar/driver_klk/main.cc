/*********************************************************************************/
/* SICK LASER DRIVER V0.9                                                        */
/* Copyright (C) 2000 - Kasper Stoy - USC Robotics Labs (See README for details) */
/*********************************************************************************/
#include <unistd.h>
#include <stdio.h>

#include "laserdevice.h"

int main(int argc, char *argv[]) {
  unsigned char data[1024];
  int result;

  CLaserDevice laserDevice("/dev/ttyS9"); /* change this */

  result = laserDevice.Setup();

  if (result==0) { // success

    /* get ten laser scans */
    for(int cnt=0;cnt<5;cnt++) {
      laserDevice.LockNGetData( data );

      /* and print them */
      for(int i=1;i<723;i+=2) {
	printf("%hu ", (unsigned short int) data[i]
                  + ((unsigned short int) data[i+1] << 8));
      }
      puts("");
      usleep( 200000 );
    }
    
  }

  laserDevice.LockNShutdown();
  return(0);
}
