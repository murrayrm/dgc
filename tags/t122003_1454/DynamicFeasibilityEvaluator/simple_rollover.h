#ifndef __SIMPLE_ROLLOEVER_H__
#define __SIMPLE_ROLLOEVER_H__

void simple_rollover( const double H, const double L, const double R, const double m, const double a_tot, const double g_z, const double g_y,
                    double *c_roll, double *F_L_z, double *F_R_z);

#endif
