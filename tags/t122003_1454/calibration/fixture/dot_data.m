%dot_data.m
%this file defines the measured values for the dots of the crosshair-less
%targets, as measured by laura rogers around 14-Dec-2003.
%the format of the data is defined in fixture.m
%this file can be used as a template for later calibrations
L=[24752.39,    10057.03,   2.846
   25919.42,    9935.20,    3.186
   25928.23,    11107.26,   3.377
   24802.38,    11337.22,   3.057];

R=[26210.43,    10010.04,   3.180
   27132.53,    10205.55,   2.695
   27136.55,    11516.58,   2.926
   26215.26,    11138.20,   3.300];

D=[24912.28,    11627.23,   3.038
   26046.20,    11339.08,   3.358
   27003.15,    11746.24,   2.895
   25712.13,    12231.43,   2.516];   
