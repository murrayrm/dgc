%fixture.m
%This program generates fixture files for a corner cube target given total
%station measurements of the corner cube.

%a future version of the program will be compiled to C, so matlab will not be
%needed to run it.
%In a future version of the program, I/O will be file-based

%Input format:
%L,R, and D are the data of the left, right, and down faces, respectively
%each input variable is a 4x3 matrix
%the rows are the 4 points. 
%the points are, in order, top left, top right, bottom right, bottom left
%each column is, in order, theta, phi, rho, the default spherical coordinates,
%measured from the total station's frame of reference
%angles are in dms format.  for example, 156 degrees, 45 minutes, 11 seconds
%is 15645.11 in dms format.
%rho is given in meters

%The output is given in meters, in total-station centered coordinates
%the output format for each face is the matrix:
%               X       Y       Z
%upper left   
%upper right  
%lower left  
%lower right  


%OUTLINE: (! denotes unimplemented steps)
%    convert degrees to radians
%    convert phi (angle off of vertical) to elevation angle
%    convert all points to cartesian with sph2cart
%    do a best fit, if necesary.
%!   reproject theta and phi onto best fit plane
%    determine cube's origin
%    determine unit vectors in cube-centered coordinates
%    transform totalstation centered cartesian coordinates to cube-centered
%        cartesian coordinates.
%    output fixture data, with the order of the points changed appropriately.

Lrad=[dms2rad(L(:,1:2)),L(:,3)];
Lelev=[Lrad(:,1), pi/2 - Lrad(:,2), Lrad(:,3)];
[x,y,z]=sph2cart(Lelev(:,1),Lelev(:,2), Lelev(:,3));
Lcart=[x,y,z];
Lfix=[Lcart(1:2,:); Lcart(4,:); Lcart(3, :)];

Rrad=[dms2rad(R(:,1:2)),R(:,3)];
Relev=[Rrad(:,1), pi/2 - Rrad(:,2), Rrad(:,3)];
[x,y,z]=sph2cart(Relev(:,1),Relev(:,2), Relev(:,3));
Rcart=[x,y,z];
Rfix=[Rcart(1:2,:); Rcart(4,:); Rcart(3, :)];

Drad=[dms2rad(D(:,1:2)),D(:,3)];
Delev=[Drad(:,1), pi/2 - Drad(:,2), Drad(:,3)];
[x,y,z]=sph2cart(Delev(:,1),Delev(:,2), Delev(:,3));
Dcart=[x,y,z];
Dfix=[Dcart(1:2,:); Dcart(4,:); Dcart(3, :)];

%solve ax+by+cz = 1
one=[1;1;1;1];
Lcoeff=Lcart\one;
Rcoeff=Rcart\one;
Dcoeff=Dcart\one;
%Now we have best bit planes, so we can solve for intersections
C=[Lcoeff, Rcoeff, Dcoeff];
Xorigin=[1,1,1]/C;
%Now we take cross products of normal vectors to get basis of corner cube in
%total station's coordinates
zvec=cross(Lcoeff,Rcoeff);
yvec=cross(Rcoeff,Dcoeff);
xvec=cross(Dcoeff,Lcoeff);
%unitize the vectors
xunit=xvec/norm(xvec);
yunit=yvec/norm(yvec);
zunit=zvec/norm(zvec);
%form the matrix of basises
A=[xunit yunit zunit];
%do the transformation from totalstation origin to cube origin
Lorigin=Lcart-[1;1;1;1]*Xorigin;
Rorigin=Rcart-[1;1;1;1]*Xorigin;
Dorigin=Dcart-[1;1;1;1]*Xorigin;
%now do the rotation transformation
Lcube=A^-1*transpose(Lorigin);
Rcube=A^-1*transpose(Rorigin);
Dcube=A^-1*transpose(Dorigin);
%change the order
Ltrn=transpose(Lcube);
Rtrn=transpose(Rcube);
Dtrn=transpose(Dcube);
%Now switch point order to make fixture file
Lfix=[Ltrn(1:2,:); Ltrn(4,:); Ltrn(3, :)];
Rfix=[Rtrn(1:2,:); Rtrn(4,:); Rtrn(3, :)];
Dfix=[Dtrn(1:2,:); Dtrn(4,:); Dtrn(3, :)];

