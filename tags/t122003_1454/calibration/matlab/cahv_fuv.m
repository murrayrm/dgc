function [A,R,T]=cahv_fuv(K)

% usage: [A,R,T]=cahv_fuv(K)
% extracts Tsai + motion parameters from linear CAHV model

  c=K(1,:);a=K(2,:); h=K(3,:); v=K(4,:); 


if size(c,1)==1 
  c=c';
  a=a';
  h=h';
  v=v';
end;


M=[h v a]';

A2=M*M';

u=A2(1,3); 
v=A2(2,3);
fy=sqrt(A2(2,2)-v^2);
s=(A2(1,2)-u*v)/fy;
fx=sqrt(A2(1,1)-s^2-u^2);

A=[fx s u; 0 fy v; 0 0 1];

R=inv(A)*M;
[j1 j2 j3]=svd(R); R=j1*j3';

T=-R*c;


