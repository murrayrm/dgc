function [parms,oms,Ts,Woms,Wts]=readtsai(file)
  
% usage: [parms,oms,Ts,Woms,Wts]=readtsai(file)
% reads .tsai file
% returns parms = [fx fy u v k1 k2 p1 p2]
%           oms = n x 3 array of rotation vectors
%            Ts = n x 3 array of translation vectors
%          Woms = rotation weights
%           Wts = translation weights
  
  fid=fopen(file,'r');
  
  while(1)  % read past initialization
    [ln,sz]=readline(fid);
    if sz==-1 return; end;
    if sz>=3
      if ln(1:4)=='Intr';
	break
      end
    end
  end
  
  while(1)
    [ln,sz]=readline(fid);
    if sz==-1 return; end;
    if sz>=3
      pref=ln(1:2);
      dln=ln(5:sz);
      if pref=='fx';
	fx=str2num(dln);
      elseif pref=='fy';
	fy=str2num(dln);
      elseif pref=='u ';
	u=str2num(dln);
      elseif pref=='v ';
	v=str2num(dln);
      elseif pref=='k1';
	k1=str2num(dln);
      elseif pref=='k2';
	k2=str2num(dln);
      elseif pref=='p1';
	p1=str2num(dln);
      elseif pref=='p2';
	p2=str2num(dln);
	break;
      end
    end
  end

  parms=[fx fy u v k1 k2 p1 p2];
  
  oms=[];
  Ts=[];
  Woms=[];
  WTs=[];
  
  while(1)
    [ln,sz]=readline(fid);
    if sz==-1 break; end
    if sz>=5
      if ln(1:5)=='Frame'
	ln=readline(fid);
	tmp=sscanf(ln,'   (omx,omy,omz) = (%f,%f,%f)');
	oms=[oms;tmp'];
	ln=readline(fid);
	tmp=sscanf(ln,'   (Womx,Womy,Womz) = (%f,%f,%f)');
	Woms=[Woms;tmp'];
	ln=readline(fid);
	tmp=sscanf(ln,'   (Tx,Ty,Tz) = (%f,%f,%f)');
	Ts=[Ts;tmp'];
	ln=readline(fid);
	tmp=sscanf(ln,'   (WTx,WTy,WTz) = (%f,%f,%f)');
	WTs=[WTs;tmp'];
      end
    end
  end
      
  fclose(fid);