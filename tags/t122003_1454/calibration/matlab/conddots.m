function [Ao,Bo]=conddots(Ai,Bi);

% usage [Ao, Bo]=conddots(Ai,Bi);
% takes output of readdots.m applied to two dots files
% returns modified dot data so that only dots
% present in both files are included. Dots files are reordered
% so that lines match.
  
 Ao=[];
 Bo=[];
  
 count=1;
 for ai=1:size(Ai,1);
   for bi=1:size(Bi,1);
     if Ai(ai,6)==Bi(bi,6) & Ai(ai,7)==Bi(bi,7) & Ai(ai,8)==Bi(bi,8)
       Ao(count,:)=Ai(ai,:);
       Bo(count,:)=Bi(bi,:);
       count=count+1;
     end
   end
 end
  
  