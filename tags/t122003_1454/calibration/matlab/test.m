[prms,oms,Ts]=readtsai('left.tsai');
numtargs=size(oms,1);

targparmL=[];
for i=1:numtargs
  targparmL=[targparmL Ts(i,:) oms(i,:)];
end

[prms,oms,Ts]=readtsai('right.tsai');
targparmR=[];
for i=1:numtargs
  targparmR=[targparmR Ts(i,:) oms(i,:)];
end

KL=readcahvor('left0.cahvor');
KR0=readcahvor('right0.cahvor');

KR=relmot('left.dots','right.dots',KL,KR0,.2032,targparmL,targparmR)

writecahvor(KL,'left.cahvor',[640 480]);
writecahvor(KR,'right.cahvor',[640 480]);
