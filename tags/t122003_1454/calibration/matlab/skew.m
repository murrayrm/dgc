function J=skew(w)

% usage J=skew(w) 
% returns skew symmetric tensor associated with vector w 
  
J=[0,-w(3),w(2);w(3),0,-w(1);-w(2),w(1),0];
