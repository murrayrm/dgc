function C=readcahvor(file);

% usage: K=readcahvor(file)
% reads cahvor file as 6 x 3 matrix
% if the file is .cahv, O=A+epsilon, R=1e-6*ones(1,3)
  
  fid=fopen(file,'r');
  
  while(1)
    [ln,sz]=readline(fid);
    if sz==-1 break; end;
    if sz>=3
      dln=ln(4:sz);
      if ln(1:3)=='C =';
	C(1,:)=str2num(dln);
      elseif ln(1:3)=='A =';
	C(2,:)=str2num(dln);
      elseif ln(1:3)=='H =';
	C(3,:)=str2num(dln);     
      elseif ln(1:3)=='V =';
	C(4,:)=str2num(dln);
      elseif ln(1:3)=='O =';
	C(5,:)=str2num(dln);
      elseif ln(1:3)=='R =';
	C(6,:)=str2num(dln);
      end
    end
  end
  
  if size(C,1)==4
    C(5,:)=C(2,:);
    C(2,1)=C(2,1)+1e-6;
    C(6,:)=1e-6*ones(1,3);
  end
  
  fclose(fid);