function [ln,cnt]=readline (fid)

% usage: [ln,cnt]=readline(fid)
% reads single line from open ascii file
% returns line as string (ln) and line length (cnt)
  
ln=[' '];
q=0;
char =0;
cnt=0;
while q~=1,
 cnt=cnt+1;
 char=fread(fid,1,'schar');
   if min(size(char))==0
     cnt=-1;
     return;
   end
 ln(cnt)=setstr(char);
 if char==10
   q=1;
 end
end

