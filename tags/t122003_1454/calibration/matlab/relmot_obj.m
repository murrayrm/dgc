function res=relmot_obj(iparm,Dl,Dr,Kl,Kr,scl);
  
  numtarg=max(max(Dl(:,8)));
  parmsz=max(size(iparm));
  targparm=iparm(7:parmsz);
  
  res=[];
  
  Rcor=expm(skew(iparm(1:3)));
  Tcor=iparm(4:6)';
  
  Krnew=movecam(Kr,Rcor,Tcor);

  for targ=1:numtarg
    T=targparm(6*targ-5:6*targ-3)';
    om=targparm(6*targ-2:6*targ);
    R=expm(skew(om));
    rws=find(Dl(:,8)==targ);
    
    wpt=scl.*[Dl(rws,7) Dl(rws,6) 10*ones(max(size(rws)),1)];
    wpt=(R*wpt')';
    wpt(:,1)=wpt(:,1)+T(1);
    wpt(:,2)=wpt(:,2)+T(2);
    wpt(:,3)=wpt(:,3)+T(3);
        
    imptl=Dl(rws,4:5);
    imptr=Dr(rws,4:5);
    dotind=Dl(rws,6:7);
    
%    rayl=cahvor_2d_ray(imptl,Kl);
%    rayr=cahvor_2d_ray(imptr,Krnew);
%    wrecpts=t_ray_3d(rayl,rayr);
    
%    resw=(wrecpts-wpt);
%    resw=reshape(resw,1,prod(size(resw)));
    
    projl=cahvor_3d_2d(wpt,Kl);
    projr=cahvor_3d_2d(wpt,Krnew);
    
    resi=([imptl-projl; imptr-projr]);
    resi=reshape(resi,1,prod(size(resi)));

%    resw=norm(resi)./norm(resw).*resw;
    
   res=[res resi];
 %  res=[res resi];   
 %   res=[res resw];
 
  end
  
  

