function Kr=relmot(Dl,Dr,Kl,Kr,scl,targparml,targparmr)
  
  Dl=readdots(Dl);
  Dr=readdots(Dr);
  
  [Dl,Dr]=conddots(Dl,Dr);
  
  numtarg=max(Dl(:,8));
 
  motparm=getrelparm(targparml,targparmr);
  
  iparm=[motparm targparml];     
  
%  LB=-Inf.*ones(1,15);
%  UB=Inf.*ones(1,15);
  
  OP=optimset('Display','iter','MaxFunEvals',10000);

  numpts=max(size(Dl));
  dropnum=round(.01*numpts);
  
  iparm=lsqnonlin('relmot_obj',iparm,[],[],OP,Dl,Dr,Kl,Kr,scl);
  
  Rcor=expm(skew(iparm(1:3)));
  Tcor=iparm(4:6)';
  
  Kr=movecam(Kr,Rcor,Tcor);
