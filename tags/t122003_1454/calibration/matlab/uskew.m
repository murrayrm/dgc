function w=uskew(J)

% usage: w=uskew(J) 
% returns vec. associated with skew symmetric matrix  
  
w=[J(3,2); J(1,3); J(2,1)];
