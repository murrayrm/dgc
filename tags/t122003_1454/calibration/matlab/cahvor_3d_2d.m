function pt=cahvor_3d_2d(wpt,K);

% usage: pts=cahvor_3d_2d(wpts,K);
% adds non-linear distortion to cahv_3d_2d.
% see cahv_3d_2d for details

num=size(wpt,1);  
  
pt=zeros(num,2);

c=K(1,:); a=K(2,:); h=K(3,:); v=K(4,:); o=K(5,:); r=K(6,:);

os=[o(1)*ones(num,1) o(2)*ones(num,1) o(3)*ones(num,1)];
cs=[c(1)*ones(num,1) c(2)*ones(num,1) c(3)*ones(num,1)];

omega=dot(wpt-cs,os,2);
lambda=wpt-cs-[omega*o(1) omega*o(2) omega*o(3)];
tau=dot(lambda,lambda,2)./dot(omega,omega,2);
mu=r(1)*ones(num,1)+r(2)*tau+r(3)*dot(tau,tau,2);

dwpt=wpt+[mu.*lambda(:,1) mu.*lambda(:,2) mu.*lambda(:,3)];

pt=cahv_3d_2d(dwpt,K);
