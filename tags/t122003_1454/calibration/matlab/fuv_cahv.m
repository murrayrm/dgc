function K=fuv_cahv(A,R,T)

% usage K=fuv_cahv(A,R,T)
% generates CAHV model from Tsai + motion parameters
  
  M=[];
  
  P=200*randn(20,3); P(3,:)=abs(P(3,:));
  c=-R'*T; c=c';
  
  for i=1:20
    pr=A*(R*P(i,:)'+T); x=pr(1)/pr(3); y=pr(2)/pr(3);
    Q=P(i,:)-c; qx=Q(1); qy=Q(2); qz=Q(3);
    M=[M;
       -x*qx -x*qy -x*qz qx qy qz  0  0  0;
       -y*qx -y*qy -y*qz  0  0  0 qx qy qz];
  end
  
  [X Y Z]=svd(M);
  
  Z=Z(:,9);
  
  nrm=norm(Z(1:3));
  Z=Z./nrm; 
  if Z(3)<0 Z=-Z; end
  
  K=[c;Z(1) Z(2) Z(3); Z(4) Z(5) Z(6); Z(7) Z(8) Z(9)];
  
  