function writecahvor(K,fn,dim);

% usage writecahvor(K,fn,dim);
% writes fake cahv(or) file from matrix K
% fn = output filename
% dim = image dimensions as [columns rows]
  
  fid=fopen(fn, 'w');
  
  if size(K,1)>4
    fprintf(fid,'Model = CAHVOR = perspective, distortion\n\n');
  else
    fprintf(fid, 'Model = CAHV = perspective\n\n');
  end
  
  fprintf(fid,'Dimensions = %.0f %.0f\n\n',dim(1),dim(2));
  fprintf(fid,'C = %f %f %f\n', K(1,:));
  fprintf(fid,'A = %f %f %f\n', K(2,:));
  fprintf(fid,'H = %f %f %f\n', K(3,:));
  fprintf(fid,'V = %f %f %f\n', K(4,:));
  if size(K,1)>4
    fprintf(fid,'O = %f %f %f\n', K(5,:));
    fprintf(fid,'R = %f %f %f\n', K(6,:));
  end
  
  fprintf(fid,'\nS = \n')
  for i=1:18
    for j=1:18
      fprintf(fid,'%f ',0);
    end
    fprintf(fid,'\n');
  end
  
  fprintf(fid,'\n');
  
  [A,R,T]=cahv_fuv(K);
  
  fprintf(fid,'Hs    =   %f\n',A(1,1));
  fprintf(fid,'Hc    =   %f\n',A(1,3));
  fprintf(fid,'Vs    =   %f\n',A(2,2));
  fprintf(fid,'Vc    =   %f\n',A(2,3));
  tmp=acos(A(1,2)/A(2,2));
  fprintf(fid,'Theta =   %f (%f deg)\n',tmp,tmp*180/pi);
  fprintf(fid,'\n S internal =\n');
  for i=1:5
    for j=1:5
      fprintf(fid,'%f ',0);
    end
    fprintf(fid,'\n');
  end
  
  fclose(fid);