function pt=cahv_3d_2d(wpt,K);

% usage: pts=cahv_3d_2d(wpts,K);
% input consists of n x 3 matrix of 3d points and CAHV model
% output consists of n x 2 matrix of 2d points
  
num=size(wpt,1);  
  
pt=zeros(num,2);

c=K(1,:); a=K(2,:); h=K(3,:); v=K(4,:);

cs=[c(1)*ones(num,1) c(2)*ones(num,1) c(3)*ones(num,1)];
wpt=wpt-cs;

M=[h;v;a];

wpt=M*wpt';
wpt(1,:)=wpt(1,:)./wpt(3,:);
wpt(2,:)=wpt(2,:)./wpt(3,:);

pt=wpt(1:2,:)';
