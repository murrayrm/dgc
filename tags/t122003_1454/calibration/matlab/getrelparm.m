function motparm=getrelparm(targparml,targparmr)
  
 numtarg=prod(size(targparml))/6;
 
 Tacc=[0 0 0];
 omacc=[0 0 0];
 
 for targ=1:numtarg
   Tl=targparml(6*targ-5:6*targ-3);
   Tr=targparmr(6*targ-5:6*targ-3);
   oml=targparml(6*targ-2:6*targ);
   omr=targparmr(6*targ-2:6*targ);
   Rl=expm(skew(oml));
   Rr=expm(skew(omr));
   R=inv(Rr)*Rl;
   T=inv(Rr)*(Tl'-Tr'); T=T';
   omacc=omacc+uskew(logm(R))';
   Tacc=Tacc+T;
 end
 
 omacc=omacc./numtarg;
 Tacc=Tacc./numtarg;
 
 motparm=[omacc Tacc];