function Ko=movecam(Ki,R,T)

%applies rotation R and translation T to camera at Ki
  
  [A,Ro,To]=cahv_fuv(Ki);
  
  Rnew=R*Ro;
  Tnew=R*To+T;
  
  Ktmp=fuv_cahv(A,Rnew,Tnew);
  
  Onew=(R'*Ki(5,:)')';
  Ko=[Ktmp;Onew;Ki(6,:)];