function dots=readdots(file);

% usage D=readdots(file)
% read a 5d .dots file. The matrix D is 
% n x 8. Rows consist of:
% (3D coords, 2D image coords, row, column, target #)
  
  fid=fopen(file,'r');
  
  curtarg=0;
  count=1;
  while(1)
    [ln,sz]=readline(fid);
    if sz==-1; break; end;
    if ln(1)=='#' 
      curtarg=curtarg+1;
    else
      pos=find(ln=='#');
      a=str2num(ln(1:pos-1));
      b=str2num(ln(pos+1:sz));
      dots(count,:)=[a b curtarg];
      count=count+1;
    end
  end

  fclose(fid);
  
