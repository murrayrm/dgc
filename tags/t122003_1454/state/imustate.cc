/************************************************
imustate.cc
Created 12/16/03 by H Huang and Ike Gremmer
Receives IMU packets from IMU laptop over ethernet
Based on triv-listener.c by Dave Benson
*************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>		/* for sockaddr_in */
#include <netdb.h>		/* for gethostbyname() */
#include <unistd.h>		/* for read(), write() */
#include <assert.h>
#include <pthread.h>
#include <sys/time.h>		/* unix: for gettimeofday() */
#include "imu/net/crc32.h"
#include "imu/net/imu_net.h"
#include "../tests/waypointnav/waypointnav.hh"

#ifndef WIN32
#define SOCKET int
#define closesocket close
#endif

// port for talking to the IMU laptop
const unsigned portnum = 9000;

// network address setup for receiver
static int
setup_addr (struct sockaddr *addr,
	     unsigned port)
{
  struct sockaddr_in addr_in;
  memset (&addr_in, 0, sizeof (addr_in));
  addr_in.sin_family = AF_INET;
  addr_in.sin_port = htons (port);
  memcpy (addr, &addr_in, sizeof (struct sockaddr_in));
  return 1;
}

// InitIMU
// Initializes attitude and state parameters so that the IMU has initial values for integration
// Current initial pitch and roll is hard coded in
int InitIMU(DATUM &state_dat) {

  // clear gotimu flag
  state_dat.gotimu = 0;

  // initialize rotation matrix
  // note: assumes an already valid heading
  state_dat.N2B.setattitude(p0, r0, state_dat.SS.heading);

  // Initializing the status of imu data
  state_dat.stateValidFromIMU = FALSE;
 
  cout<<"IMU initialized"<<endl;
  
  return 0;
}

void GrabIMULoop(DATUM &state_dat) 
{
  struct sockaddr addr;
  SOCKET sock;
  IMUReading *imupacket;

  // body delta angle rates and attitude
  // used for easy access
  double dtx, dty, dtz, p, r, h;

  // set up receive address and port number 
  if (!setup_addr (&addr, portnum))
    {
      cout<<"Error setting up receive port and address"<<endl;
      return;
    }

  // set up socket and port 
  sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (bind (sock, &addr, sizeof (addr)) < 0)
    cout<<"Error binding to port "<<portnum<<endl;

  while(state_dat.shutdown_flag == FALSE)
    {
      // wait until IMU message is received
      char buf[256];
      int len = recv(sock, buf, sizeof(buf), 0);
      if (len == 4 + sizeof (IMUReading))
	{
	  unsigned char crc32[4];
	 imupacket = (IMUReading*)(buf + 4);
	  crc32_big_endian (buf + 4, len - 4, crc32);
	  if (memcmp (crc32, buf, 4) != 0)
	    fprintf (stderr, "CRC error (corrupted packet)\n");
	  else{
	    //cout<<"got imu packet"<<endl;
	    // got imu packet correctly
	    dtx= imupacket->dtx;
	    dty = imupacket->dty;
	    dtz = imupacket->dtz;
	    boost::recursive_mutex::scoped_lock sl(state_dat.imuLock);
	    state_dat.gotimu = 1;
	  }
	} else
	  {
	    //print error message and continue loop
	    cout<<"Error: Incorrect packet size"<<endl;
	    continue;
	  }
      if (state_dat.gotimu)
	{
	  // Process IMU data
	  // lock the imu datum and update it
	  boost::recursive_mutex::scoped_lock sl(state_dat.imuLock);	  
	
	  // propagate rotation matrix 
	  state_dat.N2B.propagate(dtx, dty, dtz);

	  // lock the state struct
	  boost::recursive_mutex::scoped_lock sl2(state_dat.stateLock);

	  // update new angles
	  state_dat.SS.pitch = state_dat.N2B.get_pitch();
	  state_dat.SS.roll = state_dat.N2B.get_roll();
	  state_dat.SS.heading = state_dat.N2B.get_heading();;
          state_dat.stateValidFromIMU = TRUE;
          state_dat.stateNewDataFromIMU = TRUE;
	  // clear gotimu flag
	  state_dat.gotimu = 0;
/*
	cout << "\nPitch: " << state_dat.SS.pitch
	     << ", Roll: " << state_dat.SS.roll
             << ", Heading: " << state_dat.SS.heading;
*/
	}
  }
  return;
}
