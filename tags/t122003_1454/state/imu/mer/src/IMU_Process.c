// IMU_Process.cpp : Converts raw IMU data to decimal values
// Created 12/5/03 Haomiao Huang

//#include <iostream>
//#include <iomanip>
#include <math.h>
#include "IMU_Process.h"

//using namespace std;

//int main()
//{
//	// Debug/test code for accel and gyro processing
//	short blah=0;
//	double acc;
//
//	while(blah !=-999)
//	{
//		cout<< "Enter Raw Accel value:";
//		cin>>blah;
//
//		acc = process_accel(blah);
//		cout<< "Delta-v is: " <<setprecision(5)<< acc <<endl;
//
//		cout<< "Enter Raw gyro value:";
//		cin>>blah;
//
//		acc = process_theta(blah);
//		cout<< "Delta-theta is: " <<setprecision(5)<< acc <<endl;
//	}
//
//	return 1;
//}



// process_theta
// Converts B-4.19 encoded dtheta raw data into decimal numbers
// B-4.19 - 
// binary encoding
// binary point is 4 digits to left of highest digit after sign
// digit, so 15th digit starts as 2^-4 power
// argument a is a 16-bit signed int
double process_theta(short t)
{
	int i;
	// Declare holder variables
	int digit;	   // placeholder for binary conversion
	int sign = 1;  // sign of the dtheta
	double val;	   // another placeholder for binary conversion
	double dtheta;  // value of dtheta
	int power;		// power of 2 used for binary to decimal conversion

	// Declare mask variable for picking out digits of a
	short t_mask = 0x4000; // 0b01000000

	// Convert to positive t to make conversion easier
	if (t < 0)
	{
		sign = -1;
		t = -t;
	}
	
	// initialize dtheta
	dtheta = 0;

	// get fractional component of t by looping through each digit
	// and multiplying by successive negative powers of 2

	for (i = 1; i <= 15 ; i++)
	{
		// pick out appropriate digit and multiply by appropriate neg power of 2
		digit = t & t_mask;
		digit = digit>>(15-i);  // make the digit a 1 or 0 from a binary
		power = i +4;
		val = digit / pow(2, power);
		dtheta += val;

		// shift mask to next bit
		t_mask = t_mask>>1;
	}

	// restore appropriate sign to dtheta
	dtheta = dtheta * sign;

	return dtheta;


}

// process_accel
// Converts B1.14 encoded Accel raw data into decimal numbers
// B1.14 -
// binary encoding
// 1st digit after sign digit is the >1 portion
// 14 digits after are the fractional portion
// binary point is between 15th and 14th digits
// argument a is a 16-bit signed int
double process_accel(short a)
{
	int i;
  // Declare holder variables
  int digit;   // placeholder for binary conversion
  int sign = 1;  // sign of the accel
  double accel;  // value of accel
  double val;	// another placeholder for conversion

  // Declare mask variable for picking out digits of a
  short a_mask = 0x4000; // 0b01000000

  // Convert to positive a to make conversion easier
  if (a < 0)
    {
      sign = -1;
      a = -a;
    }

  // get component of a that's greater than one and shift mask
  accel = a & a_mask;
  a_mask = a_mask>>1;

  // get fractional component of a by looping through each digit
  // and multiplying by successive negative powers of 2

  for (i = 1; i <= 14 ; i++)
    {
      // pick out appropriate digit and multiply by appropriate neg power of 2
      digit = a & a_mask;
      digit = digit>>(14-i);  // make the digit a 1 or 0 from a binary
      val = digit / pow(2, i);
      accel += val;

      // shift mask to next bit
      a_mask = a_mask>>1;
    }

  // restore appropriate sign to accel
  accel = accel * sign;

  return accel;
}