#ifndef __STATEMAIN_HH
#define __STATEMAIN_HH

#include <time.h>

#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "../gps/gps.h"
//#include "../latlong/LatLong.h"
//#include "../../magnetometer/magnetometer.h"
#include "../Msg/dgc_time.h"
#include "../Msg/dgc_const.h"
//#include "../OBDII/OBDII.h"
#include "imu/rot_matrix.hh"

#define DEFAULT_MAGNETOMETER_SERIAL_PORT	7
#define DEFAULT_GPS_SERIAL_PORT			6

#define STATE_POLL_DELAY	400000
#define CHECK_MESSAGE_DELAY     50000

/* If velocities are less than these values, consider them to be 0. */
#define VEL_N_VALID_CUTOFF 	0.1
#define VEL_E_VALID_CUTOFF 	0.1
#define VEL_U_VALID_CUTOFF 	0.1

/* When the speed of the car is higher than this, then don't use magnetometer */
#define MAGNET_VALID_CUTOFF	1  // in m/s

// initial heading, pitch, and roll
// car has a natural 3 degree down pitch
// assumes starting facing north, as soon as we start moving it'll figure out which way 
// we're actually pointed
const double p0 = -.052;
const double r0 = 0;
const double h0 = 0;


struct state_struct
{
   double timestamp;	// time stamp when GPS data is received
   double northing;	// UTM northing
   double easting;	// UTM easting
   float  altitude;	// guess it
   float  vel_n;	// north velocity
   float  vel_e;	// east velocity
   float  vel_u;	// up velocity
   float  heading;	// heading in degrees, straight from the magnetometer. north=0 degree, clockwise increase
   float  pitch;	// pitch in degrees
   float  roll; 	// roll in degrees
   float speed;         // magnitude of velocity in m/s
   float heading_n;     // north vector of heading
   float heading_e;     // east vector of heading
};


struct DATUM {
  
  int shutdown_flag;

  // Magnetometer stuff
  //  boost::recursive_mutex magLock;
  //int magValid; // flag to determine if mag is valid (new data)
  //int magNewData;
  //float magBuffer[7];
  //timeval magTS;


  // GPS Stuff 
  boost::recursive_mutex gpsLock;
  int gpsValid;
  int gpsNewData;
  gpsDataWrapper gpsDW;
  //  timeval gpsTS;

  // OBD-II Stuff
  //  boost::recursive_mutex obdLock;
  //  int obdValid;
  //  int obdNewData;
  //  float obdSpeed;
  //  float obdRPM;

  // IMU Stuff
  boost::recursive_mutex imuLock;
  int gotimu;
  rot_matrix N2B;

  // State Struct for exporting to outside world
  boost::recursive_mutex stateLock;
  state_struct SS;
  int stateNewData;
  int stateValid;
};

int  InitGPS(DATUM &d);
void GrabGPSLoop(DATUM &d);
void GPSsimstub(DATUM &state_dat);

//int  InitMag(DATUM &d); 
//void GrabMagLoop(DATUM &d);

//int  InitOBD(DATUM &d); 
//void GrabOBDLoop(DATUM &d);

int  InitIMU(DATUM &state_dat);
void GrabIMULoop(DATUM &state_dat);

#endif 
