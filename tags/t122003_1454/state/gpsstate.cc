#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "../tests/waypointnav/waypointnav.hh"
#include "../latlong/LatLong.h"

int InitGPS(DATUM &d) {
  int tmp;
  // init GPS and MAG
  cout << "Initializing GPS";
  // init_gps( comm port, rate [Hz])
  tmp = init_gps(4, 10);
  cout << "GPS Init Response = " << tmp << endl;

  // set initial attitude
  // temporary hack until mag gets going
  d.SS.pitch = p0;
  d.SS.roll = r0;
  d.SS.heading = h0;

  d.stateValidFromGPS = FALSE;
  d.gpsValid = FALSE;
  if( tmp == 1 ) {
    return TRUE; // success
  } else {
    return FALSE;
  }
}

void GrabGPSLoop(DATUM &d) {
  int i;
  char gps_buffer[1000];
  LatLong latlong(0, 0);

  while(d.shutdown_flag == FALSE) {

    // wait until PVT message is received
    //cout << "Trying to get GPS" << endl;
    get_gps_pvt_msg(gps_buffer, 1000);
    //cout << "Got GPS" <<endl;

    if (!valid_pvt_msg(gps_buffer)) {
      //      cout<<"WARNING: GPS data invalid"<<endl;
      //cout << "GPS Invalid PVT" << endl;
      continue; // just try again
    }
    // lock the gps datum and update it
    boost::recursive_mutex::scoped_lock sl(d.gpsLock);
    d.gpsDW.update_gps_data(gps_buffer);
    d.gpsValid = TRUE;
    d.gpsNewData = TRUE;

    // lock the state struct 
    boost::recursive_mutex::scoped_lock sl2(d.stateLock);
    //    filter state (latlong-> utm), etc;
    if(d.gpsValid) {// && d.magValid) {
      if(d.gpsNewData) {
	// lock gps and state
	boost::recursive_mutex::scoped_lock slGPS(d.gpsLock);
	boost::recursive_mutex::scoped_lock slSTATE(d.stateLock);
	latlong.set_latlon(d.gpsDW.data.lat, d.gpsDW.data.lng);
	
	latlong.get_UTM(&d.SS.easting, &d.SS.northing);  // convert to UTM
	d.SS.altitude = d.gpsDW.data.altitude;

	// update speeds only if we're sure they're not noise
	if (d.gpsDW.data.vel_n >= VEL_N_VALID_CUTOFF){
	d.SS.vel_n    = d.gpsDW.data.vel_n;
	}else{
	  d.SS.vel_n = 0;
	}
	if (d.gpsDW.data.vel_e >= VEL_E_VALID_CUTOFF){
	d.SS.vel_e    = d.gpsDW.data.vel_e;
	}else{
	  d.SS.vel_e = 0;
	}

	if (d.gpsDW.data.vel_u >= VEL_U_VALID_CUTOFF){
	d.SS.vel_u    = d.gpsDW.data.vel_u;
	}else{
	  d.SS.vel_u = 0;
	}
      }
	// get magnitude of velocity (in 2d only)
	d.SS.speed = sqrt(d.SS.vel_n * d.SS.vel_n + d.SS.vel_e * d.SS.vel_e);	

	d.stateValidFromGPS = TRUE;
	//FilterState(d.SS);
	d.stateNewDataFromGPS = TRUE; // set state flag
	d.gpsNewData = FALSE;  // reset GPS flag
	
	if (d.SS.speed >= MAGNET_VALID_CUTOFF){ 
	  // only update heading if speed is high enough for GPS to be accurate
	// Calculate heading in radians from velocities
	d.SS.heading = atan2(d.SS.vel_n, d.SS.vel_e);
       
	//reset IMU heading
	boost::recursive_mutex::scoped_lock slIMU(d.imuLock);
	d.N2B.setheading(d.SS.heading);
	}
      }
  }

    // and do it all again
}

// Test function for testing state estimator structure
void GPSsimstub(DATUM &state_dat)
{
  int count = 0;
  while(state_dat.shutdown_flag == FALSE)
    {
      boost::recursive_mutex::scoped_lock sl2(state_dat.stateLock);
      // display once a second
      if (state_dat.gotimu)
	  cout<<"pitch: "<<state_dat.SS.pitch * 180 / 3.14<<" roll:"<<state_dat.SS.roll * 180/3.14<<" heading: "<<state_dat.SS.heading*180/3.14<<endl;
    }
  return;
}
 
