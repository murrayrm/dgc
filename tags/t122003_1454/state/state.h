#ifndef STATE_H
#define STATE_H

#include "Global/Vector.hh"


#define DEFAULT_MAGNETOMETER_SERIAL_PORT	7
#define DEFAULT_GPS_SERIAL_PORT			6

#define STATE_POLL_DELAY	400000
#define CHECK_MESSAGE_DELAY     50000

/* If velocities are less than these values, consider them to be 0. */
#define VEL_N_VALID_CUTOFF 	0.1
#define VEL_E_VALID_CUTOFF 	0.1
#define VEL_U_VALID_CUTOFF 	0.1

/* When the speed of the car is higher than this, then don't use magnetometer */
#define MAGNET_VALID_CUTOFF	2  // in m/s

struct state_struct
{
  double timestamp;	// time stamp when GPS data is received
  double northing;	// UTM northing
  double easting;	// UTM easting
  float  altitude;	// guess it
  float  vel_n;	// north velocity
  float  vel_e;	// east velocity
  float  vel_u;	// up velocity
  float  heading;	// heading in degrees, straight from the magnetometer. north=0 degree, clockwise increase
  float  pitch;	   // pitch in degrees
  float  roll; 	   // roll in degrees
  float speed;     // magnitude of velocity in m/s
  float heading_n; // north vector of heading
  float heading_e; // east vector of heading
  double phi;      // steering angle, radians(?)
};

// state accessor functions 
state_struct get_state();

//Vector get_heading();
//Vector get_position();
//float get_speed();
//void GrabStateLoop();
#endif
