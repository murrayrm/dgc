//
// DARPA Grand Challenge - Team Caltech
//
// DisplayFcns.cc
//
// The methods of "DisplayFcns" are meant to provide an ncurses interface for 
// visualizing vehicle information through standard terminal output in 
// real-time
// 
// Author: Lars Cremean
//
// Created: 12/10/2003
//

#include "DisplayFcns.h"

WINDOW * gps_win;
WINDOW * arb_win;
WINDOW * gen_win;

// return a pointer to the generic window
WINDOW * get_gen_window( void ) {
  return gen_win;
}

// return a pointer to the gps window
WINDOW * get_gps_window( void ) {
  return gps_win;
}

// return a pointer to the arb window
WINDOW * get_arb_window( void ) {
  return arb_win;
}

// initialize all of the windows 
void init_display( void ) {

  initscr();            // Start curses mode
  cbreak();             // disable line buffering
  noecho();             // turn off echo to terminal
  keypad(stdscr, TRUE); // enable the use of function keys

  WIN gps;
  WIN arb;
  WIN gen;

  gps.h = 15;
  gps.w = 50;
  gps.r = 0;
  gps.c = 50;
  gps_win = create_newwin(gps.h, gps.w, gps.r, gps.c, "gps");

  arb.h = gps.h;
  arb.w = gps.w;
  arb.r = gps.h;
  arb.c = gps.c;
  arb_win = create_newwin(arb.h, arb.w, arb.r, arb.c, "arbiter");

  gen.h = gps.h + arb.h;
  gen.w = 50;
  gen.r = 0;
  gen.c = 0;
  gen_win = create_newwin(gen.h, gen.w, gen.r, gen.c, "generic");

  // somehow, redirect stdout to a curses window
  //system("");

  // set the scrolling region for the generic window
  wsetscrreg( gen_win, gen.r, gen.h );
  scrollok( gen_win, true );

  // we don't want to have to call wrefresh for every print
  immedok( gen_win, true );

} // end init_display()


// shut the display down properly 
void shutdown_display( void ) {

  destroy_win(gps_win);
  destroy_win(arb_win);
  destroy_win(gen_win);
  endwin();

}

// create_newwin(...) will make a border around a new window and
// return that window.  It will put a title bar at the top of
// the window with the specified string
WINDOW * create_newwin( int h, int w, int r, int c, char * str ) 
{
  WINDOW *local_win;
  WINDOW *interior_win;

  local_win = newwin(h, w, r, c);
  box(local_win, 0 , 0);        
                    // 0, 0 gives default characters 
                    // for the vertical and horizontal
                    // lines           
  wrefresh(local_win);    // Show that box 

  // print a title
  wattrset(  local_win, A_REVERSE );
  mvwprintw( local_win, 0, 1, str );
  wattrset(  local_win, A_NORMAL );
  wrefresh( local_win );

  // leave the border that we created, and just return the interior
  // of the window for writing
  delwin(local_win);
  interior_win = newwin(h-2,w-2,r+1,c+1);
  return interior_win;
}

// completely remove a window from the screen and from memory
void destroy_win(WINDOW *local_win)
{
        // box(local_win, ' ', ' '); : This won't produce the desired
        // result of erasing the window. It will leave it's four corners
        // and so an ugly remnant of window.
        //
        wborder(local_win, ' ', ' ', ' ',' ',' ',' ',' ',' ');
        /* The parameters taken are
         * 1. win: the window on which to operate
         * 2. ls: character to be used for the left side of the window
         * 3. rs: character to be used for the right side of the window
         * 4. ts: character to be used for the top side of the window
         * 5. bs: character to be used for the bottom side of the window
         * 6. tl: character to be used for the top left corner
         * 7. tr: character to be used for the top right corner
         * 8. bl: character to be used for the bottom left corner
         * 9. br: character to be used for the bottom right corner
         */
        wrefresh(local_win);
        delwin(local_win);
}
