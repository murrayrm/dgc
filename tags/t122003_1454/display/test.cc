//
//
// DARPA Grand Challenge - Team Caltech
//
// test.cc
//
// The methods of "DisplayFcns" are meant to provide an ncurses interface for
// visualizing vehicle information through standard terminal output in
// real-time
//
// This is a unit test function for the display methods
// 
// Author: Lars Cremean
//
// Created: 12/10/2003 
//

#include "DisplayFcns.h" 

int main( void ) {

  // the display thread defines the types and 
  // locations of display windows, along with
  // their sizes and locations

  // only the main program needs to init_display()
  init_display();

  // each program that wants to write to a 
  // display will have to get its window
  // from the display thread.  this must happen
  // after init_display
  WINDOW * mine;
  mine = get_gps_window();

  // then you can print whatever you want to the 
  // window at whatever location.  there are special
  // functions for doing so...
  
  // mvwprintw( win, r, c, fmt ) prints the formatted string fmt
  // to the window win at a specified row and column location
  mvwprintw( mine, 2, 4, "The beginning of this sentence is at r=2, c=4." );
  
  // wprintw will print a string to the specified 
  // window at whatever the current cursor location is
  // for that window
  wprintw(mine, "_current_cursor_location");
 
  // wrefresh needs to be called for prints to be 
  // displayed (kind of like fflush)
  wrefresh(mine);

  // you don't want to pause, we just do it here so 
  // that you can verify that the test program is working
  pause();

  // only the main program needs to shutdown_display()
  shutdown_display();

  return 1;
}
