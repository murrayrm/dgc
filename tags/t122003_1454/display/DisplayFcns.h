//
// DARPA Grand Challenge - Team Caltech
//
// display.h
//
// The methods of "display" provide an ncurses interface for 
// visualizing vehicle information through standard terminal output
// 
// Author: Lars Cremean
//
// Created: 12/10/2003
//

#ifndef _DISPLAY__H
#define _DISPLAY__H

#include <curses.h> // For initscr(...), endwin(...), mvprintw(...), refresh(...),...
#include <unistd.h> // For usleep()
#include <stdio.h> // for printf(), ...
#include <iostream.h> // for cout, ...
#include <fstream> // for , ...

/*
#define GPS_WINDOW 0

#define MSG_GPS_LAT 0
#define MSG_GPS_LON 1
#define MSG_GPS_EASTING 2
#define MSG_GPS_NORTHING 3
*/

typedef struct {
  int h, w;
  int r, c;
} WIN;

void init_display( void );
void shutdown_display( void );

WINDOW * create_newwin(int, int, int, int, char * );
void destroy_win( WINDOW * );
WINDOW * get_gps_window( void );
WINDOW * get_arb_window( void );
WINDOW * get_gen_window( void );

#endif // ifdef _DISPLAY__H
