//
// DARPA Grand Challenge - Team Caltech
//
// test3.cc
//
// The methods of "DisplayFcns" are meant to provide an ncurses interface for
// visualizing vehicle information through standard terminal output in
// real-time
//
// This test file provides some copy-and-paste lines of code to ease 
// transition to the curses display environment
// 
// Author: Lars Cremean
//
// Created: 12/14/2003 
//

#include "DisplayFcns.h" 

int main( void ) {

  // the display thread defines the types and 
  // locations of display windows, along with
  // their sizes and locations

  // only the main program needs to init_display()
  init_display();

  // each program that wants to write to a 
  // display will have to get its window
  // from the display thread.  this must happen
  // after init_display
  WINDOW * gpsw;
  gpsw = get_gps_window();
  
  WINDOW * arbw;
  arbw = get_arb_window();

  WINDOW * genw;
  genw = get_gen_window();

  // then you can print whatever you want to the 
  // window at whatever location.  there are special
  // functions for doing so...
  
  // temp variable
  double var = 0;

  // mvwprintw( win, r, c, fmt ) prints the formatted string fmt
  // to the window win at a specified row and column location
  mvwprintw( gpsw, 1, 1, "lat [deg] = %5.2f", var );
  mvwprintw( gpsw, 2, 1, "lon [deg] = %5.2f", var );
  mvwprintw( gpsw, 4, 1, "northing [m] = %10.3f", var );
  mvwprintw( gpsw, 5, 1, "easting  [m] = %10.3f", var );
  mvwprintw( gpsw, 7, 1, "northing  [m] = %10.3f", var );
  mvwprintw( gpsw, 8, 1, "easting  [m] = %10.3f", var );
  // wrefresh needs to be called for prints to be 
  // displayed (kind of like fflush)
  wrefresh(gpsw);

  mvwprintw( arbw, 2, 4, "The bearbning of this sentence is at r=2, c=4." );
  wrefresh(arbw);
 
  printf("These are printfs\n");  
  printf("These are printfs\n");  
  std::cout << "here's a cout" << endl;

  for( int i=0; i < 40; i++ ) { 

    // printf and cout will produce undesired effects on stdscr!!!!!!!!!
    //printf("\nmwah%d.",i); // printf's will appear in unpredictable locations!
    //cout << "!" << endl; // so will these

    // instead, print to the generic window, genw
    wprintw( genw, "\nmwah%d!",i );

    wrefresh(genw);
    usleep(10000);
  }

//  printf("\nha haw!");
//  wrefresh(gpsw);

  // you don't want to pause, we just do it here so 
  // that you can verify that the test program is working
  pause();

  // only the main program needs to shutdown_display()
  shutdown_display();

  return 1;
}
