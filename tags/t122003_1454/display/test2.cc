//
// DARPA Grand Challenge - Team Caltech
//
// test2.cc
//
// The methods of "DisplayFcns" are meant to provide an ncurses interface for
// visualizing vehicle information through standard terminal output in
// real-time
//
// This is a unit test function for the DisplayFcns methods, also used to 
// test added functionality
// 
// Author: Lars Cremean
//
// Created: 12/12/2003 
//

#include "DisplayFcns.h" 

int main( void ) {

  // the display thread defines the types and 
  // locations of display windows, along with
  // their sizes and locations

  // only the main program needs to init_display()
  init_display();

  // each program that wants to write to a 
  // display will have to get its window
  // from the display thread.  this must happen
  // after init_display
  WINDOW * mine;
  mine = get_gps_window();
  
  WINDOW * arbw;
  arbw = get_arb_window();

  WINDOW * genw;
  genw = get_gen_window();

  // then you can print whatever you want to the 
  // window at whatever location.  there are special
  // functions for doing so...
  
  // mvwprintw( win, r, c, fmt ) prints the formatted string fmt
  // to the window win at a specified row and column location
  mvwprintw( mine, 2, 4, "The beginning of this sentence is at r=2, c=4." );
  mvwprintw( arbw, 2, 4, "The bearbning of this sentence is at r=2, c=4." );
  wrefresh(arbw);
 
  // wprintw will print a string to the specified 
  // window at whatever the current cursor location is
  // for that window
  wprintw(mine, "_current_cursor_location");

  printf("These are printfs\n");  
  printf("These are printfs\n");  
  std::cout << "here's a cout" << endl;

  for( int i=0; i < 40; i++ ) { 

    // printf and cout will produce undesired effects on stdscr!!!!!!!!!
    //printf("\nmwah%d.",i); // printf's will appear in unpredictable locations!
    //cout << "!" << endl; // so will these

    // instead, print to the generic window, genw
    wprintw( genw, "\nmwah%d!",i );

    wrefresh(genw);
    usleep(10000);
  }

  mvwprintw( mine, 6, 4, "The beginning of this sentence is at r=6, c=4." );

  // don't know why this doesn't work
  //std::cout << "here's a line" << endl;
 
  // wrefresh needs to be called for prints to be 
  // displayed (kind of like fflush)
  wrefresh(mine);

//  printf("\nha haw!");
//  wrefresh(mine);

  // you don't want to pause, we just do it here so 
  // that you can verify that the test program is working
  pause();

  // only the main program needs to shutdown_display()
  shutdown_display();

  return 1;
}
