// DGC_MODULE.ct
// -----------------------------------------

#ifndef DGC_MODULE_CT
#define DGC_MODULE_CT

#include "DGC_MODULE.hh"
#include "Msg/dgc_messages.h"
#include "Msg/dgc_const.h"
#include "Msg/dgc_socket.h"
#include "Msg/dgc_msg.h"

#include <iostream>


using namespace std;

void PrintCommandLineArgumentsString(DGC_MODULE & m) {
  cout << "DGC_MODULE:" << STR_MOD_NAME << endl << endl;
  cout << "This program requires command line arguments as described below." << endl 
       << "----------------------------------------------------------------" << endl;
  cout << "Runlevel: Please use the runlevel flag (-rl) followed by either "
       << "\"debug\" or \"race\"." << endl << "---" << endl;
  
  //   if( m.numSP() > 0) {
  cout << "Serial Port Numbers: This program requires " << m.numSP() 
       << " serial port(s)." << endl 
       << "  Please specify these by using the serial port flag (-sp) " << endl
       << "  followed by the required number of port values (in order)."
       << endl << "---" << endl;
  //   }
  //   if( m.numPP() > 0) {
  cout << "Parallel Port Numbers: This program requires " << m.numPP() 
       << " parallel port(s)." << endl 
       << "  Please specify these by using the parallel port flag (-pp) " << endl
       << "  followed by the required number of port values (in order)."
       << endl 
       << "----------------------------------------------------------------" << endl;
  //   }
  cout << "NOTE: You do not need to specify -sp or -pp if this program does" << endl
       << "      not require serial or parallel ports." << endl
       << "----------------------------------------------------------------" << endl;
  
  cout << "EXAMPLE: An example argument set for using parallel ports 0 and 2" << endl
       << "         and no serial ports in debug mode is:" << endl
       << "         ./" << STR_EXEC_NAME << " -rl debug -pp 0 2" << endl;
  cout << endl;
}

int strToUInt(char * str) {
  char *err;
  int ret = strtol(str, &err, 10);
  if ( *err != '\0' ) {
    return ERROR;
  }else {
    return ret;
  }

}

int uniqueElems(vector<int> & v) {
  int sz = v.size();
  int numUnique = 0;

  int i,j, newelem;
  for(i=0;i<sz;i++) {
    newelem = TRUE;
    for(j=0; j<i;j++) {
      if(v[j] == v[i]) {
	newelem = FALSE;
	break;
      }
    }
    if (newelem == TRUE) {
      numUnique++;
    }
  }
  return numUnique; 
}

/* ****************************************************************** *
 * ****************************************************************** *
 * ****************************************************************** */
DGC_MODULE::DGC_MODULE() {

  myModuleName = STR_MOD_NAME;
  numSerialPorts = NUM_SERIAL_PORTS;
  numParallelPorts = NUM_PARALLEL_PORTS;
  myModuleNumber = MODULE_NUMBER;
  myRunLevel = -1;
  //  dgc_msg_Register
}

DGC_MODULE::~DGC_MODULE() {
  //  dgc_msg_Unregister();
}

int DGC_MODULE::Args(int argc, char* argv[]) {

  if( argc <= 1 ) {
    PrintCommandLineArgumentsString(*this);
    return ERROR;
  } else {
    enum ARG_STATES {
      ST_NONE,
      ST_RL,
      ST_SP,
      ST_PP
    };

    string rl_flag = "-rl";
    string sp_flag = "-sp";
    string pp_flag = "-pp";

    string str_race = "race";
    string str_debug = "debug";

    ARG_STATES myState = ST_NONE;

    // simple state machine
    int i=1;
    int f_StopParsing = FALSE;
    while(i<argc && f_StopParsing == FALSE) {
      // first check if we need to change to a new state
      if( rl_flag.compare( argv[i] ) == 0 ) {
	myState = ST_RL;
	//	cout << "RL state" << endl;
      } else if ( sp_flag.compare( argv[i] ) == 0) {
	myState = ST_SP;
	//	cout << "SP state" << endl;
      } else if ( pp_flag.compare( argv[i] ) == 0) {
	myState = ST_PP;
	//	cout << "PP state" << endl;
      } else {
	//	cout << "NOt a state value (" << argv[i] << ")" << endl;
	if( myState == ST_RL ) {
	  // we are expecting "debug" or "race"
	  if ( str_debug.compare( argv[i]) == 0 && myRunLevel < 0) {
	    myRunLevel = RL_DEBUG;
	  } else if ( str_race.compare( argv[i]) == 0 && myRunLevel < 0) {
	    myRunLevel = RL_RACE;
	  } else {
	    cout << "ST_RL: Error reading command line arguments" << endl;
	    f_StopParsing = TRUE;
	  }
	} else {
	  // else we are looking for a number
	  int r = strToUInt( argv[i] );
	  if ( r < 0 ) {
	    cout << "Num: Error reading command line arguments" << endl;
	    f_StopParsing = TRUE;
	  } else {
	    switch( myState ) {  
	    case ST_SP:
	      mySerialPorts.push_back(r);
	      break;
	    case ST_PP:
	      myParallelPorts.push_back(r);
	      break;
	    default:
	      cout << "Port: Error reading command line arguments" << endl;
	      f_StopParsing = TRUE;
	    }
	  }
	}
      }
      i++;
    }

    // now that we have parsed the input, 
    // check to see if the data is valid.
    if ( f_StopParsing == TRUE ) {
      cout << "Exiting" << endl;
      PrintCommandLineArgumentsString(*this);      
      return ERROR;
    }else if ( myRunLevel < 0 ) {
      cout << "Error: Run Level not properly specified" << endl;
      PrintCommandLineArgumentsString(*this);
      return ERROR;
    }else if ( numSP() != uniqueElems(mySerialPorts)) {
      cout << "Error: Number of serial ports not properly specified." << endl;
      PrintCommandLineArgumentsString(*this);
      return ERROR;
    }else if ( numPP() != uniqueElems(myParallelPorts)) {
      cout << "Error: Number of parallel ports not properly specified." << endl;
      PrintCommandLineArgumentsString(*this);
      return ERROR;
    }          

    return TRUE;
  }
}

string DGC_MODULE::ModuleName() {
  return myModuleName;
}
int DGC_MODULE::ModuleNumber() {
  return myModuleNumber;
}
int DGC_MODULE::RunLevel() {
  return myRunLevel;
}

int DGC_MODULE::SP(int pn) {
  if (pn >= numSerialPorts) {
    return ERROR;
  } else {
    return mySerialPorts[pn];
  }
} 

int DGC_MODULE::PP(int pn) {
  if (pn >= numParallelPorts) {
    return ERROR;
  } else {
    return myParallelPorts[pn];
  }
}

int DGC_MODULE::numSP() {
  return numSerialPorts;
} 

int DGC_MODULE::numPP() {
  return numParallelPorts;
} 



DGC_MODULE MODULE_NAME;

int main (int argc, char * argv[]) {

  if( MODULE_NAME.Args(argc, argv)==TRUE ) {
    if( MODULE_NAME.Run() == TRUE) {
      return 0;
    } else {
      cout << "Exiting: Failure" << endl;
    }
  } else {
    cout << "Exiting: Failure" << endl;
  }

  return ERROR;
}

#endif
