#include "Log.hh"
#include <iostream>
#include <stdio.h>


using namespace std;

int main () {

  int x=6;
  float y = 3.14159265;

  cout << "testing" << endl;

  MSG::Log (MSG::LOG_DEBUG) << "This is a sample message "
	   << "which has parameters x=" 
	   << x << ", and y=" << y << ".";


  cout << endl << endl;
}
