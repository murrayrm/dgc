#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <list>

#include "Msg/dgc_const.h"
#include "WaypointFollowing.hh"
#include "Vector.hh"
#include "state/state.h"
#include "magnetometer/magnetometer.h"
#include "moveCar/brake.h"
#include "moveCar/TLL.h"
#include "moveCar/accel.h"
#include "gps/gps.h"
#include "latlong/LatLong.h"
#include "steerCar/steerCar.h"
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include "Msg/dgc_time.h"


using namespace std;

// declare and instance of waypoint following
WaypointFollowing WF;

int shutdown_flag;
const float waypointRadius = 10.0;// 5 meters around waypoint

const float MAX_STEER = .5;             // maximum steering angle in radians
const float DESIRED_VELOCITY = 5 * 1600 / 3600;    // desired velocity in m/s
const float VELO_RANGE = 5 * 1600 / 3600;
const float ACCEL_TOOMUCH = .1;
const float TOO_FAST = 2 * 1600 / 3600;  

const float MIN_ACCEL_GAS   =   0.00;
const float MAX_ACCEL_GAS   =   0.10;
const float MIN_ACCEL_BRAKE =  -0.40;
const float MAX_ACCEL_BRAKE =  -0.90;

const float SCALE_ACCEL_GAS   =  1.0; // scales m/s to accel 
const float SCALE_ACCEL_BRAKE =  1.0; 


int negCountLimit        =    -60000;
int posCountLimit        =     60000;
int verbosity_exec_cmd   =      TRUE;
int steer_errno          =  ERR_NONE;
float brakeVelo          =       0.7;

WaypointFollowing::WaypointFollowing() {
  thrdCounter = 0;
  gettimeofday(&timeZero, NULL);
  stateFile.open("state.dat");
  driveFile.open("drive.dat");
  stateFile << setiosflags(ios::fixed) << setprecision(5);
  driveFile << setiosflags(ios::fixed) << setprecision(5);
}

WaypointFollowing::~WaypointFollowing() {

}

int WaypointFollowing::NextThreadNumber() {
  boost::recursive_mutex::scoped_lock scoped_lock(threadMutex);
  return thrdCounter++;
}

void Starter() {
  // determine which thread we are 
  
  int thrd = WF.NextThreadNumber();
  if( thrd == 0) {
    // Start GrabStateLoop
    cout << "Starting GrabStateLoop" << endl;
    WF.GrabStateLoop();
  } else if ( thrd == 1 ) {
    cout << "Starting DriveWaypoints" << endl;
    WF.DriveWaypoints();
  } else {
    cout << "Something is wrong starting threads - thread index is wrong" << endl;
  }
}
      
// return value: steering angle in -1(left) to 1(right)
float WaypointFollowing::f_Steering(Vector curPos, Vector curWay, Vector curHead) {
  // find the angle between curWay and curHead, set it as the new steering
  float steer = - get_deflection(curPos, curWay, curHead);
  steer = steer * fabs(steer);
  if (fabs(steer) > MAX_STEER) {
    if (steer > 0)
      return 1.0;
    else // steer < 0
      return -1.0;
  }
  else {
    return steer/MAX_STEER;
  }
}

// super-ghetto accel code. acceleration is for the most part proportional
// to the difference between curVelo and desired velocity (VELOCITY). 
// Uh-Ooh~ Uh-Oooh~
// return value: acceleration value in -1(full brake) to 1(full throttle)
float WaypointFollowing::f_Accel(float curVelo, float curAccel, Vector curPos, Vector curWay) {
  // read the curVelo, try to make the Tahoe go at DESIRED_VELOCITY
  // assume this is called about every < second?
  float veloDiff = DESIRED_VELOCITY - curVelo;
  float accel;
  
  if (veloDiff < 0) {  // going too fast
    if (fabs(veloDiff) > TOO_FAST) {                // going way too fast, so brake
      accel = veloDiff * SCALE_ACCEL_BRAKE + MIN_ACCEL_BRAKE;
      if (accel < MAX_ACCEL_BRAKE) {
	accel = MAX_ACCEL_BRAKE;
      }
    }
    else {
      // just a little too fast, so just let it run idle
      accel = 0;
    }
  }
  else { // veloDiff > 0, going too slow
    accel = veloDiff * SCALE_ACCEL_GAS * 
           (MAX_ACCEL_GAS - MIN_ACCEL_GAS) + MIN_ACCEL_GAS;
    
    if (accel > MAX_ACCEL_GAS) {
      accel = MAX_ACCEL_GAS;
    }
  }
  return accel;
}

void shutdown(int signal) {
  //cout << "Signal Received" << endl;
  shutdown_flag = TRUE;
  // boost::recursive_mutex::scoped_lock scoped_lock(WF.brakeMutex);
  //set_brake_abs_pos_pot(0.8, brakeVelo, brakeVelo);
  //cout << "Set Abort Flag" << endl;
  //sleep_for(10);
  //exit(EXIT_SUCCESS);
}


int main() {

  // setup signals for shutdown
  shutdown_flag = FALSE;
  signal(SIGINT, &shutdown);
  signal(SIGTERM, &shutdown);

  int tmp;

  // hold down the brake while we boot up
  // init_accel();
  // init_brake();
  //set_brake_abs_pos_pot(0.8, brakeVelo, brakeVelo);
  //set_accel_abs_pos(0.0);




  // Initialize steering
    init_steer();

  // init GPS and MAG
  cout << "Initializing Magnetometer" << endl;
  tmp = mm_init(DEFAULT_MAGNETOMETER_SERIAL_PORT);
  cout << "Mag Init Response = " << tmp << endl;
  cout << "Initializing GPS";
  tmp = init_gps(DEFAULT_GPS_SERIAL_PORT, 10);
  cout << "GPS Init Response = " << tmp << endl;


  // spawn state estimator and waypoint follower
  boost::thread_group thrds;

  thrds.create_thread(Starter); // will Start Grab State Loop
  thrds.create_thread(Starter); // will Start Drive Waypoints
  
  thrds.join_all();
  cout << "Shutting Down" << endl;
  

  return 0;
}


void WaypointFollowing::GrabStateLoop() {
  timeval tv;

  state_struct tmpSS;
  //cout << "Starting GrabStateLoop, shutdown_flag = " << shutdown_flag << endl;

  while(shutdown_flag == FALSE) {
    //sleep_for(1); // sleep for 1 second
    usleep_for(200000);
    tmpSS = get_state();
    // put the next block inline so the lock will destroy itself before the sleep below

    
    gettimeofday(&tv, NULL);
    stateFile << timeval_subtract(tv, timeZero) << "\t"
	      << tmpSS.easting  << "\t"
	      << tmpSS.northing << "\t"
	      << tmpSS.vel_e    << "\t"
	      << tmpSS.vel_n    << "\t"
	      << tmpSS.speed    << "\t"
	      << tmpSS.heading  << "\t"
	      << tmpSS.heading_e<< "\t"
	      << tmpSS.heading_n<< endl;
	      

    boost::recursive_mutex::scoped_lock scoped_lock(stateMutex);
    SS = tmpSS;
    
    //    cout << "Finished Getting State, shutdown_flag = " << shutdown_flag << endl;
  }
  cout << "State Estimator Shutting Down" << endl;
  mm_uninit();
  
}

void WaypointFollowing::DriveWaypoints() {
  // Waypoint Following Thread

  int ctr=0;
  timeval tv;
  // Let the state estimator get data.
  sleep_for(5);
  
  list<Vector> waypoints;
  Vector currentWaypoint, currentHeading, currentPosition;
  float currentSpeed, currentAccel;
  float newSteering, newAccel;
  
  // Read in all waypoints
  ifstream waypointFile("waypoints.gps");
  while(!waypointFile.eof()) {
    waypointFile >> currentWaypoint.E >> currentWaypoint.N;
    cout << "East = " << currentWaypoint.E << ", " << currentWaypoint.N << endl;
    waypoints.push_back(currentWaypoint);
  }
  // close waypoints file
  waypointFile.close();
  
  while( waypoints.size() > 0 && shutdown_flag == FALSE) {
    currentWaypoint = waypoints.front();
    currentPosition = get_position();
    currentHeading  = get_heading();
    currentSpeed    = get_speed();
    cout << ctr++ 
	 << "++++++++++++++++++++++++++++++++++++++++" << endl;    
    cout << "Waypoint Position = " << currentWaypoint.E
	 << "E, " << currentWaypoint.N << "N" << endl;

    cout << "Current  Position = " << currentPosition.E 
	 << "E, " << currentPosition.N << "N" << endl;

    cout << "Current  Heading = " << currentHeading.E 
	 << "E, " << currentHeading.N << "N" << endl;

    cout << "Speed = " << currentSpeed << endl;
    
    if( withinWaypoint(currentPosition, currentWaypoint, waypointRadius) ) {
      cout << "Waypoint Acheieved" << endl;
      waypoints.pop_front();
    } else {
      newSteering = f_Steering(currentPosition, currentWaypoint, currentHeading);
      newAccel    = f_Accel(currentSpeed, currentAccel, currentPosition, currentWaypoint);

      gettimeofday(&tv, NULL);
      driveFile << timeval_subtract(tv, timeZero) << "\t"
 		<< currentPosition.E << "\t"
		<< currentPosition.N << "\t"
		<< currentHeading.E  << "\t"
		<< currentHeading.N  << "\t"
		<< currentSpeed      << "\t"
		<< newSteering       << "\t"
		<< newAccel <<endl;
      
      if( -1.0 <= newSteering && newSteering <= 1.0 ) {
	//cout << "Setting steering to " << newSteering << endl;
	steer_heading(newSteering);
      } else {
	cout << "Bad Steering Command = " << newSteering << endl;
      }
      if(-1.0 <= newAccel && newAccel <= 0.0) {
	cout << "Setting brake to " << newAccel << endl; 
	//set_accel_abs_pos(0.0);
	//set_brake_abs_pos_pot( newAccel, brakeVelo, brakeVelo );
      } else if(0.0 <= newAccel && newAccel <= 1.0) {
	cout << "Setting throttle to " << newAccel << endl;
	//set_accel_abs_pos(newAccel);
	//set_brake_abs_pos_pot( 0.0, brakeVelo, brakeVelo );
      } else {
	cout << "Bad Accel Command = " << newAccel << endl;
      }
      
      currentAccel = newAccel;
      //sleep_for(1);
      usleep_for(200000);
      // log
    }
  }

  shutdown_flag = TRUE;  // make sure the state estimator will shut down      
  // Reset the brake so we are guaranteed to stop the vehicle
  //   set_accel_abs_pos( 0.0 );
  //   set_brake_abs_pos_pot( MAX_ACCEL_BRAKE, brakeVelo, brakeVelo );
  

}





// state accessor functions
// locks state structure, reads state info, then unlocks

Vector WaypointFollowing::get_heading() {
  Vector vheading;
  boost::recursive_mutex::scoped_lock scoped_lock(stateMutex);
  vheading.N = SS.vel_n;
  vheading.E = SS.vel_e;
  return vheading;
}

Vector WaypointFollowing::get_position()
{
  Vector vpos;
  boost::recursive_mutex::scoped_lock scoped_lock(stateMutex);
  //  cout << "State N = " << SS.northing << ", E = " 
  //     << SS.easting << endl;
  vpos.N = SS.northing;
  vpos.E = SS.easting;
  return vpos;
}
float WaypointFollowing::get_speed()
{
  float speed;
  boost::recursive_mutex::scoped_lock scoped_lock(stateMutex);
  speed = SS.speed;
  return speed;
}
