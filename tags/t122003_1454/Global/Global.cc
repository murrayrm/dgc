// dgc_msg_main.cc 

#include "Msg/dgc_mta.hh"
#include "Msg/dgc_msg.h"
#include "Msg/dgc_socket.h"
#include "Msg/dgc_messages.h"
#include "Msg/dgc_const.h"
#include "Msg/dgc_mta_main.h"
#include "Msg/dgc_time.h"
#include "state/state.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
 

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <iostream>
#include <iomanip>
#include <list>
#include <time.h>
#include <fstream>
#include <math.h>

struct UTM {

  double N;
  double E;

};

struct bearing {
  double mag;
  double ang;
};

struct steeringAndAccel {
  float steering;
  float accel;
  float dist;
};

double vectorAngle(UTM a, UTM b);

bearing getVectorDims( UTM a);


const float MaxVel= 0.05;
const float stoppingDistance = 10.0;
const float WaypointHalo = 5.0;
const float RefreshTime = 0.05;///2.0;

int shutdown_flag;
using namespace std;

int main(int argc, char* argv[])
{
  ofstream outfile ( "testrun.gps");
  
  state_struct mySS;
  shutdown_flag = FALSE;
  dgc_msg_Register(dgcs_GlobalRoutePlanner);
  cout << endl << endl << "Registering at time t=" 
       << setprecision(10) << SystemTime() << endl;
  dgc_MsgHeader myMH;
  
  UTM wayHeading;
  UTM wayVector;

  timeval before, after;
  gettimeofday(&before, NULL);
  
  double dot;
  double mag;
  double ang;
  bearing myBearing;
  //  float accel, myFloat;
  steeringAndAccel af;
  
  // read in all waypoints
  UTM myUTM, activeWaypoint;
  list<UTM> myWaypoints;
  list<UTM>::iterator myWaypointIterator;
  ifstream infile ( "waypoints.gps");//, ios::binary );
  cout << "Grabbing Waypoints" << endl;
  while ( !infile.eof()) {
    infile >> myUTM.E >> myUTM.N;
    cout << "East = " << myUTM.E << ", North = " << myUTM.N << endl;
    myWaypoints.push_back(myUTM);
  }
  /// /// 
  shutdown_flag = TRUE;
  
  cout << "Num Waypoints = " << myWaypoints.size() << endl;
  if( myWaypoints.size() > 0) {
    myWaypointIterator = myWaypoints.begin();
    activeWaypoint = (*myWaypointIterator);
    cout << "First waypoint = " << activeWaypoint.E 
	 << " E, " << activeWaypoint.N << " N" << endl;
    myWaypoints.pop_front();
    shutdown_flag = FALSE;
  }
  while ( shutdown_flag == FALSE ) { 
    
    gettimeofday(&after, NULL);
    if( timeval_subtract(after, before) > RefreshTime) {
      //      cout << "Requesting a message from state" << endl;
      // Request a message from State
      myMH.Destination = 0;
      myMH.DestinationType = dgcs_StateDriver;
      myMH.MsgType = dgcm_RequestState;
      myMH.MsgSize = 0;
      dgc_SendMessage(myMH, NULL);
      before = after;
    }
    //    usleep(5000);

    //    cout << "Polling For Message" << endl;
    if ( dgc_PollMessage(myMH) == TRUE) {
      // Get the message      
      if ( dgc_GetMessage(myMH, &mySS) == TRUE ) {

	cout << "Got a message" << endl;
	// save state info
	cout << mySS.timestamp << ", " << mySS.northing << ", " 
	     << mySS.easting   << ", " << mySS.altitude << ", " 
	     << mySS.vel_n     << ", " << mySS.vel_e    << ", "
	     << mySS.vel_u     << ", " << mySS.heading  << ", " 
	     << mySS.pitch     << ", " << mySS.roll     << endl;
	
	
	// calculate where to drive the car
	wayHeading.N = (activeWaypoint.N - mySS.northing);
	wayVector.N  =  mySS.vel_n;
	wayHeading.E = (activeWaypoint.E - mySS.easting);
	wayVector.E  = mySS.vel_e;
	
	ang = vectorAngle(wayHeading, wayVector);
	myBearing = getVectorDims(wayHeading);
	
	if( myBearing.mag > stoppingDistance ) {
	  af.accel = MaxVel;
	} else {
	  af.accel = -0.9;
	}
	af.steering = ( fabs(ang * 2) > 1.0)? ang/fabs(ang): ang*2; 
	af.dist = myBearing.mag;
	/*	
	// forward visual to Flat Panel
	myMH.Destination = 0;
	myMH.DestinationType = dgcs_FlatPanel;
	myMH.MsgType = dgcm_DriverMessage;
	myMH.MsgSize = sizeof(steeringAndAccel);
	dgc_SendMessage(myMH, & af);
	*/ /*
	// Forward the gas/brake pedal portion to the gas/brake driver
	myMH.Destination = 0;
	myMH.DestinationType = dgcs_Jeremy;
	myMH.MsgType = dgcm_AccelStatePacket;
	myMH.MsgSize = sizeof(float);
	dgc_SendMessage(myMH, & af.accel);
	   */
	// Forward the steering pedal portion to the steering driver 
	/*
	myMH.Destination = 0;
	myMH.DestinationType = dgcs_SteeringDriver;
	myMH.MsgType = dgcm_SteeringStatePacket;
	myMH.MsgSize = sizeof(float);
	dgc_SendMessage(myMH, & af.steering);
	*/	
	cout << "Sent steering = " << af.steering << endl;
	// if we are within the halo, consider it a success, stop the vehicle
	// wait 10 seconds, and proceed with the next waypoint
	if(myBearing.mag < WaypointHalo) {
	  // forward visual to Flat Panel
	  // make accel extremely negative
	  cout << "It's a hit" << endl;
	  af.accel = -10;
	  /*
	  myMH.Destination = 0;
	  myMH.DestinationType = dgcs_FlatPanel;
	  myMH.MsgType = dgcm_DriverMessage;
	  myMH.MsgSize = sizeof(steeringAndAccel);
	  dgc_SendMessage(myMH, & af);
	  */
	  // load the next waypoint
	  if( myWaypoints.size() > 0) {
	    myWaypointIterator = myWaypoints.begin();
	    activeWaypoint = (*myWaypointIterator);
	    myWaypoints.pop_front();
	    cout << "Sleeping for 10" << endl;
	    sleep(10);
	    cout << "Done Sleeping" << endl;
	  } else {
	    shutdown_flag = TRUE;
	  }
	}
      }
    }
  }

  // stop the car = we are done
  af.accel = -10.0;
  af.dist = 0.0;
  myMH.Destination = 0;
  myMH.DestinationType = dgcs_Jeremy;
  myMH.MsgType = dgcm_AccelStatePacket;
  myMH.MsgSize = sizeof(float);
  dgc_SendMessage(myMH, & af.accel);

  
  // send a message to flat panel saying we are done.
  af.dist = -10.;
  af.accel = -10.;
  myMH.Destination = 0;
  myMH.DestinationType = dgcs_FlatPanel;
  myMH.MsgType = dgcm_DriverMessage;
  myMH.MsgSize = sizeof(steeringAndAccel);
  dgc_SendMessage(myMH, & af);
  
  
  cout << endl << endl << "Unregistering" << endl;
  dgc_msg_Unregister();
  
  return 0;
}




double vectorAngle(UTM a, UTM b) {
  
  bearing A = getVectorDims(a);
  bearing B = getVectorDims(b);
  
  double d = A.ang - B.ang;
  if (d > M_PI) {
    d = d - 2*M_PI;
  } else if(d < -M_PI) {
    d = d + 2 * M_PI;
  }
  return d;
}

bearing getVectorDims(UTM a){

  bearing ret;
  ret.mag = sqrt( a.N * a.N + a.E * a.E);

  if( ret.mag > 0 ) {
    ret.ang = acos(a.E / ret.mag);
    if( a.N < 0) {
      ret.ang = 2*M_PI - ret.ang;
    }
  }
  return ret; //bearing;

}






