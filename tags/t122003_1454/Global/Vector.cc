#include <cmath>
#include <iostream>
#include "Vector.hh"

using namespace std;

float distVector(Vector a, Vector b) {
  return sqrt((a.N-b.N)*(a.N-b.N) + (a.E-b.E)*(a.E-b.E));
}

// return value:  float, angle in radians
//                        < 0 if waypoint is to the right of head
//                        > 0 if waypoint is to the left of head 
//                          or completely opposite
// Precision: angles below ~0.0003 are returned as 0.
//            see def_prec.txt for more info/test run
float get_deflection(Vector pos, Vector way, Vector head) {
  Vector way_rel;
  float way_angle;  // angle to waypoint in map frame
  float head_angle; // current heading angle in map frame
  float angle; // angle to waypoint in vehicle frame
  
  // set pos as (0,0)
  way_rel.N = way.N - pos.N;
  way_rel.E = way.E - pos.E;

//    cout << "way_rel.N = " << way_rel.N << endl;
//    cout << "way_rel.E = " << way_rel.E << endl;
    //  cout << "head.N = " << head.N << endl;
    // cout << "head.E = " << head.E << endl;

  // get the angle between head and way:
  way_angle = atan2( way_rel.N, way_rel.E );
//    cout << "way_angle = " << way_angle << endl;
 
  head_angle = atan2( head.N, head.E );
  //  cout << "head_angle = " << head_angle << endl;
  
  // Desired heading in vehicle reference frame
  angle = way_angle - head_angle;
  // Normalize the desired heading to be in [-pi,pi]
  if( angle > M_PI ) angle = angle - 2.0*M_PI;
  if( angle < -M_PI ) angle = angle + 2.0*M_PI;

  // Should probably print this out
//  cout << "angle (deg) = " << angle*180.0/M_PI << endl;

  return angle;
  
}

bool withinWaypoint(Vector pos, Vector way, float threshold) {
  return threshold > sqrt((pos.N-way.N)*(pos.N-way.N) + (pos.E-way.E)*(pos.E-way.E));
}

/*
// test get_deflection real quick
int main() {
  Vector test[10];
  Vector position;
  const float north[10] = { 1, 1.001, 1, 1.0001, 1, 1.00001, 1, .999, 1, .9999};
  const float east[10]  = { 1,     1, 1,      1, 1,       1, 1,    1, 1,     1};

  position.N = 0;
  position.E = 0;
  for (int i = 0; i < 10; i++) {
    test[i].N = north[i];
    test[i].E = east[i];
  }
  for (int i = 0; i < 10; i++) {
    cout << "test #" << i << endl;
    cout << "pos: (" << position.E << ", " << position.N << ")" << endl;
    cout << "way: (" << test[i].E << ", " << test[i].N << ")" << endl;
    cout << "hea: (" << test[(i+1)%10].E << ", " << test[(i+1)%10].N << ")" << endl;
    cout << "angle: " << get_deflection(position, test[i], test[(i+1)%10]) << endl;
  }

  return 0;
}
*/
