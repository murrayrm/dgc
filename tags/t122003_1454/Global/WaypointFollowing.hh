#include <boost/thread/thread.hpp>
#include "state/state.h"
#include "Vector.hh"
#include <boost/thread/recursive_mutex.hpp>
#include <fstream>
#include <unistd.h>
#include <sys/time.h>

using namespace std;

class WaypointFollowing {

public:

  WaypointFollowing();
  ~WaypointFollowing();


  void Init();
  void GrabStateLoop();
  void DriveWaypoints();
  int  NextThreadNumber();


private:
  state_struct SS;
  boost::recursive_mutex stateMutex;
  boost::recursive_mutex brakeMutex; // must be public so shutdown function can execute
  boost::recursive_mutex threadMutex;
  float f_Steering(Vector curPos, Vector curway, Vector curhead);
  float f_Accel(float curVelo, float curAccel, Vector curPos, Vector curWay);

  Vector get_heading();
  Vector get_position();
  float  get_speed();

  int    thrdCounter;
  ofstream stateFile;
  ofstream driveFile;
  timeval timeZero;

};
