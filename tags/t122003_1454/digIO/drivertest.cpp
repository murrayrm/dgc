#include <iostream>
#include "digIO/digIO.h"

using namespace std;

int main(){
    int x = digio_open();
    unsigned int mask;
    unsigned int outval;
    while (true){
        cout << "input mask:\t";
        cin >> hex >> mask;
        cout << "input outval:\t" ;
        cin >> hex >> outval;
//        cout << outval;
        if (outval ==  0x100){
             exit(0);}
        digio_write(outval, mask, TME_OUT_PORT);}
    digio_write(outval,mask,TME_OUT_PORT);
    digio_close();
    return(0);}
