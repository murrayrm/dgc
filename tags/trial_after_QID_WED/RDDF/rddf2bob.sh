# vim: syntax=awk

#rddf2bob.sh
#by alan somers, 31-jan-2004

#this script converts a waypoint file in the RDDF format to one in the
#BOB format.  Input is stdin, Output is stdout
#Usage: ./rddf2bob.sh > outfile < infile 

#requires a make install in team/latlon
#requires team/bin to be in the path
#do PATH=/home/username/team/bin:PATH

gawk '
    BEGIN {FS=","}
    function latlon2utm(lat,lon){
        OFS=" ";
        print "test-latlon-to-utm", lat, lon | "/bin/sh";
        close("/bin/sh");
        return 0}
    /^[^#]/ {print ""; print $4; print $5; latlon2utm($2, $3)}' |
gawk '
    function ft2m(feet){
        return feet*0.304800609}
    function mph2mps(mph){
        return mph*0.447040893}
    BEGIN {FS="\n"; RS=""; OFS=" "}
    { split($7, easting, " ");
      split($8, northing, " ");
      print "N", easting[4], northing[4], ft2m($1), mph2mps($2)}'
