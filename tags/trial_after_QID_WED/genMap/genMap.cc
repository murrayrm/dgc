/* Definitions for methods in the generic map class designed to be used for
 * path evaluation by LADAR and stereo vision. See genMap.hh for the whole
 * class.
 * Author: Sue Ann Hong  -  General class methods
 *         Adam Craig - Map frame manipulation methods
 *         Thyago Consort - Path evaluation methods
 *
 * Revision History:  
 *   1/15/2004 Created
 *
 *   2004/02/18  Lars Cremean  Changed all instances of leftBottomUTMx to 
 *   leftBottomUTM_Northing and leftBottomUTMy to leftBottomUTM_Easting.
 *   Please check to make sure that these are the proper assignments.
 *   Look for "TODO" comments to see places where the assignments are 
 *   questionable
 *   2004/02/19  Adam Craig    changed assignments so that use of
 *    x = Northing -> Row : y = Easting -> Col axes was consistent. 
 */

#include "genMap.hh"

// Accessors
int genMap::getNumRows() { return numRows; }
int genMap::getNumCols() { return numCols; }
double genMap::getRowRes() { return rowRes; }
double genMap::getColRes() { return colRes; }
int genMap::getVehRow() { return vehRow; }
int genMap::getVehCol() { return vehCol; }
double genMap::getLeftBottomUTM_Northing(){ return leftBottomUTM_Northing; }
double genMap::getLeftBottomUTM_Easting() { return leftBottomUTM_Easting; }
double genMap::getVehNorthing(){ return leftBottomUTM_Northing + (numRows*numCols/2); }
double genMap::getVehEasting() { return leftBottomUTM_Easting + (numCols*colRes/2); }
int genMap::getLeftBottomRow() { return leftBottomRow; }
int genMap::getLeftBottomCol() { return leftBottomCol; }
double genMap::getPathMaxDistance() { return pathMaxDistance; }
int genMap::getNumTargetPoints() { return numTargetPoints; }
int genMap::getLastWaypoint() { return lastWaypoint; }
int genMap::getNextWaypoint() { return nextWaypoint; }
RDDFVector genMap::getTargetPoints() { return targetPoints; }

//Mutators
void genMap::setLastWaypoint(int pLastWaypoint){ 
  lastWaypoint = pLastWaypoint;
}
void genMap::setNextWaypoint(int pNextWaypoint){ 
  nextWaypoint = pNextWaypoint;
}
void genMap::setNumTargetPoints(int pNumTargetPoints){
  numTargetPoints = pNumTargetPoints;
}
void genMap::setTargetPoints(RDDFVector pTargetPoints){
  targetPoints = pTargetPoints;
}

// Default constructor
genMap::genMap(bool init)
{
  curState.Easting = 0;
  curState.Northing = 0;
  curState.Altitude = 0;
  curState.Vel_E = 0;
  curState.Vel_N = 0;
  curState.Vel_U = 0;
  curState.Speed = 0;
  curState.Accel = 0;
  curState.Pitch = 0;
  curState.Roll = 0;
  curState.Yaw = 0;
  curState.PitchRate = 0;
  curState.RollRate = 0;
  curState.YawRate = 0;

  if(init){
    initMap(NUM_ROWS,NUM_COLS,ROW_RES,COL_RES);
  }
}

// Constructor with default values (except initState)
// No default values??
genMap::genMap(VState_GetStateMsg initState, 
	       int new_num_rows, 
	       int new_num_cols, 
	       double new_row_res, 
	       double new_col_res,
               bool pEvalCorridor, double data_lifespan)
{
  // Accessing RDDF target points
  Copy_VState(initState);
  initMap(new_num_rows,new_num_cols,new_row_res,new_col_res,pEvalCorridor,data_lifespan);
}

// Set path evaluation cost to be used
void genMap::initPathEvaluationCriteria(bool pUseCellsUnderCost, 
					bool pUseOffTireCost,
					bool pUseDotVectorCost,
					bool pUseMaxHeightDiffCost,
					bool pUseMaxInclinationCost,
                                        bool pUseObsAvoidanceVelocities,
                                        bool pUseMaxHeightDiffCostRectangle,
                                        bool pUseConfinedSpaceGoodnessVotes){
  useCellsUnderCost = pUseCellsUnderCost;
  useOffTireCost    = pUseOffTireCost;
  useDotVectorCost  = pUseDotVectorCost;
  useMaxHeightDiffCost = pUseMaxHeightDiffCost;
  useMaxInclinationCost = pUseMaxInclinationCost;
  useObsAvoidanceVelocities = pUseObsAvoidanceVelocities;
  useMaxHeightDiffCostRectangle = pUseMaxHeightDiffCostRectangle;
  useConfinedSpaceGoodnessVotes = pUseConfinedSpaceGoodnessVotes;

  // debug
  /*
  if(useCellsUnderCost) cout << "USING CELLS UNDER COST" << endl;
  if(useOffTireCost) cout << "USING OFF TIRE COST" << endl;
  if(useDotVectorCost) cout << "USING DOT VECTOR COST" << endl;
  if(useMaxHeightDiffCost) cout << "USING MAX HEIGHT DIFF COST" << endl;
  if(useMaxInclinationCost) cout << "USING MAX INCLINATION COST" << endl;
  */
}

// Set path evaluation thresholds
void genMap::initThresholds(double pObstacleThreshold,
			    double pHeightDiffThreshold,
			    double pTireOffThreshold,
			    double pVectorProdThreshold,
                            double pGrowVehicleLength,
                            double pGrowVehicleWidth){

  obstacleThreshold   = pObstacleThreshold;
  heightDiffThreshold = pHeightDiffThreshold; 
  tireOffThreshold    = pTireOffThreshold;
  vectorProdThreshold = pVectorProdThreshold;
  growVehicleLength   = pGrowVehicleLength;
  growVehicleWidth    = pGrowVehicleWidth;
}
// Display thresholds
void genMap::displayThresholds(){
  cout << "Obs = " << obstacleThreshold << " DH = " << heightDiffThreshold << " TIRE_OFF = " << tireOffThreshold << " VEC_PROD = " << vectorProdThreshold << endl;
} 

// Set genMap, call just once
void genMap::initMap(int num_rows, int num_cols, double row_res, double col_res,bool pEvalCorridor, bool pActivateNoDataCell, double data_lifespan)
{
  RowCol startPos,deltaPos;
  double max;

  // Initializing thresholds
  initThresholds();
  // Initializing how path evaluation will evaluate cost
  initPathEvaluationCriteria();

  // Time stamp
  curTimeStamp = 0;

  // Row/Col
  numRows = num_rows;
  numCols = num_cols;
  rowRes = row_res;
  colRes = col_res;

  // Vehicle position in indices
  if(floor((double)numRows/2)  < (double)numRows/2){
    vehRow = (int)floor((double)numRows/2);
  }
  else{
    vehRow = (int)floor((double)numRows/2) -1;
  }
  if(floor((double)numCols/2)  < (double)numCols/2){
    vehCol = (int)floor((double)numCols/2);
  }
  else{
    vehCol = (int)floor((double)numCols/2) -1;
  }

  //Defining the maximum path
  max = (double)numRows*rowRes;
  if(max > numCols*colRes) {
    max = (double)numCols*colRes;
  }
  pathMaxDistance = max/2 - (VEHICLE_LENGTH);

  // Calculate left bottom UTM coordinates
  leftBottomUTM_Northing = curState.Northing - (double)vehRow*rowRes - rowRes/2;
  leftBottomUTM_Easting = curState.Easting - (double)vehCol*colRes - colRes/2;
  leftBottomRow = 0;
  leftBottomCol = 0;
  NUMBER_OF_COLS_RIGHT_TO_CHECK = (int)floor(RADIUS_OF_SEARCHING_FOR_DATA/colRes);
  if(NUMBER_OF_COLS_RIGHT_TO_CHECK < 1)
    {
      NUMBER_OF_COLS_RIGHT_TO_CHECK = 1;
    }
  if(NUMBER_OF_COLS_RIGHT_TO_CHECK > MAX_NUMBER_CELLS_TO_CHECK)
    {
      NUMBER_OF_COLS_RIGHT_TO_CHECK = MAX_NUMBER_CELLS_TO_CHECK;
    }

  NUMBER_OF_CELLS_DIAGONAL_TO_CHECK = (int)floor(RADIUS_OF_SEARCHING_FOR_DATA/sqrt(colRes*colRes + rowRes*rowRes));
  if(NUMBER_OF_CELLS_DIAGONAL_TO_CHECK < 1)
    {
      NUMBER_OF_CELLS_DIAGONAL_TO_CHECK = 1;
    }
  if(NUMBER_OF_CELLS_DIAGONAL_TO_CHECK > MAX_NUMBER_CELLS_TO_CHECK)
    {
      NUMBER_OF_CELLS_DIAGONAL_TO_CHECK = MAX_NUMBER_CELLS_TO_CHECK;
    }

  NUMBER_OF_ROWS_DOWN_TO_CHECK = (int)floor(RADIUS_OF_SEARCHING_FOR_DATA/rowRes);
  if(NUMBER_OF_ROWS_DOWN_TO_CHECK < 1)
    {
      NUMBER_OF_ROWS_DOWN_TO_CHECK = 1;
    }
  if(NUMBER_OF_ROWS_DOWN_TO_CHECK > MAX_NUMBER_CELLS_TO_CHECK)
    {
      NUMBER_OF_ROWS_DOWN_TO_CHECK = MAX_NUMBER_CELLS_TO_CHECK;
    }


  // TOO SLOW
  // Adding "NULL" elements to the map
  //  RowCol posMap;
  /*for(int rowInd=0; rowInd< numRows ;rowInd++){
    for(int colInd =0; colInd< numCols; colInd++){
      posMap.first = rowInd;
      posMap.second = colInd;
      theMap[posMap] = NO_DATA_CELL;
    }
  }*/
  theMap.clear();

  // Initializing Waypoints
  lastWaypoint = 0;
  nextWaypoint = 0;
  numTargetPoints = 0;
  targetPoints.clear();

  //if(numTargetPoints > 1){
  //  nextWaypoint = targetPoints[1].number;
  //}
  DataLifespan = data_lifespan;
  activateNoDataCell = pActivateNoDataCell;

  // painting corridor as obstacles (used by CorridorArcEvaluator)
  evalCorridor = pEvalCorridor;
  if(evalCorridor){
    startPos.first = leftBottomRow;
    startPos.second = leftBottomCol;
    deltaPos.first = numRows;
    deltaPos.second = numCols;
    paintCorridor(startPos,deltaPos);
  }
}
/*
 void genMap::initMap(int num_rows, int num_cols, double row_res, double col_res,bool pEvalCorridor)
 {
 RowCol startPos,deltaPos;
 double max;
 RDDF rddf;
 
 // Initializing thresholds
  initThresholds();

  // Time stamp
  curTimeStamp = 0;

  // Getting RDDF file
  targetPoints = rddf.getTargetPoints();
  numTargetPoints = rddf.getNumTargetPoints();
  //cout << numTargetPoints << endl;
  //getchar();

  // Row/Col
  numRows = num_rows;
  numCols = num_cols;
  rowRes = row_res;
  colRes = col_res;

  // Vehicle position in indices
  if(floor((double)numRows/2)  < (double)numRows/2){
    vehRow = (int)floor((double)numRows/2);
  }
  else{
    vehRow = (int)floor((double)numRows/2) -1;
  }
  if(floor((double)numCols/2)  < (double)numCols/2){
    vehCol = (int)floor((double)numCols/2);
  }
  else{
    vehCol = (int)floor((double)numCols/2) -1;
  }

  //Defining the maximum path
  max = (double)numRows*rowRes;
  if(max > numCols*colRes) {
    max = (double)numCols*colRes;
  }
  pathMaxDistance = max/2 - (VEHICLE_LENGTH);

  // Calculate left bottom UTM coordinates
  leftBottomUTM_Northing = curState.Northing - (double)vehRow*rowRes - rowRes/2;
  leftBottomUTM_Easting = curState.Easting - (double)vehCol*colRes - colRes/2;
  leftBottomRow = 0;
  leftBottomCol = 0;

  // TOO SLOW
  // Adding "NULL" elements to the map
  //  RowCol posMap;
  /*for(int rowInd=0; rowInd< numRows ;rowInd++){
    for(int colInd =0; colInd< numCols; colInd++){
      posMap.first = rowInd;
      posMap.second = colInd;
      theMap[posMap] = NO_DATA_CELL;
    }
  }*/
/*theMap.clear();
  // Initializing Waypoints
  if(numTargetPoints){
    lastWaypoint = targetPoints[0].number;
    nextWaypoint = lastWaypoint;
  }
  //if(numTargetPoints > 1){
  //  nextWaypoint = targetPoints[1].number;
  //}
  DataLifespan = 1000;
  // painting corridor as obstacles (used by CorridorArcEvaluator)
  evalCorridor = pEvalCorridor;
  if(evalCorridor){
    startPos.first = leftBottomRow;
    startPos.second = leftBottomCol;
    deltaPos.first = numRows;
    deltaPos.second = numCols;
    paintCorridor(startPos,deltaPos);
  }
}
*/
// Copy Constructor
genMap::genMap(const genMap &other)
  : numRows(other.numRows), numCols(other.numCols),
    rowRes(other.rowRes), colRes(other.colRes),
    vehRow(other.vehRow), vehCol(other.vehCol),
    leftBottomUTM_Easting(other.leftBottomUTM_Easting), 
    leftBottomUTM_Northing(other.leftBottomUTM_Northing),
    leftBottomRow(other.leftBottomRow), leftBottomCol(other.leftBottomCol),
    evalCorridor(other.evalCorridor), pathMaxDistance(other.pathMaxDistance),
    numTargetPoints(other.numTargetPoints),
    targetPoints(other.targetPoints),
    lastWaypoint(other.lastWaypoint), nextWaypoint(other.nextWaypoint), DataLifespan(other.DataLifespan)
{
  Copy_VState(other.curState);
  theMap = other.theMap;
}

// Assignment Operator
genMap &genMap::operator=(const genMap &other)
{
  if (this != &other) {
    destroy();
    numRows = other.numRows;
    numCols = other.numCols;
    rowRes = other.rowRes;
    colRes = other.colRes;
    vehRow = other.vehRow;
    vehCol = other.vehCol;
    leftBottomUTM_Northing = other.leftBottomUTM_Northing;
    leftBottomUTM_Easting  = other.leftBottomUTM_Easting;
    leftBottomRow = other.leftBottomRow;
    leftBottomCol = other.leftBottomCol;
    evalCorridor = other.evalCorridor;
    pathMaxDistance = other.pathMaxDistance;
    targetPoints = other.targetPoints;
    numTargetPoints = other.numTargetPoints; 
    lastWaypoint = other.lastWaypoint;
    nextWaypoint = other.nextWaypoint;
    DataLifespan = other.DataLifespan;
    Copy_VState(other.curState);
    theMap = other.theMap;
  }
  return *this;
}


// Accessors
CellData genMap::getCellDataRC(int rowNumber, int colNumber)
{
  CellData ret;
  double dT; // time difference
  double param;

  //  CellData retCell;
  if (rowNumber < 0 || rowNumber > numRows) {
    cout << "row number input out of range: " << rowNumber << endl;
    return(NO_DATA_CELL);
  }
  else if (colNumber < 0 || colNumber > numCols) {
    cout << "col number input out of range: " << colNumber << endl;
    return(NO_DATA_CELL);
  }

  RowCol findingIndices(rowNumber, colNumber);

  map<RowCol,CellData>::iterator cell = theMap.find(findingIndices);

  if ( cell == theMap.end() ) {
    // Return the default heigh value
    //cout << "Cell (" << rowNumber << "," << colNumber << ") not found" << endl;
    return(NO_DATA_CELL);
  }
  ret = cell->second;
  // Shrinking obstacles
  dT = curTimeStamp - ret.timeStamp;
  ret.value = shrinkObstacle(ret.value,dT);
  return(ret);
}


CellData genMap::getRecentCellDataRC(int rowNumber, int colNumber)
{
  CellData ret;
  double dT; // time difference

  //  CellData retCell;
  if (rowNumber < 0 || rowNumber > numRows) {
    cout << "row number input out of range: " << rowNumber << endl;
    return(NO_DATA_CELL);
  }
  else if (colNumber < 0 || colNumber > numCols) {
    cout << "col number input out of range: " << colNumber << endl;
    return(NO_DATA_CELL);
  }
  RowCol findingIndices(rowNumber, colNumber);
  if ( theMap.find(findingIndices) == theMap.end() ) {
    // Return the default heigh value
    return(NO_DATA_CELL);
  }
  else {
    ret = (theMap.find(findingIndices))->second;
    // Shrinking obstacles
    dT = curTimeStamp - ret.timeStamp;
    //cout << "delta T = " << dT << " param*dT = " << -(double)EXP_SHRINK_PARAMETER*dT
    //     << " exp() = " << exp(-((double)EXP_SHRINK_PARAMETER)*dT) << endl;
    //cout << " ret.value = " << ret.value << endl;
    if(dT > DataLifespan)
      {
	return(NO_DATA_CELL);
      }
    return(ret); 
  }
}

CellData genMap::getCellDataUTM(double Northing, double Easting)
{
  // From the left bottom UTM coord, find the row and col for the input coord.
  RowCol findingIndices = UTM2RowCol(Northing, Easting);
  return getCellDataRC(findingIndices.first, findingIndices.second);
}

CellData genMap::getRecentCellDataUTM(double Northing, double Easting)
{
  // From the left bottom UTM coord, find the row and col for the input coord.
  RowCol findingIndices = UTM2RowCol(Northing, Easting);
  return getRecentCellDataRC(findingIndices.first, findingIndices.second);
}

CellData genMap::getCellDataFrameRef(double dist_ahead, double dist_aside)
{
  RowCol findingIndices, vehPos;
  vehPos.first = vehRow;
  vehPos.second = vehCol;

  findingIndices = FrameRef2RowCol(vehPos,curState.Yaw,dist_ahead,dist_aside);
  return getCellDataRC(findingIndices.first, findingIndices.second);
}

CellData genMap::getRecentCellDataFrameRef(double dist_ahead, double dist_aside)
{
  RowCol findingIndices, vehPos;
  vehPos.first = vehRow;
  vehPos.second = vehCol;

  findingIndices = FrameRef2RowCol(vehPos,curState.Yaw,dist_ahead,dist_aside);
  return getRecentCellDataRC(findingIndices.first, findingIndices.second);
}

// Mutators
void genMap::setCellDataRC(int rowNumber, int colNumber, struct CellData newCD)
{
  if (rowNumber < 0 || rowNumber >= numRows) {
    cout << "row number input out of range: " << rowNumber << endl;
  }
  else if (colNumber < 0 || colNumber >= numCols) {
    cout << "col number input out of range: " << colNumber << endl;
  }
  else { 
    RowCol findingIndices(rowNumber, colNumber);
    if ( theMap.find(findingIndices) == theMap.end() ) {
      // The cell doesn't exist. Create and insert
      theMap.insert(map<RowCol, CellData>::value_type(findingIndices, newCD));
    }
    else {
      // DON'T UPDATE CORRIDOR CELL
      if(((theMap.find(findingIndices))->second).type == OUTSIDE_CORRIDOR_CELL )
      {
	newCD.type = OUTSIDE_CORRIDOR_CELL;
      }
      ((theMap.find(findingIndices))->second) = newCD;
    }

    // Updating timestamp
    ((theMap.find(findingIndices))->second).timeStamp = curTimeStamp;
  }
}

void genMap::setCellDataUTM(double Northing, double Easting, 
			    struct CellData newCD) 
{
  // From the left bottom UTM coord, find the row and col for the input coord.
  RowCol findingIndices = UTM2RowCol(Northing, Easting);
  setCellDataRC(findingIndices.first, findingIndices.second, newCD);
}

void genMap::setCellDataFrameRef(double dist_ahead, double dist_aside, 
				 struct CellData newCD) 
{
  RowCol findingIndices, vehPos;
  vehPos.first = vehRow;
  vehPos.second = vehCol;

  findingIndices = FrameRef2RowCol(vehPos,curState.Yaw,dist_ahead,dist_aside);
  setCellDataRC(findingIndices.first, findingIndices.second, newCD);
}

void genMap::setCellListDataUTM(double Northing, double Easting, 
				double list_val) 
{
  // From the left bottom UTM coord, find the row and col for the input coord.
  RowCol findingIndices = UTM2RowCol(Northing, Easting);
  setCellListDataRC(findingIndices.first, findingIndices.second, list_val);
}

void genMap::setCellListDataRC(int rowNumber, int colNumber, double list_val)
{
  if (rowNumber < 0 || rowNumber >= numRows) {
    //cout << "row number input out of range: " << rowNumber << endl;
  }
  else if (colNumber < 0 || colNumber >= numCols) {
    //cout << "col number input out of range: " << colNumber << endl;
  }
  else {
    RowCol findingIndices(rowNumber, colNumber);
    // If it's at the end of the map, then...
    if ( theMap.find(findingIndices) == theMap.end() ) {
      // Create and insert
      CellData newCD;
      pair<RowCol, CellData> insertCell(findingIndices, newCD);
      theMap.insert(insertCell);
    }
    
    // And then push the elevation value to the back of the list
    ((theMap.find(findingIndices))->second).cellList.push_back(list_val);
  }
}

void genMap::setCellListDataFrameRef(double dist_ahead, double dist_aside, 
				     double list_val)
{
  RowCol findingIndices, vehPos;
  vehPos.first = vehRow;
  vehPos.second = vehCol;
  findingIndices = FrameRef2RowCol(vehPos, curState.Yaw, dist_ahead, 
				   dist_aside);
  setCellListDataRC(findingIndices.first, findingIndices.second, list_val);
}


//////////////////////////////////////////////////////////////////////////////
// Cell Value Evaluation Methods for different Planners                     //
//////////////////////////////////////////////////////////////////////////////

void genMap::evaluateCellsSV(int numMinPoints, double verticalLimit) 
{
  double front, back, vehDOWN;
  RowCol vehPos;
  vehPos.first = vehRow;
  vehPos.second = vehCol;
  MapIterator it;
  int size;
  CellData tmpCell;

  // Cycle through all the cells
  for(int rowNumber=0; rowNumber < numRows; rowNumber++) {
    for(int colNumber=0; colNumber < numCols; colNumber++) {
      RowCol findingIndices(rowNumber, colNumber);
      //If the cell exists
      it = theMap.find(findingIndices);
      if ( it != theMap.end() ) {
	//If statement to determine whether or not to expire data
	//if((TVNow() - (it->second).timeStamp) > DataLifespan && (it->second).value != insert no data constant here) {

	  //Code to set cell to 0 - in effect, clearing the map
	  //(it->second).value = 0;
	  //Code to set cell to no data - in effect, clearing the map to no data
	  //(it->second).value = insert no data constant here;
	  
	//}

	/*
	//Below code is used to determine how many points are in
	//a given cell
	size = (it->second).cellList.size();
	if(size < 25) {
	  (it->second).value = 1;
	} else if(size < 50) {
	  (it->second).value = 2;
	} else if(size < 75) {
	  (it->second).value = 3;
	} else if(size < 100) {
	  (it->second).value = 4;
	} else {
	  (it->second).value = 5;
	}
	*/

        //If this cell's list is big enough
        if((it->second).cellList.size() > numMinPoints) {
	  //Sort the list of elevations from lowest to highest
	  (it->second).cellList.sort();
	
	  //Since +DOWN is downward, negative values are positive obstacles
  	  //So pick the highest point (the lowest value)
	  //Which is at the front of the list, and that's the value for the cel
	  //But we want to ignore values more than 10 feet=3.3m above the
	  //vehicle, so...
	  //vehDOWN = ((theMap.find(vehPos))->second).value;
	  front = (it->second).cellList.front();
	  back = (it->second).cellList.back();
	  //While the front value is less than -3.3 m
	  if(back < verticalLimit) {
	    (it->second).value = 0;
	    (it->second).cellList.clear();
	  } else {
	    while(front < verticalLimit) {
	      //Pop it off
	      (it->second).cellList.pop_front();
	      front = (it->second).cellList.front();
	    }
	    //Set the final value
            tmpCell.value = front;
            setCellDataRC(rowNumber,colNumber,tmpCell);
	    //(it->second).value = front;

	    //Below commented out code sets the average, not the max
	    //double sum = 0;
	    //int size = (it->second).cellList.size();
	    //while(!(it->second).cellList.empty()) {
	    //  sum += (it->second).cellList.front();
	    //  (it->second).cellList.pop_front();
	    //}
	    //(it->second).value = sum/((double) size);
	    //printf("Got (%d, %d, %lf)\n", rowNumber, colNumber, front);

	    //Set the timestamp of when we set it
	    //(it->second).timeStamp = TVNow().dbl();
	  }
	  //} else {
	  //(it->second) = NO_DATA_CELL;
	}

        //Don't forget to empty the list!
        if(!(it->second).cellList.empty()) {
	  (it->second).cellList.clear();
        }
      } // end cell exists
    }
  }
}


int genMap::serializedMapSize() 
{
  return 0;
}


int genMap::serializeMap(char* buffer, int bufferSize) 
{
  if(bufferSize >= serializedMapSize()) {
    double startRow, startCol, endRow, endCol;
    double rowStep, colStep;
    double val;

    startRow = -numRows/2*rowRes;
    endRow = numRows/2*rowRes;
    rowStep = rowRes;

    startCol = -numCols/2*colRes;
    endCol = numCols/2*colRes;
    colStep = colRes;

    //printf("Start: %lf, Stop: %lf, step: %lf\n", startRow, endRow, rowStep);

    for(double row = startRow; row <= endRow; row+=rowStep) {
      for(double col = startCol; col <= endCol; col+=colStep) {
	val = getCellDataFrameRef(row, col).value;
	//printf("At (%lf, %lf): %lf\n", row, col, val);

      }
    }
    
    return serializedMapSize();
  } else {
    return -1;
  }
  
  return -1;
}


//////////////////////////////////////////////////////////////////////////////
// Map Window/Frame Translation Methods                                     //
//////////////////////////////////////////////////////////////////////////////

/* *METHOD updateFrame
 *
 * +DESCRIPTION: Updates curState, vehRow, vehCol, clears out the data
 *               inside cells now "new" in the updated map. Also updates
 *               the cells outside the corridor, if asked. Further more
 *               it updates the last and next waypoint if those are given
 * +PARAMETERS
 *
 *  - newState      : the new state information
 *  - updateWindow  : if TRUE the window will slide on the memory, otherwise only
 *                     the car sweeps throught it
 *  - evalCorridor  : if TRUE the corridor will be painted
 *  - pLastWaypoint : index of the last waypoint
 *  - pNextWaypoint : index of the next waypoint
 *
 * +RETURN: 
 */
void genMap::updateFrame(struct VState_GetStateMsg newState, bool updateWindow, double nextTimeStamp,
			 int pLastWaypoint, int pNextWaypoint)
{
  double delta_north = newState.Northing - curState.Northing;
  double delta_east = newState.Easting - curState.Easting;
  double floor_delta_north, floor_delta_east;
  RowCol new_point,old_point,ind_point,delta_point;
  RowCol old_leftBottom_point, new_leftBottom_point, aux_point;
  RowCol orignCarPos, frontCarPos;
  RowCol startPaint, deltaPaint;
  pair<double,double> NorthEast;
  pair<bool,double> pointLine;
  bool crossTrackLine;
  NorthEastDown a,b,point;
  int delta_row, delta_col;
  int sgnDeltaNorth = 1,sgnDeltaEast = 1;

  // Updating time stamp
  curTimeStamp = nextTimeStamp;

  // Updating the waypoints WE ARE CHECKING INSIDE GENMAP !! DON'T NEED TO RECEIVE FROM OUTSIDE
  //############################################################
  // ### PAINTCORRIDOR ONLY USES LAST,CURRENT,NEXT WAYPOINTS TO CHECK IF INSIDE
  // ### CORRIDOR, SO WE NEED TO MAKE SURE WAYPOINTS ARE UPDATED CORRECTLY
  // ### IS THIS DONE SOMEWHERE???
  //############################################################
  //if(pLastWaypoint)
  //  lastWaypoint = pLastWaypoint;
  //if(pNextWaypoint)
  //  nextWaypoint = pNextWaypoint;

  // FINDING THE FRONT POSITION OF THE CAR
  orignCarPos.first = vehRow;
  orignCarPos.second = vehCol;
  frontCarPos = FrameRef2RowCol(orignCarPos, curState.Yaw, 
				GPS_2_FRONT_DISTANCE, 0);
  NorthEast = RowCol2UTM(frontCarPos);
  // ADDING THE ACTUAL DISPLACEMENT TO HAVE TO MOST UPDATED POSITION OF THE
  // FRONT OF THE CAR
  NorthEast.first += delta_north;
  NorthEast.second += delta_east;
  // CHECKING IF THE CAR REACHED THE NEXT WAYPOINT OF THE NEXT CORRIDOR, IF YES
  // UPDATE WAYPOINTS
  // Point
  if( (evalCorridor) && (nextWaypoint < numTargetPoints) ){
    crossTrackLine = false;
    if((nextWaypoint) && (nextWaypoint < (numTargetPoints-1)) ){
      point.Northing = NorthEast.first;
      point.Easting = NorthEast.second;
      point.Downing = 0;
      // Last Waypoint
      a.Northing = targetPoints[nextWaypoint].Northing;
      a.Easting = targetPoints[nextWaypoint].Easting;
      a.Downing = 0;
      // Next waypoint
      b.Northing = targetPoints[nextWaypoint+1].Northing;
      b.Easting = targetPoints[nextWaypoint+1].Easting;
      b.Downing = 0;

      // Distance between point and lines
      pointLine = distancePoint2Line(a,b,point);
      crossTrackLine = pointLine.first;
    }
    if( crossTrackLine || 
        (isInsideWaypointRadius(nextWaypoint,NorthEast)) ){ 
      lastWaypoint = nextWaypoint;
      if(nextWaypoint+1 < numTargetPoints){
        nextWaypoint = targetPoints[nextWaypoint+1].number;
      }
      else{
        nextWaypoint = numTargetPoints;
      }
    }
  }
  // GETTING THE SIGNAL OF THE UTM DISPLACEMENT BETWEEN THE OLD CAR POSITION AND THE NEW ONE
  if(delta_north < 0)
    sgnDeltaNorth *= -1;
  if(delta_east < 0)
    sgnDeltaEast *= -1;

  // UPDATING THE STATE
  Copy_VState(newState);  
  
  // IF THE DISPLACEMENT IS BIGGER THAN THE MAP, ERARE ALL LINES AND REDRAW CORRIDOR FOR ALL LINES
  // IF WE REACHED THE NEXT WAYPOINT ALSO, OTHERWISE WE WOULD HAVE EXPIRED CORRIDOR ON THE PATH
  if( (sgnDeltaEast*delta_east > (numCols-1)*colRes) || 
      (sgnDeltaNorth*delta_north > (numRows-1)*rowRes) )
  {
    // Vehicle position in indices
    if(floor((double)numRows/2)  < (double)numRows/2){
      vehRow = (int)floor((double)numRows/2);
    }
    else{
      vehRow = (int)floor((double)numRows/2) -1;
    }
    if(floor((double)numCols/2)  < (double)numCols/2){
      vehCol = (int)floor((double)numCols/2);
    }
    else{
      vehCol = (int)floor((double)numCols/2) -1;
    }

    // Calculate left bottom UTM coordinates
    leftBottomRow = 0;
    leftBottomCol = 0;
    if(updateWindow){
      leftBottomUTM_Northing = curState.Northing - 
	(double)vehRow*rowRes - rowRes/2;
      leftBottomUTM_Easting  = curState.Easting - 
	(double)vehCol*colRes - colRes/2;
      clearMap();                              
      if(evalCorridor){
	// Paiting the corridor
        startPaint.first = leftBottomRow;
        startPaint.second = leftBottomCol;
        deltaPaint.first = numRows;
        deltaPaint.second = numCols;
        paintCorridor(startPaint,deltaPaint);
      }
    }
  }
  //WE STILL CAN KEEP PART OF THE MAP AND UPDATE JUST THE NEW LINES
  else{
    // OLD <ROW,COL> OF THE CAR => old_point
    old_point.first = vehRow;
    old_point.second = vehCol;
    // OLD <ROW,COL> OF THE LEFT BOTTOM CORNER => old_leftBottom_point
    old_leftBottom_point.first = leftBottomRow;
    old_leftBottom_point.second = leftBottomCol;

    // THE DISPLACEMENT IS DECOUPLED AS: d = m + r
    //     - m is the part of the displacement multiple by the resolution (floor_delta_north,floor_delta_east)
    //     - r is the remaining 
    floor_delta_north = sgnDeltaNorth * floor(fabs(delta_north/rowRes));
    floor_delta_east = sgnDeltaEast * floor(fabs(delta_east/colRes));

    // NEW <ROW,COL> OF THE CAR IF THERE IS NO REMAINING
    new_point = RowColFromDeltaPosition(old_point, floor_delta_north * rowRes,
					floor_delta_east * colRes);
    // NEW <ROW,COL> OF THE LEFT BOTTOM CORNER IF THERE IS NO REMAINING
    new_leftBottom_point = RowColFromDeltaPosition(old_leftBottom_point,
						   floor_delta_north*rowRes,
						   floor_delta_east*colRes);

    // NEW CELL UTM COORDINATES OF THE CAR IF THERE IS NO REMAINING
    // (we have the real UTM coordinates of the car at curState.Northing, curState.Easting)
    // Finding the center of the cell in UTM
    NorthEast = RowCol2UTM(old_point);
    NorthEast.first  += floor_delta_north*rowRes;
    NorthEast.second += floor_delta_east*colRes;

    /* DEBUG
    cout << "veh    old position = (" << old_point.first << "," << old_point.second << ") new position = (" << new_point.first << "," << new_point.second << ")" << endl;    
    cout << "detla_north/rowRes(" << detla_north/rowRes << ") floor(ans)(" << floor(abs(detla_north/rowRes)) << ")" <<  endl;
    cout << "delta_east/colRes(" << delta_east/colRes << ") floor(ans)(" << floor(abs(delta_east/colRes)) << ")" <<  endl;
    cout << "curEasting = " << curState.Easting
         << "cell_Northing = " << NorthEast.first << " rowRes/2 = " << rowRes/2 << endl;
    cout << "curNorthing = " << curState.Northing
         << "cell_Easting = " << NorthEast.second << " colRes/2 = " << colRes/2 << endl;
    cout << endl << endl; 
    */

    // FYI: THE CAR IS NOT ALWAYS IN THE MIDDLE OF A CELL
    // ANALYSING r TO SEE IF WE NEED TO MOVE THE CAR TO ANY OF THE NEIGHBOOR CELLS
    // WE WILL HAVE AT MOST 1 CELL DISPLACEMENT (because it's the remaining of the resolution)
    if( (curState.Northing) < (NorthEast.first - rowRes/2) ){
      ind_point.first = -1;
    }
    if( (curState.Northing) > (NorthEast.first + rowRes/2) ){
      ind_point.first = +1;
    }
    if( (curState.Easting) < (NorthEast.second - colRes/2) ){
      ind_point.second = -1;
    }
    if( (curState.Easting) > (NorthEast.second + colRes/2) ){
      ind_point.second = 1;
    }

    // NEW <ROW,COL> OF THE CAR AFTER ANALYSED THE REMAINING
    new_point = RowColFromDeltaRowCol(new_point,ind_point);
    // NEW <ROW,COL> OF THE LEFT BOTTOM CORNER AFTER ANALYSED THE REMAINING
    new_leftBottom_point = RowColFromDeltaRowCol(new_leftBottom_point, 
						 ind_point);
    // IF THE CAR MOVES, THE LEFT BOTTOM CORNER MOVES
    // THE ROWS/COLS THEY MOVED HAS TO BE ERASED AND PAINTED
    if(updateWindow){

      /*********************************/
      // WHICH ROW DO WE HAVE TO UPDATE?
      /*********************************/
      ind_point.first = leftBottomRow; // old left bottom row
      ind_point.second = leftBottomCol;// old left bottom col
      delta_point.first = 1*sgnDeltaNorth;
      delta_point.second = 0;
      startPaint.first = 0; startPaint.second = 0;
      deltaPaint.first = 0; deltaPaint.second = 0;

      if( (sgnDeltaNorth < 0)   // IF THE DISPLACEMENT TO THE IS SOUTH
          && 
	  // IF WE REALLY HAVE TO JUMP THE ROW
          (new_leftBottom_point.first != leftBottomRow) 
      ){
        // AS LONG AS THE ORIGN OF THE MAP IS LEFT_BOTTOM, THE FIRST ROW TO BE ERASED
        // WILL NEVER BE THE ROW YOU USED TO BE
        ind_point = RowColFromDeltaRowCol(ind_point,delta_point); 
        aux_point = RowColFromDeltaRowCol(new_leftBottom_point,delta_point); 
      }
      else{
        // IF THE DISPLACEMENT IS POSITIVE THE ROW WE WERE HAS TO BE ERASED
        aux_point = new_leftBottom_point; 
      }

      // THE ROW WE ERASE WILL HAVE TO BE PAINTED -- good idea
      startPaint.first = ind_point.first;
      while(ind_point.first != aux_point.first){
        // debug
        //cout << "ERASING THE ROW " << ind_point.first << endl;
        clearRow(ind_point.first);
	leftBottomUTM_Northing += rowRes*sgnDeltaNorth;
        ind_point = RowColFromDeltaRowCol(ind_point,delta_point);
        deltaPaint.first = deltaPaint.first + sgnDeltaNorth;
      }

      /*********************************/
      // WHICH COL DO WE HAVE TO UPDATE?
      /*********************************/
      ind_point.first = leftBottomRow; // old left bottom row
      ind_point.second = leftBottomCol;// old left bottom col
      delta_point.first = 0;
      delta_point.second = 1*sgnDeltaEast;

      if( (sgnDeltaEast < 0)      // IF THE DISPLACEMENT TO THE IS WEST
          && 
	  // IF WE REALLY HAVE TO JUMP THE COL
          (new_leftBottom_point.second != leftBottomCol) 
      ){
        // AS LONG AS THE ORIGN OF THE MAP IS LEFT_BOTTOM, THE FIRST COL TO BE ERASED
        // WILL NEVER BE THE COL YOU USED TO BE
        ind_point = RowColFromDeltaRowCol(ind_point,delta_point); 
        aux_point = RowColFromDeltaRowCol(new_leftBottom_point,delta_point); 
      }
      else{
        // IF THE DISPLACEMENT IS POSITIVE THE COL WE WERE HAS TO BE ERASED
        aux_point = new_leftBottom_point; 
      }

      startPaint.second = ind_point.second;
      while(ind_point.second != aux_point.second){
        // debug
        //cout << "ERASING THE COL " << ind_point.second << endl;
        clearCol(ind_point.second);
	leftBottomUTM_Easting += colRes*sgnDeltaEast;
        ind_point = RowColFromDeltaRowCol(ind_point,delta_point);
        deltaPaint.second = deltaPaint.second + sgnDeltaEast;
      }

      // UPDATING NEW LEFT BOTTOM <ROW,COL>
      leftBottomRow = new_leftBottom_point.first;
      leftBottomCol = new_leftBottom_point.second;
    }
    
    // Painting corridor on the new ros/col's after wrap arround
    if( (updateWindow) && (evalCorridor) ){
      // DEBUG
      /*
      cout << "Waypoints(" << lastWaypoint << "," << nextWaypoint << ")" << endl;
      cout << "DELTA(N,E) = (" << delta_north << "," << delta_east << ")" << endl;
      cout << "LEFTBOTTOM OLD(" << old_leftBottom_point.first << "," << old_leftBottom_point.second 
           << ")   NEW(" << new_leftBottom_point.first << "," << new_leftBottom_point.second << ")" << endl;          
      cout << "PAINT    START(" << startPaint.first << "," << startPaint.second 
           << ") DELTA(" << deltaPaint.first << "," << deltaPaint.second << ")" << endl << endl;
      */
      paintCorridor(startPaint, deltaPaint);
    }

    // UPDATING NEW CAR <ROW,COL>
    vehRow = new_point.first;
    vehCol = new_point.second;
  }

  // Thread locking?!!!!!?????!!!!!

  // update rotation matrix to reflect current state
  //framematrix.setattitude(curState.Pitch, curState.Roll, curState.Yaw); 
}

void genMap::clearMap()
{
  theMap.clear();
}

void genMap::clearColor(int colorIndex)
{
  RowCol ind;
  cout << "genMap::clearColor NOOOOOOOOOOO !!!!!!!!!!" << endl;
  for(int row = 0; row < numRows; row++){
    for(int column = 0; column < numCols; column++){
      ind.first = row%numRows;
      ind.second = column%numCols;
      // Just for the presentation
      if(theMap.find(ind) != theMap.end()){
        if ((theMap[ind]).color == colorIndex){
          (theMap[ind]).color = 0;
        }
      }
    }
  }
}

void genMap::setCurrentWaypoints()
{
}

void genMap::clearRow(int row)
{
  RowCol pos;
  for(int column = 0; column < numCols; column++){
    pos.first = row%numRows; 
    pos.second = column;
    if(theMap.find(pos) != theMap.end())
      theMap.erase(pos);
  }  
}

void genMap::clearCol(int col)
{
  RowCol pos;
  for(int row = 0; row < numRows; row++){
    pos.first = row;
    pos.second = col%numCols;
    if(theMap.find(pos) != theMap.end()) 
      theMap.erase(pos);
  }
}


//////////////////////////////////////////////////////////////////////////////
// Frame Transformation Methods                                             //
//////////////////////////////////////////////////////////////////////////////

// Shape the goodness for the case we are outside of the corridor
/* double genMap::goodnessShape(int side, int indArc, int numArcs, double scale)
 * +DESCRIPTION: This functions returns a goodness of a given steering angle
 *               for the votes based on the vector field to the next waypoint
 * +PARAMETERS
 *   - side    : where you want to turn, LEFT_SIDE for left and RIGHT_SIDE for right
 *   - indArc  : index of the arc
 *   - numArcs : number of arcs
 *   - scale   : this must be a internal product between the actual direction and
 *     [0 1]     the desired one. Used to scale how hard to turn the steering wheels
 * +RETURN: 
 *   - goodness : goodnes evaluated for that arc
 */
double genMap::goodnessShape(int side, int indArc, int numArcs, double scale)
{
  double a,b,c;    // y = a(x-m)^2 + b(x-m) + c ; y' = 2a(x-m) + b
  double y,m;

  if(numArcs){
    m = ((double)(-side*(-scale+1)))*(numArcs)/4;
    a = -2*((double)(MAX_GOODNESS_SHAPE - MIN_GOODNESS_SHAPE))/(((double)(numArcs-1)/2)*((double)(numArcs-1)/2));
    b = -2*a*m;
    c = (double)MAX_GOODNESS_SHAPE - a*m*m - b*m;
    y = a*(indArc-((double)numArcs)/2)*(indArc-((double)numArcs)/2) +
        b*(indArc-((double)numArcs)/2) + c;
    if(y < MIN_GOODNESS_SHAPE) {
      y = (double)MIN_GOODNESS_SHAPE;
    }
  }
  else{
    y = 0;
  }
  //cout << " (a,b,c,m) = (" << a << "," << b << "," << c << "," << m << ")"
  //     << " y = " << y << endl;
  return(y);
}

// Shape the goodness for the case we are outside of the corridor
/* double genMap::shrinkObstacle(double value,double deltaT)
 * +DESCRIPTION: This functions returns a goodness of a given steering angle
 *               for the votes based on the vector field to the next waypoint
 * +PARAMETERS
 *   - side    : where you want to turn, LEFT_SIDE for left and RIGHT_SIDE for right
 *   - indArc  : index of the arc
 *   - numArcs : number of arcs
 *   - scale   : this must be a internal product between the actual direction and
 *     [0 1]     the desired one. Used to scale how hard to turn the steering wheels
 * +RETURN: 
 *   - goodness : goodnes evaluated for that arc
 */
double genMap::shrinkObstacle(double value,double deltaT){
  double y,param;

  param = MIN_EXP_SHRINK_PARAMETER;
  param += curState.Speed*((double)(MAX_EXP_SHRINK_PARAMETER-MIN_EXP_SHRINK_PARAMETER))/((double)MAX_SPEED);
  //cout << "delta T = " << dT << " param*dT = " << -(double)EXP_SHRINK_PARAMETER*dT 
  //     << " exp() = " << exp(-((double)EXP_SHRINK_PARAMETER)*deltaT) << endl;
  //cout << " ret.value = " << ret.value << endl;
  y = value*exp(-param*deltaT);
  return(y);
}

// Shape the goodness for obstacle avoidance, in case all is 100 we don't make circles
/* double genMap::obstacleGoodnessShape(int indArc, int numArcs)
 * +DESCRIPTION: This functions returns a goodness of a given steering angle
 *               for the votes based on the obstacle avoidance
 * +PARAMETERS
 *   - indArc  : index of the arc
 *   - numArcs : number of arcs
 * +RETURN: 
 *   - goodness(%) [95 100]
 */
double genMap::obstacleGoodnessShape(int indArc, int numArcs)
{
  double a1,b1,a2,b2;    // y = ax + b
  double y, m;

  if(numArcs){ 
    m = ((double)(numArcs-1))/2;
    a1 = ((double)(MAX_OBSTACLE_AVOIDANCE_GOODNESS-MIN_OBSTACLE_AVOIDANCE_GOODNESS))/m;
    a2 = -a1;
    b1 = (double)MIN_OBSTACLE_AVOIDANCE_GOODNESS;
    b2 = (double)MAX_OBSTACLE_AVOIDANCE_GOODNESS;
    if(indArc <= m)
      y = a1*indArc + b1;
    else
      y = a2*(indArc-m) + b2;
    if(y < MIN_OBSTACLE_AVOIDANCE_GOODNESS) {
      y = (double)MIN_OBSTACLE_AVOIDANCE_GOODNESS;
    }
  }
  else{
    y = 0;
  }
  //cout << " (a1,a2,m) = (" << a1 << "," << a2 << "," << m << ")"
  //     << " y = " << y << endl;
  return(y);
}

// Shape the velociy to reduce the speed when the steering wheel is turned hard
/* double genMap::velShape(int indArc, int numArcs)
 * +DESCRIPTION: This functions returns a percentage of the given velocity,
 *               when the car is going straigth we return the velocity passed
 *               when it is turning the velocity is reduced in a quadratic way.
 *               Full right and full left returns 20% of the original one.
 * +PARAMETERS
 *   - oldVel : velocity
 * +RETURN: 
 *   - newVel : after shaping [0.2*oldVel   oldVel]
 */
double genMap::velShape(int indArc, int numArcs, double vel)
{
  double a,b,c;    // y = ax^2 + bx + c ; y' = 2ax + b
  double y,percent;

  if(numArcs){
    a = -((double)(PERCENT_MAX_VEL - PERCENT_MIN_VEL)) / 
         (((double)(numArcs-1)/2)*((double)(numArcs-1)/2));
    b = -a*((double)(numArcs-1));
    c = (double)PERCENT_MIN_VEL;
    percent = a*indArc*indArc + b*indArc + c;
  }
  else{
    percent = (double)PERCENT_MAX_VEL;
  }
  //cout << " (a,b,c) = (" << a << "," << b << "," << c << ")"
  //     << " percent = " << percent << endl;
  y = (percent/100)*vel;
  return(y);
}

// Shape the costs returned by the cost methods
/* double genMap::costShape(double x, double alpha)
 * +DESCRIPTION: This function shape the cost in order to have more resoluition
 *               nearby 100 rarther than 0 
 *    For now it's an exponential function
 * +PARAMETERS
 *   - x    : the point to be mapped [0 1]
 *   - alpha: the exp parameter
 * +RETURN: 
 *   - y    : after mapping [0 100]
 */
double genMap::costShape(double x, double alpha)
{
  double y;
  y = (MAX_VOTE_RANGE - MIN_VOTE_RANGE)*(exp(alpha*x) - 1)/(exp(alpha) - 1) + 
       MIN_VOTE_RANGE;
  //cout << "costShape(" << x << "," << alpha << ") = " << y << endl;
  return(y);
}

// Evaluate normal vectors
/* NorthEastDown evalNormalVector(NorthEastDown point_a,NorthEastDown point_b,NorthEastDown point_c);
 * +DESCRIPTION: This method receive the three points that define the plane
 *    and return the vector normal to it
 * +PARAMETERS
 *   - point_a,point_b,point_c: north east and down  coordinates in UTM of the points inside the plane
 * +RETURN: north, east and down coordinates of the normal vector
 */

NorthEastDown genMap::evalNormalVector(NorthEastDown point_a,
				       NorthEastDown point_b,
				       NorthEastDown point_c)
{
  NorthEastDown normal,vec_a,vec_b;

  vec_a = point_a - point_b;
  vec_b = point_b - point_c;
  normal = vec_a^vec_b;
  normal.norm();

  return(normal);
}

// Evaluate cost associated with normal vectors
/* double evalNormalVectorCost(NorthEastDown normal_a, NorthEastDown normal_b, NorthEastDown normal_c, NorthEastDown normal_d)
 * +DESCRIPTION: This method receive the four normal vectors to the planes formed by three of the
 *    four tires and return a cost associated If a pair of vectors is more that VECTOR_PROD_THRESHOLD
 *    the generated vote will be zero. If not the vote will be modeled by an exponential in order to
 *    enfasize little variation arround the max goodness
 * +PARAMETERS
 *   - NorthEastDown normal_a, normal_a, normal_a, normal_a: normal vectors coordinates in UTM
 * +RETURN: cost on the range [0 100]
 */
double genMap::evalNormalVectorCost(NorthEastDown *n)
{
  double int_prod[6]; // range [0 1]
  double cost, min_int_prod, mean_int_prod;
  NorthEastDown down;
  int ind,i,j;

  // Assigning the down vector
  down.Northing = 0;
  down.Easting = 0;
  down.Downing = 1.0;
  // Getting the norm
  for(ind=0;ind < 4;ind++){
    n[ind].norm();
  }

  // Internal product
  /*
  ind=0;
  for(i=0;i < 3;i++){
    for(j=(i+1);j < 4;j++){
      if(i!=j){
        // Debug
        //cout <<" n[" << i << "] (north,east,down) = (" << n[i].Northing << ","  << n[i].Downing << ","  << n[i].Downing << ")" << endl; 
        //cout <<" n[" << j << "] (north,east,down) = (" << n[j].Northing << ","  << n[j].Downing << ","  << n[j].Downing << ")" << endl; 
        if( ( (n[i].Northing) && (n[i].Easting) && (n[i].Downing) ) && ( (n[j].Northing) && (n[j].Easting) && (n[j].Downing) ) )
          int_prod[ind] = abs(n[i]*n[j]); // <n[i],n[j]>
        else
          int_prod[ind] = 1;
        //cout << "int_prod[" << ind << "] = n[" << i << "] dot n[" << j << "] = " << int_prod[ind] << endl;
        ind++;
      }
    }
  }

  // Finding whether one of the vectors is beyong the threshold
  min_int_prod = 2.0;
  mean_int_prod = 0.0;
  for(ind=0; ind<6; ind++){
    if(int_prod[ind] < min_int_prod){
      min_int_prod = int_prod[ind];
    }
    mean_int_prod += int_prod[ind];
  }
  if(min_int_prod < vectorProdThreshold){
    cost = 0;
  }
  else{
    mean_int_prod /= 6;
    cost = costShape(mean_int_prod,EXP_PARAM);
  }
  */
  
  // Internal product between a vector and the one point down
  for(i=0;i < 4;i++){
    if( ( (n[i].Northing) && (n[i].Easting) && (n[i].Downing) ) )
      int_prod[i] = abs(n[i]*down); // <n[i],down>
    else
      int_prod[i] = 1;
    //cout << "int_prod[" << i << "] = n[" << i << "] dot downn = " << int_prod[ind] << endl;
  }

  // Finding whether one of the vectors is beyong the threshold
  min_int_prod = 2.0;
  mean_int_prod = 0.0;
  for(ind=0; ind<4; ind++){
    if(int_prod[ind] < min_int_prod){
      min_int_prod = int_prod[ind];
    }
    mean_int_prod += int_prod[ind];
  }
  if(min_int_prod < vectorProdThreshold){
    cost = 0;
  }
  else{
    mean_int_prod /= 4;
    cost = costShape(mean_int_prod,EXP_PARAM);
  }

  return(cost);
}

// Evaluate the distance betwwen a point and a plane in R^3
/* double distancePoint2Plane(NorthEastDown onPlane_a, NorthEastDown onPlane_b, NorthEastDown onPlane_c, NorthEastDown outPlane);
 * +DESCRIPTION: This method receive the three points that define the plane
 *    and a point outside it and returns the minimum distance between them
 * +PARAMETERS
 *   - onPlane_a, onPlane_b, onPlane_c: points on the plane that defines it
 *   - outPlane: point outside the plane
 * +RETURN: the distance of the projection
 */
double genMap::distancePoint2Plane(NorthEastDown a, NorthEastDown b, NorthEastDown c, NorthEastDown Po)
{
  NorthEastDown n;            // normal vector to the plane
  NorthEastDown Pn;           // intersection between the plane and the line
  double d;         // independent term the the plane equation a.north + b.east + c.down = d
  double to;        // parameter of the parametrized line equation in R^3 (v = to*w + wo)
  double distance;

  n = evalNormalVector(a, b, c);
  n.norm();
  d = (n.Northing * a.Northing) + (n.Easting * a.Easting) + (n.Northing * a.Downing);  
  to= d - ( (n.Northing * Po.Northing) + (n.Easting * Po.Easting) + (n.Downing * Po.Downing) );
  Pn = Po + n*to;
  distance = sqrt((Pn-Po)*(Pn-Po));

  return(distance);
}

// Evaluate the distance betwwen a point and a plane in R^3
/* double distancePoint2Line(NorthEastDown onLine_a, NorthEastDown onLine_b, NorthEastDown outLine);
 * +DESCRIPTION: This method receive the two points that define the line
 *    and a point outside it and returns the minimum distance between them
 * +PARAMETERS
 *   - a, b: points on the line that defines it
 *   - Po: point outside the line
 * +RETURN: the distance of the projection
 */
pair<bool,double> genMap::distancePoint2Line(NorthEastDown a, NorthEastDown b, 
					     NorthEastDown Po)
{
  NorthEastDown v;           // vector on the line direction
  NorthEastDown n;           // vector normal to the line through Po
  NorthEastDown Pl;          // point on the line resulted from the projection
  double to;       // line parametrization in terms of to
  double lineLen; // the compriment on the line
  double dist_a2Pl, dist_b2Pl;
  double distance;
  bool doesItCross=false;
  pair <bool,double> ret;

  v = a - b;
  lineLen = sqrt((b.Northing-a.Northing)*(b.Northing-a.Northing) + (b.Easting-a.Easting)*(b.Easting-a.Easting) + (b.Downing-a.Downing)*(b.Downing-a.Downing) );
  v.norm();
  to = -( v.Northing*(a.Northing - Po.Northing) + v.Easting*(a.Easting - Po.Easting) + v.Downing*(a.Downing - Po.Downing) );
  Pl = v*to + a;
  dist_a2Pl = sqrt((a.Northing-Pl.Northing)*(a.Northing-Pl.Northing) + (a.Easting-Pl.Easting)*(a.Easting-Pl.Easting) + (a.Downing-Pl.Downing)*(a.Downing-Pl.Downing) );
  dist_b2Pl = sqrt((b.Northing-Pl.Northing)*(b.Northing-Pl.Northing) + (b.Easting-Pl.Easting)*(b.Easting-Pl.Easting) + (b.Downing-Pl.Downing)*(b.Downing-Pl.Downing) );
  if( (dist_a2Pl <= lineLen) && (dist_b2Pl <= lineLen) ){
    doesItCross = true;    
  }
  n = Pl - Po;
  distance = sqrt(n*n);
  ret.first  = doesItCross;
  ret.second = distance;
  return(ret);
}

// Evaluate cost associated with the tire off the plane determined by the other three
/* double evalOffTireCost(double dist[4]);
 * +DESCRIPTION: This method receive the four distances from the tire to a plane determined by the other 
 *    three on the ground and evaluate a cost associated with it
 * +PARAMETERS
 *   - dist[4]: distances from a point to a plane
 * +RETURN: cost on the range [0 100]
 */
double genMap::evalOffTireCost(double *dist)
{
  int ind;
  double cost, param;
  double max_dist;

  max_dist = 0;
  for(ind=0; ind<4; ind++){
    if(dist[ind] > max_dist){
      max_dist = dist[ind];
    }
  }
  
  if(max_dist > tireOffThreshold){
    cost = 0;
  }
  else{
    max_dist /= tireOffThreshold;
    param = 1-max_dist;
    cost = costShape(param,EXP_PARAM);
  }
  return(cost);
}

// Evaluate each the cells the car would be if pointing to pYaw rad and on a certain row/col on the map
/* map<RowCol,RowCol> allCellsUnder(RowCol pos,double pYaw);
 * +DESCRIPTION: This method receive the position and yaw of the car and returns all the cells under it
 *
 * +PARAMETERS
 *   - RowCol position: row and col of the orign of the car
 *   - double pYaw: heading the car is pointing
 * +RETURN: a cost [0 100]
 */
double genMap::allCellsUnderCost(RowCol position,double pYaw)
{
  double lowerRes, delta_dist, dist_ahead, dist_aside;
  double cost, param;
  double height_diff;
  double max_height_diff; // the biggest height difference between cells
  RowCol cellUnderPos, findPos, deltaPos;
  CellData cell, findCell;
  int col_addition = 1, diagonal_addition = 1,row_addition = 1; 
  bool obstacleFound = false, full_cell_found = false;

  // Evaluating the smaller resolution
  lowerRes = colRes;
  if (rowRes < lowerRes)
    lowerRes = rowRes;

  // Evaluating the step of the line ahead
  delta_dist = 3*lowerRes/4;

  // Getting all cells under the car
  dist_ahead = VEHICLE_LENGTH*growVehicleLength + delta_dist;
  max_height_diff = 0;
  while( (!obstacleFound) && (dist_ahead >= -delta_dist) ){
    // Varying the car width ...
    dist_aside = -VEHICLE_WIDTH*growVehicleWidth/2;
    while( (!obstacleFound) && (dist_aside <= VEHICLE_WIDTH*growVehicleWidth/2 + delta_dist) ) {

      // Finding the RowCol of the cell
      cellUnderPos = FrameRef2RowCol(position,pYaw, dist_ahead, dist_aside);
      //debug
      cell = getCellDataRC(cellUnderPos.first,cellUnderPos.second);
      // Corridor found !!!
      if(cell.type == OUTSIDE_CORRIDOR_CELL)
        obstacleFound = true;
      if(cell.type == NO_DATA_CELL.type)
	{
	  height_diff = UNKNOWN_CELL_HEIGHT_DIFF;
          if(height_diff > max_height_diff){
            max_height_diff = height_diff;
          }
	}
      else
	{
	  full_cell_found = false;
	  for(col_addition = 1; ((col_addition <= NUMBER_OF_COLS_RIGHT_TO_CHECK)&&(full_cell_found == false)); col_addition++)
	    {
	      deltaPos.first = 0; deltaPos.second = col_addition;
	      findPos = RowColFromDeltaRowCol(cellUnderPos, deltaPos);
	      //debug
	      //cout << "(" << findPos.first << "," << findPos.second << ") "; 
	      // Getting the height of the adjacent cell
	      findCell = getCellDataRC(findPos.first,findPos.second);
	      if(findCell.type == NO_DATA_CELL.type)
		{
		  height_diff = UNKNOWN_CELL_HEIGHT_DIFF;
		  full_cell_found = false;
		}
	      else
		{
		  height_diff = fabs(cell.value - findCell.value);
		  full_cell_found = true;
		}
	      if(height_diff > max_height_diff){
		max_height_diff = height_diff;
	      }
	    }
          full_cell_found = false;
          for(diagonal_addition = 1; ((diagonal_addition <= NUMBER_OF_CELLS_DIAGONAL_TO_CHECK)&&(full_cell_found == false)); diagonal_addition++)
            {
              deltaPos.first = diagonal_addition; deltaPos.second = diagonal_addition;
              findPos = RowColFromDeltaRowCol(cellUnderPos, deltaPos);
              //debug
              //cout << "(" << findPos.first << "," << findPos.second << ") ";
              // Getting the height of the adjacent cell
              findCell = getCellDataRC(findPos.first,findPos.second);
              if(findCell.type == NO_DATA_CELL.type)
                {
                  height_diff = UNKNOWN_CELL_HEIGHT_DIFF;
                  full_cell_found = false;
                }
              else
                {
                  height_diff = fabs(cell.value - findCell.value);
                  full_cell_found = true;
                }
              if(height_diff > max_height_diff){
                max_height_diff = height_diff;
              }
            }
	  full_cell_found = false;
          for(row_addition = 1; ((row_addition <= NUMBER_OF_ROWS_DOWN_TO_CHECK)&&(full_cell_found == false)); row_addition++)
            {
              deltaPos.first = row_addition; deltaPos.second = 0;
              findPos = RowColFromDeltaRowCol(cellUnderPos, deltaPos);
              //debug
              //cout << "(" << findPos.first << "," << findPos.second << ") ";
              // Getting the height of the adjacent cell
              findCell = getCellDataRC(findPos.first,findPos.second);
              if(findCell.type == NO_DATA_CELL.type)
                {
                  height_diff = UNKNOWN_CELL_HEIGHT_DIFF;
                  full_cell_found = false;
                }
              else
                {
                  height_diff = fabs(cell.value - findCell.value);
                  full_cell_found = true;
                }
              if(height_diff > max_height_diff){
                max_height_diff = height_diff;
              }//end max_height_diff_check
            }//end for loop through rows
    	}//end else for cell not empty
      if(max_height_diff > heightDiffThreshold){
	obstacleFound = true;
	//debug
	//cout << "Found obstacle\n";
	//cout << "(" << cellUnderPos.first << "," << cellUnderPos.second << ") = " << cell.value << endl;
	//cout << "(" << findPos.second << "," << findPos.second << ") = " << findCell.value << endl;
      }
      
      // debug obstacleAvoidance
      //(theMap[cellUnderPos]).color = BLUE_INDEX;//Is this still in use?
      

      dist_aside += delta_dist;
    }
    dist_ahead -= delta_dist;
  }
  // Cost ...
  // if there is a high height difference, don't go there
  param = 1 - max_height_diff/heightDiffThreshold;
  if( (obstacleFound) || (param < 0) ){
    cost = 0;
  }
  else{
    cost = costShape(param,EXP_PARAM);
  }

  return(cost);
}

// Evaluate each the cells the car would be if pointing to pYaw rad and on a certain row/col on the map
/* double maxHeightDiffCost(RowCol pos,double pYaw);
 * +DESCRIPTION: This method receive the position and yaw of the car and returns a cost based on height
 *
 * +PARAMETERS
 *   - RowCol position: row and col of the orign of the car
 *   - double pYaw: heading the car is pointing
 * +RETURN: a cost [0 100]
 */
double genMap::maxHeightDiffCost(RowCol position,double pYaw)
{
  double lowerRes, delta_dist, dist_ahead, dist_aside;
  double cost, param;
  double min_height, max_height;
  double height_diff,max_height_diff; // the biggest height difference between cells
  RowCol cellUnderPos, findPos, deltaPos;
  CellData cell, findCell;
  bool obstacleFound = false;
  bool invalidDataCell = false;
  int numValidCellsFound = 0;

  // Evaluating the smaller resolution
  lowerRes = colRes;
  if (rowRes < lowerRes)
    lowerRes = rowRes;

  // Evaluating the step of the line ahead
  delta_dist = lowerRes;

  // Getting all cells under the car
  dist_ahead = VEHICLE_LENGTH*growVehicleLength;
  max_height_diff = 0;
  max_height = -9999;
  min_height = 9999;
  while( (!obstacleFound) && (dist_ahead >= 0) ){
    // Varying the car width ...
    dist_aside = -VEHICLE_WIDTH*growVehicleWidth/2;
    while( (!obstacleFound) && (dist_aside <= VEHICLE_WIDTH*growVehicleWidth/2) ) {

      // Finding the RowCol of the cell
      cellUnderPos = FrameRef2RowCol(position,pYaw, dist_ahead, dist_aside);
      cell = getCellDataRC(cellUnderPos.first,cellUnderPos.second);

      // Corridor found !!!
      if(cell.type == OUTSIDE_CORRIDOR_CELL){
        obstacleFound = true;
      }
      invalidDataCell = false;
      // activateNoDataCell will be true if we have to treat it differently
      if((activateNoDataCell) && (cell.type != NO_DATA_CELL_TYPE) ){
        invalidDataCell = true;
      }
      if(!invalidDataCell){
        if(numValidCellsFound == 0) {
          min_height = cell.value;
          max_height = cell.value;
        } else {
          if(cell.value < min_height)
            min_height = cell.value;
          if(cell.value > max_height)
            max_height = cell.value;
          height_diff = max_height - min_height;
          if(height_diff > max_height_diff) max_height_diff = height_diff;
        }
        numValidCellsFound++;
      }
      if((max_height_diff > heightDiffThreshold)){
        //cout << "Found obstacle of height " << max_height_diff << endl;
	obstacleFound = true;
      }
      dist_aside += delta_dist;

      // debug obstacleAvoidance
      //(theMap[cellUnderPos]).color = GREEN_INDEX;//Is this still in use?
    }
    dist_ahead -= delta_dist;
  }
  // Cost ...
  // if there is a high height difference, don't go there
  if( obstacleFound ){
    cost = 0;
  }
  else{
//  param = 1 - max_height_diff/heightDiffThreshold;
//  cost = costShape(param,EXP_PARAM);

    param = max_height_diff/heightDiffThreshold; // Mike Thielman
    cost = costShape(param,EXP_PARAM);
    cost = MAX_VOTE_RANGE - cost; // Mike Thielman
  }
  //cout << "max_height_diff: " << max_height_diff << "  cost: " << cost << endl;
  return(cost);
}

// Evaluate each the cells the car would be if pointing to pYaw rad and on a certain row/col on the map
/* double maxHeightDiffCostRectangle(RowCol pos,double pYaw);
 * +DESCRIPTION: This method receive the position and yaw of the car and returns a cost based on height
 *
 * +PARAMETERS
 *   - RowCol position: row and col of the orign of the car
 *   - double pYaw: heading the car is pointing
 * +RETURN: a cost [0 100]
 */
double genMap::maxHeightDiffCostRectangle(RowCol position,double pYaw)
{
  int i,j,ind;
  double lowerRes;
  double delta_dist, north_delta_dist;
  double north_dist, east_dist;
  double dist_ahead, dist_aside;
  double cost, param;
  double min_height, max_height;
  double height_diff,max_height_diff; // the biggest height difference between cells
  CellData cell;
  RowCol pos, delta;
  int a,d;
  pair<double,double> NE_aux, NE_center, NE_a, NE_b, NE_c, NE_d, NE_lower, NE_upper;
  vector< pair<double,double> > corners;
  double ab_slope, ac_slope, lower_slope, upper_slope;
  int change_slope;
  bool alignedRectangle=false;
  bool obstacleFound = false;
  bool invalidDataCell = false,firstTime;
  int numValidCellsFound = 0;
  map<RowCol,CellData>::iterator mapIt;

  // Evaluating the smaller resolution
  lowerRes = colRes;
  if (rowRes < lowerRes)
    lowerRes = rowRes;
  delta_dist = lowerRes;

  // Finding the UTM coordinates the the 4 points that determine the rectangle
  i=0;ind=0;
  NE_center = RowCol2UTM(position);
  corners.reserve(4);
  firstTime=true;
  while(i <= 1){  // ahead
    j = -1;
    dist_ahead = ((double)i)*((double)VEHICLE_LENGTH)*growVehicleLength;
    while(j <= 1){ // aside
      dist_aside = ((double)j)*((double)VEHICLE_WIDTH)*growVehicleWidth/2;
      north_dist = dist_ahead*cos(pYaw) - dist_aside*sin(pYaw);
      east_dist = dist_ahead*sin(pYaw) + dist_aside*cos(pYaw);
      NE_aux.first = NE_center.first + north_dist;
      NE_aux.second = NE_center.second + east_dist;
      corners.push_back(NE_aux);
      // Keeping track of the lowest Northing corner
      if(firstTime){
        NE_a = NE_aux;
        NE_d = NE_aux;
        a = ind;
        d = ind;
        firstTime = false;
      }
      else{
        if(NE_aux.first < NE_a.first){
          NE_a = NE_aux;
  	  a = ind;
        }
        if(NE_aux.first > NE_d.first){
          NE_d = NE_aux;
          d = ind;
        }
      }
      j+=2;
      ind++;
    }
    i++;
  }

  // Find if the rectangle is aligned to the NorthEast axis and the sides
  alignedRectangle = false;
  if( (abs(corners[0].first - corners[1].first) < DELTA_SMALL) ||
      (abs(corners[0].first - corners[2].first) < DELTA_SMALL) ||
      (abs(corners[0].first - corners[3].first) < DELTA_SMALL) 
  ) {
    alignedRectangle = true;
  }

  // Putting corner "a" also as lowest easting, just in case it is aligned
  if(alignedRectangle){
    for(ind=0;ind<4;ind++){
      if( (abs(corners[ind].first-NE_a.first) < DELTA_SMALL) && (corners[ind].second < NE_a.second) ){
        NE_a = corners[ind];
        a = ind;
      }
      if( (abs(corners[ind].first-NE_d.first) < DELTA_SMALL) && (corners[ind].second > NE_d.second) ){
        NE_d = corners[ind];
        d = ind;
      } 
    }
  }

  // Finding the UTM of the corners next to NE_a
  firstTime = true;
  for(ind=0;ind<4;ind++){
    if( (ind != a) && (ind != d) ){
      if(firstTime){
        NE_b = corners[ind];
        NE_c = corners[ind];
        firstTime = false;
      }
      else{
        if(corners[ind].second > NE_b.second)
          NE_b = corners[ind];
        if(corners[ind].second < NE_c.second)
          NE_c = corners[ind];
      }
    }
  }

  // debug
  /*
  cout << "(a,d) = (" << a << "," << d << ")" << endl;
  cout << "a=(" << NE_a.first << "," << NE_a.second << ")" << endl;
  cout << "b=(" << NE_b.first << "," << NE_b.second << ")" << endl;
  cout << "c=(" << NE_c.first << "," << NE_c.second << ")" << endl;
  cout << "d=(" << NE_d.first << "," << NE_d.second << ")" << endl << endl;
  getchar();
  */

  max_height_diff = 0;
  max_height = -9999;
  min_height = 9999;
  if(alignedRectangle){
    ab_slope = 0;
    ac_slope = 0;
    lower_slope = 0;
    upper_slope = 0;

    NE_lower = NE_a;
    NE_upper = NE_d;
    NE_aux   = NE_lower;
  }
  else{
    ab_slope = (NE_b.first-NE_a.first)/(NE_b.second-NE_a.second);
    ac_slope = (NE_c.first-NE_a.first)/(NE_c.second-NE_a.second);
    lower_slope = ac_slope;
    upper_slope = ab_slope;
    NE_lower = NE_a;
    NE_upper = NE_a;
    NE_aux   = NE_lower;
  }

  // Finding the cells inside the rectangle
  while( (!obstacleFound) && (NE_aux.first <= NE_upper.first) ){
    while( (!obstacleFound) && (NE_aux.second <= NE_upper.second ) ) {

      pos.first    = leftBottomRow;
      delta.first  = (int)round((NE_aux.first - leftBottomUTM_Northing)/rowRes);
      pos.second   = leftBottomCol;
      delta.second = (int)round((NE_aux.second - leftBottomUTM_Easting)/colRes);
      pos = RowColFromDeltaRowCol(pos,delta);

      // debug
      //cell.value = 2.5;
      //setCellDataRC(pos.first, pos.second,cell);

      mapIt = theMap.find(pos);
      if ( mapIt == theMap.end() ) {
        cell = NO_DATA_CELL;
      }
      else{
        cell = mapIt->second;
      }
      cell.value = shrinkObstacle(cell.value,(curTimeStamp - cell.timeStamp));

      // Corridor found !!!
      if(cell.type == OUTSIDE_CORRIDOR_CELL){
        obstacleFound = true;
      }
      invalidDataCell = false;
      // activateNoDataCell will be true if we have to treat it differently
      if((activateNoDataCell) && (cell.type != NO_DATA_CELL_TYPE) ){
        invalidDataCell = true;
      }
      if(!invalidDataCell){
        if(numValidCellsFound == 0) {
          min_height = cell.value;
          max_height = cell.value;
        } else {
          if(cell.value < min_height)
            min_height = cell.value;
          if(cell.value > max_height)
            max_height = cell.value;
          height_diff = max_height - min_height;
          if(height_diff > max_height_diff) max_height_diff = height_diff;
        }
        numValidCellsFound++;
      }
      if((max_height_diff > heightDiffThreshold)){
        obstacleFound = true;
      }

      // Limiting not to evaluate outside the rectangle
      if( (NE_aux.second < NE_upper.second) && ((NE_aux.second+delta_dist) > NE_upper.second) )
        NE_aux.second = NE_upper.second;
      else
        NE_aux.second += delta_dist;
    }

    // debug
    //cout << "NE_aux=(" << NE_aux.first << "," << NE_aux.second << ")" << endl;
    //getchar();

    if(alignedRectangle){
      if( (NE_lower.first < NE_d.first) && ((NE_lower.first+delta_dist) > NE_d.first) ){
        NE_lower.first = NE_d.first;
      }
      else{
        NE_lower.first += delta_dist;
      }
    }
    else{
      // When we have pass the corner
      if( (NE_lower.first < NE_d.first) && ((NE_lower.first+delta_dist) > NE_d.first) ){
        break;
      }
      else{
        change_slope = 0;
        if( (NE_lower.first < NE_c.first) && ((NE_lower.first+delta_dist) > NE_c.first) )
          change_slope += 1;
        if( (NE_upper.first < NE_b.first) && ((NE_upper.first+delta_dist) > NE_b.first) )
          change_slope += 2;

        switch(change_slope){
        case 0: // no changes
          NE_lower.first += delta_dist;
          NE_lower.second += delta_dist/lower_slope;
          NE_upper.first += delta_dist;
          NE_upper.second += delta_dist/upper_slope;
	  break;

        case 1: // lower_slope has to change
          north_delta_dist = (NE_c.first-NE_lower.first);
          lower_slope = ab_slope;
          NE_lower = NE_c;
          NE_upper.first += north_delta_dist;
          NE_upper.second += north_delta_dist/upper_slope;
	  break;

        case 2: // upper_slope has to change
          north_delta_dist = (NE_b.first-NE_upper.first);
          upper_slope = ac_slope;
          NE_upper = NE_b;
          NE_lower.first += north_delta_dist;
          NE_lower.second += north_delta_dist/lower_slope;
	  break;

        case 3: // both have to change
          // LOWER
          lower_slope = ab_slope;
          north_delta_dist = delta_dist - (NE_c.first-NE_lower.first);
          NE_lower = NE_c;
          NE_lower.first += north_delta_dist;
          NE_lower.second += north_delta_dist/lower_slope;
          // UPPER
          upper_slope = ac_slope;
          north_delta_dist = delta_dist - (NE_b.first-NE_upper.first);
          NE_upper = NE_b;
          NE_upper.first += north_delta_dist;
          NE_upper.second += north_delta_dist/upper_slope;
	  break;
        }
      } // end if ( (NE_lower.first < NE.d.first) && ((NE.lower.first+delta_dist) > NE_d.first) ){
    } // end else (alignedRectangle)
    NE_aux = NE_lower;
  }

  // Cost ...
  // if there is a high height difference, don't go there
  if( obstacleFound ){
    cost = 0;
  }
  else{
    param = max_height_diff/heightDiffThreshold;
    cost = costShape(param,EXP_PARAM);
    cost = MAX_VOTE_RANGE - cost;
  }
  return(cost);
}

// Evaluate each the cells the car would be if pointing to pYaw rad and on a certain row/col on the map
/* double maxInclinationCost(RowCol pos,double pYaw);
 * +DESCRIPTION: This method receive the position and yaw of the car and returns a cost based on cos of the max and min cell
 *
 * +PARAMETERS
 *   - RowCol position: row and col of the orign of the car
 *   - double pYaw: heading the car is pointing
 * +RETURN: a cost [0 100]
 */
double genMap::maxInclinationCost(RowCol position,double pYaw)
{
  double lowerRes, delta_dist, dist_ahead, dist_aside;
  double cost, param;
  double min_height, max_height;
  double cosineCost;
  double max_height_diff; // the biggest height difference between cells
  RowCol cellUnderPos, findPos, deltaPos;
  RowCol maxCellPos, minCellPos;
  NorthEastDown a,b,c, groundVector, upVector; // to evaluate the cost
  pair<double,double> NorthEast;
  CellData cell, findCell;
  bool invalidDataCell = false;
  bool obstacleFound = false;

  // Evaluating the smaller resolution
  lowerRes = colRes;
  if (rowRes < lowerRes)
    lowerRes = rowRes;

  // Evaluating the step of the line ahead
  delta_dist = lowerRes;

  // Getting all cells under the car
  dist_ahead = VEHICLE_LENGTH*growVehicleLength;
  max_height_diff = 0;
  max_height = -9999;
  min_height = 9999;
  cosineCost = 1;
  while( (!obstacleFound) && (dist_ahead >= 0) ){
    // Varying the car width ...
    dist_aside = -VEHICLE_WIDTH*growVehicleWidth/2;
    while( (!obstacleFound) && (dist_aside <= VEHICLE_WIDTH*growVehicleWidth/2) ) {

      // Finding the RowCol of the cell
      cellUnderPos = FrameRef2RowCol(position,pYaw, dist_ahead, dist_aside);
      cell = getCellDataRC(cellUnderPos.first,cellUnderPos.second);
      // Corridor found !!!
      if(cell.type == OUTSIDE_CORRIDOR_CELL)
        obstacleFound = true;

      invalidDataCell = false;
      // activateNoDataCell will be true if we have to treat it differently
      if((activateNoDataCell) && (cell.type != NO_DATA_CELL_TYPE) ){
        invalidDataCell = true;
      }
      if(!invalidDataCell){
        if(cell.value < min_height){
          min_height = cell.value;
          minCellPos = cellUnderPos;
        }
        if(cell.value > max_height){
          max_height = cell.value;
          maxCellPos = cellUnderPos;
        }
        max_height_diff = max_height - min_height;

        // Evaluating the 3D points
        NorthEast = RowCol2UTM(minCellPos);
        a.Northing = NorthEast.first;
        a.Easting = NorthEast.second;
        a.Downing = 0;
        NorthEast = RowCol2UTM(maxCellPos);
        b.Northing = NorthEast.first;
        b.Easting = NorthEast.second;
        b.Downing = 0;
        c.Northing = NorthEast.first;
        c.Easting = NorthEast.second;
        c.Downing = max_height_diff;
        // Evaluating the vectors
        groundVector = b-a; 
        upVector = c-a;     
        groundVector.norm();
        upVector.norm();
        cosineCost = groundVector*upVector;
       
      }
      if(cosineCost < vectorProdThreshold){
	obstacleFound = true;
      }
      dist_aside += delta_dist;

      // debug obstacleAvoidance
      //(theMap[cellUnderPos]).color = BLUE_INDEX;//Is this still in use?
    }
    dist_ahead -= delta_dist;
  }
  // Cost ...
  // if there is a high height difference, don't go there
  param = cosineCost;
  if( (obstacleFound) || (param < vectorProdThreshold) ){
    cost = 0;
  }
  else{
    cost = costShape(param,EXP_PARAM);
  }

  return(cost);
}


// With all the cells under the vehicle, returns a normilized cost
/* double evalCellsCost(map<Row,Col>);
 * +DESCRIPTION: This method returns a cost evaluated from the max height difference between adjacent cells
 *
 * +PARAMETERS
 *   - map<RowCol,RowCol> cellsUnder: map as a list of row/cols of the cells under the car
 * +RETURN: a cost on the range [0 100]
 */
//Alternate Approach Idea: make cellsUnder a vector, and evaluate cost from the difference between the max height and min height in the vector.
/*
double genMap::evalCellsCost(map<RowCol,RowCol> cellsUnder)
{
  RowCol curPos, deltaPos, findPos;
  CellData curCell, findCell;
  bool obstacleFound=false;
  int numCells, row, col;
  int rowInd, colInd;
  double cost, param;
  double height_diff, sum_max_height_diff, max_height_diff;
  double global_max_height_diff; // the biggest height difference between cells
  std::map< RowCol, RowCol >::iterator ind = cellsUnder.begin();

  numCells = 0;
  sum_max_height_diff = 0;
  global_max_height_diff = 0;
  while( (!obstacleFound) && (ind != cellsUnder.end()) ){
    row = (*ind).first.first;
    col = (*ind).first.second;

    // The cell we are analysing
    curPos.first = row;
    curPos.second = col;
    curCell = getCellDataRC(curPos.first,curPos.second);
    // Getting the maximum height differente from this cell the the adjacents
    //  and summing up
    max_height_diff = 0;

    for(rowInd=-1; rowInd<=1 ;rowInd++) { //for row
      for(colInd =-1; colInd<=1; colInd++) { //for col
 
        // We don't need to compare the cell with itself
        if ((rowInd) || (colInd)) { //if not curCell

          // Finding the adjacent cell
          deltaPos.first = rowInd;
          deltaPos.second = colInd;
          findPos = RowColFromDeltaRowCol(curPos, deltaPos);

          // Getting the height of the adjacent cell
          findCell = getCellDataRC(findPos.first,findPos.second);

          // Corridor found !!!
          if(findCell.type == OUTSIDE_CORRIDOR_CELL)
            obstacleFound = true;

          height_diff = fabs(curCell.value - findCell.value);
          if(height_diff > max_height_diff){
            max_height_diff = height_diff;
          }

          //cout << "dh=" << height_diff << " max_dh=" << max_height_diff <<  endl;

        } //end if not currCell

      } //end for col
    } //end for row

    // Acumulating the max height diff for all cells
    if(max_height_diff > global_max_height_diff){
      global_max_height_diff = max_height_diff;
    }
    //cout << "max_dh=" << max_height_diff << " global_max_dh=" << global_max_height_diff <<  endl;
    //getchar();

    // If it's already an obstacle
    if(global_max_height_diff > heightDiffThreshold){
      obstacleFound = true;
    }
  
    // Acumulationg the max_heightDiff and number of cells on the path
    sum_max_height_diff += max_height_diff;
    numCells++;
    ind++;
  } // end of cost evaluating while(ind != pathMap.end())

  // Cost ...
  // if there is a high height difference, don't go there
  param = 1 - global_max_height_diff/heightDiffThreshold;
  if( (obstacleFound) || (param < 0) ){
    cost = 0;
  }
  else{
    cost = costShape(param,EXP_PARAM);
  }

  return(cost);
}
*/

// Path Evaluation Methods v 2.0
/* double obstacleAvoidance(double steer)
 * +DESCRIPTION: This method receive a steering wheel angle and, based on the
 *    path the car follows on that steering angle, evaluates the distance the
 *    car can sweep until reaches a dangerous obstacle
 * +PARAMETERS
 *   - steer: steering angle position in radians. Right is positive and left
 *            is negative
 * +RETURN: distance in meters
 */
//Lars Cremean's idea to have it evaluate a whole array of angles at once could save some time.
pair<double,bool> genMap::obstacleAvoidance(double steer) 
{
  VBClock clk;
  CellData cell;
  int numCriteria=0;
  double distance, cost;
  double yaw, steerAngle;
  double higherRes, delta_radius, PathRadius;
  double delta_dist, dist, delta_ahead, delta_aside, max_dist, last_dist, first_delta_ahead;
  double stepRes=1;
  double max_phi, phi, delta_phi, last_phi;
  int steerSgn, indTire, i,j;
  bool obstacle=false;
  bool noDataCell=false;
  bool invalidDataCell = false;
  map<RowCol,RowCol> cellsUnder;
  pair<double,double> NorthEast;
  RowCol carPos, oldCarPos, tirePos, curPos;
  NorthEastDown tire[4];         // position vectors of the tires
  NorthEastDown normal[4]; // normal vectors to the planes determined by three tires
  double offTire[4];        // distance between the plane and the 4th tire
  double normalCost, offTireCost, underCellsCost, max_HeightDiffCost, max_InclinationCost, max_HeightDiffCostRectangle;
  pair <double,bool> returnVal; // .first is dist .second is if obstacle

  // Getting the heading
  yaw = curState.Yaw;

  // Storing the sign of the steering wheel and getting the abs value
  if(steer >= 0)
    steerSgn = +1;
  else
    steerSgn = -1;
  steerAngle = steerSgn*steer;

  // vehRow and vehCol is about the middle of the rear axle
  oldCarPos.first = vehRow;
  oldCarPos.second = vehCol;
  first_delta_ahead = curState.Speed*MAX_REACTION_TIME_BETWEEN_SENSOR_SWEEP_AND_BEGINNING_DECELLERATION_FOR_EVALUATING_VELOCITY/2;
  carPos = FrameRef2RowCol(oldCarPos,curState.Yaw,first_delta_ahead,0);

  // Evaluating the smaller resolution
  higherRes = colRes;
  if (rowRes > higherRes)
    higherRes = rowRes;

  // Step Resolution
  stepRes = 1;

  // GOING STRAIGHT !!
  obstacle = false;
  distance = 0;
  if(!steerAngle){
    // Evaluating the max distance to consider ahead
    max_dist = pathMaxDistance-first_delta_ahead;
    // Step
    delta_dist = higherRes;
    //delta_dist = max_dist/10;

    // For each distance ahead ...
    dist = 0;
    while ( (!obstacle) && (dist < max_dist) ) {
      last_dist = dist;
      if(stepRes*delta_dist > VEHICLE_AXLE_DISTANCE/2)
        dist += VEHICLE_AXLE_DISTANCE/2;
      else
        dist += stepRes*delta_dist;

      if( (last_dist < max_dist) && (dist > max_dist) ){
        dist = max_dist;
      }
      stepRes += 1;

      // Distance ahead of the car in a frame reference
      delta_ahead = dist;
      delta_aside = 0;

      curPos = FrameRef2RowCol(carPos,yaw, delta_ahead, delta_aside);

      // Evaluating the height and cost      
      if(useCellsUnderCost){
        underCellsCost = allCellsUnderCost(curPos,yaw);
      }
      if(useMaxHeightDiffCost){
        max_HeightDiffCost = maxHeightDiffCost(curPos,yaw);
      }
      if(useMaxHeightDiffCostRectangle){
        max_HeightDiffCostRectangle = maxHeightDiffCostRectangle(curPos,yaw);
      }
      if(useMaxInclinationCost){
        max_InclinationCost = maxInclinationCost(curPos,yaw);
      }

      // Evaluating the tires position
      if((useOffTireCost) || (useDotVectorCost)){
        indTire = 0;
        noDataCell = false;
        i=0;
        while( (!noDataCell) && (i <= 1) ){  // ahead
          j = -1;
          while( (!noDataCell) && (j <= 1) ){ // aside
            tirePos = FrameRef2RowCol(curPos, yaw, i * VEHICLE_AXLE_DISTANCE,
				    j * VEHICLE_AVERAGE_TRACK / 2);
            cell = getCellDataRC(tirePos.first,tirePos.second);
            invalidDataCell = false;
            if((activateNoDataCell) && (cell.type == NO_DATA_CELL_TYPE) ){
              invalidDataCell = true;
            }
            if(!invalidDataCell){
              noDataCell = true;
              break;
            }
            else{
              NorthEast = RowCol2UTM(tirePos);
              tire[indTire].Northing = NorthEast.first;
              tire[indTire].Easting = NorthEast.second;
              tire[indTire].Downing = cell.value;
              indTire++;
            }
            j+=2;
          }
          i++;
        }
      }

      // Evaluating the normal vectors and cost
      if(useDotVectorCost){
        if(!noDataCell){
          normal[0]   = evalNormalVector(tire[1], tire[2], tire[3]);
          normal[1]   = evalNormalVector(tire[0], tire[2], tire[3]);
          normal[2]   = evalNormalVector(tire[0], tire[1], tire[2]);
          normal[3]   = evalNormalVector(tire[0], tire[1], tire[2]);
          normalCost = evalNormalVectorCost(normal);
        }
        else{
          normalCost = obstacleThreshold;
        }
      }

      // Evaluating the tires off the plane determined by the other three,
      // and cost
      if(useOffTireCost){
        if(!noDataCell){
          offTire[0]  =  distancePoint2Plane(tire[1], tire[2], tire[3], tire[0]);
          offTire[1]  =  distancePoint2Plane(tire[0], tire[2], tire[3], tire[1]);
          offTire[2]  =  distancePoint2Plane(tire[0], tire[1], tire[3], tire[2]);
          offTire[3]  =  distancePoint2Plane(tire[0], tire[1], tire[2], tire[3]);
          offTireCost = evalOffTireCost(offTire);
        }
        else{
          offTireCost = obstacleThreshold;
        }
      }
      // debug
      //system("clear");
      //(theMap[curPos]).color = RED_INDEX;
      //display();
      //clearColor(BLUE_INDEX);//Is this still in use?
      //cout << "MAX_DIST=" << max_dist << "  DIST=" << dist << endl;
      //cout << "PRESS ... " << endl;
      //fflush(stdin);
      //getchar();

      // Averaging
      cost = 0; 
      numCriteria = 0;
      if(useCellsUnderCost){
        cost += underCellsCost;
        numCriteria++;
      }
      if(useMaxHeightDiffCost){
        cost += max_HeightDiffCost;
        numCriteria++;
      }
      if(useMaxHeightDiffCostRectangle){
        cost += max_HeightDiffCostRectangle;
        numCriteria++;
      }
      if(useMaxInclinationCost){
        cost += max_InclinationCost;
        numCriteria++;
      }
      if(useDotVectorCost){
        cost += normalCost;
        numCriteria++;
      }
      if(useOffTireCost){    
        cost += offTireCost;
        numCriteria++;
      }
      if(numCriteria){
        cost = cost/((double)numCriteria);
      }
      else{
        cost = obstacleThreshold;
      }
      // Deciding if this is or not an obstacle
      if(cost < obstacleThreshold){
        obstacle = true;
        distance = dist - delta_dist;
      }
    }
  }
  // FOLLOWING A CURVE!!
  else {

    // considering the radius of the rear tire
    PathRadius = abs(VEHICLE_AXLE_DISTANCE/(tan(steerAngle)));  
    // Evaluating the max_phi, the maximum angle the path goes
    max_phi = (pathMaxDistance-first_delta_ahead)/PathRadius;
    if(max_phi > MAX_PHI) {
      // we don't want to keep making circles and alnalysing the same path a
      // lot of times !!
      max_phi = MAX_PHI;  
    }

    // Step
    delta_radius = higherRes;
    delta_phi = delta_radius/(PathRadius);
    //delta_phi = max_phi/10;

    // For each phi ...
    phi = 0;
    while ( (!obstacle) && (phi < max_phi) ) {
      last_phi = phi;
      if(stepRes*delta_phi*PathRadius > VEHICLE_AXLE_DISTANCE/2)
        phi += VEHICLE_AXLE_DISTANCE/(2*PathRadius);
      else
        phi += stepRes*delta_phi;

      if( (last_phi < max_phi) && (phi > max_phi) ) {
        phi = max_phi;
      }
      stepRes += 1;
      // DEBUG 
      //cout << "genMap::obstacleAvoidance: \"for each phi...\" while loop " << steerAngle << ", phi = " << phi << ", max_phi = " << max_phi<< ", delta_phi = " << delta_phi<< endl;
 
      // Evaluating displacement ahead and aside
      delta_ahead = PathRadius*sin(phi);
      delta_aside = steerSgn*PathRadius*(1 - cos(phi));
      //cout << "steerAngle = " << steerAngle << endl;
      //cout << "delta_ahead = " << delta_ahead << " delta_aside = " << delta_aside << endl;
      yaw = curState.Yaw + steerSgn*phi;
      // Finding the RowCol of the cell
      //cout << "old pos = (" << curPos.first << "," << curPos.second << ")" << endl;
      curPos = FrameRef2RowCol(carPos,curState.Yaw, delta_ahead, delta_aside);
      //cout << "new pos = (" << curPos.first << "," << curPos.second << ")" << endl;
      //cout << "max path = " <<pathMaxDistance-first_delta_ahead << " delta_radius = " << delta_phi*PathRadius << endl;
      //getchar();

      // Evaluating the height and cost
      if(useCellsUnderCost){
        underCellsCost = allCellsUnderCost(curPos,yaw);
      }
      if(useMaxHeightDiffCost){
        max_HeightDiffCost = maxHeightDiffCost(curPos,yaw);
      }
      if(useMaxHeightDiffCostRectangle){
        max_HeightDiffCostRectangle = maxHeightDiffCostRectangle(curPos,yaw);
      }
      if(useMaxInclinationCost){
        max_InclinationCost = maxInclinationCost(curPos,yaw);
      }

      // Evaluating the tires position
      if((useOffTireCost) || (useDotVectorCost)){
        indTire = 0;
        noDataCell = false;
        i=0;
        while( (!noDataCell) && (i <= 1) ){  // ahead
          j = -1;
          while( (!noDataCell) && (j <= 1) ){ // aside
            tirePos = FrameRef2RowCol(curPos, yaw, i * VEHICLE_AXLE_DISTANCE,
				    j * VEHICLE_AVERAGE_TRACK / 2);
            cell = getCellDataRC(tirePos.first,tirePos.second);
            if(cell.type == NO_DATA_CELL_TYPE){
              noDataCell = true;
              break;
            }
            else{
              NorthEast = RowCol2UTM(tirePos);
              tire[indTire].Northing = NorthEast.first;
              tire[indTire].Easting = NorthEast.second;
              tire[indTire].Downing = cell.value;
              indTire++;
            }
            j+=2;
          }
          i++;
        }
      }

      // Evaluating the normal vectors and cost
      if(useDotVectorCost){
        if(!noDataCell){
          normal[0]   = evalNormalVector(tire[1], tire[2], tire[3]);
          normal[1]   = evalNormalVector(tire[0], tire[2], tire[3]);
          normal[2]   = evalNormalVector(tire[0], tire[1], tire[2]);
          normal[3]   = evalNormalVector(tire[0], tire[1], tire[2]);
          normalCost = evalNormalVectorCost(normal);
        }
        else{
          normalCost = obstacleThreshold;
        }
      }

      // Evaluating the tires off the plane determined by the other three,
      // and cost
      if(useOffTireCost){
        if(!noDataCell){
          offTire[0]  =  distancePoint2Plane(tire[1], tire[2], tire[3], tire[0]);
          offTire[1]  =  distancePoint2Plane(tire[0], tire[2], tire[3], tire[1]);
          offTire[2]  =  distancePoint2Plane(tire[0], tire[1], tire[3], tire[2]);
          offTire[3]  =  distancePoint2Plane(tire[0], tire[1], tire[2], tire[3]);
          offTireCost = evalOffTireCost(offTire);
        }
        else{
          offTireCost = obstacleThreshold;
        }
      }

      // debug
      //system("clear");
      //(theMap[curPos]).color = RED_INDEX;
      //display();
      //clearColor(BLUE_INDEX);//Is anyone using this?
      //cout << "MAX_PHI=" << max_phi*180/M_PI << "  PHI=" << phi*180/M_PI << " DELTA_PHI" << delta_phi*180/M_PI<< "  RADIUS=" << PathRadius << endl;
      //cout << "PRESS ... " << endl;
      //fflush(stdin);
      //getchar();

      // Averaging
      cost = 0; 
      numCriteria = 0;
      if(useCellsUnderCost){
        cost += underCellsCost;
        numCriteria++;
      }
      if(useMaxHeightDiffCost){
        cost += max_HeightDiffCost;
        numCriteria++;
      }
      if(useMaxInclinationCost){
        cost += max_InclinationCost;
        numCriteria++;
      }
      if(useMaxHeightDiffCostRectangle){
        cost += max_HeightDiffCostRectangle;
        numCriteria++;
      }
      if(useDotVectorCost){
        cost += normalCost;
        numCriteria++;
      }
      if(useOffTireCost){    
        cost += offTireCost;
        numCriteria++;
      }
      if(numCriteria){
        cost = cost/((double)numCriteria);
      }
      else{
        cost = obstacleThreshold;
      }
      // Deciding if this is or not an obstacle
      if(cost < obstacleThreshold){
        obstacle = true;
        distance = (phi - delta_phi)*PathRadius;
      }
    } 
  }

  returnVal.second = obstacle;

  // If there is no obstacles
  if(!obstacle){
    // Following a curve sometimes it's not possible to sweep the hole pathMaxDistance
    if( (steer) && (max_phi == MAX_PHI) )
      distance = MAX_PHI*PathRadius;
    else
      distance = pathMaxDistance;
  }
  returnVal.first = distance;
  return(returnVal);
}


// Path Evaluation Methods v 1.0
/* double evalPathCost(double steer)
 * +DESCRIPTION: This method receive a steering wheel angle and, based on the
 *    path the car follows on that steering angle, evaluates the path goodness
 *    based on difference of height.
 * +PARAMETERS
 *   - steer: steering angle position in radians. Right is positive and left
 *            is negative
 * +RETURN: cost with no normalization
 */
double genMap::evalPathCost(double steer) 
{
  double PathRadius;  // Radius of a path
  double yaw;         // Yaw from the current state 
  double north_aux, east_aux;
  double cost;        //cost to be returned

  double delta_ahead;  // aligned to the heading of the car
  double delta_aside;  // aligned to the side of the car, towards right

  int row, col, rowInd, colInd;
  RowCol gpsPos;      // GPS location
  RowCol carPos;      // Mid front car location
  RowCol circCenter;  // Center of the circunference
  RowCol obstaclePos; // Location of a obstacle in the matrix

  // Variables to evaluate cost
  CellData findCell, currCell;
  RowCol findPos, currPos, deltaPos;
  double heightDiff, max_heightDiff;
  double sum_max_heightDiff, numCells;

  int steerSgn;       // sgn of the steering angle
  double steerAngle;  // Abs of steer
  double lowerRes;    // The lower resolution between rows and cols

  // Angles to evaluate the path given a radius
  double phi, delta_phi;
  double max_phi;

  // Distance to evaluate the path staight ahead
  double dist, delta_dist;
  double max_dist;

  // Radius to evaluate the car widht, given a phi
  double radius, delta_radius;

  // Map of (row,col) of cells on the way of the path evaluated
  std::map< RowCol, RowCol > pathMap;

  // Getting the heading
  yaw = curState.Yaw;

  // Storing the sign of the steering wheel and getting the abs value
  if(steer >= 0)
    steerSgn = +1;
  else
    steerSgn = -1;
  steerAngle = steerSgn*steer;

  // vehRow and vehCol is about the GPS location
  // We need to get the vehRow and vehCol for the mid point of the front of
  //  the car
  gpsPos.first = vehRow;
  gpsPos.second = vehCol;
  carPos = FrameRef2RowCol(gpsPos,curState.Yaw,GPS_2_FRONT_DISTANCE,0);

  // Evaluating the smaller resolution
  lowerRes = colRes;
  if (rowRes < lowerRes)
    lowerRes = rowRes;

  // The step of a straight line integration is just the lower resolution
  delta_radius = lowerRes/2 - lowerRes/4;

  //*********************************************//
  //***GETTING THE CELLS THE CAR WILL RUN OVER***//
  //*********************************************//

  // GOING STRAIGHT !!
  if(!steerAngle){

    // Evaluating the step of the line ahead
    delta_dist = lowerRes/2 - lowerRes/4;

    // Evaluating the max distance to consider ahead
    max_dist = pathMaxDistance;
  
    // For each distance ahead ...
    dist = 0;
    while (dist < max_dist) {

      // Distance ahead of the car in a frame reference
      delta_ahead = dist;

      // Varying the car width ...
      radius = -VEHICLE_WIDTH/2;
      while(radius <= VEHICLE_WIDTH/2) {
        // Distance aside of the car in a frame reference
        delta_aside = radius;

        // Finding the RowCol of the cell
        obstaclePos = FrameRef2RowCol(carPos,yaw, delta_ahead, delta_aside);
        
        // debug
        (theMap[obstaclePos]).color = 1;

        pathMap.insert( map<RowCol,RowCol>::value_type(obstaclePos,obstaclePos));

        radius += delta_radius;
      }
      dist += delta_dist;
    }
    
  }
  // FOLLOWING A CURVE!!
  else {

    PathRadius = VEHICLE_AXLE_DISTANCE/(tan(steerAngle));

    // Getting the center of the circunference (not sure if we need this!)
    //north_aux =  PathRadius*cos(yaw)*sterrSgn;
    //east_aux = -PathRadius*sin(yaw)*steerSgn;
    //circCenter = RowColFromPosition(carPos,north_dist,east_dist);

    // Evaluating step of phi, the step should be small enough not to lose
    //  cells on the path evaluation (delta_phi*Radius < Res)    
    delta_phi = lowerRes/(PathRadius);

    // Evaluating the max_phi, the maximum angle the path goes
    max_phi = pathMaxDistance/PathRadius;

    // For each phi ...
    phi = 0;
    while(phi <= max_phi) {

      // Varying the car width ...
      radius = PathRadius - VEHICLE_WIDTH/2;
      while(radius <= (PathRadius + VEHICLE_WIDTH/2)) {

        // Evaluating displacement ahead and aside
        delta_ahead = radius*sin(phi);
        delta_aside = steerSgn*(PathRadius - radius*cos(phi));

        // Finding the RowCol of the cell
        obstaclePos = FrameRef2RowCol(carPos,(curState.Yaw + steerSgn*phi),
				      delta_ahead, delta_aside);

        // debug
        (theMap[obstaclePos]).color = 1;

        pathMap.insert( map<RowCol,RowCol>::value_type(obstaclePos,obstaclePos));

        radius += delta_radius;
      }

      phi += delta_phi;
    } 
  }

  //*********************************************//
  //*************EVALUATING THE COST*************//
  //*********************************************// 
  std::map<RowCol,RowCol>::iterator ind = pathMap.begin();

  numCells = 0;
  sum_max_heightDiff = 0;
  while(ind != pathMap.end()){
    row = (*ind).first.first;
    col = (*ind).first.second;

    // The cell we are analysing
    currPos.first = row;
    currPos.second = col;
    if(theMap.find(currPos) != theMap.end()){
      currCell = theMap[currPos];
    }
    // Getting the maximum height differente from this cell the the adjacents
    //  and summing up
    max_heightDiff = 0;

    for(rowInd=-1; rowInd<=1 ;rowInd++) { //for row
      for(colInd =-1; colInd<=1; colInd++) { //for col

        // We don't need to compare the cell with itself
        if ((rowInd) || (colInd)) { //if not currCell

          // Finding the adjacent cell
          deltaPos.first = rowInd;
          deltaPos.second = colInd;
          findPos = RowColFromDeltaRowCol(currPos, deltaPos);

          // Getting the height of the adjacent cell
          if(theMap.find(findPos) != theMap.end()){
            findCell = theMap[findPos];
            heightDiff = fabs(currCell.value - findCell.value);
            if(heightDiff > max_heightDiff){
              max_heightDiff = heightDiff;
            }
          }

        } //end if not currCell

      } //end for col
    } //end for row

    // Acumulationg the max_heightDiff and number of cells on the path
    sum_max_heightDiff += max_heightDiff;
    numCells++;
    ind++;
  } // end of cost evaluating while(ind != pathMap.end())

  // Cost ...
  cost = sum_max_heightDiff/numCells;

  // debug
  //cout << "Sum max heightDiff = " << sum_max_heightDiff << " and numCells = " << numCells << endl;
  
  return(cost);
} // end of EvalPathCost()

double genMap::findMaxHeightinRectangle(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4)
{
  int x_y_is_inside = 1;
  double minz = 1000000.0;
  RowCol inside_cell;
  if((((x1 <= x4)&&(y2 <= y3))&&((x2 != x1)&&(x3 != x1))) && 
     ((x4 != x2)&&(x4 != x3)))
  {
    for(double x = x1; x <= x4; x += rowRes)
    {
      for(double y = y2; y <= y3; y += colRes)
      {
	//Tests that x, y is inside the rectangle.
	if(x <= x3)
	  {
	    if(y > (x - x3)*(y3 - y1)/(x3 - x1) + y3)
	      {
		x_y_is_inside = 0;
	      }
	  }
	else
	  {
	    if(y > (x - x4)*(y4 - y3)/(x4 - x3) + y4)
	      {
		x_y_is_inside = 0;
	      }
	  }
	if(x <= x2)
	  {
	    if(y < (x - x2)*(y2 - y1)/(x2 - x1) + y2)
	      {
		x_y_is_inside = 0;
	      }
	  }
	else
	  {
	    if(y < (x - x4)*(y4 - y2)/(x4 - x2) + y4)
	      {
		x_y_is_inside = 0;
	      }
	  }
	if(x_y_is_inside == 1)
	  {
	    inside_cell = UTM2RowCol(x, y);
	    //Tests that inside_cell is inside window.
	    if(((inside_cell.first >= 0)&&(inside_cell.first < numRows)) &&
	       ((inside_cell.second >= 0)&&(inside_cell.second < numCols)))
	      {
		if(((theMap.find(inside_cell)->second).value) < minz)
		  {
		    minz = (theMap.find(inside_cell)->second).value;
		  }
	      }
	  }//close conditions for point inside rectangle
      }//close cycle through ys
    }//close cycle through xs
  }//close conditions for points in correct order
  else
  {
    if((((x1 == x2)||(x4 == x3))&&((y2 <= y3)&&(x4 >= x1)))&&(x2 != x4))
    {
      for(double x = x1; x <= x4; x += rowRes)
      {
	for(double y = y2; y <= y3; y += colRes)
   	{
	  if(y >= (x - x1)*(y3 - y1)/(x3 - x1) + y1)
          {
	    x_y_is_inside = 0;
	  }
	  if(y <= (x - x2)*(y4 - y2)/(x4 - x2) + y2)
	    {
	      x_y_is_inside = 0;
	    }
	  if(x_y_is_inside == 1)
	    {
	      inside_cell = UTM2RowCol(x, y);
	      if(((inside_cell.first >= 0)&&(inside_cell.first < numRows)) &&
		 ((inside_cell.second >= 0)&&(inside_cell.second < numCols)))
	      {
		if((theMap.find(inside_cell)->second).value < minz)
		  {
		    minz = (theMap.find(inside_cell)->second).value;
		  }
	      }
	    }//end conditional for x,y is inside rectangle
	} //end run through ys
      }//end run through xs
    }//end conditional block for points in order
    else
    {
      //cout << "Please rearrange your vertices.\n";
    }
  } //end outer else
  return minz;
}

// translade and rotate coordinates 
/* void changeCoordinates(NorthEastDown,NorthEastDown,NorthEastDown,NorthEastDown,NorthEastDown,double);
 * +DESCRIPTION: Put ref_ab as North (1,0) and evaluate the other vector at this new coordinate system
 *
 * +PARAMETERS
 *   - a_ref            : vector determining the position of the vector ab_ref
 *   - b_ref            : vector determining the extremity of the vector ab_ref
 *   - a_car            : vector determining the position of the vector ab_car
 *   - b_car            : vector determining the extremity of the vector ab_car
 * +RETURN: void, BUT THE PARAMETERS ARE UPDATED ISNSIDE
 */
void genMap::changeCoordinates(NorthEastDown &a_ref, NorthEastDown &b_ref, 
                               NorthEastDown &a_car, NorthEastDown &b_car, int &side){
  double cos_theta,sin_theta;
  NorthEastDown ref_vec,car_vec;
  NorthEastDown new_ref_vec,new_car_vec, aux;

  // Evaluating vectors

  // Rotating the ref vector antiwiseclock
  // [+cos +sin].[N]=[N']
  // [-sin +cos] [E] [E']
  ref_vec = b_ref - a_ref; ref_vec.norm();
  car_vec = b_car - a_car; car_vec.norm();

  sin_theta = ref_vec.Easting;
  cos_theta = ref_vec.Northing;
  new_ref_vec.Northing = cos_theta*ref_vec.Northing + sin_theta*ref_vec.Easting;
  new_ref_vec.Easting = -sin_theta*ref_vec.Northing + cos_theta*ref_vec.Easting;
  new_car_vec.Northing = cos_theta*car_vec.Northing + sin_theta*car_vec.Easting;
  new_car_vec.Easting = -sin_theta*car_vec.Northing + cos_theta*car_vec.Easting;

  // Finding the side
  aux.Northing = cos_theta*a_ref.Northing + sin_theta*a_ref.Easting;
  aux.Easting = -sin_theta*a_ref.Northing + cos_theta*a_ref.Easting;
  a_ref = aux;
  aux.Northing = cos_theta*b_ref.Northing + sin_theta*b_ref.Easting;
  aux.Easting = -sin_theta*b_ref.Northing + cos_theta*b_ref.Easting;
  b_ref = aux;
  aux.Northing = cos_theta*a_car.Northing + sin_theta*a_car.Easting;
  aux.Easting = -sin_theta*a_car.Northing + cos_theta*a_car.Easting;
  a_car = aux;
  aux.Northing = cos_theta*b_car.Northing + sin_theta*b_car.Easting;
  aux.Easting = -sin_theta*b_car.Northing + cos_theta*b_car.Easting;
  b_car = aux;

  if((b_car.Easting) > b_ref.Easting){
    side = RIGHT_SIDE;
  }
  else{
    side = LEFT_SIDE;
  }

  // Returning values by real reference
  b_ref = new_ref_vec;
  b_car = new_car_vec;
}

// Evaluate the votes based on distance to the obstacle v 2.0
/* vector<pair> generateVotes(double *steer)
 * +DESCRIPTION: This method receive an array of steering angle and evaluate the goodness for each
 *
 * +PARAMETERS
 *   - steer: steering angle position in radians. Right is positive and left
 *            is negative
 * +RETURN: first  = velocity
 *          second = goodness
 */
vector< pair<double,double> > genMap::generateArbiterVotes(int numArcs, vector<double> steerAngle) 
{
  int side, directionSteer, directionSteerNextWP, directionSteerVecField;
  bool allCorridorVotesLow=true;
  double delta_dist, dist, vel, PathRadius;
  double dist_ahead, dist_aside, lowerRes;
  double angle, angleProjection;
  double alpha, to, crossAngle;
  double w, d, x, offset, distanceCar2TrackLine, zeta; // for the vector field
  vector< pair<double,double> > votes,followerVotes;
  pair <double,double> oneVote;
  int ind, row, col;
  RowCol rearCarPos, frontCarPos, cellUnderPos;
  CellData cell;
  map< RowCol, RowCol > cellsUnder;
  std::map< RowCol, RowCol >::iterator mapIter;
  bool corridorFound, outsideCorridor=false, wrongDirection=false;
  bool crossTrackLine;
  pair<bool,double> pointLine;
  pair<double,double> NorthEast;
  double scale, scaleSteer, scaleVel, scaleSteerNextWP, scaleVelNextWP, scaleSteerVecField, scaleVelVecField;
  NorthEastDown v, projection, po;
  NorthEastDown begin_track,end_track;           // track to follow
  NorthEastDown rear_car, front_car;             // vector that defines the car
  NorthEastDown aux_begin_track, aux_end_track;
  NorthEastDown aux_rear_car, aux_front_car;
  NorthEastDown trackVector,carVector,aux1,aux2, projVector;
  NorthEastDown car2track;
  double maxRes=rowRes;
  pair<double,bool> obsAvoid; // return value from obstacleAvoidance
  bool obstacle; // was an obstacle detected?
  vector< pair<double,bool> > obsAvoidStorage;
  double minDist,maxDist;
  if(colRes > maxRes)
    maxRes = colRes;
  // Reserving room for the elements
  votes.reserve(numArcs);
  followerVotes.reserve(numArcs);

  // If there is an option to follow the corridor we should also add the waypointFollower votes
  if(evalCorridor){
    wrongDirection = false;
    if(nextWaypoint < numTargetPoints){

      // Car vector rear -> front
      rearCarPos.first = vehRow;
      rearCarPos.second = vehCol;
      rear_car.Northing = curState.Northing;
      rear_car.Easting = curState.Easting;
      rear_car.Downing = 0;
      front_car.Northing = curState.Northing + GPS_2_FRONT_DISTANCE*cos(curState.Yaw);
      front_car.Easting = curState.Easting + GPS_2_FRONT_DISTANCE*sin(curState.Yaw);
      front_car.Downing = 0;
      carVector = front_car - rear_car;
      // Track line vector begin -> end
      begin_track.Northing = targetPoints[lastWaypoint].Northing;
      begin_track.Easting  = targetPoints[lastWaypoint].Easting;
      end_track.Northing = targetPoints[nextWaypoint].Northing;
      end_track.Easting  = targetPoints[nextWaypoint].Easting;
      trackVector = end_track - begin_track;

      // Distance between the car and the trackline => pointLine.second is the distance
      pointLine = distancePoint2Line(begin_track,end_track,front_car);
      distanceCar2TrackLine = pointLine.second;
      crossTrackLine = pointLine.first;
      //cout << "distanceCar2TrackLine = " << distanceCar2TrackLine << endl;
      //if(crossTrackLine) cout << "CROSS THE TRACK LINE" << endl;

      // VERIFYING IF THE CAR IS OUTSIDE OF THE CORRIDOR
      dist_ahead = 0;
      outsideCorridor = false;
      while ((!outsideCorridor) && (dist_ahead <= VEHICLE_AXLE_DISTANCE) ) {
        // Varying the car width ...
        dist_aside = -VEHICLE_AVERAGE_TRACK/2;
        while ((!outsideCorridor) && (dist_aside <= VEHICLE_AVERAGE_TRACK/2) ) {
          // Finding the RowCol of the cell
          cellUnderPos = FrameRef2RowCol(rearCarPos,curState.Yaw, dist_ahead, dist_aside);
          cell = getCellDataRC(cellUnderPos.first, cellUnderPos.second);
          if(cell.type == OUTSIDE_CORRIDOR_CELL){
            outsideCorridor = true;
          }
          dist_aside += VEHICLE_AVERAGE_TRACK;
        }
        dist_ahead += VEHICLE_AXLE_DISTANCE;
      }

      /***********************************************/
      // EVALUATING THE COMMAND TO THE NEXT WAYPOINT //
      /***********************************************/

      // FINDING THE PROJECTION F THE CAR ON THE TRACK LINE
      v = begin_track - end_track; // a - b
      v.norm();
      to = -( v.Northing*(begin_track.Northing - rear_car.Northing) + v.Easting*(begin_track.Easting - rear_car.Easting) + v.Downing*(begin_track.Downing - rear_car.Downing) );
      projection = v*to + begin_track;

      // FINDING THE POINT ON THE TRACK LINE IN THE MIDDLE OF THE LINE FROM THERE AND THE NEXT WAYPOINT
      if(nextWaypoint){
	// Weighting to point more to the track line than to the next waypoint
        double near=0,far=1;
        po.Northing = (near*projection.Northing + far*end_track.Northing)/(near+far);
        po.Easting = (near*projection.Easting + far*end_track.Easting)/(near+far);
      }
      else{
        po = end_track;
      }

      // debug
      //projection.display();
      //po.display();
      //end_track.display();
      //getchar();

      // BUILDING THE VECTOR THAT WILL BE OUR NEW NORTH
      begin_track = rear_car; aux_begin_track = begin_track;
      end_track = po;  aux_end_track = po;
      // Car vector
      aux_rear_car = rear_car;
      aux_front_car = front_car;

      // CHANGE COORDINATES TO PONT TO HALF WAY FROM THE PROJECTION AND 
      changeCoordinates(aux_begin_track,aux_end_track,aux_rear_car,aux_front_car,side);
      trackVector = aux_end_track;
      //trackVector.display();
      carVector = aux_front_car;
      scale = trackVector*carVector;
      angle = abs(acos(scale));

      if(angle > USE_MAX_STEER){
        angle = USE_MAX_STEER;
      }
      //cout << "dot product = " << scale << " angle = " << 180*angle/M_PI << " USE_MAX_STEER = " << 180*USE_MAX_STEER/M_PI << endl;
      scaleSteer = -2*angle/((double)USE_MAX_STEER) + 1;
      //cout << "scaleSteer = " << scaleSteer << endl;
      scaleVel = (scale+1)/2;
      if(carVector.Easting > 0){
        directionSteer = -1;
      }
      else{
        directionSteer = 1;
      }
      // Keeping this values in case all the votes are zero
      scaleVelNextWP = scaleVel;
      scaleSteerNextWP = scaleSteer;
      directionSteerNextWP = directionSteer;

      // If it is outside we've already evaluated the votes we need
      //if(outsideCorridor){
      if(!crossTrackLine){
        wrongDirection = true;
      }
      else{
        // POINT TO THE NEXT WAYPOINT BUT IN A FLAT WAY
        aux_rear_car = rear_car;
        aux_front_car = front_car;
        changeCoordinates(begin_track,end_track,aux_rear_car,aux_front_car,side); // Now the track is the new NORTH
        trackVector = end_track;
        carVector = aux_front_car;
        scale = trackVector*carVector;

        if(scale < DELTA_SMALL){
          wrongDirection = true;
          scale = -1;  // full turn
          if(carVector.Easting > 0){
            directionSteer = -1;
          }
          else{
            directionSteer = 1;
          }
          scaleSteer = scale;
          scaleVel = (scale+1)/2;
        }
        else{ // Analyse vector field, inside the corridor it must be flat
          // Analysing how off we are related to the desired diraction
          if(scale < cos(75*M_PI/180)){
            wrongDirection = true;
          }
          else{
            wrongDirection = false;
          }
          w = (double)VECTOR_FIELD_WEIGHT;
          offset = targetPoints[lastWaypoint].radius;
          //cout << "OFFSET=" << offset << endl;
          d = distanceCar2TrackLine/offset;

	  /* OLD WAY OF DOING VECTOR FIELD
          x = ((double)side)*d*offset;
          zeta = -(M_PI_2)*((x*x*x)/((w*offset)*(w*offset)*(w*offset)));
          if(abs(zeta) > M_PI_2){
            zeta = -((double)side)*M_PI_2;
          }
	  */
          crossAngle = 3*M_PI/180;
          if(d > 1){
            zeta = -((crossAngle) + (crossAngle)*(d-1)/(w-1));
          }
          else{
            zeta = -(crossAngle)*d;
	  }
          if(abs(zeta) > M_PI_2){
            zeta = -((double)side)*M_PI_2;
          }
          //cout << "ZETA = " << zeta*180/M_PI << endl;

          // Building the vector field
          trackVector.Northing = cos(zeta);
          trackVector.Easting = sin(zeta);
          trackVector.Downing = 0;
          scale = trackVector*carVector;
          directionSteer = 1;
          if(carVector.Easting > 0){
            directionSteer = -1;
          }
          //scaleSteer = scale;

          // Evaluating the angle just like global planner
          angle = abs(acos(scale)); // reduce so it does no wayve so much
          
          if(angle > USE_MAX_STEER){
            angle = USE_MAX_STEER;
          }
          scaleSteer = -2*angle/((double)USE_MAX_STEER) + 1;
          scaleVel = (scale+1)/2;
        }
        // Keeping this values in case all the votes are zero
        scaleVelVecField = scaleVel;
        scaleSteerVecField = scaleSteer;
        directionSteerVecField = directionSteer;

      } // If outside corridor
    } // if next waypoint <  numTargetPoints
  } // if evalCorridor


  if( (!evalCorridor) && (useObsAvoidanceVelocities) ){
    obsAvoidStorage.reserve(numArcs);
    minDist = DBL_MAX;
    maxDist = DBL_MIN;
    for(ind = 0;ind < numArcs;ind++){
      obsAvoid = obstacleAvoidance(steerAngle[ind]);
      obsAvoidStorage[ind].first = obsAvoid.first;
      obsAvoidStorage[ind].second = obsAvoid.second;
      if(obsAvoidStorage[ind].first < minDist) minDist = obsAvoidStorage[ind].first;
      if(obsAvoidStorage[ind].first > maxDist) maxDist = obsAvoidStorage[ind].first;
    }
    for(ind = 0; ind < numArcs; ind++) {
      dist = obsAvoidStorage[ind].first;
      obstacle = obsAvoidStorage[ind].second;

      if(useConfinedSpaceGoodnessVotes) {
        // Make the arc with the highest distance get a vote of 100
        // All other arcs have a goodness proportional to that based on ratio of distances
        if(dist > DIST_TO_OBSTACLE_MAX_SPEED) oneVote.second = MAX_GOODNESS;
        else oneVote.second = (dist / maxDist) * MAX_GOODNESS;
      } else {
        // Shape the goodness vote a bit
        if(steerAngle[ind]){
          PathRadius = abs(VEHICLE_AXLE_DISTANCE/(tan(steerAngle[ind])));  
          if(pathMaxDistance > MAX_PHI*PathRadius)
            oneVote.second = 100*dist/(MAX_PHI*PathRadius);
          else
            oneVote.second = 100*dist/pathMaxDistance;
        }
        else{
          oneVote.second = 100*dist/pathMaxDistance;
        }
      }
      if(oneVote.second > MAX_GOODNESS) oneVote.second = MAX_GOODNESS;

      // Get the velocity 
      oneVote.first = getVelocityVote(dist, obstacle, minDist);
      if(oneVote.first == 0.0) oneVote.second = 0.0;
      votes.push_back(oneVote);
    }
  } else {
    allCorridorVotesLow = true;
    for(ind = 0;ind < numArcs;ind++){
      obsAvoid = obstacleAvoidance(steerAngle[ind]);
      dist = obsAvoid.first;
      obstacle = obsAvoid.second;
      // EVALUATING HOW MUCH WE WOULD SWEEP BEFORE THE NEXT LOOP, GIVEN THE ACTUAL VELOCITY
      delta_dist = curState.Speed*MAX_REACTION_TIME_BETWEEN_SENSOR_SWEEP_AND_BEGINNING_DECELLERATION_FOR_EVALUATING_VELOCITY;
      dist = dist - delta_dist;
      if(dist < 0){
        dist = 0;
      }
  
      // VOTE FROM OBSTACLE AVOIDANCE
      if(steerAngle[ind]){
        PathRadius = abs(VEHICLE_AXLE_DISTANCE/(tan(steerAngle[ind])));  
        if(pathMaxDistance > MAX_PHI*PathRadius)
          oneVote.second = 100*dist/(MAX_PHI*PathRadius);
        else
          oneVote.second = 100*dist/pathMaxDistance;
      }
      else{
        oneVote.second = 100*dist/pathMaxDistance;
      }
      if(oneVote.second > MAX_GOODNESS) oneVote.second = MAX_GOODNESS;
      if(oneVote.second > LOW_GOODNESS_THRESHOLD){
        allCorridorVotesLow=false;
      }
      // Shaping the votes to point in the middle
      //oneVote.second = oneVote.second*obstacleGoodnessShape(ind,numArcs)/100;
  
      // EVALUATING THE WEIGHTING BETWEEN VOTES FROM VECTOR FIELD AND PATH EVALUATION
      if(evalCorridor){
        if(outsideCorridor){
          //cout << "OUTSIDE OF THE CORRIDOR" << endl; 
          alpha = 1;
        }
        else{
          // Going in the wrong direction, but inside the corridor
          if(wrongDirection){
            //cout << "INSIDE BUT WRONG DIRECTION" << endl; 
            alpha = 0.75;
          }
          else{
            //cout << "NORMAL CASE" << endl; 
            // normal situation, trust obstacle avoidance
            //if(abs(distanceCar2TrackLine - targetPoints[lastWaypoint].radius) < 0.1*targetPoints[lastWaypoint].radius)
            alpha = 0.25;
          }
        }
      }
      else{
        alpha = 0;
      }
      
      // SPEED EVALUATION
      if( (evalCorridor) && ((outsideCorridor) || (wrongDirection)) ){
        vel = CORRIDOR_SAFE_SPEED;
      }
      else{
        vel = sqrt(2* NOMINAL_DECELERATION *dist);
      }
      //vel = velShape(ind,numArcs,vel);
      if(numTargetPoints){
        if(vel > targetPoints[lastWaypoint].maxSpeed){
          vel = targetPoints[lastWaypoint].maxSpeed;
        }
      }
      if(vel < MIN_SPEED)
        vel = MIN_SPEED;
      if(vel > MAX_SPEED)
        vel = MAX_SPEED;

      // BUILDING THE VOTES
      oneVote.first = vel;

      // Not change the votes if we are outside of the corridor 
      //if( (evalCorridor) && (outsideCorridor) )
      //oneVote.second = 100;
      //else
      oneVote.second = oneVote.second*(1 - alpha) + alpha*goodnessShape(-directionSteer,ind,numArcs,scaleSteer);

      votes.push_back(oneVote);
    }
    
    // IF ALL VOTES FROM OBSTACLE AVOIDANCE ARE LOW, STOP !!!
    // IF WE ARE ALREADY OUTSIDE OF THE CORRIDOR LET IT COME BACK TO THE TRACK LINE
    /*
    if( (evalCorridor) && (!outsideCorridor) && (allCorridorVotesLow)){
      for(ind = 0;ind < numArcs;ind++){
        votes[ind].first = 0;
        votes[ind].second = 0;
      }
    }
    */
  }
  return votes;
}

// Evaluate the votes based on costs
/* double* generateVotes(double *steer)
 * +DESCRIPTION: This method receive an array of steering angle and evaluate
 *               the goodness for each
 *
 * +PARAMETERS
 *   - steer: steering angle position in radians. Right is positive and left
 *            is negative
 * +RETURN: goodness
 */
vector<double> genMap::generateVotes(int numArcs, vector<double> steerAngle) 
{
  double max_cost;
  vector<double> cost, votes;
  int ind;

  // Reserving room for the elements
  cost.reserve(numArcs);
  votes.reserve(numArcs);

  // Evaluating costs
  max_cost = 0;
  for(ind = 0;ind < numArcs;ind++){
    cost.push_back(evalPathCost(steerAngle[ind]));
    if(cost[ind] > max_cost){
      max_cost = cost[ind];
    }
  }

  // Normalizing and generating votes [0 100]
  for(ind = 0; ind < numArcs; ind++){
    cost[ind] /= max_cost;
    votes.push_back(100*(1 - exp(cost[ind]))/(1 - exp(1.0)));
  }
  return votes;
}


//////////////////////////////////////////////////////////////////////////////
// Frame Transformation Methods                                             //
//////////////////////////////////////////////////////////////////////////////

/* *METHOD RowColDeltaRowCol*
 * +DESCRIPTION: Evaluate the new indices inside the map given the actual
 *    indices and the indices displacement
 * +PARAMETERS
 *  -oldPos  : matrix indices of the old position
 *  -deltaPos: horizontal and vertical displacement in terms of number matrix
 *             indices
 * +RETURN: RowCol of the new position in the matrix
 */
RowCol genMap::RowColFromDeltaRowCol(RowCol oldPos, RowCol deltaPos) 
{
  RowCol newPos;

  if(numCols)
    newPos.first = (numRows + oldPos.first + deltaPos.first)%numRows;
  if(numRows)
    newPos.second = (numCols + oldPos.second + deltaPos.second)%numCols;

  return(newPos);
}

/* *METHOD RowColFrameDeltaPosition*
 * +DESCRIPTION: Evaluate the new indices inside the map given the actual
 *    indices and the north and east displacement in meters
 * +PARAMETERS
 *   -oldPos: matrix indices of the old position
 *   -north_dist: horizontal displacement in meters from the original position.
 *   -east_dist: vertical displacement in meters from the original position.
 * +RETURN: RowCol of the new position in the matrix
 */
RowCol genMap::RowColFromDeltaPosition(RowCol oldPos, double north_dist, 
				       double east_dist) 
{
  RowCol deltaPos, newPos;

  // Verifying the range
  if( (abs(north_dist) > numRows*rowRes) || (abs(east_dist) > numCols*colRes) ){
    //cout << "genMap::RowColFromDeltaPosition  north_dist or east_dist outside of the map range, but returning the normal (wrapped around) value." << endl;
  }
  deltaPos.first = (int)round(north_dist/rowRes);
  deltaPos.second = (int)round(east_dist/colRes);

  // Finding the new indices
  newPos = RowColFromDeltaRowCol(oldPos,deltaPos);

  return(newPos);
}

/* *METHOD FramRef2RowCol*
 * +DESCRIPTION: This method is to evaluate the nwe RowCol in function of
 *    displacements in the frame reference. 
 *    The spot is delta_ahead meters (ahead of the car) and delta_aside meters
 *    (towards the right fo the car). 
 *    Ahead and to the right are the positive convention.
 *    Pass pYaw as zero to use the actual yaw of the car
 * +PARAMETERS
 *  -oldPos     : matrix indices of the old position
 *  -pYaw       : heading of the car
 *  -delta_ahead: horizontal displacement in meters from the original position.
 *  -delta_aside: vertical displacement in meters from the original position.
 * +RETURN: RowCol of the new position in the matrix
 */
RowCol genMap::FrameRef2RowCol(RowCol refPos, double pYaw, double delta_ahead, double delta_aside) 
{
  RowCol newPos;
  double north_dist;       // aligned to east
  double east_dist;       // aligned to north
  double yaw;

  // Getting the curent yaw
  yaw = pYaw;

  // Evaluating the horizontal and vertical displacement
  north_dist = delta_ahead*cos(yaw) - delta_aside*sin(yaw);
  east_dist = delta_ahead*sin(yaw) + delta_aside*cos(yaw);

  // Calculating the RowCol
  newPos = RowColFromDeltaPosition(refPos, north_dist, east_dist);

  return (newPos);
}

pair<double,double> genMap::RowCol2UTM(RowCol pos)
{
  pair<double,double> NorthEast;
  RowCol find, delta;
  double north_disp, east_disp;

  // First, check if the RowCol position is valid
  if (pos.first < 0 || pos.first >= numRows || pos.second < 0 || pos.second >= numCols) 
  {
    cout << "genMap::RowCol2UTM: invalid row/col number" << endl;
    NorthEast.first = 0;
    NorthEast.second = 0;
    return NorthEast;
  }

  find.first = leftBottomRow;
  find.second = leftBottomCol;

  // Counting the number of rows until the desired one
  delta.first = 1;
  delta.second = 0;
  north_disp = 0;
  while(find.first != pos.first)
  {
    find = RowColFromDeltaRowCol(find,delta);
    north_disp += rowRes;
  }

  // Counting the number of rows until the desired one
  delta.first = 0;
  delta.second = 1;
  east_disp = 0;
  while(find.second != pos.second)
  {
    find = RowColFromDeltaRowCol(find,delta);
    east_disp += colRes;
  }

  NorthEast.first = leftBottomUTM_Northing + north_disp + rowRes/2;
  NorthEast.second = leftBottomUTM_Easting + east_disp + colRes/2;

  return (NorthEast);
}

RowCol genMap::UTM2RowCol(double UTM_Northing, double UTM_Easting)
{
  RowCol cell_indices, leftBottomPos; 
  double north_dist, east_dist;

  cell_indices.first = 0;
  cell_indices.second = 0;
  // Verifing if the UTM coordinates is inside the map
  if((leftBottomUTM_Northing > UTM_Northing) || 
     (UTM_Northing > (leftBottomUTM_Northing + numRows*rowRes)))
  {
    //cout << "genMap::UTM2RowCol UTM_Northing out of range" << endl;
    return(cell_indices);
  }
  if((leftBottomUTM_Easting > UTM_Easting) || 
     (UTM_Easting > (leftBottomUTM_Easting + numCols*colRes)))
  {
    //cout << "genMap::UTM2RowCol UTM_Easting out of range" << endl;
    return(cell_indices);
  }

  leftBottomPos.first = leftBottomRow;
  leftBottomPos.second = leftBottomCol;

  north_dist = UTM_Northing - leftBottomUTM_Northing;
  east_dist = UTM_Easting - leftBottomUTM_Easting;
  cell_indices = RowColFromDeltaPosition(leftBottomPos,north_dist,east_dist);

  return cell_indices;
}


//////////////////////////////////////////////////////////////////////////////
// Misc methods                                                             //
//////////////////////////////////////////////////////////////////////////////

void genMap::Copy_VState(VState_GetStateMsg newState)
{
  curState.Timestamp = newState.Timestamp;
  curState.Easting = newState.Easting;
  curState.Northing = newState.Northing;
  curState.Altitude = newState.Altitude;
  curState.Vel_E = newState.Vel_E;
  curState.Vel_N = newState.Vel_N;
  curState.Vel_U = newState.Vel_U;
  curState.Speed = newState.Speed;
  curState.Accel = newState.Accel;
  curState.Pitch = newState.Pitch;
  curState.Roll = newState.Roll;
  curState.Yaw = newState.Yaw;
  curState.PitchRate = newState.PitchRate;
  curState.RollRate = newState.RollRate;
  curState.YawRate = newState.YawRate;
}


// Show map
void genMap::display()
{
  int rowInd,colInd;
  double height;
  RowCol mapPos;
  RowCol vehPos;

  int ind, total;
  total = numCols;

  // Getting the position of the car
  vehPos.first = vehRow;
  vehPos.second = vehCol;
  cout << "Map reference: (row, col) or (north , east)" << endl;
  cout << "Map (" << numRows << "," << numCols << ") with resolution of (" 
       << rowRes << "," << colRes << ") in meters" << endl;
  cout << "Bob (row,col) = (" << vehRow << "," << vehCol << ") UTM(north,east) = (" 
       << curState.Northing << "," << curState.Easting << ")" << endl;
  cout << "Letf bottom corner in RC  is (" << leftBottomRow << "," << leftBottomCol << ")" << endl;
  cout << "Letf bottom corner in UTM is (" << leftBottomUTM_Northing << "," << leftBottomUTM_Easting << ")" << endl;
  cout << endl;
  for(rowInd = (numRows - 1); rowInd >= 0 ;rowInd--) { //for row
    for(colInd = 0; colInd < numCols; colInd++) { //for col
      mapPos.first = rowInd;
      mapPos.second = colInd;

      cout << "| ";
      if(vehPos == mapPos){
        printf("%c%sX%c%s",ESCAPE,RED_COLOR,ESCAPE,DEFAULT);
      }
      else{
        // Colorful
        if(theMap.find(mapPos) == theMap.end()){
          printf(" ");
        }
        else{
          if((theMap.find(mapPos)->second).type == OUTSIDE_CORRIDOR_CELL){
	    printf("%c%sc%c%s", ESCAPE, GREEN_COLOR,ESCAPE, DEFAULT);
          }
          else{
            if((theMap[mapPos]).color == BLUE_INDEX)
              printf("%c%s%1.0f%c%s", ESCAPE, BLUE_COLOR, theMap[mapPos].value, 
		   ESCAPE, DEFAULT);
            else if((theMap[mapPos]).color == RED_INDEX)
              printf("%c%s%1.0f%c%s", ESCAPE, RED_COLOR, theMap[mapPos].value, 
		   ESCAPE, DEFAULT);
            else if((theMap[mapPos]).color == GREEN_INDEX)
              printf("%c%s%1.0f%c%s", ESCAPE, GREEN_COLOR, theMap[mapPos].value, 
		   ESCAPE, DEFAULT);
            else // Black and white
              printf("%1.0f",theMap[mapPos].value);
          }
        }
      }
      cout << " ";
    } //for col
    cout <<  "|" << endl;
    for(ind = 0; ind < total; ind++)
      cout << "----";
    cout << "-" << endl;
  } //for row
}

/* genMap::saveMATFile(char *filename, char *header_message, 
 *			char *variable_name)
 * filename: the name of the file (without the extension) in which genMap data
 *           will be stored.
 * header_message: Please put in info like the author, module, date, test info,
 *                 etc.
 */
int genMap::saveMATFile(char *filename, char *header_message, 
			char *variable_name)
{
  int status = 0;
  CellData current_item;
  int list_length;
  double current_list_member;
  char file[100];                   // Jeremy told me to make it big :P

  
  // filename may include relative path information
  // There may be a cleaner way, but this is my rough cut at getting the
  // filename without a relative path to appear at the top of the MATFile
  // -Lars
  char filenopath[100];
  //printf("filename = %s\n", filename);
  //printf("strlen(filename) = %d\n", strlen(filename));
  //printf("filename[strlen(filename)-1] = %c\n", filename[strlen(filename)-1]);
  // Taken from http://www.gnu.org/software/libc/manual/html_node/Finding-Tokens-in-a-String.html
  char * running;
  char * token;
  running = strdupa( filename );
  token = strsep( &running, "/" );
  //printf("token = %s\n", token );
  while( token != NULL ) {
    strcpy(filenopath, token);
    token = strsep( &running, "/" );
    //printf("token = %s\n", token );
  }
  //printf( "filenopath = %s\n", filenopath );
  // This ends my appended code to get the filename with no path. -Lars
  
  
  strcpy(file, filename);
  ofstream MATFile(strcat(file, ".m"), ios::out);
  //  FILE *fptr = fopen(file);

  if(MATFile.fail())
  {
    status = 1;
    cout << "could not open file " << filename << endl;
    return status;
  }

  //cout << "Starting saveMATfile" << endl;
  // Output function header info
  MATFile << "function [rowRes, colRes, leftBottomUTM_Northing, leftBottomUTM_Easting, "
          << variable_name << "] = " << filenopath << endl;
  MATFile << "%" << endl;
  MATFile << "% function " << filename << endl;
  MATFile << "%" << endl;
  MATFile << "% Changes:" << endl;
  // Put in the input header message.
  EditHeader(header_message);
  MATFile << "%   " /* << date */ 
	  << "  Created by genMap::saveMATFile." << endl;
  MATFile << "%   " << header_message << endl;
  // Then the rest of the general header info.
  MATFile << "%" << endl;
  MATFile << "% Function:" << endl;
  MATFile << "%  The function returns the genMap data in the form of a 5-element array, so" << endl;
  MATFile << "%  that another function/script may read the data and display, etc." << endl;
  MATFile << "%" << endl;
  MATFile << "% Usage example: " << endl;
  MATFile << "%  To display the information stored in this file, use team/matlab/viewGenMap.m" << endl;
  MATFile << "%" << endl;
  MATFile << "% Note: " << endl;
  MATFile << "%   Currently, you MUST put in only one map into each .m file and follow the" << endl;
  MATFile << "%   format exactly if you want to use viewGenMap.m to display data. This file" << endl;
  MATFile << "%   should have been generated by genMap::saveMATFile." << endl;
  MATFile << "%" << endl;
  MATFile << "% Vehicle Position = N: " << fixed << curState.Northing;
  //  fprintf(fptr, "%10.3f", curState.Northing);
  MATFile << " E: " << fixed << curState.Easting << endl; 
  //  fprintf(fptr, "%10.3f\n", curState.Easting);
  MATFile << "% Vehicle Heading = " << fixed << curState.Yaw << endl;
  //fprintf(fptr, "%10.3f\n", curState.Yaw);
  MATFile << "%" << endl << endl;

  // Finally, put in some data.
  // Map dimension/UTM coodinate info
  MATFile << "rowRes = " << fixed << rowRes << endl;
  //fprintf(fptr, "%10.3f\n", rowRes);
  MATFile << "colRes = " << fixed << colRes << endl;
  //fprintf(fptr, "%10.3f\n", colRes);
  MATFile << "leftBottomUTM_Northing = " << fixed << leftBottomUTM_Northing << endl;
  //fprintf(fptr, "%10.3f\n", leftBottomUTM_Northing);
  MATFile << "leftBottomUTM_Easting = " << fixed << leftBottomUTM_Easting << endl;
  //fprintf(fptr, "%10.3f\n", leftBottomUTM_Easting);

  // Map data (matrices)
  MATFile << variable_name << ".value = [\n";
  for(int row = (leftBottomRow+numRows-1); row >= leftBottomRow; row--)
    {
      //cout << "saving  values for row = " << row << " of " << numRows <<  endl;
      for(int col = leftBottomCol; col < leftBottomCol+numCols; col++)
	{
	  MATFile << (getCellDataRC(row%numRows, col%numCols)).value << "\t";
	}
      if (row > leftBottomRow) {
        MATFile << ";" << endl;
      }
    }
  MATFile << "];\n";

  MATFile << variable_name << ".color = [\n";
  for(int row = (leftBottomRow+numRows-1); row >= leftBottomRow; row--)
    {
      //cout << "saving color for row = " << row << " of " << numRows <<  endl;
      for(int col = leftBottomCol; col < leftBottomCol+numCols; col++)
        {
          MATFile << (getCellDataRC(row%numRows, col%numCols)).type << "\t";
        }
      if (row > leftBottomRow) {
        MATFile << ";" << endl;
      }
    }
  MATFile << "];\n";

  MATFile << variable_name << ".color = [\n";
  for(int row = (leftBottomRow+numRows-1); row >= leftBottomRow; row--)
    {
      //cout << "saving color for row = " << row << " of " << numRows <<  endl;
      for(int col = leftBottomCol; col < leftBottomCol+numCols; col++)
        {
          MATFile << (getCellDataRC(row%numRows, col%numCols)).type << "\t";
        }
      if (row > leftBottomRow) {
        MATFile << ";" << endl;
      }
    }
  MATFile << "];\n";

  MATFile << variable_name << ".list = {\n";
  for(int row = (leftBottomRow+numRows-1); row >= leftBottomRow; row--)
    {
      //cout << "saving list for row = " << row << " of " << numRows <<  endl;
      for(int col = leftBottomCol; col < leftBottomCol+numCols; col++)
        {
	  MATFile << "[ ";
	  current_item = getCellDataRC(row%numRows, col%numCols);
	  /*list_length = sizeof(current_item);
	  for(int list_i = 0; list_i < list_length; list_i++)
	    {
	      current_item = getCellDataRC(row%numRows, col%numCols);
	      //current_list_member = (current_item.cellList).front;
              //MATFile << current_list_member << "\t";
	    }*/
	  MATFile << " ]\t";
        }
      if(row > leftBottomRow) {
	MATFile << ";" << endl;
      }
    }
  MATFile << "};\n";

  // Done
  MATFile.close();
  //cout << "We have successfully saved the map.\n";
  return status;
}

/* *METHOD loadMATfile
 *
 * +DESCRIPTION: This method reads the file generated by saveMATfile and store it into
 *  theMap as initial values
 *
 * +PARAMETERS
 *
 *  - filename : the name of the ".m"  file
 *
 * +RETURN: BAD_MAT_FILE        : when the file is not there
 *          BAD_MAT_FILE_FORMAT : when the format is not as expected
 */
int genMap::loadMATFile(char *filename)
{
  VState_GetStateMsg newState;
  // pair.first = line index where the matrix begins
  // pair.second = line index where the matrix ends
  // lineIndices from the value matrix inside the file
  pair<int,int> valueIndices;  
  // lineIndices from the color matrix inside the file
  pair<int,int> colorIndices;  
  // lineIndices from the each_cell_list matrix inside the file
  pair<int,int> listIndices;   
  vector<VBString> lines;
  VBString line, varName, strList, data;
  char tok = '|';
  char tokList = ';';
  double pRowRes,pColRes,pLeftBottomUTM_Northing,pLeftBottomUTM_Easting;
  int rowResInd,colResInd,leftBottomUTM_NorthingInd,leftBottomUTM_EastingInd;
  ifstream file;
  int lineIndex, numLines;
  int pNumRows, pNumCols, auxRows, auxCols;
  int row,col;
  int indValue;
  int indColor;
  int indList;
  int numElem, indElem;
  RowCol mapPos;

  file.open(filename,ios::in);
  if(!file){
    cout << "genMap::saveMATFILE : UNABLE TO OPEN THE FILE" << endl;
    return(BAD_MAT_FILE);
  }
  numLines = 0;
  while(file >> line){
    lines.push_back(line);
    numLines++;
  }
  // debug 
  // cout << "numLines = " << numLines <<  endl;

  // Findind where the values matrix begins and ends and position of rowRes,
  // colRes, left bottom UTM coordinates
  // lineIndex = 0 is where the function header is and has no '%' marker.
  lineIndex = 1;
  while( (lineIndex < numLines) && (lines[lineIndex].existSubstring("%")) ){
    lineIndex++;
  }
  if(lineIndex == numLines){
    cout << "genMap::saveMATFile : % is on every line.\n";
    return(BAD_MAT_FILE_FORMAT);
  }
  while(  (lineIndex < numLines) && 
	  ( !(lines[lineIndex].existSubstring("=")) )  ) {
    lineIndex++;
  }
  if(lineIndex == numLines){
    cout << "genMap::saveFile : No assignment occurs after comments.\n";
    return(BAD_MAT_FILE_FORMAT);
  }
  rowResInd = lineIndex; lineIndex++;
  colResInd = lineIndex; lineIndex++;
  leftBottomUTM_NorthingInd = lineIndex; lineIndex++;
  leftBottomUTM_EastingInd = lineIndex; lineIndex++;
  valueIndices.first = lineIndex;
  // debug 
  // cout << "valueIndices.first = " << valueIndices.first <<  endl;
  // cout << "rowResInd = " << rowResInd <<  endl;
  // cout << "colResInd = " << colResInd <<  endl;
  // cout << "leftBottomUTM_NorthingInd = " <<  leftBottomUTM_NorthingInd <<  endl;
  // cout << "leftBottomUTM_EastingInd = " <<  leftBottomUTM_EastingInd <<  endl;

  while( (lineIndex < numLines) && (!lines[lineIndex].existSubstring("];")) ){
    lineIndex++;
  }
  if(lineIndex == numLines){
    cout << "No line has ];1\n";
    return(BAD_MAT_FILE_FORMAT);
  }
  valueIndices.second = lineIndex;
  // debug 
  //cout << "valueIndices.second = " << valueIndices.second <<  endl;

  // Findind where the values matrix begins and ends 
  //( it begins one line after the previous matrix ends)
  lineIndex++;
  colorIndices.first = lineIndex;
  // debug 
  //cout << "colorIndices.first = " << colorIndices.first <<  endl;

  while( (lineIndex < numLines) && (!lines[lineIndex].existSubstring("];")) ){
    lineIndex++;
  }
  if(lineIndex == numLines){
    cout << "No line has ];2\n";
    return(BAD_MAT_FILE_FORMAT);
  }
  colorIndices.second = lineIndex;
  // debug 
  //cout << "colorIndices.second = " << colorIndices.second <<  endl;

  // Findind where the cell list  matrix begins and ends 
  //( it begins one line after the previous matrix ends)
  lineIndex++;
  listIndices.first = lineIndex;
  // debug 
  //cout << "listIndices.first = " << listIndices.first <<  endl;

  while( (lineIndex < numLines) && (!lines[lineIndex].existSubstring("};")) ){
    lineIndex++;
  }
  if(lineIndex == numLines){
    cout << "No line has };\n";
    return(BAD_MAT_FILE_FORMAT);
  }
  listIndices.second = lineIndex;
  // debug 
  //cout << "listIndices.second = " << listIndices.second <<  endl;

  // Verifying whether the lines with the variable names are in the format
  //    rowRes = x.xx;
  //    colRes = x.xx;
  //    leftBottomUTM_Northing = x.xx;
  //    leftBottomUTM_Easting = x.xx;
  //    var_name.value = [
  //    var_name.color = [
  //    var_name.list  = {
  if( (!(lines[rowResInd].existSubstring("rowRes"))) || 
      (!(lines[rowResInd].existSubstring("="))) ) {
    cout << "No rowRes = \n";
    return(BAD_MAT_FILE_FORMAT);
  }
  if( (!(lines[colResInd].existSubstring("colRes"))) || 
      (!(lines[colResInd].existSubstring("="))) ) {
    cout << "No colRes = \n";
    return(BAD_MAT_FILE_FORMAT);
  }
  if( (!(lines[leftBottomUTM_NorthingInd].existSubstring("leftBottomUTM_Northing"))) || 
      (!(lines[leftBottomUTM_NorthingInd].existSubstring("="))) ) {
    cout << "No leftBottomUTM_Northing =\n";
    return(BAD_MAT_FILE_FORMAT);
  }
  if( (!(lines[leftBottomUTM_EastingInd].existSubstring("leftBottomUTM_Easting"))) || 
      (!(lines[leftBottomUTM_EastingInd].existSubstring("="))) ) {
    cout << "No leftBottomUTM_Easting =\n";
    return(BAD_MAT_FILE_FORMAT);
  }
  if( !( (lines[valueIndices.first].existSubstring(".")) && 
	 (lines[valueIndices.first].existSubstring("=")) &&
	 (lines[valueIndices.first].existSubstring("[")) ) ){
    cout << "no .value = \n";
    return(BAD_MAT_FILE_FORMAT);
  }
  if( !( (lines[colorIndices.first].existSubstring(".")) && 
	 (lines[colorIndices.first].existSubstring("=")) &&
	 (lines[colorIndices.first].existSubstring("[")) ) ){
    cout << "no .color = \n";
    return(BAD_MAT_FILE_FORMAT);
  }
  /*
  if( !( (lines[listIndices.first].existSubstring(".")) && 
	 (lines[listIndices.first].existSubstring("=")) &&
	 (lines[listIndices.first].existSubstring("{")) ) ){
    cout << "no .list = \n";
    return(BAD_MAT_FILE_FORMAT);
  }
  */

  // Getting the variable name, it has to be the same and exists on the first line
  varName = lines[valueIndices.first].beforeFind(".");
  if(!lines[0].existSubstring(varName)){
    cout << varName << " of value is not on first line of file.\n";
    return(BAD_MAT_FILE_FORMAT);
  }
  if( (varName != lines[listIndices.first].beforeFind(".")) || 
      (varName != lines[colorIndices.first].beforeFind(".")) ) {
    cout << varName << "of list is not on first line of file.\n"; 
    return(BAD_MAT_FILE_FORMAT);
  }

  // Extraction ";", "]", }" and other characters characteres and counting rows/cols
  pNumRows = 0; pNumCols = 0;
  auxRows  = 0; auxCols  = 0;
  lines[colResInd].replace(";","");
  lines[rowResInd].replace(";","");
  lines[leftBottomUTM_NorthingInd].replace(";","");
  lines[leftBottomUTM_EastingInd].replace(";","");
  for(lineIndex=valueIndices.first+1; lineIndex <= valueIndices.second; 
      lineIndex++){
    lines[lineIndex].replace(";","");
    lines[lineIndex].replace("]","");
    //This is for hand-typed MATFiles, because tab does not work.
    lines[lineIndex].replace(" ","|"); 
    lines[lineIndex].replace("\t","|");
    auxCols = lines[lineIndex].tokCount(tok);
    if( (auxCols != pNumCols) && (pNumCols) ){
      cout << "VALUE Disagreement in number of columns of values: " << pNumCols 
	   << " vs " << auxCols << endl; 
      return(BAD_MAT_FILE_FORMAT);
    }
    pNumCols = auxCols;
    auxRows++;
  }
  pNumRows = auxRows;
  // debug 
  //cout << "pNumRows = " << pNumRows << " pNumCols = " << pNumCols << endl;

  auxRows  = 0; auxCols  = 0;
  for(lineIndex=colorIndices.first+1; lineIndex <= colorIndices.second; 
      lineIndex++){
    lines[lineIndex].replace(";","");
    lines[lineIndex].replace("]","");
    //This is for hand-typed MATFiles, because tab does not work. 
    lines[lineIndex].replace(" ","|");
    lines[lineIndex].replace("\t","|");
    auxCols = lines[lineIndex].tokCount(tok);
    if( auxCols != pNumCols ){
      cout << "COLOR Disagreement in number of columns of colors: " << auxCols 
	   << " vs " << pNumCols << endl;
      return(BAD_MAT_FILE_FORMAT);
    }
    pNumCols = auxCols;
    auxRows++;
  }
  if( auxRows != pNumRows ){
    cout << "COLOR 2 Disagreement in number of rows of colors: " << auxRows << " vs " 
	 << pNumRows << endl;
    return(BAD_MAT_FILE_FORMAT);
  }  
  // debug 
  //cout << "pNumRows = " << pNumRows << " pNumCols = " << pNumCols << endl;

  /*
  auxRows  = 0; auxCols  = 0;
  for(lineIndex=listIndices.first+1; lineIndex <= listIndices.second; 
      lineIndex++){
    lines[lineIndex].replace(";","");
    lines[lineIndex].replace("{",""); 
    lines[lineIndex].replace("}","");
    lines[lineIndex].replace("[","");
    // Tab does not work when one is filling in the file by hand. 
    lines[lineIndex].replace("] ", "|"); 
    lines[lineIndex].replace("]\t","|"); // for different lists
    // for different elements for a given list
    lines[lineIndex].replace("\t",";");  
    char tok = '|';
    auxCols = lines[lineIndex].tokCount(tok);
    if( auxCols != pNumCols ){
      cout << "line " << lineIndex << endl;
      cout << "LIST disagreement in number of columns of lists: " << auxCols 
	   << " vs " << pNumCols << endl;
      return(BAD_MAT_FILE_FORMAT);
    }
    pNumCols = auxCols;
    auxRows++;
  }
  if( auxRows != pNumRows ){
    cout << "disagreement in number of rows of lists: " << auxRows << " vs " 
	 << pNumRows << endl;
    return(BAD_MAT_FILE_FORMAT);
  }  
  */
  // debug 
  //cout << "pNumRows = " << pNumRows << " pNumCols = " << pNumCols << endl;

  // Finally loading the data !!!
  data = lines[colResInd].afterFind("=");
  pRowRes = atof(data);
  data = lines[rowResInd].afterFind("=");
  pColRes = atof(data);
  data = lines[leftBottomUTM_NorthingInd].afterFind("=");
  pLeftBottomUTM_Northing = atof(data);
  data = lines[leftBottomUTM_EastingInd].afterFind("=");
  pLeftBottomUTM_Easting = atof(data);
  
  initMap(pNumRows,pNumCols,pRowRes,pColRes,false);

  indValue = valueIndices.first+1;
  indColor = colorIndices.first+1;
  indList  = listIndices.first+1;
  for(row=(pNumRows-1); row >= 0; row--){
    for(col=0; col < pNumCols; col++){
      CellData newCD;
      // Filling value and color
      newCD.value = atof(lines[indValue].strtok(tok,col));
      newCD.type = atoi(lines[indColor].strtok(tok,col));
      // debug
      //system("clear");
      //cout << "(row,col) = (" << row << "," << col << ")" <<  endl;
      //cout << "Value = " << newCD.value << " Type = " << newCD.type << endl;

      // Filling list
      /*
      strList = lines[indList].strtok(tok,col);
      numElem = strList.tokCount(tokList);
      // debug
      //cout << "List = ( ";
      for(indElem=0; indElem < numElem; indElem++){
        if(numElem == 1)
          data = strList;
        else
          data = strList.strtok(tokList,indElem);
        newCD.cellList.push_back(atof(data));
        // debug
	//cout << atof(data) << " ";
      }
      */
      // debug
      //cout << ")" << endl;      
      //getchar();

      // Adding to theMap 
      mapPos.first = row;
      mapPos.second = col;
      if ( theMap.find(mapPos) == theMap.end() ) {
        theMap.insert(map<RowCol, CellData>::value_type(mapPos, newCD));
      }
      else {
        ((theMap.find(mapPos))->second) = newCD;
      }
    }
    indValue++;
    indColor++;
    indList++;
  }  
  // Updating some variables, some of them has to be added to the MAT file
  newState.Northing = pLeftBottomUTM_Northing + (double)pNumRows*pRowRes/2 + pRowRes/2;;
  newState.Easting = pLeftBottomUTM_Easting + (double)pNumCols*pColRes/2 + pColRes/2;;
  Copy_VState(newState);
  leftBottomUTM_Northing = pLeftBottomUTM_Northing;
  leftBottomUTM_Easting  = pLeftBottomUTM_Easting;
//  cout << "max_path = " << pathMaxDistance << endl;
  return(0);
}


//Saves the genMap as an image file of some type
int genMap::saveIMGFile(char *filename) 
{
  CellData tempCell;
  double highest_val, lowest_val, value;
  char *imageBuffer;

  imageBuffer = (char*)malloc(numRows*numCols);

  tempCell = getCellDataRC(0,0);
  highest_val = tempCell.value;
  lowest_val = tempCell.value;

  for(int rowNumber=0; rowNumber < numRows; rowNumber++) {
    for(int colNumber=0; colNumber < numCols; colNumber++) {
      RowCol findingIndices(rowNumber, colNumber);
      tempCell = getCellDataRC(rowNumber, colNumber);
      if(tempCell.value > highest_val) highest_val = tempCell.value;
      if(tempCell.value < lowest_val) lowest_val = tempCell.value;
    }
  }

  printf("Highest %lf, lowest %lf\n", highest_val, lowest_val);

  for(int rowNumber=0; rowNumber < numRows; rowNumber++) {
    for(int colNumber=0; colNumber < numCols; colNumber++) {
      RowCol findingIndices(rowNumber, colNumber);
      tempCell = getCellDataRC(rowNumber, colNumber);
      value = tempCell.value;
      //*(imageBuffer + rowNumber*numCols + colNumber) = (char)(int)((value - lowest_val)/(highest_val - lowest_val) * 256);
      imageBuffer[rowNumber*numCols + colNumber] = 
	(char)(int)((value - lowest_val)/(highest_val - lowest_val) * 256);
      if(value!=0) {
	//printf("Val: %lf\n", value);
	//printf("Val: (%d, %d, %d)\n", colNumber, rowNumber, (int)((value - lowest_val)/(highest_val - lowest_val) * 256));
      }
    }
  }

  FILE* imagefile;
  imagefile=fopen(filename, "w");
  if( imagefile == NULL) {
    fprintf( stderr, "Can't create '%s'", filename);
  } else {
    fprintf(imagefile,"P5\n%u %u 255\n", numRows, numCols);
    fwrite((const char *)imageBuffer, 1, numRows*numCols, imagefile);
    fclose(imagefile);
  }

}

/* int saveNorthEastDownonlyFile(char *filename)
 * Saves the genMap's value (height, usually) data in three columns:
 * NORTH(UTM) EAST(UTM) DOWN(m)
 */
int genMap::saveNorthEastDownOnlyFile(char *filename)
{
  int status = 0;
  CellData current_item;
  int list_length;
  double current_list_member;
  ofstream file(filename, ios::out);

  if(file.fail())
  {
    status = 1;
    cout << "could not open file " << filename << endl;
    return status;
  }

  // Map 'value' data
  for(int row = leftBottomRow; row < leftBottomRow+numRows; row++) {
    for(int col = leftBottomCol; col < leftBottomCol+numCols; col++) {
      file << fixed << leftBottomUTM_Easting + ((col-leftBottomCol) * colRes) << " ";
      file << fixed << leftBottomUTM_Northing + ((row-leftBottomRow) * rowRes) << " ";
      file << fixed << (getCellDataRC(row%numRows, col%numCols)).value << endl;
    }
  }

  // Done
  file.close();
  //cout << "genMap:saveNorthEastDownonlyFile: We have successfully saved the map.\n";
  return status;
}



int genMap::findMaxListLength()
{
  int max_length = 0, new_size;
  for(int row = leftBottomRow; row < leftBottomRow+numRows; row++)
    {
      for(int col = leftBottomCol; col < leftBottomCol+numCols; col++)
        {
	  new_size = sizeof((getCellDataRC(row, col)).cellList);
	  if(new_size > max_length)
	    {
	      max_length = new_size;
	    }           
        }
    }
  return max_length;
}


void genMap::EditHeader(charptr &header_message)
{
  int length = strlen(header_message);
  for(int i=0; i < length; i++)
    {
      if((header_message[i] == '\n')&&(i < (length - 1)))
	{
	  header_message[i+1] = '%';
	}
    }
}

/* *METHOD isInsideCorridor
 *
 * +DESCRIPTION: This method receives a waypoint index and go to WP list to get information
 *               about this corridor such as offset boundary as retunrs wheather or not the
 *               point is inside the corridor
 *
 * +PARAMETERS
 *
 *  - waypointNumber: the index of the waypoint list
 *  - point         : a point in UTM coordinates
 *
 * +RETURN: TRUE if inside FALSE if not
 */
bool genMap::isInsideCorridor(int waypointNumber, 
			      pair<double,double> NorthEast)
{
  bool isInside=false;
  bool pointCrossesLine;
  pair<bool,double> pointLine;
  double dist_point2ab=0;
  NorthEastDown a,b; // points that determine the lines between lastWaypoint to nextEaypoint to the next one o----o
  NorthEastDown point;
  if(waypointNumber+1 < numTargetPoints){
    // Point
    point.Northing = NorthEast.first;
    point.Easting = NorthEast.second;
    point.Downing = 0;
    // Last Waypoint
    a.Northing = targetPoints[waypointNumber].Northing;
    a.Easting = targetPoints[waypointNumber].Easting;
    a.Downing = 0;
    // Next waypoint
    b.Northing = targetPoints[waypointNumber+1].Northing;
    b.Easting = targetPoints[waypointNumber+1].Easting;
    b.Downing = 0;

    // Distance between point and lines
    pointLine = distancePoint2Line(a,b,point);
    pointCrossesLine = pointLine.first;

    dist_point2ab = pointLine.second;
    if( (pointCrossesLine) && (dist_point2ab < targetPoints[waypointNumber].radius) ){
      isInside=true;
    }
  }
  return(isInside);
}


/* *METHOD isInsideWaypointRadius
 *
 * +DESCRIPTION: This method receives a waypoint index and go to WP list to get information
 *               about its target radius as retunrs wheather or not the point is inside the
 *               waypoint
 *
 * +PARAMETERS
 *
 *  - waypointNumber: the index of the waypoint list
 *  - point         : a point in UTM coordinates
 *
 * +RETURN: TRUE if inside FALSE if not
 */
bool genMap::isInsideWaypointRadius(int waypointNumber, 
				    pair<double,double> NorthEast)
{
  bool isInside=false;
  NorthEastDown point, center;
  double dist;
  double lastWaypointRadius=0;

  if(waypointNumber < numTargetPoints){
    // Point
    point.Northing = NorthEast.first;
    point.Easting = NorthEast.second;
    point.Downing = 0;
    // Center of the next waypoint
    center.Northing = targetPoints[waypointNumber].Northing;
    center.Easting = targetPoints[waypointNumber].Easting;
    center.Downing = 0;

    // Evaluating the distance
    dist = sqrt( (point.Northing - center.Northing)*(point.Northing - center.Northing) + (point.Easting - center.Easting)*(point.Easting - center.Easting) );

    // if we are not at waypoint zero, consider the last waypoint radius
    if( waypointNumber ) {
      lastWaypointRadius = targetPoints[waypointNumber-1].radius;
    }

    // check to see if we are within the union of the discs formed by the 
    // current and previous waypoint
    if(dist  < fmax(targetPoints[waypointNumber].radius, lastWaypointRadius))
      isInside = true;
  }
  else{
    isInside = true; // final waypoint!!!
  }
  return(isInside);
}

/* *METHOD isInsidePath
 *
 * +DESCRIPTION: This method receives a UTM coordinate point and verify wheather or not the
 *               point is inside the allowed path. It uses the index of the last/next waypoints
 *
 * +PARAMETERS
 *
 *  - point         : a point in UTM coordinates
 *
 * +RETURN: TRUE if inside FALSE if not
 */
bool genMap::isInsidePath(pair<double,double> NorthEast)
{
  // the one before
  bool isInside=false;
  // look for waypoints inside the map
  bool waypointOutsideMap;
  double minNorthing, maxNorthing;
  double minEasting, maxEasting;
  double heigth, width;
  int indWaypoint;

  // Previous wayPoint, before the one we just got
  if(lastWaypoint){
    //cout << "checking waypoint " << lastWaypoint << endl;
    if( (isInsideWaypointRadius(lastWaypoint-1,NorthEast)) || (isInsideCorridor(lastWaypoint-1,NorthEast)) ){
      isInside = true;
    }
  }
  
  // last waypoint
  //cout << "checking waypoint " << lastWaypoint << endl;
  if( (isInsideWaypointRadius(lastWaypoint,NorthEast)) || (isInsideCorridor(lastWaypoint,NorthEast)) ){
    isInside = true;
  }

  // Deermining the Northin and Easting inside the map
  heigth = numRows*rowRes;
  width = numCols*colRes;
  minNorthing = leftBottomUTM_Northing - ENLARGE_MAP_HOW_MANY_TIMES*heigth;
  maxNorthing = minNorthing + heigth + ENLARGE_MAP_HOW_MANY_TIMES*heigth;
  minEasting = leftBottomUTM_Easting - ENLARGE_MAP_HOW_MANY_TIMES*width;
  maxEasting = minEasting + width + ENLARGE_MAP_HOW_MANY_TIMES*width;
 
  // Looking for next waypoints that is inside the map
  indWaypoint=nextWaypoint;
  waypointOutsideMap=false;  
  while((indWaypoint < numTargetPoints) && (!waypointOutsideMap)){
    if( (isInsideWaypointRadius(indWaypoint,NorthEast)) || (isInsideCorridor(indWaypoint,NorthEast)) ){
      isInside = true;
    }
    if((targetPoints[indWaypoint].Northing <= minNorthing) || (targetPoints[indWaypoint].Northing >= maxNorthing)){
      waypointOutsideMap = true;
    }
    if((targetPoints[indWaypoint].Easting <= minEasting) || (targetPoints[indWaypoint].Easting >= maxEasting)){
      waypointOutsideMap = true;
    }
    indWaypoint++;
  }


  /* DEBUG */
  /*
  for(int i=0;i<numTargetPoints;i++){
    if( (isInsideWaypointRadius(i,NorthEast)) || (isInsideCorridor(i,NorthEast)) ){
      isInside = true;
    }
  }
  */

  return(isInside);
}


/* *METHOD paintCorridor
 *
 * +DESCRIPTION: This method receives the rows and cols to analyze and paint
 *  inside the corridor as a good terrain and outside as bad terrain
 *
 * +PARAMETERS
 *
 *  - startPoint : the pair <row,col> on the map we will start to paint the corridor
 *  - deltaPoint : the pair <Drow,Dcol> indicating how many rows and colls
 *
 * +RETURN: void
 */
void genMap::paintCorridor(RowCol startPoint,RowCol deltaPoint)
{
  pair<double,double> NorthEast;
  RowCol curPoint, incRow,incCol, finalPoint;
  // cells outside corridor
  CellData newCD = OUTSIDE_CELL;
  int signDeltaRow=1;
  int signDeltaCol=1;
  int row, col;

  if(deltaPoint.first < 0)
    signDeltaRow = -1;
  if(deltaPoint.second < 0)
    signDeltaCol = -1;

  // If the window swept more than the map size
  if( (signDeltaRow*deltaPoint.first >= numRows) || 
      (signDeltaCol*deltaPoint.second >= numCols) ){
    for(row=0; row<numRows; row++){
      for(col=0; col<numCols; col++){
        curPoint.first = row;
        curPoint.second = col;
        NorthEast = RowCol2UTM(curPoint);
        if(!isInsidePath(NorthEast)){
          setCellDataRC(curPoint.first, curPoint.second, newCD);
        }
      } // for col
    } // for row
  }
  else{
    finalPoint = RowColFromDeltaRowCol(startPoint,deltaPoint);

    // Evaluating the rows
    if(finalPoint.first != startPoint.first){
      incRow.first = 1*signDeltaRow;
      incRow.second = 0;
      curPoint = startPoint;
      do{
        for(col=0; col<numCols; col++){
          curPoint.second = col;
          NorthEast = RowCol2UTM(curPoint);
          if(!isInsidePath(NorthEast)){
            setCellDataRC(curPoint.first, curPoint.second, newCD);
          }
        }
        curPoint = RowColFromDeltaRowCol(curPoint,incRow);
      }while(curPoint.first != finalPoint.first);
    }

    // Evaluating the cols
    if(finalPoint.second != startPoint.second){
      incCol.second = 1*signDeltaCol;
      incCol.first = 0;
      curPoint = startPoint;
      do{
        for(row=0; row<numRows; row++){
          curPoint.first = row;
          NorthEast = RowCol2UTM(curPoint);
          if(!isInsidePath(NorthEast)){
            setCellDataRC(curPoint.first, curPoint.second, newCD);
          }
        }
        curPoint = RowColFromDeltaRowCol(curPoint,incCol);
      }while(curPoint.second != finalPoint.second);
    }
  } // End If the window swept more than the map size
}


int genMap::saveSIZEFile(char *filename, char *header_message, 
			char *variable_name)
{
  int status = 0;
  CellData current_item;
  int list_length;
  double current_list_member;
  char file[100];                   // Jeremy told me to make it big :P

  
  // filename may include relative path information
  // There may be a cleaner way, but this is my rough cut at getting the
  // filename without a relative path to appear at the top of the MATFile
  // -Lars
  char filenopath[100];
  //printf("filename = %s\n", filename);
  //printf("strlen(filename) = %d\n", strlen(filename));
  //printf("filename[strlen(filename)-1] = %c\n", filename[strlen(filename)-1]);
  // Taken from http://www.gnu.org/software/libc/manual/html_node/Finding-Tokens-in-a-String.html
  char * running;
  char * token;
  running = strdupa( filename );
  token = strsep( &running, "/" );
  //printf("token = %s\n", token );
  while( token != NULL ) {
    strcpy(filenopath, token);
    token = strsep( &running, "/" );
    //printf("token = %s\n", token );
  }
  //printf( "filenopath = %s\n", filenopath );
  // This ends my appended code to get the filename with no path. -Lars
  
  
  strcpy(file, filename);
  ofstream MATFile(strcat(file, ".m"), ios::out);
  //  FILE *fptr = fopen(file);

  if(MATFile.fail())
  {
    status = 1;
    cout << "could not open file " << filename << endl;
    return status;
  }

  //cout << "Starting saveMATfile" << endl;
  // Output function header info
  MATFile << "function [rowRes, colRes, leftBottomUTM_Northing, leftBottomUTM_Easting, "
          << variable_name << "] = " << filenopath << endl;
  MATFile << "%" << endl;
  MATFile << "% function " << filename << endl;
  MATFile << "%" << endl;
  MATFile << "% Changes:" << endl;
  // Put in the input header message.
  EditHeader(header_message);
  MATFile << "%   " /* << date */ 
	  << "  Created by genMap::saveMATFile." << endl;
  MATFile << "%   " << header_message << endl;
  // Then the rest of the general header info.
  MATFile << "%" << endl;
  MATFile << "% Function:" << endl;
  MATFile << "%  The function returns the genMap data in the form of a 5-element array, so" << endl;
  MATFile << "%  that another function/script may read the data and display, etc." << endl;
  MATFile << "%" << endl;
  MATFile << "% Usage example: " << endl;
  MATFile << "%  To display the information stored in this file, use team/matlab/viewGenMap.m" << endl;
  MATFile << "%" << endl;
  MATFile << "% Note: " << endl;
  MATFile << "%   Currently, you MUST put in only one map into each .m file and follow the" << endl;
  MATFile << "%   format exactly if you want to use viewGenMap.m to display data. This file" << endl;
  MATFile << "%   should have been generated by genMap::saveMATFile." << endl;
  MATFile << "%" << endl;
  MATFile << "% Vehicle Position = N: " << fixed << curState.Northing;
  //  fprintf(fptr, "%10.3f", curState.Northing);
  MATFile << " E: " << fixed << curState.Easting << endl; 
  //  fprintf(fptr, "%10.3f\n", curState.Easting);
  MATFile << "% Vehicle Heading = " << fixed << curState.Yaw << endl;
  //fprintf(fptr, "%10.3f\n", curState.Yaw);
  MATFile << "%" << endl << endl;

  // Finally, put in some data.
  // Map dimension/UTM coodinate info
  MATFile << "rowRes = " << fixed << rowRes << endl;
  //fprintf(fptr, "%10.3f\n", rowRes);
  MATFile << "colRes = " << fixed << colRes << endl;
  //fprintf(fptr, "%10.3f\n", colRes);
  MATFile << "leftBottomUTM_Northing = " << fixed << leftBottomUTM_Northing << endl;
  //fprintf(fptr, "%10.3f\n", leftBottomUTM_Northing);
  MATFile << "leftBottomUTM_Easting = " << fixed << leftBottomUTM_Easting << endl;
  //fprintf(fptr, "%10.3f\n", leftBottomUTM_Easting);

  // Map data (matrices)
  MATFile << variable_name << ".value = [\n";
  for(int row = (leftBottomRow+numRows-1); row >= leftBottomRow; row--)
    {
      //cout << "saving  values for row = " << row << " of " << numRows <<  endl;
      for(int col = leftBottomCol; col < leftBottomCol+numCols; col++)
	{
	  MATFile << (getCellDataRC(row%numRows, col%numCols)).cellList.size() << "\t";
	}
      if (row > leftBottomRow) {
        MATFile << ";" << endl;
      }
    }
  MATFile << "];\n";

  MATFile << variable_name << ".color = [\n";
  for(int row = (leftBottomRow+numRows-1); row >= leftBottomRow; row--)
    {
      //cout << "saving color for row = " << row << " of " << numRows <<  endl;
      for(int col = leftBottomCol; col < leftBottomCol+numCols; col++)
        {
          MATFile << (getCellDataRC(row%numRows, col%numCols)).type << "\t";
        }
      if (row > leftBottomRow) {
        MATFile << ";" << endl;
      }
    }
  MATFile << "];\n";

  MATFile << variable_name << ".color = [\n";
  for(int row = (leftBottomRow+numRows-1); row >= leftBottomRow; row--)
    {
      //cout << "saving color for row = " << row << " of " << numRows <<  endl;
      for(int col = leftBottomCol; col < leftBottomCol+numCols; col++)
        {
          MATFile << (getCellDataRC(row%numRows, col%numCols)).type << "\t";
        }
      if (row > leftBottomRow) {
        MATFile << ";" << endl;
      }
    }
  MATFile << "];\n";

  MATFile << variable_name << ".list = {\n";
  for(int row = (leftBottomRow+numRows-1); row >= leftBottomRow; row--)
    {
      //cout << "saving list for row = " << row << " of " << numRows <<  endl;
      for(int col = leftBottomCol; col < leftBottomCol+numCols; col++)
        {
	  MATFile << "[ ";
	  current_item = getCellDataRC(row%numRows, col%numCols);
	  /*list_length = sizeof(current_item);
	  for(int list_i = 0; list_i < list_length; list_i++)
	    {
	      current_item = getCellDataRC(row%numRows, col%numCols);
	      //current_list_member = (current_item.cellList).front;
              //MATFile << current_list_member << "\t";
	    }*/
	  MATFile << " ]\t";
        }
      if(row > leftBottomRow) {
	MATFile << ";" << endl;
      }
    }
  MATFile << "};\n";

  // Done
  MATFile.close();
  //cout << "We have successfully saved the map.\n";
  return status;
}

/*
void genMap::CutCellLine(NorthEastDown end)
{
  NorthEastDown start;
  start.Northing = curState.Northing;
  start.Easting = curState.Easting;
  start.Downing = 0;
  end.Downing = 0;
  CutCellLine(start, end);
}

void genMap::CutCellLine(NorthEastDown start, NorthEastDown end)
{
  vector<RowCol> cells_on_line = GetCellLine(start, end);
  RowCol currentRC = cells_on_line.front();
  RowCol backRC = cells_on_line.back();
  while((currentRC.first != backRC.first)&&(currentRC.second != backRC.second))
    {
      if(theMap.find(currentRC) != theMap.end())
	{
	  if((getCellDataRC(currentRC.first, currentRC.second)).value < end.Downing)
	    {
	      theMap.erase(currentRC);
	    }
	}
      cells_on_line.pop_back();
      currentRC = cells_on_line.front();
    } 
}
 
vector<RowCol> genMap::GetCellLine(NorthEastDown start, NorthEastDown end)
{
  RowCol startRC = UTM2RowCol(start.Northing, start.Easting);
  RowCol endRC = UTM2RowCol(end.Northing, end.Easting);
  vector<RowCol> cells_on_line;
  double lowRes;
  double slope = 0;
  bool straight_east;
  if(endRC.first == startRC.first)
    {
      straight_east = true;
    }
  else
    {
      slope =(end.Easting - start.Easting)/(end.Northing - start.Northing);
      straight_east = false;
    }
  if(rowRes < colRes)
    lowRes = rowRes;
  else
    lowRes = colRes;
  NorthEastDown current_point = start;
  RowCol current_RC = startRC;
  while((current_RC.first != endRC.first)||(current_RC.second != endRC.second))
    {
      if(straight_east == true)
	{
	  if(start.Easting > end.Easting)
	    {
	      current_point.Easting -= colRes;
	    }
	  else
	    {
	      current_point.Easting += colRes;
	    }
	}
      else
	{
	  if(startRC.second == endRC.second)
	    {
	      if(start.Northing > end.Northing)
		{
		  current_point.Northing -= rowRes;
		} 
	      else
		{
		  current_point.Northing += colRes;
		}
	    }
	  else
	    {
	      if(fabs(slope) < 1)
		{
		  if(start.Northing > end.Northing)
		    {
		      current_point.Northing -= lowRes;
		      current_point.Easting -= slope*lowRes; 
		    }
		  else
		    {
		      current_point.Northing += lowRes;
		      current_point.Easting += slope*lowRes;
		    }
		}
	      else
		{
		  if(start.Easting > end.Easting)
		    {
		      current_point.Northing -= lowRes/slope;
		      current_point.Easting -= lowRes;
		    }
		  else
		    {
		      current_point.Northing += lowRes/slope;
		      current_point.Easting += lowRes;
		    }
		}
	    }
	}
      current_RC = UTM2RowCol(current_point.Northing, current_point.Easting);
      cells_on_line.push_back(current_RC);
    }
  return cells_on_line;
}
*/

/* *METHOD getCellsLine
 *
 * +DESCRIPTION: given a point in NorthEastDown, returns all the cells on the line
 *               from the sensor to there
 *
 * +PARAMETERS
 *
 *  - point : point to make the line through
 *
 * +RETURN: vector<RowCol>
 */
vector<RowCol> genMap::getCellsLine(NorthEastDown point){
  vector<RowCol> ret;
  NorthEastDown start, end, delta;
  RowCol curPos, startPos;
  double distance, yaw, step, dist;
  int maxNumCells;
  double minNorthing, maxNorthing;
  double minEasting, maxEasting;

  // Getting the step
  step = rowRes;
  if(step > colRes)
    step = colRes;
  step /= 2;

  // Getting the distance between the car and the point
  start.Northing = curState.Northing + GPS_2_FRONT_DISTANCE*cos(curState.Yaw);
  start.Easting = curState.Easting + GPS_2_FRONT_DISTANCE*sin(curState.Yaw);
  start.Downing = 0;
  end.Northing = point.Northing;
  end.Easting = point.Easting;
  end.Downing = 0;
  delta.Northing = end.Northing - start.Northing;
  delta.Easting = end.Easting - start.Easting;
  // Getting the yaw
  delta.norm();
  yaw = atan2(delta.Easting,delta.Northing);
  cout << "yaw = " << yaw << endl;

  // Determining weather the UTM is inside the map or not
  // If outside the map analyse the point further away but inside the map
  minNorthing = leftBottomUTM_Northing;
  maxNorthing = minNorthing + numRows*rowRes;
  minEasting = leftBottomUTM_Easting;
  maxEasting = minEasting + numCols*colRes;
  if((end.Northing <= minNorthing) || (end.Northing >= maxNorthing)){
    if(abs(yaw) > M_PI/2){//traversing up on the map
      end.Northing = minNorthing;
    }
    else{//traversing down on the map
      end.Northing = maxNorthing;
    }
    end.Easting = (end.Northing-start.Northing)*tan(yaw) + start.Easting;
  }
  else if((end.Easting <= minEasting) || (end.Easting >= maxEasting)){
    if(yaw > 0){//traversing east on the map
      end.Easting = maxEasting;
    }
    else{//traversing west on the map
      end.Easting = minEasting;
    }
    end.Northing = (end.Easting-start.Easting)/tan(yaw) + start.Easting;
  }
  end.display();
  // Evaluating again with the new boundaries
  delta.Northing = end.Northing - start.Northing;
  delta.Easting = end.Easting - start.Easting;
//  distance = sqrt(delta.Northing*delta.Northing + delta.Easting*delta.Easting)-lowerRes;

  // How many cells to update
  maxNumCells = (int)floor(distance/step) + 1;
  cout << "maxNumCells="  << maxNumCells << endl;
  cout << "distance="  << distance << endl;
  ret.reserve(maxNumCells);

  // Building the vector
  dist = 0;
  startPos = UTM2RowCol(start.Northing,start.Easting);
  curPos = startPos;
  while(dist < distance){
    curPos = FrameRef2RowCol(startPos,yaw,dist,0);
    cout << "(Row,Col) = (" << curPos.first << "," << curPos.second << ")" << endl;
    theMap.find(curPos)->second.color = GREEN_INDEX;
    theMap.find(curPos)->second.value = 1;
    ret.push_back(curPos);
    dist += step;
  }
  return(ret);
}

// velocity is determine with the equation 
//   vel = (MIN_SPEED - 1.0) + e^(m * dist)
//   we want to limit the velocity such that at a distance of
//   DIST_TO_OBSTACLE_MAX_SPEED the velocity is MAX_SPEED_OBSTACLE_DETECTED
//   This functions returns the value "m" to make that possible.
// Sort of seems like this should be a constant, but oh well....
double genMap::getVelocityCurveSlope() {
  return log(MAX_SPEED_OBSTACLE_DETECTED - MIN_SPEED + 1) / DIST_TO_OBSTACLE_MAX_SPEED;
}

// This returns the velocity vote for a particular arc.
// It takes the following parameters:
//   dist              Distance to nearest obstacle along that arc
//   obstacleDetected  Whether or not an obstacle was detected on ANY arc
//   minDist           Of ALL the steering arcs, the minimum distance to any
//                     obstacle

// The following rules apply:
//    Commanded speed will never be less than MIN_SPEED (unless set to 0)
//    Commanded speed will never be greater than MAX_SPEED
//    Commanded speed will be 0.0 if there is an obstacle closer than
//      DIST_TO_OBSTACLE_HALT on ANY steering arc
//    Commanded speed will be MAX_SPEED_OBSTACLE_DETECTED if ANY arc had an
//      obstacle less than DIST_TO_OBSTACLE_MAX_SPEED away, AND the distance
//      to an obstacle on this arc is greater than or equal to 
//      DIST_TO_OBSTACLE_MAX_SPEED

//      FIXME: TODO: If steering gets fixed, we can change the above rule

//    Commanded speed will be between 0.0 and MAX_SPEED_OBSTACLE_DETECTED if
//      the distance to an obstacle on this arc is less than 
//      DIST_TO_OBSTACLE_MAX_SPEED.  The speed will be 
//        speed=(MIN_SPEED - 1.0) + e^(m * dist) 
//      where m is the value returned by getVelocityCurveSlope()

double genMap::getVelocityVote(double dist,bool obstacleDetected, double minDist)
{
  if(dist < DIST_TO_OBSTACLE_HALT) return 0.0;
  else if(minDist < DIST_TO_OBSTACLE_HALT) return MIN_SPEED;
  else if(dist < DIST_TO_OBSTACLE_MAX_SPEED) {
    return (MIN_SPEED - 1.0) + exp(getVelocityCurveSlope() * dist);
  }
  else if(minDist < DIST_TO_OBSTACLE_MAX_SPEED) {
    return MAX_SPEED_OBSTACLE_DETECTED;
  } else {
    return MAX_SPEED;
  } 
}
