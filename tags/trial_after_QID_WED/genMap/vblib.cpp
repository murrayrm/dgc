// vblib.cpp

// include this header when compiling using a project with pre-compiled header
// #include "stdafx.h"

#include "vblib.h"
#include <string> // std::string

// size of char buffer used in some rare methods
#define BUF_SIZE 100

/////////////////////////////////////////////////////////////////
//
// VBMD5
//
/////////////////////////////////////////////////////////////////

inline unsigned int VBMD5::F1(unsigned int x, unsigned int y, unsigned int z)
{
	return (z ^ (x & (y ^ z))); //OR (x & y | ~x & z)
}

inline unsigned int VBMD5::F2(unsigned int x, unsigned int y, unsigned int z)
{
	return F1(z, x, y);
}

inline unsigned int VBMD5::F3(unsigned int x, unsigned int y, unsigned int z)
{
	return x ^ y ^ z;
}

inline unsigned int VBMD5::F4(unsigned int x, unsigned int y, unsigned int z)
{
	return (y ^ (x | ~z));
}

//This is the central step in the MD5 algorithm.
inline void VBMD5::MD5STEP(unsigned int (*f)(unsigned int x, unsigned int y, unsigned int z),
	unsigned int& w, unsigned int x, unsigned int y, unsigned int z, unsigned int data, unsigned int s)
{
	w += f(x, y, z) + data;
	w = w << s | w >> (32-s);
	w += x;
}

VBMD5::VBMD5()
{
	m_auiBuf[0] = 0x67452301;
	m_auiBuf[1] = 0xefcdab89;
	m_auiBuf[2] = 0x98badcfe;
	m_auiBuf[3] = 0x10325476;
	m_auiBits[0] = 0;
	m_auiBits[1] = 0;

	//Reset the flag
	m_bAddData = false;
}


//Update context to reflect the concatenation of another buffer of bytes.
void VBMD5::AddData(char const* pcData, int iDataLength)
{
/*	if(iDataLength < 0)
		throw runtime_error(VBString("FileDigest ERROR: in VBMD5::AddData(), Data Length should be >= 0!")); */
	unsigned int uiT;
	//Update bitcount
	uiT = m_auiBits[0];
	if((m_auiBits[0] = uiT + ((unsigned int)iDataLength << 3)) < uiT)
		m_auiBits[1]++; //Carry from low to high
	m_auiBits[1] += iDataLength >> 29;
	uiT = (uiT >> 3) & (BLOCKSIZE-1); //Bytes already
	//Handle any leading odd-sized chunks
	if(uiT != 0)
	{
		unsigned char *puc = (unsigned char *)m_aucIn + uiT;
		uiT = BLOCKSIZE - uiT;
		if(iDataLength < uiT)
		{
			memcpy(puc, pcData, iDataLength);
			return;
		}
		memcpy(puc, pcData, uiT);
		Transform();
		pcData += uiT;
		iDataLength -= uiT;
	}
	//Process data in 64-byte chunks
	while(iDataLength >= BLOCKSIZE)
	{
		memcpy(m_aucIn, pcData, BLOCKSIZE);
		Transform();
		pcData += BLOCKSIZE;
		iDataLength -= BLOCKSIZE;
	}
	//Handle any remaining bytes of data
	memcpy(m_aucIn, pcData, iDataLength);
	//Set the flag
	m_bAddData = true;
}

//Final wrapup - pad to 64-byte boundary with the bit pattern 
//1 0*(64-bit count of bits processed, MSB-first)
void VBMD5::FinalDigest(char* pcDigest)
{
	//Is the User's responsability to ensure that pcDigest has
	//at least 16 bytes allocated
/*	if(false == m_bAddData)
		throw runtime_error(VBString("FileDigest ERROR: in VBMD5::FinalDigest(), No data Added before call!")); */
	unsigned int uiCount;
	unsigned char* puc;
	//Compute number of bytes mod 64
	uiCount = (m_auiBits[0] >> 3) & (BLOCKSIZE-1);
	//Set the first char of padding to 0x80. This is safe since there is
	//always at least one byte free
	puc = m_aucIn + uiCount;
	*puc++ = 0x80;
	//Bytes of padding needed to make 64 bytes
	uiCount = BLOCKSIZE - uiCount - 1;
	//Pad out to 56 mod 64
	if(uiCount < 8)
	{
		//Two lots of padding:  Pad the first block to 64 bytes
		memset(puc, 0, uiCount);
		Transform();
		//Now fill the next block with 56 bytes
		memset(m_aucIn, 0, BLOCKSIZE-8);
	}
	else
	{
		//Pad block to 56 bytes
		memset(puc, 0, uiCount - 8);
	}
	//Append length in bits and transform
	((unsigned int*)m_aucIn)[(BLOCKSIZE>>2)-2] = m_auiBits[0];
	((unsigned int*)m_aucIn)[(BLOCKSIZE>>2)-1] = m_auiBits[1];
	Transform();
	memcpy(pcDigest, m_auiBuf, MD128LENGTH<<2);
	//Reinitialize
	Reset();
}

//Reset current operation in order to prepare a new one
void VBMD5::Reset()
{
	//Reinitialize
	m_auiBuf[0] = 0x67452301;
	m_auiBuf[1] = 0xefcdab89;
	m_auiBuf[2] = 0x98badcfe;
	m_auiBuf[3] = 0x10325476;
	m_auiBits[0] = 0;
	m_auiBits[1] = 0;
	//Reset the flag
	m_bAddData = false;
}

//The core of the MD5 algorithm, this alters an existing MD5 hash to
//reflect the addition of 16 longwords of new data. MD5Update blocks
//the data and converts bytes into longwords for this routine.
void VBMD5::Transform()
{
	unsigned int* puiIn = (unsigned int*)m_aucIn;
	register unsigned int a, b, c, d;
	a = m_auiBuf[0];
	b = m_auiBuf[1];
	c = m_auiBuf[2];
	d = m_auiBuf[3];
	//
	MD5STEP(F1, a, b, c, d, puiIn[0] + 0xd76aa478, 7);
	MD5STEP(F1, d, a, b, c, puiIn[1] + 0xe8c7b756, 12);
	MD5STEP(F1, c, d, a, b, puiIn[2] + 0x242070db, 17);
	MD5STEP(F1, b, c, d, a, puiIn[3] + 0xc1bdceee, 22);
	MD5STEP(F1, a, b, c, d, puiIn[4] + 0xf57c0faf, 7);
	MD5STEP(F1, d, a, b, c, puiIn[5] + 0x4787c62a, 12);
	MD5STEP(F1, c, d, a, b, puiIn[6] + 0xa8304613, 17);
	MD5STEP(F1, b, c, d, a, puiIn[7] + 0xfd469501, 22);
	MD5STEP(F1, a, b, c, d, puiIn[8] + 0x698098d8, 7);
	MD5STEP(F1, d, a, b, c, puiIn[9] + 0x8b44f7af, 12);
	MD5STEP(F1, c, d, a, b, puiIn[10] + 0xffff5bb1, 17);
	MD5STEP(F1, b, c, d, a, puiIn[11] + 0x895cd7be, 22);
	MD5STEP(F1, a, b, c, d, puiIn[12] + 0x6b901122, 7);
	MD5STEP(F1, d, a, b, c, puiIn[13] + 0xfd987193, 12);
	MD5STEP(F1, c, d, a, b, puiIn[14] + 0xa679438e, 17);
	MD5STEP(F1, b, c, d, a, puiIn[15] + 0x49b40821, 22);
	//
	MD5STEP(F2, a, b, c, d, puiIn[1] + 0xf61e2562, 5);
	MD5STEP(F2, d, a, b, c, puiIn[6] + 0xc040b340, 9);
	MD5STEP(F2, c, d, a, b, puiIn[11] + 0x265e5a51, 14);
	MD5STEP(F2, b, c, d, a, puiIn[0] + 0xe9b6c7aa, 20);
	MD5STEP(F2, a, b, c, d, puiIn[5] + 0xd62f105d, 5);
	MD5STEP(F2, d, a, b, c, puiIn[10] + 0x02441453, 9);
	MD5STEP(F2, c, d, a, b, puiIn[15] + 0xd8a1e681, 14);
	MD5STEP(F2, b, c, d, a, puiIn[4] + 0xe7d3fbc8, 20);
	MD5STEP(F2, a, b, c, d, puiIn[9] + 0x21e1cde6, 5);
	MD5STEP(F2, d, a, b, c, puiIn[14] + 0xc33707d6, 9);
	MD5STEP(F2, c, d, a, b, puiIn[3] + 0xf4d50d87, 14);
	MD5STEP(F2, b, c, d, a, puiIn[8] + 0x455a14ed, 20);
	MD5STEP(F2, a, b, c, d, puiIn[13] + 0xa9e3e905, 5);
	MD5STEP(F2, d, a, b, c, puiIn[2] + 0xfcefa3f8, 9);
	MD5STEP(F2, c, d, a, b, puiIn[7] + 0x676f02d9, 14);
	MD5STEP(F2, b, c, d, a, puiIn[12] + 0x8d2a4c8a, 20);
	//
	MD5STEP(F3, a, b, c, d, puiIn[5] + 0xfffa3942, 4);
	MD5STEP(F3, d, a, b, c, puiIn[8] + 0x8771f681, 11);
	MD5STEP(F3, c, d, a, b, puiIn[11] + 0x6d9d6122, 16);
	MD5STEP(F3, b, c, d, a, puiIn[14] + 0xfde5380c, 23);
	MD5STEP(F3, a, b, c, d, puiIn[1] + 0xa4beea44, 4);
	MD5STEP(F3, d, a, b, c, puiIn[4] + 0x4bdecfa9, 11);
	MD5STEP(F3, c, d, a, b, puiIn[7] + 0xf6bb4b60, 16);
	MD5STEP(F3, b, c, d, a, puiIn[10] + 0xbebfbc70, 23);
	MD5STEP(F3, a, b, c, d, puiIn[13] + 0x289b7ec6, 4);
	MD5STEP(F3, d, a, b, c, puiIn[0] + 0xeaa127fa, 11);
	MD5STEP(F3, c, d, a, b, puiIn[3] + 0xd4ef3085, 16);
	MD5STEP(F3, b, c, d, a, puiIn[6] + 0x04881d05, 23);
	MD5STEP(F3, a, b, c, d, puiIn[9] + 0xd9d4d039, 4);
	MD5STEP(F3, d, a, b, c, puiIn[12] + 0xe6db99e5, 11);
	MD5STEP(F3, c, d, a, b, puiIn[15] + 0x1fa27cf8, 16);
	MD5STEP(F3, b, c, d, a, puiIn[2] + 0xc4ac5665, 23);
	//
	MD5STEP(F4, a, b, c, d, puiIn[0] + 0xf4292244, 6);
	MD5STEP(F4, d, a, b, c, puiIn[7] + 0x432aff97, 10);
	MD5STEP(F4, c, d, a, b, puiIn[14] + 0xab9423a7, 15);
	MD5STEP(F4, b, c, d, a, puiIn[5] + 0xfc93a039, 21);
	MD5STEP(F4, a, b, c, d, puiIn[12] + 0x655b59c3, 6);
	MD5STEP(F4, d, a, b, c, puiIn[3] + 0x8f0ccc92, 10);
	MD5STEP(F4, c, d, a, b, puiIn[10] + 0xffeff47d, 15);
	MD5STEP(F4, b, c, d, a, puiIn[1] + 0x85845dd1, 21);
	MD5STEP(F4, a, b, c, d, puiIn[8] + 0x6fa87e4f, 6);
	MD5STEP(F4, d, a, b, c, puiIn[15] + 0xfe2ce6e0, 10);
	MD5STEP(F4, c, d, a, b, puiIn[6] + 0xa3014314, 15);
	MD5STEP(F4, b, c, d, a, puiIn[13] + 0x4e0811a1, 21);
	MD5STEP(F4, a, b, c, d, puiIn[4] + 0xf7537e82, 6);
	MD5STEP(F4, d, a, b, c, puiIn[11] + 0xbd3af235, 10);
	MD5STEP(F4, c, d, a, b, puiIn[2] + 0x2ad7d2bb, 15);
	MD5STEP(F4, b, c, d, a, puiIn[9] + 0xeb86d391, 21);
	//
	m_auiBuf[0] += a;
	m_auiBuf[1] += b;
	m_auiBuf[2] += c;
	m_auiBuf[3] += d;
}

void VBMD5::Char2Hex(unsigned char ch, char* szHex)
{
	static unsigned char saucHex[] = "0123456789ABCDEF";
	szHex[0] = saucHex[ch >> 4];
	szHex[1] = saucHex[ch&0xF];
	szHex[2] = 0;
}

void VBMD5::Binary2Hex(unsigned char const* pucBinStr, int iBinSize, char* pszHexStr)
{
	int i;
	char szHex[3];
	unsigned char const* pucBinStr1 = pucBinStr;
	*pszHexStr = 0;
	for(i=0; i<iBinSize; i++,pucBinStr1++)
	{
		Char2Hex(*pucBinStr1, szHex);
		strcat(pszHexStr, szHex);
	}
}

VBString VBMD5::MD5_hash(const VBString source) 
{
	VBString ret;
	char acDigest[65] = {0};
	char acHex[129] = {0};

	AddData(source.c_str(), source.length());
	FinalDigest(acDigest);
	Binary2Hex((unsigned char*)acDigest, 16, acHex);

	return VBString(acHex);
}

VBString VBMD5::MD5_file(const VBString fileName)
{
	typedef unsigned char TYPE;
	
	TYPE buffer;

	ifstream readFile;   

	// check if file exist
	readFile.open(fileName);  
	if (!readFile) return VBString ("file does not exist");
	readFile.close();
	
	readFile.open(fileName, ios::binary);   
	if (!readFile)     
	{	
		return VBString ("could not open file"); 
	}

	char s[2];
	s[1] = 0; // terminate string
	// loop to read entire file, byte by byte
	while (true) 
	{     
		readFile.read((char*)&buffer, sizeof(TYPE));     
		if (readFile.eof()) break;     
		s[0] = buffer; 
		AddData((const char *)s,1);
	} 

	VBString ret;
	char acDigest[65] = {0};
	char acHex[129] = {0};

	FinalDigest(acDigest);
	Binary2Hex((unsigned char*)acDigest, 16, acHex);

	return VBString(acHex);
}

/////////////////////////////////////////////////////////////////
//
// VBString
//
/////////////////////////////////////////////////////////////////

// static variable initialization
const char *VBString::m_chgHiCaps     = "�����������������������Ǫ�";
const char *VBString::m_chgLoCaps     = "�����������������������窺";
const char *VBString::m_chgHiCapsSafe = "AEIOUAEIOUAEIOUAEIOUAONCAO";
const char *VBString::m_chgLoCapsSafe = "aeiouaeiouaeiouaeiouaoncao";


//////////////////////////////////////////////////////
// constructors

void VBString::privateConstructor() 
{
	initCin(); // stores a pointer to cin in a member
	m_str = NULL; // indispensable
	m_decs = 2; // number of decimals when adding float or double
#ifdef VBSTRINGDEBUG
	_stringObjectTag = _VBObject.tag++; // increment _VBObject tag
	_VBObject = { 0, 0 };
#endif // VBSTRINGDEBUG
}

// this constructor is private
VBString::VBString(const int len) 
{
	privateConstructor();
	allocStr(len);
}

// default constructor
VBString::VBString(const char* s)
{
	privateConstructor();
	if (!s) s = "";
	int len = strlen(s);
	allocStr(len);
	strcpy(m_str,s);
}

// copy constructor
VBString::VBString(const VBString & s) 
{
	privateConstructor();
	allocStr(s.m_size);
	if (s.m_size) strcpy(m_str,s.m_str);
}

// hash (non reversible encrypt) this using MD5 algorithm
VBString VBString::get_MD5_hash() const
{
	VBMD5 a;
	return a.MD5_hash(*this);
}

// hash (non reversible encrypt) this using MD5 algorithm
void VBString::MD5_hash()
{
	*this = get_MD5_hash();
}

VBString VBString::strAfterPosition(const size_vbs pos) const 
{ 
	return strAfter(pos); 
};  

VBString VBString::strUntilPosition(const size_vbs pos) const 
{ 
	return strBefore(pos+1); 
}; 

VBString VBString::strInside(size_vbs from, size_vbs to) const 
{
	return this->strAfterPosition(from).strUntilPosition(to-from); 
}

bool VBString::validEmail() const
{
	const char *validChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!#$&*-_";
	const char *validSymbols = ".@";
	const char *at = "@";
	if (!validHelper(validChars,validSymbols,3)) return false;
	// there must be somethig after "@"
	VBString aux = afterFind(at);
	if (aux.length() == 0) return false;
	// no @ after the first @
	if (aux.existSubstring(at)) return false;
	// there must be somethig before "@"
	aux = beforeFind(at);
	if (aux.length() == 0) return false;
	return true;
}

bool VBString::validAddress() const
{
	const char *validChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"������������������������������������������������";
	const char *validSymbols = ".,- ��/: ";
	if (!validHelper(validChars,validSymbols,2)) return false;
	return true;
}

bool VBString::validCity() const
{
	const char *validChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"������������������������������������������������";
	const char *validSymbols = "-";
	if (!validHelper(validChars,validSymbols,2)) return false;
	return true;
}

// for a given 9 digits, return the 2 digits to complete a CIC
VBString VBString::rightCIC() const
{
	VBString ret;
	VBString CICleft = strInside(0,8); // the left part of CIC of this
	unsigned digit1 = findDigit (10, 9, CICleft);
	CICleft += intToChar(digit1);
	unsigned digit2 = findDigit (11, 10, CICleft);
	ret = intToChar(digit1);
	ret += intToChar(digit2);
	return ret;
}

bool VBString::validCIC() const
{
	const char *validChars = "0123456789";
	const char *validSymbols = "-.";
	if (!validHelper(validChars,validSymbols,11,15)) return false;

	// eliminate symbols
	VBString ob;
	for (unsigned i=0 ; i < length() ; i++)
	{
		if (charIsValid((*this)[i],validChars))
			ob += (*this)[i];
	}
	// now ob has the cic without the symbols. 
	// ob has only validChars, and length must be 11
	if (ob.length() != 11) return false;

#if 0
	VBString CICleft = ob.strInside(0,8); // the left part of CIC
	VBString CICright;

	// CPF algorithm. Calculate the 2 digits at right and compare
	unsigned digit1 = findDigit (10, 9, ob);
	CICleft += intToChar(digit1);
	CICright = intToChar(digit1);
	unsigned digit2 = findDigit (11, 10, CICleft);
	CICright += intToChar(digit2);
#endif
	VBString CICright = ob.rightCIC();

	// if calculated CICright does not match with given CICright, that's false CIC
	if (ob.strInside(9,10) != CICright) 
		return false;

	return true;
}

unsigned VBString::findDigit (int i, int lenght, const char *buffer) const
{
	unsigned sum=0;
	unsigned ret=0;

	for (unsigned k=0; k < lenght; k++) {
		sum += charToInt(buffer[k])*i;
		i--;
	};

	ret = 11-(sum%11);
	if ((ret==10) || (ret==11))
		ret=0;

	return ret;
}

bool VBString::validName() const
{
	const char *validChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"������������������������������������������������";
	const char *validSymbols = "-_";
	if (!validHelper(validChars,validSymbols,2)) return false;
	return true;
}

bool VBString::validPhone() const
{
	const char *validChars = "0123456789";
	const char *validSymbols = "-.";
	if (!validHelper(validChars,validSymbols,5)) return false;
	return true;
}

bool VBString::validZip() const
{
	const char *validChars = "0123456789";
	const char *validSymbols = "-.";
	if (!validHelper(validChars,validSymbols,5,10)) return false;
	return true;
}

bool VBString::validCreditCardNumber() const
{
	const char *validChars = "0123456789";
	const char *validSymbols = "-.";
	if (!validHelper(validChars,validSymbols,16,19)) return false;

	// eliminate symbols
	unsigned i;
	VBString ob;
	for (i=0 ; i < length() ; i++)
	{
		if (charIsValid((*this)[i],validChars))
			ob += (*this)[i];
	}
	// now ob has the cic without the symbols. 
	// ob has only validChars, and length must be 11
	if (ob.length() != 16) return false;

	int sum=0;
	int oddvalue=0 , evenvalue=0;
	int odd=0 , even=0;

	for (i=0 ; i < ob.length() ; i++)
	{
		if (i%2==0) 
		{
			oddvalue = charToInt(ob[i]);
			if (2*oddvalue > 9)
				oddvalue = 2*oddvalue - 9;
			else oddvalue = 2*oddvalue;
				odd += oddvalue;
		}	
		else 
		{
			evenvalue = charToInt(ob[i]);
			even += evenvalue;
		}
	}
	sum = even + odd;
	if ( (sum > 150) || (sum%10 != 0) )
		return false;
	return true;

#if 0
	const char* cardValues = CARDSTRING;
	int i=0 ,lenght=0, sum=0;
	int oddvalue=0 , evenvalue=0;
	int odd=0 , even=0;

	lenght = strlen(buffer);

	// Tests the string size
	if (lenght !=16)
		return(0);
	else {
		while (buffer[i] != 0) {
			// Checks if there are forbidden characters in the CARD field
			if (!strchr(cardValues, buffer[i]))
				return (0);
					
			if (i%2==0) {
				oddvalue = Char_To_Int(buffer[i]);
				if (2*oddvalue > 9)
					oddvalue = 2*oddvalue - 9;
				else oddvalue = 2*oddvalue;
				odd += oddvalue;
			}	
			else {
				evenvalue = Char_To_Int(buffer[i]);
				even += evenvalue;
			};
			i++;
		};
		sum = even + odd;
		if ( (sum > 150) || (sum%10 != 0) )
			return(0);
		else
			return(1);
	};	
#endif
}

bool VBString::validHelper(const char *validChars, const char* validSymbols, 
						   unsigned minSize, unsigned maxSize) const
{
	// if user sets non-zero max or min, check size
	unsigned length = getLength();
	if (minSize > 0 && length < minSize) return false;
	if (maxSize > 0 && length > maxSize) return false;
	unsigned i;

	// chars of *this must be validChars or validSymbols
	for (i = 0 ; i < length ; i++)
	{
		if ( !(charIsValid((*this)[i],validChars)
			|| charIsValid((*this)[i],validSymbols) ) ) return false;
	}

	// if a char is a symbol, the next char should not be a symbol
	for (i = 0 ; i < length-1 ; i++)
	{
		if ( charIsValid((*this)[i],validSymbols) && 
			!charIsValid((*this)[i+1],validChars) ) return false;
	}

	return true;
}

bool VBString::charIsValid(char ch,const char *validChars) const
{
	unsigned i;
	for (i = 0 ; i < strlen(validChars) ; i++)
		if (ch == validChars[i]) return true;
	return false;
}




// true if zero length
bool VBString::IsEmpty() const
{
	return (getLength() == 0);
}

// clear contents to empty
void VBString::Empty()
{
	*this = "";
}

// convert this string to debug format
VBString VBString::debug(int format) const
{
	VBString ret = *this;
	VBString aux, aux2;
	size_vbs i;
	switch (format)
	{
	case 1:
		ret = "";
		for (i = 0; i < getLength() ; i++)
		{
			aux = m_str[i];
			aux.simpleEncrypt(0); // convert to hexa
			ret += aux;
			ret += ".";
		}
		break;
	case 2:
		ret = "";
		for (i = 0; i < getLength() ; i++)
		{
			char ch = m_str[i];
			bool charOK = ch >= ' ' && ch <= 'z'+4;
			if (charOK)
				aux = ch;
			else
				aux = ".";
			ret += aux;
			ret += "  ";
		}
		break;
	case 3:
		aux = ret;
		ret = aux.debug(1);
		ret += '\n';
		ret += aux.debug(2);
		break;
	default:
		{} // do nothing
	}
	return ret;
}

// getline for namespace version
void VBString::getLineNamespace(char endChar) 
{
	std::string str;
	std::istream *pIstream = (std::istream*)m_myIstream;
	std::getline(*pIstream,str,'\n');
	*this = str.c_str();
	wipeCtrmM();
}

// old comment
// getline is a long method, usually implemented in cpp, not in a *.h.
// but it is defined in the header to avoid ambiguity error when using namespace std
// new comment: now getLine is implemented in the *.cpp
void VBString::getLine(char endChar) 
{
	// the 2 lines below are necessary to avoid unwanted chars in the buffer
	// int gc = m_myIstream.gcount();
	// if (gc > 0) m_myIstream.ignore(gc);

	// a linked list of strList objects
	VBList<strList> myList;

	// a loop to keep adding strList objects to a list, 
	// until the input stream of char is finished.
	strList b;
	bool listEnd = false;
	int elements = 0;
	int lengthOfLastBuffer;
	while (!listEnd) {
		for (int i=0 ; i < VB_N ; i++) {
			b.buffer[i] = m_myIstream->get();
			if (m_myIstream->eof() || (b.buffer[i] == endChar)) {
				b.buffer[i] = 0;  // end string
				listEnd = true;
				lengthOfLastBuffer = i;
				break;  // end for loop
			}
		}
		myList.add(b);
		elements++;
		// cout << "DEBUG: Added element " << elements << endl;
	}

	// one buffer to store the complete input string
	char *completeBuffer = new char [elements * VB_N];
	if (!completeBuffer)
		error("Alloc error !");
	
	// scans the list and copies the contents of each element
	// to completeBuffer
	strList *p = myList.GetFirst();
	for (int i=0 ; i < elements ; i++) {
		for (int k=0 ; k < VB_N ; k++)
			completeBuffer[k + VB_N*i] = p->buffer[k];
		p = myList.GetNext();
	}

	// the contents is copied. Now terminate string with the zero at the end
	int terminateStringPosition = (elements-1)*VB_N + lengthOfLastBuffer;

	// eliminate ^M char, that happens when unix programs read text files from windows
	// char lastChar = completeBuffer[terminateStringPosition-1];
	// if (lastChar == 13 && terminateStringPosition > 0) terminateStringPosition--; // ^M in decimal is 13

	completeBuffer[terminateStringPosition] = 0; // terminate string

	(*this) = completeBuffer; // copies the buffer to the object
	delete [] completeBuffer;

	wipeCtrmM();

} // end of getLine


void VBString::wipeCtrmM()
{
	char lastChar = (*this)[getLength()-1];
	if (lastChar == 13) // 13 = ^M
		*this = strInside(0,getLength()-2);
}


// return true if the string contains the sub string
bool VBString::existSubstring(const char* subStr) const
{
	bool ret = (bool) (::strstr((*this).getStr(), subStr) != 0); 
	// ::strstr is the global strstr, 
	return ret;
}


// Example: z_aux = 15, return 'f'
// Example: z_aux = 3, return = '3'
unsigned char VBString::p_ToAscii(unsigned char z_aux) const
{
	unsigned char z = tolower(z_aux);
	if (z < 10)  z += '0';
	else 	     z += 'a' - 10;
	return z;
}

// Example: z_aux = 'F' or 'f',  return = 15
// Example: z_aux = '3', return = 3
unsigned char VBString::p_ToAsciiReverse(unsigned char z_aux) const
{
	unsigned char z = tolower(z_aux);
	if (z < ('9'+1) )  z -= '0';
	else               z -= 'a' - 10;
	return z;
}

// Example: ch = 'A' (input) ,  ch_hex = "41" (output)
void VBString::p_toHex(unsigned char ch, char *ch_hex) const
{
	unsigned char a;
	a = (ch & 0x0f);
	ch_hex[1] = p_ToAscii(a); // LSB
	a = (ch & 0xf0)/16;
	ch_hex[0] = p_ToAscii(a);; // MSB
}

// Example: ch_hex = "41",  return = 'A'
unsigned char VBString::p_toHexReverse(char *ch_hex) const
{
	unsigned char ret;
	int msb, lsb;
	msb = p_ToAsciiReverse(ch_hex[0]);
	lsb = p_ToAsciiReverse(ch_hex[1]);
	ret = lsb + msb * 16;
	return ret;
}


// encrypts object using code [0~255]
void VBString::simpleEncrypt(unsigned short code)
{
	// code is any number from 0 to 255
	VBString ret;
	size_vbs len = strlen(m_str);
	unsigned char ch;
	char ch_hex[3];
	ch_hex[2] = 0; // string terminator
	for (size_vbs i=0 ; i < len ; i++) {
		ch = m_str[i] ^ code;  // ^ is bitwise xor
		p_toHex(ch,ch_hex);
		ret += ch_hex; // adding string
	}
	*this = ret;
}

// de-encrypts object using code
void VBString::simpleEncryptReverse(unsigned short code)
{
	// code is any number from 0 to 255
	VBString ret;
	size_vbs len = strlen(m_str);
	char ch[2];  
	ch[1] = 0; // string terminator
	char ch_hex[2];
	for (size_vbs i=0 ; i < len/2 ; i++) {
		ch_hex[0] = m_str[2*i];
		ch_hex[1] = m_str[2*i+1];
		ch[0] = p_toHexReverse(ch_hex);
		ch[0] ^= code;  // ^ is bitwise xor
		ret += ch;
	}
	*this = ret;
}

// return the number of sub-strings
size_vbs VBString::countSubString(const char *subStr) const
{
	size_vbs ret=0;
	VBString a = afterFind(subStr);
	while (strlen(a)!=0) {
		// cout << "DEBUG: " << a << endl;
		ret++;
		// cout << "DEBUG: " << ret << endl;
		VBString b = a.afterFind(subStr);
		a = b;
	}
	return ret;
}

// return the number of tokens
size_vbs VBString::tokCount(char tok) const
{
	size_vbs ret=0;
	size_vbs len = getLength();
	for (size_vbs i=0; i < len ; i++) {
		if (m_str[i]==tok)
			ret++;
	}
	return ret;
}

void VBString::strtokchg(const char tok, const size_vbs n, const char *newData) 
{
	if (n > tokCount(tok)) {// if n too big
		// cout << "n too big" << endl; // debug
		return; // do nothing
	}
	size_vbs tokPos_initial = strtokpos(tok,n) + 1;
	size_vbs tokPos_final   = strtokpos(tok,n+1) + 1;
	if (n == tokCount(tok)) // the last string
		tokPos_final++;
	VBString before = strBefore(tokPos_initial);
	VBString after = strAfter(tokPos_final-1);
	*this = before + newData + after;
}

void VBString::myDelete() 
{
	if (m_str) {
		#ifdef VBSTRINGDEBUG
			cout << "Deleted object #: " << _stringObjectTag << endl;
		#endif // VBSTRINGDEBUG
		delete [] m_str;
		m_str = NULL;
	}
}

VBString::~VBString() 
{
	myDelete();
} 

char VBString::p_extendedToUpper(char in) 
{
	in = toupper(in);
	// change extended chars 
	int lenChg = strlen(m_chgHiCaps);
	for (int k=0 ; k < lenChg ; k++) {
		if (in == m_chgLoCaps[k]) { // test for each char of chgLoCaps
			return m_chgHiCaps[k];  // set correspondent HiCaps in its place
		}
	} // for k
	return in;
}

char VBString::p_extendedToLower(char in) 
{
	in = tolower(in);
	// change extended chars 
	int lenChg = strlen(m_chgHiCaps);
	for (int k=0 ; k < lenChg ; k++) {
		if (in == m_chgHiCaps[k]) { // test for each char of chgLoCaps
			return m_chgLoCaps[k];  // set correspondent LoCaps in its place
		}
	} // for k
	return in;
}

char VBString::p_safeToUpper(char in) 
{
	in = p_toSafe(in);
	in = toupper(in);
	return in;
}

char VBString::p_safeToLower(char in) 
{
	in = p_toSafe(in);
	in = tolower(in);
	return in;
}

// change extended chars to safe (non-extended) ones
char VBString::p_toSafe(char in) const
{
	int lenChg = strlen(m_chgHiCaps);
	for (int k=0 ; k < lenChg ; k++) {
		if (in == m_chgHiCaps[k]) { // test for each char of chgHiCaps
			return m_chgHiCapsSafe[k];  // set correspondent safe
		}
		if (in == m_chgLoCaps[k]) { // test for each char of chgLoCaps
			return m_chgLoCapsSafe[k];  // set correspondent safe
		}
	} // for k
	return in;
}

// ignore extended char
void VBString::toLower() 
{
	size_vbs len = strlen(m_str);
	for (size_vbs i=0 ; i < len ; i++)
		m_str[i] = tolower(m_str[i]);
}

 // ignore extended char
void VBString::toUpper() 
{
	size_vbs len = strlen(m_str);
	for (size_vbs i=0 ; i < len ; i++)
		m_str[i] = toupper(m_str[i]);
}

// convert to extended char lo caps
void VBString::extendedToLower() 
{
	size_vbs len = strlen(m_str);
	for (size_vbs i=0 ; i < len ; i++)
		m_str[i] = p_extendedToLower(m_str[i]);
}

// convert to extended char hi caps
void VBString::extendedToUpper() 
{
	size_vbs len = strlen(m_str);
	for (size_vbs i=0 ; i < len ; i++)
		m_str[i] = p_extendedToUpper(m_str[i]);
}

// convert to standard ascii char lo caps
void VBString::safeToLower() 
{
	size_vbs len = strlen(m_str);
	for (size_vbs i=0 ; i < len ; i++)
		m_str[i] = p_safeToLower(m_str[i]);
}

// convert to standard ascii char hi caps
void VBString::safeToUpper() 
{
	size_vbs len = strlen(m_str);
	for (size_vbs i=0 ; i < len ; i++) {
		// cout << "before=" << m_str[i] << endl;
		m_str[i] = p_safeToUpper(m_str[i]);
		// cout << "after="<< m_str[i] << endl;
	}
}

// convert to standard ascii char hi or lo caps
void VBString::toSafe() 
{
	size_vbs len = strlen(m_str);
	for (size_vbs i=0 ; i < len ; i++) {
		m_str[i] = p_toSafe(m_str[i]);
	}
}


// void VBString::setErrorFunction(void (*fun)(const char *)) 
// {	m_errorFunction = fun; }

void VBString::error(const char *msg) const
{
	// m_errorFunction(msg);
	throw msg;
} 

void VBString::allocStr(size_vbs size_in) // redim
{ 
	// m_size = abs(size_in); // not necessary
	m_size = size_in;
	if (m_str) delete [] m_str;
	if (m_size >= 0) {
		m_str = new char [m_size+1];
		if (!m_str) 
			error("Alloc error !!!");
		#ifdef VBSTRINGDEBUG
			cout << "Alloced object #: " << _stringObjectTag << endl;
		#endif // VBSTRINGDEBUG
		m_str[0] = 0; // terminate string
	} 
	else {
		#ifdef VBSTRINGDEBUG
			cout << "Attempt to alloc empty string, object #: " << _stringObjectTag << endl;
		#endif // VBSTRINGDEBUG
		m_str = NULL;
	}
}

// global function
VBString sbVB::operator+(const char* str,const VBString & vbs) 
{
	VBString r(str);
	VBString ret = r + vbs;
	return ret;
}

//////////////////////
// BSTR

#ifdef WIN32
void VBString::getBSTR(const BSTR & pWord) 
{
	size_vbs i=0;
	while (pWord[i]) 
		i++;
	// after this loop, i has the length of the BSTR
	size_vbs length = i;
	char *p = new char [length+1];
	if (!p) error("Could't alloc memory");
	for (i=0 ; i < length ; i++)
		p[i] = (char) pWord[i];
	p[length]=0; // terminate string
	(*this) = p; // place the string to object
	delete [] p; // delete p not to leak memory
}


BSTR VBString::exportBSTR() 
{
	CComVariant VarStr(*this);
	BSTR ret=VarStr.bstrVal;
	return ret;
}
#endif


char VBString::getChar(size_vbs pos) const
{
	if (pos < getLength())
		return m_str[pos];
	else
		return 0; // null char
}

void VBString::putChar(size_vbs pos,char ch)
{
	if (pos < getLength())
		m_str[pos] = ch;
}



/////////////////////////////////////////////////////////////
// operator=

void VBString::operator=(const VBString & s) 
{
	allocStr(s.m_size);
	if (s.m_size) strcpy(m_str,s.m_str);
}

void VBString::operator=(const char *s) 
{
	if (!s) s = "";
	size_vbs len = strlen(s);
	allocStr(len);
	if (len) strcpy(m_str,s);
}

void VBString::operator= (const char ch) 
{
	VBString aux;
	aux += ch;
	*this = aux;
}

void VBString::operator=(const int i)
{
	VBString aux;
	aux += i;
	*this = aux;
}

void VBString::operator=(const unsigned i)
{
	VBString aux;
	aux += i;
	*this = aux;
}

void VBString::operator=(const float f)
{
	VBString aux;
	aux.setDecs(getDecs()); // use the number of decs of this
	aux += f;
	*this = aux;
}

void VBString::operator=(const double d)
{
	VBString aux;
	aux.setDecs(getDecs()); // use the number of decs of this
	aux += d;
	*this = aux;
}

void VBString::operator=(const long double d)
{
	VBString aux;
	aux.setDecs(getDecs()); // use the number of decs of this
	aux += d;
	*this = aux;
}


/////////////////////////////////////////////////////////////
// operator+=
void VBString::operator+=(const char ch) 
{
	VBString ret = (*this) + ch;
	operator=(ret);
}

void VBString::operator+=(const int i) 
{
	VBString ret = (*this) + i;
	operator=(ret);
}

void VBString::operator+=(const unsigned i) 
{
	VBString ret = (*this) + i;
	operator=(ret);
}

void VBString::operator+=(const double d) 
{
	VBString ret = (*this) + d;
	operator=(ret);
}

void VBString::operator+=(const long double d) 
{
	VBString ret = (*this) + d;
	operator=(ret);
}

void VBString::operator+=(const float f) 
{
	VBString ret = (*this) + f;
	operator=(ret);
}

void VBString::operator+=(const char* s) 
{
	VBString ret = (*this) + s;
	operator=(ret);
}

void VBString::operator+=(const VBString & s) 
{
	VBString ret = (*this) + s;
	operator=(ret);
}

/////////////////////////////////////////////////////////////
// operator+

VBString VBString::operator+(const char ch) const
{
	char buffer[2];
	buffer[0] = ch;
	buffer[1] = 0; // terminator string
	VBString ret = (*this) + buffer; // buffer is char*
	return ret;
}

VBString VBString::operator+(const int i) const
{
/* old and buggy version
	char buffer[BUF_SIZE];
	sprintf(buffer,"%d",i);
	return operator+(buffer);
*/
	char buffer[BUF_SIZE];
	sprintf(buffer,"%d",i);
	VBString ret = (*this) + buffer;
	return ret;
}

VBString VBString::operator+(const unsigned i) const
{
/* old and buggy version
	char buffer[BUF_SIZE];
	sprintf(buffer,"%d",i);
	return operator+(buffer);
*/
	char buffer[BUF_SIZE];
	sprintf(buffer,"%u",i);
	VBString ret = (*this) + buffer;
	return ret;
}

VBString VBString::operator+(const float f) const
{
	VBString ret = operator+((double)f);
	return ret;
}

VBString VBString::operator+(const double d) const
{
	char buffer[BUF_SIZE];
	char sprintfBuffer[BUF_SIZE];
	sprintf(sprintfBuffer,"%c1.%df",'%',m_decs); 
	// cout << "DEBUG=" << sprintfBuffer << endl;
	// cout << "DEBUG=" << d << endl;
	// create a buffer for next sprintf. 
	// If m_decs == 2, springBuffer is "%1.2f"
	sprintf(buffer,sprintfBuffer,d);
	VBString ret = (*this) + buffer;
	return ret;
}

VBString VBString::operator+(const long double d) const
{
	char buffer[BUF_SIZE];
	char sprintfBuffer[BUF_SIZE];
	sprintf(sprintfBuffer,"%c1.%dlf",'%',m_decs); 
	sprintf(buffer,sprintfBuffer,d);
	VBString ret = (*this) + buffer;
	return ret;
}

VBString VBString::operator+(const char* s) const
{
	if (s) {
		size_vbs len = strlen(s);
		if (!m_size) { // if string is null
			VBString ret(len);
			if (len) strcpy(ret.m_str,s);
			return ret;
			/*
			myDelete();
			allocStr(strlen(s));
			strcpy(str,s);
			return (*this);
			*/
		}
		VBString ret(m_size + len);
		if (m_size) strcpy(ret.m_str,m_str);
		if (len) strcat(ret.m_str,s);
		return ret;
	}
	else 
		return (VBString(0));  // return empty VBString
}


VBString VBString::operator+(const VBString & s) const
{
	if (s.m_str)
		return operator+(s.m_str);
	else
		return operator+("");
/*	
	VBString ret(size + s.size);
	strcpy(ret.str,str);
	strcat(ret.str,s.str);
	return ret;
*/
}

VBString VBString::afterFind(const char findChar, bool & found) const
{
	char buffer[2];
	buffer[0] = findChar;
	buffer[1] = 0; // terminateString
	return afterFind(buffer,found);
}

VBString VBString::afterFind(const char *find, bool & found) const
{
	found = true;
	VBString b = beforeFind(find);
	if (b.getLength() == getLength()) {
		found = false;
		return (VBString(""));
	}
	char *inStr = m_str;
	size_vbs pos = b.getLength() + strlen(find);
	VBString ret(inStr+pos);
	return ret;
}

VBString VBString::afterFind(const char findChar) const
{
	char buffer[2];
	buffer[0] = findChar;
	buffer[1] = 0; // terminateString
	return afterFind(buffer);
}

// return the string before fing
// VBString r,a("123456789");
// r = a.afterFind("56"); // r = "789";
// r = a.afterFind("xx"); // r = ""; // string not found
VBString VBString::afterFind(const char *find) const
{
	bool found;
	return (afterFind(find,found));
}

VBString VBString::beforeFind(const char findChar) const
{
	char buffer[2];
	buffer[0] = findChar;
	buffer[1] = 0; // terminateString
	return beforeFind(buffer);
}

// return the string before fing
// VBString r,a("123456789");
// r = a.beforeFind("56"); // r = "1234";
// r = a.beforeFind("xx"); // r = "123456789"; // string not found
VBString VBString::beforeFind(const char *find) const
{
	char *inStr = new char [getLength()+1];
	if (!inStr)
		error("Alloc error !!!");
	strcpy(inStr,getStr());
	char *r = strinc(find,inStr);
	size_vbs stopLen;
	if (r) 
		stopLen = strlen(inStr) - strlen(r);
	else
		stopLen = strlen(inStr);
	inStr[stopLen] = 0;  // end string
	VBString ret(inStr);
	delete [] inStr;
	return ret;
}

// find and replace 1 string 
void VBString::strchg(const char *find,const char *replace) 
{
	VBString ret = beforeFind(find);
	bool found;
	VBString a = afterFind(find,found);
	if (found) 
		ret += replace + a;
	(*this) = ret; // place effect to the own object
}

#if 0
// this is the old version
// find and replace all instances of find string 
void VBString::strschg(const char *find,const char *replace) {
	VBString save = (*this);
	VBString ret = save;
	ret.strchg(find,replace);
	bool stop = (ret == save);
	while (!stop) {
		save = ret;
		ret.strchg(find,replace);
		stop = (ret == save);
	}
	(*this) = ret; // place effect to the own object
}
#endif

// new version
// find and replace all instances of find string 
void VBString::strschg(const char *find,const char *replace) {
	VBString ret, after, before;
	before = beforeFind(find);
	after = (*this);
	ret = before;
	bool stop = (after == before);
	if (!stop) ret += replace;
	while (!stop) {
		after = after.afterFind(find);
		before = after.beforeFind(find);
		stop = (after == before);
		if (!stop) {
			ret += before;
			ret += replace;
		}
		else {
			ret += before;
		}
	}
	(*this) = ret; // place effect to the own object
}

VBString VBString::strtok(char tok, unsigned long n, bool & found ) const
{
	unsigned long tokPos;
	return strtok_helper(tok,n,found,tokPos);
}

// VBString = "abc,def,ghi"
// tok =','
// n = 2
// return = "def"
// tokpos is the position of the token in the string
VBString VBString::strtok_helper(char tok, size_vbs n, bool & found, 
			size_vbs & tokPos) const
{
	found = false;
	char *str = m_str;
	if (!str) return VBString(""); // token not found 
	size_vbs len = getLength();
	size_vbs tokCount = 0;
	size_vbs i=0, initial=0, final=0;
	bool initOK = false, finalOK = false;

#if 0
	// fix loop in case first letter is the tok
	if ((str[0] == tok) && (n==0)) {
		initial = 0;  // initial stores the position of nth token
		initOK = true;
		tokCount = 1;
		i = 1;
	}
#endif

	// loop to identify the position of nth string
	for ( ; (i < len) && !finalOK ; i++) 
	{
		if (str[i] == tok) 
			tokCount++;
//		if ((str[0] == tok) || (!initOK && (tokCount == n))) {
		if (!initOK && (tokCount == n)) {
			initial = i;  // initial stores the position of nth token
			initOK = true;
		}
		if (tokCount == (n+1)) { 
			final = i; // final stores the position of (n+1)th token
			finalOK = true;
		}
	}
	
	if (initOK && finalOK) 
	{
		found = true;
		char *buffer = new char [final - initial + 1];
		size_vbs offset;
		if (n == 0)	offset = 0;
		else offset = 1;
		for (i=initial+offset ; i < final ; i++)
			buffer[i-initial-offset] = str[i];
		buffer[i-initial-offset] = 0; // end string
		VBString ret(buffer);
		delete [] buffer;
		tokPos = initial+1; // +1 to get on the token
		if (n==0) tokPos=0;
		return ret;
	}
	if (initOK && !finalOK)  // n points to the last string
	{ 
		found = true;
		VBString ret(str+initial+1);
		tokPos = initial+1; // +1 to get on the token
		if (n==0) 
		{
			tokPos=0;
			ret = str;
		}
		return ret;
	}

	if ((str[0] == tok) && (n==0)) 
	{
		found = true; // in this case, return is "", but str is found
		tokPos = 0;
	}

	tokPos = getLength();
	return VBString(""); // token not found or n too big

} // end of method strtok_helper


VBString VBString::strtok(char tok, size_vbs n) const
{
	bool found;
	return (strtok(tok,n,found));
}

/* use getLength method instead of length
unsigned long VBString::length() {
	if (m_str)
		return strlen(m_str);
	else
		return 0;
}
*/

// *this "1234567890"
// pos = 3
// ret = "4567890"
VBString VBString::strAfter(size_vbs pos) const
{
	if (pos > 0) {
		VBString ret = m_str + pos;
		if (pos >= getLength())
			ret = "";
		return ret;
	}
	else 
		return *this;
}

// *this "1234567890"
// pos = 3
// ret = "123"
VBString VBString::strBefore(size_vbs pos) const
{
	size_vbs len = getLength();
	char *p = new char [len+1]; // +1 for the terminator char
	strcpy(p,m_str); // copy string to other buffer
	if (pos > len) pos = len;
	p[pos] = 0;  // terminate the string
	VBString ret = p;
	delete [] p;
	return ret;
}

// return the position of the token
// VBString a = "abc,def,ghi,123,456,7890,yyy";
// int n=3;
// char tok = ',';
// int pos = a.strtokpos(tok,n); // pos=
size_vbs VBString::strtokpos(char tok, size_vbs n) const
{
	size_vbs tokPos;
	bool found;
	strtok_helper(tok,n,found,tokPos);
	return tokPos - 1 ;
}

#if 0
size_vbs VBString::strtokLen(char tok) const
{
	size_vbs ret=0;
	bool found;
	do {
		strtok(tok,ret,found);
		if (found)
			ret++;
	} while (found);

	return ret;
}
#endif


bool VBString::operator<(const VBString & s) const
{
	return (strcmp(m_str,s.m_str) < 0);
}

bool VBString::operator<=(const VBString & s) const
{
	return (strcmp(m_str,s.m_str) <= 0);
}

bool VBString::operator>(const VBString & s) const
{
	return (strcmp(m_str,s.m_str) > 0);
}

bool VBString::operator>=(const VBString & s) const
{
	return (strcmp(m_str,s.m_str) >= 0);
}

bool VBString::operator==(const VBString & s) const
{
	return (strcmp(m_str,s.m_str)==0);
}

bool VBString::operator!=(const VBString & s) const
{
	return (!((*this)==s));
}

bool VBString::operator<(const char * s) const { return operator<(VBString(s)); }
bool VBString::operator<=(const char * s) const { return operator<=(VBString(s)); }
bool VBString::operator>(const char * s) const { return operator>(VBString(s)); }
bool VBString::operator>=(const char * s) const { return operator>=(VBString(s)); }
bool VBString::operator==(const char * s) const { return operator==(VBString(s)); }
bool VBString::operator!=(const char * s) const { return operator!=(VBString(s)); }



// convert object to web scape sequence
void VBString::escapeSequenceReverse() 
{

	// Convert all + chars to space chars
    strschg("+"," ");

	bool hasChange;
	VBString ret, before, after = (*this);
	do {
		hasChange = false;
		for (size_vbs x = 0; x < after.getLength(); x++) {
			if (after.m_str[x] == '%') {
				hasChange = true;
				before = after.strBefore(x);

				// convert the code after the '%' to ascii, and use it as replace string
				char strToReplace[2];
				strToReplace[0] = p_toHexReverse(after.m_str+x+1);
				strToReplace[1] = 0; // terminate string

				ret += before + strToReplace;
				after = after.strAfter(x+3);
				break;
			}
		}
		if (!hasChange) 
			ret += after;
	}
	while (hasChange);

	// copy 
	(*this) = ret;
}

// convert object to web unscape sequence
void VBString::escapeSequence() 
{

	// Convert % separatelly not to give problem with the general % case
    strschg("%","%25");

	const char *charToConvert = "=.,?<>[]{};:'\"`~!@&#$^()+-*/_|\\����������������������������������������";

	// Convert all %xy hex codes into ASCII chars 
	bool hasChange;
	do {
		hasChange = false;
		for (size_vbs x = 0; x < m_size; x++) {
			for (size_vbs y = 0; y < strlen(charToConvert) ; y++) {
				if (m_str[x] == charToConvert[y]) {
					hasChange = true;
					char strToSearch[2];
					strToSearch[0] = charToConvert[y]; 
					strToSearch[1] = 0; // terminate string

					// replace string is %xx<zero>, where xx is charToConvert[y] converted to hex
					char strToReplace[4];
					strToReplace[0] = '%';
					p_toHex(charToConvert[y],strToReplace+1);
					strToReplace[3] = 0; // terminate string
					strschg(strToSearch,strToReplace); // convert
					break;
				} // if
			} // for y
		} // for x
	} while (hasChange);
    
	strschg(" ","+"); // convert space char separatelly, since version 2.3 of VBMcgi
}

// this = str
// useful for expat XML processing
void VBString::setUnterminatedString(const char *str, size_vbs length) 
{
	myDelete();
	m_str = new char [length + 1];
	for (size_vbs i = 0; i < length ; i++)
		m_str[i] = str[i];
	m_str[length] = 0;
	m_size = length;
}

void VBString::wipeLeadingSpace() 
{
	size_vbs i = 0;
	while ((m_str[i] == '\n' || m_str[i] == '\t' || m_str[i] == ' ') && i < m_size)
		i++;
	if (i != 0)
	{
		// *this = m_str+i; // can't do this
		VBString s = m_str+i;
		*this = s;
	}

}

#ifdef VBLIB_USE_NAMESPACE_STD
// get next word of a stream
void VBString::getNextWord(istream & stream)
{
	std::string s;
	stream >> s; // read using std string
	(*this) = s.c_str(); // copy to this

	// remove symbols if exist at right
	int len = length(); // length of this
	const char *symbols = ".,;";
	int symbol_len = strlen(symbols);
	if (len > 0)
	{
		for (int i = 0; i < symbol_len  ; i++)
		{
			if (m_str[len-1] == symbols[i]) // if last char of this is one of the symbols
				(*this) = (*this).beforeFind(symbols[i]); // remove it
		}
	}
}
#endif


/////////////////////////////////////////////////////////////////
//
// Some string global functions (comes from CXL)
//
/////////////////////////////////////////////////////////////////

char *sbVB::strins(const char *instr,char *str,size_vbs st_pos)
{
    register size_vbs i,leninstr;

    /* get length of string to insert */
    leninstr=strlen(instr);

    /* shift affected portion of str text to the right */
    for(i=strlen(str);i>=st_pos;i--) *(str+leninstr+i)=*(str+i);

    /* insert instr text */
    for(i=0;i<leninstr;i++) *(str+st_pos+i)=*(instr+i);

    /* return address of modified string */
    return(str);
}

char *sbVB::strdel(const char *substr,char *str)
{
    char *src,*dest;

    dest=strinc(substr,str);
    if(!dest) return(NULL);
    src=dest+strlen(substr);
    strcpy(dest,src);
    return(str);
}


char *sbVB::strsrep(char *str,const char *search,const char *replace)
{
    char *p;

    if((p=strinc(search,str))!=NULL) {
		sbVB::strdel(search,str);
        sbVB::strins(replace,str,(int)((unsigned long)p-(unsigned long)str));
        p=str;
    }

    return(p);
}


char *sbVB::strinc(const char *str1,char *str2)
{
    register size_vbs max;
    char *p;

    max=strlen(str1);

    for(p=str2;*p;p++) {
        if(!strncmp(str1,p,max)) return(p);
    }
    return(NULL);                       /* string1 not found in string2 */
}


char *sbVB::strschg(char *str,const char *find,const char *replace)
{
    register size_vbs count=0;
    register char *p;

    p=str;
    while((p=sbVB::strinc(find,p))!=NULL) {
        sbVB::strsrep(p,find,replace);
/***************** line included by Sergio Villas Boas **************/
        p += (int) strlen(replace); 
/*------------------------------------------------------------------*/
        count++;
    }

	if (count) return (str);
    else return (NULL);
//    return(count?str:NULL);
}

/////////////////////////////////////////////////////////////////
//
// class VBDate
//
/////////////////////////////////////////////////////////////////

// static member initialization
//                                          jan fev mar apr may jun jul ago sep oct nov dec   
const int VBDate::VBDaysInMonths[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };


// set base date if given date is valid. Do nothing otherwise
// ATTENTION: does not change m_baseWeekDay !
bool VBDate::p_setDate(int year, int month, int day) 
{
	bool OK = validDate(year,month,day);
	if (!OK) return false;
	m_baseYear = year;
	m_baseMonth = (VBMonths) month;
	m_baseDay = day;
	return true;
}

// default constructor
VBDate::VBDate() 
{
	m_baseYear = 2001;
	m_baseMonth = VBapr;
	m_baseDay = 21;
	m_baseWeekDay = VBsat;
}


bool VBDate::leapYear (int year) const
{
	int k = year % 4;  // every 4 years, force leap year true
	bool ret = (k == 0);
	k = year % 100; // over previous, every 100 years, force leap year false
	ret = ret && !(k == 0);
	k = year % 400; // over previous, every 400 years, force leap year true
	ret = ret || (k == 0);
	return ret;
}

int VBDate::daysInAYear(int year) const
{
	int ret = 365;
	if (leapYear(year)) 
		ret++;
	return ret;
}

int VBDate::daysInAMonth(int year, int month) const
{
	// if (!validDate(year,month,1)) return -1;
	// can't test valid date, because validDate calls daysInAMonth
	int ret;
	ret = VBDaysInMonths[month];
	if (leapYear(year) && (month==VBfeb)) 
		ret++;
	return ret;
}

bool VBDate::validDate(int year, int month, int day) const
{
	if (month < VBjan || month > VBdec ) return false;
	if (day <= 0 || day > daysInAMonth(year,month)) return false;
	return true;
}

bool VBDate::isFuture(int year, int month, int day) const
{
	if (year > m_baseYear) return true;
	if (year < m_baseYear) return false;
	if (month > m_baseMonth) return true;
	if (month < m_baseMonth) return false;
	if (day > m_baseDay) return true;
	return false;
}

int VBDate::remainingDaysOfGivenMonth(int year, int month, int day) const
{
	if (!validDate(year,month,day)) return -1;
	return daysInAMonth(year,month) - day;
}

int VBDate::remainingDaysOfGivenYearFromNextMonthOn(int year, int month) const
{
	if (!validDate(year,month,1)) return -1;
	int ret=0;
	for (int m = month+1 ; m < 12 ; m++)
		ret += daysInAMonth(year,m);
	return ret;
}

int VBDate::remainingDaysofGivenYear(int year, int month, int day) const
{
	int ret;
	ret = remainingDaysOfGivenMonth(year,month,day);
	ret += remainingDaysOfGivenYearFromNextMonthOn(year,month);
	return ret;
}

long VBDate::numberOfDaysOfFullYearsFromNextYearToYearBeforeGivenYear(int year) const
{
	long ret=0;
	for (int y = m_baseYear+1 ; y < year ; y++)
		ret += daysInAYear(y);
	return ret;
}

int VBDate::numberOfDaysOfGivenYearUntilGivenDate(int year, int month, int day) const
{
	if (!validDate(year,month,day)) return -1;
	int ret=0;
	for (int m = 0 ; m < month ; m++)
		ret += daysInAMonth(year,m);
	// after this loop, all days until previous month are already added
	ret += day;
	return ret;
}

bool VBDate::setDate(int year, int month, int day) 
{
	bool OK = validDate(year,month,day);
	if (!OK) return false;
	m_baseWeekDay = (VBWeekdays) getWeekDay(year,month,day);
	p_setDate(year,month,day);
	return true;
}

bool VBDate::setDate(VBDateTime dateTime)
{
	return setDate(dateTime.m_tm.tm_year+1900,dateTime.m_tm.tm_mon,dateTime.m_tm.tm_mday);
}

bool VBDate::setDateYYYYMMDD(VBString date)
{
	int year, month, day;
	year = atoi(date.strInside(0,3));
	month = atoi(date.strInside(4,5));
	day = atoi(date.strInside(6,7));
	return setDate(year, month-1, day);
}


bool VBDate::deltaDays(int year, int month, int day, long & delta) const
{
	bool OK = validDate(year,month,day);
	if (!OK) return false;

	delta = 0;
	if (year == m_baseYear && month == m_baseMonth && day == m_baseDay) 
	{
		// given day is the same as base day
		return true;
	}

	if (isFuture(year,month,day)) 
	{
		// cout << "DEBUG given date is future compared to base date" << endl;
		if (year > m_baseYear) 
		{
			// cout << "DEBUG given year is not the same as base year" << endl;
			delta += remainingDaysofGivenYear(m_baseYear,m_baseMonth,m_baseDay);
			delta += numberOfDaysOfFullYearsFromNextYearToYearBeforeGivenYear(year);
			delta += numberOfDaysOfGivenYearUntilGivenDate(year,month,day);
		}
		else 
		{
			// cout << "DEBUG given year is the same as base year << endl;
			if (month > m_baseMonth) 
			{
				// cout << "DEBUG given month is not the same as base month" << endl;
				delta += remainingDaysOfGivenMonth(m_baseYear,m_baseMonth,m_baseDay);
				// full months from next month, before given month
				for (int m = m_baseMonth+1 ; m < month ; m++)
					delta += daysInAMonth(year,m);
				delta += day; // days in the given month

			}
			else 
			{
				// cout << "DEBUG given month is the same as base month" << endl;
				delta += day - m_baseDay;
			}
		}
	}
	else 
	{
		// to solve the problem for the past, one can only invert the problem
		// so that the problem becomes future (already solved)
		VBDate aux;
		aux.p_setDate(year,month,day); // here, setDate can not be used.
		// if used, an infinite re-enntry loop is generated.
		bool ret = aux.deltaDays(m_baseYear,m_baseMonth,m_baseDay,delta);
		delta *= -1;  // invert the delta to fix the inverted problem
		return ret;
	}
	return true;
}

long VBDate::deltaDays(const VBDate & cal) const
{
	long ret;
	deltaDays(cal.m_baseYear,cal.m_baseMonth,cal.m_baseDay,ret);
	return ret;
}


int VBDate::getWeekDay(const int year,const int month,const int day) const
{
	bool OK = validDate(year,month,day);
	if (!OK) return VBinvalid;

	long delta;
	deltaDays(year,month,day,delta);
	// cout << "DEBUG delta=" << delta << endl;

	// the code below is valid for both positive and negative delta
	int k = delta % 7; // k is positive or negative
	k += m_baseWeekDay + 7;  // plus 7 to guarantee k > 0
	return (k % 7);  // return 0 ~ 6
}

int VBDate::getWeekDay() const
{
	return getWeekDay(m_baseYear,m_baseMonth,m_baseDay);
}

VBDate VBDate::dateAfterDeltaDays(const long d) const
{
	long deltaDays = d;
	VBDate ret;
	ret.setDate(m_baseYear,m_baseMonth,m_baseDay);
	if (deltaDays > 0)
	{
		// go to future
		while (deltaDays != 0)
		{
			ret.tomorrow();
			deltaDays--;
		}
	}
	else
	{
		// go to past
		while (deltaDays != 0)
		{
			ret.yesterday();
			deltaDays++;
		}
	}
	return ret;
}


void VBDate::tomorrow()
{
	int daysThisMonth = daysInAMonth(m_baseYear,m_baseMonth);
	if (m_baseDay < daysThisMonth)
	{
		setDate(m_baseYear,m_baseMonth,m_baseDay+1);
		return;
	}
	if (m_baseMonth < VBdec)
	{
		// it it the last day of month, so tomorrow is the first day of next month
		setDate(m_baseYear,m_baseMonth+1,1);
		return;
	}
	// it is the last day of year, so tomorrow is the first day of next year
	setDate(m_baseYear+1,VBjan,1);
}


void VBDate::yesterday()
{
	if (m_baseDay > 1)
	{
		setDate(m_baseYear,m_baseMonth,m_baseDay-1);
		return;
	}

	if (m_baseMonth != VBjan)
	{
		// it is the first day of a month not January, so yesterday is the last day of previous month
		int daysLastMonth = daysInAMonth(m_baseYear,m_baseMonth-1);
		setDate(m_baseYear,m_baseMonth-1,daysLastMonth);
		return;
	}

	// it is the first day of year, so yesterday is the last day of previous year
	setDate(m_baseYear-1,VBdec,31);
}


VBString VBDate::getDate(int format) const
{
	const char *monthName[][12] = {
		{ // 0 - english	
			"January", "February", "March", "April", "May", "June", 
			"July", "August", "September", "October", "November", "December" 
		},
		{ // 1 - portuguese
			"Janeiro", "Fevereiro", "Mar�o", "Abril", "Maio", "Junho", 
			"Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" 
		}
	};

	VBString ret;
	switch (format)
	{
	case 1: // Example: "November 24th, 2001"
		ret = monthName[0][this->m_baseMonth];
		ret += " ";
		ret += this->m_baseDay;
		switch (this->m_baseDay)
		{
		case 1:
		case 21:
		case 31:
			ret += "st";
			break;
		case 2:
		case 22:
			ret += "nd";
			break;
		case 3:
		case 23:
			ret += "rd";
			break;
		default:
			ret += "th";
		}
		ret += ", ";
		ret += this->m_baseYear;
		break;
	case 2: // Example: yyyy-mm-dd, that is, "2001-11-24"
		ret = this->m_baseYear;
		ret += "-";
		ret += twoDigits(this->m_baseMonth + 1);
		ret += "-";
		ret += twoDigits(this->m_baseDay);
		break;
	case 3: // Example: "24 de Novembro de 2001"
		ret = this->m_baseDay;
		ret += " de ";
		ret += monthName[1][this->m_baseMonth];
		ret += " de ";
		ret += this->m_baseYear;
		break;
	case 4: // Example: yyyy-mm-dd, that is, "20011124"
		ret = this->m_baseYear;
		ret += twoDigits(this->m_baseMonth + 1);
		ret += twoDigits(this->m_baseDay);
		break;
	default:
		ret = "";
	}

	return ret;

}

VBString VBDate::twoDigits(int i) const
{
	VBString ret;
	if (i < 10)
		ret = "0";
	ret += i;
	return ret;
}

/////////////////////////////////////////////////////////////////
//
// class VBDateTime
//
/////////////////////////////////////////////////////////////////

const char *VBDateTime::monthName[] = {	
	"Jan", "Feb", "Mar", "Apr", "May", "Jun", 
	"Jul", "Aug", "Sep", "Oct", "Nov", "Dec" 
};

	
// default constructor
VBDateTime::VBDateTime()
{
}

VBString VBDateTime::getTimeStr() const
{
	time_t now;
	time( &now ); // get time and date
	tm *tm_now = localtime( &now ); // convert to struct tm

	VBString ret;
	ret += " ";
	ret += twoDigits(tm_now->tm_hour);
	ret += ":";
	ret += twoDigits(tm_now->tm_min);
	ret += ":";
	ret += twoDigits(tm_now->tm_sec);
	return ret;
}

// Example: 31-Mar-2002 23:59:59 GMT
VBString VBDateTime::getDateStr(int dateFormat) const
{
	VBString ret;
	VBDate date = *this; // get VBDate object from this
	ret = date.getDate(dateFormat);
	ret += " " + getTimeStr();
//	ret += " GMT";
	return ret;
}

VBString VBDateTime::twoDigits(int i) const
{
	VBString ret;
	if (i < 10)
		ret = "0";
	ret += i;
	return ret;
}

// add or subtract time to this VBDate
void VBDateTime::addYear(int year)
{
#if 0
	time_t secondsToAdd = 365 * 24 * 60 * 60; // seconds of 1 year
	secondsToAdd *= year;
	time_t now = mktime(&m_tm);
	time_t newDateTime = now + secondsToAdd;
	setThisTM(newDateTime);
#endif

	m_tm.tm_year += year;

	VBDate cal_from;
	if (!cal_from.setDate(m_tm.tm_year+1900+year,m_tm.tm_mon,m_tm.tm_mday))
	{
		// invalid date. This can only be caused by leap year
		m_tm.tm_mon = 3; // march
		m_tm.tm_mday = 1;
	}
}

void VBDateTime::addMonth(int month)
{
#if 0
	time_t secondsToAdd = 30 * 24 * 60 * 60; // seconds of 1 month
	secondsToAdd *= month;
	time_t now = mktime(&m_tm);
	time_t newDateTime = now + secondsToAdd;
	setThisTM(newDateTime);
#endif

	m_tm.tm_mon += month;

	VBDate cal_from;
	if (!cal_from.setDate(m_tm.tm_year+1900,m_tm.tm_mon+month,m_tm.tm_mday))
	{
		// invalid date. This can only be caused by leap year
		m_tm.tm_mon = 3; // march
		m_tm.tm_mday = 1;
	}
}

void VBDateTime::addDay(int day)
{
	time_t secondsToAdd = 24 * 60 * 60; // seconds of 1 day
	secondsToAdd *= day;
	time_t now = mktime(&m_tm);
	time_t newDateTime = now + secondsToAdd;
	setThisTM(newDateTime);
}

void VBDateTime::addHour(int hour)
{
	time_t secondsToAdd = 60 * 60; // seconds of 1 hour
	secondsToAdd *= hour;
	time_t now = mktime(&m_tm);
	time_t newDateTime = now + secondsToAdd;
	setThisTM(newDateTime);
}

void VBDateTime::addMinute(int minute)
{
	time_t secondsToAdd = 60; // seconds of 1 minute
	secondsToAdd *= minute;
	time_t now = mktime(&m_tm);
	time_t newDateTime = now + secondsToAdd;
	setThisTM(newDateTime);
}

void VBDateTime::addSecond(int second)
{
	time_t secondsToAdd = second;
	time_t now = mktime(&m_tm);
	time_t newDateTime = now + secondsToAdd;
	setThisTM(newDateTime);
}

void VBDateTime::setThisTM(time_t ltime)
{
	struct tm *p_tm = localtime( &ltime ); // convert to struct tm
	// copy each attribute
	m_tm.tm_sec  = p_tm->tm_sec;    // seconds after the minute - [0,59] 
	m_tm.tm_min  = p_tm->tm_min;    // minutes after the hour - [0,59] 
	m_tm.tm_hour = p_tm->tm_hour;   // hours since midnight - [0,23] 
	m_tm.tm_mday = p_tm->tm_mday;   // day of the month - [1,31] 
	m_tm.tm_mon  = p_tm->tm_mon;    // months since January - [0,11] 
	m_tm.tm_year = p_tm->tm_year;   // years since 1900 
	m_tm.tm_wday = p_tm->tm_wday;   // days since Sunday - [0,6] 
	m_tm.tm_yday = p_tm->tm_yday;   // days since January 1 - [0,365] 
	m_tm.tm_isdst = p_tm->tm_isdst; // daylight savings time flag 
}


void VBDateTime::setNow()
{
	time_t ltime; 
	time( &ltime ); // get number of seconds elapsed since 00:00:00 on January 1, 1970, 
	setThisTM(ltime);

	time_t tz_sec; // Difference in seconds between Greenwitch time and local time
#ifdef WIN32	
	tz_sec = timezone; 
#else
	// TODO not working for unix :(
	// struct timezone tz;
	// gettimeofday(null,&tz);
	// tz_sec = tz->tz_minuteswest;
#endif
//	m_timeRelativeToGreenwitch = tz_sec / 60 / 60; // Difference in hours
	// cout << "DEBUG: " << m_timeRelativeToGreenwitch  << endl; // Brazil is -3
}

VBDateTime::operator VBDate () const
{
	VBDate ret;
	ret.setDate(m_tm.tm_year+1900,m_tm.tm_mon,m_tm.tm_mday);
	return ret;
}




/////////////////////////////////////////////////////////////////////
//
// class VBFileDirectory
//
/////////////////////////////////////////////////////////////////////

bool VBFileDirectory::p_goodName(VBString fileaName) 
{
	return ((VBString (".") != fileaName) && (VBString ("..") != fileaName) );

}

VBString VBFileDirectory::p_lastFileName(VBString path) 
{
	unsigned tc = path.tokCount('/');
	path = path.strtok('/',tc);
	tc = path.tokCount('\\');
	path = path.strtok('\\',tc);
	return path;
}

// default constructor
VBFileDirectory::VBFileDirectory () 
{ 
	m_isDir = false;
}
        
bool VBFileDirectory::isDir () 
{
	return m_isDir;
}
    
VBString VBFileDirectory::getPath () 
{
	return m_dirPath;
}
    
VBString VBFileDirectory::getNext() 
{
	return p_get();
}

VBString VBFileDirectory::getFirst(const char *path) 
{
	m_path = path;
	m_dirPath = path;
	#ifdef _MSC_VER // Visual C++ only
		VBString mask = path + VBString("/");
		mask += "*.*";
		m_h = FindFirstFile(mask, &m_f);
		m_endOfList = (m_h == 0) || (m_h == (void*)0xffffffff);
		if (m_endOfList) return "";
		VBString ret = m_f.cFileName;
		if (!p_goodName(ret)) ret = getNext();
		return ret;
	#else // unix only
		m_dp = opendir(path);
		m_endOfList = (m_dp == 0);
		if (m_endOfList) return "";
		return p_get();
	#endif
}
    
bool VBFileDirectory::eolist() 
{
	#ifdef _MSC_VER // Visual C++ only
	#else // unix only
		if (m_endOfList) closedir(m_dp);
	#endif
	return m_endOfList;
}

// set m_isDir true if argument is a directory
void VBFileDirectory::p_isDirectory (VBString file) 
{
#ifdef _MSC_VER // Visual C++ only
	DWORD attribs = GetFileAttributes(file);
	m_isDir = (attribs & FILE_ATTRIBUTE_DIRECTORY) != 0;
	// if (attribs == 0xFFFFFFFF) m_isDir=false;
#else // unix only
	DIR *dp = opendir(file); 
	struct stat statbuf; 
	lstat(file,&statbuf);
	m_isDir = (S_ISDIR(statbuf.st_mode) != 0);
#endif
	if (m_isDir) {
		m_dirPath = file;
	}
}

VBString VBFileDirectory::p_get() 
{
	bool isGoodFileName;
	VBString ret;
#ifdef _MSC_VER // Visual C++ only
	m_endOfList = !FindNextFile(m_h, &m_f);
	ret = m_f.cFileName;
	do {
		isGoodFileName = p_goodName(ret);
		if (!isGoodFileName && !m_endOfList) ret=p_get(); // recursive
	} while (!isGoodFileName);
	ret = p_lastFileName(ret);
#else // unix only
	struct dirent *dirp;
	do {
		dirp = readdir(m_dp);
		m_endOfList = (dirp == 0);
		if (m_endOfList) return"";
		isGoodFileName = p_goodName(dirp->d_name);
	} while (!isGoodFileName);
	ret = dirp->d_name;
#endif
	ret = m_path + VBString("/") + ret;
	p_isDirectory(ret); // set m_isDir
	return ret;
}
