// vblib.h
/**************************************************************** 
VBLib stands for "Villas-Boas Library"
VBLib is multiplatform (works exactly equal in windows and unix)
by sbVB: Sergio Barbosa Villas-Boas (villas@del.ufrj.br)
(home page: http://www.del.ufrj.br/~villas/, www.sbVB.net)
VBLib home page: http://www.vbmcgi.org/vblib

***************| VBLib classes |******************************
VBList - a linked list of a generic (template) class
VBMemCheck - a class to detect memory leak (works for Visual C++ only)
VBString - The Villas-Boas class for string in C++ multiplatform
VBParserType - evaluate a math expression in execution time
VBDateTime- a class to get weekday of given date and number of days 
  between dates.
VBShowSTLContainer - a generic class to show the contents of a STL container
VBClock - a clock
VBMD5 - MD5 encryption algorithm

code to be used in windows only are conditionally compiled
based on the define WIN32 (#ifdef WIN32).
code to be used in unix only are conditionally compiled
based on the absence of define WIN32 (#ifndef WIN32).
  
*) July 6th, 2000 - Version 1.1  
After being sure that it works fine in both Windows and Unix,
the release was made public

*) August 7th, 2000 - Version 1.2
Added methods to VBString:
beforeFind - return string before "find" string
afterFind -  return string after "find" string
strchg - find and replace 1 string 
strschg - find and replace all instances of find string 
strtok - get substring delimited by token
strtokLen - number of substrings delimited by token
Added template class VBParser and complete class VBParserType
New name for VBMemCheck (old name kept for compatibility)

*) August 21st, 2000 - Version 1.3
added type cast of VBString to "const char *"
Now any function that admits const char * as parameter can receive VBString

*) November 19th, 2000 - Version 1.4
added method "remove" to VBList. Now, one element may be removed from list
file "test_vblist5.cpp" shows the usage of "remove" method. 

*) November 27th, 2000 - Version 1.5
several functions added to VBString
int length();
int tokCount(char tok);
char operator[](unsigned long i);
VBString strAfter(int pos);
VBString strBefore(int pos);
int strtokpos(char tok, int n);
void strtokchg(char tok, int n, const char *newData);  // good function

*) December 4th, 2000 - Version 1.6
added 
VBString operator+(const char ch);
void operator+=(const char ch);
solved a bug in 
VBString operator+(const int i);

*) January 4th, 2001 - Version 1.7
added 
void operator= (const char ch);
void p_extendedToLower(); // private
void p_extendedToUpper(); // private
void ToLower(); // ignore extended char
void ToUpper(); // ignore extended char
void extendedToLower(); // convert to extended char lo caps
void extendedToUpper(); // convert to extended char hi caps
void safeToLower(); // convert to standard ascii char lo caps
void safeToUpper(); // convert to standard ascii char hi caps

*) January 17th, 2001 - Version 1.8
void simpleEncrypt(unsigned short code);
void simpleEncryptReverse(unsigned short code);
void toSafe(); // convert to safe (non-extended) ascii

*) February 09th, 2001 - Version 1.9
// new member function
int countString(const char *subStr); // return the number of sub-strings
member strchg moved to private
VBparser moved to VBmath

*) February 28th, 2001 - Version 2.0
fixed a bug in strschg that didn't work well when 
  .)the replace string contained the find string by left. Now ok.
New tutorial in the web
bool strstr(const char* subStr);  // return true if the string contains the sub string
Some changes (bug fix) in VBList
  .) no more method get (this is needless. Scan the list yourself and get what you want)
  .) methods add and remove now have const parameter.
  .) remove method now removes all elements of the list, and not only the 1st one
Inclusion of class VBClock
Inclusion of class VBShowSTLContainer
Inclusion of support to use with namespace std
Fixed a bug in getLine, to avoid unwanted chars in input buffer

(some intermediate versions were not disclosed)

*) March, 21th, 2001 - Version 2.6
Adapted class VBShowSTLContainer to be able to show a sample of the 
  container
Fixed a bug in method strschg, that added incorrectly the replace string
  at the end if the find string didn't exist
Fixed a bug in the copy contructor of VBString. Now it is const.
The header <math.h> was removed.
No more define VBLIB_VERSION. The vesion is given in the comment of
  this header file only.

*) May, 2nd, 2001 - Version 2.7
Added class VBDate

*) January, 10th 2002 - Version 3.0
added methods in VBString
void escapeSequence();   // convert object to web escape sequence
void escapeSequenceReverse(); // convert object to web escape sequence reverse
now, size of VBString and related methods are unsigned long
Example: void strtokchg(char tok, unsigned long n, const char *newData);  
Upgrde in class VBList
now it has copy constructor and operator=

*) September, 2nd 2002 - Version 4.0
many reforms
a few added methods to VBString
solved a bug in escapeSequence
solved a bug in strtokpos
many methods converted to const (much better !)
no more strstr
Added method testMemoryLeak to VBMemCheck
New classes VBDate and VBDateTime
Solved a bug in strBefore
Fixed bug in getLine to allow read windows text files in unix

*) Febryary, 4th 2003 - Version 4.1
Solved a bug in operator>> when using namespace std

*) Febryary, 25th 2003 - Version 4.2
added methods (useful for expat XML processing)
void setUnterminatedString(const char *str, size_vbs length); // this = str
void wipeLeadingSpace(); // wipe leading spaces, tabs and returns
const char *c_str(); // to be the same as std::string
void wipeCtrmM() // wipe ^M (windows text file read in unix)

*) Febryary, 25th 2003 - Version 4.3
length method equal to getLength

*) May, 29th 2003 - Version 5.0

Added class VBMD5, for MD5 hash calculation
Added methods MD5_hash and get_MD5_hash in VBString
Added methods to check for valid email, 

_MSC_VER for Visual C++
****************************************************************/

#ifndef __VBLIB__
#define __VBLIB__

// comment out this line to get rid of debug
//#define VBSTRINGDEBUG

// from version 5 on, VBLib defines VBLIB_USE_NAMESPACE_STD by default
#ifndef VBLIB_USE_NAMESPACE_STD
#define VBLIB_USE_NAMESPACE_STD
#endif

// if one wants not to define VBLIB_USE_NAMESPACE_STD, 
// the developer should define VBLIB_DO_NOT_USE_NAMESPACE_STD
#ifdef VBLIB_DO_NOT_USE_NAMESPACE_STD
#undef VBLIB_USE_NAMESPACE_STD
#endif

/////////////////////////////////////////////////////////////////////
//
// includes of VBLib
//
/////////////////////////////////////////////////////////////////////

#include <stdarg.h> // variable number of parameters

#ifdef VBLIB_USE_NAMESPACE_STD
	#include <iostream>
	#include <fstream>
	#include <string>
	#include <cstdio>  // sprintf
	#include <cstdlib>  // abs
	#include <cctype> // isspace
	#include <ctime>  // time
	using namespace std;
#else
	#include <iostream.h>
	#include <fstream.h>
	#include <string.h>
	#include <stdio.h>  // sprintf
	#include <stdlib.h>  // abs
	#include <ctype.h> // isspace
	#include <time.h>  // time
#endif

#ifdef _MSC_VER  // Visual C++ only !!!
	#include <crtdbg.h> // include necessary to handle memory leak debug (win32 only)
	#include <atlbase.h> // BSTR
#else // unix only
	#include <sys/stat.h> // struct stat, S_ISDIR
	#include <dirent.h> // DIR
#endif


namespace sbVB
{

/////////////////////////////////////////////////////////////////////
//
// defines of VBLib
//
/////////////////////////////////////////////////////////////////////
#define VB_SAFEDELETE(x) {if (x) {delete x; x=0;} }
#define VB_SAFEDELETE_A(x) {if (x) {delete [] x; x=0;} }
#ifndef NULL
	#define NULL 0
#endif

/////////////////////////////////////////////////////////////////////
//
// the two defines below make both definitions correct
// it's better to use escapeSequence and escapeSequenceReverse
//
/////////////////////////////////////////////////////////////////////
#define escapeSequence scapeSequence 
#define escapeSequenceReverse scapeSequenceReverse 


/////////////////////////////////////////////////////////////////////
//
// VBShowSTLContainer
// class to show a STL container
// if n==0, show the complete container, 
// if n > 0, show only n first elements
//
/////////////////////////////////////////////////////////////////////
template <class containerType> 
void VBShowSTLContainer(const containerType & containerObj, const int sampleCount = 0,
						ostream & myCout = cout) 
{   
	typename containerType::const_iterator iteratorObj;   
	if (containerObj.empty()) {     
		myCout << "======Container empty" << endl;
		return;   
	}   
	myCout << "======show the STL container ";
	if (sampleCount>0) 
		myCout << "sample(" << sampleCount << ")";
	else
		myCout << "contents";
	myCout << ", size=" << containerObj.size() << endl;   
	int n = 0;
	for (iteratorObj = containerObj.begin() ;     
		(iteratorObj != containerObj.end()) && ( !(sampleCount>0) || (n < sampleCount) ) ;     
		iteratorObj++) {
		myCout << *iteratorObj << endl; 
		n++;
	}
}; 


/////////////////////////////////////////////////////////////////////
//
// VBClock
// a class make a clock
//
/////////////////////////////////////////////////////////////////////
class VBClock 
{
	time_t m_start, m_finish;
	double m_duration;
	void checkDiffTime() 
	{
		m_finish = clock();
		m_duration = (double)(m_finish - m_start) / CLOCKS_PER_SEC;
	}
public:
	void markTime() 
	{
		m_start = clock();
	}
	VBClock() 
	{ 
		markTime(); 
	}
	double getDiffTime() 
	{
		checkDiffTime();
		return m_duration;
	}
	void printDiffTime(ostream & myCout = cout) 
	{
		checkDiffTime();
		myCout.precision(2);
		myCout << "Elapsed time (in seconds) is " << getDiffTime() << endl;
	}
};

/////////////////////////////////////////////////////////////////////
//
// VBMemCheck
// a class to check memory leak
//
/////////////////////////////////////////////////////////////////////
// VBMemCheck is the new name. Keeping old name vb_memCheck for compatibility
#define vb_memCheck VBMemCheck
class VBMemCheck 
{
public:	
#ifdef _MSC_VER
private:	
	_CrtMemState m_s1; 

	void reportToStdout()
	{
		// Send all reports to STDOUT
		_CrtSetReportMode(_CRT_WARN,   _CRTDBG_MODE_FILE);
		_CrtSetReportFile(_CRT_WARN,   _CRTDBG_FILE_STDOUT);
		_CrtSetReportMode(_CRT_ERROR,  _CRTDBG_MODE_FILE);
		_CrtSetReportFile(_CRT_ERROR,  _CRTDBG_FILE_STDOUT);
		_CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
		_CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDOUT);
	}

	void reportToNothing()
	{
		// Send all reports to null
		_CrtSetReportMode(_CRT_WARN,   _CRTDBG_MODE_FILE);
		_CrtSetReportFile(_CRT_WARN,   _CRTDBG_REPORT_FILE);
		_CrtSetReportMode(_CRT_ERROR,  _CRTDBG_MODE_FILE);
		_CrtSetReportFile(_CRT_ERROR,  _CRTDBG_REPORT_FILE);
		_CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
		_CrtSetReportFile(_CRT_ASSERT, _CRTDBG_REPORT_FILE);
	}

public:	
	VBMemCheck(int p1=0, int p2=0) 
	{  
		// dirty trick to avoid bug in memory leak detection
		// Run once the program using no parameter, and see the memory leak. 
		// copy the 2 pointers leaking to the parameters.
		// that should avoid the memory leak detection bug
		// if you mistake in using proper pointer, it's very likelly that your program will hang
		if (p1) delete (void*)p1;
		if (p2) delete (void*)p2;

	// Store memory checkpoint in s1 (global variable) memory-state structure
		_CrtMemCheckpoint(&m_s1);
	};
#endif
	void check() 
	{
#ifdef _MSC_VER
		reportToStdout();
		_CrtDumpMemoryLeaks();
#else
		cout << "Memory Leak not tested" << endl;
#endif
	};

	void dump()
	{
		check();
	}

	bool testMemoryLeak()
	{
#ifdef _MSC_VER
		reportToNothing();
		return _CrtDumpMemoryLeaks() != 0;
#else
		return false;
#endif
	}
};


/////////////////////////////////////////////////////////////////////
//
// VBList
// a generic (using template) single linked list 
//
/////////////////////////////////////////////////////////////////////


template <class dataType>
class VBList 
{
	struct node {
		dataType data;
		node *p_next;
	}; // end of class node
	
	node *p_first, *p_last, *p_nextSave;
	//  void (*error_function)();  // pointer to function
	
		dataType *GetFirstConst(void* & externalPointer) const {
			if (p_first)
			{
				externalPointer = p_first->p_next;
				return &p_first->data;
			}
			else
				return NULL;
		};

		dataType *GetNextConst(void* & externalPointer) const {
			node *ret = (node*) externalPointer;
			if (ret)
			{
				externalPointer = (void*) ret->p_next;
				return &ret->data;
			}
			else
				return NULL;
		};

	public:
		// default constructor
		VBList() 
		{  
			p_first = p_last = p_nextSave = NULL;
			// error_function=error_fun;
		}; // end of VBList();

		dataType *GetFirst() {
			if (p_first)
			{
				p_nextSave = p_first->p_next;
				return &p_first->data;
			}
			else
				return NULL;
		};

		dataType *GetNext() {
			node *ret = p_nextSave;
			if (p_nextSave)
			{
				p_nextSave = (node*) p_nextSave->p_next;
				return &ret->data;
			}
			else
				return NULL;
		};
	
		// add data to list (addTail)
		bool add(const dataType & newData) 
		{
			node *p_new;
			//	ASSERT(p_new=new node);
			if ((p_new = new node) == NULL)
				return (1); 
			// return true if allocation fails
			
			p_new->data = newData;  // copies data to list
			// if new element is the first element of list
			if (!p_first)
			{
				p_first = p_last = p_new;
				p_new->p_next = NULL;
			}
			// if new element is NOT the first element of list
			else 
			{ 
				p_last->p_next = p_new; // previous p_last points to new element
				p_new->p_next = NULL; // finnish list
				p_last = p_new;  // now p_last points to new element
			}
			return (0);
		}; // end of void add(dataType & newData)

		// delete the whole list
		void deleteAll() 
		{
			node *p_t1, *p_t2;
			if (p_first) // if list exists
			{
				p_t1 = p_first;
				p_t2 = p_first->p_next;
				do 
				{ // since list exists, at least 1 delete
					delete p_t1;
					p_t1 = p_t2;
					if (p_t2) 
						p_t2 = p_t2->p_next;
				} while (p_t1 != NULL);
				p_last = p_first = NULL;
			} 
		};  // end void deleteAll()
		
		// delete one data from list
		void remove(const dataType & dataToDelete) {
			node *p_t1, *p_t2, *p_t3;
			if (p_first) // if list exists
			{
				p_t1 = p_first;
				p_t2 = p_first->p_next;

				// if data to delete is the first one
				// dataType must have operator== defined
				if (p_first->data == dataToDelete) {
					// for debug
					// cout << "DEBUG Deleted:"<< p_first->data << endl;
					delete p_first;
					p_first = p_t2;
					remove(dataToDelete); // recursivelly calls remove, to 
						// be sure 
				}
				else { // the data to delete is not the first one
					
					// this loop will seek for data to delete and delete it
					while (p_t1->p_next != NULL)
					{ // since list exists, at least 1 delete

						if (p_t2->data == dataToDelete) {
							// for debug
							// cout << "DEBUG Deleted:"<< p_t2->data << endl;
							p_t3 = p_t2->p_next;
							delete p_t2;
							p_t1->p_next = p_t3; // re-links the list (bypass p_t2)
							// break; // if after deleting, stop the loop, uncomment this break
							// but it's better not to stop. Keep trying to delete until the end of list
							p_t2 = p_t3; // to keep going
						}
						else {
							// move pointers one step into the list
							// only if no remove was made.
							p_t1 = p_t2;
							p_t2 = p_t2->p_next;
						}

					} // while
				} // else
			} // if p_first
		} // remove

		// default destructor
		~VBList() 
		{ 
			deleteAll();
		};// end of ~VBList();

		// copy constructor
		VBList(const VBList & obj) {
			p_first = p_last = p_nextSave = NULL;
			void *externalPointer;
			dataType *p;
			for (p = obj.GetFirstConst(externalPointer) ; p ; p = obj.GetNextConst(externalPointer)) {
				add(*p);
			}
		}

		void operator=(const VBList & obj) {
			void *externalPointer;
			dataType *p;
			for (p = obj.GetFirstConst(externalPointer) ; p ; p = obj.GetNextConst(externalPointer)) {
				add(*p);
			}
		}
}; // end of template <class dataType>


/////////////////////////////////////////////////////////////////////
//
// VBString class
//
/////////////////////////////////////////////////////////////////////

/* VB_N is the number of chars per buffer in strList
this number can be anything more than 1, so that the 
usability of VBString is not changed.
Internally, in operator>> (for instance), "chunks" of
VB_N bytes will be alloced  in a list to store
input stream of bytes.
*/

// size of string blocks when using getline method.
// changing this number does not affect usage of the VBString class
static const int VB_N = 80;

// type to define the number of chars in a VBString
typedef unsigned long size_vbs;

class VBString
{

	// code for debug purposes only
	struct VBObjectType // struct for debug
	{
	#ifdef VBSTRINGDEBUG
		char debug;
		int tag;  // object tag
	#endif // VBSTRINGDEBUG
	};
	static VBObjectType _VBObject;
	// end of code for debug

	struct strList // char used in linked list, in method getLine (operator>>)
	{
		char buffer[VB_N];

		// useless, but needed to compile in unix
		bool operator==(strList) {return true;};
	};

	void allocStr(size_vbs size_in);  // redim

	size_vbs m_size;		// number of char inside the string
	unsigned m_decs;		// number of decimals when adding float or double
	char *m_str;			// the pointer to the string data
	istream * m_myIstream;	// pointer to copy of cin

	static const char *m_chgHiCaps; //     = "�����������������������Ǫ�";
	static const char *m_chgLoCaps; //     = "�����������������������窺";
	static const char *m_chgHiCapsSafe; // = "AEIOUAEIOUAEIOUAEIOUAONCAO";
	static const char *m_chgLoCapsSafe; // = "aeiouaeiouaeiouaeiouaoncao";

	// this consctuctor is private
	// code must be in the header due to initialization of m_myIstream
	// VBString(const int len) : m_myIstream(cin)
	VBString(const int len);

	void error(const char *) const;

	VBString strtok_helper(char tok, size_vbs n, bool & found, size_vbs & tokPos) const;
	void initCin() { m_myIstream = &cin; } // code nas to be in header due to instantiation of cin
	void privateConstructor();
	char p_extendedToUpper(char in);
	char p_extendedToLower(char in);
	char p_safeToUpper(char in);
	char p_safeToLower(char in);
	#ifdef VBSTRINGDEBUG
		int _stringObjectTag;
	#endif // VBSTRINGDEBUG
	
	unsigned char p_ToAscii(unsigned char z) const;
	unsigned char p_ToAsciiReverse(unsigned char z) const;
	void p_toHex(unsigned char ch, char *ch_hex) const;
	unsigned char p_toHexReverse(char *ch_hex) const;
	char p_toSafe(char in) const;
	void strchg(const char *find,const char *replace);

	VBString strAfter(size_vbs pos) const;
	VBString strBefore(size_vbs pos) const;

	void getLineNamespace(char endChar = '\n'); // auxiliary method for extractor
public:

	// constructors
	VBString(const VBString & s); // copy constructor
	VBString(const char* s = ""); // default contructor

	// destructor
	~VBString();
	
	// const comparison operators
	bool operator== (const VBString & s) const;
	bool operator!= (const VBString & s) const;
	bool operator<  (const VBString & s) const;
	bool operator<= (const VBString & s) const;
	bool operator>  (const VBString & s) const;
	bool operator>= (const VBString & s) const;

	// const comparison operators (continuation)
	bool operator== (const char * s) const;
	bool operator!= (const char * s) const;
	bool operator<  (const char * s) const;
	bool operator<= (const char * s) const;
	bool operator>  (const char * s) const;
	bool operator>= (const char * s) const;
	
	// const operator+
	VBString operator+(const VBString & s) const;
	VBString operator+(const char* s) const;
	VBString operator+(const char ch) const;
	VBString operator+(const int i) const;
	VBString operator+(const unsigned i) const;
	VBString operator+(const float f) const;
	VBString operator+(const double d) const;
	VBString operator+(const long double d) const;
	// global function to contatenate strings
	friend VBString operator+(const char* str,const VBString & vbs);

	// non-const operator=
	void operator=(const VBString & s);
	void operator=(const char* s);
	void operator=(const char ch);
	void operator=(const int i);
	void operator=(const unsigned i);
	void operator=(const float f);
	void operator=(const double d);
	void operator=(const long double d);

	// non-const operator+=
	void operator+=(const VBString & s);
	void operator+=(const char* s);
	void operator+=(const char ch);
	void operator+=(const int i);
	void operator+=(const unsigned i);
	void operator+=(const float f);
	void operator+=(const double d);
	void operator+=(const long double d);

	// basic utilities
	size_vbs getLength() const {return m_size;}; // get the string length (number of chars)
	size_vbs length() const {return m_size;}; // get the string length (number of chars)
	bool IsEmpty() const; // true if zero length
	void Empty(); // clear contents to empty
	void myDelete(); // release memory used by object
	operator const char*() const {return m_str;};  // VBString type cast for "const char*"
	const char *getStr() const {return m_str;};
	const char *c_str() const {return m_str;};
	char getChar(size_vbs pos) const; // return char at position
	void putChar(size_vbs pos,char ch); // put char at position, if this has enough length

	// handling number of decimal digits after the dot, when converting float point to VBString
	void setDecs(const unsigned decs) { m_decs = decs; };
	unsigned getDecs() const { return m_decs; };

	// handling of token
	size_vbs countSubString(const char *subStr) const; // return the number of sub-strings
	size_vbs tokCount(const char tok) const;
	VBString strtok(const char tok, const size_vbs n) const;
	VBString strtok(const char tok, const size_vbs n, bool & found) const;
	void strtokchg(const char tok, const size_vbs n, const char *newData);

	// advanced string manipulation
	VBString strAfterPosition(const size_vbs pos) const;
	VBString strUntilPosition(const size_vbs pos) const;
	VBString strInside(size_vbs from, size_vbs to) const; 
	size_vbs strtokpos(const char tok, const size_vbs n) const;
	VBString afterFind(const char findChar) const;
	VBString afterFind(const char findChar, bool & found) const;
	VBString afterFind(const char *find) const;
	VBString afterFind(const char *find, bool & found) const;
	VBString beforeFind(const char *find) const;
	VBString beforeFind(const char findChar) const;
	void strschg(const char *find,const char *replace); // deprecated method. Use "replace" instead
	void replace(const char *find,const char *replaceStr) 
		{ strschg(find,replaceStr); }; // alternative name for strschg
	bool existSubstring(const char* subStr) const;
	VBString debug(int format = 1) const;
	void setUnterminatedString(const char *str, size_vbs length); // this = str
	void wipeLeadingSpace(); // wipe leading spaces, tabs and returns
	void wipeCtrmM(); // wipe ^M (happens when reading windows text files in Windows
	bool eofDetected() const;

	void setIstream(istream & is) { m_myIstream = &is; };
	void getLine(char endChar = '\n'); // auxiliary method for extractor

	#ifdef VBLIB_USE_NAMESPACE_STD
	// get next word of a stream
	// this method can only be used if namespace std is used
	void getNextWord(istream & stream);
	#endif


	// overload insersor and extractor
	// code must be in the header due to instantiation of istream
	friend istream & operator>>(istream & stream, VBString & obj) // extractor
	{
		obj.setIstream(stream);
		#ifdef VBLIB_USE_NAMESPACE_STD
			obj.getLineNamespace();
		#else
			obj.getLine();
		#endif
		
		return stream;
	}
	// code must be in the header due to instantiation of ostream
	friend ostream & operator<<(ostream & stream, const VBString & obj) // insersor
	{
		if (obj.m_size) 
			stream << obj.m_str;
		return stream;
	}

	// char case manipulation, allowing accented char
	void toLower(); // ignore extended char
	void toUpper(); // ignore extended char
	void extendedToLower(); // convert to extended char lo caps
	void extendedToUpper(); // convert to extended char hi caps
	void safeToLower(); // convert to standard ascii char lo caps
	void safeToUpper(); // convert to standard ascii char hi caps
	void toSafe(); // convert to safe (non-extended) chars

	// simple encryption handling
	void simpleEncrypt(unsigned short code = 46);
	void simpleEncryptReverse(unsigned short code = 46);
	void MD5_hash(); // hash (non reversible encrypt) this using MD5 algorithm
					 // after MD5_hash method, *this becomes a 32 char long string 
					 // (independendant from original length).
	VBString get_MD5_hash() const; // hash (non reversible encrypt) this using MD5 algorithm
					 // return a 32 char long string (independendant from original length).

	// http escape sequence handling
	void escapeSequence();        // convert object to web escape sequence
	void escapeSequenceReverse(); // convert object to web reverse escape sequence

#ifdef _MSC_VER  // Visual C++ only !!!
	// methods for BSTR
	void getBSTR(const BSTR & pWord);
	BSTR exportBSTR();
#endif

	// check for valid methods
	bool validEmail() const;
	bool validAddress() const;
	bool validCity() const;
	bool validCIC() const;
	VBString rightCIC() const;
	bool validName() const;
	bool validPhone() const;
	bool validZip() const;
	bool validCreditCardNumber() const;
	bool validHelper(const char *validChars, const char* validSymbols, unsigned minSize=0, unsigned maxSize=0) const;
	bool charIsValid(char ch,const char *validChars) const;
private:
	unsigned findDigit (int i, int lenght, const char *buffer) const;
	char intToChar (int num) const { return (num+48); }
	int charToInt (char ch) const { return (ch-48); }

}; // end of VBString

/////////////////////////////////////////////////////////////////////
//
// VBMD5 is the Villas-Boas class for MD5 hash encryption
// read about md5 in the url below
// http://www.fourmilab.ch/md5/rfc1321.html
// by sbVB [Villas-Boas] 
// (villas@del.ufrj.br, www.del.ufrj.br/~villas, www.sbVB.com.br)
// based on code from Thomas Pfl�ger (unixtom@sensorme.de)
/************* sample code begin **********************
#include "vblib.h"
void main ()
{
	VBString source = "abc";
	VBMD5 a;
	VBString s;
	s = a.MD5_hash(source);
	cout << s << endl;
	s = source;
	s.MD5_hash();
	cout << s << endl;
	cout << "Hash of: " << source << " is '" << source.get_MD5_hash() << "'" << endl;
}
**********************sample code end ******************/
//
/////////////////////////////////////////////////////////////////////
class VBMD5
{
public:
	VBMD5(); // default constructor
	VBString MD5_hash(const VBString seed);
	VBString MD5_file(const VBString fileName);

private:
	// those methods might be public

	//Update context to reflect the concatenation of another buffer of bytes.
	void AddData(char const* pcData, int iDataLength);
	//Final wrapup - pad to 64-byte boundary with the bit pattern 
	//1 0*(64-bit count of bits processed, MSB-first)
	void FinalDigest(char* pcDigest);
	//Reset current operation in order to prepare a new one
	void Reset();

private:
	
	enum { BLOCKSIZE=64 };
	bool m_bAddData;
	void Char2Hex(unsigned char ch, char* szHex);
	void Binary2Hex(unsigned char const* pucBinStr, int iBinSize, char* pszHexStr);
	enum { MD128LENGTH=4 };;
	//Context Variables
	unsigned int m_auiBuf[4];
	unsigned int m_auiBits[2];
	unsigned char m_aucIn[64];
	//
	static unsigned int F1(unsigned int x, unsigned int y, unsigned int z);
	static unsigned int F2(unsigned int x, unsigned int y, unsigned int z);
	static unsigned int F3(unsigned int x, unsigned int y, unsigned int z);
	static unsigned int F4(unsigned int x, unsigned int y, unsigned int z);
	//This is the central step in the MD5 algorithm.
	static void MD5STEP(unsigned int (*f)(unsigned int x, unsigned int y, unsigned int z),
		unsigned int& w, unsigned int x, unsigned int y, unsigned int z, unsigned int data, unsigned int s);
	//
	//The core of the MD5 algorithm, this alters an existing MD5 hash to
	//reflect the addition of 16 longwords of new data. MD5Update blocks
	//the data and converts bytes into longwords for this routine.
	void Transform();
};


/////////////////////////////////////////////////////////////////////
//
// class VBDate
//
/////////////////////////////////////////////////////////////////////

class VBDateTime; // forward definition

// _VBWeekdays and _VBMonths are not used. They are declared not to have
// compiler error in some compilers
typedef enum VBWeekdays { VBsun, VBmon, VBtue, VBwed, VBthu, VBfri, VBsat, VBinvalid } _VBWeekdays;
// one should not use "oct" as short for october, and
// "dec" as short for december, because "oct" and "dec" identifiers
// are already used by the standard library
typedef enum VBMonths { VBjan, VBfeb, VBmar, VBapr, VBmay, VBjun,
	VBjul, VBaug, VBsep, VBoct, VBnov, VBdec } _VBMonths;


//                               jan fev mar apr may jun jul ago sep oct nov dec   
const int VBDaysInMonths[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

class VBDate 
{
	int m_baseYear;
	VBMonths m_baseMonth;
	int m_baseDay;
	VBWeekdays m_baseWeekDay;

	// set base date if given date is valid. Do nothing otherwise
	// ATTENTION: does not change m_baseWeekDay !
	bool p_setDate(int year, int month, int day);

	// return input with 2 digits
	VBString twoDigits(int i) const;

	// place in delta the number of days that separates the given day
	// from base day.
	// if date is not valid, do nothing and return false
	bool deltaDays(int year, int month, int day, long & deltaDays) const;

	// return true given date is valid
	// return false otherwise
	// does not depend on base date
	bool validDate(int year, int month, int day) const;

	// return true if the given day is future compared with base day
	// return false otherwise
	// if given day and base day are the same, return false
	// does not depend on base date
	bool isFuture(int year, int month, int day) const;

	// return the number of days from a given date to end of month
	// does not depend on base date
	// example: given date: November 10, 2001; return value will be 20
	int remainingDaysOfGivenMonth(int year, int month, int day) const;

	// return the number of days from a given date to given date's current year,
	// not counting the current month, that is, from the next month on
	// does not depend on base date
	// Examples: year 2001, month 12, return 0;
	// year 2001, month 11, return 31;
	int remainingDaysOfGivenYearFromNextMonthOn(int year, int month) const;

	// return the number of days from a given date to this date's current year's final day
	// example: given date: November 15, 2001; return value will be 15+31 = 46
	int remainingDaysofGivenYear(int year, int month, int day) const;

	// return the number of days 
	// example: base date: April 15, 2001; given year 2005.
	// this function will return the sum of number of days of year 2002, 2003, 2004,
	// that is, 365+365+366, that is, 1096
	long numberOfDaysOfFullYearsFromNextYearToYearBeforeGivenYear(int year) const;

	// example: base date: April 21, 2001; given date is December 27, 2001
	// return is 361
	int numberOfDaysOfGivenYearUntilGivenDate(int year, int month, int day) const;

public:

	VBDate(); // default constructor

	// return the weekday of a given date. sun=0, .. sat = 6
	// if date is invalid, the return value is -1
	int getWeekDay(const int year,const int month,const int day) const;

	//                                         jan fev mar apr may jun jul aug sep oct nov dec   
	static const int VBDaysInMonths[12]; // = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	// return true if hear is leap 
	// does not depend on base date
	bool leapYear (int year) const;

	// return the number of days of a given year, with attention to 
	// leap year. The return value is 365 or 366.
	// does not depend on base date
	int daysInAYear(int year) const;

	// return the number of days of a given month, 
	// with attention to leap year. 
	// if date is invalid return -1
	// does not depend on base date
	int daysInAMonth(int year, int month) const;

	// set date if given date is valid do nothing otherwise
	bool setDate(int year, int month, int day);
	bool setDate(VBDateTime dateTime);
	bool setDateYYYYMMDD(VBString date);

	// return the delta days between this and cal
	long deltaDays(const VBDate & cal) const;

	// Return a VBDate whose date is this VBDate added (or subtracted) by delta days.
	VBDate dateAfterDeltaDays(const long deltaDays) const;

	// get weekDay of this
	int getWeekDay() const;

	// return a string with base date in given format
	VBString getDate(int format=1) const;

	// Set this date to the day after.
	void tomorrow();

	// Set this date to the day before.
	void yesterday();

}; // end of VBDate

/////////////////////////////////////////////////////////////////////
//
// class VBDateTime
//
/////////////////////////////////////////////////////////////////////

/**********************************************************
struct tm 
{
	int tm_sec;     // seconds after the minute - [0,59] 
	int tm_min;     // minutes after the hour - [0,59] 
	int tm_hour;    // hours since midnight - [0,23] 
	int tm_mday;    // day of the month - [1,31] 
	int tm_mon;     // months since January - [0,11] 
	int tm_year;    // years since 1900 
	int tm_wday;    // days since Sunday - [0,6] 
	int tm_yday;    // days since January 1 - [0,365] 
	int tm_isdst;   // daylight savings time flag 
};
**********************************************************/

class VBDateTime
{
	tm m_tm; // standard lib's struct tm to store date and time

	// set this struct tm from time_t
	void setThisTM(time_t ltime);

	// return input with 2 digits
	VBString twoDigits(int i) const;

public:
	friend class VBDate; 

	VBDateTime(); // default constructor

	static const char *monthName[12];
	
	// return the struct tm from this
	operator struct tm () const {return m_tm;} ; 

	// return a VBDate object from this 
	operator VBDate () const; 

	// turn the this VBDateTime object to now (reading from the system)
	void setNow();

	// return system's date and time as string
	VBString getDateStr(int dateFormat=1) const;

	// return system's time as string
	VBString getTimeStr() const;

	// add or subtract time to this VBDateTime
	void addYear(int year);
	void addMonth(int month);
	void addDay(int day);
	void addHour(int hour);
	void addMinute(int minute);
	void addSecond(int second);


}; // end of VBDateTime

/////////////////////////////////////////////////////////////////////
//
// class VBDateTimeTimezone
//
/////////////////////////////////////////////////////////////////////

class VBDateTimeTimezone : public VBDateTime
{
	int m_timeRelativeToGreenwitch;
public:
	VBDateTimeTimezone() {};

}; // end of VBDateTimeTimezone


/////////////////////////////////////////////////////////////////////
//
// class VBFileDirectory
//
/////////////////////////////////////////////////////////////////////

class VBFileDirectory 
{
#ifdef _MSC_VER // Visual C++ only
	WIN32_FIND_DATA m_f;
	HANDLE m_h;
#else // unix only
	DIR *m_dp;
#endif
	VBString m_path, m_dirPath;
	bool m_endOfList;
	bool m_isDir;

	bool p_goodName(VBString fileaName);
	void p_isDirectory (VBString file); 
	VBString p_get(); 
	VBString p_lastFileName(VBString path);
public:
	VBFileDirectory (); // default constructor     
	bool isDir ();
	VBString getPath ();       
	VBString getNext();
	VBString getFirst(const char *path);        
	bool eolist();
};


/////////////////////////////////////////////////////////////////////
//
// global functions
//
/////////////////////////////////////////////////////////////////////
char *strsrep(char *str,const char *search,const char *replace);
char *strinc(const char *str1,char *str2);
char *strschg(char *str,const char *find,const char *replace);
char *strdel(const char *substr,char *str);
char *strins(const char *instr,char *str,size_vbs st_pos);

} // close namespace sbVB

using namespace sbVB;


#endif // _VBLIB


