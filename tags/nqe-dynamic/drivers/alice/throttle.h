//Code to run the throttle actuator]

#ifndef THROTTLE_H
#define THROTTLE_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>


#define THROTTLE_IDLE 0
#define THROTTLE_MAX  1
#define THROTTLE_OUTPUT_LENGTH 6
#define THROTTLE_INPUT_LENGTH 4
#define START_CHARACTER 'B'
#define THROTTLE_START  0
#define APPS1 1
#define APPS2 2
#define APPS3 3
#define THROTTLE_END 4
#define END_CHARACTER 'E'

#define THROTTLE_GET_POSITION 'S'

//values to output for idle
#define APPS1_IDLE 206
#define APPS2_IDLE 76
#define APPS3_IDLE 48

//values to output for full throttle
#define APPS1_FT 51 
#define APPS2_FT 193
#define APPS3_FT 165

//ranges, inclusive of idle and full throttle
#define APPS1_RANGE 155
#define APPS2_RANGE 117
#define APPS3_RANGE 117

#define THROTTLE_SERIAL

#ifndef THROTTLE_SERIAL
#define THROTTLE_SDS
#endif

int throttle_open(int port);
void simulator_throttle_open(int port);

int throttle_close(void);
int throttle_calibrate(void);
int throttle_close(void);
int throttle_ok(void);
int throttle_setposition(double position);
double throttle_getposition(void);
int throttle_pause(void);
int throttle_zero(void);



#endif //_THROTTLE_H




