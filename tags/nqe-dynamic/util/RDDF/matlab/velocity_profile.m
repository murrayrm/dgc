function path_new = velocity_profile(path)
% function path_new = velocity_profile(path)
%
%12-30-2004     Ben Pickett, created
%
% function that takes a path and modifies the velocity profile to satisfy
% the dynamics of the vehicle (uses simple model with maximum +tangential,
% -tangential, and radial accelerations).  The function also calculates the
% magnitudes of the accelerations and calculates where in the path the
% vehicle will switch between positive, zero, and negative accelerations.
% The function returns path_new that is the same except for new points
% interspersed to specify changes in acceleration.  The acceleration is
% piecewise continuous and the velocity is continuous throughout the path.
% 
%
%   path = [x1, y1, slx1, sly1, a0x1, a0y1;
%           x2, y2, slx2, sly2, a0x2, a0y2;
%           ..  ..  ...  ...  ...  ...;
%           xN, yN, slxN, slyN, a0xN, a0yN]
% The speeds specified in path are RDDF speed limits.  The accelerations
% for straight segments should be zero.
%
%   path_new = [x1, y1, vx1, vy1, ax1, ay1;
%               x2, y2, vx2, vy2, ax2, ay2;
%               ..  ..  ...  ...  ...  ...;
%               xK, yK, vxK, vyK, axK, ayK]
% where K >= N.
%
% Accelerations specified in path_new are tangential for straight path
% segments and centripetal for curved path segments (constant speed around
% corners).
%  

%% constants
%% note: we may want to make these global in final
%% implementation, since they need to be inherited by functions invoked by
%% velocity_profile().

%global constants
lat_max = 0.4*9.8; %maximum lateral acceleration (m/s^2)
accel_max = 0.3*9.8; %maximum positive (tangential) acceleration
decel_max = 0.6*9.8; %maximum negative (tangential) acceleration


[N,M] = size(path); % N = number of points in the path

%update rddf specified speed limits to take into account
%maximum lateral acceleration for curved segments.
path = update_curve_velocity(path, lat_max)
%make velocities feasible
velocity = update_velocity(path, accel_max, decel_max)
%plot_path(path);
i = 1; %index in path vector
path_new = [];
for i = 1:N-1
    sl0 = mag(path(i,3:4));    
    v0 = velocity(i);
    v2 = velocity(i+1);
    
    a0 = mag(path(i,5:6));
    
    uv = path(i,3:4)/mag(path(i,3:4));%unit vector in direction of vel
    
    if (a0 == 0) %straight path segment (they all have zero accel initially)
        %length of path segment
        length = mag(path(i+1,1:2)-path(i,1:2));
        if v0 == sl0 & v2 == sl0 %zero acceleration throughout
            new_segment = path(i,:); %constant velocity, zero acceleration
        else if (v0 < v2) %speeding up
                 %distance to change speed
                ds = (v2^2-v0^2)/(2*accel_max);
            else %slowing down (measured from pt2 towards pt1)
                ds = (v0^2-v2^2)/(2*decel_max);
            end
            if ds == length %constant acceleration throughout
                new_segment = single_segment(path(i:i+1,:),velocity(i:i+1),accel_max,decel_max);
            else %non-constant acceleration throughout
                %up: how much distance to spend speeding up from v0 to sl
                up = (sl0^2-v0^2)/(2*accel_max);
                %dn: how much distance before slowing down from sl to v2
                dn = length-(sl0^2-v2^2)/(2*decel_max);
                if up >= dn %flat,dn or up,flat
                    new_segment = double_segment_flat(path(i:i+1,:),velocity(i:i+1),accel_max,decel_max);
                else if up == 0 | dn == length
                        new_segment = double_segment_peak(path(i:i+1,:),velocity(i:i+1),accel_max,decel_max);
                    else %0 < up < dn < length
                        new_segment = triple_segment(path(i:i+1,:),velocity(i:i+1),accel_max,decel_max);
                    end
                end
            end
            path_new = [path_new;new_segment]; %add new segment to end of path
        end
    else %curved path segment
        ua = path(i,5:6)/mag(path(i,5:6));%unit vector in direction of accel
        %must modify acceleration to match the velocity profile velocity,
        %since acceleration is prescribed assuming maximum speed (sl)
        %path_new = [path_new;path(i,1:2), uv*v0, ua*a0*(v0/sl0)^2];
        path_new = [path_new;path(i,1:2), uv*velocity(i), ua*a0]; %velocity specified by profile
    end
end
%for last path segment, we are just going to be going in a straight line
%and, I will assume we continue in a straight line at the last point
%NOTE: velocity(N) should not be zero for this to work.
if (path(N-1,5:6) == [0,0]) %straight path segment
    %note: yes, i mean -uv for direction of acceleration, since want
    %to slow down in a straight line.
    path_new = [path_new;path(N,1:2),uv*velocity(N), -uv*decel_max];
else %path ends on a curve, I will just continue the same curve
    path_new = [path_new;path(N,1:2),0,0,0,0]; %not sure what to do for this yet.
end
%{
%debug (plot the path)
figure(3);
dist(1) = 0;
for i = 2:N
    dist(i) = 0;
    for j = 2:i
        dist(i) = dist(i) + mag(path(j,1:2)-path(j-1,1:2));
    end
end
disp(dist);
figure(3);
plot(dist,velocity,'ro-');
%/debug
%}
%% end %%


function new_segment = single_segment(path_segment, velocity, accel_max, decel_max)
%function new_segment = single_segment(path_segment, velocity, accel_max, decel_max)
%
%12-30-2004     Ben Pickett, created
%
%Specify a constant acceleration for a single straight path segment.
%Accelerations specified in origional path segment are arbitrary.
%
%velocity = [v0,v1]
%
%path_segment = [x0,y0,vx0,vy0,0,0;
%                x1,y1,vx1,vy1,0,0];
%
%new_segment  = [x0,y0,vx0,vy0,ax0,ay0;
%
uv = path_segment(1,3:4)/mag(path_segment(1,3:4)); %unit vector along path

new_segment = path_segment;

new_segment(1,3:4) = uv*velocity(1); %velocity has constant direction
if velocity(1) < velocity(2) %speeding up
    new_segment(1,5:6) = uv*accel_max;
else %velocity(1) > velocity(2) (slowing down)
    new_segment(1,5:6) = -uv*decel_max;
end
%{
disp('single segment path');
disp(path_segment(1,1:2));
%}

%% end %%


function new_segment = double_segment_flat(path_segment, velocity, accel_max, decel_max)
%function new_segment = double_segment_flat(path_segment, velocity, accel_max, decel_max)
%
%12-30-2004     Ben Pickett, created
%
%function to specify accelerations for two curved path segments joined by a
%a straight path segment with one segment of zero acceleration and one
%segment of non-zero acceleration
%
%velocity = [v0,v1]
%
%path_segment = [x0,y0,vx0,vy0,ax0,ay0;
%                x2,y2,vx2,vy2,ax2,ay2];
%new_segment  = [x0,y0,vx0,vy0,ax0,ay0;
%                x1,y1,vx1,vy1,ax1,ay1];
%
%note: accel_max and decel_max should both be non-negative.  decel_max
%taken to be in the negative direction (opposing velocity).

uv = path_segment(1,3:4)/mag(path_segment(1,3:4)); %unit vector along path
sl0= mag(path_segment(1,3:4)) %speed limit imposed by RDDF
v0 = velocity(1); %speed imposed by feasibility check
v2 = velocity(2); %for end of first curve and begining of second curve
length = mag(path_segment(1,1:2)-path_segment(2,1:2)); %length of straight segment
up = (sl0^2-v0^2)/(2*accel_max); %distance along path to maximum speed (sl0)
dn = length-(sl0^2-v2^2)/(2*decel_max); %distance before beginning deceleration

%{
%debug
disp('double segment path');
disp(path_segment(1,1:2));
%}
if up ~= 0 & dn ~= length
    disp('double segment (flat) path not feasible');
    disp(path_segment(1,1:2));
end
%end debug

new_segment = path_segment(1,:); %copy first row (first point)
if up == 0 %start at maxmimum speed
    new_segment(1,3:4) = uv*v0;
    new_segment(1,5:6) = [0,0]; %constant velocity (at maximum velocity)
    %now still maximum speed, begin deceleration
    new_segment = [new_segment; path_segment(1,1:2)+uv*dn,uv*sl0,-uv*decel_max];
else %dn == 0 (end at maximum speed)
    new_segment(1,3:4) = uv*v0;
    new_segment(1,5:6) = uv*accel_max; %constant acceleration
    %now at max velocity, begin zero acceleration
    new_segment = [new_segment; path_segment(1,1:2)+uv*up,uv*sl0,0,0];
end
%% end %%



function new_segment = double_segment_peak(path_segment, velocity, accel_max, decel_max)
%function new_segment = double_segment_peak(path_segment, velocity, accel_max, decel_max)
%
%12-30-2004     Ben Pickett, created
%
%function to specify accelerations for two curved path segments joined by a
%a straight path segment with one segment of +accelration and one segment
%of -acceleration (all non-zero acceleration).  Initial acclerations
%specified are arbitrary.
%
%velocity = [v0,v1]
%contains the desired velocity for initial point and final point of path
%segment.
%
%path_segment = [x0,y0,vx0,vy0,ax0,ay0;
%                x2,y2,vx2,vy2,ax2,ay2];
%Contains the initial point and final point, velocity is the RDDF imposed
%speed limit.
%
%new_segment  = [x0,y0,vx0,vy0,ax0,ay0;
%                x1,y1,vx1,vy1,ax1,ay1];
%
%note: accel_max and decel_max should both be non-negative.  decel_max
%taken to be in the negative direction (opposing velocity).

uv = path_segment(1,3:4)/mag(path_segment(1,3:4)); %unit vector along path
v0 = velocity(1); %speed imposed by feasibility check
v2 = velocity(2); %for end of first curve and begining of second curve
length = mag(path_segment(1,1:2)-path_segment(2,1:2)); %length of straight segment

%copy initial point, +accel
new_segment = [path_segment(1,1:2), uv*v0, uv*accel_max];

%distance along segment where we switch from + to - acceleration
ds = (v2^2-v0^2+2*decel_max)/(2*accel_max+2*decel_max);
%velocity at the point the acceleration switches
v1 = sqrt(v0^2 + 2*accel_max*ds);

%create point acceleration switches
new_segment = [new_segment; path_segment(1,1:2) + uv*ds, uv*v1, -uv*decel_max];
%% end %%



function new_segment = triple_segment(path_segment, velocity, accel_max, decel_max)
%function new_segment = triple_segment(path_segment,velocity, accel_max,
%decel_max)
%
%12-30-2004     Ben Pickett, created
%
%velocity = [v0,v1]
%velocity specified by feasibility check for end of
%first curve and beginning of second, respectively.
%
%path_segment = [x0,y0,vx0,vy0,ax0,ay0;
%                x2,y2,vx2,vy2,ax2,ay2];
%new_segment  = [x0,y0,vx0,vy0,ax0,ay0;
%                   ...
%                x2,y2,vx2,vy2,ax2,ay2];
%note: accel_max and decel_max should both be non-negative.  decel_max
%taken to be in the negative direction (opposing velocity).

uv = path_segment(1,3:4)/mag(path_segment(1,3:4)); %unit vector along path
sl0= mag(path_segment(1,3:4)); %speed limit imposed by RDDF
v0 = velocity(1); %speed imposed by feasibility check
v2 = velocity(2); %for end of first curve and begining of second curve
length = mag(path_segment(1,1:2)-path_segment(2,1:2)); %length of straight segment
up = (sl0^2-v0^2)/(2*accel_max); %distance along path to maximum speed (sl0)
dn = length - (sl0^2-v2^2)/(2*decel_max); %distance along path before slowing down

%
%debug
if up >= dn
    disp('triple segment path not feasible');
    disp(path_segment(1,1:2));
end
%end debug
%

new_segment = path_segment(1,1:2); %copy first point
new_segment(1,3:4) = uv*v0; %set velocity from feasibility check
new_segment(1,5:6) = uv*accel_max; %begin +acceleration


%insert middle segment (zero acceleration)
%and third segment (negative acceleration)
%finally, end point from origional path_segment
new_segment = [new_segment;
    path_segment(1,1:2) + uv*up, uv*sl0, 0, 0;
    path_segment(1,1:2) + uv*dn, uv*sl0, -uv*decel_max];
%% end %%


function m = mag(vec)
%function m = mag(vec)
%calculate the magnitude of a row vector
m = sqrt(vec*transpose(vec));
%% end %%