#include <math.h>
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

#include "specs.h"

extern "C" int snopt_(char *, int *, int *, int *,
											int *, int *, int *, int *, int *, 
											double *, char *,
											int (*)(int *, int *, int *, int *, double *x, double *, double *, int *, char *, int *, int *, int *, double *, int *, int),
											int (*)(int *, int *, double *, double *, double *, int *, char *, int *, int *, int *, double *, int *, int),
											double *, int *, 
											int *, double *, double *, char *, int *, 
											double *, double *, double *, int *, int *, 
											int *, int *, int *, int *, double *, 
											double *, char *, int *, int *, int *, double *,
											int *, char *, int *, int *, int *, double *,
											int *, int, int, int, int, int);
extern "C" int snseti_(char *,    int *, int *, int *, int *, char *, int *, int *, int *, double *, int *, int, int);
extern "C" int snsetr_(char *, double *, int *, int *, int *, char *, int *, int *, int *, double *, int *, int, int);
extern "C" int sninit_(int *, int *, char *, int *, int *, int *, double *, int *, int);
extern "C" int snset_ (char *,    			 int *, int *, int *, char *, int *, int *, int *, double *, int *, int, int);



#define IDX_T2           0
#define IDX_T4           1
#define IDX_B0           2
#define IDX_B2           3
#define IDX_B4           4
#define IDX_B6           5

#define T0 1.0
#define T2 pSolverState[IDX_T2]
#define T4 pSolverState[IDX_T4]
#define B0 pSolverState[IDX_B0]
#define B2 pSolverState[IDX_B2]
#define B4 pSolverState[IDX_B4]
#define B6 pSolverState[IDX_B6]



double CURVE_WIDTH, CURVE_TRANSITION, CURVE_WINDOW_MIN;

#define CURVE_TANH_OFFSET          ((CURVE_WIDTH - CURVE_TRANSITION)/2.0)

#define NUM_NUMER_VARS   2
#define NUM_DENOM_VARS   4
#define NUM_VARS         (NUM_NUMER_VARS + NUM_DENOM_VARS)
#define NUM_COLLOC       100
#define NUM_CONSTR       (NUM_COLLOC)
#define RANGE_X          (CURVE_WIDTH/2.0)

#define BELLNUM(xsq)    (T0 + (xsq)*(T2 + T4*(xsq)))
#define BELLDEN(xsq)    (B0 + (xsq)*(B2 + (xsq)*(B4 + B6*(xsq))))
#define DBELLNUM(x,xsq) ((x)*2.0* (T2 + 2.0*T4*(xsq)))
#define DBELLDEN(x,xsq) ((x)*2.0* (B2 + 2.0*(xsq)*(B4 + 1.5*B6*(xsq))))

int SNOPTfuncCost(int *mode, int *nnobj,
									double *pSolverState, double *fObj, double *gObj, int *nstate,
									char *cu, int *lencu, int *iu, int *leniu, double *ru, int *lenru, int cu_len)
{
	double dx = RANGE_X/(NUM_COLLOC-1);
	double x = 0.0;
	double val = 0.0;
	double dval[NUM_VARS];
	double num, den;
	double err;
	double tanhval;

	int i;

	for(i=0; i<NUM_VARS; i++)
		dval[i] = 0.0;

	for(i=0; i<NUM_COLLOC; i++)
	{
		num = BELLNUM(x*x);
		den = BELLDEN(x*x);

		tanhval = 0.5 * (
										 tanh((x+CURVE_TANH_OFFSET) / CURVE_TRANSITION*2.0 * atanh(1.0 - 2.0*CURVE_WINDOW_MIN) ) -
										 tanh((x-CURVE_TANH_OFFSET) / CURVE_TRANSITION*2.0 * atanh(1.0 - 2.0*CURVE_WINDOW_MIN) )
										);

		err = num/den - tanhval;
		val          += err*err;
		dval[IDX_T2] += 2.0*err * x*x/den;
		dval[IDX_T4] += 2.0*err * x*x*x*x/den;
		dval[IDX_B0] += 2.0*err * -num/den/den;
		dval[IDX_B2] += 2.0*err * (-num/den/den * x*x);
		dval[IDX_B4] += 2.0*err * (-num/den/den * x*x*x*x);
		dval[IDX_B6] += 2.0*err * (-num/den/den * x*x*x*x*x*x);

		x += dx;
	}

	if(*mode == 2 || *mode == 0)
		*fObj = val;
// 	if(*mode == 2 || *mode == 1)
// 		memcpy(gObj, dval, (*nnobj)*sizeof(gObj[0]));

	return 0;
}

int SNOPTfuncConstr(int *mode, int *nncon, int *nnjac, 
										int *nejac, double *pSolverState, double *fCon, double *gCon, int *nstate,
										char *cu, int *lencu, int *iu, int *leniu, double *ru, int *lenru, int cu_len)
{
	double dx = RANGE_X/(NUM_COLLOC-1);
	double x = 0.0;
	double constr[NUM_CONSTR];
	int i;

	double num,den, dnum,dden;
	for(i=0; i<NUM_COLLOC-1; i++)
	{
		x += dx;
		num = BELLNUM(x*x);
		den = BELLDEN(x*x);
		dnum= DBELLNUM(x,x*x);
		dden= DBELLDEN(x,x*x);
		constr[i] = (dnum*den - num*dden)/den/den;
// 		constr[i] = num/den;
	}

	constr[i] = num/den;



	if(*mode == 2 || *mode == 0)
		memcpy(fCon, constr, (*nncon)*sizeof(fCon[0]));
// 	if(*mode == 2 || *mode == 1)
// 		memcpy(gCon, pPlannerStage->m_pConstrGradData, (*nejac)*sizeof(gCon[0]));

	return 0;
}

int main(int argc, char *argv[])
{
	readspecs();

	if(argc != 2 ||
		 (strcmp(argv[1], "lat1") != 0 &&
		  strcmp(argv[1], "lat2") != 0 &&
		  strcmp(argv[1], "lat3") != 0 &&
		  strcmp(argv[1], "long") != 0) )
	{
		cerr << "Usage: ./fittanh [lat[123] | long]" << endl;
		return 0;
	}

	if(strcmp(argv[1], "lat1") == 0)
	{
		CURVE_WIDTH      = MAPSAMPLE_WIDTH1;
		CURVE_TRANSITION = MAPSAMPLE_WIDTH_TRANSITION1;
	}
	else if(strcmp(argv[1], "lat2") == 0)
	{
		CURVE_WIDTH      = MAPSAMPLE_WIDTH2;
		CURVE_TRANSITION = MAPSAMPLE_WIDTH_TRANSITION2;
	}
	else if(strcmp(argv[1], "lat3") == 0)
	{
		CURVE_WIDTH      = MAPSAMPLE_WIDTH3;
		CURVE_TRANSITION = MAPSAMPLE_WIDTH_TRANSITION3;
	}
	else
	{
		CURVE_WIDTH      = MAPSAMPLE_LENGTH;
		CURVE_TRANSITION = MAPSAMPLE_LENGTH_TRANSITION;
	}
	CURVE_WINDOW_MIN = MAPSAMPLE_WINDOW_MIN;

	int i;

	// Internal SNOPT crap
	char start[8];
	int SNOPTm, SNOPTn;
	int ne;
	int nName;
	int nnobj, nncon;
	int nnjac;
	int iobj;
	double objadd;
	char prob[8];
	double *SNOPTmatrix;
	int *ha, *ka;
	double* solverLowerBounds;
	double* solverUpperBounds;
	int *hs;
	double *pSolverState;
	double *pi;
	double *rc;
	int inform;
	int mincw, miniw, minrw, ns;
	int ninf;
	double obj, sinf;
	int lencw, leniw, lenrw;
	char *cw;
	int *iw;
	double *rw;

	strncpy(start, "Cold    ", 8);
	SNOPTn = NUM_VARS;
	SNOPTm = 1 + NUM_CONSTR;
	ne = SNOPTm*SNOPTn;
	nName = 1;

	nncon = NUM_CONSTR;
	nnobj = SNOPTn;
	nnjac = SNOPTn;
	iobj = 0;
	objadd = 0.0;
	strncpy(prob,  "planner ", 8);

	SNOPTmatrix = new double[ne];
	memset(SNOPTmatrix, 0, ne*sizeof(SNOPTmatrix[0]));

	ha = new int[ne];
	ka = new int[SNOPTn+1];
	for(i=0; i<ne; i++)
	{
		ha[i] = (i % SNOPTm) + 1;
	}

	for(i=0; i<=SNOPTn; i++)
	{
		ka[i] = (SNOPTm) * i + 1;
	}

	solverLowerBounds = new double[SNOPTn + SNOPTm];
	solverUpperBounds = new double[SNOPTn + SNOPTm];

	hs = new int[SNOPTn+SNOPTm];
	pSolverState = new double[SNOPTn+SNOPTm];
	memset(hs, 0, (SNOPTn+SNOPTm)*sizeof(hs[0]));
	memset(pSolverState + SNOPTn, 0, (SNOPTm)*sizeof(pSolverState[0]));


	pi = new double[SNOPTm];
	rc = new double[SNOPTn+SNOPTm];
	memset(pi, 0, SNOPTm*sizeof(pi[0]));
	memset(rc, 0, (SNOPTn+SNOPTm)*sizeof(rc[0]));

	inform = 0;


	lencw = 10000;
	leniw = 2000*(SNOPTm+SNOPTn);
	lenrw = 4000*(SNOPTm+SNOPTn);
	cw = new char[8*lencw];	
	iw = new int[leniw];
	rw = new double[lenrw];


	T2 =    -0.22049 ;
	T4 =     0.01160 ;
  B0 =     0.99917 ;
  B2 =     0.80632 ;
	B4 =     0.21031 ;
  B6 =     0.13073 ;

	for(i=0; i<SNOPTn; i++)
	{
		solverLowerBounds[i] = -1e20;
		solverUpperBounds[i] =  1e20;
	}
	for(i=0; i<SNOPTm; i++)
	{
		solverLowerBounds[i+SNOPTn] = -1e20;
		solverUpperBounds[i+SNOPTn] =  0e20;
	}

// 	solverLowerBounds[SNOPTm+SNOPTn-3] = 0.0;  // diff at end

 	solverLowerBounds[SNOPTm+SNOPTn-2] = CURVE_WINDOW_MIN;  // val at end
 	solverUpperBounds[SNOPTm+SNOPTn-2] = CURVE_WINDOW_MIN;  // val at end

	solverUpperBounds[SNOPTm+SNOPTn-1] = 1e20; // cost


	int isumm = 0;
	int PRINT_LEVEL = 6;

	sninit_(&PRINT_LEVEL, &isumm,
					cw, &lencw, iw, &leniw, rw, &lenrw, 8);

	char *pStr;
	pStr = "Derivative level          0";
	snset_(pStr, &PRINT_LEVEL, &isumm, &inform, cw, &lencw, iw, &leniw, rw, &lenrw, strlen(pStr), 8);
	pStr = "Verify level              0";
	snset_(pStr, &PRINT_LEVEL, &isumm, &inform, cw, &lencw, iw, &leniw, rw, &lenrw, strlen(pStr), 8);
	pStr = "Elastic weight 1e0";
	snset_(pStr, &PRINT_LEVEL, &isumm, &inform, cw, &lencw, iw, &leniw, rw, &lenrw, strlen(pStr), 8);


	snopt_(start, &SNOPTm, &SNOPTn, &ne, &nName, &nncon, &nnobj, &nnjac, &iobj, &objadd,
				 prob, SNOPTfuncConstr, SNOPTfuncCost,
				 SNOPTmatrix, ha, ka, solverLowerBounds, solverUpperBounds, NULL, hs, pSolverState, pi, rc,
				 &inform, &mincw, &miniw, &minrw, &ns, &ninf, &sinf, &obj,
				 NULL, &lencw, NULL, NULL, rw, &lenrw,
				 cw, &lencw, iw, &leniw, rw, &lenrw,
				 8, 8, 8, 8, 8);

	cout << "plot [" << 0 << ":" << CURVE_WIDTH/2.0 << "] (" << T0 << "+ x*x*(" << T2 << "+ " << T4 << "*(x*x)))/(" <<
		B0 << "+ (x*x)*(" << B2 << " + (x*x)*(" << B4 << "+" << B6 << "*(x*x)))), 0.5 * ( tanh((x+ " << CURVE_TANH_OFFSET <<
		") / " << CURVE_TRANSITION/2.0 << "* atanh(1.0 - 2.0*" << CURVE_WINDOW_MIN << ") ) - tanh((x-" << CURVE_TANH_OFFSET <<
		") / " << CURVE_TRANSITION/2.0 << "* atanh(1.0 - 2.0*" << CURVE_WINDOW_MIN << ")))" << endl; 

	delete[] SNOPTmatrix;
	delete[] ha;
	delete[] ka;
	delete[] hs;
	delete[] pSolverState;
	delete[] pi;
	delete[] rc;
	delete[] solverLowerBounds;
	delete[] solverUpperBounds;
	delete[] cw;
	delete[] iw;
	delete[] rw;
}

