*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*
*     File  sn85Hess.f
*
*     s8BFGS   s8H0     s8HDum   s8Hmap   s8Hx     s8Hmod   s8Rupd
*
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine s8BFGS( lenr, n, nFull, ydx, dxHdx,
     $                   r, Hdx, y, Rdx )

      implicit           double precision (a-h,o-z)
      double precision   r(lenr)
      double precision   Hdx(n), y(n), Rdx(n)

*     ==================================================================
*     s8BFGS  applies the quasi-Newton update to the Cholesky factor R
*     of the approximate Hessian.
*
*     On entry:
*     r      contains the first n rows and columns of an
*            upper-triangular matrix R such that R'R = H.
*
*     Hdx    contains H times the vector x2 - x1.
*            It is overwritten. 
*
*     y      contains the first  n  components of the difference g2 - g.
*
*     Rdx    is the vector such that  R'*(Rdx) = Hdx.
*            It is overwritten.
*
*     w      is a work vector.
*
*     On exit:
*
*     30 Dec 1991: First version based on  Npsol  routine npbfgs.
*     03 Jul 1998: Current version.
*     ==================================================================
      parameter         (zero = 0.0d+0, one = 1.0d+0)
*     ------------------------------------------------------------------
      tolz   = zero
      rtydx  = sqrt( ydx )

*     Normalize  Rdx.
*     Scale Hdx.

      c      = one/sqrt(dxHdx)
      call dscal ( n, (-c), Hdx, 1 )
      call dscal ( n,   c , Rdx, 1 )
 
*     ------------------------------------------------------------------
*     Apply the update to R. 
*     Set w for the update  R + v w'.
*     w is held in Hdx,  v  is held in Rdx.
*     ------------------------------------------------------------------
      call daxpy ( n, (one/rtydx), y, 1, Hdx, 1 )

      if (n .lt. nFull) then
*        --------------------------------------------
*        The leading nxn section of R is updated.
*        --------------------------------------------
         lastnz = n + 1
      else
*        --------------------------------------------
*        The full R is updated.
*        Find the last nonzero in v.
*        --------------------------------------------
         do 300, k = n, 1, -1
            lastnz = k
            if (abs( Rdx(lastnz) ) .gt. tolz) go to 500
  300    continue
      end if
*     -------------------------------------------------------
*     Restore  R + v w'  to triangular form  (overwriting v).
*     -------------------------------------------------------
  500 call s6Rmod( n, lenr, r, Rdx, Hdx, lastnz, one, tolz )

*     end of s8BFGS
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine s8H0  ( Htype, nnL, H0pre,
     $                   iw, leniw, rw, lenrw )

      implicit           double precision (a-h,o-z)
      integer            Htype
      integer            iw(leniw)
      double precision   rw(lenrw)

*     ==================================================================
*     s8H0    initializes the BFGS approximate Hessian.
*
*     s8H0   calls one of the Hessian routines s9LMH0, s9FMH0, s9SDH0
*     according to the value of the option lvlHes.
*     Each of these routines defines a particular form of the Hessian.
*     At the moment the options are:
*        lvlHes = 1   Limited-Memory (LM) BFGS Hessian  (the default).
*        lvlHes = 2   Full-Memory    (FM) BFGS Hessian.
*        lvlHes = 3   second derivative or fd Hessian plus LM updates.
*                     (coming real soon now).
*
*     On entry, the value of Htype is as follows:
*
*       Htype 
*       -----
*        -1      H undefined.
*         0      H is an approx. Hessian of the form defined by  lvlHes.
*         1      H is a diagonal matrix.
*         2      H is an identity matrix.
*
*     19 Jul 1995: First version of s8H0   written by PEG.
*     12 Jan 1996: Full memory Hessian option added.
*     24 Jul 1998: Current version.
*     ==================================================================
      parameter         (nQNmod = 201)
*     ------------------------------------------------------------------
      lvlHes    = iw( 53)

      if (Htype .lt. 0) then
         iw(nQNmod) = 0
      end if

      if      (lvlHes .eq. 1) then
         mQNmod    = iw( 66)
         lH0       = iw(381)
         lgdSav    = iw(382)
         lHdSav    = iw(383)
         lydx      = iw(384)
         ldxHdx    = iw(385)

         call s9LMH0( Htype, nnL, mQNmod, iw(nQNmod),
     $                H0pre,
     $                rw(lH0), rw(lgdSav), rw(lHdSav),
     $                rw(lydx), rw(ldxHdx) )

      else if (lvlHes .eq. 2) then
         lH        = iw(371)
         lenH      = iw(372)

         call s9FMH0( Htype, lenH, nnL, iw(nQNmod), H0pre, rw(lH) )

*     else if (lvlHes .eq. 3) then
*        call s9SDH0( )

      end if

*     end of s8H0
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine s8Hmap( nnL, nnCon, minrw, iw, leniw )

      implicit           double precision (a-h,o-z)
      integer            iw(leniw)

*     =================================================================
*     s8Hmap is called by s8Mem to set up the memory allocation for the
*     approximate Hessian.
*
*     It calls one of the Hessian routines s9LMH, s9FMH, s9SDH, ... 
*     according to the value of the option lvlHes.
*     Each of these routines defines a particular form of the Hessian.
*     At the moment the options are:
*        lvlHes = 1   Limited-Memory (LM) BFGS Hessian  (the default).
*        lvlHes = 2   Full-Memory    (FM) BFGS Hessian.
*        lvlHes = 3   second derivative or fd Hessian plus LM updates.
*                     (coming real soon now).
*
*     13 Dec 1995: First version of s8Hmap.
*     12 Jan 1996: Full memory Hessian option added.
*     16 May 1998: Current version.
*     ==================================================================
      lvlHes    = iw( 53)

      if      (lvlHes .eq. 1) then
         call s9LMmp( nnL, minrw, iw, leniw )

      else if (lvlHes .eq. 2) then
         call s9FMmp( nnL, minrw, iw, leniw )

*     else if (lvlHes .eq. 3) then
*        call s9SDmp( nnL, minrw, iw, leniw )
      end if

*     end of s8Hmap
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine s8HDum( )

*     ==================================================================
*     s8HDum  is the dummy argument for s8Hx.
*
*     18 Dec 1995: Current version.
*     ==================================================================
*     Relax

*     end of s8HDum
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine s8Hx  ( s8HDum, minimz, n, nnL, ne, nka, a, ha, ka,
     $                   x, Hx, nState, 
     $                   cu, lencu, iu, leniu, ru, lenru, 
     $                   cw, lencw, iw, leniw, rw, lenrw )

      implicit           double precision (a-h,o-z)
      external           s8HDum
      integer            ha(ne)
      integer            ka(nka)
      double precision   a(ne)
      double precision   Hx(nnL), x(nnL)

      character*8        cu(lencu), cw(lencw)
      integer            iu(leniu), iw(leniw)
      double precision   ru(lenru), rw(lenrw)

*     ==================================================================
*     s8Hx  multiplies the QP Hessian  H by the vector  x.
*     It is used to define Hx for the QP subproblem.
*
*     On entry:
*        nState  = 0  => a normal call for H*x.
*        nState  = 1  => the first entry for a given QP.
*        nState ge 2  => last call for a given QP. nState = 2+ierror.
*
*     On exit:
*        nState lt 0   the user wants to stop.
*
*     s8Hx calls one of the Hessian routines s9LMH, s9FMH, s9SDH, ... 
*     according to the value of the option lvlHes.
*     Each of these routines defines a particular form of the Hessian.
*     At the moment the options are:
*        lvlHes = 1   Limited-Memory (LM) BFGS Hessian  (the default).
*        lvlHes = 2   Full-Memory    (FM) BFGS Hessian.
*        lvlHes = 3   second derivative or fd Hessian plus LM updates.
*                     (coming real soon now).
*
*     30 Dec 1991: First version of s8Hx.
*     12 Jan 1996: Full memory Hessian option added.
*     03 Jul 1998: Current version.
*     ==================================================================
      parameter         (nQNmod = 201)
*     ------------------------------------------------------------------
      lvlHes    = iw( 53)

      if      (lvlHes .eq. 1) then
         mQNmod    = iw( 66)

         lH0       = iw(381)
         lgdSav    = iw(382)
         lHdSav    = iw(383)
         lydx      = iw(384)
         ldxHdx    = iw(385)

         call s9LMHx( nnL, x, Hx, mQNmod, iw(nQNmod),
     $                rw(lH0), rw(lgdSav), rw(lHdSav),
     $                rw(lydx), rw(ldxHdx) )

      else if (lvlHes .eq. 2) then
         lH        = iw(371)
         lenH      = iw(372)

         call s9FMHx( nnL, lenH, rw(lH), x, Hx )

*     else if (lvlHes .eq. 3) then
*        call s9SDH ( )

      end if

      sgnObj = minimz
      if (minimz .lt. 0) call dscal ( nnL, sgnObj, Hx, 1 )

*     end of s8Hx
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine s8Hmod( Update, Htype, nnL,
     $                   H0pre, H0scal, ydx, dxHdx, Hdx, y,
     $                   iw, leniw, rw, lenrw )

      implicit           double precision (a-h,o-z)
      integer            Update, Htype
      integer            iw(leniw)
      double precision   rw(lenrw)
      double precision   Hdx(nnL), y(nnL)

*     ==================================================================
*     s8Hmod  applies the pair of vectors that define the BFGS update
*     or self-scaled BFGS update.
*
*     On entry:
*
*     Hdx   contains H times the difference x2 - x1.
*
*     y     contains the gradient difference g2 - g.
*
*     s8Hmod calls one of the Hessian routines s9LMH, s9FMH, s9SDH, ... 
*     according to the value of the option lvlHes.
*     Each of these routines defines a particular form of the Hessian.
*     At the moment the options are:
*        lvlHes = 1   Limited-Memory (LM) BFGS Hessian  (the default).
*        lvlHes = 2   Full-Memory    (FM) BFGS Hessian.
*        lvlHes = 3   second derivative or fd Hessian plus LM updates.
*                     (coming real soon now).
*
*     19 Jul 1995: First version of s8Hmod written by PEG.
*     12 Jan 1996: Full-memory Hessian option added.
*     24 Jul 1998: Current version.
*     ==================================================================
      parameter         (nQNmod = 201)
*     ------------------------------------------------------------------
      lvlHes    = iw( 53)

      if      (lvlHes .eq. 1) then
         mQNmod    = iw( 66)
         lH0       = iw(381)
         lgdSav    = iw(382)
         lHdSav    = iw(383)
         lydx      = iw(384)
         ldxHdx    = iw(385)

         call s9LMup( Update, Htype, nnL, mQNmod, iw(nQNmod),
     $                H0pre, H0scal,
     $                ydx, dxHdx, Hdx, y, rw(lH0),
     $                rw(lgdSav), rw(lHdSav), rw(lydx), rw(ldxHdx) )

      else if (lvlHes .eq. 2) then
         mQNmod    = iw( 66)
         lH        = iw(371)
         lenH      = iw(372)

         call s9FMup( Update, Htype, lenH, nnL, mQNmod, iw(nQNmod),
     $                H0pre, H0scal,
     $                ydx, dxHdx, Hdx, y, rw(lH) )

*     else if (lvlHes .eq. 3) then
*        call s9SDH ( )
 
      end if

*     end of s8Hmod
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine s8Rupd( Update, lenr, m, n, nBS, nnL, nS,
     $                   H0scal, ydx, dxHdx,
     $                   ne, nka, a, ha, ka,
     $                   kBS, gdif, Hdx, r, w, y, y2,
     $                   iw, leniw, rw, lenrw )

      implicit           double precision(a-h,o-z)
      integer            ha(ne)
      integer            ka(nka), kBS(nBS)
      integer            iw(leniw)
      double precision   rw(lenrw)
      double precision   a(ne)
      double precision   r(lenr)
      double precision   gdif(nnL), Hdx(nnL)
      double precision   y(nBS), y2(nBS), w(nBS)
      integer            Update

*     ==================================================================
*     s8Rupd updates the Cholesky factor of the reduced Hessian.
*     
*     04 Aug 1995: First version based on NPSOL routine npupdt.
*     21 Jul 1998: Current version.
*     ==================================================================
      parameter         (zero = 0.0d+0, one = 1.0d+0)
*     ------------------------------------------------------------------
      eps0      = rw(  2)
     
*     ------------------------
*     Find Z'*gdif and  Z'Hdx.
*     ------------------------
      do 100, k = 1, nBS
         j      = kBS(k)
         if (j .le. nnL) then
            y (k) = gdif(j)
            y2(k) = Hdx (j)
         else
            y (k) = zero
            y2(k) = zero
         end if
  100 continue

      call s5setp( m, piNorm, y, w, iw, leniw, rw, lenrw )

      if (nS .gt. 0) then
         call s2Bprd( 'Transpose', eps0, n, nS, kBS(m+1),
     $                ne, nka, a, ha, ka,
     $                (-one), w, m, one, y(m+1), nS ) 
      end if

      call s5setp( m, piNorm, y2, w, iw, leniw, rw, lenrw )

      if (nS .gt. 0) then
         call s2Bprd( 'Transpose', eps0, n, nS, kBS(m+1),
     $                ne, nka, a, ha, ka,
     $                (-one), w, m, one, y2(m+1), nS ) 
      end if

      if (Update .gt. 1) then
*        --------------------
*        Self-scaled BFGS.
*        --------------------
         rH0scl    = sqrt( H0scal )
         lenr0     = nS*(nS + 1)/2
         call dscal( lenr0, rH0scl, r , 1 )                
      end if

*     ---------------------------------------------------------
*     Apply the BFGS update to the first nS columns of R.
*     y = Z'gdif,  y2 = Rdx  and  w = Z'Hdx.
*     ---------------------------------------------------------
      call dcopy ( nS, y2(m+1), 1, w(m+1), 1 )
      call s6Rsol( 'Rt', lenr, nS, r, y2(m+1) )
      call s8BFGS( lenr, nS, nnL, ydx, dxHdx,
     $             r, w(m+1), y(m+1), y2(m+1) ) 

*     end of s8Rupd
      end

