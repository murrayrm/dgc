#include "DBS.hh"

using namespace std;


extern int NOSKYNET;                /**< for command line argument */

DBS::DBS(int skynetKey, bool run_active_in_background, bool show_info_plots, bool show_hist_plots) : CSkynetContainer(MODDBS, skynetKey), CTimberClient(timber_types::DBS)
{
  m_dbs_state = new DBS_state;
  m_plot_state = new Plot_state;

  m_plot_state->history_data = new double[hist::LAST * HISTORY_LENGTH];
  for (int i = 0; i < hist::LAST*HISTORY_LENGTH; i++)
    m_plot_state->history_data[i] = 0;

  do_current_info_plot = show_info_plots;
  do_history_plot = show_hist_plots;

  first_ignore = true;

  // initialize plots (whether or not they're currently enabled)
  current_info_plot = gnuplot_init();
  m_plot_state->current_info_ranges = new double [DBS_NUM_VALUES];
  for (int i = 0; i < DBS_NUM_VALUES; i++)
    m_plot_state->current_info_ranges[i] = -CURRENT_INFO_RANGE;
  m_plot_state->current_info_ranges[0] = CURRENT_INFO_RANGE;

  history_plot = gnuplot_init();
  m_plot_state->history_ranges = new double [HISTORY_LENGTH];
  for (int i = 0; i < HISTORY_LENGTH; i++)
    m_plot_state->history_ranges[i] = HISTORY_RANGE_MIN;
  m_plot_state->history_ranges[0] = HISTORY_RANGE_MAX;
  

  // get all offsets within VehicleState and save them in our array
  //  vs_table[0] = 0x123; // not used
  //  vs_table[asf::Timestamp] = 0x; // not used; can't take fft of timestamp
  vs_table[asf::Northing] = &VehicleState::Northing;
  vs_table[asf::Easting] = &VehicleState::Easting;
  vs_table[asf::Altitude] = &VehicleState::Altitude;
  vs_table[asf::Vel_N] = &VehicleState::Vel_N;
  vs_table[asf::Vel_E] = &VehicleState::Vel_E;
  vs_table[asf::Vel_D] = &VehicleState::Vel_D;
  vs_table[asf::Acc_N] = &VehicleState::Acc_N;
  vs_table[asf::Acc_E] = &VehicleState::Acc_E;
  vs_table[asf::Acc_D] = &VehicleState::Acc_D;
  vs_table[asf::Roll] = &VehicleState::Roll;
  vs_table[asf::Pitch] = &VehicleState::Pitch;
  vs_table[asf::Yaw] = &VehicleState::Yaw;
  vs_table[asf::RollRate] = &VehicleState::RollRate;
  vs_table[asf::PitchRate] = &VehicleState::PitchRate;
  vs_table[asf::YawRate] = &VehicleState::YawRate;
  vs_table[asf::RollAcc] = &VehicleState::RollAcc;
  vs_table[asf::PitchAcc] = &VehicleState::PitchAcc;
  vs_table[asf::YawAcc] = &VehicleState::YawAcc;

  STATE_INFO(STATE_FIELD_MAP)
  STATE_INFO(STATE_FIELD_FFT_MAP)
  STATE_INFO(STATE_MULT_MAP)
  STATE_INFO(STATE_FFT_MULT_MAP)

  HIST_INFO(HIST_FIELD_MAP)
  HIST_INFO(HIST_MULT_MAP)

  // initialize FFT plannner
  fft_in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  fft_out = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  plan = fftw_plan_dft_1d(DBS_NUM_VALUES, fft_in, fft_out, FFTW_FORWARD, FFTW_ESTIMATE);

  // create mutexes
  DGCcreateMutex(&do_plot_mux);
  DGCcreateMutex(&m_conc_copy_mux);
  m_conc_copy_initialized = false;
  DGClockMutex(&m_conc_copy_mux);

  /** start the active loop, if applicable */
  if (run_active_in_background)
    {
      cout << "Starting DBSActiveLoop in a new thread." << endl;
      DGCstartMemberFunctionThread(this, &DBS::DBSActiveLoop);
    }
  else
    cout << "Skipped starting DBSActiveLoop." << endl;
  
  cout << "Please see the DBS README for information on how to interpret the plots" << endl;
  cout << "Finished DBS constructor." << endl;
}



DBS::~DBS()
{
  cout << "DBS destructor has been called." << endl;
  delete [] m_dbs_state;
  delete [] m_plot_state;
  DGCdeleteMutex(&m_conc_copy_mux);
  DGCdeleteMutex(&do_plot_mux);
  fftw_destroy_plan(plan);
  fftw_free(fft_in);
  fftw_free(fft_out);
  gnuplot_close(current_info_plot);
  gnuplot_close(history_plot);
}



void DBS::fftcalculatorLastN()
{
  // make a new chunk of memory to use as a buffer for the last
  // N values
  VehicleState* buffer = new VehicleState[DBS_NUM_VALUES];  

  // get last DBS_NUM_VALUES state values and store them in our buffer,
  // using GetLastNValues(DBS_NUM_VALUES,destination
  GetLastNValues(DBS_NUM_VALUES,buffer);

  // do this:
  fftw_complex *in;
  fftw_complex *out;
  fftw_plan p;
  in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  out = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  
  // copy the pitch from our buffer to "in"
  int n;
  for(n=0;n<DBS_NUM_VALUES;n++){
    in[n][0] = buffer[n].Pitch;
  }
  
  // do the fft
  p = fftw_plan_dft_1d(DBS_NUM_VALUES, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(p);

  // do something with the fft results
  PrintDebug(in, out);
  PrintValueSum(out);

  // at end, free the memory
  fftw_destroy_plan(p);
  fftw_free(in); fftw_free(out);
  delete [] buffer; 
}



/*
void DBS::stats(VehicleState &means, VehicleState &vars) {
  double vR, sumR, mVR,r,sumVR,varR,vPR,sumPR,mVPR,pR,sumVPR,varPR,vRR,sumRR,mVRR,rR,sumVRR,varRR,sumP,mVP,p,sumVP,varP,vfDD,sumfDD,mVfDD,fDD,sumVfDD,varfDD,vsDD,sumsDD,mVsDD,sDD,sumVsDD,varsDD,maxAP,maxAR;
  int n;  
  
  sumPR=0;
  sumVPR=0; 
  sumR=0;
  sumVR=0;
  sumRR=0;
  sumVRR=0;
  sumP=0;
  sumVP=0;
  
  sumfDD=0;
  sumVfDD=0;
  sumsDD=0;
  sumVsDD=0;
  n=0;
 
   
  //MaxAP = 10.87;
  // MaxAR = 7.96;

   
     //if(fftAP<MaxAP && fftAR<MaxAR){
       //Hur sager jag till systemet att sakte ner
       //else
   //Hur sager jag till systemet att oka hastigheten
   
   //terrainclassification
   //while(n!= DBS_NUM_VALUES ){
     //if fftAP<0.6
	  //pavedroad
//if fftAP<0.6 && fftAP<5
//  cout<<dirtroad<<;
//  else
//    cout<<offroad<<;

 while(n!=DBS_NUM_VALUES){
   r = m_dbs_states[n].Roll;
   sumR = sumR + r ;
   n++;
 }
 means.Roll = sumR/DBS_NUM_VALUES; 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   r = m_dbs_states[n].Roll;
   sumVR  =sumVR + pow(r-means.Roll,2);
   n++;
 }
 vars.Roll =sumVR/DBS_NUM_VALUES;

//"MeanValue and Variance rollrate"
 n=0;
 while(n!=DBS_NUM_VALUES){
   rR = m_dbs_states[n].RollRate;
   sumRR = sumRR   + rR ;
   n++;
 }
 means.RollRate = sumRR  /DBS_NUM_VALUES; 
 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   rR = m_dbs_states[n].RollRate;
   sumVRR  =sumVRR + pow(r-mVRR,2);
   n++;
 }
 vars.RollRate = sumVRR/DBS_NUM_VALUES;

// pitchrate MeanValue and Variance pitchrate"
 n=0;
 while(n!=DBS_NUM_VALUES){
   pR = m_dbs_states[n].PitchRate;
   sumPR = sumPR + pR ;
   n++;
 }
 means.PitchRate = sumPR  /DBS_NUM_VALUES; 
 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   pR = m_dbs_states[n].PitchRate;
   sumVPR = sumVPR + pow(pR-means.PitchRate,2);
   n++;
 }
 vars.PitchRate = sumPR/DBS_NUM_VALUES;

 //MeanValue and Variance pitch
 n=0;
 while(n!=DBS_NUM_VALUES){
   p = m_dbs_states[n].Pitch;
   sumP = sumP + p ;
   n++;
 }
 means.Pitch = sumP/DBS_NUM_VALUES; 
 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   p = m_dbs_states[n].Pitch;
   sumVP  =sumVP + pow(p-means.Pitch,2);
   n++;
 }
 vars.Pitch = sumVP/DBS_NUM_VALUES;

// first derivates down, mean value and varianceMeanValue and Variance
 n=0;
 while(n!=DBS_NUM_VALUES){
   fDD = m_dbs_states[n].Vel_D;
   sumfDD = sumfDD + fDD;
   n++;
 }
 means.Vel_D = sumfDD  /DBS_NUM_VALUES; 
 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   fDD = m_dbs_states[n].Vel_D;
   sumVfDD  =sumVfDD+ pow(fDD-means.Vel_D,2);
   n++;
 }
 vars.Vel_D = sumVfDD/DBS_NUM_VALUES;

// second derivates down, meanvalue and variance 
 n=0;
 while(n!=DBS_NUM_VALUES){
   sDD = m_dbs_states[n].Acc_D;
   sumsDD = sumsDD + sDD;
   n++;
 }
 means.Acc_D = sumsDD/DBS_NUM_VALUES; 
 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   sDD = m_dbs_states[n].Acc_D;
   sumVsDD=sumVsDD+ pow(sDD-means.Acc_D,2);
   n++;
 }
 vars.Acc_D = sumVsDD/DBS_NUM_VALUES;

}
*/



void DBS::DBSActiveLoop()
{
  int socket = m_skynet.get_send_sock(SNtrajFspeedCapCmd);
  newSpeedCapCmd speed_cap_container;
  speed_cap_container.m_speedCapSndModule = TF_MOD_DBS;
  speed_cap_container.m_speedCapAction = add_max_speedcap;

  int counter = 0;
  // main loop
  while (true)
    {
      counter++;
      UpdateDBSState();

      UpdatePlotStateOne();
      UpdateConclusions();
      UpdatePlotStateTwo();

      /** Replot stuff */
      DGClockMutex(&do_plot_mux);
      bool do_cur_plot = do_current_info_plot;
      bool do_hist_plot = do_history_plot;
      DGCunlockMutex(&do_plot_mux);
      if (do_cur_plot)
	PlotCurrentInfo();
      if (do_hist_plot)
	PlotHistory();


      //       cout << setw(3) << counter << " (bumpiness, speed_cap) = (" << m_conc.bumpiness 
      // 	   << ", " << m_conc.speed_cap << ")";
      
      //       cout << "  " << m_dbs_state->fft_area[areas::zdot_1_20].area << endl;

      speed_cap_container.m_speedCapArgument = m_conc.fit_speed_cap;
      m_skynet.send_msg(socket, &speed_cap_container, sizeof(speed_cap_container), 0);

      Log();

      if (m_conc_copy_initialized)
	DGClockMutex(&m_conc_copy_mux);
      m_conc_copy = m_conc;
      m_conc_copy_initialized = true;
      DGCunlockMutex(&m_conc_copy_mux);
      
      usleep(1000000/FREQUENCY);
    }

  //  gnuplot_close(handle);  // deletes files in /tmp or /usr/tmp or something
}



void DBS::UpdateDBSState()
{
  /** get last DBS_NUM_VALUES state values and store them in our buffer,
   * using GetLastNValues(DBS_NUM_VALUES,destination) */
  GetLastNValues(DBS_NUM_VALUES, m_dbs_state->astate_buffer);

  /** store most recent speed for later use... */
  m_dbs_state->recent_speed = hypot(m_dbs_state->astate_buffer[DBS_NUM_VALUES-1].Vel_N,
				    m_dbs_state->astate_buffer[DBS_NUM_VALUES-1].Vel_E);

  /** take the FFT's of all variables we're interested in */
  for (int field = ASTATE_FIELD_FFT_START; field <= ASTATE_FIELD_FFT_END; field++)
    {
      for (int i = 0; i < DBS_NUM_VALUES; i++)
	fft_in[i][0] = m_dbs_state->astate_buffer[i].*(vs_table[field]);
      fftw_execute(plan);
      for (int i = 0; i < DBS_NUM_VALUES; i++)
 	(m_dbs_state->astate_fft)[field][i] = sqrt(fft_out[i][0]*fft_out[i][0]+fft_out[i][1]*fft_out[i][1]);
    }
    
  /** recalculate area variables */
  for (int area_index = 0; area_index < areas::LAST; area_index++)
    {
      m_dbs_state->fft_area[area_index].area = 0;
      for (int i = m_dbs_state->fft_area[area_index].start; i < m_dbs_state->fft_area[area_index].end; i++)
	m_dbs_state->fft_area[area_index].area += 
	  m_dbs_state->astate_fft[m_dbs_state->fft_area[area_index].field][i];
    }

  /** figure out if we're ignoring this last iteration because we were
   * travelling too slowly */
  m_dbs_state->ignore_this_iteration = 
    IGNORE_PAUSED_PERIODS && 
    m_dbs_state->recent_speed < PAUSE_SPEED;
  if (m_dbs_state->ignore_this_iteration)
    {
      if (first_ignore)
	{
	  first_ignore = false;
	  cout << "\nspeed is too low; ignoring this iteration.";
	}
      else
	cout << ".";
      cout.flush();
    }
  else
    first_ignore = true;
	  
  
  //   /** recalculate means and stddevs */
  // #define CALC_FFT_HIST(handle, variable, iterations) **backslash**
      // m_dbs_state->##handle##_mean = m_dbs_state->##handle##_stddev = 0; **backslash**
      // for(int i = iterations - 1; i > 0; i--) **backslash**
      // { m_dbs_state->##handle##_history[i] =  m_dbs_state->##handle##_history[i-1]; **backslash**
      // m_dbs_state->##handle##_mean += handle##_history[i]; } **backslash**
      // m_dbs_state->##handle##_history[0] = m_dbs_state->variable; **backslash**
      // m_dbs_state->##handle##_mean += handle##_history[0]; **backslash**
      // for(int i = 0 i < iterations; i++) **backslash**
      // { m_dbs_state->##handle##_stddev += pow((m_dbs_state->##handle##_history[i]- handle##_mean),2); }**backslash**
      // m_dbs_state->##handle##_stddev = pow(m_dbs_state->##handle##_stddev / iterations, .5);
      
      //   FFT_HIST_INFO(CALC_FFT_HIST)
}



void DBS::UpdatePlotStateOne()
{
  PushArea(areas::pitch_1_20, hist::pitch_1_20);
  PushArea(areas::pitchdd_1_20, hist::pitchdd_1_20);
  PushArea(areas::zdot_1_20, hist::zdot_1_20);
  PushArea(areas::zdot_10_20, hist::zdot_10_20);
  PushArea(areas::zdd_1_20, hist::zdd_1_20);
  PushSpeed(hist::speed);
}



void DBS::UpdatePlotStateTwo()
{
  // this is now done in UpdateConclusions
  //  PushDouble(hist::bumpiness, m_conc.bumpiness);
  PushDouble(hist::worst_bumpiness, m_conc.worst_bumpiness);
  PushDouble(hist::decaying_bumpiness, m_conc.decaying_bumpiness);
  PushDouble(hist::fit_bumpiness, m_conc.fit_bumpiness);
  PushDouble(hist::fit_inst_bumpiness, m_conc.fit_inst_bumpiness);
  PushDouble(hist::speed_cap, m_conc.speed_cap);
  PushDouble(hist::worst_speed_cap, m_conc.worst_speed_cap);
  PushDouble(hist::decaying_speed_cap, m_conc.decaying_speed_cap);
  PushDouble(hist::fit_speed_cap, m_conc.fit_speed_cap);
}



void DBS::PlotCurrentInfo()
{
  gnuplot_resetplot(current_info_plot);

  // plot a range box to make it not 'blink'
  gnuplot_setstyle(current_info_plot, "dots");
  gnuplot_plot_x(current_info_plot, m_plot_state->current_info_ranges, 2, "-");
  gnuplot_setstyle(current_info_plot, "lines");

  PlotAstateField(asf::Pitch);
  PlotAstateField(asf::Yaw);
  PlotFFT(asf::Pitch);
  PlotFFT(asf::Yaw);
}



void DBS::PlotHistory()
{
  gnuplot_resetplot(history_plot);

  // plot a range box to make it not 'blink'
  gnuplot_setstyle(history_plot, "dots");
  gnuplot_plot_x(history_plot, m_plot_state->history_ranges, 2, "-");
  gnuplot_setstyle(history_plot, "lines");
  
  PlotThisHist(hist::speed);
  PlotThisHist(hist::fit_bumpiness);
  //  PlotThisHist(hist::worst_bumpiness);
  //  PlotThisHist(hist::decaying_bumpiness);
  //  PlotThisHist(hist::speed_cap);
  //  PlotThisHist(hist::worst_speed_cap);
  PlotThisHist(hist::fit_speed_cap);
  //  PlotThisHist(hist::pitchdd_1_20);
  //  PlotThisHist(hist::zdd_1_20);
  //  PlotThisHist(hist::zdd_bump);
  //  PlotThisHist(hist::pitchdd_bump);
  PlotThisHist(hist::fit_inst_bumpiness);
  /*
  PlotThisHist(hist::coeff_zdd_1);
  PlotThisHist(hist::coeff_pitchdd_1);
  PlotThisHist(hist::coeff_zdd_4);
  PlotThisHist(hist::coeff_pitchdd_4);
  */
}



double DBS::Speedfn(double speed)
{
  // original speed fn
  // return 1.5658 * (.6 * 1/(1 + exp(- (speed - 8) / 2)) + .2);
  // speed fn obtained from actual stoddard wells run
  return 1.8944 * (.52 * 1/(1 + exp(- (speed - 5) / 2)) + .2);
}



double DBS::TimeWeightingFunction(double time_ago)
{
  double time_delay = 15;
  double timescale = 6;
  return 1.0 / (1.0 + exp((time_ago - time_delay) / timescale));
}



void DBS::UpdateConclusions()
{
  double speed = hypot(m_dbs_state->astate_buffer[DBS_NUM_VALUES-1].Vel_N,
		       m_dbs_state->astate_buffer[DBS_NUM_VALUES-1].Vel_E);

  /** get the instantaneous bumpiness (0-1) */
  m_conc.pitchdd_bump = fmin(1.0,
			     m_dbs_state->fft_area[areas::pitchdd_1_20].area / 7.5 / Speedfn(speed));
  m_conc.zdd_bump = fmin(1.0,
			 m_dbs_state->fft_area[areas::zdd_1_20].area / 3500.0 / Speedfn(speed));
  // calculate combined (pitchdd & zdd) instantaneous bumpiness measure
  m_conc.bumpiness = (m_conc.zdd_bump + m_conc.pitchdd_bump) / 2.0;

  /** Append the bumpiness to the history so that GetDecayingBumpiness has access to it */
  PushDouble(hist::bumpiness, m_conc.bumpiness);
  PushDouble(hist::zdd_bump, m_conc.zdd_bump);
  PushDouble(hist::pitchdd_bump, m_conc.pitchdd_bump);

  /** calculate a weighted average of last HISTORY_AVE_LENGTH values */
  m_conc.decaying_bumpiness = CalculateDecayingBumpiness();
  m_conc.fit_bumpiness = CalculateFitBumpiness(HISTORY_AVE_LENGTH);
  m_conc.fit_inst_bumpiness = CalculateFitBumpiness(HISTORY_INST_LENGTH, false);

  /*
  if (USE_SPEED_WEIGHTING == 1)
    {
      double speed_divisor = Speedfn(speed);
      m_conc.decaying_bumpiness /= speed_divisor;
      m_conc.fit_bumpiness /= speed_divisor;
      m_conc.fit_inst_bumpiness /= speed_divisor;
      m_conc.decaying_bumpiness = fmin(1.0, m_conc.decaying_bumpiness);
      m_conc.fit_bumpiness = fmin(1.0, m_conc.fit_bumpiness);
      m_conc.fit_inst_bumpiness = fmin(1.0, m_conc.fit_inst_bumpiness);
    }
  */

  /** worst_bumpiness over the last n seconds */
  double worst_bumpiness = m_conc.bumpiness; // worst is highest
  double last_n_seconds = 10;
  int first_i = (int)(-1.0 * last_n_seconds*FREQUENCY + (double)HISTORY_LENGTH - 1);
  if (first_i < 0)
    {
      first_i = 0;
      cerr << "history isn't long enough for that!!";
    }
  for (int i = HISTORY_LENGTH-1; i >= first_i; i--)
    worst_bumpiness = fmax(worst_bumpiness, m_plot_state->history_data[HIST_INDEX(hist::bumpiness,i)]);
  m_conc.worst_bumpiness = worst_bumpiness;


  /** Calculate all speed caps based on bumpinesses*/
  m_conc.speed_cap = BumpinesstoSpeed(m_conc.bumpiness);
  m_conc.worst_speed_cap = BumpinesstoSpeed(m_conc.worst_bumpiness);
  m_conc.decaying_speed_cap = BumpinesstoSpeed(m_conc.decaying_bumpiness);
  m_conc.fit_speed_cap = BumpinesstoSpeed(m_conc.fit_bumpiness);  // this is the one that people want
}



double DBS::GetInstBumpiness()
{
  DGClockMutex(&m_conc_copy_mux);
  double ret = m_conc_copy.bumpiness;
  DGCunlockMutex(&m_conc_copy_mux);
  return ret;
}



double DBS::GetDecayingBumpiness()
{
  DGClockMutex(&m_conc_copy_mux);
  double ret = m_conc_copy.decaying_bumpiness;
  DGCunlockMutex(&m_conc_copy_mux);
  return ret;
}



double DBS::GetSpeedCap()
{
  DGClockMutex(&m_conc_copy_mux);
  double ret = m_conc_copy.fit_speed_cap;
  DGCunlockMutex(&m_conc_copy_mux);
  return ret;
}



bool DBS::GetDoCurrentInfoPlot()
{
  DGClockMutex(&do_plot_mux);
  bool ret = do_current_info_plot;
  DGCunlockMutex(&do_plot_mux);
  return ret;
}



bool DBS::GetDoHistoryPlot()
{
  DGClockMutex(&do_plot_mux);
  bool ret = do_history_plot;
  DGCunlockMutex(&do_plot_mux);
  return ret;
}



void DBS::SetDoCurrentInfoPlot(bool new_val)
{
  DGClockMutex(&do_plot_mux);
  do_current_info_plot = new_val;
  DGCunlockMutex(&do_plot_mux);
}



void DBS::SetDoHistoryPlot(bool new_val)
{
  DGClockMutex(&do_plot_mux);
  do_history_plot = new_val;
  DGCunlockMutex(&do_plot_mux);
}



double DBS::BumpinesstoSpeed(double bp)
{
  // .1 -> 25
  // .35 -> 20
  // .5 -> 15
  // 1 -> 3
  double a, b;
  a = -25 * 22/28;
  b = 22;

  return a * bp + b;
}



double DBS::CalculateDecayingBumpiness()
{
  double norm_power = 5;
  double sum_weights = 0;
  double weighted_bumpiness = 0;
  double this_weight;

  for(int i = HISTORY_LENGTH - 1; i >= HISTORY_LENGTH - HISTORY_AVE_LENGTH; i--)
    {
      this_weight = TimeWeightingFunction((double)(HISTORY_LENGTH - i - 1)/FREQUENCY);
      weighted_bumpiness += this_weight
	* pow(m_plot_state->history_data[HIST_INDEX(hist::bumpiness, i)], norm_power);
      sum_weights += this_weight;
    }
  weighted_bumpiness = pow(weighted_bumpiness / sum_weights, 1.0/norm_power);
  return weighted_bumpiness;
}



double DBS::CalculateFitBumpiness(int history_ave_length, bool push_hists)
{
  double overall_bumpiness_scale = OVERALL_BUMP_SCALE;
  double
    norm4 = 4,
    coeff_zdd_1 = 0,
    coeff_pitchdd_1 = 0,
    coeff_zdd_4 = 0,
    coeff_pitchdd_4 = 0,
    this_weight,
    sum_weights_1 = 0,
    sum_weights_4 = 0,
    speed_divisor = 1;

  for(int i = HISTORY_LENGTH - 1; i >= HISTORY_LENGTH - history_ave_length; i--)
    {
      /** not sure the weighting thing will work for weights not = to 1... */
      this_weight = 1;
      sum_weights_1 += this_weight;
      sum_weights_4 += pow(this_weight, norm4);

      if (USE_SPEED_WEIGHTING == 1)
	speed_divisor = Speedfn(m_plot_state->history_data[HIST_INDEX(hist::speed, i)]);

      coeff_zdd_1     += this_weight * m_plot_state->history_data[HIST_INDEX(hist::zdd_1_20, i)] / speed_divisor;
      coeff_pitchdd_1 += this_weight * m_plot_state->history_data[HIST_INDEX(hist::pitchdd_1_20, i)] / speed_divisor;
      coeff_zdd_4     += pow(this_weight * m_plot_state->history_data[HIST_INDEX(hist::zdd_1_20, i)] / speed_divisor, norm4);
      coeff_pitchdd_4 += pow(this_weight * m_plot_state->history_data[HIST_INDEX(hist::pitchdd_1_20, i)] / speed_divisor, norm4);
    }

  coeff_zdd_1 = coeff_zdd_1 / sum_weights_1;
  coeff_pitchdd_1 = coeff_pitchdd_1 / sum_weights_1;
  coeff_zdd_4 = pow(coeff_zdd_4 / sum_weights_4, 1 / norm4);
  coeff_pitchdd_4 = pow(coeff_pitchdd_4 / sum_weights_4, 1 / norm4);

  double
    b1 = 0.0074762,
    b2 = 0.0867810,
    //    b3 = -0.0020295,
    b3 = 0, // :)
    b4 = 0.0438141;

  if (push_hists)
    {
      PushDouble(hist::coeff_zdd_1, coeff_zdd_1);
      PushDouble(hist::coeff_pitchdd_1, coeff_pitchdd_1);
      PushDouble(hist::coeff_zdd_4, coeff_zdd_4);
      PushDouble(hist::coeff_pitchdd_4, coeff_pitchdd_4);
    }
 
  double bump = b1 * coeff_zdd_1 + b2 * coeff_pitchdd_1 + b3 * coeff_zdd_4 + b4 * coeff_pitchdd_4;
  bump *= overall_bumpiness_scale;
  return fmin(1, fmax(0, bump));
}



void DBS::PlotAstateField(int field)
{
  double* buffer = new double[DBS_NUM_VALUES];
  for (int i = 0; i < DBS_NUM_VALUES; i++)
    buffer[i] = m_dbs_state->astate_buffer[i].*(vs_table[field]) * state_multiplier_map[field];
  string title_str = state_field_map[field];
  gnuplot_plot_x(current_info_plot, buffer, DBS_NUM_VALUES, (char*)title_str.c_str());
  delete [] buffer;
}



void DBS::PlotFFT(int field)
{
  double* buffer = new double[DBS_NUM_VALUES/2];
  for (int i = 0; i < DBS_NUM_VALUES/2; i++)
    buffer[i] = m_dbs_state->astate_fft[field][i] * state_fft_multiplier_map[field];
  string title_str = state_field_fft_map[field];
  buffer[0] = 0;  // don't plot the average
  gnuplot_plot_x(current_info_plot, buffer, DBS_NUM_VALUES/2, (char*)title_str.c_str());
  delete [] buffer;
}



void DBS::PlotThisHist(int hist_field)
{
  double* buffer = new double[HISTORY_LENGTH];
  for (int i = 0; i < HISTORY_LENGTH; i++)
    buffer[i] = hist_multiplier_map[hist_field] * m_plot_state->history_data[HIST_INDEX(hist_field, i)];
  gnuplot_plot_x(history_plot, buffer, HISTORY_LENGTH, (char*)hist_field_map[hist_field].c_str());
}



void DBS::PushArea(int hist_field, int area)
{
  if (!m_dbs_state->ignore_this_iteration)
    for (int i = 0; i < HISTORY_LENGTH - 1; i++)
      m_plot_state->history_data[HIST_INDEX(hist_field, i)] = 
	m_plot_state->history_data[HIST_INDEX(hist_field, i+1)];
  m_plot_state->history_data[HIST_INDEX(hist_field, HISTORY_LENGTH - 1)] = m_dbs_state->fft_area[area].area;
}



void DBS::PushSpeed(int hist_field)
{
  if (!m_dbs_state->ignore_this_iteration)
    for (int i = 0; i < HISTORY_LENGTH - 1; i++)
      m_plot_state->history_data[HIST_INDEX(hist_field, i)] = 
	m_plot_state->history_data[HIST_INDEX(hist_field, i+1)];
  m_plot_state->history_data[HIST_INDEX(hist_field, HISTORY_LENGTH - 1)] = 
    hypot(m_dbs_state->astate_buffer[DBS_NUM_VALUES-1].Vel_E, m_dbs_state->astate_buffer[DBS_NUM_VALUES-1].Vel_N);
}



void DBS::PushDouble(int hist_field, double value)
{
  if (!m_dbs_state->ignore_this_iteration)
    for (int i = 0; i < HISTORY_LENGTH - 1; i++)
      m_plot_state->history_data[HIST_INDEX(hist_field, i)] = 
	m_plot_state->history_data[HIST_INDEX(hist_field, i+1)];
  m_plot_state->history_data[HIST_INDEX(hist_field, HISTORY_LENGTH - 1)] = value;
}



void DBS::PrintValue(fftw_complex* fft) 
{ int n;  
 for(n=0;n<DBS_NUM_VALUES;n++){
      cout << "fft[n][0]* = " << fft[n][0] << endl;
    }    
}



void DBS::PrintDebug(fftw_complex* in, fftw_complex* out)
{
  cout << setw(3) << "number" 
       << setw(13) << "in(real)" 
       << setw(13) << "in(imag)" 
       << setw(13) << "out(real)"
       << setw(13) << "out(imag)"
       << endl;
  for (int i = 0; i < DBS_NUM_VALUES; i++)
    {
      cout << setw(3) << i
	   << setw(13) << in[i][0]
	   << setw(13) << in[i][1]
	   << setw(13) << out[i][0]
	   << setw(13) << out[i][1]
	   << endl;
    }
}



void DBS::PrintValueSum(fftw_complex* fftA)  
{
  int n;
  double sumfft = 0;
  for(n=0;n<DBS_NUM_VALUES;n++)
    {
      double fft =fftA[n][0];
      sumfft =sumfft +fft;
    }
  cout<<"sumfft"<<sumfft<<endl; 
}




/** 
 * does all the timber logging
 */
void DBS::Log()
{
  logging_enabled = getLoggingEnabled();
  if (logging_enabled)
    {
      logging_location = getLogDir();
      string file_location = logging_location + (string)LOG_FILE;
      if (checkNewDirAndReset())
	{
	  // need to (re)open file
	  if (logging_file.is_open())
	    logging_file.close();
	  logging_file.open(file_location.c_str(), ios::app | ios::out);

	  if (!logging_file.is_open())
	    cerr << "Unable to open logging file \"" << file_location << "\", not logging!" << endl;
	  else
	    {
	      // output a header row describing what stuff is
	      logging_file << "%Timestamp(1)";
	  
	      // then the conclusions struct
	      logging_file << "\t" << "Speed(2)";
	      logging_file << "\t" << "filler(3)";
	      logging_file << "\t" << "filler(4)";
	      logging_file << "\t" << "filler(5)";
	      logging_file << "\t" << "filler(6)";
	      logging_file << "\t" << "filler(7)";
	      // and finally all the fft's we took
	      int counter = 8;
	      int points_in_fft = DBS_NUM_VALUES / 2;
	      for (int fft = ASTATE_FIELD_FFT_START; fft <= ASTATE_FIELD_FFT_END; fft++)
		for (int i = 0; i < points_in_fft; i++)
		  {
		    logging_file << "\t" << "fft[" << fft << "][" << i << "](" << counter << ")";
		    counter++;
		  }

	      logging_file << "\t" << "m_conc.bumpiness(" << counter++ << ")"
			   << "\t" << "m_conc.worst_bumpiness(" << counter++ << ")"
			   << "\t" << "m_conc.decaying_bumpiness(" << counter++ << ")"
			   << "\t" << "m_conc.speed_cap(" << counter++ << ")"
			   << "\t" << "m_conc.worst_speed_cap(" << counter++ << ")"
			   << "\t" << "m_conc.decaying_speed_cap(" << counter++ << ")";
	      
	      logging_file << endl;

	    }
	}
      if (!logging_file.is_open())
	cerr << "Unable to open logging file \"" << file_location << "\", not logging!" << endl;
      else
	{
	  // finally... actually time to log some data!
	  
	  // first the timestamp from the most recent state
#warning "is this the most recent one?"
	  logging_file << m_dbs_state->astate_buffer[DBS_NUM_VALUES-1].Timestamp;
	  
	  // then the conclusions struct
	  logging_file << "\t" << sqrt(pow(m_dbs_state->astate_buffer[DBS_NUM_VALUES-1].Vel_N, 2)+pow(m_dbs_state->astate_buffer[DBS_NUM_VALUES-1].Vel_E, 2));
	  logging_file << "\t" << 0;
	  logging_file << "\t" << 0;
	  logging_file << "\t" << 0;
	  logging_file << "\t" << 0;
	  logging_file << "\t" << 0;



	  // and finally all the fft's we took
	  int points_in_fft = DBS_NUM_VALUES / 2;
	  for (int fft = ASTATE_FIELD_FFT_START; fft <= ASTATE_FIELD_FFT_END; fft++)
	    for (int i = 0; i < points_in_fft; i++)
	      logging_file << "\t" << m_dbs_state->astate_fft[fft][i];

	  logging_file << "\t" << m_conc.bumpiness 
		       << "\t" << m_conc.worst_bumpiness
		       << "\t" << m_conc.decaying_bumpiness
		       << "\t" << m_conc.speed_cap
		       << "\t" << m_conc.worst_speed_cap
		       << "\t" << m_conc.decaying_speed_cap;

	  logging_file << endl;
	}
    }
  else
    {
      // logging is not enabled... close the file if it wasn't already
      if (logging_file.is_open())
	{
	  logging_file.flush();
	  logging_file.close();
	}
    }
}



/*
int DBS::GetRoadType()
{
  double pitch[DBS_NUM_VALUES];
  double roll[DBS_NUM_VALUES];
  double zdot[DBS_NUM_VALUES];
  roadtype=-1;
  pitcharea=0;
  pitchhighestValue=0;
  // make a new chunk of memory to use as a buffer for the last
  // N values
  VehicleState* buffer = new VehicleState[DBS_NUM_VALUES];  

  // get last DBS_NUM_VALUES state values and store them in our buffer,
  // using GetLastNValues(DBS_NUM_VALUES,destination
  GetLastNValues(DBS_NUM_VALUES,buffer);

  // do this:
  fftw_complex *in;
  fftw_complex *out;
  fftw_plan fft;
  in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  out = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  
  // copy the pitch from our buffer to "in"
  int n;
  for(n=0;n<DBS_NUM_VALUES;n++)
    {
      in[n][0] = buffer[n].Pitch;
      in[n][1] = 0;
    }
  
  // do the fft
  fft = fftw_plan_dft_1d(DBS_NUM_VALUES, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(fft);

  // do something with the fft results
  PrintDebug(in, out);
  PrintValueSum(out);
  
  for( int n=1;n<DBS_NUM_VALUES;n++)
    { 
      pitch[n] = sqrt(out[n][0]*out[n][0]+ out[n][1]*out[n][1]);
    }
  pitch[0]=0;
  for( int n=0;n<DBS_NUM_VALUES;n++)
    {
      pitcharea += pitch[n];
      //cout << "n: " << n << " abs: " << sqrt(out[n][0]*out[n][0]+ out[n][1]*out[n][1]) << " real: " << out[n][0] << "complex: " << out[n][1] << endl;
      if (pitch[n] > pitchhighestValue)
	{
	  pitchhighestValue = pitch[n];
	}
    }
  pitcharea3= 0;
  for( int n=0;n<3;n++)
    {
      pitcharea3 += pitch[n]; 
    }
  pitch10=0;
  for(int n=10;n<DBS_NUM_VALUES/2;n++)
    {
      pitch10 += pitch[n];
    }
  
for(n=0;n<DBS_NUM_VALUES;n++)
    {
      in[n][0] = buffer[n].Roll;
    }
  
  // do the fft
  fft = fftw_plan_dft_1d(DBS_NUM_VALUES, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(fft);

  for( int n=0;n<DBS_NUM_VALUES;n++)
    { 
      roll[n] = sqrt(out[n][0]*out[n][0]+ out[n][1]*out[n][1]);
    }

  // do something with the fft results
  //PrintDebug(in, out);
  //PrintValueSum(out);
  rollarea=0;
  for( int n=0;n<DBS_NUM_VALUES;n++)
    {
      rollarea += roll[n];
      if (roll[n] > rollhighestValue)
	{
	  rollhighestValue = roll[n];
	}
    }
  rollarea5to8= 0;
  for( int n=4;n<7;n++)
    {
      rollarea5to8 += roll[n]; 
    }

  for(n=0;n<DBS_NUM_VALUES;n++)
    {
      in[n][0] = buffer[n].Vel_D;
    }
  
  // do the fft
  fft = fftw_plan_dft_1d(DBS_NUM_VALUES, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(fft);

  for( int n=0;n<DBS_NUM_VALUES;n++)
    { 
      zdot[n] = sqrt(out[n][0]*out[n][0]+ out[n][1]*out[n][1]);
    }

  Zdotarea=0;

  for(n=0;n<DBS_NUM_VALUES;n++)
    {
      Zdotarea += zdot[n];
    }

  for(n=0;n<DBS_NUM_VALUES;n++)
    {
      meanspeed += sqrt(buffer[n].Vel_N*buffer[n].Vel_N + buffer[n].Vel_E*buffer[n].Vel_E);
    }
  meanspeed = meanspeed/DBS_NUM_VALUES;
  cout << "pitcharea is: " << pitcharea << " pitchhigestValue is: " << pitchhighestValue << endl;
  if(pitcharea<.5) //fix me
    {
      roadtype = 0;
    }
else  if(pitchhighestValue >1)
    {
      roadtype=3;
    }
  else 
    {
      roadtype = -1;
  }

// at end, free the memory
  fftw_destroy_plan(fft);
  fftw_free(in); fftw_free(out);
  delete [] buffer;
  return roadtype; 
}  
*/
