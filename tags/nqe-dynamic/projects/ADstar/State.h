#ifndef __STATE_H__
#define __STATE_H__

#define INFINITE 1E9

#include <stdlib.h>
#include <iostream>
using namespace std;

class State
{

 private:

  struct key //Used to prioritize the entries in the queues in the main algorith
  {
    double k1, k2;
    
    key(double first, double second);
    
    double getK1();
    
    double getK2();
  
    void setK1(double newK1);

    void setK2(double newK2);

    bool operator < (key other); //This operator compares the priority of two key objects (returns true if *this has lower priority than other)
    
    ~key();
  };

  double northing, easting; // UTM position variables
  
  double g;  //g-value of the state
  
  double rhs; //rhs value of the state
  
  bool alreadyVisited; //Has this state been visited by UpdateStateObject() yet?

  key* state_key;
  
 public:

  State();

  State(double initNorthing, double initEasting, double gVal, double rhsVal, double key1, double key2); 

  State(double initNorthing, double initEasting, double gVal, double rhsVal, key* k = NULL);
  
  State(double initNorthing, double initEasting, key* k = NULL);
  
  State(State* s);
    
  State* operator = (State* s);

  bool operator < (State* s);
  
  void setNorthing(double newX);
    
  void setEasting(double newY);

  void setg(double newG);

  void setrhs(double newRHS);

  void setAlreadyVisited(bool tf);

  void setKey(key* k);

  void setKey(double key1, double key2);

  key* getKey();

  const double getg() const;
  
  const double getrhs() const;
    
  const double getNorthing() const;

  const double getEasting() const;

  const bool getAlreadyVisited() const;

  void print();

  ~State();
};

#endif //__STATE_H__
