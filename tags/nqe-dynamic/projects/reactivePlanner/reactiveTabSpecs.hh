#define TABNAME reactive

// fields: type, name, default value, widgetType, xLabel, yLabel, xVal, yVal
// everything is TAB-RELATIVE. Thus input is FROM the client INTO the tab
#define TABINPUTS(_) \
  _(int, numDeltas,   0, Label, 0, 0, 1, 0)	\
	_(double,  cumulativeDeltaSize, 0., Label, 0, 1, 1, 1) \
	_(int, numTrajsSent,   0, Label, 0, 3, 1,3) \
	_(unsigned long long,  runtimeUs, 0, Label, 0, 2, 1, 2)
#define TABOUTPUTS(_) \
/*  _(int, SAMPLE_OUTPUT_INT, -1, Entry, 0 , 3, 1,3  )*/

//#undef TABNAME
