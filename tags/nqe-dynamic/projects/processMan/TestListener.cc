/**
 * TestListener.cc
 * A server to help debug the RingMonitor class. 
 * Listens to udp messages and prints em.
 * Revision History:
 * 11/08/2005 hbarnor created
 * $Id$
 */

#include "RingMonitor.hh"

int main(int argc, char **argv)
{
  int sockfd;
  struct sockaddr_in servaddr;
  struct sockaddr_in cliaddr;
  
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  
  bzero(&servaddr, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servaddr.sin_port = htons(WHOIS_PORT);
  
  bind(sockfd, (struct sockaddr *) &servaddr,sizeof(servaddr));
  socklen_t len;
  struct pmRmMesg r1;
  while(true)
    {
      recvfrom(sockfd, &r1, sizeof(r1), 0, (struct sockaddr *) &cliaddr, &len);
      char str[INET_ADDRSTRLEN];
      const char * ptr;
      ptr = inet_ntop(r1.addr.sin_family, &(r1.addr.sin_addr), str, sizeof(str));
      cout << "Message received is: " << r1.mesgType << " from " << str << endl;
    }
}
