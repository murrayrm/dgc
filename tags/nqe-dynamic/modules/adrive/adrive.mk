ADRIVE_PATH = $(DGC)/modules/adrive

ADRIVE_DEPEND_LIBS =    $(SDSLIB) \
                        $(SKYNETLIB) \
                        $(SPARROWLIB) \
                        $(ALICELIB) \
                        $(MODULEHELPERSLIB) \
                        $(TIMBERLIB)

ADRIVE_DEPEND_SOURCES = $(ADRIVE_PATH)/actuators.c \
                        $(ADRIVE_PATH)/adrive.c \
                        $(ADRIVE_PATH)/askynet_monitor.cc \
                        $(ADRIVE_PATH)/sn_send_commandline.cc \
                        $(ADRIVE_PATH)/AdriveTimberClient.cc \
                        $(ADRIVE_PATH)/adriveMain.c \
                        $(ADRIVE_PATH)/ATimberBox.cc \
			$(ADRIVE_PATH)/addisp.dd

ADRIVE_DEPEND = $(ADRIVE_DEPEND_LIBS) $(ADRIVE_DEPEND_SOURCES)
