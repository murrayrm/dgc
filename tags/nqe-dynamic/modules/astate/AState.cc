/* AState.cc - new vehicle state estimator
 *
 * JML 09 Jun 05
 *
 * */

#include "AState.hh"

AState::AState(int skynetKey, astateOpts inputAstateOpts)
  : CSkynetContainer(SNastate, skynetKey), CTimberClient(timber_types::astate), CSuperConClient("AST")
{
  // Set internal options.
  _astateOpts = inputAstateOpts;

  snKey = skynetKey;

  quitPressed = 0;

  //Values relevent to sparrow:
  imuCount = 0;
  gpsCount = 0;
  obdCount = 0;
  novCount = 0;
  
  gpsValid = 0;
  gpsValid = 0;

  timberEnabled = 0;

  gpsNorth = 0;
  gpsEast = 0;
  gpsHeight = 0;
  gpsFOM = 0;
  gpsDOP = 0;
  gpsSats = 0;

  novHeight = 0;
  novNorth = 0;
  novEast = 0;
  novNorthStd = 0;
  novEastStd = 0;
  novHgtStd = 0;

  obdVel = 0;
  obdSteer = 0;
  obdBrake = 0;

  trackErr = 0;
  yawErr = 0;


  stateJump = JUMP_NONE;

  imuDead = 1;
  gpsDead = 1;
  novDead = 1;


  //THIS IS A BUNCH OF CRAP THAT SHOULD PROBABLY GO SOMEWHERE ELSE

  
  
  pError = 0.001;

  stateMode = PREINIT;

  obdVelScaleFactor = 1.0;
  obdSteerScaleFactor = 1.0;


  time_t t = time(NULL);
  tm *local;
  local = localtime(&t);

  // Set up files for raw logging.
  if (_astateOpts.logRaw || _astateOpts.debug) {
    mkdir("/tmp/logs/astate_raw/", S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IXOTH);
  }
   
  if (_astateOpts.logRaw == 1) {
    sprintf(imuLogFile, "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d_IMUdbg.raw",
	    local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	    local->tm_hour, local->tm_min, local->tm_sec);

    sprintf(gpsLogFile, "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d_GPS.raw",
	    local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	    local->tm_hour, local->tm_min, local->tm_sec);

    sprintf(obdLogFile, "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d_OBD.raw",
	    local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	    local->tm_hour, local->tm_min, local->tm_sec);

    sprintf(novLogFile, "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d_NOV.raw",
	    local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	    local->tm_hour, local->tm_min, local->tm_sec);

    imuLogStream.open(imuLogFile, fstream::out | fstream::binary);
    gpsLogStream.open(gpsLogFile, fstream::out | fstream::binary);
    obdLogStream.open(obdLogFile, fstream::out | fstream::binary);
    novLogStream.open(novLogFile, fstream::out | fstream::binary);
  }

  if (_astateOpts.debug == 1) {
    sprintf(debugFile, "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d_debug.raw",
	    local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	    local->tm_hour, local->tm_min, local->tm_sec);
    debugStream.open(debugFile, fstream::out);
  }
  
  if (_astateOpts.useAutotune == 1) {
    autotuneStream.open("autotune.dat", fstream::out);
  }


  // Set up files for raw replay.

  if (_astateOpts.useReplay == 1) {
    if (_astateOpts.oldLog) {
      sprintf(imuReplayFile, "%s_IMU.raw", _astateOpts.replayFile);
    } else {
      sprintf(imuReplayFile, "%s_IMUdbg.raw", _astateOpts.replayFile);
    }

    sprintf(gpsReplayFile, "%s_GPS.raw", _astateOpts.replayFile);
    sprintf(obdReplayFile, "%s_OBD.raw", _astateOpts.replayFile);
    sprintf(novReplayFile, "%s_NOV.raw", _astateOpts.replayFile);
    imuReplayStream.open(imuReplayFile, fstream::in | fstream::binary);
    gpsReplayStream.open(gpsReplayFile, fstream::in | fstream::binary);
    obdReplayStream.open(obdReplayFile, fstream::in | fstream::binary);
    novReplayStream.open(novReplayFile, fstream::in | fstream::binary);

    if (!imuReplayStream) {
      cerr << "Problems opening file.  File may be old, try using --old" << endl;
      quitPressed = 1;
      return;
    }

  }

  //Find startting time;
  DGCgettime(startTime);



  //Initialize EVERYTHING to be zeroed for good measure.
  _vehicleState.Timestamp = startTime;
  _vehicleState.Northing = 0;
  _vehicleState.Easting = 0;
  _vehicleState.Altitude = 0;
  _vehicleState.Vel_N = 0;
  _vehicleState.Vel_E = 0;
  _vehicleState.Vel_D = 0;
  _vehicleState.Acc_N = 0;
  _vehicleState.Acc_E = 0;
  _vehicleState.Acc_D = 0;
  _vehicleState.Roll = 0;
  _vehicleState.Pitch = 0;
  _vehicleState.Yaw = 0;
  _vehicleState.RollAcc = 0;
  _vehicleState.PitchAcc = 0;
  _vehicleState.YawAcc = 0;

  //Initialize metastate struct
  _metaState.gpsEnabled = 0;
  _metaState.imuEnabled = 0;
  _metaState.obdEnabled = 0;
  _metaState.novEnabled = 0;
  _metaState.kfEnabled = 0;

  _metaState.gpsActive = 0;
  _metaState.imuActive = 0;
  _metaState.obdActive = 0;
  _metaState.gpsPvtValid = 0;
  _metaState.extJumpFlag = 0;


  //Create mutexes:
  DGCcreateMutex(&m_VehicleStateMutex);
  DGCcreateMutex(&m_MetaStateMutex);
  DGCcreateMutex(&m_GPSDataMutex);
  DGCcreateMutex(&m_IMUDataMutex);
  DGCcreateMutex(&m_OBDDataMutex);
  DGCcreateMutex(&m_NovDataMutex);
  DGCcreateMutex(&m_HeartbeatMutex);
  DGCcreateMutex(&m_jumpMutex);

  DGCcreateCondition(&newData);
  DGCcreateCondition(&gpsBufferFree);
  DGCcreateCondition(&imuBufferFree);
  DGCcreateCondition(&obdBufferFree);
  DGCcreateCondition(&novBufferFree);
  
  DGCSetConditionTrue(gpsBufferFree);
  DGCSetConditionTrue(imuBufferFree);
  DGCSetConditionTrue(obdBufferFree);
  DGCSetConditionTrue(novBufferFree);

  //Set up socket for skynet broadcasting:
  broadcastStateSock = m_skynet.get_send_sock(SNstate);
  if(broadcastStateSock < 0) {
    cerr << "AState::AState(): skynet get_send_sock returned error" << endl;
  }


  superconSock = m_skynet.listen(SNsuperconAstateCmd, ALLMODULES);
  if(superconSock < 0) {
    cerr << "AState::AState(): skynet listen returned error" << endl;
  }


  if (_astateOpts.timberPlayback == 1) {
    cout << "Starting timber playback..." << endl;
    DGCstartMemberFunctionThread(this, &AState::playback);
  } else {

    //Initialize sensors:

    imuBufferLastInd = 0;
    gpsBufferLastInd = 0;
    obdBufferLastInd = 0;
    novBufferLastInd = 0;
    imuBufferReadInd = 0;
    gpsBufferReadInd = 0;
    obdBufferReadInd = 0;
    novBufferReadInd = 0;

    if(_astateOpts.useIMU == 0) {
      DGClockMutex(&m_MetaStateMutex);     
      _metaState.imuEnabled = 0;
      DGCunlockMutex(&m_MetaStateMutex);     
    } else {
      imuInit(); 	   // This will set imuEnabled
    }

    if (_metaState.imuEnabled == 1) {
            DGCstartMemberFunctionThread(this, &AState::imuThread);
    }


    if(_astateOpts.useGPS == 0) {
      DGClockMutex(&m_MetaStateMutex);     
      _metaState.gpsEnabled = 0;
      DGCunlockMutex(&m_MetaStateMutex);     
    } else {
      gpsInit();           // This will set gpsEnabled
    }

    if (_metaState.gpsEnabled == 1) {
      DGCstartMemberFunctionThread(this, &AState::gpsThread);
    }

    if(_astateOpts.useOBD == 0) {
      DGClockMutex(&m_MetaStateMutex);     
      _metaState.obdEnabled = 0;
      DGCunlockMutex(&m_MetaStateMutex);     
    } else {
      obdInit();           // This will set obdEnabled
    }

    if (_metaState.obdEnabled == 1) {
      DGCstartMemberFunctionThread(this, &AState::obdThread);
    }

    if(_astateOpts.useNov == 0) {
      DGClockMutex(&m_MetaStateMutex);     
      _metaState.novEnabled = 0;
      DGCunlockMutex(&m_MetaStateMutex);     
    } else {
      novInit();           // This will set novEnabled
    }

    if (_metaState.novEnabled == 1) {
      DGCstartMemberFunctionThread(this, &AState::novThread);
    }



    DGCstartMemberFunctionThread(this, &AState::updateStateThread);

    DGCstartMemberFunctionThread(this, &AState::metaStateThread);

    DGCstartMemberFunctionThread(this, &AState::getJumpMessage);


    //Start sparrow threads
    if(_astateOpts.useSparrow) {
      DGCstartMemberFunctionThread(this, &AState::sparrowThread);
    }


  }
}

void AState::restart() {
  // Lock all the mutexes--this will stop all the threads at their locks.
  DGClockMutex(&m_VehicleStateMutex);
  DGClockMutex(&m_MetaStateMutex);
  DGClockMutex(&m_GPSDataMutex);
  DGClockMutex(&m_IMUDataMutex);
  DGClockMutex(&m_OBDDataMutex);
  DGClockMutex(&m_NovDataMutex);
  DGClockMutex(&m_HeartbeatMutex);

  // Reinitialize all the values

  //Initialize EVERYTHING to be zeroed for good measure.
  _vehicleState.Timestamp = startTime;
  _vehicleState.Northing = 0;
  _vehicleState.Easting = 0;
  _vehicleState.Altitude = 0;
  _vehicleState.Vel_N = 0;
  _vehicleState.Vel_E = 0;
  _vehicleState.Vel_D = 0;
  _vehicleState.Acc_N = 0;
  _vehicleState.Acc_E = 0;
  _vehicleState.Acc_D = 0;
  _vehicleState.Roll = 0;
  _vehicleState.Pitch = 0;
  _vehicleState.Yaw = 0;
  _vehicleState.RollAcc = 0;
  _vehicleState.PitchAcc = 0;
  _vehicleState.YawAcc = 0;

  // Reinitialize the Kalman Filter covariances, if they've changed.
  readCovariances();

  // Unlock the mutexes and begin again.
  DGCunlockMutex(&m_VehicleStateMutex);
  DGCunlockMutex(&m_MetaStateMutex);
  DGCunlockMutex(&m_GPSDataMutex);
  DGCunlockMutex(&m_IMUDataMutex);
  DGCunlockMutex(&m_OBDDataMutex);
  DGCunlockMutex(&m_NovDataMutex);
  DGCunlockMutex(&m_HeartbeatMutex);
}

AState::~AState()
{
  if (_astateOpts.logRaw == 1) {
    imuLogStream.close();
    gpsLogStream.close();
    obdLogStream.close();
    novLogStream.close();
  }
  
  if (_astateOpts.useReplay == 1) {
    imuReplayStream.close();
    gpsReplayStream.close();
    obdReplayStream.close();
    novReplayStream.close();
  }

  if (_astateOpts.debug == 1) {
    debugStream.close();
  }

  if (_astateOpts.useAutotune == 1) {
    autotuneStream.close();
  }

}

// VehicleState messaging Method.
void AState::broadcast()
{
  pthread_mutex_t* pMutex = &m_VehicleStateMutex;
  if(m_skynet.send_msg(broadcastStateSock,
		       &_vehicleState, 
		       sizeof(VehicleState), 
		       0, 
		       &pMutex) 
     != sizeof(VehicleState))
    {
      cerr << "AState::Broadcast(): didn't send right size state message" << endl;
    }
  if(getLoggingEnabled()) {
    if(checkNewDirAndReset()) {
      cout << "Opening new file" << endl;
      if (timberLogStream.is_open()) {
	timberLogStream.close();
      }
      sprintf(timberLogFile, "%sstate.dat",getLogDir().c_str());
      timberLogStream.open(timberLogFile, fstream::out|fstream::app);
    }
    timberLogStream.precision(10);
    timberLogStream <<  _vehicleState.Timestamp << '\t';
    timberLogStream <<  _vehicleState.Northing << '\t' << _vehicleState.Easting << '\t' << _vehicleState.Altitude << "\t";
    timberLogStream <<  _vehicleState.Vel_N << '\t' << _vehicleState.Vel_E << '\t' << _vehicleState.Vel_D << "\t";
    timberLogStream <<  _vehicleState.Acc_N << '\t' << _vehicleState.Acc_E << '\t' << _vehicleState.Acc_D << "\t";
    timberLogStream <<  _vehicleState.Roll << '\t' << _vehicleState.Pitch << '\t' << _vehicleState.Yaw << "\t";
    timberLogStream <<  _vehicleState.RollRate << '\t' << _vehicleState.PitchRate << '\t' << _vehicleState.YawRate << "\t";
    timberLogStream <<  _vehicleState.RollAcc << '\t' << _vehicleState.PitchAcc << '\t' << _vehicleState.YawAcc << '\t';
		timberLogStream <<  _vehicleState.NorthConf << '\t' << _vehicleState.EastConf << '\t' << _vehicleState.HeightConf << '\t';
		timberLogStream <<  _vehicleState.RollConf << '\t' << _vehicleState.PitchConf << '\t' << _vehicleState.YawConf << endl;
  }
}

void AState::playback()
{
  unsigned long long longTmp;
  double doubleTmp;
  unsigned long long playbackTime;
  sprintf(timberPlaybackFile, "%sstate.dat",getPlaybackDir().c_str());
  cout << "Opening file... " << timberPlaybackFile << endl;
	if(_astateOpts.oldLog == 1) cout << "Playing back an old log file without confidence info" << endl;
  timberPlaybackStream.open(timberPlaybackFile);
  _vehicleState.Timestamp = 0;
  while (1) {
    if(checkNewPlaybackDirAndReset()) {
      if (timberPlaybackStream.is_open()) {
	timberPlaybackStream.close();
      }
      sprintf(timberPlaybackFile, "%sstate.dat",getPlaybackDir().c_str());
      cout << "Re-Opening file... " << timberPlaybackFile << endl;
      timberPlaybackStream.open(timberPlaybackFile);
    } 
    if (!timberPlaybackStream) {
      cout << __FILE__ << " [" << __LINE__ 
	   << "]: Reached the end of the playback file, or couldn't open it to begin with" << endl;
      exit(0);
    } else {
      unsigned long long timberTime = getPlaybackTime();
      while(timberTime >= _vehicleState.Timestamp && timberPlaybackStream) {
	timberPlaybackStream >> longTmp;
	_vehicleState.Timestamp = longTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.Northing = doubleTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.Easting = doubleTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.Altitude = doubleTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.Vel_N = doubleTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.Vel_E = doubleTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.Vel_D = doubleTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.Acc_N = doubleTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.Acc_E = doubleTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.Acc_D = doubleTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.Roll = doubleTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.Pitch = doubleTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.Yaw = doubleTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.RollRate = doubleTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.PitchRate = doubleTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.YawRate = doubleTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.RollAcc = doubleTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.PitchAcc = doubleTmp;

	timberPlaybackStream >> doubleTmp;
	_vehicleState.YawAcc = doubleTmp;

				if(_astateOpts.oldLog == 0) {
					//We're not playing back an old log
					timberPlaybackStream >> doubleTmp;
					_vehicleState.NorthConf = doubleTmp;

					timberPlaybackStream >> doubleTmp;
					_vehicleState.EastConf = doubleTmp;

					timberPlaybackStream >> doubleTmp;
					_vehicleState.HeightConf = doubleTmp;

					timberPlaybackStream >> doubleTmp;
					_vehicleState.RollConf = doubleTmp;

					timberPlaybackStream >> doubleTmp;
					_vehicleState.PitchConf = doubleTmp;

					timberPlaybackStream >> doubleTmp;
					_vehicleState.YawConf = doubleTmp;

				} else {
					_vehicleState.NorthConf = 0.0;
					_vehicleState.EastConf = 0.0;
					_vehicleState.HeightConf = 0.0;
					_vehicleState.RollConf = 0.0;
					_vehicleState.PitchConf = 0.0;
					_vehicleState.YawConf = 0.0;
				}
      }
      playbackTime = getPlaybackTime();
      
      if (_vehicleState.Timestamp > playbackTime) {
	blockUntilTime(_vehicleState.Timestamp);
	broadcast();
      }
    }
  }
}

void AState::active() {
  while (!quitPressed) {
    sleep(1);
  }
}
  
