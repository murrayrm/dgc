/*
** get_vstate.cc
**
**  
** 
**
*/

#include "GlobalPlanner.hh" // standard includes, arbiter, datum, ...

void GlobalPlanner::UpdateState()
{ 
  Mail msg = NewQueryMessage( MyAddress(), MODULES::VState, VStateMessages::GetState);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> d.SS; // download the new state
  }

} // end WaypointNav::UpdateState() 
