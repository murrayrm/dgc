#include "ModeManModule.hh"    // sparrow e-stop var's
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"
#include "navdisplay.hh"
#include "vddtable.h"

extern int QUIT_PRESSED; 

void ModeManModule::UpdateSparrowVariablesLoop() {

  while( !QUIT_PRESSED ) {

    /* update state vars */
    Easting = m_state.Easting;
    Northing = m_state.Northing;
    Altitude = m_state.Altitude;
    Vel_N = m_state.Vel_N;
    Vel_E = m_state.Vel_E;
    Vel_D = m_state.Vel_D;
    Acc_N = m_state.Acc_N;
    Acc_E = m_state.Acc_E;
    Acc_D = m_state.Acc_D;
    Roll = m_state.Roll;
    Pitch = m_state.Pitch;
    Yaw = m_state.Yaw;
    RollRate = m_state.RollRate;
    PitchRate = m_state.PitchRate;
    YawRate = m_state.YawRate;
    //double RollAcc = 0, PitchAcc = 0, YawAcc = 0;    
    dispSS_decimaltime =  m_state.Timestamp/1000000.0;

    /* other state vars not in the state struct */
    speed        = sqrt(Vel_N * Vel_N + Vel_E * Vel_E);
    PitchDeg     = Pitch     * 180.0 /M_PI;
    RollDeg      = Roll      * 180.0 /M_PI;
    YawDeg       = Yaw       * 180.0 /M_PI;
    PitchRateDeg = PitchRate * 180.0 /M_PI;
    RollRateDeg  = RollRate  * 180.0 /M_PI;
    YawRateDeg   = YawRate   * 180.0 /M_PI;

    /* paths from inputs
    ROAD_    = d.allVoters[ArbiterInput::RoadFollower];
    ARB_     = d.combined;
    */
    
    for (int i = 0; i < PathInput::COUNT; i++) {
      InputUpdateCount[i] = pathUpdateCount[i];
      PathPresent[i] = (int)present[i];
      NewPathPresent[i] = (int)newPathPresent[i];
    }
    
    CurMode = ModeManMode;
    CurPathModule = curPathModule;
    PathOutCount = pathOutCount;
    StateUpdateCount = updateCountState;
    SparrowUpdateCount++;
    /**
     * Updating mm.PassThroughModule: this will break if ModeManModule
     * actually ever wants to PassThroughModule.
     **/
    if (spPassThroughModule > -1 && spPassThroughModule < PathInput::REACTIVE)
      PassThroughModule = spPassThroughModule;  
    else {
      spPassThroughModule = -1;
      PassThroughModule = spPassThroughModule;
    }

    // Wait a bit, because other threads will print some stuff out
    usleep(500000); 
  }

}

void ModeManModule::SparrowDisplayLoop() {

  dbg_all = 0;

  if (dd_open() < 0) exit(1);
  dd_bindkey('Q', user_quit);
  dd_bindkey('P', user_estop_pause);
  dd_bindkey('U', user_estop_unpause);

  dd_usetbl(vddtable);

  usleep(500000); // Wait a bit, because other threads will print some stuff out
  dd_loop();
  dd_close();

}

// TODO: Set the shutdown flag in the quit function
int user_quit(long arg)
{
  return DD_EXIT_LOOP;
}


int user_estop_pause(long arg)
{
  sparrowEStopPauseSignal = true;
  return 0;
}

int user_estop_unpause(long arg)
{
  sparrowEStopPauseSignal = false;
  return 0;
}

