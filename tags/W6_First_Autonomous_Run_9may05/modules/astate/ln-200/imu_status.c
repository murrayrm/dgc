/*----------------------------------------------------------------------------
--                                                                          --
--                        U N C L A S S I F I E D                           --
--                                                                          --
--                  L I T T O N   P R O P R I E T A R Y                     --
--                                                                          --
-- Copyright (c) 2004, Litton Systems, Inc.                                 --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------
--                                                                          --
--                       RESTRICTED RIGHTS LEGEND                           --
--                                                                          --
-- Use, duplication or disclosure is subject to restrictions stated in      --
-- Proprietary Information Agreement between Litton Systems, Inc.,          --
-- Guidance and Controll Systems Division and California Institute of       --
-- Technology.                                                              --
--                                                                          --
----------------------------------------------------------------------------*/

/*
example_application.c

Read data from the LN-200 via the FastComm ESCC/P card device driver, at 400 Hz.
Calculate and display scaled 1 Hz sums of the delta velocities and delta angles.

Notes:

The EXCC/P device driver can buffer up to 3 seconds worth of LN-200 data. This allows an
application program to not read data from the device driver for up to 3 seconds, as long
as the application program keeps up with the LN-200 output over the long term (read data
faster than 400 Hz once the program has gotten behind).

This example application program checks the integrity of the gather data in 4 ways, each
time data is read from the LN-200. The 4 ways are:

1) The device driver is statused to see if an internal buffer overrun has occurred.

2) The data returned from the device driver is checked for the proper byte count.

3) The SDLC (LN-200 data bus) CRC byte included at the end of the data read from the
device driver is verified.

4) The incrementing mux counter (the 14th byte in the data read from the device driver)
is tracked to verify that it increments by one each time data is read from the device
driver.

If any one of these checks fails, the application program has lost data.
*/

/*
Operating mode switches:

The following two defines enable code that shows how to status the ESCC/P card to see if data
is available for reading. With both defines set to 0, the application program call to read
data from the device driver will block until there is data available. That block can be as
long as 2.5 milli-seconds (400 Hz), the amount of time between data frames transmitted by
the LN-200.  
*/

#define READ_NONBLOCK			0	// Read, not blocking for next available data.
#define CHECK_RX_READY			0	// Check for data available before reading.

/* Program defines. */

#define MSG_RATE				400	// Rate in Hz the LN-200 outputs a message.
#define FRAME_SIZE_REQUEST		64	// Minimun number of bytes to request from a DD read call. 
#define FRAME_SIZE_GOOD_RETURN	27	// Number of bytes that should be returned from a DD read call.
#define MUX_MAX_VALUE			33	// MUX counter maximum value before rolling back over to 0.

/* Program includes. */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "esccpdrv.h"
#include <sys/ioctl.h>
#include <errno.h>
#include <math.h>
#include <sys/poll.h>

void printbits(short word) {
    int bit;
    int printcount = 0;
    int i;
    printf("LSB: ");
    for (i=0; i<=15;i++) {
        if (printcount == 4) {
            printf(" ");
            printcount = 0;
        }
        int bit = ((word >> i) & 1);
        printf("%d",bit);
        printcount++;
    }
}

int main(int argc, char * argv[])
{
	/* ESCC/P Interface vars. */
	
	int escc = -1 ;				// ESCC/P Device Driver Handle. Init to -1 for device open failure check.
	char ddname[ 80 ] ;			// ESCC/P Device Driver Name.
	unsigned bytes_read ;		// Number of bytes actually returned from a DD read.
	char rx_data[4096] ;		// Receive data buffer (for DD read calls).
	unsigned long ddstatus ;	// Device Driver return status.

	/* Read data integrity check vars. */

	unsigned long dd_rx_buf_orun_cnt= 0 ;	// Count of times the DD internal receive buffer have been overrun (data lost).
	unsigned long read_cnt_bad		= 0 ;	// Count of times DD read call did not return 27 bytes (data lost).
	unsigned long read_crc_fail_cnt	= 0	;	// Count of read data crc failures (data invalid, therefore lost).
	unsigned long read_cnt			= 0 ;	// Count of times a valid read has occured.
	unsigned char mux_last_value	= 100 ;	// Used to track the mux counter value (rotates from 0 to 33). Init to 100 for tracking initialization.
	unsigned long mux_skip_cnt		= 0 ;	// Count of times the mux counter value skipped (data lost).

	/* 1 Hz data summing and conversion vars. */

	long x_delta_vel				= 0	;	// X Delta Velocity 1 hz sum.
	long y_delta_vel				= 0	;	// Y Delta Velocity 1 Hz sum.
	long z_delta_vel				= 0	;	// Z Delta Velocity 1 Hz sum.
	long x_delta_angle				= 0 ;	// X Delta Angle 1 Hz sum.
	long y_delta_angle				= 0 ;	// Y Delta Angle 1 Hz sum.
	long z_delta_angle				= 0 ;	// Z Delta Angle 1 Hz sum.

	double vel_scale = pow( 2, -14 ) ;							// Value to scale raw velocities to meters/second.
	double ang_scale = pow( 2, -19 ) * 57.2957795131 * 3600 ;	// Value to scale raw angles to degrees/hour.

	double s_xdv ;	// Scaled X Delta Velocity 1 hz sum.
	double s_ydv ;	// Scaled Y Delta Velocity 1 hz sum.
	double s_zdv ;	// Scaled Z Delta Velocity 1 hz sum.
	double s_xda ;	// Scaled X Delta Angle 1 hz sum.
	double s_yda ;	// Scaled Y Delta Angle 1 hz sum.
	double s_zda ;	// Scaled Z Delta Angle 1 hz sum.

    short imu_status_word;
    short mode_word;
    short multi_word;

    short x_gyro_tmp;
    short y_gyro_tmp;
    short z_gyro_tmp;
    short x_accel_tmp;
    short y_accel_tmp;
    short z_accel_tmp;
    short diode_tmp;
    short receiv_tmp;
    short bfs_tmp;
    short Vminus5_mon;
    short Vplus5_mon;
    short Vminus15_mon;
    short Vplus15_mon;
    short analog_gnd;
    short imu_det_fail0;
    short imu_det_fail1;
    short imu_det_fail2a;
    short imu_det_fail3;
    short imu_det_fail15;
    short imu_det_fail2b;
    short ad_sat_count_X;
    short ad_sat_count_Y;
    short ad_sat_count_Z;

    int done = 0;


///////////////////////////////////////////////////////////////////////////////

	/* Create the DD name that will access Port 0 of the ESCC/P. */

	sprintf( ddname, "/dev/escc0" ) ;

	/* Open the DD for reading, obtaining a handle to the DD. */

#if READ_NONBLOCK
	escc = open( ddname, O_RDWR | O_NONBLOCK ) ;
#else
	escc = open( ddname, O_RDWR ) ;
#endif

	if( escc == -1 ) {

		printf( "Cannot open %s.\n", ddname ) ;

		perror( NULL ) ;

		exit( 1 ) ;
	}

	/* Read the DD status and discard to clear any previous conditions. */

	if ( ioctl( escc, ESCC_IMMEDIATE_STATUS, &ddstatus ) == -1 ) {

		printf( "Failed initial ioctl ESCC_IMMEDIATE_STATUS call.\n" ) ;

		perror( NULL ) ;

		close( escc ) ;

		exit( 1 ) ;
	}

	/* Loop forever reading data. */

	do {

		/* Check for the DD for receive data overrun condition. */

		if ( ioctl( escc, ESCC_IMMEDIATE_STATUS, &ddstatus ) == -1 ) {

			printf( "Failed ioctl ESCC_IMMEDIATE_STATUS call.\n" ) ;

			perror( NULL ) ;

			close( escc ) ;

			exit( 1 ) ;
		}
		else {

			/* The DD has lost data !!!! */

			if ( ddstatus & ST_OVF ) dd_rx_buf_orun_cnt++ ;
		}

#if CHECK_RX_READY
		/* Check if the DD has any data. */
		
		if ( ioctl( escc, ESCC_RX_READY, &ddstatus ) == -1 ) {

			printf( "Failed ioctl ESCC_RX_READY call.\n" ) ;

			perror( NULL ) ;

			close( escc ) ;

			exit( 1 ) ;
		}
		else {

			if ( ddstatus == 0 ) {

				/* No data ready for reading. */

				/************************************************************************/
				/*																		*/
				/* WHERE TO PUT LOGIC THAT DOES WHATEVER TILL THERE IS DATA TO READ.	*/
				/*																		*/
				/************************************************************************/

			}
			else {
#endif
				/* Read a frame of data. */

				if ( ( bytes_read = read( escc, rx_data, FRAME_SIZE_REQUEST ) ) == -1 ) {
#if READ_NONBLOCK
					/* Ignore errno 11, "Resource temporarily unavailable",
					   the error returned when in non-blocking mode and there is no data. */

					if ( errno != 11 ) {
#endif
						printf( "Read failed, errno = %d.\n", errno ) ;

						perror( NULL ) ;

						close( escc ) ;

						exit( 1 ) ;
#if READ_NONBLOCK
					}
					else {

						/* Non-blocked call to read() has returned with no data read. */

						/************************************************************************/
						/*																		*/
						/* WHERE TO PUT LOGIC THAT DOES WHATEVER TILL THERE IS DATA TO READ.	*/
						/*																		*/
						/************************************************************************/
					}
#endif
				}
				else {

					/* Data has been read. */

					/* Check that the correct number of bytes were read. */

					if ( bytes_read != FRAME_SIZE_GOOD_RETURN ) {

						read_cnt_bad++ ;
					}
					else {

						/* Process the data only when a full frame has been read. */

						/* Check the data frame CRC. */

						if ( ( rx_data[ bytes_read - 1 ] & 0x20 ) == 0x00 ) {

							read_crc_fail_cnt++ ;
						}
						else {

							/* Process the data only when a valid CRC has been read. */

							/* Track the rotating mux counter (data lost check). */

							if ( mux_last_value == 100 ) {

								mux_last_value = rx_data[ 14 ] ;
							}
							else {

								if ( rx_data[ 14 ] == 0 ) {
						
									if ( mux_last_value != MUX_MAX_VALUE ) {

										mux_skip_cnt++ ;
									}

								}
								else {

									if ( ( rx_data[ 14 ] - mux_last_value ) != 1 ) {

										mux_skip_cnt++ ;
									}
								}

								mux_last_value = rx_data[14] ;
							}

							/* Add the delta vel and delta angle readings to their respective sums. */

							x_delta_vel   += *(short *)&rx_data[ 0 ] ;

							y_delta_vel   += *(short *)&rx_data[ 2 ] ;

							z_delta_vel   += *(short *)&rx_data[ 4 ] ;

							x_delta_angle += *(short *)&rx_data[ 6 ] ;

							y_delta_angle += *(short *)&rx_data[ 8 ] ;

							z_delta_angle += *(short *)&rx_data[ 10 ] ;

							imu_status_word = *(short *)&rx_data[ 12 ] ;
							mode_word = *(short *)&rx_data[ 14 ] ;
							multi_word = *(short *)&rx_data[ 16 ] ;

                            if ((mode_word & 255) != mux_last_value) {
                                printf("Count isn't right!\n");
                                return 0;
                            }
                                if (done == 1) {
                                    printf("Mux Count Value: %d - Word: %d - ", mux_last_value, *(short *)&rx_data[16]);
                                    printbits(*(short *)&rx_data[16]);
                                    printf("\n");
                                }
                                    
                                if (done == 2) {

                                printf("\nIMU status summary:\n");
                                printbits(imu_status_word);
                                printf("\n");

                                printf("Mode Bit/Mux ID:\n");
                                printbits(mode_word);
                                printf("\n");

                                printf("Multiplexed data word:\n");
                                printbits(multi_word);
                                printf("\n");
                                printf("\n");

                                printf("All data aquired:\n");
                                printf("\nIMU status summary:\n");
                                printf("delta vel counter: %d\n", ((imu_status_word >> 0) & 1));
                                printf("d/a converter: %d\n", ((imu_status_word >> 1) & 1));
                                printf("gyro: %d\n", ((imu_status_word >> 2) & 1));
                                printf("accelerometer: %d\n", ((imu_status_word >> 3) & 1));
                                printf("gyro loop controller: %d\n", ((imu_status_word >> 4) & 1));
                                printf("gyro tempterature: %d\n", ((imu_status_word >> 5) & 1));
                                printf("accelerometer tempterature: %d\n", ((imu_status_word >> 6) & 1));
                                printf("power supply voltage: %d\n", ((imu_status_word >> 7) & 1));
                                printf("a/d converter: %d\n", ((imu_status_word >> 8) & 1));
                                printf("serial i/o: %d\n", ((imu_status_word >> 9) & 1));
                                printf("cpu or memory: %d\n", ((imu_status_word >> 10) & 1));
                                printf("laser diode: %d\n", ((imu_status_word >> 11) & 1));
                                printf("TEC: %d\n", ((imu_status_word >> 12) & 1));
                                printf("BFS Fiber Temp: %d\n", ((imu_status_word >> 13) & 1));
                                printf("optical receiver: %d\n", ((imu_status_word >> 14) & 1));
                                printf("reserved: %d\n", ((imu_status_word >> 15) & 1));

                                printf("\nMode BIT:\n");
                                printf("Gyro Accuracy degraded: %d\n", ((mode_word >> 8) & 1));
                                printf("Gyro disabled: %d\n", ((mode_word >> 9) & 1));
                                printf("shutdown on failure inhib: %d\n", ((mode_word >> 10) & 1));
                                printf("fast start: %d\n", ((mode_word >> 11) & 1));
                                printf("commanded bit in progress: %d\n", ((mode_word >> 12) & 1));
                                printf("reserved for external sync: %d\n", ((mode_word >> 13) & 1));
                                printf("accel data invalid: %d\n", ((mode_word >> 14) & 1));
                                printf("spare: %d\n", ((mode_word >> 15) & 1));

                                printf("\nMux data:\n");
                                printf("x gyro tmp: %d\n", x_gyro_tmp);
                                printf("y gyro tmp: %d\n", y_gyro_tmp);
                                printf("z gyro tmp: %d\n", z_gyro_tmp);
                                printf("x accel tmp: %d\n", x_accel_tmp);
                                printf("y accel tmp: %d\n", y_accel_tmp);
                                printf("z accel tmp: %d\n", z_accel_tmp);
                                printf("diode tmp: %d\n", diode_tmp);
                                printf("receiv tmp: %d\n", receiv_tmp);
                                printf("bfs tmp: %d\n", bfs_tmp);
                                printf("-5V mon: %d\n", Vminus5_mon);
                                printf("+5V mon: %d\n", Vplus5_mon);
                                printf("-15V mon: %d\n", Vminus15_mon);
                                printf("+15V mon: %d\n", Vplus15_mon);
                                printf("analog gnd: %d\n", analog_gnd);
                                printf("IMU detailed failure 0:  %d\t", imu_det_fail0);
                                printbits(imu_det_fail0);
                                printf("\n");
                                printf("IMU detailed failure 1:  %d\t", imu_det_fail1);
                                printbits(imu_det_fail1);
                                printf("\n");
                                printf("IMU detailed failure 2a: %d\t", imu_det_fail2a);
                                printbits(imu_det_fail2a);
                                printf("\n");
                                printf("IMU detailed failure 3:  %d\t", imu_det_fail3);
                                printbits(imu_det_fail3);
                                printf("\n");
                                printf("IMU detailed failure 15: %d\t", imu_det_fail15);
                                printbits(imu_det_fail15);
                                printf("\n");
                                printf("IMU detailed failure 2b: %d\t", imu_det_fail2b);
                                printbits(imu_det_fail2b);
                                printf("\n");
                                printf("a/d sat count x: %d\n", ad_sat_count_X);
                                printf("a/d sat count y: %d\n", ad_sat_count_Y);
                                printf("a/d sat coutn z: %d\n", ad_sat_count_Z);

                                close( escc );

                                return 0;
                                }
                
                            switch (mux_last_value) {
                            case 0:
                                x_gyro_tmp = *(short *)&rx_data[16];
                                break;
                            case 1:
                                y_gyro_tmp = *(short *)&rx_data[16];
                                break;
                            case 2:
                                z_gyro_tmp = *(short *)&rx_data[16];
                                break;
                            case 3:
                                x_accel_tmp = *(short *)&rx_data[16];
                                break;
                            case 4:
                                y_accel_tmp = *(short *)&rx_data[16];
                                break;
                            case 5:
                                z_accel_tmp = *(short *)&rx_data[16];
                                break;
                            case 6:
                                diode_tmp = *(short *)&rx_data[16];
                                break;
                            case 7:
                                receiv_tmp = *(short *)&rx_data[16];
                                break;
                            case 8:
                                bfs_tmp = *(short *)&rx_data[16];
                                break;
                            case 9:
                                Vminus5_mon = *(short *)&rx_data[16];
                                break;
                            case 10:
                                Vplus5_mon = *(short *)&rx_data[16];
                                break;
                            case 11:
                                Vminus15_mon = *(short *)&rx_data[16];
                                break;
                            case 12:
                                Vplus15_mon = *(short *)&rx_data[16];
                                break;
                            case 13:
                                analog_gnd = *(short *)&rx_data[16];
                                break;
                            case 14:
                                imu_det_fail0 = *(short *)&rx_data[16];
                                break;
                            case 15:
                                imu_det_fail1 = *(short *)&rx_data[16];
                                break;
                            case 16:
                                imu_det_fail2a = *(short *)&rx_data[16];
                                break;
                            case 17:
                                imu_det_fail3 = *(short *)&rx_data[16];
                                break;
                            case 29:
                                imu_det_fail15 = *(short *)&rx_data[16];
                                break;
                            case 30:
                                imu_det_fail2b = *(short *)&rx_data[16];
                                break;
                            case 31:
                                ad_sat_count_X = *(short *)&rx_data[16];
                                break;
                            case 32:
                                ad_sat_count_Y = *(short *)&rx_data[16];
                                break;
                            case 33:
                                ad_sat_count_Z = *(short *)&rx_data[16];
                                done++;
                                break;
                            }

							/* At a 1 Hz rate:
							   scale the sums to engineering units,
							   display the scaled sums (along with the error counters),
							   zero the sums for the next 1 Hz interval. */ 
						}
					}
				}
#if CHECK_RX_READY
			}
		}
#endif
	} while ( 1 ) ; 

	/* Close the DD and exit the program. */

	close( escc ) ;

	exit( 1 ) ;
}


