/* AState_IMU.cc - new vehicle state estimator
 *
 * JML 31 Dec 04
 *
 * */

#include "AState.hh"

struct raw_IMU
{
    unsigned long long imu_time;
    double dvx;
    double dvy;
    double dvz;
    double dtx;
    double dty;
    double dtz;

};

extern int QUIT_PRESSED;

void AState::IMU_init()
{
	if(imu_enabled == 0)
	{
		return;
	}
        if (replay == 1) {
                raw_IMU imu_in;

                imu_replay_stream.read((char*)&imu_in, sizeof(raw_IMU));

                imudata.dvx = imu_in.dvx;
                imudata.dvy = imu_in.dvy;
                imudata.dvz = imu_in.dvz;
                imudata.dtx = imu_in.dtx;
                imudata.dty = imu_in.dty;
                imudata.dtz = imu_in.dtz;
                imu_log_start = imu_in.imu_time;

        } else {

	imu_enabled = IMU_open();
	if(imu_enabled == -1)
		cerr << "AState::IMU_init(): failed to open IMU" << endl;

        }
}

void AState::IMU_thread()
{
    unsigned long long nowtime;

    raw_IMU imu_in;
    raw_IMU imu_out;

    if (imu_enabled == 0) {
        return;
    } else if (imu_enabled == -1) {
        cerr << "AState::IMU_thread(): not started because IMU failed to start";
        return;
    }
  	while ( QUIT_PRESSED != 1) {
		DGClockMutex(&m_IMUDataMutex);
			if (replay == 1) 
			{
                imu_replay_stream.read((char*)&imu_in, sizeof(raw_IMU));

                imudata.dvx = imu_in.dvx;
                imudata.dvy = imu_in.dvy;
                imudata.dvz = imu_in.dvz;
                imudata.dtx = imu_in.dtx;
                imudata.dty = imu_in.dty;
                imudata.dtz = imu_in.dtz;

                my_imu_time = imu_in.imu_time - imu_log_start + starttime;

                DGCgettime(nowtime);
                while( my_imu_time > nowtime ) {
                    usleep(1);
                    DGCgettime(nowtime);
                }
			} else {
      				IMU_read(&imudata); // Need to add new code for IMU_read!
			        DGCgettime(my_imu_time); // time stamp as soon as data read.
			}
            if (log_raw == 1)
            {
                imu_out.imu_time = my_imu_time;
                imu_out.dvx = imudata.dvx;
                imu_out.dvy = imudata.dvy;
                imu_out.dvz = imudata.dvz;
                imu_out.dtx = imudata.dtx;
                imu_out.dty = imudata.dty;
                imu_out.dtz = imudata.dtz;
                
                imu_log_stream.write((char*)&imu_out, sizeof(raw_IMU));
            }
      		++imu_count;
		DGCunlockMutex(&m_IMUDataMutex);
		UpdateState();
	}
}
