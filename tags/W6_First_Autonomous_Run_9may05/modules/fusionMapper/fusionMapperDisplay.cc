#include "fusionMapper.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

#include <iostream>


using namespace std;

int QUIT;

int sdSNKey;

#include "vddtable.h"

void FusionMapper::UpdateSparrowVariablesLoop() {
  /*
  int sampleStep;
  XYZcoord newPositionOffset;
  RPYangle newAngleOffset;

  sdSNKey = ladarOpts.optSNKey;
  sdCom = ladarOpts.optCom;
  sdState = ladarOpts.optState;
  strcpy(sdLadarString, ladarOpts.ladarString);
  
  strcpy(sdLogFilename, ladarObject.getLogFilename());

  sdPosX = ladarPositionOffset.X;
  sdPosY = ladarPositionOffset.Y;
  sdPosZ = ladarPositionOffset.Z;
  sdAngP = ladarAngleOffset.P;
  sdAngR = ladarAngleOffset.R;
  sdAngY = ladarAngleOffset.Y;
  
  while(!_QUIT) {
    //Update read-only variables
    sdScanIndex = ladarObject.scanIndex();
    sdNumDeltas = _numDeltas;
    strcpy(sdErrorMessage, ladarObject.getErrorMessage());
    if(ladarObject.currentSourceType()==ladarSource::SOURCE_FILES) {
      sdFiles=1;
    } else {
      sdFiles = 0;
    }
    if(ladarObject.currentSourceType()==ladarSource::SOURCE_SIM) {
      sdSim=1;
    } else {
      sdSim = 0;
    }

    sampleStep = ladarObject.numPoints()/24;
    //printf("sample is %d\n", sampleStep);
    for(int i=0; i<25; i++) {
      angleSample[i] = 90-ladarObject.angle((24-i)*sampleStep)*180.0/M_PI;
      rangeSample[i] = ladarObject.range((24-i)*sampleStep);
    }
    

    //Update variables that changed in the display
    if(CHANGE) {
      CHANGE = 0;

      ladarOpts.optCom = sdCom;
      ladarOpts.optState = sdState;

      newPositionOffset = XYZcoord(sdPosX, sdPosY, sdPosZ);
      newAngleOffset = RPYangle(sdAngR, sdAngP, sdAngY);
      if(newPositionOffset!=ladarPositionOffset || newAngleOffset!=ladarAngleOffset) {
	ladarPositionOffset = newPositionOffset;
	ladarAngleOffset = newAngleOffset;
	ladarObject.setLadarFrame(ladarPositionOffset, ladarAngleOffset);
      }
    }

    _PAUSE = PAUSE;
    _QUIT = QUIT;
    if(STEP) {
      _STEP = 1;
      STEP = 0;
    }
    if(RESET) {
      ladarObject.resetScanIndex();
      _numDeltas = 0;
      RESET = 0;
    }

    usleep(10000);
  }
  */
}

void FusionMapper::SparrowDisplayLoop() 
{
  /*
  dbg_all = 0;

  //PAUSE = _PAUSE;

  if (dd_open() < 0) exit(1);

  dd_bindkey('Q', user_quit);
  dd_bindkey('q', user_quit);

  dd_bindkey('P', user_pause);
  dd_bindkey('p', user_pause);
  dd_bindkey('R', user_reset);
  dd_bindkey('r', user_reset);
  dd_bindkey('S', user_step);
  dd_bindkey('s', user_step);

  dd_usetbl(vddtable);

  usleep(500000); // Wait a bit, because other threads will print some stuff out
  dd_loop();	
  dd_close();
  //_QUIT = 1;
*/
}
/*
int user_quit(long arg)
{
  //PAUSE = 0;
  QUIT = 1;
  return DD_EXIT_LOOP;
}
*/
/*
int user_reset(long arg)
{
  RESET = 1;
  return 0;
}


int user_step(long arg)
{
  STEP = 1;
  return 0;
}


int user_pause(long arg)
{
  if(PAUSE) {
    PAUSE=0;
  } else {
    PAUSE=1;
  }
  return 0;
}


int user_change(long arg) {
  CHANGE=1;
  return 0;
}
*/
