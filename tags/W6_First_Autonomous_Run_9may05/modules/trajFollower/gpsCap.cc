#include "gpsCap.hh"
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <fstream>
#include <pthread.h>



char *testlog  = new char[100];
char *pathlog  = new char[100];

using namespace std;


gpsCap::gpsCap(int skynet_key) 	: CSkynetContainer(SNastate, skynet_key) 
{
  cout<<"before DGCgettime"<<endl;	
	DGCgettime(starttime);
	cout<<"after DGCgettime"<<endl;
		
		char gpsFileName[256];
		time_t t = time(NULL);
		tm *local;
		local = localtime(&t);

  sprintf(gpsFileName, "logs/gps_%04d%02d%02d_%02d%02d%02d.dat", 
	  local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	  local->tm_hour, local->tm_min, local->tm_sec);

  m_outputGPS.open(gpsFileName);
		
 	m_outputGPS << setprecision(20);

}

void gpsCap::Active() {

  char cmd[99];

  m_outputGPS<<"%waypoint file modified to BOB format"<<endl;
  m_outputGPS<<"%Type"<<'\t'<<"Easting"<<'\t'<<"Northing"<<'\t'<<"Radius"<<'\t'<<"Velocity"<<endl;

  cout<<endl<<endl<<"This function now assumes a default speed of 5m/s and width of 4m."<<endl;
  cout<<"You may change it manually (in emacs, meta-shift-5 is find and replace)"<<endl<<endl<<endl;

	  printf("Enter c to capture waypoint, q for quit \n");
	  scanf("%99s",cmd);

	while(cmd[0] != 'q' ) 
	{

	  //sleep(1);
		//cout << "Trying to update state: " << endl;
		UpdateState();
		//cout << "Updated state." << endl;

		m_outputGPS << "0"<<'\t'<<m_state.Easting<<'\t'<<m_state.Northing<<'\t'<<"4.0"<<'\t'<<"5.0"<<endl;

    		//timecalc = (m_state.Timestamp - starttime) / 1e6;
		
		//    		m_outputAstate <<  timecalc << '\t';
    		//m_outputAstate <<  m_state.Northing << '\t' << m_state.Easting << '\t' << m_state.Altitude << "\t";
    		//m_outputAstate <<  m_state.Vel_N << '\t' << m_state.Vel_E << '\t' << m_state.Vel_D << "\t";
    		//m_outputAstate <<  m_state.Acc_N << '\t' << m_state.Acc_E << '\t' << m_state.Acc_D << "\t";
    		//m_outputAstate <<  m_state.Roll << '\t' << m_state.Pitch << '\t' << m_state.Yaw << "\t";
    		//m_outputAstate <<  m_state.RollRate << '\t' << m_state.PitchRate << '\t' << m_state.YawRate << "\t";
    		//m_outputAstate <<  m_state.RollAcc << '\t' << m_state.PitchAcc << '\t' << m_state.YawAcc << "\r\n";
		//m_outputAstate.close();


		//	  printf("Enter speed for this waypoint");
		//scanf("%f",speed);
		//m_outputGPS<<speed<<endl;


	  printf("Enter c to capture waypoint, q for quit \n");
	  scanf("%s",cmd);

    		}
}


int main(int argc, char **argv) {

	int sn_key = 0;

	cout<<"yet another debugging fxn"<<endl;
	char* pSkynetkey = getenv("SKYNET_KEY");
	cout<<"got skynet key"<<endl;
	if ( pSkynetkey == NULL)
	{
		cerr << "SKYNET_KEY environment variable isn't set." << endl;
	} else
	{
		sn_key = atoi(pSkynetkey);
	}
	cerr << "Constructing skynet with KEY = " << sn_key << endl;
	gpsCap ast(sn_key);
	cout<<"about to call active"<<endl;
	ast.Active();
  	return 0;
}


