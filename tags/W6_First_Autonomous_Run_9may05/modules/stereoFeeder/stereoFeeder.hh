#ifndef STEREOFEEDER_HH
#define STEREOFEEDER_HH

#include <StateClient.h>
#include <pthread.h>
#include <CMapPlus.hh>
#include <unistd.h>
#include <frames/frames.hh>
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <stereovision/stereoSource.hh>
#include <stereovision/stereoProcess.hh>
#include <time.h>
#include <sys/time.h>
#include <DGCutils>

/*
#include <stdio.h>
#include <libraw1394/raw1394.h>
#include <libdc1394/dc1394_control.h>
#include <videre/svsclass.h>
#include <stdlib.h>
*/

using namespace std;
using namespace boost;


typedef boost::recursive_mutex::scoped_lock Lock;

//Sparrow Display Functions
int user_quit(long arg);
int user_pause(long arg);
int user_step(long arg);
int user_reset(long arg);

int display_all(long arg);
int display_rect(long arg);
int display_disp(long arg);

int log_all(long arg);
int log_base(long arg);
int log_quickdebug(long arg);
int log_alldebug(long arg);
int log_none(long arg);

enum {
  NO_CAMERAS,
  SHORT_RANGE,
  LONG_RANGE,
  SHORT_COLOR,
  HOMER,

  NUM_CAMERA_TYPES
};


typedef struct{
  char SVSCalFilename[256];
  char SVSParamFilename[256];
  char CamCalFilename[256];
  char CamIDFilename[256];
  char GenMapFilename[256];
} calibFileStruct;


class StereoFeeder : public CStateClient {
public:
  StereoFeeder(int skynetKey);
  ~StereoFeeder();
  
  void ActiveLoop();
  
  void SparrowDisplayLoop();

  void UpdateSparrowVariablesLoop();

  void ImageCaptureThread();

private:
  CMapPlus stereoMap;

  boost::recursive_mutex CameraLock;
  bool *stopCapture;
  stereoSource sourceObject;
  stereoProcess processObject;

  int layerID_stereoElev;
  int layerID_stereoCount;
  double noDataVal_stereoElev;
  double outsideMapVal_stereoElev;

  VehicleState tempState;

  calibFileStruct calibFileObject[NUM_CAMERA_TYPES];
};

int change(long arg);

#endif
