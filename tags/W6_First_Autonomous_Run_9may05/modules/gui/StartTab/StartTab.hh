
/**
 * StartTab.h
 * Revision History:
 * 02/17/2005  hbarnor  Created
 */

#ifndef STARTTAB_HH
#define STARTTAB_HH

#include <gtkmm/box.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/button.h>
#include <gtkmm/frame.h>
#include <gtkmm/table.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/table.h>
#include <gtkmm/textview.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/textbuffer.h>
#include <glibmm/ustring.h>
#include <string>
#include <iostream>
#include <stdio.h>
#include <fstream>

#include "sn_msg.hh"


using namespace std;

#define DEBUG false

/**
 * StartTab class.
 * $Id$ 
 */

static string _module = "Module:";

// Forward declare struct
struct command
{
  char type[20];   //name of the command
  char action;     // the action to perform S is start and k is kill 
  pid_t PID;         //modules's PID if the module could be started or if it is already running. 
                   //It is 0 if module could not be started
  int compID;     // unique computer ID
};

struct ButtonStatus
{
  Gtk::CheckButton * btn;
  struct command cmd;
};

class StartTab : public Gtk::HBox
{

public:
  /**
   * Default constructor.
   */
  StartTab();
  /**
   * Constructor with skynet key .
   */
  StartTab(int sn_key);
  /**
   * StartTab Destructor - for taking care of dynamically 
   * allocated memory. 
   */
  ~StartTab();
  /**
   * addModule - adds a checkbox with the name of the module passed
   * and also the destination computer 
   *
   */
  void addModule(string snModName, int destComp, bool start);
  /**
   * TODO
   */
  void updateConfig(string fileName, string snModName, bool start);
  /**
   * TODO
   */
  void readFile(string fileName);
   /**
   * statusDisplay - logs feedback messages from moduleStarter
   * and displays it in a text box
   */
  void statusDisplay();
private:

  /**
   * TODO 
   *
   */
  Gtk::CheckButton * newCBtn;
  /**
   * number of checkboxes/modules
   */
  int numModules;
  int currentRow;
  const int maxCols; 
  /**
   * Temporay to keep track of the buttons
   * till i figure out TableLists
   */
  list< struct ButtonStatus> * myButtons; 
  /**
   * initialize skynet
   *
   */
  void initSN();
  /**
   * TODO
   *
   */

  void executeSkynet(char action);
  /**
   * nxm grid to hold module check boxes
   */
  Gtk::Table chkBxTbl;
  /*
   * buttonBox - holds the start and stop buttons
   */
  Gtk::HButtonBox buttonBox;
  Gtk::Button moduleSBtn; // module start button
  Gtk::Button moduleKBtn; // module kill button
  Gtk::Frame btnFrm; // frame to hold buttons
  Gtk::Frame chkBxFrm; // frame to hold check boxes
  Gtk::Frame statusFrm; // frame to hold status
  Gtk::VBox m_VBox;
  Gtk::ScrolledWindow statWindow;
  Gtk::TextView statText;
  Glib::RefPtr<Gtk::TextBuffer> buffer;
  Gtk::TextBuffer::iterator statPos;
  //took me 3 hours to figure this out
  Glib::Dispatcher updateDisplay;
  string myConfigFile;
  static const int NUM_CHAR = 256; 
  /**
   *Storage for messages
   */
  char mesg[NUM_CHAR];


  /**
   * a skynet client
   */
  skynet m_skynet;  
  /*
   * ModeMan Start action
   */
  void startModules();
  /*
   * modeman exit action 
   */
  void killModules();
  /**
   * Skynet send socket
   */
  int cmdSendSocket;
  /**
   * sender of status messages    
   */
   modulename sender;
   /**
   * mesgs i recive, this is the
   * moduleStarter status message
   */
  sn_msg myInput;
  /**
   * adds a message to the status tab
   */
  void addMessage();

};

#endif
