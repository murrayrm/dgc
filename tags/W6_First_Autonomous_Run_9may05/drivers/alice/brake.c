//Modified 2-3-05 by David Rosen for use with SDS

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "constants.h"
#include "brake.h"
//#include "serial.h"
#include "SDSPort.h"

int have_serial_port = FALSE;
int brake_serial_port = -1;     /* not a valid number */

const char brake_read[2] = {DGC_ALICE_BRAKE_READ, DGC_ALICE_BRAKE_EOT};
const char brake_command = DGC_ALICE_BRAKE_COMMAND;
const char brake_pressure_read[2] = {DGC_ALICE_BRAKE_PRESSURE_READ, DGC_ALICE_BRAKE_EOT};
char brake_position = DGC_ALICE_BRAKE_ON;
char brake_position_string[4] = "C15";  //initialize with brake on full
char brake_pressure_string[3];
double brake_got_position;
double temp;

SDSPort* brakeport;
int portCreated = 0;

extern char simulator_IP[];

#include <iostream>
using namespace std;


/*!
 * brake_open (int serial_port)
 * 
 * Author:      Will Coulter
 *
 * Description: Opens and configures the serial port for brake use.  Also
 *              applies full brakes.
 *
 * Arguments:   serial_port - the number of the serial port we wish to use.
 *
 * Returns:     FALSE - we could not open and adequately configure serial port
 *              TRUE - we are a go
 *
 * Known Bugs:  None.
 **************************************************************************************/
int brake_open (int serial_port)
{
    /* no validation testing, assume serial port code will die gracefully */
    //if (serial_open_advanced(serial_port, B9600, 
  // (SR_PARITY_DISABLE | SR_SOFT_FLOW_DISABLE | SR_TWO_STOP_BIT)) != 0)
     //{
     //     return FALSE;
     //}
  cout<<"Starting brake_open()"<<endl;

     if(portCreated == 0)
     {
      brakeport = new SDSPort("192.168.0.60", serial_port);
      if(brakeport->isOpen() == 1)
	{
	  //	  cout<<"Brake connection established"<<endl;
	  portCreated == 1;
	}
     }
     else
     {
       while(brakeport->isOpen() != 1)
       {
        brakeport->openPort();
       }
     }
    brake_serial_port = serial_port;
    have_serial_port = TRUE;

    /* Try and make sure the vehicle can brake (can use the serial port) */
    if (brake_pause() != TRUE) {
      cout<<"Brake pause() failed in serial_open() command"<<endl;
        return FALSE;
    }
    if(brake_getposition() == -1)
      {
	return FALSE;
      }

    cout<<"Brake open executed correctly"<<endl;
    return TRUE;
}

/**************************************************
simulator_brake_open called instead of brake_open when running in gazebo(simulation mode)
*/

void simulator_brake_open(int serial_port)
{
  brake_serial_port = serial_port;
  brakeport = new SDSPort(simulator_IP, serial_port);
  portCreated = 1;
  have_serial_port = TRUE;
  //  cout<<"brake read in open" << brake_read << endl;
}



/*!
 * brake_close (void)
 * 
 * Author:      Will Coulter
 *
 * Description: Sends a command to apply full brakes (not checking that they
 *              are) and then closes the serial port.
 *
 * Arguments:   None.
 *
 * Returns:     FALSE - Error closing the serial port.
 *              TRUE - Serial port has been closed.
 *
 * Known Bugs:  None.
 **************************************************************************************/
int brake_close(void)
{
//if (serial_close(brake_serial_port) == -1) {
//      return FALSE;
//  }

    if(brakeport->closePort() == 0)
    {
      cout<<"brake close() executed correctly"<<endl;
     return TRUE;
    }
    cout<<"Brake close failed"<<endl;
    return FALSE;
}

/*!
 * brake_setposition (int position)
 * 
 * Author:      Will Coulter
 * Revision:    Jeff Lamb (JCL) 20 Jan 05
 *              Changed to string based communication
 *
 * Description: Sends the microcontroller the desired position of the braking
 *              actuator.  
 *
 * Arguments:   position - Desired actuator position; *must* be a value in
 *              [DGC_ALICE_BRAKE_MINPOS,DGC_ALICE_BRAKE_MAXPOS]
 *
 * Returns:     FALSE - desired position is invalid or serial error
 *              TRUE - position was sent over serial channel
 *
 * Known Bugs:  None.
 **************************************************************************************/
int brake_setposition(double position)
{
  //  cout<<"Starting brake_setposition"<<endl;
 
    if (position > 1) position = 1; 
    if (position < 0) position = 0;
    position = position * DGC_ALICE_BRAKE_MAXPOS;
    brake_position_string[0] = brake_command;
    if(position < 10) {
      brake_position_string[1] = '0';
      sprintf(brake_position_string+2, "%d\n", (int) position);
    }
    else {
      sprintf(brake_position_string+1, "%d\n", (int) position);
    }

    // if (
    //      serial_write(brake_serial_port, &brake_command, 1, SerialBlock) != 1 //||
    //            serial_write(brake_serial_port, (char*) brake_position_string, strlen(brake_position_string), SerialBlock) != 1
    //       )
    //{
    //return TRUE;  // debugging 21 Jan 2005, should be "return FALSE"
    //}


    if(brakeport->write((char*) brake_position_string) == -1)
      {
	//	cout<<"Brake command write failed in brake setposition"<<endl;
	return FALSE;
      }
    return TRUE;
}

/*!
 * brake_getposition (void)
 * 
 * Author:      Will Coulter
 * Revision:    Jeff Lamb (JCL) 20 Jan 2005
 *              Modified to receive position as a 2 character string
 *
 * Description: Returns the current position of the braking actuator.
 *
 * Arguments:   None.
 *
 * Returns:     If the integer returned is in the allowable range, it is
 *              returned.  Otherwise, a negative number is returned.
 *
 * Known Bugs:  None.
 **************************************************************************************/
double brake_getposition(void)
{
  brakeport->clean();
  //cout<<"Starting brake getposition"<<endl;
    /* Try sending command to get position */
    //if (serial_write(brake_serial_port, &brake_read, 1, SerialBlock) != 1)
    //{
  //  return -1;
  //}
  //printf("brake read: %s", brake_read);

  if(brakeport->write((char*) brake_read, 2) == -1)
    {
      //      cout<<"brake write in brake_getposition failed"<<endl;
      return -1;
    }

  usleep(30000);

    /* Try actually retrieving position */
    //if (serial_read(brake_serial_port, (char *) brake_position_string, 3) != 1) {

  if(brakeport->read(3, (char*) brake_position_string, 70000) == -1)
    {
        /* The microcontroller or computer may be busy, so wait and try again.
           Yes, this is the poor man's read-with-timeout solution */
        usleep(DGC_ALICE_BRAKE_MAXDELAY);
        //if (serial_read(brake_serial_port, (char *) brake_position_string, 3) != 1) {
	if(brakeport->read(3, (char*) brake_position_string, 70000) == -1)
	  {
	    //cout<<"Brake read in brake getposition failed"<<endl;
	  /* now we have really failed, so return */
          //printf("brake string: %s", brake_position_string);  
	  return -1;
	  }
    }
  //    printf("brake string11111111: %s", brake_position_string);

    //getchar();
    /* Do some validation */
   
    //if (
    //        atoi(brake_position_string) > DGC_ALICE_BRAKE_MAXPOS ||
    //        atoi(brake_position_string) < DGC_ALICE_BRAKE_MINPOS
    //   )
    //{
    //    return -1;
    //}
    temp = atoi(brake_position_string);
    brake_got_position = temp / DGC_ALICE_BRAKE_MAXPOS;
    //    cout<<"brake getposition executed correctly"<<endl;
    return (double) brake_got_position;
}

/*!
 * brake_getpressure (void)
 * 
 * Author:      David Rosen
 * 
 * Description: Returns the current pressure in the brake line.
 *
 * Arguments:   None.
 *
 * Returns:     On a successful read, returns a positive integer representing 
 *              the pressure read by a pressure transducer connected to the 
 *              brake line; possible values are 0-255, although it should normal                
 *              ly be in the 15-200 range.  Returns -1 on an error.
 *  
 * 
 * Tully  2/27/05
 * Changes:     I have adjusted this function to work on a 0-1 scale as that is what we
 *              the rest of the vehicle is standardized to.  
 *              The normal range is now .058 to .78 
 *
 * Known Bugs:  None.
 **************************************************************************************/
double brake_getpressure()
{
  int bytesRead;
  brakeport->clean();

  //Send request for brake pressure
  if(brakeport->write(brake_pressure_read, 2) == -1) //Error in write
    {
      return -1;
    }

  usleep(30000);

  if((bytesRead = brakeport->read(3, brake_pressure_string, (int)(1E6))) < 3) //We didn't get all of the transmission, so clean out the buffer of any leftover junk and return an error.
    {
      brakeport->clean();
      return -1;
    }

  // cout<<"Brake read returned "<<brake_pressure_string<<endl;

  //If we've made it this far, we have the pressure data, so extract from the buffer and return
  double pressure = (double) atoi(brake_pressure_string)/255;
  //  printf("%f is the pressure", pressure);
  return pressure;
}


/*!
 * brake_pause (void)
 * 
 * Author:      Will Coulter
 *
 * Description: Commands full brake actuation.  Does not check to see that the
 *              command was executed.  This is just a wrapper around
 *              brake_setposition
 *
 * Arguments:   None.
 *
 * Returns:     FALSE - Serial error of some kind.
 *              TRUE - Command was sent over the serial channel.
 *
 * Known Bugs:  None.
 **************************************************************************************/
int brake_pause(void)
{
  return brake_setposition(1);
}

/*!
 * brake_disable (void)
 * 
 * Author:      Will Coulter
 *
 * Description: Wrapper for brake_pause (void).
 **************************************************************************************/
int brake_disable(void)
{
    return brake_setposition(1);
}

/*!
 * brake_off (void)
 * 
 * Author:      Will Coulter
 *
 * Description: Turns the brake off, i.e. let's the vehicle roll freely.  Does
 *              not check to see that the command was executed.  Yet another
 *              wrapper for brake_setposition
 **************************************************************************************/
int brake_off(void)
{
    return brake_setposition(0);
}




