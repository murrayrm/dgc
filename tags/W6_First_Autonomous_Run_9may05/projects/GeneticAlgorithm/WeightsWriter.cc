/**
 * $Id$
 */
#include "WeightsWriter.hh"
#include "Arbiter2/ArbiterDatum.hh" // ArbiterInput::Count
#include "Arbiter2/IO.hh" // IO::readWeightsLog
#include "MTA/Kernel.hh" // timestamp info

#define DEBUG false // set to true for debug printf's

WeightsWriter::WeightsWriter()
{
  // create a log of the state and arbiter commands
  time_t t = time(NULL);
  tm *local;
  local = localtime(&t);

  char timestamp[255];
  sprintf(timestamp, "%02d%02d%04d_%02d%02d%02d",
          local->tm_mon+1, local->tm_mday, local->tm_year+1900,
          local->tm_hour, local->tm_min, local->tm_sec);
  
  char filename[255];
  sprintf(filename, "weights/weights_%s.log", timestamp);

  printf("WeightsWriter: Logging weights to file: %s\n", filename);
  weights_file = fopen(filename,"w");
  if ( weights_file == NULL )
  {
    printf("Unable to open log file '%s'\n", filename);
    exit(-1);
  }

  // save the filename for later use
  //weights_filename = new char[255];
  strcpy(weights_filename, filename);
  
  // print data format information in a header at the top of the file
  fprintf(weights_file, "%| LADAR| Stereo |StereoLR| Global ");
  fprintf(weights_file, "|   DFE   |CorArcEv|FenderPl|RoadFoll|PathEval|\n" );
}

void WeightsWriter::writeWeights(double weights[])
{
  printf("Writing weights:\n");
  for(int i = 0; i < ArbiterInput::Count; i++)
  {
    fprintf(weights_file, "%f ", weights[i]);
    printf("%f ", weights[i]);
  }
  fprintf(weights_file, "\n");
  printf("\n");
  fflush(weights_file);
}

void WeightsWriter::writeAverageWeights(char* weights_filename)
{
  char weights_average_filename[255]; // output log file name
  sprintf(weights_average_filename, "%s_average", weights_filename);
  printf("WeightsWriter: Writing average weights to file: %s\n", weights_average_filename);
  FILE* averagefile = fopen(weights_average_filename, "w");
  if( averagefile == NULL)
  {
    printf("Unable to open average log file '%s'\n", weights_average_filename);
    exit(-1);
  }  
  // print data format information in a header at the top of the file
  fprintf(averagefile, "%| LADAR| Stereo |StereoLR| Global ");
  fprintf(averagefile, "|   DFE   |CorArcEv|FenderPl|RoadFoll|PathEval|\n" );
          
  // parse all columns into vectors
  vector<deque<double> > weights = IO::readWeightsLog(weights_filename);
  
  if(DEBUG) printf(" weights columns: %d\n", weights.size());
  if(DEBUG) printf(" rows in each column: %d\n", weights.front().size());
  
  
  // calculate average for each column:
  for(vector<deque<double> >::const_iterator i = weights.begin(); i != weights.end(); i++)
  {
    double sum = 0;
    for(deque<double>::const_iterator j = (*i).begin(); j != (*i).end(); j++)
    {
      sum += (*j);
    }
    if(DEBUG) printf(" \n\n sum = %f, weights size = %d, average = %f\n\n", sum, weights.size(), sum/(double)weights.front().size());
    fprintf(averagefile, "%f ", sum/(double)weights.front().size());
  }
  
}
