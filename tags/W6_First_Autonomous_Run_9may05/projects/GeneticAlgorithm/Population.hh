#ifndef POPULATION_HH
#define POPULATION_HH
#include "Individual.hh"
#include <vector>
#include <utility> // pair
using namespace std;

/** The Population class handles a collection of Individual objects. It handles
 * selection, insertion of new individuals and keeps track of the individual 
 * which has the highest fitness. It also evaluates all the individuals upon 
 * request. 
 * $Id$
 */
class Population
{
public:
  /**
   * Creates a population that can contain Individual objects
   */
  Population();

  /**
   * Destructor for the population. Deletes all the Individual objects
   */
  ~Population();

  /**
   * Calculates fitness on all individuals
   */
  void evaluate();

  /**
   * Returns a new Individual, selected with probability proportional 
   * to the fitness compared with the total fitness of the population.
   * @return a individual, selected according to the rules.
   */
  Individual* select();

  /**
   * Adds an Individual to this population 
   * @param i the individual to be added
   */
  void add(Individual* i);

  /**
   * Returns the current maximum fitness of the population.
   * @return the fitness value
   */
  double getMaxFitness();

  /**
   * Returns a pointer to the fittest individual. 
   * @return the individual
   */
  Individual* getFittestIndividual();

private:
  /** the individuals */
  vector<Individual*> generation;
  /** A vector of range pairs used for the selection rules */
  vector<pair<double, double> > selectRange;
  /** the number of individuals in the population */
  int popSize;
  /** the current maximum fit value generated from an individual */
  double maxFitness; 
  /** the index in the vector of the fittest individual */
  int fittestIndex;
  /** just a check so that the population is evaluated before doing select */
  bool isEvaluated;
};

#endif


