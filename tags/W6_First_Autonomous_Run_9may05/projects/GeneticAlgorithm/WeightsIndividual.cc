/**
 * $Id$
 */
#include "WeightsIndividual.hh"
#include "GeneticAlgorithm.hh" // my_rand, my_srand
#include <iostream> // cout
#include <iomanip> // setw
#include <list>
#include <vector>
#include <cmath> // fabs
#include "Arbiter2/VoterHandler.hh"
#include "Arbiter2/Voter.hh" // Voter list
#include "Arbiter2/ArbiterDatum.hh" // ArbiterInputStrings
using namespace std;

// Global classes
extern list<Voter> voters;
extern vector<bool> enabled_voters;
extern VoterHandler vHandler;
extern VDrive_CmdMotionMsg humanCmd;

WeightsIndividual::WeightsIndividual()
{
  for(int i = 0 ; i < ArbiterInput::Count ; i++)
  {
    if(enabled_voters[i])
    {
      w[i] = randomValue();
    }
    else
    {
      w[i] = 0.0; // disabled voter
    }
  }
}

void WeightsIndividual::evaluate()
{
  // apply the current weights to the voters, set to 0.0 if the voter isn't
  // enabled (from the settings read from the config file the user specifies).
  list<Voter>::iterator x = voters.begin();
  for(int i = 0 ; i < ArbiterInput::Count ; i++)
  // this should be safe, we shouldn't get outside the voter list (same size)
  {
      (*x++).VoterWeight = w[i];
  }

  // call arbiter and see what the result is from this voting
  VDrive_CmdMotionMsg arbiterCmd = vHandler.calculateCommand(voters, lastHumanPhi);
  
  // the human driving phi must be discretized to an arbiter arc angle
  // OBSERVE: the steer_cmd must be multiplied with MAX_STEER to become the
  //          actual steering angle.
  double humanPhi = GetPhi(GetArcNum(humanCmd.steer_cmd * MAX_STEER));
  lastHumanPhi = humanPhi;
  
  // compare the two and decide fitness
  double diff = fabs(arbiterCmd.steer_cmd - humanPhi);
  //cout << "arbiter/human phi: " << arbiterCmd.steer_cmd << "/" << humanPhi << endl;
  if(diff == 0)
  {
    fitness = 1e25; // just a very large value
  }
  else
  {  
    fitness = 1 / diff;
  }
  isEvaluated = true;
}

double WeightsIndividual::getFitness() const
{
  if(!isEvaluated)
  {
    cout << "ERROR: you can't getFitness() before you've done evaluate() on this individual!" << endl;
    exit(1);
  }
  return fitness;
}


Individual* WeightsIndividual::clone()
{
  WeightsIndividual* clone = new WeightsIndividual();
  for(int i = 0 ; i < ArbiterInput::Count ; i++) 
  {
    clone->w[i] = w[i];
  }
  return clone;
}

void WeightsIndividual::mutate()
{
  isEvaluated = false;
  int mutateIndex = my_rand() % ArbiterInput::Count; // pick a weight to mutate
  double factor = 2.0 * (my_rand()/(double)MY_RAND_MAX) - 1.0; // a factor between -1.0 and 1.0
  w[mutateIndex] += factor * WEIGHT_MUTATE_MAX;
  // keep it within the limits and set to 0 if disabled voter:
  if(w[mutateIndex] < 0.0 || !enabled_voters[mutateIndex])
  {
    w[mutateIndex] = 0.0; // normalized or disabled voter
  }
  else if(w[mutateIndex] > 1.0) // cap if it got too high
  {
    w[mutateIndex] = 1.0;
  }
}

void WeightsIndividual::crossOver(Individual* theOtherParent)
{
  isEvaluated = false;
  int limit = my_rand() % ArbiterInput::Count;
  bool upper = my_rand() % 2 == 0;
  WeightsIndividual* otherParent = static_cast<WeightsIndividual*>(theOtherParent);
  for(int i = 0 ; i < ArbiterInput::Count ; i++)
  {
    if(upper)
    {
      if(i > limit)
        w[i] = otherParent->w[i];
    }
    else
    {
      if(i < limit)
        w[i] = otherParent->w[i];
    }
    if(!enabled_voters[i]) // disabled voters shall always have 0 weights
    {
      w[i] = 0.0;
    }
  }
}

double WeightsIndividual::randomValue()
{
  // return a value between 0 and 1
  return my_rand() / (double)MY_RAND_MAX;
}

void WeightsIndividual::print()
{
  cout << "Weights are:" << endl
       << "============" << endl;
  for(int i = 0 ; i < ArbiterInput::Count; i++)
  {
    cout << setw(12) << ArbiterInputStrings[i] << w[i] << endl;
  }
  cout << endl;
}


