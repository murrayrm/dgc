# DESCRIPTION
#
# This is the projects-level Makefile for the Caltech DGC team project.
#
#

# Directories for active projects (must include trailing slash).
MODULEHELPERS = ModuleHelpers/

CTRAJ         = traj/
CTRAJCTRLR    = trajController/
PATHFOLLOWER  = PathFollower/
NEWMAT        = Newmat/
CCPAINTER     = CCorridorPainter/
FUSIONMAPPER  = sn_FusionMapper/
PLANNER       = planner/
LADARFEEDER   = LadarFeeder/
STEREOFEEDER  = StereoFeeder/

SKYNET        = skynet/
CCOSTPAINTER  = CCostPainter/
MAPDISPLAY    = MapDisplay/
MODEMAN       = ModeMan/
MODEMANMODULE = ModeManModule/
PID_CONTROLLER = PID_Controller/
PID_PATHFOLLOWER = PID_PathFollower/
RDDFPATHGEN   = RddfPathGen/
PLANNERMODULE = PlannerModule/
CPID          = Cpid/
SN_TRAJFOLLOWER = sn_TrajFollower/
#REACTIVE     = ReactivePlanner/
#SKYNETGUI    = SkyNetGUI/

#=================================================================
#
# build targets
#

.PHONY: default

default: install \
         $(CTRAJ) $(SKYNET) $(MODULEHELPERS) \
         $(CTRAJCTRLR) $(CPID) \
         $(PID_CONTROLLER) $(SN_TRAJFOLLOWER) \
         $(CCPAINTER) $(CCOSTPAINTER) $(FUSIONMAPPER) \
         $(PLANNER) $(PLANNERMODULE) \
         $(MODEMAN) $(MODEMANMODULE) $(RDDFPATHGEN)
         #$(LADARFEEDER) $(NEWMAT) $(PATHFOLLOWER) \
         #$(STEREOFEEDER) $(PID_PATHFOLLOWER) 
         #$(MAPDISPLAY) should be made on a laptop and be used. Slackware
         #stuff is too complicated for the utility.
	##
	## 'make install' already done by default.
	##

$(MODULEHELPERS): FORCE
	cd $(@D); $(MAKE)

$(SKYNET): FORCE
	cd $(@D); $(MAKE)

$(CPID): FORCE
	cd $(@D); $(MAKE)

$(CTRAJ): FORCE
	cd $(@D); $(MAKE)

$(CTRAJCTRLR): FORCE
	cd $(@D); $(MAKE)

$(PATHFOLLOWER): FORCE
	cd $(@D); $(MAKE)

$(NEWMAT): FORCE
	cd $(@D); $(MAKE)

$(CCPAINTER): FORCE
	cd $(@D); $(MAKE)

$(PLANNER): FORCE
	cd $(@D); $(MAKE)

$(FUSIONMAPPER): FORCE
	cd $(@D); $(MAKE)

$(LADARFEEDER): FORCE
	cd $(@D); $(MAKE)

$(STEREOFEEDER): FORCE
#	cd $(@D); $(MAKE)

$(CCOSTPAINTER): FORCE
	cd $(@D); $(MAKE)

$(MAPDISPLAY): FORCE
	cd $(@D); $(MAKE)

$(MODEMAN): FORCE
	cd $(@D); $(MAKE)

$(MODEMANMODULE): FORCE
	cd $(@D); $(MAKE)

$(PID_CONTROLLER): FORCE
	cd $(@D); $(MAKE)

$(PID_PATHFOLLOWER): FORCE
	cd $(@D); $(MAKE)

$(RDDFPATHGEN): FORCE
	cd $(@D); $(MAKE)

$(PLANNERMODULE): FORCE
	cd $(@D); $(MAKE)

$(SN_TRAJFOLLOWER): FORCE
	cd $(@D); $(MAKE)

#=================================================================
#
# install targets
#

install: $(CTRAJ).install $(SKYNET).install $(MODULEHELPERS).install \
         $(NEWMAT).install\
         $(CPID).install $(CTRAJCTRLR).install \
         $(PID_CONTROLLER).install $(SN_TRAJFOLLOWER).install \
         $(MODEMAN).install  $(MODEMANMODULE).install \
         $(RDDFPATHGEN).install $(PLANNER).install \
         $(CCPAINTER).install $(CCOSTPAINTER).install \
         $(PLANNERMODULE).install $(FUSIONMAPPER).install \
         #$(LADARFEEDER).install \
         #$(STEREOFEEDER).install \         #$(PATHFOLLOWER).install \
         #$(PID_PATHFOLLOWER).install \
         #$(MAPDISPLAY).install should be made on a laptop and be used. 
         #Slackware stuff is too complicated for the utility.

$(MODULEHELPERS).install: FORCE
	cd $(@D); $(MAKE) install

$(SKYNET).install: FORCE
	cd $(@D); $(MAKE) install

$(CPID).install: $(CTRAJ) FORCE
	cd $(@D); $(MAKE) install

$(CTRAJ).install: $(CTRAJ) FORCE
	cd $(@D); $(MAKE) install

$(CTRAJCTRLR).install: $(CTRAJCTRLR) FORCE
	cd $(@D); $(MAKE) install

$(PATHFOLLOWER).install: $(PATHFOLLOWER) FORCE
	cd $(@D); $(MAKE) install

$(NEWMAT).install: $(NEWMAT) FORCE
	cd $(@D); $(MAKE) install

$(CCPAINTER).install: FORCE
	cd $(@D); $(MAKE) install

$(PLANNER).install: FORCE
	cd $(@D); $(MAKE) install

$(FUSIONMAPPER).install: FORCE
#	cd $(@D); $(MAKE) install

$(LADARFEEDER).install: FORCE
#	cd $(@D); $(MAKE) install

$(STEREOFEEDER).install: FORCE
#	cd $(@D); $(MAKE) install

$(CCOSTPAINTER).install: FORCE
	cd $(@D); $(MAKE) install

$(MAPDISPLAY).install: FORCE
	cd $(@D); $(MAKE) install

$(MODEMAN).install: FORCE
	cd $(@D); $(MAKE) install

$(MODEMANMODULE).install: FORCE
	cd $(@D); $(MAKE) install

$(PID_CONTROLLER).install: FORCE
	cd $(@D); $(MAKE) install

$(PID_PATHFOLLOWER).install: FORCE
#	cd $(@D); $(MAKE) install

$(RDDFPATHGEN).install: FORCE
#	cd $(@D); $(MAKE) install

$(PLANNERMODULE).install: FORCE
	cd $(@D); $(MAKE) install

$(SN_TRAJFOLLOWER).install: FORCE
	cd $(@D); $(MAKE) install


#=================================================================
#
# clean targets
#

.PHONY: clean

clean: $(SKYNET).clean $(MODULEHELPERS).clean \
       $(CTRAJ).clean $(CTRAJCTRLR).clean $(PATHFOLLOWER).clean \
       $(PID_CONTROLLER).clean $(PID_PATHFOLLOWER).clean $(RDDFPATHGEN).clean \
       $(PLANNERMODULE).clean $(FUSIONMAPPER).clean $(PLANNER).clean \
       $(CCOSTPAINTER).clean $(CCPAINTER).clean $(MAPDISPLAY).clean \
       $(MODEMAN).clean $(MODEMANMODULE).clean \
       $(LADARFEEDER).clean $(STEREOFEEDER).clean \
       $(SN_TRAJFOLLOWER).clean
       #$(NEWMAT).clean 

$(MODULEHELPERS).clean: FORCE
	cd $(@D); $(MAKE) clean

$(SKYNET).clean: FORCE
	cd $(@D); $(MAKE) clean

$(CTRAJ).clean: FORCE
	cd $(@D); $(MAKE) clean

$(CTRAJCTRLR).clean: FORCE
	cd $(@D); $(MAKE) clean

$(PATHFOLLOWER).clean: FORCE
	cd $(@D); $(MAKE) clean

$(NEWMAT).clean: FORCE
	cd $(@D); $(MAKE) clean

$(CCPAINTER).clean: FORCE
	cd $(@D); $(MAKE) clean

$(PLANNER).clean: FORCE
	cd $(@D); $(MAKE) clean

$(FUSIONMAPPER).clean: FORCE
	cd $(@D); $(MAKE) clean

$(LADARFEEDER).clean: FORCE
	cd $(@D); $(MAKE) clean

$(STEREOFEEDER).clean: FORCE
#	cd $(@D); $(MAKE) clean

$(CCOSTPAINTER).clean: FORCE
	cd $(@D); $(MAKE) clean

$(MAPDISPLAY).clean: FORCE
	cd $(@D); $(MAKE) clean

$(MODEMAN).clean: FORCE
	cd $(@D); $(MAKE) clean

$(MODEMANMODULE).clean: FORCE
	cd $(@D); $(MAKE) clean

$(PID_CONTROLLER).clean: FORCE
	cd $(@D); $(MAKE) clean

$(PID_PATHFOLLOWER).clean: FORCE
	cd $(@D); $(MAKE) clean

$(RDDFPATHGEN).clean: FORCE
	cd $(@D); $(MAKE) clean

$(PLANNERMODULE).clean: FORCE
	cd $(@D); $(MAKE) clean

$(SN_TRAJFOLLOWER).clean: FORCE
	cd $(@D); $(MAKE) clean

#=================================================================
#
# help
#

help:
	@echo ""
	@echo "This Makefile builds the DGC common software distribution."
	@echo "The following make targets are supported:"
	@echo "  default - Projects level build."
	@ECHO "  install - Projects level install."
	@echo "  clean   - Removes compilation byproducts."
	@echo "  help    - This information."
	@echo ""


FORCE:



