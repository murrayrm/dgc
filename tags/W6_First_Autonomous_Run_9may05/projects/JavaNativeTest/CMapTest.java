import java.awt.*;
import java.awt.image.*;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import javax.swing.JFrame;

import net.java.games.jogl.*;

/**
 * This class was intended to draw the CMap originally but right now it's
 * at a very eraly stadium, just loading an image from file and displays it
 * and draws a little polygon on top of it. Mostly to demonstrate JOGL
 *
 * @author Henrik Kjellander (henrik.kjellander@home.se)
 */
public class CMapTest extends JFrame implements GLEventListener {
    
    public static final Dimension PREFERRED_FRAME_SIZE =
        new Dimension (450,375);
    
    byte[] cmapRGBA;
    int cmapHeight;
    int cmapWidth;
    
    public CMapTest() {
        // init Frame
        super ("CMap display with JOGL");
        System.out.println ("constructor");
        // get a GLCanvas
        GLCapabilities capabilities = new GLCapabilities();
        GLCanvas canvas =
            GLDrawableFactory.getFactory().createGLCanvas(capabilities);
        // add a GLEventListener, which will get called when the
        // canvas is resized or needs a repaint
        canvas.addGLEventListener(this);
        // now add the canvas to the Frame.  Note we use BorderLayout.CENTER
        // to make the canvas stretch to fill the container (ie, the frame)
        getContentPane().add (canvas, BorderLayout.CENTER);
    }
    
    /** We'd like to be 450x375, please.
     */
    public Dimension getPreferredSize () {
        return PREFERRED_FRAME_SIZE;
    }
        
    /*
     * METHODS DEFINED BY GLEventListener
     */
    
    /** Called by drawable to initiate drawing 
     */
    public void display (GLDrawable drawable) {
        long inTime = System.currentTimeMillis();
        System.out.println ("display()");
        GL gl = drawable.getGL();
        GLU glu = drawable.getGLU();
        gl.glClear (GL.GL_COLOR_BUFFER_BIT);
        drawImage (gl);
        drawOpenPoly(gl);
    }
    
    /** Called by drawable to indicate mode or device has changed
     */
    public void displayChanged (GLDrawable drawable,
                                boolean modeChanged,
                                boolean deviceChanged) {
        System.out.println ("displayChanged()");
    }
    
    /** Called after OpenGL is init'ed
     */
    public void init (GLDrawable drawable) {
        System.out.println ("init()");
        
        GL gl = drawable.getGL(); 
        // set erase color
        gl.glClearColor( 1.0f, 1.0f, 1.0f, 1.0f ); //white 
        // set drawing color and point size
        gl.glColor3f( 0.0f, 0.0f, 0.0f ); 
    }
    
    /** Called to indicate the drawing surface has been moved and/or resized
     */
    public void reshape (GLDrawable drawable,
                         int x,
                         int y,
                         int width,
                         int height) {
        System.out.println ("reshape()");
        GL gl = drawable.getGL(); 
        GLU glu = drawable.getGLU(); 
        gl.glViewport( 0, 0, width, height ); 
        gl.glMatrixMode( GL.GL_PROJECTION );  
        gl.glLoadIdentity(); 
        glu.gluOrtho2D( 0.0, 450.0, 0.0, 375.0); 
    }
    
    /*
     * OUR HELPER METHODS
     */
    
    /** An example of drawing an open polyline.  The
        last point does NOT connect back to the first
    */
    protected void drawOpenPoly (GL gl) {
        gl.glBegin (GL.GL_LINE_STRIP);
        gl.glVertex2i (100, 200);
        gl.glVertex2i (150, 250);
        gl.glVertex2i (100, 250);
        gl.glVertex2i (150, 200);
        gl.glEnd();    
    }
    
    /** Loads "duke_wave.gif" and renders it into GL space
     */
    protected void drawImage (GL gl) {
        // load map if necessary
        if (cmapRGBA == null)
            loadCMap();
        
        // draw bytes into OpenGL
        gl.glRasterPos2i (0, 0);
        gl.glDrawPixels (cmapWidth, cmapHeight, gl.GL_RGBA, gl.GL_UNSIGNED_BYTE, cmapRGBA);
    }
    
    
    protected void loadCMap () {
        Image i = Toolkit.getDefaultToolkit().createImage ("image.gif");
        MediaTracker tracker = new MediaTracker(new Canvas());
        tracker.addImage (i, 0);
        try {
            tracker.waitForAll();
        } catch (InterruptedException ie) {}
        cmapHeight = i.getHeight(null);
        cmapWidth = i.getWidth(null);
        System.out.println ("Got image, width=" +
                            cmapWidth + ", height=" + cmapHeight);
        
        // turn it into a BufferedImage
        WritableRaster raster = 
            Raster.createInterleavedRaster (DataBuffer.TYPE_BYTE,
                                            cmapWidth,
                                            cmapHeight,
                                            4,
                                            null);
        ComponentColorModel colorModel=
            new ComponentColorModel (ColorSpace.getInstance(ColorSpace.CS_sRGB),
                                     new int[] {8,8,8,8},
                                     true,
                                     false,
                                     ComponentColorModel.TRANSLUCENT,
                                     DataBuffer.TYPE_BYTE);            
        BufferedImage cmapImage = 
            new BufferedImage (colorModel, // color model
                               raster,
                               false, // isRasterPremultiplied
                               null); // properties                               
        Graphics2D g = cmapImage.createGraphics();
        // use an AffineTransformation to draw upside-down in the java
        // sense, which will make it right-side-up in OpenGL
        AffineTransform gt = new AffineTransform();
        gt.translate (0, cmapHeight);
        gt.scale (1, -1d);
        g.transform (gt);
        g.drawImage (i, null, null);
        // now retrieve the underlying byte array from dukeImg
        DataBufferByte cmapBuf = (DataBufferByte)raster.getDataBuffer();
        cmapRGBA = cmapBuf.getData();
        g.dispose();
    }
    
    
    /**
     * Returns the cmap
     */
    byte[] getCMap() {
    	// if the image is still not loaded, let's just sleep a little then try again
    	while(cmapRGBA == null) {
    		try {
    			System.out.println("waiting for image to load...");
    			Thread.sleep(1000);
    		}
    		catch(InterruptedException ie) {}
    	}
        return cmapRGBA;
    }
    
    /*
     * Native methods for updating the CMap
     */
    /**
     * Tests to change the cmap image
     */
    public native void test(byte arr[]);
    static 
    {
        System.loadLibrary("CMapTestImp"); 
    }
}

