//Constants for Alice:

// state sensor port definitions
// NOTE:  These port definitions are for Magnesium, not titanium
#define GPS_SERIAL_PORT 0   /* serial port */

// definitions for canceling out pitch/roll of imu mounting
const double IMU_PITCH_OFFSET = 0; // radians
const double IMU_ROLL_OFFSET = .01; // rad
const double IMU_YAW_OFFSET = 0; // rad
// // note these offsets are backwards in sign from the usual convention
// // because of the way the transformation is done
// FROM IMU TO VEHICLE ORIGIN
const double IMU_X_OFFSET = -1.73; 	// pos forward?
const double IMU_Y_OFFSET = .84;    	// pos right?
const double IMU_Z_OFFSET = 0; 		// pos down?
//
// // Offset from IMU to GPS:
const double GPS_X_OFFSET = .51; 	// pos forward?
const double GPS_Y_OFFSET = .84; 	// pos right?
const double GPS_Z_OFFSET = -1.62; 	// pos down?
//
const double EARTH_RADIUS = 6378 * 1000;
const double ERROR_FALLOFF = .75;
const double ERROR_WEIGHT = 2;
const double ANGLE_CUTOFF = M_PI/6;
