function result = plotVehicle(vehicle)
    hold_status = ishold;    
    v_to_g_rot_matrix = getRotMatrix(vehicle.orientation(1), vehicle.orientation(2), vehicle.orientation(3));
    vehicle_origin = [vehicle.position(1), vehicle.position(2), vehicle.position(3)]';
    bottom_back_corner = vehicle_origin+v_to_g_rot_matrix*[-4 0 0]';
    top_front_corner = vehicle_origin+v_to_g_rot_matrix*[0 0 -3]';
    top_back_corner = vehicle_origin+v_to_g_rot_matrix*[-4 0 -3]';
    plot(vehicle.position(1), vehicle.position(3), 'o');  
    hold on;
    plot([bottom_back_corner(1) vehicle_origin(1)], [bottom_back_corner(3) vehicle_origin(3)]);
    plot([bottom_back_corner(1) top_back_corner(1)], [bottom_back_corner(3) top_back_corner(3)]);
    plot([top_front_corner(1) vehicle_origin(1)], [top_front_corner(3) vehicle_origin(3)]);
    plot([top_front_corner(1) top_back_corner(1)], [top_front_corner(3) top_back_corner(3)]);
    for i=1:length(vehicle.sensors)
        plotSensor(vehicle.sensors{i}, vehicle);
    end
    xlabel('X-axis (m)');
    ylabel('Z-axis (m)');
    title('Side-View');
    view(0,-90);
    resetHoldStatus(hold_status);