//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_Random.hh"

//----------------------------------------------------------------------------

// PLATFORM DEPENDENT !!!

// generate random number uniformly distributed between [0.0, 1.0)

double uniform_CR_Random()
{
  // windows

  //  return (double) rand() / (double) RAND_MAX;

  // linux

  return drand48();
  //  CR_error("no uniform_CR_Random()");
  //  return 0;
}

//----------------------------------------------------------------------------

// PLATFORM DEPENDENT !!!

// initialize random number generator

void initialize_CR_Random()
{
  // windows

  /*
  srand((unsigned) CR_itime());

  // first number always seems the same, so let's get it out of the way

  int temp = ranged_uniform_int_CR_Random(0, 100);
  */

  //  CR_error("no initialize_CR_Random()");

  // works on hammer

  //#ifndef METEOR

  struct timeval tp;

  gettimeofday(&tp, NULL); 
  srand48(tp.tv_sec);

  //#endif
  /*
#ifdef METEOR
  srand48(100);
#endif
*/
}

//----------------------------------------------------------------------------

// random sample taken from Gaussian distribution defined by mean, variance
 
double normal_sample_CR_Random(double mean, double variance)
{
  double unit, samp;

  unit = normal_CR_Random();
  // old way--wrong
  //  samp = mean + variance * unit;
  samp = mean + sqrt(variance) * unit;
  
  return samp;
}

//----------------------------------------------------------------------------

// evaluate Gaussian

#define INV_SQRT_TWOPI    0.3989422804

// result = (1 / sqrt(2 * PI * variance)) * exp(-0.5 * (x - mean)^2 / variance)

double gaussian_CR_Random(double x, double mean, double variance)
{
  return (INV_SQRT_TWOPI / sqrt(variance)) * exp(-0.5 * (x - mean) * (x - mean) / variance);
}

//----------------------------------------------------------------------------

// returns TRUE with probability prob; FALSE with probability 1 - prob

int probability_CR_Random(double prob)
{
  return uniform_CR_Random() <= prob;
}

//----------------------------------------------------------------------------

// returns TRUE half the time; FALSE half the time
// equivalent to probability_CR_Random(0.5)

int coin_flip_CR_Random()
{
  return uniform_CR_Random() <= 0.5;
  //  return uniform_CR_Random() >= 0.5;
}

//----------------------------------------------------------------------------

double CR_round(double x)
{
  if (x - floor(x) >= 0.5)
    return ceil(x);
  else
    return floor(x);
}

//----------------------------------------------------------------------------

// random uniform integer between lower and upper, inclusive

int ranged_uniform_int_CR_Random(int lower, int upper)
{
  double result;
  double range_size;

  //  printf("ranged random\n");

  range_size = upper - lower;
  result = range_size * uniform_CR_Random();
  result += lower;

  // round off

  //  CR_error("ranged_uniform_int_CR_Random() calls rint");
  //  return 0;

  //  result = rint(result);
  result = CR_round(result);

  return (int) result;
}

//----------------------------------------------------------------------------

// random number floating point number uniformly distributed between [lower, upper)

double ranged_uniform_CR_Random(double lower, double upper)
{
  double range_size;
  double result;

  range_size = upper - lower;
  result = range_size * uniform_CR_Random();
  result += lower;

  return result;
}

//----------------------------------------------------------------------------

// generate sample from 1-D Gaussian distribution
// with 0 mean and unit variance

// taken straight out of "Numerical Recipes"

double normal_CR_Random()
{
  static int haveOne = FALSE;
  static double heldNum;
  double fac, r, v1, v2;

  if (!haveOne) {
    do {
      v1 = 2 * uniform_CR_Random() - 1;
      v2 = 2 * uniform_CR_Random() - 1;
      r = SQUARE(v1) + SQUARE(v2);
    } while (r >= 1);
    fac = sqrt(-2 * log(r) / r);
    heldNum = v1 * fac;
    haveOne = TRUE;

    return (v2 * fac);
  }
  else {
    haveOne = FALSE;
    return heldNum;
  }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
