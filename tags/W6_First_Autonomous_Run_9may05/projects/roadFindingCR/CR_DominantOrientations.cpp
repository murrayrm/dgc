//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_DominantOrientations.hh"

//----------------------------------------------------------------------------

IplImage        *gabor_max_response_index;           // indices of orientation winners
IplImage        *gabor_max_response_intensity;       // scaled indices for grayscale display
IplImage        *gabor_max_response_value;           // strengths of winners
int              gabor_size;                         // sidelength of standard-size kernel

int gabor_num_orientations            = 36;     // 72 is the old standby
int show_dominant_orientations        = FALSE;
int do_dominant_orientations          = TRUE;

//----------------------------------------------------------------------------

float gabor_start_orientation        = 0.0;   // degrees
float gabor_end_orientation          = 180.0; // degrees
float gabor_delta_orientation;       // (end - start) / num. orientations -- degrees

float gabor_wavelength               = 4.0;   // with gabor_delta_wavelength = 1.0, 4 = 12 x 12 kernel at first level, 8 = 25 x 25, 16 = 50 x 50

int use_fft                           = TRUE;  // if false, use OpenCV convolution instead

//----------------------------------------------------------------------------

// shared FFT variables

IplImage        *image_gray_float;
IplImage        *image_gray_char;
IplImage        *image_gray_float_filtered;
IplImage        *image_gray_char_filtered;

IplImage      ***conv_image;                         // convolutions of input image & kernels in image domain
IplImage       **gabor_response;                     // conv_image_odd^2 + conv_image_even^2
IplImage        *gabor_new_max_mask;

// single-precision FFT variables 

fftwf_complex   *image_fft_float_out;                // input image after FFT
fftwf_plan       image_fft_float_plan;               // plan for source image -> fft_float_out
fftwf_plan       image_fft_float_inv_plan;           // plan for fft_float_out -> source image
float           *image_fft_float_out_sum;            // sum of real and imaginary components (same dimensions as complex image)

float           *image_fft_float_inv_out;            // final image: input convolved with Gabor filter [fftw_malloc iplim?]

fftwf_complex ***gabor_fft_float_out;                // kernels after FFT (image-sized)
float         ***gabor_fft_float_out_sum;            // sum of real and imaginary components (same dimensions as complex image)
float         ***gabor_bank_fft_float;

fftwf_complex ***product_inv_fft_float_in;           // products of input image & kernels in FFT domain
fftwf_plan     **product_inv_fft_float_plan;

// OpenCV variables

float         ***gabor_bank;                         // kernels in standard sizes
CvMat         ***gabor_bank_opencv;                  // kernels in standard sizes, in OpenCV format

//----------------------------------------------------------------------------

// assuming one level of pyramid has been picked

void initialize_dominant_orientations(int w, int h)
{
  int i;

  if (!do_dominant_orientations)
    return;

  gabor_delta_orientation  = (gabor_end_orientation - gabor_start_orientation) / (float) gabor_num_orientations;

  // float fft option

  gabor_bank = (float ***) calloc(gabor_num_orientations, sizeof(float **));
  for (i = 0; i < gabor_num_orientations; i++) 
    gabor_bank[i] = (float **) calloc(2, sizeof(float *));

  gabor_bank_fft_float = make_gabor_bank_fft_float(gabor_wavelength, 
					     gabor_start_orientation, gabor_delta_orientation, gabor_num_orientations,
					     h, w,
					     gabor_bank, &gabor_size);

  initialize_gabor_fft_float(gabor_bank_fft_float, w, h);
  initialize_gabor_opencv(gabor_bank, gabor_size);
}

//----------------------------------------------------------------------------

void initialize_gabor_opencv(float ***bank, int size)
{
  int i, j, k;

  gabor_bank_opencv = (CvMat ***) calloc(gabor_num_orientations, sizeof(CvMat **));
  for (i = 0; i < gabor_num_orientations; i++) {
    gabor_bank_opencv[i] = (CvMat **) calloc(2, sizeof(CvMat *));
    for (j = 0; j < 2; j++) {
      gabor_bank_opencv[i][j] = cvCreateMat(size, size, CV_32FC1);
      for (k = 0; k < size * size; k++) 
	cvmSet(gabor_bank_opencv[i][j], k / size, k % size, bank[i][j][k]);
    }
  }
}

//----------------------------------------------------------------------------

void compute_dominant_orientations(IplImage *im)
{
  if (!do_dominant_orientations)
    return;

  if (use_fft)
    run_gabor_fft_float(im);
  else
    run_gabor_opencv(im);

  find_gabor_max_responses();
  //  filter_out_weak_responses();
}

//----------------------------------------------------------------------------

// makes custom-sized non-square kernel for FFT by embedding conventional
// kernel from make_gabor_kernel_CR_Matrix() (more details in T. S. Lee, 
// "Image Representation Using 2-D Gabor Wavelets", PAMI, Vol. 18, No. 10, 
// October, 1996) in center of black image

// also returns standard-size kernel for opencv

float *make_gabor_kernel_fft_float(float wavelength, float orientation, float even, int rows, int cols, float **kernel, int *size)
{
  int i, j, row, col, myr, myc, rr, cc;
  float r, c, f, u, v;
  CR_Matrix *filt_ga_temp;
  float *filt_ga;

  *size = (int) (15.0 * wavelength / (1.5 * PI));

  filt_ga_temp = make_gabor_kernel_CR_Matrix(wavelength, orientation, even, *size);

  filt_ga = (float *) fftwf_malloc(sizeof(float) * rows * cols);
  for (i = 0; i < rows * cols; i++)
    filt_ga[i] = 0.0;

  // now copy filt_ga_temp into filt_ga just so... (still not sure I'm doing this entirely correctly)

  rr = filt_ga_temp->rows;
  cc = filt_ga_temp->cols;

  for (row = -rr/2; row < rr/2; row++)
    for (col = -cc/2; col < cc/2; col++) {
      myr = row % rows;
      if (myr < 0)
	myr += rows;
      myc = col % cols;
      if (myc < 0)
	myc += cols;
      filt_ga[cols * myr + myc] = MATRC(filt_ga_temp, row + rr/2, col + cc/2);
    }

  *kernel = (float *) malloc(*size * *size * sizeof(float));
  for (i = 0; i < *size * *size; i++)
    (*kernel)[i] = filt_ga_temp->x[i];

  free_CR_Matrix(filt_ga_temp);

  return filt_ga;
}

//----------------------------------------------------------------------------

// just one wavelength

// gabor_bank is num_orientations x 2 array of kernels
// every kernel is the size of the image: rows x cols
 
float ***make_gabor_bank_fft_float(float wavelength,
				   float start_orientation, float orientation_interval, int num_orientations, 
				   int rows, int cols,
				   float ***gabor_bank, int *size)
{
  float ***gabor_bank_fft_float;
  int i, orientation_num, even;
  float orientation;

  gabor_bank_fft_float = (float ***) calloc(gabor_num_orientations, sizeof(float **));
  for (i = 0; i < gabor_num_orientations; i++) 
    gabor_bank_fft_float[i] = (float **) calloc(2, sizeof(float *));

  for (i = 0, orientation = start_orientation, orientation_num = 0; orientation_num < num_orientations; orientation += orientation_interval, orientation_num++) {
    for (even = 0; even <= 1; even++) {
      gabor_bank_fft_float[i][even] = make_gabor_kernel_fft_float(wavelength, orientation, even, rows, cols, &gabor_bank[i][even], size);  
      printf("%i: orientation = %.2lf, even = %i, wavelength = %.2lf (%i x %i)\n", i, orientation, even, wavelength, rows, cols);
    }
    i++;
  }
    
  return gabor_bank_fft_float;
}

//----------------------------------------------------------------------------

// make IPL_DEPTH_32F that is properly aligned for used with FFTW

// another way to do this would involve using cvSetMemoryManager to 
// make fftw_malloc get called for images automatically

IplImage *make_fft_float_IplImage(int w, int h)
{
  IplImage *im;

  im = cvCreateImage(cvSize(w, h), IPL_DEPTH_32F, 1);
  cvFree((void **) &im->imageData);
  im->imageData = (char *) fftwf_malloc(sizeof(float) * w * h);

  return im;
}

//----------------------------------------------------------------------------

void initialize_gabor_fft_float(float ***gabor_bank_fft_float, int w, int h)
{
  int i, j, t, u, v, double_comp_size;
  int fft_width;                    // the limit used for the complex domain
  fftwf_plan **gabor_fft_float_plan;       
  float *g_out;

  fft_width = w / 2 + 1;
  double_comp_size = 2 * h * fft_width;

  // input image

  gabor_max_response_index = cvCreateImage(cvSize(w, h), IPL_DEPTH_8U, 1);
  gabor_max_response_intensity = cvCreateImage(cvSize(w, h), IPL_DEPTH_8U, 1);
  gabor_new_max_mask = cvCreateImage(cvSize(w, h), IPL_DEPTH_8U, 1);
  image_gray_char = cvCreateImage(cvSize(w, h), IPL_DEPTH_8U, 1);
  image_gray_float = make_fft_float_IplImage(w, h);
  image_gray_float_filtered = make_fft_float_IplImage(w, h);
  image_gray_char_filtered = cvCreateImage(cvSize(w, h), IPL_DEPTH_8U, 1);
  image_fft_float_out = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fft_width * h);
  image_fft_float_out_sum = (float *) fftwf_malloc(sizeof(float) * fft_width * h);
  CR_flush_printf("image fft: planning...");
  image_fft_float_plan = fftwf_plan_dft_r2c_2d(h, w, (float *) image_gray_float->imageData, image_fft_float_out, FFTW_PATIENT); 
  image_fft_float_inv_plan = fftwf_plan_dft_c2r_2d(h, w, image_fft_float_out, (float *) image_gray_float->imageData, FFTW_PATIENT); 

  CR_flush_printf("done\n");

  // gabor filters

  gabor_fft_float_out = (fftwf_complex ***) calloc(gabor_num_orientations, sizeof(fftwf_complex **));
  gabor_fft_float_plan = (fftwf_plan **) calloc(gabor_num_orientations, sizeof(fftwf_plan *));
  gabor_fft_float_out_sum = (float ***) calloc(gabor_num_orientations, sizeof(float **));

  // products of gabor filters and image

  product_inv_fft_float_in = (fftwf_complex ***) calloc(gabor_num_orientations, sizeof(fftwf_complex **));
  conv_image = (IplImage ***) calloc(gabor_num_orientations, sizeof(IplImage **));
  gabor_response = (IplImage **) calloc(gabor_num_orientations, sizeof(IplImage *));
  product_inv_fft_float_plan = (fftwf_plan **) calloc(gabor_num_orientations, sizeof(fftwf_plan *));

  for (i = 0; i < gabor_num_orientations; i++) {

    //    printf("preparing ffts for gabor pair %i\n", i);

    gabor_fft_float_out[i] = (fftwf_complex **) calloc(2, sizeof(fftwf_complex *));
    gabor_fft_float_plan[i] = (fftwf_plan *) calloc(2, sizeof(fftwf_plan));
    gabor_fft_float_out_sum[i] = (float **) calloc(2, sizeof(float *));

    product_inv_fft_float_in[i] = (fftwf_complex **) calloc(2, sizeof(fftwf_complex *));
    conv_image[i] = (IplImage **) calloc(2, sizeof(IplImage *));
    gabor_response[i] = make_fft_float_IplImage(w, h);
    product_inv_fft_float_plan[i] = (fftwf_plan *) calloc(2, sizeof(fftwf_plan));

    for (j = 0; j < 2; j++) {

      gabor_fft_float_out[i][j] = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fft_width * h);
      gabor_fft_float_plan[i][j] = fftwf_plan_dft_r2c_2d(h, w, gabor_bank_fft_float[i][j], gabor_fft_float_out[i][j], FFTW_ESTIMATE);
      gabor_fft_float_out_sum[i][j] = (float *) fftwf_malloc(sizeof(float) * fft_width * h);
      fftwf_execute(gabor_fft_float_plan[i][j]);

      g_out = (float *) gabor_fft_float_out[i][j];
      for (t = 0, u = 1, v = 0; t < double_comp_size; t += 2, u += 2) 
	gabor_fft_float_out_sum[i][j][v++] = g_out[t] + g_out[u];

      // don't need these any more

      fftwf_free(gabor_bank_fft_float[i][j]);
      fftwf_free(gabor_fft_float_plan[i][j]);

      // product

      product_inv_fft_float_in[i][j] = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fft_width * h);
      conv_image[i][j] = make_fft_float_IplImage(w, h);
      product_inv_fft_float_plan[i][j] = fftwf_plan_dft_c2r_2d(h, w, product_inv_fft_float_in[i][j], (float *) conv_image[i][j]->imageData, FFTW_PATIENT);  // FFTW_MEASURE
    }

    free(gabor_bank_fft_float[i]);
    free(gabor_fft_float_plan[i]);
  }

  // finish freeing

  free(gabor_bank_fft_float);
  free(gabor_fft_float_plan);

  //  printf("done\n");
}

//----------------------------------------------------------------------------


void run_gabor_opencv(IplImage *source_im)
{
  int i, j;

  cvConvertImage(source_im, image_gray_char);    // color to gray
  cvConvert(image_gray_char, image_gray_float);      

  for (i = 0; i < gabor_num_orientations; i++) {
    for (j = 0; j < 2; j++) 
      cvFilter2D(image_gray_float, conv_image[i][j], gabor_bank_opencv[i][j]); 

    // odd^2 + even^2
    
    cvMul(conv_image[i][0], conv_image[i][0], conv_image[i][0]);
    cvMul(conv_image[i][1], conv_image[i][1], conv_image[i][1]);
    cvAdd(conv_image[i][0], conv_image[i][1], gabor_response[i]);
  }


//       cvConvertScale(conv_image[i][j]>iplim, image_gray_char);
//   write_IplImage("test5.png", image_gray_char);
  //  write_CR_Image("test3.png", image_gray_char_filtered);
      //  exit(1);
}

//----------------------------------------------------------------------------

// results are put in gabor_response array

void run_gabor_fft_float(IplImage *source_im)
{
  int i, j, t, u, v, x, y, r, c;
  int h, w, double_comp_size;
  int fft_width;
  float fft_normalization_factor;

  h = source_im->height;
  w = source_im->width;
  fft_width = w / 2 + 1;
  fft_normalization_factor = 1.0 / (float) (h * w);

  // convert raw image to FFT domain

  cvConvertImage(source_im, image_gray_char);    // color to gray
  cvConvert(image_gray_char, image_gray_float);  
  fftwf_execute(image_fft_float_plan);

  // convolve image with each gabor filter

  float ac, bd;
  float *g_out, *g_inv_in, *im_out;

  double_comp_size = 2 * h * fft_width;

  im_out = (float *) image_fft_float_out;
  for (t = 0, u = 1, v = 0; t < double_comp_size; t += 2, u += 2) 
    image_fft_float_out_sum[v++] = im_out[t] + im_out[u];
  
  for (i = 0; i < gabor_num_orientations; i++) {
    for (j = 0; j < 2; j++) {
	
      g_out = (float *) gabor_fft_float_out[i][j];
      g_inv_in = (float *) product_inv_fft_float_in[i][j];
	
      for (t = 0, u = 1, v = 0; t < double_comp_size; t += 2, u += 2, v++) {
	ac = g_out[t] * im_out[t];
	bd = g_out[u] * im_out[u];
	g_inv_in[t] = ac - bd;
	g_inv_in[u] = gabor_fft_float_out_sum[i][j][v] * image_fft_float_out_sum[v] - ac - bd;
      }

      // invert fft 
      
      fftwf_execute(product_inv_fft_float_plan[i][j]);
    }

    // odd^2 + even^2
    
    cvScale(conv_image[i][0], conv_image[i][0], fft_normalization_factor);
    cvScale(conv_image[i][1], conv_image[i][1], fft_normalization_factor);

    cvMul(conv_image[i][0], conv_image[i][0], conv_image[i][0]);
    cvMul(conv_image[i][1], conv_image[i][1], conv_image[i][1]);
    cvAdd(conv_image[i][0], conv_image[i][1], gabor_response[i]);
  }

  // two other possibilities for complex multiplication:

  // (1) cvMulSpectrums--not exactly sure about complex number format, but theoretically it does what I want
  // (2) this:
  //       cvMul(gabor_real[i][j], im_real, real_prod);
  //       cvMul(gabor_imag[i][j], im_imag, imag_prod);
  //       cvSub(real_prod, imag_prod, real_part);
  //       cvMul(gabor_sum[i][j], im_sum, sum_prod);
  //       cvSub(sum_prod, real_prod, sum_real_diff);
  //       cvSub(sum_real_diff, imag_prod, imag_part);

  // interleave real_part and imag_part into product_inv_fft_float_in
  //   --could use cvMerge with 2-channel float image as destination
}

//----------------------------------------------------------------------------

void find_gabor_max_responses()
{ 
  int i, scale_factor;

  // keep max values in gabor_response[0] to avoid copy
  gabor_max_response_value = gabor_response[0];

  cvSetZero(gabor_max_response_index);

  for (i = 1; i < gabor_num_orientations; i++) {
    cvCmp(gabor_response[i], gabor_max_response_value, gabor_new_max_mask, CV_CMP_GT);      
    cvSet(gabor_max_response_index, cvScalarAll(i), gabor_new_max_mask);                  // for actual indexing
    cvMax(gabor_response[i], gabor_max_response_value, gabor_max_response_value);         // update max. values
  }  

  if (show_dominant_orientations) {
    if (gabor_num_orientations > 1)
      scale_factor = 255 / (gabor_num_orientations - 1);
    else
      scale_factor = 0;
    cvScale(gabor_max_response_index, gabor_max_response_intensity, scale_factor);        // for grayscale display
  }
}

//----------------------------------------------------------------------------

// not using this right now

void filter_out_weak_responses()
{
  CvPoint pmin, pmax;
  double maxval, minval;
  CvScalar avgval;

  cvMinMaxLoc(gabor_max_response_value, &minval, &maxval, &pmin, &pmax);
  avgval = cvAvg(gabor_max_response_value);

  cvCmpS(gabor_max_response_value, avgval.val[0], gabor_new_max_mask, CV_CMP_LT);      
  cvSet(gabor_max_response_index, cvScalarAll(0), gabor_new_max_mask); 
  
  printf("max response = %lf, min = %lf, avg = %lf\n", maxval, minval, avgval.val[0]);
}

//----------------------------------------------------------------------------

void process_dominant_orientations_command_line_flags(int argc, char **argv) 
{
  int i;

  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i],                             "-o")) {
      gabor_num_orientations = atoi(argv[i + 1]);
      if (gabor_num_orientations < 1) {
	printf("must have positive number of orientations\n");
	exit(1);
      }
    }
    if (!strcmp(argv[i],                             "-w"))
      gabor_wavelength = atof(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-nofft"))
      use_fft = FALSE;
    else if (!strcmp(argv[i],                        "-showdom")) 
      show_dominant_orientations = TRUE;
    else if (!strcmp(argv[i],                        "-nodom")) 
      do_dominant_orientations = FALSE;
  }
}

//----------------------------------------------------------------------------


