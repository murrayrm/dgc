//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_CaptureFirewire.hh"
 
//----------------------------------------------------------------------------

// Digital Camera spec information (libdc1394)
int numPorts = MAX_PORTS;
raw1394handle_t handles[MAX_CAMERAS];
dc1394_cameracapture cameras[MAX_CAMERAS];
int numCameras = 0;
nodeid_t *camera_nodes;
dc1394_feature_set features;

/*
int fps = FRAMERATE_30;

int mode = MODE_640x480_MONO;
int width = 640;
int height = 480;
*/

/*
int mode = MODE_320x240_YUV422;
int width = 320;
int height = 240;
int fps = FRAMERATE_30;
*/

/*
int mode = MODE_640x480_YUV422;
int width = 640;
int height = 480;
*/

// this maxes out at 15 fps

//int mode = MODE_FORMAT7_0;
int mode = MODE_320x240_YUV422;

int fps, buffer_scale, width, height; 

bayer_pattern_t bayer_pattern = BAYER_PATTERN_RGGB;

const char *device_name = "/dev/video1394/0";

unsigned char *tempbuff;

//----------------------------------------------------------------------------

void clear_channel(void)
{
  int channel;

  for (channel = 0; channel < MAX_CAMERAS; channel++) {
    //    printf("Clearing channel %i on %s\n",  channel, device_name);
    ioctl(open(device_name,O_RDONLY),VIDEO1394_IOC_UNLISTEN_CHANNEL,&channel);
  }
}

//----------------------------------------------------------------------------

void initialize_capture_firewire(int *w, int *h)
{
  unsigned int channel;
  unsigned int speed;
  raw1394handle_t raw_handle;
  struct raw1394_portinfo ports[MAX_PORTS];
  int p, i;
  int camCount;

  if (mode == MODE_FORMAT7_0) {
    fps = FRAMERATE_30;
    buffer_scale = 1;
    width = 640; 
    height = 480; 
  }
  else if (mode == MODE_320x240_YUV422) {
    width = 320;
    height = 240;
    fps = FRAMERATE_30;
  }

  *w = width;
  *h = height;

  clear_channel();

  // get the number of ports (cards) 
  
  raw_handle = raw1394_new_handle();
  if (raw_handle==NULL) {
    perror("Unable to aquire a raw1394 handle\n");
    perror("did you load the drivers?\n");
    exit(-1);
  }
  numPorts = raw1394_get_port_info(raw_handle, ports, numPorts);
  raw1394_destroy_handle(raw_handle);
  
  printf("number of firewire ports = %d\n", numPorts);


  ////////////////////////////////////////////////
  //
  // * Get dc1394 handle to each port
  // * For each camera found, set up for capture by 
  //    * Snagging the ISO channel/speed
  //    * Configuring the channel for DMA x-fer
  //    * Start ISO x-fers
  //

  for (p = 0; p < numPorts; p++) {
            
    // Get the camera nodes and describe them as we find them 
    
    raw_handle = raw1394_new_handle();
    raw1394_set_port( raw_handle, p );
    camera_nodes = dc1394_get_camera_nodes(raw_handle, &camCount, 0);
    raw1394_destroy_handle(raw_handle);
    
    printf("Number of cameras = %i on port %i\n", camCount, p);
    
    // setup cameras for capture 

    for (i = 0; i < camCount; i++) {	

      handles[numCameras] = dc1394_create_handle(p);
      if (handles[numCameras]==NULL) {
	perror("Unable to aquire a raw1394 handle\n");
	cleanup_capture_firewire();
      }
      
      cameras[numCameras].node = camera_nodes[i];
      
      if(dc1394_get_camera_feature_set(handles[numCameras], 
				       cameras[numCameras].node, 
				       &features) !=DC1394_SUCCESS) 
	printf("unable to get feature set\n");
      
      //      dc1394_print_feature_set(&features);

      
      // Set any manual parameters entered vi CLI
      //      set_manual_camera_parameters(handles[numCameras],cameras[numCameras].node);	  
      
      if (dc1394_get_iso_channel_and_speed(handles[numCameras], cameras[numCameras].node,
					   &channel, &speed) != DC1394_SUCCESS) {
	printf("unable to get the iso channel number\n");
	cleanup_capture_firewire();
      }

      if ((mode = MODE_FORMAT7_0 && dc1394_dma_setup_format7_capture(handles[numCameras], cameras[numCameras].node, i+1, mode,
								     SPEED_400, fps,QUERY_FROM_CAMERA,QUERY_FROM_CAMERA,QUERY_FROM_CAMERA, 
								     QUERY_FROM_CAMERA, NUM_BUFFERS, DROP_FRAMES,
								     device_name, &cameras[numCameras]) != DC1394_SUCCESS) ||
	  dc1394_dma_setup_capture(handles[numCameras], cameras[numCameras].node, i+1, // channel
 				   FORMAT_VGA_NONCOMPRESSED, mode,
  				   SPEED_400, fps, NUM_BUFFERS, DROP_FRAMES,
  				   device_name, &cameras[numCameras]) != DC1394_SUCCESS)  {
 	fprintf(stderr, "unable to setup camera- check line %d of %s to make sure\n", __LINE__,__FILE__);
	perror("that the video mode,framerate and format are supported is one supported by your camera\n");
	cleanup_capture_firewire();
      }
      
      // Have the camera start sending us data

      if (dc1394_start_iso_transmission(handles[numCameras], cameras[numCameras].node) !=DC1394_SUCCESS) {
	perror("unable to start camera iso transmission\n");
	cleanup_capture_firewire();
      }

      numCameras++;
    }

    dc1394_free_camera_nodes(camera_nodes);

  }

  tempbuff = (unsigned char *) calloc(width * height * 2, sizeof(unsigned char));
}

//----------------------------------------------------------------------------

void cleanup_capture_firewire()
{
  int i;

  for (i=0; i < numCameras; i++) {
    dc1394_dma_unlisten( handles[i], &cameras[i] );
    dc1394_dma_release_camera( handles[i], &cameras[i]);
    dc1394_destroy_handle(handles[i]);
  }

  exit(1);
}

//----------------------------------------------------------------------------

void capture_firewire(IplImage *im)
{
  // actually captures all attached cameras...

  //  dc1394_dma_multi_capture(cameras, numCameras);
  dc1394_dma_single_capture(&cameras[0]);

  // ...but only makes a copy of one of them

  if (height != im->height || width != im->width) {
    printf("size mismatch %ix%i image vs. %ix%i source\n", im->width, im->height, width, height);
    exit(1);
  }


  //  swab((unsigned char *) cameras[0].capture_buffer, tempbuff, width * height * 2);
  //uyvy2rgb(tempbuff, (unsigned char*) im->iplim->imageData, width * height);
  //uyvy2rgb((unsigned char *) cameras[0].capture_buffer, (unsigned char*) im->iplim->imageData, width * height);

  //  write_CR_Image("test2.png", im);
  //  exit(1);

  //  my_yuv422_to_bgr((unsigned char *) cameras[0].capture_buffer, (unsigned char*) im->iplim->imageData, width, height); 
  BayerNearestNeighbor((unsigned char *)cameras[0].capture_buffer, (unsigned char *) im->imageData, im->width, im->height, bayer_pattern);
  //memcpy((unsigned char *) im->iplim->imageData, (unsigned char *)cameras[0].capture_buffer, height * width*3);

  if (dc1394_dma_done_with_buffer(&cameras[0]) < 0)
    perror("DWB failed:");
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
