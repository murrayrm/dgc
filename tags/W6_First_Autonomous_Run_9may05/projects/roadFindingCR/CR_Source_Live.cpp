//#include "decs.h"
#include "CR_Source_Live.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

//CR_Window **live_win = NULL;
IplImage **live_im = NULL;
int num_lives = 0;

CR_Movie *live_mov;

char **live_source_names;
int num_live_sources;

char *dv_resolution[4] = {"720 x 480", "360 x 240", "180 x 120", "88 x 60"};
int added_dv, dv_device;

extern int num_receives;

extern int camera_framerate;

// extern CR_Calibration *Calib1, *Calib2, *CalibCenter;
// extern CR_Image *calibim;

//----------------------------------------------------------------------------

//----------------------------------------------------------------------------

int bPreviewFaked = FALSE;

void oldInitVideo(CR_Live *);
void newInitVideo(CR_Live *);
void GrabVideo(CR_Live *);
void UpdateVideo(CR_Live *);
void CleanupVideo(CR_Live *);

char **ListConnectedDevices(int *);

int MakeBuilder(CR_Live *);
int MakeGraph(CR_Live *);
void PickConnectedDevice(CR_Live *, int);
int GetVideoFormat(CR_Live *);

// DV camera functions

int DV_ConnectFilters(CR_Live *);

void DV_InitVideo(CR_Live *, int, int, int);
int DV_SetResolution(CR_Live *, int, int);
void DV_KillVideo(CR_Live *);
void DV_InitAVICapture(char *, CR_Live *, int, int, int);
void DV_InitAVICapture2(char *, CR_Live *, int, int, int);

// non-DV camera functions

int ConnectFilters(CR_Live *);  

void IntelCamPro_InitVideo(CR_Live *, int, int, int);
int IntelCamPro_SetResolution(CR_Live *, int, int);
void IntelCamPro_CaptureVideo(char *, CR_Live *, int, int, int);
void IntelCamPro_KillVideo(CR_Live *);
void IntelCamPro_InitAVICapture(char *, CR_Live *, int, int, int);

void UnibrainFireI_InitVideo(CR_Live *, int, int, int);
int UnibrainFireI_SetResolution(CR_Live *, int, int);
void UnibrainFireI_CaptureVideo(char *, CR_Live *, int, int, int);
void UnibrainFireI_KillVideo(CR_Live *);
void UnibrainFireI_InitAVICapture(char *, CR_Live *, int, int, int);

void FirewireDVC_InitVideo(CR_Live *, int, int, int);
int FirewireDVC_SetResolution(CR_Live *, int, int);
void FirewireDVC_CaptureVideo(char *, CR_Live *, int, int, int);
void FirewireDVC_KillVideo(CR_Live *);
void FirewireDVC_InitAVICapture(char *, CR_Live *, int, int, int);

void PGRDragonfly_InitVideo(CR_Live *, int, int, int);
int PGRDragonfly_SetResolution(CR_Live *, int, int);
void PGRDragonfly_CaptureVideo(char *, CR_Live *, int, int, int);
void PGRDragonfly_KillVideo(CR_Live *);
void PGRDragonfly_InitAVICapture(char *, CR_Live *, int, int, int);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void old_add_live_source()
{
  CR_Live *live;
  IplImage *im;
  int i;

  live = (CR_Live *) CR_malloc(sizeof(CR_Live));

  //oldInitVideo(live);
  //  newInitVideo(live);
  DV_InitVideo(live, 720, 480, 0);

  while (1)
    ;

  exit(1);
}

//----------------------------------------------------------------------------

int report_num_live_sources_CR_Live()
{
  return num_live_sources;
}

//----------------------------------------------------------------------------

char *report_live_source_name_CR_Live(int i)
{
  return live_source_names[i];
}

//----------------------------------------------------------------------------

int initialize_CR_Live()
{
  int i;

  live_source_names = get_live_source_names(&num_live_sources);

  for (i = 0; i < num_live_sources; i++)
    printf("video device %i: %s\n", i, live_source_names[i]);

  return num_live_sources;
}

//----------------------------------------------------------------------------

void grab_CR_Live(CR_Image *im, CR_Live *live)
{
  grab_CR_Live(im, live, FALSE);
}

//----------------------------------------------------------------------------

typedef enum
{
  BAYER_PATTERN_BGGR,
  BAYER_PATTERN_GRBG,
  BAYER_PATTERN_RGGB,
  BAYER_PATTERN_GBRG
} bayer_pattern_t;

#define CLIP(in, out)\
{\
   in = in < 0 ? 0 : in;\
   in = in > 255 ? 255 : in;\
   out=in;\
}

void BayerNearestNeighbor(unsigned char *src, unsigned char *dest, int sx, int sy, bayer_pattern_t type);
void BayerDownsample(unsigned char *src, unsigned char *dest, int sx, int sy, bayer_pattern_t type);

void grab_CR_Live(CR_Image *im, CR_Live *live, int undistort)
{
  int i, j, bytecount, ycount, ucount, vcount, ustart, vstart, uvcount, uvi, uvj, im_w_2, im_h_2, im_w_4, nu, nv, yuvi, yuvj;
  unsigned char temp, y, u, v;
  int r, g, b;
  char uchar, vchar;

  //  printf("grabbing\n");

  // have to do this for DV -- maybe not for other devices

  if (live->device_type == DV_VIDEO) {

    //    if (num_receives) {
    if (TRUE) {
      printf("start dv grab\n");
      fflush(stdout);
      
      memcpy(live->rend->flipim->data, live->rend->bits, im->buffsize);
      
      vflip_CR_Image(live->rend->flipim, im);
      
      for (j = 0; j < im->h; j++)
	for (i = 0; i < im->w; i++) {
	  temp = (unsigned char) IMXY_R(im, i, j);
	  IMXY_R(im, i, j) = (unsigned char) IMXY_B(im, i, j);
	  IMXY_B(im, i, j) = (unsigned char) temp;
	}
      
      num_receives = 0;
      
      printf("end dv grab\n");
      fflush(stdout);
      
    }
  }
  else if (live->device_type == INTELCAMPRO_VIDEO) {

    //    CR_flush_printf("%p %p\n", live->rend->flipim->data, live->rend->bits);
    //    exit(1);

    memcpy(live->rend->flipim->data, live->rend->bits, im->buffsize / 2);

    im_w_2 = im->w / 2;
    im_h_2 = im->h / 2;

    // get Y channel

    for (j = 0, ycount = 0; j < im->h; j++)
      for (i = 0; i < im->w; i++) {
  	IMXY_R(live->rend->yuvim, i, j) = live->rend->flipim->data[ycount];
	ycount++;
      }

    // get subsampled U, V channels

    for (j = 0, nu = im->buffsize / 3, nv = 5 * im->buffsize / 12; j < im->h; j += 2) {
      for (i = 0; i < im->w; i += 2) {

	uchar = live->rend->flipim->data[nu];
	vchar = live->rend->flipim->data[nv];

 	IMXY_G(live->rend->yuvim, i, j) = uchar;
 	IMXY_B(live->rend->yuvim, i, j) = vchar;

 	IMXY_G(live->rend->yuvim, i + 1, j) = uchar;
 	IMXY_B(live->rend->yuvim, i + 1, j) = vchar;

 	IMXY_G(live->rend->yuvim, i, j + 1) = uchar;
 	IMXY_B(live->rend->yuvim, i, j + 1) = vchar;

 	IMXY_G(live->rend->yuvim, i + 1, j + 1) = uchar;
 	IMXY_B(live->rend->yuvim, i + 1, j + 1) = vchar;

	nu++;
	nv++;
      }
    }

    live->rend->yuvim->iplim->roi = NULL;
    im->iplim->roi = NULL;
    iplYUV2RGB(live->rend->yuvim->iplim, im->iplim);
  }
  else if (live->device_type == UNIBRAINFIREI_VIDEO || live->device_type == FIREWIREDVC_VIDEO) {

    memcpy(live->rend->flipim->data, live->rend->bits, 2 * im->buffsize / 3);

//     im_w_2 = im->w / 2;
//     im_h_2 = im->h / 2;

    for (j = 0, bytecount = 0; j < im->h; j++)
      for (i = 0; i < im->w; i += 2, bytecount += 4) {

	// Y

  	IMXY_R(live->rend->yuvim, i, j) = live->rend->flipim->data[bytecount + 1];
  	IMXY_R(live->rend->yuvim, i + 1, j) = live->rend->flipim->data[bytecount + 3];
	
	// U, V

  	IMXY_G(live->rend->yuvim, i, j) = live->rend->flipim->data[bytecount];
  	IMXY_G(live->rend->yuvim, i + 1, j) = live->rend->flipim->data[bytecount];

  	IMXY_B(live->rend->yuvim, i, j) = live->rend->flipim->data[bytecount + 2];
  	IMXY_B(live->rend->yuvim, i + 1, j) = live->rend->flipim->data[bytecount + 2];
      }

    live->rend->yuvim->iplim->roi = NULL;
    im->iplim->roi = NULL;
 
    if (!undistort || !live->calibration) {
      iplYUV2RGB(live->rend->yuvim->iplim, im->iplim);
    }
    else {
      live->calibim->iplim->roi = NULL;
      iplYUV2RGB(live->rend->yuvim->iplim, live->calibim->iplim);
      undistort_image_CR_Calibration(im, live->calibim, live->calibration);

//       calibim->iplim->roi = NULL;
//       iplYUV2RGB(live->rend->yuvim->iplim, calibim->iplim);
//       undistort_image_CR_Calibration(im, calibim, CalibCenter);
    }
    
  }
  else if (live->device_type == PGRDRAGONFLY_VIDEO) {
    //    printf("grabbing dragonfly: w = %i, h = %i\n", im->w, im->h);


    //BayerNearestNeighbor(live->rend->bits, (unsigned char *) im->data, im->w, im->h,   BAYER_PATTERN_RGGB);
    BayerDownsample(live->rend->bits, (unsigned char *) im->data, im->w, im->h, BAYER_PATTERN_RGGB);

    /*
    memcpy(live->rend->flipim->data, live->rend->bits, im->buffsize / 3);

    //    printf("imbuff = %i\n", im->buffsize / 3);

      for (j = 0, bytecount = 0; j < im->h; j += 2)
	for (i = 0; i < im->w; i += 2) {
	  IMXY_R(im, i, j) = live->rend->flipim->data[bytecount];
	  IMXY_G(im, i, j) = live->rend->flipim->data[bytecount];
	  IMXY_B(im, i, j) = live->rend->flipim->data[bytecount];
	  bytecount++;
	}
    */

      //    memcpy(im->data, live->rend->bits, im->buffsize / 3);

    //    printf("buffsize = %i\n", im->buffsize);

//     for (i = 0; i < im->buffsize/3; i++)
//       CR_flush_printf("%i", (int) im->data[i]);
    //   printf("%i", (int) live->rend->bits[i]);
    
    //    CR_flush_printf("\n\n\n");

    //    exit(1);

    //    im->iplim->roi = NULL;

  }

  // non-IPL version of YUV->RGB conversion
  /*
  else if (live->device_type == INTELCAMPRO_VIDEO) {

    memcpy(live->rend->flipim->data, live->rend->bits, im->buffsize / 2);

    im_w_2 = im->w / 2;

    for (j = 0, ycount = 0, ustart = im->buffsize / 3, vstart = 5 * im->buffsize / 12, uvcount = 0; j < im->h; j++)
      for (i = 0; i < im->w; i++) {

	uvi = i / 2;
	uvj = j / 2;

 	y = (unsigned char) live->rend->flipim->data[ycount];
  	u = (unsigned char) live->rend->flipim->data[ustart + uvi + uvj * im_w_2];
 	v = (unsigned char) live->rend->flipim->data[vstart + uvi + uvj * im_w_2];
	
	b = (int) (1.164 * (y - 16) + 1.596 * (u - 128));
	g = (int) (1.164 * (y - 16) - 0.813 * (u - 128) - 0.391 * (v - 128));
	r = (int) (1.164 * (y - 16) + 2.018 * (v - 128));

	//printf("r = %i, g = %i, b = %i\n", r, g, b);
 	if (r < 0)
	  IMXY_R(im, i, j) = (unsigned char) 0;
	else if (r > 255)
	  IMXY_R(im, i, j) = (unsigned char) 255;
	else
	  IMXY_R(im, i, j) = (unsigned char) r;

 	if (g < 0)
	  IMXY_G(im, i, j) = (unsigned char) 0;
	else if (g > 255)
	  IMXY_G(im, i, j) = (unsigned char) 255;
	else
	  IMXY_G(im, i, j) = (unsigned char) g;

 	if (b < 0)
	  IMXY_B(im, i, j) = (unsigned char) 0;
	else if (b > 255)
	  IMXY_B(im, i, j) = (unsigned char) 255;
	else
	  IMXY_B(im, i, j) = (unsigned char) b;

	ycount++;
	//	if (i < im->w - 1)
	//	  uvcount += 2;
	  //	else
	  // uvcount += 2 + im->w / 2;
	//	ucount++;
	//	vcount++;
      }
  }
  */
}

//----------------------------------------------------------------------------

// taken straight from coriander 1.0.0-pre

// sx = width, sy = height?
// src half width, half height of dst (in terms of raw bytes)?

void BayerNearestNeighbor(unsigned char *src, unsigned char *dest, int sx, int sy, bayer_pattern_t type)
{
  unsigned char *outR, *outG, *outB;
  register int i,j;

  // sx and sy should be even
  switch (type) {
  case BAYER_PATTERN_GRBG:
  case BAYER_PATTERN_BGGR:
    outR=&dest[0];
    outG=&dest[1];
    outB=&dest[2];
    break;
  case BAYER_PATTERN_GBRG:
  case BAYER_PATTERN_RGGB:
    outR=&dest[2];
    outG=&dest[1];
    outB=&dest[0];
    break;
  default:
    outR=NULL;outG=NULL;outB=NULL;
    break;
  }
  
  switch (type) {
  case BAYER_PATTERN_GRBG: //-------------------------------------------
  case BAYER_PATTERN_GBRG:
    // copy original RGB data to output images
    for (i=0;i<sy;i+=2) {
      for (j=0;j<sx;j+=2) {
	outG[(i*sx+j)*3]=src[i*sx+j];
	outG[((i+1)*sx+(j+1))*3]=src[(i+1)*sx+(j+1)];
	outR[(i*sx+j+1)*3]=src[i*sx+j+1];
	outB[((i+1)*sx+j)*3]=src[(i+1)*sx+j];
      }
    }
    // R channel
    for (i=0;i<sy;i+=2) {
      for (j=0;j<sx-1;j+=2) {
	outR[(i*sx+j)*3]=outR[(i*sx+j+1)*3];
	outR[((i+1)*sx+j+1)*3]=outR[(i*sx+j+1)*3];
	outR[((i+1)*sx+j)*3]=outR[(i*sx+j+1)*3];
      }
    }
      // B channel
    for (i=0;i<sy-1;i+=2)  { //every two lines
      for (j=0;j<sx-1;j+=2) {
	outB[(i*sx+j)*3]=outB[((i+1)*sx+j)*3];
	outB[(i*sx+j+1)*3]=outB[((i+1)*sx+j)*3];
	outB[((i+1)*sx+j+1)*3]=outB[((i+1)*sx+j)*3];
      }
    }
    // using lower direction for G channel
      
    // G channel
    for (i=0;i<sy-1;i+=2)//every two lines
      for (j=1;j<sx;j+=2)
	outG[(i*sx+j)*3]=outG[((i+1)*sx+j)*3];
      
    for (i=1;i<sy-2;i+=2)//every two lines
      for (j=0;j<sx-1;j+=2)
	outG[(i*sx+j)*3]=outG[((i+1)*sx+j)*3];
    
    // copy it for the next line
    for (j=0;j<sx-1;j+=2)
      outG[((sy-1)*sx+j)*3]=outG[((sy-2)*sx+j)*3];
    
    break;
  case BAYER_PATTERN_BGGR: //-------------------------------------------
  case BAYER_PATTERN_RGGB:
    // copy original data
    for (i=0;i<sy;i+=2) {
      for (j=0;j<sx;j+=2) {
	outB[(i*sx+j)*3]=src[i*sx+j];
	outR[((i+1)*sx+(j+1))*3]=src[(i+1)*sx+(j+1)];
	outG[(i*sx+j+1)*3]=src[i*sx+j+1];
	outG[((i+1)*sx+j)*3]=src[(i+1)*sx+j];
      }
    }
    // R channel
    for (i=0;i<sy;i+=2){
      for (j=0;j<sx-1;j+=2) {
	outR[(i*sx+j)*3]=outR[((i+1)*sx+j+1)*3];
	outR[(i*sx+j+1)*3]=outR[((i+1)*sx+j+1)*3];
	outR[((i+1)*sx+j)*3]=outR[((i+1)*sx+j+1)*3];
      }
    }
    // B channel
    for (i=0;i<sy-1;i+=2) { //every two lines
      for (j=0;j<sx-1;j+=2) {
	outB[((i+1)*sx+j)*3]=outB[(i*sx+j)*3];
	outB[(i*sx+j+1)*3]=outB[(i*sx+j)*3];
	outB[((i+1)*sx+j+1)*3]=outB[(i*sx+j)*3];
      }
    }
    // using lower direction for G channel
    
    // G channel
    for (i=0;i<sy-1;i+=2)//every two lines
      for (j=0;j<sx-1;j+=2)
	outG[(i*sx+j)*3]=outG[((i+1)*sx+j)*3];
    
    for (i=1;i<sy-2;i+=2)//every two lines
      for (j=0;j<sx-1;j+=2)
	outG[(i*sx+j+1)*3]=outG[((i+1)*sx+j+1)*3];
    
    // copy it for the next line
    for (j=0;j<sx-1;j+=2)
      outG[((sy-1)*sx+j+1)*3]=outG[((sy-2)*sx+j+1)*3];
    
    break;
    
  default:  //-------------------------------------------
    break;
  }
}

//----------------------------------------------------------------------------

// taken straight from coriander 1.0.0-pre

void BayerDownsample(unsigned char *src, unsigned char *dest, int sx, int sy, bayer_pattern_t type)
{
  unsigned char *outR, *outG, *outB;
  register int i,j;
  int tmp;

  sx*=2;
  sy*=2;

  switch (type) {
  case BAYER_PATTERN_GRBG:
  case BAYER_PATTERN_BGGR:
    outR=&dest[0];
    outG=&dest[1];
    outB=&dest[2];
    break;
  case BAYER_PATTERN_GBRG:
  case BAYER_PATTERN_RGGB:
    outR=&dest[2];
    outG=&dest[1];
    outB=&dest[0];
    break;
  default:
    outR=NULL;outG=NULL;outB=NULL;
    break;
  }
  
  switch (type) {
  case BAYER_PATTERN_GRBG://---------------------------------------------------------
  case BAYER_PATTERN_GBRG:
    for (i=0;i<sy;i+=2) {
      for (j=0;j<sx;j+=2) {
	tmp=((src[i*sx+j]+src[(i+1)*sx+(j+1)])>>1);
	CLIP(tmp,outG[(((i*sx)>>2)+(j>>1))*3]);
	tmp=src[i*sx+j+1];
	CLIP(tmp,outR[(((i*sx)>>2)+(j>>1))*3]);
	tmp=src[(i+1)*sx+j];
	CLIP(tmp,outB[(((i*sx)>>2)+(j>>1))*3]);
      }
    }
    break;
  case BAYER_PATTERN_BGGR://---------------------------------------------------------
  case BAYER_PATTERN_RGGB:
    for (i=0;i<sy;i+=2) {
      for (j=0;j<sx;j+=2) {
	tmp=((src[(i+1)*sx+j]+src[i*sx+(j+1)])>>1);
	CLIP(tmp,outG[(((i*sx)>>2)+(j>>1))*3]);
	tmp=src[(i+1)*sx+j+1];
	CLIP(tmp,outR[(((i*sx)>>2)+(j>>1))*3]);
	tmp=src[i*sx+j];
	CLIP(tmp,outB[(((i*sx)>>2)+(j>>1))*3]);
      }
    }
    break;
  default: //---------------------------------------------------------
    fprintf(stderr,"Bad bayer pattern ID\n");
    break;
  }
  
}

//----------------------------------------------------------------------------

/*
// list live source options
// keep the menu indices > 16 to not interfere with other 
// kinds of sources listed on the same menu

#define LIVE_MENU_BASE     64

void add_submenu_CR_Live()
{
  int i, j;
  char menu_entry[256];

  for (i = 0, added_dv = FALSE; i < num_live_sources; i++) {
    if (!strcmp(live_source_names[i], "Microsoft DV Camera and VCR")) {
      added_dv = TRUE;
      dv_device = i;
      for (j = 0; j < 4; j++) {
	sprintf(menu_entry, "%s %s", live_source_names[i], dv_resolution[j]);
	//	printf("%s\n", menu_entry);
	glutAddMenuEntry(menu_entry, LIVE_MENU_BASE + i + j);
      }
    }
    else {
      if (!added_dv)
	glutAddMenuEntry(live_source_names[i], LIVE_MENU_BASE + i);
      else
	glutAddMenuEntry(live_source_names[i], LIVE_MENU_BASE + i + 3);
    }
  }

  //  for (i = 0; i < num_live_sources; i++)
  //    glutAddMenuEntry(live_source_names[i], LIVE_MENU_BASE + i);
}

//----------------------------------------------------------------------------

void handle_submenu_CR_Live(int value, int *width, int *height, int *device_num, char *device_name)
{
  int temp_device_num;
  int non_dv_width, non_dv_height;

  non_dv_width = 320;
  non_dv_height = 240;
  //  non_dv_width = 640;
  //  non_dv_height = 480;

  if (added_dv) {
    temp_device_num = value - LIVE_MENU_BASE;
    // before dv device
    if (temp_device_num < dv_device) {
      *width = non_dv_width;
      *height = non_dv_height;
      *device_num = temp_device_num;
    }
    // after dv device
    else if (temp_device_num >= dv_device + 4){
      *width = non_dv_width;
      *height = non_dv_height;
      *device_num = temp_device_num - 3;
    }
    // dv device
    else {
      if (temp_device_num == dv_device) {
	*width = 720;
	*height = 480;
      }
      else if (temp_device_num == dv_device + 1) {
	*width = 360;
	*height = 240;
      }
      else if (temp_device_num == dv_device + 2) {
	*width = 180;
	*height = 120;
      }
      else if (temp_device_num == dv_device + 3) {
	*width = 88;
	*height = 60;
      }
      *device_num = dv_device;
    }
  }
  else {
    *width = non_dv_width;
    *height = non_dv_height;
    *device_num = value - LIVE_MENU_BASE;
  }

  strcpy(device_name, live_source_names[*device_num]);
}
*/

//----------------------------------------------------------------------------

char **get_live_source_names(int *num_sources)
{
  char **live_source_list;

  //  CoInitialize(NULL);

  live_source_list = ListConnectedDevices(num_sources);

  return live_source_list;
}

//----------------------------------------------------------------------------

#define MAX_LIVE_SOURCES 256
#define MAX_DEVICE_NAME 256

char **ListConnectedDevices(int *num_sources)
{
  HRESULT hr;
  IPropertyBag *pBag;
  ICreateDevEnum *pCreateDevEnum;
  ULONG cFetched;
  IMoniker *pM;
  ULONG uFetched, uIndex = 0;
  VARIANT var;
  IEnumMoniker *pEM;
  char **source_list;
  char cFriendlyName[120];

  source_list = (char **) CR_calloc(MAX_LIVE_SOURCES, sizeof(char *));

  // enumerate all video capture devices

  if (CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER,
		       IID_ICreateDevEnum, (void**) &pCreateDevEnum) != NOERROR)
    CR_error("Can't create device enumerator");

  hr = pCreateDevEnum->CreateClassEnumerator(CLSID_VideoInputDeviceCategory, &pEM, 0);

  pCreateDevEnum->Release();
  if (hr != NOERROR) {
    *num_sources = 0;
    return NULL;
    //    CR_error("No video capture hardware found");
  }

  pEM->Reset();

  uIndex = 0;
	
  while ((hr = pEM->Next(1, &pM, &uFetched)), hr == S_OK) {

    hr = pM->BindToStorage(0, 0, IID_IPropertyBag, (void **) &pBag);
    
    if (SUCCEEDED(hr)) {
      
      // this is a cheezy conversion from Visual basic to C style strings
      
      var.vt = VT_BSTR;
      hr = pBag->Read(L"FriendlyName", &var, NULL);
      if (hr == NOERROR) {
	WideCharToMultiByte(CP_ACP, 0, var.bstrVal, -1, cFriendlyName, 80, NULL, NULL);
	SysFreeString(var.bstrVal);
      }
      pBag->Release();

      source_list[*num_sources] = (char *) CR_calloc(MAX_DEVICE_NAME, sizeof(char));
      sprintf(source_list[*num_sources], cFriendlyName);
      (*num_sources)++;

      //      printf("Selected Device %i: %s\n", uIndex, cFriendlyName);
    }
    
    //      hr = pM->BindToObject(0, 0, IID_IBaseFilter, (void**) &(live->pVCap));
    pM->Release();	
    
    uIndex++;
  }
  
  //  *num_sources = 0;
  return source_list;
}

//----------------------------------------------------------------------------

CR_Live *capture_live_source(char *filename, int width, int height, int device_num, char *device_name)
{
  CR_Live *live;

  live = (CR_Live *) CR_malloc(sizeof(CR_Live));
  live->calibration = NULL;
  
  // assuming DV

  //  if (!strcmp(device_name, "Microsoft DV Camera and VCR"))
  //    DV_CaptureVideo(live, width, height, device_num);
  //  if (!strcmp(device_name, "Intel PC Camera Pro"))
  if (!strncmp(device_name, "Intel", 5))
    IntelCamPro_CaptureVideo(filename, live, width, height, device_num);
  //  else if (!strncmp(device_name, "Unibrain Fire-i 1394 Camera Driver", 34))
  else if (!strncmp(device_name, "Unibrain Fire-i", 15))
	   //  else if (!strcmp(device_name, "Unibrain Fire-i 1394 Camera Driver (Unibrain)") ||
	   //	   !strcmp(device_name, "Unibrain Fire-i 1394 Camera Driver (ADS Pyro)"))
    UnibrainFireI_CaptureVideo(filename, live, width, height, device_num);
  else if (!strcmp(device_name, "1394 Desktop Video Camera"))
	   //  else if (!strcmp(device_name, "Unibrain Fire-i 1394 Camera Driver (Unibrain)") ||
	   //	   !strcmp(device_name, "Unibrain Fire-i 1394 Camera Driver (ADS Pyro)"))
    FirewireDVC_CaptureVideo(filename, live, width, height, device_num);
  else if (!strcmp(device_name, "PGR Streaming Dragonfly"))
//   else if (!strcmp(device_name, "Unibrain Fire-i 1394 Camera Driver (Unibrain)") ||
// 	   !strcmp(device_name, "Unibrain Fire-i 1394 Camera Driver (ADS Pyro)"))
    PGRDragonfly_CaptureVideo(filename, live, width, height, device_num);
  else
    CR_error("unsupported video device 1");

  //  live_mov = write_avi_initialize_CR_Movie(NULL, live->width, live->height, 30);
  
  return live;
}

//----------------------------------------------------------------------------

CR_Live *initialize_live_source(int width, int height, int device_num, char *device_name)
{
  return initialize_live_source(width, height, device_num, device_name, NULL);
}

//----------------------------------------------------------------------------

CR_Live *initialize_live_source(int width, int height, int device_num, char *device_name, char *calibration_filename)
{
  CR_Live *live;
  CR_Vector *cv;

  live = (CR_Live *) CR_malloc(sizeof(CR_Live));
  if (calibration_filename) {
    cv = read_dlm_CR_Vector(calibration_filename, 8);
    live->calibration = initialize_CR_Calibration(cv->x[0], cv->x[1], cv->x[2], cv->x[3], cv->x[4], cv->x[5], cv->x[6], cv->x[7]);
  }
  else
    live->calibration = NULL;

  live->device_num = device_num;
  printf("initialize_live_source(): w = %i, h = %i, dev = %i, name = %s\n", width, height, device_num, device_name);

  // assuming DV

  if (!strcmp(device_name, "Microsoft DV Camera and VCR"))
    DV_InitVideo(live, width, height, device_num);
  //  else if (!strcmp(device_name, "Intel PC Camera Pro"))
  else if (!strncmp(device_name, "Intel", 5))
    //    IntelCamPro_InitVideo(live, width, height, device_num);
    IntelCamPro_InitVideo(live, 160, 120, device_num);
  //  else if (!strncmp(device_name, "Unibrain Fire-i 1394 Camera Driver", 34))
  else if (!strncmp(device_name, "Unibrain Fire-i", 15))
//   else if (!strcmp(device_name, "Unibrain Fire-i 1394 Camera Driver (Unibrain)") ||
// 	   !strcmp(device_name, "Unibrain Fire-i 1394 Camera Driver (ADS Pyro)"))
    UnibrainFireI_InitVideo(live, width, height, device_num);
  else if (!strcmp(device_name, "1394 Desktop Video Camera"))
//   else if (!strcmp(device_name, "Unibrain Fire-i 1394 Camera Driver (Unibrain)") ||
// 	   !strcmp(device_name, "Unibrain Fire-i 1394 Camera Driver (ADS Pyro)"))
    FirewireDVC_InitVideo(live, width, height, device_num);
  else if (!strcmp(device_name, "PGR Streaming Dragonfly"))
//   else if (!strcmp(device_name, "Unibrain Fire-i 1394 Camera Driver (Unibrain)") ||
// 	   !strcmp(device_name, "Unibrain Fire-i 1394 Camera Driver (ADS Pyro)"))
    PGRDragonfly_InitVideo(live, width, height, device_num);
  else
    CR_error("unsupported video device 2");

  //  live_mov = write_avi_initialize_CR_Movie(NULL, live->width, live->height, 30);
  
  return live;
}

//----------------------------------------------------------------------------

// should be stopped already

void kill_live_source(CR_Live *live)
{
  //  if (live->pMC->Stop() != NOERROR)
  //    CR_error("Cannot stop capture graph");

  live->pFg->Release();
  live->pVSC->Release();
  live->pVCap->Release();
  live->pB2->Release();
  live->pAVIMux->Release();
  live->pMC->Release();

  //  free(live->rend);
  free(live);

    /*  if (live->pFg->Release() != NOERROR)
    CR_error("Cannot release pFg");

  if (live->pVSC->Release() != NOERROR)
    CR_error("Cannot release VSC");

  if (live->pVCap->Release() != NOERROR)
    CR_error("Cannot release VCap");
  
  //  if (live->pVC->Release() != NOERROR)
  //    CR_error("Cannot release");


  if (live->pB2->Release() != NOERROR)
    CR_error("Cannot release pB2");

  //  if (live->pBV->Release() != NOERROR)
  //    CR_error("Cannot release pBV");
    */

  /*
  IMediaControl *pMC = NULL;
  HRESULT hr;

  hr = live->pFg->QueryInterface(IID_IMediaControl, (void **) &pMC);
  
  if (SUCCEEDED(hr)) {
    hr = pMC->Stop();
    pMC->Release();
  }

  live->pVCap->Release();
  
  live->pVC->Release();
  live->pVSC->Release();
  live->pB->Release();
  live->pFg->Release();
  live->pBV->Release();
  */

  //  CR_error("kill_live_source(): not yet ready");
}

//----------------------------------------------------------------------------

// specifically for Intel PC Camera Pro

void IntelCamPro_InitVideo(CR_Live *live, int width, int height, int device_num)
{
  HRESULT hr;
  CCritSec *plock;
  int i;

  live->device_type = INTELCAMPRO_VIDEO;

  //  plock = new CCritSec();
  live->rend = new CRender(NULL, &hr);
  live->rend->device_num = device_num;
  live->rend->ncam_docopy = FALSE;
  live->rend->m_pPin->dv_device = 0;
  live->rend->m_pPin->nondv_width = width;
  live->rend->m_pPin->nondv_height = height;

  CR_flush_printf("w = %i, h = %i\n", width, height);

  live->pB2 = NULL;
  live->pFg = NULL;

  //  CoInitialize(NULL);

  if (!MakeBuilder(live))
    CR_error("Cannot instantiate graph builder");

  //  printf("intel camera pro init device num = %i\n", device_num);

  PickConnectedDevice(live, device_num);   // 0 is default index
  //  PickConnectedDevice(live, 0);   // 0 is default index

  if (!MakeGraph(live))
    CR_error("Cannot instantiate filtergraph");
  
  if (live->pB2->SetFiltergraph(live->pFg) != NOERROR)
    CR_error("Cannot give graph to builder");

  if (live->pFg->AddFilter(live->pVCap, NULL) != NOERROR)
    CR_error("Cannot add vidcap to filtergraph");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMStreamConfig, (void **) &(live->pVSC)) != NOERROR)
    CR_error("Cannot find stream config");

  if (!GetVideoFormat(live))
    CR_error("Cannot get video format");

  if (live->pFg->AddFilter(live->rend->m_pFilter, NULL) != NOERROR)
    CR_error("Cannot add renderer filter to filtergraph");

  if (!ConnectFilters(live))
    CR_error("Cannot connect filters");

  if (!IntelCamPro_SetResolution(live, width, height))
    CR_error("Could not set intel cam pro decoder resolution");

  if (live->pFg->QueryInterface(IID_IMediaControl, (void **) &live->pMC) != NOERROR)
    CR_error("Cannot prepare to run graph");

  live->rend->m_pfnCallback = (void *) live->rend->StaticFrameCallbackProc;
  live->rend->m_cbParam = (void *) live->rend;

  if (live->pMC->Run() != NOERROR)
    CR_error("Cannot run capture graph");

  live->im = live->rend->im;

  //  for (i = 0; i < 100000000; i++);
}

//----------------------------------------------------------------------------

// 12 bits per pixel

int IntelCamPro_SetResolution(CR_Live *live, int w, int h)
{
  live->width = w;
  live->height = h;

  live->size = 3 * live->width * live->height / 2;
  //  live->size = live->height * ((live->width + 3) & ~3);
  live->rend->dib = (BITMAPINFO *) CR_malloc(sizeof(BITMAPINFOHEADER) + live->size);
  live->rend->dibh = (BITMAPINFOHEADER *) live->rend->dib;

  live->rend->dibh->biSize = sizeof(BITMAPINFOHEADER); 
  live->rend->dibh->biWidth = live->width;         // fill in width from parameter 
  live->rend->dibh->biHeight = live->height;       // was -live->height - fill in height from parameter 
  live->rend->dibh->biPlanes = 1;                  // must be 1 
  live->rend->dibh->biBitCount = 24;               // from parameter 
  live->rend->dibh->biCompression = BI_RGB;     
  live->rend->dibh->biSizeImage = 0;               // 0's here mean "default" 
  live->rend->dibh->biXPelsPerMeter = 2700;
  live->rend->dibh->biYPelsPerMeter = 2700;
  live->rend->dibh->biClrUsed = 0; 
  live->rend->dibh->biClrImportant = 0; 

  printf("ALLOCATING intel cam pro bits = %i (w = %i, h = %i)\n", live->size, live->width, live->height);
  live->rend->bits = (unsigned char *) ((char *) live->rend->dib + sizeof(BITMAPINFOHEADER));
  live->rend->im = make_CR_Image(live->width, live->height, 0);
  live->rend->flipim = make_CR_Image(live->width, live->height, 0);
  live->rend->yuvim = make_CR_Image(live->width, live->height, IPL_TYPE_COLOR_8U_YUV);

  live->calibim = make_CR_Image(live->width, live->height, 0);

  return TRUE;
}

//----------------------------------------------------------------------------

// specifically for Intel PC Camera Pro

void IntelCamPro_CaptureVideo(char *filename, CR_Live *live, int width, int height, int device_num)
{
  HRESULT hr;
  CCritSec *plock;
  int i;
  char *tempfilename;
  WCHAR wach_temp[MAX_AVI_PATH], wach_dest[MAX_AVI_PATH];
  long starttime, stoptime;
  AM_MEDIA_TYPE *pmt; 
  VIDEOINFOHEADER *pvi;

  live->device_type = INTELCAMPRO_VIDEO;

  //  plock = new CCritSec();
  /*
  live->rend = new CRender(NULL, &hr);
  live->rend->m_pPin->dv_device = 0;
  live->rend->m_pPin->nondv_width = width;
  live->rend->m_pPin->nondv_height = height;
  */

  CR_flush_printf("w = %i, h = %i\n", width, height);

  live->pB2 = NULL;
  live->pFg = NULL;

  //  CoInitialize(NULL);

  if (!MakeBuilder(live))
    CR_error("Cannot instantiate graph builder");

  //  printf("intel camera pro init device num = %i\n", device_num);

  PickConnectedDevice(live, device_num);   // 0 is default index
  //  PickConnectedDevice(live, 0);   // 0 is default index

    
  /*
  if (!MakeGraph(live))
    CR_error("Cannot instantiate filtergraph");
  
  if (live->pB2->SetFiltergraph(live->pFg) != NOERROR)
    CR_error("Cannot give graph to builder");

  if (live->pFg->AddFilter(live->pVCap, NULL) != NOERROR)
    CR_error("Cannot add vidcap to filtergraph");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMStreamConfig, (void **) &(live->pVSC)) != NOERROR)
    CR_error("Cannot find stream config");

  if (!GetVideoFormat(live))
    CR_error("Cannot get video format");
  */

  tempfilename = (char *) CR_calloc(MAX_AVI_PATH, sizeof(char));
  sprintf(tempfilename, "temp.avi");

  MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, tempfilename, -1, wach_temp, MAX_AVI_PATH);
  MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, filename, -1, wach_dest, MAX_AVI_PATH);
  GUID guid = MEDIASUBTYPE_Avi;
  if (live->pB2->SetOutputFileName(&guid, wach_temp, &live->pAVIMux, &live->pSink) != NOERROR)
    CR_error("Cannot add AVI mux, file write to capture graph");

  if (live->pB2->GetFiltergraph(&live->pFg) != NOERROR)
    CR_error("Couldn't get filter graph");

  if (live->pFg->AddFilter(live->pVCap, NULL) != NOERROR)
    CR_error("Cannot add vidcap to filtergraph");

  if (live->pB2->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, live->pVCap, NULL, live->pAVIMux) != NOERROR)
    CR_error("Cannot connect capture filter to AVI mux filter");

  if (live->pFg->QueryInterface(IID_IMediaControl, (void **) &live->pMC) != NOERROR)
    CR_error("Cannot prepare to run graph");

  // now tell it what frame rate to capture at.  Just find the format it     
  // is capturing with, and leave everything alone but change the frame rate     

  /*
  live->FrameRate = 15.0;

  hr = E_FAIL;     
  if (live->pVSC) { 
    hr = live->pVSC->GetFormat(&pmt); 
    if (hr == NOERROR) {     
      pvi = (VIDEOINFOHEADER *) pmt->pbFormat;     
      pvi->AvgTimePerFrame = (LONGLONG)(10000000 / live->FrameRate);     
      hr = live->pVSC->SetFormat(pmt);     
      DeleteMediaType(pmt); 
    }     
  }     
  if (hr != NOERROR) 
    CR_error("Cannot set frame rate for capture");              
  */

  REFERENCE_TIME stop = MAX_TIME;

  // start capturing now
  if (live->pB2->ControlStream(&PIN_CATEGORY_CAPTURE, NULL, NULL, NULL, &stop, 0, 0) != NOERROR)
    CR_error("Cannot start capturing");

  if (live->pMC->Run() != NOERROR)
    CR_error("Cannot run capture graph");

  //  CTime tstart = CTime::GetCurrentTime();

  starttime = timeGetTime();

  while (1)
    if (timeGetTime() - starttime > 5000)
      break;

  stoptime = timeGetTime();

  // stop capturing now
  if (live->pMC->Stop() != NOERROR)
    CR_error("Cannot stop capture graph");

  stoptime = timeGetTime();

  //  CTime tstop = CTime::GetCurrentTime();

  //  if (live->pB2->CopyCaptureFile(wach_temp, wach_dest, FALSE, NULL) != NOERROR)
  //    CR_error("Cannot copy capture file");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMDroppedFrames, (void **)&live->pDF) != NOERROR)
    CR_error("Cannot get dropped frames interface");

  LONG lNot, lDropped;

  if (live->pDF->GetNumNotDropped(&lNot) != NOERROR)
    CR_error("Cannot get num not dropped");

  if (live->pDF->GetNumDropped(&lDropped) != NOERROR)
    CR_error("Cannot get num dropped");

  printf("num captured = %li, num missed = %li\n", lNot, lDropped);
  printf("start = %li, stop = %li, diff = %li\n", starttime, stoptime, stoptime - starttime);  // milliseconds

  /*
  if (live->pFg->AddFilter(live->rend->m_pFilter, NULL) != NOERROR)
    CR_error("Cannot add renderer filter to filtergraph");

  if (!ConnectFilters(live))
    CR_error("Cannot connect filters");

  if (!IntelCamPro_SetResolution(live, width, height))
    CR_error("Could not set intel cam pro decoder resolution");

  if (live->pFg->QueryInterface(IID_IMediaControl, (void **) &live->pMC) != NOERROR)
    CR_error("Cannot prepare to run graph");

  live->rend->m_pfnCallback = (void *) live->rend->StaticFrameCallbackProc;
  live->rend->m_cbParam = (void *) live->rend;

  if (live->pMC->Run() != NOERROR)
    CR_error("Cannot run capture graph");

  live->im = live->rend->im;
  */

  //  for (i = 0; i < 100000000; i++);
}

//----------------------------------------------------------------------------

// specifically for Intel PC Camera Pro

void IntelCamPro_KillVideo(CR_Live *live)
{


}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// specifically for FirewireDVC

void FirewireDVC_InitVideo(CR_Live *live, int width, int height, int device_num)
{
  HRESULT hr;
  CCritSec *plock;
  int i;

  live->device_type = FIREWIREDVC_VIDEO;

  //  plock = new CCritSec();
  live->rend = new CRender(NULL, &hr);
  live->rend->device_num = device_num;
  live->rend->ncam_docopy = FALSE;
  live->rend->m_pPin->dv_device = 0;
  live->rend->m_pPin->nondv_width = width;
  live->rend->m_pPin->nondv_height = height;

  //  CR_flush_printf("w = %i, h = %i\n", width, height);

  live->pB2 = NULL;
  live->pFg = NULL;

  //  CoInitialize(NULL);

  if (!MakeBuilder(live))
    CR_error("Cannot instantiate graph builder");

  //  printf("unibrain fire-i init device num = %i\n", device_num);

  PickConnectedDevice(live, device_num);   // 0 is default index
  //  PickConnectedDevice(live, 0);   // 0 is default index

  if (!MakeGraph(live))
    CR_error("Cannot instantiate filtergraph");
  
  if (live->pB2->SetFiltergraph(live->pFg) != NOERROR)
    CR_error("Cannot give graph to builder");

  if (live->pFg->AddFilter(live->pVCap, NULL) != NOERROR)
    CR_error("Cannot add vidcap to filtergraph");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMStreamConfig, (void **) &(live->pVSC)) != NOERROR)
    CR_error("Cannot find stream config");

  if (!GetVideoFormat(live))
    CR_error("Cannot get video format");

  if (live->pFg->AddFilter(live->rend->m_pFilter, NULL) != NOERROR)
    CR_error("Cannot add renderer filter to filtergraph");

  // [DV section is here]

  // [DV connection function is called instead]

  if (!ConnectFilters(live))
    CR_error("Cannot connect filters");

  if (!FirewireDVC_SetResolution(live, width, height))
    CR_error("Could not set Unibrain Fire-i decoder resolution");

  if (live->pFg->QueryInterface(IID_IMediaControl, (void **) &live->pMC) != NOERROR)
    CR_error("Cannot prepare to run graph");

  live->rend->m_pfnCallback = (void *) live->rend->StaticFrameCallbackProc;
  live->rend->m_cbParam = (void *) live->rend;

  if (live->pMC->Run() != NOERROR)
    CR_error("Cannot run capture graph");

  live->im = live->rend->im;

  // [DV waits a bit here]

  //  for (i = 0; i < 100000000; i++);

}

//----------------------------------------------------------------------------

// 12 bits per pixel (16?)

int FirewireDVC_SetResolution(CR_Live *live, int w, int h)
{
  live->width = w;
  live->height = h;

  live->size = 2 * live->width * live->height;
  //  live->size = live->height * ((live->width + 3) & ~3);
  live->rend->dib = (BITMAPINFO *) CR_malloc(sizeof(BITMAPINFOHEADER) + live->size);
  live->rend->dibh = (BITMAPINFOHEADER *) live->rend->dib;

  live->rend->dibh->biSize = sizeof(BITMAPINFOHEADER); 
  live->rend->dibh->biWidth = live->width;         // fill in width from parameter 
  live->rend->dibh->biHeight = live->height;       // was -live->height - fill in height from parameter 
  live->rend->dibh->biPlanes = 1;                  // must be 1 
  live->rend->dibh->biBitCount = 24;               // from parameter 
  live->rend->dibh->biCompression = BI_RGB;     
  live->rend->dibh->biSizeImage = 0;               // 0's here mean "default" 
  live->rend->dibh->biXPelsPerMeter = 2700;
  live->rend->dibh->biYPelsPerMeter = 2700;
  live->rend->dibh->biClrUsed = 0; 
  live->rend->dibh->biClrImportant = 0; 

  printf("ALLOCATING 1394desktopvideocamera bytes = %i (w = %i, h = %i)\n", live->size, live->width, live->height);
  live->rend->bits = (unsigned char *) ((char *) live->rend->dib + sizeof(BITMAPINFOHEADER));
  live->rend->im = make_CR_Image(live->width, live->height, 0);
  live->rend->flipim = make_CR_Image(live->width, live->height, 0);
  live->rend->yuvim = make_CR_Image(live->width, live->height, IPL_TYPE_COLOR_8U_YUV);

  live->calibim = make_CR_Image(live->width, live->height, 0);

  return TRUE;
}

//----------------------------------------------------------------------------

// specifically for Unibrain Fire-I

void FirewireDVC_CaptureVideo(char *filename, CR_Live *live, int width, int height, int device_num)
{
  HRESULT hr;
  CCritSec *plock;
  int i;
  char *tempfilename;
  WCHAR wach_temp[MAX_AVI_PATH], wach_dest[MAX_AVI_PATH];
  long starttime, stoptime;
  AM_MEDIA_TYPE *pmt; 
  VIDEOINFOHEADER *pvi;

  live->device_type = UNIBRAINFIREI_VIDEO;

  //  plock = new CCritSec();
  /*
  live->rend = new CRender(NULL, &hr);
  live->rend->m_pPin->dv_device = 0;
  live->rend->m_pPin->nondv_width = width;
  live->rend->m_pPin->nondv_height = height;
  */

  CR_flush_printf("w = %i, h = %i\n", width, height);

  live->pB2 = NULL;
  live->pFg = NULL;

  //  CoInitialize(NULL);

  if (!MakeBuilder(live))
    CR_error("Cannot instantiate graph builder");

  //  printf("unibrain fire-i init device num = %i\n", device_num);

  PickConnectedDevice(live, device_num);   // 0 is default index
  //  PickConnectedDevice(live, 0);   // 0 is default index

    
  /*
  if (!MakeGraph(live))
    CR_error("Cannot instantiate filtergraph");
  
  if (live->pB2->SetFiltergraph(live->pFg) != NOERROR)
    CR_error("Cannot give graph to builder");

  if (live->pFg->AddFilter(live->pVCap, NULL) != NOERROR)
    CR_error("Cannot add vidcap to filtergraph");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMStreamConfig, (void **) &(live->pVSC)) != NOERROR)
    CR_error("Cannot find stream config");

  if (!GetVideoFormat(live))
    CR_error("Cannot get video format");
  */

//   tempfilename = (char *) CR_calloc(MAX_AVI_PATH, sizeof(char));
//   sprintf(tempfilename, "temp.avi");
// 
//   MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, tempfilename, -1, wach_temp, MAX_AVI_PATH);
  MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, filename, -1, wach_dest, MAX_AVI_PATH);
  GUID guid = MEDIASUBTYPE_Avi;
  //  if (live->pB2->SetOutputFileName(&guid, wach_temp, &live->pAVIMux, &live->pSink) != NOERROR)
  if (live->pB2->SetOutputFileName(&guid, wach_dest, &live->pAVIMux, &live->pSink) != NOERROR)
    CR_error("Cannot add AVI mux, file write to capture graph");

  if (live->pB2->GetFiltergraph(&live->pFg) != NOERROR)
    CR_error("Couldn't get filter graph");

  if (live->pFg->AddFilter(live->pVCap, NULL) != NOERROR)
    CR_error("Cannot add vidcap to filtergraph");

  if (live->pB2->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, live->pVCap, NULL, live->pAVIMux) != NOERROR)
    CR_error("Cannot connect capture filter to AVI mux filter");

  if (live->pFg->QueryInterface(IID_IMediaControl, (void **) &live->pMC) != NOERROR)
    CR_error("Cannot prepare to run graph");

  // now tell it what frame rate to capture at.  Just find the format it     
  // is capturing with, and leave everything alone but change the frame rate     

  /*
  live->FrameRate = 15.0;

  hr = E_FAIL;     
  if (live->pVSC) { 
    hr = live->pVSC->GetFormat(&pmt); 
    if (hr == NOERROR) {     
      pvi = (VIDEOINFOHEADER *) pmt->pbFormat;     
      //      pvi->AvgTimePerFrame = (LONGLONG)(10000000 / live->FrameRate);     
      pvi->AvgTimePerFrame = (LONGLONG)666666;
      hr = live->pVSC->SetFormat(pmt);     
      DeleteMediaType(pmt); 
    }     
  }     
  if (hr != NOERROR) 
    CR_error("Cannot set frame rate for capture");              
  else
    CR_error("did set frame rate");
  */

  REFERENCE_TIME stop = MAX_TIME;

  // start capturing now
  if (live->pB2->ControlStream(&PIN_CATEGORY_CAPTURE, NULL, NULL, NULL, &stop, 0, 0) != NOERROR)
    CR_error("Cannot start capturing");

  if (live->pMC->Run() != NOERROR)
    CR_error("Cannot run capture graph");

  //  CTime tstart = CTime::GetCurrentTime();

  starttime = timeGetTime();

  while (1)
    if (timeGetTime() - starttime > 5000)
      break;

  stoptime = timeGetTime();

  // stop capturing now
  if (live->pMC->Stop() != NOERROR)
    CR_error("Cannot stop capture graph");

  stoptime = timeGetTime();

  //  CTime tstop = CTime::GetCurrentTime();

  //  if (live->pB2->CopyCaptureFile(wach_temp, wach_dest, FALSE, NULL) != NOERROR)
  //    CR_error("Cannot copy capture file");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMDroppedFrames, (void **)&live->pDF) != NOERROR)
    CR_error("Cannot get dropped frames interface");

  LONG lNot, lDropped;

  if (live->pDF->GetNumNotDropped(&lNot) != NOERROR)
    CR_error("Cannot get num not dropped");

  if (live->pDF->GetNumDropped(&lDropped) != NOERROR)
    CR_error("Cannot get num dropped");

  printf("num captured = %li, num missed = %li\n", lNot, lDropped);
  printf("start = %li, stop = %li, diff = %li\n", starttime, stoptime, stoptime - starttime);  // milliseconds

  /*
  if (live->pFg->AddFilter(live->rend->m_pFilter, NULL) != NOERROR)
    CR_error("Cannot add renderer filter to filtergraph");

  if (!ConnectFilters(live))
    CR_error("Cannot connect filters");

  if (!FirewireDVC_SetResolution(live, width, height))
    CR_error("Could not set unibrain fire-i decoder resolution");

  if (live->pFg->QueryInterface(IID_IMediaControl, (void **) &live->pMC) != NOERROR)
    CR_error("Cannot prepare to run graph");

  live->rend->m_pfnCallback = (void *) live->rend->StaticFrameCallbackProc;
  live->rend->m_cbParam = (void *) live->rend;

  if (live->pMC->Run() != NOERROR)
    CR_error("Cannot run capture graph");

  live->im = live->rend->im;
  */

  //  for (i = 0; i < 100000000; i++);
}

//----------------------------------------------------------------------------

// specifically for Unibrain Fire-I

void FirewireDVC_KillVideo(CR_Live *live)
{


}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// specifically for PGRDragonfly

void PGRDragonfly_InitVideo(CR_Live *live, int width, int height, int device_num)
{
  HRESULT hr;
  CCritSec *plock;
  int i;

  live->device_type = PGRDRAGONFLY_VIDEO;

  //  plock = new CCritSec();
  live->rend = new CRender(NULL, &hr);
  live->rend->device_num = device_num;
  live->rend->ncam_docopy = FALSE;
  live->rend->m_pPin->dv_device = 0;
  live->rend->m_pPin->nondv_width = width;
  live->rend->m_pPin->nondv_height = height;

  CR_flush_printf("PGR init: w = %i, h = %i\n", width, height);

  live->pB2 = NULL;
  live->pFg = NULL;

  //  CoInitialize(NULL);

  if (!MakeBuilder(live))
    CR_error("Cannot instantiate graph builder");

  //  printf("unibrain fire-i init device num = %i\n", device_num);

  PickConnectedDevice(live, device_num);   // 0 is default index
  //  PickConnectedDevice(live, 0);   // 0 is default index

  if (!MakeGraph(live))
    CR_error("Cannot instantiate filtergraph");
  
  if (live->pB2->SetFiltergraph(live->pFg) != NOERROR)
    CR_error("Cannot give graph to builder");

  if (live->pFg->AddFilter(live->pVCap, NULL) != NOERROR)
    CR_error("Cannot add vidcap to filtergraph");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMStreamConfig, (void **) &(live->pVSC)) != NOERROR)
    CR_error("Cannot find stream config");

  if (!GetVideoFormat(live))
    CR_error("Cannot get video format");

  if (live->pFg->AddFilter(live->rend->m_pFilter, NULL) != NOERROR)
    CR_error("Cannot add renderer filter to filtergraph");

  // [DV section is here]

  // [DV connection function is called instead]

  if (!ConnectFilters(live))
    CR_error("Cannot connect filters");

  if (!PGRDragonfly_SetResolution(live, width, height))
    CR_error("Could not set PGR Dragonfly decoder resolution");

  if (live->pFg->QueryInterface(IID_IMediaControl, (void **) &live->pMC) != NOERROR)
    CR_error("Cannot prepare to run graph");

  live->rend->m_pfnCallback = (void *) live->rend->StaticFrameCallbackProc;
  live->rend->m_cbParam = (void *) live->rend;

  if (live->pMC->Run() != NOERROR)
    CR_error("Cannot run capture graph");

  live->im = live->rend->im;

  // [DV waits a bit here]

  //  for (i = 0; i < 100000000; i++);

}

//----------------------------------------------------------------------------

// 8 bits per pixel 

int PGRDragonfly_SetResolution(CR_Live *live, int w, int h)
{
  live->width = w;
  live->height = h;

  live->size = live->width * live->height;
  //live->size = 3 * live->width * live->height;
  //  live->size = live->height * ((live->width + 3) & ~3);
  live->rend->dib = (BITMAPINFO *) CR_malloc(sizeof(BITMAPINFOHEADER) + live->size);
  live->rend->dibh = (BITMAPINFOHEADER *) live->rend->dib;

  live->rend->dibh->biSize = sizeof(BITMAPINFOHEADER); 
  live->rend->dibh->biWidth = live->width;         // fill in width from parameter 
  live->rend->dibh->biHeight = live->height;       // was -live->height - fill in height from parameter 
  live->rend->dibh->biPlanes = 1;                  // must be 1 
  //  live->rend->dibh->biBitCount = 8;               // from parameter 
  live->rend->dibh->biBitCount = 24;               // from parameter 
  live->rend->dibh->biCompression = BI_RGB;     
  live->rend->dibh->biSizeImage = 0;               // 0's here mean "default" 
  live->rend->dibh->biXPelsPerMeter = 2700;
  live->rend->dibh->biYPelsPerMeter = 2700;
  live->rend->dibh->biClrUsed = 0; 
  live->rend->dibh->biClrImportant = 0; 

  printf("ALLOCATING dragonfly bytes = %i (w = %i, h = %i)\n", live->size, live->width, live->height);
  live->rend->bits = (unsigned char *) ((char *) live->rend->dib + sizeof(BITMAPINFOHEADER));
  live->rend->im = make_CR_Image(live->width, live->height, 0);
  live->rend->flipim = make_CR_Image(live->width, live->height, 0);
  live->rend->yuvim = make_CR_Image(live->width, live->height, IPL_TYPE_COLOR_8U_YUV);

  live->calibim = make_CR_Image(live->width, live->height, 0);

  return TRUE;
}

//----------------------------------------------------------------------------

// specifically for PGR Dragonfly

void PGRDragonfly_CaptureVideo(char *filename, CR_Live *live, int width, int height, int device_num)
{
  HRESULT hr;
  CCritSec *plock;
  int i;
  char *tempfilename;
  WCHAR wach_temp[MAX_AVI_PATH], wach_dest[MAX_AVI_PATH];
  long starttime, stoptime;
  AM_MEDIA_TYPE *pmt; 
  VIDEOINFOHEADER *pvi;

  //  live->device_type = UNIBRAINFIREI_VIDEO;
  live->device_type = PGRDRAGONFLY_VIDEO;

  //  plock = new CCritSec();
  /*
  live->rend = new CRender(NULL, &hr);
  live->rend->m_pPin->dv_device = 0;
  live->rend->m_pPin->nondv_width = width;
  live->rend->m_pPin->nondv_height = height;
  */

  CR_flush_printf("w = %i, h = %i\n", width, height);

  live->pB2 = NULL;
  live->pFg = NULL;

  //  CoInitialize(NULL);

  if (!MakeBuilder(live))
    CR_error("Cannot instantiate graph builder");

  //  printf("unibrain fire-i init device num = %i\n", device_num);

  PickConnectedDevice(live, device_num);   // 0 is default index
  //  PickConnectedDevice(live, 0);   // 0 is default index

    
  /*
  if (!MakeGraph(live))
    CR_error("Cannot instantiate filtergraph");
  
  if (live->pB2->SetFiltergraph(live->pFg) != NOERROR)
    CR_error("Cannot give graph to builder");

  if (live->pFg->AddFilter(live->pVCap, NULL) != NOERROR)
    CR_error("Cannot add vidcap to filtergraph");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMStreamConfig, (void **) &(live->pVSC)) != NOERROR)
    CR_error("Cannot find stream config");

  if (!GetVideoFormat(live))
    CR_error("Cannot get video format");
  */

//   tempfilename = (char *) CR_calloc(MAX_AVI_PATH, sizeof(char));
//   sprintf(tempfilename, "temp.avi");
// 
//   MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, tempfilename, -1, wach_temp, MAX_AVI_PATH);
  MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, filename, -1, wach_dest, MAX_AVI_PATH);
  GUID guid = MEDIASUBTYPE_Avi;
  //  if (live->pB2->SetOutputFileName(&guid, wach_temp, &live->pAVIMux, &live->pSink) != NOERROR)
  if (live->pB2->SetOutputFileName(&guid, wach_dest, &live->pAVIMux, &live->pSink) != NOERROR)
    CR_error("Cannot add AVI mux, file write to capture graph");

  if (live->pB2->GetFiltergraph(&live->pFg) != NOERROR)
    CR_error("Couldn't get filter graph");

  if (live->pFg->AddFilter(live->pVCap, NULL) != NOERROR)
    CR_error("Cannot add vidcap to filtergraph");

  if (live->pB2->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, live->pVCap, NULL, live->pAVIMux) != NOERROR)
    CR_error("Cannot connect capture filter to AVI mux filter");

  if (live->pFg->QueryInterface(IID_IMediaControl, (void **) &live->pMC) != NOERROR)
    CR_error("Cannot prepare to run graph");

  // now tell it what frame rate to capture at.  Just find the format it     
  // is capturing with, and leave everything alone but change the frame rate     

  /*
  live->FrameRate = 15.0;

  hr = E_FAIL;     
  if (live->pVSC) { 
    hr = live->pVSC->GetFormat(&pmt); 
    if (hr == NOERROR) {     
      pvi = (VIDEOINFOHEADER *) pmt->pbFormat;     
      //      pvi->AvgTimePerFrame = (LONGLONG)(10000000 / live->FrameRate);     
      pvi->AvgTimePerFrame = (LONGLONG)666666;
      hr = live->pVSC->SetFormat(pmt);     
      DeleteMediaType(pmt); 
    }     
  }     
  if (hr != NOERROR) 
    CR_error("Cannot set frame rate for capture");              
  else
    CR_error("did set frame rate");
  */

  REFERENCE_TIME stop = MAX_TIME;

  // start capturing now
  if (live->pB2->ControlStream(&PIN_CATEGORY_CAPTURE, NULL, NULL, NULL, &stop, 0, 0) != NOERROR)
    CR_error("Cannot start capturing");

  if (live->pMC->Run() != NOERROR)
    CR_error("Cannot run capture graph");

  //  CTime tstart = CTime::GetCurrentTime();

  starttime = timeGetTime();

  // how long to capture for

  while (1)
    if (timeGetTime() - starttime > 5000)
      break;

  stoptime = timeGetTime();

  // stop capturing now
  if (live->pMC->Stop() != NOERROR)
    CR_error("Cannot stop capture graph");

  stoptime = timeGetTime();

  //  CTime tstop = CTime::GetCurrentTime();

  //  if (live->pB2->CopyCaptureFile(wach_temp, wach_dest, FALSE, NULL) != NOERROR)
  //    CR_error("Cannot copy capture file");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMDroppedFrames, (void **)&live->pDF) != NOERROR)
    CR_error("Cannot get dropped frames interface");

  LONG lNot, lDropped;

  if (live->pDF->GetNumNotDropped(&lNot) != NOERROR)
    CR_error("Cannot get num not dropped");

  if (live->pDF->GetNumDropped(&lDropped) != NOERROR)
    CR_error("Cannot get num dropped");

  printf("num captured = %li, num missed = %li\n", lNot, lDropped);
  printf("start = %li, stop = %li, diff = %li\n", starttime, stoptime, stoptime - starttime);  // milliseconds

  /*
  if (live->pFg->AddFilter(live->rend->m_pFilter, NULL) != NOERROR)
    CR_error("Cannot add renderer filter to filtergraph");

  if (!ConnectFilters(live))
    CR_error("Cannot connect filters");

  if (!PGRDragonfly_SetResolution(live, width, height))
    CR_error("Could not set unibrain fire-i decoder resolution");

  if (live->pFg->QueryInterface(IID_IMediaControl, (void **) &live->pMC) != NOERROR)
    CR_error("Cannot prepare to run graph");

  live->rend->m_pfnCallback = (void *) live->rend->StaticFrameCallbackProc;
  live->rend->m_cbParam = (void *) live->rend;

  if (live->pMC->Run() != NOERROR)
    CR_error("Cannot run capture graph");

  live->im = live->rend->im;
  */

  //  for (i = 0; i < 100000000; i++);
}

//----------------------------------------------------------------------------

void PGRDragonfly_KillVideo(CR_Live *live)
{


}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// specifically for Unibrain Fire-I

void UnibrainFireI_InitVideo(CR_Live *live, int width, int height, int device_num)
{
  HRESULT hr;
  CCritSec *plock;
  int i;

  live->device_type = UNIBRAINFIREI_VIDEO;

  //  plock = new CCritSec();
  live->rend = new CRender(NULL, &hr);
  live->rend->device_num = device_num;
  live->rend->ncam_docopy = FALSE;
  live->rend->m_pPin->dv_device = 0;
  live->rend->m_pPin->nondv_width = width;
  live->rend->m_pPin->nondv_height = height;

  //  CR_flush_printf("w = %i, h = %i\n", width, height);

  live->pB2 = NULL;
  live->pFg = NULL;

  //  CoInitialize(NULL);

  if (!MakeBuilder(live))
    CR_error("Cannot instantiate graph builder");

  //  printf("unibrain fire-i init device num = %i\n", device_num);

  PickConnectedDevice(live, device_num);   // 0 is default index
  //  PickConnectedDevice(live, 0);   // 0 is default index

  if (!MakeGraph(live))
    CR_error("Cannot instantiate filtergraph");
  
  if (live->pB2->SetFiltergraph(live->pFg) != NOERROR)
    CR_error("Cannot give graph to builder");

  if (live->pFg->AddFilter(live->pVCap, NULL) != NOERROR)
    CR_error("Cannot add vidcap to filtergraph");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMStreamConfig, (void **) &(live->pVSC)) != NOERROR)
    CR_error("Cannot find stream config");

  if (!GetVideoFormat(live))
    CR_error("Cannot get video format");

  if (live->pFg->AddFilter(live->rend->m_pFilter, NULL) != NOERROR)
    CR_error("Cannot add renderer filter to filtergraph");

  // [DV section is here]

  // [DV connection function is called instead]

  if (!ConnectFilters(live))
    CR_error("Cannot connect filters");

  if (!UnibrainFireI_SetResolution(live, width, height))
    CR_error("Could not set Unibrain Fire-i decoder resolution");

  if (live->pFg->QueryInterface(IID_IMediaControl, (void **) &live->pMC) != NOERROR)
    CR_error("Cannot prepare to run graph");

  live->rend->m_pfnCallback = (void *) live->rend->StaticFrameCallbackProc;
  live->rend->m_cbParam = (void *) live->rend;

  if (live->pMC->Run() != NOERROR)
    CR_error("Cannot run capture graph");

  live->im = live->rend->im;

  // [DV waits a bit here]

  //  for (i = 0; i < 100000000; i++);
}

//----------------------------------------------------------------------------

// 12 bits per pixel

int UnibrainFireI_SetResolution(CR_Live *live, int w, int h)
{
  live->width = w;
  live->height = h;

  live->size = 2 * live->width * live->height;
  //  live->size = live->height * ((live->width + 3) & ~3);
  live->rend->dib = (BITMAPINFO *) CR_malloc(sizeof(BITMAPINFOHEADER) + live->size);
  live->rend->dibh = (BITMAPINFOHEADER *) live->rend->dib;

  live->rend->dibh->biSize = sizeof(BITMAPINFOHEADER); 
  live->rend->dibh->biWidth = live->width;         // fill in width from parameter 
  live->rend->dibh->biHeight = live->height;       // was -live->height - fill in height from parameter 
  live->rend->dibh->biPlanes = 1;                  // must be 1 
  live->rend->dibh->biBitCount = 24;               // from parameter 
  live->rend->dibh->biCompression = BI_RGB;     
  live->rend->dibh->biSizeImage = 0;               // 0's here mean "default" 
  live->rend->dibh->biXPelsPerMeter = 2700;
  live->rend->dibh->biYPelsPerMeter = 2700;
  live->rend->dibh->biClrUsed = 0; 
  live->rend->dibh->biClrImportant = 0; 

  printf("ALLOCATING unibrain fire-i bits = %i (w = %i, h = %i)\n", live->size, live->width, live->height);
  live->rend->bits = (unsigned char *) ((char *) live->rend->dib + sizeof(BITMAPINFOHEADER));
  live->rend->im = make_CR_Image(live->width, live->height, 0);
  live->rend->flipim = make_CR_Image(live->width, live->height, 0);
  live->rend->yuvim = make_CR_Image(live->width, live->height, IPL_TYPE_COLOR_8U_YUV);

  live->calibim = make_CR_Image(live->width, live->height, 0);

  return TRUE;
}

//----------------------------------------------------------------------------

void kill_avicapture_live_source(CR_Live *live)
{

}

//----------------------------------------------------------------------------

void setfilename_avicapture_live_source(char *filename, CR_Live *live)
{
  WCHAR wach_dest[MAX_AVI_PATH];

  MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, filename, -1, wach_dest, MAX_AVI_PATH);
  GUID guid = MEDIASUBTYPE_Avi;

  live->pSink->SetFileName(wach_dest, live->pmt);

//   if (live->pB2->SetOutputFileName(&guid, wach_dest, &live->pAVIMux, &live->pSink) != NOERROR)
//     CR_error("Cannot add AVI mux, file write to capture graph");
}

//----------------------------------------------------------------------------

CR_Live *init_avicapture_live_source(char *filename, int width, int height, int device_num, char *device_name)
{
  CR_Live *live;
  REFERENCE_TIME start;

  start = MAX_TIME;

  live = (CR_Live *) CR_malloc(sizeof(CR_Live));
  live->calibration = NULL;

  if (!strcmp(device_name, "Microsoft DV Camera and VCR")) 
    //    DV_InitAVICapture(filename, live, width, height, device_num);
    DV_InitAVICapture2(filename, live, width, height, device_num);
  //  else if (!strcmp(device_name, "Intel PC Camera Pro"))
  else if (!strncmp(device_name, "Intel", 5))
    IntelCamPro_InitAVICapture(filename, live, width, height, device_num);
  //  else if (!strncmp(device_name, "Unibrain Fire-i 1394 Camera Driver", 34))
  else if (!strncmp(device_name, "Unibrain Fire-i", 15))
//   else if (!strcmp(device_name, "Unibrain Fire-i 1394 Camera Driver (Unibrain)") ||
//       !strcmp(device_name, "Unibrain Fire-i 1394 Camera Driver (ADS Pyro)"))
    UnibrainFireI_InitAVICapture(filename, live, width, height, device_num);
  else if (!strcmp(device_name, "1394 Desktop Video Camera"))
    FirewireDVC_CaptureVideo(filename, live, width, height, device_num);
  else
    CR_error("unsupported video device 3");

  // get everything ready

  if (live->pB2->ControlStream(&PIN_CATEGORY_CAPTURE, NULL, NULL, &start, NULL, 0, 0))
    CR_error("Cannot prepare capture graph");
  
  if (live->pMC->Run() != NOERROR)
    CR_error("Cannot run capture graph");
  
  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMDroppedFrames, (void **)&live->pDF) != NOERROR)
    CR_error("Cannot get dropped frames interface");

  return live;
}

//----------------------------------------------------------------------------

// captures n streams for a specified number of seconds

// 0 seconds indicates that NML message should be used for stopping

void run_avicapture_live_multisource(double seconds, CR_Live **live, int n)
{
  HRESULT hr;
  int i;
  REFERENCE_TIME start, stop;
  int milliseconds;
  int hascontrol;

  start = MAX_TIME;
  stop = MAX_TIME;

  // get everything ready (put in init?)

  /*
  for (i = 0; i < n; i++) {

    if (live[i]->pB2->ControlStream(&PIN_CATEGORY_CAPTURE, NULL, NULL, &start, NULL, 0, 0))
      CR_error("Cannot prepare capture graph");

    if (live[i]->pMC->Run() != NOERROR)
      CR_error("Cannot run capture graph");

    if (live[i]->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live[i]->pVCap, IID_IAMDroppedFrames, (void **)&live[i]->pDF) != NOERROR)
      CR_error("Cannot get dropped frames interface");
  }
  */

  // check start time

  live[0]->starttime = timeGetTime();

  // start capturing everything now

  for (i = 0; i < n; i++) {

    if (live[i]->pB2->ControlStream(&PIN_CATEGORY_CAPTURE, NULL, NULL, NULL, &stop, 0, 0) != NOERROR)
      CR_error("Cannot start capturing");

  }

  // timed version

  if (seconds) {

    milliseconds = (int) floor(1000.0 * seconds);
    
    while (1) {
      if (timeGetTime() - live[0]->starttime > milliseconds) {
	
	// stop capturing everything now
	
	for (i = 0; i < n; i++)
	  if (live[i]->pMC->Stop() != NOERROR)
	    CR_error("Cannot stop capture graph");
	
	break;
      }
    }
  }

  // NML version

  else {

    seconds = 180.0;  // record for 3 minutes

    milliseconds = (int) floor(1000.0 * seconds);
    
    while (1) {
      if (timeGetTime() - live[0]->starttime > milliseconds) {
	
	// stop capturing everything now
	
	for (i = 0; i < n; i++)
	  if (live[i]->pMC->Stop() != NOERROR)
	    CR_error("Cannot stop capture graph");
	
	break;
      }
    }

    /*
    while (1) {     
      
      //      printf("waiting for stop...\n");

      if (stop_data_collection_CR_NML()) {
	
	// stop capturing everything now

	for (i = 0; i < n; i++)
 	  if (live[i]->pMC->Stop() != NOERROR)
	    CR_error("Cannot stop capture graph");

	CR_flush_printf("got stop...\n");

	break;
      }
    }
    */
  }

  // check stop time

  live[0]->stoptime = timeGetTime();

  // note dropped frames, etc.

  for (i = 0; i < n; i++) {

    if (live[i]->pDF->GetNumNotDropped(&(live[i]->lNot)) != NOERROR)
      CR_error("Cannot get num not dropped");

    if (live[i]->pDF->GetNumDropped(&(live[i]->lDropped)) != NOERROR)
      CR_error("Cannot get num dropped");

    live[i]->numtotal = live[i]->lNot + live[i]->lDropped;
    live[i]->dropped = (long *) CR_calloc(live[i]->numtotal, sizeof(long));
    //    live[i]->dropped = (int *) CR_calloc(live[i]->numtotal, sizeof(int));

    if (live[i]->pDF->GetDroppedInfo(live[i]->numtotal, live[i]->dropped, &(live[i]->numrecorded)) != NOERROR)
      CR_error("Cannot get dropped frame info");
  }
}

//----------------------------------------------------------------------------

// report on how long capture was, how many frames dropped, etc.

void report_avicapture_live_multisource(CR_Live **live, int n, FILE *logfp)
{
  int i;

  for (i = 0; i < n; i++) 
    fprintf(logfp, "Cam %i: Got = %li frames, Dropped = %li; Start = %li ms, Stop = %li, Diff = %li\n", i, live[i]->lNot, live[i]->lDropped, live[0]->starttime, live[0]->stoptime, live[0]->stoptime - live[0]->starttime); // time is in milliseconds

  fflush(logfp);
}

//----------------------------------------------------------------------------

void kill_avicapture_live_multisource(CR_Live **live, int n)
{
  int i;

  // kill all the capture graphs so we can start again fresh

  for (i = 0; i < n; i++)
    kill_live_source(live[i]);
}

//----------------------------------------------------------------------------

void IntelCamPro_InitAVICapture(char *filename, CR_Live *live, int width, int height, int device_num)
{
  WCHAR wach_dest[MAX_AVI_PATH];

  live->device_type = INTELCAMPRO_VIDEO;

  //  CR_flush_printf("w = %i, h = %i\n", width, height);

  live->pB2 = NULL;
  live->pFg = NULL;

  //  CoInitialize(NULL);

  if (!MakeBuilder(live))
    CR_error("Cannot instantiate graph builder");

  PickConnectedDevice(live, device_num);   // 0 is default index
    
  MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, filename, -1, wach_dest, MAX_AVI_PATH);
  GUID guid = MEDIASUBTYPE_Avi;

  if (live->pB2->SetOutputFileName(&guid, wach_dest, &live->pAVIMux, &live->pSink) != NOERROR)
    CR_error("Cannot add AVI mux, file write to capture graph");

  if (live->pB2->GetFiltergraph(&live->pFg) != NOERROR)
    CR_error("Couldn't get filter graph");

  if (live->pFg->AddFilter(live->pVCap, NULL) != NOERROR)
    CR_error("Cannot add vidcap to filtergraph");

  if (live->pB2->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, live->pVCap, NULL, live->pAVIMux) != NOERROR)
    CR_error("Cannot connect capture filter to AVI mux filter");

  if (live->pFg->QueryInterface(IID_IMediaControl, (void **) &live->pMC) != NOERROR)
    CR_error("Cannot prepare to run graph");
}

//----------------------------------------------------------------------------

void UnibrainFireI_InitAVICapture(char *filename, CR_Live *live, int width, int height, int device_num)
{
  WCHAR wach_dest[MAX_AVI_PATH];

  live->device_type = UNIBRAINFIREI_VIDEO;

  //  CR_flush_printf("w = %i, h = %i\n", width, height);

  live->pB2 = NULL;
  live->pFg = NULL;

  //  CoInitialize(NULL);

  if (!MakeBuilder(live))
    CR_error("Cannot instantiate graph builder");

  PickConnectedDevice(live, device_num);   // 0 is default index

  //  setfilename_avicapture_live_source(filename, live);

  MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, filename, -1, wach_dest, MAX_AVI_PATH);
  GUID guid = MEDIASUBTYPE_Avi;

//    live->wCapFileSize = 150;  // megabytes
//    if (live->pB2->AllocCapFile(wach_dest, (DWORDLONG) live->wCapFileSize * 1024L * 1024L) != NOERROR) 
//      CR_error("Cannot preallocate AVI file");

  if (live->pB2->SetOutputFileName(&guid, wach_dest, &live->pAVIMux, &live->pSink) != NOERROR)
    CR_error("Cannot add AVI mux, file write to capture graph");

  if (live->pB2->GetFiltergraph(&live->pFg) != NOERROR)
    CR_error("Couldn't get filter graph");

  if (live->pFg->AddFilter(live->pVCap, NULL) != NOERROR)
    CR_error("Cannot add vidcap to filtergraph");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMStreamConfig, (void **) &(live->pVSC)) != NOERROR)
    CR_error("Cannot find stream config");

  if (live->pVSC->GetFormat(&live->pmt) != NOERROR)
    CR_error("Cannot get stream format");

  if (live->pB2->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, live->pVCap, NULL, live->pAVIMux) != NOERROR)
    CR_error("Cannot connect capture filter to AVI mux filter");

  if (live->pFg->QueryInterface(IID_IMediaControl, (void **) &live->pMC) != NOERROR)
    CR_error("Cannot prepare to run graph");
}

//----------------------------------------------------------------------------

// specifically for Unibrain Fire-I

void UnibrainFireI_CaptureVideo(char *filename, CR_Live *live, int width, int height, int device_num)
{
  HRESULT hr;
  CCritSec *plock;
  int i;
  char *tempfilename;
  WCHAR wach_temp[MAX_AVI_PATH], wach_dest[MAX_AVI_PATH];
  long starttime, stoptime;
  AM_MEDIA_TYPE *pmt; 
  VIDEOINFOHEADER *pvi;

  live->device_type = UNIBRAINFIREI_VIDEO;

  //  plock = new CCritSec();
  /*
  live->rend = new CRender(NULL, &hr);
  live->rend->m_pPin->dv_device = 0;
  live->rend->m_pPin->nondv_width = width;
  live->rend->m_pPin->nondv_height = height;
  */

  CR_flush_printf("w = %i, h = %i\n", width, height);

  live->pB2 = NULL;
  live->pFg = NULL;

  //  CoInitialize(NULL);

  if (!MakeBuilder(live))
    CR_error("Cannot instantiate graph builder");

  //  printf("unibrain fire-i init device num = %i\n", device_num);

  PickConnectedDevice(live, device_num);   // 0 is default index
  //  PickConnectedDevice(live, 0);   // 0 is default index

    
  /*
  if (!MakeGraph(live))
    CR_error("Cannot instantiate filtergraph");
  
  if (live->pB2->SetFiltergraph(live->pFg) != NOERROR)
    CR_error("Cannot give graph to builder");

  if (live->pFg->AddFilter(live->pVCap, NULL) != NOERROR)
    CR_error("Cannot add vidcap to filtergraph");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMStreamConfig, (void **) &(live->pVSC)) != NOERROR)
    CR_error("Cannot find stream config");

  if (!GetVideoFormat(live))
    CR_error("Cannot get video format");
  */

//   tempfilename = (char *) CR_calloc(MAX_AVI_PATH, sizeof(char));
//   sprintf(tempfilename, "temp.avi");
// 
//   MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, tempfilename, -1, wach_temp, MAX_AVI_PATH);
  MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, filename, -1, wach_dest, MAX_AVI_PATH);
  GUID guid = MEDIASUBTYPE_Avi;
  //  if (live->pB2->SetOutputFileName(&guid, wach_temp, &live->pAVIMux, &live->pSink) != NOERROR)
  if (live->pB2->SetOutputFileName(&guid, wach_dest, &live->pAVIMux, &live->pSink) != NOERROR)
    CR_error("Cannot add AVI mux, file write to capture graph");

  if (live->pB2->GetFiltergraph(&live->pFg) != NOERROR)
    CR_error("Couldn't get filter graph");

  if (live->pFg->AddFilter(live->pVCap, NULL) != NOERROR)
    CR_error("Cannot add vidcap to filtergraph");

  if (live->pB2->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, live->pVCap, NULL, live->pAVIMux) != NOERROR)
    CR_error("Cannot connect capture filter to AVI mux filter");

  if (live->pFg->QueryInterface(IID_IMediaControl, (void **) &live->pMC) != NOERROR)
    CR_error("Cannot prepare to run graph");

  // now tell it what frame rate to capture at.  Just find the format it     
  // is capturing with, and leave everything alone but change the frame rate     

  /*
  live->FrameRate = 15.0;

  hr = E_FAIL;     
  if (live->pVSC) { 
    hr = live->pVSC->GetFormat(&pmt); 
    if (hr == NOERROR) {     
      pvi = (VIDEOINFOHEADER *) pmt->pbFormat;     
      pvi->AvgTimePerFrame = (LONGLONG)(10000000 / live->FrameRate);     
      hr = live->pVSC->SetFormat(pmt);     
      DeleteMediaType(pmt); 
    }     
  }     
  if (hr != NOERROR) 
    CR_error("Cannot set frame rate for capture");              
  */

  REFERENCE_TIME stop = MAX_TIME;

  // start capturing now
  if (live->pB2->ControlStream(&PIN_CATEGORY_CAPTURE, NULL, NULL, NULL, &stop, 0, 0) != NOERROR)
    CR_error("Cannot start capturing");

  if (live->pMC->Run() != NOERROR)
    CR_error("Cannot run capture graph");

  //  CTime tstart = CTime::GetCurrentTime();

  starttime = timeGetTime();

  while (1)
    if (timeGetTime() - starttime > 5000)
      break;

  stoptime = timeGetTime();

  // stop capturing now
  if (live->pMC->Stop() != NOERROR)
    CR_error("Cannot stop capture graph");

  stoptime = timeGetTime();

  //  CTime tstop = CTime::GetCurrentTime();

  //  if (live->pB2->CopyCaptureFile(wach_temp, wach_dest, FALSE, NULL) != NOERROR)
  //    CR_error("Cannot copy capture file");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMDroppedFrames, (void **)&live->pDF) != NOERROR)
    CR_error("Cannot get dropped frames interface");

  LONG lNot, lDropped;

  if (live->pDF->GetNumNotDropped(&lNot) != NOERROR)
    CR_error("Cannot get num not dropped");

  if (live->pDF->GetNumDropped(&lDropped) != NOERROR)
    CR_error("Cannot get num dropped");

  printf("num captured = %li, num missed = %li\n", lNot, lDropped);
  printf("start = %li, stop = %li, diff = %li\n", starttime, stoptime, stoptime - starttime);  // milliseconds

  /*
  if (live->pFg->AddFilter(live->rend->m_pFilter, NULL) != NOERROR)
    CR_error("Cannot add renderer filter to filtergraph");

  if (!ConnectFilters(live))
    CR_error("Cannot connect filters");

  if (!UnibrainFireI_SetResolution(live, width, height))
    CR_error("Could not set unibrain fire-i decoder resolution");

  if (live->pFg->QueryInterface(IID_IMediaControl, (void **) &live->pMC) != NOERROR)
    CR_error("Cannot prepare to run graph");

  live->rend->m_pfnCallback = (void *) live->rend->StaticFrameCallbackProc;
  live->rend->m_cbParam = (void *) live->rend;

  if (live->pMC->Run() != NOERROR)
    CR_error("Cannot run capture graph");

  live->im = live->rend->im;
  */

  //  for (i = 0; i < 100000000; i++);
}

//----------------------------------------------------------------------------

// specifically for Unibrain Fire-I

void UnibrainFireI_KillVideo(CR_Live *live)
{


}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// specifically for compressed DV video that comes out of DV camcorders and 
// the Dazzle Hollywood DV-Bridge converter

void DV_InitVideo(CR_Live *live, int width, int height, int device_num)
{
  HRESULT hr;
  CCritSec *plock;
  int i;

  live->device_type = DV_VIDEO;

  //  plock = new CCritSec();
  live->rend = new CRender(NULL, &hr);
  live->rend->device_num = device_num;
  live->rend->ncam_docopy = FALSE;
  live->rend->m_pPin->dv_device = 1;

  live->pB2 = NULL;
  live->pFg = NULL;

  //  CoInitialize(NULL);

  if (!MakeBuilder(live))
    CR_error("Cannot instantiate graph builder");

  //  printf("dv init device num = %i\n", device_num);

  PickConnectedDevice(live, device_num);   // 0 is default index
  //  PickConnectedDevice(live, 0);   // 0 is default index

  if (!MakeGraph(live))
    CR_error("Cannot instantiate filtergraph");
  
  if (live->pB2->SetFiltergraph(live->pFg) != NOERROR)
    CR_error("Cannot give graph to builder");

  if (live->pFg->AddFilter(live->pVCap, NULL) != NOERROR)
    CR_error("Cannot add vidcap to filtergraph");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMStreamConfig, (void **) &(live->pVSC)) != NOERROR)
    CR_error("Cannot find stream config");

  CR_flush_printf("here!!!!\n");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMExtTransport, (void **) &(live->pExtTrans)) != NOERROR)
    CR_error("Cannot find transport mechanism control");

  //        g_pBuilder->FindInterface(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, g_pDVCamera, IID_IAMExtTransport, reinterpret_cast<PVOID*>(&g_pExtTrans));

  if (!GetVideoFormat(live))
    CR_error("Cannot get video format");

  if (live->pFg->AddFilter(live->rend->m_pFilter, NULL) != NOERROR)
    CR_error("Cannot add renderer filter to filtergraph");

  // start DV

  if (CoCreateInstance(CLSID_DVVideoCodec, NULL, CLSCTX_INPROC_SERVER,
		       IID_IBaseFilter, (void**)&live->pDVDecoder) != NOERROR)
    CR_error("Can't create DV decoder filter");

  if (live->pFg->AddFilter(live->pDVDecoder, NULL) != NOERROR)
    CR_error("Cannot add DV decoder filter to filtergraph");
        
  if (live->pDVDecoder->QueryInterface(VIS_IID_IIPDVDec, (void**) &live->pIIPDVDec) != NOERROR)
    CR_error("Cannot get interface to DV decoder settings" );

  // end DV

  if (!DV_ConnectFilters(live))
    CR_error("Cannot connect DV filters");

  if (!DV_SetResolution(live, width, height))
    CR_error("Could not set decoder resolution");

  if (live->pFg->QueryInterface(IID_IMediaControl, (void **) &live->pMC) != NOERROR)
    CR_error("Cannot prepare to run graph");

  live->rend->m_pfnCallback = (void *) live->rend->StaticFrameCallbackProc;
  live->rend->m_cbParam = (void *) live->rend;

  //  REFERENCE_TIME stop = MAX_TIME;
  //  hr = live->pB2->ControlStream(&PIN_CATEGORY_CAPTURE, NULL, NULL, NULL, &stop, 0, 0);

  /*
  if (hr == S_OK)
    CR_error("S_OK");
  else if (hr == S_FALSE)
    CR_error("S_FALSE");
  //  else if (hr == S_TRUE)
  //    CR_error("S_TRUE");
  else if (hr == E_FAIL)
    CR_error("E_FAIL");
  else if (hr == E_INVALIDARG)
    CR_error("E_INVALIDARG");
  else if (hr == E_NOTIMPL)
    CR_error("E_NOTIMPL");
  else if (hr == E_POINTER)
    CR_error("E_POINTER");
  else if (hr == E_UNEXPECTED)
    CR_error("E_UNEXPECTED");
  else if (hr == E_NOINTERFACE)
    CR_error("E_NOINTERFACE");
  else {
    char errstr[256];

    if (AMGetErrorText(hr, errstr, 256))
      printf("%s\n", errstr);
    else
      CR_error("double error");

    printf("%#010x\n", hr);
    CR_error("unknown error code");
  }
  */

  if (live->pMC->Run() != NOERROR)
    CR_error("Cannot run capture graph");

  live->im = live->rend->im;

  for (i = 0; i < 100000000; i++);

    //    CR_error("Cannot start capturing");
}

//----------------------------------------------------------------------------

// 
void DV_InitAVICapture2(char *filename, CR_Live *live, int width, int height, int device_num)
{
  HRESULT hr;
  WCHAR wach_dest[MAX_AVI_PATH];

  live->device_type = DV_VIDEO;

  //  CR_flush_printf("w = %i, h = %i\n", width, height);

  live->pB2 = NULL;
  live->pFg = NULL;
  live->rend = new CRender(NULL, &hr);
  live->rend->device_num = device_num;
  live->rend->ncam_docopy = FALSE;

  //  CoInitialize(NULL);

  if (!MakeBuilder(live))
    CR_error("Cannot instantiate graph builder");

  PickConnectedDevice(live, device_num);   // 0 is default index

  //  setfilename_avicapture_live_source(filename, live);

  MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, filename, -1, wach_dest, MAX_AVI_PATH);
  GUID guid = MEDIASUBTYPE_Avi;

  if (live->pB2->SetOutputFileName(&guid, wach_dest, &live->pAVIMux, &live->pSink) != NOERROR)
    CR_error("Cannot add AVI mux, file write to capture graph");

  if (live->pB2->GetFiltergraph(&live->pFg) != NOERROR)
    CR_error("Couldn't get filter graph");

  if (live->pFg->AddFilter(live->pVCap, NULL) != NOERROR)
    CR_error("Cannot add vidcap to filtergraph");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMStreamConfig, (void **) &(live->pVSC)) != NOERROR)
    CR_error("Cannot find stream config");
  
  if (live->pVSC->GetFormat(&live->pmt) != NOERROR)
    CR_error("Cannot get stream format");

  // set frame rate

  if (live->pmt->formattype == FORMAT_VideoInfo) {

    VIDEOINFOHEADER *pvi = (VIDEOINFOHEADER *) live->pmt->pbFormat;
    pvi->AvgTimePerFrame = (LONGLONG)(10000000 / 30);  // denominator is frames per second
    if (live->pVSC->SetFormat(live->pmt) != NOERROR)
      CR_error("Cannot set frame rate");
  }

  // [DV here?]

  if (CoCreateInstance(CLSID_DVVideoCodec, NULL, CLSCTX_INPROC_SERVER,
		       IID_IBaseFilter, (void**)&live->pDVDecoder) != NOERROR)
    CR_error("Can't create DV decoder filter");

  if (live->pFg->AddFilter(live->pDVDecoder, NULL) != NOERROR)
    CR_error("Cannot add DV decoder filter to filtergraph");

  if (live->pDVDecoder->QueryInterface(VIS_IID_IIPDVDec, (void**) &live->pIIPDVDec) != NOERROR)
    CR_error("Cannot get interface to DV decoder settings" );

  //  if (!DV_ConnectFilters(live))
  //    CR_error("Cannot connect DV filters");

  if (!DV_SetResolution(live, width, height))
    CR_error("Could not set decoder resolution");

  if (live->pB2->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, live->pVCap, live->pDVDecoder, live->pAVIMux) != NOERROR)
    CR_error("Cannot connect capture filter to AVI mux filter");

  // [end DV?]

//   if (live->pB2->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, live->pVCap, NULL, live->pAVIMux) != NOERROR)
//     CR_error("Cannot connect capture filter to AVI mux filter");

  if (live->pFg->QueryInterface(IID_IMediaControl, (void **) &live->pMC) != NOERROR)
    CR_error("Cannot prepare to run graph");

   int i;
   for (i = 0; i < 100000000; i++);
}

//----------------------------------------------------------------------------

void DV_InitAVICapture(char *filename, CR_Live *live, int width, int height, int device_num)
{
  HRESULT hr;
  WCHAR wach_dest[MAX_AVI_PATH];

  live->device_type = DV_VIDEO;

  //  CR_flush_printf("w = %i, h = %i\n", width, height);

  live->pB2 = NULL;
  live->pFg = NULL;
  live->rend = new CRender(NULL, &hr);
  live->rend->device_num = device_num;
  live->rend->ncam_docopy = FALSE;

  //  CoInitialize(NULL);

  if (!MakeBuilder(live))
    CR_error("Cannot instantiate graph builder");

  PickConnectedDevice(live, device_num);   // 0 is default index

  //  setfilename_avicapture_live_source(filename, live);

  MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, filename, -1, wach_dest, MAX_AVI_PATH);
  GUID guid = MEDIASUBTYPE_Avi;

  if (live->pB2->SetOutputFileName(&guid, wach_dest, &live->pAVIMux, &live->pSink) != NOERROR)
    CR_error("Cannot add AVI mux, file write to capture graph");

  if (live->pB2->GetFiltergraph(&live->pFg) != NOERROR)
    CR_error("Couldn't get filter graph");

  if (live->pFg->AddFilter(live->pVCap, NULL) != NOERROR)
    CR_error("Cannot add vidcap to filtergraph");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IAMStreamConfig, (void **) &(live->pVSC)) != NOERROR)
    CR_error("Cannot find stream config");

  if (live->pVSC->GetFormat(&live->pmt) != NOERROR)
    CR_error("Cannot get stream format");

  // it seems like this approach is doing unnecessary extra work: it's decoding
  // the DV stream into raw video data and writing that to an AVI, when instead
  // we could just be writing DV-encoded data to the file directly and let
  // future AVI players or a converter programs worry about how to deal with it

  // [DV here?]

  if (CoCreateInstance(CLSID_DVVideoCodec, NULL, CLSCTX_INPROC_SERVER,
		       IID_IBaseFilter, (void**)&live->pDVDecoder) != NOERROR)
    CR_error("Can't create DV decoder filter");

  if (live->pFg->AddFilter(live->pDVDecoder, NULL) != NOERROR)
    CR_error("Cannot add DV decoder filter to filtergraph");

  if (live->pDVDecoder->QueryInterface(VIS_IID_IIPDVDec, (void**) &live->pIIPDVDec) != NOERROR)
    CR_error("Cannot get interface to DV decoder settings" );

  //  if (!DV_ConnectFilters(live))
  //    CR_error("Cannot connect DV filters");

  if (!DV_SetResolution(live, width, height))
    CR_error("Could not set decoder resolution");

  if (live->pB2->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, live->pVCap, live->pDVDecoder, live->pAVIMux) != NOERROR)
    CR_error("Cannot connect capture filter to AVI mux filter");

  // [end DV?]

//   if (live->pB2->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, live->pVCap, NULL, live->pAVIMux) != NOERROR)
//     CR_error("Cannot connect capture filter to AVI mux filter");

  if (live->pFg->QueryInterface(IID_IMediaControl, (void **) &live->pMC) != NOERROR)
    CR_error("Cannot prepare to run graph");

   int i;
   for (i = 0; i < 100000000; i++);
}

//----------------------------------------------------------------------------

// specifically for compressed DV video that comes out of DV camcorders and 
// the Dazzle Hollywood DV-Bridge converter

void DV_KillVideo(CR_Live *live)
{


}

//----------------------------------------------------------------------------

int DV_SetResolution(CR_Live *live, int w, int h)
{
  if (!((w == 720 && h == 480) || (w == 360 && h == 240) || (w == 180 && h == 120) || (w == 88 && h == 60)))
    CR_error("DV_SetResolution(): unsupported resolution");

  live->width = w;
  live->height = h;

  // try to change DV decoder resolution

  CR_flush_printf("requesting resolution change to %i x %i\n", w, h);

  if (w == 720 && h == 480) {
    if (live->pIIPDVDec->put_IPDisplay(DVDECODERRESOLUTION_720x480) != NOERROR) {
      printf("couldn't change dv resolution to %i x %i\n", w, h);
      return FALSE;
    }
  }
  else if (w == 360 && h == 240) {
    if (live->pIIPDVDec->put_IPDisplay(DVDECODERRESOLUTION_360x240) != NOERROR) {
      printf("couldn't change dv resolution to %i x %i\n", w, h);
      return FALSE;
    }
  }
  else if (w == 180 && h == 120) {
    if (live->pIIPDVDec->put_IPDisplay(DVDECODERRESOLUTION_180x120) != NOERROR) {
      printf("couldn't change dv resolution to %i x %i\n", w, h);
      return FALSE;
    }
  }
  else if (w == 88 && h == 60) {
    if (live->pIIPDVDec->put_IPDisplay(DVDECODERRESOLUTION_88x60) != NOERROR) {
      printf("couldn't change dv resolution to %i x %i\n", w, h);
      return FALSE;
    }
  }
  
  live->size = live->width * live->height * 3;
  //  live->size = live->height * ((live->width + 3) & ~3);
  live->rend->dib = (BITMAPINFO *) CR_malloc(sizeof(BITMAPINFOHEADER) + live->size);
  live->rend->dibh = (BITMAPINFOHEADER *) live->rend->dib;

  live->rend->dibh->biSize = sizeof(BITMAPINFOHEADER); 
  live->rend->dibh->biWidth = live->width;         // fill in width from parameter 
  live->rend->dibh->biHeight = live->height;       // was -live->height - fill in height from parameter 
  live->rend->dibh->biPlanes = 1;                  // must be 1 
  live->rend->dibh->biBitCount = 24;               // from parameter 
  live->rend->dibh->biCompression = BI_RGB;     
  live->rend->dibh->biSizeImage = 0;               // 0's here mean "default" 
  live->rend->dibh->biXPelsPerMeter = 2700;
  live->rend->dibh->biYPelsPerMeter = 2700;
  live->rend->dibh->biClrUsed = 0; 
  live->rend->dibh->biClrImportant = 0; 

  printf("ALLOCATING dv bits = %i (w = %i, h = %i)\n", live->size, live->width, live->height);
  live->rend->bits = (unsigned char *) ((char *) live->rend->dib + sizeof(BITMAPINFOHEADER));
  live->rend->im = make_CR_Image(live->width, live->height, 0);
  live->rend->flipim = make_CR_Image(live->width, live->height, 0);
  live->rend->yuvim = make_CR_Image(live->width, live->height, IPL_TYPE_COLOR_8U_YUV);
  
  live->calibim = make_CR_Image(live->width, live->height, 0);

  //  live->size = live->width * live->height * 3;
  //  live->rend->bits = (unsigned char *) CR_calloc(live->size, sizeof(unsigned char));

  return TRUE;
}

//----------------------------------------------------------------------------

int ConnectFilters(CR_Live *live)
{
  IPin * pcapOut = NULL;
  IPin * ptrnsIn = NULL;
  IPin * pDVIn = NULL;
  IPin * pDVOut = NULL;
  HRESULT hr;
  const CMediaType *pMediaType;
  IEnumPins ** ppEnum;
  AM_MEDIA_TYPE *pmt; 
  VIDEOINFOHEADER *pvi;

  // --------------------------------------------------
  // get render input pin
  // --------------------------------------------------

  //  CR_flush_printf("* finding render input pin\n");

  //  if (live->rendfilt->FindPin(L"Input", &ptrnsIn) != NOERROR)
  if (live->rend->m_pFilter->FindPin(L"Input", &ptrnsIn) != NOERROR)
    return FALSE;

  //  CR_flush_printf("* found render input pin\n");

  // --------------------------------------------------
  // get capture output pin
  // --------------------------------------------------

  //  CR_flush_printf("* finding capture output pin\n");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IPin, (void **) &pcapOut) != NOERROR)
    //  if (live->pB->FindInterface(&PIN_CATEGORY_CAPTURE, live->pVCap, IID_IPin, (void **) &pcapOut) != NOERROR)
    return FALSE;

  //  CR_flush_printf("* found capture output pin\n");

  // --------------------------------------------------
  // connect all of them appropriately (with NO dv)
  // --------------------------------------------------

  /*
  printf("querying out\n");
  hr = pcapOut->QueryAccept(live->pmt);
  if (hr == S_OK)
    printf("out good\n");
  else
    printf("out bad\n");

  printf("querying in\n");
  hr = ptrnsIn->QueryAccept(live->pmt);
  if (hr == S_OK)
    printf("in good\n");
  else
    printf("in bad\n");
  */

  //  pcapOut->GetMediaType(0, pMediaType);

  //  CR_exit(1);

  CR_flush_printf("* connecting input and output pin\n");

  hr = E_FAIL;     
  if (live->pVSC) {
       hr = live->pVSC->GetFormat(&pmt); 
       if (hr == NOERROR) {     
	printf("atpf = %i\n", ((VIDEOINFOHEADER*) pmt->pbFormat)->AvgTimePerFrame);

	pvi = (VIDEOINFOHEADER *) pmt->pbFormat;     
	//      pvi->AvgTimePerFrame = (LONGLONG)(10000000 / polycam->live[i]->FrameRate);
	if (camera_framerate == 30)
	  pvi->AvgTimePerFrame = (LONGLONG) 333333;
	else if (camera_framerate == 15)
	  pvi->AvgTimePerFrame = (LONGLONG) 666666;
	else
	  CR_error("unsupported framerate");
	hr = live->pVSC->SetFormat(pmt);     

	if (hr != NOERROR) 
	  CR_error("Cannot set frame rate for capture");              
	else {
	  printf("set atpf = %i\n", ((VIDEOINFOHEADER*) pmt->pbFormat)->AvgTimePerFrame);
	}

	DeleteMediaType(pmt); 
      }     
      else
	CR_error("no getformat");
  }     
  else
    CR_error("no pVSC");
    
  /*
    if (hr != NOERROR) 
      CR_error("Cannot set frame rate for capture");              
    else
      CR_error("did set frame rate");
    */

  hr = live->pFg->ConnectDirect(pcapOut, ptrnsIn, NULL);
  //  hr = live->pFg->Connect(pcapOut, ptrnsIn);

  if (hr == E_FAIL)
    CR_error("efail");
  else if (hr == E_POINTER)
    CR_error("null pointer");
  else if (hr == E_INVALIDARG)
    CR_error("invalid");
  else if (hr == E_NOTIMPL)
    CR_error("not supported");

  CR_flush_printf("* connected pin\n");

  //  printf("ALLOCATING dv bits = %i\n", live->size);
  //  live->rend->bits = (unsigned char *) CR_calloc(live->size, sizeof(unsigned char));

  // --------------------------------------------------
  // finish
  // --------------------------------------------------

  return TRUE;
}

//----------------------------------------------------------------------------

int DV_ConnectFilters(CR_Live *live)
{
  IPin * pcapOut = NULL;
  IPin * ptrnsIn = NULL;
  IPin * pDVIn = NULL;
  IPin * pDVOut = NULL;
  HRESULT hr;
  const CMediaType *pMediaType;
  IEnumPins ** ppEnum;

  // --------------------------------------------------
  // get render input pin
  // --------------------------------------------------

  //  CR_flush_printf("* finding render input pin\n");

  //  if (live->rendfilt->FindPin(L"Input", &ptrnsIn) != NOERROR)
  if (live->rend->m_pFilter->FindPin(L"Input", &ptrnsIn) != NOERROR)
    return FALSE;

  //  CR_flush_printf("* found render input pin\n");

  // --------------------------------------------------
  // get capture output pin
  // --------------------------------------------------

  //  CR_flush_printf("* finding capture output pin\n");

  if (live->pB2->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, live->pVCap, IID_IPin, (void **) &pcapOut) != NOERROR)
    //  if (live->pB->FindInterface(&PIN_CATEGORY_CAPTURE, live->pVCap, IID_IPin, (void **) &pcapOut) != NOERROR)
    return FALSE;

  //  CR_flush_printf("* found capture output pin\n");

  // --------------------------------------------------
  // get DV input pin
  // --------------------------------------------------

  //  CR_flush_printf("* finding dv input pin\n");

  //  if (live->pB2->FindPin(live->pDVDecoder, PINDIR_INPUT, NULL, NULL, TRUE, 0, &pDVIn) != NOERROR) {
  if (live->pB2->FindPin(live->pDVDecoder, PINDIR_INPUT, NULL, NULL, TRUE, 0, &pDVIn) != NOERROR) {
    printf("couldn't get input pin on DV decoder\n");
    return FALSE;
  }

  //  CR_flush_printf("* found dv input pin\n");

  // --------------------------------------------------
  // get DV output pin
  // --------------------------------------------------

  //  CR_flush_printf("* finding dv output pin\n");

  if (live->pB2->FindPin(live->pDVDecoder, PINDIR_OUTPUT, NULL , NULL, TRUE, 0, &pDVOut) != NOERROR) {
    printf("couldn't get output pin on DV decoder\n");
    return FALSE;
  }

  //  CR_flush_printf("* found dv output pin\n");

  // --------------------------------------------------
  // connect all of them appropriately (with dv)
  // --------------------------------------------------

  if (live->pFg->ConnectDirect(pcapOut, pDVIn, NULL) != NOERROR) {
    printf("couldn't connect capture output and dv input\n");
    return FALSE;
  }

  if (live->pFg->ConnectDirect(pDVOut, ptrnsIn, NULL) != NOERROR) {
    printf("couldn't connect dv output and render input\n");
    return FALSE;
  }

  //  printf("* all connected\n");

  // --------------------------------------------------
  // connect all of them appropriately (with NO dv)
  // --------------------------------------------------

  /*
  printf("querying out\n");
  hr = pcapOut->QueryAccept(live->pmt);
  if (hr == S_OK)
    printf("out good\n");
  else
    printf("out bad\n");

  printf("querying in\n");
  hr = ptrnsIn->QueryAccept(live->pmt);
  if (hr == S_OK)
    printf("in good\n");
  else
    printf("in bad\n");
  */

  //  pcapOut->GetMediaType(0, pMediaType);

  //  CR_exit(1);

  /*
  CR_flush_printf("* connecting input and output pin\n");

  //  hr = live->pFg->ConnectDirect(pcapOut, ptrnsIn, NULL);
  hr = live->pFg->Connect(pcapOut, ptrnsIn);
  if (hr == E_FAIL)
    CR_error("efail");
  else if (hr == E_POINTER)
    CR_error("null pointer");
  else if (hr == E_INVALIDARG)
    CR_error("invalid");
  else if (hr == E_NOTIMPL)
    CR_error("not supported");

  CR_flush_printf("* connected pin\n");

  printf("ALLOCATING dv bits = %i\n", live->size);
  live->rend->bits = (unsigned char *) CR_calloc(live->size, sizeof(unsigned char));

  */

  // --------------------------------------------------
  // finish
  // --------------------------------------------------

  return TRUE;
}

//----------------------------------------------------------------------------

int GetVideoFormat(CR_Live *live)
{
  char fourcc[255];

  if (live->pVSC && live->pVSC->GetFormat(&live->pmt) == S_OK) {

    if (live->pmt->formattype == FORMAT_VideoInfo) {
      live->width = ((VIDEOINFOHEADER*) live->pmt->pbFormat)->bmiHeader.biWidth;
      live->height = ((VIDEOINFOHEADER*) live->pmt->pbFormat)->bmiHeader.biHeight;
      live->bpp = ((VIDEOINFOHEADER*) live->pmt->pbFormat)->bmiHeader.biBitCount;
      live->size = ((VIDEOINFOHEADER*) live->pmt->pbFormat)->bmiHeader.biSizeImage;
      live->compression = ((VIDEOINFOHEADER*) live->pmt->pbFormat)->bmiHeader.biCompression;
    }
    else if (live->pmt->formattype == FORMAT_VideoInfo2) {
      live->width = ((VIDEOINFOHEADER2*) live->pmt->pbFormat)->rcSource.right;
      live->height = ((VIDEOINFOHEADER2*) live->pmt->pbFormat)->rcSource.bottom;
      live->bpp = ((VIDEOINFOHEADER2*) live->pmt->pbFormat)->bmiHeader.biBitCount;
      live->size = ((VIDEOINFOHEADER2*) live->pmt->pbFormat)->bmiHeader.biSizeImage;
      live->compression = ((VIDEOINFOHEADER2*) live->pmt->pbFormat)->bmiHeader.biCompression;
    }
    else
      CR_error("Unknown format type");

    sprintf(fourcc, "%c%c%c%c", 
	    ((char *) &(live->compression))[3], ((char *) &(live->compression))[2], 
	    ((char *) &(live->compression))[1], ((char *) &(live->compression))[0]);
    CR_flush_printf("%i x %i, %i bits, 4CC = '%s' (size = %i)\n", 
    		    live->width, live->height, live->bpp, fourcc, live->size);
    return TRUE;
  }
  else
    return FALSE;
}

//----------------------------------------------------------------------------

void PickConnectedDevice(CR_Live *live, int pickindex)
{
  HRESULT hr;
  IPropertyBag *pBag;
  ICreateDevEnum *pCreateDevEnum;
  ULONG cFetched;
  IMoniker *pM;
  ULONG uFetched, uIndex = 0;
  VARIANT var;

  // enumerate all video capture devices

  if (CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER,
		       IID_ICreateDevEnum, (void**)&pCreateDevEnum) != NOERROR)
    CR_error("Can't create device enumerator");

  hr = pCreateDevEnum->CreateClassEnumerator(CLSID_VideoInputDeviceCategory, &live->pEM, 0);

  pCreateDevEnum->Release();
  if (hr != NOERROR) 
    CR_error("Sorry to break this to you, but you have no video capture hardware");

  live->pEM->Reset();

  uIndex = 0;
	
  while ((hr = live->pEM->Next(1, &pM, &uFetched)), hr == S_OK) {

    if (uIndex == pickindex) {

      hr = pM->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pBag);
      
      if (SUCCEEDED(hr)) {
	    
	// this is a cheezy conversion from Visual basic to C style strings
	
	var.vt = VT_BSTR;
	hr = pBag->Read(L"FriendlyName", &var, NULL);
	if (hr == NOERROR) {
	  WideCharToMultiByte(CP_ACP, 0, var.bstrVal, -1, live->cFriendlyName, 80, NULL, NULL);
	  SysFreeString(var.bstrVal);
	}
	pBag->Release();
	printf("Selected Device %i: %s\n", uIndex, live->cFriendlyName);
      }

      hr = pM->BindToObject(0, 0, IID_IBaseFilter, (void**) &(live->pVCap));
      pM->Release();	
    }
    uIndex++;
  }

  if (!live->pVCap)
    CR_error("Cannot create video capture filter");
}

//----------------------------------------------------------------------------

// Make a graph builder object we can use for capture graph building

int MakeBuilder(CR_Live *live)
{
  // we have one already

  //  if (live->pB)
  if (live->pB2)
    return TRUE;

//   HRESULT hr = CoCreateInstance((REFCLSID) CLSID_CaptureGraphBuilder,
// 				NULL, CLSCTX_INPROC, (REFIID) IID_ICaptureGraphBuilder,
// 				(void **)&live->pB);
  HRESULT hr = CoCreateInstance((REFCLSID) CLSID_CaptureGraphBuilder2,
				NULL, CLSCTX_INPROC, (REFIID) IID_ICaptureGraphBuilder2,
				(void **)&live->pB2);
  return (hr == NOERROR) ? TRUE : FALSE;
}

//----------------------------------------------------------------------------

// Make a graph object we can use for capture graph building

int MakeGraph(CR_Live *live)
{
  // we have one already
  if (live->pFg)
    return TRUE;

  HRESULT hr = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC,
				IID_IGraphBuilder, (LPVOID *)&live->pFg);
  return (hr == NOERROR) ? TRUE : FALSE;
}

//----------------------------------------------------------------------------

void oldInitVideo(CR_Live *live)
{
  // first we create a device enumerator

  ICreateDevEnum *pCreateDevEnum;
  IEnumMoniker *pEM;
  ULONG uFetched, uIndex = 0;
  IMoniker *pM;
  HRESULT hr;
  IPropertyBag *pBag;
  VARIANT var;
  AM_MEDIA_TYPE *pmt;
  VIDEOINFOHEADER* pVideoHeader;

  CoInitialize(NULL);

  hr = CoCreateInstance((REFCLSID)CLSID_CaptureGraphBuilder,
			NULL, CLSCTX_INPROC, (REFIID)IID_ICaptureGraphBuilder,
			(void **)&(live->pB));

  hr= CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER,
		       IID_ICreateDevEnum, (void**)&pCreateDevEnum) ;

  // this creates an enumerator object for video capture devices

  hr = pCreateDevEnum->CreateClassEnumerator(CLSID_VideoInputDeviceCategory, &pEM, 0);

  // We don't need the device enumerator anymore

  pCreateDevEnum->Release();	 		

  // now we loop through and get all the video input devices that are on this system
  // ideally, the user can choose. WE should print out all the devices
  
  pEM->Reset();	
  uIndex = 0;
	
  while ((hr = pEM->Next(1, &pM, &uFetched)), hr == S_OK) {

    printf("uindex = %i\n", uIndex);

    if (uIndex == 0) {

      hr = pM->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pBag);
      
      if (SUCCEEDED(hr)) {
	    
	// this is a cheezy conversion from Visual basic to C style strings
	
	var.vt = VT_BSTR;
	hr = pBag->Read(L"FriendlyName", &var, NULL);
	if (hr == NOERROR) {
	  WideCharToMultiByte(CP_ACP, 0, var.bstrVal, -1, live->cFriendlyName, 80, NULL, NULL);
	  SysFreeString(var.bstrVal);
	}
	pBag->Release();
	printf("Selected Device %i: %s\n", uIndex,live->cFriendlyName);
      }

      hr = pM->BindToObject(0, 0, IID_IBaseFilter, (void**) &(live->pVCap));
      pM->Release();	
    }
    uIndex++;
  }

  pEM->Release();
	
  // make a filter graph
  
  hr = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC,
			IID_IGraphBuilder, (LPVOID*) &(live->pFg));
  if (FAILED(hr)) 
    CR_error("Couldn't make filter graph");
  //  else
  //    CR_flush_printf("made filter graph\n");

  // now add it to the graph
    
  hr = live->pB->SetFiltergraph(live->pFg);
  hr = live->pFg->AddFilter(live->pVCap, NULL);

  hr = live->pB->FindInterface(&PIN_CATEGORY_CAPTURE, live->pVCap,
				     IID_IAMVideoCompression, (void **)&(live->pVC));

  if (hr != NOERROR)
    CR_error("Vidcompression finding error");

  // !!! What if this interface isn't supported?
  // we use this interface to set the frame rate and get the capture size
  
  hr = live->pB->FindInterface(&PIN_CATEGORY_CAPTURE, live->pVCap,
				     IID_IAMStreamConfig, (void **)&(live->pVSC));

  if (live->pVSC && live->pVSC->GetFormat(&pmt) == S_OK) {

    if (pmt->formattype == FORMAT_VideoInfo) {

      printf("videoinfo\n");

      pVideoHeader = (VIDEOINFOHEADER*)pmt->pbFormat;
      
      // resize our window to the default capture size
      
      live->width = ((VIDEOINFOHEADER*)pmt->pbFormat)->bmiHeader.biWidth;
      live->height = ((VIDEOINFOHEADER*)pmt->pbFormat)->bmiHeader.biHeight;
    }

    if (pmt->formattype == FORMAT_VideoInfo2) {

      printf("videoinfo2\n");
      
      // resize our window to the default capture size
      
      live->width = ((VIDEOINFOHEADER2*)pmt->pbFormat)->rcSource.right;
      live->height = ((VIDEOINFOHEADER2*)pmt->pbFormat)->rcSource.bottom;
    }
  }

  CR_flush_printf("w = %i, h = %i\n", live->width, live->height);

  /*
    live->vidTex=CreateTexture(256,256);

//	live->vidTex=CreateTexture(INITIAL_TEXTURE_WIDTH, INITIAL_TEXTURE_HEIGHT);
//	live->vidBuff = (char *) malloc(256*256*4*sizeof(char));
    live->t0[0]=0;
    live->t0[1]=0;
    live->t1[0]=1.0;
    live->t1[1]=0;
    live->t2[0]=1.0;
    live->t2[1]=1;
    live->t3[0]=0;
    live->t3[1]=1;
  */

//  live->t0[0]=0.0;

//     live->t0[1]= NTSC_DIGITIZER_HEIGHT/INITIAL_TEXTURE_HEIGHT;
// 
//     live->t1[0]= NTSC_DIGITIZER_WIDTH/INITIAL_TEXTURE_WIDTH;
//     live->t1[1]= NTSC_DIGITIZER_HEIGHT/INITIAL_TEXTURE_HEIGHT;
// 
// 
//     live->t2[0]= NTSC_DIGITIZER_WIDTH/INITIAL_TEXTURE_WIDTH;
//     live->t2[1]=0.0;
//     
// 	live->t3[0]=0.0;
//     live->t3[1]=0.0;


  // 6/26/2001 start
  //i think this is where the window is secretly opened and 
  // automatic rendering is surreptitiously invoked

//	NukeDownstream(live->pVCap,live);
	
  hr = live->pB->RenderStream(&PIN_CATEGORY_CAPTURE, live->pVCap, NULL, NULL);
  if (FAILED(hr)) 
    CR_error("Couldn't init renderstream");
  else
    CR_flush_printf("Initialized renderstream\n");

  // ((HRESULT)0x0004027EL)

  bPreviewFaked = TRUE;

  if (hr == ((HRESULT)0x0004027EL)) {

    // preview was faked up for us using the (only) capture pin
    
    bPreviewFaked = TRUE;
  }
  else if (hr != S_OK) {
    //	ErrMsg("This graph cannot preview!");
  }

  IMediaControl *pMC = NULL;

  hr = live->pFg->QueryInterface(IID_IMediaControl, (void **)&pMC);
  if (FAILED(hr)) 
    CR_error("couldn't make media control");
  //  else
  //    CR_flush_printf("made media control\n");

  
  if (SUCCEEDED(hr)) {
    
    hr = pMC->Run();	
    if (FAILED(hr)) {
      // stop parts that ran
      pMC->Stop();
    }
    pMC->Release();
  }

  if (bPreviewFaked) {

    hr = live->pB->FindInterface(&PIN_CATEGORY_CAPTURE,
				       live->pVCap, IID_IBasicVideo, (void **)&(live->pBV));
  if (FAILED(hr)) 
    CR_error("couldn't make capture");
  //  else
  //    CR_flush_printf("made capture\n");

  } 
  else {
    hr = live->pB->FindInterface(&PIN_CATEGORY_PREVIEW,
				       live->pVCap, IID_IBasicVideo, (void **)&(live->pBV));

  if (FAILED(hr)) 
    CR_error("couldn't make preview");
  //  else
  //    CR_flush_printf("made preview\n");
  }

  /*
	live->width = 256;
    live->height = 256;
//	live->outW=256;
//	live->outH=256;
	
	//    live->width=live->width=NTSC_DIGITIZER_WIDTH;
  //  live->height=live->height=NTSC_DIGITIZER_HEIGHT;
	*/

  // create sample grabber
  /*
  hr = CoCreateInstance(CLSID_SampleGrabber, NULL, CLSCTX_INPROC_SERVER,
			IID_IBaseFilter, (void **)&live->pF);
  if (FAILED(hr))
    CR_error("Failed to create sample grabber!  hr=0x%x");
  
  hr = live->pF->QueryInterface(IID_ISampleGrabber, (void **)&live->pGrab);
  if (FAILED(hr))
    CR_error("Can't get sample grabber interface!  hr=0x%x");
  */

  /*
    hr = live->pB->FindInterface(&PIN_CATEGORY_CAPTURE,
				       live->pVCap, IID_ISampleGrabber, (void **)&(live->pGrab));
  if (FAILED(hr)) 
    CR_error("Couldn't make sample grabber");
  */

  // allocate IplImage for current frame

  live->iplim = make_IplImage(live->width, live->height, 0);
}

//----------------------------------------------------------------------------

void newInitVideo(CR_Live *live)
{
  // first we create a device enumerator
  
  ICreateDevEnum *pCreateDevEnum;
  IEnumMoniker *pEM;
  ULONG uFetched, uIndex = 0;
  IMoniker *pM;
  HRESULT hr;
  IPropertyBag *pBag;
  VARIANT var;
  AM_MEDIA_TYPE *pmt;
  VIDEOINFOHEADER* pVideoHeader;

  CoInitialize(NULL);

  hr = CoCreateInstance((REFCLSID)CLSID_CaptureGraphBuilder,
			NULL, CLSCTX_INPROC, (REFIID)IID_ICaptureGraphBuilder,
			(void **)&(live->pB));

  hr= CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER,
		       IID_ICreateDevEnum, (void**)&pCreateDevEnum) ;

  // this creates an enumerator object for video capture devices

  hr = pCreateDevEnum->CreateClassEnumerator(CLSID_VideoInputDeviceCategory, &pEM, 0);

  // We don't need the device enumerator anymore

  pCreateDevEnum->Release();	 		

  // now we loop through and get all the video input devices that are on this system
  // ideally, the user can choose. WE should print out all the devices

  CR_flush_printf("here\n");
  
  pEM->Reset();	

  CR_flush_printf("there\n");

  uIndex = 0;
	
  while ((hr = pEM->Next(1, &pM, &uFetched)), hr == S_OK) {

    printf("uindex = %i\n", uIndex);

    if (uIndex == 0) {

      hr = pM->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pBag);
      
      if (SUCCEEDED(hr)) {
	    
	// this is a cheezy conversion from Visual basic to C style strings
	
	var.vt = VT_BSTR;
	hr = pBag->Read(L"FriendlyName", &var, NULL);
	if (hr == NOERROR) {
	  WideCharToMultiByte(CP_ACP, 0, var.bstrVal, -1, live->cFriendlyName, 80, NULL, NULL);
	  SysFreeString(var.bstrVal);
	}
	pBag->Release();
	printf("Selected Device %i: %s\n", uIndex,live->cFriendlyName);
      }

      hr = pM->BindToObject(0, 0, IID_IBaseFilter, (void**) &(live->pVCap));
      pM->Release();	
    }
    uIndex++;
  }

  pEM->Release();
	
  // make a filter graph
  
  hr = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC,
			IID_IGraphBuilder, (LPVOID*) &(live->pFg));
  if (FAILED(hr)) 
    CR_error("Couldn't make filter graph");
  //  else
  //    CR_flush_printf("made filter graph\n");

  // now add it to the graph
    
  hr = live->pB->SetFiltergraph(live->pFg);
  hr = live->pFg->AddFilter(live->pVCap, NULL);

  hr = live->pB->FindInterface(&PIN_CATEGORY_CAPTURE, live->pVCap,
				     IID_IAMVideoCompression, (void **)&(live->pVC));

  // !!! What if this interface isn't supported?
  // we use this interface to set the frame rate and get the capture size
  
  hr = live->pB->FindInterface(&PIN_CATEGORY_CAPTURE, live->pVCap,
				     IID_IAMStreamConfig, (void **)&(live->pVSC));

  if (live->pVSC && live->pVSC->GetFormat(&pmt) == S_OK) {

    if (pmt->formattype == FORMAT_VideoInfo) {

      printf("videoinfo\n");

      pVideoHeader = (VIDEOINFOHEADER*)pmt->pbFormat;
      
      // resize our window to the default capture size
      
      live->width = ((VIDEOINFOHEADER*)pmt->pbFormat)->bmiHeader.biWidth;
      live->height = ((VIDEOINFOHEADER*)pmt->pbFormat)->bmiHeader.biHeight;
    }

    if (pmt->formattype == FORMAT_VideoInfo2) {

      printf("videoinfo2\n");
      
      // resize our window to the default capture size
      
      live->width = ((VIDEOINFOHEADER2*)pmt->pbFormat)->rcSource.right;
      live->height = ((VIDEOINFOHEADER2*)pmt->pbFormat)->rcSource.bottom;
    }
  }

  CR_flush_printf("w = %i, h = %i\n", live->width, live->height);

  // 6/26/2001 start
  //i think this is where the window is secretly opened and 
  // automatic rendering is surreptitiously invoked

//	NukeDownstream(live->pVCap,live);
	
  /*
  hr = live->pB->RenderStream(&PIN_CATEGORY_CAPTURE, live->pVCap, NULL, NULL);
  if (FAILED(hr)) 
    CR_error("Couldn't init renderstream");
  else
    CR_flush_printf("Initialized renderstream\n");
  */

  // ((HRESULT)0x0004027EL)

  bPreviewFaked = TRUE;

  /*
  if (hr == ((HRESULT)0x0004027EL)) {

    // preview was faked up for us using the (only) capture pin
    
    bPreviewFaked = TRUE;
  }
  else if (hr != S_OK) {
    //	ErrMsg("This graph cannot preview!");
  }
  */

  IMediaControl *pMC = NULL;

  hr = live->pFg->QueryInterface(IID_IMediaControl, (void **)&pMC);
  if (FAILED(hr)) 
    CR_error("Couldn't make media control");
  //  else
  //    CR_flush_printf("Made media control\n");

  
  if (SUCCEEDED(hr)) {
    
    hr = pMC->Run();	
    if (FAILED(hr)) {
      // stop parts that ran
      pMC->Stop();
    }
    pMC->Release();
  }

  GrabVideo(live);
  exit(1);

  // start new

 
  //  REFERENCE_TIME start = MAX_TIME, stop = MAX_TIME;
//   REFERENCE_TIME start = MAX_TIME;
//   REFERENCE_TIME stop = MAX_TIME;
// 
//   CR_flush_printf("1\n");
//   hr = live->pB->ControlStream(&PIN_CATEGORY_CAPTURE, NULL, NULL, &stop, 0, 0);	
//   CR_flush_printf("2\n");
//   
//   hr = live->pB->ControlStream(&PIN_CATEGORY_CAPTURE, NULL, &start, NULL, 0, 0);	
//   CR_flush_printf("3\n");
  
  // end new

  if (bPreviewFaked) {

    hr = live->pB->FindInterface(&PIN_CATEGORY_CAPTURE,
				       live->pVCap, IID_IBasicVideo, (void **)&(live->pBV));
  if (FAILED(hr)) 
    CR_error("Couldn't make capture");
  //  else
  //    CR_flush_printf("Made capture\n");

  } 
  else {
    hr = live->pB->FindInterface(&PIN_CATEGORY_PREVIEW,
				       live->pVCap, IID_IBasicVideo, (void **)&(live->pBV));

  if (FAILED(hr)) 
    CR_error("Couldn't make preview");
  //  else
  //    CR_flush_printf("Made preview\n");
  }
  /*  */

  // 6/26/2001 stop


  /*
	live->width = 256;
    live->height = 256;
//	live->outW=256;
//	live->outH=256;
	
	//    live->width=live->width=NTSC_DIGITIZER_WIDTH;
  //  live->height=live->height=NTSC_DIGITIZER_HEIGHT;
	*/

  // create sample grabber
  /*
  hr = CoCreateInstance(CLSID_SampleGrabber, NULL, CLSCTX_INPROC_SERVER,
			IID_IBaseFilter, (void **)&live->pF);
  if (FAILED(hr))
    CR_error("Failed to create sample grabber!  hr=0x%x");
  
  hr = live->pF->QueryInterface(IID_ISampleGrabber, (void **)&live->pGrab);
  if (FAILED(hr))
    CR_error("Can't get sample grabber interface!  hr=0x%x");
  */

  /*
    hr = live->pB->FindInterface(&PIN_CATEGORY_CAPTURE,
				       live->pVCap, IID_ISampleGrabber, (void **)&(live->pGrab));
  if (FAILED(hr)) 
    CR_error("Couldn't make sample grabber");
  */

  // allocate IplImage for current frame

  live->iplim = make_IplImage(live->width, live->height, 0);


//     hr = pB->SetFiltergraph(pFg);
//     if (hr != NOERROR) {
//         OutputDebugString("Cannot give graph to builder");
//         goto Error;
//     }
    }

//----------------------------------------------------------------------------

void GrabVideo(CR_Live *live)
{
  HRESULT hr;
  BITMAPINFOHEADER    bi;             // bitmap header 
  LPBITMAPINFOHEADER  lpbi;           // pointer to BITMAPINFOHEADER 
  long              dwLen;          // size of memory block 
  void*               hDIB; 
  DWORD               dwBytesPerLine; // Number of bytes per scanline 
  long lWidth,lHeight;
  char *pcBase;
  IBaseFilter   *pBase;
  IMediaControl *pMC = NULL;

  if (!SUCCEEDED(hr = live->pFg->QueryInterface(IID_IBasicVideo,(void**) &live->pBV)))
    CR_error("Could not get IBasicVideo interface");
  //  else if (!SUCCEEDED(hr = live->pBV->GetCurrentImage(&lBufferBytes, pImageBuffer)))
  //    CR_error("GetCurrentImage failed finding size of image");

  hr = live->pFg->QueryInterface(IID_IMediaControl, (void **)&pMC);
  if (FAILED(hr)) 
    CR_error("Couldn't make media control");
  else
    CR_flush_printf("Made media control\n");

  /*
  hr = live->pBV->QueryInterface(IID_IBaseFilter,(void**)&pBase);
  if (FAILED(hr)) 
    CR_error("Couldn't make base filter");
  else
    CR_flush_printf("Made base filter\n");
  */

  live->pBV->get_VideoHeight(&lHeight);
  live->pBV->get_VideoWidth(&lWidth);
   
  // initialize BITMAPINFOHEADER 
  
  bi.biSize = sizeof(BITMAPINFOHEADER); 
  bi.biWidth = lWidth;         // fill in width from parameter 
  bi.biHeight = lHeight;       // fill in height from parameter 
  bi.biPlanes = 1;              // must be 1 
  bi.biBitCount = 24;    // from parameter 
  bi.biCompression = BI_RGB;     
  bi.biSizeImage = 0;           // 0's here mean "default" 
  bi.biXPelsPerMeter = 2700;
  bi.biYPelsPerMeter = 2700;
  bi.biClrUsed = 0; 
  bi.biClrImportant = 0; 
  
  // calculate size of memory block required to store the DIB.  This 
  // block should be big enough to hold the BITMAPINFOHEADER, the color 
  // table, and the bits 
 
  dwBytesPerLine = 3 * live->width; 
  dwLen = bi.biSize + (dwBytesPerLine * live->height); 
  
  // alloc memory block to store our bitmap 
 
  //hDIB = GlobalAlloc(GHND, dwLen); 
   
  // lock memory and get pointer to it 

  // xxx
  pcBase = new char[dwLen];
  lpbi = (BITMAPINFOHEADER*) pcBase;////(LPBITMAPINFOHEADER)GlobalLock(hDIB); 
 
  // use our bitmap info structure to fill in first part of 
  // our DIB with the BITMAPINFOHEADER 
 
  // xxx
  *lpbi = bi; 

  hr = pMC->Pause();	

  hr = live->pBV->GetCurrentImage(&dwLen, (long*) lpbi);
  printf("hr = %i\n", hr);
  if (SUCCEEDED(hr))
    printf("success!\n");
  else if (FAILED(hr))
    printf("failure!\n");
  else if (hr == S_OK)
    printf("ok!\n");
  else
    printf("don't know what it is!\n");

}

//----------------------------------------------------------------------------

char FakeOutBuffer[300000];

void UpdateVideo(CR_Live *live)
{
  HRESULT hr;
  FILTER_STATE state;  
  OAFilterState fState;
  IBaseFilter   *pBase;
  long lWidth,lHeight;
  char *pcBase;
  IMediaControl *pMC = NULL;
  int i;
  //  ISampleGrabber *pSample;

  CR_flush_printf("updating\n");

  //	hr = live->pVCap->GetState(100,&state);
  
  // run the graph
  
  // 	hr = live->pFg->QueryInterface(IID_IMediaControl, (void **)&pMC);
  //     if (SUCCEEDED(hr))
  // 	{
  // 		hr = pMC->Pause();	
  // 		if (FAILED(hr)) {
  // 		    // stop parts that ran
  // 		 pMC->Stop();
  // 		}
  // 		hr = pMC->GetState(100,&fState);
  // 		pMC->Release();
  //     }

  //
  // Get the preview window to be a child of our app's window
  //

  // this will go through a possible decoder, find the video renderer it's
  // connected to, and get the IVideoWindow interface on it
  // If preview is being done through a smart tee filter, the IVideoWindow
  // interface will be found off the capture pin
  
  //	hr = live->pVCap->Pause();

  hr = live->pBV->QueryInterface(IID_IBaseFilter,(void**)&pBase);
  if (FAILED(hr)) 
    CR_error("Couldn't make base filter");
  else
    CR_flush_printf("Made base filter\n");

  //  exit(1);

//   hr = pBase->QueryInterface(IID_ISampleGrabber,(void**)&pSample);
//   if (FAILED(hr)) 
//     CR_error("Couldn't make sample grabber");
//   else
//     CR_flush_printf("Made sample grabber\n");

  // xxx
  //  live->pBV->get_SourceHeight(&lHeight);
  //  live->pBV->get_SourceWidth(&lWidth);

  //  printf("source w %i h %i\n", lWidth, lHeight);

  /*
  hr = live->pBV->put_SourceHeight(256);
  hr = live->pBV->put_SourceWidth(256);

  lHeight = 256;
  lWidth  = 256;
  */

  live->pBV->get_VideoHeight(&lHeight);
  live->pBV->get_VideoWidth(&lWidth);

  if (hr != NOERROR) {
    //		ErrMsg("This graph cannot preview properly");
  } 

  BITMAPINFOHEADER    bi;             // bitmap header 
  LPBITMAPINFOHEADER  lpbi;           // pointer to BITMAPINFOHEADER 
  long              dwLen;          // size of memory block 
  void*               hDIB; 
  DWORD               dwBytesPerLine; // Number of bytes per scanline 
   
  // initialize BITMAPINFOHEADER 
  
  bi.biSize = sizeof(BITMAPINFOHEADER); 
  bi.biWidth = lWidth;         // fill in width from parameter 
  bi.biHeight = lHeight;       // fill in height from parameter 
  bi.biPlanes = 1;              // must be 1 
  bi.biBitCount = 24;    // from parameter 
  bi.biCompression = BI_RGB;     
  bi.biSizeImage = 0;           // 0's here mean "default" 
  bi.biXPelsPerMeter = 2700;
  bi.biYPelsPerMeter = 2700;
  bi.biClrUsed = 0; 
  bi.biClrImportant = 0; 
  
  // calculate size of memory block required to store the DIB.  This 
  // block should be big enough to hold the BITMAPINFOHEADER, the color 
  // table, and the bits 
 
  dwBytesPerLine = 3 * live->width; 
  dwLen = bi.biSize + (dwBytesPerLine * live->height); 
  
  // alloc memory block to store our bitmap 
 
  //hDIB = GlobalAlloc(GHND, dwLen); 
   
  // lock memory and get pointer to it 

  // xxx
  pcBase = new char[dwLen];
  lpbi = (BITMAPINFOHEADER*) pcBase;////(LPBITMAPINFOHEADER)GlobalLock(hDIB); 
 
  // use our bitmap info structure to fill in first part of 
  // our DIB with the BITMAPINFOHEADER 
 
  // xxx
  *lpbi = bi; 
 
  // Since we don't know what the colortable and bits should contain, 
  // just leave these blank.  Unlock the DIB and return the HDIB. 
 
  //  GlobalUnlock(hDIB); 

  //  CR_flush_printf("a %i b %i\n", live->width * 3, live->iplim->widthStep);
  //  exit(1);

  if (hr == S_OK) {
    
    //    CR_flush_printf("ok!\n");

    //	live->pVCap->Run(0);
    //	hr = live->pVCap->GetState(100,&state);
    //	hr = live->pVCap->Pause();
    
    //	hr = pBase->GetState(100,&state);
    //	hr = pBase->Pause();

    // xxx	
    hr = pBase->Pause();
    //    hr = pBase->GetState(75,&state);
    hr = pBase->GetState(1000,&state);

    //	hr = pBase->Pause();
    //		while((hr = pBase->Pause()) == S_FALSE);
    //	dwLen=0;

    // xxx
    hr = live->pBV->GetCurrentImage(&dwLen, (long*) lpbi);
    printf("hr = %i\n", hr);
    if (SUCCEEDED(hr))
      printf("success!\n");
    else if (FAILED(hr))
      printf("failure!\n");
    else if (hr == S_OK)
      printf("ok!\n");
    else
      printf("don't know what it is!\n");
    //    exit(1);

    /*
    if (FAILED(hr)) 
      CR_error("Couldn't get image");
    else
    CR_flush_printf("Got image\n");
    */

    if (hr == S_OK) {
      
      //      glEnable(GL_TEXTURE_2D);
      //      glBindTexture(GL_TEXTURE_2D, live->vidTex);
      
      // put image into FakeOutBuffer

      // xxx
      //      for (i = 0; i < live->height; i++) {
      ////	memcpy(&FakeOutBuffer[i * live->width * 3], pcBase + bi.biSize + 256 * 3 * i, live->width * 3);
      //memcpy(&live->iplim->imageData[i * live->iplim->widthStep], pcBase + bi.biSize + 256 * 3 * i, live->width * 3);
      //      }

      // copy image to where you want it to go next

      //      glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, live->width, live->height,
      //		      GL_BGR_EXT,GL_UNSIGNED_BYTE, &FakeOutBuffer[0]);
    }
    pBase->Run(0);
  }

  pBase->Release();
  //	live->pBV->Release();

  // stop the graph

// 	hr = live->pFg->QueryInterface(IID_IMediaControl, (void **)&pMC);
// 	if (SUCCEEDED(hr)) 
// 	{
// 	hr = pMC->Stop();
// 	pMC->Release();
// 	}


  // xxx
  //  delete []pcBase;
  
//   hr = live->pFg->QueryInterface(IID_IMediaControl, (void **)&pMC);
//   if (SUCCEEDED(hr))
//     {
//       hr = pMC->Run();	
//       if (FAILED(hr)) {
// 	// stop parts that ran
// 	pMC->Stop();
//       }
//       pMC->Release();
// 	}
}

//----------------------------------------------------------------------------

void CleanupVideo(CR_Live *live)
{
  IMediaControl *pMC = NULL;
  HRESULT hr;

  hr = live->pFg->QueryInterface(IID_IMediaControl, (void **) &pMC);
  
  if (SUCCEEDED(hr)) {
    hr = pMC->Stop();
    pMC->Release();
  }

  live->pVCap->Release();
  
  live->pVC->Release();
  live->pVSC->Release();
  live->pB->Release();
  live->pFg->Release();
  live->pBV->Release();
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
        
