#include "LadarFinder.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"
#include "MTA/Misc/Time/Time.hh"

#include <unistd.h>
#include <iostream>

int QUIT_PRESSED = 0;

extern unsigned long SLEEP_TIME;
extern int SPARROW_DISP;
extern int NOSTATE;

LadarFinder::LadarFinder() : DGC_MODULE(MODULES::LadarFinder, "LADAR Finder", 1)
{
}

LadarFinder::~LadarFinder() 
{
}


void LadarFinder::Init() 
{
  // call the parent init first
  DGC_MODULE::Init();

  //printf("[%s][%d]\n", __FILE__, __LINE__);
  
  InitLadar();

  //printf("[%s][%d]\n", __FILE__, __LINE__);
  
  // Load in the RDDF waypoints.  
  rddf.loadFile("rddf.dat");

  //printf("[%s][%d]\n", __FILE__, __LINE__);
  
  // This RDDFVector is a standard template
  // vector of RDDFData types, defined in RDDF/rddf.hh.  The RDDFData struct 
  // has members: number, Northing, Easting, maxSpeed, and radius/offset.
  waypoints = rddf.getTargetPoints();

  for( int i = 0; i < rddf.getNumTargetPoints(); i++ )
  {
    waypoints[i].display();
  }
  
  if(SPARROW_DISP) 
  {
    cout << "Starting Sparrow display" << endl;
    RunMethodInNewThread<LadarFinder>( this, 
		    &LadarFinder::SparrowDisplayLoop);
    RunMethodInNewThread<LadarFinder>( this, 
		    &LadarFinder::UpdateSparrowVariablesLoop);
  }
  cout << "Module Init Finished" << endl;
}

void LadarFinder::Active() 
{
  cout << "Starting Active Function" << endl;
 
/*
  // update the State Struct
  if(!NOSTATE) 
  {
    UpdateState();
    if(d.SS.Northing == 0) 
    {
      cout << endl << "UNABLE TO COMMUNICATE WITH VSTATE" << endl;
      cout << "NORTHING IS ZERO, PLEASE CHECK OR" << endl;
      cout << "RUN WITH --nostate SWITCH IF THIS IS OK" << endl << endl;
      exit(-1);
    }
  }
*/

  /************************************/
  /**************test*code*************/
  /************************************/
  
  // Fake this for now.
  d.SS.Northing = 3834760.000; 
  d.SS.Easting = 442528.000;

  CTraj trj(3); // Trajectory class implementation
  trj.startDataInput(); // Must be called before data input
  
  ppSpline spline;
  double pathpoint[6];
          
  RDDFVector waypoints;
  waypoints = rddf.getTargetPoints();
  int wp = rddf.getCurrentWaypointNumber(d.SS.Northing, d.SS.Easting);

  for(double dist=0.0; dist<=CONTROL_POINT_MAXDIST; dist+=CONTROL_POINT_SPACING)
  {
    XYcoord coord;
    coord = rddf.getPointAlongTrackLine(waypoints[wp].Northing, 
                                        waypoints[wp].Easting, 
                                        dist);
    printf("Adding control point [%f, %f, %f] to spline.\n", 
                    dist/NOMINAL_SPEED, coord.X, coord.Y);
    spline.inputDataPoint(dist/NOMINAL_SPEED, coord.X, coord.Y);
  } 
  cout << "Calling csapi." << endl;
  spline.calcCoefs(); // This sets the coefficients.

  ofstream outfile("coefs.dat");
  cout << "Spline coefficients saved in coefs.dat." << endl;

  spline.dispCoefs(outfile, -1); // Save 'em.
  cout << "Displaying the spline break values..." << endl;
  spline.dispBreaks(cout, -1); // Show 'em.
  cout << "Displaying the spline coefficients..." << endl;
  spline.dispCoefs(cout, -1); // Show 'em.

  double* loc;

  // Now fill in a traj with the spline data and send it over MTA (either to 
  // path following or to mode management
  // Let's test it out to make sure
  for( double incr = 0.0; incr <= CONTROL_POINT_MAXDIST/NOMINAL_SPEED; incr += 1.0 )
  {
    loc = spline.getPoint(incr);
    printf("Point-on-spline at %f is [%f %f %f %f %f %f]\n", incr, loc[0], loc[1], loc[2], loc[3], loc[4], loc[5]);

    // Fill in the trajectory (path plus derivatives)
    trj.inputWithDiffs(loc, &loc[3]);
    //trj.inputWithDiffs(loc, loc+3*sizeof(double));
  }
  cout << "Number of points in traj is " << trj.getNumPoints() << endl;
  cout << "Displaying the filled in traj..." << endl;
  trj.print(cout, -1);
  cout << "Saving the filled in traj..." << endl;
  ofstream pathfile("test.path");
  trj.print(pathfile, -1);
  cout << "... done." << endl;
 
  exit(-1);

// if(bSendTraj)
// {
//   // Create a Mail message for PathFollower
//   Mail msg = NewOutMessage( MyAddress(),
//                   MODULES::PathFollower,
//                   PathFollowerMessages::NewPath);
//   // Fill and send this message to the PathFollower
//   CTraj *pTraj = planner.getTraj();
//   msg.QueueTo(pTraj, CTraj::getHeaderSize());
// 	msg.QueueTo(pTraj->getNdiffarray(0), pTraj->getNumPoints() * sizeof(double));
// 	msg.QueueTo(pTraj->getNdiffarray(1), pTraj->getNumPoints() * sizeof(double));
// 	msg.QueueTo(pTraj->getNdiffarray(2), pTraj->getNumPoints() * sizeof(double));
// 	msg.QueueTo(pTraj->getEdiffarray(0), pTraj->getNumPoints() * sizeof(double));
// 	msg.QueueTo(pTraj->getEdiffarray(1), pTraj->getNumPoints() * sizeof(double));
// 	msg.QueueTo(pTraj->getEdiffarray(2), pTraj->getNumPoints() * sizeof(double));
//   SendMail(msg);
// }



  /************************************/
  /***********end*test*code************/
  /************************************/
  
  
  while( ContinueInState() && !QUIT_PRESSED) 
  {
    // update the State Struct
    if(!NOSTATE) 
    {
      UpdateState();
      if(d.SS.Northing == 0) 
      {
        cout << endl << "UNABLE TO COMMUNICATE WITH VSTATE" << endl;
        cout << "NORTHING IS ZERO, PLEASE CHECK OR" << endl;
        cout << "RUN WITH --nostate SWITCH IF THIS IS OK" << endl << endl;
        exit(-1);
      }
    }
    // Get a list of measurements
    LadarGetMeasurements();

    if(SPARROW_DISP) UpdateSparrowVariablesLoop();

    // and sleep for a bit (subtract sleep overhead)
    usleep_for(max((int)(SLEEP_TIME - 10000), 0));
  }
  cout << "Finished Active State" << endl;
}

void LadarFinder::Shutdown() 
{
  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;
  LadarShutdown();
}

void LadarFinder::UpdateState()
{ 
  Mail msg = NewQueryMessage( MyAddress(), 
		                          MODULES::VState, 
                              VStateMessages::GetState);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) 
  {
    reply >> d.SS; // download the new state
  }

} // end UpdateState() 

// end LadarFinder.cc
