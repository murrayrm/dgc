function dphi = steering(phi, steercmd)
% This function sets up the dynamics of the steering.  Right now it's
% modeled as a first order lag

global A_PHI

dphi =  -A_PHI * phi + A_PHI * steercmd;