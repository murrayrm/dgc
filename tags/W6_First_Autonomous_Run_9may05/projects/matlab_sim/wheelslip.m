function alpha = wheelslip(state) 
% Calculates the front and rear wheel slip angles based on the current
% vehicle state

global L_f L_r

alpha = [0; 0];

[thd, vlat, vlon, x, y, theta, phi] = set_states(state);

% calculate slip angles for front and rear
alpha(1) = phi - atan2(L_f * thd + vlat, vlon);
alpha(2) = atan2(L_r * thd - vlat, vlon);