/*
 * throttle.c - code to control the accelerometer position
 * note: originally named accel.c 
 * 
 * Original author: Jeremy G.
 *                  Sue Ann Hong
 * Revised: Richard Murray
 * 2 January 2003
 * Revised: Will Heltsley
 * 30 Jan 2003
 *
 */

#include "vehports.h"
#include "brake2.h"
#include "throttle.h"
#include "sparrow/dbglib.h"
#include "MTA/Misc/Time/Time.hh"
#include <iostream>
#include <time.h>

using namespace std;


//NOTE: throughout this file, variables ending in counts refer to the digipot's native
//counts, 0-127; variables without this ending refer to throttle position from 0-1
//The system design dictates that the counts value of 127 is equivalent to throttle
//position of 0.0, corresponding to a fully closed, or idle, throttle.  The counts value
//of 0 is equivalent to a throttle position of 1, corresponding to fully open, or full 
//throttle

//Create the global variable throttle_pos_counts which indicates the setting of the digipot
//Set throttle_pos_counts to -150, in case throttle_write gets called before throttle_calibrate,
//the throttle will go to fully open (idle) and reset its position to 0 and return error.  Note
//that the possible positions of the pot range from 0 to 127.  Also creats two markers for 
//port initialization and calibration to be used in the throttle_ok function
int throttle_pos_counts = -150;
int throttle_port_opened = 0;
int throttle_calibrated = 0;

/* Intialize the throttle actuator, and open the parallel port for it, the brake pot,
the ignition, and the transmission.  Must call this function to run the brake */
int throttle_open(int port) {
  if(pp_init(port, PP_DIR_IN) >= 0) {
    dbg_info("Throttle actuator init-ed on port %d\n", PP_GASBR);
    pp_set_bits(PP_GASBR, THROTTLE_CS, 0); //disable the inputs on the potentiometer - 
    //this cuts down on the amount of noise.  We will assume that the input is always off
    //until we need it.  requred to turn off after use.
    throttle_port_opened = 1;
    return 0;
  } 
  else {
    dbg_info("\nError opening parallel port %d\n", PP_GASBR);
    return -1;
  }
}



/* Zero and calibrate the throttle actuator.  Note that this code must be run to calibrate
the throttle actuator for first movement after vdrive is started. */
int throttle_calibrate() {
  throttle_increment(-127); 
  throttle_increment(127);
  usleep_for(500000);

  if( (throttle_increment(-127) == 0) && (throttle_increment(127) == 0)) {   
    //push all the way out, and then zero, so that we "catch" the throttle actuator. 
    //the actuator will not respond to a move unless the wiper of the pot has passed
    //the position of the actuator.
    throttle_pos_counts = 127;
    throttle_calibrated = 1;
    return 0;
  }
  else return -1;
}


/* Zeros the actuator to fully closed (idle) throttle.  Due to the nature of the actuator and 
   controller, this function just calls throttle_calibrate.
*/
int throttle_zero() {
  //Set the actuator to increment out to the fully out position.  This will now insure
  //that the throttle is in the 0 position.  
  if(throttle_increment(127) == 0) {  
    throttle_pos_counts = 127;
    return 0;
  }
  else return -1;
}

//Since there isn't any way to check the health of any of the throttle systems, ok just checks
//the throttle_port_opened and throttle_calibrated markers to see if these things have been done.
int throttle_ok() {
  if(throttle_calibrated == 1 && throttle_port_opened == 1) return 0;
  else return -1;
}

//TBD: This will eventually return a struct full of the requested data and zeros elsewhere.  
//Doesnt do anything now
int throttle_status(int request_type, struct throttle_status_type *throttle_status_type){
  cout<<"throttle_status called, but code is not written--nothing to do.";
  return 0;
}

//Send the throttle to idle
int throttle_pause(){
  return throttle_write(0);
}

//Sends the throttle to idle via the pause function.
int throttle_disable(){
  return throttle_pause();
}

//Should be ready to receive new commands vie throttle_write
int throttle_resume(){
  return 0;
}

//Closes the parallel port.  Note that this will also close the port used by the brake
//pot, transmission, and ignition.
int throttle_close() {
  if(pp_end(PP_GASBR) >= 0) {
    dbg_info("Throttle actuator port %d\n closed", PP_GASBR);
    return 0;
  } 
  else {
    dbg_info("\nError opening parallel port %d\n", PP_GASBR);
    return -1;
  }
}

//Returns the gas actuator's position on the 0-1 scale.  Note that this will return garbage
//that is out of range if called before throttle_calibrate is run
double throttle_read() {
  return (((double) (THROTTLE_MAX_POT_VAL_COUNTS - throttle_pos_counts)) / 
	  (THROTTLE_MAX_POT_VAL_COUNTS - THROTTLE_MIN_POT_VAL_COUNTS));
}

//Sets the gas actuator's position based on a float, which is on the 0-1 scale
//0 sets the actuator to let the engine idle
//1 is full throttle
int throttle_write(double desired_pos) {
  //  printf("\nSetting position to %f\n", position);
  desired_pos = throttle_in_range(desired_pos, 0, 1); //ensure a valid argument

  if(desired_pos == 0) {
    // If position is 0, increment more than the number of positions, which effectively 
    //calibrates the throttle every time the brake is actuated
    return(throttle_zero());  //this makes sure we're at 0, and sets, like we wanted to.
  } 

  else {
    int desired_pos_counts;
    int desired_pos_change_counts;

    // Otherwise, convert from the 0-1 scale to the digipot counts scale, and 
    // call the function to change the digipot
    
    desired_pos_counts = THROTTLE_MAX_POT_VAL_COUNTS - 
      ((int) ((THROTTLE_MAX_POT_VAL_COUNTS - THROTTLE_MIN_POT_VAL_COUNTS) * desired_pos));

    desired_pos_change_counts = desired_pos_counts - throttle_pos_counts;

    if(desired_pos_change_counts > 135) {
      throttle_calibrate();//this prevents undesired throttle operation
      //      cout<<"Throttle not calibrated before moved!! but it is now\n";
      return -1;
    }

    else {
      if(throttle_increment(desired_pos_change_counts) == -1) return -1;    
      throttle_pos_counts += desired_pos_change_counts;   

    }
  }
  return 0;
}

//Used by the MTA-ized vdrive-2.0 to reset throttle
int throttle_fault_reset() {
  return throttle_calibrate();
}


// Helper function to change the digipot by desired_pos_change_counts, on the 0-127 scale
int throttle_increment(int desired_pos_change_counts) {
  
  int lock_was_set=0; /*say that we couldn't get the lock, so, we have to jump into the 
		       *while statement
		       */
  timeval start_time, current_time;
  
  gettimeofday(&start_time, NULL);
  current_time = start_time;
  

  while( !lock_was_set & ( (current_time.tv_sec - start_time.tv_sec) < .05 ) )  {  
    //as long as we haven't acquired the lock, and also not timed out (50ms), 
    //try to get the lock.
    
    gettimeofday(&current_time, NULL); //refresh the current time
    lock_was_set = pp_lock(PP_GASBR);  //see if we got it, if we did, we'll continue
    
  }
  
  //could be here because either timed out, or lock was given
  
  if(lock_was_set) {
    if(desired_pos_change_counts > 0) {
      pp_set_bits(PP_GASBR, THROTTLE_UD|THROTTLE_CLK, 0);
      while(desired_pos_change_counts > 0) {
	pp_set_bits(PP_GASBR, 0, THROTTLE_CS);
	pp_set_bits(PP_GASBR, 0, THROTTLE_CLK);
	pp_set_bits(PP_GASBR, THROTTLE_CS, 0);      
	pp_set_bits(PP_GASBR, THROTTLE_CLK, 0);
	desired_pos_change_counts--;
      }
    }
    
    else if(desired_pos_change_counts < 0) {    
      pp_set_bits(PP_GASBR, THROTTLE_CLK, THROTTLE_UD);
      while(desired_pos_change_counts < 0) {
	pp_set_bits(PP_GASBR, 0, THROTTLE_CS);
	pp_set_bits(PP_GASBR, 0, THROTTLE_CLK);
	pp_set_bits(PP_GASBR, THROTTLE_CS, 0);
	pp_set_bits(PP_GASBR, THROTTLE_CLK, 0);
	desired_pos_change_counts++;
      }
    }
    //else {}  //desired_pos_change==0, so, nothing to do;
    pp_unlock(PP_GASBR);  //done with this port and can release the lock.
    return 0;  //we ran just fine
  }

  else{
    dbg_info("Didn't get to move within 50ms, couldn't get hold of parallel port\n");
    return -1;
  }
}

//Function to make sure min_val<=val<=max_val
double throttle_in_range(double val, double min_val, double max_val) {
	if(val < min_val) return min_val;
	else if(val > max_val) return max_val;
	else return val;
}
