#!/bin/bash

# Script for automating movie-making
if [[ ! "$1" ]]; then
  echo "Script for automating mpeg2 movie creation from a sequence of .bmp files"
  echo "Created for moviemaking from stereo vision logged images..."
  echo
  echo "Usage: $0 movie_name"
  echo "Examples:"
  echo "  $0 movie.mpg"
  exit 0;
fi

MOVIE=$1
STATEFILE=$(ls *.state)

echo "converting to .ppm format..."
mogrify -format ppm *.bmp
echo "converting to .ppm format: done!"

echo "removing old .bmp files..."
rm *.bmp
echo "removing old .bmp files: done!"

# Compile the fps_fixer applicaton:
echo "compiling and installing fps_fixer..."
pushd .
cd ~/dgc/local/utils/fps_fixer
make install
popd
echo "compiling and installing fps_fixer: done!"

# Make sure you have dgc/bin in your PATH (that's where fps_fixer binary executable is put)

echo "running fps_fixer..."
fps_fixer $STATEFILE 25
echo "running fps_fixer: done!"

# After a while you will have a new sequence of images created.

echo "creating movie..."
cat *.ppm | ppmtoy4m | mpeg2enc -F 3 -o $MOVIE
echo "creating movie: done"

exit 0
