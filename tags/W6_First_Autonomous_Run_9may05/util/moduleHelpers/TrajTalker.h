#ifndef _TRAJTALKER_H_
#define _TRAJTALKER_H_

#include "SkynetContainer.h"
#include "traj.h"
#include "pthread.h"
#include <unistd.h>

class CTrajTalker : virtual public CSkynetContainer
{
	char* m_pDataBuffer;

public:
	CTrajTalker();
	~CTrajTalker();

	bool SendTraj(int trajSocket, CTraj* pTraj, pthread_mutex_t* pMutex = NULL);
	bool RecvTraj(int trajSocket, CTraj* pTraj, pthread_mutex_t* pMutex = NULL);
};

#endif // _TRAJTALKER_H_
