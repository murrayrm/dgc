#include "StateClient.h"
#include "DGCutils"
#include <iostream>
#include <fstream>
using namespace std;

CStateClient::CStateClient()
{
	cerr << "In CStateClient()" << endl;

  // initialize the mutexes and the condition variable
  DGCcreateMutex(&m_actuatorstateMutex);
  DGCcreateMutex(&m_stateBufferMutex);
  DGCcreateMutex(&m_filledStateConditionMutex);
  DGCcreateCondition(&m_filledStateCondition);

  DGCcreateMutex(&m_futureStateConditionMutex);
  DGCcreateCondition(&m_futureStateCondition);
  m_bFutureStateDone = true;


  m_bFilledState = false;
	memset(state_buffer, 0, sizeof(state_buffer));
  memcpy(&m_state, &(state_buffer[0]), sizeof(m_state));
  state_index = 0;

  DGCstartMemberFunctionThread(this, &CStateClient::getStateThread);
  DGCstartMemberFunctionThread(this, &CStateClient::getActuatorStateThread);

	cerr << "Waiting for state data to fill..." << endl;
	DGCWaitForConditionTrue(m_bFilledState, m_filledStateCondition, m_filledStateConditionMutex);
	cerr << "State data filled!" << endl;
}

CStateClient::~CStateClient()
{
  DGCdeleteMutex(&m_stateBufferMutex);
  DGCdeleteMutex(&m_filledStateConditionMutex);
  DGCdeleteMutex(&m_futureStateConditionMutex);
  DGCdeleteCondition(&m_filledStateCondition);
  DGCdeleteCondition(&m_futureStateCondition);
}

void CStateClient::state_Interpolate(double val1, double val1_dot, double val2, double val2_dot, unsigned long long time1, unsigned long long time2, unsigned long long timereq, double& interp, double& interp_dot, double& interp_dot_dot)
{
	double scale = 1000000.0 / (time2 - time1);
	double val1_shift = 0;
	double val2_shift = val2 - val1;
	double a = val1_shift;
	double b = (val1_dot / scale);
	double c = -3.0*val1_shift - 2.0*(val1_dot / scale) + 3.0*val2_shift - 1.0*(val2_dot / scale);
	double d =  2.0*val1_shift + 1.0*(val1_dot / scale) - 2.0*val2_shift + 1.0*(val2_dot / scale);
	double t = (double)(timereq - time1) / (double)(time2 - time1);
	interp = (a + b*t + c*pow(t,2.0) + d*pow(t,3.0)) + val1;
	interp_dot = (b + 2.0*c*t + 3.0*d*pow(t,2.0)) * scale;
	interp_dot_dot = (2.0*c + 6.0*d*t) * pow(scale,2.0);

	//cout << "avg dvaldt: " << 0.5*(val1_dot+val2_dot) << endl;
	//cout << "cmp dvaldt: " << (val2-val1) / (double)(time2-time1) * 1.0e6 << endl;
	//cout << "where: " << t << endl;
// cout << "Time 1: " << time1 - time1 << endl;
// cout << "Time 2: " << time2 - time1 << endl;
// cout << "Time i: " << timereq - time1 << endl;


//   double scale = (double)(timereq - time1) / (double)(time2 - time1);
//   interp = val1 + (val2 - val1) * scale;
//   interp_dot = val1_dot + (val2_dot - val1_dot) * scale;
//   interp_dot_dot = 0;
}

void CStateClient::UpdateState()
{
  DGClockMutex(&m_stateBufferMutex);
  memcpy(&m_state, &(state_buffer[state_index]), sizeof(m_state));
  DGCunlockMutex(&m_stateBufferMutex);
}

void CStateClient::UpdateState(unsigned long long time_req)
{
  DGClockMutex(&m_stateBufferMutex);




	if(time_req > state_buffer[state_index].Timestamp)
	{
		m_bFutureStateDone = false;
		m_requestedTime = time_req;
  		DGCunlockMutex(&m_stateBufferMutex);
		DGCWaitForConditionTrue(m_bFutureStateDone, m_futureStateCondition, m_futureStateConditionMutex);
  		DGClockMutex(&m_stateBufferMutex);
	}

  int ind = state_index - 1;

  while (time_req < state_buffer[ind].Timestamp)
  {
    ind --;
    if (ind < 0)
      ind = 99;

		// if we wrapped around all the way: requested state too old
    if (ind == state_index) {
      ind = state_index + 1;
      if (ind == 100) ind = 0;

			memcpy(&m_state, &state_buffer[ind], sizeof(m_state));
			DGCunlockMutex(&m_stateBufferMutex);
			return;
    }
  }
  int ind_u = ind + 1;
  if (ind_u == 100) ind_u = 0;

  VehicleState vl = state_buffer[ind];
  VehicleState vu = state_buffer[ind_u];

  DGCunlockMutex(&m_stateBufferMutex);
    
  m_state.Timestamp = time_req;

  //cout << endl << "New Batch!" << endl;

  state_Interpolate(vl.Northing, vl.Vel_N, 
                    vu.Northing, vu.Vel_N, 
                    vl.Timestamp, vu.Timestamp, time_req,
                    m_state.Northing, m_state.Vel_N, m_state.Acc_N);

  state_Interpolate(vl.Easting, vl.Vel_E, 
                    vu.Easting, vu.Vel_E, 
                    vl.Timestamp, vu.Timestamp, time_req,
                    m_state.Easting, m_state.Vel_E, m_state.Acc_E);

  state_Interpolate(vl.Altitude, vl.Vel_D, 
                    vu.Altitude, vu.Vel_D, 
                    vl.Timestamp, vu.Timestamp, time_req,
                    m_state.Altitude, m_state.Vel_D, m_state.Acc_D);

  state_Interpolate(vl.Roll, vl.RollRate, 
                    vu.Roll, vu.RollRate, 
                    vl.Timestamp, vu.Timestamp, time_req,
                    m_state.Roll, m_state.RollRate, m_state.RollAcc);

  state_Interpolate(vl.Pitch, vl.PitchRate, 
                    vu.Pitch, vu.PitchRate, 
                    vl.Timestamp, vu.Timestamp, time_req,
                    m_state.Pitch, m_state.PitchRate, m_state.PitchAcc);

  state_Interpolate(vl.Yaw, vl.YawRate, 
                    vu.Yaw, vu.YawRate, 
                    vl.Timestamp, vu.Timestamp, time_req,
                    m_state.Yaw, m_state.YawRate, m_state.YawAcc);
}

void CStateClient::getStateThread()
{
  // listen for state from everybody.
  int statesocket = m_skynet.listen(SNstate, ALLMODULES);
  //ofstream logfile("interp_debug");

  while(true)
  {
    //    cerr << "GETTING STATE" << endl;
    if(m_skynet.get_msg(statesocket, &m_receivedState, sizeof(m_receivedState), 0) != sizeof(m_receivedState))
      cerr << "Didn't receive the right number of bytes in the state structure" << endl;

    //logfile.precision(20);
    //logfile << m_receivedState.Timestamp << '\t';
    //logfile << m_receivedState.Northing << '\t';
    //logfile << m_receivedState.Vel_N << endl;

    DGClockMutex(&m_stateBufferMutex);
    state_index++; 
    if (state_index == 100)
    {
      state_index = 0;
			if(!m_bFilledState)
				DGCSetConditionTrue(m_bFilledState, m_filledStateCondition, m_filledStateConditionMutex);
    }

    memcpy(&(state_buffer[state_index]), &m_receivedState, sizeof(m_state));
    DGCunlockMutex(&m_stateBufferMutex);
		if(!m_bFutureStateDone && state_buffer[state_index].Timestamp > m_requestedTime)
		{
			DGCSetConditionTrue(m_bFutureStateDone, m_futureStateCondition, m_futureStateConditionMutex);
		}
  }
}

void CStateClient::UpdateActuatorState()
{
	DGClockMutex(&m_actuatorstateMutex);
	memcpy(&m_actuatorState, &m_receivedActuatorstate, sizeof(m_actuatorState));
	DGCunlockMutex(&m_actuatorstateMutex);
}

void CStateClient::getActuatorStateThread()
{
  // listen for state from everybody.
  int actuatorstatesocket    = m_skynet.listen(SNactuatorstate, ALLMODULES);

  while(true)
  {
    if(m_skynet.get_msg(actuatorstatesocket, &m_receivedActuatorstate, sizeof(m_receivedActuatorstate), 0, &m_actuatorstateMutex) !=
			 sizeof(m_receivedActuatorstate))
		{
      cerr << "Didn't receive the right number of bytes in the actuatorstate structure" << endl;
		}
  }
}
