#ifndef _REFINEMENTSTAGE_H_
#define _REFINEMENTSTAGE_H_

#include "PlannerStage.h"
#include "AliceConstants.h"

class CRefinementStage : public CPlannerStage
{
	double*   m_pColl_speed;
	double*   m_pColl_dspeed;

	double*   m_pOutputSpeed;
	double*   m_pOutputDSpeed;

	bool			m_bGetVProfile;

	void NonLinearInitConstrFunc(void);
	void NonLinearTrajConstrFunc(int constr_index_snopt, int collIndex);
	void NonLinearFinlConstrFunc(void);
	void makeCollocationData(double* pSolverState);
	void getSplineCoeffsSpeed(double* pSolverState);

public:
	// this function takes in a traj and performs a least-squares fit to make it C^2
	void MakeTrajC2(CTraj* pTraj);
	void outputTraj(CTraj* pTraj);
	void SetUpLinearConstr(void);
	void SetUpPlannerBounds(VehicleState *pState);
	void makeSNOPTBoundsArrays(void);
	bool SetUpPlannerBoundsAndSeedCoefficients(double* pPrevState, VehicleState* pState);
	void funcConstr(void);
	void funcCost  (void);

	CRefinementStage(CMap *pMap = NULL, int mapLayerID = 0, RDDF* pRDDF = NULL, bool USE_MAXSPEED = false, bool bGetVProfile = false);
	~CRefinementStage();

	double getLength(void)
	{
		return m_pSolverState[NPnumVariables-1];
	}
};

#endif // _REFINEMENTSTAGE_H_
