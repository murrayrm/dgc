#ifndef _SPACIALSTAGE_H_
#define _SPACIALSTAGE_H_

#include "PlannerStage.h"
#include "AliceConstants.h"
#include "CPath.hh"


class CSpacialStage : public CPlannerStage
{
	CTraj *m_pSeedTraj;
	CPath m_trajgenpath;

	void NonLinearInitConstrFunc(void);
	void NonLinearTrajConstrFunc(int constr_index_snopt, int collIndex);

public:
	bool isFeasible(void);
	void outputTraj(CTraj* pTraj);
	void SetUpLinearConstr(void);
	bool SetUpPlannerBoundsAndSeedCoefficients(double* pPrevState, VehicleState* pState);
	void SetUpPlannerBounds(VehicleState *pState);
	void SeedCoefficients(double* pPrevState);
	void funcConstr(void);
	void funcCost  (void);

	CSpacialStage(CMap *pMap, int mapLayerID, RDDF* pRDDF, bool USE_MAXSPEED = false);
	~CSpacialStage();

	CTraj* getSeedTraj(void);
};

#endif // _SPACIALSTAGE_H_
