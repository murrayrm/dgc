function plot_path(varargin)
% 
% function plot_path(path_matrix)
% function plot_path(path_matrix, offset)
%
% Changes: 
%   11-9-2004, Jason Yosinski, created (some code is from plot_corridor.m)
%   12-12-2004, Jason Yosinski, added offset functionality
%
% Function:
%   This function plots a path as specified in a path matrix.
%
% Input:
%   path_matrix = [x1, y1, vx1, vy1, ax1, ay1;
%                  x2, y2, vx2, vy2, ax2, ay2;
%                  ..  ..  ...  ...  ...  ...;
%                  xN, yN, vxN, vyN, axN, ayN]
%       The last point in a path matrix is the endpoint of the path and
%       will be simply plotted as a point.  vxN, vyN, axN, and ayN are
%       irrelevant.
%   offset = [x_offset, y_offset]
%       These offsets will be subtracted from every point in the path, effectively
%       translating the entire path in space.  If offset is not used, the
%       path will be translated so that the path starts at the point (0,0)
%
% Output:
%   none
%
% Usage Example:
%   plot_path(path);
%   plot_path(path, [10, 20]);
%


% parameters to be set by user
res = 50;               % number of points plotted in a curve
point_style = 'ro';     % style for points (see help plot)
path_style = 'b-';      % style used for paths (see help plot)
line_width = .5;        % width of path lines (in points), default = .5


hold on;

% set varargin defaults
if(nargin < 1)
    error('Need an argument (waypoint_file)');
end

path = varargin{1};
N = size(path,1);

% the format defined by traj is [x xd xdd y yd ydd]
TRAJ_INPUT_FORMAT = 1;
if( TRAJ_INPUT_FORMAT )
  % copy the input path (format defined in traj.h)      
  path2 = path; % path2 = [x xd xdd y yd ydd]
  % reassign path to the format originally expected here
  %                  xN, yN, vxN, vyN, axN, ayN]
  path(:,1) = path2(:,4);
  path(:,2) = path2(:,1);
  path(:,3) = path2(:,5);
  path(:,4) = path2(:,2);
  path(:,5) = path2(:,6);
  path(:,6) = path2(:,3);
end

if(nargin == 1)
    % if they don't specify an offset, then we'll assume they want the path
    % plotted starting at (0,0)
    path(:,1) = path(:,1) - ones(N,1) .* path(1,1);   % offset the x's
    path(:,2) = path(:,2) - ones(N,1) .* path(1,2);   % offset the y's
elseif(nargin > 1)
    offset = varargin{2};
    path(:,1) = path(:,1) - ones(N,1) .* offset(1);   % offset the x's
    path(:,2) = path(:,2) - ones(N,1) .* offset(2);   % offset the y's
end

% make path x and y coordinates relative to the first point (ie make
%   x1 = 0; y1 = 0)
% OBSOLETE NOW DUE TO VARARGIN OFFSET SECTION ABOVE
% path(1:N,1) = path(1:N,1) - path(1,1);
% path(1:N,2) = path(1:N,2) - path(1,2);

PLOT_OTHER_STUFF_BESIDES_PATH = 1
if PLOT_OTHER_STUFF_BESIDES_PATH

% plot all but last path points and segments
for i = 1:N-1
    % plot point
    % removed for compatibility purposes
    % plot(path(i,1), path(i,2), point_style, 'LineWidth', line_width);
    plot(path(i,1), path(i,2), point_style);

    % plot connecting curve segment
    if path(i,5) == 0 & path(i,6) == 0
        % next path segment has no acceleration -> straight line
        p1 = path(i,1:2);                   % start point
        p2 = path(i+1,1:2);                 % end point
        v = path(i,3:4);                    % velocity (at start pt)
        length = (p2-p1) * v' ./ norm(v);    % length of segment
        s = p1 + v./norm(v) .* length;       % endpoint
        segpoints = [p1; s];
        % removed for compatibility purposes
        %    plot(segpoints(:,1), segpoints(:,2), path_style, 'LineWidth', line_width);
        plot(segpoints(:,1), segpoints(:,2), path_style);
    else
        % next path segment has curvature
        p1 = path(i,1:2);                   % start point
        p2 = path(i+1,1:2);                 % end point
        v = path(i,3:4);                    % velocity (at start pt)
        a = path(i,5:6);                    % accel (at start pt)
        c = p1 + a .* (norm(v)./norm(a)).^2;  % center point
        cp1 = p1 - c;                       % ray from c to p1
        cp2 = p2 - c;                       % ray from c to p2
        % calculate radians eclipsed by this segment
        % thetamax = acos((cp1*cp2')./(norm(cp1).*norm(cp2)));
        % theta = linspace(0, thetamax, res);
        t = linspace(0, 1, res);
        segpoints = zeros(res, 2);
        for j = 1:res   
            segpoints(j,:) = c + norm(p1-c) .* (((1-t(j)).*p1+t(j).*p2)-c)./norm(((1-t(j)).*p1+t(j).*p2)-c);
        end
        % removed ot make it compatible
        % plot(segpoints(:,1), segpoints(:,2), path_style, 'LineWidth', line_width);
        plot(segpoints(:,1), segpoints(:,2), path_style);
    end
end

% plot last point (no segment)
% X = Northing, Y = Easting (we'll plot Northing on the vertical axis and 
% Easting on the horizontal axis, just as if we were looking at a map)

% removed ot make it compatible
%plot(path(N,2), path(N,1), point_style, 'LineWidth', line_width);
plot(path(N,2), path(N,1), point_style);



% make it look nice
axis tight
axis equal
hold on
%grid on

else % only plot the path

  plot(path(:,1), path(:,2), 'b')

end

% norm(vec) is built-in
%function m = mag(vec)
%vec;
%m = sqrt(vec*transpose(vec));
