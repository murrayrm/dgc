function original = plot_corridor_matrix( varargin );
% 
% function original = plot_corridor_matrix( waypoint_file, [offset], [wprange] )
%
% Changes: 
%   01/03/05, Lars Cremean, created (pulled from plot_corridor_file)
%
% Function:
%   This function takes Bob format waypoint specification (in the form of a matrix) and displays a 
%   plot of the associated corridor.
%
% Inputs: 
%   corridor: matrix representing BOB format waypoint file.
%   offset (optional): 2-vector of [Easting Northing] offset of first waypoint.
%                      if not specified, the first waypoint in the file is used.
%   wprange (optional): 2-vector of integers indicating index range of 
%                       waypoints to plot [minwp maxwp]
% 
% Outputs:
%   original: Easting/Northing array of original waypoint locations.
%
% Options (change in .m file):
%   circle_style = 'k';     % linestyle of the circles in corridor
%   edge_style = 'k';       % linestyle of the edges of the corridor
%   draw_numbers = 10;   % draw waypoint numbers along the way (e.g. every 10 waypoints)
%
% Usage example:
%   wp = plot_corridor_file('waypoints.bob')
%   The waypoints file must be in Bob format!
%

% DEFAULT OPTIONS SECTION
circle_style = 'k';   % linestyle of the circles in corridor
edge_style = 'k';     % linestyle of the edges of the corridor
draw_numbers = 0;    % draw waypoint numbers along the way at draw_numbers spacing
                      % (1 is every waypoint, 10 is every 10, 0 is no waypoints)
draw_grid = true;     % draw grid or not - true or false
plot_xy_as_ne = 0;    % plot northing on X-axis and easting on Y-axis

%gset nokey
hold on;

% just for fun, output the total distance along the course
td = 0;

% set the defaults for the optional arguments 

if( nargin < 1 )
  error('Need an argument (waypoint_matrix)');
end
if( nargin >= 1 )
  corridor_matrix = varargin{1};

  eastings = corridor_matrix(:,1);
  northings = corridor_matrix(:,2);
  radii = corridor_matrix(:,3);
  vees = corridor_matrix(:,4);

  % waypoints array has columns {easting, northing}
  waypoints(:,1) = eastings;
  waypoints(:,2) = northings;

  % return the original waypoints
  original = [eastings northings];
end
% if there's a non-empty second argument
if( nargin >= 2 && ~isempty(varargin{2}) )
  offset = varargin{2};
  e_offset = offset(1);
  n_offset = offset(2);
else
  e_offset = eastings(1);
  n_offset = northings(1);
end
if( nargin >= 3 )
  limits = varargin{3};
  if( isempty(limits) ) 
    limits = [1; length(waypoints)];
  end
  if( prod(size(limits)) ~= 2 )
    error('Third argument should be a vector of integers that specify the waypoint limits; or empty to specify all waypoints.')
  end
else
  limits = [1; length(waypoints)]
end
limits % display them
if( nargin >= 4 )
  plot_xy_as_ne = varargin{4}
end
if( nargin >= 5 )
  error('Too many arguments!');
end

   
% format of waypoint_file
% easting northing GRRR
% make all the measurements relative to first waypoint
waypoints(:,1) = waypoints(:,1) - e_offset;
waypoints(:,2) = waypoints(:,2) - n_offset;

% draw circles around waypoints
theta = [0:30:360]/180*pi;

EIDX = 1; % easting index in waypoints array
NIDX = 2; % northing index in waypoints array

if( plot_xy_as_ne )
  XIDX = NIDX;
  YIDX = EIDX;
  xlabelstr='Northing (m)';
  ylabelstr='Easting (m)';
else
  XIDX = EIDX;
  YIDX = NIDX;
  xlabelstr='Easting (m)';
  ylabelstr='Northing (m)';
end

if draw_numbers
    text(waypoints(limits(1),XIDX), waypoints(limits(1),YIDX), num2str(limits(1)));
end

for i=limits(1):limits(2)-1;

    % draw a circle around the current waypoint
    circle_x = radii(i)*cos(theta);
    circle_y = radii(i)*sin(theta);
    plot(circle_x + waypoints(i,XIDX), circle_y + waypoints(i,YIDX),circle_style);

    % draw the same circle around the next waypoint
    plot(circle_x + waypoints(i+1,XIDX), circle_y + waypoints(i+1,YIDX), circle_style);
    if draw_numbers && mod(i+1,round(draw_numbers))==0
        text(waypoints(i+1,XIDX), waypoints(i+1,YIDX), num2str(i+1));
    end

    % draw the lateral offset lines 
    % get the angle to the next waypoint
    point = atan2( waypoints(i+1,YIDX) - waypoints(i,YIDX), ...
                   waypoints(i+1,XIDX) - waypoints(i,XIDX) );

    % draw the left lateral offset line
    fir = waypoints(i,[XIDX YIDX]) + radii(i) * [cos(point+pi/2) sin(point+pi/2)];
    nex = waypoints(i+1,[XIDX YIDX])+radii(i) * [cos(point+pi/2) sin(point+pi/2)];
    plot( [fir(1) nex(1)], [fir(2) nex(2)], edge_style);
    % draw the right lateral offset line
    fir = waypoints(i,[XIDX YIDX]) + radii(i) * [cos(point-pi/2) sin(point-pi/2)];
    nex = waypoints(i+1,[XIDX YIDX])+radii(i) * [cos(point-pi/2) sin(point-pi/2)];
    plot( [fir(1) nex(1)], [fir(2) nex(2)], edge_style);
    
    % increment the total distance of the course
    % (sum of track line distances)
    td = td + sqrt( (waypoints(i+1,1)-waypoints(i,1))^2 + ...
                    (waypoints(i+1,2)-waypoints(i,2))^2 );
end

axis('auto')
axis('equal')
xlabel(xlabelstr)
ylabel(ylabelstr)
hold on
if draw_grid
    grid on
end

% output the total distance
disp('Total corridor length =');
td
