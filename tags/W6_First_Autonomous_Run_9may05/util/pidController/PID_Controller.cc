/*
 *CPID_Controller: PID Lateral & Longitudinal Trajectory Follower implemented using
 *the wrapper classes (HISTORIC:PathFollower (MTA)), (HISTORIC:sn_TrajFollower (Skynet))
 *TrajFollower (CURRENT: Skynet)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <math.h>
#include <unistd.h>
#include <iostream> 
#include <iomanip>
#include <time.h>
using namespace std;

#include "AliceConstants.h" // for vehicle constants
#include "PID_Controller.hh"

/**
   Constructor: initializes variables, etc, etc
*/
CPID_Controller::CPID_Controller(EYerrorside yerrorside,
                                 EAerrorside aerrorside,
                                 bool bUseLateralFF)
: m_bUseLateralFF(bUseLateralFF)
{
  m_YerrorIndex = (yerrorside==YERR_FRONT ? FRONT : REAR);
  m_AerrorIndex = (aerrorside==AERR_FRONT ? FRONT : REAR); // NOTE that aerror==yaw does NOT get its own index
	m_AerrorYaw = aerrorside==AERR_YAW;

	//Added output of reference position for y-error & a-error when trajfollower is run
	//for information & confirmation
	cout <<"You selected:"<<' '<<m_YerrorIndex<<"for y-error reference position"<<endl;
	cout <<"You selected:"<<' '<<m_AerrorIndex<<"for a-error reference position"<<endl;

  setup_Output();

  // Allocate the speed (longitudinal) PID controller and the lateral PID
  // controller
  m_pSpeedPID   = new Cpid(LongGainFilename);
  m_pLateralPID = new Cpid(LatGainFilename);


	// first, reset all values to be stored. this makes memory profilers not
	// complain about uninitialized data
  m_Aerror[m_AerrorIndex] = 0.0;
	m_Yerror[m_YerrorIndex] = 0.0;
  m_Cerror                = 0.0;
  m_Verror                = 0.0;
  m_AccelFF               = 0.0;
  m_AccelFB               = 0.0;
  m_accelCmd              = 0.0;
  m_SteerFF               = 0.0;
  m_SteerFB               = 0.0;
  m_steerCmd              = 0.0;
  m_refSpeed              = 0.0;
  m_trajSpeed             = 0.0;
	m_oldSteerCmd           = 0.0;

  //logging the variables used (what all the consts in the .hh file were set to)
#warning "These logged constants are not correct if USE_LAT_GAIN_FILE is defined!"
  m_outputVars<<"VEHICLE_MAX_ACCEL = "<<VEHICLE_MAX_ACCEL<<endl;
  m_outputVars<<"VEHICLE_MAX_DECEL = "<<VEHICLE_MAX_DECEL<<endl;
  m_outputVars<<"VEHICLE_MAX_LATERAL_ACCEL = "<<VEHICLE_MAX_LATERAL_ACCEL<<endl;
  m_outputVars<<"DFE_DELTA_SLOPE = "<<DFE_DELTA_SLOPE<<endl;
  m_outputVars<<"DFE_DELTA_OFFSET = "<<DFE_DELTA_OFFSET<<endl;
  m_outputVars<<"PERP_THETA_DIST = "<<PERP_THETA_DIST<<endl;
  m_outputVars<<"ALPHA_GAIN = "<<ALPHA_GAIN<<endl;
  m_outputVars<<"BETA_GAIN = "<<BETA_GAIN<<endl;

  cout << "CPID_Controller constructor exiting" << endl;
}

CPID_Controller::~CPID_Controller()
{
  delete m_pSpeedPID;
  delete m_pLateralPID;
}

void CPID_Controller::setup_Output()
{
  time_t t = time(NULL);
  tm *local;
  local = localtime(&t);

  char stepsFileName[256];
  char pidFileName[256];
  char varsFileName[256];
  char state2FileName[256];
  char errorFileName[256];
  char commandsFileName[256];
  char testFileName[256];
  char velocityFileName[256];

  char varsFilePath[256];
  char pidFilePath[256];
  char state2FilePath[256];
  char stepsFilePath[256];
  char errorFilePath[256];
  char commandsFilePath[256];
  char testFilePath[256];
  char velocityFilePath[256];

  char lnCmd[256];

  //this creates the output file names as a function of time called  
  sprintf(varsFileName, "vars_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);

  sprintf(pidFileName, "pid_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);

  sprintf(stepsFileName, "steps_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);

  sprintf(errorFileName, "error_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);

  sprintf(velocityFileName, "velocity_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);

  sprintf(state2FileName, "state2_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);

  sprintf(commandsFileName, "commands_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);

  sprintf(testFileName, "test_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);

  sprintf(varsFilePath, "logs/%s", varsFileName);
  sprintf(pidFilePath, "logs/%s", pidFileName);
  sprintf(stepsFilePath, "logs/%s", stepsFileName);
  sprintf(state2FilePath, "logs/%s", state2FileName);
  sprintf(errorFilePath, "logs/%s", errorFileName);
  sprintf(commandsFilePath, "logs/%s", commandsFileName);
  sprintf(testFilePath, "logs/%s", testFileName);
  sprintf(velocityFilePath, "logs/%s", velocityFileName);

  //opens the files so that functions can write to them 
  m_outputError.open(errorFilePath);
  m_outputPID.open(pidFilePath);
  m_outputVars.open(varsFilePath);
  m_outputSteps.open(stepsFilePath);
  m_outputVelocity.open(velocityFilePath);
  m_outputState2.open(state2FilePath);
  m_outputCommands.open(commandsFilePath);
  m_outputTest.open(testFilePath);

  // make state.dat, angle.dat,yerror.dat,waypoint.dat link to newest files
  //I'm not sure how/why it works, but I copied it from Dima, and it seems to work perfectly
  lnCmd[0] = '\0';
  strcat(lnCmd, "cd logs; ln -fs ");
  strcat(lnCmd, varsFileName);
  strcat(lnCmd, " vars.dat; cd ..");
  system(lnCmd); 
 
  lnCmd[0] = '\0';
  strcat(lnCmd, "cd logs; ln -fs ");
  strcat(lnCmd, pidFileName);
  strcat(lnCmd, " pid.dat; cd ..");
  system(lnCmd); 
 
  lnCmd[0] = '\0';
  strcat(lnCmd, "cd logs; ln -fs ");
  strcat(lnCmd, stepsFileName);
  strcat(lnCmd, " steps.dat; cd ..");
  system(lnCmd); 


  lnCmd[0] = '\0';
  strcat(lnCmd, "cd logs; ln -fs ");
  strcat(lnCmd, state2FileName);
  strcat(lnCmd, " state2.dat; cd ..");
  system(lnCmd); 

  lnCmd[0] = '\0';
  strcat(lnCmd, "cd logs; ln -fs ");
  strcat(lnCmd, velocityFileName);
  strcat(lnCmd, " velocity.dat; cd ..");
  system(lnCmd);

  lnCmd[0] = '\0';
  strcat(lnCmd, "cd logs; ln -fs ");
  strcat(lnCmd, errorFileName);
  strcat(lnCmd, " error.dat; cd ..");
  system(lnCmd);

  lnCmd[0] = '\0';
  strcat(lnCmd, "cd logs; ln -fs ");
  strcat(lnCmd, commandsFileName);
  strcat(lnCmd, " commands.dat; cd ..");
  system(lnCmd);

  lnCmd[0] = '\0';
  strcat(lnCmd, "cd logs; ln -fs ");
  strcat(lnCmd, testFileName);
  strcat(lnCmd, " test.dat; cd ..");
  system(lnCmd);

  m_outputError << setprecision(20);
  m_outputPID << setprecision(20);
  m_outputVelocity << setprecision(20);
  m_outputState2 << setprecision(20);
  m_outputCommands << setprecision(20);
  m_outputSteps <<setprecision(20);
  m_outputTest <<setprecision(20);
}

/* getControlVariables is the main interface to the controller. This function is
 * called by trajFollower to get the desired accel and phi commands.  The
 * architecture it is based on can be seen in Alex's "trajfollower_flow.jpg"
 *
 * Inputs:  current vehicle state
 * Outputs: accel and phi, passed back through the provided pointers
 */

void CPID_Controller::getControlVariables(VehicleState *pState, double phicurrent, double *accel, double *phi)
{
  // If our reference is empty (if we haven't yet gotten a reference, set 0 inputs and return
  if(m_traj.getNumPoints() == 0)
  {
    *accel = 0.0;
    *phi   = 0.0;
    return;
  }

  // compute the state of the two axles
  compute_FrontAndRearState_veh(pState);
	m_vehPhi = phicurrent;

  // search the reference traj to get the index of the reference point
  // the argument is the fraction of the distance from the rear to the front 
  // axle. NOTE: The argument here represents a lookahead from the REAR axle.  
  // An argument of 1.0 here represents lookup at the front axle.
#warning "Check case where we are facing the wrong direction and argument is nonzero."

  m_refIndex = getClosestIndex(LOOKAHEAD_FF_LAT_REFPOS);

  // compute the desired state of the two axles
  compute_FrontAndRearState_ref();

  if( DEBUG_LEVEL > 1 )
  {
    cout << "m_refIndex: " << m_refIndex << endl;
  }

  // log to file
  m_outputState2 << m_vehN << ' '
                 << m_vehE << ' '
                 << m_refIndex << ' '
                 << m_refN << ' '
                 << m_refE << endl;


  // 
  /******* LATERAL CONTROL SETUP AND IMPLEMENTATION *******/
  //
  
  // Now compute our deviations from the reference state:
  compute_YError(); // The spatial error (distance to reference point)
  compute_AError(); // The angular error (error in the angle)
  compute_CError(); // The combined error (linear combination of the y and angle errors)

  m_outputError
    <<m_Yerror[FRONT]<<' '<<m_Aerror[FRONT]<< ' '
    <<m_Yerror[REAR] <<' '<<m_Aerror[REAR] <<' '<<m_Cerror<<' '<<endl;

  if( DEBUG_LEVEL > 0 )
  {
    printf("At %s:%d\n", __FILE__, __LINE__);
    printf("Yerror[FRONT, REAR] = [%f, %f]\n", m_Yerror[FRONT], m_Yerror[REAR] );
    printf("Aerror[FRONT, REAR] = [%f, %f] DEGREES\n", 
           m_Aerror[FRONT]*180.0/M_PI, m_Aerror[REAR]*180.0/M_PI );
    printf("Cerror = %f\n", m_Cerror );
  }
  
  // compute the commanded steering value
#ifdef PURE_PURSUIT_LATERAL_CONTROL
  compute_SteerCmd_purepursuit(5.0);
#else
  compute_SteerCmd();
#endif

  //
  /******* LONGITUDINAL CONTROL SETUP AND IMPLEMENTATION *******/
  // 
  
  // compute the actual speed
  m_actSpeed = hypot(m_vehDN[FRONT], m_vehDE[FRONT]);

  // Calculate the reference speed
  compute_refSpeed();

  m_Verror = m_refSpeed - m_actSpeed;

  m_outputVelocity<<m_refSpeed<<' '<<m_actSpeed<<' '<<m_Verror<<endl;   

  //Calculate the commanded acceleration
  compute_AccFF();

  if( DEBUG_LEVEL > 1 )
  {
    cout<<"stepping speed..."<<endl;
  }
  m_AccelFB = m_pSpeedPID->step_forward(m_Verror);
  m_accelCmd = m_AccelFF + m_AccelFB;
  //Check the commanded acceleration is < +/- saturation, if not saturate 
  //steering command
  m_accelCmd = fmax(fmin(m_accelCmd, VEHICLE_MAX_ACCEL), -VEHICLE_MAX_DECEL);
  if( DEBUG_LEVEL > 1 )
  {
    cout<<"accel Cmd, FF, FB: "<<m_accelCmd<<' '<<m_AccelFF<<' '<<m_AccelFB<<endl;
  }

  // logging
  m_outputTest<<"desired accel, bounds: "<<m_accelCmd<<' '
              <<VEHICLE_MAX_ACCEL<<' '<<VEHICLE_MAX_DECEL<<endl;
  

  if( DEBUG_LEVEL > 1 )
  {
    cout<<"FRONT yerror, aerror: "<<m_Yerror[FRONT]<<' '<<m_Aerror[FRONT]<<endl;
    cout<<"REAR yerror, aerror: "<<m_Yerror[REAR]<<' '<<m_Aerror[REAR]<<endl;
  }



  //checking if we're approaching the end of the path
  //I'm not sure if this is the best way to do it, but I'd rather not be inadvertently slowing down a trajectory unneedfully. 
  int end, ref, Ne, Ee, Nr, Er, dist,diff;
  end = m_traj.getNumPoints();
  ref = m_refIndex;
  /* only uncomment this if you'd rather use distance than number of traj points left
   Ne = m_traj.getNorthing(end);
  Ee = m_traj.getEasting(end);
  Nr = m_traj.getNorthing(ref);
  Er = m_traj.getEasting(ref);
  dist = sqrt(pow((Ne-Nr),2)-pow((Ee-Er),2)); 
  */
  diff = end-ref;
  // cout<<end<<' '<<ref<<' '<<diff<<endl;
  //hopefully, this will be about 10 meters, which is sufficient stopping distance for the speeds we'll be traveling. easily increased if this is not the case. sorry for the magic numbers
  if(diff <= 25) {
    //this will be conveted to a brake command later..currently in m/s^2
    //hopefully, this will correpond to stomping on the brakes
    m_accelCmd = -5;
  }

m_outputCommands<<m_steerCmd<<' '<<m_accelCmd<<' '<<endl;
  //
  /******* SETTING THE OUTPUT VARIABLE POINTERS *******/
  // 
  *accel = m_accelCmd;
  *phi   = m_steerCmd;

}

int CPID_Controller::getClosestIndex(double frac)
{
  // Determine the closest point on the reference traj (reference traj is of the
  // REAR axle) to the vehicle's rear axle. This reference point represents the
  // reference value we want the controller to match to the rear axle.
  NEcoord rear(m_vehN[REAR], m_vehE[REAR]);
  NEcoord front(m_vehN[FRONT], m_vehE[FRONT]);

  // Calculate the point that is "frac" distance from the rear axle to the 
  // front axle location.
  NEcoord look = rear * (1.0 - frac) + front * frac;
 
  return m_traj.getClosestPoint(look.N, look.E);
}

/*! This function takes in the state of the vehicle and stores the pertinent
  information about both axles into the data structures in the class
*/
void CPID_Controller::compute_FrontAndRearState_veh(VehicleState* pState)
{
  // get the various elements of state for the two axles. If we're moving very
  // slowly, assume that the wheels are straight
  m_vehN      [REAR] = pState->Northing_rear();
  m_vehE      [REAR] = pState->Easting_rear();
  m_vehDN     [REAR] = pState->Vel_N_rear();
  m_vehDE     [REAR] = pState->Vel_E_rear();
  m_vehAngle  [REAR] = m_AerrorYaw ? pState->Yaw :
		(pState->Speed2() > MINSPEED ? atan2(m_vehDE[REAR], m_vehDN[REAR]) : pState->Yaw);

  m_vehN      [FRONT] = pState->Northing;
  m_vehE      [FRONT] = pState->Easting;
  m_vehDN     [FRONT] = pState->Vel_N;
  m_vehDE     [FRONT] = pState->Vel_E;
  m_vehAngle[FRONT] = (pState->Speed2() > MINSPEED ?
                       atan2(m_vehDE[FRONT], m_vehDN[FRONT]) : pState->Yaw);

  m_vehYaw            = pState->Yaw;
  m_yawdot            = pState->YawRate;
 
  if (DEBUG_LEVEL > 0)
  {
    cout << "REAR veh diff N" << ' ' <<m_vehDN[REAR] << endl;
    cout << "REAR veh diff E" << ' ' <<m_vehDE[REAR] << endl;
  }
  if (DEBUG_LEVEL > 1)
  {
    cout << "FRONT veh diff N" << ' ' <<m_vehDN[FRONT] << endl;
    cout << "FRONT veh diff E" << ' ' <<m_vehDE[FRONT] << endl;
  }
}

/*! This function computes the pertinent information about the reference state
  of both axles into the data structures in the class
*/
void CPID_Controller::compute_FrontAndRearState_ref()
{
  m_refN      [REAR] = m_traj.getNorthingDiff(m_refIndex, 0);
  m_refE      [REAR] = m_traj.getEastingDiff (m_refIndex, 0);
  m_refDN     [REAR] = m_traj.getNorthingDiff(m_refIndex, 1);
  m_refDE     [REAR] = m_traj.getEastingDiff (m_refIndex, 1);
  m_refDDN_REAR      = m_traj.getNorthingDiff(m_refIndex, 2);
  m_refDDE_REAR      = m_traj.getEastingDiff (m_refIndex, 2);
  m_refAngle[REAR] = atan2(m_refDE[REAR], m_refDN[REAR]);

  m_refN      [FRONT] = m_refN[REAR] + VEHICLE_WHEELBASE*cos(m_refAngle[REAR]);
  m_refE      [FRONT] = m_refE[REAR] + VEHICLE_WHEELBASE*sin(m_refAngle[REAR]);

	double speedRear = hypot(m_refDN[REAR], m_refDE[REAR]);

  m_refDN     [FRONT] = m_refDN[REAR] - VEHICLE_WHEELBASE*sin(m_refAngle[REAR]) * (m_refDN[REAR]*m_refDDE_REAR - m_refDDN_REAR*m_refDE[REAR]) / speedRear/speedRear;
  m_refDE     [FRONT] = m_refDE[REAR] + VEHICLE_WHEELBASE*cos(m_refAngle[REAR]) * (m_refDN[REAR]*m_refDDE_REAR - m_refDDN_REAR*m_refDE[REAR]) / speedRear/speedRear;
  m_refAngle[FRONT]   = atan2(m_refDE[FRONT], m_refDN[FRONT]);

  if( DEBUG_LEVEL > 0 )
  {
    printf("REAR REF (N, E) = (%f, %f)\n", m_refN[REAR], m_refE[REAR] );
    printf("REAR REF (Nd, Ed) = (%f, %f)\n", m_refDN[REAR], m_refDE[REAR] );
    printf("REAR REF (Ndd, Edd) = (%f, %f)\n", m_refDDN_REAR, m_refDDE_REAR );
    printf("REAR REF HEADING = %f deg\n", m_refAngle[REAR]*180.0/M_PI );
  }
  if( DEBUG_LEVEL > 1 )
  {
    printf("FRONT REF (N, E) = (%f, %f)\n", m_refN[FRONT], m_refE[FRONT] );
    printf("FRONT REF (Nd, Ed) = (%f, %f)\n", m_refDN[FRONT], m_refDE[FRONT] );
    printf("FRONT REF HEADING = %f deg\n", m_refAngle[FRONT]*180.0/M_PI );
  }
}

/**
 *function: compute_YError
 *action: Function to calculate the y-error for any given position of Alice, y-error (m) is
 *defined as the PERPENDICULAR distance of Alice's reference point from the CURRENT 
 *path-segment (this is a straight line segment drawn between the CURRENT and NEXT traj-points)
 */
void CPID_Controller::compute_YError()
{
  // the deviations in northing and easting
  double errN, errE;

  errN = m_refN[FRONT] - m_vehN[FRONT];
  errE = m_refE[FRONT] - m_vehE[FRONT];
  m_Yerror[FRONT] = (-sin(m_refAngle[FRONT])*errN +
                      cos(m_refAngle[FRONT])*errE);

  errN = m_refN[REAR] - m_vehN[REAR];
  errE = m_refE[REAR] - m_vehE[REAR];
  m_Yerror[REAR]  = (-sin(m_refAngle[REAR])*errN +
                      cos(m_refAngle[REAR])*errE);
}

/**
 *function: compute_Aerror
 *action: This function calculates the angle error, defined as [DESIRED angle (rad) - 
 *ACTUAL angle (rad)], where the DESIRED angle is calculated from the first
 *derivatives of northing & easting at the CURRENT traj-point, and the ACTUAL
 *angle is taken as the YAW stored in astate
 *the current error in angle from our path. Note that it uses the N/E convention, as it uses the *GLOBAL reference frame (NOT the vehicle reference frame)
 *output: angle in radians returned as type double
 *
 **/
void CPID_Controller::compute_AError() 
{
  m_Aerror[FRONT] = m_refAngle[FRONT] - m_vehAngle[FRONT];
  m_Aerror[REAR]  = m_refAngle[REAR]  - m_vehAngle[REAR];

  //Constrain the value of h_error to be within the range -pi -> pi    
  m_Aerror[FRONT] = atan2(sin(m_Aerror[FRONT]), cos(m_Aerror[FRONT]));
  m_Aerror[REAR]  = atan2(sin(m_Aerror[REAR]),  cos(m_Aerror[REAR]));
}

/**
 *function: compute_Cerror
 *input: y-error and h-error values output from compute_YError and compute_Aerror
 *action: Function to calculate the combination error (m_Cerror) from the y_error and h_error
 *using the values of alpha & beta (gains) specified in the header file associated
 *with this source file.  Alpha represents the differential weighting between the
 *y-error and h-error in the combination error equation, and Beta is used to magnify
 *the h-error so that it has a magnitude approximately equal to the y-error for 
 *'equal' badness
 *output: perpendicular distance from the traj-line, in meters
 */
void CPID_Controller::compute_CError()
{
  //y-error_tilda: y-error after consideration of the perpendicular
  //theta distance, this is the y-error, greater than which the
  //controller commands a phi such that Alice heads directly
  //towards the path (angle perpendicular to that of the path)
  // y_error_tilda is m_Yerror, saturated so that 
  // -PERP_THETA_DIST <= y_error_tilda <= PERP_THETA_DIST
  double y_error_tilda;

  double selectedYError = m_Yerror[m_YerrorIndex];
  double selectedAerror = m_Aerror[m_AerrorIndex];

  y_error_tilda  = fmax(fmin(selectedYError, PERP_THETA_DIST), -PERP_THETA_DIST);
  m_Cerror = ((ALPHA_GAIN*y_error_tilda) + ((1.0-ALPHA_GAIN)*BETA_GAIN*selectedAerror));
}


// See header file for documentation.
void CPID_Controller::compute_SteerCmd_simple()
{
  // This function implements the following simple lateral controller
  // \phi = \theta_e + \beta tan^{-1}(\alpha y_error), where
  // \phi = steering command [rad]
  // \theta_e = yaw error [rad]
  // y_e = y_error [m]
  // This function sets the member variable m_steerCmd.
 

  // Note that this function is expected to be called from the end of 
  // getControlVariables, so we assume that m_Aerror[2] and m_Yerror[2]
  // have been properly set (with compute_AError() and compute_YError()) before 
  // this function is called.
  
  // \alpha, \beta = parameters
  // alpha is determined here heuristically, by defining what fraction we want 
  // to point directly toward the path (e.g. 0.95) at some distance away (e.g.  
  // 20.0 [m])
  double alpha = tan(0.95*M_PI/2.0)/20.0; 

  m_SteerFF = compute_SteerFF_simple();
  // Do the computation
  //double beta = 0.01;
  //m_steerFB = beta * atan(alpha * m_Yerror[m_YerrorIndex]);
  // Postscript: This tended toward oscillatory behavior, presumably due to 
  // closed-loop delays in steering and messaging.  A derivative term on the 
  // y_error should result in better performance.

  double error = atan(alpha * m_Yerror[m_YerrorIndex]);
  
  //calculate the feedback (lateral) phi term
  double p,i,d;
  p=m_pLateralPID->getP();
    i=m_pLateralPID->getI();
  d=m_pLateralPID->getD();
  m_outputPID<<p<<' '<<i<<' '<<d<<endl;

  m_SteerFB = m_pLateralPID->step_forward(error);
 
#ifdef USE_DFE
  m_steerCmd = DFE(m_SteerFB + m_SteerFF, m_oldSteerCmd);        
#else
  m_steerCmd = m_SteerFB + m_SteerFF;        
#endif
  //  cout<<m_SteerFF<<' '<<m_SteerFB<<endl;
  m_outputSteps<<m_SteerFF<<' '<<m_SteerFB<<endl;

  m_steerCmd = fmax(fmin(m_steerCmd, VEHICLE_MAX_RIGHT_STEER), -VEHICLE_MAX_LEFT_STEER);
  m_oldSteerCmd = m_steerCmd;
}


// See header file for documentation.
void CPID_Controller::compute_SteerCmd_purepursuit(double lookahead)
{
  // This function implements a simple lateral controller that points the 
  // steering wheels at some point on the trajectory a distance ahead of the 
  // front axle.  It sets the member variables m_steerCmd, m_SteerFF and 
  // m_SteerFB

  // get the closest index on the trajectory from the front axle
  int frontind = getClosestIndex(1.0);
  int targetind = m_traj.getPointAhead(frontind, lookahead);

  if( DEBUG_LEVEL > 0 )
  {
    printf("[%s:%d] (frontind, targetind) = (%d, %d)\n", 
           __FILE__, __LINE__, frontind, targetind);
  }
 
  NEcoord target(m_traj.getNorthing(targetind), m_traj.getEasting(targetind));
  // global steering angle = Yaw + steer
  double angle = atan2(target.E-m_vehE[FRONT], target.N-m_vehN[FRONT]);
  
  m_SteerFF = atan2(sin(angle - m_vehYaw), cos(angle - m_vehYaw));
  m_SteerFB = 0.0;


#ifdef USE_DFE
  m_steerCmd = DFE(m_SteerFB + m_SteerFF, m_oldSteerCmd);        
#else
  m_steerCmd = m_SteerFB + m_SteerFF;        
#endif

  m_outputSteps<<m_SteerFF<<' '<<m_SteerFB<<endl;

  m_steerCmd = fmax(fmin(m_steerCmd, VEHICLE_MAX_RIGHT_STEER), -VEHICLE_MAX_LEFT_STEER);
  m_oldSteerCmd = m_steerCmd;
}

/**
 * This function computers the steering command from the FeedForward and
 * FeedBack components.
 **/
void CPID_Controller::compute_SteerCmd()
{
  //calculate the feed-forward (lateral) phi term. If the user disabled the
  //lateral feed forward term, this will get set to 0
#ifdef SIMPLE_STEER_FEEDFORWARD
  m_SteerFF = compute_SteerFF_simple();
#else
  compute_SteerFF();
#endif
  
  if( DEBUG_LEVEL > 1 )
  {
    cout<<"stepping steer..."<<endl;
  }

  double p,i,d;
  p=m_pLateralPID->getP();
  i=m_pLateralPID->getI();
  d=m_pLateralPID->getD();
  m_outputPID<<p<<' '<<i<<' '<<d<<endl;

  //calculate the feedback (lateral) phi term
  m_SteerFB = m_pLateralPID->step_forward(m_Cerror);

  //calculate the steering command output
#ifdef USE_DFE
  m_steerCmd = DFE(m_SteerFB + m_SteerFF, m_oldSteerCmd);        
#else
  m_steerCmd = m_SteerFB + m_SteerFF;
#endif
  //  cout<<m_SteerFF<<' '<<m_SteerFB<<endl;
  m_outputSteps<<m_SteerFF<<' '<<m_SteerFB<<endl;

  if( DEBUG_LEVEL > 1 )
  {
    // note that the output is in RADIANS
    printf("(m_steerCmd, m_SteerFF, m_SteerFB, m_vehPhi) = (%f, %f, %f, %f)\n", 
             m_steerCmd, m_SteerFF, m_SteerFB, m_vehPhi);
  }

  //NOTE that at this point, we're still in radians (conversion happens in trajFollower)
  m_outputTest<<"steerCmd, m_SteerFF, m_SteerFB: "<<m_steerCmd<<' '<<m_SteerFF<<' '<<m_SteerFB<<endl;

  //Check the commanded steering angle IS < +/- saturation, if not saturate steering command
  m_steerCmd = fmax(fmin(m_steerCmd, VEHICLE_MAX_RIGHT_STEER), -VEHICLE_MAX_LEFT_STEER);
  m_oldSteerCmd = m_steerCmd;
}

/**
 *function: compute_SteerFF
 *input: 
 *action: This feed-forward term uses the first and second order northing & easting derivatives 
 *included in the traj file to determine the nominal steering angle that would be required
 *to follow the path - (calculations based upon the closest point in the traj file to 
 *the vehicles current position).  The nominal steering angle is calculated based upon
 *the nominal angle (commanded by the point in the traj file) using the REAR AXLE
 *centred model, i.e. this uses function uses the CURRENT REAR TRAJ-POINT for the spatial
 *variables & their derivatives used. This is important, and CANNOT be changed to the FRONT
 *axle system, as in the equations for the FRONT axled model, there is an additional UNKNOWN
 *term which is very tricky to evaluate, and hence the Planner uses the REAR axled model
 *and the Controller MUST be exactly consistent with the planner for lateral feed-forwared, as it
 *uses the second derivatives (whose values are dependent upon the model used)
 *output: feed-forward steering angle (radians)
 */
void CPID_Controller::compute_SteerFF()
{
  // if the user disabled the lateral feed-forward term, set it to 0
  if(!m_bUseLateralFF)
  {
    m_SteerFF = 0.0;
    return;
  }

  double speedRef = hypot(m_refDN[REAR], m_refDE[REAR]);
  
  m_SteerFF = atan(VEHICLE_WHEELBASE *
                   (m_refDN[REAR]*m_refDDE_REAR - m_refDDN_REAR*m_refDE[REAR])
                   / pow(speedRef, 3.0));
 

}

// Doxygen comments are generated from the header file.
double CPID_Controller::compute_SteerFF_simple()
{
  // if the user disabled the lateral feed-forward term, set it to 0
  if(!m_bUseLateralFF) { return 0.0; }
#warning "compute_SteerFF_simple is only valid if the --yaw option is used."
  // m_refAngle[FRONT] is the direction the front of the vehicle would move if 
  // the rear of the vehicle followed exactly the (rear-axle based) reference 
  // trajectory.  m_vehAngle[REAR] is the vehicle yaw if no sideslip.
  return m_refAngle[FRONT] - m_vehAngle[REAR];
}



/**
 *function: DFE
 *inputs: desired steering command (on a scale of -1 to 1)
 *action: compares desired steering angle to curr angle and speed, and
 *   places hard boundaries on the angle and angle's rate of change
 *   as a function of speed
 *outputs: new, bounded steering command
 **/
double CPID_Controller::DFE(double cmd, double old)
{
  //I'd like this to take in the actual steering angle, but since I don't currently have that we have to use previously commanded one instead
  double speed,diff, maxCmd, maxDiff,newCmd;
  speed = hypot(m_vehDN[FRONT], m_vehDE[FRONT]);

  diff = cmd-old;
  //these will be the functions of speed that limit

	/*
	 *               a=v*theta_dot             //for circular motion with
	 *   ->  theta_dot=a/v                     //with a and v scalar
	 *
	 *       theta_dot=v*sin(phi)/l            //from bicycle model
	 *   ->        a/v=v*sin(phi)/l
	 *   ->      |phi|=arcsin(a*l/v^2)         //for a*l/v^2<=1
	 *   ->      |phi|~=a*l/v^2                //for small phi
	 */
	if(speed == 0) speed = .001;	
	maxCmd = asin(VEHICLE_MAX_LATERAL_ACCEL*VEHICLE_WHEELBASE/(speed*speed));

	/*I doubt that we could actually damage the vehicle by turning the wheel too
		fast, so maxDiff can probably be removed. --Ryan Cable 2005-03-28*/
        //this is currently a pass-through
	maxDiff = DFE_DELTA_SLOPE*speed+DFE_DELTA_OFFSET;

  newCmd = cmd;

  if(diff > maxDiff) 
  {
    //cout<<"diff too large"<<endl;
    newCmd = maxDiff+old;
  }
  if(diff < -maxDiff) 
  {
    //cout<<"diff too small"<<endl; 
    newCmd = -maxDiff+old;
  }

  if(newCmd > maxCmd)
  {
    if( DEBUG_LEVEL > 1 ) 
    { 
      cout<<"new command too large"<<endl; 
    }
    newCmd = maxCmd;
  }    

  if(newCmd < -maxCmd) 
  {
    if( DEBUG_LEVEL > 1 ) 
    { 
      cout<<"new command too small"<<endl;
    }
    newCmd = -maxCmd;
  }
  //note, this is in radians
  return newCmd;

}

void CPID_Controller::compute_trajSpeed() 
{
  //taking the desired velocity to be the sqrt of E' and N' squared

  m_trajSpeed = hypot(m_refDN[FRONT], m_refDE[FRONT]);
}

void CPID_Controller::compute_refSpeed() 
{
  //Retrieve the desired speed
  compute_trajSpeed();

  // Handle the case where the traj speed is slow anyway
  if( m_trajSpeed <= V_LAT_ERROR_SAT )
  {
    m_refSpeed = m_trajSpeed;
    return;
  }
  
  m_refSpeed = m_trajSpeed + fabs(m_Cerror)*(V_LAT_ERROR_SAT - m_trajSpeed)/C_ERROR_SAT;
  
  // Conditional to ensure that the vref speed is NEVER negative (i.e. not trying to slow down
  // below zero, without this check this condition can occur, and is a parasitic case
  if( m_refSpeed <= V_LAT_ERROR_SAT )
	{
		m_refSpeed = V_LAT_ERROR_SAT;
		return;
	}
  // The previous code implements what's described in the pictorial below, 
  // where the horiz. axis is fabs(combined error) and the vertical axis is 
  // reference speed.
  //
  //           - <- v_traj (m_trajSpeed)
  //            \ 
  //             \
    //              \ 
  //               \-------- <- V_LAT_ERROR_SAT
  //           
  //        0--x   ^ 
  //           |   |
  //           0   C_ERROR_SAT  
  //
  //IF the c-error is > the saturation value, saturate the reference speed at the specified
  //saturation speed, otherwise set the reference speed to a value determined by a linear
  //-ve gradient function with a y-intercept equal to the desired speed (from traj) 
  if(fabs(m_Cerror) >= C_ERROR_SAT) 
  {
    m_refSpeed = V_LAT_ERROR_SAT;
  }
  
  
}

void CPID_Controller::compute_AccFF() 
{
#warning "Check m_AccelFF here"
  m_AccelFF = (m_refDN[REAR]*m_refDDN_REAR + m_refDE[REAR]*m_refDDE_REAR) 
              / hypot(m_refDN[REAR], m_refDE[REAR]);

  if( DEBUG_LEVEL > 1 )
  {
    cout<<"N ddot, E ddot and Accel FF: "<<m_refDDN_REAR<<' '<<m_refDDE_REAR
        <<' '<<m_AccelFF<<' '<<m_AccelFF<<endl;
  }
}

/**
 *SetNewPath: the method by which PathFollower can inform CPID_Controller of new plans.
 *inputs: pointer to new path
 *outputs: none, it changes internal variables in CPID_Controller
 */
void CPID_Controller::SetNewPath(CTraj* pTraj)
{
  // Set the member variable
  m_traj = *pTraj;
  m_outputTest<<"subsequent first path points: "<<m_traj.getNorthing(0)<<' '<<m_traj.getEasting(0)<<endl;


  // received a new trajectory, so reset the PID controllers
  
  //commenting out the speed controller reset, as with the planner this leads to constant oscillations in teh engine revs/brake (alternately)
//  m_pSpeedPID->reset();
  m_pLateralPID->reset();
}


