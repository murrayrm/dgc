#error "sn_msg.h is obsolete!  Use sn_msg.hh instead"

/* This is an example sn_msg.h for skynet.  It is intended as a demonstration
 * only
 */

#ifndef sn_msg_H
#define sn_msg_H

#include "sn_types.h"
#include <stddef.h>
#include <netinet/in.h>
/* sn_msg and modulename types are temporary.  Eventually their functionality
   will be replaced by an rc file
*/


/** \var int sn_key 
 * sn_key is a magic key that must be the same for all modules which
 * intend to interact with each other.  It is used in exactly the same way as
 * the recievekey from the old MTA.  In the final version of SkyNet, it will
 * probably not be set in this file, but some other
 */

//const int sn_key = 12345;

/** note: many of these addresses are claimed for specific services.  Sending
   messages with TTL > 1 may hose some of these services and piss people off.
   Conversely, if our router is configured to forward multicast traffic from
   outside, it will piss us off
*/
#define MIN_ADDR "224.0.2.0"
#define MAX_ADDR "238.255.255.255"
#if 0
const int MIN_PORT = 1024;  /* can be opened by non-priveleged process */
const int MAX_PORT = 65535;
#endif

/** get_dg_addr figures out what multicast address to use for a datagram-like
 * connection.  With multicast, this is a simple function.  Without, we would
 * probably have to consult the SkyNet daemon.  Also, I don't know if multicast
 * is efficient for intracomputer communications.  If not, we may want to use
 * unix sockets for that instead.  Using sn_key from the outer scope may
 * not work if this function is implemented within a dynamically linked library.
 */
int get_dg_addr(int sn_key, sn_msg type, struct in_addr *my_addr);

#define dg_port  38475 //random


/** sn_register is used for both control and messaging purposes.  A module should 
   register before it can send or receive messages.  However, debugging modules
   can operate with limited functionality without registring.
   return value is your module ID.  -1 on error.
*/

int sn_register(modulename myname, int sn_key);

/** sn_unregister tells skynetd that the module is quitting.  It is not stictly
  * neccesary, but will prevent memory leaks in skynetd.
*/
int sn_unregister();

/** sn_listen takes as an argument the type of messages you want to listen to and
   the name of the module that is sending them.  You can't listen to all
   messages of a all types coming from one module.  Return value is the socket 
   to use.  return value of -1 indicates error.
*/

int sn_listen(sn_msg mytype, modulename somemodule);

#define sn_BLOCK    0   /*wait till a message is ready*/
#define sn_NOBLOCK  1   /*return immidieatly*/
/* these can only be implemented if the messaging layer knows the size of a type
#define sn_FIRSTMSG 0   
#define sn_LASTMSG  2   
#define sn_ALLMSG   4
*/

/** sn_get_msg gets a message.  Socket is what you got from sn_listen.  mybuf
   is a buffer that the user must allocate.  bufsize is the size in bytes of
   that buffer.  options is an ORed list of the following: 0 is default.
   Return value is #of bytes read, or -1 on error.
*/

size_t sn_get_msg(int socket, void* mybuf, size_t bufsize, int options);

/** sn_get_send_sock gets a socket to send stuff to.  It returns the number of
   the socket, or -1 on error.  It's not neccesary to have this call, but it
   makes it more efficient to send messages.
*/

int sn_get_send_sock(sn_msg type);

/* sn_BLOCK and sn_NOBLOCK apply */
/* sn_SENDALL and sn_SENDONE only apply if the messaging layer knows a message
   size */
#define sn_SENDALL  0
#define sn_SENDONE  2

/** sn_send_msg sends a messages.  You cannot choose to whom to send a message,
   you may only send it.  return value is the number of bytes sent.  -1 on eror.
   Options are as follows: 0 is default
  */
size_t sn_send_msg(int socket, void* msg, size_t msgsize, int options);

/** sn_send_atomic sends a message and guarantees that it will be correctly
   received.  This is slower than sn_send_msg, so it should only be used when
   you expect your messages to be more than 1000 bytes.  This call always
   blocks.  returns size of data sent, or -1 on error.
*/

size_t sn_send_atomic(int socket, void* msg, size_t msgsize, int options);

/** sn_get_atomic gets a message and guarantees atomicity.  It MUST be used
   iff the message was sent with sn_send_atomic.  return value is number
   of bytes received, or -1 on error.  
*/

size_t sn_get_atomic(int socket, void* mybuf, size_t bufsize);
#endif
