#include "sn_msg.h"
#include "string.h"

int main(){
  modulename myself = stereo;
  sn_msg myoutput = pointcloud;
  const int sn_key = 12345;
 
  sn_register(myself, sn_key);
  int socket = sn_get_send_sock(myoutput);
  char* mymsg = "Hello, World!";
  sn_send_msg(socket, (void*)mymsg, strlen(mymsg)+1, 0);
  return 0;
  }
