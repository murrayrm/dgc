#ifndef __OPENGL_WIDGET_HH__
#define __OPENGL_WIDGET_HH__

#include <GL/gl.h>
#include <GL/glu.h>

#include <gtkmm.h>
#include <gtkglmm.h>

#include <iostream>
using namespace std;

//extern // Glib::RecMutex                g_mutex;

class OpenGLWidget : public Gtk::GL::DrawingArea {

protected:

  sigc::signal0<void>                gl_init_function;
  sigc::signal2<void, int, int>      gl_reshape_function;
  sigc::signal2<void, int, int>      gl_draw_function;
  sigc::signal3<bool, int, int, int> gl_button_function;

  bool                          m_realized;
  bool                          m_force_update;
  // timeout signal connection:
  sigc::connection              m_ConnectionTimeout;

  static const int TIMEOUT_INTERVAL = 50;
  
public:

  OpenGLWidget( sigc::slot0 <void>           init_function, 
		sigc::slot2 <void, int, int> reshape_function,		
		sigc::slot2 <void, int, int> draw_function,
		sigc::slot3 <bool, int, int, int> button_function)
    : m_realized(false), m_force_update(false) 
  {
    // Glib::RecMutex::Lock   lock(g_mutex);
    gl_draw_function.connect( draw_function );
    gl_reshape_function.connect( reshape_function );
    gl_init_function.connect( init_function );
    gl_button_function.connect( button_function );

    //
    // Configure OpenGL-capable visual.
    //
  
    Glib::RefPtr<Gdk::GL::Config> glconfig;

    // Try double-buffered visual
    glconfig = Gdk::GL::Config::create(Gdk::GL::MODE_RGB    |
                                       Gdk::GL::MODE_DEPTH  |
                                       Gdk::GL::MODE_DOUBLE);
    if (!glconfig) {
      std::cerr << "*** Cannot find the double-buffered visual.\n"
                << "*** Trying single-buffered visual.\n";

      // Try single-buffered visual
      glconfig = Gdk::GL::Config::create(Gdk::GL::MODE_RGB   |
                                         Gdk::GL::MODE_DEPTH);
      if (!glconfig) {
        std::cerr << "*** Cannot find any OpenGL-capable visual.\n";
        std::exit(1);
      }
    }
   
    //
    // Set OpenGL-capability to the widget.
    //
    
    set_gl_capability(glconfig);
    
    
    //
    // Add events.
    //
    add_events(Gdk::BUTTON1_MOTION_MASK    |
	       Gdk::BUTTON2_MOTION_MASK    |
	       Gdk::BUTTON_PRESS_MASK      |
	       Gdk::VISIBILITY_NOTIFY_MASK);
    
  }

  
public:

  void on_realize()
  {
    // cout << "OpenGL On Realize" << endl;
    // Glib::RecMutex::Lock   lock(g_mutex);
    // We need to call the base on_realize()
    Gtk::DrawingArea::on_realize();
        
    // Get GL::Drawable.    
    Glib::RefPtr<Gdk::GL::Drawable> gldrawable = get_gl_drawable();


    // *** OpenGL BEGIN ***
    if (!gldrawable->gl_begin(get_gl_context()))
      return;

    gl_init_function();

    gldrawable->gl_end();
    // *** OpenGL END ***
    m_realized = true;
    // cout << "OpenGL On Realize Finished" << endl;

    timeout_add();
  }

  bool on_configure_event(GdkEventConfigure* event)
  {    
    // cout << "OpenGL Configure Event" << endl;
    // Glib::RecMutex::Lock   lock(g_mutex);
    
    // Get GL::Drawable.    
    Glib::RefPtr<Gdk::GL::Drawable> gldrawable = get_gl_drawable();

    // GL calls.

    // *** OpenGL BEGIN ***
    if (!gldrawable->gl_begin(get_gl_context())) return false;

    gl_reshape_function( get_width(), get_height() );

    gldrawable->gl_end();
    // *** OpenGL END ***

    // cout << "OpenGL Configure Event Finished" << endl;
    return true;
  }

  bool on_expose_event(GdkEventExpose* event)
  {
    // cout << "OpenGL Expose Event" << endl;
    if( !m_realized) return true;
    // Glib::RecMutex::Lock   lock(g_mutex);
    //    get_parent()->on_expose_event(event);

    // cout << "OpenGL Expose Event Got Lock" << endl;

    // Get GL::Drawable.
    Glib::RefPtr<Gdk::GL::Drawable> gldrawable = get_gl_drawable();

    // cout << "OpenGL Expose Event Got Drawable" << endl;

    // *** OpenGL BEGIN ***
    if (!gldrawable->gl_begin(get_gl_context())) return false;

    // cout << "OpenGL Expose Event After Begin" << endl;

    gl_draw_function( get_width(), get_height() );

    // cout << "OpenGL Expose Event After Draw Function" << endl;

    glFlush();

    // cout << "OpenGL Expose Event After Draw Flush" << endl;

    // Swap buffers.
    if (gldrawable->is_double_buffered())
      gldrawable->swap_buffers();
    else
      glFlush();

    // cout << "OpenGL Expose Event After Swap Buffers" << endl;

    gldrawable->gl_end();
    // *** OpenGL END ***

    // cout << "OpenGL Expose Event Finished" << endl;
    return true;
  }
  /*
    void on_size_request( Gtk::Requisition * req ) 
    {
    static int  offset = 0;
    offset += 10;
    req->width=200;
    req->height=100 + offset;
    }
  */
public:

  void redraw() 
  {
    m_force_update = true;
  }


protected:

  bool OpenGLWidget::on_map_event(GdkEventAny* event)
  {
    timeout_add();

    return true;
  }

  bool OpenGLWidget::on_unmap_event(GdkEventAny* event)
  {
    timeout_remove();

    return true;
  }

  bool OpenGLWidget::on_visibility_notify_event(GdkEventVisibility* event)
  {
    if (event->state == GDK_VISIBILITY_FULLY_OBSCURED)
      timeout_remove();
    else
      timeout_add();
  
    return true;
  }
  bool OpenGLWidget::on_button_press_event(GdkEventButton* event)
  {    
    //    cout << "OpenGLWidget::on_button_press_event" << endl;
    // don't block
    return gl_button_function( (int) event->x, get_height() - (int) event->y, (int) event->button );
  }

  bool OpenGLWidget::on_timeout()
  {
    // cout << "OpenGLWidget::OnTimeout" << endl;
    static int counter=0;

    if( m_force_update || counter++ % 12 == 11) 
      {
	get_window()->invalidate_rect(get_allocation(), false);
	counter = 0;
      }    
    get_window()->process_updates(false);

    m_force_update = false;
    return true;
  }

  void OpenGLWidget::timeout_add()
  {
    if (!m_ConnectionTimeout.connected())
      m_ConnectionTimeout = Glib::signal_timeout().connect(
        sigc::mem_fun(*this, &OpenGLWidget::on_timeout), TIMEOUT_INTERVAL);
  }

  void OpenGLWidget::timeout_remove()
  {
    if (m_ConnectionTimeout.connected())
      m_ConnectionTimeout.disconnect();
  }


};


class FramedOpenGLWidget : public Gtk::Alignment {

private:
  OpenGLWidget      m_OpenGLWidget;

  bool              m_use_fixed_width;
  int               m_fixed_width;

  bool              m_use_fixed_height;
  int               m_fixed_height;

  bool              m_use_fixed_ratio;
  float             m_fixed_ratio;

  int               m_old_width;
  int               m_old_height;

  int               m_force_reset_padding;
  int               m_add_timeout;
public:

  FramedOpenGLWidget( sigc::slot0 <void>           init_function, 
		      sigc::slot2 <void, int, int> reshape_function,		
		      sigc::slot2 <void, int, int> draw_function,
		      sigc::slot3 <bool, int, int, int> button_function) 
    : Gtk::Alignment (),
      m_OpenGLWidget( init_function, reshape_function, draw_function, button_function ),      
      m_use_fixed_width(0), m_use_fixed_height(0), m_use_fixed_ratio(0),
      m_old_width(-1), m_old_height(-1), m_force_reset_padding(true),
      m_add_timeout(true)
  {
    add( m_OpenGLWidget );
    add_events(Gdk::EXPOSURE_MASK);
  }

  void reset_padding() {

    int fixed_height = -1, fixed_width= -1;
    
    if( m_use_fixed_width || m_use_fixed_height ) 
      {
	if( m_use_fixed_width  && m_fixed_width >= 0 ) 
	  fixed_width = m_fixed_width;
	if( m_use_fixed_height && m_fixed_height >= 0 )
	  fixed_height = m_fixed_height;
      } 
    else if( m_use_fixed_ratio ) 
      {
	if( m_fixed_ratio >= 0.0 ) 
	  {
	    
	    int pix_req_height = (int) ( (float) get_width () / m_fixed_ratio );
	    if( pix_req_height > get_height() ) 
	      {
		// size limited by height
		fixed_height = get_height();
		fixed_width  = (int) ((float) fixed_height * m_fixed_ratio );
	      } 
	    else 
	      {
		// size limited by width
		fixed_width  = get_width();
		fixed_height = (int) ((float) fixed_width / m_fixed_ratio );
	      }
	  }	
      }
    
    int padding_top=0, padding_bottom=0, padding_left=0, padding_right=0;
    if( fixed_width >= 0 ) 
      {
	int padded_pixels = get_width() - fixed_width;
	padding_left = padded_pixels / 2;
	padding_right = padded_pixels - padding_left;
      }
    if( fixed_height >= 0 )
      {
	int padded_pixels = get_height() - fixed_height;
	padding_top = padded_pixels / 2;
	padding_bottom = padded_pixels - padding_top;
      }
    
    set_padding(padding_top, padding_bottom, padding_left, padding_right);
     
    m_old_width  = get_width();
    m_old_height = get_height();

    m_force_reset_padding = false;
    // cout << "Padding Reset " << endl;
  }
  
  void set_fixed_ratio(float asp) 
  {
    m_fixed_ratio      = asp;
    m_use_fixed_ratio  = true;
    m_use_fixed_height = false;
    m_use_fixed_width  = false;
    m_force_reset_padding = true;
    
  }

  void set_fixed_width(int width)
  {
    m_fixed_width = width;
    m_use_fixed_width  = true;
    m_use_fixed_ratio = false;
    m_force_reset_padding = true;
  }

  void set_fixed_height(int height)
  {
    m_fixed_height      = height;
    m_use_fixed_height  = true;
    m_use_fixed_ratio  = false;
    m_force_reset_padding = true;
  }

  void unset_fixed_ratio()
  {
    m_use_fixed_ratio = false;
    m_force_reset_padding = true;
  }
  
  void unset_fixed_width()
  {
    m_use_fixed_width = false;
    m_force_reset_padding = true;
  }
  
  void unset_fixed_height()
  {
    m_use_fixed_height = false;
    m_force_reset_padding = true;
  }


public:

  void redraw() 
  {
    queue_draw_area( 0, 0, get_width(), get_height() );
  }

  void redraw_immediately() 
  {
    m_OpenGLWidget.redraw();
  }

    
protected:
  sigc::connection              m_ConnectionTimeout;
  static const int TIMEOUT_INTERVAL = 200;

  bool on_timeout()
  {
    // cout << "OnTimeout" << endl;
    
    if( m_old_width != get_width() || m_old_height != get_height() ) 
      reset_padding();

    return true;
  }

  void timeout_add()
  {
    if (!m_ConnectionTimeout.connected())
      m_ConnectionTimeout = Glib::signal_timeout().connect(
        sigc::mem_fun(*this, &FramedOpenGLWidget::on_timeout), TIMEOUT_INTERVAL);
  }

  void timeout_remove()
  {
    if (m_ConnectionTimeout.connected())
      m_ConnectionTimeout.disconnect();
  }

  bool on_expose_event(GdkEventExpose* event)
  {
    if( m_add_timeout ) 
      {
	timeout_add();
	m_add_timeout = false;
      }
    return true;
  }
  
};



#endif







