#!/bin/sh

# This script will add users to the current machine with the same UIDs as they
# are on grandchallenge.  
#
# NOTE: There is a better way of synchronizing the logins to grandchallenge, since
# 1) this list is out-of-date, and 2) this script doesn't enable the login for new
# users - you have to run passwd to do this.  The better way is to synchronize the 
# /etc/passwd and /etc/shadow files, and then make sure home directories exist.  
# See http://gc.caltech.edu/bugzilla/show_bug.cgi?id=1920

for pair in "klk 531" "jeremy 508" "jleibs 1053" "soudek 1052" "cady 1057" "duncklee 1022" "ryanf 1054" "dkogan 1021" "lindzey 1056" "cer 1013" "lars 521" "pickett 1059" "astewart 1048" "jason 1026" "ryanc 1063" "craig 1012" "joseph 1051" "sueh 504" "haomiao 517" "ben 519" "hbarnor 1020" "ezra 1047" "thiago 1062" "foote 1029" "edhsiao 1044" "jlamb 1011" "jmalmaud 1040" "drosen 1060" "somers 510" "cosselk 1027" "jeanty 1042" "murray 1000"
do

set -- $pair  # Parses variable "pair" and sets positional parameters.
              # the "--" prevents nasty surprises if $pair is null or begins with a dash.

# grep for the user
grep $1 /etc/passwd > /dev/null 2>&1
# capture the return value and act accordingly
if [ "$?" -eq 0 ]; then
  echo "user $1 exists"
  usermod -G wheel $1
else
  # add the user if it doesn't exist
  echo "user $1 does not exist.  creating user"
  useradd -m -d /home/$1 -u $2 -G wheel $1
  usermod -G wheel $1
fi

done


