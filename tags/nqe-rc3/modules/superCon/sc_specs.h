#ifndef SC_SPECS_H
#define SC_SPECS_H

#include <math.h>
#include "AliceConstants.h"

#warning superCon *must* use the outside RDDF value for a superCon obstacle set in cMap config

/** this should contain "_(type, name, init value)\" **/
#define SPECLIST(_) \
  /*** TOP-LEVEL SUPERCON DEFINES ***/ \
  _( double, SUPERCON_FREQ, 10.0 ) \
  /* (fewest log messages) --> LOG_ERROR = 0, LOG_WARNING = 1, LOG_INFORM = 2, LOG_ANNOY = 3 --> (most log messages) */ \
  _( int, SPARROW_LOG_LEVEL, 3 ) \
  /* Units = Meters/Second */ \
  _( double, TERRAIN_OBSTACLE_SPEED, 1.0 ) \
  /* Units in meters/second */ \
  _( double, OUTSIDE_RDDF_SPEED_UPPER_BOUND, 0.10) \
  _( double, OUTSIDE_RDDF_SPEED_LOWER_BOUND, 0.04) \ 
  /* cost-value assigned to a cell in the superCon map layer to denote a superCon \
   * obstacle */ \
  _( double, SUPERCON_OBSTACLE_SPEED_MS, 0.08 )  \
  \
  \
  /*** DEFINES USED BY DIAGNOSTIC RULES ***/ \
  /* This is the slowest speed in M/S that the TrajFollower can maintain, speeds \
   * below this are assumed to be zero in some form  */ \
  /* Need these values to be > 1.0 (as this is the value used by trajFollower) */ \
  _( double, SLOWEST_MAINTAINABLE_ALICE_SPEED, 1.0 ) \
  _( double, SLOWEST_MAINTAINABLE_WHEEL_SPEED, 0.1 ) \
  /* Throttle position which must be met, or exceeded before it will be assumed \
   * that an unseen obstacle is in front of Alice  */ \
  _( double, THROTTLE_THRESHOLD_FOR_UNSEEN_OBSTACLE, 0.5 ) \
  \
  \
  /** REPETITION DEFINES USED IN TRANS_/TERM_ RULES **/ \
  _( int, REPEAT_TRUE_COUNT_NOMINALOPERATION, 10 ) \
  _( int, REPEAT_TRUE_COUNT_GPSREACQ, 1 ) \
  _( int, REPEAT_TRUE_COUNT_LTURNREVERSE, 20 ) \
  _( int, REPEAT_TRUE_COUNT_UNSEENOBSTACLE, 10 ) \
  _( int, REPEAT_TRUE_COUNT_SLOWADVANCE, 3 ) \
  _( int, REPEAT_TRUE_COUNT_ESTOP_PAUSED_NOT_SUPERCON, 3 ) \
  _( int, REPEAT_TRUE_COUNT_PLN_NOT_NFP, 10 ) \
  _( int, REPEAT_TRUE_COUNT_PLNFAILEDNOTRAJ, 10 ) \
  _( int, REPEAT_TRUE_COUNT_END_OF_RDDF, 10 ) \
  \
  \
  /** REPETITION DEFINES USED IN NON-TRANS/TERM RULES (which consider true counts) **/ \
  _( int, REPEAT_TRUE_COUNT_ALLOW_GEARCHANGE, 30 ) \
  \
  \
  /*** DEFINES USED IN STRATEGY CLASSES  ***/ \
  \
  /** Generic Strategy class **/ \
  /* Timeout loop thresholds are measured in SUPERCON EXECUTION CYCLES and are used during action 'time-out' checking */ \
  /* THESE ARE HACK VALUES - NEED PROPER ONES! */ \
  _( int, TIMEOUT_LOOP_ESTOP_THRESHOLD, 100 ) \
  _( int, TIMEOUT_LOOP_ASTATE_RESPONSE_THRESHOLD, 300 ) \
  _( int, TIMEOUT_LOOP_GEARCHANGE, 100 ) \
  _( int, TIMEOUT_LOOP_TRAJFMODECHANGE, 100 ) \
  _( int, TIMEOUT_LOOP_TRAJF_REVERSE_FINISHED, 1200 ) \
  _( int, TIMEOUT_LOOP_SLOWADVANCE_APPROACH, 600 ) \
  _( int, TIMEOUT_LOOP_UNSEENOBST_WAIT_FOR_STATIONARY, 50 ) \
  _( int, TIMEOUT_LOOP_WAIT_FOR_ALICE_STATIONARY, 100 ) \
  _( int, TIMEOUT_LOOP_WAIT_FOR_MAPDELTASAPPLIED, 100 ) \
  _( int, TIMEOUT_LOOP_WAIT_FOR_NEW_PLAN, 300 ) \
  _( int, TIMEOUT_LOOP_WAIT_FOR_MAPCLEAR, 450 ) \
  _( int, TIMEOUT_LOOP_WAIT_FOR_DARPA_PAUSE_ENDOFRDDF, 6000 ) \
  \
  /** GPSreAcq Strategy **/ \
  \
  /** LoneRanger Strategy **/ \
  /* Speed at which to attempt to drive through obstacles [METERS/SECOND] */ \
  _( double, LONE_RANGER_MIN_SPEED_MS, 2.0 ) \
  /* persistance count threshold for the number of consecutive plans made \
   * whilst in LoneRanger strategy which pass through superCon obstacles
   * (condition for transition to  */ \
  \
  /** LturnReverse Strategy **/ \
  /* Distance to reverse (along the path we came in on) for an L-turn */ \
  _( double, LTURN_REVERSE_DISTANCE_METERS, 15.0 ) \
  \
  /** Nominal Strategy **/ \
  \
  /** SlowAdvance Strategy **/ \
  /* Speed [meters/second] below which the maps can reasonably be considered \
   * to be 'high-quality' as sensor data gathered at slower speeds will be more \
   * heavily weighted than sensor data gathered at higher speeds, and reduced \
   * high frequency oscillations etc */ \
  _(double, HIGH_ACCURACY_MAPS_SPEED_MS, 7.0 ) \
  \
  /** UnseenObstacle Strategy **/ \
  /* This is the offset in METERS where the superCon obstacle will be painted \
   * (TOWARDS THE FRONT AXLE = +VE) from the center of the rear axle (assumes that \
   * vehicle origin is along the center-line (line joining the centers of both axles) \
   * of the vehicle) of the actual vehicle origin. */ \
  _( double, OFFSET_OF_VEHICLE_ORIGIN_FROM_REAR_AXLE, 2.75 ) \
  /* Width of the superCon obstacle to be painted into the map, centered on the
   * center-line of Alice */ \
  _( double, SUPERCON_OBSTACLE_WIDTH, VEHICLE_WIDTH ) \
  /* Number of points along the bumper for which map-delta updates are sent when an \
   * unseen-obstacle has been detected (should be even) */ \
  _( int, NUM_POINTS_TO_EVALUATE_ALONG_OBSTACLE, 42 ) \
  \
  /** NOTsuperConEstopPause Strategy **/ \
  \
  /** Multiple Strategies **/

#define EXTERNDEFINESPEC(type, name, val) \
extern type name;

#define DEFINESPEC(type, name, val) \
type name = val;

#define EXTERNDEFINESPECS namespace specs { SPECLIST(EXTERNDEFINESPEC) }; using namespace specs
#define DEFINESPECS namespace specs { SPECLIST(DEFINESPEC)	}; using namespace specs


EXTERNDEFINESPECS;


extern void readspecs(void);

#endif //SC_SPECS_H
