#ifndef SUPERCONCLIENT_HH
#define SUPERCONCLIENT_HH

#include<pthread.h>

#include"SkynetContainer.h"
#include"SuperConInterface.hh"
#include"DGCutils"

/*! CSuperConClient class. Class to be inherited in all modules in contact with SuperCon.
 * It defines basic functions for throwing warnings, errors and assertions to SuperCon,
 * with the ability to supply any number of user specified variables (currently limited to
 * 10 variables, but easily extendable, contact gustav).
 * \brief Class to be inherited from to access SuperCon throw functionality
 * $Id$
 */

//SuperCon client interface
class CSuperConClient : virtual public CSkynetContainer
{
public:
  /*! CSuperConClient Constructor
   * \param module_name Name of the module using initiating the SuperCon interface, this should be a 3 letter abrivation
   */
  CSuperConClient(string module_name);
  /*! SuperConClient Desctructor */
  ~CSuperConClient();

private:
  /*! Sends a message to SuperCon module. This function is never used directly, but instead the defines scThrow(...) are used
   * \param rev Current subversion revision - not currently implemented
   * \param file Filename where the error was thrown
   * \param line Line # where error was thrown
   * \param type The type of the message - message, warning, error or assert
   * \param msgID The module unique identifier of the throw (so SuperCon can identify more exactly what happened)
   *        or one of the standard errors such as memory alloc error.
   * \param data a supercon_throw_cmd with user data portion already filled in
   */
  void scDoTheMessage(int rev, const char *file, int line, enum sc_msg_type type, int msgID, supercon_msg_cmd &cmd); ///Main throw function

private:
  /*! The name of the current module */
  string m_module_name;
  /*! The skynet send socket */
  int m_send_socket;
  int m_listen_socket;
  /*! Mutex making sure only one SuperCon throw is done at the time */
  pthread_mutex_t m_sc_mutex;
  /*! The one and only supercon_throw_cmd instans, whenever used, the mutex should be locked */
  supercon_msg_cmd m_cmd;

public:  //All code below this point are functions used to handle multiple arbritrary user supplied arguments
  //Templated versions of throw that handle and arbritrary number (10) of user supplied arguments
  void scDoMessage(int rev, const char *file, int line, sc_msg_type type, int msgID) {
    DGClockMutex(&m_sc_mutex);
    m_cmd.user_bytes=0;
    scDoTheMessage(rev, file, line, type, msgID, m_cmd);
    DGCunlockMutex(&m_sc_mutex);
  }

  template<class T1>
  void scDoMessage(int rev, const char *file, int line, sc_msg_type type, int msgID, T1 t1) {
    DGClockMutex(&m_sc_mutex);
    m_cmd.user_bytes=0;
    sc_serialize(&m_cmd,t1);
    scDoTheMessage(rev, file, line, type, msgID, m_cmd);
    DGCunlockMutex(&m_sc_mutex);
  }

  template<class T1, class T2>
  void scDoMessage(int rev, const char *file, int line, sc_msg_type type, int msgID, T1 t1, T2 t2) {
    DGClockMutex(&m_sc_mutex);
    m_cmd.user_bytes=0;
    sc_serialize(&m_cmd, t1);
    sc_serialize(&m_cmd, t2);
    scDoTheMessage(rev, file, line, type, msgID, m_cmd);
    DGCunlockMutex(&m_sc_mutex);
  }
  template<class T1, class T2, class T3>
  void scDoMessage(int rev, const char *file, int line, sc_msg_type type, int msgID, T1 t1, T2 t2, T3 t3) {
    DGClockMutex(&m_sc_mutex);
    m_cmd.user_bytes=0;
    sc_serialize(&m_cmd, t1);
    sc_serialize(&m_cmd, t2);
    sc_serialize(&m_cmd, t3);
    scDoTheMessage(rev, file, line, type, msgID, m_cmd);
    DGCunlockMutex(&m_sc_mutex);
  }
  template<class T1, class T2, class T3, class T4>
  void scDoMessage(int rev, const char *file, int line, sc_msg_type type, int msgID, T1 t1, T2 t2, T3 t3, T4 t4) {
    DGClockMutex(&m_sc_mutex);
    m_cmd.user_bytes=0;
    sc_serialize(&m_cmd, t1);
    sc_serialize(&m_cmd, t2);
    sc_serialize(&m_cmd, t3);
    sc_serialize(&m_cmd, t4);
    scDoTheMessage(rev, file, line, type, msgID, m_cmd);
    DGCunlockMutex(&m_sc_mutex);
  }
  template<class T1, class T2, class T3, class T4, class T5>
  void scDoMessage(int rev, const char *file, int line, sc_msg_type type, int msgID, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5) {
    DGClockMutex(&m_sc_mutex);
    m_cmd.user_bytes=0;
    sc_serialize(&m_cmd, t1);
    sc_serialize(&m_cmd, t2);
    sc_serialize(&m_cmd, t3);
    sc_serialize(&m_cmd, t4);
    sc_serialize(&m_cmd, t5);
    scDoTheMessage(rev, file, line, type, msgID, m_cmd);
    DGCunlockMutex(&m_sc_mutex);
  }
  template<class T1, class T2, class T3, class T4, class T5, class T6>
  void scDoMessage(int rev, const char *file, int line, sc_msg_type type, int msgID, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6) {
    DGClockMutex(&m_sc_mutex);
    m_cmd.user_bytes=0;
    sc_serialize(&m_cmd, t1);
    sc_serialize(&m_cmd, t2);
    sc_serialize(&m_cmd, t3);
    sc_serialize(&m_cmd, t4);
    sc_serialize(&m_cmd, t5);
    sc_serialize(&m_cmd, t6);
    scDoTheMessage(rev, file, line, type, msgID, m_cmd);
    DGCunlockMutex(&m_sc_mutex);
  }
  template<class T1, class T2, class T3, class T4, class T5, class T6, class T7>
  void scDoMessage(int rev, const char *file, int line, sc_msg_type type, int msgID, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6, T7 t7) {
    DGClockMutex(&m_sc_mutex);
    m_cmd.user_bytes=0;
    sc_serialize(&m_cmd, t1);
    sc_serialize(&m_cmd, t2);
    sc_serialize(&m_cmd, t3);
    sc_serialize(&m_cmd, t4);
    sc_serialize(&m_cmd, t5);
    sc_serialize(&m_cmd, t6);
    sc_serialize(&m_cmd, t7);
    scDoTheMessage(rev, file, line, type, msgID, m_cmd);
    DGCunlockMutex(&m_sc_mutex);
  }
  template<class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8>
  void scDoMessage(int rev, const char *file, int line, sc_msg_type type, int msgID, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6, T7 t7, T8 t8) {
    DGClockMutex(&m_sc_mutex);
    m_cmd.user_bytes=0;
    sc_serialize(&m_cmd, t1);
    sc_serialize(&m_cmd, t2);
    sc_serialize(&m_cmd, t3);
    sc_serialize(&m_cmd, t4);
    sc_serialize(&m_cmd, t5);
    sc_serialize(&m_cmd, t6);
    sc_serialize(&m_cmd, t7);
    sc_serialize(&m_cmd, t8);
    scDoTheMessage(rev, file, line, type, msgID, m_cmd);
    DGCunlockMutex(&m_sc_mutex);
  }
  template<class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9>
  void scDoMessage(int rev, const char *file, int line, sc_msg_type type, int msgID, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6, T7 t7, T8 t8, T9 t9) {
    DGClockMutex(&m_sc_mutex);
    m_cmd.user_bytes=0;
    sc_serialize(&m_cmd, t1);
    sc_serialize(&m_cmd, t2);
    sc_serialize(&m_cmd, t3);
    sc_serialize(&m_cmd, t4);
    sc_serialize(&m_cmd, t5);
    sc_serialize(&m_cmd, t6);
    sc_serialize(&m_cmd, t7);
    sc_serialize(&m_cmd, t8);
    sc_serialize(&m_cmd, t9);
    scDoTheMessage(rev, file, line, type, msgID, m_cmd);
    DGCunlockMutex(&m_sc_mutex);
  }
  template<class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9, class T10>
  void scDoMessage(int rev, const char *file, int line, sc_msg_type type, int msgID, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6, T7 t7, T8 t8, T9 t9, T10 t10) {
    DGClockMutex(&m_sc_mutex);
    m_cmd.user_bytes=0;
    sc_serialize(&m_cmd, t1);
    sc_serialize(&m_cmd, t2);
    sc_serialize(&m_cmd, t3);
    sc_serialize(&m_cmd, t4);
    sc_serialize(&m_cmd, t5);
    sc_serialize(&m_cmd, t6);
    sc_serialize(&m_cmd, t7);
    sc_serialize(&m_cmd, t8);
    sc_serialize(&m_cmd, t9);
    sc_serialize(&m_cmd, t10);
    scDoTheMessage(rev, file, line, type, msgID, m_cmd);
    DGCunlockMutex(&m_sc_mutex);
  }
};

//Define used to call throw in SuperConClient
#ifndef _REV_
#define _REV_ 0
#endif

#define scMessage(msgID, ...) scDoMessage(_REV_, __FILE__, __LINE__, scMessage, msgID, ##__VA_ARGS__)
#define scThrow(type, msgID, ...) scDoMessage(_REV_, __FILE__, __LINE__, type, msgID, ##__VA_ARGS__)
#define scCThrow(condition, type, msgID, ...) { if(condition) scDoMessage(_REV_, __FILE__, __LINE__, type, msgID, ##__VA_ARGS__); }

#endif //SUPERCONCLIENT_HH
