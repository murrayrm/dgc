//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_SunPosition.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// from http://www.squ1.com/solar/solar-position.html

int monthday[12] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 };

//----------------------------------------------------------------------------
 
// date, time supplied (log file version)
// assuming pacific time zone, year = 2005

void sunpos(double northing, double easting, int utmzone, int month, int day, int hours, int minutes, float *sunalt, float *sunazi)   
{
  float gmt_offset;

  if (month < 4 || (month == 4 && day < 3))
    gmt_offset = -8.0;
  else
    gmt_offset = -7.0;

  s1_compute(northing, easting, utmzone, gmt_offset, month, day, (float) hours, (float) minutes, sunalt, sunazi);
}

//----------------------------------------------------------------------------

// get date, time from system time now (skynet version)

void sunpos(double northing, double easting, int utmzone, float *sunalt, float *sunazi)   
{
  
  // get calendar day, time

  /*
struct tm {
  int     tm_sec;         // seconds -- The  number of seconds after the minute, normally in the range 0 
                          // to 59, but can be up to 61 to allow for leap seconds.

  int     tm_min;         // minutes -- The number of minutes after the hour, in the range 0 to 59
  int     tm_hour;        // hours -- The number of hours past midnight, in the range 0 to 23.
  int     tm_mday;        // day of the month -- The day of the month, in the range 1 to 31.
  int     tm_mon;         // month -- The number of months since January, in the range 0 to 11.
  int     tm_year;        // year -- The number of years since 1900.
  int     tm_wday;        // day of the week -- The number of days since Sunday, in the range 0 to 6.
  int     tm_yday;        // day in the year -- The number of days since January 1, in the range 0 to 365.
  int     tm_isdst;       // daylight saving time -- A flag that indicates whether daylight saving time is in  effect
                          // at the time described.  The value is positive if daylight saving
                          // time is in effect, zero if it is not, and negative if the information is not available.
			  */

  time_t ltime;
  struct tm *today;
  
  time(&ltime);
  today = localtime(&ltime);

  sunpos(northing, easting, utmzone, 1+today->tm_mon, today->tm_mday, today->tm_hour, today->tm_min, sunalt, sunazi);
}

//----------------------------------------------------------------------------

void s1_compute(double northing, double easting, int utmzone, float gmt_offset, int month, int day, float hours, float minutes, float *sunalt, float *sunazi) 
{
  // Loc radians.
  float	fLatitude;
  float	fLongitude;
  float	fTimeZone;
  
  // Calculated data.
  float	fDifference;
  float	fDeclination;
  float	fEquation;
  
  // Solar information.
  float	fLocalTime;
  float	fSolarTime;
  float	fAltitude;
  float	fAzimuth;
  float	fSunrise;
  float	fSunset;
  
  // Integer values.
  float iJulianDate;

  // Temp data.
  float t, m, test;
  //hour, minute, 

  ////////////////////////////////////////////////////////////
  // READ INPUT DATA 
  ////////////////////////////////////////////////////////////

  double latitude, longitude;
  //  float gmt_offset;
//   int month, day;
//   float hours, minutes;

  // UD Friday morning

  /*
  latitude = 39.6823;
  longitude = -75.7485;
  gmt_offset = -4;   
  month = 9;
  day = 2;
  hours = 9; // 0-23
  minutes = 30;
  */

  // Stoddard Wells into the sun

  /*
  latitude = 34.688528;
  longitude = -117.095157;
  gmt_offset = -7;  // should be -8 if before april 3
  month = 8;
  day = 27;
  hours = 18;  // 17
  minutes = 11;  // 19
  */

  UTMtoLL(northing, easting, utmzone, &latitude, &longitude);

  // Get location data.
  fLatitude = DEG2RAD(latitude);
  fTimeZone = gmt_offset * 15 * ( PI/180.0);   
  fLongitude = DEG2RAD(longitude);
  
  // Get julian date.
  iJulianDate = (float) (monthday[month-1] + day);
  
  // Get local time value.
  fLocalTime = hours + minutes / 60.0;
  
  // Check for military time (2400).
  if (fLocalTime > 100) {
    fLocalTime /= 100.0;
    t =  floor(fLocalTime);
    m =  round((fLocalTime - t) * 100.0);
    fLocalTime = t + (m / 60.0);
  }
  
  
  ////////////////////////////////////////////////////////////
  // CALCULATE SOLAR VALUES
  ////////////////////////////////////////////////////////////
  
  // Preliminary check.
  if (iJulianDate > 365) iJulianDate -= 365;
  if (iJulianDate < 0) iJulianDate += 365;
  
  // Secondary check of julian date.
  if (iJulianDate > 365) iJulianDate = 365;
  if (iJulianDate < 1) iJulianDate = 1;
  
  // Calculate solar declination as per Carruthers et al.
  t = 2 *  PI * ((iJulianDate - 1) / 365.0);
  
  fDeclination = (0.322003
		  - 22.971 *  cos(t)
		  - 0.357898 *  cos(2*t)
		  - 0.14398 *  cos(3*t)
		  + 3.94638 *  sin(t)
		  + 0.019334 *  sin(2*t)
		  + 0.05928 *  sin(3*t)
		  );
  
  // Convert degrees to radians.
  if (fDeclination > 89.9) fDeclination = 89.9;
  if (fDeclination < -89.9) fDeclination = -89.9;
  
  // Convert to radians.
  fDeclination = fDeclination * ( PI/180.0);
  
  // Calculate the equation of time as per Carruthers et al.
  t = (279.134 + 0.985647 * iJulianDate) * ( PI/180.0);
  
  fEquation = (5.0323
	       - 100.976 *  sin(t)
	       + 595.275 *  sin(2*t)
	       + 3.6858 *  sin(3*t)
	       - 12.47 *  sin(4*t)
	       - 430.847 *  cos(t)
	       + 12.5024 *  cos(2*t)
	       + 18.25 *  cos(3*t)
	       );
  
  // Convert seconds to hours.
  fEquation = fEquation / 3600.00;
  
  // Calculate difference (in minutes) from reference longitude.
  fDifference = (((fLongitude - fTimeZone) * 180/ PI) * 4) / 60.0;
  
  // Convert solar noon to local noon.
  float local_noon = 12.0 - fEquation - fDifference;
  
  // Calculate angle normal to meridian plane.
  if (fLatitude > (0.99 * ( PI/2.0))) fLatitude = (0.99 * ( PI/2.0));
  if (fLatitude < -(0.99 * ( PI/2.0))) fLatitude = -(0.99 * ( PI/2.0));
  
  test = - tan(fLatitude) *  tan(fDeclination);
  
  if (test < -1) t =  acos(-1.0) / (15 * ( PI / 180.0));
  // else if (test > 1) t = acos(1.0) / (15 * ( PI / 180.0)); ### Correction - missing ' acos'
  else if (test > 1) t =  acos(1.0) / (15 * ( PI / 180.0));
  else t =  acos(- tan(fLatitude) *  tan(fDeclination)) / (15 * ( PI / 180.0));
  
  // Sunrise and sunset.
  fSunrise = local_noon - t;
  fSunset  = local_noon + t;
  
  // Check validity of local time.
  if (fLocalTime > fSunset) fLocalTime = fSunset;
  if (fLocalTime < fSunrise) fLocalTime = fSunrise;
  if (fLocalTime > 24.0) fLocalTime = 24.0;
  if (fLocalTime < 0.0) fLocalTime = 0.0;
  
  // Caculate solar time.
  fSolarTime = fLocalTime + fEquation + fDifference;
  
  // Calculate hour angle.
  float fHourAngle = (15 * (fSolarTime - 12)) * ( PI/180.0);
  
  // Calculate current altitude.
  t = ( sin(fDeclination) *  sin(fLatitude)) + ( cos(fDeclination) *  cos(fLatitude) *  cos(fHourAngle));
  fAltitude =  asin(t);
  
  // Original calculation of current azimuth - LEAVE COMMENTED AS DOES NOT WORK CORRECTLY.
  // t = ( cos(fLatitude) *  sin(fDeclination)) - ( cos(fDeclination) *  sin(fLatitude) *  cos(fHourAngle));
  // fAzimuth =  acos(t /  cos(fAltitude));
  
  // FIX:
  // ##########################################
  // Need to do discrete quadrant checking.
  
  // Calculate current azimuth.
  t = ( sin(fDeclination) *  cos(fLatitude))
    - ( cos(fDeclination) *  sin(fLatitude)
       *   cos(fHourAngle));
  
  float sin1, cos2;

  // Avoid division by zero error.
  if (fAltitude < ( PI/2.0)) { 
    sin1 = (- cos(fDeclination) *  sin(fHourAngle)) /  cos(fAltitude);
    cos2 = t /  cos(fAltitude);
  }
  
  else {
    sin1 = 0.0;
    cos2 = 0.0;
  }
  
  // Some range checking.
  if (sin1 > 1.0) sin1 = 1.0;
  if (sin1 < -1.0) sin1 = -1.0;
  if (cos2 < -1.0) cos2 = -1.0;
  if (cos2 > 1.0) cos2 = 1.0;
  
  // Calculate azimuth subject to quadrant.
  if (sin1 < -0.99999) fAzimuth =  asin(sin1);
  
  else if ((sin1 > 0.0) && (cos2 < 0.0)) {
    if (sin1 >= 1.0) fAzimuth = -( PI/2.0);
    else fAzimuth = ( PI/2.0) + (( PI/2.0) -  asin(sin1));
  }
  
  else if ((sin1 < 0.0) && (cos2 < 0.0)) {
    if (sin1 <= -1.0) fAzimuth = ( PI/2.0);
    else fAzimuth = -( PI/2.0) - (( PI/2.0) +  asin(sin1));
  }
  
  else fAzimuth =  asin(sin1);
  
  // A little last-ditch range check.
  if ((fAzimuth < 0.0) && (fLocalTime < 10.0)) {
    fAzimuth = -fAzimuth;
  }
  
  ////////////////////////////////////////////////////////////
  // FORMAT OUTPUT DATA
  ////////////////////////////////////////////////////////////

  *sunalt = fAltitude;
  *sunazi = fAzimuth;

  //  printf("alt = %f, az = %f\n", RAD2DEG(fAltitude), RAD2DEG(fAzimuth));

  /*    
  // Output altitude value.
  t =  round(fAltitude * (180.0/ PI) * 100) / 100.0;
  document.theForm.outputAltitude.value = t;
  
  // Output aziumth value.
  t =  round(fAzimuth * (180.0/ PI) * 100) / 100.0;
  document.theForm.outputAzimuth.value = t;
  
  return;	
  */

}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//  Adapted from AA.ARC v5.5
//
//  This program computes the orbital positions of planetary
//  bodies and performs rigorous coordinate reductions to apparent
//  geocentric and topocentric place (local altitude and azimuth).
//
//  Copyright 1984, 1987 by Stephen L. Moshier
//  www.moshier.net
//----------------------------------------------------------------------------
//  The functions below are from AA200.C, KFILES.C, sun.c, and various 
//  utility function .c files (deltat.c, dms.c, zatan2.c, lonlat.c, 
//  precess.c, epsiln.c, kepj.c, vearth.c, nutate.c, altaz.c, sidrlt.c, 
//  diurab.c, trnsit.c, diurpx.c, refrac.c, kepjpl.c, and tdb.c) that 
//  support computation of sun position.  there are some non-algorithmic 
//  edits because aa is not being used as a standalone program
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

char defile[80] = {0}; /* DE file */

/* Meeus purturbations are invoked for earth if POLYN = 0.
 * If POLYN = 1, the more accurate B&S or other polynomial expansion
 * is computed.
 * Fixed numerical values will be used if read in from a file
 * named earth.orb.
 * See kfiles.c, kep.h, oearth.c, and pearth.c.
 */
#if !POLYN
int oearth();
#endif
#if !DEPOLYN
int cearth();
#endif

struct orbit earth = {
"Earth          ",
2446800.5,
0.0,
0.0,
102.884,
0.999999,
0.985611,
0.016713,
1.1791,
2446800.5,
-3.86,
0.0,
#if POLYN
0,
#else
oearth,
#endif
#if DEPOLYN
0,
#else
cearth,
#endif
0.0,
0.0,
0.0
};
 

double jvearth = -1.0;
double vearth[3] = {0.0};
double pearthb[3] = {0.0};

/* Conversion factors between degrees and radians */
double DTR = 1.7453292519943295769e-2;
double RTD = 5.7295779513082320877e1;
double RTS = 2.0626480624709635516e5; /* arc seconds per radian */
double STR = 4.8481368110953599359e-6; /* radians per arc second */
//double PI = 3.14159265358979323846;
//extern double PI;

/* Standard epochs.  Note Julian epochs (J) are measured in
 * years of 365.25 days.
 */
double J2000 = 2451545.0;	/* 2000 January 1.5 */
double B1950 = 2433282.42345905; /* 1950 January 0.923 Besselian epoch */
double J1900 = 2415020.0;	/* 1900 January 0, 12h UT */

/*  These values are recalculated in kfiles.c from the astronomical
   constants in deXXX.h  */
/* Speed of light, in astronomical units per day. */
double Clightaud = 173.144632720536344565;
/* Speed of light, 299792.458 meters per second */
double Clight; 
/* Equatorial radius of Earth, in au. */
double Rearthau =  4.26352325064817808471e-5;

/* Read initialization file aa.ini
 * and adjust topocentric coordinates of observer.
 *
 * The following items will be read in automatically from the disc file
 * named aa.ini, if one is provided.  The file contains one ASCII
 * string number per line so is easily edited.
 *
 * Terrestrial geocentric latitude and longitude of observer
 * in degrees East of Greenwich, North of equator
 * (kfiles.c converts from geodetic latitude input.)
 */

double tlong = -71.13;	/* Cambridge, Massachusetts */
double tlat = 42.38; /* geocentric latitude of observer, degrees: */
double glat = 42.17; /* geodetic */
/* Parameters for calculation of azimuth and elevation
 */
double attemp = 20.0;	/* atmospheric temperature, degrees Centigrade */
double atpress = 1013.0; /* atmospheric pressure, millibars */

/* Distance from observer to center of earth, in earth radii */
double trho = 0.9985;
static double flat = 298.257222;

/* This is not necessarily the same value used in ephemeris calculation. */
double aearth = 6378137.;

static double height = 0.0;

/* heliocentric velocity of given object (geocentric, if moon) */
double pobjb[3];
double pobjh[3];
double vobjb[3];
double vobjh[3];
double psunb[3];
double vsunb[3];

/* Barycentric position of the earth */
//double pearthb[3];

// double JDb = 625360.5; /* starting JED -3000 Feb 23, for de406.unx */
// double JDe = 2816848.5; /* ending JED 3000 March 3 */
// /* Willmann-Bell CD ROM 
//   starts  3001 B.C. Feb 24 = 625360.5
//   ends at 3000 July 8 = 2816976.5      */
// double JDi = 64.0; /* days per record */

/* 728 values per record times size of floating point format (8 bytes). */
// #define DRSIZ 5824
// long recsiz = (long )DRSIZ; /* bytes per record */
// 
// long rec0 = 11648L; /* offset to first record */
/* long rec0 = 0L; */  /* Use 0 for excerpted segments of the file.  */

// int objs[13][3] = {
// {  3, 14, 4},
// {171, 12, 1},
// {207, 9, 2},
// {261, 10, 1},
// {291, 6, 1},
// {309, 6, 1},
// {327, 6, 1},
// {345, 6, 1},
// {363, 6, 1},
// {381, 13, 8},
// {693, 12, 1},
// {729, 0, 0},  /* No nutations */
// {729, 0, 0},  /* No librations */
// };

// double au = 1.49597870691e8; /* Kilometers per au */
// double emrat = 81.30056; /* Earth/Moon mass ratio */
// double radearth = 6378.137;
// double clight = 2.99792458e5;
// char *dename = "de406.unx";
/*  Starting dates of posted de406 subfiles */
// double de406dates[] = {
//   625360.5,
//   734864.5,
//   844432.5,
//   954000.5,
//   1063568.5,
//   1173136.5,
//   1282704.5,
//   1392272.5,
//   1501904.5,
//   1611472.5,
//   1721040.5,
//   1830608.5,
//   1940176.5,
//   2049744.5,
//   2159312.5,
//   2268880.5,
//   2378448.5,
//   2488016.5,
//   2597584.5,
//   2707152.5,
//   };
// /* Corresponding file names of posted subfiles */
// char *de406names[] = {
// "unxm3000.406",
// "unxm2700.406",
// "unxm2400.406",
// "unxm2100.406",
// "unxm1800.406",
// "unxm1500.406",
// "unxm1200.406",
// "unxm0900.406",
// "unxm0600.406",
// "unxm0300.406",
// "unxp0000.406",
// "unxp0300.406",
// "unxp0600.406",
// "unxp0900.406",
// "unxp1200.406",
// "unxp1500.406",
// "unxp1800.406",
// "unxp2100.406",
// "unxp2400.406",
// "unxp2700.406",
// };

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// tdb.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* Find Barycentric Dynamical Time from Terrestrial Dynamical Time.
   Reference: Astronomical Almanac, page B5.  */

/* radians per arc second */
//#define STR 4.8481368110953599359e-6

/* 2000 January 1.5 */
//#define J2000 2451545.0

#if __STDC__
double sin (double);
double floor (double);
#else
double sin(), floor();
#endif

/* Argument JED is a Julian date, in TDT.
   Output is the corresponding date in TDB.  */

double tdb(double JED)
{
double M, T;

/* Find time T in Julian centuries from J2000.  */
T = (JED - J2000)/36525.0;

/* Mean anomaly of sun = l' (J. Laskar) */
M = 129596581.038354 * T +  1287104.76154;

/* Reduce arc seconds mod 360 degrees.  */
M = M - 1296000.0 * floor( M/1296000.0 );

M += ((((((((
  1.62e-20 * T
- 1.0390e-17 ) * T
- 3.83508e-15 ) * T
+ 4.237343e-13 ) * T
+ 8.8555011e-11 ) * T
- 4.77258489e-8 ) * T
- 1.1297037031e-5 ) * T
+ 1.4732069041e-4 ) * T
- 0.552891801772 ) * T * T;
M *= STR;
/* TDB - TDT, in seconds.  */
T = 0.001658 * sin(M) + 0.000014 * sin(M+M);

T = JED + T / 86400.0;
return(T);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// kepjpl.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* Read "Type 66" data records from JPL tape.
   This program extracts sampled time series data from the ephemeris. */


#ifdef DEBUG
#undef DEBUG
#endif
//#define DEBUG 1

/* Define or not in the makefile.  Use kepi.c when SSYSTEM defined.  */
#if ! SSYSTEM

/* Results wanted in TDT, convert to TDB while reading ephemeris.  */
#define TDB_TIME 0

/* Constants for the JPL DE405 Chebyshev Ephemeris
 */

#define DE102F 0
#define DE200F 0
#define DE245F 0
#define DE400F 1

double JDb = 2305424.5; /* starting JED 1599 Dec 9, for de405.unx */
double JDe = 2525008.5; /* ending JED 2201 Feb 20 */
double JDi = 32.0; /* days per record */
#define DRSIZ 8144
long recsiz = (long )DRSIZ; /* bytes per record */
long rec0 = 16288L; /* offset to first record */

int objs[13][3] = {
  {  3, 14, 4},
  {171, 10, 2},
  {231, 13, 2},
  {309, 11, 1},
  {342, 8, 1},
  {366, 7, 1},
  {387, 6, 1},
  {405, 6, 1},
  {423, 6, 1},
  {441, 13, 8},
  {753, 11, 2},
  {819, 10, 4},
  {899, 10, 4}
};

double au = 1.49597870691e8; /* Kilometers per au */
double emrat = 81.30056; /* Earth/Moon mass ratio */
double radearth = 6378.137;
double clight = 2.99792458e5;
char *dename = "unxp2000.405";

/* System header files required for I/O.
   Be sure your system is represented here.  */

#ifdef _MSC_VER
#include <sys\types.h>
#include <sys\stat.h>
#include <fcntl.h>
#include <string.h>
#include <io.h>
#endif

#ifdef __BORLANDC__
#include <io.h>
#include <sys\types.h>
#include <sys\stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#endif

#if UNIX
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#endif

#ifndef O_BINARY
#define O_BINARY 0
#endif

// extern double pearthb[];
// extern struct orbit earth;
// extern double coseps, sineps, J2000;
// extern char defile[];

int defd = 0;
static long bcnt = 0;

/* heliocentric velocity of given object (geocentric, if moon) */
// double pobjb[3];
// double pobjh[3];
// double vobjb[3];
// double vobjh[3];
// double psunb[3];
// double vsunb[3];

#if SETTHREE
static struct orbit xorb;
int set3();
#endif
#if ENDIANNESS
#if __STDC__
void swapend(double *);
#else
void swapend();
#endif
#endif

/* Number of coordinates ndim = 2 for nutations, else 3.  */
static int ndim;

int kepjpl(double J, struct orbit *e, double rect[], double polar[])
{
double JD, ratio, x, y, z;
double pm[3], vm[3];
int nobj, i;

nobj = objnum;
if( nobj == 3 )
	nobj = 10;	/* moon */
if( nobj == 0 )
	nobj = 3;	/* sun */

#if TDB_TIME
JD = tdb(J);
#else
JD = J;
#endif

/* Nutations or librations.  */
#if LIB403
ndim = 2;
nobj = 0;
#else
ndim = 3;
if( nobj == 12 || nobj == 13)
#endif
  {
    if (nobj == 12)
      ndim = 2;
    if( jpl( JD, nobj, pobjb, vobjb ) )
	{
	printf( "Error, DE file boundary violation\n" );
	exit(0);
	}
    for( i=0; i<ndim; i++)
      {
	rect[i] = pobjb[i];
	polar[i] = vobjb[i];
      }
    return 0;
  }

/* Sun */
#if DEBUG
printf( "psunb, vsunb\n" );
#endif
if( jpl( JD, 11, psunb, vsunb ) )
	{
	printf( "Error, DE file boundary violation\n" );
	exit(0);
	}

if( (e == &earth) || (nobj == 3) || (nobj == 10) )
	{
/* Moon, geocentric
 */
#if DEBUG
	printf( "pgmoon, vgmoon\n" );
#endif
	jpl( JD, 10, pm, vm );
#if DE102
#if SETTHREE
		set3( pm, vm, JD, 10, &xorb );
#else
		rotvec( pm, JD );
		rotvec( vm, JD );
#endif
#endif
	if( e != &earth )
		{
		for( i=0; i<3; i++ )
			{
			vobjb[i] = vm[i];
			pobjb[i] = pm[i];
			vobjh[i] = vm[i];
			pobjh[i] = pm[i];
			}
		goto output;
		}


/* Earth-Moon barycenter
 */
#if DEBUG
	printf( "pemb, vemb\n" );
#endif
	jpl( JD, 3, pobjb, vobjb );
/* Heliocentric EMB */
	for( i=0; i<3; i++ )
		{
		pobjh[i] = pobjb[i] - psunb[i];
		vobjh[i] = vobjb[i] - vsunb[i];
		}
#if DE102
#if SETTHREE
		set3( pobjh, vobjh, JD, 3, &xorb );
#else
		rotvec( pobjh, JD );
		rotvec( vobjh, JD );
#endif
#endif

/* Heliocentric Earth
 */
	ratio = 1.0/(emrat+1.0);
	for( i=0; i<3; i++ )
		{
		pobjh[i] = pobjh[i] - ratio * pm[i];
		vobjh[i] = vobjh[i] - ratio * vm[i];
		}

/* Earth position and velocity re solar system barycenter
 */
#if DEBUG
	printf( "barycentric earth = pemb - pgmoon/(1+emratio)\n" );
#endif
	for( i=0; i<3; i++ )
		{
		pearthb[i] = pobjh[i] + psunb[i];
		vearth[i] = vobjh[i] + vsunb[i];
		}
	jvearth = JD;
	}
else
	{
/* Selected object, not Earth or Moon
 */
#if DEBUG
	printf( "pobjb, vobjb\n" );
#endif
	jpl( JD, nobj, pobjb, vobjb );
	for( i=0; i<3; i++ )
		{
		pobjh[i] = pobjb[i] - psunb[i];
		vobjh[i] = vobjb[i] - vsunb[i];
		}
#if DE102
#if SETTHREE
	set3( pobjh, vobjh, JD, nobj, &xorb );
#else
	rotvec( pobjh, JD );
	rotvec( vobjh, JD );
#endif
#endif
#if DEBUG
	printf( "pobjb-psunb\n" );
	prvec(pobjh);
	prvec(vobjh); /* heliocentric velocity */
#endif
	}

output:

/* Write out the heliocentric object coordinates */
for( i=0; i<3; i++ )
	rect[i] = pobjh[i];
epsiln(J2000);
x = pobjh[0];
y = pobjh[1];
z = pobjh[2];
/* radius distance */
ratio = x*x + y*y + z*z;
polar[2] = sqrt(ratio);
/* Convert from equatorial to ecliptic coordinates */
ratio  =  coseps * y  +  sineps * z;
z  = -sineps * y  +  coseps * z;
y = ratio;
/* convert from rectangular to polar */
polar[0] = zatan2( x, y );
if( polar[2] != 0.0 )
  polar[1] = asin( z/polar[2] );
else
  polar[1] = 0.0;
return(0);
}



#if DEBUG
void prvec( double vec[] )
{
int k;

for( k=0; k<ndim; k++ )
	printf( "%24.16E ", vec[k] );
printf( "\n" );
}
#endif


int jpl( double JD, int iobj, double p[], double v[] )
{
  printf("all gets() calls commented out\n");
  exit(1);

int i, subis, nc, k, nchar;
long recno;
double JD0, JDs, x, t;
double cofs[18];
long double ps, vs;
long double pcofs[18];
long double vcofs[18];
#if DEBUG
double a[3];
long double acofs[18];
long double as;
#endif

/* Open up the ephemeris file.  */
#if DE406CD
/* The whole ephemeris is in one file.  */
#endif
#if DE406
      if (JD < de406dates[0])
	{
	  printf ("? date too early\n");
	  return -1;
	}
      for (ifile = 0; ifile < 20; ifile++)
        {
	  if (JD < de406dates[ifile])
	    break;
	}
      if (ifile >= 20)
	{
	  printf ("? date too large\n");
	  return -1;
	}
      ifile -= 1;
      if (ifile != lfile)
	{
	  if (defd > 0)
	    close (defd);
	  defd = 0;
	  pc = defile;
	  /* Find end of current file name string.  */
	  while (*pc != '\0')
	    ++pc;
	  /* Find last separator slash or colon in file name path.  */
	  while ((*pc != '/') && (*pc != ':') && (*pc != '\\'))
	    --pc;
	  ++pc;
	  /* Append file name to directory.  */
	  strcpy (pc, de406names[ifile]);
	  lfile = ifile;
	}
#endif  /* DE406 */
if( defd <= 0 )
	{
floop:
	if( defd < 0 )
		{
		printf( "Enter DE file name ? " );
		//gets( &defile[0] );
		}
	//	defd = open( &defile[0], O_BINARY | O_RDONLY, S_IREAD );
	defd = open( &dename[0], O_BINARY | O_RDONLY, S_IREAD );
	if( defd <= 0 )
		{
		printf( "Can't find DE file <%s>\n", &defile[0] );
		defd = -1;
		goto floop;
		}
	printf( "Opened %s\n", &dename[0] );

#if DE245 | DE400 | DE403 | DE404 | DE405 | DE406 | DE406CD | LIB403
	bcnt = rec0;
#else
#if DE200CD
/* This is for DE200 on CD rom. */
	bcnt = rec0;
#else
/* This is for DE200, DE102 magtapes. */
	bcnt = rec0 + 4L;
#endif
#endif
	lseek( defd, bcnt, SEEK_SET );
	read( defd, &JDb, 8 );
#if DECDATA
	cvtdec((unsigned short *)&JDb);
#endif
#if ENDIANNESS
	swapend( &JDb );
#endif

#if 1
	printf( "First date in file = %.8E\n", JDb );
#endif
	}

if (JD < JDb)
  return (-1);

/* Nutations have only two coordinates.  */
if( iobj == 12 )
  ndim = 2;
else
  ndim = 3;

iobj -= 1;
x = (JD - JDb) / JDi;	/* record number */
recno = (long) x;
JD0 = JDb + (double )recno * JDi;
bcnt = recno * recsiz + rec0;
i = sizeof( double ) * ( objs[iobj][0] - 1 );
#if DE245 | DE400 | DE403 | DE404 | DE405 | DE406 | DE406CD | LIB403
bcnt = bcnt + (long )i;
#else
#if DE200CD
bcnt = bcnt + (long )i;
#else
/* This is for DE200, DE102 on magtape. */
bcnt = bcnt + 4 + (long )i;
#endif
#endif
subis = objs[iobj][2];
nc = objs[iobj][1];
JDs = JDi/( (double )subis ); /* days per subrecord */

#if DEBUG
printf( "recno %ld\n", recno );
printf( "nc %d  subis %d\n", nc, subis );
#endif

if( subis > 1 )
	{
	recno = (long) ((JD - JD0) / JDs);
	JD0 += (double )recno * JDs; /* start time of subrecord */
	recno *= ndim * sizeof( double ) * nc;
	bcnt += recno;
	}

#if DEBUG
printf( "bcnt = %ld\n", bcnt );
#endif
lseek( defd, bcnt, SEEK_SET );
t = (JD - JD0)/JDs; /* time scaled to fraction of subrecord */

/* Chebyshev polynomials
 */
t = 2.0 * t - 1.0;
#if DEBUG
printf( "t %17.9E\n", t );
#endif
pcofs[0] = 1.0;
pcofs[1] = t;
vcofs[0] = 0.0;
vcofs[1] = 1.0;
#if DEBUG
acofs[0] = 0.0;
acofs[1] = 0.0;
#endif
t *= 2.0;
for( i=2; i<nc; i++ )
	{
	pcofs[i] = t * pcofs[i-1]  -  pcofs[i-2];
	vcofs[i] = 2.0 * pcofs[i-1] + t * vcofs[i-1] - vcofs[i-2];
#if DEBUG
	acofs[i] = 4.0 * vcofs[i-1] + t * acofs[i-1] - acofs[i-2];
#endif
	}

for( k=0; k<ndim; k++ )
	{
	nchar = sizeof( double ) * nc;
	if( read( defd, &cofs[0], nchar ) != nchar )
		return(-1);
#if DECDATA
	for( i=0; i<nc; i++ )
		cvtdec( (unsigned short *) &cofs[i] );
#endif
#if ENDIANNESS
	for( i=0; i<nc; i++ )
		swapend( &cofs[i] );
#endif
	ps = 0.0;
	vs = 0.0;
#if DEBUG
	as = 0.0;
#endif
	for( i=nc-1; i>=0; i-- )
		{
		ps += pcofs[i] * cofs[i];
		vs += vcofs[i] * cofs[i];
#if DEBUG
		as += acofs[i] * cofs[i];
#endif
		}
	t = 0.5 * JDs;
	/* Don't scale nutations or librations.  */
#if LIB403
	if (1)
#else
	if (iobj == 11 || iobj == 12)
#endif
	  {
	    p[k] = ps;
	    v[k] = vs / t;
#if DEBUG
	    a[k] = as / (t * t);
#endif
	  }
	else
	  {
	    p[k] = ps/au;
	    v[k] = vs / (t * au);
#if DEBUG
	    a[k] = as / (t * t * au);
#endif
	  }
#if DEBUG
printf (
  "k= %d: pcofs                vcofs              acofs            cofs\n",
  k);
for(i=0; i<nc; i++)
  {
    printf("%19.12Le%19.12Le%19.12Le%19.12e\n",
	   pcofs[i], vcofs[i], acofs[i], cofs[i]);
  }
#endif
      }
#if DEBUG
prvec(p);
prvec(v);
prvec(a);
#endif
return(0);
}

#if DE102
#if SETTHREE

#else
/* Convert the equinox of the DE102 ephemeris to J2000.
 * Note there are other, orbital, corrections required to make
 * the DE102 agree more accurately with DE200.
 */
rotvec( double vec[], double J )
{
double y[3];
double yy, zz;
int k;

y[0] =	 0.999925712980 * vec[0]
	-0.011178735677 * vec[1]
	-0.004858435955 * vec[2];
y[1] =	 0.012188864294 * vec[0]
	+0.917414007867 * vec[1]
	+0.397747369277 * vec[2];
y[2] =	 0.000010884494 * vec[0]
	-0.397777040626 * vec[1]
	+0.917482111996 * vec[2];


epsiln(J2000);
yy = y[1];
zz = y[2];
y[1]  =  coseps * yy  +  -sineps * zz;
y[2]  =  sineps * yy  +  coseps * zz;


for( k=0; k<3; k++ )
	vec[k] = y[k];
}
#endif
#endif

/* Convert DEC VAX/PDP-11 double precision number format
 * to IBM PC IEEE double number format.
 */
#if DECDATA
#include "cvtdec.c"
#endif

#if ENDIANNESS
#include <string.h>

void swapend(double *x)
{
union {
  double d;
  char ch[8];
}u;
char c;

#if 0
u.d = *x;
#else
memcpy ((void *) u.ch, (void *) x, 8);
#endif
c = u.ch[0];
u.ch[0] = u.ch[7];
u.ch[7] = c;
c = u.ch[1];
u.ch[1] = u.ch[6];
u.ch[6] = c;
c = u.ch[2];
u.ch[2] = u.ch[5];
u.ch[5] = c;
c = u.ch[3];
u.ch[3] = u.ch[4];
u.ch[4] = c;
*x = u.d;
}
#endif

#endif  /* not SSYSTEM  */

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// refrac.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* Atmospheric refraction
 * Returns correction in degrees to be added to true altitude
 * to obtain apparent altitude.
 *
 * -- S. L. Moshier
 */

#if __STDC__
double tan (double);
#else
double tan();
#endif

/* alt = altitude in degrees */

double refrac(double alt)
{
double y, y0, D0, N, D, P, Q;
int i;

if( (alt < -2.0) || (alt >= 90.0) )
	return(0.0);

/* For high altitude angle, AA page B61
 * Accuracy "usually about 0.1' ".
 */
if( alt > 15.0 )
	{
	D = 0.00452*atpress/((273.0+attemp)*tan( DTR*alt ));
	return(D);
	}

/* Formula for low altitude is from the Almanac for Computers.
 * It gives the correction for observed altitude, so has
 * to be inverted numerically to get the observed from the true.
 * Accuracy about 0.2' for -20C < T < +40C and 970mb < P < 1050mb.
 */

/* Start iteration assuming correction = 0
 */
y = alt;
D = 0.0;
/* Invert Almanac for Computers formula numerically
 */
P = (atpress - 80.0)/930.0;
Q = 4.8e-3 * (attemp - 10.0);
y0 = y;
D0 = D;

for( i=0; i<4; i++ )
	{
	N = y + (7.31/(y+4.4));
	N = 1.0/tan(DTR*N);
	D = N*P/(60.0 + Q * (N + 39.0));
	N = y - y0;
	y0 = D - D0 - N; /* denominator of derivative */

	if( (N != 0.0) && (y0 != 0.0) )
/* Newton iteration with numerically estimated derivative */
		N = y - N*(alt + D - y)/y0;
	else
/* Can't do it on first pass */
		N = alt + D;

	y0 = y;
	D0 = D;
	y = N;
	}
return( D );
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// diurpx.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* Diurnal parallax, AA page D3
 */

/* Earth radii per au. */
static double DISFAC;
//extern double aearth, au;
/* distance to center of Earth, in Earth radii */
//extern double trho;

/* observer's geocentric latitude, degrees */
//extern double tlat;

/* last = local apparent sidereal time, radians */
/* ra = right ascension, radians */
/* dec = declination, radians */
/* dist = Earth - object distance, au */

int diurpx( double last, double *ra, double *dec, double dist )
{
double cosdec, sindec, coslat, sinlat;
double p[3], dp[3], x, y, z, D;

/* Don't bother with this unless the equatorial horizontal parallax
 * is at least 0.005"
 */
if( dist > 1758.8 )
	return(-1);

DISFAC = au / (0.001 * aearth);
cosdec = cos(*dec);
sindec = sin(*dec);

/* Observer's astronomical latitude
 */
x = tlat * DTR;
coslat = cos(x);
sinlat = sin(x);

/* Convert to equatorial rectangular coordinates
 * in which unit distance = earth radius
 */
D = dist * DISFAC;
p[0] = D*cosdec*cos(*ra);
p[1] = D*cosdec*sin(*ra);
p[2] = D*sindec;

dp[0] = -trho*coslat*cos(last);
dp[1] = -trho*coslat*sin(last);
dp[2] = -trho*sinlat;

x = p[0] + dp[0];
y = p[1] + dp[1];
z = p[2] + dp[2];
D = x*x + y*y + z*z;
D = sqrt(D);	/* topocentric distance */

/* recompute ra and dec */
*ra = zatan2(x,y);
*dec = asin(z/D);
//showcor( "diurnal parallax", p, dp );
return(0);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// trnsit.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* Calculate time of transit
 * assuming RA and Dec change uniformly with time
 *
 * -- S. L. Moshier
 */

#if __STDC__
static void iter_func( double t, int (* func)(void) );
#else
static void iter_func();
#endif
//extern double glat;

/* Earth radii per au */
#define DISFAC 2.3454780e4

/* cosine of 90 degrees 50 minutes: */
#define COSSUN -0.014543897651582657
/* cosine of 90 degrees 34 minutes: */
#define COSZEN -9.8900378587411476e-3

/* Returned transit, rise, and set times in radians (2 pi = 1 day) */
double r_trnsit;
double r_rise;
double r_set;
int f_trnsit;

/* J = Julian date */
/* lha = local hour angle, radians */
/* dec = declination, radians */
int trnsit(double J, double lha, double dec)
{
double x, y, z, N, D, TPI;
double lhay, cosdec, sindec, coslat, sinlat;

f_trnsit = 0;
TPI = 2.0*PI;
/* Initialize to no-event flag value. */
r_rise = -10.0;
r_set = -10.0;
/* observer's geodetic latitude, in radians */
x = glat * DTR;
coslat = cos(x);
sinlat = sin(x);

cosdec = cos(dec);
sindec = sin(dec);

x = (J - floor(J-0.5) - 0.5) * TPI;
/* adjust local hour angle */
y = lha;
while( y < -PI )
	y += TPI;
while( y > PI )
	y -= TPI;
lhay = y;
y =  y/( -dradt/TPI + 1.00273790934);
r_trnsit = x - y;
/* Ordinarily never print here.  */
if( prtflg > 1 )
	{
	printf( "approx local meridian transit" );
	hms( r_trnsit );
	printf( "UT  (date + %.5f)\n", r_trnsit/TPI );
	}
if( (coslat == 0.0) || (cosdec == 0.0) )
	goto norise; 

/* The time at which the upper limb of the body meets the
 * horizon depends on the body's angular diameter.
 */
switch( objnum )
	{
/* Sun */
	case 0: N = COSSUN; break;

/* Moon, elevation = -34' - semidiameter + parallax
 * semidiameter = 0.272453 * parallax + 0.0799"
 */
	case 3:
		N = 1.0/(DISFAC*obpolar[2]);
		D = asin( N ); /* the parallax */
		N =  - 9.890199094634534e-3
			+ (1.0 - 0.2725076)*D
			- 3.874e-7;
		N = sin(N);
		break;

/* Other object */
	default: N = COSZEN; break;
	}
y = (N - sinlat*sindec)/(coslat*cosdec);

if( (y < 1.0) && (y > -1.0) )
	{
	f_trnsit = 1;
/* Derivative of y with respect to declination
 * times rate of change of declination:
 */
	z = -ddecdt*(sinlat + COSZEN*sindec);
	z /= TPI*coslat*cosdec*cosdec;
/* Derivative of acos(y): */
	z /= sqrt( 1.0 - y*y);
	y = acos(y);
	D = -dradt/TPI + 1.00273790934;
	r_rise = x - (lhay + y)*(1.0 + z)/D;
	r_set = x - (lhay - y)*(1.0 - z)/D;
	/* Ordinarily never print here.  */
	if( prtflg > 1 )
		{
		printf( "rises approx " );
		hms(r_rise);
		printf( "UT,  sets approx " );
		hms(r_set);
		printf( "UT\n" );
		}
	}
norise:		;
return(0);
}

// extern double r_rise;
// extern double r_trnsit;
// extern double r_set;
//extern struct orbit earth;

/* Julian dates of rise, transet and set times.  */
double t_rise;
double t_trnsit;
double t_set;
// int f_trnsit;

/* Compute estimate of lunar rise and set times for iterative solution.  */

static void iter_func(double t, int (* func)(void))
{
  int prtsave;

  prtsave = prtflg;
  prtflg = 0;
  JD = t;
  update(); /* Set UT and TDT */
  kepler( TDT, &earth, rearth, eapolar );
  (*func)();
  prtflg = prtsave;
}


/* Iterative computation of rise, transit, and set times.  */

int iter_trnsit(int (* func)())
{
  double JDsave, TDTsave, UTsave;
  double date, date_save, date_trnsit, t0, t1;
  double rise1, set1, trnsit1;
  double TPI;
  int prtsave;

  prtsave = prtflg;
  TPI = PI + PI;
  JDsave = JD;
  TDTsave = TDT;
  UTsave = UT;
  date = floor(UT - 0.5) + 0.5;
  /* Find transit time. */
  if (r_trnsit < 0.0)
    {
      date -= 1.0;
      r_trnsit += TPI;
     }
  if (r_trnsit > TPI)
    {
      date += 1.0;
      r_trnsit -= TPI;
    }
  t1 = date + r_trnsit/TPI;
  date_save = date;
  do
    {
      date = date_save;
      t0 = t1;
      iter_func(t0, func);
      if (r_trnsit < 0.0)
	{
	  date -= 1.0;
	  r_trnsit += TPI;
	}
      if (r_trnsit > TPI)
	{
	  date += 1.0;
	  r_trnsit -= TPI;
	}
      t1 = date + r_trnsit / TPI;
    }
  while (fabs(t1 - t0) > .0001);

  t_trnsit = t1;
  trnsit1 = r_trnsit;
  set1 = r_set;

  if (f_trnsit == 0)
	goto prtrnsit;

  /* Set current date to be that of the transit just found.  */
  date_trnsit = date;
  if (r_rise < 0.0)
    {
      date -= 1.0;
      r_rise += TPI;
    }
  if (r_rise > TPI)
    {
      date += 1.0;
      r_rise -= TPI;
    }
  t1 = date + r_rise / TPI;
  /* Choose rising no later than transit.  */
  do
    {
      t0 = t1;
      iter_func(t0, func);
      t1 = date + r_rise / TPI;
      if (t1 > t_trnsit)
	{
	  date -= 1;
	  t1 = date + r_rise / TPI;
	}
    }
  while (fabs(t1 - t0) > .0001);
  rise1 = r_rise;
  t_rise = t1;

  date = date_trnsit;
  r_set = set1;
  if (r_set < 0.0)
    {
      date -= 1.0;
      r_set += TPI;
    }
  if (r_set > TPI)
    {
      date += 1.0;
      r_set -= TPI;
    }
  t1 = date + r_set / TPI;
  /* Choose setting no earlier than transit.  */
  do
    {
      t0 = t1;
      iter_func(t0, func);
      t1 = date + r_set / TPI;
      if (t1 < t_trnsit)
	{
	  date += 1.0;
	  t1 = date + r_set / TPI;
	}
    }
  while (fabs(t1 - t0) > .0001);

  t_set = t1;
  r_trnsit = trnsit1;
  r_rise = rise1;

prtrnsit:

  prtflg = 1;
  printf( "local meridian transit " );
  UT = t_trnsit;
  jtocal (t_trnsit);
  if (f_trnsit != 0)
    {
      printf( "rises " );
      UT = t_rise;
      jtocal (t_rise);
      printf( "sets " );
      UT = t_set;
      jtocal (t_set);
      printf("Visible hours %.4f\n", 24.0 * (t_set - t_rise));
    }
  JD = JDsave;
  TDT = TDTsave;
  UT = UTsave;
  /* Reset to original input date entry.  */
  prtflg = 0;
  update();
  prtflg = prtsave;
  return 0;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// diurab.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* Diurnal aberration
 * This formula is less rigorous than the method used for
 * annual aberration.  However, the correction is small.
 */

/* last = local apparent sidereal time, radians */
/* ra = right ascension, radians */
/* dec = declination, radians */

int diurab( double last, double *ra, double *dec )
{
double lha, coslha, sinlha, cosdec, sindec;
double coslat, N, D;

lha = last - *ra;
coslha = cos(lha);
sinlha = sin(lha);
cosdec = cos(*dec);
sindec = sin(*dec);
coslat = cos( DTR*tlat );

if( cosdec != 0.0 )
	N = 1.5472e-6*trho*coslat*coslha/cosdec;
else
	N = 0.0;
*ra += N;

D = 1.5472e-6*trho*coslat*sinlha*sindec;
*dec += D;

if( prtflg )
	{
	  //printf( "diurnal aberration dRA %.3fs dDec %.2f\"\n", RTS*N/15.0, RTS*D );
	}
return(0);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// sidrlt.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* Local Apparent Sidereal Time with equation of the equinoxes
 * AA page B6
 *
 * The siderial time coefficients are from Williams (1994).
 *
 * Caution. At epoch J2000.0, the 16 decimal precision
 * of IEEE double precision numbers
 * limits time resolution measured by Julian date
 * to approximately 24 microseconds.
 */

//extern double J2000, TDT, RTD, nutl, coseps;
#if __STDC__
double floor (double);
int nutlo (double);
int epsiln (double);
double sidrlt (double, double);
double refrac (double);
#else
double floor();
int nutlo(), epsiln();
double sidrlt(), refrac();
#endif

/* program returns sidereal seconds since sidereal midnight */
/* j = Julian date and fraction, tlong = East longitude of observer, degrees */
double sidrlt(double j, double tlong )
{
double jd0;     /* Julian day at midnight Universal Time */
double secs;   /* Time of day, UT seconds since UT midnight */
double eqeq, gmst, jd, T0, msday;
/*long il;*/

  /* Julian day at given UT */
jd = j;
jd0 = floor(jd);
secs = j - jd0;
if( secs < 0.5 )
	{
	jd0 -= 0.5;
	secs += 0.5;
	}
else
	{
	jd0 += 0.5;
	secs -= 0.5;
	}
secs *= 86400.0;

  /* Julian centuries from standard epoch J2000.0 */
/* T = (jd - J2000)/36525.0; */
  /* Same but at 0h Universal Time of date */
T0 = (jd0 - J2000)/36525.0;

/* The equation of the equinoxes is the nutation in longitude
 * times the cosine of the obliquity of the ecliptic.
 * We already have routines for these.
 */
nutlo(TDT);
epsiln(TDT);
/* nutl is in radians; convert to seconds of time
 * at 240 seconds per degree
 */
eqeq = 240.0 * RTD * nutl * coseps;
  /* Greenwich Mean Sidereal Time at 0h UT of date */
#if (DE403 | DE404 | DE405 | DE406 | DE406CD)
#if 1
/* Williams (1994) updated to DE403 values.  */
gmst = (((-2.0e-6*T0 - 3.e-7)*T0 + 9.27701e-2)*T0 + 8640184.7942063)*T0
       + 24110.54841;
msday = (((-(4. * 2.0e-6)*T0 - (3. * 3.e-7))*T0 + (2. * 9.27701e-2))*T0
          + 8640184.7942063)/(86400.*36525.) + 1.0;
#else
  /* J. G. Williams, "Contributions to the Earth's obliquity rate, precession,
     and nutation,"  Astronomical Journal 108, p. 711 (1994)  */
gmst = (((-2.0e-6*T0 - 3.e-7)*T0 + 9.27695e-2)*T0 + 8640184.7928613)*T0
       + 24110.54841;
/* mean solar (er, UT) days per sidereal day at date T0  */
msday = (((-(4. * 2.0e-6)*T0 - (3. * 3.e-7))*T0 + (2. * 9.27695e-2))*T0
          + 8640184.7928613)/(86400.*36525.) + 1.0;
#endif /* not 1 */
#else /* not DE403 or DE404 */
/* This is the 1976 IAU formula. */
gmst = (( -6.2e-6*T0 + 9.3104e-2)*T0 + 8640184.812866)*T0 + 24110.54841;
/* mean solar days per sidereal day at date T0  */
msday = 1.0 + ((-1.86e-5*T0 + 0.186208)*T0 + 8640184.812866)/(86400.*36525.);
#endif /* not DE403 or DE404 */
  /* Local apparent sidereal time at given UT */
gmst = gmst + msday*secs + eqeq + 240.0*tlong;
  /* Sidereal seconds modulo 1 sidereal day */
gmst = gmst - 86400.0 * floor( gmst/86400.0 );
/*
 * il = gmst/86400.0;
 * gmst = gmst - 86400.0 * il;
 * if( gmst < 0.0 )
 *	gmst += 86400.0;
 */
return( gmst );
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// altaz.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* Apply diurnal aberrations and calculate topocentric
 * altitude and azimuth, given the geocentric apparent
 * right ascension and declination.
 *
 * Ephemeris transit times can be obtained by modifying
 * aa.ini to declare TDT = UT.
 * 
 * This program assumes that deltaT, the difference
 * between Ephemeris Time (TDT) and Universal Time (UT),
 * has already been calculated.  The global variables
 * TDT and UT must already have been loaded with the
 * correct values of TDT and UT, respectively.  See deltat.c.
 *
 * Inputs are polar coordinates:
 * dist is the true geocentric distance in au.
 * ra and dec are in radians.
 *
 * J is the Julian date in UT measure.
 *
 * AA page B60 and D3.
 */

int altaz( double pol[3], double J )
{
double dec, cosdec, sindec, lha, coslha, sinlha;
double ra, dist, last, alt, az, coslat, sinlat;
double N, D, x, y, z, TPI;

ra = pol[0];
dec = pol[1];
dist = pol[2];
TPI = 2.0*PI;

/* local apparent sidereal time, seconds converted to radians
 */
last = sidrlt( J, tlong ) * DTR/240.0;
lha = last - ra; /* local hour angle, radians */
if( prtflg )
	{
	  printf( "Local apparent sidereal time " );
	hms( last );
	//	printf( "\n" );
	}
/* Display rate at which ra and dec are changing
 */
/*
 *if( prtflg )
 *	{
 *	x = RTS/24.0;
 *	N = x*dradt;
 *	D = x*ddecdt;
 *	if( N != 0.0 )
 *		printf( "dRA/dt %.2f\"/h, dDec/dt %.2f\"/h\n", N, D );
 *	}
 */

diurab( last, &ra, &dec );
/* Do rise, set, and transit times
   trnsit.c takes diurnal parallax into account,
   but not diurnal aberration.  */
lha = last - ra;
trnsit( J, lha, dec );

/* Diurnal parallax
 */
diurpx( last, &ra, &dec, dist );

/* Diurnal aberration
 */
/*diurab( last, &ra, &dec );*/

/* Convert ra and dec to altitude and azimuth
 */
cosdec = cos(dec);
sindec = sin(dec);
lha = last - ra;
coslha = cos(lha);
sinlha = sin(lha);

/* Use the geodetic latitude for altitude and azimuth */
x = DTR * glat;
coslat = cos(x);
sinlat = sin(x);

N = -cosdec * sinlha;
D =  sindec * coslat  -  cosdec * coslha * sinlat;
az = RTD * zatan2( D, N );
alt = sindec * sinlat  +  cosdec * coslha * coslat;
alt = RTD * asin(alt);

/* Correction for atmospheric refraction
 * unit = degrees
 */
D = refrac( alt );
alt += D;

/* Convert back to R.A. and Dec.
 */
y = sin(DTR*alt);
x = cos(DTR*alt);
z = cos(DTR*az);
sinlha = -x * sin(DTR*az);
coslha = y*coslat - x*z*sinlat;
sindec = y*sinlat + x*z*coslat;
lha = zatan2( coslha, sinlha );

y = ra; /* save previous values, before refrac() */
z = dec;
dec = asin( sindec );
ra = last - lha;
y = ra - y; /* change in ra */
while( y < -PI )
	y += TPI;
while( y > PI )
	y -= TPI;
y = RTS*y/15.0;
z = RTS*(dec - z);
//if( prtflg )
if( 1 )
	{
	  //	printf( "atmospheric refraction %.3f deg  dRA %.3fs dDec %.2f\"\n",
	  //		 D, y, z );
	printf( "Topocentric:  Altitude %.3f deg, ", alt );
	printf( "Azimuth %.3f deg\n", az );

 	printf( "Topocentric: R.A." );
 	hms( ra );
 	printf( " Dec." );
 	dms( dec );
 	printf( "\n" );
 	}
return(0);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// nutate.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* Nutation in longitude and obliquity
 * computed at Julian date J.
 *
 * References:
 * "Summary of 1980 IAU Theory of Nutation (Final Report of the
 * IAU Working Group on Nutation)", P. K. Seidelmann et al., in
 * Transactions of the IAU Vol. XVIII A, Reports on Astronomy,
 * P. A. Wayman, ed.; D. Reidel Pub. Co., 1982.
 *
 * "Nutation and the Earth's Rotation",
 * I.A.U. Symposium No. 78, May, 1977, page 256.
 * I.A.U., 1980.
 *
 * Woolard, E.W., "A redevelopment of the theory of nutation",
 * The Astronomical Journal, 58, 1-3 (1953).
 *
 * This program implements all of the 1980 IAU nutation series.
 * Results checked at 100 points against the 1986 AA; all agreed.
 *
 *
 * - S. L. Moshier, November 1987
 * October, 1992 - typo fixed in nutation matrix
 * October, 1995 - fixed typo in node argument,
 *                 tested against JPL DE403 ephemeris file.
 */

#if __STDC__
int sscc(int, double, int);
extern int epsiln(double);
double sin(double), cos(double), floor(double);
int showcor (char *, double *, double *);
int nutlo (double);
#else
int sscc(), epsiln(), showcor(), nutlo();
double sin(), cos(), floor();
#endif

/* The answers are posted here by nutlo():
 */
double jdnut = -1.0;	/* time to which the nutation applies */
double nutl = 0.0;	/* nutation in longitude (radians) */
double nuto = 0.0;	/* nutation in obliquity (radians) */
//extern double eps, coseps, sineps, STR;

/* Each term in the expansion has a trigonometric
 * argument given by
 *   W = i*MM + j*MS + k*FF + l*DD + m*OM
 * where the variables are defined below.
 * The nutation in longitude is a sum of terms of the
 * form (a + bT) * sin(W). The terms for nutation in obliquity
 * are of the form (c + dT) * cos(W).  The coefficients
 * are arranged in the tabulation as follows:
 * 
 * Coefficient:
 * i  j  k  l  m      a      b      c     d
 * 0, 0, 0, 0, 1, -171996, -1742, 92025, 89,
 * The first line of the table, above, is done separately
 * since two of the values do not fit into 16 bit integers.
 * The values a and c are arc seconds times 10000.  b and d
 * are arc seconds per Julian century times 100000.  i through m
 * are integers.  See the program for interpretation of MM, MS,
 * etc., which are mean orbital elements of the Sun and Moon.
 *
 * If terms with coefficient less than X are omitted, the peak
 * errors will be:
 *
 *   omit	error,		  omit	error,
 *   a <	longitude	  c <	obliquity
 * .0005"	.0100"		.0008"	.0094"
 * .0046	.0492		.0095	.0481
 * .0123	.0880		.0224	.0905
 * .0386	.1808		.0895	.1129
 */
short nt[105*9] = {
 0, 0, 0, 0, 2, 2062, 2,-895, 5,
-2, 0, 2, 0, 1, 46, 0,-24, 0,
 2, 0,-2, 0, 0, 11, 0, 0, 0,
-2, 0, 2, 0, 2,-3, 0, 1, 0,
 1,-1, 0,-1, 0,-3, 0, 0, 0,
 0,-2, 2,-2, 1,-2, 0, 1, 0,
 2, 0,-2, 0, 1, 1, 0, 0, 0,
 0, 0, 2,-2, 2,-13187,-16, 5736,-31,
 0, 1, 0, 0, 0, 1426,-34, 54,-1,
 0, 1, 2,-2, 2,-517, 12, 224,-6,
 0,-1, 2,-2, 2, 217,-5,-95, 3,
 0, 0, 2,-2, 1, 129, 1,-70, 0,
 2, 0, 0,-2, 0, 48, 0, 1, 0,
 0, 0, 2,-2, 0,-22, 0, 0, 0,
 0, 2, 0, 0, 0, 17,-1, 0, 0,
 0, 1, 0, 0, 1,-15, 0, 9, 0,
 0, 2, 2,-2, 2,-16, 1, 7, 0,
 0,-1, 0, 0, 1,-12, 0, 6, 0,
-2, 0, 0, 2, 1,-6, 0, 3, 0,
 0,-1, 2,-2, 1,-5, 0, 3, 0,
 2, 0, 0,-2, 1, 4, 0,-2, 0,
 0, 1, 2,-2, 1, 4, 0,-2, 0,
 1, 0, 0,-1, 0,-4, 0, 0, 0,
 2, 1, 0,-2, 0, 1, 0, 0, 0,
 0, 0,-2, 2, 1, 1, 0, 0, 0,
 0, 1,-2, 2, 0,-1, 0, 0, 0,
 0, 1, 0, 0, 2, 1, 0, 0, 0,
-1, 0, 0, 1, 1, 1, 0, 0, 0,
 0, 1, 2,-2, 0,-1, 0, 0, 0,
 0, 0, 2, 0, 2,-2274,-2, 977,-5,
 1, 0, 0, 0, 0, 712, 1,-7, 0,
 0, 0, 2, 0, 1,-386,-4, 200, 0,
 1, 0, 2, 0, 2,-301, 0, 129,-1,
 1, 0, 0,-2, 0,-158, 0,-1, 0,
-1, 0, 2, 0, 2, 123, 0,-53, 0,
 0, 0, 0, 2, 0, 63, 0,-2, 0,
 1, 0, 0, 0, 1, 63, 1,-33, 0,
-1, 0, 0, 0, 1,-58,-1, 32, 0,
-1, 0, 2, 2, 2,-59, 0, 26, 0,
 1, 0, 2, 0, 1,-51, 0, 27, 0,
 0, 0, 2, 2, 2,-38, 0, 16, 0,
 2, 0, 0, 0, 0, 29, 0,-1, 0,
 1, 0, 2,-2, 2, 29, 0,-12, 0,
 2, 0, 2, 0, 2,-31, 0, 13, 0,
 0, 0, 2, 0, 0, 26, 0,-1, 0,
-1, 0, 2, 0, 1, 21, 0,-10, 0,
-1, 0, 0, 2, 1, 16, 0,-8, 0,
 1, 0, 0,-2, 1,-13, 0, 7, 0,
-1, 0, 2, 2, 1,-10, 0, 5, 0,
 1, 1, 0,-2, 0,-7, 0, 0, 0,
 0, 1, 2, 0, 2, 7, 0,-3, 0,
 0,-1, 2, 0, 2,-7, 0, 3, 0,
 1, 0, 2, 2, 2,-8, 0, 3, 0,
 1, 0, 0, 2, 0, 6, 0, 0, 0,
 2, 0, 2,-2, 2, 6, 0,-3, 0,
 0, 0, 0, 2, 1,-6, 0, 3, 0,
 0, 0, 2, 2, 1,-7, 0, 3, 0,
 1, 0, 2,-2, 1, 6, 0,-3, 0,
 0, 0, 0,-2, 1,-5, 0, 3, 0,
 1,-1, 0, 0, 0, 5, 0, 0, 0,
 2, 0, 2, 0, 1,-5, 0, 3, 0, 
 0, 1, 0,-2, 0,-4, 0, 0, 0,
 1, 0,-2, 0, 0, 4, 0, 0, 0,
 0, 0, 0, 1, 0,-4, 0, 0, 0,
 1, 1, 0, 0, 0,-3, 0, 0, 0,
 1, 0, 2, 0, 0, 3, 0, 0, 0,
 1,-1, 2, 0, 2,-3, 0, 1, 0,
-1,-1, 2, 2, 2,-3, 0, 1, 0,
-2, 0, 0, 0, 1,-2, 0, 1, 0,
 3, 0, 2, 0, 2,-3, 0, 1, 0,
 0,-1, 2, 2, 2,-3, 0, 1, 0,
 1, 1, 2, 0, 2, 2, 0,-1, 0,
-1, 0, 2,-2, 1,-2, 0, 1, 0,
 2, 0, 0, 0, 1, 2, 0,-1, 0,
 1, 0, 0, 0, 2,-2, 0, 1, 0,
 3, 0, 0, 0, 0, 2, 0, 0, 0,
 0, 0, 2, 1, 2, 2, 0,-1, 0,
-1, 0, 0, 0, 2, 1, 0,-1, 0,
 1, 0, 0,-4, 0,-1, 0, 0, 0,
-2, 0, 2, 2, 2, 1, 0,-1, 0,
-1, 0, 2, 4, 2,-2, 0, 1, 0,
 2, 0, 0,-4, 0,-1, 0, 0, 0,
 1, 1, 2,-2, 2, 1, 0,-1, 0,
 1, 0, 2, 2, 1,-1, 0, 1, 0,
-2, 0, 2, 4, 2,-1, 0, 1, 0,
-1, 0, 4, 0, 2, 1, 0, 0, 0,
 1,-1, 0,-2, 0, 1, 0, 0, 0,
 2, 0, 2,-2, 1, 1, 0,-1, 0,
 2, 0, 2, 2, 2,-1, 0, 0, 0,
 1, 0, 0, 2, 1,-1, 0, 0, 0,
 0, 0, 4,-2, 2, 1, 0, 0, 0,
 3, 0, 2,-2, 2, 1, 0, 0, 0,
 1, 0, 2,-2, 0,-1, 0, 0, 0,
 0, 1, 2, 0, 1, 1, 0, 0, 0,
-1,-1, 0, 2, 1, 1, 0, 0, 0,
 0, 0,-2, 0, 1,-1, 0, 0, 0,
 0, 0, 2,-1, 2,-1, 0, 0, 0,
 0, 1, 0, 2, 0,-1, 0, 0, 0,
 1, 0,-2,-2, 0,-1, 0, 0, 0,
 0,-1, 2, 0, 1,-1, 0, 0, 0,
 1, 1, 0,-2, 1,-1, 0, 0, 0,
 1, 0,-2, 2, 0,-1, 0, 0, 0,
 2, 0, 0, 2, 0, 1, 0, 0, 0,
 0, 0, 2, 4, 2,-1, 0, 0, 0,
 0, 1, 0, 1, 0, 1, 0, 0, 0,
};


/* arrays to hold sines and cosines of multiple angles
 */

double aa_ss[5][8];
double cc[5][8];

#define mod3600(x) ((x) - 1296000. * floor ((x)/1296000.))

int nutlo(double J)
{
double f, g, T, T2, T10;
double MM, MS, FF, DD, OM;
double cu, su, cv, sv, sw;
double C, D;
int i, j, k, k1, m;
short *p;

if( jdnut == J )
	return(0);
jdnut = J;

/* Julian centuries from 2000 January 1.5,
 * barycentric dynamical time
 */
T = (J-2451545.0)/36525.0;
T2 = T * T;
T10 = T / 10.0;

/* Fundamental arguments in the FK5 reference system.  */

/* longitude of the mean ascending node of the lunar orbit
 * on the ecliptic, measured from the mean equinox of date
 */
OM = (mod3600 (-6962890.539 * T + 450160.280) + (0.008 * T + 7.455) * T2)
    * STR;

/* mean longitude of the Sun minus the
 * mean longitude of the Sun's perigee
 */
MS = (mod3600 (129596581.224 * T + 1287099.804) - (0.012 * T + 0.577) * T2)
    * STR;

/* mean longitude of the Moon minus the
 * mean longitude of the Moon's perigee
 */
MM = (mod3600 (1717915922.633 * T + 485866.733) + (0.064 * T + 31.310) * T2)
    * STR;

/* mean longitude of the Moon minus the
 * mean longitude of the Moon's node
 */
FF = (mod3600 (1739527263.137 * T + 335778.877) + (0.011 * T - 13.257) * T2)
    * STR;

/* mean elongation of the Moon from the Sun.
 */
DD = (mod3600 (1602961601.328 * T + 1072261.307) + (0.019 * T - 6.891) * T2)
    * STR;

/* Calculate sin( i*MM ), etc. for needed multiple angles
 */
sscc( 0, MM, 3 );
sscc( 1, MS, 2 );
sscc( 2, FF, 4 );
sscc( 3, DD, 4 );
sscc( 4, OM, 2 );

C = 0.0;
D = 0.0;
p = &nt[0]; /* point to start of table */

for( i=0; i<105; i++ )
	{
/* argument of sine and cosine */
	k1 = 0;
	cv = 0.0;
	sv = 0.0;
	for( m=0; m<5; m++ )
		{
		j = *p++;
		if( j )
			{
			k = j;
			if( j < 0 )
				k = -k;
			su = aa_ss[m][k-1]; /* sin(k*angle) */
			if( j < 0 )
				su = -su;
			cu = cc[m][k-1];
			if( k1 == 0 )
				{ /* set first angle */
				sv = su;
				cv = cu;
				k1 = 1;
				}
			else
				{ /* combine angles */
				sw = su*cv + cu*sv;
				cv = cu*cv - su*sv;
				sv = sw;
				}
			}
		}
/* longitude coefficient */
	f  = *p++;
	if( (k = *p++) != 0 )
		f += T10 * k;

/* obliquity coefficient */
	g = *p++;
	if( (k = *p++) != 0 )
		g += T10 * k;

/* accumulate the terms */
	C += f * sv;
	D += g * cv;
	}
/* first terms, not in table: */
C += (-1742.*T10 - 171996.)*aa_ss[4][0];	/* sin(OM) */
D += (   89.*T10 +  92025.)*cc[4][0];	/* cos(OM) */
/*
printf( "nutation: in longitude %.3f\", in obliquity %.3f\"\n", C, D );
*/
/* Save answers, expressed in radians */
nutl = 0.0001 * STR * C;
nuto = 0.0001 * STR * D;
return(0);
}



/* Nutation -- AA page B20
 * using nutation in longitude and obliquity from nutlo()
 * and obliquity of the ecliptic from epsiln()
 * both calculated for Julian date J.
 *
 * p[] = equatorial rectangular position vector of object for
 * mean ecliptic and equinox of date.
 */
int nutate( double J, double p[] )
{
double ce, se, cl, sl, sino, f;
double dp[3], p1[3];
int i;

nutlo(J); /* be sure we calculated nutl and nuto */
epsiln(J); /* and also the obliquity of date */

f = eps + nuto;
ce = cos( f );
se = sin( f );
sino = sin(nuto);
cl = cos( nutl );
sl = sin( nutl );

/* Apply adjustment
 * to equatorial rectangular coordinates of object.
 *
 * This is a composite of three rotations: rotate about x axis
 * to ecliptic of date; rotate about new z axis by the nutation
 * in longitude; rotate about new x axis back to equator of date
 * plus nutation in obliquity.
 */
p1[0] =   cl*p[0]
        - sl*coseps*p[1]
        - sl*sineps*p[2];

p1[1] =   sl*ce*p[0]
        + ( cl*coseps*ce + sineps*se )*p[1]
        - ( sino + (1.0-cl)*sineps*ce )*p[2];

p1[2] =   sl*se*p[0]
        + ( sino + (cl-1.0)*se*coseps )*p[1]
        + ( cl*sineps*se + coseps*ce )*p[2];

for( i=0; i<3; i++ )
	dp[i] = p1[i] - p[i];
//showcor( "nutation", p, dp );

for( i=0; i<3; i++ )
	p[i] = p1[i];
return(0);
}


/* Prepare lookup table of sin and cos ( i*Lj )
 * for required multiple angles
 */
int sscc( int k, double arg, int n )
{
double cu, su, cv, sv, s;
int i;

su = sin(arg);
cu = cos(arg);
aa_ss[k][0] = su;			/* sin(L) */
cc[k][0] = cu;			/* cos(L) */
sv = 2.0*su*cu;
cv = cu*cu - su*su;
aa_ss[k][1] = sv;			/* sin(2L) */
cc[k][1] = cv;

for( i=2; i<n; i++ )
	{
	s =  su*cv + cu*sv;
	cv = cu*cv - su*sv;
	sv = s;
	aa_ss[k][i] = sv;		/* sin( i+1 L ) */
	cc[k][i] = cv;
	}
return(0);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// vearth.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* Calculate the velocity vector of the earth
 * as it moves in its elliptical orbit.
 * The difference in velocity between this and assuming
 * a circular orbit is only about 1 part in 10**4.
 *
 * Note that this gives heliocentric, not barycentric, velocity.
 *
 * Input is Julian date.  Output left in global array vearth[].
 */

/* Barycentric position and velocity of the earth
 * at time jvearth.
 */

int velearth( double J )
{
  double e[3], p[3];
  double q;
  int i;
#if DEBUG
double x[3], A;
#endif

if( J == jvearth )
	return(0);

jvearth = J;

#if DEPOLYN
kepler( TDT, &earth, e, p );
#else
/* calculate heliocentric position of the earth
 * as of a short time ago.
 */
q = 0.005;
kepler( TDT-q, &earth, e, p );
for( i=0; i<3; i++ )
	vearth[i] = (rearth[i] - e[i])/q;
#endif
#if DEBUG
/* Generate display for comparison with Almanac values. */
A = 0.0;
for( i=0; i<3; i++ )
	{
	q = vearth[i];
	A += q*q;
	x[i] = q;
	}
A = sqrt(A);
precess( x, TDT, 1 );
printf( "Vearth %.6e, X %.6f, Y %.6f, Z %.6f\n", A, x[0], x[1], x[2] );
#endif
return(0);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// annuab.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* Annual aberration - AA pages B17, B37, C24
 */

int annuab( double p[] ) /* unit vector pointing from earth to object */
{
double A, B, C;
double betai, pV;
double x[3], V[3];
int i;

/* Calculate the velocity of the earth (see vearth.c).
 */
velearth( TDT );
betai = 0.0;
pV = 0.0;
for( i=0; i<3; i++ )
	{
	A = vearth[i]/Clightaud;
	V[i] = A;
	betai += A*A;
	pV += p[i] * A;
	}
/* Make the adjustment for aberration.
 */
betai = sqrt( 1.0 - betai );
C = 1.0 + pV;
A = betai/C;
B = (1.0  +  pV/(1.0 + betai))/C;

for( i=0; i<3; i++ )
	{
	C = A * p[i]  +  B * V[i];
	x[i] = C;
	dp[i] = C - p[i];
	}

//showcor( "annual aberration", p, dp );
for( i=0; i<3; i++ )
	p[i] = x[i];
return(0);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// kepj.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* Program to solve Keplerian orbit
 * given orbital parameters and the time.
 * Returns Heliocentric equatorial rectangular coordinates of
 * the object.
 *
 * This program detects several cases of given orbital elements.
 * If a program for perturbations is pointed to, it is called
 * to calculate all the elements.
 * If there is no program, then the mean longitude is calculated
 * from the mean anomaly and daily motion.
 * If the daily motion is not given, it is calculated
 * by Kepler's law.
 * If the eccentricity is given to be 1.0, it means that
 * meandistance is really the perihelion distance, as in a comet
 * specification, and the orbit is parabolic.
 *
 * Reference: Taff, L.G., "Celestial Mechanics, A Computational
 * Guide for the Practitioner."  Wiley, 1985.
 */

#if DEPOLYN

#ifndef ANSIPROT
int kepler0();
#endif

int kepler(double J, struct orbit *e, double rect[], double polar[])
{
int k;

if( (e == &earth) || (objnum != 99) )
	{
	k = kepjpl( J, e, rect, polar );
	return(k);
	}
else
	kepler0(J, e, rect, polar);
return(0);
}



int kepler0(double J, struct orbit *e, double rect[], double polar[])
#else
int kepler(double J, struct orbit *e, double rect[], double polar[])
#endif
{
double alat, E, M, W, v, temp;
double epoch, inclination, ascnode, argperih;
double meandistance, dailymotion, eccent, meananomaly;
double r, coso, sino, cosa, sina, sinW, cosv, sinv;
int k;

#if !DEPOLYN
/* Compute orbital elements if a program for doing so
 * is supplied
 */
 if ( e->oelmnt ) {
   printf("oelmnt not supported\n");
   exit(1);
//    k = (*(e->oelmnt) )(e,J);
//    if( k == -1 )
//      goto dobs;
 }
 else if( e->celmnt ) { // call B & S algorithm 
   printf("celmnt not supported\n");
   exit(1);
   /*
   dobs:
     (*(e->celmnt) )( J, polar );
     polar[0] = modtp( polar[0] );
     E = polar[0]; // longitude 
     e->L = E;
     W = polar[1]; // latitude
     r = polar[2]; // radius 
     e->r = r;
     e->epoch = J;
     e->equinox = J;
     goto kepdon;
   */
   }
#endif

/* Decant the parameters from the data structure
 */
epoch = e->epoch;
inclination = DTR * e->i;
ascnode = DTR * e->W;
argperih = DTR * e->w;
meandistance = e->a; /* semimajor axis */
dailymotion = e->dm;
eccent = e->ecc;
meananomaly = e->M;
/* Check for parabolic orbit. */
if( eccent == 1.0 )
	{
/* meandistance = perihelion distance, q
 * epoch = perihelion passage date
 */
	temp = meandistance * sqrt(meandistance);
	W = (J - epoch ) * 0.0364911624 / temp;
	E = 0.0;
	M = 1.0;
	while( fabs(M) > 1.0e-15 )
		{
		temp = E * E;
		temp = (2.0 * E * temp + W)/( 3.0 * (1.0 + temp));
		M = temp - E;
		if( temp != 0.0 )
			M /= temp;
		E = temp;
		}
	r = meandistance * (1.0 + E * E );
	M = atan( E );
	M = 2.0 * M;
	alat = M + argperih;
	v = M;
	goto parabcon;
	}
/* Calculate the daily motion, if it is not given.
 */
if( dailymotion == 0.0 )
	{
	dailymotion = 0.985607828/( meandistance * sqrt(meandistance) );
	}
dailymotion *= J - epoch;
/* M is proportional to the area swept out by the radius
 * vector of a circular orbit during the time between
 * perihelion passage and Julian date J.
 * It is the mean anomaly at time J.
 */
M = DTR*( meananomaly + dailymotion );
M = modtp(M);
/* If mean longitude was calculated, adjust it also
 * for motion since epoch of elements.
 */
if( e->L )
	{
	e->L += dailymotion;
	e->L = mod360( e->L );
	}

/* By Kepler's second law, M must be equal to
 * the area swept out in the same time by an
 * elliptical orbit of same total area.
 * Integrate the ellipse expressed in polar coordinates
 *     r = a(1-e^2)/(1 + e cosW)
 * with respect to the angle W to get an expression for the
 * area swept out by the radius vector.  The area is given
 * by the mean anomaly; the angle is solved numerically.
 * 
 * The answer is obtained in two steps.  We first solve
 * Kepler's equation
 *    M = E - eccent*sin(E)
 * for the eccentric anomaly E.  Then there is a
 * closed form solution for W in terms of E.
 */

E = M; /* Initial guess is same as circular orbit. */
temp = 1.0;
do
	{
/* The approximate area swept out in the ellipse */
	temp = E - eccent * sin(E)
/* ...minus the area swept out in the circle */
		- M;
/* ...should be zero.  Use the derivative of the error
 * to converge to solution by Newton's method.
 */
	E -= temp/(1.0 - eccent*cos(E));
	}
while( fabs(temp) > 1.0e-11 );

/* The exact formula for the area in the ellipse is
 *    2.0*atan(c2*tan(0.5*W)) - c1*eccent*sin(W)/(1+e*cos(W))
 * where
 *    c1 = sqrt( 1.0 - eccent*eccent )
 *    c2 = sqrt( (1.0-eccent)/(1.0+eccent) ).
 * Substituting the following value of W
 * yields the exact solution.
 */
temp = sqrt( (1.0+eccent)/(1.0-eccent) );
v = 2.0 * atan( temp * tan(0.5*E) );

/* The true anomaly.
 */
v = modtp(v);

meananomaly *= DTR;
/* Orbital longitude measured from node
 * (argument of latitude)
 */
if( e->L )
	alat = (e->L)*DTR + v - meananomaly - ascnode;
else
	alat = v + argperih; /* mean longitude not given */

/* From the equation of the ellipse, get the
 * radius from central focus to the object.
 */
cosv = cos( v );
r = meandistance*(1.0-eccent*eccent)/(1.0+eccent*cosv);

parabcon:

/* The heliocentric ecliptic longitude of the object
 * is given by
 *   tan( longitude - ascnode )  =  cos( inclination ) * tan( alat ).
 */
coso = cos( alat );
sino = sin( alat );
W = sino * cos( inclination );
E = zatan2( coso, W ) + ascnode;

/* The ecliptic latitude of the object
 */
sinW = sino * sin( inclination );
W = asin(sinW);

#if !DEPOLYN
kepdon:
/* Apply perturbations, if a program is supplied.
 */
 if( e->celmnt ) {
   printf("celmnt not supported\n");
   exit(1); 
   /*
	e->L = E;
	e->r = r;
	(*(e->celmnt))(e);
	E = e->L;
	r = e->r;
	W += e->plat;
   */
 }

/* If earth, Adjust from earth-moon barycenter to earth
 * by AA page E2
 * unless orbital elements are calculated by formula.
 * (The Meeus perturbation formulas include this term for the moon.)
 */
if( (e == &earth) && (e->oelmnt == 0) )
	{
	temp = (J-2451545.0)/36525.0;
	temp = DTR*(298. + 445267.*temp); /* elongation of Moon from Sun */
	r += 3.076e-5 * cos(temp); /* au */
	E += 3.12e-5 * sin(temp); /* radians */
/*  same operation on rectangular coordinates:
	temp = DTR*(218. + 481268.*temp);
	rect[0] -= 3.12e-5*cos(temp);
	rect[1] -= 3.12e-5*sin(temp);
*/
	}
sinW = sin(W);
#endif

/* Output the polar cooordinates
 */
polar[0] = E; /* longitude */
polar[1] = W; /* latitude */
polar[2] = r; /* radius */


/* Convert to rectangular coordinates,
 * using the perturbed latitude.
 */
rect[2] = r * sinW;
cosa = cos(W);
rect[1] = r * cosa * sin(E);
rect[0] = r * cosa * cos(E);

/* Convert from heliocentric ecliptic rectangular
 * to heliocentric equatorial rectangular coordinates
 * by rotating eps radians about the x axis.
 */
epsiln( e->equinox );
W = coseps*rect[1] - sineps*rect[2];
M = sineps*rect[1] + coseps*rect[2];
rect[1] = W;
rect[2] = M;

/* Compute the velocity vector
 * from the orbital parameters
 */
coso = cos( ascnode );
sino = sin( ascnode );
cosa = cos( argperih );
sina = sin( argperih );
temp = cos( inclination );
cosv = r * cos(v);
sinv = r * sin(v);
vobjh[0] = (coso * cosa - sino * sina * temp ) * cosv
		- ( coso * sina + sino * cosa * temp ) * sinv;
vobjh[1] = ( sino * cosa + coso * sina * temp ) * cosv
		- ( sino * sina - coso * cosa * temp ) * sinv;
temp = sin( inclination );
vobjh[2] = sina * temp * cosv + cosa * temp * sinv;

/* Precess the position
 * to ecliptic and equinox of J2000.0
 * if not already there.
 */
precess( rect, e->equinox, 1 );
precess( vobjh, e->equinox, 1 );

for( k=0; k<3; k++ )
	{
	M = rect[k];
	pobjb[k] = M;
	pobjh[k] = M;
	vobjb[k] = vobjh[k];
	}
return(0);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// epsiln.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* Obliquity of the ecliptic at Julian date J  */

#if (DE403 | DE404 | DE405 | DE406 | DE406CD | LIB403)
#define WILLIAMS 1
#define SIMON 0
#else
#if (DE200 | DE200CD | DE102 | SSYSTEM)
#define WILLIAMS 0
#define SIMON 0
#else
#define WILLIAMS 0
#define SIMON 1
#endif
#endif
/* J. L. Simon, P. Bretagnon, J. Chapront, M. Chapront-Touze', G. Francou,
   and J. Laskar, "Numerical Expressions for precession formulae and
   mean elements for the Moon and the planets," Astronomy and Astrophysics
   282, 663-683 (1994)  */

/* IAU Coefficients are from:
 * J. H. Lieske, T. Lederle, W. Fricke, and B. Morando,
 * "Expressions for the Precession Quantities Based upon the IAU
 * (1976) System of Astronomical Constants,"  Astronomy and Astrophysics
 * 58, 1-16 (1977).
 *
 * Before or after 200 years from J2000, the formula used is from:
 * J. Laskar, "Secular terms of classical planetary theories
 * using the results of general theory," Astronomy and Astrophysics
 * 157, 59070 (1986).
 *
 *  See precess.c and page B18 of the Astronomical Almanac.
 * 
 */

#if __STDC__
double sin (double);
double cos (double);
double fabs (double);
#else
double sin(), cos(), fabs();
#endif

/* The results of the program are returned in these
 * global variables:
 */
double jdeps = -1.0; /* Date for which obliquity was last computed */
double eps = 0.0; /* The computed obliquity in radians */
double coseps = 0.0; /* Cosine of the obliquity */
double sineps = 0.0; /* Sine of the obliquity */
//extern double eps, coseps, sineps, STR;

int epsiln(double J) /* Julian date input */
{
double T;

if( J == jdeps )
	return(0);

#if WILLIAMS
/* DE403 */
	T = (J - 2451545.0)/365250.0;
	eps = ((((((((( 2.45e-10*T + 5.79e-9)*T + 2.787e-7)*T
        + 7.12e-7)*T - 3.905e-5)*T - 2.4967e-3)*T
	- 5.138e-3)*T + 1.9989)*T - 0.0175)*T - 468.33960)*T
	+ 84381.406173;
#else
#if SIMON
	T = (J - 2451545.0)/365250.0;
	eps = ((((((((( 2.45e-10*T + 5.79e-9)*T + 2.787e-7)*T
        + 7.12e-7)*T - 3.905e-5)*T - 2.4967e-3)*T
	- 5.138e-3)*T + 1.9989)*T - 0.0152)*T - 468.0927)*T
	+ 84381.412;
#else
/* This expansion is from the AA.
 * Note the official 1976 IAU number is 23d 26' 21.448", but
 * the JPL numerical integration found 21.4119".
 */
T = (J - 2451545.0)/36525.0;
if( fabs(T) < 2.0 )
	{
	eps = ((1.813e-3*T - 5.9e-4)*T - 46.8150)*T + 84381.448;
	}
/* This expansion is from Laskar, cited above.
 * Bretagnon and Simon say, in Planetary Programs and Tables, that it
 * is accurate to 0.1" over a span of 6000 years. Laskar estimates the
 * precision to be 0.01" after 1000 years and a few seconds of arc
 * after 10000 years.
 */
else
	{
	T /= 10.0;
	eps = ((((((((( 2.45e-10*T + 5.79e-9)*T + 2.787e-7)*T
        + 7.12e-7)*T - 3.905e-5)*T - 2.4967e-3)*T
	- 5.138e-3)*T + 1.99925)*T - 0.0155)*T - 468.093)*T
	+ 84381.448;
	}
#endif /* not SIMON */
#endif /* not WILLIAMS */
eps *= STR;
coseps = cos( eps );
sineps = sin( eps );
jdeps = J;
return(0);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// precess.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* Precession of the equinox and ecliptic
 * from epoch Julian date J to or from J2000.0
 *
 * Program by Steve Moshier.  */

#if (DE200CD | DE200 | DE102 | SSYSTEM)
#define IAU 1
#endif

#define WILLIAMS 1
/* James G. Williams, "Contributions to the Earth's obliquity rate,
   precession, and nutation,"  Astron. J. 108, 711-724 (1994)  */

#define SIMON 0
/* J. L. Simon, P. Bretagnon, J. Chapront, M. Chapront-Touze', G. Francou,
   and J. Laskar, "Numerical Expressions for precession formulae and
   mean elements for the Moon and the planets," Astronomy and Astrophysics
   282, 663-683 (1994)  */

#ifndef IAU
#define IAU 0
#endif
/* IAU Coefficients are from:
 * J. H. Lieske, T. Lederle, W. Fricke, and B. Morando,
 * "Expressions for the Precession Quantities Based upon the IAU
 * (1976) System of Astronomical Constants,"  Astronomy and
 * Astrophysics 58, 1-16 (1977).
 */

#define LASKAR 0
/* Newer formulas that cover a much longer time span are from:
 * J. Laskar, "Secular terms of classical planetary theories
 * using the results of general theory," Astronomy and Astrophysics
 * 157, 59070 (1986).
 *
 * See also:
 * P. Bretagnon and G. Francou, "Planetary theories in rectangular
 * and spherical variables. VSOP87 solutions," Astronomy and
 * Astrophysics 202, 309-315 (1988).
 *
 * Laskar's expansions are said by Bretagnon and Francou
 * to have "a precision of about 1" over 10000 years before
 * and after J2000.0 in so far as the precession constants p^0_A
 * and epsilon^0_A are perfectly known."
 *
 * Bretagnon and Francou's expansions for the node and inclination
 * of the ecliptic were derived from Laskar's data but were truncated
 * after the term in T**6. I have recomputed these expansions from
 * Laskar's data, retaining powers up to T**10 in the result.
 *
 * The following table indicates the differences between the result
 * of the IAU formula and Laskar's formula using four different test
 * vectors, checking at J2000 plus and minus the indicated number
 * of years.
 *
 *   Years       Arc
 * from J2000  Seconds
 * ----------  -------
 *        0	  0
 *      100	.006	
 *      200     .006
 *      500     .015
 *     1000     .28
 *     2000    6.4
 *     3000   38.
 *    10000 9400.
 */

#if __STDC__
double cos (double);
double sin (double);
double fabs (double);
int epsiln (double);
#else
double cos(), sin(), fabs();
extern int epsiln();
#endif
#define COS cos
#define SIN sin
extern double J2000; /* = 2451545.0, 2000 January 1.5 */
extern double STR; /* = 4.8481368110953599359e-6 radians per arc second */
extern double coseps, sineps; /* see epsiln.c */
extern int epsiln();

/* In WILLIAMS and SIMON, Laskar's terms of order higher than t^4
   have been retained, because Simon et al mention that the solution
   is the same except for the lower order terms.  */
#if WILLIAMS
static double pAcof[] = {
#if (DE403 | LIB403 | DE404 | DE405 | DE406 | DE406CD )
 -8.66e-10, -4.759e-8, 2.424e-7, 1.3095e-5, 1.7451e-4, -1.8055e-3,
 -0.235316, 0.076, 110.5414, 50287.91959
#else
 -8.66e-10, -4.759e-8, 2.424e-7, 1.3095e-5, 1.7451e-4, -1.8055e-3,
 -0.235316, 0.076, 110.5407, 50287.70000
#endif /* not DE403 */
 };
#endif
#if SIMON
/* Precession coefficients from Simon et al: */
static double pAcof[] = {
 -8.66e-10, -4.759e-8, 2.424e-7, 1.3095e-5, 1.7451e-4, -1.8055e-3,
 -0.235316, 0.07732, 111.2022, 50288.200 };
#endif
#if LASKAR
/* Precession coefficients taken from Laskar's paper: */
static double pAcof[] = {
 -8.66e-10, -4.759e-8, 2.424e-7, 1.3095e-5, 1.7451e-4, -1.8055e-3,
 -0.235316, 0.07732, 111.1971, 50290.966 };
#endif
#if WILLIAMS
/* Pi from Williams' 1994 paper, in radians.  No change in DE403.  */
static double nodecof[] = {
6.6402e-16, -2.69151e-15, -1.547021e-12, 7.521313e-12, 1.9e-10, 
-3.54e-9, -1.8103e-7,  1.26e-7,  7.436169e-5,
-0.04207794833,  3.052115282424};
/* pi from Williams' 1994 paper, in radians.  No change in DE403.  */
static double inclcof[] = {
1.2147e-16, 7.3759e-17, -8.26287e-14, 2.503410e-13, 2.4650839e-11, 
-5.4000441e-11, 1.32115526e-9, -6.012e-7, -1.62442e-5,
 0.00227850649, 0.0 };
#endif
#if SIMON
static double nodecof[] = {
6.6402e-16, -2.69151e-15, -1.547021e-12, 7.521313e-12, 1.9e-10, 
-3.54e-9, -1.8103e-7, 2.579e-8, 7.4379679e-5,
-0.0420782900, 3.0521126906};

static double inclcof[] = {
1.2147e-16, 7.3759e-17, -8.26287e-14, 2.503410e-13, 2.4650839e-11, 
-5.4000441e-11, 1.32115526e-9, -5.99908e-7, -1.624383e-5,
 0.002278492868, 0.0 };
#endif
#if LASKAR
/* Node and inclination of the earth's orbit computed from
 * Laskar's data as done in Bretagnon and Francou's paper.
 * Units are radians.
 */
static double nodecof[] = {
6.6402e-16, -2.69151e-15, -1.547021e-12, 7.521313e-12, 6.3190131e-10, 
-3.48388152e-9, -1.813065896e-7, 2.75036225e-8, 7.4394531426e-5,
-0.042078604317, 3.052112654975 };

static double inclcof[] = {
1.2147e-16, 7.3759e-17, -8.26287e-14, 2.503410e-13, 2.4650839e-11, 
-5.4000441e-11, 1.32115526e-9, -5.998737027e-7, -1.6242797091e-5,
 0.002278495537, 0.0 };
#endif


/* Subroutine arguments:
 *
 * R = rectangular equatorial coordinate vector to be precessed.
 *     The result is written back into the input vector.
 * J = Julian date
 * direction =
 *      Precess from J to J2000: direction = 1
 *      Precess from J2000 to J: direction = -1
 * Note that if you want to precess from J1 to J2, you would
 * first go from J1 to J2000, then call the program again
 * to go from J2000 to J2.
 */

int precess( double R[], double J, int direction)
{
double A, B, T, pA, W, z, TH;
double x[3];
double *p;
int i;
#if IAU
double sinth, costh, sinZ, cosZ, sinz, cosz, Z;
#endif

if( J == J2000 )
	return(0);
/* Each precession angle is specified by a polynomial in
 * T = Julian centuries from J2000.0.  See AA page B18.
 */
T = (J - J2000)/36525.0;

#if IAU
/* Use IAU formula only for a few centuries, if at all.  */
if( fabs(T) > 2.0 )
	goto laskar;

Z =  (( 0.017998*T + 0.30188)*T + 2306.2181)*T*STR;
z =  (( 0.018203*T + 1.09468)*T + 2306.2181)*T*STR;
TH = ((-0.041833*T - 0.42665)*T + 2004.3109)*T*STR;

sinth = SIN(TH);
costh = COS(TH);
sinZ = SIN(Z);
cosZ = COS(Z);
sinz = SIN(z);
cosz = COS(z);
A = cosZ*costh;
B = sinZ*costh;

if( direction < 0 )
	{ /* From J2000.0 to J */
	x[0] =    (A*cosz - sinZ*sinz)*R[0]
	        - (B*cosz + cosZ*sinz)*R[1]
	                  - sinth*cosz*R[2];

	x[1] =    (A*sinz + sinZ*cosz)*R[0]
	        - (B*sinz - cosZ*cosz)*R[1]
	                  - sinth*sinz*R[2];

	x[2] =              cosZ*sinth*R[0]
	                  - sinZ*sinth*R[1]
	                       + costh*R[2];
	}
else
	{ /* From J to J2000.0 */
	x[0] =    (A*cosz - sinZ*sinz)*R[0]
	        + (A*sinz + sinZ*cosz)*R[1]
	                  + cosZ*sinth*R[2];

	x[1] =   -(B*cosz + cosZ*sinz)*R[0]
	        - (B*sinz - cosZ*cosz)*R[1]
	                  - sinZ*sinth*R[2];

	x[2] =             -sinth*cosz*R[0]
	                  - sinth*sinz*R[1]
	                       + costh*R[2];
	}	
goto done;

laskar:
#endif /* IAU */

/* Implementation by elementary rotations. */

/* Obliquity of the equator at initial date.  */
if( direction == 1 )
	epsiln( J ); /* To J2000 */
else
	epsiln( J2000 ); /* From J2000 */

/* Precession in longitude
 */
T /= 10.0; /* thousands of years */
p = pAcof;
pA = *p++;
for( i=0; i<9; i++ )
	pA = pA * T + *p++;
pA *= STR * T;

/* Node of the moving ecliptic on the J2000 ecliptic.
 */
p = nodecof;
W = *p++;
for( i=0; i<10; i++ )
	W = W * T + *p++;

/* Inclination of the ecliptic of date to the J2000 ecliptic.  */
p = inclcof;
TH = *p++;
for( i=0; i<10; i++ )
	TH = TH * T + *p++;

/* First rotate about the x axis from the initial equator
 * to the initial ecliptic. (The input is equatorial.)
 */
x[0] = R[0];
z = coseps*R[1] + sineps*R[2];
x[2] = -sineps*R[1] + coseps*R[2];
x[1] = z;

/* Rotate about z axis to the node.
 */
if( direction == 1 )
	z = W + pA;
else
	z = W;
B = COS(z);
A = SIN(z);
z = B * x[0] + A * x[1];
x[1] = -A * x[0] + B * x[1];
x[0] = z;

/* Rotate about new x axis by the inclination of the moving
 * ecliptic on the J2000 ecliptic.
 */
if( direction == 1 )
	TH = -TH;
B = COS(TH);
A = SIN(TH);
z = B * x[1] + A * x[2];
x[2] = -A * x[1] + B * x[2];
x[1] = z;

/* Rotate about new z axis back from the node.
 */
if( direction == 1 )
	z = -W;
else
	z = -W - pA;
B = COS(z);
A = SIN(z);
z = B * x[0] + A * x[1];
x[1] = -A * x[0] + B * x[1];
x[0] = z;

/* Rotate about x axis to final equator.
 */
if( direction == 1 )
	epsiln( J2000 );
else
	epsiln( J );
z = coseps * x[1] - sineps * x[2];
x[2] = sineps * x[1] + coseps * x[2];
x[1] = z;

#if IAU
done:
#endif

for( i=0; i<3; i++ )
	R[i] = x[i];
return(0);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// lonlat.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* Display ecliptic longitude and latitude (for
 * equinox of date, if ofdate nonzero).
 * Input pp is equatorial rectangular
 * coordinates for equinox J2000.
 */

int lonlat( double pp[], double J, double polar[], int ofdate)
{
double s[3], x, y, z, yy, zz, r;
int i;

/* Make local copy of position vector
 * and calculate radius.
 */
r = 0.0;
for( i=0; i<3; i++ )
	{
	x = pp[i];
	s[i] = x;
	r += x * x;
	}
r = sqrt(r);

/* Precess to equinox of date J
 * if flag is set
 */
if( ofdate )
	{
	precess( s, J, -1 );
	epsiln(J);
	}
else
	epsiln(J2000);

/* Convert from equatorial to ecliptic coordinates
 */
yy = s[1];
zz = s[2];
x  = s[0];
y  =  coseps * yy  +  sineps * zz;
z  = -sineps * yy  +  coseps * zz;

yy = zatan2( x, y );
zz = asin( z/r );

polar[0] = yy;
polar[1] = zz;
polar[2] = r;

if( prtflg == 0 )
	return(0);

//printf( "ecliptic long" );
// dms( yy );
// printf( " lat" );
// dms( zz );
// printf( " rad %.9E\n", r );
return(0);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// zatan2.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/*							atan2()
 *
 *	Quadrant correct inverse circular tangent
 *
 *
 *
 * SYNOPSIS:
 *
 * double x, y, z, atan2();
 *
 * z = atan2( x, y );
 *
 *
 *
 * DESCRIPTION:
 *
 * Returns radian angle between 0 and +2pi whose tangent
 * is y/x.
 *
 *
 *
 * ACCURACY:
 *
 * See atan.c.
 *
 */


/*
Cephes Math Library Release 2.0:  April, 1987
Copyright 1984, 1987 by Stephen L. Moshier
Direct inquiries to 30 Frost Street, Cambridge, MA 02140
Certain routines from the Library, including this one, may
be used and distributed freely provided this notice is retained
and source code is included with all distributions.
*/


//extern double PI;
#if __STDC__
double atan (double);
#else
double atan();
#endif

double zatan2(double x, double y)
{
double z, w;
short code;


code = 0;

if( x < 0.0 )
	code = 2;
if( y < 0.0 )
	code |= 1;

if( x == 0.0 )
	{
	if( code & 1 )
		return( 1.5*PI );
	if( y == 0.0 )
		return( 0.0 );
	return( 0.5*PI );
	}

if( y == 0.0 )
	{
	if( code & 2 )
		return( PI );
	return( 0.0 );
	}


switch( code )
	{
	default:
	case 0: w = 0.0; break;
	case 1: w = 2.0 * PI; break;
	case 2:
	case 3: w = PI; break;
	}

z = atan( y/x );

return( w + z );
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// dms.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#if __STDC__
double caltoj (long, int, double);
#else
double caltoj();
#endif

char *intfmt = "%d";
char *lngfmt = "%ld";
char *dblfmt = "%lf";
char *strfmt = "%s";

/* Display Right Ascension and Declination
 * from input equatorial rectangular unit vector.
 * Output vector pol[] contains R.A., Dec., and radius.
 */
int showrd(char *msg, double p[], double pol[] )
{
double x, y, r;
int i;

r = 0.0;
for( i=0; i<3; i++ )
	{
	x = p[i];
	r += x * x;
	}
r = sqrt(r);

x = zatan2( p[0], p[1] );
pol[0] = x;

y = asin( p[2]/r );
pol[1] = y;

pol[2] = r;

if (prtflg != 0)
  {
//     printf( "%s  R.A. ", msg );
//     hms( x );
//     printf( "Dec. " );
//     dms( y );
//     printf( "\n" );
  }
return(0);
}




/* Display magnitude of correction vector
 * in arc seconds
 */
int showcor(char *strng, double p[], double dp[] )
{
double p1[3], dr, dd;
int i;

if( prtflg == 0 )
	return(0);

for( i=0; i<3; i++ )
	p1[i] = p[i] + dp[i];

deltap( p, p1, &dr, &dd );
//printf( "%s dRA %.3fs dDec %.2f\"\n", strng, RTS*dr/15.0, RTS*dd );
return(0);
}



/* Radians to degrees, minutes, seconds
 */
int dms(double x)
{
double s;
int d, m;

s = x * RTD;
if( s < 0.0 )
	{
	printf( " -" );
	s = -s;
	}
else
	{
	printf( "  " );
	}
d = (int) s;
s -= d;
s *= 60;
m = (int) s;
s -= m;
s *= 60;
printf( "%3dd %02d\' %05.2f\"  ", d, m, s );
return(0);
}




/* Radians to hours, minutes, seconds
 */
#define RTOH (12.0/PI)

int hms(double x)
{
int h, m;
long sint, sfrac;
double s;

s = x * RTOH;
if( s < 0.0 )
	s += 24.0;
h = (int) s;
s -= h;
s *= 60;
m = (int) s;
s -= m;
s *= 60;
/* Handle shillings and pence roundoff. */
sfrac = (long) (1000.0 * s + 0.5);
if( sfrac >= 60000L )
  {
    sfrac -= 60000L;
    m += 1;
    if( m >= 60 )
      {
	m -= 60;
	h += 1;
      }
  }
sint = sfrac / 1000;
sfrac -= sint * 1000;
printf( "%3dh %02dm %02ld.%03lds  ", h, m, sint, sfrac );
return(0);
}

/*		julian.c
 *
 * This program calculates Julian day number from calendar
 * date, and the date and day of the week from Julian day.
 * The Julian date is double precision floating point
 * with the origin used by astronomers.  The calendar output
 * converts fractions of a day into hours, minutes, and seconds.
 * There is no year 0.  Enter B.C. years as negative; i.e.,
 * 2 B.C. = -2.
 *
 * The approximate range of dates handled is 4713 B.C. to
 * 54,078 A.D.  This should be adequate for most applications.
 *
 * B.C. dates are calculated by extending the Gregorian sequence
 * of leap years and century years into the past.  This seems
 * the only sensible definition, but I don't know if it is
 * the official one.
 *
 * Note that the astronomical Julian day starts at noon on
 * the previous calendar day.  Thus at midnight in the morning
 * of the present calendar day the Julian date ends in .5;
 * it rolls over to tomorrow at noon today.
 *
 * The month finding algorithm is attributed to Meeus.
 *
 * - Steve Moshier
 */


char *months[12] = {
"January",
"February",
"March",
"April",
"May",
"June",
"July",
"August",
"September",
"October",
"November",
"December"
};

char *days[7] = {
"Sunday",
"Monday",
"Tuesday",
"Wednesday",
"Thursday",
"Friday",
"Saturday"
};

long cyear = 1986L;
extern long cyear;
static int month = 1;
static double day = 1.0;
short yerend = 0;
extern short yerend;

double zgetdate()
{
double J;

/* Get operator to type in a date.
 */
getnum( "Calendar date: Year", &cyear, lngfmt );

if( (cyear > 53994L) || (cyear < -4713L) )
	{
	printf( "Year out of range.\n" );
	goto err;
	}
if( cyear == 0 )
	{
	printf( "There is no year 0.\n" );
err:	J = 0.0;
	goto pdate;
	}

getnum( "Month (1-12)", &month, intfmt);
getnum( "Day.fraction", &day, dblfmt );

/* Find the Julian day. */
J = caltoj(cyear,month,day);
/*printf( "Julian day %.1f\n", J );*/

pdate:
/* Convert back to calendar date. */
/* jtocal( J ); */
return(J);
}



/*     Calculate Julian day from Gregorian calendar date
 */
double caltoj(long year, int month, double day )
{
long y, a, b, c, e, m;
double J;


/* The origin should be chosen to be a century year
 * that is also a leap year.  We pick 4801 B.C.
 */
y = year + 4800;
if( year < 0 )
	{
	y += 1;
	}

/* The following magic arithmetic calculates a sequence
 * whose successive terms differ by the correct number of
 * days per calendar month.  It starts at 122 = March; January
 * and February come after December.
 */
m = month;
if( m <= 2 )
	{
	m += 12;
	y -= 1;
	}
e = (306 * (m+1))/10;

a = y/100;	/* number of centuries */
if( year <= 1582L )
	{
	if( year == 1582L )
		{
		if( month < 10 )
			goto julius;
		if( month > 10)
			goto gregor;
		if( day >= 15 )
			goto gregor;
		}
julius:
	printf( " Julian Calendar assumed.\n" );
	b = -38;
	}
else
	{ /* -number of century years that are not leap years */
gregor:
	b = (a/4) - a;
	}

c = (36525L * y)/100; /* Julian calendar years and leap years */

/* Add up these terms, plus offset from J 0 to 1 Jan 4801 B.C.
 * Also fudge for the 122 days from the month algorithm.
 */
J = b + c + e + day - 32167.5;
return( J );
}

/* Calculate month, day, and year from Julian date */

int jtocal(double J)
{
int month, day;
long year, a, c, d, x, y, jd;
int BC;
double dd;

if( J < 1721425.5 ) /* January 1.0, 1 A.D. */
	BC = 1;
else
	BC = 0;

jd = (long) (J + 0.5); /* round Julian date up to integer */

/* Find the number of Gregorian centuries
 * since March 1, 4801 B.C.
 */
a = (100*jd + 3204500L)/3652425L;

/* Transform to Julian calendar by adding in Gregorian century years
 * that are not leap years.
 * Subtract 97 days to shift origin of JD to March 1.
 * Add 122 days for magic arithmetic algorithm.
 * Add four years to ensure the first leap year is detected.
 */
c = jd + 1486;
if( jd >= 2299160.5 )
	c += a - a/4;
else
	c += 38;
/* Offset 122 days, which is where the magic arithmetic
 * month formula sequence starts (March 1 = 4 * 30.6 = 122.4).
 */
d = (100*c - 12210L)/36525L;
/* Days in that many whole Julian years */
x = (36525L * d)/100L;

/* Find month and day. */
y = ((c-x)*100L)/3061L;
day = (int) (c - x - ((306L*y)/10L));
month = (int) (y - 1);
if( y > 13 )
	month -= 12;

/* Get the year right. */
year = d - 4715;
if( month > 2 )
	year -= 1;

/* Day of the week. */
a = (jd + 1) % 7;

/* Fractional part of day. */
dd = day + J - jd + 0.5;

/* post the year. */
cyear = year;

if( BC )
	{
	year = -year + 1;
	cyear = -year;
	if( prtflg )
		printf( "%ld B.C. ", year );
	}
else
	{
	if( prtflg )
		printf( "%ld ", year );
	}
day = (int) dd;
if( prtflg )
  printf( "%s %d %s", months[month-1], day, days[(int) a] );

/* Flag last or first day of year */
if( ((month == 1) && (day == 1))
	|| ((month == 12) && (day == 31)) )
	yerend = 1;
else
	yerend = 0;

/* Display fraction of calendar day
 * as clock time.
 */
a = (long) dd;
dd = dd - a;
if( prtflg )
	{
	hms( 2.0*PI*dd );
	if( J == TDT )
		printf( "TDT\n" ); /* Indicate Terrestrial Dynamical Time */
	else if( J == UT )
		printf( "UT\n" ); /* Universal Time */
	else
		printf( "\n" );
	}
return(0);
}


/* Reduce x modulo 360 degrees
 */
double mod360(double x)
{
long k;
double y;

k = (long) (x/360.0);
y = x  -  k * 360.0;
while( y < 0.0 )
	y += 360.0;
while( y > 360.0 )
	y -= 360.0;
return(y);
}




/* Reduce x modulo 2 pi
 */
#define TPI (2.0*PI)
double modtp(double x)
{
double y;

y = floor( x/TPI );
y = x - y * TPI;
while( y < 0.0 )
	y += TPI;
while( y >= TPI )
	y -= TPI;
return(y);
}


/* Get operator to type in hours, minutes, and seconds
 */
static int hours = 0;
static int minutes = 0;
static double seconds = 0.0;

double gethms()
{
double t;

getnum( "Time: Hours", &hours, intfmt );
getnum( "Minutes", &minutes, intfmt );
getnum( "Seconds", &seconds, dblfmt );
t = (3600.0*hours + 60.0*minutes + seconds)/86400.0;
return(t);
}

int getnum(char *msg, void *num, char *format )
{
  printf("all getnum calls to gets() commented out\n");
  exit(1);

char s[40];

printf( "%s (", msg );
if( format == strfmt )
	printf( format, (char *) num );
else if( format == dblfmt )
	printf( format, *(double *)num );
else if( format == intfmt )
	printf( format, *(int *)num );
else if( format == lngfmt )
	printf( format, *(long *)num );
else
	printf( "Illegal input format\n"  );
printf( ") ? ");
//gets(s);
if( s[0] != '\0' )
	sscanf( s, format, num );
return(0);
}



/*
 * Convert change in rectangular coordinatates to change
 * in right ascension and declination.
 * For changes greater than about 0.1 degree, the
 * coordinates are converted directly to R.A. and Dec.
 * and the results subtracted.  For small changes,
 * the change is calculated to first order by differentiating
 *   tan(R.A.) = y/x
 * to obtain
 *    dR.A./cos**2(R.A.) = dy/x  -  y dx/x**2
 * where
 *    cos**2(R.A.)  =  1/(1 + (y/x)**2).
 *
 * The change in declination arcsin(z/R) is
 *   d asin(u) = du/sqrt(1-u**2)
 *   where u = z/R.
 *
 * p0 is the initial object - earth vector and
 * p1 is the vector after motion or aberration.
 *
 */

int deltap( double p0[], double p1[], double *dr, double *dd)
{
double dp[3], A, B, P, Q, x, y, z;
int i;

P = 0.0;
Q = 0.0;
z = 0.0;
for( i=0; i<3; i++ )
	{
	x = p0[i];
	y = p1[i];
	P += x * x;
	Q += y * y;
	y = y - x;
	dp[i] = y;
	z += y*y;
	}

A = sqrt(P);
B = sqrt(Q);

if( (A < 1.e-7) || (B < 1.e-7) || (z/(P+Q)) > 5.e-7 )
	{
	P = zatan2( p0[0], p0[1] );
	Q = zatan2( p1[0], p1[1] );
	Q = Q - P;
	while( Q < -PI )
		Q += 2.0*PI;
	while( Q > PI )
		Q -= 2.0*PI;
	*dr = Q;
	P = asin( p0[2]/A );
	Q = asin( p1[2]/B );
	*dd = Q - P;
	return(0);
	}


x = p0[0];
y = p0[1];
if( x == 0.0 )
	{
	*dr = 1.0e38;
	}
else
	{
	Q = y/x;
	Q = (dp[1]  -  dp[0]*y/x)/(x * (1.0 + Q*Q));
	*dr = Q;
	}

x = p0[2]/A;
P = sqrt( 1.0 - x*x );
*dd = (p1[2]/B - x)/P;
return(0);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// deltat.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* DeltaT = Ephemeris Time - Universal Time
 *
 * The tabulated values of deltaT, in hundredths of a second,
 * were taken from The Astronomical Almanac, page K8.  The program
 * adjusts for a value of secular tidal acceleration ndot. It is -25.8
 * arcsec per century squared for JPL's DE403 ephemeris.
 * ELP2000 and DE200 use the value -23.8946.
 *
 * The tabulated range is 1620.0 through 2004.0.  Bessel's interpolation
 * formula is implemented to obtain fourth order interpolated values at
 * intermediate times.
 * Updated deltaT predictions can be obtained from this network archive:
 *    ftp://maia.usno.navy.mil/ser7/deltat.preds
 *    ftp://maia.usno.navy.mil/ser7/deltat.data
 *
 * For dates earlier than the tabulated range, the program
 * calculates approximate formulae of Stephenson and Morrison
 * or K. M. Borkowski.  These approximations have an estimated
 * error of 15 minutes at 1500 B.C.  They are not adjusted for small
 * improvements in the current estimate of ndot because the formulas
 * were derived from studies of ancient eclipses and other historical
 * information, whose interpretation depends only partly on ndot.
 *
 * A quadratic extrapolation formula, that agrees in value and slope with
 * current data, predicts future values of deltaT.
 *
 * Input Y is the Julian epoch expressed in Julian years.  Y can be
 * found from the Julian date JD by
 *     Y = 2000.0 + (JD - 2451545.0)/365.25.
 * See AA page B4.
 *
 * Output double deltat(Y) is ET-UT in seconds.
 *
 *
 * References:
 *
 * Stephenson, F. R., and L. V. Morrison, "Long-term changes
 * in the rotation of the Earth: 700 B.C. to A.D. 1980,"
 * Philosophical Transactions of the Royal Society of London
 * Series A 313, 47-70 (1984)
 *
 * Borkowski, K. M., "ELP2000-85 and the Dynamical Time
 * - Universal Time relation," Astronomy and Astrophysics
 * 205, L8-L10 (1988)
 * Borkowski's formula is derived from eclipses going back to 2137 BC
 * and uses lunar position based on tidal coefficient of -23.9 arcsec/cy^2.
 *
 * Chapront-Touze, Michelle, and Jean Chapront, _Lunar Tables
 * and Programs from 4000 B.C. to A.D. 8000_, Willmann-Bell 1991
 * Their table agrees with the one here, but the entries are
 * rounded to the nearest whole second.
 *
 * Stephenson, F. R., and M. A. Houlden, _Atlas of Historical
 * Eclipse Maps_, Cambridge U. Press (1986)
 */

/* If the following number (read from the file aa.ini)
 * is nonzero, then the program will return it
 * and not calculate anything.
 */
double dtgiven = 0.0;
extern double dtgiven;

#define DEMO 0
#define TABSTART 1620
#define TABEND 2011
#define TABSIZ (TABEND - TABSTART + 1)

/* Note, Stephenson and Morrison's table starts at the year 1630.
 * The Chapronts' table does not agree with the Almanac prior to 1630.
 * The actual accuracy decreases rapidly prior to 1780.
 */
short dt[TABSIZ] = {
/* 1620.0 thru 1659.0 */
12400, 11900, 11500, 11000, 10600, 10200, 9800, 9500, 9100, 8800,
8500, 8200, 7900, 7700, 7400, 7200, 7000, 6700, 6500, 6300,
6200, 6000, 5800, 5700, 5500, 5400, 5300, 5100, 5000, 4900,
4800, 4700, 4600, 4500, 4400, 4300, 4200, 4100, 4000, 3800,
/* 1660.0 thru 1699.0 */
3700, 3600, 3500, 3400, 3300, 3200, 3100, 3000, 2800, 2700,
2600, 2500, 2400, 2300, 2200, 2100, 2000, 1900, 1800, 1700,
1600, 1500, 1400, 1400, 1300, 1200, 1200, 1100, 1100, 1000,
1000, 1000, 900, 900, 900, 900, 900, 900, 900, 900,
/* 1700.0 thru 1739.0 */
900, 900, 900, 900, 900, 900, 900, 900, 1000, 1000,
1000, 1000, 1000, 1000, 1000, 1000, 1000, 1100, 1100, 1100,
1100, 1100, 1100, 1100, 1100, 1100, 1100, 1100, 1100, 1100,
1100, 1100, 1100, 1100, 1200, 1200, 1200, 1200, 1200, 1200,
/* 1740.0 thru 1779.0 */
1200, 1200, 1200, 1200, 1300, 1300, 1300, 1300, 1300, 1300,
1300, 1400, 1400, 1400, 1400, 1400, 1400, 1400, 1500, 1500,
1500, 1500, 1500, 1500, 1500, 1600, 1600, 1600, 1600, 1600,
1600, 1600, 1600, 1600, 1600, 1700, 1700, 1700, 1700, 1700,
/* 1780.0 thru 1799.0 */
1700, 1700, 1700, 1700, 1700, 1700, 1700, 1700, 1700, 1700,
1700, 1700, 1600, 1600, 1600, 1600, 1500, 1500, 1400, 1400,
/* 1800.0 thru 1819.0 */
1370, 1340, 1310, 1290, 1270, 1260, 1250, 1250, 1250, 1250,
1250, 1250, 1250, 1250, 1250, 1250, 1250, 1240, 1230, 1220,
/* 1820.0 thru 1859.0 */
1200, 1170, 1140, 1110, 1060, 1020, 960, 910, 860, 800,
750, 700, 660, 630, 600, 580, 570, 560, 560, 560,
570, 580, 590, 610, 620, 630, 650, 660, 680, 690,
710, 720, 730, 740, 750, 760, 770, 770, 780, 780,
/* 1860.0 thru 1899.0 */
788, 782, 754, 697, 640, 602, 541, 410, 292, 182,
161, 10, -102, -128, -269, -324, -364, -454, -471, -511,
-540, -542, -520, -546, -546, -579, -563, -564, -580, -566,
-587, -601, -619, -664, -644, -647, -609, -576, -466, -374,
/* 1900.0 thru 1939.0 */
-272, -154, -2, 124, 264, 386, 537, 614, 775, 913,
1046, 1153, 1336, 1465, 1601, 1720, 1824, 1906, 2025, 2095,
2116, 2225, 2241, 2303, 2349, 2362, 2386, 2449, 2434, 2408,
2402, 2400, 2387, 2395, 2386, 2393, 2373, 2392, 2396, 2402,
/* 1940.0 thru 1979.0 */
 2433, 2483, 2530, 2570, 2624, 2677, 2728, 2778, 2825, 2871,
 2915, 2957, 2997, 3036, 3072, 3107, 3135, 3168, 3218, 3268,
 3315, 3359, 3400, 3447, 3503, 3573, 3654, 3743, 3829, 3920,
 4018, 4117, 4223, 4337, 4449, 4548, 4646, 4752, 4853, 4959,
/* 1980.0 thru 1995.0 */
 5054, 5138, 5217, 5296, 5379, 5434, 5487, 5532, 5582, 5630,
 5686, 5757, 5831, 5912, 5998, 6078,
/* Extrapolated values (USNO) */
                                     6163, 6230, 6297, 6360,
 6480, 6580, 6700, 6800, 6900, 7000, 7100, 7200, 7300, 7400,
 7500, 7600,
};

double deltat(double Y)
{
double ans, p, B;
int d[6];
int i, iy, k;

if( dtgiven != 0.0 )
	return( dtgiven );

if( Y > TABEND )
	{
#if 0
/* Morrison, L. V. and F. R. Stephenson, "Sun and Planetary System"
 * vol 96,73 eds. W. Fricke, G. Teleki, Reidel, Dordrecht (1982)
 */
	B = 0.01*(Y-1800.0) - 0.1;
	ans = -15.0 + 32.5*B*B;
	return(ans);
#else
/* Extrapolate forward by a second-degree curve that agrees with
   the most recent data in value and slope, and vaguely fits
   over the past century.  This idea communicated by Paul Muller,
   who says NASA used to do something like it.  */
	B = Y - TABEND;
	/* slope */
	p = dt[TABSIZ-1] - dt[TABSIZ-2];
	/* square term */
	ans = (dt[TABSIZ - 101] - (dt[TABSIZ - 1] - 100.0 * p)) * 1e-4;
        ans = 0.01 * (dt[TABSIZ-1] + p * B + ans * B * B);
	if( prtflg )
	  printf("[extrapolated deltaT] ");
	return(ans);
#endif
	}

if( Y < TABSTART )
	{
	if( Y >= 948.0 )
		{
/* Stephenson and Morrison, stated domain is 948 to 1600:
 * 25.5(centuries from 1800)^2 - 1.9159(centuries from 1955)^2
 */
		B = 0.01*(Y - 2000.0);
		ans = (23.58 * B + 100.3)*B + 101.6;
		}
	else
		{
/* Borkowski */
		B = 0.01*(Y - 2000.0)  +  3.75;
		ans = 35.0 * B * B  +  40.;
		}
	return(ans);
	}

/* Besselian interpolation from tabulated values.
 * See AA page K11.
 */

/* Index into the table.
 */
p = floor(Y);
iy = (int) (p - TABSTART);
/* Zeroth order estimate is value at start of year
 */
ans = dt[iy];
k = iy + 1;
if( k >= TABSIZ )
	goto done; /* No data, can't go on. */

/* The fraction of tabulation interval
 */
p = Y - p;

/* First order interpolated value
 */
ans += p*(dt[k] - dt[iy]);
if( (iy-1 < 0) || (iy+2 >= TABSIZ) )
	goto done; /* can't do second differences */

/* Make table of first differences
 */
k = iy - 2;
for( i=0; i<5; i++ )
	{
	if( (k < 0) || (k+1 >= TABSIZ) )
		{
		d[i] = 0;
		}
	else
		d[i] = dt[k+1] - dt[k];
	k += 1;
	}

/* Compute second differences
 */
for( i=0; i<4; i++ )
	d[i] = d[i+1] - d[i];
B = 0.25*p*(p-1.0);
ans += B*(d[1] + d[2]);
#if DEMO
printf( "B %.4lf, ans %.4lf\n", B, ans );
#endif
if( iy+2 >= TABSIZ )
	goto done;

/* Compute third differences
 */
for( i=0; i<3; i++ )
	d[i] = d[i+1] - d[i];
B = 2.0*B/3.0;
ans += (p-0.5)*B*d[1];
#if DEMO
printf( "B %.4lf, ans %.4lf\n", B*(p-0.5), ans );
#endif
if( (iy-2 < 0) || (iy+3 > TABSIZ) )
	goto done;

/* Compute fourth differences
 */
for( i=0; i<2; i++ )
	d[i] = d[i+1] - d[i];
B = 0.125*B*(p+1.0)*(p-2.0);
ans += B*(d[0] + d[1]);
#if DEMO
printf( "B %.4lf, ans %.4lf\n", B, ans );
#endif

done:
/* Astronomical Almanac table is corrected by adding the expression
 *     -0.000091 (ndot + 26)(year-1955)^2  seconds
 * to entries prior to 1955 (AA page K8), where ndot is the secular
 * tidal term in the mean motion of the Moon.
 *
 * Entries after 1955 are referred to atomic time standards and
 * are not affected by errors in Lunar or planetary theory.
 */
ans *= 0.01;
if( Y < 1955.0 )
	{
	B = (Y - 1955.0);
#if (DE400 | DE403 | DE404 | DLIB403 | DE405 | DE406 | DE406CD)
	ans += -0.000091 * (-25.8 + 26.0) * B * B;
#else
	ans += -0.000091 * (-23.8946 + 26.0) * B * B;
#endif
	}
return( ans );
}

/* Routine to fill in time values for TDT and UT.
 * These and the input date, JD are global variables.
 * jdflag is set up on reading the initialization file aa.ini.
 */

int update()
{
double T;

/* Convert Julian date to Julian years re J2000.0.
 */
T = 2000.0 + (JD - J2000)/365.25;

switch( jdflag )
	{
	case 0:
		TDT = JD;
		UT = JD;
		break;
	case 1:
		TDT = JD;
		UT = TDT - deltat(T)/86400.0;
		jtocal(UT); /* display the other date */
		break;
	case 2:
		UT = JD;
		TDT = UT + deltat(T)/86400.0;
		jtocal(TDT);
		break;
	}
jtocal(JD);
return(0);
}

/* Exercise program.
 */
/*
#if DEMO
main()
{
char s[20];
double ans, y;

loop:
printf( "year ? " );
gets(s);
sscanf( s, "%lf", &y );
ans = deltat(y);
printf( "%.4lf\n", ans );
goto loop;
}
#endif
*/

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// KFILES.C
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* Disc file input routines to read initialization parameters
 * or file containing orbital elements.
 */
#if __BORLANDC__
#include <stdlib.h>
#endif

#ifdef ANSIPROT
#include <string.h>
#endif

#ifdef _MSC_VER
#if _MSC_VER >= 1000
#include <stdlib.h>
#endif
#include <string.h>
#endif

extern char *intfmt, *strfmt;/* see dms.c */

static char starnam[80] = {'s','t','a','r','.','c','a','t','\0'};
static char orbnam[80] = {'o','r','b','i','t','.','c','a','t','\0'};
static int linenum = 1;
//extern char defile[];
//extern double au;  /* length of astronomical unit  */
//extern double radearth; /* Earth radius in km.  */
//extern double Clight, clight;
//extern double Clightaud;

//extern double tlong, tlat, glat, trho, attemp, atpress, dtgiven;


int kinit()
{
  double a, b, fl, co, si, u;
 FILE *f; //, *fopen();
 char s[84];
 char *inifile;

 printf( "Steve Moshier's Ephemeris Reader v5.4f\n\n" );
#if DE400
 printf( "Planetary positions are from DE400.\n" );
 inifile = "aa400.ini";
#endif
#if DE403
 printf( "Planetary positions are from DE403.\n" );
 inifile = "aa403.ini";
#endif
#if LIB403
 printf( "Librations from long DE404 file.\n" );
 inifile = "lib403.ini";
#endif
#if DE404
 printf( "Planetary positions are from DE404.\n" );
 inifile = "aa404.ini";
#endif
#if DE405
 printf( "Planetary positions are from DE405.\n" );
 inifile = "aa405.ini";
#endif
#if DE406 | DE406CD
 printf( "Planetary positions are from DE406.\n" );
 inifile = "aa406.ini";
#endif
#if DE245
 printf( "Planetary positions are from DE245.\n" );
 inifile = "aa245.ini";
#endif
#if DE200 | DE200CD
 printf( "Planetary positions are from DE200.\n" );
 inifile = "aa200.ini";
#endif
#if DE102
 printf( "Planetary positions are from DE102.\n" );
 inifile = "aa102.ini";
#endif
#if SSYSTEM
 printf( "Planetary positions are from ssystem.exe.\n" );
 inifile = "aa118i.ini";
#endif

 /*
 tlong = ;
 glat = ;
 height = ;
 attemp = ;
 atpress = ;
 jdflag = ;
 dtgiven = ;
 defile = ;
 */

 f = fopen( inifile, "r" );
 
 if( f )
   {
     fgets( s, 80, f );
     sscanf( s, "%lf", &tlong );
     fgets( s, 80, f );
     sscanf( s, "%lf", &glat );
     fgets( s, 80, f );
     sscanf( s, "%lf", &height );
     u = glat * DTR;
     
     /* Reduction from geodetic latitude to geocentric latitude
      * AA page K5
      */
     co = cos(u);
     si = sin(u);
     fl = 1.0 - 1.0/flat;
     fl = fl*fl;
     si = si*si;
     u = 1.0/sqrt( co*co + fl*si );
     a = aearth*u + height;
     b = aearth*fl*u  +  height;
     trho = sqrt( a*a*co*co + b*b*si );
     tlat = RTD * acos( a*co/trho );
     if( glat < 0.0 )
       tlat = -tlat;
     trho /= aearth;
     /* Reduction from geodetic latitude to geocentric latitude
      * AA page K5
      */
     /*
       tlat = glat
       - 0.19242861 * sin(2.0*u)
       + 0.00032314 * sin(4.0*u)
       - 0.00000072 * sin(6.0*u);
       
       trho =    0.998327073
       + 0.001676438 * cos(2.0*u)
       - 0.000003519 * cos(4.0*u)
       + 0.000000008 * cos(6.0*u);
       trho += height/6378160.;
     */
     printf( "Terrestrial east longitude %.4f deg\n", tlong );
     printf( "geocentric latitude %.4f deg\n", tlat );
     printf( "Earth radius %.5f\n", trho );
     
     fgets( s, 80, f );
     sscanf( s, "%lf", &attemp );
     printf( "temperature %.1f C\n", attemp );
     fgets( s, 80, f );
     sscanf( s, "%lf", &atpress );
     printf( "pressure %.0f mb\n", atpress );
     fgets( s, 80, f );
     sscanf( s, "%d", &jdflag );
     switch( jdflag )
       {
       case 0: printf("TDT and UT assumed equal.\n");
	 break;
       case 1: printf("Input time is TDT.\n" );
	 break;
       case 2: printf("Input time is UT.\n" );
	 break;
       default: printf("Illegal jdflag\n" );
	 exit(0);
       }
     fgets( s, 80, f );
     sscanf( s, "%lf", &dtgiven );
     if( dtgiven != 0.0 )
       printf( "Using deltaT = %.2fs.\n", dtgiven );
     fgets( s, 80, f );
     sscanf( s, "%s", &defile[0] ); /* file containing DE data */
     fclose(f);
   }
 ephprint = 0;
 
 /* Calculate speed of light in astronomical units per day.  */
 Clight = clight; /* meters per second */
 Clightaud = Clight * 86400.0 / au;
 /* Equatorial radius of the earth, in au.  */
 Rearthau = radearth / au;
 return(0);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// sun.c
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* Calculate and display apparent coordinates of the sun at the
 * time given by the external variables TDT, UT.
 * Before calling this routine, the geometric position of the
 * earth must be calculated and put in rearth[].
 */

// extern double rearth[];
// extern double pearthb[];
// extern double psunb[];
// extern double coseps, sineps, nutl, dradt, ddecdt;
// extern double Clightaud;

/* Apparent geocentric ra, dec of the Sun.  */
double psunapp[3];

int dosun()
{
double r, x, y, t;
double ecr[3], rec[3], pol[3];
double pearthbT[3], psunbT[3], rsunT[3], rsunL[3];
int i;
double asin(), modtp(), sqrt(), cos(), sin();

/* Display ecliptic longitude and latitude.
 */
for( i=0; i<3; i++ )
	rsunT[i] = -rearth[i];
r = eapolar[2];

if( prtflg )
	{
	lonlat( rsunT, TDT, pol, 1 );
	}

/* Rigorous light time iteration - AA page B39
 */
/* Save current earth and sun coordinates */
for( i=0; i<3; i++ )
  {
    pearthbT[i] = pearthb[i];
    psunbT[i] = psunb[i];
  }
/* Find the earth and sun at time TDT - t */
pol[2] = r;
for( i=0; i<2; i++ )
	{
	t = pol[2]/Clightaud;
	kepler( TDT-t, &earth, rsunL, pol );
	}
r = pol[2];


for( i=0; i<3; i++ )
  {
    rsunL[i] = -rsunL[i];
/* Sun t days ago minus earth now.  */
    x = psunb[i] - pearthbT[i];
    ecr[i] = x;
/* Sun now minus earth now.  */
    y = psunbT[i] - pearthbT[i];
    rec[i] = y;
    pol[i] = y - x; /* change in position */
  }

if( prtflg )
	{
	  //	printf( "light time %.4fm,  ", 1440.0*t );
	//	showcor( "aberration", ecr, pol );
	}

/* Estimate rate of change of RA and Dec
 * for use by altaz().
 */
deltap( rsunL, rsunT, &dradt, &ddecdt );  /* see dms.c */
dradt /= t;
ddecdt /= t;

/* There is no light deflection effect.
 * AA page B39.
 */

/* Annual aberration.  */
for( i=0; i<3; i++ )
    ecr[i] /= r;

annuab(ecr);

/* precess to equinox of date
 */
precess( ecr, TDT, -1 );

for( i=0; i<3; i++ )
    rec[i] = ecr[i];

/* Nutation.
 */
epsiln( TDT );
nutate( TDT, ecr );

ephprint = 1;
//showrd( "    Apparent:", ecr, pol );
ephprint = 0;
for( i=0; i<3; i++ )
  psunapp[i] = pol[i];

/* Show the apparent ecliptic longitude (AA page C2).
   It is the geometric longitude plus nutation in longitude
   plus aberration. */
if( prtflg )
	{
	y  =  coseps * rec[1]  +  sineps * rec[2];
	y = zatan2( rec[0], y ) + nutl;
	//	printf( "Apparent longitude %.6f deg\n", RTD*y );
	}

/* Report altitude and azimuth. Here pol[] is ra and dec.
 */
pol[2] = r;
altaz( pol, UT );
return(0);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// AA200.C
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/* This program calculates orbits of planetary bodies and reduces
 * the coordinates of planets or stars to geocentric and topocentric
 * place.  An effort has been made to use rigorous methods throughout.
 *
 * References to AA page numbers are to The Astronomical Almanac, 1986
 * published by the U.S. Government Printing Office.
 *
 * The version named "aa200" finds planetary, lunar, and solar
 * positions from the DE200 numerical integration of the solar
 * system produced by the Jet Propulsion Laboratory.
 *
 * The program named "aa" uses as a default the orbital perturbations
 * given in Jean Meeus, "Astronomical Formulae for Calculators",
 * 3rd ed.,  Willmann-Bell, Inc., 1985.  It can also be configured
 * to use the tables from Bretagnon and Simon, "Planetary Programs
 * and Tables from -4000 to +2800."
 *
 * Warning! Your atan2() function may not work the same as the one
 * assumed by this program.
 * atan2(x,y) computes atan(y/x), result between 0 and 2pi.
 *
 * S. L. Moshier, September, 1989 (v4.3)
 * November, 1987 (v4.2)
 * November, 1993 (v5.2)
 */




#if __STDC__
#include <stdlib.h>
void librations (void);
void EtoM (double e[], double M[]);
void midentity (double M[]);
void mmpy3 (double A[], double B[], double C[]);
void MtoE (double M[], double e[]);
void mrotx ( double M[], double theta);
#else
void librations();
void EtoM();
void midentity();
void mmpy3();
void MtoE();
void mrotx();
#endif
#ifdef _MSC_VER
#if _MSC_VER > 1000
#include <stdlib.h>
#endif
#endif

/* approximate motion of right ascension and declination
 * of object, in radians per day
 */
double dradt = 0.0;
double ddecdt = 0.0;


/* coordinates of object
 */
int objnum = 0;	/* I.D. number of object */
double robject[3] = {0.0}; /* position */
/* ecliptic polar coordinates:
 * longitude, latitude in radians
 * radius in au
 */
double obpolar[3] = {0.0};

/* coordinates of Earth
 */
/* Heliocentric rectangular equatorial position
 * of the earth at time TDT re equinox J2000
 */
double rearth[3];
/* Corresponding polar coordinates of earth:
 * longitude and latitude in radians, radius in au
 */
double eapolar[3] = {0.0};

/* Julian date of ephemeris
 */
double JD = 0.0;
double TDT = 0.0;
double UT = 0.0;

/* flag = 0 if TDT assumed = UT,
 *      = 1 if input time is TDT,
 *      = 2 if input time is UT.
 */
int jdflag = 0;

/* correction vector, saved for display  */
double dp[3] = {0.0};

/* display formats for printf()
 */
extern char *intfmt, *dblfmt;

/* display enable flag
 */
int prtflg = 1;

/* log file enable flag
 */
int ephprint = 0;

/* Tabulation parameters
 */
static double  jdstart;
static double djd = 1.0;
static int ntab = 1;
static int itab;
struct orbit *elobject;	/* pointer to orbital elements of object */

/* Main program starts here.
 */
int aa_main()
{
  int i;
  
  kinit();

 while (1) {

   prtflg = 1;
   printf( "Enter starting date of tabulation\n" );
   JD = zgetdate(); /* date */
   JD += gethms();	/* time of day */
   update(); /* find UT and ET */
   printf( "Julian day %.7f\n", JD );
   
   getnum( "Enter interval between tabulations in days", &djd, dblfmt );
   getnum( "Number of tabulations to display", &ntab, intfmt );
   if( ntab <= 0 )
     ntab = 1;
   
   printf( "\nThe Sun\n" );
   
   jdstart = JD;
   for (itab=0; itab<ntab; itab++ ) {
     JD = jdstart  +  itab * djd;
     printf( "\nJD %.2f,  ", JD );
     update();
     
     /* Always calculate heliocentric position of the earth */
     kepler( TDT, &earth, rearth, eapolar );
     
     dosun(); 
     iter_trnsit(dosun);
     printf( "\n" );
   }
 }

#if _MSC_VER
 return 0;
#endif
}

//----------------------------------------------------------------------------

// read in ephemerides file once

// year, month, day, hour, minute, second that everything else is relative to

// pacific_timezone = TRUE => time is from California
// pacific_timezone = FALSE => time is from Delaware

void aa_init(long year, int month, double day, int hours, int minutes, double seconds, int pacific_timezone)
{
 printf( "Initializing S. Moshier's Ephemeris Reader v5.4f\n" );
#if DE405
 printf( "Planetary positions are from DE405.\n" );
#else
 printf("only DE405 supported\n");
 exit(1);
#endif

 attemp = 30.0;  // atmospheric temperature, degrees C (celsius?) (was 12.0)
 atpress = 1010; // atmospheric pressure, millibars
 jdflag = 2;  // was 1    // 0: TDT=UT, 1: input=TDT, 2: input=UT
 dtgiven = 0.0;  // Use this deltaT (sec) if nonzero, else compute it.
 // dename is assumed to be unxp2000.405
 
 switch( jdflag ) {
 case 0: printf("TDT and UT assumed equal.\n");
   break;
 case 1: printf("Input time is TDT.\n" );
   break;
 case 2: printf("Input time is UT.\n" );
   break;
 default: printf("Illegal jdflag\n" );
   exit(0);
 }

 ephprint = 0;
 
 /* Calculate speed of light in astronomical units per day.  */
 Clight = clight; /* meters per second */
 Clightaud = Clight * 86400.0 / au;
 /* Equatorial radius of the earth, in au.  */
 Rearthau = radearth / au;

 // time

 prtflg = 1;
 cyear = year;
 JD = caltoj(year,month,day);
 hours = 9;
 minutes = 14;
 seconds = 9;
 double t = (3600.0*hours + 60.0*minutes + seconds)/86400.0;
 JD += t;

 double daylight_adj = 0.0;

 // assuming 2005, daylight savings started on April 3, ends on Oct. 30

 if (month < 4 || (month == 4 && day < 3))
   daylight_adj = 1.0;

 printf("local time: ");
 // printf("%li %i %.2lf, %i:%i:%.2f\n", year, month, day, hours, minutes, seconds);
 jtocal( JD );


 if (pacific_timezone) 
   JD += (daylight_adj + 7.0) / 24.0;  // PST daylight savings time
 else
   JD += (daylight_adj + 4.0) / 24.0;  // EST daylight savings time
   
 printf("GMT time: ");
 jtocal( JD );

 prtflg = 1;

 // JD += 8.0 / 24.0;  // PST standard time

 //   JD = zgetdate(); /* date */
 //JD += gethms();	/* time of day */

//  update(); /* find UT and ET */
//  printf( "Julian day %.7f\n", JD );  
}

//----------------------------------------------------------------------------

// ASSUMES NORTHERN HEMISPHERE!!!

//converts UTM coords to lat/long.  Equations from USGS Bulletin 1532 
//East Longitudes are positive, West longitudes are negative. 
//North latitudes are positive, South latitudes are negative
//Lat and Long are in decimal degrees. 
//Written by Chuck Gantz- chuck.gantz@globalstar.com

void UTMtoLL(double UTMNorthing, double UTMEasting, int ZoneNumber, double *Lat,  double *Long )
{

  double a = WGS_84_EQUATORIAL_RADIUS;
  double eccSquared = WGS_84_SQUARE_OF_ECCENTRICITY;
  double k0 = 0.9996;

  double eccPrimeSquared;
  double e1 = (1-sqrt(1-eccSquared))/(1+sqrt(1-eccSquared));
  double N1, T1, C1, R1, D, M;
  double LongOrigin;
  double mu, phi1, phi1Rad;
  double x, y;
//   int ZoneNumber;
//   char* ZoneLetter;
  int NorthernHemisphere = 1; //1 for northern hemispher, 0 for southern
  double LatRad, LongRad;

  x = UTMEasting - 500000.0; //remove 500,000 meter offset for longitude
  y = UTMNorthing;

//   ZoneNumber = strtoul(UTMZone, &ZoneLetter, 10);
//   if((*ZoneLetter - 'N') >= 0)
//     NorthernHemisphere = 1;//point is in northern hemisphere
//   else
//     {
//       NorthernHemisphere = 0;//point is in southern hemisphere
//       y -= 10000000.0;//remove 10,000,000 meter offset used for southern hemisphere
//     }
  
  LongOrigin = (ZoneNumber - 1)*6 - 180 + 3;  //+3 puts origin in middle of zone
  
  eccPrimeSquared = (eccSquared)/(1-eccSquared);
  
  M = y / k0;
  mu = M/(a*(1-eccSquared/4-3*eccSquared*eccSquared/64-5*eccSquared*eccSquared*eccSquared/256));
  
  phi1Rad = mu	+ (3*e1/2-27*e1*e1*e1/32)*sin(2*mu) 
    + (21*e1*e1/16-55*e1*e1*e1*e1/32)*sin(4*mu)
    +(151*e1*e1*e1/96)*sin(6*mu);
  phi1 = RAD2DEG(phi1Rad);
  
  N1 = a/sqrt(1-eccSquared*sin(phi1Rad)*sin(phi1Rad));
  T1 = tan(phi1Rad)*tan(phi1Rad);
  C1 = eccPrimeSquared*cos(phi1Rad)*cos(phi1Rad);
  R1 = a*(1-eccSquared)/pow(1-eccSquared*sin(phi1Rad)*sin(phi1Rad), 1.5);
  D = x/(N1*k0);
  
  LatRad = phi1Rad - (N1*tan(phi1Rad)/R1)*(D*D/2-(5+3*T1+10*C1-4*C1*C1-9*eccPrimeSquared)*D*D*D*D/24
					+(61+90*T1+298*C1+45*T1*T1-252*eccPrimeSquared-3*C1*C1)*D*D*D*D*D*D/720);
  *Lat = RAD2DEG(LatRad);
  
  LongRad = (D-(1+2*T1+C1)*D*D*D/6+(5-2*C1+28*T1-3*C1*C1+8*eccPrimeSquared+24*T1*T1)
	  *D*D*D*D*D/120)/cos(phi1Rad);
  *Long = LongOrigin + RAD2DEG(LongRad);
}

//----------------------------------------------------------------------------

// new observer position, new time

// ignoring UTM->latlong, time conversion, heading, pitch for now

void aa_update(double northing, double easting, int zone, double altitude, double time)
{
  double a, b, fl, co, si, u;

  // position

  UTMtoLL(northing, easting, zone, &glat, &tlong);
  height = altitude;

  // UD lab

  glat = 39.6823;
  tlong = -75.7485;

  //glat = 42.27; // Geodetic latitude, degrees
  //tlong = -71.13; // Terrestrial east longitude of observer, degrees

  printf("geodetic lat = %lf, long %lf, height = %.2lf\n", glat, tlong, height);


//   height = 0.0; //Height above sea level, meters

  // Reduction from geodetic latitude to geocentric latitude (AA page K5)
  // -- flat, aearth, and trho are global constants

  u = glat * DTR;
  co = cos(u);
  si = sin(u);
  fl = 1.0 - 1.0/flat;
  fl = fl*fl;
  si = si*si;
  u = 1.0/sqrt( co*co + fl*si );
  a = aearth*u + height;
  b = aearth*fl*u  +  height;
  trho = sqrt( a*a*co*co + b*b*si );
  tlat = RTD * acos( a*co/trho );
  if( glat < 0.0 )
    tlat = -tlat;
  trho /= aearth;


  // add relative time to JD

  //  JD += some function of time (convert to fraction of day)
  update(); /* find UT and ET */
  printf( "Julian day %.7f\n", JD );  

  // Calculate heliocentric position of the earth
  kepler( TDT, &earth, rearth, eapolar );
  dosun();
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
