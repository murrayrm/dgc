/**
 * DGCButton.hh
 * Revision History:
 * 07/08/2005  hbarnor  Created
 * $Id: DGCButton.hh 8500 2005-07-09 10:45:45Z hbarnor $
 */

#ifndef DGCBUTTON_HH
#define DGCBUTTON_HH

#include <gtkmm/button.h>
#include "DGCWidget.hh"

using namespace std;
/**
 * Customized Gtk::Button for our the Tab api. 
 * Subclasses Gtk::Button and adds a type-field. The type field is used to
 * determine the type of widget.. This is used mostly for error
 * correction. 
 */

class DGCButton : public DGCWidget, public Gtk::Button
{

public:
  /**
   * Constructor for the button.
   * Contructor for a button with a default text value.
   * @param label - the default text to be displayed
   */
  DGCButton(const Glib::ustring& label);
  
};

#endif
