/* FakeState.cc - new vehicle state estimator
 *
 * JML 31 Dec 04
 *
 * */

#include "FakeState.hh"

FakeState::FakeState(int skynet_key, int k_switch = 1, int imu_switch = 1, int gps_switch = 1, 
		int mag_switch = 1, int verbose_switch = -1)
	: m_skynet(SNastate, skynet_key)
{
	//FInd startting time;
	DGCgettime(starttime);
	DGCgettime(dgctime);
	lastIndex = 0;
	
	// set up the player stuff
	 robot = new PlayerClient("localhost");
	 GPSpos = new GpsProxy(robot, 0, (unsigned char) 'r');

	 /*
	  * The robot position comes from gazebo, through player. 
	  * For now, this is the position of the first instance
	  * of position 3D, as defined in the gazebo config file.
	  *
	  * To correspond to what astate does, this point should
	  * be attached to the front of the center wheels.
	  *
	  */
	 RobotPos = new Position3DProxy(robot, 0, (unsigned char) 'r'); 

	//Initialize structs apropriately:
	
	DGCgettime(vehiclestate.Timestamp);
	vehiclestate.Northing = 0;
	vehiclestate.Easting = 0;
	vehiclestate.Altitude = 0;
	vehiclestate.Vel_N = 0;
	vehiclestate.Vel_E = 0;
	vehiclestate.Vel_D = 0;
	vehiclestate.Acc_N = 0;
	vehiclestate.Acc_E = 0;
	vehiclestate.Acc_D = 0;
	vehiclestate.Roll = 0;
	vehiclestate.Pitch = 0;
	vehiclestate.Yaw = 0;
	vehiclestate.RollAcc = 0;
	vehiclestate.PitchAcc = 0;
	vehiclestate.YawAcc = 0;
	vehiclestate.Timestamp = 0;

	broadcast_statesock = m_skynet.get_send_sock(SNstate);
	if (broadcast_statesock < 0)
		cerr << "FakeState::FakeState(): get_send_sock returned error" << endl;
}

void FakeState::Broadcast()
{
  //cout <<"sending state at "<< vehiclestate.Timestamp << endl;
	if(m_skynet.send_msg(broadcast_statesock,
                       &vehiclestate,
                       sizeof(vehiclestate),
                       0) != sizeof(vehiclestate))
  {
    cerr << "FakeState::Broadcast(): didn't send right size state message" << endl;
	}
}


void FakeState::UpdateState()
{
  robot->SetDataMode(PLAYER_DATAMODE_PUSH_ASYNC);
  robot->Read();
  dgctimeold = dgctime;
  DGCgettime(dgctime);

  timediff = (double) ((dgctime - dgctimeold)/1000000.0);

  // Save the previous value of vehicle state
  struct VehicleState vehiclestate_old;
  vehiclestate_old.Acc_N =  vehiclestate.Acc_N; 
  vehiclestate_old.Acc_E = vehiclestate.Acc_E;
  vehiclestate_old.Acc_D =  vehiclestate.Acc_D;
  vehiclestate_old.RollAcc =   vehiclestate.RollAcc;
  vehiclestate_old.PitchAcc =  vehiclestate.PitchAcc; 
  vehiclestate_old.YawAcc = vehiclestate.YawAcc; 

  // Velocities
  vehiclestate_old.Vel_N = vehiclestate.Vel_N;
  vehiclestate_old.Vel_E = vehiclestate.Vel_E;
  vehiclestate_old.Vel_D = vehiclestate.Vel_D;
  vehiclestate_old.PitchRate =  vehiclestate.PitchRate;
  vehiclestate_old.RollRate = vehiclestate.RollRate;
  vehiclestate_old.YawRate =  vehiclestate.YawRate;
  vehiclestate_old.Northing = vehiclestate.Northing;
  vehiclestate_old.Easting = vehiclestate.Easting;
  vehiclestate_old.Altitude = vehiclestate.Altitude;
  vehiclestate_old.Yaw =  vehiclestate.Yaw;
  vehiclestate_old.Pitch = vehiclestate.Pitch;
  vehiclestate_old.Roll =  vehiclestate.Roll;
  vehiclestate_old.Timestamp =  vehiclestate.Timestamp;

  //New State
  //vehiclestate.Timestamp = (long) (GPSpos->time).tv_usec + (GPSpos->time).tv_sec * 1000000;
  vehiclestate.Timestamp = dgctime;

# ifdef USE_GPS_FOR_POS
  vehiclestate.Northing = GPSpos->utm_northing + Northing_Offset;
  vehiclestate.Easting = GPSpos->utm_easting + Easting_Offset;
  vehiclestate.Altitude =GPSpos->altitude - Z_IMU;
# endif

  /*
   * Grab the vehicle state from gazebo (via player)
   *
   * This requests the position of the origin of Alice.  In gazebo,
   * the simulation initializes to (0,0,0) and so we have to add of
   * the offsets of where the simulation is started in UTM coords.  This
   * is handled through {Northing,Easting}_Offset, which is initialized
   * in FakeState_Main.
   *
   */
  vehiclestate.Northing = Northing_Offset + RobotPos->Ypos();
  vehiclestate.Easting  = Easting_Offset + RobotPos->Xpos();
  vehiclestate.Altitude = -RobotPos->Zpos();

  /*
   * To compute velocities, use the RobotSpeed, but update the sign
   * according to the direction we are heading (XYSpeed() is always > 0)
   *
   */
  double vyaw = M_PI/2 - RobotPos->Yaw();
  vehiclestate.Vel_N = cos(vyaw) > 0 ? RobotPos->YSpeed() : -RobotPos->YSpeed();
  vehiclestate.Vel_E = sin(vyaw) > 0 ? RobotPos->XSpeed() : -RobotPos->XSpeed();
  // vehiclestate.Vel_D = -RobotPos->ZSpeed();

  if (timediff != 0) 
  {
    vehiclestate.Acc_N = 
      (vehiclestate.Vel_N - vehiclestate_old.Vel_N) / timediff;
    vehiclestate.Vel_D = 
      (vehiclestate.Altitude - vehiclestate_old.Altitude) / timediff;

    vehiclestate.Acc_E = 
      (vehiclestate.Vel_E - vehiclestate_old.Vel_E) / timediff;
    vehiclestate.Acc_D = 
      (vehiclestate.Vel_D - vehiclestate_old.Vel_D) / timediff;
  }

  /*
   * Compute roll, pitch, yaw for Alice 
   *
   * At one point this was subtracking off IMU offsets.  However, this
   * should *not* be required since this is the yaw of the vehicle frame,
   * not that IMU
   */

  // Compute the yaw using Alice coords and convert to -pi to pi
  double yaw = M_PI/2 - RobotPos->Yaw();
  vehiclestate.Yaw = atan2(sin(yaw), cos(yaw));
  vehiclestate.Pitch = RobotPos->Pitch();
  vehiclestate.Roll = RobotPos->Roll();


  // Update angular rates, accelerations if timediff != 0 (running)
  if (timediff != 0) 
  {
    // Compute filtered version of yaw rate
    // Note that RobotPos->YawSpeed() is always zero (bug in gz?)
    vehiclestate.YawRate =  
      0.5 * vehiclestate_old.YawRate + 
      0.5 * (vehiclestate.Yaw - vehiclestate_old.Yaw) / timediff;
    //cout << "Yaw Speed = " << RobotPos->YawSpeed() << endl;

    vehiclestate.YawAcc = 
      (vehiclestate.YawRate - vehiclestate_old.YawRate) / timediff;
  
    vehiclestate.PitchRate = 
      (vehiclestate.Pitch - vehiclestate_old.Pitch) / timediff;
    vehiclestate.PitchAcc = 
      (vehiclestate.PitchRate - vehiclestate_old.PitchRate )/ timediff;

    vehiclestate.RollRate = 
      (vehiclestate.Roll - vehiclestate_old.Roll) / timediff;
    vehiclestate.RollAcc = 
      (vehiclestate.RollRate - vehiclestate_old.RollRate ) / timediff;
  }
  
  //cout << "updated state" << endl;

  //cout << "------------------" << endl;
  //cout << "NorthingGPS  : " << GPSpos->utm_northing << endl;  
  //cout << "EastingGPS   : " << GPSpos->utm_easting << endl;  
  
  cout << "------------------" << endl;
  cout << "NEA: " << vehiclestate.Northing << ", "
		  << vehiclestate.Easting << ", "
                  << vehiclestate.Altitude << endl;
  cout << "YRP: " << vehiclestate.Yaw << ", "
		  << vehiclestate.Roll << ", "
		  << vehiclestate.Pitch << endl;
  
  cout << "------------------" << endl;
  cout << "Northing V: " << vehiclestate.Vel_N << endl;
  cout << "Easting V : " << vehiclestate.Vel_E << endl;
  cout << "Altitude V: " << vehiclestate.Vel_D << endl;
  cout << "Yaw V     : " << vehiclestate.YawRate << endl;
  cout << "Roll V    : " << vehiclestate.RollRate << endl;
  cout << "Pitch V   : " << vehiclestate.PitchRate << endl;
  //cout << "------------------" << endl;
  //cout << "Northing A: " << vehiclestate.Acc_N << endl;
  //cout << "Easting A : " << vehiclestate.Acc_E << endl;
  //cout << "Altitude A: " << vehiclestate.Acc_D << endl;
  //cout << "Yaw A     : " << vehiclestate.YawAcc << endl;
  //cout << "Roll A    : " << vehiclestate.RollAcc << endl;
  //cout << "Pitch A   : " << vehiclestate.PitchAcc << endl;
  //cout << "------------------" << endl;
  
  //cout << "X: " << Easting_Offset + RobotPos->Xpos() - 495000.0<< endl;
  //cout << "Y: " << Northing_Offset + RobotPos->Ypos() - 3834000.0<< endl;

  cout << "X: " << RobotPos->Xpos() << endl;
  cout << "Y: " << RobotPos->Ypos() << endl;
  cout << "TimeStamp (old) : " << vehiclestate_old.Timestamp <<endl;
  cout << "TimeStamp (new) : " << vehiclestate.Timestamp <<endl;
  cout << "Time Diff: " << timediff << endl;
  cout << "GPS Time (sec) : " << (GPSpos->time).tv_sec <<endl;
  cout << "GPS Time (usec) : " << (GPSpos->time).tv_usec <<endl;
  cout << "DGCtime  : " << dgctime/1000000 << endl;
  cout << "------------------" << endl;
  
}

void FakeState::printVehicleState()
{
	cout << "Vehicle State: FIXME" << endl;
}


