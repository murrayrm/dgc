#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

#include <stdlib.h>

#include "sn_msg.hh"
#include "DGCutils"

#define MAX_BUFFER_SIZE 10000000

int main(int argc, char *argv[])
{
	char*                pDatabuffer;
	sn_msg               msgtype;
	int                  msgsize;
	unsigned long long   timestamp;
	int bytesSent;

	int sockets[last_type];
	int i;
	int position = 0;

	pDatabuffer = new char[MAX_BUFFER_SIZE];

	bool bPlayall = false;
	ifstream logfile;
	istream* pLogstream = &logfile;
	if(argc == 1)
	{
		logfile.open("/tmp/guilog");
	}
	else
	{
		if(strcmp(argv[1], "-") == 0)
		{
			pLogstream = &cin;
			bPlayall = true;
		}
		else
			logfile.open(argv[1]);
	}

	int sn_key;
	cerr << "Searching for skynet KEY " << endl;
	char* pSkynetkey = getenv("SKYNET_KEY");
	if( pSkynetkey == NULL )
	{
		cerr << "SKYNET_KEY environment variable isn't set" << endl;
		return -1;
	}
	else {
		sn_key = atoi(pSkynetkey);
		cout << "Construting skynet with key " << sn_key << endl;
	}
	skynet skynetobject(SNguilogplayer, sn_key);

	for(i=0; i<last_type; i++)
	{
		sockets[i] = skynetobject.get_send_sock((sn_msg)i);
	}

	string line;
	int numToPlayback;
	int lastPlayedBack = 1;
	bool doPlay;

	while(true)
	{
		doPlay = true;

		if(!bPlayall)
		{
			cout << endl << "Position: " << position << endl;
			cout << "Enter the number of messages you want to playback. Precede with + to FF. 0 to quit" << endl;

			cin.getline(pDatabuffer, MAX_BUFFER_SIZE);
			if(pDatabuffer[0] == '\0')
				numToPlayback = lastPlayedBack;
			else
			{
				if(pDatabuffer[0] == '+')
				{
					doPlay = false;
					istringstream linestream(pDatabuffer+1);
					linestream >> numToPlayback;
				}
				else
				{
					istringstream linestream(pDatabuffer);
					linestream >> numToPlayback;
					lastPlayedBack = numToPlayback;
				}
			}

			if(numToPlayback == 0)
				break;
		}
		else
			numToPlayback = 1000000000;

		position += numToPlayback;

		for(i = 0; i<numToPlayback; i++)
		{
			pLogstream->read((char*)&timestamp, sizeof(timestamp));

			pLogstream->read((char*)&msgtype, sizeof(msgtype));
			if(msgtype < 0 || msgtype >= last_type)
			{
				cerr << "msgtype too large; corrupt data;" << endl;
				delete pDatabuffer;
				return -1;
			}

			pLogstream->read((char*)&msgsize, sizeof(msgsize));
			if(doPlay)
			{
				if(msgsize > MAX_BUFFER_SIZE)
				{
					cerr << "message too large: size = " << msgsize << ". skipping..." << endl;
					pLogstream->seekg(msgsize, ios_base::cur);
				}
				else
				{
					if(! *pLogstream)
					{
						cerr << "Reached end of log file" << endl;
						delete pDatabuffer;
						return 0;
					}
					pLogstream->read(pDatabuffer, msgsize);
				}

				cout << "Timestamp: " << timestamp << ", msgtype: " << sn_msg_asString(msgtype) << endl;

				//			cerr << "about to send " << msgsize << " bytes" << endl;
				// 			if(msgtype == SNstate) cerr << "Northing = " << ((double*)pDatabuffer)[1] << endl;
				bytesSent = skynetobject.send_msg(sockets[msgtype], pDatabuffer, msgsize, 0);

				if(bytesSent != msgsize)
				{
					cerr << "Tried to send " << msgsize << " but sent " << bytesSent << " bytes." << endl;
					delete pDatabuffer;
					return -1;
				}
			}
			else
			{
				pLogstream->seekg(msgsize, ios_base::cur);
				if(! *pLogstream)
				{
					cerr << "Reached end of log file" << endl;
					delete pDatabuffer;
					return 0;
				}
				//				cout << "Skipped: Timestamp: " << timestamp << ", msgtype: " << sn_msg_asString(msgtype) << endl;
			}
		}
	}

	delete pDatabuffer;
}
