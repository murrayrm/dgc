#include "CElevationFuser.hh"

CElevationFuser::CElevationFuser() {
  _data.meanElevation = 0;
  //_data.meanN = 0;
  //_data.meanE = 0;
  _data.meanSquaredElevation = 0;
  _data.numPoints = 0;
	_data.timestamp = 0;
	_data.forgettingThreshold = 0;

  _data.cellType = EMPTY;

  _data.displayType = ELEVATION;
}


CElevationFuser::~CElevationFuser() {
}


CElevationFuser::STATUS CElevationFuser::setOutsideMap() {
  _data.cellType = OUTSIDE_MAP;

  return OK;
}


CElevationFuser::STATUS CElevationFuser::resetNoData() {
  _data.cellType = EMPTY;
	_data.meanElevation = 0.0;
  
  return OK;
}


CElevationFuser::STATUS CElevationFuser::resetNoData(unsigned long long neighborsTimestamp) {
	if(neighborsTimestamp - _data.timestamp > _data.forgettingThreshold && _data.cellType==DATA &&
		 neighborsTimestamp > _data.timestamp) {
		this->resetNoData();
		_data.timestamp = neighborsTimestamp;
		return OK_OVERWRITTEN;
	}

	return OK;
}


CElevationFuser::CELL_TYPE CElevationFuser::getCellType() const {
  return _data.cellType;
}


CElevationFuser::STATUS CElevationFuser::fuse_MeanElevation(NEDcoord otherPoint, unsigned long long otherTimestamp) {
	STATUS okStatus = OK;
	if(otherTimestamp-_data.timestamp > _data.forgettingThreshold && _data.cellType == DATA &&
		 otherTimestamp > _data.timestamp) {
// 		cout << "other timestamp was " << otherTimestamp << ", datats was " << _data.timestamp << " diff was "
// 				 << otherTimestamp-_data.timestamp << " and thresh is " << _data.forgettingThreshold << endl;
		okStatus = OK_OVERWRITTEN;
		_data.cellType = EMPTY;
	}

  switch(_data.cellType) {
    case EMPTY:
    _data.meanElevation = otherPoint.D;
    //_data.meanN = otherPoint.N;
    //_data.meanE = otherPoint.E;
    _data.meanSquaredElevation = pow(otherPoint.D,2);
    _data.numPoints = 1;
    _data.cellType = DATA;
		_data.timestamp = otherTimestamp;
		return okStatus;
    break;
  case DATA:
    _data.meanElevation = (_data.numPoints*_data.meanElevation + otherPoint.D)/(_data.numPoints+1);
    //_data.meanN = (_data.numPoints*_data.meanN + otherPoint.N)/(_data.numPoints+1);
    //_data.meanE = (_data.numPoints*_data.meanE + otherPoint.E)/(_data.numPoints+1);
    _data.meanSquaredElevation = (_data.numPoints*_data.meanSquaredElevation + pow(otherPoint.D, 2))/(_data.numPoints + 1);
		_data.timestamp = otherTimestamp;
    _data.numPoints++;
		return OK;
  case OUTSIDE_MAP:
  default:
    return ERROR;
    break;
  }

  return ERROR;
}

CElevationFuser::STATUS CElevationFuser::fuse_MeanElevation(CElevationFuser otherCell) {
	STATUS okStatus = OK;

  switch(otherCell.getCellType()) {
  case OUTSIDE_MAP:
    return ERROR;
    break;
  case EMPTY:
    return OK;
    break;
  case DATA:
		if(otherCell.getTimestamp()-_data.timestamp > _data.forgettingThreshold && _data.cellType==DATA &&
			 otherCell.getTimestamp() > _data.timestamp) {
			_data.cellType = EMPTY;
			okStatus = OK_OVERWRITTEN;
		}

    switch(_data.cellType) {
    case OUTSIDE_MAP:
      return ERROR;
      break;
    case EMPTY:
      //DISPLAY_TYPE tempDisplayType = _data.displayType;
      _data = otherCell.getData();
      _data.cellType = DATA;
      //_data.displayType = tempDisplayType;
			_data.timestamp = otherCell.getTimestamp();
      return okStatus;
      break;
    case DATA:
      _data.meanElevation = (_data.meanElevation*_data.numPoints + otherCell.getMeanElevation()*otherCell.getNumPoints()) / (_data.numPoints+otherCell.getNumPoints());
      //_data.meanN = (_data.meanN*_data.numPoints + otherCell.getMeanN()*otherCell.getNumPoints()) / (_data.numPoints + otherCell.getNumPoints());
      //_data.meanE = (_data.meanE*_data.numPoints + otherCell.getMeanE()*otherCell.getNumPoints()) / (_data.numPoints + otherCell.getNumPoints());
      _data.meanSquaredElevation = (_data.meanSquaredElevation*_data.numPoints + otherCell.getMeanSquaredElevation()*otherCell.getNumPoints()) / (_data.numPoints+otherCell.getNumPoints());
      _data.numPoints += otherCell.getNumPoints();
			_data.timestamp = otherCell.getTimestamp();
      return OK;
      break;
    default:
      return ERROR;
      break;
    }
    break;
  default:
    return ERROR;
    break;
  }

	return ERROR;
}


CElevationFuser::ElevationFuserData CElevationFuser::getData() const{
  return _data;
}


double CElevationFuser::getMeanElevation() const {
  return _data.meanElevation;
}


CElevationFuser::STATUS CElevationFuser::setMeanElevation(double elev) {
	_data.meanElevation = elev;

	return OK;
}


double CElevationFuser::getMeanSquaredElevation() const {
  return _data.meanSquaredElevation;
}


// double CElevationFuser::getMeanN() const{
//   return _data.meanN;
// }


// double CElevationFuser::getMeanE() const{
//   return _data.meanE;
// }


double CElevationFuser::getStdDev() const {
  return sqrt(_data.meanSquaredElevation - pow(_data.meanElevation,2));
}


double CElevationFuser::getNumPoints() const {
  return _data.numPoints;
}


bool CElevationFuser::operator== (const CElevationFuser other) const {
  if(_data.cellType!=other.getCellType()) {
    return false;
  } else if(_data.cellType!=DATA) {
		return true;
  }

  return ((_data.meanElevation == other.getMeanElevation()) &&
					(_data.meanSquaredElevation == other.getMeanSquaredElevation()) &&
					(_data.numPoints == other.getNumPoints()));
// 	  (_data.meanN == other.getMeanN()) &&
// 	  (_data.meanE == other.getMeanE()));
}


bool CElevationFuser::operator!= (const CElevationFuser other) const {
  return !(*this == other);
}


CElevationFuser& CElevationFuser::operator= (const CElevationFuser& other) {
  if(this != &other) {
    _data = other.getData();
  }

  return *this;
}

CElevationFuser::STATUS CElevationFuser::fuse_MaxElevation(NEDcoord otherPoint, unsigned long long otherTimestamp) {
	STATUS okStatus = OK;

	if(otherTimestamp - _data.timestamp > _data.forgettingThreshold && _data.cellType==DATA &&
		 otherTimestamp > _data.timestamp) {
		_data.cellType = EMPTY;
		okStatus = OK_OVERWRITTEN;
	}

  if(otherPoint.D < _data.meanElevation || _data.cellType == EMPTY) {
    _data.meanElevation = otherPoint.D;
    //_data.meanN = otherPoint.N;
    //_data.meanE = otherPoint.E;
		_data.timestamp = otherTimestamp;
  }

  return okStatus;
}


CElevationFuser::STATUS CElevationFuser::fuse_MaxElevation(CElevationFuser otherCell) {
	STATUS okStatus = OK;

	if(otherCell.getTimestamp() - _data.timestamp > _data.forgettingThreshold && _data.cellType==DATA &&
		 otherCell.getTimestamp() > _data.timestamp) {
		_data.cellType = EMPTY;
		okStatus = OK_OVERWRITTEN;
	}
  if(otherCell.getCellType()==DATA &&
     (otherCell.getMeanElevation() < _data.meanElevation ||
      _data.cellType == EMPTY)) {
    _data.meanElevation = otherCell.getMeanElevation();
    //_data.meanN = otherCell.getMeanN();
    //_data.meanE = otherCell.getMeanE();
  }
  
  return okStatus;
}


CElevationFuser::DISPLAY_TYPE CElevationFuser::getDisplayType() const {
  return _data.displayType;
}


CElevationFuser::STATUS CElevationFuser::setDisplayType(DISPLAY_TYPE displayType) {
  _data.displayType = displayType;

  return OK;
}


 unsigned long long CElevationFuser::getTimestamp() const {
	 return _data.timestamp;
 }


istream& operator>> (istream& is, CElevationFuser& destination) {
  double meanElevation, meanSquaredElevation, meanN, meanE;
  int numPoints, displayType, cellType;
	unsigned long long timestamp, forgettingThreshold;

  is >> meanElevation >> meanSquaredElevation >> meanN >> meanE
     >> numPoints >> displayType >> cellType 
		 >> forgettingThreshold >> timestamp;

  destination._data.meanElevation = meanElevation;
  destination._data.meanSquaredElevation = meanSquaredElevation;
  //destination._data.meanN = meanN;
  //destination._data.meanE = meanE;
  destination._data.numPoints = numPoints;
  destination._data.displayType = (CElevationFuser::DISPLAY_TYPE) displayType;
  destination._data.cellType = (CElevationFuser::CELL_TYPE) cellType;
	destination._data.forgettingThreshold = forgettingThreshold;
	destination._data.timestamp = timestamp;

  return is;
}


ostream& operator<< (ostream& os, const CElevationFuser& source) {
  switch(source.getDisplayType()) {
  case CElevationFuser::ALL:
    os << setprecision(10) 
			 << "_data.meanElevation=" << source.getMeanElevation()
			//<< ", _data.meanN=" << source.getMeanN()
			//<< ", _data.meanE=" << source.getMeanE()
       << ", _data.meanSquaredElevation=" << source.getMeanSquaredElevation()
       << ", _data.numPoints=" << source.getNumPoints()
       << ", _data.cellType=" << (int)source.getCellType()
			 << ", _data.timestamp=" << source.getTimestamp();
    break;
  case CElevationFuser::ELEVATION:
  default:
    os << source.getMeanElevation();
    break;
  }
  return os;
}


