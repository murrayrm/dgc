#include <iostream>
#include <iomanip>
#include "DGCutils"
#include "DBS.hh"

using namespace std;


int main(int argc, char** argv)
{
  int skynet_key;
  char* p_skynet_key = getenv("SKYNET_KEY");
  if ( p_skynet_key == NULL )
    {
      cout << "You need to set a skynet key!" << endl;
      return 1;
    }
  else
    {
      skynet_key = atoi(p_skynet_key);
      cout << "Using skynet key: " << skynet_key << endl;
    }


  DBS DBSObj(skynet_key, false);
  //  DBSObj.fftcalculatorLastN();
  DBSObj.DBSActiveLoop();


  cout << "\nProgram finished, push Control-C to exit";
  while (true)
    {
      cout.flush();
      //      cout << "road type is: " << DBSObj.GetRoadType() << endl;
      sleep(1);
    }
  return 0;
}
