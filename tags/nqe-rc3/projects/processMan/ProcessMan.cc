/**
 * ProcessMan.cc
 * Blackbox Implementation 7/22/05
 * Revision by Lusann Yang
 */

#include "ProcessMan.hh"

ProcessMan::ProcessMan(int snKey)
  : m_pmSkynet(SNProcMon, snKey),
    myInput(SNmodlist),
    sender(ALLMODULES)
{
  
  cout << " I got constructed " << endl;  
  listen();
}

ProcessMan::~ProcessMan()
{
}

void ProcessMan::genList()
{
}

void ProcessMan::broadcast()
{
}

void ProcessMan::listen()
{
#ifndef OLD_SKYNET
  int listenSocket = m_pmSkynet.listen(myInput,sender);
  SSkynetID id;
  while(true)
    {
      int size = m_pmSkynet.get_msg(listenSocket, &id, sizeof(id), 0);
      if(size != sizeof(id))
	{
	  cerr << "ProcessMan::listen(); got " << size << " bytes. expected " << sizeof(id) << endl;
	}
      else
	{
	  cout << "Received heartbeat from " << modulename_asString(id.name) << " running on " << id.host << " with uniD " << id.unique << endl;
	  //intelliAdd(id);
	}
    }
#endif
}

void ProcessMan::readConfig()
{
}
