
/* hardcoded information about the contest date */

/* Define the contest date. */
/* Values for the actual contest date of March 13, 2004 are 2,2004,13,0 */
#define CONTEST_MONTH				2	/* 0=January; 2=March */
#define CONTEST_YEAR				2004
#define CONTEST_DAY_OF_MONTH			13
#define CONTEST_IS_DAYLIGHT_SAVINGS_TIME	0

#define GEODETIC_MODEL	 			GIS_GEODETIC_MODEL_WGS_84
 
