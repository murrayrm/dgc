#ifndef __GEOMETRY_HH__
#define __GEOMETRY_HH__

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <iostream>

using namespace std;

//! Make sure you know you're working with Fortran matrices
//! This destroys the pointArray
void calculateWeightedPlaneFit(double pointArray[], 
			       double weightVector[],
			       int numPoints, 
			       double coeffs[]);

//! Make sure you know you're working with Fortran matrices
//! This destroys the inputMatrix
void pseudoInv(double inputMatrix[], int numCols, int numRows,
	       double outputMatrix[]);

//! This takes normal arrays and computes the residual
//! This destroys pointArray maybe
//! It probably destroys weightVector too
double calculateWeightedPlaneFitAndResidual(double pointArray[],
					    double weightVector[],
					    int numPoints,
					    double coeffs[]);


#endif //__GEOMETRY_HH__
