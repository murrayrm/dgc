#ifndef _PLANNER_H_
#define _PLANNER_H_

#include "VehicleState.hh"
#include "rddf.hh"
#include "CMap.hh"
#include "traj.h"
#include "RefinementStage.h"
#include "ReactiveStage.h"

class CPlanner
{
	CRefinementStage	m_refinementStage;
	CReactiveStage	  m_reactiveStage;

	ofstream					m_interm;
	ofstream					m_after;

public:
	CPlanner(CMap *pMap,  int mapLayerID,
					 RDDF* pRDDF, bool USE_MAXSPEED = false,
					 bool bGetVProfile = false);
	~CPlanner();

	int plan(VehicleState *pState, bool bIsStateFromAstate = true,
					 CTraj* pPrevTraj = NULL);
	double getVProfile(CTraj* pTraj);

	CTraj* getTraj(void);
	CTraj* getSeedTraj(void);
	CTraj* getIntermTraj(void);

	double getLength(void);
};

#endif // _PLANNER_H_
