#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

#include "CMap.hh"
#include "RefinementStage.h"

extern CRefinementStage* g_pRefinementStage;

CRefinementStage::CRefinementStage(CMap *pMap, int mapLayerID, RDDF* pRDDF, bool USE_MAXSPEED, bool bGetVProfile)
	: m_pRDDF(pRDDF), m_pMap(pMap), m_mapLayer(mapLayerID), PRINT_LEVEL(0), 
		m_USE_MAXSPEED(USE_MAXSPEED), m_bGetVProfile(bGetVProfile)
{
	// Allocate memory for the trajectory. Trajectory is 3th order (has up to and
	// including 2nd derivatives)
	m_pTraj		= new CTraj(3);

	m_initN = m_initE = 0.0;

	// Allocate memory for various arrays and init various SNOPT variables
	strncpy(start, "Cold    ", 8);
	nName = 1;
	objadd = 0.0;
	strncpy(prob   ,  "planner ", 8);
	strncpy(prob+8 ,  "none    ", 8);
	strncpy(prob+16,  "rhs     ", 8);
	strncpy(prob+24,  "ranges  ", 8);
	strncpy(prob+32,  "bounds  ", 8);
	inform = 0;

	m_bOnlySeed = USE_ONLY_THE_SEED_REFINEMENT;

	NPnumVariables = NUM_POINTS + NUM_SPEED_SEGMENTS + 1;

	NUM_NLIN_INIT_CONSTR = 2; // initial a, dtheta/dt (v0,theta0 are specified inside the spline)
	NUM_NLIN_TRAJ_CONSTR = (VCONSTR + ACONSTR + PHICONSTR + PHIDCONSTR + ROLLOVERCONSTR);
	NUM_NLIN_FINL_CONSTR = 2; // final (n,e) arc
	NUM_LIN_INIT_CONSTR  = 0;
	NUM_LIN_TRAJ_CONSTR  = 0;
	NUM_LIN_FINL_CONSTR  = 0;

	initialize("dercoeffs.dat");

	m_specsName = "SPECS REFINEMENT";

	m_pColl_speed   = new double[NUM_COLLOCATION_POINTS];
	m_pColl_dspeed  = new double[NUM_COLLOCATION_POINTS];

	m_pOutputSpeed  = new double [NUM_COLLOCATION_POINTS_FINE];
	m_pOutputDSpeed = new double [NUM_COLLOCATION_POINTS_FINE];

	m_speedestimatematrix = new double[NUM_SPEED_SEGMENTS*NUM_COLLOCATION_POINTS];
	ifstream speedestimatematrix("speedestimatematrix.dat");
	if(!speedestimatematrix)
	{
		cerr << "couldn't open speed estimate matrix file\n";
		exit(1);
	}
	speedestimatematrix.read((char*)m_speedestimatematrix, NUM_SPEED_SEGMENTS*NUM_COLLOCATION_POINTS*sizeof(double));

	m_pSeedTraj = new CTraj;

	memset(m_pCollGradN, 0, NUM_POINTS*sizeof(m_pCollGradN[0]));
	memset(m_pCollGradE, 0, NUM_POINTS*sizeof(m_pCollGradE[0]));

	g_pRefinementStage = this;
 	PLANNING_TARGET_DIST = LEAST_TARGET_DIST;
}

void CRefinementStage::initialize(char *pDercoeffsFile)
{
	int i;

	SNOPTn = NPnumVariables;
	nnobj = NPnumVariables;
	nnjac = NPnumVariables;
	// no linear objective
	iobj = 0;


	m_splinecoeffsTheta = new double[(NUM_POINTS-1) * 3];
	m_PlannerLowerBounds = new double[NUM_LIN_INIT_CONSTR  + NUM_LIN_FINL_CONSTR  + NUM_LIN_TRAJ_CONSTR +
																		NUM_NLIN_INIT_CONSTR + NUM_NLIN_FINL_CONSTR + NUM_NLIN_TRAJ_CONSTR];
	m_PlannerUpperBounds = new double[NUM_LIN_INIT_CONSTR  + NUM_LIN_FINL_CONSTR  + NUM_LIN_TRAJ_CONSTR +
																		NUM_NLIN_INIT_CONSTR + NUM_NLIN_FINL_CONSTR + NUM_NLIN_TRAJ_CONSTR];
	m_valcoeffs = new double[NUM_COLLOCATION_POINTS*NUM_POINTS];
	m_d1coeffs  = new double[NUM_COLLOCATION_POINTS*NUM_POINTS];
	m_d2coeffs  = new double[NUM_COLLOCATION_POINTS*NUM_POINTS];
	m_valcoeffsfine = new double[NUM_COLLOCATION_POINTS_FINE*NUM_POINTS];
	m_d1coeffsfine  = new double[NUM_COLLOCATION_POINTS_FINE*NUM_POINTS];
	m_d2coeffsfine  = new double[NUM_COLLOCATION_POINTS_FINE*NUM_POINTS];

	m_valcoeffs_speed     = new double[NUM_COLLOCATION_POINTS     *NUM_SPEED_SEGMENTS];
	m_d1coeffs_speed      = new double[NUM_COLLOCATION_POINTS     *NUM_SPEED_SEGMENTS];
	m_valcoeffsfine_speed = new double[NUM_COLLOCATION_POINTS_FINE*NUM_SPEED_SEGMENTS];
	m_d1coeffsfine_speed  = new double[NUM_COLLOCATION_POINTS_FINE*NUM_SPEED_SEGMENTS];

	// the matrix that takes the spline control points and generates the
	// polynomial coefficients of each spline segment. There are NUM_POINTS
	// control points. There are NUM_POINTS-1 spline segments, and since each is
	// quadratic, each is defined by 3 values. Note that since the initial value
	// of the spline (m_initTheta) is not included, these will need to be manually
	// added.
	m_dercoeffsmatrix     = new double[NUM_POINTS*((NUM_POINTS-1) * 3)];
	m_stateestimatematrix = new double[NUM_POINTS*NUM_COLLOCATION_POINTS_FINE];
	m_speedestimatematrixfine = new double[NUM_SPEED_SEGMENTS*NUM_COLLOCATION_POINTS_FINE];

	m_pColl_theta       = new double[NUM_COLLOCATION_POINTS];
	m_pColl_dtheta      = new double[NUM_COLLOCATION_POINTS];
	m_pColl_ddtheta     = new double[NUM_COLLOCATION_POINTS];
	m_pCollN            = new double[NUM_COLLOCATION_POINTS];
	m_pCollE            = new double[NUM_COLLOCATION_POINTS];
	m_pCollGradN        = new double[NUM_COLLOCATION_POINTS * NUM_POINTS];
	m_pCollGradE        = new double[NUM_COLLOCATION_POINTS * NUM_POINTS];
	m_pCollVlimit       = new double[NUM_COLLOCATION_POINTS];
	m_pCollDVlimitDN    = new double[NUM_COLLOCATION_POINTS];
	m_pCollDVlimitDE    = new double[NUM_COLLOCATION_POINTS];
	m_pCollDVlimitDTheta= new double[NUM_COLLOCATION_POINTS];

	m_pConstrData       = new double[NPnumNonLinearConstr];
	m_pConstrGradData   = new double[NPnumNonLinearConstr*NPnumVariables];
	m_pCostGradData     = new double[NPnumVariables];

	m_grad_solver = new double*[NUM_NLIN_INIT_CONSTR +
															NUM_NLIN_TRAJ_CONSTR +
															NUM_NLIN_FINL_CONSTR];
	for(i=0;
			i<NUM_NLIN_INIT_CONSTR +
				NUM_NLIN_TRAJ_CONSTR +
				NUM_NLIN_FINL_CONSTR;
			i++)
	{
		m_grad_solver[i] = new double[SIZE_ZP];
	}

	m_pOutputTheta  = new double[NUM_COLLOCATION_POINTS_FINE];
	m_pOutputDTheta = new double[NUM_COLLOCATION_POINTS_FINE];

	m_pThetaVector = new double[NUM_COLLOCATION_POINTS_FINE];

	ifstream dercoeffs(pDercoeffsFile);
	if(!dercoeffs)
	{
		cerr << "couldn't open derivative coefficients file\n";
		exit(1);
	}
	dercoeffs.read((char*)m_valcoeffs,       NUM_COLLOCATION_POINTS*NUM_POINTS        *sizeof(double));
	dercoeffs.read((char*)m_d1coeffs,        NUM_COLLOCATION_POINTS*NUM_POINTS        *sizeof(double));
	dercoeffs.read((char*)m_d2coeffs,        NUM_COLLOCATION_POINTS*NUM_POINTS        *sizeof(double));
	dercoeffs.read((char*)m_valcoeffs_speed, NUM_COLLOCATION_POINTS*NUM_SPEED_SEGMENTS*sizeof(double));
	dercoeffs.read((char*)m_d1coeffs_speed,  NUM_COLLOCATION_POINTS*NUM_SPEED_SEGMENTS*sizeof(double));

	ifstream dercoeffsfine("dercoeffsfine.dat");
	if(!dercoeffsfine)
	{
		cerr << "couldn't open derivative coefficients fine file\n";
		exit(1);
	}
	dercoeffsfine.read((char*)m_valcoeffsfine,       NUM_COLLOCATION_POINTS_FINE*NUM_POINTS        *sizeof(double));
	dercoeffsfine.read((char*)m_d1coeffsfine,        NUM_COLLOCATION_POINTS_FINE*NUM_POINTS        *sizeof(double));
	dercoeffsfine.read((char*)m_d2coeffsfine,        NUM_COLLOCATION_POINTS_FINE*NUM_POINTS        *sizeof(double));
	dercoeffsfine.read((char*)m_valcoeffsfine_speed, NUM_COLLOCATION_POINTS_FINE*NUM_SPEED_SEGMENTS*sizeof(double));
	dercoeffsfine.read((char*)m_d1coeffsfine_speed,  NUM_COLLOCATION_POINTS_FINE*NUM_SPEED_SEGMENTS*sizeof(double));

	ifstream dercoeffsmatrix("dercoeffsmatrix.dat");
	if(!dercoeffsmatrix)
	{
		cerr << "couldn't open derivative matrix file\n";
		exit(1);
	}
	dercoeffsmatrix.read((char*)m_dercoeffsmatrix, NUM_POINTS*((NUM_POINTS-1) * 3)*sizeof(double));

	ifstream stateestimatematrix("stateestimatematrix.dat");
	if(!stateestimatematrix)
	{
		cerr << "couldn't open state estimate matrix file\n";
		exit(1);
	}
	stateestimatematrix.read((char*)m_stateestimatematrix    , NUM_POINTS        *NUM_COLLOCATION_POINTS_FINE*sizeof(double));
	stateestimatematrix.read((char*)m_speedestimatematrixfine, NUM_SPEED_SEGMENTS*NUM_COLLOCATION_POINTS_FINE*sizeof(double));

	// extra 1 for the cost
	SNOPTm = NPnumNonLinearConstr + NPnumLinearConstr;
	nncon = NPnumNonLinearConstr;
	ne = (NPnumNonLinearConstr + NPnumLinearConstr) * NPnumVariables;

	m_SNOPTmatrix = new double[ne];
	ha = new int[ne];
	ka = new int[SNOPTn+1];
	hs = new int[SNOPTn+SNOPTm];
	m_pSolverState = new double[SNOPTn+SNOPTm];
	m_pPrevSolverState = new double[SNOPTn];
	pi = new double[SNOPTm];
	rc = new double[SNOPTn+SNOPTm];

	memset(m_SNOPTmatrix, 0, ne*sizeof(m_SNOPTmatrix[0]));
	for(i=0; i<ne; i++)
	{
		ha[i] = (i % (NPnumNonLinearConstr + NPnumLinearConstr)) + 1;
	}

	for(i=0; i<=SNOPTn; i++)
	{
		ka[i] = (NPnumNonLinearConstr + NPnumLinearConstr) * i + 1;
	}

	m_solverLowerBounds = new double[NPnumVariables + NPnumNonLinearConstr + NPnumLinearConstr];
	m_solverUpperBounds = new double[NPnumVariables + NPnumNonLinearConstr + NPnumLinearConstr];

	lencw = 10000;
	leniw = 2000*(SNOPTm+SNOPTn);
	lenrw = 4000*(SNOPTm+SNOPTn);
	cw = new char[8*lencw];	
	iw = new int[leniw];
	rw = new double[lenrw];

	SetUpLinearConstr();
}

void CRefinementStage::SetUpLinearConstr(void)
{
// 	int i,j;
// 	// this is probably redundant
// 	memset(m_SNOPTmatrix,0, (NPnumNonLinearConstr + NPnumLinearConstr) * NPnumVariables * sizeof(m_SNOPTmatrix[0])); 

//	int i,j;
// #error "this is for SNOPT"
// 	// set up the linear constraints
// 	// traj constraint: theta (measured from the init theta)
// 	for(j=0; j<NUM_COLLOCATION_POINTS; j++)
// 	{
// 		for(i=0; i<NUM_POINTS; i++)
// 		{
// 			m_SNOPTmatrix[i * (NPnumNonLinearConstr + NPnumLinearConstr) + NPnumNonLinearConstr + j] =
// 				SCALEFACTOR_THETA * m_valcoeffs[j + i*NUM_COLLOCATION_POINTS];
// 		}
// 		m_SNOPTmatrix[(NPnumVariables-1) * (NPnumNonLinearConstr + NPnumLinearConstr) + NPnumNonLinearConstr + j] =
// 			0.0;
// 	}

// 	// final constraint: final theta
// 	for(i=0; i<NUM_POINTS; i++)
// 	{
// 		m_SNOPTmatrix[i * (NPnumNonLinearConstr + NPnumLinearConstr) + NPnumNonLinearConstr +
// 									NUM_COLLOCATION_POINTS*NUM_LIN_TRAJ_CONSTR] =
// 			SCALEFACTOR_THETA * m_valcoeffs[(NUM_COLLOCATION_POINTS-1) + i*NUM_COLLOCATION_POINTS];
// 	}
// 	m_SNOPTmatrix[(NPnumVariables-1) * (NPnumNonLinearConstr + NPnumLinearConstr) + NPnumNonLinearConstr +
// 								NUM_COLLOCATION_POINTS*NUM_LIN_TRAJ_CONSTR] =
// 		0.0;


	// Now zero out the parts of the 'm_SNOPTmatrix' matrix in the theta constraint that
	// correspond to the v
// 	for(i=NUM_POINTS; i<2*NUM_POINTS; i++)
// 	{
// 		memset(&m_SNOPTmatrix[i * (NPnumNonLinearConstr + NPnumLinearConstr) + NPnumNonLinearConstr], 0,
// 					 NUM_COLLOCATION_POINTS*sizeof(m_SNOPTmatrix[0]));
// 		m_SNOPTmatrix[i * (NPnumNonLinearConstr + NPnumLinearConstr) + NPnumNonLinearConstr +
// 									NUM_COLLOCATION_POINTS*NUM_LIN_TRAJ_CONSTR] = 0.0;
// 	}
// 	// traj constraint: v
// 	for(j=0; j<NUM_COLLOCATION_POINTS; j++)
// 	{
// 		for(i=NUM_POINTS; i<NUM_POINTS+NUM_SPEED_SEGMENTS; i++)
// 		{
// 			m_SNOPTmatrix[i * (NPnumNonLinearConstr + NPnumLinearConstr) + NPnumNonLinearConstr + j] =
// 				SCALEFACTOR_SPEED * m_valcoeffs_speed[j + (i-NUM_POINTS)*NUM_COLLOCATION_POINTS];
// 		}
// 		m_SNOPTmatrix[(NPnumVariables-1) * (NPnumNonLinearConstr + NPnumLinearConstr) + NPnumNonLinearConstr + j] =
// 			0.0;
// 	}
// 	// zero out the components of the v traj constraint that deal with theta
// 	for(i=0; i<NUM_POINTS; i++)
// 	{
// 		memset(&m_SNOPTmatrix[i * (NPnumNonLinearConstr + NPnumLinearConstr) + NPnumNonLinearConstr], 0, NUM_COLLOCATION_POINTS*sizeof(m_SNOPTmatrix[0]));
// 	}
}

void CRefinementStage::makeSolverBoundsArrays(void)
{
	// if we're only getting the velocity profile then lock in the spatial part of the traj
	if(m_bGetVProfile)
	{
		m_PlannerLowerBounds[1] = -BIGNUMBER;
		m_PlannerUpperBounds[1] =  BIGNUMBER;
	}

	int i, j;

	for(i=0; i<NUM_POINTS; i++)
	{
		m_solverLowerBounds[i] = -BIGNUMBER;
		m_solverUpperBounds[i] =  BIGNUMBER;
	}
	for(i=NUM_POINTS; i<NUM_POINTS+NUM_SPEED_SEGMENTS; i++)
	{
		m_solverLowerBounds[i] = MIN_V - m_initSpeed;
		m_solverUpperBounds[i] = 140.0 - m_initSpeed; // set the absolute maximum speed
	}

	// set the bounds for sf
#ifndef CONSTANT_SF
	m_solverLowerBounds[NPnumVariables-1] = MIN_SF;
	m_solverUpperBounds[NPnumVariables-1] = MAX_SF;
#else
	m_solverLowerBounds[NPnumVariables-1] = m_pSolverState[NPnumVariables-1];
	m_solverUpperBounds[NPnumVariables-1] = m_pSolverState[NPnumVariables-1];
#endif

	// put in the non-linear constraints
	int nonlinearBoundOffset = NPnumVariables;
	for(i=0; i<NUM_NLIN_INIT_CONSTR; i++)
	{
		m_solverLowerBounds[nonlinearBoundOffset + i] = m_PlannerLowerBounds[i];
		m_solverUpperBounds[nonlinearBoundOffset + i] = m_PlannerUpperBounds[i];
	}
	for(i=0; i<NUM_NLIN_TRAJ_CONSTR; i++)
	{
		for(j=0; j<NUM_COLLOCATION_POINTS; j++)
		{
			m_solverLowerBounds[nonlinearBoundOffset + i+j*NUM_NLIN_TRAJ_CONSTR+NUM_NLIN_INIT_CONSTR] =
				m_PlannerLowerBounds[i + NUM_NLIN_INIT_CONSTR];
			m_solverUpperBounds[nonlinearBoundOffset + i+j*NUM_NLIN_TRAJ_CONSTR+NUM_NLIN_INIT_CONSTR] =
				m_PlannerUpperBounds[i + NUM_NLIN_INIT_CONSTR];
		}
	}
	// ignore the specified number of speed limits
	i = 0;
	for(j=0; j<NUM_IGNORED_CONSTRAINTS; j++)
	{
		m_solverLowerBounds[nonlinearBoundOffset + i+j*NUM_NLIN_TRAJ_CONSTR+NUM_NLIN_INIT_CONSTR] =
			-BIGNUMBER;
		m_solverUpperBounds[nonlinearBoundOffset + i+j*NUM_NLIN_TRAJ_CONSTR+NUM_NLIN_INIT_CONSTR] =
			BIGNUMBER;
	}

	for(i=0; i<NUM_NLIN_FINL_CONSTR; i++)
	{
		m_solverLowerBounds[nonlinearBoundOffset + i + NUM_NLIN_INIT_CONSTR + NUM_COLLOCATION_POINTS*NUM_NLIN_TRAJ_CONSTR] =
			m_PlannerLowerBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + i];
		m_solverUpperBounds[nonlinearBoundOffset + i + NUM_NLIN_INIT_CONSTR + NUM_COLLOCATION_POINTS*NUM_NLIN_TRAJ_CONSTR] =
			m_PlannerUpperBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + i];
	}

	// put in the linear constraints
//	int linearBoundOffset    = NPnumVariables + NPnumNonLinearConstr;
// 	for(i=0; i<NUM_LIN_INIT_CONSTR; i++)
// 	{
// 		m_solverLowerBounds[linearBoundOffset + i] = m_PlannerLowerBounds[i + NUM_NLIN_CONSTR];
// 		m_solverUpperBounds[linearBoundOffset + i] = m_PlannerUpperBounds[i + NUM_NLIN_CONSTR];
// 	}
// 	for(i=0; i<NUM_LIN_TRAJ_CONSTR; i++)
// 	{
// 		for(j=0; j<NUM_COLLOCATION_POINTS; j++)
// 		{
// 			m_solverLowerBounds[linearBoundOffset + i*NUM_COLLOCATION_POINTS+j+NUM_LIN_INIT_CONSTR] =
// 				m_PlannerLowerBounds[i + NUM_NLIN_CONSTR + NUM_LIN_INIT_CONSTR];
// 			m_solverUpperBounds[linearBoundOffset + i*NUM_COLLOCATION_POINTS+j+NUM_LIN_INIT_CONSTR] =
// 				m_PlannerUpperBounds[i + NUM_NLIN_CONSTR + NUM_LIN_INIT_CONSTR];
// 		}
// 	}
// 	for(i=0; i<NUM_LIN_FINL_CONSTR; i++)
// 	{
// 		m_solverLowerBounds[linearBoundOffset + i + NUM_LIN_INIT_CONSTR + NUM_COLLOCATION_POINTS*NUM_LIN_TRAJ_CONSTR] =
// 			m_PlannerLowerBounds[NUM_NLIN_CONSTR + NUM_LIN_INIT_CONSTR + NUM_LIN_TRAJ_CONSTR + i];
// 		m_solverUpperBounds[linearBoundOffset + i + NUM_LIN_INIT_CONSTR + NUM_COLLOCATION_POINTS*NUM_LIN_TRAJ_CONSTR] =
// 			m_PlannerUpperBounds[NUM_NLIN_CONSTR + NUM_LIN_INIT_CONSTR + NUM_LIN_TRAJ_CONSTR + i];
// 	}

	if(m_bGetVProfile)
	{
		for(int i=0; i<NUM_POINTS; i++)
		{
			m_solverLowerBounds[i] = m_pSolverState[i];
			m_solverUpperBounds[i] = m_pSolverState[i];
		}
		m_solverLowerBounds[NPnumVariables-1] = m_pSolverState[NPnumVariables-1];
		m_solverUpperBounds[NPnumVariables-1] = m_pSolverState[NPnumVariables-1];
	}
}

void CRefinementStage::SetUpPlannerBounds(void)
{
#ifndef CONSTANT_SF
	m_PlannerLowerBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + 0] = SCALEFACTOR_DIST * (-HALF_TARGET_ERROR_LONG);
	m_PlannerUpperBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + 0] = SCALEFACTOR_DIST * ( HALF_TARGET_ERROR_LONG);
	m_PlannerLowerBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + 1] = SCALEFACTOR_DIST * (-HALF_TARGET_ERROR_LAT);
	m_PlannerUpperBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + 1] = SCALEFACTOR_DIST * ( HALF_TARGET_ERROR_LAT);
#else
	m_PlannerLowerBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + 0] = -BIGNUMBER;
	m_PlannerUpperBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + 0] =  BIGNUMBER;
	m_PlannerLowerBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + 1] = -BIGNUMBER;
	m_PlannerUpperBounds[NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + 1] =  BIGNUMBER;
#endif

	// Define the non-linear constraint bounds
	if(USE_INITIAL_ACCEL)
	{
		m_PlannerLowerBounds[0] = m_PlannerUpperBounds[0] = SCALEFACTOR_ACCEL * m_initAcc;
		m_PlannerLowerBounds[0] -= SCALEFACTOR_ACCEL * VEHICLE_MAX_ACCEL * NONLINEAR_MARGIN;
		m_PlannerUpperBounds[0] += SCALEFACTOR_ACCEL * VEHICLE_MAX_ACCEL * NONLINEAR_MARGIN;
	}
	else
	{
		m_PlannerLowerBounds[0] = -BIGNUMBER;
		m_PlannerUpperBounds[0] =	 BIGNUMBER;
	}

	if(USE_INITIAL_YAWRATE)
	{
		m_PlannerLowerBounds[1] = m_PlannerUpperBounds[1] = SCALEFACTOR_YAWDOT * m_initYawRate;
		m_PlannerLowerBounds[1] -= SCALEFACTOR_YAWDOT * MAX_TAN_PHI * m_initSpeed / VEHICLE_WHEELBASE * NONLINEAR_MARGIN;
		m_PlannerUpperBounds[1] += SCALEFACTOR_YAWDOT * MAX_TAN_PHI * m_initSpeed / VEHICLE_WHEELBASE * NONLINEAR_MARGIN;
	}
	else
	{
		m_PlannerLowerBounds[1] = -BIGNUMBER;
		m_PlannerUpperBounds[1] =	 BIGNUMBER;
	}

	// These can be removed by setting preprocessor variables to 0
#if VCONSTR
	// nonlinear speed constraint: speed < vlimit
	m_PlannerLowerBounds[NUM_NLIN_INIT_CONSTR + VCONSTR_IDX] = 0.0;
	m_PlannerUpperBounds[NUM_NLIN_INIT_CONSTR + VCONSTR_IDX] = BIGNUMBER;
#endif

#if ACONSTR
	// vehicle has limited power, so a is bounded
	m_PlannerLowerBounds[NUM_NLIN_INIT_CONSTR + ACONSTR_IDX] = -SCALEFACTOR_ACCEL * VEHICLE_MAX_DECEL;
	m_PlannerUpperBounds[NUM_NLIN_INIT_CONSTR + ACONSTR_IDX] =  SCALEFACTOR_ACCEL * VEHICLE_MAX_ACCEL;
#endif

#if PHICONSTR
	// vehicle can't turn its wheel past a certain point, so phi (and thus
	// tan(phi)) is bounded
	m_PlannerLowerBounds[NUM_NLIN_INIT_CONSTR + PHICONSTR_IDX] = SCALEFACTOR_TANPHI * (-MAX_TAN_PHI);
	m_PlannerUpperBounds[NUM_NLIN_INIT_CONSTR + PHICONSTR_IDX] = SCALEFACTOR_TANPHI * ( MAX_TAN_PHI);
#endif

#if PHIDCONSTR
	// vehicle can't turn its wheel too quickly so
	// d(phi)/dt is bounded
	m_PlannerLowerBounds[NUM_NLIN_INIT_CONSTR + PHIDCONSTR_IDX] = SCALEFACTOR_PHIDOT * (-2.0 * MAX_PHI / MIN_WHEEL_RAIL_TO_RAIL_TIME);
	m_PlannerUpperBounds[NUM_NLIN_INIT_CONSTR + PHIDCONSTR_IDX] = SCALEFACTOR_PHIDOT * ( 2.0 * MAX_PHI / MIN_WHEEL_RAIL_TO_RAIL_TIME);
#endif

#if ROLLOVERCONSTR
	// vehicle can't move too fast while turning
	m_PlannerLowerBounds[NUM_NLIN_INIT_CONSTR + ROLLOVERCONSTR_IDX] =	-SCALEFACTOR_ROLLOVER * MAXSPEED_AT_FULL_STEERING*MAXSPEED_AT_FULL_STEERING* VEHICLE_MAX_TAN_AVG_STEER / VEHICLE_WHEELBASE;
	m_PlannerUpperBounds[NUM_NLIN_INIT_CONSTR + ROLLOVERCONSTR_IDX] =  SCALEFACTOR_ROLLOVER * MAXSPEED_AT_FULL_STEERING*MAXSPEED_AT_FULL_STEERING* VEHICLE_MAX_TAN_AVG_STEER / VEHICLE_WHEELBASE;
#endif
}

bool CRefinementStage::SetUpPlannerBoundsAndSeedCoefficients(CTraj* pSeedTraj)
{
	SetUpPlannerBounds();

	// if we're running the refinement stage just to get the velocity profile of a
	// spatial path, only seed from the traj (this also sets the target
	// point). Otherwise, seed from the passed-in seed traj
	if(m_bGetVProfile)
	{
		SeedFromTraj(m_pTraj);
	}
	else
	{
		SeedFromTraj(pSeedTraj, PLANNING_TARGET_DIST);
	}

	// set up the speed(s) spline
	if(SEED_MAXDECEL)
	{
// 		for(int i=NUM_POINTS; i<2*NUM_POINTS; i++)
// 		{
// 			double s = (double)(i - NUM_POINTS) / (double)(NUM_POINTS-1);
// 			double a = -VEHICLE_MAX_DECEL * 0.8; // safety factor
// 			double sf = m_pSolverState[NPnumVariables-1];
// 			double v = sqrt(2.0*a*sf*s + m_initSpeed*m_initSpeed);
// 			if(isnan(v) || v < MIN_V)
// 			{
// 				memset(&m_pSolverState[i], 0, sizeof(double)*(2*NUM_POINTS-i));
// 				break;
// 			}
// 			else
// 				m_pSolverState[i] = a*sf / v;
// 		}
		cerr << "SEED_MAXDECEL: nothing here. fix it" << endl;
	}
	else if(SEED_MAXACCEL)
	{
// 		for(int i=NUM_POINTS; i<2*NUM_POINTS; i++)
// 			m_pSolverState[i]= (MAXSPEED - m_initSpeed) / m_pSolverState[NPnumVariables-1];
		cerr << "SEED_MAXACCEL: nothing here. fix it" << endl;
	}
	else if(SEED_NOACCEL)
	{
		memset(&m_pSolverState[NUM_POINTS], 0, NUM_SPEED_SEGMENTS*sizeof(m_pSolverState[0]));
	}
	else if(SEED_SPEED_FROM_MAP)
		SeedSpeedFromState();


	outputTraj(m_pSeedTraj);

	return true;
}
