/********************
Bare bones Matrix class using BLAS routines
********************/

#include "Matrix.hh"
#include <iostream>
#include <cassert>
#include <math.h>

enum BLAS_ORDER {BlasRowMajor=101, BlasColMajor=102 };
enum BLAS_TRANSPOSE {BlasNoTrans=111, BlasTrans=112, BlasConjTrans=113,
		      AtlasConj=114};

extern "C" void daxpy_(const int *N, const double *alpha, const double *X,
                const int *incX, double *Y, const int *incY, const int *info);

extern "C" void dgemm_(const char *TransA,
                 const char *TransB, const int *M, const int *N,
                 const int *K, const double *alpha, const double *A,
                 const int *lda, const double *B, const int *ldb,
                 const double *beta, double *C, const int *ldc, const int *info);

extern "C" void dscal_(const int *N, const double *alpha, double *X, const int *incX, const int *info);

extern "C" int dgetrf_(const int *M, const int *N, double *A, const int *lda, const int *ipiv, const int *info);
extern "C" int dgetri_(const int *N, const double *A, const int *lda, int *ipiv, double *work, const int *lwork, const int *info);

extern "C" double dnrm2_(const int *N, const double *X, const int *incX, const int *info);

using namespace std;

// ructors
Matrix::Matrix()
{
  rows = 0;
  cols = 0;
  size = 0;
  data = NULL;
}

Matrix::Matrix(int newrows, int newcols) // create empty matrix
{
  rows = newrows;
  cols = newcols;
  size = rows*cols;
  data = new double[size];
  clear();

}

Matrix::Matrix(int elems) // create empty matrix
{
  rows = elems;
  cols = 1;
  size = rows*cols;
  data = new double[size];
  clear();
}


Matrix::Matrix(const Matrix &matrix2)// copy ructor
{  
  rows = matrix2.getrows(); 
  cols = matrix2.getcols();
  size = matrix2.getsize();
  data = new double[size];
  memcpy(data, matrix2.data, (size*sizeof(double))); 
}

void Matrix::clear()
{
  memset(data,0,size*sizeof(double));
}

void Matrix::setelems(double* newdata)  
{
  for(int i = 0;i < rows;i++) {
    for(int j = 0;j < cols;j++) {
      data[j*rows + i] = newdata[i*cols + j];
    }
  }
}

void Matrix::setelem(int row, int col, double value) 
{
  assert(row < rows && col < cols);
  data[col*rows + row] = value;
}

void Matrix::setelem(int elem, double value) 
{
  assert(elem < size);
  data[elem] = value;
}

double Matrix::getelem(int row, int col)  const 
{
  assert(row < rows && col < cols);
  return data[col*rows + row];
} 
double Matrix::getelem(int elem)  const 
{
  assert(elem < size);
  return data[elem];
} 

double* Matrix::getdata() const 
{
  return data;
}

Matrix Matrix::transpose() const
{
  Matrix result(cols, rows);
  result.setelems(data); // implicit transpose in setelems
  return result;
}

Matrix Matrix::eye() const
{
  Matrix result(cols, rows);
  for(int i = 0;i < rows;i++) {
    result.setelem(i,i,1.0);
  }
  return result;
}

Matrix Matrix::inverse() const
{
  Matrix result(*this);
  int *invdata = new int[rows];
  int lwork1 = -1;
  double work1[1];
  int info;


  dgetrf_(&rows, &rows, result.getdata(), &rows, invdata, &info);

  dgetri_(&rows, result.getdata(), &rows, invdata, work1, &lwork1, &info);

  int lwork2 = (int) work1[0];
  double *work2 = new double[lwork2];

  dgetri_(&rows, result.getdata(), &rows, invdata, work2, &lwork2, &info);

  delete [] invdata;
  delete [] work2;
  return result;
}

double Matrix::norm() const
{
  int one = 1;
  int info;
  return dnrm2_(&size,data,&one,&info);
}


Matrix Matrix::operator+(const Matrix& matrix2) const 
{
  assert((matrix2.getrows() == rows)&&(matrix2.getcols() == cols));
  int ione = 1;
  double done = 1.0;
  int info;

  Matrix result(*this);
  daxpy_(&size,&done,matrix2.getdata(),&ione,result.getdata(),&ione,&info);
  return result;
}

Matrix Matrix::operator-(const Matrix &matrix2) const
{
  assert((matrix2.getrows() == rows)&&(matrix2.getcols() == cols));
  int ione = 1;
  double ndone = -1.0;
  int info;

  Matrix result(*this);
  daxpy_(&size,&ndone,matrix2.getdata(),&ione,result.getdata(),&ione,&info);
  return result;
}


Matrix Matrix::operator*(const Matrix &matrix2) const
{
  assert(cols == matrix2.getrows());

  char trans = 'N';
  double done = 1.0;
  double dzero = 0.0;
  int m2rows = matrix2.getrows();
  int m2cols = matrix2.getcols();
  int info;

  Matrix result(rows, matrix2.getcols());
  dgemm_(&trans, &trans, &rows, &m2cols, &cols, &done, data, &rows, matrix2.getdata(), &m2rows, &dzero, result.getdata(), &rows, &info);
  return result;
}

Matrix Matrix::operator*(const double sf) const
{
  int ione = 1;
  int info;

  Matrix result(*this);
  dscal_(&size,&sf,result.getdata(),&ione,&info);
  return result;
}

Matrix Matrix::operator/(const double sf) const
{
  int ione = 1;
  double invsf = 1/sf;
  int info;

  Matrix result(*this);
  dscal_(&size,&invsf,result.getdata(),&ione,&info);
  return result;
}


Matrix& Matrix::operator=(const Matrix &matrix2)
{
  if (size != matrix2.getsize()) {
    delete [] data;
    data = new double[matrix2.getsize()];
  }
  rows = matrix2.getrows(); 
  cols = matrix2.getcols();
  size = matrix2.getsize();

  memcpy(data, matrix2.data, (size*sizeof(double)));
  return *this;
}

void Matrix::print() {
  for (int i=0; i<rows; ++i) {
    for (int j=0; j<cols; ++j)  printf ("%2.20f\t", data[j*rows+i]);
    printf("\n");
  }
  printf("\n");
}

Matrix::~Matrix()
{
  if (data != NULL) {
    delete [] data;
  }
}

// Note--cross product functions should only be used on 3x3 matrices
Matrix matrixCross(Matrix vect) {
  double tmp[] = {0, -vect.getelem(2), vect.getelem(1),
		  vect.getelem(2), 0, -vect.getelem(0),
		  -vect.getelem(1), vect.getelem(0), 0};
  Matrix newmat(3,3);
  newmat.setelems(tmp);
  return newmat;
}

Matrix unmatrixCross(Matrix mat) {
  double tmp[] = {-mat.getelem(1,2),mat.getelem(0,2),-mat.getelem(0,1)};
  Matrix newmat(3);
  newmat.setelems(tmp);
  return newmat;
}

Matrix matrixDiffEqStep(Matrix w) {
  Matrix rot;
  if (w.norm() > 0) {
    double nw = w.norm();
    w = w/nw;
    Matrix what = matrixCross(w);
    rot = Matrix(3,3).eye() + what * sin(-nw) + what * what * (1 - cos(-nw));
  } else {
    rot = Matrix(3,3).eye();
  }
  return rot;
}

void makeBody2Nav(Matrix &Cnb, double roll, double pitch, double yaw) {
  assert(Cnb.getrows() == 3 && Cnb.getcols() == 3);

  double cr, cp, cy, sr, sp, sy;
  sincos(roll, &sr, &cr);
  sincos(pitch, &sp, &cp);
  sincos(yaw, &sy, &cy);

  double newCnb[] = {
    cp*cy,  cy*sp*sr - cr*sy,  cr*cy*sp + sr*sy,
    cp*sy,  cr*cy + sp*sr*sy, -cy*sr + cr*sp*sy,
    -sp,    cp*sr,             cp*cr
  };  

  Cnb.setelems(newCnb);
}

void makeEarth2Nav(Matrix &Cne, double lat, double lon, double alpha) {
  assert(Cne.getrows() == 3 && Cne.getcols() == 3);

  double cla, clo, ca, sla, slo, sa;
  sincos(lat, &sla, &cla);
  sincos(lon, &slo, &clo);
  sincos(alpha, &sa, &ca);

  double newCne[] = {
    -ca*clo*sla - sa*slo,  clo*sa - ca*sla*slo,  ca*cla,
    clo*sa*sla - ca*slo,   ca*clo + sa*sla*slo, -cla*sa,
    -cla*clo,             -cla*slo,              -sla
  };

  Cne.setelems(newCne);
}

void makeNav2Geo(Matrix &Cgn, double alpha){
  assert(Cgn.getrows() == 3 && Cgn.getcols() == 3);

  double ca, sa;
  sincos(alpha, &sa, &ca);
  
  double newCgn[] = {
    ca, -sa, 0,
    sa,  ca, 0,
    0,   0,   1  };
  Cgn.setelems(newCgn);
}

void makeTransform(Matrix &Ct, double rx, double ry, double rz, double tx, double ty, double tz) {
  assert(Ct.getrows() == 4 && Ct.getcols() == 4);

  double crx, srx, cry, sry, crz, srz;
  sincos(rx, &srx, &crx);
  sincos(ry, &sry, &cry);
  sincos(rz, &srz, &crz);
  
  double newCt[] = {
    cry*crz, crz*sry*srx - crx*srz, crx*crz*sry + srx*srz,  tx,
    cry*srz, crx*crz + srx*sry*srz, -crz*srx + crx*sry*srz, ty,
    -sry,    cry*srx,               crx*cry,                tz,
    0,       0,                     0,                      1
  };  

  Ct.setelems(newCt);
}
