#include "herman.hh"

using namespace std;

/* herman one arg constructor */
herman::herman(char* rddf_file)
  : RDDF(rddf_file)
{
  pd("herman::herman start", rmsg::fncall, 1);
  hermanInit();  
  pd("herman::herman finish", rmsg::fncall, 1);
}



/* herman no arg constructor */
herman::herman()
  : RDDF(RDDF_FILE)
{
  pd("herman::herman start", rmsg::fncall, 1);
  hermanInit();  
  pd("herman::herman finish", rmsg::fncall, 1);
}



/* Destructor */
herman::~herman()
{
  pd("herman::~herman start", rmsg::fncall, 1);
  delete m_herman_state;
  pd("herman::~herman finish", rmsg::fncall, 1);
}



/* constructor helper */
void herman::hermanInit()
{
  pd("herman::hermanInit start", rmsg::fncall, 1);
  m_herman_state = new hermanState;
  m_herman_state->cur_waypoint = 0;
  m_herman_state->cur_dist_from_start = 0;
  pd("herman::hermanInit finish", rmsg::fncall, 1);
}



void herman::updateState(const VehicleState & new_state)
{
  updateState(new_state.ne_coord());
}



void herman::updateState(const NEcoord & new_state)
{
  // uncomment whichever we want to use
  if (USE_BLOBBED_UPDATE_STATE)
    updateState_blob(new_state);
  else
    updateState_center(new_state);
}



void herman::updateState(double n, double e)
{
  NEcoord the_state(n, e);
  updateState(the_state);
}



void herman::updateState_center(const NEcoord & new_state)
{
  pd("herman::updateState_center start", rmsg::fncall, 1);
  /** First, attempt to slide along the trackline, one point
   * at a time, until we stop making progress. */
  int cur_wpt = m_herman_state->cur_waypoint;

  double cur_dist_from_start;
  double this_dist;

  double ratio = 2;
  /** first step back along the centerline, if necessary */
  {
    ostringstream oss;
    oss << "herman::updateState_center stepping, starting at wpt " << cur_wpt 
	<< " ratio " << ratio;
    pd(oss.str(), rmsg::updateState, 1);
  }    
  while (true)
    {
      this_dist = getDistToCorridorSegment(cur_wpt, new_state,
					   cur_dist_from_start, ratio);
      {
	ostringstream oss;
	oss << "  (back) " << this_dist << " = getDist(" << cur_wpt << ")"
	    << "  ratio returned is " << ratio;
	pd(oss.str(), rmsg::updateState, 1);
      }
      if (ratio == 0 || ratio == -1)
	{
	  cur_wpt--;
	  {
	    ostringstream oss;
	    oss << "herman::updateState_center stepping backward, now at wpt " << cur_wpt
		<< " ratio " << ratio;
	    pd(oss.str(), rmsg::updateState, 1);
	  }
	}
      else
	break;
      if (cur_wpt < 0)
	{
	  cur_wpt = 0;
	  ratio = 0;
	  break;
	}
    }

  /** then, step forward along the trackline, if necessary */
  while (true)
    {
      this_dist = getDistToCorridorSegment(cur_wpt, new_state,
					   cur_dist_from_start, ratio);
      {
	ostringstream oss;
	oss << "  (fwd)  " << this_dist << " = getDist(" << cur_wpt << ")"
	    << "  ratio returned is " << ratio;
	pd(oss.str(), rmsg::updateState, 1);
      }
      if (ratio > .99999 || ratio == -1)
	{
	  cur_wpt++;
	  {
	    ostringstream oss;
	    oss << "herman::updateState_center stepping forward, now at wpt " << cur_wpt
		<< " ratio " << ratio;
	    pd(oss.str(), rmsg::updateState, 1);
	  }
	}
      else
	break;
      if (cur_wpt >= numTargetPoints - 1)
	{
	  cur_wpt = numTargetPoints - 1;
	  ratio = 1;
	  break;
	}
    }
      {
	ostringstream oss;
	oss << "herman::updateState_center stepping finished at wpt " << cur_wpt
	    << " ratio " << ratio;
	pd(oss.str(), rmsg::updateState, 1);
      }


  /** Second, if that attempt failed, then do an exhaustive search
   * of all RDDF corridors and use the best one.
   * We'll define a failed attempt by being more than some number
   * of rddf path widths away from the centerline of the corridor */
  if (this_dist > targetPoints[cur_wpt].radius * MAX_DIST_FROM_PATH_ASPECT_RATIO)
    {
      // crap... gotta do an exhaustive search
      ostringstream oss;
      oss << "Too far from corridor centerline (" << this_dist
	  << "m > " << targetPoints[cur_wpt].radius * MAX_DIST_FROM_PATH_ASPECT_RATIO
	  << " m), doing exhaustive search... ";
      
      cur_wpt = numTargetPoints - 1;
      cur_dist_from_start = 0;
      int best_wpt_so_far = cur_wpt;
      double best_dist_so_far = DBL_MAX;
      double best_dist_from_start = 0;
      double junk;

      while (cur_wpt >= 0)
	{
	  this_dist = getDistToCorridorSegment(cur_wpt, new_state,
					       cur_dist_from_start, junk);
	  
	  if (this_dist < best_dist_so_far)
	    {
	      // we found a new best
	      best_dist_so_far = this_dist;
	      best_wpt_so_far = cur_wpt;
	      best_dist_from_start = cur_dist_from_start;
	    }
	    --cur_wpt;

	}

      oss << "new best is wpt " << best_wpt_so_far << " at dist of "
	  << best_dist_so_far << "m.";
      pd(oss.str(), rmsg::updateState, 0);

      cur_wpt = best_wpt_so_far;
      cur_dist_from_start = best_dist_from_start;
    }

  m_herman_state->cur_waypoint = cur_wpt;
  m_herman_state->cur_dist_from_start = cur_dist_from_start;
  pd("herman::updateState_center finish", rmsg::fncall, 1);
}



void herman::updateState_blob(const NEcoord & new_state)
{
  pd("herman::updateState_blob start", rmsg::fncall, 1);

  int cur_wpt = m_herman_state->cur_waypoint;
  double n = new_state.N;
  double e = new_state.E;

  /** see if we can enlarge the corridor to include ourselves in
   * this segment, or the previous or next */
  double ratio = 1;
  bool within_nearby_corridor = false;
  while (ratio < CORRIDOR_ENLARGEMENT_MAX)
    {
      within_nearby_corridor = isPointInCorridor(cur_wpt - 1, n, e, ratio)
	|| isPointInCorridor(cur_wpt, n, e, ratio)
	|| isPointInCorridor(cur_wpt + 1, n, e, ratio);
      if (within_nearby_corridor)
	break;
      else
	ratio *= CORRIDOR_ENLARGEMENT_STEP;
    }

  if (within_nearby_corridor)
    {
      ostringstream oss;
      oss << "herman::updateState_blob is within nearby corridor to wpt " << cur_wpt;
      pd(oss.str(), rmsg::updateState, 1);
    }
  else
    {
      ostringstream oss;
      oss << "herman::updateState_blob is not within nearby corridor to wpt " << cur_wpt;
      pd(oss.str(), rmsg::updateState, 1);
    }



  if (within_nearby_corridor)
    {
      {
	ostringstream oss;
	oss << "herman::updateState_blob stepping, starting at wpt " << cur_wpt;
	pd(oss.str(), rmsg::updateState, 1);
      }    

      /** First, attempt to move forward or backward within the corridor's
       * blobbed area, one point at a time */

      /** first step back along the corridor, if possible */
      while (true)
	{
	  bool in_prev_segment = isPointInCorridor(cur_wpt - 1, n, e, ratio);
	  if (in_prev_segment)
	    {
	      cur_wpt--;
	      ostringstream oss;
	      oss << "  moving back to wpt " << cur_wpt;
	      pd(oss.str(), rmsg::updateState, 1);
	    }
	  else
	    break;
	}

      /** then step forward, if possible */
      while (true)
	{
	  bool in_next_segment = isPointInCorridor(cur_wpt + 1, n, e, ratio);
	  if (in_next_segment)
	    {
	      cur_wpt++;
	      ostringstream oss;
	      oss << "  moving forward to wpt " << cur_wpt;
	      pd(oss.str(), rmsg::updateState, 1);
	    }
	  else
	    break;
	}

      {
	ostringstream oss;
	oss << "herman::updateState_blob stepping finished at wpt " << cur_wpt;
	pd(oss.str(), rmsg::updateState, 1);
      }
    }


  else
    {
      /** need to do exhaustive search */
      ostringstream oss;
      oss << "Not within corridor, doing exhaustive search...\n";

      double junk;
      double ratio;
      double dist =  exhaustiveNearestSearch(cur_wpt, NEcoord(n, e), junk, ratio);

      oss << "new best is wpt " << cur_wpt << " at dist of "
	  << dist << "m.";
      pd(oss.str(), rmsg::updateState, 0);
    }

  /** set our state to be what we just found */
  m_herman_state->cur_waypoint = cur_wpt;
  m_herman_state->cur_dist_from_start = -1234; // < not really used 

  pd("herman::updateState_blob finish", rmsg::fncall, 1);
}



/* returns current point */
int herman::getCurrentPoint()
{
  pd("herman::getCurrentPoint", rmsg::fncall, 1);
  return m_herman_state->cur_waypoint;
}

