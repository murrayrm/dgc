wp = plot_corridor('DARPA_QID_RDDF_ZERO_BASED.bob')

hold on

% plot the original versions
plot_corridor('qid_practice_straight1.bob',wp(1,:))
plot_corridor('qid_practice_straight2.bob',wp(1,:))

plot_corridor('qid_practice_sturn.bob',wp(1,:))

plot_corridor('qid_practice_uturn_grass.bob',wp(1,:))
plot_corridor('qid_practice_uturn_paved.bob',wp(1,:))

title('practice routes')

% plot the updated versions
%plot_corridor('qid_practice_straight1_v2.bob',wp(1,:))
%plot_corridor('qid_practice_straight2_v2.bob',wp(1,:))
%
%plot_corridor('qid_practice_sturn_v2.bob',wp(1,:))
%
%plot_corridor('qid_practice_uturn_grass_v2.bob',wp(1,:))
%plot_corridor('qid_practice_uturn_road_v2.bob',wp(1,:))
