#ifndef HERMAN_HH
#define HERMAN_HH

#include "sn_msg.hh"
#include <iostream>
#include <iomanip>
#include <string>
#include <float.h>
#include <sstream>
#include "SkynetContainer.h"
#include "DGCutils"
#include "raid.hh"
#include "rddf.hh"
#include "VehicleState.hh"


/** which update state method to use */
#define USE_BLOBBED_UPDATE_STATE           1 /**< use new method */

/** updateState_center options */
#define MAX_DIST_FROM_PATH_ASPECT_RATIO 10.0

/** updateState_blob options */
#define CORRIDOR_ENLARGEMENT_STEP        1.2  /**< better be > 1 */
#define CORRIDOR_ENLARGEMENT_MAX           3  /**< better be > 1 */

using namespace std;


/** 
 * Herman is the class which handles functions related to navigating
 * the rddf.
 *
 * "California is undoubtedly an island. Why, I have had in my office
 *  mariners who have sailed round it"
 *    --Herman Moll, 1711
 */
class herman : virtual public raid, public RDDF
{
public:
  /** Default constructor.  Reads in the RDDF specified by
   * the macro RDDF_FILE in GlobalConstants.h */
  herman();

  /** This contructore allows you to specify which RDDF file to use. */
  herman(char* rddf_file);

  /** Standard destructor */
  ~herman();

  /** Updates where in the RDDF herman thinks we are */
  void updateState(const VehicleState & new_state);
  void updateState(const NEcoord & new_state);
  void updateState(double n, double e);

  /** the two actual methods we might use... these are called
   * by updateState*/
  void updateState_center(const NEcoord & new_state);
  void updateState_blob(const NEcoord & new_state);

  /** Returns the current point in the rddf */
  int getCurrentPoint();

private:
  /** 
   * VARIABLES
   */

  /** Keeps track of where we are in the corridor.  The reason we need
   * some concept of state is so that:
   * 1. We can deal with overlapping corridors.  The state will will
   *    let us keep track of which part of the corridor we're on.
   * 2. We can more efficiently call updateState.  If we've only moved
   *    a little bit, we don't need to do an exhaustive search to find
   *    the nearest point on the trackline. */
  struct hermanState
  {
    /** cur_waypoint is the waypoint at the beginning of the corridor
     * segment that we're closest to (as determinted by the
     * updateState algorithm). */
    int cur_waypoint;

    /** The distance along the trackline from the start of the RDDF, 
     * as last calculated by updateState. */
    double cur_dist_from_start;
  };
  hermanState* m_herman_state;



  /**
   * HELPER FUNCTIONS
   */

  /** Constructor helper.  Called by all herman constructors */
  void hermanInit();


};

#endif  // HERMAN_HH
