#include"DBS.hh"
#include<string>
using namespace std;

int NOSKYNET = 0;                /**< for command line argument */

int main(int argc, char** argv)
{
  // stick the whole thing in a try loop, so we can catch errors
  try {
    for (int i = 1; i < argc; ++i)
      {
	string arg(argv[i]);
      
	if (arg == "h" || arg == "-h" || arg == "--help" || arg == "help")
	  {
	    /* User has requested usage information. Print it to standard
	       output, and exit with exit code zero. */
	    cout << "| Command line options:\n"
		 << "| (you must type command line options just as they appear in this help\n"
		 << "| screen; do not preceed them with some arbitrary number of -'s.\n"
		 << "===============================================================\n"
		 << "  h,-h,help,--help    Prints this.\n"
		 << "  k, noskynet         Doesn't send any skynet messages.  We still need skynet\n"
		 << "                         to run, because we need state.\n"
		 << "  \n"
		 << "| Additional info:\n"
		 << "==============================================================\n"
		 << "  \n"
		 << "==============================================================\n"
		 << "  Current RDDF:  ";
	    cout.flush();
	    cout << "Current SKYNET_KEY:  ";
	    cout.flush();
	    system("echo $SKYNET_KEY");
	    cout << endl;
	    return 0;
	  }
	else if (arg == "k" || arg == "noskynet")
	  {
	    cout << "NOT SENDING SKYNET MESSAGES!" << endl;
	    NOSKYNET = 1;
	  }
	else
	  cout << "UNKNOWN SWITCH: \"" << arg << "\"" << endl;
      }

    //Setup skynet
    int intSkynetKey = 0;

    char* ptrSkynetKey = getenv("SKYNET_KEY");
  
    if(ptrSkynetKey == NULL)
      {
	cout << "Unable to get skynet key!" << endl;
	return 1;
      }
    else
      {
	//cout << "Got to RddfPathGen int main" << endl;
	intSkynetKey = atoi(ptrSkynetKey);
	DBS dbs(intSkynetKey);
	VehicleState means, vars;

	while(true) {
	  dbs.stats(means, vars);

	  cout<<"Roll: "<<means.Roll<<" +- "<<vars.Roll<<endl;
	  cout<<"Pitch: "<<means.Pitch<<" +- "<<vars.Pitch<<endl;
	  cout<<"RollRate: "<<means.RollRate<<" +- "<<vars.RollRate<<endl;
	  cout<<"PitchRate: "<<means.PitchRate<<" +- "<<vars.PitchRate<<endl;          cout<<"Vel_D: "<<means.Vel_D<<" +- "<<vars.Vel_D<<endl;
	  cout<<"Acc_D: "<<means.Acc_D<<" +- "<<vars.Acc_D<<endl;
	  
	  
	  usleep(1000);
	}
	
      }
    
  } // end of try {
  catch (std::string message)
    {
      cout << "Caught exception, error was \"" << message << "\"" << endl;
      return 1;
    }
  
  return 0;
}
