global norm;
rs = [];
for i = [1:6]
  disp(i)
  figure(i); norm = i; init_dbs; dbsfns; manual_data;
  [r, beta, x1, x2, y1, y2] = fitme(manual_data);
  rs = [rs, r];
end
