## loads data

##
## Runs at santa anita on 09_01
##
dirs{1} = ('/tmp/test_logs/2005_09_01/19_16_37');
dirs{2} = ('/tmp/test_logs/2005_09_01/19_17_03');
dirs{3} = ('/tmp/test_logs/2005_09_01/19_18_46');
dirs{4} = ('/tmp/test_logs/2005_09_01/19_20_07');
dirs{5} = ('/tmp/test_logs/2005_09_01/19_21_41');
dirs{6} = ('/tmp/test_logs/2005_09_01/19_26_27');
dirs{7} = ('/tmp/test_logs/2005_09_01/19_27_29');
dirs{8} = ('/tmp/test_logs/2005_09_01/19_28_47');
## pavement runs
for i = 1:8
#  pave{i} = load(strcat(dirs{i}, '/DBS/dbs.dat'));
end

dirs{1} = ('/tmp/test_logs/2005_09_01/19_36_45');
dirs{2} = ('/tmp/test_logs/2005_09_01/19_37_38');
dirs{3} = ('/tmp/test_logs/2005_09_01/19_38_42');
dirs{4} = ('/tmp/test_logs/2005_09_01/19_40_18');
dirs{5} = ('/tmp/test_logs/2005_09_01/19_41_37');
dirs{6} = ('/tmp/test_logs/2005_09_01/19_43_12');
dirs{7} = ('/tmp/test_logs/2005_09_01/19_54_43');
dirs{8} = ('/tmp/test_logs/2005_09_01/19_56_26');
## block runs
for i = 1:8
#  wood{i} = load(strcat(dirs{i}, '/DBS/dbs.dat'));
end

dirs{1} = ('/tmp/test_logs/2005_09_01/20_16_39');
dirs{2} = ('/tmp/test_logs/2005_09_01/20_18_08');
dirs{3} = ('/tmp/test_logs/2005_09_01/20_19_02');
## parking stop runs
for i = 1:3
#  conc{i} = load(strcat(dirs{i}, '/DBS/dbs.dat'));
end


##
## Runs on the long_loop on 09_10
##
dirs{1} = ('/tmp/test_logs/2005_09_10/14_04_31');
dirs{2} = ('/tmp/test_logs/2005_09_10/14_11_40');
dirs{3} = ('/tmp/test_logs/2005_09_10/14_36_31');
dirs{4} = ('/tmp/test_logs/2005_09_10/14_39_57');
dirs{5} = ('/tmp/test_logs/2005_09_10/21_33_12');
dirs{6} = ('/tmp/test_logs/2005_09_10/21_36_50');
dirs{7} = ('/tmp/test_logs/2005_09_10/21_38_11');
dirs{8} = ('/tmp/test_logs/2005_09_10/21_40_07');
dirs{9} = ('/tmp/test_logs/2005_09_10/21_46_36');
## long loop
for i = 1:9
  long{i} = load(strcat(dirs{i}, '/DBS/dbs.dat'));
end
