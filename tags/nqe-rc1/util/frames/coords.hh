/*! Coordinate system definitions header.  Defines NEcoord and NEDcoord, which 
 ** represent (Northing,Easting) and (Northing,Easting,Down) in the global 
 ** coordinate system.  Also defines XYcoord and XYZcoord, which are the 
 ** identical implementations for local coordinate systems.  Also defines
 ** RPYangle, which represent (Roll,Pitch,Yaw).  See comments below.
 */

#ifndef COORDS_HH
#define COORDS_HH

#include <math.h>
#include <iostream> // for cout
using namespace std;

/*! \brief XYcoord: struct defined for 2-D coordinate points.
 **        For local coordinate system vector representations.
 ** 
 ** In our coordinate system conventions, (X, Y) corresponds to (Northing, 
 ** Easting), respectively, in the global coordinate system, and (Forward, To 
 ** the Right) in vehicle and sensor coordinate systems.  For manipulation of 
 ** vectors in the global coordinate frame, NEcoord is /identical/ in 
 ** implementation to XYcoord, but it's use may make code easier to read.  
 ** XYcoord should be used for local (vehicle or sensor frame) coordinate 
 ** representations, since they don't actually represent Northing and Easting.
 */
struct XYcoord 
{
  /*! The X coordinate.  In our coordinate conventions, this represents 
   ** /Northing/ in the global coordinate system, and /forward/ in the vehicle 
   ** or sensor coordinate systems. */
  double X;
  /*! The Y coordinate.  In our coordinate conventions, this represents 
   ** /Easting/ in the global coordinate system, and /to the right/ in the 
   ** vehicle or sensor coordinate systems. */
  double Y;

  /*! Default constructors. */
  XYcoord() {}
  XYcoord(double X_new, double Y_new) : X(X_new), Y(Y_new) {}

  /*! Copy Constructor. */
  XYcoord(const XYcoord &other) 
  {
    X = other.X;
    Y = other.Y;
  }
  /*! Assignment Operator. */
  XYcoord &operator=(const XYcoord &other) 
  { 
    if (this != &other) 
    {
      X = other.X;
      Y = other.Y;
    }
    return *this;
  }
  /*! Addition operator. */
  XYcoord operator+ (XYcoord param) const
  {
    XYcoord temp;
    temp.X = X + param.X;
    temp.Y = Y + param.Y;
    return temp;
  }
  /*! Subtraction operator. */
  XYcoord operator- (XYcoord param) const
  {
    XYcoord temp;
    temp.X = X - param.X;
    temp.Y = Y - param.Y;
    return temp;
  }
  /*! Scalar product operator. */
  XYcoord operator* (double param) const
  {
    XYcoord ret;
    ret.X = param * X;
    ret.Y = param * Y;
    return ret;
  }
  /*! Inner product operator (dot product). */
  double operator* (XYcoord param) const
  {
    return X * param.X + Y * param.Y;
  }
  /*! Norm accessor function. */
  double norm() const
  {
    return hypot(X,Y);
  }
  /*! Display function. */
  void display() const
  {
    cout << "XYcoord (X, Y) = (" << X << ", " << Y << ")" << endl;
  }
  /*! Destructor. */
  ~XYcoord() {}
};


/*! \brief XYZcoord: struct defined for 3-D coordinate points.
 **        For local coordinate system vector representations.
 ** 
 ** XYZcoord: struct defined for 3-D coordinate points.  In our coordinate 
 ** system conventions, (X,Y,Z) represents (Northing, Easting, Down) in global 
 ** coordinates, with sea-level representing Z=0.  In local coordinates, 
 ** (X,Y,Z) represents (Forward, To the Right, Down).  For manipulation of 
 ** vectors in the global coordinate frame, NEDcoord is /identical/ in 
 ** implementation to XYZcoord, but it's use may make code easier to read.  
 ** XYZcoord should be used for local (vehicle or sensor frame) coordinate 
 ** representations, since they don't actually represent Northing, Easting.
 */
struct XYZcoord 
{
  /*! The X coordinate.  In our coordinate conventions, this represents 
   ** /Northing/ in the global coordinate system, and /forward/ in the vehicle 
   ** or sensor coordinate systems. */
  double X;
  /*! The Y coordinate.  In our coordinate conventions, this represents 
   ** /Easting/ in the global coordinate system, and /to the right/ in the 
   ** vehicle or sensor coordinate systems. */
  double Y;
  /*! The Z coordinate.  In our coordinate conventions, this represents 
   ** /Altitude (positive down)/ in the global coordinate system, and /down/ in 
   ** the vehicle or sensor coordinate systems. */
  double Z;

  /*! Default constructors. */
  XYZcoord() {}
  XYZcoord(double X_new, double Y_new, double Z_new)
    : X(X_new), Y(Y_new), Z(Z_new) {}

  /*! Copy Constructor. */
  XYZcoord(const XYZcoord &other) 
  {
    X = other.X;
    Y = other.Y;
    Z = other.Z;
  }

  /*! Assignment operator */
  XYZcoord &operator=(const XYZcoord &other) 
  { 
    if (this != &other) 
    {
      X = other.X;
      Y = other.Y;
      Z = other.Z;
    }
    return *this;
  }

  /*! Addition operator. */
  XYZcoord operator+ (XYZcoord param) const
  {
    XYZcoord ret;
    ret.X = X + param.X;
    ret.Y = Y + param.Y;
    ret.Z = Z + param.Z;
    return (ret);
  }

  /*! Subtraction operator. */
  XYZcoord operator- (XYZcoord param) const
  {
    XYZcoord ret;
    ret.X = X - param.X;
    ret.Y = Y - param.Y;
    ret.Z = Z - param.Z;
    return (ret);
  }

  /*! Scalar product operator. */
  XYZcoord operator* (double param) const
  {
    XYZcoord ret;
    ret.X = param*X;
    ret.Y = param*Y;
    ret.Z = param*Z;
    return (ret);
  }

  /*! Inner (dot) product operator. */
  double operator* (XYZcoord param) const
  {
    return (X * param.X) + (Y * param.Y) + (Z * param.Z);
  }

  /*! Cross product operator. */
  XYZcoord operator^ (XYZcoord param) const
  {
    XYZcoord temp;
    temp.X = (Y * param.Z) - (Z * param.Y);
    temp.Y = (Z * param.X) - (X * param.Z);
    temp.Z = (X * param.Y) - (Y * param.X);
    return (temp);
  }

  /*! Equality operator. */
  bool operator== (XYZcoord param) const
  {
    bool result;
    result = (X==param.X) && (Y==param.Y) && (Z==param.Z);
    return (result);
  }

  /*! Inequality operator. */
  bool operator!= (XYZcoord param) const
  {
    bool result;
    result = (X!=param.X) || (Y!=param.Y) || (Z!=param.Z);
    return (result);
  }

  /*! Norm accessor function. */
  double norm() const
  {
    return sqrt(X*X + Y*Y + Z*Z);
  }

  /*! Display function. */
  void display() const
  {
    cout << "XYZcoord (X,Y,Z) = ("<<X<<", "<<Y<<", "<<Z<<")" << endl;
  }
  
  /*! Destructor. */
  ~XYZcoord() {}

 
};


/*! \brief NEcoord: struct defined for 2-D coordinate points.
 **        For global (Northing, Easting) coordinate system vector 
 **        representations.
 ** 
 ** NEcoord: struct defined for 2-D coordinate points.  In our coordinate 
 ** system conventions, (X, Y) corresponds to (Northing, Easting), 
 ** respectively, in the global coordinate system, and (Forward, To the Right) 
 ** in vehicle and sensor coordinate systems.  For manipulation of vectors in 
 ** the global coordinate frame, NEcoord is /identical/ in implementation to 
 ** XYcoord, but it's use may make code easier to read.  XYcoord should be used 
 ** for local (vehicle or sensor frame) coordinate representations, since they 
 ** don't actually represent Northing and Easting.
 */
struct NEcoord 
{
  /*! The N coordinate.  In our coordinate conventions, this represents 
   ** /Northing/ in the global coordinate system. */
  double N;
  /*! The E coordinate.  In our coordinate conventions, this represents 
   ** /Easting/ in the global coordinate system. */
  double E;

  /*! Default constructors. */
  NEcoord() {}
  NEcoord(double N_new, double E_new) : N(N_new), E(E_new) {}

  /*! Copy Constructor. */
  NEcoord(const NEcoord &other) 
  {
    N = other.N;
    E = other.E;
  }

  /*! Assignment Operator. */
  NEcoord &operator=(const NEcoord &other) 
  { 
    if (this != &other) 
    {
      N = other.N;
      E = other.E;
    }
    return *this;
  }
  /*! Addition operator. */
  NEcoord operator+ (NEcoord param) const
  {
    NEcoord temp;
    temp.N = N + param.N;
    temp.E = E + param.E;
    return temp;
  }
  /*! Subtraction operator. */
  NEcoord operator- (NEcoord param) const
  {
    NEcoord temp;
    temp.N = N - param.N;
    temp.E = E - param.E;
    return temp;
  }
  /*! Assignment Operator. */
  NEcoord &operator-=(const NEcoord &other) 
  { 
    N -= other.N;
    E -= other.E;
    
    return *this;
  }
  /*! Assignment Operator. */
  NEcoord &operator+=(const NEcoord &other) 
  { 
    N += other.N;
    E += other.E;
    
    return *this;
  }
  /*! Assignment Operator. */
  NEcoord &operator*=(const double mul) 
  { 
    N *= mul;
    E *= mul;
    
    return *this;
  }
  /*! Assignment Operator. */
  NEcoord &operator/=(const double div) 
  { 
    N /= div;
    E /= div;
    
    return *this;
  }
  /*! Scalar product operator. */
  NEcoord operator* (double param) const
  {
    NEcoord ret;
    ret.N = param * N;
    ret.E = param * E;
    return ret;
  }
  /*! Inner product operator (dot product). */
  double operator* (NEcoord param) const
  {
    return N * param.N + E * param.E;
  }
  /*! Scaling. */
  NEcoord operator/ (double param) const
  {
    return NEcoord (N/param, E/param);
  }
  /*! Set new values for N and E */
  void setvals(double new_N, double new_E)
  {
    N = new_N;
    E = new_E;
  }
  /*! Rotation (clockwise) */
  void rotate(double angle) {
    double t,
      cosa = cos(angle),
      sina = sin(angle);
    t = N;
    N = t*cosa + E*sina;
    E = -t*sina + E*cosa;

  }
  /*! Norm accessor function. */
  double norm() const
  {
    return hypot(N,E);
  }
  /*! Display function. */
  void display() const
  {
    cout << "NEcoord (N, E) = (" << N << ", " << E << ")" << endl;
  }
  /*! Destructor. */
  ~NEcoord() {}
};


/*! \brief NEDcoord: struct defined for 3-D coordinate points.
 **        For global coordinate system vector representations.
 ** 
 ** NEDcoord: struct defined for 3-D coordinate points.  In our coordinate 
 ** system conventions, (X,Y,Z) represents (Northing, Easting, Down) in global 
 ** coordinates, with sea-level representing Z=0.  In local coordinates, 
 ** (X,Y,Z) represents (Forward, To the Right, Down).  For manipulation of 
 ** vectors in the global coordinate frame, NEDcoord is /identical/ in 
 ** implementation to XYZcoord, but it's use may make code easier to read.  
 ** XYZcoord should be used for local (vehicle or sensor frame) coordinate 
 ** representations, since they don't actually represent Northing, Easting and 
 ** global Altitude.
 */
struct NEDcoord 
{
  /*! The N coordinate.  In our coordinate conventions, this represents 
   ** /Northing/ in the global coordinate system. */
  double N;
  /*! The E coordinate.  In our coordinate conventions, this represents 
   ** /Easting/ in the global coordinate system. */
  double E;
  /*! The D coordinate.  In our coordinate conventions, this represents 
   ** /Altitude (positive down)/ (a.k.a. "Down") in the global coordinate 
   ** system. */
  double D;

  /*! Default constructors. */
  NEDcoord() {}
  NEDcoord(double N_new, double E_new, double D_new)
    : N(N_new), E(E_new), D(D_new) {}
  /** construct NEDcoord from NEcoord, filling in 0 for D */
  NEDcoord(NEcoord other)
    : N(other.N), E(other.E), D(0) {}

  /*! Copy Constructor. */
  NEDcoord(const NEDcoord &other) 
  {
    N = other.N;
    E = other.E;
    D = other.D;
  }

  /*! Assignment operator */
  NEDcoord &operator=(const NEDcoord &other) 
  { 
    if (this != &other) 
    {
      N = other.N;
      E = other.E;
      D = other.D;
    }
    return *this;
  }

  /*! Addition operator. */
  NEDcoord operator+ (NEDcoord param) const
  {
    NEDcoord ret;
    ret.N = N + param.N;
    ret.E = E + param.E;
    ret.D = D + param.D;
    return (ret);
  }

  /*! Subtraction operator. */
  NEDcoord operator- (NEDcoord param) const
  {
    NEDcoord ret;
    ret.N = N - param.N;
    ret.E = E - param.E;
    ret.D = D - param.D;
    return (ret);
  }

  /*! Scalar product operator. */
  NEDcoord operator* (double param) const
  {
    NEDcoord ret;
    ret.N = param*N;
    ret.E = param*E;
    ret.D = param*D;
    return (ret);
  }

  /*! Inner (dot) product operator. */
  double operator* (NEDcoord param) const
  {
    return (N * param.N) + (E * param.E) + (D * param.D);
  }

  /*! Cross product operator. */
  NEDcoord operator^ (NEDcoord param) const
  {
    NEDcoord temp;
    temp.N = (E * param.D) - (D * param.E);
    temp.E = (D * param.N) - (N * param.D);
    temp.D = (N * param.E) - (E * param.N);
    return (temp);
  }

  /*! Norm accessor function. */
  double norm() const
  {
    return sqrt(N*N + E*E + D*D);
  }

  /*! Display function. */
  void display() const
  {
    cout << "NEDcoord (N,E,D) = ("<<N<<", "<<E<<", "<<D<<")" << endl;
  }
  
  /*! Destructor. */
  ~NEDcoord() {}

 
};


/*! \brief RPYangle: struct defined for 3-D angles.
 **        For local angle representations.
 ** 
 ** RPYangle: struct defined for 3-D angles.  In our angle 
 ** system conventions, (R,P,Y) represents (Roll, Pitch, Yaw).  Note that
 ** Roll is rotation about the X-axis, Pitch is rotation about the Y-axis,
 ** and Yaw is rotation about Z-axis.
 */
struct RPYangle 
{
  /*! The Roll angle.  This is rotation about the X-axis.*/
  double R;
  /*! The Pitch angle.  This is rotation about the Y-axis.*/
  double P;
  /*! The Yaw angle.  This is rotation about the Z-axis.*/
  double Y;

  /*! Default constructors. */
  RPYangle() {}
  RPYangle(double R_new, double P_new, double Y_new)
    : R(R_new), P(P_new), Y(Y_new) {}

  /*! Copy Constructor. */
  RPYangle(const RPYangle &other) 
  {
    R = other.R;
    P = other.P;
    Y = other.Y;
  }

  /*! Assignment operator */
  RPYangle &operator=(const RPYangle &other) 
  { 
    if (this != &other) 
    {
      R = other.R;
      P = other.P;
      Y = other.Y;
    }
    return *this;
  }

  /*! Addition operator. */
  RPYangle operator+ (RPYangle param) const
  {
    RPYangle ret;
    ret.R = R + param.R;
    ret.P = P + param.P;
    ret.Y = Y + param.Y;
    return (ret);
  }

  /*! Subtraction operator. */
  RPYangle operator- (RPYangle param) const
  {
    RPYangle ret;
    ret.R = R - param.R;
    ret.P = P - param.P;
    ret.Y = Y - param.Y;
    return (ret);
  }

  /*! Scalar product operator. */
  RPYangle operator* (double param) const
  {
    RPYangle ret;
    ret.R = param*R;
    ret.P = param*P;
    ret.Y = param*Y;
    return (ret);
  }

  /*! Equality operator. */
  bool operator== (RPYangle param) const
  {
    bool result;
    result = (R==param.R) && (Y==param.Y) && (P==param.P);
    return (result);
  }

  /*! Inequality operator. */
  bool operator!= (RPYangle param) const
  {
    bool result;
    result = (R!=param.R) || (Y!=param.Y) || (P!=param.P);
    return (result);
  }

  /*! Display function. */
  void display() const
  {
    cout << "RPYangle (R,P,Y) = ("<<R<<", "<<P<<", "<<Y<<")" << endl;
  }
  
  /*! Destructor. */
  ~RPYangle() {} 
};



#endif
