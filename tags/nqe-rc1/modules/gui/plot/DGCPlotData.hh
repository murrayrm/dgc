/**
 * DGCPlotData.hh
 * Revision History:
 * 07/07/2005  hbarnor  Created
 * $Id$
 */

#ifndef DGCPLOTDATA_HH
#define DGCPLOTDATA_HH

struct point
{
  double x; 
  double y;
};


#endif // DGCPLOTDATA_HH
