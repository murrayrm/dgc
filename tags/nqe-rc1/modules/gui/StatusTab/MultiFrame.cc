/**
 * MultiFrame.hh
 * Revision History:
 * 05/21/2005  hbarnor  Created
 */

#include "MultiFrame.hh"

MultiFrame::MultiFrame(const Glib::ustring &label)

  : Frame(label),
    myTable(maxRow,maxCol,false)
{
  init();
}

void MultiFrame::init()
{
  curX = 0;
  curY = 0;
  set_shadow_type(Gtk::SHADOW_ETCHED_IN);
  // set the style of the frames
  set_label_align(Gtk::ALIGN_RIGHT, Gtk::ALIGN_TOP); 
  Frame::add(myTable);
}

void MultiFrame::add(Widget &widget)
{
  myTable.attach(widget, curX,curX+1, curY, curY+1,Gtk::AttachOptions(),Gtk::AttachOptions(),0,0);
  if(curX+1 == maxCol)
    {
      curY = (curY+1)%maxRow;
    }
  curX = (curX+1)%maxCol;
  show_all_children();
}
