#include <stdlib.h>
#include <iostream.h>
#include <stdio.h>
#include <unistd.h>
#include <fstream.h>
#include "imu_fastcom.h"

struct raw_IMU
{
      unsigned long long time;
          IMU_DATA data;
};

int main(int argc, char **argv)
{
  if (argc < 3) {
    cout << "Syntax: decompress_imu infile outfile" << endl;
    return 0;
  }
  char infile[100];
  char outfile[100];
  ifstream input_stream;
  ofstream output_stream;

  raw_IMU imu_in;
  IMU_DATA my_imudata;

  unsigned long long imutime;

  sprintf(infile, "%s", argv[1]);
  sprintf(outfile, "%s", argv[2]);

  input_stream.open(infile);
  output_stream.open(outfile);

  output_stream.precision(40);

  while (input_stream) {

    input_stream.read((char*)&imu_in, sizeof(raw_IMU));

    memcpy(&my_imudata, &(imu_in.data), sizeof(IMU_DATA));

    imutime = imu_in.time;

    output_stream << imutime << "\t";
    output_stream << my_imudata.dvx << "\t";
    output_stream << my_imudata.dvy << "\t";
    output_stream << my_imudata.dvz << "\t";
    output_stream << my_imudata.dtx << "\t";
    output_stream << my_imudata.dty << "\t";
    output_stream << my_imudata.dtz << endl;
  }
}

