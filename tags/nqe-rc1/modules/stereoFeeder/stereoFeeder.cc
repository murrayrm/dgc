#include "stereoFeeder.hh"
#include <DGCutils>

int QUIT=0;
int PAUSE=0;
int STEP=0;
int CLEAR=0;
int RESEND=0;

double time_lock;
double time_cap;
double time_rect;
double time_disp;
double time_cloud;
double time_map;
double time_log;
double time_total;

unsigned long long startTime, endTime, resultTime;
unsigned long long startTotal, endTotal, resultTotal;

extern int optSparrow, optPair, optExpCtrl, optLevel;
extern double optDelay, optRate;

#define CALIB_PATH_PREFIX "./config/stereovision/"

#define CALIB_SVSCAL_NAME    "SVSCal.ini"
#define CALIB_SVSPARAMS_NAME "SVSParams.ini"
#define CALIB_CAMCAL_NAME    "CamCal.ini"
#define CALIB_CAMID_NAME     "CamID.ini"

#define CALIB_SHORT_RANGE_SUFFIX ".short"
#define CALIB_LONG_RANGE_SUFFIX  ".long"

#define SUNPARAMS_NAME "SunParams.ini"

#define DEFAULT_LOGGING_FREQUENCY 0.5

#define DEBUG(x) {cout << x << endl;}

using namespace std;

extern char debugFilenamePrefix[200];
extern char logFilenamePrefix[200];
extern char sourceFilenamePrefix[200];
extern char optFormat[10];
extern int logFrames, logRect, logDisp, logState, logMaps, logTime, logVotes;
extern int optDisplay, optPair, optUseCameras, optSim, optFile, optSparrow, optMaxFrames, optMaxLoops, optZeroAltitude, optOneFile;
extern bool waitForState;

double time_ms = 0;
double rate = 0;
int nbrSentMessages = 0;
int isPaused = 0;

int pairIndex;

int lastPair;

StereoFeeder::StereoFeeder(int skynetKey) 
  : CSkynetContainer(MODstereofeeder, skynetKey), CTimberClient(timber_types::stereoFeeder),
		CStateClient(waitForState) {
  
  last_logtime=0;
  prevLogLine = 0;

  //Initialize our calibration file stuff to defined defaults
  sprintf(calibFileObject[SHORT_RANGE].SVSCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSCAL_NAME, CALIB_SHORT_RANGE_SUFFIX);
  sprintf(calibFileObject[SHORT_RANGE].SVSParamFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSPARAMS_NAME, CALIB_SHORT_RANGE_SUFFIX);
  sprintf(calibFileObject[SHORT_RANGE].CamCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMCAL_NAME, CALIB_SHORT_RANGE_SUFFIX);
  sprintf(calibFileObject[SHORT_RANGE].CamIDFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMID_NAME, CALIB_SHORT_RANGE_SUFFIX);

  sprintf(calibFileObject[LONG_RANGE].SVSCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSCAL_NAME, CALIB_LONG_RANGE_SUFFIX);
  sprintf(calibFileObject[LONG_RANGE].SVSParamFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSPARAMS_NAME, CALIB_LONG_RANGE_SUFFIX);
  sprintf(calibFileObject[LONG_RANGE].CamCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMCAL_NAME, CALIB_LONG_RANGE_SUFFIX);
  sprintf(calibFileObject[LONG_RANGE].CamIDFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMID_NAME, CALIB_LONG_RANGE_SUFFIX);

  if(optUseCameras) {
    if(sourceObject.init(0, calibFileObject[optPair].CamIDFilename, logFilenamePrefix, optFormat, calibFileObject[optPair].SVSParamFilename)!=stereoSource_OK) {
      fprintf(stderr, "Error while initializing cameras!  Quitting...\n");
      exit(1);
    }
  }

  char* sunParams;
  char* cutOffsParamsFilename;
  if(optPair==SHORT_RANGE){
    sunParams = "./config/stereovision/SunParams.ini.short";
    cutOffsParamsFilename = "./config/stereovision/PointCutoffs.ini.short";
  }
  else if(optPair == LONG_RANGE){
    sunParams = "./config/stereovision/SunParams.ini.long";
    cutOffsParamsFilename = "./config/stereovision/PointCutoffs.ini.long";
  }
  
  processObject.init(0,
		     calibFileObject[optPair].SVSCalFilename,
		     calibFileObject[optPair].SVSParamFilename,
		     calibFileObject[optPair].CamCalFilename,
		     logFilenamePrefix,
		     optFormat, 0, sunParams, cutOffsParamsFilename);

  if(optSim || optFile) {
    sourceObject.init(0, processObject.imageSize.width, processObject.imageSize.height, sourceFilenamePrefix, optFormat, 1);
  }
  stereoMap.initMap(CONFIG_FILE_DEFAULT_MAP);

  layerID_stereoAvgDbl = stereoMap.addLayer<double>(CONFIG_FILE_STEREO_MEAN_ELEV, true);
  layerID_fusedElev = stereoMap.addLayer<CElevationFuser>(CONFIG_FILE_STEREO_FUSED_ELEV, true); 

  DGCcreateMutex(&cameraMutex);
  
  if(optDisplay) {
    processObject.show();
  }

  if(optExpCtrl) {
    sourceObject.startExposureControl(processObject.subWindow);
  }

  DGCcreateMutex(&mapMutex);

  _estopRun = false;

}


StereoFeeder::~StereoFeeder() {
#warning "Need to implement deconstructor"
  printf("Thank you for using the stereoFeeder destructor\n");
}


void StereoFeeder::ActiveLoop() {
  int socket_num_mean;
  int socketNumFused;
  if (optPair == SHORT_RANGE) {
    socketNumFused  = m_skynet.get_send_sock(SNstereoShortDeltaMap);    
    socket_num_mean = m_skynet.get_send_sock(SNstereoShortDeltaMapElev);
  }
  else if (optPair == LONG_RANGE) {
    socketNumFused  = m_skynet.get_send_sock(SNstereoLongDeltaMap);    
    socket_num_mean = m_skynet.get_send_sock(SNstereoLongDeltaMapElev);
  }

  CDeltaList* deltaPtr = NULL;

  unsigned long long playbackTime = 0;
  unsigned long long newPlaybackTime;

  int lineNum;
  unsigned long long grabTimestamp;
  
  while(!QUIT) {
    if(optSparrow) {
      UpdateSparrowVariablesLoop();
    }
    
    if(!PAUSE) {
      if(optMaxLoops == 0) {
	if(optSparrow) {
	  user_quit(0);
	  sleep(1);
	}
	QUIT = 1;
	return;
      }

  
      if(optMaxLoops>0) optMaxLoops--;
      
      NEDcoord UTMPoint, Point;
      //unsigned long long start, end, result;
    
      DGCgettime(startTotal);
      
      {
	DGClockMutex(&cameraMutex);
	DGCgettime(endTime);  
	resultTime = endTime-startTotal;  
	time_lock = DGCtimetosec(resultTime)*1000;

	if(optFile) {
	  newPlaybackTime = getPlaybackTime();
	  unsigned long long ihate;
	  DGCgettime(ihate);
	  //printf("current time is %llu, dgcgettime is %llu\n", newPlaybackTime, ihate);
	  playbackTime = 0;
	  FILE *stateLogFile = NULL;
	  char stateLogFilename[256];
	  sprintf(stateLogFilename, "%s.state", sourceFilenamePrefix);
	  stateLogFile = fopen(stateLogFilename, "r");
	  if(stateLogFile != NULL) {
	    //while(newPlaybackTime >= playbackTime && !feof(stateLogFile)) {
	    do {  
	      fscanf(stateLogFile, "%d %llu %llu %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
		     &lineNum,
		     &grabTimestamp,
		     &m_state.Timestamp, 
		     &m_state.Northing, &m_state.Easting, &m_state.Altitude,
		     &m_state.Vel_N, &m_state.Vel_E, &m_state.Vel_D,
		     &m_state.Acc_N, &m_state.Acc_E, &m_state.Acc_D,
		     &m_state.Roll, &m_state.Pitch, &m_state.Yaw,
		     &m_state.RollRate, &m_state.PitchRate, &m_state.YawRate,
		     &m_state.RollAcc, &m_state.PitchAcc, &m_state.YawAcc);
	      if (optZeroAltitude == 1) {
		m_state.Altitude = 0;
	      }
	      playbackTime = grabTimestamp;
	      // cout << "feeder: just read line number " << lineNum << " from .state file" << endl;
	      // cout << "checking for file... " << endl;
	      char testFilename[256];
	      sprintf(testFilename, "%s%.5d-L.%s", sourceFilenamePrefix, lineNum, optFormat);
	      FILE *testFile = NULL;
	      if (lineNum > prevLogLine) {
		testFile = fopen(testFilename, "r");
		if (testFile != NULL) {
		  // cout << "and there was a file!" << endl;
		  // cout << "I think I've found a file with name: " << testFilename << endl;
		  fclose(testFile);
		  prevLogLine = lineNum;
		  break;
		} 
	      }
	    } while (!feof(stateLogFile));
	    if (feof(stateLogFile)) {
	      cout << "Stereofeeder: Reached the end of the stereo state log." << endl;
	      exit(0);
	    }
	    //cout << "feeder: Settled for line/file number " << lineNum << endl;
	    if(sourceObject.grab(sourceFilenamePrefix, optFormat, lineNum)) {
	      //cout << "feeder: found a file!" << endl; 
	    }
	    else {
	      cout << "stereoFeeder: did not find a file when it should have!" << endl;
	      exit(1);
	    }
	    fclose(stateLogFile);
	    blockUntilTime(grabTimestamp);
	  } 
	  else {
	    if(optSparrow) {
	      user_quit(0);
	      sleep(1);
	    }
	    QUIT = 1;
	    return;
	  }
	}
	
	DGCgettime(startTime);
	if(stereoProcess_OK != processObject.loadPair(sourceObject.pair(), m_state)) {
	  printf("Error while grabbing images from cameras at line %d in %s!  Quitting...\n", __LINE__, __FILE__);
	  exit(1);
	}	
	DGCgettime(endTime);  
	resultTime = endTime-startTime;  
	time_cap = DGCtimetosec(resultTime)*1000;
 
	pairIndex = sourceObject.pairIndex();
	DGCunlockMutex(&cameraMutex);
      }
	// cout << "starting to process image pair" << endl;
      tempState = processObject.currentState();
      
      if(_estopRun) {
	processObject.pitchControl();

	DGCgettime(startTime);
	processObject.calcRect();	
	DGCgettime(endTime);  
	resultTime = endTime-startTime;  
	time_rect = DGCtimetosec(resultTime)*1000;

	startTime = endTime;
	processObject.calcDisp();
	DGCgettime(endTime);  
	resultTime = endTime-startTime;  
	time_disp = DGCtimetosec(resultTime)*1000;
	
	DGClockMutex(&mapMutex);
	stereoMap.updateVehicleLoc(tempState.Northing, tempState.Easting);
	//      ofstream tempFile("/tmp/UTMPointCloud.txt", ios::app);  	

	DGCgettime(startTime);
	bool pointCloudOK = processObject.generateUTMPointCloud();
	DGCgettime(endTime);  
	resultTime = endTime-startTime;  
	time_cloud = DGCtimetosec(resultTime)*1000;
	
	startTime = endTime;
	if (pointCloudOK) {
	  while(processObject.getNextUTMPoint(&UTMPoint)) {
	    //  tempFile << "N: " << UTMPoint.N << ", E: " << UTMPoint.E << ", D: " << UTMPoint.D << endl;
	    CElevationFuser tempCell = stereoMap.getDataUTM<CElevationFuser>(layerID_fusedElev, UTMPoint.N, UTMPoint.E);
	    double before = tempCell.getMeanElevation();
	    CElevationFuser::CELL_TYPE ctBefore = tempCell.getCellType();
	    tempCell.fuse_MeanElevation(UTMPoint, tempState.Timestamp);
	    stereoMap.setDataUTM_Delta<CElevationFuser>(layerID_fusedElev, UTMPoint.N, UTMPoint.E, tempCell);
	    if((tempCell.getMeanElevation()==1.0 || tempCell.getMeanElevation()==0.0) && tempCell.getCellType() != CElevationFuser::OUTSIDE_MAP)
	      cout << "Holy crap " << setprecision(10) << UTMPoint.N << "," << UTMPoint.E << " = " << tempCell.getMeanElevation()
		   << " and fused in" << UTMPoint.D
		   << " and before was " << before
		   << " and celltype was " << ctBefore << " and now is " << tempCell.getCellType() << endl;
	    stereoMap.setDataUTM_Delta<double>(layerID_stereoAvgDbl, UTMPoint.N, UTMPoint.E, tempCell.getMeanElevation());
	    if(stereoMap.getDataUTM<double>(layerID_stereoAvgDbl, UTMPoint.N, UTMPoint.E) ==
	       stereoMap.getLayerNoDataVal<double>(layerID_stereoAvgDbl)) 
	      printf("%s [%d]: Setting a no data val of %lf into the map!?!?!\n", __FILE__, __LINE__, stereoMap.getDataUTM<double>(layerID_stereoAvgDbl, UTMPoint.N, UTMPoint.E));
	  } 
	}
	else {
	  //	tempFile << "Could not generate UTMPointcloud!" << endl;
	}

	//   tempFile.close();
	
	/*
	  for(int y=processObject.subWindow.y; y<processObject.subWindow.y+processObject.subWindow.height; y++) {
	  //for(int y=0; y<processObject.subWindow.height; y++) {
	  for(int x=processObject.subWindow.x; x<processObject.subWindow.x+processObject.subWindow.width; x++) {
	  //for(int x=0; x<processObject.subWindow.width; x++) {
	  
	  if(processObject.SinglePoint(x, y, &UTMPoint)) {
	  CElevationFuser tempCell = stereoMap.getDataUTM<CElevationFuser>(layerID_fusedElev, UTMPoint.N, UTMPoint.E);
	  double before = tempCell.getMeanElevation();
	  CElevationFuser::CELL_TYPE ctBefore = tempCell.getCellType();
	  tempCell.fuse_MeanElevation(UTMPoint, tempState.Timestamp);
	  stereoMap.setDataUTM_Delta<CElevationFuser>(layerID_fusedElev, UTMPoint.N, UTMPoint.E, tempCell);
	  if((tempCell.getMeanElevation()==1.0 || tempCell.getMeanElevation()==0.0) && tempCell.getCellType() != CElevationFuser::OUTSIDE_MAP)
	  cout << "Holy crap " << setprecision(10) << UTMPoint.N << "," << UTMPoint.E << " = " << tempCell.getMeanElevation()
	  << " and fused in" << UTMPoint.D
	  << " and before was " << before
	  << " and celltype was " << ctBefore << " and now is " << tempCell.getCellType() << endl;
	  stereoMap.setDataUTM_Delta<double>(layerID_stereoAvgDbl, UTMPoint.N, UTMPoint.E, tempCell.getMeanElevation());
	  if(stereoMap.getDataUTM<double>(layerID_stereoAvgDbl, UTMPoint.N, UTMPoint.E) ==
	  stereoMap.getLayerNoDataVal<double>(layerID_stereoAvgDbl)) 
	  printf("%s [%d]: Setting a no data val of %lf into the map!?!?!\n", __FILE__, __LINE__, stereoMap.getDataUTM<double>(layerID_stereoAvgDbl, UTMPoint.N, UTMPoint.E));
	  
	  
	  }
	  }
	  }
	*/
	
	//SKYNET
	deltaPtr = stereoMap.serializeDelta<CElevationFuser>(layerID_fusedElev);
	
	//code to send delta
	if(!deltaPtr->isShiftOnly()) {
	  SendMapdelta(socketNumFused, deltaPtr);
	}
	//stereoMap.resetDelta(layerID_stereoAvgDbl);
	stereoMap.resetDelta<CElevationFuser>(layerID_fusedElev);
	
	deltaPtr = stereoMap.serializeDelta<double>(layerID_stereoAvgDbl);

	//code to send delta
	if(!deltaPtr->isShiftOnly()) {	  
	  ++nbrSentMessages;      
	  SendMapdelta(socket_num_mean, deltaPtr);
	}
	stereoMap.resetDelta<double>(layerID_stereoAvgDbl);
	//stereoMap.resetDelta(layerID_fusedElev);
	DGCunlockMutex(&mapMutex);

	DGCgettime(endTime);  
	resultTime = endTime-startTime;  
	time_map = DGCtimetosec(resultTime)*1000;
	
	startTime = endTime;
	
	if(logRect) processObject.saveRect(logFilenamePrefix, optFormat, pairIndex);
	if(logDisp) processObject.saveDisp(logFilenamePrefix, optFormat, pairIndex);
	if(logMaps) {
	  char logMapsFilename[256];
	  sprintf(logMapsFilename, "%s%.6d", logFilenamePrefix, pairIndex);
	}
	
	time_ms = DGCtimetosec(resultTotal)*1000;
	rate = 1/DGCtimetosec(resultTotal);
	
	if(logTime) {
	  FILE *timeLogFile = NULL;
	  char timeLogFilename[256];
	  sprintf(timeLogFilename, "%s.time", logFilenamePrefix);
	  
	  timeLogFile = fopen(timeLogFilename, "a");
	  if(timeLogFile) {
	    fprintf(timeLogFile, "%.6d %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
		    pairIndex,
		    time_lock,
		    time_cap,
		    time_rect,
		    time_disp,
		    time_cloud,
		    time_map,
		    time_log,
		    time_total);
	    fclose(timeLogFile);
	  }
	}
	DGCgettime(endTime);  
	resultTime = endTime-startTime;  
	time_log = DGCtimetosec(resultTime)*1000;

	DGCgettime(endTotal);
	resultTotal = endTotal-startTotal;  
	time_total = DGCtimetosec(resultTotal)*1000;
      }
    }
  }
}
  
void StereoFeeder::ImageCaptureThread() {
  unsigned long long grabTimestamp;

  while(true && !QUIT) {
    if(!PAUSE && (optUseCameras || optSim)) {
      if(optMaxFrames == 0) {
	if(optSparrow) {
	  user_quit(0);
	  sleep(1);
	}
	QUIT = 1;
	return;
      }
      
      if(optMaxFrames>0) optMaxFrames--;
      
      {
	DGClockMutex(&cameraMutex);
	if(optUseCameras) {
	  grabTimestamp = sourceObject.grab();
	  UpdateState(grabTimestamp);
	  if(optZeroAltitude)
	    m_state.Altitude = 0;
	} else if(optSim) {
	  usleep(33000);
	  if(sourceObject.grab() || true) {
	    FILE *stateLogFile = NULL;
	    char stateLogFilename[256];
	    sprintf(stateLogFilename, "%s.state", sourceFilenamePrefix);
	    stateLogFile = fopen(stateLogFilename, "r");
	    if(stateLogFile != NULL) {
	      int lineNum=-1;
	      while(lineNum != sourceObject.pairIndex()-1) {
		fscanf(stateLogFile, "%d: %lld %lld %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
		       &lineNum,
		       &grabTimestamp,
		       &m_state.Timestamp, 
		       &m_state.Northing, &m_state.Easting, &m_state.Altitude,
		       &m_state.Vel_N, &m_state.Vel_E, &m_state.Vel_D,
		       &m_state.Acc_N, &m_state.Acc_E, &m_state.Acc_D,
		       &m_state.Roll, &m_state.Pitch, &m_state.Yaw,
		       &m_state.RollRate, &m_state.PitchRate, &m_state.YawRate,
		       &m_state.RollAcc, &m_state.PitchAcc, &m_state.YawAcc);
	      }
	      fclose(stateLogFile);
	    }
	    if(optOneFile) {
	      DGCunlockMutex(&cameraMutex);
	      return;
	    }
	  } else {
	    if(optSparrow) {
	      user_quit(0);
	      sleep(1);
	    }
	    QUIT = 1;
	    return;
	  }
	}
	DGCunlockMutex(&cameraMutex);
      }

      if(getLoggingEnabled()) {
	if(checkNewDirAndReset()) {
	  logFrames = 1;
	  logState = 1;
	  sprintf(logFilenamePrefix, "%sstereo", getLogDir().c_str());
	}
      } else {
	logFrames = 0;
	logState = 0;
      }
      
      
      if(logFrames) {
	int level = getMyLoggingLevel();
	//int level =optLevel;
	if( level >=3){
	  DGCgettime(last_logtime);
	  sourceObject.save(logFilenamePrefix, optFormat, sourceObject.pairIndex());
	}      
	else if(level==2){
	  unsigned long long current_time;
	  DGCgettime(current_time);
	  if((double)(current_time -last_logtime)> (1000000.0/(double)DEFAULT_LOGGING_FREQUENCY)){
	    last_logtime = current_time;
	    sourceObject.save(logFilenamePrefix, optFormat, sourceObject.pairIndex());
	  }
	}
      }
      if(logState) {
	FILE *stateLogFile = NULL;
	char stateLogFilename[256];
	sprintf(stateLogFilename, "%s.state", logFilenamePrefix);
	
	stateLogFile = fopen(stateLogFilename, "a");
	if(stateLogFile) {
	  fprintf(stateLogFile, "%.6d %llu %llu %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
		  sourceObject.pairIndex(),
		  grabTimestamp,
		  m_state.Timestamp, 
		  m_state.Northing, m_state.Easting, m_state.Altitude,
		  m_state.Vel_N, m_state.Vel_E, m_state.Vel_D,
		  m_state.Acc_N, m_state.Acc_E, m_state.Acc_D,
		  m_state.Roll, m_state.Pitch, m_state.Yaw,
		  m_state.RollRate, m_state.PitchRate, m_state.YawRate,
		  m_state.RollAcc, m_state.PitchAcc, m_state.YawAcc);
	  fclose(stateLogFile);
	}
      }
      usleep(100);
      if(optDelay!=-1) {
	usleep((unsigned int)(optDelay*1e6));
      }
      if(optRate!=-1) {
	usleep((unsigned int)((1/optRate)*1e6));
      }
    }
  }    
}
  
void StereoFeeder::ReceiveDataThread_EStop() {
  
  while(!QUIT) {
    WaitForNewActuatorState();
    if(m_actuatorState.m_estoppos == 2 && m_state.Speed2() > 0.1) {
      _estopRun = true;
      isPaused = 0;
    } else{
      _estopRun = false;
      isPaused = 1;
      time_rect = time_disp = time_cloud = time_map = 0;
    }
    
  }
}
