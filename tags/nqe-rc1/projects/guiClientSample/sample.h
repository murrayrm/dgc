#include "sampleTabSpecs.h"
#include "TabClient.h"

class CSampleClient : public CModuleTabClient
{
	SSampleTabInput  m_input;
	SSampleTabOutput m_output;

public:
	CSampleClient(int skynet_key);
	void getDataThread(void);
	void activeLoop(void);
	void updatePlot(void);
};

