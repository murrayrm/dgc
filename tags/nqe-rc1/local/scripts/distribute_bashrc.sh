#!/bin/bash

# This script copies your local .bashrc file to a default set of race computers
# or a string of hosts given as argument.
#
# Created: Oct 20, 2004
# Author: Henrik Kjellander

RACE_COMPUTERS="co ti ta mg ir os"

if [[ ! "$1" ]]; then
  echo Usage: $0 username \"hosts separated by spaces\"]
  echo The second argument is optional, if not given this will be used: $RACE_COMPUTERS 
  echo Example 1: $0 henrik
  echo Example 2: $0 henrik "titanium magnesium"
  echo
  echo "Requirements: You must have the short names of the race computers as "
  echo "              aliases in your /etc/hosts file. If you don't, check out "
  echo "              the dgc/local/files/hosts file and use that."
  echo 
  echo "Recommendation: To easily use this script, first run the ssh key "
  echo "                exchange script in"
  echo "                dgc/local/scripts/distribute_sshkeys.sh"
  exit 0
fi

USER=$1

cd ~

for comp in $RACE_COMPUTERS
do
  echo Copying .bashrc to: $comp
  scp .bashrc $USER@$comp:.
done

echo Finished copying .bashrc to: $RACE_COMPUTERS

