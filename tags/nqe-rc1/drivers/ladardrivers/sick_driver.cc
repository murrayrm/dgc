#include "DGCutils"
#include "sick_driver.h"


#define LOBYTE(w) ((uint8_t) (w & 0xFF))
#define HIBYTE(w) ((uint8_t) ((w >> 8) & 0xFF))

#define MAKEUINT16(a,b) ((unsigned short)(a)|((unsigned short)(b)<<8))
#define DEBUG(c) cerr << c << endl

#define MAX_RANGE_SICK_LMS_221 8183
#define MIN_RANGE_SICK_LMS_221 150

// Assuming that the scan data gets transmitted at the start of the next cycle,
// and takes most of the cycle to actually transmit. 1/75 s = 13.333ms
#define TIME_FROM_SCANSTART_TO_DATAEND 25000ULL

#define TIME_PER_ANGLE 1000000.0/75.0/(2.0*M_PI)

SickLMS200::SickLMS200()
{
  m_bHaveNewScan = false;
  DGCcreateCondition(&m_newScanCondition);
  DGCcreateMutex(&m_newScanCondMutex);
  DGCcreateMutex(&m_laserDataMutex);

 	m_laserData.Init(sizeof(laser_data_t));
	m_timestampsSum        = 0ULL;
	m_timestampBufferIndex = 0;
	m_bTimestampBufferFull = false;

	_errorMessage = "SICK - No errors detected";
}
SickLMS200::~SickLMS200()
{
  DGCdeleteCondition(&m_newScanCondition);
  DGCdeleteMutex(&m_newScanCondMutex);
  DGCdeleteMutex(&m_laserDataMutex);
}

int SickLMS200::getNumDataPoints()
{
  return scan_num_segments;
}

int SickLMS200::ParseConfigFile(char *filename) {
  FILE *cf = fopen(filename, "r");
  char line[MAX_CONFIG_FILE_LINE];
  char name[MAX_CONFIG_FILE_LINE];
  char value[MAX_CONFIG_FILE_LINE];
  
  memset(device_name, 0, MAX_CONFIG_FILE_LINE);

  if (cf != NULL) {
    while(fgets(line, sizeof(line), cf)) {
      sscanf(line, "%s %s\n", name, value);
      if(strcmp(name, "serialnum:") == 0) {
				strcpy(device_name,value);
      } else if (strcmp(name, "rate:") == 0) {
				port_rate = atoi(value);
      } else if (strcmp(name, "delay:") == 0) {
				startup_delay = atoi(value);
      } else if (strcmp(name, "resolution:") == 0) {
				scan_res = lround(atof(value)*100.0);
				_resolution = atof(value);
				_scanAngleOffset = ((180.0 - (double)_scanAngle)/2.0) * M_PI/180.0;
				if (scan_res != 25 &&
						scan_res != 50 &&
						scan_res != 100)
				{
					cerr << "Invalid angular resolution: " << scan_res << endl; 
				} 
      } else if (strcmp(name, "units:") == 0) {
				range_res = lround(atof(value)*1000.0);
				_ladarUnits = atof(value);
      } else if (strcmp(name, "scan_angle:") == 0) {
				scan_width = atoi(value);
				_scanAngle = scan_width;
      } else if (strcmp(name, "authority:")==0) {
	_ladarAuthority = (LADAR_AUTHORITY_TYPE)atoi(value);
      } else {
				//	cerr << "Invalid scan parameter " << name << endl;
      }
    }
  } else {
    return 1;
  }
  fclose(cf);
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
// Set up the device
int SickLMS200::Setup(char *filename)
{
	cerr << "Entering SickLMS200::Setup()" << endl;
  // Set the defaults first, then overwrite as necessary with configurations.
  port_rate = DEFAULT_LASER_PORT_RATE;

  _logFileRaw = NULL;
  _ladarAuthority = NEITHER;

	sprintf(_configFilename, "%s", filename);

#ifdef HAVE_HI_SPEED_SERIAL
  can_do_hi_speed = true;
#else
  can_do_hi_speed = false;
#endif
  
  startup_delay = 0;
  scan_width = 180;
  scan_res = 50;
  min_angle = -9000;
  max_angle = +9000;
  scan_min_segment = 0;
  scan_max_segment = 360;
  intensity = true;
  range_res = 10;

  ParseConfigFile(filename);

  if (device_name == NULL) {
    cerr << "need device serial number for this ladar. fuck" << endl;
  }
  
  if (!can_do_hi_speed && port_rate > 38400)
	{      
		cerr << "sicklms200: requested hi speed serial, but no support compiled in. Defaulting to 38400 bps." << endl;
		port_rate = 38400;
	}

  if (!CheckScanConfig()) {
    cerr << "invalid scan configuration" << endl;
  }

  int rate = 0;
    
  // Open the terminal
  if (OpenTerm()) {
		cerr << "SickLMS200::Setup(): OpenTerm() failed" << endl;
    return 1;
	}
	SetNonBlockingMode();


  // Some Pioneers only power laser after the terminal is opened; wait
  // for the laser to initialized
  sleep(startup_delay);
  
  // Try connecting at the given rate
  if (ChangeTermSpeed(port_rate)) {
		cerr << "SickLMS200::Setup(): ChangeTermSpeed() failed" << endl;
    return 1;
	}

  if (EnterInstallationMode()) {
    rate = port_rate;
	} else {
		cerr << "SickLMS200::Setup(): EnterInstallationMode() failed: Couldn't communicate at 500000 baud" << endl;
	}

  // Try connecting at 9600
  if (rate == 0 && port_rate != 9600) {
    if (ChangeTermSpeed(9600)) {
			cerr << "SickLMS200::Setup(): ChangeTermSpeed() failed" << endl;
      return 1;
		}
    if (EnterInstallationMode())
      rate = 9600;
		else
			cerr << "SickLMS200::Setup(): EnterInstallationMode() failed: Couldn't communicate at 9600 baud" << endl;
  } 

  // Try connecting at 38400
  if (rate == 0 && port_rate >= 38400) {
    if (ChangeTermSpeed(38400)) {
			cerr << "SickLMS200::Setup(): ChangeTermSpeed() failed" << endl;
      return 1;
		}
    if (EnterInstallationMode())
      rate = 38400;
		else
			cerr << "SickLMS200::Setup(): EnterInstallationMode() failed:  Couldn't communicate at 38400 baud" << endl;
  }
  
  // Try connecting at 500000
  if (rate == 0 && port_rate >= 500000 && can_do_hi_speed) {
    if (ChangeTermSpeed(500000)) {
			cerr << "SickLMS200::Setup(): ChangeTermSpeed() failed" << endl;
      return 1;
		}
    if (EnterInstallationMode())
      rate = 500000;
		else
			cerr << "SickLMS200::Setup(): EnterInstallationMode() failed: Couldn't communicate at 500000 baud. Again" << endl;
  }

  if (rate == 0)
  {
		cerr << "SickLMS200::Setup(): Could not find the laser failed" << endl;
    return 1;
  }

  // Jump up to 38400 or 500000
  if ( (rate != 38400 && port_rate == 38400) ||
			 (rate != 500000 && port_rate == 500000 && can_do_hi_speed)
		 )
  {
    if (!SetLaserSpeed(port_rate))
		{
			cerr << "SickLMS200::Setup(): SetLaserSpeed(port_rate) failed" << endl;
      return 1;
		}
    sleep(1);
    if (ChangeTermSpeed(port_rate))
		{
			cerr << "SickLMS200::Setup(): ChangeTermSpeed(port_rate) failed" << endl;
      return 1;
		}
    sleep(1);
  }
  else if (rate != port_rate)
  {
		cerr << "SickLMS200::Setup(): don't know the rate" << endl;
    return 1;
  }

  // Display the laser type
  char type[64];
  if (!GetLaserType(type, sizeof(type)))
	{
		cerr << "SickLMS200::Setup(): GetLaserType() failed" << endl;
    return 1;
  }

	cout << "Detected a " << type << endl;
	
	//Reset();
  	DumpErrors();
  // Configure the laser
  if (!SetLaserRes(scan_width, scan_res))
	{
		cerr << "SickLMS200::Setup(): SetLaserRes() failed" << endl;
    return 1;
  }
  if (!SetLaserConfig(intensity))
	{
		cerr << "SickLMS200::Setup(): SetLaserConfig() failed" << endl;
    return 1;
  }

	scan_num_segments = scan_max_segment - scan_min_segment + 1;

	//DumpConfig();

  // Start the device thread
  DGCstartMemberFunctionThreadInRealtime(this, &SickLMS200::MainThread);

	cerr << "Exited SickLMS200::Setup() successfully" << endl;
  return 0;
}

////////////////////////////////////////////////////////////////////////////////
// Main function for device thread
void SickLMS200::MainThread(void) 
{
  // Ask the laser to send data
  if (!RequestLaserData())
	{
		cerr << "void SickLMS200::MainThread(void): RequestLaserData() failed. exiting thread" << endl;
    return;
	}

	SetBlockingMode();

  laser_data_t localLaserData;
  while (true)
  {
		if (ReadScanFromLaser(localLaserData.ranges, MAX_LADAR_SCAN))
    {
			if(m_bTimestampBufferFull)
			{
				// Get the time at which the blocking read ended. I'm assuming that the
				// data is TIME_FROM_SCANSTART_TO_DATAEND late
#warning "This assumes scans at 75hz"
				localLaserData.ladar_timestamp = m_timestampsSum/SIZE_TIMESTAMP_BUFFER + 1000000/2/75*(SIZE_TIMESTAMP_BUFFER-1) - TIME_FROM_SCANSTART_TO_DATAEND;

				// Prepare packet and byte swap
				localLaserData.min_angle = (scan_min_segment * scan_res - scan_width * 50);
				localLaserData.max_angle = (scan_max_segment * scan_res - scan_width * 50);
				localLaserData.resolution = scan_res;
				localLaserData.range_count = scan_num_segments;
				localLaserData.range_res = ((float) range_res)/1000.0f;
			  if(_logFileRaw!=NULL) {
			    fprintf(_logFileRaw, "%llu ", localLaserData.ladar_timestamp);
			    fprintf(_logFileRaw, "%d ", localLaserData.range_count);
			  }


				for (int i = 0; i < localLaserData.range_count; i++)
				{
					localLaserData.intensity[i] = ((localLaserData.ranges[i] >> 13) & 0x000E);
					localLaserData.ranges[i] &= 0x1FFF;
					if(_logFileRaw!=NULL) {
						fprintf(_logFileRaw, "%d ", localLaserData.ranges[i]);
					}
				}

				if(_logFileRaw!=NULL) {
					fprintf(_logFileRaw, "\n");
				}
				// Make data available
				DGClockMutex(&m_laserDataMutex);
				m_laserData.FillMsg((char*)&localLaserData, sizeof(laser_data_t));
				DGCSetConditionTrue(m_bHaveNewScan, m_newScanCondition, m_newScanCondMutex);
				DGCunlockMutex(&m_laserDataMutex);
			}
		}
	}
}

int SickLMS200::GetScanFrame(double* ranges, 
			     double* angles, 
			     unsigned long long* latestScanTimestamp,
														 unsigned long long* timestamps,
			     char* errorMessage) {
  laser_data_t scan_frame;

  // wait for a new scan if the buffer is empty
	if(m_laserData.IsEmpty()) {
		m_bHaveNewScan = false;
		DGCWaitForConditionTrue(m_bHaveNewScan, m_newScanCondition, m_newScanCondMutex);
	}

	DGClockMutex(&m_laserDataMutex);
	char *pFrame = m_laserData.GetFrame();
  memcpy((char *)&scan_frame, pFrame, sizeof(laser_data_t));
  DGCunlockMutex(&m_laserDataMutex);
  *latestScanTimestamp = scan_frame.ladar_timestamp;
  
  int numGoodPoints = 0;
  for(int i=0; i<scan_num_segments; i++) {
    if(scan_frame.ranges[i] > MIN_RANGE_SICK_LMS_221 &&
       scan_frame.ranges[i] < MAX_RANGE_SICK_LMS_221) {
      ranges[numGoodPoints] = scan_frame.ranges[i] * _ladarUnits;
      angles[numGoodPoints] = _scanAngleOffset + (((double)i)*_resolution*M_PI)/180.0;
      timestamps[numGoodPoints] = *latestScanTimestamp + (unsigned long long)(angles[numGoodPoints]*TIME_PER_ANGLE);
      numGoodPoints++;
    }
  }
  
  sprintf(errorMessage, "%s", _errorMessage.c_str());

  return numGoodPoints;
}

////////////////////////////////////////////////////////////////////////////////
// Compute the start and end scan segments based on the current resolution and
// scan angles.  Returns true
bool SickLMS200::CheckScanConfig()
{
  if (scan_res == 25)
  {
    // For high res, drop the scan range down to 100 degrees.
    // The angles must be interpreted differently too.
    scan_width = 100;
    scan_min_segment = (min_angle + 5000) / scan_res;
    scan_max_segment = (max_angle + 5000) / scan_res;

		scan_min_segment = min(max(scan_min_segment, 0), 400);
		scan_max_segment = min(max(scan_max_segment, 0), 400);

    return true;
  }

  if (scan_res == 50 || scan_res == 100)
  {
    scan_width = 180;
    scan_min_segment = (min_angle + 9000) / scan_res;
    scan_max_segment = (max_angle + 9000) / scan_res;
    
		scan_min_segment = min(max(scan_min_segment, 0), 360);
		scan_max_segment = min(max(scan_max_segment, 0), 360);

    return true;
  }

  return range_res == 1 || range_res == 10 || range_res == 100;
}


void SickLMS200::getTTYport(char* pTTY)
{
  int i,j;

  for(i=1; i<8; i++)
  {
    for(j=1;j<4;j++)
    {
      char serialnum[10];

      ostringstream driverdir;
      driverdir << "/sys/bus/usb/devices/" << i << '-' << j << "/";
      ostringstream serialfile;
      serialfile << driverdir.str() << "serial";

      ifstream inserial(serialfile.str().c_str());
      inserial.read(serialnum,10);

      if(strncmp(device_name, serialnum, 8) != 0)
	continue;

      // found it!
      ostringstream devicedir;
      devicedir << driverdir.str() << '/' << i << '-' << j << ":1.0/";

      for(i=0;i<10;i++)
      {
	ostringstream dev;
	dev << devicedir.str() << "ttyUSB" << i;
	ifstream serport(dev.str().c_str());
	if(!serport)
	  continue;

	// found it!!!!
	sprintf(pTTY, "/dev/ttyUSB%d", i);
	return;
      }
    }
  }
  pTTY[0] = '\0';
}

////////////////////////////////////////////////////////////////////////////////
// Open the terminal
// Returns 0 on success
int SickLMS200::OpenTerm()
{
  char pTTY[20];
  getTTYport(pTTY);
  if(pTTY[0] == '\0')
  {
    cerr << "int SickLMS200::OpenTerm(): serial number not found: " << device_name << endl;
    return 1;
  }

  laser_fd = ::open(pTTY, O_RDWR | O_SYNC, S_IRUSR | S_IWUSR );
  if (laser_fd < 0)
  {
    return 1;
  }

  // set the serial port speed to 9600 to match the laser
  // later we can ramp the speed up to the SICK's 38K
  //

  struct termios term;
  if( tcgetattr( laser_fd, &term ) < 0 ){
    RETURN_ERROR(1, "Unable to get serial port attributes");
  }

  cfmakeraw( &term );
  cfsetispeed( &term, B9600 );
  cfsetospeed( &term, B9600 );
  
  if( tcsetattr( laser_fd, TCSAFLUSH, &term ) < 0 ){
    RETURN_ERROR(1, "Unable to set serial port attributes");
  }

  // Make sure queue is empty
  //
  tcflush(laser_fd, TCIOFLUSH);
    
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
// Close the terminal
// Returns 0 on success
//
int SickLMS200::CloseTerm()
{
  ::close(laser_fd);
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
// Set the terminal speed
// Valid values are 9600 and 38400
// Returns 0 on success
//
int SickLMS200::ChangeTermSpeed(int speed)
{
  struct termios term;

  current_rate = speed;

#ifdef HAVE_HI_SPEED_SERIAL
  struct serial_struct serial;

  // we should check and reset the AYSNC_SPD_CUST flag
  // since if it's set and we request 38400, we're likely
  // to get another baud rate instead (based on custom_divisor)
  // this way even if the previous player doesn't reset the
  // port correctly, we'll end up with the right speed we want
  if (ioctl(laser_fd, TIOCGSERIAL, &serial) < 0) 
  {
    //RETURN_ERROR(1, "error on TIOCGSERIAL in beginning");
  }
  else
  {
    serial.flags &= ~ASYNC_SPD_CUST;
    serial.custom_divisor = 0;
    if (ioctl(laser_fd, TIOCSSERIAL, &serial) < 0) 
    {
      //RETURN_ERROR(1, "error on TIOCSSERIAL in beginning");
    }
  }
#endif  

  //printf("LASER: change TERM speed: %d\n", speed);

  switch(speed)
  {
	case 9600:
		if( tcgetattr( laser_fd, &term ) < 0 ) {
			RETURN_ERROR(1, "unable to get device attributes");
		}        

		cfmakeraw( &term );
		cfsetispeed( &term, B9600 );
		cfsetospeed( &term, B9600 );
        
		if( tcsetattr( laser_fd, TCSAFLUSH, &term ) < 0 ) {
			RETURN_ERROR(1, "unable to set device attributes");
		}
		break;

	case 38400:
		if( tcgetattr( laser_fd, &term ) < 0 ) {
			RETURN_ERROR(1, "unable to get device attributes");
		}
 
		cfmakeraw( &term );
		cfsetispeed( &term, B38400 );
		cfsetospeed( &term, B38400 );
        
		if( tcsetattr( laser_fd, TCSAFLUSH, &term ) < 0 ) {
			RETURN_ERROR(1, "unable to set device attributes");
		}
		break;

	case 500000:

#ifdef HAVE_HI_SPEED_SERIAL    
		if (ioctl(laser_fd, TIOCGSERIAL, &old_serial) < 0) {
			RETURN_ERROR(1, "error on TIOCGSERIAL ioctl");
		}
    
		serial = old_serial;
    
		serial.flags |= ASYNC_SPD_CUST;
		serial.custom_divisor = 48; // for FTDI USB/serial converter divisor is 240/5
    
		if (ioctl(laser_fd, TIOCSSERIAL, &serial) < 0) {
			RETURN_ERROR(1, "error on TIOCSSERIAL ioctl");
		}
    
#else
		fprintf(stderr, "sicklms200: Trying to change to 500kbps, but no support compiled in, defaulting to 38.4kbps.\n");
#endif

		// even if we are doing 500kbps, we have to set the speed to 38400...
		// the driver will know we want 500000 instead.

		if( tcgetattr( laser_fd, &term ) < 0 )
			RETURN_ERROR(1, "unable to get device attributes");    

		cfmakeraw( &term );
		cfsetispeed( &term, B38400 );
		cfsetospeed( &term, B38400 );
    
		if( tcsetattr( laser_fd, TCSAFLUSH, &term ) < 0 )
			RETURN_ERROR(1, "unable to set device attributes");
    
		break;
	default:
		cerr << "unknown speed " << speed << endl;
  }
  return 0;
}


bool SickLMS200::SetBlockingMode()
{
	int flags = ::fcntl(laser_fd, F_GETFL);
	if (flags < 0)
		return false;

	if (::fcntl(laser_fd, F_SETFL, flags & (~O_NONBLOCK)) < 0)
		return false;

	return true;
}

bool SickLMS200::SetNonBlockingMode()
{
	int flags = ::fcntl(laser_fd, F_GETFL);
	if (flags < 0)
		return false;

	if (::fcntl(laser_fd, F_SETFL, flags | O_NONBLOCK) < 0)
		return false;

	return true;
}
  

////////////////////////////////////////////////////////////////////////////////
// Put the laser into configuration mode
//
bool SickLMS200::EnterInstallationMode()
{
  int tries;
  ssize_t len;
  uint8_t packet[20];

  tries = 0;
  while(tries++ < DEFAULT_LASER_RETRIES)
  {
    packet[0] = '\x20'; /* mode change command */
    packet[1] = '\x00'; /* configuration mode */
    packet[2] = 'S'; // the password 
    packet[3] = 'I';
    packet[4] = 'C';
    packet[5] = 'K';
    packet[6] = '_';
    packet[7] = 'L';
    packet[8] = 'M';
    packet[9] = 'S';
    len = 10;
  
    if (WriteToLaser(packet, len) < 0)
      return false;

    // Wait for laser to return ack
    // This could take a while...
    //

    len = ReadFromLaser(packet, sizeof(packet), true, 1000);
		if(len == 0)
			continue;
    return (len > 0) && (packet[0] == ACK);
  }

	return false;
}


////////////////////////////////////////////////////////////////////////////////
// Set the laser data rate
// Valid values are 9600 and 38400
// Returns true on success
//
bool SickLMS200::SetLaserSpeed(int speed)
{
  int tries;
  ssize_t len;
  uint8_t packet[20];

  tries = 0;
  while(tries++ < DEFAULT_LASER_RETRIES)
  {
    packet[0] = '\x20';
    packet[1] = (speed == 9600 ? '\x42' : (speed == 38400 ? '\x40' : '\x48'));
    len = 2;

    if (WriteToLaser(packet, len) < 0) {
      return false;
    }

    // Wait for laser to return ack
    len = ReadFromLaser(packet, sizeof(packet), true, 2000);
    if(len == 0)
      continue;

    return (len > 0) && (packet[0] == ACK);
  }
  return false;
}

////////////////////////////////////////////////////////////////////////////////
// Reset the ladar.
// Returns true on success
//
bool SickLMS200::Reset()
{
  ssize_t len;
  uint8_t packet[30];

	packet[0] ='\x10';
	len = 1;

	if (WriteToLaser(packet, len) < 0)
		return false;
            
	// Wait for laser to return ack

	//putmein
	sleep(60);
	Setup(_configFilename);


// 	len = ReadFromLaser(packet, sizeof(packet), false, 10000, false);	

// 	return (len >= 1) && (packet[0] == 0x90);
	return true;
}

// gets the errors
bool SickLMS200::DumpErrors()
{
  ssize_t len;
  uint8_t packet[300];

	packet[0] ='\x32';
	len = 1;

	if (WriteToLaser(packet, len) < 0)
		return false;
            
	// Wait for laser to return ack

	len = ReadFromLaser(packet, sizeof(packet), false, 10000, false);	
	if(len > 0)
	{
	  ofstream errors("ladarerrors");
	  errors.write((char*)packet,len);
	}
	return (len >= 1) && (packet[0] == 0xb2);
}


////////////////////////////////////////////////////////////////////////////////
// Get the laser type
//
bool SickLMS200::GetLaserType(char *buffer, size_t bufflen)
{
  int tries;
  ssize_t len;
  uint8_t packet[512];

  tries = 0;
  while(tries++ < DEFAULT_LASER_RETRIES)
  {
    packet[0] = 0x3A;
    len = 1;

    if (WriteToLaser(packet, len) < 0)
      return false;

    // Wait for laser to return data
    len = ReadFromLaser(packet, sizeof(packet), false, 1000);
		if(len == 0)
			continue;

		if(len < 0 || packet[0] != 0xBA)
			return false;

    // NULL terminate the return string
    assert((size_t) len + 1 < sizeof(packet));
    packet[len + 1] = 0;

    // Copy to buffer
    assert(bufflen >= (size_t) len - 1);
    strcpy(buffer, (char*) (packet + 1));

    return true;
  }

  return false;
}

////////////////////////////////////////////////////////////////////////////////
// Set the laser configuration
// Returns true on success
//
bool SickLMS200::SetLaserConfig(bool intensity)
{
  int tries;
  ssize_t len;
  uint8_t packet[512];
	uint8_t oldPacket[512];

  tries = 0;
  while(tries++ < DEFAULT_LASER_RETRIES)
  {
    oldPacket[0] = '\x74';
    len = 1;

    if (WriteToLaser(oldPacket, len) < 0)
      return false;

    // Wait for laser to return data

    len = ReadFromLaser(oldPacket, sizeof(oldPacket), false, 1000);
		if(len == 0)
			continue;

    if ((len < 0) || (oldPacket[0] != 0xF4))
      return false;

    break;
  }
  if (tries >= DEFAULT_LASER_RETRIES)
    return false;

  tries = 0;
  while(tries++ < DEFAULT_LASER_RETRIES)
  {
		memcpy(packet, oldPacket, sizeof(oldPacket));

    // Modify the configuration and send it back
    packet[0] = '\x77';

		//Packet 4 controls sensitivity levels - see the manual
		//packet[4] = '\x00';

		//Don't go to availability level 3
    //packet[5] = '\x00';

		//Uncomment to go to availability level 3
		packet[5] = packet[5] | 1;

    // Return intensity in top 3 data bits
    packet[6] = (intensity ? '\x01' : '\x00'); 

		//cout << "packet[6] = (dec)" << ((int)packet[6]) << endl;

    // Set the units for the range reading
    if (range_res == 1)
      packet[7] = '\x01';
    else if (range_res == 10)
      packet[7] = '\x00';
    else if (range_res == 100)
      packet[7] = '\x02';
    else
      packet[7] = '\x01';

		//cout << "packet[7] = (dec)" << ((int)packet[7]) << endl;

    switch(_ladarAuthority) {
    case SLAVE:
      packet[11] = '\x06';
      break;
    case MASTER:
      packet[11] = '\x42';
      break;
    case NEITHER:
    default:
      packet[11] = '\x02';
      break;
    }

		if(memcmp(packet+1, oldPacket+1, sizeof(packet)-1) != 0) {
			cerr << "Old and new configuration packets differ at (byte N: old/new): " << endl;
			for(int i=1; i<(int)sizeof(packet); i++) {
				if(packet[i]!=oldPacket[i])
					fprintf(stderr, "byte %d: %X/%X, ", i, oldPacket[i], packet[i]);
			}
			fprintf(stderr, "\n");
			srand(time(NULL));
			int randNum = rand();
			int userNum = 0;
			cerr << "Are you sure you want to write the EPROM?  If so, please enter this random number: " << randNum << endl;
			cin >> userNum;
			if(userNum==randNum) {
				cerr << "OK - writing the EPROM!" << endl;
				if (WriteToLaser(packet, len) < 0)
					return false;
				
				len = ReadFromLaser(packet, sizeof(packet), false, 1000);
				if(len == 0)
					continue;
				
				return (len > 0) && (packet[0] == 0xF7);
			} else {
				cerr << "Good - don't write the EPROM." << endl;
				return false;
			}
		}
	}
  return true;
}


////////////////////////////////////////////////////////////////////////////////
// Change the resolution of the laser
// Valid widths are: 100, 180 (degrees)
// Valid resolitions are: 25, 50, 100 (1/100 degree)
//
bool SickLMS200::SetLaserRes(int width, int res)
{
  int tries;
  ssize_t len;
  uint8_t packet[512];

  tries = 0;
  while(tries++ < DEFAULT_LASER_RETRIES)
  {
    len = 0;
    packet[len++] = '\x3B';
    packet[len++] = (width & 0xFF);
    packet[len++] = (width >> 8);
    packet[len++] = (res & 0xFF);
    packet[len++] = (res >> 8);

    if (WriteToLaser(packet, len) < 0)
      return false;

    // Wait for laser to return data
    len = ReadFromLaser(packet, sizeof(packet), false, 1000);
		if(len == 0)
			return false;

    if (len < 0 || packet[0] != 0xBB)
      return false;

    // See if the request was accepted
    return (packet[1] != 0);
  }

  return false;
}


////////////////////////////////////////////////////////////////////////////////
// Request data from the laser
// Returns true on success
//
bool SickLMS200::RequestLaserData()
{
  int tries;
  ssize_t len;
  uint8_t packet[20];

  tries = 0;
  while(tries++ < DEFAULT_LASER_RETRIES)
  {
    len = 0;
    packet[len++] = '\x20'; /* mode change command */
    
    if (scan_min_segment == 0 && scan_max_segment == 360)
    {
      // Use this for raw scan data...
      //
      packet[len++] = '\x24';
    }
    else
    {        
      // Or use this for selected scan data...
      //
      int first = scan_min_segment + 1;
      int last  = scan_max_segment + 1;
      packet[len++] = '\x27';
      packet[len++] = (first & 0xFF);
      packet[len++] = (first >> 8);
      packet[len++] = (last & 0xFF);
      packet[len++] = (last >> 8);
    }

    //printf("LASER: RLD: writing scan data\n");
    if (WriteToLaser(packet, len) < 0)
      return false;

    // Wait for laser to return ack
    // This should be fairly prompt
    len = ReadFromLaser(packet, sizeof(packet), true, 1000);
    if(len == 0)
      continue;

    return (len > 0) && (packet[0] == ACK);
  }

  return false;
}


////////////////////////////////////////////////////////////////////////////////
// Read range data from laser
//
bool SickLMS200::ReadScanFromLaser(uint16_t *data, size_t datalen)
{
  uint8_t raw_data[1024];

  // Read a packet from the laser
  //
  int len = ReadFromLaser(raw_data, sizeof(raw_data));
  if (len == 0)
  {
    return false;
  }

  // Process raw packets
  //
  if (raw_data[0] == 0xB0)
  {
		assert((raw_data[2] & 0xF8) == 0   ); // make sure data is in cm (not mm) and that we're not getting partial scans

    // Determine the number of values returned
    //
    //int units = raw_data[2] >> 6;
    int count = (int) raw_data[1] | ((int) (raw_data[2] & 0x1F) << 8);
    assert((size_t) count <= datalen);

    // Strip the status info and shift everything down a few bytes
    // to remove packet header.
    //
    for (int i = 0; i < count; i++)
    {
      int src = 2 * i + 3;
      data[i] = raw_data[src + 0] | (raw_data[src + 1] << 8);
    }
  }
  else if (raw_data[0] == 0xB7)
  {
    // Determine which values were returned
    //
    //int first = ((int) raw_data[1] | ((int) raw_data[2] << 8)) - 1;
    //int last =  ((int) raw_data[3] | ((int) raw_data[4] << 8)) - 1;
        
    // Determine the number of values returned
    //
    //int units = raw_data[6] >> 6;
    int count = (int) raw_data[5] | ((int) (raw_data[6] & '\x3F') << 8);
    assert((size_t) count <= datalen);

    // Strip the status info and shift everything down a few bytes
    // to remove packet header.
    //
    for (int i = 0; i < count; i++)
    {
      int src = 2 * i + 7;
      data[i] = raw_data[src + 0] | (raw_data[src + 1] << 8);
    }
  }
  else
    RETURN_ERROR(false, "unexpected packet type");
  
  return true;
}


// ////////////////////////////////////////////////////////////////////////////////
// // Write a packet to the laser
// //
ssize_t SickLMS200::WriteToLaser(uint8_t *data, ssize_t len)
{
  uint8_t buffer[4 + 1024 + 2];
  assert(4 + len + 2 < (ssize_t) sizeof(buffer));

  // Create header
  //
  buffer[0] = STX;
  buffer[1] = '\0';
  buffer[2] = LOBYTE(len);
  buffer[3] = HIBYTE(len);

  // Copy body
  //
  memcpy(buffer + 4, data, len);

  // Create footer (CRC)
  //
  uint16_t crc = CreateCRC(buffer, 4 + len);
  buffer[4 + len + 0] = LOBYTE(crc);
  buffer[4 + len + 1] = HIBYTE(crc);

  // Make sure both input and output queues are empty
  //
  tcflush(laser_fd, TCIOFLUSH);

  ssize_t bytes = 0;
  struct timeval start, end;

#ifdef HAVE_HI_SPEED_SERIAL
  // have to write one char at a time, because if we're
  // high speed, then must take no longer than 55 us between
  // chars

  int ret;
  if (current_rate > 38400) {
    //printf("LASER: writing %d bytes\n", 6+len);
    for (int i =0; i < 6 + len; i++)
		{
      do
			{
        gettimeofday(&start, NULL);
        ret = ::write(laser_fd, buffer + i, 1);
      } while (ret == 0);
      if (ret > 0)
        bytes += ret;

      // need to do this sort of busy wait to ensure the right timing
      // although I've noticed you will get some anamolies that are
      // in the ms range; this could be a problem...
      int usecs; 
      do
			{
        gettimeofday(&end, NULL);
        usecs= (end.tv_sec - start.tv_sec)*1000000 +
          (end.tv_usec - start.tv_usec);
      } while (usecs < 60);

      //printf("usecs: %d bytes=%02X\n", (end.tv_sec - start.tv_sec)*1000000 +
      //     (end.tv_usec - start.tv_usec), *(buffer + i));
      
    }
  } else {
    bytes = ::write( laser_fd, buffer, 4 + len + 2);
  }
#else
    
  // Write the data to the port
  //
  bytes = ::write( laser_fd, buffer, 4 + len + 2);
#endif
  // Make sure the queue is drained
  // Synchronous IO doesnt always work
  //
  ::tcdrain(laser_fd);

  // Return the actual number of bytes sent, including header and footer
  //
  return bytes;
}


////////////////////////////////////////////////////////////////////////////////
// Read a packet from the laser
// Set ack to true to ignore all packets except ack and nack
// Set timeout to -1 to make this blocking, otherwise it will return in timeout ms.
// Returns the packet length (0 if timeout occurs)
//
ssize_t SickLMS200::ReadFromLaser(uint8_t *data, ssize_t maxlen, bool ack, int timeout, bool checkStatus)
{
	unsigned long long cur_time, stop_time;
	DGCgettime(cur_time);
  stop_time = cur_time + timeout*1000;

  int bytes = 0;
  uint8_t header[5] = {0};
  uint8_t footer[3];
  
  // Read until we get a valid header
  // or we timeout
  //
  
  while (true)
	{
    if (timeout >= 0)
      usleep(1000);
    
    bytes = ::read(laser_fd, header + sizeof(header) - 1, 1);
    
    if (header[0] == STX && header[1] == 0x80 &&
				(!ack || header[4] == ACK || header[4] == NACK) )
		{
			break;
		}
    memmove(header, header + 1, sizeof(header) - 1);
		DGCgettime(cur_time);
    if (timeout >= 0 && cur_time >= stop_time)
		{
			return 0;
		}
  }
  
  // Determine data length
  // Includes status, but not CRC, so subtract status to get data packet length.
  //
  ssize_t len = ((int) header[2] | ((int) header[3] << 8)) - 1;
    
  // Check for buffer overflows
  //
  if (len > maxlen) {
    RETURN_ERROR(0, "buffer overflow (len > max_len)");
  }

  // Read in the data
  // Note that we smooge the packet type from the header
  // onto the front of the data buffer.
  //
  bytes = 0;
  data[bytes++] = header[4];
  while (bytes < len)
  {
    if (timeout >= 0)
      usleep(1000);
    bytes += ::read(laser_fd, data + bytes, len - bytes);
		DGCgettime(cur_time);
    if (timeout >= 0 && cur_time >= stop_time)
    {
      RETURN_ERROR(0, "timeout on read (3)");
    }
  }

  // Read in footer
  //
  bytes = 0;
  while (bytes < 3)
  {
    if (timeout >= 0)
      usleep(1000);
    bytes += ::read(laser_fd, footer + bytes, 3 - bytes);
		DGCgettime(cur_time);
    if (timeout >= 0 && cur_time >= stop_time)
    {
      RETURN_ERROR(0, "timeout on read (4)");
    }
  }
    
  // Construct entire packet
  // And check the CRC
  //
  uint8_t buffer[4 + 1024 + 1];
  assert(4 + len + 1 < (ssize_t) sizeof(buffer));
  memcpy(buffer, header, 4);
  memcpy(buffer + 4, data, len);
  memcpy(buffer + 4 + len, footer, 1);
  uint16_t crc = CreateCRC(buffer, 4 + len + 1);
  if (crc != MAKEUINT16(footer[1], footer[2])) {
    RETURN_ERROR(0, "CRC error, ignoring packet");
  }
  
  if (timeout < 0)
	{
		unsigned long long oldstamp;
		if(m_bTimestampBufferFull)
			oldstamp = m_timestamps[m_timestampBufferIndex];
		else
			oldstamp = 0ULL;

		DGCgettime(m_timestamps[m_timestampBufferIndex]);
		if(m_timestampBufferIndex == SIZE_TIMESTAMP_BUFFER-1)
			m_bTimestampBufferFull = true;
      
		m_timestampsSum += m_timestamps[m_timestampBufferIndex] - oldstamp;
		m_timestampBufferIndex = (m_timestampBufferIndex+1) % SIZE_TIMESTAMP_BUFFER;
	}

	if(checkStatus)
		CheckStatus(footer[0]);

  return len;
}

int SickLMS200::CheckStatus(uint8_t status)
{
  if(status & 128) {
    //cerr << "Sick: pollution detected!  Clean the window." << endl;
		_errorMessage = "Error: Pollution detected: Clean the window!";
  }

	status &= 7;
	if(status != 0) {
	  //cerr << "error" << endl;
		if(status == 1)
			_errorMessage =  "Sick information: ";
		else if(status == 2)
			_errorMessage = "Sick warning: ";
		else if(status == 3)
			_errorMessage = "Sick error: ";
		else if(status == 4)
			_errorMessage = "Sick fatal error: ";
		else
			_errorMessage = "Sick status: unknown code: ";

		CheckErrors();
	}

  return 0;
}


int SickLMS200::DumpConfig() {
  ssize_t len;
  uint8_t packet[512];
  
  packet[0] = '\x74';
  len = 1;
  
	if(WriteToLaser(packet, len) < 0)
		return false;
	
	len = ReadFromLaser(packet, sizeof(packet), false, 2000, false);

	if(packet[0]!=0xF4)
		return false;

	ofstream dumpFile("dumpFile");

	for(int i=0; i<len; i++) {
		dumpFile << "Byte " << i << ": " << ((int)packet[i]) << endl;
	}
	dumpFile.close();

	return true;
}

int SickLMS200::CheckErrors() {
  ssize_t len;
  uint8_t packet[512];
  
  packet[0] = '\x32';
  len = 1;
  
  if (WriteToLaser(packet, len) < 0)
    return false;
  
  len = ReadFromLaser(packet, sizeof(packet), false, 2000, false);

  if(packet[0] != 0xB2)
    return false;

  //errorStream << "Checking error log...";
  
  cerr << "Got " << (len-1)/2 << " errors" << endl;


  for(int i=0; i<(len-1)/2; i++) {
  ostringstream errorStream;
  //errorStream.str() = "";
    errorStream << "Error " << i << " of " << ((len-1)/2-1) << ": ";
    if(packet[1+i*2] & 128) 
      errorStream << "No longer relevant ";
    switch(packet[1 + i*2] & 7) {
    case 1:
      errorStream << "info ";
      break;
    case 2:
      errorStream << "warning ";
      break;
    case 3:
      errorStream << "error ";
      break;
    case 4:
      errorStream << "fatal error ";
      break;
    case 0:
    default:
      errorStream << "non-error ";
      break;
    }
    errorStream << ((int)packet[2+i*2]);
        
    //If we have a fatal error
    if(!(packet[1+i*2]&128) && 
       (packet[1+i*2]&7) == 4) {
      errorStream << " - Resetting!";
      _errorMessage = errorStream.str();
			cout << errorStream.str() << endl;
		//exit(0);
      return Reset();
    }
    _errorMessage = errorStream.str();
  }



  cerr << "Error message was: " << _errorMessage << endl;

  return (len > 0) && (packet[0] == 0xB2);
}

////////////////////////////////////////////////////////////////////////////////
// Create a CRC for the given packet
//
unsigned short SickLMS200::CreateCRC(uint8_t* data, ssize_t len)
{
  uint16_t uCrc16;
  uint8_t abData[2];
  
  uCrc16 = 0;
  abData[0] = 0;
  
  while(len-- )
  {
    abData[1] = abData[0];
    abData[0] = *data++;
    
    if( uCrc16 & 0x8000 )
    {
      uCrc16 = (uCrc16 & 0x7fff) << 1;
      uCrc16 ^= CRC16_GEN_POL;
    }
    else
    {    
      uCrc16 <<= 1;
    }
    uCrc16 ^= MAKEUINT16(abData[0],abData[1]);
  }
  return (uCrc16); 
}

int SickLMS200::startLogging(char* filename) {
  _logFileRaw = fopen(filename, "w");
  if(_logFileRaw==NULL) {
    fprintf(stderr, "%s [%d]: Error opening file '%s', quitting...\n", __FILE__, __LINE__, filename);
    exit(1);
  }
  fprintf(_logFileRaw, "%% Time [usec] | Scan Points\n");

  return 0;
}


int SickLMS200::stopLogging() {
  fclose(_logFileRaw);
  _logFileRaw = NULL;

  return 0;
}

bool SickLMS200::isLogging() {
  if(_logFileRaw!=NULL) {
    return true;
  } else {
    return false;
  }
}

int SickLMS200::SetupFromFile(char* configFilename, char* logFilename) {
  char tempBuffer[2048];
  ParseConfigFile(configFilename);

  strcpy(_logFilename, logFilename);
  _logFileRaw = fopen(logFilename, "r");
  fgets(tempBuffer, 2048, _logFileRaw);

  lastTime = 0;
  return 0;
}

/////////////////////////////////////////////////////////////////////////////
//
//
unsigned long long SickLMS200::Scan(unsigned long long timestampThresh) {
  int tempInt;
  unsigned long long timestampActual;
  char tempBuffer[2048];
  
  laser_data_t localdata;
  
  if(timestampThresh < lastTime) {
    if(_logFileRaw != NULL)
      fclose(_logFileRaw);
    _logFileRaw = fopen(_logFilename, "r");
    fgets(tempBuffer, 2048, _logFileRaw);
  }

  fscanf(_logFileRaw, "%llu ", &timestampActual);
  while(timestampThresh >= timestampActual && !feof(_logFileRaw)) {
    //Read until the new line
    fgets(tempBuffer, 2048, _logFileRaw);
    //Then get the timestamp
    fscanf(_logFileRaw, "%llu ", &timestampActual);
  }
  lastTime = timestampActual;
  if(feof(_logFileRaw)) {
    fprintf(stderr, "%s [%d]: Reached end of log file\n", __FILE__, __LINE__);
		memset(localdata.ranges, 0, MAX_LADAR_SCAN*sizeof(localdata.ranges[0]));
    return 0;
  }

  //Now read the actual data:
  fscanf(_logFileRaw, "%d ", &scan_num_segments);
  scan_num_segments = min(scan_num_segments, MAX_LADAR_SCAN);
  for(int i = 0; i < scan_num_segments; i++) {
    fscanf(_logFileRaw, "%d ", &tempInt);
    localdata.ranges[i] = tempInt;
  }
  fscanf(_logFileRaw, "\n");

  DGClockMutex(&m_laserDataMutex);
  m_laserData.FillMsg((char*)&localdata, sizeof(laser_data_t));
  DGCunlockMutex(&m_laserDataMutex);
 
  return timestampActual;
}
