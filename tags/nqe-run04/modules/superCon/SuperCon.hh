/* SuperCon.hh
**
** Description:Contains GLOBAL defines of state structure and diagnostic 
** tables (variables stored in the tables)
**
*/

#ifndef SUPERCON_HH
#define SUPERCON_HH

#include "SuperConHelpers.hh"
#include "bool_counter.hh"
#include <string>

#define DBL_ZERO 0.0
#define INT_ZERO 0

/*
**
** SCstate declaration - This is the SuperCon state table
**
** The 4 parameters are type, var_name, default_value, description
**
*/

#define STATE_LIST(_)							\
  /*									\
  **									\
  ** astate								\
  **									\
  */									\
  _(double, northing, 0.0, "Northing")					\
    _(double, easting, 0.0, "Easting")					\
    _(double, nVel, 0.0, "Northing Velocity")				\
    _(double, eVel, 0.0, "Easting Velocity")				\
    _(double, vehicleSpeed, 0.0, "astate vehicle speed")		\
    _(double, yaw, 0.0, "Yaw")						\
    _(double, pitch, 0.0, "Pitch")					\
    _(double, northingConfidence, 0.0, "Jeremy, what is this???")	\
    _(double, eastingConfidence, 0.0, "Jeremy, what is this???")	\
    _(double, heightConfidence, 0.0, "Jeremy, what is this???")		\
    _(double, rollConfidence, 0.0, "Jeremy, what is this???")		\
    _(double, pitchConfidence, 0.0, "Jeremy, what is this???")		\
    _(double, yawConfidence, 0.0, "Jeremy, what is this???")		\
									\
  /*									\
  **									\
  ** adrive								\
  **									\
  */									\
    _(double, wheelSpeed, 0.0, "wheel speed from OBD2 in m/s")		\
    _(double, timeSinceEngineStart, 0.0, "time is s since engine start") \
    _(int, transmissionPos, 0, "current gear, enums in interface_superCon_adrive.hh") \
  /* STARTS IN PAUSE SO THAT SUPERCON CAN BE STARTED BEFORE ADRIVE */ \
    _(int, estopDARPAPos, 1, "DARPA e-stop track position 0 = disable, 1= pause, 2 = run") \
    _(int, estopSuperConPos, 2, "SuperCon e-stop track position 0 = disable, 1= pause, 2 = run") \
    _(int, estopAdrivePos, 2, "adrive e-stop track position 0 = disable, 1= pause, 2 = run") \
    _(double, gasCmd, 0.0, "current *final* (actually passed to actuator) THROTTLE ONLY (normalised 0-1) command in adrive") \
    _(double, brakeCmd, 0.0, "current *final* (actually passed to actuator) BRAKE ONLY (normalised 0-1) command in adrive" ) \
    _(int, obd2operational, 0, "OBD2 on/off status, 0 = OFF, 1 = ON" ) \
                                                                       \
  /*									\
  **									\
  ** TrajFollower							\
  **									\
  */									\
    _(double, currentSpeedRef, 0.0, "Speed trajfollower is attempting to follow" ) \
    _(double, currentAccelCmd, 0.0, "Normalised accel (THROTTLE & BRAKE: -1-->+1) command from trajFollower to adrive") \
    _(double, currentSteerCmd, 0.0, "Normalised steering command from trajFollower to adrive") \
    _(int, currentTFmode, 0, "TrajFollower mode - reverse or FORWARDS, def in trajF_status_struct.hh") \
    _(double, trajFMAXspeedCap, 0.0, "trajFollower MAX speed-cap")	\
    _(double, trajFMINspeedCap, 0.0, "trajFollower MIN speed-cap")	\
  /*                                                                    \
  ** PlannerModule                                                      \
  */                                                                    \
    _(int, trajReceivedFromPln, 0, "[int bool] valid (met planner's internal constraints) traj output by the planner") \
									\
  /*									\
  **									\
  ** scThrows								\
  ** Use type scThrowInt to automatically reset value			\
  **									\
  */									\
  /* WARNING: minCellSpeedOnCurrentTraj IS passed as an scMessage, HOWEVER, it must NOT be cleared when the state table \
   * is copied, as doing so will trigger actions in superCon!!  */ \
    _(double, minCellSpeedOnCurrentTraj, 0.0, "lowest speed cell passed through by the current trajectory" ) \
    _(scThrowInt, tfFinishedReversing, 0, "Signals TF has successfully reversed for the distance specified by SuperCon and has now stopped the vehicle and awaits further instructions") \
    _(scThrowInt, astNeedToIncludeGPSafterReAcq, 0, "Signals astate has re-acquired GPS after an outage, and is ready to be re-included into the astate estimate - re-inclusion will result in a potentially big jump") \
    _(scThrowInt, astate_move_with_clear, 0, "used in the GPSreAcq strategy, signal from astate that we should proceed (->NOMINAL) AFTER sending a message to fusionMapper to clear the map") \
  /* WARNING: astate_jump_size is passed in an scMessage, BUT is a plain double so that it is \
   * NOT destructively read (as information from the same (single) message is used in \
   * sequential strategy stages) */ \
    _(double, astate_jump_size, 0.0, "[meters] magnitude of the jump in state after a GPS re-acquisition") \
    _(scThrowInt, astate_move_without_clear, 0, "used in the GPSreAcq strategy, signal from astate that we should proceed (->NOMINAL) WITHOUT sending a message to fusionMapper to clear the map") \
  /* WARNING: This message is sent as an scMessage, however plannerModule can/will run more slowly that superCon, hence \
   * if the value was reset after each SCstate copy, it could become FALSE, even if it were TRUE */ \
    _(int, pln_failed_deceleration_constraint, 0, "[int bool] set when plannerModule states that its current plan does NOT meet DECELERATION criteria - hence the plan will NOT be sent") \
    _(scThrowInt, clear_maps_complete, 0, "[int bool] - confirmation from fusionMapper that the maps have been cleared") \
									\
  /*									\
  **									\
  ** Meta states							\
  **									\
  */									\
    _(double, componentOfWeightDownHill, 0.0, "component of the vehicle weight that is directed DOWN the current incline (taken from vehicle pitch)") \
    _(double, astateINVconfidence, 0.0, "astate's confidence in its global position estimate - combined estimate") \
									\
  /*                                                                    \
  **									\
  ** superCon internal							\
  **									\
  */									\
    _(double, copyTimestamp, 0.0, "Timestamp of the state copy to diagnostics") \
  /** END OF TABLE **/


#define scThrowInt int
#define scThrowDbl double
BEGIN_DEFINE_STATES(SCstate, STATE_LIST);
#undef scThrowInt
#define scThrowInt(x) 0

#undef scThrowDbl
#define scThrowDbl(x) 0.0

END_DEFINE_STATES(SCstate, STATE_LIST);

/*
**
** SCdiagnostic declaration - This is the SuperCon Diagnostic table
**
** All items are of time bool_counter
** The 2 parameters are: var_name, desc
**
*/

#define DIAGNOSTIC_LIST(_)					\
  /*								\
  **								\
  ** 'STANDARD' DIAGNOSTIC TABLE CONDITIONAL ELEMENTS		\
  **								\
  */									\
    _(engineStatus, "Engine started/off? -> engine on = TRUE")		\
    _(validAliceStop, "Is Alice stationary && is the traj-speed for the current traj-point < the slowest maintainable speed && throttle/brake cmd < 0 (braking) (if all conditions == true, then rule -> TRUE") \
    _(aliceStationary, "Is Alice currently stationary && is the adrive brake command >= ZERO? (braking) --> if all conditions true then -> TRUE") \
    _(aliceOBD2stationary, "Is the OBD2 wheelspeed below the maintainable wheelspeed threshold && is the brake cmd > 0.0 ?") \
    _(superConPaused, "Is superCon e-stop == pause? -> superCon e-stop = pauseD --> = TRUE") \
    _(pausedNOTsuperCon, "Is Alice paused by DARPA/adrive e-stop? superCon e-stop position is ignored") \
    _(allEstopsRUN, "Are ALL estops (adrive, superCon & darpa) == RUN position? -> TRUE if ALL in RUN" ) \
    _(trajFForwards, "Is the TrajFollower in FORWARDS mode? -> TRUE if in FORWARDS mode") \
    _(trajFReverse, "Is the TrajFollower in reverse mode? -> TRUE if in reverse mode") \
    _(trajFLoneRspeedCap, "Is the current MIN speed-cap being used by the trajFollower = LoneRanger strategy drive through terrain obstacles speed? --> TRUE if min speedcap is = LoneRanger min-speedcap") \
    _(adriveGearReverse, "Is the current gear = reverse? -> TRUE iff gear = reverse") \
    _(adriveGearDrive, "Is the current gear = drive? -> TRUE iff gear = drive") \
    _(adriveGearPark, "Is the current gear = par? -> TRUE iff gear = park") \
    _(vehORwheelSpeedsLessThanMinMaintainable, "Are the current vehicle speed OR (||) OBD2 wheelspeed (if OBD2 is available) < minimum maintainable Alice speeds?") \
    _(plnValidTraj, "Is the current traj output by the PLN valid (i.e. NOT NFP - *for any reason*)") \
    _(plnNFP_Terrain, "Is the current NFP traj, NFP because it passes through a terrain obstacle (and NO SC obstacles)? -> pln NFP due to terrain (only) = TRUE") \
    _(plnNFP_SuperC, "Is the current NFP traj, NFP because it passes through a SuperC. obstacle? -> pln NFP due to SC = TRUE") \
    _(superConRun, "Is the superCon estop == RUN?" ) \
    _(endOfRDDF, "Are we at the end of the RDDF? -> true iff yes") \
    _(astate_jump_requires_TFrevBuffer_cleared, "Is the current astate-jump distance > threshold above which trajFollower's reversing state buffer should be cleared" ) \
    									\
  /*                                                                    \
  ** NON-TRANSITION/TERMINATION, COUNT REFERENCING RULES                \
  **                                                                    \
  */                                                                    \
    _(safeGearChangeConds, "conditions that are required before superCon should change gear") \
   \
   \
  /*									\
  **									\
  ** STRATEGY TRANSITION/TERMINATION CONDITIONAL ELEMENTS		\
  **									\
  **									\
  */									\
    \
    /*                                                                                                      \
    ** STRATEGY TRANSITION/TERMINATION INSTANTANEOUS CONDITIONS (do NOT consider repetition counts)         \
    */                                                                                                      \
    _(nominalOperation_InstCondition, "Are the requriements for NOMINAL OPERATION (these describe the NOMINAL STATE) in place? -> TRUE if nominal conditions are met") \
    _(transUnseenObstacle_InstCondition, "Unseen Obstacle has occured")		\
    _(transSlowAdvance_InstCondition, "current plan passes through TERRAIN obstacle (&& started in NOMINAL") \
    _(transGPSreAcq_InstCondition, "GPS reaquired after a period of outage - will result in a state jump") \
    _(transEstopPausedNOTsuperCon_InstCondition, "e-stop pause from adrive/DARPA has occurred - hold in strategy until released (to RUN)" ) \
    _(transLturnReverse_InstCondition, "planner's current trajectory passes through a superCon obstacle - hence need to reverse") \
    _(transPlnFailedNoTraj_InstCondition, "planner has failed to output a trajectory, as no trajectory that met its internal constraints could be found") \
    _(transEndOfRDDF_InstCondition, "End of RDDF reached" ) \
    \
    \
    /*                                                                                                                             \
    ** STRATEGY TRANSITION/TERMINATION CONDITIONS - use the instanenous conditions && REPETITION COUNTS (these are used to control \
    ** whether or not to *actually* transition to a different strategy)                                                            \
    */                                                                                                                             \
   _(nominalOperation, "Alice is in her Nominal operating state" )     \
   _(transUnseenObstacle, "Unseen Obstacle has occured")		\
   _(transSlowAdvance, "current plan passes through TERRAIN obstacle (&& started in NOMINAL") \
   _(transGPSreAcq, "GPS reaquired after a period of outage - will result in a state jump") \
   _(transEstopPausedNOTsuperCon, "e-stop pause from adrive/DARPA has occurred - hold in strategy until released (to RUN)" ) \
   _(transLturnReverse, "planner's current trajectory passes through a superCon obstacle - hence need to reverse") \
   _(transPlnFailedNoTraj, "planner has failed to output a trajectory, as no trajectory that met its internal constraints could be found") \
   _(transEndOfRDDF, "End of RDDF reached") \
   _(termPlnNotNFP, "planner's current trajectory does not pass through ANY obstacles (of any kind)") \
  /*									\
  **									\
  ** DIAGNOSTIC TABLE ELEMENTS THAT CARRY FORWARD STATE TABLE VARIABLES	\
  **									\
  */									\
   _(tfFinishedReversing, "Signals TF has successfully reversed for the distance specified by SuperCon and has now stopped the vehicle and awaits further instructions") \
   _(astNeedToIncludeGPSafterReAcq, "Signals astate has re-acquired GPS after an outage, and is ready to be re-included into the astate estimate - re-inclusion will result in a potentially big jump") \
   _(astate_move_with_clear, "used in the GPSreAcq strategy, signal from astate that we should proceed (->NOMINAL) AFTER sending a message to fusionMapper to clear the map") \
   _(astate_move_without_clear, "used in the GPSreAcq strategy, signal from astate that we should proceed (->NOMINAL) WITHOUT sending a message to fusionMapper to clear the map") \
   _(pln_failed_deceleration_constraint, "set when plannerModule states that its current plan does NOT meet DECELERATION criteria - hence the plan will NOT be sent") \
   _(trajReceivedFromPln, "valid (met planner's internal constraints) traj output by the planner, NOTE this is RESET to FALSE whenever the planner reports that it was unable to produce a traj that met its constraints") \
   _(clear_maps_complete, "confirmation from fmap that the maps have been cleared")
  /** END OF TABLE **/

DEFINE_DIAGNOSTICS(SCdiagnostic, DIAGNOSTIC_LIST);


/*
**
** SUPERCON STRATEGY TABLE
**
*/
enum SCStrategy
  {
    StrategyNone=0,         //"Null" value, should never be used (except internaly)
    StrategyNominal,
    StrategyUnseenObstacle,
    StrategySlowAdvance,
    StrategyLoneRanger,
    StrategyLturnReverse,
    StrategyGPSreAcq,
    StrategyEstopPausedNOTsuperCon,
    StrategyPlnFailedNoTraj,
    StrategyEndOfRDDF,
    StrategyLast
  };


#endif   //SUPERCON_HH
