//SUPERCON STRATEGY TITLE: End of RDDF - ANYTIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyFourHorsemenReset.hh"
#include "StrategyHelpers.hh"

void CStrategyFourHorsemenReset::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE (this MUST remain here)
  currentStageName = fourhorsemen_stage_names_asString( fourhorsemen_stage_names(stgItr.nextStage()) );
  
  switch( stgItr.nextStage() ) {
  
  case s_fourhorsemen::estop_pause: 
    //end of RDDF has been reached -> estop pause
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = trans_EstopPausedNOTsuperCon( (*m_pdiag), strprintf( "FourHorsemenReset: %s", currentStageName.c_str() ) );      
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "FourHorsemenReset - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;
    
  case s_fourhorsemen::clear_all_maps: 
    //Clear all maps everywhere, and then paint a ramp of 2m/s in front of
    //Alice into the map (to prevent her getting stuck outside of the RDDF)
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */      
      skipStageOps = trans_EstopPausedNOTsuperCon( (*m_pdiag), strprintf( "FourHorsemenReset: %s", currentStageName.c_str() ) );      

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      if( m_pdiag->endOfRDDF ) {
	
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "StrategyFourHorsemenReset: Still at end of RDDF -> will poll" );
	checkForPersistLoop( this, persist_loop_waitforDARPA, stgItr.currentStageCount() );		
      }
      
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	transitionStrategy( StrategyNominal, "StrategyFourHorsemenReset: Reached end of traj rule now NOT TRUE -> NOMINAL" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "FourHorsemenReset - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;    

  case s_fourhorsemen::trans_loneranger:
    //-> LoneRanger after receiving confirmation of map clearing from
    //fusionMapper
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */      
      skipStageOps = trans_EstopPausedNOTsuperCon( (*m_pdiag), strprintf( "FourHorsemenReset: %s", currentStageName.c_str() ) );
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->clear_maps_complete == false ) {
	 
	skipStageOps = true;
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "FourHorsemenReset - confirmation of map-clearing NOT yet received from Fmapper -> will poll" );
	checkForPersistLoop( this, persist_loop_clearmap, stgItr.currentStageCount() );	
       }

      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	transitionStrategy( StrategyLoneRanger, "StrategyFourHorsemenReset: Reached end of traj rule now NOT TRUE -> NOMINAL" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "FourHorsemenReset - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }      
    }
    break;

    /* END OF STRATEGY */

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyFourHorsemenReset::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyFourHorsemenReset::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: FourHorsemenReset - default stage reached!");
    }
  }
}


void CStrategyFourHorsemenReset::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "FourHorsemenReset - Exiting");
}

void CStrategyFourHorsemenReset::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset(); //reset the stage iterator
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "FourHorsemenReset - Entering");
}


