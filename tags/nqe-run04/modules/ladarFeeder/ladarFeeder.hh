#ifndef __LADARFEEDER_HH__
#define __LADARFEEDER_HH__

//System include files
#include <unistd.h>

//DGC Include files
#include "MapdeltaTalker.h"
#include "StateClient.h"
#include "CMapPlus.hh"
#include "frames/frames.hh"
#include "DGCutils"
#include "ladar/ladarSource.hh"
#include "CTimberClient.hh"
#include "GlobalConstants.h"
#include "CElevationFuser.hh"
#include "MapConstants.h"

#ifdef LADAR_GUI
#include "MapDisplay/MapDisplay.hh"
#endif

#define CYCLES_TO_TIME_OVER 75

using namespace std;

//Sparrow Display Functions
int user_quit(long arg);
int user_pause(long arg);
int user_step(long arg);
int user_reset(long arg);
int user_change(long arg);
int user_resend(long arg);
int user_clear(long arg);

int log_all(long arg);
int log_base(long arg);
int log_quickdebug(long arg);
int log_alldebug(long arg);
int log_none(long arg);


struct LadarFeederOptions {
  char sourceFilenamePrefix[256];
  char logFilenamePrefix[256];
  char ladarString[256];
  int optSNKey, optPause;
  int optCom, optState, optLadar;
  int optLog, logTime, logMaps;
  ladarSourceLoggingOptions loggingOpts;
  int optMaxScans, optSource, optVerbose;
  double optDelay, optRate;
  double optThresh;
  int optMapDisp;
  int optTimber;
  double optDeltaRate;
  int optZeroAltitude;
	int optCalibrate;
	int optReset;
	int optSendElev;
	int optSendNum;
	int optSendStdDev;
	int optSendPitch;
};


class LadarFeeder : public CStateClient, public CTimberClient, public CMapdeltaTalker{
public:
  LadarFeeder(int skynetKey, LadarFeederOptions ladarFeederOptsArg);
  ~LadarFeeder();
  
  void ActiveLoop();
  
  void ReceiveDataThread();

  void SendDataThread();

  void SparrowDisplayLoop();

  void UpdateSparrowVariablesLoop();

	void ReceiveDataThread_EStop();

  void SendMapDeltas();

  enum {
    LADAR_BUMPER,
    LADAR_ROOF,
    LADAR_SMALL,
    LADAR_RIEGL,
		LADAR_FRONT,

    LADAR_UNKNOWN
  };

  ladarSource ladarObject;

	bool _careAboutEstop;

private:
  double GetCalibRoll();
  double GetCalibPitch();

  CMapPlus ladarMap;

  //Mutex to lock the map when accessing it
  pthread_mutex_t ladarMapMutex;

	sn_msg _msgTypeFusedElev;
	sn_msg _msgTypeMeanElev;
	sn_msg _msgTypeStdDev;
	sn_msg _msgTypeNum;
	sn_msg _msgTypePitch;

  LadarFeederOptions ladarOpts;

  XYZcoord ladarPositionOffset;
  RPYangle ladarAngleOffset;

  int _QUIT;
  //Reached end of log file
  int _EOL;
  int _PAUSE;
  int _STEP;

	bool _estopRun;

  int _numDeltas;
  
  unsigned long long processingTime;
  
  unsigned long long timingArray[CYCLES_TO_TIME_OVER];
  unsigned int timingArrayIndex;

  int layerID_ladarElev;
  int layerID_ladarElevFused;
  int layerID_ladarElevStdDev;
  int layerID_ladarElevNum;
	int layerID_ladarElevPitch;

#ifdef LADAR_GUI
  //Thread to display the map in a GUI
  MapDisplayThread* mapDisp;
#endif
};

#endif
