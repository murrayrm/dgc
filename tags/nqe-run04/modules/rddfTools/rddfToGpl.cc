#include "rddf.hh"
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <fstream>

// Data format from www.frontiernet.net/~werner/gps/

struct gpl {
  unsigned long status;
  unsigned long dummy;
  double      latitude;
  double      longitude;
  double      altitude;
  double      heading;
  double      speed;
  unsigned long time;                           // in <time.h format
  unsigned long dummy3;
};

int main(int argc, char **argv) {
  string line;
  gpl thisGpl;
  thisGpl.altitude = 0;
  thisGpl.heading = 0;
  thisGpl.dummy = 0;
  thisGpl.dummy3 = 0;
  thisGpl.status = 0;

  if (argc != 3) {
    cerr << "Syntax: rddfToGpl infile outfile " << endl;
    exit(1);
  }

  ifstream fileIn;
  fileIn.open(argv[1], ios::in);

  ofstream fileOut;
  fileOut.open(argv[2], ios::out | ios::binary);

  int i = 0;
  while(!fileIn.eof()) {
    // Read in value.
    if(i == 4) { // last element in RDDF line
      getline(fileIn,line,'\n');
    } else {
      getline(fileIn,line,RDDF_DELIMITER);
    }

    // Drop blank lines
    if (line.empty()) {
      continue;
    }
    if(i == 0) { // Store waypoint here, for lack of a better spot
      //      unsigned long temp = 
      thisGpl.time = (unsigned long)atoi(line.c_str()); 
    } else if(i == 1) {
      thisGpl.latitude = atof(line.c_str());
    } else if(i == 2) {
      thisGpl.longitude = atof(line.c_str());
    } else if(i == 4) {
      thisGpl.speed = atof(line.c_str());
      fileOut.write((char *)&thisGpl, sizeof(gpl)); 
    }

    // Iterate the item being looked at
    i = (i + 1)%5;
  }
  fileIn.close();
  fileOut.close();
  return 0;
}

