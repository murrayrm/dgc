#define TABNAME asim

// fields: type, name, default value, widgetType, xLabel, yLabel, xVal, yVal
// everything is TAB-RELATIVE. Thus input is FROM the client INTO the tab
#define TABINPUTS(_) \
  _(double, Time, 0., Label, 0, 0, 1, 0) \
	_(double, Northing, 0., Label, 0, 1, 1, 1) \
  _(double, Easting, 0., Label, 0, 2, 1, 2) \
  _(double, Altitude, 0., Label, 0, 3, 1, 3) \
  _(double, Vel_N, 0., Label, 0, 4, 1, 4) \
  _(double, Vel_E, 0., Label, 0, 5, 1, 5) \
  _(double, Vel_D, 0., Label, 0, 6, 1, 6) \
  _(double, Speed, 0., Label, 0, 7, 1, 7) \
  _(double, Acc_N, 0., Label, 0, 8, 1, 8) \
  _(double, Acc_E, 0., Label, 0, 9, 1, 9) \
  _(double, Acc_D, 0., Label, 0, 10, 1, 10) \
  _(double, SteerCmd, 0., Label, 2, 0, 3, 0) \
  _(double, SteerCmdRad, 0., Label, 2, 1, 3, 1) \
  _(double, SteerAngleRad, 0., Label, 2, 2, 3, 2) \
  _(double, AccelCommand, 0., Label, 2, 3, 3, 3) \
  _(double, Pitch, 0., Label, 2, 4, 3, 4) \
  _(double, Roll, 0., Label, 2, 5, 3, 5) \
  _(double, YawRad, 0., Label, 2, 6, 3, 6) \
  _(double, PitchRate, 0., Label, 2, 7, 3, 7) \
  _(double, RollRate, 0., Label, 2, 8, 3, 8) \
  _(double, YawRateRad, 0., Label, 2, 9, 3, 9) \
  _(double, PitchAccel, 0., Label, 2, 10, 3, 10) \
  _(double, RollAccel, 0., Label, 2, 11, 3, 11) \
  _(double, YawAccel, 0., Label, 2, 12, 3, 12) \
  _(int, simulationPAUSED, 1, Label, 2, 13, 3, 13) \
  _(int, P_to_pause_U_to_unpause_R_to_reset, 0, Label, 2, 15, 3, 15) \
  _(double, SteerCmdDeg, 0., Label, 4, 1, 5, 1) \
  _(double, SteerAngleDeg, 0., Label, 4, 2, 5, 2) \
  _(double, YawDeg, 0., Label, 4, 6, 5, 6) \
  _(double, YawRateDeg, 0., Label, 4, 7, 5, 7)

#define TABOUTPUTS(_) \
  _(char, EnterCommand, 'Z', Entry, 2, 14, 3, 14)

//#undef TABNAME
