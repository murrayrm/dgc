#include <iostream>
#include <fstream>
using namespace std;

#include <stdlib.h>
#include <signal.h>

#include "sn_msg.hh"
#include "DGCutils"
#include "CTimber.hh"

#define MAX_BUFFER_SIZE 1000000

struct SContext
{
	skynet*          pSkynet;
	sn_msg           msgtype;
	ostream*         pStream;
	pthread_mutex_t* pMutex;
	int              socket;
};

void* listenToType(void *pArg)
{
	int old;
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &old);
	pthread_setcanceltype (PTHREAD_CANCEL_ASYNCHRONOUS, &old);

	SContext *pContext = (SContext*)pArg;

	char* pBuffer = new char[MAX_BUFFER_SIZE];

	pContext->socket = pContext->pSkynet->listen(pContext->msgtype, ALLMODULES);

	while(true)
	{
		int msgsize = pContext->pSkynet->get_msg (pContext->socket, pBuffer, MAX_BUFFER_SIZE, 0);
		unsigned long long stamp;
		DGCgettime(stamp);
		
		DGClockMutex(pContext->pMutex);
		pContext->pStream->write((char*)&stamp, sizeof(stamp));
		pContext->pStream->write((char*)&pContext->msgtype, sizeof(pContext->msgtype));
		pContext->pStream->write((char*)&msgsize, sizeof(msgsize));
		pContext->pStream->write((char*)pBuffer, msgsize);
		DGCunlockMutex(pContext->pMutex);
	}
	delete pBuffer;
}


int main(int argc, char *argv[])
{
	int i;
	bool bStarted = false;

	ofstream logfile;
	ostream* pLogstream = &logfile;
	if(argc == 2)
	{
		if(strcmp(argv[1], "-") == 0)
			pLogstream = &cout;
		else
			logfile.open(argv[1]);
	}
	else if(argc != 1)
	{
		cerr << "usage: ./author [file]" << endl;
		cerr << "logs to file if specified; or to /tmp/logs/tome.date.time" << endl;
		return 0;
	}

	int sn_key;

	char* pSkynetkey = getenv("SKYNET_KEY");
	if( pSkynetkey == NULL )
		sn_key = 0;
	else
		sn_key = atoi(pSkynetkey);

	cerr << "Author: using snkey " << sn_key << endl;

	skynet skynetobject(SNguilogwriter, sn_key);

	pthread_mutex_t filemutex;
	DGCcreateMutex(&filemutex);

//   int logControlSock = skynetobject.listen(SNguiToTimberMsg, ALLMODULES);

	SContext  contexts[last_type];
	pthread_t thread_id[last_type];

	for(i=0; i<last_type; i++)
	{
		if(i != SNguiToTimberMsg)
		{
// 			contexts[i].socket = skynetobject.listen((sn_msg)i, ALLMODULES);
		}
	}


	cerr << "author: starting in STOP. Waiting for SNguiToTimberMsg messages" << endl;
	CTimber::GUI_MSG_TYPES msg;
	while(true)
	{
// 		skynetobject.get_msg(logControlSock, &msg, sizeof(CTimber::GUI_MSG_TYPES));

// 		if(msg == CTimber::START && !bStarted)
		if(true)
		{
			cerr << endl;
			if(argc == 1)
			{
				char buffer[50];
				char tmpbuf[10];

				time_t timestamp = time(NULL);
				struct tm* tmstruct = localtime(&timestamp);

				strcpy(buffer,"/tmp/logs/tome.");
				sprintf(tmpbuf, "%04d", tmstruct->tm_year + 1900);
				strcat(buffer, tmpbuf);
				strcat(buffer, "_");

				sprintf(tmpbuf, "%02d", tmstruct->tm_mon + 1);
				strcat(buffer, tmpbuf);
				strcat(buffer, "_");

				sprintf(tmpbuf, "%02d", tmstruct->tm_mday);
				strcat(buffer, tmpbuf);
				strcat(buffer, ".");

				sprintf(tmpbuf, "%02d", tmstruct->tm_hour);
				strcat(buffer, tmpbuf);
				strcat(buffer, "_");

				sprintf(tmpbuf, "%02d", tmstruct->tm_min);
				strcat(buffer, tmpbuf);
				strcat(buffer, "_");

				sprintf(tmpbuf, "%02d", tmstruct->tm_sec);
				strcat(buffer, tmpbuf);

				cerr << "Starting writing " << buffer << endl;
				logfile.open(buffer);
				if(!logfile)
				{
					cerr << "Couldn't open " << buffer << "..... Doing nothing" << endl;
					continue;
				}
			}
			bStarted = true;

			// listen to every message except logging control messages
			for(i=0; i<last_type; i++)
			{
				if(i != SNguiToTimberMsg)
				{
					contexts[i].pSkynet = &skynetobject;
					contexts[i].msgtype = (sn_msg)i;
					contexts[i].pStream = pLogstream;
					contexts[i].pMutex  = &filemutex;
					pthread_create(&thread_id[i], NULL, &listenToType, (void*)&contexts[i]);
				}
			}
			pthread_join(thread_id[0], NULL);
		}
		else if(msg == CTimber::STOP && bStarted)
		{
			cerr << "Stopped writing" << endl;

			// kill threads
			for(i=0; i<last_type; i++)
			{
				if(i != SNguiToTimberMsg)
				{
					pthread_cancel(thread_id[i]);
				}
			}
			logfile.close();
			bStarted = false;
		}
		else
		{
			cerr << "author: got unknown or degenerate logging command. doing nothing...." << endl;
		}
	}

	DGCdeleteMutex(&filemutex);
}
