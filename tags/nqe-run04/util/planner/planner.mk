PLANNER_PATH = $(DGC)/util/planner

PLANNER_DEPEND_LIBS = \
	$(RDDFLIB) \
	$(GGISLIB) \
	$(CMAPLIB) \
	$(CMAPPLIB) \
	$(TRAJLIB) \
	$(CCPAINTLIB) \
	$(MODULEHELPERSLIB) \
	$(FRAMESLIB) \
	$(NLPSOLVERLIB)

PLANNER_DEPEND_SOURCES = \
	$(PLANNER_PATH)/planner.cc \
	$(PLANNER_PATH)/ReactiveStage.cc \
	$(PLANNER_PATH)/RefinementStage.h \
	$(PLANNER_PATH)/RefinementSetup.cc \
	$(PLANNER_PATH)/RefinementStage.cc \
	$(PLANNER_PATH)/RefinementConstraintFuncs.cc \
	$(PLANNER_PATH)/MapAccess.cc  \
	$(PLANNER_PATH)/planner.h \
	$(PLANNER_PATH)/ReactiveStage.h \
	$(PLANNER_PATH)/defs.h  \
	$(PLANNER_PATH)/specs.h \
	$(PLANNER_PATH)/specs.cc \
	$(PLANNER_PATH)/Makefile

PLANNER_DEPEND = $(PLANNER_DEPEND_LIBS) $(PLANNER_DEPEND_SOURCES)
