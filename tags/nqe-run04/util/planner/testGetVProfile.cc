#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

#include "planner.h"
#include "CMapPlus.hh"

int main(int argc, char *argv[])
{
	CMapPlus costmap;
	int layer;

	RDDF rddf("rddf.dat");

	double res = 0.2;

	costmap.initMap(-1e6, -1e6, 2500, 1000, res, res, 0);
	layer = costmap.addLayer<double>(-1.0, -1.01);
 	costmap.loadLayer<double>(layer, "cmap", true);



	CTraj intraj(3, "intraj");


	// planner constructor arguments: speed limit map, the layer, the rddf,
	// whether we read a map from a file or from fusionmapper (false = from
	// fusionmapper), whether we are just computing the velocity profiles (if
	// true)
	CPlanner getvprofile(&costmap, layer, &rddf, true, true);

	getvprofile.getVProfile(&intraj);

	ofstream outtraj("outtraj");
	getvprofile.getTraj()->print(outtraj);
	exit(1);
}
