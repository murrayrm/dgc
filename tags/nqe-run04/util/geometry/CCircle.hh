#ifndef CIRCLE_HH
#define CIRCLE_HH

#include "frames/coords.hh"
#include <math.h>
#include <iostream>

using namespace std;

class CCircle {
public:
  NEcoord _center;
  double _radius;

  CCircle(NEcoord center,
	  double radius);
  ~CCircle();
};

#endif //CIRCLE_HH
