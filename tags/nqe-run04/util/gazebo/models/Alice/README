Alice - A racevehicle model for Gazebo

Updated: 4/2/05 braman

See documentation for Alice.hh for instructions

Geometry
The basis for the geometry of Alice was taken from the Tahoe model,
which was in turn taken from the Clodbuster model.  The dimensions of
Alice in the model were found on the Sportsmobile website.  More 
accurate values can be found by actually measuring the vehicle.  The
break down of the height of the two parts of Alice (lower chassis and 
top part) are only an estimation and should be measured and 
corrected.  The wheel locations are based on the Tahoe model, and 
could also be updated to actual Alice dimensions.  

The mass of Alice is an approximated, scaled value.  Gazebo worlds 
have a hard time dealing with very large masses, so the suspension 
and motor torques have been tuned to the scaled mass value.  The 
suspension values, the spring and damping coefficients, are estimated
in Gazebo by constraint force mixing (CFM) and error reduction
parameter (ERP) values.  In Alice, these are set to provide a 
realistic ride for Alice over terrain and for her scaled mass.  If 
the mass is adjusted, these values also need to be adjusted.  
However, the ERP should be set from about 0.2 to 0.8 for stable 
results, and the CFM should be set from about 1E-5 to about 0.2 for 
best results.

The suspension and engine of Alice is represented in Gazebo by four
Hinge2Joints.  The connections have to degrees of freedom.  The first
allows the wheels to yaw, or allows the wheels to have a steering 
angle.  The second allow the wheels to spin, thereby driving the 
vehicle.  As of now, only the front wheels can have a steering angle,
though this can be easily changed by allowing the back wheels to have
non-zero joint stops.  The steering axis joint stops on the front 
tires are set to positive and negative thirty degrees.  There are, of
course, no joint stops on the drive axis.

VEHICLE DYNAMICS
The suspension constants of the vehicle were set as stated above.  
The velocity of the vehicle was simply modelled as a percentage of
the maximum velocity that can be achieved by Alice, about 36 m/s 
(80 mph).  The amount of throttle applied as a percentage of open 
throttle is used as the percentage parameter.  The motor dynamics
are modelled by setting the maximum torque that the motor can produce
for a given throttle position.  There is an offset of 20 Nm, then the
additional torque is calculated using the average acceleration value 
for a Chevy Tahoe when accelerating from 0 to 30 mph.  That value is
multiplied by Alice's mass (and is thereby scaled to the simulation),
tire radius, and throttle position percentage, and divided by the 
number of "motors" (4).

The brakes are modelled in a similar way.  When the brakes are 
applied, the desired velocity is set to zero.  Then, the maximum 
force parameter for the motor is set to the amount of torque that 
opposes the tire motion.  There is an offset of 1 Nm, and the brake
torque is calculated the same as the accelerating torque, only by 
using a value of maximum brake deceleration (without locking) from
the Chevy Tahoe.  Both parameters, the brake deceleration (BRAKE) and
the acceleration value (FORCE), should be updated to represent Alice
when the data is available.

The steering motor is driven by a proportional controller meant to 
approximate the turn rate of Alice (75 deg/s) as best as possible.  
The program checks the current turn angle and compares with the 
commanded value, and then commands the motor to turn at a velocity
proportional to the difference, with the motor rate for turning from
center to hard right or left to start at about 75 deg/s.

GAZEBO/PLAYER INTERFACE
Three commands are passed through player to Gazebo that control the
actual motion of the vehicle.  Those are the throttle percentage, the
brake percentage, and the steering angle.  They are passed through 
pre-existing interface data structure variables called 
cmd_vel_pos[0], cmd_vel_pos[1], and cmd_vel_rot[2], respectively.


