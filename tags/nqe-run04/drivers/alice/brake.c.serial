#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "constants.h"
#include "brake.h"
#include "serial.h"
//#include "MTA/Misc/Time/Time.hh"

int have_serial_port = FALSE;
int brake_serial_port = -1;     /* not a valid number */

char brake_read = DGC_ALICE_BRAKE_READ;
char brake_command = DGC_ALICE_BRAKE_COMMAND;
char brake_position = DGC_ALICE_BRAKE_ON;
char brake_position_string[3] = "15";
double brake_got_position;
double temp;


/*!
 * brake_open (int serial_port)
 * 
 * Author:      Will Coulter
 *
 * Description: Opens and configures the serial port for brake use.  Also
 *              applies full brakes.
 *
 * Arguments:   serial_port - the number of the serial port we wish to use.
 *
 * Returns:     FALSE - we could not open and adequately configure serial port
 *              TRUE - we are a go
 *
 * Known Bugs:  None.
 **************************************************************************************/
int brake_open (int serial_port)
{
    /* no validation testing, assume serial port code will die gracefully */
    if (serial_open_advanced(serial_port, B9600, 
            (SR_PARITY_DISABLE | SR_SOFT_FLOW_DISABLE | SR_TWO_STOP_BIT)) != 0)
    {
            return FALSE;
    }

    brake_serial_port = serial_port;
    have_serial_port = TRUE;

    /* Try and make sure the vehicle can brake (can use the serial port) */
    if (brake_pause() != TRUE) {
        return FALSE;
    }

    return TRUE;
}


/*!
 * brake_close (void)
 * 
 * Author:      Will Coulter
 *
 * Description: Sends a command to apply full brakes (not checking that they
 *              are) and then closes the serial port.
 *
 * Arguments:   None.
 *
 * Returns:     FALSE - Error closing the serial port.
 *              TRUE - Serial port has been closed.
 *
 * Known Bugs:  None.
 **************************************************************************************/
int brake_close(void)
{
    if (serial_close(brake_serial_port) == -1) {
        return FALSE;
    }
    return TRUE;
}

/*!
 * brake_setposition (int position)
 * 
 * Author:      Will Coulter
 * Revision:    Jeff Lamb (JCL) 20 Jan 05
 *              Changed to string based communication
 *
 * Description: Sends the microcontroller the desired position of the braking
 *              actuator.  
 *
 * Arguments:   position - Desired actuator position; *must* be a value in
 *              [DGC_ALICE_BRAKE_MINPOS,DGC_ALICE_BRAKE_MAXPOS]
 *
 * Returns:     FALSE - desired position is invalid or serial error
 *              TRUE - position was sent over serial channel
 *
 * Known Bugs:  None.
 **************************************************************************************/
int brake_setposition(double position)
{
    if (position > 1) position = 1; 
    if (position < 0) position = 0;
    position = position * DGC_ALICE_BRAKE_MAXPOS;
    if(position < 10) {
      brake_position_string[0] = '0';
      sprintf(brake_position_string+1, "%d\n\r", (int) position);
    }
    else {
      sprintf(brake_position_string, "%d\n\r", (int) position);
    }

    if (
            serial_write(brake_serial_port, &brake_command, 1, SerialBlock) != 1 ||
            serial_write(brake_serial_port, (char*) brake_position_string, strlen(brake_position_string), SerialBlock) != 1
       )
    {
      return TRUE;  // debugging 21 Jan 2005, should be "return FALSE"
    }

    return TRUE;
}

/*!
 * brake_getposition (void)
 * 
 * Author:      Will Coulter
 * Revision:    Jeff Lamb (JCL) 20 Jan 2005
 *              Modified to receive position as a 2 character string
 *
 * Description: Returns the current position of the braking actuator.
 *
 * Arguments:   None.
 *
 * Returns:     If the integer returned is in the allowable range, it is
 *              returned.  Otherwise, a negative number is returned.
 *
 * Known Bugs:  None.
 **************************************************************************************/
double brake_getposition(void)
{
    /* Try sending command to get position */
    if (serial_write(brake_serial_port, &brake_read, 1, SerialBlock) != 1)
    {
        return -1;
    }

    /* Try actually retrieving position */
    if (serial_read(brake_serial_port, (char *) brake_position_string, 3) != 1) {
        /* The microcontroller or computer may be busy, so wait and try again.
           Yes, this is the poor man's read-with-timeout solution */
        usleep(DGC_ALICE_BRAKE_MAXDELAY);
        if (serial_read(brake_serial_port, (char *) brake_position_string, 3) != 1) {
            /* now we have really failed, so return */
          //printf("brake string: %s", brake_position_string);  
	  //return -1;
        }
    }
    //printf("brake string: %s", brake_position_string);
    //getchar();
    /* Do some validation */
   
    //if (
    //        atoi(brake_position_string) > DGC_ALICE_BRAKE_MAXPOS ||
    //        atoi(brake_position_string) < DGC_ALICE_BRAKE_MINPOS
    //   )
    //{
    //    return -1;
    //}
    temp = atoi(brake_position_string);
    brake_got_position = temp / DGC_ALICE_BRAKE_MAXPOS;
    return (double) brake_got_position;
}

/*!
 * brake_pause (void)
 * 
 * Author:      Will Coulter
 *
 * Description: Commands full brake actuation.  Does not check to see that the
 *              command was executed.  This is just a wrapper around
 *              brake_setposition
 *
 * Arguments:   None.
 *
 * Returns:     FALSE - Serial error of some kind.
 *              TRUE - Command was sent over the serial channel.
 *
 * Known Bugs:  None.
 **************************************************************************************/
int brake_pause(void)
{
  return brake_setposition(1);
}

/*!
 * brake_disable (void)
 * 
 * Author:      Will Coulter
 *
 * Description: Wrapper for brake_pause (void).
 **************************************************************************************/
int brake_disable(void)
{
    return brake_setposition(1);
}

/*!
 * brake_off (void)
 * 
 * Author:      Will Coulter
 *
 * Description: Turns the brake off, i.e. let's the vehicle roll freely.  Does
 *              not check to see that the command was executed.  Yet another
 *              wrapper for brake_setposition
 **************************************************************************************/
int brake_off(void)
{
    return brake_setposition(0);
}




#ifdef THREADING

#endif //THREADING

