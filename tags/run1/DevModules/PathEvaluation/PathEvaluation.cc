
#include "PathEvaluation.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include <unistd.h>
#include <iostream>

extern int SIM;
extern int DISPLAY;
extern int PRINT_VOTES;

//#define LOG_GEN_MAP 1

PathEvaluation::PathEvaluation() 
  : DGC_MODULE(MODULES::PathEvaluation, "PathEvaluation Planner", 0) {

}

PathEvaluation::~PathEvaluation() {
}

void PathEvaluation::Init() {
  // call the parent init first
  DGC_MODULE::Init();

  //Insert code to initialize PathEvaluation here
  d.localMap.initMap(200,200,0.5,0.5,true);
  d.localMap.initPathEvaluationCriteria(false,false,false,false,false,true,true,false);
  d.localMap.initThresholds(70,0.5,0.5,0.5,1,1.2);
  /*cout << "Numrows=" <<  d.localMap.getNumRows()<< endl;
  cout << "NumCols=" <<  d.localMap.getNumCols()<< endl;
  cout << "RowRes=" <<  d.localMap.getRowRes()<< endl;
  cout << "ColRes=" <<  d.localMap.getColRes()<< endl;
  cout << "VehRow=" <<  d.localMap.getVehRow()<< endl;
  cout << "VehCol=" <<  d.localMap.getVehCol()<< endl;
  cout << "NorBot=" <<  d.localMap.getLeftBottomUTM_Northing()<< endl;
  cout << "EasBot=" <<  d.localMap.getLeftBottomUTM_Easting()<< endl;
  cout << "RowBot=" <<  d.localMap.getLeftBottomRow()<< endl;
  cout << "ColBot=" <<  d.localMap.getLeftBottomCol()<< endl;
  cout << "MxPath=" <<  d.localMap.getPathMaxDistance()<< endl;
  sleep(5);*/

  //d.localMap.saveMATFile("MAT","genMap_test1","genMap_test1");
  // Above puts in corridors
  // Put in obstacles in the map?

  cout << "Module Init Finished" << endl;
}

void PathEvaluation::Active() {
  RDDFVector targetPoints;
  VBClock clk;
  int i,count=0;
  vector< pair<double,double> > vel_goodness;
  vector<double> steerAngle;
  bool firstLoop=true;

#ifdef LOG_GEN_MAP
  //VARIABLES NEEDED TO LOG GEN MAP
  time_t curTime;
  struct tm* localTime;
  VBString filename, voteFileName, directory, command, msg;
  double diff_northing,diff_easting;
  int ret, logCount=0;
  char year[5], month[3], day[3], hour[3], min[3], sec[3];
  char genMapFile[256],genMapMsg[256];
  ofstream voteFile;
  // Get the current time
  curTime = time(NULL);
  // Convert it to local time representation
  localTime = localtime (&curTime);
  ret = sprintf(year,"%d",localTime->tm_year + 1900);
  ret = sprintf(month,"%d",localTime->tm_mon + 1);
  ret = sprintf(day,"%d",localTime->tm_mday);
  ret = sprintf(hour,"%d",localTime->tm_hour);
  ret = sprintf(min,"%d",localTime->tm_min);
  ret = sprintf(sec,"%d",localTime->tm_sec);
  directory = "genMapLog_";
  directory += year;
  directory += "-";
  directory += month;
  directory += "-";
  directory += day;
  directory += "_";
  directory += hour;
  directory += "h";
  directory += min;
  directory += "m";
  directory += sec;
  directory += "s";
  voteFileName = directory;
  voteFileName += "/";
  //voteFileName += directory;
  voteFileName += "genMap.votes";
  /*filename += year;filename += "-";filename += month;filename += "-";
  filename += day;filename += "_";filename += hour;filename += "h";
  filename += min;filename += "m";filename += sec;filename += "s";*/
  command = "mkdir ";
  command += directory;
  //cout << "directory   = " << directory << endl;
  //cout << "votes file  = " << voteFileName << endl;
  //cout << "genMap file = " << filename << endl;
  //getchar();
  system(command.c_str());
  voteFile.open(voteFileName.c_str());
  voteFile << "# VOTES TO ARBITER FROM 0 TO 24\n";
  voteFile << "# Nro: 100;100;90; ...\n#\n";
  //END LOG GEN MAP
#endif

  cout << "Starting Active Funcion" << endl;
  steerAngle.reserve(NUMARCS);
  vel_goodness.reserve(NUMARCS);

  int logGenMap = 0;
  targetPoints = d.localMap.getTargetPoints();
  while( ContinueInState() ) {

    clk.markTime();
    // update the state so we know where the vehicle is at
    count++;
    //cout << "about to update state" << endl;

#ifdef LOG_GEN_MAP
    //LOG GEN MAP ***BEFORE UPDATE STATE***
    diff_northing = d.SS.Northing;
    diff_easting = d.SS.Easting;
    //END LOG GEN MAP
#endif

    UpdateState();
    d.localMap.updateFrame(d.SS,true);
    //cout << "finished updating Frame" << endl;

#ifdef LOG_GEN_MAP
    //LOG GEN MAP ***AFTER UPDATE STATE***
    diff_northing = d.SS.Northing - diff_northing;
    diff_easting = d.SS.Easting - diff_easting;
    //END LOG GEN MAP
#endif

    // Update steerAngle ... 
    if(firstLoop){
      for(i = 0; i < NUMARCS; i++) {
        steerAngle.push_back(GetPhi(i));      
      }
      firstLoop = false;
    }
    else{
      for(i = 0; i < NUMARCS; i++) {
        steerAngle[i] = GetPhi(i);
      }      
    }
    //cout << "Finished updating steerAngle" << endl;
    //cout << "Now generating Arbiter votes" << endl;

    // Update votes
    //cout << "lastWaypoint = " << d.localMap.lastWaypoint << " nextWaypoint = " 
    //	 << d.localMap.nextWaypoint << endl;
    //cout << "N = " << d.SS.Northing << " E = " << d.SS.Easting << endl;

    system("clear");
    cout << "current position: " << endl;
    cout << "N = " << d.SS.Northing << " E = " << d.SS.Easting << endl;
    cout << "lastWaypoint = " << d.localMap.getLastWaypoint() << endl;
    cout << "nextWaypoint = " << d.localMap.getNextWaypoint() << endl;
    cout << "N = " << targetPoints[d.localMap.getNextWaypoint()].Northing
         << "E = " << targetPoints[d.localMap.getNextWaypoint()].Easting
	 << endl;
    printf("Relative position = %10.3f m Northing, %10.3f m Easting\n",
	   targetPoints[d.localMap.getNextWaypoint()].Northing - d.SS.Northing ,  
	   targetPoints[d.localMap.getNextWaypoint()].Easting - d.SS.Easting );


    /*cout << "numTargetPoints = " << d.localMap.getNumTargetPoints() << endl;
    for(i = 0; i < d.localMap.getNumTargetPoints(); i++) {
      targetPoints[i].display();
    }    
    getchar();*/

    vel_goodness = d.localMap.generateArbiterVotes(NUMARCS,steerAngle);
    //cout << "-------Finished generating arbiter votes--------------" << endl;

    for(i = 0; i < NUMARCS; i++) {      
      //      cout << "Vel = " << vel_goodness[i].first << " Goodness = " 
      //	   << vel_goodness[i].second << endl;
      d.pathEvaluation.Votes[i].Goodness = vel_goodness[i].second;
      d.pathEvaluation.Votes[i].Velo = vel_goodness[i].first;
      //if(d.pathEvaluation.Votes[i].Velo > 3)
      //  d.pathEvaluation.Votes[i].Velo = 3;
    }

    // and send them to the arbiter
    Mail m = NewOutMessage(MyAddress(), MODULES::Arbiter, 
			   ArbiterMessages::Update);
    // Send the arbiter our "ID" and the vote struct.  The weights are
    // taken care of by the arbiter.
    int myID = ArbiterInput::PathEvaluation;
    m << myID << d.pathEvaluation;
    SendMail(m);

// logging genMap
#ifdef LOG_GEN_MAP
    if (!(logGenMap % 3))
    {
      //usleep(1000);
      //      cout << "---saving genMap using saveMATFile" << endl;
      msg = " last Waypoint = ";
      msg += d.localMap.getLastWaypoint();
      msg += " next Waypoint = ";
      msg += d.localMap.getNextWaypoint();
      filename = directory;
      filename += "/genMap_";
      filename += logCount;
      strcpy(genMapFile,filename.getStr());
      strcpy(genMapMsg,msg.getStr());
      d.localMap.saveMATFile(genMapFile, genMapMsg, "PEmap");

      voteFile << logCount << ": ";
      for(i = 0; i < NUMARCS; i++) {
        voteFile << vel_goodness[i].second << ";";
      }
      voteFile << endl;
      
      logCount++;
    }
    logGenMap++;
  #endif

    //   d.localMap.display();
    cout << "LOOP TIME CONSUMING = " << (100*clk.getDiffTime()) << " ms" << endl;
    cout << endl;

    // and sleep for a bit
    // if you do not do this you may flood the MTA with messages
    usleep(10000);
  }

  cout << "Finished Active State" << endl;
  
}


void PathEvaluation::Shutdown() {

  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;

  //Insert code to shut down PathEvaluation here

}



