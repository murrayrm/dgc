#include <time.h>
#include <stdio.h>
#include <iostream>
#include "gps/gps.h"
int GPS_COM;

//Sample code showing the basic functions for use with the GPS unit

int main(int argc, char** argv) {
   //run-time help
  if ((argc == 1) || (argc > 2)){
    printf("Usage:\tgps_data_logging serial-device-number\nUse 32 for ttyUSB0, 33 for ttyUSB1\n");
  exit(0);}

  //Get some variables ready
  GPS_COM = atoi(argv[1]);
  gpsDataWrapper wrapper;
  char *buffer_start;

  //Make room in the buffer to store incoming GPS messages  
  buffer_start=(char*)malloc(2000);
  
  //Initialize the GPS
  if(init_gps(3,1) == 1) {
    printf("\nGPS Initialized.");

    //Cycle through 10 GPS messages 
    for(int i=0; i<10; i++) {
      //Get the GPS message
      get_gps_msg(buffer_start, 1000);
      //If it's a PVT (position velocity time) message
      if(get_gps_msg_type(buffer_start)==GPS_MSG_PVT) {
	//Update the data based on the message
	wrapper.update_gps_data(buffer_start);
	//Print out some of the data
	printf("\n\nData Received - ");
	if(wrapper.data.nav_mode & 128) {
	  printf("Valid");
	} else {
	  printf("NOT Valid");
	}
	printf("\nLat: %.10lf", wrapper.data.lat);
	printf("\nLng: %.10lf", wrapper.data.lng);
	printf("\nVel U: %.10lf", wrapper.data.vel_u);
	printf("\nVel N: %.10lf", wrapper.data.vel_n);
	printf("\nVel E: %.10lf", wrapper.data.vel_e);
      } else {
	//Otherwise, print out to see what type of message we got - it'll be in hex, though
	print_gps_msg(buffer_start);
      }
    }
  } else {
    printf("\nGPS NOT initialized.");
  }
  
  printf("\n");
  
  //Close the GPS
  return close_gps();
}
