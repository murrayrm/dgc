#ifndef WAYPOINTFOLLOWING_HH
#define WAYPOINTFOLLOWING_HH

//#include "../../state/state.h"
#include "../../Global/Vector.hh"
#include <fstream>
#include <unistd.h>
#include <sys/time.h>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <list>

#include "../../Msg/dgc_time.h"
#include "../../steerCar/steerCar.h"

//#include "../../magnetometer/magnetometer.h"
//#include "../../moveCar/brake.h"
//#include "../../moveCar/TLL.h"
//#include "../../moveCar/accel.h"
//#include "../../gps/gps.h"
//#include "../../latlong/LatLong.h"

using namespace std;

typedef struct WaypointFollowing {
 public:
  int    thrdCounter;
  ofstream stateFile;
  ofstream driveFile;
  timeval timeZero;
  list<Vector> waypoints;

  WaypointFollowing() {
    thrdCounter = 0;
    gettimeofday(&timeZero, NULL);
    //stateFile.open("state.dat");
    driveFile.open("drive.dat");
    //  stateFile << setiosflags(ios::fixed) << setprecision(5);
    driveFile << setiosflags(ios::fixed) << setprecision(5);
  }
};

pair<double, double> globalEst(Vector head, Vector pos, double speed, 
                               struct WaypointFollowing &WF);
double f_Steering(Vector curPos, Vector curWay, Vector curHead);
double f_Velo(double curVelo, Vector curPos, Vector curWay);
void initWaypts(struct WaypointFollowing &WF);

#endif
