Test log key for tests 11/15/2003.  Ask Lars if more info. needed.

1 - 3 m/s cruise control (c.c.), Integral_Control, saturated at [0.0,0.3]
2 - 6 m/s c.c. "
3 - 9 m/s c.c. "

Note: Tests 1-3 don't indicate saturation at throttle = 0.0

4 - 12 m/s c.c. Int. Ctrl., same saturation, Tully killed throttle at ~30-35s
5 - Same as #4

Note: Tests 6-9 performed in 1st gear

6 - 3 m/s cruise control (c.c.), Feedforward controller, saturated at [0.0,0.3]
7 - 6 m/s c.c. "
8 - 9 m/s c.c. "
9 - 12 m/s c.c. " -> aborted

Note: Switched to 3rd gear automatic, repeat tests 1-5

10 - 3 m/s cruise control (c.c.), Integral_Control, saturated at [0.0,0.3]
11 - 6 m/s "
12 - 9 m/s "
13 - 12 m/s "

OPEN LOOP TESTS:

(No assist):

14 - Throttle position = 0.05 E->W
15 - Throttle position = 0.05 W->E

14 - Throttle position = 0.10 E->W
15 - Throttle position = 0.10 W->E

nOTE: 18, 19 performed with Tully assisting throttle to get to s.s. speed

18 - Throttle position = 0.10 E->W
19 - Throttle position = 0.10 W->E

nOTE: 20, 21 performed with Tully assisting throttle to get to s.s. speed

20 - Throttle position = 0.08 E->W
21 - Throttle position = 0.08 W->E

(Unless otherwise noted... *no* assist):

22 - Throttle position = 0.08 E->W
23 - Throttle position = 0.08 W->E

24 - Throttle position = 0.09 E->W
25 - Throttle position = 0.09 W->E

26 - Throttle position = 0.07 E->W
27 - Throttle position = 0.07 W->E

28 - Throttle position = 0.06 E->W
29 - Throttle position = 0.06 W->E

30 - Throttle position = 0.04 E->W
31 - Throttle position = 0.04 W->E

Note: At these higher throttle values, cut off test when vehicle reached ~15 m/s

32 - Throttle position = 0.15 E->W
33 - Throttle position = 0.15 W->E

34 - Throttle position = 0.20 E->W
35 - Throttle position = 0.20 W->E

34 - Throttle position = 0.20 E->W
35 - Throttle position = 0.20 W->E

36 - Throttle position = 0.25 E->W
37 - Throttle position = 0.25 W->E

38 - Throttle position = 0.30 E->W
39 - Throttle position = 0.30 W->E

Note: Skipped #40 for no apparent reason

41 - Throttle position = 0.35 E->W
42 - Throttle position = 0.35 W->E

43 - Throttle position = 0.40 E->W
44 - Throttle position = 0.40 W->E

45 - Throttle position = 0.45 W->E
46 - Throttle position = 0.45 E->W

47 - Throttle position = 0.50 W->E
48 - Throttle position = 0.50 E->W

Coast tests

49 - Coast test from Tully gunning up to 25 mph and then coasting, going ~NW
50 - Coast test from Tully gunning up to 25 mph and then coasting, going ~SE
