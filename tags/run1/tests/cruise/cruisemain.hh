#ifndef cruisemain_HH
#define cruisemain_HH

#include <time.h>

#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "WaypointFollowing.hh"
#include "../../state/state.h"
#include "../../gps/gps.h"
//#include "../../latlong/LatLong.h"
#include "../../magnetometer/magnetometer.h"
#include "../../moveCar/brake.h"
#include "../../moveCar/accel.h"
//#include "../../moveCar/TLL.h"
#include "../../moveCar/cruise.h"
#include "../../Msg/dgc_time.h"
//#include "Msg/dgc_const.h"
#include "../../steerCar/steerCar.h"
#include "../../steerCar/steerState.hh"
#include "../../OBDII/OBDII.h"

struct DATUM {
  
  // for data logging 
  int TestNumber; 
  string TestNumberStr;

  // Time zero
  timeval TimeZero;

  // Module Level stuff
  int shutdown_flag;


  // Magnetometer stuff
  boost::recursive_mutex magLock;
  int magValid; // flag to determine if mag is valid (new data)
  int magNewData;
  float magBuffer[7];
  timeval magTS;

  // GPS Stuff 
  boost::recursive_mutex gpsLock;
  int gpsValid;
  int gpsNewData;
  gpsDataWrapper gpsDW;
  timeval gpsTS;

  // OBD-II Stuff
  boost::recursive_mutex obdLock;
  int obdValid;
  int obdNewData;
  float obdSpeed;
  float obdRPM;

  // State Struct for driving code to use
  boost::recursive_mutex stateLock;
  state_struct SS;
  int stateNewData;
  int stateValid;

  // Accel Data
  float targetVelocity;                   // in m/s
  float accel;
  int accelValid;

  // Waypoints data
  struct WaypointFollowing WF;

  // A lock for actually allowing the software to drive the car
  boost::recursive_mutex drivingLock;
  int drivingValid;
};

int  InitGPS(DATUM &d);
void GrabGPSLoop(DATUM &d);

int  InitMag(DATUM &d); 
void GrabMagLoop(DATUM &d);

int  InitOBD(DATUM &d); 
void GrabOBDLoop(DATUM &d);

int  InitStateFilter(DATUM &d);
void StateFilterLoop(DATUM &d);

int  InitAccel(DATUM &d);
void AccelLoop(DATUM &d);

float cruise(float targetVelo, float curVelo, float curAccel);

const float brakeVelo = 0.7;

#endif 
