#ifndef WAYDISP_HH
#define WAYDISP_HH

#include "Arbiter/Arbiter.h" // for NUMARCS

double waydisp_arbiter_phi;
double waydisp_arbiter_velo;
int    waydisp_arbiter_good;
double waydisp_arbiter_yaw;
int    waydisp_arbiter_count;

double waydisp_ladar_p[NUMARCS];
double waydisp_ladar_v[NUMARCS];
int waydisp_ladar_g[NUMARCS];

double waydisp_global_p[NUMARCS];
double waydisp_global_v[NUMARCS];
int waydisp_global_g[NUMARCS];

double waydisp_stereo_p[NUMARCS];
double waydisp_stereo_v[NUMARCS];
int waydisp_stereo_g[NUMARCS];

double waydisp_dfe_p[NUMARCS];
double waydisp_dfe_v[NUMARCS];
int waydisp_dfe_g[NUMARCS];

double waydisp_arbiter_p[NUMARCS];
double waydisp_arbiter_v[NUMARCS];
int waydisp_arbiter_g[NUMARCS];

#endif
