#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
// DATUM & cruise function protocols??
#include "actuators.hh"

int Init1(DATUM &d) {
  cout << "INITIALIZING LOOP 1"  << endl;
  return TRUE; // success

} 


int Loop1(DATUM &d) {

  while(d.shutdown_flag == FALSE) {
    cout << "LOOP 1"  << endl;
    boost::recursive_mutex::scoped_lock sl(d.loop1Lock);

    sleep(1);
  }

}

int Init2(DATUM &d) {
  cout << "INITIALIZING LOOP 2"  << endl;
  return TRUE; // success

} 


int Loop2(DATUM &d) {
  while(d.shutdown_flag == FALSE) {
    cout << "LOOP 2"  << endl;
    boost::recursive_mutex::scoped_lock sl(d.loop2Lock);
    brakePos = 1;
    sleep(1);
  }

}


