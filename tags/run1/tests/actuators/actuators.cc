/**
 * 12/6/2003   For demo today, we have a fixed set of arcs for every cycle.
 **/

#include "actuators.hh"
using namespace std;

DATUM d;

boost::recursive_mutex StarterMutex;
int StarterCount;
void Starter();


void shutdown(int signal) {

  cout << "Signal Received" << endl;
  d.shutdown_flag = TRUE;
  // boost::recursive_mutex::scoped_lock scoped_lock(WF.brakeMutex);
  //set_brake_abs_pos_pot(0.8, brakeVelo, brakeVelo);
  cout << "Set Abort Flag" << endl;
  //sleep_for(10);
  //exit(EXIT_SUCCESS);
}


int main(int argc, char **argv) {


  cout << "Starting Main" << endl;

  //strstream strStream;
  //strStream << d.TestNumber;
  //strStream >> d.TestNumberStr;

  // setup signals for shutdown
  d.shutdown_flag = FALSE;
  //signal(SIGINT, &shutdown);
  //signal(SIGTERM, &shutdown);

  Init1(d);
  Init2(d);

  boost::thread_group thrds;

  StarterCount = 0;
  thrds.create_thread(Starter);  // thread 1
  thrds.create_thread(Starter);  // thread 2

  thrds.join_all();

  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;

  return 0;

} // end main(int argc, char **argv)


void Starter() {
  // Determine which thread we are 
  int myThread;
  if( TRUE ) {
    boost::recursive_mutex::scoped_lock sl(StarterMutex);
    myThread = StarterCount;
    StarterCount ++;
  }

  switch( myThread ) {
  case 0:
    cout << "Starting Loop 1: (" << myThread  << ")" << endl;
    Loop1(d);
    break;
  case 1:
    cout << "Starting Loop 2: (" << myThread  << ")" << endl;
    Loop2(d);
    break;
  default:
    cout << "Starter error, default reached" << endl;
  }
}
