#ifndef LADARCONSTANTS_H
#define LADARCONSTANTS_H

#define ROOF_LADAR_MAX_RANGE 80    //[m] should be better characterized!!
#define BUMPER_LADAR_MAX_RANGE 50  //[m]

#define LADAR_BAD_SCANPOINT -1 // number to assign to a bad scanpoint

// Preliminary measurements, should be redone
// FIXME: Not sure if sign is correct
//        Nor are the axes right!!!

// 75.75" from ground to bottom of ladar, 99mm to center of scanning beam
//   for a total of 2.03 meters // measured 2004-02-26
// 6.7 deg pitch down

#define ROOF_LADAR_OFFSET_X 1.805               // roof ladar x offset in bob's body frame
#define ROOF_LADAR_OFFSET_Y 0.0                 // roof ladar y offset in bob's body frame
#define ROOF_LADAR_OFFSET_Z -2.09               // roof ladar z offset in bob's body frame
#define ROOF_LADAR_PITCH -(6.28*2*M_PI/360.0)   // roof ladar pitch angle in body frame 
#define ROOF_LADAR_ROLL -(0.20*2*M_PI/360.0)    // roof ladar roll angle in body frame
#define ROOF_LADAR_YAW 0                        // roof ladar yaw angle in body frame

#define BUMPER_LADAR_OFFSET_X 3.86              // bumper ladar x offset in bob's body frame
#define BUMPER_LADAR_OFFSET_Y 0.0               // bumper ladar y offset in bob's body frame
#define BUMPER_LADAR_OFFSET_Z -.737             // bumper ladar z offset in bob's body frame
#define BUMPER_LADAR_PITCH (0*2*M_PI/360.0)     // bumper ladar pitch angle in body frame 
#define BUMPER_LADAR_ROLL (0*2*M_PI/360.0)      // bumper ladar roll angle in body frame
#define BUMPER_LADAR_YAW 0                      // bumper ladar yaw angle in body frame

#define DEFAULT_LADARMAP_NUM_ROWS 500
#define DEFAULT_LADARMAP_NUM_COLS 500
#define DEFAULT_LADARMAP_ROW_RES  0.2
#define DEFAULT_LADARMAP_COL_RES  0.2

#endif
