/* 
** VehicleConstants.h
** 
** This header is for any needed inherent physical constants of the vehicle.
**
*/
#ifndef VEHICLECONSTANTS_H
#define VEHICLECONSTANTS_H

#define  VEHICLE_MASS     2291    // [kg] TODO: NOTE: this is an approximation

#define  VEHICLE_LENGTH   5.057  // [m] length of the vehicle from ? to ? (from below)
#define  VEHICLE_WIDTH    1.941  // [m] width of the vehicle from ? to ? (from below)
#define  VEHICLE_HEIGHT   1.783  // [m] height of the vehicle from ? to ? (from below)

#define VEHICLE_MAX_LEFT_STEER  0.3495 // maximum turning angle to the left
#define VEHICLE_MAX_RIGHT_STEER 0.3641 // maximum turning angle to the right
#define VEHICLE_MAX_AVG_STEER   0.3568 // average max turning angle

// TODO: [Luis] Can make these names more descriptive
#define  VEHICLE_H   (1.95/2.0)  // [m], height of Center of Mass from ground
                                 // TODO: NOTE: this is the worst case scenario, 
                                 // since cannot know from spec sheet
                                    
#define  VEHICLE_L   (1.651/2.0) // [m], distance of left wheel to projection of CM down on axle
#define  VEHICLE_R   (1.651/2.0) // [m], distance of right wheel to projection of CM down on axle

// TODO: Check the tire radius with the new set of tires
#define  VEHICLE_TIRE_RADIUS   0.4216  

// [m] distance between front and rear axles
// TODO: Check this number! Dave says in 
// http://grandchallenge.caltech.edu/bugzilla/show_bug.cgi?id=185
// that the vehicle wheelbase is 111.5" = 2.832 m
#define  VEHICLE_AXLE_DISTANCE   2.985  
#define  VEHICLE_WHEELBASE       VEHICLE_AXLE_DISTANCE

#define  VEHICLE_REAR_TRACK      1.613  // [m] distance between centerline of rear wheels
                                        // TODO: This has changed from stock with the new wheels
#define  VEHICLE_FRONT_TRACK     1.628  // [m] distance between centerline of front wheels
                                        // TODO: This has changed from stock with the new wheels

#define  VEHICLE_AVERAGE_TRACK   (VEHICLE_REAR_TRACK+VEHICLE_FRONT_TRACK)/2.0

		
/* from http://www.internetautoguide.com/reviews/1996/1996_Chevrolet_Tahoe.html
    1996 Chevy Tahoe          Model tested	Tahoe LT 4-door
    Specifications
    Base engine	5.7-liter ohv V-8
    Horsepower @ rpm	250 @ 4600
    Torque, lb.-ft. @ rpm	335 @ 2800
    EPA fuel economy, mpg city/hwy	13/16
    Optional engines	180-hp 6.5-liter ohv V8 turbodiesel
    Standard transmission	4-speed automatic
    Optional transmission	NA
    Wheelbase	117.5 in. (2.985 m)
    Length/width/height	199.1/76.4/70.2 in. (5.057/1.941/1.783 m)
    Track, f/r 	64.1/63.5 in. (1.628/1.613 m)
    Min. ground clearance	9.6 in. (2wd) (0.244 m)
    Turning circle	41.5 ft. (12.649 m)
    Suspension, f/r	ind./live
    Min. curb weight	4731 lbs.
    Standard tires	P235/75R-15
    Brakes, f/r	disc/drum
    Fuel tank capacity	30.0 gal.
*/

/* from http://www.new-cars.com/2003/chevrolet/chevy-tahoe-specs.html
    Exterior Dimensions  	2003 Chevy Tahoe
    Wheelbase (in / mm) 	116 / 2946
    Overall length (in / mm) 	198.8 / 5050
    Overall width (in / mm) 	78.8 / 2002
    Overall height (in / mm) 	2WD 	74.8 / 1899 4WD 	76.7 / 1948
    Track (in / mm) 	Front 	65 / 1651 Rear 	66 / 1676
    Minimum ground clearance (in / mm) 	8.4 / 213.4
    Ground to top of load floor (in / mm) 	2WD 	30 / 762 4WD 	31.5 / 800
    Approach angle 	19.8\x{00B0}
    Departure angle 	27.3\x{00B0}
    Curb weight (lbs / kg) 	2WD 	4828 / 2190 4WD 	5050 / 2291
    Weight distribution (% front / rear) 	2WD 	51 / 49 4WD 	53 / 47
*/
#endif
