#ifndef CONSTANTS_H
#define CONSTANTS_H

// constants only relevant to the ladar
#include "LadarConstants.h"
#include "VehicleConstants.h"
#include "SimulatorConstants.h"

// INCLUDE ONLY ONE OF THESE - these define maximum allowed speed, max
//   speed when an obstacle is detected, how close to allow obstacles to
//   get, etc...
#include "QIDConstants.h"
// #include "RACEConstants.h"
// #include "TESTConstants.h"

// Conversions
#define meters_per_foot 0.304800609
#define mps_per_mph     0.447040893

#define RDDF_FILE "rddf.dat"

#define GPS_SERIAL_PORT 4
#define OBD_SERIAL_PORT 6
#define MAG_SERIAL_PORT 8
#define LADAR_ROOF_SERIAL_PORT 0
#define LADAR_BUMPER_SERIAL_PORT 5
#define STEER_SERIAL_PORT 0
#define BRAKE_SERIAL_PORT 5

// Distance between the gps in the middle of the rear axle and the middle of the front of the car
#define GPS_2_FRONT_DISTANCE 3.0

// Maximum deceleration (how long it takes to slow down under **full braking**)
// This value is used in GlobalPlanner2 (defunct) and was previously used in 
// the generateArbiterVotes method of genMap.cc, but not anymore
#define MAX_DECEL 3.0 // m/s^2

// Deceleration value used for determining what speed we want to 
// go given an obstacle is some distance in front of us.  This value is used
// in the generateArbiterVotes method of genMap.cc
#define  NOMINAL_DECELERATION  1.5 // m/s/s 

// Maximum steer deflection in radians, from straight, assumed symmetric
// Only used by the simulator. Other modules (Planners) use MAX_STEER defined in 
// /team/RaceModules/Arbiter/SimpleArbiter.hh
#define SIM_MAX_STEER 25.0*M_PI/180.0               

// Maximum steer deflection in radians, from straight, assumed symmetric
// Used by global_functions.[cc/hh] because it causes great headaches to use
// the const double MAX_STEER defined in
// team/RaceModules/Arbiter/SimpleArbiter.hh
//#define USE_MAX_STEER 25.0*M_PI/180.0               
#define  USE_MAX_STEER  VEHICLE_MAX_AVG_STEER

// This is the maximum speed the DFE will allow us to go
// It's used in RaceModules/DFEPlanner/DFE.cc
// #define  DFE_MAX_SPEED  15.0 // m/s


// PATH EVALUATOR CONSTANTS
#define VECTOR_PROD_THRESHOLD 0.866 // cos(30) = 0.866
#define HEIGHT_DIFF_THRESHOLD 0.5   // 0.5m difference between cells
#define TIRE_OFF_THRESHOLD 1        // 1m from a tire and the plane determined by the other three
#define OBSTACLE_THRESHOLD 70       // bellow this threshold it's considered an obstacle

#define EXP_PARAM 2.0               // Defines the exponential parameter that models the votes
#define MIN_VOTE_RANGE 0.0
#define MAX_VOTE_RANGE 100.0
#define MAX_REACTION_TIME_BETWEEN_SENSOR_SWEEP_AND_BEGINNING_DECELLERATION_FOR_EVALUATING_VELOCITY 0.5 //The velocity evaluattion in getArbiterVotes uses this to evaluate the velocity at which the vehicle can travel along a given arc. We need to arrive at a more accurate estimate of this value.

#endif
