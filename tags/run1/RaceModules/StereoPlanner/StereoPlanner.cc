
#include "StereoPlanner.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include <unistd.h>
#include <iostream>

extern int QUIT;
extern int DISPLAY;
extern int PRINT_VOTES;
extern int no_vote, verbose, which_pair;

StereoPlanner::StereoPlanner() 
  : DGC_MODULE(MODULES::StereoPlanner, "Stereo Planner", 0) {

}

StereoPlanner::~StereoPlanner() {
}


void StereoPlanner::Init() {
  // call the parent init first
  printf("Got to line %d in %s\n", __LINE__, __FILE__);
  DGC_MODULE::Init();
  printf("Got to line %d in %s\n", __LINE__, __FILE__);
  //  InitStereo();
  cout << "Module Init Finished" << endl;
}

void StereoPlanner::Active() {
  int logGenMap = 0;

  RunMethodInNewThread<StereoPlanner>(this, &StereoPlanner::StereoCaptureThread);

#ifdef LOG_GEN_MAP
  //VARIABLES NEEDED TO LOG GEN MAP
  time_t curTime;
  struct tm* localTime;
  VBString filename, voteFileName, directory, command, msg;
  double diff_northing,diff_easting;
  int ret, logCount=0;
  char year[5], month[3], day[3], hour[3], min[3], sec[3];
  char genMapFile[256],genMapMsg[256];
  ofstream voteFile;
  // Get the current time
  curTime = time(NULL);
  // Convert it to local time representation
  localTime = localtime (&curTime);
  ret = sprintf(year,"%d",localTime->tm_year + 1900);
  ret = sprintf(month,"%d",localTime->tm_mon + 1);
  ret = sprintf(day,"%d",localTime->tm_mday);
  ret = sprintf(hour,"%d",localTime->tm_hour);
  ret = sprintf(min,"%d",localTime->tm_min);
  ret = sprintf(sec,"%d",localTime->tm_sec);
  directory = "genMapLog_";
  directory += year;
  directory += "-";
  directory += month;
  directory += "-";
  directory += day;
  directory += "_";
  directory += hour;
  directory += "h";
  directory += min;
  directory += "m";
  directory += sec;
  directory += "s";
  voteFileName = directory;
  voteFileName += "/";
  //voteFileName += directory;
  voteFileName += "genMap.votes";
  /*filename += year;filename += "-";filename += month;filename += "-";
  filename += day;filename += "_";filename += hour;filename += "h";
  filename += min;filename += "m";filename += sec;filename += "s";*/
  command = "mkdir ";
  command += directory;
  //cout << "directory   = " << directory << endl;
  //cout << "votes file  = " << voteFileName << endl;
  //cout << "genMap file = " << filename << endl;
  //getchar();
  system(command.c_str());
  voteFile.open(voteFileName.c_str());
  voteFile << "# VOTES TO ARBITER FROM 0 TO 24\n";
  voteFile << "# Nro: 100;100;90; ...\n#\n";
  //END LOG GEN MAP
#endif


  cout << "Starting Active Funcion" << endl;
  while( ContinueInState() ) {

    // update the state so we know where the vehicle is at

#ifdef LOG_GEN_MAP
    //LOG GEN MAP ***BEFORE UPDATE STATE***
    diff_northing = d.SS.Northing;
    diff_easting = d.SS.Easting;
    //END LOG GEN MAP
#endif

    if(verbose) printf("Updating state...");
    UpdateState();
    if(verbose) printf("Got state\n");

#ifdef LOG_GEN_MAP
    //LOG GEN MAP ***AFTER UPDATE STATE***
    diff_northing = d.SS.Northing - diff_northing;
    diff_easting = d.SS.Easting - diff_easting;
    //END LOG GEN MAP
#endif

    // Update the votes ... 
    StereoUpdateVotes();
    if(QUIT) return;
    // and send them to the arbiter
    Mail m = NewOutMessage(MyAddress(), MODULES::Arbiter, ArbiterMessages::Update);
    // Send the arbiter our "ID" and the vote struct.  The weights are taken care of
    // by the arbiter.
    int myID = -1;  

    if(which_pair == SHORT_RANGE) {
      myID = ArbiterInput::Stereo;
    } else if(which_pair == LONG_RANGE) {
      myID = ArbiterInput::StereoLR;
    }
    m << myID << d.stereo;
    if(no_vote == 0) {
      SendMail(m);
    }
// logging genMap
#ifdef LOG_GEN_MAP
    if (!(logGenMap % 10))
    {
      usleep(1000);
      //      cout << "---saving genMap using saveMATFile" << endl;
      msg = " yaw  = ";
      msg += d.SS.Yaw;
      msg += " diff_northing = ";
      msg += diff_northing;
      msg += " diff_easting = ";
      msg += diff_easting;
      filename = directory;
      filename += "/";
      filename += "genMap_";
      filename += logCount;
      strcpy(genMapFile,filename.getStr());
      strcpy(genMapMsg,msg.getStr());
      d.svMap.saveMATFile(genMapFile, genMapMsg, "PEmap");

      voteFile << logCount << ": ";
      for(int i = 0; i < NUMARCS; i++) {
	//        voteFile << vel_goodness[i].second << ";";
      }
      voteFile << endl;
      
      logCount++;
    }
    logGenMap++;
#endif

    // and sleep for a bit
    //usleep(1000);
  }

  cout << "Finished Active State" << endl;

}


void StereoPlanner::Shutdown() {

  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;
  StereoShutdown();

}



