/*
** get_vstate.cc
**
**  
** 
**
*/

#include "StereoPlanner.hh" // standard includes, arbiter, datum, ...

void StereoPlanner::UpdateState()
{ 
  Mail msg = NewQueryMessage( MyAddress(), MODULES::VState, VStateMessages::GetState);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> d.tempState; // download the new state
  }

} // end StereoPlanner::UpdateState() 
