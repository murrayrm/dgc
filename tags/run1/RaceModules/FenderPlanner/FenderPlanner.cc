
#include "FenderPlanner.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include <unistd.h>
#include <iostream>
#include <stdlib.h>

FenderPlanner::FenderPlanner() 
  : DGC_MODULE(MODULES::FenderPlanner, "Fender Planner", 0) {
  //  : DGC_MODULE(MODULES::DFEPlanner, "DFE Planner", 0) {  // this is only if we're pretending to be the DFE

}

FenderPlanner::~FenderPlanner() {
}


void FenderPlanner::Init() {
  // call the parent init first
  DGC_MODULE::Init();

  InitFender();
  cout << "Module Init Finished" << endl;
}

void FenderPlanner::Active() {

  cout << "Starting Active Funcion" << endl;
  while( ContinueInState() ) {
    system("clear");
    cout << "Active Function" << endl;
    // update the state so we know where the vehicle is at
    UpdateState();

    // update the votes...
    FenderUpdateVotes();
    // and send them to the arbiter
    Mail m = NewOutMessage(MyAddress(), MODULES::Arbiter, ArbiterMessages::Update);
    // Send the arbiter our "ID" and the vote struct.  The weights are taken care of
    // by the arbiter.
    int myID = ArbiterInput::FenderPlanner;
    //    int myID = ArbiterInput::DFE; // just to pretend we're DFE
    m << myID << d.fender;
    SendMail(m);

    // and sleep for a bit.  if you do not sleep here it will try to run
    // too fast and flood the MTA
    usleep(100000);
  }

  cout << "Finished Active State" << endl;

}


void FenderPlanner::Shutdown() {

  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;
  //FenderShutdown();  // necessary??
}



/*Mail FenderPlanner::QueryMailHandler(Mail& msg)
{
  Mail reply;   // allocate space for the reply

  reply = DGC_MODULE::QueryMailHandler(msg);

  return reply;
}
*/
