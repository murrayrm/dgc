/* 
   fenderDDF.cc - implementation for fenderDDF.hh
   it's a lot like rddf.hh, actually...
   created by Rocky Velez, March 3, 2004
*/

#include "fenderDDF.hh"
#include <iomanip.h>

// Constructors
fenderDDF::fenderDDF()
  : numSegments(0)
{
  readFDDF("fenderDDF.dat");
}

fenderDDF::fenderDDF(char *pFileName)
  : numSegments(0)
{
  readFDDF(pFileName);
}

// Destructor
fenderDDF::~fenderDDF(){
}

// Accessors
bool fenderDDF::fenderpointsInSegment(int segmentNum){
  if (segmentFPdata[segmentNum-1].numFenderpoints > 0)
    return true;
else
  return false;
}

staticFender fenderDDF::findCentroid(int segmentNum, staticFender pos){
  // Find the speed-weighted centroid for all points in the current corridor closer to the waypoint than BOB is
  // 1. sum.n = 0, sum.e = 0, sumSpeed = 0
  // 2. for i in struct
  //    1. if fp.dist <= bob.dist then  
  //       1. sum.n = sum.n + fp.n * fp.speed
  //       2. sum.e = sum.e + fp.e * fp.speed
  //       3. sumSpeed = sumSpeed + fp.speed
  //   centroid.n = sum.n/sumSpeed.n, centroid.e = sum.e/sumSpeed.e
  //   Cast votes
  //   if (still in corridor) then goto 3.1 else goto 1
//cout << "segment num: " << segmentNum << endl;
  staticFender sum;
  sum.N = 0;
  sum.E = 0;
  sum.speed = 0;
  double wpnorthing = segmentFPdata[segmentNum - 1].WPnorthing;
  double wpeasting = segmentFPdata[segmentNum - 1].WPeasting;

   // cout << "numFenderPoints= " << segmentFPdata[segmentNum-1].numFenderpoints << endl;

  for (int fpNum = 0; fpNum < segmentFPdata[segmentNum - 1].numFenderpoints; fpNum++) {

    fenderpoint fp = segmentFPdata[segmentNum-1].fplist[fpNum];
    if (fp.DistToWP <= distBOB2WP(wpnorthing, wpeasting, pos)) {
      sum.N += fp.FPnorthing * fp.Speed;
      sum.E += fp.FPeasting * fp.Speed;
      sum.speed += fp.Speed;
    }
  }

/*  staticFender sum;
  sum.N = 3846460;
  sum.E = 499065;  // this is just so to make sure calc_steer_cmd() works
  sum.speed = 1;
*/
  staticFender centroid;
  centroid.N = sum.N / sum.speed;
  centroid.E = sum.E / sum.speed;

  return centroid;
}

int fenderDDF::getCurrentSegment(double Northing, double Easting) {
  // this is the same code as getCurrentWaypointNumber in RDDF/rddf.cc with a few minor modifications
  // the proper segment corresponds with the next waypoint (which is what this returns)
  //cout << "getCurrentSegment: " ;

  // set the current position vector
  Vector curPos( Northing, Easting );
  cout << "curPos.N: " << Northing << ", curPos.E: " << Easting << endl;
  // declare waypoint vectors
  Vector curWay, nextWay;

  // minimum found distance to waypoint
  double minDist = 123456789;
  int minDistWay = 0;
  
  // cycle through all of the segments
  for( int i = 0; i < numSegments; i++ ) 
  {
    //cout << "loop through segment " << i+1 << ": " ;
    curWay.N = segmentFPdata[i].WPnorthing;
    //cout << ", curWay.N: " << curWay.N;
    curWay.E = segmentFPdata[i].WPeasting;
    //cout << ", curWay.E: " << curWay.E;
    if (i != numSegments - 1) {
      nextWay.N = segmentFPdata[i+1].WPnorthing;
      //cout << ", nextWay.N: " << nextWay.N;
      nextWay.E = segmentFPdata[i+1].WPeasting;
      //cout << ", nextWay.E: " << nextWay.E << endl;
    }
    else {
      nextWay.N = 0;
      //cout << ", nextWay.N: " << nextWay.N;
      nextWay.E = 0;
      //cout << ", nextWay.E: " << nextWay.E << endl;
    }

    // check to see if we are within the corridor segment
    // if we are, just return with the index of the segment
    if (i != 0) {
      //cout << "segmentFPdata[i-1].WPradius: " << segmentFPdata[i-1].WPradius << endl;
      //cout << "withinCorridor value: " << withinCorridor( curPos, curWay, nextWay, segmentFPdata[i-1].WPradius) << endl;
      if( withinCorridor( curPos, curWay, nextWay, segmentFPdata[i-1].WPradius ) ) { 
	// remember wp's are at the second end of the segment, but radius is from first end.
	printf("------ I am within corridor segment %d -------\n", i ); 
	return i+1;
      } 
    }
    else {
      //cout << "segmentFPdata[i].WPradius: " << segmentFPdata[i].WPradius << endl;
      //cout << "withinCorridor value: " << withinCorridor( curPos, curWay, nextWay, segmentFPdata[i].WPradius) << endl;
      if (withinCorridor(curPos, curWay, nextWay, segmentFPdata[i].WPradius)) {
	printf("-----  I am within corridor segment %d -------\n", i); 
	return i+1;
      }
    } 
      
    // if this waypoint is closer than any other we've found,
    // then remember so 
    //cout << "waypoint is closer than any other we've found" << endl;
    //cout << "curPos.N - curWay.N: " << curPos.N - curWay.N << ", curPos.E - curWay.E: " << curPos.E - curWay.E << endl;
minDist = hypot(curPos.N - curWay.N, curPos.E - curWay.E); 
    //cout << "minDist: " << minDist << endl;

    if( hypot(curPos.N - curWay.N, curPos.E - curWay.E) < minDist ) 
      {
	minDist = hypot(curPos.N - curWay.N, curPos.E - curWay.E); 
	minDistWay = i;
      }
  }
  printf("------ I am NOT within the corridor -------\n" ); 

  cout << "minDistWay: " << minDistWay << endl;
  
  // if we get to this point in the function, it means that we are
  // **not** within the corridor.  In this case, return the index of
  // the waypoint that is closest to us, +1.  the +1 is so we go to the segment
  // that starts with minDistWay, rather than ends with it.  But the final
  // waypoint is a special case
  if(minDistWay == numSegments -1){
	//we are shooting for the final waypoint
	return minDistWay;}
  else {
	return minDistWay + 1; }
} 

int fenderDDF::getNumSegments() {
  return numSegments;
}

bool fenderDDF::withinCentroid(staticFender pos, staticFender centroid, double threshold) 
{
  return threshold > sqrt((pos.N-centroid.N)*(pos.N-centroid.N) + (pos.E-centroid.E)*(pos.E-centroid.E));
}

// Private functions

int fenderDDF::readFDDF(char * fileName){
 // reads the fenderDDF
  cout << "readFDDF is running\n";
  int segmentCount;
  ifstream file;
  string line;
  segmentData eachSegment;
  
  file.open(fileName,ios::in);
  if(!file){
    cout << "UNABLE TO OPEN THE FenderDDF FILE" << endl;
    exit(1);
  }
  file >> segmentCount;              //first number of file is segment count
  
  //  cout << setprecision(10);
  //  cout << "segmentCount= " << segmentCount << endl;
  segmentFPdata = new segmentData[segmentCount];   //get some memory
  numSegments = segmentCount;         //set this private variable
  //while(!file.eof()){
  // need to check for lines starting with '#' (these should be ignored) - check bob reading format
  //skipping that detail for now
  //  cout << "about to loop on mySegNum\n";
  for (int mySegNum = 0; mySegNum < segmentCount; mySegNum++ ) {
//    cout << "mySegNum= " << mySegNum << endl;
    // Number of fenderpoints

    getline(file,line,SEGMENTDATA_DELIMITER);
    segmentFPdata[mySegNum].numFenderpoints = atoi(line.c_str());
    //    cout << "numFenderpoints= " << segmentFPdata[mySegNum].numFenderpoints << endl;
    segmentFPdata[mySegNum].fplist = new fenderpoint[segmentFPdata[mySegNum].numFenderpoints];    
    
    // Segment Number
    getline(file,line,SEGMENTDATA_DELIMITER);
    segmentFPdata[mySegNum].segmentNumber = atoi(line.c_str());
    // segment 1 corresponds to WP 1 (from WP 0 to WP 1) => WP 0/segment 0 doesn't exist

    //    cout << "Segment Number: " << segmentFPdata[mySegNum].segmentNumber << endl;
    //get the waypoint coordinates
    getline(file,line,FENDERPT_DELIMITER);
    segmentFPdata[mySegNum].WPnorthing = atof(line.c_str());  // WPnorthing
    
    getline(file,line,FENDERPT_DELIMITER);
    segmentFPdata[mySegNum].WPeasting = atof(line.c_str());   // WPeasting
     
    getline(file,line,SEGMENTDATA_DELIMITER);
    segmentFPdata[mySegNum].WPradius = atof(line.c_str());   // WP radius

    // Now add each fenderpoint and its data into fplist:

    //    cout << "about to loop on fenderpoints\n";

    for (int i = 0; i < segmentFPdata[mySegNum].numFenderpoints; i++) {
      //      cout << setprecision(10);
      //      cout <<"fp num= " << i << endl;
      getline(file,line,FENDERPT_DELIMITER);
      segmentFPdata[mySegNum].fplist[i].FPnorthing = atof(line.c_str());  // FPnorthing
      //      cout << "fpnorthing: " << segmentFPdata[mySegNum].fplist[i].FPnorthing << ", " ;
  
      getline(file,line,FENDERPT_DELIMITER);
      segmentFPdata[mySegNum].fplist[i].FPeasting = atof(line.c_str());   // FPeasting
      //      cout <<"fpeasting: " << segmentFPdata[mySegNum].fplist[i].FPeasting << ", " ;

      getline(file,line,FENDERPT_DELIMITER);
      segmentFPdata[mySegNum].fplist[i].DistToWP = atof(line.c_str());    // DistToWP
      //      cout << "distToWP: " << segmentFPdata[mySegNum].fplist[i].DistToWP << ", " ;
      
      getline(file,line,SEGMENTDATA_DELIMITER);
      segmentFPdata[mySegNum].fplist[i].Speed = atof(line.c_str());       // Speed
      //      cout << "Speed: " << segmentFPdata[mySegNum].fplist[i].Speed << endl;
    }

    getline(file, line, SEGMENT_DELIMITER);
  }
cout << "finished looping on mySegNum";
/*relic from vector days
   if (numSegments != segmentFPdata.size())
    cerr << "error occured in filling in number of segments into segmentFPdata" << endl;
 */

  // debug
  //cout << "RDDF: " << numTargetPoints << " target points read !!!" << endl;
  return(0);
}

double fenderDDF::distBOB2WP(const double &WPnorthing, const double &WPeasting,const staticFender &pos){
  return hypot(pos.N - WPnorthing, pos.E - WPeasting); 
}

