#include <sys/time.h>
#include <iomanip>
#include <algorithm>  // for min
#include <stdio.h>

#include "FenderPlanner.hh"

#include "fender.hh"

using namespace std;

// Reads in fender points from the specified file (hardcoded now)
// Fender point file format is:  Northing  Easting
int FenderPlanner::InitFender() 
{
  cout << "[InitFender] Starting InitFender()" << endl;
  cout << "[InitFender] d.fenderNum = "        << ArbiterInput::FenderPlanner << endl;

 // Make sure we update our state first 
  while( d.SS.Northing < 100000.0 ) 
  {
    printf("[InitFender] Stuck trying to update state...\n");
    UpdateState();
    // chill out.  
    usleep(500000);
  }
 
  //  d.fp.read("fenderpoints.gps");     // this is just so that it'll compile; these methods will eventually get put into staticFender...
  //  d.fp.readRDDF("rddf.dat");
  //  cout << " Done reading fender point data" << endl;

  //  cout << " File contains " << d.fp.get_size() << " fender points." << endl;

  cout << " --- SHOOTING FOR FENDER CENTROID --- "; 
  // should this announce location of centroid?
  //  printf("(%10.2f,%10.2f)\n",d.fp.get_current().easting,d.fp.get_current().northing);
  sleep(2);

  return true;

}

/*void FenderPlanner::FenderUpdateVotes() 
{
  // This is where we send votes to the arbiter

  // Eventually what will need to happen is this will look something like GlobalUpdateVotes(), 
  // but instead of heading towards a particular waypoint, the vehicle will go towards the centroid
  // of the closest fender points.

  // Fake votes...
  for (int i = 0; i<NUMARCS; i++) {
    d.fender.Votes[i].Goodness = 2*i;
    d.fender.Votes[i].Velo = i;
    cout << "Goodness: " << d.fender.Votes[i].Goodness << ", Velo: " << d.fender.Votes[i].Velo << endl;
  }
  
  
  } // end FenderUpdateVotes */  // there's a more extensive version of FenderUpdateVotes in FenderUpdateVotes.cc


void FenderPlanner::FenderShutdown() 
{

}
