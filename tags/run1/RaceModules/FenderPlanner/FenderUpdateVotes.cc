using namespace std;

#include "fender.hh"
#include "include/Constants.h"

/******  remember to explain in documentation why using staticFender and not Vector ******/

// just like GlobalUpdateVotes(), but it takes the centroid of a set of 
// fenderpoints into account instead of waypoints.

double calc_steer_command(staticFender curPos, staticFender curCentroid, double curHeading) {
  double desiredRelYaw;   // the desired Yaw (clockwise angle from forward) 
  double steerCommand; // the desired steering angle
  double centroid_angle;    // angle to the centroid (measured from east)
  
  staticFender centroid_rel;
  centroid_rel.N = curCentroid.N - curPos.N;
  centroid_rel.E = curCentroid.E - curPos.E;
  
  // get the angle between head and way 
  centroid_angle = atan2( centroid_rel.N, centroid_rel.E );
  
  //  cout << "centroid_angle = " << centroid_angle << endl;
  //  cout << "head_angle = " << head_angle << endl;

  // Desired heading in vehicle reference frame
  desiredRelYaw = curHeading - centroid_angle;

  // Normalize the desired heading to be in [-pi,pi]
  if( desiredRelYaw > M_PI ) desiredRelYaw = desiredRelYaw - 2.0*M_PI;
  if( desiredRelYaw < -M_PI ) desiredRelYaw = desiredRelYaw + 2.0*M_PI;

  // Should probably print this out
  // cout << "centroid_rel.N= " << centroid_rel.N << endl << "centroid_rel.E= " << centroid_rel.E << endl;

   //cout << "Desired Yaw to target (deg in local frame) = " 
//	cout << desiredRelYaw*180.0/M_PI << endl;

  // Saturate the steering command to be in [-USE_MAX_STEER, USE_MAX_STEER]
  steerCommand = fmax( fmin( desiredRelYaw, USE_MAX_STEER ), -USE_MAX_STEER ); 
  cout << "steerCommand: " << steerCommand << endl;
  return steerCommand;

} // end calc_steer_command(staticFender curPos, staticFender curCentroid, staticFender curHead)

// we're not going to worry about speed control... we'll send equal speed votes to the arbiter


void FenderPlanner::FenderUpdateVotes() 
{
  bool done = false;
  
  double steer_cmd; // commanded steering angle (rad)
  //  double speed_cmd; // commanded speed (m/s)
  
  staticFender pos;           // our current UTM position
  staticFender vehFrontPos;  // do i need this?
  pos.N = d.SS.Northing;
  pos.E = d.SS.Easting;
  
  /* Vote generation:
   * 1. Determine current segment
   * 2. If no fenderpoints are within the current corridor:
   *    1. Vote all arcs equal
   *    2. If corridor has changed go to 2.0
   *    3. else sleep (10 ms)
   *    4. Go to 2.0
   * 3. Else, find the centroid in corridor
   * 4. Cast votes
   * 5. Go to 1
   */
  int segmentNumber = d.fenderpoints.getCurrentSegment(pos.N, pos.E);
  
  // if we're in the last segment
  if (segmentNumber == d.fenderpoints.getNumSegments() - 1) {
      //cout << "Last segment!!" << endl;
      done = true;
    }     
   
/* if no fp in segment
     set all votes equal
     return
   else
     find centroid
     calc steer command
     calc speed command
     return 
*/

//cout << "passed the last segment test\n";
  /*    if (segmentNumber == d.fenderpoints.getCurrentSegment(pos.N, pos.E))
	usleep(10000);
	else {
	segmentNumber = d.fenderpoints.getCurrentSegment(pos.N, pos.E);
	continue;  // i think this is the right command...  correct me if i'm wrong!
	}*/
  
  /* 3. Else, find the centroid in corridor
   * 4. Cast votes
   * 5. Go to 1
   */
  
  if (d.fenderpoints.fenderpointsInSegment(segmentNumber)) {
    staticFender currentCentroid = d.fenderpoints.findCentroid(segmentNumber, pos);  // find centroid in current segment
    vehFrontPos.E = pos.E + VEHICLE_LENGTH * cos(M_PI_2 - d.SS.Yaw);  // are these even slightly necessary?
    vehFrontPos.N = pos.N + VEHICLE_LENGTH * sin(M_PI_2 - d.SS.Yaw);
    if (!done) {
      segmentNumber = d.fenderpoints.getCurrentSegment(pos.N, pos.E);
      
      d.SS.Yaw = atan2( sin(d.SS.Yaw), cos(d.SS.Yaw) ); 
      
      // calculate the desired steering angle 
      steer_cmd = calc_steer_command(pos, currentCentroid, M_PI_2 - d.SS.Yaw);  
      printf("current segment num = %d\n", segmentNumber);
      printf("Current position  = %10.3f m Easting, %10.3f m Northing\n", 
	     d.SS.Easting, d.SS.Northing );
      printf("Centroid position = %10.3f m Easting, %10.3f m Northing\n",  currentCentroid.E  , currentCentroid.N );
      printf("Vehicle front position = %10.3f m Easting, %10.3f m Northing\n", vehFrontPos.E  , vehFrontPos.N );
      printf("Relative position = %10.3f m Easting, %10.3f m Northing\n", 
	     currentCentroid.E - d.SS.Easting , currentCentroid.N - d.SS.Northing );
      printf("Rel. pos. from front = %10.3f m Easting, %10.3f m Northing\n", 
	     currentCentroid.E - vehFrontPos.E, currentCentroid.N - vehFrontPos.N );
      printf("yaw = %f [%.1f degrees], steer_cmd = %f\n", d.SS.Yaw, d.SS.Yaw*180.0/M_PI, steer_cmd ); 
    }
    
    else {
      
      cout << "[FenderLoop] Fenderpoint following done" << endl;
      cout << "[FenderLoop] Calling ForceKernelShutdown()!" << endl;
      
      // Tell the arbiter to stop the demo since we are at the
      // end of the waypoints - this will stop the vehicle
      for(int i=0; i<NUMARCS; i++) {
	d.fender.Votes[i].Goodness = 0.0;
	d.fender.Votes[i].Velo = 0.0;
      }
      Mail m = NewOutMessage(MyAddress(), MODULES::Arbiter, 
			     ArbiterMessages::Update);
      
      sleep(1); // wait a second
      ForceKernelShutdown();
    }
    
    
    // this stuff is exactly the same as in GlobalUpdateVotes...  
    // since we only want to head in the general direction of the 
    // centroid, there's no reason to create a set of arcs, like most 
    // of the other modules.
    // in the interest of space, if you want to know what used to be 
    // written here, refer to GlobalUpdateVotes
    
    
    double goodness = 0;
    
    int i = 0;
    
    for(i=0; i<NUMARCS; i++) 
      {
	double ggparam = 10.0;
	goodness = fmax( ggparam, MAX_GOODNESS - 
			 fabs( GetPhi(i) - steer_cmd )/2.0 * 
			 (MAX_GOODNESS - ggparam) );
	
	//cout << "i= " << i << ", phi= " << GetPhi(i) << ", DesRelYaw= "
	//	 << steer_cmd << ", goodness= " << goodness << endl;
	
	// assign the actual vote goodness
	d.fender.Votes[i].Goodness = goodness;
	
	//    d.fender.Votes[i].Velo = speed_cmd;
	d.fender.Votes[i].Velo = MAX_SPEED;  // we don't care about speed, so go as fast as is allowed
      }
  }  
  else {  // no fenderpoints in corridor
    for(int i = 0; i < NUMARCS; i++) {
      d.fender.Votes[i].Goodness = MAX_VOTE_RANGE;  // this allows us to say "go wherever you want, we don't care"
      d.fender.Votes[i].Velo = MAX_SPEED;  // we don't care about speed, so go as fast as is allowed
    }
  //  return; //?
  }
    
} // end FenderUpdateVotes
