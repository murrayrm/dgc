#ifndef FENDER_HH
#define FENDER_HH

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <frames/frames.hh>

#include <iostream>
#include "FenderPlanner.hh" // standard includes, arbiter, datum, ...
#include "fenderDDF.hh"
#include "RDDF/rddf.hh"

// define whatever constants are needed here...
#define CENTROID_THRESHOLD 0.001  // is this reasonable???

#endif
