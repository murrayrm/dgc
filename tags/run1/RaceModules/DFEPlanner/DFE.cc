
#include <sys/time.h>
#include <iomanip>
#include <algorithm>  // for min

#include "DFEPlanner.hh"

// These constants have all been removed to team/include/VehicleConstants.h
//#include "DynamicFeasibilityEvaluator/vehicle_constants.h"

#include "DynamicFeasibilityEvaluator/rollover_functions.h"

using namespace std;

extern int SIM; // get the simulation flag from the main function
extern int PRINT_VOTES; // get the print_votes flag from the main function

int DFEPlanner::InitDFE() {

  return true;
}

void DFEPlanner::DFEUpdateVotes() {
    // d.SS.speed is the vehicle velocity, from GPS
    double current_velocity = d.SS.Speed;
    // current_velocity = 5.0;
    double yaw = d.SS.Yaw;
    double pitch =d.SS.Pitch;
    double roll = d.SS.Roll;
    //system("clear"); // clear screen on each loop to show output
    //fprintf(stderr, "speed = %f, yaw = %f, pitch = %f, roll = %f\n", current_velocity, yaw, pitch, roll);

    /* Given yaw, pitch, roll, figure out vertical and lateral components of gravity in the 
     * vehicle reference frame */
    double g_z, g_y, g_x;
    gravity_components_in_vehicle_ref_frame(yaw, pitch, roll, &g_z, &g_y, &g_x);
    //fprintf(stderr, "g_z = %f, g_y = %f, g_x = %f\n", g_z, g_y, g_x);

    // TODO: NOTE: rollover safety threshold should be an input to the DFE, given
    //             by Arbiter (or the race strategist)
    // for now, set it's value here.
    double Thresh_roll = 0.5;

    /* Given rollover safety threshold compute min and max lateral accelerations */
    double a_R, a_L;
    min_max_lateral_acceleration_for_rollover_safety(g_z, g_y, Thresh_roll, &a_R, &a_L);
    //fprintf(stderr, "a_L = %f, a_R = %f\n", a_L, a_R);

    double a_T; // [m/s^2] the total lateral acceleration of the vehicle, due to following the specified trajectory
                // at the specified speed

    // Evalute arcs here
    int goodness = 0;
    double timestamp = 0;        // Dummy for now

    // Now wayPt should contain all the rigth arcs to be evaluated.
    // So evaluate them.
    for(int i=0; i<NUMARCS; i++) 
    {


        /*
          // hardcoded, replace with meaningful calculations
          d.dfe.Votes[i].Goodness = 50.0;
          d.dfe.Votes[i].Velo     = MAX_SPEED;

          /* TODO - Make this code meaningful
                    It is giving a lot of NAN and INF values
        */

        // use code from DynamicFeasibilityEstimator/dynamic_feasibility_evaluator.cc
        
        double arc_steering_angle = GetPhi(i);    
        double arc_radius = steering_angle_2_arc_radius( arc_steering_angle );
        
        // arc_radius is positive for rightward turns, negative for leftwards turns,
        // and ZERO for moving straigh ahead (more useful than infinity)
        
        if (arc_radius == 0)
        {
            // special case of moving straight ahead, total lateral acceleration is zero
            a_T = 0;
        }
        else
        {
            a_T = current_velocity*current_velocity/arc_radius;
            //fprintf(stderr, "angle = %f, a_T = %f\n", arc_steering_angle, a_T);
        }
        
        // given the current velocity and arc (i.e.; current lateral acceleration)
        // compute the rollover coefficient
        double c_roll = rollover_coefficient(a_T, g_z, g_y);
        //fprintf(stderr, "c_roll = %f", c_roll);
        
        double min_safe_velocity, max_safe_velocity;
        min_max_velocity_for_rollover_safety(a_R, a_L, arc_radius, 
		 	                     &min_safe_velocity, &max_safe_velocity);
        //fprintf(stderr, ", v_min = %f, v_max = %f", min_safe_velocity, max_safe_velocity);
        
        // set the vote goodness and max velocity
        // TODO: NOTE: I think the DFE should return more info than just the
        //             goodness of an arc at the current velocity, 
        //             it should evaluate all combinations of arcs and velocities
        //             and take into account the dynamics of changing the velocity as well!!!
         
        if (max_safe_velocity > MAX_SPEED) 
        {
            d.dfe.Votes[i].Velo = MAX_SPEED;
        }
        else 
        {   
            d.dfe.Votes[i].Velo = max_safe_velocity;
        }
          
        // convert rollover coefficient for arc @ current velocity into 
        // a 'goodness'
        // goodness = 0   : very bad    if |c_roll| >= Thresh_roll
        // goodness = 100 : very good   if |c_roll| == 0
        // TODO: NOTE: we may want to have the range Thresh_roll < |c_roll| < 1 not
        //             all be mapped to zero goodness, since they are still
        //             feasible, but just not very desireable
          
        if (fabs(c_roll) > Thresh_roll) 
        {
            d.dfe.Votes[i].Goodness = 0;
        }
        else 
        {
            d.dfe.Votes[i].Goodness = MAX_GOODNESS * (1.0 - fabs(c_roll)/Thresh_roll);
        }

        //fprintf(stderr, ", goodness = %f\n", d.dfe.Votes[i].Goodness);

      
    } // end iterating over arcs to vote on
    
    // and sleep for a bit
    //    usleep(100000);
    // Sleeping is taken care of in the module code that calls update votes function


} // end DFEUpdateVotes


void DFEPlanner::DFEShutdown() {


}

