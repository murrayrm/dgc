#ifndef ARBITERMODULE_HH
#define ARBITERMODULE_HH

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <vector> // for a vector of vote log files

#include "ArbiterModuleDatum.hh"

using namespace std;
using namespace boost;

class Arbiter : public DGC_MODULE {
public:
  // and change the constructors
  Arbiter();
  ~Arbiter();

  // The states in the state machine.
  // Uncomment these if you wish to define these functions.

  void Init();
  void Active();
  void Standby();
  void Shutdown();
  //  void Restart();

  // The message handlers. 
  void InMailHandler(Mail & ml);
  Mail QueryMailHandler(Mail & ml);

  // the status function.
  //string Status();

  // Any other functions [can run separate threads]
  int  InitArbiter();
  void ArbiterLoop();

  // Added data logging 01/31/04
  void UpdateState();

  void SparrowDisplayLoop();
  void UpdateSparrowVariablesLoop();

private:
  // and a datum named d.
  ArbiterDatum d;
};


// enumerate all module messages that could be received
namespace ArbiterMessages {
  enum {
    Start,  // starts the arbiter and outputs to VDrive
    Stop,   // ends execution.
    
    Update, // takes int (vote source) and Voter

    GetVotes,

    // A place holder
    NumMessages
  };
};


#endif
