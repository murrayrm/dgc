
#include "GlobalPlanner.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include <unistd.h>
#include <iostream>

extern int SIM;
extern int DISPLAY;
extern int PRINT_VOTES;


GlobalPlanner::GlobalPlanner() 
  : DGC_MODULE(MODULES::GlobalPlanner, "GLOBAL Planner", 0) {

}

GlobalPlanner::~GlobalPlanner() {
}


void GlobalPlanner::Init() {
  // call the parent init first
  DGC_MODULE::Init();

  InitGlobal();
  cout << "Module Init Finished" << endl;
}

void GlobalPlanner::Active() {

  cout << "Starting Active Funcion" << endl;
  while( ContinueInState() ) {

    // update the state so we know where the vehicle is at
    UpdateState();

    // Update the votes ... 
    GlobalUpdateVotes();
    // and send them to the arbiter
    Mail m = NewOutMessage(MyAddress(), MODULES::Arbiter, ArbiterMessages::Update);
    // Send the arbiter our "ID" and the vote struct.  The weights are taken care of
    // by the arbiter.
    int myID = ArbiterInput::Global;
    m << myID << d.global;
    SendMail(m);

    //    cout << "Sent mail to arbiter" << endl;

    // and sleep for a bit
    usleep(100000);
    system("clear"); // clear screen on each loop to show output in same place
    
  }

  cout << "Finished Active State" << endl;

}


void GlobalPlanner::Shutdown() {

  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;
  GlobalShutdown();

}



