#include "VManage.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"
#include <boost/shared_ptr.hpp>

#include <strstream>


int main (int argc, char**argv) {

  boost::shared_ptr<DGC_MODULE> ptr(new VManage(argc, argv));
  Register(ptr);
  StartKernel();

};


int DEBUG_ON     = false;
int VERBOSE_BOOT = false;
int ERR_OUT      = false;
int OBD_DISABLE = true;


VManage::VManage(int ac, char** av)
  : DGC_MODULE(MODULES::VManage, "MTA VManage", 0),
    argc(ac), argv(av) {
  // Nothing
}

VManage::~VManage() {}

void VManage::Init() {

  // call the default init first
  DGC_MODULE::Init();


  /* Set Default Boot Conditions for each device
     and config variable
  */

  // Serial and parallel ports for devices
  //  d.OBDPort  = OBD_SERIAL_PORT;
  // trans, ig, estop
  

  /* 
   * Parse command line arguments
   *
   * Command line arguments
   * -b -- disable brake by default
   * -t -- disable throttle
   * -s -- disable steering
   * -o -- disable OBD (assume good)
   * -v -- Verbose Bootup
   * -e -- Print Error Messages
   * -d -- ALL DEBUG Messages (bootup and device)

   * Each device should have a flag that can be used to disable that 
   * device/thread from the command line.  Set flag to -1 to indicate
   * that device should not be initialized.
   */

  char c;
  while ((c = getopt(argc, argv, "vrtohbms?")) != EOF)
    switch (c) {
    case 'v':  VERBOSE_BOOT = true;                                break;
    case 'e':  ERR_OUT = true;                                     break;
    case 'd':  DEBUG_ON=true; VERBOSE_BOOT=true; ERR_OUT=true;     break;
    case 'o':  OBD_DISABLE=true;				   break;
    default:
      cout << "Invalid Command Line Option: " << c << endl;
    case 'h':
      cout << " * Valid Command line arguments" << endl
	   << " * -v -- Verbose Bootup" << endl
	   << " * -e -- Print Error Messages" << endl
	   << " * -d -- ALL DEBUG Messages (bootup and device)" << endl;
      exit( 0 );
      break;
    }


  // Setup/Initialize All Devices

  // DEBUG_ON is the flag that is checked when trying to print out stuff.
  // Therefore if we have VERBOSE_DEBUG, but not DEBUG_ON, we must turn
  // on DEBUG_ON while we boot, and then turn it off.
  int tmpturnondebug = false;
  if( VERBOSE_BOOT && ! DEBUG_ON ) {
    tmpturnondebug = true; 
    DEBUG_ON = true;
  }

  int BOOT_FAILED = false;

  // Setup Devices
  // estop, ig, trans, obd.


  // Dont do this here (the car may be in reverse and moving when we 
  // start VManage).
  if (trans_open(TRANS_PORT) < 0) {
    cout << "can't open transmission" << endl;
  }

  // Turn off DEBUG_ON if we turned it on for boot
  if( tmpturnondebug ) DEBUG_ON = false;
  

  // If something failed - quit now 
  if( BOOT_FAILED ) {

    cout << "VMANAGE INITIALIZATION FAILED -- EXITING NOW" << endl;
    cout << "TRY TURNING ON ERROR PRINTING OR VERBOSE BOOT" << endl;
    cout << " * Valid Command line arguments" << endl
	 << " * -v -- Verbose Bootup" << endl
	 << " * -e -- Print Error Messages" << endl
	 << " * -d -- ALL DEBUG Messages (bootup and device)" << endl << endl;
    exit(1);
  }



  // Initialize Entire Datum  for good measure

  // The Stack
  //  vdrive
  d.s.vdrive.smode = 'a';
  d.s.vdrive.amode = 'a';
  d.s.vdrive.estop.EStop = 'p'; // paused on boot
  for(int q=0; q<VDriveDevices::Num; q++) {
    d.s.vdrive.calibrate[q] = false;
  }
  d.s.vdrive.max_velo = 0.0;
  d.VM_for_VD_Count = 0;

  //  obd
  d.s.obd.Velocity = d.s.obd.RPM = d.s.obd.OBDUpdateCounter = 0;
  d.s.obd.valid = false;

  //  transmission/ignition
  d.s.transmission.Gear = -55;  // force set 3rd gear on boot for now
  d.s.ignition.Enabled  = true; // assume car is started for now
  d.TransmissionEStopOn = false; 

  // TimeZero
  d.TimeZero = TVNow();
  
  // OBD Stuff
  d.OBDPort = OBD_SERIAL_PORT;
  d.OBDLastUpdate = TVNow() - Timeval ( 100, 0); // set last update was 100 seconds ago
                                                 // so that it will be outdated

  // Internal EStop Variables
  d.EStopMode = 'o';
  d.VMEStopOn = true; // wait till we initialize to release.
  d.KeyboardEStopOn = false;
  d.EStopGoTime = TVNow() - Timeval( 100, 0); // once again, just invalidate it

  // More ESTop Stuff
  d.EStopPort  = ESTOP_PORT; 
  d.EStopValid = false; // Until we read estop it is not valid
  d.EStopLastUpdate = TVNow() - Timeval( 100, 0); // once again, just invalidate it

  // Transmission/Ignition stuff
  d.TransmissionPort = PP_TRANS;
  d.IgnitionPort     = PP_TRANS;

  // VState Stuff
  d.VStateLastUpdate = TVNow() - Timeval( 100, 0); // once again, just invalidate it
  d.VStateValid = false;
  d.s.vstate.kf_lat = d.s.vstate.kf_lng 
    = d.s.vstate.Vel_E = d.s.vstate.Vel_N = d.s.vstate.Vel_U
    = d.s.vstate.Speed = d.s.vstate.Accel
    = d.s.vstate.Pitch = d.s.vstate.Roll = d.s.vstate.Yaw
    = d.s.vstate.PitchRate = d.s.vstate.RollRate = d.s.vstate.YawRate
    = 0.0;
  d.VStateEStopOn = true;


  // goals
  d.g.SetTransmissionGear   = 3; // set 3rd gear on boot
  d.g.EStopDecelerationRate = 0.0;


  // Now it is time to spawn all the threads that 
  // run everything.
  RunMethodInNewThread<VManage>(this, &VManage::GoalsLoop);     // Start Decision loop
  RunMethodInNewThread<VManage>(this, &VManage::EStopLoop);        // Start EStop Loo
  RunMethodInNewThread<VManage>(this, &VManage::OBDLoop);          // OBD
  RunMethodInNewThread<VManage>(this, &VManage::GetVStateLoop);    // VState update
  RunMethodInNewThread<VManage>(this, &VManage::InvalidatorLoop);  // Invalidator
  RunMethodInNewThread<VManage>(this, &VManage::ConsoleLoop);      // Display loop


  // now release the internal estop
  d.VMEStopOn = false;

}


// VManage::Active
void VManage::Active() {

  while( ContinueInState() ) {
    sleep(1);
  }

}



string VManage::Status() {

  strstream status;
  status << "-----  VManage Status Block  -----" << endl;
  status << "VManage Uptime "     << TVNow() - d.TimeZero
	 << ", Keyboard EStop = " << d.KeyboardEStopOn
	 << ", VMEStop = "        << d.VMEStopOn << endl
	 << "  Hardware EStop = " << d.EStopMode
	 << ", Trans EStop = " << d.TransmissionEStopOn
	 << ", VState EStop = " << d.VStateEStopOn
	 << endl;

  status << "Hardware -- EStop Valid = " << d.EStopValid
	 << ", OBD Valid = " << d.s.obd.valid
	 << endl;

  status << "The Stack:"         << endl;

  status << "    vdrive:" 
	 << "  smode = "  << d.s.vdrive.smode
	 << ", amode = "  << d.s.vdrive.amode
	 << ", estop = "    << d.s.vdrive.estop.EStop
	 << ", Max_Velo = " << d.s.vdrive.max_velo
	 << ", ReqCnt = " << d.VM_for_VD_Count
	 << endl;

  status << "    obd:"
	 << "  Velocity = " << d.s.obd.Velocity
	 << ", RPM = " << d.s.obd.RPM
	 << ", Update Counter = " << d.s.obd.OBDUpdateCounter
	 << ", Valid? = " << d.s.obd.valid 
	 << endl;
  status << "    vstate:"
	 << "  Speed = " << d.s.vstate.Speed
	 << ", Time  = " << d.s.vstate.Timestamp
	 << ", Valid = " << d.VStateValid
	 << endl;
  status << "    transmission/ignition: "
	 << "  Gear = " << d.s.transmission.Gear
	 << ", Car On = " << d.s.ignition.Enabled
	 << endl;


  // Now print out the "Goals" VManage is trying to accomplish
  status << "VManage Goals:" << endl;

  if( d.s.transmission.Gear != d.g.SetTransmissionGear )
    status << "    Want to shift from "
	   << d.s.transmission.Gear << " to " 
	   << d.g.SetTransmissionGear << endl;

  // Deceleration on estop
  if( d.g.EStopDecelerationRate > 0.0 ) {
    status << "    In the event of an EStop we will slow down at "
	   << d.g.EStopDecelerationRate 
	   << " m/s instead of slamming on the brakes." << endl;
  } else {
    status << "    In the event of an EStop we will slam on the brakes."
	   << endl;
  }
  


  string ret;
  //  status >> ret;
  while ( ! status.eof() ) {
    string tmp;
    getline( status, tmp);
    ret += tmp + "\n";
  }

  return ret;

}



void VManage::InMailHandler(Mail& msg) {

  switch (msg.MsgType()) {
  case VManageMessages::Set_Transmission:
    { 
      Transmission_Packet tp;
      msg >> tp;
      cout << "received transmission package; gear = " << tp.Gear << endl;
      if( tp.Gear >= -1 && tp.Gear <= 3 ) d.g.SetTransmissionGear = tp.Gear;
    }
    break;

  case VManageMessages::Set_Ignition:
    { 
      Ignition_Packet ip;
      msg >> ip;

      // do nothing at the moment
    }
    break;

  case VManageMessages::Set_Keyboard_Pause:
    { 
      d.KeyboardEStopOn = true;
    }
    break;
  case VManageMessages::Set_Keyboard_Resume:
    { 
      // lock the datum
      Lock myLock( d.VMDatumMutex );
      if( d.KeyboardEStopOn ) { // only change things if the keyboard is in
	                        // fact set to keyboard pause mode.

	d.KeyboardEStopOn = false;
	d.EStopGoTime = TVNow();
      }
    }
    break;
  case VManageMessages::Set_Steer_Mode:
    {
      // read in the mode 
      char smode; msg >> smode;
      
      switch ( smode ) {
      case 'a': case 'A':
      case 'o': case 'O':
      case 'm': case 'M':
      case 'r': case 'R':
	// Then option is OK.  Allow the change.
	{
	  // lock the datum
	  Lock myLock( d.VMDatumMutex );
	  d.s.vdrive.smode = smode;
	}
	break;
      default:
	cout << "In Mail Hander: Invalid Steer Mode " << smode << endl;
      }
    }
    break;
  case VManageMessages::Set_Accel_Mode:
    {
      // read in the mode 
      char amode; msg >> amode;
      
      switch ( amode ) {
      case 'a': case 'A':
      case 'o': case 'O':
      case 'm': case 'M':
      case 'r': case 'R':
	// Then option is OK.  Allow the change.
	{
	  // lock the datum
	  Lock myLock( d.VMDatumMutex );
	  d.s.vdrive.amode = amode;
	}
	break;
      default:
	cout << "In Mail Hander: Invalid Accel Mode " << amode << endl;
      }
    }
    break;
    
  case VManageMessages::Set_EStop_Behavior:
    // TODO: add in here.
    break;

  default:
    // let the parent mail handler handle it
    DGC_MODULE::InMailHandler(msg);
    break;
  }



}


Mail VManage::QueryMailHandler(Mail & msg) {
  
  Mail reply = ReplyToQuery(msg);

  // figure out what we are responding to
  switch(msg.MsgType()) {  
  case VManageMessages::Get_Transmission:  
    {
      // tack on the transmission packet
      Transmission_Packet tp = Fill_Transmission_Packet();
      reply << tp;
    }    
    break;
  case VManageMessages::Get_Ignition:
    {
      // tack on the ignition packet
      Ignition_Packet ip = Fill_Ignition_Packet();
      reply << ip;
    }    
    break;
  case VManageMessages::Get_EStop:
    {
      // tack on the estop packet
      EStop_Packet ep = Fill_EStop_Packet();
      reply << ep;
    }    
    break;
  case VManageMessages::Get_OBD:
    {
      // tack on the ignition packet
      OBD_Packet op = Fill_OBD_Packet();
      reply << op;
    }    
    break;
  case VManageMessages::Get_VM_for_VD:
    {
      Lock myLock( d.VMDatumMutex );
      // tack on the VM_for_VD packet
      VM_for_VD_Packet vp = d.s.vdrive;
      reply << vp;
      d.VM_for_VD_Count++;
    }    
    break;
  case VManageMessages::Get_VManageStack:
    {
      Lock myLock( d.VMDatumMutex );
      // tack on the VM_for_VD packet
      VManageStack vs = d.s;
      vs.uptime = TVNow() - d.TimeZero;
      reply << vs;
    }
    break;
  default:
    reply = DGC_MODULE::QueryMailHandler(msg);
  }

  return reply;

}





// Functions for VManage to fill these packets
Transmission_Packet VManage::Fill_Transmission_Packet() {
  Lock myLock( d.VMDatumMutex );
  return d.s.transmission;
}

Ignition_Packet VManage::Fill_Ignition_Packet() {
  Lock myLock( d.VMDatumMutex );
  return d.s.ignition;
}

EStop_Packet VManage::Fill_EStop_Packet() {
  Lock myLock( d.VMDatumMutex );
  return d.s.vdrive.estop;
}

OBD_Packet VManage::Fill_OBD_Packet() {
  Lock myLock( d.VMDatumMutex );
  return d.s.obd;
}
