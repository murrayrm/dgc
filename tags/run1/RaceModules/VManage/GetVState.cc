#include "VManage.hh"

#include "MTA/Kernel.hh"
#include "MTA/Modules.hh"

#include <iostream>
using namespace std;

extern int DEBUG_ON;
extern int ERR_OUT;

void VManage::GetVStateLoop() {

  int vstate_status;
  VState_Packet myPacket;

  while( !ShuttingDown() ) {

    int ret = Get_VState_Packet( MyAddress() , myPacket);
    if ( ret >= 0 ) {
      Lock mylock( d.VMDatumMutex );
      
      // update datum
      d.s.vstate = myPacket;
      vstate_status = state_validity(d.s.vstate);
      if (vstate_status == STATE_VALID){
	d.VStateLastUpdate = TVNow();
	d.VStateValid = true;
      }else{
	d.VStateValid = false;
	
	// Kill vstate if it is not behaving 
	if( vstate_status == STATE_KILL ) {
	  Mail msg = NewOutMessage (MyAddress(), MODULES::VState, VStateMessages::KILL);
	  SendMail( msg );
	}
      }
 
    } else {
      //      d.VStateValid = false;
      //      cout << "ERROR Receiving vstate" <<endl;
    }

    usleep( 100000 );
  }
}


