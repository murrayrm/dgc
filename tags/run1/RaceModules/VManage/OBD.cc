#include "VManage.hh"

#include "vehlib/OBDII.h"

#include <iostream>
using namespace std;

extern int DEBUG_ON;
extern int ERR_OUT;
extern int OBD_DISABLE;

int OBD_INVALID_COUNT = 0;

void VManage::OBDLoop() {

#ifndef OBDII
  return; // for now do nothing.
#endif

  double RPM, Velocity;

  while( !ShuttingDown() ) {

    int now_fail_count = 0;

    // Initialize OBD
    OBD_Init(d.OBDPort);

    while( now_fail_count < 10 ) {

      usleep( 100000 );
      
      RPM = Get_RPM();
      Velocity = Get_Speed();

      if( RPM >= 0.0 && Velocity >= 0.0 ) {
     
	// update the datum
	Lock mylock( d.VMDatumMutex );
	
	d.s.obd.Velocity = Velocity;
	d.s.obd.RPM      = RPM;
	d.s.obd.OBDUpdateCounter ++;
	d.s.obd.valid = true
;	d.OBDLastUpdate = TVNow();

	now_fail_count = 0;
      } else {
	now_fail_count ++;
	if (OBD_DISABLE) {
	  d.s.obd.valid = true;
	  d.OBDLastUpdate = TVNow();
	}
      }
    }

    // close the obd port and try again
    OBD_Close();
    sleep(5);
  }
}


