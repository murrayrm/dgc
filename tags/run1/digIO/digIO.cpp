/*                  digIO.cpp
    Driver for accessing digital I/O ports
    Note:  This source file must be compiled with at least
            one level of optimization.
    Ex: g++ -O1 digIO.cpp

Revision History:
2003-sep-15 Alan Somers     Fixed mask bug, changed structs
2003-sep-11 Alan Somers     Bug Fixes
2003-aug-22 Alan Somers     Created
*/

#include <sys/io.h>
#include <iostream>
#include "digIO/digIO.h"

using namespace std;

struct portname TME_IN_PORT={addr: 0x139};
struct portname TME_OUT_PORT={addr: 0x138};
 
int digio_open(portname port){
    if (ioperm(port.addr, 1, 1)) {
        perror("digio_open: error"); 
        exit(1);}
    return(0);}

int digio_close(portname port){
    if (ioperm(port.addr, 1, 0)) {
        perror("digio_close: error"); 
        exit(1);}
    return(0);}

/*digio_write write to some of the pins on the specified port.  Only those 
pins which are set to 1 in mask are written.  Thus, the pins should be named
1, 2, 4, 8, 16, 32, 64, 128 NOT 0,1,2,3,4,5,6,7.  This way you can write
multiple pins at once by using a mask of "pinA || pinB..."*/

int digio_write(unsigned char data, unsigned char mask, portname port){
    int newval = (data & mask) | (port.oldval & (mask ^ 0xFF));
    /*cout << "oldval:\t" << int(oldval[port]) << "oldval & mask:\t";
    cout << int(oldval[port] & !mask) << "\t" << endl;
    cout << "newval:\t" << newval << endl; */
    outb(newval, port.addr);
    port.oldval = newval;}

/*digio_read read a byte from the specified port.  It should take at least
a microsecond.  The calling process must have read permissions from the port.*/
unsigned char digio_read(portname port){
    return(inb(port.addr));}
