// Cruise Controller
// version 1.1
// 11/13/03 H Huang
// File creation
// 11/14/03 H Huang
// Added lookup control

#include "LookUpTable.h"
#include "PermQueue.h"

// External Functions used by car
// Continously changing throttle cruise controller
double Integral_Control(double vcurrent, double vdesired, double current_throttle);

// cruise controller initialization
void cruise_init();

// Cruise control using lookup tables of desired forces
double accel_lookup_control(double vcurrent, double vdesired);



// Internal functions used by Cruise Control

// Acceleration lookup function
double Accel_Lookup(double v_steady);

// Throttle lookup function
double Throttle_Lookup(double acceleration);


// Throttle lookup function from the control signal
double Ctr_Throttle_Lookup(double control);

// Throttle lookup function from a velocity
double Vref_Throttle_Lookup(double velRef);

// generates change in throttle setting for
double delta_throttle(double vcurrent, double vdesired);

