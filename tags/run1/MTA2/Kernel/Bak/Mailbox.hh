#ifndef Mailbox_HH
#define Mailbox_HH

#include "Misc/Mail/Mail.hh"

class Mailbox {
public:
  Mailbox(int MailboxNum);
  ~Mailbox();

  Mail Pop();
  void Push(Mail& msg);

  void MailboxNumber
  
private:
  list<Mail> Q;
  int M_MailboxNumber;
  boost::recursive_mutex MailboxLock;
};


#endif
