
#include "rollover_functions.h"
#include "yaw_pitch_roll_2_matrix.h"
#include "vehicle_constants.h"
#include <math.h>

#define GRAVITY     9.8     // [m/s^2]

// steering_angle_2_arc_radius can return a positive or a negative number
// it is positive if the steering angle is positive (to the right) and 
// negative if steering angle is negative.
// instead of returning infinity for straight ahead, it returns zero
double steering_angle_2_arc_radius( double s_angle )
{
    // NOTE : requirement: for steering angle of 0 (moving straight ahead,
    //          instead of returning an infinite arc radius,
    //          return a value of ZERO

    if (fabs(s_angle) < EPSILON )
        return 0;
    else
    {
        return VEHICLE_AXLE_DISTANCE / tan(s_angle);
    }

    return 0;

}

void gravity_components_in_vehicle_ref_frame(const double yaw, const double pitch, const double roll, 
        double *g_z, double *g_y, double *g_x)
{
    /* Given yaw, pitch, roll, figure out vertical and lateral components of gravity in the 
     * vehicle reference frame */

    double x[3], y[3], z[3];
    yaw_pitch_roll_2_matrix(yaw, pitch, roll, x, y, z);
    /* x, y, z define the rotation matrix from vehicle ref frame to inertial ref frame;
     * R_vw = [x y z], v = vehicle, w = world (inertial)
     *
     * so R_wv = R_vw' = [x'; y'; z']
     *
     * gravity vector in inertial ref frame is 
     * 
     *  g_w = [0; 0; 1] * GRAVITY
     *  
     * so gravity vector in vehicle ref frame is
     *
     *  g_v = R_wv g_w = R_vw' g_w = GRAVITY * [x[2]; y[2]; z[2]]
     */

    *g_z = GRAVITY * z[2]; // vertical (in vehicle ref frame) component of gravity
    *g_y = GRAVITY * y[2]; // lateral  (in vehicle ref frame) component of gravity
    *g_x = GRAVITY * x[2]; // forward  (in vehicle ref frame) component of gravity
}



void min_max_lateral_acceleration_for_rollover_safety(const double g_z, const double g_y, const double T_roll,
        double *a_R, double *a_L)
{
    /* Given rollover safety threshold compute min and max lateral accelerations */

    /* Inputs:
     *  g_z : [m/s^2] downward component of gravity (in vehicle ref frame)
     *  g_y : [m/s^2] lateral  component of gravity (in vehicle ref frame)
     *  T_roll: [unitless] rollover safety threshold
     *
     *  Outputs:
     *  a_R : [m/s^2] acceleration for which rollover safety threshold is achieved for a rightward roll
     *  a_L : [m/s^2] acceleration for which rollover safety threshold is achieved for a leftward roll
     *
     *  NOTE: the safe lateral accelerations, a_safe are in the range
     *          a_R <= a_safe <= a_L
     */

    *a_L = ((VEHICLE_L+VEHICLE_R)/(2*VEHICLE_H)*(1+T_roll) - VEHICLE_R/VEHICLE_H)*g_z + g_y;
    // a_L is maximum acceleration permissible before leftward roll exceeds rollover threshold
    
    *a_R = ((VEHICLE_L+VEHICLE_R)/(2*VEHICLE_H)*(1-T_roll) - VEHICLE_R/VEHICLE_H)*g_z + g_y;
    // a_R is minimum acceleration permissible before rightward roll exceeds rollover threshold
}


double rollover_coefficient(const double a_T, const double g_z, const double g_y)
{
    // given the current lateral acceleration (derived from current velocity and arc radius),
    // compute the rollover coefficient

    /* 
     * Inputs:
         *  a_T : [m/s^2] lateral acceleration of vehicle due to following specified arc path at specified velocity
         *  g_z : [m/s^2] downward component of gravity (in vehicle ref frame)
         *  g_y : [m/s^2] lateral  component of gravity (in vehicle ref frame)
         *
     * Outputs:
         *  c_roll : [unitless] rollover safety coefficient.
         *                  c_roll = 
         *                      0 if vehicle is perfectly stable
         *                      magnitude 1 if on the verge of rolling over
         *                      magnitue > 1 if there is a rollover torque present
         *                      c_roll < 0 if leaning to the right
         *                      c_roll > 0 if leaning to the left
         *                      so c_roll < -1 => rolling rightward
         *
     */

    return ((VEHICLE_L-VEHICLE_R)*g_z + 2*VEHICLE_H*(a_T - g_y)) / ((VEHICLE_L+VEHICLE_R)*g_z);
}



void min_max_velocity_for_rollover_safety(const double a_R, const double a_L, const double arc_radius, 
        double *min_v, double *max_v)
{

    // Given the current arc and the lateral acceleration limits to stay within some rollover safety threshold,
    // compute the corresponding min and max forward velocities

    /* 
     * Inputs:
         *  a_R : [m/s^2] acceleration for which rollover safety threshold is achieved for a rightward roll
         *  a_L : [m/s^2] acceleration for which rollover safety threshold is achieved for a leftward roll
         *  arc_radius : [m] the radius of the arc being considered
         *                  NOTE: arc_radius == 0 signifies moving straight ahead
         *                          (more simple representation than arc_radius = infinity)
         *
     * Outputs:
         *  min_v : [m/s] minimum forward velocity to be within the rollover safety threshold
         *  max_v : [m/s] maximum forward velocity to be within the rollover safety threshold
     */

    double v_squared;

    if (arc_radius ==0)
    {
        // moving straight ahead, we can have any forward velocity
        // and not risk rollover
        // UNLESS one of the max accelerations is actually ZERO
        // TODO: may want to replace the return value with something that
        //          signals "no limit"

        // NOTE: TODO: ??? will this make moving straight ahead look too good on a dangerous slope ???
        if (fabs(a_R)<1e-3 | fabs(a_L)<1e-3)
        {
            *min_v = 0;
            *max_v = 0;
        }
        else
        {
            *min_v = 0;
            *max_v = VEHICLE_MAX_V; // NOTE: TODO: ??? will this make moving straight ahead look too good on a dangerous slope ???
        }
    }
    else
    {
        if (arc_radius > 0)
        {
            // min velocity
            v_squared = a_R * arc_radius;
            if (v_squared < 0)
                *min_v = 0;
            else
                *min_v = sqrt(v_squared);
            
            // max velocity
            v_squared = a_L * arc_radius;
            if (v_squared < 0)
                *max_v = 0;
            else
                *max_v = sqrt(v_squared);
        }
        else
        {
            // min velocity
            v_squared = a_L * arc_radius;
            if (v_squared < 0)
                *min_v = 0;
            else
                *min_v = sqrt(v_squared);

            // max velocity
            v_squared = a_R * arc_radius;
            if (v_squared < 0)
                *max_v = 0;
            else
                *max_v = sqrt(v_squared);

        }
    }
}
