#include <stdio.h>

#include <fstream>
#include <sstream>
#include <iostream>
#include <string>

#include "serial.h"

#define COM_PORT        0
#define BUFF_LEN        1000
#define ERROR   -1

using namespace std;

int main(int argc, char* argv[]) 
{
    string outString;
    char inputBuff[BUFF_LEN];
    int expectedLength, readLength, readUntil;
    double timeout;

    cout << "Trying to initialize the serial port"  << endl;
    
    // Open the serial port or die trying.
    if (serial_open_advanced(
                COM_PORT, 
                B9600, 
                (SR_PARITY_DISABLE | SR_SOFT_FLOW_INPUT | SR_SOFT_FLOW_OUTPUT | SR_ONE_STOP_BIT)
                ) != 0) 
    {
        fprintf(stderr, "[ERROR] %s (%d): Cannot open Serial Port (%d)!\n",
                __FILE__, __LINE__, COM_PORT);
    }

    cout << "Serial port opened with 9600bps, Xon/Xoff, 8 data bits, no flow control, one parity" << endl;

    while ( 1 ) 
    {
        cout << "What should the timeout be (in seconds?)\n";
        cin >> timeout;

        cout << "\nGive the number of characters you expect: \n";
        cin >> expectedLength;
        if (expectedLength > BUFF_LEN) {
            expectedLength = BUFF_LEN;
            cout << "Sorry, can't read that much, reading only " << expectedLength << " bytes\n";
        }

        cout << "Give the ASCII number for the character you want to read \nuntil (in decimal) or type a number less than 0 for regular \nserial_blocking with no until.\n\n";
        cin >> readUntil;
        cout << (char) readUntil << (unsigned char) readUntil << (signed char) readUntil << endl;

        if ( readUntil > 0 && readUntil < 128) {
            // Use serial_read_until()
            cout << "You are using UNTIL.\n\n"; fflush(stdout);
            readLength = serial_read_until(COM_PORT, inputBuff, (char) readUntil, timeout, expectedLength);
        } else {
            readLength = serial_read_blocking(COM_PORT, inputBuff, expectedLength, timeout);
        }

        cout << readLength << " characters read:\n";

        for (int i = 0; i < readLength; i++) {
            cout << inputBuff[i];
        }
        cout << endl;

        if (serial_write (
                    COM_PORT, 
                    (char *) inputBuff, 
                    readLength, 
                    SerialBlock
                    ) 
                != (int) readLength)
        {
            fprintf(stderr, "[ERROR] %s (%d): Incomplete serial port write!\n",
                    __FILE__, __LINE__);
        }
 
        if (readLength != expectedLength) {
            cout << " !!! We must have timed out since the expected length ("<< expectedLength <<") is different from that received!\n";
        }

    }

    return 0;
}
