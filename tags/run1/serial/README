**************
*SERIAL PORTS*
**************

FUNCTION
---------

In the serial directory, there are test1.c,  test2.c, test12.c, serial.c
serial.h, and makefile that are used for serial ports. All the test*.c 
files are for testing. You don't need 
them when you use serial port for your application. makefile is also used to
compile those test programs. Do "make" to compile test1.c test2.c and test12.c 
into executable files. When you use serial port, you only need serial.c
and serial.h

Before you use the serial port, you need to call serial_open with a com number
from 0 to 9 (on the linux machine I worked on, it only has ttyS0-ttyS9), and
baud rate. After calling serial_open, 
you can use serial_read and serial_write to read and write. serial_read
is a nonblocking function, i.e. it will return right away even if there is 
nothing to read. However, you have to specify whether you want serial_write
to be blocking or nonblocking when you call the function. If you call 
blocking serial_write, it won't return unless all the data passed to the
function has been written to the serial port. If you call nonblocking
serial_write, the function will return right away if the serial port is busy,
and a write cannot be performed.


When you are done, call serial_close to close the serial port.


HOW IT WORKS
------------
There is a buffer of dynamic size for reading. When the serial port receives 
something, it will send a signal to our process, and the signal handler 
serial_input_handler(int signal) will be called to read from serial port
and save data in a buffer. Since we read from the serial port whenever
data is ready, even if we use blocking serial write on the other end of the 
serial port, it won't take too much time. serial_read reads from the buffer
so it is a nonblocking call.

You can test the program with test1 running on a PC and test2 running on the
other PC with a serial connection as shown below.



HOW TO CONNECT TWO PCs WITH SERIAL PORTS
----------------------------------------
I tested my code with two PCs hooked up with a serial cable.

7 is connected to 8 on the other side
1 is connected to 6 on the same side, then 6 is connected to 4 on the other side

RTS 7-----\/-----7 RTS	
CTS 8-----/\-----8 CTS
TXD 3-----\/-----3 TXD
RXD 2-----/\-----2 RXD
DTR 4-----\/-----4 DTR
DSR 6-|---/\---|-6 DSR
DCD 1-|        |-6 DCD
GND 5------------5 GND
 



**************
*MAGNETOMETER*
**************

FUNCTION
---------

See mtest.c and mm_calib.c for magnetometer examples.

APIs are in magnetometer.c and magnetometer.h.

Call mm_init(int com) with a com port number to initialize the com port.
mm_init will also set up some initial paremeters for TCM2 modules, such
as the units of measurement. All angles are measured in degrees, and
temperature is measured in Celcius. Call mm_uninit(void) when you are
done.

After TCM2 is initialized, you can call mm_send_command to take measurements.

/* There are 7 commands available.
 * MM_COMPASS_UP:  compass update
 * MM_MM_UP: 	   magnetometer update
 * MM_IN_UP: 	   inclinometer update
 * MM_TEMP_UP: 	   temperature update
 * MM_OW_UP: 	   outpute word update
 * MM_LC_SC: 	   last calibration score
 * MM_CR_CA:	   clear calibration data
 * MM_EMP_CA:	   enable multipoint calibration
 * MM_DMP_CA:	   disable multipoint calibration
 * MM_GO:	   enter continuous sampling mode
 * MM_HALT: 	   stop continous sampling
 * MM_WARM_REBOOT: warm reboot
 */

Pass necessary memory space to mm_send_command so that it can fill it with
measurements.

mm_calibration(void) can be called to calibrate TCM2. It will compensate for
ambient magnetic field after calibration. Turn the module several times during
the calibration process. You have about 25 seconds to do it after calling the
function. The results of the calibration will be shown on the screen.



Any suggestion for improvement?? Email Johnson at chihhao@its.caltech.edu
