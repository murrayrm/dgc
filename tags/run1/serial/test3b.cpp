#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream.h>
#include "serial.h"

int main(int argc, char **argv) {
  int serial_num=0;
  char out_buffer[256];
  char *buffer_ptr;
  buffer_ptr=out_buffer;

  for(int i=0; i<argc; i++) {
    if(strncmp(argv[i], "-com=", 5)==0) sscanf(argv[i], "-com=%d", &serial_num);
  }

  if(serial_open(serial_num, B57600) != -1) {
    //Fill out_buffer with all the possible char byte values
    for(int j=0; j<256; j++) {
      out_buffer[j]=(unsigned char)j;
    }

    //Write them in 32 byte blocks, waiting 1 second in between (so as to test blocking_read)
    for(int k=0; k<256; k+=32) {
      serial_write(serial_num, buffer_ptr+k, 32, SerialBlock);
      cout << "\nWrote bytes " << k << " to " << k+31;
      sleep(1);
    }    
    serial_close(serial_num);

    cout << "\nOutput successful.\n";
  } else {
    cout << "\nError opening com " << serial_num << ".\n";
  }

  return 0;
}
