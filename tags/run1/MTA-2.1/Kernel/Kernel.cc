#include "Kernel.hh"

#include "Misc/Thread/ThreadsForClasses.hpp"
#include "Misc/Thread/ThreadsForFunctions.hh"
#include "Misc/Thread/ThreadManager.hh"


#include <iostream>
using namespace std;
using namespace boost;

Kernel K;

int MTA_KERNEL_COUT = 0;

boost::thread_group KD_ThreadGroup;

Kernel::Kernel(){
  M_ShutdownFlag = false;
  M_LocalModulesCounter = 0;

}

Kernel::~Kernel() {
}


void Register(shared_ptr<DGC_MODULE> dm) {
  recursive_mutex::scoped_lock s(K.M_LocalModulesLock);
  K.M_LocalModulesCounter ++;
  (*dm).M_ModuleNumber = K.M_LocalModulesCounter;
  K.M_LocalModules.push_back(dm);
}

void Unregister(int Mailbox) {
  recursive_mutex::scoped_lock s(K.M_LocalModulesLock);
  list<shared_ptr<DGC_MODULE> >::iterator x =    K.M_LocalModules.begin();
  list<shared_ptr<DGC_MODULE> >::iterator stop = K.M_LocalModules.end();
  while( x != stop ) {
    if( (*(*x)).M_ModuleNumber == Mailbox ) {
      (*(*x)).ForceShutdown();
      K.M_LocalModules.erase(x);
    }
    x++;
  }
}

void catch_signals(int sig) {

  if( MTA_KERNEL_COUT ) {
    cout << "Kernel Signal Handler Received: ";
    switch( sig ) 
      {
      case SIGUSR1: cout << "SIGUSR1"; break;
      case SIGALRM: cout << "SIGALRM"; break;
      case SIGPIPE: cout << "SIGPIPE"; break;
      default:      cout << sig;
      }
    
    cout << endl;   
  }

  signal(sig, &catch_signals);
}

void StartKernel(int mtacouton) {

  // Do we want to print all of our cout messages?
  MTA_KERNEL_COUT = mtacouton;

  // User signals that we may throw.
  signal(SIGUSR1, &catch_signals);
  signal(SIGALRM, &catch_signals);
  signal(SIGPIPE, &catch_signals);

  Thread::MaintainThreadCount(10);


  cout << "MTA - STARTING KERNEL, MTA RECEIVE KEY = " << DEF::ReceiveKey << endl;


  int ret =  K.Bootstrap();
  //  cout << "Finished Bootstrapping" << endl;
  if( ret != true ) {
    if( MTA_KERNEL_COUT ) cout << "Kernel Boostrap Failed, Shutting Down" << endl;
    //    K.ForceKernelShutdown();
  } else {
    
    cout << "MTA BOOTSTRAP SUCCESSFUL -- Found Modules:" << endl;
    PrintModuleList( AllModules() );

    sleep(2);

    // run stuff

    // Setup catching of signals

    // cout << "Kernel Starting " << endl;

    // spawn threads

    if( MTA_KERNEL_COUT ) cout << "Kernel Starting Threads" << endl;
    RunMethodInNewThread<Kernel>(&K, &Kernel::ReceiveMailThread);
    RunMethodInNewThread<Kernel>(&K, &Kernel::SendMailThread);
    RunMethodInNewThread<Kernel>(&K, &Kernel::SystemMapThread);
    RunMethodInNewThread<Kernel>(&K, &Kernel::TimeSyncThread);
  
    // sleep for a second before starting the modules
    sleep(1);
    // now start all modules.
    if( true ) {  
      recursive_mutex::scoped_lock s(K.M_LocalModulesLock);
      list<shared_ptr<DGC_MODULE> >::iterator x    = K.M_LocalModules.begin();
      list<shared_ptr<DGC_MODULE> >::iterator stop = K.M_LocalModules.end();
      while( x != stop ) {
	if( MTA_KERNEL_COUT ) cout << "Starting " << (*(*x)).ModuleName() << endl;
	RunMethodInNewThread<DGC_MODULE>( &(*(*x)), &DGC_MODULE::operator());
	x++;
      }
    }

    /// Temporary Hack - get rid of later
    while( !ShuttingDown() ) {
      //cout << "Kernel is not shutting down" << endl;
      sleep(1);
    }
    if( MTA_KERNEL_COUT ) cout << "Kernel shutting down in 3 seconds" << endl;
    sleep(3);
    exit(EXIT_SUCCESS);

    // JOIN ALL THREADS
    // This will ensure we wait here until all threads
    // have terminated.
    Thread::Join();
  }  
}


void ForceKernelShutdown() {

  cout << "ForceKernelShutdown Called" << endl << endl << endl;
  // go down the list and shutdown all local modules
  recursive_mutex::scoped_lock s(K.M_LocalModulesLock);
  list<shared_ptr<DGC_MODULE> >::iterator x =    K.M_LocalModules.begin();
  list<shared_ptr<DGC_MODULE> >::iterator stop = K.M_LocalModules.end();
  while( x != stop ) {
    Unregister( (*(*x)).ModuleNumber() );
    x++;
  }

  // then flag for Kernel shutdown
  K.M_ShutdownFlag = true;
  K.M_ActiveFlag = false;

  // Kill all threads
  Thread::ForceShutdown();
  

  // Once we are ready, we must release the server 
  // socket by issuing a local shutdown command.
  
  //  ...code goes here...
}

int ShuttingDown() {
  return K.M_ShutdownFlag;
}


int Kernel::Bootstrap() {
  if( MTA_KERNEL_COUT ) cout << "Kernel Boostrapping" << endl;
  if( ReceiveMailInit() ) {
    if( MTA_KERNEL_COUT ) cout << "ReceiveMailInit() Finished" << endl;
    if( SendMailInit() ) {
      if( MTA_KERNEL_COUT ) cout << "SendMailInit() Finished" << endl;
      if( SystemMapInit("") ) {
	if( MTA_KERNEL_COUT ) cout << "SystemMapInit() Finished" << endl;
	if( TimeSyncInit() ) {
	  if( MTA_KERNEL_COUT ) cout << "TimeSyncInit() Finished" << endl;
	  return true;
	}
      }
    }
  }
  
  return false;
}


Mail Kernel::KernelQueryMailHandler(Mail& ml) {
  //  cout << "Query Mail Arrived of type " << ml.MsgType() << " for the kernel." << endl;
  int size = ml.Size();

  switch( ml.MsgType() ) {

  case KernelMessages::S_ECHO: 
    // download the message, and send it back
    if (true) {
      char *p = new char[ size ];
      ml.DequeueFrom( (void*) p, size );
      Mail reply = Mail(ml.To(), ml.From(), ml.MsgType(), MailFlags::QueryMessage); // reverse
      reply.QueueTo((void*) p, size);
      delete p;
      return reply;
    }
    break;

  case KernelMessages::MODULEINFO:
    // nothing to download just return
    if( true ) {
      list<ModuleInfo> r = LocalModules();
      list<ModuleInfo>::iterator x = r.begin();
      list<ModuleInfo>::iterator stop = r.end();
      Mail reply = Mail(ml.To(), ml.From(), ml.MsgType(), MailFlags::QueryMessage); // reverse
      while( x != stop) {
	reply.QueueTo( (void*) &(*x), sizeof(ModuleInfo));
	x++;
      }
      return reply;
    }
    break;

  default:
    if( MTA_KERNEL_COUT ) cout << "Kernel: Unknown Query Message Received" << endl;
  }

  return Mail(ml.To(), ml.From(), 1, MailFlags::BadMessage); //  reverse
}

void Kernel::KernelInMailHandler(Mail& ml) {
  //  cout << "In Mail Arrived of type = " << ml.MsgType() << endl;
  int size = ml.Size();

  switch( ml.MsgType() ) {
  case KernelMessages::S_ECHO:
    // download the message, and send it back
    if (true) {
      char *p = new char[ size ];
      ml.DequeueFrom( (void*) p, size );
      Mail reply = Mail(ml.To(), ml.From(), ml.MsgType(), 0); // reverse
      reply.QueueTo((void*) p, size);
      delete p;
      SendMail(reply);
    }
  case KernelMessages::SHUTDOWN: 
    if( MTA_KERNEL_COUT ) cout << "Shutdown Message Received" << endl;
    ForceKernelShutdown();
    break;

  default:
    if( MTA_KERNEL_COUT ) cout << "Kernel: Unknown In Message Received" << endl;
  }

}


