#ifndef Modules_HH
#define Modules_HH

// Add your module to the bottom of this list,
// above the place holder.

namespace MODULES {
  enum {
    EchoServer,               // Echo server will return any MTA message that was sent to it.

    Clock,              // Master clock. All modules automatically syncronize their clocks with this module.

    Passive,            // A module that only sends query messages and does not need to receive any messages 
                        // may be considered passive

    SystemMap,          // Serves a text file that tells the kernel which IP addresses correspond to 
                        // active computers that may be running modules.  The kernel will then port scan
                        // these IP addresses to find modules.

    StateEstimator,     // Runs GPS, IMU, OBD-II -- serves state data via query message.
    
    Actuators,          // Runs steering, gas and brake -- accepts velocity and steering angle.  Contains
                        // cruise controller to try to set speed appropriately.



    TestModule,         // Just a test module.  Not important to anyone but Ike.


    VDrive,
    VState,
    SimVDrive,

    WaypointNav,

    Arbiter,
    StereoPlanner,
    GlobalPlanner,
    CorridorArcEvaluator,    // New module for genMaps/corridor following
    LADARBumper,             // This is the simple ladar bumper code
    LADARMapper,             // This ladar module does the terrain mapping
    DFEPlanner,
    RoadFollower,
    PathEvaluation, // Code to test the genMap
    FenderPlanner, // New module for using a priori data

    StereoImageLogger,

    MatlabDisplay,

    /* Commands added by RMM for vehlib-2.0 functionality */
    VTest,			// Test program for command vdrive
    VRemote,			// Remote control of vdrive/vstate

    VManage,

    // BEGIN --- PLACE HOLDER ---- PLACE HOLDER
    // ADD NEW MODULES ABOVE THIS LINE (ABOVE THE PLACE HOLDER)
    // AND MAKE SURE TO PLACE A COMMA AFTER ITS NAME.
    NumberOfModules
    // END   --- PLACE HOLDER ---- PLACE HOLDER
  };
};

#endif
