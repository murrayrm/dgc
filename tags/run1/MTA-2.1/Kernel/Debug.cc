#include "Kernel.hh"

void PrintModuleList(list<ModuleInfo> l) {
  
  list<ModuleInfo>::iterator x    = l.begin();
  list<ModuleInfo>::iterator stop = l.end();
  
  cout << endl;
  cout << "Listing Module Set..." << endl;
  while ( x != stop) {
    ModuleInfo mi = (*x);
    cout << "IP["    
	 << (int) * ( ((unsigned char*) & mi.MA.TCP.IP) + 0) << "."
	 << (int) * ( ((unsigned char*) & mi.MA.TCP.IP) + 1) << "."
	 << (int) * ( ((unsigned char*) & mi.MA.TCP.IP) + 2) << "."
      	 << (int) * ( ((unsigned char*) & mi.MA.TCP.IP) + 3) << "] "
	 << "PORT["  << mi.MA.TCP.PORT     << "] "
	 << "MBOX["  << mi.MA.MAILBOX      << "] "
	 << "TYPE["  << mi.ModuleType      << "]." << endl;
    x++;
  }
  cout << endl;
}
