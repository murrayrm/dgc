#include "ThreadManager.hh"
#include "ThreadsForClasses.hpp"
#include "ThreadsForFunctions.hh"

#include <unistd.h>
#include <string>
#include <iostream>

using namespace std;
using namespace Thread;

class TestClass {
public:
  TestClass();
  TestClass(const TestClass& rhs);
  void Func();
  ~TestClass();
};

TestClass::TestClass() {

}

TestClass::TestClass(const TestClass& rhs) {
}

void TestClass::Func() {
  cout << "Test Class called successfully" << endl;
}

TestClass::~TestClass() {
  sleep(5);
  cout << "Test Class Destructor Called" << endl;

}

void TestFunction(void*f) {

  cout << "Test Function called successfully" << endl;
  sleep(3);

  ForceShutdown();
}


int main () {

  TestClass a;

  RunMethodInNewThread<TestClass> ( &a, &TestClass::Func);

  CopyObjectAndRunMethodInNewThread<TestClass>( &a, &TestClass::Func);

  RunFunctionInNewThread( &TestFunction, NULL);

  cout << "Joining Threads" << endl;
  Join();

  cout << "Main Function Shutting Down" << endl;
  return 0;
}
