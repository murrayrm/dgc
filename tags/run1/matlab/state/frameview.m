
% frameview
% takes in frames.dat data and plots the xyz coordinates as a 3d plot

load frames.dat
x = frames(:,4);
y = frames(:,5);
z = frames(:,6);
plot3(x, y, z, '.')
grid on
xlabel('X Axis');
ylabel('Y Axis');
zlabel('Z Axis');