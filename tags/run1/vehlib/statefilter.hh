
void imu_update(rot_matrix &nb, IMU_DATA imu, VState_GetStateMsg &vstate);
void gps_update(gpsDataWrapper &gps, VState_GetStateMsg &vstate, rot_matrix &nb);
void mag_update(mag_data mag, VState_GetStateMsg &vstate, rot_matrix &nb);
