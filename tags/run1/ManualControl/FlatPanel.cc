
#include "Msg/dgc_mta.hh"
#include "Msg/dgc_msg.h"
#include "Msg/dgc_socket.h"
#include "Msg/dgc_messages.h"
#include "Msg/dgc_const.h"
#include "Msg/dgc_mta_main.h"
#include "state/state.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
 

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <iostream>
#include <iomanip>
#include <list>
#include <time.h>
#include <fstream>


struct steeringAndAccel {
  float steering;
  float accel;
  float dist;
};

int shutdown_flag;
using namespace std;

int main(int argc, char* argv[])
{
  state_struct mySS;
  int stopped = TRUE;

  shutdown_flag = FALSE;
  dgc_msg_Register(dgcs_FlatPanel);
  cout << endl << endl << "Registering at time t=" 
       << setprecision(10) << SystemTime() << endl;
  dgc_MsgHeader myMH;

  steeringAndAccel mySAS;

  while ( shutdown_flag == FALSE ) { 
    
    if( dgc_PollMessage(myMH) == TRUE) {
	dgc_GetMessage(myMH, &mySAS);

	if( mySAS.accel > 0.0 && mySAS.dist > 0.0) {
	  if( stopped == TRUE ) {
	    cout << "Let go of brake" << endl;
	    stopped = FALSE;
	  }
	  cout << "Accelerating" << endl;
	} else if ( -1.0 < mySAS.accel && mySAS.accel < 0.0 && mySAS.dist > 0.0) {
	  cout << endl << "Stop in   " << setiosflags(ios::fixed) << setprecision(2) << " meters." << endl;
	} else if ( mySAS.accel < -1.0 && mySAS.dist > 0.0) {
	  cout << endl << "STOP STOP STOP - wait for release signal" << endl;
	  stopped = TRUE;
	} else if ( mySAS.dist < 0.0 ) {
	  cout << "Test run is over.  Override steering using joystick buttons (hold down both right buttons)" << endl;
	  shutdown_flag = TRUE;
	}
    }
  }
  cout << endl << endl << "Unregistering" << endl;
  dgc_msg_Unregister();
    
  return 0;
}














