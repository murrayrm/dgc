//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_IplImage.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// To use the IntelR Image Processing Library to process video images (in a DIB format) being capturing in real time and avoid copying the image data when converting the data into an Intel Image Processing Library image, please note the following: 
// 
// Intel Image Processing Library images are a superset of DIB and so you may use the data directly with one caveat: for efficiency, Intel Image Processing Library images should be 8 byte aligned. A simple scheme for getting 8 byte aligned data areas is as follows (for RGB images): 
// unsigned char Image[640*480*3 + 8]; //Allow 8 bytes slop for alignment
// unsigned char *pI = Image + ((8 - (int)Image%8)%8); //Aligned image data pointer
// //Make an IPL header for the above int width = 640, height = 480;
// IplImage IPL;
// IPL.nSize = sizeof(IplImage);
// IPL.nChannels = 3;
// IPL.depth = IPL_DEPTH_8U;
// IPL.colorModel[0] = 'R';
// IPL.colorModel[1] = 'G';
// IPL.colorModel[2] = 'B';
// IPL.colorModel[3] = 0;
// IPL.channelSeq[0] = 'B';
// IPL.channelSeq[1] = 'G';
// IPL.channelSeq[2] = 'R';
// IPL.channelSeq[3] = 0;
// IPL.dataOrder = IPL_DATA_ORDER_PIXEL;
// IPL.origin = IPL_ORIGIN_BL;
// IPL.align = IPL_ALIGN_QWORD;
// IPL.height = height;
// IPL.width = width;
// IPL.roi = NULL;
// IPL.tileInfo = NULL;
// int twidth = width*3;
// IPL.imageSize = height*twidth;
// IPL.widthStep = twidth + ((8 - (twidth%8))%8);
// IPL.imageData = pI;

//----------------------------------------------------------------------------

 double CR_myround(double x)
 {
   if (x - floor(x) >= 0.5)
     return ceil(x);
   else
     return floor(x);
 }

//----------------------------------------------------------------------------

IplImage *read_IplImage(char *filename)
{
  return 0;
}

//----------------------------------------------------------------------------

void write_IplImage(char *filename, IplImage *im, ...)
{
  
}

//----------------------------------------------------------------------------

char pbm_getc(FILE *file)
{
  register int ich;
  register char ch;
  
  //  ich = getc( file );
  //  if ( ich == EOF )
  //    CR_error( "EOF 1 / read error" );


  do {
    ich = getc( file );
  } while ( ich == EOF );
  ch = (char) ich;
  
  if ( ch == '#' )
    {
      do
	{
	  ich = getc( file );
	  if ( ich == EOF )
	    CR_error( "EOF 2 / read error" );
	  ch = (char) ich;
	}
      while ( ch != '\n' && ch != '\r' );
    }
  
  return ch;
}

//----------------------------------------------------------------------------

int pbm_getint(FILE *file)
{
  register char ch;
  register int i;

    CR_flush_printf("aa\n");
  
  do
    {
      ch = pbm_getc( file );
      printf("%i\n", (int) ch);
      //      ch = (char) getc(file);
    }
  while ( ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r' );

    CR_flush_printf("bb\n");

  /*  
  if ( ch < '0' || ch > '9' )
    CR_error( "junk in file where an integer should be" );
  
  i = 0;
  do
    {
      i = i * 10 + ch - '0';
      ch = pbm_getc( file );
    }
  while ( ch >= '0' && ch <= '9' );
  */

  //  return i;
  return (int) ch;
}

//----------------------------------------------------------------------------

void free_IplImage(IplImage *im)
{
  iplDeallocate(im, IPL_IMAGE_ALL);
}

//----------------------------------------------------------------------------

  // assuming type indicates 24-bit color right now

IplImage *make_IplImage(int width, int height, int type)
{
  IplImage *im;
  int nchannels, depth;
  char colorModel[5], channelSeq[5];

  if (type == IPL_TYPE_COLOR_8U || type == IPL_TYPE_COLOR_8U_YUV) {
    nchannels = 3;
    depth = IPL_DEPTH_8U;
    sprintf(colorModel, "%s\0", "RGB");
    sprintf(channelSeq, "%s\0", "RGB");
  }
  else if (type == IPL_TYPE_GRAY_8U) {
    nchannels = 1;
    depth = IPL_DEPTH_8U;
    sprintf(colorModel, "%s\0", "GRAY");
    sprintf(channelSeq, "%s\0", "G");
  }
  else if (type == IPL_TYPE_COLOR_32F) {
    nchannels = 3;
    depth = IPL_DEPTH_32F;
    sprintf(colorModel, "%s\0", "RGB");
    sprintf(channelSeq, "%s\0", "RGB");
  }
  else if (type == IPL_TYPE_GRAY_32F) {
    nchannels = 1;
    depth = IPL_DEPTH_32F;
    sprintf(colorModel, "%s\0", "GRAY");
    sprintf(channelSeq, "%s\0", "G");
  }

  im = iplCreateImageHeader(nchannels,
			    0,
			    depth,
			    colorModel,
			    channelSeq,     // "BGR" might be better for fast display with DirectX
			    IPL_DATA_ORDER_PIXEL,
			    IPL_ORIGIN_TL,   // IPL_ORIGIN_BL might be better for display with DirectX
			    //			    IPL_ALIGN_QWORD,
			    IPL_ALIGN_DWORD,
			    width, height,
			    NULL,
			    NULL,
			    NULL,
			    NULL);

  if (type == IPL_TYPE_COLOR_8U || type == IPL_TYPE_COLOR_8U_YUV || type == IPL_TYPE_GRAY_8U)
    iplAllocateImage(im, 0, 0);
  else if (type == IPL_TYPE_COLOR_32F || type == IPL_TYPE_GRAY_32F)
    iplAllocateImageFP(im, 0, 0);

  im->roi = iplCreateROI(0, 0, 0, im->width, im->height);

  return im;
}

//----------------------------------------------------------------------------

IplImage *make_FP_IplImage(int width, int height, int type)
{
  IplImage *im;

  im = iplCreateImageHeader(3,
			    0,
			    IPL_DEPTH_32F,
			    "RGB",
			    "RGB",     // "BGR" might be better for fast display with DirectX
			    IPL_DATA_ORDER_PIXEL,
			    IPL_ORIGIN_TL,   // IPL_ORIGIN_BL might be better for display with DirectX
			    //			    IPL_ALIGN_QWORD,
			    IPL_ALIGN_DWORD,
			    width, height,
			    NULL,
			    NULL,
			    NULL,
			    NULL);
  iplAllocateImageFP(im, 0, 0);

  im->roi = iplCreateROI(0, 0, 0, im->width, im->height);

  return im;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void read_PPM_to_IplImage(char *filename, IplImage *im)
{
  int i, j, width, height;
  FILE *fp;
  int r, g, b, iscomment;
  unsigned char u, v, w;
  unsigned char pixel[3];
  char imtype[3];
  char str[80];
  char c;

  //  CR_flush_printf("ip read_PPM_to_IplImage(): trying to open %s\n", filename);

  // get size info

  fp = fopen(filename, "rb");
  if (!fp)
    CR_error("read_PPM_to_IplImage(): no such file");

  fscanf(fp, "%s\n", &imtype);

  // attempt to eat comments

  do {
    iscomment = FALSE;
    c = fgetc(fp);
    ungetc(c, fp);
    if (c == '#') {
      iscomment = TRUE;
      fgets (str, 79, fp);
    }
  } while (iscomment);

  // read image dimensions

  fscanf(fp, "%i %i\n255\n", &width, &height);    

  // image is already allocated

  // actually read it in

  // ascii
  if (!strcmp(imtype, "P3")) {

    for (j = 0; j < im->height; j++) {
      for (i = 0; i < im->width; i++) {
	fscanf(fp, "%i %i %i ", &r, &g, &b);
	pixel[IPL_RED] = (unsigned char) r;
	pixel[IPL_GREEN] = (unsigned char) g;
	pixel[IPL_BLUE] = (unsigned char) b;
	iplPutPixel(im, i, j, pixel);
      }
      fscanf(fp, "\n");
    }
  }

  // binary

  else if (!strcmp(imtype, "P6")) {

    printf("here!\n");

    for (j = 0; j < im->height; j++) {
      for (i = 0; i < im->width; i++) {

	fscanf(fp, "%c%c%c", &r, &g, &b);

	pixel[IPL_RED] = (unsigned char) r;
 	pixel[IPL_GREEN] = (unsigned char) g;
 	pixel[IPL_BLUE] = (unsigned char) b;

	iplPutPixel(im, i, j, pixel);
      }
    }
  }

  // unknown

  else
    CR_error("unrecognized ppm file");

  fclose(fp);
}

//----------------------------------------------------------------------------

IplImage *read_PPM_to_IplImage(char *filename)
{
  int i, j, width, height;
  FILE *fp;
  IplImage *im;
  int r, g, b, iscomment;
  unsigned char u, v, w;
  unsigned char pixel[3];
  char imtype[3];
  char str[80];
  char c;

  //  CR_flush_printf("read_PPM_to_IplImage(): trying to open %s\n", filename);

  // get size info

  //  fp = fopen(filename, "r");
  fp = fopen(filename, "rb");
  if (!fp)
    CR_error("read_PPM_to_IplImage(): no such file");

  fscanf(fp, "%s\n", &imtype);

  // attempt to eat comments

  do {
    iscomment = FALSE;
    c = fgetc(fp);
    ungetc(c, fp);
    if (c == '#') {
      iscomment = TRUE;
      fgets (str, 79, fp);
    }
  } while (iscomment);

    //    do {
    //      fgets (str, 79, fp);
  //  }  while (str[0]=='#');  // skip comments

  // read image dimensions

  fscanf(fp, "%i %i\n255\n", &width, &height);    

  // allocate space

  // make_IplImage here
  im = make_IplImage(width, height, IPL_TYPE_COLOR_8U);

//   printf("--------------------\nin: %s\n--------------------\n", filename);
//   print_IplImage_properties(im);
//   printf("==========================\n");

  // actually read it in

  // ascii
  if (!strcmp(imtype, "P3")) {

    for (j = 0; j < im->height; j++) {
      for (i = 0; i < im->width; i++) {
	fscanf(fp, "%i %i %i ", &r, &g, &b);
	pixel[IPL_RED] = (unsigned char) r;
	pixel[IPL_GREEN] = (unsigned char) g;
	pixel[IPL_BLUE] = (unsigned char) b;
	//	printf("x = %i y = %i: %i %i %i\n", i, j, pixel[IPL_RED], pixel[IPL_GREEN], pixel[IPL_BLUE]);
	iplPutPixel(im, i, j, pixel);
      }
      fscanf(fp, "\n");
    }
  }

  // binary

  else if (!strcmp(imtype, "P6")) {

    //    CR_error("raw ppm reads not yet working");

    //    CR_error("leaving now");

    for (j = 0; j < im->height; j++) {
      for (i = 0; i < im->width; i++) {
	//    for (i = 0; i < im->width; i++) {
	//      for (j = 0; j < im->height; j++) {
	//	fscanf(fp, "%c%c%c", &(pixel[IPL_RED]), &(pixel[IPL_GREEN]), &(pixel[IPL_BLUE]));

	fscanf(fp, "%c%c%c", &r, &g, &b);
// 	r = pbm_getint(fp);
// 	g = pbm_getint(fp);
// 	b = pbm_getint(fp);

	pixel[IPL_RED] = (unsigned char) b;
	pixel[IPL_GREEN] = (unsigned char) r;
	pixel[IPL_BLUE] = (unsigned char) g;
	//	printf("x = %i y = %i: %i %i %i\n", i, j, pixel[IPL_RED], pixel[IPL_GREEN], pixel[IPL_BLUE]);
	//	fscanf(fp, "%c%c%c", &u, &v, &w);
	//	pixel[IPL_RED] = (unsigned char) u;
	//	pixel[IPL_GREEN] = (unsigned char) v;
	//	pixel[IPL_BLUE] = (unsigned char) w;

	iplPutPixel(im, i, j, pixel);
	//      printf("%i %i %i\n", (int) u, (int) v, (int) w);
      }
      //      fscanf(fp, "\n");
    }
  }

  // unknown

  else
    CR_error("unrecognized ppm file");

  fclose(fp);

  // return

  return im;
}

//----------------------------------------------------------------------------

void write_IplImage_to_PPM(char *filename, IplImage *im)
{
  int i, j, t, intensity;
  FILE *fp;
  unsigned char pixel[3];
  float imin, imax;

  //  fprintf(fp, "P3\n%d %d\n255\n", im->width - 1, im->height);

//   if (im->nChannels == 1) 
//     printf("grayscale\n");
//   else
//     printf("color\n");

  // grayscale
  if (im->nChannels == 1) {

    fp = fopen(filename, "w");

    fprintf(fp, "P2\n%d %d\n255\n", im->width, im->height);

    if (im->depth == IPL_DEPTH_32F) {

      iplMinMaxFP(im, &imin, &imax);

      for (j = 0; j < im->height; j++) {
	for (i = 0; i < im->width; i++) {
	  intensity = (int) CR_myround(255 * ((((float *) &(im->imageData[j * im->widthStep + 4 * i]))[0]) - imin) / (imax - imin));
	  //	  fprintf(fp, "%i %i %i ", intensity, intensity, intensity);
	  fprintf(fp, "%i ", intensity);
	}      
	fprintf(fp, "\n");
      }
    }

    else {

      fprintf(fp, "P3\n%d %d\n255\n", im->width, im->height);

      for (j = 0; j < im->height; j++) {
	for (i = 0; i < im->width; i++) {
	  iplGetPixel(im, i, j, pixel);
	  fprintf(fp, "%i %i %i ", (int) pixel[IPL_RED], (int) pixel[IPL_RED], (int) pixel[IPL_RED]);
	}
	fprintf(fp, "\n");
      }
    }

    fclose(fp); 
  }
  // color
  else {

    // write size info


    /*
    fp = fopen(filename, "wb");

    //    fprintf(fp, "P6\n%i %i\n255\n", im->width, im->height);
    fprintf(fp, "P5\n%i %i\n255\n", im->width, im->height);

    // write binary image data

    for (j = 0, t = 0; j < im->height; j++) 
      for (i = 0; i < im->width; i++, t += 3) {
	//	fprintf(fp, "%c%c%c", im[t], im[t+1], im[t+2]);
	iplGetPixel(im, i, j, pixel);
	//	fprintf(fp, "%c%c%c", (unsigned char) pixel[IPL_RED], (unsigned char) pixel[IPL_GREEN], (unsigned char) pixel[IPL_BLUE]);
	fprintf(fp, "%c", (unsigned char) pixel[IPL_RED]);
      }
    */

    fp = fopen(filename, "w");

    fprintf(fp, "P3\n%d %d\n255\n", im->width, im->height);

  
    for (j = 0; j < im->height; j++) {
      //      for (i = 1; i < im->width; i++) {
      for (i = 0; i < im->width; i++) {
	iplGetPixel(im, i, j, pixel);
	fprintf(fp, "%i %i %i ", (int) pixel[IPL_RED], (int) pixel[IPL_GREEN], (int) pixel[IPL_BLUE]);
	// rgb
	// bgr
	// brg
	// rbg
	// gbr
	// grb
	//fprintf(fp, "%i %i %i ", (int) pixel[IPL_BLUE], (int) pixel[IPL_GREEN], (int) pixel[IPL_RED]);
      }
      fprintf(fp, "\n");
    }
  

  // finish up

  fclose(fp);

  }
}

//----------------------------------------------------------------------------

// for now we assume this is a grayscale floating point image

void write_IplImage_to_PFM(char *filename, IplImage *im)
{
  int i, j;
  FILE *fp;
  float intensity;

  fp = fopen(filename, "w");

  fprintf(fp, "PF\n%d %d\n1\n", im->width, im->height);

  if (im->nChannels == 1) {
    if (im->depth == IPL_DEPTH_32F) {

      for (j = 0; j < im->height; j++) {
	for (i = 0; i < im->width; i++) {
	  intensity = ((float *) &(im->imageData[j * im->widthStep + 4 * i]))[0];
	  fprintf(fp, "%f%f%f", intensity, intensity, intensity);
	}
	//	fprintf(fp, "\n");
      }
    }
    else
      CR_error("write_IplImage_to_PFM(): not 32-bit grayscale");
  }
  else
    CR_error("write_IplImage_to_PFM(): not 32-bit grayscale");
  
  fclose(fp); 
}

//----------------------------------------------------------------------------

// Create an IPLImage from a JPEGfile.
// This function creates a flat(not tile-based) IPLImage from a JPEGfile.
// iplDeallocate must be called to free the IplImage structure
// and memory created with this function

IplImage*CreateImageFromJPEG(LPCSTR filename) 
{
  BOOL error=FALSE;
  IJLERR jerr;
  IplImage*aImage=NULL;

  // allocate the JPEG core properties
  JPEG_CORE_PROPERTIES jcprops;
  __try
    {
      jerr=ijlInit(&jcprops);
      if(IJL_OK!=jerr)
	{
	  // can't initialize IJLib...
	  error=TRUE;
	  __leave;
	}
      jcprops.JPGFile=const_cast<LPSTR>(filename);
      // read image parameters: width, height,...
      jerr=ijlRead(&jcprops,IJL_JFILE_READPARAMS);
      if(IJL_OK!=jerr)
	{
	  // can't read JPEG image parameters...
	  error=TRUE;
	  __leave;
	}
      // create the IPLImage header, using a NULL tile info struct.
      aImage=CreateImageHeaderFromIJL(&jcprops);
      if(NULL==aImage)
	{
	  // can't create IPLimage header...
	  error=TRUE;
	  __leave;
	}
      // allocate memory for image
      iplAllocateImage(aImage,0,0);
      if(NULL==aImage->imageData)
	{
	  // can't allocate memory for IPLimage...
	  error=TRUE;
	  __leave;
	}
      // tune JPEG decompressor
      jcprops.DIBBytes =(BYTE*)aImage->imageData;
      jcprops.DIBWidth =jcprops.JPGWidth;
      jcprops.DIBHeight =jcprops.JPGHeight;
      jcprops.DIBChannels=3;
      jcprops.DIBPadBytes=IJL_DIB_PAD_BYTES(jcprops.DIBWidth, jcprops.DIBChannels);
      switch(jcprops.JPGChannels)
	{
	case1:
	  {
	    jcprops.JPGColor=IJL_G;
	    break;
	  }
	case3:
	  {
	    jcprops.JPGColor=IJL_YCBCR;
	    break;
	  }
	default:
	  {
	    jcprops.DIBColor=(IJL_COLOR)IJL_OTHER;
	    jcprops.JPGColor=(IJL_COLOR)IJL_OTHER;
	    break;
	  }
	  
	}

      printf("--------------------\nin: %s\n--------------------\n", filename);
      print_IplImage_properties(aImage);
      printf("==========================\n");
      print_JPEG_properties(&jcprops);

      // read data from the JPEG into the Image
      jerr = ijlRead(&jcprops,IJL_JFILE_READWHOLEIMAGE);
      if(IJL_OK != jerr)
	{
	  // can't read JPEG image data...
	  error = TRUE;
	  __leave;
	}
    }//try
  __finally
    {
      // release the JPEG core properties
      jerr = ijlFree(&jcprops);
      if(IJL_OK != jerr)
	{
	  // can't free IJLib...
	  error = TRUE;
	}
      if(FALSE != error)
	{
	  if(NULL != aImage)
	    {
	      iplDeallocate(aImage,IPL_IMAGE_ALL);
	      aImage = NULL;
	    }
	}
    }
  return aImage;
} // CreateImageFromJPEG()

//----------------------------------------------------------------------------

// Initalize an IPL Image given a JPEG image

IplImage* CreateImageHeaderFromIJL(const JPEG_CORE_PROPERTIES* jcprops)
{
  int channels;
  int alphach;
  char colorModel[4];
  char channelSeq[4];
  IplImage* image;

  switch(jcprops->JPGChannels)
    {
    case 1:
      {
	channels = 3;
	alphach = 0;
	colorModel[0] = channelSeq[2] = 'R';
	colorModel[1] = channelSeq[1] = 'G';
	colorModel[2] = channelSeq[0] = 'B';
	colorModel[3] = channelSeq[3] = '\0';
	break;
      }
    case 3:
      {
	channels = 3;
	alphach = 0;
 	colorModel[0] = channelSeq[2] = 'R';
 	colorModel[1] = channelSeq[1] = 'G';
 	colorModel[2] = channelSeq[0] = 'B';
	// test
// 	colorModel[0] = channelSeq[0] = 'R';
// 	colorModel[1] = channelSeq[2] = 'G';
// 	colorModel[2] = channelSeq[1] = 'B';
// 	
	colorModel[3]=channelSeq[3]='\0';
	break;
      }
    default:
      {
	// number of channels not supported in this samples
	return NULL;
      }
    }//switch

//   image=iplCreateImageHeader(
// 			     channels,
// 			     alphach,
// 			     IPL_DEPTH_8U,
// 			     colorModel,
// 			     channelSeq,
// 			     IPL_DATA_ORDER_PIXEL,
// 			     IPL_ORIGIN_TL,
// 			     IPL_ALIGN_DWORD,
// 			     jcprops->JPGWidth,
// 			     jcprops->JPGHeight,
// 			     NULL,
// 			     NULL,
// 			     NULL,
// 			     tileInfo);

  printf("w = %i, h = %i\n", jcprops->JPGWidth, jcprops->JPGHeight);

  image=iplCreateImageHeader(
			     channels,
			     alphach,
			     IPL_DEPTH_8U,
			     colorModel,
			     channelSeq,
			     IPL_DATA_ORDER_PIXEL,
			     IPL_ORIGIN_TL,
			     //			     IPL_ALIGN_QWORD,
			     IPL_ALIGN_DWORD,
			     jcprops->JPGWidth,
			     jcprops->JPGHeight,
			     NULL,
			     NULL,
			     NULL,
			     NULL);
  return image;
}//CreateImageHeaderFromIJL()

//----------------------------------------------------------------------------

// Write an IPLImage to a JPEG file.
// This function requires an IPLImage that is not tile-based

bool WriteImageToJPEG(IplImage*aImage, LPCSTR filename)
{
  bool bres;
  IJLERR jerr;
  JPEG_CORE_PROPERTIES jcprops;
  bres=true;

  
  __try
    {
      jerr=ijlInit(&jcprops);
      if(IJL_OK!=jerr)
	{
	  // can't initialize IJLib...
	  bres=false;
	  __leave;
	}
      bres=SetJPEGProperties(jcprops,aImage);

      printf("--------------------\nout: %s\n--------------------\n", filename);
      //      jcprops.DIBColor = (IJL_COLOR)IJL_OTHER;
      //      jcprops.JPGColor = (IJL_COLOR)IJL_OTHER;
      print_IplImage_properties(aImage);
      printf("==========================\n");
      print_JPEG_properties(&jcprops);

      if(false==bres)
	{
	  __leave;
	}
      jcprops.JPGFile=const_cast<LPSTR>(filename);
      jerr=ijlWrite(&jcprops,IJL_JFILE_WRITEWHOLEIMAGE);
      if(IJL_OK!=jerr)
	{
	  // can't write JPEGimage...
	  bres=false;
	  __leave;
	}
    }//__try
  __finally
    {
      ijlFree(&jcprops);
    }
  return bres;
}//WriteImageToJPEG()

//----------------------------------------------------------------------------

//Set JPEG properties from IplImage

#define IS_RGB(image) \
(image->colorModel[0]=='R'&& \
image->colorModel[1]=='G'&& \
image->colorModel[2]=='B')

#define IS_GRAY(image) \
(image->colorModel[0]=='G'&& \
image->colorModel[1]=='R'&& \
image->colorModel[2]=='A'&& \
image->colorModel[3]=='Y')

#define IS_SEQUENCE_RGB(image) \
(image->channelSeq[0]=='R'&& \
image->channelSeq[1]=='G'&& \
image->channelSeq[2]=='B')

#define IS_SEQUENCE_BGR(image) \
(image->channelSeq[0]=='B'&& \
image->channelSeq[1]=='G'&& \
image->channelSeq[2]=='R')

//----------------------------------------------------------------------------

bool SetJPEGProperties(JPEG_CORE_PROPERTIES& jcprops, const IplImage *image)
{
  if (IPL_DEPTH_8U!=image->depth) {  //only IPL_DEPTH_8U issupportednow
    return false;
  }
  if (IPL_DATA_ORDER_PIXEL != image->dataOrder) { // only IPL_DATA_ORDER_PIXEL is supported now
    return false;
  }
  if (!IS_RGB(image) && !IS_GRAY(image)) {  // only RGB or GRAY color model supported in this sample
    return false;
  }
  if (image->nChannels != 1 && image->nChannels != 3) { // only 1 or 3 channels supported in this example
    return false;
  }
  jcprops.DIBChannels = image->nChannels;
  // set the color space
  // assume that 1 channel image is GRAY, and
  // 3 channel image is RGB or BGR
  switch(jcprops.DIBChannels)
    {
    case 1:
      printf("1 channel G\n");
      jcprops.DIBColor = IJL_G;
      jcprops.JPGColor = IJL_G;
      jcprops.JPGChannels = 1;
      break;
    case 3:
      if(IS_SEQUENCE_RGB(image))
	{
	  printf("3 channels RGB\n");
	  jcprops.DIBColor = IJL_RGB;
	  jcprops.JPGColor = IJL_YCBCR;
	  jcprops.JPGChannels = 3;
	}
      else if(IS_SEQUENCE_BGR(image))
	{
	  printf("3 channels BGR\n");
	  jcprops.DIBColor = IJL_BGR;
	  jcprops.JPGColor = IJL_YCBCR;
	  jcprops.JPGChannels = 3;
	}
      else
	{
	  printf("3 channels ???\n");
	  jcprops.DIBColor = (IJL_COLOR)IJL_OTHER;
	  jcprops.JPGColor = (IJL_COLOR)IJL_OTHER;
	  jcprops.JPGChannels = 3;
	}
      break;
    default:
      // error for now
      break;
    }

  // my addition
  /*
  jcprops.DIBColor = (IJL_COLOR)IJL_OTHER;
  jcprops.JPGColor = (IJL_COLOR)IJL_OTHER;
  jcprops.JPGChannels = 3;
  */

  jcprops.DIBHeight = image->height;
  jcprops.DIBWidth = image->width;
  jcprops.JPGHeight = image->height;
  jcprops.JPGWidth = image->width;
  jcprops.JPGSubsampling = (IJL_JPGSUBSAMPLING)IJL_NONE;
  if(IPL_ORIGIN_BL == image->origin)
    {
      jcprops.DIBHeight = -jcprops.DIBHeight;
    }
  
  switch(image->align)
    {
    case IPL_ALIGN_4BYTES:
      printf("4\n");
      jcprops.DIBPadBytes = IJL_DIB_PAD_BYTES(jcprops.DIBWidth, jcprops.DIBChannels);
      break;
    case IPL_ALIGN_8BYTES:
      printf("8\n");
      jcprops.DIBPadBytes = (jcprops.DIBWidth*jcprops.DIBChannels + 7)/8*8 -jcprops.DIBWidth*jcprops.DIBChannels;
      break;
    case IPL_ALIGN_16BYTES:
      printf("16\n");
      jcprops.DIBPadBytes = (jcprops.DIBWidth*jcprops.DIBChannels + 15)/16*16 -jcprops.DIBWidth*jcprops.DIBChannels;
      break;
    case IPL_ALIGN_32BYTES:
      printf("32\n");
      jcprops.DIBPadBytes = (jcprops.DIBWidth*jcprops.DIBChannels + 31)/32*32 -jcprops.DIBWidth*jcprops.DIBChannels;
      break;
    default:
      // error if go there
      break;
    }
  printf("padbytes = %i\n", jcprops.DIBPadBytes);
  // set the source for the input data
  // note-this will fail if the image is tile based.
  jcprops.DIBBytes = (BYTE*)image->imageData;
  return true;
} // SetJPEGProperties()

//----------------------------------------------------------------------------

void print_IplImage_properties(IplImage *im)
{
  printf("nsize = %i, ID = %i, nchannels = %i, alphachannel = %i, depth = %i\n", im->nSize, im->ID, im->nChannels, im->alphaChannel, im->depth);
  printf("colormodel = %s, channelseq = %s, dataorder = %i, origin = %i, align = %i, width = %i, height = %i, imagesize = %i, widthstep = %i\n", im->colorModel, im->channelSeq, im->dataOrder, im->origin, im->align, im->width, im->height, im->imageSize, im->widthStep);
}

//----------------------------------------------------------------------------

void print_JPEG_properties(JPEG_CORE_PROPERTIES* jcp)
{
  printf("DIBWidth = %i, DIBHeight = %i, DIBPadBytes = %i, DIBChannels = %i, DIBColor = %i, DIBSubsampling = %i\n", jcp->DIBWidth, jcp->DIBHeight, jcp->DIBPadBytes, jcp->DIBChannels, jcp->DIBColor, jcp->DIBSubsampling);
  printf("JPGSizeBytes = %i, JPGWidth = %i, JPGHeight = %i, JPGChannels = %i, JPGColor = %i, JPGSubsampling = %i\n", jcp->JPGSizeBytes, jcp->JPGWidth, jcp->JPGHeight, jcp->JPGChannels, jcp->JPGColor, jcp->JPGSubsampling);
  printf("color converted = %i, upsampled = %i\n", jcp->cconversion_reqd, jcp->upsampling_reqd);

}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------





