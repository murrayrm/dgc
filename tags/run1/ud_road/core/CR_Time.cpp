//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_Time.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// current time, written in the following format: "Mar_14_2002_Thu_07_28_00PM"

char *CR_datetime_string()
{
  time_t ltime;
  struct tm *today;
  char *s;
  
  s = (char *) CR_calloc(128, sizeof(char));

  time(&ltime);
  today = localtime(&ltime);

  strftime(s, 128, "%b_%d_%Y_%a_%I_%M_%S_%p", today);

  return s;
}

//----------------------------------------------------------------------------

// returns time in seconds

int CR_itime()
{
  return time(NULL);
}

//----------------------------------------------------------------------------

// returns time in seconds.milliseconds as a double 

double CR_time()
{
  long current;

#if defined(_WIN32)
  static struct timeb tb;
  ftime(&tb);
  current = tb.time * 1000 + tb.millitm;
#else
  static struct tms tb;
  current = times(&tb);
#endif
  
  return (double) current;
}

//----------------------------------------------------------------------------

// number of seconds since last call

double CR_difftime()
{
  static long begin = 0;
  static long finish, difference;

#if defined(_WIN32)
  static struct timeb tb;
  ftime(&tb);
  finish = tb.time * 1000 + tb.millitm;
#else
  static struct tms tb;
  finish = times(&tb);
#endif
  
  difference = finish - begin;
  begin = finish;
  
  return (double) difference / 1000.0;
}

//----------------------------------------------------------------------------

// in seconds

void CR_sleep(double time)
{
  double start_time, diff_time;

  diff_time = 0.0;
  start_time = CR_difftime();

  do {
    diff_time += CR_difftime();
  } while (diff_time < time);
}

//----------------------------------------------------------------------------

// double mytime(struct timeval *tp)
// {
//   gettimeofday(tp, NULL); 
//   return ((double) tp->tv_sec + ((double) tp->tv_usec) / 1000000.0);
// }

//----------------------------------------------------------------------------

// sleep for t seconds (fractions OK)

// void mysleep(double t)
// {
//   struct timeval tp;
//   double tottime = 0.0;
//   double lasttime;
// 
//   lasttime = mytime(&tp);
//   do {
//     tottime += difftime(&tp, &lasttime);
//   } while (tottime < t);
// }

//----------------------------------------------------------------------------

// sleep for t microseconds (millionths of a second)

// void usleep_CRTime(unsigned long t)
// {
//   usleep(t);
// }

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
