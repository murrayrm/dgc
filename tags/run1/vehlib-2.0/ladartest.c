#include "ladar_power.h"
#include <stdlib.h>
#include <stdio.h>
#include "MTA/Misc/Time/Time.hh"


int main() {
  ladar_pp_init();
  ladar_on(LADAR_BUMPER);
  ladar_on(LADAR_ROOF);
  
  for(int i=0; 10; i++){
    usleep_for(1000000);
    ladar_off(LADAR_BUMPER);
    printf("Bumper ladar is off.\n");
    
    usleep_for(1000000);
    ladar_off(LADAR_ROOF);
    printf("Roof ladar is off.\n");
    
    usleep_for(1000000);
    ladar_on(LADAR_BUMPER);
    printf("Bumper ladar is on.\n");
    
    usleep_for(1000000);
    ladar_on(LADAR_ROOF);
    printf("Roof ladar is on.\n");
    
  }
  printf("DONE\n");
  return 0;

}
