/*
 * vstate.cc - vehicle state estimtor
 *
 * RMM 31 Dec 03
 *
 * This program is the state estimator for the vehicle.  It provides an
 * on-screen display of the vehicle state and also sets up an MTA mailbox
 * that can be used to query the vehicle state.
 *
 * H 03 Jan 04 
 * Added magnetometer reading 
 * Added statefilter code
 *
 * Alex Fax 15 Jan 04
 * Kalman Filter added, statefilter no longer is run
 *
 * H 16 Jan 04
 * added data logging and display to KF
 * gps valid display
 *
 * H 30 Jan 04
 * Comments added, code cleaned up, all old statefilter stuff now gone
 * 
 * H 08 Feb 04
 * GPS disable implemented
 * GPS nav mode display implemented
 *
 * H 13 Feb 04
 * Added GPS jump finding routine for disconts in gps solution
 * created in new statefilter.cc
 *
 * H 27 Feb 04
 * Added function to open up gps error when a gps jump is detected
 *
 */

/*
  Kalman filtered state estimator
  Structure:  multiple threads are started up, one each for IMU, magnetometer,
  gps, and display. 
  Each thread loops continuously getting data.  The Kalman filter code runs 
  at 400 Hz, triggered by the IMU thread. 
  The KF itself updates at 1 Hz.  Magnetometer is used to initialize heading 
  but not during actual driving.  The vehicle
  MUST be stationary when the program starts, since the IMU is used to derive
  initial pitch and yaw assuming the car is stationary.
  KF takes a couple of seconds to converge to the initial settings, then is 
  updated at 1 Hz from GPS.  
  External dependencies:  
    kfilter.a
    kfilter/Global.h, Kalmano.h, kfilter.h
        
    LatLong.h (for UTM conversion)
*/

/* Logging details
   NOTE: all time stamps are local, i.e. referenced from start of this 
   program, except the Timeval timestamp
   in the vehstate struct, which I think is gotten through MTA
   statelog.dat - logs vehstate details, UTM, vel, etc
   kflog.dat - logs kalman filter details such as biases, residuals, etc
   kplog.dat - logs covariance matrix:
               first logs time, then fills rest of row with zeros, then next 
	       30 rows are Kp
   imulog.dat - imu raw data and timestamp (local)
   gpslog.dat - gps raw data and timestamp (local)
*/

// KNOWN BUGS AND ISSUES:
// 


#include <stdlib.h>
#include <math.h>
#include <iostream.h>
#include <unistd.h>
#include <pthread.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"
#include "sparrow/channel.h"
#include <string.h>

#include "vehports.h"
#include <fstream.h>

#include "MTA/Modules.hh"
#include "vsmta.hh"
#include "MTA/Kernel.hh"
#include <boost/shared_ptr.hpp>

#include "LatLong.h"

#include "kfilter/kfilter.h"
#include "kfilter/Kalmano.h"
#include "kfilter/Global.h"

#include "statefilter.hh"

#include "VStarPackets.hh"
#include "rot_matrix.hh"

/* Functions defined in this file */
static int user_quit(long);
int veh_gps_init(long), veh_gps_process(long);


// Externally defined Kalman Filter variables
extern int NavBufferIndex;
extern TSaveNavData NavBuffer[XRATIO2];

extern double gps_lat, gps_lon, gps_alt;
extern double gps_vn, gps_ve, gps_vu;
extern double gps_time;
extern double imu_time;
extern double mag_hdg;

extern int New_GPS_data_flag;   // flag set when new GPS data comes in
extern int New_Mag_data_flag;

extern double imu_dvx,imu_dvy,imu_dvz; // Delta-v's m/s (should be)
extern double imu_dtx,imu_dty,imu_dtz; // Delta-Thetas,should be rads

extern double kp[NUM_ST+1][NUM_ST+1];  // covariance matrix
extern double kx[NUM_ST+1];            // kalman gains

extern double tot_kabx, tot_kaby, tot_kabz;
extern double tot_ksfx, tot_ksfy, tot_ksfz;
extern double tot_kgbx, tot_kgby, tot_kgbz;
extern double tot_kgsfx;

extern double ExtGPSPosMeas_x, ExtGPSPosMeas_y, ExtGPSPosMeas_z;
extern double ExtGPSVelMeas_x, ExtGPSVelMeas_y, ExtGPSVelMeas_z;

extern double GPS_xy_kr, GPS_h_kr, GPS_v_kr; // errors

extern int inhib_vert_vel; 
int vvel;

/* Variables used in this file */
int mta_flag = 1;			/* run MTA */
int kfilter_flag = 1;			/* run Kalman filter */
int fake_filter = 1; // fake the kalman filter
struct VState_GetStateMsg vehstate;	/* current vehicle state */
struct timeval tv;
struct timeval start;


// note, enable = 0 means device is enabled but not active, enabled = 1 
// means device has been initialized update flags are for statefilter, 
// 0= no new data, 1 = new data received, -1 = currently updating

/* Define variables for GPS thread */
#define GPS
#include "gps.h"
pthread_t gps_thread;			/* GPS thread */
void *gps_start(void *);		/* GPS startup routine */
int veh_gps_com = GPS_SERIAL_PORT;	/* serial port for GPS */
int veh_gps_rate = 10;			/* GPS rate (Hz) */
gpsDataWrapper gpsdata;			/* GPS data */
static char *gps_buffer;		/* buffer for holding GPS data */
int gps_count = 0;			/* keep track of GPS reads */
int new_gps = 0;                        /* gps update flag */ 
double gheading;                        /* gps heading, used to compare gps 
					   heading with magnetometer */
int gps_valid;                          /* valid gps pvt flag */
double gps_local_time;                  // gps timestamp 
int gps_enabled = 0;			/* flag for enabling GPS */
int gps_active = 1;                     // flag for manually deactivating 
                                        // GPS during run
char gps_nav_msg[MAX_MSG_LEN];          // character message explaining 
                                        // nav mode
int gps_nav_mode;                       // current navigational mode 
double GPS_ALT_DEFAULT = 0;          // default altitude to feed to kf
int gps_ext_nav;                     // extended nav mode
double gps_jumped=0;                    // flag for gps discontinuities
int gps_invalid_count = 0;           // counter for how long we've been invalid
int gps_nav_state = GPS_NAV_VALID;      // gps nav state machine 
double gps_speed = 0.0;                   // speed from GPS

/* Define variable for IMU thread */
#define IMU
#include "imu.h"
pthread_t imu_thread;			/* IMU thread */
void *imu_start(void *);		/* IMU startup routine */
IMU_DATA imudata;			/* IMU data */
int imu_count = 0;			/* keep track of IMU reads */
int imu_enabled = 0;			/* flag for enabling IMU */
int imu10hz = 0;                        /* int to count down to 10 hz for 
					   IMU */
int imu1hz = 0;                         /* counter for 1hz tasks */
int on_startup = true;                     // first time startup

// declarations for converting imu location to vehicle origin
#include "frames.hh"

/* Magnetometer definitions */
#define MAGNETOMETER
#include "magnetometer.h"
pthread_t mag_thread;                   /* magnetometer thread */
void *mag_start(void *);                /* magnetometer startup routine */
mag_data magreading;                    /* read magnetometer data */
int veh_mag_com = MAG_SERIAL_PORT;      /* magnetometer serial port */
int mag_count = 0;                      /* keep track of magnetometer reads */
int mag_enabled = 0;                    /* magnetometer enable flag */
int first_mag = 1;                      // first time magnetometer run
double mag_time;                        // magnetometer time
double mag_est;                         // estimated mag heading
double mag_offset = 0;		// magnetometer offset
double magpitch;  // magnetometer pitch and rolls to be used for initializing 
double magroll;
int need_initialize = 1;
double mag_yaw;

// mag filter definitions
extern double mag_yaw_err;              // estimated error in magnetometer yaw
extern double mag_pk;
extern double mag_bk;
extern double yk;
extern double mag_pk1;

/* OBD2 definitions */
#include "OBDII.h"
pthread_t obd_thread;                   /* obd thread */
void *obd_start(void *);                /* obd startup routine */
int obdflag = 1;                       // flag for activating obd2
double obd_vel;                        // obd2 velocity
double obd_stored_lat;                 // stored positions for updating 
double obd_stored_lng;                 // when gps lost
double obd_stored_alt;
int obd_count;
int obd_valid = 0;

//Data logging declarations
fstream kffile;                        // fstream for logging kf vars
fstream imufile;                       // fstream for logging imu raw output
fstream gpsfile;                       // fstream for logging raw gps data
fstream statefile;                     // fstream for logging state data
fstream kpfile;                        // fstream for logging covariance data
fstream magfile;                       // fstream for logging mag data
fstream dopfile;                       // gps dop data
char *dopname = "dopfile.dat";        
char *statename = "statelog.dat";      // data log file for state
char *imuname = "imulog.dat";          // data log file for imu
char *gpsname = "gpslog.dat";          // data log for gps
char *kfname = "kflog.dat";            // data log for kf
char *kpname = "kplog.dat";            // covariance matrix log
char *magname = "magfile.dat";         // mag data log file
int logflag = 0;                       // enable/disable data logging
void logheaders();                     // write logfile headers to file

int display_flag = 1;   // flag to disable sparrow display

/* Usage message */
char *usage = "\
Usage: %s [-v] [options]\n\
  -d    disable dynamic display\n\
  -g    disable GPS subsystem\n\
  -h    print this message\n\
  -i    disable IMU subsystem\n\
  -k	disable Kalman filter; use best available data sources\n\
  -m    disable MTA subsystem\n\
  -v    turn on verbose error messages\n\
  -a    turns off magnetometer\n\
  -l    turns on logging from start\n\
  -o    enables obd2 corrections of velocity\n\
";

// include display header
// NOTE: these two lines need to be at the bottom of all the 
// declarations in order to ensure that all the desired variables 
// are readable by the display
#include "vsdisp.h"			/* display */
pthread_t dd_thread;			/* display thread */

rot_matrix nav_rot;  // rotation matrix to keep track of pitch/roll

VState *p_VState = new VState;

// Added by Ike to do process locking.
#define USE_LOCKS
#ifdef USE_LOCKS

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
boost::recursive_mutex      STATE_MUTEX;
extern int MTA_VALID;

#endif // USE_LOCKS


int main(int argc, char **argv)
{
    int c, errflg = 0, error = 0;
    

    /* Turn on error processing during startup */
    dbg_flag = dbg_all = dbg_outf = 1;

    /* Parse command line arguments */
    while ((c = getopt(argc, argv, "dvimkghalo?")) != EOF)
      switch (c) {
      case 'd':         display_flag = -1;              break;
      case 'v':		dbg_flag = 1;			break;
      case 'i':		imu_enabled = -1;		break;
      case 'm':		mta_flag = -1;			break;
      case 'g':		gps_enabled = -1;		break;
      case 'h':         errflg++;                       break;
      case 'a':         mag_enabled = -1;               break;
      case 'l':         logflag = 1;                    break;
      case 'o':         obdflag = -1;                    break;
      case 'k':		kfilter_flag = 0;		break;
      default:		errflg++;			break;
	break;
      }

    /* Print an error message if anything went wrong */
    if (errflg || argc < optind) {
      fprintf(stderr, usage, argv[0]);
      exit(1);
    }

    /*
     * Device Initialization
     *
     * Initialize all of the devices that might causes errors that
     * would stop us from running.
     *
     */

     // initialize headers for data log columns
    if (logflag)
	logheaders();
   

    /* GPS initialization; gps_enabled = -1 if disabled from command line */
    if (gps_enabled != -1) {
      if (gps_open(veh_gps_com) <0) {
	gps_set_nav_rate(veh_gps_rate);
	dbg_error("GPS: initialization failure\n");
	++error;
      } else
	{
	  gps_enabled = 1;
	  strcpy(gps_nav_msg, "Initialized");
	  overview[NAVMODE].initialized = 0;
	}
    } else
      {
	gps_enabled = 0;
	strcpy(gps_nav_msg, "Disabled");
	overview[NAVMODE].initialized = 0; // force display update
      }

    /* Mag initialization; mag_enabled = -1 if disabled from command line */
    if (mag_enabled != -1) {
      if (mm_open(veh_mag_com) != 0){
	dbg_error("Magnetometer error");
	++error;

	// No Mag present, so manually get heading 
	cout<<"Mag not enabled, enter current heading in degrees:";
	cin>>magreading.heading;
    
      }else{
	mag_enabled = 1;
	}
    }else{ 
	mag_enabled = 0;
	//Mag disabled, so manually initialize heading
	cout<<"Mag not enabled, enter current heading in degrees:";
	cin>>magreading.heading;
    }


    /* IMU initialization; imu_enabled = -1 if disabled from command line */
    if (imu_enabled != -1) {
       if (IMU_open() < 0) {
	 dbg_error("IMU error");
	++error; 
      } else {
	imu_enabled = 1;
	if (kfilter_flag == 1) DGCNavInit();
      }
    } else
      imu_enabled = 0;
    
    /*
    // OBD2 Initialization
    if (obdflag != -1){
      if (OBD_Init(OBD_SERIAL_PORT) < 0) {
	dbg_error("OBDII error");
	++error;
      }else{
	obdflag = 1;
      }
    }else
	obdflag = 0;
    */

    /* Initialize sparrow channel structures 
    if (chn_config("kfconfig.dev") < 0) {
      dbg_error("can't open kfconfig.dev");
      ++error;
    } else if (chn_init() < 0) {
      dbg_error("error initializing channel interface");
      ++error;
      }*/

    // initialize starting time 
    gettimeofday(&start, NULL);


    // set flags for sensors active
    vehstate.gps_enabled = gps_enabled;
    vehstate.imu_enabled = imu_enabled;
    vehstate.mag_enabled = mag_enabled;
      
    /* Pause if there are any errors */
    dbg_info("gps_enabled = %d, imu_enabled = %d, mag_enabled = %d, kf = %d\n", 
	     gps_enabled, imu_enabled, mag_enabled, kfilter_flag);
    if (error) {
      fprintf(stdout, "Errors on startup; continue (y/n)? ");
      if (getchar() != 'y') exit(1);
    }

    /* Turn off printf while display is running */
    dbg_all = 0;

    if (display_flag >0) {
    /* Initialize the display package */
    if (dd_open() < 0) exit(1);		/* initialize the database, screen */
    dd_bindkey('Q', user_quit);         // Q quits
    dd_bindkey('L', user_log_inc);      // L causes the log to increment
    dd_bindkey('G', user_gps_toggle);   // G causes the gps to switch on 
                                        // and off
    dd_usetbl(overview);		/* set display description table */
    }
    /* Start up threads for execution */

    // imu thread
    if (imu_enabled > 0 ) {
    if (pthread_create(&imu_thread, NULL, imu_start, (void *) NULL) < 0) {
      dbg_error("Can't start IMU thread \n");
      exit(1);
    }
    }

    // gps thread
    if (gps_enabled > 0) {
    if (pthread_create(&gps_thread, NULL, gps_start, (void *) NULL) < 0) {
      dbg_error("Can't start GPS thread \n");
      exit(1);
    }
    }

    // magnetometer thread
    if (mag_enabled > 0){
    if (pthread_create(&mag_thread, NULL, mag_start, (void *) NULL) < 0) {
      dbg_error("Can't start mag thread \n");
      exit(1);
    }
    }


    // obd thread
    if (obdflag > 0 ) {
    if (pthread_create(&obd_thread, NULL, obd_start, (void *) NULL) < 0) {
      dbg_error("Can't start OBD thread \n");
      exit(1);
    }
    }

    if (display_flag >0) {
    /* Run the display manager as a thread */
    pthread_create(&dd_thread, NULL, dd_loop_thread, (void *) NULL);
    }

    if (mta_flag  < 0)
      /* If we aren't running MTA, wait until dd_loop ends */
      pthread_join(dd_thread, (void **) NULL);
    else {
      /* Otherwise, pass control to MTA */
      Register(shared_ptr<DGC_MODULE>( p_VState ));
      StartKernel();
    }
    
    if (display_flag > 0) {
    dd_close();	     /* clear the screen and free memory */
    }
    /* User cleanup goes here */
}

/*
 * Process threads 
 *
 * These routines define the process threads used in vstate.  Each of
 * them should be started up in the main code.  Once called, they
 * should loop forever.
 *
 * TBD: should integrate initialization and move these routines to their
 * respective files, so that startup becomes particularly simple.
 *
 */



#ifdef IMU
void *imu_start(void *arg)
{
  LatLong latlong(0 , 0); // latlong conversion
  int row, col;
  XYZcoord blank(0,0,0); // blank vector for initializing imu frame
  XYZcoord imu_offset(IMU_X_OFFSET, IMU_Y_OFFSET, IMU_Z_OFFSET);
  frames imu_vector(imu_offset, 0, 0, 0);
  XYZcoord imu_correct(0,0,0);
  double speed, ve, vn, vu;

  cout<<"IMU Loop started"<<endl;

  while( ! MTA_VALID ) {
    usleep(200000);
  }   

  // holding thread to prevent kf from initializing after we segfault
  if (kfilter_flag && imu_enabled && on_startup){
    while ((obdflag && (obd_vel > 0.0) && obd_valid) || 
	   (gps_enabled && (gps_speed > GPS_VEL_CUTOFF) && gps_valid) ||
	   (gps_count == 0))
      {
	cout<<"waiting for speed to come down"<<endl;
	usleep(500000);
      }
    on_startup = false;
  }


  /* Thread reads the imu, calls a KF update, and then stores results */
  while (1) {
    //    vehstate.Timestamp = TVNow();
    if ((imu_enabled > 0)) {
      IMU_read(&imudata);

      ++imu_count;

      //log data
      /* Save the time the data was taken */
      gettimeofday(&tv, NULL);
      
      // pass data to Kalman filter   
      imu_time = (double) (tv.tv_sec - start.tv_sec) + 
	((double) (tv.tv_usec - start.tv_usec)) / 1e6;
      //imu_dvx = imudata.dvx;
      //imu_dvy = imudata.dvy;
      //imu_dvz = imudata.dvz;
      imu_dtx = imudata.dtx;
      imu_dty = imudata.dty;
      imu_dtz = imudata.dtz;

      imu_dvx = 0;
      imu_dvy = 0;
      imu_dvz = .025;
      
      if (kfilter_flag) {
#ifdef USE_LOCKS
      boost::recursive_mutex::scoped_lock sl( STATE_MUTEX );
#endif

	// stores KF data into vehstate
      	vehstate.Timestamp = TVNow();
	/*
	vehstate.kf_lat = NavBuffer[NavBufferIndex].lat / DEG2RAD;
	vehstate.kf_lng = NavBuffer[NavBufferIndex].lon / DEG2RAD;
	//vehstate.Altitude = -NavBuffer[NavBufferIndex].alt;
	vehstate.Altitude = 0;
	latlong.set_latlon(vehstate.kf_lat, vehstate.kf_lng);
	latlong.get_UTM(&vehstate.Easting, &vehstate.Northing);
        */
	//vehstate.Yaw = NavBuffer[NavBufferIndex].thdg * DEG2RAD;
	vehstate.Pitch = NavBuffer[NavBufferIndex].pitch;// + IMU_PITCH_OFFSET;
	vehstate.Roll =  NavBuffer[NavBufferIndex].roll;// + IMU_ROLL_OFFSET;
	/*
	// only update the velocities if we're at higher velocities
	vn = NavBuffer[NavBufferIndex].vn;
	ve = NavBuffer[NavBufferIndex].ve;
	vu = NavBuffer[NavBufferIndex].vd;
	speed = sqrt(vn * vn + ve * ve + vu * vu);

	if ((obdflag > 0) && (speed < OBD_CUTOFF) && (obd_vel >= 0)){
	  vehstate.Speed = obd_vel;
	  vehstate.Vel_N = obd_vel * cos(vehstate.Yaw);
	  vehstate.Vel_E = obd_vel * sin(vehstate.Yaw);
	  vehstate.Vel_U = vu;
	}else{
	  vehstate.Speed = speed;
	  vehstate.Vel_E = ve;
	  vehstate.Vel_N = vn;
	  vehstate.Vel_U = vu;
	  }
	
	
	// transform the IMU offset vector into UTM meters
	imu_vector.updateState(vehstate);
	imu_correct = imu_vector.transformS2N(blank); 
	vehstate.Northing = imu_correct.x;
	vehstate.Easting = imu_correct.y;
	vehstate.actual_Altitude = imu_correct.z 
	  - NavBuffer[NavBufferIndex].alt;
	vehstate.Altitude = 0;
	

	// generate rates 
	vehstate.PitchRate = imudata.dty * 400;
	vehstate.RollRate = imudata.dtx * 400;
	vehstate.YawRate = imudata.dtz * 400;
	// harshness
	// vehstate.pitch_harshness = p_harsh(vehstate.PitchRate);
	// vehstate.roll_harshness = r_harsh(vehstate.RollRate);
      }else{
	if (need_initialize){
	  nav_rot.setattitude(magpitch, magroll, vehstate.Yaw);
	  need_initialize = 0;
	}
	nav_rot.propagate(imudata.dtx, imudata.dty, imudata.dtz);
	vehstate.Pitch = nav_rot.get_pitch();
	vehstate.Roll = nav_rot.get_roll(); */
      }

	// log data
	if (logflag)
	  {
	    imufile.precision(10);
	    imufile<<imu_time<<'\t'<<imudata.dvx<<'\t'<<imudata.dvy<<
	      '\t'<<imudata.dvz<<'\t'<<imudata.dtx<<'\t'<<imudata.dty<<
	      '\t'<<imudata.dtz<<endl;

	    // if there's been a Kalman update then save the kf data
	    // counter to slow logging to 10 Hz
	    if ((imu1hz >= 400) && 
		(NavBuffer[NavBufferIndex].KalmanUpdate != 0))
	      {
		statefile.precision(10);
		statefile<<imu_time<<'\t'<<vehstate.Easting<<
		  '\t'<<vehstate.Northing<<'\t'<<vehstate.Altitude<<
		  '\t'<<vehstate.Vel_E<<'\t'<<vehstate.Vel_N<<
		  '\t'<<vehstate.Vel_U<<'\t'<<vehstate.Speed<<
		  '\t'<<vehstate.Accel<<'\t'<<vehstate.Pitch<<
		  '\t'<<vehstate.Roll<<'\t'<<vehstate.Yaw<<
		  '\t'<<vehstate.kf_lat<<'\t'<<vehstate.kf_lng<<
		   '\t'<<vehstate.PitchRate<<
		  '\t'<<vehstate.RollRate<<'\t'<<vehstate.YawRate<<
		  '\t'<<sqrt(GPS_xy_kr) * EARTH_RADIUS<<
		  '\t'<<obd_vel<<endl;

		kffile.precision(10);
		kffile<<imu_time<<
		  '\t'<<NavBuffer[NavBufferIndex].KalmanUpdate<<
		  '\t'<<ExtGPSPosMeas_x<<'\t'<<ExtGPSPosMeas_y<<
		  '\t'<<ExtGPSPosMeas_z<<'\t'<<ExtGPSVelMeas_x<<
		  '\t'<<ExtGPSVelMeas_y<<'\t'<<ExtGPSVelMeas_z<<
		  '\t'<<tot_kabx<<'\t'<<tot_kaby<<'\t'<<tot_kabz<<
		  '\t'<<tot_kgbx<<'\t'<<tot_kgby<<'\t'<<tot_kgbz<<
		  '\t'<<tot_kgsfx<<'\t'<<endl;
	    
	    
		// log covariance matrix, row by row   
		kpfile.precision(10);
		
		// log the time and fill the rest of the row with zeros
		kpfile<<imu_time<<'\t';
		for (col = 1; col <= NUM_ST; col++){
		  kpfile<<0<<'\t';
		}
		kpfile<<endl;
		    
		// log the covariance matrix itself
		for(col = 0; col <= NUM_ST; col++)
		  {
		    for (row = 0; row <= NUM_ST; row++)
		      {
			kpfile<<kp[row][col]<<'\t';
		      }
		    kpfile<<endl;
		  }
	    
		imu1hz = 0;
	      }
	  }
	imu1hz ++;
	// store raw data into vehstate
      vehstate.imudata = imudata;

      if (kfilter_flag == 1) {
	// run kalman filter routines
	DGCNavRun();
      }

      // increment counter to for logging state data
      //imu10hz++;
      
    }

  }

  return NULL;
}
#endif


#ifdef GPS
/* Read messages from the GPS unit and put them into a buffer */
void *gps_start(void *arg)
{
  char *gps_buffer;
  LatLong gps_ll(0,0);
  double gps_east, gps_north;
  double speed;
  double gps_error;
  double gps_pdop;
  
  Transmission_Packet veh_transmission;
  int ret;

  if ((gps_buffer = (char *) malloc(2000)) == NULL) {
    dbg_panic("GPS: memory allocation error\n");
    return NULL;
  }

  while( ! MTA_VALID ) {
    usleep(200000);
  }   

  while (1) {
  
  ret = Get_Transmission_Packet(p_VState->MyAddress(), veh_transmission);    
    
    if (gps_enabled > 0) {
      gps_read(gps_buffer, 1000);  //this is not blocking

      switch (gps_msg_type(gps_buffer)) {
      case GPS_MSG_PVT:
	gpsdata.update_gps_data(gps_buffer);
	++gps_count;
	
	// store raw data into MTA
	vehstate.gpsdata = gpsdata.data;
	
	//PREPROCESSING SECTION
	
	// check for pvt valid flag 
	gps_valid = ((gpsdata.data.nav_mode & NAV_VALID) && (gps_active));  
	//gps_valid = 1;

	gps_nav_mode = (int)gpsdata.data.nav_mode;
	gps_ext_nav = (int)gpsdata.data.extended_nav_mode;

	if (display_flag > 0){
	// get display string for message
	strcpy(gps_nav_msg, gps_mode(gpsdata.data.nav_mode)); 
        overview[NAVMODE].initialized = 0; // force update of string
	}

	// set pvt valid flag in MTA so rest of system knows 
	// if we're getting gps
	vehstate.gps_pvt_valid = gps_valid;	
	gps_pdop = gpsdata.data.pdop;

	//log data
	/* Save the time the data was taken */
	gettimeofday(&tv, NULL);
        gps_local_time = (double) (tv.tv_sec - start.tv_sec) + 
	  ((double) (tv.tv_usec - start.tv_usec)) / 1e6;

	//gps_error = (gpsdata.data.pdop + gpsdata.data.hdop + gpsdata.data.tdop
	//	     + gpsdata.data.gdop + gpsdata.data.vdop) / 5;
      //gps_error = pow(gps_error / EARTH_RADIUS, 2);

	// transform gps lat/long into utm for logging
	gps_ll.set_latlon(gpsdata.data.lat, gpsdata.data.lng);
	gps_ll.get_UTM(&gps_east, &gps_north);
	//END PREPROCESSING

	// speed checking for initializing kf
	if (gps_valid > 0){
	  gps_speed = sqrt(pow(gpsdata.data.vel_n, 2) 
	     + pow(gpsdata.data.vel_e, 2));
	}
	
	//gps_speed *= .75;
	
	//DATA PROCESSING SECTION       
	// GPS nav state machine
	// switches behavior of GPS depending on the the state we're in
	// NAV_VALID and NAV_INVALID are self-explanatory
	// if we have invalid gps for a prolonged time then we get set up to 
	// pull the kf solution over and set the jump flag for planning
	// software
	switch (gps_nav_state) {

	case GPS_NAV_VALID:	  
	  if(gps_valid > 0)
	    { 
	      
	      gps_jumped = detect_jump(gps_north, gps_east
				       , gps_local_time, vehstate);	      
	      // if there's been a jump in the gps solution, open up the error
	      GPS_xy_kr = gps_err_reset(GPS_xy_kr, gps_jumped, gps_local_time);
	      //GPS_xy_kr = gps_error;
	      GPS_v_kr = GPS_VEL_ERR;
	      	      
	    }else{
	      // go to invalid state and reset the jump detect filter 
	      gps_nav_state = GPS_NAV_INVALID;
	      gps_invalid_count++;	      
	      inhib_vert_vel = true; // disable KF z velocity updates
	      vvel = inhib_vert_vel;
	    } // if gps invalid
	  break;

	case GPS_NAV_INVALID:
	  // check if we've picked up GPS again
	  if(gps_valid > 0)
	    { 
	      // switch state
	      gps_nav_state = GPS_NAV_VALID;
	      inhib_vert_vel = false; // enable KF z vel updates
	      vvel = inhib_vert_vel;
	      gps_jumped = detect_jump(gps_north, gps_east
				       , gps_local_time, vehstate);
	      
	      // if there's been a jump in the gps solution, open up the error
	      GPS_xy_kr = gps_err_reset(GPS_xy_kr, gps_jumped, gps_pdop);
	    }else{	     
	      gps_invalid_count++;
	      if (gps_invalid_count >= JUMP_RESET_THRESHOLD) {
		detect_reset();
		// reset the gps error to the default error
		GPS_xy_kr = pow(GPS_DEFAULT_ERROR / EARTH_RADIUS , 2);
	      }
	      if (gps_invalid_count >= INVALID_THRESHOLD){
		gps_nav_state = EXT_NAV_INVALID;
	      }
	    } // if gps invalid
	  break;

	case EXT_NAV_INVALID:
	  // check if we've picked up GPS again
	  if(gps_valid > 0)
	    { 
	      // switch state
	      gps_nav_state = GPS_NAV_VALID;
	      inhib_vert_vel = false; // enable KF z vel updates
	      vvel = inhib_vert_vel;
	      gps_invalid_count = 0; // reset time counter for invalid gps

	      gps_jumped = detect_jump(gps_north, gps_east
				       , gps_local_time, vehstate);
	      
	      // if there's been a jump in the gps solution, open up the error
	      GPS_xy_kr = gps_err_reset(GPS_xy_kr, gps_jumped, gps_pdop);

	      // EXTENDED GPS INVALID JUMP DETECT
	      // insert code here for giving planning amount of displacement 
	      // for gps jump 
	      /*
		vehstate.ext_jump_n = gps_north - vehstate.Northing;
		vehstate.ext_jump_e = gps_east - vehstate.Easting; */
	      vehstate.ext_jump_flag = true;
	      
	    }else{	     
	      gps_invalid_count++;	      
	    } // if gps invalid
	  break;
	  
	default:
	  // default, in case we somehow get into a weird mode, is to just
	  // do what we do when GPS is normal and valid
	  if(gps_valid > 0)
	    { 
	      gps_jumped = detect_jump(gps_north, gps_east
				       , gps_local_time, vehstate);
	      
	      // if there's been a jump in the gps solution, open up the error
	      GPS_xy_kr = gps_err_reset(GPS_xy_kr, gps_jumped, gps_pdop);  
	      gps_nav_state = GPS_NAV_VALID;
	      inhib_vert_vel = false;
	      vvel = inhib_vert_vel;
	    }else{
	      // go to invalid state and reset the jump detect filter 
	      gps_nav_state = GPS_NAV_INVALID;
	      gps_invalid_count++;	      
	      detect_reset(); 
	      inhib_vert_vel = true; // disable KF z velocity updates
	      vvel = inhib_vert_vel;
	    } // if gps invalid
	  break;
	}
	
	// if KF needs new GPS data, write it over
	if ((New_GPS_data_flag == 0) && (kfilter_flag) && (gps_valid)) {
	  //inhib_vert_vel = false;
	  gps_time = gps_local_time;
	  gps_lat = gpsdata.data.lat*DEG2RAD;
	  gps_lon = gpsdata.data.lng*DEG2RAD;
	  gps_alt = gpsdata.data.altitude;
	  
	  if (obdflag && obd_valid && obd_vel == 0.0){
	    gps_vu = gps_vn = gps_ve = 0.0;
	    GPS_v_kr = OBD_VEL_ERR;
	  }else{
	    GPS_v_kr = GPS_VEL_ERR;
	    gps_vu  = gpsdata.data.vel_u;
	    gps_vn  = gpsdata.data.vel_n;
	    gps_ve  = gpsdata.data.vel_e;
	  }
	  New_GPS_data_flag = 1;
	}
	
	// log all data after everything is done
	if (logflag){
	  // log gps data
	  gpsfile.precision(10);
	  gpsfile<<gps_local_time<<'\t'<<gps_valid<<
	    '\t'<<New_GPS_data_flag<<'\t'<<gps_nav_mode<<
	    '\t'<<gpsdata.data.lat<<'\t'<<gpsdata.data.lng<<
	    '\t'<<gps_east<<'\t'<<gps_north<<'\t'<<gpsdata.data.altitude<<
	    '\t'<<gpsdata.data.vel_n<<'\t'<<gpsdata.data.vel_e<<
	    '\t'<<gpsdata.data.vel_u<<'\t'<<gps_active<<
	    '\t'<<gps_ext_nav<<'\t'<<gps_jumped<<
	    '\t'<<gpsdata.data.sats_used<<endl;
	  
	  dopfile.precision(10);
	  dopfile<<gpsdata.data.position_fom<<'\t'<<gpsdata.data.gdop<<
	    '\t'<<gpsdata.data.pdop<<'\t'<<gpsdata.data.hdop<<
	    '\t'<<gpsdata.data.vdop<<'\t'<<gpsdata.data.tdop<<
	    '\t'<<gpsdata.data.tfom<<endl;
	  
	} // if log flag

	/* If kfilter is turned off, use best available data source */
	if (kfilter_flag == 0 || fake_filter) {
	/* Get state information from best available sensors */
	  vehstate.Roll = vehstate.Pitch = 0;	/* assume flat driving */
	  vehstate.Timestamp = TVNow();

	  /* Use GPS for position */
	  if ((gps_enabled == 1) && (gps_valid)) {
	    vehstate.kf_lat = gpsdata.data.lat;
	    vehstate.kf_lng = gpsdata.data.lng;
	    vehstate.Altitude = 0;
	    gps_ll.set_latlon(vehstate.kf_lat, vehstate.kf_lng);
	    gps_ll.get_UTM(&vehstate.Easting, &vehstate.Northing);
	    
	    vehstate.Vel_N = gpsdata.data.vel_n;
	    vehstate.Vel_E = gpsdata.data.vel_e;
	    vehstate.Speed = sqrt(pow(vehstate.Vel_N,2) 
				  + pow(vehstate.Vel_E,2));
	    
	    if (vehstate.Speed > GPS_VEL_CUTOFF) {
	      if (ret == 0 && veh_transmission.Gear > 0){
		vehstate.Yaw = atan2(gpsdata.data.vel_e, gpsdata.data.vel_n);
	      }else{
		vehstate.Yaw = norm_yaw(atan2(gpsdata.data.vel_e, 
					      gpsdata.data.vel_n) + M_PI);
	      }
		magfilter(mag_yaw, vehstate.Yaw);
	    
	    } else {
	      vehstate.Yaw = mag_est;
	      
	      if (obdflag && obd_valid){
		vehstate.Speed = obd_vel;
	      }else{
		/* Assume we are stopped at less than the cutoff */
		vehstate.Speed = 0;
	      }
	    }
	  } //if gps_enabled == 1
	}// if kfilter_flag  
	
	break;
	
      default:
	/* print_gps_msg(gps_buffer) */
	break;
	
      }
    }
  }
  return NULL;
}
#endif

#ifdef MAGNETOMETER
// updates magnetometer data continuously
void *mag_start(void *arg)
{
  
  while( ! MTA_VALID ) {
    usleep(200000);
  }   

  while(1){
    if (mag_enabled > 0){
      mm_read(magreading);
      mag_count++;

      //log data
      /* Save the time the data was taken */
      gettimeofday(&tv, NULL);
      
      // pass data to Kalman filter   
      mag_time = (double) (tv.tv_sec - start.tv_sec) + 
	((double) (tv.tv_usec - start.tv_usec)) / 1e6;
     
      if (logflag){
	magfile<<mag_time<<'\t'<<magreading.heading<<'\t'<<magreading.pitch<<
	  '\t'<<magreading.roll<<'\t'<<mag_est<<'\t'<<mag_yaw_err<<
	  '\t'<<mag_pk<<'\t'<<mag_bk<<endl;
      }

      // store raw mag data
      //magreading.heading += mag_offset;
      vehstate.magreading = magreading;
  

    if (kfilter_flag == 1) {
      if (New_Mag_data_flag == 0){
	mag_hdg = magreading.heading*DEG2RAD;
	New_Mag_data_flag = 1;
      } // if new mag
    }

    if (fake_filter){
      mag_yaw = magreading.heading * DEG2RAD;
      mag_est = norm_yaw(mag_yaw + mag_yaw_err);
      
      magpitch = norm_yaw((magreading.pitch + mag_pitch_offset) * DEG2RAD);
      magroll = norm_yaw((magreading.roll + mag_roll_offset) * DEG2RAD);

      // update heading if speed low, otherwise calculate error
      if (vehstate.Speed <= GPS_VEL_CUTOFF)
	{ 
	  
	}else if ((vehstate.Speed > GPS_VEL_CUTOFF) && (gps_valid)){
	  
	}

    } // if kfilter_flag == 1
  } // if mag_enabled
  } // while(1)
  return NULL;
}
#endif

/*
 * Callbacks to control operation of the program
 *
 */

/* Quit the dfan program */
int user_quit(long arg)
{
  int count = 0;
  if (logflag)
    {
      imufile.close();
      gpsfile.close();
      kffile.close();
      kpfile.close();
      statefile.close();
    }

  if( mta_flag == 1)
    {
  /* Tell MTA to shut down */
  ForceKernelShutdown();

  /* Wait for 1 second to make sure it is shut down */
  while (ShuttingDown()) {
    if (count++ > 1000) break;
    usleep(1000);
  }

}
    /* Tell dd_loop to abort now */
    return DD_EXIT_LOOP;
}


/* Toggle capture mode */
//int user_cap_toggle(long arg)
//{
//  return chn_capture_flag ? chn_capture_off() : chn_capture_on();
//}

// enables or disables data logging
// if data logging is disabled, then enable.  Otherwise disable and close
int user_log_inc(long arg)
{
  if (logflag == 1)
    {
      // disable logging and close log files
      logflag = 0;
      imufile.close();
      gpsfile.close();
      statefile.close();
      kffile.close();
      kpfile.close();
      dopfile.close();
    }
  else
    {
      logflag = 1;
      logheaders();
    }
  return 1;
}

// keeps the KF from getting gps updates, for testing only
int user_gps_toggle(long arg)
{
  if (gps_active)
    gps_active = 0;
  else
    gps_active = 1;
  
  return 1;
}

void logheaders()
{
  // imu file logging
	imufile.open(imuname, fstream::out|fstream::app);
        imufile<<"%imu_time"<<'\t'<<"imudata.dvx"<<'\t'<<"imudata.dvy"<<
	  '\t'<<"imudata.dvz"<<'\t'<<"imudata.dtx"<<'\t'<<"imudata.dty"<<
	  '\t'<<"imudata.dtz"<<endl;

	// gps data logging
	gpsfile.open(gpsname, fstream::out|fstream::app);
	gpsfile<<"%gps_local_time"<<'\t'<<"gps_valid"<<
	  '\t'<<"New_GPS_data_flag"<<'\t'<<"GPS Nav Mode"<<
	  '\t'<<"gpsdata.data.lat"<<'\t'<<"gpsdata.data.lng"<<
	  '\t'<<"gps_easts"<<'\t'<<"gps_north"<<
	  '\t'<<"gpsdata.data.altitude"<<'\t'<<"gpsdata.data.vel_n"<<
	  '\t'<<"gpsdata.data.vel_e"<<'\t'<<"gpsdata.data.vel_u"<<
	  '\t'<<"gps_active"<<'\t'<<"gps_ext_nav"<<'\t'<<"gps_jumped"<<
	  '\t'<<"gpsdata.data.sats_used"<<endl; 

	dopfile.open(dopname, fstream::out|fstream::app);
	dopfile<<"%position_fom"<<'\t'<<"gdop"<<
	  '\t'<<"pdop"<<'\t'<<"hdop"<<
	    '\t'<<"vdop"<<'\t'<<"tdop"<<
	    '\t'<<"tfom"<<endl;


	statefile.open(statename, fstream::out|fstream::app);
	statefile<<"%imu_time"<<'\t'<<"vehstate.Easting"<<
	  '\t'<<"vehstate.Northing"<<'\t'<<"vehstate.Altitude"<<
	  '\t'<<"vehstate.Vel_E"<<'\t'<<"vehstate.Vel_N"<<
	  '\t'<<"vehstate.Vel_U"<<'\t'<<"vehstate.Speed"<<
	  '\t'<<"vehstate.Accel"<<'\t'<<"vehstate.Pitch"<<
	  '\t'<<"vehstate.Roll"<<'\t'<<"vehstate.Yaw"<<'\t'<<"kf_lat"<<
	  '\t'<<"kf_lng"<<'\t'<<"PitchRate"<<'\t'<<
	  "RollRate"<<'\t'<<"YawRate"<<'\t'<<"gpserror"<<'\t'<<"obd"<<endl;

	// kf data logging
	kffile.open(kfname, fstream::out|fstream::app);
	kffile<<"%imu_time"<<'\t'<<"KalmanUpdate"<<
	  '\t'<<"ExtGPSPosMeas_x"<<'\t'<<"ExtGPSPosMeas_y"<<
	  '\t'<<"ExtGPSPosMeas_z"<<'\t'<<"ExtGPSVelMeas_x"<<
	  '\t'<<"ExtGPSVelMeas_y"<<'\t'<<"ExtGPSVelMeas_z"<<
	  '\t'<<"tot_kabx"<<'\t'<<"tot_kaby"<<'\t'<<"tot_kabz"<<
	  '\t'<<"tot_kgbx"<<'\t'<<"tot_kgby"<<'\t'<<"tot_kgbz"<<
	  '\t'<<"tot_kgsfx"<<endl;

	//kf covariance matrix logging
	kpfile.open(kpname, fstream::out|fstream::app);
	
	// mag data logging
	magfile.open(magname, fstream::out|fstream::app);
	magfile<<"%mag_time"<<'\t'<<"magreading.heading"<<
	  '\t'<<"magreading.pitch"<<'\t'<<"magreading.roll"<<
	  '\t'<<"mag_est"<<'\t'<<"mag_yaw_err"<<'\t'<<"mag_pk"<<
	  '\t'<<"mag_bk"<<endl;

	return;
}


void *obd_start(void *)
{
  double speed;
  OBD_Packet myOBD;
  myOBD.Velocity = myOBD.RPM = myOBD.valid = 0;
  int ret;

  cout << "Starting OBD2 Loop" << endl;
  
  while( ! MTA_VALID ) {
    usleep(200000);
  }   

  cout << "OBD2 Started" << endl;

  while (1){
    usleep(300000);
    if (obdflag > 0){
      //obd_vel = Get_Speed(); 
      ret = Get_OBD_Packet( p_VState->MyAddress(), myOBD ); 
     
      if( ret == 0 && myOBD.valid ) {

	{ // lock the obd velocity update
#ifdef USE_LOCKS
      boost::recursive_mutex::scoped_lock sl( STATE_MUTEX );
#endif

	obd_vel = myOBD.Velocity;
	obd_valid = myOBD.valid;
	obd_count++;
	}

	// OBDII SECTION 
	switch (gps_nav_state) {
	case GPS_NAV_VALID:
	  //obd_stored_lat = vehstate.kf_lat;
	  //obd_stored_lng = vehstate.kf_lng;
	  obd_stored_alt = vehstate.actual_Altitude;
	  break;
	  
	case GPS_NAV_INVALID:
	  // start storing the position so that we know what to feed to the kf
	  // if we're stopped
	  
	  if (obd_vel > 0.0)
	    {
	      //obd_stored_lat = vehstate.kf_lat;
	      //obd_stored_lng = vehstate.kf_lng;
	      //obd_stored_alt = vehstate.real_Altitude;
	    }else{
	      // if KF needs new GPS data, write it over
	      if ((New_GPS_data_flag == 0) && (kfilter_flag)) {
		GPS_v_kr = OBD_VEL_ERR;
		gps_time = gps_local_time;
		gps_lat = vehstate.kf_lat;
		gps_lon = vehstate.kf_lng;;
		gps_alt = -obd_stored_alt;
		gps_vu  = 0;
		gps_vn  = obd_vel * cos(vehstate.Yaw);
		gps_ve  = obd_vel * sin(vehstate.Yaw);
		
		New_GPS_data_flag = 1;
	      }
	    }	
	  break;
	  
	case EXT_NAV_INVALID:
	  // start storing the position so that we know what to feed to the kf
	  // if we're stopped	
	  
	  if (obd_vel > 0.0)
	    {
	      //obd_stored_lat = vehstate.kf_lat;
	      //obd_stored_lng = vehstate.kf_lng;
	      // obd_stored_alt = vehstate.real_Altitude;
	    }else{
	      // if KF needs new GPS data, write it over
	      if ((New_GPS_data_flag == 0) && (kfilter_flag)) {
		GPS_v_kr = OBD_VEL_ERR;
		gps_time = gps_local_time;
		gps_lat = vehstate.kf_lat;
		gps_lon = vehstate.kf_lng;;
		gps_alt = -obd_stored_alt;		
		gps_vu  = 0;
		gps_vn  = obd_vel * cos(vehstate.Yaw);
		gps_ve  = obd_vel * sin(vehstate.Yaw);
		
		New_GPS_data_flag = 1;
	      }
	    }	
	  break;
	  
	default:
	  break;
	}
	
      } else { 
	// if not got new obd data
	if (display_flag <= 0){
	  cout << "Did not Get OBD2, ret = " 
	       << ret << "myOBD.valid = " 
	       << myOBD.valid << endl;
	}
	usleep(500000);
      }
    }
  }
  return NULL;
}

