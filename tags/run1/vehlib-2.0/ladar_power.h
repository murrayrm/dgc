#ifndef _LADAR_POWER_H
#define _LADAR_POWER_H

#include "parallel.h"

#define LADAR_PPORT 2
#define LADAR_BUMPER PP_DATA0
#define LADAR_ROOF PP_DATA1

int ladar_pp_init();
int ladar_on(int ladarnumber);
int ladar_off(int ladarnumber);

#endif
