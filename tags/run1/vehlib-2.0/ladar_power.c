#include "ladar_power.h"

int ladar_pp_init(){
  //set up the parallel port with data output
  return(pp_init(LADAR_PPORT, PP_DIR_OUT));
  ladar_on(LADAR_BUMPER);
  ladar_on(LADAR_ROOF);
}

int ladar_on(int ladar_number){
  //put ladar_number low -> output is high, relay on
  return(pp_set_bits(LADAR_PPORT, 0, ladar_number));
}

int ladar_off(int ladar_number){
  //put ladar_number high-> output is low, relay off
  return(pp_set_bits(LADAR_PPORT, ladar_number, 0));
}
