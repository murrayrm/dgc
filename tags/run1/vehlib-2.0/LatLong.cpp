/*
 * Known concerns:
 *   Case of right on the bounds between two zones, like 120 degrees west
 * longitude.  I don't know how to handle such a case.  For now, I've assumed
 * that on such a border longitude, lon_0 is the input longitude.  This may
 * be incorrect.
 *
 *   This probably only works in the North West hemisphere.
 */

#include "LatLong.h"
#include <assert.h>

using namespace std;

LatLong::LatLong(double lat, double lon, double a, double e) {
  _lat = lat;
  _lon = lon;
  if (a <= 0 || e <= 0) {
    cout << "LatLong Error: Earth radius and eccentricity must be positive.\n";
    cout << "  Attempting to recover using default values (WGS84 datum).\n";
    _a = DEFAULT_A;
    _e = DEFAULT_E;
  }
  else {
    _a = a;
    _e = e;
  }
  _b = _a * sqrt(1 - _e*_e);
  _f = (_a - _b)/ _a;
}

LatLong::LatLong(const LatLong& ll) {
  _lat = ll.get_lat();
  _lon = ll.get_lon();
  _a = ll.get_a();
  _b = ll.get_b();
  _e = ll.get_e();
  _f = ll.get_f();
}


// This code does not consider the case where the distance change moves the
// value into a new UTM zone!

LatLong LatLong::m_left(const double n) const {
  LatLong ret_val(get_lat(), get_lon(), get_a(), get_e());
  double easting;
  double northing;
  double k;
  get_UTM(&easting, &northing, &k);

  // Change the easting and northing by the appropriate ammounts.
  easting -= n;
  // If the translation moves into a new UTM zone, this examination of which
  // zone the point current LatLong is in rather than which zone the return
  // will be in will fail!
  int zone = get_zone();
  ret_val.set_to_UTM(easting, northing, zone);
  return ret_val;
}
  

LatLong LatLong::m_up(const double n) const {
  LatLong ret_val(get_lat(), get_lon(), get_a(), get_e());
  double easting;
  double northing;
  double k;
  get_UTM(&easting, &northing, &k);

  // Change the easting and northing by the appropriate ammounts.
  northing += n;
  // If the translation moves into a new UTM zone, this examination of which
  // zone the point current LatLong is in rather than which zone the return
  // will be in will fail!
  int zone = get_zone();
  ret_val.set_to_UTM(easting, northing, zone);
  return ret_val;
}
  

LatLong LatLong::m_right(const double n) const {
  LatLong ret_val(get_lat(), get_lon(), get_a(), get_e());
  double easting;
  double northing;
  double k;
  get_UTM(&easting, &northing, &k);

  // Change the easting and northing by the appropriate ammounts.
  easting += n;
  // If the translation moves into a new UTM zone, this examination of which
  // zone the point current LatLong is in rather than which zone the return
  // will be in will fail!
  int zone = get_zone();
  ret_val.set_to_UTM(easting, northing, zone);
  return ret_val;
}
  

LatLong LatLong::m_down(const double n) const {
  LatLong ret_val(get_lat(), get_lon(), get_a(), get_e());
  double easting;
  double northing;
  double k;
  get_UTM(&easting, &northing, &k);

  // Change the easting and northing by the appropriate ammounts.
  northing -= n;
  // If the translation moves into a new UTM zone, this examination of which
  // zone the point current LatLong is in rather than which zone the return
  // will be in will fail!
  int zone = get_zone();
  ret_val.set_to_UTM(easting, northing, zone);
  return ret_val;
}
  

LatLong LatLong::km_left(const double n) const {
  LatLong ret_val(get_lat(), get_lon(), get_a(), get_e());
  double easting;
  double northing;
  double k;
  get_UTM(&easting, &northing, &k);

  // Change the easting and northing by the appropriate ammounts.
  easting -= 1000.0 * n;
  // If the translation moves into a new UTM zone, this examination of which
  // zone the point current LatLong is in rather than which zone the return
  // will be in will fail!
  int zone = get_zone();
  ret_val.set_to_UTM(easting, northing, zone);
  return ret_val;
}
  

LatLong LatLong::km_up(const double n) const {
  LatLong ret_val(get_lat(), get_lon(), get_a(), get_e());
  double easting;
  double northing;
  double k;
  get_UTM(&easting, &northing, &k);

  // Change the easting and northing by the appropriate ammounts.
  northing += 1000.0 * n;
  // If the translation moves into a new UTM zone, this examination of which
  // zone the point current LatLong is in rather than which zone the return
  // will be in will fail!
  int zone = get_zone();
  ret_val.set_to_UTM(easting, northing, zone);
  return ret_val;
}
  

LatLong LatLong::km_right(const double n) const {
  LatLong ret_val(get_lat(), get_lon(), get_a(), get_e());
  double easting;
  double northing;
  double k;
  get_UTM(&easting, &northing, &k);

  // Change the easting and northing by the appropriate ammounts.
  easting += 1000.0 * n;
  // If the translation moves into a new UTM zone, this examination of which
  // zone the point current LatLong is in rather than which zone the return
  // will be in will fail!
  int zone = get_zone();
  ret_val.set_to_UTM(easting, northing, zone);
  return ret_val;
}
  

LatLong LatLong::km_down(const double n) const {
  LatLong ret_val(get_lat(), get_lon(), get_a(), get_e());
  double easting;
  double northing;
  double k;
  get_UTM(&easting, &northing, &k);

  // Change the easting and northing by the appropriate ammounts.
  northing -= 1000.0 * n;  
  // If the translation moves into a new UTM zone, this examination of which
  // zone the point current LatLong is in rather than which zone the return
  // will be in will fail!
  int zone = get_zone();
  ret_val.set_to_UTM(easting, northing, zone);
  return ret_val;
}  


/* This returns the UTM zone that a lat/long point would fall in.  Points on
 * the exact edge between UTM zones are assumed to be in the origin of the
 * new zone formed to their right, rather than at the extreme of the zone
 * to their left.
 *
 */
inline int LatLong::get_zone() const {
  return (int)floor(_lon/6.0) + 31;
}
   
inline double LatLong::get_lat() const {
  return _lat;
}

inline double LatLong::get_lon() const {
  return _lon;
}

inline double LatLong::get_a() const {
  return _a;
}

inline double LatLong::get_b() const {
  return _b;
}

inline double LatLong::get_e() const {
  return _e;
}

double LatLong::get_f() const {
  return _f;
}

void LatLong::set_lat(const double lat) { // don't declare the function inline, unable to link
  _lat = lat;
  return;
}

void LatLong::set_lon(const double lon) {
  _lon = lon;
  return;
}


void LatLong::set_latlon(const double lat, const double lon) {
  _lon = lon;
  _lat = lat;
  return;
}

bool LatLong::get_latlong(double* lat, double* lon) {
  // Test for error conditions.
  if (!lat || !lon) {
    // One of the pointers is NULL, cannot give return.
    return false;
  }
  else {
    *lat = _lat;
    *lon = _lon;
    return true;
  }
}



bool LatLong::get_UTM(double* easting, double* northing, double* k) const {
  double lat_0 = 0;
  double k_0 = 0.9996;
  double lon_0 = -177 + (get_zone() - 1) * 6;
  // (fmod(_lon, 6.0) == 0) ? _lon : (_lon - fmod(_lon, 6.0)) - 3;

  lat_0 = (lat_0 / 180.0) * PI;
  lon_0 = (lon_0 / 180.0) * PI;

  // Define variable indicating whether or not the function calculated the
  // returns correctly.
  bool success = true;

  // Test for error conditions.
  if (!easting || !northing) {
    // One of the pointers is NULL, cannot give return.
    success = false;
  }
  else if (_e * _e >= 1) {
    // Violation of class constants and risk of divide by 0 when defining 'ep2'
    // or 'n' below.
    success = false;
  }

  // If not errors tripped, proceed.
  if (success) {
    // Latitude and longitude are needed in radians.
    double lat = (_lat / 180.0) * PI;
    double lon = (_lon / 180.0) * PI;

    

    // Perform intermediate calculations.  Variable names follow Snyder, but
    // are similar to web reference as well.  These variables will compose the
    // final functions for easting, northing, and k.

    double e2 = _e * _e;
    double ep2 = e2 / (1 - e2);
    double N = _a / sqrt(1 - (e2 * pow(sin(lat), 2)));
    double T = pow(tan(lat), 2);
    double C = ep2 * pow(cos(lat), 2);
    double A = (lon - lon_0) * cos(lat);

    // According to Snyder, "M is the true distance along the central meridian
    // from the Equator to [the latitude]."  It is ugly.  It is an infinite
    // series of terms 'sin(2 * n * lat)' (the n = 0 term is instead simply a
    // 'lat' term outside any sin function).  Each coefficient of this infinit
    // series is itself an infinite series featuring even powers of _e.  Each
    // successive series has successively fewwer small powers of _e.  I have
    // not found a generalization for any part of this series, so I have
    // included here the terms given in Snyder.  I have broken M into an
    // expression of its coefficient series for readibility.

    double coeff_series_0 =
      1 - (e2/4) - (3*pow(e2, 2)/64) - (5*pow(e2, 3)/256);
    double coeff_series_2 =
      (3*e2/8) + (3*pow(e2, 2)/32) + (45*pow(e2, 3)/1024);
    double coeff_series_4 = (15*pow(e2, 2)/256) + (45*pow(e2, 3)/1024);
    double coeff_series_6 = 35*pow(e2, 3)/3072;

    // Note that the sign alternates between terms.
    double M = _a * (coeff_series_0 * lat - coeff_series_2 * sin(2 * lat) +
		     coeff_series_4 * sin(4 * lat) -
		     coeff_series_6 * sin(6 * lat));
    double M_0 = _a * (coeff_series_0 * lat_0 -
		       coeff_series_2 * sin(2 * lat_0) +
		       coeff_series_4 * sin(4 * lat_0) -
		       coeff_series_6 * sin(6 * lat_0));
    
    // Now the function is ready to calculate the final values.
    *easting = k_0 * N * (A + ((1 - T) + C) * (pow(A, 3) / 6) +
			    (5 - (18 * T) + pow(T, 2) + (72 * C) - (58 * ep2))
			    * (pow(A, 5)/120));
    // Add in the false easting for the UTM system.
    *easting += 500000;
    
    *northing = ((pow(A, 2)/2 +
		  (pow(A, 4)/24) * (5 - T + (9*C) + (4*pow(C, 2))) +
		  (pow(A, 6)/720) * (61 - (58*T) + pow(T, 2) + (600*C) -
				     (330*ep2))
		  ) * N * tan(lat) + M - M_0) * k_0;

    if (k) {
      *k = k_0 * (1 +
		  (pow(A, 2)/2) * (1+C) +
		  (pow(A, 4)/24) * (5 - (4*T) + (42*C) + (13*pow(C, 2)) -
				    (28*e2)) +
		  (pow(A, 6)/720) * (61 - (148*T) + (16*pow(T, 2))));
    }

    // Debugging statements.
    /*
    cout << "a = " << fixed << _a << "\n";
    cout << "e^2 = " << fixed << e2 << "\n";
    cout << "lat_0 = " << fixed << lat_0 << "\n";
    cout << "lon_0 = " << fixed << lon_0 << "\n";
    cout << "k_0 = " << fixed << k_0 << "\n";
    cout << "lat = " << fixed << (lat * 180) / PI << "\n";
    cout << "lon = " << fixed << (lon * 180) / PI << "\n";
    cout << "e'^2 = " << fixed << ep2 << "\n";
    cout << "N = " << fixed << N << "\n";
    cout << "T = " << fixed << T << "\n";
    cout << "C = " << fixed << C << "\n";    
    cout << "A = " << fixed << A << "\n";
    cout << "M = " << fixed << M << "\n";
    cout << "M_0 = " << fixed << M_0 << "\n";
    cout << "x = " << fixed << *easting << "\n";
    cout << "y = " << fixed << *northing << "\n";
    if (k) {
      cout << "k = " << fixed << *k << "\n";
    }
    cout << "northing diff = " << fixed << *northing - 4484124.4 << "\n";
    cout << "easting diff = " << fixed << *easting - 127106.5 << "\n";
    */
  }
  
  // Return whether or not the function was a success.
  return success;
}


bool LatLong::set_to_UTM(double easting, double northing, int zone) {
  double lat_0 = 0;
  double lon_0 = -177 + 6 * (zone - 1);
  lat_0 = (lat_0 / 180.0) * PI;
  lon_0 = (lon_0 / 180.0) * PI;
  double k_0 = 0.9996;
  double y = northing;
  double x = easting;

  // Subtract the false easting
  x -= 500000;
    
  assert (k_0 != 0);

  
  double e2 = _e * _e;
  double ep2 = e2 / (1 - e2);

  double coeff_series_0 =
    1 - (e2/4) - (3*pow(e2, 2)/64) - (5*pow(e2, 3)/256);
  double coeff_series_2 =
    (3*e2/8) + (3*pow(e2, 2)/32) + (45*pow(e2, 3)/1024);
  double coeff_series_4 = (15*pow(e2, 2)/256) + (45*pow(e2, 3)/1024);
  double coeff_series_6 = 35*pow(e2, 3)/3072;
  
  double M_0 = _a * (coeff_series_0 * lat_0 - coeff_series_2 * sin(2 * lat_0) +
		     coeff_series_4 * sin(4 * lat_0) -
		     coeff_series_6 * sin(6 * lat_0));
  
  double M = M_0 + (y/k_0);
  double u = M / (_a * (1 - (e2/4) - (3*pow(e2, 2)/64) - (5*pow(e2, 3)/256)));
  double e_1 = (1 - sqrt(1 - e2)) / (1 + sqrt(1 - e2));

  double lat_1 = u + sin(2*u) * (3*e_1/2 - 27*pow(e_1, 3)/32) +
    sin(4*u) * (21*pow(e_1, 2)/16) - (55*pow(e_1, 4)/32) +
    sin(6*u) * (151*pow(e_1, 3)/96) + sin(8*u) * (1097*pow(e_1, 4)/512);
    
  double C_1 = ep2 * pow(cos(lat_1), 2);
  double T_1 = pow(tan(lat_1), 2);
  double N_1 = _a / sqrt(1 - (e2 * pow(sin(lat_1), 2)));
  double R_1 = (_a * (1 - e2)) / pow(1 - e2 * pow(sin(lat_1), 2), 1.5);
  double D = x / (N_1 * k_0);

  _lat = lat_1 - (pow(D, 2)/2 -
		  (pow(D, 4)/24) * (5 + (3*T_1) + (10*C_1) - (4*pow(C_1, 2)) -
				    9*ep2) +
		  (pow(D, 6)/720) * (61 + (90*T_1) + (298*C_1) +
				     (45*pow(T_1, 2)) - (252*ep2) -
				     (3*pow(C_1, 2)))) *
    (N_1 * tan(lat_1) / R_1);

  _lon = lon_0 + (D - (pow(D, 3)/6) * (1 + (2*T_1) + C_1) +
		  (pow(D, 5)/120) * (5 - (2*C_1) + (28*T_1) - (3*pow(C_1, 2)) +
				     (8*ep2) + (24*pow(T_1, 2)))) / cos(lat_1);

  // Quick conversion back out of radians to degrees.
  _lat = (_lat * 180) / PI;
  _lon = (_lon * 180) / PI;

  // Debugging statements.
  /*
  cout << "a = " << fixed << _a << "\n";
  cout << "e^2 = " << fixed << e2 << "\n";
  cout << "lat_0 = " << fixed << lat_0 << "\n";
  cout << "lon_0 = " << fixed << lon_0 << "\n";
  cout << "k_0 = " << fixed << k_0 << "\n";
  cout << "x = " << fixed << x << "\n";
  cout << "y = " << fixed << y << "\n";
  cout << "M_0 = " << fixed << M_0 << "\n";
  cout << "e'^2 = " << fixed << ep2 << "\n";
  cout << "M = " << fixed << M << "\n";
  cout << "e_1 = " << fixed << e_1 << "\n";
  cout << "u = " << fixed << u << "\n";
  cout << "lat_1 = " << fixed << lat_1 << "\n";
  cout << "C_1 = " << fixed << C_1 << "\n";    
  cout << "T_1 = " << fixed << T_1 << "\n";
  cout << "N_1 = " << fixed << N_1 << "\n";
  cout << "R_1 = " << fixed << R_1 << "\n";
  cout << "D = " << fixed << D << "\n";
  cout << "lat = " << fixed << _lat << "\n";
  cout << "lon = " << fixed << _lon << "\n";
  */
  return true;
}
  


/*  
int main() {
  cout << "starting test\n";

  double ex_lat;
  double ex_lon;
  double easting = 0;
  double northing = 0;
  double k = 0;
  double ex_nor;
  double ex_eas;
  int ex_zone;

  cout << "\n";
  
  double ex_a = CLARK_66_A;
  double ex_e = CLARK_66_E;
  
  ex_lat = 40.5;
  ex_lon = -73.5;
  cout << "Creating LatLong object, lat = " << fixed << ex_lat << ", lon = "
       << ex_lon << "\n   and defining the datum, a = " << ex_a << ", and e = "
       << ex_e << "\n";
  LatLong clark(ex_lat, ex_lon, ex_a, ex_e);
  clark.get_UTM(&easting, &northing, &k);
  cout << "UTM outputs \'east / north\' are: " << fixed << easting << " / "
       << northing << "\n";
  cout << "\n";
  cout << "This second run confirms leaving k to be NULL is safe\n";
  clark.get_UTM(&easting, &northing);
  cout << "No errors were reported in the previous run\n";
  cout << "\n";
  ex_nor = 4484124.4;
  ex_eas = 627106.5;
  ex_zone = 18;
  cout << "Setting LatLong to a set of UTMs.  Input easting is " << fixed
       << ex_eas << ",\n   input northing is " << ex_nor << ", both for the "
       << "UTM zone " << ex_zone << "\n";
  clark.set_to_UTM(ex_eas, ex_nor, ex_zone);
  cout << "Actual lat/long are: " << fixed << clark.get_lat() << " / "
       << clark.get_lon() << "\n";
  cout << "\n";


  cout << "From here, use a new LatLong created to use the same datum as the\n"
       << "  DOQ maps\n";

  cout << "\n";
  ex_a = NAD83_A;
  ex_e = NAD83_E;

  cout << "Creating LatLong object, lat = " << fixed << ex_lat << ", lon = "
       << ex_lon << "\n   and defining the datum, a = " << ex_a << ", and e = "
       << ex_e << "\n";
  LatLong nad(ex_lat, ex_lon, ex_a, ex_e);
  nad.get_UTM(&easting, &northing, &k);
  cout << "UTM outputs \'east / north\' are: " << fixed << easting << " / "
       << northing << "\n";
  cout << "\n";
  ex_zone = 18;
  cout << "Setting LatLong to a set of UTMs.  Input easting is " << fixed
       << ex_eas << ",\n   input northing is " << ex_nor << ", both for the "
       << "UTM zone " << ex_zone << "\n";
  nad.set_to_UTM(ex_eas, ex_nor, ex_zone);
  cout << "Actual lat/long are: " << fixed << nad.get_lat() << " / "
       << nad.get_lon() << "\n";
  cout << "\n";
  
cout << "From here, use a new LatLong created to use the same datum as the\n"
       << "  DOQ maps\n";

  cout << "\n";

  ex_lat = 34.1875;
  ex_lon = -118.1875;
  cout << "Creating LatLong object, lat = " << fixed << ex_lat << ", lon = "
       << ex_lon << "\n   and defining the datum, a = " << ex_a << ", and e = "
       << ex_e << "\n";
  nad.set_lat(ex_lat);
  nad.set_lon(ex_lon);
  nad.get_UTM(&easting, &northing, &k);
  cout << "UTM outputs \'east / north\' are: " << fixed << easting << " / "
       << northing << "\n";
  cout << "\n";
  ex_nor = 3783583.150;
  ex_eas = 390575.571;
  ex_zone = 11;
  cout << "Setting LatLong to a set of UTMs.  Input easting is " << fixed
       << ex_eas << ",\n   input northing is " << ex_nor << ", both for the "
       << "UTM zone " << ex_zone << "\n";
  nad.set_to_UTM(ex_eas, ex_nor, ex_zone);
  cout << "Actual lat/long are: " << fixed << nad.get_lat() << " / "
       << nad.get_lon() << "\n";
  cout << "\n";

  ex_lat = 35.5;
  ex_lon = -115.3125;
  cout << "Creating LatLong object, lat = " << fixed << ex_lat << ", lon = "
       << ex_lon << "\n   and defining the datum, a = " << ex_a << ", and e = "
       << ex_e << "\n";
  nad.set_lat(ex_lat);
  nad.set_lon(ex_lon);
  nad.get_UTM(&easting, &northing, &k);
  cout << "UTM outputs \'east / north\' are: " << fixed << easting << " / "
       << northing << "\n";
  cout << "\n";
  ex_nor = 3929802.510;
  ex_eas = 653051.873;
  ex_zone = 11;
  cout << "Setting LatLong to a set of UTMs.  Input easting is " << fixed
       << ex_eas << ",\n   input northing is " << ex_nor << ", both for the "
       << "UTM zone " << ex_zone << "\n";
  nad.set_to_UTM(ex_eas, ex_nor, ex_zone);
  cout << "Actual lat/long are: " << fixed << nad.get_lat() << " / "
       << nad.get_lon() << "\n";
  cout << "\n";

  ex_lat = 37.000;
  ex_lon = -118.625;
  cout << "Creating LatLong object, lat = " << fixed << ex_lat << ", lon = "
       << ex_lon << "\n   and defining the datum, a = " << ex_a << ", and e = "
       << ex_e << "\n";
  nad.set_lat(ex_lat);
  nad.set_lon(ex_lon);
  nad.get_UTM(&easting, &northing, &k);
  cout << "UTM outputs \'east / north\' are: " << fixed << easting << " / "
       << northing << "\n";
  cout << "\n";
  ex_nor = 4096106.540;
  ex_eas = 355408.500;
  ex_zone = 11;
  cout << "Setting LatLong to a set of UTMs.  Input easting is " << fixed
       << ex_eas << ",\n   input northing is " << ex_nor << ", both for the "
       << "UTM zone " << ex_zone << "\n";
  nad.set_to_UTM(ex_eas, ex_nor, ex_zone);
  cout << "Actual lat/long are: " << fixed << nad.get_lat() << " / "
       << nad.get_lon() << "\n";
  cout << "\n";

  ex_lat = 34.875;
  ex_lon = -116.1875;
  cout << "Creating LatLong object, lat = " << fixed << ex_lat << ", lon = "
       << ex_lon << "\n   and defining the datum, a = " << ex_a << ", and e = "
       << ex_e << "\n";
  nad.set_lat(ex_lat);
  nad.set_lon(ex_lon);
  nad.get_UTM(&easting, &northing, &k);
  cout << "UTM outputs \'east / north\' are: " << fixed << easting << " / "
       << northing << "\n";
  cout << "\n";
  ex_nor = 3859482.241;
  ex_eas = 574255.412;
  ex_zone = 11;
  cout << "Setting LatLong to a set of UTMs.  Input easting is " << fixed
       << ex_eas << ",\n   input northing is " << ex_nor << ", both for the "
       << "UTM zone " << ex_zone << "\n";
  nad.set_to_UTM(ex_eas, ex_nor, ex_zone);
  cout << "Actual lat/long are: " << fixed << nad.get_lat() << " / "
       << nad.get_lon() << "\n";
  cout << "\n";

  ex_lat = 34.0625;
  ex_lon = -117.5625;
  cout << "Creating LatLong object, lat = " << fixed << ex_lat << ", lon = "
       << ex_lon << "\n   and defining the datum, a = " << ex_a << ", and e = "
       << ex_e << "\n";
  nad.set_lat(ex_lat);
  nad.set_lon(ex_lon);
  nad.get_UTM(&easting, &northing, &k);
  cout << "UTM outputs \'east / north\' are: " << fixed << easting << " / "
       << northing << "\n";
  cout << "\n";
  ex_nor = 3769228.606;
  ex_eas = 448092.106;
  ex_zone = 11;
  cout << "Setting LatLong to a set of UTMs.  Input easting is " << fixed
       << ex_eas << ",\n   input northing is " << ex_nor << ", both for the "
       << "UTM zone " << ex_zone << "\n";
  nad.set_to_UTM(ex_eas, ex_nor, ex_zone);
  cout << "Actual lat/long are: " << fixed << nad.get_lat() << " / "
       << nad.get_lon() << "\n";
  cout << "\n";
  
  return 0;
}

*/
