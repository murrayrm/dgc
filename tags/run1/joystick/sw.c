#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>

#include "sw.h"

/* July 24, 2003. Initial Creation by Johnson Liu. chihhao@its.caltech.edu
 */

static int st_port;		// the paralllel port number that steering wheel is connected to

static float x_center; 		// center values of x-axis
static float y_center; 		// center values of y-axis
static float x_right_range;	// range of x to the right of center
static float x_left_range;  	// range of x to the left of center
static long  count_per_usec;	// how many counts can the system increment per usec
static long  STEER_INPUT_DELAY_COUNT; // use for loop to count to this number as delay

/* call read_steering() to get a reading of x axis, y axis, and buttons
 */
int read_steering(struct steer_struct *st)
{
   long i;
   pp_set_bits(st_port, STEER_X_PIN, STEER_Y_PIN | STEER_BUTTON_PIN);	// poll x axis
   //usleep(STEER_UC_DELAY); usleep sucks, use for loop to delay
   usleep(50);
   //for(i=0; i<STEER_INPUT_DELAY_COUNT; i++);
   st->x=(float) pp_get_data(st_port);//printf("x: %3d ", st->x);   		
   pp_set_bits(st_port, 0, STEER_X_PIN | STEER_Y_PIN | STEER_BUTTON_PIN);
   //usleep(STEER_WAIT);

   pp_set_bits(st_port, STEER_Y_PIN, STEER_X_PIN | STEER_BUTTON_PIN);	// poll y axis
   usleep(50);
   //for(i=0; i<STEER_INPUT_DELAY; i++)i=i;
   st->y= (float) pp_get_data(st_port);//printf("y: %3d ", st->y));   		
   pp_set_bits(st_port, 0, STEER_X_PIN | STEER_Y_PIN | STEER_BUTTON_PIN);
   //usleep(STEER_WAIT);

   pp_set_bits(st_port, STEER_BUTTON_PIN, STEER_X_PIN | STEER_Y_PIN);	// poll buttons
   usleep(50);
   //for(i=0; i<STEER_INPUT_DELAY; i++)i=i;
   st->buttons=~pp_get_data(st_port);   		

   //pp_set_bits(st_port, 0, STEER_X_PIN | STEER_Y_PIN | STEER_BUTTON_PIN);

   return 0;
}

/* when init_steering is called, a thread will be created to execute this function.
 * This function will poll the steering wheel STEER_POLL_NUM number of times and 
 * process the results. If the results are not close enough, there may be an error
 * in reading so we would ignore this set of polling. If the results are close within
 * a range specified by STEER_READ_TOLERANCE, then we take the average of max and min
 * then return it to the system.
 */

void get_steering(struct steer_struct *steer)
{
   struct steer_struct st[STEER_POLL_NUM]; // structures that contain polling results
   int i, max_read_x, min_read_x, max_read_y, x_average, y_average,min_read_y, read_mistake;
   int temp;

   read_mistake=1;
   while(read_mistake==1)
   {

	read_mistake=0;
	for(i=0; i<STEER_POLL_NUM; i++)  // poll steering wheel STEER_POLL_NUM number of times
   	{
      	   read_steering(&st[i]);
           //usleep(STEER_POLL_DELAY);
   	}
	
   	max_read_x= st[0].x;
   	min_read_x= st[0].x;
   	max_read_y= st[0].y;
   	min_read_y= st[0].y;

   	x_average=st[0].x;
   	y_average=st[0].y;

   	for(i=1; i<STEER_POLL_NUM; i++)	// find the max and min for x and y axis polling
   	{	
	   if(st[i].x > max_read_x) max_read_x=st[i].x;   
	   if(st[i].y > max_read_y) max_read_y=st[i].y;   
	   if(st[i].x < min_read_x) min_read_x=st[i].x;   
	   if(st[i].y < min_read_y) min_read_y=st[i].y;
	   if(st[i-1].buttons!=st[i].buttons)  // if button readins are different, we just ignore
           {					// this set of readins
           	//printf("Button Mistake: %d %d ", st[i-1].buttons, st[i].buttons);
               	read_mistake=1;
	       	break;
           }
	   x_average+= st[i].x;
	   y_average+= st[i].y;
    	}

   	if( (max_read_x-min_read_x) <= STEER_READ_X_TOLERANCE && (max_read_y-min_read_y) <= STEER_READ_Y_TOLERANCE && (!read_mistake) && (min_read_x!=0) && (min_read_y!=0))
   	{
	   /* the read values are valid, start processing them.*/

      	   steer->x = x_average/STEER_POLL_NUM; // take the x average
	   steer->y = y_average/STEER_POLL_NUM; // take the y average
	   steer->buttons = st[0].buttons; // get buttons

	   if(steer->buttons == STEER_CALIBRATE) // if all 4 buttons are pressed, calibrate the x and y axis
	   {					// center positions
	   	x_center=steer->x;
	   	y_center=steer->y;
           //printf("Calibrate!!!\n\n");fflush(stdout);
	   }
 	   //printf("Absolute X--%3.0f Y--%3.0f ", steer->x, steer->y);
	   // calculate the relative position of the steering wheel with respect to the center
   	   temp=steer->x-x_center;	

	   // divide by range so that we can return a ratio
   	   steer->x = (temp>0)? (temp/x_right_range): (temp/x_left_range);

   	   if((steer->x)>1) (steer->x)=1;	// if abs(st->x) >1, make abs(st->x)=1
   	   if((steer->x)<-1) (steer->x)=-1;

	   // calculate the y relative position then ratio   
   	   if(steer->y > (y_center-STEER_GAS_SAFE_MARGIN) && steer->y < STEER_BRAKE_UNPRESS)
		steer->y=0;// if the pedals are in the safe margin, we say nothing is pressed
   	   else if (steer->y >= STEER_BRAKE_UNPRESS) 
   	   {
        	// if brake is pressed, brake ratio is returned
        	steer->y = ((STEER_BRAKE_UNPRESS-steer->y)/STEER_BRAKE_RANGE); // if brake is pressed, brake ratio is returned
        	if((steer->y)<-1) steer->y=-1;	// and make sure the ratio is greater than -1
   	   }
   	   else if (steer->y <= (STEER_GAS_UNPRESS-STEER_GAS_SAFE_MARGIN))
   	   {
       		// if gas is pressed, calcaulte gas ratio
       		steer->y = ((STEER_GAS_UNPRESS-STEER_GAS_SAFE_MARGIN-steer->y)/STEER_GAS_RANGE);
       		if((steer->y)>1) steer->y=1;	// and make sure the ratio is less than 1

   	   }
	}
	else	
	{ 
	   usleep(STEER_ERROR_DELAY);
	   read_mistake=1;
	}
   }   
}
    


/* call init_steering to set the parallel port we are using and create a thread
 * to start polling steering wheel
 * ret_val: 0 if no error, -1 if an error occurs.
 */
int init_steering(int port)
{
   int ret_val;
   long loop_counter;
   int loop_index;
   struct timeval init, end;
   long   elapsed=0, total_elapsed=0;
 
   st_port = port;
   x_center= STEER_DEFAULT_X_CENTER;	// set center postition to default
   y_center= STEER_DEFAULT_Y_CENTER;
   x_right_range = STEER_RIGHT_END - STEER_DEFAULT_X_CENTER;	// calculate range for x axis
   x_left_range = STEER_DEFAULT_X_CENTER - STEER_LEFT_END;	// range for y axis is in sw.h
    
   /* execute several loops to benchmark the system*/ 
   for(loop_index=0; loop_index<BUSY_LOOP_NUM; loop_index++)
   {
	//printf("loop_index:%d", loop_index);fflush(stdout);
    	gettimeofday(&init, NULL);
      	for(loop_counter=0; loop_counter<BUSY_LOOP_COUNT; loop_counter++);
    	gettimeofday(&end, NULL);
	elapsed= (end.tv_sec-init.tv_sec)*1000000+(end.tv_usec-init.tv_usec);
	//printf(" elapsed: %ld	", elapsed);
	if(elapsed==0) fprintf(stderr, "BUSY_LOOP_COUNT not big enough!!!\n");
	total_elapsed+=elapsed;
   }
   
   // calculate how many counts the system can increment per microsecond
   count_per_usec= (long) BUSY_LOOP_COUNT/total_elapsed*BUSY_LOOP_NUM;
   //printf("count_per_usec %ld\n", count_per_usec);
   STEER_INPUT_DELAY_COUNT = STEER_INPUT_DELAY * count_per_usec;

   ret_val=pp_init(st_port, PP_DIR_IN); // initialize parallel port
   			
   return   ret_val;
}

/* System should call this function to get reading of steering wheel. The data supplied from 
 * this function is the results of several polls, so it is unlikely that this function 
 * returns an erroneous reading.
 * struct steer_struct *st: defined in sw.h. Use this to pass back reading to system.
 */
/*void get_steering(struct steer_struct *st)
{
   int temp;

   poll_steering(st)
 
   temp=st->x-x_center;
   st->x = (temp>0)? (temp/x_right_range): (temp/x_left_range);// divide by range so that we can return a ratio
   if((st->x)>1) (st->x)=1;	// if abs(st->x) >1, make abs(st->x)=1
   if((st->x)<-1) (st->x)=-1;
   
   if(st->y > (y_center-STEER_GAS_SAFE_MARGIN) && st->y < STEER_BRAKE_UNPRESS)
	st->y=0;// if the pedals are in the safe margin, we say nothing is pressed
   else if (st->y >= STEER_BRAKE_UNPRESS) 
   {
        // if brake is pressed, brake ratio is returned
        st->y = ((STEER_BRAKE_UNPRESS-st->y)/STEER_BRAKE_RANGE); // if brake is pressed, brake ratio is returned
        if((st->y)<-1) st->y=-1;	// and make sure the ratio is greater than -1
   }
   else if (st->y <= (STEER_GAS_UNPRESS-STEER_GAS_SAFE_MARGIN))
   {
       // if gas is pressed, calcaulte gas ratio
       st->y = ((STEER_GAS_UNPRESS-STEER_GAS_SAFE_MARGIN-st->y)/STEER_GAS_RANGE);
       if((st->y)>1) st->y=1;	// and make sure the ratio is less than 1
   }
}*/

/* call end_steer() to end steering.... what else do you think it does?? :)
 */
int end_steering()
{
   return   pp_end(st_port);
}
