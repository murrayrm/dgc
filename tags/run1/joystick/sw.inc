;CheckX		equ	b'00000001'
;CheckY		equ	b'00000010'
;CheckButton	equ	b'00000011'
ButtonMask	equ	b'00001111'

UpdateX			equ	b'00000001'
UpdateY			equ	b'00000010'
UpdateButton	equ	b'00000100'

UpdateXBit		equ	d'0'	; 0th bit of update and PORTA is x
UpdateYBit		equ	d'1'	; 1st bit of update and PORTA is y
UpdateButtonBit	equ	d'2'	; 2nd bit of update and PORTA is button

;physical connection

TRIGGER1	equ	d'0'	; output
TRIGGER2	equ	d'3'	; output

KEY			equ	d'4'	; input

