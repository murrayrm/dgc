/*  static.cc
 *  static map class code
 *
 *  Alan Somers     15-Feb-04   Created
 *  Rocky Velez     20-Feb-04   Modified
 */

#include <memory.h>
#include <math.h>
#include "static.hh"
#include <fstream>
#include <string.h>
#include <iostream>
//Static Map Class functions

/* //Empty Constructor
staticMap::staticMap() {}
*/

/* Constructor<br>
static_map(const VState_GetStateMsg initState, int sizeE, int sizeN, int flags)
Declares and initializes a square map with borders of size meters. 
controls whether the map is populated with static data, or whether it is
initialized blank or filled, and if the corridor is drawn.<br>
*/

//Default Constructor (default size = 500, default flags = 0)
staticMap::staticMap(int LBn=0, int LBe=0, int sizeN=500, int sizeE=500, int flags=0) : __LBn(LBn), __LBe(LBe), __sizeN(sizeN), __sizeE(sizeE) 
{
  __isGood=FALSE;
  __isElev=FALSE;  

  if (flags & G_BLANK_POP){
    allocGood();
    fillMapGood(0);
  }
  else if (flags & G_FILLED_POP){
    allocGood();
    fillMapGood(255);
  }
  else if (flags & G_STATIC_POP){
    allocGood();
    rePopulate(__LBn + __sizeN, __LBe + __sizeE, __LBn, __LBe, G_STATIC_POP);
  }

  if (flags & E_ZERO_POP){
    allocElev();
    fillMapElev(0);
  }
  else if (flags & E_STATIC_POP){
    allocElev();
    rePopulate(__LBn + __sizeN, __LBe + __sizeE, __LBn, __LBe, E_STATIC_POP);
  } 
}


//Copy Constructor
staticMap::staticMap(const staticMap &other){
  __LBn = other.__LBn;
  __LBe = other.__LBe;
  __sizeN = other.__sizeN; 
  __sizeE = other.__sizeE;

 //note:  this is not correct.  need copy contents, not pointers
  __goodnesses = other.__goodnesses;
  __elevations = other.__elevations;
}

//Destructor
staticMap::~staticMap(){
  if(__isGood) delete [] __goodnesses;
  if(__isElev) delete [] __elevations;
}

//dynamic allocators
void staticMap::allocGood(){
  cout << "allocGood()\n";
  __goodnesses = new unsigned char[__sizeN * __sizeE];
  __isGood=TRUE;
}

void staticMap::allocElev(){
  __elevations = new int[__sizeN * __sizeE];
  __isElev=TRUE;
}

//Accessors
int staticMap::getLeftBottomUTMe() const {return __LBe;}
int staticMap::getLeftBottomUTMn() const {return __LBn;}

unsigned char staticMap::getCellGood(int UTMn, int UTMe) const {
  int location;
  location = (UTMn - __LBn) * __sizeE + (UTMe - __LBe);
  return __goodnesses[location];
}

int staticMap::getCellElev(int UTMn, int UTMe) const {
  int location;
  location = (UTMn - __LBn) * __sizeE + (UTMe - __LBe);
  return __elevations[location];
}

//Mutators

void staticMap::fillMapGood(unsigned char color){
  memset(__goodnesses, color, __sizeN * __sizeE);
}

void staticMap::fillMapElev(int elev){
  for(int i=0; i < __sizeN * __sizeE; i++){
    __elevations[i]=elev;
  }
}
void staticMap::setCellGood(int UTMn, int UTMe, unsigned char desiredGoodness){
  int location;
  location = (UTMn - __LBn) * __sizeE + (UTMe - __LBe);
  __goodnesses[location] = desiredGoodness;
}

void staticMap::setCellElev(int UTMn, int UTMe, int desiredElevation){
  int location;
  location = (UTMn - __LBn) * __sizeE + (UTMe - __LBe);
  __elevations[location] = desiredElevation;
}

void staticMap::rePopulate(int northN, int eastE, int southN, int westE, int flags){
  //hardcoding the sample map for now
  cout << "repopulate\n";  
  ifstream infile("sample_goodness.pgm");
  //have to read through the header
  infile.getline((char*)__goodnesses,80);
  infile.getline((char*)__goodnesses,80);
  infile.getline((char*)__goodnesses,80);
  infile.getline((char*)__goodnesses,80);
  for(int i=0; i<__sizeE; i++){
     infile.read((char*)__goodnesses + __sizeN * i, __sizeN);
  }

}

void staticMap::fakePopulate(int fakeLBn, int fakeLBe, int northN, int eastE, int southN, int westE, int flags){
}
#if 0
// goodnesses only, not elevations
void staticMap::drawCircle(double centerN, double centerE, double radius, unsigned char desiredGoodness){
  int spotN, spotE, location,rad;
  double theta;  
  for (rad = 0; rad <= radius; rad++) {
    for (theta = 0; theta <= 2 * M_PI; theta++) {   // this should be fixed...  it won't get everything!
      spotN = int( rad * cos(theta) + centerN);
      spotE = int( rad * sin(theta) + centerE);
      location = spotN * __sizeE + spotE;
      __goodnesses[location] = desiredGoodness;
    }
  }
}
#endif
 void staticMap::drawCircle(double centerN, double centerE, double radius, unsigned char desiredGoodness) {
    if(__isGood == FALSE) allocGood();
    for (int i = int(centerN - radius); i <= int(centerN + radius); i++){
        int w = (int)sqrt((float) (radius*radius - (centerN - i)*(centerN - i)));
        memset(__goodnesses + i*__sizeE + int(centerE) - w, desiredGoodness, 2 * w + 1); } 
}

#if 0
void staticMap::drawRectangle(double centerN, double centerE, double length, double latBdry, double angle, 
			      unsigned char desiredGoodness){
  int spotN, spotE, location, len, d, refN, refE;
  double phi = M_PI_2 - angle;
  for (d = 0; d <= latBdry; d++) {
    refN = int(centerN + float(d) * cos(phi));
    refE = int(centerE + float(d) * sin(phi));
    for (len = 0; len <= length; len++) {
      spotN = len * int(cos(angle)) + refN;
      spotE = len * int(sin(angle)) + refE;
      location = spotN * __sizeE + spotE;
      __goodnesses[location] = desiredGoodness;
    }    
     
    if (d != 0) {
      refN = int(centerN - float(d) * cos(phi));
      refE = int(centerE - float(d) * sin(phi));
      for (len = 0; len <= length; len++) {
	spotN = len * int(cos(angle)) + refN;
	spotE = len * int(sin(angle)) + refE;
	location = spotN * __sizeE + spotE;
	__goodnesses[location] = desiredGoodness;
      }
    }
  }
}
#endif
/*here is a picture of my "rectangle", actually a convex quadrilateral
             1              
            /a\             ------>
           /aaa\            |    X
          4aaaaa\           |
           \bbbbb\          V  Y
            \bbbbb2
             \ccc/
              \c/
               3
The points may be specified in any order, so long as they are clockwise.

this code should work for any parallelogram, and some other quadrilaterals
it will even work for some concave quadilaterals.  it will not work for a
quadrilateral in which opposing points are not at the extents of the figure.
for example, if "2" were below "3" in the above figure, this function would
not work*/
void staticMap::drawRectangle(double centerN, double centerE, double length, double latBdry, double angle, unsigned char desiredGoodness){
    if(__isGood == FALSE) allocGood();
    pixel q1, q2, q3, q4;
    double topN, topE, bottomN, bottomE;
//Calculate 4 corner points
    topN = centerN + length / 2 * sin(angle);
    topE = centerE + length / 2 * cos(angle);
    bottomN = centerN - length / 2 * sin(angle);
    bottomE = centerE - length / 2 * cos(angle);
    q1.N = int(topN + latBdry * sin(angle + M_PI_2));
    q1.E = int(topE + latBdry * cos(angle + M_PI_2));
    q4.N = int(topN + latBdry * sin(angle - M_PI_2));
    q4.E = int(topE + latBdry * cos(angle - M_PI_2));
    q3.N = int(bottomN + latBdry * sin(angle - M_PI_2));
    q3.E = int(bottomE + latBdry * cos(angle - M_PI_2));
    q2.N = int(bottomN + latBdry * sin(angle + M_PI_2));
    q2.E = int(bottomE + latBdry * cos(angle + M_PI_2));
/*    cout << "q1: " << q1.N << "," << q1.E << endl;
    cout << "q2: " << q2.N << "," << q2.E << endl;
    cout << "q3: " << q3.N << "," << q3.E << endl;
    cout << "q4: " << q4.N << "," << q4.E << endl;*/
   //rotate point definitions
    pixel mh, ml;
    pixel top = (q1.N <= q2.N ? q1 : q2);
    top = (top.N <= q3.N ? top : q3);
    top = (top.N <= q4.N ? top : q4);
    if (pix_is_eq(top, q2)) {
            top = q1;   //now top is being used as a temp variable
            q1 = q2; q2 = q3; q3 = q4; q4 = top; }
    else if (pix_is_eq(top, q3)) {
            top = q1;  //top is temp
            q1 = q3;
            q3 = q2;   //q3 is a temp variable
            q2 = q4;
            q4 = q3;   //this is really the old q2 
            q3 = top; }
    else if (pix_is_eq(top, q4)) { 
            top = q1;   //top is temp
            q1=q4; q4=q3; q3=q2; q2=top; }
    if (q4.N <= q2.N) {
        mh = q4; ml = q2;}
    else {
        mh = q2; ml = q4;} 
/*    cout << "q1: " << q1.N << "," << q1.E << endl;
    cout << "q2: " << q2.N << "," << q2.E << endl;
    cout << "q3: " << q3.N << "," << q3.E << endl;
    cout << "q4: " << q4.N << "," << q4.E << endl;*/
   //draw region a
    for (int y=q1.N; y<mh.N; y++) {
        int left_edge=(int)((float)(y-q4.N)*(float)(q1.E-q4.E)/
            (float) (q1.N-q4.N)+(float)q4.E);
        int right_edge=(int)((float)(y-q1.N)*(float)(q1.E-q2.E)/
            (float)(q1.N-q2.N)+(float)q1.E);
/*        cout << "goodnesses: " << __goodnesses << endl;
        cout << "offset: " << y*__sizeN+left_edge << endl;
        cout << "length: " << right_edge - left_edge << endl;*/
            memset(__goodnesses + y * __sizeN+left_edge, desiredGoodness, right_edge-left_edge); }
    //draw region b
    if (q4.N <= q2.N ) {
        for (int y=q4.N; y<q2.N; y++) {
            int left_edge=(int)((float)(y-q3.N)*(float)(q4.E-q3.E)/
                (float) (q4.N-q3.N)+(float)q3.E);
            int right_edge=(int)((float)(y-q1.N)*(float)(q1.E-q2.E)/
                (float)(q1.N-q2.N)+(float)q1.E);
            memset(__goodnesses + y*__sizeN+left_edge, desiredGoodness, right_edge-left_edge); }}
    else {
        for (int y=q2.N; y<q4.N; y++) {
           int left_edge=(int)((float)(y-q4.N)*(float)(q1.E-q4.E)/
                (float) (q1.N-q4.N)+(float)q4.E);
           int right_edge=(int)((float)(y-q2.N)*(float)(q2.E-q3.E)/
                (float)(q2.N-q3.N)+(float)q2.E);
           memset(__goodnesses + y*__sizeN+left_edge, desiredGoodness, right_edge-left_edge); }}
    //draw region c
    for (int y=ml.N; y<q3.N; y++) {
        int left_edge=(int)((float)(y-q3.N)*(float)(q4.E-q3.E)/
            (float) (q4.N-q3.N)+(float)q3.E);
        int right_edge=(int)((float)(y-q2.N)*(float)(q2.E-q3.E)/
            (float)(q2.N-q3.N)+(float)q2.E);
        memset(__goodnesses + y*__sizeN+left_edge, desiredGoodness, right_edge-left_edge); }
    }

//I/O methods
int staticMap::write_file(const char* filename, int flags){
    ofstream outfile(filename);
    outfile << "P5\n" << __sizeE << " " << __sizeN << "\n255\n";
    for(int row=0; row < __sizeN; row++){
        outfile.write((char*)__goodnesses + row * __sizeE , __sizeE-1);
        outfile << endl;
    }
} 
