/**********************************************************************
 **********************************************************************
 * +FILE:        rddf.cc
 *    
 * +DESCRIPTION: rddf class methods
 *
 * 
 **********************************************************************
 *********************************************************************/

#include "rddf.hh"

// Constructor
RDDF::RDDF(){
  loadFile(RDDF_FILE);
}
RDDF::RDDF(char *pFileName){
  loadFile(pFileName);
}
// Destructor
RDDF::~RDDF(){
}

RDDF &RDDF::operator=(const RDDF &other)
{
  numTargetPoints = other.getNumTargetPoints();
  targetPoints = other.getTargetPoints();
  return *this;
}

/****************************************************************************************
 * +DESCRIPTION: return the UTM coordinates of  the waypoint we are looking for
 *    
 * +PARAMETERS 
 *
 *   - fileName      : the rddf file
 *
 * +RETURN: 
 *   - int : 0 if OK
 ***************************************************************************************/
int RDDF::loadFile(char* fileName){
  int count;
  ifstream file;
  string line;
  RDDFData eachWaypoint;

  file.open(fileName,ios::in);
  if(!file){
    cout << "UNABLE TO OPEN THE FILE /team/RDDF/rddf.dat" << endl;
    cout << "PLEASE ADD IN YOUR MAKEFILE:" << endl;
    cout << "( ln -fs $(DGC)/RDDF/rddf.dat $(DGC)/YOUR_DIRECTORY/rddf.dat)" << endl;
    exit(1);
  }
  count = 0;
  while(!file.eof()){
    GisCoordLatLon latlon;
    GisCoordUTM utm;

    // Number
    getline(file,line,RDDF_DELIMITER);
    eachWaypoint.number = atoi(line.c_str());
    // Latitude
    getline(file,line,RDDF_DELIMITER);
    latlon.latitude = atof(line.c_str());
    
    // Longitude
    getline(file,line,RDDF_DELIMITER);
    latlon.longitude = atof(line.c_str());

    // Converting to UTM
    if(!gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL)){ 
      cerr << "Error converting coordinate " << eachWaypoint.number << " to utm\n";
      exit(1); 
    }
    eachWaypoint.Northing = utm.n;
    eachWaypoint.Easting = utm.e;

    // Boundary offset and radius
    getline(file,line,RDDF_DELIMITER);
    eachWaypoint.offset = atof(line.c_str())*meters_per_foot;
    eachWaypoint.radius = eachWaypoint.offset;
    // Speed limit
    getline(file,line,RDDF_DELIMITER);
    eachWaypoint.maxSpeed = atof(line.c_str())*mps_per_mph;
    // Hour
    getline(file,line,RDDF_DELIMITER);
    if(line.find("#") != string::npos){
      eachWaypoint.hour = 0;
    }
    else{
      eachWaypoint.hour = atoi(line.c_str());
    }
    // Minute
    getline(file,line,RDDF_DELIMITER);
    if(line.find("#") != string::npos){
      eachWaypoint.min = 0;
    }
    else{
      eachWaypoint.min = atoi(line.c_str());
    }
    // Second
    file >> line;
    if(line.find("#") != string::npos){
      eachWaypoint.sec = 0;
    }
    else{
      eachWaypoint.sec = atoi(line.c_str());
    }

    if(count == eachWaypoint.number){
      targetPoints.push_back(eachWaypoint);
      count++;
    }
  }
  numTargetPoints = count;
  // debug
  //cout << "RDDF: " << numTargetPoints << " target points read !!!" << endl;
  return(0);
}

/*****************************************************************************/
/***************************************************************************** 
 * + DESCRIPTION: 
 *     Given a waypoint description and position of the vehicle,
 *     determine and return the index of the current waypoint.
 * + ARGUMENTS: 
 *     - RDDFVector: a list of waypoints 
 *     - Northing  : Northing of the vehicle 
 *     - Easting   : Easting of the vehicle
 * + RETURN:
 *     - int: index of the current waypoint 
 *            (the one that we should be going to)
 *             
 *****************************************************************************/
/*****************************************************************************/
int RDDF::getCurrentWaypointNumber( double Northing, double Easting )
{
  // set the current position vector
  Vector curPos( Northing, Easting );
  // declare waypoint vectors
  Vector curWay, nextWay;

  // minimum found distance to waypoint
  double minDist = 123456789;
  int minDistWay = 0;
  
  // cycle through all of the waypoints
  for( int i = 0; i < numTargetPoints-1; i++ ) 
  {
    curWay.N = targetPoints[i].Northing;
    curWay.E = targetPoints[i].Easting;
    nextWay.N = targetPoints[i+1].Northing;
    nextWay.E = targetPoints[i+1].Easting;
 
    // check to see if we are within the corridor segment
    // if we are, just return with the index of the segment
    if( withinCorridor( curPos, curWay, nextWay, targetPoints[i].radius ) ) 
    {
      printf("------ I am within corridor segment %d -------\n", i ); 
      return i;
    }

    // if this waypoint is closer than any other we've found,
    // then remember so 
    if( distVector(curPos, curWay) < minDist ) 
    {
      minDist = distVector(curPos, curWay); 
      minDistWay = i;
    }
  } 
  printf("------ I am NOT within the corridor -------\n" ); 
  
  // if we get to this point in the function, it means that we are
  // **not** within the corridor.  In this case, return the index of
  // the waypoint that is closest to us
  return minDistWay;
}

/****************************************************************************************
 * +DESCRIPTION: saves the file in the bob format for matlab plots
 *               PROBLEM WITH PRECISION .....
 * +PARAMETERS 
 *
 *   - fileName : file name
 *
 * +RETURN: 
 *   - int : 0 if OK
 ***************************************************************************************/
int RDDF::createBobFile(char* fileName){
  ofstream file;
  int ind=0;
  file.open(fileName);
  if(!file){
    cout << "UNABLE TO OPEN THE FILE" << endl;
    return(BAD_BOB_FILE);
  }
  file << "# This is a Bob format waypoint specification file.\n";
  file << "# The column format here is: \n";
  file << "# 1. character [ignored]\n";
  file << "# 2. Easting[m]\n";
  file << "# 3. Northing[m] \n";
  file << "# 4. lateral_offset[m] \n";
  file << "# 5. speed_limit [m/s] \n";
  file << "# NOTE THAT THESE ARE IN SI AS OPPOSED TO THE RDDF SPEC.\n#\n";
  for(ind=0;ind < numTargetPoints;ind++){

    file << "N   " << targetPoints[ind].Northing << " " << targetPoints[ind].Easting 
         << " " << targetPoints[ind].offset << " " << targetPoints[ind].maxSpeed << "\n"; 
  }
  return(0);
}
