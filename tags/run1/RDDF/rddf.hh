/**********************************************************************
 **********************************************************************
 * +FILE:        rddf.hh 
 *    
 * +DESCRIPTION: class to read the rddf file and return the waypoints
 *
 * +AUTHOR     : Thyago Consort 
 **********************************************************************
 *********************************************************************/

#ifndef __RDDF_HH__
#define __RDDF_HH__

#include <stdio.h>
#include <string.h>
#include <iostream.h>
#include <fstream.h>
#include <math.h>
#include <vector>
#include "Constants.h"
#include "ggis.h"
#include "global_functions.hh" // for withinWaypoint, ...

#define RDDF_DELIMITER ','
#define BAD_BOB_FILE -1

using namespace std;

// waypoints
typedef struct RDDFData{
  int number;       // number of the wayoint

  double Northing;
  double Easting;

  double maxSpeed;  // speed limit m/s
  double offset;    // boundary offset of the corridor to the next waypoint
  double radius;    // waypoint radius

  double hour;      // phase line - hour
  double min;       // phase line - min
  double sec;       // phase line - sec

  void display(){
    printf("N=%d North=%10.3f East=%10.3f VEL=%.2f Off= %.2f R=%.2f hour=%d min=%d sec=%d\n",
		    number,Northing,Easting,maxSpeed,offset,radius,hour,min,sec);
  }
};

typedef std::vector<RDDFData> RDDFVector;

class RDDF {
private:

  int numTargetPoints;
  RDDFVector targetPoints;

public:

  // Constructors
  RDDF();
  RDDF(char*pFileName);
  RDDF(const RDDF &other) {numTargetPoints = other.getNumTargetPoints(); targetPoints = other.getTargetPoints(); }
  // Destructor
  ~RDDF();

  RDDF &operator=(const RDDF &other);
  // Accessors
  int getNumTargetPoints() const {return numTargetPoints; }
  RDDFVector getTargetPoints() const {return targetPoints; }

  // Method to automatically find the current waypoint 
  // (the one that we are going to)
  int getCurrentWaypointNumber( double Northing, double Easting );
  
  // Save the file at the bob format
  int createBobFile(char*);

  // Load data from file
  int loadFile(char*);
};

#endif //__RDDF_HH__
