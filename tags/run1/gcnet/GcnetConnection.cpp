/* C++ Wrapper around gcnet.h */

#include "GcnetConnection.hpp"
#include "gcnet.h"

GcnetConnection::GcnetConnection()
{
    gcnet = 0;
}
GcnetConnection::~GcnetConnection()
{
    if (gcnet)
        gcnet_destroy (gcnet);
    gcnet = 0;
}

bool
GcnetConnection::finish_init()
{
    if (!gcnet)
        return false;

    gcnet_set_receive_handler (gcnet, receive_func, (void*) this);
    return true;
}

void
GcnetConnection::receive_func(const void *data, unsigned length, void *user_data)
{
    GcnetConnection *conn = (GcnetConnection *) user_data;
    conn->handle_incoming(data, length);
}

bool
GcnetConnection::connect(const char *hostname, unsigned port)
{
    if (gcnet)
        return false;
    gcnet = gcnet_new_client (hostname, port);
    return gcnet ? true : false;
}

bool
GcnetConnection::listen(unsigned port)
{
    if (gcnet)
        return false;
    gcnet = gcnet_new_server (port);
    return gcnet ? true : false;
}

void
GcnetConnection::transmit(const void *data, unsigned length, bool reliable)
{
    if (gcnet)
        gcnet_transmit (gcnet, data, length, reliable);
}

void
GcnetConnection::handle_incoming(const void *data, unsigned length)
{
    /* This is a stub implementation. */
    (void) data;		// suppress warnings
    (void) length;		// suppress warnings
}
