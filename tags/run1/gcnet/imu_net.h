/* This structure contains the IMU data that needs to be transmitted
 * (needs to be less that a few kilobytes
 * due to UDP packet length restrictions).
 */
typedef struct
{
  double dvx;
  double dvy;
  double dvz;
  double dtx;
  double dty;
  double dtz;
  int frame;
  int filler;
} IMUReading;
