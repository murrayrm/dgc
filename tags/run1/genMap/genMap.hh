/* This is the generic map class designed to be used for path evaluation by 
 * LADAR and stereo vision. Most methods are defined in genMap.cc.
 * Author: Adam Craig, Sue Ann Hong, Thyago Consort
 * Revision History:  1/15/2004  Created
 */

#ifndef __GENMAP_HH__
#define __GENMAP_HH__

#include<stdio.h>
#include <map>
#include <vector>
#include <algorithm>
#include <math.h>
#include <time.h>
#include <list>
#include <MTA/Misc/Time/Timeval.hh>
#include "Constants.h"
#include "vblib.h" // string class
#include <rddf.hh>
#include <float.h>
#include "vehlib/VState.hh"
#include "RaceModules/Arbiter/SimpleArbiter.hh"

// Map color
#define ESCAPE 27
#define RED_COLOR "[31m"
#define RED_INDEX 2
#define BLUE_COLOR "[34m"
#define BLUE_INDEX 1
#define GREEN_COLOR "[32m"
#define GREEN_INDEX 3
#define DEFAULT "[0m"

// Fail returns
#define BAD_MAT_FILE -20
#define BAD_MAT_FILE_FORMAT -21

// Cell types
#define NORMAL_CELL 0
#define OUTSIDE_CORRIDOR_CELL 1
#define NO_DATA_CELL_TYPE 2
#define NO_DATA_CELL_VAL 0.0
#define DEFAULT_CELL_VAL 0.0
#define OUTSIDE_CORRIDOR_VAL -1             // Needs a permanent value

// Max Path
#define MAX_PHI M_PI
#define NUM_ROWS 100
#define NUM_COLS 100
#define ROW_RES 0.2
#define COL_RES 0.2

// Velocity shaping
#define PERCENT_MAX_VEL 100 // percentage
#define PERCENT_MIN_VEL 50  // percentage

// Goodness shaping
#define MAX_GOODNESS_SHAPE 100
#define MIN_GOODNESS_SHAPE 20
#define MAX_OBSTACLE_AVOIDANCE_GOODNESS 100
#define MIN_OBSTACLE_AVOIDANCE_GOODNESS 95
#define LOW_GOODNESS_THRESHOLD 20
#define LEFT_SIDE -1
#define RIGHT_SIDE 1

// Vetor Field shaping
#define VECTOR_FIELD_WEIGHT 2.0
#define DELTA_SMALL 0.0001

// Shrinking obstacles
#define MIN_EXP_SHRINK_PARAMETER 0.05
#define MAX_EXP_SHRINK_PARAMETER 0.1

// How many times the map size do we have to search for new waypoints
#define ENLARGE_MAP_HOW_MANY_TIMES 1.0

// Growing the vehicle
#define GROW_VEHICLE_LENGTH 1
#define GROW_VEHICLE_WIDTH 1

//Presumed difference in height from one cell to the next when no data is available.
#define UNKNOWN_CELL_HEIGHT_DIFF 0.05
#define RADIUS_OF_SEARCHING_FOR_DATA 1.0 //meters to check North, East of point to check for cell data for height difference in allCellsUnder.
#define MAX_NUMBER_CELLS_TO_CHECK 10  
using namespace std;

typedef char *charptr;

// waypoints
typedef struct WPdata{
  double northing;
  double easting;
  double offset;
};

typedef std::list<WPdata> WP;
typedef std::pair<int, int> RowCol;

// Used a lot on the new obstacle avoidance
typedef struct NorthEastDown {
  int row,col;
  double Northing;
  double Easting;
  double Downing;

  // Constructor
  NorthEastDown(double pNorth=0.0,double pEast=0.0, double pDown=0.0,
		int bottomLeftRow = 0, int bottomLeftCol = 0,
		double rowRes = 1.0, double colRes = 1.0, int numRows = 1,
		int numCols = 1){ 
    Northing = pNorth;
    Easting = pEast;
    Downing = pDown;
    fix_RC(bottomLeftRow, bottomLeftCol, rowRes, colRes, numRows, numCols);
  }

  // Copy Constructor
  NorthEastDown(const NorthEastDown &other) { 
    Northing = other.Northing;
    row = other.row; 
    Easting = other.Easting;
    col = other.col;
    Downing = other.Downing;
  }

  // Assignment Operator
  NorthEastDown &operator=(const NorthEastDown &other) { 
    if (this != &other) {
      Northing = other.Northing;
      Easting = other.Easting;
      Downing = other.Downing;
      row = other.row;
      col = other.col;
    }
    return *this;
  }

  // + and - operators
  NorthEastDown operator+ (NorthEastDown param) {
    NorthEastDown temp;
    temp.Northing = Northing + param.Northing;
    temp.Easting = Easting + param.Easting;
    temp.Downing = Downing + param.Downing;
    return (temp);
  }

  NorthEastDown operator- (NorthEastDown param) {
    NorthEastDown temp;
    temp.Northing = Northing - param.Northing;
    temp.Easting = Easting - param.Easting;
    temp.Downing = Downing - param.Downing;
    return (temp);
  }

  // Produtc by a constant
  NorthEastDown operator* (double param){
    NorthEastDown ret;
    ret.Northing = Northing*param;
    ret.Easting = Easting*param;
    ret.Downing = Downing*param;
    return (ret);
  }

  // Internal product operator
  double operator* (NorthEastDown param){
    double ret;
    ret = (Northing * param.Northing) + (Easting * param.Easting) + (Downing * param.Downing);
    return (ret);
  }

  // Vectorial product operator
  NorthEastDown operator^ (NorthEastDown param){
    NorthEastDown temp;
    temp.Northing = (Easting * param.Downing) - (Downing * param.Easting);
    temp.Easting = (Downing * param.Easting) - (Northing * param.Downing);
    temp.Downing = (Easting * param.Easting) - (Easting * param.Easting);
    return (temp);
  }

  // Norma
  void norm(){
    double modulus = 0;
    modulus = sqrt(Northing*Northing + Easting*Easting + Downing*Downing);
    if(modulus){
      Northing /= modulus;
      Easting /= modulus;
      Downing /= modulus;
    }
  }

  void fix_RC(int bottomLeftRow, int bottomLeftCol, double rowRes, double colRes, int numRows, int numCols){
    row = (bottomLeftRow + (int)floor(Northing/rowRes))%numRows;
    col = (bottomLeftCol + (int)floor(Easting/colRes))%numCols;
  }

// Display
  void display(){
    printf("(Northing,Easting,Downin) = (%10.2f,%10.2f,%10.2f)\n",Northing,Easting,Downing);
  }
  
  // Destructor
  ~NorthEastDown() {}
};


/** 
 * CellData: struct to store attributes of a cell in the map.
 *  'value' as height is positive for positive obstacles, unlike how it's
 *  handled in other modules like state, sv, etc. Note this when generating
 *  genMaps.
 **/
typedef struct CellData {
  double value;
  double timeStamp; //second
  int color;
  int type;
  list<double> cellList;

  // Constructor ------------------------ ZERO???
  CellData(double val=DEFAULT_CELL_VAL, int cl=0, int tp=NORMAL_CELL, int time=0) :
    value(val), color(cl), type(tp), timeStamp(time) {}
  // Copy Constructor
  CellData(const CellData &other) { 
    value = other.value; 
    color = other.color; 
    type = other.type; 
    timeStamp = other.timeStamp; 
    cellList = other.cellList;
  }
  // Assignment Operator
  CellData &operator=(const CellData &other) { 
    if (this != &other) {
      destroy();
      value = other.value; 
      color = other.color; 
      type = other.type; 
      timeStamp = other.timeStamp; 
      cellList = other.cellList;
    }
    return *this;
  }
  // Destructor
  ~CellData() {
    destroy();
  }
  void destroy() {
    cellList.clear();
  }
};

// Change the NO_DATA_CELL value to something big?!!!
static const struct CellData NO_DATA_CELL = CellData(NO_DATA_CELL_VAL, 0, NO_DATA_CELL_TYPE);
//static const struct CellData NO_DATA_CELL = CellData(NO_DATA_CELL_VAL,
//						     BLUE_INDEX, NORMAL_CELL);
static const struct CellData INSIDE_CELL = CellData(DEFAULT_CELL_VAL);
static const struct CellData OUTSIDE_CELL = CellData(OUTSIDE_CORRIDOR_VAL, 
						     GREEN_INDEX, 
						     OUTSIDE_CORRIDOR_CELL);


//-------genMap--------------------------------------------------------------//

class genMap {
private:
  typedef std::map< RowCol, CellData > MapData;
  typedef std::map< RowCol, CellData >::iterator MapIterator;

  /* theMap: holds the CellData values for all cells.
   *        (I'm running out of names that mean map.)
   *  (0,0) points to the bottom left of the map (note that that is
   *   not necessarily equal to the left bottom of the current window
   *   represented by leftBottomUTM_Northing and leftBottomUTM_Easting.)
   */
  MapData theMap;
  double curTimeStamp;

  RDDF WaypointList;
  int lastWaypoint;
  int nextWaypoint;

  // Thresholds for path evaluation
  double obstacleThreshold;
  double heightDiffThreshold;
  double tireOffThreshold;
  double vectorProdThreshold;

  // Growing the vehicle
  double growVehicleLength;
  double growVehicleWidth;

  // Path evaluation criteria
  bool useCellsUnderCost;
  bool useDotVectorCost;
  bool useOffTireCost;
  bool useMaxHeightDiffCost;
  bool useMaxInclinationCost;
  bool useObsAvoidanceVelocities;
  bool useMaxHeightDiffCostRectangle;
  bool useConfinedSpaceGoodnessVotes;

  bool evalCorridor;           // If TRUE the corridor will be painted
  bool activateNoDataCell;     // IF TRUE the no data cell will NOT represent a valiad height

  double pathMaxDistance;
  int numRows;                 // The total number of rows (for cells) in map
  int numCols;                 // The total number of cols (for cells) in map
  VState_GetStateMsg curState; //The current X, Y position and velocity, pitch,
                               // yaw, timestamp and other state information
                               // of the vehicle 
  double rowRes;               // The resolution of the x-axis in m
  double colRes;               // The resolution of the y-axis in m
  int vehRow;                  // The x-index of vehicle position
  int vehCol;                  // The y-index of vehicle position
  double leftBottomUTM_Northing;       // UTM coord of the bottom left corner of the
                               // bottom left cell in current window
  double leftBottomUTM_Easting;       // UTM coord of the bottom left corner of the
                               // bottom left cell in current window
  int leftBottomRow;     // row of the left bottom of the window
  int leftBottomCol;     // col of the left bottom of the window
  
  double DataLifespan;
  //allCellsUnderCost checks this far or until it finds a cell with data in it around some other cell.
  int NUMBER_OF_COLS_RIGHT_TO_CHECK;
  int NUMBER_OF_CELLS_DIAGONAL_TO_CHECK;
  int NUMBER_OF_ROWS_DOWN_TO_CHECK;

public:
  // Constructors
  // Empty constructor
  genMap(bool init=false);
  // Constructor with default values (except initState)
  genMap(VState_GetStateMsg initState, int num_rows, int num_cols, 
	 double row_res, double col_res, bool pEvalCorridor=false, double data_lifespan=1000);
  // Copy Constructor
  genMap(const genMap &other);
  // Assignment Operator
  genMap &operator=(const genMap &other);

  // Destructor
  ~genMap() {
    destroy();
  }
  void destroy() {
    theMap.clear();    // Erase all the map data
  }

  // Set genMap, call just once
  void initMap(int num_rows, int num_cols, double row_res, double col_res,
	       bool pEvalCorridor=false, bool pActivateNoDataCell=false, double data_lifespan=1000);

  // Set path evaluation thresholds
  void initThresholds(double pObstacleThreshold=OBSTACLE_THRESHOLD, 
                      double pHeightDiffThreshold=HEIGHT_DIFF_THRESHOLD,
                      double pTireOffThreshold=TIRE_OFF_THRESHOLD,
                      double pVectorProdThreshold=VECTOR_PROD_THRESHOLD,
                      double pGrowVehicleLength=GROW_VEHICLE_LENGTH,
                      double pGrowVehicleWidth=GROW_VEHICLE_WIDTH);
  void displayThresholds();

  // Set path evaluation cost to be used
  void initPathEvaluationCriteria(bool pUseCellsUnderCost=false, 
				  bool pUseOffTireCost=false,
				  bool pUseDotVectorCost=false,
				  bool pUseMaxHeightDiffCost=true,
				  bool pUseMaxInclinationCost=false,
                                  bool pUseObsAvoidanceVelocities=false,
                                  bool pUseMaxHeightDiffCostRectangle=false,
                                  bool pUseConfinedSpaceGoodnessVotes=false);

  // Accessors
  int getNumRows();
  int getNumCols();
  double getRowRes();
  double getColRes();
  int getVehRow();
  int getVehCol();
  double getLeftBottomUTM_Northing();
  double getLeftBottomUTM_Easting();
  int getLeftBottomRow();
  int getLeftBottomCol();
  double getPathMaxDistance();
  int getLastWaypoint();
  int getNextWaypoint();
  int getNumTargetPoints();
  RDDFVector getTargetPoints();

  //Mutators
  void setLastWaypoint(int);
  void setNextWaypoint(int);

  //Function for serializing a genMap for transmission over the network
  int serializedMapSize();
  int serializeMap(char* buffer, int bufferSize);

  /* The following accessors return the (value of the) whole CellData struct
   * of the designated cell. They also perform obstacle shrinking.
   */
  CellData getCellDataRC(int rowNumber, int colNumber);
  CellData getCellDataUTM(double Northing, double Easting);
  CellData getCellDataFrameRef(double dist_ahead, double dist_aside);
  
  /*The following accessors only return the unshrunken values of data 
   *with timestamps from after custate - DataLifespan.
   */
  CellData getRecentCellDataRC(int rowNumber, int colNumber);
  CellData getRecentCellDataUTM(double Northing, double Easting);
  CellData getRecentCellDataFrameRef(double dist_ahead, double dist_aside);


  // Mutators
  /* The following mutators replace the whole CellData struct of the designated
   * cell with the given newCD.
   */
  void setCellDataRC(int rowNumber, int colNumber, struct CellData newCD);
  void setCellDataUTM(double Northing, double Easting, struct CellData newCD);
  void setCellDataFrameRef(double dist_ahead, double dist_aside, 
			   struct CellData newCD);
  /* The following mutators add the given list_val to the cellList of the
   * CellData of the designated cell, but doesn't change anything else in its
   * CellData. 
   */
  void setCellListDataRC(int rowNumber, int colNumber, double list_val);
  void setCellListDataUTM(double Northing, double Easting, double list_val);
  void setCellListDataFrameRef(double dist_ahead, double dist_aside, 
			       double list_val);

  // Map frame manipulation methods
  /* updateFrame(): Updates curState, vehRow, vehCol, leftBottomUTM_Northing, 
   *                leftBottomUTM_Easting, and clears out the data inside cells now
   *                "new" in the updated map.
   * Calls: floor in math.h, clearRow, clearCol, and Copy_Vstate. 
   */
  void updateFrame(struct VState_GetStateMsg newState, bool updateWindow=true, double nextTimeStamp=0,
		   int pLastWaypoint=0, int pNextWaypoint=0);
  /* evaluateCells()
   *  Sets each cell whose elevList is not smaller than numMinPoints
   *  to the maximum of the elements in the list, under 3.3m
   *  and then clears the list
   */

  /* setCurrentWaypoints(): Uses the vehicle location from curState and 
   * the WaypointList to find the correct values of lastWaypoint and 
   * nextWaypoint.
   */
  void setCurrentWaypoints();

  void evaluateCellsSV(int numMinPoints, double verticalLimit);

  /* void clearMap() 
   *  Uses setCellDataRC to set all cells in the map to NO_DATA_CELL.
   */
  void clearMap();
  /* void clearColor(int colorIndex)
   *  Just set the map to black and white
   */
  void clearColor(int colorIndex);
  /* void clearRow( int row )
   *  Uses setCellDataRC to set all cells in specified row to NO_DATA_CELL. 
   */
  void clearRow( int row );
  /* void clearCol( int col )
   *  Uses setCellDataRC to set all cells in specified col to NO_DATA_CELL. 
   */
  void clearCol( int col );

  // Path Evaluation Methods
  //*******************************//
  //**METHODS FOR PATH EVALUATION**//
  //*******************************//

  /*** Given a distance, obstacle flag, and min distance on all arcs, this
       returns a velocity vote ****/
  double getVelocityVote(double,bool,double);
  double genMap::getVelocityCurveSlope();

  /* shrinkObstacle
   *This functions shrinks the obstacles in an exponectial way
   */
  double shrinkObstacle(double, double);

  /* goodnessShape
   *This functions returns a goodness of a given steering angle
   * for the votes based on the vector field to the next waypoint
   */
  double goodnessShape(int, int, int, double);

  /* obstacleGoodnessShape
   *This functions returns a goodness of a given steering angle
   * for the votes based on the obstacle avoidance votes
   */
  double obstacleGoodnessShape(int, int);

  /* velShape
   * This function shape velocity of the votes in order to decrease the desired velocity as
   * a function of the steering wheel from MAX% (straight) to MIN% (most angle to left or right)
   * as a quadratic function
   */
  double velShape(int,int,double);

  /* costShape
   * This function shape the cost in order to have more resoluition nearby 100
   * rarther than 0 goodness
   */
  double costShape(double,double);

  /* findMaxHeightinRectangle: Given three pairs of (x, y) coordinates, finds
   * the maximum height value that occurs in the rectangle they define.
   * It requires that x1 be the lowest x coordinate, x4 the highest, and that
   * y2 be the lowest y coordinate and y3 the highest. 
   * Otherwise, it returns one million and prints an error message.
   */
  double findMaxHeightinRectangle(double x1, double y1, double x2, double y2,
				  double x3, double y3, double x4, double y4);

  // Evaluate the R^3 normal vector from a plane
  /* NorthEastDown evalNormalVector(NorthEastDown,NorthEastDown,NorthEastDown)
   *  This method gets a steering wheel angle as input, calculates the path
   *  the car follows on that steering angle, and evaluates the distance to
   *  the next dangerous obstacle
   */
  NorthEastDown evalNormalVector(NorthEastDown,NorthEastDown,NorthEastDown);

  // Evaluate the cost based on the normal vectors
  /* double evalNormalCost(NorthEastDown,NorthEastDown,NorthEastDown,NorthEastDown);
   *  This method gets the 4 normal vectors and evaluate a cost associated
   *  normilized from 0 to 100
   */
  double evalNormalVectorCost(NorthEastDown*);

  // Evaluate a simple distance
  /* double distancePoint2Plane(NorthEastDown,NorthEastDown,NorthEastDown,NorthEastDown);
   *  This method evaluates the distance between a point and a plane
   */
  double distancePoint2Plane(NorthEastDown,NorthEastDown,NorthEastDown,NorthEastDown);

  // Evaluate the cost based on the distances between the 4th tire and the plane of the other three on the ground
  /* double evalOffTireCost(double,double,double,double);
   *  This method gets the 4 distances between the plane formed by three tires and the other one
   *  and evaluate a cost associated, normilized from 0 to 100
   */
  double evalOffTireCost(double*);

  // Given a heading and the orign of the car return a map of the cells under the car
  /* map<RowCol,RowCol> allCellsUnder(RowCol,double);
   *  This method returns a list of cells with no repetition that is under the car given the position and yaw
   */
  double allCellsUnderCost(RowCol,double);

  // With all the cells under the vehicle, returns a normilized cost
  /* double maxHeightDiffCost();
   *  This method returns a cost evaluated from the max height difference between ANY cells under the car
   */
  double maxHeightDiffCost(RowCol,double);

  // With all the cells under the vehicle, returns a normilized cost
  /* double maxHeightDiffCostRectangle();
   *  This method returns a cost evaluated from the max height difference between ANY cells under the car, but in a different way
   */
  double maxHeightDiffCostRectangle(RowCol,double);

  // With all the cells under the vehicle, returns a normilized cost
  /* double maxInclinationCost();
   *  This method returns a cost evaluated from the max cos formed by the highest cell, the lowest cell and the ground
   */
  double maxInclinationCost(RowCol,double);

  // Evaluate a simple distance
  /* double distancePoint2Line(NorthEastDown,NorthEastDown,NorthEastDown);
   *  This method evaluates the distance between a point and a line and verify if the 
   *  projection of the off line point crosses the line
   */
  pair<bool,double> distancePoint2Line(NorthEastDown,NorthEastDown,NorthEastDown);

  /* double evalPathCost()
   *  This method gets a steering wheel angle as input, calculates the path
   *  the car follows on that steering angle, and evaluates the path goodness
   *  based on difference of height.
   */
  double evalPathCost(double steer);

  // Path Evaluation Methods 2
  /* double obstacleAvoidance()
   *  This method gets a steering wheel angle as input, calculates the path
   *  the car follows on that steering angle, and evaluates the path goodness
   *  based on difference of height.
   */
  pair<double,bool> obstacleAvoidance(double);

  // translade and rotate coordinates 
  void changeCoordinates(NorthEastDown &,NorthEastDown &,NorthEastDown &,NorthEastDown &,int &);

  /* generateVotes: Evaluates all paths given by the input array and outputs
   *                the vote array to be sent to the arbiter.
   */
  vector< pair<double,double> > generateArbiterVotes(int numArcs, vector<double> steerAngleArray);

  /* generateVotes: Evaluates all paths given by the input array and outputs
   *                the vote array to be sent to the arbiter.
   */
  vector<double> generateVotes(int numArcs, vector<double> steerAngleArray);


  // Frame Transformation Methods
  /* pair<Northing,Easting> RowCol2UTM: Returns the UTM coordinates from a 
   *                       row/col pair
   */
  pair<double,double> RowCol2UTM(RowCol);
  /* RowCol UTMtoRowCol: Finds the row and col for the cell containing the
   *                       input UTM coord
   */
  RowCol UTM2RowCol(double Northing, double Easting);
  /* RowCol RowColFromDeltaRowCol()
   *  Evaluate the new indices inside the map given the actual indices and the
   *  indices displacement.
   */
  RowCol RowColFromDeltaRowCol(RowCol oldPos, RowCol deltaPos);
  /* RowCol RowColFromDeltaPosition()
   *  Evaluate the new indices inside the map given the actual indices and the
   *  x and y displacement in meter
   */
  RowCol RowColFromDeltaPosition(RowCol oldPos, double x_dist, double y_dist);
  /* RowCol FrameRef2RowCol()
   *  This method is to evaluate the new RowCol in function of displacements in
   *  the frame reference
   */
  RowCol FrameRef2RowCol(RowCol refPos,double pYaw, double delta_ahead, 
			 double delta_aside);


  /***********************************/
  /***METHODS FOR CORRIDOR PAINTING***/
  /***********************************/

  /*
   * This method returns if whether a cell is inside a corridor or not
   */
  bool isInsideCorridor(int,pair<double,double>);
  /*
   * This method returns if whether a cell is inside a corridor or not
   */
  bool isInsideWaypointRadius(int,pair<double,double>);
  /*
   * This method returns if whether a cell is inside a corridor or not
   */
  bool isInsidePath(pair<double,double>);
  /*
   * This method assigns the cells inside the path as good cells
   */
  void paintCorridor(RowCol,RowCol);


  // Misc methods
  /* void Copy_Vstate
   *  Copies all data from newState to curState, except raw magnetometer, IMU,
   *  and GPS data.
   */
  void Copy_VState(VState_GetStateMsg newState);

  // Display the map on the screen
  void display();

  /* int saveMATFile(char *filename, char *header_message, char *variable_name)
   *   Store the current window values and colors in a MATFile named charname,
   *   in a variable named variable_name.
   *  See MATsample.m for the output file format.
   * Input Values:
   *  filename: the name of the file (without the extension) in which genMap
   *            data will be stored.
   *  header_message: Please put in info like the author, module, date, test
   *                  info, etc.
   * returns: 0 iff successful
   *
   * To Display Map data using this file, follow instructions below:
   *  1. Must save the data in a file in the form xxx.m
   *  2. Run matlab
   *  3. Run viewGenMap(filename without '.m'):
   *  >> viewGenMap(xxx)
   */
  int saveMATFile(char *filename, char *header_message, char *variable_name);
  int saveSIZEFile(char *filename, char *header_message, char *variable_name);
  // Loads the file generated by the method above
  int loadMATFile(char *filename);

  // Saves the genMap as an image file of some type
  int saveIMGFile(char *filename);
  /* int saveNorthEastDownOnlyFile(char *filename)
   * Saves the genMap's value (height, usually) data in three columns:
   * Northing(UTM) Easting(UTM) Downing(m)
   */
  int saveNorthEastDownOnlyFile(char *filename);

  //returns the length of the longest cellList
  int findMaxListLength();
  //Replaces the first character of each line with %.
  void EditHeader(charptr &header_message);

  //Runs through the list of cells on the line between end and the vehicle's position
  //and deletes all cells with values higher (i.e. more negative) than end.Downing.
  //void CutCellLine(NorthEastDown end);
  //Runs through the list of cells on the line between end and start
  //and deletes all cells with values higher (i.e. more negative) than end.Downing.
  //void CutCellLine(NorthEastDown start, NorthEastDown end);
  //Generates a list of the cells through which the line between end and start passes.
  //vector<RowCol> GetCellLine(NorthEastDown start, NorthEastDown end);
  vector<RowCol> getCellsLine(NorthEastDown point);
};

#endif //__GENMAP_HH__
