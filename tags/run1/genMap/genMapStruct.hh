/* genMapStruct.hh - message format for genMap struct
 *
 * Lars Cremean
 * 19 February 2004
 * 
 * This file defines the format for a genMap struct that is requested
 * from various modules by MatlabDisplay.  
 *
 * Changes:
 *   2004/02/19  Lars Cremean, created
 *
 */

#ifndef GENMAPSTRUCT_HH
#define GENMAPSTRUCT_HH

// Not sure if I need these
#include "MTA/Misc/Time/Timeval.hh"
#include "MTA/Misc/Mail/Mail.hh"
#include "genMap.hh"

#include <boost/shared_ptr.hpp>

struct dbl_block {

  double * ptr;
  dbl_block(int size) { ptr = new double[size]; }
  ~dbl_block() { if ( ptr != NULL) delete ptr; }

};

struct genMapStruct
{
  double row_res;  // row (Easting) cell resolution
  double col_res;  // column (Northing) cell resolution

  // UTM location of the left bottom corner of the left bottom cell
  double leftBottomUTM_Northing;
  double leftBottomUTM_Easting;

  double leftBottomRow;
  double leftBottomCol;

  int numRows;
  int numCols;

  double* cell(int x, int y) {
    if( 0 <= x && x <= numRows && 
	0 <= y && y <= numCols) 
      return &(*data).ptr[x* numCols + y]; 
    else return NULL;
  }
  //  double **data;
private:
  boost::shared_ptr<dbl_block> data;

public:


  // Allocate will set numRows and numCols and allocate
  // the associated amount of memory
  void Allocate(int rows, int cols) {
    numRows = rows;
    numCols = cols;
    data = shared_ptr<dbl_block>( new dbl_block(rows*cols));      
  }
  
  genMapStruct() : numRows(0), numCols(0) {}
  ~genMapStruct() {}


  // TODO: Add an array of data here!
  

private:
  genMapStruct( const genMapStruct& gms);



};

Mail& operator << (Mail& ml, genMapStruct& rhs);  // send the struct
Mail& operator >> (Mail& ml, genMapStruct& rhs);  // recover the struct



void ConvertGenMapToStruct( genMap& gm, genMapStruct& gms );

#endif
