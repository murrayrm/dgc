/* A test program for the genMap class's path evaluation methods.
 * Author: Thyago Consort
 * Revision History:  Created  Thyago C.  1/17/2004
 */

#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <time.h>
#include <string.h>
#include <iostream.h>
#include "genMap.hh"

#define PI 3.1415

map <RowCol,double> d_map;
map <RowCol,float> f_map;
map <RowCol,int> i_map;
int n;

void rec_fun(int i,int j,double value){
  RowCol pos;
  if(j < n){
    pos.first = i;
    pos.second = j;
    d_map[pos] = value*value;
    rec_fun(i,j+1,value);
  }
}

int main (int argc,char* argv[]){
  VState_GetStateMsg state;
  RowCol before;
  RDDFVector targetPoints;
  RowCol add, after, pos;
  double north_dist, east_dist;
  double Northing, Easting;
  double ahead, aside, yaw;
  double cost, phi;
  CellData newCell, compCell, cell;
  vector<double> arcs;
  vector<double> votes;
  int numArcs,i,j,ret,ind,NORTH,EAST;
  NorthEastDown Po,v(1,2,3),w(4,5,6),z(7,8,9),east,plus,minus,equal,vec[4];
  NorthEastDown a_ref,b_ref,a_car,b_car;
  VBString line;
  double dist,offTire[4];
  pair<bool,double> aux_pair;
  map<RowCol,RowCol> cells;
  pair<double,double> NorthEast;
  vector< pair<double,double> > arbiterVotes;
  pair<double,bool> distObs;
  char fileName[100];
  bool aux,paint;
  double t1[50],t2[50],t3[50];
  double k=1000;
  double d_value;
  float f_value;
  int i_value;
  int i_matrix[500][500];
  short s_value;
  map <RowCol,short> s_map;
  NorthEastDown start, end;//for testing GetCellLine
  vector<RowCol> cells_on_line;//also for testing GetCellLine 
  /*************************************************************************/
  /********** DECLARING VARIABLES ******************************************/
  /*************************************************************************/
  // Variales used to get parameters from command line
  int opt, outOfMemory;
  char *argMethod;
  int method,param;
  system("clear");

  /*************************************************************************
   ********** GETTING THE PARAMETERS FROM COMMAND LINE			
   **********  CAUTION:						  
   **********    - optind,opterr:  extern variables, do not declare a
   **********	         local variable with any of these names	     
   **********	 - optind: indice of the argv where to find the argument 
   **********		  desired					    
   *************************************************************************/

  // Defining the characters used as delimiter of a command line argument  
  param = 0;
  outOfMemory = 0;

  //double argh = 1112230.0234;
  //cout << "blah: " << fixed << argh << endl;

  while ((opt = getopt (argc, argv, "m:")) != -1){
    switch ((unsigned char)opt){
    case 'm':
      param = 1;
      if(optarg != NULL){
	argMethod = (char*) malloc(strlen(optarg)+1);
	if(argMethod == NULL)
	  outOfMemory = 1;
	else
	  strcpy(argMethod,optarg);
      }
      break;
    case '?':
    default:
      system("clear");
      cout << "usage: [-m method]" << endl << endl
           << "1 : Constructor method" << endl
           << "2 : RowColFromDeltaRowCol" << endl
           << "3 : RowColFromDeltaPosition" << endl
           << "4 : FrameRef2RowCol" << endl
           << "5 : UTM2RowCol" << endl
           << "6 : evalPathCost" << endl
           << "7 : set/get CellDataRC" << endl
           << "8 : set/get CellDataUTM" << endl
           << "9 : set/get CellDataFrameRef" << endl
           << "10: generateVotes" << endl
           << "11: updateFrame" << endl
	   << "12: setCellListDataUTM" << endl
	   << "13: NorthEastDown struct testing" << endl
           << "14: evalNormalVector" << endl
           << "15: evalNormalVectorCost" << endl
           << "16: distancePoint2Plane" << endl
           << "17: distancePoint2Line" << endl
           << "18: evalOffTireCost" << endl
           << "19: NOT USED ANY MORE" << endl
           << "20: allCellsUnderCost" << endl
           << "21: RowCol2UTM" << endl
           << "22: obstacleAvoidance" << endl
           << "23: GENERATE_ARBITER_VOTES" << endl 
	   << "24: save map with saveMATFile" << endl
	   << "25: load map with loadMATFile" << endl
	   << "26: isInsideWaypointRadius" << endl
	   << "27: isInsideCorridor" << endl
	   << "28: isInsidePath" << endl
	   << "29: paintCorridor" << endl
	   << "30: updateFrame + corridor + generate arbiter votes" << endl
	   << "31: P E R F O R M A N C E" << endl
	   << "32: double for => recursive algorithm" << endl
	   << "33: clearRow" << endl
	   << "34: clearCol" << endl
	   << "35: clearColor" << endl
	   << "36: clearMap" << endl
	   << "37: targetPoint update" << endl
	   << "38: save MAT file with corridor" << endl
	   << "39: goodnessShape" << endl
	   << "40: changeCoordinates" << endl
	   << "41: test follower votes" << endl
	   << "42: shrink obstacles" << endl
	   << "43: maxHeightDiffCost" << endl
	   << "44: maxInclinationCost" << endl
	   << "45: sa_test.m" << endl 
	   << "46: getCellLine" << endl
	   << "47: obstacleGoodnessShape" << endl  
	   << "48: getCellsLine" << endl
	   << "49: maxHeightDiffCostRectangle" << endl << endl;  
      return(-1);
    }    
  }
  
  // If any allocation failed
  if(outOfMemory){
    if(argMethod != NULL) free(argMethod);
    system("clear");
    cout << "Problems with memory management" << endl;
    return(-1);
  }
  
  // Showing the choosen test
  if(param)
    method = atoi(argMethod);
  else
    method = 1;

  // STARTING TESTS ...
  state.Easting = 0;
  state.Northing = 0;
  state.Altitude = 0;
  state.Vel_E = 0;
  state.Vel_N = 0;
  state.Vel_U = 0;
  state.Speed = 0;
  state.Accel = 0;
  state.Pitch = 0;
  state.Roll = 0;
  state.Yaw = 0;
  state.PitchRate = 0;
  state.RollRate = 0;
  state.YawRate = 0;

  // Counting time
  VBClock clk;
  VBString name;
  // 10x10 map with 0.5x0.5 cell
  paint = false;
  double res = 0.2;
  genMap mapPath(state,40,40,0.5,0.5,true);
  genMap map;
  map.initMap(20,20,1,1,false);
  genMap map2(true),map3; 
  state.Easting =396070;
  state.Northing =3777070;
  genMap mapPaint;
  state.Easting =SIMULATOR_INITIAL_EASTING;
  state.Northing =SIMULATOR_INITIAL_NORTHING;
  genMap mapCorridor(state,25,25,1.0,1.0,false);
  genMap bigMapCorridor;
  clk.markTime();
  genMap map100(state,200,200,res,res,paint);
  map100.initPathEvaluationCriteria(false,false,false,false,false,false,true);
  cout << "Constructor 100 " << 1000*clk.getDiffTime() << endl;
  clk.markTime();
  genMap map500(state,300,300,res,res,paint);
  map500.initPathEvaluationCriteria(false,false,false,false,false,false,true);
  cout << "Constructor 250 " << 1000*clk.getDiffTime() << endl;
  clk.markTime();
  genMap map1000(state,500,500,res,res,paint);
  map1000.initPathEvaluationCriteria(false,false,false,false,false,false,true);
  cout << "Constructor 500 " << 1000*clk.getDiffTime() << endl;
  cout << endl;
  /*
  cout << "Numrows=" <<  map.getNumRows()<< endl;
  cout << "NumCols=" <<  map.getNumCols()<< endl;
  cout << "RowRes=" <<  map.getRowRes()<< endl;
  cout << "ColRes=" <<  map.getColRes()<< endl;
  cout << "VehRow=" <<  map.getVehRow()<< endl;
  cout << "VehCol=" <<  map.getVehCol()<< endl;
  cout << "NorBot=" <<  map.getLeftBottomUTM_Northing()<< endl;
  cout << "EasBot=" <<  map.getLeftBottomUTM_Easting()<< endl;
  cout << "RowBot=" <<  map.getLeftBottomRow()<< endl;
  cout << "ColBot=" <<  map.getLeftBottomCol()<< endl;
  cout << "MxPath=" <<  map.getPathMaxDistance()<< endl;
  cout << "PRESS ..." << endl;
  getchar();
  */

  switch(method){
  case 1:

    //**********************************************//
    // Testing constructor
    //**********************************************//

    system("clear");
    map3.display();
    cout << "NOT INITIALIZED YET" << endl << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    system("clear");
    map3.initMap(15,15,1,1);
    map3.displayThresholds();
    getchar();
    map3.initThresholds(10,20,30,40);
    map3.displayThresholds();
    getchar();
    map3.display(); 
    cout << "INITIALIZED !!!" << endl << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    system("clear");
    map2.display(); 
    cout << "INITIALIZED WITH DEFAULT VALUES 100x100 !!!" << endl << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    break;
  case 2:

    //**********************************************//
    // Testing RowColFromDeltaRowCol
    //**********************************************//

    before.first = 6;
    before.second = 6;
    cout << "Map (" << map.getNumRows() << "," << map.getNumCols() << ")" << endl;

    // First test
    add.first = 2;
    add.second = 4;
    after = map.RowColFromDeltaRowCol(before,add);
    cout << "Before (" << before.first << "," << before.second << ") Add (" << add.first << "," << add.second << ") After (" << after.first << "," << after.second << ")" << endl;
    cout << endl;

    // Second test
    before = after;
    add.first = 6;
    add.second = -7;
    after = map.RowColFromDeltaRowCol(before,add);
    cout << "Before (" << before.first << "," << before.second << ") Add (" << add.first << "," << add.second << ") After (" << after.first << "," << after.second << ")" << endl;
    cout << endl;

    // Third test
    before = after;
    add.first = 4;
    add.second = -5;
    after = map.RowColFromDeltaRowCol(before,add);
    cout << "Before (" << before.first << "," << before.second << ") Add (" << add.first << "," << add.second << ") After (" << after.first << "," << after.second << ")" << endl;
    cout << endl;

    break;

  case 3:

    //**********************************************//
    // RowColFromDeltaPosition
    //**********************************************//

    cout << "Map (" << map.getNumRows() << "," << map.getNumCols() << ") Resolution (" << map.getRowRes() << "," << map.getColRes() << ")" << endl << endl;
    before.first = 2;
    before.second = 3;

    // First test
    north_dist = map.getNumRows()*map.getRowRes(); //meters
    east_dist = map.getNumCols()*map.getColRes(); //meters

    after = map.RowColFromDeltaPosition(before,north_dist,east_dist);
    cout << "Before (" << before.first << "," << before.second << ") Add (" << north_dist << "," << east_dist << ") After (" << after.first << "," << after.second << ")" << endl;
    cout << endl;

    // Second test
    north_dist = -0.6; //meters
    east_dist = -0.2; //meters

    after = map.RowColFromDeltaPosition(before,north_dist,east_dist);
    cout << "Before (" << before.first << "," << before.second << ") Add (" << north_dist << "," << east_dist << ") After (" << after.first << "," << after.second << ")" << endl;
    cout << endl;

    cout << endl;

    // Third test
    north_dist = -1.5; //meters
    east_dist = 5.2; //meters

    after = map.RowColFromDeltaPosition(before,north_dist,east_dist);
    cout << "Before (" << before.first << "," << before.second << ") Add (" << north_dist << "," << east_dist << ") After (" << after.first << "," << after.second << ")" << endl;
    cout << endl;

    break;

  case 4:

    //**********************************************//
    // FrameRef2RowCol
    //**********************************************//

    cout << "Map (" << map.getNumRows() << "," << map.getNumCols() << ") Resolution (" << map.getRowRes() << "," << map.getColRes() << ")" << endl << endl;
    before.first = 2;
    before.second = 3;
    yaw = asin(0.5);  //30 degrees
    ahead = 5;       //meters
    aside = 3;       //meters
    cout << "yaw = pi/6 degrees (" << yaw  << ") (N=0 , E=pi/2), each test the sign change" << endl << endl; 
    cout << "ahead*sin(pi/6) = " << ahead*sin(yaw) <<  " ahead*cos(pi/6) = " << ahead*cos(yaw) << endl;
    cout << "aside*sin(pi/6) = " << aside*sin(yaw) <<  " aside*cos(pi/6) = " << aside*cos(yaw) << endl << endl;

    // 1.a test - 1st quadrantic 
    cout << "First quadrantic (yaw = " << yaw << ") " << endl;
    state.Yaw = yaw;
    ahead *= 1;
    aside *= 1;
    map.Copy_VState(state);
    
    after = map.FrameRef2RowCol(before,state.Yaw,ahead,aside);
    cout << "Before (" << before.first << "," << before.second << ") aside = " << aside << " ahead = " << ahead << " After (" << after.first << "," << after.second << ")" << endl;
    cout << endl;

    // 1.b test - 1st quadrantic 
    ahead *= -1;
    aside *= -1;

    after = map.FrameRef2RowCol(before,state.Yaw,ahead,aside);
    cout << "Before (" << before.first << "," << before.second << ") aside = " << aside << " ahead = " << ahead << " After (" << after.first << "," << after.second << ")" << endl;
    cout << endl;

    // 2.a test - 2nd quadrantic 
    cout << "Second quadrantic (yaw = -60) " << endl;
    state.Yaw -= PI/2;
    ahead *= -1;
    aside *= -1;
    map.Copy_VState(state);

    after = map.FrameRef2RowCol(before,state.Yaw,ahead,aside);
    cout << "Before (" << before.first << "," << before.second << ") aside = " << aside << " ahead = " << ahead << " After (" << after.first << "," << after.second << ")" << endl;
    cout << endl;

    // 2.b test - 2nd quadrantic 
    ahead *= -1;
    aside *= -1;

    after = map.FrameRef2RowCol(before,state.Yaw,ahead,aside);
    cout << "Before (" << before.first << "," << before.second << ") aside = " << aside << " ahead = " << ahead << " After (" << after.first << "," << after.second << ")" << endl;
    cout << endl;

    // 3.a test - 3rd quadrantic 
    cout << "Third quadrantic (yaw = -150) " << endl;
    state.Yaw -= PI/2;
    ahead *= -1;
    aside *= -1;
    map.Copy_VState(state);

    after = map.FrameRef2RowCol(before,state.Yaw,ahead,aside);
    cout << "Before (" << before.first << "," << before.second << ") aside = " << aside << " ahead = " << ahead << " After (" << after.first << "," << after.second << ")" << endl;
    cout << endl;

    // 3.b test - 3rd quadrantic 
    ahead *= -1;
    aside *= -1;

    after = map.FrameRef2RowCol(before,state.Yaw,ahead,aside);
    cout << "Before (" << before.first << "," << before.second << ") aside = " << aside << " ahead = " << ahead << " After (" << after.first << "," << after.second << ")" << endl;
    cout << endl;

    // 4.a test - 4th quadrantic 
    cout << "Fourth quadrantic (yaw = 120) " << endl;
    state.Yaw -= PI/2;
    ahead *= -1;
    aside *= -1;
    map.Copy_VState(state);

    after = map.FrameRef2RowCol(before,state.Yaw,ahead,aside);
    cout << "Before (" << before.first << "," << before.second << ") aside = " << aside << " ahead = " << ahead << " After (" << after.first << "," << after.second << ")" << endl;
    cout << endl;

    // 4.b test - 4th quadrantic 
    ahead *= -1;
    aside *= -1;

    after = map.FrameRef2RowCol(before,state.Yaw,ahead,aside);
    cout << "Before (" << before.first << "," << before.second << ") aside = " << aside << " ahead = " << ahead << " After (" << after.first << "," << after.second << ")" << endl;
    cout << endl;

    break;

  case 5:

    //**********************************************//
    // UTM2RowCol
    //**********************************************//

    cout << "Map (" << map.getNumRows() << "," << map.getNumCols() << ") Resolution (" << map.getRowRes() << "," << map.getColRes() << ")" << " Left Bottom Corner in UTM (N,E) = (" << map.getLeftBottomUTM_Northing() << "," << map.getLeftBottomUTM_Easting() << ") (row,col) = (" << map.getLeftBottomRow() << "," << map.getLeftBottomCol() << ")" << endl << endl;

    // First test
    Northing = 5; //meters
    Easting = 4; //meters

    after = map.UTM2RowCol(Northing,Easting);
    cout << "(Northing,Easting) = (" << Northing << "," << Easting << ") After (" << after.first << "," << after.second << ")" << endl;
    cout << endl;

    // second test
    Northing = 3; //meters
    Easting = 9.6; //meters

    after = map.UTM2RowCol(Northing,Easting);
    cout << "(Northing,Easting) = (" << Northing << "," << Easting << ") After (" << after.first << "," << after.second << ")" << endl;
    cout << endl;

    // third test
    Northing = -10; //meters
    Easting = -2; //meters

    after = map.UTM2RowCol(Northing,Easting);
    cout << "(Northing,Easting) = (" << Northing << "," << Easting << ") After (" << after.first << "," << after.second << ")" << endl;
    cout << endl;

    break;

  case 6:

    //**********************************************//
    // evalPathCost
    //**********************************************//

    cout << "Map (" << map.getNumRows() << "," << map.getNumCols() << ") Resolution (" << map.getRowRes() << "," << map.getColRes() << ")" << "Left Bottom Corner in UTM (N,E) = (" << map.getLeftBottomUTM_Northing() << "," << map.getLeftBottomUTM_Easting() << ")" << endl << endl;

    // 1.a Straight
    phi = 0;
    yaw = 0;
    state.Yaw = yaw;
    state.Northing = 0;
    state.Easting = 0;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = 0 PHI = 0" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.a.i Curves
    yaw = 0;
    phi = -PI/3;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = 0 PHI = -pi/3" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.a.ii Curves
    yaw = 0;
    phi = PI/3;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = 0 PHI = pi/3" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.b Straight
    phi = 0;
    yaw = - PI/4;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = -pi/4 PHI = 0" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.b.i Curves
    yaw = -PI/4;
    phi = -PI/3;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = -pi/4 PHI = -pi/3" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.b.ii Curves
    yaw = -PI/4;
    phi = PI/3;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = -pi/4  PHI = pi/3" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.c Straight
    phi = 0;
    yaw = -PI/2;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = -pi/2 PHI = 0" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.c.i Curves
    phi = -PI/3;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = -pi/2 PHI = -pi/3" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.c.ii Curves
    phi = PI/3;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = -pi/2  PHI = pi/3" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.d Straight
    phi = 0;
    yaw = -3*PI/4;
    state.Yaw = yaw;
    ahead *= 1;
    aside *= 1;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = -3pi/4 PHI = 0" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.d.i Curves
    phi = -PI/3;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = -3pi/4 PHI = -pi/3" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.d.ii Curves
    phi = PI/3;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = -3pi/4  PHI = pi/3" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.e Straight
    phi = 0;
    yaw = -PI;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = pi/4 PHI = 0" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.e.i Curves
    phi = -PI/3;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = -pi PHI = -pi/3" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.e.ii Curves
    phi = PI/3;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = -pi  PHI = pi/3" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.f Straight
    phi = 0;
    yaw = -PI - PI/4;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = 3pi/4 PHI = 0" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.f.i Curves
    phi = -PI/3;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = -3pi/4 PHI = -pi/3" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.f.ii Curves
    phi = PI/3;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = -3pi/4  PHI = pi/3" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.g Straight
    phi = 0;
    yaw = PI/2;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = pi/2 PHI = 0" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.g.i Curves
    phi = -PI/3;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = pi/2 PHI = -pi/3" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.g.ii Curves
    phi = PI/3;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = pi/2  PHI = pi/3" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.h Straight
    phi = 0;
    yaw = PI/4;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = pi/4 PHI = 0" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.h.i Curves
    phi = -PI/3;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = pi/4 PHI = -pi/3" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.h.ii Curves
    phi = PI/3;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = pi/4  PHI = pi/3" << endl;
    cost = map.evalPathCost(phi);
    map.display();
    map.clearMap();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    break;

  case 7:

    //**********************************************//
    // set/get CellDataRC
    //**********************************************//
    map.clearMap();
    newCell.color = 2;

    // Getting a value without being assigned and out of range
    system("clear");
    cout << "Getting a value without being assigned and out of range" << endl << endl;
    pos.first = 0;
    pos.second = 0;
    compCell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") assigned with " << compCell.value << endl;
    pos.first = 16;
    pos.second = 2;
    compCell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << compCell.value << endl;
    pos.first = 14;
    pos.second = -7;
    compCell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << compCell.value << endl;
    sleep(5);
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // Assigning non existent cells
    system("clear");
    cout << "Assigning non existent cells" << endl << endl;
    compCell.value = 2;
    newCell.value = 5;
    pos.first = 9;
    pos.second = 12;    
    cell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << cell.value << endl;
    map.setCellDataRC(pos.first,pos.second,newCell);
    cout << "Cell at (" << pos.first << "," << pos.second << ") assigned with " << newCell.value << endl;
    compCell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << compCell.value << endl;
    sleep(5);
    pos.first = 1;
    pos.second = 5;    
    cell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << cell.value << endl;
    map.setCellDataRC(pos.first,pos.second,newCell);
    cout << "Cell at (" << pos.first << "," << pos.second << ") assigned with " << newCell.value << endl;
    compCell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << compCell.value << endl;
    sleep(5);
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // Assigning existent cells
    system("clear");
    cout << "Assigning existent cells" << endl << endl;
    compCell.value = 3;
    newCell.value = 4;
    newCell.type = OUTSIDE_CORRIDOR_CELL;
    pos.first = 9;
    pos.second = 12;    
    cell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << cell.value << endl;
    map.setCellDataRC(pos.first,pos.second,newCell);
    cout << "Cell at (" << pos.first << "," << pos.second << ") assigned with " << newCell.value << endl;
    compCell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << compCell.value << endl;
    sleep(5);
    pos.first = 1;
    pos.second = 5;    
    cell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << cell.value << endl;
    map.setCellDataRC(pos.first,pos.second,newCell);
    cout << "Cell at (" << pos.first << "," << pos.second << ") assigned with " << newCell.value << endl;
    compCell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << compCell.value << endl;
    sleep(5);
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // Assigning corridor cells
    system("clear");
    cout << "Assigning corridor cells" << endl;
    compCell.value = 3;
    newCell.value = 7;
    pos.first = 9;
    pos.second = 12;    
    cell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << cell.value << endl;
    map.setCellDataRC(pos.first,pos.second,newCell);
    cout << "Cell at (" << pos.first << "," << pos.second << ") assigned with " << newCell.value << endl;
    compCell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << compCell.value << endl;
    sleep(5);
    pos.first = 1;
    pos.second = 5;    
    cell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << cell.value << endl;
    map.setCellDataRC(pos.first,pos.second,newCell);
    cout << "Cell at (" << pos.first << "," << pos.second << ") assigned with " << newCell.value << endl;
    compCell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << compCell.value << endl;
    sleep(5);
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // Assigning corridor cells asking to ignore them and assign anyway
    system("clear");
    cout << "Assigning corridor cells asking to ignore them and assign anyway" << endl << endl;
    compCell.value = 9;
    newCell.value = 10;
    newCell.type = 0;
    pos.first = 9;
    pos.second = 12;    
    cell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << cell.value << endl;
    map.setCellDataRC(pos.first,pos.second,newCell);
    cout << "Cell at (" << pos.first << "," << pos.second << ") assigned with " << newCell.value << endl;
    compCell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << compCell.value << endl;
    sleep(5);
    pos.first = 1;
    pos.second = 5;    
    cell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << cell.value << endl;
    map.setCellDataRC(pos.first,pos.second,newCell);
    cout << "Cell at (" << pos.first << "," << pos.second << ") assigned with " << newCell.value << endl;
    compCell = map.getCellDataRC(pos.first,pos.second);
    cout << "Cell at (" << pos.first << "," << pos.second << ") returned is " << compCell.value << endl;
    sleep(5);
    map.display();


    break;

  case 8:

    //**********************************************//
    // set/get CellDataUTM
    //**********************************************//
    map.clearMap();
    newCell.color = 2;

    // 1st test
    system("clear");
    newCell.value = 7;
    Northing = -2;
    Easting = 4.5;
    map.setCellDataUTM(Northing,Easting,newCell);
    cout << "Cell at (north,east) = (" << Northing << "," << Easting << ") assigned with " << newCell.value << endl;
    compCell = map.getCellDataUTM(Northing,Easting);
    cout << "Cell at (north,east) = (" << Northing << "," << Easting << ") returned is " << compCell.value << endl;
    sleep(5);
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 2nd test
    system("clear");
    newCell.value = 4;
    Northing = 3;
    Easting = -1;
    map.setCellDataUTM(Northing,Easting,newCell);
    cout << "Cell at (north,east) = (" << Northing << "," << Easting << ") assigned with " << newCell.value << endl;
    compCell = map.getCellDataUTM(Northing,Easting);
    cout << "Cell at (north,east) = (" << Northing << "," << Easting << ") returned is " << compCell.value << endl;
    sleep(5);
    map.display();

    break;

  case 9:

    //**********************************************//
    // set/get CellDataFrameRef
    //**********************************************//
    map.clearMap();
    phi = 0; // steering wheel
    newCell.color = 2;

    // 1.a
    yaw = 0;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = 0 PHI = 0" << endl;
    map.clearMap();
    cost = map.evalPathCost(phi);
    newCell.value = 7;
    ahead = -1;
    aside = 2;
    map.setCellDataFrameRef(ahead,aside,newCell);
    compCell = map.getCellDataFrameRef(ahead,aside);
    map.display();
    cout << "Cell at (ahead,aside) = (" << ahead << "," << aside << ") assigned with " << newCell.value << endl;
    cout << "Cell at (ahead,aside) = (" << ahead << "," << aside << ") returned is " << compCell.value << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.b
    yaw = -PI/4;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = -pi/4" << endl;
    map.clearMap();
    cost = map.evalPathCost(phi);
    newCell.value = 7;
    map.setCellDataFrameRef(ahead,aside,newCell);
    compCell = map.getCellDataFrameRef(ahead,aside);
    map.display();
    cout << "Cell at (ahead,aside) = (" << ahead << "," << aside << ") assigned with " << newCell.value << endl;
    cout << "Cell at (ahead,aside) = (" << ahead << "," << aside << ") returned is " << compCell.value << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.c
    yaw = -PI/2;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = -pi/2" << endl;
    map.clearMap();
    cost = map.evalPathCost(phi);
    newCell.value = 7;
    map.setCellDataFrameRef(ahead,aside,newCell);
    compCell = map.getCellDataFrameRef(ahead,aside);
    map.display();
    cout << "Cell at (ahead,aside) = (" << ahead << "," << aside << ") assigned with " << newCell.value << endl;
    cout << "Cell at (ahead,aside) = (" << ahead << "," << aside << ") returned is " << compCell.value << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.c
    yaw = -3*PI/4;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = -3pi/4" << endl;
    map.clearMap();
    cost = map.evalPathCost(phi);
    newCell.value = 7;
    map.setCellDataFrameRef(ahead,aside,newCell);
    compCell = map.getCellDataFrameRef(ahead,aside);
    map.display();
    cout << "Cell at (ahead,aside) = (" << ahead << "," << aside << ") assigned with " << newCell.value << endl;
    cout << "Cell at (ahead,aside) = (" << ahead << "," << aside << ") returned is " << compCell.value << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.d
    yaw = -PI;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = -PI" << endl;
    map.clearMap();
    cost = map.evalPathCost(phi);
    newCell.value = 7;
    map.setCellDataFrameRef(ahead,aside,newCell);
    compCell = map.getCellDataFrameRef(ahead,aside);
    map.display();
    cout << "Cell at (ahead,aside) = (" << ahead << "," << aside << ") assigned with " << newCell.value << endl;
    cout << "Cell at (ahead,aside) = (" << ahead << "," << aside << ") returned is " << compCell.value << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.e
    yaw = 3*PI/4;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = 3pi/4" << endl;
    map.clearMap();
    cost = map.evalPathCost(phi);
    newCell.value = 7;
    map.setCellDataFrameRef(ahead,aside,newCell);
    compCell = map.getCellDataFrameRef(ahead,aside);
    map.display();
    cout << "Cell at (ahead,aside) = (" << ahead << "," << aside << ") assigned with " << newCell.value << endl;
    cout << "Cell at (ahead,aside) = (" << ahead << "," << aside << ") returned is " << compCell.value << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.f
    yaw = PI/2;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = pi/2" << endl;
    map.clearMap();
    cost = map.evalPathCost(phi);
    newCell.value = 7;
    map.setCellDataFrameRef(ahead,aside,newCell);
    compCell = map.getCellDataFrameRef(ahead,aside);
    map.display();
    cout << "Cell at (ahead,aside) = (" << ahead << "," << aside << ") assigned with " << newCell.value << endl;
    cout << "Cell at (ahead,aside) = (" << ahead << "," << aside << ") returned is " << compCell.value << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // 1.f
    yaw = PI/4;
    state.Yaw = yaw;
    map.Copy_VState(state);
    
    system("clear");
    cout << "YAW = pi/4" << endl;
    map.clearMap();
    cost = map.evalPathCost(phi);
    newCell.value = 7;
    map.setCellDataFrameRef(ahead,aside,newCell);
    compCell = map.getCellDataFrameRef(ahead,aside);
    map.display();
    cout << "Cell at (ahead,aside) = (" << ahead << "," << aside << ") assigned with " << newCell.value << endl;
    cout << "Cell at (ahead,aside) = (" << ahead << "," << aside << ") returned is " << compCell.value << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    break;

  case 10:
    //**********************************************//
    // generateVotes
    //**********************************************//
    // 1.a
    map.clearMap();
    yaw = 0;
    state.Yaw = yaw;
    map.Copy_VState(state);
    newCell.color = 2;
    
    system("clear");
    cout << "YAW = 0" << endl;

    // Filling some cells
    ahead = 8;
    aside = 0;
    newCell.value = 1;
    map.setCellDataFrameRef(ahead,aside,newCell);

    // Filling some cells
    ahead = 6;
    aside = 3;
    newCell.value = 1;
    map.setCellDataFrameRef(ahead,aside,newCell);

    // Filling some cells
    ahead = 6;
    aside = -1;
    newCell.value = 2;
    map.setCellDataFrameRef(ahead,aside,newCell);

    // Filling some cells
    ahead = 5;
    aside = 1;
    newCell.value = 5;
    map.setCellDataFrameRef(ahead,aside,newCell);

    // Filling some cells
    ahead = 7;
    aside = -3;
    newCell.value = 3;
    map.setCellDataFrameRef(ahead,aside,newCell);
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    numArcs = 3;
    arcs.reserve(numArcs);
    votes.reserve(numArcs);
    arcs.push_back(-PI/3);
    arcs.push_back(0);
    arcs.push_back(PI/3);
    map.clearColor(BLUE_INDEX);
    votes = map.generateVotes(numArcs,arcs);

    // Visualizing the costs
    map.clearColor(BLUE_INDEX);
    cout << "Cost for (phi=-pi/3) is " <<  map.evalPathCost(arcs[0]) << " and vote is " << votes[0] << endl;
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // Visualizing the costs
    map.clearColor(BLUE_INDEX);
    cout << "Cost for (phi=  0  ) is " <<  map.evalPathCost(arcs[1]) << " and vote is " << votes[1] << endl;
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // Visualizing the costs
    map.clearColor(BLUE_INDEX);
    cout << "Cost for (phi=+pi/3) is " <<  map.evalPathCost(arcs[2]) << " and vote is " << votes[2] << endl;
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // Displaying votes
    system("clear");
    cout << "         | -pi/3 |   0   | pi/3" << endl;
    cout << "---------|-------|-------|-------" << endl;  
    printf ("goodness | %3.1f | %3.1f  | %3.1f\n\n\n",votes[0],votes[1],votes[2]);

    break;

  case 11:
    //**********************************************//
    // updateFrame
    //**********************************************//
    map.clearMap();
    state.Easting = 0;
    state.Northing = 0;
    map.initMap(20,20,0.5,0.5,false);
    mapCorridor.initMap(25,25,0.5,0.5,false);
    yaw = 0;
    state.Yaw = yaw;
    newCell.value = 1;
    newCell.color = 1;
    system("clear");
    /*
    for(i=0;i<15;i++){
      for(j=0;j<15;j++){
	if((i==7)&&((j==7)||(j==8)))
	  {
	    newCell.value = 2;
	    newCell.value = 2;
	  }
        map.setCellDataRC(i,j,newCell);
	if((i==7)&&((j==7)||(j==8)))
	  {
	    newCell.value = 1;
	    newCell.value = 1;
	  }
      }
    }
    */
 
    /*
    cout << "PLEASE UNCOMMENT DEBUG INSIDE UPDATE FRAME" << endl;
    cout << "TESTING A 20x20 MAP FIRST !!!!" << endl;
    cout << "PRESS ..." << endl;

    fflush(stdin);
    getchar();
    //map.display();
    //cout << "PRESS ..." << endl;
    //fflush(stdin);
    //getchar();
    state.Northing = 0;
    state.Easting = 0;
    map.updateFrame(state,true);
    //map.display();
    //cout << "PRESS ..." << endl;
    //fflush(stdin);
    //getchar();
    state.Northing = 0 + 0.6;
    state.Easting = 0 - 0.6;
    map.updateFrame(state,true);
    //map.display();
    //cout << "PRESS ..." << endl;
    //fflush(stdin);
    //getchar();
    state.Northing = 0 + 2*0.6;
    state.Easting = 0 - 2*0.6;
    map.updateFrame(state,true);
    //map.display();
    //cout << "PRESS ..." << endl;
    //fflush(stdin);
    //getchar();
    state.Northing = 0 + 3*0.6;
    state.Easting = 0 - 3*0.6;
    map.updateFrame(state,true);
    //map.display();
    //cout << "PRESS ..." << endl;
    //fflush(stdin);
    //getchar();

    cout << "TESTING A 25x25 MAP NOW !!!!" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    //mapCorridor.display();
    //cout << "PRESS ..." << endl;
    //fflush(stdin);
    //getchar();
    state.Northing = 0;
    state.Easting = 0;
    mapCorridor.updateFrame(state,true);
    //mapCorridor.display();
    //cout << "PRESS ..." << endl;
    //fflush(stdin);
    //getchar();
    state.Northing = 0 + 0.1;
    state.Easting = 0 - 0.1;
    mapCorridor.updateFrame(state,true);
    //mapCorridor.display();
    //cout << "PRESS ..." << endl;
    //fflush(stdin);
    //getchar();
    state.Northing =0 + 2*0.1;
    state.Easting = 0 - 2*0.1;
    mapCorridor.updateFrame(state,true);
    //mapCorridor.display();
    //cout << "PRESS ..." << endl;
    //fflush(stdin);
    //getchar();
    state.Northing = 0 + 3*0.1;
    state.Easting = 0 - 3*0.1;
    mapCorridor.updateFrame(state,true);
    //mapCorridor.display();
    //cout << "PRESS ..." << endl;
    //fflush(stdin);
    //getchar();
    */

    cout << "PLEASE COMMENT DEBUG INSIDE UPDATE FRAME" << endl;
    cout << "TEST CLEAR ROW AND COL" << endl;
    cout << "TESTING A 20x20 MAP FIRST !!!!" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    state.Northing = 0;
    state.Easting = 0;
    map.updateFrame(state,true);
    mapCorridor.updateFrame(state,true);

    newCell.value = 1;
    newCell.color = 1;
    system("clear");
    for(i=0;i<20;i++)
      for(j=0;j<20;j++)
        map.setCellDataRC(i,j,newCell);
    for(i=0;i<25;i++)
      for(j=0;j<25;j++)
        mapCorridor.setCellDataRC(i,j,newCell);

    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    state.Northing = 1*0.6;
    state.Easting = -1*0.6;
    map.updateFrame(state,true);
    system("clear");
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    state.Northing = 2*0.6;
    state.Easting = -2*0.6;
    map.updateFrame(state,true);
    system("clear");
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    state.Northing = 3*0.6;
    state.Easting = -3*0.6;
    map.updateFrame(state,true);
    system("clear");
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    cout << "TESTING A 25x25 MAP NOW !!!!" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    system("clear");
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    state.Northing = 1*0.1;
    state.Easting = -1*0.1;
    mapCorridor.updateFrame(state,true);
    system("clear");
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    state.Northing = 2*0.1;
    state.Easting = -2*0.1;
    mapCorridor.updateFrame(state,true);
    system("clear");
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    state.Northing = 3*0.1;
    state.Easting = -3*0.1;
    mapCorridor.updateFrame(state,true);
    system("clear");
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    /*
    state.Northing = -1;
    state.Easting = 5;
    map.updateFrame(state,true);
    map.display();
    cout << "The window swept to (Easting,Northing) = (" << state.Northing << "," <<\
      state.Easting << ")" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    system("clear");
    for(i=0;i<15;i++){
      for(j=0;j<15;j++){
        if((i==7)&&((j==7)||(j==8)))
          {
            newCell.value = 2;
            newCell.value = 2;
          }
        map.setCellDataRC(i,j,newCell);
        if((i==7)&&((j==7)||(j==8)))
          {
            newCell.value = 1;
            newCell.value = 1;
          }
      }
    }
    map.display();
    cout << "The window swept to (Easting,Northing) = (" << state.Northing << "," << state.Easting << ")" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    state.Northing = -1;
    state.Easting = -1;
    map.updateFrame(state,true);
    system("clear");
    map.display();
    cout << "The window swept to (Easting,Northing) = (" << state.Northing << "," << state.Easting << ")" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    newCell.value = 1;
    newCell.color = 1;
    system("clear");
    for(i=0;i<15;i++){
      for(j=0;j<15;j++){
        if((i==7)&&((j==7)||(j==8)))
          {
            newCell.value = 2;
            newCell.value = 2;
          }
        map.setCellDataRC(i,j,newCell);
        if((i==7)&&((j==7)||(j==8)))
          {
            newCell.value = 1;
            newCell.value = 1;
          }
      }
    }
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    //=======
    //>>>>>>> 1.41
    state.Northing = -15;
    state.Easting = 20;
    map.updateFrame(state,true);
    system("clear");
    map.display();
    cout << "The window swept to (Easting,Northing) = (" << state.Northing << "," << state.Easting << ")" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // Window not been up

    system("clear");
    cout << "NOW NOT UPDATING THE WINDOW !!!!!" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    state.Northing = 0;
    state.Easting = 0;
    map.updateFrame(state,false);
    map.clearMap();
    state.Easting = 0;
    state.Northing = 0;
    yaw = 0;
    state.Yaw = yaw;
    newCell.value = 1;
    newCell.color = 1;
    system("clear");
    for(i=0;i<15;i++){
      for(j=0;j<15;j++){
        map.setCellDataRC(i,j,newCell);
      }
    }
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    state.Northing = -2;
    state.Easting = -5;
    map.updateFrame(state,false);
    system("clear");
    map.display();
    cout << "The window swept to (Easting,Northing) = (" << state.Northing << "," << state.Easting << ")" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    state.Northing = 1;
    state.Easting = 2;
    map.updateFrame(state,false);
    system("clear");
    map.display();
    cout << "The window swept to (Easting,Northing) = (" << state.Northing << "," << state.Easting << ")" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    state.Northing = -15;
    state.Easting = 20;
    map.updateFrame(state,false);
    system("clear");
    map.display();
    cout << "The window swept to (Easting,Northing) = (" << state.Northing << "," << state.Easting << ")" << endl;
    */
    break;

  case 12:
    //**********************************************//
    // setCellListDataUTM
    //**********************************************//
    double list_val, Northing, Easting;
    int option_num, num_vals, quit;
    quit = 0;
    map.clearMap();
    yaw = 0;
    state.Yaw = yaw;
    map.updateFrame(state);

    while(quit != 1) {
      map.display();
      cout << "Options: " << endl
	   << "1: setCellListDataUTM" << endl
	   << "2: evaluateCellsSV" << endl
	   << "3: Refresh Map Display" << endl
	   << "0: Quit" << endl
	   << "Enter choice: ";
      cin >> option_num;
      
      switch(option_num) {
      case 1:
	cout << "Enter Northing: ";
	cin >> Northing;
	cout << "Enter Easting: ";
	cin >> Easting;
	cout << "Enter number of values for input: ";
	cin >> num_vals;
	for(int i=0; i < num_vals; i++) {
	  cout << "Enter a list_val: ";
	  cin >> list_val;
	  map.setCellListDataUTM(Northing, Easting, list_val);
	}
	break;
      case 2:
	map.evaluateCellsSV(1,0.0);
	break;
      case 3:
	break;
      case 0:
	cout << "\nQuitting..." << endl;
	quit = 1;
	break;
      default:
	cout << "No such option" << endl;
	quit = 1;
	break;
      }
    }

    break;

  case 13:
    //**********************************************//
    // NorthEastDown struct testing
    //**********************************************//
    cout << "norm of a small vector" << endl;
    plus.Northing = 0;plus.Easting = 0;plus.Downing = 0;
    plus.norm();
    plus.display();
    plus = v+w;
    minus = v-w;
    equal = v;
    cout << "v     ";
    equal.display();
    cout << "v     ";
    v.display();
    cout << "w     ";
    w.display();
    cout << "v + w ";
    plus.display();
    cout << "v - w ";
    minus.display();
    minus.norm();
    cout << "norma ";
    minus.display();
    equal = v*3;
    equal = equal*3;
    equal.display();
    v.Northing = 1;
    v.Easting = 2;
    v.Downing = 3;

    w.Northing = -1;
    w.Easting = -6;
    w.Downing = 3;
    cout << "internal product between v and w " << v*w << endl;
    equal = v^w;
    cout << "e = v x w" << endl;
    cout << "internal product between e and w " << equal*w << endl;
    cout << "internal product between e and v " << equal*v << endl;
    break;

  case 14:
    //**********************************************//
    // evalNormalVector
    //**********************************************//
    v.Northing = 1;
    v.Easting = 0;
    v.Downing = 0;

    w.Northing = 1;
    w.Easting = 1;
    w.Downing = 1;

    z.Northing = 1;
    z.Easting = 2;
    z.Downing = 3;

    equal = map.evalNormalVector(v,w,z);
    equal.norm();
    equal.display();
    cout << "internal product between e and v-w " << equal*(v-w) << endl;    
    cout << "internal product between e and v-z " << equal*(v-z) << endl;    
    cout << "internal product between e and w-z " << equal*(w-z) << endl;    

    break;

  case 15:
    //**********************************************//
    // evalNormalVectorCost
    //**********************************************//
    vec[0].Northing = 0;
    vec[0].Easting = 0;
    vec[0].Downing = 1;

    vec[1].Northing = 0.1;
    vec[1].Easting = -0.1;
    vec[1].Downing = 1;

    vec[2].Northing = 0;
    vec[2].Easting = 0;
    vec[2].Downing = 1;

    vec[3].Northing = 0;
    vec[3].Easting = 0.2;
    vec[3].Downing = 1;

    cout << "cost = " << map.evalNormalVectorCost(vec) << endl;

    break;

  case 16:
    //**********************************************//
    // distancePoint2Plane
    //**********************************************//

    // Plane
    v.Northing = 1; v.Easting = 0; v.Downing = 0;
    w.Northing = 0; w.Easting = 1; w.Downing = 0;
    z.Northing = 0; z.Easting = 0; z.Downing = 1;

    // Point outside the plane
    Po.Northing = 0; Po.Easting = 0; Po.Downing = 0;
    dist = map.distancePoint2Plane(v,w,z,Po);
    cout << "Dist = " << dist << endl;

    // Point outside the plane
    Po.Northing = 2; Po.Easting = 2; Po.Downing = 2;    
    dist = map.distancePoint2Plane(v,w,z,Po);
    cout << "Dist = " << dist << endl;

    break;

  case 17:
    //**********************************************//
    // distancePoint2Line
    //**********************************************//

    // Line
    v.Northing = 3; v.Easting = 0; v.Downing = 0;
    w.Northing = 0; w.Easting = 4 ; w.Downing = 0;

    // Point outside the plane
    Po.Northing = 0.1; Po.Easting = 4.01; Po.Downing = 0;
    aux_pair = map.distancePoint2Line(v,w,Po);
    cout << "Dist = " << aux_pair.second << endl;
    if(aux_pair.first){
      cout <<  "The projection crosses the line !!!" << endl;
    }
    else{
      cout <<  "The projection DON'T cross the line !!!" << endl;
    }

    break;

  case 18:
    //**********************************************//
    // evalOffTireCost
    //**********************************************//

    offTire[0]=0;
    offTire[1]=0;
    offTire[2]=0;
    offTire[3]=0;
    cout << "max = " << offTire[3] << " offTireCost = " << map.evalOffTireCost(offTire) << endl;

    offTire[3]=0.1;
    cout << "max = " << offTire[3] << " offTireCost = " << map.evalOffTireCost(offTire) << endl;

    offTire[3]=0.4;
    cout << "max = " << offTire[3] << " offTireCost = " << map.evalOffTireCost(offTire) << endl;

    offTire[3]=0.5;
    cout << "max = " << offTire[3] << " offTireCost = " << map.evalOffTireCost(offTire) << endl;

    offTire[3]=0.8;
    cout << "max = " << offTire[3] << " offTireCost = " << map.evalOffTireCost(offTire) << endl;

    offTire[3]=1;
    cout << "max = " << offTire[3] << " offTireCost = " << map.evalOffTireCost(offTire) << endl;

    offTire[3]=1.1;
    cout << "max = " << offTire[3] << " offTireCost = " << map.evalOffTireCost(offTire) << endl;

    break;

  case 19:

    //**********************************************//
    // allCellsUnder
    //**********************************************//
    /*
    map.clearMap();

    map.clearColor(RED_INDEX);
    map.clearColor(BLUE_INDEX);
    yaw = 0.489;
    pos.first = 8;
    pos.second = 8;
    cells = map.allCellsUnder(pos,yaw);
    map.display();
    cout << "yaw = " << yaw*180/M_PI << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    map.clearColor(RED_INDEX);
    map.clearColor(BLUE_INDEX);
    yaw = 1.178;
    pos.first = 8;
    pos.second = 8;
    cells = map.allCellsUnder(pos,yaw);
    map.display();
    cout << "yaw = " << yaw*180/M_PI << endl;
    */
    break;

  case 20:
    //**********************************************//
    // allCellsUnderCost
    //**********************************************//
    
    //debug
    //cout << "about to clear map\n";
    map.clearMap();
    /*
    newCell.value = 1;
    newCell.color = 1;
    for(i=0;i<15;i++){
      for(j=0;j<15;j++){
        map.setCellDataRC(i,j,newCell);
      }
    }
    */
    map.clearColor(RED_INDEX);
    map.clearColor(BLUE_INDEX);
    //debug
    //cout << "cleared colours\n";
    yaw = PI/3;
    pos.first = 5;
    pos.second = 10;
    newCell.value = 0;
    newCell.color = RED_INDEX;
    pos.first = 6;
    pos.second = 11;
    //debug
    //cout << "made it to setCellDataRC\n";
    map.setCellDataRC((pos.first+1), (pos.second+1), newCell);
    map.setCellDataRC(pos.first,pos.second,newCell);
    //debug
    //cout << "cells pos and one next to it are set.\n";
    cout << "Obstcle of " << newCell.value << "m => ";
    cout << "Cost = " << map.allCellsUnderCost(pos,yaw) << endl;
    newCell.value = 0.1;
    map.setCellDataRC(pos.first,pos.second,newCell);
    cout << "Obstcle of " << newCell.value << "m => ";
    cout << "Cost = " << map.allCellsUnderCost(pos,yaw) << endl;
    newCell.value = 0.2;
    map.setCellDataRC(pos.first,pos.second,newCell);
    cout << "Obstcle of " << newCell.value << "m => ";
    cout << "Cost = " << map.allCellsUnderCost(pos,yaw) << endl;
    newCell.value = 0.3;
    map.setCellDataRC(pos.first,pos.second,newCell);
    cout << "Obstcle of " << newCell.value << "m => ";
    cout << "Cost = " << map.allCellsUnderCost(pos,yaw) << endl;
    newCell.value = 0.5;
    map.setCellDataRC(pos.first,pos.second,newCell);
    cout << "Obstcle of " << newCell.value << "m => ";
    cout << "Cost = " << map.allCellsUnderCost(pos,yaw) << endl;
    newCell.value = 1.5;
    map.setCellDataRC(pos.first,pos.second,newCell);
    cout << "Obstcle of " << newCell.value << "m => ";
    cout << "Cost = " << map.allCellsUnderCost(pos,yaw) << endl;

    break;

  case 21:
    //**********************************************//
    // RowCol2UTM
    //**********************************************//
    map.clearMap();

    system("clear");
    newCell.value = 1;
    newCell.color = 1;
    for(i=0;i<15;i++){
      for(j=0;j<15;j++){
        map.setCellDataRC(i,j,newCell);
      }
    }

    // Assiging a cell out of range
    system("clear");
    map.display();
    cout << "Assiging a cell out of range" << endl << endl;
    pos.first = -1;
    pos.second = 1;
    NorthEast = map.RowCol2UTM(pos);
    cout << "row = " << pos.first << " col = " << pos.second << " UTM: north = " << NorthEast.first << "  east = " << NorthEast.second << endl;
    pos.first = 1;
    pos.second = -1;
    NorthEast = map.RowCol2UTM(pos);
    cout << "row = " << pos.first << " col = " << pos.second << " UTM: north = " << NorthEast.first << "  east = " << NorthEast.second << endl;
    pos.first = 4;
    pos.second = 16;
    NorthEast = map.RowCol2UTM(pos);
    cout << "row = " << pos.first << " col = " << pos.second << " UTM: north = " << NorthEast.first << "  east = " << NorthEast.second << endl;
    pos.first = 15;
    pos.second = 1;
    NorthEast = map.RowCol2UTM(pos);
    cout << "row = " << pos.first << " col = " << pos.second << " UTM: north = " << NorthEast.first << "  east = " << NorthEast.second << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // Now Existent cells
    system("clear");
    map.display();
    cout << "Now Existent cells" << endl << endl;
    pos.first = 5;
    pos.second = 2;
    NorthEast = map.RowCol2UTM(pos);
    cout << "row = " << pos.first << " col = " << pos.second << " UTM: north = " << NorthEast.first << "  east = " << NorthEast.second << endl;
    pos.first = 13;
    pos.second = 9;
    NorthEast = map.RowCol2UTM(pos);
    cout << "row = " << pos.first << " col = " << pos.second << " UTM: north = " << NorthEast.first << "  east = " << NorthEast.second << endl;
    pos.first = 1;
    pos.second = 0;
    NorthEast = map.RowCol2UTM(pos);
    cout << "row = " << pos.first << " col = " << pos.second << " UTM: north = " << NorthEast.first << "  east = " << NorthEast.second << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // Moving the car 2m to the north and 2m to the west  and returning the same cells
    state.Northing = 2;
    state.Easting = -2;
    pos.first = 1;
    pos.second = 1;
    map.updateFrame(state,true);
    system("clear");
    map.display();
    cout << "Moving the car 2m to the north and 2m to the west  and returning the same cells" << endl << endl;
    pos.first = 5;
    pos.second = 2;
    NorthEast = map.RowCol2UTM(pos);
    cout << "row = " << pos.first << " col = " << pos.second << " UTM: north = " << NorthEast.first << "  east = " << NorthEast.second << endl;
    pos.first = 13;
    pos.second = 9;
    NorthEast = map.RowCol2UTM(pos);
    cout << "row = " << pos.first << " col = " << pos.second << " UTM: north = " << NorthEast.first << "  east = " << NorthEast.second << endl;
    pos.first = 1;
    pos.second = 0;
    NorthEast = map.RowCol2UTM(pos);
    cout << "row = " << pos.first << " col = " << pos.second << " UTM: north = " << NorthEast.first << "  east = " << NorthEast.second << endl;
    
    break;

  case 22:
    //**********************************************//
    // obstacleAvoidance
    //**********************************************//

    /*
    newCell.value = 0;
    newCell.color = 0;
    for(i=0;i<25;i++){
      for(j=0;j<25;j++){
        mapCorridor.setCellDataRC(i,j,newCell);
      }
    }
    */

    newCell.color = GREEN_INDEX;
    newCell.value = 1;
    pos.first = 17;
    pos.second = 12;
    //mapCorridor.setCellDataRC(pos.first,pos.second,newCell);
    mapCorridor.initPathEvaluationCriteria(false,true,true);

    pos.first = 0; pos.second = 0;
    add.first = mapCorridor.getNumRows(); add.second = mapCorridor.getNumCols();
    // 1.a Straight
    phi = 0;
    state.Yaw = 0;
    cout << "YAW = 0  PHI = 0" << endl;
    distObs = mapCorridor.obstacleAvoidance(phi);
    cout << "dist = " << dist;
    cout << " PRESS ...";
    fflush(stdin);
    getchar();

    // 1.b Straight
    phi = PI/6;
    state.Yaw = PI;
    cout << "YAW = 0  PHI = pi/6" << endl;
    mapCorridor.Copy_VState(state);
    distObs = mapCorridor.obstacleAvoidance(phi);
    cout << "dist = " << dist;
    cout << " PRESS ...";
    fflush(stdin);
    getchar();

    // 1.c Straight
    phi = -PI/6;
    state.Yaw = 0;
    cout << "YAW = 0  PHI = -pi/6" << endl;
    mapCorridor.Copy_VState(state);
    mapCorridor.paintCorridor(pos,add);
    mapCorridor.clearColor(RED_INDEX);
    distObs = mapCorridor.obstacleAvoidance(phi);
    cout << "dist = " << dist;
    cout << " PRESS ...";
    fflush(stdin);
    getchar();

    // 1.a Curve
    phi = 0;
    state.Yaw = PI + PI/4;
    cout << "YAW = pi + pi/4  PHI = 0" << endl;
    mapCorridor.Copy_VState(state);
    mapCorridor.paintCorridor(pos,add);
    mapCorridor.clearColor(RED_INDEX);
    distObs = mapCorridor.obstacleAvoidance(phi);
    cout << "dist = " << dist;
    cout << " PRESS ...";
    fflush(stdin);
    getchar();

    // 1.b Curve
    phi = PI/6;
    state.Yaw = PI + PI/4;
    cout << "YAW = pi + pi/4  PHI = pi/6" << endl;
    mapCorridor.Copy_VState(state);
    mapCorridor.paintCorridor(pos,add);
    mapCorridor.clearColor(RED_INDEX);
    distObs = mapCorridor.obstacleAvoidance(phi);
    cout << "dist = " << dist;
    cout << " PRESS ...";
    fflush(stdin);
    getchar();

    // 1.c Curve
    phi = -PI/6;
    state.Yaw = PI + PI/4;
    cout << "YAW = pi + pi/4  PHI = -pi/6" << endl;
    mapCorridor.Copy_VState(state);
    mapCorridor.paintCorridor(pos,add);
    mapCorridor.clearColor(RED_INDEX);
    distObs = mapCorridor.obstacleAvoidance(phi);
    cout << "dist = " << dist << endl;

    break;

  case 23:
    //**********************************************//
    // GENERATE_ARBITER_VOTES
    //**********************************************//

    map.initMap(200,200,0.5,0.5,true);
    map.initPathEvaluationCriteria(false,false,false,false,false,false,true,false);
    state.Northing = 3777000 - 2.5*cos(M_PI/4);
    state.Easting = 396000 + 2.5*sin(M_PI/4);;
    state.Yaw = 0;
    map.updateFrame(state,true);
    numArcs = 25;
    arcs.clear();
    arcs.reserve(numArcs);
    for(i = -(int)(numArcs/2); i <= (int)(numArcs/2); i++) {
      arcs.push_back(2*i*USE_MAX_STEER/numArcs);
    }
    arbiterVotes = map.generateArbiterVotes(numArcs,arcs);
    cout << "NEXT WAYPOINT = " << map.getNextWaypoint() << endl;
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    state.Northing = 3777000 + 5*cos(M_PI/4);
    state.Easting = 396000 - 5*sin(M_PI/4);;
    state.Yaw = M_PI/4;
    map.updateFrame(state,true);
    arbiterVotes = map.generateArbiterVotes(numArcs,arcs);
    cout << "NEXT WAYPOINT = " << map.getNextWaypoint() << endl;
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    state.Northing = 3777000 + 7.5*cos(M_PI/4);
    state.Easting = 396000 - 7.5*sin(M_PI/4);;
    state.Yaw = M_PI/4;
    map.updateFrame(state,true);
    arbiterVotes = map.generateArbiterVotes(numArcs,arcs);
    cout << "NEXT WAYPOINT = " << map.getNextWaypoint() << endl;
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    state.Northing = 3777000 + 10*cos(M_PI/4);
    state.Easting = 396000 - 10*sin(M_PI/4);;
    state.Yaw = M_PI/4;
    map.updateFrame(state,true);
    arbiterVotes = map.generateArbiterVotes(numArcs,arcs);
    cout << "NEXT WAYPOINT = " << map.getNextWaypoint() << endl;
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    state.Northing = 3777000 + 15*cos(M_PI/4);
    state.Easting = 396000 - 15*sin(M_PI/4);;
    state.Yaw = M_PI/4;
    map.updateFrame(state,true);
    arbiterVotes = map.generateArbiterVotes(numArcs,arcs);
    cout << "NEXT WAYPOINT = " << map.getNextWaypoint() << endl;
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    break;
    /*
     * Test saveMATFile saves the map in the MATLAB format.
     */

  case 24:

    char filename[100];
    char variable_name[100];
    char message[1000];
    for(int index = 0; index < 10000; index++)
      {
	for(int jndex = 0; jndex < 10000; jndex++)
	  {
	    newCell.value = index*jndex/100000000;
	    newCell.color = (index*jndex)%4;
	    map.setCellDataRC(index, jndex, newCell);
	  }
      }
    map.display();
    cout << "Please enter the name of a file to which to save the map.\n";
    cin >> filename;
    cout << "Please enter the name of a variable in which to save the map.\n";
    cin >> variable_name;
    cout << "Please enter a message to save with the map.\nPlease use _ instead of space.\n";
    cin >> message;
    map.saveMATFile(filename, message, variable_name);
    cout << "We have now saved " << variable_name << " to " << filename << " with the message \n" << message << endl; 

    break;

  case 25:

    //**********************************************//
    // Load MAT file
    //**********************************************//

    ret = map.loadMATFile("sa_test.m");
    cout << "Please enter the name of a file to which to save the map.\n";
    cin >> filename;
    cout << "Please enter the name of a variable in which to save the map.\n";
    cin >> variable_name;
    cout << "Please enter a message to save with the map.\nPlease use _ instead of space.\n";
    cin >> message;
    map.saveMATFile(filename, message, variable_name);
    if(ret){
      cout << "INVALID MAT FILE" << endl;
    }
    else{
      cout << "MAT FILE OK !!!" << endl;
    }

    break;

  case 26:

    //**********************************************//
    // IS_INSIDE_WAYPOINT_RADIUS
    //**********************************************//
    /*
    targetPoints = map.getTargetPoints();
    for(ind=0;ind<10;ind++){
      targetPoints[ind].display();
    }
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    for(EAST=0;EAST<15*5;EAST+=5){
      cout << "(EAST,NORTH)" << endl;
      for(NORTH=0;NORTH<15*5;NORTH+=5){
        for(ind=0;ind<10;ind++){
          printf("(%2d,%2d,",EAST,NORTH);
          NorthEast.second  = EAST;
          NorthEast.first = NORTH;
          aux = map.isInsideWaypointRadius(ind,NorthEast);
          if(aux){
            cout << "INN) ";
          }
          else{
            cout << "OUT) ";
          }
        }
        cout << endl;
      }
      cout << "PRESS ..." << endl;
      fflush(stdin);
      getchar();
      system("clear");
      NorthEast.second += 5;
    }
    */
    targetPoints = map.getTargetPoints();
    map.initMap(500,500,1.5,1.5,true);
    state.Easting =targetPoints[1].Easting;
    state.Northing =targetPoints[1].Northing;;
    map.updateFrame(state,true);
    map.saveMATFile("test_map","isInsideWaypoint","theMap");
    
    break;

  case 27:

    //**********************************************//
    // IS_INSIDE_CORRIDOR
    //**********************************************//
    targetPoints = map.getTargetPoints();
    for(ind=0;ind<10;ind++){
      targetPoints[ind].display();
    }
    NorthEast.second = 5;
    NorthEast.first = 15;
    aux = map.isInsideCorridor(0,NorthEast);
    cout << "IT HAS TO BE OUTSIDE .... PLEASE ..." << endl;
    if(aux)
      cout << "INSIDE" << endl;
    else
      cout << "OUTSIDE" << endl;

    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    for(EAST=0;EAST<15*5;EAST+=5){
      cout << "(EAST,NORTH)" << endl;
      for(NORTH=0;NORTH<15*5;NORTH+=5){
        for(ind=0;ind<10;ind++){
          printf("(%2d,%2d,",EAST,NORTH);
          NorthEast.second  = EAST;
          NorthEast.first = NORTH;
          aux = map.isInsideCorridor(ind,NorthEast);
          if(aux){
            cout << "INN) ";
          }
          else{
            cout << "OUT) ";
          }
        }
        cout << endl;
      }
      cout << "PRESS ..." << endl;
      fflush(stdin);
      getchar();
      system("clear");
      NorthEast.second += 5;
    }

    break;

  case 28:

    //**********************************************//
    // IS_INSIDE_PATH
    //**********************************************//

    targetPoints = map.getTargetPoints();
    for(ind=0;ind<10;ind++){
      targetPoints[ind].display();
    }
    NorthEast.second = 0;
    NorthEast.first = 0;
    ind=8;
    map.setLastWaypoint(ind);
    map.setNextWaypoint(ind+1);
     aux = map.isInsidePath(NorthEast);
    cout << "IT HAS TO BE OUTSIDE .... PLEASE ..." << endl;
    if(aux)
      cout << "INSIDE" << endl;
    else
      cout << "OUTSIDE" << endl;

    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    for(EAST=0;EAST<15*5;EAST+=5){
      cout << "(EAST,NORTH)" << endl;
      for(NORTH=0;NORTH<15*5;NORTH+=5){
        for(ind=0;ind<10;ind++){
          printf("(%2d,%2d,",EAST,NORTH);
          NorthEast.second  = EAST;
          NorthEast.first = NORTH;
          map.setLastWaypoint(ind);
          map.setNextWaypoint(ind+1);
          aux = map.isInsidePath(NorthEast);
          if(aux){
            cout << "INN) ";
          }
          else{
            cout << "OUT) ";
          }
        }
        cout << endl;
      }
      cout << "PRESS ..." << endl;
      fflush(stdin);
      getchar();
      system("clear");
    }

    break;

  case 29:

    //**********************************************//
    // PAINT_CORRIDOR
    //**********************************************//
    system("clear");
    state.Easting =396000;
    state.Northing =3777000;
    mapPaint.initMap(100,100,0.5,0.5,true);
    mapPaint.updateFrame(state,true);
    mapPaint.saveMATFile("up1","test","theMap");
    state.Easting =396035;
    state.Northing =3777035;
    mapPaint.updateFrame(state,true);
    mapPaint.saveMATFile("up2","test","theMap");
    state.Easting =396065;
    state.Northing =3777055;
    mapPaint.updateFrame(state,true);
    mapPaint.saveMATFile("up3","test","theMap");

    state.Easting =396065;
    state.Northing =3777055;
    mapPaint.initMap(100,100,0.5,0.5,true);
    mapPaint.updateFrame(state,true);
    mapPaint.saveMATFile("down1","test","theMap");
    state.Easting =396030;
    state.Northing =3777020;
    mapPaint.updateFrame(state,true);
    mapPaint.saveMATFile("down2","test","theMap");
    state.Easting =396010;
    state.Northing =3777000;
    mapPaint.updateFrame(state,true);
    mapPaint.saveMATFile("down3","test","theMap");

    /*
    targetPoints = map.getTargetPoints();
    i=0;
    state.Easting =targetPoints[1].Easting;
    state.Northing =targetPoints[1].Northing;
    mapCorridor.initMap(25,25,0.7,0.7,);
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    
    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    i--;
    state.Easting =SIMULATOR_INITIAL_EASTING+i*0.3;
    state.Northing =SIMULATOR_INITIAL_NORTHING+i*0.3;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    */

    break;

  case 30:

    //********************************************************//
    // PAINT_CORRIDOR + UPDATE_FRAME + GENERATE_ARBITER_VOTES
    //********************************************************//
    system("clear");
    numArcs = 25;
    arcs.clear();
    arcs.reserve(numArcs);
    for(i = -(int)(numArcs/2); i <= (int)(numArcs/2); i++) {
      arcs.push_back(2*i*USE_MAX_STEER/numArcs);
      //cout << "arcs[" << i << "] = " << 2*i*USE_MAX_STEER/numArcs << endl;
    }
 
    // First testing
    state.Yaw = -PI/2;
    mapCorridor.updateFrame(state,true);
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << "YAW = " << 180*state.Yaw/PI << endl;
    cout << "lastWaypoint = " << mapCorridor.getLastWaypoint() << " nextWaypoint = " << mapCorridor.getNextWaypoint() << endl;
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %02.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %02.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // Second testing
    state.Yaw = PI/6;
    mapCorridor.updateFrame(state,true);
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << "YAW = " << 180*state.Yaw/PI << endl;
    cout << "lastWaypoint = " << mapCorridor.getLastWaypoint() << " nextWaypoint = " << mapCorridor.getNextWaypoint() << endl;
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    system("clear");
    state.Yaw = PI/8;
    state.Northing = 9;
    state.Easting = 11;
    mapCorridor.updateFrame(state,true);
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << "YAW = " << 180*state.Yaw/PI << endl;
    cout << "lastWaypoint = " << mapCorridor.getLastWaypoint() << " nextWaypoint = " << mapCorridor.getNextWaypoint() << endl;
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Yaw = PI/8;
    state.Northing = 11;
    state.Easting = 12;
    mapCorridor.updateFrame(state,true);
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << "YAW = " << 180*state.Yaw/PI << endl;
    cout << "lastWaypoint = " << mapCorridor.getLastWaypoint() << " nextWaypoint = " << mapCorridor.getNextWaypoint() << endl;
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Yaw = PI/8;
    state.Northing = 13;
    state.Easting = 12;
    mapCorridor.updateFrame(state,true);
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << "YAW = " << 180*state.Yaw/PI << endl;
    cout << "lastWaypoint = " << mapCorridor.getLastWaypoint() << " nextWaypoint = " << mapCorridor.getNextWaypoint() << endl;
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Yaw = PI/8;
    state.Northing = 15;
    state.Easting = 15; 
    mapCorridor.updateFrame(state,true);
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << "YAW = " << 180*state.Yaw/PI << endl;
    cout << "lastWaypoint = " << mapCorridor.getLastWaypoint() << " nextWaypoint = " << mapCorridor.getNextWaypoint() << endl;
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Yaw = PI/8;
    state.Northing = 20;
    state.Easting = 15;
    mapCorridor.updateFrame(state,true);
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << "YAW = " << 180*state.Yaw/PI << endl;
    cout << "lastWaypoint = " << mapCorridor.getLastWaypoint() << " nextWaypoint = " << mapCorridor.getNextWaypoint() << endl;
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Yaw = PI/8;
    state.Northing = 25;
    state.Easting = 20;
    mapCorridor.updateFrame(state,true);
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << "YAW = " << 180*state.Yaw/PI << endl;
    cout << "lastWaypoint = " << mapCorridor.getLastWaypoint() << " nextWaypoint = " << mapCorridor.getNextWaypoint() << endl;
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    mapCorridor.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    system("clear");
    state.Yaw = PI/8;
    state.Northing = 30;
    state.Easting = 25;
    mapCorridor.updateFrame(state,true);
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << "YAW = " << 180*state.Yaw/PI << endl;
    cout << "lastWaypoint = " << mapCorridor.getLastWaypoint() << " nextWaypoint = " << mapCorridor.getNextWaypoint() << endl;
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    mapCorridor.display();

    break;
  case 31:

    //**********************************************//
    // P E R F O R M A N C E
    //**********************************************//

    // setCellDataRC
    pos.first = 9;
    pos.second = 9;
    cout << "setCellDataRC" << endl;
    clk.markTime();
    map100.setCellDataRC(pos.first,pos.second,newCell);
    
    t1[3] = (k*clk.getDiffTime());
    clk.markTime();
    map500.setCellDataRC(pos.first,pos.second,newCell);
    
    t2[3] = (k*clk.getDiffTime());
    clk.markTime();
    map1000.setCellDataRC(pos.first,pos.second,newCell);
    
    t3[3] = (k*clk.getDiffTime());    

    // setCellDataUTM
    Northing = 10;
    Easting = 10;
    cout << "setCellDataUTM" << endl;
    clk.markTime();
    map100.setCellDataUTM(Northing,Easting,newCell);
    
    t1[4] = (k*clk.getDiffTime());
    clk.markTime();
    map500.setCellDataUTM(Northing,Easting,newCell);
    
    t2[4] = (k*clk.getDiffTime());
    clk.markTime();
    map1000.setCellDataUTM(Northing,Easting,newCell);
    
    t3[4] = (k*clk.getDiffTime());    

    // setCellDataFrameRef
    ahead = 30;
    aside = 30;
    cout << "setCellDataFrameRef" << endl;
    clk.markTime();
    map100.setCellDataFrameRef(ahead,aside,newCell);
    
    t1[5] = (k*clk.getDiffTime());
    clk.markTime();
    map500.setCellDataFrameRef(ahead,aside,newCell);
    
    t2[5] = (k*clk.getDiffTime());
    clk.markTime();
    map1000.setCellDataFrameRef(ahead,aside,newCell);
    
    t3[5] = (k*clk.getDiffTime());    

    // getCellDataRC
    cout << "getCellDataRC" << endl;
    clk.markTime();
    compCell = map100.getCellDataRC(pos.first,pos.second);
    
    t1[0] = (k*clk.getDiffTime());
    clk.markTime();
    compCell = map500.getCellDataRC(pos.first,pos.second);
    
    t2[0] = (k*clk.getDiffTime());
    clk.markTime();
    compCell = map1000.getCellDataRC(pos.first,pos.second);
    
    t3[0] = (k*clk.getDiffTime());    

    // getCellDataUTM
    cout << "getCellDataUTM" << endl;
    clk.markTime();
    compCell = map100.getCellDataUTM(Northing,Easting);
    
    t1[1] = (k*clk.getDiffTime());
    clk.markTime();
    compCell = map500.getCellDataUTM(Northing,Easting);
    
    t2[1] = (k*clk.getDiffTime());
    clk.markTime();
    compCell = map1000.getCellDataUTM(Northing,Easting);
    
    t3[1] = (k*clk.getDiffTime());    

    // getCellDataFrameRef
    cout << "getCellDataFrameRef" << endl;
    clk.markTime();
    compCell = map100.getCellDataFrameRef(ahead,aside);
    
    t1[2] = (k*clk.getDiffTime());
    clk.markTime();
    compCell = map500.getCellDataFrameRef(ahead,aside);
    
    t2[2] = (k*clk.getDiffTime());
    clk.markTime();
    compCell = map1000.getCellDataFrameRef(ahead,aside);
    
    t3[2] = (k*clk.getDiffTime());    

    // setCellListDataRC
    cout << "setCellListDataRC" << endl;
    clk.markTime();
    map100.setCellListDataRC(pos.first, pos.second, 10.0);
    
    t1[6] = (k*clk.getDiffTime());
    clk.markTime();
    map500.setCellListDataRC(pos.first, pos.second, 10.0);
    
    t2[6] = (k*clk.getDiffTime());
    clk.markTime();
    map1000.setCellListDataRC(pos.first, pos.second, 10.0);
    
    t3[6] = (k*clk.getDiffTime());

    // setCellListDataUTM
    cout << "setCellListDataUTM" << endl;
    clk.markTime();
    map100.setCellListDataUTM(Northing,Easting, 10.0);
    
    t1[7] = (k*clk.getDiffTime());
    clk.markTime();
    map500.setCellListDataUTM(Northing,Easting, 10.0);
    
    t2[7] = (k*clk.getDiffTime());
    clk.markTime();
    map1000.setCellListDataUTM(Northing,Easting, 10.0);
    
    t3[7] = (k*clk.getDiffTime());    

    // setCellListDataFrameRef
    cout << "setCellListDataFrameRef" << endl;
    clk.markTime();
    map100.setCellListDataFrameRef(ahead,aside, 10.0);
    
    t1[8] = (k*clk.getDiffTime());
    clk.markTime();
    map500.setCellListDataFrameRef(ahead,aside, 10.0);
    
    t2[8] = (k*clk.getDiffTime());
    clk.markTime();
    map1000.setCellListDataFrameRef(ahead,aside, 10.0);
    
    t3[8] = (k*clk.getDiffTime());    

    // evalCellsSV
    cout << "evalCellsSV" << endl;
    for(i=0;i<map100.getNumRows();i++){
      for(j=0;j<map100.getNumCols();j++){
        pos.first = i; pos.second = j;
        map100.setCellDataRC(pos.first,pos.second,newCell);
      }
    }
    clk.markTime();
    map100.evaluateCellsSV(1,0.0);
    
    t1[9] = (k*clk.getDiffTime());
    for(i=0;i<map500.getNumRows();i++){
      for(j=0;j<map500.getNumCols();j++){
        pos.first = i; pos.second = j;
        map500.setCellDataRC(pos.first,pos.second,newCell);
      }
    }
    clk.markTime();
    map500.evaluateCellsSV(1,0.0);
    
    t2[9] = (k*clk.getDiffTime());
    for(i=0;i<map1000.getNumRows();i++){
      for(j=0;j<map1000.getNumCols();j++){
        pos.first = i; pos.second = j;
        map1000.setCellDataRC(pos.first,pos.second,newCell);
      }
    }
    clk.markTime();
    map1000.evaluateCellsSV(1,0.0);
    
    t3[9] = (k*clk.getDiffTime());    

    // clearRow
    cout << "clearRow" << endl;
    clk.markTime();
    map100.clearRow(90);
    t1[10] = k*clk.getDiffTime();
    clk.markTime();
    map500.clearRow(90);
    
    t2[10] = (k*clk.getDiffTime());
    clk.markTime();
    map1000.clearRow(90);
    
    t3[10] = (k*clk.getDiffTime());    

    // clearCol
    cout << "clearCol" << endl;
    clk.markTime();
    map100.clearCol(90);
    
    t1[11] = (k*clk.getDiffTime());
    clk.markTime();
    map500.clearCol(90);
    
    t2[11] = (k*clk.getDiffTime());
    clk.markTime();
    map1000.clearCol(90);
    
    t3[11] = (k*clk.getDiffTime());    

    // clearMap
    cout << "clearMap" << endl;
    clk.markTime();
    map100.clearMap();
    
    t1[12] = (k*clk.getDiffTime());
    clk.markTime();
    map500.clearMap();
    
    t2[12] = (k*clk.getDiffTime());
    clk.markTime();
    map1000.clearMap();
    
    t3[12] = (k*clk.getDiffTime());    

    // evalNormalVector
    cout << "evalNormalVector" << endl;
    v.Northing = 1;
    v.Easting = 0;
    v.Downing = 0;

    w.Northing = 1;
    w.Easting = 1;
    w.Downing = 1;

    z.Northing = 1;
    z.Easting = 2;
    z.Downing = 3;

    clk.markTime();
    equal = map100.evalNormalVector(v,w,z);
    equal.norm();    
    
    t1[13] = (k*clk.getDiffTime());
    clk.markTime();
    equal = map500.evalNormalVector(v,w,z);
    equal.norm();    
    
    t2[13] = (k*clk.getDiffTime());
    clk.markTime();
    equal = map1000.evalNormalVector(v,w,z);
    equal.norm();    
    
    t3[13] = (k*clk.getDiffTime());    

    // evalNormalVectorCost
    cout << "evalNormalVectorCost" << endl;
    vec[0].Northing = 1;
    vec[0].Easting = 2;
    vec[0].Downing = 1;

    vec[1].Northing = 1;
    vec[1].Easting = 2;
    vec[1].Downing = 1;

    vec[2].Northing = 2;
    vec[2].Easting = 2;
    vec[2].Downing = 1;

    vec[3].Northing = 1;
    vec[3].Easting = 2;
    vec[3].Downing = 2;

    clk.markTime();
    map100.evalNormalVectorCost(vec);
    
    t1[14] = (k*clk.getDiffTime());
    clk.markTime();
    map500.evalNormalVectorCost(vec);
    
    t2[14] = (k*clk.getDiffTime());
    clk.markTime();
    map1000.evalNormalVectorCost(vec);
    
    t3[14] = (k*clk.getDiffTime());    

    // distancePoint2Plane
    cout << "distancePoint2Plane" << endl;
    v.Northing = 1; v.Easting = 0; v.Downing = 0;
    w.Northing = 0; w.Easting = 1; w.Downing = 0;
    z.Northing = 0; z.Easting = 0; z.Downing = 1;
    Po.Northing = 0; Po.Easting = 0; Po.Downing = 0;

    clk.markTime();
    map100.distancePoint2Plane(v,w,z,Po);
    
    t1[15] = (k*clk.getDiffTime());
    clk.markTime();
    map500.distancePoint2Plane(v,w,z,Po);
    
    t2[15] = (k*clk.getDiffTime());
    clk.markTime();
    map1000.distancePoint2Plane(v,w,z,Po);
    
    t3[15] = (k*clk.getDiffTime());    

    // distancePoint2Line
    cout << "distancePoint2Line" << endl;
    v.Northing = 3; v.Easting = 0; v.Downing = 0;
    w.Northing = 0; w.Easting = 4 ; w.Downing = 0;
    Po.Northing = 0.1; Po.Easting = 4.01; Po.Downing = 0;

    clk.markTime();
    map100.distancePoint2Line(v,w,Po);
    
    t1[16] = (k*clk.getDiffTime());
    clk.markTime();
    map500.distancePoint2Line(v,w,Po);
    
    t2[16] = (k*clk.getDiffTime());
    clk.markTime();
    map1000.distancePoint2Line(v,w,Po);
    
    t3[16] = (k*clk.getDiffTime());    

    // evalOffTireCost
    cout << "evalOffTireCost" << endl;
    offTire[0]=0.2;
    offTire[1]=0.1;
    offTire[2]=0.2;
    offTire[3]=0.1;

    clk.markTime();
    map100.evalOffTireCost(offTire);
    
    t1[17] = (k*clk.getDiffTime());
    clk.markTime();
    map500.evalOffTireCost(offTire);
    
    t2[17] = (k*clk.getDiffTime());
    clk.markTime();
    map1000.evalOffTireCost(offTire);
    
    t3[17] = (k*clk.getDiffTime());    

    // maxInclinationCost
    cout << "maxInclinationCost" << endl;
    yaw = PI/4;
    pos.first = 5;
    pos.second = 10;
    clk.markTime();
    map100.maxInclinationCost(before,yaw);
    
    t1[18] = (k*clk.getDiffTime());
    clk.markTime();
    map500.maxInclinationCost(before,yaw);
    
    t2[18] = (k*clk.getDiffTime());
    clk.markTime();
    map1000.maxInclinationCost(before,yaw);
    
    t3[18] = (k*clk.getDiffTime());    

    // allCellsUnderCost
    cout << "allCellsUnderCost" << endl;
    clk.markTime();
    map100.allCellsUnderCost(pos,yaw);
    
    t1[19] = (k*clk.getDiffTime());
    clk.markTime();
    map500.allCellsUnderCost(pos,yaw);
    
    t2[19] = (k*clk.getDiffTime());
    clk.markTime();
    map1000.allCellsUnderCost(pos,yaw);
    
    t3[19] = (k*clk.getDiffTime());    

    // obstacleAvoidance
    cout << "obstacleAvoidance" << endl;
    phi = (M_PI/10);
    clk.markTime();
    distObs = map100.obstacleAvoidance(phi);
    
    t1[20] = (k*clk.getDiffTime());
    clk.markTime();
    distObs = map500.obstacleAvoidance(phi);
    
    t2[20] = (k*clk.getDiffTime());
    clk.markTime();
    distObs = map1000.obstacleAvoidance(phi);
    
    t3[20] = (k*clk.getDiffTime());    

    // generateArbiterVotes
    cout << "generateArbiterVotes" << endl;
    numArcs = 25;
    arbiterVotes.reserve(numArcs+2);
    arcs.reserve(numArcs+2);
    for(i=-(int)round(numArcs/2);i<numArcs/2;i++){
      arcs.push_back(i*M_PI/180);
    }
    clk.markTime();
    arbiterVotes = map100.generateArbiterVotes(numArcs,arcs);
    
    t1[21] = (k*clk.getDiffTime());
    clk.markTime();
    arbiterVotes = map500.generateArbiterVotes(numArcs,arcs);
    
    t2[21] = (k*clk.getDiffTime());
    clk.markTime();
    arbiterVotes = map1000.generateArbiterVotes(numArcs,arcs);
    
    t3[21] = (k*clk.getDiffTime());    

    // RowColFromDeltaRowCol
    cout << "RowColFromDeltaRowCol" << endl;
    before.first = map.getVehRow();
    before.second = map.getVehCol();
    add.first = 350;
    add.second = 350;
    clk.markTime();
    after = map100.RowColFromDeltaRowCol(before,add);
    
    t1[22] = (k*clk.getDiffTime());
    clk.markTime();
    after = map500.RowColFromDeltaRowCol(before,add);
    
    t2[22] = (k*clk.getDiffTime());
    clk.markTime();
    after = map1000.RowColFromDeltaRowCol(before,add);
    
    t3[22] = (k*clk.getDiffTime());

    // RowColFromDeltaPosition
    cout << "RowColFromDeltaPosition" << endl;
    north_dist = 20;
    east_dist = 20;
    clk.markTime();
    after = map100.RowColFromDeltaPosition(before,north_dist,east_dist);
    
    t1[23] = (k*clk.getDiffTime());
    clk.markTime();
    after = map500.RowColFromDeltaPosition(before,north_dist,east_dist);
    
    t2[23] = (k*clk.getDiffTime());
    clk.markTime();
    after = map1000.RowColFromDeltaPosition(before,north_dist,east_dist);
    
    t3[23] = (k*clk.getDiffTime());    

    // FrameRef2RowCol
    cout << "FrameRef2RowCol" << endl;
    clk.markTime();
    after = map100.FrameRef2RowCol(before,state.Yaw,ahead,aside);
    
    t1[24] = (k*clk.getDiffTime());
    clk.markTime();
    after = map500.FrameRef2RowCol(before,state.Yaw,ahead,aside);
    
    t2[24] = (k*clk.getDiffTime());
    clk.markTime();
    after = map1000.FrameRef2RowCol(before,state.Yaw,ahead,aside);
    
    t3[24] = (k*clk.getDiffTime());    

    // RowCol2UTM
    cout << "RowCol2UTM" << endl;
    clk.markTime();
    NorthEast = map100.RowCol2UTM(pos);
    
    t1[25] = (k*clk.getDiffTime());
    clk.markTime();
    NorthEast = map500.RowCol2UTM(pos);
    
    t2[25] = (k*clk.getDiffTime());
    clk.markTime();
    NorthEast = map1000.RowCol2UTM(pos);
    
    t3[25] = (k*clk.getDiffTime());    

    // UTM2RowCol
    cout << "UTM2RowCol" << endl;
    clk.markTime();
    after = map100.UTM2RowCol(Northing,Easting);
    
    t1[26] = (k*clk.getDiffTime());
    clk.markTime();
    after = map500.UTM2RowCol(Northing,Easting);
    
    t2[26] = (k*clk.getDiffTime());
    clk.markTime();
    after = map1000.UTM2RowCol(Northing,Easting);
    
    t3[26] = (k*clk.getDiffTime());    

    // Copy_VState
    cout << "Copy_VState" << endl;
    clk.markTime();
    map100.Copy_VState(state);
    
    t1[27] = (k*clk.getDiffTime());
    clk.markTime();
    map500.Copy_VState(state);
    
    t2[27] = (k*clk.getDiffTime());
    clk.markTime();
    map1000.Copy_VState(state);
    
    t3[27] = (k*clk.getDiffTime());    

    // isInsideCorridor
    cout << "isInsideCorridor" << endl;
    NorthEast.second  = 10;
    NorthEast.first = 20;
    clk.markTime();
    aux = map100.isInsideCorridor(3,NorthEast);
    
    t1[28] = (k*clk.getDiffTime());
    clk.markTime();
    aux = map500.isInsideCorridor(3,NorthEast);
    
    t2[28] = (k*clk.getDiffTime());
    clk.markTime();
    aux = map1000.isInsideCorridor(3,NorthEast);
    
    t3[28] = (k*clk.getDiffTime());    

    // isInsideWaypointRadius
    cout << "isInsideWaypointRadius" << endl;
    clk.markTime();
    aux = map100.isInsideWaypointRadius(3,NorthEast);
    
    t1[29] = (k*clk.getDiffTime());
    clk.markTime();
    aux = map500.isInsideWaypointRadius(3,NorthEast);
    
    t2[29] = (k*clk.getDiffTime());
    clk.markTime();
    aux = map1000.isInsideWaypointRadius(3,NorthEast);
    
    t3[29] = (k*clk.getDiffTime());    

    // isInsidePath
    cout << "isInsidePath" << endl;
    clk.markTime();
    aux = map100.isInsidePath(NorthEast);
    
    t1[30] = (k*clk.getDiffTime());
    clk.markTime();
    aux = map500.isInsidePath(NorthEast);
    
    t2[30] = (k*clk.getDiffTime());
    clk.markTime();
    aux = map1000.isInsidePath(NorthEast);
    
    t3[30] = (k*clk.getDiffTime());    

    // paintCorridor
    cout << "paintCorridor" << endl;
    pos.first = 0;
    pos.second = 0;
    add.first = map100.getNumRows();
    add.second = map100.getNumCols();
    clk.markTime();
    map100.paintCorridor(pos,add);
    
    t1[31] = (k*clk.getDiffTime());
    add.first = map500.getNumRows();
    add.second = map500.getNumCols();
    clk.markTime();
    map500.paintCorridor(pos,add);
    
    t2[31] = (k*clk.getDiffTime());
    add.first = map1000.getNumRows();
    add.second = map1000.getNumCols();
    clk.markTime();
    map1000.paintCorridor(pos,add);
    
    t3[31] = (k*clk.getDiffTime());    

    // initMap
    cout << "initMap" << endl;
    clk.markTime();
    map100.initMap(100,100,1.0,1.0,true);
    
    t1[32] = (k*clk.getDiffTime());
    clk.markTime();
    map500.initMap(500,500,1.0,1.0,paint);
    
    t2[32] = (k*clk.getDiffTime());
    clk.markTime();
    map1000.initMap(1000,1000,1.0,1.0,paint);
    
    t3[32] = (k*clk.getDiffTime());    

    // updateFrame
    cout << "updateFrame" << endl;
    state.Northing = 99;
    state.Easting = 99;
    clk.markTime();
    map100.updateFrame(state,true);
    
    t1[33] = (k*clk.getDiffTime());
    state.Northing = 499;
    state.Easting = 499;
    clk.markTime();
    map500.updateFrame(state,true);
    
    t2[33] = (k*clk.getDiffTime());
    state.Northing = 999;
    state.Easting = 999;
    clk.markTime();
    map1000.updateFrame(state,true);
    
    t3[33] = (k*clk.getDiffTime());  

    system("clear");
    cout << "                        |   " << map100.getNumRows() << "x" << map100.getNumCols() << "   |   " << map500.getNumRows() << "x" << map500.getNumCols() << "   |   " << map1000.getNumRows() << "x" << map1000.getNumCols() << "   |" << endl;
    cout << "getCellDataRC          ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[0],t2[0],t3[0]);
    cout << "getCellDataUTM         "; 
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[1],t2[1],t3[1]);
    cout << "getCellDataFrameRef    ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[2],t2[2],t3[2]);
    cout << "setCellDataRC          ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[3],t2[3],t3[3]);
    cout << "setCellDataUTM         ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[4],t2[4],t3[4]);
    cout << "setCellDataFrameRef    ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[5],t2[5],t3[5]);
    cout << "setCellListDataRC      ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[6],t2[6],t3[6]);
    cout << "setCellListDataUTM     ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[7],t2[7],t3[7]);
    cout << "setCellListDataFrameRef";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[8],t2[8],t3[8]);
    cout << "evalCellsSV            ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[9],t2[9],t3[9]);
    cout << "clearRow               ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[10],t2[10],t3[10]);
    cout << "clearCol               ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[11],t2[11],t3[11]);
    cout << "clearMap               ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[12],t2[12],t3[12]);
    cout << "evalNormalVector       ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[13],t2[13],t3[13]);
    cout << "evalNormalVectorCost   ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[14],t2[14],t3[14]);
    cout << "distancePoint2Plane    ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[15],t2[15],t3[15]);
    cout << "distancePoint2Line     ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[16],t2[16],t3[16]);
    cout << "evalOffTireCost        ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[17],t2[17],t3[17]);
    cout << "maxInclinationCost     ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[18],t2[18],t3[18]);
    cout << "allCellsUnderCost      ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[19],t2[19],t3[19]);
    cout << "obstacleAvoidance      ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[20],t2[20],t3[20]);
    cout << "generateArbiterVotes   ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[21],t2[21],t3[21]);
    cout << "RowColFromDeltaRowCol  ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[22],t2[22],t3[22]);
    cout << "RowColFromDeltaPosition";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[23],t2[23],t3[23]);
    cout << "FrameRef2RowCol        ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[24],t2[24],t3[24]);
    cout << "RowCol2UTM             ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[25],t2[25],t3[25]);
    cout << "UTM2RowCol             ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[26],t2[26],t3[26]);
    cout << "Copy_VState            ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[27],t2[27],t3[27]);
    cout << "isInsideCorridor       ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[28],t2[28],t3[28]);
    cout << "isInsideWaypointRadius ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[29],t2[29],t3[29]);
    cout << "isInsidePath           ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[30],t2[30],t3[30]);
    cout << "paintCorridor          ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[31],t2[32],t3[31]);
    cout << "initMap                ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n",t1[32],t2[32],t3[32]);
    cout << "updateFrame            ";
    printf(" |  %09.0f  |  %09.0f  |  %09.0f  |\n\n",t1[33],t2[33],t3[33]);

    break;

  case 32:

    //**********************************************//
    // FOR vs RECURSIVE LOOP
    //**********************************************//

    n = 1000;
    d_value = 123.3456;
    f_value = 13.36;
    i_value = 256;
    s_value = 13;

    clk.markTime();
    for(i=0;i<n;i++){
      rec_fun(i,0,d_value);
    }
    cout << "ForxRec (double) " << (k*clk.getDiffTime()) << endl;
    getchar();

    clk.markTime();
    for(i=0;i<n;i++){
      for(j=0;j<n;j++){
        pos.first = i;
        pos.second = j;
        d_map[pos] = d_value*d_value;
      }
    }
    cout << "2xFor (double)" << (k*clk.getDiffTime()) << endl;

    clk.markTime();
    for(i=0;i<n;i++){
      for(j=0;j<n;j++){
        pos.first = i;
        pos.second = j;
        f_map[pos] = f_value*f_value;
      }
    }
    cout << "2xFor  (float)" << (k*clk.getDiffTime()) << endl;

    clk.markTime();
    for(i=0;i<n;i++){
      for(j=0;j<n;j++){
        pos.first = i;
        pos.second = j;
        i_map[pos] = i_value*i_value;
      }
    }
    cout << "2xFor  ( int )" << (k*clk.getDiffTime()) << endl;

    clk.markTime();
    for(i=0;i<n;i++){
      for(j=0;j<n;j++){
        pos.first = i;
        pos.second = j;
        s_map[pos] = s_value*s_value;
      }
    }
    cout << "2xFor  (short)" << (k*clk.getDiffTime()) << endl;

    break;

  case 33:

    //**********************************************//
    // CLEAR ROW
    //**********************************************//

    // Everything
    newCell.value = 1;
    newCell.color = 1;
    for(i=0;i<15;i++){
      for(j=0;j<15;j++){
        map.setCellDataRC(i,j,newCell);
      }
    }
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // But the first row
    map.clearRow(0);
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // But the last row
    map.clearRow(map.getNumRows()-1);
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // But a row in the middle
    map.clearRow(86);
    map.display();

    break;

  case 34:

    //**********************************************//
    // CLEAR COL
    //**********************************************//

    // Everything
    newCell.value = 1;
    newCell.color = 1;
    for(i=0;i<15;i++){
      for(j=0;j<15;j++){
        map.setCellDataRC(i,j,newCell);
      }
    }
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // But the first col
    map.clearCol(0);
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // But the last col
    map.clearCol(map.getNumCols()-1);
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // But a row in the middle
    map.clearCol(86);
    map.display();

    break;

  case 35:

    //**********************************************//
    // CLEAR COLOR
    //**********************************************//

    // Everything
    newCell.value = 1;
    newCell.color = 1;
    for(i=0;i<15;i++){
      for(j=0;j<15;j++){
        map.setCellDataRC(i,j,newCell);
      }
    }
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // But the first col
    map.clearColor(1);
    map.display();

    break;

  case 36:

    //**********************************************//
    // CLEAR MAP
    //**********************************************//

    // Everything
    newCell.value = 1;
    newCell.color = 1;
    for(i=0;i<15;i++){
      for(j=0;j<15;j++){
        map.setCellDataRC(i,j,newCell);
      }
    }
    map.display();
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    // But the first col
    map.clearMap();
    map.display();


    break;

  case 37:

    //**********************************************//
    // TARGET POINTS UPDATE
    //**********************************************//

    // Testing how many waypoints we are checking
    state.Northing = 3777000;
    state.Easting = 396000;
    mapPath.initMap(20,20,1,1,false);
    mapPath.initPathEvaluationCriteria(false,false,false,true,true);
    mapPath.updateFrame(state,true);
    cout << "finish updating the frame" << endl;
    getchar();
    numArcs = 25;
    arcs.clear();
    arcs.reserve(numArcs);
    for(i = -(int)(numArcs/2); i <= (int)(numArcs/2); i++) {
      arcs.push_back(2*i*USE_MAX_STEER/numArcs);
    }
    arbiterVotes = mapPath.generateArbiterVotes(numArcs,arcs);
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    state.Northing = 3777000 + 3;
    state.Easting = 396000;
    mapPath.updateFrame(state,true);
    line = "corridor";
    strcpy(filename,line.getStr());
    line = "message";
    strcpy(message,line.getStr());
    line = "test";
    strcpy(variable_name,line.getStr());
    mapPath.saveMATFile(filename, message, variable_name);
    cout << "finish updating the frame for waypoint at (2,0)" << endl;
    getchar();

    newCell.value = 0;
    newCell.color = 0;
    for(i=0;i<25;i++){
      for(j=0;j<25;j++){
        mapCorridor.setCellDataRC(i,j,newCell);
      }
    }
    numArcs = 25;
    arcs.clear();
    arcs.reserve(numArcs);
    for(i = -(int)(numArcs/2); i <= (int)(numArcs/2); i++) {
      arcs.push_back(2*i*USE_MAX_STEER/numArcs);
    }
    /*
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing << "," << state.Easting << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();
    */

    // Way to the first waypoint
    system("clear");
    state.Yaw = 0;
    state.Northing = 0;
    state.Easting = 0;
    mapCorridor.updateFrame(state,true);
    state.Northing = SIMULATOR_INITIAL_NORTHING + 180;
    state.Easting = SIMULATOR_INITIAL_EASTING;
    mapCorridor.updateFrame(state,true);
    newCell.value = 0;
    newCell.color = RED_INDEX;
    mapCorridor.setCellDataFrameRef(8,-1,newCell);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing << "," << state.Easting << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();

    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 50;
    state.Easting = SIMULATOR_INITIAL_EASTING + 50;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 110;
    state.Easting = SIMULATOR_INITIAL_EASTING + 70;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 140;
    state.Easting = SIMULATOR_INITIAL_EASTING + 110;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 200;
    state.Easting = SIMULATOR_INITIAL_EASTING + 150;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    // Way to the second waypoint
    system("clear");
    state.Yaw = atan((double)((350.0-150.0)/(400.0-200.0)));
    state.Northing = SIMULATOR_INITIAL_NORTHING + 250;
    state.Easting = SIMULATOR_INITIAL_EASTING + 200;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 300;
    state.Easting = SIMULATOR_INITIAL_EASTING + 250;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 350;
    state.Easting = SIMULATOR_INITIAL_EASTING + 300;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 400;
    state.Easting = SIMULATOR_INITIAL_EASTING + 350;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    // Way to the third waypoint
    system("clear");
    state.Yaw = atan((double)((100.0-350.0)/(500.0-400.0)));
    state.Northing = SIMULATOR_INITIAL_NORTHING + 450;
    state.Easting = SIMULATOR_INITIAL_EASTING + 245;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 500;
    state.Easting = SIMULATOR_INITIAL_EASTING + 100;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    fflush(stdin);
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    // Way to the fourth waypoint
    system("clear");
    state.Yaw = PI + atan((double)((400.0-100.0)/(100.0-500.0)));
    state.Northing = SIMULATOR_INITIAL_NORTHING + 450;
    state.Easting = SIMULATOR_INITIAL_EASTING + 130;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 275;
    state.Easting = SIMULATOR_INITIAL_EASTING + 260;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 250;
    state.Easting = SIMULATOR_INITIAL_EASTING + 290;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 200;
    state.Easting = SIMULATOR_INITIAL_EASTING + 330;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 100;
    state.Easting = SIMULATOR_INITIAL_EASTING + 400;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    // Way to the fifth waypoint
    system("clear");
    state.Yaw = atan((double)((600.0-400.0)/(250.0-100.0)));
    state.Northing = SIMULATOR_INITIAL_NORTHING + 140;
    state.Easting = SIMULATOR_INITIAL_EASTING + 450;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 175;
    state.Easting = SIMULATOR_INITIAL_EASTING + 500;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 220;
    state.Easting = SIMULATOR_INITIAL_EASTING + 560;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 250;
    state.Easting = SIMULATOR_INITIAL_EASTING + 600;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    // Way to the sixth waypoint
    system("clear");
    state.Yaw = atan((double)((700.0-600.0)/(400.0-250.0)));
    state.Northing = SIMULATOR_INITIAL_NORTHING + 300;
    state.Easting = SIMULATOR_INITIAL_EASTING + 640;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 350;
    state.Easting = SIMULATOR_INITIAL_EASTING + 675;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 400;
    state.Easting = SIMULATOR_INITIAL_EASTING + 700;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    // Way to the 7th waypint
    system("clear");
    state.Yaw = atan((double)((500.0-700.0)/(600.0-400.0)));
    state.Northing = SIMULATOR_INITIAL_NORTHING + 440;
    state.Easting = SIMULATOR_INITIAL_EASTING + 670;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 490;
    state.Easting = SIMULATOR_INITIAL_EASTING + 620;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 540;
    state.Easting = SIMULATOR_INITIAL_EASTING + 565;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 600;
    state.Easting = SIMULATOR_INITIAL_EASTING + 500;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    // way to the 8th waypoint
    system("clear");
    state.Yaw = atan((double)((700.0-600.0)/(600.0-500.0)));
    state.Northing = SIMULATOR_INITIAL_NORTHING + 650;
    state.Easting = SIMULATOR_INITIAL_EASTING + 550;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 700;
    state.Easting = SIMULATOR_INITIAL_EASTING + 600;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();



    //way to the last waypoint
    system("clear");
    state.Yaw = PI + atan((double)((600.0-700.0)/(700.0-600.0)));
    state.Northing = SIMULATOR_INITIAL_NORTHING + 640;
    state.Easting = SIMULATOR_INITIAL_EASTING + 670;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    cout << "PRESS ..." << endl;
    fflush(stdin);
    getchar();


    system("clear");
    state.Northing = SIMULATOR_INITIAL_NORTHING + 600;
    state.Easting = SIMULATOR_INITIAL_EASTING + 700;
    mapCorridor.updateFrame(state,true);
    mapCorridor.display();
    fflush(stdin);
    cout << "PRESS ... " << "LAST TARGET = " << mapCorridor.getLastWaypoint() << "  NEXT TARGET = " << mapCorridor.getNextWaypoint()
         << "  YAW = " << state.Yaw*180/PI
         << "  (N,E) = (" << state.Northing-SIMULATOR_INITIAL_NORTHING << "," << state.Easting-SIMULATOR_INITIAL_EASTING << ")"  << endl;
    getchar();
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;

    break;

  case 38:

    //**********************************************//
    // SAVE MAT FILE
    //**********************************************//    

    state.Easting = SIMULATOR_INITIAL_EASTING + 400;
    state.Northing = SIMULATOR_INITIAL_NORTHING + 400;
    bigMapCorridor.initMap(800,800,1.0,1.0,true);
    bigMapCorridor.updateFrame(state,true);
    bigMapCorridor.saveMATFile("MAT","genMap_test1","genMap_test1");

    break;

  case 39:

    //**********************************************//
    // GOODNESS SHAPE
    //**********************************************//    

    numArcs = 25;
    for(ind = 0;ind < numArcs; ind++){
      map.goodnessShape(LEFT_SIDE,ind,numArcs,-1);
    }
    getchar();
    for(ind = 0;ind < numArcs; ind++){
      map.goodnessShape(RIGHT_SIDE,ind,numArcs,-1);
    }
    getchar();
    for(ind = 0;ind < numArcs; ind++){
      map.goodnessShape(RIGHT_SIDE,ind,numArcs,0);
    }
    break;

  case 40:

    //**********************************************//
    // CHANGE COORDINATES
    //**********************************************//    
    a_ref.Northing = 0;
    a_ref.Easting = 0;
    a_ref.Downing = 0;
    b_ref.Northing = 0;
    b_ref.Easting = 1;
    b_ref.Downing = 0;

    a_car.Northing =  0;
    a_car.Easting =  0;
    a_car.Downing = 0;
    b_car.Northing =  1;
    b_car.Easting =  1;
    b_car.Downing = 0;

    map.changeCoordinates(a_ref,b_ref,a_car,b_car,ind);
    cout << "Ref axis" << endl;
    b_ref.display();
    cout << "Car axis" << endl;
    b_car.display();
    if(ind == LEFT_SIDE) cout << "VECTOR FIELD FROM -PI/2 TO 0" << endl;
    if(ind == RIGHT_SIDE) cout << "VECTOR FIELD FROM PI/2 TO 0" << endl;

    break;

  case 41:

    //**********************************************//
    // CHANGE COORDINATES
    //**********************************************//    

    numArcs = 25;
    state.Yaw = PI/4;
    targetPoints = mapCorridor.getTargetPoints();

    state.Northing = SIMULATOR_INITIAL_NORTHING;
    state.Easting = SIMULATOR_INITIAL_EASTING+5;
    mapCorridor.updateFrame(state,true);
    arcs.clear();
    arcs.reserve(numArcs);
    for(i = -(int)(numArcs/2); i <= (int)(numArcs/2); i++) {
      arcs.push_back(2*i*USE_MAX_STEER/numArcs);
    }
    arbiterVotes = mapCorridor.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |         |       |     |" << endl;

    break;

  case 42:

    //**********************************************//
    // SHRINK_ONSTACLES
    //**********************************************//
    system("clear");
    compCell;
    state.Easting =396000;
    state.Northing =3777000;
    mapPaint.initMap(20,20,1,1,true);
    mapPaint.updateFrame(state,true);
    newCell = mapPaint.getCellDataRC(9,9);    
    newCell.value = 3;
    for(i=0;i<mapPaint.getNumRows();i++){
      for(j=0;j<mapPaint.getNumCols();j++){
        mapPaint.setCellDataRC(i,j,newCell);
      }
    }
    for(i=0;i< 10;i++){
      name = "shrink_";name += i;
      strcpy(fileName,name.getStr());
      mapPaint.updateFrame(state,true);
      newCell.value = 3;
      mapPaint.setCellDataRC(1,0,newCell);
      newCell.value = 0;
      mapPaint.setCellDataRC(mapPaint.getNumRows()-1,mapPaint.getNumCols()-2,newCell);
      mapPaint.saveMATFile(fileName,"test","theMap");
    }

    break;

  case 43:

    //**********************************************//
    // MAX_HEIGHT_DIFF_COST
    //**********************************************//
    system("clear");
    map.initMap(25,25,1.0,1.0,false);
    newCell.value = 0;
    newCell.type = 0;
    newCell.color = 0;
    for(i=0;i<map.getNumRows();i++){
      for(j=0;j<map.getNumCols();j++){
        map.setCellDataRC(i,j,newCell);
      }
    }
    newCell.value = 1;
    newCell.type = 0;
    newCell.color = GREEN_INDEX;
    map.setCellDataRC(20,12,newCell);
    map.initPathEvaluationCriteria(false,false,false,true,false);
    numArcs = 25;
    state.Yaw = PI/4;
    arcs.clear();
    arcs.reserve(numArcs);
    for(i = -(int)(numArcs/2); i <= (int)(numArcs/2); i++) {
      arcs.push_back(2*i*USE_MAX_STEER/numArcs);
    }
    arbiterVotes = map.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |         |       |     |" << endl;

    break;

  case 44:

    //**********************************************//
    // MAX_INCLINATION_COST
    //**********************************************//
    system("clear");
    map.initMap(20,20,1.0,1.0,false);
    newCell.value = 0;
    newCell.type = 0;
    newCell.color = 0;
    //for(i=0;i<map.getNumRows();i++){
    //  for(j=0;j<map.getNumCols();j++){
    //    map.setCellDataRC(i,j,newCell);
    //  }
    //}
    newCell.value = -1;
    newCell.type = 0;
    newCell.color = GREEN_INDEX;
    //map.setCellDataRC(19,9,newCell);
    newCell.value = 2;
    newCell.type = 0;
    newCell.color = GREEN_INDEX;
    //map.setCellDataRC(19,11,newCell);
    map.setCellDataRC(19,11,newCell);
    map.initPathEvaluationCriteria(false,false,false,true,false);
    map.initThresholds(70,0.5,0.5,0.5,0.5,2);
    numArcs = 25;
    state.Yaw = 0;
    arcs.clear();
    arcs.reserve(numArcs);
    for(i = -(int)(numArcs/2); i <= (int)(numArcs/2); i++) {
      arcs.push_back(2*i*USE_MAX_STEER/numArcs);
    }
    arbiterVotes = map.generateArbiterVotes(numArcs,arcs);
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |         |       |     |" << endl;


    break;

  case 45:

    //**********************************************//
    // MAX_INCLINATION_COST
    //**********************************************//

    // Loading the file
    map.clearMap();
    ret = map.loadMATFile("sa_test.m");
    //map.saveMATFile("sa_test2","test","theMap");
    // Verifying if there is cell different than NO_DATA_CELL
    for(i=0;i<map.getNumRows();i++){
      for(j=0;j<map.getNumCols();j++){
        if(map.getCellDataRC(i,j).type != 2){
          cout << "(" << i << "," << j << ") => REAL DATA (value = " << map.getCellDataRC(i,j).value << ")" << endl;
        }
      }
    }
    getchar();
    // Updating the state
    state.Yaw = 0;
    state.Northing = map.getLeftBottomUTM_Northing() + map.getNumRows()*map.getRowRes();
    state.Easting = map.getLeftBottomUTM_Easting() + map.getNumCols()*map.getColRes();
    map.Copy_VState(state);
    // Generating votes
    numArcs = 25;
    map.initPathEvaluationCriteria(false,false,false,true,false);
    map.initPathEvaluationCriteria(false,false,false,true,false);
    arcs.clear();
    arcs.reserve(numArcs);
    for(i = -(int)(numArcs/2); i <= (int)(numArcs/2); i++) {
      arcs.push_back(2*i*USE_MAX_STEER/numArcs);
    }
    arbiterVotes = map.generateArbiterVotes(numArcs,arcs);
    // Displaying votes
    cout << " i |   steer  | votes | velo |" << endl;
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |         |       |     |" << endl;


    break;

  case 46:
    /*
    double input_coordinate;
    cout << "We are using map100 for this test.\n";
    cout << "numRows = " << map100.getNumRows() << endl;
    cout << "numCols = " << map100.getNumCols() << endl;
    cout << "rowRes = " << map100.getRowRes() << endl;
    cout << "colRes = " << map100.getColRes() << endl;
    cout << "bottomLeftUTM_Northing = " << map100.getLeftBottomUTM_Northing() << endl;
    cout << "bottomLeftUTM_Easting = " << map100.getLeftBottomUTM_Easting() << endl; 
    cout << "Please enter the vantage point from which you are looking in UTM.\n";
    cout << "Northing(m):\n";
    cin >> input_coordinate;
    start.Northing = input_coordinate;
    cout << "Easting(m):\n";
    cin >> input_coordinate;
    start.Easting = input_coordinate;
    cout << "Plese enter the point at which you are looking in UTM.\n";
    cout << "Northing(m):\n";
    cin >> input_coordinate;
    end.Northing = input_coordinate;
    cout << "Easting(m):\n";
    cin >> input_coordinate;
    end.Easting = input_coordinate;
    cells_on_line = map100.GetCellLine(start, end);
    cout << "In map100, your line of sight passes through points\n";
    while(cells_on_line.size() >= 1)
      {
	cout << "(" << (cells_on_line.front()).first << ", " << (cells_on_line.front()).second << ")\n";
    	cells_on_line.pop_back();
      }
    */
    break;

  case 47:

    //**********************************************//
    // OBSTACLE GOODNESS SHAPE
    //**********************************************//    

    numArcs = 25;
    for(ind = 0;ind < numArcs; ind++){
      cout << ind << " ";
      map.obstacleGoodnessShape(ind,numArcs);
    }
    getchar();

    break;

  case 48:

    //**********************************************//
    // GET CELLS LINE
    //**********************************************//    

    map.initMap(120,120,0.1,0.1,false);
    state.Northing = 0;
    state.Easting = 0;
    state.Yaw = 0;
    map.updateFrame(state,true);

    break;

  case 49:

    map.initMap(120,120,0.2,0.2,false);
    state.Northing = 0;
    state.Easting = 0;
    pos.first  = map.getVehRow();
    pos.second = map.getVehCol();

    cout << "Initializing ..." << endl;
    state.Yaw = -M_PI/4;
    map.clearMap();
    map.maxHeightDiffCostRectangle(pos,state.Yaw);
    map.saveMATFile("max_test0","rectangle","theMap");

    state.Yaw = -2*M_PI/3;
    map.clearMap();
    map.maxHeightDiffCostRectangle(pos,state.Yaw);
    map.saveMATFile("max_test1","rectangle","theMap");


    state.Yaw = -3*M_PI/3;
    map.clearMap();
    map.maxHeightDiffCostRectangle(pos,state.Yaw);
    map.saveMATFile("max_test2","rectangle","theMap");

    state.Yaw = -4*M_PI/3;
    map.clearMap();
    map.maxHeightDiffCostRectangle(pos,state.Yaw);
    map.saveMATFile("max_test3","rectangle","theMap");

    state.Yaw = -5*M_PI/3;
    map.clearMap();
    map.maxHeightDiffCostRectangle(pos,state.Yaw);
    map.saveMATFile("max_test4","rectangle","theMap");

    state.Yaw = M_PI/4;
    map.clearMap();
    map.maxHeightDiffCostRectangle(pos,state.Yaw);
    map.saveMATFile("max_test5","rectangle","theMap");

    state.Yaw = 4*M_PI/6;
    map.clearMap();
    map.maxHeightDiffCostRectangle(pos,state.Yaw);
    map.saveMATFile("max_test6","rectangle","theMap");

    state.Yaw = 5*M_PI/6;
    map.clearMap();
    map.maxHeightDiffCostRectangle(pos,state.Yaw);
    map.saveMATFile("max_test7","rectangle","theMap");

    /*
    state.Northing = 0;
    state.Easting = 0;
    state.Yaw = 0;
    map.updateFrame(state,true);
    map.initMap(120,120,0.2,0.2,false);
    map.initPathEvaluationCriteria(false,false,false,true,false,false,false);
    newCell.value = 1;
    map.setCellDataRC(90,90,newCell);
    numArcs = 25;
    arcs.clear();
    arcs.reserve(numArcs);
    for(i = -(int)(numArcs/2); i <= (int)(numArcs/2); i++) {
      arcs.push_back(2*i*USE_MAX_STEER/numArcs);
    }
    arbiterVotes = map.generateArbiterVotes(numArcs,arcs);
    for(i=0;i<numArcs;i++){
      if(arcs[i] < 0)
        printf("%02d |L. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
      else
        printf("%02d |R. %03.4f | %03.1f | %02.1f |\n",i,abs(arcs[i]),arbiterVotes[i].second,arbiterVotes[i].first);
    }
    cout << "   |          |       |     |" << endl;
    */

    break;

  default:
    cout << "Wrong choice !!" << endl;
  }
  
  return(0);
}
