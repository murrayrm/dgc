/*------------------------------------------------------------------------*/
/* Program     : mapinterface.h                                           */
/* Programmer  : Art Rankin                                               */
/* Employer    : Jet Propulsion Laboratory (JPL)                          */
/*             : Pasadena, California                                     */
/* Group       : Robotic Vehicles Group                                   */
/* Project     : Unmanned Ground Vehicle (UGV)                            */
/*                                                                        */
/* Date        : 03/14/2000 alr                                           */
/* Last Update : 04/12/2000                                               */
/*------------------------------------------------------------------------*/
/* Description: This program                                              */
/*                                                                        */
/*------------------------------------------------------------------------*/

#ifndef MAPINTERFACE_H
#define MAPINTERFACE_H



 
#define ELEVATION_SIGN_MASK             0x00000001 /* bits   1   */
#define ELEVATION_DATA_MASK             0x000007FE /* bits  2-11 */
#define ELEVATION_CONFIDENCE_MASK       0x00003800 /* bits 12-14 */
#define CLASSIFICATION_DATA_MASK        0x0001C000 /* bits 15-17 */
#define CLASSIFICATION_CONFIDENCE_MASK  0x000E0000 /* bits 18-20 */
#define OBJECT_DATA_MASK                0x00300000 /* bits 21-22 */
#define OBJECT_CONFIDENCE_MASK          0x0FC00000 /* bits 23-28 */
#define ROUGHNESS_DATA_MASK             0xF0000000 /* bits 29-32 */
#define ROUGHNESS_CONFIDENCE_MASK       0x00000000 /* no bits allocated */
 
 
#define ELEVATION_SIGN_SHIFT            0
#define ELEVATION_DATA_SHIFT            1
#define ELEVATION_CONFIDENCE_SHIFT      11
#define CLASSIFICATION_DATA_SHIFT       14
#define CLASSIFICATION_CONFIDENCE_SHIFT 17
#define OBJECT_DATA_SHIFT               20
#define OBJECT_CONFIDENCE_SHIFT         22
#define ROUGHNESS_DATA_SHIFT            28
#define ROUGHNESS_CONFIDENCE_SHIFT      0
 
/* elevation sign bit is 1 for negative elevation */
/* elevation sign bit is 0 for positive elevation */

/* objects data within bits 21-22 */
#define NO_OBSTACLE                     0
#define POSITIVE_OBSTACLE               1
#define NEGATIVE_OBSTACLE               2
#define NO_OBSTACLE_DATA		3
 
 
/* classification data within bits 15-17 */
#define NO_CLASSIFICATION_DATA		0
#define OUTLIER_CLASSIFICATION		1
#define TREE_CLASSIFICATION		2
#define SOIL_CLASSIFICATION		3
#define ROCK_CLASSIFICATION		3
#define TERRAIN_CLASSIFICATION		3	/* soil or rock */
#define GREEN_VEGETATION_CLASSIFICATION	4
#define DRY_VEGETATION_CLASSIFICATION	4
#define VEGETATION_CLASSIFICATION	4	/* green or dry vegetation */
#define TALL_GRASS_CLASSIFICATION	5
#define COVER_CLASSIFICATION		6
#define WATER_CLASSIFICATION		7

#define NUM_ACTIVE_CLASSES		8

#if 0
/* Sensors definition, can be combined in a char for 0202 evalution*/
#define FRONT_COLOR_CAMERAS		0x01
#define FRONT_FLIR_CAMERAS		0x02
#define RIEGL_LADAR			0x04
#define VISTEON_RADAR			0x08
#define LIGHT_STRIPER			0x10
#define REAR_COLOR_CAMERAS		0x20
#define SICK_LASER			0x40
#define OTHER_SENSOR			0x80
#endif


/* roughness data within bits 26-29 */
#define NO_ROUGHNESS_DATA               15


#define TREE_CLASS(x)			((x == TREE_CLASSIFICATION) \
					  ? 1 : 0)

#define VEGETATION_CLASS(x)		((x == GREEN_VEGETATION_CLASSIFICATION || \
					  x == DRY_VEGETATION_CLASSIFICATION || \
					  x == VEGETATION_CLASSIFICATION || \
					  x == TALL_GRASS_CLASSIFICATION)  \
					  ? 1 : 0)

#define TERRAIN_CLASS(x)		((x == SOIL_CLASSIFICATION || \
					  x == ROCK_CLASSIFICATION || \
					  x == TERRAIN_CLASSIFICATION)  \
					  ? 1 : 0)

#define WATER_CLASS(x)			((x == WATER_CLASSIFICATION) \
					  ? 1 : 0)



#define NUM_ELEV_DATA_BITS		10
#define NUM_ELEV_CONF_BITS		3
#define NUM_CLASS_DATA_BITS		3
#define NUM_CLASS_CONF_BITS		3
#define NUM_OBJECT_DATA_BITS		2
#define NUM_OBJECT_CONF_BITS		6
#define NUM_ROUGH_DATA_BITS		4
#define NUM_ROUGH_CONF_BITS		0


#define MAX_ELEV_DATA_VAL		0x03FF
#define MAX_ELEV_CONF_VAL		0x0007
#define MAX_CLASS_DATA_VAL		0x0007
#define MAX_CLASS_CONF_VAL		0x0007
#define MAX_OBJECT_DATA_VAL		0x0003
#define MAX_OBJECT_CONF_VAL		0x003F
#define MAX_ROUGH_DATA_VAL		0x000F
#define MAX_ROUGH_CONF_VAL		0x0000

#define NO_ELEVATION_DATA_FLAG		MAX_ELEV_DATA_VAL

#define MAX_ELEV_DATA			(MAX_ELEV_DATA_VAL-1)
#define MAX_OBJECT_DATA			MAX_OBJECT_DATA_VAL
#define MAX_CLASS_DATA			MAX_CLASS_DATA_VAL
#define MAX_ROUGH_DATA			(MAX_ROUGH_DATA_VAL-1)

#define MAX_ELEV_CONF			MAX_ELEV_CONF_VAL
#define MAX_OBJECT_CONF			MAX_OBJECT_CONF_VAL
#define MAX_CLASS_CONF			MAX_CLASS_CONF_VAL
#define MAX_ROUGH_CONF			MAX_ROUGH_CONF_VAL


/* Accessing 2D map */
#define GET_ELEVATION_DATA(map,i,j)	(((map[i][j] & ELEVATION_DATA_MASK) \
					>> ELEVATION_DATA_SHIFT)* \
					((map[i][j] & ELEVATION_SIGN_MASK) ? \
					-1 : 1))

#define GET_ELEVATION_CONF(map,i,j)	((map[i][j] & ELEVATION_CONFIDENCE_MASK) \
					>> ELEVATION_CONFIDENCE_SHIFT)

#define GET_ELEVATION_SIGN(map,i,j)	((map[i][j] & ELEVATION_SIGN_MASK) \
					>> ELEVATION_SIGN_SHIFT)

#define GET_CLASS_DATA(map,i,j)		((map[i][j] & CLASSIFICATION_DATA_MASK) \
					>> CLASSIFICATION_DATA_SHIFT)

#define GET_CLASS_CONF(map,i,j)		((map[i][j] & \
					CLASSIFICATION_CONFIDENCE_MASK) \
					>> CLASSIFICATION_CONFIDENCE_SHIFT)

#define GET_OBJECT_DATA(map,i,j)	((map[i][j] & OBJECT_DATA_MASK) \
					>> OBJECT_DATA_SHIFT)

#define GET_OBJECT_CONF(map,i,j)	((map[i][j] & OBJECT_CONFIDENCE_MASK) \
					>> OBJECT_CONFIDENCE_SHIFT)

#define GET_ROUGHNESS_DATA(map,i,j)	((map[i][j] & ROUGHNESS_DATA_MASK) \
					>> ROUGHNESS_DATA_SHIFT)

#define GET_ROUGHNESS_CONF(map,i,j)	((map[i][j] & ROUGHNESS_CONFIDENCE_MASK) \
					>> ROUGHNESS_CONFIDENCE_SHIFT)


/* Accessing 1D map */
#define GET_ELEVATION_DATA2(map,i)	(((map[i] & ELEVATION_DATA_MASK) \
					>> ELEVATION_DATA_SHIFT)* \
					((map[i] & ELEVATION_SIGN_MASK) ? \
					-1 : 1))

#define GET_ELEVATION_CONF2(map,i)	((map[i] & ELEVATION_CONFIDENCE_MASK) \
					>> ELEVATION_CONFIDENCE_SHIFT)

#define GET_ELEVATION_SIGN2(map,i)	((map[i] & ELEVATION_SIGN_MASK) \
					>> ELEVATION_SIGN_SHIFT)

#define GET_CLASS_DATA2(map,i)		((map[i] & CLASSIFICATION_DATA_MASK) \
					>> CLASSIFICATION_DATA_SHIFT)

#define GET_CLASS_CONF2(map,i)		((map[i] & \
					CLASSIFICATION_CONFIDENCE_MASK) \
					>> CLASSIFICATION_CONFIDENCE_SHIFT)

#define GET_OBJECT_DATA2(map,i)		((map[i] & OBJECT_DATA_MASK) \
					>> OBJECT_DATA_SHIFT)

#define GET_OBJECT_CONF2(map,i)		((map[i] & OBJECT_CONFIDENCE_MASK) \
					>> OBJECT_CONFIDENCE_SHIFT)

#define GET_ROUGHNESS_DATA2(map,i)	((map[i] & ROUGHNESS_DATA_MASK) \
					>> ROUGHNESS_DATA_SHIFT)

#define GET_ROUGHNESS_CONF2(map,i)	((map[i] & ROUGHNESS_CONFIDENCE_MASK) \
					>> ROUGHNESS_CONFIDENCE_SHIFT)


/* Extracting data from a int */
#define GET_ELEVATION_DATA3(value)	(((value & ELEVATION_DATA_MASK) \
					>> ELEVATION_DATA_SHIFT)* \
					((value & ELEVATION_SIGN_MASK) ? \
					-1 : 1))

#define GET_ELEVATION_CONF3(value)	((value & ELEVATION_CONFIDENCE_MASK) \
					>> ELEVATION_CONFIDENCE_SHIFT)

#define GET_ELEVATION_SIGN3(value)	((value & ELEVATION_SIGN_MASK) \
					>> ELEVATION_SIGN_SHIFT)

#define GET_CLASS_DATA3(value)		((value & CLASSIFICATION_DATA_MASK) \
					>> CLASSIFICATION_DATA_SHIFT)

#define GET_CLASS_CONF3(value)		((value & \
					CLASSIFICATION_CONFIDENCE_MASK) \
					>> CLASSIFICATION_CONFIDENCE_SHIFT)

#define GET_OBJECT_DATA3(value)		((value & OBJECT_DATA_MASK) \
					>> OBJECT_DATA_SHIFT)

#define GET_OBJECT_CONF3(value)		((value & OBJECT_CONFIDENCE_MASK) \
					>> OBJECT_CONFIDENCE_SHIFT)

#define GET_ROUGHNESS_DATA3(value)	((value & ROUGHNESS_DATA_MASK) \
					>> ROUGHNESS_DATA_SHIFT)

#define GET_ROUGHNESS_CONF3(value)	((value & ROUGHNESS_CONFIDENCE_MASK) \
					>> ROUGHNESS_CONFIDENCE_SHIFT)


#define PUT_ELEVATION_DATA(cell, value)		((((abs(value) << ELEVATION_DATA_SHIFT) | \
                                                   ((value > 0 ? 0:1) << ELEVATION_SIGN_SHIFT)) \
						& (ELEVATION_DATA_MASK | ELEVATION_SIGN_MASK)) \
						| ((~ELEVATION_DATA_MASK | ~ELEVATION_SIGN_MASK) & cell))

#define PUT_ELEVATION_CONF(cell, value)		(((value << ELEVATION_CONFIDENCE_SHIFT) \
						& ELEVATION_CONFIDENCE_MASK) \
						| (~ELEVATION_CONFIDENCE_MASK & cell))

#define PUT_ELEVATION_SIGN(cell, value)		(((((value > 0) ? 0:1) << ELEVATION_SIGN_SHIFT) \
						& ELEVATION_SIGN_MASK) \
						| (~ELEVATION_SIGN_MASK & cell))

#define PUT_CLASS_DATA(cell, value)		(((value << CLASSIFICATION_DATA_SHIFT) \
						& CLASSIFICATION_DATA_MASK) \
						| (~CLASSIFICATION_DATA_MASK & cell))

#define PUT_CLASS_CONF(cell, value)		(((value << \
						CLASSIFICATION_CONFIDENCE_SHIFT) \
						& CLASSIFICATION_CONFIDENCE_MASK) \
						| (~CLASSIFICATION_CONFIDENCE_MASK & cell))

#define PUT_OBJECT_DATA(cell, value)		(((value << OBJECT_DATA_SHIFT) \
						& OBJECT_DATA_MASK) \
						| (~OBJECT_DATA_MASK & cell))

#define PUT_OBJECT_CONF(cell, value)		(((value << OBJECT_CONFIDENCE_SHIFT) \
						& OBJECT_CONFIDENCE_MASK) \
						| (~OBJECT_CONFIDENCE_MASK & cell))

#define PUT_ROUGHNESS_DATA(cell, value)		(((value << ROUGHNESS_DATA_SHIFT) \
						& ROUGHNESS_DATA_MASK) \
						| (~ROUGHNESS_DATA_MASK & cell))

#define PUT_ROUGHNESS_CONF(cell, value)		(((value << ROUGHNESS_CONFIDENCE_SHIFT) \
						& ROUGHNESS_CONFIDENCE_MASK) \
						| (~ROUGHNESS_CONFIDENCE_MASK & cell))


#ifndef SUCCESS
#define SUCCESS 0
#endif

#ifndef FAILURE
#define FAILURE -1
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef M_PI
#define M_PI            3.14159265358979323846
#endif
 

#ifndef DEG2RAD
#define DEG2RAD         (M_PI/180.0)
#endif


#ifndef RAD2DEG
#define RAD2DEG         (180.0/M_PI)
#endif


#ifndef SQR
#define SQR(x)          ((x)*(x))
#endif
 
#ifndef MIN
#define MIN(x,y)        ((x < y) ? x : y);
#endif
 
#ifndef MAX
#define MAX(x,y)        ((x > y) ? x : y);
#endif
 
#ifndef SIGN
#define SIGN(x)		((x < 0) ? -1 : 1)
#endif

#endif
