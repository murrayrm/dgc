#include "serial.h"
#include <time.h>
#include <iostream>

using namespace std;

/* serial.c
 * Chih-Hao Liu. chihhao@its.caltech.edu
 * 6/18/2003 Initial Creation 
 * 6/19/2003 Got the code working on /dev/ttyS0
 * 6/20/2003 Got the code working on /dev/ttyS1, doesn't work simultaneously on ttyS0 and ttyS1
 * 6/23/2003 work simulatneously on ttyS0 and ttyS1. Finish README. 
 * Jeremy Gillula. jeremy@caltech.edu
 * 7/18/2003 Eliminated 8th-bit cutoff error, added a serial_read_blocking function
 * *** This is wholly not-up-to-date. Look at the CVS revision log -- wkc5 (Will) ***
 * 9/13/2003   Will Coulter  serial_read_blocking now takes a timeout factor
 * 9/13/2003   Sue Ann Hong  changed serial_read_until to use timeval for better precision in time
 * 9/14/2003   Sue Ann Hong  serial_read_until tested and works. segfaulted once. won't do it anymore so can't debug. tell me if it happens again. (sueh@its)
 */

struct 		SerialBuffer sbuffer[TtysCount];
int		SerialFile[TtysCount];
static int 	block_flag[TtysCount];		// =1 when blocking. =0 when nonblocking. initialized to 1 in serial_open

void serial_block(int com)
{
  int temp;
  if(block_flag[com]==SerialNonBlock)
    {
      if((temp=fcntl(SerialFile[com], F_GETFL, 0))<0)
	fprintf(stderr, "set_block: fcntl F_GETFL error\n");
      temp &= ~O_NONBLOCK;
      if(fcntl(SerialFile[com], F_SETFL, temp)<0)
	fprintf(stderr, "set_block: fcntl F_SETFL error\n");
      block_flag[com]=SerialBlock;
    }
}

void serial_nonblock(int com)
{
  int temp;
  
  if(block_flag[com]==SerialBlock)
    {
      if((temp=fcntl(SerialFile[com], F_GETFL, 0))<0)
	fprintf(stderr, "set_nonblock: fcntl F_GETFL error\n");
      temp |= O_NONBLOCK;
      if(fcntl(SerialFile[com], F_SETFL, temp)<0)
	fprintf(stderr, "set_block: fcntl F_SETFL error\n");
      block_flag[com]=SerialNonBlock;
    }
}

		
void serial_input_handler(int signal)
{
  char buffer[300];
  int len;
  int i;
  struct timeval time={0,0};
  fd_set	rfds;
  
  FD_ZERO(&rfds);
  
  for(i=0; i<TtysCount; i++)
    if (SerialFile[i]<=0)
      continue;
    else
      {
	FD_SET(SerialFile[i], &rfds);
	while(select(SerialFile[i]+1, &rfds, NULL, NULL, &time)<0)
	  {
	    fprintf(stderr, "serial_input_handler: select error\n");
	    fflush(stdout);
	  }
	if(FD_ISSET(SerialFile[i], &rfds))
	  {
	    len=read(SerialFile[i], buffer, 300);
	    if (len < 0)
	      {
		if (errno == EAGAIN)
		  {
		    return; // assume that command generated no response
		  }
		else
		  {
		    fprintf(stderr,"serial_read: error %d %s\n", errno, strerror(errno));
		    return ;
		  } // end if
	      }
	    else if (len==0) continue;
	    else
	      {
		if(sbuffer[i].len==0)		// when nothing is in the buffer
		  {
		    sbuffer[i].data= (char *) malloc(len);
		    memcpy(sbuffer[i].data, buffer, len);
		    sbuffer[i].len = len;
		  }
		else
		  {
		    sbuffer[i].data= (char *) realloc(sbuffer[i].data,sbuffer[i].len+len);
		    memcpy(sbuffer[i].data+sbuffer[i].len, buffer, len);
		    sbuffer[i].len = sbuffer[i].len+len;
		  }
	      }
	  }
      }
}

/*  serial_open_advanced opens a serial port and allow more setting for the serial port.
 *  set options to set parity, flow control, and stop bits. Note: even if you don't set
 *  stop bit, there is still one stop bit, I guess it is default to serial port.
 *  com: serial port you want to open
 *  BaudRate: baud rate you want to set to
 *  options: set parity, flow control, and stop bits.
 *  
 *  available flags for options ( OR these flags to select multiple flags):
 *
 *	SR_PARITY_DISABLE	// disable parity
 *	SR_PARITY_EVEN		// set parity to even	
 *	SR_PARITY_ODD		// set parity to odd
 *	
 *	SR_SOFT_FLOW_DISABLE	// disable software flow control
 *	SR_HARD_FLOW_DISABLE	// disalbe hardware flow control
 *	SR_SOFT_FLOW_INPUT	// enable software input flow control
 *	SR_SOFT_FLOW_OUTPUT	// enable software output flow control
 *	SR_HARD_FLOW_ENABLE	// enable hardware flow control
 *
 *	SR_ONE_STOP_BIT		// set one stop bit
 *	SR_TWO_STOP_BIT		// set two stop bit
 */
int serial_open_advanced(int com, int BaudRate, int options)
{
  struct termios	term;
  char SerialPathName[40];
  int oflags;
  
  sprintf(SerialPathName, "%s%d", SerialPath, com);
  printf("Connecting to %s..\n", SerialPathName);
  if((SerialFile[com]=open(SerialPathName, O_RDWR | O_NOCTTY /*| O_NDELAY*/))<0)// initialize to blocking state
  {	
    fprintf(stderr, "serial_open: open error\n");
    return -1;
  }
  /* Get the current options for the port */
  if(tcgetattr(SerialFile[com], &term)<0)
  {
    fprintf(stderr, "serial_open: tcgetattr error\n");
    return -1;
  }
  block_flag[com]=SerialBlock;
  sbuffer[com].len=0;
  cfsetispeed(&term, BaudRate);	/* Set the baud rates to dafult */
  cfsetospeed(&term, BaudRate);
  tcflush(SerialFile[com], TCIFLUSH);
  /* Enable the receiver and set local mode */
  
  if((options & SR_PARITY_DISABLE) && ((options & SR_PARITY_EVEN) || (options & SR_PARITY_ODD)))
  {
    fprintf(stderr, "Cannot disable and enable parity at the same time.\n");
    return -1;
  }
  else if ((options & SR_PARITY_EVEN) && (options & SR_PARITY_ODD))
  {
    fprintf(stderr, "Cannot set parity to even and odd at the same time.\n");
    return -1;
  }
  else if (options & SR_PARITY_EVEN) term.c_cflag = PARENB;		// enable even parity	
  else if (options & SR_PARITY_ODD)  term.c_cflag = PARENB | PARODD;	// enable odd parity
  
  /* Check stop bits */
  
  if((options & SR_ONE_STOP_BIT) && (options & SR_TWO_STOP_BIT))
  {
    fprintf(stderr, "Cannot set stop bit to one and two at the same time.\n");
    return -1;
  }
  else if (options & SR_TWO_STOP_BIT) term.c_cflag = term.c_cflag | CSTOPB;
  // default is one stop bit, so don't worry about it
  
  /* Check hardware flow control*/
  if((options & SR_HARD_FLOW_DISABLE) && (options & SR_HARD_FLOW_ENABLE))
  {
    fprintf(stderr, "Cannot disable and enable hardware flow control at the same time\n");
    return -1;
  }
  if(options & SR_HARD_FLOW_ENABLE)  	term.c_cflag = term.c_cflag | CRTSCTS;// enable hard 
  
  term.c_cflag = term.c_cflag | 	// set the parity ,stop bit, and hardware flow control
    BaudRate | 	// set the baurd rate
    CS8 |		// 8-bit data
    CLOCAL | 	// enable receiver
    CREAD;		// ignore modem status line
  
  term.c_oflag &= ~OPOST;		// turn off post processing
  
  /* check software flow control*/
  /* Caution : if you use flow control, you won't receive bytes with values of 17 and 19*/
  
  if((options & SR_SOFT_FLOW_DISABLE) && ((options & SR_SOFT_FLOW_INPUT) || (options & SR_SOFT_FLOW_OUTPUT)))
  {
    fprintf(stderr, "Cannot disable and enable software flow control at the same time\n");
    return -1;
  }
  if(options & SR_SOFT_FLOW_INPUT)   	term.c_iflag = term.c_iflag | IXOFF;	// enable soft output 
  if(options & SR_SOFT_FLOW_OUTPUT)  	term.c_iflag = term.c_iflag | IXON;	// enable soft input
  
  term.c_iflag = term.c_iflag | IGNBRK;   	// ignore breaks
  term.c_lflag =0;		// everything off
  
  /* Set the new options for the port */
  if(tcsetattr(SerialFile[com], TCSANOW, &term)<0)
  {
    fprintf(stderr, "serial_open: tcsetattr error\n");
    return -1;
  }
  
  // set asynchronous notification
  signal(SIGIO, &serial_input_handler);
  fcntl(SerialFile[com], F_SETOWN, getpid());
  oflags=fcntl(SerialFile[com], F_GETFL);
  fcntl(SerialFile[com], F_SETFL, oflags | O_ASYNC);
  
  return 0;
}


/* serial_open: opens a serial port (ttyS0 to ttyS9) for reading and writing.
 *  RETURN: -1 if an error occurs; 0 otherwise.
 *  com: serial port number
 *  BaudRate: can be
 *	      B0, B50, B75, B110, B134, B150, B200, B300, B600, B1200, B1800, B2400, B4800, 
 *	      B9600, B19200, B38400, B57600, B115200, B230400, B460800, B500000, B576000, 
 *	      B921600, B1000000, B1152000, B1500000, B2000000, B2500000, B3000000, B3500000, 
 *	      B4000000
 */
int serial_open(int com, int BaudRate)
{
  struct termios	term;
  char SerialPathName[40];
  int oflags;
  
  sprintf(SerialPathName, "%s%d", SerialPath, com);
  printf("Connecting to %s..\n", SerialPathName);
  if((SerialFile[com]=open(SerialPathName, O_RDWR | O_NOCTTY /*| O_NDELAY*/))<0)// initialize to blocking state
  {	
    fprintf(stderr, "serial_open: open error\n");
    return -1;
  }
  /* Get the current options for the port */
  if(tcgetattr(SerialFile[com], &term)<0)
  {
    fprintf(stderr, "serial_open: tcgetattr error\n");
    return -1;
  }
  block_flag[com]=SerialBlock;
  sbuffer[com].len=0;
  cfsetispeed(&term, BaudRate);	/* Set the baud rates to dafult */
  cfsetospeed(&term, BaudRate);
  tcflush(SerialFile[com], TCIFLUSH);
  /* Enable the receiver and set local mode */
  term.c_cflag = BaudRate | 
    CS8 |		// 8-bit data
    CLOCAL | 	// enable receiver
		       CREAD;		// ignore modem status line
  term.c_oflag &= ~OPOST;		// turn off post processing
  term.c_iflag = IGNBRK;   	// ignore breaks
  //Commented out to fix receiving of bytes with values of 17 or 19
  //IXON | IXOFF |	// flow control
  //Commented out to fix 8th-bit cut-off problem
  //ISTRIP |		// strip input to 7 bits
  //Commented out to fix receiving of bytes with values of 13
  //IGNCR;		// ignore received CR*/
  term.c_lflag =0;		// everything off
  
  /* Set the new options for the port */
  if(tcsetattr(SerialFile[com], TCSANOW, &term)<0)
  {
    fprintf(stderr, "serial_open: tcsetattr error\n");
    return -1;
  }
  
  // set asynchronous notification
  signal(SIGIO, &serial_input_handler);
  fcntl(SerialFile[com], F_SETOWN, getpid());
  oflags=fcntl(SerialFile[com], F_GETFL);
  fcntl(SerialFile[com], F_SETFL, oflags | O_ASYNC);
  
  return 0;
}

/* serial_read:	read from a com port. If the com port is not open, 
 *  		call serial_open to open the serial port.
 * RETURN:	length of data read from serial, -1 if an error occurs
 */
int serial_read(int com, char *buffer, int length)
{
  int len=0;
  char *temp;
  sigset_t  newmask, oldmask;
  
  sigemptyset(&newmask);
  sigaddset(&newmask, SIGIO);
  
  if(sigprocmask(SIG_BLOCK, &newmask, &oldmask)<0)
  {
    fprintf(stderr, "serial_read: unable to mask SIGIO.\n");
    return -1;
  }
  
  if (sbuffer[com].len==0 || length==0);
  else if ( SerialFile[com]<=0)		// if the serial port is not opened yet, open it with default baud rate
  {
    if(serial_open(com, DefaultBaud)<0)
      return -1;
  }
  
  else if( sbuffer[com].len<=length)	// if requesting more than needed, copy everything in the buffer
  {
    memcpy(buffer, sbuffer[com].data, sbuffer[com].len);
    free(sbuffer[com].data);	// free the buffer
    len=sbuffer[com].len;
    sbuffer[com].len=0;
  }
  else				// buffer has more data than requested
  {
    memcpy(buffer, sbuffer[com].data, length);	// copy as much as requested
    temp= (char *) malloc(sbuffer[com].len-length);	// copy the data that hasn't be used to a new memory space
    memcpy(temp, sbuffer[com].data+length, sbuffer[com].len-length);
    free(sbuffer[com].data);				// free the old memory space
    sbuffer[com].data=temp;				// and substitue it with the new memory space 
    sbuffer[com].len=sbuffer[com].len-length;		// with data hasn't be transferred
    len=length;
  }
  
  if(sigprocmask(SIG_SETMASK, &oldmask, NULL)<0)
  {
    fprintf(stderr, "serial_read: unable to remove mask.\n");   
    return -1;
  }
  return len; 
}

/* serial_read_blocking: 
 *      Uses calls to serial_read for various buffer lengths to read until all
 *      data comes in or the timeout is reached.  A timeout of 0 means that the
 *      function returns the contents of the buffer and returns immediately.
 * NOTE:        Doesn't actually use serial_block or serial_nonblock
 * RETURN:	length of data read from serial, -1 if an error occurs
 */
int serial_read_blocking(int com, char *orig_buffer, int length, double timeout)
{
  int read_left, read_in=0, read_this_time=0;;
  char *buffer;
  timeval before, after;
  double elapsed;

  gettimeofday(&before, NULL);


  while(read_in!=length) {
    buffer=orig_buffer+read_in;
    read_left=length-read_in;
    read_this_time=serial_read(com, buffer, read_left);
    if (read_this_time == 0) {
      if (timeout > 0) {
	gettimeofday(&after, NULL);
	// Get the elapsed time. 
	elapsed = after.tv_sec - before.tv_sec;
	elapsed += ((double) (after.tv_usec - before.tv_usec)) / 1000000;
	// Fix Midnight rollover
	if ( elapsed < 0 ) {
	  // Add number of seconds in a day
	  elapsed += 24 * 3600;
	}
	
	if ( elapsed > timeout ) {
	  return read_in;
	}
      } 
      // else: keep reading, since negative timeout means block forever.
    } else {
      // We read something, so update the last-read time.
      gettimeofday(&before, NULL);
      read_in += read_this_time;
    }
    if ( timeout == 0 ) {
      return read_this_time;
    } 
    usleep(5000);
  }
  return read_in;
}

/* serial_read_until:	read from a com port. If the com port is not open, 
 *  		call serial_open to open the serial port. The user has to 
 *		specify a character he or she is looking for, and this
 * 		function won't return until either the character has
 * 		been found or timeout or buffer length is longer than
 *		limitlength. If the character is found, data
 *		in the buffer up to the character will be 
 *		returned to the user. 
 * com   	: number of serial port
 * buffer	: buffer that will be returned with data
 * delimit	: character the user is looking for
 * timeout	: time out in microseconds
 * limitlength	: limit length of the buffer. If serial buffer is longer than this, 
 *		  then return data of limitlength from the buffer
 * RETURN	: length of data read from serial, -1 if an error occurs
 * NOTE         : it's segfaulted once, but i can't debug it because it won't do it
 *                anymore. maybe it'll happen in the future. watch out for that.
 */
int serial_read_until(int com, char *buffer, char delimit, double timeout, int limitlength)
{
  int i, length=0;
  char *temp;
  sigset_t  newmask, oldmask;
  
  sigemptyset(&newmask);
  sigaddset(&newmask, SIGIO);
    
  //printf("Call serial_read_until1\n");fflush(stdout);
  if ( SerialFile[com]<=0)		// if the serial port is not opened yet, open it with default baud rate
  {
    if(serial_open(com, DefaultBaud)<0)
      return -1;
  }  
  else
  {
    //printf("Call serial_read_until2\n");fflush(stdout);
    timeval before, after;
    double elapsed;
    
    gettimeofday(&before, NULL);
    while(true)
    {
      if(sigprocmask(SIG_BLOCK, &newmask, &oldmask)<0)	// before using buffer, disable signal
      {
	fprintf(stderr, "serial_read: unable to mask SIGIO.\n");
	return -1;
      }
      for(i=0; i<sbuffer[com].len; i++) // traverse buffer to find the character
      {
	if(sbuffer[com].data[i]==delimit)// look for the character
	{
	  length=i+1;
	  if( sbuffer[com].len==length)	// if requesting more than needed, copy everything in the buffer
	  {
	    memcpy(buffer, sbuffer[com].data, sbuffer[com].len);
	    free(sbuffer[com].data);	// free the buffer
	    sbuffer[com].len=0;
	  }
	  else				// buffer has more data than requested
	  {
	    memcpy(buffer, sbuffer[com].data, length);	// copy as much as requested
	    temp= (char *) malloc(sbuffer[com].len-length);	// copy the data that hasn't be used to a new memory space
	    memcpy(temp, sbuffer[com].data+length, sbuffer[com].len-length);
	    free(sbuffer[com].data);				// free the old memory space
	    sbuffer[com].data=temp;				// and substitue it with the new memory space 
	    sbuffer[com].len=sbuffer[com].len-length;		// with data hasn't be transferred
	  }
	  if(sigprocmask(SIG_SETMASK, &oldmask, NULL)<0)
	  {
	    fprintf(stderr, "serial_read: unable to remove mask.\n");   
	    return -1;
	  }
	  //cout << "found delimiter, returning" << endl;
	  return length;
	}
      }
      if(sbuffer[com].len>=limitlength) // if buffer length is more than limit length, then 
      {				// just return data of limitlength
	length=limitlength;
	memcpy(buffer, sbuffer[com].data, length);	// copy as much as requested
	temp= (char *) malloc(sbuffer[com].len-length);	// copy the data that hasn't be used to a new memory space
	memcpy(temp, sbuffer[com].data+length, sbuffer[com].len-length);
	free(sbuffer[com].data);		// free the old memory space
	sbuffer[com].data=temp;			// and substitue it with the new memory space 
	sbuffer[com].len=sbuffer[com].len-length;
	if(sigprocmask(SIG_SETMASK, &oldmask, NULL)<0)
	{
	  fprintf(stderr, "serial_read: unable to remove mask.\n");   
	  return -1;
	}
	//cout << "received more than limit, returning" << endl;
	return length;
      }	
      length = sbuffer[com].len;
      if(sigprocmask(SIG_SETMASK, &oldmask, NULL)<0)	// before going to sleep, reenable signal
      {
      	fprintf(stderr, "serial_read: unable to remove mask.\n");   
      	return -1;
      }
      while (length >= sbuffer[com].len) {               // wait for the new data
	if (timeout >= 0) {
	  gettimeofday(&after, NULL);
	  // Get the elapsed time. 
	  elapsed = after.tv_sec - before.tv_sec;
	  elapsed += ((double) (after.tv_usec - before.tv_usec)) / 1000000;
	  // Fix Midnight rollover
	  if ( elapsed < 0 ) {
	    // Add number of seconds in a day
	    elapsed += 24 * 3600;
	  }	
	  if ( elapsed > timeout ) {
	    // time to return after saving sbuffer to buffer
	    if(sigprocmask(SIG_BLOCK, &newmask, &oldmask)<0)	// before using buffer, disable signal
	    {
	      fprintf(stderr, "serial_read: unable to mask SIGIO.\n");
	      return -1;
	    }
	    // sbuffer has less than equal to whatever was requested, so just copy everything
	    length = sbuffer[com].len;
	    memcpy(buffer, sbuffer[com].data, sbuffer[com].len);
	    free(sbuffer[com].data);	// free the buffer
	    sbuffer[com].len=0;
	    if(sigprocmask(SIG_SETMASK, &oldmask, NULL)<0)
	    {
	      fprintf(stderr, "serial_read: unable to remove mask.\n");   
	      return -1;
	    }
	    //cout << "timed out, returning" << endl;
	    return length;
	  }
	}
	// else: keep reading, since negative timeout means block forever
      }
      gettimeofday(&before, NULL);	// reset the timeout timer, since we just read something.
      //printf("sleep1 %f ", timeout);fflush(stdout);
      //timeout=sleep(timeout); // sleep until woken up or timeout
      //printf("sleep2 %f ", timeout);fflush(stdout);
    }
  }
  cout << "you should never see this. returning" << endl;
  return length;
}


/* serial_write: write to a com port. If the com port is not open, 
 *  		 call serial_open to open the serial port.
 * RETURN: length of data written. -1 if an error occurs.
 * bflag:  whether the write is a block or nonblocking write. can be SerialBlock or SerialNonBlock
 */
int serial_write(int com, char *buffer, int length, int bflag)
{
	int len;

	if(bflag==SerialNonBlock)
		serial_nonblock(com);
	else	serial_block(com);

	if (SerialFile[com]<=0)			// if serial port is not opened yet, open it with default baud rate
		if(serial_open(com, DefaultBaud)<0)
			return -1;

	if((len=write(SerialFile[com], buffer, length))>=0)
	{
		serial_nonblock(com);
		return len;
	}
	else
	{
		fprintf(stderr, "serial_write: serial port %d write error.\n", com);
		serial_nonblock(com);
		return -1;
	}
}

/* serial_close: closes serial port number com
 * RETURN: -1 if an error occurs; 0 otherwise.
 */

int serial_close(int com)
{
	if(close(SerialFile[com])<0)
	{
	   fprintf(stderr, "serial_close: close serial port error\n");
	   return -1;
	}
	else
	{
	   SerialFile[com]=0;
	   return 0;
	}
}

/* serial_clean_buffer: delete the contents of the buffer for specified com number
 */
int serial_clean_buffer(int com)
{
	sigset_t  newmask, oldmask;

	if(SerialFile[com]<=0)
	   return -1;
	else
	{

           sigemptyset(&newmask);
	   sigaddset(&newmask, SIGIO);

	   if(sigprocmask(SIG_BLOCK, &newmask, &oldmask)<0)
	   {
	      fprintf(stderr, "serial_read: unable to mask SIGIO.\n");
	      return -1;
	   }
	   if(sbuffer[com].data!=NULL && sbuffer[com].len!=0)
	   {
	      free(sbuffer[com].data);
	      sbuffer[com].len=0;
	   }
	   if(sigprocmask(SIG_SETMASK, &oldmask, NULL)<0)
	   {
	      fprintf(stderr, "serial_read: unable to remove mask.\n");   
	      return -1;
	   }
	   return 0;
	}
}



/*  serial_data_length returns the length of the available data in the buffer
 */ 
int serial_data_length(int com)
{
   int length=0;
   sigset_t  newmask, oldmask;

   sigemptyset(&newmask);
   sigaddset(&newmask, SIGIO);

   if(sigprocmask(SIG_BLOCK, &newmask, &oldmask)<0)	// before using buffer, disable signal
   {
	fprintf(stderr, "serial_read: unable to mask SIGIO.\n");
	return -1;
   }

   length=sbuffer[com].len;	

   if(sigprocmask(SIG_SETMASK, &oldmask, NULL)<0)	// before going to sleep, reenable signal
   {
  	fprintf(stderr, "serial_read: unable to remove mask.\n");   
	return -1;
   }
   return length;
}
