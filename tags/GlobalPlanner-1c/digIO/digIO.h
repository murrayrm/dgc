/*                      digIO.h
    Constants for accessing digital I/O ports

Revision History:
2003-sep-15     Alan Somers     implemented portnames as structs 
2003-sep-11     Alan Somers     Modified
2003-aug-22     Alan Somers     Created
*/

struct portname {
    unsigned int addr;
    unsigned char oldval;}; 
extern struct portname TME_IN_PORT, TME_OUT_PORT;

/*digio_open gives the calling process access permissions to a digital I/O port
It must be called by root.  To use the ports, the calling process should call
digio_open(), then setuid to a non-root uid*/
int digio_open(portname);

/*digio_close removes access permissions to a digital I/O ports.  It is never
neccesary to call this function, but will help prevent contention for the ports
if custody of the ports is to be handed from one process to another.*/
int digio_close(portname);

/*digio_write writes the byte to the specified port.  It should take at least
a microsecond.  The calling process must have write permissions to that port.*/
int digio_write(unsigned char data, unsigned char mask, portname port);

/*digio_read read a byte from the specified port.  It should take at least
a microsecond.  The calling process must have read permissions from the port.*/
unsigned char digio_read(portname port);
