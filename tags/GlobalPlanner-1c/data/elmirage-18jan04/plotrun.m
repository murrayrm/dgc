% plotrun.m - simple MATLAB script to plot out data
% RMM, 18 Jan 04

function plotrun(data)

time = data(:,1) - data(1,1);
pitch = data(:,4);
roll = data(:,5);
yaw = data(:,6)-data(1,6);
speed = data(:,7);

plot(time, pitch*180/pi, time, speed, time, roll*180/pi);

