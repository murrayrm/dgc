#include "Log.hh"

#include <string>
#include <iostream>
#include <iomanip>
#include <strstream>


using namespace std;

namespace MSG {

  LOG::LOG(char * file, int line) {
    x << file << "(" << line <<  "): ";
  }
  LOG::~LOG() {
    cout << x.str() << endl;
  }

  LOG& LOG::operator () (LOG_LEVEL loglevel, int logoutput) {
    myLogLevel = loglevel;
    return (*this);
  }
  LOG& LOG::operator << (const ostrstream & rhs) {
    x << rhs;
    return (*this);
  }
  LOG& LOG::operator << (const int & rhs) {
    x << rhs;
    return (*this);
  }
  LOG& LOG::operator << (const long & rhs) {
    x << rhs;
    return (*this);
  }
  LOG& LOG::operator << (const float & rhs) {
    x << rhs;
    return (*this);
  }
  LOG& LOG::operator << (const double & rhs) {
    x << rhs;
    return (*this);
  }
  LOG& LOG::operator << (const char * & rhs) {
    x << rhs;
    return (*this);
  }
  LOG& LOG::operator << (const string & rhs) {
    x << rhs;
    return (*this);
  }
  


}
