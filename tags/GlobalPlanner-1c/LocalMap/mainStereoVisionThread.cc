/*
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!
  IMPORTANT!  To run this using the MTA, comment out the constant below!
  Otherwise, the program will run in debug/diagnostic mode, in which
  it attempts to read Perceptor input from files, and logs its results
  to files instead of sending them to the Arbiter!
*/
#define USE_MTA

/*
This program is meant to include elements of the local-map/stereovision
processing sequence of modules for path-planning/decision-making,
starting with the Local Goodness Map Estimator, which takes as input 
map data from Perceptor, and ending with the Local Path Evaluator, which
creates as output a goodness vote on each of a set of possible vehicle
trajectories.
*/

#include <string>
#include <sstream>
#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#include "svTrajectory.h"
#include "LocalMap.h"
#include "../pertocla/per_to_cla.h"
#include "../Arbiter/Vote.h"
#include "../Arbiter/Arbiter.h"

using namespace std;

#define LOCAL_SIZE 50000
#define LOCAL_KEY 111

#ifndef USE_MTA
//These constants set angle info for debug/diag purposes
static const double MIN_ANGLE = -PI/2;
static const double MAX_ANGLE =  PI/2;
static const int NUM_ANGLES = 21;
static const double ANGLE_SPACING = (MAX_ANGLE-MIN_ANGLE)/(NUM_ANGLES-1);
static const double ANGLE_LENGTH = 20;
/*
  These constants indicate the path to logged Perceptor output
  which this program can use as input for debug/diagnostic purposes
*/
static const std::string PATH_TO_MAPS = "../pertocla/serialized_maps";
static const std::string INPUT_FILE_PREFIX = "ptc_frame_";
static const std::string INPUT_FILE_SUFFIX = ".ptc";
static const int MAX_PERCEPTOR_FRAMES = 1;
#endif

int main() {
  /*
    First, create a LocalMap object
    MAP_COLS, MAP_ROWS, etc. are constants defined by Perceptor in
    perceptor/MapGen/map.h
  */
  //LocalMap lMap(MAP_COLS, MAP_ROWS, MAP_X_RES, MAP_Y_RES, 0, 0, 0, 0, 0);
  LocalMap lMap;

  /*
    Next, create a serialized map - this SerialMap standarizes input so
    that either the MTA can send a SerialMap or it can be read from file
    Either way, the LocalMap reads it the same way
  */
  serialMap sMap;

  /*
    IMPORTANT! IMPORTANT! IMPORTANT! IMPORTANT! IMPORTANT! IMPORTANT! 
    There are two sets of code below!  The first uses the MTA, the 
    second does not!  Be sure you are debugging the correct one!
  */
#ifdef USE_MTA
  int size = LOCAL_SIZE;
  int key = LOCAL_KEY;
  int shmflg = (IPC_CREAT | 0x1ff);
  void *map_data = NULL;
  int shmid;
  int frame_num = 0;

  cout << "\nAttemtping to init shared memory";
  if ((shmid = shmget(key, size, shmflg)) < 0) {
    cout << "\nError initing shared memory";
  } else {
    map_data = (void*)shmat(shmid, NULL, 0);
    cout << "\nShared memory Inited OK";
  }
  
  cout << "\nStarting loop";
  while(true) {

    //Cycle through all the latest data waiting in the inbox
    //Step 1: Receive the latest Perceptor data from MTA
    printf("\nGetting map");
    sMap.deserializeMap(map_data);
	
    //Step 2: Combine the latest Perceptor data with pre-existing data
    printf("\nUpdating map");
    lMap.updateMap(sMap);

    std::ostringstream outputFileName;
    outputFileName << "outputMaps/output_map_";
    outputFileName.width(3);
    outputFileName.fill('0');
    outputFileName << frame_num;
    outputFileName << ".bmp";

    printf("\nPrinting map %d", frame_num);
    lMap.outputGoodnessMap(outputFileName.str());

    frame_num++;

    /*
      if(MTA has message saying we should evaluate some arcs) {      //Step 3: Generate a list of trajectories and create an empty list of votes

      //Step 4: Evaluate each trajectory, and then populate the list of votes
      //with correspodning angles, velocities, and goodnesses

      //Step 5: Send the results back to the arbiter
      }
    */
  }    
#else
  //MTA NOT USED BELOW!
  for(int frame_num = 0; frame_num < MAX_PERCEPTOR_FRAMES; frame_num++) {
    //This is simple string manipulation to create a string with the right path/filename
    //to the Perceptor log files
    std::ostringstream inputFileName;
    inputFileName << INPUT_FILE_PREFIX;
    inputFileName.width(3);
    inputFileName.fill('0');
    inputFileName << frame_num;
    inputFileName << INPUT_FILE_SUFFIX;
    std::ostringstream inputFullPath;
    inputFullPath << PATH_TO_MAPS << "/" << inputFileName.str();

    //Step 1: Receive the latest Perceptor data from file
    sMap.readMap( inputFullPath.str().c_str() ) ;

    //Step 2: Integrate the latest Perceptor data with our pre-existing data
    lMap.updateMap(sMap);

    Arbiter::combinedVoteListType voteList;
    svTrajectoryType path;
    Vote singleVote;

    //HACK! HACK! HACK! HACK!  - For lack of getting the actual vehicle heading (as
    //you would if state were working) just say the vehicle's going south
    double vehicle_heading = 0;

    for(double angle=MIN_ANGLE; angle <= MAX_ANGLE; angle+=ANGLE_SPACING) {
      cout << "\nAngle (in deg): " << (angle-PI/2)*180/PI;
      //Step 3: Generate a trajectory for each angle
      path = lMap.computePath(angle+vehicle_heading, ANGLE_LENGTH);

      //Step 4: Evaluate over each trajectory for goodness and velocity,
      //and then populate our list of votes with that information
      singleVote.goodness = lMap.getPathGoodness(path);
      singleVote.velo = lMap.getPathVelocity(path);
      singleVote.phi = angle;

      voteList.push_back(singleVote);
    }

    //Step 5: Log the results instead of sending to the arbiter
    Arbiter::combinedVoteListType::iterator voteIter = voteList.begin();
    Arbiter::combinedVoteListType::iterator voteEnd = voteList.end();

    
    cout << "\nAngle, Goodness, Velocity (Frame " << frame_num << ")";
    for(; voteIter != voteEnd; ++voteIter) {
      cout << "\n" << (*voteIter).phi << ", " << (*voteIter).goodness << ", " << (*voteIter).velo;
    }
    
    //cout << "\nVehicle loc: (" << lMap.getGlobalXVehicleLocation() << ", " << lMap.getGlobalYVehicleLocation() << ")";

  }
  cout << endl;
#endif
}
