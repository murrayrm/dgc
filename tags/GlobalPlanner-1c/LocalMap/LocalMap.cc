/* LocalMap.cc
   Author: J.D. Salazar
   Created: 7-15-03
   
   This class is the map structure for the local obstacle avoidance mechanism to be used by Team Caltech in the 
   2004 Darpa Grand Challenge. */

#include <algorithm>
#include <sstream>
#include "LocalMap.h"

// Basic cell containing no data
const CellData LocalMap::NO_DATA_CELL;

// Basica constructor.  All values have defaults if not specified.
LocalMap::LocalMap(int numXCells, int numYCells, 
		   float xCellResolution, float yCellResolution, 
		   double globalXOrigin, double globalYOrigin, 
		   int xVehicleDisplacement, int yVehicleDisplacement,
		   int timeOrigin)
  : // The number of cells in the X and Y direction for the map.
    _numXCells(numXCells),
    _numYCells(numYCells),
    // The width of cells in the X and Y direction in meters.
    _xCellResolution(xCellResolution), 
    _yCellResolution(yCellResolution), 
    // Because the map shifts as the vehicle moves the vehicle is always a constant distance from each side of the map.
    // These variables are how far away the vehicle is in local coordinates.
    _xVehicleDisplacement(xVehicleDisplacement),
    _yVehicleDisplacement(yVehicleDisplacement),
    // The time with which the last map data was taken is stored in 'currTime' so 'timeOrigin' represents the starting value
    // of this time.
    _currTime(timeOrigin),
    // The map is stored in a wrap around fashion so that all the data does not need to be shifted every time the vehicle is moved.
    // Therefore, the "origin" or lower left corner of the map changes and the postion of the origin is stored in local coordinates.
    _localXOrigin(DEFAULT_LOCAL_X_ORIGIN),
    _localYOrigin(DEFAULT_LOCAL_Y_ORIGIN)
{
  // The coordinates of the lower left hand corner of the map in global coordinates (UTM).
  // Note the set functions are used so that roundToNearestX and roundToNearestY are used.
  setGlobalXOrigin(globalXOrigin);  
  setGlobalYOrigin(globalYOrigin);
}

// Destructor
LocalMap::~LocalMap()
{
  eraseMap();
}

// Converst global coordinates to local coordinates.
int LocalMap::globalToLocalX(double globalX) const
{
  return globalToPseudolocalX(globalX) % getNumXCells();
}

int LocalMap::globalToLocalY(double globalY) const
{
  return globalToPseudolocalY(globalY) % getNumYCells();
}

// Converts global coordinates to pseudolocal coordinates.
// Pseudolocal coordinates are local coordinates that haven't been transformed using modular arithmetic so they are between 
// 0 and 'numXCells' or 'numYCells' accordingly.
int LocalMap::globalToPseudolocalX(double globalX) const
{
  return getLocalXOrigin() + int( (roundToNearestX(globalX) - getGlobalXOrigin() ) / getXCellResolution() );
}

int LocalMap::globalToPseudolocalY(double globalY) const
{
  return getLocalYOrigin() + int( ( roundToNearestY(globalY) - getGlobalYOrigin() ) / getYCellResolution() );
}


// Converts pseudolocal coordinates to global coordiantes
double LocalMap::pseudolocalToGlobalX(int x) const
{
  return roundToNearestX( (x - getLocalXOrigin()) * getXCellResolution() + getGlobalXOrigin() ); 
}

double LocalMap::pseudolocalToGlobalY(int y) const
{
  return roundToNearestY( (y - getLocalYOrigin()) * getYCellResolution() + getGlobalYOrigin() ); 
}

// Check to make sure requested global coordinates are in the map.
bool LocalMap::inMap(double globalX, double globalY) const
{
  globalX = roundToNearestX(globalX);
  globalY = roundToNearestY(globalY);
  return !( globalX < getGlobalXOrigin() || globalY < getGlobalYOrigin() ||
	   globalX >= getGlobalXOrigin() + getNumXCells() * getXCellResolution() ||
	   globalY >= getGlobalYOrigin() + getNumYCells() * getYCellResolution() );
}

// Finds a cell in the map given global coordinates and returns the data from that cell, making sure the data is
// expired to the current map time.
CellData LocalMap::getCellData(double globalX, double globalY) const
{
  globalX = roundToNearestX(globalX);
  globalY = roundToNearestY(globalY);

  // Check to make sure requested global coordinates are in the map.  If it is not returna copy of NO_DATA_CELL.
  CellData noData = NO_DATA_CELL;
  if( !inMap(globalX, globalY) )
    return noData;

  // Convert global coordinates to local coordinates.
  int localX = globalToLocalX(globalX);
  int localY = globalToLocalY(globalY);

  // Find the cell in the map.
  CoordsType localCoords(localX, localY);
  MapType::const_iterator mapIter = dataMap.find(localCoords);

  // If the cell is in the map return the 'CellData' making sure to expired the data to the current map time.
  if ( mapIter != dataMap.end() )
    {
      const CellData &cell = (*mapIter).second;
      //cout << "\nCell found at (x,y)=(" << localX << ", " << localY << ")";

      return cell.expireData( getCurrentTime() );
    }
  // Otherwhise there is no data for this cell so return a copy of NO_DATA_CELL;
  else
    {
      //cout << "\nNO cell found at (x,y)=(" << localX << ", " << localY << ")";
      return noData;
    }
}

// Inserts a copy of 'cell' into the map unless there already exists a cell at ('globalX', 'globalY').  If a cell already exists
// than it calls 'updateData' using the information from 'cell'.  If the new cell contains no data than nothing happens.
// If the coordinates are not currently part of the globabl map, the out_of_range exception is thrown.
void LocalMap::addCellData(double globalX, double globalY, CellData &cell) throw(std::out_of_range)
{
  globalX = roundToNearestX(globalX);
  globalY = roundToNearestY(globalY);

  // Check to make sure requested global coordinates are in the map.
  if( !inMap(globalX,globalY) )
    {
      std::stringstream errorMessage;
      errorMessage << "Cell at (" << globalX << ", " << globalY << ") given to 'addCellData' is out of range for the local map.",
      throw std::out_of_range( errorMessage.str() );
    }

  // Check if new cell contains no data.
  if(cell == NO_DATA_CELL)
    {
      return;
    }

  // Convert global coordinates to local coordinates.
  int localX = globalToLocalX(globalX);
  int localY = globalToLocalY(globalY);

  CoordsType localCoords(localX, localY);

  // Add the cell to the map.
  MapType::value_type c(localCoords, cell);
  std::pair<MapType::iterator, bool> p = dataMap.insert(c);
  
  // Check if a cell with the same coordinates is already in the map and update the data accordingly.
  if( !p.second )
    {
      (*p.first).second.updateData( cell );
    }
}

// Sets the new vehicle location and updates the map accordingly by changing both the local and globabl coordinates of the origin
// and clearing any data that is now outside the range of the local map.
void LocalMap::setGlobalVehicleLocation(double globalX, double globalY)
{
  globalX = roundToNearestX(globalX);
  globalY = roundToNearestY(globalY);

  // Find the new values of the origin in pseudolocal coordinates.
  int pseudoXOrigin = globalToPseudolocalX(globalX)  - getXVehicleDisplacement();
  int pseudoYOrigin = globalToPseudolocalY(globalY) - getYVehicleDisplacement();

  // If the origin is more than 'numXCells' ('numYCells') cells away in the X (Y) direction than the entire map needs to be erased.
  if( abs(pseudoXOrigin - getLocalXOrigin() ) > getNumXCells() ||
      abs(pseudoYOrigin - getLocalYOrigin() ) > getNumYCells() )
    eraseMap();
  // Otherwise simply erase the part of the map that is now too far away from the vehicle.  To do this we simply erase all the rows
  // and columns in between the old origin and the new origin
  else
    {
      // Erase all the rows in between the old origin and the new origin
      for(int x = std::min(getLocalXOrigin(), pseudoXOrigin); 
	  x < std::max(getLocalXOrigin(), pseudoXOrigin);
	  ++x)
	eraseRow(x % getNumXCells() );

      // Erase all the rows in between the old origin and the new origin
      for(int y = std::min(getLocalYOrigin(), pseudoYOrigin); 
	  y < std::max(getLocalYOrigin(), pseudoYOrigin);
	  ++y)
	eraseCol(y % getNumYCells() );
    }

  // Set the new origin in local coordinates
  setLocalXOrigin( pseudoXOrigin % getNumXCells() );
  setLocalYOrigin( pseudoYOrigin % getNumYCells() );

  // Set the new origin in global coordinates
  setGlobalXOrigin( globalX - getXVehicleDisplacement() * getXCellResolution() );
  setGlobalYOrigin( globalY - getYVehicleDisplacement() * getYCellResolution() );
}

// Outputs the elevation map to a bitmap at fileName
bool LocalMap::outputElevationMap(std::string fileName)
{
  unsigned char *r, *g, *b;
  r = (unsigned char*) malloc(getNumXCells() * getNumYCells());
  g = (unsigned char*) malloc(getNumXCells() * getNumYCells());
  b = (unsigned char*) malloc(getNumXCells() * getNumYCells());

  int i = 0;
  for(int row = getLocalYOrigin(); row < getLocalYOrigin() + getNumYCells(); ++row)
    {
      for(int col = getLocalXOrigin(); col < getLocalXOrigin() + getNumXCells(); ++col)
	{
	  CellData cell = getCellData( pseudolocalToGlobalX(col), pseudolocalToGlobalY(row) );	  
	  if( cell.getElevation() == CellData::NO_DATA )
	    {
	      *(r+i) = (unsigned char)0;
	      *(g+i) = (unsigned char)0;
	      *(b+i) = (unsigned char)0;
	    }
	  else
	    {
	      // Scale elevation to a number between 1 and 510
	      int scaledElevation = cell.getElevation();
	      scaledElevation = std::max( -100, scaledElevation );
	      scaledElevation = std::min( 200, scaledElevation );
	      scaledElevation += 10;
	      
	      scaledElevation = int( double(scaledElevation) / 300.0 * 510 ) + 1;

	      *(r+i) = (unsigned char)( scaledElevation / 2 );
	      *(g+i) = (unsigned char)( scaledElevation <= 255 ? scaledElevation : 255 - scaledElevation );
	      *(b+i) = (unsigned char)( 510 - scaledElevation / 2 );
	    }
	  ++i;
	}
    }
  
  //Draw a red square representing the car.
  for(double x = roundToNearestX(getGlobalXVehicleLocation() - vehicleSize / 2);
      x <= roundToNearestX(getGlobalXVehicleLocation() + vehicleSize / 2);
      x += getXCellResolution() )
    for(double y = roundToNearestY(getGlobalYVehicleLocation() - vehicleSize / 2);
	y <= roundToNearestY(getGlobalYVehicleLocation() + vehicleSize / 2);
	y += getYCellResolution() )
      {
	int pseudolocalX = globalToPseudolocalX(x);
	int pseudolocalY = globalToPseudolocalY(y);
	
	int index = ( pseudolocalX - getLocalXOrigin() ) + ( pseudolocalY - getLocalYOrigin() ) * getNumXCells();


	*(r+index) = 0x00000000;
	*(g+index) = 0x00000063;
	*(b+index) = 0x00000063;
      }


  // write_bmp is in per_to_cla.h
  bool result;
  result = write_bmp(fileName.c_str(), getNumXCells(), getNumYCells(), r, g, b);

  free(r);
  free(g);
  free(b);
  
  return result;
}

// Outputs the obstacle map to a bitmap at fileName.  Postivie obstacles are green, negative obstacles are blue.
bool LocalMap::outputObstacleMap(std::string fileName)
{
  unsigned char *r, *g, *b;
  r = (unsigned char*) malloc(getNumXCells() * getNumYCells());
  g = (unsigned char*) malloc(getNumXCells() * getNumYCells());
  b = (unsigned char*) malloc(getNumXCells() * getNumYCells());

  int i = 0;
  for(int row = getLocalYOrigin(); row < getLocalYOrigin() + getNumYCells(); ++row)
    {
      for(int col = getLocalXOrigin(); col < getLocalXOrigin() + getNumXCells(); ++col)
	{
	  CellData cell = getCellData( pseudolocalToGlobalX(col), pseudolocalToGlobalY(row) );	  
	  if( cell.getObstacleData() == CellData::NO_DATA )
	    {
	      *(r+i) = (unsigned char)0;
	      *(g+i) = (unsigned char)0;
	      *(b+i) = (unsigned char)0;
	    }
	  else
	    {
	      int obstacleData = cell.getObstacleData();
	      
	      if( obstacleData == POSITIVE_OBSTACLE )
		{
		  *(r+i) = (unsigned char)0x000000FF;
		  *(g+i) = (unsigned char)0x00000000;
		  *(b+i) = (unsigned char)0x00000000;
		}
	      else if( obstacleData == NEGATIVE_OBSTACLE )
		{
		  *(r+i) = (unsigned char)0x00000008;
		  *(g+i) = (unsigned char)0x00000000;
		  *(b+i) = (unsigned char)0x00000000;
		}
	      else
		{
		  *(r+i) = (unsigned char)0x000000FF;
		  *(g+i) = (unsigned char)0x000000FF;
		  *(b+i) = (unsigned char)0x000000FF;
		}

	    }
	  ++i;
	}
    }
  
  //Draw a red square representing the car.
  for(double x = roundToNearestX(getGlobalXVehicleLocation() - vehicleSize / 2);
      x <= roundToNearestX(getGlobalXVehicleLocation() + vehicleSize / 2);
      x += getXCellResolution() )
    for(double y = roundToNearestY(getGlobalYVehicleLocation() - vehicleSize / 2);
	y <= roundToNearestY(getGlobalYVehicleLocation() + vehicleSize / 2);
	y += getYCellResolution() )
      {
	int pseudolocalX = globalToPseudolocalX(x);
	int pseudolocalY = globalToPseudolocalY(y);
	
	int index = ( pseudolocalX - getLocalXOrigin() ) + ( pseudolocalY - getLocalYOrigin() ) * getNumXCells();

	*(r+index) = 0x00000000;
	*(g+index) = 0x00000063;
	*(b+index) = 0x00000063;
      }


  // write_bmp is in per_to_cla.h
  bool result;
  result = write_bmp(fileName.c_str(), getNumXCells(), getNumYCells(), r, g, b);

  free(r);
  free(g);
  free(b);
  
  return result;
}


// Outputs the classification map to a bitmap at fileName.
// Key to the map:
//       Outlier classification = white
//       Trees = dark green
//       Soil/Rock/Terrain = tan
//       Vegetation = light green
//       Tall grass = bright green
//       Cover classification = red
//       Water classification = blue
bool LocalMap::outputClassificationMap(std::string fileName)
{
  unsigned char *r, *g, *b;
  r = (unsigned char*) malloc(getNumXCells() * getNumYCells());
  g = (unsigned char*) malloc(getNumXCells() * getNumYCells());
  b = (unsigned char*) malloc(getNumXCells() * getNumYCells());

  int i = 0;
  for(int row = getLocalYOrigin(); row < getLocalYOrigin() + getNumYCells(); ++row)
    {
      for(int col = getLocalXOrigin(); col < getLocalXOrigin() + getNumXCells(); ++col)
	{
	  CellData cell = getCellData( pseudolocalToGlobalX(col), pseudolocalToGlobalY(row) );	  
	  if( cell.getClassification() == CellData::NO_DATA )
	    {
	      *(r+i) = (unsigned char)0;
	      *(g+i) = (unsigned char)0;
	      *(b+i) = (unsigned char)0;
	    }
	  else
	    {
	      int classificationData = cell.getClassification();
	      
	      if( classificationData == OUTLIER_CLASSIFICATION )
		{
		  *(r+i) = (unsigned char)0x000000FF;
		  *(g+i) = (unsigned char)0x000000FF;
		  *(b+i) = (unsigned char)0x000000FF;
		}
	      else if( classificationData == TREE_CLASSIFICATION )
		{
		  *(r+i) = (unsigned char)0x00000023;
		  *(g+i) = (unsigned char)0x0000008E;
		  *(b+i) = (unsigned char)0x00000023;
		}
	      else if( classificationData == SOIL_CLASSIFICATION )
		{
		  *(r+i) = (unsigned char)0x000000EA;
		  *(g+i) = (unsigned char)0x000000EA;
		  *(b+i) = (unsigned char)0x000000AE;
		}
	      else if( classificationData == VEGETATION_CLASSIFICATION )
		{
		  *(r+i) = (unsigned char)0x00000093;
		  *(g+i) = (unsigned char)0x000000DB;
		  *(b+i) = (unsigned char)0x00000070;
		}
	      else if( classificationData == TALL_GRASS_CLASSIFICATION )
		{
		  *(r+i) = (unsigned char)0x00000000;
		  *(g+i) = (unsigned char)0x000000FF;
		  *(b+i) = (unsigned char)0x0000007F;
		}
	      else if( classificationData == COVER_CLASSIFICATION )
		{
		  *(r+i) = (unsigned char)0x000000FF;
		  *(g+i) = (unsigned char)0x00000000;
		  *(b+i) = (unsigned char)0x00000000;
		}
	      else if( classificationData == WATER_CLASSIFICATION )
		{
		  *(r+i) = (unsigned char)0x00000000;
		  *(g+i) = (unsigned char)0x00000000;
		  *(b+i) = (unsigned char)0x000000FF;
		}

	    }
	  ++i;
	}
    }
  
  //Draw a red square representing the car.
  for(double x = roundToNearestX(getGlobalXVehicleLocation() - vehicleSize / 2);
      x <= roundToNearestX(getGlobalXVehicleLocation() + vehicleSize / 2);
      x += getXCellResolution() )
    for(double y = roundToNearestY(getGlobalYVehicleLocation() - vehicleSize / 2);
	y <= roundToNearestY(getGlobalYVehicleLocation() + vehicleSize / 2);
	y += getYCellResolution() )
      {
	int pseudolocalX = globalToPseudolocalX(x);
	int pseudolocalY = globalToPseudolocalY(y);
	
	int index = ( pseudolocalX - getLocalXOrigin() ) + ( pseudolocalY - getLocalYOrigin() ) * getNumXCells();

	*(r+index) = 0x00000000;
	*(g+index) = 0x00000063;
	*(b+index) = 0x00000063;
      }


  // write_bmp is in per_to_cla.h
  bool result;
  result = write_bmp(fileName.c_str(), getNumXCells(), getNumYCells(), r, g, b);

  free(r);
  free(g);
  free(b);
  
  return result;
}

// Outputs a roughness map to a bitmap at fileName.
bool LocalMap::outputRoughnessMap(std::string fileName)
{
  unsigned char *r, *g, *b;
  r = (unsigned char*) malloc(getNumXCells() * getNumYCells());
  g = (unsigned char*) malloc(getNumXCells() * getNumYCells());
  b = (unsigned char*) malloc(getNumXCells() * getNumYCells());

  int i = 0;
  for(int row = getLocalYOrigin(); row < getLocalYOrigin() + getNumYCells(); ++row)
    {
      for(int col = getLocalXOrigin(); col < getLocalXOrigin() + getNumXCells(); ++col)
	{
	  CellData cell = getCellData( pseudolocalToGlobalX(col), pseudolocalToGlobalY(row) );	  
	  if( cell.getRoughness() == CellData::NO_DATA )
	    {
	      *(r+i) = (unsigned char)0;
	      *(g+i) = (unsigned char)0;
	      *(b+i) = (unsigned char)0;
	    }
	  else
	    {
	      // Scale elevation to a number between 1 and 510
	      int scaledRoughness = cell.getRoughness();
	      scaledRoughness = int( double(scaledRoughness) / double(MAX_ROUGH_DATA) * 510.0 ) + 1;	      
	      *(r+i) = (unsigned char)( scaledRoughness / 2 );
	      *(g+i) = (unsigned char)( scaledRoughness <= 255 ? scaledRoughness : 255 - scaledRoughness );
	      *(b+i) = (unsigned char)( 510 - scaledRoughness / 2 );	   
	    }
	  ++i;
	}
    }
  
  //Draw a red square representing the car.
  for(double x = roundToNearestX(getGlobalXVehicleLocation() - vehicleSize / 2);
      x <= roundToNearestX(getGlobalXVehicleLocation() + vehicleSize / 2);
      x += getXCellResolution() )
    for(double y = roundToNearestY(getGlobalYVehicleLocation() - vehicleSize / 2);
	y <= roundToNearestY(getGlobalYVehicleLocation() + vehicleSize / 2);
	y += getYCellResolution() )
      {
	int pseudolocalX = globalToPseudolocalX(x);
	int pseudolocalY = globalToPseudolocalY(y);
	
	int index = ( pseudolocalX - getLocalXOrigin() ) + ( pseudolocalY - getLocalYOrigin() ) * getNumXCells();

	*(r+index) = 0x00000000;
	*(g+index) = 0x00000063;
	*(b+index) = 0x00000063;
      }


  // write_bmp is in per_to_cla.h
  bool result;
  result = write_bmp(fileName.c_str(), getNumXCells(), getNumYCells(), r, g, b);

  free(r);
  free(g);
  free(b);
  
  return result;
}


// Outputs a roughness map to a bitmap at fileName.
bool LocalMap::outputGoodnessMap(std::string fileName)
{
  unsigned char *r, *g, *b;
  r = (unsigned char*) malloc(getNumXCells() * getNumYCells());
  g = (unsigned char*) malloc(getNumXCells() * getNumYCells());
  b = (unsigned char*) malloc(getNumXCells() * getNumYCells());

  int i = 0;
  for(int row = getLocalYOrigin(); row < getLocalYOrigin() + getNumYCells(); ++row)
    {
      for(int col = getLocalXOrigin(); col < getLocalXOrigin() + getNumXCells(); ++col)
	{
	  CellData cell = getCellData( pseudolocalToGlobalX(col), pseudolocalToGlobalY(row) );	  
	  if( cell.getGoodness() == CellData::NO_DATA )
	    {
	      *(r+i) = (unsigned char)0;
	      *(g+i) = (unsigned char)0;
	      *(b+i) = (unsigned char)0;
	    }
	  else
	    {
	      // Scale elevation to a number between 1 and 510
	      int scaledGoodness = cell.getGoodness();
	      scaledGoodness = int( double(scaledGoodness) / double(MAX_GOOD_DATA) * 510.0 ) + 1;	      
	      *(r+i) = (unsigned char)( scaledGoodness / 2 );
	      *(g+i) = (unsigned char)( scaledGoodness <= 255 ? scaledGoodness : 255 - scaledGoodness );
	      *(b+i) = (unsigned char)( 510 - scaledGoodness / 2 );	   
	    }
	  ++i;
	}
    }
  
  //Draw a red square representing the car.
  for(double x = roundToNearestX(getGlobalXVehicleLocation() - vehicleSize / 2);
      x <= roundToNearestX(getGlobalXVehicleLocation() + vehicleSize / 2);
      x += getXCellResolution() )
    for(double y = roundToNearestY(getGlobalYVehicleLocation() - vehicleSize / 2);
	y <= roundToNearestY(getGlobalYVehicleLocation() + vehicleSize / 2);
	y += getYCellResolution() )
      {
	int pseudolocalX = globalToPseudolocalX(x);
	int pseudolocalY = globalToPseudolocalY(y);
	
	int index = ( pseudolocalX - getLocalXOrigin() ) + ( pseudolocalY - getLocalYOrigin() ) * getNumXCells();

	*(r+index) = 0x00000000;
	*(g+index) = 0x00000063;
	*(b+index) = 0x00000063;
      }


  // write_bmp is in per_to_cla.h
  bool result;
  result = write_bmp(fileName.c_str(), getNumXCells(), getNumYCells(), r, g, b);

  free(r);
  free(g);
  free(b);
  
  return result;
}


// Erases the data in every cell of the map.
void LocalMap::eraseMap()
{
  dataMap.clear();
}

// The following two functions delete an entire row (column) from the map.  The row or column are given in local coordinates.
void LocalMap::eraseRow(int localX)
{
  CoordsType l_bound(localX, 0);
  CoordsType u_bound(localX, getNumYCells());

  MapType::iterator lower_bound = dataMap.lower_bound(l_bound);
  MapType::iterator upper_bound = dataMap.upper_bound(u_bound);

  dataMap.erase( lower_bound, upper_bound );
}

void LocalMap::eraseCol(int localY)
{
  for(int x=0; x < getNumXCells(); ++x)
    {
      CoordsType c(x, localY);
      MapType::iterator iter = dataMap.find(c);
      if(iter != dataMap.end())
	{
	  dataMap.erase(c);
	}
    }
}

// Takes in a unserialized perceptor map and adds the data to the current local map.
bool LocalMap::updateMap(const serialMap map)
{
  bool result = true;

  // Update the time and vehicleposition
  setCurrentTime(map.map_time);

  // Update the vehicle's position 
  setGlobalVehicleLocation(roundToNearestX(map.tranRel_x), roundToNearestY(map.tranRel_y));

  // The first two cells in any row are the number of blank cells at the beginning and 
  // the number of data cells in a row.
  int leadingBlankCells;
  int dataCells;

  // Current index of map.map_map
  int i=0;
  for(int row=0; row < map.num_rows; ++row)
    {
      // Read the first two cells in a row.
      leadingBlankCells = map.map_map[i];
      ++i;
      dataCells = map.map_map[i];
      ++i;
   
      for(int col = leadingBlankCells; col < leadingBlankCells + dataCells; ++col)
	{
	  CellData newCell( map.map_map[i], getCurrentTime() );
	  ++i;
	  try
	    {
	      // Convert (col, row) to global coordinates
	      double x = roundToNearestX(row * map.map_x_res + map.origin_x);
	      double y = roundToNearestY(col * map.map_y_res + map.origin_y);
	      
	      addCellData( x, y, newCell );
	    }
	  catch(std::exception &e)
	    {
	      // Catches out of range exception
	      result = false;
	      std::cerr << "LocalMap::updateMap() caught: " << e.what() << std::endl;
	    }
	  
	}
    }

  return result;
}

//THE FOLLOWING FUNCTION SHOULD NOT BE USED
/*
bool LocalMap::updateGoodness()
{
  bool result=true;

  for(int row = getLocalYOrigin(); row < getLocalYOrigin() + getNumYCells(); row++) {
    for(int col = getLocalXOrigin(); col < getLocalXOrigin() + getNumXCells(); col++) {
      CellData cell = getCellData( pseudolocalToGlobalX(col), pseudolocalToGlobalY(row) );

      double globalX = pseudolocalToGlobalX(col);
      double globalY = pseudolocalToGlobalY(row);

      globalX = roundToNearestX(globalX);
      globalY = roundToNearestY(globalY);

      int localX = globalToLocalX(globalX);
      int localY = globalToLocalY(globalY);

      CoordsType localCoords(localX, localY);
      MapType::iterator mapIter = dataMap.find(localCoords);

      if(mapIter != dataMap.end()) {
	//mapIter->second.setGoodness(250);

	CellData finalCell = getCellData(pseudolocalToGlobalX(col), pseudolocalToGlobalY(row));


	cout << "\nCell at (col,row)=(" << col << ", " << row 
	     << "); (globalX, globalY)=(" << globalX << ", " << globalY 
	     << "); (localX, localY)=(" << localX << ", " << localY 
	     << "); mapIter=" << mapIter->second.getGoodness() 
	     << "; cell=" << finalCell.getGoodness() << ";";

	if(!inMap(globalX, globalY)) cout << " NOT IN MAP";
	//cout << "\nCell at (col,row) (" << localX << ", " << localY << ") = " << mapIter->second.getGoodness();
	//cout << "\nCell at (x,y) (" << localX << ", " << localY << ") = " << cell.getGoodness();
	//cout << "\nCell at (x,y) " << localX << ", " << localY << ") = (" << localCoords.first << ", " << localCoords.second << ")";
	//cout << "\nCell at (x,y) " << pseudolocalToGlobalX(col) << ", " << pseudolocalToGlobalY(row) << ") = (" << localCoords.first << ", " << localCoords.second << ")";
	}


	//Strategy: get cell data, evaluate goodness=(obstacle)*obs_weight+roughness*rough_weight+...

      int localX = globalToLocalX(x);
      int localY = globalToLocalY(y);
      CoordsType localCoords(localX, localY);


      MapType::value_type c(localCoords, cell);
      std::pair<MapType::iterator, bool> p = dataMap.insert(c);

  CoordsType localCoords(localX, localY);

  // Add the cell to the map.
  MapType::value_type c(localCoords, cell);
  std::pair<MapType::iterator, bool> p = dataMap.insert(c);
  
  // Check if a cell with the same coordinates is already in the map and update the data accordingly.
  if( !p.second )
    {
      (*p.first).second.updateData( cell );
    }



    }
  }


  return result;
}
*/

double LocalMap::getPathVelocity(svTrajectoryType path) {
  /*
    Simple max velocity determination algorithm:
    Return the maximum velocity

    Possible future idea:
    Determine the max velocity based on the roughness of a given cell, as
    well as the difference in elevation/obstacle data/terrain type between
    the previous and next cells
  */

  return MAX_VELOCITY;
}

int LocalMap::getPathGoodness(svTrajectoryType path) {
  float result = MAX_GOOD_DATA/2;
  int cell_goodness = 0;
  int i=1;
  svTrajectoryType::iterator pathIter = path.begin();
  svTrajectoryType::iterator pathEnd = path.end();

  /*
    Simple goodness determination algorithm:
    Iterate over all the cells, and take the minimum goodness found
    WARNING! WARNING! WARNING! WARNING! WARNING! WARNING! WARNING! 
    This is a really simple, really stupid algorithm!
    DO NOT ATTEMPT TO USE BEYOND 12/6/2003!!!!
  */
  for(; pathIter != pathEnd; ++pathIter) {
    cell_goodness = getCellData((*pathIter).first, (*pathIter).second).getGoodness();
    if(cell_goodness == CellData::NO_DATA) {
      result += 0;
      //cout << "\nHit No data";
    } else {
      if(cell_goodness!=250) 
      cout << "\nHit cell of goodness: " << cell_goodness << " at (" 
	   << globalToPseudolocalX((*pathIter).first) - getLocalXOrigin() << ", " 
	   << globalToPseudolocalY((*pathIter).second) - getLocalYOrigin() << ")";
      result = ((i-1)*result + cell_goodness)/i;
    }
    i++;
  }

  return (int) result;
}

svTrajectoryType LocalMap::computePath(double angle, double length) {
  svTrajectoryType result;
  svCoordsType old_point, new_point, rounded_point;

  old_point.first = getGlobalXVehicleLocation();
  old_point.second = getGlobalYVehicleLocation();

  double adjusted_angle = PI/2 - angle;
  double slope = tan(adjusted_angle);
  double dir_x, dir_y;


  if(sin(adjusted_angle) <0) {
    dir_y = -1;
  } else {
    dir_y = 1;
  }

  if(cos(adjusted_angle) < 0) {
    dir_x = -1;
  } else {
    dir_x = 1;
  }

  double origin_x = getGlobalXOrigin();
  double origin_y = getGlobalYOrigin();
  double res_x = getXCellResolution();
  double res_y = getYCellResolution();

  //cout << "\nSlope: " << slope;

  while(length > 0) {
    if(fabs(old_point.second + slope*distToNextLine(old_point.first,  origin_x, res_x, dir_x)) < 
       fabs(old_point.second +       distToNextLine(old_point.second, origin_y, res_y, dir_y))) {
      new_point.first  = old_point.first  +       distToNextLine(old_point.first, origin_x, res_x, dir_x);
      new_point.second = old_point.second + slope*distToNextLine(old_point.first, origin_x, res_x, dir_x);
      
      rounded_point.first  = floorToNearest(new_point.first,  origin_x, res_x, dir_x);
      rounded_point.second = floorToNearest(new_point.second, origin_y, res_y, 0);
    } else {
      new_point.first  = old_point.first  + (1/slope)*distToNextLine(old_point.second, origin_y, res_y, dir_y);
      new_point.second = old_point.second +           distToNextLine(old_point.second, origin_y, res_y, dir_y);

      rounded_point.first  = floorToNearest(new_point.first,  origin_x, res_x, 0);
      rounded_point.second = floorToNearest(new_point.second, origin_y, res_y, dir_y);
    }

    result.push_back(rounded_point);

    //cout << "\nAdded (" << rounded_point.first << ", " << rounded_point.second << ")";
    //cout << "\nVehicle at (" << globalToPseudolocalX(roundToNearestX(getGlobalXVehicleLocation())) - getLocalXOrigin() 
    //	 << ", " << globalToPseudolocalY(roundToNearestY(getGlobalYVehicleLocation())) - getLocalYOrigin() << ")";
    //cout << "\nAdded (" << globalToPseudolocalX(rounded_point.first) - getLocalXOrigin() 
    //	 << ", " << globalToPseudolocalY(rounded_point.second)  - getLocalYOrigin() << ")";
    //cout << "\nDebug: " << slope*distToNextLine(old_point.first,  origin_x, res_x, dir_x);

    length-=distance(old_point.first, old_point.second, new_point.first, new_point.second);

    old_point = new_point;
  }

  return result;
}


double LocalMap::distance(double x1, double y1, double x2, double y2) {
  return sqrt(pow((y1-y2), 2) + pow((x1-x2), 2));
}

double LocalMap::floorToNearest(double current, double origin, double res, double dir) {
  if(dir > 0) {
    return (std::floor((current-origin)/res)-1) * res + origin;
  } else {
    return (std::floor((current-origin)/res)) * res + origin;
  }
}


double LocalMap::distToNextLine(double current, double origin, double res, double dir) {
  return (roundToNextLine(current, origin, res, dir)-current);
}


double LocalMap::roundToNextLine(double current, double origin, double res, double dir) {
  if(dir > 0) {
    return (std::floor((current-origin)/res) + 1) * res + origin;
  } else {
    return (std::ceil((current-origin)/res) - 1) * res + origin;
  }
}
