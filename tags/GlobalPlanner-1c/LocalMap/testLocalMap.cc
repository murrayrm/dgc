/* testLocalMap.cc
   Author: J.D. Salazar
   Created: 7-22-03
   
Altered by Jeremy Gillula

   This program tests the LocalMap class. */

#include <iostream>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <set>
#include <functional>
#include "LocalMap.h"
#include "CellData.h"
#include "ErrorContext.h"
#include "../pertocla/per_to_cla.h"

// Constants used by 'perceptorMapTests()'
static const std::string PATH_TO_MAPS = "../pertocla/serialized_maps";
static const std::string INPUT_FILE_PREFIX = "ptc_frame_";
static const std::string INPUT_FILE_SUFFIX = ".ptc";
static const int MAX_PERCEPTOR_FRAMES = 330;
static const std::string PATH_TO_OUTPUT_ELEV_MAPS = "sampleElevationMaps";
static const std::string OUTPUT_ELEV_FILE_PREFIX = "elevationMap";
static const std::string OUTPUT_ELEV_FILE_SUFFIX = ".bmp";
static const std::string PATH_TO_OUTPUT_OBS_MAPS = "sampleObstacleMaps";
static const std::string OUTPUT_OBS_FILE_PREFIX = "obstacleMap";
static const std::string OUTPUT_OBS_FILE_SUFFIX = ".bmp";
static const std::string PATH_TO_OUTPUT_CLASS_MAPS = "sampleClassificationMaps";
static const std::string OUTPUT_CLASS_FILE_PREFIX = "classificationMap";
static const std::string OUTPUT_CLASS_FILE_SUFFIX = ".bmp";
static const std::string PATH_TO_OUTPUT_ROUGH_MAPS = "sampleRoughnessMaps";
static const std::string OUTPUT_ROUGH_FILE_PREFIX = "roughnessMap";
static const std::string OUTPUT_ROUGH_FILE_SUFFIX = ".bmp";
static const std::string PATH_TO_OUTPUT_GOOD_MAPS = "sampleGoodnessMaps";
static const std::string OUTPUT_GOOD_FILE_PREFIX = "goodnessMap";
static const std::string OUTPUT_GOOD_FILE_SUFFIX = ".bmp";


// Reliably reproducible pseudorandom numbers:
int rnd() { return int(lrand48()); }
double drnd(double max) { return double(drand48() * max); }
int rnd(int max) { return int(drand48() * max); }


void cellDataTests(ErrorContext &ec, int testLevel)
{
  
  {
    std::ostringstream oss;
    oss << "--- Cell Data tests ---";
    ec.DESC(oss.str());
  }

  // Check default constructor
  {
    CellData cell;
    {
      std::ostringstream oss;
      oss << "Default constructor sets elevation to 'NO_DATA'.";
      ec.DESC(oss.str());
    }
    ec.result( cell.getElevation() == CellData::NO_DATA);
    
    {
      std::ostringstream oss;
      oss << "Default constructor sets classification to 'NO_DATA'.";
      ec.DESC(oss.str());
    }
    ec.result( cell.getClassification() == CellData::NO_DATA);
    
    {
      std::ostringstream oss;
      oss << "Default constructor sets obstacle data to 'NO_DATA'.";
      ec.DESC(oss.str());
    }
    ec.result( cell.getObstacleData() == CellData::NO_DATA);
    
    {
      std::ostringstream oss;
      oss << "Default constructor sets roughness to 'NO_DATA'.";
      ec.DESC(oss.str());
    }
    ec.result( cell.getRoughness() == CellData::NO_DATA);

    {
      std::ostringstream oss;
      oss << "Default constructor sets time modified to 'NO_DATA'.";
      ec.DESC(oss.str());
    }
    ec.result( cell.getTimeLastModified() == CellData::NO_DATA);
  }
  
  // Check constructo where parameters are passed to it.
  {
    for(int pass = 1; pass <= testLevel; ++pass)
      {
	int elevation = rnd(MAX_ELEV_DATA);
	int elevationConfidence = rnd(MAX_ELEV_CONF);
	int classification = std::max(1, rnd(MAX_CLASS_DATA));
	int classificationConfidence = rnd(MAX_CLASS_CONF_VAL);
	int objectData = std::max(1, rnd(MAX_OBJECT_DATA));
	int objectConfidence = rnd(MAX_OBJECT_CONF);
	int roughness = rnd(MAX_ROUGH_DATA);
	int roughnessConfidence = rnd(MAX_ROUGH_CONF);
	int timeLastModified = rnd(INT_MAX);

	CellData cell(elevation, elevationConfidence,
		  classification, classificationConfidence,
		  objectData, objectConfidence,
		  roughness, roughnessConfidence,
		  timeLastModified);

	{
	  std::ostringstream oss;
	  oss << "Constructor set elevation = "
	      << elevation 
	      << "."
	      << " (pass="
	      << pass << ")";
	  ec.DESC(oss.str());
	}
	ec.result( cell.getElevation() == elevation);

	{
	  std::ostringstream oss;
	  oss << "Constructor set elevation confidence = "
	      << elevationConfidence 
	      << "."
	      << " (pass="
	      << pass << ")";
	  ec.DESC(oss.str());
	}
	ec.result( cell.getElevationConfidence() == elevationConfidence);

	{
	  std::ostringstream oss;
	  oss << "Constructor set classification = "
	      << classification 
	      << "."
	      << " (pass="
	      << pass << ")";
	  ec.DESC(oss.str());
	}
	ec.result( cell.getClassification() == classification);

	{
	  std::ostringstream oss;
	  oss << "Constructor set classification confidence = "
	      << elevation 
	      << "."
	      << " (pass="
	      << pass << ")";
	  ec.DESC(oss.str());
	}
	ec.result( cell.getClassificationConfidence() == classificationConfidence);

	{
	  std::ostringstream oss;
	  oss << "Constructor set obstacle data = "
	      << objectData
	      << "."
	      << " (pass="
	      << pass << ")";
	  ec.DESC(oss.str());
	}
	ec.result( cell.getObstacleData() == objectData);

	{
	  std::ostringstream oss;
	  oss << "Constructor set obstacle data confidence = "
	      << objectConfidence
	      << "."
	      << " (pass="
	      << pass << ")";
	  ec.DESC(oss.str());
	}
	ec.result( cell.getObstacleDataConfidence() == objectConfidence);

	{
	  std::ostringstream oss;
	  oss << "Constructor set roughness = "
	      << roughness
	      << "."
	      << " (pass="
	      << pass << ")";
	  ec.DESC(oss.str());
	}
	ec.result( cell.getRoughness() == roughness);
	
	// Currently roughness confidence is not used.
	/*
	{
	  std::ostringstream oss;
	  oss << "Constructor set roughness confidence = "
	      << roughnessConfidence
	      << "."
	      << " (pass="
	      << pass << ")";
	  ec.DESC(oss.str());
	}
	ec.result( cell.getRoughnessConfidnece() == roughnessConfidence);
	*/

	{
	  std::ostringstream oss;
	  oss << "Constructor set time modified = "
	      << elevation 
	      << "."
	      << " (pass="
	      << pass << ")";
	  ec.DESC(oss.str());
	}
	ec.result( cell.getTimeLastModified() == timeLastModified);

      }
  }

  // Check == operator and != operator
  {
    CellData cell1;
    CellData cell2;

    {
      std::ostringstream oss;
      oss << "== and != operators on cells with no data";
      ec.DESC(oss.str());
    }
    ec.result( cell1 == cell2 
	       && cell2 == cell1
	       && !(cell1 != cell2)
	       && !(cell2 != cell1));
  }

  {
    int elevationConfidence = rnd(MAX_ELEV_CONF);
    int classificationConfidence = rnd(MAX_CLASS_CONF_VAL);
    int objectConfidence = rnd(MAX_OBJECT_CONF);
    int roughnessConfidence = rnd(MAX_ROUGH_CONF);
    int timeLastModified = rnd(INT_MAX);
    
    CellData cell1( CellData::NO_DATA, CellData::NO_DATA,
		    CellData::NO_DATA, CellData::NO_DATA,
		    CellData::NO_DATA, CellData::NO_DATA,
		    CellData::NO_DATA, CellData::NO_DATA,
		    CellData::NO_DATA);
    CellData cell2( CellData::NO_DATA, elevationConfidence,
		    CellData::NO_DATA, classificationConfidence,
		    CellData::NO_DATA, objectConfidence,
		    CellData::NO_DATA, roughnessConfidence,
		    timeLastModified);

    {
      std::ostringstream oss;
      oss << "== and != operators on cells with no data (one with confidences)";
      ec.DESC(oss.str());
    }
    ec.result( cell1 == cell2
	       && cell2 == cell1
	       && !(cell1 != cell2)
	       && !(cell2 != cell1) );
  }



  {
    for(int pass = 1; pass <= testLevel; ++pass)
      {
	int elevation = rnd(MAX_ELEV_DATA);
	int elevationConfidence = rnd(MAX_ELEV_CONF);
	int classification =rnd(MAX_CLASS_DATA);
	int classificationConfidence = rnd(MAX_CLASS_CONF_VAL);
	int objectData = rnd(MAX_OBJECT_DATA);
	int objectConfidence = rnd(MAX_OBJECT_CONF);
	int roughness = rnd(MAX_ROUGH_DATA);
	int roughnessConfidence = rnd(MAX_ROUGH_CONF);
	int timeLastModified = rnd(INT_MAX);
	
	CellData cell1(elevation, elevationConfidence,
		       classification, classificationConfidence,
		       objectData, objectConfidence,
		       roughness, roughnessConfidence,
		       timeLastModified);
	CellData cell2(elevation, elevationConfidence,
		       classification, classificationConfidence,
		       objectData, objectConfidence,
		       roughness, roughnessConfidence,
		       timeLastModified);
	{
	  std::ostringstream oss;
	  oss << "== and != operators on cells with equal random data (pass="
	      << pass << ")";
	  ec.DESC(oss.str());
	}
	ec.result( cell1 == cell2
		   && cell2 == cell1
		   && !(cell1 != cell2)
		   && !(cell2 != cell1));
      }
  }

  {
    int elevation = rnd(MAX_ELEV_DATA);
    int elevationConfidence = rnd(MAX_ELEV_CONF);
    int classification =rnd(MAX_CLASS_DATA);
    int classificationConfidence = rnd(MAX_CLASS_CONF_VAL);
    int objectData = rnd(MAX_OBJECT_DATA);
    int objectConfidence = rnd(MAX_OBJECT_CONF);
    int roughness = rnd(MAX_ROUGH_DATA);
    int roughnessConfidence = rnd(MAX_ROUGH_CONF);
    int timeLastModified = rnd(INT_MAX);
    
    CellData cell1( CellData::NO_DATA, CellData::NO_DATA,
		    CellData::NO_DATA, CellData::NO_DATA,
		    CellData::NO_DATA, CellData::NO_DATA,
		    CellData::NO_DATA, CellData::NO_DATA,
		    CellData::NO_DATA);
    CellData cell2( elevation, elevationConfidence,
		    classification, classificationConfidence,
		    objectData, objectConfidence,
		    roughness, roughnessConfidence,
		    timeLastModified);
    
    {
      std::ostringstream oss;
      oss << "== and != operators on cell with no data and cell with random data";
      ec.DESC(oss.str());
    }
    ec.result( cell1 != cell2
	       && cell2 != cell1
	       && !(cell1 == cell2)
	       && !(cell2 == cell1) );
  }


  {
    for(int pass = 1; pass <= testLevel; ++pass)
      {
	int elevation1, elevationConfidence1, classification1, classificationConfidence1;
	int objectData1, objectConfidence1, roughness1, roughnessConfidence1, timeLastModified1;
	int elevation2, elevationConfidence2, classification2, classificationConfidence2;
	int objectData2, objectConfidence2, roughness2, roughnessConfidence2, timeLastModified2;
	
	do
	  {
	    elevation1 = rnd(MAX_ELEV_DATA);
	    elevationConfidence1 = rnd(MAX_ELEV_CONF);
	    classification1 = std::max(1, rnd(MAX_CLASS_DATA));
	    classificationConfidence1 = rnd(MAX_CLASS_CONF_VAL);
	    objectData1 = std::max(1, rnd(MAX_OBJECT_DATA));
	    objectConfidence1 = rnd(MAX_OBJECT_CONF);
	    roughness1 = rnd(MAX_ROUGH_DATA);
	    roughnessConfidence1 = rnd(MAX_ROUGH_CONF);
	    timeLastModified1 = rnd(INT_MAX);
	    
	    elevation2 = rnd(MAX_ELEV_DATA);
	    elevationConfidence2 = rnd(MAX_ELEV_CONF);
	    classification2 = rnd(MAX_CLASS_DATA);
	    classificationConfidence2 = rnd(MAX_CLASS_CONF_VAL);
	    objectData2 = rnd(MAX_OBJECT_DATA);
	    objectConfidence2 = rnd(MAX_OBJECT_CONF);
	    roughness2 = rnd(MAX_ROUGH_DATA);
	    roughnessConfidence2 = rnd(MAX_ROUGH_CONF);
	    timeLastModified2 = rnd(INT_MAX);
	  }
	while( elevation1 == elevation2 &&
	       elevationConfidence1 == elevationConfidence2 &&
	       classification1 == classification2 &&
	       classificationConfidence1 == classificationConfidence2 &&
	       objectData1 == objectData2 &&
	       objectConfidence1 == objectConfidence2 &&
	       roughness1 == roughness2 &&
	       // Currently roughness confidence is stored in a cell.
	       // roughnessConfidence1 == roughnessConfidence2 &&
	       timeLastModified1 == timeLastModified2 );
	
	CellData cell1(elevation1, elevationConfidence1,
		       classification1, classificationConfidence1,
		       objectData1, objectConfidence1,
		       roughness1, roughnessConfidence1,
		       timeLastModified1);
	CellData cell2(elevation2, elevationConfidence2,
		       classification2, classificationConfidence2,
		       objectData2, objectConfidence2,
		       roughness2, roughnessConfidence2,
		       timeLastModified2);
	{
	  std::ostringstream oss;
	  oss << "== and != operators on cells with unequal random data (pass="
	      << pass << ")";
	  ec.DESC(oss.str());
	}
	ec.result( cell1 != cell2
		   && cell2 != cell1
		   && !(cell1 == cell2)
		   && !(cell2 == cell1));
      }
  }

  // Constructor using encoded perceptor data
  {
    for(int pass = 1; pass <= testLevel; ++pass)
      {
	int cellData1 = rnd(INT_MAX);
	int timeLastModified1 = rnd(INT_MAX);
	// Half the time have cells with equal data or modified time
	int cellData2 = ( rnd(2) == 1 ? rnd(INT_MAX) : cellData1);
	int timeLastModified2 = ( rnd(2) == 1 ? rnd(INT_MAX) : timeLastModified1 );
	    
	CellData cell1(cellData1, timeLastModified1);
	CellData cell2(cellData2, timeLastModified2);

	{
	  std::ostringstream oss;
	  oss << "Constructor using encoded perceptor data"
	      << " (pass=" << pass << ").";
	  ec.DESC(oss.str());
	}
	ec.result( ( (cell1 == cell2) ?
		     (cellData1 == cellData2 && timeLastModified1 == timeLastModified2)
		     || (cell1.getElevation() == CellData::NO_DATA &&
			 cell1.getClassification() == CellData::NO_DATA &&
			 cell1.getRoughness() == CellData::NO_DATA &&
			 cell1.getObstacleData() == CellData::NO_DATA &&
			 cell2.getElevation() == CellData::NO_DATA &&
			 cell2.getClassification() == CellData::NO_DATA &&
			 cell2.getRoughness() == CellData::NO_DATA &&
			 cell2.getObstacleData() == CellData::NO_DATA)
		     : (cellData1 != cellData2 || timeLastModified1 != timeLastModified2) ) );	
      }
  }
  
  // Copy constructor
  {
    for(int pass = 1; pass <= testLevel; ++pass)
      {
	int cellData = rnd(INT_MAX);
	int timeLastModified = rnd(INT_MAX);
	CellData cell1(cellData, timeLastModified);
	CellData cell2(cell1);

	{
	  std::ostringstream oss;
	  oss << "Copy constructor"
	      << " (pass=" << pass << ").";
	  ec.DESC(oss.str());
	}
	ec.result( cell1 == cell2 );
      }
  }

  // Assignment operator
  {
    for(int pass = 1; pass <= testLevel; ++pass)
      {
	int cellData = rnd(INT_MAX);
	int timeLastModified = rnd(INT_MAX);
	CellData cell1(cellData, timeLastModified);
	CellData cell2;
	
	cell2 = cell1;

	{
	  std::ostringstream oss;
	  oss << "Assignment operator"
	      << " (pass=" << pass << ").";
	  ec.DESC(oss.str());
	}
	ec.result( cell1 == cell2 );

	cell1 = cell1;


	{
	  std::ostringstream oss;
	  oss << "Assignment operator, self assignment"
	      << " (pass=" << pass << ").";
	  ec.DESC(oss.str());
	}
	ec.result( cell1 == cell2 );

      }
  }
}

void constructorTests(ErrorContext &ec, int testLevel)
{  
  {
    std::ostringstream oss;
    oss << "--- Constructor and get (except 'getCellData()') operations ---";
    ec.DESC(oss.str());
  }

  // Verify default values make sense
  {
    std::ostringstream oss;
    oss << "Default vehicle displacements are in proper range.";
    ec.DESC(oss.str());
  }
  ec.result( LocalMap::DEFAULT_X_VEHICLE_DISPLACEMENT >= 0 && 
	     LocalMap::DEFAULT_X_VEHICLE_DISPLACEMENT < LocalMap::DEFAULT_NUM_X_CELLS &&
	     LocalMap::DEFAULT_Y_VEHICLE_DISPLACEMENT >= 0 && 
	     LocalMap::DEFAULT_Y_VEHICLE_DISPLACEMENT < LocalMap::DEFAULT_NUM_Y_CELLS );

  // Verify that roundToNearest works
  {
    for(int pass=1; pass<= testLevel; ++pass)
      {
	double randomDouble = drnd( testLevel^3 / 2 );
	float randomResolution = drnd( testLevel^3 / 8 );
	double roundedDouble = LocalMap::roundToNearest( randomDouble, randomResolution );
	{
	  std::ostringstream oss;
	  oss << "roundToNearest(n, res) return a multiple of res"
	      << " {pass=" << pass << ").";
	  ec.DESC(oss.str());
	}
	ec.result( int(roundedDouble / randomResolution) * randomResolution == roundedDouble );

	{
	  std::ostringstream oss;
	  oss << "roundToNearest(n, res) return closest multiple of res"
	      << " {pass=" << pass << ").";
	  ec.DESC(oss.str());
	}
	ec.result( std::fabs( roundedDouble - randomDouble ) < std::fabs( roundedDouble + randomResolution - randomDouble ) &&
		   std::fabs( roundedDouble - randomDouble ) < std::fabs( roundedDouble - randomResolution - randomDouble ) );
      }
  }
  
  for(int pass = 1; pass <= testLevel; ++pass)
    {
      const int numXCells = rnd(100 * testLevel) + 1;
      const int numYCells = rnd(100 * testLevel) + 1;
      const float xResolution = float(drnd(testLevel) + 1);
      const float yResolution = float(drnd(testLevel) + 1);
      const double globalXOrigin = LocalMap::roundToNearest( drnd(100 * testLevel), xResolution );
      const double globalYOrigin = LocalMap::roundToNearest( drnd(100 *testLevel), yResolution );
      const int xVehicleDisplacement = int( numXCells / 2 );
      const int yVehicleDisplacement = int( numYCells / 2 );
      const int timeOrigin = rnd(testLevel);
      
      LocalMap map(numXCells, numYCells, 
		   xResolution, yResolution, 
		   globalXOrigin, globalYOrigin,
		   xVehicleDisplacement, yVehicleDisplacement,
		   timeOrigin);
      
      // Verify constructor set map up correctly
      {
	std::ostringstream oss;
	oss << "constructor set numXCells to " << numXCells
	    << " (pass=" << pass << ")";
	ec.DESC(oss.str());
      }
      ec.result( map.getNumXCells() == numXCells );
      
      {
	std::ostringstream oss;
	oss << "constructor set numYCells to " << numYCells
	    << " (pass=" << pass << ")";
	ec.DESC(oss.str());
      }
      ec.result( map.getNumYCells() == numYCells );

      {
	std::ostringstream oss;
	oss << "constructor set xCellResolution to " << xResolution
	    << " (pass=" << pass << ")";
	ec.DESC(oss.str());
      }
      ec.result( map.getXCellResolution() == xResolution );

      {
	std::ostringstream oss;
	oss << "constructor set yCellResolution to " << yResolution
	    << " (pass=" << pass << ")";
	ec.DESC(oss.str());
      }
      ec.result( map.getYCellResolution() == yResolution );

      {
	std::ostringstream oss;
	oss << "constructor set globalXOrigin to " << globalXOrigin
	    << " (pass=" << pass << ")";
	ec.DESC(oss.str());
      }
      ec.result( map.getGlobalXOrigin() == globalXOrigin );

      {
	std::ostringstream oss;
	oss << "constructor set globalYOrigin to " << globalYOrigin
	    << " (pass=" << pass << ")";
	ec.DESC(oss.str());
      }
      ec.result( map.getGlobalYOrigin() == globalYOrigin );

      {
	std::ostringstream oss;
	oss << "constructor set xVehicleDisplacement to " << xVehicleDisplacement
	    << " (pass=" << pass << ")";
	ec.DESC(oss.str());
      }
      ec.result( map.getXVehicleDisplacement() == xVehicleDisplacement );
      
      {
	std::ostringstream oss;
	oss << "constructor set yVehicleDisplacemnt to " << yVehicleDisplacement
	    << "(pass=" << pass << ")";
	ec.DESC(oss.str());
      }
      ec.result( map.getYVehicleDisplacement() == yVehicleDisplacement );

      {
	std::ostringstream oss;
	oss << "constructor set timeOrigin to " << timeOrigin
	    << " (pass=" << pass << ")";
	ec.DESC(oss.str());
      }
      ec.result( map.getCurrentTime() == timeOrigin );

      // Verify vehicle coordinates are correctly set.
      {
	std::ostringstream oss;
	oss << "Vehicle coordinates correctly set."
	    << " (pass=" << pass << ")";
	ec.DESC(oss.str());
      }
      ec.result( map.getGlobalXVehicleLocation() == map.roundToNearestX(globalXOrigin + xResolution * xVehicleDisplacement) &&
		 map.getGlobalYVehicleLocation() == map.roundToNearestY(globalYOrigin + yResolution * yVehicleDisplacement) );
    }
}

void getAndAddTests(ErrorContext &ec, int testLevel)
{

  {
    std::ostringstream oss;
    oss << "--- 'addCellData() and 'getCellData()' operations on a default map ---";
    ec.DESC(oss.str());
  }

  LocalMap map;

  // Verify cells have no data currently.
  for(int pass=1; pass <= testLevel; ++pass)
    {
      double x, y;
      x = map.roundToNearestX(drnd( ( map.getNumXCells() - 1 ) * double( map.getXCellResolution() ) + map.getGlobalXOrigin() ) );
      y = map.roundToNearestY(drnd( ( map.getNumYCells() - 1 ) * double( map.getYCellResolution() ) + map.getGlobalYOrigin() ) );

      std::ostringstream oss;
      oss << "Cell at (" << x << ", " << y << ") has no data. (pass=" << pass << ")";
      ec.DESC(oss.str());

      CellData testCell = map.getCellData(x,y);

      ec.result( testCell.getElevation() == CellData::NO_DATA &&
		 testCell.getClassification() == CellData::NO_DATA &&
		 testCell.getObstacleData() == CellData::NO_DATA &&
		 testCell.getRoughness() == CellData::NO_DATA );
    }

  std::set< std::pair<double, double> > alreadyAddedCoordinates; 
  for(int pass=1; pass <= testLevel; ++pass)
    {
      double x, y;
      std::pair<std::set<std::pair<double, double> >::iterator, bool> result;
      do
	{
	  x = map.roundToNearestX(drnd( ( map.getNumXCells() - 1 ) * double( map.getXCellResolution() ) + 
					map.getGlobalXOrigin() ) );
	  y = map.roundToNearestY(drnd( ( map.getNumYCells() - 1 ) * double( map.getYCellResolution() ) + 
					map.getGlobalYOrigin() ) );
	  std::pair<double,double> coords(x,y);
	  result = alreadyAddedCoordinates.insert(coords);
	}
      while( !result.second );
  
      std::ostringstream oss;
      oss << "Adding and verifying cell at (" << x << ", " << y << ") (pass=" << pass << ").";
      ec.DESC(oss.str());

      int cellData = rnd(INT_MAX);
      CellData copy;
      {
	CellData cell(cellData, map.getCurrentTime());
	copy = cell;

	map.addCellData(x, y, cell);
      }
      ec.result(map.getCellData(x,y) == copy);
    }
}

void perceptorMapTests(ErrorContext &ec, int testLevel)
{

  {
    std::ostringstream oss;
    oss << "--- Perceptor map tests on maps in " << PATH_TO_MAPS << " ---";
    ec.DESC(oss.str());
  }

  LocalMap map;

  for(int pass = 1; pass <= std::min(testLevel, MAX_PERCEPTOR_FRAMES); ++pass)
    {
      std::ostringstream inputFileName;
      inputFileName << INPUT_FILE_PREFIX;
      inputFileName.width(3);
      inputFileName.fill('0');
      inputFileName << pass;
      inputFileName << INPUT_FILE_SUFFIX;

      std::ostringstream oss;
      oss << "Reading file " << inputFileName.str() << "...";
      ec.DESC(oss.str());

      std::ostringstream inputFullPath;
      inputFullPath << PATH_TO_MAPS << "/" << inputFileName.str();

      serialMap sMap;
      ec.result( sMap.readMap( inputFullPath.str().c_str() ) );

      std::ostringstream oss2;
      oss2 << "Updating map with frame " << inputFileName.str() << "...";
      ec.DESC(oss2.str());

      ec.result( map.updateMap(sMap) );


      std::ostringstream outputElevFileName;
      outputElevFileName << OUTPUT_ELEV_FILE_PREFIX;
      outputElevFileName.width(3);
      outputElevFileName.fill('0');
      outputElevFileName << pass;
      outputElevFileName << OUTPUT_ELEV_FILE_SUFFIX;
      std::ostringstream outputElevFullPath;
      outputElevFullPath << PATH_TO_OUTPUT_ELEV_MAPS << "/" << outputElevFileName.str();

      std::ostringstream oss3;
      oss3 << "Writing elevation map to " << outputElevFileName.str() << "...";
      ec.DESC(oss3.str());

      ec.result( map.outputElevationMap( outputElevFullPath.str() ) );


      std::ostringstream outputObsFileName;
      outputObsFileName << OUTPUT_OBS_FILE_PREFIX;
      outputObsFileName.width(3);
      outputObsFileName.fill('0');
      outputObsFileName << pass;
      outputObsFileName << OUTPUT_OBS_FILE_SUFFIX;
      std::ostringstream outputObsFullPath;
      outputObsFullPath << PATH_TO_OUTPUT_OBS_MAPS << "/" << outputObsFileName.str();

      std::ostringstream oss4;
      oss4 << "Writing obstacle map to " << outputObsFileName.str() << "...";
      ec.DESC(oss4.str());

      ec.result( map.outputObstacleMap( outputObsFullPath.str() ) );
      

      std::ostringstream outputClassFileName;
      outputClassFileName << OUTPUT_CLASS_FILE_PREFIX;
      outputClassFileName.width(3);
      outputClassFileName.fill('0');
      outputClassFileName << pass;
      outputClassFileName << OUTPUT_CLASS_FILE_SUFFIX;
      std::ostringstream outputClassFullPath;
      outputClassFullPath << PATH_TO_OUTPUT_CLASS_MAPS << "/" << outputClassFileName.str();

      std::ostringstream oss5;
      oss5 << "Writing classification map to " << outputClassFileName.str() << "...";
      ec.DESC(oss5.str());

      ec.result( map.outputClassificationMap( outputClassFullPath.str() ) );

      std::ostringstream outputRoughFileName;
      outputRoughFileName << OUTPUT_ROUGH_FILE_PREFIX;
      outputRoughFileName.width(3);
      outputRoughFileName.fill('0');
      outputRoughFileName << pass;
      outputRoughFileName << OUTPUT_ROUGH_FILE_SUFFIX;
      std::ostringstream outputRoughFullPath;
      outputRoughFullPath << PATH_TO_OUTPUT_ROUGH_MAPS << "/" << outputRoughFileName.str();

      std::ostringstream oss6;
      oss6 << "Writing roughness map to " << outputRoughFileName.str() << "...";
      ec.DESC(oss6.str());

      ec.result( map.outputRoughnessMap( outputRoughFullPath.str() ) );


      /*   
      std::ostringstream oss7;
      oss7 << "Generating goodness map";
      ec.DESC(oss7.str());
      ec.result( map.updateGoodness() );
      */

      //cout << "\nVehicle loc: (" << map.getGlobalXVehicleLocation() << ", " << map.getGlobalYVehicleLocation() << ")";
      //cout << "\nVehicle loc: (" << map.getXVehicleDisplacement() << ", " << map.getYVehicleDisplacement() << ")";
      //cout << "\nVehicle loc: (" << map.getGlobalXOrigin() << ", " << map.getGlobalYOrigin() << ")";

      //cout << endl;

      std::ostringstream outputGoodFileName;
      outputGoodFileName << OUTPUT_GOOD_FILE_PREFIX;
      outputGoodFileName.width(3);
      outputGoodFileName.fill('0');
      outputGoodFileName << pass;
      outputGoodFileName << OUTPUT_GOOD_FILE_SUFFIX;
      std::ostringstream outputGoodFullPath;
      outputGoodFullPath << PATH_TO_OUTPUT_GOOD_MAPS << "/" << outputGoodFileName.str();

      std::ostringstream oss8;
      oss8 << "Writing goodness map to " << outputGoodFileName.str() << "...";
      ec.DESC(oss8.str());

      ec.result( map.outputGoodnessMap( outputGoodFullPath.str() ) );

    }
}

int main(int argc, const char *argv[])
{
  // Use the first argument, if any, as the test leve.
  int testLevel = ( (argc > 1) ? atoi(argv[1]) : 0);

  if( testLevel <= 0 )
    {
      // User gave an invalid argument or didn't give an argument so tell them how to use the program.
      std::cout << "\n\nTest level may be specified as a positivie integer argument, where a higher level yields more tests.\n"
		<< "Miminum level needed for specific tests are:\n "
		<< "\t1 - Cell Data tests\n"
		<< "\t2 - Constructor and get (except 'getCellData()') tests,\n"
		<< "\t3 - 'addCellData()' and 'getCellData()' tests.\n"
		<< "\t4 - Test updateData using perceptor maps (not implemented yet).\n\n";
      
      return 0;
    }

  // Set everything up
  std::cout << "\n\nPerforming a level " << testLevel 
	    << " check of LocalMap.\n\n";
  ErrorContext ec( std::cout, __FILE__ );
  srand48(testLevel);

  // Perform tests
  if( testLevel >= 1 )
    {
      cellDataTests(ec, testLevel);
    }

  if( testLevel >= 2 )
    {
      constructorTests(ec, testLevel);
    }

  if( testLevel >= 3 )
    {
      getAndAddTests(ec, testLevel);
    }

  if( testLevel >= 4 )
    {
      perceptorMapTests(ec, testLevel);
    }

  // Return 0 (success) if all tests were passed, 1 otherwise 
  return !ec.passedAllTests();
}
