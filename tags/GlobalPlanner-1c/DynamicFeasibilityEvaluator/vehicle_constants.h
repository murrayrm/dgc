
//#define USE_DEBUG_VEHICLE_CONSTANTS 

#define EPSILON .0000000001  // 10^-10.  assume below this is zero.

#ifdef      USE_DEBUG_VEHICLE_CONSTANTS

/* these are the vehicle constants that make it easy to derive the correct values for 
 * all the functions we need to test
 *
 * NOTE: more, better tests could be done if these parameters were variables that could be
 *      modified...
 */

#define     VEHICLE_MASS    1000    // [kg]

#define     VEHICLE_H   1.0     // [m], height of Center of Mass from ground

#define     VEHICLE_L   1.0     // [m], distance of left wheel to projection of CM down on axle
#define     VEHICLE_R   1.0     // [m], distance of right wheel to projection of CM down on axle

#define     VEHICLE_AXLE_DISTANCE   1.0     // [m] distance between front and rear axles

#define     VEHICLE_MAX_V   18.0 // [m/s], max permissible velocity of vehicle (approx 40 mph)

#else

/* These are the vehicle's true constants */

#define     VEHICLE_MASS    2291    // [kg] TODO: NOTE: this is an approximation

#define     VEHICLE_H   (1.95/2.0)    // [m], height of Center of Mass from ground
                                    // TODO: NOTE: this is the worst case scenario, since cannot know from spec sheet
                                    //
#define     VEHICLE_L   (1.651/2.0)     // [m], distance of left wheel to projection of CM down on axle
#define     VEHICLE_R   (1.651/2.0)     // [m], distance of right wheel to projection of CM down on axle

#define     VEHICLE_AXLE_DISTANCE   2.946     // [m] distance between front and rear axles

#define     VEHICLE_MAX_V   15.0 // [m/s], max permissible velocity of vehicle (approx 40 mph)

/* from http://www.new-cars.com/2003/chevrolet/chevy-tahoe-specs.html
    Exterior Dimensions  	2003 Chevy Tahoe
    Wheelbase (in / mm) 	116 / 2946
    Overall length (in / mm) 	198.8 / 5050
    Overall width (in / mm) 	78.8 / 2002
    Overall height (in / mm) 	2WD 	74.8 / 1899 4WD 	76.7 / 1948
**  Track (in / mm) 	Front 	65 / 1651 Rear 	66 / 1676
    Minimum ground clearance (in / mm) 	8.4 / 213.4
    Ground to top of load floor (in / mm) 	2WD 	30 / 762 4WD 	31.5 / 800
    Approach angle 	19.8\x{00B0}
    Departure angle 	27.3\x{00B0}
    Curb weight (lbs / kg) 	2WD 	4828 / 2190 4WD 	5050 / 2291
    Weight distribution (% front / rear) 	2WD 	51 / 49 4WD 	53 / 47
*/

#endif
