
README For DynamicFeasibilityEvaluator Directory

*********************************************************************
HISTORY
1/16/04 - Luis Goncalves - Initial Document




*********************************************************************

This directory contains the code to evaluate the dynamic feasibility
of a trajectory (an arc).  

To date, only lateral rollover safety is taken into account in evaluating dynamic
feasibility. Other things to consider may be:
    - full 3-D model of rollover, instead of just 2-D lateral rollover
    - effect of bumps and suspension on rollover
    - terrain bumpiness (in determining a safe max velocity)
    - sliding and skidding

A discussion of the DFE problem, assumptions, solution, and future work
can be found in the doc/ subdirectory.


*********************************************************************

Description of files:

    DFE.cc                          -   main file computing DFE votes for
                                        Arbiter
                                        *** NOTE ***
                                        This file resides in 
                                        team/RaceModules/DFEPlanner
                                        *** NOTE ****
        

    DynamicFeasibilityEvaluator.m   -   defunct attempt of matlab simulator
                                        can/should be removed

    Vehicle_dynamics_control_for_rollover_avoidance.pdf
                                    -   an excellent document describing the
                                        calculation of the rollover coefficient
                                
    doc/                            -   holds html documentation of DFE 
                                        solution, assumptions, future work

    dynamic_feasibility_evaluator.cc    - defunct DFE computation
    dynamic_feasibility_evaluator.h

    rollover_functions.cc           -   all functions to compute rollover, 
                                        called by main DFE function
    rollover_functions.h

    simple_rollover.cc              -   simple DFE function for testing
    simple_rollover.h

    test_rollover_functions.cc      -   unit test for rollover_functions
    test_simple_rollover.cc         -   unit test for simple_rollover

    tune_rollover.m                 -   MATLAB code that performs same
                                        computation as main DFE.cc function
                                        can be used for debug, and 
                                        tuning of constants

    vehicle_constants.h             -   vehicle geometry and (ESTIMATED)
                                        center of gravity

    yaw_pitch_roll_2_matrix.cc      -   convert Euler coords to Rotation matrix
    yaw_pitch_roll_2_matrix.h
    yaw_pitch_roll_2_matrix.m       -   MATLAB version of same function

