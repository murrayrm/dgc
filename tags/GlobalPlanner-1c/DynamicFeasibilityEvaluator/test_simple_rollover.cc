
#include "yaw_pitch_roll_2_matrix.h"
#include "simple_rollover.h"
#include <math.h>
#include <stdio.h>

#define EPS 1e-8

#define g 9.8 // m/s^2, acceleration due to gravity

int main(int argc, const char *argv[])
{

    double m = 1000.0; // [kg], mass of vehicle

    double H = 1.0; // [m], height of center of mass bottom of vehicle (terrain level)
    double L = 1.0; // [m], distance from left wheel to projection of C.of.M. onto real axle
    double R = 1.0; // [m], distance from right wheel to projection of C.of.M. onto real axle

    double a_T = 0; // [m/s^2], total resultant acceleration of vehcile;
                    // the centripetal force when the vehicle follows the arc trajectory

    double g_z = g; // [m/s^2], downward (in vehicle ref frame) component of gravity
    double g_y = 0;   // [m/s^2], lateral (in vehicle ref frame) component of gravity

    // the outputs to check
    double c_roll;
    double F_L_z;
    double F_R_z;

    bool all_ok = true;
    double c_roll_correct;
    
    // apply some simple cases where the answer is known

    fprintf(stdout,"\nTesting moving straight ahead:  ");
    c_roll_correct = 0.0;
    simple_rollover( H, L, R, m, a_T, g_z, g_y, &c_roll, &F_L_z, &F_R_z);
    if (fabs(c_roll - c_roll_correct) > EPS)
    {
        fprintf(stdout,"Failed, c_roll = %f instead of %f\n", c_roll, c_roll_correct);
        all_ok = false;
    }
    else
    {
        fprintf(stdout,"OK\n");
    }

    fprintf(stdout,"\nTesting about to rollover leftwards:  ");
    c_roll_correct = -1.0;
    a_T = g;
    simple_rollover( H, L, R, m, a_T, g_z, g_y, &c_roll, &F_L_z, &F_R_z);
    if (fabs(c_roll - c_roll_correct) > EPS)
    {
        fprintf(stdout,"Failed, c_roll = %f instead of %f\n", c_roll, c_roll_correct);
        all_ok = false;
    }
    else
    {
        fprintf(stdout,"OK\n");
    }
    
    fprintf(stdout,"\nTesting about to rollover rightwards:  ");
    c_roll_correct = 1.0;
    a_T = -g;
    simple_rollover( H, L, R, m, a_T, g_z, g_y, &c_roll, &F_L_z, &F_R_z);
    if (fabs(c_roll - c_roll_correct) > EPS)
    {
        fprintf(stdout,"Failed, c_roll = %f instead of %f\n", c_roll, c_roll_correct);
        all_ok = false;
    }
    else
    {
        fprintf(stdout,"OK\n");
    }
    
    // rightward embankment zero rollover
    fprintf(stdout,"\nTesting zero rollover on a 45 degree embankment (rightward turning):  ");
    
    double angle = 45.0/180.0*M_PI;
    c_roll_correct = 0.0;
    g_z = cos(angle) * g;
    g_y = sin(angle) * g;
    a_T = g_y;

    simple_rollover( H, L, R, m, a_T, g_z, g_y, &c_roll, &F_L_z, &F_R_z);
    if (fabs(c_roll - c_roll_correct) > EPS)
    {
        fprintf(stdout,"Failed, c_roll = %f instead of %f\n", c_roll, c_roll_correct);
        all_ok = false;
    }
    else
    {
        fprintf(stdout,"OK\n");
    }


    // rightward embankment rightward rollover
    fprintf(stdout,"\nTesting about to rollover rightward on a 45 degree embankment (rightward turning):  ");
    
    angle = 45.0/180.0*M_PI;
    c_roll_correct = 1.0;
    g_z = cos(angle) * g;
    g_y = sin(angle) * g;
    a_T = 0;

    simple_rollover( H, L, R, m, a_T, g_z, g_y, &c_roll, &F_L_z, &F_R_z);
    if (fabs(c_roll - c_roll_correct) > EPS)
    {
        fprintf(stdout,"Failed, c_roll = %f instead of %f\n", c_roll, c_roll_correct);
        all_ok = false;
    }
    else
    {
        fprintf(stdout,"OK\n");
    }


    // leftward embankment zero rollover
    fprintf(stdout,"\nTesting zero rollover on a 45 degree embankment (leftward turning):  ");
    
    angle = -45.0/180.0*M_PI;
    c_roll_correct = 0.0;
    g_z = cos(angle) * g;
    g_y = sin(angle) * g;
    a_T = g_y;

    simple_rollover( H, L, R, m, a_T, g_z, g_y, &c_roll, &F_L_z, &F_R_z);
    if (fabs(c_roll - c_roll_correct) > EPS)
    {
        fprintf(stdout,"Failed, c_roll = %f instead of %f\n", c_roll, c_roll_correct);
        all_ok = false;
    }
    else
    {
        fprintf(stdout,"OK\n");
    }


    // leftward embankment leftward rollover
    fprintf(stdout,"\nTesting about to rollover leftward on a 45 degree embankment (leftward turning):  ");
    
    angle = -45.0/180.0*M_PI;
    c_roll_correct = -1.0;
    g_z = cos(angle) * g;
    g_y = sin(angle) * g;
    a_T = 0;

    simple_rollover( H, L, R, m, a_T, g_z, g_y, &c_roll, &F_L_z, &F_R_z);
    if (fabs(c_roll - c_roll_correct) > EPS)
    {
        fprintf(stdout,"Failed, c_roll = %f instead of %f\n", c_roll, c_roll_correct);
        all_ok = false;
    }
    else
    {
        fprintf(stdout,"OK\n");
    }


    
    // low center of mass
    fprintf(stdout,"\nTesting moving straight ahead with low center of mass:  ");
    H = 0.1; // 1/10th of distance to wheels, so need 10*g to rollover
    c_roll_correct = 0.0;
    g_z = g;
    g_y = 0;
    a_T = 0;
    simple_rollover( H, L, R, m, a_T, g_z, g_y, &c_roll, &F_L_z, &F_R_z);
    if (fabs(c_roll - c_roll_correct) > EPS)
    {
        fprintf(stdout,"Failed, c_roll = %f instead of %f\n", c_roll, c_roll_correct);
        all_ok = false;
    }
    else
    {
        fprintf(stdout,"OK\n");
    }

    fprintf(stdout,"\nTesting about to rollover leftwards with low center of mass:  ");
    H = 0.1; // 1/10th of distance to wheels, so need 10*g to rollover
    c_roll_correct = -1.0;
    a_T = 10*g;

    simple_rollover( H, L, R, m, a_T, g_z, g_y, &c_roll, &F_L_z, &F_R_z);
    if (fabs(c_roll - c_roll_correct) > EPS)
    {
        fprintf(stdout,"Failed, c_roll = %f instead of %f\n", c_roll, c_roll_correct);
        all_ok = false;
    }
    else
    {
        fprintf(stdout,"OK\n");
    }
    
    fprintf(stdout,"\nTesting about to rollover rightwards with low center of mass:  ");
    H = 0.1; // 1/10th of distance to wheels, so need 10*g to rollover
    c_roll_correct = 1.0;
    a_T = -10*g;
    simple_rollover( H, L, R, m, a_T, g_z, g_y, &c_roll, &F_L_z, &F_R_z);
    if (fabs(c_roll - c_roll_correct) > EPS)
    {
        fprintf(stdout,"Failed, c_roll = %f instead of %f\n", c_roll, c_roll_correct);
        all_ok = false;
    }
    else
    {
        fprintf(stdout,"OK\n");
    }
    

    // Final Answer ?
    if (all_ok)
    {
        fprintf(stdout, "All checks OK !!!\n");
    }
    else
    {
        fprintf(stdout, "*** Some checks have failed !!! ***\n");
    }

    return 0;
}

