#ifndef __VSTARMESSAGES__
#define __VSTARMESSAGES__

// Created  2/21/04 by Ike Gremmer.
// This file contains MTA Messages received by V* 
// programs.

#include "VStarDevices.hh"
#include "VStarPackets.hh"



//----------------------------------------------------------------------
//------VManage Section-------------------------------------------------
//--------------------------VManage Section-----------------------------
//----------------------------------------------------------------------

// MTA Messages to be received by VManage
namespace VManageMessages {
  enum {
    Get_Transmission,        // query - returns Get_Transmission_Packet
    Set_Transmission,        // inMsg - sets Transmission (Don't Use Yet)
    
    Get_Ignition,            // same basic thing
    Set_Ignition, 

    Get_EStop,
    Get_OBD,

    Set_Keyboard_Pause,      // inMsg - Allows VRemote to fake an estop
    Set_Keyboard_Resume,     // inMsg - and resume from it.

    Set_Steer_Mode,
    Set_Accel_Mode,

    Get_VM_for_VD,           // query - returns VM_for_VD_Packet.

    Set_EStop_Behavior,      // inMsg.

    Get_VManageStack,        // query - gets the display stack.



    // Place-Holder.
    Num
  };
};




//----------------------------------------------------------------------
//------VDrive Section--------------------------------------------------
//--------------------------VDrive Section------------------------------
//----------------------------------------------------------------------

// MTA Messages to be received by VDrive
/*  To Be Defined later
namespace VDriveMessages {
  enum {
    

    Num
  };
};
*/

// Functions for VDrive to fill these packets
// DO NOT USE THESE FUNCTIONS TO ACCESS DATA
// THEY ARE FOR VDrive USE ONLY
Steer_Packet        Fill_Steer_Packet();
Throttle_Packet     Fill_Throttle_Packet();
Brake_Packet        Fill_Brake_Packet();







//----------------------------------------------------------------------
//------VRemote Section-------------------------------------------------
//--------------------------VRemote Section-----------------------------
//----------------------------------------------------------------------

// MTA Messages to be received by VRemote
namespace VRemoteMessages {
  enum {

    Get_Joystick,

    Get_VRemoteStack,        // query - gets the display stack.



    // Place-Holder.
    Num
  };
};


// Functions for VRemote to fill these packets
// DO NOT USE THESE FUNCTIONS TO ACCESS DATA
// THEY ARE FOR VRemote USE ONLY
Transmission_Packet Fill_Transmission_Packet();
Ignition_Packet     Fill_Ignition_Packet();
EStop_Packet        Fill_EStop_Packet();
OBD_Packet          Fill_OBD_Packet();



#endif
