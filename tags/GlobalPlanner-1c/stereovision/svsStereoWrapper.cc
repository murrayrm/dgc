#include "svsStereoWrapper.hh"

//Default constructor
svsStereoWrapper::svsStereoWrapper() {
  //Set all the flags FALSE
  camerasReady = FALSE;
  imagesLoaded = FALSE;
  verboseMode = FALSE;
}

//Default destructor
svsStereoWrapper::~svsStereoWrapper() {

}


int svsStereoWrapper::init(int whichPair, char* SVSCalFilename, char* SVSParamsFilename, char* CamParamsFilename, bool useCameras, bool useVerbose) {
  verboseMode = useVerbose;

  if(verboseMode) printf("Starting initialization...\n");

  if(useCameras) {
    //Initialize the cameras  
    if(verboseMode) printf(" - Initializing cameras...");
    if (  cameras_start(&handle,node2cam,cameras,whichPair) < 0 ) {
      fprintf( stderr, "Error starting cameras\n" );
      if(verboseMode) printf("FAILURE!!!\n");
      camerasReady = FALSE;
      exit(1);
    } else {
      if(verboseMode) printf("SUCCESS!!!\n");
      camerasReady = TRUE;
    }
  } else {
    node2cam[LEFT_CAM] = 0;
    node2cam[RIGHT_CAM] = 1;
  }

  if(verboseMode) printf(" - Creating SVS vars...\n");
  //Initialize the SVS variables
  videoObject = new svsStoredImages();
  processObject = new svsStereoProcess();

  if(verboseMode) printf(" - Loading SVS calibration data...\n");
  //Load the internal calibration data
  FILE *svsCalFile = NULL;
  svsCalFile = fopen(SVSCalFilename, "r");
  if(svsCalFile != NULL) {
    fclose(svsCalFile);
    videoObject->ReadParams(SVSCalFilename);
    videoObject->Start();
  } else {
    if(verboseMode) printf("Error finding file %s\n", SVSCalFilename);
    return svsSW_NO_FILE;
  }

  //Load the SVS parameter data
  //Variables for getting the SVS parameters
  int corrsize, thresh, ndisp, offx, lr_temp;
  bool lr;
  //Variables for resizing the image
  int width, height;
  //Variables for using a sub-window of the resized image
  int sub_window_x_offset, sub_window_y_offset, sub_window_width, sub_window_height, use_sub_window_temp;
  bool use_sub_window;
  FILE *svsParamsFile = NULL;

  svsParamsFile = fopen(SVSParamsFilename, "r");

  if(svsParamsFile != NULL) {
    fscanf(svsParamsFile, "corrsize: %d\nthresh: %d\nndisp: %d\noffx: %d\nlr: %d\nwidth: %d\nheight: %d\nminpoints: %d\nuse_sub_window: %d\nsub_window_x_offset: %d\nsub_window_y_offset: %d\nsub_window_width: %d\nsub_window_height: %d", &corrsize, &thresh, &ndisp, &offx, &lr_temp, &width, &height, &numMinPoints, &use_sub_window_temp, &sub_window_x_offset, &sub_window_y_offset, &sub_window_width, &sub_window_height);

    if(lr_temp == 1) {
      lr = TRUE;
    } else {
      lr = FALSE;
    }
    
    if(use_sub_window_temp == 1) {
      use_sub_window = TRUE;
    } else {
      use_sub_window = FALSE;
    }

    videoObject->Load(0, 0, NULL, NULL, NULL, NULL, FALSE, FALSE);
    imageObject = videoObject->GetImage(1);
    
    imageObject->dp.corrsize = corrsize;
    imageObject->dp.thresh = thresh;
    imageObject->dp.ndisp = (int)ndisp;
    imageObject->dp.offx = offx;
    imageObject->dp.lr = lr;

    correctedSize = cvSize(width, height);

    subWindow = cvRect(sub_window_x_offset, sub_window_y_offset, sub_window_width, sub_window_height);

    fclose(svsParamsFile);
  } else {
    if(verboseMode) printf("Error finding file %s\n", SVSParamsFilename);
    return svsSW_NO_FILE;
  }

  if(verboseMode) printf(" - Initializing the transformation vars...\n");
  //Initialize the frame variable
  double cameraRoll, cameraPitch, cameraYaw;
  XYZcoord cameraOffset(0, 0, 0);
  FILE *camParamsFile = NULL;

  camParamsFile = fopen(CamParamsFilename, "r");

  if(camParamsFile != NULL) {
    fscanf(camParamsFile, "x: %lf\ny: %lf\nz: %lf\npitch: %lf\nroll: %lf\nyaw: %lf",
	   &cameraOffset.x, &cameraOffset.y, &cameraOffset.z,
	   &cameraPitch, &cameraRoll, &cameraYaw);
    
    cameraFrame.initFrames(cameraOffset, cameraPitch, cameraRoll, cameraYaw);
    
    fclose(camParamsFile);
  } else {
    if(verboseMode) printf("Error finding file %s\n", CamParamsFilename);
    return svsSW_NO_FILE;
  }


  if(verboseMode) printf(" - Initializing OpenCV vars...\n");
  for(int i=0; i < MAX_CAMERAS; i++) {
    stereoImages[i] = cvCreateImage(correctedSize, IPL_DEPTH_8U, 1);
    if(useCameras) {
      imageSize = cvSize(cameras[i].frame_width, cameras[i].frame_height);
      tempImages[i] = cvCreateImage(imageSize, IPL_DEPTH_8U, 1);
    } 
  }

  if(verboseMode) printf("Initialization complete\n");
  return svsSW_OK;
}

int svsStereoWrapper::snapCameraPair() {
  //if(verboseMode) printf("Grabbing pair from cameras...\n");
  Timeval start, end, result;

  start = TVNow();  

  if(camerasReady) {
    if (cameras_snap(handle,cameras)<0) {
      fprintf( stderr, "Unable to capture frames\n" );
      cameras_stop( handle,cameras );
      exit(1);
    }

  end = TVNow();
  result = end-start;

  //printf("Took %lf\n", result.dbl()*1000);

    return svsSW_OK;
  }


  return svsSW_NO_CAMERAS;
}

int svsStereoWrapper::loadCameraPair(VState_GetStateMsg currentState) {
  Timeval start, end, result;

  start = TVNow();

  for(int i=0; i < MAX_CAMERAS; i++) {
    if(verboseMode) printf(" - Setting data for camera %d\n", i);
    cvSetData(tempImages[i], (char *)cameras[i].capture_buffer, cameras[i].frame_width);
    cvResize(tempImages[i], stereoImages[i], CV_INTER_LINEAR);
  }

  if(verboseMode) printf(" - Loading images into videoObject...\n");
  
  imagesLoaded = videoObject->Load(stereoImages[0]->width, stereoImages[0]->height,
				   (unsigned char*) stereoImages[node2cam[LEFT_CAM]]->imageData,
				   (unsigned char*) stereoImages[node2cam[RIGHT_CAM]]->imageData,
				   NULL, NULL,
				   FALSE, FALSE);
    
  SS = currentState;
  cameraFrame.updateState(currentState);
  
  if(verboseMode) printf("Grab complete\n");

  end = TVNow();

  result = end-start;

  return svsSW_OK;
}

int svsStereoWrapper::grabCameraPair(VState_GetStateMsg currentState) {
  double msSnap=0, msSet=0, msResize=0, msState=0, msTotal=0;
  Timeval start, end, startTotal, endTotal, result;

  startTotal = TVNow();
  start = TVNow();

  if(verboseMode) printf("Grabbing pair from cameras...\n");

  if(camerasReady) {
    if (cameras_snap(handle,cameras)<0) {
      fprintf( stderr, "Unable to capture frames\n" );
      cameras_stop( handle,cameras );
      exit(1);
    }
    if(verboseMode) printf(" - Frames grabbed from cameras...\n");    

  end = TVNow();
  result = end - start;
  msSnap = (((double)result.sec())/1000000 + (double)result.usec())/1000;
  start = TVNow();

    for(int i=0; i < MAX_CAMERAS; i++) {
      if(verboseMode) printf(" - Setting data for camera %d\n", i);
      cvSetData(tempImages[i], (char *)cameras[i].capture_buffer, cameras[i].frame_width);
      //cvResetImageROI(stereoImages[i]);
      cvResize(tempImages[i], stereoImages[i], CV_INTER_LINEAR);
      //cvSetImageROI(stereoImages[i], subWindow);
    }

  end = TVNow();
  result = end - start;
  msSet = (((double)result.sec())/1000000 + (double)result.usec())/1000;
  start = TVNow();

  int offset = (subWindow.y*stereoImages[0]->width) + subWindow.x;

    if(verboseMode) printf(" - Loading images into videoObject...\n");
    
    imagesLoaded = videoObject->Load(stereoImages[0]->width, stereoImages[0]->height,
				     (unsigned char*) stereoImages[node2cam[LEFT_CAM]]->imageData,
				     (unsigned char*) stereoImages[node2cam[RIGHT_CAM]]->imageData,
				     /*
    imagesLoaded = videoObject->Load(subWindow.width, subWindow.height,
				     (unsigned char*) (stereoImages[node2cam[LEFT_CAM]]->imageData + offset),
				     (unsigned char*) (stereoImages[node2cam[RIGHT_CAM]]->imageData + offset),
    */
       				     NULL, NULL,
				     FALSE, FALSE);
    
  end = TVNow();
  result = end - start;
  msResize = (((double)result.sec())/1000000 + (double)result.usec())/1000;
  start = TVNow();

    SS = currentState;
    cameraFrame.updateState(currentState);

    if(verboseMode) printf("Grab complete\n");

  end = TVNow();
  result = end - start;
  msState = (((double)result.sec())/1000000 + (double)result.usec())/1000;

  endTotal = TVNow();
  result = endTotal - startTotal;
  msTotal = (((double)result.sec())/1000000 + (double)result.usec())/1000;

  printf("Snap %.3lf, Set %.3lf, Resize %.3lf, State %.3lf, Total %.3lf\n",
  	 msSnap, msSet, msResize, msState, msTotal);

    return svsSW_OK;
  } else {
    return svsSW_NO_CAMERAS;
  }

  return svsSW_UNKNOWN_ERROR;
}


int svsStereoWrapper::grabFilePair(char* leftFilename, char* rightFilename, char* logFilename, int lineNum) {
  FILE *logFile=NULL, *leftTestFile=NULL, *rightTestFile=NULL;
  char c;
  double doubleTime;
  int sec, usec;

  if(verboseMode) printf("Grabbing pair (%s, %s, %s) from file...\n", leftFilename, rightFilename, logFilename);
  
  leftTestFile = fopen(leftFilename, "r");
  rightTestFile = fopen(rightFilename, "r");
  if(leftTestFile != NULL && rightTestFile != NULL) {
    fclose(leftTestFile);
    fclose(rightTestFile);

    stereoImages[node2cam[LEFT_CAM]] = cvLoadImage(leftFilename, -1);
    stereoImages[node2cam[RIGHT_CAM]] = cvLoadImage(rightFilename, -1);

    if(stereoImages[node2cam[LEFT_CAM]] == NULL || stereoImages[node2cam[RIGHT_CAM]] == NULL) {
      if(verboseMode) printf("Error loading %s, %s into OpenCV object!\n", leftFilename, rightFilename);
      return svsSW_NO_FILE;
    }
  } else {
    if(verboseMode) printf("Error finding %s or %s\n", leftFilename, rightFilename);
    return svsSW_NO_FILE;
  }

  if(verboseMode) printf(" - Loaded images (%s, %s) from file...\n", leftFilename, rightFilename);

  logFile = fopen(logFilename, "r");

  if(logFile != NULL) {
    if(verboseMode) printf(" - Finding line %d in %s...\n", lineNum, logFilename);
    for(int i=0; i<lineNum; i++) {
      c = 'a';
      while(c != EOF && c != '\n') {
	c = fgetc(logFile);
      }
    }
    
    fscanf(logFile, "%lf %lf %lf %f %f %f %f %f %lf %f %f %f %f %f %f\n",
	   &doubleTime,
	   &SS.Easting, &SS.Northing, &SS.Altitude, 
	   &SS.Vel_E, &SS.Vel_N, &SS.Vel_U, 
	   &SS.Speed, &SS.Accel, 
	   &SS.Pitch, &SS.Roll, &SS.Yaw, 
	   &SS.PitchRate, &SS.RollRate, &SS.YawRate );
    
    sec = (int)doubleTime;
    usec = (int)((doubleTime - floor(doubleTime)) * 1000000);
    Timeval timevalTime(sec, usec);
    SS.Timestamp = timevalTime;
    
    fclose(logFile);
  } else {
    return svsSW_NO_FILE;
  }

  if(verboseMode) printf(" - Loading images into videoObject...\n");

  imagesLoaded = videoObject->Load(stereoImages[0]->width, stereoImages[0]->height,
				   (unsigned char*) stereoImages[node2cam[LEFT_CAM]]->imageData,
				   (unsigned char*) stereoImages[node2cam[RIGHT_CAM]]->imageData,
  /*
  int offset = (subWindow.y*stereoImages[0]->width) + subWindow.x;
    imagesLoaded = videoObject->Load(subWindow.width, subWindow.height,
				     (unsigned char*) (stereoImages[node2cam[LEFT_CAM]]->imageData + offset),
				     (unsigned char*) (stereoImages[node2cam[RIGHT_CAM]]->imageData + offset),
  */
       				     NULL, NULL,
				     FALSE, FALSE);

  cameraFrame.updateState(SS);

  if(verboseMode) printf("Grabbed complete\n");

  return svsSW_OK;
}


int svsStereoWrapper::saveStereoPair(char* leftFilename, char* rightFilename) {
  if(imagesLoaded) {
    cvSaveImage(leftFilename, stereoImages[node2cam[LEFT_CAM]]);
    cvSaveImage(rightFilename, stereoImages[node2cam[RIGHT_CAM]]);
    if(verboseMode) printf("Saved pair (%s, %s)\n", leftFilename, rightFilename);
  } else {
    return svsSW_NO_IMAGES;
  }

  return svsSW_OK;
}


int svsStereoWrapper::saveDispImage(char* dispFilename) {
  if(verboseMode) printf("Saving disparity image %s\n", dispFilename);  
  if(imageObject->haveDisparity) {
    imageObject->SaveDisparity(dispFilename, TRUE);
  } else {
    return svsSW_NO_DISP;
  }

  return svsSW_OK;
}


int svsStereoWrapper::processPair() {
  double msRect=0, msDisp=0, msTotal=0;
  Timeval start, end, startTotal, endTotal, result;

  startTotal = TVNow();
  start = TVNow();

  if(verboseMode) printf("Processing pair...\n");

  if(imagesLoaded) {
    if(verboseMode) printf(" - Images OK...\n");
    imageObject = videoObject->GetImage(1);
    if(videoObject->IsRect()) {
      if(verboseMode) printf(" - Rect OK...\n");

      end = TVNow();
      result = end - start;
      msRect = (((double)result.sec())/1000000 + (double)result.usec())/1000;
      start = TVNow();
      
      processObject->CalcStereo(imageObject);
      if(imageObject->haveDisparity) {
	if(verboseMode) printf(" - Disp OK...\n");

	if(verboseMode) printf("Processing finished\n");

	end = TVNow();
	result = end - start;
	msDisp = (((double)result.sec())/1000000 + (double)result.usec())/1000;
	endTotal = TVNow();
	result = endTotal - startTotal;
	msTotal = (((double)result.sec())/1000000 + (double)result.usec())/1000;
	
	//printf("Rect %.3lf, Disp %.3lf, Total %.3lf\n",
	//       msRect, msDisp, msTotal);

	return svsSW_OK;
      } else {
	//CalcStereo failed
	return svsSW_NO_DISP;
      }
    } else {
      //Rectification failed
      return svsSW_NO_RECT;
    }
  } else {
    //Images aren't loaded
    return svsSW_NO_IMAGES;
  }

  return svsSW_UNKNOWN_ERROR;
}


int svsStereoWrapper::numPoints() {
  return imageObject->numPoints;
}


bool svsStereoWrapper::validPoint(int i) {
  if(i < numPoints() && i >= 0) {
    return imageObject->V[i];
  } else {
    return FALSE;
  }
}


XYZcoord svsStereoWrapper::UTMPoint(int i) {
  XYZcoord retVal(0, 0, 0);
  XYZcoord cameraPoint;

  if(validPoint(i)) {
    //According to the SVS documentation:
    //The camera's Z-axis is forward - so our X-axis
    cameraPoint.x = imageObject->Z[i]/1000;
    //The camera's X axis is right - so our Y-axis
    cameraPoint.y = imageObject->X[i]/1000;
    //The camera's Y-axis is down - so our Z-axis
    cameraPoint.z = imageObject->Y[i]/1000;
    retVal = cameraFrame.transformS2N(cameraPoint);
  }

  return retVal;
}


XYZcoord svsStereoWrapper::Point(int i) {
  XYZcoord retVal(0, 0, 0);

  if(validPoint(i)) {
    retVal.x = imageObject->X[i];
    retVal.y = imageObject->Y[i];
    retVal.z = imageObject->Z[i];
  }

  return retVal;
}


bool svsStereoWrapper::SinglePoint(int x, int y, XYZcoord *resultPoint) {
  XYZcoord tempPoint(0, 0, 0);
  XYZcoord transformedPoint(0, 0, 0);

  if(processObject->CalcPoint3D(x, y, imageObject, &tempPoint.y, &tempPoint.z, &tempPoint.x)) {
    tempPoint.x = tempPoint.x/1000;
    tempPoint.y = tempPoint.y/1000;
    tempPoint.z = tempPoint.z/1000;
    
    transformedPoint = cameraFrame.transformS2N(tempPoint);

    (resultPoint->x) = transformedPoint.x;
    (resultPoint->y) = transformedPoint.y;
    (resultPoint->z) = transformedPoint.z;

    return true;
  } 

  return false;
}


bool svsStereoWrapper::SingleRelativePoint(int x, int y, XYZcoord *resultPoint) {
  XYZcoord tempPoint(0, 0, 0);
  XYZcoord transformedPoint(0, 0, 0);

  if(processObject->CalcPoint3D(x, y, imageObject, &tempPoint.y, &tempPoint.z, &tempPoint.x)) {
    tempPoint.x = tempPoint.x/1000;
    tempPoint.y = tempPoint.y/1000;
    tempPoint.z = tempPoint.z/1000;
    
    transformedPoint = cameraFrame.transformS2B(tempPoint);

    (resultPoint->x) = transformedPoint.x;
    (resultPoint->y) = transformedPoint.y;
    (resultPoint->z) = transformedPoint.z;

    return true;
  } 

  return false;
}


VState_GetStateMsg svsStereoWrapper::getCurrentState() {
  return SS;
}
