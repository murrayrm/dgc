/***************************************************************************************/
//FILE: 	main.hh
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file is responsible to creates all the threads necessary to test
//		the cruise control, the heading control and other control laws without
//		having to put bob on the road.
//
/***************************************************************************************/

#ifndef MAIN_HH
#define MAIN_HH

#include <iostream>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "bob.h"
#include "cruise.h"
#include "steering.h"

#define TRUE 1
#define FALSE 0
#define MASS 2000
#define DRAG 63
#define GPS_DELTAT 0.11
#define ENGINE_LAG 0.2
#define TIRE_RADIUS 0.4216

struct DATUM {

  // Data from the model
  int shutdown_flag;
  
  boost::recursive_mutex bobLock;
  bobDataWrapper bobDW;

  boost::recursive_mutex cruiseLock;
  cruiseDataWrapper cruiseDW;

  boost::recursive_mutex steeringLock;
  steeringDataWrapper steeringDW;
};

void GrabBobLoop(DATUM &d);

void GrabCruiseLoop(DATUM &d);

void GrabSteeringLoop(DATUM &d);

#endif
