clear all;

%Initializing the steering model
m = 2000;		%mass
b = 63;		%drag parameter
lag = 0.2;		%engine lag parameter

tau = 500;		%%applied torque
l = 2;			%width of the front chassis

phi0 = 0;		%the initial position of the steering wheel
phi_min = -pi/4;	%steering limits
phi_max = pi/4;		%steering limits
phi_desired = 0.5236;	%desired steering angle position

theta0 = 0;		%initial heading of the chassis
sim('steering');
figure(2);
subplot(3,2,1:2), plot(time,v);
title('Speed');
ylabel('v');

subplot(3,2,3), plot(time,theta);
title('heading');
ylabel('\theta');

subplot(3,2,4), plot(x,y);
title('Planar view (x _|_ y)');
ylabel('y');
axis([-8 8 -8 8]);

subplot(3,2,5), plot(time,x);
title('x position');
ylabel('x');
xlabel('t');

subplot(3,2,6), plot(time,y);
title('y position');
ylabel('y');
xlabel('t');
