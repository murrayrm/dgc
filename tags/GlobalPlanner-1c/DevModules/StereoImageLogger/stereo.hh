#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define LEFT_FILE_NAME_PREFIX_DEFAULT    "Default"
#define RIGHT_FILE_NAME_PREFIX_DEFAULT   "Default"
#define LEFT_FILE_NAME_SUFFIX_DEFAULT    "-L"
#define RIGHT_FILE_NAME_SUFFIX_DEFAULT   "-R"
#define FILE_TYPE_DEFAULT "pgm"
#define MAX_FRAMES_DEFAULT 10
