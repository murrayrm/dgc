/* Voter.cc
   Author: J.D. Salazar
   Created: 9-16-03

   A sample class derived from Voter which generates weights given to steering \
   arcs randomly. */

#include "RandomVoter.h"

// Constructor
RandomVoter::RandomVoter(unsigned int numberOfArcs, unsigned int maxVote,
                         double leftPhi, double rightPhi)
  : Voter(numberOfArcs, maxVote, leftPhi, rightPhi)
{}

// The guts of any Voter class.  'getVotes' must figure out the vote values for\
 each voting arc
 // and put them in the votes_ structure which it then returns.  (In the future \
it may be necessary
 // to create a seperate computeVotes() function...).  'getVotes' must also chan\
ge the timestamp
 // to the data that the votes computed are being based on.  In RandomVoter, the\
 votes are given
   // random weights.
   RandomVoter::voteListType RandomVoter::getVotes()
{
  for(voteListType::iterator iter = votes_.begin();
      iter != votes_.end();
      ++iter)
    {
      //      (*iter).goodness = rnd( getMaxVote() );
    }

  time_ = 0;

  return votes_;
}
