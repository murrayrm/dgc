
#ifndef MTAStruct_HH
#define MTAStruct_HH

#include "MTA/Misc/Mail/Mail.hh"

struct MTAStruct {
  float theta;
  int nothing;
};

Mail& operator << (Mail& ml, const MTAStruct& mta);
Mail& operator >> (Mail& ml, MTAStruct& mta);

#endif
