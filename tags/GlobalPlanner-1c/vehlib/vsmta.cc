 /*
 * vsmta.cc - MTA interface for vstate
 * 
 * RMM, 1 Jan 04
 *
 * This file contains all of the MTA interface routines required for 
 * vstate.  For right now, this is a just a copy of Ike's test example,
 * to see if I can get everything to compile and run.
 */

#include <time.h>
#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>

#include "MTA/Kernel.hh"
#include "MTA/Misc/Time/Time.hh"

#include "sparrow/dbglib.h"
#include "vsmta.hh"
#include "gps.h"

extern struct imu_data imudata;
extern gpsDataWrapper gpsdata;
extern VState_GetStateMsg vehstate;

int mta_counter = 0;

using namespace std;

VState::VState() : DGC_MODULE(MODULES::VState, "Vehicle State", 0) {}
VState::~VState() {}

void VState::Active() {
  D.MyCounter=0;
  mta_counter = 1;

  while( ContinueInState() ) {  // while stay active
    D.MyCounter ++;
    dbg_info("VState active; count = %d", D.MyCounter);
    sleep(2);
  }
} 

Mail VState::QueryMailHandler(Mail& msg) {
  Mail reply;			// allocate space for the reply

  // figure out what we are responding to
  switch(msg.MsgType()) {  
  case VStateMessages::GetState:  
    reply = ReplyToQuery(msg);
    {
      struct VState_GetStateMsg ms;

      /* Timestamp the data; need to link to MTA */
      // gettimeofday(&ms.Timestamp, NULL);

      /* Copy over the data from our local memory space */
      ms = vehstate;
      ++mta_counter;

#     ifdef UNUSED
      /* TBD: implement process locks to prevent partial data transfers */
      ms.Northing = 1;
      ms.Easting = 2;
      ms.Altitude = 3;

      ms.Speed = 0;

      ms.Vel_N = gpsdata.data.vel_n;
      ms.Vel_E = gpsdata.data.vel_e;
      ms.Vel_U = gpsdata.data.vel_u;
      ms.Pitch = 0;
      ms.Roll = 0;
      ms.Yaw = 0;

      ms.PitchRate = 0;
      ms.RollRate = 0;
      ms.YawRate = 0;

      /* Raw data */
      ms.gpsdata = gpsdata.data;
      ms.imudata = imudata;
#endif
      reply.QueueTo(&ms, sizeof(ms));
    }
    break;

  default:
    // let the parent mail handler handle it.
    reply = DGC_MODULE::QueryMailHandler(msg);
    break;
  }

  return reply;
}


