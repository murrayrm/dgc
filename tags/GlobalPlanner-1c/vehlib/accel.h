#ifndef ACCEL_H
#define ACCEL_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include "parallel.h"

/* Constants determined by the minimum and maximum values the digipot can have
 *  The gas actuator has been mechanically adjusted so this represents the full
 *  range of motion of the gas pedal
 */
#define MIN_POT_VAL 0
#define MAX_POT_VAL 127

/* Constants determining which pins on the digipot correspond to which pins on 
 * the parallel port
 */
//~~  #define ACCEL_CLK PP_PIN07
//~~  #define ACCEL_UD PP_PIN08
#define ACCEL_CLK PP_PIN01
#define ACCEL_UD PP_PIN16
#define ACCEL_CS PP_PIN17

//Initialization function for the gas actuator
//It sets up the parallel port used by the gas, transmission, and ignition
int init_accel();

//Calibration function for the gas actuator
int calib_accel();

//Change the gas actuator position based on the 0-1 scale
int set_accel_abs_pos(double position);

//Change the gas actuator position based on an amount to increment or decrement on the 0-127 scale
int change_digipot_by(int pos_change);

//Get the current actuator position in the 0-1 scale
double get_accel_pos();

//Helper function which returns min_val if val<min_val, max_val if val>max_val, or otheriwse returns val
int in_range(int val, int min_val, int max_val);

#endif









