/*
 * vddisp.h - vdrive display screen for Tahoe
 *
 * RMM, 30 Dec 03
 *
 */

/* Local function declarations */
int user_quit(long);
static int user_cap_toggle(long);

extern DD_IDENT strtbl[];	/* Steering display table */
extern int steer_cmdangle;	/* Commanded steering angle (steer.c) */
extern double brk_current_abs_pos_buf, brk_current_acc_buf, brk_current_vel_buf;
extern double brk_current_brake_pot, brk_pot_voltage;
extern int brk_case;


%%
%TITLE		        J: %joyc  S: %strc  A: %accc  M: %mtac
Mode: D=%md S=%ms	T: %thr   B: %brk   S: %str   X: %trx

         Off    Ref   Act   Gain   Cmd	      | Joy:  %jx  %jy    %jbut
Drive   %aref  %vref %vact %dgain %gc   %bc   |   %jyraw	  %jydir
Steer   %pref        %pact %sgain %scmd       |   %pitalt	  %xypos

Axis   IMU      GPS      |  KF     Vel     | Configuration files:
 X/N   %imdx    %gpx     |%kfx     %kfvx   |  Chn config: vdrive.dev
 Y/E   %imdy    %gpy     |%kfy     %kfvy   |  Dump file:  %dumpfile
 Z/U   %imdz    %gpz     |%kfz     %kfvz   |  Brake: pos: %brkpos vel: %brkvel 
 R     %imdr    -------  |%kfr             |         cur: %brkcur cas: %brkcas
 P     %imdp    M %magn  |%kfp             |  estop: %estopval
 Y     %imdw             |%kfw             |   

                    <SPACE> emergency stop

Accel: %acce	IMU: %imue      Cruise: %crue
Brake: %brke	GPS: %gpse      %STRBT %stre

%QUIT %ACDV	%CAPTURE %DUMP

%%

# <F1> manual	    <F5> toggle capture	    <F9>  reset defaults
# <F2> remote	    <F6> dump data	    <F10> zero counters
# <F3> autonomous	    <F7> start trajectory   <ESC> control off

tblname: vddisp;
bufname: vddbuf;

label: %TITLE "vdrive 1.0e" -fg=YELLOW;

button:	%QUIT	"QUIT"	user_quit	-idname=QUITB;
button:	%ACDV	"ACTDEV" dd_usetbl_cb -userarg=acttbl;
button:	%STRBT	"Steer:" dd_usetbl_cb -userarg=strtbl;
button:	%CAPTURE "CAPTURE" user_cap_toggle;
button:	%DUMP	"DUMP" chn_capture_dump_cb -userarg=dumpfile;

# Counters that are used to make sure we are still running
short: %joyc joy_count "%5d" -ro;
short: %strc str_count "%5d" -ro;
short: %accc acc_count "%5d" -ro;
short: %mtac mta_counter "%5d" -ro;

short: %thr acc_commanded "%4d" -ro;
short: %str steer_cmdangle "%4d" -ro;
float: %vact vehstate.Speed "%5.2f" -ro;

double: %brkpos brk_current_abs_pos_buf "%5.2f" -ro;
double: %brkvel brk_current_vel_buf "%6.0f" -ro;
# double: %brkacc brk_current_acc_buf "%6.0f" -ro;
# double: %brkvol brk_pot_voltage "%5.2f" -ro;
double: %brkcur brk_current_brake_pot "%5.2f" -ro;
short: %brkcas brk_case "%d" -ro;

short: %brke brk_enabled "%1d";
short: %acce thr_enabled "%1d";
short: %stre str_enabled "%1d";
short: %crue cruise_enabled "%1d";
# short: %gpse gps_enabled "%1d";
# short: %imue imu_enabled "%1d";

short: %ms mode_steer "%c" -manager=vdrive_mode;
short: %md mode_drive "%c" -manager=vdrive_mode;
short: %crue cruise_enabled "%d";

double: %aref ref_accel "%5.2f";
double: %pref ref_steer "%5.2f";
double: %vref ref_speed "%5.2f";

double: %dgain cruise_gain "%5.2f";

double: %scmd str_angle "%5.2f";
double: %bc brk_pos "%5.2f";
double: %gc thr_pos "%5.2f";

double:	%jx		joy.data[0]		"%5.0f"		-ro;
double:	%jy		joy.data[1]		"%5.0f"		-ro;
short: %jbut		joy.buttons		"%d"		-ro;

# double: %aeps ACCEL_EPS "%5.2f";
# double: %bvel BRAKE_VEL "%5.2f";
# double: %bacc BRAKE_ACC "%5.2f";
short: %estopval estop_value "%d";

double: %gpx vehstate.gpsdata.lat "%7.4f" -ro;
double: %gpy vehstate.gpsdata.lng "%7.4f" -ro;
double: %gpz vehstate.gpsdata.altitude "%7.4f" -ro;

double: %imdx vehstate.imudata.dvx "%7.2f" -ro;
double: %imdy vehstate.imudata.dvy "%7.2f" -ro;
double: %imdz vehstate.imudata.dvz "%7.2f" -ro;
double: %imdr vehstate.imudata.dtx "%7.4f" -ro;
double: %imdp vehstate.imudata.dty "%7.4f" -ro;
double: %imdw vehstate.imudata.dtz "%7.4f" -ro;

double: %kfx vehstate.kf_lat "%7.2f";
double: %kfy vehstate.kf_lng "%7.2f";
float: %kfz vehstate.Altitude "%7.2f";
float: %kfvx vehstate.Vel_N "%5.2f";
float: %kfvy vehstate.Vel_E "%5.2f";
float: %kfvz vehstate.Vel_U "%5.2f";
float: %kfr vehstate.Roll "%7.2f";
float: %kfp vehstate.Pitch "%7.2f";
float: %kfw vehstate.Yaw "%7.2f";
float: %magn vehstate.magreading.heading "%5.2f";

string: %dumpfile dumpfile "%s";

# string: %ctrlfile ctrlfile "%s" -callback=df_load_ctrl -idname=CTRLFILE;
# string: %trajfile trajfile "%s" -callback=df_load_traj;
# string: %velfile velfile "%s" -callback=df_load_vels;
# string: %mapfile mapfile "%s" -callback=df_load_map;


