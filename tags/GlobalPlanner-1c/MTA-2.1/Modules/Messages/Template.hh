#include "../../Kernel/Misc/Mail/Mail.hh"

// change these two lines to the name of your message,
// such as Message_VelocityAndSteeringAngle_HH
#ifndef Message_Template_HH
#define Message_Template_HH


// Rename struct Template to the name of your message,
// such as struct VelocityAndSteeringAngle.

struct Template {

  // fill in the details of your struct. (get rid of these).
  int x;
  float y;
  double z;
  char array[50]; // note: a fixed size array is stored natively in
                  // the struct and therefore this does not
                  // violate the simple struct requirement
};


// Now we need to define the pipe functions to add our Template
// struct to a message and read it back from the message.

// use the form given below, changing Template to the name
// of your module.

  // to add Template struct to a message
Mail& operator << (Mail& ml, const Template& rhs);

  // to read back Template struct from a message
Mail& operator >> (Mail& ml, Template& rhs);


// End of Header file :).

#endif
