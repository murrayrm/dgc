#include "../../Kernel/Misc/Mail/Mail.hh"

// change these two lines to the name of your message,
// such as Message_VelocityAndSteeringAngle_HH

//#ifndef Message_Template_HH
//#define Message_Template_HH
#ifndef Message_VelocityAndSteeringAngle_HH
#define Message_VelocityAndSteeringAngle_HH



// Rename struct Template to the name of your message,
// such as struct VelocityAndSteeringAngle.

//struct Template {
struct VelocityAndSteeringAngle {

  // fill in the details of your struct. (get rid of these).

  // Note: i deleted the example members

  float Velocity;
  float Theta;
};


// Below I changed template to VelocityAndSteeringAngle, even
// in the comment fields.

// Now we need to define the pipe functions to add our VelocityAndSteeringAngle
// struct to a message and read it back from the message.

// use the form given below, changing Template to VelocityAndSteeringAngle.

  // to add VelocityAndSteeringAngle struct to a message
Mail& operator << (Mail& ml, const VelocityAndSteeringAngle& rhs);

  // to read back VelocityAndSteeringAngle struct from a message
Mail& operator >> (Mail& ml, VelocityAndSteeringAngle& rhs);


// End of Header file :).

#endif
