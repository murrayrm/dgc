#include "Modules.hh"
#include "Echo.hh"
#include "Kernel.hh"

#include <boost/shared_ptr.hpp>

using namespace boost;

int main() {

  Register(shared_ptr<DGC_MODULE>(new Echo() ));
  StartKernel();

  return 0;
}
