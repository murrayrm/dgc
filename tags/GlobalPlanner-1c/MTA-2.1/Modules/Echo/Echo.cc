#include "Echo.hh"

#include "Kernel.hh"
#include <time.h>
#include <iostream>

#include "Misc/Time/Timeval.hh"

using namespace std;
Timeval dispBefore, dispAfter;


Echo::Echo() : DGC_MODULE(MODULES::Echo, "Echo Server", 0) {
}
Echo::~Echo() {
}

void Echo::Init() {
  // call the default Init Function First
  DGC_MODULE::Init();

  D.EchoCount = 0;
}

void Echo::Active() {
  while( ContinueInState() ) {  // while stay active
    sleep(3);
    cout << "D.EchoCount = " << D.EchoCount << endl;
  }
} 

 
void Echo::InMailHandler(Mail& ml) {



  int size = ml.Size();
  char *p = new char[ size ];
  // download the message locally
  ml.DequeueFrom( (void*) p, size );
  Mail reply = Mail(ml.To(), ml.From(), ml.MsgType(), ml.Flags() );
  // now put it back in a new pipe
  reply.QueueTo((void*) p, size);
  delete p;
  // and send it on its way
  //  cout << "Reply time = " << TVNow() << endl;
  SendMail(reply);

  D.EchoCount++;
  //  cout << "Echoing InMail - Echo Count = " << D.EchoCount << endl;
}


Mail Echo::QueryMailHandler(Mail& ml) {
  D.EchoCount++;
  //  cout << "QueryMail Received - Echo Count =" 
  //       << D.EchoCount << endl;

  int size = ml.Size();
  char *p = new char[ size ];
  ml.DequeueFrom( (void*) p, size );
  Mail reply = Mail(ml.To(), ml.From(), ml.MsgType(), ml.Flags());
  reply.QueueTo((void*) p, size);
  delete p;
  return reply;
}

