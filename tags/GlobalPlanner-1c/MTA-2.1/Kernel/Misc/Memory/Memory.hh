#ifndef Memory_HH
#define Memory_HH

class MemoryPointer {
public:
  MemoryPointer();
  MemoryPointer(char * ptr, int sz);
  MemoryPointer(const MemoryPointer & m);
  ~MemoryPointer();

  char* slider();
  char* ptr();
  char* shr(int r);
  char* shl(int l);
  int   size();

  int used();
  int left();

  MemoryPointer& operator= (const MemoryPointer & rhs); 
  
private:
    
  void destruct();
  void insert();

  char* myPtr;
  char* slider;
  int   mySize;
};





class MemoryContainer {
public:
  MemoryContainer(int alloc_inc=1000);
  ~MemoryContainer();
  void destruct();

  int  push(char* ptr, int sz);
  int  pop(MemoryContainer& mc, int sz);

  char* ptr();
  char* slider();

  MemoryContainer& operator= (const MemoryContainer& mc);

private:

  MemoryPointer myMP;
  int myBlockSize;

  int myHead;
  

};





#endif
