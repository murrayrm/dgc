//#include "Time.hh"

#include "Misc/Time/Time.hh"
#include "Misc/Time/Timeval.hh"
#include "Kernel.hh"

#include "Clock.hh"

#include <iostream>

extern Kernel K;
extern int MTA_KERNEL_COUT;
using namespace boost;
using namespace std;


int Kernel::TimeSyncInit() {
  // declare "zero" time as beginning of program for now
  M_TimeZero = TVNow();

  //  And Try to find the clock
  SystemTimeZeroUpdate();
  return true;
}


void Kernel::TimeSyncThread() {

  while( ! ShuttingDown() ) {
    // fill in time syncronization stuff here.
    SystemTimeZeroUpdate();
    sleep_for(10);
  }
}

void Kernel::TimeSyncShutdown() {


}

void Kernel::SystemTimeZeroUpdate() {
  // find the clock
  Timeval before, after;

  // try to time sync up to 5 times.  This will exit
  // after a successful try.
  for(int i=0; i<5; i++) {
    // Prepare a query message
    MailAddress ma = FindModuleAddress(MODULES::Clock);
    Mail m( ma , ma, ClockMessages::GetTime, MailFlags::QueryMessage);

    // we need to determine how long it takes to send 
    // and receive the message - we want a delay of 
    // no more than 1 ms.

    before = TVNow();
    Mail returnMessage = SendQuery(m);
    after  = TVNow();

    if( ! returnMessage.OK() || returnMessage.Size() != sizeof(Timeval) ) {
      if( MTA_KERNEL_COUT ) cout << "No Clock Module Found" << endl;
      //   return BadTimeval();
      return;
      // if the send and receive took less than 10ms, 
      // then all is good :).
    } else if( (after - before) <= Timeval(0, 1000) ) { 
      Timeval tv;
      returnMessage.DequeueFrom( &tv, sizeof(tv));

      recursive_mutex::scoped_lock s(M_TimeZeroLock);
      M_TimeZero = before-tv;
      return;
      //      return (before - tv);
    }
  }

  if( MTA_KERNEL_COUT ) cout << "Clock Synchronize Failed 5 Times" << endl;
  //  return BadTimeval(); // failed 5 times

  // dont update clock if sync failed
}


Timeval Now() {
  Timeval x = TVNow();
  recursive_mutex::scoped_lock s(K.M_TimeZeroLock);
  return (x-K.M_TimeZero);
}
