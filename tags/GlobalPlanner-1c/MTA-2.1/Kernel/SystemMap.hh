
#ifndef __SYSTEMMAP_HH__
#define __SYSTEMMAP_HH__


#include <list>
using namespace std;

#include "Misc/Structures/IP.hh"
#include "Misc/Structures/ModuleInfo.hh"

int PortScanIP( IP_ADDRESS ip, list<ModuleInfo> & ret );


#endif
