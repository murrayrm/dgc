// 
// testLadar.cc
// 
// Date created: 12/31/03
// 
// History:
//   12/31/03 - created (lars), copied from tests/waypointnav/ladar.cc
//
using namespace std;

#include <time.h> 
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <iostream>
#include "laserdevice.h"
#include "Constants.h"

// for test display output
#define NUMROWS 20 
#define NUMCOLS 60

// THIS IS NASTY BUT IT'LL WORK FOR NOW...
// TODO: fix this nastiness
#define LADAR_NUM_SCANPOINTS 361

int SIM = 0;
int HELP = 0;
char *tty;
int TestInitLadar( int );
void TestLadarLoop( int );

double laserdata[LADAR_NUM_SCANPOINTS];

CLaserDevice laser;

/*****************************************************************************/
/*****************************************************************************/
int main( int argc, char **argv ) {
  int c;
  int digit_optind = 0;

  tty = strdup("/dev/ttyS9");
  while (1) {
    int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    static struct option long_options[] = {
      {"sim",       0, &SIM, 1},
      {"help",      0, &HELP, 1},
      {"tty",       1, 0, 2},
      {0,0,0,0}
    };

    c = getopt_long_only (argc, argv, "", long_options, &option_index);
    if(c == -1) break;

    if(c != '?') {
      if(!strcmp(long_options[option_index].name,"tty")) {
        tty = strdup(optarg);
      }
      cout << "option " << long_options[option_index].name;
      if(optarg) cout << " with arg " << optarg;
      cout << endl;
    }
  }

  cout << endl << "Argument summary:" << endl;
  cout << "  SIM: " << SIM << endl;

  if(HELP) {
    printf("\n\n  This is a unit test program for the ladar unit.\n");
    printf("  It displays graphical output of the ladar ranges.\n");
    printf("  The top of the display represents far left and the bottom is far right\n");
    printf("  Each '*' represents a scaled distance in that direction.\n");
    printf("  This program can be run in simulation mode by using -sim.\n");
    printf("\n");
    exit(0);
  }

  // Initialize the ladar unit
  int result;
  if( !SIM ) {

      result = TestInitLadar( SIM ); 
  }
  if(!result) {
    exit(-1);
  }
  printf( "Done initting ladar!\n" );

  while( 1) {
    TestLadarLoop( SIM );
  }

  // TODO: Figure out how to shut down properly (handle Ctrl-c)

  laser.LockNShutdown();  

} // end int main( int argc, char **argv )

/*****************************************************************************/
/*****************************************************************************/
int TestInitLadar(int simQ) {
  // Ladar initialization tasks
  if( !simQ ) { // use this if you truly want to simulate
    return !laser.Setup(tty,180,0.5);
  } 
  else {
    // Do simulation stuff here...
    return 1;
  }  
  
} // end int TestInitLadar(...)


/*****************************************************************************/
/*****************************************************************************/
void TestLadarLoop( int simQ ) {

  // for display stuff
  int scannum = 0; // which ladar scanpoint do we want to display?
  int slength = 0; // length of scanpoint display for each row
  
    if( !simQ ) { // use this if you truly want to simulate
      // Take a scan
      laser.UpdateScan(); // NEW WAY

      // now the scans should be set
      // i = 0 corresponds to far right scan
      // i = LADAR_NUM_SCANPOINTS-1 corresponds to far left scan

      // update local data buffer
      for(int i = 0; i < LADAR_NUM_SCANPOINTS; i++) {
	// LMS200 returns data in mm (divide by 1000.0)
	// LMS220 returns data in cm (divide by 100.0)
	laserdata[i] = (laser.Scan[i]) / 10.0;
      }
    } 
    else {
      // fake the laser scan here...
      for (int i = 0; i < LADAR_NUM_SCANPOINTS; i++) {
	laserdata[i] = ROOF_LADAR_MAX_RANGE; 
      }

      // fake an obstacle
      for (int i = 60; i < 100; i++) {
	laserdata[i] = 0.5 * ROOF_LADAR_MAX_RANGE; 
      }

    }  

    // clear the display
    system("clear");
   
    // go through each line and print graphical display of ladar scans
    for( int i = NUMROWS-1; i >= 0; i-- ) {

      // determine which scan approximates the row we're printing to
      scannum = (int)round( (double)i / (NUMROWS - 1) * LADAR_NUM_SCANPOINTS );
      // det. how many characters to display for each scanpoint
      slength = (int)round( laserdata[scannum] / ROOF_LADAR_MAX_RANGE * NUMCOLS ); 

      for( int j = 0; j < slength; j++ ) {
        printf("*");
      }
      printf("\n");
    }
    
    usleep(1000000);    // Evaluation rate

} // end TestLadarLoop(...)

// end testLadar.cc
