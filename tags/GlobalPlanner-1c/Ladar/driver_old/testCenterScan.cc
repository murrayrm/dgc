/*********************************************************************************/
/* SICK LASER DRIVER V0.9                                                        */
/* Copyright (C) 2000 - Kasper Stoy - USC Robotics Labs (See README for details) */
/*********************************************************************************/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "laserdevice.h"

int main(int argc, char *argv[]) {
  int result;
  double laserdata[361];
  CLaserDevice laserDevice("/dev/ttyS9"); /* change this */

  result = laserDevice.Setup();

  if (result==0) { // success

    /* get laser scans */
    for(int cnt=0;cnt<50;cnt++) {
      laserDevice.UpdateScan();
      
      std::cout << laserDevice.Scan[180] << "\n";
      
      //       for(int i=0;i<361;i++) {
      // 	laserdata[i] = (laser.Scan[i]) / 100.0;
      //       }
      puts("");
      usleep( 200000 );
    }
    
  }

  laserDevice.LockNShutdown();
  return(0);
}
