@c -*- texinfo -*-

@setfilename ladar.info

@node ladar,,,(Top)
@chapter LADAR Documentation

This documentation describes how a LADAR scanner works, how we are using
it on Bob, and how to compile and use the various pieces of software.

@menu
* intro: ladar/intro.		introduction to LADAR scanners	
* bobsetup: ladar/bobsetup.	How we are using LADARs with Bob
* comm: ladar/comm.		LADAR Communications
* driver: ladar/driver.		Description and usage of LADAR driver
* testcode: ladar/testcode.	Description and usage of testcode
* planner: ladar/planner.	Description and usage of LADARBumper
* mapper: ladar/mapper.		Description and usage of LADARMapper
* checklists: ladar/checklists.	Checklists to determine if everything works
* bugs: ladar/bugs.		Known bugs and workarounds
@end menu

@node ladar/intro,ladar/bobsetup,,ladar
@section Introduction

(Taken from the LMS Technical Description published by SICK)

The LMS system operates by measuring the time of flight of laser light
pulses: a pulses laser beam is emitted and reflected if it meets an object.
The reflection is registered by the scanner's receiver.  The time between
transmission and reception of the impulse is directly proportional to the
distance between the scanner and the object (time of flight).

The pulsed laser beam is deflected by an internal rotating mirror so that
a fan-shaped scan is made of the surrounding area (laser radar).  The 
contour of the target object is determined from the sequence of impulses
received.  The measurement data is available in real time for further
evaluation via a serial interface.

@node ladar/bobsetup,ladar/comm,ladar/intro,ladar
@section How Bob uses our LADAR scanners

===============================================================================
Basic implementation
===============================================================================
Bob currently has 2 LADAR scanners, one mounted on the roof and another
on the front bumper.  Both of these scanners are pointed slightly down.
This allows the scanners to see the ground some distance ahead of the
vehicle.  As the vehicle moves forward, the scans will be transformed
into the vehicle coordinate frame, and the height of the detected obstacles
will be determined.  This information will be encoded into a local map class,
which can then be used by a path planning algorithm to attempt to plot
a course through any obstacles.

===============================================================================
Transmission Rates:
===============================================================================
The LADAR can be set to a 1 degree or 0.5 degree resolution.  
When using 1 degree steps, the scanner takes 13.32 ms per scan (75 Hz) and 
sends 181 distance measurements per scan.
When using 0.5 degree steps, the scanner takes 26.64 ms per scan (37.5 Hz) and
sends 361 distance measurements per scan.

Each measurements takes 2 bytes, so in either case the scanner can transmit up
to 27150 bytes per second (plus some overhead for headers, etc)

Using RS232, we are able to achieve communication rates of up to 38,400 bits
per second, or 4800 bytes per second.  So we will be losing some of the scans.

Using RS422, we can achieve 500 kilobits per second, or 62,500 bytes per second.


TODO: Go into detail about why various mounting angles were chosen, how
far ahead of the vehicle we can see, how that relates to how quickly
Bob can stop, etc.

@node ladar/comm,ladar/driver,ladar/bobsetup,ladar
@section LADAR communications

@menu
* commintro: ladar/commintro.	Telegram description
* config: ladar/commconfig.	LMS Configuration
* errors: ladar/commerrors.	LMS error states
@end menu

@node ladar/commintro,ladar/commconfig,,ladar/comm
@subsection Communications introduction 

Communications with the LADAR consists of sending a @code{send telegram} to the
scanner.  Upon receipt of a correctly formatted @code{send telegram}, the
LADAR responds with an @code{ACK} followed by a @code{response telegram}.
If it is not correctly formatted, the scanner responds with a @code{NAK}.

SEND TELEGRAM:
=========================================================================
Name             |Position| Explanation
=================+========+==============================================
STX              |    1   | Start byte (0x02)
ADDRESS          |    2   | Subscriber address (???)
LENGTH           |   3,4  | Number of subsequent bytes EXCLUDING checksum
COMMAND/RESPONSE |    5   | Command
DATA             |   6-n  | Optional, data for command
CHECKSUM         | n+1,n+2|
=========================================================================

RESPONSE TELEGRAM:
=========================================================================
Name             |Position| Explanation
=================+========+==============================================
STX              |    1   | Start byte (0x02)
ADDRESS          |    2   | Subscriber address (send address + 0x80)
LENGTH           |   3,4  | Number of subsequent bytes EXCLUDING checksum
COMMAND/RESPONSE |    5   | Response (usually send command + 0x80)
DATA             |    6   | Status byte
CHECKSUM         | n+1,n+2|
=========================================================================

Refer to the LMS Telegram Listing.pdf file for details on each of the commands

@node ladar/commconfig,ladar/commerrors,ladar/commintro,ladar/comm
@subsection LMS Configuration

When first configuring the LMS, use the following series of commands:

1) Power On
2) Wait for "power-on" string                          
3) Send command "Switch to installation mode" (0x20)
4) Wait for ACK
5) Wait for response (0xA0)
6) Send command "Set Parameters" (0x77)           
7) Wait for ACK
8) Wait for response (0xF7)
9) Send command "Switch to monitoring mode" (0x20)
10) Wait for ACK
11) Wait for response (0xA0)

Subsequent power-on can go as follows:

1) Power On
2) Wait for "power-on" string
3) Send command "Set serial settings" ()
4) Wait for ACK
5) Wait for response ()
6) Send command "Send data"
7) Wait for ACK
8) Wait for response ()

@node ladar/commerrors,,ladar/commconfig,ladar/comm
@subsection Errors that can occur

Check the returned measurement values for the following.  If any of them are
received, then that measurement is not valid and it should be assumed that
an obstacle is present.

@itemize @bullet
@item
0x1FFF         Measure value not valid
0x1FFE         Dazzled
0x1FFD         Operation overflow
0x1FFB         Signal-to-noise too small
0x1FFA         Error reading channel 1
0x1FF7         Measured value > Maximum value
@end itemize

See page 126 of MLS Telegram Listing.pdf for a complete list of errors that can be returned.

@node ladar/driver,ladar/testcode,ladar/comm,ladar
@section Description and usage of LADAR driver

The most current LADAR driver exists in @code{team/Ladar/driver}.

Usage of the LADAR driver is quite simple.  You need to instantiate
a @code{CLaserDevice mydevice;}.  You then initialize the LADAR with
@code{result = mydevice.Setup(char *tty)} where @code{tty} is a path to
the appropriate serial device to open.  Currently on Bob, the roof
LADAR is attached to @code{/dev/ttyS8} and the bumper LADAR is on
@code{/dev/ttyS9}.

CLaserDevice methods:

@itemize @bullet
@item
Setup(char *tty)
@item
reset()
@item
setMode(int mode) - 0 on request (default)  1 continuous
@item
setBaudRate(int baud)
@item
int[] getScan()
@item
int status()
@item
int getErrors()
@item
setAngleResolution(int angle,double resolution)
@item

@end itemize


The LADAR is constantly sending scan data over the serial port to the driver.
This data will be discarded whenever new data is available.  If you want to
look at the most recent scan, calling @code{laser.UpdateScan();} will freeze
a copy of the most recent scan.  You can then read each of the scan points
with @code{laser.Scan[int i]} where @code{i} is the index of the scan point
whose value you want.

Currently you are very limited in what you can do with the driver.  It
is hardcoded to be in 180 degree mode, with 0.5 degree resolution.  So
you will have a fixed 361 scan points to read.  This and other constants
are defined in @code{team/include/LadarConstants.h}.

@itemize @bullet
@item
LADAR_NUM_SCANPOINTS	- number of scan points being recorded (currently 361)
@item
LADAR_SWEEP_ANGLE	- sweep angle in radians (currently M_PI)
@item
LADAR_ANGLE_RESOLUTION	- angular resolution of scans in radians
@end itemize

Note that a new driver is being developed which will avoid many of these 
shortcomings and allow much more flexibility in the use of the LADAR.

TODO: Add section on how to compile LADAR driver and link it with your
programs.  Include code for laserreader? as example of use.

@node ladar/testcode,ladar/planner,ladar/driver,ladar
@section Description and usage of testcode

All test code exists in the /team/Ladar directory.  Compile by doing a
@code{make} in that directory.

@menu
* laserreader: ladar/laserreader.	Description and usage of @code{laserreader}
* testcenter: ladar/testcenter.		Description and usage of @code{testcenter}
* testLadar: ladar/testLadar.		Description and usage of @code{testLadar}
@end menu

@node ladar/laserreader,ladar/testcenter,,ladar/testcode
@unnumberedsubsec Basic LADAR test program to dump scans

@example
Usage:   laserreader tty

                     tty - serial device to open
@end example

This will output some debugging information as it tried to communicate
with the LADAR, and if successful, it will dump 5 scans to the screen.

@node ladar/testcenter,ladar/testLadar,ladar/laserreader,ladar/testcode
@unnumberedsubsec LADAR test program to repeatedly output center scan

@example
Usage:     centerscan
@end example

@code{centerscan} is hardcoded to look at /dev/ttyS9

This will output the value of the center scan (looking straight ahead)
50 times.  This program is never used, which is why it has not been
updated to take a tty parameter.

@node ladar/testLadar,,ladar/testcenter,ladar/testcode
@unnumberedsubsec LADAR test program to display what LADAR "sees"

@example
Usage:  testLadar [-help] [-sim] [-tty tty]

           -help      Displays this usage summary
           -sim       Run in simulation mode.  Does not communicate with ladar
           -tty tty   Specifies which serial device to talk to
@end example

This program will display an ASCII representation of what the LADAR sees.
For example:

@example
*****************
***********
***********
*****************
*****************
*****************
@end example

This is seeing an obstacle in the way of the 2nd and 3rd scan lines.

@node ladar/planner,ladar/mapper,ladar/testcode,ladar
@section Description and usage of LADARBumper

This section will eventually be filled in, but for now it exists
partially by running LADARBumper -help or by looking at the README
file. 

Describe how to compile and use LADARBumper

@node ladar/mapper,ladar/checklists,ladar/planner,ladar
@section Description and usage of LADARMapper

Describe how to compile and use LADARMapper

@node ladar/checklists,ladar/bugs,ladar/mapper,ladar
@section Checklists used to determine if everything is working.

This will be filled in, but for now look at SaturdayQuickTest in the
RaceModules/LADARBumper directory to see what steps to perform to
verify that the LADARBumper is ready for testing.

@node ladar/bugs,,ladar/checklists,ladar
@section Known bugs and workarounds

The following are known bugs with the LADAR:
@itemize @bullet
@item
The LADAR sometimes stop sending scan lines.  This can be observed by
running LADARBumper -disp, and seeing that the sparrow display never
updates.

TEMPORARY WORKAROUND: Power cycling the LADAR fixes this.
@item
When running 2 LADAR scanners at the same time, I get faulty data.

TEMPORARY WORKAROUND: Unplug one scanner and only use the other.  This
is caused by the fact that we have not wired the 2 scanners to be
master/slave, and so one LADAR can pick up returns from the other.
@end itemize
