
#include "MTA/Clock.hh"

Timeval TimeZero;
int     ServedCount = 0;


void Clock::Active() {

  cout << "Clock Starting Active" << endl;

  TimeZero = TVNow();

  Timeval stActive = TVNow();

  while ( ContinueInState() ) { 
    
    if( TVNow() - stActive > Timeval( 1, 000)) {
      cout << "Time = " << TVNow() - TimeZero 
	   << ", Served = " << ServedCount << endl;
      stActive = TVNow();
    }

    Sleep( Timeval( 1, 0));

  }

  ForceKernelShutdown();
}

void Clock::InMailHandler(Mail & ml) {

  switch( ml.MsgType() ) {

  case ClockMessages::Reset: 
    TimeZero = TVNow();
    break;
  default:
    DGC_MODULE::InMailHandler(ml);
  }

}

Mail Clock::QueryMailHandler(Mail & ml) {
  
  Mail reply;
  Timeval curTime = TVNow() - TimeZero;

  switch( ml.MsgType() ) {    
  case ClockMessages::GetTime:
    reply = ReplyToQuery(ml);
    reply << curTime;
    ServedCount ++;
    break;
  default:
    reply = DGC_MODULE::QueryMailHandler(ml);
  }

  return reply;
}

int main(int argc, char* argv[])
{
  
  Register( shared_ptr<DGC_MODULE> ( new Clock));
  StartKernel();

  return 0;
}














