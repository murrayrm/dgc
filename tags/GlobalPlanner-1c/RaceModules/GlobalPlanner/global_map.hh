//function prototypes for global_map
#include "global_functions.hh"
//horrible hack for SIZEX, SIZEY
#define SIZEX 1629 
#define SIZEY 2395

struct pixel {
    int Y;
    int X;
};

inline bool pix_is_eq(pixel A, pixel B);

int fill_map(unsigned char map[SIZEX*SIZEY],  unsigned char color);

int draw_circle(unsigned char map[SIZEX*SIZEY], pixel center, int  r, 
    unsigned char color);

int draw_rectangle(unsigned char map[SIZEX*SIZEY], pixel q1, pixel q2, 
    pixel q3, pixel q4, unsigned char color);

pixel Vec2pix(Vector x);
