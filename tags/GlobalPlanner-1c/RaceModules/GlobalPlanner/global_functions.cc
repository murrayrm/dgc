#include <cmath>
#include <unistd.h>
#include <iostream>
#include "global_functions.hh"

using namespace std;

double calc_steer_command(Vector curPos, Vector curWay, double curHeading) 
{
  double desiredRelYaw;   // the desired Yaw (clockwise angle from forward) 
  double steerCommand; // the desired steering angle
  double way_angle;    // angle to the waypoint (measured from east)
  
  Vector way_rel;
  way_rel.N = curWay.N - curPos.N;
  way_rel.E = curWay.E - curPos.E;

  // get the angle between head and way 
  way_angle = atan2( way_rel.N, way_rel.E );

  //  cout << "way_angle = " << way_angle << endl;
  //  cout << "head_angle = " << head_angle << endl;

  // Desired heading in vehicle reference frame
  desiredRelYaw = curHeading - way_angle;

  // Normalize the desired heading to be in [-pi,pi]
  if( desiredRelYaw > M_PI ) desiredRelYaw = desiredRelYaw - 2.0*M_PI;
  if( desiredRelYaw < -M_PI ) desiredRelYaw = desiredRelYaw + 2.0*M_PI;

  // Should probably print this out
  // cout << "way_rel.N= " << way_rel.N << endl << "way_rel.E= " << way_rel.E << endl;

   cout << "Desired Yaw to target (deg in local frame) = " 
	<< desiredRelYaw*180.0/M_PI << endl;

  // Saturate the steering command to be in [-USE_MAX_STEER, USE_MAX_STEER]
  steerCommand = fmax( fmin( desiredRelYaw, USE_MAX_STEER ), -USE_MAX_STEER ); 
  return steerCommand;

} // end calc_steer_command(Vector curPos, Vector curWay, Vector curHead)


// double calc_speed_command(...)
// Return value: desired velocity at this point
// Needs a body that calculates a good velocity given state & possibly steering
double calc_speed_command( double curYaw, Vector curPos, Vector curWay, 
    Vector nextWay, double curRadius )
{  
  double speedCommand;
  double curHeading;    // heading is measured CCW from East
  double desiredRelYaw; // the desired relative Yaw (clockwise angle from forward) 
  double way_angle;     // angle to the waypoint (measured from east)
  double dist_to_wp = distVector(curPos, curWay) - curRadius;
  double anticipate_velo;
   
  //slow down if we are nearing a waypoint
  //vf^2 - v0^2 = 2 a x
  anticipate_velo = sqrt(pow(GLOBAL_FUNCTIONS_MIN_SPEED,2) + 
			 2 * GLOBAL_FUNCTIONS_MAX_ACCEL * dist_to_wp);
  // Check to see what the max speed possible is
  anticipate_velo = anticipate_velo > GLOBAL_FUNCTIONS_MAX_SPEED ? 
    GLOBAL_FUNCTIONS_MAX_SPEED : anticipate_velo;  // m/s

  Vector way_rel;
  way_rel.N = curWay.N - curPos.N;
  way_rel.E = curWay.E - curPos.E;

  // get the angle between the vehicle and waypoint locations 
  way_angle = atan2( way_rel.N, way_rel.E );

  // Current heading
  curHeading = M_PI_2 - curYaw;
  // Desired heading in vehicle reference frame
  desiredRelYaw = curHeading - way_angle;

  // Normalize the desired heading to be in [-pi,pi]
  if( desiredRelYaw > M_PI ) desiredRelYaw = desiredRelYaw - 2.0*M_PI;
  if( desiredRelYaw < -M_PI ) desiredRelYaw = desiredRelYaw + 2.0*M_PI;

  // Set the commanded speed to be big when we're going in the desired direction,
  // and small when we're not!
  double diff = fabs( desiredRelYaw / M_PI ); // normalized error in heading
  double angleErrorThreshold = 0.10; // threshold on normalized heading error, above 
                                    // which we will command MIN_VELO
  if( diff >= angleErrorThreshold ) {
    speedCommand = GLOBAL_FUNCTIONS_MIN_SPEED;
  }
  else {
    speedCommand = GLOBAL_FUNCTIONS_MAX_SPEED - 
      (GLOBAL_FUNCTIONS_MAX_SPEED - GLOBAL_FUNCTIONS_MIN_SPEED) * 
      (diff / angleErrorThreshold );
    speedCommand = (speedCommand > anticipate_velo ? anticipate_velo 
		    : speedCommand);
  }

  //  printf( "diff = %f, speedCommand = %f\n", diff, speedCommand );
  
  return speedCommand;

} // end calc_speed_command()


float distVector(Vector a, Vector b) 
{
  return sqrt((a.N-b.N)*(a.N-b.N) + (a.E-b.E)*(a.E-b.E));
}

Vector subVector(Vector a, Vector b) 
{
  a.N -= b.N; 
  a.E -= b.E;
  return (a); 
}

/* non-destructive add */
Vector addVector(Vector a, Vector b) 
{
  Vector result;
  result.N = a.N + b.N; 
  result.E = a.E + b.E;
  return result; 
}

Vector scaleVector(Vector a, float b)
{
  Vector result;
  if (distVector(a, Vector(0,0)) != 0) {
    result.N = a.N * b / distVector(a, Vector(0,0));
    result.E = a.E * b / distVector(a, Vector(0,0));
  }
  else {
    result.N = 0;
    result.E = 0;
  }
  return result;
}


bool withinWaypoint(Vector pos, Vector way, float threshold) 
{
  return threshold > sqrt((pos.N-way.N)*(pos.N-way.N) + (pos.E-way.E)*(pos.E-way.E));
}


/* withinCorridor()
 * Vector cur_pos : Should be the front of the vehicle to be more effective.
 * Vector cur_way, Vector next_way, double next_corr_radius
 * Returns a boolean indicating whether we are in the next corridor or not.
 * User may want to update the current waypoint according to the return value
 * from calling this function.
 * Note: This won't be useful for cases where the vehicle travels through the
 *       next corridor too fast, because then cur_way and next_way won't be
 *       correct. (unless the function calling this does some extra work)
 */
bool withinCorridor(Vector cur_pos, Vector cur_way,
                    Vector next_way, double next_corr_rad)
{
  bool within = false;

  // Check if within the circular part of the corridor    
  // Within the circle around the current waypoint
  // -or- within the circle around the next waypoint
  if (distVector(cur_pos, cur_way) < next_corr_rad ||
      distVector(cur_pos, next_way) < next_corr_rad) {
    within = true;
 //   cout << "~~~~~~~WITHIN THE CICULAR PART~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
  }
  //  else {
  // Check if within the rectangular part of the corridor
  // We consider the four vertices of the rectangle
  // v[1]------------------------v[2]               
  // |                              |                 
  // * <-- cur_way                  * <-- next_way  
  // |                              |               
  // v[0]------------------------v[3]               
    
  // v is an array for the vertices of the rectangle
  Vector v[4];     
  // way2way is the vector from the current to next waypoint
  Vector way2way = subVector(next_way, cur_way);
  // offset is the vector perpendicular to way2way, rotated 
  // counter-clockwise by pi/2
  Vector offset(way2way.E, -way2way.N);
  // Now scale the offset to be the vector from the waypoint to the
  // vertex of the rectangle on the waypoint radius
  // TODO: This will break if the successive waypoints are the same!
  offset = scaleVector(offset, next_corr_rad);
  // assign the vectors from the waypoints to the corresponding vertices
  v[0] = subVector(cur_way, offset);
  v[1] = addVector(cur_way, offset);
  v[2] = addVector(next_way, offset);
  v[3] = subVector(next_way, offset);

  // Now find the angles formed by the lines from the vehicle position
  // to adjacent vertices vpv = "vertex-position-vertex"
  double vpv[4];          
  
  for (int i = 0; i < 4; i++) 
  {
    vpv[i] = atan2(subVector(v[i%4], cur_pos).N, 
		   subVector(v[i%4], cur_pos).E) - 
             atan2(subVector(v[(i+1)%4], cur_pos).N, 
		   subVector(v[(i+1)%4], cur_pos).E);
    // normalize and get the acute angle absolute value
    vpv[i] = atan2( sin(vpv[i]), cos(vpv[i]) ); 
    vpv[i] = fabs( vpv[i] );
  }
  if (fabs((2*M_PI - (vpv[0] + vpv[1] + vpv[2] + vpv[3]))) < 
      PI2_ACCURACY_THRESHOLD) 
  {
    within = true; 
//    cout << "$$$$$$$$$$$$$$$$WITHIN THE RECTANGLE$$$$$$$$$$$$$$$$$$$$" << endl;
  }
  //} // end of else

  return within;
}

