#include "staticFender2rddf.hh"
#include <fstream.h>
#include <iostream.h>
#include <iomanip.h>
#include <math.h>
#include "RDDF/rddf.hh"
#include "RaceModules/GlobalPlanner/waypoints.hh"
#include "RaceModules/GlobalPlanner/global_functions.hh"

double dist2WP(const staticFender& fp, const waypoint& wp){
    return hypot(fp.N - wp.northing, fp.E - wp.easting); 
    }


int main(){
    //setup RDDF stuff
    Waypoints rddf;
    rddf.readRDDF("myrddf.dat", 0, 0);  //zeros are needed for function form
                                        //they have no meaning 
    //setup input file
    ifstream infile("staticFender.dat");
    int fileLength;
    infile >> fileLength;
    staticFender* fenderPoints = new staticFender[fileLength];
    for(int i=0; infile; i++) {
        infile >> fenderPoints[i].N;
        infile >> fenderPoints[i].E;
        infile >> fenderPoints[i].speed;
    }
    //setup output file
    ofstream outfile("fenderDDF.tmp");
    outfile << setprecision(10);

    int num_wp=rddf.get_last().num + 1;
    rddf.get_start();               //reset to top of list
//    rddf.get_next();                //never need to analyze waypoint 0
    //decided to start at 0 afterall to make indexing simpler later.
    while(rddf.get_current().t != NONE) { //.num<num_wp) {
        outfile << rddf.get_current().num << " "; 
        outfile << rddf.get_current().northing << ",";
        outfile << rddf.get_current().easting << ",";
        outfile << rddf.get_current().radius << " ";

        //do some precalculations needed for within Corridor calc
        double theta = atan2(rddf.get_current().northing - rddf.peek_previous().northing, rddf.get_current().easting - rddf.peek_previous().easting);
        double corrLen = hypot(rddf.get_current().northing - rddf.peek_previous().northing, rddf.get_current().easting - rddf.peek_previous().easting);
        double LB=rddf.peek_previous().radius;

        //now do the withinCorridor calculation
        for(int i=0; i<=fileLength; i++){
            double xfpN = fenderPoints[i].N - rddf.peek_previous().northing;
            double xfpE = fenderPoints[i].E - rddf.peek_previous().easting;
            double rotfpN = cos(theta) * xfpE + sin(theta) * xfpN;  
            double rotfpE = -sin(theta) * xfpE + cos(theta) * xfpN;
            bool withinCorridor = (0 < rotfpN) && (rotfpN < corrLen);
            withinCorridor = withinCorridor && (fabs(rotfpE) < LB); 
            if(withinCorridor) {
                outfile << fenderPoints[i].N << ",";
                outfile << fenderPoints[i].E << ",";
                outfile << dist2WP(fenderPoints[i], rddf.get_current()) << ",";
                outfile << fenderPoints[i].speed << " ";
            }
        }
        rddf.get_next();
        outfile << endl;
    }    
    delete [] fenderPoints;
}

/* Main Questions:
- how do we know what segment each fender point is in?
- How do we access these fender points?
- is there a method for reading in/storing fender points?
*/
