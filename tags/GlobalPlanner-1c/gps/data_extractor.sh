#!/bin/bash
#This script take a log produced by gps_data_logging and extracts
#latitude and longitude postions, which it prints to stdout.
#OUTPUT FORMAT:
#time, lat, long
#maintainer:  Alan Somers
#USAGE:
#data_extractor filename
#optionally, for output to a file:
#data_extractor filename > outfile

gawk 'BEGIN {FS="\n";RS="";OFS=" "; ORS="\n"}
        {print $4, $5}' $1 | \
gawk 'BEGIN {FS=" ";OFS=" "}
        {print $2, $4}' 

