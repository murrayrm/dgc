#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/fcntl.h>
#include <sys/io.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <unistd.h>
#include <linux/parport.h>
#include <linux/ppdev.h>

#include "parallel.h"

struct pp_struct pp[PP_NUM_PORTS];
// extern int errno;

/*****************************************************************************
 * pp_init
 *
 * Description:
 *      This function initializes the given parallel port, opens it for data
 *      transfer, and stores what we know about it in the pp_struct.
 *
 * Arguments:
 *      int port        the port number that we wish to initialize.
 *      int dir         the direction of the port.
 *
 * Return Values:
 *      0       success
 *      -1      error, errno set.
 ****************************************************************************/
int pp_init (int port, int dir) 
{
    int fd;             
    unsigned int mode = IEEE1284_MODE_BYTE;      /* Run port in Byte mode */ 
    char portName[strlen("/dev/parport00")];
    sprintf(portName, "/dev/parport%d", port);

    // dgc_log(sprintf("%d: Parallel Port #%d initialization attempted (dir=%d)", __FILE__, port, dir));

    /* Sanity check on the port number */
    if (port < 0 || port >= PP_NUM_PORTS) 
    {
        fprintf (stderr, "%s: Illegal Port specified (%d).\n", __FILE__, port);
        errno = EINVAL;
        return -1;
    } 
    if (dir != PP_DIR_IN && dir != PP_DIR_OUT) 
    {
        fprintf (stderr, "%s: Illegal Direction specified (%d).\n", __FILE__, dir);
        errno = EINVAL;
        return -1;
    } 
    /* actually initialize the struct; assume the array has been created */
    if ((fd = open(portName, O_RDWR)) == -1) 
    {
        fprintf (stderr, "%s (%d): Parallel Port %d could not be opened.\n", __FILE__, __LINE__, port);
        /* errno set by open */
        return -1;
    } 
    /* Set up the port for the correct read/write direction */
    if (ioctl(fd, PPCLAIM, NULL) == -1 || ioctl(fd, PPSETMODE, &mode) == -1 || ioctl(fd, PPDATADIR, &dir) == -1) 
    {
        fprintf (stderr, "%s (%d): ioctl() error, %s.\n", __FILE__, __LINE__, strerror(errno));
        // close(fd);
        return -1;
    } 
    /* Finally, save this information and set the current value */
    pp[port].fd = fd;
    pp[port].dir = dir;
    pp[port].data_val = 0;
    pp[port].ctrl_val = 0;
    /* Make it so, safely. */
    if (pp_set_bits(port, 0, PP_CTRL) == -1)
    {
        fprintf (stderr, "%s (%d): pp_set_bits returned an error.", __FILE__, __LINE__);
        return -1;
    }
    if (dir == PP_DIR_OUT) 
    {
        if (pp_set_bits(port, 0, PP_DATA) == -1)
        {
            fprintf (stderr, "%s (%d): pp_set_bits returned an error.", __FILE__, __LINE__);
            return -1;
        }
    }
    return 0;         /* yay */
}

int pp_end (int port) 
{
    if (ioctl(pp[port].fd, PPRELEASE) == -1)
    {
        fprintf (stderr, "%s (%d): ioctl() error, %s.\n", __FILE__, __LINE__, strerror(errno));
    }
    close(pp[port].fd);

    /* get rid of all data we have in the array */
    pp[port].fd = 0;
    pp[port].dir = 0;
    pp[port].data_val = 0;
    pp[port].ctrl_val = 0;

    return 0;
}

/*****************************************************************************
 * pp_set_bits
 *
 * Description:
 *      This function asserts bits on the parallel ports according to the 
 *      inputs (logic high is signal high).
 *
 * Arguments:
 *      int port        the port number that we wish to use.
 *      int high_bits   pins will be set high according to bits set here.
 *      int low_bits    pins will be set low according to bits set here.
 *
 * Return Values:
 *      0       no errors
 *      -1      error
 ****************************************************************************/
int pp_set_bits(int port, int pin_high, int pin_low)
{
    int data_high=0, data_low=0;        /* Will hold the data lines that are 
                                           actually being set. */
    int ctrl_high=0, ctrl_low=0;        /* Control lines that are actually 
                                           being set. */
    /* Note that we are not setting the status lines */

    if(pp[port].fd==0)
    {
        fprintf(stderr, "--pp_set_bits-- Parallel port %d is not initialized!!\n", port);
        return -1;
    }
    if((pin_high & pin_low) != 0)
    {
        fprintf (stderr, "A pin can't be both high and low!!\n");
        return -1;
    }
    if((pin_high & PP_DATA) != 0 || (pin_low & PP_DATA) != 0)
    {
        /* We are setting data pins either high or low */
        if(pp[port].dir==PP_DIR_IN )
        {
            fprintf (stderr, "Data lines are input, can't be set!!\n");
            return -1;          // DATA port is input, can't be set. Error.
        }
        data_high = pin_high & PP_DATA; // Data pins that are set high
        data_high = data_high >> 1;     // shift out the status line pin
        data_low = pin_low & PP_DATA;   // Data pins that are set low
        data_low = ~data_low;           // Make the low voltages zero bits.
        data_low = data_low >> 1;       // shift out the status line pin
        pp[port].data_val |= data_high;
        pp[port].data_val &= data_low;

        ioctl(pp[port].fd, PPWDATA, &pp[port].data_val);
    }
    if((pin_high & PP_CTRL)!=0 || (pin_low & PP_CTRL)!=0)
    {
        /* We are dealing with the control lines.  All but initialize are 
           inverted in hardware, so compensate for that before outputting. See
           http://www.beyondlogic.org/spp/parallel.pdf for more information. */
        ctrl_high=0;
        ctrl_low=0;
        if((pin_high & PP_CTRL_STROBE) !=0)
            ctrl_low |= PARPORT_CONTROL_STROBE;
        if ((pin_high & PP_CTRL_AUTOFD) !=0)
            ctrl_low |= PARPORT_CONTROL_AUTOFD;
        if ((pin_high & PP_CTRL_INIT) !=0)
            ctrl_high |= PARPORT_CONTROL_INIT;
        if ((pin_high & PP_CTRL_SELECT) !=0)
            ctrl_low |= PARPORT_CONTROL_SELECT;

        if((pin_low & PP_CTRL_STROBE) !=0)
            ctrl_high |= PARPORT_CONTROL_STROBE;
        if ((pin_low & PP_CTRL_AUTOFD) !=0)
            ctrl_high |= PARPORT_CONTROL_AUTOFD;
        if ((pin_low & PP_CTRL_INIT) !=0)
            ctrl_low |= PARPORT_CONTROL_INIT;
        if ((pin_low & PP_CTRL_SELECT) !=0)
            ctrl_high |= PARPORT_CONTROL_SELECT;

        ctrl_low = ~ctrl_low;
        pp[port].ctrl_val |= ctrl_high;
        pp[port].ctrl_val &= ctrl_low;

        ioctl(pp[port].fd, PPWCONTROL, &pp[port].ctrl_val);
    }
    /* Success */
    return 0;
}

/*****************************************************************************
 * pp_set_bits
 *
 * Description:
 *      This function sets the data byte (pins 2-9) to a given value at once.
 *
 * Arguments:
 *      int port        the port number that we wish to use.
 *      unsigned char val       value to be output.
 *
 * Return Values:
 *      0       no errors
 *      -1      error
 ****************************************************************************/
int pp_set_data(int port, unsigned char val)
{
    if(pp[port].fd==0)
    {
        fprintf(stderr, "--pp_set_data-- Parallel port %d is not initialized!!\n", port);
        return -1;
    }
    if(pp[port].dir==PP_DIR_IN)
    {
        fprintf (stderr, "Data lines are input, can't be set!!\n");
        return -1;
    }
    pp[port].data_val=val;
    ioctl(pp[port].fd, PPWDATA, &pp[port].data_val);
    return 0;
}

/* pp_get_bit(pp_struct port, int pin)
   Returns the value (1 or 0) of the pin given by the user.
 */
unsigned char pp_get_bit(int port, int pin) {
    unsigned char c;    /* Store the character read here */
    int b;              /* This is used to figure out if the bit we want is on
                           a data or status line */
    if(pp[port].fd==0)
    {
        fprintf(stderr, "--pp_get_bit-- Parallel port %d is not initialized!!\n", port);
        return -1;
    }
    /* Find out which byte the bit is from */
    if ((b = pin & PP_DATA)) {
        if(pp[port].dir==PP_DIR_OUT) {
            fprintf (stderr, "Data lines are output, shouldn't be reading!!\n");
            return -1;
        }
        ioctl(pp[port].fd, PPRDATA, &c);
        pp[port].data_val = c;
        return !!((b>>1) & (int) c);
    }
    else if ((b = pin & PP_STATUS)) {
        ioctl(pp[port].fd, PPRSTATUS, &c);
        switch (b) {
            case PP_PIN15 : return !!(PP_STAT15 & (int) c);
            case PP_PIN13 : return !!(PP_STAT13 & (int) c);
            case PP_PIN12 : return !!(PP_STAT12 & (int) c);
            case PP_PIN10 : return !!(PP_STAT10 & (int) c);
            case PP_PIN11 : return !(PP_STAT11 & (int) c);   /* pin11 inverted */
            default : fprintf(stderr, "you shouldn't be here -_-;; \n");
        }
    }
    else
        fprintf(stderr, "Cannot read from the designated pin %d \n", pin);

    return -1;
}

/* pp_get_data
   Returns the byte on success or negative one.
 */
unsigned char pp_get_data(int port) {
    unsigned char c;
    if(pp[port].fd==0)
    {
        fprintf (stderr, "--pp_get_data-- Parallel port %d is not initialized!!\n", port);
        return -1;
    }
    if(pp[port].dir==PP_DIR_OUT)
    {
        fprintf (stderr, "Data lines are output, shouldn't be reading!!\n");
        return -1;
    }
    ioctl(pp[port].fd, PPRDATA, &c);    // Assume no error.
    pp[port].data_val = c;
    return c;
}

