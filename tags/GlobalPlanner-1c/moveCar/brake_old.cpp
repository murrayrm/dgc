#include "moveCar/brake.h"

//Makes use of the receive_brake_command to reduce the need for extra code
//Just uses a dummy buffer
int send_brake_command(int command_num, char* parameters, int length) {
  char buffer[50];

  return receive_brake_command(command_num, parameters, length, buffer, 50, 1);
}

//Returns the response from sending a brake command
int receive_brake_command(int command_num, char* parameters, int length, char* buffer, int buf_length, int timeout) {
  char command[400];
  char word[11];  

  //Set the strings to null
  memset(command, 0, strlen(command));
  memset(buffer, 0, buf_length);

  //Prepare the command according to the brake actuator specs
  command[0]='@';
  sprintf(word, "%d ", BRAKE_ID);
  strcat(command, word);
  sprintf(word, "%d ", command_num);
  strcat(command, word);
  strcat(command, parameters);
  strcat(command, "\r\n");

  //Write the command
  serial_write(BRAKEPORT, command, strlen(command)-1, SerialBlock);
  
  //Read the response
  serial_read_until(BRAKEPORT, buffer, '\n', timeout, buf_length);

  //If it's a negative acknowledge (it starts with a !)
  if(strncmp(buffer, "!", 1)==0) {
    //Return an error
    return -1;
  } else {
    //Otherwise it's good
    return 0;
  }

}

//Sends a polling command to see if the brake is ready
int brake_ready() {
  char response[50];
  
  receive_brake_command(0, " ", 0, response, 50, 1);
  if(strcmp(response, "# 10 0000 2000\n")==0) {
    return 0;
  } else {
    return -1;
  }
}

//Initializes the brake
int init_brake() {
  char parameters[400];

  if (serial_open_advanced(BRAKEPORT, BRAKEBAUD, SR_PARITY_DISABLE | SR_SOFT_FLOW_DISABLE | SR_HARD_FLOW_DISABLE | SR_TWO_STOP_BIT) != 0) {
    printf("Unable to open com port %d \n", BRAKEPORT);
    return -1;
  } else {
    //If the serial port is initialized, send initialization commands to the brake
    //These are copied directly from the default initialization in the QuickSilver Controls Windows program
    //Note: They probably aren't needed!
    //For information on each command, look up the command ID (first argument in the send_brake_command command) in the QuickSilver manual

    //Format:
    //sprintf(parameters, "parameter format string", param1, param2, ...);
    //send_brake_command(command_num, parameters, strlen(parameters));
    
    sprintf(parameters, "%d ", 5136);
    send_brake_command(155, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 1);
    send_brake_command(185, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 0);
    send_brake_command(186, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 576);
    send_brake_command(174, parameters, strlen(parameters));

    //ACK Delay
    sprintf(parameters, "%d ", 0);
    send_brake_command(173, parameters, strlen(parameters));
    
    //MCT
    //FLC

    sprintf(parameters, "%d %d %d %d %d %d %d ", 0, 6, 6, 20, 20, 200, 500);
    send_brake_command(148, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 0);
    send_brake_command(237, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 0);
    send_brake_command(184, parameters, strlen(parameters));

    sprintf(parameters, "%d %d %d %d ", 15000, 20000, 6000, 10000);
    send_brake_command(149, parameters, strlen(parameters));

    sprintf(parameters, "%d %d ", 10, 4);
    send_brake_command(150, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 1250);
    send_brake_command(230, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 32767);
    send_brake_command(195, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 10);
    send_brake_command(212, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 52);
    send_brake_command(213, parameters, strlen(parameters));

    //Error Limits (ERL)
    sprintf(parameters, "%d %d %d ", 500, 200, 120);
    send_brake_command(151, parameters, strlen(parameters));

    //Kill Motor Condition (KMC)
    sprintf(parameters, "%d %d ", 128, 0);
    send_brake_command(167, parameters, strlen(parameters));

    sprintf(parameters, "%d %d ", 0, 83);
    send_brake_command(252, parameters, strlen(parameters));
    
    /*
    //Soft Stop Limits
    sprintf(parameters, "%d ", SSL_REGISTER);
    send_brake_command(221, parameters, strlen(parameters));

    //Write Register (of min Soft Stop Limit)
    sprintf(parameters, "%d %d ", SSL_REGISTER, SSL_MIN);
    send_brake_command(11, parameters, strlen(parameters));

    //Write Register (of max Soft Stop Limit)
    sprintf(parameters, "%d %d ", SSL_REGISTER+1, SSL_MAX);
    send_brake_command(11, parameters, strlen(parameters));
    */

    return 0;
  }
}

//Sets the brake's absolute position in 0-1 units
int set_brake_abs_pos(double abs_pos, double vel, double acc) {
  char parameters[50];
  char response[50];

  //Limit and scale the velocity, acceleration, and range
  vel=in_range_double(vel, 0, 1);
  vel=scale_double(vel, 0, 1, MIN_VEL, MAX_VEL);

  acc=in_range_double(acc, 0, 1);
  acc=scale_double(acc, 0, 1, MIN_ACC, MAX_ACC);

  abs_pos=in_range_double(abs_pos, 0, 1);
  abs_pos=scale_double(abs_pos, 0, 1, MIN_POS, MAX_POS);

  /*
  send_brake_command(5, " ", 0);
  send_brake_command(0, " ", 0);
  send_brake_command(1, "65535 ", strlen("65535 "));
  send_brake_command(3, "500000 ", strlen("500000 "));
  */

  //The following commands are copied directly from how the QuickSilverMax WIndows program does it
  //Send a polling command
  send_brake_command(0, " ", 0);
  //Clear the polling status word
  send_brake_command(1, "65535 ", strlen("65535 "));
  //Get revision info?
  send_brake_command(5, " ", 0);
  //Clear the program buffer
  send_brake_command(8, " ", 0);
  //Start the program download
  send_brake_command(9, " ", 0);

  //Move Absolute, Velocity Based (MAV)
  sprintf(parameters, "%.0f %.0f %.0f %d %d ", abs_pos, acc, vel, STOP_ENABLE_ABS, STOP_STATE_ABS);
  send_brake_command(134, parameters, strlen(parameters));

  //End the program download
  send_brake_command(128, " ", 0);
  //Run the program
  receive_brake_command(10, " ", 0, response, 50, 1);

  if(strcmp(response, BRAKE_ACK)==0) {
    return 0;
  } else {
    return -1;
  }
}

//Sets the brake's relative position in 0-1 units
int set_brake_rel_pos(double rel_pos, double vel, double acc) {
  char parameters[50];
  char response[50];

  //Limit and scale the velocity, acceleration, and range
  vel=in_range_double(vel, 0, 1);
  vel=scale_double(vel, 0, 1, MIN_VEL, MAX_VEL);

  acc=in_range_double(acc, 0, 1);
  acc=scale_double(acc, 0, 1, MIN_ACC, MAX_ACC);

  rel_pos=in_range_double(rel_pos, -1, 1);
  rel_pos=scale_double(rel_pos, -1, 1, -MAX_POS, MAX_POS);

  //send_brake_command(5, " ", 0);
  //send_brake_command(0, " ", 0);
  //send_brake_command(1, "65535 ", strlen("65535 "));
  //send_brake_command(3, "500000 ", strlen("500000 "));
  //Send a polling command
  send_brake_command(0, " ", 0);
  //Clear the polling status word
  send_brake_command(1, "65535 ", strlen("65535 "));
  //Get revision info?
  send_brake_command(5, " ", 0);
  //Clear the program buffer
  send_brake_command(8, " ", 0);
  //Start the program download
  send_brake_command(9, " ", 0);

  //Move Relative, Velocity Based (MRV)
  sprintf(parameters, "%.0f %.0f %.0f %d %d ", rel_pos, acc, vel, STOP_ENABLE_ABS, STOP_STATE_ABS);
  send_brake_command(135, parameters, strlen(parameters));

  //End the program download
  send_brake_command(128, " ", 0);
  //Run the program
  receive_brake_command(10, " ", 0, response, 50, 1);

  if(strcmp(response, BRAKE_ACK)==0) {
    return 0;
  } else {
    return -1;
  }
}
int set_brake_rel_pos_counts(double rel_pos, double vel, double acc) {
  char parameters[50];
  char response[50];

  //Limit and scale the velocity and acceleration
  vel=in_range_double(vel, 0, 1);
  vel=scale_double(vel, 0, 1, MIN_VEL, MAX_VEL);

  acc=in_range_double(acc, 0, 1);
  acc=scale_double(acc, 0, 1, MIN_ACC, MAX_ACC);

  //send_brake_command(5, " ", 0);
  //send_brake_command(0, " ", 0);
  //send_brake_command(1, "65535 ", strlen("65535 "));
  //send_brake_command(3, "500000 ", strlen("500000 "));
  //Send a polling command
  send_brake_command(0, " ", 0);
  //Clear the polling status word
  send_brake_command(1, "65535 ", strlen("65535 "));
  //Get revision info?
  send_brake_command(5, " ", 0);
  //Clear the program buffer
  send_brake_command(8, " ", 0);
  //Start the program download
  send_brake_command(9, " ", 0);

  //Move Relative, Velocity Based (MRV)
  sprintf(parameters, "%.0f %.0f %.0f %d %d ", rel_pos, acc, vel, STOP_ENABLE_ABS, STOP_STATE_ABS);
  send_brake_command(135, parameters, strlen(parameters));

  //End the program download
  send_brake_command(128, " ", 0);
  //Run the program
  receive_brake_command(10, " ", 0, response, 50, 1);

  if(strcmp(response, BRAKE_ACK)==0) {
    return 0;
  } else {
    return -1;
  }
}

//Get the actuator position in counts
int get_brake_pos() {
  char buffer[50];
  int command, upper_word, lower_word, position;

  receive_brake_command(12, "1 ", strlen("1 "), buffer, 50, 1);  

  sscanf(buffer, "# 10 %X %X %X", &command, &upper_word, &lower_word);

  upper_word <<= 16;
  position=lower_word+upper_word;

  if(command == 12) {
    return position;
  } else {
    return -1;
  }
}

int calib_brake() {
  //Restart the Actuator
  send_brake_command(4, " ", 0);
  //Wait until the actuator is ready again
  while(brake_ready()!=0) {};
  //Send the brake all the way out
  set_brake_rel_pos_counts(250000, 1, 1);
  //Sleep for two seconds to give it time to go the full range of motion
  sleep(2);
  //Restart it again, since we probably generated a moving error
  send_brake_command(4, " ", 0);
  //Wait until the actuator is ready again
  while(brake_ready()!=0) {};
  set_brake_rel_pos_counts(-10000, 0.6, 0.6);
  sleep(2);
  set_brake_rel_pos_counts(ZERO_POS+10000, 0.5, 0.5);
  while(brake_ready()!=0) {};
  //Make the current position the 0 position
  return send_brake_command(145, " " , 0);
}

//Helper functions for limiting and scaling numbers
double in_range_double(double val, double min_val, double max_val) {
  if(val<min_val) return min_val;
  if(val>max_val) return max_val;
  return val;
}

double scale_double(double val, double old_min_val, double old_max_val, double new_min_val, double new_max_val) {
  double old_range, new_range;

  old_range=old_max_val-old_min_val;
  new_range=new_max_val-new_min_val;

  return ((new_range*(val-old_min_val)/old_range)+new_min_val);
}
