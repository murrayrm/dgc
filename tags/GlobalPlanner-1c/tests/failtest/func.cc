#include "func.hh"





// BS for steering to work
int negCountLimit        =    -60000;
int posCountLimit        =     60000;
int verbosity_exec_cmd   =      TRUE;
int steer_errno          =  ERR_NONE;
string lastSteeringCommanded;



const float brakeVelo = 0.9;

// This function takes in the state data of the vehicle and commands
// actuators. In case of waypoint following (added 11/30/2003) the function
// will commannd both steering and throttle/brake actuators. 

void SteerSet(float steer) {
  // Send command to steering actuator
  if( -1.0 <= steer && steer <= 1.0 ) {
    //cout << "Setting steering to " << steer << endl;
    steer_heading(steer);
  } else {
    cout << "Bad Steering Command = " << steer << endl;
  }
}

void AccelSet(float accel) {
  // Send commands to throttle/brake actuators
  if(-1.0 <= accel && accel <= 0.0) {
    // cout << "Setting brake to " << accel << endl; 
    set_accel_abs_pos(0.0);
    //set_brake_abs_pos_pot( accel, brakeVelo, brakeVelo );
    
  } else if(0.0 <= accel && accel <= 1.0) {
    // cout << "Setting throttle to " << accel << endl;
    //set_brake_abs_pos_pot( 0.0, brakeVelo, brakeVelo );
    set_accel_abs_pos(0);
    set_accel_abs_pos(accel);
  } else {
    cout << "Bad Accel Command = " << accel << endl;
  }
  
}

void BrakeSet(float accel) {
  // Send commands to throttle/brake actuators
  if(-1.0 <= accel && accel <= 0.0) {
    // cout << "Setting brake to " << accel << endl; 
    // set_accel_abs_pos(0.0);
    set_brake_abs_pos_pot( accel*-1.0, brakeVelo, brakeVelo );
    
  } else if(0.0 <= accel && accel <= 1.0) {
    // cout << "Setting throttle to " << accel << endl;
    set_brake_abs_pos_pot( 0.0, brakeVelo, brakeVelo );
    //set_accel_abs_pos(0);
    //set_accel_abs_pos(accel);
  } else {
    cout << "Bad Accel Command = " << accel << endl;
  }
}
