#ifndef WAYPOINTNAV_HH
#define WAYPOINTNAV_HH

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

using namespace std;

#define TRUE 1
#define FALSE 0

struct DATUM {
  int shutdown_flag;
  boost::recursive_mutex loop1Lock;
  boost::recursive_mutex loop2Lock;
  double brakePos; 

};

// Method protocols
int Init1(DATUM &d);
int Loop1(DATUM &d);
int Init2(DATUM &d);
int Loop2(DATUM &d);

#endif 
