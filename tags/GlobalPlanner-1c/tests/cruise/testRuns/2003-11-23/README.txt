11/23/2003 testing at Santa Anita

* open loop coast tests
* H's feedforward cruise control
* Thyago's cruise controller
* open loop shifting (ideally to third gear)

Test numbers and descriptions: all tests in 3rd gear automatic

Coast tests:

1. East to west (EW) run coast test
2. WE coast test (same path)
3. EW same path as (1) (quite swervy)
4. WE coast test (same path as 2)
5. SE NW coast test
6. NW SE coast test (same path)  he turned west half way
7. SE NW coas test (same path as 5)
8. NW SE coast test (same path as 6)
9. H cruise control at (Gp = .01;Gd = .05, UpperCutOff = .3; LowerCutOff = 0; Vref = 5m/s) 
10. H cruise control at (Gp = .012;Gd = .06, UpperCutOff = .3; LowerCutOff = 0; Vref = 5m/s)
11. H cruise control at (Gp = .018;Gd = .085, UpperCutOff = .3; LowerCutOff = 0; Vref = 5m/s)
12. H cruise control at (Gp = .025;Gd = .11, UpperCutOff = .3; LowerCutOff = 0; Vref = 5m/s)
13. H cruise control at (Gp = .1;Gd = .5, UpperCutOff = .3; LowerCutOff = 0; Vref = 5m/s)
14. H cruise control at (Gp = .1;Gd = .2, UpperCutOff = .3; LowerCutOff = 0; Vref = 5m/s)
15. H cruise control at (Gp = .15;Gd = .2, UpperCutOff = .3; LowerCutOff = 0; Vref = 5m/s)
16. H cruise control at (Gp = .12;Gd = .2, UpperCutOff = .3; LowerCutOff = 0; Vref = 5m/s)
17. H cruise control at (Gp = .105;Gd = .2, UpperCutOff = .3; LowerCutOff = 0; Vref = 5m/s)
18. H cruise control at (Gp = .0;Gd = .2, UpperCutOff = .3; LowerCutOff = 0; Vref = 5m/s)

Acceleration tests (get direction from GPS data).  Throttle at

19. 0.3
20. 0.3
21. 0.2
22. 0.2
23. 0.35
24. 0.35
25. 0.4
26. 0.4
27. 0.45
28. 0.45
29. 0.5
30. 0.5
31. 1.0
32. Throttle at 1.0 EW
33. Throttle at 0.25 WE
34. Thr. at 0.25 EW (killed)
35. Cruise control test with updated accel map (aborted.. lost OBDII)
36. After adjustments, H cruise control at (Gp = .01;Gd = .05, UpperCutOff = .3; LowerCutOff = 0; Vref = 4m/s)
37. After adjustments, H cruise control at (Gp = .01;Gd = .05, UpperCutOff = .3; LowerCutOff = 0; Vref = 4m/s)

We changed the gain for the feed fawared to 0.01

38. After adjustments, H cruise control at (Gp = .02;Gd = .05, UpperCutOff = .3; LowerCutOff = 0; Vref = 4m/s)
39. After adjustments, H cruise control at (Gp = .04;Gd = .1, UpperCutOff = .3; LowerCutOff = 0; Vref = 4m/s)
40. After adjustments, H cruise control at (Gp = .04;Gd = .1, UpperCutOff = .3; LowerCutOff = 0; Vref = 8m/s)

We changed the gain for the feed fawared to 0

41. H cruise control at (Gp = .04;Gd = .1, UpperCutOff = .3; LowerCutOff = 0; Vref = 10m/s)
42. Thyago cruise control at (Gp = .075;Gd = 1.25, UpperCutOff = .3; LowerCutOff = 0; Vref = 8m/s)
