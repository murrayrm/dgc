#include "WaypointFollowing.hh"

using namespace std;

int shutdown_flag;
const double waypointRadius = 10.0;      // meters around waypoint

const double MAX_STEER = .5;             // maximum steering angle in radians
const double DESIRED_VELOCITY = 5 * 1600 / 3600;    // desired velocity in m/s
const double VELO_RANGE = 5 * 1600 / 3600;
const double ACCEL_TOOMUCH = .1;
const double TOO_FAST = 2 * 1600 / 3600;  

const double MIN_ACCEL_GAS   =   0.00;
const double MAX_ACCEL_GAS   =   0.10;
const double MIN_ACCEL_BRAKE =  -0.40;
const double MAX_ACCEL_BRAKE =  -0.90;

const double SCALE_ACCEL_GAS   =  1.0; // scales m/s to accel 
const double SCALE_ACCEL_BRAKE =  1.0; 

/* Already defined in cruisemain.cc, probably don't need them here anyway.
int negCountLimit        =    -60000;
int posCountLimit        =     60000;
int verbosity_exec_cmd   =      TRUE;
int steer_errno          =  ERR_NONE;
double brakeVelo          =       0.7;
*/
  
// return value: steering angle in -1(left) to 1(right)
double f_Steering(Vector curPos, 
                  Vector curWay, 
                  Vector curHead) {
  // find the angle between curWay and curHead, set it as the new steering
  double steer = -get_deflection(curPos, curWay, curHead);
  
  //  cout << "steer = " << steer << endl;

  // we're squaring the magnitude of the steering here for some reason
  // what is the explanation???  
  //steer = steer * fabs(steer);

  // saturate the steering command to be in [-1, 1]
  if (fabs(steer) > MAX_STEER) {
    if (steer > 0)
      return 1.0;
    else // steer < 0
      return -1.0;
  }
  else {
    return steer/MAX_STEER;
  }
} // end f_Steering

// return value: desired velocity at this point
// Needs a body that calculates a good velocity given state & possibly steering
double f_Velo(double curVelo, Vector curPos, Vector curWay)
{  
  double des_velo;

  // Check to see what the max speed possible is
  des_velo = 1.5;  // m/s

  return des_velo;
}

// Output: pair<double, double> = <velocity, steering angle>
// Designed to be called in AccelLoop for now.
pair<double, double> globalEst(Vector head, Vector pos, 
                               double speed, struct WaypointFollowing &WF) 
{
  pair<double, double> result;
  Vector currentWaypoint;
  timeval tv;

  if( WF.waypoints.size() > 0) {
    currentWaypoint = WF.waypoints.front();
    cout << "Waypoint Position = " << currentWaypoint.E
	 << "E, " << currentWaypoint.N << "N" << endl;
    cout << "Current  Position = " << pos.E 
	 << "E, " << pos.N << "N" << endl;
    cout << "Current  Heading = " << head.E 
	 << "E, " << head.N << "N" << endl;
    cout << "Speed = " << speed << endl;
    
    if( withinWaypoint(pos, currentWaypoint, waypointRadius) ) {
      cout << "!!!!!!!!!!!!!!!Waypoint Acheieved!!!!!!!!!!!" << endl;
      WF.waypoints.pop_front();
    } 
    else {   // Gotta get to the next waypoint
      result.second = f_Steering(pos, currentWaypoint, head);  // Steering
      result.first = f_Velo(speed, pos, currentWaypoint);      // Velocity
      // Log driving decisions
      gettimeofday(&tv, NULL);
      WF.driveFile << timeval_subtract(tv, WF.timeZero) << "\t"
 		   << pos.E << "\t"
		   << pos.N << "\t"
		   << head.E  << "\t"
		   << head.N  << "\t"
		   << speed   << "\t"
		   << result.second << "\t"
		   << result.first  <<endl;
      //sleep_for(1);
      //      usleep_for(100000);        // no reaso to sleep here anymore since the main program is AccelLoop
    } // end of if(withinWaypoint) else ...
  } 
  else {   // Done with this set of waypoint following
    // Reset the brake so we are guaranteed to stop the vehicle
    result.first = 0;               // Velocity
    result.second = 0;              // Steering
  } // end of if(waypoints.size > 0) else ...

  cout << "--GlobalEst: done 1 cycle--" << endl;
  return result;
}


void initWaypts(struct WaypointFollowing &WF){

  // Let the state estimator get data.
  //  sleep_for(5);
  Vector currentWaypoint;

  // Read in all waypoints
  ifstream waypointFile("../../Global/waypoints.gps");
  WF.waypoints.clear();
  while(!waypointFile.eof()) {
    waypointFile >> currentWaypoint.N >> currentWaypoint.E;
    cout << "North = " << currentWaypoint.N << ", East = " << currentWaypoint.E << endl;
    WF.waypoints.push_back(currentWaypoint);
  }

  // close waypoints file
  waypointFile.close();
}
