function [rowRes, colRes, leftBottomUTMx, leftBottomUTMy, sample_map] = MATsample2
%
% function MATsample2
%
% Changes:
%     Created by genMap::saveMATFile.
%   %This MATFile is mostly the output of MATFile_test.
% Adam Craig added the lists by hand.
% Function:
%  The function returns the genMap data in the form of a 5-element array, so
%  that another function/script may read the data and display, etc.
%
% Usage example: 
%  To display the information stored in this file, use team/matlab/viewGenMap.m
%
% Note: 
%   Currently, you MUST put in only one map into each .m file and follow the
%   format exactly if you want to use viewGenMap.m to display data. This file
%   should have been generated by genMap::saveMATFile.
%

rowRes = 0.666
colRes = 0.3
leftBottomUTMx = 1
leftBottomUTMy = 2
sample_map.value = [
9	8	7	;
6	5	4	;
3	2	1	];
sample_map.color = [
3	2	3	;
2	1	2	;
3	2	3	];
sample_map.list = {
[ 1 2 ]	[ 4 5 ]	[ 6 7 ]	;
[ 8 9 ]	[ 10 ] [ 11 ] ;
[ 12 ] [ 13 ] [ 14 ] };
