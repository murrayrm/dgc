/*
 * prog1.c - sample shared memory program
 *
 * RMM, 19 Dec 03
 *
 * This is a very simple program that opens up a piece of shared memory
 * (creating it if necessary), then writes numbers into the first few
 * bytes (so that it can be read by prog2).
 *
 * Note:
 *   - for the permissions, this is a 9 bit pattern (3 bits/type)
 *   - memory key number is random; should be the same for everyone
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define PRGNAME "P1"

main(int argc, char **argv)
{
  int shmid;				/* Shared memory identifier */
  int size = 1024;		   /* Size of shared memory segment */
  int key = 117;			/* Unique shared memory key */
  int shmflg;				/* Flags to shmget, set below */
  int *p;				/* pointer to shared memory */
  int i;

  /* Create a piece of shared memory */
  shmflg = (IPC_CREAT | 0x1ff);		/* Create R/W segment */
  if ((shmid = shmget(key, size, shmflg)) < 0) {
    fprintf(stderr, "%s: shmget failed\n", PRGNAME);
    perror(PRGNAME);
    exit(0);
  }
  fprintf(stderr, "%s: shmget succeeded\n", PRGNAME);

  /* Now attach to the memory segment */
  p = (int *) shmat(shmid, NULL, 0);

  /* Finally, write out some data to the segment */
  for (i = 1; i < 200; ++i) p[0] = i;

  return 0;
}
