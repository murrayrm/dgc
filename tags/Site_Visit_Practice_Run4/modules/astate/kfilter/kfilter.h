/*----------------------------------------------------------------------------
--                                                                          --
--                        U N C L A S S I F I E D                           --
--                                                                          --
--       N O R T H R O P   G R U M M A N   P R O P R I E T A R Y            --
--                                                                          --
-- Copyright (c) 2002 Northrop Grumman Corp.                                --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------
--                                                                          --
--    The code contained herein is proprietary to Northrop Grumman Corp.    --
--    and is subject to any non-disclosure agreements and proprietary       --
--    information exchange agreements between Northrop Grumman and the      --
--    and the receiving party.  The code contained herein should be         --
--    considered technical data and is subject to all relevant export       --
--    control regulations.                                                  --
--                                                                          --
----------------------------------------------------------------------------*/

/* File: kfilter.h
 * Date: 12/29/03
 */


/***************************************************************************
 * Timing Control
 ***************************************************************************/

extern double imu_update_rate;                 // Rate of IMU data output, Hz

/*
 * The variables below govern the repeat rates and offsets of differnt Kalman 
 * filtering functions.  The constants "XREP###" define the rates at which 
 * different functions execute as multiples of the IMU output rate.  The 
 * definitions are clear if one imagines the IMU to be outputting at 1000 Hz, 
 * in which case XREP500 executes every 500 Hz, or every 2 cycles.  The 
 * xdue variables store the time until the next execution of those routines,
 * and the XOFFSET### store the offsets of the initial execution.  For example.
 * if XOFFSET10 = 50, then those routines would execute every 100 cycles of the
 * IMU output, starting with the 50th cycle after initialization.  Note that
 * there are multiple sets running at "500Hz"


 */

#define  IMU_HZ       400               // output rate of IMU, Hz.
#define  XREP500      2                  // 500 Hz tasks rep rate
#define  XREP250      4                  // 250 Hz tasks rep rate
#define  XREP100      10                 // 100 Hz tasks rep rate
#define  XREP10       100                // 10  Hz tasks rep rate
#define  XREP2        200                // 2 Hz tasks rep rate
#define  XREP1        400                // 1 Hz tasks rep rate
#define  XRATIO       5                  // should be XREP100/XREP500;
#define  XRATIO2      10                 // should be XRATIO*2;

#define  XOFFSET500A  0                  // 500 Hz (A) task offset   usually 2
#define  XOFFSET500B  0                  // 500 Hz (B) task offset   usually 1
#define  XOFFSET500C  2                  // 500 Hz (C) task offset
#define  XOFFSET250   1                  // 250 Hz task offset
#define  XOFFSET100   0                  // 100 Hz task offset
#define  XOFFSET10    100                // 10 Hz task offset
#define  XOFFSET2     500                // 2 Hz  task offset
#define  XOFFSET1     0                  // 1 Hz task offset


/****************************************************************************
 * Function Definitions
 ****************************************************************************/

void DGCNavInit(void);
void DGCNavRun(void);
