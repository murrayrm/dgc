MODEMANMODULE_PATH = $(DGC)/modules/modeManModule

MODEMANMODULE_DEPEND_LIBS = \
	$(SPARROWLIB) \
	$(TRAJLIB) \
	$(SKYNETLIB) \
	$(MODULEHELPERSLIB) \
	$(CMAPLIB) \
	$(CMAPPLIB)

MODEMANMODULE_DEPEND_SOURCES = \
	$(MODEMANMODULE_PATH)/ModeManModule.hh \
	$(MODEMANMODULE_PATH)/ModeManModule.cc \
	$(MODEMANMODULE_PATH)/ModeManSparrow.cc \
	$(MODEMANMODULE_PATH)/navdisplay.cc \
	$(MODEMANMODULE_PATH)/navdisplay.dd \
	$(MODEMANMODULE_PATH)/Makefile

MODEMANMODULE_DEPEND = $(MODEMANMODULE_DEPEND_LIBS) $(MODEMANMODULE_DEPEND_SOURCES)
