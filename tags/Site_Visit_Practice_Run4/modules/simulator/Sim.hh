#ifndef SIM_HH
#define SIM_HH

#define LOG_FILE_DIR "logs/"
#define LOG_FILE_NAME "sim_"
#define LOG_FILE_EXT ".dat"

#include <time.h>
#include <unistd.h>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max

#include "VehicleState.hh"
#include "sn_msg.hh"

#include "models/SimModel.hh"
#include "models/SimModelSimple.hh"

#include "adrive_skynet_interface_types.h"
#include "adrive.h"
using namespace std;



struct SIM_DATUM
{
  /** Vehicle state struct, from VehicleState.hh. */
  VehicleState SS;
  /** Steering command [-1,1]. */
  double steer_cmd;
  /** Acceleration command [-1,1]. */
  double accel_cmd;
  /** Time stamp (UNIX time in us). */
  unsigned long long lastUpdate;
  /** Steering angle [rad]. */
	double phi;
};

class asim
{
	bool m_bSimpleModel;
	double m_steerLag, m_steerRateLimit;

	StateReport	simState;
	SimModelSimple	simEngineSimple;
	SimModel		    simEngine;

	vehicle_t*  m_pVehicle;

	skynet m_skynet;

	pthread_mutex_t m_stateMutex;

	void SimInit(void);
	void updateState(void);

	int broadcast_statesock;

public:
  asim(int sn_key, bool bSimpleModel, double steerLag, double steerRateLimit);
  ~asim();

  void StateComm();
  void DriveComm();

  void Active();

  void broadcast();

  void SimUpdate(float deltaT);

  void SparrowDisplayLoop();
  void UpdateSparrowVariablesLoop();

	struct vehicle_t* getADriveVehicle(void)
	{
		return m_pVehicle;
	}
};

#endif
