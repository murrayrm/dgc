/**
 * main.cc
 * Revision History:
 * 01/21/2005  hbarnor  Created
 */

#include <getopt.h>
#include <assert.h>

#include "MapDisplaySN/MapDisplaySN.hh"
#include "MapDisplay/MapConfig.hh"
#include "MainDisplay/GuiThread.h"
#include "MainDisplay/gui.h"

int NOSTATE   = 0;       // Don't update state if = 1.
int NOSKYNET  = 0;       // don't use skynet if = 1

//forward declare call to mta_main
int mta_main(MapConfig * mapC);

/* The name of this program. */
const char * program_name;

/* Prints usage information for this program to STREAM (typically stdout
   or stderr), and exit the program with EXIT_CODE. Does not return. */
void print_usage (FILE* stream, int exit_code)
{
  fprintf( stream, "Usage:  %s [options]\n", program_name );
  fprintf( stream,
           "  --nostate         Continue even if state data is not received.\n"
           "  --noskynet        Don't use skynet for messaging.\n"
           "  --help, -h        Display this message.\n" );
  exit(exit_code);
}

int main(int argc, char *argv[])
{
  /* Set the default arguments that won't need external access here. */
 
  /* Temporary character. */
  int ch;
  /* A string listing valid short options letters. */
  const char* const short_options = "h";
  /* An array describing valid long options. */
  static struct option long_options[] = 
  {
    // first: long option (--option) string
    // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
    // third: if pointer, set variable to value of fourth argument
    //        if NULL, getopt_long returns fourth argument
    {"nostate",    0, &NOSTATE,          1},
    {"noskynet",   0, &NOSKYNET,         1},
    {"help",       0, NULL,              'h'},
    {NULL,         0, NULL,              0}
  };

  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  program_name = argv[0];
  printf("\n");
  
  // Loop through and process all of the command-line input options.
  while((ch = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
  {
    switch(ch)
    {
      case 'h':
        /* User has requested usage information. Print it to standard
           output, and exit with exit code zero (normal 
           termination). */
        print_usage(stdout, 0);

      case '?': /* The user specified an invalid option. */
        /* Print usage information to standard error, and exit with exit
           code one (indicating abnormal termination). */
        print_usage(stderr, 1);

      case -1: /* Done with options. */
        break;
    }
  }
  GuiThread * myGui;
  MapConfig * mc = new MapConfig();
  if( NOSKYNET)
    {
      cerr << "MTA implementation deprecated." << endl;
    }
  else
    {
      cerr << "Searching for skynet KEY " << endl;
      int sn_key = 0;
      char* pSkynetkey = getenv("SKYNET_KEY");
      if( pSkynetkey == NULL )
	{
	  cerr << "SKYNET_KEY environment variable isn't set" << endl;
	}
      else
	sn_key = atoi(pSkynetkey);
      
      cerr << "Constructing skynet with KEY = " << sn_key << endl;
      cerr << "Starting skynet " << endl;
    
      // mapconfig stuff 

      // Set the RDDF in the MapConfig
      RDDF rddf("rddf.dat");
      RDDFVector rddf_vector = rddf.getTargetPoints();
      mc->set_rddf( rddf_vector );    
      MapDisplaySN mapDisplay(mc, sn_key);
      mapDisplay.init();    
      //now run gui thread
      myGui  = new GuiThread(mc);      
      myGui->getGui()->buildStartTab("moduleStarter.config");
      myGui->getGui()->initGui();;      
      myGui->start_thread();
      cout << "Spawned GUI Thread " << endl;
      // start a thread for showing all of the traj's except for the history traj.
      // this one is handled inside getStateThread. feel free to make this cleaner
    for(int i=1; i<PATHIDX_NUMPATHS; i++)
      {
	DGCstartMemberFunctionThreadWithArg(&mapDisplay,&MapDisplaySN::getTrajThread, (void*)i);
      }

    cout << "starting " << SNGUI_LAYER_TOTAL << endl;
    for(int i=0; i<SNGUI_LAYER_TOTAL; i++) 
      {
	DGCstartMemberFunctionThreadWithArg(&mapDisplay,&MapDisplaySN::getMapDeltaThread, (void*)i);
    }
    cerr << "Guide to layers: secret=0, FusionMapper=1, Stereo=2, Ladar=3, Static=4" << endl;

    mapDisplay.getStateThread();
    
    }
  /**  while(true)
   * {
   *  sleep(500);
   *      }
   */
  delete mc;
  delete myGui;
  cerr << "Done .. Exiting ..." << endl;
  return 0;
}
