#include <unistd.h>
#include <iostream>

#include "PlannerModule.hh"
#include "planner.h"
#include "DGCutils"

int QUIT_PRESSED = 0;

extern int           NOSTATE;
extern int           NOPLAN;
extern int           NOMM;
extern unsigned long SLEEP_TIME;
extern int					 STATICMAP;

#define MAX_DELTA_SIZE							1000000
#define MIN_PLANNING_CYCLETIME_US		0
#define PLANNING_LOOKAHEAD					0.2
#define PROXIMITY_TO_TRAJ_TO_LOOKAHEAD 0.5
// #define NO_LOOKAHEAD

CPlannerModule::CPlannerModule(int sn_key)
	: CSkynetContainer(SNplanner, sn_key), m_rddf("rddf.dat"), m_bReceivedAtLeastOneDelta(false)
{
	m_pMapDelta = new char[MAX_DELTA_SIZE];

#warning "these shouldn't be hard coded"
	if(STATICMAP == 1)
	{
		m_map.initMap(0.0 ,0.0, 2500, 1000, 0.2, 0.2, 0 );
		// Add the speed limit layer to the map
		m_mapLayer = m_map.addLayer<double>(-3.0, -2.0);
		m_map.loadLayer<double>(m_mapLayer, "cmap", true);
	}
	else
	{
		m_map.initMap(0.0, 0.0, 150, 150, 1.0, 1.0, 0 );
		// Add the speed limit layer to the map
		m_mapLayer = m_map.addLayer<double>(2.0, 2.1);
	}
	DGCcreateMutex(&m_mapMutex);
	DGCcreateMutex(&m_deltaReceivedMutex);
	DGCcreateCondition(&m_deltaReceivedCond);
}

CPlannerModule::~CPlannerModule() 
{
	delete m_pMapDelta;
	DGCdeleteMutex(&m_mapMutex);
	DGCdeleteMutex(&m_deltaReceivedMutex);
	DGCdeleteCondition(&m_deltaReceivedCond);
}

void CPlannerModule::getMapDeltasThread()
{
  ofstream delta_log("logs/delta.log");
  delta_log << "# plannerModule produced this.  Time then size." << endl;
  unsigned long long now;
      
  // The skynet socket for receiving map deltas (from fusionmapper)
	int mapDeltaSocket = m_skynet.listen(SNfusiondeltamap, SNfusionmapper);
	if(mapDeltaSocket < 0)
		cerr << "CPlannerModule::getMapDeltasThread(): skynet listen returned error" << endl;

	int mapDeltaLeft;

	while(true)
	{
		int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pMapDelta, MAX_DELTA_SIZE, 0);

 		DGClockMutex(&m_mapMutex);
		cerr << "Received mapdelta" << endl;
		m_map.applyDelta<double>(m_mapLayer, m_pMapDelta, numreceived);
		cerr << "Applied mapdelta" << endl;
 		DGCunlockMutex(&m_mapMutex);

		// set the condition to signal that the first delta was received
		if(!m_bReceivedAtLeastOneDelta)
			DGCSetConditionTrue(m_bReceivedAtLeastOneDelta, m_deltaReceivedCond, m_deltaReceivedMutex);

    DGCgettime(now);
    delta_log << now << " " << numreceived << endl;
	}
}

void CPlannerModule::PlanningLoop(void) 
{
	CPlanner			planner(&m_map, m_mapLayer,
												&m_rddf, STATICMAP == 1);
	int						trajSocket, trajSocketSeed, trajSocketInterm;
	int						planresult;
	CTraj         prevSuccessfulTraj;
	CTraj*        pCurTraj;
	unsigned long long time1, time2;


	DGCgettime(time1);

	bool bSentTraj = false;

	if (NOMM)
		trajSocket     = m_skynet.get_send_sock(SNtraj);
	else
		trajSocket     = m_skynet.get_send_sock(SNplannertraj);

	trajSocketSeed   = m_skynet.get_send_sock(SNtrajPlannerSeed);
	trajSocketInterm = m_skynet.get_send_sock(SNtrajPlannerInterm);

	cout << "Waiting for a delta map" << endl;

	// don't try to plan until at least a single plan is received
	DGCWaitForConditionTrue(m_bReceivedAtLeastOneDelta, m_deltaReceivedCond, m_deltaReceivedMutex);

	ofstream planfile("logs/pmplans.dat");

  while( !QUIT_PRESSED )
	{
		if(!NOSTATE) 
		{
		  UpdateState();
		}

#ifdef NO_LOOKAHEAD

#if 0
	double ststate[]=
{3834528.7036177786067, 1.9625494408429147164, -0.12031789308008045136,
442624.57282297825441, 0.75617731768330342934, 0.21415203500229329503,
0.1120598116244396758, 0.18951424501995356442}
;

	
	m_state.Northing = ststate[0];
	m_state.Easting  = ststate[3];
	m_state.Vel_N = ststate[1];
	m_state.Vel_E = ststate[4];
	m_state.Acc_N = ststate[2];
	m_state.Acc_E = ststate[5];
	m_state.Yaw = atan2(m_state.Vel_E, m_state.Vel_N);
	m_state.YawRate = (m_state.Vel_N*m_state.Acc_E - m_state.Acc_N*m_state.Vel_E) / (m_state.Vel_N*m_state.Vel_N + m_state.Vel_E*m_state.Vel_E);
	m_state.Vel_D = 0.0;
#endif


		DGClockMutex(&m_mapMutex);
		cout << "starting to plan" << endl;
		planresult = planner.plan(&m_state);
		cout << "finished plan" << endl;
		DGCunlockMutex(&m_mapMutex);
#else
		if(!bSentTraj)
		{
			DGClockMutex(&m_mapMutex);
			cout << "starting to plan" << endl;
			planresult = planner.plan(&m_state);
			cout << "finished plan" << endl;
			DGCunlockMutex(&m_mapMutex);
		}
		else
		{
			VehicleState lookaheadState = m_state;
			double lookaheadDist = PLANNING_LOOKAHEAD * m_state.Speed2();
			int closestpoint = planner.getTraj()->getClosestPoint(m_state.Northing, m_state.Easting);
			if( hypot(planner.getTraj()->getNorthing(closestpoint) - m_state.Northing,
								planner.getTraj()->getEasting(closestpoint) - m_state.Easting) < PROXIMITY_TO_TRAJ_TO_LOOKAHEAD)
			{
				closestpoint += lround( lookaheadDist / planner.getLength() * (double)planner.getTraj()->getNumPoints());

				if(closestpoint >= planner.getTraj()->getNumPoints())
					closestpoint = planner.getTraj()->getNumPoints()-1;

				lookaheadState.Northing = planner.getTraj()->getNorthingDiff(closestpoint, 0);
				lookaheadState.Easting  = planner.getTraj()->getEastingDiff (closestpoint, 0);
				lookaheadState.Vel_N    = planner.getTraj()->getNorthingDiff(closestpoint, 1);
				lookaheadState.Vel_E    = planner.getTraj()->getEastingDiff (closestpoint, 1);
				lookaheadState.Acc_N    = planner.getTraj()->getNorthingDiff(closestpoint, 2);
				lookaheadState.Acc_E    = planner.getTraj()->getEastingDiff (closestpoint, 2);
				lookaheadState.Yaw      = atan2(lookaheadState.Vel_E, lookaheadState.Vel_N);
				lookaheadState.YawRate  = (lookaheadState.Vel_N*lookaheadState.Acc_E -
																	 lookaheadState.Acc_N*lookaheadState.Vel_E) / lookaheadState.Speed2()/lookaheadState.Speed2();
			}

			DGClockMutex(&m_mapMutex);
			cout << "starting to plan" << endl;
			planresult = planner.plan(&lookaheadState);
			cout << "finished plan" << endl;
			DGCunlockMutex(&m_mapMutex);
		}
#endif

		SendTraj(trajSocketSeed,   planner.getSeedTraj());
		SendTraj(trajSocketInterm, planner.getIntermTraj());

		if(planresult == 0 || planresult == 4 || planresult == 9)
		{
			cerr << "succeeded: " << planresult << endl;
			prevSuccessfulTraj = *planner.getTraj();
			pCurTraj = &prevSuccessfulTraj;
		}
		else
		{
			cerr << "failed: " << planresult << endl;
			planner.getVProfile(&prevSuccessfulTraj);
			pCurTraj = planner.getTraj();
		}

		SendTraj(trajSocket, pCurTraj);
		pCurTraj->print(planfile);
		planfile << endl;

		double n   = pCurTraj->getNorthingDiff(0, 0);
		double nd  = pCurTraj->getNorthingDiff(0, 1);
		double ndd = pCurTraj->getNorthingDiff(0, 2);
		double e   = pCurTraj->getEastingDiff (0, 0);
		double ed  = pCurTraj->getEastingDiff (0, 1);
		double edd = pCurTraj->getEastingDiff (0, 2);
		cerr <<   "desired a: "   << (nd*ndd + ed*edd)/hypot(nd,ed)
				 << ", desired phi: " << asin( VEHICLE_WHEELBASE*(nd*edd - ndd*ed)/pow(nd*nd+ed*ed, 1.5) ) << endl;

		bSentTraj = true;

		DGCgettime(time2);
		if(time2 - time1 < MIN_PLANNING_CYCLETIME_US)
			usleep(MIN_PLANNING_CYCLETIME_US - (time2 - time1));
		DGCgettime(time1);
	}

	cerr << endl;
}
