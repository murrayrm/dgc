#error "sn_msg.c is obsolete!  Use sn_msg.cc instead"

#include "sn_msg.h"
#include <netinet/in.h>
#include <assert.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <strings.h>

/* this code assumes that unsigned int is 32 bits long */
int sn_key;   //this only works for statically linked programs
/* TODO: create a struct containing sn_key that is passed to every sn function call.
 * Then create a C++ interface which will be a skynet object and have sn_key as
 * a private variable and call these c functions internally.
 */

int sn_register(modulename myname, int key)
{
  sn_key = key; 
  /* TODO */
  return 0;
}

int get_dg_addr(int sn_key, sn_msg type, struct in_addr* my_addr) 
{
  assert(sizeof(unsigned int) == 4);

  struct in_addr min_addr, max_addr;
  unsigned int min_addr_int, max_addr_int, ret_addr_int;
  if (inet_aton(MIN_ADDR, &min_addr) == 0)
  {
    perror("get_dg_addr:inet_aton");
    return 1;
  }

  if (inet_aton(MAX_ADDR, &max_addr) == 0)
  {
    perror("get_dg_addr:inet_aton");
    return 1;
  }

  min_addr_int = ntohl(min_addr.s_addr);
  max_addr_int = ntohl(max_addr.s_addr);

  /* Compute the address to used based on the message type and key.  this way,
     modules with different keys will never communicate with each other, and
     messages of different types can be filtered based on addres.  using
     last_type this way is a hack that relies on last_type being assigned the
     highest value of all types.  Though it is a hack, it will be obsolete once
     the rc file stuff is programmed, so I'll leave it for now
  */

  ret_addr_int = min_addr_int +(sn_key * last_type + type) % 
    (max_addr_int - min_addr_int);

  (*my_addr).s_addr = htonl(ret_addr_int);

  assert(IN_MULTICAST(ntohl((*my_addr).s_addr)));

  return 0;
}

int sn_listen(sn_msg mytype, modulename somemodule)
{
  /* 1:   socket
     2:   setsockopt SO_REUSEADDR
     3:   setsockopt IP_ADD_MEMBERSHIP
     4:   bind
     5:   optional: connect, if you only want to listen to a certain modulename
     TODO: step 5 and ID discrimination will require skynetd and registration

  */
  struct sockaddr_in localAddr;
  localAddr.sin_family=AF_INET;
  localAddr.sin_port=htons(dg_port); 
  bzero(&localAddr.sin_zero, 8);

  if (get_dg_addr(sn_key, mytype, &localAddr.sin_addr) != 0)
  {
    perror("sn_listen: get_dg_addr");
    exit(1);
  }

  int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  if (sockfd == -1)
  {
    perror("sn_listen: socket");
    exit(1);
  }

  //Allow multiple sockets on same computer to listen to same port
  u_int yes=1;
  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) <0)
  {
    perror("Reusing addr failed");
    exit(1);
  }

  struct ip_mreq my_mreq;
  my_mreq.imr_multiaddr = localAddr.sin_addr;
  my_mreq.imr_interface.s_addr = htonl(INADDR_ANY);
  
  if (setsockopt(sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (void *) &my_mreq, 
    sizeof(my_mreq)))
  {
    perror("setsockopt");
    exit(1);
  }

/* trying to bind to the mcast address instead of INADDR_ANY might not work.
   in that case, do localAddr.sin_addr.s_addr=htonl(INADDR_ANY)
*/

  if(bind(sockfd,(struct sockaddr *) &localAddr, sizeof(localAddr))<0) 
  {
    printf("%s: cannot bind port %d\n", inet_ntoa(localAddr.sin_addr), dg_port);
    exit(1);
  }

/* TODO connect if you want to, to discriminate against sending modulename and ID */

  return sockfd;
}

size_t sn_get_msg(int socket, void* mybuf, size_t bufsize, int options)
{
 //TODO: implement nonblocking calls
 return recv(socket, mybuf, bufsize, 0); 
}

int sn_get_send_sock(sn_msg type)
{
  /* 1:   socket
     2:   bind? it isn't neccesary.  i don't know what it gets you
     3:   connect
  */
  int sockfd;
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd == -1) 
    {
      perror("socket");
      exit(1);
    }

  struct sockaddr_in multi_addr ;
  multi_addr.sin_family = AF_INET ;
  multi_addr.sin_port = htons(dg_port);
  //inet_aton(MADDR, &(multi_addr.sin_addr));
  bzero(&multi_addr.sin_zero, 8);
  if (get_dg_addr(sn_key, type, &multi_addr.sin_addr) != 0)
  {
    perror("sn_get_send_sock: get_dg_addr");
    exit(1);
  }

 
  int connect_suceed =
    connect(sockfd, (struct sockaddr*)&multi_addr, sizeof(struct sockaddr));
  if ( connect_suceed == -1 ) 
  {
    perror("connect");
    exit(1);
  }

  return sockfd;
}

size_t sn_send_msg(int socket, void* msg, size_t msgsize, int options)
{
  //TODO: implement options
  return send(socket, msg, msgsize, 0);
}


  /* sn_send_atomic */

  size_t sn_send_atomic(int socket, void* msg, size_t msgsize, int options)
  {
    perror("sn_send_atomic: unimplemented feature");
    exit(1);
    return 0;
  }

  size_t sn_get_atomic(int socket, void* mybuf, size_t bufsize)
  {
    perror("sn_get_atomic: unimplemented feature");
    exit(1);
    return 0;
  }
