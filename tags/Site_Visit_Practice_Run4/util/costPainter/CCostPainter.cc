#include "CCostPainter.hh"

CCostPainter::CCostPainter() {
  _inputMap = NULL;

  _costLayerNum = 0;
  _elevLayerNum = 0;
  _corridorLayerNum = 0;
}


CCostPainter::~CCostPainter() {

}


int CCostPainter::initPainter(CMap* inputMap, int costLayerNum, int elevLayerNum, int corridorLayerNum) {
  _inputMap = inputMap;
  _costLayerNum = costLayerNum;
  _elevLayerNum = elevLayerNum;
  _corridorLayerNum = corridorLayerNum;
//  _blurredLayerNum = blurredLayerNum;

  _numRows = _inputMap->getNumRows();
  _numCols = _inputMap->getNumCols();

  return 0;
}

int CCostPainter::paintChanges(double UTMNorthing, double UTMEasting, int useGrad) {
  if(_inputMap->checkBoundsUTM(UTMNorthing, UTMEasting) == CMap::CM_OK) {
    double maxVelo, currentVelo, newVelo;
    double maxGradient;
    double corridorReference;
    int maxLocalRow, maxLocalCol;
    int minLocalRow, minLocalCol;
  //  int maxGlobalRow, maxGlobalCol;
   // int minGlobalRow, minGlobalCol;
    int cellRow, cellCol;

    //First, set us some limits
    _inputMap->UTM2Win(UTMNorthing, UTMEasting, &cellRow, &cellCol);
    if(cellRow < _numRows-1) {
      maxLocalRow = cellRow+1;
    } else {
      maxLocalRow = cellRow;
    }

    if(cellCol < _numCols-1) {
      maxLocalCol = cellCol+1;
    } else {
      maxLocalCol = cellCol;
    }

    if(cellRow>0) {
      minLocalRow = cellRow-1;
    } else {
      minLocalRow = cellRow;
    }

    if(cellCol > 0) {
      minLocalCol = cellCol-1;
    } else {
      minLocalCol = cellCol;
    }

    double currentElev, northElev, southElev, eastElev, westElev;    

    currentElev = _inputMap->getDataWin<double>(_elevLayerNum, cellRow, cellCol);
    northElev = _inputMap->getDataWin<double>(_elevLayerNum, maxLocalRow, cellCol);
    southElev = _inputMap->getDataWin<double>(_elevLayerNum, minLocalRow, cellCol);
    eastElev = _inputMap->getDataWin<double>(_elevLayerNum, cellRow, maxLocalCol);
    westElev = _inputMap->getDataWin<double>(_elevLayerNum, cellRow, minLocalCol);
	double noData = _inputMap->getLayerNoDataVal<double>(_elevLayerNum);
	if(northElev !=  noData)
	{
		northElev = fabs( northElev-currentElev);
	}
	else
	{
		northElev = 0;
	}
	
	if(southElev != noData )
	{
		southElev = fabs( southElev-currentElev);
	}
	else
	{
		southElev = 0;
	}
	if(eastElev != noData )
	{
		eastElev = fabs( eastElev-currentElev);
	}
	else
	{
		eastElev = 0;
	}
	if(westElev != noData )
	{
		westElev = fabs( westElev-currentElev);
	}
	else
	{
		westElev = 0;
	}



    maxGradient = maxGradientCalc(northElev,
				  southElev,
				  eastElev,
				  westElev);

    //maxVelo = generateVeloFromGrad(maxGradient);

    //int r = cellRow;
    //int c= cellCol;
    for(int r=minLocalRow; r<=maxLocalRow; r++) {
      for(int c=minLocalCol; c<=maxLocalCol; c++) {
	currentVelo = _inputMap->getDataWin<double>(_costLayerNum, r, c);
	corridorReference = _inputMap->getDataWin<double>(_corridorLayerNum, r, c);
	
	//if(currentElev=123.456) {
	if(maxGradient>0.3) {
	  maxVelo=0.001;
	} else {
	  maxVelo = corridorReference;
	}
	if(useGrad==0) {
	  maxVelo=0.001;
	}
	
//   if(corridorReference != _inputMap->getLayerNoDataVal<double>(_corridorLayerNum) || corridorReference != _inputMap->getLayerOutsideMapVal<double>(_corridorLayerNum)){
	if(currentVelo != maxVelo){
	  _inputMap->setDataWin_Delta<double>(_costLayerNum, r, c, maxVelo);
	  //         }
	}
      }
    }
/*
    double finalVelo = 0;
    double b = 1;

    const int gaussianWidth = 5;
    int windowSize=(gaussianWidth-1)/2;
    double kernel[gaussianWidth][gaussianWidth];
    double gaussianSum = 0;
    for(int i=0; i<gaussianWidth; i++) {
      for(int j=0; j<gaussianWidth; j++) {
	kernel[i][j] = exp(-b)*(pow((double)(i-windowSize),2)+pow((double)(j-windowSize),2));
	gaussianSum+=kernel[i][j];
      }
    }

    for(int i=0; i<gaussianWidth; i++) {
      for(int j=0; j<gaussianWidth; j++) {
	kernel[i][j] = kernel[i][j]/gaussianSum;
      }
    }


    minGlobalRow = cellRow-windowSize;
    minGlobalCol = cellCol-windowSize;
    maxGlobalRow = cellRow+windowSize;
    maxGlobalCol = cellCol+windowSize;

    if(minGlobalRow < 0) minGlobalRow = 0;
    if(minGlobalCol < 0) minGlobalCol = 0;
    if(maxGlobalRow > _numRows-1) maxGlobalRow = _numRows-1;
    if(maxGlobalCol > _numCols-1) maxGlobalCol = _numCols-1;
    

    for(int row = minGlobalRow; row<=maxGlobalRow; row++) {
      for(int col = minGlobalCol; col<=maxGlobalCol; col++) {
	minLocalRow = row-windowSize;
	minLocalCol = col-windowSize;
	maxLocalRow = row+windowSize;
	maxLocalCol = col+windowSize;
	
	if(minLocalRow < 0) minLocalRow = 0;
	if(minLocalCol < 0) minLocalCol = 0;
	if(maxLocalRow > _numRows-1) maxLocalRow = _numRows-1;
	if(maxLocalCol > _numCols-1) maxLocalCol = _numCols-1;
	
	finalVelo = 0;

	for(int r=minLocalRow; r<=maxLocalRow; r++) {
	  for(int c=minLocalCol; c<=maxLocalCol; c++) {
	    finalVelo += kernel[row][col]*_inputMap->getDataWin<double>(_costLayerNum, row, col);
	  }
	}
	_inputMap->setDataWin_Delta<double>(_blurredLayerNum, row, col, finalVelo);
      }
    }

*/
    //printf("Out of bounds: (%lf, %lf)!\n", UTMNorthing, UTMEasting);
  }
  return 0;
}


double CCostPainter::generateVeloFromGrad(double gradient) {
  double temp = fabs(gradient);
  double someConst = 1;
  double maxSpeedAt30 = 5;
  double firstLimit = 0.5;
  double secondLimit = 1;
  if( temp < firstLimit)
    return 55-temp/((55-maxSpeedAt30)/firstLimit);
  if( temp >= firstLimit && temp <= secondLimit)
    return maxSpeedAt30- pow((temp-firstLimit), 2)/someConst;
  if( temp > secondLimit)
    return 0;
}


double CCostPainter::maxGradientCalc(double g1, double g2, double g3, double g4) {
  double temp1, temp2;
  temp1 = max(g1,g2);
  temp2 = max(g3,g4);
  return max(temp1, temp2);
}
