#ifndef __STEREOPROCESS_H__
#define __STEREOPROCESS_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <opencv/cv.h>
#include <opencv/cvaux.h>
#include <opencv/highgui.h>

#include <svs/src/svsclass.h>
#include <svs/samples/flwin.h>

#include <stereovision/stereoSource.hh>
#include <frames/frames.hh>
#include <VehicleState.hh>


enum {
  stereoProcess_OK,
  stereoProcess_NO_FILE,
  stereoProcess_UNKNOWN_ERROR,

  stereoProcess_COUNT
};


class stereoProcess {
public:
  stereoProcess();
  ~stereoProcess();

  int init(int verboseLevel, char* SVSCalFilename, char* SVSParamsFilename, char* CamParamsFilename, 
	   char *baseFilename = "", char *baseFileType = "", int num = 0);

  int loadPair(stereoPair pair, VehicleState state);
  int loadPair(unsigned char *left, unsigned char *right, VehicleState state);

  int calcRect();
  int calcDisp();
  bool calc3D();
  
  int save();
  int save(char *baseFilename, char *baseFileType, int num);
  int saveDisp();
  int saveDisp(char *baseFilename, char *baseFileType, int num);
  int saveRect();
  int saveRect(char *baseFilename, char *baseFileType, int num);


  int show(int width=0, int height=0);
  int showDisp(int width=0, int height=0);
  int showRect(int width=0, int height=0);
  int showRectLeft(int width=0, int height=0);
  int showRectRight(int width=0, int height=0);


  unsigned char* disp();
  unsigned char* rectLeft();
  unsigned char* rectRight();
  stereoPair rectPair();

  int numPoints();
  bool validPoint(int i);
  XYZcoord UTMPoint(int i);
  XYZcoord Point(int i);
  bool SinglePoint(int x, int y, XYZcoord* resultPoint);
  bool SingleRelativePoint(int x, int y, XYZcoord* resultPoint);

  int pairIndex();  
  int resetPairIndex();
  VehicleState currentState();

  int numMinPoints;

  CvSize imageSize;
  CvRect subWindow;

  int corrsize();
  int thresh();
  int ndisp();
  int offx();
  int lr();
  int multi();

  int setSVSParams(int corrsize, int thresh, int ndisp, int offx, int lr, int multi, int subwindow_x_off, int subwindow_y_off, int subwindow_width, int subwindow_height);

private:
  IplImage* stereoImages[MAX_CAMERAS];
  IplImage* tempImages[MAX_CAMERAS];

  //SVS variables
  svsStoredImages *videoObject;
  svsMultiProcess *processObject;
  svsStereoImage *imageObject;

  //Transformation variables
  frames cameraFrame;
  
  VehicleState _currentState;

  char _currentFilename[256];
  char _currentFileType[10];

  int _currentSourceType;
  int _pairIndex;

  int _verboseLevel;


  svsWindow *rectLeftWindow;
  svsWindow *rectRightWindow;
  svsWindow *dispWindow;

  int showingDisp;
  int showingRectLeft;
  int showingRectRight;
  
};

#endif  //__STEREOPROCESS_H__
