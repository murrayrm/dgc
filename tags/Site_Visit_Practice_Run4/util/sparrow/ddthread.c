/*
 * ddthread - start display manager as a thread 
 *
 */

#include <stdlib.h>
#include <pthread.h>
#include "display.h"

void *dd_loop_thread(void *arg)
{
  dd_loop();
  return NULL;
}
