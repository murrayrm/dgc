function result = calcRangeMeasurement(terrain, vehicle, sensor)    
    v_to_g_rot_matrix = getRotMatrix(vehicle.orientation(1), vehicle.orientation(2), vehicle.orientation(3));
    sensor_location = vehicle.position + v_to_g_rot_matrix*sensor.offset;      
    switch sensor.type
        case 0
            s_to_v_rot_matrix = getRotMatrix(sensor.orientation(1), sensor.orientation(2), sensor.orientation(3));
            range_end_point_location = vehicle.position+v_to_g_rot_matrix*(sensor.offset + s_to_v_rot_matrix*[sensor.max_range 0 0]');   
            m = (sensor_location(3)-range_end_point_location(3))/(sensor_location(1)-range_end_point_location(1));
            b = (sensor_location(1)*range_end_point_location(3)-range_end_point_location(1)*sensor_location(3))/(sensor_location(1)-range_end_point_location(1));
            result = calcRayIntersection(terrain, vehicle, sensor, m, b);
        case 1
            for i=1:sensor.num_pixels                          
                sensor_orientation = sensor.orientation(1)+(i-1)/(sensor.num_pixels-1)*sensor.fov - sensor.fov/2;
                s_to_v_rot_matrix = getRotMatrix(sensor_orientation, sensor.orientation(2), sensor.orientation(3));
                range_end_point_location = vehicle.position+v_to_g_rot_matrix*(sensor.offset + s_to_v_rot_matrix*[1 0 0]');
                m = (sensor_location(3)-range_end_point_location(3))/(sensor_location(1)-range_end_point_location(1));
                b = (sensor_location(1)*range_end_point_location(3)-range_end_point_location(1)*sensor_location(3))/(sensor_location(1)-range_end_point_location(1));
                result(i) = calcRayIntersection(terrain, vehicle, sensor, m, b);
            end            
        otherwise
            sprintf('Unknown sensor type')
    end