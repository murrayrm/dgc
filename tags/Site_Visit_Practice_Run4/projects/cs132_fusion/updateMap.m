function map = updateMap(map, orig_point, orig_covar, sensor, vehicle, options)
    numPoints = length(orig_covar);
    if nargin<6
        options(1) = 1/map.res;
    end
    if isempty(orig_point)
        map = map;
        return;
    end
    switch map.type
        case -1
            for j=1:numPoints        
                point = orig_point(:,j);
                covar = orig_covar{j};
                cellNum = getCellNum(map, point);            
                if 0<cellNum & cellNum<length(map.vals)
                    map.vals(cellNum,2) = point(3);
                end
            end
        case 0
            for j=1:numPoints        
                point = orig_point(:,j);
                covar = orig_covar{j};
                cellNum = getCellNum(map, point);   
                if 0<cellNum & cellNum<length(map.vals)
                    if map.vals(cellNum,2) > point(3) | map.vals(cellNum,3)<0
                        map.vals(cellNum, 2) = point(3);
                        map.vals(cellNum, 3) = 0;
                    end
                end
            end
        case 1
            for j=1:numPoints        
                point = orig_point(:,j);
                covar = orig_covar{j};
                cellNum = getCellNum(map, point);
                cells_to_update = getCellsToUpdate(map, point, covar, options(1));
                for i=cells_to_update
                    map.vals(i,:) = getKalmanUpdate(map, i, point, covar);
                end
            end
        otherwise
            sprintf('Unknown map type')
    end    