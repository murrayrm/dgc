function result = plotSensor(sensor, vehicle)
    s_to_v_rot_matrix = getRotMatrix(sensor.orientation(1), sensor.orientation(2), sensor.orientation(3));
    v_to_g_rot_matrix = getRotMatrix(vehicle.orientation(1), vehicle.orientation(2), vehicle.orientation(3));
    hold_status = ishold;    
    switch sensor.type
        case 0
            sensor_location = vehicle.position + v_to_g_rot_matrix*sensor.offset;
            range_end_point_location = vehicle.position+v_to_g_rot_matrix*(sensor.offset + s_to_v_rot_matrix*[sensor.max_range 0 0]');
            plot(sensor_location(1), sensor_location(3), '>');            
            hold on;
            scanline(:,1) = sensor_location;
            scanline(:,2) = range_end_point_location;
            plot(scanline(1,:), scanline(3,:), '-.');
        case 1
            sensor_location = vehicle.position + v_to_g_rot_matrix*sensor.offset;
            plot(sensor_location(1), sensor_location(3), '<');
            hold on;
            scanline(:,1) = sensor_location;
            range_end_point_location1 = vehicle.position+v_to_g_rot_matrix*(sensor.offset + s_to_v_rot_matrix*[2*cos(-sensor.fov/2) 0 2*sin(-sensor.fov/2)]');            
            scanline(:,2) = range_end_point_location1;
            plot(scanline(1,:), scanline(3,:), '-.');
            range_end_point_location2 = vehicle.position+v_to_g_rot_matrix*(sensor.offset + s_to_v_rot_matrix*[2*cos(sensor.fov/2) 0 2*sin(sensor.fov/2)]');
            scanline(:,2) = range_end_point_location2;
            plot(scanline(1,:), scanline(3,:), '-.');
        otherwise
            sprintf('Unknown sensor type')
    end
    view(0,-90);
    resetHoldStatus(hold_status);
