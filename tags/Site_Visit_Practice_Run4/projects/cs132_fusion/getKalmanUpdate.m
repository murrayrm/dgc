function result = getKalmanUpdate(map, i, point, covar)
    P_ij = map.vals(i,3);
    z_ij = map.vals(i,2);
    z_m = point(3);
    mvnpdfval = mvnpdf([map.vals(i,1) point(3)]', point([1 3]), covar)*map.res;
    if mvnpdfval ~= 0
        sigma_m = 1/(sqrt(2*pi)*mvnpdfval);
    else
        sigma_m = 0;
    end
    if (map.vals(i,3)>0)
        map.vals(i, 2) = (P_ij*z_m+sigma_m*z_ij)/(P_ij+sigma_m);
        map.vals(i, 3) = P_ij*sigma_m/(P_ij+sigma_m);
        map.vals(i,3);
    else
        map.vals(i,2) = z_m;
        map.vals(i,3) = sigma_m;
    end
    result = map.vals(i,:);