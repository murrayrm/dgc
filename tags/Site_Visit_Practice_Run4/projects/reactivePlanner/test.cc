#include <fstream>
#include <iostream>
#include <sys/time.h>
#include "frames/coords.hh"
#include "trajMap.hh"
#include "rddf.hh"
#include "CCorridorPainter.hh"
#include "reactivePlanner.hh"
#include "reactiveModule.hh"
using namespace std;

int main()
{
  timeval t0, t1, t2, t3, t4;

  gettimeofday(&t0, NULL);

  reactivePlanner rP;
  int layer, blurLayer;
  CTraj *traj = new CTraj(3);
  RDDF rddf("rddf.dat");
  NEcoord initialPoint(rddf.getWaypointNorthing(0), rddf.getWaypointEasting(0));
  CMap map;
  CCorridorPainter paint;
  ofstream outfile("output.traj");

  map.initMap(0., 0., 200, 200, 1., 1., 0);
  layer = map.addLayer<double>(1e-3, 1e-4);
  blurLayer = map.addLayer<double>(1e-3, 1e-4);
  paint.initPainter(&map, layer, &rddf, 1., 1e-3, 10., 7., 0.25, 0);
  map.updateVehicleLoc(initialPoint.N, initialPoint.E);
  NEcoord exposedRowBox[4];
  NEcoord exposedColBox[4];
  map.getExposedRowBoxUTM(exposedRowBox);
  map.getExposedColBoxUTM(exposedColBox);
  paint.paintChanges(exposedRowBox, exposedColBox);

  gettimeofday(&t1, NULL);
  
  int i, j, k, f, r, c, n, e;

  n = (int)(BLUR_SIZE / map.getResRows() + 0.5);
  e = (int)(BLUR_SIZE / map.getResCols() + 0.5);
  if(n > 0 || e > 0)
  {
    double goodness;

    r = map.getNumRows() - n;
    c = map.getNumCols() - e;

    for(i = n; i < r; i++)
      for(j = e; j < c; j++)
      {
        goodness = map.getDataWin<double>(layer, i, j);
        if(goodness >= 0.01)
          for(k = i - n; k <= i + n; k++)
            for(f = j - e; f <= j + e; f++)
              goodness = fmin(goodness, map.
                  getDataWin<double>(layer, k, f));
        map.setDataWin<double>(blurLayer, i, j, goodness);
      }
  }

  gettimeofday(&t2, NULL);

  traj = rP.plan(&rddf, &map, layer, initialPoint, rddf.getTrackLineYaw(0),
      1.e10);

  gettimeofday(&t3, NULL);

  traj->print(outfile);

  gettimeofday(&t4, NULL);

  cout << "Init time: " << t1.tv_sec - t0.tv_sec +
      (double)(t1.tv_usec - t0.tv_usec) / 1000000 << " s" << endl;
  cout << "Blur time: " << t2.tv_sec - t1.tv_sec +
      (double)(t2.tv_usec - t1.tv_usec) / 1000000 << " s" << endl;
  cout << "Run time: " << t3.tv_sec - t2.tv_sec +
      (double)(t3.tv_usec - t2.tv_usec) / 1000000 << " s" << endl;
  cout << "Output time: " << t4.tv_sec - t3.tv_sec +
      (double)(t4.tv_usec - t3.tv_usec) / 1000000 << " s" << endl;
  cout << "Total time: " << t4.tv_sec - t0.tv_sec +
      (double)(t4.tv_usec - t0.tv_usec) / 1000000 << " s" << endl;
  return 0;
}
