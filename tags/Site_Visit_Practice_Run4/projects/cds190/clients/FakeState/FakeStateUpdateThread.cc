/* AState.cc - new vehicle state estimator
 *
 * JML 31 Dec 04
 *
 * */

#include "FakeState.hh"

void FakeState::Update_thread()
{
  while(1)
  {
    UpdateState();
    Broadcast();
  }
}
