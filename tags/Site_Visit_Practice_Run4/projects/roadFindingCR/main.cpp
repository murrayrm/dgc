//----------------------------------------------------------------------------
//  Road follower module
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//----------------------------------------------------------------------------

#include "decs.h"

//----------------------------------------------------------------------------

extern IplImage *gabor_max_response_index;
extern IplImage *gabor_max_response_intensity;
extern int show_dominant_orientations;

extern IplImage *candidate_image;
extern int show_vanishing_point;
extern int show_particles;
extern int show_support;

//----------------------------------------------------------------------------

int win_w, win_h;                     // this really should be just window--not image size
int image_number;                     // how many frames processed so far
double *stage_time;                   // how many seconds spent on each stage of image processing
double image_time;                    // how many seconds spent processing this frame (sum of stage times)
IplImage **source_im;                 // directly from the image source
IplImage *target_im;                  // image source after pyramid step
CR_Movie *movie_source;               // if the source is a movie

int num_pyramid_levels                = 2;    // makes 720 x 480 image -> 360 x 240, or the latter -> 180 x 120, etc.

int source_type                       = SOURCE_TYPE_LIVE;
char source_path[256]                 = "../road_data/fitz/dvData_25_clip1.avi";
//char source_path[256]                 = "../road_data/fitz/cmu_canyonsmall.avi";
int do_display                        = TRUE;
int report_interval                   = 10;
int do_timing                         = FALSE;
int print_image_number                = FALSE;

// source = images in a directory

int do_reset                          = FALSE;   // reset tracker parameters before next image is loaded?
int image_num_iterations              = 1;       // tracker iterations per image

int image_cur_iteration;
int image_imnum;
int image_startnum                    = 0;
int image_endnum;    
char *image_filename;
char logstate_filename[256];
FILE *image_filelist_fp;
FILE *logstate_fp;
int image_num;
int save_image_num                    = -1;
char *imname;

float tracker_confidence;

//----------------------------------------------------------------------------

void display()
{
  if (show_vanishing_point)
    glDrawPixels(candidate_image->width, candidate_image->height, GL_LUMINANCE, GL_FLOAT, candidate_image->imageData);
  else if (show_dominant_orientations)
    glDrawPixels(gabor_max_response_intensity->width, gabor_max_response_intensity->height, GL_LUMINANCE, GL_UNSIGNED_BYTE, gabor_max_response_intensity->imageData);
  else
    glDrawPixels(target_im->width, target_im->height, GL_RGB, GL_UNSIGNED_BYTE, target_im->imageData);

  // vertical midline

  glColor3f(0,1,1);
  glLineWidth(1);
  glBegin(GL_LINES);
  glVertex2f(win_w / 2, 0);
  glVertex2f(win_w / 2, win_h);
  glEnd();


  float x, y;
  get_tracker_position(&x, &y);
  draw_vanishing_point_support(x, y, win_w, win_h);


  draw_tracker(win_w, win_h);           // estimate smoothed by particle filtering

  glutSwapBuffers();
}

//----------------------------------------------------------------------------

void mouse(int button, int state, int x, int y)
{
  // cycle between raw image view, dominant orientation view, and vote view

  if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
    if (!show_dominant_orientations && !show_vanishing_point)
      show_dominant_orientations = TRUE;
    else if (show_dominant_orientations && !show_vanishing_point) {
      show_dominant_orientations = FALSE;
      show_vanishing_point = TRUE;
    }
    else if (!show_dominant_orientations && show_vanishing_point)
      show_vanishing_point = FALSE;
  }

  // cycle between raw image view, tracker particles, and support rays

  else if (button == GLUT_RIGHT_BUTTON && state == GLUT_UP) {
    if (!show_particles && !show_support)
      show_particles = TRUE;
    else if (show_particles && !show_support) {
      show_particles = FALSE;
      show_support = TRUE;
    }
    else if (!show_particles && show_support)
      show_support = FALSE;
  }

  // save whatever the display is currently showing to file

  else if (button == GLUT_MIDDLE_BUTTON && state == GLUT_UP) {

    glReadPixels(0, 0, target_im->width, target_im->height, GL_RGB, GL_UNSIGNED_BYTE, target_im->imageData);
 
    sprintf(imname, "../mviv_data/%i.png", image_number);
    cvConvertImage(target_im, target_im, CV_CVTIMG_SWAP_RB | CV_CVTIMG_FLIP);  // RGB->BGR	
    cvSaveImage(imname, target_im);
  }
}

//----------------------------------------------------------------------------

// when the window is hidden -- needed if you want to use OpenGL functions without a window

void fake_display() { }

//----------------------------------------------------------------------------

static double start, finish, difference;
struct timeval tv;

void init_per_image()
{
  int i, stage;

  // start timing

  if (do_timing && !(image_number % report_interval)) 
    start = (double) cvGetTickCount() / cvGetTickFrequency();

  // acquire image

  // next AVI frame

  if (source_type == SOURCE_TYPE_AVI) {
    if (image_number < movie_source->lastframe)
      get_frame_CR_Movie(movie_source, image_number);
    else
      finish_run();
  }

  // live capture

  else if (source_type == SOURCE_TYPE_LIVE) {
    capture_firewire_image(source_im[0], 0);
  }

  // images in a directory (in whatever order "ls" results in)

  else if (source_type == SOURCE_TYPE_IMAGE) {

    if (image_num_iterations < 0 || image_cur_iteration < image_num_iterations - 1) 
      image_cur_iteration++;
    else {   // get rid of old image; load new one

      if (do_reset)
	reset_tracker();
        
      cvReleaseImage(&source_im[0]);

      if (image_imnum >= image_endnum || !read_next_image(image_filelist_fp, &source_im[0])) {
	printf("done\n");
	exit(1);
      }

      image_cur_iteration = 0;
      image_imnum++;
    }
  }
  else
    CR_error("Unknown image source");

  // "pyramidize" acquired image

  for (i = 0; i < num_pyramid_levels - 1; i++) 
    cvPyrDown(source_im[i], source_im[i + 1]);

  // calculate dominant orientations

  compute_dominant_orientations(target_im);

  // find vanishing point

  compute_vanishing_point(target_im->width, target_im->height);

  // track vanishing point

  update_tracker();

  // confidence in [0, 1] range that we are actually on or near a road

  tracker_confidence = compute_onoff_confidence(candidate_image);

  printf("%f\n", tracker_confidence);

  // end timing

  if (do_timing && !(image_number % report_interval)) {
    finish = (double) cvGetTickCount() / cvGetTickFrequency();
    printf("%.3lf fps\n", 1000000.0/(finish - start));
  }

  if (print_image_number)
    printf("%i\n", image_number);

  image_number++;
}

//----------------------------------------------------------------------------

void process_image()
{
  //  printf("%i\n", image_number);

  init_per_image();

  if (do_display)
    glutPostRedisplay();
}

//----------------------------------------------------------------------------

void finish_run()
{
  exit(1);
}

//----------------------------------------------------------------------------

// position file pointer at beginning of line n

int goto_image(FILE *fp, int n)
{
  int i;

  for (i = 0; i < n; i++) {
    if (!fgets(image_filename, 80, fp)) {
      printf("no such line in file\n");
      exit(1);
    }
  }
}

//----------------------------------------------------------------------------

// fp is a file containing a full-path filename of an input image on each line
// assumes image_filename already allocated
// returns TRUE if filename gotten; FALSE if EOF encountered
 
int read_next_image(FILE *fp, IplImage **im)
{
  if (!fgets(image_filename, 80, fp))
    return FALSE;
  image_filename[strlen(image_filename)-1] = '\0';
  //  printf("%s\n", image_filename);

  *im = cvLoadImage(image_filename);
  //  printf("w = %i, h = %i\n", (*im)->width, (*im)->height);
  cvConvertImage(*im, *im, CV_CVTIMG_SWAP_RB);  // RGB->BGR
  return TRUE;
}

//----------------------------------------------------------------------------

void init_per_run(int argc, char **argv)
{
  int i;

  //-------------------------------------------------
  // initialize image source
  //-------------------------------------------------

  source_im = (IplImage **) calloc(num_pyramid_levels, sizeof(IplImage *));
  
  if (source_type == SOURCE_TYPE_AVI) {
    movie_source = open_CR_Movie(source_path);
    source_im[0] = movie_source->im;
  }
  else if (source_type == SOURCE_TYPE_LIVE) {
    int w, h;
    //    initialize_capture_firewire(&w, &h);
    //    if (!initialize_capture_firewire(&w, &h,  MODE_320x240_YUV422)) {
    if (!initialize_capture_firewire(&w, &h,  MODE_640x480_MONO)) {
      printf("no firewire cameras attached\n");
      exit(1);
    }
    source_im[0] = cvCreateImage(cvSize(w, h), IPL_DEPTH_8U, 3);
  }
  else if (source_type == SOURCE_TYPE_IMAGE) {

    image_cur_iteration = 0;
    image_filename = (char *) calloc(256, sizeof(char));

    // generate this file with "find <path> | sort > filename.txt" (and remove first line of file)

    image_filelist_fp = fopen(source_path, "r");
    if (!image_filelist_fp) {
      printf("no such file list\n");
      exit(1);
    }

    goto_image(image_filelist_fp, image_startnum);
    image_imnum = image_startnum;
    if (!read_next_image(image_filelist_fp, &source_im[0])) {
      printf("no images in file list\n");
      exit(1);
    }
  }
  else
    CR_error("No image source set!\n");

  for (i = 1; i < num_pyramid_levels; i++)
    source_im[i] = cvCreateImage(cvSize(source_im[i - 1]->width / 2, source_im[i - 1]->height / 2), IPL_DEPTH_8U, 3);

  target_im = source_im[num_pyramid_levels - 1];

  //-------------------------------------------------
  // miscellaneous
  //-------------------------------------------------

  imname = (char *) calloc(256, sizeof(char));

  //-------------------------------------------------
  // initialize OpenGL
  //-------------------------------------------------

  if (do_display) {

    win_w = target_im->width;
    win_h = target_im->height;

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(win_w, win_h); 
    glutInitWindowPosition(100, 100);
    glutCreateWindow("road");
    
    glutIdleFunc(process_image);

    glutDisplayFunc(display); 
    glutMouseFunc(mouse); 
    
    glPixelZoom(1, -1);
    glRasterPos2i(-1, 1);
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, win_w, 0, win_h);
  }
  else {

    win_w = target_im->width;
    win_h = target_im->height;

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(win_w, win_h); 
    glutInitWindowPosition(100, 100);
    glutCreateWindow("RoadVP");
    
    glutIdleFunc(process_image);

    glutDisplayFunc(fake_display); 
    glutHideWindow();
  }

  //-------------------------------------------------
  // timing 
  //-------------------------------------------------

  image_number = image_startnum;

  //-------------------------------------------------
  // random number generation
  //-------------------------------------------------

  initialize_CR_Random();

  //-------------------------------------------------
  // dominant orientation estimation
  //-------------------------------------------------

  initialize_dominant_orientations(target_im->width, target_im->height);

  //-------------------------------------------------
  // vanishing point finding
  //-------------------------------------------------

  initialize_vanishing_point_estimation(target_im->width, target_im->height);

  //-------------------------------------------------
  // tracking vanishing point
  //-------------------------------------------------

  initialize_tracker();

  //-------------------------------------------------
  // on vs. off road confidence
  //-------------------------------------------------

  initialize_onoff_confidence(0.1, 300, 0.667);

  //-------------------------------------------------
  // start OpenGL GLUT
  //-------------------------------------------------

  glutMainLoop();
}

//----------------------------------------------------------------------------

void process_command_line_flags(int argc, char **argv)
{
  int i;

  // general variables

  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i],                             "-p"))
      num_pyramid_levels = atoi(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-image")) {
      source_type = SOURCE_TYPE_IMAGE;
      strcpy(source_path, argv[i + 1]);     // full path name of text file with one image name (full path) per line
      image_startnum = atoi(argv[i + 2]);   // which line in that file to start reading from
      image_endnum = atoi(argv[i + 3]);     // which line to stop reading at
    }
    else if (!strcmp(argv[i],                        "-avi")) {
      source_type = SOURCE_TYPE_AVI;
      strcpy(source_path, argv[i + 1]);
    }
    else if (!strcmp(argv[i],                        "-live")) 
      source_type = SOURCE_TYPE_LIVE;
    else if (!strcmp(argv[i],                        "-time")) 
      do_timing = TRUE;
    else if (!strcmp(argv[i],                        "-nodisplay")) {
      do_display = FALSE;
      win_w = win_h = 1;
    }
    else if (!strcmp(argv[i],                        "-printnum")) 
      print_image_number = TRUE;
    else if (!strcmp(argv[i],                        "-doreset")) 
      do_reset = TRUE;
    else if (!strcmp(argv[i],                        "-save")) 
      save_image_num = atoi(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-iterations")) 
      image_num_iterations = atoi(argv[i + 1]);
  }

  // specialized variables

  process_dominant_orientations_command_line_flags(argc, argv);
  process_vanishing_point_command_line_flags(argc, argv);
  process_tracker_command_line_flags(argc, argv);
}

//----------------------------------------------------------------------------

int main(int argc, char **argv)
{
  process_command_line_flags(argc, argv);
  init_per_run(argc, argv);

  return 1;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
