#!/bin/zsh
# zsh is needed for floating point arithmetic

MIN_RATE=8192
MAX_RATE=100000000   #100MBs
TEST_TIME=1         #seconds each test
ECHO="/bin/echo"   #don't use zsh builtin echo  
RESDIR=$HOSTNAME-$2-`date --iso-8601`
RESFILE="result.csv" #output here

if (( $# == 2 ));
then SLAVE=$2; KEY=$1;
else
  $ECHO "usage: main-snm.sh key slave"
  $ECHO "example: main-snm.sh 587 cobalt.caltech.edu"
  $ECHO "make sure skynetd is running on both computers"
  exit
fi

function do1test 
{
  $1 $3 > markpipe &
  slaveppid=$!
  #output should be sent_messages real_rate real_time
  SEND_DATA=`ssh $SLAVE $2 $3 $4 $5 $6`
  wait $slavepid
  RECV_MSG=`cat markpipe`
  rm markpipe
  $ECHO $RECV_MSG $SEND_DATA
}

function tput121
{
  do1test ./single-throughput-slave dgc/projects/skynetmark/single-throughput-master $1 $2 $3 $4  
}

function lat121
{
  do1test ./single-latency-slave dgc/projects/skynetmark/single-latency-master $1 $2 $3 $4
}

function dotput121test
{
  #x = pkt size , y = rate , z = percent received
  #$ECHO "1->1 throughput test" >> $RESFILE
  rm -f $RESFILE
  for (( i=1; i<= 65536; i = 2 * i )) #loop on msg_size
    do FAILED=0
    for (( j=MIN_RATE; j<=MAX_RATE; j = j*2 )) #loop on rate
      do 
      if (( j < $i / TEST_TIME ))
        then $ECHO $i $j 100 >> $RESFILE
      else 
        if (( $FAILED == 0 ))
        then result=`tput121 $KEY $i $j $TEST_TIME`
        RECV_MSG=`$ECHO $result | cut -d ' ' -f 1`
        SENT_MSG=`$ECHO $result | cut -d ' ' -f 2`
        SENT_TIME=`$ECHO $result | cut -d ' ' -f 4`
        $ECHO result=$result
        $ECHO SENT_TIME=$SENT_TIME
        $ECHO TEST_TIME=$TEST_TIME
        #TODO: use bc for arithmetic to handle floats
          if (( $SENT_TIME < $TEST_TIME+1 ))          #messages sent in timely fashion
            then $ECHO $i $j $(( 100.*$RECV_MSG/$SENT_MSG )) >> $RESFILE
            else $ECHO $i $j 0  >> $RESFILE                  #sender cannot meet rate request
            FAILED=1
          fi
        else $ECHO $i $j 0 >> $RESFILE
        fi
      fi
    done
    $ECHO >> $RESFILE
  done
}

function dolat121test
{
  rm -f $RESFILE
  for (( i=1; i<= 65536; i = 2 * i )) #loop on msg_size
    do $ECHO -n $i ' ' >> $RESFILE
    result=`lat121 $KEY $i $TEST_TIME`
    $ECHO `$ECHO $result | cut -d ' ' -f 4-6` >> $RESFILE
  done
}

install -m 755 -d $RESDIR

RESFILE=$RESDIR/lat121.dat
dolat121test
RESFILE=$RESDIR/tput121.dat
dotput121test
RESFILE=$RESDIR/tput121local.dat
SLAVE="localhost"
dotput121test 
RESFILE=$RESDIR/lat121local.dat
SLAVE="localhost"
dolat121test 
