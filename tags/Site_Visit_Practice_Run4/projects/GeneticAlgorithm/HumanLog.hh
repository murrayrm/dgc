#ifndef HUMANLOG_HH
#define HUMANLOG_HH
#include "vehlib/VDrive.hh" // VDrive_CmdMotionMsg sent to actuators
#include <deque>
using namespace std;

/**
 * A class to handle the reading of the human commands that is parsed from the
 * arbiter log. Each time getCommand is called, a steer value and a speed value
 * is picked from the log.
 * $Id$
 */
class HumanLog
{
public:
  /**
   * Creates a Human driver log class
   */
  HumanLog(char* filename);

  /**
   * Gets a decision for a specific timestamp of a human driver.
   * OBSERVE: the VDrive_CmdMotionMsg contains two attributes:
   * - velocity_cmd in m/s
   * - steer_cmd in the interval[-1, 1] where -1 is the maximum left steering
   *   and 1 is the maximum right steering!
   * 
   */
  VDrive_CmdMotionMsg getCommand();

  /** method to check if we have more steer commands in our log 
   * @return true if we have more steer commands available
   */
  bool hasMore() { return steerLog.size() > 0; }
  
  /** Returns the number of remaining votes 
   * @return the number of remaining votes
   */
  int getNbrCmds() { return steerLog.size(); }
  
private:
  /** The steer value log */
  deque<double> steerLog;
  /** The speed value log */
  deque<double> speedLog;
};

#endif

