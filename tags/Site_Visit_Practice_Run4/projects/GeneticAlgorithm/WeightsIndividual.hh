#ifndef WEIGHTSINDIVIDUAL_HH
#define WEIGHTSINDIVIDUAL_HH
#include "Individual.hh"
#include "Arbiter2/ArbiterDatum.hh" // ArbiterInput::Count;

/** plus/minus this value is max change for mutation */
const double WEIGHT_MUTATE_MAX = 0.2; 

/** An Individual implementation that solves the problem of finding out good
 * weights to make the arbiter imitate a human's decisions when deciding which
 * voters to pay more attention to. The different weights may only have a 
 * value in the range [0,1].
 * $Id$
 */
class WeightsIndividual : public Individual
{
public:
  /**
   * Creates a WeightsIndividual
   */
  WeightsIndividual();

  /**
   * Evaluates which steering/speed value we shall fill in our 
   * VDrive_CmdMotionMsg
   */
  void evaluate();

  /**
   * Returns the fitness for this individual (the sum inverted)
   * @return the fitness
   */
  double getFitness() const;

  /**
   * Returns a complete copy of this individual (must be deleted later)
   * @return a copy of this object
   */
  Individual* clone();

  /**
   * Mutates the properties of this individual by multiplying each weight with
   * plus/minus WEIGHT_MUTATE_MAX.
   */
  void mutate();

  /**
   * Makes a cross-over change of the properties of this individual
   * from the current properties and properties from and another.
   * The cross-over modification does the following:
   * - a random number of weights is considered for selection and is copied over
   *   to the new inidividual
   * - the amount of number of weights is copied from the other parent to 
   *   the new individual (this object) while the other weights becomes the ones
   *   that the first parent has (this individual).
   */
  void crossOver(Individual* theOtherParent);

  /**
   * Prints this individual
   */
  void print();

  /**
   * Returns the weights
   * @return the weights
   */
  double* getWeights() { return w; }

  
private:
  /** the weights */
  double w[ArbiterInput::Count];
  /** a random value in the range 0.0 - 1.0 */
  double randomValue(); 
  /**  a check so that the individual is evaluated before getting fitness */
  bool isEvaluated;
  /** because VoteHandler requires that for the votepicking, ugly fix? */
  double lastHumanPhi;
};

#endif

