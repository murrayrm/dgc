
//#ifndef LATERAL_HH
//#define LATERAL_HH

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include "sys/time.h"
#include <unistd.h>
#include <cmath>

// TODO -- FIX PATHS!  Kludgy.
#include "MTA/Modules.hh"
#include "MTA/Kernel.hh"
#include "MTA/Misc/Time/Time.hh"
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include "waypoints.hh"
#include "rddf.hh"
#include "vehlib/VState.hh"
#include "vehlib/VDrive.hh"

// Eric's.
#define PATH_SWITCH_VAR 10 // meters from the waypoint before we go to next
                          // path segment
#define CONSTANT_SPEED 5 // m/s
#define SMALL_ACCELERATION 1 // m/s^2

// Jeff's.
#define MAX_LATERAL_ERROR 10
#define LATERAL_GAIN 0.6
#define LATERAL_PROPORTIONAL_GAIN 0.22
#define HEADING_PROPORTIONAL_GAIN 0.0
#define LATERAL_INTEGRATOR_GAIN 0.05
#define HEADING_INTEGRATOR_GAIN 0.0
#define LATERAL_DERIVATIVE_GAIN 1.5
#define HEADING_DERIVATIVE_GAIN 0.0

#define LATERAL_INTEGRATOR_SAT 1.3
#define HEADING_INTEGRATOR_SAT 100

// Eric's code starts here

struct pathSegment {
  double northingStart;
  double eastingStart;
  double northingEnd;
  double eastingEnd;
  double heading;
  int count;
};

class lateralController : public DGC_MODULE {
public:
  lateralController();

  void initializePath(char *);
  void updateState();
  int updatePathSegment();
  void Active();
  void stopTheCar();

  double distPointAlongLine();
  double distPointToLine();
  double headingError();
  double PIDController();

  VState_GetStateMsg state;
  VDrive_CmdSteerAccel u;
  RDDF lateralPath;
  pathSegment p;

  ~lateralController();
};
//#endif
