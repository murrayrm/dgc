/*
 * brake.h - header file for brake actuation
 * 
 * Sue Ann Hong, Jeremy G.
 *
 * RMM, 2 Jan 04: changes to incorporate brake acutation into vdrive
 *   * merged TLL.h into this file; may split back out later
 *   * set up to use local version of serial.h; elimited Constants.h
 *
 * WHN, 1-30-04: adapted to new device driver function naming convention
 */

//TBD: finish functions like status

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>

#ifndef SERIAL2
#include "serial.h"
#else
#include "serial2.hh"
#endif

#include "vehports.h"
#include "parallel.h"        // compile with ../parallel/parallel.c

//Brake Constants
#define BRAKE_ID 16
#define BRAKE_BAUD B57600
// TODO: THIS IS SLIGHTLY DIRTY.  but we want to keep all constants,
// etc in Constants.h

//Brake Message Constants
#define BRAKE_ACK "* 10\n"

//Brake Movement Constant Values **_ABS commands used for hardware stop on actuator
#define BRAKE_STOP_ENABLE_ABS 0
#define BRAKE_STOP_STATE_ABS 0

//Max acc and vel are in the native units of the brake controller. The hardware maximum 
//for accelleration is 1073741824, but the motor can actually only physically achieve 
//about 10737418, but I am not sure if an error will result if you specify an accelleration
//that is too high (although I don't think so) so we cut this down a little to be safe.
//We specify here the max and min allowed absolute acceleration from 1 to 10000000.  
//These are used to scale the 0-1 inputs to the correct units.
//NOTE: there is a value above which you WILL have errors, and it is somewher between 5 and 10
//million.  TBD: what is it?
#define BRAKE_MAX_ACC 5000000.0
#define BRAKE_MIN_ACC 1.0

//The hardware maximum for velocity is +/-2147483647 which corresponds directly to +/-4000 RPM.
//We specify here the max and min allowed absolute velocity from 0 to 2147483647.
//These are used to scale the 0-1 inputs to the correct units
#define BRAKE_MAX_VEL 2147483647.0
#define BRAKE_MIN_VEL 1.0

/* Position constants for set_brake_abs_pos(). All should be in counts.
 * ZERO_POS: 
 * MAX_POS: Corresponds to our_max_pos, just in counts. Maximum position used
 *          in set_brake_abs_pos(). Same role as max_count used in
 *          set_brake_abs_pos_pot().
 * MIN_POS:  
 *
*/

#define BRAKE_POS_TOLERANCE .04  // =tolerance (out of 0-1),
//this is a desensitized zone that prevents the brake from hunting.
//note that changing this will require a commensurate change in BRAKE_VEL
//and BRAKE_ACC in vdrive.

/* Parallel Port set-up
 Gas:
   Data lines (in): none
   Control lines (out):
  pin14  not used
  pin01  digipot CLK
  pin16  digipot direction (UD)
  pin17  digipot CS [pin only used by this mode]
*/

/* Constants */
// Brake actuator pot control lines
#define BPOT_RC PP_PIN01
#define BPOT_CS PP_PIN16
#define BPOT_D8 PP_PIN10
#define BPOT_D9 PP_PIN11
#define BPOT_D10 PP_PIN12
#define BPOT_D11 PP_PIN13
#define BT_OE PP_PIN14
// Brake act pot constants
#define BPOT_TBUSY 8              // max t_BUSY in us
#define BP_CORR_FACTOR -0.06862 // correction for error in adc
#define BP_NUM_DIV 2047        // 12-bit signed ADC => 11-bit positive
#define BP_VREF_LOW 0         // in V, according to ADC chip spec
#define BP_VREF_HIGH 10        // in V, according to ADC chip spec
#define POT_VHIGH 9.71         // 9.73V according to multimeter
                                        // 9.71V (1988,1987) according to s/w
#define BP_NUM_HIGH 1987       /* Highest number output by ADC box,
                                         * Corresponds to POT_VHIGH */
/* BP_MIN & BP_MAX: Used in getBrakePot() to scale brake pot reading. 
 *                  These values provide no safety. If want safety, then should
 *                  adjust min and max values in higher brake functions like 
 *                  set_brake_pos_abs_pot(), etc. (Jan 17, 2004)
 * Current Usage: getBrakePot()
 */
#define BP_MIN 0.0
#define BP_MAX 1.0


//moved the extern brake_lock 

//Below are the common driver functions

/*break_open()
 Opens the brake port at port, then it starts the break control loop code from brake.c 
in its own thread, so the brake is ready to receive code.  NOTE: to run the brake successfully,
the parallel port that controls the throttle, brake pot, transmission, and ignition must
be opened first.  throttle_open must be called before brake movement is attempted.
*/
int brake_open(int port); 

/*brake_zero()
 Just sets the brake to the calibrated 0 position.
*/
int brake_zero();

/*brake_calibrate()
 Calibrates the maximum and minimum pot positions min_pot and max_pot to the limit
 switch positions.  NOTE: currently there are no limit switches, so this just calls brake zero
 TBD: build and integrate these limit switches.
*/
int brake_calibrate();

//returns a 0 for OK, else returns -1
int brake_ok();

//Arguments for brake_status, valued to facilitate using bit masks to determine which data to return
//user must simply pass in the sum of the data elements he wishes returned to the struct
#define BRAKE_POS 1
#define BRAKE_ENABLED 2
#define BRAKE_MOVING 4
#define BRAKE_OK 8
#define BRAKE_COMMAND_POS 16
#define BRAKE_ALL (BRAKE_POS + BRAKE_ENABLED + BRAKE_MOVING + BRAKE_OK + BRAKE_COMMAND_POS)

//The struct for the status return
struct brake_status_data {
  int status;
};

//Request status report of type request_type to be returned at *brake_status_data which is zeros except
//for the data requested.  Note that this function will currently not return a struct.  Call brake_ok
//to get that info and break_read to get a position.
int brake_status(int request_type, struct brake_status_type *brake_status_data);

//Suspend brake operation upon a pause estop command

//int brake_pause(double pos);
int brake_pause();

//Suspend brake operation upon a estop disable command
int brake_disable();

//Resume brake operation after a pause command
int brake_resume();

//Closes the port, unallocates memory, stops threads
int brake_close();

//Below are some brake specific functions, the brake_get_pot function is included at the end of this file

//Resets the brake and waits for a response. Will timeout and return -1 after 1 minute.  
//Called in brake_open
int brake_reset();

//Resets the brake actuator when the rst button on the vdrive interface is pushed,
//hopefully will stop the whine that the brake actuator soes sometimes.  We are not
//sure of the cause of the whine, but power cycling the actuator and restarting vdrive 
//seems to fix it, so this command mimics that in software.
int brake_fault_reset();

/* brake_read()
 * Description: Returns the current brake position in the 0-1 scale from the
 *  pot data.  If there is an error, the argument is not changed, and
 *  brake_read returns -1.  if the read is successful, it returns 0.
 */
double brake_read();



/* brake_write()
 * Inputs: abs_pos   0 = calibrated zero (no braking)
 *                   1 = fully retracted (full braking)
 *         vel, acc  0 to 1
 * Description: Sets the brake actuator, taking the first argument as the
 *  desired position in the absolute scale, defined by constants set in
 *  calib_brake(). 'abs_pos' is on the 0-1 scale, and goes from no braking to
 *  full braking (It does NOT go from actuator fully out to fully in, unless
 *  that's how the actuator was calibrated.)  This function uses the velocity
 *  mode movement command to move the brake so that we can get a reasonably
 *  paced actuator movement.
 */
//int brake_write(double abs_pos, double vel, double acc);
int brake_write(double abs_pos, double vel, double acc);

/* start_break_control_loop()
 * Description: Forks off the brake control loop which updates the commanded 
 * brake position at 10Hz.  It also uses the velocity mode movement commands 
 * for the actuator to allow near real-time monitoring of new commanded brake
 * positions.
 */
void *brake_start_control_loop(void* brake_enabled);

/* brake_send_command()
 * Description: Sends the brake a command of command_num with parameters of
 *  length
 */
int brake_send_command(int command_num, char *parameters, int length);

/* brake_receive_command()
 * Return value: The response from sending a brake command
 * Description: Sends the brake a command of command_num with parameters of
 *  length, and then stores the response in buffer of length buf_length (or
 *  times out in timeout seconds)
 */
int brake_receive_command(int command_num, char *parameters, int length,
			  char *buffer, int buf_length, int timeout);

// Helper functions for scaling and limiting numbers
double brake_in_range(double val, double min_val, double max_val);
double brake_scale(double val, double old_min_val, double old_max_val, double new_min_val, double new_max_val);

double brake_get_pot();



