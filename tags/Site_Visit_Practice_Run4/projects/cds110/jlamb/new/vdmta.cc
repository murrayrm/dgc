/*
 * vdmta.cc - MTA interface for vdrive
 * 
 * RMM, 1 Jan 04
 *
 * This file contains all of the MTA interface routines required for 
 * vdrive.  For right now, this is a just a copy of Ike's test example,
 * to see if I can get everything to compile and run.
 */

//WNH 2-22-04, Added many functions that will be needed to transfer messages 
//between the new human interface vremote and vdrive.

#include <time.h>
#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>

#include "MTA/Kernel.hh"
#include "MTA/Misc/Time/Time.hh"
#include "MTA/Misc/Time/Timeval.hh"

#include "sparrow/dbglib.h"  
#include "vdrive.h"		// function prototypes for vdrive.c 
#include "steer.h"		// function prototypes
#include "brake2.h"		// function prototypes
#include "throttle.h"		// function prototypes

#include "vdmta.hh"
#include "VState.hh"
#include "VStarPackets.hh"

extern struct VState_GetStateMsg vehstate; // vehicle state
extern struct VDrive_CmdMotionMsg mtacmd; // commanded motion from vdrive.c
// direct (accel instead of speed) commanded motion from vdrive.c
extern struct VDrive_CmdSteerAccel mtacmd_direct; 

int mta_counter = 0;
int vstate_count = 0;		/* keep track of good VState packets */
int vmanage_count = 0;		// keep track of good VManage packets 
int MTA_VDRIVE_VALID = false;

extern int ACCEL_PID;
extern int STEER_PID;

using namespace std;

Timeval lastAutonomousCommand = Timeval(0,0);

VDrive::VDrive() : DGC_MODULE(MODULES::VDrive, "Vehicle Drive", 0) {}
VDrive::~VDrive() {}

void VDrive::Active() 
{
  D.MyCounter=0;

  // activate vdrive
  MTA_VDRIVE_VALID = true;

  while( ContinueInState() ) // while stay active
  {
    D.MyCounter++;
    dbg_info("VDrive active; count = %d", D.MyCounter);

    /* Send a message requesting position from VState */
    Mail msg = NewQueryMessage(MyAddress(), MODULES::VState, 
   			     VStateMessages::GetState);
    Mail reply = SendQuery(msg);
    reply.DequeueFrom(&vehstate, sizeof(vehstate));
    if (reply.OK()) ++vstate_count; else vstate_count = -1;


    // check to see if the autonomous command has timed out
    if( TVNow() - lastAutonomousCommand > Timeval( 1, 0)) 
    {
      mtacmd.velocity_cmd = 0;
    }

    mta_counter++;
    usleep(10000);
  }
} 

void VDrive::InMailHandler(Mail& msg) 
{
  switch (msg.MsgType()) 
  {
    // Input Commands
    // ***********************************************************************


    case VDriveMessages::CmdMotion:
      /* Get the motion data and store it locally.  This is used to send 
       ** commands to steering and brake in autonomous mode. */
      msg.DequeueFrom(&mtacmd, sizeof(mtacmd));
      lastAutonomousCommand = TVNow();

      // new, wake up accel thread
      if( ACCEL_PID > 0 ) 
      {
        kill( ACCEL_PID, SIGUSR1 );
      }
      if( STEER_PID > 0) 
      {
        kill( ACCEL_PID, SIGUSR1 );
      }
      break;

    /* Direct drive (command acceleration rather than speed). */
    case VDriveMessages::CmdDirectDrive:

      //printf("MESS.\n");
      
      /* Get the motion data and store it locally.  This is used to send 
       ** commands to steering and brake in autonomous mode. */
      msg.DequeueFrom(&mtacmd_direct, sizeof(mtacmd_direct));
      lastAutonomousCommand = TVNow();

      // new, wake up accel thread
      if( ACCEL_PID > 0 ) 
      {
        kill( ACCEL_PID, SIGUSR1 );
      }
      if( STEER_PID > 0) 
      {
        kill( ACCEL_PID, SIGUSR1 );
      }
      break;

    case VDriveMessages::SetAccelOff:
      // Sets accel_off to the value of the argument
      double offset1;
      extern double accel_off;
      msg.DequeueFrom(&offset1, sizeof(offset1));
      accel_off = offset1;
      break;

    case VDriveMessages::SetSteerOff:
      // Sets steer_off to the value of the argument
      double offset2;
      extern double steer_off;
      msg.DequeueFrom(&offset2, sizeof(offset2));
      steer_off = offset2;
      break;

    case VDriveMessages::SetSpeedRef:
      // Sets speed_ref to the value of the argument
      double ref;
      extern double speed_ref;
      msg.DequeueFrom(&ref, sizeof(ref));
      speed_ref = ref;
      break;

    case VDriveMessages::SetCruiseGain:
      // Sets cruise_gain to the value of the argument
      double gain;
      extern double cruise_gain;
      msg.DequeueFrom(&gain, sizeof(gain));
      cruise_gain = gain;
      break;

    // Calibrate Commands
    // ************************************************************************

    case VDriveMessages::ThrottleCalib:
      // Calibrates the throttle
      throttle_calibrate();
      break;

    case VDriveMessages::BrakeCalib:
      // Calibrates the brake
      brake_calibrate();
      break;

    case VDriveMessages::SteerCalib:
      // Calibrates the steering
      steer_calibrate();
      break;


    //Reset Commands
    // **********************************************************************

    case VDriveMessages::ThrottleReset:
      // Resets the throttle
      throttle_fault_reset(); 		
      break;

    case VDriveMessages::BrakeReset:
      // Resets the brake
      brake_fault_reset();
      break;

    case VDriveMessages::SteerReset:
      // Resets the steering
      steer_fault_reset();
      break;


    // Enable Commands
    // ***********************************************************************


    case VDriveMessages::CaptureEnable:
      // Enables or disables the capture mode, note that this command takes no argument
      // and simply toggles the capture mode off and on, unlike the other enable functions
      user_cap_toggle((long) 0);
      break;

    case VDriveMessages::ThrottleEnable:
      // Sets the throttle_flag variable in vdrive to the value of the argument
      // note that there is no error checking, ie any int argument will be sent
      int flag1;
      extern int throttle_flag;
      msg.DequeueFrom(&flag1, sizeof(flag1));
      throttle_flag = flag1;
      break;

    case VDriveMessages::BrakeEnable:
      // Sets the brake_flag variable in vdrive to the value of the argument
      // note that there is no error checking, ie any int argument will be sent
      int flag2;
      extern int brake_flag;
      msg.DequeueFrom(&flag2, sizeof(flag2));
      brake_flag = flag2;
      break;

    case VDriveMessages::SteerEnable:
      // Sets the steer_flag variable in vdrive to the value of the argument
      // note that there is no error checking, ie any int argument will be sent
      int flag3;
      extern int steer_flag;
      msg.DequeueFrom(&flag3, sizeof(flag3));
      steer_flag = flag3;
      break;

    case VDriveMessages::CruiseEnable:
      // Sets the cruise_flag variable in vdrive to the value of the argument
      // note that there is no error checking, ie any int argument will be sent
      int flag4;
      extern int cruise_flag;
      msg.DequeueFrom(&flag4, sizeof(flag4));
      cruise_flag = flag4;
      break;


      // Misc Commands
      // ***********************************************************************

    case VDriveMessages::SetMode:
      // Set the mode of vdrive e.g. off, remote, etc.
      int mode;
      msg.DequeueFrom(&mode, sizeof(mode));
      switch (mode) 
      {
              case 'o':      mode_off((long) 0);      break;
              case 'a':      mode_autonomous((long) 0);      break;
              case 'r':      mode_remote((long) 0);      break;
              case 'm':      mode_manual((long) 0);      break;
              case 't':      mode_test((long) 0);        break;    
      }
      break;

    case VDriveMessages::DoEstop:
      // Invokes the user_estop_call function, which toggles between estop_pause and estop_resume
      user_estop_toggle((long) 0);
      break;

    case VDriveMessages::DumpFilename:
      // Sets the name of the file to be used for the data capture dump
      char filename[FILENAME_MAX];
      extern char dumpfile[FILENAME_MAX];
      msg.DequeueFrom(&filename, sizeof(filename));
      for(int counter = 1; (counter <= FILENAME_MAX); counter++) 
      {
        dumpfile[counter] = filename[counter];
      }
      break;

    case VDriveMessages::SteerInc:
      // Increments the steering
      steer_inc((long) 0);
      break;

    case VDriveMessages::SteerDec:
      // Decrements the steering
      steer_dec((long) 0);
      break;

    case VDriveMessages::AccelInc:
      // Increments the acceleration (throttle + brake)
      accel_inc((long) 0);
      break;

    case VDriveMessages::AccelDec:
      // Decrements the acceleration (throttle + brake)
      accel_dec((long) 0);
      break;

    case VDriveMessages::FullStop:
      // Invokes a full stop (emergency stop)
      full_stop((long) 0);
      break;

    case VDriveMessages::UserQuit:
      // Quits Vdrive
      user_quit((long) 0);
      break;

    default:
      // let the parent mail handler handle it
      break;
  }
}

Mail VDrive::QueryMailHandler(Mail& msg) 
{
  Mail reply;

  switch (msg.MsgType()) 
  {
    case VDriveMessages::Get_VDriveStack:
    {
      /* Copy data from local variables to buffer and respond */
      reply = ReplyToQuery(msg);
      struct VDriveStack vd;

      Set_VDriveStack(vd);
      reply.QueueTo(&vd, sizeof(vd));
      break;
    }
    case VDriveMessages::Get_Steer:
    {
      /* Copy data from local variables to buffer and respond */
      reply = ReplyToQuery(msg);
      struct Steer_Packet sp;  

      Set_Steer_Packet(sp);
      reply.QueueTo(&sp, sizeof(sp));
      break; 
    }
    default:
      // let the parent mail handler handle it
      reply = DGC_MODULE::QueryMailHandler(msg);
      break;
  }
  return reply;
}
