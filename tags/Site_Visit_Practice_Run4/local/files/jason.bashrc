# Jason Yosinski's bashrc file
# placed here so I'll have it on all the computers

# firse source the team's bash file
source $HOME/dgc/local/files/dgc.bashrc

# do some aliases
alias la='ls -al --color=auto'
alias grep='grep --color'
alias grin='grep -in --color'
alias ssh='ssh -X'
alias key='echo $SKYNET_KEY'
alias ci='svn ci -m "'
#alias setkey='export $SKYNET_KEY'
alias em='emacs -nw'
alias x='xterm -fg white -bg black -fn andytype10f &'
alias src='source ~/.bashrc'
alias play='mpg123 -r 48000 -Z -C'
alias pl='mpg123 -r 48000 -Z -C -@'

# set up .emacs file
cp $HOME/dgc/local/files/dgc.emacs $HOME/.emacs
cat $HOME/dgc/local/files/jason.emacs >> $HOME/.emacs

# Change the window title of X terminals 
case $TERM in
	xterm*|rxvt|Eterm|eterm)
		PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/$HOME/~}\007"'
		;;
	screen)
		PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/$HOME/~}\033\\"'
		;;
esac

# export svn editor
export SVN_EDITOR=emacs

# example prompts
PS1='[\u@TEST \w]\n \#\$ \n\
\[\
\e[1mBold Text\e[m\n\
\e[4mUnderline Text\e[m\n\
\e[5mBlink Text\e[m\n\
\e[7mInverse Text\e[m\]\n\
Should be normal text
Foreground colors:
\[\
\e[0;30m30: Black\n\
\e[0;31m31: Red\n\
\e[0;32m32: Green\n\
\e[0;33m33: Yellow\Orange\n\
\e[0;34m34: Blue\n\
\e[0;35m35: Magenta\n\
\e[0;36m36: Cyan\n\
\e[0;37m37: Light Gray\Black\n\
\e[0;39m39: Default\n\
Bright foreground colors:
\e[1;30m30: Dark Gray\n\
\e[1;31m31: Red\n\
\e[1;32m32: Green\n\
\e[1;33m33: Yellow\n\
\e[1;34m34: Blue\n\
\e[1;35m35: Magenta\n\
\e[1;36m36: Cyan\n\
\e[1;37m37: White\n\
\e[0;39m39: Default\n\
\e[m\]Background colors:
\[\e[1;37m\e[40m40: Black\e[0;49m\n\
\e[41m41: Red\e[0;49m\n\
\e[42m42: Green\e[0;49m\n\
\e[43m43: Yellow\Orange\e[0;49m\n\
\e[44m44: Blue\e[0;49m\n\
\e[45m45: Magenta\e[0;49m\n\
\e[46m46: Cyan\e[0;49m\n\
\e[47m47: Light Gray\Black\e[0;49m\n\
\e[49m49: Default\e[m\]\n'



# ok, this is the right way to do a prompt...finally...
green='\[\033[01;32m\]'
blue='\[\033[01;34m\]'
default='\[\033[00;00m\]'

PS1="$green\
\u@\H \
$blue\
[\
$green\
\w\
$blue\
]\
$green\
 $ \
$default"
