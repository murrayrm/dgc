/*
 * ArbiterModuleDatum.hh
 * 
 * Who knows what's been done and when...
 */

#ifndef ARBITERMODULEDATUM_HH
#define ARBITERMODULEDATUM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "Vote.hh"
#include "SimpleArbiter.hh"

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"

// Send to actuators 
#include "vehlib/VDrive.hh"


using namespace std;

//-------Constants----------------------------------------------------------//
// Constants (shared with planners) MAX_VELO, MIN_VELO, MAX_SPEED have been
// moved to /team/include/Constants.h (2/26/04) And then deleted there at
// some point later.

//---Tweak-able constants for timing----------------------------------------//
// If an input has not been updated in a certain period
// of time it will not be considered valid.
const Timeval InputTimeout(1,0);
// Send commands to Vdrive if it's already been this long.
// (2/28/04, 100Hz, though genMap-using modules run too slowly for even 10Hz)
const Timeval CommandTimeout(0, 100000);
// Thresholds for determining if the new command is different enough from the 
// most recent command.
const double THRESHOLD_VELO_DIFF = 0.3;
const double THRESHOLD_STEER_DIFF = 1;

// Sleep amounts (in us)
// The sleep in command-argument handling. It's really so that we don't
// paint the whole display field, but for arbiter running rate reasons, we
// shouldn't technically have it. We don't use this for missing obstacle
// avoidance modules, since we want to check for them as quickly as possible,
// because we don't stop the vehicle in this case as in others.
// (Still we wait for SLEEP_ACTIVE_LOOP)
const long SLEEP_WAIT_FOR_MISSING_MODULES = 1000000;
// How much we sleep for in each iteration of the Active() loop.
// Mostly for sharing CPU with other processes and not frying MTA/Vdrive.
const long SLEEP_ACTIVE_LOOP = 50000;            // 20Hz
// Not really sleeping, but used for waiting before going into REVERSE.
const double SLEEP_WAIT_BEFORE_REVERSE = 12.0;    // in seconds
// How many times we want to get zero command decisions before going reverse.
// The duration of a PAUSE before REVERSE >= how long the arbiter
// sleeps in total per cycle * MAX_PAUSE_COUNT = SLEEP_WAIT_BEFORE_REVERSE *
// MAX_PAUSE_COUNT.
const int MAX_PAUSE_COUNT = (int)round( SLEEP_WAIT_BEFORE_REVERSE / 
                            ( (double)SLEEP_ACTIVE_LOOP / 1000000.0 ) );
// REVERSE_ONE_PERIOD is how long we command REVERSE commands to Vdrive for.
// However, currently there's a delay in switching to reverse in embedded
// level becuase we use the e-stop mechanism to do it. This delay is about
// 5 seconds, so the actual reverse time is REVERSE_ONE_PERIOD - 5 sec -
// time it takes to stop the vehicle.
const Timeval REVERSE_ONE_PERIOD(8,0);
const Timeval REVERSE_STOP_TIME(11,0);

//-----Tweak-able constants for "special" modes----------------------------//
// Number of past commands to remember
const int NUM2REMEMBER = 10;
// "Special" velocities
// Inching forward w/ no obstacle avoidance
const double NO_OBSTAVOID_VELO = MIN_SPEED;
// Reversing w/ no sensing whatsoever!
const double REVERSE_VELO = -MIN_SPEED; 


namespace ArbiterModes {
  enum {
    NORMAL,
    REVERSE,
    PAUSE,
    COUNT    // [place holder] the total number of modes.
  };
};

// Weights of each Voter, ranging from 0 to 1.
const double InputWeights[] = {
  1,   // LADAR
  1,   // Stereo
  1,   // StereoLR
  1,   // Global
  1,   // DFE -- weight for DFE doesn't matter.
  1,   // CorrArcEval
  1,   // FenderPlanner
  1,   // RoadFollower
  1,   // PathEvaluation

  0    // [place holder] for the total number of inputs, 'Count'.
};

// Enumerate all arbiter inputs 
// PLEASE do not change the order of these, the order is used to 
// identify log files.  Add new ones at the end instead.
namespace ArbiterInput {
  enum {
    LADAR,
    Stereo,
    StereoLR,
    Global,
    DFE,
    CorrArcEval,
    FenderPlanner,
    RoadFollower,
    PathEvaluation,

    Count    // [place holder] the total number of inputs.
  };
};

struct ArbiterDatum {

  // Added for logging 1/31/04
  VState_GetStateMsg SS;

  // for data logging 
  int TestNumber;
  string TestNumberStr;

  // for testing
  Timeval lastUpdateState;
  int UpdateCountState;

  // Time zero
  Timeval TimeZero;

  // Accel Data
  VDrive_CmdMotionMsg toVDrive;

  // voter array
  Voter allVoters[ArbiterInput::Count];
  Voter combined;
  // a timestamp for all vote sources
  Timeval lastUpdate[ArbiterInput::Count];
  // and a count of how many times a source has updated
  int UpdateCount[ArbiterInput::Count];
  int CombinedUpdateCount;

  // Keeping track of how long we've been in PAUSE mode.
  int pauseCounter;
  Timeval reverseBeginPeriodTime;
  // Keeping track of commands, for REVERSE
  int currCommandIndex;
  VDrive_CmdMotionMsg lastCommand[NUM2REMEMBER];

  // The current mode of arbiter.
  int ArbiterMode;

  // and a lock for the votes timestamps.
  boost::recursive_mutex voterLock;

};


#endif
