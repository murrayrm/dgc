#ifndef STATEFILTER_HH
#define STATEFILTER_HH

#include <math.h>

#include "gps.h"
#include "LatLong.h"
#include "VState.hh"


// CONSTANT DECLARATIONS
// obd2 related constant decs
#define OBD_CUTOFF .25 // m/s
#define OBD_VEL_ERR .05 // m/s
#define GPS_VEL_ERR 1 // m/s

// definitions for running without IMU
#define GPS_VEL_CUTOFF .5 // m/s

// detects a jump by looking at the heading, and comparing it to angle
// of displacement, if angle is off by more than 45 degs from the heading
// register a jump
double detect_jump(double cur_n, double cur_e, double update_time, 
	       VState_GetStateMsg vehstate);

// resets the jump detector when gps is lost
void detect_reset();

// 1-d kalman filter that calculates magnetometer error
void magfilter(double magheading, double gpsheading);

// bounds yaw by -pi to pi
double norm_yaw(double yaw);

// calculates running std. dev of gps 
int dev_reject(double northing, double easting);

// helper funcs for running std. dev
double get_avg();
double get_var();
void filter_reset();

// constant definitions
const double ANGLE_CUTOFF = M_PI/4;
//const double DV_WEIGHT = 1.5;

const int NUM_ELEMENTS = 5;
#define  SigmaMult 4
#define  INVALID_THRESHOLD 5 // 10 seconds

#endif
