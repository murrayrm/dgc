#include "VStarPackets.hh"
#include "VStarMessages.hh"
#include "VStarDevices.hh"


#include "MTA/Kernel.hh"
#include "MTA/Modules.hh"

// This file implements all of the helper functions in VStarPackets.hh

// Note: Documentation in this file will be pretty sparse.

const int ret_error = -1;
const int ret_ok    =  0;

int Get_Transmission_Packet( const MailAddress& myAddress, Transmission_Packet& tp) {

  Mail msg = NewQueryMessage( myAddress, MODULES::VManage, VManageMessages::Get_Transmission);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> tp; // download the packet
    return ret_ok; // we are good
  }
  return ret_error;  // else an error occured and data packet is not altered.
}


int Get_Ignition_Packet( const MailAddress& myAddress, Ignition_Packet& tp) {

  Mail msg = NewQueryMessage( myAddress, MODULES::VManage, VManageMessages::Get_Ignition);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> tp; // download the packet
    return ret_ok; // we are good
  }
  return ret_error;  // else an error occured and data packet is not altered.
}


int Get_EStop_Packet( const MailAddress& myAddress, EStop_Packet& tp) {

  Mail msg = NewQueryMessage( myAddress, MODULES::VManage, VManageMessages::Get_EStop);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> tp; // download the packet
    return ret_ok; // we are good
  }
  return ret_error;  // else an error occured and data packet is not altered.
}


int Get_OBD_Packet( const MailAddress& myAddress, OBD_Packet& tp) {

  Mail msg = NewQueryMessage( myAddress, MODULES::VManage, VManageMessages::Get_OBD);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> tp; // download the packet
    return ret_ok; // we are good
  }
  return ret_error;  // else an error occured and data packet is not altered.
}



int Get_Steer_Packet( const MailAddress& myAddress, Steer_Packet& tp) {

  Mail msg = NewQueryMessage( myAddress, MODULES::VDrive, VDriveMessages::Get_Steer);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> tp; // download the packet
    return ret_ok; // we are good
  }
  return ret_error;  // else an error occured and data packet is not altered.
}



int Get_Throttle_Packet( const MailAddress& myAddress, Throttle_Packet& tp) {

  Mail msg = NewQueryMessage( myAddress, MODULES::VDrive, VDriveMessages::Get_Throttle);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> tp; // download the packet
    return ret_ok; // we are good
  }
  return ret_error;  // else an error occured and data packet is not altered.
}


int Get_Brake_Packet( const MailAddress& myAddress, Brake_Packet& tp) {

  Mail msg = NewQueryMessage( myAddress, MODULES::VDrive, VDriveMessages::Get_Brake);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> tp; // download the packet
    return ret_ok; // we are good
  }
  return ret_error;  // else an error occured and data packet is not altered.
}



int Get_Joystick_Packet( const MailAddress& myAddress, Joystick_Packet& tp) {

  Mail msg = NewQueryMessage( myAddress, MODULES::VRemote, VRemoteMessages::Get_Joystick);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> tp; // download the packet
    return ret_ok; // we are good
  }
  return ret_error;  // else an error occured and data packet is not altered.
}

int Set_Transmission( const MailAddress& myAddress, Transmission_Packet& tp) {

  Mail msg = NewOutMessage( myAddress, MODULES::VManage, VManageMessages::Set_Transmission);
  if ( ! msg.OK()) return ret_error; // couldnt find module

  msg << tp;
  SendMail(msg);
  return ret_ok;
}


int Set_Ignition( const MailAddress& myAddress, Ignition_Packet& tp) {

  Mail msg = NewOutMessage( myAddress, MODULES::VManage, VManageMessages::Set_Ignition);
  if ( ! msg.OK()) return ret_error; // couldnt find module

  msg << tp;
  SendMail(msg);
  return ret_ok;
}


int Set_Keyboard_Pause( const MailAddress& myAddress, int tp) {
  Mail msg;
  // if tp set (i.e. set pause == ret_ok;
  if ( tp ) {
    msg = NewOutMessage( myAddress, MODULES::VManage, VManageMessages::Set_Keyboard_Pause);
  } else {
    msg = NewOutMessage( myAddress, MODULES::VManage, VManageMessages::Set_Keyboard_Resume);
  }
  if ( ! msg.OK()) return ret_error; // couldnt find module
  SendMail(msg);
  return ret_ok;
}



int Get_VM_for_VD_Packet( const MailAddress& myAddress, VM_for_VD_Packet& tp) {

  Mail msg = NewQueryMessage( myAddress, MODULES::VManage, VManageMessages::Get_VM_for_VD);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> tp; // download the packet
    return ret_ok; // we are good
  }
  return ret_error;  // else an error occured and data packet is not altered.
}




int Set_Steer_Mode( const MailAddress& myAddress, char tp) {

  Mail msg = NewOutMessage( myAddress, MODULES::VManage, VManageMessages::Set_Steer_Mode);
  if ( ! msg.OK()) return ret_error; // couldnt find module

  msg << tp;
  SendMail(msg);
  return ret_ok;
}



int Set_Accel_Mode( const MailAddress& myAddress, char tp) {

  Mail msg = NewOutMessage( myAddress, MODULES::VManage, VManageMessages::Set_Accel_Mode);
  if ( ! msg.OK()) return ret_error; // couldnt find module

  msg << tp;
  SendMail(msg);
  return ret_ok;
}



int Set_EStop_Behavior( const MailAddress& myAddress, VM_EStop_Behavior_Packet& tp) {

  Mail msg = NewOutMessage( myAddress, MODULES::VManage, VManageMessages::Set_EStop_Behavior);
  if ( ! msg.OK()) return ret_error; // couldnt find module

  msg << tp;
  SendMail(msg);
  return ret_ok;
}



int Get_VState_Packet( const MailAddress& myAddress, VState_Packet& tp) {

  Mail msg = NewQueryMessage( myAddress, MODULES::VState, VStateMessages::GetState);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> tp; // download the packet
    return ret_ok; // we are good
  }
  return ret_error;  // else an error occured and data packet is not altered.
}


int Get_VManageStack( const MailAddress& myAddress, VManageStack& tp) {

  Mail msg = NewQueryMessage( myAddress, MODULES::VManage, VManageMessages::Get_VManageStack);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> tp; // download the packet
    return ret_ok; // we are good
  }
  return ret_error;  // else an error occured and data packet is not altered.
}


int Get_VDriveStack( const MailAddress& myAddress, VDriveStack& tp) {

  Mail msg = NewQueryMessage( myAddress, MODULES::VDrive, VDriveMessages::Get_VDriveStack);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> tp; // download the packet
    return ret_ok; // we are good
  }
  return ret_error;  // else an error occured and data packet is not altered.
}


int Get_VStateStack( const MailAddress& myAddress, VStateStack& tp) {

  Mail msg = NewQueryMessage( myAddress, MODULES::VState, VStateMessages::Get_VStateStack);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> tp; // download the packet
    return ret_ok; // we are good
  }
  return ret_error;  // else an error occured and data packet is not altered.
}
