/*
 * joystick.h - header file for joystick interface
 *
 * RMM, 29 Dec 03
 * 
 */

#ifndef JOYSTICK_HH
#define JOYSTICK_HH

struct joy_data {
  int buttons;				/* button bitmask */
  double data[2];		/* joystick reading */
  double scale[2];		/* scaling factor of the joystick */
  double offset[2];		/* center location for the joystick */
};

int joy_open(int, struct joy_data *);
int joy_read(struct joy_data *);

#endif
