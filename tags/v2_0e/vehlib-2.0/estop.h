#ifndef ESTOP_H
#define ESTOP_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include "parallel.h"

#define DISABLE_PIN  PP_PIN08
#define PAUSE_PIN    PP_PIN07
#define DISABLE      1
#define PAUSE        1
#define DISABLE_FLAG 2
#define PAUSE_FLAG   1
#define NORMAL_FLAG  0
#define ERROR_FLAG   -1
#define ESTOP_PARALLEL_PORT 0  //this is assuming this will run on something
                               //other than titanium (its ports are full)

//initialize the parallel port for estop status check
int estop_open(int);

//get the status of the estop
//returns -1 error 0 normal 1 pause 2 disable
int estop_status();

#endif
