//this begins the transmission shifting and ignition program definitions
#include <stdio.h>
#include <time.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>

#include "steer.h"
#include "serial.h"
#include "sparrow/dbglib.h"
#include "MTA/Misc/Time/Time.hh"

using namespace std;

int trans_parallel_port = -1;
int trans_gear = NEUTRAL;
int run_status = -1;
int ign_status = -1;
#define trans_DB0  PP_PIN6
#define trans_DB1  PP_PIN5
#define trans_DB2  PP_PIN4
/* DB2 = 0, DB1 = 0, DB0 = 0 --> NEUTRAL
 * 0 0 0 --> NEUTRAL
 * 0 0 1 --> FREE
 * 0 1 0 --> FIRST
 * 0 1 1 --> SECOND
 * 1 0 0 --> THIRD
 * 1 0 1 --> REVERSE

NOTE: Transmission shifting may change to the following ...
NOTE: as of 02-23-04 2218, shifting is as follows
*  0 1 0 --> NEUTRAL
*  0 1 1 --> FIRST
*  1 0 0 --> THIRD
*  1 0 1 --> REVERSE
* this will be used if we decide not to use solenoids
 */

#define run        PP_PIN2
#define ign        PP_PIN3
#define vehicle_run 2
#define vehicle_start 3
#define vehicle_off 1

int trans_open(trans_parallel_port) {
  //if(pp_is_available(trans_parallel_port) != TRUE) return ERROR
  //pp_lock(trans_parallel_port);
  pp_init(trans_parallel_port, PP_DIR_OUT);
  trans_setposition(trans_gear);
  ignition_setposition(ignition_setposition);
  return TRUE;
}

int trans_close(void) {
  pp_end(trans_parallel_port);
  pp_unlock(trans_parallel_port);
  trans_parallel_port = -1;
  return TRUE;
}

/*****************************************************************
 * int trans_setposition(int)
 * author: Jeff Lamb (JCL)
 * date: 02-23-04
 * functionality:
 *   takes in a gear (defined in transmission.h) and outputs bits
 *   to the parallel port based on input
 * return values:  TRUE  if gear exists and parallel port can be opened
 *                 FALSE if port not available or gear invalid
 ***************************************************************/

int trans_setposition(trans_gear) {
  pp_lock(trans_parallel_port);
  //pp_init(trans_parallel_port, PP_DIR_OUT);
  switch(trans_gear) {
  case NEUTRAL: //shift to neutral gear
    pp_set_bits(trans_parallel_port, trans_DB1, trans_DB0 |  trans_DB2);
    break;
    //case FREE:    //shift to free (1st gear, solenoids off)
    //pp_set_bits(trans_parallel_port, trans_DB0, trans_DB1 | trans_DB2);
    //break;
  case FIRST:   //shift to first (1st gear, solenoids on)
    pp_set_bits(trans_parallel_port, trans_DB1, trans_DB0 | trans_DB2);
    break;
    //  case SECOND:  //shift to second gear
    //pp_set_bits(trans_parallel_port, trans_DB0 | trans_DB1, trans_DB2);
    //break;
  case THIRD:   //shift to third gear
    pp_set_bits(trans_parallel_port, trans_DB2, trans_DB0 | trans_DB1);
    break;
  case REVERSE: //shift to reverse
    pp_set_bits(trans_parallel_port, trans_DB0 | trans_DB2, trans_DB1);
  case default: //else, failure, return error
    pp_unlock(trans_parallel_port);
      return ERROR;
  }
  pp_unlock(trans_parallel_port);
  return TRUE;
}


/***********************************************************************
 * int ignition_setposition(int)
 * author: Jeff Lamb (JCL)
 * date: 02-23-04
 * functionality:  takes in ignition position (RUN, START, OFF) and
 *  returns with corresponding return value (RUN = 2, OFF = 1, START = 3)
 *  any other inputs will return ERROR = -1
 **********************************************************************/
int ignition_setposition(vehicle_status) {
  pp_lock(trans_parallel_port);
  switch(vehicle_status) {
  case RUN:
    pp_set_bits(trans_parallel_port, run, ign);
    pp_unlock(trans_parallel_port);
    return vehicle_run;
    break;
  case START:
    //note: this is really really really shitty logic
    //we send the ignition line high for half a second, assuming
    //the ignition will start in that time (the function is also blocking
    //for that amount of time.  What should happen here is we should
    //check OBDII for rpm or some other signal to indicate ignition
    //start and then return.
    pp_set_bits(trans_parallel_port, run | ign, NULL);
    usleep_for(500000);
    pp_set_bits(trans_parallel_port, run, ign);
    pp_unlock(trans_parallel_port);
    return vehicle start;
    break;
  case OFF:
    pp_set_bits(trans_parallel_port, NULL, run | ign);
    pp_unlock(trans_parallel_port);
    return vehicle_off;
    break;
  case default:
    pp_unlock(trans_parallel_port);
    return ERROR;
  }
  pp_unlock(trans_parallel_port);
  return TRUE;
}

  







