

#include "statefilter.hh"

// detectjump declarations
int first_time = 1;  // flag for running first time through
double last_update_time; // time of last update (for getting delta-t)
double n_last, e_last; // previous northing/easting


// dev_reject declarations
int counter = 1;  // how many times have we run?
double buffer[NUM_ELEMENTS]; // buffer to hold elements 
int buff_ptr = 0; // pointer for element in the buffer
double pn, pe; // previous northing and eastings 
double avg, var, dev; // mean, variance, std deviation
double sigma; // Xn - avg

// magfilter definitions
double mag_yaw_err = .43;  // magnetometer yaw error
double mag_pk = 0;       // magnetometer  mean sq err
double mag_pk1=0;     // mean sq err coeff
double mag_bk;       // magnetometer kalman gain
double yk;  // current gps - mag heading

// magnetometer heading mean sq. err (est)
double mag_sigma = pow(3 * M_PI / 180, 2);
// gps heading mean sq. err (est)
double gps_sigma = pow(.5 * M_PI / 180, 2);    

// magfilter
// runs a single state kalman filter to estimate the error in the 
// mag heading reading
void magfilter(double magheading, double gpsheading)
{
  yk = gpsheading - magheading;

  // bound yaw error
  yk = norm_yaw(yk);

  // get mean sq err coeff
  mag_pk1 = mag_pk + mag_sigma;
  
  // update gain from mean sq err
  mag_bk = mag_pk / (mag_pk + gps_sigma);

  // get mean sq err
  mag_pk = mag_pk1 - mag_bk * mag_pk1;

  // get next estimate of the error
  mag_yaw_err = mag_yaw_err + mag_bk * (yk - mag_yaw_err);

  return;
}

// norm_yaw
// bounds yaw by -pi to pi
double norm_yaw(double yaw)
{
  while ((yaw > M_PI) || (yaw < - M_PI)){
    if (yaw > M_PI)
      yaw -= M_PI * 2;
    else if (yaw < - M_PI)
      yaw += M_PI * 2;
  }
 
  return yaw;
}
// *** Not currently used *** 
// dev_reject
// This function keeps a running average of the distance between
// gps points that it's fed, and calculates the std. dev. of these points.
// if a received point is outside some specified multiple of this std dev
// it returns 0, otehrwise it returns 1  
int dev_reject(double northing, double easting)
{
  int accept;
  double dn, de, displace; // change in northing/easting 

  if (counter == 1)
    {
      pn = northing;
      pe = easting;
      avg = 0;
      var = 0;
      dev = 0;
      accept = 1;
    }
  else if (counter <= NUM_ELEMENTS)
    {
      // get the deltas
      dn = northing - pn;
      de = easting - pe;
      displace = sqrt(dn * dn + de * de);
      buffer[buff_ptr] = displace;
      
      // get and store the stats
      avg = get_avg();
      var = get_var();
      dev = sqrt(var);
  
      // increment things that need to be incremented
      buff_ptr = (buff_ptr + 1) % NUM_ELEMENTS;
      counter++;
      accept = 1;
      pn = northing;
      pe = easting;
    }
  else
    {
      // get the deltas
      dn = northing - pn;
      de = easting - pe;
      displace = sqrt(dn * dn + de * de);
     
      sigma = abs(displace - avg);

      if (sigma >= dev * SigmaMult)
	{
	  accept = 0;
	  buffer[buff_ptr] = displace;	  
	}else{
	  accept = 1;
	  buffer[buff_ptr] = displace;
	  }
      
      // get and store the stats
      avg = get_avg();
      var = get_var();
      dev = sqrt(var);
      
      buff_ptr = (buff_ptr + 1) % NUM_ELEMENTS;
      counter++;
      pn = northing;
      pe = easting;
    }


  return accept;
}

// run through the buffer and get the average of the elements
double get_avg()
{
  double total=0;
  int limit;

  // sum up only the number of elements that've been put in to buffer
  if (counter <= NUM_ELEMENTS)
    {
      limit = counter;
    }else{
      limit = NUM_ELEMENTS;
    }


  for (int i = 0; i < limit; i++)
    {
      total += buffer[i];
    }

  return total/limit;
}

// gets 1/n * (x - avg)^2
double get_var()
{
  double total = 0;
  int limit;

  // sum up only the number of elements that've been put in to buffer
  if (counter <= NUM_ELEMENTS)
    {
      limit = counter;
    }else{
      limit = NUM_ELEMENTS;
    }


  for (int i = 0; i < limit; i++)
    {
      total += (buffer[i] - avg) * (buffer[i] - avg);
    }

  return total/limit;
}

// reset the filter to initial conditions
void filter_reset()
{
  counter = 1;
  buff_ptr = 0;

  return;
}


// detect_jump
// checks if there's been a jump in the GPS data by looking at the heading 
// angle and comparing it to the displacement angle.  If there's been a 
// jump, then return the magnitude of that jump, otherwise return 0
double detect_jump(double cur_n, double cur_e, double update_time, 
	       VState_GetStateMsg vehstate)
{
  double angle; // angle between heading and the displacement vector
  double magnitude; // magnitude of the displacement vector
  double jump; // answer to return
  double dn, de; // displacement between last point and this one
  double product; // the dot product between the heading and the displacement

  if (first_time){
    first_time = 0;
    }
  else{
    dn = cur_n - n_last;
    de = cur_e - e_last;

    // normalize displacement vector
    magnitude = sqrt(dn * dn + de * de);

    // take dot product between this displacement and the heading
    product = (dn * cos(vehstate.Yaw) + de * sin(vehstate.Yaw)) / magnitude;
    angle = acos(product);
   
    if (angle >= ANGLE_CUTOFF)
      jump = magnitude;
    else
      jump = 0;
  }

  n_last = cur_n;
  e_last = cur_e;
  last_update_time = update_time;

  return jump;
}

// detect_reset
// resets the jump detection so that it doesn't think we've jumped horribly
// between gps jumps
void detect_reset()
{
  first_time = 1;

  return;
}

/*
// gps_err_reset
// If there's been a jump, the gps error value in the kfilter is reset higher
// and then allowed to come down to a low defalt value as an exponential curve
double gps_err_reset(double gps_err, double gps_jump, double gps_time)
{
  double new_err; // the new gps error to be returned

  gps_err * = 6378.0 * 1000; // convert gps error back into meters
  
  */
      
