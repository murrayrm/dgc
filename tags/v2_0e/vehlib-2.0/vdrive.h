/*
 * vdrive.h - header file for vdrive.c
 *
 * RMM/WH, 21 Feb 04
 *
 * This file contains the function and variable definitions for anything
 * in vdrive.c that might be accessed externally.
 *
 */

/* vdrive mode switching comments */
int mode_manual(long), mode_remote(long), mode_off(long), 
  mode_autonomous(long), mode_test(long), mode_estop(long);

int user_cap_toggle(long arg);

int user_quit(long arg);

int user_estop_call(long arg);

int full_stop(long arg);

int steer_inc(long arg);
int steer_dec(long arg);
int accel_inc(long arg);
int accel_dec(long arg);

