/*
 * imustate.cc
 *
 * Created 12/16/03 by H Huang and Ike Gremmer
 *
 * Receives IMU packets from IMU laptop over ethernet
 * Based on triv-listener.c by Dave Benson
 *
 * 30 Dec 03, RMM: modified for use with vdrive
 *   * got rid of boost thread library
 *   * created local data structure instead of using DATUM
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>		/* for sockaddr_in */
#include <netdb.h>		/* for gethostbyname() */
#include <unistd.h>		/* for read(), write() */
#include <assert.h>
#include <pthread.h>
#include <sys/time.h>		/* unix: for gettimeofday() */
#include "crc32.h"
#include "imu_net.h"
#include "imu.h"
#include "sparrow/dbglib.h"
#include "vehports.h"

#ifndef WIN32
#define SOCKET int
#define closesocket close
#endif

// port for talking to the IMU laptop
const unsigned portnum = IMU_IP_PORT;
SOCKET imu_sock;

// network address setup for receiver
static int
setup_addr (struct sockaddr *addr,
	     unsigned port)
{
  struct sockaddr_in addr_in;
  memset (&addr_in, 0, sizeof (addr_in));
  addr_in.sin_family = AF_INET;
  addr_in.sin_port = htons (port);
  memcpy (addr, &addr_in, sizeof (struct sockaddr_in));
  return 1;
}

// InitIMU
// Initializes network communications for IMU
int IMU_open() {
  struct sockaddr addr;

  // set up receive address and port number 
  if (!setup_addr (&addr, portnum)) {      
    dbg_error("Error setting up receive port and address\n");
    return -1;
  }

  // set up socket and port 
  imu_sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (bind (imu_sock, &addr, sizeof (addr)) < 0) {
    dbg_error("Error binding to port %d\n", portnum);
    return -1;
  }

  dbg_info("IMU initialized\n");
  return 0;
}

int IMU_read(IMU_DATA *state_dat) 
{
  IMUReading *imupacket;

  // Read a message from the IMU
  char buf[256];
  int len = recv(imu_sock, buf, sizeof(buf), 0);

  if (len != 4 + sizeof (IMUReading)) {
    //print error message and continue loop
    dbg_error("Error: Incorrect packet size\n");
    return -1;
  }

  // Process the data
  imupacket = (IMUReading*)(buf + 4);

  /* Make sure we received the packet OK */
  unsigned char crc32[4];
  crc32_big_endian (buf + 4, len - 4, crc32);
  if (memcmp (crc32, buf, 4) != 0) {
    dbg_error("CRC error (corrupted packet)\n");
    return -1;
  }

  state_dat->dvx = imupacket->dvx;
  state_dat->dvy = imupacket->dvy;
  state_dat->dvz = imupacket->dvz;
  state_dat->dtx = imupacket->dtx;
  state_dat->dty = imupacket->dty;
  state_dat->dtz = imupacket->dtz;

  return 0;
}
