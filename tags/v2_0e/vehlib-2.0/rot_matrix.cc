#include <iostream>
#include <cassert>
#include <cmath>
#include <iomanip>
#include "rot_matrix.hh"

using namespace std;

// Constructors

// creats identity transformation
rot_matrix::rot_matrix()
{
  // create the identity rotation
  Matrix m;
  m.setelem(0, 0, 1);
  m.setelem(1, 1, 1);
  m.setelem(2, 2, 1);

  rmatrix = m;

  // attitude starts out perfectly level, facing north
  pitch = 0;
  roll = 0;
  heading = 0;

}

// creates matrix for a p,r,h transformation
rot_matrix::rot_matrix(double p, double r, double h)
{
  pitch = p;
  roll = r;
  heading = h;
  Matrix m(); 

  rmatrix = make_b2n(pitch, roll, heading);

}

// copy constructor
rot_matrix::rot_matrix(const rot_matrix &rot2)
{
  pitch = rot2.get_pitch();
  roll = rot2.get_roll();
  heading = rot2.get_heading();

  rmatrix = rot2.get_matrix();

}


// mutators
// set the pitch to a certain angle, then recalculate rotation matrix
void rot_matrix::setpitch(double p)
{
  // set new pitch and generate new matrix
  pitch = p;
  rmatrix = make_b2n(pitch, roll, heading);

  return;
}

// set the roll to a certain angle
void rot_matrix::setroll(double r)
{
  // set new roll and generate new matrix
  roll = r;
  rmatrix = make_b2n(pitch, roll, heading);

  return;
}

// set the heading to a certain angle
void rot_matrix::setheading(double h)
{
 // set new heading and generate new matrix
  heading = h;
  rmatrix = make_b2n(pitch, roll, heading);

  return;
}

// set all three attitudes at same timex
void rot_matrix::setattitude(double p, double r, double h)
{
 // set new attitude and generate new matrix
  pitch = p;
  roll = r;
  heading = h;
  rmatrix = make_b2n(pitch, roll, heading);

  return;
}

// propagates rotation matrix by a rotation (dx, dy, dz)
rot_matrix& rot_matrix::propagate(double dx, double dy, double dz)
{
  // create a new rotation based on rotation vector
  *this = make_drot(dx, dy, dz) * *this;

  return *this;
  
}

// generates a rotation matrix given a 3-element rotation vector
// NOTE: the attitude angles are NOT proplerly set, since this is only used
// in propagation, where the angles will be set after a multiplication
// this is to save time so that the angles will not be calculated uselessly
rot_matrix rot_matrix::make_drot(double dx, double dy, double dz)
{
  rot_matrix result;
  Matrix drot_mat = Matrix_drot(dx, dy, dz);
  result.set_matrix(drot_mat);

  return result;
  
}

// set up a b2n rotation matrix based on the passed parameters
void rot_matrix::set_b2n_attitude(double p, double r, double h) 
{
  // no difference between set b2n and set attitude
  setattitude(p, r, h);

  return;
}

// accessors
// returns euler angles associated with this rotation matrix
double rot_matrix::get_pitch() const
{
  return pitch;
}

double rot_matrix::get_roll() const
{
  return roll;
}

double rot_matrix::get_heading() const
{
  return heading;
}

// get element
double rot_matrix::get_element(int row, int col) const
{
  return rmatrix.getelem(row, col);
}

// displays matrix and angles to screen
void rot_matrix::display() const
{
  cout<<setiosflags(ios::fixed | ios::showpoint | ios::right)<<setprecision(4);
  cout<<"Pitch: "<<pitch<<" Roll: "<<roll<<"Heading: "<<heading<<endl;
  cout<<setw(6)<<rmatrix.getelem(0,0)<<" "<<setw(6)<<rmatrix.getelem(0,1)<<" "<<setw(6)<<rmatrix.getelem(0,2)<<endl;
  cout<<setw(6)<<rmatrix.getelem(1,0)<<" "<<setw(6)<<rmatrix.getelem(1,1)<<" "<<setw(6)<<rmatrix.getelem(1,2)<<endl;
  cout<<setw(6)<<rmatrix.getelem(2,0)<<" "<<setw(6)<<rmatrix.getelem(2,1)<<" "<<setw(6)<<rmatrix.getelem(2,2)<<endl;
  cout<<endl;
  return;
}

// operators  
// multiply by another rotation matrix
rot_matrix rot_matrix::operator*(const rot_matrix &matrix2) const
{
  //uses *= and a new matrix to get a result
  rot_matrix result;
  result = *this;
  result *= matrix2;
  return result;
} 

// matrix is multiplied by another matrix
rot_matrix& rot_matrix::operator*=(const rot_matrix &matrix2)
{
  // multiply the matrices
  rmatrix *= matrix2.get_matrix();
  
  // back out the angles from the matrix
  pitch = asin(-rmatrix.getelem(2, 0));
  roll = asin(rmatrix.getelem(2, 1) / cos(pitch));
  heading = atan2(rmatrix.getelem(1, 0), rmatrix.getelem(0, 0));

  return *this;
}

// set one matrix equal to another 
rot_matrix& rot_matrix::operator=(const rot_matrix &matrix2)
{
  if(matrix2 == *this){
    return *this;
  }

  pitch = matrix2.get_pitch();
  roll = matrix2.get_roll();
  heading = matrix2.get_heading();
  rmatrix = matrix2.get_matrix();

  return *this;
}

// check rotation matrix equality
bool rot_matrix::operator==(const rot_matrix &matrix2) const
{
  bool equals;
  equals = (pitch == matrix2.get_pitch()) && (roll == matrix2.get_roll()) && (heading == matrix2.get_heading()) && (rmatrix == matrix2.get_matrix());

  return equals;
}

// multiplies the passed vector by the rotation matrix
XYZcoord rot_matrix::operator*(const XYZcoord point) const
{
  XYZcoord result(0 , 0, 0);
  result.x = rmatrix.getelem(0, 0) * point.x + rmatrix.getelem(0, 1) * point.y + rmatrix.getelem(0, 2) * point.z;
  result.y = rmatrix.getelem(1, 0) * point.x + rmatrix.getelem(1, 1) * point.y + rmatrix.getelem(1, 2) * point.z;
  result.z = rmatrix.getelem(2, 0) * point.x + rmatrix.getelem(2, 1) * point.y + rmatrix.getelem(2, 2) * point.z;
  
  return result;
} 

// destructor - nothing needs to be done, no dynamic memory allocation
rot_matrix::~rot_matrix()
{ 
}

// private functions
// returns matrix portion of rot_matrix
Matrix rot_matrix::get_matrix() const
{
  return rmatrix;
}

Matrix rot_matrix::make_b2n(double p, double r, double h) const
{
  Matrix m;

  // set b2n direction cosine matrix:
  // [ ch*cp  -cr*sh+ch*sp*sr  ch*cr*sp+sh*sr
  //   cp*sh  ch*cr+sh*sp*sr   cr*sh*sp-ch*sr
  //   -sp        cp*sr          cp*cr ]

  double ch = cos(heading);
  double sh = sin(heading);
  double cr = cos(roll);
  double sr = sin(roll);
  double cp = cos(pitch);
  double sp = sin(pitch);
 
  // first row
  m.setelem(0, 0, ch*cp);
  m.setelem(0, 1, -cr*sh + ch*sp*sr);
  m.setelem(0, 2, ch*cr*sp + sh*sr);

  //second row
  m.setelem(1, 0, cp*sh);
  m.setelem(1, 1, ch*cr+sh*sp*sr);
  m.setelem(1, 2, cr*sh*sp-ch*sr);
  
  //third row
  m.setelem(2, 0, -sp);
  m.setelem(2, 1, cp*sr);
  m.setelem(2, 2, cp*cr);
 
  return m;
}

// sets the matrix to a value
void rot_matrix::set_matrix(const Matrix newmat)
{
  rmatrix = newmat;
  return;
}


// generate a matrix from a rotation vector
Matrix rot_matrix::Matrix_drot(double dx, double dy, double dz)
{
  Matrix m;
  double temp;

  // Using formula courtesy of Victor Chueh at NGC
  // Victor's stuff gives body to nav, this gives nav to body
  // I don't know the derivation of this matrix either, so just trust that it's right 
  double theta = sqrt(dx*dx + dy*dy + dz*dz);
  if (theta == 0)
    {
      // create the identity rotation
      m.setelem(0, 0, 1);
      m.setelem(1, 1, 1);
      m.setelem(2, 2, 1);
    }
  else
    {
      double L = dx / theta;
      double M = dy / theta;
      double N = dz / theta;

      double c = cos(theta);
      double s = sin(theta);

      // first column
      temp = c + L * L * (1 - c);
      m.setelem(0, 0, temp);
      temp = L * M * (1 - c) - N * s;
      m.setelem(1, 0, temp);
      temp = L * N * (1 - c) + M * s;
      m.setelem(2, 0, temp);

      // second column
      temp = L * M * (1 - c) + N * s;
      m.setelem(0, 1, temp);
      temp = c + M * M * (1 - c);
      m.setelem(1, 1, temp);
      temp = M * N * (1 - c) - L * s;
      m.setelem(2, 1, temp);

      // third column
      temp = L * N * (1 - c) - M * s;
      m.setelem(0, 2, temp);
      temp = M * N * (1 - c) + L * s;
      m.setelem(1, 2, temp);
      temp = c + N * N * (1 - c);
      m.setelem(2, 2, temp);

    }
  return m;
}
