//this begins the transmission header file
#define TRUE 1
#define FALSE 0
#define ERROR -1

using namespace std;
//transmission gears to pass to trans_setposition(int)
#define NEUTRAL
//#define FREE   //no longer used
#define FIRST
//#define SECOND //no longer used
#define THIRD
#define REVERSE

//ignition states to pass to ignition_setposition(int)
#define RUN
#define START
#define OFF

int trans_open(int);
int trans_setposition(int);
int trans_close(void);
int ignition_setposition(int);
#endif
