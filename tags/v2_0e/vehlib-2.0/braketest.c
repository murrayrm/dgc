/*
 * braketest.c - simple test program for the brakes
 *
 * RMM, 24 Feb 04
 *
 * This is a test program that is used to check out the brake device
 * driver.  Not needed for the race.
 */

#include <stdlib.h>
#include <pthread.h>
#include "brake2.h"
#include "vehports.h"
#include "parallel.h"
#include "sparrow/dbglib.h"

pthread_t brake_thread;			/* brake thread */
int brake_flag = 0;			/* enabled flag */
extern int brake_case;

main(int argc, char **argv)
{
  char inpbuf[100];
  double pos;

  /* Turn on debugging */
  dbg_flag = dbg_outf = dbg_all = 1;

  /* Open up brake/throttle parallel port */
  pp_init(PP_GASBR, PP_DIR_IN);

  brake_open(5);
  /* brake_calibrate(); */

  while (1) {
    double curpos;

    printf("cur = %g, cmd: ", brake_pot_current);
    scanf("%s", inpbuf);

    switch (inpbuf[0]) {
    case 'q':				/* all done */
    case 'Q':
      brake_close();
      return 0;

    case '?':				/* check brake status */
      int status;
      brake_status(&status);
      printf("brake_case = %d\n", brake_case);
      printf("brake_curpos = %g\n", brake_pot_current);
      break;

    case 'c':				/* clear polling status register */
      brake_clrpsw(0xffff);
      break;

    case 'C':
      brake_calibrate();
      break;

    case 'p':				/* print the position */
      brake_read(&curpos);
      printf("pos = %g\n", curpos);
      break;

    case 'r':
      brake_reset();
      break;

    case 'h':
      brake_home();
      break;

    case 't':				/* turn on servo thread */
      pthread_create(&brake_thread, NULL, brake_servo, (void *) &brake_flag);
      dbg_info("thread started");
      break;

    case 'e':      brake_flag = 1;	break;
    case 'd':      brake_flag = 0;	break;
    case 'k':	   brake_flag = -1;	break;

    default:
      pos = atof(inpbuf);
      if (brake_flag != 1) {
	brake_setmav(pos, 1.0, 1.0);
      } else {
	brake_write(pos, 1.0, 1.0);
      }
    }
  }

  brake_close();
  return 0;
}
