Changes for vehlib 2.0 package
RMM, 21 Jan 04

***** v2_0e *****

27 Feb 04, RMM: various fixes
  * put in #ifdefs in serial.c if SERIAL2 is defined
  * reset moving and holding limits to large numbers (zero was wrong)
  * put in #defines around all serial code; recompiled *without* SERIAL2

26 Feb 04, RMM: more brake2 fixes
  * Implemented brake_status that checks I/O and internal registers as well
  * Set moving and holding error limits to zero
  * Reset supply voltage from truck to 36V with adjustment screw
  * Brake is now working in vdrive!!

26 Feb 04, RMM: brake2 fixes
  * Use brake_case to only command motion when needed
  * Fixed brake_home in the same manner
  * Added define for brake cycle time
  * Tightened up brake tolerance and decreased cycle time (10 msec)
  * Added parallel port locking in brake_read(); eliminated read errors

25 Feb 04, RMM: intial release of brake2 driver
  * same functionality as brake.c
  * working standalong, but still whining in vdrive

24 Feb 04, RMM: brake debugging
  * moved BRAKE_ACC, BRAKE_VEL, MAX_BRAKE to brake.c instead of vdrive.c
      + needed to do this to test out braketest

22 Feb 04, RMM: field updates
  * added magnetometer offset that can be changed from screen
  * changed order of variables in atan2 call for kfilter_flag == 0 case
  * played around with variations on steerings to stop hangs
      + never found anything absolutely reliable
  * had to compile vstate with old serial code to get working (??)

15 Feb 04, RMM: install target changes
  * Changed install target in Makefile to remove version number
  * This was causing problems for the rest of the team

15 Feb 04, RMM: vstate modifications to work around IMU
  * added 'k' flag to turn off Kalman filter
  * added code to get state data from best avaiable source when kfilter off
  * added -v flag processing to stop before entering display
  * kalman filter replace runs in IMU loop; need to use -i if IMU is broke

14 Feb 04, RMM: vtest modes
  * added -f flag to set frequency

13 Feb 04, RMM: created vtest
  * Prelimary version; sends sinusoidal commands to vdrive
  * Modified vdrive to accept velocity commands in autonomous mode

7 Feb 04, RMM: prep for embedded systems team test
  * added check in serial_open_* to make sure serial port is OK
  * added back display flags for steering
  * fixed bug in serial code: could accidently close stdin
     + serial_close would close whatever was listed in the array
     + by default, this was zero (= stdin)
     + steer_open was calling serial_close, which caused this problem
  * added processing of long arguments (in addition to short)
  * switched order of keys
  * cleaned up display and rearranged things a bit
  * added calibrate button to display for steering
  * tested out steering code; something wrong in logic (Jeff to fix)

30 Jan 04, RMM: added data logging back in
  * used printfs withing capture thread instead of sparrow
  * put in logic for locking access to capture file access
  * removed all sparrow channel code (no longer required)
  * Set up variables to be stored in same way as simulation file (bug #194)

30 Jan 04, RMM: Makefile changes
  * added vdrive.ini default back in
  * set things up to compile vdparm table

21 Jan 04, RMM: initialization of vehlib 2.0
  * created Changes file
  * initial version of vdrive with almost everything commented out
