/*
 * estop.c - code to control the estop
 *
 * Author: Jeff Lamb
 * 01-16-04
 *
 * estop disable is parallel port pin 8
 * estop pause is parallel port pin 7
 * function returns -1 error, 0 normal, 1 pause, 2 disable
 */

#include <stdlib.h>
#include "estop.h"
#include "vehports.h"
#include "sparrow/dbglib.h"

static int estoppport = ESTOP_PARALLEL_PORT;
int status;
int estopOK=0;

/*initialize estop connection*/
int estop_init() {
  if(pp_init(estoppport, PP_DIR_IN) >=0) {              //set up port to recieve data
    dbg_info("Estop init on port %d\n", estoppport);
    estopOK=1;
    return NORMAL_FLAG;
  } else {
    dbg_info("\nError opening parallel port %d\n", estoppport);  
    estopOK=0;
    return ERROR_FLAG;
  }
}

/*check parallel for estop conditions.  We give disable estop priority over pause
 *disable line checked first then pause
 *function returns after any pause or disable, or after finding a normal condition
 *non-blocking
 */
int estop_status() {
  if (estopOK != 1) return ERROR_FLAG;
  status=pp_get_bit(estoppport, DISABLE_PIN);  //check pin 8 for high signal
  if (status == DISABLE) return DISABLE_FLAG;  //if high, then return disable
  else {
    status=pp_get_bit(estoppport, PAUSE_PIN);  //else check pin 7 for high signal
    if (status == PAUSE) return PAUSE_FLAG;    //if high, then return pause
    else return NORMAL_FLAG;                    //else, return normal
  }
}
  
