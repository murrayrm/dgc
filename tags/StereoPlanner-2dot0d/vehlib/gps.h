#ifndef GPS_H
#define GPS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream.h>
#include <fstream.h>
#include <iomanip.h>
#include "serial.h"

//using namespace std;

//GPS Message Types
#define GPS_MSG_PVT 0xB1

//debug messages 
#define PRINT_DEBUG_MESSAGES 0

//GPS Message Data Rates
//If a rate of less than 2Hz is desired, simply use the interval (in seconds) that you want
//For example, for 0.2 Hz (once every five seconds) just use 5 instead of a pre-defined rate
#define GPS_MSG_RT_STOP 0
#define GPS_MSG_RT_02HZ -1
#define GPS_MSG_RT_05HZ -2
#define GPS_MSG_RT_10HZ -3
#define GPS_MSG_RT_25HZ -4
#define GPS_MSG_RT_50HZ -5
#define GPS_MSG_RT_OEXT -6
#define GPS_MSG_RT_ONCH -7

// mask for valid PVT
#define NAV_VALID 128

//Struct which contains all the data returned by a PVT (position velocity time) message from the GPS unit
//Refer to message B1 in the NavCom technical reference for information on the different varables
struct gpsData {
	int gps_week;
	int ms_in_week;
	int sats_used;

	double lat;
	double lng;

	char nav_mode;

	float ellipsoidal_height;

	float altitude;

	float vel_n;
	float vel_e;
	float vel_u;

	int position_fom;
	float gdop;
	float pdop;
	float hdop;
	float vdop;
	float tdop;

	int tfom;

	float max_dgps_corr_age;

	char dgps_config;

	char extended_nav_mode;
	
	int antenna_height_adj;
	
	int adjust_v_height;
};

//A class to wrap the GPS PVT data around
class gpsDataWrapper {
 public:
  //Constructors/Destructors
  gpsDataWrapper();
  ~gpsDataWrapper();

  //Public Functions
  //Prints all the data in the object to the screen
  void print_to_screen();
  //Appends all the data in the object to filename
  void write_to_file(char *filename);
  //Updates the data in the object based on the GPS message stored in pvt_buffer
  int update_gps_data(char *pvt_buffer);

  //Data
  gpsData data;
  
 private:
};


//GPS Functions
//Initializes the GPS, and makes it output PVT messages at the desired rate
int init_gps(int com, int rate);
//Or close the gps by simply closing the serial port
int close_gps();

//Gets the first GPS message waiting in the serial buffer, and stores it in orig_buffer
//If the message is longer than buf_length (which should be the length of orig_buffer) then it returns an error
int get_gps_msg(char *orig_buffer, int buf_length);

// returns only if the message received is pvt.
int get_gps_pvt_msg(char* orig_buffer, int buf_length);

//Returns the type (command ID) of a valid GPS message stored in buffer
int get_gps_msg_type(char *buffer);

//Sets the GPS message of command_id to the desired rate
//See above on setting rates other than predefined constants
int set_gps_msg_rate(int command_id, int rate);
int set_gps_nav_rate(int rate);

//Clears all messages so the GPS outputs nothing
int set_gps_msg_none();

//Prints a GPS message stored in buffer to the screen, generally for debugging purposes
int print_gps_msg(char *buffer);

//Writes a GPS message of command_id stored in orig_msgptr to the GPS
//msg_length is the length of the message in orig_msgptr
int write_gps_msg(int command_id, char *orig_msgptr, int msg_length);

//Function which returns the checksum (according to the GPS specs) for a string in buffer with length length
unsigned int get_cksum(unsigned char *buffer, int length);

//Checks orig_msg to see if it conforms to what a GPS message should look like
bool valid_gps_msg(char* orig_msg);

//Checks orig_msg to see if the PVT message has valid data
bool valid_pvt_msg(char* orig_msg);

//The following functions are used to extract data from GPS messages
//u stands for unsigned integer, s stands for signed integer
//The two-digit number is the number of bits
//ptr is a pointer to the data in memory
unsigned int get_u08(unsigned char* ptr);
unsigned int get_u16(unsigned char* ptr);
unsigned int get_u24(unsigned char* ptr);
unsigned int get_u32(unsigned char* ptr);
int get_s08(unsigned char* ptr);
int get_s16(unsigned char* ptr);
int get_s24(unsigned char* ptr);
int get_s32(unsigned char* ptr);

//The following functions are used to create data formatted for GPS messages
//u stands for unsigned integer, s stands for signed integer
//The two-digit number is the number of bits
//ptr is a pointer to the data in memory
//Each functions returns what would be the next memory location at which data could be written,
//preventing the need to constantly increment your pointer
//For example, to write an 16-bit signed integer (6) to a pointer and then write an unsigned 8 bit integer (9) immediately after it in memory, you would use the following commands
//buffer_ptr=make_u16(6, buffer_ptr);
//buffer_ptr=make_u08(9, buffer_ptr);
unsigned char* make_u08(unsigned int val, unsigned char* ptr);
unsigned char* make_u16(unsigned int val, unsigned char* ptr);
unsigned char* make_u24(unsigned int val, unsigned char* ptr);
unsigned char* make_u32(unsigned int val, unsigned char* ptr);
unsigned char* make_s08(int val, unsigned char* ptr);
unsigned char* make_s16(int val, unsigned char* ptr);
unsigned char* make_s24(int val, unsigned char* ptr);
unsigned char* make_s32(int val, unsigned char* ptr);

#endif /* GPS_H */
