#include "waypoints.h"

Waypoints::Waypoints (): _current(0),_last(0)
{
  _waypoint_list[0].t = NONE; 
  _waypoint_list[0].northing = 0.0; 
  _waypoint_list[0].easting  = 0.0; 
  _waypoint_list[0].radius   = 0.0; 
  _waypoint_list[0].max_velocity = 0.0; 
}

Waypoints::Waypoints (char * infile)
{
  read(infile);
}

void Waypoints::read (char * infile)
{
  ifstream wp_file(infile);
  char type_str;
  char dummy[256];
  int i=0;
  while (!wp_file.eof())
    {
      _waypoint_list[i].num = i;
      wp_file >> type_str;
      switch (type_str)
	{
	case 'S':
	  wp_file >> _waypoint_list[i].easting
		  >> _waypoint_list[i].northing
		  >> _waypoint_list[i].radius
		  >> _waypoint_list[i].max_velocity;
	  _waypoint_list[i++].t = START;
	  break;
	case 'N':
	  wp_file >> _waypoint_list[i].easting
		  >> _waypoint_list[i].northing
		  >> _waypoint_list[i].radius
		  >> _waypoint_list[i].max_velocity;
	  _waypoint_list[i++].t = NORMAL;
	  break;
	case 'C':
	  wp_file >> _waypoint_list[i].easting
		  >> _waypoint_list[i].northing
		  >> _waypoint_list[i].radius
		  >> _waypoint_list[i].max_velocity;
	  _waypoint_list[i++].t = CHECKPOINT;
	  break;
	case 'E':
	  wp_file >> _waypoint_list[i].easting
		  >> _waypoint_list[i].northing
		  >> _waypoint_list[i].radius
		  >> _waypoint_list[i].max_velocity;
	  _waypoint_list[i++].t = END;
	  break;
	default:
	  wp_file.ignore(256,'\n');
	  _waypoint_list[i].t = NONE;
	  break;
	}
    }
  _last = i;
  _current = 0;
}

int Waypoints::get_size()
{
  return _last;
}
waypoint Waypoints::get_start()
{
  _current = 0;
  return _waypoint_list[0];
}

waypoint Waypoints::get_next()
{
  if (_current < _last) _current++;
  return _waypoint_list[_current];
}

waypoint Waypoints::get_previous()
{
  if (_current > 0) _current--;
  return _waypoint_list[_current];
}

waypoint Waypoints::get_last()
{
  _current = _last;
  return _waypoint_list[_last];
}

waypoint Waypoints::get_current()
{
  return _waypoint_list[_current];
}

waypoint Waypoints::get_waypoint(int i)
{
  if (i < 0) i=0;
  if (i > _last) i=_last;
  _current = i;
  return _waypoint_list[_current];
}

waypoint Waypoints::peek_waypoint(int i)
{
  if (i < 0) i=0;
  if (i > _last) i=_last;
  return _waypoint_list[i];
}

waypoint Waypoints::peek_next()
{
  return _waypoint_list[(_current < _last)?(_current+1):(_last)];
}

waypoint Waypoints::peek_previous()
{
  return _waypoint_list[(_current > 0)?(_current-1):(0)];
}

