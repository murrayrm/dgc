#include <stdio.h>
#include <time.h>

#include <iostream>

#include "../serial/serial.h"
#include "steerCar.h"

using namespace std;

int negCountLimit = -60000;
int posCountLimit = 60000;
int verbosity_exec_cmd = TRUE;          // steer_exec_cmd uses verbose output.
int steer_errno = ERR_NONE;             // We start out with no errors.

///////////////////////////THIS IS A HACK/////////////////
string lastSteeringCommanded = (string) "Hello";
//////////////////////////////////////////////////////////

int main(int argc, char* argv[]) 
{
    char inputChar;         // Character to use for input.
    double steerAngle;      // Angle that we will steer at.
    // This code is *very* blocking.  Johnson wrote it and I don't know why.
    while (init_steer() != TRUE) { printf("waiting on intialization\n"); }

    //
    // Figure out if we are using manual commands or not and enter the correct
    // loop.
    //
   
    if (argc != 2) {
        // Enable ghetto command-line steering in a loop.
        cout << "\nManual Steering Commands:\n";
        cout << "1\tFull (lock) Left\n";
        cout << "2-4\tLeft in decreasing intensity\n";
        cout << "5\tDead Straight\n";
        cout << "6-8\tRight in increasing intensity\n";
        cout << "9\tFull (lock) Right\n";
        cout << "'r' resets (runs init_steer() again and waits 10 secs)\n";
        cout << "Use 'q' to exit!!\n\n";
        while ( (inputChar = getchar()) != 'q') {
            switch (inputChar) {
                case '1': case '2': case '3': case '4': case '5': 
                    steerAngle = (double) (inputChar - '5') / 4;
                    steer_heading(steerAngle);
                    break;
                case '6': case '7': case '8': case '9':
                    steerAngle = (double) (inputChar - '5') / 4;
                    steer_heading(steerAngle);
                    break;
                case 'r': 
                    init_steer();
                    cout << "\nThe same commands as before apply\n\n";
                    break;
                case '\n':
                    steer_state_update(TRUE, TRUE);
                    // Do nothing.
                    break;
                default:
                    cout << "Illegal character ('" << inputChar << "'), try again!!\n";
                    break;
            }
        }
    } 

    /* This should have some checking to see if the command actually happened,
       like a loop that only exits when we are at the zero position. */
    cout << "\n\nTrying to steer back straight...\n\n";
    steer_heading(0);
 
    serial_close(STEER_SERIAL_PORT);

    return 0;
}

