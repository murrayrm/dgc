#! /bin/sh

TEAMDIR="/home/$USER/team/"
# all paths are relative to the team directory.
MSG_PATH="Msg/"
JOY_PATH="joystick/"
STEER_PATH="steerCar/"

MAKE_CMD="make all"

# Redirection Locations
MTA_CMD=$TEAMDIR$MSG_PATH"mta > mta_log &"
MODEMANAGER_CMD=$TEAMDIR$MSG_PATH"JoystickModeManager > Mode_log &"
JOY_CMD=$TEAMDIR$JOY_PATH"joymain -rl debug -pp 0 > /dev/null &" #> joy_log &"
STEER_CMD=$TEAMDIR$STEER_PATH"steerCar blahblahblah | tee steer_log &"

# Make all the code, assuming that it all works.
echo "Making all code, not checking for errors in make"
#set -x
cd $TEAMDIR$MSG_PATH
eval $MAKE_CMD 
cd $TEAMDIR$JOY_PATH
eval $MAKE_CMD
cd $TEAMDIR$STEER_PATH
eval $MAKE_CMD
#unset -x
echo "Waiting a second so the user can abort by pressing ^C"
sleep 1

echo "Starting mta ..."
echo "$MTA_CMD"
eval $MTA_CMD
echo "Waiting 1 second."
sleep 1
echo "STarting Mode Manager"
echo "$MODEMANAGER_CMD"
eval $MODEMANAGER_CMD
echo "Starting Joystick"
echo "$JOY_CMD"
eval $JOY_CMD
echo "Starting Steering actuation."
echo "$STEER_CMD"
eval $STEER_CMD

echo ""
echo "Press ENTER to quit"
echo ""
read promptInput
echo "$promptInput pressed, quitting ..."

killall steerCar
killall joymain
killall JoystickModeManager
killall mta

echo "Thank you, see you later."
