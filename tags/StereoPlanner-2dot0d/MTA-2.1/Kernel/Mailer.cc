#include "Mailer.hh"
#include "Misc/Mail/Mail.hh"
#include "Kernel.hh"
#include "Misc/Time/Timeval.hh"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include <iostream>

#include "Misc/Thread/ThreadsForClasses.hpp"


extern Kernel K;
extern int MTA_KERNEL_COUT;

using namespace std;
using namespace boost;

int receivecount = 0;
int sendcount = 0;

int ReceiveErrorCount = 0;
int SendErrorCount = 0;

int Kernel::ReceiveMailInit() {  
  
  int port = DEF::MinPort;
  while( ! M_ServerSocket.Listen(port) ) {
    port ++;
    if (port > (DEF::MinPort+DEF::NumPorts)) {
      if( MTA_KERNEL_COUT ) cout << "Error Creating Socket Server - No available sockets. Trying Again" << endl;
      port = DEF::MinPort;
    }
  }
  if( MTA_KERNEL_COUT ) cout << "Socket Server started on port " << port << endl;
  return true;
}
  
void Kernel::ReceiveMailShutdown() {
  // shutting down now
  // M_ServerSocket.Close();

}
  
void Kernel::ReceiveMailThread() {
  
  int msgType;
  
  //  cout << "ReceiveMailServer Starting" << endl;
  while( ! ShuttingDown() ) {
    File in = M_ServerSocket.Accept();
    ReceiveMailHandler rmh(in);
    
    //        receivecount ++;
    //        if( receivecount % 500 == 0) cout << "Receive count = " << receivecount << endl;

    CopyObjectAndRunMethodInNewThread<ReceiveMailHandler>( &rmh, &ReceiveMailHandler::operator());

    //    rmh();
  }
    
}

  
ReceiveMailHandler::ReceiveMailHandler(File &in) {
  f = in;
}
ReceiveMailHandler::ReceiveMailHandler(const ReceiveMailHandler &rhs) {
  f = rhs.f;
}
ReceiveMailHandler::~ReceiveMailHandler() {
  //  cout << "ReceiveMailHandler - Destructor Called" << endl;
}
  
void ReceiveMailHandler::operator() () {
    
  //  cout << "ReceiveMailHandler time = " << TVNow() << endl;


  if(f.FD() < 0) {
    if( MTA_KERNEL_COUT ) cout << "ReceiveMailHandler - Negative File Handler Detected" << endl;
    ReceiveErrorCount++;
    return;
  }
  
  // read in the key to verify the message
  int myKey = 0, flags;
  f.BRead((void*) &myKey, sizeof(myKey));
  

  if( myKey != DEF::ReceiveKey ) {
    if( MTA_KERNEL_COUT ) cout << "ReceiveMailHandler - Rogue Socket Connection" << endl;
    if( MTA_KERNEL_COUT ) cout << "  myKey = " << myKey << ", but expecting " << DEF::ReceiveKey << endl;
    ReceiveErrorCount++;
    return;
  }

  //    cout << "ReceiveMailHandler myKey = " << myKey << endl;

  // if that was successful, then the rest of 
  // the data is the message
  Mail newMessage(f);

    
  if(  newMessage.OK() ) {
    //      cout << "ReceiveMailHandler Message Download Success, Size = "
    //	   << newMessage.Size() << endl;
    
    // now we need to determine what type of message
    // and who it goes to
    
    // first, find the recipient
    int mbox = newMessage.To().MAILBOX;
    flags = newMessage.Flags();
    
    // cout << "Message For Mailbox = " << mbox << endl;
    
    if ( mbox == 0 ) { // for the kernel
      if(flags & MailFlags::QueryMessage) {
	// it's a query message
	//	cout << "Query Message For the Kernel" << endl;
	Mail returnMessage = K.KernelQueryMailHandler(newMessage);
	// so send it on its way.
	//	cout << "Sending Reply To Query Message" << endl;
	SendMail(returnMessage, f); // using the same socket
      } else {
	// just deliver
	//	cout << "In Message For the Kernel" << endl;
	K.KernelInMailHandler(newMessage);
      }
      
    } else { 
      // message for a module
      // find the module the message is for
      DGC_MODULE *x = FindLocalModule(mbox);
      if( x == NULL ) {
	if( MTA_KERNEL_COUT ) cout << "Message for unknown mailbox " << mbox << endl;
	ReceiveErrorCount++;
      } else {
	// now check if inmessage or query message
	if(flags & MailFlags::QueryMessage) {
	  // it's a query message
	  //	  cout << "Query Message for Mailbox=" << mbox << endl;
	  // Let the module respond
	  Mail returnMessage = (*x).QueryMailHandler(newMessage);
	  // and send the reply on its way.
	  SendMail(returnMessage, f); // using the same socket
	} else {
	  // just deliver
	  //	  cout << "In Message for Mailbox=" << mbox << endl;
	  (*x).InMailHandler(newMessage);
	}
      }
    }
  } else {
    if( MTA_KERNEL_COUT ) cout << "ReceiveMailHandler(): Message Receive Failure" << endl;
    ReceiveErrorCount++;
  }
}
 

 
int Kernel::SendMailInit() {
  return true;
}

void Kernel::SendMailShutdown() {
  
}
  


void Kernel::SendMailThread() {

  DeliverPID = getpid();
  
  //  Mail m;
  int newout = false;
  
  while ( ! ShuttingDown() ) {
    if( true ) {
      boost::recursive_mutex::scoped_lock sl(M_OutboxLock);
      if( M_Outbox.size() > 0) {
	newout = true;
      }
    }
    
    if( newout ) {
      //      DeliverMail( m );

      //RunMethodInNewThread<Kernel>(this, &Kernel::DeliverNextMessage);
       DeliverNextMessage();
      newout = false;
    } else {
      // sleep for a bit before trying again
      // theoretically this thread should be 
      // signaled and woken up as soon as 
      // the next message is ready to be sent
      sleep( 1 );
      //      for(int i=0; i<10000; i++) {}
    }
  } 

}


void SendMail(Mail & ml, File f) {
  if( ml.To().MAILBOX < 0 ||
      ml.To().TCP.IP == 0 ||
      ml.To().TCP.PORT == 0) {
    if( MTA_KERNEL_COUT ) cout << "SendMail: Bad Mail" << endl;
    return; // an error occurred, just abandon msg
  }

  // if we dont have a socket ready, enqueue it for delivery
  //  if( f.FD() < 0) {
  //    if(true) {
  //      boost::recursive_mutex::scoped_lock s(K.M_OutboxLock);
  //      K.M_Outbox.push_back(ml);
  //    }
    //    cout << "Adding mail to send queue" << endl;

    // cout << "Killing " << K.DeliverPID << endl;
  //    kill(K.DeliverPID, SIGUSR1); // wake up the Deliver PID Queue
    
  //  } else {    
    // just deliver the mail

    K.DeliverMail(ml, f);
    //  }
}

Mail SendQuery(Mail &ml, File mySocket) {
  if( ml.To().MAILBOX < 0 ||
      ml.To().TCP.IP == 0 ||
      ml.To().TCP.PORT == 0) {
    if( MTA_KERNEL_COUT ) cout << "SendQuery: Bad Mail" << endl;
    return Mail(ml.To(), ml.From(), ml.MsgType(), MailFlags::BadMessage); 
      // an error occurred, just abandon msg
  }

  if(mySocket.FD() < 0) { 
    mySocket = CSocket(ml.To().TCP.IP, ml.To().TCP.PORT);
  } 

  //  cout << "Sending Query Mail To " << ml.To().TCP.IP
  //       << ":" << ml.To().TCP.PORT << endl;
    
  if(mySocket.FD() < 0) {
    // error opening socket 
    // just return a "bad" message
    if( MTA_KERNEL_COUT ) cout << "SendQuery: Error Opening Socket" << endl;
    SendErrorCount++;
    return Mail(ml.To(), ml.From(), ml.MsgType(), MailFlags::BadMessage); // reverse
  } else {
    
    // send the key to verify the message
    int myKey = DEF::ReceiveKey;
    mySocket.BWrite((void*) &myKey, sizeof(myKey));
    // now send the message data
    ml.ToFile(mySocket);
    
    // now read back the response

    // read the key to verify the message
    myKey = 0;
    mySocket.BRead((void*) &myKey, sizeof(myKey));
    //        cout << "SendQuery myKey = " << myKey << endl;
    if( myKey != DEF::ReceiveKey ) {
      // error - bad message
      // just return a "bad" message
      SendErrorCount++;
      return Mail(ml.To(), ml.From(), ml.MsgType(), MailFlags::BadMessage); // reverse
    } else {
      // let the mailer download the message
      //      cout << "Response Of Query ..." ;
      return Mail(mySocket);
    }
  }
  
  // should never get here but just in case
  SendErrorCount++;
  return Mail();
}


void Kernel::DeliverNextMessage() {
  Mail m;
  if ( true ) {
    boost::recursive_mutex::scoped_lock sl(M_OutboxLock);
    if( M_Outbox.size() > 0) {
      m = M_Outbox.front();
      M_Outbox.pop_front();
    }
  }

  if ( m.OK() ) {
    DeliverMail(m);
  }
}


void Kernel::DeliverMail(Mail &ml, File f) {

  //    sendcount ++;
  //    if( sendcount % 500 == 0) cout << "Send count = " << sendcount << endl;
  if( ml.To().MAILBOX < 0 ||
      ml.To().TCP.IP == 0 ||
      ml.To().TCP.PORT == 0) {
    if( MTA_KERNEL_COUT ) cout << "DeliverMail: Bad Mail Destination" << endl;
    return; // an error occurred, just abandon msg
  }
  
  // if fd<0 open a socket
  if( f.FD() < 0) {
    f = CSocket (ml.To().TCP.IP, ml.To().TCP.PORT);
  }
  
  // if still < 0 just give up
  if( f.FD() < 0) {
    if( MTA_KERNEL_COUT ) cout << "Socket Open Error - trying to connect to " 
	 << ml.To().TCP.IP << ":" << ml.To().TCP.PORT << endl;
    SendErrorCount++;
    return;
  }
  
  //  cout << "Sending Mail To " << ml.To().TCP.IP
  //       << ":" << ml.To().TCP.PORT 
  //       << "[" << ml.To().MAILBOX << "]." << endl;
  
  // send the key to verify the message
  int myKey = DEF::ReceiveKey;
  f.BWrite((void*) &myKey, sizeof(myKey));

  // and let the mail object send its data
  ml.ToFile(f);
  
}




  


