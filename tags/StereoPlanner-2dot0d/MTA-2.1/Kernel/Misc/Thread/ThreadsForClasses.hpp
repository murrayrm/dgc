#ifndef ThreadsForClasses_HH
#define ThreadsForClasses_HH


#include <iostream>
#include <string.h>

#include "ThreadManager.hh"

using namespace Thread;

// ---------------- Prototypes --------------------------
// ---------------- Prototypes --------------------------
// ---------------- Prototypes --------------------------


// Calls T::m on an existing class that will exist for the
// entire time the thread will be executing
template<class T>
void RunMethodInNewThreadLoader( void* arg );

template<class T>
void RunMethodInNewThread(T *c, void (T::*m)() );

// Copies the class before calling the class function
template<class T>
void CopyObjectAndRunMethodInNewThreadLoader( void* arg );

template<class T>
void CopyObjectAndRunMethodInNewThread(T *c, void (T::*m)() );


// ------------- Implementation --------------------------
// ------------- Implementation --------------------------
// ------------- Implementation --------------------------


template<class T>
void RunMethodInNewThreadLoader( void* arg ) {

  T* cp;          // class pointer
  void (T::*mp)();// method pointer

  memcpy ( &cp, arg,                     sizeof(cp));
  memcpy ( &mp, (char*)arg + sizeof(cp), sizeof(mp));

  (cp->*mp)();
  delete (char*) arg;
}

template <class T>
void RunMethodInNewThread(T *c, void(T::*m)() ) {
  
  ThreadReconstructPointers toEnqueue;
  toEnqueue.p_fun = &(RunMethodInNewThreadLoader<T>);
  toEnqueue.p_arg = (void*) new char[ sizeof(c) + sizeof(m) ]; 

  memcpy ( toEnqueue.p_arg,                    &c, sizeof(c));
  memcpy ( (char*)toEnqueue.p_arg + sizeof(c), &m, sizeof(m));

  EnqueueFunctionToExecute(toEnqueue);

}


// Enqueue the class in the thread loader
template <class T>
void CopyObjectAndRunMethodInNewThread(T *c, void(T::*m)() ) {

  // first, copy the class to a new instance
  c = new T(*c);
  
  // and the rest is the same as above
  ThreadReconstructPointers toEnqueue;
  // except we must call the loader that will delete the object
  // after it is finished executing
  toEnqueue.p_fun = &(CopyObjectAndRunMethodInNewThreadLoader<T>);


  toEnqueue.p_arg = (void*) new char[ sizeof(c) + sizeof(m) ]; 

  memcpy ( toEnqueue.p_arg,                    &c, sizeof(c));
  memcpy ( (char*)toEnqueue.p_arg + sizeof(c), &m, sizeof(m));

  EnqueueFunctionToExecute(toEnqueue);

}

template<class T>
void CopyObjectAndRunMethodInNewThreadLoader( void* arg ) {

  T* cp;          // class pointer
  void (T::*mp)();// method pointer

  memcpy ( &cp, arg,                     sizeof(cp));
  memcpy ( &mp, (char*)arg + sizeof(cp), sizeof(mp));

  (cp->*mp)();
  delete (char*) arg;
  delete cp;  // deletes the class that was created
}


#endif
