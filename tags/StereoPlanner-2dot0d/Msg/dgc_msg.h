#ifndef dgc_msg_H
#define dgc_msg_H


#include "dgc_messages.h"

/* Any Messages - This function returns true (nonzero) if there is a message
   downloaded for this system. With one argument, it will check if there is a
   message of that type.  With 2 arguments, it will check if there is a
   message in the applicable range (doubly inclusive).  FALSE (zero) will be
   returned if there are no messages, and ERROR (-1) will be returned if there
   was an error with the request. */
int dgc_AnyMessages(int MessageStartType=0, int MessageEndType=0);




/* Poll Message - If there is a message available in the message buffer,
   this will check if a message of apropriate type is available, and return
   the message header for it.  If PollMessage is called with no arguments,
   then the first message header in the buffer will be returned.  If there is
   no message available of appropriate type or an error occured, MsgID will be zero. */
int dgc_PollMessage(dgc_MsgHeader &, int MessageStartType=0, int MessageEndType=0); 


/* Remove Message - If there is a message available in the message buffer
   that has some sort of error, just let the MTA remove it. */
int dgc_RemoveMessage(dgc_MsgHeader & msgh ); 



/* Get Message - Grabs the message matching the header by copying the data 
   into the supplied buffer location. It then DELETES the
   message from the message transport layer.  The function will return true
   if the function was successful or ERROR (-1) if it failed. */
int dgc_GetMessage(dgc_MsgHeader &msgh, void * buffer);


/* Get Last Message - Grabs the last message matching the type in the MTA.
   It deletes all previous messages of the same type and this one after the
   message is copied.  The function will return true
   if the function was successful, FALSE if no messages were available, 
   or ERROR (-1) if it failed. */
int dgc_GetLastMessage(void * buffer, int min=0, int max=0);



/* Send Message - Sends a message.  Msg Size is from the header, msgID and source are
   automatically filled in by the transport layer, so whatever is passed is ignored.
   Flags are defined in dgc_messages.h.  Error = -1, success = 1 */
int dgc_SendMessage(dgc_MsgHeader &, void * msgData, int flags=0);







int dgc_msg_Register(int systype);
int dgc_msg_Unregister(); 


double SystemTime();







#endif
