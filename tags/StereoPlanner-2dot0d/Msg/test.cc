#include <iostream>
#include <stdio.h>

#define Log test(__FILE__)(5);

using namespace std;

namespace testing {
  int x;

}

namespace testing {
  int y;
}

class test {

public:
  int x;

  test () : x(4) {}
  test (int y) {}
  test (char * y) { cout << y << endl;}

  
  ~test() { cout << "destructor " << endl; }

  void operator()(int y) {
    x+=y;
  }

  void operator() (char * s) { 
    string str = s;
    cout << "string: " << str << endl; 
  }

private:

};

int main () {

  test myTest;

  char myString[] = "Testing myString";
  char *p_myString = myString;

  myTest(p_myString);

  Log;
  cout << myTest.x << " File = " << __FILE__ << " Line = " << __LINE__ << endl;

  return 0;

};
