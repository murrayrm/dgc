#ifndef dgc_mta_main_H
#define dgc_mta_main_H

#include <sys/socket.h>
#include <time.h>
#include "dgc_time.h"

class ServerPortWatcher 
{
 public:
  ServerPortWatcher(int port) : myPort(port), myPortFD(0) {}
  ~ServerPortWatcher();
  void operator ()();
  
 private:
  int myPort;
  int myPortFD;
};

class ServerPortHandler 
{
 public:
  ServerPortHandler(int sd, sockaddr sa); /*  : 
    sd(sd), sa(sa), in_msgSize(0), out_msgSize(0), in_msgPtr(NULL), 
    out_msgPtr(NULL) { } */
  ~ServerPortHandler();
  void operator ()();


private:
  int sd;
  sockaddr sa;
  int in_msgSize;
  char * in_msgPtr;

  int out_msgSize;
  char * out_msgPtr;
  timeval before, after;
};










#endif
