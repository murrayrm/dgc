/* TLL.cc - reading the brake force sensor through parallel interface
           - reading the linear pot on the brake actuator for act position
   Revision History: 7/17/2003  Created                      Sue Ann Hong
                     7/18/2003  Finished ver.1               Sue Ann Hong
		     8/17/2003  Added linear pot inteface    Sue Ann Hong
 Note: accuracy ~.005 (for ratio)
       max 1446-1456
 */

#include <cstdlib>
#include <unistd.h>
//#include <cstdio>
#include <iostream>
//#include "../team/parallel/parallel.h"
//#include "moveCar/accel.h"
//#include "moveCar/TLL.h"
#include "../parallel/parallel.h"        // compile with ../parallel/parallel.c
#include "accel.h"
#include "TLL.h"

using namespace std;

// remember the parallel port is PP_BTENS (or something like that)

// don't need to call this if init_accel() is called. in fact, one should
// always use init_accel().
int initBrakeSensors() {
  return pp_init(tllpport, PP_DIR_IN);  /* data lines: read */
}

double getBrakeForce() {
  pp_set_bits(tllpport, ACCEL_CS, 0);     // make sure accel doesn't move
  int tllOutput;
  /* Set ALE, START, OE */
  pp_set_bits(tllpport, BT_ALE, 0);
  pp_set_bits(tllpport, BT_START, 0);
  //usleep(1);
  pp_set_bits(tllpport, 0, BT_ALE);
  pp_set_bits(tllpport, 0, BT_START);
  usleep(100);
  pp_set_bits(tllpport, BT_OE, 0);
  double force = (double) pp_get_data(tllpport); 
  pp_set_bits(tllpport, 0, BT_OE);

  /* Get "voltage" */
  // A fast change in the force -- what happens?
  //  double force = (double) pp_get_data(tllpport); 
  cout << "voltage div #: " << force << endl;
  cout << "voltage: " << force/BT_NUM_DIV*(BT_VREF_HIGH-BT_VREF_LOW)+BT_VREF_LOW << endl;
  /* Convert to lbs */
  return ((force-7) / BT_NUM_DIV * (BT_VREF_HIGH - BT_VREF_LOW) + BT_VREF_LOW) * BT_MAX_FORCE / BT_MAX_OUTPUT;
  // return ((force / BT_NUM_DIV * (BT_VREF_HIGH - BT_VREF_LOW) + BT_VREF_LOW) + BT_CORR_FACTOR) * BT_MAX_FORCE / BT_MAX_OUTPUT;
}

/* getBrakePot
  Return value: (double) position of the brake actuator between 0 and 1
*/
double getBrakePot() {
  pp_set_bits(tllpport, ACCEL_CS, 0);     // make sure accel doesn't move
  pp_set_bits(tllpport, 0, BT_OE);        // make sure pot is the only input 
  pp_set_bits(tllpport, 0, BPOT_RC);
  pp_set_bits(tllpport, 0, BPOT_CS);
  usleep(1); // <65ns
  pp_set_bits(tllpport, BPOT_CS, 0);
  usleep(BPOT_TBUSY);    
  pp_set_bits(tllpport, BPOT_RC, 0);
  pp_set_bits(tllpport, 0, BPOT_CS);
  usleep(1); // <83ns
  // read 
  int lowerEight = (int) pp_get_data(tllpport);
  int upperFour = (int) pp_get_bit(tllpport, BPOT_D8) 
    + (int) (pp_get_bit(tllpport, BPOT_D9)<<1)
    + (int) (pp_get_bit(tllpport, BPOT_D10)<<2);
  int sign = (int) pp_get_bit(tllpport, BPOT_D11);
  pp_set_bits(tllpport, BPOT_CS, 0);  
  int potReading = lowerEight + (upperFour<<8);

  if (sign == 1) {  // negative probably means zero
    cout << "WARNING: how can the wiper voltage be NEGATIVE???" << endl;
    cout << "Voltage: " << (potReading*(BP_VREF_HIGH-BP_VREF_LOW)/BP_NUM_DIV + BP_VREF_LOW) << endl;
    cout << "I'm going to assume that it's zero on return." << endl;
    return 0;
    // return -1;
  }  
  else if (sign == 0) {
    //    cout << "D0-D11: " << potReading << endl;
    //cout << "Voltage: " << (potReading*(BP_VREF_HIGH-BP_VREF_LOW)/BP_NUM_DIV + BP_VREF_LOW) << endl;
    // return (potReading*(BP_VREF_HIGH-BP_VREF_LOW)/BP_NUM_DIV + BP_VREF_LOW)/POT_VHIGH;
    //cout << potReading/BP_NUM_HIGH << endl;
    return (potReading/BP_NUM_HIGH - BP_MIN)/(BP_MAX-BP_MIN);
 }
  else {
    cout << "If you're reading this, sign != 0 or 1. Just not possible..." << endl;
    return -1;
  }
}
