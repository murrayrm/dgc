#include <string>
#include <iostream>
#include <fstream>
#include <map>

#define PRINT_DEBUG_MESSAGES 0

class LookUpTable {
 public:
  /* Constructors */
  LookUpTable();
  LookUpTable(std::string filename);

  /* Destructors */
  ~LookUpTable();

  void loadTable(std::string filename);

  double getValue(double key);

 private:
  typedef std::map<double, double> TableType;

  TableType dataTable;
  double min_key;
  double max_key ;

};
