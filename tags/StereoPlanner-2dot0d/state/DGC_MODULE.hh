#ifndef DGC_MODULE_HH
#define DGC_MODULE_HH

/* TODO:  Set the constants below for your module. */ 

// How many serial and parallel ports
#define MODULE_NAME State
// same thing in quotes (yes i know this is stupid).
#define STR_MOD_NAME "State"
// The name of the executable in quotes
#define STR_EXEC_NAME "statemain"
// MODULE_NUMBERs are defined in Msg/dgc_messages.h
// USE THE CONSTANT: DO NOT put a number in here.
#define MODULE_NUMBER dgcs_StateDriver
#define NUM_SERIAL_PORTS 2
#define NUM_PARALLEL_PORTS 0

// now rename your main() function to MODULE_NAME::Run()
// literally (do not substitute the name of your module)

/* End of TODO: You are done editing this file. 
 * *************************************************** */

#include "Msg/DGC_MODULE.ht"

#endif
