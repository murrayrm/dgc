// rotation matrix class
// Uses Matrix class to keep track of a rotation matrix
//  Operates using a z-y-x transformation
// pitch, roll, and heading indicated rotations around the y, x, and z rotations respectively
// the matrix generated is by default a body to nav transforomation

#ifndef ROT_MAT2_HH
#define ROT_MAT2_HH

#include "Matrix.hh"

/* XYZcoord: struct defined for 3-D coordinate points.
 */
struct XYZcoord {
  double x;
  double y;
  double z;
  // Constructors
  XYZcoord() {}
  XYZcoord(double x_new, double y_new, double z_new)
    : x(x_new), y(y_new), z(z_new) {}
  // Copy Constructor
  XYZcoord(const XYZcoord &other) { 
    x = other.x;
    y = other.y;
    z = other.z;
  }
};


class rot_matrix
{
private:
  Matrix rmatrix;
  double pitch, roll, heading;

  // private accessor to grab just the matrix
  Matrix get_matrix() const;

  // private generator of a rotation matrix from angles
  Matrix make_b2n(double p, double r, double h) const;

  // sets the internal matrix
  void set_matrix(const Matrix newmat);

  // generate a matrix from a rotation vector
  Matrix Matrix_drot(double dx, double dy, double dz);

public:
  // Constructors
  rot_matrix(); // creats identity transformation
  rot_matrix(double p, double r, double h); // creates matrix for a p,r,h transformation
  rot_matrix(const rot_matrix &rot2); // copy constructor

  // mutators
  void setpitch(double p); // set the pitch to a certain angle
  void setroll(double r); // set the roll to a certain angle
  void setheading(double h); // set the heading to a certain angle
  void setattitude(double p, double r, double h); // set all three attitudes at same timex
  rot_matrix& propagate(double dx, double dy, double dz); // propagates rotation matrix by a rotation (dx, dy, dz)
  rot_matrix make_drot(double dx, double dy, double dz); // make a rotation matrix given a vector rotation 

  void set_b2n_attitude(double p, double r, double h); // generates a body to nav rotation matrix based on prh

  // accessors
  // returns euler angles associated with this rotation matrix
  double get_pitch() const;
  double get_roll() const;
  double get_heading() const;

  // get element 
  double get_element(int row, int col) const;

  // displays matrix and angles to screen
  void display() const;

  // operators 
  // multiply by another rotation matrix
  rot_matrix operator*(const rot_matrix &matrix2) const; 
  rot_matrix& operator*=(const rot_matrix &matrix2);

  // set one matrix equal to another 
  rot_matrix& operator=(const rot_matrix &matrix2);

  // check rotation matrix equality
  bool operator==(const rot_matrix &matrix2) const;


  // rotate a coordinate to generate a new coordinate
  XYZcoord operator*(const XYZcoord point) const;  

  // destructor
  ~rot_matrix();
};

#endif
