#!/bin/csh
# 
# vdrive.sh - startup script for vdrive
# RMM, 27 Feb 04
#
# This script starts up vdrive on titanium and restarts it if it exits
# unexpectedly.  This contains an absolute path to vdrive so that it
# can be run from /etc/rc.d.
#
# The script tries to be smart about whether it is running on a terminal
# or not and doesn't request input unless it is running on a terminal
#
# Usage:
#   vdrive flags [&]
#
# Environment variables:
#   DGC		path to DGC executables
#   VDFLAGS	optional flags to pass to vdrive
#

# Set up process limits to allow core files to be generated
limit coredumpsize unlimited

# Make sure we have a path to the file
if (! $?DGC) set DGC=`pwd`
if (! $?VDFLAGS) set VDFLAGS="-v"
if (! -e $DGC/vdrive) then
  echo "FATAL: can't find vdrive in $DGC"
  exit
endif

# Print out a banner so that we know where we are running
echo "vdrive@`hostname`: pwd = `pwd`; VDFLAGS = $VDFLAGS";

# Check to make sure we are on the right machine
if (`hostname` != "titanium") then
  echo "WARNING: not running on titanium"
  if ($tty != "") then
      echo -n "Continue (y/n)? ";
      if ($< != "y") exit
  endif
endif

# Check to make sure that vdrive is not already running
set vdlist = "`ps -aux | grep vdrive | grep -v vdrive.sh`"
if ("$vdlist" != "") then
  echo "WARNING: vdrive already running on this machine:"
  echo "  $vdlist"
  if ($tty != "") then
      echo -n "Kill/Continue/Abort (k/c/a)? ";
      set response = $<;
      if ($response == "a") exit
  endif

  # Kill all running copies of vdrive
  if ($response == "k") then
    if (`killall -9 vdrive` != "") then
      echo "WARNING: couldn't kill all copies of vdrive"
      if ($tty != "") then
        echo -n "Continue (y/n)? ";
        if ($< != "y") exit
      endif
    endif
  endif
endif

# Run vdrive and restart it whenever it crashes
while (1) 
  echo "======== starting vdrive $VDFLAGS $argv ========"
  $DGC/vdrive $VDFLAGS $argv
  echo "vdrive aborted"

  # kill anything that is left over
  killall -9 vdrive >& /dev/null

  # restart immediately
end
