#include "MatlabDisplay.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

// Added for display 2/24/04 
VState_GetStateMsg dispSS;
double dispSS_decimaltime;

double PitchDeg, RollDeg, YawDeg;
double PitchRateDeg, RollRateDeg, YawRateDeg;

double steerCommand;
double speedCommand;

int UpdateCount   = 0;

extern int QUIT_PRESSED;
extern int PAUSED;
extern int DRAW_FIGURE;
extern int CLEAR_FIGURE;

extern MatlabDisplayDatum _d;

#include "vddtable.h"
int user_quit(long arg);
int pause_figure(long arg);
int unpause_figure(long arg);
int draw_figure_toggle(long arg);
int clear_figure(long arg);

void MatlabDisplay::UpdateSparrowVariablesLoop() 
{
  while( !ShuttingDown() ) {

    // this is the same as in the arbiter display 
    dispSS = _d.SS;
    dispSS_decimaltime = (_d.SS.Timestamp.sec()+_d.SS.Timestamp.usec()/1000000.0);

    PitchDeg     = _d.SS.Pitch     * 180.0 /M_PI;
    RollDeg      = _d.SS.Roll      * 180.0 /M_PI;
    YawDeg       = _d.SS.Yaw       * 180.0 /M_PI;
    PitchRateDeg = _d.SS.PitchRate * 180.0 /M_PI;
    RollRateDeg  = _d.SS.RollRate  * 180.0 /M_PI;
    YawRateDeg   = _d.SS.YawRate   * 180.0 /M_PI;

    steerCommand = _d.arbiter_cmd.steer_cmd;
    speedCommand = _d.arbiter_cmd.velocity_cmd;
    
    UpdateCount ++;
    
    usleep(500000); // Wait a bit, because other threads will print some stuff out
  }

}

void MatlabDisplay::SparrowDisplayLoop() 
{
  dbg_all = 0;

  if (dd_open() < 0) exit(1);

  dd_bindkey('Q', user_quit);
  dd_bindkey('P', pause_figure);
  dd_bindkey('U', unpause_figure);
  dd_bindkey('D', draw_figure_toggle); // only one operational so far
  dd_bindkey('C', clear_figure);

  dd_usetbl(vddtable);

  usleep(500000); // Wait a bit, because other threads will print some stuff out
  dd_loop();
  dd_close();
  QUIT_PRESSED = 1; // Following LADARMapper's lead
}

// TODO: Set the shutdown flag in the quit function
int user_quit(long arg)
{
  return DD_EXIT_LOOP;
}

// Pause the updating of the figure
int pause_figure(long arg)
{
  PAUSED = 1;
}

// Unpause the updating of the figure
int unpause_figure(long arg)
{
  PAUSED = 0;
}

// Clear the path traces on the figure
int draw_figure_toggle(long arg)
{
  if( DRAW_FIGURE ) {
    DRAW_FIGURE = 0;
  }
  else {
    DRAW_FIGURE = 1;
  }
}

// Clear the path traces on the figure
int clear_figure(long arg)
{
  CLEAR_FIGURE = 1;
}

