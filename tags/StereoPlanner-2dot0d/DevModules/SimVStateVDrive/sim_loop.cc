/***************************************************************************************/
//FILE: 	sim_loop.cc
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file has the loop that will run in one of the threads
//
/***************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>
#include <algorithm>

#include "Sim.hh"
#include "latlong/ggis.h"
#include "latlong/contest-info.h"
#include "Constants.h"
#include "simulator.hh"

// Yaw uncertainty in percentage of the real one, 0.1 = 10%
#define YAW_UNCERTAINTY 0.05
// Position Uncertainty between 0 and 30cm (0.6 = 30cm)
#define POSITION_UNCERTAINTY 0.6

using namespace std;

simData simState;
Simulator simEngine;
double t, accel, steer;
int seed;

double TimeGain = 1.0;

extern SIM_DATUM d;

/* Create a vehstate object to use for cruise */
#include "vehlib-2.0/VState.hh"		/* grab message format for VState */
struct VState_GetStateMsg vehstate;	/* current vehicle state */

void SimVState::SimInit() {

  // Initializing
  // TODO: Initialize variables.
  t = 0;
  accel = 0;
  steer = 0;
  seed = 0;

  // Updating initial state
  simState.x = SIMULATOR_INITIAL_EASTING;
  simState.y = SIMULATOR_INITIAL_NORTHING;
  simState.vel = 0;
  simState.n_vel = 0;
  simState.e_vel = 0;
  simState.u_vel = 0;
  simState.yaw = SIMULATOR_INITIAL_YAW;
  simEngine.initState(simState);
}
  
void SimVState::SimUpdate(float deltaT) {
  // Generating randomic numbers
  double deltaYaw, deltaPosition;
  seed++;
  srand(seed);
  deltaYaw = ( (double)rand() / (double)(RAND_MAX+1) );
  seed++;
  srand(seed);
  deltaPosition = ( (double)rand() / (double)(RAND_MAX+1) );

  // Step
  deltaT = deltaT * TimeGain;
  simEngine.setStep(deltaT);

  // Getting the accel and steer comands
  vehstate.Speed = d.SS.Speed;
  vehstate.Accel = d.Accel;
  accel = cruise( d.cmd.velocity_cmd);
  steer = d.cmd.steer_cmd;

  // Adding commands to the simulator
  simEngine.setSim(t,accel,steer);

  // Simulating
  simEngine.simulate();
  simState = simEngine.getState();

  //Updating state
  d.SS.Northing = simState.y + deltaPosition*POSITION_UNCERTAINTY + POSITION_UNCERTAINTY/2; // noise
  d.SS.Easting  = simState.x + deltaPosition*POSITION_UNCERTAINTY + POSITION_UNCERTAINTY/2; // noise
  d.SS.Speed    = simState.vel;
  d.SS.Vel_N    = simState.n_vel;
  d.SS.Vel_E    = simState.e_vel;
  d.SS.Vel_U    = simState.u_vel;
  d.SS.Yaw    = simState.yaw*(1 + deltaYaw*YAW_UNCERTAINTY); // noise
  d.SS.Pitch = 0;
  d.SS.Roll = 0;
  // Updating commands
  d.Accel = accel;
  d.Steer = simState.phi/SIM_MAX_STEER;

  // Normalize angle to be between [-pi,pi] and
  d.SS.Yaw = atan2( sin(d.SS.Yaw), cos(d.SS.Yaw) );

  // Other updates
  d.SS.Altitude = 0;
  d.SS.Timestamp = Now();

  // Stop the velocity if negligible
  if( fabs( d.SS.Vel_N )  < 0.01 ) {
    d.SS.Vel_N = 0.0;
  }
  if( fabs( d.SS.Vel_E )  < 0.01 ) {
    d.SS.Vel_E = 0.0;
  }

  t += deltaT;
} // end SimLoop()
