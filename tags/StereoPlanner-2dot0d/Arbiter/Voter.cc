/* Voter.cc
   Author: J.D. Salazar
   Created: 8-25-03
   Revision History:  11/10/2003  Sue Ann Hong  Added updateArcs,
                                                modified constructor

   A class representing something that evaluates steering arcs for the Arbiter 
   to be used by Team Caltech in the 2004 Darpa Grand Challenge */

#include "Voter.h"
#include "Vote.h"
#include <algorithm>  // For 'std::max'
#include <utility>

// DEBUG
#include <iostream>
using namespace std;


void Voter::updateArcs(double leftPhi, double rightPhi, int numArcs) {
  double increment;

  cout << "Starting updateArcs..." << endl;

  leftPhi_ = leftPhi;
  rightPhi_ = rightPhi;
  numberOfArcs_ = std::max( numArcs, 1 );
  // Should add some kind of error if rightPhi = leftPhi
  //   and numberOfArcs > 1
  if ( numberOfArcs_ > 1 && rightPhi_ == leftPhi_)
    // Error - for now, cout
   cout << "rightPhi = leftPhi, numArcs should not be greater than 1." << endl;
  if ( rightPhi_ < leftPhi_)
    // Error - for now, cout
    cout << "Error: rightPhi < leftPhi"  << endl;

  // Fill 'votes_' structure w/ angles and zero velo, zero goodness
  if( numberOfArcs_ > 1 && rightPhi_ != leftPhi_ )
    increment = (rightPhi_ - leftPhi_) / ((double)numberOfArcs_ - 1);
  else
    increment = 1.0;

  cout << "leftPhi: " << leftPhi_ << endl;
  cout << "rightPhi: " << rightPhi_ << endl;
  cout << "numArcs: " << numberOfArcs_ << endl;

  votes_.clear();
  double i;
  for( i = leftPhi_; i <= rightPhi_; i += increment )
    {
      //      cout << "updateArcs: " << i << endl;
      Vote v(i);
      votes_.push_back(v);
    }

  cout << "Voter::updateArcs: done" << endl;
}


Voter::voteListType Voter::getVotes() {
  return votes_;
}


void Voter::updateVotes(voteListType &votes, double timestamp) {
  int i = 0;

  // Update votes_  -- CURRENTLY WRITES OVER THE EXISTING votes_
  votes_.clear();
  //  voteListType::iterator iter = votes_.begin();
  for (voteListType::iterator other = votes.begin(); other != votes.end();
       ++other) {
    //++iter;
    //if (other->phi == iter->phi) {
      Vote v(other->phi, other->goodness, other->velo);
      votes_.push_back(v);
      i++;
    /*    }
    else {
      cout << "--Voter::updateVotes--why don't the phi's match?" << endl;
      cout << "votes_: " << iter->phi << endl;
      cout << "other: " << other->phi << endl;
    }
    */
  }

  if (i > 0) {
    time_ = timestamp;
  }
  //  cout << "--Voter::updateVotes--done--" << endl;
}

void Voter::printVotes() {
  cout << "--Voter::printVotes--" << endl;
  for(voteListType::iterator i = votes_.begin(); i != votes_.end(); ++i) {
    cout << "(Phi, Velo, Good): " << "(" << i->phi << ", " 
         << i->velo << ", " << i->goodness << ")" << endl;
  }
}
