#ifndef __RANDOM_VOTER_H__
#define __RANDOM_VOTER_H__
/* RandomVoter.h
   Author: J.D. Salazar
   Created: 9-216-03

   A sample class derived from Voter which generates weights given to steering
   arcs randomly. */

#include "Voter.h"

class RandomVoter : public Voter
{
 public:
  // Constructor
  RandomVoter(unsigned int numberOfArcs = DEFAULT_NUMBER_OF_ARCS,
	      unsigned int maxVote = DEFAULT_MAX_VOTE,
	      double leftPhi = DEFAULT_LEFT_PHI,
	      double rightPhi = DEFAULT_RIGHT_PHI);


  // The guts of any Voter class.  'getVotes' must figure out the vote values
  // for each voting arc and put them in the votes_ structure which it then
  // returns.  (In the future it may be necessary to create a seperate
  // computeVotes() function...).  'getVotes' must also change the timestamp
  // to the data that the votes computed are being based on.  In RandomVoter,
  // the votes are given random weights.
  virtual voteListType getVotes();

 private:
// Reliably create a random number.
 int rnd(int max) { return int(drand48() * max); }
};

#endif
