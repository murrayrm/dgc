% Another jump finding algorithm
% we have a heading vector for each gps point from the velocity, so use
% this to compare with the vector of the switch from one gps point to the
% next and if there's a big difference between the heading and that vector
% than register it as a jump

% find the difference vectors from one to the next of each position point
dn = [gpsnorthing(2:end); gpsnorthing(end)] - gpsnorthing;
de = [gpseasting(2:end); gpseasting(end)] - gpseasting;

% get normalized vectors
mag_delta = sqrt(dn .* dn + de .* de);
dn = dn ./ mag_delta;
de = de ./ mag_delta;

mag_vel = sqrt(gpsve .* gpsve + gpsvn .* gpsvn);
vn = gpsvn ./ mag_vel;
ve = gpsve ./ mag_vel;

% now take the dot products and get the angles
dot = vn .* dn + ve .* de;
angle = acos(dot);

% now plot the points, noting the jumps
figure;
hold on;

color = 0;

for i = 2:length(angle)
    if angle(i) >= pi/4
        color = color + 1;
        color = mod(color, 2);
    end
    if color == 1 
       plot(gpseasting(i), gpsnorthing(i), '.r')
   else
       plot(gpseasting(i), gpsnorthing(i), '.g')
   end
end

    