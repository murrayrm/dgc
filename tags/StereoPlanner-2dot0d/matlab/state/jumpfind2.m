% find jumps/changes

color = 1;
figure;
hold on

for i= 2:1:length(gpsmode)
    if (gpsmode(i) ~= gpsmode(i - 1)) ||( gps_ext_mode(i) ~= gps_ext_mode(i - 1)) || (gpsvalid(i) ~= gpsvalid(i-1))
        if color == 1
            color = 2;
        else
            color = 1;
        end
    end
    
    % plot with different colors
   if color == 1
        plot(gpseasting(i), gpsnorthing(i), '.b')
    else
        plot(gpseasting(i), gpsnorthing(i), '.r')
    end
end

plot(gpseasting, gpsnorthing, 'k')
            
