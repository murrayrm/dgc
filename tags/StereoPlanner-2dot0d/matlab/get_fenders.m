function points = get_fenders(image_file, xref, yref, img_origin)
% this function returns fender points from a 16-bit bitmap where the fender
% points are green.  xref and yref are the size of the x,y reference lines
% used by fplot_corridor and img_origin is the origin returned by
% fplot_fender.  Note, the image MUST be 16-color bitmap or the color finding
% will not work.  

% read in image file and find all the green points
[image, colors] = imread(image_file);
[points_y, points_x] = find(image == 10);

% change references from top left to bottom left 
points_y = length(image(:,1)) - points_y;

% find all the blue points for the reference mark
[by, bx] = find(image==12);
by = length(image(:,1)) - by;

% set the origin to the origin of the reference mark
origin = [min(bx) min(by)];
points_y = points_y - origin(2);
points_x = points_x - origin(1);
by = by - origin(2);
bx = bx - origin(1);

% use the reference mark to scale the coordinates
x_meters_per_pixel = xref / (max(bx) - min(bx)+1);
y_meters_per_pixel = yref / (max(by) - min(by)+1);

% transform the coordinates of the mark from pixels to meters from the
% origin

points_x = points_x * x_meters_per_pixel + img_origin(1);
points_y = points_y * y_meters_per_pixel + img_origin(2);

points = [points_x points_y];

% output to file
% all points are weighted equally
weights = ones(length(points_x), 1);
outpoints = [points_x points_y weights];
dlmwrite('artificial_fenderpoints.dat', outpoints, '\t');
