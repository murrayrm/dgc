load statelog.dat;
kflat = statelog(:,13);
kflng = statelog(:,14);

% go down until we find that the KF has started using GPS locations to
% generate data
i = 1;
while kflat(i) == 0
    i = i +1;
end
kflat = kflat(i:end);
kflng = kflng(i:end);

%plot data 
figure(1)
hold on
plot(kflng, kflat, '.b')
plot(kflng, kflat, 'r')

%get gps data
load gpslog.dat;
gpslat = gpslog(:,4);
gpslng = gpslog(:,5);
i = 1;
while gps_north(i) == 0
    i = i +1;
end
gps_north = gps_north(i:end);
gps_east = gps_east(i:end);
figure(2)
hold on
plot(gps_east, gps_north, '.g')
plot(gps_east, gps_north, 'k')