#include "per_to_cla.h"

using namespace std;

//Serial map constructor, meant to take arguments from the output of the MapGen module
serialMap::serialMap(char map_type_arg, unsigned long map_size_arg, int map_time_arg, int num_rows_arg, int num_cols_arg, float map_x_res_arg, float map_y_res_arg, float map_z_res_arg, double rot_x_arg, double rot_y_arg, double rot_z_arg, double rot_s_arg, double tranRel_x_arg, double tranRel_y_arg, double tranRel_z_arg, double origin_x_arg, double origin_y_arg, double origin_z_arg, int* map_map_arg) {
	//Simply assign each argument to its appropriate variable
	map_type = map_type_arg;
	map_size = map_size_arg;
	num_rows = num_rows_arg;
	num_cols = num_cols_arg;
	map_x_res = map_x_res_arg;
	map_y_res = map_y_res_arg;
	map_z_res = map_z_res_arg;
	rot_x = rot_x_arg;
	rot_y = rot_y_arg;
	rot_z = rot_z_arg;
	rot_s = rot_s_arg;
	tranRel_x = tranRel_x_arg;
	tranRel_y = tranRel_y_arg;
	tranRel_z = tranRel_z_arg;
	origin_x = origin_x_arg;
	origin_y = origin_y_arg;
	origin_z = origin_z_arg;
	map_time = map_time_arg;

	//Allocate the right amount of memory for the map
	map_map = (int*) malloc(map_size*sizeof(int));
	//Copy map data from the argument to the variable
	memcpy(map_map, map_map_arg, map_size*sizeof(int));
};

//Default constructor
serialMap::serialMap() {
	map_map=NULL;
}

//Default destructor
serialMap::~serialMap() {

}

int serialMap::updateMap(char map_type_arg, unsigned long map_size_arg, int map_time_arg, int num_rows_arg, int num_cols_arg, float map_x_res_arg, float map_y_res_arg, float map_z_res_arg, double rot_x_arg, double rot_y_arg, double rot_z_arg, double rot_s_arg, double tranRel_x_arg, double tranRel_y_arg, double tranRel_z_arg, double origin_x_arg, double origin_y_arg, double origin_z_arg, int* map_map_arg) {
	//Simply assign each argument to its appropriate variable
	map_type = map_type_arg;
	map_size = map_size_arg;
	num_rows = num_rows_arg;
	num_cols = num_cols_arg;
	map_x_res = map_x_res_arg;
	map_y_res = map_y_res_arg;
	map_z_res = map_z_res_arg;
	rot_x = rot_x_arg;
	rot_y = rot_y_arg;
	rot_z = rot_z_arg;
	rot_s = rot_s_arg;
	tranRel_x = tranRel_x_arg;
	tranRel_y = tranRel_y_arg;
	tranRel_z = tranRel_z_arg;
	origin_x = origin_x_arg;
	origin_y = origin_y_arg;
	origin_z = origin_z_arg;
	map_time = map_time_arg;

	//Allocate the right amount of memory for the map
	map_map = (int*) malloc(map_size*sizeof(int));
	//Copy map data from the argument to the variable
	memcpy(map_map, map_map_arg, map_size*sizeof(int));

	return 0;
};


//Prints a serialMap, for debugging purposes
void serialMap::print_map() {
	int* bufferptr;
	bufferptr = map_map;

	cout << "\nmap_type: " << map_type;
	cout << "\nmap_size: " << map_size;
	cout << "\nmap_time: " << map_time;
	cout << "\nnum_rows: " << num_rows;
	cout << "\nnum_cols: " << num_cols;
	cout << "\nmap_x_res: " << map_x_res;
	cout << "\nmap_y_res: " << map_y_res;
	cout << "\nmap_z_res: " << map_z_res;
	cout << "\nrot_x: " << rot_x;
	cout << "\nrot_y: " << rot_y;
	cout << "\nrot_z: " << rot_z;
	cout << "\nrot_s: " << rot_s;
	cout << "\ntranRel_x: " << tranRel_x;
	cout << "\ntranRel_y: " << tranRel_y;
	cout << "\ntranRel_z: " << tranRel_z;
	cout << "\norigin_x: " << origin_x;
	cout << "\norigin_y: " << origin_y;
	cout << "\norigin_z: " << origin_z;

	cout << "\nMap Data: \n";
	for(unsigned long i=0; i<map_size; i++) {
		cout << *bufferptr++ << ", ";
	}
}


int serialMap::writeMap(char *filename) {
	void *mapptr;
	ofstream out_file(filename, ios::binary);
	int result=0;

	mapptr = this->serializeMap();

	//Make sure the file's available to write to
	if(out_file) {
		//Then write the serialized map and return
		out_file.write((char*)mapptr, map_size*sizeof(int)+MAP_HEADER_SIZE);
		out_file.close();
		result = 1;
	} else  {	
		//Return that there was an error
		cout << "\nError writing serialized map.";
	}

	free(mapptr);
	return result;
}

int serialMap::readMap(const char *filename) {
	void *mapptr;
	int result=0;

	ifstream in_file(filename, ios::binary);

	//Allocate the maximum memory for the serialized map
	mapptr = malloc(MAX_TOTAL_MAP_SIZE);


	//Make sure the file's available to read from
	if(in_file) {
	  //Read the file into the serialized map and return
	  in_file.read((char*)mapptr, MAX_TOTAL_MAP_SIZE);
	  in_file.close();
	  result = 1;
	  this->deserializeMap(mapptr);
	} else {
	  //Return that there was an error
	  cerr << "\nError reading serialized map " << filename << "." << endl;
	}



	free(mapptr);
	return result;
}

int serialMap::sendMap() {
	void *mapptr;
	int result=0;
	
	//Assign it to the mapptr
	mapptr = this->serializeMap();
	
/*  To be filled in when I know how to generate header stuff
	//Message Passing initialization
	dgc_MsgHeader header;

	header.Destination = ;
	header.MsgType = dgcm_SendingPerceptorMap;
	header.MsgSize = map.map_size*sizeof(int)+MAP_HEADER_SIZE+sizeof(dgc_MsgHeader);

	return SendMessage(header, mapptr, dgct_EthernetOrSerial);

*/
	//Free memory to prevent memory leaks
	free(mapptr);

	return result;
}


int serialMap::getMap() {
	int result=0;
	void* mapptr;
/*	dgc_MsgHeader header;	

	while(dgc_AnyMessages()!=dgcm_SendingPerceptorMap);
	
	header = dgc_PollMsg(dgcm_SendingPerceptorMap);

	result = GetMessage(header, mapptr);
*/
	this->deserializeMap(mapptr);

	free(mapptr);

	return result;
}

//Converts a serialMap into a serial data message
void* serialMap::serializeMap() {
	void *outptr, *bufferptr;

	//Allocate the right amount of memory for the total serialized map
	outptr = (char*)malloc(map_size*sizeof(int)+MAP_HEADER_SIZE);
	
	//Make the buffer point to the output pointer
	bufferptr=outptr;

	//Copy the header info to the buffer pointer's location
	*(char*)bufferptr=map_type;
	//Then increment by the size of the info copied
	bufferptr=(char*)bufferptr+sizeof(char);

	//Repeat for all fields in the class
	*(unsigned long*)bufferptr=map_size;
	bufferptr=(char*)bufferptr+sizeof(unsigned long);

	*(int*)bufferptr=map_time;
	bufferptr=(char*)bufferptr+sizeof(int);

	*(int*)bufferptr=num_rows;
	bufferptr=(char*)bufferptr+sizeof(int);

	*(int*)bufferptr=num_cols;
	bufferptr=(char*)bufferptr+sizeof(int);

	*(float*)bufferptr=map_x_res;
	bufferptr=(char*)bufferptr+sizeof(float);
	
	*(float*)bufferptr=map_y_res;
	bufferptr=(char*)bufferptr+sizeof(float);

	*(float*)bufferptr=map_z_res;
	bufferptr=(char*)bufferptr+sizeof(float);

	*(double*)bufferptr=rot_x;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=rot_y;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=rot_z;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=rot_s;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=tranRel_x;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=tranRel_y;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=tranRel_z;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=origin_x;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=origin_y;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=origin_z;
	bufferptr=(char*)bufferptr+sizeof(double);

	//Copy the map info to the buffer pointer's location
	memcpy(bufferptr, map_map, map_size*sizeof(int));

	return outptr;
}

//Writes a serial map to a specific location in memory
int serialMap::writeMemoryMap(void *outptr) {
	void *bufferptr;

	//Make the buffer point to the output pointer
	bufferptr=outptr;

	//Copy the header info to the buffer pointer's location
	*(char*)bufferptr=map_type;
	//Then increment by the size of the info copied
	bufferptr=(char*)bufferptr+sizeof(char);

	//Repeat for all fields in the class
	*(unsigned long*)bufferptr=map_size;
	bufferptr=(char*)bufferptr+sizeof(unsigned long);

	*(int*)bufferptr=map_time;
	bufferptr=(char*)bufferptr+sizeof(int);

	*(int*)bufferptr=num_rows;
	bufferptr=(char*)bufferptr+sizeof(int);

	*(int*)bufferptr=num_cols;
	bufferptr=(char*)bufferptr+sizeof(int);

	*(float*)bufferptr=map_x_res;
	bufferptr=(char*)bufferptr+sizeof(float);
	
	*(float*)bufferptr=map_y_res;
	bufferptr=(char*)bufferptr+sizeof(float);

	*(float*)bufferptr=map_z_res;
	bufferptr=(char*)bufferptr+sizeof(float);

	*(double*)bufferptr=rot_x;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=rot_y;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=rot_z;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=rot_s;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=tranRel_x;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=tranRel_y;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=tranRel_z;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=origin_x;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=origin_y;
	bufferptr=(char*)bufferptr+sizeof(double);

	*(double*)bufferptr=origin_z;
	bufferptr=(char*)bufferptr+sizeof(double);

	//Copy the map info to the buffer pointer's location
	memcpy(bufferptr, map_map, map_size*sizeof(int));

	return 0;
}


//Converts a serial data message into a serialMap, which it returns
void serialMap::deserializeMap(void* serializedMap) {
	char* ptr;
	ptr=(char*) serializedMap;
	//Assign the map field to the proper portion of the serial stream
	map_type = *(char*)serializedMap;
	//Increment the pointer to point to the next value
	serializedMap = (char*)serializedMap+sizeof(char);

	//Repeat for all fields
	map_size = *(unsigned long*)serializedMap;
	serializedMap = (char*)serializedMap+sizeof(unsigned long);

	map_time = *(int*)serializedMap;
	serializedMap = (char*)serializedMap+sizeof(int);

	num_rows = *(int*)serializedMap;
	serializedMap = (char*)serializedMap+sizeof(int);

	num_cols = *(int*)serializedMap;
	serializedMap = (char*)serializedMap+sizeof(int);

	map_x_res = *(float*)serializedMap;
	serializedMap = (char*)serializedMap+sizeof(float);

	map_y_res = *(float*)serializedMap;
	serializedMap = (char*)serializedMap+sizeof(float);

	map_z_res = *(float*)serializedMap;
	serializedMap = (char*)serializedMap+sizeof(float);

	rot_x = *(double*)serializedMap;
	serializedMap = (char*)serializedMap+sizeof(double);

	rot_y = *(double*)serializedMap;
	serializedMap = (char*)serializedMap+sizeof(double);

	rot_z = *(double*)serializedMap;
	serializedMap = (char*)serializedMap+sizeof(double);

	rot_s = *(double*)serializedMap;
	serializedMap = (char*)serializedMap+sizeof(double);

	tranRel_x = *(double*)serializedMap;
	serializedMap = (char*)serializedMap+sizeof(double);

	tranRel_y = *(double*)serializedMap;
	serializedMap = (char*)serializedMap+sizeof(double);

	tranRel_z = *(double*)serializedMap;
	serializedMap = (char*)serializedMap+sizeof(double);

	origin_x = *(double*)serializedMap;
	serializedMap = (char*)serializedMap+sizeof(double);

	origin_y = *(double*)serializedMap;
	serializedMap = (char*)serializedMap+sizeof(double);

	origin_z = *(double*)serializedMap;	
	serializedMap = (char*)serializedMap+sizeof(double);

	free(map_map);

	//Allocate the right amount of memory for the map data
	map_map = (int*) malloc(map_size*sizeof(int));	

	//Copy map data from the serializedMap to the outputMap
	memcpy(map_map, serializedMap, map_size*sizeof(int));
}

void makeClaratyMap(serialMap perMap, int num/*, claratyMap &claMap*/) {
	int leading_blank_cells=0, data_cells=0, elev_sign, i, j, goodness, confidence;
	//These indicate the weight each type of goodness data is given
	//They represent in order: elevation, classification, the existence of an obstacle, and the cell's roughness
	float elev_weight, class_weight, obs_weight, rough_weight;
	//These variables store the data gathered from each cell
	int elev_data, elev_conf_data, class_data, class_conf_data, obs_data, obs_conf_data, rough_data, rough_conf_data;
	//These variables store the actual goodness value, from 0 to 255, that each cell has
	int elev_val, class_val, obs_val, rough_val;
	//These indicate the weight each confidence is given
	//For now, they will be identical to the weight of the goodness data themselves, but they could be changed if there's a good reason
	float elev_conf_weight, class_conf_weight, obs_conf_weight, rough_conf_weight;
	//These variables tore the actual confidence value, from 0 to 255, that each cell has for a specific type of data
	int elev_conf_val, class_conf_val, obs_conf_val, rough_conf_val;

	unsigned char *r, *g, *b;
	r=(unsigned char*)malloc(perMap.num_cols*perMap.num_rows);
	g=(unsigned char*)malloc(perMap.num_cols*perMap.num_rows);
	b=(unsigned char*)malloc(perMap.num_cols*perMap.num_rows);

	//Set the data weights
	elev_weight = 0.25;
	class_weight = 0.25;
	obs_weight = 0.25;
	rough_weight = 0.25;

	//Set the confidence weights
	elev_conf_weight = elev_weight;
	class_conf_weight = class_weight;
	obs_conf_weight = obs_weight;
	rough_conf_weight = rough_weight;

	//Initialize the map pointer to point to the Perceptor Map
	int* mapptr = perMap.map_map;

	//Cycle through the rows
	for(i=0; i<perMap.num_rows; i++) {
		//The first element in each row is the number of leading "no data" cells
		leading_blank_cells = *mapptr;
		mapptr++;
		//The second element is the number of data cells
		//This results in (perMap.num_cols-leading_blank_cells-data_cells) trailing "no data" cells
		data_cells = *mapptr;
		mapptr++;

		//Cycle across the row setting the goodness of all the leading "no data" cells
		for(j=0; j<leading_blank_cells; j++) {
			goodness = DEFAULT_UNKNOWN_GOODNESS;
			confidence = DEFAULT_UNKNOWN_CERTAINTY;
			//claMap.set_cell(j, i, goodness, confidence);
			*(r+i*perMap.num_cols+j)=(unsigned char)goodness;
			*(g+i*perMap.num_cols+j)=(unsigned char)goodness;
			*(b+i*perMap.num_cols+j)=(unsigned char)goodness;
		}

		//Cycle across the row, setting the goodness of all the cells with data
		for(j=leading_blank_cells; j<leading_blank_cells+data_cells; j++) {
			//First, collect all the data:
			elev_data = (*mapptr & ELEVATION_DATA_MASK) >> ELEVATION_DATA_SHIFT;
			elev_sign = (*mapptr & ELEVATION_SIGN_MASK) >> ELEVATION_SIGN_SHIFT;
			elev_conf_data = (*mapptr & ELEVATION_CONFIDENCE_MASK) >> ELEVATION_CONFIDENCE_SHIFT;

			class_data = (*mapptr & CLASSIFICATION_DATA_MASK) >> CLASSIFICATION_DATA_SHIFT;
			class_conf_data = (*mapptr & CLASSIFICATION_CONFIDENCE_MASK) >> CLASSIFICATION_CONFIDENCE_SHIFT;

			obs_data = (*mapptr & OBJECT_DATA_MASK) >> OBJECT_DATA_SHIFT;
			obs_conf_data = (*mapptr & OBJECT_CONFIDENCE_MASK) >> OBJECT_CONFIDENCE_SHIFT;

			rough_data = (*mapptr & ROUGHNESS_DATA_MASK) >> ROUGHNESS_DATA_SHIFT;
			rough_conf_data = (*mapptr & ROUGHNESS_CONFIDENCE_MASK) >> ROUGHNESS_CONFIDENCE_SHIFT;

			//Then, assign goodness based on it:

			//An elev_data value of 1023 indicates no data
			if(elev_data == 1023) {
				elev_val = DEFAULT_UNKNOWN_GOODNESS;
				elev_conf_val = DEFAULT_UNKNOWN_CERTAINTY;
			} else {
				//Otherwise it's good data
				//An elev_sign value of 1 indicates the elevation is negative
				if(elev_sign) elev_data*=-1;
				//elev_data values are between -1022 and 1022
				elev_val = int_scale(elev_data, -1022, 1022, MIN_GOODNESS, MAX_GOODNESS);
				//elev_conf_data values are between 0 and 7
				elev_conf_val = int_scale(elev_conf_data, 0, 7, MIN_GOODNESS, MAX_GOODNESS);
			}

			//A class_data value of 0 indicates no data
			if(class_data == 0) {
				class_val = DEFAULT_UNKNOWN_GOODNESS;
				class_conf_val = DEFAULT_UNKNOWN_CERTAINTY;
			} else {
				//Otherwise it's good data
				//Set goodness based on the terrain type stored in class_data
				switch(class_data) {
				case 1:
					//Cell classified as outlier
					class_val = 0;
					break;
				case 2:
					//Trees
					class_val = 0;
					break;
				case 3:
					//Cell classified as soil, rocks, or other terrain
					class_val = 255;
					break;
				case 4:
					//Cell classified as green, dry, or other vegetation
					class_val = 255;
					break;
				case 5:
					//Cell classified as tall grass
					class_val = 128;					
					break;
				case 6:
					//Cell classified as "cover"
					class_val = 0;
					break;
				case 7:
					//Cell classified as water
					class_val = 0;
					break;
				default:
					//Cell terrain classification data out of bounds
					class_val = DEFAULT_UNKNOWN_GOODNESS;
				}

				//class_conf_data values are between 0 and 7
				class_conf_val = int_scale(class_conf_data, 0, 7, MIN_GOODNESS, MAX_GOODNESS);
			}
		
			//An obs_data value of 3 indicates no data
			if(obs_data != 3) {
				obs_val = DEFAULT_UNKNOWN_GOODNESS;
				obs_conf_val = DEFAULT_UNKNOWN_CERTAINTY;
			} else {
				//Otherwise it's good data
				//Assign goodness based on the type of obstacle stored in obs_data
				switch(obs_data) {
				case 0:
					//Cell has no obstacle
					obs_val = 255;
					break;
				case 1:
					//Cell is a positive obstacle
					obs_val = 0;
					break;
				case 2:
					//Cell is a negative obstacle
					obs_val = 0;
					break;
				default:
					//Cell obstacle data out of bounds
					obs_val = DEFAULT_UNKNOWN_GOODNESS;
				}

				//obs_conf_data values range from 0 to 63
				obs_conf_val = int_scale(obs_conf_data, 0, 63, MIN_GOODNESS, MAX_GOODNESS);
			}

			//A rough_data value of 15 indicates no data
			if(rough_data == 15) {
				rough_val = DEFAULT_UNKNOWN_GOODNESS;
				rough_conf_val = DEFAULT_UNKNOWN_CERTAINTY;
			} else {
				//Otherwise it's good data
				//rough_data values are from 0 to 14
				rough_val = int_scale(rough_data, 0, 14, MIN_GOODNESS, MAX_GOODNESS);
				//Currently roughness confidence/certainty is not transmitted, so just use the default unknown
				rough_conf_val = DEFAULT_UNKNOWN_CERTAINTY;
				//rough_conf_val = int_scale(rough_data, 0, ??, MIN_GOODNESS, MAX_GOODNESS);
			}
			
			//Assign and set the goodness and confidence values
			goodness=(int)((elev_weight  * elev_val)  +
					       (class_weight * class_val) +
						   (obs_weight   * obs_val)   +
						   (rough_weight * rough_val));
			confidence=(int)((elev_conf_weight  * elev_conf_val)  +
					         (class_conf_weight * class_conf_val) +
						     (obs_conf_weight   * obs_conf_val)   +
						     (rough_conf_weight * rough_conf_val));
			

			*(r+i*perMap.num_cols+j)=(unsigned char)elev_val;
			*(g+i*perMap.num_cols+j)=(unsigned char)obs_val;
			*(b+i*perMap.num_cols+j)=(unsigned char)class_val;


			//Check to make sure the values are in range
			goodness = in_range_ptc(goodness, MIN_GOODNESS, MAX_GOODNESS);
			confidence = in_range_ptc(confidence, MIN_GOODNESS, MAX_GOODNESS);
			//claMap.set_cell(j, i, goodness, confidence);

			//Increment the map pointer to point to the next cell
			mapptr++;
		}

		//Cycle across the row setting the goodness of all the trailing "no data" cells
		for(j=leading_blank_cells+data_cells; j<perMap.num_cols; j++) {
			goodness = DEFAULT_UNKNOWN_GOODNESS;
			confidence = DEFAULT_UNKNOWN_CERTAINTY;
			//claMap.set_cell(j, i, goodness, confidence);
			*(r+i*perMap.num_cols+j)=(unsigned char)goodness;
			*(g+i*perMap.num_cols+j)=(unsigned char)goodness;
			*(b+i*perMap.num_cols+j)=(unsigned char)goodness;
		}
	}

	char filename[20];
	sprintf(filename, "ptc_image_%.3d.bmp", num);

	write_bmp(filename, perMap.num_rows, perMap.num_cols, r, g, b);

	free(r);
	free(g);
	free(b);
}

//Function to set make sure min_val<=val<=max_cal, and if that's not true, set val properly
int in_range_ptc(int val, int min_val, int max_val) {
	if(val<min_val) return min_val;
	if(val>max_val) return max_val;
	return val;
}

int int_scale(int val, int old_min_val, int old_max_val, int new_min_val, int new_max_val) {
	int old_range, new_range;
	float rel_pos;
	
	old_range=old_max_val-old_min_val;
	new_range=new_max_val-new_min_val;

	rel_pos=((float)(val-old_min_val))/((float)old_range);

	val=round(rel_pos*(float)new_range)+new_min_val;

	return val;
}

int round(float num) {
	if((num-(int)num) <.5) {
		return (int)num;
	}
	return (int)num+1;
}

int write_bmp(const char* filename, int rows, int cols, unsigned char* r, unsigned char* g, unsigned char* b) {
	ofstream out_file(filename,ios::binary);
	char *bufferptr, *outputptr;
	int length, i, j, row_length, result=0;

	//The length of each row must be a multiple of 4 bytes long, regardless of the number of pixels, so find that length
	row_length=3*cols;
	while((row_length)%4!=0) row_length++;

	//Allocate memory for the buffer pointer, and prepare an output pointer for writing
	outputptr=(char*)malloc(54+rows*row_length);
	bufferptr=outputptr;
	
	//For more info on the proper specifiction for bitmap files,
	//try http://www.fortunecity.com/skyscraper/windows/364/bmpffrmt.html

	//Indicate that this is a bitmap
	*bufferptr='B';
	bufferptr++;
	*bufferptr='M';
	bufferptr++;
	//Write the bitmap size
	*(unsigned int*)bufferptr=(unsigned int)(54+rows*row_length);
	bufferptr+=sizeof(unsigned int);
	//Write the reserved bits
	*(unsigned int*)bufferptr=0;
	bufferptr+=sizeof(unsigned int);
	//Write the offset from the beginning of the file to the data
	*(unsigned int*)bufferptr=54;
	bufferptr+=sizeof(unsigned int);
	//Write the size of the bitmap header
	*(unsigned int*)bufferptr=40;
	bufferptr+=sizeof(unsigned int);
	//Write the width of the image in pixels
	*(int*)bufferptr=cols;
	bufferptr+=sizeof(int);
	//Write the height of the image in pixels
	*(int*)bufferptr=rows;
	bufferptr+=sizeof(int);
	//Write the number of target planes
	*(unsigned short*)bufferptr=1;
	bufferptr+=sizeof(unsigned short);
	//Write the number of bits per pixel
	*(unsigned short*)bufferptr=24;
	bufferptr+=sizeof(unsigned short);
	//Write the type of compression
	*(unsigned int*)bufferptr=0;
	bufferptr+=sizeof(unsigned int);
	//Write the size of the image data in bytes
	*(unsigned int*)bufferptr=(unsigned int)rows*row_length;
	bufferptr+=sizeof(unsigned int);
	//Write the horizontal conversion from pixels to meters
	*(int*)bufferptr=0;
	bufferptr+=sizeof(int);
	//Write the vertical conversion from pixels to meters
	*(int*)bufferptr=0;
	bufferptr+=sizeof(int);
	//Write the number of colors used
	*(unsigned int*)bufferptr=0;
	bufferptr+=sizeof(unsigned int);
	//Write the number of important colors used
	*(unsigned int*)bufferptr=0;
	bufferptr+=sizeof(unsigned int);

	//Cycle through the color arrays (r,g,b) and write them
	//Note that in bitmaps, the bottom row on the image is written first, and the top row is written last
	for(i=rows; i>0; i--) {
		//Increment across the row
		for(j=0; j<cols; j++) {
			//Write the value in proper b,g,r order to the buffer
			*bufferptr=(unsigned char)*(b+cols*(i-1)+j);
			bufferptr++;
			*bufferptr=(unsigned char)*(g+cols*(i-1)+j);
			bufferptr++;
			*bufferptr=(unsigned char)*(r+cols*(i-1)+j);
			bufferptr++;
		}
		//If extra padding is needed to make each row be a multiple of 4 bytes long, add the padding to the buffer
		for(j=3*cols; j<row_length; j++) {
			*bufferptr=(unsigned char)0;
			bufferptr++;
		}
	}

	//Calculate the length of the buffer
	length = (int)(bufferptr-outputptr);

	//Make sure the file's available to write to
	if(out_file) {
		//Then write the bitmap and return
		out_file.write(outputptr, length);
		out_file.close();
		result=1;
	}

	//Memory cleanup
	free(outputptr);

	return result;
}
