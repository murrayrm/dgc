; DARPA Grand Challenge PC Game Port to Parallel Port Interface
; This microcontroller will poll the x and y axis timers of the
; steering wheel every 10 ms, and save the positions in buffers
; When the user requests positions of the steering wheel, the uC
; will stop what it is doing (polling timers), and output the 
; values saved in buffers right away. The users requests data by
; setting PORTA, pin 0, 1, 2.

; PORTB 0-7 are outputs to parallel port
; PORTA pins 1, 0 are output reqest
; PORTA:
; 	input pins:
; 			pin 2		==1 	=> request x-axis
;		 		3		==1 	=> request y-axis
;		 		4		==1 	=> request buttons
;		 	pin 4, 3, 2	==	000	=> request nothing
;			Only one of pin 2, 3, 4 can be high at a time
; PORTC:
; 	input pins:
; 			pin 7,6,5,4 == button A, B, C, D of the joystick
; 	output pins:
;			pin 0 == trigger x-axis timer
;			pin 1 == trigger y-axis timer

	list	p=16f873
	include sw.inc
	include	p16f873.inc
	__config _HS_OSC & _WDT_OFF & _PWRTE_ON & _LVP_OFF ; H'3F7F'  

	cblock h'20'
block
index1
index2
ccp1x		; buffer for saving values from CCP1, X-axis
ccp2y		; buffer for saving values from CCP2, Y-axis
update		; update flag
temp
	endc


	org		0
	goto	PortInit
	org		4
	goto	Interrupt

PortInit
	clrf	INTCON
	bcf		STATUS, RP0
	bcf		STATUS, RP1
	bsf 	STATUS, RP0		; move to bank1
	clrf 	TRISA
	clrf	TRISB	
	clrf	TRISC
	movlw	b'00000111'		; Buttons inputs, and output request
	movwf	TRISA
	movlw	b'11110110'		; CCP2, CCP1, and Button A, B, C, D are inputs
	movwf	TRISC
	movlw	b'00000110'		; PORTA all digital inputs
	movwf	ADCON1
	bcf		STATUS, RP0

	bsf		PORTC, TRIGGER1
	bsf		PORTC, TRIGGER2
	clrf	CCP1CON
	clrf	CCP2CON
 	movlw   b'11000000'     ;GIE=ON,PEIE=ON
    movwf   INTCON          ;Set INTCON register
	
	;movlw	b'10101010'
	;movwf	ccp1x
	;movlw	b'01010101'
	;movwf	ccp2y
	clrf	update
	clrf	ccp1x
	clrf	ccp2y

Timer1Init
	movlw	b'00000001'		; 1:1 prescale
	movwf	T1CON
	goto	CheckUpdate1

CheckUpdate1
	movf	update, w
	andwf	PORTA, w		; AND update and PORTA to see if both an update is ready and output is requested
	btfss	STATUS, Z		
	goto	UpdatePort1
	goto	StartTrigger1

UpdatePort1
	clrf	CCP1CON				; disable CCP1 capture
	clrf	CCP2CON
	movwf	temp
CheckRequestX1
	btfss	temp, UpdateXBit		; if X-bit is set, output for x-axis is requested
	goto	CheckRequestY1
	;clrf	PORTB
	movf	ccp1x, w
	movwf	PORTB
	bcf		update, UpdateXBit	; the latest x-axis data has been output, clear X bit of update
	bsf		update, UpdateYBit	; reenable Y-axis and buttons update in case the system requests
	bsf		update, UpdateButtonBit	; Y and buttons are request before it is ready. Old values
	goto 	StartTrigger1			; will be returned if new data is not ready yet.
CheckRequestY1
	btfss	temp, UpdateYBit		; if Y-bit is set, output for y-axis is requested
	goto	OutputButton1
	;clrf	PORTB
	movf	ccp2y, w
	movwf	PORTB
	bcf		update, UpdateYBit	; the latest y-axis data has been output, clear Y bit of update
	bsf		update, UpdateXBit	; enable update for X-axis and buttons in case the system requests
	bsf		update, UpdateButtonBit	; positions before new data is ready. In that case, we will
	goto 	StartTrigger1		; just return old values.
OutputButton1
	movf	PORTC, w
	;clrf	PORTB
	movwf	temp
	rrf		temp, f
	rrf		temp, f
	rrf		temp, f
	rrf		temp, f
	movf	temp, w
	andlw	ButtonMask
	movwf	PORTB
	bcf		update, UpdateButtonBit	; the latest button data has been output, clear button bit of update
	bsf		update, UpdateXBit
	bsf		update, UpdateYBit
	goto 	StartTrigger1 	

StartTrigger1			; start sending a pulse
	clrf 	CCPR1H
  	clrf	CCPR1L
    movlw   b'00000100'     ;CCP1M=0100(Capture), falling edge
	movwf   CCP1CON       	;Set CCP1CON register
	bsf     STATUS, RP0	;Change to Bank1
	bsf     PIE1, CCP1IE    ;CCP1 interruptin enable
	bcf     STATUS, RP0     ;Change to Bank0
	bcf     PIR1, CCP1IF    ;Clear CCP1 int flag
	clrf    TMR1H           ;Clear TMR1H register
	clrf    TMR1L           ;Clear TMR1L register

	bcf		PORTC, TRIGGER1
	movlw	0xFF
	movwf	block
	nop
	nop
	bsf		PORTC, TRIGGER1


WaitForTimeout1				; first check if there is timeout, whiling waiting for timeout, check to see
	nop					; if output is requested
	nop
	nop
	btfss	TMR1H, d'1'		; AND result is 0. No output is needed
	goto	WaitForCapture1	; if there is no timeout, check if interrupt has been called.
	goto	TimeOut1

WaitForCapture1
	btfsc	block , 0
	goto	WaitForTimeout1
	goto	StartTrigger2

TimeOut1
    clrf    CCP1CON     ;CCP1 off	
	goto	CheckUpdate2
	
CheckUpdate2
	movf	update, w
	andwf	PORTA, w		; AND update and PORTA to see if both an update is ready and output is requested
	btfss	STATUS, Z		
	goto	UpdatePort2
UpdatePort2
	clrf	CCP1CON
	clrf	CCP2CON				; disable CCP2 capture
	movwf	temp
CheckRequestX2
	btfss	temp, UpdateXBit		; if X-bit is set, output for x-axis is requested
	goto	CheckRequestY2
	;clrf	PORTB
	movf	ccp1x, w
	movwf	PORTB
	bcf		update, UpdateXBit	; the latest x-axis data has been output, clear X bit of update
	bsf		update, UpdateYBit
	bsf		update, UpdateButtonBit
	goto 	StartTrigger2
CheckRequestY2
	btfss	temp, UpdateYBit		; if Y-bit is set, output for y-axis is requested
	goto	OutputButton2
	;clrf	PORTB
	movf	ccp2y, w
	movwf	PORTB
	bcf		update, UpdateYBit	; the latest y-axis data has been output, clear Y bit of update
	bsf		update, UpdateXBit	; enable update for X-axis and buttons in case the system requests
	bsf		update, UpdateButtonBit	; positions before new data is ready. In that case, we will
	goto 	StartTrigger2			; just output old values
OutputButton2
	movf	PORTC, w
	movwf	temp
	;clrf	PORTB
	rrf		temp, f
	rrf		temp, f
	rrf		temp, f
	rrf		temp, f
	movf	temp, w
	andlw	ButtonMask
	movwf	PORTB
	bcf		update, UpdateButtonBit	; the latest button data has been output, clear button bit of update
	bsf		update, UpdateXBit
	bsf		update, UpdateYBit
	goto 	StartTrigger2	

StartTrigger2			; start sending a pulse to y-axis
	clrf 	CCPR2H
  	clrf	CCPR2L
    movlw   b'00000100'     ;CCP2M=0100(Capture), falling edge
	movwf   CCP2CON       	;Set CCP2CON register
	bsf     STATUS, RP0		;Change to Bank1
	bsf     PIE2, CCP2IE    ;CCP2 interruptin enable
	bcf     STATUS, RP0     ;Change to Bank0
	bcf     PIR2, CCP2IF    ;Clear CCP2 int flag
	clrf    TMR1H           ;Clear TMR1H register
	clrf    TMR1L           ;Clear TMR1L register

	bcf		PORTC, TRIGGER2
	movlw	0xFF
	movwf	block
	nop
	nop
	bsf		PORTC, TRIGGER2



WaitForTimeout2				; first check if there is timeout
	nop
	nop
	nop
	btfss	TMR1H, d'1'		; AND result is 0. No output is needed
	goto	WaitForCapture2	; if there is no timeout, check if interrupt has been called.
	goto	TimeOut2

WaitForCapture2
	btfsc	block , 0
	goto	WaitForTimeout2
	goto	CheckUpdate1

TimeOut2
    clrf    CCP2CON     ;CCP2 off	
	bsf		update, UpdateButtonBit	; Pretend button data is reset every cycle(~10ms)
	goto	CheckUpdate1


Interrupt
	movlw	0
	movwf	block
	movf	PIR1, w
	btfsc 	PIR1, CCP1IF
	goto	Capture1
	btfsc	PIR2, CCP2IF
	goto	Capture2
	goto 	InterruptEnd

Capture1
	bcf     PIR1, CCP1IF     ;Clear CCP1 int flag
	movf	CCPR1H, w
	btfss	STATUS, Z
	goto	Overflow1
	movf	CCPR1L, w	
	movwf	ccp1x
	clrf	CCP1CON
	bsf		update, UpdateXBit	; X-axis is updated
	goto 	InterruptEnd

Overflow1
	clrf	ccp1x
	bsf		update, UpdateXBit	; X-axis is updated
	goto 	InterruptEnd

Capture2
	bcf     PIR2, CCP2IF     ;Clear CCP1 int flag
	movf	CCPR2H, w
	btfss	STATUS, Z
	goto	Overflow2
	movf	CCPR2L, w	
	movwf	ccp2y
	clrf	CCP2CON
	bsf		update, UpdateYBit	; Y-axis is updated
	goto 	InterruptEnd

Overflow2
	clrf	ccp2y
	bsf		update, UpdateYBit	; Y-axis is updated
	goto 	InterruptEnd

InterruptEnd		
	retfie 



	end
