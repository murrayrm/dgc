function [phi, g, v] = dfe(state, terrain)
% function [phi, g, v] = dfe()
% LG031118
%
% Das Dynamik Feasibility Evaluator
%
% The Dynamic Feasibility Evaluator estimates the 'goodness' or 'safeness' 
% of each of a set of possible trajectories (arcs) by computing how close
% the vehicle comes to rolling over if that arc is executed at the current 
% speed.
%
% In the future, other potential criteria for goodness may be:
%   - how close the vehicle comes to sliding/skidding
%   - how close the vehicle comes to slipping/spinning the wheels
%
% Questions:
%   What are definitions of variables
%   Which way is positive for phi, roll, pitch, yaw
%   How do I go from roll, pitch, yaw to reference frame of vehicle 
%
% Inputs :
%   - state : the state vector of the vehicle
%       instantaneous variables:
%           .v      : speed          [m/s]
%           .phi    : steering angle    [rad/s], 0 is forward, +ve is to the left
%
%           .roll   : 
%           .pitch  : 
%           .yaw    : heading as seen from above (projected onto global floor plane)    [rad]
%   
%   NOTE: terrain quantifiers currently not used
%   - terrain : 
%       .terrain_type   :   specification of type of terrain, which could have implications
%                           on the safe speed to avoid slippage/skidding
%       .terrain_roughness : 
%       .terrain_bumpiness : 
%
% Outputs:
%   phi     :   Nx1 vector of possible steering angles considered
%   g       :   Nx1 vector of 'goodness' of each arc
%   v       :   Nx1 vector of maximum speed permissible for each arc
%   
% Question: 
%   How different is the physics of driving on sand (especially a sand dune)
%   to that of driving on hard pavement ?
%       - there is certainly a lot more slippage and sliding
%       - there is the hazard of becoming stuck
%   How well can we estimate velocity when on sand (and wheels slip)?
%
% Algorithm:
%   
%   Basic idea: compute total force on center of gravity of vehicle, and determine if
%               that causes a torque downwards (force inside the vehicle wheelbase),
%               or upwards (force outside of the wheelbase).
%
%   Assumptions:
%       - ignore effect of suspension in shifting COG of vehicle
%       - calculating only the instantaneous effect of a particular arc,
%           as if we switch steering angles instantaneously, ignoring the
%           steering dynamics.
%       - also ignoring effect of throttle acceleration or braking
%           NOTE: if we do want to include this, do we have to distinguish
%               between accel/decel due to engine torque/brakes and due
%               to gravity? One is a force on the center of gravity, the
%               others are torques around the rear axle.
%       - assuming current state vector yaw, pitch, roll angles are good
%           description of orientation of surface
%           NOTE: may need to have a separate filter to estimate surface
%               orientation since IMU rate is 100Hz -> high freq content.
%       - assuming steering angle does not affect contact point of wheels
%           (this seems reasonable)

% Constants

WHEELBASE_SS = 1.5; % [m], side-to-side wheel base
WHEELBASE_FB = 2.2; % [m], front-to-backk wheel base

MASS = 1000; % [kg], total vehicle mass

COG = [0; 0; 1.0]; % [m], vehicle center of gravity, expressed in a coordinate ref frame
                   % at center of 4 wheel contact points. x is forward, y to left, z up.

GRAVITY_ACCELERATION = 9.8; % [m/s^2]                   

% compute ref frame of vehicle wrt global 'flat earth' ref frame
R_RW = angles2rotation_matrix( state ); % R for robot (or vehicle), and W for world
                                        % _RW means transform from Robot to World ref frame

% we want to work in the vehicle ref frame, so compute gravity acceleration vector in 
% the vehicle ref frame
a_g_W = GRAVITY_ACCELERATION * [0; 0; -1];

a_g_R = R_RW' * a_g_W;

% compute centripetal accelereation
    % first compute turning radius
    % NOTE: many other modules will need this to compute the so called arcs
    r = steering_angle2turning_radius( steering_angle );
    % returns positive if center point is on postive y axis of vehicle,
    % negative otherwise. zero if we're going straight ahead!
    
    a_c_R = state.v^2 / r * [0; 1; 0];
