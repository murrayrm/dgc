
#include "yaw_pitch_roll_2_matrix.h"
#include <math.h>

void yaw_pitch_roll_2_matrix(const double yaw, const double pitch, const double roll, double *x, double *y, double *z)
{
    /* (from the original matlab function)

        function [R_ab, R_ab_true, R_pr, R_pr_true] = yaw_pitch_roll_2_matrix(yaw, pitch, roll)
        % function yaw_pitch_roll_2_matrix(yaw, pitch, roll)
        % LG031127
        %
        % Richard Murray's "A Mathematical Introducation to Robotic Manipulation", p32.
        %
        % ZYX Euler angles commonly referred to as yaw, pitch, roll angles:
        %
        % R_ab = R_z(yaw) R_y(pitch) R_x(roll)
        %
        % where R_ab transforms from b ref frame to a ref frame,
        % where b is the body reference frame
        % and a is the inertial (fixed) reference frame

            R_pr_true = rodrigues([0 pitch 0]) * rodrigues([roll 0 0]);
            R_ab_true = rodrigues([0 0 yaw]) * R_pr_true;

            sy = sin(yaw); cy = cos(yaw);
            sp = sin(pitch); cp = cos(pitch);
            sr = sin(roll); cr = cos(roll);

            R_pr = [ cp     sp*sr       sp*cr;
                     0       cr          -sr;
                    -sp     cp*sr       cp*cr];

            R_ab = [ cy*cp      cy*sp*sr-sy*cr      cy*sp*cr+sy*sr;
                     sy*cp      sy*sp*sr+cy*cr      sy*sp*cr-cy*sr;
                      -sp           cp*sr               cp*cr       ];
    */

    /*  Inputs:
            yaw, pitch, roll    :   the ZYX Euler angles, units : radians

        Outputs:
            x, y, z     : the coordinates of the vehicles x, y, z axii in the 
                            inertial reference frame
                        x, y, z assumed to be double x[3], y[3], z[3] for now,
                        since we don't have a vector/matrix class
    */

    double sy = sin(yaw);
    double cy = cos(yaw);

    double sp = sin(pitch); 
    double cp = cos(pitch);

    double sr = sin(roll); 
    double cr = cos(roll);

    x[0] =     cy*cp;
    x[1] =     sy*cp;        
    x[2] =      -sp;  

    y[0] =   cy*sp*sr-sy*cr;
    y[1] =   sy*sp*sr+cy*cr;
    y[2] =        cp*sr;

    z[0] =   cy*sp*cr+sy*sr;
    z[1] =   sy*sp*cr-cy*sr;
    z[2] =       cp*cr;

}
    

