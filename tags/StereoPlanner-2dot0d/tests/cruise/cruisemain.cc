#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>


#include "cruisemain.hh"

using namespace std;


// declare a DATUM
DATUM d;

// BS for steering to work
int negCountLimit        =    -60000;
int posCountLimit        =     60000;
int verbosity_exec_cmd   =      TRUE;
int steer_errno          =  ERR_NONE;
string lastSteeringCommanded;

// used for starting threads
boost::recursive_mutex StarterMutex;
int StarterCount;
void Starter();


// the "kill switch"
void shutdown(int signal) {
  cout << "Signal Received" << endl;
  d.shutdown_flag = TRUE;
  // boost::recursive_mutex::scoped_lock scoped_lock(WF.brakeMutex);
  //set_brake_abs_pos_pot(0.8, brakeVelo, brakeVelo);
  cout << "Set Abort Flag" << endl;
  //sleep_for(10);
  //exit(EXIT_SUCCESS);
}


int main() {
  cout << "Starting Main" << endl;

  // setup signals for shutdown
  d.shutdown_flag = FALSE;
  signal(SIGINT, &shutdown);
  signal(SIGTERM, &shutdown);

  // For Data Logging
  string in;
  strstream strStream;
  cout << "What test number? ";
  getline(cin, in);
  d.TestNumber = atoi(in.c_str());
  if( d.TestNumber <= 0) {
    cout << "Invalid Test Number" << endl;
    exit(EXIT_SUCCESS);
  }

  strStream << d.TestNumber;
  strStream >> d.TestNumberStr;

  // set desired velocity to ...
  // This is the default value, but will be overwritten in accel.cc
  d.targetVelocity = 1.5; //  m/s, 

  // initialize all devices before spawning threads
  cout << "Initializing GPS... ";
  InitGPS(d);
  cout << "done." << endl;

//  cout << "Initializing OBD II... ";
//  InitOBD(d);
//  cout << "done." << endl;

  cout << "Initializing StateFilter... ";
  InitStateFilter(d);

  cout << "Initializing Accel... ";
  InitAccel(d);    
  init_steer();    //change JL                     // Needed for waypoint following
  initWaypts(d.WF);                         // Needed for waypoint following

  cout << "Finished Initializing" << endl;

  gettimeofday(&d.TimeZero, NULL);
  // spawn threads
  boost::thread_group thrds;

  StarterCount = 0;
  thrds.create_thread(Starter); 
  thrds.create_thread(Starter); 
  thrds.create_thread(Starter);
  thrds.create_thread(Starter);


  thrds.join_all();
  cout << "Shutting Down" << endl;
  

  return 0;
}


void Starter() {
  // determine which thread we are 
  int myThread;
  if( TRUE ) {
    boost::recursive_mutex::scoped_lock sl(StarterMutex);
    myThread = StarterCount;
    StarterCount ++;
  }

  switch( myThread ) {
  case 0:
    cout << "Starting GrabGPS Loop" << endl;
    GrabGPSLoop(d);
    break;
  case 1:
    cout << "Starting StateFilterLoop" << endl;
    StateFilterLoop(d);
    break;
  case 2:
    cout << "Starting AccelLoop" << endl;
    AccelLoop(d);
    break;
  case 3:
  //  cout << "Starting Grab OBD loop" <<endl;
  //  GrabOBDLoop(d);
    break;
  default:
    cout << "Starter error, default reached" << endl;
  }
}
