#include "waypointnav.hh"
#include "Ladar/driver_klk/laserdevice.h"
#include "Ladar/Ladar.h"
#include "DynamicFeasibilityEvaluator/rollover_functions.h"
#include "DynamicFeasibilityEvaluator/vehicle_constants.h"

// ADDED BY MIKE THIELMAN
extern double waydisp_ladar_p[];
extern double waydisp_ladar_v[];
extern int waydisp_ladar_g[];

extern int SIM; // get the simulation flag from the main function
extern int PRINT_VOTES; // get the print_votes flag from the main function

// THIS IS NASTY BUT IT'LL WORK FOR NOW...
// TODO: fix this nastiness
CLaserDevice laser("/dev/ttyS9"); // NEW WAY
//CLaserDevice *laser;

/*****************************************************************************/
/*****************************************************************************/
int InitLadar(DATUM &d) {

  int result;
  char * port_name;
  d.voterArray[d.LadarNum] = Voter(NUMARCS, LEFTPHI, RIGHTPHI);
  d.LadarWeight = LADAR_W;
  d.demoArb.addVoter(&(d.voterArray[d.LadarNum]), d.LadarWeight);
  cout << "Added Ladar Voter" << endl;
  // Ladar initialization tasks
  if( 1 ) { // use this if you want to use the LADAR unit in simulation
  //if( !SIM ) { // use this if you truly want to simulate

    sprintf(port_name,"/dev/ttyS%d",LADAR_SERIAL_PORT);
    // THIS WAY DIDN'T WORK FOR ME (LARS) 12/20/03
    // BUT I DON'T THINK THIS WAS THE PROBLEM
    //CLaserDevice laser(port_name);  // NEW WAY
    //laser = new CLaserDevice(port_name); // change this to appropriate serial port
    result = laser.Setup();
    //result = laser->Setup();
    
    if ( result == 0 ) {
      return TRUE;
    } 
    else {
      return FALSE;
    }
  } 
  else {
    // Do simulation stuff here...
    return TRUE;
  }  
  
} // end int InitLadar(DATUM &d)

/*****************************************************************************/
/*****************************************************************************/
void LadarLoop(DATUM &d) {

  double laserdata[LADAR_NUM_SCANPOINTS];
  double growndata[LADAR_NUM_SCANPOINTS];

  while(d.shutdown_flag == FALSE) {

    //printf("--in ladar.cc-- \n");

    if( 1 ) { // use the LADAR unit even in simulation
    //if( !SIM ) { // use this if you truly want to simulate
      // Take a scan
      laser.UpdateScan(); // NEW WAY
      //laser->UpdateScan();

      // update local data buffer
      for(int i = 0; i < LADAR_NUM_SCANPOINTS; i++) {
	laserdata[i] = (laser.Scan[i]) / 1000.0; //note: laser returns data in mm.
	growndata[i] = laserdata[i];
	//laserdata[i] = (laser->Scan[i]) / 1000.0; //note: laser returns data in mm.
      }
    } 
    else {
      // fake the laser scan here...
      for (int i = 0; i < LADAR_NUM_SCANPOINTS; i++) {
	laserdata[i] = LADAR_MAX_RANGE; 
	growndata[i] = laserdata[i];
      }

      //int snum = 261;
      int snum = 180;
      laserdata[snum] = 0.95 * LADAR_MAX_RANGE;
      growndata[snum] = laserdata[snum];
      
      /*
      for (int i = 60; i < 100; i++) {
        laserdata[i] = LADAR_MAX_RANGE * .8;
	growndata[i] = laserdata[i];
      }
      */
    } // end else 

    // If we want to grow the obstacles
    if(1) {

      // we don't want to assume that the car is a point, so here is our way
      // to grow obstacles. 
      double d_grow = 1.0;
      double alpha_grow; // the angle (radians) in each direction that we grow the obstacle
      // this will depend on the distance of the obstacle 
      int alpha_grow_num;  // number of scanpoints that correspond to a given angle
      int index_a, index_b;
    
      double bleed_fraction = 0.2; // fraction of alpha_grow_num that we want to further grow 
      // obstacles, but fade them out, so that it's better to be farther from an obstacle
      int index_fl, index_fr; // far left and far right scanpoint indices for growing obstacles
      double fade_rate = 0.1; // rate (in meters per scanline) to fade the obstacle
    
      // here is the code that (effectively) grows the obstacles
      // iterate through each scanpoint and modify the neighboring scanpoints 
      //for(int i = 0; i < LADAR_NUM_SCANPOINTS; i++) {
      // HACK: Had problem that farthest right scanpoint was reading a small value and 
      // growing this obstacle all the way past straight (laserdata[0]=0.90)
      // hack is to iterate from scanpoint 1 instead...
      for(int i = 1; i < LADAR_NUM_SCANPOINTS; i++) {

        // scan distance is in laserdata[i]
        if( laserdata[i] > 0 ) {

          // alpha_grow is the angle on either side of the obstacle that
          // we want the obstacle to grow to
          alpha_grow =  d_grow / laserdata[i]; // radians
          // number of scanpoints on each side to potentially modify
	  alpha_grow_num = (int)((alpha_grow / LADAR_SWEEP_ANGLE) * LADAR_NUM_SCANPOINTS );
        }     

        // set the indices of the farthest neighbors that scan "i" will affect 
        index_a = max( 0, i - alpha_grow_num ); 
        index_b = min( LADAR_NUM_SCANPOINTS, i + alpha_grow_num ); 

        // far right and far left indices, to which we further grow the obstacle, 
        // but fade it away as we get farther away
        index_fr = max( 0, (int)(i - (int)alpha_grow_num*(1+bleed_fraction)) );
        index_fl = min( LADAR_NUM_SCANPOINTS, (int)(i + (int)alpha_grow_num*(1+bleed_fraction)) ); 

        //DEBUGGING
        /*
        if( i == 180 ) {
          printf("\n\nalpha_grow = %f, alpha_grow_num = %d\n", alpha_grow, alpha_grow_num );
          printf("LADAR_NUM_SCANPOINTS = %d,  LADAR_SWEEP_ANGLE_2 = %f\n", 
		      LADAR_NUM_SCANPOINTS, LADAR_SWEEP_ANGLE_2);
          printf("i = %d, index_fr = %d, index_a = %d, index_b = %d, index_fl = %d\n\n", 
		     i, index_fr, index_a, index_b, index_fl );
        }
        */
        for( int j = index_a; j < index_b; j++ ) {
	  // original growth algorithm
	  growndata[j] = fmin( laserdata[i], growndata[j] ); 
	  // new algorithm... let obstacles fade away as they're grown
          // this did not work very well!
	  //growndata[j] = fmin( laserdata[i]+fade_rate*abs(i-j), growndata[j] ); 
        }

        // make the obstacles fade away linearly
        // for the scanlines at far right
        for( int j = index_fr; j < index_a; j++ ) {
  	  growndata[j] = fmin( laserdata[i]+fade_rate*abs(j-index_a), growndata[j] );
        }
        // for the scanlines at far left
        for( int j = index_b; j < index_fl; j++ ) {
  	  growndata[j] = fmin( laserdata[i]+fade_rate*abs(j-index_b), growndata[j] );
	}

        //printf("alpha_grow[%d] = %4.2f   and   alpha_grow_num[%d] = %d\n", 
        //       i, alpha_grow, i, alpha_grow_num );

      } // end iterating over scanpoints

    } // End if we want to grow the obstacles

    // DEBUGGING
    if(  0  ) { 
      for(int i = 0; i < LADAR_NUM_SCANPOINTS; i++) {
        printf("laserdata[%d] = %4.2f   and   growndata[%d] = %4.2f",
			i, laserdata[i], i, growndata[i] );
        printf("  for angle from straight = %f\n", 
          (i*LADAR_ANGLE_RESOLUTION-M_PI/2)*180.0/M_PI );
      }
      sleep(200); 
      //getch(); 
      printf("\n\n");
    }

    //figure out the votes...
    double timestamp = 0;  //just a dummy timestamp
    Voter::voteListType voteList = d.voterArray[d.LadarNum].getVotes();

    // Now voteList should contain all the right arcs to be evaluated.
    // So evaluate them.
    int i = 0;
    for (Voter::voteListType::iterator iter = voteList.begin();
		    iter != voteList.end(); ++iter, ++i) {

      // DEBUGGING
      if( 0 ) {
        if(iter->phi > 0 ) {
          printf("\n\nEVALUATING ARC PHI (DEGREES) = %f\n\n", iter->phi*180/M_PI );
	  usleep(200000);
        }
      }
      //printf( "p(deg) = %5.2f, v(m/s) = %5.2f, ", iter->phi*180.0/M_PI, iter->velo );
      
      if( 1 ) {
        // point vehicle calculation
        iter->goodness = arc_goodness_based_on_point_vehicle( iter->phi, growndata );
      }
      else {
        // vehicle path calculation (not yet implemented)
        iter->goodness = arc_goodness_based_on_vehicle_swath( iter->phi, growndata );
      }	

      if( iter->goodness == MAX_GOODNESS && fabs(iter->phi) > 0.0000001 ) {
        iter->goodness = MAX_GOODNESS - 1;
      } 

      //TODO: actually set this to something useful
      // DEFAULT_VELOCITY defined in team/include/Constants.h
      if( iter->goodness < 100 ) {
        iter->velo = 0.0; // in meters/sec
      }
      else {
        iter->velo = DEFAULT_VELOCITY; // in meters/sec
      }
        
      // ADDED BY MIKE THIELMAN for Sparrow display
      waydisp_ladar_p[i] = (iter->phi * 180.0/M_PI);
      waydisp_ladar_v[i] = (iter->velo);
      waydisp_ladar_g[i] = iter->goodness;

    } // end iterating over votes/arcs

    // TODO: I HAVE NO IDEA IF ANYTHING NEEDS TO BE LOCKED HERE...
    d.voterArray[d.LadarNum].updateVotes(voteList, timestamp);
    //if( 1 ) { // cheat and print votes anyway
    if( 0 ) { // cheat and don't print votes 
    //if( PRINT_VOTES ) {
 
     // print the arbiter votes
     //system("clear");
     cout << "--LadarLoop-- LADAR votes" << endl;
     d.voterArray[d.LadarNum].printVotes();
     // cout << "--ladar.cc-- Sleeping so you can read my output." << endl;
     // sleep(2);
    }
    //usleep_for(10000);    // Evaluation rate
    usleep(10000);    // Evaluation rate

  } // end while(d.shutdown_flag == FALSE)

  // copying from main.cc in Ladar (Lars)
  laser.LockNShutdown();  

} // end LadarLoop(DATUM &d)


/*****************************************************************************/
/*****************************************************************************/
int arc_goodness_based_on_vehicle_swath( double phi, double rangedata[LADAR_NUM_SCANPOINTS] ) {

  // in this method, we consider the swath that the vehicle travels
  // rather than a point vehicle and grown obstacles

  // set the default to be the maximum goodness
  int goodness = MAX_GOODNESS;

  /*    // CONCEPTUALLY
  //for( each arc to consider ) {
  set the goodness to maximum
  do( go through each scanpoint to see if it's in the vehicle path ) {
  if( it is ) {
  then calculate travel distance along path to scanpoint;
  if the travel distance is smaller than previously calculated {
  set the goodness according to this smaller travel distance;
  }
  }
  }
  //} // end 
  */

  return goodness;

} // end int arc_goodness_based_on_vehicle_swath(...) 


/*****************************************************************************/
/*****************************************************************************/
int arc_goodness_based_on_point_vehicle( double phi, double * rangedata ) {

// DEBUGGING ONLY
  int ix = 180;
  
  double arc_radius = steering_angle_2_arc_radius( phi );
  // arc_radius is positive for right turns, neg for left turns,
  // and ZERO straight ahead

  // set the default to be the maximum goodness
  int goodness = MAX_GOODNESS;

  // the range of scanline angle is [0,pi] (radians)
  // and is measured zero directly to the right
  // THIS WILL NOT WORK IF YOUR ANGULAR FOV IS NOT PI
  double scanline_angle;
  double arc_scan_intersection = 0;
  double arc_drive_dist = 10000; // set to some large value

  // TODO: note - some of the numbers below depend on what mode
  // the ladar is in, ie the number of scanlines depends on if you
  // are in .5deg mode, 1deg mode, or .25 deg mode.  these should
  // probably be #defined somewhere, but for now they'll just he
  // hardcoded in here.

  // set goodnes to max, and we will reduce it appropriately if
  // we can't drive the whole arc
  goodness = MAX_GOODNESS;
  
  // straight ahead, special case
  if ( arc_radius == 0 ) {

    if ( rangedata[180] < LADAR_MAX_RANGE ) {

      // set the goodness proportional to the scan distance straight ahead
      goodness = (int)round(fmin( (rangedata[180] / LADAR_MAX_RANGE), 1 ) * MAX_GOODNESS);
    }

  }
  else if ( arc_radius > 0 ) {
    // right turn, so only use scan lines on the right side scan
    // line # 181 is the "middle" -> index *180* since arrays start
    // at zero

    // start at the middle and work outwards so as soon as we find
    // a point that is too close, we can break
    for ( int i = 179; i >= 0; i-- ) {

      // find the distance at which the scan line intersects the
      // steering arc
      scanline_angle = i * LADAR_ANGLE_RESOLUTION;
      // from the law of cosines, c^2 = a^2 + b^2 - 2*a*b*cos(C)
      arc_scan_intersection = sqrt(2.0)*arc_radius*sqrt(1-cos(M_PI-2*scanline_angle));

     // there's a scan return between us and where the arc takes us
      // modify the goodness appropriately...
      if ( arc_scan_intersection >= rangedata[i] ) {

	// make the goodness proportional to the direct range to the obstacle along the arc
	// one choice of goodness assignment.  others may be better.
	goodness = (int)round(fmin( (rangedata[i] / LADAR_MAX_RANGE), 1 ) * MAX_GOODNESS);

	// DEBUGGING
	ix = i;

	break;
      }

    } //end for ( int i = 179; i >= 0; i-- )

  } // end else if ( arc_radius > 0 )
  else {
    // left turn, so only use scan lines on the left side
    // note, arc_radius will be negative here.

    for ( int i = 181; i < LADAR_NUM_SCANPOINTS; i++ ) {
      // same as above basically, start at middle and work
      // outwards with the scanlines.

      // on the left hand side the sla is calculated slightly differently
      scanline_angle = M_PI - i * LADAR_ANGLE_RESOLUTION;
      // from the law of cosines, c^2 = a^2 + b^2 - 2*a*b*cos(C)
      arc_scan_intersection = sqrt(2.0)*fabs(arc_radius)*sqrt(1-cos(M_PI-2*scanline_angle));

//printf("i=%d, sla = %6.3f, arc_scan_ix= %6.3f, arc_rad= %6.3f, phi=%6.3f \n", 
//		i, scanline_angle, arc_scan_intersection, arc_radius );
      
      // there's something between us and where the arc takes us
      // modify the goodness appropriately...
      if ( arc_scan_intersection >= rangedata[i] ) {

	// make the goodness proportional to the direct range to the obstacle along the arc
	// one choice of goodness assignment.  others may be better.
	goodness = (int)round(fmin( (rangedata[i] / LADAR_MAX_RANGE), 1 ) * MAX_GOODNESS);

	// DEBUGGUNG
	ix = i;

	break;
      }

    } //end looping thru left half scan lines

  } //end the if arc_radius == or <= or >= checks

//DEBUGGING
//  printf("sla(deg) = %6.2f, a_s_i= %5.2f, range[%d] = %5.2f, ", 
//		  (scanline_angle-M_PI/2)*180.0/M_PI,arc_scan_intersection, ix, rangedata[ix]);
  
  return goodness;
  
} // end int arc_goodness_based_on_point_vehicle(...)


// end ladar.cc
