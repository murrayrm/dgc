//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "decs.h"

//#define RUN_VP
//#define STRIP_VP
#define OPENGL_VOTE_VP

//#define RUN_SEGMENTER
//#define DEBUG

#define SOURCE_DIRECTORY
//#define SOURCE_AVI

#define CONNECT_TO_TITANIUM

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void init_per_run(int argc, char **argv);
void init_per_image();
void process_image();

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

extern float *map_data;
extern int map_rows, map_cols;

int left_down = FALSE;

int image_shrink_factor;
int image_number;
double image_time, time_since_start;
double grab_time, blur_time, gabor_time, vote_time, segment_time, display_time;

//char avi_filename[128] = "C:\\Documents\\tex\\planx\\images\\input\\dvData_23_clip1.avi";
//char avi_filename[128] = "C:\\Documents\\software\\grandchallenge\\images\\dvData_23_clip1.avi";
char avi_filename[128] = "..\\..\\images\\dvData_23_clip1.avi";
//char avi_filename[128] = "..\\..\\images\\dvData_25_clip1.avi";
//char avi_filename[128] = "..\\..\\images\\dvData_25_clip2.avi";

int num_still_image_iterations, cur_still_image_iteration;
char in_filedir[128] = "..\\..\\images\\collected\\jan29_ppm\\";
char ls_filedir[128];
char in_filename[128];
char image_filename[128];
FILE *filelist_fp;

CR_Movie *gc_movie;
CR_Image *gc_im;
CR_Image *raw_im;

// double *image_fft_in;   // natural image format is float
// fftw_complex *image_fft_out;
// fftw_plan image_fft_plan;

extern CR_Image *planx_gray_fp_im, *planx_gray_fft_fp_im;

extern float *vp_votes;

int planx_w, planx_h;
float fplanx_w, fplanx_h;

extern int planx_voter_current_top;
extern int planx_voter_strip_height;
extern int planx_voter_top, planx_voter_bottom;
extern int do_planx_strip;
extern int do_opengl_voting;

extern float *hist_seg_class;
extern float *seg_voter_disc;

extern float *seg_voter_disc_edge;

// integer in range = 0 to 24, inclusive
int vehicle_steering_vote;

// double in range 0 to 100, inclusive
double vehicle_steering_goodness;

// double in range ??? (neg?) to MAX_VEL, which is about 5.0 m/s right now
double vehicle_recommended_velocity;

double vehicle_speed;

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

int main(int argc, char **argv)
{
  init_per_run(argc, argv);

  glutMainLoop();

  return 0;

  //  while (1) 
  //    process_image();

}

//----------------------------------------------------------------------------

void mouse(int button, int state, int x, int y)
{
  if (button == GLUT_LEFT_BUTTON) {
    if (state == GLUT_DOWN) 
      left_down = TRUE;
    else if (state == GLUT_UP) 
      left_down = FALSE;
  }
}

//----------------------------------------------------------------------------

void display()
{
  int result;

  // raw image
  //  draw_hist_segmenter(gc_im);

  if (!left_down)
    glDrawPixels(gc_im->w, gc_im->h, GL_RGB, GL_UNSIGNED_BYTE, gc_im->data);

  // vanishing point stuff

#ifdef RUN_VP
  /*
  glEnable(GL_BLEND);

  glPixelTransferf(GL_BLUE_SCALE, 2.0);
  glDrawPixels(gc_im->w, gc_im->h, GL_BLUE, GL_FLOAT, vp_votes);
  glPixelTransferf(GL_BLUE_SCALE, 1.0);

  glDisable(GL_BLEND);
  */

  //  draw_vp_votes(gc_im);

  draw_vp_particlefilter(gc_im);
#endif

  // segmentation stuff

#ifdef RUN_SEGMENTER
  //  draw_hist_segmenter(NULL);
  //  glBlendFunc(GL_ONE, GL_ONE);
  glBlendFunc(GL_ONE, GL_ZERO);
  //  glBlendFunc(GL_ONE, GL_ONE);

  glEnable(GL_BLEND);
  //  glDrawPixels(gc_im->w, gc_im->h, GL_BLUE, GL_FLOAT, seg_voter_disc);
  
  //  glDrawPixels(gc_im->w, gc_im->h, GL_BLUE, GL_FLOAT, hist_seg_class);
  //  glDrawPixels(gc_im->w, gc_im->h, GL_BLUE, GL_FLOAT, hist_seg_class);

  //  glDrawPixels(gc_im->w, gc_im->h, GL_BLUE, GL_FLOAT, seg_voter_disc_edge);

  glDisable(GL_BLEND);

  draw_fish_segmenter();

  //  glBlendFunc(GL_ONE, GL_ONE);

  //glDrawPixels(gc_im->w, gc_im->h, GL_ALPHA, GL_FLOAT, hist_seg_class);
  // draw_seg_particlefilter();
  
#endif

#ifdef CONNECT_TO_TITANIUM

  glBlendFunc(GL_ONE, GL_ZERO);

  glEnable(GL_BLEND);
  glDrawPixels(map_cols, map_rows, GL_RED, GL_FLOAT, map_data);
  glDisable(GL_BLEND);
#endif

  // additional guides

  // draw "midline" of image...which side VP is one indicates recommended steering direction

  glColor3f(1.0, 0.0, 0.0);
  glLineWidth(1);

  glBegin(GL_LINES);

  glVertex2i(gc_im->w / 2, 0);
  glVertex2i(gc_im->w / 2, gc_im->h);

#ifdef STRIP_VP
  glColor3f(0.0, 1.0, 0.0);
  glVertex2i(0, planx_h - planx_voter_current_top);
  glVertex2i(gc_im->w, planx_h - planx_voter_current_top);

  glVertex2i(0, planx_h - (planx_voter_current_top + planx_voter_strip_height));
  glVertex2i(gc_im->w, planx_h - (planx_voter_current_top + planx_voter_strip_height));
  glColor3f(1.0, 0.0, 0.0);
  
#endif

  glEnd();

  glutSwapBuffers();

#ifdef SOURCE_DIRECTORY
#ifdef RUN_SEGMENTER
  if (cur_still_image_iteration >= num_still_image_iterations) {
    CR_sleep(3.0);
//     cur_still_image_iteration = 0;
  }
#endif
#endif
}

//----------------------------------------------------------------------------

void init_per_run(int argc, char **argv)
{
  //-------------------------------------------------
  // connect to titanium on vehicle 
  //-------------------------------------------------

  vehicle_steering_vote = -1;  // this tells the send thread that there is no vote yet
  vehicle_recommended_velocity = 10.0;  // this should be bigger than max, meaning that we have no opinion

#ifdef CONNECT_TO_TITANIUM
  make_connection();
#endif

  //-------------------------------------------------
  // disable some annoying memory warning crap
  //-------------------------------------------------

  int tmpFlag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
  tmpFlag &= ~_CRTDBG_ALLOC_MEM_DF;
  _CrtSetDbgFlag(tmpFlag);

  //-------------------------------------------------
  // timing 
  //-------------------------------------------------

  image_number = 0;
  time_since_start = 0;

  //-------------------------------------------------
  // random number generation
  //-------------------------------------------------

  initialize_CR_Random();

  //-------------------------------------------------
  // image source (camera/movie)
  //-------------------------------------------------

  image_shrink_factor = 4;

  // all images in a directory

#ifdef SOURCE_DIRECTORY
  num_still_image_iterations = 10;
  cur_still_image_iteration = 0;

  sprintf(ls_filedir, "ls %s > temp.txt", in_filedir);
  system(ls_filedir);
  filelist_fp = fopen("temp.txt", "r");
  if (!fgets(in_filename, 80, filelist_fp))
    exit(1);
  in_filename[strlen(in_filename)-1] = '\0';
  sprintf(image_filename, "%s%s", in_filedir, in_filename);
  raw_im = read_CR_Image(image_filename);
  planx_w = raw_im->w / image_shrink_factor;
  planx_h = raw_im->h / image_shrink_factor;
  //  free_CR_Image(raw_im);
  //  rewind(filelist_fp);
#endif

  // avi

#ifdef SOURCE_AVI
  gc_movie = open_CR_Movie(avi_filename);
  raw_im = gc_movie->im;
  planx_w = raw_im->w / image_shrink_factor;
  planx_h = raw_im->h / image_shrink_factor;
#endif

  gc_im = make_CR_Image(planx_w, planx_h, 0);
  fplanx_w = (float) planx_w;
  fplanx_h = (float) planx_h;

  //-------------------------------------------------
  // display
  //-------------------------------------------------

  glutInit(&argc, argv);
  //glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  //  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  glutInitWindowSize(gc_im->w, gc_im->h); 
  glutInitWindowPosition(100, 100);
  glutCreateWindow("gc");

  glutDisplayFunc(display); 
  glutMouseFunc(mouse); 
  glutIdleFunc(process_image);

  glPixelZoom(1, -1);
  glRasterPos2i(-1, 1);

  // initialize viewing values  

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, gc_im->w, 0, gc_im->h);

  glClearColor(0, 0, 0, 0);

#ifdef DEBUG
  print_opengl_info();
#endif

  //-------------------------------------------------
  // image processing/analysis
  //-------------------------------------------------

  int i, j, t, x, y;
  double theta;

  double start_lambda = 16.0;   // 4 = 12 x 12 kernel at first level, 8 = 25 x 25, 16 = 50 x 50
  double delta_lambda = 1.0;   // 1 = kernel size basically doubles in each dimension
  int num_wavelengths = 1; // was 3 for CVPR; was 4 for all of the more recent stuff; was 5 for all of the full-res unsurfaced_straight experiments

  //  int num_orientations = 72;  // best results so far with 72
  //  double start_theta = 0.0; 
  //  double delta_theta  = 180.0 / (double) num_orientations; 

  //  int num_orientations = 72;  // best results so far with 72
  int num_orientations = 48;  // best results so far with 72
  double start_theta = 0.0; 
  double delta_theta  = 180.0 / (double) num_orientations; 

  //  int max_filter_frame = 25;    
  int max_filter_frame = 50 / image_shrink_factor;    
  //  int max_filter_frame = 50; // for a top kernel size of 101 x 101

  int voter_left = max_filter_frame;
  int voter_right = gc_im->w - max_filter_frame;
  //  int voter_top = max_filter_frame;
  int voter_top = gc_im->h / 4;
  int voter_bottom = gc_im->h - max_filter_frame;
//   int voter_dx = 2;   // 2 standard
//   int voter_dy = 2;   // 2 standard
  int voter_dx = 1;   // 2 standard
  int voter_dy = 1;   // 2 standard
  int voter_strip_height = 250 / image_shrink_factor;

#ifdef STRIP_VP
  do_planx_strip = TRUE;
#endif
#ifndef STRIP_VP
  do_planx_strip = FALSE;
#endif

#ifdef OPENGL_VOTE_VP
  do_opengl_voting = TRUE;
#endif
#ifndef OPENGL_VOTE_VP
  do_opengl_voting = FALSE;
#endif

  int candidate_left = max_filter_frame;                   // max_filter_frame;
  int candidate_right = gc_im->w - max_filter_frame;        // planx_w - max_filter_frame;
//   int candidate_left = 0;                   // max_filter_frame;
//   int candidate_right = gc_im->w;        // planx_w - max_filter_frame;
  int candidate_top = 0;
  int candidate_bottom = 3 * gc_im->h /4;
  int candidate_dx = 1;  // 1
  int candidate_dy = 1;  // 1

  init_candidate_and_voter_regions(voter_top, voter_bottom, voter_left, voter_right, voter_strip_height, voter_dx, voter_dy,
				   candidate_top, candidate_bottom, candidate_left, candidate_right, candidate_dx, candidate_dy);

#ifdef RUN_VP
  PlanX_initialize_gabor_filters(start_lambda, delta_lambda, num_wavelengths, start_theta, delta_theta, num_orientations, gc_im);

  initialize_voting();

  initialize_vp_particlefilter();
#endif

#ifdef RUN_SEGMENTER
  init_seg_voter_region(0, gc_im->h, 0, gc_im->w, 1, 1);
  initialize_segmenter();
#endif
}

//----------------------------------------------------------------------------

void init_per_image()
{
  //-------------------------------------------------
  // start timer
  //-------------------------------------------------

  image_time = 0;
  CR_difftime();

  // load/grab new image: all images in a directory

#ifdef SOURCE_DIRECTORY
  if (cur_still_image_iteration < num_still_image_iterations) 
    cur_still_image_iteration++;
  else {   // get rid of old image; load new one
    // write result to file?
    free_CR_Image(raw_im);
    if (!fgets(in_filename, 80, filelist_fp)) {
      system("rm temp.txt");
      exit(1);
    }
    in_filename[strlen(in_filename)-1] = '\0';
    sprintf(image_filename, "%s%s", in_filedir, in_filename);
    raw_im = read_CR_Image(image_filename);
    cur_still_image_iteration = 0;
#ifdef RUN_VP
    reset_vp_particlefilter();
#endif
  }
#endif

  // load/grab new image: AVI

  //  for (voter_current_top = planx_voter_bottom - planx_voter_strip_height; 
  //     voter_current_top >= planx_voter_top;
  //     voter_current_top--) {


#ifdef SOURCE_AVI
  if (image_number <= gc_movie->lastframe)
    get_frame_CR_Movie(gc_movie, image_number);
#endif

  image_time += grab_time = CR_difftime();

  // reduce image from raw size by shrink factor in each dimension

  //  iplDecimateBlur(gc_movie->im->iplim, gc_im->iplim, 
  iplDecimateBlur(raw_im->iplim, gc_im->iplim, 
		  1, image_shrink_factor, 1, image_shrink_factor, IPL_INTER_LINEAR, 3, 3);

#ifdef SOURCE_DIRECTORY
  //  free_CR_Image(raw_im);
#endif

  image_time += blur_time = CR_difftime();
 
  //-------------------------------------------------
  // vanishing point estimation
  //-------------------------------------------------

#ifdef RUN_VP
  // convolve gabor filters with new image using fourier transforms via convolution theorem

  //  PlanX_run_gabor_filters(gc_im);
  fft_run_gabor_filters(gc_im);

  image_time += gabor_time = CR_difftime();

  // compute dominant orientations & count votes

  compute_dominant_orientations();

  //  compute_vp_votes(gc_im);

  tally_votes(gc_im);

  image_time += vote_time = CR_difftime();
#endif

  //-------------------------------------------------
  // segmentation
  //-------------------------------------------------

#ifdef RUN_SEGMENTER
  update_segmenter(gc_im);

  image_time += segment_time = CR_difftime();
#endif

  //-------------------------------------------------
  // display
  //-------------------------------------------------

  glutPostRedisplay();
  glFlush();

  image_time += display_time = CR_difftime();

  //-------------------------------------------------
  // timing
  //-------------------------------------------------

  //  image_time = CR_difftime();
  time_since_start += image_time;

  image_number++;

#ifdef STRIP_VP
  if (planx_voter_current_top >= planx_voter_top)
    planx_voter_current_top--;
  else
    planx_voter_current_top = planx_voter_bottom - planx_voter_strip_height; 
#endif

}

//----------------------------------------------------------------------------

void process_image()
{
  init_per_image();

#ifdef DEBUG
  if (!(image_number % 10)) {
  //    printf("%lf fps\n", 1 / image_time);
  printf("%i/%i: %lf fps (total time %lf)\n", image_number, gc_movie->numframes, 1 / image_time, time_since_start);
  printf("\tgrab =\t\t%lf\n\tblur =\t\t%lf\n\tgabor =\t\t%lf\n\tvote =\t\t%lf\n\tsegment =\t%lf\n\tdisplay =\t%lf\n\t------------------------\n\timage =\t\t%lf\n", grab_time, blur_time, gabor_time, vote_time, segment_time, display_time, image_time);
 printf("%li bytes\n", CR_allocated_bytes());
  }
#endif
 
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
