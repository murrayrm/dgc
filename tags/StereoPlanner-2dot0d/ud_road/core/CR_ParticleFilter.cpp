//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_ParticleFilter.hh"

// C = random_normal_sample_CR_Matrix(A, B);

// A = make_CR_Matrix(n, m)
// free_CR_Matrix(A)
// B = scale_CR_Matrix(n, A)
// B = copy_CR_Matrix(A)
// C = multiply_CR_Matrix(A, B) => C = product_CR_Matrix(A, B)
// S = sum_CR_Matrices(M_array, n) => S = sum_CR_Matrix_array(M_array, n)
// uniform_CRRandom() => uniform_CR_Random()

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void reset_CR_XCondensation(CR_Condensation *con)
{
  int i;

  con->updates = 0;

  //----------------------------------------------------
  // generate samples from prior p(x)
  //----------------------------------------------------

  for (i = 0; i < con->n; i++) 
    con->samp_prior(con->s[i]);

}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// mean is a d x 1 matrix, where d is the dimensionality of the state
// entries of s are also d x 1 matrices

CR_Condensation *make_CR_XCondensation(int n, CR_Vector *mean, CR_Matrix *Cov, CondP_ZX condprob_zx, SamplePrior samp_prior, SampleState samp_state, DynamicsState dyn_state)
{
  int i;
  CR_Condensation *con;

  print_CR_Matrix(Cov);

  con = (CR_Condensation *) CR_malloc(sizeof(CR_Condensation));

  con->updates = 0;

  con->n = n;

  con->s = (CR_Vector **) CR_calloc(n, sizeof(CR_Vector *));
  con->s_new = (CR_Vector **) CR_calloc(n, sizeof(CR_Vector *));

  con->pi = make_CR_Vector("pi", n);
  con->c = make_CR_Vector("c", n);

  con->pi_s = (CR_Vector **) CR_calloc(con->n, sizeof(CR_Vector *)); 

  con->sdet = copy_CR_Vector(mean);

  con->x = copy_CR_Vector(mean);

  con->condprob_zx = condprob_zx;
  con->samp_prior = samp_prior;
  con->samp_state = samp_state;
  con->dyn_state = dyn_state;

  con->Cov = copy_CR_Matrix(Cov);
  con->Covsqrt = copy_CR_Matrix(con->Cov);
  square_root_CR_Matrix(con->Cov, con->Covsqrt);
  con->Covalpha = gaussian_alpha_CR_Matrix(con->Cov);

  con->M = make_CR_Matrix("M", n, mean->rows);
  con->MT = make_CR_Matrix("MT", mean->rows, n);
  con->C = make_CR_Matrix("C", mean->rows, mean->rows);
  con->mean = copy_CR_Vector(mean);

  //----------------------------------------------------
  // generate samples from prior p(x)
  //----------------------------------------------------

  for (i = 0; i < n; i++) {
    con->s[i] = copy_CR_Vector(mean);
    con->pi_s[i] = copy_CR_Vector(mean);
    con->samp_prior(con->s[i]);
    con->s_new[i] = copy_CR_Vector(con->s[i]);
  }

  return con;
}

//----------------------------------------------------------------------------

// z is array of measurements, n is how many there are

void update_CR_XCondensation(void *Z, CR_Condensation *con)
{
  int i, samp_chosen;
  double r;

  //----------------------------------------------------

  con->updates++;

  //----------------------------------------------------
  // FIRST UPDATE:
  // compute conditional probabilities p(z|x) 
  // and keep track of their partial sums
  //----------------------------------------------------

  /*
  if (con->updates == 1) {      // this is the first update

    con->pi_sum = 0;
    for (i = 0; i < con->n; i++) {
      con->pi->x[i] = con->condprob_zx(Z, con->s[i]);
      con->pi_sum += con->pi->x[i]; 
    }
    
    for (i = 0; i < con->n; i++) {
      con->pi->x[i] /= con->pi_sum;
      if (i == 0)
	con->c->x[i] = con->pi->x[i];
      else
	con->c->x[i] = con->c->x[i - 1] + con->pi->x[i];
    }
  }

  //----------------------------------------------------
  // pick n samples (with replacement) from sample set,
  // choosing sample s_i with probability pi_i,
  // to obtain s_new
  //----------------------------------------------------
  
  for (i = 0; i < con->n; i++) {
    r = uniform_CR_Random();
    samp_chosen = interval_membership(0, con->n - 1, con->c, r);
    copy_CR_Vector(con->s[samp_chosen], con->s_new[i]);
  }

  //----------------------------------------------------
  // predict by applying deterministic and stochastic
  // transformations to elements of s_new,
  // which become s for the next round.
  // also, keep track of weighted samples in pi_s
  // and estimate state
  //----------------------------------------------------

  for (i = 0; i < con->n; i++) {
    //    product_sum_CR_Matrix(1.0, con->F, con->s_new[i], 0.0, NULL, con->sdet);
    con->dyn_state(con->s_new[i], con->sdet);
    con->samp_state(con->sdet, con->s[i]);
    //    sample_gaussian_CR_Matrix(con->Qalpha, con->s[i], con->sdet, con->Qsqrt);
  }
  */

  //----------------------------------------------------
  // compute conditional probabilities p(z|x) 
  // and keep track of their partial sums
  //----------------------------------------------------

  con->pi_sum = 0;
  for (i = 0; i < con->n; i++) {
    con->pi->x[i] = con->condprob_zx(Z, con->s[i]);
    con->pi_sum += con->pi->x[i]; 
  }
  
  for (i = 0; i < con->n; i++) {
    con->pi->x[i] /= con->pi_sum;
    if (i == 0)
      con->c->x[i] = con->pi->x[i];
    else
      con->c->x[i] = con->c->x[i - 1] + con->pi->x[i];
  }

  //----------------------------------------------------
  // pick n samples (with replacement) from sample set,
  // choosing sample s_i with probability pi_i,
  // to obtain s_new
  //----------------------------------------------------
  
  for (i = 0; i < con->n; i++) {
    r = uniform_CR_Random();
    samp_chosen = interval_membership(0, con->n - 1, con->c, r);
    copy_CR_Vector(con->s[samp_chosen], con->s_new[i]);
  }

  //----------------------------------------------------
  // predict by applying deterministic and stochastic
  // transformations to elements of s_new,
  // which become s for the next round.
  // also, keep track of weighted samples in pi_s
  // and estimate state
  //----------------------------------------------------

  for (i = 0; i < con->n; i++) {
    //    product_sum_CR_Matrix(1.0, con->F, con->s_new[i], 0.0, NULL, con->sdet);
    con->dyn_state(con->s_new[i], con->sdet);
    con->samp_state(con->sdet, con->s[i]);
    //    sample_gaussian_CR_Matrix(con->Qalpha, con->s[i], con->sdet, con->Qsqrt);
  }

  //----------------------------------------------------
  // keep track of weighted samples and their sum,
  // which is the estimated state.
  // a mode finder makes more sense
  //----------------------------------------------------
  
  // remember to initialize M, MT, mean, and C when con is created
  /*
  convert_CR_Vector_array_to_CR_Matrix(con->s, con->M);
  mean_cov_CR_Matrix(con->M, con->MT, con->mean, con->C);
  print_CR_Vector(con->mean);
  print_CR_Matrix(con->C);
  printf("%lf\n", determinant_CR_Matrix(con->C));
  */

  for (i = 0; i < con->n; i++) { 
    copy_CR_Vector(con->s[i], con->pi_s[i]);
    scale_CR_Vector(con->pi->x[i], con->pi_s[i]);
  }

  sum_CR_Vector_array(con->pi_s, con->n, con->x);
  //  print_CR_Vector(con->x);

  // confidence is ratio of determinant of basic process covariance to overall sample distribution covariance
  // but these samples are weighted (pi_s vs. s)--how to incorporate that?

  //----------------------------------------------------
  // clean up
  //----------------------------------------------------

//   for (i = 0; i < n; i++) 
//     free_CR_Vector(z[i]);
// 
//   CR_free_calloc(z, n, sizeof(CR_Vector *));
}

//----------------------------------------------------------------------------

// mean, cov specify initial Gaussian that describes prior p(x0)
// mean is a d x 1 matrix, where d is the dimensionality of the state
// cov is a d x d matrix
// entries of s are also d x 1 matrices

CR_Condensation *make_CR_Condensation(int n, CR_Matrix *F, CR_Matrix *Q, CR_Vector *mean, CR_Matrix *Cov, CondP_ZX condprob_zx, SamplePrior samp_prior, SampleState samp_state)
{
  int i;
  CR_Condensation *con;

  //  if (!mean || !Cov || mean->rows != Cov->rows || Cov->rows != Cov->cols) 
  //    CR_error("condensation mean mismatch");

  con = (CR_Condensation *) CR_malloc(sizeof(CR_Condensation));

  con->updates = 0;

  con->n = n;

  con->F = copy_CR_Matrix(F);
  //  con->Q = copy_CR_Matrix(Q);
  //  con->Qsqrt = copy_CR_Matrix(Q);
  //  square_root_CR_Matrix(con->Q, con->Qsqrt);
  //  con->Qalpha = gaussian_alpha_CR_Matrix(con->Q);

  con->s = (CR_Vector **) CR_calloc(n, sizeof(CR_Vector *));
  con->s_new = (CR_Vector **) CR_calloc(n, sizeof(CR_Vector *));

  con->pi = make_CR_Vector("pi", n);
  con->c = make_CR_Vector("c", n);
  //  con->pi = (float *) CR_calloc(n, sizeof(float));
  //  con->c = (float *) CR_calloc(n, sizeof(float));

  con->pi_s = (CR_Vector **) CR_calloc(con->n, sizeof(CR_Vector *)); 

  //  con->mean = copy_CR_Vector(mean);
  //  con->Cov = copy_CR_Matrix(Cov);
  //  con->Covsqrt = copy_CR_Matrix(Cov);
  //  square_root_CR_Matrix(con->Cov, con->Covsqrt);
  //  con->Covalpha = gaussian_alpha_CR_Matrix(con->Cov);

  con->sdet = copy_CR_Vector(mean);

  con->x = copy_CR_Vector(mean);
  //  con->xmed = make_CR_Matrix(2, 1);     // specialized for postional state

  //  con->presample = FALSE;

  con->condprob_zx = condprob_zx;
  con->samp_prior = samp_prior;
  con->samp_state = samp_state;

  //----------------------------------------------------
  // generate samples from prior p(x)
  //----------------------------------------------------

  for (i = 0; i < n; i++) {
    con->s[i] = copy_CR_Vector(mean);
    con->pi_s[i] = copy_CR_Vector(mean);
    con->samp_prior(con->s[i]);
    //    sample_gaussian_CR_Matrix(con->Covalpha, con->s[i], con->mean, con->Covsqrt);
    con->s_new[i] = copy_CR_Vector(con->s[i]);
  }

  return con;
}

//----------------------------------------------------------------------------

// z is array of measurements, n is how many there are

void update_CR_Condensation(void *Z, CR_Condensation *con)
{
  int i, samp_chosen;
  double r;

  //----------------------------------------------------

  con->updates++;

  //----------------------------------------------------
  // FIRST UPDATE:
  // compute conditional probabilities p(z|x) 
  // and keep track of their partial sums
  //----------------------------------------------------

  if (con->updates == 1) {      // this is the first update

    con->pi_sum = 0;
    for (i = 0; i < con->n; i++) {
      con->pi->x[i] = con->condprob_zx(Z, con->s[i]);
      con->pi_sum += con->pi->x[i]; 
    }
    
    for (i = 0; i < con->n; i++) {
      con->pi->x[i] /= con->pi_sum;
      if (i == 0)
	con->c->x[i] = con->pi->x[i];
      else
	con->c->x[i] = con->c->x[i - 1] + con->pi->x[i];
    }
  }

  //----------------------------------------------------
  // pick n samples (with replacement) from sample set,
  // choosing sample s_i with probability pi_i,
  // to obtain s_new
  //----------------------------------------------------
  
  for (i = 0; i < con->n; i++) {
    r = uniform_CR_Random();
    samp_chosen = interval_membership(0, con->n - 1, con->c, r);
    copy_CR_Vector(con->s[samp_chosen], con->s_new[i]);
  }

  //----------------------------------------------------
  // predict by applying deterministic and stochastic
  // transformations to elements of s_new,
  // which become s for the next round.
  // also, keep track of weighted samples in pi_s
  // and estimate state
  //----------------------------------------------------

  for (i = 0; i < con->n; i++) {
    product_sum_CR_Matrix(1.0, con->F, con->s_new[i], 0.0, NULL, con->sdet);
    con->samp_state(con->sdet, con->s[i]);
    //    sample_gaussian_CR_Matrix(con->Qalpha, con->s[i], con->sdet, con->Qsqrt);
  }

  //----------------------------------------------------
  // compute conditional probabilities p(z|x) 
  // and keep track of their partial sums
  //----------------------------------------------------

  con->pi_sum = 0;
  for (i = 0; i < con->n; i++) {
    con->pi->x[i] = con->condprob_zx(Z, con->s[i]);
    con->pi_sum += con->pi->x[i]; 
  }
  
  for (i = 0; i < con->n; i++) {
    con->pi->x[i] /= con->pi_sum;
    if (i == 0)
      con->c->x[i] = con->pi->x[i];
    else
      con->c->x[i] = con->c->x[i - 1] + con->pi->x[i];
  }

  //----------------------------------------------------
  // keep track of weighted samples and their sum,
  // which is the estimated state.
  // a mode finder makes more sense
  //----------------------------------------------------
                                                          
  for (i = 0; i < con->n; i++) { 
    copy_CR_Vector(con->s[i], con->pi_s[i]);
    scale_CR_Vector(con->pi->x[i], con->pi_s[i]);
  }

  sum_CR_Vector_array(con->pi_s, con->n, con->x);

  //----------------------------------------------------
  // clean up
  //----------------------------------------------------

//   for (i = 0; i < n; i++) 
//     free_CR_Vector(z[i]);
// 
//   CR_free_calloc(z, n, sizeof(CR_Vector *));
}

//----------------------------------------------------------------------------

// conditional probability of measurements z (of which there
// are n) given that the state is x

// this obviously varies depending on the application/specific
// tracking problem

// bear in mind that x's position may not even be in the image

// normalized later

double test_cond_prob_zx(CR_Vector **z, int n, CR_Vector *x, CR_Condensation *con)
{
  int i;
  double totdist, meandist;

  // second stab for point clutter tracking: 
  // sum over measurements of e^{-0.5 * distance}
  // (sort of a Gaussian with unit variance)
  // assumes 1-D

  totdist = 0;
  for (i = 0; i < n; i++)
    totdist += exp(-0.5 * fabs(z[i]->x[0] - x->x[0]));
  
  meandist = totdist / (double) n;
  
  //    printf("using p(z|x) = %f\n", meandist);
  
  return meandist;
}

//----------------------------------------------------------------------------

int interval_membership(int l, int u, CR_Vector *c, double r)
{
  int m;

  if (l == u)
    return l;
  else {
    m = (l + u) / 2;
    if (c->x[m] >= r)
      return interval_membership(l, m, c, r);
    else {
      if (m == u - 1)
	return u;
      else
	return interval_membership(m, u, c, r);
    }
  }
}

//----------------------------------------------------------------------------

/*
void presample_CR_Condensation(CR_Condensation *con)
{
  int i, samp_chosen;
  double r;
  CR_Matrix *temp;

  if (con->updates > 0) {

    //----------------------------------------------------
    // pick n samples (with replacement) from sample set,
    // choosing sample s_i with probability pi_i,
    // to obtain s_new
    //----------------------------------------------------
    
    for (i = 0; i < con->n; i++) {
      r = uniform_CR_Random();
      samp_chosen = interval_membership(0, con->n - 1, con->c, r);
      //      if (con->s_new[i])
      //	free_CR_Vector(con->s_new[i]);
      copy_CR_Vector(con->s[samp_chosen], con->s_new[i]);
    }
    
    //----------------------------------------------------
    // predict by applying deterministic and stochastic
    // transformations to elements of s_new,
    // which become s for the next round.
    // also, keep track of weighted samples in pi_s
    // and estimate state
    //----------------------------------------------------
    
    for (i = 0; i < con->n; i++) {
      product_sum_CR_Matrix(1.0, con->F, con->s_new[i], 0.0, NULL, con->sdet);
      //      temp = product_CR_Matrix(con->F, con->s_new[i]);
      //      free_CR_Matrix(con->s[i]);
      sample_gaussian_CR_Matrix(con->Qalpha, con->s[i], con->sdet, con->Qsqrt);
      //      con->s[i] = random_normal_sample_CR_Matrix(temp, con->Q);
      //      free_CR_Matrix(temp);
    }

  }

}
*/

//----------------------------------------------------------------------------

void print_CR_Condensation(CR_Condensation *con)
{
  int i;

  printf("\n");

  printf("%i ------------------------------\n\n", con->updates);

  for (i = 0; i < con->n; i++) {
    printf("%i:\n", i);
    print_CR_Vector(con->s[i]);
  }

  printf("\n");

  for (i = 0; i < con->n; i++)
    printf("%i: pi = %f\n", i, con->pi->x[i]);

  printf("\n");

  for (i = 0; i < con->n; i++)
    printf("%i: c = %f\n", i, con->c->x[i]);

  printf("\n");
  printf("***********************\n");
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
