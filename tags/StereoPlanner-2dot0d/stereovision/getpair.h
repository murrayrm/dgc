/**************************************************************************
**       Title: grab a stereo pair using libdc1394
**
**   manage cameras, grab synced pairs
**   Based on 'samplegrab' from Chris Urmson, more work done by Steve Waydo
**   this version by Gunnar Ristroph
**
**************************************************************************/
#ifndef GETPAIR_H
#define GETPAIR_H

#include <stdio.h>
#include <libraw1394/raw1394.h>
#include <libdc1394/dc1394_control.h>
#include <stdlib.h>
#include <string.h>


//Below are the new dragonflies - SHORT RANGE
#define SHORT_LEFT_CAMERA_STRING "0x003D3039"
#define SHORT_RIGHT_CAMERA_STRING "0x003D3038"

//Below are the new draonflies - LONG RANGE
#define LONG_LEFT_CAMERA_STRING "0x003D31F5"
#define LONG_RIGHT_CAMERA_STRING "0x003D303B"

//Below are the old dragonflies - lent by Kevin Watson
#define LEFT_CAMERA_STRING "0x002F51E9"
#define RIGHT_CAMERA_STRING "0x002F51EA"

#define LEFT_CAM     0
#define RIGHT_CAM    1 

enum {
  HIGH_RES,
  SHORT_RANGE,
  LONG_RANGE,

  NUM_PAIRS
};



// setup handle, cameras, node2cam; start data transmission
int cameras_start( raw1394handle_t * handle_ptr,
		   int node2cam[],
		   dc1394_cameracapture cameras[], 
		   int which_pair);

// stop cameras, close handles
int cameras_stop( raw1394handle_t handle,
		  dc1394_cameracapture cameras[] );

// take a synced shot
int cameras_snap(raw1394handle_t handle, 
		 dc1394_cameracapture cameras[]);

// print diagnostic information and identifying strings to stdout
int cameras_info();

#endif
 
