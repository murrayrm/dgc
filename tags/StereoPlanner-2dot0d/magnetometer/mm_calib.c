#include "magnetometer.h"


int main()
{
	float a[MM_MAX_RETURN_PARAM_NUM];
	int error;

	mm_init(0);
	//sleep(1);
	//sleep(2);

	if((error=mm_send_command(MM_LC_SC, a, mm_command_return_num[MM_LC_SC]))<0)
		fprintf(stderr, "Error reading last calibration score\n");
	else printf("last calibration score: H:%f V:%f M:%f\n", a[0], a[1], a[2]);

	/*if((error=mm_send_command(MM_CR_CA, a, mm_command_return_num[MM_CR_CA]))<0)
		fprintf(stderr, "Error clearing calibration\n");
	else printf("clear calibration score: H:%f V:%f M:%f\n", a[0], a[1], a[2]);*/
   	
	mm_calibration();

	mm_uninit();

	return 0;	
}
