#ifndef __LOCAL_MAP_H__
#define __LOCAL_MAP_H__

/* LocalMap.h
   Author: J.D. Salazar
   Created: 7-15-03
   
   This class is the map structure for the local obstacle avoidance mechanism to be used by Team Caltech in the 2004 Darpa 
   Grand Challenge. */

#include <map>
#include <functional>
#include <stdexcept>
#include <algorithm>
#include <iostream>
#include <math.h>
#include "../pertocla/per_to_cla.h"
#include "CellData.h"
#include "svTrajectory.h"

#define MAX_VELOCITY 1  // in meters/second
#define PI 3.14159265

using namespace std;

class LocalMap 
{
 public:

  /* Constructors */
  LocalMap(int numXCells = DEFAULT_NUM_X_CELLS, int numYCells = DEFAULT_NUM_Y_CELLS, 
	   float xCellResolution = DEFAULT_X_CELL_RESOLUTION, float yCellResolution = DEFAULT_Y_CELL_RESOLUTION, 
	   double globalXOrigin = DEFAULT_GLOBAL_X_ORIGIN, double globalYOrigin = DEFAULT_GLOBAL_Y_ORIGIN, 
	   int xVehicleDisplacement = DEFAULT_X_VEHICLE_DISPLACEMENT, int yVehicleDisplacement = DEFAULT_Y_VEHICLE_DISPLACEMENT,
	   int timeOrigin = DEFAULT_TIME_ORIGIN);

  /* Destructor */
  ~LocalMap();

  /* Accessors */
  int getNumXCells() const { return _numXCells; }
  int getNumYCells() const { return _numYCells; }
  float getXCellResolution() const { return _xCellResolution; }
  float getYCellResolution() const { return _yCellResolution; }
  double getGlobalXOrigin() const { return roundToNearestX( _globalXOrigin ); }
  double getGlobalYOrigin() const { return roundToNearestY( _globalYOrigin ); }
  int getXVehicleDisplacement() const { return _xVehicleDisplacement; }
  int getYVehicleDisplacement() const { return _yVehicleDisplacement; }
  int getCurrentTime() const { return _currTime; }


  // Returns the location of the vehicle in global coordinates.
  double getGlobalXVehicleLocation() const 
    { 
      return roundToNearestX( getGlobalXOrigin() + getXVehicleDisplacement() * getXCellResolution() ); 
    }

  double getGlobalYVehicleLocation() const 
    { 
      return roundToNearestY( getGlobalYOrigin() + getYVehicleDisplacement() * getYCellResolution() ); 
    }

  CellData getCellData(double globalX, double globalY) const;

  /* Mutators */
  void setGlobalVehicleLocation(double globalX, double globalY);
  void addCellData(double globalX, double globalY, CellData &cell) throw(std::out_of_range);
  bool updateMap(const serialMap map);
  bool updateGoodness();

  // Rounds global coordinates to the nearest values based on xCellResolution or yCellResolution
  // For example if the xCellResolution is .125 then
  //   roundToNearestX( 1223.6111123 ) = 1223.60
  static double roundToNearest(double n, float res)
    {
      return double(std::floor(n / res + .5) * res);
    }
  double roundToNearestX(double globalX) const { return roundToNearest(globalX, getXCellResolution()); }
  double roundToNearestY(double globalY) const { return roundToNearest(globalY, getYCellResolution()); }

  /* Output */
  // Creates bitmaps of various maps at filename.
  bool outputElevationMap(std::string fileName);
  bool outputObstacleMap(std::string fileName);
  bool outputClassificationMap(std::string fileName);
  bool outputRoughnessMap(std::string fileName);
  bool outputGoodnessMap(std::string fileName);

  // Evaluates along paths for their goodness and velocity
  int getPathGoodness(svTrajectoryType path);
  double getPathVelocity(svTrajectoryType path);

  // Creates paths (lists of cells) based on some data
  /*
    Computes a path as a straight line at a certain angle (in radians, 0 is north, PI/2 is east, etc.)
    and for a certain length (in meters)
  */
  svTrajectoryType computePath(double angle, double length);

  /* Constants */
  // Cell with no data
  static const CellData NO_DATA_CELL;

  // Default values
  static const int DEFAULT_NUM_X_CELLS = 1000;
  static const int DEFAULT_NUM_Y_CELLS = 1000;
  static const float DEFAULT_X_CELL_RESOLUTION = 0.2;  // In meters
  static const float DEFAULT_Y_CELL_RESOLUTION = 0.2;  // In meters
  static const double DEFAULT_GLOBAL_X_ORIGIN = 0;     // In UTM
  static const double DEFAULT_GLOBAL_Y_ORIGIN = 0;     // In UTM
  static const int DEFAULT_LOCAL_X_ORIGIN = 0;
  static const int DEFAULT_LOCAL_Y_ORIGIN = 0;
  static const int DEFAULT_X_VEHICLE_DISPLACEMENT = DEFAULT_NUM_X_CELLS / 2;
  static const int DEFAULT_Y_VEHICLE_DISPLACEMENT = DEFAULT_NUM_Y_CELLS / 2;
  static const int DEFAULT_TIME_ORIGIN = 0;          // Currently stored in ??

private:
  // Declare types for both the coordinates used to access the map and the map.
  // The Coords will use the STL's pair where first and second are the x and y local coordinates respectively.
  // The Map will use the STL's map which maps a CoordsType to a CellData object.
  typedef std::pair<int, int> CoordsType;
  typedef std::map< CoordsType, CellData > MapType;
  

  // The map is stored as a 2 dimensional map of CellData.
  MapType dataMap;

  // The number of cells in the X and Y direction for the map.
  int _numXCells;
  int _numYCells;

  // The width of cells in the X and Y direction in meters.
  float _xCellResolution;
  float _yCellResolution;

  // The coordinates of the lower left hand corner of the map in global coordinates (UTM).
  double _globalXOrigin;
  double _globalYOrigin;

  // Because the map shifts as the vehicle moves the vehicle is always a constant distance from each side of the map.
  // These variables are how far away the vehicle is from the origin in local coordinates.
  int _xVehicleDisplacement;
  int _yVehicleDisplacement;

  // This stores the time that the last data to update the map was taken.
  int _currTime;

  // The map is stored in a wrap around fashion so that all the data does not need to be shifted every time the vehicle is moved.
  // Therefore, the "origin" or lower left corner of the map changes and the postion of the origin is stored in local coordinates.
  int _localXOrigin;
  int _localYOrigin;

  /* Private Accessors */
  int getLocalXOrigin() const { return _localXOrigin; }
  int getLocalYOrigin() const { return _localYOrigin; }

  // Converst global coordinates to local coordinates.
  int globalToLocalX(double globalX) const;
  int globalToLocalY(double globalY) const;

  // Converts global coordinates to pseudolocal coordinates.
  // Pseudolocal coordinates are local coordinates that haven't been transformed using modular arithmetic so they are between 
  // 0 and numXCells or numYCells accordingly.
  int globalToPseudolocalX(double globalX) const;
  int globalToPseudolocalY(double globalY) const;

  // Converts pseudolocal coordinates to global coordinates
  double pseudolocalToGlobalX(int x) const;
  double pseudolocalToGlobalY(int y) const;

  double distance(double x1, double y1, double x2, double y2);

  double floorToNearest(double current, double origin, double res, double direction);
  double distToNextLine(double current, double origin, double res, double dir);
  double roundToNextLine(double current, double origin, double res, double dir);

  /* Private Mutators */
  void setNumXCells(int n) { _numXCells = n; }
  void setNumYCells(int n) { _numYCells = n; }
  void setXCellResolution(float n) { _xCellResolution = n; }
  void setYCellResolution(float n) { _yCellResolution = n; }
  void setGlobalXOrigin(double n) { _globalXOrigin = roundToNearestX(n); }
  void setGlobalYOrigin(double n) { _globalYOrigin = roundToNearestY(n); }
  void setXVehicleDisplacement(int n) { _xVehicleDisplacement = n; }
  void setYVehicleDisplacement(int n) { _yVehicleDisplacement = n; }
  void setLocalXOrigin(int n) { _localXOrigin = n; }
  void setLocalYOrigin(int n) { _localYOrigin = n; }
  void setCurrentTime(int n) { _currTime = n; }

  // Checks if the global coordinates given are in the local map
  bool inMap(double globalX, double globalY) const;
    
  /* Functions to erase parts of the map */
  void eraseMap();
  void eraseRow( int localX );
  void eraseCol( int localY );

  // This variable is only used when creating the bitmaps of the map.
  static const double vehicleSize = 3;    // Needs to be same units as X/Y Cell Resolution
};

#endif
