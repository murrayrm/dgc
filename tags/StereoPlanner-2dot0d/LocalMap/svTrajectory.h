#ifndef __SVTRAJECTORY_H__
#define __SVTRAJECTORY_H__

#include <vector>

using namespace std;

/*
  A pair of doubles where the first is the X coordinate (in UTM) of a cell
  on the local stereovision map, and the second is the Y coordinate (in UTM)
*/
typedef std::pair<double, double> svCoordsType;

//A trajectory composed of grid cells (essentially a path)
typedef std::vector<svCoordsType> svTrajectoryType;

//A list of trajectories (paths)
typedef std::vector<svTrajectoryType> svTrajectoryListType;

#endif
