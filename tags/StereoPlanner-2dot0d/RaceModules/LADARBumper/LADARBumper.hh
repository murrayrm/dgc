#ifndef LADARBUMPER_HH
#define LADARBUMPER_HH


#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include "MTA/Misc/Time/Time.hh"
#include "LADARBumperDatum.hh"


using namespace std;
using namespace boost;


class LADARBumper : public DGC_MODULE {
public:
  // and change the constructors
  LADARBumper();
  ~LADARBumper();

  // The states in the state machine.
  // Uncomment these if you wish to define these functions.

  void Init();
  void Active();
  //  void Standby();
  void Shutdown();
  //  void Restart();

  // The message handlers. 
  //  void InMailHandler(Mail & ml);
  //  Mail QueryMailHandler(Mail & ml);

  // the status function.
  //string Status();


  // Any functions which run separate threads
  
  // Method protocols
  int  InitLadar();
  void LadarUpdateVotes();
  void LadarShutdown();

  void UpdateState();

  void SparrowDisplayLoop();
  void UpdateSparrowVariablesLoop();

private:
  // and a datum named d.
  LADARBumperDatum d;
};



// enumerate all module messages that could be received
namespace WaypointNavMessages {
  enum {
    // we are not yet receiving messages.
    // A place holder
    NumMessages
  };
};






#endif
