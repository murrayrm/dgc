using namespace std;

#include "fender.hh"
#include "include/Constants.h"
#include "include/global_functions.hh"

// just like GlobalUpdateVotes(), but it takes the centroid of a set of 
// fenderpoints into account instead of waypoints.

void FenderPlanner::FenderUpdateVotes() 
{
  cout << "FenderUpdateVotes()\n" ;
  bool done = false;
  int waypointNumber;
  int segmentNumber;
  double steer_cmd; // commanded steering angle (rad)
  //  double speed_cmd; // commanded speed (m/s)
  
  Vector pos;           // our current UTM position
  Vector vehFrontPos;  // do i need this?
  pos.N = d.SS.Northing;
  pos.E = d.SS.Easting;
  
  /* Vote generation:
   * 1. Determine current segment
   * 2. If no fenderpoints are within the current corridor:
   *    1. Vote all arcs equal
   *    2. If corridor has changed go to 2.0
   *    3. else sleep (10 ms)
   *    4. Go to 2.0
   * 3. Else, find the centroid in corridor
   * 4. Cast votes
   * 5. Go to 1
   */

  //+1 is for now (8-march).  Not precisely sure if necesary
  waypointNumber = d.rddf.getCurrentWaypointNumber(pos.N, pos.E) + 1;
  cout << "Waypoint Number= " << waypointNumber << endl;
  if (waypointNumber == d.fenderpoints.getNumSegments()) {
    segmentNumber = waypointNumber; //handler for last segment, for now  
  }
  else { 
  segmentNumber = waypointNumber;
  }
  cout << "Segment Number= " << segmentNumber << endl;
/* if no fp in segment
     set all votes equal
     return
   else
     find centroid
     calc steer command
     calc speed command
     return 
*/

    
  /* 3. Else, find the centroid in corridor
   * 4. Cast votes
   * 5. Go to 1
   */
  
  if (d.fenderpoints.fenderpointsFOV(segmentNumber, pos)) {
    cout << "fenderpoints in segment\n";
    Vector currentCentroid = d.fenderpoints.findCentroid(segmentNumber, pos);  // find centroid in current segment
    cout << "Centroid.N= " << currentCentroid.N << "\t";
    cout << "Centroid.E= " << currentCentroid.E << endl;
    vehFrontPos.E = pos.E + VEHICLE_LENGTH * cos(M_PI_2 - d.SS.Yaw);  // are these even slightly necessary?
    vehFrontPos.N = pos.N + VEHICLE_LENGTH * sin(M_PI_2 - d.SS.Yaw);
      
    // calculate the desired steering angle 
    steer_cmd = calc_steer_command(pos, currentCentroid, M_PI_2 - d.SS.Yaw);  
    printf("current segment num = %d\n", segmentNumber);
    printf("Current position  = %10.3f m Easting, %10.3f m Northing\n", 
    d.SS.Easting, d.SS.Northing );
    printf("Centroid position = %10.3f m Easting, %10.3f m Northing\n",  currentCentroid.E  , currentCentroid.N );
    printf("Vehicle front position = %10.3f m Easting, %10.3f m Northing\n", vehFrontPos.E  , vehFrontPos.N );
    printf("Relative position = %10.3f m Easting, %10.3f m Northing\n", 
    currentCentroid.E - d.SS.Easting , currentCentroid.N - d.SS.Northing );
    printf("Rel. pos. from front = %10.3f m Easting, %10.3f m Northing\n", 
    currentCentroid.E - vehFrontPos.E, currentCentroid.N - vehFrontPos.N );
    printf("yaw = %f [%.1f degrees], steer_cmd = %f\n", d.SS.Yaw, d.SS.Yaw*180.0/M_PI, steer_cmd ); 
    
    // this stuff is exactly the same as in GlobalUpdateVotes...  
    // since we only want to head in the general direction of the 
    // centroid, there's no reason to create a set of arcs, like most 
    // of the other modules.
    // in the interest of space, if you want to know what used to be 
    // written here, refer to GlobalUpdateVotes
    
    double goodness = 0;
    
    int i = 0;
    
    for(i=0; i<NUMARCS; i++) { 
	double ggparam = 10.0;
	goodness = fmax( ggparam, MAX_GOODNESS - 
			 fabs( GetPhi(i) - steer_cmd )/2.0 * 
			 (MAX_GOODNESS - ggparam) );
	
	// assign the actual vote goodness
	d.fender.Votes[i].Goodness = goodness;
	
	d.fender.Votes[i].Velo = FENDER_MAX_SPEED;  // we don't care about speed, so go as fast as is allowed
    }
  }
  else {  // no fenderpoints in corridor
   cout << "no fenderpoitns in segment\n";
   for(int i = 0; i < NUMARCS; i++) {
      d.fender.Votes[i].Goodness = MAX_VOTE_RANGE;  // this allows us to say "go wherever you want, we don't care"
      d.fender.Votes[i].Velo = FENDER_MAX_SPEED;  // we don't care about speed, so go as fast as is allowed
    }
  //  return; //?
  }
}    
// end FenderUpdateVotes
