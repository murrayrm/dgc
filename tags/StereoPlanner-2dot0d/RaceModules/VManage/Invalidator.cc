#include "VManage.hh"

#include <iostream>
using namespace std;

extern int DEBUG_ON;
extern int ERR_OUT;
extern int OBD_DISABLE;

Timeval OBD_Timeout( 0, 500000 );
Timeval VState_Timeout( 0, 500000);
Timeval EStop_WaitTime( 6, 0); // wait 6 seconds after estop pause released


void VManage::InvalidatorLoop() {

  // Invalidate old data if timeouts
 
  while( !ShuttingDown() ) {

    {
      Lock mylock( d.VMDatumMutex );

      // check obd data
      if ( (TVNow() - d.OBDLastUpdate) > OBD_Timeout) {
	d.s.obd.Velocity = d.s.obd.RPM = 0;
	if (!OBD_DISABLE) d.s.obd.valid = false;
      }
      
      // check VState
      if ( (TVNow() - d.VStateLastUpdate) > VState_Timeout) {
	//	cout << "vstate last update = " 
	//	     << d.VStateLastUpdate - TVNow() << endl;
	d.VStateValid = false;
      } 

      if( d.VStateValid ) {
	if( d.VStateEStopOn ) {
	  d.VStateEStopOn = false;
	  d.EStopGoTime = TVNow();
	}
      } else {
	d.VStateEStopOn = true;
      }



      // check EStop Go
      if( d.KeyboardEStopOn     || d.VMEStopOn     ||
	  d.TransmissionEStopOn || d.VStateEStopOn ||
	  d.EStopMode == 'p'    || d.EStopMode == 'P' ) {
	d.s.vdrive.estop.EStop = 'p';
      } else if( d.EStopMode == 'd'|| d.EStopMode == 'D') {
	d.s.vdrive.estop.EStop = 'd';
      } else if( ! d.KeyboardEStopOn && 
		 ( d.EStopMode == 'o' || d.EStopMode == 'O') &&
		 (TVNow() - d.EStopGoTime) > EStop_WaitTime) {
	// we can go now
	d.s.vdrive.estop.EStop = 'o';
      }
    }


    usleep( 30000 );
  }
}


