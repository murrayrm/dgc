#ifndef GLOBAL_FUNCTIONS_HH
#define GLOBAL_FUNCTIONS_HH

#include "Constants.h" // for USE_MAX_STEER

#define GLOBAL_FUNCTIONS_MIN_SPEED 2.0
//#define GLOBAL_FUNCTIONS_MAX_SPEED 5.0
#define GLOBAL_FUNCTIONS_MAX_SPEED 8.0

//#define GLOBAL_FUNCTIONS_MIN_SPEED 3.0
//#define GLOBAL_FUNCTIONS_MAX_SPEED 7.5
//#define GLOBAL_FUNCTIONS_MAX_ACCEL 0.6
#define GLOBAL_FUNCTIONS_MAX_ACCEL 0.2

#include <math.h>
/* Since withinCorridor uses trig fns that may generate accuracy errors,
 * (i.e. a sum in there may not exactly equal 2*PI), we need a threshold.
 */
const static double PI2_ACCURACY_THRESHOLD = .0001;

struct Vector {
  float N;
  float E;
  Vector() : N(0), E(0) {};
  Vector(float northing, float easting) : N(northing), E(easting) {};
};

double calc_steer_command(Vector curPos, Vector curWay, double curHeading);
double calc_speed_command(double curYaw, Vector curPos, Vector curWay, 
			  Vector nextWay, double curRadius);

float distVector(Vector a, Vector b);
Vector addVector(Vector a, Vector b); //add two vectors
Vector subVector(Vector a, Vector b); //subtract two vectors 
/* 
 *scaleVector(): Adjust the length of Vector a to b
 */
Vector scaleVector(Vector a, float b);

// determine if we are within distance "threshold" of the waypoint
bool withinWaypoint(Vector pos, Vector way, float threshold);

/* withinCorridor()
 * Vector cur_pos, Vector cur_way, Vector next_way, double next_corr_radius
 * Returns a boolean indicating whether we are in the next corridor or not.
 * From this one should update the current waypoint.
 * Note: This won't be useful for cases where the vehicle travels through the
 *       next corridor too fast, because then cur_way and next_way won't be
 *       correct.
 */
bool withinCorridor(Vector cur_pos, Vector cur_way, 
		    Vector next_way, double next_corr_radius);

#endif
