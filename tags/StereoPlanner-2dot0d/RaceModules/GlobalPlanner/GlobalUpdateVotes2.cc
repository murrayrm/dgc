// Fixed up etc. by Rocky Velez, 1/23/04
// Lots of smart smart matlab work by Luis Goncalves, 1/23/04

#include "global.hh"

// CHECK THAT SIN'S AND COS'S ARE IN THE PROPER PLACES!!!!!!

// second version of GlobalUpdateVotes that takes corridor following into account

///////////////////////////////////////////////////
// this function finds the intersections caused by the turn and the lateral boundaries
void find_intersections(double arc_radius, double dn, double de, double dr, double D, Vector &int1, Vector &int2) {

  double sgn = 0;
  if (dn < 0) { sgn = -1; }
  else { sgn = 1; }          

  // the following formula was found at http://mathworld.wolfram.com/Circle-LineIntersection.html
  // the following intersections depict the two points that occur when the circle caused by the turn intersect the LB
  int1.N = ((D * dn) + (sgn * de * sqrt(arc_radius * arc_radius * dr * dr - D * D))) / (dr * dr);
  int1.E = ((D * -1 * de) + (fabs(dn) * sqrt(arc_radius * arc_radius * dr * dr - D * D))) / (dr * dr);

  //  cout << "int1 - N: " << int1.N << ", E: " << int1.E << endl;

  int2.N = ((D * dn) - (sgn * de * sqrt(arc_radius * arc_radius * dr * dr - D * D))) / (dr * dr);
  int2.E = ((D * -1 * de) - (fabs(dn) * sqrt(arc_radius * arc_radius * dr * dr - D * D))) / (dr * dr);

  //  cout << "int2 - N: " << int2.N << ", E: " << int2.E << endl;
}

///////////////////////////////////////////////////
/* From the RDDF we know that at each waypoint there exist a lateral boundary
 * (LB) to the next waypoint. Also, the vehicle drives in arcs, so in order to
 * calculate the goodness factor of each arc, we need to know the distance
 * between the  vehicle and the boundary, which first depends on finding the
 * intersection between any given arc and the LB
 */
double distance_to_boundary(int i, Vector pos, double yaw, double arc_radius,
			    double LB, Vector currentWaypoint, 
			    Vector nextWaypoint) {

  // First decide if we're making a right-hand or left-hand turn:
  bool RIGHT_TURN;
  if (GetPhi(i) > 0) {   // should this depend on i or on the arcradius?  (i.e. if arcradius < 0 => left turn)
    RIGHT_TURN = true;   // we need to take moving straightward into account!
    cout << "RIGHT TURN!" << endl;
  }
  else {
    RIGHT_TURN = false;
    cout << "LEFT TURN!" << endl;
  }

  //  cout << "i: " << i << ", yaw: " << yaw << ", arc_radius: " << arc_radius << ", LB: " << LB << endl;
  //  cout << "pos.N: " << pos.N << ", pos.E: " << pos.E << ", curWay.N: " << currentWaypoint.N << ", curWay.E: " << currentWaypoint.E << endl;
  //  cout << "nextWay.N: " << nextWaypoint.N << ", nextWay.E: " << nextWaypoint.E << endl;

  // now find the center of the turning circle:
  Vector turnCenter;
  
  if (RIGHT_TURN) {
    turnCenter.N = pos.N + fabs(arc_radius) * cos(yaw + M_PI_2);
    turnCenter.E = pos.E + fabs(arc_radius) * sin(yaw + M_PI_2);
  }
  else {
    turnCenter.N = pos.N + fabs(arc_radius) * cos(yaw - M_PI_2);
    turnCenter.E = pos.E + fabs(arc_radius) * sin(yaw - M_PI_2);
  }

  //  cout << "turnCenter - N: " << turnCenter.N << ", E: " << turnCenter.E << endl; // assume this is correct for now...

  // Next, find the intersection points:
  Vector intersectionL1, intersectionL2, intersectionR1, intersectionR2;

  // these are for the LEFT boundary:
  Vector LLB_current, LLB_next;
  LLB_current.E = -1 * (currentWaypoint.E) * cos(currentWaypoint.E) / LB;
  LLB_current.N = (currentWaypoint.N) * sin(currentWaypoint.N) / LB;
  LLB_next.E = -1 * (nextWaypoint.E) * cos(nextWaypoint.E) / LB;
  LLB_next.N = (nextWaypoint.N) * sin(nextWaypoint.N) / LB;                

  double ldn = LLB_next.N - LLB_current.N;
  double lde = LLB_next.E - LLB_current.E;
  double ldr = sqrt(ldn * ldn + lde * lde);
  double lD = LLB_current.N * LLB_next.E - LLB_next.N * LLB_current.E;

  // Check the determinant to see if there's an intersection on the left side:
  double LLB_determinant = ldr * ldr * arc_radius * arc_radius - lD * lD;

  if (LLB_determinant > 0) {
    find_intersections(fabs(arc_radius), ldn, lde, ldr, lD, 
		       intersectionL1, intersectionL2);    
  }
  // else?

  //  cout << "ldn: " << ldn << ", lde: " << lde << ", ldr: " << ldr << ", lD: " << lD << endl;

  // these are for the RIGHT boundary:
  Vector RLB_current, RLB_next;
  RLB_current.E = (currentWaypoint.E) * cos(currentWaypoint.E) / LB;             
  RLB_current.N = -1 * (currentWaypoint.N) * sin(currentWaypoint.N) / LB;
  RLB_next.E = (nextWaypoint.E) * cos(nextWaypoint.E) / LB;
  RLB_next.N = -1 * (nextWaypoint.N) * sin(nextWaypoint.N) / LB;

  double rdn = RLB_next.N - RLB_current.N;
  double rde = RLB_next.E - RLB_current.E;
  double rdr = sqrt(ldn * ldn + lde * lde);
  double rD = RLB_current.N * RLB_next.E - RLB_next.N * RLB_current.E;

  // Check the determinant to see if there's an intersection on the left side:
  double RLB_determinant = rdr * rdr * arc_radius * arc_radius - rD * rD;

  if (RLB_determinant > 0) {
    find_intersections(fabs(arc_radius), rdn, rde, rdr, rD, 
		       intersectionR1, intersectionR2);    
  }
  // else?

  //  cout << "rdn: " << rdn << ", rde: " << rde << ", rdr: " << rdr << ", rD: " << rD << endl;

  //  cout << "intersectionL1 - N: " << intersectionL1.N << ", E: " << intersectionL1.E << endl;
  //  cout << "intersectionL2 - N: " << intersectionL2.N << ", E: " << intersectionL2.E << endl;
  //  cout << "intersectionR1 - N: " << intersectionR1.N << ", E: " << intersectionR1.E << endl;
  //  cout << "intersectionR2 - N: " << intersectionR2.N << ", E: " << intersectionR2.E << endl;


  // Now compute coords in turn center ref frame
  // We want to translate everthing by T so that turning center is at origin
  // And then rotate around turning center origin by theta so that the vehicle is on the x axis
  // (different cases for left and rightward turns)
  // first, go from (Northing, Easting) to an (x,y) ref frame

  /*  pos_xy = [pos.E; pos.N];                         // pos of vehicle
      turn_center_xy = [turn_center.E; turn_center.N]; // pos of turnCenter
      obstacle_1_xy = [obstacle_1.E; obstacle_1.N];    // pos of intersection 1
      obstacle_2_xy = [obstacle_2.E; obstacle_2.N];    // pos of intersection 2
  */

  // Compute necessary translation
  Vector T;
  T.N = - turnCenter.N;
  T.E = - turnCenter.E;
    
  double theta;

  // Compute necesary rotation
  if (RIGHT_TURN) { theta = -yaw + M_PI; }
  else { theta = -yaw; }

  theta = -theta;

  // R is the rotation matrix...  it's all been converted below
  // R = 1; [ cos(theta) -sin(theta);       
  //        sin(theta)  cos(theta)];
  Vector pos_O, obstacleL1_O, obstacleL2_O, obstacleR1_O, obstacleR2_O;
  // apply transformation
  pos_O.E = cos(theta) * (pos.E + T.E) - sin(theta) * (pos.N + T.N);
  pos_O.N = sin(theta) * (pos.E + T.E) + cos(theta) * (pos.N + T.N);

  obstacleL1_O.E = cos(theta) * (intersectionL1.E + T.E) - sin(theta) * (intersectionL1.N + T.N);
  obstacleL1_O.N = sin(theta) * (intersectionL1.E + T.E) + cos(theta) * (intersectionL1.N + T.N);
  obstacleL2_O.E = cos(theta) * (intersectionL2.E + T.E) - sin(theta) * (intersectionL2.N + T.N);
  obstacleL2_O.N = sin(theta) * (intersectionL2.E + T.E) + cos(theta) * (intersectionL2.N + T.N);

  obstacleR1_O.E = cos(theta) * (intersectionR1.E + T.E) - sin(theta) * (intersectionR1.N + T.N);
  obstacleR1_O.N = sin(theta) * (intersectionR1.E + T.E) + cos(theta) * (intersectionR1.N + T.N);
  obstacleR2_O.E = cos(theta) * (intersectionR2.E + T.E) - sin(theta) * (intersectionR2.N + T.N);
  obstacleR2_O.N = sin(theta) * (intersectionR2.E + T.E) + cos(theta) * (intersectionR2.N + T.N);


  // As sanity check, compute also the yaw_vector of the vehicle in the new ref frame
  // first compute yaw in new ref frame
  // first compute yaw in (x,y) coordinates from (Northing, Easting)
  // this means change the handedness of the angle
  // (+ve counter-clockwise instead of clockwise)
  // and add pi/2 since N direction is the y axis

  double yaw_xy = -yaw + M_PI_2;

  // now apply same rotation as to all the points
  double yaw_O = yaw_xy + theta;


  // compute arc angles (and arc distance) to obstacles, and find first collision
  double obstacle_arc_angle_L1 = atan2(obstacleL1_O.E, obstacleL1_O.N);
  double obstacle_arc_angle_L2 = atan2(obstacleL2_O.E, obstacleL2_O.N);
  double obstacle_arc_angle_R1 = atan2(obstacleR1_O.E, obstacleR1_O.N);
  double obstacle_arc_angle_R2 = atan2(obstacleR2_O.E, obstacleR2_O.N);

  // First, fix angles to be either in [0 2*pi] or [-2*pi 0] range
  if (RIGHT_TURN) {
    // that means make positive angles negative
    if (obstacle_arc_angle_L1 > 0)
      obstacle_arc_angle_L1 = obstacle_arc_angle_L1 - 2*M_PI;
    else if (obstacle_arc_angle_L2 > 0)
      obstacle_arc_angle_L2 = obstacle_arc_angle_L2 - 2*M_PI;
    else if (obstacle_arc_angle_R1 > 0)
      obstacle_arc_angle_R1 = obstacle_arc_angle_R1 - 2*M_PI;
    else if (obstacle_arc_angle_R2 > 0)
      obstacle_arc_angle_R2 = obstacle_arc_angle_R2 - 2*M_PI;
  }
  else {
    // make negative angles positive
    if (obstacle_arc_angle_L1 < 0)
      obstacle_arc_angle_L1 = obstacle_arc_angle_L1 + 2*M_PI;
    else if (obstacle_arc_angle_L2 < 0)
      obstacle_arc_angle_L2 = obstacle_arc_angle_L2 + 2*M_PI;
    else if (obstacle_arc_angle_R1 < 0)
      obstacle_arc_angle_R1 = obstacle_arc_angle_R1 + 2*M_PI;
    else if (obstacle_arc_angle_R2 < 0)
      obstacle_arc_angle_R2 = obstacle_arc_angle_R2 + 2*M_PI;
  }

  float obstacle_dist_L1 = fabs(obstacle_arc_angle_L1) * fabs(arc_radius);
  float obstacle_dist_L2 = fabs(obstacle_arc_angle_L2) * fabs(arc_radius);
  float obstacle_dist_R1 = fabs(obstacle_arc_angle_R1) * fabs(arc_radius);
  float obstacle_dist_R2 = fabs(obstacle_arc_angle_R2) * fabs(arc_radius);

  //  cout << "obstacle dist L - 1: " << obstacle_dist_L1 << ", 2: " << obstacle_dist_L2 << endl;
  //  cout << "obstacle dist R - 1: " << obstacle_dist_R1 << ", 2: " << obstacle_dist_R2 << endl;

  //  cout << "min obstacle dist: " << fmin(fmin(obstacle_dist_L1, obstacle_dist_L2), fmin(obstacle_dist_R1, obstacle_dist_R2)) << endl;
  //  cout << endl;

  return fmin(fmin(obstacle_dist_L1, obstacle_dist_L2), 
	      fmin(obstacle_dist_R1, obstacle_dist_R2));
}

/*///////////////////////////////////////////////////
 * this function calculates the distance from the origin of the vehicle to the
 * LB (which is defined in the RDDF)
 * since the vehicle drives in arcs, we need the arclength, which is equal to
 * the steering_angle * the arc_radius
double distance_to_boundary(Vector intersection, Vector position, double arc_radius) {
  double D = sqrt((intersection.N - position.N) * (intersection.N - position.N) + 
		  (intersection.E - position.E) * (intersection.E - position.E));

  double angle = 2 * asin(D / (2 * arc_radius)); // angle between the vector from the pos of vehicle to center of circle
                                                   // and vector from center of circle to intersection

  return arc_radius * angle;  // arclength from origin of car
}
*////////////////////////////////////////////////////


/* This is the New UpdateVotes... it takes into account corridor following when
 * sending goodness factors
 */
void GlobalPlanner::GlobalUpdateVotes() {
  double steer_cmd; // commanded steering angle (rad)
  double speed_cmd; // commanded speed (m/s)
  int currentWPnum;

  //  pair<double, double> result;
  Vector currentWaypoint, nextWaypoint;
  // Now the state has a heading in degrees
  Vector pos;
  double heading;
  Timeval tv;
  //  double posE, posN, curSpeed=0;
  pos.N = d.SS.Northing;
  pos.E = d.SS.Easting;
  double posYaw = d.SS.Yaw;
  double LB = d.wp.get_current().radius;
  double corridor = 2 * LB;
  currentWaypoint.N = (double) d.wp.get_current().northing;
  currentWaypoint.E = (double) d.wp.get_current().easting;
  nextWaypoint.N = (double) d.wp.peek_next().northing;
  nextWaypoint.E = (double) d.wp.peek_next().easting;
  currentWPnum = d.wp.get_current().num;

  // Are we at the waypoint?
  if (withinWaypoint(pos, currentWaypoint, d.wp.get_current().radius)) {
    cout << "Waypoint Achieved" <<endl;
    d.wp.get_next();   // Jump to the nexts wp
    currentWPnum = d.wp.get_current().num;
    cout << " -----SHOOTING FOR WAYPOINT --- (" 
	 << d.wp.get_current().easting << ", "
	 << d.wp.get_current().northing << ")" << endl;
  }

  // Move from one wp to the next, always aiming for that next wp
  if (d.wp.get_current().num != d.wp.get_size()) {
    currentWaypoint.N = (double) d.wp.get_current().northing;
    currentWaypoint.E = (double) d.wp.get_current().easting;
    nextWaypoint.N = (double) d.wp.peek_next().northing;
    nextWaypoint.E = (double) d.wp.peek_next().easting;

    // HACK! 01/25/04 manual offset in Yaw
    /* d.SS.Yaw = d.SS.Yaw + 20.0*M_PI/180.0; */
    // don't be so sure that we're getting Yaw between [-pi, pi]
    // Normalize the desired heading to be in [-pi,pi]
    //if( d.SS.Yaw > M_PI ) d.SS.Yaw = d.SS.Yaw - 2.0*M_PI;
    //if( d.SS.Yaw < -M_PI ) d.SS.Yaw = d.SS.Yaw + 2.0*M_PI;
    d.SS.Yaw = atan2( sin(d.SS.Yaw), cos(d.SS.Yaw) ); 

    // calculate the desired steering angle and speed    
    cout << "current waypoint num: " << currentWPnum + 1 << endl;
    //    printf("current waypoint num+1 = %d\n", currentWPnum+1 );
    steer_cmd = calc_steer_command(pos, currentWaypoint, M_PI_2 - d.SS.Yaw);  
    printf("yaw = %f, steer_cmd = %f\n", d.SS.Yaw, steer_cmd );
    speed_cmd = calc_speed_command(d.SS.Yaw, pos, currentWaypoint,
         nextWaypoint, d.wp.get_current().radius);  

    // result.first  = calc_speed_command(d.SS.Yaw, pos, currentWaypoint, nextWaypoint, d.wp.get_current().radius);  
    // result.second = calc_steer_command(pos, currentWaypoint, d.SS.Yaw);  
  }
  else  {
    cout << "[GlobalLoop] Waypoint following done" << endl;
    cout << "[GlobalLoop] Calling ForceKernelShutdown()!" << endl;
    
    // Tell the arbiter to stop the demo since we are at the
    // end of the waypoints - this will stop the vehicle
    // NO LONGER NEEDED - logic now handled in arbiter.
    // YOU CAN PERMANENTLY REMOVE THE COMMENTED OUT LINES HERE
    //    Mail m = NewOutMessage( MyAddress(), 
    //    MODULES::Arbiter, ArbiterMessages::Stop);
    //    SendMail(m);

    sleep(1); // wait a second
    
    ForceKernelShutdown();
  }
  

  // to calculate goodness per arc (g/a), we need:
  // - what is the arc radius?
  // - how far away (length of curve) is the intersection from the car?  (so that we know how much room we have to move)
  // - what are the coordinates (x, y, Yaw) of the arc intersecting the lateral boundary (LB)?

  // g/a = some function dependent on coords of arc intersecting LB, steering radius, and curve length to LB


  double goodness = 0;
  
  // Now wayPt should contain all the right arcs to be evaluated.
  // So evaluate them.
  int i = 0;
  //    for (Voter::voteListType::iterator iter = wayPt.begin();
  //		    iter != wayPt.end(); ++iter,++i) {
  double *goodnessVotes = new double[NUMARCS];  
  double *veloVotes = new double[NUMARCS];
  double maxArcLength = 0;
  for(i=0; i<NUMARCS; i++) {
    
    // Calculate goodness by looking at how close to the desired steering
    // the angle is:       
    // this one scales the steering linearly between 0 and MAX_GOODNESS
    // for evaluating, respectively, exactly where we want to steer and 
    // MAX_STEER when we want to steer to -MAX_STEER (or vice versa)
    /*      goodness = max( 0, (int)( round(MAX_GOODNESS 
	    - fabs( (iter->phi)/MAX_STEER - result.second) * MAX_GOODNESS) ) );
    */    
    // we'll have to think smarter about how the arbiter evaluates these
    // votes.  we don't want to give a zero vote just because we want to 
    // turn the other way 
    // let's try to make the same linear scaling, but between some
    // parameter (e.g. 100) and MAX_GOODNESS
    double ggparam = 10.0;

    // arc_steering_angle and arc_radius are borrowed from DFE.cc - should it be used?
    double arc_steering_angle = GetPhi(i);

    double arc_radius = steering_angle_2_arc_radius( arc_steering_angle );
    //    cout << "arc_steering_angle: " << arc_steering_angle << endl;

    double arclength = distance_to_boundary(i, pos, posYaw, arc_radius, LB, 
					    currentWaypoint, nextWaypoint);
    
    //    cout << "arclength: " << arclength << endl;

    if (arclength > maxArcLength) {
      maxArcLength = arclength;
    }

    // goodness is proportional to the amount of space between the car and the boundary:
    
    /*goodness = fmax( ggparam, arclength / MAX_GOODNESS); 
    // this will take either the minimum ggparam (so it's never zero)
    // or the arclength/MAX_GOODNESS so that MG is prop to arclength
    */
    /*    goodness = fmax( ggparam, MAX_GOODNESS - 
	  fabs( GetPhi(i) - result.second)/2.0 * (MAX_GOODNESS - ggparam) ); */ // what does this do?? 
    double velocity = sqrt(2 * MAX_DECEL * arclength);
    //    goodness = arclength * MAX_GOODNESS;
    //    cout << "maxArcLength: " << maxArcLength << ", MAX_GOODNESS: " << MAX_GOODNESS << endl;

    cout << "arclength: " << arclength << ", velo: " << velocity << endl;

    //    d.global.Votes[i].Goodness = goodness;  //goodnessVotes[i] / maxArcLength * MAX_GOODNESS;
    //    d.global.Votes[i].Velo = velocity; //veloVotes[i];


    // assign a temporary goodness and velocity (velocity is correct, 
    // but it should be sent at the same time as the goodness votes)
    goodnessVotes[i] = arclength;
    veloVotes[i] = sqrt(2 * MAX_DECEL * arclength); 
    // the temporary goodness is necessary so we can scale the goodnesses before they're sent to the arbiter

  }

  // maxArcLength = 10;
  cout << " MaxArcLength: " << maxArcLength << endl;

  for (i = 0; i < NUMARCS; i++) {
    // assign the actual vote goodness (scaled based on the maxArcLength calc'd in the above for loop)
    d.global.Votes[i].Goodness = goodnessVotes[i] / maxArcLength * MAX_GOODNESS;
    // velocity sent to arbiter based on distance to corridor and maximum deceleration
    d.global.Votes[i].Velo = veloVotes[i];
    } 
    
  //usleep(10000); // this needs to be here so global doesn't hog the processor
  // Sleeping is done in the GlobalPlanner file and is not needed here


} // end GlobalUpdate
