#!/bin/csh
# 
# vmanage.sh - startup script for vmanage
# RMM, 27 Feb 04
#
# This script starts up vmanage on magnesium and restarts it if it exits
# unexpectedly.  This contains an absolute path to vmanage so that it
# can be run from /etc/rc.d.
#
# The script tries to be smart about whether it is running on a terminal
# or not and doesn't request input unless it is running on a terminal
#
# Usage:
#   VManage flags [&]
#
# Environment variables:
#   DGC		path to DGC executables
#   FLAGS	optional flags to pass to StereoPlanner-1c
#

# Set up process limits to allow core files to be generated
limit coredumpsize unlimited

# Make sure we have a path to the file
if (! $?DGC) set DGC=`pwd`
if (! $?FLAGS) set FLAGS=""
if (! -e $DGC/StereoPlanner-2.0c) then
  echo "FATAL: can't find StereoPlanner-2.0c in $DGC"
  exit
endif

# Print out a banner so that we know where we are running
echo "StereoPlanner-2.0c@`hostname`: pwd = `pwd`; FLAGS = $FLAGS";

# Check to make sure we are on the right machine
#if (`hostname` != "magnesium") then
#  echo "WARNING: not running on magnesium"
#  if ($tty != "") then
#      echo -n "Continue (y/n)? ";
#      if ($< != "y") exit
#  endif
#endif


# Run StereoPlanner-1c and restart it whenever it crashes
while (1) 
  # kill anything that is left over
  killall -9 StereoPlanner-2.0c >& /dev/null

  echo "======== starting StereoPlanner-2.0c $FLAGS $argv ========"
  setenv LD_LIBRARY_PATH "/home/team/team/lib/"
  echo $LD_LIBRARY_PATH
  $DGC/StereoPlanner-2.0c $FLAGS $argv
  echo "StereoPlanner-2.0c aborted"

  sleep 2	# sleep for a few seconds to let things die
end
