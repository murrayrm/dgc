#include <sys/types.h>
#include <sys/socket.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>		/* for sockaddr_in */
#include <netdb.h>		/* for gethostbyname() */
#include <unistd.h>		/* for read(), write() */
#include <assert.h>
#include <pthread.h>
#include <sys/time.h>		/* unix: for gettimeofday() */

#include "crc32.h"
#include "imu_net.h"

#ifndef WIN32
#define SOCKET int
#define closesocket close
#endif


static int
lookup_addr (struct sockaddr *addr,
	     const char *host,
	     unsigned port)
{
  struct hostent *hent;
  struct sockaddr_in addr_in;
  hent = gethostbyname (host);
  if (hent == NULL)
    {
      fprintf (stderr, "WARNING: error resolving hostname '%s'\n", host);
      return 0;
    }
  memset (&addr_in, 0, sizeof (addr_in));
  addr_in.sin_family = AF_INET;
  addr_in.sin_port = htons (port);
  memcpy (&addr_in.sin_addr, hent->h_addr_list[0], 4);
  memcpy (addr, &addr_in, sizeof (struct sockaddr_in));
  return 1;
}

int main (int argc, char **argv)
{
  struct sockaddr addr;
  SOCKET sock;
  if (argc != 3)
    {
      fprintf (stderr, "usage: %s ARBITER_HOSTNAME ARBITER_PORT\n"
	       "\nRun the GPS stub against the given arbiter.\n",
	       argv[0]);
      return 1;
    }
  if (!lookup_addr (&addr, argv[1], atoi (argv[2])))
    {
      return 1;
    }

  sock = socket(AF_INET, SOCK_DGRAM, 0);

  while (1)
    {
      IMUReading reading;
      char buf[128];
      unsigned len;
      sleep(1);
      reading.dvx = rand();
      reading.dvy = rand();
      reading.dvz = rand();
      reading.dtx = rand();
      reading.dty = rand();
      reading.dtz = rand();
      memcpy (buf + 4, &reading, sizeof (reading));
      len = sizeof (reading);
      crc32_big_endian (buf + 4, len, (unsigned char*)buf);
      sendto(sock, buf, len + 4, 0, &addr, sizeof(addr));
    }
}
