/***************************************************************************************/
//FILE: 	cruise.h
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file has the data that the cruise control generates
//
/***************************************************************************************/

#ifndef CRUISE_H
#define CRUISE_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <fstream>


// Data from the model
// err     : actual error
// derr    : actual derivative error
// refVel  : velocity reference to be followed
// actuator: command to be sent to bob
struct cruiseData {
  float err;
  float derr;
  float refVel;
  float actuator;
};

// Class to access the data
class cruiseDataWrapper {

  public:
  // Constructors and Destructors
  cruiseDataWrapper();
  ~cruiseDataWrapper();

  // Prints all the data in the object to the screen
  void print_to_screen();

  // Appends all the data in the object to filename
  void write_to_file(char *filename);

  // Get the values
  void get_data(cruiseData &buffer);

  // Updates the values
  void update_data(cruiseData buffer);

  // Data
  cruiseData data;
};
#endif
