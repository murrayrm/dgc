#include <iostream>
#include <string>
#include "steerCar/steerCar.h"
#include "steerCar/steerState.hh"
#include "joystick/sw.h"
#include "moveCar/accel.h"
#include "moveCar/brake.h"
#include "moveCar/TLL.h"
//#include "braketesting/accel.h"
//#include "braketesting/brake.h"
//#include "braketesting/TLL.h"

using namespace std;

int negCountLimit = -60000;
int posCountLimit = 60000;
int STEER_BY_HAND = TRUE;       // Assume that we steer by hand unless told so.
//int STEER_BY_HAND = FALSE;    // Assume that we steer by hand unless told so.
int verbosity_exec_cmd = TRUE;          // steer_exec_cmd uses verbose output.
int steer_errno = ERR_NONE;             // We start out with no errors.
static float brakeSpeed = .3;     // relative speed/acceleration of brake

///////////////////////////THIS IS A HACK/////////////////
string lastSteeringCommanded = (string) "Hello";
//////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
  char inputChar;         // Character to use for input.
  double steerAngle;      // Angle that we will steer at.
  char in = 'y';
  struct steer_struct st;
  bool steering = false;
  bool braking = false;
  bool acceling = false;

  if (argc <= 1 || argc > 5) {
    cout << "Usage: ./" << argv[0] << " [flags]" << endl;
    cout << "    -ni \t no init" << endl;
    cout << "    -st \t steering" << endl;
    cout << "    -gs \t gas" << endl;
    cout << "    -br \t braking" << endl;
    cout << "\"./" << argv[0] << " -st -gs -br\" means all actuators active" << endl;
    cout << "Please put it in that order and make me happy." << endl;
    return 0;
  }

  cout << "Initializing joystick" << endl;
  // This code is *very* blocking.  Johnson wrote it and I don't know why.
  // Initialize "Joystick" control unit
  //  if(init_steering(PP_STEER)!=0)
  //    cout<< "Can't open port for steering wheel (joystick)!!!"<< endl;
  if(init_steering(0)!=0)
    cout<< "Can't open port for steering wheel (joystick)!!!"<< endl;

  cout << "joystick initialized" << endl;

  if (strcmp(argv[1], "-ni")) {

  cout << "starting initialization of actuators" << endl;
  for (int i = 1; i < argc; i++) {
    if (strcmp(argv[i], "-st") == 0) {
      cout << "--starting steering init" << endl;
      steering = true;
      while (init_steer() != TRUE) 
	printf("waiting on steering intialization\n");
      // This is a hack so that everything is actually initialized for the trip.
      cout << "--steering initialized" << endl;
    }
    if (strcmp(argv[i], "-gs") == 0 /*|| strcmp(argv[2], "-gs") == 0*/) {
      cout << "--starting gas init" << endl;
      if (braking == true) {
        cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
        cout << "you didn't put the flags in the right order, did you" << endl;
        cout << "quitting..." << endl;
        return -1;
      }
      acceling = true;
      init_accel();      // includes initBrakesensors( )
      cout << "--gas actuator initialized" << endl;
    }
    if (strcmp(argv[i], "-br") == 0 /*|| strcmp(argv[2], "-br") == 0 || 
				      strcmp(argv[3], "-br") == 0*/) {
      cout << "--starting brake init" << endl;
      braking = true;
      if (acceling == false)
	initBrakeSensors();
      init_brake();      // currently needs to be after init_accel() or initBrakeSensors()
      cout << "--brake actuator initialized" << endl;
      /*      while (in != 'n') {
	cout<<"Do you want to calibrate the brake actuator? (y or n) "<<endl;
	cin >> in;
	cout << endl;
	if(in=='y' || in=='Y')
	  calib_brake();
	  } */
    }
  }

  }

  cout << "-----done initialization" << endl;

  float blah = 0;
  timeval before, after;
  // Driving!
  while(1) {
    get_steering(&st);
      
    gettimeofday(&before, NULL);
    gettimeofday(&after, NULL);
    if (steering == true)
        if (blah == 0) {
            blah = 0.5;
        }else if (blah > 0) {
            blah = -0.5;
        } else {
            blah = 0;
        }
    /*    while (after.tv_sec - before.tv_sec < 4) {
        steer_heading(blah);
        steer_state_update(TRUE);
        usleep(20000);
        gettimeofday(&after, NULL);
	} */

      steer_heading(st.x);
      steer_state_update(TRUE, TRUE);
    
    if (braking == true) {
      if (acceling == true) {
	if ( -1.0 <= st.y && st.y <= 0.0 ) {
	  // should put accel to free if braking
	  set_accel_abs_pos( 0.0 );  // all the way out
	  set_brake_abs_pos_pot( st.y *-1.0, brakeSpeed, brakeSpeed);
	  cout << "Setting gas to 0.0, brake to " << st.y * -1.0 << endl;
	}
	else if ( 0.0 <= st.y && st.y <= 1.0 ) {
	  // should put brake to free if accelerating
	  set_brake_abs_pos_pot( 0.0, brakeSpeed, brakeSpeed);
	  set_accel_abs_pos( st.y );
	  cout << "Setting gas to " << st.y << endl;
	}
	else
	  cout << "Bad Accel Data" << endl;
      } // end of if (acceling == TRUE)
      else {  // no acceling
	if ( -1.0 <= st.y && st.y <= 0.0 ) {
	  set_brake_abs_pos_pot( st.y *-1.0, brakeSpeed, brakeSpeed);
	  cout << "Setting brake to " << st.y * -1.0 << endl;
	}
	else if ( 0.0 < st.y && st.y <= 1.0 ) {
	  set_brake_abs_pos_pot( 0.0, brakeSpeed, brakeSpeed);
	  cout << "Setting brake to zero" << endl;
	}
	else
	  cout << "Bad Accel Data" << endl;
      } // end of else
    } // end of if (braking == TRUE)
    else if (acceling == true) { // no braking
      if ( -1.0 <= st.y && st.y <= 0.0 ) {
	set_accel_abs_pos( 0.0 );  // all the way out
	cout << "Braking, Gas to zero" << endl;
      }
      else if ( 0.0 < st.y && st.y <= 1.0 ) {
	set_accel_abs_pos( st.y );
	cout << "Setting gas to " << st.y << endl;
      }
      else
	cout << "Bad Accel Data" << endl;
    } // end of else if (acceling == TRUE)

    // must find a good delay
    //usleep(13000);  // ~60Hz
    //usleep(26000);   // ~30Hz
    //cout << "brake position: " << getBrakePot() << endl;
  } // end of while(1)

  return 0;
}

