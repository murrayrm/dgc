// header file for eatop input

#define ESTOP_INPUT_LENGTH 1

#define RUN_CHAR 'N'
#define PAUSE_CHAR 'P'
#define DISABLE_CHAR 'D'

#define RUN 2
#define PAUSE 1
#define DISABLE 0



int estop_open(int);
void simulator_estop_open(int);
int estop_close(void);
int estop_status(void);
