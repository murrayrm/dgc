#!/bin/sh

# Definition of packages to install:
# NOTE: If you add a new line, make sure to comment it and add it to the PACKAGES variable!
COMPILE=
DGC_LIBS="boost"
EDITORS="vim gvim qemacs xemacs emacs"
DEVELOPMENT="$COMPILE $DGC_LIBS $EDITORS"

MATH="gnuplot f2c octave blas-atlas"
SYSADMIN="subversion yp-tools ypbind autoconf strace doxygen"
BASIC_UTILS="wget zip unzip less bison xterm screen"
IMAGE_UTILS="imagemagick xv gimp" # availability of mplayer?
MORE_UTILS="ifplugd"
ACROREAD="acroread"
BASIC="$MATH $SYSADMIN $BASIC_UTILS $IMAGE_UTILS $MORE_UTILS $ACROREAD"

PYTHON="pyopengl python swig wxgtk pygtk pygtkglext"
PROJ="proj"
LIB3DS="lib3ds"
XML="libxml2"

GTK2=""
GTKMM24="gtkmm"
GTKGLEXT="gtkglext"
GTKGLEXTMM="gtkglextmm" 
MAPDISPLAY="$GTK2 $GTKMM24 $GTKGLEXT $GTKGLEXTMM"

#OPENCV="libopencv0.9-5 libopencv-dev libopencv-doc libhighgui-dev libcvaux-dev"
CAM="libraw1394"
CANVAS="libgnomecanvas"
GSL="gsl"
PLAYER="$OPENCV $CAM $CANVAS $GSL"

PACKAGES="$DEVELOPMENT $BASIC $MAPDISPLAY"

if [ "$UID" != "0" ]; then
        echo "You must be root to run this script!"
        exit 0
fi

# update the portage cache
echo
echo Running emerge sync...
emerge sync

# this is the standard set of packages to install for a development laptop
echo
echo Installing Team Caltech packages: $PACKAGES
echo
echo Are the use flags right ?
echo
emerge -vp $PACKAGES

exit 0

echo
echo Team Caltech installation script finished!
echo "(Don't forget to check for errors)."
echo
exit 0

echo Displaying system updates
emerge -uvp system
echo 
echo Displaying world updates
emerge -uvp world
echo 

echo Please update system appropriately
echo
#echo If you got any error about 404 file not found, you may need to
#echo remove toughguy.caltech.edu from sources.list and run this script again.

exit 0

