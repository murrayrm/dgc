#!/bin/bash

# This script creates a ssh keypair and distrubutes the public key to the 
# hosts given as arguments.
#
# Created: Oct 20, 2004
# Author: Henrik Kjellander

if [[ ! "$1" || ! "$2" ]]; then
  echo Usage: $0 username \"hosts separated by spaces\"
  echo Example: $0 team \"titanium osmium\"
  exit 0
fi

USER=$1
HOSTS=$2
SSH_CMD="mkdir -p .ssh; cat id_rsa.pub >> .ssh/authorized_keys; rm id_rsa.pub"

cd ~/.ssh
if [[ -f id_rsa && -f id_rsa.pub ]]; then
  echo Existing files found in ~/.ssh already, using them insead of creating new
else
  echo About to create keypair, please select empty passphrase \(press enter\)
  ssh-keygen -t rsa -f id_rsa
  echo Done creating keypair
fi

echo 

for host in $HOSTS
do
  echo Copying the public key to: $host
  if(scp id_rsa.pub $USER@$host:.); then
    echo Successfully copied key, appending key to .ssh/authorized_keys
    xterm -sl 9999 -e "ssh $USER@$host '$SSH_CMD'; \
    echo; echo; echo -n $comp: Finished. Press enter to exit; \
    read foo" &
    echo Done appending key for host $host
  else
    echo Error copying key to $host
  fi    
done

echo Done!
exit 0

