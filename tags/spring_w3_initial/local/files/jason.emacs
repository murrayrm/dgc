(fset 'goto-other-buffer "\C-xb\C-m")
(global-set-key [(control b)] 'goto-other-buffer)
(global-set-key [backtab] 'dabbrev-expand)
