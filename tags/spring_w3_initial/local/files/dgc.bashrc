# This is a sample .bashrc that you should add to your 
# .bashrc in order to get the code repository to compile 
# and run (add this line to the top of your ~/.bashrc 
# file, assuming that you have checked out your code to
# $HOME/dgc/): 
# source $HOME/dgc/local/files/dgc.bashrc

# custom prompt
# without highlighting
PS1='\n\w\n\u@\H\$ '
# with highlighting
#PS1='\n\w\n\e[43m\u@\H\e[0;49m$ '

# display ls indicators and color
alias ls='ls -F --color=tty'

# enable X forwarding by default
alias ssh='ssh -X'

# print out the skynet key
alias key='echo $SKYNET_KEY'

# print the skynetd processes
alias psd='ps aux | grep skynetd'

# alias to tell what object files, local changes and non-Subversion files
# (including logs) are in the current checkout.  It's a good way to check 
# whether or not you can be confident that your repository copy is "fresh"
alias ssn='svn stat --no-ignore'

# set an editor for svn check-in logs
export SVN_EDITOR=vim

# Set the alias for your dgc root directory
export DGC=$HOME/dgc

# Must have these to be able compiling the svn tree
export PATH=$PATH:$DGC/bin:$DGC/bob/bin

# The rest below is optional but recommended:
export CPATH=$CPATH:/usr/local/include

# Set up some useful paths for scripts and binaries
export PATH=$PATH:/sbin:/usr/sbin:/usr/local/bin:$DGC/local/scripts

# MTA home, for the ip and grid files
export MTAHOME=$HOME/local

# Matlab dir on grandchallenge and race computers 
# (uncomment if this .bashrc is is on those computers)
export MATLAB_ROOT=/usr/local/matlab6p5
#export MATLAB_ROOT=/opt/Matlab14

# For MatlabDisplay to run properly
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MATLAB_ROOT/extern/lib/glnx86

# For GTK Map Display to run
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

# Player/Gazebo specific
# in order to find all installed packages:
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/lib/pkgconfig:/usr/local/lib/pkgconfig
# in order to compile and run Python and wxPython
export PYTHONPATH=$PYTHONPATH:/usr/lib/python2.3/site-packages:/usr/local/lib/python2.3/site-packages

