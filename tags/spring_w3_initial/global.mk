# DGC Globaly Included Makefile
# Include this at the top of every makefile after setting DGC
.PHONY: redirect folders all
redirect: folders all

# Directory constants
BINDIR = $(DGC)/bin
LIBDIR = $(DGC)/lib
LOGSDIR = $(DGC)/logs
CONFIGDIR = $(BINDIR)/config
INCLUDEDIR = $(DGC)/include
CONSTANTSDIR = $(DGC)/constants

# Obsolete library locations
MTALIB              = $(LIBDIR)/mta2_1.a 
GPILIB              = $(LIBDIR)/gnuplot_i-camhy.a
NEWMATLIB           = $(LIBDIR)/libnewmat.a
VBLIB               = $(LIBDIR)/vblib.a
ARB2LIB             = $(LIBDIR)/arbiter2.a

# Module library locations

# Module executables list

# Util library locations
DGCUTILS            = $(LIBDIR)/DGCutils.o
SPARROWLIB          = $(LIBDIR)/sparrow.a
GGISLIB             = $(LIBDIR)/ggis.a
CMAPLIB             = $(LIBDIR)/CMap.a
CMAPPLIB            = $(LIBDIR)/CMapPlus.a
RDDFLIB             = $(LIBDIR)/rddf.a
CCPAINTLIB          = $(LIBDIR)/CCorridorPainter.a
CCOSTPAINTLIB       = $(LIBDIR)/CCostPainter.a
FRAMESLIB           = $(LIBDIR)/frames.a
ALICELIB            = $(LIBDIR)/alice.a
SKYNETLIB           = $(LIBDIR)/libskynet.a
TRAJLIB             = $(LIBDIR)/traj.a
SERIALLIB           = $(LIBDIR)/serial.a
SDSLIB              = $(LIBDIR)/SDS.a
MODULEHELPERSLIB    = $(LIBDIR)/libmodulehelpers.a 
RDDFPATHGENLIB      = $(LIBDIR)/rddfPathGen.a
PIDCONTROLLERLIB    = $(LIBDIR)/PID_Controller.a
CPIDLIB             = $(LIBDIR)/libpid.a
TRAJFOLLOWERLIB     = $(LIBDIR)/trajFollower.a
FINDFORCELIB        = $(LIBDIR)/findforce.a
FUSIONMAPPERLIB     = $(LIBDIR)/fusionMapper.a
MAPDISPLAYLIB       = $(LIBDIR)/MapDisplay.a
SKYNETGUILIB        = $(LIBDIR)/skynetgui.a
MODULESTARTERLIB    = $(LIBDIR)/moduleStarter.a
FRAMESLIB           = $(LIBDIR)/frames.a
SIMULATORLIB        = $(LIBDIR)/simulator.a
MODEMANMODULELIB    = $(LIBDIR)/modeManModule.a
PLANNERLIB          = $(LIBDIR)/planner.a
PLANNERMODULELIB    = $(LIBDIR)/plannerModule.a
ADRIVELIB           = $(LIBDIR)/adrive.a
IMU_FASTCOMLIB      = $(LIBDIR)/imu_fastcom.a
ASTATELIB           = $(LIBDIR)/astate.a
CONSTANTSLIB        = $(LIBDIR)/constants
FLWINLIB            = $(LIBDIR)/flwin.a
STEREOFEEDERBIN     = $(BINDIR)/StereoFeeder
STEREOSOURCELIB     = $(LIBDIR)/stereoSource.a
STEREOPROCESSLIB    = $(LIBDIR)/stereoProcess.a
SICKLMS221LIB       = $(LIBDIR)/ladarlib.a
LADARFEEDERBIN      = $(BINDIR)/LadarFeeder
REACTIVEPLANNERLIB  = $(LIBDIR)/reactivePlanner.a
LOGGERLIB           = $(LIBDIR)/logger.a

ADRIVECONFIGDIR = $(CONFIGDIR)/adrive

# Module specifc include files
# Must have MODULENAME_PATH and MODULENAME_DEPEND defined within
include $(DGC)/drivers/alice/alice.mk
include $(DGC)/util/skynet/skynet.mk
include $(DGC)/util/sparrow/sparrow.mk
include $(DGC)/util/RDDF/Rddf.mk
include $(DGC)/util/traj/traj.mk
include $(DGC)/modules/adrive/adrive.mk
include $(DGC)/drivers/serial/serial.mk
include $(DGC)/drivers/SDS/SDS.mk
include $(DGC)/drivers/gpsSDS/gps.mk
include $(DGC)/drivers/imu_fastcom/imu_fastcom.mk
include $(DGC)/util/moduleHelpers/ModuleHelpers.mk
include $(DGC)/util/moduleHelpers/DGCutils.mk
include $(DGC)/modules/fusionMapper/FusionMapper.mk
include $(DGC)/util/corridorPainter/CCorridorPainter.mk
include $(DGC)/util/costPainter/CCostPainter.mk
include $(DGC)/modules/rddfPathGen/rddfPathGen.mk
include $(DGC)/modules/simulator/simulator.mk # This needs to come after adrive.
include $(DGC)/util/cMap/CMap.mk
include $(DGC)/util/pidController/PID_Controller.mk
include $(DGC)/util/pid/Cpid.mk
include $(DGC)/util/controllerUtils/force/find_force.mk
include $(DGC)/modules/trajFollower/trajFollower.mk
include $(DGC)/util/mapDisplay/MapDisplay.mk
include $(DGC)/modules/skynetGui/skynetGui.mk
include $(DGC)/modules/moduleStarter/moduleStarter.mk
include $(DGC)/util/frames/frames.mk
include $(DGC)/modules/modeManModule/modeManModule.mk
include $(DGC)/util/planner/planner.mk
include $(DGC)/modules/plannerModule/plannerModule.mk #Must come after planner
include $(DGC)/modules/astate/astate.mk
include $(DGC)/constants/constants.mk
include $(DGC)/modules/ladarFeeder/ladarFeeder.mk
include $(DGC)/util/flwin/flwin.mk
include $(DGC)/util/latlong/latlong.mk
include $(DGC)/modules/stereoFeeder/stereoFeeder.mk
include $(DGC)/drivers/stereovision/stereoSource.mk
include $(DGC)/util/stereoProcess/stereoProcess.mk
include $(DGC)/drivers/sicklms221/sicklms221.mk
include $(DGC)/modules/ladarFeeder/ladarFeeder.mk
include $(DGC)/projects/reactivePlanner/reactivePlanner.mk
include $(DGC)/projects/logger/Logger.mk

# Tool definitions
CC = gcc
CPP=g++
INCLUDE=-I$(INCLUDEDIR)
CFLAGS += -g -Wall $(INCLUDE)
CPPFLAGS += -pthread $(CFLAGS)
LDFLAGS += -g -pthread -L$(LIBDIR)
INSTALL = install
INSTALL_DATA = $(INSTALL) -m 644
INSTALL_PROGRAM = $(INSTALL) -m 754
MAKEDEPEND = makedepend -Y
CDD = $(BINDIR)/cdd

# Rule to make the target folders and copy constants before any rules execute
ifneq (1,${MAKINGFIRST})
MAKEFIRST := $(shell export MAKINGFIRST=1 && $(MAKE) first)
endif

# DGC Global Makefile Rules
first: folders $(CONSTANTSLIB)

folders: $(INCLUDEDIR) $(LIBDIR) $(BINDIR) $(LOGSDIR) $(CONFIGDIR) $(ADRIVECONFIGDIR)

$(CONSTANTSLIB): $(CONSTANTS_DEPEND)
	cd $(CONSTANTS_PATH) && $(MAKE) all && $(MAKE) install

$(LADARFEEDERBIN): $(LADARFEEDER_DEPEND)
	cd $(LADARFEEDER_PATH) && $(MAKE) && $(MAKE) install

$(SICKLMS221LIB): $(SICKLMS221_DEPEND)
	cd $(SICKLMS221_PATH) && $(MAKE) && $(MAKE) install

$(STEREOPROCESSLIB): $(STEREOPROCESS_DEPEND)
	cd $(STEREOPROCESS_PATH) && $(MAKE) && $(MAKE) install

$(STEREOSOURCELIB): $(STEREOSOURCE_DEPEND)
	cd $(STEREOSOURCE_PATH) && $(MAKE) && $(MAKE) install

$(STEREOFEEDERBIN): $(STEREOFEEDER_DEPEND)
	cd $(STEREOFEEDER_PATH) && $(MAKE) && $(MAKE) install

$(GGISLIB): $(LATLONG_DEPEND)
	cd $(LATLONG_PATH) && $(MAKE) && $(MAKE) install

$(FLWINLIB): $(FLWIN_DEPEND)
	cd $(FLWIN_PATH) && $(MAKE) && $(MAKE) install

$(ADRIVELIB): $(ADRIVE_DEPEND)
	cd $(ADRIVE_PATH) && $(MAKE) && $(MAKE) install

$(ADRIVECONFIGDIR):
	cd $(CONFIGDIR) && mkdir adrive

$(ASTATELIB): $(ASTATE_DEPEND)
	cd $(ASTATE_PATH) && $(MAKE) && $(MAKE) install

$(IMU_FASTCOMLIB): $(IMU_FASTCOM_DEPEND)
	cd $(IMU_FASTCOM_PATH) && $(MAKE) && $(MAKE) install

$(PLANNERMODULELIB): $(PLANNERMODULE_DEPEND)
	cd $(PLANNERMODULE_PATH) && $(MAKE) && $(MAKE) install

$(PLANNERLIB): $(PLANNER_DEPEND)
	cd $(PLANNER_PATH) && $(MAKE) && $(MAKE) install

$(MODEMANMODULELIB): $(MODEMANMODULE_DEPEND)
	cd $(MODEMANMODULE_PATH) && $(MAKE) && $(MAKE) install

$(SIMULATORLIB): $(SIMULATOR_DEPEND)
	cd $(SIMULATOR_PATH) && $(MAKE) && $(MAKE) install

$(FRAMESLIB): $(FRAMES_DEPEND)
	cd $(FRAMES_PATH) && $(MAKE) && $(MAKE) install

$(SKYNETGUILIB): $(SKYNETGUI_DEPEND)
	cd $(SKYNETGUI_PATH) && $(MAKE) && $(MAKE) install

$(MODULESTARTERLIB): $(MODULESTARTER_DEPEND)
	cd $(MODULESTARTER_PATH) && $(MAKE) && $(MAKE) install

$(MAPDISPLAYLIB): $(MAPDISPLAY_DEPEND)
	cd $(MAPDISPLAY_PATH) && $(MAKE) && $(MAKE) install

$(FUSIONMAPPERLIB): $(FUSIONMAPPER_DEPEND)
	cd $(FUSIONMAPPER_PATH) && $(MAKE) && $(MAKE) install

$(TRAJFOLLOWERLIB): $(TRAJFOLLOWER_DEPEND)
	cd $(TRAJFOLLOWER_PATH) && $(MAKE) && $(MAKE) install

$(CPIDLIB): $(CPID_DEPEND)
	cd $(CPID_PATH) && $(MAKE) && $(MAKE) install

$(PIDCONTROLLERLIB): $(PIDCONTROLLER_DEPEND)
	cd $(PIDCONTROLLER_PATH) && $(MAKE) && $(MAKE) install

$(FINDFORCELIB): $(FINDFORCE_DEPEND)
	cd $(FINDFORCE_PATH) && $(MAKE) && $(MAKE) install

$(CMAPLIB): $(CMAP_DEPEND)
	cd $(CMAP_PATH) && $(MAKE) && $(MAKE) install

$(CMAPPLIB): $(CMAP_DEPEND)
	cd $(CMAP_PATH) && $(MAKE) && $(MAKE) install

$(RDDFLIB): $(RDDF_DEPEND)
	cd $(RDDF_PATH) && $(MAKE) && $(MAKE) install

$(CCPAINTLIB): $(CCORRIDORPAINTER_DEPEND)
	cd $(CCORRIDORPAINTER_PATH) && $(MAKE) && $(MAKE) install

$(CCOSTPAINTLIB): $(CCOSTPAINTER_DEPEND)
	cd $(CCOSTPAINTER_PATH) && $(MAKE) && $(MAKE) install

$(SERIALLIB): $(SERIAL_DEPEND)
	cd $(SERIAL_PATH) && $(MAKE) && $(MAKE) install

$(SDSLIB): $(SDS_DEPEND)
	cd $(SDS_PATH) && $(MAKE) && $(MAKE) install

$(MODULEHELPERSLIB): $(MODULEHELPERS_DEPEND) $(DGCUTILS_DEPEND)
	cd $(MODULEHELPERS_PATH) && $(MAKE) && $(MAKE) install

$(DGCUTILS): $(DGCUTILS_DEPEND) $(MODULEHELPERS_DEPEND)
	cd $(DGCUTILS_PATH) && $(MAKE) install

$(SKYNETLIB): $(SKYNET_DEPEND)
	cd $(SKYNET_PATH) && $(MAKE) install

$(TRAJLIB): $(TRAJ_DEPEND)
	cd  $(TRAJ_PATH) && $(MAKE) && $(MAKE) install

$(RDDFPATHGENLIB): $(RDDFPATHGEN_DEPEND)
	cd $(RDDFPATHGEN_PATH) && $(MAKE) && $(MAKE) install

$(SPARROWLIB): $(SPARROW_DEPEND)
	cd $(SPARROW_PATH) && $(MAKE) && $(MAKE) install

$(LOGGERLIB): $(LOGGER_DEPEND)
	cd $(LOGGER_PATH) && $(MAKE) install

$(CDD): $(SPARROWLIB)

$(INCLUDEDIR):
	cd $(DGC) && mkdir include

$(LIBDIR):
	cd $(DGC) && mkdir lib

$(BINDIR):
	cd $(DGC) && mkdir bin

$(LOGSDIR):
	cd $(DGC) && mkdir logs

$(CONFIGDIR):
	cd $(BINDIR) && mkdir config

$(ALICELIB): $(ALICE_DEPEND)
	cd $(ALICE_PATH) && $(MAKE) && $(MAKE) install

$(REACTIVEPLANNERLIB): $(REACTIVEPLANNER_DEPEND)
	cd $(REACTIVEPLANNER_PATH) && $(MAKE) && $(MAKE) install
