#include "CCorridorPainter.hh"

CCorridorPainter::CCorridorPainter() {
  _inputMap = NULL;
  _inputRDDF = NULL;

  _prevBottomLeftRowMultiple = 0;
  _prevBottomLeftColMultiple = 0;

  _behindWaypointNum = 0;
  _aheadWaypointNum = 0;

  _useDelta = 0;
}


CCorridorPainter::~CCorridorPainter() {

}


int CCorridorPainter::initPainter(CMap* inputMap, 
				  int layerNum, 
				  RDDF* inputRDDF, 
				  double insideCorridorVal, double outsideCorridorVal,
				  double tracklineVal, double edgeVal, double percentSwitch, int useDelta) {
  _inputMap = inputMap;
  _layerNum = layerNum;
  _inputRDDF = inputRDDF;

  _insideCorridorVal = insideCorridorVal;
  _outsideCorridorVal = outsideCorridorVal;
  _tracklineVal = tracklineVal;
  _edgeVal = edgeVal;
  _percentSwitch = percentSwitch;


  _prevBottomLeftRowMultiple = _inputMap->getWindowBottomLeftUTMNorthingRowResMultiple();
  _prevBottomLeftColMultiple = _inputMap->getWindowBottomLeftUTMEastingColResMultiple();
  
  _behindWaypointNum = 0;
  _aheadWaypointNum = 0; 
  
  _useDelta = useDelta;

  return 0;
}

#ifdef CP_OBS
int CCorridorPainter::paintChanges() {
  int row, col;
  int currentWaypoint;
  double waypointBottomLeftNorthing, waypointBottomLeftEasting;
  double waypointTopRightNorthing, waypointTopRightEasting;
  double Northing, Easting;

  int newBottomLeftRowMultiple = _inputMap->getWindowBottomLeftUTMNorthingRowResMultiple();
  int newBottomLeftColMultiple = _inputMap->getWindowBottomLeftUTMEastingColResMultiple();
  int deltaRows, deltaCols;

  //Box 0 is row box
  //Box 1 is col box
  //Box 2 is combo box
  int boxBottomLeftRow[3];
  int boxBottomLeftCol[3];
  int boxTopRightRow[3];
  int boxTopRightCol[3];

  double boxBottomLeftUTMNorthing[3];
  double boxBottomLeftUTMEasting[3];
  double boxTopRightUTMNorthing[3];
  double boxTopRightUTMEasting[3];

  int prevWaypointNum[3];
  int nextWaypointNum[3];

  int corridorBoxBottomLeftRow;
  int corridorBoxBottomLeftCol;
  int corridorBoxTopRightRow;
  int corridorBoxTopRightCol;

  //get the seed
  currentWaypoint = _inputRDDF->getCurrentWaypointNumber(_inputMap->getVehLocUTMNorthing(), _inputMap->getVehLocUTMEasting());
  //printf("Seed was %d\n", currentWaypoint);

  //get the corners
  deltaRows = newBottomLeftRowMultiple - _prevBottomLeftRowMultiple;
  deltaCols = newBottomLeftColMultiple - _prevBottomLeftColMultiple;
  
  if(abs(deltaRows) >= _inputMap->getNumRows() ||
     abs(deltaCols) >= _inputMap->getNumCols()) {
    //printf("Redrawing rddf\n");
    //printf("%s [%d]: \n", __FILE__, __LINE__);
    _inputMap->Win2UTM(0, 0,
		       &(boxBottomLeftUTMNorthing[0]), &(boxBottomLeftUTMEasting[0]));
    _inputMap->Win2UTM(_inputMap->getNumRows()-1, _inputMap->getNumCols()-1,
		       &(boxTopRightUTMNorthing[0]), &(boxTopRightUTMEasting[0]));

    prevWaypointNum[0] = _inputRDDF->getWaypointNumBehindDist(_inputMap->getVehLocUTMNorthing(),
							      _inputMap->getVehLocUTMEasting(),
							      1000);
    nextWaypointNum[0] = _inputRDDF->getWaypointNumAheadDist(_inputMap->getVehLocUTMNorthing(),
							      _inputMap->getVehLocUTMEasting(),
							      1000) - 1;
    //printf("Drawing from %d to %d\n", prevWaypointNum[0], nextWaypointNum[0]);

    for(int j=prevWaypointNum[0]; j <= nextWaypointNum[0]; j++) { 
      //get coords for waypt box
      int temp = _inputRDDF->getCorridorSegmentBoundingBoxUTM(j, 
							      boxBottomLeftUTMNorthing[0], boxBottomLeftUTMEasting[0],
							      boxTopRightUTMNorthing[0], boxTopRightUTMEasting[0],
							      waypointBottomLeftNorthing, waypointBottomLeftEasting,
							      waypointTopRightNorthing, waypointTopRightEasting);
      _inputMap->UTM2Win(waypointBottomLeftNorthing, waypointBottomLeftEasting,
			 &corridorBoxBottomLeftRow, &corridorBoxBottomLeftCol);
      _inputMap->UTM2Win(waypointTopRightNorthing, waypointTopRightEasting,
			 &corridorBoxTopRightRow, &corridorBoxTopRightCol);
      if(temp >= 0) {
	for(row=corridorBoxBottomLeftRow; row <= corridorBoxTopRightRow; row++) {
	  for(col=corridorBoxBottomLeftCol; col <= corridorBoxTopRightCol; col++) {
	    if(_inputMap->getDataWin<double>(_layerNum, row, col) != _insideCorridorVal) {
	      _inputMap->Win2UTM(row, col, &Northing, &Easting);
	      double tempVal = _inputRDDF->isPointInCorridor(j, Northing, Easting);
	      if(tempVal == 1) {
		tempVal = _insideCorridorVal;
	      } else {
		tempVal = _outsideCorridorVal;
	      }
	      //printf("%s [%d]: Setting data!\n", __FILE__, __LINE__);
	      if(_useDelta==1) {
		_inputMap->setDataWin_Delta<double>(_layerNum, row, col, 
						    tempVal);
	      } else {
		_inputMap->setDataWin<double>(_layerNum, row, col, 
					      tempVal);
	      }
	    }
	  }
	}
      }
    }

    //after = TVNow();
    //printf("Shift all took %lf, paint took %lf, trans took %lf, set took %lf/%d=%lf\n", 
    //(after-before).dbl(), painting, trans/((double)counter), sets, counter, sets/((double)counter));
  } else {

  if(deltaRows>=0) {
    //we moved north, empty area is at top
    boxBottomLeftRow[2] = _inputMap->getNumRows()-deltaRows;
    boxTopRightRow[2] = _inputMap->getNumRows() - 1;
    boxBottomLeftRow[0] = _inputMap->getNumRows() - deltaRows;
    boxTopRightRow[0] = _inputMap->getNumRows() - 1;
    boxBottomLeftRow[1] = 0;
    boxTopRightRow[1] = _inputMap->getNumRows() - deltaRows;
  } else {
    //moved south, empty at bottom
    boxBottomLeftRow[2] = 0;
    boxTopRightRow[2] = -1*deltaRows;
    boxBottomLeftRow[0] = 0;
    boxTopRightRow[0] = -1*deltaRows;
    boxBottomLeftRow[1] = -1*deltaRows;
    boxTopRightRow[1] = _inputMap->getNumRows() -1;
  }
  if(deltaCols>=0) {
    //we moved east, empty area is at right
    boxBottomLeftCol[2] = _inputMap->getNumCols() - deltaCols;
    boxTopRightCol[2] = _inputMap->getNumCols() -1;

    boxBottomLeftCol[1] = _inputMap->getNumCols() - deltaCols;
    boxTopRightCol[1] = _inputMap->getNumCols() - 1;
    boxBottomLeftCol[0] = 0;
    boxTopRightCol[0] = _inputMap->getNumCols() - deltaCols;
  } else {
    //moved west, empty at left
    boxBottomLeftCol[2] = 0;
    boxTopRightCol[2] = -1*deltaCols;

    boxBottomLeftCol[1] = 0;
    boxTopRightCol[1] = -1*deltaCols;
    boxBottomLeftCol[0] = -1*deltaCols;
    boxTopRightCol[0] = _inputMap->getNumCols()-1;
  }
  /*
  prevWaypointNum[0] = _inputRDDF->getWaypointNumBehindOut(currentWaypoint,
							   _inputMap->getWindowBottomLeftUTMNorthing(),
							   _inputMap->getWindowBottomLeftUTMEasting(),
							   _inputMap->getWindowTopRightUTMNorthing(),
							   _inputMap->getWindowTopRightUTMEasting());
  nextWaypointNum[0] = _inputRDDF->getWaypointNumAheadOut(currentWaypoint,
							  _inputMap->getWindowBottomLeftUTMNorthing(),
							  _inputMap->getWindowBottomLeftUTMEasting(),
							  _inputMap->getWindowTopRightUTMNorthing(),
							  _inputMap->getWindowTopRightUTMEasting());
  */
    prevWaypointNum[0] = _inputRDDF->getWaypointNumBehindDist(_inputMap->getVehLocUTMNorthing(),
							      _inputMap->getVehLocUTMEasting(),
							      1000);
    nextWaypointNum[0] = _inputRDDF->getWaypointNumAheadDist(_inputMap->getVehLocUTMNorthing(),
							      _inputMap->getVehLocUTMEasting(),
							      1000) - 1;


  for(int i=0; i<3; i++) {
    //get the waypoints for each box  
    //printf("Box %d: (%d, %d) to (%d, %d)\n", i,
    //boxBottomLeftRow[i], boxBottomLeftCol[i],
    //boxTopRightRow[i], boxTopRightCol[i]);
    _inputMap->Win2UTM(boxBottomLeftRow[i], boxBottomLeftCol[i],
		       &(boxBottomLeftUTMNorthing[i]), &(boxBottomLeftUTMEasting[i]));
    _inputMap->Win2UTM(boxTopRightRow[i], boxTopRightCol[i],
		       &(boxTopRightUTMNorthing[i]), &(boxTopRightUTMEasting[i]));
    
    /*
    prevWaypointNum[i] = _inputRDDF->getWaypointNumBehindOut(currentWaypoint,
							     boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
							     boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i]);
    nextWaypointNum[i] = _inputRDDF->getWaypointNumAheadOut(currentWaypoint,
							    boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
							    boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i]);
    */
    for(int j=prevWaypointNum[0]; j <= nextWaypointNum[0]; j++) { 
      //get coords for waypt box
      int temp = _inputRDDF->getCorridorSegmentBoundingBoxUTM(j, 
							      boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
							      boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i],
							      waypointBottomLeftNorthing, waypointBottomLeftEasting,
							      waypointTopRightNorthing, waypointTopRightEasting);

      _inputMap->UTM2Win(waypointBottomLeftNorthing, waypointBottomLeftEasting,
			 &corridorBoxBottomLeftRow, &corridorBoxBottomLeftCol);
      _inputMap->UTM2Win(waypointTopRightNorthing, waypointTopRightEasting,
			 &corridorBoxTopRightRow, &corridorBoxTopRightCol);
      /*
      printf("%d: (%d=%lf, %d=%lf) to (%d=%lf, %d=%lf)\n", j,
	     corridorBoxBottomLeftRow, waypointBottomLeftNorthing,
	     corridorBoxBottomLeftCol, waypointBottomLeftEasting,
	     corridorBoxTopRightRow, waypointTopRightNorthing,
	     corridorBoxTopRightCol, waypointTopRightEasting);
      */
      if(temp >= 0) {
	for(row=corridorBoxBottomLeftRow; row <= corridorBoxTopRightRow; row++) {
	  for(col=corridorBoxBottomLeftCol; col <= corridorBoxTopRightCol; col++) {
	    _inputMap->Win2UTM(row, col, &Northing, &Easting);
	    if(_inputMap->getDataWin<double>(_layerNum, row, col) != _insideCorridorVal) {
	      double tempVal = _inputRDDF->isPointInCorridor(j, Northing, Easting);
	      if(tempVal == 1) {
		tempVal = _insideCorridorVal;
	      } else {
		tempVal = _outsideCorridorVal;
	      }
	      if(_useDelta==1) {
		_inputMap->setDataWin_Delta<double>(_layerNum, row, col, 
						    tempVal);
	      } else {
		_inputMap->setDataWin<double>(_layerNum, row, col, 
					      tempVal);
	      }
	      //printf("%s [%n]: Setting data!\n", __FILE__, __LINE__);
	    }
	  }
	}
      }
    }
  }
  }

  _prevBottomLeftRowMultiple = newBottomLeftRowMultiple;
  _prevBottomLeftColMultiple = newBottomLeftColMultiple;

  return 0;
}
#endif


int CCorridorPainter::paintChanges(NEcoord rowBox[], NEcoord colBox[]) {
  int row, col;
  int currentWaypoint;
  double waypointBottomLeftNorthing, waypointBottomLeftEasting;
  double waypointTopRightNorthing, waypointTopRightEasting;
  double Northing, Easting;

  int prevWaypointNum[3];
  int nextWaypointNum[3];

  prevWaypointNum[0] = _inputRDDF->getWaypointNumBehindDist(_inputMap->getVehLocUTMNorthing(),
                                                            _inputMap->getVehLocUTMEasting(),
                                                            1000);
  nextWaypointNum[0] = _inputRDDF->getWaypointNumAheadDist(_inputMap->getVehLocUTMNorthing(),
                                                           _inputMap->getVehLocUTMEasting(),
                                                           1000) - 1;

  double boxBottomLeftUTMNorthing[2];
  double boxBottomLeftUTMEasting[2];
  double boxTopRightUTMNorthing[2];
  double boxTopRightUTMEasting[2];

  int corridorBoxBottomLeftRow;
  int corridorBoxBottomLeftCol;
  int corridorBoxTopRightRow;
  int corridorBoxTopRightCol;

  boxBottomLeftUTMNorthing[0] = rowBox[0].N;
  boxBottomLeftUTMEasting[0] = rowBox[0].E;
  boxTopRightUTMNorthing[0] = rowBox[2].N;
  boxTopRightUTMEasting[0] = rowBox[2].E;

  boxBottomLeftUTMNorthing[1] = colBox[0].N;
  boxBottomLeftUTMEasting[1] = colBox[0].E;
  boxTopRightUTMNorthing[1] = colBox[2].N;
  boxTopRightUTMEasting[1] = colBox[2].E;
  

  for(int i=0; i<2; i++) {
    //printf("[%d] %s: \n", __LINE__, __FILE__);
    //get the waypoints for each box  
    //printf("Box %d: (%d, %d) to (%d, %d)\n", i,
    //boxBottomLeftRow[i], boxBottomLeftCol[i],
    //boxTopRightRow[i], boxTopRightCol[i]);
    /*
      _inputMap->Win2UTM(boxBottomLeftRow[i], boxBottomLeftCol[i],
      &(boxBottomLeftUTMNorthing[i]), &(boxBottomLeftUTMEasting[i]));
      _inputMap->Win2UTM(boxTopRightRow[i], boxTopRightCol[i],
      &(boxTopRightUTMNorthing[i]), &(boxTopRightUTMEasting[i]));
    */
    //printf("Drawing from %d to %d\n", prevWaypointNum[0], nextWaypointNum[0]);
    /*
      prevWaypointNum[i] = _inputRDDF->getWaypointNumBehindOut(currentWaypoint,
      boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
      boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i]);
      nextWaypointNum[i] = _inputRDDF->getWaypointNumAheadOut(currentWaypoint,
      boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
      boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i]);
    */
    //for(int j=3; j<4; j++) {
    for(int j=prevWaypointNum[0]; j <= nextWaypointNum[0]; j++) { 
      //get coords for waypt box
      int temp = _inputRDDF->getCorridorSegmentBoundingBoxUTM(j, 
                                                              boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
                                                              boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i],
                                                              waypointBottomLeftNorthing, waypointBottomLeftEasting,
                                                              waypointTopRightNorthing, waypointTopRightEasting);

      _inputMap->UTM2Win(waypointBottomLeftNorthing, waypointBottomLeftEasting,
                         &corridorBoxBottomLeftRow, &corridorBoxBottomLeftCol);
      _inputMap->UTM2Win(waypointTopRightNorthing, waypointTopRightEasting,
                         &corridorBoxTopRightRow, &corridorBoxTopRightCol);

      // force the values into range
      corridorBoxBottomLeftRow = max(corridorBoxBottomLeftRow, 0);
      corridorBoxBottomLeftCol = max(corridorBoxBottomLeftCol, 0);
      corridorBoxTopRightRow   = min(corridorBoxTopRightRow, _inputMap->getNumRows()-1);
      corridorBoxTopRightCol   = min(corridorBoxTopRightCol, _inputMap->getNumCols()-1);
      /*  
        printf("%d: (%d=%lf, %d=%lf) to (%d=%lf, %d=%lf)\n", j,
        corridorBoxBottomLeftRow, waypointBottomLeftNorthing,
        corridorBoxBottomLeftCol, waypointBottomLeftEasting,
        corridorBoxTopRightRow, waypointTopRightNorthing,
        corridorBoxTopRightCol, waypointTopRightEasting);
      */
      //printf("[%d] %s: \n", __LINE__, __FILE__);
      if(temp >= 0) {
        for(row=corridorBoxBottomLeftRow; row <= corridorBoxTopRightRow; row++) {
          for(col=corridorBoxBottomLeftCol; col <= corridorBoxTopRightCol; col++) {
            if(_inputMap->getDataWin<double>(_layerNum, row, col) == _outsideCorridorVal) {
              _inputMap->Win2UTM(row, col, &Northing, &Easting);
              double tempVal = _inputRDDF->isPointInCorridor(j, Northing, Easting);
              int inNext = 0;
              if(j+1<_inputRDDF->getNumTargetPoints()) {
                inNext = _inputRDDF->isPointInCorridor(j+1, Northing, Easting);
              }
              int inPrev = 0;
              if(j-1>0) {
                int inPrev= _inputRDDF->isPointInCorridor(j-1, Northing, Easting);
              }

              if(tempVal == 1) {
		double prevSpeed = 0.0;
		double currSpeed = 0.0;
		double nextSpeed = 0.0;
		if(inPrev) {
		  prevSpeed = _inputRDDF->getWaypointSpeed(j-1);
		}
		if(inNext) {
		  nextSpeed = _inputRDDF->getWaypointSpeed(j+1);
		}
		currSpeed = _inputRDDF->getWaypointSpeed(j);
		tempVal = max(max(prevSpeed, nextSpeed), currSpeed);
		/*
                //tempVal = _inputRDDF->getRatioToTrackline(NEcoord(Northing, Easting));
                double ratio = _inputRDDF->getRatioToTrackline(NEcoord(Northing, Easting));
                if((ratio) < (1-_percentSwitch)) {
                  tempVal = _edgeVal+(_tracklineVal - _edgeVal)*((1-_percentSwitch - ratio)/(1-_percentSwitch));
                } else if(inNext || inPrev){
                  tempVal = _edgeVal+(_tracklineVal - _edgeVal)*((1-_percentSwitch - ratio)/(1-_percentSwitch));
                } else {
                  tempVal = _outsideCorridorVal + (_edgeVal - _outsideCorridorVal)*(1-ratio)/(_percentSwitch);
                }
                //tempVal = ratio;
                //tempVal = _edgeVal + (_tracklineVal - _edgeVal)*(1-_inputRDDF->getRatioToTrackline(NEcoord(Northing, Easting)));
                //tempVal = _insideCorridorVal;
		*/
              } else {
                tempVal = _outsideCorridorVal;
              }
	      
	      _inputMap->setDataWin_Delta<double>(_layerNum, row, col, 
						  tempVal);
              //printf("%s [%d]: Setting data! to %lf\n", __FILE__, __LINE__, tempVal);
            }
          }
        }
      }
    }
  }

}


#ifdef CP_OBS
int CCorridorPainter::paintChanges_TracklineSlope() {
  int row, col;
  int currentWaypoint;
  double waypointBottomLeftNorthing, waypointBottomLeftEasting;
  double waypointTopRightNorthing, waypointTopRightEasting;
  double Northing, Easting;

  int newBottomLeftRowMultiple = _inputMap->getWindowBottomLeftUTMNorthingRowResMultiple();
  int newBottomLeftColMultiple = _inputMap->getWindowBottomLeftUTMEastingColResMultiple();
  int deltaRows, deltaCols;

  //Box 0 is row box
  //Box 1 is col box
  //Box 2 is combo box
  int boxBottomLeftRow[3];
  int boxBottomLeftCol[3];
  int boxTopRightRow[3];
  int boxTopRightCol[3];

  double boxBottomLeftUTMNorthing[3];
  double boxBottomLeftUTMEasting[3];
  double boxTopRightUTMNorthing[3];
  double boxTopRightUTMEasting[3];

  int prevWaypointNum[3];
  int nextWaypointNum[3];

  int corridorBoxBottomLeftRow;
  int corridorBoxBottomLeftCol;
  int corridorBoxTopRightRow;
  int corridorBoxTopRightCol;

  //get the seed
  currentWaypoint = _inputRDDF->getCurrentWaypointNumber(_inputMap->getVehLocUTMNorthing(), _inputMap->getVehLocUTMEasting());
  //printf("Seed was %d\n", currentWaypoint);

  //get the corners
  deltaRows = newBottomLeftRowMultiple - _prevBottomLeftRowMultiple;
  deltaCols = newBottomLeftColMultiple - _prevBottomLeftColMultiple;


  if(abs(deltaRows) >= _inputMap->getNumRows() ||
     abs(deltaCols) >= _inputMap->getNumCols()) {
    //printf("Redrawing rddf\n");
    _inputMap->Win2UTM(0, 0,
		       &(boxBottomLeftUTMNorthing[0]), &(boxBottomLeftUTMEasting[0]));
    _inputMap->Win2UTM(_inputMap->getNumRows()-1, _inputMap->getNumCols()-1,
		       &(boxTopRightUTMNorthing[0]), &(boxTopRightUTMEasting[0]));

    prevWaypointNum[0] = _inputRDDF->getWaypointNumBehindDist(_inputMap->getVehLocUTMNorthing(),
							      _inputMap->getVehLocUTMEasting(),
							      1000);
    nextWaypointNum[0] = _inputRDDF->getWaypointNumAheadDist(_inputMap->getVehLocUTMNorthing(),
							      _inputMap->getVehLocUTMEasting(),
							      1000) - 1;
    //printf("Drawing from %d to %d\n", prevWaypointNum[0], nextWaypointNum[0]);

    for(int j=prevWaypointNum[0]; j <= nextWaypointNum[0]; j++) { 
      //get coords for waypt box
      int temp = _inputRDDF->getCorridorSegmentBoundingBoxUTM(j, 
							      boxBottomLeftUTMNorthing[0], boxBottomLeftUTMEasting[0],
							      boxTopRightUTMNorthing[0], boxTopRightUTMEasting[0],
							      waypointBottomLeftNorthing, waypointBottomLeftEasting,
							      waypointTopRightNorthing, waypointTopRightEasting);
      _inputMap->UTM2Win(waypointBottomLeftNorthing, waypointBottomLeftEasting,
			 &corridorBoxBottomLeftRow, &corridorBoxBottomLeftCol);
      _inputMap->UTM2Win(waypointTopRightNorthing, waypointTopRightEasting,
			 &corridorBoxTopRightRow, &corridorBoxTopRightCol);
      if(temp >= 0) {
	for(row=corridorBoxBottomLeftRow; row <= corridorBoxTopRightRow; row++) {
	  for(col=corridorBoxBottomLeftCol; col <= corridorBoxTopRightCol; col++) {
	    /*
	    printf("outside is %lf, this is %lf\n",
		   _outsideCorridorVal,
		   _inputMap->getDataWin<double>(_layerNum, row, col));
	    */
	    if(_inputMap->getDataWin<double>(_layerNum, row, col) == _outsideCorridorVal) {
	      _inputMap->Win2UTM(row, col, &Northing, &Easting);
	      double tempVal = _inputRDDF->isPointInCorridor(j, Northing, Easting);
	      int inNext = 0;
	      if(j+1<_inputRDDF->getNumTargetPoints()) {
		inNext = _inputRDDF->isPointInCorridor(j+1, Northing, Easting);
	      }
	      int inPrev = 0;
	      if(j-1>0) {
		int inPrev= _inputRDDF->isPointInCorridor(j-1, Northing, Easting);
	      }

	      if(tempVal == 1) {
		//tempVal = _inputRDDF->getRatioToTrackline(NEcoord(Northing, Easting));
		//printf("Ratio is %lf\n", tempVal);
		//printf("tv: %lf, ev %lf, ov: %lf, ps: %lf\n", _tracklineVal, _edgeVal, _outsideCorridorVal, _percentSwitch);
		double ratio = _inputRDDF->getRatioToTrackline(NEcoord(Northing, Easting));
		if((ratio) < (1-_percentSwitch)) {
		  //printf("not steep\n");
		  tempVal = _edgeVal+(_tracklineVal - _edgeVal)*((1-_percentSwitch - ratio)/(1-_percentSwitch));
		} else if(inNext || inPrev){
		  tempVal = _edgeVal+(_tracklineVal - _edgeVal)*((1-_percentSwitch - ratio)/(1-_percentSwitch));
		} else {
		  //printf("Steep\n");
		  tempVal = _outsideCorridorVal + (_edgeVal - _outsideCorridorVal)*(1-ratio)/(_percentSwitch);
		}
		//printf("Ratio is %lf, val is %lf\n", ratio, tempVal);
		//tempVal = ratio;
		//tempVal = _edgeVal + (_tracklineVal - _edgeVal)*(1-_inputRDDF->getRatioToTrackline(NEcoord(Northing, Easting)));
	      } else {
		tempVal = _outsideCorridorVal;
	      }
	      //printf("%s [%d]: Setting data!\n", __FILE__, __LINE__);
	      if(_useDelta==1) {
	      _inputMap->setDataWin_Delta<double>(_layerNum, row, col, 
					 tempVal);
	      } else {
	      _inputMap->setDataWin<double>(_layerNum, row, col, 
					    tempVal);
	      }
	    }
	  }
	}
      }
    }

    //after = TVNow();
    //printf("Shift all took %lf, paint took %lf, trans took %lf, set took %lf/%d=%lf\n", 
    //(after-before).dbl(), painting, trans/((double)counter), sets, counter, sets/((double)counter));
  } else {
    //printf("[%d] %s: deltarows: %d, deltacols %d\n", __LINE__, __FILE__, deltaRows, deltaCols);
  if(deltaRows>=0) {
    //we moved north, empty area is at top
    boxBottomLeftRow[2] = _inputMap->getNumRows()-deltaRows;
    boxTopRightRow[2] = _inputMap->getNumRows() - 1;
    boxBottomLeftRow[0] = _inputMap->getNumRows() - deltaRows;
    boxTopRightRow[0] = _inputMap->getNumRows() - 1;
    boxBottomLeftRow[1] = 0;
    boxTopRightRow[1] = _inputMap->getNumRows() - deltaRows;
  } else {
    //moved south, empty at bottom
    boxBottomLeftRow[2] = 0;
    boxTopRightRow[2] = -1*deltaRows;
    boxBottomLeftRow[0] = 0;
    boxTopRightRow[0] = -1*deltaRows;
    boxBottomLeftRow[1] = -1*deltaRows;
    boxTopRightRow[1] = _inputMap->getNumRows() -1;
  }
  if(deltaCols>=0) {
    //we moved east, empty area is at right
    boxBottomLeftCol[2] = _inputMap->getNumCols() - deltaCols;
    boxTopRightCol[2] = _inputMap->getNumCols() -1;

    boxBottomLeftCol[1] = _inputMap->getNumCols() - deltaCols;
    boxTopRightCol[1] = _inputMap->getNumCols() - 1;
    boxBottomLeftCol[0] = 0;
    boxTopRightCol[0] = _inputMap->getNumCols() - deltaCols;
  } else {
    //moved west, empty at left
    boxBottomLeftCol[2] = 0;
    boxTopRightCol[2] = -1*deltaCols;

    boxBottomLeftCol[1] = 0;
    boxTopRightCol[1] = -1*deltaCols;
    boxBottomLeftCol[0] = -1*deltaCols;
    boxTopRightCol[0] = _inputMap->getNumCols()-1;
  }
  /*
  prevWaypointNum[0] = _inputRDDF->getWaypointNumBehindOut(currentWaypoint,
							   _inputMap->getWindowBottomLeftUTMNorthing(),
							   _inputMap->getWindowBottomLeftUTMEasting(),
							   _inputMap->getWindowTopRightUTMNorthing(),
							   _inputMap->getWindowTopRightUTMEasting());
  nextWaypointNum[0] = _inputRDDF->getWaypointNumAheadOut(currentWaypoint,
							  _inputMap->getWindowBottomLeftUTMNorthing(),
							  _inputMap->getWindowBottomLeftUTMEasting(),
							  _inputMap->getWindowTopRightUTMNorthing(),
							  _inputMap->getWindowTopRightUTMEasting());
  */
    prevWaypointNum[0] = _inputRDDF->getWaypointNumBehindDist(_inputMap->getVehLocUTMNorthing(),
							      _inputMap->getVehLocUTMEasting(),
							      1000);
    nextWaypointNum[0] = _inputRDDF->getWaypointNumAheadDist(_inputMap->getVehLocUTMNorthing(),
							      _inputMap->getVehLocUTMEasting(),
							     1000) - 1;
    //printf("[%d] %s: \n", __LINE__, __FILE__);
  
  for(int i=0; i<3; i++) {
    //printf("[%d] %s: \n", __LINE__, __FILE__);
    //get the waypoints for each box  
    //printf("Box %d: (%d, %d) to (%d, %d)\n", i,
    //boxBottomLeftRow[i], boxBottomLeftCol[i],
    //boxTopRightRow[i], boxTopRightCol[i]);
    _inputMap->Win2UTM(boxBottomLeftRow[i], boxBottomLeftCol[i],
		       &(boxBottomLeftUTMNorthing[i]), &(boxBottomLeftUTMEasting[i]));
    _inputMap->Win2UTM(boxTopRightRow[i], boxTopRightCol[i],
		       &(boxTopRightUTMNorthing[i]), &(boxTopRightUTMEasting[i]));
    
    //printf("Drawing from %d to %d\n", prevWaypointNum[0], nextWaypointNum[0]);
    /*
    prevWaypointNum[i] = _inputRDDF->getWaypointNumBehindOut(currentWaypoint,
							     boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
							     boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i]);
    nextWaypointNum[i] = _inputRDDF->getWaypointNumAheadOut(currentWaypoint,
							    boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
							    boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i]);
    */
    for(int j=prevWaypointNum[0]; j <= nextWaypointNum[0]; j++) { 
      //get coords for waypt box
      int temp = _inputRDDF->getCorridorSegmentBoundingBoxUTM(j, 
							      boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
							      boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i],
							      waypointBottomLeftNorthing, waypointBottomLeftEasting,
							      waypointTopRightNorthing, waypointTopRightEasting);

      _inputMap->UTM2Win(waypointBottomLeftNorthing, waypointBottomLeftEasting,
			 &corridorBoxBottomLeftRow, &corridorBoxBottomLeftCol);
      _inputMap->UTM2Win(waypointTopRightNorthing, waypointTopRightEasting,
			 &corridorBoxTopRightRow, &corridorBoxTopRightCol);
      /*
      printf("%d: (%d=%lf, %d=%lf) to (%d=%lf, %d=%lf)\n", j,
	     corridorBoxBottomLeftRow, waypointBottomLeftNorthing,
	     corridorBoxBottomLeftCol, waypointBottomLeftEasting,
	     corridorBoxTopRightRow, waypointTopRightNorthing,
	     corridorBoxTopRightCol, waypointTopRightEasting);
      */
      //printf("[%d] %s: \n", __LINE__, __FILE__);
      if(temp >= 0) {
	for(row=corridorBoxBottomLeftRow; row <= corridorBoxTopRightRow; row++) {
	  for(col=corridorBoxBottomLeftCol; col <= corridorBoxTopRightCol; col++) {
	    if(_inputMap->getDataWin<double>(_layerNum, row, col) == _outsideCorridorVal) {
	      _inputMap->Win2UTM(row, col, &Northing, &Easting);
	      double tempVal = _inputRDDF->isPointInCorridor(j, Northing, Easting);
	      int inNext = 0;
	      if(j+1<_inputRDDF->getNumTargetPoints()) {
		inNext = _inputRDDF->isPointInCorridor(j+1, Northing, Easting);
	      }
	      int inPrev = 0;
	      if(j-1>0) {
		int inPrev= _inputRDDF->isPointInCorridor(j-1, Northing, Easting);
	      }

	      if(tempVal == 1) {
		//tempVal = _inputRDDF->getRatioToTrackline(NEcoord(Northing, Easting));
		double ratio = _inputRDDF->getRatioToTrackline(NEcoord(Northing, Easting));
		if((ratio) < (1-_percentSwitch)) {
		  tempVal = _edgeVal+(_tracklineVal - _edgeVal)*((1-_percentSwitch - ratio)/(1-_percentSwitch));
		} else if(inNext || inPrev){
		  tempVal = _edgeVal+(_tracklineVal - _edgeVal)*((1-_percentSwitch - ratio)/(1-_percentSwitch));
		} else {
		  tempVal = _outsideCorridorVal + (_edgeVal - _outsideCorridorVal)*(1-ratio)/(_percentSwitch);
		}
		//tempVal = ratio;
		//tempVal = _edgeVal + (_tracklineVal - _edgeVal)*(1-_inputRDDF->getRatioToTrackline(NEcoord(Northing, Easting)));
		//tempVal = _insideCorridorVal;
	      } else {
		tempVal = _outsideCorridorVal;
	      }
	      if(_useDelta==1) {
		_inputMap->setDataWin_Delta<double>(_layerNum, row, col, 
						    tempVal);
	      } else {
		_inputMap->setDataWin<double>(_layerNum, row, col, 
					      tempVal);
	      }
	      //printf("%s [%d]: Setting data! to %lf\n", __FILE__, __LINE__, tempVal);
	    }
	  }
	}
      }
    }
  }
  }

  //printf("[%d] %s: \n", __LINE__, __FILE__);
  _prevBottomLeftRowMultiple = newBottomLeftRowMultiple;
  _prevBottomLeftColMultiple = newBottomLeftColMultiple;

  double northing[4];
  double easting[4];

  int int_northing[4];
  int int_easting[4];

  row = 0;
  col = 1;
  int combo=2;

  if(deltaRows>=0) {
    printf("CP North\n");
    northing[0] = boxBottomLeftUTMNorthing[col];
    northing[1] = boxTopRightUTMNorthing[col];
    northing[2] = boxTopRightUTMNorthing[col];
    northing[3] = boxBottomLeftUTMNorthing[col];
  } else {
    printf("CP South\n");
    northing[0] = boxBottomLeftUTMNorthing[col];
    northing[1] = boxTopRightUTMNorthing[col];
    northing[2] = boxTopRightUTMNorthing[col];
    northing[3] = boxBottomLeftUTMNorthing[col];
  }
  if(deltaCols>=0) {
    printf("CP East\n");
    easting[0] = boxBottomLeftUTMEasting[col];
    easting[1] = boxBottomLeftUTMEasting[col];
    easting[2] = boxTopRightUTMEasting[col];
    easting[3] = boxTopRightUTMEasting[col];
  } else {
    printf("CP West\n");
    easting[0] = boxBottomLeftUTMEasting[col];
    easting[1] = boxBottomLeftUTMEasting[col];
    easting[2] = boxTopRightUTMEasting[col];
    easting[3] = boxTopRightUTMEasting[col];
  }

  printf("CP %d %d\n", deltaRows, deltaCols);
  printf("CP Points are: ");
  /*
  printf("(%lf, %lf), ", northing[0], easting[0]);
  printf("(%lf, %lf), ", northing[1], easting[1]);
  printf("(%lf, %lf), ", northing[2], easting[2]);
  printf("(%lf, %lf), ", northing[3], easting[3]);
  */
  for(int i=0; i<4; i++) {
    _inputMap->UTM2Win(northing[i], easting[i], &(int_northing[i]), &(int_easting[i]));
    printf("(%d, %d), ", int_northing[i], int_easting[i]);
  }
  printf("\n");

  return 0;
}
#endif
