/*
 * FakeState_Main - argument parsing for FakeState
 *
 * JML, Dec 04
 *
 * 22 Mar 04, RMM: added argument processing for offsets
 * 23 Mar 04, KLK: cahnged to new method of pushing state out.
 */

#include "FakeState.hh"
#include "DGCutils"
#include <fstream>
#include <iostream>
#include <boost/tokenizer.hpp>
#include <string>
#include <cstdio>
#define FAKESTATE_PUSH_SLEEP 5000 //changed from 5000 chris

int main(int argc, char **argv)
{
        int c, errflg = 0;
	int nflg = 0, eflg = 0;
	double northing_off, easting_off;
	int sn_key = 0;

	/* Process command line arguments */
	while ((c = getopt(argc, argv, "n:e:")) != EOF)
	  switch (c) {
	  case 'n':
	    northing_off = atof(optarg);
	    nflg = 1;
	    break;

	  case 'e':
	    easting_off = atof(optarg);
	    eflg = 1;
	    break;

	  default:
	    errflg++;
	    break;
	  }
	if (errflg) {
	  fprintf(stderr, 
	    "usage: %s [-n northing_offset] [-e easting offset] [sn_key]\n",
	    argv[0]); 
	  exit(2);
	}

	char* default_key = getenv("SKYNET_KEY");
	if (argc > optind)
	  {
	    sn_key  = atoi(argv[optind]);
	  }
	else if (default_key != NULL )
	  {
	    sn_key = atoi(default_key);
	  }else
	{
		cerr << "SKYNET_KEY environment variable isn't set." << endl;
	}
	cerr << "Constructing skynet with KEY = " << sn_key << endl;
	FakeState ss(sn_key,1,1,1,1,1);

	// Set the offset of the Northing and Easting
	// Default = Stoddard Wells starting point

	char *rddf;

	// First check to see if the RDDF environment variable is set
	if ((rddf = getenv("RDDF_BOB")) == NULL) {
	  // Otherwise, look for rddf.bob in the current directory
	  rddf = "bob.dat";
	}
	  std::ifstream file(rddf);
	
	
	char buffer[1024];
	while(file.peek() == '#') // read through all the remaining commented lines
	  {
	    file.getline(buffer,1024);
	  }
	
	string line;
	getline(file, line); // first line of log data
	typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
	boost::char_separator<char> sep(" \t"); // spaces or tabs
	
	tokenizer tok(line, sep);
	tok.assign(line, sep); // feed a line to the tokenizer	
	
	vector<double> w(3);
	int col = 0;
	
	for(tokenizer::iterator tok_iter = tok.begin(); tok_iter != tok.end();++tok_iter)
	  {
	    if(col >= 1 && col <= 2) // we want columns 2-4
	      {
		w.at(col-1) = atof((*tok_iter).c_str());
	      }
	    col++;
	  }
	//cout << "setting Northing Offset: " << w.at(0)<<endl;
	//cout << "setting Easting Offset: " << w.at(1)<<endl;
	//ss.Northing_Offset = nflg ? northing_off : w.at(0);
	//ss.Easting_Offset = eflg ? easting_off : w.at(1);
         
	ss.Northing_Offset = nflg ? northing_off : 3834841.834; //for stoddard wells
	ss.Easting_Offset = eflg ? easting_off : 496059.329;    //for stoddard wells
	//ss.Northing_Offset = nflg ? northing_off : 3777395.000;   //for Santa Anita
	//ss.Easting_Offset = eflg ? easting_off : 403533.000;      // for Santa Anita

	
	while (true) 
	  {
	    ss.UpdateState();
	    ss.Broadcast();
	    usleep(FAKESTATE_PUSH_SLEEP);
	  }
	return 0;
}





 
  
 
