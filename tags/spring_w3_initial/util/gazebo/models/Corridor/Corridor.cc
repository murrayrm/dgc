/**
 * Author: Henrik Kjellander
 * $Id$
 */

#include <gazebo/ModelFactory.hh>

#include "Corridor.hh"
#include "CircleGeom.hh"
#include "LineGeom.hh"

//#include "RayGeom.hh"

#include <fstream> // ifstream
#include <cstdio> // getenv
#include <iostream>
#include <string>
#include <boost/tokenizer.hpp> // parsing the .bob file

#define DEBUG false // set this to true for more debug printinga

/////////////////////////////////////////////////////////////////////////////
//// Register the model
GZ_REGISTER_PLUGIN("Corridor", Corridor)

//////////////////////////////////////////////////////////////////////////////
// Constructor
Corridor::Corridor( World *world )
    : Model( world )
{
  return;
}


//////////////////////////////////////////////////////////////////////////////
// Destructor
Corridor::~Corridor()
{
  return;
}


//////////////////////////////////////////////////////////////////////////////
// Load the sensor
int Corridor::Load( WorldFile *file, WorldFileNode *node )
{  
  // Create the canonical body
  this->body = new Body( this->world );
  this->AddBody( this->body, true );

  //
  // Open up the RDDF file
  // 
  char *rddf;

  // First check to see if the RDDF environment variable is set
  if ((rddf = getenv("RDDF_BOB")) == NULL) {
    // Otherwise, look for rddf.bob in the current directory
    rddf = "bob.dat";
  }
  printf("Reading RDDF waypoints from %s\n", rddf);

  // Parse the waypoint file
  waypoints = parseWaypoints(rddf);
  
  translateWaypoints();
  
  createCorridor();

  //Some code from an attempt to use the RayGeom instead
  /*GzVector a = GzVectorSet(0, 0, 1);
  GzVector b = GzVectorSet(40, 40, 1);
  
  dGeomSetCategoryBits((dGeomID) this->modelSpaceId, GZ_LASER_COLLIDE);
  dGeomSetCollideBits((dGeomID) this->modelSpaceId, ~GZ_LASER_COLLIDE);

  // A ray
  RayGeom *ray = new RayGeom(this->body, this->modelSpaceId);
  ray->SetColor( GzColor(0, 0, 1) );
  ray->SetCategoryBits( GZ_LASER_COLLIDE );
  ray->SetCollideBits( ~GZ_LASER_COLLIDE );
  ray->SetLength(GzVectorMag(GzVectorSub(b, a)));
  ray->Set(a, b);
  */
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Initialize sensors
int Corridor::Init( WorldFile *file, WorldFileNode *node )
{
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Finalize sensors
int Corridor::Fini()
{
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Update sensor information; returns non-zero if sensor data has
// been updated
void Corridor::Update( double step )
{
  // Get commands from the external interface
  //this->IfaceGetCmd();

  // Command any actuators
  
  // Update any sensors

  // Update the external interface with the new data
  //IfacePutData();
  return;
}

vector<vector<double> > Corridor::parseWaypoints(char* filename)
{
  if(DEBUG) printf("parseWaypoints: reading file '%s'\n", filename);
  std::ifstream file(filename);
  if(!file.is_open())
  {
    printf("parseWaypoints: Cannot read file '%s'\n", filename);
    exit(1);
  }
  
  char buffer[1024];
  while(file.peek() == '#') // read through all the remaining commented lines
  {
    file.getline(buffer, 1024);
    if(DEBUG) printf("%s\n", buffer);
  }

  string line;
  getline(file, line); // first line of log data
  typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
  boost::char_separator<char> sep(" \t"); // spaces or tabs
  
  tokenizer tok(line, sep);
  vector<vector<double> > waypoints;
  
  // now start reading the values
  if(DEBUG) printf(" Easting    Northing  Width\n");
  if(DEBUG) printf("========   =========  =====\n");
  
  do // first line has already been read
  {
    tok.assign(line, sep); // feed a line to the tokenizer
    vector<double> w(3);
    int col = 0;
    for(tokenizer::iterator tok_iter = tok.begin(); tok_iter != tok.end();++tok_iter)
    {
      if(col >= 1 && col <= 3) // we want columns 2-4
      {
        w.at(col-1) = atof((*tok_iter).c_str());
      }
      col++;
    }
    // push this waypoint into the vector if this is not the last line (empty vector)
    // seems like the last line gives zero values of some reason, remove that:
    if(w.size() == 3 && w.at(0) != 0)
    {
      waypoints.push_back(w);
      if(DEBUG) printf("%-10.1lf %-10.1lf %-10.1lf", w.at(0), w.at(1), w.at(2));
    }
    if(DEBUG) cout << endl;
  }
  while(getline(file, line));
  
  return waypoints;
}

void Corridor::translateWaypoints()
{
  double x = waypoints.front().at(0);
  double y = waypoints.front().at(1);
  for(vector<vector<double> >::iterator i = waypoints.begin(); i != waypoints.end(); i++)
  {
    (*i).at(0) -= x;
    (*i).at(1) -= y;
    if(DEBUG) printf("x,y = %lf,%lf\n", (*i).at(0), (*i).at(1));
  }
}

void Corridor::createCorridor()
{
  CircleGeom *circle;
  LineGeom *line;
  for(unsigned int i=0; i < waypoints.size() ;i++)
  {
    vector<double> w = waypoints.at(i);
    circle = new CircleGeom( this->body, this->modelSpaceId, w.at(0), w.at(1), w.at(2));
    if(DEBUG) printf("creating original circle number %d\n", i);
    // draw a similar circle and corridor lines to the next waypoint if it 
    // isn't the last one:
    if(i+1 < waypoints.size()) 
    {
      vector<double> w_next = waypoints.at(i+1);
      // draw a circle at the next waypoint with this waypoints radius.
      circle = new CircleGeom( this->body, this->modelSpaceId, w_next.at(0), w_next.at(1), w.at(2));
      if(DEBUG) printf("creating extended circle number %d\n", i);
      
      // calculate the angle to the next waypoint
      double bearing = atan2( w_next.at(1) - w.at(1), w_next.at(0) - w.at(0) );
      
      // draw the left lateral offset line
      double fromE, fromN, toE, toN;
      //      coord        radius    perpedicular angle
      fromE = w.at(0)    + w.at(2) * cos(bearing + 0.5*M_PI);
      fromN = w.at(1)    + w.at(2) * sin(bearing + 0.5*M_PI);
      toE = w_next.at(0) + w.at(2) * cos(bearing + 0.5*M_PI);
      toN = w_next.at(1) + w.at(2) * sin(bearing + 0.5*M_PI);
      line = new LineGeom(this->body, this->modelSpaceId, fromE, fromN, toE, toN);
      if(DEBUG) printf("drawing left line\n");
      
      // draw the right lateral offset line
      //      coord        radius    perpedicular angle
      fromE = w.at(0)    + w.at(2) * cos(bearing - 0.5*M_PI);
      fromN = w.at(1)    + w.at(2) * sin(bearing - 0.5*M_PI);
      toE = w_next.at(0) + w.at(2) * cos(bearing - 0.5*M_PI);
      toN = w_next.at(1) + w.at(2) * sin(bearing - 0.5*M_PI);
      line = new LineGeom(this->body, this->modelSpaceId, fromE, fromN, toE, toN);      
      if(DEBUG) printf("drawing right line\n");
      
    }
  }
  if(DEBUG) printf("Finished creating waypoints!\n");
}
