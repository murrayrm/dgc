#ifndef LINEGEOM_HH
#define LINEGEOM_HH

#include <gazebo/Geom.hh>

/** A simple drawn red line. 
 * This body draws a line 1cm above the ground (to avoid rendering issues with
 * objects close to each other) in a red color. It was originally made for the
 * purpose of drawing waypoints in the Gazebo world
 * Author: Henrik Kjellander
 * $Id$
 */
class LineGeom : public Geom
{
  /** Constructor
   * @param body the body
   * @param dSpaceID space id
   * @param fromE   from x-coordinate (Easting in meters)
   * @param fromN   from y-coordinate (Northing in meters)
   * @param toE     to x-coordinate (Easting in meters)
   * @param toN     to y-coordinate (Northing in meters)
   */
  public: LineGeom( Body *body, const dSpaceID space, const double fromE, const double fromN, const double toE, const double toN);

  // Destructor
  public: virtual ~LineGeom();

  // Render the geom (GL)
  public: virtual void Render(RenderOptions *opt);

  // Flag set if the line needs redrawing
  private: bool dirty;
  
  // Where the drawing takes place
  private: void draw();

  // Coordinates
  public: double fromE, fromN;
  public: double toE, toN;
};


#endif

