#ifndef CORRIDOR_HH
#define CORRIDOR_HH

#include <gazebo/Body.hh>
#include <gazebo/Model.hh>
#include <vector>

using namespace std;

/**
 * Desc: Corridor drawing model
 * This is created as a Gazebo 0.5.0 plugin (shared object).
 * INSTALLATION
 * -# Type 'make' to build.
 * -# Make sure your dgc/bob/RDDF/bob.dat points at the .bob file you want to use.
 * -# Edit your .world file and add something like the following:
 *  <model:Corridor>
 *    <plugin>../models/Corridor/Corridor.so</plugin>
 *  </model:Corridor>
 *  Make sure to add this within the <GroundPlane> section, or it will ont be
 *  attached to the groundplane, resulting in that the corridor falls down.
 *  -# Run gazebo as usual (i.e. wxgazebo filename.world)
 * 
 * The waypoints drawn are drawn in this way:
 * - The first waypoint's coordinates are picked as (0,0) in the Gazebo world 
 * - The remaining waypoints are translated relative to that. 
 * 
 * NOTE: Currently this plugin looks for the file $RDDF_BOB or ./rddf.bob
 *       for RDDF data to plot.  If neither of these exist 
 *       this plugin won't function properly.
 *
 * See README file for full list of changes
 *
 * @author Henrik Kjellander
 * $Id$
 */
 class Corridor : public Model
{
  // Constructor
  public: Corridor( World *world );
  
  // Destructor
  public: virtual ~Corridor();

  // Load stuff
  public: int Load( WorldFile *file, WorldFileNode *node );
  
  // Initialize stuff
  public: int Init( WorldFile *file, WorldFileNode *node );

  // Finalize stuff
  public: int Fini();
  
  // Do updates
  public: void Update( double step );

  // The canonical body
  private: Body *body;
   
  /** A vector of waypoints, each element is a vector of 3 doubles;
  * -# Easting (m)
  * -# Northing (m)
  * -# radius (m) 
  */
  private: vector<vector<double> > waypoints;
  
  /** Parses a .bob file into waypoint corrdinates
   * @param filename the filename to the .bob file.
   * @return the vector of waypoints
   */
  private: vector<vector<double> > parseWaypoints(char* filename);
  
  /** Creates all the lines and circles that displays the corridor
   */
  private: void createCorridor();
  
  /** This function takes the first waypoint and translates that one to origo 
   * and then all the other waypoints the same amount of Easting/Northing
   * so we get coordinates that are easier to handle. */
  private: void translateWaypoints();
  
};

#endif
