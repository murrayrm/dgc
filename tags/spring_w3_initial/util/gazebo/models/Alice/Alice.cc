/**
 * $Id: Alice.cc 4763 2005-1-25 15:22:29Z braman $
 */

#include <assert.h>
#include <gazebo/World.hh>
#include <gazebo/WorldFile.hh>
#include <gazebo/BoxGeom.hh>
#include <gazebo/SphereGeom.hh>
#include <gazebo/CylinderGeom.hh>
#include <gazebo/WheelGeom.hh>
#include <gazebo/ModelFactory.hh>

#include "Alice.hh"
#include <stdio.h>
#include <ode/ode.h>
#define sign(x) (x/fabs(x))
#define RMIN 1.000
const double RBIG=1e6; 
const double kSteer=2.5; // approx steering speed of 75 deg/s 
const double ACCEL = 36 ; // m/s - top speed of 80mph
const double BRAKE = 10 ; // m/s/s - max brake acceleration
const double FORCE = 4 ; // m/s/s - nominal acceleration (0-30mph)

/////////////////////////////////////////////////////////////////////////////
// Register the model
GZ_REGISTER_PLUGIN("Alice", Alice)

//////////////////////////////////////////////////////////////////////////////
// Constructor
Alice::Alice( World *world )
  : Model( world )
{
  this->encoders[0] = 0;
  this->encoders[1] = 0;
  return;
}


//////////////////////////////////////////////////////////////////////////////
// Destructor
Alice::~Alice()
{
  return;
}


//////////////////////////////////////////////////////////////////////////////
// Load the model
int Alice::Load( WorldFile *file, WorldFileNode *node )
{
  // Create the ODE objects
  if (this->OdeLoad(file, node) != 0)
    return -1;

  // Model id
  
  this->raw_encoder_position = node->GetBool( "raw_encoder_position", false );

  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Initialize the model
int Alice::Init( WorldFile *file, WorldFileNode *node )
{
  // Initialize ODE objects
  if (this->OdeInit(file, node) != 0)
    return -1;

  // Initialize external interface
  if (this->IfaceInit() != 0)
    return -1;
  
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Load ODE objects
int Alice::OdeLoad( WorldFile *file, WorldFileNode *node )
{
  int i;
  Geom *geom;

  // Create a model of Alice 
  // Wheel suspension and softness might screw up the precision here
  
  // The wheels
  this->wheelSep = 1.7907; //?? wheel separation (meters) 
  this->wheelDiam = 0.8382;  // (meters) van
  const double wheelThick = 0.3; // (meters)
  const double wheelMass = 1; // (kilos) scaled so gazebo can handle it
  
  // the "main" part of the chassis
  this->mainLength = 5.5; // 5.5 (meters) 
  const double mainWidth = 2.014; // 2.014 (meters) 
  const double mainHeight = .9126 + this->wheelDiam * 0.5; // (meters) approx
  const double mainMass = 60; // (kilos) scaled so gazebo can handle it
  
  // the top part of the chassis
  const double topLength = 0.8 * mainLength; // (meters) approx
  const double topWidth =  mainWidth;   // (meters) 
  const double topHeight = 1.0;              // (meters) approx
  const double topMass = 20; // (kilos) scaled so gazebo can handle it
    
  // Create the main body of the robot
  this->body = new Body( this->world );
  
  // the main part of the chassis
  this->bodyGeoms[0] = new BoxGeom( this->body, this->modelSpaceId, 
                                    mainLength, mainWidth, mainHeight);
  this->bodyGeoms[0]->SetRelativePosition( GzVectorSet(0, 0, 0.2 * mainHeight) );
  this->bodyGeoms[0]->SetMass( mainMass );
  this->bodyGeoms[0]->SetColor( GzColor(0.9, 0.9, 0.9) ); // almost pure white

  // the top part of the chassi:
  this->bodyGeoms[1] = new BoxGeom( this->body, this->modelSpaceId, 
                                    topLength, topWidth, topHeight); 
  this->bodyGeoms[1]->SetRelativePosition(GzVectorSet(-0.1*mainLength, 0, 1.35*topHeight));
  this->bodyGeoms[1]->SetMass( topMass );
  this->bodyGeoms[1]->SetColor( GzColor(0.9, 0.9, 0.9) ); // almost pure white 

  this->AddBody( this->body, true );
   
  // Create the wheels
  for (i = 0; i < 4; i++)
  {
    // Create a wheel
    this->wheels[i] = new Body( this->world );
    geom = new WheelGeom( this->wheels[i], this->modelSpaceId, 0.5* this->wheelDiam, 0.5 * wheelThick);
    geom->SetRelativePosition(GzVectorSet(0, 0, 0));
    geom->SetMass( wheelMass * 0.5 );
    geom->SetColor( GzColor(0.3, 0.3, 0.3) ); // dark grey

    // Create a white box to attach to the wheel so it's easier to see
    // when they're rotating:
    geom = new BoxGeom( this->wheels[i], this->modelSpaceId, 0.5 * this->wheelDiam, 
                        0.3 * this->wheelDiam, 0.02);
    geom->SetRelativePosition(GzVectorSet(0, 0, 0.4 * wheelThick));
    geom->SetMass( wheelMass * 0.5 );
    geom->SetColor( GzColor(1.0, 1.0, 1.0) ); // white

    this->AddBody( this->wheels[i] );
  }

  this->totalMass = mainMass + topMass + 4 * wheelMass ;

  // position the wheels:
  this->wheels[0]->SetPosition(GzVectorSet( 0.37*mainLength, +0.5*mainWidth+0.1, -0.5*mainHeight));
  this->wheels[1]->SetPosition(GzVectorSet( 0.37*mainLength, -0.5*mainWidth-0.1, -0.5*mainHeight));
  this->wheels[2]->SetPosition(GzVectorSet(-0.37*mainLength, +0.5*mainWidth+0.1, -0.5*mainHeight));
  this->wheels[3]->SetPosition(GzVectorSet(-0.37*mainLength, -0.5*mainWidth-0.1, -0.5*mainHeight));
  
  // set the rotation for the wheels
  this->wheels[0]->SetRotation(GzQuaternFromAxis(1, 0, 0, -M_PI / 2));
  this->wheels[1]->SetRotation(GzQuaternFromAxis(1, 0, 0, +M_PI / 2));
  this->wheels[2]->SetRotation(GzQuaternFromAxis(1, 0, 0, -M_PI / 2));
  this->wheels[3]->SetRotation(GzQuaternFromAxis(1, 0, 0, +M_PI / 2));
  
  // Attach the wheels to the body
  for (i = 0; i < 4; i++)
  {
    // Create suspension joint
    this->sjoints[i] = new Hinge2Joint( this->world->worldId );
    this->sjoints[i]->Attach(this->body, this->wheels[i] );
    
    GzVector ab = wheels[i]->GetPosition();
    this->sjoints[i]->SetAnchor( ab.x, ab.y, ab.z );
  
    // Axis 1: steering, Axis 2: wheel
    this->sjoints[i]->SetAxis1(0,0,1);
    this->sjoints[i]->SetAxis2(0,1,0);

    // Set suspension spring & damping constants
    this->sjoints[i]->SetParam( dParamSuspensionERP, 0.8 ); // 0.4
    this->sjoints[i]->SetParam( dParamSuspensionCFM, 0.01 ); //0.8

    // Set joint stops for steering (+/-30 deg)
    this->sjoints[i]->SetParam( dParamLoStop,-M_PI/6); 
    this->sjoints[i]->SetParam( dParamHiStop,+M_PI/6); 
  }
  
  // fix rear wheels (no steering) 
  this->sjoints[2]->SetParam( dParamLoStop,0);
  this->sjoints[2]->SetParam( dParamHiStop,0);
  this->sjoints[3]->SetParam( dParamLoStop,0);
  this->sjoints[3]->SetParam( dParamHiStop,0);

  this->body->SetFiniteRotationMode(1);  
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Initialize ODE
int Alice::OdeInit( WorldFile *file, WorldFileNode *node )
{
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Finalize the model
int Alice::Fini()
{
  // Finalize external interface  
  this->IfaceFini();

  // Finalize ODE objects
  this->OdeFini();

  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Finalize ODE
int Alice::OdeFini()
{
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Update model
void Alice::Update( double step )
{
  // Get commands from the external interface (player or wxgazebo)
  this->IfaceGetCmd();
  
  // Player sends commands in degrees- must convert 
  double steer = this->steerAngle * M_PI / 180 ;
  //double steer = this->steerAngle ;
  double thpos = this->throttle * -1.0 ;
  double bpos = this->brake ;

  if( bpos <= 0 ) {
    // Set forward speed based on throttle command
    double vel = thpos * ACCEL ;
   
    this->sjoints[0]->SetParam( dParamVel2, vel / this->wheelDiam);
    this->sjoints[1]->SetParam( dParamVel2, vel / this->wheelDiam);
    this->sjoints[2]->SetParam( dParamVel2, vel / this->wheelDiam); 
    this->sjoints[3]->SetParam( dParamVel2, vel / this->wheelDiam); 
    
    double et = abs(thpos) * FORCE * this->totalMass * this->wheelDiam / 8 ;
 //   double et = thpos * FORCE * this->totalMass * this->wheelDiam / 8 ;

  // Must have some initial value at thpos = 0 or else crazy stuff happens
    this->sjoints[0]->SetParam( dParamFMax2, 20 + et ); 
    this->sjoints[1]->SetParam( dParamFMax2, 20 + et ); 
    this->sjoints[2]->SetParam( dParamFMax2, 20 + et); 
    this->sjoints[3]->SetParam( dParamFMax2, 20 + et);

  } else {
    // Set brake torque based on brake command
    double braketorque = bpos * BRAKE * this->wheelDiam * this->totalMass / 8 ; 

    // Set desired wheel speed to zero
    this->sjoints[0]->SetParam( dParamVel2, 0 );
    this->sjoints[1]->SetParam( dParamVel2, 0 );
    this->sjoints[2]->SetParam( dParamVel2, 0 ); 
    this->sjoints[3]->SetParam( dParamVel2, 0 ); 

    // Set opposing brake force
    this->sjoints[0]->SetParam( dParamFMax2, 1 + braketorque ); 
    this->sjoints[1]->SetParam( dParamFMax2, 1 + braketorque );
    this->sjoints[2]->SetParam( dParamFMax2, 1 + braketorque );
    this->sjoints[3]->SetParam( dParamFMax2, 1 + braketorque ); 

  }

 
   double angleL = this->sjoints[0]->GetAngle1();
   double angleR = this->sjoints[1]->GetAngle1();
  // printf("steerL=%f angleL=%f steerR=%f angleR=%f\n",steerL,angleL,steerR,angleR);
 
  // Command steering to correct angle 
  this->sjoints[0]->SetParam(dParamVel,kSteer*(steer-angleL));
  this->sjoints[1]->SetParam(dParamVel,kSteer*(steer-angleR));

  this->sjoints[2]->SetParam(dParamVel, 0);
  this->sjoints[3]->SetParam(dParamVel, 0);

  // these parameters affects how easy it is to turn. The higher values, the
  // faster and sharper turning is possible.
  this->sjoints[0]->SetParam( dParamFMax, 1.1 ); 
  this->sjoints[1]->SetParam( dParamFMax, 1.1 ); 
  this->sjoints[2]->SetParam( dParamFMax, 1.8 ); 
  this->sjoints[3]->SetParam( dParamFMax, 1.8 ); 

  
  // Update the odometry
  this->UpdateOdometry( step );

  // Update the interface
  this->IfacePutData();

  return;
}


//////////////////////////////////////////////////////////////////////////////
// Update the odometry

void Alice::UpdateOdometry( double step )
{
  double wd, ws;
  double d1, d2;
  double dr, da;

  wd = this->wheelDiam;
  ws = this->wheelSep;

  // Average distance travelled by left and right wheels
  //d1 = step * wd * (joints[0]->GetAngle2Rate() + joints[2]->GetAngle2Rate()) / 2;
  //d2 = step * wd * (joints[1]->GetAngle2Rate() + joints[3]->GetAngle2Rate()) / 2;
//  d1 = step * wd * wjoints[2]->GetAngleRate() / 2;
//  d2 = step * wd * wjoints[3]->GetAngleRate() / 2;

  d1 = step * wd * sjoints[2]->GetAngle2Rate() / 2;
  d2 = step * wd * sjoints[3]->GetAngle2Rate() / 2;

  dr = (d1 + d2) / 2;
  da = (d1-d2)/2*ws;  
  // Compute odometric pose
  this->odomPose[0] += dr * cos( this->odomPose[2] );
  this->odomPose[1] += dr * sin( this->odomPose[2] );
  this->odomPose[2] += da;
 
  //update encoder counts
  this->encoders[0] += d1*408/M_PI/wd;
  this->encoders[1] += d2*408/M_PI/wd;   
  
  /*
    GzVector pos;
    
    pos=this->body->GetRelPointPos(0,0,0);
    //  GzVector p0,p1;
    //  p0=this->body->GetRelPointPos(0,0,0);
    //  p1=this->body->GetRelPointPos(1,0,0);
    
    GzQuatern angles=this->body->GetRotation();
    double r,p,y;
    GzQuaternToEuler(&r,&p,&y,angles);
    
    // if(y<0)
    //  y=y+2*M_PI;
    // printf(" dy=%f dx=%f\n",(p1.y-p0.y),(p1.x-p0.x));
    
    this->odomPose[0] =pos.x;
    this->odomPose[1] =pos.y;
    this->odomPose[2] =y; //rad? yep
    //(180/M_PI)*(atan2((p1.y-p0.y),(p1.x-p0.x)));
    //printf("yaw = %f, %f\n",(180/M_PI)*y,this->odomPose[2]);
  */
  return;
}


// Initialize the external interface
int Alice::IfaceInit()
{
  this->iface = gz_position_alloc();
  assert(this->iface);

  if (gz_position_create(this->iface, this->world->gz_server, this->GetId(),
                          "Alice", (int)this, (int)this->parent) != 0)
    return -1;
  
  return 0;
}


// Finalize the external interface
int Alice::IfaceFini()
{
  gz_position_destroy( this->iface );
  gz_position_free( this->iface );
  this->iface = NULL;
  
  return 0;
}


// Get commands from the external interface
void Alice::IfaceGetCmd()
{
  double th, an, br;

  th = this->iface->data->cmd_vel_pos[0];
  br = this->iface->data->cmd_vel_pos[1];
  an = this->iface->data->cmd_vel_rot[2];

  // printf("th=%f br=%f \n",th,br);  
  this->throttle = th ;
  this->brake = br ;
  this->steerAngle = an ;
  
  return;
}

// Update the data in the interface
void Alice::IfacePutData()
{
  // Copy the old data to be able calculating speed
  double last_time = this->iface->data->time;
  
  // Set new timestamp
  this->iface->data->time = this->world->GetSimTime();
  
  double delta_time = this->iface->data->time - last_time;

  /*
  if(this->raw_encoder_position)
  {
    this->iface->data->pos[0] = this->encoders[0]*1e-3;
    this->iface->data->pos[1] = this->encoders[1]*1e-3;
    this->iface->data->rot[2] = NAN;
  }
  else 
  {
    this->iface->data->pos[0] = ( this->odomPose[0]);
    this->iface->data->pos[1] = ( this->odomPose[1]);
    this->iface->data->rot[2] = ( this->odomPose[2]);
    // printf("CLOD DUMP %f,%f,%f\n",this->odomPose[0],this->odomPose[1],this->odomPose[2]);
  }
  printf("old (x,y) = (%lf, %lf)\n", this->iface->data->pos[0], this->iface->data->pos[1]);
  */
    
  // Update positions and rotation
  GzVector pos, rot;
  pos = this->body->GetPosition();
  rot = GzQuaternToEuler(this->body->GetRotation());
  //printf("(x,y,z) = (%lf, %lf, %lf)\n", pos.x, pos.y, pos.z);        
  //printf("(p,r,y) = (%lf, %lf, %lf)\n", rot.x, rot.y, rot.z);     
  
  // calculate an approximated speed before we overwrite our old values
  this->iface->data->vel_pos[0] = fabs(this->iface->data->pos[0] - pos.x) / delta_time;
  this->iface->data->vel_pos[1] = fabs(this->iface->data->pos[1] - pos.y) / delta_time;
  this->iface->data->vel_pos[2] = fabs(this->iface->data->pos[2] - pos.z) / delta_time;

  // now let's fill in the new positioin values to the position interface.
  this->iface->data->pos[0] = pos.x; // Easting
  this->iface->data->pos[1] = pos.y; // Northing
  this->iface->data->pos[2] = pos.z; // Altitude
  this->iface->data->rot[0] = rot.x; // Pitch
  this->iface->data->rot[1] = rot.y; // Roll
  this->iface->data->rot[2] = rot.z; // Yaw
  
  //printf("new (x,y) = (%lf, %lf)\n", this->iface->data->pos[0], this->iface->data->pos[1]);
  return;
}


