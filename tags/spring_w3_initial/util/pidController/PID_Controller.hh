/*
 *CPID_Controller: PID Lateral & Longitudinal Trajectory Follower implemented using
 *the wrapper classes PathFollower (MTA) or sn_TrajFollower (Skynet)
 */

#ifndef PID_CONTROLLER_HH
#define PID_CONTROLLER_HH

//#define PID_COUT_DISPLAY

#include "VehicleState.hh"  // for VehicleState

// The CTraj trajectory container class and teh CPid pid controller wrapper
// class
#include "traj.h"
#include "pid.hh"


#warning "These should come from vehicle constants"
#define MAX_ACCEL							( 6.0)
#define MAX_DECEL							(-2.0)
#define MAX_LAT_ACCEL					( 3.0)

//defines for the DFE
#define DFE_DELTA_SLOPE		 		( 0.0)
#define DFE_DELTA_OFFSET			( 2.0)

#define LatGainFilename				"LatGains.dat"
#define LongGainFilename			"LongGains.dat"

//Distance in meters from the path (perpendicular distance) outside of  which the controller
//will point Alice directly towards (perpendicular to) the path
#define PERP_THETA_DIST				20.0

//alpha and beta gains for the combined error controller
#define ALPHA_GAIN						0.5
#define BETA_GAIN							(2.0*ALPHA_GAIN*PERP_THETA_DIST/(M_PI - M_PI*ALPHA_GAIN))

//value of combined error that causes the modified
//commanded speed (which has a max value = v_traj
//although its current value is scaled by a negative
//gradient linear slope which has a y-intercept of
//v_traj, and a calculated -ve gradient, until the
//saturation point at a small speed, and a relatively
//large combined error
#define C_ERROR_SAT   2.0


//This is the 'small speed' at which the reference
//speed is set when the combined error is greater
//than the C_ERROR_SAT
#define V_LAT_ERROR_SAT 4.0


// indices into the error arrays
#define FRONT 0
#define REAR  1
#define YAW   2

// minimum speed. anything lower than this is bumped up
#define MINSPEED 0.01


enum EYerrorside {YERR_FRONT, YERR_BACK};
enum EAerrorside {AERR_FRONT, AERR_BACK, AERR_YAW};

class CPID_Controller 
{
	// the PID controllers
	Cpid* m_pSpeedPID;
  Cpid* m_pLateralPID;
 
  // the reference trajectory we're following
  CTraj m_traj;

	// these are the indices into the desired Yerror and Aerror arrays
	int m_YerrorIndex, m_AerrorIndex;

	// whether we're feeding back on yaw error
	bool m_AerrorYaw;

	// whether we want to use the lateral feed forward term
	bool m_bUseLateralFF;

	// m_refIndex stores the index of the closest (spatially) point on the
	// reference traj (reference traj is REAR-based) to the rear axle of our
	// vehicle.
  int m_refIndex;

	// These contain the state of the front and rear axles of the VEHICLE when
	// asked for control inputs. Angle is either the direction the point in
	// question is moving (if front or rear) or the yaw. Thus arg(angle) at the
	// back is yaw and arg(angle) at the front is yaw+steeringangle
	double m_vehN[2];
	double m_vehE[2];
	double m_vehDN[2];
	double m_vehDE[2];
	double m_vehAngle[2];

	// These contain the state of the front and rear axles of the REFERENCE when
	// asked for control inputs. Angle is either the direction the point in
	// question is moving (if front or rear) or the yaw. Thus arg(angle) at the
	// back is yaw and arg(angle) at the front is yaw+steeringangle
	double m_refN[2];
	double m_refE[2];
	double m_refDN[2];
	double m_refDE[2];
	double m_refDDN_REAR;
	double m_refDDE_REAR;
	double m_refAngle[2];
	double m_yawdot;


  double m_Yerror[2];   // y-error (meters)
  double m_Aerror[2];   // heading-error (rad)
  double m_Cerror;      // combined-error
  double m_Verror;      // speed-error (m/s)

  double m_steerCmd;    // steering command (rad)
  double m_accelCmd;    // acceleration command (m/s^2)
  double m_oldSteerCmd; // previous steering command (rad)

  // Feedback and feedforward terms for lateral and speed control
  double m_SteerFB; // steering control signal from feedback component of controller (rad)
  double m_SteerFF; // steering control signal from feedforward component of controller (rad)

  double m_AccelFB; // acceleration control signal from feedback component of controller (m/s^2)
  double m_AccelFF; // acceleration control signal from feedforward component of controller (m/s^2)

	// the reference and actual speeds
	double m_refSpeed;
	double m_trajSpeed;
	double m_actSpeed;

	// random files
  //outputs file in format: yerror, aerror, cerror (adds line every time compute_CError is called)
  ofstream m_outputError;
  ofstream m_outputVelocity;
  //outputs file in format: n_act, e_act, index, n_des, e_des
  ofstream m_outputState2;
  ofstream m_outputVars;
  //outputs file in format: steerCmd, accelCmd
  ofstream m_outputCommands;
  //output file meant for testing purposes. it's content will change
  ofstream m_outputTest;



  void setup_Output();

  void getClosestIndex();
	void compute_FrontAndRearState_veh(VehicleState* pState);
	void compute_FrontAndRearState_ref();


  void compute_YError();
  void compute_AError();
  void compute_CError();

  void compute_SteerCmd();
	void compute_SteerFF();

  double DFE(double cmd, double old);


  void compute_trajSpeed();
  void compute_actSpeed();
  void compute_refSpeed();

  void compute_AccFF();


public:
  CPID_Controller(EYerrorside yerrorside = YERR_FRONT,
									EAerrorside aerrorside = AERR_YAW,
									bool bUseLateralFF = true);

  ~CPID_Controller();

  // set control values
  void getControlVariables(VehicleState *state, double *accel, double *phi);

  // sets new path
  void SetNewPath(CTraj* path);


  /** Use this to check the internal variable corresponding to error in heading */
  double access_AError()    { return m_Aerror[m_AerrorIndex]; }

  /** Use this to check the internal variable corresponding to perpendicular error from path */
  double access_YError()    { return m_Yerror[m_YerrorIndex];}

  /** Use this to check the internal variable corresponding to combined error.  */
  double access_CError()    { return m_Cerror; }

  /** Use this to check the internal variable corresponding to speed error. */
  double access_VError()    { return m_Verror; }

  double access_AccelFF()   { return m_AccelFF;}
  double access_AccelFB()   { return m_AccelFB;}
  double access_AccelCmd()  { return m_accelCmd;}
  
  double access_SteerFF()   { return m_SteerFF;}
  double access_SteerFB()   { return m_SteerFB;}
  double access_SteerCmd()  { return m_steerCmd;}

  double access_VRef()      { return m_refSpeed; }
  double access_VTraj()     { return m_trajSpeed; }
};


#endif
