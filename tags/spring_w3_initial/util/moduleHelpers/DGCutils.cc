#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <iostream>
using namespace std;

void DGCgettime(unsigned long long& DGCtime)
{
	timeval tv;
	gettimeofday(&tv, NULL);

	DGCtime = (unsigned long long)tv.tv_usec + 1000000ULL * tv.tv_sec;
}

double DGCtimetosec(unsigned long long& DGCtime)
{
	return ((double)DGCtime)/1.0e6;
}


bool DGCcreateMutex(pthread_mutex_t* pMutex)
{
	if(pthread_mutex_init(pMutex, NULL) != 0)
	{
		cerr << "Couldn't create mutex" << endl;
		return false;
	}
	return true;
}

bool DGCdeleteMutex(pthread_mutex_t* pMutex)
{
	if(pthread_mutex_destroy(pMutex) != 0)
	{
		cerr << "Couldn't destroy mutex" << endl;
		return false;
	}
	return true;
}

bool DGClockMutex(pthread_mutex_t* pMutex)
{
	if(pthread_mutex_lock(pMutex) != 0)
	{
		cerr << "Couldn't lock mutex" << endl;
		return false;
	}
	return true;
}

bool DGCunlockMutex(pthread_mutex_t* pMutex)
{
	if(pthread_mutex_unlock(pMutex) != 0)
	{
		cerr << "Couldn't unlock mutex" << endl;
		return false;
	}
	return true;
}

bool DGCcreateCondition(pthread_cond_t* pCondition)
{
	if(pthread_cond_init(pCondition, NULL) != 0)
	{
		cerr << "Couldn't create condition" << endl;
		return false;
	}
	return true;
}

bool DGCdeleteCondition(pthread_cond_t* pCondition)
{
	if(pthread_cond_destroy(pCondition) != 0)
	{
		cerr << "Couldn't delete condition" << endl;
		return false;
	}
	return true;
}

bool DGCWaitForConditionTrue(bool& bCond, pthread_cond_t& cond, pthread_mutex_t& mutex)
{
	DGClockMutex(&mutex);
// 	cerr << "while" << endl;
  while(!bCond)
	{
		pthread_cond_wait(&cond, &mutex);
	}
	DGCunlockMutex(&mutex);

	return true;
}

bool DGCSetConditionTrue(bool& bCond, pthread_cond_t& cond, pthread_mutex_t& mutex)
{
	DGClockMutex(&mutex);
	bCond = true;
// 	cerr << "signalling" << endl;
	pthread_cond_signal(&cond);
	DGCunlockMutex(&mutex);

	return true;
}
