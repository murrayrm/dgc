#ifndef _STATECLIENT_H_
#define _STATECLIENT_H_

#include "SkynetContainer.h"
#include "VehicleState.hh"
#include <pthread.h>
#include <unistd.h>

class CStateClient : virtual public CSkynetContainer
{
	/*! The skynet sockets for the communication with state. m_reqstatesocket is
	**	for sending a single byte to the state server to request the state sent
	**	back on the m_statesocket */
	int m_reqstatesocket;
	int m_statesocket;

	/*! The mutex and condition necessary to block on state being received. These
		are used by the UpdateState function make sure a state was received */
	pthread_mutex_t	m_gotStateConditionMutex;
	pthread_cond_t	m_gotStateCondition;

	/*! The gotState condition variable. true if we got a state after we requested
		one */
	bool m_bGotState;

	/*! This is the state that is received from skynet. This is actually locked
		and received. UpdateState() copies this data into m_state */
  VehicleState		m_receivedState;
	/*! The mutex to protect the state */
	pthread_mutex_t	m_receivedStateMutex;
	pthread_mutex_t	m_stateBufferMutex;

	/*! The function which continually updates the state when any is received from
		the network. It also sets the m_bGotState variable and signals this
		condition */
	void getStateThread();
    void state_Interpolate(double,double,double,double,unsigned long long, unsigned long long, unsigned long long,double&,double&,double&);

    VehicleState   state_buffer[100];
    int state_index;
    int full_state;

protected:
	/*! The state itself. This is the copy of the state data that's coming
		in. This is visible from the derived classes */
  VehicleState m_state;

public:
	CStateClient();
	~CStateClient();

	void UpdateState(void);
	void UpdateState(unsigned long long);
};

#endif // _STATECLIENT_H_
