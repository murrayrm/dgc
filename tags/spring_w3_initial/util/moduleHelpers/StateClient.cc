#include "StateClient.h"
#include "DGCutils"
#include <iostream>
using namespace std;

CStateClient::CStateClient()
{
	// initialize the mutexes and the condition variable
	DGCcreateMutex(&m_receivedStateMutex);
	DGCcreateMutex(&m_gotStateConditionMutex);
	DGCcreateMutex(&m_stateBufferMutex);
	DGCcreateCondition(&m_gotStateCondition);

	// open the send socket for the state request and listen for state from everybody.
	m_reqstatesocket = m_skynet.get_send_sock(SNreqstate);
	m_statesocket    = m_skynet.listen(SNstate, ALLMODULES);

	// initialize the gotstate variable to true because we don't start out waiting
	// for a state. This only happens when we request a state
	m_bGotState = true;

	// start the thread that continually gets the state

	for (int i = 0;i < 100;i++)
	{
	        state_buffer[i].Timestamp = 0;
	        state_buffer[i].Northing = 0;
	        state_buffer[i].Easting = 0;
	        state_buffer[i].Altitude = 0;
	        state_buffer[i].Vel_N = 0;
	        state_buffer[i].Vel_E = 0;
	        state_buffer[i].Vel_D = 0;
	        state_buffer[i].Acc_N = 0;
	        state_buffer[i].Acc_E = 0;
	        state_buffer[i].Acc_D = 0;
	        state_buffer[i].Roll = 0;
	        state_buffer[i].Pitch = 0;
	        state_buffer[i].Yaw = 0;
	        state_buffer[i].RollAcc = 0;
	        state_buffer[i].PitchAcc = 0;
	        state_buffer[i].YawAcc = 0;
	}
	memcpy(&m_state, &(state_buffer[0]), sizeof(m_state));

	state_index = 0;
	full_state = 0;

	DGCstartMemberFunctionThread(this, &CStateClient::getStateThread);

	while (full_state == 0) {
		cerr << "Waiting for state data to fill..." << endl;
		sleep(1);
	}
}

CStateClient::~CStateClient()
{
	DGCdeleteMutex(&m_receivedStateMutex);
	DGCdeleteMutex(&m_stateBufferMutex);
	DGCdeleteMutex(&m_gotStateConditionMutex);
	DGCdeleteCondition(&m_gotStateCondition);
}

void CStateClient::state_Interpolate(double val1, double val1_dot, double val2, double val2_dot, unsigned long long time1, unsigned long long time2, unsigned long long timereq, double& interp, double& interp_dot, double& interp_dot_dot)
{
    double a = val1;
    double b = val1_dot;
    double c = -3*val1 - 2*val1_dot + 3*val2 - 1*val2_dot;
    double d = 2*val1 + 1*val1_dot - 2*val2 + 1*val2_dot;
    double scale = 1000000 / (time2 - time1);
    cout << "Scale: " << scale << endl;
    double t = (timereq - time1) / (time2 - time1);
    interp = (a + b*t + c*pow(t,2) + d*pow(t,3));
    interp_dot = (b * 2*c*t + 3*d*pow(t,2)) * scale;
    interp_dot_dot = (2*c + 6*d*t) * pow(scale,2);
}

void CStateClient::UpdateState()
{
	DGClockMutex(&m_receivedStateMutex);
	DGClockMutex(&m_stateBufferMutex);
	memcpy(&m_state, &(state_buffer[state_index]), sizeof(m_state));
	DGCunlockMutex(&m_stateBufferMutex);
	DGCunlockMutex(&m_receivedStateMutex);
}

void CStateClient::UpdateState(unsigned long long time_req)
{
    int ind = state_index - 1;
    
	DGClockMutex(&m_stateBufferMutex);
    while (time_req < state_buffer[ind].Timestamp)
    {
        ind --;
        if (ind < 0)
            ind = 99;

        if (ind == state_index) {
            ind = state_index + 1;
    	    if (ind == 100) ind = 0;
            break;
        }
    }
    int ind_u = ind + 1;
    if (ind_u == 100) ind_u = 0;

    VehicleState vl = state_buffer[ind];
    VehicleState vu = state_buffer[ind_u];

	DGCunlockMutex(&m_stateBufferMutex);
    
	DGClockMutex(&m_receivedStateMutex);

    state_Interpolate(vl.Northing, vl.Vel_N, 
                vu.Northing, vu.Vel_N, 
                vl.Timestamp, vu.Timestamp, time_req,
                m_state.Northing, m_state.Vel_N, m_state.Acc_N);

    state_Interpolate(vl.Easting, vl.Vel_E, 
                vu.Easting, vu.Vel_E, 
                vl.Timestamp, vu.Timestamp, time_req,
                m_state.Easting, m_state.Vel_E, m_state.Acc_E);

    state_Interpolate(vl.Altitude, vl.Vel_D, 
                vu.Altitude, vu.Vel_D, 
                vl.Timestamp, vu.Timestamp, time_req,
                m_state.Altitude, m_state.Vel_D, m_state.Acc_D);

    state_Interpolate(vl.Roll, vl.RollRate, 
                vu.Roll, vu.RollRate, 
                vl.Timestamp, vu.Timestamp, time_req,
                m_state.Roll, m_state.RollRate, m_state.RollAcc);

    state_Interpolate(vl.Pitch, vl.PitchRate, 
                vu.Pitch, vu.PitchRate, 
                vl.Timestamp, vu.Timestamp, time_req,
                m_state.Pitch, m_state.PitchRate, m_state.PitchAcc);

    state_Interpolate(vl.Yaw, vl.YawRate, 
                vu.Yaw, vu.YawRate, 
                vl.Timestamp, vu.Timestamp, time_req,
                m_state.Yaw, m_state.YawRate, m_state.YawAcc);

    m_state.Timestamp = time_req;

	DGCunlockMutex(&m_receivedStateMutex);
}

void Interpolate(double val1, double val1_dot, unsigned long long time1, double val2, double val2_dot, unsigned long long time2, unsigned long long timereq, double& interp, double& interp_dot, double& interp_dot_dot)
{
    double a = val1;
    double b = val1_dot;
    double c = -3*val1 - 2*val1_dot + 3*val2 - 1*val2_dot;
    double d = 2*val1 + 1*val1_dot - 2*val2 + 1*val2_dot;
    double scale = (time2 - time1);
    double t = (timereq - time1) / scale;
    interp = (a + b*t + c*pow(t,2) + d*pow(t,3));
    interp_dot = (b * 2*c*t + 3*d*pow(t,2)) / scale;
    interp_dot_dot = (2*c + 6*d*t) / pow(scale,2);
}

void CStateClient::getStateThread()
{
	while(true)
	{
// 		cerr << "GETTING STATE" << endl;
		if(m_skynet.get_msg(m_statesocket, &m_receivedState, sizeof(m_receivedState), 0, &m_receivedStateMutex) != sizeof(m_receivedState))
			cerr << "Didn't receive the right number of bytes in the state structure" << endl;
       
	    DGClockMutex(&m_stateBufferMutex);

        state_index++; 
        if (state_index == 100)
        {
            state_index = 0;
	    full_state = 1;
        }

        memcpy(&(state_buffer[state_index]), &m_receivedState, sizeof(m_state));
	    DGCunlockMutex(&m_stateBufferMutex);
// 		cerr << "GOT STATE" << endl;

    //cerr << "received state" << endl;
//		DGCSetConditionTrue(m_bGotState, m_gotStateCondition, m_gotStateConditionMutex);
	}
}
