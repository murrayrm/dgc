#include <math.h>

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
using namespace std;
#include "pid.hh"
#include "DGCutils"

// Default constructor
Cpid::Cpid(double Kp, double Ki, double Kd, double sat)
  : m_Kp(Kp), m_Ki(Ki), m_Kd(Kd), m_integralLimit(sat)
{
	m_pParamFile = NULL;

  for( int i=0; i < ERROR_SIGNAL_LENGTH; i++ )
  {
    m_errors[i] = 0.0;
    m_stamps[i] = 0.0;
  }
  m_integralError = 0.0;
}

Cpid::Cpid(char* pParamFile)
{
	m_pParamFile = pParamFile;
	m_paramReadCounter = 0;

  ifstream paramFile(m_pParamFile);
  if(!paramFile)
  {
    cerr << "COULDN'T READ PID PARAMETER FILE" << endl;
    exit(1);
  }
}

void Cpid::reset(void)
{
  for( int i=0; i < ERROR_SIGNAL_LENGTH; i++ )
  {
    m_errors[i] = 0.0;
    m_stamps[i] = 0.0;
  }
  m_integralError = 0.0;
}

double Cpid::step_forward(double error)
{
	// read in from a parameter file if we need to
	if(m_pParamFile!=NULL && m_paramReadCounter--)
	{
		m_paramReadCounter = PARAM_READ_INTERVAL;
		ifstream paramFile(m_pParamFile);
		if(!paramFile)
		{
			cerr << "COULDN'T READ PID PARAMETER FILE" << endl;
			exit(1);
		}

		paramFile >> m_Kp >> m_Ki >> m_Kd >> m_integralLimit;
	}


  // First shift the error signal history
  for( int i=ERROR_SIGNAL_LENGTH-1; i>0; i-- )
  {
    m_errors[i] = m_errors[i-1];
    m_stamps[i] = m_stamps[i-1];
  }
  // Fill in the latest signal
  m_errors[0] = error;
  // Get the time stamp for this
  DGCgettime(m_time);
  //cout<<"time...: "<<m_time<<endl;
  m_stamps[0] = DGCtimetosec(m_time);

  // Make sure that the initial case doesn't blow this up
  if( m_stamps[1] < 1000.0 ) 
  { 
    m_stamps[1] = 0.0; 
    m_integralError = 0.0;
  }
	else
	{
		// Calculate the integral (forward difference)
		m_integralError += m_errors[0] * (m_stamps[0] - m_stamps[1]);
	}

  // DEBUG
  //printf("m_errors[0] = %f, m_integralError = %f\n", m_errors[0], m_integralError);
  //printf("m_stamps[0] = %f, m_stamps[1] = %f\n", m_stamps[0], m_stamps[1]);
  //printf("m_stamps[1] - m_stamps[0] = %f\n", m_stamps[1] - m_stamps[1]);

  // Saturate the integral to assure that -m_integralLimit <= m_integralError <= m_integralLimit
  m_integralError=fmin(m_integralLimit,fmax(m_integralError,-m_integralLimit));

  // Calculate the derivative (initialize to zero)
  double errorDeriv = 0.0;
  // Assume that the bandwidth we are interested in is on the timescale of
  // about 10 Hz.  We care about the derivative over ~100 ms.
  for( int i = 0; i < ERROR_SIGNAL_LENGTH; i++ )
  {
      //printf("m_stamps[i] = %f\n",m_stamps[i]);
   
     // Look far enough back to get a decent sample time
    if( fabs(m_stamps[i] - m_stamps[0]) > 0.100 && m_stamps[i] > 1000.0 )
    {
      //printf("error diff = %f\n",m_errors[i] - m_errors[0]);
      //printf("time diff = %f\n",m_stamps[i] - m_stamps[0]);

      errorDeriv = (m_errors[i] - m_errors[0])/(m_stamps[i] - m_stamps[0]);
      break;
    }
  }

  // Return the sum of the three terms
  //printf("m_errors[0] = %f, m_integralError = %f, errorDeriv = %f\n", 
  //        m_errors[0], m_integralError, errorDeriv );

  //cout<<"errors calculated (p,i,d): "<<m_errors[0]<<' '<<m_integralError<<' '<<errorDeriv<<endl;
  //cout<<"gains used (p,i,d): "<<m_Kp<<' '<<m_Ki<<' '<<m_Kd<<endl;
  //cout<<"CPID OUTPUT: "<< m_Kp * m_errors[0] + m_Ki * m_integralError + m_Kd * errorDeriv<<endl;

	return m_Kp * m_errors[0] + m_Ki * m_integralError + m_Kd * errorDeriv;
}

// Deconstructor
Cpid::~Cpid()
{
}
