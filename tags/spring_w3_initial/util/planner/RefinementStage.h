#ifndef _REFINEMENTSTAGE_H_
#define _REFINEMENTSTAGE_H_

#include "PlannerStage.h"
#include "AliceConstants.h"

class CRefinementStage : public CPlannerStage
{
	double*   m_pColl_speed;
	double*   m_pColl_dspeed;

	double*   m_pOutputSpeed;
	double*   m_pOutputDSpeed;

	bool			m_bGetVProfile;

	void NonLinearInitConstrFunc(void);
	void NonLinearTrajConstrFunc(int constr_index_snopt, int collIndex);
	void NonLinearFinlConstrFunc(void);
	void makeCollocationData(double* pSolverState);
	void getSplineCoeffsSpeed(double* pSolverState);

public:
	void outputTraj(CTraj* pTraj);
	void SetUpLinearConstr(void);
	void SetUpPlannerBounds(VehicleState *pState);
	void makeSNOPTBoundsArrays(void);
	void SetUpPlannerBoundsAndSeedCoefficients(double* pPrevState, VehicleState* pState);
	void funcConstr(void);
	void funcCost  (void);

	CRefinementStage(CMap *pMap, int mapLayerID, RDDF* pRDDF, bool USE_MAXSPEED, bool bGetVProfile = false);
	~CRefinementStage();

	double getLength(void)
	{
		return m_pSolverState[NPnumVariables-1];
	}
};

#endif // _REFINEMENTSTAGE_H_
