N  = 8; # how many variables represent this spline
M1 = 50;  # how many collocation points in each spline segment
M2 = 20;  # how many collocation points in each spline segment
MFINE = 100;


## these variables are as described on the mathworld B-spline page
#p = 3; # quadratic B-splines
#n = N-1;
#m = p + n + 1;

## the knots vector
#knots = [zeros(p, 1); linspace(0, 1, m - 2*p + 1)'; ones(p, 1)];

#midKnot0 = p+1;
#midKnot1 = m-p-1;
#numsegments = midKnot1 - midKnot0 + 2;

## now compute the basis functions: (polynomial coeff + numcoeffs*(which segment), order)
## segment indices go from p to n. 
#bas = zeros((p+1)*numsegments, p+1);
#bas(1:(p+1):((p+1)*numsegments), 1) = 1;

#for j=2:(p+1)  # orders of the bases
#	for i=midKnot0:(midKnot1+1)    # internal knots
#		if(norm(bas( ((i - midKnot0    )*(p+1) + 1):((i - midKnot0    )*(p+1) + p), j-1)) != 0.0)
#			bas( ((i - midKnot0)*(p+1) + 2):((i - midKnot0)*(p+1) + 1+p), j) += bas( ((i - midKnot0    )*(p+1) + 1):((i - midKnot0    )*(p+1) + p), j-1) / (knots(i+p  ) - knots(i  ));
#		end

#		if(i - midKnot0 + 1 < numsegments && norm(bas( ((i - midKnot0 + 1)*(p+1) + 1):((i - midKnot0 + 1)*(p+1) + p), j-1)) != 0.0)
#			bas( ((i - midKnot0)*(p+1) + 2):((i - midKnot0)*(p+1) + 1+p), j) -= bas( ((i - midKnot0 + 1)*(p+1) + 1):((i - midKnot0 + 1)*(p+1) + p), j-1) / (knots(i+p+1) - knots(i+1));
#		end

#		if(norm(bas( ((i - midKnot0    )*(p+1) + 1):((i - midKnot0    )*(p+1) + p), j-1)) != 0.0)
#			bas( ((i - midKnot0)*(p+1) + 1):((i - midKnot0)*(p+1) +   p), j) -= bas( ((i - midKnot0    )*(p+1) + 1):((i - midKnot0    )*(p+1) + p), j-1) / (knots(i+p  ) - knots(i  )) * knots(i);
#		end

#		if(i - midKnot0 + 1 < numsegments && norm(bas( ((i - midKnot0 + 1)*(p+1) + 1):((i - midKnot0 + 1)*(p+1) + p), j-1)) != 0.0)
#			bas( ((i - midKnot0)*(p+1) + 1):((i - midKnot0)*(p+1) +   p), j) += bas( ((i - midKnot0 + 1)*(p+1) + 1):((i - midKnot0 + 1)*(p+1) + p), j-1) / (knots(i+p+1) - knots(i+1)) * knots(i+p+1);
#		end

#	end
#end
#break;











#v     theta
#a     dtheta
#jerk  ddtheta


# these are in the constraint order:
# N-1 left  d1 value
# N-1 right d1 value
# N-2 val continuity
# 1 initial condition (val)
#---------
# 3N-3


# a * allpolynomialcoeffs = b = W * y
# y = [d1; v(0)]
a = zeros(3*(N-1), 3*(N-1));

# go through the 1st derivative points
for k=1:(N-1)
	a(k,:)     = (N-1) * [zeros(1,3*(k-1)) 0 1 0 zeros(1,3*(N-k-1))];
	a(k+N-1,:) = (N-1) * [zeros(1,3*(k-1)) 0 1 2 zeros(1,3*(N-k-1))];
end

# continuous value
for k=1:(N-2)
	a(2*N-2+k,:) += [zeros(1,3*(k-1)) 1 1 1 zeros(1,3*(N-k-1))];
	a(2*N-2+k,:) -= [zeros(1,3*k)     1 0 0 zeros(1,3*(N-k-2))];
end

# val at the beginning
a(3*N-3,:) = [1 0 0 zeros(1,3*(N-2))];


M = M1;
filename = "dercoeffs1.dat";
mkdercoeffs_writecollocationdata;

M = M2;
filename = "dercoeffs2.dat";
mkdercoeffs_writecollocationdata;

M = MFINE;
filename = "dercoeffsfine.dat";
mkdercoeffs_writecollocationdata;

v = valcoeffs(:,1:N);
fil = fopen("stateestimatematrix.dat", "w+b", "ieee-le");
fwrite(fil, inv(v'*v)*v', "double");
fclose(fil);

# write out the data. don't include the last column (offset), since it's trivial
dercoeffmatrix = inv(a) * W;
fil = fopen("dercoeffsmatrix.dat", "w+b", "ieee-le");
fwrite(fil, dercoeffmatrix(:,1:N) , "double");
fclose(fil);

