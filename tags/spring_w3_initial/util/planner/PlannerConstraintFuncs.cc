#include "PlannerStage.h"

// #define NEW_INTEGRATE_NE

// for simpler access
// NOTE: n,e here are not scaled by Sf
#define theta						m_pColl_theta  [collIndex]
#define dtheta					m_pColl_dtheta [collIndex]
#define ddtheta					m_pColl_ddtheta[collIndex]
#define sf							m_collSF
#define n								m_pCollN[collIndex]
#define e								m_pCollE[collIndex]
#define vlimit          m_pCollVlimit[collIndex]
#define dvldN           m_pCollDVlimitDN[collIndex]
#define dvldE           m_pCollDVlimitDE[collIndex]

#define DO_BENCHMARK

#ifdef DO_BENCHMARK
#include "DGCutils"
extern int numinterpblurs;
extern unsigned long long integrateNEtime;
extern unsigned long long blurtime;
#endif

void CPlannerStage::makeCollocationData(double* pSolverState)
{
	int i;

	getSplineCoeffsTheta(pSolverState);

	char notrans = 'N';
	int rows = NUM_COLLOCATION_POINTS;
	int cols = NUM_POINTS;
	double alpha = 1.0;
	double beta = 0.0;
	int incr = 1;

	// make theta, dtheta, ddtheta
#warning "do we need ALL the derivatives?"
	dgemv_(&notrans, &rows, &cols, &alpha, m_valcoeffs, &rows,
				 pSolverState, &incr, &beta, m_pColl_theta, &incr);
	dgemv_(&notrans, &rows, &cols, &alpha, m_d1coeffs, &rows,
				 pSolverState, &incr, &beta, m_pColl_dtheta, &incr);
	dgemv_(&notrans, &rows, &cols, &alpha, m_d2coeffs, &rows,
				 pSolverState, &incr, &beta, m_pColl_ddtheta, &incr);

	for(i=0; i<NUM_COLLOCATION_POINTS; i++)
		m_pColl_theta[i] += m_initTheta;


	m_collSF = pSolverState[NPnumVariables-1];

	// integrate through to get N,E and their gradients, as well as
	// vlimit(N,E) and its gradients. First, set up the variables for
	// the integration

	int collIndex = 0;

#warning "these should be hardcoded"
	n = e = 0.0;
	memset(m_pCollGradN, 0, NUM_POINTS*sizeof(m_pCollGradN[0]));
	memset(m_pCollGradE, 0, NUM_POINTS*sizeof(m_pCollGradE[0]));

	int gradVlimitIndex = 0;
	for(;
			collIndex<NUM_COLLOCATION_POINTS;
			collIndex++)
	{
		getContinuousMapValueDiff(n*sf, e*sf,
															&vlimit, &dvldN, &dvldE);

		for(i=0; i<NUM_POINTS; i++)
		{
			m_pCollGradVlimit[gradVlimitIndex++] =
				sf * (dvldN * m_pCollGradN[i + NUM_POINTS*collIndex] +
							dvldE * m_pCollGradE[i + NUM_POINTS*collIndex]);
		}
		m_pCollGradVlimit[gradVlimitIndex++] = dvldN*n + dvldE*e;

		// integrate the (N,E) values, unless we're already at the end
		if(collIndex != NUM_COLLOCATION_POINTS-1)
			integrateNE(collIndex);
	}
}

void CPlannerStage::getContinuousMapValueDiff(double UTMNorthing, double UTMEasting,
																							double *f, double *dfdN, double *dfdE)
{
	UTMNorthing += m_initN;
	UTMEasting  += m_initE;

	int nearestSWCornerRow, nearestSWCornerCol;
	int row, col, matrixidx;

	// Find the R/C coords of the cell whose SW corner is nearest to the given (N,E)
	m_pMap->UTM2Win(UTMNorthing + m_pMap->getResRows() / 2.0,
									UTMEasting  + m_pMap->getResCols() / 2.0,
									&nearestSWCornerRow, &nearestSWCornerCol);

	// middleCellN, middleCellE are the UTM coords of the corner nearest to UTMNorthing, UTMEasting
	// (rescaledN, rescaledE) is now [0,1] from the mid of the SW cell to the mid of the NE cell
	double middleCellN, middleCellE;
	m_pMap->Win2UTM(nearestSWCornerRow, nearestSWCornerCol, &middleCellN, &middleCellE);
	double rescaledN = (UTMNorthing - (middleCellN - m_pMap->getResRows()/2.0)) / m_pMap->getResRows();
	double rescaledE = (UTMEasting  - (middleCellE - m_pMap->getResCols()/2.0)) / m_pMap->getResCols();

#ifdef DO_BENCHMARK
	unsigned long long t1, t2;
	DGCgettime(t1);
#endif

	// At this point we sample the data in a window around (nearestSWCornerRow,
	// nearestSWCornerCol). Then we apply a gaussian blur to this window 4 times
	// to get the blurred value of the map at the 4 cells around our point (the
	// blur is done with a matrix multiplication). We then apply a bilinear
	// interpolation to compute the actual value of the map here. The
	// sampling/blurring (mostly sampling) of the map is a computational
	// bottleneck. We thus cache precomputed sampling/blurring results and use
	// those if we can.
	double& cachedBlurSW = m_pMap->getDataWin<double>(m_blurLayer, nearestSWCornerRow-1, nearestSWCornerCol-1);
	double& cachedBlurS  = m_pMap->getDataWin<double>(m_blurLayer, nearestSWCornerRow-1, nearestSWCornerCol  );
	double& cachedBlurW  = m_pMap->getDataWin<double>(m_blurLayer, nearestSWCornerRow  , nearestSWCornerCol-1);
	double& cachedBlur   = m_pMap->getDataWin<double>(m_blurLayer, nearestSWCornerRow  , nearestSWCornerCol  );

	double blurredwindow[4] = {0.0, 0.0, 0.0, 0.0};

	// if any one of the corners we need has invalid data, compute everything
	if(cachedBlurSW < 0.0 || cachedBlurS < 0.0 || cachedBlurW < 0.0 || cachedBlur < 0.0)
	{
		// zero out the blurring result
		memset(blurredwindow, 0, 4*sizeof(blurredwindow[0]));

		// these double loops can't be trivially optimized, since the map could wrap in memory (it's a torus).
		matrixidx = 0;
		for(col = nearestSWCornerCol-MAP_BLURINTERP_DIAM_CELLS/2;
				col < nearestSWCornerCol+MAP_BLURINTERP_DIAM_CELLS/2;
				col += MAP_BLURINTERP_STEP)
		{
			for(row = nearestSWCornerRow-MAP_BLURINTERP_DIAM_CELLS/2;
					row < nearestSWCornerRow+MAP_BLURINTERP_DIAM_CELLS/2;
					row += MAP_BLURINTERP_STEP)
			{
				double mapval = m_pMap->getDataWin<double>(m_mapLayer, row, col);
				blurredwindow[0] += mapval * m_mapblurcoeffs[matrixidx*4    ];
				blurredwindow[1] += mapval * m_mapblurcoeffs[matrixidx*4 + 1];
				blurredwindow[2] += mapval * m_mapblurcoeffs[matrixidx*4 + 2];
				blurredwindow[3] += mapval * m_mapblurcoeffs[matrixidx*4 + 3];
				matrixidx++;
			}
		}

		// if the cached values were NODATA, set them
		if(cachedBlurSW == -2.0) cachedBlurSW = blurredwindow[0];
		if(cachedBlurW  == -2.0) cachedBlurW  = blurredwindow[1];
		if(cachedBlurS  == -2.0) cachedBlurS  = blurredwindow[2];
		if(cachedBlur   == -2.0) cachedBlur   = blurredwindow[3];
	}
	else
	{
		// read in the cached values
		blurredwindow[0] = cachedBlurSW;
		blurredwindow[1] = cachedBlurW;
		blurredwindow[2] = cachedBlurS;
		blurredwindow[3] = cachedBlur;
	}


#ifdef DO_BENCHMARK
	DGCgettime(t2);
	blurtime += t2-t1;
	numinterpblurs++;
#endif

	if(f != NULL)
	{
		*f =
			blurredwindow[0] * ((1.0 - rescaledN) * (1.0 - rescaledE)) +
			blurredwindow[1] * (rescaledN * (1.0 - rescaledE)) +
			blurredwindow[2] * ((1.0 - rescaledN) * rescaledE) +
			blurredwindow[3] * (rescaledN * rescaledE);
	}
	if(dfdN != NULL)
	{
		*dfdN =
			(blurredwindow[0] * (rescaledE - 1.0) +
			 blurredwindow[1] * (1.0 - rescaledE) +
			 blurredwindow[2] * (-rescaledE) +
			 blurredwindow[3] * (rescaledE)) / m_pMap->getResRows();
		*dfdE =
			(blurredwindow[0] * (rescaledN - 1.0) +
			 blurredwindow[1] * (-rescaledN) +
			 blurredwindow[2] * (1.0 - rescaledN) +
			 blurredwindow[3] * (rescaledN)) / m_pMap->getResCols();
	}

	if(m_USE_MAXSPEED)
	{
		if(f != NULL)
			*f = (*f) * (MAXSPEED - MIN_V) + MIN_V;
		if(dfdN != NULL)
		{
			*dfdN *= MAXSPEED - MIN_V;
			*dfdE *= MAXSPEED - MIN_V;
		}
	}
}

void CPlannerStage::getSplineCoeffsTheta(double* pSolverState)
{
	char notrans = 'N';
	int rows = (NUM_POINTS-1) * 3; // number of polynomial coefficients
	int cols = NUM_POINTS;         // number of spline-defining variables
	double alpha = 1.0;
	double beta = 0.0;
	int incr = 1;

	dgemv_(&notrans, &rows, &cols, &alpha, m_dercoeffsmatrix, &rows,
				 pSolverState, &incr, &beta, m_splinecoeffsTheta, &incr);

	for(int i=0; i<(NUM_POINTS-1); i++)
		m_splinecoeffsTheta[3*i] += m_initTheta;
}

void CPlannerStage::integrateNE(int collIndex)
{
#ifdef DO_BENCHMARK
	unsigned long long t1, t2;
	DGCgettime(t1);
#endif

#ifdef NEW_INTEGRATE_NE
	int i;
	int splinecoeffsstart = 3 * (collIndex / COLLOCATION_FACTOR);

	double a  = m_splinecoeffsTheta[splinecoeffsstart];
	double b  = m_splinecoeffsTheta[splinecoeffsstart + 1];
	double c  = m_splinecoeffsTheta[splinecoeffsstart + 2];

	double s0 = (double)(collIndex % COLLOCATION_FACTOR)/(double)(COLLOCATION_FACTOR);
	double di = 1.0 / (double)(COLLOCATION_FACTOR);
	double dicubed = di*di*di;
	double s1 = s0 + di;

	double x0_plus_a = 0.5 * (s0*(b+c*s0) + s1*(b+c*s1)) + a;
	double cos_x0_plus_a = cos(x0_plus_a);
	double sin_x0_plus_a = sin(x0_plus_a);

	double val = 7.0 * (s0*s0 + s1*s1) + 6.0*s0*s1;
	double small = c * dicubed / 6.0;
	double big   = 1.0 - dicubed/120.0 * ( c*c* val + 5.0*b* (2.0*c*(s1 + s0) + b) );

	double dsmall_dc =   dicubed/6.0;
	double dbig_db   = - dicubed/12.0 * (c*(s1 + s0) + b );
	double dbig_dc   = - dicubed/60.0 * (c*val + 5.0*b*(s1 + s0) );
	double dx0_plus_a_da = 1.0;
	double dx0_plus_a_db = 0.5*(s0 + s1);
	double dx0_plus_a_dc = 0.5*(s0*s0 + s1*s1);



	double delN = (cos_x0_plus_a*big + sin_x0_plus_a*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));
	double delE = (sin_x0_plus_a*big - cos_x0_plus_a*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));

	double ddelN_da = (                        - sin_x0_plus_a*big +
										                           cos_x0_plus_a*small              ) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));
	double ddelN_db = (cos_x0_plus_a*dbig_db   - sin_x0_plus_a*dx0_plus_a_db*big +
										                           cos_x0_plus_a*dx0_plus_a_db*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));
	double ddelN_dc = (cos_x0_plus_a*dbig_dc   - sin_x0_plus_a*dx0_plus_a_dc*big +
										 sin_x0_plus_a*dsmall_dc + cos_x0_plus_a*dx0_plus_a_dc*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));

	double ddelE_da = (                        + cos_x0_plus_a*big +
										                           sin_x0_plus_a*small              ) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));
	double ddelE_db = (sin_x0_plus_a*dbig_db   + cos_x0_plus_a*dx0_plus_a_db*big +
										                           sin_x0_plus_a*dx0_plus_a_db*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));
	double ddelE_dc = (sin_x0_plus_a*dbig_dc   + cos_x0_plus_a*dx0_plus_a_dc*big -
										 cos_x0_plus_a*dsmall_dc + sin_x0_plus_a*dx0_plus_a_dc*small) / (double)(COLLOCATION_FACTOR*(NUM_POINTS-1));

	m_pCollN[collIndex+1] = n + delN;
	m_pCollE[collIndex+1] = e + delE;

	double ddelN_dcoeff, ddelE_dcoeff;
	for(i=0; i<NUM_POINTS; i++)
	{
		ddelN_dcoeff = 
			ddelN_da * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3    ] +
			ddelN_db * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 1] +
			ddelN_dc * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 2];
		ddelE_dcoeff = 
			ddelE_da * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3    ] +
			ddelE_db * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 1] +
			ddelE_dc * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 2];

#warning "this could be done with a static matrixindex int"
		m_pCollGradN[i + NUM_POINTS*(collIndex+1)] = m_pCollGradN[i + NUM_POINTS*collIndex] + ddelN_dcoeff;
		m_pCollGradE[i + NUM_POINTS*(collIndex+1)] = m_pCollGradE[i + NUM_POINTS*collIndex] + ddelE_dcoeff;
	}

#else

	int i;
	int splinecoeffsstart = 3 * (collIndex / COLLOCATION_FACTOR);

	double a = m_splinecoeffsTheta[splinecoeffsstart + 0];
	double b = m_splinecoeffsTheta[splinecoeffsstart + 1];
	double c = m_splinecoeffsTheta[splinecoeffsstart + 2];
	double s1 = (double)(collIndex % COLLOCATION_FACTOR)/(double)(COLLOCATION_FACTOR);
	double diff = 1.0 / (double)(COLLOCATION_FACTOR);
	double sum = 2.0*s1 + diff;
	double smid = s1 + diff/2.0;

	double ce = smid*(smid*c + b) + a;

	double cos_ce = cos(ce);
	double sin_ce = sin(ce);

#warning ce_minus_a could be 0 if we choose ce as such
	double ce_minus_a = ce - a;

	double sumsq = sum*sum;
	double diffsq = diff*diff;
	double sumsqplusdiffsq = sumsq + diffsq;
	double binom = (sqrt(5.0)*sumsq + (sqrt(5.0) - 2.0) * diffsq) * (sqrt(5.0)*sumsq + (sqrt(5.0) + 2.0)*diffsq);

	double small_ce_minus_a_0 = -diff*(3.0*c* (c*binom + 20.0*b*sum*sumsqplusdiffsq) +
																		20.0*b*b*(3.0*sumsq+diffsq)-480.0) / 480.0;
	double small_ce_minus_a_1 = diff * (3.0*sum * (c*sum + 2.0*b) + c*diffsq) / 12.0;
	double small_ce_minus_a_2 = -diff / 2.0;

	double big_ce_minus_a_0 =
		(
		 sum * 35.0 *( sum *( sum *( sum *( sum * c*c * (c*sum +
																										 6.0*b) +
																				c * (5.0*c*c*diffsq + 12.0*b*b)) +
																 4.0*b*(5.0*c*c*diffsq + 2.0*b*b)) +
													3.0 * c*(diffsq*(c*c*diffsq+8.0*b*b)-32.0)) +
									 2.0 * b*(diffsq*(3.0*c*c*diffsq+4.0*b*b)-96.0)) +
		 c*diffsq*(diffsq*(5.0*c*c*diffsq+84.0*b*b)-1120.0)
		) * diff / 13440.0;

	// these are hardcoded
// 	double big_ce_minus_a_1 = small_ce_minus_a_0;
// 	double big_ce_minus_a_2 = small_ce_minus_a_1 / 2.0;
// 	double big_ce_minus_a_3 = small_ce_minus_a_2 / 3.0;

	double big =
		ce_minus_a *( ce_minus_a *( ce_minus_a*small_ce_minus_a_2 / 3.0 +
																small_ce_minus_a_1 / 2.0) +
									small_ce_minus_a_0) +
		big_ce_minus_a_0;

	double small =
		ce_minus_a *( ce_minus_a * small_ce_minus_a_2 +
									small_ce_minus_a_1 ) +
		small_ce_minus_a_0;

	m_pCollN[collIndex+1] = n + (cos_ce*small + sin_ce*big) / (double)(NUM_POINTS-1);
	m_pCollE[collIndex+1] = e + (sin_ce*small - cos_ce*big) / (double)(NUM_POINTS-1);


	double dsmall_ce_minus_a_0_db = -diff * (1.5*c*sum*sumsqplusdiffsq + b*(3.0*sumsq+diffsq)) / 12.0;
	double dsmall_ce_minus_a_0_dc = -diff / 80.0 * (10.0*b*sum*sumsqplusdiffsq + binom*c);
	double dsmall_ce_minus_a_1_db = 0.5*diff*sum;
	double dsmall_ce_minus_a_1_dc = diff*(3.0*sumsq + diffsq) / 12.0;
	double dbig_ce_minus_a_0_db = diff / 13440.0 * 
		(35.0*sum*(sum*(sum*(6.0*sum*c*(c*sum + 4.0*b) +
												 20.0*(c*c*diffsq + 1.2*b*b)) +
										48.0*b*c*diffsq) +
							 2.0*(diffsq*12.0*(0.25*c*c*diffsq + b*b) - 96.0)) +
		 168.0*b*c*diffsq*diffsq);
	double dbig_ce_minus_a_0_dc = diff / 13440.0 *
			  (35.0*sum*(sum*(sum*(sum*3.0*(c*sum*(c*sum + 4.0*b) +
																	5.0*c*c*diffsq + 4.0*b*b) +
														 40.0*b*c*diffsq) +
												3.0 * diffsq * (3.0*c*c*diffsq + 8.0*b*b) - 96.0) +
									 12.0*b*c*diffsq*diffsq) +
				 diffsq * (diffsq * (diffsq*15.0*c*c + 84.0*b*b) - 1120.0));


	double dsmall_da = -small_ce_minus_a_1 - 2.0*ce_minus_a*small_ce_minus_a_2;
	double dsmall_db = ce_minus_a*dsmall_ce_minus_a_1_db + dsmall_ce_minus_a_0_db;
	double dsmall_dc = ce_minus_a*dsmall_ce_minus_a_1_dc + dsmall_ce_minus_a_0_dc;
	double dbig_da   = -small_ce_minus_a_0 - ce_minus_a*small_ce_minus_a_1 - small_ce_minus_a_2*ce_minus_a*ce_minus_a;
	double dbig_db   =
		ce_minus_a *( ce_minus_a * dsmall_ce_minus_a_1_db / 2.0 +
									dsmall_ce_minus_a_0_db) +
		dbig_ce_minus_a_0_db;
	double dbig_dc   =
		ce_minus_a *( ce_minus_a * dsmall_ce_minus_a_1_dc / 2.0 +
									dsmall_ce_minus_a_0_dc) +
		dbig_ce_minus_a_0_dc;

	double dsmall_dcoeff;
	double dbig_dcoeff;
#warning "BLAS-ify this"
	for(i=0; i<NUM_POINTS; i++)
	{
		dsmall_dcoeff =
			dsmall_da * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3] +
			dsmall_db * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 1] +
			dsmall_dc * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 2];

		dbig_dcoeff =
			dbig_da * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3] +
			dbig_db * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 1] +
			dbig_dc * m_dercoeffsmatrix[splinecoeffsstart + i*(NUM_POINTS-1)*3 + 2];

#warning "this could be done with a static matrixindex int"
		m_pCollGradN[i + NUM_POINTS*(collIndex+1)] = m_pCollGradN[i + NUM_POINTS*collIndex] +
			(cos_ce * dsmall_dcoeff + sin_ce*dbig_dcoeff) /
			(double)(NUM_POINTS-1);
		m_pCollGradE[i + NUM_POINTS*(collIndex+1)] = m_pCollGradE[i + NUM_POINTS*collIndex] +
			(sin_ce * dsmall_dcoeff - cos_ce*dbig_dcoeff) /
			(double)(NUM_POINTS-1);
	}
#endif


#ifdef DO_BENCHMARK
	DGCgettime(t2);
	integrateNEtime += t2-t1;
#endif
}


void CPlannerStage::NonLinearFinlConstrFunc(void)
{
	const int constr_index_snopt = NUM_NLIN_INIT_CONSTR + NUM_COLLOCATION_POINTS*NUM_NLIN_TRAJ_CONSTR;
	const int collIndex = NUM_COLLOCATION_POINTS-1;

	if(CARTESIAN_TARGET_SPECIFICATION)
	{
		m_pConstrData[constr_index_snopt + 0] = SCALEFACTOR_DIST * (n*sf - m_targetN);
		m_pConstrData[constr_index_snopt + 1] = SCALEFACTOR_DIST * (e*sf - m_targetE);

		m_grad_solver[0][IDX_THETA]		= 0.0;
		m_grad_solver[0][IDX_DTHETA]	= 0.0;
		m_grad_solver[0][IDX_DDTHETA]	= 0.0;
		m_grad_solver[0][IDX_SF]			= SCALEFACTOR_DIST * n;
		m_grad_solver[0][IDX_N]				= SCALEFACTOR_DIST * sf;
		m_grad_solver[0][IDX_E]				= 0.0;

		m_grad_solver[1][IDX_THETA]		= 0.0;
		m_grad_solver[1][IDX_DTHETA]	= 0.0;
		m_grad_solver[1][IDX_DDTHETA]	= 0.0;
		m_grad_solver[1][IDX_SF]			= SCALEFACTOR_DIST * e;
		m_grad_solver[1][IDX_N]				= 0.0;
		m_grad_solver[1][IDX_E]				= SCALEFACTOR_DIST * sf;
	}
	else
	{
		// This constrains the final point to be within some distance of the target
		// point as well as being a certain distance from the virtual starting point
		// (defined by extending the final rddf segment back from target point
		// PLANNING_TARGET_DIST meters. final rddf segment orientation is already in
		// m_finalTheta). This constrains the final point to lie on a circular arc
		// of some limited span. The virtual starting point is used to make the arc
		// perpendicular to the corridor even when we're planning through a turn

		// squared distance from final point to the target point
		m_pConstrData[constr_index_snopt + 0] = SCALEFACTOR_DIST*SCALEFACTOR_DIST*
			(
			 (n*sf - m_targetN)*(n*sf - m_targetN) +
			 (e*sf - m_targetE)*(e*sf - m_targetE)
			 );

		m_pConstrData[constr_index_snopt + 1] = SCALEFACTOR_DIST*SCALEFACTOR_DIST*
			(
			 (n*sf - m_virtN)*(n*sf - m_virtN) +
			 (e*sf - m_virtE)*(e*sf - m_virtE)
			 );


		m_grad_solver[0][IDX_THETA]		= 0.0;
		m_grad_solver[0][IDX_DTHETA]	= 0.0;
		m_grad_solver[0][IDX_DDTHETA]	= 0.0;
		m_grad_solver[0][IDX_SF]			= SCALEFACTOR_DIST*SCALEFACTOR_DIST * 2.0*((n*sf - m_targetN)*n + (e*sf - m_targetE)*e);
		m_grad_solver[0][IDX_N]				= SCALEFACTOR_DIST*SCALEFACTOR_DIST * 2.0*(n*sf - m_targetN)*sf;
		m_grad_solver[0][IDX_E]				= SCALEFACTOR_DIST*SCALEFACTOR_DIST * 2.0*(e*sf - m_targetE)*sf;

		m_grad_solver[1][IDX_THETA]		= 0.0;
		m_grad_solver[1][IDX_DTHETA]	= 0.0;
		m_grad_solver[1][IDX_DDTHETA]	= 0.0;
		m_grad_solver[1][IDX_SF]			= SCALEFACTOR_DIST*SCALEFACTOR_DIST * 2.0*((n*sf - m_virtN)*n + (e*sf - m_virtE)*e);
		m_grad_solver[1][IDX_N]				= SCALEFACTOR_DIST*SCALEFACTOR_DIST * 2.0*(n*sf - m_virtN)*sf;
		m_grad_solver[1][IDX_E]				= SCALEFACTOR_DIST*SCALEFACTOR_DIST * 2.0*(e*sf - m_virtE)*sf;
	}
}


#warning "This function really shouldn't be mostly a copy of integrateNE"
void CPlannerStage::integrateNEfine(int collIndex, double& outN, double& outE)
{
#ifdef NEW_INTEGRATE_NE

	int i;
	int splinecoeffsstart = 3 * (collIndex / COLLOCATION_FACTOR_FINE);

	double a  = m_splinecoeffsTheta[splinecoeffsstart];
	double b  = m_splinecoeffsTheta[splinecoeffsstart + 1];
	double c  = m_splinecoeffsTheta[splinecoeffsstart + 2];

	double s0 = (double)(collIndex % COLLOCATION_FACTOR_FINE)/(double)(COLLOCATION_FACTOR_FINE);
	double di = 1.0 / (double)(COLLOCATION_FACTOR_FINE);
	double dicubed = di*di*di;
	double s1 = s0 + di;

	double x0_plus_a = 0.5 * (s0*(b+c*s0) + s1*(b+c*s1)) + a;
	double cos_x0_plus_a = cos(x0_plus_a);
	double sin_x0_plus_a = sin(x0_plus_a);

	double val = 7.0 * (s0*s0 + s1*s1) + 6.0*s0*s1;
	double small = c * dicubed / 6.0;
	double big   = 1.0 - dicubed/120.0 * ( c*c* val + 5.0*b* (2.0*c*(s1 + s0) + b) );

	double delN = (cos_x0_plus_a*big + sin_x0_plus_a*small) / (double)((NUM_POINTS-1)*COLLOCATION_FACTOR_FINE);
	double delE = (sin_x0_plus_a*big - cos_x0_plus_a*small) / (double)((NUM_POINTS-1)*COLLOCATION_FACTOR_FINE);
	outN += delN;
	outE += delE;

#else

	int splinecoeffsstart = 3 * (collIndex / COLLOCATION_FACTOR_FINE);

	double a = m_splinecoeffsTheta[splinecoeffsstart];
	double b = m_splinecoeffsTheta[splinecoeffsstart + 1];
	double c = m_splinecoeffsTheta[splinecoeffsstart + 2];
	double s1 = (double)(collIndex % COLLOCATION_FACTOR_FINE)/(double)(COLLOCATION_FACTOR_FINE);
	double diff = 1.0 / (double)(COLLOCATION_FACTOR_FINE);
	double smid = s1 + diff/2.0;

	double ce = smid*(smid*c + b) + a;
	double ce_minus_a = ce - a;
	double sum = 2.0*s1 + diff;

	double sumsq = sum*sum;
	double diffsq = diff*diff;
	double sumsqplusdiffsq = sumsq + diffsq;
	double binom = (sqrt(5.0)*sumsq + (sqrt(5.0) - 2.0) * diffsq) * (sqrt(5.0)*sumsq + (sqrt(5.0) + 2.0)*diffsq);

	double small_ce_minus_a_0 = -diff*(3.0*c* (c*binom + 20.0*b*sum*sumsqplusdiffsq) +
																		 20.0*b*b*(3.0*sumsq+diffsq)-480.0) / 480.0;
	double small_ce_minus_a_1 = diff * (3.0*sum * (c*sum + 2.0*b) + c*diffsq) / 12.0;
	double small_ce_minus_a_2 = -diff / 2.0;

	double big_ce_minus_a_0 =
		(
		 sum * 35.0 *( sum *( sum *( sum *( sum * c*c * (c*sum +
																										 6.0*b) +
																				c * (5.0*c*c*diffsq + 12.0*b*b)) +
																 4.0*b*(5.0*c*c*diffsq + 2.0*b*b)) +
													3.0 * c*(diffsq*(c*c*diffsq+8.0*b*b)-32.0)) +
									 2.0 * b*(diffsq*(3.0*c*c*diffsq+4.0*b*b)-96.0)) +
		 c*diffsq*(diffsq*(5.0*c*c*diffsq+84.0*b*b)-1120.0)
		 ) * diff / 13440.0;

	// these are hardcoded
	// double big_ce_minus_a_1 = small_ce_minus_a_0;
	// double big_ce_minus_a_2 = small_ce_minus_a_1 / 2.0;
	// double big_ce_minus_a_3 = small_ce_minus_a_2 / 3.0;

	double big =
		ce_minus_a *( ce_minus_a *( ce_minus_a*small_ce_minus_a_2 / 3.0 +
																small_ce_minus_a_1 / 2.0) +
									small_ce_minus_a_0) +
		big_ce_minus_a_0;

	double small =
		ce_minus_a *( ce_minus_a * small_ce_minus_a_2 +
									small_ce_minus_a_1 ) +
		small_ce_minus_a_0;

	outN += (cos(ce)*small + sin(ce)*big) / (double)(NUM_POINTS-1);
	outE += (sin(ce)*small - cos(ce)*big) / (double)(NUM_POINTS-1);
#endif
}
