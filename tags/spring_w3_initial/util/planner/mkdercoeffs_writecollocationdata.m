tc = linspace(0,1,(N-1)*M + 1)';
tck = floor(tc * (N-1));
tck(find(tc == 1))--;
tcx = tc * (N-1) - tck;

# at first, valcoeffs * coefficients = collocationvalues
valcoeffs = zeros(length(tc), 3*(N-1));
d1coeffs  = zeros(length(tc), 3*(N-1));
d2coeffs  = zeros(length(tc), 3*(N-1));

# this is terrible
for i=1:length(tc)
	valcoeffs(i,3*tck(i)+1) = 1;
	valcoeffs(i,3*tck(i)+2) = tcx(i);
	valcoeffs(i,3*tck(i)+3) = tcx(i)^2;

	d1coeffs(i,3*tck(i)+2) = (N-1) * 1;
	d1coeffs(i,3*tck(i)+3) = (N-1) * 2 * tcx(i);

	d2coeffs(i,3*tck(i)+3) = (N-1)^2 * 2;
end

W = [eye(N-1) zeros(N-1,1) zeros(N-1,1); \
		 zeros(N-1,1) eye(N-1) zeros(N-1,1); \
		 zeros(N-2, N)         zeros(N-2,1); \
		 zeros(1, N)           1];

# make it so, valcoeffs * y = collocationvalues
valcoeffs = valcoeffs * inv(a) * W;
d1coeffs = d1coeffs * inv(a) * W;
d2coeffs = d2coeffs * inv(a) * W;

# write out the data. don't include the last column (offset), since it's trivial
fil = fopen(filename, "w+b", "ieee-le");
fwrite(fil, valcoeffs(:,1:N) , "double");
fwrite(fil, d1coeffs (:,1:N) , "double");
fwrite(fil, d2coeffs (:,1:N) , "double");
fclose(fil);
