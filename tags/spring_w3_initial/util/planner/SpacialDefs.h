// NUM_POINTS is the number of points in the spline
// COLLOCATION_FACTOR is the number of collocation points in each spline segment
// NUM_COLLOCATION_POINTS is the resulting total number of collocation points.
#define	NUM_POINTS_SPACIAL							8
#define	COLLOCATION_FACTOR_SPACIAL			50

#define PRINT_LEVEL_SPACIAL									0
#define USE_FINAL_ANGLE_CONSTRAINT_SPACIAL	false

#define HALF_TARGET_ERROR_SPACIAL								1.0
#define TARGET_WINDOW_RADIUS_SPACIAL  				  5.0
#define CARTESIAN_TARGET_SPECIFICATION_SPACIAL	true
#define HALF_TARGET_WINDOW_SPACIAL							5.0

#define PLANNING_TARGET_DIST_SPACIAL						70.0

// how many constraints are ignored at the start of the trajectory
#define NUM_IGNORED_CONSTRAINTS_SPACIAL					0

#define PHICONSTR				    1
#define PHICONSTR_IDX				(PHICONSTR - 1)

#define MAX_PHI				VEHICLE_MAX_AVG_STEER
#define MAX_TAN_PHI		VEHICLE_MAX_TAN_AVG_STEER

// Higher to make shorter paths more important and running into shit less important
#define SF_COST_FACTOR				(1.0 * NUM_COLLOCATION_POINTS*MAXSPEED/PLANNING_TARGET_DIST_SPACIAL)
#define DDTHETA_COST_FACTOR		(0.01 * NUM_COLLOCATION_POINTS*MAXSPEED)

/* #define SPACIAL_USEPREVSOLUTION */
/* #define SPACIAL_ZEROSEED */
#define SPACIAL_PATHGENSEED
/* #define SPACIAL_REACTIVESEED */

#define PATHGENSEED_MERGEDIST 30.0

/* #define USE_INITIAL_YAWRATE */

#define SPEED_EQUALS_OBSTACLE (MIN_V*4.0)

#define SCALEFACTOR_COST ( MAXSPEED / PLANNING_TARGET_DIST_SPACIAL / NUM_COLLOCATION_POINTS *2.0)

#define SPACIAL_SIMPLECOST

// how many CELLS wide the map blurring kernel is. must be odd
#define MAP_BLUR_KERNEL_DIAM_SPACIAL		5

// how many map cells per a single data point. used in the map blurring/interpolation
#define MAP_BLURINTERP_STEP_SPACIAL     1
