#include <iostream>
using namespace std;

#include <stdlib.h>
#include <math.h>
#include "CMapPlus.hh"
#include "defs.h"
#include "RefinementStage.h"
#include "RefinementDefs.h"

// for simpler access
// NOTE: n,e here are not scaled by Sf
#define theta						m_pColl_theta  [collIndex]
#define dtheta					m_pColl_dtheta [collIndex]
#define ddtheta					m_pColl_ddtheta[collIndex]
#define v               m_pColl_speed  [collIndex]
#define dv              m_pColl_dspeed [collIndex]
#define sf							m_collSF
#define n								m_pCollN[collIndex]
#define e								m_pCollE[collIndex]
#define vlimit          m_pCollVlimit[collIndex]
#define dvldN           m_pCollDVlimitDN[collIndex]
#define dvldE           m_pCollDVlimitDE[collIndex]

#warning "some more things should use these functions. maybe makezp. search for valcoeffs, search for for(i=0; i<NUM_POINTS; i++)"
extern "C" void dgemv_(char*, int*, int*, double*, double*, int*, double*, int*, double*, double*, int*);
extern "C" void dgemm_(char*, char*, int*, int*, int*, double*, double*, int*, double*, int*, double*, double*, int*);
extern "C" double ddot_(int*, double*, int*, double*, int*);
extern "C" void daxpy_(int*, double*, double*, int*, double*, int*);


void CRefinementStage::makeCollocationData(double* pSolverState)
{
	CPlannerStage::makeCollocationData(pSolverState);

	getSplineCoeffsSpeed(pSolverState);

	char notrans = 'N';
	int rows = NUM_COLLOCATION_POINTS;
	int cols = NUM_POINTS;
	double alpha = 1.0;
	double beta = 0.0;
	int incr = 1;

	dgemv_(&notrans, &rows, &cols, &alpha, m_valcoeffs, &rows,
				 pSolverState+NUM_POINTS, &incr, &beta, m_pColl_speed, &incr);
	dgemv_(&notrans, &rows, &cols, &alpha, m_d1coeffs, &rows,
				 pSolverState+NUM_POINTS, &incr, &beta, m_pColl_dspeed, &incr);

	for(int i=0; i<NUM_COLLOCATION_POINTS; i++)
		m_pColl_speed[i] += m_initSpeed;
}

void CRefinementStage::NonLinearInitConstrFunc(void)
{
	const int collIndex = 0;

	m_pConstrData[0] = SCALEFACTOR_ACCEL  * v*dv/sf;
	m_pConstrData[1] = SCALEFACTOR_YAWDOT * v*dtheta/sf;

	m_grad_solver[0][IDX_THETA]		= 0.0;
	m_grad_solver[0][IDX_DTHETA]	= 0.0;
	m_grad_solver[0][IDX_DDTHETA]	= 0.0;
	m_grad_solver[0][IDX_V]				= SCALEFACTOR_ACCEL * dv/sf;
	m_grad_solver[0][IDX_DV]			= SCALEFACTOR_ACCEL * v/sf;
	m_grad_solver[0][IDX_DDV]			= 0.0;
	m_grad_solver[0][IDX_SF]			= SCALEFACTOR_ACCEL * (-v*dv/sf/sf);
	m_grad_solver[0][IDX_N]				= 0.0;
	m_grad_solver[0][IDX_E]				= 0.0;

	m_grad_solver[1][IDX_THETA]		= 0.0;
	m_grad_solver[1][IDX_DTHETA]	= SCALEFACTOR_YAWDOT * v/sf;
	m_grad_solver[1][IDX_DDTHETA]	= 0.0;
	m_grad_solver[1][IDX_V]				= SCALEFACTOR_YAWDOT * dtheta/sf;
	m_grad_solver[1][IDX_DV]			= 0.0;
	m_grad_solver[1][IDX_DDV]			= 0.0;
	m_grad_solver[1][IDX_SF]			= SCALEFACTOR_YAWDOT * (-v*dtheta/sf/sf);
	m_grad_solver[1][IDX_N]				= 0.0;
	m_grad_solver[1][IDX_E]				= 0.0;
}

void CRefinementStage::NonLinearTrajConstrFunc(int constr_index_snopt, int collIndex)
{
#if VCONSTR
	m_pConstrData[constr_index_snopt + VCONSTR_IDX] = SCALEFACTOR_SPEED * (vlimit - v);
#endif
#if ACONSTR
	m_pConstrData[constr_index_snopt + ACONSTR_IDX]		= SCALEFACTOR_ACCEL * v*dv/sf;
#endif
#if PHICONSTR
	// This is actually tan(phi)
	m_pConstrData[constr_index_snopt + PHICONSTR_IDX]	= SCALEFACTOR_TANPHI * VEHICLE_WHEELBASE / sf * dtheta;
#endif
#if PHIDCONSTR
	m_pConstrData[constr_index_snopt + PHIDCONSTR_IDX]	= SCALEFACTOR_PHIDOT * VEHICLE_WHEELBASE * v * ddtheta
		/sf/sf/sqrt(1.0 - m_pConstrData[constr_index_snopt + PHICONSTR_IDX]*m_pConstrData[constr_index_snopt + PHICONSTR_IDX]);
#endif
#if ROLLOVERCONSTR
	m_pConstrData[constr_index_snopt + ROLLOVERCONSTR_IDX]	= SCALEFACTOR_ROLLOVER * v*v * dtheta / sf;
#endif


#if VCONSTR
	m_grad_solver[VCONSTR_IDX][IDX_THETA]		= 0.0;
	m_grad_solver[VCONSTR_IDX][IDX_DTHETA]	= 0.0;
	m_grad_solver[VCONSTR_IDX][IDX_DDTHETA]	= 0.0;
	m_grad_solver[VCONSTR_IDX][IDX_V]				= -SCALEFACTOR_SPEED;
	m_grad_solver[VCONSTR_IDX][IDX_DV]			= 0.0;
	m_grad_solver[VCONSTR_IDX][IDX_DDV]			= 0.0;
	m_grad_solver[VCONSTR_IDX][IDX_SF]			= SCALEFACTOR_SPEED * (dvldN*n + dvldE*e);
	m_grad_solver[VCONSTR_IDX][IDX_N]				= SCALEFACTOR_SPEED * dvldN*sf;
	m_grad_solver[VCONSTR_IDX][IDX_E]				= SCALEFACTOR_SPEED * dvldE*sf;
#endif
#if ACONSTR
	m_grad_solver[ACONSTR_IDX][IDX_THETA]		= 0.0;
	m_grad_solver[ACONSTR_IDX][IDX_DTHETA]	= 0.0;
	m_grad_solver[ACONSTR_IDX][IDX_DDTHETA]	= 0.0;
	m_grad_solver[ACONSTR_IDX][IDX_V]				= SCALEFACTOR_ACCEL * dv/sf;
	m_grad_solver[ACONSTR_IDX][IDX_DV]			= SCALEFACTOR_ACCEL * v/sf;
	m_grad_solver[ACONSTR_IDX][IDX_DDV]			= 0.0;
	m_grad_solver[ACONSTR_IDX][IDX_SF]			= SCALEFACTOR_ACCEL * (-v*dv/sf/sf);
	m_grad_solver[ACONSTR_IDX][IDX_N]				= 0.0;
	m_grad_solver[ACONSTR_IDX][IDX_E]				= 0.0;
#endif
#if PHICONSTR
	m_grad_solver[PHICONSTR_IDX][IDX_THETA]		= 0.0;
	m_grad_solver[PHICONSTR_IDX][IDX_DTHETA]	= SCALEFACTOR_TANPHI * (VEHICLE_WHEELBASE / sf);
	m_grad_solver[PHICONSTR_IDX][IDX_DDTHETA]	= 0.0;
	m_grad_solver[PHICONSTR_IDX][IDX_V]				= 0.0;
	m_grad_solver[PHICONSTR_IDX][IDX_DV]			= 0.0;
	m_grad_solver[PHICONSTR_IDX][IDX_DDV]			= 0.0;
	m_grad_solver[PHICONSTR_IDX][IDX_SF]			= SCALEFACTOR_TANPHI * (-VEHICLE_WHEELBASE * dtheta /sf/sf);
	m_grad_solver[PHICONSTR_IDX][IDX_N]				= 0.0;
	m_grad_solver[PHICONSTR_IDX][IDX_E]				= 0.0;
#endif
#if PHIDCONSTR
#warning "these computations can be optimized"
	m_grad_solver[PHIDCONSTR_IDX][IDX_THETA]	= 0.0;
	m_grad_solver[PHIDCONSTR_IDX][IDX_DTHETA]	= SCALEFACTOR_PHIDOT * VEHICLE_WHEELBASE * v * VEHICLE_WHEELBASE / sf * dtheta *
		m_grad_solver[PHICONSTR_IDX][IDX_DTHETA] * ddtheta/sf/sf/
		pow(1.0 - pow(VEHICLE_WHEELBASE / sf * dtheta,2.0), 1.5);
	m_grad_solver[PHIDCONSTR_IDX][IDX_DDTHETA]= SCALEFACTOR_PHIDOT * VEHICLE_WHEELBASE * v
		/sf/sf/sqrt(1.0 - pow(VEHICLE_WHEELBASE / sf * dtheta, 2.0));
	m_grad_solver[PHIDCONSTR_IDX][IDX_V]			= SCALEFACTOR_PHIDOT * VEHICLE_WHEELBASE * ddtheta
		/sf/sf/sqrt(1.0 - pow(VEHICLE_WHEELBASE / sf * dtheta, 2.0));
	m_grad_solver[PHIDCONSTR_IDX][IDX_DV]			= 0.0;
	m_grad_solver[PHIDCONSTR_IDX][IDX_DDV]		= 0.0;
	m_grad_solver[PHIDCONSTR_IDX][IDX_SF]			= SCALEFACTOR_PHIDOT * (-VEHICLE_WHEELBASE * v * ddtheta /
																																		(pow(sf,4.0)*(1.0 - pow(VEHICLE_WHEELBASE / sf * dtheta, 2.0))) *
																																		(2.0*sf*sqrt(1.0 - pow(VEHICLE_WHEELBASE / sf * dtheta, 2.0)) -
																																		 sf*sf/sqrt(1.0 - pow(VEHICLE_WHEELBASE / sf * dtheta, 2.0))*
																																		 VEHICLE_WHEELBASE / sf * dtheta*m_grad_solver[PHICONSTR_IDX][IDX_SF]));
	m_grad_solver[PHIDCONSTR_IDX][IDX_N]			= 0.0;
	m_grad_solver[PHIDCONSTR_IDX][IDX_E]			= 0.0;
#endif
#if ROLLOVERCONSTR
	m_grad_solver[ROLLOVERCONSTR_IDX][IDX_THETA]	= 0.0;
	m_grad_solver[ROLLOVERCONSTR_IDX][IDX_DTHETA]	= SCALEFACTOR_ROLLOVER * v*v/sf;
	m_grad_solver[ROLLOVERCONSTR_IDX][IDX_DDTHETA]= 0.0;
	m_grad_solver[ROLLOVERCONSTR_IDX][IDX_V]			= SCALEFACTOR_ROLLOVER * 2.0*v*dtheta/sf;
	m_grad_solver[ROLLOVERCONSTR_IDX][IDX_DV]			= 0.0;
	m_grad_solver[ROLLOVERCONSTR_IDX][IDX_DDV]		= 0.0;
	m_grad_solver[ROLLOVERCONSTR_IDX][IDX_SF]			= SCALEFACTOR_ROLLOVER * (-v*v*dtheta/sf/sf);
	m_grad_solver[ROLLOVERCONSTR_IDX][IDX_N]			= 0.0;
	m_grad_solver[ROLLOVERCONSTR_IDX][IDX_E]			= 0.0;
#endif
}

void CRefinementStage::NonLinearFinlConstrFunc(void)
{
	CPlannerStage::NonLinearFinlConstrFunc();

// 	m_grad_solver[0][IDX_V]				= 0.0;
// 	m_grad_solver[0][IDX_DV]			= 0.0;
// 	m_grad_solver[0][IDX_DDV]			= 0.0;
// 	m_grad_solver[1][IDX_V]				= 0.0;
// 	m_grad_solver[1][IDX_DV]			= 0.0;
// 	m_grad_solver[1][IDX_DDV]			= 0.0;
	memset(&m_grad_solver[0][IDX_V], 0, 3*sizeof(m_grad_solver[0][IDX_V]));
	memset(&m_grad_solver[1][IDX_V], 0, 3*sizeof(m_grad_solver[1][IDX_V]));
}

void CRefinementStage::getSplineCoeffsSpeed(double* pSolverState)
{
	char notrans = 'N';
	int rows = (NUM_POINTS-1) * 3; // number of polynomial coefficients
	int cols = NUM_POINTS;         // number of spline-defining variables
	double alpha = 1.0;
	double beta = 0.0;
	int incr = 1;

	dgemv_(&notrans, &rows, &cols, &alpha, m_dercoeffsmatrix, &rows,
				 pSolverState+NUM_POINTS, &incr, &beta, m_splinecoeffsSpeed, &incr);

	for(int i=0; i<(NUM_POINTS-1); i++)
		m_splinecoeffsSpeed[3*i] += m_initSpeed;
}


void CRefinementStage::funcConstr(void)
{
	int collIndex, constr_index_snopt, constr_index_planner;

	int varNUM_COLLOCATION_POINTS = NUM_COLLOCATION_POINTS;
	int varNPnumNonLinearConstr = NPnumNonLinearConstr;
	int one = 1;

	// zero out the gradient matrix
	memset(m_pConstrGradData, 0, NPnumNonLinearConstr*NPnumVariables*sizeof(m_pConstrGradData[0]));

	constr_index_snopt = 0;

	NonLinearInitConstrFunc();
	for(constr_index_planner=0;
			constr_index_planner < NUM_NLIN_INIT_CONSTR;
			constr_index_planner++)
	{
		// compute
		// 		m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, ALL)]  +=
		// 			m_grad_solver[constr_index_planner][IDX_THETA] * m_valcoeffs[0 + ALL*NUM_COLLOCATION_POINTS];
		// etc, etc
		// prev code was
// 		for(i=0; i<NUM_POINTS; i++)
// 		{
// 			// This is the initial constraint, so there's no dependence on (N,E), since it's (0,0) here
// 			gCon[MATRIX_INDEX(constr_index_snopt, i)]  = m_valcoeffs[0*(NUM_POINTS+1) + i]*m_grad_solver[constr_index_planner][IDX_THETA];
// 			gCon[MATRIX_INDEX(constr_index_snopt, i)] += m_d1coeffs [0*(NUM_POINTS+1) + i]*m_grad_solver[constr_index_planner][IDX_DTHETA];
// 			gCon[MATRIX_INDEX(constr_index_snopt, i)] += m_d2coeffs [0*(NUM_POINTS+1) + i]*m_grad_solver[constr_index_planner][IDX_DDTHETA];
// 			gCon[MATRIX_INDEX(constr_index_snopt, NUM_POINTS+i)]  = m_valcoeffs[0*(NUM_POINTS+1) + i]*m_grad_solver[constr_index_planner][IDX_V];
// 			gCon[MATRIX_INDEX(constr_index_snopt, NUM_POINTS+i)] += m_d1coeffs [0*(NUM_POINTS+1) + i]*m_grad_solver[constr_index_planner][IDX_DV];
// 			gCon[MATRIX_INDEX(constr_index_snopt, NUM_POINTS+i)] += m_d2coeffs [0*(NUM_POINTS+1) + i]*m_grad_solver[constr_index_planner][IDX_DDV];
// 		}
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_THETA],
					 &m_valcoeffs[0], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_DTHETA],
					 &m_d1coeffs[0], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_DDTHETA],
					 &m_d2coeffs[0], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_V],
					 &m_valcoeffs[0], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NUM_POINTS)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_DV],
					 &m_d1coeffs[0], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NUM_POINTS)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_DDV],
					 &m_d2coeffs[0], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NUM_POINTS)], &varNPnumNonLinearConstr);
		// Sf derivatives
		m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NPnumVariables-1)] = m_grad_solver[constr_index_planner][IDX_SF];

		constr_index_snopt++;
	}

	for(collIndex=0; collIndex<NUM_COLLOCATION_POINTS; collIndex++)
	{
		NonLinearTrajConstrFunc(constr_index_snopt, collIndex);

		for(constr_index_planner=0;
				constr_index_planner < NUM_NLIN_TRAJ_CONSTR;
				constr_index_planner++)
		{
			daxpy_(&NUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_N],
						 &m_pCollGradN[NUM_POINTS*collIndex], &one,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
			daxpy_(&NUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_E],
						 &m_pCollGradE[NUM_POINTS*collIndex], &one,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
			daxpy_(&NUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_THETA],
						 &m_valcoeffs[collIndex], &varNUM_COLLOCATION_POINTS,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
			daxpy_(&NUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_DTHETA],
						 &m_d1coeffs[collIndex], &varNUM_COLLOCATION_POINTS,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
			daxpy_(&NUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_DDTHETA],
						 &m_d2coeffs[collIndex], &varNUM_COLLOCATION_POINTS,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
			daxpy_(&NUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_V],
						 &m_valcoeffs[collIndex], &varNUM_COLLOCATION_POINTS,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NUM_POINTS)], &varNPnumNonLinearConstr);
			daxpy_(&NUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_DV],
						 &m_d1coeffs[collIndex], &varNUM_COLLOCATION_POINTS,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NUM_POINTS)], &varNPnumNonLinearConstr);
			daxpy_(&NUM_POINTS,
						 &m_grad_solver[constr_index_planner][IDX_DDV],
						 &m_d2coeffs[collIndex], &varNUM_COLLOCATION_POINTS,
						 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NUM_POINTS)], &varNPnumNonLinearConstr);

			// Sf derivatives
			m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NPnumVariables-1)] = m_grad_solver[constr_index_planner][IDX_SF];
			constr_index_snopt++;
		}
	}

	// compute the constr, gradients from the user
	NonLinearFinlConstrFunc();
	for(constr_index_planner=0;
			constr_index_planner < NUM_NLIN_FINL_CONSTR;
			constr_index_planner++)
	{
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_N],
					 &m_pCollGradN[NUM_POINTS*(NUM_COLLOCATION_POINTS-1)], &one,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_E],
					 &m_pCollGradE[NUM_POINTS*(NUM_COLLOCATION_POINTS-1)], &one,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_THETA],
					 &m_valcoeffs[(NUM_COLLOCATION_POINTS-1)], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_DTHETA],
					 &m_d1coeffs[(NUM_COLLOCATION_POINTS-1)], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_DDTHETA],
					 &m_d2coeffs[(NUM_COLLOCATION_POINTS-1)], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, 0)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_V],
					 &m_valcoeffs[(NUM_COLLOCATION_POINTS-1)], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NUM_POINTS)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_DV],
					 &m_d1coeffs[(NUM_COLLOCATION_POINTS-1)], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NUM_POINTS)], &varNPnumNonLinearConstr);
		daxpy_(&NUM_POINTS,
					 &m_grad_solver[constr_index_planner][IDX_DDV],
					 &m_d2coeffs[(NUM_COLLOCATION_POINTS-1)], &varNUM_COLLOCATION_POINTS,
					 &m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NUM_POINTS)], &varNPnumNonLinearConstr);

		// Sf derivatives
		m_pConstrGradData[MATRIX_INDEX(constr_index_snopt, NPnumVariables-1)] = m_grad_solver[constr_index_planner][IDX_SF];
		constr_index_snopt++;
	}
}


void CRefinementStage::funcCost(void)
{
#ifdef EXACT_COST
	int i;
#warning "this 'a' overloads the member 'a'"
	double a;
	double b;
	double c;
	double descr;
	double sqrdescr;
	double dsqrdescr_da, dsqrdescr_db, dsqrdescr_dc;
	double dObj_da;
	double dObj_db;
	double dObj_dc;
	double arctan1, arctan2;
	double logarg;
	double num,denom;
	double dnum_da, dnum_db, dnum_dc;
	double ddenom_da, ddenom_db, ddenom_dc;

	if(bVal)
	{
		// reset the accumulator
		*fObj = 0.0;
	}

	// cycle through each spline segment and add the time contribution of each
	for(i=0; i<(NUM_POINTS-1); i++)
	{
#warning "This can be optimized"
		a = m_splinecoeffsSpeed[i*3];
		b = m_splinecoeffsSpeed[i*3 + 1];
		c = m_splinecoeffsSpeed[i*3 + 2];

		descr = b*b - 4.0*a*c;
		sqrdescr = sqrt(fabs(descr));
		dsqrdescr_da = 2.0*c / sqrdescr;
		dsqrdescr_db = -b    / sqrdescr;
		dsqrdescr_dc = 2.0*a / sqrdescr;

		if(bVal)
		{
			if(c == 0.0)
			{
				if(b==0.0)
				{
					*fObj += 1.0/a;
				}
				else
				{
					*fObj += log((a+b)/a) / b;
				}
			}
			else
			{
				if(descr < 0.0)
				{
#warning "this can be combined into a single atan"
					// 				double ang = atan(sqrdescr/(b + 2.0*a));
					// 				if(ang < 0.0)
					// 					ang += M_PI;
					// 				*fObj += (2.0 * ang / sqrdescr);
					arctan1 = atan((2.0*c + b)/sqrdescr);
					arctan2 = atan(b/sqrdescr);
					*fObj += ( arctan1 - arctan2 ) * 2.0 / sqrdescr;

				}
				else if(descr > 0.0)
				{
					num = sqrdescr - c - b;
					denom = b*sqrdescr - descr - 2.0*a*c;
					logarg = a / (a+b+c) * (1.0 + 2.0*c*num/denom);
					*fObj += log(logarg) / sqrdescr;
				}
				else
				{
					*fObj += 2.0/b - 2.0/(2.0*c + b);
				}
			}
		}
	}
	if(bVal)
	{
		*fObj *= -pSolverState[NPnumVariables-1];
// 		cerr << "Cost: " << (*fObj) << endl;
		if(isnan(*fObj))
			cerr << "nan!!!!!!!!!!!!!" << endl;
	}
// 	if(bGrad)
// 	{
// 		for(i=0; i<NPnumVariables; i++)
// 			gObj[i] = 2.0;
// 	}



#else // !define EXACT_COST



	int i,j;
	double a;
	double b;
	double c;
	double dObj_da;
	double dObj_db;
	double dObj_dc;
	double val;

	// reset the accumulator
	m_obj = 0.0;
	memset(m_pCostGradData, 0, NPnumVariables*sizeof(m_pCostGradData[0]));

	// cycle through each spline segment and add the time contribution of each
	for(i=0; i<(NUM_POINTS-1); i++)
	{
#warning "This can be optimized"
		a = m_splinecoeffsSpeed[i*3];
		b = m_splinecoeffsSpeed[i*3 + 1];
		c = m_splinecoeffsSpeed[i*3 + 2];

		val = (4.0*(45.0*c*c*c*c+270.0*b*c*c*c+220.0*a*c*c*c+680.0*b*b*c*c+1600.0*a*b*c*c+1568.0*a*a*c*c
								+800.0*b*b*b*c+3296.0*a*b*b*c+5920.0*a*a*b*c+3520.0*a*a*a*c+368.0*b*b*b*b
								+2240.0*a*b*b*b+6080.0*a*a*b*b+7680.0*a*a*a*b+3840.0*a*a*a*a)
					 /(15.0*pow(c+2.0*b+4.0*a,5.0)));

		m_obj += val * m_collSF * SCALEFACTOR_COST;

		dObj_da = -32.0*(85.0*c*c*c*c+420.0*b*c*c*c+48.0*a*c*c*c+888.0*b*b*c*c+936.0*a*b*c*c+1032.0*a*a*c*c
										 +896.0*b*b*b*c+2112.0*a*b*b*c+3360.0*a*a*b*c+1600.0*a*a*a*c+360.0*b*b*b*b
										 +1440.0*a*b*b*b+3360.0*a*a*b*b+3840.0*a*a*a*b+1920.0*a*a*a*a)
			/ (15.0*pow(c+2.0*b+4.0*a, 6.0));
		dObj_db = -16.0*(45.0*c*c*c*c+200.0*b*c*c*c-120.0*a*c*c*c+420.0*b*b*c*c+192.0*a*b*c*c+840.0*a*a*c*c
										 +432.0*b*b*b*c+864.0*a*b*b*c+2208.0*a*a*b*c+960.0*a*a*a*c+184.0*b*b*b*b
										 +768.0*a*b*b*b+2400.0*a*a*b*b+3200.0*a*a*a*b+1920.0*a*a*a*a)
			/(15.0*pow(c+2.0*b+4.0*a, 6.0));
		dObj_dc = -4.0*(45.0*c*c*c*c+180.0*b*c*c*c-280.0*a*c*c*c+420.0*b*b*c*c+240.0*a*b*c*c+2064.0*a*a*c*c
										+480.0*b*b*b*c+1344.0*a*b*b*c+4608.0*a*a*b*c+1536.0*a*a*a*c+240.0*b*b*b*b
										+1408.0*a*b*b*b+5376.0*a*a*b*b+7680.0*a*a*a*b+5120.0*a*a*a*a)
			/(15.0*pow(c+2.0*b+4.0*a, 6.0));


#warning "BLAS-ify this"
		// cycle through all of the v(s) spline parameters
		for(j=0; j<NUM_POINTS; j++)
		{
			m_pCostGradData[j+NUM_POINTS] += m_collSF * SCALEFACTOR_COST *
				(dObj_da * m_dercoeffsmatrix[i*3+0 + j*(NUM_POINTS-1)*3] +
				 dObj_db * m_dercoeffsmatrix[i*3+1 + j*(NUM_POINTS-1)*3] +
				 dObj_dc * m_dercoeffsmatrix[i*3+2 + j*(NUM_POINTS-1)*3]);
		}
		m_pCostGradData[NPnumVariables-1] += val * SCALEFACTOR_COST;
	}

#endif
}
