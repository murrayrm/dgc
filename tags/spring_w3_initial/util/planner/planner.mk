PLANNER_PATH = $(DGC)/util/planner

PLANNER_DEPEND_LIBS = \
	$(SPARROWLIB) \
	$(RDDFLIB) \
	$(GGISLIB) \
	$(CMAPLIB) \
	$(CMAPPLIB) \
	$(TRAJLIB) \
	$(CCPAINTLIB) \
	$(MODULEHELPERSLIB) \
	$(FRAMESLIB) \
	$(RDDFPATHGENLIB) \
	$(REACTIVEPLANNERLIB)

PLANNER_DEPEND_SOURCES = \
	$(PLANNER_PATH)/planner.cc \
	$(PLANNER_PATH)/PlannerStage.cc \
	$(PLANNER_PATH)/PlannerConstraintFuncs.cc \
	$(PLANNER_PATH)/SpacialStage.cc \
	$(PLANNER_PATH)/SpacialConstraintFuncs.cc \
	$(PLANNER_PATH)/RefinementStage.cc \
	$(PLANNER_PATH)/RefinementConstraintFuncs.cc \
	$(PLANNER_PATH)/planner.h \
	$(PLANNER_PATH)/SpacialStage.h \
	$(PLANNER_PATH)/PlannerStage.h \
	$(PLANNER_PATH)/defs.h  \
	$(PLANNER_PATH)/RefinementStage.h \
	$(PLANNER_PATH)/RefinementDefs.h \
	$(PLANNER_PATH)/Makefile

PLANNER_DEPEND = $(PLANNER_DEPEND_LIBS) $(PLANNER_DEPEND_SOURCES)
