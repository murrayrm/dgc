function bob2bob( bobfile, E_first, N_first, Yaw )
%
% function bob2bob( bobfile, E_first, N_first, Yaw )
%
% This function takes a Bob format waypoint file and a specification
% to translate and rotate the waypoints in it.  It writes a modified
% waypoint file with the waypoints translated and rotated as 
% specified
%
% Changes:
%
%   2004/02/21, Lars Cremean, created
%   2005/01/17, LBC, changed to take first WP location as argument, changed 
%               to new way of loading bob files.
% Inputs:
%
%   bobfile: A string that specifies the Bob format file in the current 
%   directory to read
%   N_first: The Northing of the first waypoint, in meters
%   E_first: The Easting of the first waypoint, in meters
%   Yaw: The angle about which to rotate the waypoints, measured 
%   CLOCKWISE.  The waypoints will rotate about waypoint 0.
%
% Example usage:
%
%   bob2bob( 'qid_original.bob', 376000, 3777000, pi/2 )

% new, modular way to read corridor file.
corridor_matrix = file2corridor(bobfile);
eastings = corridor_matrix(:,1);
northings = corridor_matrix(:,2);
radii = corridor_matrix(:,3);
vees = corridor_matrix(:,4);

wp0_orig = [eastings(1) northings(1)];

% translate the waypoint set so that the first waypoint is the origin
eastings  = eastings  - wp0_orig(1);
northings = northings - wp0_orig(2);

% rotate each of the waypoints about the first waypoint
% recall that Yaw is defined positive clockwise
R = [cos(Yaw) sin(Yaw); -sin(Yaw) cos(Yaw)];

for i=1:size(eastings)

    % assign the new eastings and northings
    % this will be a [2xN] array
    newen(:,i) = R * [eastings(i); northings(i)];
    
end

% then translate to the new location
newen(1,:) = newen(1,:) + E_first;
newen(2,:) = newen(2,:) + N_first;

% now write the resulting data to another file
file = fopen('new.bob', 'w');

fprintf(file, '% This is a modified Bob format waypoint file that has been\n');
fprintf(file, '% output by the MATLAB function bob2bob.m\n');
fprintf(file, '% Format: {char, Easting[m], Northing[m], Offset[m], Speed[m/s]}\n');
fprintf(file, '% The original file is %s\n', bobfile);

for i=1:size(eastings)
    
    fprintf(file, '%d  %9.2f  %10.2f  %7.2f  %6.2f\n', ...
    i, newen(1,i), newen(2,i), radii(i), vees(i));
    
end

fclose(file);
