#include "sn_msg.hh"
#include <string.h>
#include <stdlib.h>
#define datalen 64000

int main(int argc, char** argv){
  int sn_key;
  char* default_key = getenv("SKYNET_KEY");
  if (2 == argc)
  {
    sn_key  = atoi(argv[1]);
  }
  else if (default_key != NULL )
  {
    sn_key = atoi(default_key);
  }
  else
  {
    printf("usage: sn_send_reliable key\nDefault key is $SKYNET_KEY\n");
    exit(2);
  }
  
  modulename myself = SNstereo;
 
  skynet Skynet(myself, sn_key);
  printf("My module id is: %u\n", Skynet.get_id());
  
  /*Make an rmultimessage*/
  void* buffer = malloc(datalen);
  //char buffer[datalen + sizeof(reghdr)];
    //memset((char*)buffer + sizeof(reghdr), 'B', datalen);
  int* ptr;
  ptr = (int*)buffer;
  unsigned int i;
  for(i = 0; i < datalen/sizeof(int); i++)
  {
    *(ptr + i) = i;
    //printf("%p %u\t", (ptr+i), i);
  }
  printf("\n");
  //memset(buffer , 'A', datalen);
  //printf("%p\t%p\t%x\n", &buffer, (&buffer+1), (uint)((&buffer+1) - &buffer));
  //printf("%s\n%s\n", buffer, (char*)buffer + sizeof(reghdr));
  //char* mynull = NULL;
  //printf("%x\t%x\n", mynull, mynull+1);
  int mychan = Skynet.get_send_sock(SNpointcloud); 
  Skynet.send_msg(mychan, buffer, datalen, 0);
  
  //free(buffer);
  return 0;
  }
