#include "sn_msg.hh"
#include <stdio.h>
#include <stdlib.h>
#define BUFSIZE 1024

int main(int argc, char** argv){
  int sn_key;
  char* default_key = getenv("SKYNET_KEY");
  if (2 == argc)
  {
    sn_key  = atoi(argv[1]);
  }
  else if (default_key != NULL )
  {
    sn_key = atoi(default_key);
  }
  else
  {
    printf("usage: sn_get_test key\nDefault key is $SKYNET_KEY\n");
    exit(2);
  }
    
  modulename myself = SNGuimodstart;
  modulename sender = ALLMODULES;
  skynet Skynet(myself, sn_key);
  printf("My module ID: %u\n", Skynet.get_id());
  sn_msg myinput = SNmodlist;
  
  int sock1 = Skynet.listen(myinput, sender);
  char* buffer[BUFSIZE];
  while(1)
  {
    int size = Skynet.get_msg(sock1, buffer, BUFSIZE, 0);
    int i;
    for(i=0; i<size ;i+=sizeof(ml_elem))
    {
      ml_elem* mod = (ml_elem*)buffer;
      printf("id: %u\tlocation: %u\tname: %u\n", mod->id, mod->location, mod->name); 
    }
    printf("\n");
  }
  return 0;
  }
