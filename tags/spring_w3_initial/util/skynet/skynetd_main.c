#include "skynetd.h"
#include <string.h>
#include <stdio.h>
#include <sys/select.h>
#include <assert.h>
#include <limits.h>
#include <unistd.h>
#include <sys/stat.h>

int main(int argc, char** argv){
  
  /*get external variables*/
  extern sn_sock sock_list[NUM_SOCKS]; 
  extern int to_fork;
  extern long long drop_rmultiin_last;
  /*check assumptions*/
  assert(sizeof(sn_msg) == 4);
  /* initialize static data */ 
  init_data(argc, argv);
 
  if (to_fork)
  {
    /* fork and become a daemon */
    close (STDIN_FILENO);
    close (STDOUT_FILENO);
    close (STDERR_FILENO);
    pid_t pid = fork();
    if (pid != 0){
      exit(0);       //in parent
    }
    else if(pid < 0){
      perror("fork");
      exit(1);
    }
    //in child
    setsid();                 //create a new session
    chdir("/");               //get off any mounted file system
    umask(0);
  }
    
  /* loop on multiplexed input */
  while (1){
    int i;
    unsigned long long now;
    struct timeval sometimeval;
    struct timeval* timeout = &sometimeval;    
    //Using LONG_MAX below creates a Y2039 bug, i believe
    unsigned long long int timeouttime = ULLONG_MAX ;
    fd_set readfds, writefds, exceptfds;
    FD_ZERO(&readfds);
    FD_ZERO(&writefds);
    FD_ZERO(&exceptfds);
    int highsock = 0;               /* highest numbered socket to wait for */
    for (i=0; i < NUM_SOCKS; i++){  /*all socks are reading or listening */
      if (sock_list[i].present == 1){
        FD_SET(i, &readfds);       //TODO: fix this.  not all sockets are input
        if (i >= highsock) highsock = i + 1;
        if (sock_list[i].checktime != 0 && sock_list[i].checktime < timeouttime)
          timeouttime = sock_list[i].checktime;
      }
    }  
    /* set timeouttime to account for drop_rmultiin */
    long long drmilt = drop_rmultiin_last + RMULTIIN_TIMEOUT;
    if(timeouttime > drmilt) timeouttime = drmilt;
    if (timeouttime == ULLONG_MAX)
      timeout = NULL;
    else
    {
      SNgettime(&now);
      long long timeoutll = timeouttime - now;
      if (timeoutll < 0) timeoutll = 0; 
      timeout->tv_sec = timeoutll / 1000000;
      timeout->tv_usec = timeoutll % 1000000;
    }  
    
    /*TODO implement writefds.  For now, never come back to main loop while
     * waiting for writing
     */
    
    if (highsock == 0){
      fprintf(stderr, "no sockets to use.  What should I do?\n");
      exit(1);
    }
    
    int select_ret = select(highsock, &readfds, &writefds, &exceptfds, timeout);
    if(select_ret == -1) {
      perror("select: ");
    }
    SNgettime(&now);
    
    /* now the fun stuff: dealing with data */
    for (i = 0; i < NUM_SOCKS; i++)
    {
      if (FD_ISSET(i, &readfds))
      {
        switch( sock_list[i].type)
        {
          case global_var:
            /* TODO: implement global variables */
            break;
          case listening:
            accept_module_com(i);        
            break;
          case rmulti_listen:
            handle_rmultiin(i);
            break;
          case module_com:
            handle_module_com(i);
            break;
          case rmultiin:              /*unimplemented*/
            break;
          case rmultiout:             /*unimplemented*/
            break;
          default:
            break;
            fprintf(stderr, "unhandled socket type.  type = %d\n", sock_list[i].type);
            //exit(1);
        }
      }
      //now test for timeouts
      if (sock_list[i].checktime != 0 && sock_list[i].checktime < now)
      {
        switch( sock_list[i].type )
        {
          case chirp_modlist_type:
            chirp_modlist();
            break;
          default:
            break;
        }
      }
      /* TODO: implement tests for writefds and exceptfds */
    }  
  if(now > drmilt) 
  {
    drop_rmultiin();
  }
  /*main loop ends here*/
  }
}
