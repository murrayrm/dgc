#include "sn_types.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <sys/time.h>

int recv_quant(int s, void* buf, ssize_t len, int flags){
  /* blocks until it has received exactly len bytes.  buf must have at least
   * that much room.  Do NOT call this function with a nonblocking socket
   * TODO: eliminate this function.  skynetd's main loop should be able to handle
   * receiving only 1 byte then going back to sleep
   */
  ssize_t bytes_received = 0;
  do{
    ssize_t got = recv(s, (void*)((ssize_t)buf + bytes_received), len - bytes_received, flags);
    bytes_received += got;
  }while (bytes_received < len && bytes_received > 0);
  return (bytes_received);
}

int send_quant(int s, void* buf, ssize_t len, int flags){
  /*blocks until it has sent exactly len bytes.  see recv_quant*/
  ssize_t bytes_sent = 0;
  do{
    ssize_t sent = send(s, (void*)((ssize_t)buf + bytes_sent), len - bytes_sent, flags);
    bytes_sent += sent;
  }while (bytes_sent < len && bytes_sent > 0);
  return (bytes_sent);
}

int get_dg_addr (sn_msg type, struct in_addr *my_addr, int key)
{
  //assert (sizeof (unsigned int) == 4);

  struct in_addr min_addr, max_addr;
  unsigned int min_addr_int, max_addr_int, ret_addr_int;
  if (inet_aton (MIN_ADDR, &min_addr) == 0)
    {
      perror ("get_dg_addr:inet_aton");
      return 1;
    }
  if (inet_aton (MAX_ADDR, &max_addr) == 0)
    {
      perror ("get_dg_addr:inet_aton");
      return 1;
    }

  min_addr_int = ntohl (min_addr.s_addr);
  max_addr_int = ntohl (max_addr.s_addr);
                                                                                   /* Compute the address to used based on the message type and key.  this
   * way,
   modules with different keys will never communicate with each other, and
   messages of different types can be filtered based on addres.  using
   last_type this way is a hack that relies on last_type being assigned the
   highest value of all types.  Though it is a hack, it will be obsolete
   once the rc file stuff is programmed, so I'll leave it for now
   */

   ret_addr_int = min_addr_int + (key * last_type + type) %
     (max_addr_int - min_addr_int);

   (*my_addr).s_addr = htonl (ret_addr_int);

   assert (IN_MULTICAST (ntohl ((*my_addr).s_addr)));

   return 0;
 }

/** compute port number to use in host byte order */
short unsigned int get_stream_port(sn_msg type, int key)
{
  short unsigned int ret;
  ret = STREAM_MIN_PORT + (key * last_type + type) % 
        (STREAM_MAX_PORT - STREAM_MIN_PORT);
  return ret;
}

void SNgettime(long long* DGCtime)
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  *DGCtime = (long long)tv.tv_usec + 1000000ULL * tv.tv_sec;
}
