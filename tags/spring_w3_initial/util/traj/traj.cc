#include <math.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include "frames/frames.hh"
using namespace std;
#include "traj.h"

CTraj::CTraj(int order)
  : m_order(order)
{
  m_pN = new double[TRAJ_MAX_LEN * m_order];
  m_pE = new double[TRAJ_MAX_LEN * m_order];
  m_numPoints = 0;
}

// Copy constructor
CTraj::CTraj(const CTraj & other)
{
  m_order = other.m_order;
  m_pN = new double[TRAJ_MAX_LEN * m_order];
  m_pE = new double[TRAJ_MAX_LEN * m_order];
  m_numPoints = 0;

  *this = other;
}

CTraj::CTraj(int order, istream& inputstream)
  : m_order(order)
{
	load(inputstream);
}

CTraj::CTraj(int order, char* pFilename)
  : m_order(order)
{
	ifstream inputstream(pFilename);
	load(inputstream);
}

void CTraj::load(istream& inputstream)
{
  string line;
  int d;
  
  m_numPoints = 0;
  
  // allocate memory for the data arrays
  m_pN = new double[TRAJ_MAX_LEN * m_order];
  m_pE = new double[TRAJ_MAX_LEN * m_order];
  
  
  if(!inputstream)
    {
      cerr << "CTraj couldn't read from a stream\n";
      return;
    }
  
  while(inputstream)
    {
      getline(inputstream, line, '\n');
      if(line.size() == 0)
	return;
      
      if(line[0] == '#' || line[0] == '%')
	continue;
      
      istringstream linestream(line);
      
      for(d=0; d<m_order; d++)
	linestream >> m_pN[INDEX(m_numPoints, d)];
      
      for(d=0; d<m_order; d++)
	linestream >> m_pE[INDEX(m_numPoints, d)];
      
      if(!linestream)
	return;
      
      m_numPoints++;
    }
}

CTraj &CTraj::operator=(const CTraj &other)
{
  if (&other != this) {       // avoid self-assignment
    // delete the contents of this if needed
    // copy all elements
    m_numPoints = other.m_numPoints;
    m_confidence = other.m_confidence;
    m_order = m_order;
    memcpy(m_pN, other.m_pN, sizeof(double)*m_order*TRAJ_MAX_LEN);
    memcpy(m_pE, other.m_pE, sizeof(double)*m_order*TRAJ_MAX_LEN);
  }
  
  return *this;
}

CTraj &CTraj::operator+=(const CTraj &other)
{
	if(other.m_order != m_order)
	{
		cerr << "CTraj::operator+=(): orders don't match" << endl;
		return *this;
	}
	if(other.m_numPoints + m_numPoints > TRAJ_MAX_LEN)
	{
		cerr << "CTraj::operator+=(): resulting traj would be too long" << endl;
		return *this;
	}

	for(int i=0; i<m_order; i++)
	{
		memcpy(m_pN+INDEX(m_numPoints,i), other.m_pN+INDEX(0,i), other.m_numPoints*sizeof(double));
		memcpy(m_pE+INDEX(m_numPoints,i), other.m_pE+INDEX(0,i), other.m_numPoints*sizeof(double));
	}
	m_numPoints += other.m_numPoints;

  return *this;
}


CTraj::~CTraj()
{
  delete [] m_pN;
  delete [] m_pE;
}

double CTraj::lastE()
{
  return m_pE[INDEX(m_numPoints-1, 0)];
}
double CTraj::lastN()
{
  return m_pN[INDEX(m_numPoints-1, 0)];
}

bool CTraj::inTraj(double northing, double easting)
{
  for(int i=0; i<m_numPoints; i++)
	{
		if( (northing - m_pN[INDEX(i, 0)])*(northing - m_pN[INDEX(i, 0)]) +
				(easting  - m_pE[INDEX(i, 0)])*(easting  - m_pE[INDEX(i, 0)]) <
				TRAJ_IN_PATH_RES * TRAJ_IN_PATH_RES)
      return true;
	}
  
  return false;
}

void CTraj::startDataInput(void)
{
  m_numPoints = 0;
}

void CTraj::inputNoDiffs(double northing, double easting)
{
#warning "maybe specify 'no data' for derivatives here"
  m_pN[INDEX(m_numPoints, 0)] = northing;
  m_pE[INDEX(m_numPoints, 0)] = easting;
  
  if(m_numPoints != TRAJ_MAX_LEN-1)
    m_numPoints++;
  else
    cerr << "CTraj: overflow\n";
}

void CTraj::inputWithDiffs(double* northing, double* easting)
{
  for(int i=0; i<m_order; i++)
  {
    m_pN[INDEX(m_numPoints, i)] = northing[i];
    m_pE[INDEX(m_numPoints, i)] = easting[i];
  }
  
  if(m_numPoints != TRAJ_MAX_LEN-1)
    m_numPoints++;
  else
    cerr << "CTraj: overflow\n";
}

void CTraj::shiftNoDiffs(int ptsToToss)
{
	memmove((char*)m_pN, ((char*)m_pN) + ptsToToss*sizeof(m_pN[0]), (m_numPoints-ptsToToss)*sizeof(m_pN[0]));
	memmove((char*)m_pE, ((char*)m_pE) + ptsToToss*sizeof(m_pN[0]), (m_numPoints-ptsToToss)*sizeof(m_pN[0]));
	m_numPoints -= ptsToToss;
}

// This function cuts down the current trajectory to be of length desiredDist
// (if the traj is longer), or lengthens it to be dist long
void CTraj::truncate(double desiredDist)
{
  if(m_numPoints < 2)
    return;

  int i, j;
  double d = 0.0;
  double dN, dE;
  
  for(i=0; i<m_numPoints-1; i++)
	{
		dN = m_pN[INDEX(i+1, 0)] - m_pN[INDEX(i, 0)];
		dE = m_pE[INDEX(i+1, 0)] - m_pE[INDEX(i, 0)];
		d = sqrt( dN*dN + dE*dE );
		desiredDist -= d;
      
		// we exceeded than desiredDist
		if(desiredDist < 0.0)
		{
			// we want to truncate the last segment by -desiredDist long, but along
			// the same direction
			for(j=0; j<m_order; j++)
	    {
	      m_pN[INDEX(i+1, j)] = m_pN[INDEX(i, j)] + (m_pN[INDEX(i+1, j)] - m_pN[INDEX(i, j)]) * (d+desiredDist) / d;
	      m_pE[INDEX(i+1, j)] = m_pE[INDEX(i, j)] + (m_pE[INDEX(i+1, j)] - m_pE[INDEX(i, j)]) * (d+desiredDist) / d;
	    }
	  
			// Set new point count
			m_numPoints = i + 2;
	  
			return;
		}
	}
  
  // we get here if the total length was not enough. We want to grow the last
  // segment to meet the required length. desiredDist left, so the last segment
  // should be d+desiredDist long
  for(j=0; j<m_order; j++)
	{
		m_pN[INDEX(m_numPoints-1, j)] = m_pN[INDEX(m_numPoints-2, j)] + (m_pN[INDEX(m_numPoints-1, j)] - m_pN[INDEX(m_numPoints-2, j)]) * (d+desiredDist) / d;
		m_pE[INDEX(m_numPoints-1, j)] = m_pE[INDEX(m_numPoints-2, j)] + (m_pE[INDEX(m_numPoints-1, j)] - m_pE[INDEX(m_numPoints-2, j)]) * (d+desiredDist) / d;
	}
}

void CTraj::getThetaVector(double dist, int num, double* thetavector, double bias)
{
	double delta = dist / (double)(num - 1);

	int i;
	int pointIndex;
	double distInSegment;

	double distLeftInSegment;
	double n0, e0, n1, e1;
	double nd0, ed0, nd1, ed1;
	double lenSegment;
	double distLeft;

	pointIndex = 0;
	distInSegment = 0.0;

	n0 = m_pN[INDEX(pointIndex  , 0)];
	n1 = m_pN[INDEX(pointIndex+1, 0)];
	e0 = m_pE[INDEX(pointIndex  , 0)];
	e1 = m_pE[INDEX(pointIndex+1, 0)];
	for(i=0; i<num; i++)
	{
		// compute the angle and move it to sit in [-pi..pi]
		double theta = atan2(e1-e0, n1-n0) - bias;
		thetavector[i] = atan2(sin(theta), cos(theta));

		distLeft = delta;

		while(distLeft > 0.0)
		{
			n0 = m_pN[INDEX(pointIndex  , 0)];
			n1 = m_pN[INDEX(pointIndex+1, 0)];
			e0 = m_pE[INDEX(pointIndex  , 0)];
			e1 = m_pE[INDEX(pointIndex+1, 0)];
			lenSegment = hypot(n1-n0, e1-e0);

			distLeftInSegment = lenSegment - distInSegment;
			if(distLeft < distLeftInSegment)
			{
				distInSegment += distLeft;
				break;
			}

			distLeft -= distLeftInSegment;

			pointIndex++;
			distInSegment = 0.0;
		}
	}
}

double CTraj::getLength(void)
{
	double length = 0.0;

	for(int i=0; i<m_numPoints-1; i++)
	{
		length += hypot(m_pN[INDEX(i+1, 0)] - m_pN[INDEX(i, 0)],
										m_pE[INDEX(i+1, 0)] - m_pE[INDEX(i, 0)]);
	}
	return length;
}

void CTraj::print(ostream& outstream, int numPoints)
{
  int i, d;
  
  if(numPoints == -1)
    numPoints = m_numPoints;
 
  // print a comment header at the top
  outstream << "# This is a trajectory file output by CTraj::print()." << endl;
  outstream << "# Column format is [n, nd, ndd, e, ed, edd], in SI units." << endl;

  outstream << setprecision(10);
  for(i=0; i<numPoints; i++)
    {
      for(d=0; d<m_order; d++)
	outstream << m_pN[INDEX(i, d)] << " ";
      for(d=0; d<m_order; d++)
	outstream << m_pE[INDEX(i, d)] << " ";
      outstream << endl;
    }
}

//This funstion applies a speed limit to the path.
void CTraj::speed_adjust(double speedlimit,NEcoord* pState,double aspeed)
{
  const double DECEL_RATE=2;
  int nearestOnPathIndex, i;
  double nearestDistSq;
  double currentDistSq;
  //Compute the index of the point that is closest to where we are located.
  nearestDistSq = 1.0e20;
  for(i=0; i<m_numPoints; i++)
  {
    currentDistSq =
	    pow(pState->N - m_pN[INDEX(i, 0)], 2.0) +
	    pow(pState->E - m_pE[INDEX(i, 0)], 2.0);
      
    if(currentDistSq < nearestDistSq)
    {
	    nearestDistSq = currentDistSq;
	    nearestOnPathIndex = i;
    }
  }
  
  i=nearestOnPathIndex;
  int flag=1;
  double speed,t_aN,t_aE,n_aN,n_aE,distance;
  for(i; i<m_numPoints; i++)
  {
    speed=
      sqrt(m_pN[INDEX(i,1)]*m_pN[INDEX(i,1)]+m_pE[INDEX(i,1)]*m_pE[INDEX(i,1)]);
    /*If the speed of the initial point is above the speed limit, then we
    need to decelerate to below the speed limit in a dynamically feasible
    way.*/
    if(aspeed>speedlimit)
    {
      //Check if the source traj is decelerating faster than our DECEL_RATE.
      if(aspeed<speed)
      {
        //Set the tangential acceleration to the decel rate.
        t_aN=-DECEL_RATE*m_pN[INDEX(i,1)]/speed;
        t_aE=-DECEL_RATE*m_pE[INDEX(i,1)]/speed;
        //Calculate acceleration that is normal to velocity.
        n_aN=
          -m_pE[INDEX(i,1)]*(m_pN[INDEX(i,1)]*m_pE[INDEX(i,2)]-
          m_pE[INDEX(i,1)]*m_pN[INDEX(i,2)])/(speed*speed);
        n_aE=
          m_pN[INDEX(i,1)]*(m_pN[INDEX(i,1)]*m_pE[INDEX(i,2)]-
          m_pE[INDEX(i,1)]*m_pN[INDEX(i,2)])/(speed*speed);
        //Reduce speed to match any deceleration from previous points.
        m_pN[INDEX(i,1)]=m_pN[INDEX(i,1)]*aspeed/speed;
        m_pE[INDEX(i,1)]=m_pE[INDEX(i,1)]*aspeed/speed;
        /*Normal acceleration is scaled down to match the new velocity and
        added to the tangential acceleration giving total acceleration*/
        m_pN[INDEX(i,2)]=t_aN+n_aN*aspeed*aspeed/(speed*speed);
        m_pE[INDEX(i,2)]=t_aE+n_aE*aspeed*aspeed/(speed*speed);
      }
      else aspeed=speed;
      //Calculate the new decelerated speed for the next point.
      if(i<m_numPoints-1)
      {
        distance=
          sqrt(pow(m_pN[INDEX(i+1,0)]-m_pN[INDEX(i,0)],2.0)+
		      pow(m_pE[INDEX(i+1,0)]-m_pE[INDEX(i,0)],2.0));
        aspeed=sqrt(max(aspeed*aspeed-2*DECEL_RATE*distance,0.));
      }
    }
    /*Once the speed is below the speed limit it is only necessary to make
    sure the path does not acellerate back above the speed limit.*/
    else if(speed>speedlimit)
    {
      //Calculate acceleration that is normal to velocity.
      n_aN=
        -m_pE[INDEX(i,1)]*(m_pN[INDEX(i,1)]*m_pE[INDEX(i,2)]-
        m_pE[INDEX(i,1)]*m_pN[INDEX(i,2)])/(speed*speed);
      n_aE=
        m_pN[INDEX(i,1)]*(m_pN[INDEX(i,1)]*m_pE[INDEX(i,2)]-
        m_pE[INDEX(i,1)]*m_pN[INDEX(i,2)])/(speed*speed);
      //The speed is reduced to the speed limit.
      m_pN[INDEX(i,1)]=m_pN[INDEX(i,1)]*speedlimit/speed;
      m_pE[INDEX(i,1)]=m_pE[INDEX(i,1)]*speedlimit/speed;
      /*Tangential acceleration is set to zero, and normal acceleration is
      scaled down to match the new velocity.*/
      m_pN[INDEX(i,2)]=n_aN*speedlimit*speedlimit/(speed*speed);
      m_pE[INDEX(i,2)]=n_aE*speedlimit*speedlimit/(speed*speed);
    }
    //Points that already have speed below the speed limit are not changed.
  }
}

// Returns the index of the closest point in this traj to the provided
// (N,E). Note: this function returns the closest (spatially) point. It doesn't
// look to see how close we actually are. So even if we're miles away from this
// traj or facing the wrong way, this function will still return just the
// spatially closest point.
int CTraj::getClosestPoint(double n, double e)
{
	int			i;

	// index of the point on the trajectory that's closest to the current system
	// point, and the squared distance from the system point to that point on the
	// trajectory
	int     nearestIndex = 0;
	double	nearestDistSq;
	double	currentDistSq;

	// Compute the nearest point on the trajectory to where we currently are
	nearestDistSq = 1.0e20;
	for(i=0; i<getNumPoints(); i++)
	{
		currentDistSq =
			pow(n - getNorthing(i),2.0) +
			pow(e	- getEasting(i) ,2.0);

		if(currentDistSq < nearestDistSq)
		{
			nearestDistSq = currentDistSq;
			nearestIndex = i;
		}
	}

	return nearestIndex;
}
