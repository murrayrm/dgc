#include <math.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include "frames/frames.hh"
#include "CMap.hh"
using namespace std;
#include "traj.h"
#include "trajMap.hh"


/*Evaluates traj through a CMap layer and returns 1 if any point in the traj
  lies in a cell with goodness below the cutoff level.  Currently is only
  defined for a layer of doubles.  Default cutoff is 0.1 m/s*/
bool CTrajMap::isTrajObstructed(CMap * map, int layer, double cutoff)
{
  for(int i = 0; i < m_numPoints; i++)
    if(map->getDataUTM<double>(layer, m_pN[INDEX(i, 0)], m_pE[INDEX(i, 0)])
        <= cutoff)
      return 1;
  return 0;
}

/*Evaluates traj through a CMap layer and returns the time it would take to
  complete the traj if at every point in the traj the vehicle traveled at the
  speed specified by the map cell that point falls within.  Only defined for a
  layer of doubles.  For trajs with known and constant spacing, specifing
  spacing will save calculations.*/
double CTrajMap::evalCost(CMap * map, int layer, double spacing)
{
  int i;
  double cost = 0;
  if(spacing <=0)
  {
    for(i = 0; i < m_numPoints - 1; i++)
    {
      spacing = hypot(m_pN[INDEX(i + 1, 0)] - m_pN[INDEX(i, 0)],
                      m_pE[INDEX(i + 1, 0)] - m_pE[INDEX(i, 0)]);
      cost += spacing / map->getDataUTM<double>(layer, m_pN[INDEX(i, 0)],
                                                m_pE[INDEX(i, 0)]);
    }
    cost += spacing / map->getDataUTM<double>(layer, m_pN[INDEX(i, 0)],
                                              m_pE[INDEX(i, 0)]);
  }
  else
    for(i = 0; i < m_numPoints - 1; i++)
      cost += spacing / map->getDataUTM<double>(layer, m_pN[INDEX(i, 0)],
                                                m_pE[INDEX(i, 0)]);
  return cost;
}
