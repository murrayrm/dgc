#include "traj.h"
#include "frames/coords.hh"
#include <fstream>
using namespace std;

int main(void)
{
  cout << "Reading in trajfiles/elmirage_qid.traj." << endl;
	ifstream infile("trajfiles/elmirage_qid.traj");
  CTraj traj(3,infile);

  /*cout << "Printing the traj." << endl;
  traj.print();*/
  
  cout << "Speed adjusting traj." << endl;
  NEcoord current_position(3834417.696,442695.076);
  traj.speed_adjust(5.,&current_position,10.);
  //traj.print();

  cout << "Saving the speed adjusted traj to output.traj." << endl;
  ofstream outfile("output.traj");
  traj.print(outfile);

  cout << "trajfiles/plottraj.m can be used to graph the output traj."<< endl <<
    "Exiting." << endl;
  return 0;
}
