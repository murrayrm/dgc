#ifndef FUSIONMAPPER_HH
#define FUSIONMAPPER_HH

#include <unistd.h>
#include <pthread.h>

#include "StateClient.h"
#include "DGCutils"

#include "CMapPlus.hh"
#include "rddf.hh"
#include "CCorridorPainter.hh"
#include "CCostPainter.hh"
//#include "RoadFinding.hh"


using namespace std;

class FusionMapper : public CStateClient {
public:
  FusionMapper(int skynetKey);
  ~FusionMapper();
  
  void ActiveLoop();
  
  void ReceiveDataThread();

  void ReceiveRoadDataThread();

private:

  char* m_pMapDelta;
  double* m_pRoadInfo;

  pthread_mutex_t m_mapMutex;

  CMapPlus fusionMap;
  
  RDDF fusionRDDF;
  
  CCorridorPainter fusionCorridorPainter;
  CCostPainter fusionCostPainter;

  int layerID_ladarElev;
  double noDataVal_ladarElev;
  double outsideMapVal_ladarElev;
  
  int layerID_cost;
  double noDataVal_cost;
  double outsideMapVal_cost;
  
  double rddf_insideVal;
  double rddf_outsideVal;
  double rddf_tracklineVal;
  double rddf_edgeVal;

  // Temporary way to debug
  ofstream delta_log;

};

#endif
