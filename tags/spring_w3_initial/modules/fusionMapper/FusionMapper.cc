#include "FusionMapper.hh"

#define MAX_DELTA_SIZE 999999
#define MAX_ROAD_SIZE 1000

#define NOLADAR

FusionMapper::FusionMapper(int skynetKey) 
  : CSkynetContainer(SNfusionmapper, skynetKey), delta_log("logs/delta.log") {
  printf("Welcome to fusionmapper init\n");

  delta_log << "# fusionMapper produced this.  Time then size." << endl;
    
  //m_pMapDelta = new char[MAX_DELTA_SIZE];
  m_pRoadInfo = new double[MAX_ROAD_SIZE];

  int numRows = 150;
  int numCols = 150;
  double resRows = 1;
  double resCols = 1;
  
  fusionMap.initMap(0,0, 
		  numRows, numCols,
		  resRows, resCols,
		  0);

  layerID_ladarElev = fusionMap.addLayer<double>(0,
						 -1);
  layerID_cost    = fusionMap.addLayer<double>(1e-3, 1e-4);

//  layerID_blurred = fusionMap.addLayer<double>(1e-3, 1e-4);
  
  fusionCorridorPainter.initPainter(&(fusionMap), 
				    layerID_cost, 
				    &(fusionRDDF),
				    1.0,
				    1e-3,
				    5.0,
				    4.5,
				    0.5,
				    0);
				    // 1);
/*  fusionBlurredPainter.initPainter(&(fusionMap), 
				   layerID_blurred, 
				   &(fusionRDDF),
				   1.0,
				   1e-3,
				   10.0,
				   4.5,
				   0.5,
				   1);
				    
				    rddf_insideVal,
				    rddf_outsideVal,
				    rddf_tracklineVal,
				    rddf_edgeVal);
				    */
  fusionCostPainter.initPainter(&(fusionMap),
				layerID_cost,
				layerID_ladarElev
				);  

  //fusionMap.initLayerLog(layerID_cost, "temp");
  //fusionMap.startLayerLog(layerID_cost);
  DGCcreateMutex(&m_mapMutex);
}


FusionMapper::~FusionMapper() {
  //FIX ME
  DGCdeleteMutex(&m_mapMutex);
  printf("Thank you for using the fusionmapper destructor\n");
}


void FusionMapper::ActiveLoop() {
  //TODO - Give this a sparrow/sngui display
  printf("Entering FusionMapper active-loop\n");
  printf("Press Ctrl+C to quit\n");

  int socket_num = m_skynet.get_send_sock(SNfusiondeltamap);

  void* deltaPtr = NULL;
  int deltaSize = 0;

  //printf("%s [%d]: \n", __FILE__, __LINE__);

  // int mapDeltaSocket = m_skynet.listen(SNladardeltamap, SNladarfeeder);
  //  cout <<"mapDeltaSocket is: " << mapDeltaSocket << endl;
  //int mapDeltaSocket = m_skynet.listen(SNstereodeltamap, MODstereofeeder);

  /*  if(mapDeltaSocket < 0)
    cerr << "CPlannerModule::getMapargasfgsfgsdfgDeltasThread(): skynet listen returned error" << endl;
  */

  double UTMNorthing;
  double UTMEasting;
  int int_northing[4];
  int int_easting[4];
  
  double cellval;

  NEcoord exposedRow[4], exposedCol[4];

  
  while(true) {
    //cout << "m_pMapDelta was : "<< m_pMapDelta << endl;
    //int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pMapDelta, MAX_DELTA_SIZE, 0);
    //cout << "m_pMapDelta is : " << m_pMapDelta << endl; 
    //cout <<"numreceived is :" <<numreceived << endl;

    UpdateState();


#warning "Should change updateVehicleLoc to be updateMapCenter"
    //check to see if we've moved enough to make it worth moving
    //acquire mutex?
    DGClockMutex(&m_mapMutex);
    fusionMap.updateVehicleLoc(m_state.Northing, m_state.Easting);
    fusionMap.getExposedRowBoxUTM(exposedRow);
    fusionMap.getExposedColBoxUTM(exposedCol);
    fusionCorridorPainter.paintChanges(exposedRow, exposedCol);    
    deltaPtr = fusionMap.serializeDelta<double>(layerID_cost, deltaSize);
    DGCunlockMutex(&m_mapMutex);    

    //   cout << "Northing is: "<< m_state.Northing << endl;
    // cout <<"Easting is: " << m_state.Easting << endl;



    /*
    printf("FM Points are: ");
    for(int i=0; i<4; i++) {
      //printf("(%lf, %lf), ", exposedRow[i].N, exposedRow[i].E);
      fusionMap.UTM2Win(exposedCol[i].N, exposedCol[i].E, &(int_northing[i]), &(int_easting[i]));
      printf("(%d, %d), ", int_northing[i], int_easting[i]);
    }
    printf("\n");
    */
    //printf("At %lf, %lf\n", m_state.Northing, m_state.Easting);

    //printf("%s [%d]: \n", __FILE__, __LINE__);

    //fusionCorridorPainter.paintChanges();


//    fusionBlurredPainter.paintChanges_TracklineSlope();    




    //fusionMap.saveLayer<double>(layerID_blurred, "blurred");
    //printf("%s [%d]: \n", __FILE__, __LINE__);






    //printf("%s [%d]: \n", __FILE__, __LINE__);

//     printf("Delta is %d\n", deltaSize);    
    int sent_OK;

    //code to send delta
    if(deltaSize>20)
		{
		  sent_OK = m_skynet.send_msg(socket_num, deltaPtr, deltaSize, 0);
		  printf("Sent was %d\n", sent_OK);
      unsigned long long now;
      DGCgettime(now);
      delta_log << now << " " << sent_OK << endl;
		}
		else
		{
		  //printf("deltaSize <= 20. Didn't send\n");
		}

    //fusionMap.applyDelta<double>(layerID_blurred, deltaPtr, deltaSize);

    //fusionMap.saveLayer<double>(layerID_blurred, "blurred2");
    
    //printf("sleepy\n");
    //sleep(1);

    
    //release mutex?
    //some sleep?
    //printf("Sleepy sleepy\n");
    //sleep(1);
    usleep(10000);
    //printf("%s [%d]: \n", __FILE__, __LINE__);
    fusionMap.resetDelta(layerID_cost);
	}

}


void FusionMapper::ReceiveDataThread() {
  int mapDeltaSocket = m_skynet.listen(SNladardeltamap, SNladarfeeder);
  double UTMNorthing;
  double UTMEasting;
  
  double cellval;
  int count=0;

  while(true) { 
    m_pMapDelta = new char[MAX_DELTA_SIZE];
    cout << "about to recieve message" << endl;
    int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pMapDelta, MAX_DELTA_SIZE, 0);
    cout << "m_pMapDelta is : " << m_pMapDelta << endl; 
    cout <<"numreceived is :" <<numreceived << endl;
    if(numreceived>0)
      {
	//fusionMap.applyDelta<double>(layerID_ladarElev, m_pMapDelta, numreceived);
	printf("got ladar delta%d\n", count++);
	int cellsize = fusionMap.getDeltaSize(m_pMapDelta);
	DGClockMutex(&m_mapMutex);
	//	cout << "cellsize is: " << cellsize << endl;
	for(int i=0; i<cellsize; i++) {
	  //  cout << "i is: "<< i<< endl;
	  cellval = fusionMap.getDeltaVal<double>(i, layerID_ladarElev, m_pMapDelta, &UTMNorthing, &UTMEasting);
	  fusionMap.setDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting, cellval);
	  fusionCostPainter.paintChanges(UTMNorthing, UTMEasting);
	  //fusionMap.setDataUTM_Delta<double>(layerID_cost, UTMNorthing, UTMEasting, cellval);
	}
	DGCunlockMutex(&m_mapMutex);
       	delete m_pMapDelta;	
	//printf("Applied delta of size %d\n", numreceived);
	//d.myCMap.updateVehicleLoc(m_state.Northing,m_state.Easting);
      }	    
	  //printf("%s [%d]: \n", __FILE__, __LINE__);
  }

    /*
  printf("Data receiving yet\n");


  if(mapDeltaSocket < 0)
    cerr << "CPlannerModule::getMapargasfgsfgsdfgDeltasThread(): skynet listen returned error" << endl;

  printf("%d [%s]\n", __LINE__, __FILE__);

  double UTMNorthing;
  double UTMEasting;
  
  double cellval;

  while(true) {
    printf("Attempting to get ladar delta\n");
      //UpdateState();
    int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pMapDelta, MAX_DELTA_SIZE, 0);		  
    if(numreceived>0)
      {
	//fusionMap.applyDelta<double>(layerID_ladarElev, m_pMapDelta, numreceived);
	printf("Applying ladar deltA %d\n", numreceived);
	int cellsize = fusionMap.getDeltaSize(m_pMapDelta);
	for(int i=0; i<cellsize; i++) {
	  cellval = fusionMap.getDeltaVal<double>(i, layerID_ladarElev, m_pMapDelta, &UTMNorthing, &UTMEasting);
	  fusionMap.setDataUTM<double>(layerID_ladarElev, UTMNorthing, UTMEasting, cellval);
	  fusionCostPainter.paintChanges(UTMNorthing, UTMEasting);
	  fusionMap.setDataUTM_Delta<double>(layerID_cost,UTMNorthing, UTMEasting, cellval);
	  printf("Set %lf, %lf to %lf\n", UTMNorthing, UTMEasting, cellval);
	}


	
	//printf("Applied delta of size %d\n", numreceived);
	//d.myCMap.updateVehicleLoc(m_state.Northing,m_state.Easting);
      }		
    // update map at 10hz
    usleep(10000);
  }
    */
}


void FusionMapper::ReceiveRoadDataThread() {
  /*
  int roadInfoSocket = m_skynet.listen(SNroadboundary, SNroadfinding);

  if(roadInfoSocket < 0)
    cerr << "error - FIXME" << endl;

  while(true) {
    printf("Attempting to get road info\n");
    int numreceived = m_skynet.get_msg(roadInfoSocket, m_pRoadInfo, MAX_ROAD_SIZE, 0);		  
    if(numreceived>0)
      {
	int numScanLines = m_pRoadInfo[0];
	if(numreceived != sizeof(double)*(numScanLines*6+1)) {
	  cerr << "error -FIXME" << endl;
	} else {
	  RoadBoundary* roadArray = new RoadBoundary[numScanLines];
	  for(int i=0; i<numScanLines; i++) {
	    roadArray[i].left = new XYZcoord(m_pRoadInfo[i*6+1], 
					     m_pRoadInfo[i*6+2], 
					     m_pRoadInfo[i*6+3]);
	    roadArray[i].right = new XYZcoord(m_pRoadInfo[i*6+4], 
					      m_pRoadInfo[i*6+5], 
					      m_pRoadInfo[i*6+6]);
	    //do stuff with the road array here
	    //like put it in the map
	  }
	}
      }

  }		
  // update map at 10hz
  usleep(10000);
  */
}

