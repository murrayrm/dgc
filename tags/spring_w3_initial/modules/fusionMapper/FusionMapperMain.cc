#include <stdlib.h>
#include <stdio.h>

#include "FusionMapper.hh"
#include <DGCutils>

#include <getopt.h>

enum {
  OPT_USELADAR,

  NUM_OPTS
};


int main(int argc, char** argv) {
  int optUseLADAR=0;

  //Add options later
  int c;
  int digit_optind = 0;


  static struct option long_options[] = {
    //Options that require arguments
    {"ladar", no_argument, &optUseLADAR, 1},
    {0,0,0,0}
  };
  
  while (1) {
    int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    
    c = getopt_long_only(argc, argv, "", long_options, &option_index);
    if(c == -1) break;

    switch(c) {
    default:
      if(c!=0) {
	printf("Unknown option %d!\n", c);
	//printStereoHelp();
	exit(1);
      }
    }
  }

  //Setup skynet
  int intSkynetKey = 0;
  char* ptrSkynetKey = getenv("SKYNET_KEY");
  
  if(ptrSkynetKey == NULL) {
    //FIX ME
    printf("no sn key");
    return 0;
  } else {
    printf("Welcome to FusionMapperMain\n");
    intSkynetKey = atoi(ptrSkynetKey);
    FusionMapper fusionMapperObj(intSkynetKey);
    
    //start extra threads here

    printf("here we go actiiiiiiiiiive\n");

    if(optUseLADAR) {
      printf("USING LADAR!\n");
      DGCstartMemberFunctionThread(&fusionMapperObj, &FusionMapper::ReceiveDataThread);
    } else {
      printf("NOT USING LADAR - to use ladar, use --ladar\n");
    }

    fusionMapperObj.ActiveLoop();
  }

  return 0;
}
