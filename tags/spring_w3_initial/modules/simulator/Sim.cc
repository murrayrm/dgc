#include "Sim.hh"
#include "DGCutils"

SIM_DATUM d;

int QUIT_PRESSED = 0;
int PAUSED       = 0;
int RESET        = 0;

#warning 
asim::asim(int sn_key)
	: m_skynet(SNasim, sn_key)
{
  // clear the state struct and actuators
  d.accel_cmd = 0;
  d.steer_cmd = 0;


	SimInit();

	DGCcreateMutex(&m_stateMutex);
	DGCcreateMutex(&m_drivecmdMutex);

	broadcast_statesock = m_skynet.get_send_sock(SNstate);
}

void asim::SimInit(void)
{
	DGCgettime(d.lastUpdate);
	simState.readFile("simInitState.dat");
	simEngine.Init(simState, 0.0);
}

void asim::StateComm()
{
  cout << "asim::StateComm called" << endl;
  
  int getstatesock = m_skynet.listen(SNreqstate, ALLMODULES);
  if(getstatesock < 0)
    cerr << "asim::StateComm(): skynet listen returned error" << endl;
  
  int statesock = m_skynet.get_send_sock(SNstate);
  if(statesock < 0)
    cerr << "asim::StateComm(): skynet get_send_sock returned error" << endl;
  
  char getstatedummy;
  
	ofstream log("logs/message.log");
  while(true)
    {
			log << "getting req" << endl;
      if(m_skynet.get_msg(getstatesock,
			  &getstatedummy,
			  sizeof(getstatedummy), 0) != sizeof(getstatedummy))
	{
	  cerr << "asim didn't receive the right number of bytes in the getstate command" << endl;
	  continue;
	}

      log << "sending state" << endl;
      if(m_skynet.send_msg(statesock,
			   &d.SS,
			   sizeof(d.SS), 0, &m_stateMutex) != sizeof(d.SS))
	{
	  cerr << "asim didn't send the right number of bytes in the state" << endl;
	}
      log << "sent state" << endl;
    }
}

void asim::DriveComm() 
{
  cout << "asim::DriveComm called" << endl;
  
  int cmdsock = m_skynet.listen(SNdrivecmd, SNtrajfollower);
  if(cmdsock < 0)
    cerr << "asim::DriveComm(): skynet listen returned error" << endl;

  drivecmd_t my_command;
  
  while(true)
    {
      if(m_skynet.get_msg(cmdsock, &my_command, sizeof(my_command), 0) != sizeof(my_command))
	cerr << "asim::DriveComm() didn't receive the right number of bytes in the drive command" << endl;

      double steer_Norm, brake_Norm, gas_Norm, accel_Norm;

      if(my_command.my_actuator == steer) {
	steer_Norm = my_command.number_arg;
      }

      if(my_command.my_actuator == accel) {
	accel_Norm = my_command.number_arg;
      }

      d.steer_cmd = steer_Norm;
      d.accel_cmd = accel_Norm;

      // Adding commands to the simulator
      DGClockMutex(&m_drivecmdMutex);
      simEngine.SetCommands(d.steer_cmd, d.accel_cmd);
      DGCunlockMutex(&m_drivecmdMutex);
    }
}

void asim::broadcast()
{
	if (m_skynet.send_msg(broadcast_statesock,
				&d.SS,
				sizeof(d.SS),
				0,
				&m_stateMutex)
			!= sizeof(d.SS))
	{
		cerr << "asim::broadcast(): didn't send right size state message" << endl;
	}
}

void asim::Active() 
{
  unsigned long long dt;
  unsigned long long biasedTime;
  cout << "entering asim::Active loop" << endl;
  
  while (true) {
    usleep(10000);
    
    // handle time update
    DGCgettime(dt);    
    dt -= d.lastUpdate;
    DGCgettime(d.lastUpdate);

    if( !PAUSED ) 
    {
      // now perform state update with known time step
      DGClockMutex(&m_drivecmdMutex);
      simEngine.RunSim(DGCtimetosec(dt));
      DGCunlockMutex(&m_drivecmdMutex);
    } 
    if( RESET )
    {
      SimInit();
      RESET = 0;
    }

    updateState();
    broadcast();

    
  }
}

void asim::updateState(void)
{
	simState = simEngine.GetFrontState();

  // Updating the state struct
	DGClockMutex(&m_stateMutex);
  d.SS.Northing = simState.x;
  d.SS.Easting  = simState.y;
  d.SS.Vel_N    = simState.xd;
  d.SS.Vel_E    = simState.yd;
  d.SS.Acc_N    = simState.xdd;
  d.SS.Acc_E    = simState.ydd;
	d.SS.YawRate  = simState.yawd;	
	d.SS.Yaw      = simState.yaw;

	d.SS.Altitude = 0.0;
	d.SS.Acc_D    = 0.0;
	d.SS.YawAcc   = 0.0;
	d.SS.RollAcc  = 0.0;
	d.SS.PitchAcc = 0.0;
	d.SS.Vel_D    = 0.0;
  d.SS.PitchRate= 0.0;
  d.SS.RollRate = 0.0;
  d.SS.Pitch    = 0.0;
  d.SS.Roll     = 0.0;

  DGCgettime(d.SS.Timestamp);
	DGCunlockMutex(&m_stateMutex);
}
