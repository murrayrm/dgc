// *************************************
// SimModel                            *
// Haomiao Huang                       *
// 1/15/2005                           *
// *************************************

// This is the model that the simulator updates/runs to get vehicle state.  
// The model is a dynamic bicycle model that models the forces acting upon
// the tires (lateral and longitudinal) to generate the state of the vehicle.
// Sideslip is included.  For more information on the model itself see Wiki
// documentation. 

// TODO: Turn into command-line parameter
int FORCE_NO_SIDESLIP = 0;

#include "SimModel.hh"
#include "rddf.hh"
#include <unistd.h>
#include <fstream>
using namespace std;

void StateReport::readFile(char* pFilename)
{
  ifstream infile(pFilename);

  char * frame_from_rddf_file = new char[256]; // whether the initial conditions aree in absolute or relative coords
  
  string frame(frame_from_rddf_file);
  unsigned int start_point;

  infile >> frame;
  
  if (frame == "absolute")
  {
    cout << "\nUSING ABSOLUTE INITIAL CONDITIONS!";
      
    infile >> x >> xd >> xdd >>
      y >> yd >> ydd >> yaw >> yawd >> phi;

    cout << "\nx:       " << x 
         << "\nxd:      " << xd
         << "\nxdd:     " << xdd
         << "\ny:       " << y
         << "\nyd:      " << yd
         << "\nydd:     " << ydd
         << "\nyaw:     " << yaw
         << "\nyawd:    " << yawd
         << "\nphi:     " << phi << endl;

  }
  else if (frame == "relative2")
  {
    cout << "\nUSING RELATIVE2 INITIAL CONDITIONS!";
    double WPrelX, WPrelY, relYaw, firstN, firstE;
		double speed_forward;

    infile >> start_point 
           >> WPrelX >> WPrelY >> relYaw
           >> speed_forward;

    // get values of x, y, and yaw from rddf.dat
    RDDF rddf("rddf.dat");
      
    // check the start_point to make sure it's not too big or small
    int num_points = rddf.getNumTargetPoints();
    if (start_point < 0 || start_point > num_points-2)
    {
      cerr << "ERROR:  start_point = " << start_point 
           << " but num_points-2 = " 
           << num_points-2 << endl;
      while(true)
      {
        cout << "."; cout.flush();
      }
    }

    firstN = rddf.getWaypointNorthing(start_point);
    firstE = rddf.getWaypointEasting(start_point);

    double cor_theta = atan2(rddf.getWaypointEasting(start_point+1)  - firstE,
                             rddf.getWaypointNorthing(start_point+1) - firstN);

    x  = firstN + WPrelX*cos(cor_theta) - WPrelY*sin(cor_theta);
    y  = firstE + WPrelX*sin(cor_theta) + WPrelY*cos(cor_theta);
    yaw = cor_theta + relYaw;
    xd = speed_forward*cos(yaw);
    yd = speed_forward*sin(yaw);
    xdd = ydd = 0.0;

    yawd = 0.0;
    phi  = 0.0;
  }
  else if (frame == "relative")
  {
    cout << "\nUSING RELATIVE INITIAL CONDITIONS!";
    double relN, relE, relYaw, firstN, firstE;

    infile >> start_point 
           >> relN >> xd >> xdd 
           >> relE >> yd >> ydd
           >> relYaw >> yawd >> phi; 

    cout << "\nstart_point: " << start_point 
         << "\nrelN:        " << relN 
         << "\nxd:          " << xd
         << "\nxdd:         " << xdd
         << "\nrelE:        " << relE
         << "\nyd:          " << yd
         << "\nydd:         " << ydd
         << "\nrelYaw:      " << relYaw
         << "\nyawd:        " << yawd
         << "\nphi:         " << phi << endl;
      
    // get values of x, y, and yaw from rddf.dat
    RDDF rddf("rddf.dat");
      
    // check the start_point to make sure it's not too big or small
    int num_points = rddf.getNumTargetPoints();
    if (start_point < 0 || start_point > num_points-2)
    {
      cerr << "ERROR:  start_point = " << start_point 
           << " but num_points-2 = " 
           << num_points-2 << endl;
      while(true)
      {
        cout << "."; cout.flush();
        usleep(100000);
      }
    }


    firstN = rddf.getWaypointNorthing(start_point);
    firstE = rddf.getWaypointEasting(start_point);
    double yaw_of_corridor = atan2(rddf.getWaypointEasting(start_point+1)  - firstE,
                                   rddf.getWaypointNorthing(start_point+1) - firstN);
      
    x = firstN + relN;
    y = firstE + relE;
    yaw = yaw_of_corridor + relYaw;
  }
  else
  {
#warning "do something more elegant here... like make asim stop execution"
    cout << "THE FIRST LINE OF simInitState.dat MUST BE EITHER relative OR absolute\n";
    cout << "The problem is, it's currently \"" << frame << "\"\n"
         << "See the README." << endl;
    while(true)
    {
      cout << "."; cout.flush();
      usleep(100000);
    }
  }

  
  sleep(1);
}


// ****** Private Functions *******

// void SimModel::dTire_Long(double* forces, double* dforces)
// *********************************************************
// This function simulates the engine dynamics to generate the derivatives of  
// longitudinal force on the front and rear tires.  Current implementation
// is nothing.  Will put in actual dynamics when we get info from STI.
void SimModel::dTire_Long(double* forces, double* planar){
  double vlon = planar[VLON_I];
  double w_f, w_engine, w_wheel, throt_act, throttle, brake;
  double Td, Tf, Te, T_wheel, F_eng, R_we;
  double F_brake, b_act;
  
  if (pedal  >= 0 ){
    throttle = fmin(pedal, 1);
    brake = 0;
  }else {
    throttle = 0;
    brake = fmax(pedal, -1);
  }

  //cout<<"brake: "<<brake<<" th "<<throttle<<endl;

  // get the speeds of everything
  R_we =  R_TC * R_TRANS1 * R_DIFF * R_TRANSFER;
  w_wheel = vlon / R_WHEEL;
  w_engine = w_wheel * R_we;
  w_f = fmax(w_engine, W_IDLE);

  // calculate engine forces now
  // get the nonlinear throttle mapping
  throt_act = 1 - exp( -K_T1 * pow(throttle,K_T2));

  // Get the torques and convert to newton-m from lb-ft
  Tf = FUDGE * (K_F1 + K_F2  * w_f  + K_F3 * pow(w_f,2)) * LBFT2NM;
  Td = FUDGE * (K_F4 + K_F5  * w_f + K_F6 * pow(w_f,2))  * LBFT2NM;

  // get the resultant torque from throttle setting
  Te = Tf * throt_act + Td * (1 - throt_act); 

  // get the torque at the wheel
  T_wheel = Te * R_we; 
  
  // force is torque * wheel radius
  F_eng = T_wheel * R_WHEEL; 

  // calculate brake forces
  // invert brake to go from 0 to 1 in stead of -1
  brake = -brake;
  b_act = 1 - exp( -K_B1 * pow(brake,K_B2));
  if(vlon > V_EPS){
    F_brake = b_act * BF_MAX;
  }else if (vlon < - V_EPS){
    F_brake = -b_act * BF_MAX;
  }else{
    F_brake = 0;
  }

  //cout<<"Fe: "<<F_eng<<" Fb: "<<F_brake<<endl;

  // resultant force of wheels, two-wheel drive, so just use rear for now
  forces[FRONT] = 0;
  forces[REAR] = F_eng + F_brake;

  //cout<<"vlon: "<<vlon<<endl;
}

// void SimModel::FTire_Lat(double* forces)
// ********************************************************
// This model calculates the lateral tire forces on the tires based on
// the current state of the vehicle and the current steering angle.  
// The model used is Pacejka's Magic Formula tire model (yes, that's it's
// real name).  Details on the model implementation can be found in Wiki 
// models documentation. 
void SimModel::FTire_Lat(double* planar, double phi, double* forces){

  double alpha[NUM_TIRES];

  // first calculate sideslip angles
  sideslip(planar, phi, alpha);

  // now use pajecka to calculate the tire forces
  forces[FRONT] = pacejka(alpha[FRONT]);
  forces[REAR] = pacejka(alpha[REAR]);
}

// void SimModel::dPhi(double* phi, double* dphi)
// *********************************************
// Calculates the derivative of the steering angle based on the steering
// dynamics.  Currently model as first-order lag, but changing the model
// can be easily done by changing the equations here.
// dphi = -tau * phi + tau  * u
// phi: steering angle
// tau: lag time constant
// u: command steering angle
// Note: commanded steering angle is in radians, not normalized
void SimModel::dPhi(double phi, double& dphi){

  dphi = - TAU_PHI * phi + TAU_PHI * steer_phi;

  return;
}

// void dPlanar_State
// *************************************************************
// Takes the current planar state of the vehicle, the engine tire forces,
// and the current steering angle and calculates the derivatives of the planar
// state (x, y, theta, thetadot, lateral velocity, long. velocity). The model
// used is a dynamic planar bicycle that uses nonlinear tire forces.  The
// model is described in greater detail on Wiki documentation. 
void SimModel::dPlanar_State(double* planar, double* ln_forces, 
			     double phi, double* dplanar){

  double F_lat[NUM_TIRES];
  double x, y, th, thd, vlat, vlon;

  x = planar[X_I]; 
  y = planar[Y_I]; 
  th = planar[TH_I]; 
  thd = planar[THD_I]; 
  vlat = planar[VLAT_I]; 
  vlon = planar[VLON_I]; 

  // get the lateral tire forces
  FTire_Lat(planar, phi, F_lat);

  // calculate the derivatives
  // See the documentation in the Wiki for details on equations
  dplanar[TH_I] = thd;
  dplanar[X_I] = vlon * cos(th) - vlat * sin(th);
  dplanar[Y_I] = vlon * sin(th) + vlat * cos(th);
  dplanar[VLAT_I] = (ln_forces[FRONT] * phi + F_lat[FRONT] + F_lat[REAR]) / 
    VEHICLE_MASS - thd * vlon;
  dplanar[VLON_I] = (ln_forces[FRONT] + ln_forces[REAR] - F_lat[FRONT] * 
		     phi) / VEHICLE_MASS + vlat * thd;
  dplanar[THD_I] = (L_f  * ln_forces[FRONT] * phi  + L_f * F_lat[FRONT] - 
		    L_r * F_lat[REAR]) / I_VEH;
}

// void sideslip(double* planar, double steer, double* alpha)
// ***************************************************************
// Calculates the front and rear sideslip angles based on the current
// planar state of the vehicle.  See Wiki for detailed documentation.
void SimModel::sideslip(double* planar, double phi, double* alpha){
  double vlat, vlon, thd;

  vlat = planar[VLAT_I];
  vlon = planar[VLON_I];
  thd = planar[THD_I];

  alpha[FRONT] = phi - atan2(L_f * thd + vlat, vlon);
  alpha[REAR] = atan2(L_r * thd - vlat, vlon);  
}

// void pacejka(double alpha, double N)
// *********************************************************************
// Calculate the lateral tire forces based on slip angle and normal load
// Uses the Pacejka Magic Formula (empirical formula with data from STI).
double SimModel::pacejka(double alpha){
  double Fy;
  Fy = Dy * sin(Cy * atan(By * alpha - Ey *(By * alpha - atan(By * alpha))));

  return Fy;
}

// ****** Public Functions ********

// Constructor
SimModel::SimModel(){

}

// Destructor
SimModel::~SimModel(){
}

// void Init(StateReport& initial, double iTime);
// *********************************************
// Initialize the simulator to the conditions as set in the struct
// and also sets the initial time. 
#warning: set initial lateral velocity to 0
void SimModel::Init(StateReport& initial, double iTime)
{ 
  // debug
  //max_step = 0;

#warning: change init to come over skynet?
  state.simX = initial.x - L_f * cos(initial.yaw);
  state.simY = initial.y -  L_f * sin(initial.yaw);
  state.simTheta = initial.yaw;
  state.simDTheta = initial.yawd;
  state.simVLon = sqrt(powf(initial.xd, 2) + powf(initial.yd, 2));
  state.simVLat = 0;
  state.simPhi = initial.phi;
  
#warning: change initialization of actuator commands
  // initializes to no brakes/throttle and no steering
  steer = 0;
  pedal = -1;

  t_last = iTime;

}

//void RunSim(double currTime, double SteerCmd, double PedalCmd)
//*************************************************************
// Runs the simulation forward in time.  Sets the actuator commands and then
// generates the derivatives using private functions that define the dynamics.
// Integrates the derivatives forward using a 4th-order Runge Kutta solver.
// Quick overview of Runge-Kutta Integration:
// Method of integrating by averaging derivatives across a time step
// ydot = f(t_n, y_n)
// k1 = f(t_n, y_n)
// k2 = f(t_n + dt/2, y_n + k1 * dt/2)
// k3 = f(t_n + dt/2, y_n + k2 * dt/2)
// k4 = f(t_n + dt, y_n + k3 * dt)
// y_n+1 = y_n + dt  * (k1 + 2*k2 + 2 * k3 + k4) / 6
void SimModel::RunSim(double step){

  int i; // index for counting

  double F_tlon[NUM_TIRES];  // array containing tire long. forces 
  double PState[NUM_STATES];    // array of planar state
  double phi;                   // steering angle 

  // auxiliary copies of the state arrays for manipulation later 
  //double T_aux[NUM_TIRES];
  double P_aux[NUM_STATES];
  double F_aux[NUM_TIRES];
  double phi_aux;

  // arrays for storing calculated derivatives
  double k1[NUM_STATES], k2[NUM_STATES], k3[NUM_STATES], k4[NUM_STATES];
  double kp1, kp2, kp3, kp4;

  // now set the variables in the array
  // All computational operations will be on the array rather than on the 
  // struct for simplicity
  PState[X_I] = state.simX;
  PState[Y_I] = state.simY;
  PState[TH_I] = state.simTheta;
  PState[THD_I] = state.simDTheta;
  PState[VLON_I] = state.simVLon;
  PState[VLAT_I] = state.simVLat;

 
  // steering 
  phi = state.simPhi;
 
  // copy over to the auxilaray array 
  memcpy(P_aux, PState, NUM_STATES * sizeof(double));
  phi_aux = phi;

  // Calculate the change in time from the last update
  //step = currTime - t_last;
  //t_last = currTime;

  // ****
  // Calculate k1
  // k1 = f(y_n)
  // get the tire derivatives 
  dTire_Long(F_tlon, P_aux);

  // get the steering derivatives
  dPhi(phi, kp1);

  // get the planar derivatives
  dPlanar_State(PState, F_tlon, phi, k1);
  // ****
  
  // ****
  // Calculate k2
  // k2 = f(y_n  + dt/2)

  // steering
  phi_aux += kp1 * step / 2;
  dPhi(phi_aux, kp2);

  // planar
  for (i = 0; i<NUM_STATES; i++){
    P_aux[i] += k1[i] * step /2;
  }
  // tires 
  dTire_Long(F_tlon, P_aux);
  dPlanar_State(P_aux, F_tlon, phi_aux, k2);
  // ****

  // restore aux arrays 
  memcpy(P_aux, PState, NUM_STATES * sizeof(double));
  phi_aux = phi;


  // ****
  // Calculate k3
  // k3 = f(y_n  + k2 * dt /2)

  // steering
  phi_aux += kp2 * step / 2;
  dPhi(phi_aux, kp3);

  // planar
  for (i = 0; i<NUM_STATES; i++){
    P_aux[i] += k2[i] * step /2;
  }
  // tires  
  dTire_Long(F_tlon, P_aux);
  dPlanar_State(P_aux, F_tlon, phi_aux, k3);
  // ****

  // restore aux arrays 
  memcpy(P_aux, PState, NUM_STATES * sizeof(double));
  phi_aux = phi;

  // ****
  // Calculate k4
  // k4 = f(y_n  + k3 * dt)

  // steering
  phi_aux += kp3 * step;
  dPhi(phi_aux, kp4);

  // planar
  for (i = 0; i < NUM_STATES; i++){
    P_aux[i] += k3[i] * step;
  }
  // tires
  dTire_Long(F_tlon, P_aux);
  dPlanar_State(P_aux, F_tlon, phi_aux, k4);
  // ****

  // Add to get new state
  // y_n+1 = y_n + dt * (k1 + 2*k2 + 2*k3 + k4) / 6
  // steering
  phi += step * (kp1 + 2 * kp2 + 2 * kp3 + kp4) / 6;

  // planar
  for (i = 0; i < NUM_STATES; i++){
    PState[i] += step * (k1[i] + 2 * k2[i] + 2 * k3[i] + k4[i]) / 6;
  } 

  // **** Copy New States back into Old State ****
  state.simX = PState[X_I]; 
  state.simY = PState[Y_I]; 
  state.simTheta = PState[TH_I]; 
  state.simDTheta = PState[THD_I]; 
  state.simVLon = PState[VLON_I]; 
  state.simVLat = PState[VLAT_I]; 
  state.simPhi = phi;

  //if (state.simVLon < V_EPS){
  //  state.simVLon = 0;
  // }

  // normalize theta
  state.simTheta = atan2(sin(state.simTheta), cos(state.simTheta));
  
  state.simEngine.simFf = F_tlon[FRONT];
  state.simEngine.simFr = F_tlon[REAR];  

  // set the front state
  set_front_state(step);

  //max_step = fmax(step, max_step);
  //cout<<"step: "<<max_step<<endl;

  return;
}

// void SetCommands(double SteerCmd, double PedalCmd)
// *************************************************
// sets the global steering and pedal commands for future runs of the 
// simulator.  Also converts normalized steering command into a commanded
// steering angle in radians.  Assumes that normalized steering is from -1 to 
// 1, and that steering commanad is piecewise linear from phi_min to 0, then 
// from 0 to phi_max
void SimModel::SetCommands(double SteerCmd, double PedalCmd){
 
  steer = SteerCmd;

  // conversion to radians.  Assumes that normalized steering is commanded
  // from -1 to 1.  Treats the 0 to PHI_MIN as linear, then 0 to PHI_MAX as 
  // another linear section
  if (steer < 0){
    steer_phi =  -PHI_MIN * steer; 
  } else if (steer > 0){
    steer_phi = PHI_MAX * steer;
  } else {
    steer_phi = 0;
  }

  pedal = PedalCmd;

}

// simState GetState()
// ******************************************************
// Used to access the state struct with outside functions
simState SimModel::GetState(){
  return(state);
}

// simState GetFrontState()
// ******************************************************
// Gives the state of the center of the front axle by doing the appropriate
// conversions from the CG to the front axle
StateReport SimModel::GetFrontState(){
  return report;
}


// void set_front_state(double dt)
// ******************************
// calculates the state of the front axle based on the state of the c.g.
// this is the state reported by GetFrontState()
void SimModel::set_front_state(double dt)
{
  double thd, vlat, vlon, x, y, theta, phi, xd, yd;
  
  x = state.simX;
  y = state.simY;
  vlon = state.simVLon;
  vlat = state.simVLat;
  theta = state.simTheta;
  thd = state.simDTheta;
  phi = state.simPhi;

  xd = vlon * cos(theta) - (vlat + L_f * thd) * sin(theta);
  yd = vlon * sin(theta) + (vlat + L_f * thd) * cos(theta);

  // set accelerations first
  report.xdd = (xd - report.xd) / dt;
  report.ydd = (yd - report.yd) / dt;

  report.x = x + L_f * cos(theta);
  report.y = y + L_f * sin(theta);
  report.xd = xd;
  report.yd = yd;
  if( FORCE_NO_SIDESLIP )
  {
      report.yaw = atan2(yd, xd);
  }
  else
  {
    report.yaw = theta;
  }
  report.yawd = thd;
  report.phi = phi; 
}

// simState GetRearState()
// ******************************************************
// Gives the location of the center of the rear axle 
StateReport SimModel::GetRearState(){
  StateReport report;
  return report;
}
