// *************************************
// SimModel                            *
// Haomiao Huang                       *
// 1/15/2005                           *
// *************************************

// This is the model that the simulator updates/runs to get vehicle state.  
// The model is a dynamic bicycle model that models the forces acting upon
// the tires (lateral and longitudinal) to generate the state of the vehicle.
// Sideslip is included.  For more information on the model itself see Wiki
// documentation. 

#ifndef SIMMODEL_HH
#define SIMMODEL_HH

using namespace std;
#include <math.h>
#include <iostream>
#include <string>

#include "AliceConstants.h"
#include "planar_definitions.hh"


// States for Engine dynamics.  Currently there are only
// two, for the forces from the engine at the wheels.  As the
// longitudinal dynamics model 
typedef struct EngineStates{
  double simFf;     // forward wheels long. force (N)
  double simFr;     // rear wheels long. force (N)
};

// Data structure for all of the states used by the model.  
struct simState{
  // dynamic bicycle model states
  double simX;      // northing (m)
  double simY;      // easting (m)
  double simTheta;  // yaw (rad cw from N)
  double simDTheta; // delta yaw (rad/s)
  double simVLon;   // longitudinal velocity (m/s)
  double simVLat;   // lateral velocity (m/s)

  // steering states
  double simPhi;    // actual steering angle (rad)  
  
  EngineStates simEngine; // states of the engine/lateral dynamics  

};

// This is the struct used to report state back up to higher levels
struct StateReport{
  double x;       // northing
  double xd;      // northing velocity
  double xdd;     // northing accel
  double y;       // easting
  double yd;      // easting velocity
  double ydd;     // easting accel
  double yaw;     // heading angle
  double yawd;    // change in heading angle
  double phi;     // steering angle

	void readFile(char* pFilename);
};

class SimModel {
private:
  // private variables
  double steer;  // current steering command
  double pedal;  // current pedal command
  double t_last; // time of last simulation update
  double step;   // time step from last time of simulation to new time
  simState state; // planar state
  double steer_phi; // de-normalized steering in rad
  StateReport report; // front axle state 

  // debug
  double max_step ;

  // private functions
  // calculate the derivatives of the longitudinal tire forces
  void dTire_Long(double* forces, double* planar);

  // calculate the lateral tire forces
  void FTire_Lat(double* planar, double steering, double* forces);

  // calculate the derivative of the steering angle
  void dPhi(double phi, double& dphi);

  // calculate the derivatives of the vehicle positional state
  void dPlanar_State(double* planar, double* ln_forces,
		     double steering, double* dplanar);

  // calculate the sideslip angles
  void sideslip(double* planar, double steer, double* alpha);

  // calculate the lateral tire forces based on slip angle and normal load
  double pacejka(double alpha);

  // sets the front axle state
  void set_front_state(double dt);

public:
  // Constructors/Destructor
  SimModel();
  ~SimModel();
  
  // Copy Constructor 
  SimModel(simState initial, double initTime);  
  
  // Initialization
  void Init(StateReport& initial, double iTime); 

  // Gives the simulator the time delta  since last update and then
  // solves the diff eqs forward to arrive at the new state.  
  void RunSim(double step);
  
  // Sets the current actuator commands
  void SetCommands(double SteerCmd, double PedalCmd);

  // Sets the current state to NewState
  void SetState(simState NewState);

  // Returns the current state of the CG
  simState GetState(); 

  // Returns the state of the center front axle
  StateReport GetFrontState();

  // Returns the state of the center rear axle
  StateReport GetRearState();

};

#endif
