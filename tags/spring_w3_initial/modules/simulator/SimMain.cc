#include "Sim.hh"

#include "DGCutils"

using namespace std;

int main(int argc, char **argv)
{
  // process command line options
  for (int i = 1; i < argc; ++i)
    {
      string arg(argv[i]);
      
      if (arg == "h" || arg == "-h" || arg == "--help" || arg == "help")
        {
          /* User has requested usage information. Print it to standard
             output, and exit with exit code zero. */
          cout << "asim command line options:\n"
               << "--------------------------\n"
               << "h,-h,help,--help    Prints this.\n"
	       << "\n"
               << "Status:\n"
               << "---------------------\n"
               << "Current RDDF:         ";
          cout.flush();
          system("readlink ../../RDDF/rddf.dat");
          cout << "Current SKYNET_KEY:   ";
          cout.flush();
          system("echo $SKYNET_KEY");
          cout << endl;
	  return 0;
	}
    }
  
  int sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if( pSkynetkey == NULL )
    {
      cerr << "SKYNET_KEY environment variable isn't set" << endl;
    }
  else
    sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;
  usleep(750000);
  
  asim sim(sn_key);
  
  DGCstartMemberFunctionThread(&sim, &asim::StateComm);
  DGCstartMemberFunctionThread(&sim, &asim::SparrowDisplayLoop);
  DGCstartMemberFunctionThread(&sim, &asim::UpdateSparrowVariablesLoop);
  DGCstartMemberFunctionThread(&sim, &asim::DriveComm);
  sim.Active();
  
  return 0;
}
