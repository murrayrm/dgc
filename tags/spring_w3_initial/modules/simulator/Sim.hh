#ifndef SIM_HH
#define SIM_HH

#define LOG_FILE_DIR "logs/"
#define LOG_FILE_NAME "sim_"
#define LOG_FILE_EXT ".dat"

#define NOMTA

#include <time.h>
#include <unistd.h>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max

#include "VehicleState.hh"
#include "sn_msg.hh"
#include "models/SimModel.hh"
#include "adrive_skynet_interface_types.h"
#include "adrive.h"

using namespace std;

struct SIM_DATUM
{
  VehicleState SS;
  //VDrive_CmdSteerAccel cmd;
  double steer_cmd, accel_cmd;
  unsigned long long lastUpdate;
};

class asim
{
	StateReport	simState;
	SimModel		simEngine;

	skynet m_skynet;

	pthread_mutex_t m_stateMutex;
	pthread_mutex_t m_drivecmdMutex;

	void SimInit(void);
	void updateState(void);

	int broadcast_statesock;

public:
  asim(int sn_key);

  void StateComm();
  void DriveComm();

  void Active();

  void broadcast();

  void SimUpdate(float deltaT);

  void SparrowDisplayLoop();
  void UpdateSparrowVariablesLoop();
};

#endif
