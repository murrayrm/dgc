#include "Sim.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

#include <iostream>
using namespace std;

// Added for display 2/24/04 
// Changed to use new, platform-independent struct 1/6/05
VehicleState dispSS;
double d_decimaltime;
double d_speed;

double steerCommand = 0.0;
double accelCommand = 0.0;

int UpdateCount   = 0;

extern int QUIT_PRESSED;
extern int PAUSED;
extern int RESET;

extern SIM_DATUM d;

#include "vddtable.h"
int user_quit(long arg);
int pause_simulation(long arg);
int unpause_simulation(long arg);
int reset_simulation(long arg);

void asim::UpdateSparrowVariablesLoop() 
{
  while(true) 
  {
    // this is the same as in the arbiter display 
    dispSS = d.SS;
    d_decimaltime = ((double)d.SS.Timestamp)/1000000.0;
    d_speed = d.SS.Speed2();

    steerCommand = d.steer_cmd; 
    accelCommand = d.accel_cmd;

		UpdateCount ++;
    
    usleep(500000); // Wait a bit, because other threads will print some stuff out
  }
}

void asim::SparrowDisplayLoop() 
{
  dbg_all = 0;

  if (dd_open() < 0) exit(1);

  dd_bindkey('Q', user_quit);
  dd_bindkey('P', pause_simulation);
  dd_bindkey('U', unpause_simulation);
  dd_bindkey('R', reset_simulation);

  dd_usetbl(vddtable);

  usleep(500000); // Wait a bit, because other threads will print some stuff out
  dd_loop();	
  dd_close();
  QUIT_PRESSED = 1; // Following LADARMapper's lead
}

// TODO: Set the shutdown flag in the quit function
int user_quit(long arg)
{
  return DD_EXIT_LOOP;
}

// Pause the simulation
int pause_simulation(long arg)
{
  PAUSED = 1;
}

// Unpause the simulation
int unpause_simulation(long arg)
{
  PAUSED = 0;
}

// Reset the simulation
int reset_simulation(long arg)
{
  RESET = 1;
}

