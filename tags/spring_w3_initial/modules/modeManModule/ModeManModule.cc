/**
 * ModeManModule.cc
 *  12/26/2004     Sue Ann Hong       Created based on ArbiterModule.cc
 *     For the Dec,2004 field test, only PASSTHROUGH and STOP should work.
 *     In PASSTHROUGH mode, ModeMan only listens to one module and relays the 
 *     path to pathfollowing without any safety checking.
 *     Also, we're not receiving state as of now (12/27/2004).
 *  2/26/2005      Sue Ann Hong       Turned into a skynet wrapper only
 *     Now it doesn't contain internal functions of ModeMan, but only acts
 *     as a skynet wrapper and a main-loop that calls ModeMan's methods.
 *     TODO: either use friends or make emergency members of ModeMan public
 *     to make modifying them easier.
 * 3/20/2005       Sue Ann Hong       Everything's back in ModeManModule!
 *     Ugly, but it's the easiest way to get state to ModeMan core functions
 *     and deadbaby functions. 
 * 3/22/2005       Sue Ann Hong       Starting to complete modeswitching
 *     Fault management will change the mode switch grid, but it's about time
 *     to put in all modes in the mode switch grid to prepare for that time
 *     when we actually get skynet messages with source names (so we can handle
 *     multiple sources of trajectories).
 *
 * Search for
 *  TODO  to find incomplete stuff
 *  PARAM to find variable parameters (part of incomplete stuff, I'd say)
 **/

#include <iostream>
#include <fstream>

#include "ModeManModule.hh"
#include "DGCutils"
//#include "sn_msg.hh"

using namespace std;

int QUIT_PRESSED = 0;   // Doesn't get modified at all at the moment

// Variables for command-line arguments/flags
extern int LOG_DATA;    // whether or not the -log option was detected
extern int NODISPLAY;   // 0 -> display
extern int NOSTATE;   // 0 -> use a/vstate
extern char * TESTNAME; // the string identifier for the log file
FILE * logfile;         // logfile is for state and command logs
// These files are for path logging
vector<FILE *> voteLogFileVector;

/* Helper function prototypes */
void InitLog();
void LogData();

/********** Constructors & Destructor **************************************/
/* Shouldn't ever use the default constructor. */
ModeManModule::ModeManModule(int sn_key, bool noDisplay) 
  : CSkynetContainer(SNModeMan, sn_key)
{
  cout << "ModeManModule: Starting Init with skynet key: " << sn_key << endl;

  // Initialize class members
  modeManSocket = m_skynet.get_send_sock(SNmodemantraj);
  InitModeMan(noDisplay);

  /* TODO: need test numbers as part of init?
     msg >> d.TestNumber;
     strstream strStream;
     strStream << d.TestNumber;
     strStream >> d.TestNumberStr;
     d.TimeZero = TVNow();
  */
  //------------------Logging Initialization--------------------------//
  /*
  char filename[255]; // output log file name
  // set up logging file if option was set
  if( LOG_DATA ) { InitLog(filename); }
  */

  cout << "ModeManModule: Init Finished" << endl;
}

/* Destructor */
ModeManModule::~ModeManModule() 
{
  cout << "-------------ModeManModule: Shutting Down----------------" << endl;

  // if we get to here a break has been detected                               
  // Deallocate members                                                       
  for( int i = 0; i < PathInput::COUNT; i++)
    {
      delete new_path[i];
      delete path[i];
    }
  delete curPath;

  // Delete locks                                                             
  for (int i = 0; i < PathInput::COUNT; i++) {
    DGCdeleteMutex(&newPathMutex[i]);
  }

}



/*********************** MAIN LOOP ******************************************
 * The main loop for mode management:
 * Fault management is done in ManageFaults() called in this loop.
 * Dead baby sensing is done in a separate thread for detection
 * but handled in this loop (i.e. slowing down the path if dead babies
 * have been detected). Also note that new_path[] are updated in separate
 * threads and loaded into path[] for evaluation for mutex. Also, curPath,
 * sent to path follower is only a pointer that points to one of path[] because
 * path[] shouldn't be modified while sending curPath to path follower.
 * Outline:
 *  1. Update State.
 *  2. Update faults and manage them.
 *  3. Update paths from each source.
 *  4. Update mode and pick a new curPath.
 *  5. Send curPath to path follower.
 **/
void ModeManModule::ModeManLoop()
{
  while( !QUIT_PRESSED ) {
    // Sleeping because we share CPU with other modules...
    // NOTE: Currently hitting 10Hz, may want to sleep less to utilize faster
    // pathmakers
    usleep(SLEEP_ACTIVE_LOOP);

    // state update defined by parent StateClient
    if (!NOSTATE) { UpdateState(); updateCountState++; }

    UpdateModeManFaults();

    // Don't do anything until we get a new path
    if (needToSendPath()) {
      UpdateModeManPaths();   // check for timeouts/validity
      PickNewMode();
      getcurPath();           // Get a new path
      ManageFaults();
      if (curPath != NULL) {  // Send curPath to path follower
        if (!SendTraj(modeManSocket, curPath))
          cerr << "ModeManModule: Failed to broadcast the next path." << endl;
        else {
          pathOutCount++;
          if (noDisplay) cout << "Sent a path to the pathfollower" << endl;
        }
      }
      else cerr << "ModeManModule: new path is null! uh oh!!" << endl;
    }

    //sn_heartbeat()
    //sn_pause()    
    
    //------------------Logging Data---------------------------------------//
    // if (LOG_DATA) { LogData(); }

  } // end of while(!QUIT_PRESSED)

  // Stop the car now that we're no longer active
  /* However, if we haven't been driving already (i.e., curPath == NULL)
   * then we send nothing. 
   * TODO: can anyone think of a situation where
   *  curPath != NULL && we have a non-zero speed path in the path follower?\
   */
  getStopPath();
  if (curPath != NULL) {
    SendTraj(modeManSocket, curPath);   // Send to path follower
  }

  // close the log file if we opened it
  /*
  if( LOG_DATA ) {
    fclose(logfile);
    for( int i = 0; i<PathInput::COUNT; i++) {
      fclose( voteLogFileVector[i] );
    }
  }
  */

  cout << "----------------ModeMan: Shutting Down----------------" << endl;

} // end of ModeManModule::ModeManLoop()


/*************** MODULE PATH & EMERGENCY UPDATES ****************************/

/** 
 * UpdateModulePath_module() methods:
 *  Each of them calls UpdatModulePath with skynet module name for 'module'.
 *  Hopefully in the future we can avoid hardcoding in these modules and
 *  making a method for each module.
 **/

void ModeManModule::UpdateModulePath_planner() {
// Update new_path in a loop forever!
  int trajSocket = m_skynet.listen(SNplannertraj, SNplanner);
  UpdateModulePath(trajSocket, PathInput::DELIBERATE);
}

void ModeManModule::UpdateModulePath_RddfPathGen() {
  // Update new_path in a loop forever!
  int trajSocket = m_skynet.listen(SNRDDFtraj, SNRddfPathGen);
  UpdateModulePath(trajSocket, PathInput::RDDF);
}

void ModeManModule::UpdateModulePath_Reactive() {
  //  int trajSocket = m_skynet.listen(SNreactivetraj..., SNreactive...);
  // UpdateModulePath(trajSocket, PathInput::REACTIVE);
}

/**
 * This is for testing any module that's sending SNtraj 
 **/
void ModeManModule::UpdateModulePath_default() {
  // SNplanner is a placeholder since m_skynet.listen doesn't use the second
  // parameter
  int trajSocket = m_skynet.listen(SNtraj, SNplanner); 
  UpdateModulePath(trajSocket, PathInput::DEFAULT);
}


/**
 * UpdateModulePath
 *  In an infinite loop, receives trajectories from src and updates
 * new_path[src] and other associated var's (timestamp, etc).
 **/
void ModeManModule::UpdateModulePath(int trajSocket, int src)
{
  numMsgsReceived = 0;

  while(!QUIT_PRESSED)  {

    // Get new_path from this source -- blocking
    RecvTraj(trajSocket, new_path[src], &(newPathMutex[src]));

    if (NODISPLAY) { 
      cout << "Update #" << ++numMsgsReceived << endl;
      cout << "ModeManModule: got path from " << src << endl;
    }

    // Update other new_path-related variables
    DGClockMutex(&(newPathMutex[src]));
    UpdateCount[src]++;
    // Currently, we only use the clock on ModeMan's computer
    DGCgettime( lastUpdate[src] );
    newPathPresent[src] = true;
    numNewPaths++;
    DGCunlockMutex(&(newPathMutex[src]));

    if (NODISPLAY) {
      cout << "order = " << new_path[src]->getOrder() << endl;
      cout << "num   = " << new_path[src]->getNumPoints() << endl;
      // new_path[src]->print(std::cout, new_path[src]->getNumPoints());
    }
  } // end of while (!QUIT_PRESSED)

} // end of UpdateModulePath


/**
 * Update emergency variables from external fault detection
 * (i.e. sensors, actuators, computers, etc)
 * TODO: fill it in with fault detection stuff
 *   probably will include e-stop signal (not from sparrow)
 **/
void ModeManModule::UpdateFaultDetectionVars() {}



/********************* LOG HELPERS ******************************************/
/* Initialize logging files */
void InitLog() {

  /*
  // create a log of the state and ModeMan commands
  // filename will be TESTNAME_ddmmyyyy_hhmmss.log
  time_t t = time(NULL);
  tm *local;
  local = localtime(&t);
  
  sprintf(filename, "testRuns/%s_ModeMan_%02d%02d%04d_%02d%02d%02d.log",
	  TESTNAME, local->tm_mon+1, local->tm_mday, local->tm_year+1900,
	  local->tm_hour, local->tm_min, local->tm_sec);
  printf("Logging data to file: %s\n", filename);
  logfile = fopen(filename,"w");
  if ( logfile == NULL ) {
    printf("Unable to open log file!!!\n");
    exit(-1);
  }
  // print data format information in a header at the top of the file
  fprintf(logfile, "%% Time[sec] | Easting[m] | Northing[m] | Altitude[m]");
  fprintf(logfile, " | Vel_E[m/s] | Vel_N[m/s] | Vel_U[m/s]" );
  fprintf(logfile, " | Speed[m/s] | Accel[m/s/s]" );
  fprintf(logfile, " | Pitch[rad] | Roll[rad] | Yaw[rad]" );
  fprintf(logfile, " | PitchRate[rad/s] | RollRate[rad/s] | YawRate[rad/s]");
  fprintf(logfile, " | PhiDes[rad] | VelDes[m/s]");
  fprintf(logfile, "\n");
  
  // reserve the space for our 
  voteLogFileVector.reserve(PathInput::COUNT);
  
  // additionally, create a log file for each voter 
  // LM = LADAR [LADARMapper],                SV = Stereo [StereoPlanner], 
  // GP = Global [GlobalPlanner],             DF = DFE [DFEPlanner],
  // CE = CorrArcEval [CorridorArcEvaluator], RF = RoadFollower [RoadFollower] 
  // PE = PathEvaluation [PathEvaluation]
  
  for( int i = 0; i<PathInput::COUNT; i++) {
    // create the name of the file and open it for writing
    sprintf(filename, "testRuns/%s_votes%02d_%02d%02d%04d_%02d%02d%02d.log",
	    TESTNAME, i, local->tm_mon+1, local->tm_mday, 
	    local->tm_year+1900, local->tm_hour, local->tm_min, 
	    local->tm_sec);
    printf("Logging data to file: %s\n", filename);
    voteLogFileVector.push_back( fopen(filename,"w") );
    if ( voteLogFileVector[i] == NULL ) {
      printf("Unable to open vote log file %02d!!!\n", i);
      exit(-1);
    }
    // print data format information in a header at the top of the file
    fprintf(voteLogFileVector[i], "%% Vote log file %02d\n", i );
    fprintf(voteLogFileVector[i], 
	    "%% Votes are goodnesses and then velocities for each\n");
    fprintf(voteLogFileVector[i], "%% arc from full left to full right.\n");
    //      fprintf(voteLogFileVector[i], "%% NUMARCS = %d\n", NUMARCS );
  }
  */
}

/* Log paths & state data */
void LogData() {
  /*
  fprintf(logfile, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",
	  (double)(d.SS.Timestamp.sec()+d.SS.Timestamp.usec()/1000000.0), 
	  d.SS.Easting, d.SS.Northing, d.SS.Altitude, d.SS.Vel_E, d.SS.Vel_N,
	  d.SS.Vel_U, d.SS.Speed, d.SS.Accel, d.SS.Pitch, d.SS.Roll,
	  d.SS.Yaw, d.SS.PitchRate, d.SS.RollRate, d.SS.YawRate,
	  toSend.cmd.phi, toSend.cmd.vel );
  fflush(logfile);
  
  for( int i = 0; i<PathInput::COUNT; i++)
    {
      // write the goodness votes to the file
      for( int j = 0; j<NUMARCS; j++ ) {
	fprintf(voteLogFileVector[i], "%5.1f\t", 
		d.allVoters[i].Votes[j].Goodness );
      }
      // write the speed votes to the file
      for( int j = 0; j<NUMARCS; j++ ) {
	fprintf(voteLogFileVector[i], "%5.2f\t", 
		d.allVoters[i].Votes[j].Velo );
      }
      fprintf(voteLogFileVector[i], "\n");
    }
  */
}

