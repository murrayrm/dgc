/**
 * ModeManModuleMain.cc
 *  12/26/2004     Sue Ann Hong       Created based on ArbiterModuleMain.cc
 **/

#include "ModeManModule.hh"
#include "DGCutils"

#ifdef MACOSX
  /* Install libgnugetopt for getopt_long_only support in Mac OS X */
  #include <gnugetopt/getopt.h>
#else
  #include <getopt.h>
#endif

using namespace std;

#define LOG_OPT  10
#define HELP_OPT 11
/* variables/flags for command line options */
int LOG_DATA = 0;     // whether or not to log state and command data
int NODISPLAY = 0;      // 0 -> display
int NOSTATE = 0;      // 0 -> use a/vstate
char *TESTNAME;         // id for test to be part of log file name

int main(int argc, char **argv) 
{
  //-----------------------------------------------------------------------//
  // Do some argument handling
  int c;

  while (1) {
    int option_index = 0;
    static struct option long_options[] = {
      {"log",          1, 0,             LOG_OPT},
      {"help",         0, 0,             HELP_OPT},
      {"nodisp",       0, &NODISPLAY,    1},
      {"nostate",      0, &NOSTATE,      1},
      {0,0,0,0}
    };

    c = getopt_long_only(argc, argv, "", long_options, &option_index);
    if(c == -1) break;

    switch(c) {
      case LOG_OPT:
        LOG_DATA = 1;
        TESTNAME = strdup(optarg);
        break;
      case HELP_OPT:
        cout << endl << "ModeMan Usage:" << endl << endl;
        cout << "ModeMan [-nodis -nostate -log testname]" << endl;
	      cout << "  -nodisp        No Sparrow display. " << endl;
        cout << "  -nostate       No need for state. " << endl;
        cout << "  -log testname  Log state and commands in testRuns subdirectory" 
	           << endl;
        cout << "  -help          Displays this help" << endl << endl;
        exit(0);
    }
  } // end while(1)

  cout << endl << "Argument summary:" << endl;
  cout << "  NO DISPLAY: " << NODISPLAY;
  cout << "  NO STATE: " << NOSTATE;
  cout << "  LOG_DATA: " << LOG_DATA;
  if( LOG_DATA ) cout << "  test name: " << TESTNAME;
  cout << endl;
  // Done doing argument handling
  //-----------------------------------------------------------------------//

  // TODO: Register with skynet??

  // Set skynet key
  int sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if( pSkynetkey == NULL )
    cerr << "SKYNET_KEY environment variable isn't set" << endl;
  else sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;
  ModeManModule modeManModule(sn_key, NODISPLAY);
  cerr << "Done constructing ModeManModule" << endl;
  
  /* Start the main loop and message handler */
  if( !NODISPLAY ) {
    cout << "Starting sparrow display loops" << endl;
    DGCstartMemberFunctionThread(&modeManModule, 
				 &ModeManModule::SparrowDisplayLoop);
    DGCstartMemberFunctionThread(&modeManModule, 
				 &ModeManModule::UpdateSparrowVariablesLoop);
  }
  
  /* Start path-listening threads */
  /* Currently, we're hardcoding functions for each path-generating 
     module. Use function pointers somehow? */
  DGCstartMemberFunctionThread(&modeManModule, 
			       &ModeManModule::UpdateModulePath_planner);
  DGCstartMemberFunctionThread(&modeManModule, 
			       &ModeManModule::UpdateModulePath_RddfPathGen);
  /*
  DGCstartMemberFunctionThread(&modeManModule, 
			       &ModeManModule::UpdateModulePath_Reactive);
  DGCstartMemberFunctionThread(&modeManModule, 
			       &ModeManModule::UpdateModulePath_default);
  */
  // TODO: May want to call deadbaby & emergency update stuff here
  /*
  // Update emergency variables from external fault detection
  // (i.e. sensors, actuators, computers, etc)
  DGCstartMemberFunctionThread(&(modeManModule.mm), 
			       &ModeManModule::UpdateFaultDetectionVars);
  */

  modeManModule.ModeManLoop();

  // TODO: Unregister with skynet??

  return 0;
}
