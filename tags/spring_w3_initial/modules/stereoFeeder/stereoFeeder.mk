STEREOFEEDER_PATH = $(DGC)/modules/stereoFeeder

STEREOFEEDER_DEPEND_LIBS = $(SPARROWLIB) \
						   $(DGCUTILS) \
						   $(MODULEHELPERSLIB) \
						   $(SKYNETLIB) \
						   $(FLWINLIB) \
						   $(FRAMESLIB) \
						   $(CMAPPLIB) \
						   $(CMAPLIB) \
						   $(STEREOSOURCELIB) \
						   $(STEREOPROCESSLIB)

STEREOFEEDER_DEPEND_SOURCES = \
	$(STEREOFEEDER_PATH)/StereoFeeder.cc \
	$(STEREOFEEDER_PATH)/StereoFeeder.hh \
	$(STEREOFEEDER_PATH)/StereoFeederDisplay.cc \
	$(STEREOFEEDER_PATH)/StereoFeederDisplay.dd \
	$(STEREOFEEDER_PATH)/StereoFeederMain.cc \
	$(STEREOFEEDER_PATH)/Makefile

STEREOFEEDER_DEPEND = $(STEREOFEEDER_DEPEND_LIBS) $(STEREOFEEDER_DEPEND_SOURCES)
