#define TRYLOGGER

#include "RddfPathGen.hh"
#include <string>
using namespace std;
//RDDF& rddf = *(new RDDF)


int NOSTATE = 0;                 /**< for command line argument */
int NOSKYNET = 0;                /**< for command line argument */
int WRITE_COMPLETE_DENSE = 0;    /**< for command line argument */
int WRITE_COMPLETE_SPARSE = 1;   /**< for command line argument */
int NOMM = 0;                    /**< for command line argument; 
				    not using ModeManModule */

RddfPathGen::RddfPathGen(int skynetKey) : CSkynetContainer(SNRddfPathGen, skynetKey), CLoggerClient()
{
  // do something here if you want
}




RddfPathGen::~RddfPathGen()
{
  // Do we need to destruct anything??
  cout << "RddfPathGen destructor has been called." << endl;
}

corridorstruct corridor_whole;

void RddfPathGen::ActiveLoop()
{
  /**
   * this is the main function which is run.  execution should be trapped
   * somewhere in this funciton during operation.
   */
  printf("[%s:%d] Entering RddfPathGen::ActiveLoop()\n", __FILE__, __LINE__);

  //###
  // trying to get cloggerclient to work
  //char* p_SkynetKey = getenv("SKYNET_KEY");
  //cout << "got key " << p_SkynetKey << endl;
  //int tempSkynetKey = atoi(p_SkynetKey);
  //CLoggerClient lcObj();

  //  int thekey = lcObj.getSkynetKey();
  //cout << "The key is \"" << thekey << "\"" << endl;

  /*
  cout << "got here." << endl;
  char* junk = new char[32];
  storeMyIp(junk);
  cout << junk << endl;
  delete junk;
  */
  //###  

#ifdef UNTESTED
  /* Get rid of the ifdefs to test initial logger capability */
  cout << "Getting log dir" << endl;
  /* This function hangs in a loop by default if a Logger isn't running.  You 
   * can tell it to do nothing if the Logger is not running by giving it an 
   * argument of "true".  TODO: Make this function return a default path even 
   * if the Logger is not running. -LBC */
  string logdir = getLogDir();
  cout << "Log dir is \"" << logdir << "\"" << endl;
#endif

  /*
  addFile("rddfdebug");
  addFile("rddfdebug");
  addFile("rddfdebug");
  addFile("rddfdebug2");
  addFile("rddfdebug3");
  addFile("rddfdebug4");
  addFile("rddfdebug");
  addFolder("folder1");
  addFolder("folder2");
  addFolder("folder1");
  addFolder("folder1/");
  addFolder("folder3/");
  addFolder("folder4/");
  */
  //  debug();

  // INITIALIZATION
  // set up message counter
  int message_counter = 0;

  // echo SKYNET_KEY
  cout << "\n\nCurrent SKYNET_KEY: ";
  cout.flush();
  system("echo $SKYNET_KEY");

  // let people know which file we're operating on
  cout << "Current RDDF is: ";
  cout.flush();
  system("readlink ../../util/RDDF/rddf.dat");

  // read the file into a corridor segment and get the waypoints in northing
  // and easging.  we'll use the nice functions in dgc/RDDF/rddf.hh
  cout << "Reading RDDF... "; cout.flush();
  RDDF& rddf = *(new RDDF);
  rddf.loadFile(RDDF_FILE);
  RDDFVector& waypoints = *(new RDDFVector);
  waypoints = rddf.getTargetPoints();
  
  // extract the info from waypoints and store in corridor
  int N = rddf.getNumTargetPoints();   // number of points
  // store data in corridor
  corridor_whole.numPoints = N;
  for(int i = 0; i < N; i++)
    {
      // shouldn't get any segfaults here, but who knows...
      corridor_whole.e.push_back(waypoints[i].Easting);
      corridor_whole.n.push_back(waypoints[i].Northing);
      //shrink the corridor width to account for vehicle width, tracking error
      corridor_whole.width.push_back(waypoints[i].radius-RDDF_VEHWIDTH_EFF);
      corridor_whole.speedLimit.push_back(waypoints[i].maxSpeed);
      
      if (waypoints[i].radius-RDDF_VEHWIDTH_EFF < .01)
	cerr << "CORRIDOR WIDTH IS SMALLER THAN RDDF_VEHWIDTH_EFF!" << endl;

      //###
      //printf("waypoints[ %d ].Easting is %f\n", i, waypoints[i].Easting);
      //printf("waypoints[ %d ].Northing is %f\n", i, waypoints[i].Northing);
      //###
    }
  cout << "done (read " << N << " points)." << endl;
  
  // generate and store whole path (sparse path, so max of 4 path points
  // per corridor point)
  cout << "Generating complete (sparse) path... "; cout.flush();
  //  pathstruct path_whole_sparse = (pathstruct)Path_From_Corridor(corridor_whole);
  pathstruct& path_whole_sparse = *(new pathstruct);
  path_whole_sparse = Path_From_Corridor(corridor_whole);
  path_whole_sparse.currentPoint = 0; // we haven't went anywhere yet, so we're still at the start of the path
  cout << "done." << endl;


  // output the sparse path to a file (in traj format)
  cout << "Writing sparse path to file " << SPARSE_PATH_FILE << " ... "; cout.flush();
  if (WRITE_COMPLETE_SPARSE == 1)
    {
      WritePathToFile(path_whole_sparse, SPARSE_PATH_FILE);
      cout << "done." << endl;
    }
  else
    cout << "skipped." << endl;


  // Because we had issues with integrating RddfPathGen and the path follower last
  // time, we'll generate and output the complete dense path to a file

  cout << "Densifying sparse path and saving to file " << TRAJOUTPUT << " ... "; cout.flush();

#ifdef TRYLOGGER
  string log_dir = getLogDir();
  addRelFolder(".");
  string file_location = log_dir + (string)TRAJOUTPUT;
  char* file = new char[256];

  strcpy(file, file_location.c_str());
  if(WRITE_COMPLETE_DENSE)
    {
      pathstruct path_whole_dense = DensifyPath(path_whole_sparse);
      WritePathToFile(path_whole_dense, file);  // TRAJOUTPUT is defined in RddfPathGen.hh
      cout << "done." << endl;
    }
  else
    cout << "skipped." << endl;

  delete file;
#endif


  
  int trajSocket;
  int skynetguiSocket;

  if(!NOSKYNET) {
    if (NOMM)
      trajSocket = m_skynet.get_send_sock(SNtraj);
    else {
      skynetguiSocket = m_skynet.get_send_sock(SNtraj);
      trajSocket = m_skynet.get_send_sock(SNRDDFtraj);
    }
  }
  

  // generate blank trajectory
  CTraj* ptraj;
  ptraj = new CTraj(3);  // order 3 (by default)
  
  int usleep_time = 1000000/FREQUENCY; // set frequency in RddfPathGen.hh
  cout << endl << "   Num        N        E            First   Cur    Last" << endl;
  
  //###
  //this is for testing of CPath as a means of generating traj's for the planner
  //CTraj* seed_traj;
  //seed_traj = new CTraj(3);
  //CPath seed_path("rddf.dat");
  //###

  
  // Loop is trapped here during execution
  while(true)
    {
      message_counter++;
      
      // grab where we are and store it as location, so we only have to access astate
      // once per time through the loop
      vector<double> location(2);
      GetLocation(location);

      //###
      //this is for testing of CPath as a means of generating traj's for the planner
      //seed_path.SeedFromLocation(m_state, seed_traj, 20.0);
      //seed_traj->print();
      //###
      
      //###
      //cout << "About to call DensifyAndChop..." << endl;
      //###
      
      pathstruct path_chopped_dense = DensifyAndChop(path_whole_sparse, location,
						     CHOPBEHIND, CHOPAHEAD);
      //pathstruct path_chopped_dense = path_whole_dense;
      
      //###
      //cout << "Finished DensifyAndChop" << endl;
      //###
      
      // fill the trajectory using helper function from pathLib.hh
      //ptraj->print();
      StorePath(*ptraj, path_chopped_dense);

      if(SEND_TRAJ && !NOSKYNET)
	{
	  SendTraj(trajSocket, ptraj);
	  // Also send to skynetgui
	  if (!NOMM) SendTraj(skynetguiSocket, ptraj);
	}
      
      
      //### save the last sent chopped path
      WritePathToFile(path_chopped_dense, DEBUGOUTPUT);
      //###
      
      printf(" %4d   %8.1f   %8.1f     %6d  %6d  %6d   (%d, %d, %d)", message_counter, 
	     location[0], location[1], 0, path_chopped_dense.currentPoint,
	     path_chopped_dense.numPoints-1, 0, path_whole_sparse.currentPoint,
	     path_whole_sparse.numPoints-1);
      cout << endl;
      
      usleep(usleep_time);
    }


  delete ptraj;
  delete &rddf;
  delete &waypoints;
  delete &path_whole_sparse;
}



void RddfPathGen::GetLocation(vector<double> & location)
{
  if (NOSTATE || NOSKYNET)
    location[0] = location[1] = 0.0;
  else
    {
      UpdateState(); 
      location[0] = m_state.Northing;
      location[1] = m_state.Easting;
    }
}



void RddfPathGen::ReceiveDataThread()
{
  cout << "RddfPathGen is unable to receive data at this time." << endl;
}



int main(int argc, char** argv)
{
  for (int i = 1; i < argc; ++i)
    {
      string arg(argv[i]);
      
      if (arg == "h" || arg == "-h" || arg == "--help" || arg == "help")
	{
	  /* User has requested usage information. Print it to standard
	     output, and exit with exit code zero. */
	  cout << "Command line options:\n"
	       << "(you must type command line options just as they appear in this help\n"
	       << " screen; do not preceed them with some arbitrary number of -'s.\n"
	       << "---------------------------------------------------------------\n"
	       << "h,-h,help,--help    Prints this.\n"
	       << "a, nostate          Sets location to (0,0) every time GetLocation is called.\n"
	       << "                       Use if state is being quirky.  Note:  you still need\n"
	       << "                       something serving state to fool ModuleHelpers into\n"
	       << "                       letting us start up.\n"
	       << "k, noskynet         Runs without skynet (doesn't send any messages).  Acutally,\n"
	       << "                       we still need skynet to run, because we derive from\n"
	       << "                       ModuleHelpers :(\n"
	       << "d, writedense       Writes the dense traj to a file.\n"
	       << "s, nosparse         Skips writing the sparse traj to a file.\n"
	       << "w, nowrite          Skips writing the dense and sparse traj to a file.\n"
	       << "nomm                Don't use ModeManModule.\n"
	       << "\n"
	       << "Status:\n"
	       << "--------------------------------------------------------------\n"
	       << "Current RDDF:  ";
	  cout.flush();
	  system("readlink ../../util/RDDF/rddf.dat");
	  cout << "Current SKYNET_KEY:  ";
	  cout.flush();
	  system("echo $SKYNET_KEY");
	  cout << endl;
	  return 0;
	}
      else if (arg == "a" || arg == "nostate")
	{
	  cout << "RUNNING WITHOUT STATE!" << endl;
	  NOSTATE = 1;
	}
      else if (arg == "d" || arg == "writedense")
	{
	  cout << "WRITING DENSE TRAJ TO FILE!" << endl;
	  WRITE_COMPLETE_DENSE = 1;
	}
      else if (arg == "s" || arg == "nosparse")
	{
	  cout << "NOT WRITING SPARSE TRAJ TO FILE!" << endl;
	  WRITE_COMPLETE_SPARSE = 0;
	}
      else if (arg == "w" || arg == "nowrite")
	{
	  cout << "NOT WRITING DENSE TRAJ TO FILE!" << endl;
	  cout << "NOT WRITING SPARSE TRAJ TO FILE!" << endl;
	  WRITE_COMPLETE_DENSE = 0;
	  WRITE_COMPLETE_SPARSE = 0;
	}
      else if (arg == "k" || arg == "noskynet")
	{
	  cout << "RUNNING WITHOUT SKYNET!" << endl;
	  NOSKYNET = 1;
	}
      else if (arg == "nomm")
	{
	  cout << "RUNNING WITHOUT MODEMANMODULE!" << endl;
	  NOMM = 1;
	}
      else
	cout << "UNKNOWN SWITCH: \"" << arg << "\"" << endl;
    }

  //Setup skynet
  int intSkynetKey = 0;

  char* ptrSkynetKey = getenv("SKYNET_KEY");
  
  if(ptrSkynetKey == NULL)
    {
      cout << "Unable to get skynet key!" << endl;
      return 0;
    }
  else
    {
      //cout << "Got to RddfPathGen int main" << endl;
      intSkynetKey = atoi(ptrSkynetKey);
      RddfPathGen RddfPathGenObj(intSkynetKey);

      //cout << "Entering ActiveLoop." << endl;
      RddfPathGenObj.ActiveLoop();
    }
  
  return 0;
}
