/* Ben Pickett created 02-26-2005
 * Much of this code is stolen from Jason's RddfPathGen.cc
 * This file is to test trajDFE functionality
*/

#include "../../RDDF/rddf.hh"
#include "trajDFE.hh"
#include "pathLib.hh"

#define DENSETRAJ "bp_dense.traj"
#define DENSETRAJ_VEL "bp_dense_vel.traj"

int main (void)
{
  corridorstruct corridor_whole;

  cout << "reading rddf...";
  RDDF rddf;
  rddf.loadFile(RDDF_FILE);
  RDDFVector waypoints;
  waypoints = rddf.getTargetPoints();
  
  // extract the info from waypoints and store in corridor
  int N = rddf.getNumTargetPoints();   // number of points
  // store data in corridor
  corridor_whole.numPoints = N;
  for(int i = 0; i < N; i++)
    {
      // shouldn't get any segfaults here, but who knows...
      corridor_whole.e.push_back(waypoints[i].Easting);
      corridor_whole.n.push_back(waypoints[i].Northing);
      //shrink the corridor width to account for vehicle width, tracking error
      corridor_whole.width.push_back(waypoints[i].radius-RDDF_VEHWIDTH_EFF);
      corridor_whole.speedLimit.push_back(waypoints[i].maxSpeed);

      //###
      //printf("waypoints[ %d ].Easting is %f\n", i, waypoints[i].Easting);
      //printf("waypoints[ %d ].Northing is %f\n", i, waypoints[i].Northing);
      //###
    }
  cout << "done (read " << N << " points)." << endl;
  
  // generate and store whole path (sparse path, so max of 4 path points
  // per corridor point)
  cout << "Generating complete (sparse) path... ";
  pathstruct path_whole_sparse = (pathstruct)Path_From_Corridor(corridor_whole);
  path_whole_sparse.currentPoint = 0; // we haven't went anywhere yet, so we're still at the start of the path
  cout << "done." << endl;


  // output the sparse path to a file (in traj format)
  cout << "Writing sparse path to file " << SPARSE_PATH_FILE << " ... ";
  WritePathToFile(path_whole_sparse, SPARSE_PATH_FILE);
  cout << "done." << endl;


  // Because we had issues with integrating RddfPathGen and the path follower last
  // time, we'll generate and output the complete dense path to a file

  cout << "Densifying sparse path and saving to file " << DENSETRAJ << " ... ";
  pathstruct path_whole_dense = DensifyPath(path_whole_sparse);
  WritePathToFile(path_whole_dense, DENSETRAJ);
  cout << " done." << endl;
  
  cout << "Updating Velocity...";
  TrajDFE(path_whole_dense);
  cout <<" done." << endl;

  cout << "Saving updated velocities to file " << DENSETRAJ_VEL << "...";
  WritePathToFile(path_whole_dense, DENSETRAJ_VEL);
  cout << " done." << endl;

  return 0;
}
