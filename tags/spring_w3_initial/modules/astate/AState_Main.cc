#include <getopt.h>
#include "AState.hh"
#include <stdlib.h>
#include "DGCutils"

void print_usage() {
	cout << "Usage: astate [options]" << endl;
	cout << '\t' << "--help, -h" << '\t' << "Display this message" << endl;
	cout << '\t' << "--noimu, -i" << '\t' << "Disable IMU" << endl;
	cout << '\t' << "--nogps, -g" << '\t' << "Disable GPS" << endl;
	cout << '\t' << "--nomag, -m <heading>" << '\t' << "Disable Mag and start with <heading>" << endl;
	cout << '\t' << "--nokf -k" << '\t' << "Disable the kalman filter" << endl;
	cout << '\t' << "--verbose, -v" << '\t' << "Enable verbose mode" << endl;
	cout << '\t' << "--snkey, -s <snkey>" << '\t' << "Use skynetkey <snkey> (defaults to $SKYNET_KEY)" << endl;
	cout << '\t' << "--log, -l <logfile>" << '\t' << "Log raw data to <logfile>" << endl;
	cout << '\t' << "--replay, -r <logfile>" << '\t' << "Replay raw data from <logfile>" << endl;
}

const char* const short_options = "higm:kvs:l:r:";

static struct option long_options[] =
{
	//first: long option (--option) string
	//second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
	//third: if pointer, set variable to value of fourth argument
	//	 if NULL, getopt_long returns fourth argument
	{"help",	0,	NULL,	'h'},
	{"noimu",	0,	NULL,	'i'},
	{"nogps",	0,	NULL,	'g'},
	{"nomag",	1,	NULL,	'm'},
	{"m",	1,	NULL,	'm'},
	{"nokf",	0,	NULL,	'k'},
	{"verbose",	0,	NULL,	'v'},
	{"skynet",	1,	NULL,	's'},
	{"s",	1,	NULL,	's'},
	{"log",	1,	NULL,	'l'},
	{"l",	1,	NULL,	'l'},
	{"replay",	1,	NULL,	'r'},
	{"r",	1,	NULL,	'r'}
};

int USE_IMU = 1;
int USE_GPS = 1;
int USE_MAG = 1;
double MAG_HDG = 0;
int USE_KF = 1;
int GOT_SN = 0;
int USE_VERBOSE = 0;
int LOG_RAW = 0;
char LOG_FILE[100];
int REPLAY = 0;
char REPLAY_FILE[100];

int main(int argc, char **argv)
{
	int sn_key = 0;
	int options_index;

	int ch;

	while((ch = getopt_long(argc, argv, short_options, long_options, &options_index)) != -1)
	{
		switch(ch)
		{
			case 'h':
				print_usage();
				return 0;
				break;
			case 'i':
				USE_IMU = 0;
				break;
			case 'g':
				USE_GPS = 0;
				break;
			case 'm':
				USE_MAG = 0;
				MAG_HDG = atof(optarg);
				break;
			case 'k':
				USE_KF = 0;
				break;
			case 'v':
				USE_VERBOSE = 1;
				break;
			case 's':
				GOT_SN = 1;
				sn_key = atoi(optarg);
				break;
			case 'l':
				LOG_RAW = 1;
				strncpy(LOG_FILE, optarg, 99);
				break;
			case 'r':
				REPLAY = 1;
				strncpy(REPLAY_FILE, optarg, 99);
				break;
		}
	}

	cout << "USE_IMU = " << USE_IMU << endl;
	cout << "USE_GPS = " << USE_GPS << endl;
	cout << "USE_MAG = " << USE_MAG << endl;
	cout << "MAG_HDG = " << MAG_HDG << endl;
	cout << "USE_KF = " << USE_KF << endl;
	cout << "GOT_SN = " << GOT_SN << endl;
	cout << "USE_VERBOSE = " << USE_VERBOSE << endl;
	cout << "LOG_RAW = " << LOG_RAW << endl;
	cout << "LOG_FILE = " << LOG_FILE << endl;
	cout << "REPLAY = " << REPLAY << endl;
	cout << "REPLAY_FILE" << REPLAY_FILE << endl;

	char* pSkynetkey = getenv("SKYNET_KEY");
	if (GOT_SN == 0) {
		if ( pSkynetkey == NULL )
		{
			cerr << "SKYNET_KEY environment variable isn't set." << endl;
		} else 
		{
			sn_key = atoi(pSkynetkey);
		}
	}



	cerr << "Constructing skynet with KEY = " << sn_key << endl;
	AState ss(sn_key,USE_KF,USE_IMU,USE_GPS,USE_MAG,USE_VERBOSE,MAG_HDG,LOG_RAW,LOG_FILE,REPLAY,REPLAY_FILE);
	return 0;
}
