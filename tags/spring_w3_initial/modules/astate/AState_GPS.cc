/* AState.cc - new vehicle state estimator
 *
 * JML 31 Dec 04
 *
 * */

#include "AState.hh"

extern double GPS_xy_kr;

extern int QUIT_PRESSED;

void AState::GPS_init()
{
	if (gps_enabled == 0) {
		return;
	}
    if (replay == 1) {
             gps_replay_stream >> my_gps_time;
             gps_replay_stream >> gpsdata.data.nav_mode;
             gps_replay_stream >> gpsdata.data.lat;
             gps_replay_stream >> gpsdata.data.lng;
                        gps_replay_stream >> gpsdata.data.altitude;
                        gps_replay_stream >> gpsdata.data.vel_e;
                        gps_replay_stream >> gpsdata.data.vel_n;
                        gps_replay_stream >> gpsdata.data.vel_u;
             gps_log_start = my_gps_time;
    } else {
	    gps_enabled = gps_open(4); // GPS_open needs to return -1 on failure!
	    if(gps_enabled == -1)
	    {
		    cerr << "AState::GPS_init(): failed to open GPS" << endl;
	    } else
	    {
		    gps_set_nav_rate(10);
		    gps_set_msg_rate(GPS_MSG_PVT,-3);
	    }
    }
}


void AState::GPS_thread()
{
	char *gps_buffer;
	unsigned long long gps_local_time;
	if ((gps_buffer = (char *) malloc(2000)) == NULL)
	{
		cerr << "AState::GPS_thread(): memory allocation error" << endl;
	}

	LatLong gps_ll(0,0);
	double dn;
	double de;
	double product;
	double magnitude;
	double angle;
   	double jump;
    unsigned long long nowtime;
	
	while ( QUIT_PRESSED != 1) {
		if (replay == 1) {
			gps_buffer[3] = GPS_MSG_PVT;
		} else {
			gps_read(gps_buffer, 1000);  //Need to determine if this is blocking...
		}
		//cout << "GPS message: " << gps_msg_type(gps_buffer) << endl;
		DGCgettime(gps_local_time); // Time stamp as soon as data read.
		switch (gps_msg_type(gps_buffer)) {
			case GPS_MSG_PVT:

				// LOCK THE MUTEX!
				DGClockMutex(&m_GPSDataMutex);
					my_gps_time = gps_local_time;
					if (replay == 1) {
                        gps_replay_stream >> my_gps_time;
                        gps_replay_stream >> gps_valid;
                        gps_replay_stream >> gpsdata.data.lat;
                        gps_replay_stream >> gpsdata.data.lng;
                        gps_replay_stream >> gpsdata.data.altitude;
                        gps_replay_stream >> gpsdata.data.vel_e;
                        gps_replay_stream >> gpsdata.data.vel_n;
                        gps_replay_stream >> gpsdata.data.vel_u;
                        my_gps_time = my_gps_time - imu_log_start + starttime;
                        DGCgettime(nowtime);
                        while (my_gps_time > nowtime) {
				DGCunlockMutex(&m_GPSDataMutex);
                            usleep(10);
				DGClockMutex(&m_GPSDataMutex);
                            DGCgettime(nowtime);
                        }
                        
						//gpsdata.data.nav_mode = 128;
						//gpsdata.data.lat = 34;
						//gpsdata.data.lng = -118;
					} else {
						if (gpsdata.update_gps_data(gps_buffer) == -1) {
              //cout << "Ignored invalid PVT" << endl;
				      DGCunlockMutex(&m_GPSDataMutex);
              continue;
            }
					  gps_valid = (gpsdata.data.nav_mode & NAV_VALID) && 1;
					}

                    if (log_raw == 1) {
                        gps_log_stream.precision(35);
                        gps_log_stream << my_gps_time;
                        gps_log_stream << '\t' << gps_valid;
                        gps_log_stream << '\t' << fixed << gpsdata.data.lat;
                        gps_log_stream << '\t' << fixed << gpsdata.data.lng;
                        gps_log_stream << '\t' << fixed << gpsdata.data.altitude;
                        gps_log_stream << '\t' << fixed << gpsdata.data.vel_e;
                        gps_log_stream << '\t' << fixed << gpsdata.data.vel_u;
                        gps_log_stream << '\t' << fixed << gpsdata.data.vel_u;
                        gps_log_stream << endl;
                    }
          //gps_valid = 1;
					if (gps_valid) {
						gps_ll.set_latlon(gpsdata.data.lat, gpsdata.data.lng);
						gps_ll.get_UTM(&gps_east, &gps_north);
						de = gps_east - vehiclestate.Easting;
						dn = gps_north - vehiclestate.Northing;
						magnitude = sqrt( dn * dn + de * de );
						product = (dn * cos(vehiclestate.Yaw) + de * sin(vehiclestate.Yaw)) / magnitude;
						angle = acos(product);
						gps_err = sqrt(GPS_xy_kr) * EARTH_RADIUS;

						if (angle >= ANGLE_CUTOFF) {
                            jump = magnitude;
                        } else {
                            jump = 0;
                        }
                        if (jump > gps_err) {   
							gps_err = magnitude * ERROR_WEIGHT;
						} else {
							gps_err *= ERROR_FALLOFF;
						}

                        if (gps_err < 2) {
                            gps_err = 2;
                        }  
						GPS_xy_kr = pow(gps_err / EARTH_RADIUS, 2);
					  ++gps_count;
					}
				DGCunlockMutex(&m_GPSDataMutex);
		}
	}
  free(gps_buffer);
}
