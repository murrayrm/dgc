#include "AState_test.hh"

char *testlog  = new char[100];
char *pathlog  = new char[100];


AState_test::AState_test(int skynet_key) 
	: CSkynetContainer(SNastate_test, skynet_key) {
		DGCgettime(starttime);
		testlogfile.open(testlog, fstream::out|fstream::app);
		testlogfile << "Time\tnorth\teast\talt\tVel_N\tVel_E\tVel_U\tacc_n\tacc_e\tacc_u\tRoll\tPitch\tYaw\trollrate\tpitchrate\tyawrate\trollacc\tpitchacc\tyawacc\n\r";
		testlogfile.close();

}

void AState_test::Active() {
  //double outtime;
	while( true ) 
	{
		UpdateState();
	  testlogfile.open(testlog, fstream::out|fstream::app);
    //outtime = DGCTimetoSec(m_state.Timestamp);
    
		testlogfile.precision(10);
    		testlogfile <<  m_state.Timestamp << '\t';
    		testlogfile <<  m_state.Northing << '\t' << m_state.Easting << '\t' << m_state.Altitude << "\t";
    		testlogfile <<  m_state.Vel_N << '\t' << m_state.Vel_E << '\t' << m_state.Vel_D << "\t";
    		testlogfile <<  m_state.Acc_N << '\t' << m_state.Acc_E << '\t' << m_state.Acc_D << "\t";
    		testlogfile <<  m_state.Roll << '\t' << m_state.Pitch << '\t' << m_state.Yaw << "\t";
    		testlogfile <<  m_state.RollRate << '\t' << m_state.PitchRate << '\t' << m_state.YawRate << "\t";
    		testlogfile <<  m_state.RollAcc << '\t' << m_state.PitchAcc << '\t' << m_state.YawAcc << "\r\n";
		testlogfile.close();

    		pathlogfile.open(pathlog, fstream::out|fstream::app);
        	pathlogfile.precision(10);
	        pathlogfile << m_state.Northing << '\t' << m_state.Vel_N << '\t' << m_state.Acc_N << '\t';
		pathlogfile << m_state.Easting << '\t' << m_state.Vel_E << '\t' << m_state.Acc_E << '\t';
		pathlogfile << m_state.Altitude << '\t' << m_state.Vel_D << '\t' << m_state.Acc_D << "\r\n";
		pathlogfile.close();
    		usleep(3000);
	}
}


int main(int argc, char **argv) {

	sprintf(testlog,"%s_full.dat",argv[1]);
	sprintf(pathlog,"%s_path.dat",argv[1]);

	int sn_key = 0;
	char* pSkynetkey = getenv("SKYNET_KEY");
	if ( pSkynetkey == NULL)
	{
		cerr << "SKYNET_KEY environment variable isn't set." << endl;
	} else
	{
		sn_key = atoi(pSkynetkey);
	}
	cerr << "Constructing skynet with KEY = " << sn_key << endl;
	AState_test ast(sn_key);
	ast.Active();
  	return 0;
}
