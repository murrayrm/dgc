/* AState_IMU.cc - new vehicle state estimator
 *
 * JML 31 Dec 04
 *
 * */

#include "AState.hh"

extern double mag_hdg;
extern int New_Mag_data_flag;

void AState::Mag_init(double MAG_HDG = 0)
{
    if (mag_enabled == 0) {
        mag_heading = MAG_HDG*DEG2RAD;
        mag_hdg = mag_heading;
	    New_Mag_data_flag = 1;
        return;
    }
    mag_enabled = -1;
	double tmp = 0.0;
	cout << "Mag could not be started, enter current heading in degrees:";
	cin >> tmp;
	cin.clear();
	mag_heading = tmp*DEG2RAD;
	mag_hdg = mag_heading;
	New_Mag_data_flag = 1;
}

void AState::Mag_thread()
{
	if (mag_enabled == -1)
	{
		cerr << "AState::Mag_thread() unable to start" << endl;
	    return;
	}
}
