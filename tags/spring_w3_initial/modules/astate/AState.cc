/* AState.cc - new vehicle state estimator
 *
 * JML 31 Dec 04
 *
 * */

#include "AState.hh"

extern double gps_offset_X;
extern double gps_offset_Y;
extern double gps_offset_Z;
extern int inhib_vert_vel;

extern double GPS_xy_kr;

int QUIT_PRESSED = 0;

AState::AState(int skynet_key, int USE_KF = 1, int USE_IMU = 1, int USE_GPS = 1, 
		int USE_MAG = 1, int USE_VERBOSE = -1, double MAG_HDG = 0, 
        int LOG_RAW = -1, char* LOG_FILE = NULL, int REPLAY = -1, char* REPLAY_FILE = NULL)
	: m_skynet(SNastate, skynet_key),
	latlong(0,0),
	blank(0,0,0),
	imu_offset(-X_IMU, -Y_IMU, -Z_IMU),
	imu_vector(imu_offset, -PITCH_IMU, -ROLL_IMU, -YAW_IMU) // These are negative because of way transformation is done
{
	snkey = skynet_key;

	kfilter_enabled = USE_KF;
	imu_enabled = USE_IMU;
	gps_enabled = USE_GPS;
	mag_enabled = USE_MAG;
	verbose = USE_VERBOSE;

    log_raw = LOG_RAW;
    sprintf(imu_log_file, "%s_IMU.raw", LOG_FILE);
    sprintf(gps_log_file, "%s_GPS.raw", LOG_FILE);
    replay = REPLAY;
    sprintf(imu_replay_file, "%s_IMU.raw", REPLAY_FILE);
    sprintf(gps_replay_file, "%s_GPS.raw", REPLAY_FILE);

    if (log_raw == 1) {
        imu_log_stream.open(imu_log_file, fstream::out);
        gps_log_stream.open(gps_log_file, fstream::out);
    }

    if (replay == 1) {
        imu_replay_stream.open(imu_replay_file, fstream::in);
        gps_replay_stream.open(gps_replay_file, fstream::in);
    }

	//FInd startting time;
	DGCgettime(starttime);

	DGCgettime(vehiclestate.Timestamp);

    // Zero stuff for good measure.
	vehiclestate.Northing = 0;
	vehiclestate.Easting = 0;
	vehiclestate.Altitude = 0;
	vehiclestate.Vel_N = 0;
	vehiclestate.Vel_E = 0;
	vehiclestate.Vel_D = 0;
	vehiclestate.Acc_N = 0;
	vehiclestate.Acc_E = 0;
	vehiclestate.Acc_D = 0;
	vehiclestate.Roll = 0;
	vehiclestate.Pitch = 0;
	vehiclestate.Yaw = 0;
	vehiclestate.RollAcc = 0;
	vehiclestate.PitchAcc = 0;
	vehiclestate.YawAcc = 0;

	imudata.dvx = 0;
	imudata.dvy = 0;
	imudata.dvz = 9.8/400;
	imudata.dtx = 0;
	imudata.dty = 0;
	imudata.dtz = 0;

	gps_north = 0;
	gps_east = 0;
	gpsdata.data.altitude = 0;
	gpsdata.data.vel_n = 0;
	gpsdata.data.vel_e = 0;
	gps_err = 0;
	mag_heading = 0;

	imu_count = 0;
	gps_count = 0;
	mag_count = 0;

    lastIndex = 0;

	//Create mutexes
	DGCcreateMutex(&m_VehicleStateMutex);
	DGCcreateMutex(&m_MetaStateMutex);
	DGCcreateMutex(&m_GPSDataMutex);
	DGCcreateMutex(&m_IMUDataMutex);


	//Initialize sensors:
	IMU_init();
	GPS_init();
	Mag_init(MAG_HDG);


	//Initialize Kalmanfilter:
	// These values should be GPS in IMU reference frame


	gps_offset_X = X_GPS - X_IMU;
	gps_offset_Y = Y_GPS - Y_IMU;
	gps_offset_Z = Z_GPS - Z_IMU;

	if(kfilter_enabled == 1)
	    DGCNavInit();

    inhib_vert_vel = 0;

	metastate.gps_enabled = gps_enabled;
	metastate.imu_enabled = imu_enabled;
	metastate.mag_enabled = mag_enabled;
	metastate.gps_pvt_valid = 0;
	metastate.ext_jump_flag = 0;


    //Start information threads
	DGCstartMemberFunctionThread(this, &AState::GPS_thread);
	DGCstartMemberFunctionThread(this, &AState::Mag_thread);
	DGCstartMemberFunctionThread(this, &AState::IMU_thread);

	broadcast_statesock = m_skynet.get_send_sock(SNstate);
	if(broadcast_statesock < 0)
		cerr << "AState::VehicleStateMsg_thread(): skynet get_send_sock returned error" << endl;

	DGCstartMemberFunctionThread(this, &AState::UpdateSparrow_thread);
	//DGCstartMemberFunctionThread(this, &AState::SparrowDisplayLoop);
  SparrowDisplayLoop();
	
	//VehicleStateMsg_thread();

}

AState::~AState()
{
	cout << "Thankyou for using AState..." << endl;
}

void AState::printMetaState()
{
	cout << "Meta State:" << endl;
}

void AState::printVehicleState()
{
	cout << "Vehicle State:" << endl;
}

// VehicleState messaging thread:
void AState::Broadcast()
{
		if(m_skynet.send_msg(broadcast_statesock,
					&vehiclestate, 
					sizeof(vehiclestate), 
					0, 
					&m_VehicleStateMutex) 
				!= sizeof(vehiclestate))
		{
			cerr << "AState::Broadcast(): didn't send right size state message" << endl;
		}
}

void AState::VehicleStateMsg_thread()
{
	//Get sockets for receiving/sending data:
	int getstatesock = m_skynet.listen(SNreqstate, ALLMODULES);
	if(getstatesock < 0)
		cerr << "AState::VehicleStateMsg_thread(): skynet listen returned error" << endl;

	int statesock = m_skynet.get_send_sock(SNstate);
	if(statesock < 0)
		cerr << "AState::VehicleStateMsg_thread(): skynet get_send_sock returned error" << endl;

	char getstatedummy;

	//Loop listening for messages and then sending:
	while(QUIT_PRESSED != 1)
	{
		cout << "Starting to listen for reqstate" << endl;
		if(m_skynet.get_msg(getstatesock,
					&getstatedummy, 
					sizeof(getstatedummy), 0)
				!= sizeof(getstatedummy))
		{
			cerr << "AState::VehicleStateMsg_thread(): didn't received right size getstate command" << endl;
			continue;
		}
		cout << "Starting to broadcast reqstate" << endl;
		if(m_skynet.send_msg(statesock,
					&vehiclestate, 
					sizeof(vehiclestate), 
					0, 
					&m_VehicleStateMutex) 
				!= sizeof(vehiclestate))
		{
			cerr << "AState::VehicleStateMsg_thread(): didn't send right size state message" << endl;
		}
	}
}

// MetaState messaging thread:
void AState::MetaStateMsg_thread()
{
	//Get sockets for receiving/sending data:
	int getstatesock = m_skynet.listen(SNgetmetastate, ALLMODULES);
	if(getstatesock < 0)
		cerr << "AState::MetaStateMsg_thread(): skynet listen returned error" << endl;

	int statesock = m_skynet.get_send_sock(SNmetastate);
	if(statesock < 0)
		cerr << "AState::MetaStateMsg_thread(): skynet get_send_sock returned error" << endl;

	char getstatedummy;

	//Loop listening for messages and then sending:
	while(QUIT_PRESSED != 1)
	{
		if(m_skynet.get_msg(getstatesock, &getstatedummy, 
					sizeof(getstatedummy), 0) != sizeof(getstatedummy))
		{
			cerr << "AState::MetaStateMsg_thread(): didn't received right size getstate command" << endl;
			continue;
		}

		if(m_skynet.send_msg(statesock, &metastate, 
					sizeof(metastate), 0, &m_MetaStateMutex) != sizeof(metastate))
		{
			cerr << "AState::MetaStateMsg_thread(): didn't send right size state message" << endl;
		}
	}
}
