/* AState_IMU.cc - new vehicle state estimator
 *
 * JML 31 Dec 04
 *
 * */

#include "AState.hh"

extern int QUIT_PRESSED;

void AState::IMU_init()
{
	if(imu_enabled == 0)
	{
		return;
	}
        if (replay == 1) {
                imu_replay_stream >> my_imu_time;
                imu_replay_stream >> imudata.dvx;
                imu_replay_stream >> imudata.dvy;
                imu_replay_stream >> imudata.dvz;
                imu_replay_stream >> imudata.dtx;
                imu_replay_stream >> imudata.dty;
                imu_replay_stream >> imudata.dtz;
                imu_log_start = my_imu_time;
        } else {

	imu_enabled = IMU_open();
	if(imu_enabled == -1)
		cerr << "AState::IMU_init(): failed to open IMU" << endl;

        }
}

void AState::IMU_thread()
{
    unsigned long long nowtime;
    if (imu_enabled == 0) {
        return;
    } else if (imu_enabled == -1) {
        cerr << "AState::IMU_thread(): not started because IMU failed to start";
    }
  	while ( QUIT_PRESSED != 1) {
		DGClockMutex(&m_IMUDataMutex);
			if (replay == 1) 
			{
                imu_replay_stream >> my_imu_time;
                imu_replay_stream >> imudata.dvx;
                imu_replay_stream >> imudata.dvy;
                imu_replay_stream >> imudata.dvz;
                imu_replay_stream >> imudata.dtx;
                imu_replay_stream >> imudata.dty;
                imu_replay_stream >> imudata.dtz;
                    //fscanf(imu_replay_stream, "%Lu\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\r\n", &my_imu_time, &imudata.dvx, &imudata.dvy, &imudata.dvz, &imudata.dtx, &imudata.dty, &imudata.dtz);
                    my_imu_time = my_imu_time - imu_log_start + starttime;
                    DGCgettime(nowtime);
                    while( my_imu_time > nowtime ) {
                        usleep(5);
                        DGCgettime(nowtime);
                    }
			} else {
      				IMU_read(&imudata); // Need to add new code for IMU_read!
			        DGCgettime(my_imu_time); // time stamp as soon as data read.
			}
            if (log_raw == 1)
            {
                imu_log_stream.precision(15);
                imu_log_stream << my_imu_time;
                imu_log_stream << '\t' << fixed << imudata.dvx;
                imu_log_stream << '\t' << fixed << imudata.dvy;
                imu_log_stream << '\t' << fixed << imudata.dvz;
                imu_log_stream << '\t' << fixed << imudata.dtx;
                imu_log_stream << '\t' << fixed << imudata.dty;
                imu_log_stream << '\t' << fixed << imudata.dtz;
                imu_log_stream << endl;
            }
      		++imu_count;
		DGCunlockMutex(&m_IMUDataMutex);
		UpdateState();
	}
}
