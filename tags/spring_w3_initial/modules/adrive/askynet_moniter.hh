/*! This is the header file to allow other functions to call skynet_main.  
 * Which is the skynet thread for adrive.*/

#ifndef ASKYNET_MONITER_H
#define ASKYNET_MONITER_H
void* skynet_main(void *arg);

#endif // ASKYNET_MONITER_H
