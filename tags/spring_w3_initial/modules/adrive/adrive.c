/* This is a initial try at building a threaded adrive.  
 * It lays down the functions needed for each actuator.  
 * and provides all the structure.  
 * Tully Foote
 * 12/29/04
 */


/* None of the serial code has been implemented.  It is 
 * either in comments or in //printf statements. 
 * 12/29/04 Tully */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <fstream>
// I have gotten fed up with DGCutils.  
#include <sys/time.h>
#include <time.h>
//#include "DGCutils"
#include "parker_steer.h"
#include "adrive.h"
#include "actuators.h"
#include "brake.h"
#include "throttle.h"
#include "askynet_moniter.hh"
#include "sn_msg.hh"

#include "sparrow/display.h"     /* sparrow display header */
#include "sparrow/debug.h"       /* sparrow debugging routines */
#include "addisp.h"			/* adrive display table */


#define LOGGING_DELAY 10000

// Set initial values of flags.  
int shutdown_flag = false;
int run_sparrow_thread = true;
int run_skynet_thread = true;
int run_logging_thread = true;
pthread_cond_t steer_calibrated_flag;
int simulation_flag = false;
char simulator_IP[15];

struct vehicle_t my_vehicle;		/* The vehicle data structure */

void init_vehicle_struct(vehicle_t & new_vehicle);
void read_config_file(vehicle_t & my_vehicle);


// Callback functions from sparrow to start command threads.  
/*! The gas function call for sparrow bind key to execute a gas command. */
int flag_gas_condition(long x)
{
  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_GAS].cond_flag );
  return 0;
}
/*! The brake function call for sparrow bind key to execute a brake command. */
int flag_brake_condition(long x)
{
  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_BRAKE].cond_flag );
  return 0;
}
/*! The steer function call for sparrow bind key to execute a steer command. */
int flag_steer_condition(long x)
{
  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_STEER].cond_flag );
  return 0;
}
/*! This function is for the sparrow binding to reenable the steering motor
 * after being manually diabled. */
int flag_steer_enable(long x)
{
  steer_enable_overide();
  return 0;
}


/*! The function that is called as the logging thread of adrive if enabled.  
 * This function periodically records the actuators commanded and 
 * actual positions. */
void* logging_main (void* arg){
  char testlog[255];
  char datestamp[255];
  unsigned long long current_time;
  fstream testlogfile;
  time_t t = time(NULL);
  tm *local;
  

  local = localtime(&t);
  
  //sprintf(testlog,"adrive_log.dat");
  sprintf(datestamp, "%02d%02d%04d_%02d%02d%02d",
	  local->tm_mon+1, local->tm_mday, local->tm_year+1900,
	  local->tm_hour, local->tm_min, local->tm_sec);
 
  sprintf(testlog, "logs/adrive_%s.log", datestamp);
    
  while (run_logging_thread)
    {
      //      printf("Running Logging thread");
      // Fed up with DGCutils
	timeval tv;
	gettimeofday(&tv, NULL);
	current_time = (unsigned long long)tv.tv_usec + 1000000ULL * tv.tv_sec;      
	//DGCgettime(current_time);
      testlogfile.open(testlog, fstream::out|fstream::app);
      testlogfile.precision(10);
      testlogfile <<  current_time << '\t';
      testlogfile <<  my_vehicle.actuator[ACTUATOR_STEER].command << '\t' << my_vehicle.actuator[ACTUATOR_STEER].position << '\t';
      testlogfile <<  my_vehicle.actuator[ACTUATOR_BRAKE].command << '\t' << my_vehicle.actuator[ACTUATOR_BRAKE].position << '\t' << my_vehicle.actuator[ACTUATOR_BRAKE].pressure << "\t";
      testlogfile <<  my_vehicle.actuator[ACTUATOR_GAS].command   << '\t' << my_vehicle.actuator[ACTUATOR_GAS].position   << "\r\n" ;
      testlogfile.close();
      usleep(LOGGING_DELAY);
    }
  return 0;
}




/*! THe function that runs as the sparrow thread of adrive. When sparrow is 
 * enabled this thread waits for the steering to calibrate then will take over
 * the screen with the UI.  
 *
 **** Caution should be exercized when using this interface there are
 * no protections against bad values and all fields are editable.  
 * */
void* sparrow_main (void* arg)
{
  //printf("Sparrow main is running\n");

  vehicle_t *vehicle = (vehicle_t*) arg;
  pthread_mutex_lock(& my_vehicle.actuator[ACTUATOR_STEER].mutex);
  
  //printf("command %d, status %d\n", my_vehicle.actuator[ACTUATOR_STEER].command_enabled, my_vehicle.actuator[ACTUATOR_STEER].status_enabled);
  if( my_vehicle.actuator[ACTUATOR_STEER].command_enabled || my_vehicle.actuator[ACTUATOR_STEER].status_enabled)
    {
      //    printf("the steering is enabled I will wait\n");
      pthread_cond_wait(&steer_calibrated_flag, &my_vehicle.actuator[ACTUATOR_STEER].mutex);
      // printf("sparrow has been awakened\n");
    }
  pthread_mutex_unlock(& my_vehicle.actuator[ACTUATOR_STEER].mutex);

  /* Initialize the screen and run the main display loop */
  if (dd_open() < 0) { dbg_error("can't open display"); exit(-1); }
  if (dd_usetbl(addisp) < 0) { dbg_error("can't open display table"); exit(-1); }
  //*! Pressing g executes the gas command */
  dd_bindkey( 'g' , flag_gas_condition   ); 
  //*! Pressing b executes the brake command */
  dd_bindkey( 'b' , flag_brake_condition );
  //*! Pressing s executes the steer command */
  dd_bindkey( 's' , flag_steer_condition );
  //*! Pressing e reenables steering */
  dd_bindkey( 'e' , flag_steer_enable    );
  dd_loop();
  dd_close();

  sleep(3);
  exit(0);
}


/*! This is the generic thread function for all actuator status loops.  
 * Each different actuator status thread it dynamically created from this
 * template if it is enabled when adrive is started. */
void* actuator_status_thread_function(void* arg)
{
  actuator_t *act = (actuator_t*) arg;

  //printf("Starting Thread: %s Status\n", act->name);

  pthread_once(&(act->start_bit), act->execute_init);

  while( !shutdown_flag && act->status_enabled) {  
    //    pthread_wait_cond(&(steer->cond_flag));
    usleep(STATUS_SLEEP_LENGTH);
    // Lock the mutex
    pthread_mutex_lock(&act->mutex);

    // Execute the function
    if ( act->execute_status() == ERROR_HAPPENED) 
      {
      //printf("%s Status: SOMETHING BAD HAPPENED\n", act->name);
      }
    else {
      act->status_loop_counter++;
      //printf("%s Status: Que Bueno! %d\n", act->name, act->status_loop_counter);
    }
    // Unlock the mutex
    pthread_mutex_unlock(&act->mutex);
  }
  
  // SHUTDOWN CODE GOES HERE INCLUDING SYNCHRONIZATION WITH STATUS THREAD.
  return NULL;
}


/*! This is the generic thread function for all actuator command loops.  
 * Each different actuator command thread it dynamically created from this
 * template if it is enabled when adrive is started. */
void* actuator_command_thread_function(void* arg)
{
  actuator_t *act = (actuator_t*) arg;

  //printf("Starting Thread: %s Command\n", act->name);

  pthread_once(&(act->start_bit), act->execute_init);

  while( !shutdown_flag && act->command_enabled) {  

    // Lock the mutex
    pthread_mutex_lock(&act->mutex);

    pthread_cond_wait(&act->cond_flag, &act->mutex);

    if ( act->execute_command(act->command) == ERROR_HAPPENED) 
      {
	//printf("%s Command: SOMETHING BAD HAPPENED\n", act->name);
      }
    else {
      act->command_loop_counter++;
      //printf("%s Command: Que Bueno! %d\n", act->name, act->command_loop_counter);
    }
    // Unlock the mutex
    pthread_mutex_unlock(&act->mutex);   
  }
  
  // SHUTDOWN CODE GOES HERE INCLUDING SYNCHRONIZATION WITH STATUS THREAD.
  return NULL;
}















/*! Adrive is a multi threaded program.  For each actuator there will be 
 * 2 threads running once to periodically poll for status, and a second 
 * interupt based thread executing commands. In addition sparrow is run 
 * in its own thread.  Also logging is another thread.  And the skynet 
 * interface is another thread.   
 *
 * The process is first to initialize the vehicle struct and mutexes.
 * Second if sparrow is enabled it must be started first for it waits for 
 * the steering to initialize, if steering is enabled, so that the calibration
 * routine can happen while the user can still see the screen.  
 * After that each enabled actuator thread is created. 
 * Adrive is then done, it waits for all the threads to end. ANd has the 
 * to add additional shutdown functionality.  But nothing is implemented. */
int main(int argc, char** argv)
{
  int i;
  // Run statup functions.
  init_vehicle_struct( my_vehicle );
  read_config_file( my_vehicle );

  //Start up sparrow first so that it can wait for steering if necessary.
  pthread_t sparrow_handle;
  if( run_sparrow_thread )  
    pthread_create( &sparrow_handle, NULL, &sparrow_main, &my_vehicle );



  // Start threads for actuators if we are using them
  // 
  for(i=0; i<NUM_ACTUATORS; i++) {
    actuator_t * act = &(my_vehicle.actuator[i]);

    if( act->status_enabled ) 
      pthread_create(& act->pthread_status_handle, NULL, &actuator_status_thread_function, act );
    if( act->command_enabled ) 
      pthread_create(& act->pthread_command_handle, NULL, &actuator_command_thread_function, act );
  }

  
  pthread_t skynet_handle;
  if( run_skynet_thread )  
    pthread_create( &skynet_handle, NULL, &skynet_main, &my_vehicle );

  pthread_t logging_handle;  
  if (run_logging_thread)
    pthread_create( &logging_handle, NULL, &logging_main, 0);

  // NOW JOIN THREADS IN ETERNAL HAPPINESS  
  for(i=0; i<NUM_ACTUATORS; i++) {
    actuator_t * act = &(my_vehicle.actuator[i]);

    if( act->status_enabled ) 
      pthread_join( act->pthread_status_handle, NULL);
    if( act->command_enabled ) 
      pthread_join( act->pthread_command_handle, NULL);
  }
  if( run_sparrow_thread ) {
    pthread_join( sparrow_handle, NULL);
  }
  if( run_skynet_thread )  
    pthread_join( skynet_handle, NULL);


  // TIME TO SHUT DOWN 

  // .. stuff goes here maybe
  
  return 0;
} /* End of main */


























// MOVE THIS SOMEWHERE ELSE LATER
char * actuator_names[NUM_ACTUATORS] = { "BRAKE",
					 "GAS",
					 "TRANS",
					 "STEER",
					 "ESTOP" };

// MOVE THIS LATER
actuator_command_function actuator_command_functions[NUM_ACTUATORS] = { execute_brake_command,
									execute_gas_command,
									execute_trans_command,
									execute_steer_command,
									execute_estop_command };
// MOVE THIS LATER
actuator_status_function actuator_status_functions[NUM_ACTUATORS] = { execute_brake_status,
								      execute_gas_status,
								      execute_trans_status,
								      execute_steer_status,
								      execute_estop_status };
// MOVE THIS LATER
actuator_init_function actuator_init_functions[NUM_ACTUATORS] = { execute_brake_init,
								  execute_gas_init,
								  execute_trans_init,
								  execute_steer_init,
								  execute_estop_init };


/*!** THe function which allocates the necessary  *****
 *  memory for all the vehicle srructures */
void init_vehicle_struct(vehicle_t & new_vehicle)
{
  int i;

  //  1.  Initialize Mutexes

  // Initialize acutator mutexes
  for(i=0; i<NUM_ACTUATORS; i++) 
    {
      actuator_t * p_actuator = & (new_vehicle.actuator[i]);

      pthread_mutex_init(&p_actuator->mutex, NULL);
      pthread_cond_init(&p_actuator->cond_flag, NULL);
      p_actuator->start_bit = 0;
      p_actuator->status_enabled  = true;
      p_actuator->command_enabled = true;
      p_actuator->status   = OFF;
      p_actuator->command  = 0;
      p_actuator->position = 0;
      p_actuator->pressure = 0;
      p_actuator->error    = NO_ERROR;      
      p_actuator->status_loop_counter = 0;
      p_actuator->command_loop_counter = 0;
      strcpy( p_actuator->name, actuator_names[i] );
      
      p_actuator->execute_command = actuator_command_functions[i];
      p_actuator->execute_status  = actuator_status_functions[i];
      p_actuator->execute_init    = actuator_init_functions[i];

    }
  
  //Initialize the steering calibration conditional.  
  pthread_cond_init(&steer_calibrated_flag, NULL);
         
}   /* ENd of build_vehicle() */

#include <iostream>
#include <fstream>
using namespace std;

void read_config_file(vehicle_t & my_vehicle) 
{
  ifstream infile("config/adrive/adrive.config");

  std::string cmd;

  while ( ! infile.eof() )
    {
      infile >> cmd;
      /* TO BE SAFE ALL OF THESE WRITINGS WOULD NEED TO BE MUTEX LOCKED
       * BUT SINCE THIS IS A ONETIME START UP BEFORE THE OTHER THREADS START 
       * IT IS OK.  IF THAT CHANGES LOCKING MUST BE ADDED.  */
      // Serial Ports
      if( cmd == "gas_port" )
	{
	  infile >> my_vehicle.actuator[ACTUATOR_GAS].port;
	  //printf("using gas_port &d\n", my_vehicle.actuator[ACTUATOR_GAS].port);
	}
      else if ( cmd == "brake_port" )
	{
	  infile >> my_vehicle.actuator[ACTUATOR_BRAKE].port;
	  //printf("using brake_port &d\n", my_vehicle.actuator[ACTUATOR_BRAKE].port);
	}
      else if ( cmd == "trans_port" )
	{
	  infile >> my_vehicle.actuator[ACTUATOR_TRANS].port;
	  //printf("using trans_port &d\n", my_vehicle.actuator[ACTUATOR_TRANS].port);
	}
      else if ( cmd == "estop_port" )
	{
	  infile >> my_vehicle.actuator[ACTUATOR_ESTOP].port;
	  //printf("using estop_port &d\n", my_vehicle.actuator[ACTUATOR_ESTOP].port);
	}
      else if ( cmd == "steer_port" )
	{
	  infile >> my_vehicle.actuator[ACTUATOR_STEER].port;
	  //printf("using steer_port &d\n", my_vehicle.actuator[ACTUATOR_STEER].port);
	}


      /***********************************
       * Check for disabled threads.
       ***********************************/
      // Status Threads
      else if ( cmd == "disable_steer_status" )
	{
	  my_vehicle.actuator[ACTUATOR_STEER].status_enabled = 0;
	}
      else if ( cmd == "disable_brake_status" )
	{
	  my_vehicle.actuator[ACTUATOR_BRAKE].status_enabled = 0;
	}
      else if ( cmd == "disable_gas_status" )
	{
	  my_vehicle.actuator[ACTUATOR_GAS].status_enabled = 0;
	}
      else if ( cmd == "disable_trans_status" )
	{
	  my_vehicle.actuator[ACTUATOR_TRANS].status_enabled = 0;
	}
      else if ( cmd == "disable_estop_status" )
	{
	  my_vehicle.actuator[ACTUATOR_ESTOP].status_enabled = 0;
	}

      // Command Threads
      else if ( cmd == "disable_steer_command" )
	{
	  my_vehicle.actuator[ACTUATOR_STEER].command_enabled = 0;
	}
      else if ( cmd == "disable_brake_command" )
	{
	  my_vehicle.actuator[ACTUATOR_BRAKE].command_enabled = 0;
	}
      else if ( cmd == "disable_gas_command" )
	{
	  my_vehicle.actuator[ACTUATOR_GAS].command_enabled = 0;
	}
      else if ( cmd == "disable_trans_command" )
	{
	  my_vehicle.actuator[ACTUATOR_TRANS].command_enabled = 0;
	}
      else if ( cmd == "disable_estop_command" )
	{
	  my_vehicle.actuator[ACTUATOR_ESTOP].command_enabled = 0;
	}


      // Check for Sparrow and Skynet interface enabled
      else if ( cmd == "disable_sparrow" )
	{
	  //The default is true
	  run_sparrow_thread = false;
	}	  
      else if ( cmd == "disable_skynet" )
	{
	  //The default is true
	  run_skynet_thread = false;
	}	  
      else if ( cmd == "disable_logging" )
	{
	  //The default is true
	  run_logging_thread = false;
	}	  
      else if ( cmd == "enable_simulation" )
	{
	  //The default is true
	  simulation_flag = true;
	  infile >> simulator_IP;
	}	  

      
      // ACTUATORS ENABLED DEFAULTS -- BOTH STATUS AND COMMMAND
      /*
      else if ( cmd == "gas_enabled" )
	{
	  infile >> my_vehicle.actuator[ACTUATOR_GAS].enabled;
	}
      
      #warning "Finish read_text_file"
      #warning "Add flags for both status and command threads"
      */
    }

}
