#include <stdio.h>
#include <pthread.h>


#include "adrive.h"
#include "adrive_skynet_interface_types.h"
#include "sn_msg.hh"


#define BUFSIZE 1024

// The vehicle structure to work with.  
extern struct vehicle_t my_vehicle;
extern int shutdown_flag;
extern int run_skynet_thread;

void parse_status_request(char buffer[BUFSIZE]);
void parse_command(char buffer[BUFSIZE]);
void sn_broadcast_position(int actuator);

/*! This is the thread that will moniter skynet for adrive commands and 
 * cause them to execute.  It will pull the skynet key from the environment
 * variable.  */
void* skynet_main(void *){
  char* default_key = getenv("SKYNET_KEY");
  int sn_key = atoi(default_key);
  my_vehicle.skynet_key = sn_key;
  modulename myself = SNadrive;
  modulename other_mod = SNadrive_commander;
  skynet Skynet(myself, sn_key);
  //printf("My module ID: %u\n My key is %d\n", Skynet.get_id(),sn_key);
  sn_msg myinput = SNdrivecmd;
  sn_msg myoutput = SNdrivecom;
  sn_msg myoutput2 = SNdrivelog;

  
  int socket_input = Skynet.listen(myinput, other_mod);
  int socket_output = Skynet.get_send_sock(myoutput);
  int socket_output2 = Skynet.get_send_sock(myoutput2);
  drivecmd_t  my_command;
  drivecom_t  my_return;

  while (shutdown_flag == false && run_skynet_thread == true)
    {
      // Listen for an incoming sknet message.  
      Skynet.get_msg(socket_input,&my_command, sizeof(my_command), 0);
      // Parse the command and execute.  
      switch (my_command.my_actuator){
	// These cases came from adrive_skynet_interface_types.h
	// This structure assumes that the actions for each actuator
	// will be different.  If they are determined to be homogeneous, 
	// this case structure can be serialized.  
	
      case steer: 
	// record that we recieved a skynet message to steering.  
	  pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_STEER].mutex );
	  my_vehicle.actuator[ACTUATOR_STEER].command_loop_counter ++;
	  pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_STEER].mutex );
       
	//Do something with it.
	switch (my_command.my_command_type){
	case get_position:
	  my_return.my_actuator = steer;
	  my_return.my_return_type = return_position;
	  
	  pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_STEER].mutex );
	  my_return.number_arg = my_vehicle.actuator[ACTUATOR_STEER].position;
	  pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_STEER].mutex );
       	  
	  Skynet.send_msg(socket_output, &my_return, sizeof(my_return), 0);
	  break;
	case set_position:
	  pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_STEER].mutex );
	  my_vehicle.actuator[ACTUATOR_STEER].command = my_command.number_arg; 
	  pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_STEER].mutex );
	  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_STEER].cond_flag );
	  break;      
	}
	break;
      case gas:
	// record that we recieved a skynet message to gas.  
	  pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_GAS].mutex );
	  my_vehicle.actuator[ACTUATOR_GAS].command_loop_counter ++;
	  pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_GAS].mutex );

	//Do something with it.       
	switch (my_command.my_command_type){
	case set_position:
	  pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_GAS].mutex );
	  my_vehicle.actuator[ACTUATOR_GAS].command = my_command.number_arg;
	  pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_GAS].mutex );
	  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_GAS].cond_flag );
	  break;
       	case get_position:
	  my_return.my_actuator = gas;
	  my_return.my_return_type = return_position;

	  pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_GAS].mutex );
	  my_return.number_arg = my_vehicle.actuator[ACTUATOR_GAS].position;
	  pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_GAS].mutex );
	  
	  Skynet.send_msg(socket_output, &my_return, sizeof(my_return), 0);
	  break;
	}
	break;
      case brake:
	// record that we recieved a skynet message to brake.  
	pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_BRAKE].mutex );
	my_vehicle.actuator[ACTUATOR_BRAKE].command_loop_counter ++;
	pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_BRAKE].mutex );

	//Do something with it.
	switch (my_command.my_command_type){
	case get_position:
	  my_return.my_actuator = brake;
	  my_return.my_return_type = return_position;
	  
	  pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_BRAKE].mutex );
	  my_return.number_arg = my_vehicle.actuator[ACTUATOR_BRAKE].position;
	  pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_BRAKE].mutex );
	  
	  Skynet.send_msg(socket_output, &my_return, sizeof(my_return), 0);
	  break;
	case get_pressure:
	  my_return.my_actuator = brake;
	  my_return.my_return_type = return_pressure;
	  
	  pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_BRAKE].mutex );
	  my_return.number_arg = my_vehicle.actuator[ACTUATOR_BRAKE].pressure;
	  pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_BRAKE].mutex );
	  
	  Skynet.send_msg(socket_output, &my_return, sizeof(my_return), 0);
	  break; 
	case set_position:
	  pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_BRAKE].mutex );
	  my_vehicle.actuator[ACTUATOR_BRAKE].command = my_command.number_arg; 
	  pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_BRAKE].mutex );
	  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_BRAKE].cond_flag );
	  break;
	}
	break;
	
	/* This is the case that should be used during the race.  It combines the accel and brake
	 * preventing commanding brake and accel to be both commanded at once.  */
      case accel:
	// record that we recieved a skynet message to brake and gas.  
	pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_BRAKE].mutex );
	my_vehicle.actuator[ACTUATOR_BRAKE].command_loop_counter ++;
	pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_BRAKE].mutex );
	pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_GAS].mutex );
	my_vehicle.actuator[ACTUATOR_GAS].command_loop_counter ++;
	pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_GAS].mutex );
	
	//Do something with it.
	switch (my_command.my_command_type){
	case set_position:
	  //Check the gas
	  pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_GAS].mutex );
	  if(my_command.number_arg >= 0 && my_command.number_arg <= 1)
	    // Check that the command is in the right range for gas and execute.  
	    {
	      my_vehicle.actuator[ACTUATOR_GAS].command = my_command.number_arg;
	    }
	  else //if (my_command.number_arg > -1 && my_command.number_arg <= 0)
	    // if it is outside the range set gas to zero.  
	    {
	      my_vehicle.actuator[ACTUATOR_GAS].command = 0;
	    }
	  pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_GAS].mutex );
	  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_GAS].cond_flag );


	  //The brake
	  pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_BRAKE].mutex );
	  if (my_command.number_arg > 0 && my_command.number_arg <= 1)
	    // If the 
	    {
	      my_vehicle.actuator[ACTUATOR_BRAKE].command = 0; 
	    }
	  else if (my_command.number_arg >= -1 && my_command.number_arg <= 0)
	    {
	      // Negate this when passed to the brake for it takes 0 to 1.
	      // This is so that the actuator takes 0 to 1 from the commanded 
	      // general accel. 
	      my_vehicle.actuator[ACTUATOR_BRAKE].command = -1 * my_command.number_arg; 
	    }
	  pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_BRAKE].mutex );
	  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_BRAKE].cond_flag );
	  break;

       	case get_position:
	  printf("You really shouldn't be asking for accel position");
	  break;
	}
	break;

      case estop:
	// Only one command which is to return condition. 
   	my_return.my_actuator = estop;
	my_return.my_return_type = return_position;

	pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_ESTOP].mutex);
	my_return.number_arg = my_vehicle.actuator[ACTUATOR_ESTOP].position;
	pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_ESTOP].mutex);
	break;
      case all:
	switch (my_command.my_command_type){
	case get_all:
	  for(int i = 0; i < NUM_ACTUATORS; i++)
	    {
	      pthread_mutex_lock(&my_vehicle.actuator[i].mutex );
	      Skynet.send_msg(socket_output2, &my_vehicle, sizeof(my_vehicle), 0);
	      pthread_mutex_unlock(&my_vehicle.actuator[i].mutex );
	    }
	  break;
	}
      }
    }
  
  pthread_exit(0); 
}


