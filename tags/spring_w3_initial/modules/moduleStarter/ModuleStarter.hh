/**
 * ModuleStarter.hh
 * Revision History:
 * Initially written by Thiago
 * 04/06/2005  hbarnor  modified to be OOP
 *                      also added functionality to read 
 *                      command path from config
 */

#ifndef  MODULESTARTER_HH
#define  MODULESTARTER_HH

#include "sn_msg.hh"
#include <string>
#include <cstdlib>
#include <iostream>
#include <list>
#include <sys/wait.h>

#define BUFSIZE 1024
//#define MAXPROCS 25
/**
 * A struct for the command 
 * sent over skynet
 * type - contains the SNmodname in string format
 * action - K means kill and S means Start the module
 * PID - used to hold the PID of the command when its 
 * spawned
 */
struct command
{
  char type[20];   //name of the command
  char action;     // the action to perform S is start and k is kill 
  pid_t PID;         //modules's PID if the module could be started or if it is already running. 
                   //It is 0 if module could not be started
  int compID;     // unique computer ID
};

/**
 * Struct to hold the 
 * tokens obtained from parsing the config
 * file
 */
struct cmdTokens
{
  char *snModName;
  char *cmd;
  char *opts;
};

#define CONFIG_FILE_NAME "ModuleStarter.config"

using namespace std;

#define DEBUG false

const char preModule[] = "Module:";

// variables
char* cmdFileName; // 
struct cmdTokens execCmd;
skynet* Skynet;
modulename myself = SNmodstart;                            //defining paramters for skynet
modulename other_mod = SNGuimodstart;
sn_msg myinput = SNmodcom;  
sn_msg myoutput = SNmodcom;
list<struct command> myModules;
//struct command myModules[MAXPROCS];
//int numModules = 0;

/**
 * listens for skynet command and spawns appropriate
 * calls spawn function with the snModuleName if the 
 * message is meant for us
 */
void listen();
/** 
 * spawnModule - calls getPath with the 
 * snModuleName, forks and execs the 
 * returned path
 */
void spawnModule(char* snModuleName);
/**
 * getPath - searches the config file 
 * for the path of the snModuleName 
 * passed to it as a param
 */
bool getPath(char* snModName);
/**
 * Read the config file 
 * and find out the id of the computer
 */
int verify_configuration(); 
/**
 * fill the exeCmd struct with 
 * the path to the cmd and options
 */
bool parse(char* line, char* snModName);
/**
 * read a line of text
 */
int getLine(char* line, int max, FILE *fp);
/**
 * spawn - forks and execs the commands in the updated 
 * execCmd struct
 */
pid_t spawn(char* snModuleName);

/**
 * stopModule - kills the module passed
 */
void stopModule(char* snModuleName);
#endif  
//MODULESTARTER_HH  
