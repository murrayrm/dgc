/* ModuleStarter.cc: Start up and kill modules*/

#include "ModuleStarter.hh"

char my_comp[50];
int my_comp_id;

int main(int argc, char** argv)
{   
  int sn_key;
  char* default_key = getenv("SKYNET_KEY");   //defining the skynet_key  
  gethostname(my_comp,50);
  my_comp_id =   verify_configuration();
  if (2 == argc)
    {
      sn_key = atoi(argv[1]);                 //either it is given as argument of main
    }
  else if (default_key != NULL )
    {
      sn_key = atoi(default_key);             //or it is the default key
    }
  else
    {
      printf("usage: sn_get_test key\nDefault key is $SKYNET_KEY\n\n");
      exit(2);
    }

  printf("Starting Skynet with SKYNET_KEY %s",default_key); 
  Skynet = new  skynet(myself,sn_key);
  //printf("my module id: %d\n\n", Skynet->get_id());
  listen();
  delete Skynet;
  return(0);
}

void listen()
  {
    int sock1 = Skynet->listen(myinput, other_mod);            //getting message via skynet
    while(1)
      {                                                   //server loop
	int CMDSIZE = sizeof(struct command);
	struct command cmd;
	Skynet->get_msg(sock1, &cmd, CMDSIZE, 0); 
	char* snModuleName = cmd.type;
	printf("received, %d, %s\n\n",my_comp_id,my_comp);
	
	if(cmd.compID == my_comp_id)
	  {
	    switch(cmd.action)
	      {	  
	      case 'K':
	      case 'k':
		//cout << "Kill Action Undefined" << endl;
		stopModule(snModuleName);
		break;
	      case 'S':
	      case 's':
		cmd.PID = spawn(snModuleName);
		myModules.push_back(cmd);
		break;
	      default:
		cout << "Unknown Action: " << cmd.action << endl;		
		break;
	      }	
	  }
	else
	  {
	    cout << " Not my module: " << cmd.type << endl;
	  }
      }
  }

pid_t spawn(char* snModuleName)
{
  pid_t pid;
  if((pid =fork()) < 0) 
    {
      perror("fork error");
    }
  else
    {
      if(pid == 0)
	{
	  spawnModule(snModuleName);
	}
    }
  return pid;
}

void spawnModule(char* snModuleName)
{
  if(getPath(snModuleName))
    { 
      cout << "Executing: " << execCmd.snModName <<" "  << execCmd.cmd << " " << execCmd.opts << endl;
      int status =execv(execCmd.cmd, &execCmd.opts);
      cout << "Status: " << status << endl << endl;
    }
  else
    {
      cout << "Command not found not found " << endl;
    }
}

int getLine(char* line, int max, FILE *fp)
{
  if(fgets(line,max, fp) == NULL)
    {
	   return 0;
	 }
	else
	 {
	   return strlen(line);
	 }
   }

 bool getPath(char* snModName)
   {
     FILE* fp; // file pointer 
     fp = fopen(CONFIG_FILE_NAME, "r"); //open file for reading     
     char line[BUFSIZE];
     while(getLine(line, BUFSIZE, fp) != 0)
       {
	 if(strncmp(preModule, line, 6) == 0)
	   {	     
	     if(parse(line, snModName))
	       {
		 return true;
	       }
	   }
       }
     fclose(fp);
     return false;
   }
     
bool parse(char* line, char* snModName)
{
  strtok(line," "); //ignore first token 
  execCmd.snModName = strtok(NULL, " ");  // get module name
  if(strcmp(execCmd.snModName, snModName) == 0 ) // update cmd string if correct module name
    {       
      execCmd.cmd = strtok(NULL, " ");
      execCmd.opts = strtok(NULL, " ");	 
      return true;
    }
  else  // reset module name if not correct
    {
      execCmd.snModName = NULL;
      return false;
    }
}

int verify_configuration()
{      //Read the config file and find out the id of the computer
  int i=-1;
  FILE* fp;
  int ID;
  char comp_name[50];
  fp = fopen(CONFIG_FILE_NAME,"r");
  while(EOF!=fscanf(fp,"%d",&ID))
    {
      fscanf(fp,"%s",comp_name);
      if(strcmp(comp_name,my_comp) == 0)
      i = ID;
    }
  fclose(fp);
  return i;
}

void stopModule(char* snModuleName)
{
  list<struct command>::iterator i;
  //pid_t killed;
  int found;
  for(i = myModules.begin(); i!=myModules.end(); ++i)
    {
      if(strcmp(i->type,snModuleName) == 0 )
	{
	  if(kill(i->PID, SIGKILL) == 0)
	    {
	      int status;
	      found = waitpid(i->PID, &status, 0); // clear status
	      cout << i->type << " Killed" << endl;
	    }
	  else
	    {
	      cerr << "Kill Failed " << endl;
	    }
	} 
    }
  if(!found)
    cerr << " Stop Failed: Module not found" << endl;
}
