#!/bin/sh -x

cd logs

rm -f state path path2 yerror vref vact verror

#state is the places we've been, path is the desired trajectory, path2 is the
# part of the trajectory that sn_TrajFollower has come closest to

paste state2.dat | awk '{print $1, $2}' > state
paste state2.dat| awk '{if($4 != 0)print $4, $5}' > path2
paste ../default.traj | awk '{print $1, $4}' > path

echo "plot 'path2', 'state' with dots" | gnuplot -persist

# plotting the perpendicular y-error as a function of some arbitrary time
# variable...

paste error.dat | awk '{print $NR,$1}' > yerror
#echo "plot 'yerror'" | gnuplot -persist

#plotting the desired vs actual velocities
paste velocity.dat | awk '{print $NR, $1}' > vref
paste velocity.dat | awk '{print $NR, $2}' > vact

echo "plot 'vref' with l, 'vact' with l" | gnuplot -persist

#plotting the velocity error (as an arbitrary function of time...)

paste velocity.dat | awk '{print $NR, $3}' > verror
#echo "plot 'verror' " | gnuplot -persist
