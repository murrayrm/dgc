/*! 
 * $Id$
 */
#include "TrajFollower.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

VehicleState dispSS;
double d_decimaltime;
double d_speed;

double PitchDeg, RollDeg, YawDeg;
double PitchRateDeg, RollRateDeg, YawRateDeg;

#ifdef LQR_CONTROLLER
int refIndex;
STrajCtrlState* pRefState;
STrajCtrlState* pErrState;
STrajCtrlState refState;
STrajCtrlState errState;
#endif

double* pK;
// TODO: Why is this necessary?
double K0, K1, K2, K3, K4;
double K5, K6, K7;

double yError, aError, cError;

double vRef, vTraj;
double accelFF, accelFB, accelCmd;
double steerFF, steerFB, steerCmd;

double dphiCmd;
double daccelCmd;

//double steerCommand;
double steerCommandRad;
double accelCommand;

int SparrowUpdateCount = 0;
int RecvTrajCount = 0;
int TrajIndex = 0;

#include "vddtable.h"
int user_quit(long arg);

void TrajFollower::UpdateSparrowVariablesLoop() 
{
  // Used to be !ShuttingDown()
  while( 1) 
  {
    dispSS = m_state;
    d_decimaltime = 0;
    d_speed = m_state.Speed2();

    PitchDeg     = m_state.Pitch     * 180.0 /M_PI;
    RollDeg      = m_state.Roll      * 180.0 /M_PI;
    YawDeg       = m_state.Yaw       * 180.0 /M_PI;
    PitchRateDeg = m_state.PitchRate * 180.0 /M_PI;
    RollRateDeg  = m_state.RollRate  * 180.0 /M_PI;
    YawRateDeg   = m_state.YawRate   * 180.0 /M_PI;

#warning "Replace this with steering angle from vdrive."
    double currphi = atan(VEHICLE_WHEELBASE * -m_state.YawRate / fmax( 0.1, m_state.Speed2()));
    
#ifdef LQR_CONTROLLER
    /* Get a pointer to the feedback matrix */
    pK = m_pPIDcontroller->getLastFeedbackMatrix();

    /* Get the index of the reference point */
    refIndex = m_pPIDcontroller->getLastDesiredStateIndex();

    /* Get the reference and error states */
    pRefState = m_pPIDcontroller->getLastDesiredState();
    pErrState = m_pPIDcontroller->getLastErrorState();
		memcpy(&refState, pRefState, sizeof(refState));
		memcpy(&errState, pErrState, sizeof(errState));

		if(pK != NULL)
		{
			K0 = pK[0]; K1 = pK[1]; K2 = pK[2]; K3 = pK[3]; K4 = pK[4];
			K5 = pK[5]; K6 = pK[6]; K7 = pK[7];
		}

    /* TODO: Get the error state from the controller rather than here
     * so that you are debugging the controller rather than Sparrow */    
//--------------------------------------------------
//     cout << "pK[0] = " << pK[0] << endl;
//     cout << "pK[1] = " << pK[1] << endl;
//     cout << "pK[2] = " << pK[2] << endl;
//     cout << "pK[3] = " << pK[3] << endl;
//     cout << "pK[4] = " << pK[4] << endl;
//     cout << "pK[5] = " << pK[5] << endl;
//     cout << "pK[6] = " << pK[6] << endl;
//     cout << "pK[7] = " << pK[7] << endl;
//     cout << "pK[8] = " << pK[8] << endl;
//     cout << "pK[9] = " << pK[9] << endl << endl;
//-------------------------------------------------- 
#endif
    dphiCmd = m_steer_cmd;
    daccelCmd = m_accel_cmd;

    yError = m_pPIDcontroller->access_YError();
    aError = m_pPIDcontroller->access_AError();
    cError = m_pPIDcontroller->access_CError();

    vRef = m_pPIDcontroller->access_VRef();
    vTraj = m_pPIDcontroller->access_VTraj();
    
    accelFF = m_pPIDcontroller->access_AccelFF();
    accelFB = m_pPIDcontroller->access_AccelFB();
    accelCmd = m_pPIDcontroller->access_AccelCmd();

    steerFF = m_pPIDcontroller->access_SteerFF();
    steerFB = m_pPIDcontroller->access_SteerFB();
    steerCmd = m_pPIDcontroller->access_SteerCmd();

    RecvTrajCount = m_trajCount;

#warning "This needs to be changed to reflect radians."
    //steerCommand = toVDrive.steer_cmd; 
    steerCommandRad = m_steer_cmd; 
    accelCommand = m_accel_cmd;

    SparrowUpdateCount ++;

    // Wait a bit, because other threads will print some stuff out
    usleep(500000); 
  }
}

void TrajFollower::SparrowDisplayLoop() 
{
  dbg_all = 0;

  if (dd_open() < 0) exit(1);
  dd_bindkey('q', user_quit);

  dd_usetbl(vddtable);

  usleep(250000); // Wait a bit, because other threads will print some stuff out
  dd_loop();
  dd_close();

}

// TODO: Set the shutdown flag in the quit function
int user_quit(long arg)
{
  return DD_EXIT_LOOP;
}
