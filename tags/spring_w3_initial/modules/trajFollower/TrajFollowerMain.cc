#include "TrajFollower.hh"

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>

#include "DGCutils"
#include "sn_msg.hh"

#define SLEEP_OPT          1
#define LOG_OPT            2
#define TRAJ_OPT           3

int           NOSTATE     = 0;       // Don't update state if = 1.
int           NODISPLAY   = 0;       // Don't start Sparrow display interface.
int           USEMODEMAN  = 0;       // Use ModeManModule
int           SIM  = 0;              // Don't send commands to adrive.
unsigned long SLEEP_TIME = 100000;   // How long in us to sleep in active fn.
int           LATERAL_FF_OFF = 0;    // Turn *OFF* the *LATERAL FEED FORWARD* term
                                     // (i.e. ignore its contribution) IF this is
                                     // SET (i.e. = 1)
int           A_HEADING_REAR = 0;    // Calculate the *ANGULAR ERROR* from the
                                     // heading of the *REAR* axle.
int           A_HEADING_FRONT = 0;   // Calculate the *ANGULAR ERROR* from the
                                     // heading of the *FRONT* axle.
int           A_YAW = 0;             // Calculate the *ANGULAR ERROR* from the
                                     // yaw
int           Y_REAR = 0;            // Calculate the *Y-ERROR* from the
                                     // position of the *REAR* axle
int           Y_FRONT = 0;           // Calculate the *Y-ERROR* from the
                                     // position of the *FRONT* axle

/* The name of this program. */
const char * program_name;
char* logNamePrefix = NULL;
char* loadFilename = strdup("default.traj");

/* Prints usage information for this program to STREAM (typically stdout
   or stderr), and exit the program with EXIT_CODE. Does not return. */
void print_usage (FILE* stream, int exit_code)
{
  fprintf( stream, "Usage:  %s [options]\n", program_name );
  fprintf( stream,
           "  --nostate         Continue even if state data is not received.\n"
           "  --nodisp          Don't start the Sparrow display interface.\n"
           "  --mm              Use ModeManModule.\n"
           "  --sim             Don't send commands to VDrive.\n"
           "  --sleep us        Specifies us sleep time in Active function.\n"
           "  --traj filename   Use filename as the trajectory to follow.\n"
           "  --latffoff        Turn the Lateral feed-forward OFF (ignore its contribution).\n"
					 "  --headingfront    Use the centre of the FRONT axle as the reference for ANGULAR-ERROR.\n"
           "  --headingrear     Use the centre of the REAR axle as the reference for ANGULAR-ERROR.\n"
					 "  --yaw             Use the yaw for ANGULAR-ERROR. (default)\n"
           "  --yerrorfront     Use the center of the REAR axle as the reference for Y-ERROR. (default)\n"
           "  --yerrorrear      Use the center of the REAR axle as the reference for Y-ERROR\n"
           "  --help, -h        Display this message.\n" );
  exit(exit_code);
}

int main(int argc, char **argv) 
{
  /* Set the default arguments that won't need external access here. */

  /* Temporary character. */
  int ch;
  /* A string listing valid short options letters. */
  const char* const short_options = "h";
  /* An array describing valid long options. */
  static struct option long_options[] = 
	{
		// first: long option (--option) string
			// second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
			// third: if pointer, set variable to value of fourth argument
			//        if NULL, getopt_long returns fourth argument
			{"nostate",     0, &NOSTATE,           1},
			{"nodisp",      0, &NODISPLAY,         1},
			{"mm",          0, &USEMODEMAN,        1},
			{"sim",         0, &SIM,               1},
			{"sleep",       1, NULL,               SLEEP_OPT},
			{"traj",        1, NULL,               TRAJ_OPT},
			{"headingfront",0, &A_HEADING_FRONT,   1},
      {"headingrear", 0, &A_HEADING_REAR,    1},
			{"yaw",         0, &A_YAW,             1},
      {"yerrorfront", 0, &Y_FRONT,           1},
      {"yerrorrear",  0, &Y_REAR,            1},
			{"latffoff",    0, &LATERAL_FF_OFF,    1},
			{"help",        0, NULL,               'h'},
			{"log",         1, NULL,               LOG_OPT},
			{NULL,          0, NULL,               0}
		};


	/* Remember the name of the program, to incorporate in messages.
		 The name is stored in argv[0]. */
	program_name = argv[0];
	printf("\n");

	// Loop through and process all of the command-line input options.
	while((ch = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
	{
		switch(ch)
		{
		case TRAJ_OPT:
			loadFilename = strdup(optarg);
			break;

		case LOG_OPT:
			logNamePrefix = strdup(optarg);
			break;

		case SLEEP_OPT:
			SLEEP_TIME = (unsigned long) atol(optarg);
			break;
      
		case 'h':
			/* User has requested usage information. Print it to standard
				 output, and exit with exit code zero (normal 
				 termination). */
			print_usage(stdout, 0);

		case '?': /* The user specified an invalid option. */
			/* Print usage information to standard error, and exit with exit
				 code one (indicating abnormal termination). */
			print_usage(stderr, 1);

		case -1: /* Done with options. */
			break;
		}
	}

	// finish processing the command line
	if(A_HEADING_FRONT == 0 &&
		 A_HEADING_REAR  == 0 &&
		 A_YAW           == 0)
	{
		A_YAW = 1;
	}
	if(Y_FRONT == 0 &&
		 Y_REAR  == 0)
	{
		Y_FRONT = 1;
	}

	if(A_HEADING_FRONT + A_HEADING_REAR + A_YAW != 1)
	{
		cerr << "choose ONLY ONE setting for the computation of the angular error" << endl;
		return 1;
	}
	if(Y_FRONT + Y_REAR != 1)
	{
		cerr << "choose ONLY ONE setting for the computation of the y error" << endl;
		return 1;
	}

	int sn_key = 0;
	char* pSkynetkey = getenv("SKYNET_KEY");
	if( pSkynetkey == NULL )
	{
		cerr << "SKYNET_KEY environment variable isn't set" << endl;
	}
	else
		sn_key = atoi(pSkynetkey);
	cerr << "Constructing skynet with KEY = " << sn_key << endl;
	TrajFollower trajFollower(sn_key);
	cerr << "Done constructing TrajFollower" << endl;

	if( !NODISPLAY )
	{
		cout << "Starting sparrow display loops" << endl;
		DGCstartMemberFunctionThread(&trajFollower, &TrajFollower::SparrowDisplayLoop);
		DGCstartMemberFunctionThread(&trajFollower, &TrajFollower::UpdateSparrowVariablesLoop);
	}

	DGCstartMemberFunctionThread(&trajFollower, &TrajFollower::Comm);
	trajFollower.Active();

	return 0;
} // end main()
