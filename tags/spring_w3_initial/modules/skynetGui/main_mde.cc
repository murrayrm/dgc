
#include <time.h>
#include <unistd.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>  


#include "MapDisplay.hh"
// vehicle-independent state struct location.
#include "VehicleState.hh"
// Include the Map class here.
#include "CMap/CMapPlus.hh"

VehicleState SS;

bool within_waypoint(const RDDFData & waypoint, const UTM_POINT & vehicle_pos)
{
  double dx = waypoint.Easting - vehicle_pos.E;
  double dy = waypoint.Northing - vehicle_pos.N;

  if( pow(dx*dx + dy*dy, 0.5) < waypoint.radius ) return true;
  else return false; 
}

UTM_POINT update_pos( const UTM_POINT & car_pos, const UTM_POINT & dest )
{
  double dx = dest.E - car_pos.E;
  double dy = dest.N - car_pos.N;

  double theta = atan2( dy, dx );

  SS.Yaw = M_PI/2.0 - theta;
  // For some reason, the RDDF won't update if I change this(?) 
  // Is it because I used calls to random()?
  return car_pos + UTM_POINT( 0.3* cos(theta ), 0.3 * sin(theta ));
}

CTraj list_UTM_POINT_to_CTraj( list<UTM_POINT> & history )
{



}


int main () 
{
  // get a state struct 
  SS.Northing = SS.Easting = 0;

  // Initialize the Map class that we will use
  CMap my_cmap;

  my_cmap.initMap(0,0, 200, 200, 0.5, 0.5, 0);
  // Add a layer to the map
  int ladarID = my_cmap.addLayer<double>(-11, -12);

  // The map display thread class
  MapDisplayThread * mdt;

  // A path we will use to keep track of the vehicle position
  PATH path_history;
  PATH planned_path;

  // Read in the RDDF File
  RDDF race_rddf("rddf.dat"); 
  RDDFVector waypoints = race_rddf.getTargetPoints();

  // MAP DISPLAY STUFF ----------------------------------
  {
    // Initialize the MapDisplayThread with a new MapConfig Struct
    mdt = new MapDisplayThread( "Copy of MapDisplayExample", new MapConfig );

    // Pass it the CMap and state struct
    mdt->get_map_config()->set_cmap( & my_cmap );
    mdt->get_map_config()->set_state_struct( & SS );

    // and start the thread
    mdt->start_thread();
    
    mdt->get_map_config()->set_rddf( waypoints );
    cout << "Grabbed " << waypoints.size() << " waypoints." << endl;
    
    
    // Set the color for our path history to red.
    path_history.color = RGBA_COLOR(1.0, 0.0, 0.0); // red
    planned_path.color = RGBA_COLOR(0.0, 1.0, 0.0); // green
  }

  if (waypoints.size() < 2 )
    {
      cout << "Not enough waypoints" << endl;
      return 0;
    } 
    
  RDDFVector::iterator x = waypoints.begin();
  RDDFVector::iterator stop = waypoints.end();
  RDDFVector::iterator y;

  SS.Easting  = x->Easting;
  SS.Northing = x->Northing;

  for( ; x != stop; x++ )
    {
      UTM_POINT car_utm(SS.Easting, SS.Northing);

      while( ! within_waypoint( *x, car_utm ) )
	{
	  int i;
	  for( i=0; i<2 && !within_waypoint(*x, car_utm ); i++ )
	    {
	      //cout << "Heading for " << x->utm.E << ", " << x->utm.N << endl;

	      // head towards the next waypoint
	      car_utm = update_pos( car_utm , UTM_POINT( x->Easting, x->Northing) );
	      SS.Easting  = car_utm.E;
	      SS.Northing = car_utm.N;
	    }

	  // Add our current position to the path history	  
	  // If our history has too many points in it, we need to shift them and add
	  int num_points;
	  if( num_points = path_history.points.getNumPoints() > TRAJ_MAX_LEN )
	    {
	      double *p_N = path_history.points.getNdiffarray(0);
	      double *p_E = path_history.points.getEdiffarray(0);
	      
	      for(i=0; i<num_points-1; i++)
		{
		  p_N[i] = p_N[i+1];
		  p_E[i] = p_E[i+1];		  
		}
	      p_N[i] = car_utm.N;
	      p_E[i] = car_utm.E;
	    }
	  else
	    {
	      // else just use the method to add it
	      path_history.points.inputNoDiffs( car_utm.N, car_utm.E );
	    }

	  // create a "planned" path which simply includes the next couple of waypoints
	  planned_path.points.startDataInput();
	  planned_path.points.inputNoDiffs( car_utm.N, car_utm.E );
	  
	  for( i=0, y=x; i<5; i++, y++ ) 
	    planned_path.points.inputNoDiffs( y->Northing, y->Easting );

	  list<PATH> paths; 
	  paths.push_back(path_history);
	  paths.push_back(planned_path);

	  mdt->get_map_config()->set_paths( paths );

	  // Update the CMap center
	  my_cmap.updateVehicleLoc( SS.Northing, SS.Easting );


	  // draw some random data on the map
	  double scale = 100.0 / (double) RAND_MAX;
	  double theta_scale = mdt->get_map_config()->m_sensor_angle / (double) RAND_MAX;
	  double theta_mid = mdt->get_map_config()->m_sensor_angle / 2.0;
	  for(i = 1; i<50; i++)
	    {
	      double theta = M_PI/2.0 - SS.Yaw + theta_scale * random() - theta_mid;
	      double radius = scale * random();

	      my_cmap.setDataWin<double>( ladarID, (int) (100 + radius*sin(theta) ), (int)( 100 + radius*cos(theta) ),  radius );
	    }

	  mdt->notify_update();
	  usleep(20000);
	}
      static int ctr= 0;
      cout << "Reached waypoint " << ctr++ << endl;
    }
  
  



  return 0;

};





