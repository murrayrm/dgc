/**
 * MapDisplaySN.cc
 * Revision History:
 * 02/05/2005  hbarnor  Created
 */


#include "MapDisplaySN.hh"

#include <unistd.h>
#include <iostream>

// define external variables
extern int           NOSTATE;

#define MAX_DELTA_SIZE	10000000
#define THREAD_INTERVAL 50000

//#define USE_STEREO
//#define USE_LADAR

MapDisplaySN::MapDisplaySN(MapConfig * mapConfig, int sn_key)
  : CSkynetContainer(SNMapDisplay,sn_key), m_mapConfig(mapConfig)
{
  m_mapConfig->set_state_struct(&m_state);
  RDDFData& x = mapConfig->get_rddf_data(0);
  //d.myCMap.initMap(x.Northing,x.Easting,150,150,1,1,0);
  d.myCMap.initMap(0,0,150,150,1,1,0);
  d.layerNum[SNGUI_LAYER_FUSIONMAP_001] = d.myCMap.addLayer<double>(0, -1);
  m_mapTypes[SNGUI_LAYER_FUSIONMAP_001] = SNfusiondeltamap;
  m_mapSources[SNGUI_LAYER_FUSIONMAP_001] = SNfusionmapper;
  d.layerNum[SNGUI_LAYER_STEREO_001] = d.myCMap.addLayer<double>(0, -1);
  m_mapTypes[SNGUI_LAYER_STEREO_001] = SNstereodeltamap;
  m_mapSources[SNGUI_LAYER_STEREO_001] = MODstereofeeder;
  d.layerNum[SNGUI_LAYER_LADAR_001] = d.myCMap.addLayer<double>(0, -1);
  m_mapTypes[SNGUI_LAYER_LADAR_001] = SNladardeltamap;
  m_mapSources[SNGUI_LAYER_LADAR_001] = SNladar;      
  d.layerNum[SNGUI_LAYER_STATIC_001] = d.myCMap.addLayer<double>(0, -1);
  m_mapTypes[SNGUI_LAYER_STATIC_001] = SNstaticdeltamap;
  m_mapSources[SNGUI_LAYER_STATIC_001] = MODstaticpainter;

  DGCcreateMutex(&m_mapMutex);

  m_mapConfig->set_cmap(&d.myCMap);



	static const sn_msg pathTypes[] =
		{SNtraj, SNtrajPlannerSeed, SNtrajPlannerInterm, SNtraj};
	static const modulename pathSources[] =
		{ALLMODULES,SNplanner,SNplanner,SNplanner};
	static const RGBA_COLOR pathColors[] = 
		{RGBA_COLOR(1.0, 0.0, 0.0),
		 RGBA_COLOR(0.0, 0.5, 0.0),
		 RGBA_COLOR(0.0, 1.0, 1.0),
		 RGBA_COLOR(1.0, 0.0, 1.0)};

	memcpy(m_pathTypes, pathTypes, sizeof(m_pathTypes));
	memcpy(m_pathSources, pathSources, sizeof(m_pathSources));
	memcpy(m_pathColors, pathColors, sizeof(m_pathColors));

  // initialize paths
  // Set the color for our path history to red.
	for(int i=0; i<PATHIDX_NUMPATHS; i++)
		paths[i].color = m_pathColors[i];

  // trajectory
  m_pMapDelta = new char[MAX_DELTA_SIZE];

	// set the paths
	m_mapConfig->set_paths( paths, PATHIDX_NUMPATHS);
}

MapDisplaySN::~MapDisplaySN()
{
	delete m_pMapDelta;
}

void MapDisplaySN::init()
{
  if(!NOSTATE)
    {
      printf("[%s:%d] Calling UpdateState() in init()... ", __FILE__, __LINE__);
      UpdateState(); // update the state
      printf(" ...done.");
      
      //do error checking 
      if(m_state.Northing == 0)   
	{
	  printf("\nUnable to communicate with vstate (Northing is zero).\n");
	  printf("Run with --nostate option if this is OK. (Exiting...)\n\n");
	  exit(-1);
	}
      else
	{
	  // center map display on vehicle
	  d.myCMap.updateVehicleLoc(m_state.Northing,m_state.Easting);
	}
      cout << "Skynet Gui: MapDisplay Module Init Finished" << endl;
    }    
}

// This thread reads in the map deltas as they come in. The display is only
// notified that something's changed in the getStateThread function
void MapDisplaySN::getMapDeltaThread(void* pArg)
{
  ofstream delta_log("logs/delta.log");
  delta_log << "# skynetgui produced this.  Time then size." << endl;
  unsigned long long now;
  
  int layerIndex = (int)pArg;
  
  sn_msg mapType = m_mapTypes[layerIndex];
  modulename mapSource = m_mapSources[layerIndex];
  
    int mapDeltaSocket = m_skynet.listen(mapType, mapSource);
  
  if(mapDeltaSocket < 0)
    cerr << "MapDisplaySN::getMapDeltasThread(): skynet listen returned error" << endl;
  
  int mapDeltaLeft;
  
  cerr << "MapDisplaySN::getMapDeltasThread(): running" << endl;
  while(true)
    {
      int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pMapDelta, MAX_DELTA_SIZE, 0);		  
      if(numreceived>0)
	{
	  DGClockMutex(&m_mapMutex);
	  d.myCMap.applyDelta<double>(d.layerNum[layerIndex], m_pMapDelta, numreceived);
	  DGCunlockMutex(&m_mapMutex);
	  DGCgettime(now);
	  delta_log << now << " " << numreceived << endl;
	}
    }		
}

// This thread reads in paths as they come in. The display is only notified that
// something's changed in the getStateThread function
void MapDisplaySN::getTrajThread(void* pArg)
{
	int pathIndex = (int)pArg;
	sn_msg pathType = m_pathTypes[pathIndex];
	modulename pathSource = m_pathSources[pathIndex];

  int trajSocket = m_skynet.listen(pathType, pathSource);

  while(true)
	{
		RecvTraj(trajSocket, &paths[pathIndex].points);
	}
}

void MapDisplaySN::getStateThread(void)
{
  while(true)
	{
		/* This sets m_state */
		UpdateState();

		// Add our current position to the path history	  
		// If our history has too many points in it, we need to shift them and add
		int num_points;
		if( (num_points = paths[PATHIDX_HISTORY].points.getNumPoints()) > TRAJ_MAX_LEN-2 )
		{
			paths[PATHIDX_HISTORY].points.shiftNoDiffs(1);
		}
		paths[PATHIDX_HISTORY].points.inputNoDiffs(m_state.Northing, m_state.Easting);  

		m_mapConfig->notify_update();      
		usleep(THREAD_INTERVAL);
	}
}
