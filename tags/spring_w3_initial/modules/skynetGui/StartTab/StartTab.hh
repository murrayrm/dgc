/**
 * StartTab.h
 * Revision History:
 * 02/17/2005  hbarnor  Created
 */

#ifndef STARTTAB_HH
#define STARTTAB_HH

#include <gtkmm/box.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/button.h>
#include <gtkmm/frame.h>
#include <gtkmm/table.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/table.h>
#include <string>
#include <iostream>
#include <stdio.h>
#include <fstream>

#include "sn_msg.hh"


using namespace std;

#define DEBUG false

/**
 * StartTab class.
 * $Id$ 
 */

static string _module = "Module:";
static int compArray[] = {1,2,3,4,5};
static int maxNumComputers = 5;

// Forward declare struct
struct command
{
  char type[20];   //name of the command
  char action;     // the action to perform S is start and k is kill 
  pid_t PID;         //modules's PID if the module could be started or if it is already running. 
                   //It is 0 if module could not be started
  int compID;     // unique computer ID
};

struct ButtonStatus
{
  Gtk::CheckButton * btn;
  struct command cmd;
};

class StartTab : public Gtk::HBox
{

public:
  /**
   * Default constructor.
   */
  StartTab();
  /**
   * Constructor with skynet key .
   */
  StartTab(int sn_key);
  /**
   * StartTab Destructor - for taking care of dynamically 
   * allocated memory. 
   */
  ~StartTab();
  /**
   * addModule - adds a checkbox with the name of the module passed
   *
   */
  void addModule(string snModName, bool start);
  /**
   * TODO
   */
  void updateConfig(string fileName, string snModName, bool start);
  /**
   * TODO
   */
  void readFile(string fileName);
private:
  /**
   * int value for next computer the 
   * the module is going to e run 
   * on 
   */
  int currComp;

  /**
   * TODO 
   *
   */
  Gtk::CheckButton * newCBtn;
  /**
   * number of checkboxes/modules
   */
  int numModules;
  int currentRow;
  const int maxCols; 
  /**
   * Temporay to keep track of the buttons
   * till i figure out TableLists
   */
  list< struct ButtonStatus> * myButtons; 
  /**
   * initialize skynet
   *
   */
  void initSN();
  /**
   * TODO
   *
   */

  void executeSkynet(char action);
  /**
   * nxm grid to hold module check boxes
   */
  Gtk::Table chkBxTbl;
  /*
   * buttonBox - holds the start and stop buttons
   */
  Gtk::HButtonBox buttonBox;
  Gtk::Button moduleSBtn; // module start button
  Gtk::Button moduleKBtn; // module kill button
  Gtk::Frame btnFrm; // frame to hold buttons
  Gtk::Frame chkBxFrm; // frame to hold check boxes
  Gtk::VBox m_VBox;
  string myConfigFile;
  // various buttons

  /**
  Gtk::Button modManSBtn; // mode man start button
  Gtk::Button modManKBtn; // mode man kill button
  Gtk::Frame modManFrm;  // Frame to hold buttons
  Gtk::Button trajFollowerSBtn; // TrajFollower start button
  Gtk::Button trajFollowerKBtn; // TrajFollower kill button
  Gtk::Frame trajFollowerFrm; // frame to hold traj buttons
  Gtk::Button rddfGenSBtn; // Rddf Gen start button
  Gtk::Button rddfGenKBtn; // rddf gen kill button
  Gtk::Frame rddfGenFrm; // frame to hold the rddf buttons
  Gtk::Button asimSBtn; // asim start button
  Gtk::Button asimKBtn; // asim kill button
  Gtk::Frame asimFrm; // frame to hold rddf buttons
  */
  /**
   * a skynet client
   */
  skynet m_skynet;  
  /*
   * ModeMan Start action
   */
  void startModules();
  /*
   * modeman exit action 
   */
  void killModules();
  /**
   * Skynet send socket
   */
  int cmdSendSocket;
};

#endif
