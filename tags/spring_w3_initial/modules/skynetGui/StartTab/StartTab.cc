/**
 * StartTab.cc
 * Revision History:
 * 02/19/2005  hbarnor  Created
 */

#include "StartTab.hh"
//#include "ModuleStarter.hh"


StartTab::StartTab(int sn_key)
  : maxCols(3), 
    chkBxTbl(6,3,false),
    buttonBox(Gtk::BUTTONBOX_SPREAD), 
    moduleSBtn("Start Modules"),
    moduleKBtn("Exit Modules"),
    btnFrm("Start/Stop"),
    chkBxFrm("Module Management"),
    m_skynet(SNGuimodstart, sn_key)
{
  numModules = 0;
  currentRow = 0;
  currComp = 0;
  myButtons = new list< struct ButtonStatus >();
  initSN();
  add(m_VBox);
  chkBxFrm.set_shadow_type(Gtk::SHADOW_ETCHED_IN);  
  chkBxFrm.add(chkBxTbl);
  m_VBox.pack_start(chkBxFrm);
  btnFrm.set_shadow_type(Gtk::SHADOW_ETCHED_IN);
  btnFrm.add(buttonBox);
  buttonBox.add(moduleSBtn);
  buttonBox.add(moduleKBtn);
  moduleSBtn.signal_clicked().connect(sigc::mem_fun(this,&StartTab::startModules));
  moduleKBtn.signal_clicked().connect(sigc::mem_fun(this,&StartTab::killModules));
  m_VBox.pack_end(btnFrm); 
  show_all_children();
}

StartTab::~StartTab()
{
  // nothing here yet
}

void StartTab::initSN()
{
  cmdSendSocket = m_skynet.get_send_sock(SNmodcom);
  if(cmdSendSocket< 0)
    {
      cerr << "StartTab::initSN: skynet get send returned error" << endl;
    }  
}



void StartTab::startModules()
{

  executeSkynet('s');
  /**  struct command cmd;
  strcpy(cmd.type,"SNModeMan");
  cmd.action='s';
  cmd.PID = 0;
  //cmd.compID = m_skynet.get_id();
  cmd.compID = 5;
  if(m_skynet.send_msg(cmdSendSocket,&cmd,sizeof(struct command),0) != sizeof(struct command))
    {
      cerr << "ModeMan not started. Invalid command length" << endl;
      }*/
}

void StartTab::executeSkynet(char action)
{
#warning "Need to figure out how to use TableList"
  /**
   * Gtk::Table_Helpers::TableList& tl = chkBxTbl.children();
   *for (Gtk::Table_Helpers::TableList::iterator cur = tl.begin(); cur != tl.end(); cur++)
   * {
   * }
   */
  for(list<struct ButtonStatus>::iterator iter = myButtons->begin(); iter != myButtons->end(); iter++)
    {
      if((*iter).btn->get_active() && (*iter).cmd.action != action)
	{
	  (*iter).cmd.action = action;  // set the action to perform 
	  //(*iter).cmd.compID = compArray[currComp];	
	  cout << "Sending Command: " <<  (*iter).cmd.type << " Action: " <<  (*iter).cmd.action << " Computer: " <<  (*iter).cmd.compID << endl;
	  if(m_skynet.send_msg(cmdSendSocket,&(*iter).cmd,sizeof(struct command),0) != sizeof(struct command))
	    {
	      if(action == 's')
		{
		  cerr << (*iter).btn->get_label() << " not started. Invalid command length" << endl;
		}
	      else
		{
		  cerr << (*iter).btn->get_label() << " not killed. Invalid command length" << endl;
		}
	    }
	}
      else
	{
	  if(action == 's')
	    {
	      cerr << (*iter).btn->get_label() << " Is not Checked or already started" << endl;
	    }
	  else
	    {
	      cerr << (*iter).btn->get_label() << " Is not Checked or already killed" << endl;
	    }
	}
    }
}


void StartTab::killModules()
{
  executeSkynet('k');
}

void StartTab::addModule(string snModName, bool start)
{
  cout << "AddModule " << snModName << " " << start << endl;
  newCBtn = manage(new Gtk::CheckButton(snModName));
  newCBtn->set_active(start); // check and uncheck
  int position = numModules%maxCols;
  chkBxTbl.attach(*newCBtn, position,position+1, currentRow, currentRow+1, Gtk::SHRINK);
  struct ButtonStatus newBtnStatus;
  newBtnStatus.btn = newCBtn;
  strcpy(newBtnStatus.cmd.type, snModName.c_str());
  newBtnStatus.cmd.action='u'; //mark as unused
  newBtnStatus.cmd.compID = compArray[currComp]; // give it a computer to run on
  currComp = (currComp+1)%(maxNumComputers-1); // update for next computer
  myButtons->push_back(newBtnStatus);
  cout << "Placed at " << position << ","<< position+1 << " : " <<  currentRow << ","<< currentRow+1 << endl; 
  numModules++;
  if(numModules%maxCols == 0)
    {
      currentRow++;
    }
  chkBxTbl.show_all_children();
}


void StartTab::readFile(string fileName)
{
  // read in config file passed in as param
  // format of config file will be 
  // first line of config file is a number 
  // specifying the number of modules 
  // read that and call the function  
  // setMaxModule(int n)
  // Moule:snModName 0/1 
  // snModName is the synet module name
  // 0/1 - means either a 0 or a 1 
  // where 1 means start it up automatically and
  // 0 means show it with a non-checked checkbox 
  // but don't start it up 
  // for each line in the config file call the function
  // addModule(String snModName, bool start)

  //char buffer[256];
  //int numLines = 0;
  string snModName, line;
  unsigned int pos, i;
  bool start;

  ifstream configfile(fileName.c_str() );
    if (! configfile.is_open())
      { cout << "Error opening file"; exit (1); }
    while (! configfile.eof() )
      {
	//cout << "\nreading line..." << endl;
	getline(configfile, line);
	if(! line.empty() )
	  if(line[0] != '#') // # at beginning of line denotes comment
	    {	    
	      //parse string
	      if( (pos = line.find(" ")) != string::npos)
		{
		  if(line.substr(0,pos) == _module)
		    {
		      //module listing
		      line.erase(0,pos);
		      for(i = 0;line[i] == ' ';++i);//elim whitepsace
		      line.erase(0,i);
		      //cout << "module listing length: " << pos << endl;
		      //cout << "line is now: " << line << endl;
		      if( (pos = line.find(" ")) != string::npos)
			{
			  snModName = line.substr(0,pos); //module name
			  line.erase(0,pos);
			  for(i = 0;line[i] == ' ';++i);//elim whitepsace
			  line.erase(0,i);
			  //cout << "module name length: " << pos << endl;
			  //cout << "line is now: " << line << endl;
			  switch (line[0])
			    {
			    case '0':
			      start = 0;
			      break;
			    case '1':
			      start = 1;
			      break;
			    default:
			      //cout << "invalid boolean specifier: " << line[0] <<  endl;
			      //cout << "in line: " << line << endl;
			      exit (1);
			    }
			  addModule(snModName, start); //add module to skynetGUI
			}
		    }
		}
	    }
      }
    configfile.close();
}

void StartTab::updateConfig(const string fileName, const string snModName, const bool start)
{
  // updates the config file start-up value
  string s;  //input string read from file
  char start_char = (start ? 1 : 0) + '0';
  fstream configfile(fileName.c_str(), fstream::in | fstream::out);

  if (! configfile.is_open() )
    { cout << "Error opening file"; exit (1); }
  while (! configfile.eof() )
    {
      configfile >> s;
      if(s[0] != '#')
	{
	  if(s == _module) // module listing
	    {
	      configfile >> s;
	      if (s == snModName)
		{
		  cout << "module name: " << s << endl;
		  cout << "snModName: " << snModName << endl;
		  configfile.get();
		  configfile.put(start_char);
		}
	      else
		{
		  getline(configfile,s);
		  cout << "Instead of module name we have: " << s << endl;
		  cout << "string in: " << s << endl;
		}
	    }
	  else
	    {
	      cout << "Instead of Module: we have: " << s << endl;
	      //get rid of the rest of the line
	      getline(configfile, s);
	      cout << "string out: " << s << endl;
	    }
	}
      else
	{
	  getline(configfile, s); //clear commented line
	  cout << "commented line... " << s << endl;
	}
    }
  configfile.close();  
}

