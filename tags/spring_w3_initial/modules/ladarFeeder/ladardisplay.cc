#include "LadarFeeder.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

#include <iostream>
using namespace std;

int UpdateCount   = 0;



double rangevals[20];
double myladarx[20];
double myladary[20];
double myladarz[20];
int mynumscans;

double myx;
double myy;
double myz;

double mypitch;
double myroll;
double myyaw;

int mysent;

#include "vddtable.h"

void LadarFeeder::UpdateSparrowVariablesLoop() {
  while(true) {
    for(int k=0; k<20; k++) {
      myladarx[k]=ladarx[k];
      myladary[k]=ladary[k];
      myladarz[k]=ladarz[k];
    }
    
    mynumscans = numscans;
    //printf("num %d\n", mynumscans);
    
    double roofLADARX=myx;
    double roofLADARY=myy;
    double roofLADARZ=myz;
    double roofLADARPitch=mypitch;
    double roofLADARRoll=myroll;
    double roofLADARYaw=myyaw;

      XYZcoord roofLADAROffset(roofLADARX, roofLADARY, 
			       roofLADARZ);

  ladarFrame.initFrames(roofLADAROffset, 
			    roofLADARPitch,
			    roofLADARRoll,
			    roofLADARYaw);

  mysent= sentnum;

    UpdateCount++;
  }
}

void LadarFeeder::SparrowDisplayLoop() 
{
  dbg_all = 0;

  if (dd_open() < 0) exit(1);

  /*
  myx=X_ROOF_LADAR;
  myy=Y_ROOF_LADAR;
  myz=Z_ROOF_LADAR;
  mypitch=PITCH_ROOF_LADAR;
  myroll=ROLL_ROOF_LADAR;
  myyaw=YAW_ROOF_LADAR;
  */

  myx=X_BUMPER_LADAR;
  myy=Y_BUMPER_LADAR;
  myz=Z_BUMPER_LADAR;
  mypitch=PITCH_BUMPER_LADAR;
  myroll=ROLL_BUMPER_LADAR;
  myyaw=YAW_BUMPER_LADAR;

  //dd_bindkey('Q', user_quit);
  //dd_bindkey('R', reset_simulation);

  dd_usetbl(vddtable);

  usleep(500000); // Wait a bit, because other threads will print some stuff out
  dd_loop();	
  dd_close();
  //QUIT_PRESSED = 1; // Following LADARMapper's lead

}

// TODO: Set the shutdown flag in the quit function
int user_quit(long arg)
{
  return DD_EXIT_LOOP;
}


