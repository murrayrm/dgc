#ifndef LADARFEEDER_HH
#define LADARFEEDER_HH

#include <StateClient.h>
#include <pthread.h>
#include "laserdevice.h"
//#include "ladar_power.h"
#include "LadarConstants.h"
#include <CMapPlus.hh>
#include <unistd.h>
#include "frames/frames.hh"
#include "LadarClient.hh"
#include <DGCutils>
#include "SensorConstants.h"

// Player/Gazebo stuff
/*! the name of the config file */
#define CONFIG_FILENAME "config"
/*! the host to connect to the Player Server at (if no config file found) */
#define PLAYER_GAZEBO_HOST "localhost"
/*! the port to connect to the Player Server at (if no config file found) */
#define PLAYER_GAZEBO_PORT 6665

#define _PLAYER_

using namespace std;

enum {
  ROOF,
  BUMPER,

  NUM_LADAR_TYPES
};

class LadarFeeder : public CStateClient {
public:
  LadarFeeder(int skynetKey, int RoofOrBumper);
  ~LadarFeeder();
  
  void ActiveLoop();
  
  void ReceiveDataThread();

  void SparrowDisplayLoop();

  void UpdateSparrowVariablesLoop();

  int sim_mode;

private:
  int sentnum;

  int numscans;

  CMapPlus ladarMap;

  CLaserDevice LADAR;

#ifdef _PLAYER_
  LadarClient roofPlayerLadar;
#endif
  frames ladarFrame;

  XYZcoord ladarPositionOffset;
  RPYangle ladarAngleOffset;

  double ladarx[20];
  double ladary[20];
  double ladarz[20];

  int layerID_ladarElev;
  double noDataVal_ladarElev;
  double outsideMapVal_ladarElev;
};

#endif
