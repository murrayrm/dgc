#include <stdlib.h>
#include <stdio.h>

#include "LadarFeeder.hh"
#include <DGCutils>

#include <getopt.h>

enum {
  OPT_NONE,
  OPT_WHICHLADAR,
  OPT_SNKEY,
  OPT_NOSPARROW,

  NUM_OPTS
};

int main(int argc, char** argv) {
  int useRoofOrBumper;
  int optSNKey;
  int optLADAR=-1;

  int optNoSparrow = 0;

  int c;
  int digit_optind = 0;


  static struct option long_options[] = {
    //Options that require arguments
    {"snkey", required_argument, 0, OPT_SNKEY},
    {"ladar", required_argument, 0, OPT_WHICHLADAR},
    {0,0,0,0}
  };
  
  while (1) {
    int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    
    c = getopt_long_only(argc, argv, "", long_options, &option_index);
    if(c == -1) break;

    switch(c) {
    case OPT_WHICHLADAR:
      if(strcmp(optarg, "bumper")==0 || strcmp(optarg, "b")==0) {
	optLADAR = BUMPER;
      } else if(strcmp(optarg, "roof")==0 || strcmp(optarg, "r")==0) {
	optLADAR = ROOF;
      } else {
	printf("No such LADAR as '%s' is known - please use roof or bumper\n", optarg);
	exit(1);
      }
      break;
    case OPT_SNKEY:
      optSNKey = atoi(optarg);
      break;
    default:
      if(c!=0) {
	printf("Unknown option %d!\n", c);
	//printStereoHelp();
	exit(1);
      }
    }
  }
  
  if(optLADAR<0) {
    printf("No LADAR unit specified - please use the --ladar {roof, bumper} argument to choose a LADAR\n");
    exit(1);
  }


  //Setup skynet
  int intSkynetKey = 0;
  char* ptrSkynetKey = getenv("SKYNET_KEY");
  
  if(ptrSkynetKey == NULL) {
    //FIX ME
    printf("no sn key");
    return 0;
  } else {
    printf("Welcome to LadarFeederMain\n");
    intSkynetKey = atoi(ptrSkynetKey);
    LadarFeeder ladarFeederObj(intSkynetKey, optLADAR);
    
    //start extra threads here
    ladarFeederObj.sim_mode = 1; 

    //DGCstartMemberFunctionThread(&ladarFeederObj, &LadarFeeder::SparrowDisplayLoop);
    //DGCstartMemberFunctionThread(&ladarFeederObj, &LadarFeeder::UpdateSparrowVariablesLoop);
    
    printf("here we go actiiiiiiiiiive\n");


    ladarFeederObj.ActiveLoop();
  }

  return 0;
}
