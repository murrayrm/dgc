#include "LadarFeeder.hh"

#define TTY "/dev/ttyS0"
#define ROOF_LADAR_SCAN_ANGLE 100
#define ROOF_LADAR_RESOLUTION 0.5
#define ROOF_LADAR_UNITS 0.01
#define LOG_SCANS 1  //need to make this an option

FILE* scanlog;           // Log files for various data
FILE* statelog;

LadarFeeder::LadarFeeder(int skynetKey, int optLADAR) 
  : CSkynetContainer(SNladarfeeder, skynetKey) {
  printf("Welcome to ladarfeeder init\n");

  int numRows = 150;
  int numCols = 150;
  double resRows = 1.0;
  double resCols = 1.0;

  sentnum=0;

  numscans = 0;

  if(optLADAR==BUMPER) {
    printf("=====BUMPER=====\n");
    ladarPositionOffset = XYZcoord(X_BUMPER_LADAR, Y_BUMPER_LADAR, Z_BUMPER_LADAR);
    ladarAngleOffset = RPYangle(ROLL_BUMPER_LADAR, PITCH_BUMPER_LADAR, YAW_BUMPER_LADAR);
  } else if(optLADAR==ROOF) {
    printf("=====ROOF!====\n");
    ladarPositionOffset = XYZcoord(X_ROOF_LADAR, Y_ROOF_LADAR, Z_ROOF_LADAR);
    ladarAngleOffset = RPYangle(ROLL_ROOF_LADAR, PITCH_ROOF_LADAR, YAW_ROOF_LADAR);
  } else {
    printf("No such LADAR known\n");
    exit(1);
  }

  ladarFrame.initFrames(ladarPositionOffset, 
			ladarAngleOffset.P,
			ladarAngleOffset.R,
			ladarAngleOffset.Y);
  
 ladarMap.initMap(0,0, 
		  numRows, numCols,
		  resRows, resCols,
		  0);


  noDataVal_ladarElev = -99;
  outsideMapVal_ladarElev = -100;
  layerID_ladarElev = ladarMap.addLayer<double>(noDataVal_ladarElev,
						 outsideMapVal_ladarElev);
  
  //ladarMap.initLayerLog(layerID_ladarElev, "temp");
  //ladarMap.startLayerLog(layerID_ladarElev);
  
  sim_mode = 1; //need to make this an option
  if(!sim_mode)
    {
      while( LADAR.Setup(TTY,ROOF_LADAR_SCAN_ANGLE,
			 ROOF_LADAR_RESOLUTION) != 0 )//initiate the real ladar
	{
	  sleep(1);
	}
    }
  else
    {
#ifdef _PLAYER_
      // initialize any player stuff.
      roofPlayerLadar.init();
      //initPlayer();
#endif
    }
  
  
  time_t t = time(NULL);
  tm *local;
  local = localtime(&t);
  int i;
  
  char filename[255];
  char datestamp[255];

  sprintf(datestamp, "%02d%02d%04d_%02d%02d%02d",
	  local->tm_mon+1, local->tm_mday, local->tm_year+1900,
	  local->tm_hour, local->tm_min, local->tm_sec);
  
  sprintf(filename, "logs/scans_%s.log", datestamp);
  printf("Logging scans to file: %s\n", filename);
  scanlog=fopen(filename,"w");
    if ( scanlog == NULL ) {
      printf("Unable to open scan log!!!\n");
      exit(-1);
    }
    // print data format information in a header at the top of the scanlog file
    fprintf(scanlog, "%% angle = %d\n", ROOF_LADAR_SCAN_ANGLE);
    fprintf(scanlog, "%% resolution = %lf\n", ROOF_LADAR_RESOLUTION);
    fprintf(scanlog, "%% units = %lf\n", ROOF_LADAR_UNITS);
    fprintf(scanlog, "%% Time[sec] | scan points\n");
    fprintf(scanlog, "%% x offset = %lf\n", ladarPositionOffset.X);
    fprintf(scanlog, "%% y offset = %lf\n", ladarPositionOffset.Y);
    fprintf(scanlog, "%% z offset = %lf\n", ladarPositionOffset.Z);
    fprintf(scanlog, "%% roll offset = %lf\n", ladarAngleOffset.R);
    fprintf(scanlog, "%% pitch offset = %lf\n", ladarAngleOffset.P);
    fprintf(scanlog, "%% yaw offset = %lf\n", ladarAngleOffset.Y);

    sprintf(filename, "logs/state_%s.log", datestamp);
    printf("Logging state to file: %s\n", filename);
    statelog=fopen(filename,"w");
    if ( statelog == NULL ) {
      printf("Unable to open state log!!!\n");
      exit(-1);
    }
    // print data format information in a header at the top of the state file
    fprintf(statelog, "%% LADARTime [usec] | StateTime[usec] | Northing[m] | Easting [m] Altitude[m]");
    fprintf(statelog, " | Vel_N[m/s] | Vel_E[m/s] | Vel_U[m/s]" );
    fprintf(statelog, " | Acc_N[m/s^2] | Acc_E[m/s^2] | Acc_U[m/s^2]" );
    fprintf(statelog, " | Roll[rad] | Pitch[rad] | Yaw[rad]" );
    fprintf(statelog, " | RollRate[rad/s] | PitchRate[rad/s] | YawRate[rad/s]");
    fprintf(statelog, " | RollAcc[rad/s^2] | PitchAcc[rad/s^2] | YawAcc[rad/s^2]");
    fprintf(statelog, "\n");



}


LadarFeeder::~LadarFeeder() {
  //FIX ME
  printf("Thank you for using the ladarmapper destructor\n");
}


void LadarFeeder::ActiveLoop() {
  //TODO - Give this a sparrow/sngui display
  printf("Entering LadarFeeder active-loop\n");
  printf("Press Ctrl+C to quit\n");

  int socket_num = m_skynet.get_send_sock(SNladardeltamap);
  //int channel_num = m_skynet.listen(SNladardeltamap, SNladarfeeder);

  void* deltaPtr = NULL;
  int deltaSize = 0;
  int numScanPoints=0;


  double *laserdata;
  unsigned long long scanTimestamp;
  int status=0;
  int i=0;
  double phi;
  double roofOffset;
  int numruns=0;
  XYZcoord npoint, scanpoint;
  while(true)
  	{
  UpdateState();
  m_state.Altitude = 0;
  
#warning "Should change updateVehicleLoc to be updateMapCenter"
  //check to see if we've moved enough to make it worth moving
  //acquire mutex?
  ladarMap.updateVehicleLoc(m_state.Northing, m_state.Easting);
  
  if (sim_mode) 
    {
#ifdef _PLAYER_
      // Read from player
      // after this we will have a roofPlayerLadar.scanRanges[] array
      roofPlayerLadar.grab();
      numScanPoints = roofPlayerLadar.numScanPoints;
      //cout << "\nplayer: numscanpoints: " << numScanPoints <<endl;
      status = 1;
#endif
    } 
  else 
    {
      numScanPoints = LADAR.getNumDataPoints();
      status = LADAR.UpdateScan();
    }
  laserdata = (double *) malloc(numScanPoints * sizeof(double));
  DGCgettime(scanTimestamp);
  // int i;	  
  
  fprintf(statelog, "%llu %llu %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
	  scanTimestamp,
	  m_state.Timestamp, 
	  m_state.Northing, m_state.Easting, m_state.Altitude,
	  m_state.Vel_N, m_state.Vel_E, m_state.Vel_D,
	  m_state.Acc_N, m_state.Acc_E, m_state.Acc_D,
	  m_state.Roll, m_state.Pitch, m_state.Yaw,
	  m_state.RollRate, m_state.PitchRate, m_state.YawRate,
	  m_state.RollAcc, m_state.PitchAcc, m_state.YawAcc);
  fflush(statelog);
  
  
  fprintf(scanlog, "%llu ", scanTimestamp);
  // update local data buffer
  if (sim_mode) 
    {
#ifdef _PLAYER_
      
      //cout << "looping" << endl;
      // Fill laserdata array
      for(i=0; i < numScanPoints ;i++)
	{
#warning "Check the units!  The Sick LADAR returns centimeters(?).  What does Player give?"
	  //cout << roofPlayerLadar.scanRanges[i]<< " ";
	  laserdata[i] = roofPlayerLadar.scanRanges[i];
	}
      cout << "\n";
      if (LOG_SCANS) 
	{
	  for ( i = 0; i < numScanPoints; i++) 
	    {
	      fprintf(scanlog,"%d ", (int) (laserdata[i] * 100.0) );
	    }
	  fprintf(scanlog,"\n");
	}
#endif
    } 
  else // not sim
    {
      for( i = 0; i < numScanPoints; i++)
	{
	  if(LADAR.isDataError(status,LADAR.Scan[i]) == LADAR_DATA_BAD) {
	    laserdata[i] = LADAR_BAD_SCANPOINT;
	  }
	  else {
	    laserdata[i] = (LADAR.Scan[i] * ROOF_LADAR_UNITS);
	    fprintf(scanlog,"%d ", LADAR.Scan[i]);
	  }
	}
      fprintf(scanlog, "\n");
    }  
  
  // The Sick LADAR delivers scans from right to left.  This offset describes the
  // yaw of the first range measurement in the sensor coordinate system
  // (center range measurement is zero yaw).  
  roofOffset = ((180.0 - ROOF_LADAR_SCAN_ANGLE) / 2.0) * M_PI / 180.0;
  
  //printf("got %d points\n", numScanPoints);
  
  XYZcoord vehpos(m_state.Northing, m_state.Easting, m_state.Altitude);
  // Set the transformation from vehicle to nav coordinates
  ladarFrame.updateState(vehpos, m_state.Pitch, m_state.Roll, m_state.Yaw);
  for(i = 0; i < numScanPoints; i++) 
    {
      // phi is meant to be the scan yaw in the sensor frame
      phi = roofOffset + (i * ROOF_LADAR_RESOLUTION * M_PI) / 180.0;
      scanpoint.X = laserdata[i]*sin(phi);
      scanpoint.Y = laserdata[i]*cos(phi);
      scanpoint.Z = 0;
      // Calculate the range measurement's global
      npoint = ladarFrame.transformS2N(scanpoint);
      ladarMap.setDataUTM_Delta<double>(layerID_ladarElev, npoint.X, npoint.Y, npoint.Z);
    }

  // Calculate a few scan points for sparrow.
  for(int j=0; j<20; j++)
    {
    i=j*10;
    phi = roofOffset + (i * ROOF_LADAR_RESOLUTION * M_PI) / 180.0;
    scanpoint.X = laserdata[i]*sin(phi);
    scanpoint.Y = laserdata[i]*cos(phi);
    scanpoint.Z = 0;
    
    npoint = ladarFrame.transformS2B(scanpoint);
    
    ladarx[j] = npoint.X;
    ladary[j] = npoint.Y;
    ladarz[j] = npoint.Z;
    
    //ladarx[j] = laserdata[i];
    //ladary[j] = 0;
    //ladarz[j]=0;
    
    numscans++;
    
    }
  
  //while(true)
  //  {
  cout << "numScanPoints: " <<numScanPoints << endl;
  deltaPtr = ladarMap.serializeDelta<double>(layerID_ladarElev, deltaSize);
  //cout << "deltaPtr is: " << deltaPtr <<endl;
  printf("deltaSize is %d\n", deltaSize);    

  
  //code to send delta
  if(deltaSize>20)
    {
      cout << "socketnum is: " << socket_num << endl;
      //cout << "channelnum(listen) is: " << channel_num << endl;
      sentnum = m_skynet.send_msg(socket_num, deltaPtr, deltaSize, 0);
      numruns++;
      cout << numruns;
      //usleep(10000);
      //cout << "get msg returns :" <<m_skynet.get_msg(socket_num, deltaSize, deltaSize,0);
      printf("Sent was %d\n", sentnum);
    }
  else
    {
      numruns++;
      cout << numruns;
      //printf("deltaSize <= 20. Didn't send\n");
    }
  //release mutex?
  //some sleep?
  //printf("Sleepy sleepy\n");
  //sleep(1);
  //usleep(100000);
  ladarMap.resetDelta(layerID_ladarElev);
	}
  free(laserdata);
}


void LadarFeeder::ReceiveDataThread() {
  printf("Data receiving not yet");
}
