#include "PlannerModule.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

extern int LOG_DATA;
extern int HALT_VEHICLE;
extern int QUIT_PRESSED;
extern unsigned long SLEEP_TIME;

int TimeSec     = 0;
int TimeUSec    = 0;
double TimeDiff = 0.0;

int SparrowUpdateCount  = 0;

double state_pitch,state_yaw,state_roll;
double state_e,state_n,state_a;
double state_speed;

char* currLayer;

int user_quit(long arg);
int save_CMapPlus(long arg);
int clear_CMapPlus(long arg);
int refresh_CMapPlus(long arg);
int set_nextActiveLayer(long arg);

#include "vddtable.h"

void CPlannerModule::UpdateSparrowVariablesLoop() 
{
  TimeSec = 0;
  TimeUSec = 0;
  TimeDiff = 0.0;
  
  state_pitch = (m_state.Pitch / (2 * M_PI)) * 360.0;
  state_yaw = (m_state.Yaw / (2 * M_PI)) * 360.0;
  state_roll = (m_state.Roll / (2 * M_PI)) * 360.0;
  state_n = m_state.Northing;
  state_e = m_state.Easting;
  state_a = m_state.Altitude;
	//  state_speed = m_state.Speed;

  SparrowUpdateCount++;
}

void CPlannerModule::SparrowDisplayLoop() 
{
  dbg_all = 0;

  if (dd_open() < 0) exit(1);
  dd_bindkey('q', user_quit);
  dd_bindkey('n', set_nextActiveLayer);
  dd_bindkey('s', save_CMapPlus);
  dd_bindkey('c', clear_CMapPlus);
  dd_bindkey('r', refresh_CMapPlus);

  dd_usetbl(vddtable);

  cout << "Sparrow display should show up in 1 sec" << endl;
  sleep(1); // Wait a bit, because other threads will print some stuff out
  dd_loop();
  dd_close();
  QUIT_PRESSED = 1;
}

// TODO: Set the shutdown flag in the quit function
int user_quit(long arg)
{
  QUIT_PRESSED = 1;
  return DD_EXIT_LOOP;
}

int set_nextActiveLayer(long arg)
{
  return 0;
}

int refresh_CMapPlus(long arg)
{
  return 0;
}

int save_CMapPlus(long arg)
{
  return 0;
}

int clear_CMapPlus(long arg)
{
  return 0;
}
