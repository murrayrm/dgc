#include <getopt.h>

#include <iostream>
using namespace std;

#include "PlannerModule.hh"
#include "DGCutils"

#define LOG_OPT             9
#define SLEEP_OPT          11
#define HELP_OPT           13

extern int EF_ALLOW_MALLOC_0=1;

// whether or not the data will be dumped to a log file
int LOG_DATA = 1; 

unsigned long SLEEP_TIME = 50000;   // How long in us to sleep in active fn.
int NO_SPARROW_DISP      = 1;       // Whether or not to show the display
int NOSTATE              = 0;       // Don't update state if = 1.
int SIMPLEUPDATE         = 1;       // Use the simple method to update cells
int NOCONFIRM            = 1;       // Don't ask for confirmation of options
int NOPLAN               = 0;
int NOMM                 = 1;       // Not using ModeManModule
int STATICMAP            = 0;       // static map or map from fusionmapper?
char *TESTNAME           = 0;       // id for test to be part of log file name

/* The name of this program. */
const char * program_name;

/* Prints usage information for this program to STREAM (typically stdout 
   or stderr), and exit the program with EXIT_CODE. Does not return. */
void print_usage (FILE* stream, int exit_code)
{
  fprintf( stream, "Usage:  %s [options]\n", program_name );
  fprintf( stream,
           "  --noconfirm       Do not ask for confirmation of options.\n"
           "  --nodisp          Do not run Sparrow display.\n"
           "  --nostate         Continue even if state data is not received.\n"
	         "  --noplan          Do not run the planner\n"
           "  --mm              Using ModeManModule.\n"
           "  --simple          Use the simple cell update method.\n"
           "  --staticmap       Use the static map defined by cmap.dat and cmap.hdr\n"
           "  --log testname    Log the state and votes in the testRuns\n"  
           "                    testLogs directory. Log files will be named\n"
					 "                    as <testname>_<type>_<date>_<time>.log\n"
           "                    where <type> is state, or votes.\n"
           "  --sleep us        Specifies us sleep time in Active function.\n"  
           "  --help, -h        Display this message.\n" );      
  exit(exit_code);
}

int main(int argc, char **argv) 
{
  // set the default argument parameters that won't need external access.
  // keep the strdup because it allocates the memory.  sprintf doesn't.
  TESTNAME = strdup("temp");
  
  int ch;
  /* A string listing valid short options letters. */
  const char* const short_options = "h";
  /* An array describing valid long options. */
  static struct option long_options[] = 
  {
    // first: long option (--option) string
    // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
    // third: if pointer, set variable to value of fourth argument
    //        if NULL, getopt_long returns fourth argument
    {"noconfirm",  0, &NOCONFIRM,        1},
    {"nodisp",     0, &NO_SPARROW_DISP,  1},
    {"nostate",    0, &NOSTATE,          1},
    {"simple",     0, &SIMPLEUPDATE,     1},
    {"noplan",     0, &NOPLAN,           1},
    {"mm",         0, &NOMM,             0},
    {"staticmap",  0, &STATICMAP,        1},
    {"log",        1, NULL,              LOG_OPT},
    {"sleep",      1, NULL,              SLEEP_OPT},
    {"help",       0, NULL,              'h'},
    {NULL,         0, NULL,              0}
  };

  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  program_name = argv[0];
  printf("\n");

  // Loop through and process all of the command-line input options.
  while((ch = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
  {
    switch(ch)
    {
      case LOG_OPT:
        { LOG_DATA = 1;
          TESTNAME = strdup(optarg); }
          break;

      case SLEEP_OPT:
        SLEEP_TIME = (unsigned long) atol(optarg);
        break;
     
      case 'h':
        /* User has requested usage information. Print it to standard
           output, and exit with exit code zero (normal 
           termination). */
        print_usage(stdout, 0);

      case '?': /* The user specified an invalid option. */
        /* Print usage information to standard error, and exit with exit
           code one (indicating abnormal termination). */
        print_usage(stderr, 1);

      case -1: /* Done with options. */
        break;

    }
  }

	int sn_key = 0;
	char* pSkynetkey = getenv("SKYNET_KEY");
  if( pSkynetkey == NULL )
  {
    cerr << "SKYNET_KEY environment variable isn't set" << endl;
  }
	else
		sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;

	CPlannerModule plannerModule(sn_key);

  if( !NO_SPARROW_DISP )
	{
		DGCstartMemberFunctionThread(&plannerModule, &CPlannerModule::SparrowDisplayLoop);
		DGCstartMemberFunctionThread(&plannerModule, &CPlannerModule::UpdateSparrowVariablesLoop);
	}
	if( !STATICMAP )
		DGCstartMemberFunctionThread(&plannerModule, &CPlannerModule::getMapDeltasThread);

	plannerModule.PlanningLoop();

  return 0;
}
