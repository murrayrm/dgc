CONSTANTS_PATH = $(DGC)/constants

CONSTANTS_DEPEND = \
	$(CONSTANTS_PATH)/AliceConstants.h  \
	$(CONSTANTS_PATH)/GlobalConstants.h  \
	$(CONSTANTS_PATH)/VehicleState.hh \
	$(CONSTANTS_PATH)/LadarConstants.h \
	$(CONSTANTS_PATH)/Makefile
