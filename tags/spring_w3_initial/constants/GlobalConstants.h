#ifndef GLOBALCONSTANTS_H
#define GLOBALCONSTANTS_H

/*! This file should contain all constants that are independent of which 
 ** platform the code is running on and independent of any specific code
 ** implementation. */

// Conversions
#define METERS_PER_FOOT 0.304800609
#define MPS_PER_MPH     0.447040893

#define RDDF_FILE "rddf.dat"

#endif
