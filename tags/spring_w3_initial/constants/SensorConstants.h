/*
** SensorConstants.h
** Created by: George Hines, 20 March 2005
** Last Modified by: George Hines, 23 March 2005
********************
** This file contains the coordinates of the sensors in the coordinate system of Alice
** It does not include the stereocameras; these read separate calibration files
********************
** Alice's vehicle coordinate system has its origin in the ground plane directly below the center of the front axle
** It is in the North-East-Down System, which means the following:
***** The x-axis points forward
***** The y-axis points starboard
***** The z-axis points down (into the ground)
*/

#ifndef SENSORCONSTANTS_H
#define SENSORCONSTANTS_H

/****************ROOF LADAR****************/
#define X_ROOF_LADAR        -0.477      // [m]
#define Y_ROOF_LADAR         0.0        // [m]; by definition
#define Z_ROOF_LADAR        -2.387      // [m]

#define ROLL_ROOF_LADAR     -0.0019439  // roll angle [rad]; measured by least squares estimation
#define PITCH_ROOF_LADAR    -0.20407    // pitch angle [rad]; measured by least squares estimation
#define YAW_ROOF_LADAR       0.0        // yaw angle [rad]; assumed

/****************BUMPER LADAR**************/
#define X_BUMPER_LADAR        0.965       // [m]
#define Y_BUMPER_LADAR        0.0         // [m]; this is by definition
#define Z_BUMPER_LADAR       -0.648       // [m]

#define ROLL_BUMPER_LADAR     0.000589    // roll angle [rad]; measured by least squares estimation
#define PITCH_BUMPER_LADAR   -0.1682      // pitch angle [rad]; measured by least squares estimation
#define YAW_BUMPER_LADAR      0.0         // yaw angle [rad]; assumed

/****************GPS******************/
#define X_GPS           -1.391     // [m]
#define Y_GPS           -0.3       // [m]
#define Z_GPS           -2.501     // [m]

                                   // These constants are necessary for correct operation of the GPS, and are here until a better place can be found for them
#define EARTH_RADIUS  6378000.     // [m]
#define ERROR_FALLOFF .75          // dimensionless
#define ERROR_WEIGHT  2.           // dimensionless
#define ANGLE_CUTOFF  M_PI/6.      // dimensionless

/*******************IMU********************/
#define X_IMU                -1.391     // [m]
#define Y_IMU                0.0     // [m]
#define Z_IMU                -2.501     // [m]

#define ROLL_IMU         0.0        // roll angle [rad]; by definition
#define PITCH_IMU        0.2        // pitch angle [rad]; by definition
#define YAW_IMU          0.0        // yaw angle [rad]; by definition

#endif
