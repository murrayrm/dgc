/* 
** AliceConstants.h
**
**NOTE TO ANYONE CONCERNED: On H's go-ahead, I cleaned up a superfluous reference to the incorrect (Bob's) vehicle mass. -- George Hines
** 
** This header is for any needed inherent physical constants of the vehicle.
** ******** This file contains shop measurements for shop-measurable statistics, taken before sensors were mounted and befor the new bumper
** ******** was installed.  More accurate measurements will be taken after sensor installation and pending characterization by STI
**
** ******** NOTE: everything below H's comment below ("Constant definitions for the model of Alice") is considered not shop-measurable.
** ******** They are still Bob constants until STI does their thing.
**
**
*/
#ifndef VEHICLECONSTANTS_H
#define VEHICLECONSTANTS_H


#define  VEHICLE_MASS     4309    // [kg] NOTE: this came from the vehicle gross weight in the owner's manual.
                                  // A more accurate figure will be obtained from STI
#define  VEHICLE_LENGTH   5.4356  // [m] length of the vehicle from the end of the hitch to the front of the front bumper.
#define  VEHICLE_WIDTH    2.1336  // [m] width of the vehicle, measured between the sidewalls of the front tires.
#define  VEHICLE_HEIGHT   2.2352  // [m] height of the vehicle from ground to crown of roof.  NOTE: this does NOT include Yakima rack or sensors!!!

#define VEHICLE_MAX_LEFT_STEER  0.3495 // [rad] maximum turning angle to the left
#define VEHICLE_MAX_RIGHT_STEER 0.3495 // [rad] maximum turning angle to the right
#define VEHICLE_MAX_AVG_STEER   0.3495 // [rad] average max turning angle
#define VEHICLE_MAX_TAN_AVG_STEER   (tan(VEHICLE_MAX_AVG_STEER)) // tan(average max turning angle)


// TODO: [Luis] Can make these names more descriptive
#define  VEHICLE_H   1.0668  // [m], height of Center of Mass from ground
                                 // TODO: NOTE: this is the worst case scenario, 
                                 // since cannot know from spec sheet
                                    
#define  VEHICLE_L   (1.759/2.0) // [m], distance of left wheel to projection of CM down on axle
#define  VEHICLE_R   (1.759/2.0) // [m], distance of right wheel to projection of CM down on axle

#define  VEHICLE_TIRE_RADIUS   0.3937 // [m]  

#define  VEHICLE_AXLE_DISTANCE   3.5306 // [m] distance between front and rear axles
#define  VEHICLE_WHEELBASE       (VEHICLE_AXLE_DISTANCE)

#define  VEHICLE_REAR_TRACK      1.7145  // [m] distance between centerline of rear wheels
#define  VEHICLE_FRONT_TRACK     1.8034  // [m] distance between centerline of front wheels
#define  VEHICLE_AVERAGE_TRACK   ((VEHICLE_REAR_TRACK+VEHICLE_FRONT_TRACK)/2.0)

// Constant definitions for the model of Alice
// Haomiao Huang
// 1/20/2005

// NOTE: These constants are currently actually bob
// constants, cause we don't have anything for Alice yet

// lengths from C.G. to front and rear axles, resp. (m)
#warning "These don't add up to VEHICLE_AXLE_DISTANCE!"
#define L_f 1.50
#define L_r 1.36

// max right and left steering angles (rad)
#define PHI_MAX  ( VEHICLE_MAX_RIGHT_STEER)
#define PHI_MIN  (-VEHICLE_MAX_LEFT_STEER)

// steering lag constant
#define TAU_PHI 1.0

// Weight of vehicle in newtons
#define WEIGHT ((VEHICLE_MASS) * 9.8)

// moment of inertia in kg m^2
// this is guesstimated by taking alice as a rectangular block (4.8m long,
// 1.95 m tall, the numbers are smaller than actual cause she's empty)
// and finding the moment of inertia that way.  The formula
// approximately got a close answer for Bob's size, so I'm using this fudge
// for now. 
//#define I_VEH 4951.0
#define I_VEH 10342.0

// engine gearing and force constants
#define K_F1 57.32058
#define K_F2 2.289722
#define K_F3 (-.00368453)
#define K_F4 8.314834
#define K_F5 (-.1950403)
#define K_F6 .0001505491
#define K_T1 6.0
#define K_T2 2.0
#define FUDGE 2.0
#define LBFT2NM 1.35

#define R_TC 1.0 // torque converter
#define R_TRANS1 3.114 // transmission
#define R_TRANS2 2.216
#define R_TRANS3 1.545
#define R_TRANS4 1.000
#define R_TRANS5 0.712
#define R_DIFF 4.10 // diffrential
#define R_TRANSFER 1.08 // transfer case
#define R_WHEEL VEHICLE_TIRE_RADIUS // wheel radius
//#define R_WE R_TC * R_TRANS * R_DIFF * R_TRANSFER

// Idle engine speed (somewhat arbitrary)
#define W_IDLE (200 * 2 * M_PI / 60.0)

// Pacejka Tire Constants
#define By 7.685
#define Cy 1.1344 
#define Ey -1.552
#define MU 1.0
// #define Dy 10675.0 
#define Dy (WEIGHT * MU / 2.0)

// brake constants 
// NOTE: THESE ARE MASSIVELY FUDGED, AND ARE JUST THERE SO WE HAVE BRAKES
// OF SOME SORT
#define BF_MAX (-13645.0)
#define K_B1 5.0
#define K_B2 4.0



// constants needed for RddfPathGen
// paths will be generated with these constants - turn them down
// if the path is too aggressive
/*
#define RDDF_MAXACCEL   (9.8 * .1) // maximum forward acceleration
#define RDDF_MAXDECEL   (9.8 * .1) // maximum braking acceleration
#define RDDF_MAXLATERAL (9.8 * .5) // maximum sideways acceleration
*/

// redefined for modeman
#define RDDF_MAXACCEL   (9.8 * .1) // maximum forward acceleration
#define RDDF_MAXDECEL   2.0        // maximum braking acceleration
#define RDDF_MAXLATERAL (9.8 * .5) // maximum sideways acceleration

//This width is subtracted from either side of the corridor to make sure the paths we generate keep the entire vehicle within the corridor.
#define RDDF_VEHWIDTH_EFF  ((VEHICLE_WIDTH+0.40)/2.0) // width/2.0 plus a margin of error

// end constants needed for RddfPathGen
    
#endif
