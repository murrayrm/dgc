/* FakeState_Update.cc - new vehicle state estimator
 *
 * JML 31 Dec 04
 *
 * */

#include "FakeState.hh"
#include "playerclient.h"

PlayerClient* robot = new PlayerClient("localhost");
GpsProxy* GPSpos = new GpsProxy(robot, 0, (unsigned char) 'r');
Position3DProxy* RobotPos = new Position3DProxy(robot, 0, (unsigned char) 'r');

void FakeState::UpdateState()
{
  robot->SetDataMode(PLAYER_DATAMODE_PULL_ALL);
  robot->Read();
  long currentTime = (long) (GPSpos->time).tv_usec + (GPSpos->time).tv_sec * 1000000;

  timediff = (double) (currentTime - vehiclestate.Timestamp) / 1000000;
 
  if (timediff == 0)
  {
    return;
  }

  DGClockMutex(&m_VehicleStateMutex);
  struct VehicleState vehiclestate_old;
  vehiclestate_old.Acc_N =  vehiclestate.Acc_N; 
  vehiclestate_old.Acc_E = vehiclestate.Acc_E;
  vehiclestate_old.Acc_D =  vehiclestate.Acc_D;
  vehiclestate_old.RollAcc =   vehiclestate.RollAcc;
  vehiclestate_old.PitchAcc =  vehiclestate.PitchAcc; 
  vehiclestate_old.YawAcc = vehiclestate.YawAcc; 
  // Velocities
  vehiclestate_old.Vel_N = vehiclestate.Vel_N;
  vehiclestate_old.Vel_E = vehiclestate.Vel_E;
  vehiclestate_old.Vel_D = vehiclestate.Vel_D;
  vehiclestate_old.PitchRate =  vehiclestate.PitchRate;
  vehiclestate_old.RollRate = vehiclestate.RollRate;
  vehiclestate_old.YawRate =  vehiclestate.YawRate;
  vehiclestate_old.Northing = vehiclestate.Northing;
  vehiclestate_old.Easting = vehiclestate.Easting;
  vehiclestate_old.Altitude = vehiclestate.Altitude;
  vehiclestate_old.Yaw =  vehiclestate.Yaw;
  vehiclestate_old.Pitch = vehiclestate.Pitch;
  vehiclestate_old.Roll =  vehiclestate.Roll;
  vehiclestate_old.Timestamp =  vehiclestate.Timestamp;

  //New State
 
  vehiclestate.Timestamp = (long) (GPSpos->time).tv_usec + (GPSpos->time).tv_sec * 1000000;

  vehiclestate.Northing = GPSpos->utm_northing;
  vehiclestate.Easting = GPSpos->utm_easting;
  vehiclestate.Altitude =GPSpos->altitude;
  
  vehiclestate.Vel_N = RobotPos->XSpeed();
  vehiclestate.Vel_E = RobotPos->YSpeed();
  vehiclestate.Vel_D = RobotPos->ZSpeed();
  
  vehiclestate.Acc_N = (vehiclestate.Vel_N - vehiclestate_old.Vel_N) / timediff;
  vehiclestate.Acc_E = (vehiclestate.Vel_E - vehiclestate_old.Vel_E) / timediff;
  vehiclestate.Acc_D = (vehiclestate.Vel_D - vehiclestate_old.Vel_D) / timediff;

  vehiclestate.Yaw = RobotPos->Yaw();
  vehiclestate.Yaw = vehiclestate.Yaw + IMU_YAW_OFFSET;
  vehiclestate.YawRate =  (vehiclestate.Yaw - vehiclestate_old.Yaw) / timediff; //RobotPos->YawSpeed();
  vehiclestate.YawAcc = (vehiclestate.YawRate - vehiclestate_old.YawRate) / timediff;
  
  vehiclestate.Pitch = RobotPos->Pitch();
  vehiclestate.Pitch = vehiclestate.Pitch + IMU_PITCH_OFFSET; 
  vehiclestate.PitchRate = (vehiclestate.Pitch - vehiclestate_old.Pitch) / timediff; //RobotPos->PitchSpeed();
  vehiclestate.PitchAcc = (vehiclestate.PitchRate - vehiclestate_old.PitchRate )/ timediff;

  vehiclestate.Roll = RobotPos->Roll();
  vehiclestate.Roll = vehiclestate.Roll + IMU_ROLL_OFFSET;
  vehiclestate.RollRate = (vehiclestate.Roll - vehiclestate_old.Roll) / timediff; //RobotPos->RollSpeed();
  vehiclestate.RollAcc = (vehiclestate.RollRate - vehiclestate_old.RollRate ) / timediff;

  cout << "------------------" << endl;
  cout << "Northing  : " << vehiclestate.Northing << endl;
  cout << "Easting   : " << vehiclestate.Easting << endl;
  cout << "Altitude  : " << vehiclestate.Altitude << endl;
  cout << "Yaw       : " << vehiclestate.Yaw << endl;
  cout << "Roll      : " << vehiclestate.Roll << endl;
  cout << "Pitch     : " << vehiclestate.Pitch << endl;
  cout << "------------------" << endl;
  cout << "Northing V: " << vehiclestate.Vel_N << endl;
  cout << "Easting V : " << vehiclestate.Vel_E << endl;
  cout << "Altitude V: " << vehiclestate.Vel_D << endl;
  cout << "Yaw V     : " << vehiclestate.YawRate << endl;
  cout << "Roll V    : " << vehiclestate.RollRate << endl;
  cout << "Pitch V   : " << vehiclestate.PitchRate << endl;
  cout << "------------------" << endl;
  cout << "Northing A: " << vehiclestate.Acc_N << endl;
  cout << "Easting A : " << vehiclestate.Acc_E << endl;
  cout << "Altitude A: " << vehiclestate.Acc_D << endl;
  cout << "Yaw A     : " << vehiclestate.YawAcc << endl;
  cout << "Roll A    : " << vehiclestate.RollAcc << endl;
  cout << "Pitch A   : " << vehiclestate.PitchAcc << endl;
  cout << "------------------" << endl;


  cout << "X: " << RobotPos->Xpos() << endl;
  cout << "Y: " << RobotPos->Ypos() << endl;
  cout << "TimeStamp (old) : " << vehiclestate_old.Timestamp <<endl;
  cout << "TimeStamp (new) : " << vehiclestate.Timestamp <<endl;
  cout << "Time Diff: " << timediff << endl;
  cout << "GPS Time (sec) : " << (GPSpos->time).tv_sec <<endl;
  cout << "GPS Time (usec) : " << (GPSpos->time).tv_usec <<endl;
  cout << "------------------" << endl;

  DGCunlockMutex(&m_VehicleStateMutex);
}
