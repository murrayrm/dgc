/**
 * SDSsim.cc
 * A multi-threaded server that can be used to emulate the SDS
 * for Gazebo testing.
 * Revision History:
 * 03/29/05 hbarnor created
 */

#include "SDSsimMT.hh"

SDSSimMT::SDSsimMT()
{
  if(initSockets() == ERROR)
    {
      exit(1);
    }
  else
    {

    }
}

SDSsimMT::~SDSsimMT()
{

}

int SDSsimMT::initSockets()
{
  LadarSocket = socket(AF_INET, SOCK_STREAM, 0); // create the ladar socket
  if(LadarSocket < 0 )
    {
      perror("cannot open ladar socket");
      return ERROR; 
    }
  bzero(&serverAddr, sizeof(serverAddr)); // clear the address structure
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_addr.s_addr = htonl(INADDR_ANY); // set address in byte order
  serverAddr.sin_port = htons(LADAR_SDS_PORT); // set the port 
  if(bind(LadarSocket, (struct sockaddr *) & serverAddr, sizeof(serverAddr)) < 0 )
    {
      perror("Cannot bind Ladar Port ");
      return ERROR;
    }
  if(listen(LadarSocket, QLEN) < 0)
    {
      perror("Listen Failed on LadarSocket");
      return EROOR;
    }
  return 1;
}

void SDSsimMT::ladarServer()
{
  
}
