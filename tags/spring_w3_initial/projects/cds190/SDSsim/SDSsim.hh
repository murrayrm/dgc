/**
 * SDSsim.hh
 * A multi-threaded server that can be used to emulate the SDS
 * for Gazebo testing.
 * Revision History:
 * 03/29/05 hbarnor created
 */

#ifndef SDSSIMMT_HH
#define SDSSIMMT_HH

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <iostream>
#include <string>

//DEFINES
/** port number for Ladar */
#define LADAR_SDS_PORT 8003
/** size of data buffer */
#define BSIZE 1000 

using namespace std;
/**
 * The class representing the SDS
 *
 */
class SDSsimMT
{

public:
  /**
   * Constructor - creates an SDSsimMT instance and spawns threads
   * to listen on the ports needed for the different modules
   */
  SDSsimMT();
  /**
   * Destructor for memory management
   *
   **/
  ~SDSsimMT();
  /**
   *
   */
private:
  /** structure to hold server address */
  struct sockaddr_in serverAddr; 
  /** structure to hold client computer address */
  struct sockaddr_in ladarClientAddr;
  /** socket descriptors */
  int LadarSocket; 
  /** length of address */
  int addLength;
  /** buffer for sending message */
  char buf[BSIZE];
  /**
   * initSockets - performs all the background initialization for 
   * for a socket 
   *
   **/
  int initSockets();
};
#endif
