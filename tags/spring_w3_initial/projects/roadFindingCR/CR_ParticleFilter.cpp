//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_ParticleFilter.hh"

// C = random_normal_sample_CR_Matrix(A, B);

// A = make_CR_Matrix(n, m)
// free_CR_Matrix(A)
// B = scale_CR_Matrix(n, A)
// B = copy_CR_Matrix(A)
// C = multiply_CR_Matrix(A, B) => C = product_CR_Matrix(A, B)
// S = sum_CR_Matrices(M_array, n) => S = sum_CR_Matrix_array(M_array, n)
// uniform_CRRandom() => uniform_CR_Random()

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void reset_CR_ParticleFilter(CR_ParticleFilter *PF)
{
  int i;

  PF->updates = 0;

  //----------------------------------------------------
  // generate samples from prior p(x)
  //----------------------------------------------------

  for (i = 0; i < PF->n; i++) 
    PF->samp_prior(PF->s[i]);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// mean is a d x 1 matrix, where d is the dimensionality of the state
// entries of s are also d x 1 matrices

CR_ParticleFilter *make_CR_ParticleFilter(int n, CR_Vector *mean, CR_Matrix *Cov, CondP_ZX condprob_zx, SamplePrior samp_prior, SampleState samp_state, DynamicsState dyn_state)
{
  int i;
  CR_ParticleFilter *PF;

  print_CR_Matrix(Cov);

  PF = (CR_ParticleFilter *) CR_malloc(sizeof(CR_ParticleFilter));

  PF->updates = 0;

  PF->n = n;

  PF->s = (CR_Vector **) CR_calloc(n, sizeof(CR_Vector *));
  PF->s_new = (CR_Vector **) CR_calloc(n, sizeof(CR_Vector *));

  PF->pi = make_CR_Vector("pi", n);
  PF->c = make_CR_Vector("c", n);

  PF->pi_s = (CR_Vector **) CR_calloc(PF->n, sizeof(CR_Vector *)); 

  PF->sdet = copy_CR_Vector(mean);

  PF->x = copy_CR_Vector(mean);

  PF->condprob_zx = condprob_zx;
  PF->samp_prior = samp_prior;
  PF->samp_state = samp_state;
  PF->dyn_state = dyn_state;

  PF->Cov = copy_CR_Matrix(Cov);
  PF->Covsqrt = copy_CR_Matrix(PF->Cov);
  square_root_CR_Matrix(PF->Cov, PF->Covsqrt);
  //copy_CR_Matrix(PF->Cov, PF->Covsqrt); // this was just a workaround while LAPACK was busted

  PF->Covalpha = gaussian_alpha_CR_Matrix(PF->Cov);

  PF->M = make_CR_Matrix("M", n, mean->rows);
  PF->MT = make_CR_Matrix("MT", mean->rows, n);
  PF->C = make_CR_Matrix("C", mean->rows, mean->rows);
  PF->mean = copy_CR_Vector(mean);

  //----------------------------------------------------
  // generate samples from prior p(x)
  //----------------------------------------------------

  for (i = 0; i < n; i++) {
    PF->s[i] = copy_CR_Vector(mean);
    PF->pi_s[i] = copy_CR_Vector(mean);
    PF->samp_prior(PF->s[i]);
    PF->s_new[i] = copy_CR_Vector(PF->s[i]);
  }

  return PF;
}

//----------------------------------------------------------------------------

// z is array of measurements, n is how many there are

void update_CR_ParticleFilter(void *Z, CR_ParticleFilter *PF)
{
  int i, samp_chosen;
  double r;

  //----------------------------------------------------

  PF->updates++;

  //----------------------------------------------------
  // FIRST UPDATE:
  // compute conditional probabilities p(z|x) 
  // and keep track of their partial sums
  //----------------------------------------------------

  /*
  if (PF->updates == 1) {      // this is the first update

    PF->pi_sum = 0;
    for (i = 0; i < PF->n; i++) {
      PF->pi->x[i] = PF->condprob_zx(Z, PF->s[i]);
      PF->pi_sum += PF->pi->x[i]; 
    }
    
    for (i = 0; i < PF->n; i++) {
      PF->pi->x[i] /= PF->pi_sum;
      if (i == 0)
	PF->c->x[i] = PF->pi->x[i];
      else
	PF->c->x[i] = PF->c->x[i - 1] + PF->pi->x[i];
    }
  }

  //----------------------------------------------------
  // pick n samples (with replacement) from sample set,
  // choosing sample s_i with probability pi_i,
  // to obtain s_new
  //----------------------------------------------------
  
  for (i = 0; i < PF->n; i++) {
    r = uniform_CR_Random();
    samp_chosen = interval_membership(0, PF->n - 1, PF->c, r);
    copy_CR_Vector(PF->s[samp_chosen], PF->s_new[i]);
  }

  //----------------------------------------------------
  // predict by applying deterministic and stochastic
  // transformations to elements of s_new,
  // which become s for the next round.
  // also, keep track of weighted samples in pi_s
  // and estimate state
  //----------------------------------------------------

  for (i = 0; i < PF->n; i++) {
    //    product_sum_CR_Matrix(1.0, PF->F, PF->s_new[i], 0.0, NULL, PF->sdet);
    PF->dyn_state(PF->s_new[i], PF->sdet);
    PF->samp_state(PF->sdet, PF->s[i]);
    //    sample_gaussian_CR_Matrix(PF->Qalpha, PF->s[i], PF->sdet, PF->Qsqrt);
  }
  */

  //----------------------------------------------------
  // compute conditional probabilities p(z|x) 
  // and keep track of their partial sums
  //----------------------------------------------------

  PF->pi_sum = 0;
  for (i = 0; i < PF->n; i++) {
    PF->pi->x[i] = PF->condprob_zx(Z, PF->s[i]);
    PF->pi_sum += PF->pi->x[i]; 
  }
  
  for (i = 0; i < PF->n; i++) {
    PF->pi->x[i] /= PF->pi_sum;
    if (i == 0)
      PF->c->x[i] = PF->pi->x[i];
    else
      PF->c->x[i] = PF->c->x[i - 1] + PF->pi->x[i];
  }

  //----------------------------------------------------
  // pick n samples (with replacement) from sample set,
  // choosing sample s_i with probability pi_i,
  // to obtain s_new
  //----------------------------------------------------
  
  for (i = 0; i < PF->n; i++) {
    r = uniform_CR_Random();
    samp_chosen = interval_membership(0, PF->n - 1, PF->c, r);
    copy_CR_Vector(PF->s[samp_chosen], PF->s_new[i]);
  }

  //----------------------------------------------------
  // predict by applying deterministic and stochastic
  // transformations to elements of s_new,
  // which become s for the next round.
  // also, keep track of weighted samples in pi_s
  // and estimate state
  //----------------------------------------------------

  for (i = 0; i < PF->n; i++) {
    //    product_sum_CR_Matrix(1.0, PF->F, PF->s_new[i], 0.0, NULL, PF->sdet);
    PF->dyn_state(PF->s_new[i], PF->sdet);
    PF->samp_state(PF->sdet, PF->s[i]);
    //    sample_gaussian_CR_Matrix(PF->Qalpha, PF->s[i], PF->sdet, PF->Qsqrt);
  }

  //----------------------------------------------------
  // keep track of weighted samples and their sum,
  // which is the estimated state.
  // a mode finder makes more sense
  //----------------------------------------------------
  
  // remember to initialize M, MT, mean, and C when PF is created
  /*
  convert_CR_Vector_array_to_CR_Matrix(PF->s, PF->M);
  mean_cov_CR_Matrix(PF->M, PF->MT, PF->mean, PF->C);
  print_CR_Vector(PF->mean);
  print_CR_Matrix(PF->C);
  printf("%lf\n", determinant_CR_Matrix(PF->C));
  */

  for (i = 0; i < PF->n; i++) { 
    copy_CR_Vector(PF->s[i], PF->pi_s[i]);
    scale_CR_Vector(PF->pi->x[i], PF->pi_s[i]);
  }

  sum_CR_Vector_array(PF->pi_s, PF->n, PF->x);
  //  print_CR_Vector(PF->x);

  // confidence is ratio of determinant of basic process covariance to overall sample distribution covariance
  // but these samples are weighted (pi_s vs. s)--how to incorporate that?

  //----------------------------------------------------
  // clean up
  //----------------------------------------------------

//   for (i = 0; i < n; i++) 
//     free_CR_Vector(z[i]);
// 
//   CR_free_calloc(z, n, sizeof(CR_Vector *));
}

//----------------------------------------------------------------------------

int interval_membership(int l, int u, CR_Vector *c, double r)
{
  int m;

  if (l == u)
    return l;
  else {
    m = (l + u) / 2;
    if (c->x[m] >= r)
      return interval_membership(l, m, c, r);
    else {
      if (m == u - 1)
	return u;
      else
	return interval_membership(m, u, c, r);
    }
  }
}

//----------------------------------------------------------------------------

void print_CR_ParticleFilter(CR_ParticleFilter *PF)
{
  int i;

  printf("\n");

  printf("%i ------------------------------\n\n", PF->updates);

  for (i = 0; i < PF->n; i++) {
    printf("%i:\n", i);
    print_CR_Vector(PF->s[i]);
  }

  printf("\n");

  for (i = 0; i < PF->n; i++)
    printf("%i: pi = %f\n", i, PF->pi->x[i]);

  printf("\n");

  for (i = 0; i < PF->n; i++)
    printf("%i: c = %f\n", i, PF->c->x[i]);

  printf("\n");
  printf("***********************\n");
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
