//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_Linux_Image.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// in-place vertical flip (assuming even height)

void vflip_CR_Image(CR_Image *im)
{
  int i;

  for (i = 0; i < im->h / 2; i++) {
    // swap rows i and im->h - i - 1
    memcpy(im->temprow, &(im->data[i * im->wstep]), im->wstep);
    memcpy(&(im->data[i * im->wstep]), &(im->data[(im->h - i - 1) * im->wstep]), im->wstep);
    memcpy(&(im->data[(im->h - i - 1) * im->wstep]), im->temprow, im->wstep);
  }
}

//----------------------------------------------------------------------------

void decimate_CR_Image(CR_Image *src, CR_Image *dst, int shrink_factor)
{
  int xsrc, ysrc, xdst, ydst;

  for (ysrc = ydst = 0; ysrc < src->h; ysrc += shrink_factor, ydst++)
    for (xsrc = xdst = 0; xsrc < src->w; xsrc += shrink_factor, xdst++) {
      IMXY_R(dst, xdst, ydst) = IMXY_R(src, xsrc, ysrc);
      IMXY_G(dst, xdst, ydst) = IMXY_G(src, xsrc, ysrc); 
      IMXY_B(dst, xdst, ydst) = IMXY_B(src, xsrc, ysrc);
    }


}


//----------------------------------------------------------------------------

// figures out desired image format from filename extension

void write_CR_Image(char *filename, CR_Image *im)
{
  int image_type;

  image_type = determine_type_CR_Image(filename);

  if (image_type == CR_IMAGE_PPM)
    write_CR_Image_to_PPM(filename, im);
  else 
    CR_error("write_CR_Image(): can not handle that kind of image right now");
}

//----------------------------------------------------------------------------

void write_CR_Image_to_PPM(char *filename, CR_Image *im)
{
  int i, j, t, intensity;
  FILE *fp;


  // write size info


  /*
    fp = fopen(filename, "wb");
    
    //    fprintf(fp, "P6\n%i %i\n255\n", im->width, im->height);
    fprintf(fp, "P5\n%i %i\n255\n", im->width, im->height);
    
    // write binary image data
    
    for (j = 0, t = 0; j < im->height; j++) 
    for (i = 0; i < im->width; i++, t += 3) {
    //	fprintf(fp, "%c%c%c", im[t], im[t+1], im[t+2]);
    iplGetPixel(im, i, j, pixel);
    //	fprintf(fp, "%c%c%c", (unsigned char) pixel[IPL_RED], (unsigned char) pixel[IPL_GREEN], (unsigned char) pixel[IPL_BLUE]);
    fprintf(fp, "%c", (unsigned char) pixel[IPL_RED]);
    }
  */
  
  fp = fopen(filename, "w");
  
  fprintf(fp, "P3\n%d %d\n255\n", im->w, im->h);

  
  for (j = 0; j < im->h; j++) {
      //      for (i = 1; i < im->width; i++) {
      for (i = 0; i < im->w; i++) {

	fprintf(fp, "%i %i %i ", (unsigned char) IMXY_R(im, i, j), (unsigned char) IMXY_G(im, i, j), (unsigned char) IMXY_B(im, i, j));
	// rgb
	// bgr
	// brg
	// rbg
	// gbr
	// grb
	//fprintf(fp, "%i %i %i ", (int) pixel[IPL_BLUE], (int) pixel[IPL_GREEN], (int) pixel[IPL_RED]);
      }
      fprintf(fp, "\n");
  }
  

  // finish up

  fclose(fp);

}

//----------------------------------------------------------------------------

CR_Image *read_CR_Image(char *filename)
{
  CR_Image *im;
  int image_type;

  image_type = determine_type_CR_Image(filename);

  if (image_type == CR_IMAGE_PPM)
    im = read_PPM_to_CR_Image(filename);
  else if (image_type == CR_IMAGE_JPEG)
    im = read_JPEG_to_CR_Image(filename);
  else 
    CR_error("read_CR_Image(): can only handle ppm, jpg images");

  // may not be true in the future, but it is now

  //  im->type = IPL_TYPE_COLOR_8U;  
  im->type = 0;

  return im;
}

//----------------------------------------------------------------------------

CR_Image *read_JPEG_to_CR_Image(char *filename)
{
  CR_Image *im;

  CR_error("not yet ready for JPEGs\n");

  /*
  im = (CR_Image *) CR_malloc(sizeof(CR_Image));

  im->iplim = CreateImageFromJPEG(filename);

  // im->type ???
  im->w = im->iplim->width;
  im->h = im->iplim->height;
  im->wstep = im->iplim->widthStep;
  im->data = im->iplim->imageData;
  */

  return im;
}

//----------------------------------------------------------------------------

CR_Image *read_PPM_to_CR_Image(char *filename)
{
  int i, j, width, height;
  FILE *fp;
  CR_Image *im;
  int r, g, b, iscomment;
  unsigned char u, v, w;
  unsigned char pixel[3];
  char imtype[3];
  char str[80];
  char c;

  // get size info

  fp = fopen(filename, "rb");
  if (!fp)
    CR_error("read_PPM_to_CR_Image(): no such file");

  fscanf(fp, "%s\n", &imtype);

  // attempt to eat comments

  do {
    iscomment = FALSE;
    c = fgetc(fp);
    ungetc(c, fp);
    if (c == '#') {
      iscomment = TRUE;
      fgets (str, 79, fp);
    }
  } while (iscomment);

  // read image dimensions

  fscanf(fp, "%i %i\n255\n", &width, &height);    

  // allocate space

  im = make_CR_Image(width, height, 0);

  // actually read it in

  // ascii
  if (!strcmp(imtype, "P3")) {

    for (j = 0; j < im->h; j++) {
      for (i = 0; i < im->w; i++) {
	fscanf(fp, "%i %i %i ", &r, &g, &b);
	IMXY_R(im, i, j) = r;
	IMXY_G(im, i, j) = g;    
	IMXY_B(im, i, j) = b;
      }
      fscanf(fp, "\n");
    }
  }

  // binary

  else if (!strcmp(imtype, "P6")) {

    //    CR_error("raw ppm reads not yet working");

    //    CR_error("leaving now");

    for (j = 0; j < im->h; j++) {
      for (i = 0; i < im->w; i++) {

	fscanf(fp, "%c%c%c", &r, &g, &b);

	IMXY_R(im, i, j) = r;
	IMXY_G(im, i, j) = g;    
	IMXY_B(im, i, j) = b;
      }
    }
  }

  // unknown

  else
    CR_error("unrecognized ppm file");

  fclose(fp);

  // return

  return im;
}

//----------------------------------------------------------------------------

// just use suffix

int determine_type_CR_Image(char *filename)
{
  char suffix[4];
  int len;

  len = strlen(filename);
  suffix[0] = filename[len - 3];
  suffix[1] = filename[len - 2];
  suffix[2] = filename[len - 1];
  suffix[3] = '\0';

  //  printf("suffix = %s\n", suffix);

  // Images

  if (!strcmp(suffix, "ppm") || !strcmp(suffix, "PPM"))
    return CR_IMAGE_PPM;
  else if (!strcmp(suffix, "pgm") || !strcmp(suffix, "PGM"))
    return CR_IMAGE_PPM;
  else if (!strcmp(suffix, "pfm") || !strcmp(suffix, "PFM"))
    return CR_IMAGE_PFM;
  else if (!strcmp(suffix, "jpg") || !strcmp(suffix, "JPG"))
    return CR_IMAGE_JPEG;
  else if (!strcmp(suffix, "bmp") || !strcmp(suffix, "BMP"))
    return CR_IMAGE_BITMAP;
  else if (!strcmp(suffix, "rif") || !strcmp(suffix, "RIF"))
    return CR_IMAGE_DEMO3_RIF;
  else if (!strcmp(suffix, "ps") || !strcmp(suffix, "PS") || !strcmp(suffix, "eps") || !strcmp(suffix, "EPS"))
    return CR_IMAGE_EPS;

  // Unknown

  else 
    return CR_UNKNOWN;
}

//----------------------------------------------------------------------------

CR_Image *make_CR_Image(int w, int h, int type)
{
  CR_Image *im;

  im = (CR_Image *) CR_malloc(sizeof(CR_Image));
  im->type = type;

  im->w = w;
  im->h = h;
  im->wstep = 3 * im->w; // this depends on the type---assuming RGB24 for now, so 3 bytes per pixel

 
  im->buffsize = h * im->wstep;   

  im->temprow = (unsigned char *) CR_calloc(im->wstep, sizeof(unsigned char));
  im->data = (unsigned char *) CR_calloc(im->buffsize, sizeof(unsigned char));

  return im;
}

//----------------------------------------------------------------------------

void free_CR_Image(CR_Image *im)
{
  CR_free_calloc(im->data, im->buffsize, sizeof(unsigned char)); 
  CR_free_malloc(im, sizeof(CR_Image));
}

//----------------------------------------------------------------------------

// copy without regard for ROIs

void ip_copy_CR_Image(CR_Image *src, CR_Image *dst)
{
  memcpy(dst->data, src->data, src->buffsize);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------


