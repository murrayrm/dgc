#include <sn_msg.hh>
#include <sys/time.h>
#include <iostream>

#define us_p_s  1000000 // 4294967296

using namespace std;

int main(int argc, char** argv)
{
 int key; 
 unsigned int msg_size;
  double msg_rate;  //Bytes per second
  int testlen;
  if (argc < 4)
  {
    cerr << "This program should only be called from snm-main.sh" << endl;
    cerr << "usage: single-throughput-master key msg_size rate time" << endl;
    cerr << "key is the skynet key, ";
    cerr << "msg_size is in bytes, ";
    cerr << "rate is bytes per second, time is test time in seconds" << endl;
      
    exit(2);
  }
  else
  {
    key = atoi(argv[1]);
    msg_size = atoi(argv[2]);
    msg_rate = (double)atoi(argv[3]);
    testlen = atoi(argv[4]); 
    //cout << key << " " << msg_size << " " << msg_rate << " " << testlen << endl ;
  }
  //converg msg_rate into messages per second
  msg_rate = (double)msg_rate / (double)msg_size;
  unsigned int delay = (unsigned int) (1000000 / msg_rate); //microseconds / message
  struct timeval mytime;
  long long int mytimeint;
  gettimeofday(&mytime,0);
  skynet Skynet(MODmark, key);
  int send_chan = Skynet.get_send_sock(SNmark1);
  char buffer[msg_size];
  buffer[0] = 0;                            //continue character
  mytimeint = mytime.tv_sec * us_p_s + mytime.tv_usec; 
  long long int starttime = mytimeint;
  long long int newmytimeint;
  long long int dtime;
  unsigned int n = 0;
  long long int timer = 1000000 * testlen;  //in microseconds
  while(timer > 0)
  {
    Skynet.send_msg(send_chan, buffer, msg_size, 0);
    gettimeofday(&mytime, 0);
    newmytimeint = mytime.tv_sec * us_p_s + mytime.tv_usec;
    dtime = newmytimeint - mytimeint;
    if (delay - dtime > 1) usleep(delay - dtime);
    mytimeint += delay;
    timer -= delay;
    n++;
  }
  double totaltime = (double)(newmytimeint - starttime)/1000000;
  //now kill the slave.  Send multiple kill signals
  buffer[0] = 1;                            //kill character
  int m;
  for(m=0; m<=25; m++)
  {
    usleep(1000);                             //wait a while
    Skynet.send_msg(send_chan, buffer, 1, 0); //send 1 char
  }
  cerr << "messages= " << n << "\ttime= " << totaltime ;
  double real_rate = (double)(n * msg_size) / totaltime;
  cerr << "\trate= " << real_rate << endl;
  cout << n << " " << real_rate << " " << totaltime <<endl; 
  return 0;
}
