#!/bin/zsh
# zsh is needed for floating point arithmetic

MAX_RATE=100000000   #100MBs
TEST_TIME=1         #seconds each test
ECHO="/bin/echo"   #don't use zsh builtin echo  
RESDIR=$HOSTNAME-$2-`date --iso-8601`
RESFILE="result.csv" #output here

if (( $# == 2 ));
then SLAVE=$2; KEY=$1;
else
  $ECHO "usage: main-snm.sh key slave"
  $ECHO "example: main-snm.sh 587 cobalt.caltech.edu"
  $ECHO "make sure skynetd is running on both computers"
  exit
fi

function do1test 
{
  $1 $3 > markpipe &
  slaveppid=$!
  #output should be sent_messages real_rate real_time
  SEND_DATA=`ssh $SLAVE $2 $3 $4 $5 $6`
  wait $slavepid
  RECV_MSG=`cat markpipe`
  rm markpipe
  $ECHO $RECV_MSG $SEND_DATA
}

function tput121
{
  do1test ./single-throughput-slave dgc/projects/skynetmark/single-throughput-master $1 $2 $3 $4  
}

function lat121
{
  do1test ./single-latency-slave dgc/projects/skynetmark/single-latency-master $1 $2 $3 $4
}

function dotput121test
{
  #$ECHO "1->1 throughput test" >> $RESFILE
  for (( j=1; j<=MAX_RATE; j = j*2 )) #print x axis
    do $ECHO -n $j, >> $RESFILE
  done
  $ECHO >> $RESFILE 
  
  for (( i=1; i<= 65536; i = 2 * i )) #loop on msg_size
    do $ECHO -n $i >> $RESFILE
    $ECHO -n ', ' >> $RESFILE
    for (( j=1; j<=MAX_RATE; j = j*2 )) #loop on rate
      do 
      if (( j < $i / TEST_TIME ))
        then $ECHO -n '-' >> $RESFILE
      else
        result=`tput121 $1 $i $j $TEST_TIME`
        RECV_MSG=`$ECHO $result | cut -d ' ' -f 1`
        SENT_MSG=`$ECHO $result | cut -d ' ' -f 2`
        SENT_TIME=`$ECHO $result | cut -d ' ' -f 4`
        $ECHO result=$result
        $ECHO SENT_TIME=$SENT_TIME
        $ECHO TEST_TIME=$TEST_TIME
        #TODO: use bc for arithmetic to handle floats
        if (( $SENT_TIME < $TEST_TIME+1 ))          #messages sent in timely fashion
          then $ECHO -n $(( 100.*$RECV_MSG/$SENT_MSG )) >> $RESFILE
          else $ECHO -n 'x' >> $RESFILE                  #sender cannot meet rate request
          break
        fi
      fi
      $ECHO -n ', ' >> $RESFILE
    done
    $ECHO >> $RESFILE
  done
}  
  
function dolat121test  
{
  #$ECHO "1->1 latency test" >> $RESFILE
  for (( j=1; j<=MAX_RATE; j = j*2 )) #print x axis
    do $ECHO -n $j, >> $RESFILE
  done
  $ECHO >> $RESFILE 
  
  for (( i=1; i<= 65536; i = 2 * i )) #loop on msg_size
    do $ECHO -n $i >> $RESFILE
    $ECHO -n ', ' >> $RESFILE
    for (( j=1; j<=MAX_RATE; j = j*2 )) #loop on rate
      do 
      if (( j < $i / TEST_TIME ))
        then $ECHO -n '-' >> $RESFILE
      else
        result=`lat121 $1 $i $j $TEST_TIME`
        RECV_MSG=`$ECHO $result | cut -d ' ' -f 1`
        SENT_MSG=`$ECHO $result | cut -d ' ' -f 2`
        SENT_TIME=`$ECHO $result | cut -d ' ' -f 4`
        AVGLAT=`$ECHO $result | cut -d ' ' -f 5`
        STDDEVLAT=`$ECHO $result | cut -d ' ' -f 6`
        MAXLAT=`$ECHO $result | cut -d ' ' -f 7`
  
        $ECHO result=$result
        $ECHO SENT_TIME=$SENT_TIME
        $ECHO TEST_TIME=$TEST_TIME
        if (( $SENT_TIME < $TEST_TIME+1 ))          #messages sent in timely fashion
          then $ECHO -n $AVGLAT:$STDDEVLAT:$MAXLAT >> $RESFILE
          else $ECHO -n 'x' >> $RESFILE                  #sender cannot meet rate request
          break
        fi
      fi
      $ECHO -n ', ' >> $RESFILE
    done
    $ECHO >> $RESFILE
  done
}

install -m 755 -d $RESDIR
#RESFILE=$RESDIR/tput121.csv
#SLAVE=$2
#dotput121test $KEY
#RESFILE=$RESDIR/lat121.csv
#dolat121test $KEY
RESFILE=$RESDIR/tput121local.csv
SLAVE="localhost"
dotput121test $KEY
RESFILE=$RESDIR/lat121local.csv
SLAVE="localhost"
dolat121test $KEY
