#ifndef LOGGERTALKER_HH
#define LOGGERTALKER_HH

#include "SkynetContainer.h"
#include "pthread.h"
#include <unistd.h>
#include "LoggerConfig.hh"

using namespace std;


class CLoggerTalker : virtual public CSkynetContainer
{
public:
  CLoggerTalker();
  ~CLoggerTalker();

  int getTalkerDebugLevel();
  void setTalkerDebugLevel(int value);

  int SendLoggerString(int stringSocket, char* p_string, pthread_mutex_t* pMutex = NULL);
  int RecvLoggerString(int stringSocket, char* p_string, pthread_mutex_t* pMutex = NULL);

  int sendSocket;

private:
  int talker_debug_level;
  pthread_mutex_t talker_debug_level_mutex;

  char* m_pDataBuffer;
};

#endif // LOGGERTALKER_HH
