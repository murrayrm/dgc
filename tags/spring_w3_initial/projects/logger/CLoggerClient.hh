#ifndef CLOGGERCLIENT_HH
#define CLOGGERCLIENT_HH

#include <iostream>
#include "sn_msg.hh"
#include <sys/types.h>
#include <pwd.h>
#include <ifaddrs.h>
#include <unistd.h>
#include <string>
#include <fstream>
#include <net/if.h>
#include <string.h>
#include "DGCutils"
#include "CLoggerTalker.hh"
#include "SkynetContainer.h"
#include "LoggerConfig.hh"


using namespace std;

class CLoggerClient : public CLoggerTalker
{
public:
  CLoggerClient();
  ~CLoggerClient();

  /** Accessors */
  string getModuleName();
  int getDebugLevel();
  bool getLoggingEnabled();

  /** Mutators */
  void setModuleName(string name);
  void setDebugLevel(int level);

  string getLogDir();

  //  int addFile(char* p_file_name); // need directory info as well...
  void addRelFolder(string rel_folder_name); // need directory info as well...
  //  int storeMyIp(char* p_dest);

private:
  /**
   * First, a little documentation on the strategy for managing
   * the different states that the Logger Client may find 
   * itself in.
   *
   * I'll let the complete state be defined by two variables:
   * char* session_timestamp;
   * bool logging_enabled;
   *
   * Other variables depend directly on these, for example,
   * logging_enabled implies that session_timestamp exists and is valid.
   * That and other relations are presented below:
   * 
   * (logging_enabled) => (strlen(session_timestamp) != 0)
   * (strlen(session_timestamp) != 0) => logging directory exists
   * constructor called  => (strlen(module_name ) != 0)
   * constructor called  => (log_directory_base exists and ends with "/")
   *
   * Anytime the state is changed in such a way that breaks part of it
   * (for fixing in subsequent steps), the logger_state_mutex must be locked,
   * so that when the state is available, it's always "verified" or proper
   * or something like that
   */

  /** logger_state */
  void lockState();
  void unlockState();
  pthread_mutex_t logger_state_mutex;
  char* session_timestamp;
  bool logging_enabled;
  // end logger state

  void lockName();
  void unlockName();
  pthread_mutex_t module_name_mutex;
  char* module_name;

  void lockDebug();
  void unlockDebug();
  pthread_mutex_t debug_level_mutex;
  int debug_level;

  void lockLastSeen();
  void unlockLastSeen();
  unsigned long long getLastSeen();
  void setLastSeen(unsigned long long des_time);
  pthread_mutex_t server_last_seen_mutex;
  unsigned long long server_last_seen;

  void lockLocalLogging();
  void unlockLocalLogging();
  bool getLocalLogging();
  void setLocalLogging(bool value);
  pthread_mutex_t local_logging_mutex;
  bool local_logging;

  char* log_directory_base; /**< ends with a "/", doesn't change after constructor */

  void RecvThread();
  void printDebug(string message, int req_level, bool newline = true);
  void createDirFromTimestamp();
  string getDirFromTimestamp();
  string getLogDirectoryBase();
  string getUser();
  string getIPString();
  bool connectionStatus();

  // old functions...do we need these?
  //  the cat from file one
  //void catLogDirName(char* dest, bool fake_it);

};


#endif  // CLOGGERCLIENT_HH
