/*
 * CLogger - logger class member functions
 *
 * Jason Yosinski, Apr 05
 * Documented by Richard and Lars, 16 Apr 05
 *
 */

#include "CLogger.hh"

using namespace std;

/* Clogger constructor */
CLogger::CLogger(int des_skynet_key)
  : CSkynetContainer(MODLogger, des_skynet_key)
{
  /* Initialize data structure */
  skynet_key = des_skynet_key;
  session_timestamp = new char[MAX_TIMESTAMP_LENGTH];
  logging_on = false; // turn logging off when we start
  shutdown = false;
  debug_level = DEFAULT_DEBUG;

  /* Initialize commands to be processed (??) */
  todoSet.insert(""); // because getCommand returns a "" when
                      // we don't do anything

  // make some mutexes before we start the threads
  DGCcreateMutex(&session_timestamp_mutex);
  DGCcreateMutex(&todoListFile_mutex);
  DGCcreateMutex(&logging_on_mutex);
  DGCcreateMutex(&shutdown_mutex);
  DGCcreateMutex(&debug_level_mutex);

  /* Start send and receive threads */
  DGCstartMemberFunctionThread(this, &CLogger::RecvThread);
  DGCstartMemberFunctionThread(this, &CLogger::SendThread);
}


/* CLogger destructor */
CLogger::~CLogger()
{
  delete session_timestamp;

  /* Get rid of mutexes that were used by the threads */
  DGCdeleteMutex(&session_timestamp_mutex);
  DGCdeleteMutex(&todoListFile_mutex);
  DGCdeleteMutex(&logging_on_mutex);
  DGCdeleteMutex(&shutdown_mutex);
  DGCdeleteMutex(&debug_level_mutex);
}


/**
 *
 * ActiveLoop - this is the where user input is accepted.
 *
 * This function prompts for use input and acts on it.  The actually
 * data is taken inside of the receive thread.
 *
 */
void CLogger::LoggerActiveLoop()
{
  char* line_buffer = new char[MAX_INPUT_LENGTH];
  string user_cmd;

  /* Print out a header to let the user know that we are here */
  cout << "\n"
       << "\nLogger Control"
       << "\n====================="
       << "\nType help for a list of commands." << endl;

  /* Loop until quit */
  while (true)
    {
      cout << PROMPT;
      cin >> user_cmd;
      if (user_cmd == "help")
	cout << "  Commands are:\n"
	     << "    start - start logging\n"
	     << "     stop - stop logging\n"
	     << "       d0 - set debug level 0\n"
	     << "       d1 - set debug level 1\n"
	     << "       d2 - set debug level 2\n"
	     << "     quit - quit" << endl;
      else if (user_cmd == "quit")
	{
	  cout << OUTPUT << "quit called" << endl;
	  DGClockMutex(&shutdown_mutex);
	  shutdown = true;
	  DGCunlockMutex(&shutdown_mutex);
	  break;
	}
      else if (user_cmd == "stop")
	{
	  cout << OUTPUT << "stop called" << endl;
	  DGClockMutex(&logging_on_mutex);
	  logging_on = false;
	  DGCunlockMutex(&logging_on_mutex);
	}
      else if (user_cmd == "d0")
	{
	  cout << OUTPUT << "debug level 0" << endl;
	  setDebugLevel(0);
	  setTalkerDebugLevel(0);
	}
      else if (user_cmd == "d1")
	{
	  cout << OUTPUT << "debug level 1" << endl;
	  setDebugLevel(1);
	  setTalkerDebugLevel(1);
	}
      else if (user_cmd == "d2")
	{
	  cout << OUTPUT << "debug level 2" << endl;
	  setDebugLevel(2);
	  setTalkerDebugLevel(2);
	}
      else if (user_cmd == "start")
	{
	  cout << OUTPUT << "start called" << endl;
	  DGClockMutex(&logging_on_mutex);
	  if (logging_on)
	    {
	      DGCunlockMutex(&logging_on_mutex);
	      cout << OUTPUT << "but logging is already going" << endl;
	    }
	  else
	    {
	      /* Turn on data logging */
	      logging_on = true;
	      DGCunlockMutex(&logging_on_mutex);

	      // we also need to update the timestamp at this point
	      unsigned long long the_time;
	      DGCgettime(the_time);
	      long long int the_time_round = llround(DGCtimetosec(the_time));
	      DGClockMutex(&session_timestamp_mutex);
	      sprintf(session_timestamp, "%lli", the_time_round);
	      string timestring(session_timestamp);
	      cout << OUTPUT << "new timestamp is " <<  timestring << endl;

	      // and create a new todo file (base + timestamp)
	      char* filename = new char[MAX_PATH_LENGTH]; filename[0] = '\0';
	      strcat(filename, TODOFILE);
	      strcat(filename, session_timestamp);
	      DGCunlockMutex(&session_timestamp_mutex);
	      todoListFile.open(filename);

	      //change permissions
	      char* permissions_command = new char[MAX_COMMAND_LENGTH];
	      strcat(permissions_command, TODOPERMISSIONS);
	      strcat(permissions_command, " ");
	      strcat(permissions_command, filename);
	      system(permissions_command);

	      todoListFile << TODOHEADER << endl;
	      delete filename;
	    }
	}
      else
	cout << OUTPUT << "unknown command \"" << user_cmd << "\"" << endl;

      fflush(stdin);
    }

  /* Wait for everyone to shut down */
  #warning Should be done by rejoining threads
  sleep(2);

  delete line_buffer;
}



string CLogger::getTimestamp()
{
  DGClockMutex(&session_timestamp_mutex);
  string ret(session_timestamp);
  DGCunlockMutex(&session_timestamp_mutex);
  return ret;
}



int CLogger::getDebugLevel()
{
  DGClockMutex(&debug_level_mutex);
  int ret = debug_level;
  DGCunlockMutex(&debug_level_mutex);
  return ret;
}



void CLogger::setDebugLevel(int value)
{
  DGClockMutex(&debug_level_mutex);
  debug_level = value;
  DGCunlockMutex(&debug_level_mutex);
}



int CLogger::sendDebug()
{
  DGClockMutex(&debug_level_mutex);
  if (debug_level > 0)
    cout << "beginning of sendDebug" << endl;
  DGCunlockMutex(&debug_level_mutex);

  char* test_string = new char[MESSAGE_LENGTH];
  memset(test_string, 'A', MESSAGE_LENGTH-1);
  test_string[MESSAGE_LENGTH-1] = '\0';
  SendLoggerString(sendSocket, test_string);
  delete test_string;

  DGClockMutex(&debug_level_mutex);
  if (debug_level > 0)
    cout << "end of sendDebug" << endl;
  DGCunlockMutex(&debug_level_mutex);

  return 0;
}


/* Thread for receiving logging information */
void CLogger::RecvThread()
{
  DGClockMutex(&debug_level_mutex);
  if (debug_level > 0)
    cout << "CLogger::RecvThread() has been called" << endl;
  DGCunlockMutex(&debug_level_mutex);
  
  int recvSocket = m_skynet.listen(SNloggerstring, ALLMODULES);
  char* p_buffer = new char[MESSAGE_LENGTH];
  char* command = new char[MESSAGE_LENGTH];
  
  while (!shutdown)
    {
      // get a message
      DGClockMutex(&debug_level_mutex);
      if (debug_level > 0)
	cout << "trying to receive message...";
      DGCunlockMutex(&debug_level_mutex);

      RecvLoggerString(recvSocket, p_buffer);
      
      DGClockMutex(&debug_level_mutex);
      if (debug_level > 0)
	cout << "got message \"" << p_buffer << "\"" << endl;
      DGCunlockMutex(&debug_level_mutex);
      
      getCommand(command, p_buffer);
      
      DGClockMutex(&debug_level_mutex);
      if (debug_level > 0)
	cout << "adding command to set \"" << command << "\"... ";
      DGCunlockMutex(&debug_level_mutex);
      
      pair <set<string>::iterator, bool> insertResult;
      insertResult = todoSet.insert(string(command));
      if ( (insertResult.second) )
	{
	  DGClockMutex(&debug_level_mutex);
	  if (debug_level > 0)
	    cout << "unique command!" << endl;
	  DGCunlockMutex(&debug_level_mutex);
	  
	  DGClockMutex(&todoListFile_mutex);
	  todoListFile << command << " " << LOG_DESTINATION << endl;
	  DGCunlockMutex(&todoListFile_mutex);
	}
      else
	{
	  DGClockMutex(&debug_level_mutex);
	  if (debug_level > 0)
	    cout << "duplicate." << endl;
	  DGCunlockMutex(&debug_level_mutex);
	}
    }

  /* Flush the todo list */
  cout << "Closing todo list" << endl;
  todoListFile.flush();
  todoListFile.close();
  
  delete p_buffer;
  delete command;
}



/** 
 * should just constantly be sending out the current timestamp to
 * be used for logging
 */
void CLogger::SendThread()
{
  char* session_timestamp_copy = new char[MAX_TIMESTAMP_LENGTH];
  char* session_timestamp_old = new char[MAX_TIMESTAMP_LENGTH];
  char* message = new char[MESSAGE_LENGTH];

  while (!shutdown)
    {
      message[0] = '\0';

      if (!logging_on)
	strcat(message, TAG_LOGOFF);
      else
	{
	  DGClockMutex(&session_timestamp_mutex);
	  strcpy(session_timestamp_copy, session_timestamp);
	  DGCunlockMutex(&session_timestamp_mutex);
	  
	  // do we have to do anything special for new logs?
	  // if (strcmp(session_timestamp_copy, session_timestamp_old) != 0)
	  strcat(message, TAG_LOGTIMEON);
	  strcat(message, session_timestamp_copy);
	}

      SendLoggerString(sendSocket, message);
      
      usleep((unsigned int)(1000000.0 * SERVER_SLEEP));
    }

  delete message;
  delete session_timestamp_old;
  delete session_timestamp_copy;
}



void CLogger::getCommand(char* command, char* message)
{
  command[0] = '\0';
  int length = strlen(message);

  //cout << " 1 command is \"" << command << "\"" << endl;
  if (length >= TAGLENGTH)
    {
      if ( (strncmp(message, TAG_ADDFILE, TAGLENGTH) == 0) )
	{
	  strcat(command, CMD_ADDFILE);
	  //cout << " 2 command is \"" << command << "\"" << endl;
	  strcat(command, " ");
	  //cout << " 3 command is \"" << command << "\"" << endl;
	  strcat(command, (message + TAGLENGTH));
	  //cout << " 4 command is \"" << command << "\"" << endl;
	  strcat(command, " ");
	  //cout << " 5 command is \"" << command << "\"" << endl;
	  catDGCDirFromFile(command);
	  //cout << " 6 command is \"" << command << "\"" << endl;
	}
      else if ( (strncmp(message, TAG_ADDFOLDER, TAGLENGTH) == 0) )
	{
	  // make sure it ends with a '/', since it's a folder
	  // (makes it more obvious)
	  if (message[strlen(message) - 1] != '/')
	    strcat(message, "/");

	  strcat(command, CMD_ADDFOLDER);
	  //cout << " 2 command is \"" << command << "\"" << endl;
	  strcat(command, " ");
	  //cout << " 3 command is \"" << command << "\"" << endl;
	  strcat(command, (message + TAGLENGTH));
	  //cout << " 4 command is \"" << command << "\"" << endl;
	  strcat(command, " ");
	  //cout << " 5 command is \"" << command << "\"" << endl;
	  catDGCDirFromFile(command);
	  //cout << " 6 command is \"" << command << "\"" << endl;
	}
    }
}



/**
 * Attempts to read in the DGCDIRFILE in the current directory to get
 * the path to the root (usually will be "/home/user/dgc")
 */
void CLogger::catDGCDirFromFile(char* dest)
{
  ifstream file(DGCDIRFILE);
  if (!file.is_open())
    cout << "COULDN'T OPEN \"" << DGCDIRFILE << "\"!" << endl;
  string from_file;
  file >> from_file;
  strcat(dest, from_file.c_str());

  file.close();

  DGClockMutex(&debug_level_mutex);
  if (debug_level > 0)
    cout << "CLogger::catDGCDirFromFile() - cat'ed \"" << from_file << "\"" << endl;
  DGCunlockMutex(&debug_level_mutex);
}
