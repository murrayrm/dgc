#ifndef LOGGER_CONFIG
#define LOGGER_CONFIG

#define MAX_TIMESTAMP_LENGTH 32
#define MAX_MODULENAME_LENGTH 32
#define MAX_INPUT_LENGTH 256
#define MAX_PATH_LENGTH 128
#define MAX_COMMAND_LENGTH 256
#define MESSAGE_LENGTH 128

#define TAGLENGTH 6
#define TAG_LOGTIMEON  "Slto  "  /**< server logging time, logging is on*/
#define TAG_LOGOFF     "S  f  "  /**< server logging is off */
#define TAG_ADDFILE    "Caf   "  /**< client add file */
#define TAG_ADDFOLDER  "Cad   "  /**< client add folder */
#define CMD_ADDFILE    "scp"
#define CMD_ADDFOLDER  "scp -r"
#define CMD_MKDIR      "mkdir -p"
#define PROMPT         "> "
#define OUTPUT         " * "

#define DEFAULT_DEBUG 3
#define MAX_SERVER_DELAY 10.0  /**< seconds before we consider the server AWOL */
#define SERVER_SLEEP 2.0      /**< how long the server sleeps */

#define TODOFILE "todo_"
#define TODOHEADER "#!/bin/sh"
#define TODOPERMISSIONS "chmod a+wrx"
#define DEFAULT_MODULENAME "NameGoesHere"
#define FAKE_TIMESTAMP_MODIFIER "_i"
#define LOG_DIRECTORY "logs" // base path of where to log, from dgc.  no "/"
#define DGCDIRFILE "dgcpath.txt"
#define LOG_DESTINATION "$1"

typedef enum
  {
    LOGRddfPathGen, // all modules must begin with LOG
    LOGLAST
  }
logger_module;

#endif //LOGGER_CONFIG
