#include <jni.h>
#include "ByteArray.h"

JNIEXPORT jint JNICALL 
Java_ByteArray_sumArray(JNIEnv *env, jobject obj, jbyteArray arr)
{
  jsize len = env->GetArrayLength(arr);
  int i;
  int sum = 0;
  
  jbyte *body = env->GetByteArrayElements(arr, 0);
  for (i=0; i<len; i++) {
    sum += body[i];
  }
  env->ReleaseByteArrayElements(arr, body, 0);
  return sum;
}
