#include <math.h>
#include <sys/time.h>
#include "frames/coords.hh"
#include "trajMap.hh"
#include "rddf.hh"
#include "CCorridorPainter.hh"
#include "AliceConstants.h"
#include "reactivePlanner.hh"
using namespace std;

reactivePlanner::reactivePlanner()
{
  traj = new CTraj(3);
  for(int i = 0; i < LINES_DEEP * ((LINES_LONG - 1) * POINTS_WIDE *
      POINTS_WIDE + POINTS_WIDE); i++)
    arc[i] = new CTrajMap(2);
}

reactivePlanner::~reactivePlanner()
{
  delete traj;
  for(int i = 0; i < LINES_DEEP * ((LINES_LONG - 1) * POINTS_WIDE *
      POINTS_WIDE + POINTS_WIDE); i++)
    delete arc[i];
}

CTraj *reactivePlanner::plan(RDDF *rddf, CMap map, int layer,
          NEcoord &initialPoint, double initialTheta, double initialSpeed)
{
  m_rddf = rddf;
  m_layer = layer;
  m_map = &map;
  points[0] = initialPoint;
  theta[0] = initialTheta;
  m_speed0 = initialSpeed;

  blurMap();

  pickPoints();

  makeBundles();

  pickArcs();

  makeTrajFromArcs();
  
  return traj;
}

/*Produces lines of points perpendicular to the RDDF track line.
  Also gives the heading of points in each line*/
void reactivePlanner::pickPoints()
{
  int i, j, p, q;
  double corRad, dF, dB, tF, tB, dtF, dtB;
  NEcoord pos, vec;

  for(i = 0; i < LINES_LONG; i++)
  {   
    pos = m_rddf->getPointAlongTrackLine(points[0], DISTANCE_BETWEEN_LINES *
        (i + 1), &theta[i + 1], &corRad);
    //Rotate trackline yaw if near waypoint to make smoother turns.
    p = m_rddf->getNumTargetPoints();
    q = m_rddf->getCurrentWaypointNumber(pos);
    if(q > 0) dB = (pos - m_rddf->getWaypoint(q)).norm();
    else dB = 1e10;
    if(q < p) dF = (pos - m_rddf->getWaypoint(q + 1)).norm();
    else dF = 1e10;
    if(dB < corRad && dF < corRad)
    {
      tB = m_rddf->getTrackLineYaw(q - 1);
      tF = m_rddf->getTrackLineYaw(q + 1);
      dtB = fmod(theta[i + 1] - tB, 2 * M_PI);
      if(dtB <= -M_PI) dtB  += 2 * M_PI;
      else if(dtB > M_PI) dtB  -= 2 * M_PI;
      dtF = fmod(tF - theta[i + 1], 2 * M_PI);
      if(dtF <= -M_PI) dtF  += 2 * M_PI;
      else if(dtF > M_PI) dtF  -= 2 * M_PI;
      theta[i + 1] += (dB * dtF - dF * dtB) / (2 * (dB + dF));
    }
    else if(dB < corRad)
    {
      tB = m_rddf->getTrackLineYaw(q - 1);
      dtB = fmod(theta[i + 1] - tB, 2 * M_PI);
      if(dtB <= -M_PI) dtB  += 2 * M_PI;
      else if(dtB > M_PI) dtB  -= 2 * M_PI;
      theta[i + 1] -= (corRad - dB) * dtB / (2 * corRad);
    }
    else if(dF < corRad)
    {
      tF = m_rddf->getTrackLineYaw(q + 1);
      dtF = fmod(tF - theta[i + 1], 2 * M_PI);
      if(dtF <= -M_PI) dtF  += 2 * M_PI;
      else if(dtF > M_PI) dtF  -= 2 * M_PI;
      theta[i + 1] += (corRad - dF) * dtF / (2 * corRad);
    }
    //Convert corridor radius to effective corridor radius.
    if(corRad > RDDF_VEHWIDTH_EFF) corRad -=RDDF_VEHWIDTH_EFF;
    else corRad = 0;
    //Vector from pos to right corridor boundary.
    vec = NEcoord(-corRad * sin(theta[i + 1]), corRad * cos(theta[i + 1]));
    //Points are evenly spaced between the effective corridor boundaries.
    #warning Need to worry about wide corridors.
    for(j=0; j<POINTS_WIDE; j++)
      points[INDEX2(i, j)] = pos + vec * (2 * (double)j /
          (POINTS_WIDE - 1) - 1);
  }
}

void reactivePlanner::makeBundles()
{
  int i, j, k, f;

  for(f = 0; f < LINES_DEEP; f++)
  {
    for(k = 0; k < POINTS_WIDE; k++)
    {
      makeArc(INDEX2a(f, k), points[0], theta[0], points[INDEX2(f, k)],
          theta[f + 1], r[INDEX2a(f, k)], d[INDEX2a(f, k)], m[INDEX2a(f, k)],
          n[INDEX2a(f, k)]);
      for(i = 0; i < LINES_LONG - f - 1; i++)
        for(j = 0; j < POINTS_WIDE; j++)
          makeArc(INDEX4(i, j, f, k), points[INDEX2(i, j)], theta[i + 1],
              points[INDEX2(i + f + 1, k)], theta[i + f + 2],
              r[INDEX4(i, j, f, k)], d[INDEX4(i, j, f, k)],
              m[INDEX4(i, j, f, k)], n[INDEX4(i, j, f, k)]);
    }
  }
}

/*Returns a pointer to a traj made up of 2 circular arcs that smoothly connect
  two unit vectors.  Also, gives the radius of the arcs and the arc length
  between points.*/
void reactivePlanner::makeArc(int index, NEcoord p0, double theta0,
              NEcoord p1, double theta1, double &r, double &d, int &m, int &n)
{
  makeArc(index, p0.N, p0.E, theta0, p1.N, p1.E, theta1, r, d, m, n);
}

/*Returns a pointer to a traj made up of 2 circular arcs that smoothly connect
  two unit vectors.  Also, gives the radius of the arcs and the arc length
  between points.*/
void reactivePlanner::makeArc(int index, double x0, double y0,
                          double theta0, double x1, double y1,
                          double theta1, double &r, double &d, int &m, int &n)
{
  int i, p;
  double x, y, theta, ra, rb, ang0, ang1, turn0, turn1, turn0a, turn1a,
          turn0b, turn1b, angD, l, la, lb, northing[2], easting[2],
          ct0, st0, ct, st, c0, c1, c2, cn0, ce0, cn1, ce1, ctt;

  delete arc[index];
  arc[index] = new CTrajMap(2);
  ct0 = cos(theta0);
  st0 = sin(theta0);
  //Translate and rotate coordinate system to simplify equations.
  x = (x1 - x0) * ct0 + (y1 - y0) * st0;
  y = (y1 - y0) * ct0 + (x0 - x1) * st0;
  theta = fmod(theta1 - theta0, 2 * M_PI);
  if(theta < 0) theta += 2 * M_PI;
  if(0 <= theta && theta < 0.00001) theta = 0.00001;  //can't div by 0
  if(2 * M_PI - 0.00001 < theta && theta <= 2 * M_PI)
    theta = 2 * M_PI - 0.00001;
  //Thus spake Mathematica: (see makeArc.nb)
  ct = cos(theta);
  st = sin(theta);
  c0 = y + y * ct - x * st;
  c1 = sqrt(3 * x * x + 4 * y * y - (x * ct + y * st) * 
        (x * (2 + ct) + y * st));
  c2 = -2 + 2 * ct;

  ra = (c0 - c1) / c2;
  if(ra >= 0)
  {
    turn0a = fmod(atan2(x + ra * st, ra - (y - ra * ct)), 2 * M_PI);
    turn1a = fmod(turn0a - theta, 2 * M_PI);
  }
  else
  {
    turn0a = fmod(atan2(x + ra * st, y - ra * ct - ra), 2 * M_PI);
    turn1a = fmod(turn0a + theta, 2 * M_PI);
  }
  if(turn0a < 0) turn0a += 2 * M_PI;
  if(turn1a < 0) turn1a += 2 * M_PI;
  la = fabs(ra) * (turn0a + turn1a);

  rb = (c0 + c1) / c2;
  if(rb >= 0)
  {
    turn0b = fmod(atan2(x + rb * st, rb - (y - rb * ct)), 2 * M_PI);
    turn1b = fmod(turn0b - theta, 2 * M_PI);
  }
  else
  {
    turn0b = fmod(atan2(x + rb * st, y - rb * ct - rb), 2 * M_PI);
    turn1b = fmod(turn0b + theta, 2 * M_PI);
  }
  if(turn0b < 0) turn0b += 2 * M_PI;
  if(turn1b < 0) turn1b += 2 * M_PI;
  lb = fabs(rb) * (turn0b + turn1b);

  if(la <= lb) { r = ra; turn0 = turn0a; turn1 = turn1a; l = la; }
  else { r = rb; turn0 = turn0b; turn1 = turn1b; l = lb; }

  p = (int)(l / INTERNAL_TRAJ_SPACING + 1);
  angD = (turn0 + turn1) / p;
  d = l / p;
  m = (int)(turn0 / angD) + 1;
  n = p - m;

  cn0 = x0 - r * st0;
  ce0 = y0 + r * ct0;
  if(r >= 0) ctt = theta0 + turn0;
  else
  {
    angD = -angD;
    ctt = theta0 - turn0;
  }
  cn1 = cn0 + 2 * r * sin(ctt);
  ce1 = ce0 - 2 * r * cos(ctt);
  for(i = 0; i < m; i++)
  {
    ang0 = i * angD + theta0;
    northing[1] = cos(ang0);
    easting[1] = sin(ang0);
    northing[0] = cn0 + r * easting[1];
    easting[0] = ce0 - r * northing[1];
    arc[index]->inputWithDiffs(northing, easting);
  }
  angD = -angD;
  for(i = 1; i <= n; i++)
  {
    ang1 = i * angD + ctt;
    northing[1] = cos(ang1);
    easting[1] = sin(ang1);
    northing[0] = cn1 - r * easting[1];
    easting[0] = ce1 + r * northing[1];
    arc[index]->inputWithDiffs(northing, easting);
  }
}

void reactivePlanner::pickArcs()
{
  int i, j, k, f, a[(LINES_LONG - 1) * POINTS_WIDE],
      a1[(LINES_LONG - 1) * POINTS_WIDE], a2[LINES_LONG], a3[LINES_LONG - 1];
  double cost[LINES_LONG * POINTS_WIDE], temp;
  
  for(k = 0; k < POINTS_WIDE; k++)
  {
    if(fabs(r[k]) < TURNING_RADIUS || arc[k]->isTrajObstructed(m_map, m_layer))
    {
      cost[k] = 1.e10;
    }
    else
    {
    cost[k] = arc[k]->evalCost(m_map, m_layer, d[k]);
    }
  }
  for(i = 0; i < LINES_LONG - 1; i++)
  {
    for(k = 0; k < POINTS_WIDE; k++)
    {
      a[INDEX2a(i, k)] = 0;
      a1[INDEX2a(i, k)] = 0;
      if(fabs(r[INDEX4(i, 0, 0, k)]) < TURNING_RADIUS ||
          arc[INDEX4(i, 0, 0, k)]->isTrajObstructed(m_map, m_layer))
      {
        cost[INDEX2a(i + 1, k)] = cost[INDEX2a(i, 0)] + 1.e10;
      }
      else
      {
        cost[INDEX2a(i + 1, k)] = cost[INDEX2a(i, 0)] + arc[INDEX4(i, 0, 0,
            k)]->evalCost(m_map, m_layer, d[INDEX4(i, 0, 0, k)]);
      }
      for(j = 1; j < POINTS_WIDE; j++)
      {
        if(fabs(r[INDEX4(i, j, 0, k)]) >= TURNING_RADIUS &&
            !arc[INDEX4(i, j, 0, k)]->isTrajObstructed(m_map, m_layer))
        {
          temp = cost[INDEX2a(i, j)] + arc[INDEX4(i, j, 0, k)]->
              evalCost(m_map, m_layer, d[INDEX4(i, j, 0, k)]);
          if(temp < cost[INDEX2a(i + 1, k)])
          {
            a[INDEX2a(i, k)] = j;
            cost[INDEX2a(i + 1, k)] = temp;
          }
        }
      }
      if(i < LINES_DEEP - 1)
      {
        if(fabs(r[INDEX2a(i + 1, k)]) >= TURNING_RADIUS &&
            !arc[INDEX2a(i + 1, k)]->isTrajObstructed(m_map, m_layer))
        {
          temp = arc[INDEX2a(i + 1, k)]->evalCost(m_map, m_layer,
              d[INDEX2a(i + 1, k)]);
          if(temp < cost[INDEX2a(i + 1, k)])
          {
            a1[INDEX2a(i, k)] = i + 1;
            cost[INDEX2a(i + 1, k)] = temp;
          }
        }
        for(f = 1; f < i; f++)
        {
          for(j = 0; j < POINTS_WIDE; j++)
          {
            if(fabs(r[INDEX4(i - f, j, f, k)]) >= TURNING_RADIUS &&
                !arc[INDEX4(i - f, j, f, k)]->isTrajObstructed(m_map, m_layer))
            {
              temp = cost[INDEX2a(i - f, j)] + arc[INDEX4(i - f, j, f, k)]->
                  evalCost(m_map, m_layer, d[INDEX4(i - f, j, f, k)]);
              if(temp < cost[INDEX2a(i + 1, k)])
              {
                a[INDEX2a(i, k)] = j;
                a1[INDEX2a(i, k)] = f;
                cost[INDEX2a(i + 1, k)] = temp;
              }
            }
          }
        }
      }
      else
      {
        for(f = 1; f < LINES_DEEP; f++)
        {
          for(j = 0; j < POINTS_WIDE; j++)
          {
            if(fabs(r[INDEX4(i - f, j, f, k)]) >= TURNING_RADIUS &&
                !arc[INDEX4(i - f, j, f, k)]->isTrajObstructed(m_map, m_layer))
            {
              temp = cost[INDEX2a(i - f, j)] + arc[INDEX4(i - f, j, f, k)]->
                  evalCost(m_map, m_layer, d[INDEX4(i - f, j, f, k)]);
              if(temp < cost[INDEX2a(i + 1, k)])
              {
                a[INDEX2a(i, k)] = j;
                a1[INDEX2a(i, k)] = f;
                cost[INDEX2a(i + 1, k)] = temp;
              }
            }
          }
        }
      }
    }
  }
  a2[0] = 0;
  temp = cost[INDEX2a(LINES_LONG - 1, 0)];
  for(k = 1; k < POINTS_WIDE; k++)
  {
    if(cost[INDEX2a(LINES_LONG - 1, k)] < temp)
    {
      a2[0] = k;
      temp = cost[INDEX2a(LINES_LONG - 1, k)];
    }
  }
  i = LINES_LONG - 2;
  m_g = 1;
  while(i >= 0 && i - a1[INDEX2a(i, a2[m_g - 1])] >= 0)
  {
    a2[m_g] = a[INDEX2a(i, a2[m_g - 1])];
    i -= a1[INDEX2a(i, a2[m_g - 1])] + 1;
    a3[m_g - 1] = i + 1;
    m_g++;
  }
  b[0] = INDEX2a(a3[m_g - 2], a2[m_g - 1]);
  for(i = 1; i < m_g - 1; i++)
  {
    b[i] = INDEX4(a3[m_g - i - 1], a2[m_g - i], a3[m_g - i - 2] -
        a3[m_g - i - 1] - 1, a2[m_g - i - 1]);
  }
  b[m_g - 1] = INDEX4(a3[0], a2[1], LINES_LONG - a3[0] - 2, a2[0]);
}

void reactivePlanner::makeTrajFromArcs()
{
  int i, j, p, q, c;
  double maxV, speed, nextSpeed, speedF[TRAJ_MAX_LEN], speedB[TRAJ_MAX_LEN],
          speedI, speedD, northing[3], easting[3], accel;

  delete traj;
  traj = new CTraj(3);
  p = 0;
  speedF[0] = fmin(m_speed0, m_map->getDataUTM<double>(m_layer, arc[b[0]]->
      getNorthing(0), arc[b[0]]->getEasting(0)));
  speedI = 2 * MAX_ACCEL * d[b[0]];
  maxV = sqrt(fabs(MAX_LAT_ACCEL * r[b[0]]));
  q = arc[b[0]]->getNumPoints();
  for(j = 1; j < q; j++)
  {
    speedF[j] = fmin(fmin(sqrt(speedF[j - 1] * speedF[j - 1] + speedI), maxV),
        m_map->getDataUTM<double>(m_layer, arc[b[0]]->getNorthing(j),
        arc[b[0]]->getEasting(j)));
  }
  p += q;
  for(i = 1; i < m_g; i++)
  {
    speedI = 2 * MAX_ACCEL * d[b[i]];
    maxV = sqrt(fabs(MAX_LAT_ACCEL * r[b[i]]));
    q = arc[b[i]]->getNumPoints();
    for(j = 0; j < q; j++)
    {
      speedF[p + j] = fmin(fmin(sqrt(speedF[p + j - 1] * speedF[p + j - 1] +
          speedI), maxV), m_map->getDataUTM<double>(m_layer, arc[b[i]]->
          getNorthing(j), arc[b[i]]->getEasting(j)));
    }
    p += q;
  }

  q = arc[b[m_g -1]]->getNumPoints();
  speedB[p - 1] = m_map->getDataUTM<double>(m_layer, arc[b[m_g -1]]->
      getNorthing(q - 1), arc[b[m_g -1]]->getEasting(q - 1));
  speedD = -2 * MAX_DECEL * d[b[m_g - 1]];
  maxV = sqrt(fabs(MAX_LAT_ACCEL * r[b[m_g - 1]]));
  for(j = - 2; j >= -q; j--)
  {
    speedB[p + j] = fmin(fmin(sqrt(speedB[p + j + 1] * speedB[p + j + 1] +
        speedD), maxV), m_map->getDataUTM<double>(m_layer,
        arc[b[m_g -1]]->getNorthing(q + j), arc[b[m_g -1]]->
        getEasting(q + j)));
  }
  p -= q;
  for(i = m_g - 2; i >= 0; i--)
  {
    speedD = -2 * MAX_DECEL * d[b[i]];
    maxV = sqrt(fabs(MAX_LAT_ACCEL * r[b[i]]));
    q = arc[b[i]]->getNumPoints();
    for(j = -1; j >= -q; j--)
    {
      speedB[p + j] = fmin(fmin(sqrt(speedB[p + j + 1] * speedB[p + j + 1] +
          speedD), maxV), m_map->getDataUTM<double>(m_layer, arc[b[i]]->
          getNorthing(q + j), arc[b[i]]->getEasting(q + j)));
    }
    p -= q;
  }

  nextSpeed = fmin(speedF[0], speedB[0]);
  for(i = 0; i < m_g - 1; i++)
  {
    for(j = 0; j < m[b[i]]; j++)
    {
      speed = nextSpeed;
      nextSpeed = fmin(speedF[p + j + 1], speedB[p + j + 1]);
      accel = (nextSpeed * nextSpeed - speed * speed) / (2 * d[b[i]]);
      northing[0] = arc[b[i]]->getNorthing(j);
      easting[0] = arc[b[i]]->getEasting(j);
      northing[1] = arc[b[i]]->getNorthingDiff(j, 1) * speed;
      easting[1] = arc[b[i]]->getEastingDiff(j, 1) * speed;
      northing[2] = -easting[1] * speed / r[b[i]] + accel * arc[b[i]]->
          getNorthingDiff(j, 1);
      easting[2] = northing[1] * speed / r[b[i]] + accel * arc[b[i]]->
          getEastingDiff(j, 1);
      traj->inputWithDiffs(northing, easting);
    }
    p += m[b[i]];
    q = m[b[i]];
    for(j = 0; j < n[b[i]]; j++)
    {
      speed = nextSpeed;
      nextSpeed = fmin(speedF[p + j + 1], speedB[p + j + 1]);
      accel = (nextSpeed * nextSpeed - speed * speed) / (2 * d[b[i]]);
      northing[0] = arc[b[i]]->getNorthing(q + j);
      easting[0] = arc[b[i]]->getEasting(q + j);
      northing[1] = arc[b[i]]->getNorthingDiff(q + j, 1) * speed;
      easting[1] = arc[b[i]]->getEastingDiff(q + j, 1) * speed;
      northing[2] = easting[1] * speed / r[b[i]] + accel * arc[b[i]]->
          getNorthingDiff(q + j, 1);
      easting[2] = -northing[1] * speed / r[b[i]] + accel * arc[b[i]]->
          getEastingDiff(q + j, 1);
      traj->inputWithDiffs(northing, easting);
    }
    p += n[b[i]];
  }
  if(n[b[m_g - 1]] <= 0)
  {
    c = m[b[m_g - 1]] - 1;
  }
  else
  {
    c = m[b[m_g - 1]];
  }
  for(j = 0; j < c; j++)
  {
    speed = nextSpeed;
    nextSpeed = fmin(speedF[p + j + 1], speedB[p + j + 1]);
    accel = (nextSpeed * nextSpeed - speed * speed) /
        (2 * d[b[m_g -1]]);
    northing[0] = arc[b[m_g -1]]->getNorthing(j);
    easting[0] = arc[b[m_g -1]]->getEasting(j);
    northing[1] = arc[b[m_g -1]]->getNorthingDiff(j, 1) * speed;
    easting[1] = arc[b[m_g -1]]->getEastingDiff(j, 1) * speed;
    northing[2] = -easting[1] * speed / r[b[m_g - 1]] + accel *
        arc[b[m_g -1]]->getNorthingDiff(j, 1);
    easting[2] = northing[1] * speed / r[b[m_g - 1]] + accel *
        arc[b[m_g -1]]->getEastingDiff(j, 1);
    traj->inputWithDiffs(northing, easting);
  }
  p += m[b[m_g - 1]];
  q = m[b[i]];
  for(j = 0; j < n[b[m_g - 1]] - 1; j++)
  {
    speed = nextSpeed;
    nextSpeed = fmin(speedF[p + j + 1], speedB[p + j + 1]);
    accel = (nextSpeed * nextSpeed - speed * speed) /
        (2 * d[b[m_g - 1]]);
    northing[0] = arc[b[m_g -1]]->getNorthing(q + j);
    easting[0] = arc[b[m_g -1]]->getEasting(q + j);
    northing[1] = arc[b[m_g -1]]->getNorthingDiff(q + j, 1) * speed;
    easting[1] = arc[b[m_g -1]]->getEastingDiff(q +  j, 1) * speed;
    northing[2] = easting[1] * speed / r[b[m_g - 1]] + accel *
        arc[b[m_g -1]]->getNorthingDiff(q + j, 1);
    easting[2] = -northing[1] * speed / r[b[m_g - 1]] + accel *
        arc[b[m_g -1]]->getEastingDiff(q + j, 1);
    traj->inputWithDiffs(northing, easting);
  }
  q = arc[b[m_g - 1]]->getNumPoints() - 1;
  speed = nextSpeed;
  northing[0] = arc[b[m_g -1]]->getNorthing(q);
  easting[0] = arc[b[m_g -1]]->getEasting(q);
  northing[1] = arc[b[m_g -1]]->getNorthingDiff(q, 1) * speed;
  easting[1] = arc[b[m_g -1]]->getEastingDiff(q, 1) * speed;
  if(n[b[m_g - 1]] > 0)
  {
    northing[2] = easting[1] * speed / r[b[m_g - 1]];
    easting[2] = -northing[1] * speed / r[b[m_g - 1]];
  }
  else
  {
    northing[2] = -easting[1] * speed / r[b[m_g - 1]];
    easting[2] = northing[1] * speed / r[b[m_g - 1]];
  }
  traj->inputWithDiffs(northing, easting);
}

void reactivePlanner::blurMap()
{
  int i, j, k, f, r, c, n, e;

  n = (int)(BLUR_SIZE / m_map->getResRows() + 0.5);
  e = (int)(BLUR_SIZE / m_map->getResCols() + 0.5);
  if(n > 0 || e > 0)
  {
    double goodness, goodTmp[MAX_MAP_SIZE][MAX_MAP_SIZE];

    r = m_map->getNumRows() - n;
    c = m_map->getNumCols() - e;

    for(i = n; i < r; i++)
      for(j = e; j < c; j++)
      {
        goodness = m_map->getDataWin<double>(m_layer, i, j);
        if(goodness >= 0.01)
          for(k = i - n; k <= i + n; k++)
            for(f = j - e; f <= j + e; f++)
              goodness = fmin(goodness, m_map->
                  getDataWin<double>(m_layer, k, f));
        goodTmp[i][j] = goodness;
      }
    for(i = n; i < r; i++)
      for(j = e; j < c; j++)
        m_map->setDataWin<double>(m_layer, i, j, goodTmp[i][j]);
  }
}
