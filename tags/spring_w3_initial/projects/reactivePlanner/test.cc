#include <fstream>
#include <iostream>
#include <sys/time.h>
#include "frames/coords.hh"
#include "trajMap.hh"
#include "rddf.hh"
#include "CCorridorPainter.hh"
#include "reactivePlanner.hh"
using namespace std;

int main()
{
  timeval t0, t1, t2, t3;

  gettimeofday(&t0, NULL);

  reactivePlanner rP;
  int layer;
  CTraj *traj = new CTraj(3);
  RDDF rddf("rddf.dat");
  NEcoord initialPoint(rddf.getWaypointNorthing(0), rddf.getWaypointEasting(0));
  CMap map;
  CCorridorPainter paint;
  ofstream outfile("output.traj");

  map.initMap(0., 0., 200, 200, 1., 1., 0);
  layer = map.addLayer<double>(1e-3, 1e-4);
  paint.initPainter(&map, layer, &rddf, 1., 1e-3, 10., 7., 0.25, 0);
  map.updateVehicleLoc(initialPoint.N, initialPoint.E);
  NEcoord exposedRowBox[4];
  NEcoord exposedColBox[4];
  map.getExposedRowBoxUTM(exposedRowBox);
  map.getExposedColBoxUTM(exposedColBox);
  paint.paintChanges(exposedRowBox, exposedColBox);
  gettimeofday(&t1, NULL);

  traj = rP.plan(&rddf, map, layer, initialPoint, rddf.getTrackLineYaw(0), 1.e10);

  gettimeofday(&t2, NULL);

  traj->print(outfile);

  gettimeofday(&t3, NULL);

  cout << "Init time: " << t1.tv_sec - t0.tv_sec +
          (double)(t1.tv_usec - t0.tv_usec) / 1000000 << " s" << endl;
  cout << "Run time: " << t2.tv_sec - t1.tv_sec +
          (double)(t2.tv_usec - t1.tv_usec) / 1000000 << " s" << endl;
  cout << "Output time: " << t3.tv_sec - t2.tv_sec +
          (double)(t3.tv_usec - t2.tv_usec) / 1000000 << " s" << endl;
  cout << "Total time: " << t3.tv_sec - t0.tv_sec +
          (double)(t3.tv_usec - t0.tv_usec) / 1000000 << " s" << endl;
  return 0;
}
