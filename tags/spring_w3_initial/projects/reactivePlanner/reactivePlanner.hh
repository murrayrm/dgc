#include <math.h>
#include <sys/time.h>
#include "frames/coords.hh"
#include "trajMap.hh"
#include "rddf.hh"
#include "CCorridorPainter.hh"
#include "AliceConstants.h"
using namespace std;

#ifndef _REACTIVEPLANNER_HH_
#define _REACTIVEPLANNER_HH_

#define INTERNAL_TRAJ_SPACING 0.5 //  m
//#define OUTPUT_TRAJ_SPACING 1.    //  m
#define POINTS_WIDE 7
#define LINES_LONG 6
#define LINES_DEEP 3
#define DISTANCE_BETWEEN_LINES 12.  //  m/s

#define BLUR_SIZE 1.  //  m
#define MAX_MAP_SIZE 200

#define MAX_ACCEL 2.      //  m/s^2
#define MAX_DECEL (-3.)   //  m/s^2
#define MAX_LAT_ACCEL 3.  //  m/s^2
#define TURNING_RADIUS 8. //  m

#define INDEX2(i, j) (POINTS_WIDE * (i) + (j) + 1)
#define INDEX2a(i, j) (POINTS_WIDE * (i) + (j))
/*#define INDEX3(i, j, k) (POINTS_WIDE * POINTS_WIDE * (i) + POINTS_WIDE * (j) +\
    (k) + POINTS_WIDE)*/
#define INDEX4(i, j, f, k) (LINES_DEEP * POINTS_WIDE * POINTS_WIDE * (i) +\
    LINES_DEEP * POINTS_WIDE * (j) + POINTS_WIDE * (f) + (k) + LINES_DEEP *\
    POINTS_WIDE)

class reactivePlanner
{
  private:
    int m[LINES_DEEP * ((LINES_LONG - 1) * POINTS_WIDE * POINTS_WIDE +
        POINTS_WIDE)], n[LINES_DEEP * ((LINES_LONG - 1) * POINTS_WIDE *
        POINTS_WIDE + POINTS_WIDE)], b[LINES_LONG], m_layer, m_g;
    NEcoord points[LINES_LONG * POINTS_WIDE + 1];
    double m_speed0, theta[LINES_LONG + 1], r[LINES_DEEP * ((LINES_LONG - 1) *
        POINTS_WIDE * POINTS_WIDE + POINTS_WIDE)], d[LINES_DEEP * ((LINES_LONG -
        1) * POINTS_WIDE * POINTS_WIDE + POINTS_WIDE)];
    CTrajMap *arc[LINES_DEEP * ((LINES_LONG - 1) * POINTS_WIDE * POINTS_WIDE +
                  POINTS_WIDE)];
    CTraj *traj;
    RDDF *m_rddf;
    CMap *m_map;

    /*! Produces lines of points perpendicular to the RDDF track line.
        Also gives the heading of points in each line*/
    void pickPoints();

    void makeBundles();
    /*! Returns a pointer to a traj made up of 2 circular arcs that smoothly
        connect two unit vectors.  Also, gives the radius of the arcs and the
        arc length between points.*/
    void makeArc(int index, NEcoord p0, double theta0, NEcoord p1, double theta1,
                      double &r, double &d, int &m, int &n);

    /*! Returns a pointer to a traj made up of 2 circular arcs that smoothly
        connect two unit vectors.  Also, gives the radius of the arcs and the
        arc length between points.*/
    void makeArc(int index, double x0, double y0, double theta0, double x1, double y1,
                      double theta1, double &r, double &d, int &m, int &n);

    void pickArcs();

    void makeTrajFromArcs();
    
    void blurMap();
  public:
    reactivePlanner();

    ~reactivePlanner();

    CTraj *reactivePlanner::plan(RDDF *rddf, CMap map, int layer,
            NEcoord &initialPoint, double initialTheta, double initialSpeed);
};

#endif
