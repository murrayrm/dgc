#include <unistd.h>
#include <iostream>

#include "reactiveModule.hh"
#include "reactivePlanner.hh"
#include "DGCutils"

int QUIT_PRESSED = 0;

extern int           NO_SPARROW_DISP;
extern int           NOSTATE;
extern int           NOPLAN;
extern int           NOMM;
extern int           SIMPLEUPDATE;
extern unsigned long SLEEP_TIME;
extern int					 STATICMAP;

#define MAX_DELTA_SIZE							250000
#define MIN_PLANNING_CYCLETIME_US		500000
#define PLANNING_LOOKAHEAD					0.
#define PROXIMITY_TO_TRAJ_TO_LOOKAHEAD 2.5

//#define NO_LOOKAHEAD

reactiveModule::reactiveModule(int sn_key)
	: CSkynetContainer(MODreactive, sn_key), m_rddf("rddf.dat")
{
	m_pMapDelta = new char[MAX_DELTA_SIZE];

#warning "these shouldn't be hard coded"
	if(STATICMAP == 1)
	{
		m_map.initMap(0.0 ,0.0, 2500, 1000, 0.2, 0.2, 0 );
		// Add the speed limit layer to the map
		m_mapLayer = m_map.addLayer<double>(-3.0, -2.0);
		m_map.loadLayer<double>(m_mapLayer, "cmap", true);
	}
	else
	{
		m_map.initMap(0.0, 0.0, 150, 150, 1.0, 1.0, 0 );
		// Add the speed limit layer to the map
		m_mapLayer = m_map.addLayer<double>(1e-3, 1e-4);
	}
	DGCcreateMutex(&m_mapMutex);
}

reactiveModule::~reactiveModule() 
{
	delete m_pMapDelta;
	DGCdeleteMutex(&m_mapMutex);
}

void reactiveModule::getMapDeltasThread()
{
  ofstream delta_log("logs/delta.log");
  delta_log << "# reactiveModule produced this.  Time then size." << endl;
  unsigned long long now;
      
  // The skynet socket for receiving map deltas (from fusionmapper)
	int mapDeltaSocket = m_skynet.listen(SNfusiondeltamap, SNfusionmapper);
	if(mapDeltaSocket < 0)
		cerr << "reactiveModule::getMapDeltasThread(): skynet listen returned error" << endl;

	int mapDeltaLeft;

	while(true)
	{
		int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pMapDelta, MAX_DELTA_SIZE, 0);

		DGClockMutex(&m_mapMutex);
		cerr << "Received mapdelta" << endl;
		m_map.applyDelta<double>(m_mapLayer, m_pMapDelta, numreceived);
		cerr << "Applied mapdelta" << endl;
		DGCunlockMutex(&m_mapMutex);

    DGCgettime(now);
    delta_log << now << " " << numreceived << endl;
	}
}

void reactiveModule::planningLoop(void) 
{
#warning "same map used here, even though two physical ones exist"
  reactivePlanner rP;
	int trajSocket;
	CTraj *traj = new CTraj(3);
	NEcoord pos;
	unsigned long long time1, time2;

	DGCgettime(time1);

	bool bSentTraj = false;

	if (NOMM)
		trajSocket     = m_skynet.get_send_sock(SNtraj);
	else
		trajSocket     = m_skynet.get_send_sock(SNreactiveTraj);


  while( !QUIT_PRESSED )
	{
		if(!NOSTATE) 
		{
		  UpdateState();
		}

#ifdef NO_LOOKAHEAD
		DGClockMutex(&m_mapMutex);
		cout << "starting to plan" << endl;
		pos = NEcoord(m_state.Northing, m_state.Easting);
		traj = rP.plan(&m_rddf, m_map, m_mapLayer, pos, m_state.Yaw, m_state.Speed2());
		cout << "finished plan" << endl;
		DGCunlockMutex(&m_mapMutex);
#else
		if(!bSentTraj)
		{
			DGClockMutex(&m_mapMutex);
			cout << "starting to plan" << endl;
		  pos = NEcoord(m_state.Northing, m_state.Easting);
		  traj = rP.plan(&m_rddf, m_map, m_mapLayer, pos, m_state.Yaw, m_state.Speed2());
			cout << "finished plan" << endl;
			DGCunlockMutex(&m_mapMutex);
		}
		else
		{
			VehicleState lookaheadState = m_state;
			double lookaheadDist = PLANNING_LOOKAHEAD * m_state.Speed2();
			int closestpoint = traj->getClosestPoint(m_state.Northing, m_state.Easting);
			if( hypot(traj->getNorthing(closestpoint) - m_state.Northing,
								traj->getEasting(closestpoint) - m_state.Easting) <
								PROXIMITY_TO_TRAJ_TO_LOOKAHEAD)
			{
				closestpoint += lround( lookaheadDist / (LINES_LONG * 
				    DISTANCE_BETWEEN_LINES) * (double)traj->getNumPoints());

				if(closestpoint >= traj->getNumPoints())
            closestpoint = traj->getNumPoints()-1;

				lookaheadState.Northing = traj->getNorthingDiff(closestpoint, 0);
				lookaheadState.Easting  = traj->getEastingDiff (closestpoint, 0);
				lookaheadState.Vel_N    = traj->getNorthingDiff(closestpoint, 1);
				lookaheadState.Vel_E    = traj->getEastingDiff (closestpoint, 1);
				lookaheadState.Acc_N    = traj->getNorthingDiff(closestpoint, 2);
				lookaheadState.Acc_E    = traj->getEastingDiff (closestpoint, 2);
				lookaheadState.Yaw      = atan2(lookaheadState.Vel_E, lookaheadState.Vel_N);
				lookaheadState.YawRate  = (lookaheadState.Vel_N*lookaheadState.Acc_E -
																	 lookaheadState.Acc_N*lookaheadState.Vel_E) /
																	 lookaheadState.Speed2()/lookaheadState.Speed2();
			}

			DGClockMutex(&m_mapMutex);
			cout << "starting to plan" << endl;
		  pos = NEcoord(lookaheadState.Northing, lookaheadState.Easting);
		  traj = rP.plan(&m_rddf, m_map, m_mapLayer, pos, lookaheadState.Yaw,
		      lookaheadState.Speed2());
			cout << "finished plan" << endl;
			DGCunlockMutex(&m_mapMutex);
		}
#endif

			cerr << "Succeeded!" << endl;
			SendTraj(trajSocket, traj);

			double n   = traj->getNorthingDiff(0, 0);
			double nd  = traj->getNorthingDiff(0, 1);
			double ndd = traj->getNorthingDiff(0, 2);
			double e   = traj->getEastingDiff (0, 0);
			double ed  = traj->getEastingDiff (0, 1);
			double edd = traj->getEastingDiff (0, 2);
			cerr <<   "desired a: "   << (nd*ndd + ed*edd)/hypot(nd,ed)
					 << ", desired phi: " << asin( VEHICLE_WHEELBASE*(nd*edd - ndd*ed)/
					                               pow(nd*nd+ed*ed, 1.5) ) << endl;

			bSentTraj = true;

			DGCgettime(time2);
			if(time2 - time1 < MIN_PLANNING_CYCLETIME_US)
				usleep(MIN_PLANNING_CYCLETIME_US - (time2 - time1));
			DGCgettime(time1);

	}
}
