//author: Jeff Lamb

//#include <math.h>
//#include <sys/time.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include "waypoints.h"
#include "lateral.h"


//double t_startNorthing;
//double t_startEasting;
//double t_endNorthing;
//double t_endEasting;
//double t_currentNorthing;
//double t_currentEasting;
double path_heading;
double ninety_degrees = 3.141592654/2;
double my_pi = 3.141592654;
//double distance;
//double error;
double headingError_p;     //proportional heading error
double lastHeadingError_p; //previous proportional heading error
double lateralError_p;     //proportinal lateral error
double lastLateralError_p;  //previous lateral error
double lateralIntegral;
double headingIntegral;
double lateralDerivative;
double headingDerivative;
double time_new;
double time_old;
//double time_interval;
double lateralCommand;
double headingCommand;
int currentPathSegment = 0;
double distPoint_to_Line(double startWaypointNorthing,
			 double startWaypointEasting,
			 double endWaypointNorthing,
			 double endWaypointEasting,
			 double currentNorthing,
			 double currentEasting) {
  double distance;
  //translate to a coordinate system w/ the start waypoint at the origin
  double t_startNorthing = 0;
  double t_startEasting = 0;
  double t_endNorthing = endWaypointNorthing - startWaypointNorthing;
  double t_endEasting = endWaypointEasting - startWaypointEasting;
  double t_currentNorthing = currentNorthing - startWaypointNorthing;
  double t_currentEasting = currentEasting - startWaypointEasting;
  
  cout <<"start waypoint Northing = " << t_endNorthing << endl;
  cout <<"start waypoint Easting  = " << t_endEasting << endl;
  cout <<"t_currentNorthing = " <<t_currentNorthing << endl;
  cout <<"t_currentEasting = " << t_currentEasting << endl;
  //find angle of heading of path wrt to x axis 
  path_heading = atan2(t_endNorthing, t_endEasting);
  cout << "path heading = " << path_heading << endl;
  //Rotate the coordinate system to force path to x axis
  distance = t_currentEasting * (-sin(path_heading)) + 
             t_currentNorthing * cos(path_heading);
  cout << "lateral distance = " << distance << endl;
  return distance;
}

double heading_error(double currentHeading){
  double error = currentHeading - path_heading;
  //normalize to be in [-pi, pi] clockwise from North (y axis)
  if (error > my_pi) error = error - 2.0*my_pi;
  if (error < -my_pi) error = error + 2.0*my_pi;
  return error;
}


double lateral_controller(double currentNorthing,
			  double currentEasting,
			  double currentHeading){
  timeval lateralTime;
  double steer_angle_cmd;
  int nextWaypoint = currentPathSegment + 1;
  if((double)fabs(currentNorthing - waypoints_lateralq[nextWaypoint][NORTHING]) < 10 &&
     (double)fabs(currentEasting - waypoints_lateralq[nextWaypoint][EASTING]) < 10)
    currentPathSegment++;
  
  int startWaypoint = currentPathSegment;
  nextWaypoint = currentPathSegment + 1;
  lateralError_p = distPoint_to_Line(waypoints_lateralq[startWaypoint][NORTHING],
			       waypoints_lateralq[startWaypoint][EASTING],
			       waypoints_lateralq[nextWaypoint][NORTHING],
			       waypoints_lateralq[nextWaypoint][EASTING],
			       currentNorthing,
			       currentEasting);
  headingError_p = heading_error(currentHeading);
  gettimeofday(&lateralTime, NULL);  //get current time
  double secs = (double)lateralTime.tv_sec;
  double usecs = (double)lateralTime.tv_usec;
  time_new = secs+usecs/1000000;
  double time_interval = time_new - time_old;  //amount of time passed since last cycle
  
  //derivative:
  headingDerivative = (headingError_p - lastHeadingError_p) / time_interval;
  lateralDerivative = (lateralError_p - lastLateralError_p) / time_interval;

  //integrator:
  headingIntegral += (headingError_p + lastHeadingError_p) * time_interval/2;
  lateralIntegral += (lateralError_p + lastLateralError_p) * time_interval/2;
  if(lateralIntegral > LATERAL_INTEGRATOR_SAT) lateralIntegral = LATERAL_INTEGRATOR_SAT;
  if(headingIntegral > HEADING_INTEGRATOR_SAT) headingIntegral = HEADING_INTEGRATOR_SAT;

  //commanded angles, normalized from -1 (left) to 1 (right)
  lateralCommand = (LATERAL_PROPORTIONAL_GAIN * lateralError_p +
		    LATERAL_INTEGRATOR_GAIN * lateralIntegral +
		    LATERAL_DERIVATIVE_GAIN * lateralDerivative);
  cout << "lateral command" << lateralCommand << endl;
  if(lateralCommand > 1) lateralCommand = 1;
  if(lateralCommand < -1) lateralCommand = -1;

  headingCommand = (HEADING_PROPORTIONAL_GAIN * headingError_p +
		    HEADING_INTEGRATOR_GAIN * headingIntegral +
		    HEADING_DERIVATIVE_GAIN * headingDerivative);
  if(headingCommand > 1) headingCommand = 1;
  if(headingCommand < -1) headingCommand = -1;
  
  //resolution of heading and lateral conflicts
  steer_angle_cmd = lateralCommand;
  cout << "steering angle " << steer_angle_cmd << endl;
  if(steer_angle_cmd > 1) steer_angle_cmd = 1;
  if(steer_angle_cmd < -1) steer_angle_cmd = 1;
  
  //update values for next cycle and return
  lastHeadingError_p = headingError_p;
  lastLateralError_p = lateralError_p;
  time_old = time_new;
  return steer_angle_cmd;
}
