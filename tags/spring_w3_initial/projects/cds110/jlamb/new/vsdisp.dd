/*
 * overview.h - overview display screen for Tahoe
 *
 * RMM, 23 Apr 95
 *
 */

/* Local function declarations */
static int user_quit(long);
static int user_log_inc(long);
static int user_gps_toggle(long);
extern int mta_counter;
%%

%TITLE	    		I: %imuc  G: %gpsc  M: %magc MTA: %mtac
			L: %logflag O: %obdc
Axis   IMU   GPS/MAG           KF         | Status Indicators
 X  %imdx   %gpx    vn:%gpvn   %imax      | GPS Nav Valid: %gnav  
 Y  %imdy   %gpy    ve:%gpve   %imay      | GPS Active: %gpsact 
 Z  %imdz   %gpz    vu:%gpvu   %imaz      | GPS State: %gpsstate
 R  %imdr   %magr   s: %gpvv   %imar      | Vert Vel Inhib: %vertvel
 P  %imdp   %magp              %imap      | Inval: %inval
 Y  %imdw   %magy              %imaw      | GotVal: %gotval  
 V  %imdv   %gpdv              %imadv     gla: %glat   gln: %glng 
OBD: %obdv     KFLat:%kflat    KFLng:%kflng    KF Alt: %actalt
GPS Nav Mode: %gnavmode                %oflag OBD Valid: %oval
KF/IMU Scale factors, biases, and residuals: 	Mag KF:		
ABX	%abx    	RPX: %rspx		PK1: %mpk1
ABY	%aby		RPY: %rspy		PK: %mpk
ABZ	%abz		RPZ: %rspz		BK: %mbk		
GBX	%gbx		RVX: %rsvx		ERR: %merr
GSFX	%gsfx		RVY: %rsvy		YAW: %myaw
			RVZ: %rsvz		YK: %myk

%QUIT   %LOG		

%%
tblname: overview;
bufname: ovwbuf;

double: %glat gps_lat "%3.4f" -ro;
double: %glng gps_lon "%3.4f" -ro;

short: %oflag obdflag "%5d" -ro;
short: %oval obd_valid "%5d" -ro;
short: %gotval gps_got_valid "%5d" -ro;

double: %gpvv gps_speed "%5.2f" -ro;
double: %actalt vehstate.actual_Altitude "%5.2f" -ro;
short: %vertvel inhib_vert_vel "%d" -ro;
short: %gpsstate gps_nav_state "%d" -ro;
short: %inval gps_invalid_count "%d" -ro;

label: %TITLE "vstate 2.1j" -fg=YELLOW;
short: %imuc imu_count "%5d" -ro;
short: %gpsc gps_count "%5d" -ro;
short: %magc mag_count "%5d" -ro;
short: %mtac mta_counter "%5d" -ro;
short: %logflag logflag "%d" -ro;

button:	%QUIT	"QUIT"	user_quit	-idname=QUITB;
button: %LOG	"Log" user_log_inc;

float: %magy magreading.heading "%5.2f" -ro;
float: %magp magreading.pitch "%5.2f" -ro;
float: %magr magreading.roll "%5.2f" -ro;

double: %gpx gpsdata.data.lat "%3.2f" -ro;
double: %gpy gpsdata.data.lng "%3.2f" -ro;
float: %gpz gpsdata.data.altitude "%3.2f" -ro;
float: %gpdv vehstate.Speed "%5.2f" -ro;
short: %gnav gps_valid "%d" -ro;
string: %gnavmode gps_nav_msg "%s" -idname=NAVMODE;
short: %gpsact gps_active "%d" -ro;
float: %gpve gpsdata.data.vel_e "%5.2f" -ro;
float: %gpvn gpsdata.data.vel_n "%5.2f" -ro;
float: %gpvu gpsdata.data.vel_u "%5.2f" -ro;

double: %obdv obd_vel "%5.2f" -ro;
double: %magoff mag_offset "%5.2f";

double: %imdx imudata.dvx "%7.4f" -ro;
double: %imdy imudata.dvy "%7.4f" -ro;
double: %imdz imudata.dvz "%7.4f" -ro;
double: %imdr imudata.dtx  "%7.4f" -ro;
double: %imdp imudata.dty  "%7.4f" -ro;
double: %imdw imudata.dtz  "%7.4f" -ro;

double: %imax vehstate.Northing "%7.1f" -ro;
double: %imay vehstate.Easting "%7.1f" -ro;
float: %imaz vehstate.Altitude  "%7.1f" -ro;
float: %imap vehstate.Pitch "%2.4f" -ro;
float: %imar vehstate.Roll "%2.4f" -ro;
float: %imaw vehstate.Yaw "%2.4f" -ro;
float: %imadv vehstate.Speed "%5.2f" -ro;
double: %kflat vehstate.kf_lat "%7.4f" -ro;
double: %kflng vehstate.kf_lng "%7.4f" -ro;

double: %abx tot_kabx "%7.6f" -ro;
double: %aby tot_kaby "%7.6f" -ro;
double: %abz tot_kabz "%7.6f" -ro;
double: %gbx tot_kgbx "%7.6f" -ro;
double: %gbx tot_kgby "%7.6f" -ro;
double: %gbx tot_kgbz "%7.6f" -ro;
double: %gsfx tot_kgsfx "%7.6f" -ro;

double: %rspx ExtGPSPosMeas_x "%7.4f" -ro;
double: %rspy ExtGPSPosMeas_y "%7.4f" -ro;
double: %rspz ExtGPSPosMeas_z "%7.4f" -ro;
double: %rsvx ExtGPSVelMeas_x "%7.4f" -ro;
double: %rsvy ExtGPSVelMeas_y "%7.4f" -ro;
double: %rsvz ExtGPSVelMeas_z "%7.4f" -ro;

double: %mpk mag_pk "%5.2f" -ro;
double: %mbk mag_bk "%5.2f" -ro;
double: %merr mag_yaw_err "%5.2f" -ro;
double: %myaw mag_est "%5.2f" -ro;
double: %myk yk "%5.2f" -ro;
double: %mpk1 mag_pk1 "%5.2f" -ro; 

short: %obdc obd_count "%d" -ro;

