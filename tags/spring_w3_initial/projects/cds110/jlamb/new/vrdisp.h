/*
 * vrdisp.h - vremote display screen for Tahoe
 *
 * RMM, 30 Dec 03
 *
 */

/* Local function declarations */
int user_quit(long);
int user_cap_toggle(long);

extern DD_IDENT parmtbl[];	/* Parameter table */
extern int steer_cmdangle;	/* Commanded steering angle (steer.c) */
extern double brk_current_abs_pos_buf, brk_current_acc_buf, brk_current_vel_buf;
extern double brk_current_brake_pot, brk_pot_voltage;
extern int brk_case;
extern int brk_count;

/* Object ID's (offset into table) */
#define QUITB	78

/* Allocate space for buffer storage */
char vddbuf[244];

DD_IDENT vddisp[] = {
{1, 1, (void *)("VREMOTE 2.1a"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 25, (void *)("J:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 35, (void *)("S:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 45, (void *)("A:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 55, (void *)("M:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 1, (void *)("Mode: D="), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 13, (void *)("S="), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{4, 10, (void *)("Off    Ref   Act   Gain  Cmd"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{4, 47, (void *)("| Joy:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 1, (void *)("Drive"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 47, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{6, 1, (void *)("Steer"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{6, 47, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 1, (void *)("Axis   IMU"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 17, (void *)("GPS"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 26, (void *)("|  KF     Vel     | Configuration files:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 2, (void *)("X/N"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 26, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 44, (void *)("|  Chn config: vdrive.dev"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{10, 2, (void *)("Y/E"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{10, 26, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{10, 44, (void *)("|  Dump file"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{11, 2, (void *)("Z/U"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{11, 26, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{11, 44, (void *)("|  Brake: pos:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{11, 67, (void *)("vel:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{12, 2, (void *)("R"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{12, 17, (void *)("-------  |"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{12, 44, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{12, 54, (void *)("cur:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{12, 67, (void *)("cas:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 2, (void *)("P"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 17, (void *)("M"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 26, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 44, (void *)("|  estop:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{14, 2, (void *)("Y"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{14, 26, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{14, 44, (void *)("|  Do:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{15, 44, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{17, 1, (void *)("Drivers"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{17, 13, (void *)("Flag    Cmd     Cal    Zero"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{17, 42, (void *)("Functions   Flag"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{18, 3, (void *)("Brake"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{18, 44, (void *)("Cruise"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{19, 3, (void *)("Throttle"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{19, 42, (void *)("Modes"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{20, 3, (void *)("Steer"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{20, 44, (void *)("O = off     M = manual (human)"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{21, 44, (void *)("R = remote  A = autonomous (MTA)"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 58, (void *)(&(mta_counter)), dd_short, "%5d", vddbuf+0, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "mta_counter", -1},
{4, 55, (void *)(&(vd.joystick.X)), dd_double, "%5.0f", vddbuf+8, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vd.joystick.X", -1},
{4, 60, (void *)(&(vd.joystick.Y)), dd_double, "%5.0f", vddbuf+16, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vd.joystick.Y", -1},
{4, 67, (void *)(&(vd.joystick.Buttons)), dd_short, "%d", vddbuf+24, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vd.joystick.Buttons", -1},
{9, 17, (void *)(&(vehstate.gpsdata.lat)), dd_double, "%7.4f", vddbuf+32, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.gpsdata.lat", -1},
{10, 17, (void *)(&(vehstate.gpsdata.lng)), dd_double, "%7.4f", vddbuf+40, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.gpsdata.lng", -1},
{11, 17, (void *)(&(vehstate.gpsdata.altitude)), dd_double, "%7.4f", vddbuf+48, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.gpsdata.altitude", -1},
{9, 8, (void *)(&(vehstate.imudata.dvx)), dd_double, "%7.4f", vddbuf+56, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.imudata.dvx", -1},
{10, 8, (void *)(&(vehstate.imudata.dvy)), dd_double, "%7.4f", vddbuf+64, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.imudata.dvy", -1},
{11, 8, (void *)(&(vehstate.imudata.dvz)), dd_double, "%7.4f", vddbuf+72, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.imudata.dvz", -1},
{12, 8, (void *)(&(vehstate.imudata.dtx)), dd_double, "%7.4f", vddbuf+80, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.imudata.dtx", -1},
{13, 8, (void *)(&(vehstate.imudata.dty)), dd_double, "%7.4f", vddbuf+88, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.imudata.dty", -1},
{14, 8, (void *)(&(vehstate.imudata.dtz)), dd_double, "%7.4f", vddbuf+96, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.imudata.dtz", -1},
{9, 27, (void *)(&(vehstate.kf_lat)), dd_double, "%7.2f", vddbuf+104, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.kf_lat", -1},
{10, 27, (void *)(&(vehstate.kf_lng)), dd_double, "%7.2f", vddbuf+112, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.kf_lng", -1},
{11, 27, (void *)(&(vehstate.Altitude)), dd_float, "%7.2f", vddbuf+120, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.Altitude", -1},
{9, 36, (void *)(&(vehstate.Vel_N)), dd_float, "%5.2f", vddbuf+128, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.Vel_N", -1},
{10, 36, (void *)(&(vehstate.Vel_E)), dd_float, "%5.2f", vddbuf+136, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.Vel_E", -1},
{11, 36, (void *)(&(vehstate.Vel_U)), dd_float, "%5.2f", vddbuf+144, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.Vel_U", -1},
{12, 27, (void *)(&(vehstate.Roll)), dd_float, "%7.2f", vddbuf+152, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.Roll", -1},
{13, 27, (void *)(&(vehstate.Pitch)), dd_float, "%7.2f", vddbuf+160, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.Pitch", -1},
{14, 27, (void *)(&(vehstate.Yaw)), dd_float, "%7.2f", vddbuf+168, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.Yaw", -1},
{13, 19, (void *)(&(vehstate.magreading.heading)), dd_float, "%5.2f", vddbuf+176, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vehstate.magreading.heading", -1},
{18, 13, (void *)(&(vd.brake.BrakeState)), dd_short, "%1d", vddbuf+184, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vd.brake.BrakeState", -1},
{18, 21, (void *)(&(vd.brake.Position)), dd_double, "%5.2f", vddbuf+192, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vd.brake.Position", -1},
{19, 13, (void *)(&(vd.throttle.ThrottleState)), dd_short, "%1d", vddbuf+200, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vd.throttle.ThrottleState", -1},
{19, 21, (void *)(&(vd.throttle.Position)), dd_double, "%5.2f", vddbuf+208, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vd.throttle.Position", -1},
{20, 13, (void *)(&(vd.steer.SteerState)), dd_short, "%1d", vddbuf+216, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vd.steer.SteerState", -1},
{20, 21, (void *)(&(vd.steer.Steer)), dd_double, "%5.2f", vddbuf+224, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vd.steer.Steer", -1},
{22, 1, (void *)("QUIT"), dd_label, "NULL", (char *) NULL, 1, user_quit, (long)(long) 0, 0, 0, Button, "", -1},
{22, 9, (void *)("PARAM"), dd_label, "NULL", (char *) NULL, 1, dd_usetbl_cb, (long)parmtbl, 0, 0, Button, "", -1},
{2, 15, (void *)(&(vd.mode_steer)), vdrive_mode, "%c", vddbuf+232, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vd.mode_steer", -1},
{2, 9, (void *)(&(vd.mode_drive)), vdrive_mode, "%c", vddbuf+240, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "vd.mode_drive", -1},
DD_End};
