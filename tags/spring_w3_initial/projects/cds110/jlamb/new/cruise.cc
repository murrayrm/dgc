/*
 * cruise.cc - velocity control computation
 *
 * Version 1.0
 * 11/13/03 Haomiao Huang
 * File Creation
 *
 * 11/14/03 Haomiao Huang
 * Added force lookup control
 *
 */

#include "cruise.h"

#define USEFILES

#ifdef USEFILES
#define V_TO_A_FILENAME "v_to_a_lookup"
#define A_TO_T_FILENAME "a_to_t_lookup"
#define V_TO_T_FILENAME "v_to_t_lookup"

#ifndef CRUISE_OLD
#define CTR_TO_T_FILENAME "ctr_to_t_PID"
#endif

#ifdef CRUISE_OLD
#define CTR_TO_T_FILENAME "ctr_to_t_lookup"
#endif


LUtable v_to_a_Table(V_TO_A_FILENAME);
LUtable a_to_t_Table(A_TO_T_FILENAME);
LUtable v_to_t_Table(V_TO_T_FILENAME);
LUtable ctr_to_t_Table(CTR_TO_T_FILENAME);
#endif

//This file contains code for two different cruise controllers.  select
//between them by the definition or lack of CRUISE_OLD.
//If CRUISE_OLD is not set in cruise.h, CRUISE_NEW is the default set of 
//instructions.  Shared functions appear at the end of this file.

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#ifndef CRUISE_OLD


/*cruise new uses a PID controller coupled with input from pitch and roll data
  to command the minimum of the commanded velocity, an internal maximum 
  velocity, and a velocity that sets save limits on vehicle pitch and roll 
  rate */


////////////////////////////////////////////////////////////////////////////////
//libraries
#include <time.h>
#include "VState.hh"


////////////////////////////////////////////////////////////////////////////////
//external variables
extern struct VState_GetStateMsg vehstate;	/* current vehicle state */


////////////////////////////////////////////////////////////////////////////////
//constant definitions
#define vel_cruise_max 25 //max velocity the cruise controller will use in m/s


double G_vel_p = DEFAULT_P;    	// Proportional velocity error gain
double G_vel_d = DEFAULT_D;    	// Derivative velocity error gain


double G_vel_i = DEFAULT_I;    	// Integral velocity error gain
double G_vel_i_pos = DEFAULT_I_Pos;
double G_vel_i_neg = DEFAULT_I_Neg;

#define integrator_sat 10.0

#define velocity_timeout 10.0   // if we have real old time values, reset integral, derivatve

#define G_roll_p 20		// proportional roll rate error gain
#define G_roll_i 0.0		// integral roll rate error gain
#define roll_harsh_thresh 1	// Roll rate to seek

#define G_pitch_p 20		// proportional pitch rate error gain
#define G_pitch_i 0.0		// integral pitch rate error gain
#define pitch_harsh_thresh 1	// pitch rate to seek

#define terrain_timeout 60 // reset terrain values if we have a long delay

#define throttle_maximum 1  //maximum throttle to send
#define throttle_minimum -1 //maximum brake to send

using namespace std;


////////////////////////////////////////////////////////////////////////////////
// Functions

////////////////////////////////////////////////////////////
//cruise
//
//the new cruise controller takes three velocities, and controlls to their
//minimum.  the velocities are from vdrive (vel_vdrive), from the roll and
//pitch controller (vel_terrain_max), and the internally set vel_cruise_max.
double cruise(double commanded_vel) {  
  double throttle;
  double vel_terrain_max;
  
  //find out what our terrain says we can go
  vel_terrain_max = vel_cruise_max; // cruise_terrain();  //commented out in order
  //to test without terrain limiting.  Just set to cruise_max


  //now, figure out what we want to command, based on sign, and max speeds

  //1. command the absolute velocity; transmission setting and error checking is
  //done for us by vmanage and vdrive.
  if (commanded_vel<0) commanded_vel = -commanded_vel;
  
  //2. control to the minimum of the maximums
  if (commanded_vel>vel_terrain_max) commanded_vel = vel_terrain_max;
  if (commanded_vel>vel_cruise_max) commanded_vel = vel_cruise_max;
  
  //obtain a throttle setting from the PID controller
  throttle = cruise_PID(commanded_vel, vehstate.Speed); 
  
  //put throttle in range
  if (throttle > throttle_maximum) return throttle_maximum;
  else if (throttle < throttle_minimum) return throttle_minimum;
  return throttle;
}


double cruise(double commanded_vel, double act_vel, double act_accel) {  
  double throttle;
  double vel_terrain_max;
  //find out what our terrain says we can go
  vel_terrain_max = vel_cruise_max; // cruise_terrain();  //commented out in order
  //to test without terrain limiting.  Just set to cruise_max

  //command the absolute velocity; direction and error checking is
  //done in vmanage and vdrive
  if (commanded_vel<0) commanded_vel = -commanded_vel;

  
  //control to the minimum of the maximums
  if (commanded_vel>vel_terrain_max) commanded_vel = vel_terrain_max;
  if (commanded_vel>vel_cruise_max) commanded_vel = vel_cruise_max;
  
  throttle = cruise_PID(commanded_vel, act_vel); 
  
  //put throttle in range
  if (throttle > throttle_maximum) return throttle_maximum;
  else if (throttle < throttle_minimum) return throttle_minimum;
  return throttle;
}


//////////////////////////////////////////////////////////////////////
// Helper Functions

////////////////////////////////////////////////////////////
//cruise_pid
//
//takes commanded velocity, and computes an error w.r.t current velocity (from
//vstate).  Produce a control signal that should be the force on the vehicle, 
//and translates this force to a throttle/brake setting through a lookup table.
//This throttle position is added to a feedforward signal based on velocity
//that is commanded.  

//these values are stored outside of cruise so that sparrow has access to them,
//even if we are running the old cruise controller.
#endif
bool cruise_value_good = 0; //is the time data valid?
double cruise_act_vel = 0; //actual speed
double cruise_cmd_vel = 0; //commanded speed
double cruise_cmd_out =0; //what we output (for debugging)
double vel_error_d = 0; //proportional error
double vel_error_p = 0; //derivative error
double vel_error_i = 0; //integral error
double time_interval = 0;//time interval
int timeouts = 0;  //number of times the func has timed out
#ifndef CRUISE_OLD

double cruise_PID(double vel_command, double vel_actual) {
  double throttle_control;  //the error control signal
  timeval cruiseTime;  //time this function was called
  double time_new;  //current time this function was called, to usecs, in secs
  static double time_old;  //previous call time, to usecs, in secs
  static double vel_error_old; //previous velocity error (for I and D)


  /* Save the values that are passed for debugging */
  cruise_cmd_vel = vel_command;  //exporting for sparrow
  cruise_act_vel = vel_actual; //get current speed.

  /* Figure out how much time has passed */
  gettimeofday(&cruiseTime, NULL);  //get the current time
  double secs = (double)cruiseTime.tv_sec;
  double usecs = (double)cruiseTime.tv_usec;
  time_new = secs+usecs/1000000; //
  time_interval = time_new - time_old;//this will be huge on first call.

  // Calculate the proportional error
  vel_error_p = cruise_cmd_vel - cruise_act_vel; //proportional error

  // If we have good history, we can get integral and derivative errors
  //otherwise, reset the integral, and get an appropriate value for the old
  //time.
  
  if (!cruise_value_good || (time_interval>velocity_timeout) ||
      (time_interval<=.01)) {
    //can't take the derivative or integral, because history is unknown
    vel_error_i = 0;    //reset the integral, because history is unknown
    cruise_value_good = 1;  //we are reset and ready to go
    
    //if we timed out because it's been too long, set the old time.  this
    //will happen on the first time through, because the time will be huge
    //(number of seconds since 1980). If it hasnt been too long, we just
    //want to wait a little longer, so, don't kill the old time yet.  
    if (time_interval>velocity_timeout ){
      time_old=time_new;
      timeouts++; //for reporting to sparrow
    }
    
  } else {    // we have old data that is current    
    //take the derivative
    vel_error_d = (vel_error_p - vel_error_old) / time_interval;
    
    //take the trapezoidal approx. to integral, then saturate

#ifndef CRUISE_DIFF_INTS
    //if positive, add the slice to the integral, (gain added later)
    vel_error_i += (vel_error_p + vel_error_old) * time_interval / 2;
    if (vel_error_i >  integrator_sat) vel_error_i =  integrator_sat;
    if (vel_error_i < -integrator_sat) vel_error_i = -integrator_sat;
#else
    double vel_delta;
    //find the size of the slice
    vel_delta = (vel_error_p + vel_error_old) * time_interval / 2;
    //if positive (we are too slow), add the slice with positve gain
    if (vel_delta > 0) vel_error_i += G_vel_i_pos * (vel_delta);
    //if negative (we are too fast), add a more agressive multiple of the slice
    else vel_error_i += G_vel_i_neg * (vel_delta);

    //used to be integral is saturated, then gain is applied.  since gain is done first,
    //we also need to put a gain on the saturation.
    if (vel_error_i >  integrator_sat*G_vel_i_pos) vel_error_i =  integrator_sat*G_vel_i_pos;
    if (vel_error_i < -integrator_sat*G_vel_i_neg) vel_error_i = -integrator_sat*G_vel_i_neg;
#endif

    //update the time
    time_old = time_new;
  }
  
  //save the old error for our next integration and derivative calc.
  vel_error_old = vel_error_p; //save the current velocity error as old
  
  //multiply by gains, and add, to find control signal (we are controlling
  //velocity, so this will be like an acceleration (the m/s/s kind).  
#ifndef CRUISE_DIFF_INTS
  throttle_control = (G_vel_p * vel_error_p) + (G_vel_i * vel_error_i) +
    (G_vel_d * vel_error_d);
#else
  //gain for integral term is already taken care of inside the integral computation
  throttle_control = (G_vel_p * vel_error_p) + (vel_error_i) +
    (G_vel_d * vel_error_d);
#endif  
  
  //return a value from the lookup table to give a better approximation
  //of the right acceleration (throtle/brake) in terms of what we want
  //acceleration (m/s/s) to be. 
  cruise_cmd_out = Vref_Throttle_Lookup(vel_command) + Ctr_Throttle_Lookup(throttle_control);
  return cruise_cmd_out;
}

bool cruise_vehstopped=0;
double cruise_optimized(double vel_command, double vel_actual) {
  double throttle_control;  //the error control signal
  timeval cruiseTime;  //time this function was called
  double time_new;  //current time this function was called, to usecs, in secs
  static double time_old;  //previous call time, to usecs, in secs
  static double vel_error_old; //previous velocity error (for I and D)
  static double begin_stopped;
  double stopped_time;
#define CRUISE_RESET_TIME 3
  
  
  
  
  /* Save the values that are passed for debugging */
  cruise_cmd_vel = vel_command;  //exporting for sparrow
  cruise_act_vel = vel_actual; //get current speed.

  /* Figure out how much time has passed */
  gettimeofday(&cruiseTime, NULL);  //get the current time
  double secs = (double)cruiseTime.tv_sec;
  double usecs = (double)cruiseTime.tv_usec;
  time_new = secs+usecs/1000000; //
  time_interval = time_new - time_old;//this will be huge on first call.


  //first, see if we are stopped and are commanded to stop
  if ((vel_command==0)&&(vel_actual<.05)){
    
    //if we are in a stopped condition, start the timer
    if (cruise_vehstopped)
      stopped_time = time_new - begin_stopped;
    else{
      begin_stopped = time_new;
      stopped_time = 0;
      cruise_vehstopped = 1;
    }
  }
  
  if( (vel_command!=0) && (stopped_time>CRUISE_RESET_TIME) ) {
    cruise_vehstopped = 0; //vehicle is no longer stopped, so, next time 
    //we are stopped and stopping, we should reset the time.
    
    cruise_value_good = 0; //kill the integral, pretend we are starting over
  }
  
  // Calculate the proportional error
  vel_error_p = cruise_cmd_vel - cruise_act_vel; //proportional error
  
  // If we have good history, we can get integral and derivative errors
  //otherwise, reset the integral, and get an appropriate value for the old
  //time.
  
  if ( !cruise_value_good ) {  //could be that we are starting after a big 
    //pause, or starting for the first time.
    vel_error_i=0;
    time_old = time_new;
    cruise_value_good = 1;
  }
  
  else if (time_interval>velocity_timeout) {
    vel_error_i =0;
    time_old = time_new;
    timeouts++;
  }
  
  else {// if time_interval<velocity_timeout && cruise_value_good    
    //take the derivative
    vel_error_d = (vel_error_p - vel_error_old) / time_interval;
    
    //take the trapezoidal approx. to integral, then saturate
    
#ifndef CRUISE_DIFF_INTS
    //if positive, add the slice to the integral, (gain added later)
    vel_error_i += (vel_error_p + vel_error_old) * time_interval / 2;
    if (vel_error_i >  integrator_sat) vel_error_i =  integrator_sat;
    if (vel_error_i < -integrator_sat) vel_error_i = -integrator_sat;
#else
    double vel_delta;
    //find the size of the slice
    vel_delta = (vel_error_p + vel_error_old) * time_interval / 2;
    //if positive (we are too slow), add the slice with positve gain
    if (vel_delta > 0) vel_error_i += G_vel_i_pos * (vel_delta);
    //if negative (we are too fast), add a more agressive multiple of the slice
    else vel_error_i += G_vel_i_neg * (vel_delta);
    
    //used to be integral is saturated, then gain is applied.  since gain is done first,
    //we also need to put a gain on the saturation.
    if (vel_error_i >  integrator_sat*G_vel_i_pos) vel_error_i =  integrator_sat*G_vel_i_pos;
    if (vel_error_i < -integrator_sat*G_vel_i_neg) vel_error_i = -integrator_sat*G_vel_i_neg;
#endif
    
    //update the time
    time_old = time_new;
  }
  
  //save the old error for our next integration and derivative calc.
  vel_error_old = vel_error_p; //save the current velocity error as old
  
  //multiply by gains, and add, to find control signal (we are controlling
  //velocity, so this will be like an acceleration (the m/s/s kind).  
#ifndef CRUISE_DIFF_INTS
  throttle_control = (G_vel_p * vel_error_p) + (G_vel_i * vel_error_i) +
    (G_vel_d * vel_error_d);
#else
  //gain for integral term is already taken care of inside the integral computation
  throttle_control = (G_vel_p * vel_error_p) + (vel_error_i) +
    (G_vel_d * vel_error_d);
#endif  
  
  //return a value from the lookup table to give a better approximation
  //of the right acceleration (throtle/brake) in terms of what we want
  //acceleration (m/s/s) to be. 
  return Vref_Throttle_Lookup(vel_command) + 
    Ctr_Throttle_Lookup(throttle_control);
}




////////////////////////////////////////////////////////////
//cruise_terrain
//
//this function takes harshness values of pitch and roll, and computes an
//error with respect to the acceptible values.  Velocity is the control output
//and is passed to a cruise control function.

bool terrain_value_good = 0;

double cruise_terrain(){
  
  double max_terrain_vel;  //the control signal
  
  timeval terrainTime;
  double time_new;
  double time_old;
  double time_interval;
  
  double roll_harshness;
  double roll_harshness_error_p;
  double roll_harshness_error_i;
  double roll_harshness_error_old;

  double pitch_harshness;  
  double pitch_harshness_error_p;
  double pitch_harshness_error_i;
  double pitch_harshness_error_old;
  

  //vehstate doens't implement these yet.
  //  roll_harshness = vehstate.roll_harshness;
  //  pitch_harshness = vehstate.pitch_harshness;\  
  roll_harshness=0;
  pitch_harshness=0;

  gettimeofday(&terrainTime, NULL);  //get the current time
  time_new = ( (double)terrainTime.tv_sec + ((double)terrainTime.tv_usec / 1000000) ) ; //save it to time_new
  time_interval = time_new-time_old;
  time_old = time_new;
  
  roll_harshness_error_p = roll_harsh_thresh - roll_harshness; //find the error
  pitch_harshness_error_p = pitch_harsh_thresh - pitch_harshness; //find the error  
  
  if (!terrain_value_good || (time_interval>terrain_timeout)) {
    //can't take the derivative
    roll_harshness_error_i = 0;    //reset the integral 
    pitch_harshness_error_i = 0;    //reset the integral 
    terrain_value_good = 1;  //we are reset and ready to go
  }
  
  else { //we have old data that is current    
    roll_harshness_error_i += (roll_harshness_error_p + roll_harshness_error_old) * time_interval / 2;
    if (roll_harshness_error_i >0) roll_harshness_error_i = 0;
    
    pitch_harshness_error_i += (pitch_harshness_error_p + pitch_harshness_error_old) * time_interval / 2;
    if (pitch_harshness_error_i >0) pitch_harshness_error_i = 0;    
  }
  
  //save the current harshness for the next integration
  roll_harshness_error_old = roll_harshness_error_p;
  pitch_harshness_error_old = pitch_harshness_error_p;
  
  max_terrain_vel = (G_roll_p * roll_harshness_error_p + G_roll_i * roll_harshness_error_i +
		     G_pitch_p * pitch_harshness_error_p + G_pitch_i * pitch_harshness_error_i);
  
  if (max_terrain_vel < 0) return 0;
  else if (max_terrain_vel > vel_cruise_max) return vel_cruise_max;
  return max_terrain_vel;
}
#endif


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#ifdef CRUISE_OLD


/* First basic version of cruise controller, uses simple
proportional/derivative gains to generate a change in the controller setting
which should be added to the current control setting (assuming -1 to 1 control
setting).  Characterized by slow rise times, smooth, gentle accelerations.
Will primarily run the throttle to control speed, reactions to large changes
in desired speed will be slow.  Best for maintaining a cruise speed when
already close to it.  A more aggressive controller will be implemented in v2.0 */

/*v.2 2003/11/22 THYAGO CONSORT
	CREATED:
	+ Feed fowared from reference velocity to throttle position thru lookUp table "V_TO_T"
	+ Mapping from control signal to throttle position thru lookUp table "CTR_TO_T"
	MODIFICATION:
	+Parameters
		- Gp = 0.75
		- Gd = 1.25
	+Functions
		- accel_lookup_control*/


////////////////////////////////////////////////////////////////////////////////
// Constant definitions
const double Gp = 0.75; // Proportional Gain
const double Gd = 1.25; // Derivative Gain
//const int avg_N = 5;  // number of time steps to average errors over
const double Upper_Cutoff = .3; // Maximum throttle setting to use
const double Lower_Cutoff = -0.8; // Maximum brake setting to use
//const double Gp_lookup = .5; // Proportional Gain for force lookup control
//const double Gd_lookup = .05; // Derivative Gain for force lookup control


////////////////////////////////////////////////////////////////////////////////
// Global variable declaration
double last_error = 0.0; // stores previous error to calculate change in 
//velocity error, initializes to 0


////////////////////////////////////////////////////////////////////////////////
// External functions

////////////////////////////////////////////////////////////
// cruise
// Uses a map of the accelerations from different throttle/brake settings
// to get a steady state acceleration needed to balance out the acceleration
// from drag.  Then generates a delta_accel based on the current error in
// velocity using a PD controller.  Remaps the desired accel as a throttle/
// brake setting (-1 to 1).
double cruise(double vdesired, double vcurrent, double acurrent){
  
  // variable definitions
  double verr;  // error in velocity
  //double derror; // change in the error
  double adesired;  // desired acceleration
  double aerr;  // error in acceleration
  double control;  // output of the PD control
  double throttle;  // throttle setting
  
  // Compute the current velocity error
  verr = vdesired - vcurrent;
  // Look up the desired acceleration and compute current acceleration error
  //derror = verr - last_error;
  adesired = Accel_Lookup(vdesired);
  aerr = adesired - acurrent;
  // Generate the Proportional-Derivative control signal
  control = Gp * verr + Gd * aerr;
  // Obtain the throttle/brake setting
  throttle = Vref_Throttle_Lookup(vdesired) + Ctr_Throttle_Lookup(control);
  // sets saturation on actuation settings
  if (throttle >= Upper_Cutoff)
    throttle = Upper_Cutoff;
  else if (throttle <= Lower_Cutoff)
    throttle = Lower_Cutoff;
  return throttle;
}

////////////////////////////////////////////////////////////////////////////////
// Internal Functions

////////////////////////////////////////////////////////////
//  delta_throttle
//  Generates a change in throttle setting based on the following algorithm:
//  dthrottle = Gp (vdesired - vcurrent) + Gd (verr - last_error)
double delta_throttle(double vcurrent, double vdesired){
  double verr =  vdesired - vcurrent;
  double dthrot = Gp * verr + Gd * (verr - last_error);
  last_error = verr;
  return dthrot;
}

////////////////////////////////////////////////////////////
// Integral_Control
// Wrapper for delta throttle, sets saturation limits and does the addition
// of the delta value to the old throttle value
double Integral_Control(double vcurrent, double vdesired, double current_throttle){
  double throttle; // new throttle setting
  throttle = current_throttle + delta_throttle(vcurrent, vdesired);
  // sets saturation on actuation settings
  if (throttle >= Upper_Cutoff)
    throttle = Upper_Cutoff;
  else if (throttle <= Lower_Cutoff)
    throttle = Lower_Cutoff;
  return throttle;
}


#endif


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//SHARED CODE


////////////////////////////////////////////////////////////////////////////////
//  Internal Functions

////////////////////////////////////////////////////////////
// cruise_init
//
void cruise_init() {
  cruise_zero();
  return;
}

////////////////////////////////////////////////////////////
// cruise_zero
// 
void cruise_zero(){
  /*  vel_last_error = 0.0;
      vel_integrator = 0.0;
      roll_last_error = 0.0;
      roll_integrator = 0.0;
      pitch_last_error = 0.0;
      pitch_integrator = 0.0;
      time_last = 0.0;
      data_good = 0;
  */
  return;
}

////////////////////////////////////////////////////////////
// Accel_Lookup
// Looks up the acceleration required to overcome drag at a certain
// steady state velocity and returns this acceleration
double Accel_Lookup(double v_steady){
  double a; // the desired acceleration
  a = v_to_a_Table.getValue(v_steady);
  return a;
}

////////////////////////////////////////////////////////////
// Throttle_Lookup
// Looks up the throttle or brake setting necessary to give the car the passed
// acceleration
double Throttle_Lookup(double acceleration){
  double t; // throttle setting
  t = a_to_t_Table.getValue(acceleration);
  return t;
}

////////////////////////////////////////////////////////////
// Ctr_Throttle_Lookup
// Looks up the throttle position required to cancel the error
// control and returns a throttle position
double Ctr_Throttle_Lookup(double control){
  double t; // the desired throttle position
  t = ctr_to_t_Table.getValue(control);
  return t;
}

////////////////////////////////////////////////////////////
// Vref_Throttle_Lookup
// Looks up the throttle position required to keep the velocity reference
// desired velocity and returns a throttle position
double Vref_Throttle_Lookup(double velRef){
  double t; // the desired throttle position
  t = v_to_t_Table.getValue(velRef);
  return t;
}
