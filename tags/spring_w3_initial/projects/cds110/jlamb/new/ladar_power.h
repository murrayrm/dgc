#ifndef _LADAR_POWER_H
#define _LADAR_POWER_H

#include "parallel.h"

#define LADAR_PPORT 0 //parallel port on motherboard
#define LADAR_BUMPER PP_DATA0 //pin 2 of a db25, desigates bumper ladar
#define LADAR_ROOF PP_DATA1 //pin 3 of a db25, designates roof ladar

int ladar_pp_init(); //opens the parallel port, turn both on via ladar_on calls
int ladar_on(int ladarnumber); // turn on for LADAR_BUMPER or LADAR_ROOF
int ladar_off(int ladarnumber);// turn of for...

#endif
