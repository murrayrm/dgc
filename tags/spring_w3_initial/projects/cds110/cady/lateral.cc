// Original version: Jeff Lamb
// Update for MTA: Eric Cady

using namespace std;
#include "lateral.hh"
#include "VehicleConstants.h"

double my_pi = 3.141592654;
double lastHeadingError_p; //previous proportional heading error
double lastLateralError_p;  //previous lateral error
double lateralIntegral;
double headingIntegral;
double time_old;

// Eric's code begins here

// updateState() and constructor code borrowed from Laura's code, so as 
// to get the MTA transfer right.

lateralController::lateralController() : DGC_MODULE (MODULES::lateralController, "Jeff & Eric's miraculous PID controller",0) {}

void lateralController::updateState() {
  Mail msg = NewQueryMessage(MyAddress(), MODULES::VState,VStateMessages::GetState);
  Mail reply = SendQuery(msg);

  if(reply.OK()) {
    reply >> state;
  }
}

// initializePath() runs at the beginning of Active(), to load an RDDF file
// for general use, and then return the first segment of that file for the 
// vehicle to start driving.

void lateralController::initializePath(char *RDDFfile) {
  // Read in RDDF file
  RDDF temp(RDDFfile);
  lateralPath = temp;

  // Check to make sure we can make an initial segment
  if (lateralPath.getNumTargetPoints() < 2) {
    cerr << "Error: RDDF file contains insufficient points to make a path."
         << endl;
    exit(1);
  }

  // Convention from rddf.cc: 1st waypoint is #0, 2nd is #1, etc.
  // Path segment convention: 1st segment is #1, 2nd is #2, etc.
  //   (In general, will pull number from end waypoint.)
  //   numTargetPoints in lateralPath = # of waypoints 
  //                                  = # of last waypoint + 1
  //                                  = # of last segment + 1
  p.northingStart = lateralPath.getWaypointNorthing(0);
  p.northingEnd = lateralPath.getWaypointNorthing(1);
  p.eastingStart = lateralPath.getWaypointEasting(0);
  p.eastingEnd = lateralPath.getWaypointEasting(1);
  p.heading = atan2((p.northingEnd - p.northingStart), 
		    (p.eastingEnd - p.eastingStart));
  p.count = 1;
  
  return;
}

// Jeff's code, modified to work w/ MTA.

double lateralController::distPointAlongLine() {
  double distance;
  double northing = state.Northing;
  double easting = state.Easting;

  // Translate to a coordinate system w/ the start waypoint at the origin
  double t_endNorthing = p.northingEnd - p.northingStart;
  double t_endEasting = p.eastingEnd - p.eastingStart;
  double t_currentNorthing = northing - p.northingStart;
  double t_currentEasting = easting - p.eastingStart;

  // Rotate the coordinate system to force path to x axis
  distance = t_currentEasting * cos(p.heading) +
    t_currentNorthing * sin(p.heading);

  return distance;
}

double lateralController::distPointToLine() {
  double distance;
  double northing = state.Northing;
  double easting = state.Easting;

  // Translate to a coordinate system w/ the start waypoint at the origin
  double t_endNorthing = p.northingEnd - p.northingStart;
  double t_endEasting = p.eastingEnd - p.eastingStart;
  double t_currentNorthing = northing - p.northingStart;
  double t_currentEasting = easting - p.eastingStart;

  // Rotate the coordinate system to force path to x axis
  distance = t_currentEasting * (-sin(p.heading)) +
    t_currentNorthing * cos(p.heading);

  return distance;
}

double lateralController::headingError() {
  double error = state.Yaw - p.heading;
  //normalize to be in [-pi, pi] clockwise from North (y axis)
  if (error > my_pi) error = error - 2.0*my_pi;
  if (error < -my_pi) error = error + 2.0*my_pi;
  return error;
}

void lateralController::stopTheCar() {
  Mail input = NewOutMessage(MyAddress(), MODULES::VDrive,
                             VDriveMessages::FullStop);
  input << u;
  SendMail(input);

  cerr << "Halt command received.  Shutting down." << endl;
  exit(1);
}

// updatePathSegment sets the segment of the path being traversed to the
// next one if we get close enough to the waypoint.  If we pass the final
// waypoint, we stop.
// Return values:
//         0: going along normally
//         1: stop, we've reached the end of the RDDF.

int lateralController::updatePathSegment() { 
  double u;  // Longitudinal distance
  double northing = state.Northing;
  double easting = state.Easting;

  // Add last-waypoint stopping.
  if (p.count + 1 == lateralPath.getNumTargetPoints()) {
    u = distPointAlongLine();
    if (u > lateralPath.getTrackLineDist(p.count)) {
      return 1;
    }
    return 0;
  }
  
  // If we're not very close to the waypoint, skip all this.  If we are,
  // switch path segments.
  if (sqrt((p.northingEnd - northing)*(p.northingEnd - northing) +
	   (p.eastingEnd - easting)*(p.eastingEnd - easting)) 
      < PATH_SWITCH_VAR) {
    p.count++;
    p.northingStart = p.northingEnd;
    p.eastingStart = p.eastingEnd;
    p.northingEnd = lateralPath.getWaypointNorthing(p.count);
    p.eastingEnd = lateralPath.getWaypointEasting(p.count);
    p.heading = atan2((p.northingEnd - p.northingStart), 
		      (p.eastingEnd - p.eastingStart));
    // Reset the integrators, too.
    lateralIntegral = 0;
    headingIntegral = 0;
  }
  
  // Don't need to stop if you made it here.  
  return 0;
}

void lateralController::Active() {
  // Global vars used here:
  //    lateralPath (type RDDF, contains entire waypoint set)
  //    p (type pathSegment, describes the current section being traversed)
  //    state (type VState_GetStateMsg, contains all state info)
  double angle;

  // Initialize variables as needed
  int stop = 0;
  lastLateralError_p = 0;
  lastHeadingError_p = 0;
  lateralIntegral = 0;
  headingIntegral = 0;

  initializePath(RDDF_FILE);

  while(ContinueInState()) {
    // Get the vehicle state
    updateState();

    // check for state errors
    // If this gets used anywhere else but for the CDS110 tests, may
    // want to implement a more robust error-handling mechanism.
    //if (state.gps_enabled <= 0) {
    //  cerr << "GPS offline, discontinuing test." << endl;
    //  stopTheCar();
    //}

    // Get path segment; update it if we should be going to the next one.  
    stop = updatePathSegment();
    if (stop != 0) { // Only happens when we pass the last waypoint.
      stopTheCar();
    }

    // SimVStateVDrive unfortunately only takes inputs of the form
    // CmdDirectDrive, not CmdMotionMsg, so we have to command an
    // acceleration instead of a desired speed.  I'm not sure why
    // this was done--velocity would have been much more convenient for 
    // our purposes, and I don't want to make this a lateral AND speed
    // controller.  Ah well, we'll try to keep it going at about a
    // constant speed, only accelerating when braking/turning causes
    // it to slow down. Can make it more robust later, if desired, but 
    // not necessary for this.
    if (state.Speed < CONSTANT_SPEED) {    
      u.accel_cmd = SMALL_ACCELERATION;
    } else {
      u.accel_cmd = 0;
    }
    u.Timestamp = TVNow();

    // Command new steering position
    u.steer_cmd = PIDController();

    // Send new position to vdrive and simulator
    // Note: these MTA commands derived from Laura's code
    Mail realMessage = NewOutMessage(MyAddress(),MODULES::VDrive, VDriveMessages::CmdDirectDrive);
    realMessage << u;
    SendMail(realMessage);
    
    Mail simMessage = NewOutMessage(MyAddress(),MODULES::SimVDrive, VDriveMessages::CmdDirectDrive);
    simMessage << u;
    SendMail(simMessage);
    
    // Normally, I'd log things here, but SimVStateVDrive already does 
    // logging, so there's really no reason to duplicate.  Can add it
    // in the future if SimVStateVDrive doesn't work out.

    usleep(100000); // 10Hz
    // repeat until commanded to stop
    // TODO -- insert emergency stop functionality?
  }
} 

// This is Jeff's code, again, with some modifications.

double lateralController::PIDController() {
  timeval lateralTime;
  double steer_angle_cmd;
  double lateralDerivative, headingDerivative;
  double lateralCommand, headingCommand;

  // Calculate proportional errors.
  double lateralError_p = distPointToLine();
  double headingError_p = headingError();
  
  gettimeofday(&lateralTime, NULL); // get current time
  double secs = (double)lateralTime.tv_sec;
  double usecs = (double)lateralTime.tv_usec;
  double time_new = secs+usecs/1000000;
  double time_interval = time_new - time_old;  //amount of time passed since last cycle
  
  // derivative:
  headingDerivative = (headingError_p - lastHeadingError_p) / time_interval;
  lateralDerivative = (lateralError_p - lastLateralError_p) / time_interval;

  // integrator:
  if(fabs(headingError_p - lastHeadingError_p) > 1) headingIntegral = 0;
  if(fabs(lateralError_p - lastLateralError_p) > 1) lateralIntegral = 0;
  headingIntegral += (headingError_p + lastHeadingError_p) * time_interval/2;
  lateralIntegral += (lateralError_p + lastLateralError_p) * time_interval/2;
  if(lateralIntegral > LATERAL_INTEGRATOR_SAT) lateralIntegral = LATERAL_INTEGRATOR_SAT;
  if(lateralIntegral < -LATERAL_INTEGRATOR_SAT) lateralIntegral = -LATERAL_INTEGRATOR_SAT;
  if(headingIntegral > HEADING_INTEGRATOR_SAT) headingIntegral = HEADING_INTEGRATOR_SAT;
  if(headingIntegral < -HEADING_INTEGRATOR_SAT) headingIntegral = -HEADING_INTEGRATOR_SAT;

  //commanded angles, normalized from -1 (left) to 1 (right)
  lateralCommand = LATERAL_GAIN *(LATERAL_PROPORTIONAL_GAIN * lateralError_p +
		    LATERAL_INTEGRATOR_GAIN * lateralIntegral +
		    LATERAL_DERIVATIVE_GAIN * lateralDerivative);
  headingCommand = (HEADING_PROPORTIONAL_GAIN * headingError_p +
		    HEADING_INTEGRATOR_GAIN * headingIntegral +
		    HEADING_DERIVATIVE_GAIN * headingDerivative); 
  steer_angle_cmd = lateralCommand + headingCommand;
  

  // conversion to -1 to 1 notation
  // -1 is far left, 1 is far right.
  if (steer_angle_cmd > 0) {
    steer_angle_cmd /= VEHICLE_MAX_RIGHT_STEER;
  }
  if (steer_angle_cmd < 0) {
    steer_angle_cmd /= VEHICLE_MAX_LEFT_STEER;
  }

  // account for steering actuator saturation  
  if (steer_angle_cmd >= 1) {
    steer_angle_cmd = 0.99;
  }
  if (steer_angle_cmd <= -1) {
    steer_angle_cmd = -0.99;
  }

  //update values for next cycle and return
  lastHeadingError_p = headingError_p;
  lastLateralError_p = lateralError_p;
  time_old = time_new;

  // For debugging gain stuff:
  // Can output all proportional/integral/derivative terms, etc here,
  // and watch to see where errors come in. (Yes, need to add this.)

  cout << "E Lateral: " << lateralError_p << '\t' << "E Heading: " 
       << headingError_p << '\t' << "Time: " << time_interval <<endl;
  cout << "Lateral Integrator: " << lateralIntegral
       << '\t' << "steer command: " << steer_angle_cmd <<endl;

  return steer_angle_cmd;
}

int main(int argc, char **argv) {
  int i,j;
  
  // Command-line handling...don't need that yet.

  /*  for (i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      j = 0;
      while(argv[i][j] != '\0') {
	switch(argv[i][j]) {
	case 'g':
	  //stuff
	  break;
	default:
	  break;
	}
      }
    }
    }*/

  Register(shared_ptr<DGC_MODULE>(new lateralController));
  StartKernel();
  return 0;
}

lateralController::~lateralController() {};
