function result = calcRayIntersection(terrain, vehicle, sensor, m, b)
    s_to_v_rot_matrix = getRotMatrix(sensor.orientation(1), sensor.orientation(2), sensor.orientation(3));
    v_to_g_rot_matrix = getRotMatrix(vehicle.orientation(1), vehicle.orientation(2), vehicle.orientation(3));
    result(1)=-1;
    index_list=find(terrain(:,1)>vehicle.position(1) & terrain(:,1)>2);
    second_index_list = find((terrain(index_list-1,2)>=(m*terrain(index_list-1,1)+b)) & (terrain(index_list,2)<(m*terrain(index_list,1)+b)));
    if ~isempty(second_index_list)
        i=index_list(second_index_list(1));
        m_terrain = (terrain(i-1,2)-terrain(i,2))/(terrain(i-1,1)-terrain(i,1));
        b_terrain = (terrain(i-1,1)*terrain(i,2)-terrain(i,1)*terrain(i-1,2))/(terrain(i-1,1)-terrain(i,1));
        x_intersect = (b_terrain-b)/(m-m_terrain);
        y_intersect = m*x_intersect+b;
        sensor_location = vehicle.position + v_to_g_rot_matrix*sensor.offset;
        point_location = [x_intersect 0 y_intersect]';
        range = sqrt((sensor_location(1)-x_intersect)^2 + (sensor_location(3)-y_intersect)^2);
        if (range <= sensor.max_range) & (sensor.min_range <= range)                    
            result(1) = range;            
%             plot(point_location(1), point_location(3), 'bd');
            return;
        end
    end
