% script lat_ctrl_fig.m
%
% script to investigate a new idea for feedback control for lateral control
% of a vehicle (under bicycle model assumptions)

% for arrow
addpath('/Users/lars/Development/matlab/bin')

% maximum lateral distance to trajectory
maxdist = 12;

% define a line in R^2 with two points
% use convention (N,E) for ordering
ref1 = [2 3];
ref2 = [3 5];

ref_dir = (ref2 - ref1)/norm(ref2 - ref1);
% perp_dir is ref_dir rotated CW by 90 degrees
perp_dir = [ref_dir(2) -ref_dir(1)];
ref_yaw = atan2(ref_dir(2), ref_dir(1));

end1 = ref1 + ref_dir * 15;
end2 = ref1 - ref_dir * 15;

% offset points to the right and left of the reference
offr = ref1 + maxdist * perp_dir;
offl = ref1 - maxdist * perp_dir;

end3 = offl + ref_dir * 15;
end4 = offl - ref_dir * 15;

end5 = offr + ref_dir * 15;
end6 = offr - ref_dir * 15;

% plot easting (second element) on X-axis and
% northing (first element) on Y-axis

% plot squareds on the reference line
plot([ref1(1) ref2(1)], [ref1(2) ref2(2)], 'rs')
hold on

% plot the reference line and offset lines
plot([end1(1) end2(1)], [end1(2) end2(2)], '-')
plot([end3(1) end4(1)], [end3(2) end4(2)], '-.')
plot([end5(1) end6(1)], [end5(2) end6(2)], '-.')

grid on, axis equal


% the idea is to feedback the error signal
% \alpha y_e + (1 - \alpha) \beta \yaw_e
% choose \alpha first
% then use heuristic that we want to be pointed directly
% at reference when we are maxdist away from it.
% i.e. error signal is zero when \yaw_e == \theta_ref - \theta
% is such that \yaw_e = -\alpha y_e / ((1 - \alpha) * \beta)
alpha = 0.50;
beta = -alpha*maxdist/((1-alpha)*pi/2);

xlabel('N')
ylabel('E')

view(90,-90)

for easting = [-10:10]

  for northing = [-10:10]

    % calculate y_e, saturate at \pm maxdist, then calculate
    % the \yaw_e that will make the error signal as defined above zero.
    y_e = (ref1(1)-northing)*ref_dir(2) - ref_dir(1)*(ref1(2)-easting);
    y_e = max(-maxdist,min(maxdist,y_e));
    yaw_e = -(alpha*y_e)/((1-alpha)*beta);
    
    plot(northing, easting, 'r.')
    %text(northing, easting, num2str(y_e,1))
    %arrow([northing easting],[northing+cos(yaw_e) easting+sin(yaw_e)])
    plot([northing northing+cos(ref_yaw-yaw_e)],[easting easting+sin(ref_yaw-yaw_e)],'b')
 
    %drawnow

  end

end



axis([-15 15 -15 15])
