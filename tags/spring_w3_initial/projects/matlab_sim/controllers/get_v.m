function vlon = get_v(state)
% Picks out the longitudinal velocity in the state vector
vlon = state(3);