#ifndef WEIGHTS_WRITER_HH
#define WEIGHTS_WRITER_HH

#include <iostream>
#include <fstream>
using namespace std;

/** Help class for writing the weights to file and calculate the average of 
 * the weights.
 * $Id$
 */
class WeightsWriter
{
public:
  /** Constructor */
  WeightsWriter();

  /** Logs a weights set
   * @param weights an array of weights (must be of size ArbiterInput::Count
    */
  void writeWeights(double weights[]);
  
  /** Returns the filename of the weights log
   * @return the filename
   */
  char* getWeightLogFilename() { return weights_filename; }
  
  /** Takes the specified logfile, calculates the average weight for each voter
   * and prints it to a new file which has the same name but with _average 
   * appended at the end 
   * @param the filename
   */
  void writeAverageWeights(char* weights_filename);

private:
  /** logfile is for the weights */
  FILE * weights_file;
  
  /** filename of the weights logfile */
  char weights_filename[255];
  
};

#endif
