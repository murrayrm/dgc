/**
 * $Id$
 */
#include "Arbiter2/IO.hh"
#include "HumanLog.hh"
#include <vector> 
using namespace std;

HumanLog::HumanLog(char* filename) {
  vector<deque<double> > arbiterLog = IO::readArbiterLog(filename);
  speedLog = arbiterLog.at(7); // this column has the speed
  steerLog = arbiterLog.at(17); // this column has the steer command
  printf("Humanlog created from %s\n", filename);
}

VDrive_CmdMotionMsg HumanLog::getCommand() {
  VDrive_CmdMotionMsg cmd;
  cmd.steer_cmd = steerLog.front();
  steerLog.pop_front();
  cmd.velocity_cmd = speedLog.front();
  speedLog.pop_front();
  return cmd;
}  
