/**
 * $Id$
 */
#include "Population.hh"
#include "GeneticAlgorithm.hh" // my_rand, my_srand
#include <iostream>     // cout
using namespace std;

Population::Population()
{
  popSize = 0;
  isEvaluated = false;
  generation = vector<Individual*>();
}

Population::~Population()
{
  Individual* ind;
  vector<Individual*>::iterator iter = generation.begin();
  while(iter != generation.end())
  {
    ind = *iter;
    iter = generation.erase(iter);
    delete ind;
  }
}

void Population::add(Individual* ind)
{
  generation.push_back(ind);
  selectRange.push_back(pair<double, double>());
  popSize++;
  isEvaluated = false;
}

Individual* Population::select()
{
  if(!isEvaluated)
  {
    cout << "ERROR: you must run evaluate() before doing any selections!" << endl;
    exit(1);
  }
  double p = my_rand()/(double)MY_RAND_MAX; // a number between 0.0 and 1.0
  for(int i = 0 ; i < popSize ; i++)
  {
    if(p >= selectRange[i].first && p < selectRange[i].second)
    {
      return generation[i];
    }
  }
  cout << "Error: didn't find any with select() - should never happen!!!" << endl;
  return generation[0];
}

void Population::evaluate()
{
  int i;
  double fitnessSum = 0.0;
  maxFitness = -10e38; // low negative number
  double fitness;
  for(i = 0 ; i < popSize ; i++)
  {
    generation[i]->evaluate();
    fitness = generation[i]->getFitness();
    if(fitness > maxFitness)
    { // update max fitness
      maxFitness = fitness;
      fittestIndex = i;
    }
    fitnessSum += fitness;
  }
  // create probablity to be selected, based on the fitness of each individual
  // fitness / fitnessSum probability on each
  selectRange.clear();
  double current = 0.0; // we start at 0%
  for(i = 0 ; i < popSize ; i++)
  {
    selectRange[i].first = current;
    current += generation[i]->getFitness() / fitnessSum;
    selectRange[i].second = current; // the upper limit for this interval
  }
  isEvaluated = true;
}

double Population::getMaxFitness()
{
  if(!isEvaluated)
  {
    cout << "ERROR: you must run evaluate() before doing any getMaxFitness() calls!" << endl;
    exit(1);
  }
  return maxFitness;
}

Individual* Population::getFittestIndividual()
{
  return generation[fittestIndex];
}
