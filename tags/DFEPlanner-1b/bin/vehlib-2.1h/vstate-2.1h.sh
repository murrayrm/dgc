#!/bin/csh
# 
# vstate-2.1h.sh - startup script for vstate-2.1h
# RMM, 27 Feb 04
#
# This script starts up vstate-2.1h on magnesium and restarts it if it exits
# unexpectedly.  This contains an absolute path to vstate-2.1h so that it
# can be run from /etc/rc.d.
#
# The script tries to be smart about whether it is running on a terminal
# or not and doesn't request input unless it is running on a terminal
#
# Usage:
#   vstate-2.1h flags [&]
#
# Environment variables:
#   DGC		path to DGC executables
#   VSFLAGS	optional flags to pass to vstate-2.1h
#

# Set up process limits to allow core files to be generated
limit coredumpsize unlimited

# Make sure we have a path to the file
if (! $?DGC) set DGC=`pwd`
if (! $?VSFLAGS) set VSFLAGS=""
if (! -e $DGC/vstate-2.1h) then
  echo "FATAL: can't find vstate-2.1h in $DGC"
  exit
endif

# Print out a banner so that we know where we are running
echo "vstate-2.1h@`hostname`: pwd = `pwd`; VSFLAGS = $VSFLAGS";

# Check to make sure we are on the right machine
if (`hostname` != "magnesium") then
  echo "WARNING: not running on magnesium"
  if ($tty != "") then
      echo -n "Continue (y/n)? ";
      if ($< != "y") exit
  endif
endif

# Check to make sure that vstate-2.1h is not already running
set vslist = "`ps -aux | grep vstate-2.1h | grep -v vstate-2.1h.sh`"
if ("$vslist" != "") then
  echo "WARNING: vstate-2.1h already running on this machine:"
  echo "  $vslist"
  if ($tty != "") then
      echo -n "Kill/Continue/Abort (k/c/a)? ";
      set response = $<;
      if ($response == "a") exit
  endif

  # Kill all running copies of vstate-2.1h
  if ($response == "k") then
    if (`killall -9 vstate-2.1h` != "") then
      echo "WARNING: couldn't kill all copies of vstate-2.1h"
      if ($tty != "") then
        echo -n "Continue (y/n)? ";
        if ($< != "y") exit
      endif
    endif
  endif
endif

# Run vstate-2.1h and restart it whenever it crashes
while (1) 
  echo "======== starting vstate-2.1h $VSFLAGS $argv ========"
  $DGC/vstate-2.1h $VSFLAGS $argv
  echo "vstate-2.1h aborted"

  # kill anything that is left over
  killall -9 vstate-2.1h >& /dev/null

  sleep 2	# sleep for a few seconds to let things die
end
