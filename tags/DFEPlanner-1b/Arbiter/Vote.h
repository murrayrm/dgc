#ifndef __VOTE_H__
#define __VOTE_H__

/* Vote.h
   Author: J.D. Salazar
   Created: 8-25-03

   A class representing a vote on a steering arc for the arbiter to be used by
   Team Caltech in the 2004 Darpa Grand Challenge */


struct Vote
{
  public:
  // The value of the vote
  int goodness;

  // The angle of the arc in radians
  double phi;

  // The velocity along the arc in m/s
  double velo;

  // Basic constructor
  Vote(double t = DEFAULT_PHI, int w = DEFAULT_GOODNESS, double v = DEFAULT_VELO)
    : goodness(w)
     , phi(t)
     , velo(v)
  {}

  // Copy Constructor
  Vote(const Vote &other) { copy(other); }

  // Assignment Operator
  Vote &operator= (const Vote &other)
  {
    // Check for self assignment
    if( this == &other ) return *this;

    destroy();
    copy(other);

    return *this;
  }

  // Comparison operators
  bool operator== (const Vote &other) const
  {
    return (goodness == other.goodness && phi == other.phi && velo == other.velo);
  }

  bool operator!= (const Vote &other) const { return !(*this==other); }

  // Default values
  static const int DEFAULT_GOODNESS = 0;
  static const double DEFAULT_PHI = 0;
  static const double DEFAULT_VELO = 0;
 
  private:
  // Copies other to this.
  void copy(const Vote &other) { goodness = other.goodness; phi = other.phi;
  velo = other.velo; }

  // Deallocates any memory and resets all values of this.
  void destroy() { goodness = 0; phi = 0; velo = 0; }

};

#endif
