#include "vehports.h"
#include "brake.h"
#include "accel.h"			/* for ACCEL_CS */


#include "sparrow/dbglib.h"
#include <iostream>
#include <time.h>

using namespace std;

/* Declare some global variables for use in display */
double brk_current_abs_pos_buf, brk_current_acc_buf, brk_current_vel_buf;
double brk_current_brake_pot, brk_pot_voltage;
int brk_case;
int brk_lock = 0;			/* use for memory locking */

/* Constants used in set_brake_pos...() functions mainly as safety limits. */
/*  The following four variables are used by set_brake_abs_pos_pot to
 * move the brake's absolute position based on the pot.
 *  Although they are initialized to values that will give decent braking
 * characteristics most of the time, it is better to calibrate them using
 * the calib_brake function.
 *  If we use the values below as static var's we need to a large set of
 * counts that we can average on.
 *  min_pot & max_pot: max and min position used in set_brake_abs_pos_pot
 *    to scale input position values. [0,1] -> [min_pot, max_pot]. Hence,
 *    these values act as software limits for the brake actuator's movement
 *    range.
 *  min_counts & max_counts: must correspond to min_pot and max_pot in counts.
 *    As of 1/17/2004, no look-up table for the relationship.
 *    In calib_brake, they're modified with get_brake_pos()
 * Current Usage: set_brake_abs_pos_pot(), calib_brake()
 */
double current_brake_pot = 0; //needed or hack below

double min_pot=0.37; //normlle .37/* Limits set by hardware at Santa Anita, 9/12/2003 */
double max_pot=0.70; //normally .9 /* Limits set by hardware at Santa Anita, 9/12/2003 */
double min_counts=0.0;
double max_counts=-119868;

/* our_max_pos & our_min_pos: 
 *  Modified in calib_brake() with getBrakePot() values, just like min_pot and
 *    max_pot, i.e. (as of 1/17/2004) same as min_pot & max_pot if
 *    calib_brake() is run.
 * Current Usage: set_brake_abs_pos()
 */double our_max_pos = .05;
double our_min_pos = .95;

//****************************************************
//****************************************************
/* Declares the buffer that stores the current commanded brake position,
 * vel and acc values.
 * Current Usage: set_brake_abs_pos_vel(), start_brake_control_loop()
 */
double* abs_pos_buf;
double* vel_buf;
double* acc_buf;
// ****************************************************


int send_brake_command(int command_num, char* parameters, int length) {
  char buffer[50];
  int status;

  dbg_info("brake: sending cmd %d\n", command_num);
  brk_lock = 1;
  status = receive_brake_command(command_num, parameters, length, 
				 buffer, 50, 1);
  brk_lock = 0;

  return status;
}



//never uses the length perameter
int receive_brake_command(int command_num, char* parameters, int length, char* buffer, int buf_length, int timeout) {
  char command[400];
  char word[11];  

  //Set the strings to null
  memset(command, 0, strlen(command));
  memset(buffer, 0, buf_length);

  //Prepare the command according to the brake actuator specs
  command[0]='@';
  sprintf(word, "%d ", BRAKE_ID);
  strcat(command, word);
  sprintf(word, "%d ", command_num);
  strcat(command, word);
  strcat(command, parameters);
  strcat(command, "\r\n");

  //note that the serial_write command is being passed a command that contains 
  //******\r\n, where *****is the prake_port, command number. etc, but the 
  //string length that serial write is using as an argument is the length-1
  //so the \n is never used.

  //Write the command
  serial_write(BRAKEPORT, command, strlen(command)-1, SerialBlock);
  //  printf("sending command %d ", command_num, "with parameters %d\n", parameters);
  //Read the response
  //serial_read_until(BRAKEPORT, buffer, '\r', timeout, buf_length);
  serial_read_until(BRAKEPORT, buffer, '\n', timeout, buf_length);

  //If it's a negative acknowledge (it starts with a !)
  if(strncmp(buffer, "!", 1)==0) {
    //Return an error
    return -1;
  } else {
    //Otherwise it's good
    /*  
    for (int i = 0; i < strlen(command); i++) 
      cout << command[i];
    cout << endl;
    
    for (int i = 0; i < strlen(buffer); i++) 
      //printf("%X ", (int)buffer[i]);
      cout << buffer[i];
    //cout << endl;
    */
    return 0;
  }

}

int brake_ready() {
  char response[50];
  
  receive_brake_command(0, " ", 0, response, 50, 1);
  if(strcmp(response, "# 10 0000 2000\n")==0) {
    return 0;
  }
  else if (strcmp(response, "# 10 0000 0100\n") == 0
        || strcmp(response, "# 10 0000 0200\n") == 0) {
    cout << "Brake ERROR~ restarting!" << endl;
    reset_brake();
    return 0;
  } 
  else {
    return -1;
  }
}

//This function send the command to reset the brake
//And then waits until the brake is ready before returning
//IT IS A BLOCKING FUNCTION
int reset_brake() {
  double time;
  timeval before, after;
  double elapsed;
  char response[50];

  gettimeofday(&before, NULL);

  //Restarting it gets rid of moving error  
  //serial_write(BRAKEPORT, "@16 4 \r", strlen("@16 4 \r"), SerialBlock);
  send_brake_command(4, " ", 0);

  //Wait until the actuator is ready again
  while(strcmp(response, "# 10 0000 2000\n")!=0) {
    //cout << "stop";
    receive_brake_command(0, " ", 0, response, 50, 1);
  }

  // should be calibrate it again?
  // or at least set the min max variables?
  // depends on how stuff is done:
  //  using set_brake_abs_pos_pot 
  //  => either must use hardcoded
  //         or use the first calibrated number and hope the program doesn't crash
  //         or calibrate every time we reset this (using brake pot).


  // Get the elapsed time. 
  gettimeofday(&after, NULL);
  elapsed = after.tv_sec - before.tv_sec;
  elapsed += ((double) (after.tv_usec - before.tv_usec)) / 1000000;
  // Fix Midnight rollover
  if ( elapsed < 0 ) {
    // Add number of seconds in a day
    elapsed += 24 * 3600;
  }
  //cout << "\nTime to reset in seconds: " << elapsed << endl;

  return 0;
}

int init_brake() {
  char parameters[400];
  cout<<"initializing brake"<<endl;

  //***************************************************************
  // **************************************************************
  // allocate memory for the position in start_brake_control_loop
  abs_pos_buf = (double*)malloc(sizeof(double));
  vel_buf = (double*)malloc(sizeof(double));
  acc_buf = (double*)malloc(sizeof(double));
  if ((abs_pos_buf == NULL) || (vel_buf == NULL) || (acc_buf == NULL)) {
    printf("Failed to allocate memory for abs_pos_buf, vel_buf, and/or acc_buf.");
    return -1;
  }
  /* Initialize everything to zero */
  *abs_pos_buf = *vel_buf = *acc_buf = 0.0;

  //***************************************************************  


  if (serial_open_advanced(BRAKEPORT, BRAKEBAUD, SR_PARITY_DISABLE | SR_SOFT_FLOW_DISABLE | SR_HARD_FLOW_DISABLE | SR_TWO_STOP_BIT) != 0) {
    printf("Unable to open com port %d \n", BRAKEPORT);
    return -1;
  } else {
    //If the serial port is initialized, send initialization commands to the brake
    //These are copied directly from the default initialization in the QuickSilver Controls Windows program
    //Note: They probably aren't needed!
    //For information on each command, look up the command ID (first argument in the send_brake_command command) in the QuickSilver manual

    //Format:
    //sprintf(parameters, "parameter format string", param1, param2, ...);
    //send_brake_command(command_num, parameters, strlen(parameters));

    // Because of the following line, this function cannot be called
    // before init_accel() or initBrakeSensors()
    // Get the current zero position: we should probably move the
    // actuator to the absolute zero and then do this... will init_brake()
    // always be followed by calib_brake? 

    //our_min_pos = getBrakePot();

    /*    
    sprintf(parameters, "%d ", 5136);
    send_brake_command(155, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 1);
    send_brake_command(185, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 0);
    send_brake_command(186, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 576);
    send_brake_command(174, parameters, strlen(parameters));

    //ACK Delay
    sprintf(parameters, "%d ", 0);
    send_brake_command(173, parameters, strlen(parameters));
    
    //MCT
    //FLC

    sprintf(parameters, "%d %d %d %d %d %d %d ", 0, 6, 6, 20, 20, 200, 500);
    send_brake_command(148, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 0);
    send_brake_command(237, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 0);
    send_brake_command(184, parameters, strlen(parameters));

    sprintf(parameters, "%d %d %d %d ", 15000, 20000, 6000, 10000);
    send_brake_command(149, parameters, strlen(parameters));

    sprintf(parameters, "%d %d ", 10, 4);
    send_brake_command(150, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 1250);
    send_brake_command(230, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 32767);
    send_brake_command(195, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 10);
    send_brake_command(212, parameters, strlen(parameters));

    sprintf(parameters, "%d ", 52);
    send_brake_command(213, parameters, strlen(parameters));

    //Error Limits (ERL)
    sprintf(parameters, "%d %d %d ", 500, 200, 120);
    send_brake_command(151, parameters, strlen(parameters));

    //Kill Motor Condition (KMC)
    sprintf(parameters, "%d %d ", 128, 0);
    send_brake_command(167, parameters, strlen(parameters));

    sprintf(parameters, "%d %d ", 0, 83);
    send_brake_command(252, parameters, strlen(parameters));
    
    */

    /*
    //Soft Stop Limits
    sprintf(parameters, "%d ", SSL_REGISTER);
    send_brake_command(221, parameters, strlen(parameters));

    //Write Register (of min Soft Stop Limit)
    sprintf(parameters, "%d %d ", SSL_REGISTER, SSL_MIN);
    send_brake_command(11, parameters, strlen(parameters));

    //Write Register (of max Soft Stop Limit)
    sprintf(parameters, "%d %d ", SSL_REGISTER+1, SSL_MAX);
    send_brake_command(11, parameters, strlen(parameters));
    */

    cout << "end of initialization" << endl;
    return 0;
  }
}

// Sets the brake's absolute position in 0-1 units
// 0: calibrated zero, 1:fully retracted
// Will not move brake by more than max(MAX_MOV_TEN, function of vel, acc)
//function of vel, acc determined by velocity profile.
//Suitable for 10Hz control loop
int set_brake_abs_pos_limited(double abs_pos, double vel, double acc) {
  char parameters[50];
  char response[50];
  double real_min_pos, real_max_pos, current_pos, limited_abs_pos, max_mov;
  //Limit and scale the velocity, acceleration, and range
  vel=in_range_double(vel, 0, 1);
  vel=scale_double(vel, 0, 1, MIN_VEL, MAX_VEL);

  acc=in_range_double(acc, 0, 1);
  acc=scale_double(acc, 0, 1, MIN_ACC, MAX_ACC);

  //calculate max displacement based on vel, acc
  //from waypoint nav i see vel>>acc.  so i will assume a triangular profile
  //this also fits with the short time=0.1s
  //here vel and acc are in steps, max_mov is in steps  
  max_mov = 0.0025*acc; 
  if (max_mov > MAX_MOV_TEN) {max_mov=MAX_MOV_TEN;}
  // absolute position goes from 0 to 1, and this spans the length
  // from the zero position to the full position set in calibration.
  // in calibration, zero position should be the "fully" extended 
  // and the full position should be the "fully" retracted position.
  abs_pos=in_range_double(abs_pos, 0, 1);
  //abs_pos=scale_double(abs_pos, 0, 1, MIN_POS, MAX_POS);  real_min_pos= MIN_POS*(our_max_pos-our_min_pos);
  real_max_pos= MAX_POS*(our_max_pos-our_min_pos);
  abs_pos=scale_double(abs_pos, 0, 1, real_min_pos, real_max_pos);
  current_pos = get_brake_pos();
  limited_abs_pos = in_range_double(abs_pos, current_pos - max_mov, current_pos + max_mov);
  /*
  send_brake_command(5, " ", 0);
  send_brake_command(0, " ", 0);
  send_brake_command(1, "65535 ", strlen("65535 "));
  send_brake_command(3, "500000 ", strlen("500000 "));
  */

  dbg_info("brake: setting to %g\r", limited_abs_pos);

  // The following commands are copied directly from how the QuickSilverMax
  //  Windows program does it. Sadly, no one knows why.
  //Send a polling command
  send_brake_command(0, " ", 0);
  //Clear the polling status word
  send_brake_command(1, "65535 ", strlen("65535 "));
  //Get revision info?
  send_brake_command(5, " ", 0);
  //Clear the program buffer
  send_brake_command(8, " ", 0);
  //Start the program download
  send_brake_command(9, " ", 0);

  //Move Absolute, Velocity Based (MAV)
  sprintf(parameters, "%.0f %.0f %.0f %d %d ", limited_abs_pos, acc, vel, STOP_ENABLE_ABS, STOP_STATE_ABS);
  send_brake_command(134, parameters, strlen(parameters));

  //End the program download
  send_brake_command(128, " ", 0);
  //Run the program
  receive_brake_command(10, " ", 0, response, 50, 1);

  if(strcmp(response, BRAKE_ACK)==0) {
    return 0;
  } else {
    return -1;
  }
}

//Sets the brake to wherever you want it, but may take up to 2 seconds
//Not suitable for Control Loop.  Useful only for calibration or other
//DC commands
int set_brake_abs_pos(double abs_pos, double vel, double acc) {
  char parameters[50];
  char response[50];
  double real_min_pos, real_max_pos;
  cout<<"moving with set_brake_abs_pos"<<endl;
  //Limit and scale the velocity, acceleration, and range
  vel=in_range_double(vel, 0, 1);
  vel=scale_double(vel, 0, 1, MIN_VEL, MAX_VEL);

  acc=in_range_double(acc, 0, 1);
  acc=scale_double(acc, 0, 1, MIN_ACC, MAX_ACC);

  // Absolute position goes from 0 to 1, and this spans the length
  // from the zero position to the full position set in calibration.
  // in calibration, zero position should be the "fully" extended 
  // and the full position should be the "fully" retracted position.
  abs_pos=in_range_double(abs_pos, 0, 1);
  //abs_pos=scale_double(abs_pos, 0, 1, MIN_POS, MAX_POS);
  real_min_pos= MIN_POS*(our_max_pos-our_min_pos);
  real_max_pos= MAX_POS*(our_max_pos-our_min_pos);
  abs_pos=scale_double(abs_pos, 0, 1, real_min_pos, real_max_pos);
  /*
  send_brake_command(5, " ", 0);
  send_brake_command(0, " ", 0);
  send_brake_command(1, "65535 ", strlen("65535 "));
  send_brake_command(3, "500000 ", strlen("500000 "));
  */

  // The following commands are copied directly from how the QuickSilverMax
  //  Windows program does it. Sadly, no one knows why.
  //Send a polling command
  send_brake_command(0, " ", 0);
  //Clear the polling status word
  send_brake_command(1, "65535 ", strlen("65535 "));
  //Get revision info?
  send_brake_command(5, " ", 0);
  //Clear the program buffer
  send_brake_command(8, " ", 0);
  //Start the program download
  send_brake_command(9, " ", 0);

  //Move Absolute, Velocity Based (MAV)
  sprintf(parameters, "%.0f %.0f %.0f %d %d ", abs_pos, acc, vel, 
	  STOP_ENABLE_ABS, STOP_STATE_ABS);
  send_brake_command(134, parameters, strlen(parameters));

  //End the program download
  send_brake_command(128, " ", 0);
  //Run the program
  receive_brake_command(10, " ", 0, response, 50, 1);

  if(strcmp(response, BRAKE_ACK)==0) {
    return 0;
  } else {
    return -1;
  }
  cout<<"done moving with set_brake_abs_pos"<<endl;
}


//Sets the brake's position based on current pot value and calibration values
//abs_pos is on the 0-1 scale as follows
// 0: calibrated zero (no braking), 1:fully retracted (full-braking)
//Where these points are set during calibration
int set_brake_abs_pos_pot_limited(double abs_pos, double vel, double acc) {
  double abs_pos_pot, rel_pos_pot, rel_pos_counts, current_pos, max_mov, acc_steps;

  if (abs_pos < 0.0 || abs_pos > 1.0) {
    return -1;
  }
  current_pos = getBrakePot();
  cout << "-set_brake_abs_pos_pot- Current brake position: " << current_pos 
       << endl;
  cout << "-set_brake_abs_pos_pot- Setting the brake to: " << abs_pos << endl;

  // First, figure out what pot value the desired abs_pos would have
  abs_pos_pot=scale_double(abs_pos, 0, 1, min_pot, max_pot);
  // Then, figure out what the relative position change is in pot units
  rel_pos_pot = current_pos - abs_pos_pot;
  // Set threshold to avoid jerkiness.
  if (abs(rel_pos_pot) < .02) {        // 1% error margin
    rel_pos_counts = 0;
  }
  else {
    //Then, figure out how many counts the desired change in pot units 
    //corresponds to
    rel_pos_counts = scale_double(rel_pos_pot, -max_pot+min_pot, 
				  max_pot-min_pot, max_counts-min_counts,
				  -max_counts+min_counts);
    // Now, limit rel_pos_counts for 10Hz operation
    acc_steps=in_range_double(acc, 0, 1);
    acc_steps=scale_double(acc_steps, 0, 1, MIN_ACC, MAX_ACC);
    max_mov = 0.0025*acc; 
    if (max_mov > MAX_MOV_TEN) {
      max_mov=MAX_MOV_TEN; //assume triangular velocity profile
    }
    rel_pos_counts = in_range_double(rel_pos_counts,  - max_mov, max_mov);   
  }

  cout << "-set_brake_abs_pos_pot- Relative change: " << rel_pos_pot 
       << endl;

  /*
  cout << "\n*Min Pot, Max Pot: " << min_pot << ", " << max_pot;
  cout << "\n*Min Count, Max Count: " << min_counts << ", " << max_counts;
  cout << "\n*abs_pos_pot: " << abs_pos_pot << endl;
  cout << "\n*current pot val: " << getBrakePot();
  cout << "\n*pot change: " << rel_pos_pot;
  cout << "\n*rel_pos_counts: " << rel_pos_counts << endl;
  fflush(stdout);
  */

  // Finally, move that many counts
  return set_brake_rel_pos_counts(rel_pos_counts, vel, acc);
}

//This proc sets the position to what you tell it; unsuitable for 10Hz
int set_brake_abs_pos_pot(double abs_pos, double vel, double acc) {
  double abs_pos_pot, rel_pos_pot, rel_pos_counts, current_pos;
  cout<<"moving with set_brake_abs_pos_pot"<<endl;
  if (abs_pos < 0.0 || abs_pos > 1.0)
    return -1;

  current_pos = getBrakePot();
  cout << "-set_brake_abs_pos_pot-Current brake position: " << current_pos 
       << endl;
  cout << "-set_brake_abs_pos_pot-Setting the brake to: " << abs_pos << endl;

  // First, figure out what pot value the desired abs_pos would have
  abs_pos_pot=scale_double(abs_pos, 0, 1, min_pot, max_pot);
  // Then, figure out what the relative position change is in pot units
  rel_pos_pot = current_pos - abs_pos_pot;
  // Set threshold to avoid jerkiness.
  if (abs(rel_pos_pot) < .02) {        // 1% error margin
    rel_pos_counts = 0;
  }
  else {
    /* Then, figure out how many counts the desired change in pot units 
     * corresponds to. */
    rel_pos_counts = scale_double(rel_pos_pot, -max_pot+min_pot, 
				  max_pot-min_pot, max_counts-min_counts,
				  -max_counts+min_counts);
  }

  cout << "-set_brake_abs_pos_pot- relative change: " << rel_pos_pot 
       << endl;

  /*
  cout << "\n*Min Pot, Max Pot: " << min_pot << ", " << max_pot;
  cout << "\n*Min Count, Max Count: " << min_counts << ", " << max_counts;
  cout << "\n*abs_pos_pot: " << abs_pos_pot << endl;
  cout << "\n*current pot val: " << getBrakePot();
  cout << "\n*pot change: " << rel_pos_pot;
  cout << "\n*rel_pos_counts: " << rel_pos_counts << endl;
  fflush(stdout);
  */

  //Then move that many counts
  return set_brake_rel_pos_counts(rel_pos_counts, vel, acc);
}

//******************************************************************
// ******************************************************************
/* This function sets the buffer to the latest commanded brake position, 
 * vel, and acc.
 */
int set_brake_abs_pos_vel(double abs_pos, double vel, double acc) {
  // cout << "set_brake_abs_pos_vel: Setting abs_pos to " << abs_pos << endl;
  *abs_pos_buf = abs_pos;
  *vel_buf = vel;
  *acc_buf = acc;
  return 0;
  cout<<"done moving with set_brake_abs_pos_pot"<<endl;
}

// starts the loop that monitors the buffer above and moves to the most current value
void *start_brake_control_loop(void *arg) {
  int *brake_enabled = (int *) arg;
  char parameters[50];
  // Don't bother setting brake position initially
  // cout<<"setting pos to .5"<<endl;
  // set_brake_abs_pos(.5, .5, .5);
  // cout<<"sleeping"<<endl;
  // usleep(2000000);

  //The following is copied from set_brake_abs_pos_limited, might not need them!!

  // The following commands are copied directly from how the QuickSilverMax
  //  Windows program does it. Sadly, no one knows why.
  //Send a polling command
  ////send_brake_command(0, " ", 0);
  //Clear the polling status word
  ////send_brake_command(1, "65535 ", strlen("65535 "));
  //Get revision info?
  ////send_brake_command(5, " ", 0);
  //Clear the program buffer
  ////send_brake_command(8, " ", 0);

  while (1) {
      // sets up some values to so they only have to be calculated once each loop
      current_brake_pot = getBrakePot();
      double current_abs_pos_buf=*abs_pos_buf;
      double current_vel_buf=scale_double(in_range_double(*vel_buf, 0, 1), 0, 1, MIN_VEL, MAX_VEL);
      double current_acc_buf=scale_double(in_range_double(*acc_buf, 0, 1), 0, 1, MIN_ACC, MAX_ACC);

    // checks if brk_enabled from vdrive is high  
    if (*brake_enabled==1) {

      /*
      Original code uses the following:
        //Limit and scale the velocity, acceleration, and range
        vel=in_range_double(vel, 0, 1);
        vel=scale_double(vel, 0, 1, MIN_VEL, MAX_VEL);

        acc=in_range_double(acc, 0, 1);
        acc=scale_double(acc, 0, 1, MIN_ACC, MAX_ACC);
      */

      // checks to see if break is far enough from the desired point and within acceptable range
      if(current_brake_pot>current_abs_pos_buf &&
	 abs(current_brake_pot-current_abs_pos_buf) > BRAKE_POS_TOLERANCE && 
        current_brake_pot>min_pot) {
        //send brake a negative velocity move

        sprintf(parameters, "%.0f %.0f %d %d ", current_acc_buf, current_vel_buf, 
          STOP_ENABLE_ABS, STOP_STATE_ABS);
	// cout<<"sending brake negative move"<<endl;
        send_brake_command(15, parameters, strlen(parameters));
        // cout<<"current_acc_buf, current_vel_buf, STOP_ENABLE_ABS, STOP_STATE_ABS "
        // <<parameters<<" current_brake_pot "<<current_brake_pot<<" current_abs_pos_buf "
        // <<current_abs_pos_buf<<endl;
	brk_case = 1;
	brk_current_abs_pos_buf = current_abs_pos_buf;
	brk_current_acc_buf = current_acc_buf;
	brk_current_vel_buf = current_vel_buf;
	brk_current_brake_pot = current_brake_pot;
      }
      // checks to see if break is far enough from the desired point and within acceptable range
      else if(current_abs_pos_buf > current_brake_pot &&
	 abs(current_brake_pot-current_abs_pos_buf) > BRAKE_POS_TOLERANCE && 
	 current_brake_pot<max_pot) {
        //send brake a positive move
        sprintf(parameters, "%.0f %.0f %d %d ", current_acc_buf, -current_vel_buf,
          STOP_ENABLE_ABS, STOP_STATE_ABS);
	//          cout<<"sending brake negative move"<<endl;
        send_brake_command(15, parameters, strlen(parameters));
        /*
        cout<<"Parameters for the brake 
        command"<<parameters<<current_brake_pot<<current_abs_pos_buf<<endl;
        */
	//        cout<<"current_acc_buf, current_vel_buf, STOP_ENABLE_ABS, STOP_STATE_ABS "
	//        <<parameters<<" current_brake_pot "<<current_brake_pot<<" current_abs_pos_buf "
	//        <<current_abs_pos_buf<<endl;
	brk_case = 2;
	brk_current_abs_pos_buf = current_abs_pos_buf;
	brk_current_acc_buf = current_acc_buf;
	brk_current_vel_buf = current_vel_buf;
	brk_current_brake_pot = current_brake_pot;
      }
      else {
        //send brake stop if it is either close enough or out of range
        sprintf(parameters, "%.0f %.0f %d %d ", current_acc_buf, 0.0,
          STOP_ENABLE_ABS, STOP_STATE_ABS);
	//	cout<<"sending brake stop"<<endl;
        send_brake_command(15, parameters, strlen(parameters));
        /*
        cout<<"Parameters for the brake 
        command"<<parameters<<current_brake_pot<<current_abs_pos_buf<<endl;
        */
	//        cout<<"current_acc_buf, 0-current_vel_buf, STOP_ENABLE_ABS, STOP_STATE_ABS "
	//        <<parameters<<" current_brake_pot "<<current_brake_pot<<" current_abs_pos_buf "
	//        <<current_abs_pos_buf<<endl;
	brk_case = 3;
	brk_current_abs_pos_buf = current_abs_pos_buf;
	brk_current_acc_buf = current_acc_buf;
	brk_current_vel_buf = current_vel_buf;
	brk_current_brake_pot = current_brake_pot;
      }
      usleep(10000);
    } else {
      brk_case = 4;
      /* Brake has been disabled; stop moving */
      //      sprintf(parameters, "%.0f %.0f %d %d ", current_acc_buf, 0,
      //	      STOP_ENABLE_ABS, STOP_STATE_ABS);
      //      send_brake_command(15, parameters, strlen(parameters));
      //    cout<<"Parameters for the brake command--loop disabled"<<parameters<<current_brake_pot<<current_abs_pos_buf<<endl;
    }
  }
}
//***********************************************************************


// Sets the brake's relative position in 0-1 units
// still in the full-scale units. i.e. calibrated zero position doesn't matter.
//  if we change this, we'd need to change calib_brake.
int set_brake_rel_pos(double rel_pos, double vel, double acc) {
  char parameters[50];
  char response[50];

  //Limit and scale the velocity, acceleration, and range
  vel=in_range_double(vel, 0, 1);
  vel=scale_double(vel, 0, 1, MIN_VEL, MAX_VEL);

  acc=in_range_double(acc, 0, 1);
  acc=scale_double(acc, 0, 1, MIN_ACC, MAX_ACC);

  // leaving relative position in absolute scale (i.e. 1 means the full
  // length of the actuator itself.
  rel_pos=in_range_double(rel_pos, -1, 1);
  //rel_pos=in_range_double(rel_pos, our_min_pos-getBrakePot(), our_max_pos-getBrakePot());
  rel_pos=scale_double(rel_pos, -1, 1, -MAX_POS, MAX_POS);
  //rel_pos=scale_double(rel_pos, -1, 1, -MAX_POS*(our_max_pos-our_min_pos), MAX_POS*(our_max_pos-our_min_pos));

  //send_brake_command(5, " ", 0);
  //send_brake_command(0, " ", 0);
  //send_brake_command(1, "65535 ", strlen("65535 "));
  //send_brake_command(3, "500000 ", strlen("500000 "));
  //Send a polling command
  send_brake_command(0, " ", 0);
  //Clear the polling status word
  send_brake_command(1, "65535 ", strlen("65535 "));
  //Get revision info?
  send_brake_command(5, " ", 0);
  //Clear the program buffer
  send_brake_command(8, " ", 0);
  //Start the program download
  send_brake_command(9, " ", 0);

  //Move Relative, Velocity Based (MRV)
  sprintf(parameters, "%.0f %.0f %.0f %d %d ", rel_pos, acc, vel, 
      STOP_ENABLE_ABS, STOP_STATE_ABS);
  send_brake_command(135, parameters, strlen(parameters));

  //End the program download
  send_brake_command(128, " ", 0);
  //Run the program
  receive_brake_command(10, " ", 0, response, 50, 1);

  if(strcmp(response, BRAKE_ACK)==0) {
    return 0;
  } else {
    return -1;
  }
}


int set_brake_rel_pos_counts(double rel_pos, double vel, double acc) {
  char parameters[50];
  char response[50];

  //Limit and scale the velocity and acceleration
  vel=in_range_double(vel, 0, 1);
  vel=scale_double(vel, 0, 1, MIN_VEL, MAX_VEL);

  acc=in_range_double(acc, 0, 1);
  acc=scale_double(acc, 0, 1, MIN_ACC, MAX_ACC);

  //rel_pos=in_range_double(rel_pos, -MAX_POS*(our_min_pos-getBrakePot()), MAX_POS*(our_max_pos-getBrakePot()));

  //send_brake_command(5, " ", 0);
  //send_brake_command(0, " ", 0);
  //send_brake_command(1, "65535 ", strlen("65535 "));
  //send_brake_command(3, "500000 ", strlen("500000 "));
  //Send a polling command
  send_brake_command(0, " ", 0);
  //Clear the polling status word
  send_brake_command(1, "65535 ", strlen("65535 "));
  //Get revision info?
  send_brake_command(5, " ", 0);
  //Clear the program buffer
  send_brake_command(8, " ", 0);
  //Start the program download
  send_brake_command(9, " ", 0);

  //Move Relative, Velocity Based (MRV)
  sprintf(parameters, "%.0f %.0f %.0f %d %d ", rel_pos, acc, vel, 
      STOP_ENABLE_ABS, STOP_STATE_ABS);
  send_brake_command(135, parameters, strlen(parameters));

  //End the program download
  send_brake_command(128, " ", 0);
  //Run the program
  receive_brake_command(10, " ", 0, response, 50, 1);

  if(strcmp(response, BRAKE_ACK)==0) {
    return 0;
  } else {
    return -1;
  }
}

/* Get the actuator position in counts -- be careful trusting this value.
 *                                        see brake.h 
 */
int get_brake_pos() {
  char buffer[50];
  int command, upper_word, lower_word, position;

  receive_brake_command(12, "1 ", strlen("1 "), buffer, 50, 1);  
  sscanf(buffer, "# 10 %X %X %X", &command, &upper_word, &lower_word);

  upper_word <<= 16;
  position=lower_word+upper_word;

  if(command == 12) {
    return position;
  } else {
    return -1;
  }
}


int calib_brake() {
  float pos;
  int val;
  char comm;
  //Restart the Actuator
  send_brake_command(4, " ", 0);
  //Wait until the actuator is ready again
  while(brake_ready()!=0) {};
  do {
    printf("\nCurrent Position(current setting, counts): %d", get_brake_pos());
    printf("\nCurrent Position(pot reading, 0-1): %f\n", getBrakePot());
    printf("Enter command (c: continue to move actuator z: set zero(full-out?), f: set full-position(all-in?), q: quit): ");
    cin>>comm;
    cout<<endl;
    if(comm=='c'){
	  printf("\nEnter rel pos change (0-1): ");
	  scanf("%f", &pos);
	  if (getBrakePot()+pos <= 0.05 || getBrakePot()+pos >= 1) {
    char cont = 'c';
    while (cont != 'y' && cont != 'n') {
      cout << "This may break the actuator, continue? (y=yes,n=no):";
      cin >> cont;
      cout << endl;
    }
    if (cont == 'y') {
      val = set_brake_rel_pos(pos, 0.02, 0.02);
	  printf("\nVal: %d\n", val);  
    }
     }
     else {
    val = set_brake_rel_pos(pos, 0.05, 0.05);  //orig 0.9
    printf("\nVal: %d\n", val);  
      }
    }
    while(brake_ready()!=0) {
      // cout << "waiting for the brake actuator to finish" << endl;
    };    
    //usleep(1500000);
    //sleep(2);          // wait for the actuator to finish moving?
  } while(comm!='q' && comm!='z' && comm!='f');     
  if(comm=='z') { 
    //Restarting it gets rid of moving error
    //send_brake_command(4, " ", 0);
    //Wait until the actuator is ready again
    //while(brake_ready()!=0) {};
    //set_brake_rel_pos_counts(-10000, 0.06, 0.06);
    //sleep(2);
    //set_brake_rel_pos_counts(ZERO_POS+10000, 0.05, 0.05);
    //Make the current position the 0 position
    our_min_pos = getBrakePot();
    min_pot=getBrakePot();
    min_counts=get_brake_pos();
    cout << "\n*min_pot set to " << min_pot;
    cout << "\n*min_counts set to " << min_counts;
    return send_brake_command(145, " " , 0);
  }
  else if(comm=='f') {
    //Restarting it gets rid of moving error
    //send_brake_command(4, " ", 0);
    //Wait until the actuator is ready again
    //while(brake_ready()!=0) {};
    //sleep(2);
    //Make the current position the 1 position...???
    //return send_brake_command(145, " " , 0);
    our_max_pos = getBrakePot();
    max_pot=getBrakePot();
    max_counts=get_brake_pos();
    cout << "\n*max_pot set to " << max_pot;
    cout << "\n*max_counts set to " << max_counts;
  }
  return 0;
}


//Helper functions for limiting and scaling numbers
double in_range_double(double val, double min_val, double max_val) {
  if(val<min_val) return min_val;
  if(val>max_val) return max_val;
  return val;
}

double scale_double(double val, double old_min_val, double old_max_val, double new_min_val, double new_max_val) {
  double old_range, new_range;

  old_range=old_max_val-old_min_val;
  new_range=new_max_val-new_min_val;

  return ((new_range*(val-old_min_val)/old_range)+new_min_val);
}

/* TLL.cc - reading the brake force sensor through parallel interface
           - reading the linear pot on the brake actuator for act position
   Revision History: 7/17/2003  Created                      Sue Ann Hong
                     7/18/2003  Finished ver.1               Sue Ann Hong
		     8/17/2003  Added linear pot inteface    Sue Ann Hong
 Note: accuracy ~.005 (for ratio)
       max 1446-1456
 */
using namespace std;

// remember the parallel port is PP_BTENS (or something like that)

// don't need to call this if init_accel() is called. in fact, one should
// always use init_accel().
int initBrakeSensors() {
  return pp_init(tllpport, PP_DIR_IN);  /* data lines: read */
}

double getBrakeForce() {
  pp_set_bits(tllpport, ACCEL_CS, 0);     // make sure accel doesn't move
  int tllOutput;
  /* Set ALE, START, OE */
  pp_set_bits(tllpport, BT_ALE, 0);
  pp_set_bits(tllpport, BT_START, 0);
  //usleep(1);
  pp_set_bits(tllpport, 0, BT_ALE);
  pp_set_bits(tllpport, 0, BT_START);
  usleep(100);
  pp_set_bits(tllpport, BT_OE, 0);
  double force = (double) pp_get_data(tllpport); 
  pp_set_bits(tllpport, 0, BT_OE);

  /* Get "voltage" */
  // A fast change in the force -- what happens?
  //  double force = (double) pp_get_data(tllpport); 
  cout << "voltage div #: " << force << endl;
  cout << "voltage: " << force/BT_NUM_DIV*(BT_VREF_HIGH-BT_VREF_LOW)+BT_VREF_LOW << endl;
  /* Convert to lbs */
  return ((force-7) / BT_NUM_DIV * (BT_VREF_HIGH - BT_VREF_LOW) + BT_VREF_LOW) * BT_MAX_FORCE / BT_MAX_OUTPUT;
  // return ((force / BT_NUM_DIV * (BT_VREF_HIGH - BT_VREF_LOW) + BT_VREF_LOW) + BT_CORR_FACTOR) * BT_MAX_FORCE / BT_MAX_OUTPUT;
}

/* getBrakePot
  Return value: (double) position of the brake actuator between 0 and 1
*/
double getBrakePot() {
  pp_set_bits(tllpport, ACCEL_CS, 0);     // make sure accel doesn't move
  pp_set_bits(tllpport, 0, BT_OE);        // make sure pot is the only input 
  pp_set_bits(tllpport, 0, BPOT_RC);
  pp_set_bits(tllpport, 0, BPOT_CS);
  usleep(1); // <65ns
  pp_set_bits(tllpport, BPOT_CS, 0);
  usleep(BPOT_TBUSY);    
  pp_set_bits(tllpport, BPOT_RC, 0);
  pp_set_bits(tllpport, 0, BPOT_CS);
  usleep(1); // <83ns
  // read 
  int lowerEight = (int) pp_get_data(tllpport);
  int upperFour = (int) pp_get_bit(tllpport, BPOT_D8) 
    + (int) (pp_get_bit(tllpport, BPOT_D9)<<1)
    + (int) (pp_get_bit(tllpport, BPOT_D10)<<2);
  int sign = (int) pp_get_bit(tllpport, BPOT_D11);
  pp_set_bits(tllpport, BPOT_CS, 0);  
  int potReading = lowerEight + (upperFour<<8);

  if (sign == 1) {  // negative probably means zero
    //dbg_error("WARNING: how can the wiper voltage be NEGATIVE???");
    //dbg_error("Potreading: %d", potReading);
    // return -1;
    // cout << "I'm going to assume that it's zero on return." << endl;
    return current_brake_pot; //part of hack below
  }  
  else if (sign == 0) {
    //    cout << "Voltage: " 
    //         << (potReading*(BP_VREF_HIGH-BP_VREF_LOW)/BP_NUM_DIV + BP_VREF_LOW) 
    //         << endl;
    brk_pot_voltage = (potReading/BP_NUM_HIGH - BP_MIN)/(BP_MAX-BP_MIN);
    /* Return the value potReading scaled to BP_MAX, BP_MIN range. */
    //    return (potReading/BP_NUM_HIGH - BP_MIN)/(BP_MAX-BP_MIN);
 
    //trying the version 2.0 hack here
    if(brk_pot_voltage == 0) {
      brk_pot_voltage = current_brake_pot;
	}
    return brk_pot_voltage;
}
  else {
    // cout << "If you're reading this, sign != 0 or 1. Just not possible..." << endl;
    dbg_error("can't read parallel port for brake");
    return 0;
  }
}
