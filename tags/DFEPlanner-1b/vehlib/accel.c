/*
 * accel.c - code to control the accelerometer position
 *
 * Original author: Jeremy G.
 *                  Sue Ann Hong
 * Revised: Richard Murray
 * 2 January 2003
 *
 */

#include <stdlib.h>
#include "accel.h"
#include "vehports.h"
#include "sparrow/dbglib.h"

/* Local variables */
int last_pos = MAX_POT_VAL;		/* last position (in digipot units) */
static int accelpport =	PP_GASBR;       /* parallel port location  */
      
int acc_commanded;   		    /* copy of the commanded acceleration */

/* Intialize the accelerator actuator */
/* TBD: this function should be in the form accel_<action> */
int init_accel() {
  if(pp_init(accelpport, PP_DIR_IN) >= 0) {
    calib_accel();
    dbg_info("Accel actuator init-ed on port %d\n", accelpport);
    return 1;
  } else {
    dbg_info("\nError opening parallel port %d\n", accelpport);
    return -1;
  }
}

/* Calibration the accelerator actuator */
int calib_accel() {
  //Set last_pos so that the actuator moves double what it should
  last_pos=255;

  //Set the actuator to the fully out position
  set_accel_abs_pos(0.0);
}


//Returns the gas actuator's position on the 0-1 scale
double get_accel_pos() {
  return (((double) (last_pos - MIN_POT_VAL)) / (MAX_POT_VAL - MIN_POT_VAL));
}

//Sets the gas actuator's position based on a float, which is on the 0-1 scale
//0 sets the actuator to let the engine idle
//1 is full throttle
// 
// 4 Jan 04, RMM: note that throttle counts are *opposite* of this
//  Set throttle count to MAX when we are at idle.
//
int set_accel_abs_pos(double position) {
  //  printf("\nSetting position to %f\n", position);
  if(position == 0) {
    // If position is 0, increment for twice normal amount to be sure
    change_digipot_by(255);
  } else if(position == 1) {
    //If position is 1, decrement for twice normal amount to be sure
    change_digipot_by(-255);
  } else {
    // Otherwise, convert from the 0-1 scale to the 127-0 scale, and 
    // call the function to change the digipot
    int digipot_pos;
    digipot_pos = MAX_POT_VAL - 
      ((int) ((MAX_POT_VAL - MIN_POT_VAL) * position));
      
    change_digipot_by(digipot_pos-last_pos);
  }
  return 0;
}

//Changes the digipot by pos_change, on the 0-127 scale
int change_digipot_by(int pos_change) {
  pp_set_bits(accelpport, 0, ACCEL_CS);

  //Update last_pos, and make sure it stays within range
  last_pos+=pos_change;
  last_pos=in_range(last_pos, MIN_POT_VAL, MAX_POT_VAL);
  
  acc_commanded = last_pos;

  //If the desired digipot value is higher...
  if(pos_change > 0) {
    //Set UD high to indicate incrementing
    //~~  pp_set_bits(PP_GASBR, ACCEL_UD, 0);
    pp_set_bits(accelpport, ACCEL_UD, 0);
    //Cycle the clock until the desired pos_change is reached
    while(pos_change > 0) {
      //~~  pp_set_bits(PP_GASBR, ACCEL_CLK, 0);
      //~~  pp_set_bits(PP_GASBR, 0, ACCEL_CLK);
      pp_set_bits(accelpport, ACCEL_CLK, 0);
      pp_set_bits(accelpport, 0, ACCEL_CLK);
      pos_change--;
    }
  }
  //Else if the desired digipot value is lower...
  else if(pos_change < 0) {
    //Set UD low to indicate incrementing
    //~~    pp_set_bits(PP_GASBR, 0, ACCEL_UD);
    pp_set_bits(accelpport, 0, ACCEL_UD);
    //Cycle the clock until the desired pos_change is reached
    while(pos_change < 0) {
      //~~  pp_set_bits(PP_GASBR, ACCEL_CLK, 0);
      //~~  pp_set_bits(PP_GASBR, 0, ACCEL_CLK);
      pp_set_bits(accelpport, ACCEL_CLK, 0);
      pp_set_bits(accelpport, 0, ACCEL_CLK);
      pos_change++;
    }
  }
  pp_set_bits(accelpport, ACCEL_CS, 0);
  return 0;
}

//Function to make sure min_val<=val<=max_val, and if not set val properly
//TBD: this sort of function does not belong here
int in_range(int val, int min_val, int max_val) {
	if(val<min_val) return min_val;
	if(val>max_val) return max_val;
	return val;
}
