/*  static.hh
 *  static map files
 *
 *  Alan Somers     15-Feb-04   Created
 *  Rocky Velez     20-Feb-04   Modified
 */

//flags for population
#define G_NO_POP        0x0
#define G_BLANK_POP     0x1     //goodnesses all 0
#define G_FILLED_POP    0x2     //goodnesses all 255
#define G_STATIC_POP    0x4
#define E_NO_POP        0x0
#define E_ZERO_POP      0x8  
#define E_STATIC_POP    0x10

#define TRUE            1
#define FALSE           0

using namespace std;

//Useful struct for drawing
struct pixel {
    int N;
    int E;
};
inline bool pix_is_eq(pixel A, pixel B) {return (A.N == B.N) && (A.E == B.E);}

//Static Map Class functions

class staticMap {
private:
  unsigned char*  __goodnesses;
  int* __elevations;
  int __LBn;                    //northing of left bottom
  int __LBe;                    //easting of left bottom
  int __sizeE, __sizeN;
  bool __isGood;                //true iff goddnessess has been allocated
  bool __isElev;                //true iff Elev has been allocated
  int __gNpop, __gSpop, __gEpop, __gWpop; //coordinates of goodness populated region
  int __eNpop, __eSpop, __eEpop, __eWpop; //coordinates of elevation populated region

  //dynamic allocation functions
  void allocGood();
  void allocElev();
  void fillMapGood(unsigned char color);
  void fillMapElev(int elev);
public:
  //Empty Constructor
  staticMap() {} 
  //Default Constructor (default size = 500, default flags = 0)
  staticMap(int LBn, int LBe, int sizeN, int sizeE, int flags);
  //Copy Constructor
  staticMap(const staticMap &other);
  
  //Destructor
  ~staticMap();
  
  //Accessors
  int getLeftBottomUTMe() const;
  int getLeftBottomUTMn() const;
  unsigned char getCellGood(int UTMn, int UTMe) const;
  int getCellElev(int UTMn, int UTMe) const;
  
  //Mutators
  void setCellGood(int UTMn, int UTMe, unsigned char desiredGoodness);
  void setCellElev(int UTMn, int UTMe, int desiredElevation);
  void rePopulate(int northN, int eastE, int southN, int westE, int flags);
  void fakePopulate(int fakeLBn, int fakeLBe, int northN, int eastE, int southN, int westE, int flags);
  
  void drawCircle(double centerN, double centerE, double radius, unsigned char desiredGoodness);
  void drawRectangle(double centerN, double centerE, double length, double width, double angle, unsigned char desiredGoodness);
  
  //I/O methods
  int write_file(const char* filename, int flags); 
}; 
