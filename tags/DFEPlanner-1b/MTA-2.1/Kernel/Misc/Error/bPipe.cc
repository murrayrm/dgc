#include "misc/Pipes/bPipe.hh"
#include <string>
#include <list>
#include <iostream>

using namespace std;

bPipe& bPipe::operator << (const int& RHS) {
  push(&RHS, sizeof(int));
  return (*this);
}
bPipe& bPipe::operator << (const unsigned int& RHS) {
  push(&RHS, sizeof(unsigned int));
  return (*this);
}

bPipe& bPipe::operator << (const short& RHS) {
  push(&RHS, sizeof(short));
  return (*this);
}

bPipe& bPipe::operator << (const long& RHS) {
  push(&RHS, sizeof(long));
  return (*this);
}

bPipe& bPipe::operator << (const float& RHS) {
  push(&RHS, sizeof(float));
  return (*this);
}

bPipe& bPipe::operator << (const double& RHS) {
  push(&RHS, sizeof(double));
  return (*this);
}

bPipe& bPipe::operator << (const char& RHS) {
  push(&RHS, sizeof(char));
  return (*this);
}

bPipe& bPipe::operator << (const std::string& RHS) {
  push(RHS.c_str(), RHS.length());
  (*this) << (char) 0x00;
  return (*this);
}

/************************************** */









bPipe& bPipe::operator >> (int& RHS) {
  pop(&RHS, sizeof(int));
  return (*this);
}
bPipe& bPipe::operator >> (unsigned int& RHS) {
  pop(&RHS, sizeof(unsigned int));
  return (*this);
}

bPipe& bPipe::operator >> (short& RHS) {
  pop(&RHS, sizeof(short));
  return (*this);
}

bPipe& bPipe::operator >> (long& RHS) {
  pop(&RHS, sizeof(long));
  return (*this);
}

bPipe& bPipe::operator >> (float& RHS) {
  pop(&RHS, sizeof(float));
  return (*this);
}

bPipe& bPipe::operator >> (double& RHS) {
  pop(&RHS, sizeof(double));
  return (*this);
}

bPipe& bPipe::operator >> (char& RHS) {
  pop(&RHS, sizeof(char));
  return (*this);
}

bPipe& bPipe::operator << (std::string& RHS) {
  char ch;
  RHS = "";

  pop(ch, sizeof(char));
  while( ch != 0x00 ) {
    RHS += ch;
    pop(ch, sizeof(char));
  }
  return (*this);
}
