#ifndef Friends_HH
#define Friends_HH

#include "Modules.hh"
#include "DGC_MODULE.hh"
#include "KernelDef.hh"

#include "Misc/Structures/ModuleInfo.hh"
#include "Misc/Socket/Socket.hh"
#include "Misc/File/File.hh"
#include "Misc/Mail/Mail.hh"
#include "Misc/Time/Timeval.hh"

#include <list>
#include <map>

#include <boost/shared_ptr.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

using namespace std;
using namespace boost;



void Register(shared_ptr<DGC_MODULE> dgc);
void Unregister(int Mailbox);

  
// Kernel Startup and Shutdown functions, 
// plus a flag function - Kernel.cc
void StartKernel(int mtacouton=false); // start of program, allow module to operate
void ForceKernelShutdown(); // Flags for shutdown
int  ShuttingDown(); // returns ShutdownFlag
  
// Time accessors - Time.cc
Timeval Now(); // returns current time

// System Map Functions - SystemMap.cc
TCP_ADDRESS MyTCPAddress(); // returns TCP Address of current module
DGC_MODULE* FindLocalModule(int mailbox_number);
MailAddress FindModuleAddress(std::string moduleName);
MailAddress FindModuleAddress(int ModuleType); 
list<ModuleInfo> LocalModules();
list<ModuleInfo> AllModules();

// Mail Functions
void  SendMail(Mail & ml, File f= File()); // puts mail in a queue to be sent
Mail  SendQuery(Mail & ml, File f= File()); // sends mail immediately and waits for respons (returned by function)


#endif
