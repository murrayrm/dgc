/* VDrive.hh - header file for communicating with vdrive MTA interface
 *
 * Richard Murray
 * 3 January 2004
 * 
 * This file defines the format for motion command that is sent
 * to Vdrive.
 *
 */

#ifndef VDRIVE_HH
#define VDRIVE_HH

#include "MTA/Misc/Time/Timeval.hh"
#include "MTA/Misc/Mail/Mail.hh"

struct VDrive_CmdMotionMsg
{
  Timeval Timestamp;		// time stamp when data was sent
  double velocity_cmd;		// desired velocity
  double steer_cmd;		// commanded steering angle
};

// enumerate all module messages that could be received
namespace VDriveMessages {
  enum {

    //Input Commands
    CmdMotion,			// Command a given motion
    SetAccelOff,		// Set accel_off
    SetSteerOff,		// Set steer_off
    SetSpeedRef,		// Set speed_ref
    SetCruiseGain,		// Set cruise_gain

    //Calibrate commands
    ThrottleCalib,		// Performs a throttle calibration
    BrakeCalib,			// Performs a brake calibration
    SteerCalib,			// Performs a throttle calibration
    
    //Reset commands
    ThrottleReset,		// Perform a throttle reset
    BrakeReset,			// Perform a brake reset
    SteerReset,			// Perform a steer reset

    //Enable commands
    CaptureEnable,                  // Sets the capture mode on or off
    ThrottleEnable,		// Sets the throttle actuator on or off
    BrakeEnable,		// Sets the brake actuator on or off
    SteerEnable,		// Sets the steering actuator on or off
    CruiseEnable,		// Sets the cruise controller on or off

    //Misc commands
    SetMode,			// Set mode to off, auto, manual, etc
    DoEstop,			// Performs an estop_pause
    DumpFilename,		// Inputs name of dumpfile
    SteerInc,			// Performs a steer_inc
    SteerDec,			// Performs a steer_dec
    AccelInc,			// Performs an accel_inc
    AccelDec,			// Performs an accel_dec
    FullStop,			// Performs a full_stop
    UserQuit,			// Performs a user_quit

    // Added by Ike (to get VStarPackets.cc to compile
    // ignore for now)
    Get_Steer,         // returns a steer packet
    Get_Throttle,      // returns a throttle packet
    Get_Brake,

    Get_VDriveStack,   // grabs a vdrive display stack

    NumMessages			// Placeholder (not used)
  };
};

#endif

