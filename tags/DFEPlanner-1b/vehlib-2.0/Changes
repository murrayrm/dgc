Changes for vehlib 2.0 package
RMM, 21 Jan 04

7 Mar 04, RMM: performance tuning, 7:30 pm
  * put in code to treat any speed less than SPPED_MIN (0.001) as zero

7 Mar 04, RMM: performance tuning, 6:45 pm
  * reset speed to 2500000

7 Mar 04, RMM: VManage changes (in RaceModules/VManage, not here)
  * Added -o flag to ignore OBD II errors
  * Set transmission into third gear on start

7 Mar 04, RMM: Santa Anita performance tweaks
  * added cruise_stop to display to allow online tuning; default unchanged
  * put in steer_off default of -0.12, based on El Mirage testing
  * set version number to v2.1d in Makefile
  * added timestampe to vstate when kfilter not running and IMU not running
  * put brake_min_pot and max_pot on display to allow tuning
  * added steer_off change (lost???)
  * Reset brake limits to 0.25-0.75 based on Santa Anita testing
  * Jeff reset buffering on steering to speed up response
  * Jeff reimplented commands for setting speed to 20 vs 12
  * Jeff commented out steering code that was forcing slow response in vdrive

7 Mar 04, RMM: wrote brake_reset
  * based on braketest
  * sets brake position through simple velocity commands

**** v2_1c released ****

6 Mar 04, RMM: El Mirage changes
  * added steer_off to autonomous position to allow trimming steering

5 Mar 04, RMM: steering performance tuning
  * Added logic to break out of sleep early if new MTA command is sent
  * Re-installed code to rewrite command after 10 cycles of the same
  * Added flags to display to try to debug hanging problems
  * Got rid of wait in steer_setposition until moving stopped [helped!] 
  * Changed braking table in cmd_to_t_PID to scale from 0 to -1

5 Mar 04, RMM: integration of new brake into vdrive
  * Tried to set max speed to 4000000 (double previous value)
      + seemed to cause faults on large moves; need to fix this
      + for now, reset speed to lower value (2000000)
  * Reset brake limit to 0.75 (was 0.85 previously)

5 Mar 04, RMM: fixes to brake2 for animatics controller
  * brake_port wasn't being initalized properly on startup
  * removed port argument from commands; OK to use brake_port
  * set up on port 6 as default, to avoid sending to old brake
  * temporarily removed baud rate change; should work OK without this

5 Mar 04, RMM: minor changes based on bugs from yesterday
  * switched over to indivdual scripts instead of a template
  * reduced sleep time in vdrive restart to 0
  * vmanage autonomous mode now turns on cruise; remote turns off
  * Makefile creates binary directory on install

4 Mar 04, RMM: changes for field testing
  * Created v2_1b branch so that I can do testing independent of trunk
  * Defined BRAKE_OLD and recompiled => old brake code (tested: no)
  * Defined CRUISE_OLD and recompiled => old cruise code (tested: no)
  * Added estop flag to display to see when it is being commanded
  * Added VState and VManage counters to see what is going on

**** v2_1b branch *****

3 Mar 04, WH, JR: Changes to brake2 to use the new actuator.  At the top of
	brake2.h, comment in or out the define for BRAKE_OLD to select which 
	actuator the code is compiled for.  Currently, the new brake driver
	does not support braketest.


28 Feb 04, RMM: changes to integrate vmanage
  * got rid of estop "mode"; use function calls instead
  * minor mods to Will's estop code (me being anal):
      + renamed user_estop_call() to user_estop_toggle()
  * moved setting of estop_call_flag inside estop_pause/resume
      + allows vmanage to call the same code as keyboard
  * did a first cut at logic to slow vehicle down before slamming on brakes
    this still needs some work (posted in bugzilla for Will to have a look)

28 Feb 04, RMM: changes to get vdrive working on Tahoe
  * fixed logic error in display flags; prompt was disappearing in vdrive

28 Feb 04, RMM: more vremote implementation
  * copied more string variables; created functions for saving data

28 Feb 04, RMM: merged v2.1 branch changes into main trunk
  * branches are quite tedious; probably not worth using unless set up right
  * added vremote to default list of things to make

28 Feb 04, RMM: added estop flag
  * vdrive -e will turn off vmanage error message for estop

28 Feb 04, WNH: vdrive added reverse capability
  *Added code to the drive mode switch that accepts a negatice speed_ref value
   and switches to the correct gear to proceed
  *Switching should work for not, but I dont think the cruise controller will handle
   the negative values, and OBDII transmission speed needs to be integrated into the
   code rather than the crude sleeps it uses now
  *see inline comments for info on this

28 Feb 04, WNH: brake2 fixes
  *Redefined BRAKE_ACC, BRAKE_VEL seperately in brake2.h because RMM's changed
   caused created some logic errors with counts values inputed where 0-1
   values were expected.  The code fortuitously still worked because of 
   luck and robustness I had built in to the functions he used.
  *fixed counts vs 0-1 problem in brake_pause() and brake_write()
  *fixed math error in brake_setmav
  *tried to fix problems with full_stop and brake_pause not working, need to
   try on titanium before I know they worked

27 Feb 04, RMM: vmanage changes
  * Switched to SERIAL2 as default; appears to be working

27 Feb 04, RMM: miscellaneous changes
  * got rid of #include vehlib/VState.hh in frames.hh

27 Feb 04, RMM: write shell script for starting vdrive/vstate/vmanage
  * Checks to make sure you are on the right machine
  * Restarts process if it ends unexpectedly
  * Uses single template file + sed to create scripts

27 Feb 04, RMM: vstate/vdrive non-display mode changes
  * Commented out sparrow capture code in vstate
  * Added -d flag to turn off display and not generate prompts
  * Fixed magnetometer heading read to avoid losing error prompt 

26 Feb 04, RMM: started working on MTA-ized vdrive
  * turned on VMANAGE define in vdrive; working!
  * serial2: not working (serial_read_until); had to leave commented out
  * added -d flag to turn off sparrow display

26 Feb 04, RMM: created v2.1 (branch)
  * used cvs tag -bv2_1 to create branch from v2_0e
  * checked out branch (this file should be there)

***** v2_0e *****

26 Feb 04, RMM: various fixes
  * put in #ifdefs in serial.c if SERIAL2 is defined
  * reset moving and holding limits to large numbers (zero was wrong)
  * put in #defines around all serial code; recompiled *without* SERIAL2

26 Feb 04, RMM: more brake2 fixes
  * Implemented brake_status that checks I/O and internal registers as well
  * Set moving and holding error limits to zero
  * Reset supply voltage from truck to 36V with adjustment screw
  * Brake is now working in vdrive!!

26 Feb 04, RMM: brake2 fixes
  * Use brake_case to only command motion when needed
  * Fixed brake_home in the same manner
  * Added define for brake cycle time
  * Tightened up brake tolerance and decreased cycle time (10 msec)
  * Added parallel port locking in brake_read(); eliminated read errors

25 Feb 04, RMM: intial release of brake2 driver
  * same functionality as brake.c
  * working standalong, but still whining in vdrive

24 Feb 04, RMM: brake debugging
  * moved BRAKE_ACC, BRAKE_VEL, MAX_BRAKE to brake.c instead of vdrive.c
      + needed to do this to test out braketest

22 Feb 04, RMM: field updates
  * added magnetometer offset that can be changed from screen
  * changed order of variables in atan2 call for kfilter_flag == 0 case
  * played around with variations on steerings to stop hangs
      + never found anything absolutely reliable
  * had to compile vstate with old serial code to get working (??)

15 Feb 04, RMM: install target changes
  * Changed install target in Makefile to remove version number
  * This was causing problems for the rest of the team

15 Feb 04, RMM: vstate modifications to work around IMU
  * added 'k' flag to turn off Kalman filter
  * added code to get state data from best avaiable source when kfilter off
  * added -v flag processing to stop before entering display
  * kalman filter replace runs in IMU loop; need to use -i if IMU is broke

14 Feb 04, RMM: vtest modes
  * added -f flag to set frequency

13 Feb 04, RMM: created vtest
  * Prelimary version; sends sinusoidal commands to vdrive
  * Modified vdrive to accept velocity commands in autonomous mode

7 Feb 04, RMM: prep for embedded systems team test
  * added check in serial_open_* to make sure serial port is OK
  * added back display flags for steering
  * fixed bug in serial code: could accidently close stdin
     + serial_close would close whatever was listed in the array
     + by default, this was zero (= stdin)
     + steer_open was calling serial_close, which caused this problem
  * added processing of long arguments (in addition to short)
  * switched order of keys
  * cleaned up display and rearranged things a bit
  * added calibrate button to display for steering
  * tested out steering code; something wrong in logic (Jeff to fix)

30 Jan 04, RMM: added data logging back in
  * used printfs withing capture thread instead of sparrow
  * put in logic for locking access to capture file access
  * removed all sparrow channel code (no longer required)
  * Set up variables to be stored in same way as simulation file (bug #194)

30 Jan 04, RMM: Makefile changes
  * added vdrive.ini default back in
  * set things up to compile vdparm table

21 Jan 04, RMM: initialization of vehlib 2.0
  * created Changes file
  * initial version of vdrive with almost everything commented out
