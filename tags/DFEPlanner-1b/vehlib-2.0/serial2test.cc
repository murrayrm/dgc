#include <iostream>

#include <string.h>
#include "serial2.hh"
#include "MTA/Misc/Time/Timeval.hh"

#include <string>

using namespace std;

char message[] = "This is a test message.  It may destruct at any time.  Hopefully everything will work fine.\r\n This is line 2.  The end.";


char readBuffer[500];

int port = 6;

void clearbuffer(char* buffer) {
  
  for(int i=0; i<500; i++) buffer[i] = (char) 0;
  
}


int main () {

  cout << "Trying serial open advanced" << endl;
  if(serial_open_advanced(port, B57600, 
			  SR_PARITY_DISABLE | SR_SOFT_FLOW_DISABLE | 
			   SR_HARD_FLOW_DISABLE | SR_TWO_STOP_BIT) != 0) {
    cout << " ---- ERROR OPENING PORT" << endl;
  } else {
    cout << " ---- SUCCESS OPENING PORT" << endl;
  }

  int ret, i;
  Timeval before;


  while(1) {

    before = TVNow();
    cout << "Trying Serial Write" << endl;
    serial_write( port, message, strlen(message));
    cout << "...took " << TVNow() - before << " seconds." << endl;

    clearbuffer(readBuffer);

    //    sleep(10);



    before = TVNow();
    cout << "Trying Serial Read" << endl;
    ret = serial_read( port, readBuffer, 10 );
    cout << "Serial Read ret=" << ret << " and readBuffer = " << (string(readBuffer)).substr(0, 20);
    cout << "...took " << TVNow() - before << " seconds." << endl;


    clearbuffer(readBuffer);

    //sleep(10);



    before = TVNow();
    cout << "Trying to write a whole lot of stuff " << endl;
    for( i=0; i<100; i++) {
      serial_write( port, message, strlen(message));
    }
    cout << "...took " << TVNow() - before << " seconds." << endl;
    

    clearbuffer(readBuffer);
    //    sleep(10);


    before = TVNow();
    cout << "Trying to read 100 characters using read blocking with timeout = 20 seconds" << endl;
    ret = serial_read_blocking( port, readBuffer, 100, Timeval(20, 0) );
    cout << "Serial Read ret=" << ret << " and readBuffer = " << (string(readBuffer)).substr(0, 20);
    cout << "...took " << TVNow() - before << " seconds." << endl;

    clearbuffer(readBuffer);

    //    sleep(10);


    before = TVNow();
    cout << "Trying to read until user hits enter key. Timeout = 20 seconds" << endl;
    ret = serial_read_until( port, readBuffer, '\n', 100, Timeval(20, 0) );
    cout << "Serial Read ret=" << ret << " and readBuffer = " << (string(readBuffer)).substr(0, 20);
    cout << "...took " << TVNow() - before << " seconds." << endl;

    clearbuffer(readBuffer);


    cout << "---------------------------------------------" << endl;
    cout << "------------ DONE WITH ROUND ----------------" << endl;
    cout << "---------------------------------------------" << endl;

    cout << endl << "Serial Status = " << endl;
    print_serial_status(port);


  }





}




