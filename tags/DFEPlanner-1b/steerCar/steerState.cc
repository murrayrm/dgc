#include <stdio.h>

#include <fstream>
#include <sstream>
#include <iostream>
#include <string>

#include "../Msg/dgc_const.h"           // TRUE, FALSE, ERROR
#include "steerCar.h"
#include "../serial/serial.h"

using namespace std;

int steer_state_blocking (int);


extern int negCountLimit;
extern int posCountLimit;
extern int STEER_BY_HAND;
extern int verbosity_exec_cmd;
extern int steer_errno;

///////////////////////////THIS IS A HACK/////////////////
extern string lastSteeringCommanded;
//////////////////////////////////////////////////////////

enum {
    TPE,        // Transfer Encoder position
    TAS,        // Transfer Axis State
    TASX,       // Transfer Extended Axis State
    TSS,        // Transfer System Status
    TER,
    TDTEMP,     // Transfer Drive Temp. (higher of DSP/power block)
    TMTEMP,     // Transfer Motor Temp. (predicted motor winding).
    TDHRS,      // Transfer Operating hours (nearest 1/4 hour).
    NUM_STATE_VARS
};
// The actual saved states/values.
string steerState[NUM_STATE_VARS] = {(string) "1234567890", (string) "1234567890", (string) "1234567890", (string) "1234567890", (string) "1234567890", (string) "1234567890", (string) "1234567890", (string) "1234567890"};
// Timestamps for each of the saved values.
timeval steerStateTimestamps[NUM_STATE_VARS] = {{0,0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}};
// Time in seconds for each datum to expire.  (.3 works, stresses the system A LOT)
double steerStateExpire[NUM_STATE_VARS] = {0.5, 2.6, 6.7, 8.9, 13, 21, 23, 27};
// Strings sent to the steering controller.
string serialStateQuery[NUM_STATE_VARS] = {(string) "TPE", (string) "TAS", (string) "TASX", (string) "TSS", (string) "TER", (string) "TDTEMP", (string) "TMTEMP", (string) "TDHRS"};

/******************************************************************************
 * steer_state_update
 *
 * Description:
 *      Updates the controller state every time called.  Global strings are
 *      written with the values of the commands read, without the echoing of
 *      the command given.  Alas, indexing is one off on the string versus the
 *      manual, because the manual starts with one instead of zero.  
 *      Even though it's annoying, the code writes everything to the controller
 *      and then reads it all back instead of doing things individually since
 *      communication with the controller is the bottleneck.  Also, there are
 *      those stupid underscores to watch out for, so they are removed for
 *      storage in the global variables.
 *
 * Arguments:
 *      log             integer that causes controller state to be logged when 
 *                      TRUE.
 *      updateAll       integer that spefifies that all parameters should be
 *                      recorded when TRUE.  Otherwise, TPE,TAS are updated.
 *
 * Global Variables:
 * [all of the below are written with the command of the same name]
 [...]
 * 
 * Return Values:
 *      TRUE    Data has been updated and/or logged.
 *      FALSE   Returned if function is called to fast (no new data).
 *      ERROR   either file I/O or serial.
 *****************************************************************************/
int steer_state_update (int log, int updateAll)
{
    unsigned int lastSTEER_EOT;  // The last STEER_EOT parsed (for updating variables).
    int readLength;     // Length Read by the serial code.
    int expectedReplies = NUM_STATE_VARS;
    char inputBuff[STEER_BUFF_LEN];  // Buffer serial code can read into.
    string serialInputString;
    timeval timeStamp;
    double elapsed;

//cout << __FILE__ << __LINE__ << endl; fflush(stdout);
    if ( !updateAll ) 
        expectedReplies = 2;    // Magic number follows.


    string fileName;    // Filname we log to.
    ofstream logFile;   // File we log to.
    if (log == TRUE) {
        // http://beta.experts-exchange.com/Programming/Programming_Languages/Cplusplus/Q_20650231.html#8751396
        char Time[20];
        time_t time_now = time(NULL);
        strftime( Time, 20, "%Y%m%d", localtime( &time_now ) );
        fileName = (string) "LOG.steering." + Time + ".dat";
        logFile.open( fileName.c_str(), ios::app );
        if (logFile.good() != true) {
            cout << "File " << fileName << " could not be opened to append." << endl;
            steer_errno = ERR_FILE_IO;
            return ERROR;
        }
    }

    // Clear any data in the buffer.
    serial_clean_buffer(STEER_SERIAL_PORT);


    //
    // Open a big loop to read and write the commands we care about.
    //

    for (int i = 0; i < expectedReplies; i++) {
        //
        // Check how long it's been since the last time we tried to update the
        // variables and decide whether or not to proceed
        //

        gettimeofday(&timeStamp, NULL);
        // Get the elapsed time. 
        elapsed = timeStamp.tv_sec - steerStateTimestamps[i].tv_sec;
        elapsed += ((double) (timeStamp.tv_usec - steerStateTimestamps[i].tv_usec)) / 1000000;
        // Fix Midnight rollover
        if ( elapsed < 0 ) {
            // Add number of seconds in a day
            elapsed += 24 * 3600;
        }

        // Don't get new data if the data hasn't expired.
        if ( elapsed < steerStateExpire[i] ) {         
            continue;
        }


        //
        // Send commands to the controller.
        // 

        if ( -1 == serial_write (
                    STEER_SERIAL_PORT, 
                    (char *) ( (string) "!" + serialStateQuery[i] + (char) STEER_EOT ).c_str(), 
                    serialStateQuery[i].length() + 2, 
                    SerialBlock
                    )
           ) {
            steer_errno = ERR_SERIAL_IO;
            return ERROR;
        }

        // 
        // Read the replies
        //
        readLength = 0;
        readLength = serial_read_until (
                STEER_SERIAL_PORT, 
                inputBuff + readLength, 
                (char) STEER_EOT, 
                SEC_TIMEOUT, 
                STEER_BUFF_LEN - 1
                );
        // Error Checking
        if ( readLength == 0 ) {
            cout << "Nothing read (trying " << serialStateQuery[i] << "), returning " << endl; fflush(stdout);
            logFile << "Nothing read (trying " << serialStateQuery[i] << "), returning " << endl; fflush(stdout);
            steer_errno = ERR_SERIAL_IO;
            return ERROR;
        }
 
        // Make it a string.
        inputBuff[readLength] = '\0';       

//cout << inputBuff;
        if ( updateAll ) {
            cout << inputBuff;
        }
        //
        // Deal with the data.
        //

        // STL is so much cooler.
        serialInputString = (string) inputBuff;
        // Get Rid of underscores
        while ( serialInputString.find('_') != string::npos ) {
            serialInputString.erase(serialInputString.find('_'), 1);
        }

        lastSTEER_EOT = serialInputString.find( (char) STEER_EOT, serialStateQuery[i].length() );
        if ( lastSTEER_EOT == string::npos ) {
            cout << "Parse error in serialInputString (" << serialInputString << ")\n";
            steer_errno = ERR_SERIAL_IO;
            return ERROR;
        }
        steerState[i] = serialInputString.substr(
                serialStateQuery[i].length(), 
                lastSTEER_EOT - serialStateQuery[i].length()
                );
        serialInputString = serialInputString.substr(lastSTEER_EOT + 1);

        // Set the time of update.
        gettimeofday( &(steerStateTimestamps[i]), NULL);

        if ( log ) {
            logFile << "[" 
                << ((string) asctime(localtime(&(steerStateTimestamps[i].tv_sec)))).erase(24)
                << " (+"
                << ((double) steerStateTimestamps[i].tv_usec) / 1000000
                << " sec)] " 
                << serialStateQuery[i] << "\t" << steerState[i] << endl;
            ////////////THIS IS A HACK/////////////
            if (i == TPE) {
                logFile << ((string) asctime(localtime(&(steerStateTimestamps[i].tv_sec)))).erase(24) << "\t" << steerState[i] << "\t" << lastSteeringCommanded;
            }
        }

    }

    if ( log ) {
        logFile.close();
    }

    return TRUE;
}



/******************************************************************************
 * steer_state_limit_pos
 *
 * Description:
 *      Checks if we have reached a positive limit, hardware or software.
 *
 * Global Variables:
 *      steerState      read, not modified
 *
 * Return Values:
 *      TRUE    We are at a limit
 *      FALSE   We are not at a limit
 *****************************************************************************/
int steer_state_limit_pos (int blocking)
{
    if ( blocking ) {
        if (steer_state_blocking (TAS) == ERROR) 
            return ERROR;
    }
    else {
        if (steer_state_update(FALSE, FALSE) == ERROR) 
            return ERROR;
    }

    if ( 
            steerState[TAS][16] == '1' // POS SW
            || steerState[TAS][14] == '1' // POS HW
       ) 
    {
        return TRUE;
    }
    else {
        return FALSE;
    }
}


/******************************************************************************
 * steer_state_limit_neg
 *
 * Description:
 *      Checks if we have reached a negative limit, hardware or software.
 *
 * Global Variables:
 *      steerState[TAS]   read, not modified
 *
 * Return Values:
 *      TRUE    We are at a limit
 *      FALSE   We are not at a limit
 *****************************************************************************/
int steer_state_limit_neg (int blocking)
{
    if ( blocking ) {
        if (steer_state_blocking (TAS) == ERROR) 
            return ERROR;
    }
    else {
        if (steer_state_update(FALSE, FALSE) == ERROR)
            return ERROR;
    }

    if ( 
            steerState[TAS][17] == '1'    // NEG SW
            || steerState[TAS][15] == '1' // NEG HW
            ) 
    {
        return TRUE;
    }
    else {
        return FALSE;
    }
}


/******************************************************************************
 * steer_state_home_complete
 *
 * Description:
 *      Checks if we have completed a home sequence
 *
 * Global Variables:
 *      steerState[TAS]   read, not modified
 *
 * Return Values:
 *      TRUE    We have completed the sequence
 *      FALSE   We have not completed the sequence
 *****************************************************************************/
int steer_state_home_complete (int blocking)
{
    if ( blocking ) {
        if (steer_state_blocking (TAS) == ERROR) 
            return ERROR;
    }
    else {
        if (steer_state_update(FALSE, FALSE) == ERROR) 
            return ERROR;
    }

    if ( steerState[TAS][4] == '1' ) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}


/******************************************************************************
 * steer_state_faulted
 *
 * Description:
 *      Checks if the drive is faulted.
 *
 * Global Variables:
 *      steerState[TAS]   read, not modified
 *
 * Return Values:
 *      TRUE    We are faulted
 *      FALSE   We are not faulted
 *****************************************************************************/
int steer_state_faulted (int blocking)
{
    if ( blocking ) {
        if (steer_state_blocking (TAS) == ERROR) 
            return ERROR;
    }
    else {
        if (steer_state_update(FALSE, FALSE) == ERROR) 
            return ERROR;
    }

    if ( steerState[TAS][13] == '1' ) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}


/******************************************************************************
 * steer_state_moving
 *
 * Description:
 *      Checks if the wheels are moving.
 *
 * Global Variables:
 *      steerState[TAS]   read, not modified
 *
 * Return Values:
 *      TRUE    We are moving
 *      FALSE   We are stopped
 *****************************************************************************/
int steer_state_moving (int blocking)
{
    if ( blocking ) {
        if (steer_state_blocking (TAS) == ERROR) 
            return ERROR;
    }
    else {
        if (steer_state_update(FALSE, FALSE) == ERROR)
            return ERROR;
    }

    if ( steerState[TAS][0] == '1' ) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}


/******************************************************************************
 * steer_state_TPE
 *
 * Description:
 *      Returns the position of the steering.
 *
 * Global Variables:
 *      steerState[TPE]   read, not modified
 *
 * Return Value:
 *      current encoder position.
 *****************************************************************************/
int steer_state_TPE (int blocking)
{
    if ( blocking ) {
        if (steer_state_blocking (TAS) == ERROR) 
            return ERROR;
    }
    else {
        if (steer_state_update(FALSE, FALSE) == ERROR) 
            return ERROR;
    }

    return atoi ( steerState[TPE].c_str() );
}


/******************************************************************************
 * steer_state_blocking
 *
 * Description:
 *      Does not return until the read time of a value is newer than the time
 *      given.
 *
 * Global Variables:
 *      steerState      read, not modified
 *
 * Return Value:
 *      ERROR           Problem with update.
 *      TRUE            Time has elapsed as planned.
 *****************************************************************************/
int steer_state_blocking (int stateIndex)
{
    timeval startTime = {steerStateTimestamps[stateIndex].tv_sec, steerStateTimestamps[stateIndex].tv_usec};

    while ( startTime.tv_sec == steerStateTimestamps[stateIndex].tv_sec
            || startTime.tv_usec == steerStateTimestamps[stateIndex].tv_usec )
    {
        // The updatetime has not changed, so try update again and see.
        if (ERROR == steer_state_update (FALSE, FALSE) ) 
            return ERROR;
    } 
    return TRUE;
}
