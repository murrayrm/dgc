#ifndef QIDCONSTANTS_H
#define QIDCONSTANTS_H

// All speeds given in meters per second

// MAX_SPEED is the highest speed the vehicle is ever allowed to go 
#define MAX_SPEED 5.0

// MAX_SPEED_OBSTACLE_DETECTED is the highest speed the vehicle is allowed
// to go if there is an obstacle detected (determined by obstacle threshold in
// genMap). This constant used by genMap when the appropriate velocity 
// shaping method is used.
#define MAX_SPEED_OBSTACLE_DETECTED 5.0

// MIN_SPEED is the minimum speed at which we can effectively command the
// vehicle.  Below this the vehicle would move at an indeterminant speed, or
// possibly halt.
#define MIN_SPEED 1.5

// DIST_TO_OBSTACLE_HALT is the minimum distance the vehicle is allowed to
// get to an obstacle.  If the vehicle would get any closer than this, then
// genMap will set the velocity vote to 0.  Specified in meters.
// NOTE: genMap will send a 0.01 velocity on ALL steering arcs if any one of
//       them has an obstacle closer than this.
//       It will vote 0.0 velocity on an arc if THAT arc has an obstacle
//       closer than this
#define DIST_TO_OBSTACLE_HALT 18  // Result of ReactionTest at 1 m/s 

// DIST_TO_OBSTACLE_MAX_SPEED defines a border around the vehicle.  If an
// obstacle gets within that border (such that any steering arc passes
// through it) then all votes will be limited to MAX_SPEED_OBSTACLE_DETECTED
#define DIST_TO_OBSTACLE_MAX_SPEED 20.0

// CORRIDOR_SAFE_SPEED is the speed we will go at if we are outside the
// corridor, going the wrong direction, or facing the edge of the corridor
#define CORRIDOR_SAFE_SPEED 2.0

// This is the maximum speed the DFE will allow us to go
// It's used in RaceModules/DFEPlanner/DFE.cc
// Arbiter reduces this by 1.0 for whatever reason
#define  DFE_MAX_SPEED  11.0 // m/s

#endif
