#ifndef dgc_mta_H
#define dgc_mta_H

#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "dgc_msg.h"
#include "dgc_messages.h"
#include <sys/types.h>


#define MTA_READ_BUFFER_SIZE 16

#define MTA_CTRL_PORT 13131
#define MTA_DATA_PORT (MTA_CTRL_PORT + 1)
#define MTA_LOCAL_IP 234
// #define str_localhost "127.0.0.1" 

#define HTTP_SIZEOFMSG 542393671

#define CLOCKS_TILL_ERASE_NODE 2000







/* status messages enumerated */

enum {
  dgcc_NullMsg = 0,
  dgcc_RegisterModule,
  dgcc_UnregisterModule,
  dgcc_AnyMessages,
  dgcc_PollMessage,
  dgcc_GetMessage,
  dgcc_RemoveMessage,
  dgcc_SendMessage,
  dgcc_GetLastMessage
};


struct dgc_range {
  int mtaMsgType;
  int start;
  int end;
  int len;
};

/* *********************************************************
************************************************************ */

struct mta_msg {
  char * memptr;
  //  int memsize;
};

   

/* *********************************************************
************************************************************ */

class mta_LocalNode
{
 public:
  mta_LocalNode(int id, int type) : id(id), type(type) {}
  mta_LocalNode(const mta_LocalNode & a) : id(a.id), type(a.type) {}

  int addMsg(mta_msg theMessage);
  int getLastTime();
  int getID();
  int getType();

  int anyMessages(int min=0, int max=0);

  int pollMessage(dgc_MsgHeader &fillme, int min=0, int max=0);

  int getMsg(const dgc_MsgHeader & msgh, mta_msg & fillme);


 private:
  boost::recursive_mutex mutex;
  
  int id;
  int type;
  long my_time;

  std::list<mta_msg> inbound;


};


/* *********************************************************
************************************************************ */

class mta_Mail
{
public:
  mta_Mail(): nodeCtr(2), msgCtr(1) {}

  int in_addNode(int nodeType);
  int in_rmNodeByID(int id);
  int in_rmNodeByType(int type);
  
  int purgeNodes();

  int refreshTime();

  int in_addMessageByID(int node, mta_msg mm, int flags=0);
  int in_addMessageByType(int node, mta_msg mm, int flags=0);

  int in_anyMessages(int node, int min=0, int max=0);  
  int in_pollMessage(dgc_MsgHeader &fillme, int node, int min=0, int max=0);
  int in_getMsg(const dgc_MsgHeader & msgh, mta_msg & fillme);
  int in_getLastMsg(int node, mta_msg & fillme, int min=0, int max=0);

  int in_nodeTypeExists(int nodeType);
  
  int out_pushMessage( mta_msg mm );
  int out_popMessage( mta_msg & fillme);
 
  int nextMsg();

private:

  int in_addNode(int nodeNum, int nodeType,  int flags);

  std::list<mta_LocalNode> inbox;
  std::list<mta_msg> outbox;

  boost::recursive_mutex inMutex;
  boost::recursive_mutex outMutex;

  int nodeCtr;
  int nextNode();

  int msgCtr;

};









#endif
