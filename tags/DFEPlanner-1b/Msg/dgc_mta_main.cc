
// dgc_mta_main.cc 

#include "dgc_mta.hh"
#include "dgc_msg.h"
#include "dgc_socket.h"
#include "dgc_messages.h"
#include "dgc_const.h"
#include "dgc_mta_main.h"
#include "dgc_time.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <signal.h>
#include <unistd.h>

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <iostream>
#include <iomanip>
#include <string>
#include <list>
#include <new>

using namespace std;



int shutdown_flag;
//unsigned int localhost;
//unsigned int myIP;

timeval sys0;
mta_Mail mailbox;

int ConnectionCounter;
int AllocatorCounter;
int ServerFD;

/* **************************************************************
*****************************************************************
***************************************************************** */

// class ServerPortWatcher
ServerPortWatcher::~ServerPortWatcher()
{
    dgc_CloseSocket(myPortFD);
}

void ServerPortWatcher::operator()()
{
  /* 1) Open the server socket, bind to it and listen for incoming connections.
     2) Accept the connections and ...
     3) Pass the FD for the socket connection to a function that handles that message transaction
  */
  // step 1
   
  cout << "Trying to open socket, port " << myPort << endl;
  
  int ServerFD = dgc_OpenSSocket(myPort);
  if (ServerFD < 0) {

    cout << "Port open failure" << endl ;
    return; // quit if there is an error
  }

  //  cout << "Port open success.  FD=" << myPortFD << endl;
  
  // step 2
  int newFD;
  sockaddr sa;
  socklen_t socklen = sizeof(sa);
  


  cout << "About to accept sockets." << endl;
  //  while (TRUE) {

  while(shutdown_flag == FALSE) {    
    newFD = dgc_AcceptSSocket(ServerFD, sa, socklen);
    if(newFD >= 0) {
      //      cout << "New socket accepted, FD = " << newFD << endl; 
      
      // step 3
      ServerPortHandler sph(newFD, sa);
      //      boost::thread thrd(sph);
      
      //      cout << "Connection Counter = " << ConnectionCounter << endl;
      
      sph();
      //      newFD = dgc_AcceptSSocket(myPortFD, sa, socklen);
    } else {
      shutdown_flag = TRUE;
    }
    
  }
    
    // }
}
// Class ServerPortWatcher
/* ************************************************************** */




/* **************************************************************
*****************************************************************
***************************************************************** */
// Class ServerPortHandler
ServerPortHandler::ServerPortHandler(int sd, sockaddr sa) :
  sd(sd), sa(sa), in_msgSize(0), out_msgSize(0), 
  in_msgPtr(NULL), out_msgPtr(NULL) {

  gettimeofday(&before, NULL);
}
ServerPortHandler::~ServerPortHandler() { 
  double tm;
  dgc_CloseSocket(sd); 
  if (in_msgPtr != NULL) {
    AllocatorCounter --;
    delete in_msgPtr;
  }
  if (out_msgPtr != NULL) {
    AllocatorCounter --;
    delete out_msgPtr;
  }
  gettimeofday(&after, NULL);
  tm = timeval_subtract(after, before);
  if( tm > 0.001 ) {
    cout << "ServerPortHandler: Total Transaction Time = " 
	 << setiosflags(ios::fixed) << setprecision(10) << tm << endl;
  }
  //  cout << "Allocator Counter = " << AllocatorCounter << endl;
  //  cout << endl;
}

void ServerPortHandler::operator()()
{
  sockaddr_in *sai = (sockaddr_in *) &sa;
  //  cout << "ServerPortHandler:  New socket from IP " << (*sai).sin_addr.s_addr
  //       << ", Port " << (*sai).sin_port << ".  FD=" << sd << endl;
  
  unsigned int isocket_sizeofmsg;
  unsigned int in_command;

  if( dgc_ReadSocket(sd, &isocket_sizeofmsg, sizeof(isocket_sizeofmsg), 10) == TRUE) {
    if( isocket_sizeofmsg == HTTP_SIZEOFMSG ) {
      cout << "Handling HTTP Request" << endl; 
      char mystr[] = "<html> <h1> Testing HTTP Response </h1> </html>";
      dgc_WriteSocket(sd, mystr, strlen(mystr)+1, 10);
    } else {

      if( isocket_sizeofmsg >= sizeof(in_command) &&
	  dgc_ReadSocket(sd, &in_command, sizeof(in_command), 10) == TRUE) { 

	isocket_sizeofmsg -= sizeof(in_command);
	//	cout << "ServerPortHandler: Allocating " << isocket_sizeofmsg << " bytes." << endl;
	
	in_msgPtr = new char[isocket_sizeofmsg];
	AllocatorCounter ++;
	in_msgSize = isocket_sizeofmsg;
	
	if(dgc_ReadSocket(sd, in_msgPtr, isocket_sizeofmsg, 10) == TRUE){
	  unsigned int nodeType;
	  unsigned int min, max, osocket_sizeofmsg;
	  int ret, nodeNum;
	  int wheretosend;
	  dgc_MsgHeader * p_msgh;
	  dgc_MsgHeader msgh;
	  mta_msg myMM;

	  // cout << "ServerPortHandler: InCommand = " << in_command << endl;
	  
	  switch (in_command) {
	    
	  case dgcc_RegisterModule:
	    nodeType = * ((unsigned int*)  in_msgPtr);
	    cout << "ServerPortHandler: RegisterModule: nodeType = " << nodeType << endl;
	    nodeNum = mailbox.in_addNode(nodeType);
	    if( nodeNum > 0 ) {
	      //cout << "ServerPortHandler: RegisterModule: Node # = " << nodeNum << endl;
	    } else {
	      cout << "ServerPortHandler: RegisterModule: Error Registering Module" << endl;
	    }
	    // return the node number
	    osocket_sizeofmsg = sizeof(nodeNum) + sizeof(sys0); // send "zero" time for system
	    dgc_WriteSocket(sd, &osocket_sizeofmsg, sizeof(osocket_sizeofmsg), 10);
	    dgc_WriteSocket(sd, &nodeNum, sizeof(nodeNum), 10);
	    dgc_WriteSocket(sd, &sys0,    sizeof(sys0), 10);
	    cout << "Sys0 = " << sys0.tv_sec << "sec, " << sys0.tv_usec << "usec" << endl;
	    
	    break;
	  case dgcc_UnregisterModule:
	    nodeNum = * ((int*)  in_msgPtr );
	    if ( mailbox.in_rmNodeByID(nodeNum) < 0 ) {
	      cout << "ServerPortHandler: UnregisterModule: nodeNum = " << nodeNum << " Failed" << endl;
	    } else {
	      //cout << "ServerPortHandler: UnregisterModule: nodeNum = " << nodeNum << " Successful" << endl;
	    } 
	    break;
	    
	  case dgcc_AnyMessages:

	    nodeNum = * ((int*)  (in_msgPtr));
	    min     = * ((int*)  (in_msgPtr +    sizeof(int)));
	    max     = * ((int*)  (in_msgPtr + 2* sizeof(int)));
	    ret = mailbox.in_anyMessages(nodeNum, min, max);
	    //	    cout << "ServerPortHandler: AnyMessages = " << ret << endl;
	    // write the result back to the socket
	    osocket_sizeofmsg = sizeof(ret);
	    dgc_WriteSocket(sd, &osocket_sizeofmsg, sizeof(osocket_sizeofmsg), 10);
	    dgc_WriteSocket(sd, &ret, osocket_sizeofmsg, 10);	  
	      
	    break;
	  case dgcc_PollMessage:

	    nodeNum = * ((int*)  (in_msgPtr));
	    min     = * ((int*)  (in_msgPtr +    sizeof(int)));
	    max     = * ((int*)  (in_msgPtr + 2* sizeof(int)));
	    ret = mailbox.in_pollMessage(msgh, nodeNum, min, max);
	    //	    cout << "ServerPortHandler: pollMessage: ret = " << ret << endl;
	    switch (ret) {
	    case ERROR:
	    case FALSE:
	      // write the result back to the socket
	      osocket_sizeofmsg = sizeof(ret);
	      dgc_WriteSocket(sd, &osocket_sizeofmsg, sizeof(osocket_sizeofmsg), 10);
	      dgc_WriteSocket(sd, &ret, osocket_sizeofmsg, 10);
	      break;
	    case TRUE:
	      // write the result back to the socket
	      osocket_sizeofmsg = sizeof(msgh);
	      dgc_WriteSocket(sd, &osocket_sizeofmsg, sizeof(osocket_sizeofmsg), 10);
	      dgc_WriteSocket(sd, &msgh, osocket_sizeofmsg, 10);	  
	    }
	    
	    break;
	  case dgcc_GetMessage:

	    ConnectionCounter++;

	    p_msgh = (dgc_MsgHeader *)  in_msgPtr;
	    ret = mailbox.in_getMsg( *p_msgh, myMM );
	    //	    cout << "ServerPortHandler: getMessage: ret = " << ret << endl;
	    switch (ret) {
	    case ERROR:
	    case FALSE:
	      // write the result back to the socket
	      osocket_sizeofmsg = sizeof(ret);
	      dgc_WriteSocket(sd, &osocket_sizeofmsg, sizeof(osocket_sizeofmsg), 10);	
	      dgc_WriteSocket(sd, &ret, osocket_sizeofmsg, 10);	  
	      break;
	    case TRUE: 
	      // write the result back to the socket
	      osocket_sizeofmsg = sizeof(ret) + (*p_msgh).MsgSize;
	      dgc_WriteSocket(sd, &osocket_sizeofmsg, sizeof(osocket_sizeofmsg), 10);
	      dgc_WriteSocket(sd, &ret, sizeof(ret), 10);	  
	      dgc_WriteSocket(sd, myMM.memptr + sizeof(dgc_MsgHeader), (*p_msgh).MsgSize, 10);
	      //cout << "ServerPortHandler: Get Message: reading from address " << (void*) myMM.memptr << endl;
	      //cout << "ServerPortHandler: Get Message first data = " 
	      //		   << * ((int*) (myMM.memptr + sizeof(dgc_MsgHeader))) << endl;
	      // set out_msgPter = myMM.memptr so the destructor will erase it
	      out_msgPtr = myMM.memptr;
	    }
    
	    break;
	  case dgcc_GetLastMessage:
	    // Gets last message of a type and discards the rest.
	    ConnectionCounter++;

	    p_msgh = (dgc_MsgHeader *)  in_msgPtr;
	    ret = mailbox.in_getLastMsg( p_msgh->SourceID, myMM, p_msgh->MsgType);
	    switch (ret) {
	    case ERROR:
	    case FALSE:
	      // write the result back to the socket
	      osocket_sizeofmsg = sizeof(ret);
	      dgc_WriteSocket(sd, &osocket_sizeofmsg, sizeof(osocket_sizeofmsg), 10);	
	      dgc_WriteSocket(sd, &ret, osocket_sizeofmsg, 10);	  
	      break;
	    case TRUE: 
	      // write the result back to the socket
	      osocket_sizeofmsg = sizeof(ret) + (*p_msgh).MsgSize;
	      dgc_WriteSocket(sd, &osocket_sizeofmsg, sizeof(osocket_sizeofmsg), 10);
	      dgc_WriteSocket(sd, &ret, sizeof(ret), 10);	  
	      dgc_WriteSocket(sd, myMM.memptr + sizeof(dgc_MsgHeader), (*p_msgh).MsgSize, 10);
	      //cout << "ServerPortHandler: Get Message: reading from address " << (void*) myMM.memptr << endl;
	      //cout << "ServerPortHandler: Get Message first data = " 
	      //		   << * ((int*) (myMM.memptr + sizeof(dgc_MsgHeader))) << endl;
	      // set out_msgPter = myMM.memptr so the destructor will erase it
	      out_msgPtr = myMM.memptr;
	    }
    
	    break;
	  case dgcc_RemoveMessage:
	    // same as get message except we dont return anything
	    p_msgh = (dgc_MsgHeader *)  in_msgPtr;
	    ret = mailbox.in_getMsg( *p_msgh, myMM );
	    //	    cout << "ServerPortHandler: getMessage: ret = " << ret << endl;
	    switch (ret) {
	    case ERROR:
	    case FALSE:
	      // do nothing
	      break;
	    case TRUE: 
	      // set out_msgPter = myMM.memptr so the destructor will erase it
	      out_msgPtr = myMM.memptr;
	    }
    
	    break;
	  case dgcc_SendMessage:
	    
	    // This means that the msg program is using the MTA to send a msg.
	    // Therefore, the mta is RECEIVING the message.
	    
	    //TODO: Add timestamps

	    //	    cout << " ******************************************* " << endl;
	    //	    cout << " ******************************************* " << endl;

	    //      cout << "ServerPortHandler: SendMessage" << endl;

	    p_msgh = (dgc_MsgHeader *)  in_msgPtr;
	    
	    (*p_msgh).Source = MTA_LOCAL_IP;
	    (*p_msgh).MsgID = mailbox.nextMsg();
	    
	    // cout << "MsgID=" << (*p_msgh).MsgID << ", Destination=" << (*p_msgh).Destination 
	    //	 << ", DestinationType=" << (*p_msgh).DestinationType << endl;

	    
	    if( (*p_msgh).Destination == 0 ) {
	      // fill In Destination Type
	      ret = mailbox.in_nodeTypeExists( (*p_msgh).DestinationType );
	      //	      cout << "ServerPortHandler: Destination Type Exists = " << ret << endl;
	      if( ret >= 0 ) {
		(*p_msgh).Destination = MTA_LOCAL_IP;
		(*p_msgh).DestinationID = ret;
	      } else {
		
		// TODO:
		// find the type on a different computer
	      }
	    }
	    
	    if( (*p_msgh).Destination == MTA_LOCAL_IP) {
	      // Deliver to local mailbox
	      mta_msg mm = { in_msgPtr };
	      if ( mailbox.in_addMessageByID( (*p_msgh).DestinationID, mm) == TRUE ) {
		//		cout << "ServerPortHandler:SendLocalMessage Successful" << endl;
		// Set Ptr to NULL so destructor won't delete it.
		//		cout << "ServerPortHandler:SendLocalMessage address " << (void*) in_msgPtr << endl;
		//		cout << "ServerPortHandler:SendLocalMessage first data = " 
		//		     << * ((int*) (in_msgPtr + sizeof(dgc_MsgHeader))) << endl;
		in_msgPtr = NULL;   
	      } else {
		cout << "ServerPortHandler:SendLocalMessage Unsuccessful" << endl;
		//TODO: Return Message to sender
	      }
	    } else {
	      
	      // TODO: Forward Message to another computer.
	      
	    }
	      
	    // End of case:dgcm_sendMessage
	  break;
	default:
	    cout << "ServerPortHandler: InCommand Failure" << endl;
	  } 
	}
      } 
    }
  }
  else {
    cout << "ServerPortHandler: Error Reading Socket" << endl;
    //   delete in_msgPtr;
    // in_msgPtr = NULL;
  }
}



void terminate(int t) {

  dgc_CloseSocket(ServerFD);
  shutdown_flag = TRUE;
}

int main(int argc, char* argv[])
{
  signal(SIGTERM, &terminate);

  gettimeofday(&sys0, NULL);
  ConnectionCounter = 0;
  AllocatorCounter = 0;
  shutdown_flag = FALSE;

  ServerPortWatcher x(MTA_LOCAL_PORT);
  x();
  return 0;
  // thread_alarm alarm(secs);
  // boost::thread thrd(alarm);
  // thrd.join();
}














