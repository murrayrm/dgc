#ifndef __LADTEST_H__
#define __LADTEST_H__
/****************************************************
 *
 *  Contants and Macros
 *
 ****************************************************/

#define DEFAULT_VERBOSITY          ( FALSE )
#define DEFAULT_SLOT_NUMBER        ( 0     )
#define DEFAULT_ITERATIONS         ( 1     )
#define DEFAULT_FREQUENCY          ( 40.0  )

#define IMAGE_FILENAME        ( "pe_dimes_v10" )
#define IMAGE_FILENAME_REVD   ( "pe_dimes_v10_revd" )

#define MEM_BASE                   ( 0x0 )
#define LEFT_MEM_OFFSET            ( 0x1200 )
#define RIGHT_MEM_OFFSET           ( 0x1400 )

#define DMA_WRITE_CHANNEL          ( 0 )
#define DMA_READ_CHANNEL           ( 0 )
#define DMA_TIMEOUT_MS             ( 2000 )

#define BRAM_SIZE_DWORDS           ( 256 )
#define BRAM_OFFSET                ( 0x2000 )
#define NUM_REGISTERS              (  32 )
#define REGISTER_OFFSET            ( 0x1000 )

#define MAX_ERR_COUNT              ( 32 )

typedef struct _TestInfo_
{
  WC_DeviceNum DeviceNum;
  WC_DevConfig DeviceCfg;
  WC_Version   Version;
  DWORD        dIterations;
  float        fClkFreq;
  BOOLEAN      bVerbose;
} WC_TestInfo;

/****************************************************
 *
 *  Prototypes
 *
 ****************************************************/

WC_RetCode WC_LADTest_Main( WC_TestInfo *TestInfo );

WC_RetCode WC_LADTest_Init( WC_TestInfo *TestInfo );

WC_RetCode WC_LADTest_Run( WC_TestInfo *TestInfo );

WC_RetCode WC_LADTest_Shutdown( WC_TestInfo *TestInfo );

WC_RetCode VerifyData(DWORD ref[], DWORD test[], DWORD size);

#endif 
