#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <stereovision/getpair.h>
#include <videre/svsclass.h>
#include <frames/frames.hh>
#include <genMap.hh>
#include <cv.h>
#include <cvaux.h>
#include <highgui.h>
#include <vehlib/VState.hh>

#include <stereovision/svsStereoWrapper.hh>

#define DEFAULT_SVMAP_NUM_ROWS 100
#define DEFAULT_SVMAP_NUM_COLS 100
#define DEFAULT_SVMAP_ROW_RES  0.2
#define DEFAULT_SVMAP_COL_RES  0.2
#define DEFAULT_SVSCAL_FILE_NAME    "../calibration/SVSCal.ini"
#define DEFAULT_SVSPARAMS_FILE_NAME "../calibration/SVSParams.ini"
#define DEFAULT_CAMCAL_FILE_NAME    "../calibration/CamCal.ini"


int main(int argc, char *argv[]) {
  int numPoints;
  genMap svMap;
  VState_GetStateMsg SS;
  XYZcoord UTMPoint;

  //svMap = genMap(SS, DEFAULT_SVMAP_NUM_ROWS, DEFAULT_SVMAP_NUM_COLS, DEFAULT_SVMAP_ROW_RES, DEFAULT_SVMAP_COL_RES, FALSE); 

  svsStereoWrapper svsObject;

  svsObject.init(DEFAULT_SVSCAL_FILE_NAME, DEFAULT_SVSPARAMS_FILE_NAME, DEFAULT_CAMCAL_FILE_NAME, FALSE, TRUE);
  //svsObject.grabCameraPair(SS);
  svsObject.grabFilePair("pretest0001-L.pgm", "pretest0001-R.jpg", "pretest.log", 1);

  svsObject.saveStereoPair("Left.pgm", "Right.pgm");
  svsObject.saveDispImage("disp.pgm");

  svsObject.processPair();

  numPoints = svsObject.numPoints();
  printf("Got %d points\n", numPoints);
  for(int i=0; i < numPoints; i++) {
    if(svsObject.validPoint(i)) {
      //printf("Setting point %d\n", i);
      UTMPoint = svsObject.UTMPoint(i);
      //printf("Returned %d (%lf, %lf, %lf)\n", i, UTMPoint.x, UTMPoint.y, UTMPoint.z);
      //svMap.setCellListDataUTM(UTMPoint.x, UTMPoint.y, UTMPoint.z);
    }
  }
  //svMap.evaluateCellsSV();
  //svMap.saveIMGFile("map.pgm");


  return 0;
}
