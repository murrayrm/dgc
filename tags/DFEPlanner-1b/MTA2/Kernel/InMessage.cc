#include "InMessage.hh"
#include "Misc/Mail/Mail.hh"
#include "Kernel.hh"

#include <iostream>
using namespace std;

extern Kernel K;
extern DGC_MODULE DGCM;

namespace KERNEL { 
  
  InMessageAcceptor::InMessageAcceptor() {
    //  TCP_ADDRESS tcp;// = K->MyTCPAddress();
    
    int port = DEF::MinPort;
    M_ActiveFlag = false;
    while( ! M_ss.Listen(port) ) {
      port ++;
      if (port > (DEF::MinPort+DEF::NumPorts)) {
	cout << "Error Creating Socket Server, Trying Again" << endl;
	port = DEF::MinPort;
      }
    }
    cout << "Socket Server started on " << port << endl;
  }
  
  InMessageAcceptor::~InMessageAcceptor() {}
  
  void InMessageAcceptor::operator ()() {
    int ActiveMsgs = 0;
    
    int msgType;
    InMessageHandler imh;
    M_ActiveFlag = true;
    
    cout << "InMessageAcceptor Starting" << endl;
    while(K.ShuttingDown() == false) {

      //      cout << "InMessageAcceptor - waiting to accept" << endl;
      File in = M_ss.Accept();
      //cout << "InMessageAcceptor, Socket Accepted !, fd = " << in.FD() << endl;

      imh(in);
      // in will close automatically
    }
    
    // shutting down now
  //  M_ss.Close();
    M_ActiveFlag = false;
  }
  
  int InMessageAcceptor::ServerPort() {
    return M_ss.Port();
  }

  
  InMessageHandler::InMessageHandler()  {}
  InMessageHandler::~InMessageHandler() {}
  
  void InMessageHandler::operator() (File& in) {
    
    if(in.FD() < 0) {
      cout << "InMessageHandler - Negative File Handler Detected" << endl;
      return;
    }

    // read in the key to verify the message
    int myKey = 0, flags;
    in.BRead((void*) &myKey, sizeof(myKey));
    

    if( myKey != DEF::ReceiveKey ) {
      cout << "InMessageHandler - Rogue Socket Connection" << endl;
      cout << "  myKey = " << myKey << ", but expecting " << DEF::ReceiveKey << endl;
      return;
    }

    //    cout << "InMessageHandler myKey = " << myKey << endl;

    // if that was successful, then the rest of 
    // the data is the message
    Mail newMessage(in);

    
    if(  newMessage.OK() ) {
      //      cout << "InMessageHandler Message Download Success, Size = "
      //	   << newMessage.Size() << endl;

      // now we need to determine what type of message
      // and who it goes to
      
      // first, find the recipient
      int mbox = newMessage.To().MAILBOX;
      flags = newMessage.Flags();

      //      cout << "Message For Mailbox = " << mbox << endl;

      if ( mbox == 0 ) { // for the kernel
	if(flags & MailFlags::QueryMessage) {
	  // it's a query message
	  //	  cout << "Query Message For the Kernel" << endl;
	  Mail returnMessage = K.QueryMailHandler(newMessage);
	  // so send it on its way.
	  K.SendMail(returnMessage, in); // using the same socket
	} else {
	  // just deliver
	  //	  cout << "In Message For the Kernel" << endl;
	  K.InMailHandler(newMessage);
	}
	
      } else { // find the module the message is for
	//	vector<shared_ptr <ModuleHandler> >::iterator x    = (K.KD_Modules).begin();
	//	vector<shared_ptr <ModuleHandler> >::iterator stop = (K.KD_Modules).end();
	//	while( x != stop) {
	//	  if( (*(*x)).ModuleNumber() == mbox ) {
	    // found the right module
	    // now check if inmessage or query message

	    if(flags & MailFlags::QueryMessage) {
	      // it's a query message
	      //	      Mail returnMessage = (*(*x)).QueryMail(newMessage);
	      Mail returnMessage = DGCM.QueryMailHandler(newMessage);
	      // so send it on its way.
	      K.SendMail(returnMessage, in); // using the same socket
	    } else {
	      // just deliver
	      //	      (*(*x)).InMail(newMessage);
	      DGCM.InMailHandler( newMessage ); //ml.From(), ml.To(), 0  msg t(newMessage);
	    }
	    //	  }
	    //	  x++;
      }
      
      
    } else cout << "InMessageHandler: Message Receive Failure" << endl;
  }
  
  
  
};
