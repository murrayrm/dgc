#include "Time.hh"

#include "Misc/Time/Time.hh"
#include "Kernel.hh"

extern Kernel K;

namespace KERNEL {

  Time::Time():M_TimeZero(0,0) {
  }

  Time::~Time() {
  }

  void Time::operator ()() {
    // declare "zero" time as beginning of program for now
    M_TimeZero = TVNow();

    while( ! K.ShuttingDown()) {
      // fill in time syncronization stuff here.

      sleep_for(1);
    }
  }

  Timeval Time::Now() {
    Timeval x = TVNow();
    return (x-M_TimeZero);
  }

};
