function process_data(run)
% Data processing function for DARPA Grand Challenge
%
% Lars Cremean
% 11 Nov 2003
%
%

% Had some problems loading some of the data files.  Errors included:
%
% Unable to read exepected data size from ASCII file
% L:\other\dgc\20031108_engine_characteristics\gps14.dat
%
% Number of columns on line 2 of ASCII file L:\other\dgc\20031108_engine_characteristics\accel10.dat
% must be the same as previous lines.


% load all of the data files...
% runs = [1:9 11:13 15:51 55];
% 
% for i=1:length(runs)
%     
%     eval(['load accel' num2str(runs(i)) '.dat'])
%     eval(['load gps' num2str(runs(i)) '.dat'])
%     eval(['load obd' num2str(runs(i)) '.dat'])
%     
% end

% load the corresponding data files
eval(['load accel' num2str(run) '.dat'])
eval(['load gps' num2str(run) '.dat'])
eval(['load obd' num2str(run) '.dat'])


figure(1), clf
% if you want a separate figure for each run
% eval(['figure(',num2str(run),'), clf'])

eval(['title(''Test ',num2str(run),', 8 Nov 2003'')'])
hold on

% plot latitude versus longitude
subplot(3,2,1) 
eval(['plot(gps',num2str(run),'(:,3),gps',num2str(run),'(:,2))'])
grid on, hold on
eval(['plot(gps',num2str(run),'(1,3),gps',num2str(run),'(1,2),''g.'')'])
axis equal
xlabel('longitude')
ylabel('latitude')

% plot northing versus easting
subplot(3,2,2)
eval(['plot(gps',num2str(run),'(:,4),gps',num2str(run),'(:,5))'])
grid on, hold on
eval(['plot(gps',num2str(run),'(1,4),gps',num2str(run),'(1,5),''g.'')'])
axis equal
xlabel('easting')
ylabel('northing')

% plot time trace of easting velocity
subplot(3,2,3)
eval(['plot(gps',num2str(run),'(:,1)/2,gps',num2str(run),'(:,7))'])
grid on, hold on
xlabel('time')
ylabel('easting velocity')

% plot time trace of northing velocity
subplot(3,2,4)
eval(['plot(gps',num2str(run),'(:,1)/2,gps',num2str(run),'(:,6))'])
grid on, hold on
xlabel('time')
ylabel('northing velocity')

% plot time trace of engine RPM
subplot(3,2,5)
eval(['plot(obd',num2str(run),'(:,1)/2,obd',num2str(run),'(:,2))'])
grid on, hold on
xlabel('time')
ylabel('engine RPM')

% plot time trace of vehicle MPH
subplot(3,2,6)
eval(['plot(obd',num2str(run),'(:,1)/2,obd',num2str(run),'(:,3))'])
grid on, hold on
xlabel('time')
ylabel('vehicle MPH')