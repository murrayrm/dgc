/* local.cc



*/

#include <iomanip>
#include <sys/ipc.h>
#include <sys/shm.h>

#include "WaypointNav.hh"

#include "../../LocalMap/LocalMap.h"
#include "../../LocalMap/svTrajectory.h"
#include "../../pertocla/per_to_cla.h"

#define LOCAL_KEY 111
#define LOCAL_SIZE 50000

extern int SIM; // get the simulation flag from the main function
extern int PRINT_VOTES; // get the print_votes flag from the main function

using namespace std;

//The length of a path in meters to be evaluated
static const double ANGLE_LENGTH = 20;

void *map_data;

int WaypointNav::InitStereo() {
  int shmid;
  int size;
  int key;
  int shmflg;
  
  // Sue Ann -- can you comment these lines?
  // in particular, LW was undefined and I don't know what type
  // it is and I'm not sure where to find this information
  d.voterArray[d.LocalNum] = Voter(NUMARCS, LEFTPHI, RIGHTPHI);
  // Initialize weight here since can't do it in the struct DATUM itself
  d.LocalWeight = LOCAL_W;
  d.demoArb.addVoter(&(d.voterArray[d.LocalNum]), d.LocalWeight);
  cout << "Local voter Added" << endl;
  // Other initialization tasks

  map_data = NULL;
  size = LOCAL_SIZE;
  key = LOCAL_KEY;

  shmflg = (IPC_CREAT | 0x1ff);
  if ((shmid = shmget(key, size, shmflg)) < 0) {
    cout << "\nLocal voter error during init - shared memory error";
  } else {
    map_data = (void*)shmat(shmid, NULL, 0);
  }

  return 0;
}

void WaypointNav::StereoLoop() {

  LocalMap lMap;
  serialMap sMap;
  svTrajectoryType path;
  double vehicle_heading=0;

  while( ! ShuttingDown() ) {

    // lock state if need to
    //    boost::recursive_mutex::scoped_lock slSTATE(d.stateLock);
    // Should take in velocity data and deal with it, but for now this is
    // a very stupid evaluator

    // Evalute arcs here
    int goodness = 0;
    double timestamp = 0;        // Dummy for now
    Voter::voteListType wayPt = d.voterArray[d.DFEnum].getVotes();

    if(!SIM) {
      //Step 1: Receive the latest map from Perceptor
      //sMap.deserializeMap(map_data);
      
      //Step 2: Integrate the latest Perceptor data with our pre-existing data
      lMap.updateMap(sMap);

      //Step 2B: Get the latest vehicle heading
      //vehicle_heading = state!?
     
    }
    // Now wayPt should contain all the rigth arcs to be evaluated.
    // So evaluate them.
    for (Voter::voteListType::iterator iter = wayPt.begin(); 
	 iter != wayPt.end(); ++iter) {
      if(!SIM) {
	//Step 3: Generate a trajectory for each angle 
	path = lMap.computePath(vehicle_heading+iter->phi, ANGLE_LENGTH);
	
	//Step 4: Evaluate over each trajectory for goodness and velocity,
	//and then populate our list of votes with that information
	iter->goodness = min(lMap.getPathGoodness(path), MAX_GOODNESS);
	iter->velo = MAX_VELO;
	//iter->velo = lMap.getPathVelocity(path);
      } else {
	iter->goodness = MAX_GOODNESS;
	iter->velo = MAX_VELO;
      }
    }
      

    // lock others while modifying d
    //    boost::recursive_mutex::scoped_lock slLOCAL(d.LocalLock);

    // Update this evaluator's Voter's voteList.
    d.voterArray[d.LocalNum].updateVotes(wayPt, timestamp);
    if( PRINT_VOTES ) {
      //cout << "--LocalLoop-- LOCAL votes" << endl;
      //d.voterArray[d.LocalNum].printVotes();   /////////////////////////
      //cout << "--local.cc-- Sleeping so you can read my output." << endl;
      //sleep(2);
    }
    usleep_for(30000);      // Evaluation rate, if needed

  } // end while(d.shutdown_flag == FALSE)

} // end LocalLoop(DATUM &d)
