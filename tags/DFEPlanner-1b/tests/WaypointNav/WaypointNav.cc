
//WaypointNav.cc

#include "WaypointNav.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

extern int SIM;
extern int DISPLAY;
extern int PRINT_VOTES;


WaypointNav::WaypointNav() 
  : DGC_MODULE(MODULES::WaypointNav, "WaypointNav", 0) {

}

WaypointNav::~WaypointNav() {


}

void WaypointNav::Init() {
  // call the parent init first
  DGC_MODULE::Init();


  cout << "Starting Module" << endl;
  // For Data Logging
  cout << "What test number? " << endl;
  cin >> d.TestNumber;
  // echo the test number entered
  cout << endl << d.TestNumber << endl;  

  if( d.TestNumber <= 0) {
    cout << "Invalid Test Number" << endl;
    ForceKernelShutdown();
    exit(EXIT_SUCCESS);
  }

  strstream strStream;
  strStream << d.TestNumber;
  strStream >> d.TestNumberStr;

  cout << "Initializing Arbiter... ";
  InitArbiter();    

  // Initialize Voter/evaluators
  int voterCounter = 0;
  d.globalNum = voterCounter; // = 0  
  voterCounter++;
  d.DFEnum =  voterCounter; // = 1
  voterCounter++;
  //  d.LadarNum =  voterCounter; // = 2
  //  voterCounter++;
  //d.LocalNum =  voterCounter; // = 3
  //voterCounter++;

  // Let's define num_voters here
  d.numVoters = voterCounter;
  d.voterArray = new Voter[d.numVoters];    // delete down there 

  cout << "Initialized Voters" << endl;


  // set desired velocity to ...
  // This is the default value, but will be overwritten in accel.cc
  d.TargetVelocity = 15; //  m/s

  // this must be done after the global voter is added in main
  cout << "Initializing Global..." << endl;
  InitGlobal();  // Needed for waypoint following

  // this must be done after the global voter is added in main
  cout << "Initializing DFE..." << endl;
  InitDFE();

  // this must be done after the voter array is created in main
  //  cout << "Initializing Ladar ..." << endl;
  // OK to call InitLadar whether running -sim or not; SIM gets caught 
  // in the function
  //  InitLadar();

  //  cout << "Initializing Stereo..." << endl;
  //InitStereo(d);                         // Needed for stereo vision

  cout << "Finished Initializing!!!! Hooray!" << endl;
  
  d.TimeZero = TVNow();

  cout << "Debugging temp cout" << endl;
  
  // Spawn threads
  
  // GrabVstateLoop
  RunMethodInNewThread<WaypointNav>(this, &WaypointNav::GrabVStateLoop);

  // Arbiter & Accel
  RunMethodInNewThread<WaypointNav>(this, &WaypointNav::ArbiterLoop);

  // Global
  RunMethodInNewThread<WaypointNav>(this, &WaypointNav::GlobalLoop);

  // DFE
  RunMethodInNewThread<WaypointNav>(this, &WaypointNav::DFELoop);

  // LADAR
//  RunMethodInNewThread<WaypointNav>(this, &WaypointNav::LadarLoop);

  //  Stereo
  //  RunMethodInNewThread<WaypointNav>(this, &WaypointNav::StereoLoop);

  cout << "Module Init Finished" << endl;
}

void WaypointNav::Active() {

  cout << "Starting Active Funcion" << endl;


  sleep(1);
  while( ContinueInState() ) {
    if( DISPLAY ) {
      SparrowDisplayLoop();
    } else {
      cout << "Time = " << (TVNow() - d.TimeZero) 
	   << ", Pos = " << d.SS.Northing << "-N, " << d.SS.Easting << "-E"
	   << ", Steer = " <<  d.Steer 
	   << ", Target Vel = " << d.TargetVelocity
	   << ", Actual Vel = " << d.SS.Speed
	   << ", Accel = " << d.Accel
	   << endl;
      sleep(1);
    }
  }

  cout << "Finished Active State" << endl;

}


void WaypointNav::Shutdown() {

  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;
  delete [] d.voterArray;        // Free the Voter array in d

}



