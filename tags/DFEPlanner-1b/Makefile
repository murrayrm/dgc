# DESCRIPTION
#
# This is the top-level Makefile for the Caltech DGC team project.
#
# CVS version information
#
# $Header: /usr/local/src/cvsroot/team/Makefile,v 1.23 2004/03/04 18:37:13 jeremy Exp $
#

# "make install" for SPARROW, MTA, VEHLIB
SPARROW       = sparrow/
MTA           = MTA-2.1/
FRAMES        = frames/
VEHLIB        = vehlib-2.0/
LADAR         = Ladar/
GLOBAL 	      = RaceModules/GlobalPlanner/
RDDF	      = RDDF/
GENMAP        = genMap/
SIMULATOR     = models/
ARBITER	      = RaceModules/Arbiter/
LATLONG       = latlong/
SV	      = stereovision/

# These appear to be old test programs:
# just "make" for everything else
GPS           = gps/
IMUNET        = state/imu/net
STATE         = state/
WAYPOINT      = tests/WaypointNav/

#=================================================================
#
# build targets
#

.PHONY: default

default: $(LATLONG) $(SPARROW) $(MTA) $(VEHLIB) $(LADAR) $(GLOBAL) \
	$(RDDF) $(GENMAP) $(SIMULATOR) $(GLOBAL) $(RDDF) $(ARBITER) 

$(LATLONG): FORCE
	cd $(@D); $(MAKE) install

$(SPARROW): FORCE
	cd $(@D); $(MAKE) install

$(MTA): FORCE
	cd $(@D); $(MAKE) install

$(FRAMES): FORCE
	cd $(@D); $(MAKE) install

$(VEHLIB): FORCE
	cd $(@D); $(MAKE) install

$(LADAR): FORCE
	cd $(@D); $(MAKE) install

$(GENMAP): FORCE 
	cd $(@D); $(MAKE) install

$(SIMULATOR): FORCE
	cd $(@D); $(MAKE) install

$(GLOBAL): FORCE
	cd $(@D); $(MAKE) install

$(RDDF): FORCE
	cd $(@D); $(MAKE) install

$(ARBITER): FORCE
	cd $(@D); $(MAKE) install

$(SV): FORCE
	cd $(@D); $(MAKE) install

#=================================================================
#
# build old targets
#

.PHONY: old

old: default $(GPS) $(IMUNET) $(STATE) $(WAYPOINT)

$(GPS): FORCE
	cd $(@D); $(MAKE)

$(IMUNET): FORCE
	cd $(@D); $(MAKE)

$(STATE): FORCE
	cd $(@D); $(MAKE)

$(WAYPOINT): FORCE
	cd $(@D); $(MAKE)

#=================================================================
#
# install targets
#

install: default 


#=================================================================
#
# clean targets
#

.PHONY: clean

clean:  $(SPARROW).clean $(MTA).clean $(VEHLIB).clean $(LADAR).clean \
	$(GENMAP).clean $(SIMULATOR).clean $(GLOBAL).clean $(RDDF).clean \
	$(ARBITER).clean 

$(SPARROW).clean:
	cd $(@D); $(MAKE) clean

$(MTA).clean:
	cd $(@D); $(MAKE) clean

$(VEHLIB).clean:
	cd $(@D); $(MAKE) clean

$(LADAR).clean:
	cd $(@D); $(MAKE) clean

$(GENMAP).clean:
	cd $(@D); $(MAKE) clean

$(SIMULATOR).clean:
	cd $(@D); $(MAKE) clean

$(GLOBAL).clean:
	cd $(@D); $(MAKE) clean

$(RDDF).clean:
	cd $(@D); $(MAKE) clean

$(ARBITER).clean:
	cd $(@D); $(MAKE) clean

$(FRAMES).clean:
	cd $(@D); $(MAKE) clean

$(SV).clean:
	cd $(@D); $(MAKE) clean

#=================================================================
#
# clean old targets
#

.PHONY: cleanold

cleanold:  clean $(GPS).clean $(IMUNET).clean $(STATE).clean $(LATLONG).clean $(WAYPOINT).clean

$(GPS).clean:
	cd $(@D); $(MAKE) clean

$(IMUNET).clean:
	cd $(@D); $(MAKE) clean

$(STATE).clean:
	cd $(@D); $(MAKE) clean

$(LATLONG).clean:
	cd $(@D); $(MAKE) clean

$(WAYPOINT).clean:
	cd $(@D); $(MAKE) clean


#=================================================================
#
# help
#

help:
	@echo ""
	@echo "This Makefile builds the FCS uRouter software distribution."
	@echo "The following make targets are supported:"
	@echo "  default - Top level build."
	@ECHO "  install - Same as default right now."
	@echo "  clean   - Removes compilation byproducts."
	@echo "  help    - This information."
	@echo ""


FORCE:



