#include<stdlib.h>
#include<unistd.h>
#include<ctype.h>
#include "Sim.hh"

/**
 * 12/6/2003   For demo today, we have a fixed set of arcs for every cycle.
 **/

using namespace std;

int LOG_DATA; // whether the data is logged or not

int main(int argc, char **argv) {
   
  // To get the command line arguments
  int opt;
  while ((opt = getopt (argc, argv, "l")) != -1){
    switch ((unsigned char)opt){
    case 'l':
      LOG_DATA = 1;
      break;
    case '?':
    default:
      cout << "Usage: -l (activate log file)" << endl;
      return(-1);
    }
  }
  
  // Start the MTA By registering a module
  // and then starting the kernel

  Register(shared_ptr<DGC_MODULE>(new SimVState));
  Register(shared_ptr<DGC_MODULE>(new SimVDrive));
  StartKernel();

  return 0;
}
