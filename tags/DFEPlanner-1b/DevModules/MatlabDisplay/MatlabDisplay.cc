/*
 * MatlabDisplay.cc
 */

#include "MatlabDisplay.hh"

extern int loggedDataRun;
extern char *logFileInfo;

char * timestamp = NULL;
char * testname = NULL;

// originally we had thought about reading in these files within C++
// instead, we will load them directly into the Matlab workspace
/*
FILE * logfile;         // logfile is for state and command logs
// These files are for vote logging
vector<FILE *> voteLogFileVector;
*/

int PAUSED       = 0;
int CLEAR_FIGURE = 0;
int QUIT_PRESSED = 0;
int DRAW_FIGURE  = 0;


MatlabDisplay::MatlabDisplay() : DGC_MODULE(MODULES::MatlabDisplay, 
					    "Matlab Display", 0)
{
}

MatlabDisplay::~MatlabDisplay()
{
}


/*****************************************************************************/
/*****************************************************************************/
void MatlabDisplay::Init() 
{
    // call the parent init first
    DGC_MODULE::Init();

    /*
    * Use engOutputBuffer to capture MATLAB output, so we can
    * echo it back.
    */
    engOutputBuffer(_ep, buffer, BUFSIZE);	

    // Insert code to initialize MatlabDisplay here
    // First things first... receive the genMap over the MTA to 
    // make sure we know how much to allocate for the array
    // This assigns our genMapStruct in the datum.
    // TODO: Things will go bad if this first one is not received;
    // add some error handling 
    /* This was experimental, let's not do it right before our 
     * second QID run  
     */
    ReceiveCAEgenMap();

    /////////////////OPEN LOGFILES (COPY FROM LADAR)/////////////
    // Actually, we won't open the logfiles here, but we will infer 
    // the names of the logfiles from the logFileInfo variable 
    if( loggedDataRun ) 
    {
      if( logFileInfo == NULL ) {
	fprintf(stderr, "ERROR! logFileInfo is NULL! \n");
	exit(-1);
      }
      // the filename specified should be the one that 
      // logs the state and the arbiter commands
      printf("logFileInfo: %s\n", logFileInfo);

      //p = strstr(file, "_scans_"); // Mike's original
      char * p = strstr(logFileInfo, "_arbiter_");
      timestamp = strdup(p+9);
      *p = 0;
      testname = strdup(logFileInfo);
      p = strstr(timestamp,".log");
      *p = 0;

      printf("testname: %s\n", testname);
      printf("timestamp: %s\n", timestamp);
    }
/*
  printf("Converting scan points from (%lf,%lf) to genMap\n", start, stop);

  sprintf(filename, "%s_scans_%s.log", testname, timestamp );
  printf("Reading scans from file: %s\n", filename);
  scanlog=fopen(filename,"r");
  if ( scanlog == NULL ) {
    printf("Unable to open scan log!!!\n");
    exit(-1);
  }
  fscanf(scanlog, "%% angle = %d\n", &angle);
  fscanf(scanlog, "%% resolution = %lf\n", &resolution);
  fscanf(scanlog, "%% units = %lf\n", &units);
  
  fgets(tmp, 1000, scanlog); // Read and discard header

  sprintf(filename, "%s_state_%s.log", testname, timestamp );
  printf("Reading state from file: %s\n", filename);
  statelog=fopen(filename,"r");
  if ( statelog == NULL ) {
    printf("Unable to open state log!!!\n");
    exit(-1);
  }
  fgets(tmp, 1000, statelog); // Read and discard header

  printf("LADAR scanning angle: %d degrees\n", angle);
  printf("LADAR scanning resolution: %lf degrees\n", resolution);
  printf("LADAR units: %lf m\n", units);
  printf("Number of scan points: %d\n", (int) ((double)angle/resolution) + 1);
*/

    
    
    // LG: code from DFEPlanner_Matlab/DFEPlanner.cc
    // Initialize the Matlab engine

    _counter = 0; // setup our fake data generator, remove once message passing 
                  // with real data is available

    // start the Matlab Engine !!!
    // NOTE: see the Matlab External Interfaces Guide for more details.
    //       It is available online at: 
    //          External Interfaces / API:
    //  http://www.mathworks.com/access/helpdesk/help/techdoc/matlab_external/matlab_external.shtml
    //          External Interfaces / API Reference
    //  http://www.mathworks.com/access/helpdesk/help/techdoc/apiref/apiref.shtml
    {
      fprintf(stderr,"Starting the matlab engine...\n");
      if (!(_ep = engOpen("matlab -nojvm -nosplash"))) 
      {
	fprintf(stderr, "\nCan't start MATLAB engine\n");
	return; // EXIT_FAILURE;
      }
      fprintf(stderr,"Matlab engine started.\n");
    }

    // ADD CODE HERE: Initialize the matlab variables
    _votes_M  = mxCreateDoubleMatrix(1, NUMARCS, mxREAL);
    _vel_M    = mxCreateDoubleMatrix(1, NUMARCS, mxREAL);
    // special array approximately matches the state struct.
    // Easting, Northing, Altitude, Vel_E, Vel_N, Vel_U (1-6)
    // Pitch, Roll, Yaw, PitchRate, RollRate, YawRate   (7-12)
    // Speed                                            (13)
    _currSS_M = mxCreateDoubleMatrix(1, 13, mxREAL);

#ifdef GENMAP_DISPLAY_DEV
    // set the variable that represents the genMap sent
    _mapNumRows_M       = mxCreateDoubleMatrix(1, 1, mxREAL);
    _mapNumCols_M       = mxCreateDoubleMatrix(1, 1, mxREAL);
    _mapRowRes_M        = mxCreateDoubleMatrix(1, 1, mxREAL);
    _mapColRes_M        = mxCreateDoubleMatrix(1, 1, mxREAL);
    _mapLeftBottomRow_M = mxCreateDoubleMatrix(1, 1, mxREAL);
    _mapLeftBottomCol_M = mxCreateDoubleMatrix(1, 1, mxREAL);
    _mapLB_Northing_M   = mxCreateDoubleMatrix(1, 1, mxREAL);
    _mapLB_Easting_M    = mxCreateDoubleMatrix(1, 1, mxREAL);
    // TODO: This is where things go bad if we didn't receive the genMap above
    _mapData_M          = mxCreateDoubleMatrix(1, 
		    _d.CAE_genMapStruct.numRows * _d.CAE_genMapStruct.numCols, 
		    mxREAL);
#endif

    // ADD CODE HERE: Initialize the C counterparts that point to the actual array of data
    _votes_C  = mxGetPr(_votes_M); // mxGetPr means "Get 'P'ointer to 'r'eal 
                                  // (as opposed to complex) data
    _vel_C    = mxGetPr(_vel_M);
    // C pointer to state array
    _currSS_C = mxGetPr(_currSS_M);

#ifdef GENMAP_DISPLAY_DEV
    _mapData_C          = mxGetPr(_mapData_M);    
    _mapNumRows_C       = mxGetPr(_mapNumRows_M);    
    _mapNumCols_C       = mxGetPr(_mapNumCols_M);    
    _mapRowRes_C        = mxGetPr(_mapRowRes_M);    
    _mapColRes_C        = mxGetPr(_mapColRes_M);    
    _mapLeftBottomRow_C = mxGetPr(_mapLeftBottomRow_M);    
    _mapLeftBottomCol_C = mxGetPr(_mapLeftBottomCol_M);    
    _mapLB_Northing_C   = mxGetPr(_mapLB_Northing_M);    
    _mapLB_Easting_C    = mxGetPr(_mapLB_Easting_M);    
#endif

    // Initialize the Matlab variables for displaying module votes
    _voters_goodness_M = mxCreateDoubleMatrix(ArbiterInput::Count, NUMARCS, mxREAL);
    _voters_velocity_M = mxCreateDoubleMatrix(ArbiterInput::Count, NUMARCS, mxREAL);

    _combined_goodness_M = mxCreateDoubleMatrix(1, NUMARCS, mxREAL);
    _combined_velocity_M = mxCreateDoubleMatrix(1, NUMARCS, mxREAL);

    _arbiter_best_arc_M = mxCreateDoubleMatrix(1, 1, mxREAL);

    // Initialize C pointer to array in Matlab variables
    _voters_goodness_C = mxGetPr(_voters_goodness_M);
    _voters_velocity_C = mxGetPr(_voters_velocity_M);

    _combined_goodness_C = mxGetPr(_combined_goodness_M);
    _combined_velocity_C = mxGetPr(_combined_velocity_M);

    _arbiter_best_arc_C = mxGetPr(_arbiter_best_arc_M);

    
    // ADD CODE HERE: Set up plot figures with graphics handles
    {
        // open figure and setup for quick plotting
        /*      Ordinarily, you would just plot data with a 
                    plot(x,y) command.
                However, if you keep track of the plotted element's handle
                    h_line = plot(x,y);
                Then you can replot new data much more quickly by doing
                    set(h_line, 'XData', x_new, 'YData', y_new);
                    drawnow;
        */
        /* This was the initial sample figure from Luis
            engEvalString(_ep, " figure(100);                        ");
            engEvalString(_ep, " subplot(2,1,1)                      ");
            // h_votes is a handle to the line plot of votes
            // later we can efficiently replot it by replacing the
            // data that h_votes uses. 
            engEvalString(_ep, " h_votes = plot(zeros(1,25),'.-');   ");
            engEvalString(_ep, " xlabel('votes')                     ");

            engEvalString(_ep, " subplot(2,1,2)                      ");
            // likewise, h_vel is a handle to the line plot of velocities
            engEvalString(_ep, " h_vel = plot(zeros(1,25),'.-');     ");
            engEvalString(_ep, " xlabel('vel')                       ");

            engEvalString(_ep, " drawnow;                            "); // force rendering of plots
        */

        // initialize the figure in order to draw the vehicle path
        engEvalString(_ep, "fh = figure('Position',[0 400 1000 384]); hold on   ");
	engEvalString(_ep, "set(gcf,'DoubleBuffer','on')                        ");

	engEvalString(_ep, "figure(fh), subplot(1,2,2)                          ");
	engEvalString(_ep, "wp = plot_corridor('bob.dat')                       ");
        engEvalString(_ep, "draw_vehicle('veh',wp(1,1),wp(1,2),0,0,'k',1)       ");

        // Initialize figure that plots all the Votes the Arbiter sees
	// The indices of the subplots are in initialize_voters_figure
        engEvalString(_ep, "initialize_voters_figure;                        ");

    	// Get the genMap from CorridorArcEvaluator
	// This assigns our _d.map and the variables associated with it.
	UpdateCAEgenMap();

	// initialize the figure to draw the genMaps
        //engEvalString(_ep, "fh2 = figure('Position',[200 400 512 384]); hold on ");
	//engEvalString(_ep, "set(gcf,'DoubleBuffer','on')                        ");
	
	// the results from this are too funky
	//engEvalString(_ep, "plot_gmstruct_2d(mapData,[mapRowRes mapColRes],[mapLBRow mapLBCol],[mapLBNorthing mapLBEasting])           ");
	//engEvalString(_ep, "hgm = plot_genmap_2d('CAEgenMap',[wp(1,2) wp(1,1)]);   ");
    }

    fprintf(stderr, "Sleeping for 1 seconds... this is what the plots look like after initialization\n");
    usleep(1000000);

    // start Sparrow Display
    RunMethodInNewThread<MatlabDisplay>( this, &MatlabDisplay::SparrowDisplayLoop);
    RunMethodInNewThread<MatlabDisplay>( this, &MatlabDisplay::UpdateSparrowVariablesLoop);

    cout << ModuleName() << "Init Finished" << endl;
}

/*****************************************************************************/
/*****************************************************************************/
void MatlabDisplay::Active()
{
    cout << "MatlabDisplay: Starting Active Funcion" << endl;

    int counter = 0;
    
    while( ContinueInState() ) 
    {
      counter++;
      
        // ADD CODE HERE: Get messages from other modules
        // update the state so we know where the vehicle is at
        // This assigns our _d.SS
        if (!loggedDataRun) {
	  UpdateState();
	}
	else {
	  // Read in the file and update _d.SS
	  /////////////////READ IN FROM THE FILE///////////////////////
	  /*
	  _d.SS.Easting = //////////////?????????;
	  _d.SS.Northing = //////////////?????????;
	  _d.SS.Altitude = //////////////?????????;
	  _d.SS.Vel_E  = //////////////?????????;
	  _d.SS.Vel_N  = //////////////?????????;
	  _d.SS.Vel_U  = //////////////?????????;
	  _d.SS.Pitch  = //////////////?????????;
	  _d.SS.Roll  = //////////////?????????;
	  _d.SS.Yaw  = //////////////?????????;
	  _d.SS.PitchRate  = //////////////?????????;
	  _d.SS.RollRate  = //////////////?????????;
	  _d.SS.YawRate  = //////////////?????????;
	  _d.SS.Speed = //////////////?????????;
	  */
	}
        // This assigns all of the votes that we receive from the arbiter
	if (!loggedDataRun) {
	  UpdateArbiterVoters();
	}
	else {
	  // Read in the file and update _d.Lars knows
	  /////////////////READ IN FROM THE FILE///////////////////////
	    /*
	  for (int i=0; i<ArbiterInput::Count; i++) {
	    for (int j=0; j<NUMARCS; j++) {
	      _d.arbiterVoters[i].Votes[j].Goodness = /////////????????????;
	      _d.arbiterVoters[i].Votes[j].Velo = ////////????????????;
	    }
	  }

	  for (int j=0; j<NUMARCS; j++)
	  {
	    _d.combinedVote.Votes[j].Goodness = //////////////??????????????;
	    _d.combinedVote.Votes[j].Velo = /////////////??????????????;
	  }
	  _voters_goodness_M = ///////?????;
	  _voters_velocity_M = ///////?????;
	  _combined_goodness_M = ///////?????;
	  _combined_velocity_M = ///////?????;
	    //	  _arbiter_best_arc_M = ///////?????;
	    */
	  }

#ifdef GENMAP_DISPLAY_DEV
	if( DRAW_FIGURE ) {
	  // Update the genMap we receive from CorridorArcEvaluator
	  // This assigns the proper variables and puts the appropriate
	  // Matlab variables in the workspace
	  UpdateCAEgenMap();

	  // PlotGenMap will take the new data we have from the above call to 
	  // UpdateCAEgenMap and will plot it to figure fh2
	  PlotGenMap();
	  //engEvalString(_ep, "plot_gmstruct_2d(mapArray,[mapRowRes mapColRes],[mapLBRow mapLBCol],[mapLBNorthing mapLBEasting]);  ");
	  // DEBUG:
	  // Echo the output from the command.  First two characters
	  // are always the double prompt (>>).
	  printf("%d %s", __LINE__, buffer+2);
	}
#endif

        for (int i=0; i<ArbiterInput::Count; i++) {
            for (int j=0; j<NUMARCS; j++) {
                _voters_goodness_C[i + ArbiterInput::Count*j] = 
		  _d.arbiterVoters[i].Votes[j].Goodness;
                _voters_velocity_C[i + ArbiterInput::Count*j] = 
		  _d.arbiterVoters[i].Votes[j].Velo;
            }
        }

        for (int j=0; j<NUMARCS; j++)
        {
            _combined_goodness_C[j] = _d.combinedVote.Votes[j].Goodness;
            _combined_velocity_C[j] = _d.combinedVote.Votes[j].Velo;
        }

        // initialize the arbiter best_arc variable too
        // first set it to the commanded angle in radians
	_arbiter_best_arc_C[0] = _d.arbiter_cmd.steer_cmd * USE_MAX_STEER;
        //printf(" _arbiter_best_arc rad = %f\n", _arbiter_best_arc_C[0] );
	// then reset it to the arc index
        _arbiter_best_arc_C[0] = (double)GetArcNum(_arbiter_best_arc_C[0]);
        //printf(" _arbiter_best_arc index = %d\n", round(_arbiter_best_arc_C[0]) ); 

	// put the votes in the Matlab workspace 
	engPutVariable(_ep, "voter_goodness", _voters_goodness_M );
	engPutVariable(_ep, "voter_velocity", _voters_velocity_M );

	engPutVariable(_ep, "combined_goodness", _combined_goodness_M );
	engPutVariable(_ep, "combined_velocity", _combined_velocity_M );
           
        // arbiter_best_arc is the 0-indexed index of the arbiter's chosen arc
	engPutVariable(_ep, "arbiter_best_arc", _arbiter_best_arc_M );

	// update the matlab voters figure
	engEvalString(_ep, "update_voters_figure;                        ");


        // For now we just make up some data
            /*
            for(int i=0; i<NUMARCS; i++) 
            {
                _d.matlabdisplay.Votes[i].Goodness = 15* sin( (_counter + i)*3.14/180.0 );
                _d.matlabdisplay.Votes[i].Velo = 15* cos( (_counter + i)*3.14/180.0 );

                _counter += 1;
            }
            */

        // ADD CODE HERE: Copy variables for matlab display
        // NOTE that since _votes_C points inside _votes_M, we've copied the
        // data to the matlab variable.
            /*
            for(int i=0; i<NUMARCS; i++) 
            {
                _votes_C[i] = _d.matlabdisplay.Votes[i].Goodness;
                _vel_C[i] = _d.matlabdisplay.Votes[i].Velo;
            }
            */

	// assign the pointer array elements
	// special array approximately matches the state struct.
	// Easting, Northing, Altitude, Vel_E, Vel_N, Vel_U (1-6)
	// Pitch, Roll, Yaw, PitchRate, RollRate, YawRate   (7-12)
	// Speed                                            (13)
        _currSS_C[0]  = _d.SS.Easting;
        _currSS_C[1]  = _d.SS.Northing;
        _currSS_C[2]  = _d.SS.Altitude;
        _currSS_C[3]  = _d.SS.Vel_E;
        _currSS_C[4]  = _d.SS.Vel_N;
        _currSS_C[5]  = _d.SS.Vel_U;
        _currSS_C[6]  = _d.SS.Pitch;
        _currSS_C[7]  = _d.SS.Roll;
        _currSS_C[8]  = _d.SS.Yaw;
        _currSS_C[9]  = _d.SS.PitchRate;
        _currSS_C[10] = _d.SS.RollRate;
        _currSS_C[11] = _d.SS.YawRate;
        _currSS_C[12] = _d.SS.Speed;

    // ADD CODE HERE: Place the matlab variables into the MATLAB workspace.
    // example code
    /*
        engPutVariable(_ep, "votes", _votes_M);
            engPutVariable(_ep, "vel", _vel_M);
    */

	// put the state array in the workspace 
        engPutVariable(_ep, "currSS", _currSS_M );

	// ADD CODE HERE: Call matlab plot commands
        // for the fastest replotting, we just reset the 
        // Y-axis data that each line plot handle uses
        // (same number of elements, in vector, same XData)

        /*
        // sample code
        engEvalString(_ep, " figure(100);                        ");
        engEvalString(_ep, " set(h_votes,'YData',votes);         ");
        engEvalString(_ep, " set(h_vel  ,'YData',vel  );         ");
        engEvalString(_ep, " drawnow;                            "); // force rendering of plots
        */     

        // wait until we get a nonzero value
        if( _d.SS.Easting > 0 ) 
        {	
            //printf("Got here and _d.SS.Easting = %10.3f\n", _d.SS.Easting );

            engEvalString(_ep, "figure(fh); hold on; subplot(2,2,[2 4])          ");
            // plot the position relative to the first waypoint
	    // DEBUG:
            engEvalString(_ep, "currSS(1),wp(1,1),currSS(2),wp(1,2)  "); 
  /*
       * Echo the output from the command.  First two characters
         * are always the double prompt (>>).
	   */
	      printf("%d %s", __LINE__, buffer+2);


            engEvalString(_ep, "plot(currSS(1)-wp(1,1), currSS(2)-wp(1,2), '.')  "); 
            // draw the vehicle with zero steering angle until we get it from the arbiter
            engEvalString(_ep,
			    "draw_vehicle('veh',currSS(1)-wp(1,1),currSS(2)-wp(1,2),currSS(9),0,'k',1,arbiter_best_arc) ");
            // make the axes follow the vehicle
	    engEvalString(_ep, "axis([currSS(1)-wp(1,1)-50 currSS(1)+50-wp(1,1) currSS(2)-wp(1,2)-50 currSS(2)-wp(1,2)+50]) ");

	    // This is experimental and can be ignored
	    /*
	    if( counter % 100 == 0 ) {
	      printf("  starting plot_genmap_2d\n");
	      engEvalString(_ep, "hgm = plot_genmap_2d('CAEgenMap',[wp(1,2) wp(1,1)]);   ");
	      printf("  done with plot_genmap_2d\n");
	    }
            */

	    //engEvalString(_ep, "axis tight; axis equal               "); 
            engEvalString(_ep, "drawnow;                             "); // force rendering of plots
        }

        // and sleep for a bit
        usleep(10000);
	}

    cout << "Finished Active State" << endl;
 }

/*****************************************************************************/
/*****************************************************************************/
void MatlabDisplay::Shutdown()
{

    // if we get to here a break has been detected 
    cout << "Shutting Down" << endl;

    //Insert code to shut down MatlabDisplay here

    // ADD CODE HERE: release matlab variables
    // example variables
    mxDestroyArray(_votes_M);
    mxDestroyArray(_vel_M);
    // special state array
    mxDestroyArray(_currSS_M);

    // close the Matlab Engine
    engClose(_ep);

}

/*****************************************************************************/
/*****************************************************************************/
void MatlabDisplay::PlotGenMap()
{
  engEvalString(_ep, "figure(fh2), colorbar                 ");
  //engEvalString(_ep, "plot_gmstruct_2d(mapArray, [mapRowRes mapColRes], [mapLBNorth-wp(1,2) mapLBEast-wp(1,1)]), colormap cool;  ");
  engEvalString(_ep, "imagesc(mapArray), axis tight, axis equal");
	  
  /*
  * Echo the output from the command.  First two characters
  * are always the double prompt (>>).
  */
  printf("%d %s", __LINE__, buffer+2);

  // This is now done in UpdateCAEgenMap
  //engEvalString(_ep, "imagesc( circshift(flipud(mapArray), [-mapLBRow -mapLBCol]) )");
  //engEvalString(_ep, "axis([1 mapNumCols 1 mapNumRows]), axis equal              ");
  //engEvalString(_ep, "drawnow                                                 ");
  //engEvalString(_ep, "figure(3), plot([1:100],sin([1:100])),  drawnow               ");

  // DEBUG
  //printf(" got an image!\n");
  //engEvalString(_ep, "mapArray                                          ");
  //engEvalString(_ep, "save CtoMat.mat                                    ");

  /*
  * Echo the output from the command.  First two characters
  * are always the double prompt (>>).
  */
  //printf("%d %s", __LINE__, buffer+2);
}
