#ifndef STATEVIEW_HH
#define STATEVIEW_HH

#include "MTA/Misc/Thread/ThreadsForClasses.hpp"
#include "MTA/Kernel.hh"

#include <unistd.h>
#include <iostream>

#include "vehlib/VState.hh"

extern int SIM;
extern int DISPLAY;
extern int PRINT_VOTES;




class StateView : public DGC_MODULE {

public:
  StateView(): DGC_MODULE(MODULES::Passive, "Passive State View", 0) {}

  void Active();
  void UpdateState();
  VState_GetStateMsg SS;

};

#endif
