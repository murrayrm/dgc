
#include <sys/time.h>
#include <iomanip>
#include <algorithm>  // for min

#include "StereoImageLogger.hh"
#include "stereo.hh"

#define DEFAULT_SVSCAL_FILE_NAME    "../../calibration/SVSCal.ini"
#define DEFAULT_SVSPARAMS_FILE_NAME "../../calibration/SVSParams.ini"
#define DEFAULT_CAMCAL_FILE_NAME    "../../calibration/CamCal.ini"

using namespace std;

extern int SIM; // get the simulation flag from the main function
extern int PRINT_VOTES; // get the print_votes flag from the main function
extern char left_file_name_prefix[200], right_file_name_prefix[200], left_file_name_suffix[40], right_file_name_suffix[40], file_type[10], state_file_name[256];
extern int max_frames, current_frame, test_num, verbose, prompt, delay, forever, which_pair;

//Global variables used between InitStereo, StereoUpdateVotes, and StereoShutdown
FILE* statelog=NULL;
int i, j, current_frame=0;

int StereoImageLogger::InitStereo() {
  //Initialize the SVS object
  d.svsObject.init(which_pair, DEFAULT_SVSCAL_FILE_NAME, DEFAULT_SVSPARAMS_FILE_NAME, DEFAULT_CAMCAL_FILE_NAME, TRUE, verbose);

  //Setup the state log file
  sprintf(state_file_name, "%s.log", left_file_name_prefix);
  
  statelog=fopen(state_file_name, "w");
  if( statelog == NULL)
    {
      fprintf( stderr, "Can't create '%s'", state_file_name);
    } else {
      // print data format information in a header at the top of the state file
      fprintf(statelog, "% Time[sec] | Easting[m] | Northing[m] | Altitude[m]");
      fprintf(statelog, " | Vel_E[m/s] | Vel_N[m/s] | Vel_U[m/s]" );
      fprintf(statelog, " | Speed[m/s] | Accel[m/s/s]" );
      fprintf(statelog, " | Pitch[rad] | Roll[rad] | Yaw[rad]" );
      fprintf(statelog, " | PitchRate[rad/s] | RollRate[rad/s] | YawRate[rad/s]");
      fprintf(statelog, "\n");
      fclose(statelog);
    }
   
  return true;
}

void StereoImageLogger::StereoUpdateVotes() {
  char left_file_name[256];
  char right_file_name[256];
  
  if( current_frame < max_frames || forever==1) {
    usleep(delay);
    if(prompt == 1) {
      printf("Hit enter for next capture");
      getchar();
    }

    //Grab the images
    if(verbose) printf("Grabbing image...\n");
    d.svsObject.grabCameraPair(d.SS);
    
    //Setup the file names
    current_frame++;    
    sprintf(left_file_name, "%s%.6d%s.%s", left_file_name_prefix, current_frame, left_file_name_suffix, file_type);
    sprintf(right_file_name, "%s%.6d%s.%s", right_file_name_prefix, current_frame, right_file_name_suffix, file_type);

    //Save the images
    if(verbose) printf("Saving image %d\n", current_frame);
    d.svsObject.saveStereoPair(left_file_name, right_file_name);

    //Save the state log
    statelog=fopen(state_file_name, "a");
    if(statelog != NULL) {
      // Print state      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 
      fprintf(statelog, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",
	      (double)(d.SS.Timestamp.sec()+d.SS.Timestamp.usec()/1000000.0),
	      d.SS.Easting, d.SS.Northing, d.SS.Altitude, 
	      d.SS.Vel_E, d.SS.Vel_N, d.SS.Vel_U, 
	      d.SS.Speed, d.SS.Accel, 
	      d.SS.Pitch, d.SS.Roll, d.SS.Yaw, 
	      d.SS.PitchRate, d.SS.RollRate, d.SS.YawRate );
      fclose(statelog);
      if(verbose == 1) printf("Wrote state\n");
    } else {
      if(verbose == 1) printf("Couldn't open %s to write state\n", state_file_name);
    }
    if(current_frame == max_frames && forever!=1) printf("\nDone\n");
  }

  usleep(1000000/10); //for approximately 10 fps

} // end StereoUpdateVotes


void StereoImageLogger::StereoShutdown() {

}
