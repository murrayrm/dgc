/* C++ Wrapper around gcnet.h */

#include "gcnet.h"

class GcnetConnection
{
public:
    // Object Lifetime methods.
    GcnetConnection();
    virtual ~GcnetConnection();

    // Initialize this connection as a client, with the given destination.
    bool connect(const char *hostname, unsigned port);

    // Initialize this connection as a server, listening on the given port.
    bool listen(unsigned port);

    // Transmit a packet to the remote side of this connection.
    // If this is a server then this will send a packet to each connected client.
    void transmit(const void *data, unsigned length, bool reliable = false);

protected:
    // Override this method to handle incoming data.
    virtual void handle_incoming(const void *data, unsigned length);

private:
    Gcnet *gcnet;
    static void receive_func(const void *data, unsigned length, void *user_data);
    bool finish_init();
};



