/* NOTES:

   - functions whose names end in __locked may only be called
     when the global lock is held.

   - user callbacks must not be called with the lock!  that way,
     they can feature reentrance.
 */
#include "gcnet.h"
#include "crc32.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>		/* for sockaddr_in */
#include <netdb.h>		/* for gethostbyname() */
#include <unistd.h>		/* for read(), write() */
#include <assert.h>
#include <pthread.h>
#include <sys/time.h>		/* unix: for gettimeofday() */

/* =====================================================================
 *               Miscellaneous Helper Macros & Types
 * ===================================================================== */
#define FALSE 0
#define TRUE  1
typedef int boolean;
#define MIN(a,b)  (((a) < (b)) ? (a) : (b))
#define MAX(a,b)  (((a) > (b)) ? (a) : (b))

#define MAX_PACKET_SIZE		(1<<16)		/* 64k */

#define CONNECT_RETRY_MS	5000		/* retry every 5secs */

/* =====================================================================
 *                        Error handling.
 * ===================================================================== */

static void
warn (const char *format, ...)
{
  va_list args;
  va_start (args, format);
  fprintf (stderr, "WARNING: *** ");
  vfprintf (stderr, format, args);
  fprintf (stderr, " ***\n");
  va_end (args);
}

static void
die (const char *format, ...)
{
  va_list args;
  va_start (args, format);
  fprintf (stderr, "FATAL ERROR: *** ");
  vfprintf (stderr, format, args);
  fprintf (stderr, " ***\n");
  va_end (args);
  exit (1);
}

/* =====================================================================
 *                        Socket Abstraction.
 *
 * NOTE: All Platform-Specific Code should go in THIS section!
 * ===================================================================== */

#ifdef WIN32
typedef SOCKET GcnetSocket;
#define GCNET_SOCKET_INVALID	SOCKET_INVALID
#define gcnet_socket_is_invalid(sock)   ((sock) == GCNET_SOCKET_INVALID)
typedef int socklen_t;
#else
typedef int GcnetSocket;
#define GCNET_SOCKET_INVALID	(-1)
#define gcnet_socket_is_invalid(sock)	((sock) < 0)
#endif

static void gcnet_socket_close (GcnetSocket sock);

/* Is the given error code just a transient condition? */
static int
gcnet_socket_errno_is_ignorable (int errcode)
{
  if (errcode == EINTR)
    return 1;
#ifdef EAGAIN
  if (errcode == EAGAIN)
    return 1;
#endif
#ifdef EWOULDBLOCK
  if (errcode == EWOULDBLOCK)
    return 1;
#endif
  return 0;
}

/* Set the socket to have non-blocking semantics. */
static void
gcnet_socket_set_nonblocking (GcnetSocket sock)
{
  int flags = fcntl (sock, F_GETFL);
  if (flags >= 0)
    fcntl (sock, F_SETFL, flags | O_NONBLOCK);
}

/* Create a new socket, bound to a particular port
   in the TCP or UDP namespace */
static GcnetSocket
gcnet_socket_listen (int port, int is_tcp)
{
  GcnetSocket sock;
  struct sockaddr addr;
  struct sockaddr_in *addr_in = (struct sockaddr_in *) (&addr);

  sock = socket (PF_INET, is_tcp ? SOCK_STREAM : SOCK_DGRAM, 0);
  if (gcnet_socket_is_invalid (sock))
    {
      warn ("failed to make listening socket");
      return GCNET_SOCKET_INVALID;
    }

  memset (addr_in, 0, sizeof (struct sockaddr_in));
  addr_in->sin_family = AF_INET;
  addr_in->sin_port = htons (port);
  if (bind (sock, &addr, sizeof (addr)) < 0)
    {
      warn ("error binding on socket");
      gcnet_socket_close (sock);
      return GCNET_SOCKET_INVALID;
    }

  if (is_tcp)
    if (listen (sock, 128) < 0)
      {
	warn ("error listening on socket: %s", strerror (errno));
	gcnet_socket_close (sock);
	return GCNET_SOCKET_INVALID;
      }

  return sock;
}

static GcnetSocket
gcnet_socket_connect_by_addr (const struct sockaddr *addr_tmp, boolean is_tcp,
			      boolean *is_connecting_out)
{
  struct sockaddr addr = *addr_tmp;
  GcnetSocket sock;
  sock = socket (PF_INET, is_tcp ? SOCK_STREAM : SOCK_DGRAM, 0);
  if (gcnet_socket_is_invalid (sock))
    {
      warn ("failed to make listening socket");
      return GCNET_SOCKET_INVALID;
    }

  if (is_tcp)
    {
      gcnet_socket_set_nonblocking (sock);
retry_connect:
      if (connect (sock, &addr, sizeof (struct sockaddr)) < 0)
	{
	  if (gcnet_socket_errno_is_ignorable (errno))
	    {
	      warn ("ignorable error connecting; retry");
	      goto retry_connect;
	    }
	  if (errno == EINPROGRESS)
	    {
	      *is_connecting_out = TRUE;
	      return sock;
	    }

	  warn ("error connecting: %s", strerror (errno));
	  gcnet_socket_close (sock);
	  return GCNET_SOCKET_INVALID;
	}
    }
  else
    {
#if 0
      /* use 'bind' to express sole interest in a particular host */
      if (bind (sock, addr, sizeof (struct sockaddr)) < 0)
	{
	  warn ("error binding UDP socket to remote addr: %s",
		strerror (errno));
	  gcnet_socket_close (sock);
	  return GCNET_SOCKET_INVALID;
	}
#endif
    }
  *is_connecting_out = FALSE;
  return sock;
}

static int
lookup_addr (struct sockaddr *addr,
	     const char *host,
	     unsigned port)
{
  struct hostent *hent;
  struct sockaddr_in addr_in;
  hent = gethostbyname (host);
  if (hent == NULL)
    {
      warn ("error resolving hostname '%s'", host);
      return 0;
    }
  memset (&addr_in, 0, sizeof (addr_in));
  addr_in.sin_port = htons (port);
  memcpy (&addr_in.sin_addr, hent->h_addr_list[0], 4);
  memcpy (addr, &addr_in, sizeof (struct sockaddr_in));
  return 1;
}

#if 0
/* Create a new socket which is connecting
   to the remote host and port and domain (either TCP or UDP).
   If *is_connecting is TRUE, the socket is not yet connected.
   If *is_connecting is FALSE, the socket is connected. */
static GcnetSocket
gcnet_socket_connect(const char *hostname, int port, int is_tcp,
		     boolean *is_connecting)
{
  struct sockaddr addr;
  if (!lookup_addr (&addr, hostname, port))
    return GCNET_SOCKET_INVALID;
  return gcnet_socket_connect_by_addr (&addr, is_tcp, is_connecting);
}
#endif

static int
errno_from_sock (GcnetSocket sock)
{
  socklen_t size_int = sizeof (int);
  int value = EINVAL;
  if (getsockopt (sock, SOL_SOCKET, SO_ERROR, &value, &size_int) < 0)
    {
      /* Note: this behavior is vaguely hypothetically broken,
       *       in terms of ignoring getsockopt's error;
       *       however, this shouldn't happen, and EINVAL is ok if it does.
       *       Furthermore some broken OS's return an error code when
       *       fetching SO_ERROR!
       */
      return value;
    }

  return value;
}

static boolean
gcnet_socket_finish_connect (GcnetSocket sock,
			     boolean    *is_connected_out)
{
  int err = errno_from_sock (sock);
  if (err == 0)
    {
      *is_connected_out = TRUE;
      return TRUE;
    }

  if (!gcnet_socket_errno_is_ignorable (err))
    {
      warn ("error finishing connection: %s", strerror (err));
      return FALSE;
    }

  /* CONSIDER: maybe we should try again... ?? */
  warn ("gcnet_socket_finish_connect: got ignorable error");
  *is_connected_out = TRUE;
  return TRUE;
}

static GcnetSocket
gcnet_socket_accept (GcnetSocket listener)
{
  struct sockaddr addr;
  socklen_t len = sizeof (addr);
  return accept (listener, &addr, &len);
}

/* Returns an error message. */
static const char *
gcnet_socket_transmit (GcnetSocket sock,
		       const void *data,
		       unsigned max_length,
		       unsigned *n_written_out)
{
  int rv = write (sock, data, max_length);
  if (rv < 0)
    {
      int e = errno;
      if (gcnet_socket_errno_is_ignorable (e))
	{
	  *n_written_out = 0;
	  return NULL;
	}
      return strerror (e);
    }
  *n_written_out = rv;
  return NULL;
}

static const char *
gcnet_socket_sendto   (GcnetSocket sock,
		       const void *data,
		       unsigned    length,
		       const struct sockaddr *addr)
{
  if (sendto (sock, data, length,
	      MSG_DONTWAIT,  // ???
	      addr, sizeof (struct sockaddr)) < 0)
    {
      int e = errno;
      if (gcnet_socket_errno_is_ignorable (e))
	return NULL;
      return strerror (e);
    }
  return NULL;
}

/* Returns an error message.
 * May return the string "EOF". */
static const char *
gcnet_socket_read     (GcnetSocket sock,
		       void *data,
		       unsigned max_length,
		       unsigned *n_recvd_out)
{
  int rv = read (sock, data, max_length);
  if (rv < 0)
    {
      int e = errno;
      if (gcnet_socket_errno_is_ignorable (e))
	{
	  *n_recvd_out = 0;
	  return NULL;
	}
      return strerror (e);
    }
  *n_recvd_out = rv;
  return NULL;
}

static void
gcnet_socket_close (GcnetSocket sock)
{
#if WIN32
  closesocket(sock);
#else
  close(sock);
#endif
}

static void
gcnet_socket_pipe (GcnetSocket *pipe_ends)
{
  GcnetSocket sock = gcnet_socket_listen (0, TRUE);
  struct sockaddr addr;
  socklen_t addr_len = sizeof(addr);
  GcnetSocket connector, accepted;
  boolean is_connecting;
  if (getsockname (sock, &addr, &addr_len) < 0)
    die ("getsockname() failed on listening socket: %s",
	 strerror (errno));
  connector = gcnet_socket_connect_by_addr (&addr, TRUE, &is_connecting);
  accepted = gcnet_socket_accept (sock);
  if (gcnet_socket_is_invalid (sock)
   || gcnet_socket_is_invalid (connector)
   || gcnet_socket_is_invalid (accepted))
    {
      die ("error creating one of socketpair");
    }
  gcnet_socket_close (sock);

  pipe_ends[0] = connector;
  pipe_ends[1] = accepted;
}

/* --- time --- */
#ifdef WIN32
typedef LONG GcnetTimeval;
static GcnetTimeval gcnet_get_timeval (void)
{
  GcnetTimeval rv;
  GetSystemTime (&rv);
  return rv;
}
static int gcnet_timeval_subtract_ms (GcnetTimeval a, GcnetTimeval b)
{
  return a - b;
}
#else
typedef struct timeval GcnetTimeval;
static GcnetTimeval gcnet_get_timeval (void)
{
  GcnetTimeval rv;
  gettimeofday (&rv, NULL);
  return rv;
}
static void gcnet_timeval_add_ms (GcnetTimeval *inout, int ms)
{
  if (ms < 0)
    {
      int new_usec;
      inout->tv_sec -= (-ms) / 1000;
      new_usec = inout->tv_usec - (-ms) % 1000 * 1000;
      if (new_usec < 0)
	{
	  inout->tv_sec--;
	  inout->tv_usec = 1000*1000 + new_usec;
	}
      else
	inout->tv_usec = new_usec;
    }
  else if (ms > 0)
    {
      int new_usec;
      inout->tv_sec += ms / 1000;
      new_usec = inout->tv_usec + ms % 1000 * 1000;
      if (new_usec >= 1000*1000)
	{
	  inout->tv_sec++;
	  inout->tv_usec = new_usec - 1000*1000;
	}
      else
	inout->tv_usec = new_usec;
    }
}
static int gcnet_timeval_subtract_ms (GcnetTimeval a, GcnetTimeval b)
{
  return (a.tv_sec - b.tv_sec) * 1000
       + (a.tv_usec - b.tv_usec) * 1000;
}
#endif


/* =====================================================================
 *                           Structures
 * ===================================================================== */
struct _Gcnet
{
  unsigned is_server : 1;
  unsigned must_retry : 1;	/* only if !is_server */
  unsigned port;
  GcnetReceiveFunc receive_func;
  void *receive_func_data;
  struct sockaddr remote_addr;	/* only if !is_server */

  /* XXX: retry_time is not needed */
  GcnetTimeval retry_time;	/* only if must_retry */
};


/* =====================================================================
 *                         List of Connections to Retry.
 * ===================================================================== */
typedef struct _RetryConnectionList RetryConnectionList;
struct _RetryConnectionList
{
  struct sockaddr connect_to;
  GcnetTimeval retry_time;
  RetryConnectionList *next;
  Gcnet *gcnet;
};
static RetryConnectionList *retry_list = NULL;
static void 
append_connection_to_retry__locked (GcnetTimeval timeout,
			            Gcnet       *gcnet)
{
  RetryConnectionList *list;
  list = malloc (sizeof (RetryConnectionList));
  list->connect_to = gcnet->remote_addr;
  list->retry_time = timeout;
  list->next = retry_list;
  list->gcnet = gcnet;
  retry_list = list;
}

/* =====================================================================
 *                           Main-Loop Implementation
 * ===================================================================== */
typedef struct _GcnetSockInfo GcnetSockInfo;
typedef struct _GcnetTcpConnection GcnetTcpConnection;

/* GcnetSocketType:
     Describes the type and use of a given socket.
    
     Any SOCKET/file-descriptor known to the main-loop
     has one of these types.

 Other remarks:

   Non-main loop file-descriptors.
     - the write-end of the wakeup file-descriptor in not managed;
       it is assumed that if a single-byte write fails then the buffer
       is full, so there is no need to indicate later.
       Since it is not managed by the main-loop,
       the write-end of the wakeup pipe is not included in
       this list.
 */
typedef enum
{
  GCNET_SOCKET_TYPE_TCP_LISTENER,
  GCNET_SOCKET_TYPE_TCP_SERVER_CONNECTION,
  GCNET_SOCKET_TYPE_TCP_CLIENT_CONNECTION,
  GCNET_SOCKET_TYPE_UDP_SERVER,
  GCNET_SOCKET_TYPE_UDP_CLIENT,
  GCNET_SOCKET_TYPE_WAKEUP
} GcnetSocketType;

struct _GcnetSockInfo
{
  int is_tcp;
  GcnetSocket sock;
  GcnetSocketType type;

  union {
    Gcnet *listener;
    GcnetTcpConnection *tcp_connection;
    Gcnet *udp;
  } info;
};

/* --- Data shared between threads: the SockInfo table --- */
static unsigned n_global_sock_info = 0;
static GcnetSockInfo *global_sock_info = NULL;
static unsigned global_sock_info_alloced = 0;
static pthread_mutex_t global_lock = PTHREAD_MUTEX_INITIALIZER;
static GcnetSocket wakeup_sock;
#define LOCK()		pthread_mutex_lock(&global_lock)
#define UNLOCK()	pthread_mutex_unlock(&global_lock)

#if 0
static GcnetSockInfo *
gcnet_sock_info_lookup__locked (GcnetSocket sock)
{
  unsigned i;
  for (i = 0; i < n_global_sock_info; i++)
    if (global_sock_info[i].sock == sock)
      return &global_sock_info[i];
  return NULL;
}
#endif

static void
gcnet_sock_info_append_uninit__locked (unsigned count)
{
  while (n_global_sock_info + count > global_sock_info_alloced)
    {
      if (global_sock_info_alloced == 0)
	global_sock_info_alloced = 16;
      else
	global_sock_info_alloced *= 2;
      global_sock_info = realloc (global_sock_info, sizeof (GcnetSockInfo) * global_sock_info_alloced);
    }
  n_global_sock_info += count;
}

typedef struct _GcnetConnection GcnetConnection;
typedef struct _GcnetPacket GcnetPacket;
typedef struct _GcnetPacketRing GcnetPacketRing;

struct _GcnetTcpConnection
{
  /* For TCP connections. */
  unsigned char packet_header[4];
  unsigned packet_length_length;

  /* If packet_length_length == 4. */
  GcnetPacket *partial;
  unsigned partial_size;

  /* Ring of completed incoming packets. */
  GcnetPacketRing *incoming_packets;
  unsigned incoming_count;

  /* Outgoing packet whose transmission has begun.
   * The packet will be transmitted once outgoing_partial_sent==outgoing_partial->size + 4,
   * since that is the header size. */
  unsigned char outgoing_header[4];
  GcnetPacket *outgoing_partial;
  unsigned outgoing_partial_sent;

  /* Full outgoing packets. */
  GcnetPacketRing *outgoing;
  unsigned outgoing_count;

  /* Is this a server or client connection? */
  int is_server;
  int is_connected;

  Gcnet *gcnet;

  /* Socket in the underlying system. */
  GcnetSocket sock;
};

struct _GcnetPacket
{
  unsigned length;
  void *data;
  unsigned ref_count;
};

static GcnetPacket *
gcnet_packet_alloc (unsigned len)
{
  GcnetPacket *packet = malloc (sizeof (GcnetPacket));
  packet->data = malloc (len);
  packet->length = len;
  packet->ref_count = 1;
  return packet;
}

static void
gcnet_packet_unref__locked (GcnetPacket *packet)
{
  if (--(packet->ref_count) == 0)
    {
      free (packet->data);
      free (packet);
    }
}

static void
gcnet_packet_ref__locked (GcnetPacket *packet)
{
  ++(packet->ref_count);
}

struct _GcnetPacketRing
{
  GcnetPacket *packet;
  GcnetPacketRing *next;
  GcnetPacketRing *prev;
};

static GcnetPacketRing *
gcnet_packet_ring_append__locked (GcnetPacketRing *ring,
			          GcnetPacket *packet)
{
  GcnetPacketRing *new_elt = malloc (sizeof (GcnetPacketRing));
  gcnet_packet_ref__locked (packet);
  new_elt->packet = packet;
  if (ring != NULL)
    {
      new_elt->next = ring;
      new_elt->prev = ring->prev;
      ring->prev->next = new_elt;
      ring->prev = new_elt;
    }
  else
    {
      new_elt->next = new_elt->prev = new_elt;
      ring = new_elt;
    }
  return ring;
}

static void
gcnet_packet_ring_free__locked (GcnetPacketRing *ring)
{
  GcnetPacketRing *at = ring;
  if (at == NULL)
    return;
  do
    {
      GcnetPacketRing *next = at->next;
      gcnet_packet_unref__locked (at->packet);
      free (at);
      at = next;
    }
  while (at != ring);
}

static GcnetTcpConnection *
tcp_connection_alloc    (Gcnet  *gcnet,
		         GcnetSocket sock,
		         boolean is_server,
		         boolean is_connected)
{
  GcnetTcpConnection *conn = malloc (sizeof (GcnetTcpConnection));
  conn->packet_length_length = 0;
  conn->partial = NULL;
  conn->partial_size = 0;
  conn->incoming_packets = NULL;
  conn->incoming_count = 0;
  conn->outgoing_partial = NULL;
  conn->outgoing_partial_sent = 0;
  conn->outgoing = NULL;
  conn->outgoing_count = 0;
  conn->is_server = is_server;
  conn->is_connected = is_connected;
  conn->sock = sock;
  conn->gcnet = gcnet;
  return conn;
}

static void
tcp_connection_free__locked (GcnetTcpConnection *conn)
{
  if (conn->partial)
    gcnet_packet_unref__locked (conn->partial);
  if (conn->outgoing_partial)
    gcnet_packet_unref__locked (conn->outgoing_partial);
  gcnet_packet_ring_free__locked (conn->incoming_packets);
  gcnet_packet_ring_free__locked (conn->outgoing);
  free (conn);
}

typedef struct _CallbackInfo CallbackInfo;
struct _CallbackInfo
{
  Gcnet *net;
  GcnetPacket *packet;
};

/* Initialize the 'read', 'write' and 'except' file-descriptor sets. */
static void
initialize_fdsets__locked (fd_set *read_set_out,
		           fd_set *write_set_out,
		           fd_set *except_set_out,
			   unsigned *max_sock_out)
{
  unsigned i;
  unsigned max_sock = 0;
  warn ("n_global_sock_info=%u", n_global_sock_info);
  FD_ZERO (read_set_out);
  FD_ZERO (write_set_out);
  FD_ZERO (except_set_out);
  for (i = 0; i < n_global_sock_info; i++)
    {
      GcnetSockInfo *info = global_sock_info + i;

      if ((unsigned)info->sock > max_sock)
	max_sock = info->sock;

      warn ("i=%u", i);
      
      switch (global_sock_info[i].type)
	{
	case GCNET_SOCKET_TYPE_TCP_LISTENER:
	  FD_SET (info->sock, read_set_out);
	  FD_SET (info->sock, except_set_out);
	  break;
        case GCNET_SOCKET_TYPE_TCP_SERVER_CONNECTION:
        case GCNET_SOCKET_TYPE_TCP_CLIENT_CONNECTION:
	  {
	    GcnetTcpConnection *conn = info->info.tcp_connection;
	    FD_SET (info->sock, read_set_out);
	    FD_SET (info->sock, except_set_out);
	    if (!conn->is_connected
             || conn->outgoing_partial != NULL
	     || conn->outgoing != NULL)
	      FD_SET (info->sock, write_set_out);
	    break;
	  }
        case GCNET_SOCKET_TYPE_UDP_SERVER:
        case GCNET_SOCKET_TYPE_UDP_CLIENT:
	  {
	    FD_SET (info->sock, read_set_out);
	    FD_SET (info->sock, except_set_out);
	    break;
	  }
	case GCNET_SOCKET_TYPE_WAKEUP:
	  /* Discard any incoming data. */
	  {
	    char buf[4096];
	    unsigned n_read;
	    gcnet_socket_read (info->sock, buf, sizeof (buf), &n_read);
	  }
	  break;
	default:
	  assert (0);
	}
    }

  warn ("initialize_fdsets__locked");

  *max_sock_out = max_sock;
}

/* Handle accepting a new connection from a Listener socket.

   listener_index is the index in the global socket info array
   corresponding to the socket (file-descriptor on unix)
   that had the readable-event (meaning that a new connection is ready
   to be accepted.). */
static void
tcp_listener_try_accept__locked (unsigned listener_index)
{
  GcnetSockInfo *info = global_sock_info + listener_index;
  GcnetSocket accepted = gcnet_socket_accept (info->sock);
  unsigned new_index;
  GcnetTcpConnection *new_connection;
  if (gcnet_socket_is_invalid (accepted))
    {
      warn ("Failed to accept new connection");
      return;
    }

  /* Allocate a new server connection. */
  new_index = n_global_sock_info;
  gcnet_sock_info_append_uninit__locked (1);
  new_connection = tcp_connection_alloc (info->info.listener, accepted, TRUE, TRUE);

  global_sock_info[new_index].is_tcp = 1;
  global_sock_info[new_index].sock = accepted;
  global_sock_info[new_index].type = GCNET_SOCKET_TYPE_TCP_SERVER_CONNECTION;
  global_sock_info[new_index].info.tcp_connection = new_connection;
}

/* Handle a TCP socket that is finished connecting.

   sock_info_index is the index in the global socket info array
   corresponding to the socket (file-descriptor on unix)
   that had the event. */
static boolean
process_tcp_finish_connect__locked (unsigned sock_info_index)
{
  GcnetSockInfo *info = global_sock_info + sock_info_index;
  GcnetTcpConnection *conn = info->info.tcp_connection;
  assert (info->type == GCNET_SOCKET_TYPE_TCP_CLIENT_CONNECTION);
  assert (!conn->is_connected);
  if (gcnet_socket_finish_connect (info->sock, &conn->is_connected))
    return TRUE;
  warn ("error finishing TCP connection");
  return FALSE;
}

/* Handle a TCP socket that is ready-to-read.

   sock_info_index is the index in the global socket info array
   corresponding to the socket (file-descriptor on unix)
   that had the event. */
static boolean
process_tcp_is_readable__locked (unsigned sock_info_index)
{
  GcnetSockInfo *info = global_sock_info + sock_info_index;
  GcnetTcpConnection *conn = info->info.tcp_connection;
  char buf[8192];
  unsigned n_read;
  const char *err = gcnet_socket_read (info->sock, buf, sizeof(buf), &n_read);
  assert (info->type == GCNET_SOCKET_TYPE_TCP_CLIENT_CONNECTION
     ||   info->type == GCNET_SOCKET_TYPE_TCP_SERVER_CONNECTION);
  if (err != NULL)
    return FALSE;
  else
    {
      const char *at = buf;
      unsigned rem = n_read;
      while (rem > 0)
	{
	  if (conn->packet_length_length < 4)
	    {
	      unsigned xmit = MIN (4 - conn->packet_length_length, rem);
	      memcpy (conn->packet_header + conn->packet_length_length, at, xmit);
	      at += xmit;
	      rem -= xmit;
	      conn->packet_length_length += xmit;
	      if (conn->packet_length_length == 4)
		{
		  /* Allocate partial packet. */
		  unsigned len = htonl (* (unsigned *) conn->packet_header);
		  conn->partial = gcnet_packet_alloc (len);
		  conn->partial_size = 0;
		}
	    }
	  if (rem > 0 && conn->partial != NULL)
	    {
	      unsigned xmit = MIN (conn->partial->length - conn->partial_size, rem);
	      memcpy ((char *)conn->partial->data + conn->partial_size, at, xmit);
	      conn->partial_size += xmit;
	      rem -= xmit;
	      at += xmit;
	      if (conn->partial_size == conn->partial->length)
		{
		  /* ok, its a complete packet. */
		  
		  /* Compute/verify CRC32. */
		  unsigned char crc32[CRC32_SIZE];
		  GcnetPacket *packet = conn->partial;
		  crc32_big_endian ((char*)packet->data + 4, packet->length - 4, crc32);
		  if (memcmp (crc32, packet->data, CRC32_SIZE) != 0)
		    warn ("bad CRC-32 in TCP packet");
		  else
		    conn->incoming_packets = gcnet_packet_ring_append__locked (conn->incoming_packets, packet);
		  gcnet_packet_unref__locked (conn->partial);
		  conn->partial = NULL;
		  conn->partial_size = 0;
		  conn->packet_length_length = 0;
		}
	    }
	}
    }
  return TRUE;
}

static void
move_first_outgoing_packet_to_partial__locked (GcnetTcpConnection *conn)
{
  GcnetPacketRing *to_free = conn->outgoing;
  unsigned packet_size = conn->outgoing->packet->length;
  conn->outgoing_partial = conn->outgoing->packet;
  conn->outgoing_partial_sent = 0;

  conn->outgoing_header[0] = packet_size >> 24;
  conn->outgoing_header[1] = packet_size >> 16;
  conn->outgoing_header[2] = packet_size >> 8;
  conn->outgoing_header[3] = packet_size;

  if (conn->outgoing->next == conn->outgoing)
    {
      conn->outgoing = NULL;
    }
  else
    {
      GcnetPacketRing *new_outgoing = conn->outgoing->next;
      conn->outgoing->next->prev = conn->outgoing->prev;
      conn->outgoing->prev->next = conn->outgoing->next;
      conn->outgoing = new_outgoing;
    }
  free (to_free);
}

/* Handle a TCP socket that is ready-to-write.

   sock_info_index is the index in the global socket info array
   corresponding to the socket (file-descriptor on unix)
   that had the event. */
static boolean
process_tcp_is_writable__locked (unsigned sock_info_index)
{
  GcnetSockInfo *info = global_sock_info + sock_info_index;
  GcnetTcpConnection *conn = info->info.tcp_connection;
  unsigned n_written;
  const char *err;
  assert (info->type == GCNET_SOCKET_TYPE_TCP_CLIENT_CONNECTION
     ||   info->type == GCNET_SOCKET_TYPE_TCP_SERVER_CONNECTION);

  if (conn->outgoing_partial == NULL
   && conn->outgoing != NULL)
    move_first_outgoing_packet_to_partial__locked  (conn);
  while (conn->outgoing_partial != NULL)
    {
      /* Write the current partial packet. */
      if (conn->outgoing_partial_sent < 4)
	{
	  err = gcnet_socket_transmit (info->sock,
				       conn->outgoing_header + conn->outgoing_partial_sent,
				       4 - conn->outgoing_partial_sent,
				       &n_written);
	  if (err != NULL)
	    {
	      warn ("error transmitting packet header");
	      return FALSE;
	    }
	  conn->outgoing_partial_sent += n_written;
	}
      if (conn->outgoing_partial_sent >= 4)
	{
	  unsigned offset = conn->outgoing_partial_sent - 4;

	  err = gcnet_socket_transmit (info->sock,
				       (char*)(conn->outgoing_partial->data) + offset,
				       conn->outgoing_partial->length - offset,
				       &n_written);
	  if (err != NULL)
	    {
	      warn ("error transmitting packet header");
	      return FALSE;
	    }
	  conn->outgoing_partial_sent += n_written;
	  if (conn->outgoing_partial_sent == conn->outgoing_partial->length + 4)
	    {
	      conn->outgoing_partial = NULL;
	      conn->outgoing_partial_sent = 0;
	    }
	}

      /* Stop if we did not finish writing the current packet. */
      if (conn->outgoing_partial != NULL)
	break;

      /* Try processing the next packet, if possible. */
      if (conn->outgoing != NULL)
        move_first_outgoing_packet_to_partial__locked (conn);
    }

  return TRUE;
}

/* Run a given CallbackInfo, after verifying the CRC of the packet. */
static void
callback_info_maybe_invoke (CallbackInfo *callback_info)
{
  Gcnet *gcnet = callback_info->net;
  if (gcnet->receive_func != NULL)
    {
      GcnetPacket *packet = callback_info->packet;
      if (packet->length >= 4)
	{
	  unsigned char crc32[4];
	  crc32_big_endian ((char*)packet->data + 4, packet->length - 4, crc32);
	  if (memcmp (crc32, packet->data, 4) == 0)
	    (*gcnet->receive_func) ((char*)packet->data + 4,
				    packet->length - 4,
				    gcnet->receive_func_data);
	}
    }
}

/* Get the maximum number of callback-info that could be required
   to handle for a given select() statement. */
static unsigned
get_max_callback_info_required__locked (void)
{
  return n_global_sock_info;
}


/* Process the results of the select(2) statement.
   The cb_info array must be long enough,
   (see get_max_callback_info_required__locked()).
    
   The return value is the number of cb_info we filled up. */
static unsigned
process_select_results__locked (const fd_set *read_set,
				const fd_set *write_set,
				const fd_set *except_set,
				CallbackInfo *cb_info)
{
  unsigned n_cb_info = 0;
  unsigned i;
  unsigned orig_count = n_global_sock_info;

  /* If a connect failure occurs right now, when should we 
     retry the connection? */
  GcnetTimeval retry_time = gcnet_get_timeval ();
  gcnet_timeval_add_ms (&retry_time, CONNECT_RETRY_MS);

  for (i = 0; i < orig_count; )
    {
      GcnetSockInfo *info = global_sock_info + i;
      int kill_this_info = 0;

      warn ("process_select_results__locked: i=%u of %u", i, orig_count);

      switch (global_sock_info[i].type)
	{
	case GCNET_SOCKET_TYPE_TCP_LISTENER:
	  if (FD_ISSET (info->sock, read_set))
	    tcp_listener_try_accept__locked (i);
	  else if (FD_ISSET (info->sock, except_set))
	    {
	      warn ("Listening socket had exception");
	      kill_this_info = 1;
	    }
	  break;
        case GCNET_SOCKET_TYPE_TCP_SERVER_CONNECTION:
        case GCNET_SOCKET_TYPE_TCP_CLIENT_CONNECTION:
	  {
	      warn ("GCNET_SOCKET_TYPE_TCP readable");
	    if (FD_ISSET (info->sock, except_set))
	      kill_this_info = TRUE;
	    if (info->info.tcp_connection->is_connected)
	      {
		if (!kill_this_info && FD_ISSET (info->sock, read_set))
		  kill_this_info = ! process_tcp_is_readable__locked (i);
		if (!kill_this_info && !FD_ISSET (info->sock, write_set))
		  kill_this_info = ! process_tcp_is_writable__locked (i);
	      }
	    else
	      {
		if (!kill_this_info)
		  {
		    if  (FD_ISSET (info->sock, read_set)
		      || FD_ISSET (info->sock, write_set))
		      {
			kill_this_info = ! process_tcp_finish_connect__locked (i);
		      }
		  }
	      }
	    break;
	  }
        case GCNET_SOCKET_TYPE_UDP_SERVER:
        case GCNET_SOCKET_TYPE_UDP_CLIENT:
	  if (FD_ISSET (info->sock, read_set))
	    {
	      char buf[MAX_PACKET_SIZE];
	      unsigned n_read;
	      const char *err;
	      GcnetPacket *packet;
	      unsigned char crc32[CRC32_SIZE];
	      warn ("GCNET_SOCKET_TYPE_UDP_CLIENT readable");
	      err = gcnet_socket_read (info->sock, buf, MAX_PACKET_SIZE, &n_read);
	      if (err != NULL)
		{
		  if (strcmp (err, "EOF") != 0)
		    {
		      warn ("error reading from UDP socket: %s", err);
		      break;
		    }
		}
	      if (n_read < 4)
		{
		  warn ("got packet which was too short");
		  break;
		}

	      /* verify CRC32 */
	      crc32_big_endian (buf + 4, n_read - 4, crc32);
	      if (memcmp (buf, crc32, 4) != 0)
		{
		  warn ("bad CRC32 from UDP socket");
		  break;
		}

              /* Append packet to listening connections. */
              packet = gcnet_packet_alloc (n_read);
	      memcpy (packet->data, buf, n_read);
	      cb_info[n_cb_info].net = info->info.udp;
	      cb_info[n_cb_info].packet = packet;
	      n_cb_info++;
	    }
	  if (FD_ISSET (info->sock, except_set))
	    {
	      kill_this_info = TRUE;
	    }
	  break;
	case GCNET_SOCKET_TYPE_WAKEUP:
	  /* Discard any incoming data. */
	  {
	    char buf[4096];
	    unsigned n_read;
	    gcnet_socket_read (info->sock, buf, sizeof (buf), &n_read);
	  }
	  break;
	default:
	  assert (0);
	}

      warn ("kill_this_info=%u", i);
      if (kill_this_info)
	{
	  if (global_sock_info[i].type == GCNET_SOCKET_TYPE_TCP_CLIENT_CONNECTION)
	    {
	      /* We should sleep for CONNECT_RETRY_MS milliseconds
		 and then retry the connection. */
	      Gcnet *net = global_sock_info[i].info.tcp_connection->gcnet;
	      net->must_retry = 1;
	      net->retry_time = retry_time;
	      append_connection_to_retry__locked (retry_time, net);
	    }

	  gcnet_socket_close (global_sock_info[i].sock);
	  memmove (global_sock_info + i, global_sock_info + i + 1,
		   sizeof (GcnetSockInfo) * (n_global_sock_info - (i + 1)));
	  orig_count--;
	}
      else
	i++;
      warn ("next-iter");
    }
  return n_cb_info;
}

static int
get_timeout__locked (GcnetTimeval cur_time,
		     int          timeout)
{
  RetryConnectionList *list;
  for (list = retry_list; list != NULL; list = list->next)
    {
      int ms = gcnet_timeval_subtract_ms (retry_list->retry_time, cur_time);
      if (ms < 0)
	ms = 0;
      if (timeout < 0 || timeout > ms)
	timeout = ms;
    }
  return timeout;
}

static void
free_callback_info_array (CallbackInfo *cb_info, unsigned n_cb_info)
{
  unsigned i;
  LOCK ();
  for (i = 0; i < n_cb_info; i++)
    gcnet_packet_unref__locked (cb_info[i].packet);
  UNLOCK ();
  free (cb_info);
}

/* Run one iteration of the main-loop.

   timeout_ms may be a non-negative timeout in milliseconds,
   or -1 to indicate no timeout. */
static void
run_main_loop_once (int timeout_ms)
{
  fd_set read_set, write_set, except_set;
  GcnetSocket max_sock = 0;
  struct timeval tv;
  struct timeval *timeout;
  CallbackInfo *cb_info;
  unsigned n_cb_info;
  int select_rv;
  unsigned i;
  GcnetTimeval cur_time = gcnet_get_timeval ();

  warn("run_main_loop_once");

  /* Update timeout to consider any connections which must be retried. */
  timeout_ms = get_timeout__locked (cur_time, timeout_ms);

  /* Translate timeout_ms to a struct timeval. */
  if (timeout_ms < 0)
    timeout = NULL;
  else
    {
      tv.tv_sec = timeout_ms / 1000;
      tv.tv_usec = (timeout_ms % 1000) * 1000;
      timeout = &tv;
    }

  warn("LOCK");
  LOCK ();
  initialize_fdsets__locked (&read_set, &write_set, &except_set, &max_sock);
  UNLOCK ();

  /* Run the select.
     
     NOTE: This definitely needs to be run WITHOUT the lock,
     b/c we should not hold the lock while we are sleeping.
     
     You can break out of this by calling gcnet_wakeup();
     that may be called with or without a lock. */
  warn("running select");
  select_rv = select (max_sock + 1, &read_set, &write_set, &except_set, timeout);
  if (select_rv < 0)
    {
      int e = errno;
      if (gcnet_socket_errno_is_ignorable (e))
	return;

      /* TODO: warn user. */

      return;
    }
  warn("select returned %u", select_rv);

  /* Process the results of the select. */
  LOCK ();
  cb_info = malloc (sizeof (CallbackInfo) * get_max_callback_info_required__locked ());
  n_cb_info = process_select_results__locked (&read_set, &write_set, &except_set, cb_info);
  UNLOCK ();

  /* Invoke the user's callbacks for incoming packets
     that have valid CRC-32.

     This should be done WITHOUT holding the lock. */
  /* TODO: must defend against Gcnet getting destroyed before
     we get here.  Use a reference count or a flag i guess. */
  for (i = 0; i < n_cb_info; i++)
    callback_info_maybe_invoke (cb_info + i);

  free_callback_info_array (cb_info, n_cb_info);
}

static void *
comm_thread (void *data)
{
  for (;;)
    run_main_loop_once (-1);

  return data;
}

static void
create_thread (void *(*func)(void*), void *data)
{
  pthread_t thread;
  pthread_attr_t attr;
  pthread_attr_init (&attr);
  pthread_attr_setdetachstate (&attr, TRUE);
  if (pthread_create (&thread, &attr, func, data) < 0)
    {
      warn ("error creating communications thread");
      return;
    }
}

/* --- To initialize --- */

/* Do not use directly; use the maybe_initialize_gcnet() macro instead
   to guarantee that this function only gets called once. */
static void
_initialize_gcnet (void)
{
  GcnetSocket pipe_ends[2];
  unsigned sock_index;
  gcnet_socket_pipe (pipe_ends);
  wakeup_sock = pipe_ends[0];

  LOCK ();
  sock_index = n_global_sock_info;
  gcnet_sock_info_append_uninit__locked (1);
  global_sock_info[sock_index].is_tcp = TRUE;
  global_sock_info[sock_index].sock = pipe_ends[1];
  global_sock_info[sock_index].type = GCNET_SOCKET_TYPE_WAKEUP;
  UNLOCK ();

  warn ("create_thread");
  create_thread (comm_thread, NULL);
}

static pthread_once_t init_once_control = PTHREAD_ONCE_INIT;

#define maybe_initialize_gcnet()	pthread_once(&init_once_control, _initialize_gcnet)

/* --- To wakeup the main-loop --- */
static void
gcnet_wakeup (void)
{
  unsigned char zero = 0;
  unsigned n_written;
  const char *err = gcnet_socket_transmit (wakeup_sock, &zero, 1, &n_written);
  if (err)
    warn ("gcnet_wakeup: %s", err);
}

static Gcnet *
gcnet_alloc (int is_server, unsigned port)
{
  Gcnet *gcnet = malloc (sizeof (Gcnet));
  gcnet->is_server = is_server;
  gcnet->must_retry = 0;
  gcnet->port = port;
  gcnet->receive_func = NULL;
  gcnet->receive_func_data = NULL;
  return gcnet;
}

/* =====================================================================
 *                             Public Interface
 * ===================================================================== */
Gcnet *gcnet_new_client          (const char      *host,
                                  unsigned         port)
{
  int tcp_is_connected;
  int udp_is_connected;
  GcnetSocket tcp_sock;
  GcnetSocket udp_sock;
  GcnetTcpConnection *tcp_client_connection;
  Gcnet *gcnet;
  unsigned first_index;

  maybe_initialize_gcnet ();

  struct sockaddr addr;

  if (!lookup_addr (&addr, host, port))
    return NULL;
  tcp_sock = gcnet_socket_connect_by_addr (&addr, 1, &tcp_is_connected);
  if (gcnet_socket_is_invalid (tcp_sock))
    return NULL;
  udp_sock = gcnet_socket_connect_by_addr (&addr, 0, &udp_is_connected);
  if (gcnet_socket_is_invalid (udp_sock))
    return NULL;

  gcnet = gcnet_alloc (FALSE, port);
  gcnet->remote_addr = addr;
  tcp_client_connection = tcp_connection_alloc (gcnet, tcp_sock, FALSE, tcp_is_connected);

  /* Resize FD info array. */
  LOCK();
  first_index = n_global_sock_info;
  gcnet_sock_info_append_uninit__locked (2);
  global_sock_info[first_index+0].is_tcp = 1;
  global_sock_info[first_index+0].sock = tcp_sock;
  global_sock_info[first_index+0].type = GCNET_SOCKET_TYPE_TCP_CLIENT_CONNECTION;
  global_sock_info[first_index+0].info.tcp_connection = tcp_client_connection;
  global_sock_info[first_index+1].is_tcp = 0;
  global_sock_info[first_index+1].sock = udp_sock;
  global_sock_info[first_index+1].type = GCNET_SOCKET_TYPE_UDP_CLIENT;
  global_sock_info[first_index+1].info.udp = gcnet;
  UNLOCK();

  gcnet_wakeup();

  return gcnet;
}

Gcnet *gcnet_new_server          (unsigned         port)
{
  Gcnet *gcnet;
  unsigned first_index;
  GcnetSocket tcp_sock, udp_sock;

  maybe_initialize_gcnet ();

  gcnet = gcnet_alloc (TRUE, port);
  tcp_sock = gcnet_socket_listen (port, TRUE);
  udp_sock = gcnet_socket_listen (port, FALSE);

  LOCK ();
  first_index = n_global_sock_info;
  gcnet_sock_info_append_uninit__locked (2);
  global_sock_info[first_index+0].is_tcp = 1;
  global_sock_info[first_index+0].sock = tcp_sock;
  global_sock_info[first_index+0].type = GCNET_SOCKET_TYPE_TCP_LISTENER;
  global_sock_info[first_index+0].info.listener = gcnet;
  global_sock_info[first_index+1].is_tcp = 0;
  global_sock_info[first_index+1].sock = udp_sock;
  global_sock_info[first_index+1].type = GCNET_SOCKET_TYPE_UDP_SERVER;
  global_sock_info[first_index+1].info.udp = gcnet;
  UNLOCK ();

  gcnet_wakeup ();
  return gcnet;
}

static GcnetPacket *
packet_encapsulate (const void *body, unsigned len)
{
  GcnetPacket *packet = gcnet_packet_alloc (len + 4);
  memcpy ((char*)packet->data + 4, body, len);

  /* Prefix with CRC32 */
  crc32_big_endian (body, len, packet->data);

  return packet;
}

void   gcnet_transmit            (Gcnet           *connection,
                                  const void      *packet_body,
                                  unsigned         packet_length,
      	                          int              reliable)
{
  int found_client = 0;
  unsigned i;
  GcnetPacket *packet = NULL;

  if (connection->is_server && !reliable)
    {
      warn ("cannot transmit from a UDP server");
      return;
    }

  LOCK ();
  for (i = 0; i < n_global_sock_info; i++)
    {
      if (global_sock_info[i].type == GCNET_SOCKET_TYPE_TCP_SERVER_CONNECTION
       || global_sock_info[i].type == GCNET_SOCKET_TYPE_TCP_CLIENT_CONNECTION)
	{
	  GcnetTcpConnection *conn = global_sock_info[i].info.tcp_connection;
	  if (connection == conn->gcnet)
	    {
	      if (packet == NULL)
		packet = packet_encapsulate (packet_body, packet_length);
              conn->outgoing = gcnet_packet_ring_append__locked (conn->outgoing, packet);
	      found_client = 1;
	    }
	}
      else if (global_sock_info[i].type == GCNET_SOCKET_TYPE_UDP_CLIENT)
	{
	  if (connection == global_sock_info[i].info.udp)
	    {
	      const char *err;
	      /* Send UDP packet. */
	      if (packet == NULL)
		packet = packet_encapsulate (packet_body, packet_length);
	      err = gcnet_socket_sendto (global_sock_info[i].sock, packet->data, packet->length,
				         &(global_sock_info[i].info.udp->remote_addr));
	      if (err != NULL)
		warn ("error sending to address: %s", err);
	    }
	}
    }
  if (packet != NULL)
    gcnet_packet_unref__locked (packet);
  UNLOCK ();

  if (found_client)
    gcnet_wakeup ();
}

void   gcnet_set_receive_handler (Gcnet           *connection,
      	                          GcnetReceiveFunc receive_func,
      	                          void            *user_data)
{
  LOCK();
  connection->receive_func = receive_func;
  connection->receive_func_data = user_data;
  UNLOCK();
}

void   gcnet_destroy             (Gcnet           *connection)
{
  unsigned out = 0;
  unsigned i;
  LOCK();
  for (i = 0; i < n_global_sock_info; i++)
    {
      int to_kill = 0;
      switch (global_sock_info[i].type)
	{
        case GCNET_SOCKET_TYPE_TCP_LISTENER:
	  if (connection == global_sock_info[i].info.listener)
	    to_kill = 1;
	  break;
        case GCNET_SOCKET_TYPE_TCP_SERVER_CONNECTION:
        case GCNET_SOCKET_TYPE_TCP_CLIENT_CONNECTION:
	  if (connection == global_sock_info[i].info.tcp_connection->gcnet)
	    {
	      tcp_connection_free__locked (global_sock_info[i].info.tcp_connection);
	      to_kill = 1;
	    }
	  break;
        case GCNET_SOCKET_TYPE_UDP_SERVER:
        case GCNET_SOCKET_TYPE_UDP_CLIENT:
	  if (connection == global_sock_info[i].info.udp)
	    to_kill = 1;
	  break;
	case GCNET_SOCKET_TYPE_WAKEUP:
	  break;
	}
      if (to_kill)
	gcnet_socket_close (global_sock_info[i].sock);
      else
	global_sock_info[out++] = global_sock_info[i];
    }
  n_global_sock_info = out;
  UNLOCK();

  free (connection);
}
