#include "CorridorArcEvaluator.hh"

using namespace std;

int main(int argc, char **argv) {

  ////////////////////////////
  // do some argument checking
 
  // See LADARMapper for a clean way to do this.

  // Done doing argument checking.
  ////////////////////////////////
  
  
  // Start the MTA By registering a module
  // and then starting the kernel
  
  Register(shared_ptr<DGC_MODULE>(new CorridorArcEvaluator));
  StartKernel();
  
  return 0;
}

