
#include "CorridorArcEvaluator.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include <unistd.h>
#include <iostream>

// RDDF target points
RDDF rddf;

CorridorArcEvaluator::CorridorArcEvaluator() 
  : DGC_MODULE(MODULES::CorridorArcEvaluator, "CorridorArcEvaluator Planner", 0) {

}

CorridorArcEvaluator::~CorridorArcEvaluator() {
}


void CorridorArcEvaluator::Init() {
  // call the parent init first
  DGC_MODULE::Init();

  InitCorrArcEval();

  d.corrArcEmap.setNumTargetPoints(rddf.getNumTargetPoints());
  d.corrArcEmap.setTargetPoints(rddf.getTargetPoints());
  d.corrArcEmap.setLastWaypoint(0);
  d.corrArcEmap.setNextWaypoint(0);

  cout << "Module Init Finished" << endl;
}

void CorridorArcEvaluator::Active() {
  int i, lastWaypoint, nextWaypoint;
  double steerangle;
  double frontNorth, frontEast;
  int counter = 0;

  UpdateState();
  d.corrArcEmap.updateFrame(d.SS, true);
  cout << "Starting Active Funcion" << endl;
  while( ContinueInState() ) {

    //system("clear"); // clear screen on each loop to show output in same place
    
    //    cout << "updating state... " << endl;
    // update the state so we know where the vehicle is at
    UpdateState();
    
    nextWaypoint = rddf.getNextWaypointNumber(frontNorth, frontEast, nextWaypoint);
    lastWaypoint = nextWaypoint-1;
    if(lastWaypoint < 0)
      lastWaypoint = 0;
    d.corrArcEmap.setLastWaypoint(lastWaypoint);
    d.corrArcEmap.setNextWaypoint(nextWaypoint);
    d.corrArcEmap.updateFrame(d.SS, true);

    // Update the votes ... just a place holder
    /*    for(i = 0; i < NUMARCS; i++) {
      steerangle = GetPhi(i);
      d.corrArcEval.Votes[i].Goodness = MAX_GOODNESS/2;
      d.corrArcEval.Votes[i].Velo = MAX_SPEED;
    }
    */
    //    cout << "updating votes... " << endl;
    CorrArcEvalUpdateVotes();

    // and send them to the arbiter
    Mail m = NewOutMessage(MyAddress(), MODULES::Arbiter, ArbiterMessages::Update);
    // Send the arbiter our "ID" and the vote struct.  The weights are taken care of
    // by the arbiter.
    int myID = ArbiterInput::CorrArcEval;
    //printf(" [CorridorArcEvaluator::Active] ArbiterInput::CorrArcEval = %d\n",
    //		    ArbiterInput::CorrArcEval );
    //printf(" [CorridorArcEvaluator::Active] sending myID = %d\n", myID);
    m << myID << d.corrArcEval;
    SendMail(m);

#if 0
    // logging genMap
    // The condition on Northing is there to make sure our position is initialized
    //if( counter==0 && d.SS.Northing > 0 )//(!(counter % 10))
    if( !(counter % 10) && d.SS.Northing > 0 ) 
    {
      cout << "---saving genMap using saveMATFile" << endl;
      char filename[15] = "CAEgenMap";
      char filenum[5];
      sprintf(filenum, "%d", counter/10);
      strcat(filename, filenum);
      d.corrArcEmap.saveMATFile(filename, "CorrArcEval genMap data", "CAEmap");
      // added for MatlabDisplay to read the same file
      d.corrArcEmap.saveMATFile("../../DevModules/MatlabDisplay/CAEgenMap", 
		                "CorrArcEval genMap data for MatlabDisplay", "CAEmap");
      printf( "---saved genMap with d.SS.Northing = %10.3f\n", d.SS.Northing );

      // TODO: Take me out!!
      //getchar();
    }
#endif

    // print something out once in a while to indicate whether or not 
    // we're receiving state information properly
    if( !(counter % 10) ) {
      printf(" Northing = %10.2f, Easting = %10.2f, ", d.SS.Northing, d.SS.Easting );    
      printf(" waypoint = %d.\n", d.corrArcEmap.getNextWaypoint() );
    }

    // increment our frame counter
    counter++;
    
    // and sleep for a bit.  if you do not sleep here it will try to run
    // too fast and flood the MTA
    usleep(100000);
  }

  cout << "Finished Active State" << endl;

}


void CorridorArcEvaluator::Shutdown() {

  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;

  //Insert code to shut down PathEvaluation here

}

Mail CorridorArcEvaluator::QueryMailHandler(Mail& msg)
{
  Mail reply;   // allocate space for the reply

  // figure out what we are responding to
  switch(msg.MsgType()) 
  {
    case CorridorArcEvaluatorMessages::Get_genMapStruct:

      reply = ReplyToQuery(msg);
      {
	genMapStruct ms;

	// DEBUG
        printf(" About to ConvertGenMapToStruct\n");

        // TODO: Sometimes this next line will cause this module to 
	// segfault.  I suspect it's because we are trying to read from 
	// the map and write to it at the same time.  If so, we need
	// to fix it by doing thread locks. -Lars
	
	ConvertGenMapToStruct( d.corrArcEmap, ms );

	// DEBUG
        printf(" Just finished ConvertGenMapToStruct\n");
	
	// DEBUG
        printf(" ms.numRows = %d\n", ms.numRows );
	
	reply << ms;
      }
      break;

    default:
      // let the parent mail handler handle it.
      reply = DGC_MODULE::QueryMailHandler(msg);
      break;
  }

  return reply;
}

