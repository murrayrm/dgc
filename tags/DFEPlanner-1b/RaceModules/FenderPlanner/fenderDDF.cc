/* 
   fenderDDF.cc - implementation for fenderDDF.hh
   it's a lot like rddf.hh, actually...
   created by Rocky Velez, March 3, 2004
*/

#include "fenderDDF.hh"
#include <iomanip.h>

// Constructors
fenderDDF::fenderDDF()
  : numSegments(0)
{
  readFDDF("fenderDDF.dat");
}

fenderDDF::fenderDDF(char *pFileName)
  : numSegments(0)
{
  readFDDF(pFileName);
}

// Destructor
fenderDDF::~fenderDDF(){
}

// Accessors
/* bool fenderDDF::fenderpointsInSegment(int segmentNum){
  if (segmentList[segmentNum].numFenderpoints > 0)
    return true;
else
  return false;
} */

bool fenderDDF::fenderpointsFOV(int segmentNum, Vector pos){
  bool truth = false;
  if (segmentList[segmentNum].numFenderpoints == 0) {
    return false;
  }
  else {
    double wpnorthing = segmentList[segmentNum].WPnorthing;
    double wpeasting = segmentList[segmentNum].WPeasting;
    for (int fpNum = 0; fpNum < segmentList[segmentNum].numFenderpoints; fpNum++) {
      if (segmentList[segmentNum].fplist[fpNum].DistToWP < distBOB2WP(wpnorthing, wpeasting, pos)){
        truth = true;
      }
    }
  }
return truth;
}

Vector fenderDDF::findCentroid(int segmentNum, Vector pos){
  // Find the speed-weighted centroid for all points in the current corridor closer to the waypoint than BOB is
  // 1. sum.n = 0, sum.e = 0, sumSpeed = 0
  // 2. for i in struct
  //    1. if fp.dist <= bob.dist then  
  //       1. sum.n = sum.n + fp.n * fp.speed
  //       2. sum.e = sum.e + fp.e * fp.speed
  //       3. sumSpeed = sumSpeed + fp.speed
  //   centroid.n = sum.n/sumSpeed.n, centroid.e = sum.e/sumSpeed.e
  //   Cast votes
  //   if (still in corridor) then goto 3.1 else goto 1
//cout << "segment num: " << segmentNum << endl;
  staticFender sum; //not a real fenderpoint; just useful for math
  sum.N = 0;
  sum.E = 0;
  sum.speed = 0;
  //these are coordinates of WP at end of segment
  double wpnorthing = segmentList[segmentNum].WPnorthing;
  double wpeasting = segmentList[segmentNum].WPeasting;

   // cout << "numFenderPoints= " << segmentList[segmentNum-1].numFenderpoints << endl;
  cout << "fp: ";
  for (int fpNum = 0; fpNum < segmentList[segmentNum].numFenderpoints; fpNum++) {
    cout << fpNum << " "; 
    fenderpoint fp = segmentList[segmentNum].fplist[fpNum];
    if (fp.DistToWP < distBOB2WP(wpnorthing, wpeasting, pos)) {
      sum.N += fp.FPnorthing * fp.Speed;
      sum.E += fp.FPeasting * fp.Speed;
      sum.speed += fp.Speed;
    }
  }

  Vector centroid;
  centroid.N = sum.N / sum.speed;
  centroid.E = sum.E / sum.speed;

  return centroid;
}

int fenderDDF::getNumSegments() {
  return numSegments;
}

// Private functions

int fenderDDF::readFDDF(char * fileName){
 // reads the fenderDDF
//  cout << "readFDDF is running\n";
  int segmentCount;
  ifstream file;
  string line;
  segmentData eachSegment;
  
  file.open(fileName,ios::in);
  if(!file){
    cout << "UNABLE TO OPEN THE FenderDDF FILE" << endl;
    exit(1);
  }
  file >> segmentCount;              //first number of file is waypoint count
  segmentCount--;               //waypoint count = segmentCount + 1
  //  cout << setprecision(10);
  //  cout << "segmentCount= " << segmentCount << endl;
  segmentList = new segmentData[segmentCount+1];   //have a bogus segment 0
  numSegments = segmentCount;         //set this private variable
  numWP = segmentCount + 1;
  //while(!file.eof()){
  // need to check for lines starting with '#' (these should be ignored) - check bob reading format
  //skipping that detail for now
  //  cout << "about to loop on myWP\n";
  for (int myWP = 0; myWP < numWP; myWP++ ) {
//    cout << "myWP= " << myWP << endl;
    // Number of fenderpoints

    getline(file,line,SEGMENTDATA_DELIMITER);
    segmentList[myWP].numFenderpoints = atoi(line.c_str());
    //    cout << "numFenderpoints= " << segmentList[myWP].numFenderpoints << endl;
    segmentList[myWP].fplist = new fenderpoint[segmentList[myWP].numFenderpoints];    
    
    // Segment Number
    getline(file,line,SEGMENTDATA_DELIMITER);
    segmentList[myWP].segmentNumber = atoi(line.c_str());
    // segment 1 corresponds to WP 1 (from WP 0 to WP 1) => WP 0/segment 0 doesn't exist

    //    cout << "Segment Number: " << segmentList[myWP].segmentNumber << endl;
    //get the waypoint coordinates
    getline(file,line,FENDERPT_DELIMITER);
    segmentList[myWP].WPnorthing = atof(line.c_str());  // WPnorthing
    
    getline(file,line,FENDERPT_DELIMITER);
    segmentList[myWP].WPeasting = atof(line.c_str());   // WPeasting
     
    getline(file,line,SEGMENTDATA_DELIMITER);
    segmentList[myWP].WPradius = atof(line.c_str());   // WP radius

    // Now add each fenderpoint and its data into fplist:

    //    cout << "about to loop on fenderpoints\n";

    for (int i = 0; i < segmentList[myWP].numFenderpoints; i++) {
      //      cout << setprecision(10);
      //      cout <<"fp num= " << i << endl;
      getline(file,line,FENDERPT_DELIMITER);
      segmentList[myWP].fplist[i].FPnorthing = atof(line.c_str());  // FPnorthing
      //      cout << "fpnorthing: " << segmentList[myWP].fplist[i].FPnorthing << ", " ;
  
      getline(file,line,FENDERPT_DELIMITER);
      segmentList[myWP].fplist[i].FPeasting = atof(line.c_str());   // FPeasting
      //      cout <<"fpeasting: " << segmentList[myWP].fplist[i].FPeasting << ", " ;

      getline(file,line,FENDERPT_DELIMITER);
      segmentList[myWP].fplist[i].DistToWP = atof(line.c_str());    // DistToWP
      //      cout << "distToWP: " << segmentList[myWP].fplist[i].DistToWP << ", " ;
      
      getline(file,line,SEGMENTDATA_DELIMITER);
      segmentList[myWP].fplist[i].Speed = atof(line.c_str());       // Speed
      //      cout << "Speed: " << segmentList[myWP].fplist[i].Speed << endl;
    }

    getline(file, line, SEGMENT_DELIMITER);
  }
cout << "finished looping on myWP";

  // debug
  //cout << "RDDF: " << numTargetPoints << " target points read !!!" << endl;
  return(0);
}

double fenderDDF::distBOB2WP(const double &WPnorthing, const double &WPeasting,const Vector &pos){
  return hypot(pos.N - WPnorthing, pos.E - WPeasting); 
}

