#include <iostream>
#include <stdlib.h>
#include <fstream>
#include "global_map.hh"
#include "global.hh"
#include "waypoints.hh"

/* global_map_test.cc
23-jan-04   Alan Somers     creation */

/* This program tests the global_map functions.  It loads waypoints.gps,
then draws the corridor and writes it to a pgm file.  Proper usage is:
./global_map_test waypointfile outfile
*/

int main(){
    ofstream outfile("corridor.pgm");
    unsigned char* map ;
//    map=(unsigned char*)malloc(SIZEX*SIZEY);
    long int minSize = SIZEX < SIZEY ? SIZEX : SIZEY;
    pixel q1, q2, q3, q4, center;
    signed int northmax, northmin, eastmax, eastmin;
    Waypoints wp;                       //instance of Waypoints class
    Vector origin;
    
    //get the overall size of the map
    wp.read("waypoints.gps");
    northmax = (int) wp.get_current().northing;
    northmin = (int) northmax;
    eastmax = (int) wp.get_current().easting;
    eastmin = (int) eastmax;
    for(int i = 0; i < wp.get_size(); i++) {    
        int north = (int) wp.peek_waypoint(i).northing;
        int east = (int) wp.peek_waypoint(i).easting;
        int radius = (int) wp.peek_waypoint(i).radius;
        northmax = northmax > north + radius ? northmax : north + radius;
        northmin = northmin < north - radius ? northmin : north - radius;
        eastmin = eastmin < east - radius ? eastmin : east - radius;
        eastmax = eastmax > east + radius ? eastmax : east + radius; }

    //allocate the map
    origin.E = eastmin; origin.N = northmin;
//    map = (unsigned char*)malloc((eastmax - eastmin) * (northmax - northmin));
    map = (unsigned char*)malloc(SIZEX*SIZEY);
    //blanken the map
    fill_map(map, 0);

    // draw the waypoints as circles
    for (wp.get_start(); wp.get_current().num < wp.get_size(); wp.get_next()) {
    Vector waypoint;
    waypoint.N = wp.get_current().northing;
    waypoint.E = wp.get_current().easting; 
    pixel center = Vec2pix(subVector(waypoint, origin));
    cout << "X " << center.X << "\tY " << center.Y << endl;
    cout << "radius " << wp.get_current().radius << endl;
    draw_circle(map, center, 
        (int) wp.get_current().radius, 255);
    cout << "X " << center.X << "\tY " << center.Y << "after draw" << endl;}


/*    q1.Y=SIZEY/10;      q1.X = SIZEX/2;
    q2.Y=(SIZEY*1)/4;   q2.X = (SIZEX*3)/4;
    q3.Y=(SIZEY*9)/10;  q3.X = (SIZEX*3)/5;
    q4.Y=(SIZEY*3)/4;       q4.X = SIZEX/4;
    center.Y = SIZEY/3; center.X = SIZEX/3;
    fill_map(map, 100);
    draw_circle(map, center, minSize/4, 0);
    draw_rectangle(map, q2, q3, q4, q1, 50); 
    q1.Y=(SIZEY/4);     q1.X = SIZEX/4;
    q2.Y=SIZEY/4;       q2.X = (SIZEX*3)/4;
    q3.Y=(SIZEY*3)/4;   q3.X = (SIZEX*3)/4;
    q4.Y=(SIZEY*3)/4;   q4.X = (SIZEX) /4;
    draw_rectangle(map, q1, q2, q3, q4, 150); */
    outfile << "P2\n" << SIZEX << " " << SIZEY << "\n255\n";
    for (int i=0; i<SIZEY; i++) {
        for (int j=0; j<SIZEX; j++) {
            outfile << int(map[i*SIZEX + j]) << " ";}
        outfile << "\n"; }
    free(map);
    return(0); } 
