function draw_vehicle(str, x, y, Yaw, phi, color)
% Description: 
%   This MATLAB function creates a graphic of the Tahoe
%   vehicle on its first call, at a given position and
%   orientation.  On subsequent calls, it changes the 
%   location and orientation of this graphic.  It also
%   indicates the steering angle of the front wheels
%   
% Author: Lars Cremean
% Modified from: draw_vehicle.m <-- for the MVWT
% By: Lars Cremean
% Date: 05/28/2002
%
% Usage:
%   draw_vehicle( 'str', X, Y, Yaw, phi, 'color' )
%     
%     'str': string identifier (should be unique for
%       each  drawn vehicle)
%     x: x position of vehicle (meters)
%     y: y position of vehicle (meters)
%     th: vehicle orientation (radians)
%     phi: steering angle (radians)
%     color: MATLAB color specification
%
% Note: 
%   You may have to run "clear draw_vehicle" or
%   "clear all", and "hold on" to get this 
%   function to work properly.  Also, "axis equal"
%   will make the aspect ratio right
%
% Example function call:
%   draw_vehicle('2r', 1, 2, pi/4, 0.43, 'b' )
%

% set the heading
th = pi/2 - Yaw;

% make persistent variables for the line elements
persistent chassisX chassisY
persistent genwheelX genwheelY % generic wheel
persistent lbwX lbwY % left back wheel
persistent rbwX rbwY % right back wheel
persistent lfwX lfwY % left front wheel
persistent rfwX rfwY % right front wheel
persistent lfwXr lfwYr % rotated left front wheel
persistent rfwXr rfwYr % rotated right front wheel
% declare handles to the vehicle graphics persistent so
% we don't need to recreate them every call to this function
eval(['persistent h',str,'VEH h',str,'LBW h',str,'RBW'])
eval(['persistent h',str,'LFW h',str,'RFW'])


if eval(['isempty(h',str,'VEH)'])
    
    % initialize vehicle geometry (only need to do this once)  
    % define the geometry of the vehicle here
    % take the origin of the car to be at the rear axle
    % define zero Yaw to be pointing north, but plot using
    % heading (positive CCW from East)
    % from DynamicFeasibilityEvaluator/vehicle_constants.h
    length = 5.05;
    width  = 2.002;
    wbase  = 2.946; % distance between axles
    track  = 1.66;  % distance between wheels (average front/back)

    % wheel geometry
    wheelW = 0.5;   % wheel width
    wheelD = 1.0;   % wheel diameter

    % the geometry of each of the shapes to draw
    % the vehicle is pointing right initially (heading = 0)
    % with the center of the rear axle at the origin
    % go through the points CW from bottom left
    % make a generic wheel (rectangle) in this manner
    genwheelX = [-wheelD/2  wheelD/2 wheelD/2 -wheelD/2];
    genwheelY = [-wheelW/2 -wheelW/2 wheelW/2  wheelW/2];
    lbwX = 0        + genwheelX; % left back wheel
    lbwY = track/2  + genwheelY; % left back wheel
    rbwX = 0        + genwheelX; % right back wheel
    rbwY = -track/2 + genwheelY; % right back wheel
    lfwX = wbase    + genwheelX; % left front wheel
    lfwY = track/2  + genwheelY; % left front wheel
    rfwX = wbase    + genwheelX; % right front wheel
    rfwY = -track/2 + genwheelY; % right front wheel
    % the chassis line element
    chassisX = wbase/2 - length/2 + .3 + [0 length length 0];
    chassisY = [-width/2 -width/2 width/2 width/2]; 

    emopt = '''xor'''; % erase mode option
    
    % create the plot the vehicle (only need to do this once)
    eval(['h',str,'VEH = line(chassisX,chassisY);']); 
    % draw the wheels    
    eval(['h',str,'LBW = line(lbwX,lbwY);']);
    eval(['h',str,'RBW = line(rbwX,rbwY);']);
    eval(['h',str,'LFW = line(lfwX,lfwY);']);
    eval(['h',str,'RFW = line(rfwX,rfwY);']);
    % set properties of the figure elements
    eval(['set(h',str,'VEH,''EraseMode'',',emopt,',''Color'',''k'');']);
    eval(['set(h',str,'LBW,''EraseMode'',',emopt,',''Color'',''k'');']);
    eval(['set(h',str,'RBW,''EraseMode'',',emopt,',''Color'',''k'');']);
    eval(['set(h',str,'LFW,''EraseMode'',',emopt,',''Color'',''k'');']);
    eval(['set(h',str,'RFW,''EraseMode'',',emopt,',''Color'',''k'');']);
    
end

% front wheels (rotate in place first)
lfwXr = genwheelX*cos(-phi) - genwheelY*sin(-phi) + lfwX;
lfwYr = genwheelY*cos(-phi) + genwheelX*sin(-phi) + lfwY;
rfwXr = genwheelX*cos(-phi) - genwheelY*sin(-phi) + rfwX;
rfwYr = genwheelY*cos(-phi) + genwheelX*sin(-phi) + rfwY;

% reset figure handle parameters
eval(['set(h',str,'VEH,''Xdata'', x + chassisX*cos(th) - chassisY*sin(th));'])
eval(['set(h',str,'VEH,''Ydata'', y + chassisY*cos(th) + chassisX*sin(th));'])

drawnow

eval(['set(h',str,'LBW,''Xdata'', x + lbwX*cos(th) - lbwY*sin(th));'])
eval(['set(h',str,'LBW,''Ydata'', y + lbwY*cos(th) + lbwX*sin(th));'])
eval(['set(h',str,'RBW,''Xdata'', x + rbwX*cos(th) - rbwY*sin(th));'])
eval(['set(h',str,'RBW,''Ydata'', y + rbwY*cos(th) + rbwX*sin(th));'])
% front wheels (rotate, then translate)
eval(['set(h',str,'LFW,''Xdata'', x + lfwXr*cos(th) - lfwYr*sin(th));'])
eval(['set(h',str,'LFW,''Ydata'', y + lfwYr*cos(th) + lfwXr*sin(th));'])
eval(['set(h',str,'RFW,''Xdata'', x + rfwXr*cos(th) - rfwYr*sin(th));'])
eval(['set(h',str,'RFW,''Ydata'', y + rfwYr*cos(th) + rfwXr*sin(th));'])

drawnow
