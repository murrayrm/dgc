function update(a)
% takes vector of accels and applies INS to each accel value
global state s est_state es A C P R kstate ks gs

% generate noise in measurement
noise = randn(1)*sqrt(2);

% update actual
state(3) = a;

% update estimate
est_state(3) = a+noise;

% generate new states
state = INS(state);
est_state = INS(est_state);
s = [s; state'];
es = [es; est_state'];

% "Kalman Filter" 
% predicted state
kstate = A * kstate;

% update covariance matrix
P1 = A * P * A';

% measurement update 
% Kalman Gain
K = P1 * C' * inv(C * P1 * C' + R);

% take measurement
xnoise = randn * sqrt(.5);
xm = state(1) + xnoise;
am = a + noise;
Y = [xm; am];
gs = [gs; xm];

% update state estimate
kstate = kstate + K * ( Y - C * kstate);

%update covariance matrix
P = (eye(3) - K * C) * P1;

% update state track
ks = [ks; kstate'];


