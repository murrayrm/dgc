<!--
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.0//EN" "/usr/share/sgml/docbook/dtd/4.2/docbookx.dtd">
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.0//EN" "dtd/4.2/docbookx.dtd">
-->

<book id="dfe"><title>Dynamic Feasibility Evaluator for GrandChallenge</title>
    <chapter id="brief"><title>Brief Description of the DFE</title>
        <para>
        The Dynamic Feasibility Evaluator provides the arbiter with information on how feasible
        potential trajectories (arcs) are. 
        </para>
        <para>
        Currently, the DFE will check for lateral rollover, but not for frontal or backward rollover, nor for skidding or wheel slippage.
        It also does not consider "bottoming out" the suspension, nor going airborne over a bump or hill.
        </para>
        <para>
        To check for lateral rollover, a simple model of the lateral vehicle dynamics is used, for
        which the following assumptions are made:
        <itemizedlist>
            <listitem> ignore effect of suspension shifting the center of mass of the vehicle </listitem>
            <listitem> calculating only the instantaneous effect of a particular arc as if we switch steering angles 
            instantaneously, ignoring the steering dynamics. </listitem>
            <listitem> ignoring effect of conservation of angular momentum when instantaneously switching steering angles </listitem>
            <listitem> ignoring effect of throttle acceleration or braking.
               NOTE: if we do want to include this, we have to distinguish
                   between accel/decel due to engine torque/brakes and due
                   to gravity. One is a force on the center of gravity, the
                   others are torques around the rear axle. </listitem>
            <listitem> assuming steering angle does not affect contact point of wheels (this seems reasonable) </listitem>
            <listitem> assuming no slipping when computing roll torque. (? Does it make a difference ? Yes, if it alters
                the arc trajectory.)</listitem>
            <listitem> considering only the instanteous rollover, not throughout the entire arc.
                (If we wanted to, we could make the additional assumption that the terrain slope is constant throughout the arc,
                or that we have a model of the terrain with which to evaluate the vehcile dynamics)
            </listitem> 
        </itemizedlist>
        </para>
        <section id="better_parameterization"><title>? A better parameterization of control choices required ?</title>
            <para>
                NOTE: It is not clear that the current parameterization of arc choices is sufficient
                for the arbiter to have proper control of the vehicle.  Besides choosing a steering angle, the arbiter must
                also choose a velocity. It is probably also necessary to evaluate the safety/feasibility of sudden accelerations or 
                decelerations, but the current framework does not enable that. 
            </para>
            <para>
                One possible solution is to expand the evaluation domain to not only consider various choices of arcs (steering angle),
                but also various choices for velocities, providing a goodness/safety value for each combination of steering angle and 
                velocity.
            </para>
        </section>
    </chapter>

    <chapter id="functions"><title>Main and Auxilliary functions of the DFE</title>
        <section id="main_function"><title>Main Function: dynamic_feasibility_evaluator</title>
            <para><envar>void dynamic_feasibility_evaluator( const double v, 
                    const double yaw, const double pitch, const double roll, 
                    const double T_roll,
                    const double *steering_angle, const unsigned int n_steering_angles,
                    double *c_roll, double *min_v, double *max_v)</envar>
            </para>
            <para>
                The main function accepts as inputs:
                <itemizedlist>
                    <listitem> 
                        the dynamical state of the vehicle
                        <para>
                            Out of all the state variables, currently these are used:
                            <itemizedlist>
                                <listitem>speed</listitem>
                                <listitem>steering angle</listitem>
                                <listitem>yaw, pitch, and roll, to determine terrain slope</listitem>
                            </itemizedlist>
                        </para>
                    </listitem>
                    <listitem> 
                        information about the terrain (bumpiness, coefficient of friction) (currently not used)
                    </listitem>
                    <listitem>
                        a list of arcs to evaluate
                    </listitem>
                    <listitem>
                        a rollover safety threshold, <envar>T_roll</envar>, to compute the maximum (and minimum) safe velocities of each
                        arc (under the current control choice parameterization scheme)
                    </listitem>
                </itemizedlist>
            </para>
            <para>
                The outputs of the function are:
                <itemizedlist>
                    <listitem>
                        a goodness value for each arc evaluated, the goodness related to the safety with respect to rollover.
                    </listitem>
                    <listitem>
                        a maximum velocity for which it is safe to travel along that arc.
                        <para>
                            NOTE: for some peculiar cases (such as a steep embankment) there may be a minimum 
                            velocity as well as a maximum. So it is not clear that providing the arbiter with a
                            maximum velocity per each arc is the right parameterization.
                        </para>
                    </listitem>
                </itemizedlist>
            </para>
            <para>Inputs:
                <itemizedlist>
                    <listitem> v         : [m/s] forward velocity</listitem>
                     
                     <listitem>yaw, pitch, roll  : [radians] angles defining orientation of vehicle to inertial reference frame
                                             (i.e., from these we know the slope of the terrain)</listitem>
                     
                     <listitem>steering_angle    : [radians] array of steering angles defining the arcs to be evaluated</listitem>
                     
                     <listitem>n_steering_angles : number of entries in the array</listitem>
                     
                     <listitem>T_roll    : [unitless] rollover safety threshold, bounded from 0 to 1. At a value of 1, the vehicle</listitem>
                </itemizedlist>
            </para>
            <para>Outputs:
                <itemizedlist>
                    <listitem>c_roll    : [unitless] array of rollover coefficients for the evalutated arcs
                                     for the current forward velocity.
                        <itemizedlist>
                            <listitem> == 0 if the vehicle is completely stable, no lateral torque</listitem>
                            <listitem> < 0 if there is a tendency for a rightward lateral torque</listitem>
                            <listitem> > 0 if there is a tendency for a leftward lateral torque</listitem>
                            <listitem> == -1 if the vehicle is about to roll over rightwards, zero downwards force on left wheel</listitem>
                            <listitem> < -1 if the vehcile is rolling over rightwards</listitem>
                            <listitem> == 1 if the vehicle is about to roll over leftwards, zero downwards force on right wheel</listitem>
                            <listitem> > 1 if the vehcile is rolling over leftwards</listitem>
                        </itemizedlist>
                    </listitem>
                     
                    <listitem>min_v      : [m/s] array of minimum velocity for each arc such that the rollover coefficient is 
                                 within the rollover safety threshold T_roll
                    </listitem>

                    <listitem>max_v      : [m/s] array of maximum velocity for each arc such that the rollover coefficient is 
                                   within the rollover safety threshold T_roll
                    </listitem>
                </itemizedlist>
            </para>
        </section>

        <section id="steer_angle_2_radius"><title>steering_angle_2_arc_radius</title>
            <para><envar>double steering_angle_2_arc_radius( double s_angle )</envar></para>
            <para>Computes the radius of the arc the vehicle will follow for a given steering angle</para>
        </section>

        <section id="gravity_in_vehcile_ref_frame"><title>gravity_components_in_vehicle_ref_frame</title>
            <para><envar> void gravity_components_in_vehicle_ref_frame(const double yaw, const double pitch, const double roll, 
                    double *g_z, double *g_y, double *g_z)</envar></para>
            <para>Given yaw, pitch, roll, figure out vertical and lateral (and forward) components of gravity in the 
                vehicle reference frame</para>
            <para>Inputs:
                <itemizedlist>
                    <listitem>yaw, pitch, roll  : [radians] angles defining orientation of vehicle to inertial reference frame</listitem>
                </itemizedlist>
            </para>
            <para>Outputs:
                <itemizedlist>
                    <listitem>g_z : [m/s^2] downward component of gravity (in vehicle ref frame)</listitem>
                    <listitem>g_y : [m/s^2] lateral  component of gravity (in vehicle ref frame)</listitem>
                    <listitem>g_x : [m/s^2] forward  component of gravity (in vehicle ref frame)
                        <para>such that norm([g_x, g_y, g_z]) = 9.8 m/s^2</para>
                    </listitem>
                </itemizedlist>
            </para>
        </section>


        <section id="min_max_lateral_accel_for_rollover"><title>min_max_lateral_acceleration_for_rollover_safety</title>
            <para><envar>
            void min_max_lateral_acceleration_for_rollover_safety(const double g_z, const double g_y, const double T_roll,
                    double *a_R, double *a_L)</envar>
            </para>
            <para>Given rollover safety threshold compute min and max lateral accelerations</para>
            <para>Inputs:
                <itemizedlist>
                    <listitem>g_z : [m/s^2] downward component of gravity (in vehicle ref frame)</listitem>
                    <listitem>g_y : [m/s^2] lateral  component of gravity (in vehicle ref frame)</listitem>
                    <listitem>g_x : [m/s^2] forward  component of gravity (in vehicle ref frame)</listitem>
                    <listitem>T_roll: [unitless] rollover safety threshold</listitem>
                </itemizedlist>
            </para>
            <para>Outputs:
                <itemizedlist>
                    <listitem>a_R : [m/s^2] acceleration for which rollover safety threshold is achieved for a rightward roll</listitem>
                    <listitem>a_L : [m/s^2] acceleration for which rollover safety threshold is achieved for a leftward roll</listitem>
                        <para> NOTE: the safe lateral accelerations, a_safe are in the range
                            <equation id="safe_rollover_acceleration">a_R <= a_safe <= a_L</equation>
                        </para>
                </itemizedlist>
            </para>
        </section>

        <section id="rollover_coefficient"><title>rollover_coefficient</title>
            <para><envar>
                double rollover_coefficient(const double a_T, const double g_z, const double g_y)</envar>
            </para>
            <para>Given the current lateral acceleration (derived from current velocity and arc radius),
                compute the rollover coefficient
            </para>
            <para>Inputs:
                <itemizedlist>
                    <listitem> a_T : [m/s^2] lateral acceleration of vehicle due to following specified arc path at specified velocity
                    </listitem>
                    <listitem> g_z : [m/s^2] downward component of gravity (in vehicle ref frame)</listitem>
                    <listitem> g_y : [m/s^2] lateral  component of gravity (in vehicle ref frame)</listitem>
                </itemizedlist>
            </para>
            <para>Outputs:
                <itemizedlist>
                    <listitem>
                        c_roll : [unitless] the rollover coeffecient, with the following properties:
                            <itemizedlist>
                                <listitem> == 0 if the vehicle is completely stable, no lateral torque</listitem>
                                <listitem> < 0 if there is a tendency for a rightward lateral torque</listitem>
                                <listitem> > 0 if there is a tendency for a leftward lateral torque</listitem>
                                <listitem> == -1 if the vehicle is about to roll over rightwards, zero downwards force on left wheel</listitem>
                                <listitem> < -1 if the vehcile is rolling over rightwards</listitem>
                                <listitem> == 1 if the vehicle is about to roll over leftwards, zero downwards force on right wheel</listitem>
                                <listitem> > 1 if the vehcile is rolling over leftwards</listitem>
                            </itemizedlist>
                    </listitem>
                </itemizedlist>
            </para>
        </section>
                 
        <section id="min_max_rollover_velocity"><title>min_max_velocity_for_rollover_safety</title>
            <para><envar>
                void min_max_velocity_for_rollover_safety(const double a_R, const double a_L, const double arc_radius, 
                        double *min_v, double *max_v)</envar>
            </para>
            <para>
                Given the current arc and the lateral acceleration limits to stay within some rollover safety threshold,
                compute the corresponding min and max forward velocities.
            </para>
            <para>Inputs:
                <itemizedlist>
                    <listitem> a_R : [m/s^2] acceleration for which rollover safety threshold is achieved for a rightward roll</listitem>
                    <listitem> a_L : [m/s^2] acceleration for which rollover safety threshold is achieved for a leftward roll</listitem>
                    <listitem> arc_radius : [m] the radius of the arc being considered
                        <para>
                                  NOTE: arc_radius == 0 signifies moving straight ahead
                                          (more simple representation than arc_radius = infinity)
                        </para>
                    </listitem>
                </itemizedlist>
            </para>
            <para>Outputs:
                <itemizedlist>
                    <listitem> min_v : [m/s] minimum forward velocity to be within the rollover safety threshold</listitem>
                    <listitem> max_v : [m/s] maximum forward velocity to be within the rollover safety threshold</listitem>
                </itemizedlist>
            </para>
        </section>

        <!--
        <section id="simple_rollover"><title>simple_rollover</title>
            <para>
                The function that computes lateral rollover, <filename>simple_rollover</filename>, accepts as inputs:
                <itemizedlist>
                    <listitem>H : [m] the height of the center of mass off the ground</listitem>
                    <listitem>L : [m] the (lateral) distance from the left wheel to the projection of the
                        center of mass onto the rear axle</listitem>
                    <listitem>R : [m] the (lateral) distance from the right wheel to the projection of the
                        center of mass onto the rear axle</listitem>
                    <listitem>m : [kg] the total mass of the vehicle </listitem>
                    <listitem>a_T : [m/s^2] the total resultant acceleration of the vehicle, i.e., 
                        lateral centripetal acceleration (in the vehicle reference frame)
                        corresponding to following the given arc at the given speed
                    </listitem>
                    <listitem>g_z : [m/s^2] the downward (in vehicle reference frame) component of gravity</listitem>
                    <listitem>g_y : [m/s^2] the lateral  (in vehicle reference frame) component of gravity</listitem>
                </itemizedlist>
            </para>
            <para>
                The outputs of the function are:
                <itemizedlist>
                    <listitem>
                        c_roll : [unitless] the rollover coeffecient, with the following properties:
                            <itemizedlist>
                                <listitem> == 0 if the vehicle is completely stable, no lateral torque</listitem>
                                <listitem> < 0 if there is a tendency for a rightward lateral torque</listitem>
                                <listitem> > 0 if there is a tendency for a leftward lateral torque</listitem>
                                <listitem> == -1 if the vehicle is about to roll over rightwards, zero downwards force on left wheel</listitem>
                                <listitem> < -1 if the vehcile is rolling over rightwards</listitem>
                                <listitem> == 1 if the vehicle is about to roll over leftwards, zero downwards force on right wheel</listitem>
                                <listitem> > 1 if the vehcile is rolling over leftwards</listitem>
                            </itemizedlist>
                    </listitem>
                    <listitem>
                        F_L_z : [N] the upward (vehicle ref frame) ground reaction force on the left wheel
                        F_R_z : [N] the upward (vehicle ref frame) ground reaction force on the right wheel
                    </listitem>
                </itemizedlist>
            </para>
        </section>
        -->

        <section id="other_functions"><title>Other Functions</title>
            <section id="yaw_pitch_roll_2_matrix"><title>yaw_pitch_roll_2_matrix</title>
                <para>
                The function <filename>yaw_pitch_roll_2_matrix</filename> converts yaw, pitch, and roll angles
                into the corresponding rotation matrix that defines the relative orientation of the vehicle reference
                frame wrt the inertial ref frame.
                </para>
                <para> Inputs:
                    <itemizedlist> 
                        <listitem>yaw, pitch, roll : [radians] the yaw, pitch roll angles</listitem>
                    </itemizedlist>
                </para>
                <para> Outputs:
                    <itemizedlist>
                        <listitem>x, y, z : the unit vectors of the vehicle's ref frame expressed in the 
                            coordinate system of the inertial reference frame
                        </listitem>
                    </itemizedlist>
                </para>
                <para>
                    A good explanation of the definition of yaw, pitch, roll angles and their conversion to
                    a rotation matrix can be found on p 31 of Richard Murray's "A Mathematical Introduction
                    To Robotic Manipulation".
                </para>
            </section>
        </section>
    </chapter>

    <chapter id="lateral_dynamics_model"><title>Lateral Vehicle Dynamics Model</title>
        <section id="flat_ground"><title>Simplest case : flat ground</title>
            <para>
                The figure below shows the free body diagram of the vehicle on flat ground. It is a model of the lateral dynamics only,
                so we are looking at the vehicle from the back. We assume that the center of mass of the vehcile is:
                <itemizedlist>
                    <listitem>a height <envar>H</envar> above the ground,</listitem>
                    <listitem>a distance <envar>L</envar> from the left wheel ground contact point,</listitem>
                    <listitem>a distance <envar>R</envar> from the right wheel ground contact point.</listitem>
                </itemizedlist>
            </para>
            <mediaobject>
                <imageobject><imagedata fileref="rollover_on_flat_ground.jpg" format="jpg"></imagedata></imageobject>
            </mediaobject>
            <para>Solving the torque constraint equations about the left and right wheel contact points, we can
                find the vertical ground reaction forces, <envar>F_L_z</envar> and <envar>F_R_z</envar>, on the 
                left and right wheels, respectively, as shown in the figure.
            </para>
            <para>
                From the calculation of the ground reaction forces <envar>F_L_z</envar> and <envar>F_R_z</envar>,
                we can define the rollover safety coefficient, <envar>c_roll</envar> as:
                <equation>c_roll = ( F_R_z - F_L_z ) / ( F_R_z + F_L_z )</equation>
            </para>
            <para> The rollover safety cofficient has the following properties:
                <itemizedlist>
                    <listitem> c_roll == 0 if the vehicle is completely stable, no lateral torque</listitem>
                    <listitem> c_roll < 0  if there is a tendency for a rightward lateral torque</listitem>
                    <listitem> c_roll > 0  if there is a tendency for a leftward lateral torque</listitem>
                    <listitem> c_roll == -1 if the vehicle is about to roll over rightwards, zero downwards force on left wheel</listitem>
                    <listitem> c_roll < -1  if the vehcile is rolling over rightwards</listitem>
                    <listitem> c_roll == 1 if the vehicle is about to roll over leftwards, zero downwards force on right wheel</listitem>
                    <listitem> c_roll > 1 if the vehcile is rolling over leftwards</listitem>
                </itemizedlist>
            </para>
        </section>

        <section id="arbitray_terrain_slope"><title>Dealing with arbitray terrain slope</title>
            <para>
                The figure below shows how to solve for the ground reaction forces when the vehcile is on a slope. On a slope, 
                gravity does not point straight down, but will have a lateral (and forward, not shown) component as well. The equations
                are pretty similar.
            </para>
            <mediaobject>
                <imageobject><imagedata fileref="rollover_on_a_slope.jpg" format="jpg"></imagedata></imageobject>
            </mediaobject>
            <para>
                From the equations for the ground reaction forces, we can solve for the maximum accelerations to be within
                the rollover safety threshold for a righward and leftward roll. The limiting acceleartion is found by solving
                for the lateral acceleration that would cause
                <equation> abs(c_roll) = T_roll </equation>
            </para>
            <para> Let's call those two limits <envar>a_R</envar> and <envar>a_L</envar> such that:
                <equation>a_L = ( ((L_R)/(2*H))*(1+T) - R/H )*g_z + g_y</equation>
                <equation>a_R = ( ((L_R)/(2*H))*(1-T) - R/H )*g_z + g_y</equation>
            </para>
            <para>
                Thus, a motion is within the rollover safety treshold if the acceleration <envar>a_safe</envar> is
                within the range:
                <equation> a_R <= a_safe <= a_L </equation>
            </para>
        </section>
    </chapter>

    <chapter id="future_work"><title>Future Work</title>
        <section id="2D_sliding"><title>Modeling Sliding</title>
        ??? Lateral ground reaction forces are proportional to vertical component.
        
        ??? Each surface has a threshold, 
        </section>

        <section id="3D_rollover"><title>Modeling 3-D rollover</title>
        </section>

        <section id="bumpiness"><title>Modeling terrain bumpiness</title>
            <para>
            High frequency bumps can cause a rollover due sudden forces that make the vehicle unstable
            </para>
        </section>

        <section id="bumpiness"><title>Modeling terrain curvature</title>
            <para>

            </para>
        </section>

        <section id="bottoming_out"><title>Modeling suspension bottoming-out</title>
        </section>
    </chapter>
</book>
