//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// CR_Matrix
//----------------------------------------------------------------------------

#ifndef CR_MATRIX_DECS

//----------------------------------------------------------------------------

#define CR_MATRIX_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

// general

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdarg.h>
#include <string.h>

// Intel (thanks, Intel!)

#ifdef __cplusplus
extern "C" {
#endif
#include "mkl.h"
#ifdef __cplusplus
}
#endif

//#include <mkl.h>

// mine

#include "CR_Random.hh"
#include "CR_Error.hh"
#include "CR_Memory.hh"

// when g++
//#include "CRFunction.h"

// when gcc
//extern "C" {
//#include "CRFunction.h"
//	   }

//-------------------------------------------------
// defines
//-------------------------------------------------

//#define CR_MATRIX_DEBUG

#ifndef TRUE
#define TRUE          1
#endif

#ifndef FALSE
#define FALSE         0
#endif

#define SQUARE(x)     ((x) * (x))

// row major
#define MATRC(mat, r, c)     (mat->x[(c) + mat->cols * (r)])
// column major
#define CM_MATRC(mat, r, c)     (mat->x[(r) + mat->rows * (c)])

#define IN_MAT_HORIZ(mat, c)    (((c) >= 0) && ((c) < mat->cols))
#define IN_MAT_VERT(mat, r)     (((r) >= 0) && ((r) < mat->rows))
#define IN_MAT(mat, r, c)       (IN_MAT_HORIZ(mat, (c)) && IN_MAT_VERT(mat, (r)))

#ifndef PI
#define PI      3.14159265358979323846
#endif

#ifndef PI_2
#define PI_2    1.57079632679489661923
#endif

#ifndef TWOPI
#define TWOPI   (PI * 2.0)
#endif

#ifndef LN2
#define LN2   (0.6931471806)
#endif

#ifndef INV_LN2
#define INV_LN2   (1.442695041)
#endif

#define CR_EQUAL(A, B)        (fabs((A) - (B)) < 0.00001)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))
#define MAX3(A, B, C)         (((A) > (B)) ? MAX2((A), (C)) : MAX2((B), (C)))
#define MIN3(A, B, C)         (((A) < (B)) ? MIN2((A), (C)) : MIN2((B), (C)))

#define DEGS_PER_RADIAN     57.29578

#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define CR_MATRIX_MAX_NAME     256

//-------------------------------------------------

// Numerical Methods routines sometimes use these

#define SIGN(a,b) ((b) > 0.0 ? fabs(a) : -fabs(a))

static double sqrarg;
#define SQR(a) ((sqrarg = (a)) == 0.0 ? 0.0 : sqrarg * sqrarg)

static double maxarg1, maxarg2;
#define FMAX(a,b) (maxarg1=(a), maxarg2=(b), (maxarg1) > (maxarg2) ? (maxarg1) : (maxarg2))

static int iminarg1, iminarg2;
#define IMIN(a,b) (iminarg1=(a), iminarg2=(b),(iminarg1) < (iminarg2) ? (iminarg1) : (iminarg2))

#define GOLD               1.618034
#define GLIMIT             100.0
#define TINY               1.0e-20
#define SHFT(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);
#define GOLDEN_R           0.61803399          // "R" in Num. Rec.
#define GOLDEN_C           (1.0 - GOLDEN_R)    // "C" in Num. Rec.
#define SHFT2(a,b,c) (a)=(b);(b)=(c);
#define SHFT3(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);
#define ITMAX 100
#define CGOLD 0.3819660
#define ZEPS 1.0e-10
#define MOV3(a,b,c, d,e,f) (a)=(d);(b)=(e);(c)=(f);
#define TOL 2.0e-4
//#define FREEALL CR_free_calloc(xi,n,sizeof(double));CR_free_calloc(h,n,sizeof(double));CR_free_calloc(g,n,sizeof(double));
#define EPS 1.0e-10
#define CR_FREEALL free_CR_Vector(xi);free_CR_Vector(h);free_CR_Vector(g);

//-------------------------------------------------
// structure
//-------------------------------------------------

// 1-D of doubles

typedef struct crvector
{
  int rows;                  // currently used rows
  double *x;
  double buffsize;           // rows * sizeof(double)
  char *name;

} CR_Vector;

// 2-D of doubles

typedef struct crmatrix 
{
  int rows, cols;            // currently used rows, cols
  double *x;
  double buffsize;           // rows * cols * sizeof(double)
  char *name;

} CR_Matrix;

//-------------------------------------------------
// functions
//-------------------------------------------------

void convert_CR_Vector_array_to_CR_Matrix(CR_Vector **, CR_Matrix *);
void colsum_CR_Matrix(CR_Matrix *, CR_Vector *);
void mean_cov_CR_Matrix(CR_Matrix *, CR_Matrix *, CR_Vector *, CR_Matrix *);

double log2(double);
double tansig(double);
int count_lines(FILE *);
CR_Matrix **make_CR_Matrix_array(int);
void free_CR_Matrix_array(CR_Matrix **, int);
CR_Matrix *sum_CR_Matrix_array(CR_Matrix **, int);

void max_CR_Matrix(CR_Matrix *, double *, int *, int *);
void min_CR_Matrix(CR_Matrix *, double *, int *, int *);

//-------------------------------------------------
// Level 3 BLAS and LAPACK
//-------------------------------------------------

void init_svd(int);
void clapack_svd_CR_Matrix(CR_Matrix *, CR_Matrix *, double *, CR_Matrix *);;

int svdcmp(float **a, int m, int n, float w[], float **v);
float **matrix(long nrl, long nrh, long ncl, long nch);
float *vector(long nl, long nh);
void init_svdcmp(int n);

void write_pfm_CR_Matrix(char *, CR_Matrix *);
CR_Matrix **make_gabor_filter_bank_CR_Matrix_array(double, double, int, double, int, int *);
CR_Matrix **make_gabor_filter_bank_CR_Matrix_array(double, double, int, double, double, int, CR_Matrix *);
CR_Matrix **make_gabor_filter_bank_CR_Matrix_array(double, double, int, double, double, int, CR_Matrix *, int, int);
CR_Matrix **make_gabor_filter_bank_CR_Matrix_array(double, double, int, double, int, int *, int);
CR_Matrix *make_gabor_kernel_CR_Matrix(double, double, double);
CR_Matrix *make_gabor_kernel_CR_Matrix(double, double, double, int);
CR_Matrix *make_gabor_kernel_CR_Matrix(double, double, double, int, int);
CR_Matrix *convert_rc_matrix_to_CR_Matrix(int, int, double **);
void biggest_connected_component_CR_Matrix(int, CR_Matrix *, CR_Matrix *);
int connected_components_CR_Matrix(int, CR_Matrix *, CR_Matrix *);
void my_product_CR_Matrix(CR_Matrix *, CR_Matrix *, CR_Matrix *);
void product_CR_Matrix(CR_Matrix *, CR_Matrix *, CR_Matrix *);
CR_Matrix *product_CR_Matrix(CR_Matrix *, CR_Matrix *);
CR_Matrix *product3_CR_Matrix(CR_Matrix *, CR_Matrix *, CR_Matrix *);
int inverse_CR_Matrix(CR_Matrix *, CR_Matrix *);
CR_Matrix *svd_inverse_CR_Matrix(CR_Matrix *);
CR_Matrix *inverse_CR_Matrix(CR_Matrix *);
double determinant_CR_Matrix(CR_Matrix *);
double gaussian_alpha_CR_Matrix(CR_Matrix *);
void square_root_CR_Matrix(CR_Matrix *, CR_Matrix *);
void eig_CR_Matrix(CR_Matrix *, CR_Matrix *, CR_Matrix *);
void svd_CR_Matrix(CR_Matrix *, CR_Matrix *, CR_Matrix *, CR_Matrix *);
void square_svd_CR_Matrix(CR_Matrix *, CR_Matrix *, CR_Matrix *, CR_Matrix *);
CR_Matrix *pseudoinverse_CR_Matrix(CR_Matrix *);
CR_Matrix *scale_CR_Matrix(double, CR_Matrix *);
void sum_CR_Matrix(CR_Matrix *, CR_Matrix *, CR_Matrix *);
CR_Matrix *sum_CR_Matrix(CR_Matrix *, CR_Matrix *);
void sum_in_place_CR_Matrix(CR_Matrix *, CR_Matrix *);
CR_Matrix *sum3_CR_Matrix(CR_Matrix *, CR_Matrix *, CR_Matrix *);
CR_Matrix *difference_CR_Matrix(CR_Matrix *, CR_Matrix *);

void frprmn_CR_Matrix(int, CR_Vector *, double, int *, double *, double (*)(CR_Vector *), void (*)(CR_Vector *, CR_Vector *));
void powell_CR_Matrix(CR_Vector *, CR_Matrix *, double, int *, double *, double (*)(CR_Vector *));
void linmin_CR_Vector(CR_Vector *, CR_Vector *, double *, double (*)(CR_Vector *));
void mnbrak(double *, double *, double *, double *, double *, double *, double (*)(double));
double f1dim_CR_Vector(double);
double brent(double, double, double, double (*)(double), double, double *);

CR_Matrix *make_CR_Matrix(char *, int, int);
CR_Matrix *read_CR_Matrix(char *);
CR_Matrix *read_dlm_CR_Matrix(char *, int, int);
void read_dlm_CR_Matrix(char *, CR_Matrix *);
CR_Matrix *init_CR_Matrix(char *, int, int, ...);
CR_Matrix *init_diagonal_CR_Matrix(char *, int, ...);
CR_Matrix *make_diagonal_all_CR_Matrix(char *, int, double);
void free_CR_Matrix(CR_Matrix *);
void set_CR_Matrix(CR_Matrix *, ...);
void set_all_CR_Matrix(double, CR_Matrix *);
CR_Matrix *make_all_CR_Matrix(char *, int, int, double);
void print_CR_Matrix(CR_Matrix *);
void CM_print_CR_Matrix(CR_Matrix *);
void print_as_CR_Matrix(char *, double *, int, int);
void fprint_CR_Matrix(FILE *, CR_Matrix *);
CR_Matrix *copy_CR_Matrix(CR_Matrix *);
void copy_CR_Matrix(CR_Matrix *, CR_Matrix *);
CR_Matrix *copy_header_CR_Matrix(CR_Matrix *);
void transpose_CR_Matrix(CR_Matrix *, CR_Matrix *);
CR_Matrix *transpose_CR_Matrix(CR_Matrix *);
int point_in_poly_CR_Matrix(double, double, CR_Matrix *);
void write_dlm_CR_Matrix(char *, CR_Matrix *);
void write_dlm_CR_Matrix(FILE *, CR_Matrix *);
void read_dlm_CR_Matrix(char *, CR_Matrix *, int);
void write_svmlight_CR_Matrix(char *, CR_Matrix *, int, int);

//-------------------------------------------------
// Level 2 BLAS and LAPACK
//-------------------------------------------------

void vectorize_CR_Matrix(CR_Matrix *, CR_Vector *);
void quadratic_surface_fit_CR_Matrix(CR_Vector *, CR_Vector *, CR_Vector *, CR_Vector *);
CR_Vector *polyfit_CR_Matrix(int, CR_Vector *, CR_Vector *);
void product_sum_CR_Matrix(double, CR_Matrix *, CR_Vector *, double, CR_Vector *, CR_Vector *);
void matrix_vector_product_CR_Matrix(CR_Matrix *, CR_Vector *, CR_Vector *);
void my_product_sum_CR_Matrix(double, CR_Matrix *, CR_Vector *, double, CR_Vector *, CR_Vector *);
double mahalanobis_CR_Matrix(CR_Vector *, CR_Vector *, CR_Matrix *);
double mahalanobis_3x3_CR_Matrix(double *, CR_Vector *, CR_Matrix *);
double gaussian_CR_Matrix(double, CR_Vector *, CR_Vector *, CR_Matrix *);
void sample_gaussian_CR_Matrix(double, CR_Vector *, CR_Vector *, CR_Matrix *);
void mean_and_covariance_nx3_CR_Matrix(CR_Vector *, CR_Vector *, CR_Vector *, CR_Vector *, CR_Matrix *);

//-------------------------------------------------
// Level 1 BLAS and VML
//-------------------------------------------------

void scale_CR_Vector(double, CR_Vector *);
void divide_CR_Vector(CR_Vector *, CR_Vector *, CR_Vector *);
void tanh_CR_Vector(CR_Vector *, CR_Vector *);
void log_CR_Vector(CR_Vector *, CR_Vector *);
void log2_CR_Vector(CR_Vector *, CR_Vector *);
void sqrt_CR_Vector(CR_Vector *, CR_Vector *);
void pow_CR_Vector(double, CR_Vector *, CR_Vector *);
void pow2_CR_Vector(double, CR_Vector *, CR_Vector *);
double magnitude_CR_Vector(CR_Vector *);
double dotproduct_CR_Vector(CR_Vector *, CR_Vector *);
double sum_of_terms_CR_Vector(CR_Vector *);
void difference_CR_Vector(CR_Vector *, CR_Vector *, CR_Vector *);
void sum_CR_Vector(CR_Vector *, CR_Vector *, CR_Vector *);
int max_index_CR_Vector(CR_Vector *);
int min_index_CR_Vector(CR_Vector *);
void sum_CR_Vector_array(CR_Vector **, int, CR_Vector *);

CR_Vector *make_CR_Vector(char *, int rows);
CR_Vector *init_CR_Vector(char *, int rows, ...);
void free_CR_Vector(CR_Vector *);
void set_CR_Vector(CR_Vector *, ...);
void set_all_CR_Vector(double, CR_Vector *);
CR_Vector *make_all_CR_Vector(char *, int, double);
void print_CR_Vector(CR_Vector *);
void print_as_CR_Vector(char *, double *, int);
void fprint_CR_Vector(FILE *, CR_Vector *);
CR_Vector *read_dlm_CR_Vector(char *, int);
void write_dlm_CR_Vector(char *, CR_Vector *);
void read_dlm_CR_Vector(char *, CR_Vector *);
CR_Vector *copy_CR_Vector(CR_Vector *);
void copy_CR_Vector(CR_Vector *, CR_Vector *);
CR_Vector *copy_header_CR_Vector(CR_Vector *);
void fread_CR_Vector(FILE *, CR_Vector *);
void write_excel_CR_Vector(char *, CR_Vector *);
void randperm_CR_Vector(CR_Vector *);
double median_CR_Vector(CR_Vector *);
void sort_CR_Vector(CR_Vector *);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif
