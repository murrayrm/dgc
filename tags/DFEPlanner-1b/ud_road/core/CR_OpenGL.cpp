//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_OpenGL.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void initialize_opengl()
{
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  
  // disable depth testing, fog, stenciling, scissoring, pixel scaling, dithering and biasing, etc.
  // for faster drawpixel() calls
}

//----------------------------------------------------------------------------

// draw binary matrix (1's as white, 0's as black) with upper-left corner at (x, y)
// (remember that origin is at lower-left corner of image in OpenGL)

void ogl_draw_scaled_CR_Matrix(int scale, int x, int y, int h, CR_Matrix *A)
{
  double *x_white, *y_white, *x_black, *y_black;
  int i, j, whites, blacks, which_white, which_black;

  for (j = 0, whites = blacks = 0; j < A->rows; j++)
    for (i = 0; i < A->cols; i++) {
      if (MATRC(A, j, i))
	whites++;
      else
	blacks++;
    }

  x_white = (double *) CR_calloc(whites, sizeof(double));
  y_white = (double *) CR_calloc(whites, sizeof(double));
  x_black = (double *) CR_calloc(blacks, sizeof(double));
  y_black = (double *) CR_calloc(blacks, sizeof(double));

  for (j = 0, which_white = which_black = 0; j < A->rows; j++)
    for (i = 0; i < A->cols; i++) {
      if (MATRC(A, j, i)) {
	x_white[which_white] = (double) (scale * i + x);
	y_white[which_white] = (double) (h - scale * j - y);
	which_white++;
      }	
      else {
	x_black[which_black] = (double) (scale * i + x);
	y_black[which_black] = (double) (h - scale * j - y);
	which_black++;
      }
    }

  // draw

  ogl_draw_points_d(x_white, y_white, scale, whites, 255, 255, 255);
  ogl_draw_points_d(x_black, y_black, scale, blacks, 0, 0, 0);

  // clean up

  CR_free_calloc(x_white, whites, sizeof(double));
  CR_free_calloc(y_white, whites, sizeof(double));
  CR_free_calloc(x_black, blacks, sizeof(double));
  CR_free_calloc(y_black, blacks, sizeof(double));
}

//----------------------------------------------------------------------------

// draw binary matrix (1's as white, 0's as black) with upper-left corner at (x, y)
// (remember that origin is at lower-left corner of image in OpenGL)

void ogl_draw_CR_Matrix(int x, int y, int h, CR_Matrix *A)
{
  double *x_white, *y_white, *x_black, *y_black;
  int i, j, whites, blacks, which_white, which_black;

  for (j = 0, whites = blacks = 0; j < A->rows; j++)
    for (i = 0; i < A->cols; i++) {
      if (MATRC(A, j, i))
	whites++;
      else
	blacks++;
    }

  x_white = (double *) CR_calloc(whites, sizeof(double));
  y_white = (double *) CR_calloc(whites, sizeof(double));
  x_black = (double *) CR_calloc(blacks, sizeof(double));
  y_black = (double *) CR_calloc(blacks, sizeof(double));

  for (j = 0, which_white = which_black = 0; j < A->rows; j++)
    for (i = 0; i < A->cols; i++) {
      if (MATRC(A, j, i)) {
	x_white[which_white] = (double) (i + x);
	y_white[which_white] = (double) (h - j - y);
	which_white++;
      }	
      else {
	x_black[which_black] = (double) (i + x);
	y_black[which_black] = (double) (h - j - y);
	which_black++;
      }
    }

  // draw

  ogl_draw_points_d(x_white, y_white, 1, whites, 255, 255, 255);
  ogl_draw_points_d(x_black, y_black, 1, blacks, 0, 0, 0);

  // clean up

  CR_free_calloc(x_white, whites, sizeof(double));
  CR_free_calloc(y_white, whites, sizeof(double));
  CR_free_calloc(x_black, blacks, sizeof(double));
  CR_free_calloc(y_black, blacks, sizeof(double));
}

//----------------------------------------------------------------------------

// user-specified colors 

// draw binary matrix with upper-left corner at (x, y)
// (remember that origin is at lower-left corner of image in OpenGL)

void ogl_draw_CR_Matrix(int x, int y, int h, CR_Matrix *A, int r_white, int g_white, int b_white, int r_black, int g_black, int b_black)
{
  double *x_white, *y_white, *x_black, *y_black;
  int i, j, whites, blacks, which_white, which_black;

  for (j = 0, whites = blacks = 0; j < A->rows; j++)
    for (i = 0; i < A->cols; i++) {
      if (MATRC(A, j, i))
	whites++;
      else
	blacks++;
    }

  x_white = (double *) CR_calloc(whites, sizeof(double));
  y_white = (double *) CR_calloc(whites, sizeof(double));
  x_black = (double *) CR_calloc(blacks, sizeof(double));
  y_black = (double *) CR_calloc(blacks, sizeof(double));

  for (j = 0, which_white = which_black = 0; j < A->rows; j++)
    for (i = 0; i < A->cols; i++) {
      if (MATRC(A, j, i)) {
	x_white[which_white] = (double) (i + x);
	y_white[which_white] = (double) (h - j - y);
	which_white++;
      }	
      else {
	x_black[which_black] = (double) (i + x);
	y_black[which_black] = (double) (h - j - y);
	which_black++;
      }
    }

  // draw

  ogl_draw_points_d(x_white, y_white, 1, whites, r_white, g_white, b_white);
  ogl_draw_points_d(x_black, y_black, 1, blacks, r_black, g_black, b_black);

  // clean up

  CR_free_calloc(x_white, whites, sizeof(double));
  CR_free_calloc(y_white, whites, sizeof(double));
  CR_free_calloc(x_black, blacks, sizeof(double));
  CR_free_calloc(y_black, blacks, sizeof(double));
}

//----------------------------------------------------------------------------

void ogl_draw_cross(int x, int y, int scale, int r, int g, int b)
{
  // use nearest even number for scale
  if (scale % 2)
    scale = scale + 1;
  glPushMatrix();
  glColor3ub(r, g, b);
  glBegin(GL_LINES);
  glVertex2i(x - scale / 2, y);
  glVertex2i(x + scale / 2, y);
  glVertex2i(x, y - scale / 2);
  glVertex2i(x, y + scale / 2);
  glEnd();
  glPopMatrix();
}

//----------------------------------------------------------------------------

// draw a single line segment

void ogl_draw_line(int x1, int y1, int x2, int y2, int r, int g, int b)
{
  glPushMatrix();
  glColor3ub(r, g, b);
  glBegin(GL_LINES);
  glVertex2i(x1, y1);
  glVertex2i(x2, y2);
  glEnd();
  glPopMatrix();
}

//----------------------------------------------------------------------------

// draw a single line segment

void ogl_draw_thick_line(int x1, int y1, int x2, int y2, int width, int r, int g, int b)
{
  glPushMatrix();
  glLineWidth(width);  // 2 for reproduction
  glColor3ub(r, g, b);
  glBegin(GL_LINES);
  glVertex2i(x1, y1);
  glVertex2i(x2, y2);
  glEnd();
  glLineWidth(width);  // 2 for reproduction
  glPopMatrix();
}

//----------------------------------------------------------------------------

void ogl_draw_point(int x, int y, int size, int r, int g, int b)
{
  glPushMatrix();
  glColor3ub(r, g, b);
  glPointSize(size);
  glBegin(GL_POINTS);
  glVertex2i(x, y);
  glEnd();
  glPopMatrix();
}

//----------------------------------------------------------------------------

// draw a list of points as a set of unconnected points

void ogl_draw_intensity_points(int *x, int *y, int *intensity, int n, int size, double alpha)
{
  int i;
  float fintensity;

  glPushMatrix();
  //  glColor3ub(r, g, b);
  glPointSize(size);
  glBegin(GL_POINTS);
  for (i = 0; i < n; i++) {
    fintensity = intensity[i] / 255;
    glColor4f(fintensity, fintensity, fintensity, alpha);
    //    glColor4f(0, 0, fintensity, alpha);
    glVertex2i(x[i], y[i]);
  }
  glEnd();
  glPopMatrix();
}

//----------------------------------------------------------------------------

// draw a list of points as a set of unconnected dots

void ogl_draw_points_d(double *x, double *y, int size, int n, int r, int g, int b)
{
  int i;

  glPushMatrix();
  glColor3ub(r, g, b);
  glPointSize(size);
  glBegin(GL_POINTS);
  for (i = 0; i < n; i++)
    glVertex2d(x[i], y[i]);
  glEnd();
  glPopMatrix();
}

//----------------------------------------------------------------------------

// draw a list of points as a set of connected line segments

void ogl_draw_line_strip(int *x, int *y, int n, int r, int g, int b)
{
  int i;

  glPushMatrix();
  glColor3ub(r, g, b);
  glBegin(GL_LINE_STRIP);
  for (i = 0; i < n; i++)
    glVertex2i(x[i], y[i]);
  glEnd();
  glPopMatrix();
}

//----------------------------------------------------------------------------

void ogl_draw_parametric_quadratic(double ax, double bx, double cx, double ay, double by, double cy, double t_start, double t_end, double x, double y, double theta, int num_steps, int r, int g, int b)
{
  int i;
  double t, t_delta, c, c_delta;
  double *xp, *yp;

  t_delta = (t_end - t_start) / (double) (num_steps - 1);
  xp = (double *) CR_calloc(num_steps, sizeof(double));
  yp = (double *) CR_calloc(num_steps, sizeof(double));

  printf("ax = %lf, bx = %lf, cx = %lf\n", ax, bx, cx);
  printf("ay = %lf, by = %lf, cy = %lf\n", ay, by, cy);

  for (i = 0, t = t_start; i < num_steps; i++, t += t_delta) {
    xp[i] = x + ax * t * t + bx * t + cx;
    yp[i] = y + ay * t * t + by * t + cy;
    printf("t = %lf, x = %lf, y = %lf\n", t, xp[i], yp[i]);
  }

  ogl_draw_line_strip_d(xp, yp, num_steps, r, g, b);

  CR_free_calloc(xp, num_steps, sizeof(double));
  CR_free_calloc(yp, num_steps, sizeof(double));

}

//----------------------------------------------------------------------------

// clothoid integration,  (4th order Runga Kutta)
// This is a single step integration and should be good if the angle doesn't
// change a lot, say less than 90 deg.
// Returns:  void
void
clothoidRk( double distAlongClothoid,
             double thStart,
             double c0,
             double c1,
             double *x,
             double *y)
{
   double s;
   double hh,h6;
   double th1,th23,th4;  // angle at start, mid, and end of step

   hh = distAlongClothoid / 2;
   h6 = distAlongClothoid / 6;

   th1  = thStart;
   s = hh;
   th23 = thStart + c0 * s + c1 * s * s / 2;
   s += hh;
   th4  = thStart + c0 * s + c1 * s * s / 2;

   // integrate and store in next pos[]
   *x = h6*(cos(th1) + 4*cos(th23) + cos(th4));
   *y = h6*(sin(th1) + 4*sin(th23) + sin(th4));
}

// c = pi * t / a

void ogl_draw_clothoid2(double c0, double c1, double s_start, double s_end, double theta, double x, double y, int num_steps, int r, int g, int b)
{
  int i;
  double t, t_delta, c, c_delta, a;
  double *xp, *yp;
  double t_start, t_end;

  t_start = 0;
  t_end = s_end;
  t_delta = (t_end - t_start) / (double) num_steps;

  xp = (double *) CR_calloc(num_steps, sizeof(double));
  yp = (double *) CR_calloc(num_steps, sizeof(double));

  for (i = 0, t = t_start; i < num_steps; i++, t += t_delta) {

    clothoidRk(t, theta, c0, c1, &(xp[i]), &(yp[i])); 
    xp[i] += x;
    yp[i] += y;

    printf("i = %i, xp = %lf, yp = %lf\n", i, xp[i], yp[i]);
  }

  ogl_draw_line_strip_d(xp, yp, num_steps, r, g, b);

  CR_free_calloc(xp, num_steps, sizeof(double));
  CR_free_calloc(yp, num_steps, sizeof(double));  
}
//----------------------------------------------------------------------------

// c = pi * t / a
/*
void ogl_draw_clothoid(double c0, double c1, double s_start, double s_end, double theta, double x, double y, int num_steps, int r, int g, int b)
{
  int i;
  double t, t_delta, c, c_delta, a;
  double *xp, *yp;
  double t_start, t_end;

  //  t_start = c0 / PI;
  //  t_end = c1 / PI;

  t_start = s_start;
  t_end = s_end;

  c_delta = (c1 - c0) / (double) num_steps;
  t_delta = (t_end - t_start) / (double) num_steps;
  xp = (double *) CR_calloc(num_steps, sizeof(double));
  yp = (double *) CR_calloc(num_steps, sizeof(double));

  a = 300.0;
  for (i = 0, t = t_start, c = c0; i < num_steps; i++, t += t_delta, c += c_delta) {
    //    a = PI * t / c;
    xp[i] = x + a * fresnel_c_integral(t);
    yp[i] = y + a * fresnel_s_integral(t);
    //    a += 0.5 * t_delta;
    //   printf("t = %lf, a = %lf, x = %lf, y = %lf\n", t, a, xp[i], yp[i]);
  }

  ogl_draw_line_strip_d(xp, yp, num_steps, r, g, b);

  CR_free_calloc(xp, num_steps, sizeof(double));
  CR_free_calloc(yp, num_steps, sizeof(double));  
}

//----------------------------------------------------------------------------

// dickmanns approximation

void ogl_draw_approx_clothoid(double c0, double c1, double t_start, double t_end, double x, double y, int num_steps, int r, int g, int b)
{
  int i;
  double t, t_delta, delta_theta, l;
  double *xp, *yp;

  l = t_end - t_start;
  delta_theta = c0 * l + c1 * l * l / 2.0;
 
  printf("delta heading = %lf\n", delta_theta);
  
  t_delta = (t_end - t_start) / (double) num_steps;
  xp = (double *) CR_calloc(num_steps, sizeof(double));
  yp = (double *) CR_calloc(num_steps, sizeof(double));

  for (i = 0, t = t_start; i < num_steps; i++, t += t_delta) {
    xp[i] = x + c0 * t * t / 2.0 + c1 * t * t * t / 6.0;
    yp[i] = y + t;
  }

  ogl_draw_line_strip_d(xp, yp, num_steps, r, g, b);

  CR_free_calloc(xp, num_steps, sizeof(double));
  CR_free_calloc(yp, num_steps, sizeof(double));
  
}

//----------------------------------------------------------------------------

void ogl_draw_fresnel_spiral(double a, double t_start, double t_end, int num_steps, double x, double y, int r, int g, int b)
{
  int i;
  double t, t_delta;
  double *xp, *yp;

  t_delta = (t_end - t_start) / (double) num_steps;
  xp = (double *) CR_calloc(num_steps, sizeof(double));
  yp = (double *) CR_calloc(num_steps, sizeof(double));

  for (i = 0, t = t_start; i < num_steps; i++, t += t_delta) {
    xp[i] = x + a * fresnel_c_integral(t);
    yp[i] = y + a * fresnel_s_integral(t);
  }

  ogl_draw_line_strip_d(xp, yp, num_steps, r, g, b);

  CR_free_calloc(xp, num_steps, sizeof(double));
  CR_free_calloc(yp, num_steps, sizeof(double));
}
*/

//----------------------------------------------------------------------------

// draw a list of points as a set of lines (1 & 2 points connected, 3 & 4, 5 & 6, etc.)

void ogl_draw_lines_d(double *x, double *y, int n, int r, int g, int b)
{
  int i;

  glPushMatrix();
  glLineWidth(1);   // 2 for reproduction
  glColor3ub(r, g, b);
  glBegin(GL_LINES);
  for (i = 0; i < n; i++)
    glVertex2d(x[i], y[i]);
  glEnd();
  glPopMatrix();
}

//----------------------------------------------------------------------------

// double version
// draw a list of points as a set of connected line segments

void ogl_draw_thick_line_strip_d(double *x, double *y, int n, int w, int r, int g, int b)
{
  glPushMatrix();
  glLineWidth(w);  // 2 for reproduction
  ogl_draw_line_strip_d(x, y, n, r, g, b);
  glLineWidth(1);   // 2 for reproduction
  glPopMatrix();
}

//----------------------------------------------------------------------------

// double version
// draw a list of points as a set of connected line segments

void ogl_draw_thick_line_loop_d(double *x, double *y, int n, int w, int r, int g, int b)
{
  glPushMatrix();
  glLineWidth(w);  // 2 for reproduction
  ogl_draw_line_loop_d(x, y, n, r, g, b);
  glLineWidth(1);   // 2 for reproduction
  glPopMatrix();
}

//----------------------------------------------------------------------------

// double version
// draw a list of points as a set of connected line segments

void ogl_draw_line_strip_d(double *x, double *y, int n, int r, int g, int b)
{
  int i;

  glPushMatrix();
  glColor3ub(r, g, b);
  glBegin(GL_LINE_STRIP);
  for (i = 0; i < n; i++)
    glVertex2d(x[i], y[i]);
  glEnd();
  glPopMatrix();
}

//----------------------------------------------------------------------------

// double version
// draw a list of points as a set of connected line segments, connecting
// first and last as well

void ogl_draw_line_loop_d(double *x, double *y, int n, int r, int g, int b)
{
  int i;

  glPushMatrix();
  glColor3ub(r, g, b);
  glBegin(GL_LINE_LOOP);
  for (i = 0; i < n; i++)
    glVertex2d(x[i], y[i]);
  glEnd();
  glPopMatrix();
}

//----------------------------------------------------------------------------

// corners specified in clockwise order

void ogl_draw_quadrilateral(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int r, int g, int b)
{
  glPushMatrix();
  glColor3ub(r, g, b);
  glBegin(GL_LINE_LOOP);
  glVertex2i(x1, y1);
  glVertex2i(x2, y2);
  glVertex2i(x3, y3);
  glVertex2i(x4, y4);
  glEnd();
  glPopMatrix();
}

//----------------------------------------------------------------------------

void ogl_draw_rectangle(int x1, int y1, int x2, int y2, int r, int g, int b)
{
  glPushMatrix();
  //  glLineWidth(1);   // 2 for reproduction
  glColor3ub(r, g, b);
  glBegin(GL_LINE_LOOP);
  glVertex2i(x1, y1);
  glVertex2i(x2, y1);
  glVertex2i(x2, y2);
  glVertex2i(x1, y2);
  glEnd();
  glPopMatrix();
}

//----------------------------------------------------------------------------

void ogl_dummy()
{
  //  glPushMatrix();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, glutGet(GLUT_WINDOW_WIDTH), 
	  0, glutGet(GLUT_WINDOW_HEIGHT), -1, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  //  glColor3ub(0, 0, 0);

//   glRasterPos2i(x+1, y-1);
//   for(p = s, lines = 0; *p; p++) {
//     if (*p == '\n') {
//       lines++;
//       glRasterPos2i(x+1, y-1-(lines*18));
//     }
//     //    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *p);
//     glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *p);
//   }
//   glColor3ub(r, g, b);
//   glRasterPos2i(x, y);
//   for(p = s, lines = 0; *p; p++) {
//     if (*p == '\n') {
//       lines++;
//       glRasterPos2i(x, y-(lines*18));
//     }
//     //    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *p);
//     glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *p);
//   }
// //   glMatrixMode(GL_PROJECTION);
// //   glPopMatrix();
// //   glMatrixMode(GL_MODELVIEW);
//   glRasterPos2i(f[0], f[1]);
//   CR_free_calloc(f, 4, sizeof(float));

  //  glPopMatrix();
}

//----------------------------------------------------------------------------

// text: draws a string of text with an 12 point helvetica bitmap font
// at position (x,y) in window space (bottom left corner is (0,0)

void ogl_draw_string2(char *s, int x, int y, int r, int g, int b) 
{
  int lines;
  char* p;
  float *f;

  f = (float *) CR_calloc(4, sizeof(float));
  glGetFloatv(GL_CURRENT_RASTER_POSITION, f);
  //  printf("%f, %f\n", f[0], f[1]);

//   glDisable(GL_LIGHTING);
//   glDisable(GL_LIGHT0);
// 
//   glDisable(GL_DEPTH_TEST);
//   //  glDisable(GL_FOG);
  glPushMatrix();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, glutGet(GLUT_WINDOW_WIDTH), 
	  0, glutGet(GLUT_WINDOW_HEIGHT), -1, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glColor3ub(0, 0, 0);
  glRasterPos2i(x+1, y-1);
  for(p = s, lines = 0; *p; p++) {
    if (*p == '\n') {
      lines++;
      glRasterPos2i(x+1, y-1-(lines*18));
    }
    //    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *p);
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *p);
  }
  glColor3ub(r, g, b);
  glRasterPos2i(x, y);
  for(p = s, lines = 0; *p; p++) {
    if (*p == '\n') {
      lines++;
      glRasterPos2i(x, y-(lines*18));
    }
    //    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *p);
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *p);
  }
//   glMatrixMode(GL_PROJECTION);
//   glPopMatrix();
//   glMatrixMode(GL_MODELVIEW);
  glRasterPos2i(f[0], f[1]);
  CR_free_calloc(f, 4, sizeof(float));
  glPopMatrix();
  //  glEnable(GL_FOG);
//   glEnable(GL_DEPTH_TEST);
// 
//   glEnable(GL_LIGHTING);
//   glEnable(GL_LIGHT0);
// 
}

//----------------------------------------------------------------------------

// text: draws a string of text with an 12 point helvetica bitmap font
// at position (x,y) in window space (bottom left corner is (0,0)

void ogl_draw_string(char *s, int x, int y, int r, int g, int b) 
{
  int lines;
  char* p;
  
//   glDisable(GL_LIGHTING);
//   glDisable(GL_LIGHT0);
// 
//   glDisable(GL_DEPTH_TEST);
//   //  glDisable(GL_FOG);
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glOrtho(0, glutGet(GLUT_WINDOW_WIDTH), 
	  0, glutGet(GLUT_WINDOW_HEIGHT), -1, 1);
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();
  glColor3ub(0, 0, 0);
  glRasterPos2i(x+1, y-1);
  for(p = s, lines = 0; *p; p++) {
    if (*p == '\n') {
      lines++;
      glRasterPos2i(x+1, y-1-(lines*18));
    }
    //    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *p);
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *p);
  }
  glColor3ub(r, g, b);
  glRasterPos2i(x, y);
  for(p = s, lines = 0; *p; p++) {
    if (*p == '\n') {
      lines++;
      glRasterPos2i(x, y-(lines*18));
    }
    //    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *p);
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *p);
  }
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
  //  glEnable(GL_FOG);
//   glEnable(GL_DEPTH_TEST);
// 
//   glEnable(GL_LIGHTING);
//   glEnable(GL_LIGHT0);
// 
}

//----------------------------------------------------------------------------

void print_opengl_info()
{
  char *vendor, *renderer, *version, *extensions;
  int i, length;

  vendor = (char *) glGetString(GL_VENDOR);
  renderer = (char *) glGetString(GL_RENDERER);
  version = (char *) glGetString(GL_VERSION);

  printf("----------------------\nOpenGL\n----------------------\n");
  printf("Vendor: %s\nRenderer: %s\nOpenGL version: %s\nExtensions:\n", vendor, renderer, version);

  extensions = (char *) glGetString(GL_EXTENSIONS);
  length = strlen(extensions);
  for (i = 0; i < length; i++)
    if (extensions[i] == ' ')
      printf("\n");
    else
      printf("%c", extensions[i]);
  printf("\n");
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
