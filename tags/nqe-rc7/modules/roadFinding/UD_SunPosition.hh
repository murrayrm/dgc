//----------------------------------------------------------------------------
//  Adapted from AA.ARC v5.5
//
//  This program computes the orbital positions of planetary
//  bodies and performs rigorous coordinate reductions to apparent
//  geocentric and topocentric place (local altitude and azimuth).
//
//  Copyright 1984, 1987 by Stephen L. Moshier
//  www.moshier.net
//
//  used by Road follower module; this file is a concatenation of AA200.C, 
//  KFILES.C, sun.c, and various utility function .c files that support
//  computation of sun position.  there are some non-algorithmic edits 
//  because aa is not being used as a standalone program
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//----------------------------------------------------------------------------

#ifndef UD_SUNPOSITION_DECS

//----------------------------------------------------------------------------

#define UD_SUNPOSITION_DECS

/* General definitions for aa program. */

#include <stdio.h>
#include <stdlib.h>

#include <math.h>
#include <time.h>

#define WGS_84_EQUATORIAL_RADIUS        6378137
#define WGS_84_SQUARE_OF_ECCENTRICITY	0.00669438

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

//#include "UD_Aerial.hh"

void sunpos(double, double, int, int, int, int, int, float *, float *);   // time supplied (log file version)
void sunpos(double, double, int, float *, float *);                       // no time supplied (skynet version)

void s1_compute(double, double, int, float, int, int, float, float, float *, float *); 

/* Constants for the JPL DE403 Chebyshev Ephemeris.  */

#define DE102F 0
#define DE200F 0
#define DE245F 0
#define DE400F 0
#define DE403F 0
#define DE404F 1

// globals defined here put into UD_SunPosition.cpp to avoid multiple
// definitions

/* Indicate the endian-ness of your computer's float format.
   IBM PC is little endian, 68K and SPARC are big endian.  */
#define LITTLEEND 1
#define BIGEND 0
/* If you have to swap ends to read the ephemeris file,
   then define ENDIANNESS nonzero.  For example, the files deNNN.unx
   from JPL are bigendian and must be byte reversed for Intel format. */
#define ENDIANNESS 1
/* Define 1 if ephemeris tape is DEC PDP-11/VAX float format,
   but your computer has IEEE float format.  */
#define DECDATA 0

/* Describe your software system.
   See kepjpl.c and kfiles.c for system-dependent I/O. */
/* Microsoft C */
#ifdef _MSC_VER
#define IBMPC 1
#if _MSC_VER > 1000
#include <stdlib.h>
#endif
#endif
/* Unix, DJGPP, GNU */
#ifdef unix
#define UNIX 1
#endif

#ifdef __GNUC__
#ifndef UNIX
#define UNIX 1
#endif
#endif

/* Please send patches for other systems.  */

/* DEBUG = 1 to enable miscellaneous printouts. */
#define DEBUG 0

/* Interpret the configuration as defined in compiler command line.
   Don't change these; do it in the makefile.  */
#ifdef DE200
#undef DE200
#define DE200 1
#else
#define DE200 0
#endif

#ifdef DE200CD
#undef DE200CD
#define DE200CD 1
#else
#define DE200CD 0
#endif

#ifdef DE102
#undef DE102
#define DE102 1
#else
#define DE102 0
#endif

#ifdef DE245
#undef DE245
#define DE245 1
#else
#define DE245 0
#endif

#ifdef DE400
#undef DE400
#define DE400 1
#else
#define DE400 0
#endif

#ifdef DE403
#undef DE403
#define DE403 1
#else
#define DE403 0
#endif

#ifdef LIB403
#undef LIB403
#define LIB403 1
#undef DE403
#define DE403 1
#else
#define LIB403 0
#endif

#ifdef DE404
#undef DE404
#define DE404 1
#else
#define DE404 0
#endif

#define DE405

#ifdef DE405
#undef DE405
#define DE405 1
#else
#define DE405 0
#endif

/* DE406 CD published by Willman-Bell */
#ifdef DE406CD
#undef DE406CD
#define DE406CD 1
#else
#define DE406CD 0
#endif

/* DE406 files from JPL's ftp archive site.  */
#ifdef DE406
#undef DE406
#define DE406 1
#else
#define DE406 0
#endif

#ifdef SSYSTEM
#undef SSYSTEM
#define SSYSTEM 1
#else
#define SSYSTEM 0
#endif


struct orbit
	{
	char obname[16]; /* name of the object */
	double epoch;	/* epoch of orbital elements */
	double i;	/* inclination	*/
	double W;	/* longitude of the ascending node */
	double w;	/* argument of the perihelion */
	double a;	/* mean distance (semimajor axis) */
	double dm;	/* daily motion */
	double ecc;	/* eccentricity */
	double M;	/* mean anomaly */
	double equinox;	/* epoch of equinox and ecliptic */
	double mag;	/* visual magnitude at 1AU from earth and sun */
	double sdiam;	/* equatorial semidiameter at 1au, arc seconds */
/* The following used by perterbation formulas: */
	int (*oelmnt )(); /* address of program to compute elements */
	int (*celmnt )(); /* program to correct longitude and radius */
	double L;	/* computed mean longitude */
	double r;	/* computed radius vector */
	double plat;	/* perturbation in ecliptic latitude */
	};

struct star
	{
	char obname[32];	/* Object name (31 chars) */
	double epoch;		/* Epoch of coordinates */
	double ra;		/* Right Ascension, radians */
	double dec;		/* Declination, radians */
	double px;		/* Parallax, radians */
	double mura;		/* proper motion in R.A., rad/century */
	double mudec;		/* proper motion in Dec., rad/century */
	double v;		/* radial velocity, km/s */
	double equinox;		/* Epoch of equinox and ecliptic */
	double mag;		/* visual magnitude */
	};
/* Note the items for a star are in different measurement units
 * in the ASCII file description.
 */

/* aa.c */

#ifndef PI
#define PI      3.14159265358979323846
#endif

extern double DTR;
extern double RTD;
extern double RTS;
extern double STR;
//extern double PI;
extern double J2000;
extern double B1950;
extern double J1900;
extern double Caud;
extern double Rearthau;
extern double JD;
extern double TDT;
extern double UT;
extern double dradt, ddecdt;
extern int objnum, jdflag, prtflg;
extern double obpolar[];
extern double eapolar[];
extern double rearth[];
extern double dp[];
/* angles.c */
extern double SE, SO, EO, pq, ep, qe;
/* nutate.c */
extern double jdnut, nutl, nuto;
/* epsiln.c */
extern double jdeps, eps, coseps, sineps;
/* vearth.c */
extern double jvearth, vearth[];
/* dms.c */
double mod360(), modtp();

/* OFDATE = 1 for equinox of date in Meeus planetary
 * orbit perturbation formulas.
 * OFDATE = 0 for equinox J2000.
 */
#define OFDATE 0

//#define SSYSTEM  1

#define POLYN 1
#define DEPOLYN (DE102 | DE200 | DE200CD | DE245 | DE400 | DE403 | DE404 | DE405 | DE406 | DE406CD | SSYSTEM)

#if __STDC__
extern int altaz ( double pol[3], double J );
extern int angles ( double p[], double q[], double e[] );
extern int annuab ( double p[] );
extern int iter_trnsit ( int (*func)() );
extern double caltoj ( long year, int month, double day );
extern int cvtdec ( unsigned short *x );
extern int deltap ( double p0[], double p1[], double *dr, double *dd );
extern double deltat ( double Y );
extern int diurab ( double last, double *ra, double *dec );
extern int diurpx ( double last, double *ra, double *dec, double dist );
extern int dms ( double x );
extern int domoon ( void );
extern int doscsi ( unsigned char *cmdb, unsigned char *datbuf );
extern int dosun ( void );
extern int dostar ( void );
extern int doplanet ( void );
extern int epsiln ( double J );
extern FILE *fincat ( char *name, int n, char *str1, char *str2 );
extern int fk4fk5 ( double p[], double m[], struct star *el );
extern double zgetdate ( void );
extern double gethms ( void );
extern int getnum ( char *msg, void *num, char *format );
extern int getorbit ( struct orbit *el );
extern int getstar ( struct star *el );
extern int hms ( double x );
extern int jpl ( double JD, int objnum, double p[], double v[] );
extern int jtocal ( double J );
extern int kepjpl ( double J, struct orbit *e, double rect[], double polar[] );
extern int kepler0 (double J, struct orbit *e, double rect[], double polar[]);
extern int kepler ( double J, struct orbit *e, double rect[], double polar[] );
extern int kinit ( void );
extern int lightt ( struct orbit *elemnt, double q[], double e[] );
extern int lonlat ( double pp[], double J, double polar[], int ofdate );
extern double mod360 ( double x );
extern double modtp ( double x );
extern int moonll ( double J, double pp[] );
extern int nutate ( double J, double p[] );
extern int nutlo ( double J );
extern int oparams ( double re[], double rdote[], double J, double JDE, int objnum, struct orbit *orb );
extern int precess ( double R[], double J, int direction );
extern int printorb ( struct orbit *o );
extern void prvec ( double vec[] );
extern int reduce ( struct orbit *elemnt, double q[], double e[] );
extern double refrac ( double alt );
extern int relativity ( double p[], double q[], double e[] );
extern int reselh ( void );
extern int revbytes ( void );
extern int rstar ( struct star *el );
extern long sccomm ( long code, long block, long count );
extern long scrset ( long x );
extern int showcname ( char *in );
extern int showcor ( char *strng, double p[], double dp[] );
extern int showrd ( char *msg, double p[], double pol[] );
extern double sidrlt ( double j, double tlong );
extern int sread ( char *q, int k );
extern int sscc ( int k, double arg, int n );
extern int swrite ( char *q, int k );
extern double tdb ( double JED );
extern int trnsit ( double J, double lha, double dec );
extern int update ( void );
extern int velearth ( double J );
extern int watnreq ( void );
extern int watphas ( void );
extern int watreq ( void );
extern double zatan2 ( double x, double y );
extern int zopen ( char *name, int a, int b );
extern int zread ( int fd, char *buf, int bct );
extern int zseek ( int fd, unsigned long bcnt, int a );
extern double acos (double);
extern double asin (double);
extern double atan (double);
extern double cos (double);
extern double sin (double);
extern double tan (double);
extern double sqrt (double);
extern double fabs (double);
extern double log (double);
extern double floor (double);
extern char *whatconstel (double *, double);
extern void EtoM( double e[], double M[] );
extern void MtoE( double M[], double e[] );
extern void midentity ( double M[] );
extern void mtransp ( double A[], double B[] );
extern void mmpy3 ( double A[], double B[], double C[] );
extern void mrotx (double M[], double theta );
extern void mroty (double M[], double theta );
extern void mrotz (double M[], double theta );
extern int aa_main( void );
extern void aa_init( long , int, double, int, int, double, int);
extern void aa_update(double, double, int, double, double);
extern void UTMtoLL(double, double, int, double *,  double *);

//#include "protos.h"
#define ANSIPROT
#else
int showrd(), showcor(), dms(), hms(), jtocal(), epsiln();
int fk4fk5(), kepler(), kepjpl(), kinit(), getnum(), deltap();
int lonlat(), nutate(), precess(), reduce(), rstar();
int lightt(), velearth(), diurpx(), diurab(), update();
int relativity(), showcname(), annuab(), angles(), altaz();
int dosun(), doplanet(), dostar(), iter_trnsit();
double mod360(), modtp();
int domoon(), jpl(), cvtdec();
int getstar(), getorbit(), trnsit();
double sidrlt(), refrac();
void prvec();
double tdb(), zgetdate(), gethms();
double acos(), asin(), atan(), zatan2(), cos(), sin();
double tan(), sqrt(), fabs(), log(), floor(), polevl();
char *whatconstel();
double zgetdate(), gethms();
int update(), getnum(), getstar(), getorbit(), showcname(), kepler();
int dosun(), iter_trnsit(), domoon(), dostar(), doplanet(), jpl();
void EtoM(), MtoE(), midentity(), mtransp(), mmpy3();
void mrotx(), mroty(), mrotz();
int aa_main();
void aa_init();
void aa_update();
void UTMtoLL();
#endif

/* ASCII ephemeris output file. */
extern FILE *ephfile;
extern int ephprint;      

#endif
