//#include "fusionMapperTabSpecs.hh"
#include "fusionMapper.hh"

using namespace sc_interface;

extern double sparrowTimeLockFeeder[MAX_INPUT_ELEV_LAYERS];
extern double sparrowTimeLockFusionMapper;

FusionMapper::FusionMapper(int skynetKey, FusionMapperOptions mapperOptsArg, int waitForState)
  : CSkynetContainer(SNfusionmapper, skynetKey, &(this->_status)),
		//CModuleTabClient(&m_input, &m_output),
    CStateClient(waitForState),
		CSuperConClient("FMP")
{
	_status = 10;
  printf("Welcome to fusionmapper init0\n");
  _QUIT = 0;
  _PAUSE = 1;

  mapperOpts = mapperOptsArg;

  // static allocated for memory efficiency, if larger than 100 pts, change this
  m_pRoadReceive = new double[MAX_ROAD_SIZE * 3 + 1];
  m_pRoadX = new double[MAX_ROAD_SIZE];
  m_pRoadY = new double[MAX_ROAD_SIZE];
  m_pRoadW = new double[MAX_ROAD_SIZE];
  m_pRoadPoints = 0;

  fusionMap.initMap(CONFIG_FILE_DEFAULT_MAP);
  roadMap.initMap(CONFIG_FILE_DEFAULT_MAP);
	swapMapA.initMap(CONFIG_FILE_DEFAULT_MAP);
	swapMapB.initMap(CONFIG_FILE_DEFAULT_MAP);

	//	lowResMap.initMap(CONFIG_FILE_LOWRES_MAP);

  layerID_costFinal      = fusionMap.addLayer<double>(CONFIG_FILE_COST,            true);
  layerID_corridor  = fusionMap.addLayer<double>(CONFIG_FILE_RDDF, true);
	//	layerID_corridor_lowres = lowResMap.addLayer<double>(CONFIG_FILE_RDDF);
  layerID_road      = roadMap.addLayer<double>(CONFIG_FILE_ROAD);
  layerID_roadCounter = roadMap.addLayer<double>(CONFIG_FILE_ROAD_COUNTER);
  layerID_superCon = fusionMap.addLayer<double>(CONFIG_FILE_SUPERCON, false);
  costFuser = new CCostFuser(&fusionMap, layerID_costFinal, &fusionMap, layerID_corridor, 
														 &swapMapA, &swapMapB,
														 &costFuserOpts, &_corridorOptsFinalCost);
	costFuser->addLayerRoad(&roadMap, layerID_road);
	costFuser->addLayerSuperCon(&fusionMap, layerID_superCon);

	//	defaultTerrainPainter = new CTerrainCostPainter(&fusionMap, layerID_corridor, costFuser);


	//	lowResTerrainPainter = new CTerrainCostPainter(&lowResMap, layerID_corridor_lowres, costFuser);

	layerArray[0].msgTypeElev = SNladarRoofDeltaMap;
	layerArray[0].msgTypeCost = SNladarRoofDeltaMapCost;
	layerArray[0].relWeight = 1.0;
	sprintf(layerArray[0].name, "LADAR (Roof)");
	sprintf(layerArray[0].optionsFilename, "config/fusionMapper/opts.fusionMapper.ladar.roof");

	layerArray[1].msgTypeElev = SNladarSmallDeltaMap;
	layerArray[1].msgTypeCost = SNladarSmallDeltaMapCost;
	layerArray[1].relWeight = 0.1;
	sprintf(layerArray[1].name, "LADAR (Small)");
	sprintf(layerArray[1].optionsFilename, "config/fusionMapper/opts.fusionMapper.ladar.small");

	layerArray[2].msgTypeElev = SNladarRieglDeltaMap;
	layerArray[2].msgTypeCost = SNladarRieglDeltaMapCost;
	layerArray[2].relWeight = 0.01;
	sprintf(layerArray[2].name, "LADAR (Riegl)");
	sprintf(layerArray[2].optionsFilename, "config/fusionMapper/opts.fusionMapper.ladar.riegl");

	layerArray[3].msgTypeElev = SNladarBumperDeltaMap;
	layerArray[3].msgTypeCost = SNladarBumperDeltaMapCost;
	layerArray[2].relWeight = 0.0;
	sprintf(layerArray[3].name, "LADAR (Bumper)");
	sprintf(layerArray[3].optionsFilename, "config/fusionMapper/opts.fusionMapper.ladar.bumper");	

	layerArray[4].msgTypeElev = SNladarFrontDeltaMap;
	layerArray[4].msgTypeCost = SNladarFrontDeltaMapCost;
	layerArray[2].relWeight = 0.5;
	sprintf(layerArray[4].name, "LADAR (Front)");
	sprintf(layerArray[4].optionsFilename, "config/fusionMapper/opts.fusionMapper.ladar.front");	


	layerArray[5].msgTypeElev = SNstereoShortDeltaMap;
	layerArray[5].msgTypeCost = SNstereoShortDeltaMapCost;
	layerArray[5].relWeight = 1.0;
	sprintf(layerArray[5].name, "Stereo (Short)");
	sprintf(layerArray[5].optionsFilename, "config/fusionMapper/opts.fusionMapper.stereo.short");
	

	layerArray[6].msgTypeElev = SNstereoLongDeltaMap;
	layerArray[6].msgTypeCost = SNstereoLongDeltaMapCost;
	layerArray[6].relWeight = 1.0;
	sprintf(layerArray[6].name, "Stereo (Long)");
	sprintf(layerArray[6].optionsFilename, "config/fusionMapper/opts.fusionMapper.stereo.long");


	
	for(int i=0; i < NUM_ELEV_LAYERS; i++) {
		/*		if(i != 2) {
			layerArray[i].map = &fusionMap;
			layerArray[i].terrainPainter = defaultTerrainPainter;
		} else {
			layerArray[i].map = &lowResMap;
			layerArray[i].terrainPainter = lowResTerrainPainter;
			}*/
		layerArray[i].sendCost = true;
		if(i!=3) {
			layerArray[i].enabled = true;
		} else {
			layerArray[i].enabled = false;
		}
		layerArray[i].map = new CMapPlus();
		if(i != 2){
			layerArray[i].map->initMap(CONFIG_FILE_DEFAULT_MAP);
		}
		else{
			layerArray[i].map->initMap(CONFIG_FILE_LOWRES_MAP);
		}
		DGCcreateMutex(&layerArray[i].shiftMutex);
		//layerArray[i].corridorLayerNum = (layerArray[i].map)->addLayer<double>(CONFIG_FILE_RDDF, true);
		layerArray[i].elevLayerNum = (layerArray[i].map)->addLayer<CElevationFuser>(CONFIG_FILE_LADAR_FUSED_ELEV);
		layerArray[i].terrainPainter = new CTerrainCostPainter( layerArray[i].map , costFuser);

		layerArray[i].terrainPainterIndex = (layerArray[i].terrainPainter)->addElevLayer(layerArray[i].elevLayerNum, 
																																					 &layerArray[i].options, layerArray[i].sendCost);
		layerArray[i].costLayerNum = (layerArray[i].terrainPainter)->_costLayerNums[layerArray[i].terrainPainterIndex];
		layerArray[i].costPainterIndex = costFuser->addLayerElevCost(layerArray[i].map, layerArray[i].costLayerNum,
																																 layerArray[i].relWeight,
																																 layerArray[i].map, layerArray[i].elevLayerNum);

		layerArray[i].corridorPainter = new CCorridorPainter(layerArray[i].map, &(fusionRDDF));
		layerArray[i].corridorPainter->addLayer(layerArray[i].corridorLayerNum, &_corridorOptsReference, 1);


	}

  // layerID_superCon = fusionMap.addLayer<int>(0, -1, false);

  fusionCorridorPainter = new CCorridorPainter(&(fusionMap), 
																					 &(fusionRDDF));
	fusionCorridorPainter->addLayer(layerID_costFinal, &_corridorOptsFinalCost, 1);
	fusionCorridorPainter->addLayer(layerID_corridor, &_corridorOptsReference, 1);

	//lowResCorridorPainter = new CCorridorPainter(&(lowResMap), &(fusionRDDF));
	//lowResCorridorPainter->addLayer(layerID_corridor_lowres, &_corridorOptsReference, 1);

	costFuserOpts.maxSpeed = 10.0; //defaultTerrainPainterOpts.maxSpeed = 10.0;
	costFuserOpts.maxCellsToInterpolate = 1;
	costFuserOpts.minNumSupportingCellsForInterpolation=5;
	costFuserOpts.paintRoadWithoutElevation = 1;

 	_corridorOptsFinalCost.optRDDFscaling = 1.0;
 	_corridorOptsFinalCost.optMaxSpeed = -2.0;

 	_corridorOptsReference.optRDDFscaling = 1.0;
 	_corridorOptsReference.optMaxSpeed = 15.0;
	DGCcreateMutex(&roadMutex);

	DGCcreateRWLock(&allMapsRWLock);
 
  usleep(500);

  ReadConfigFile();
}


FusionMapper::~FusionMapper() {
  //FIX ME

  printf("Thank you for using the fusionmapper destructor\n");
}


void FusionMapper::ActiveLoop() {
  //TODO - Give this a sparrow/sngui display
  printf("Entering FusionMapper active-loop\n");
  printf("Press Ctrl+C to quit\n");
  //cout << "Active loop concurrency " << pthread_getconcurrency() << endl;
  //fusionCostPainter.setMapperOpts(&mapperOpts);
	int socket_num = m_skynet.get_send_sock(SNfusiondeltamap);
	int socketMapDeltaCorridor = m_skynet.get_send_sock(SNdeltaMapCorridor);
 	int mapDeltaSocketCost[6];
	for(int i=0; i < NUM_ELEV_LAYERS; i++)
		mapDeltaSocketCost[i] = m_skynet.get_send_sock(layerArray[i].msgTypeCost);

  NEcoord exposedRow[4], exposedCol[4];
	//NEcoord exposedRowLowRes[4], exposedColLowRes[4];
 
	NEcoord frontLeftAliceCorner, frontRightAliceCorner, rearLeftAliceCorner, rearRightAliceCorner;

	bool firstPainting = true;

	_status = 0;
	while(!_QUIT) {
		int deltaSize = 0;

		WaitForNewState();
		_infoFusionMapper.startProcessTimer();

		if(!_PAUSE) { 
			DGCreadlockRWLock(&allMapsRWLock);
		  fusionMap.updateVehicleLoc(m_state.Northing, m_state.Easting);
		
		
			if(fusionMap.getExposedRowBoxUTM(exposedRow) ||
				 fusionMap.getExposedColBoxUTM(exposedCol)) {
				fusionMap.getExposedColBoxUTM(exposedCol);

			DGClockMutex(&roadMutex);
			roadMap.updateVehicleLoc(m_state.Northing, m_state.Easting);
			for(int i=0; i < NUM_ELEV_LAYERS; i++) {
			  DGClockMutex(&layerArray[i].shiftMutex);
			  (layerArray[i].map)->updateVehicleLoc(m_state.Northing, m_state.Easting);
				DGCunlockMutex(&layerArray[i].shiftMutex);
			};
		  _infoFusionMapper.endLockTimer();

				//first calculate the stddev on our position
// 				double posUncertainty = hypot(m_state.NorthConf, m_state.EastConf);
// 				if(fusionRDDF.getExtraWidth()/1.2 > ceil(posUncertainty/fusionMap.getResRows())*fusionMap.getResRows()) {
// 					fusionRDDF.setExtraWidth(ceil(posUncertainty/fusionMap.getResRows())*fusionMap.getResRows());
// 					//start shrinking
// 				} else if(fusionRDDF.getExtraWidth()*1.2 < floor(posUncertainty/fusionMap.getResRows())*fusionMap.getResRows()) {
// 					fusionRDDF.setExtraWidth(floor(posUncertainty/fusionMap.getResRows())*fusionMap.getResRows());
// 					fusionCorridorReference.repaintAll();
// 					fusionCorridorPainter.repaintAll();
// 					//fusionCostPainter.repaintAll(1);
// 				}				
				
				// 				frontLeftAliceCorner = NEcoord(m_state.Northing + DIST_FRONT_AXLE_TO_FRONT*cos(m_state.Yaw) + VEHICLE_WIDTH/2.0*sin(m_state.Yaw),
				// 																			 m_state.Easting + DIST_FRONT_AXLE_TO_FRONT*sin(m_state.Yaw) - VEHICLE_WIDTH/2.0*cos(m_state.Yaw));
				// 				frontRightAliceCorner = NEcoord(m_state.Northing + DIST_FRONT_AXLE_TO_FRONT*cos(m_state.Yaw) - VEHICLE_WIDTH/2.0*sin(m_state.Yaw),
				// 																				m_state.Easting + DIST_FRONT_AXLE_TO_FRONT*sin(m_state.Yaw) + VEHICLE_WIDTH/2.0*cos(m_state.Yaw));
				// 				rearLeftAliceCorner = NEcoord(m_state.Northing + (DIST_FRONT_AXLE_TO_FRONT - VEHICLE_LENGTH)*cos(m_state.Yaw) + VEHICLE_WIDTH/2.0*sin(m_state.Yaw),
				// 																			m_state.Easting + (DIST_FRONT_AXLE_TO_FRONT - VEHICLE_LENGTH)*sin(m_state.Yaw) - VEHICLE_WIDTH/2.0*cos(m_state.Yaw));
				// 				rearRightAliceCorner = NEcoord(m_state.Northing + (DIST_FRONT_AXLE_TO_FRONT - VEHICLE_LENGTH)*cos(m_state.Yaw) - VEHICLE_WIDTH/2.0*sin(m_state.Yaw),
				// 																			 m_state.Easting + (DIST_FRONT_AXLE_TO_FRONT - VEHICLE_LENGTH)*sin(m_state.Yaw) + VEHICLE_WIDTH/2.0*cos(m_state.Yaw));

				// 				//Check to see if Alice is inside the corridor - if not
				// 				if(!fusionRDDF.isPointInCorridor(frontLeftAliceCorner) ||
				// 					 !fusionRDDF.isPointInCorridor(frontRightAliceCorner) ||
				// 					 !fusionRDDF.isPointInCorridor(rearLeftAliceCorner) ||
				// 					 !fusionRDDF.isPointInCorridor(rearRightAliceCorner)) {
				// 					//Then we need to expand the corridor where we are now and repaint the whole map
				// 					int largestWayptNum = fusionRDDF.findWaypointWithLargestWidth(NEcoord(m_state.Northing, m_state.Easting));
				// 					double currentOffset = fusionRDDF.getOffset(largestWayptNum) - fusionRDDF.getExtraWidth();
				// 					double distanceToTrackline = fmax(fmax(fusionRDDF.getDistToTrackline(frontLeftAliceCorner),
				// 																								 fusionRDDF.getDistToTrackline(frontRightAliceCorner)),
				// 																						fmax(fusionRDDF.getDistToTrackline(rearLeftAliceCorner),
				// 																								 fusionRDDF.getDistToTrackline(rearRightAliceCorner)));
				// 					double widthToAdd = distanceToTrackline - currentOffset;
				// 					fusionRDDF.setExtraWidth(ceil(widthToAdd/fusionMap.getResRows())*fusionMap.getResRows());
				// 					//cout << "expanding to " << ceil(widthToAdd/fusionMap.getResRows())*fusionMap.getResRows() << endl;
				// 					fusionCorridorPainter.repaintAll();
				// 					fusionSampledCorridorPainter.repaintAll();
				// 					if(!mapperOpts.optJeremy) {
				// 						//mapperOpts.optRDDFScaling,mapperOpts.optMaxSpeed);
				// 						fusionCostPainter.repaintAll(1);
				// 					} else {
				// 						fusionCostPainter.uberRepaintAll(1);
				// 					}
				// 				} else if(fusionRDDF.getExtraWidth()!=0.0) {
				// 					//We're inside the RDDF, but it may be too big - see if we should shrink it
				// 					int largestWayptNum = fusionRDDF.findWaypointWithLargestWidth(NEcoord(m_state.Northing, m_state.Easting));
				// 					double currentOffset = fusionRDDF.getOffset(largestWayptNum) - fusionRDDF.getExtraWidth();
				// 					double distanceToTrackline = fmax(fmax(fusionRDDF.getDistToTrackline(frontLeftAliceCorner),
				// 																								 fusionRDDF.getDistToTrackline(frontRightAliceCorner)),
				// 																						fmax(fusionRDDF.getDistToTrackline(rearLeftAliceCorner),
				// 																								 fusionRDDF.getDistToTrackline(rearRightAliceCorner)));
				// 					double widthToAdd = distanceToTrackline - currentOffset;
				// 					if(fusionRDDF.getExtraWidth() > ceil(widthToAdd/fusionMap.getResRows())*fusionMap.getResRows()) {
				// 						fusionRDDF.setExtraWidth(fmax(ceil(widthToAdd/fusionMap.getResRows())*fusionMap.getResRows(), 0.0));
				// 						//cout << "Shrinking to " << fmax(ceil(widthToAdd/fusionMap.getResRows())*fusionMap.getResRows(), 0.0) << endl;
				// 					} 
				// 					fusionCorridorPainter.paintChanges(exposedRow, exposedCol);
				// 					fusionCorridorReference.paintChanges(exposedRow, exposedCol);
				// 				} else {
				// 					fusionCorridorPainter.paintChanges(exposedRow, exposedCol);
				// 					fusionCorridorReference.paintChanges(exposedRow, exposedCol);
				// 				}
				// 			}      
				fusionCorridorPainter->paintChanges(exposedRow, exposedCol);
				if(firstPainting) {
					fusionCorridorPainter->paintData(NEcoord(m_state.Northing, m_state.Easting), m_state.Yaw,
																					 -1*_corridorOptsFinalCost.optMaxSpeed);
					firstPainting = false;
				}

				//	lowResCorridorPainter->paintChanges(exposedRowLowRes, exposedColLowRes);
// 				for(int i=0; i<4; i++) {
// 					if(readOptsCtr%40 == 0)
// 						defaultTerrainPainter->readOptionsFromFile(layerArray[i].terrainPainterIndex, layerArray[i].optionsFilename);
// 					defaultTerrainPainter->paintChanges(layerArray[i].terrainPainterIndex,
// 																							layerArray[i].costPainterIndex);
// 					if(layerArray[i].sendCost) {
// 						CDeltaList* deltaList = fusionMap.serializeDelta<double>(layerArray[i].costLayerNum, 0);
// 						if(!deltaList->isShiftOnly()) {
// 							SendMapdelta(mapDeltaSocketCost[i], deltaList);
// 							fusionMap.resetDelta<double>(layerArray[i].costLayerNum);
// 						}
// 					}
// 				}
			}
			_infoFusionMapper.endStage1Timer();
			sparrowTimeLockFusionMapper = (costFuser->fuseChangesCost(NEcoord(m_state.Northing, m_state.Easting)))/1000.0;
			_infoFusionMapper.endStage2Timer();
				
			unsigned long long timestamp;
			DGCgettime(timestamp);
	
			
			CDeltaList* deltaList = NULL;
						     
			deltaList = fusionMap.serializeDelta<double>(layerID_costFinal, timestamp);	
			if(!deltaList->isShiftOnly()) {
				SendMapdelta(socket_num, deltaList);
				deltaSize = 0;
				for(int i=0; i<deltaList->numDeltas; i++) {
					deltaSize += deltaList->deltaSizes[i];
				}
				_infoFusionMapper.endProcessTimer(deltaSize);	
				fusionMap.resetDelta<double>(layerID_costFinal);
			}

			deltaList = fusionMap.serializeDelta<double>(layerID_corridor, timestamp);	
			if(!deltaList->isShiftOnly() && mapperOpts.sendRDDF == 1) {
			  SendMapdelta(socketMapDeltaCorridor, deltaList);
			  fusionMap.resetDelta<double>(layerID_corridor);
			}

				
			DGCunlockMutex(&roadMutex);
			DGCunlockRWLock(&allMapsRWLock);
		} else {
			DGCusleep(50000);
		}
	}

	printf("%s [%d]: Active loop quitting\n", __FILE__, __LINE__);
}


void FusionMapper::ReceiveDataThread_ElevFeeder(void* pArg) {

	int index = (int)pArg;
	//cout << "elev feeder "<<  index << " concurrency is " << pthread_getconcurrency() << endl ;
	//NEcoord exposedRow[4], exposedCol[4];

  int mapDeltaSocketElev = m_skynet.listen(layerArray[index].msgTypeElev, ALLMODULES);
 	int mapDeltaSocketCost = m_skynet.get_send_sock(layerArray[index].msgTypeCost);
  double UTMNorthing;
  double UTMEasting;

  CElevationFuser::STATUS overwrittenStatus = CElevationFuser::OK;

	unsigned long long timeOptsLastRead = 0;
	unsigned long long tempTimestamp;
	unsigned long long diffTimestamp;

	char* pMapDelta = new char[MAX_DELTA_SIZE];
  while(!_QUIT) { 
    if(!_PAUSE) {
      int numreceived = m_skynet.get_msg(mapDeltaSocketElev, pMapDelta, MAX_DELTA_SIZE, 0);
      layerArray[index].statusInfo.startProcessTimer();
      if(layerArray[index].enabled) {
				if(numreceived>0) {
					DGCreadlockRWLock(&allMapsRWLock);
				  DGClockMutex(&layerArray[index].shiftMutex);
					layerArray[index].statusInfo.endLockTimer();

					int numCells = layerArray[index].map->getDeltaSize(pMapDelta);
					for(int i=0; i<numCells; i++) {
					  CElevationFuser receivedData = layerArray[index].map->getDeltaVal<CElevationFuser>(i, layerArray[index].elevLayerNum, pMapDelta, &UTMNorthing, &UTMEasting);
						CElevationFuser previousData = layerArray[index].map->getDataUTM<CElevationFuser>(layerArray[index].elevLayerNum, UTMNorthing, UTMEasting);
						overwrittenStatus = previousData.fuse_MeanElevation(receivedData);
						layerArray[index].map->setDataUTM<CElevationFuser>(layerArray[index].elevLayerNum, UTMNorthing, UTMEasting, receivedData);
					  layerArray[index].terrainPainter->markChanges(layerArray[index].terrainPainterIndex, UTMNorthing, UTMEasting, overwrittenStatus);
					}
					layerArray[index].statusInfo.endStage1Timer();

					sparrowTimeLockFeeder[index] = layerArray[index].terrainPainter->paintChanges(layerArray[index].terrainPainterIndex,
																																												layerArray[index].costPainterIndex);
					DGCgettime(tempTimestamp);
					diffTimestamp = tempTimestamp - timeOptsLastRead;
					if(DGCtimetosec(diffTimestamp, true) > 1.0) {
						timeOptsLastRead = tempTimestamp;
						layerArray[index].terrainPainter->readOptionsFromFile(layerArray[index].terrainPainterIndex, layerArray[index].optionsFilename);
						costFuser->_relWeights[layerArray[index].costPainterIndex] = layerArray[index].terrainPainter->_options[layerArray[index].terrainPainterIndex]->relativeWeight;
					}
// 					counter++;
					layerArray[index].statusInfo.endStage2Timer();
					if(layerArray[index].sendCost) {
						CDeltaList* deltaList = (layerArray[index].map)->serializeDelta<double>(layerArray[index].costLayerNum, 0);
						if(!deltaList->isShiftOnly()) {
							SendMapdelta(mapDeltaSocketCost, deltaList);
							(layerArray[index].map)->resetDelta<double>(layerArray[index].costLayerNum);
						}
					}
					layerArray[index].statusInfo.endProcessTimer(numreceived);
					DGCunlockMutex(&layerArray[index].shiftMutex);
					DGCunlockRWLock(&allMapsRWLock);
				}
			}
		}	 else {
			DGCusleep(50000);
		}
  }
  delete pMapDelta; 
}

void FusionMapper::ReceiveDataThread_Road()
{
  int road_socket = m_skynet.listen(SNroad2map, SNroadfinding);

  bool first_road=true;
  int cur_road=0;
  Road road[2];
  CRoadPainter painter;

  printf("RoadThread started\n");

  int bytes=0;
  int cnt=0;

  while(!_QUIT) {
    //Read road data over skynet, make sure we always get the last transfer
    //so that we don't build up the buffert
    int received;
    do {
      received = m_skynet.get_msg(road_socket, &road[cur_road], sizeof(Road), 0);
      if(received < road[cur_road].data_size()) {
				printf("Error receiving road data\n");
				continue;
      }
      
      bytes +=received;
      ++cnt;
      //printf("Received %i messages, %i bytes, %i road points\n", cnt, bytes, road[cur_road].length);
    } while(m_skynet.is_msg(road_socket));
    
    if(received < road[cur_road].data_size())   //Invalid road data, get next package
      continue;
    
    _infoRoad.startProcessTimer();
    if(mapperOpts.optRoad) {      
      //Paint the trajectory into the map
			DGCreadlockRWLock(&allMapsRWLock);
      DGClockMutex(&roadMutex);
      //myCMap.clearLayer(layerNum[SNGUI_LAYER_ROAD]);
			_infoRoad.endLockTimer();
			_infoRoad.endStage1Timer();

			DGCreadlockRWLock(&costFuser->_swapLock);
      if(!first_road) {
				painter.paint(road[cur_road], road[1-cur_road], &roadMap, layerID_road, layerID_roadCounter, costFuser);
      } else {
				painter.paint(road[cur_road], &roadMap, layerID_road, layerID_roadCounter, costFuser);
      }
			DGCunlockRWLock(&costFuser->_swapLock);

      first_road=false;
      cur_road = 1-cur_road;
      
      DGCunlockMutex(&roadMutex);
			DGCunlockRWLock(&allMapsRWLock);
			_infoRoad.endStage2Timer();
      _infoRoad.endProcessTimer(received);
    }
  }
}

void FusionMapper::ReceiveDataThread_Supercon() {
  int mapDeltaSocket = m_skynet.listen(SNsuperConMapDelta, MODsupercon);
  double UTMNorthing;
  double UTMEasting;
  NEcoord point;
  double cellval;

  m_pSuperconMapDelta = new char[MAX_DELTA_SIZE];
  while(!_QUIT) { 
    if(!_PAUSE) {
      _infoSupercon.startProcessTimer();
      int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pSuperconMapDelta, MAX_DELTA_SIZE, 0);
      if(mapperOpts.optSupercon) {
				if(numreceived>0) {
				  // Begin cutting here?
					int cellsize = fusionMap.getDeltaSize(m_pSuperconMapDelta);
					DGCreadlockRWLock(&allMapsRWLock);
					DGCreadlockRWLock(&costFuser->_swapLock);					
					_infoSupercon.endLockTimer();
					_infoSupercon.endStage1Timer();
					for(int i=0; i<cellsize; i++) {
						cellval = fusionMap.getDeltaVal<double>(i, layerID_superCon, m_pSuperconMapDelta, &UTMNorthing, &UTMEasting);
						fusionMap.setDataUTM<double>(layerID_superCon, UTMNorthing, UTMEasting, cellval);
						point.N = UTMNorthing;
						point.E = UTMEasting;
						costFuser->markChangesCost(point);
						//fusionCostPainter.fuseChanges(UTMNorthing, UTMEasting  );
					}
					DGCunlockRWLock(&costFuser->_swapLock);
					DGCunlockRWLock(&allMapsRWLock);
					_infoSupercon.endStage2Timer();
					_infoSupercon.endProcessTimer(numreceived);
				}
      }
    }
    usleep(100);
  }
  delete m_pSuperconMapDelta; 
}


void FusionMapper::ReceiveDataThread_SuperconActions() {
	int mapActionSocket = m_skynet.listen(SNsuperConMapAction, MODsupercon);
	int socket_num = m_skynet.get_send_sock(SNfusiondeltamap);
	CDeltaList* deltaList;
	superCon_map_action scCommand;
	while(!_QUIT) {
		int numBytesRcvd = m_skynet.get_msg(mapActionSocket, &scCommand, sizeof(scCommand), 0);
		if(mapperOpts.optSupercon) {
			if(numBytesRcvd > 0) {
				switch(scCommand.mapActionCmd) {
				case sc_interface::broaden_rddf_one_map:
					DGCwritelockRWLock(&allMapsRWLock);
					fusionRDDF.setExtraWidth(scCommand.dblArg);
					fusionCorridorPainter->repaintAll();
					for(int i=0; i < NUM_ELEV_LAYERS; i++) {
						layerArray[i].map->clearLayer(layerArray[i].costLayerNum);
						layerArray[i].terrainPainter->readOptionsFromFile(layerArray[i].terrainPainterIndex, layerArray[i].optionsFilename);
						layerArray[i].terrainPainter->repaintAll(layerArray[i].terrainPainterIndex,
																										 layerArray[i].costPainterIndex);
					}
					costFuser->markChangesCostAll();
					costFuser->fuseChangesCost(NEcoord(m_state.Northing, m_state.Easting));										
					fusionRDDF.setExtraWidth(0.0);
					DGCunlockRWLock(&allMapsRWLock);					
					scMessage( int(broaden_rddf_complete) );  //generic RDDF broaden confirmation
					break;
				case sc_interface::clear_all_maps_everywhere:
					DGCwritelockRWLock(&allMapsRWLock);
					costFuser->clearBuffers();
					fusionMap.clearMap();
					roadMap.clearMap();
					for(int i=0; i < NUM_ELEV_LAYERS; i++) {
						layerArray[i].map->clearMap();
					}
					costFuser->clearBuffers();
					fusionMap.clearMap();
					roadMap.clearMap();
					for(int i=0; i < NUM_ELEV_LAYERS; i++) {
						layerArray[i].map->clearMap();
					}
					fusionCorridorPainter->repaintAll();
// 					costFuser->markChangesCostAll();
// 					costFuser->fuseChangesCost(NEcoord(m_state.Northing, m_state.Easting));					
					fusionCorridorPainter->paintData(NEcoord(m_state.Northing, m_state.Easting), 
																					 m_state.Yaw, _corridorOptsFinalCost.optMaxSpeed);
					unsigned long long timestamp;
					DGCgettime(timestamp);	
			
					deltaList = NULL;
						     
					deltaList = fusionMap.serializeFullMapDelta<double>(layerID_costFinal, timestamp);	
					if(!deltaList->isShiftOnly()) {
					  SendMapdelta(socket_num, deltaList);
					  fusionMap.resetDelta<double>(layerID_costFinal);
					}

					DGCunlockRWLock(&allMapsRWLock);					
					scMessage( int(clear_maps_complete) ); //generic clear-maps confirmation
					cout << "MAPS CLEARED! - (at superCon's request)" << endl;
					break;
				default:
					break;
				}
			}
		}
	}
}


void FusionMapper::PlannerListenerThread() { 
	int mapDeltaSocket = m_skynet.listen(SNfullmaprequest, SNplanner);
	int socket_num = m_skynet.get_send_sock(SNfusiondeltamap);
	bool bRequestMap;
	while(!_QUIT)
		{
			int numreceived =  m_skynet.get_msg(mapDeltaSocket, &bRequestMap, MAX_DELTA_SIZE, 0);
			//cout << "Received "  << numreceived << " Messages"  << endl; 
			if(numreceived>0) {
				
				if(bRequestMap == true) {
					unsigned long long timestamp;
					DGCgettime(timestamp);
					DGCwritelockRWLock(&allMapsRWLock);
					CDeltaList* deltaList = fusionMap.serializeFullMapDelta<double>(layerID_costFinal, timestamp);

					if(!SendMapdelta(socket_num, deltaList))
						cerr << "FusionMapper::PlannerListenerThread(): couldn't send delta" << endl;
					else
						fusionMap.resetDelta<double>(layerID_costFinal);

					DGCunlockRWLock(&allMapsRWLock);
				} 
				
			}
		}
}


void FusionMapper::ReceiveDataThread_EStop() {
	//int oldStatus = -1;
	_PAUSE=true;
	while(!_QUIT) {
		WaitForNewActuatorState();
		/*
		if(m_actuatorState.m_about_to_unpause == true && oldStatus != 2) {
			DGClockMutex(&m_mapMutex);
			fusionMap.clearMap();
			fusionCorridorPainter->repaintAll();
			//fusionCorridorReference.repaintAll();
			//fusionSampledCorridorPainter.repaintAll();
			//fusionCostPainter.repaintAll(1);
			DGCunlockMutex(&m_mapMutex);
		}
		oldStatus = m_actuatorState.m_estoppos;
		*/
		
		if(!_PAINTWHILEPAUSED){

		if(m_actuatorState.m_about_to_unpause == true || 
			 (m_actuatorState.m_estoppos == 2 && m_state.Speed2() > 0.1)){		
		    _PAUSE = false;
		  }
		  else{
		    _PAUSE = true;
		  }
		}
		else{
		  _PAUSE = false;
		}
	}
}


void FusionMapper::ReadConfigFile() {
  FILE* configFile = NULL;
  char fieldBuffer[512];
  char fieldName[256];
  char fieldValue[256];

  configFile = fopen(mapperOpts.configFilename, "r");
  if(configFile != NULL) {
    while(!feof(configFile)) {
      fgets(fieldBuffer, 512, configFile);
      if(strncmp(fieldBuffer, "%", 1)!=0) {
				sscanf(fieldBuffer, "%s %s\n", fieldName, fieldValue);
				if(strcmp(fieldName, "Max_Speed:")==0) {
					costFuserOpts.maxSpeed = atof(fieldValue);
					_corridorOptsReference.optMaxSpeed = atof(fieldValue);
				} else if(strcmp(fieldName, "RDDF_Speed_Scaling:")==0) {
					_corridorOptsReference.optRDDFscaling = atof(fieldValue);
					_corridorOptsFinalCost.optRDDFscaling = 1.0;
				} else if(strcmp(fieldName, "Inside_RDDF_No_Data_Speed:")==0) {
					_corridorOptsFinalCost.optMaxSpeed = atof(fieldValue);
				} else if(strcmp(fieldName, "Max_Cells_to_Interp:")==0) {
					costFuserOpts.maxCellsToInterpolate = atoi(fieldValue);
				} else if(strcmp(fieldName, "Min_Cells_for_Interp:")==0) {
					costFuserOpts.minNumSupportingCellsForInterpolation = atoi(fieldValue);
				} else if(strcmp(fieldName, "Paint_Road_Without_Elev:")==0) {
					costFuserOpts.paintRoadWithoutElevation = atoi(fieldValue);
				} else if(strcmp(fieldName, "Paint_While_Paused:")==0) {
					_PAINTWHILEPAUSED = atoi(fieldValue);
				} else {
					printf("%s [%d]: Unknown field '%s' with value '%s' - pausing\n", __FILE__, __LINE__, fieldName, fieldValue);
				}
      }
    }
    fclose(configFile);
  }

}


void FusionMapper::WriteConfigFile() {
  FILE* configFile = NULL;

  configFile = fopen(mapperOpts.configFilename, "w");
  if(configFile != NULL) {
    fprintf(configFile, "%% FusionMapper Aggressiveness Tuning Configuration File\n");
    fprintf(configFile, "RDDF_Speed_Scaling: %lf\n", _corridorOptsReference.optRDDFscaling);
    fprintf(configFile, "Inside_RDDF_No_Data_Speed: %lf\n", _corridorOptsFinalCost.optMaxSpeed);
    fprintf(configFile, "Max_Speed: %lf\n", costFuserOpts.maxSpeed);
		fprintf(configFile, "Max_Cells_to_Interp: %d\n", costFuserOpts.maxCellsToInterpolate);
		fprintf(configFile, "Min_Cells_for_Interp: %d\n", costFuserOpts.minNumSupportingCellsForInterpolation);
		fprintf(configFile, "Paint_Road_Without_Elev: %d\n", costFuserOpts.paintRoadWithoutElevation);
		fprintf(configFile, "Paint_While_Paused: %d\n", _PAINTWHILEPAUSED);
    fclose(configFile);
  }
}
