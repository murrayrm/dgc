/*
 * CTimberTalker - not sure what this does
 *
 * Jason Yosinski, April 2005
 * Commented by Richard, 16 Apr 05
 *
 */

#include "CTimberTalker.hh"
#include "DGCutils"
#include <iostream>

using namespace std;


CTimberTalker::CTimberTalker()
{
  DGCcreateMutex(&talker_debug_level_mutex);

  DGClockMutex(&talker_debug_level_mutex);
  talker_debug_level = DEFAULT_DEBUG;
  if (talker_debug_level > 1)
    cout << "1CTimberTalker::CTimberTalker()" << endl;
  DGCunlockMutex(&talker_debug_level_mutex);

  m_pDataBuffer = new char[MESSAGE_LENGTH];
  sendSocket = m_skynet.get_send_sock(SNtimberstring);

  DGClockMutex(&talker_debug_level_mutex);
  if (talker_debug_level > 1)
    cout << "2CTimberTalker::CTimberTalker()" << endl;
  DGCunlockMutex(&talker_debug_level_mutex);
}



CTimberTalker::~CTimberTalker()
{
  DGCdeleteMutex(&talker_debug_level_mutex);
  delete [] m_pDataBuffer;
}



int CTimberTalker::getTalkerDebugLevel()
{
  DGClockMutex(&talker_debug_level_mutex);
  int ret = talker_debug_level;
  DGCunlockMutex(&talker_debug_level_mutex);
  return ret;
}



void CTimberTalker::setTalkerDebugLevel(int value)
{
  DGClockMutex(&talker_debug_level_mutex);
  talker_debug_level = value;
  DGCunlockMutex(&talker_debug_level_mutex);
}



/** sends messages of type SNtimberstring, a char that's MESSAGE_LENGTH long */
int CTimberTalker::SendTimberString(int stringSocket, char* p_string)
{
  int bytesToSend;
  int bytesSent;
  char* pBuffer = m_pDataBuffer;

#warning "should this be strlen(p_string) + 1?"
  memcpy(pBuffer, p_string, strlen(p_string) + 1);

#warning "should this be strlen(p_string) + 1?"
  bytesToSend = strlen(p_string) + 1;

  //  if (talker_debug_level > 0)
  //  cout << "Send (" << bytesToSend << "b) \"" << m_pDataBuffer << "\"" << endl;

  bytesSent = m_skynet.send_msg(stringSocket, m_pDataBuffer, bytesToSend, 0);

  if(bytesSent != bytesToSend)
    {
      cout << "CTimberTalker::SendTimberFile(): sent " << bytesSent << " bytes while expected to send " << bytesToSend << " bytes" << endl;
      return false;
    }
  
  return true;
}

int CTimberTalker::RecvTimberString(int stringSocket, char* p_string)
{
  int bytesToReceive;
  int bytesReceived;
  char* pBuffer = m_pDataBuffer;

  bytesToReceive = MESSAGE_LENGTH;
  bytesReceived = m_skynet.get_msg(stringSocket, m_pDataBuffer, bytesToReceive, 0);
  if(bytesReceived <= 0)
    {
      cerr << "CTimberTalker::RecvTimberString(): skynet error" << endl;
      return false;
    }

  DGClockMutex(&talker_debug_level_mutex);
  if (talker_debug_level > 1)
    cout << "CTimberTalker: received " << bytesReceived << " bytes" << endl;
  DGCunlockMutex(&talker_debug_level_mutex);
  //  cout << "1 here" << endl;
  memcpy(p_string, pBuffer, bytesReceived);
  // cout << "2 here" << endl;
  return true;
}
