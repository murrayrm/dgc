#ifndef TIMBERTALKER_HH
#define TIMBERTALKER_HH

#include "SkynetContainer.h"
#include "pthread.h"
#include <unistd.h>
#include "TimberConfig.hh"

using namespace std;


class CTimberTalker : virtual public CSkynetContainer
{
public:
  CTimberTalker();
  ~CTimberTalker();

  int getTalkerDebugLevel();
  void setTalkerDebugLevel(int value);

  int SendTimberString(int stringSocket, char* p_string);
  int RecvTimberString(int stringSocket, char* p_string);

  int sendSocket;

private:
  int talker_debug_level;
  pthread_mutex_t talker_debug_level_mutex;

  char* m_pDataBuffer;
};

#endif // TIMBERTALKER_HH
