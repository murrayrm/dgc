#!/bin/bash

# This script creates a ssh keypair and distrubutes the public key to the 
# hosts given as arguments.
#
# Created: Oct 20, 2004
# Author: Henrik Kjellander

if [[ ! "$1" || ! "$2" ]]; then
  echo Usage: $0 username \"hosts separated by spaces\"
  echo Example: $0 team \"titanium osmium\"
  exit 0
fi

USER=$1
HOSTS=$2

cd ~/.ssh
if [[ -f id_rsa && -f id_rsa.pub ]]; then
  echo Existing files found in ~/.ssh already, using them insead of creating new
else
  echo About to create keypair, please select empty passphrase \(press enter\)
  ssh-keygen -t rsa -f id_rsa
  echo Done creating keypair
fi

echo 

for host in $HOSTS
do
  echo Copying the public key to: $host
  cat id_rsa.pub | (ssh $USER@$host "mkdir -p .ssh; touch .ssh/authorized_keys; cat >> .ssh/authorized_keys")
done

echo Done!
exit 0

