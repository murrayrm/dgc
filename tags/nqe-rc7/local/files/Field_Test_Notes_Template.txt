== Summary ==
<!-- Put summary of field test here.  Revisit at end and put summary of what 
/actually/ happened too. -->

<pre>
Field Test Name  : 
Test number/name : 
Date/time        : 
</pre>

== Overview ==
<pre>
Safety Driver              : 
Navigator                  : 
Commander (Left Passenger) : 
Pilot (Right Passenger)    : 

Operator(s) : 
Notetaker   : 

RDDF        : 
SKYNET_KEY  : 
</pre>

== Modules ==
<!-- Include user, machine, path to repository copy, revision, module name and
flags (if any) for EACH module that is run. For example:
astate        : team@skynet1:~/dgc,-r6617
adrive        : team@skynet7:~/lindzey,-r6619 (made commit of adrive.config)
RddfPathGen   : team@skynet7:~/lindzey,-r6617 
TrajFollower  : team@skynet7:~/lindzey,-r6617 
LadarFeeder   : none
-->
<pre>
astate        : 
adrive        : 
RddfPathGen   : 
TrajFollower  : 
ModeMan       : 
FusionMapper  : 
PlannerModule : 
LadarFeeder   : 
StereoFeeder  : 
skynetgui     : 
</pre>

== Post-test check-in ==
<!-- Indicate which logs are being run and ... -->
The test team in the vehicle should perform the following test check-in, and
<s>strike</s> these items as they are completed.

# Post these notes to the Wiki and to /dgc/test_logs/YYYYMMDD_description/ (make sure it is group writeable)
# Sort through and save ALL non-garbage logs in the above directory, with a README explaining what the logs are and what they represent.  Verify that if not 
explained in the README, the logs should have comment headers that indicate the format and units for all columns in the logs, and have standard timestamps 
(seconds since UNIX epoch to six decimal places) as the first column.

== Preparation Notes ==
<!--
* NOTE: can put notes in like this.
* BUG: or bugs like this
<pre>
Tue Mar 22 17:10:05 2005
1111540205 Can use <pre> html tag for pretty printing in Wiki.
</pre>
-->

== Autonomous Operations ==

<pre>
</pre>

=== Example Section Title ===
<pre>
</pre>

=== Second section title ===
<pre>
</pre>
