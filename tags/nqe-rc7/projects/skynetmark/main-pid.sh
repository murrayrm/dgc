#!/bin/zsh
# zsh is needed for floating point arithmetic

MIN_RATE=8192
MAX_RATE=100000000   #100MBs
LTEST_TIME=1         #seconds each test
TTEST_TIME=1         #seconds each test
ECHO="/bin/echo"   #don't use zsh builtin echo  
RESDIR=$HOSTNAME-$2-`date --iso-8601`
RESFILE="result.csv" #output here

if (( $# == 2 ));
then SLAVE=$2; KEY=$1;
else
  $ECHO "usage: main-snm.sh key slave"
  $ECHO "example: main-snm.sh 587 cobalt.caltech.edu"
  $ECHO "make sure skynetd is running on both computers"
  exit
fi

function do1test 
{
  $1 $3 > markpipe &
  slaveppid=$!
  #output should be sent_messages real_rate real_time
  SEND_DATA=`ssh $SLAVE $2 $3 $4 $5 $6`
  wait $slavepid
  RECV_MSG=`cat markpipe`
  rm markpipe
  $ECHO $RECV_MSG $SEND_DATA
}

function tput121
{
  do1test ./single-throughput-slave dgc/projects/skynetmark/single-throughput-master $1 $2 $3 $4  
}

function lat121
{
  do1test ./single-latency-slave dgc/projects/skynetmark/single-latency-master $1 $2 $3 $4
}

function dotput121test
{
  #S = pkt size , D = rate , R = percent received, L = 1-R
  #$ECHO "1->1 throughput test" >> $RESFILE
  rm -f $RESFILE
  rm curve
  $ECHO \# 1-1 throughput with PID. >> $RESFILE
  $ECHO \# $HOSTNAME to $SLAVE >> $RESFILE
  $ECHO \# size rate  loss time n >> $RESFILE  
  D=23000 #first guess
  Lref=.01   
  S=1;
  while (( S<=131072 ));
    do FAILED=0
    E=0;
    N=0;
    L=0;
    Khigh=0.3;
    Klow=.8;
    I=0
    Dnew=$D;
    while true  
      do 
      N=$(( N+1 ))
      int=$(( int + E))
      D=$Dnew
      result=`tput121 $KEY $S $D $TTEST_TIME`
      RECV_MSG=`$ECHO $result | cut -d ' ' -f 1`
      SENT_MSG=`$ECHO $result | cut -d ' ' -f 2`
      SENT_TIME=`$ECHO $result | cut -d ' ' -f 4`  L=$(( 1 - 1.* RECV_MSG / SENT_MSG ))
      L=$(( 1 - 1.0 * RECV_MSG / SENT_MSG ))
      E=$(( Lref - L ))
      if (( L == 0 )) then Dnew=$(( (Khigh+1) * D )); else Dnew=$(( D + D*E*Klow )); fi
      echo $D >> curve
      echo S=$S D=$D  L=$L
      echo
      if (( L > 0 && L < 0.02 || ( L == 0 && SENT_TIME > TTEST_TIME*1.05 ))) then break; fi;
    done
    Dadj=$(( D * TTEST_TIME / SENT_TIME ))
    $ECHO $S  $Dadj  $L $SENT_TIME $N >> $RESFILE
    S=$(( 1.414 * S ))
  done
}

function dolat121test
{
  rm -f $RESFILE
  for (( i=1; i<= 65536; i = 2 * i )) #loop on msg_size
    do $ECHO -n $i ' ' >> $RESFILE
    result=`lat121 $KEY $i $LTEST_TIME`
    $ECHO `$ECHO $result | cut -d ' ' -f 4-6` >> $RESFILE
  done
}

install -m 755 -d $RESDIR

RESFILE=$RESDIR/lat121.dat
dolat121test
RESFILE=$RESDIR/tput121.dat
dotput121test
RESFILE=$RESDIR/tput121local.dat
SLAVE="localhost"
dotput121test 
RESFILE=$RESDIR/lat121local.dat
SLAVE="localhost"
dolat121test 
