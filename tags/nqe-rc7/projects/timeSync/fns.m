#comment i think
1; #useless line



function line()
endfunction



function funplot()
  points = linspace(-1,1,5);
  [x,y] = meshgrid(points, points);
  surf(x,y,x.*y);

endfunction



function randobst(N)

  xmin = 0;
  xmax = 20;
  ymin = 0;
  ymax = 10;
  
  hold off; figure(1);
  plot([xmin, xmax, xmax, xmin], [ymin, ymin, ymax, ymax], 'b-');
  hold on; axis;
  plot([xmin-1, xmax+1, xmax+1, xmin-1], [ymin-1, ymin-1, ymax+1, ymax+1], 'b.');
  %plot([xmin, xmax], [ymax, ymax]);
  
  o = rand(N,2);
  o(:,1) *= (xmax-xmin);
  o(:,2) *= (ymax-ymin);
  
  plot(o(:,1), o(:,2), 'ro');
endfunction



function plotr(array, col1, col2)
  plot(array(:,col1), array(:,col2));
endfunction



function [ret, index] = getInterp(time, x_col, xd_col, array, start_index)
				# get the two bracketting values in the array
				# step back
  i = start_index;
  while 1
    if (array(i+1,1) < time)
      i++;
    elseif (array(i,1) > time)
      i--;
    else
      break
    end
  end
  
  time1 = array(i,1);
  time2 = array(i+1,1);
  val1 = array(i,x_col);
  val2 = array(i+1,x_col);
  val1_dot = array(i,xd_col);
  val2_dot = array(i+1,xd_col);

  scale = 1/(time2 - time1);
  val1_shift = 0;
  val2_shift = val2 - val1;
  a = val1_shift;
  b = (val1_dot / scale);
  c = -3.0*val1_shift - 2.0*(val1_dot / scale) + 3.0*val2_shift - 1.0*(val2_dot / scale);
  d =  2.0*val1_shift + 1.0*(val1_dot / scale) - 2.0*val2_shift + 1.0*(val2_dot / scale);
  t = (time - time1) / (time2 - time1);
  ret = (a + b*t + c*t^2 + d*t^3) + val1;
  #interp_dot = (b + 2.0*c*t + 3.0*d*pow(t,2.0)) * scale;
  #interp_dot_dot = (2.0*c + 6.0*d*t) * pow(scale,2.0);  
  index = i;
endfunction



function [ret, index] = getInterpSimple(time, x_col, xd_col, array, start_index)
				# get the two bracketting values in the array
				# step back
  i = start_index;
  while 1
    if (array(i+1,1) < time)
      i++;
    elseif (array(i,1) > time)
      i--;
    else
      break
    end
  end
  
  time1 = array(i,1);
  time2 = array(i+1,1);
  val1 = array(i,x_col);
  val2 = array(i+1,x_col);

  t = (time - time1) / (time2 - time1);
  ret = val1 + (val2-val1)*t;
  #interp_dot = (b + 2.0*c*t + 3.0*d*pow(t,2.0)) * scale;
  #interp_dot_dot = (2.0*c + 6.0*d*t) * pow(scale,2.0);  
  index = i;
endfunction



function plotdiffs(array, varargin)
  if (nargin >= 2)
    plotnum = varargin{1};
  else
    plotnum = 0;
  end
  
  N = 1000;
#  x_col = 11; xd_col = 14; #roll
  x_col = 12; xd_col = 15; #pitch
#  x_col = 13; xd_col = 16; #yaw
  delta_time = .005;
  ladar_scan_dist = 100;

  times = linspace(array(2,1),array(size(array,1)-10,1),N)';
  results = times;
  
  index = 1;
  for i = 1:N
#    [results(i,2), index] = getInterp(times(i), x_col, xd_col, array, index);
#    [results(i,3), index] = getInterp(times(i)+delta_time, x_col, xd_col, array, index);
    [results(i,2), index] = getInterpSimple(times(i), x_col, xd_col, array, index);
    [results(i,3), index] = getInterpSimple(times(i)+delta_time, x_col, xd_col, array, index);
  end


  res = results;
  if (plotnum == 1 || plotnum == 0)
    figure(1);
    plot(res(:,1), res(:,2), 'ro-',
	 res(:,1), res(:,3), 'bo-',
	 array(:,1), array(:,x_col), 'go-',
	 array(:,1), array(:,xd_col), 'mo-');
  end
  if (plotnum == 2 || plotnum == 0)
    figure(2); plot(res(:,1), (res(:,3)-res(:,2))*ladar_scan_dist, 'bo-');
  end
endfunction



function selfcon(a, col)
  ## self consistency check... compare column `col' to column `col' + 3
  plot(a(:,1), a(:,col), 'ro-',
       a(:,1), a(:,col+3), 'bo-');
  title(strcat('Self consistency check, column num ', num2str(col)));
  legend('Value', 'Value dot');
  xlabel('time (s)');
  ylabel('(m and m/s) or (rad and rad/s)');
endfunction



function ret = fixtime(array)
  a = array(1,1);
  array(:,1) = array(:,1) - a;
  array(:,1) = array(:,1)/1000000;
  ret = array;
endfunction



function slpitch(st, lad)
  do_small = 0;
  do_roof = 1;
  do_bumper = 0;
  do_riegl = 0;

  if (do_small)
    ## for small
    theta_0 = .13845;
    theta_0 += -.0006;
    height = 2.3876;
    lad_start = 100;
    lad_end = 123;
  end

  if (do_roof)
    ## for roof
    theta_0 = .09032;
    theta_0 += .01;
    height = 2.413;
    lad_start = 100;
    lad_end = 123;
  end

  if (do_bumper)
    ## for roof
    theta_0 = .0;
    theta_0 += .0;
    height = .648;
    lad_start = 100;
    lad_end = 123;
  end

  if (do_riegl)
    ## for riegl
    theta_0 = .055;
    theta_0 += .013;
    height = 2.4384;
    lad_start = 80;
    lad_end = 100;
  end

  lad = [lad(:,1) lad(:,lad_start:lad_end)];
  dist = mean(lad(:,2:(1+lad_end-lad_start))') / 100; ## convert to meters

  pitch = theta_0 - asin(height ./ dist);
  ## just the ladar plot
  ## plot(lad(:,1), dist);
  plot(lad(:,1), pitch, 'ro-', st(:,1), st(:,12), 'bo-');
  legend('ladar', 'state');
  legend('ladar', 'state');
endfunction



function lplot(lad, start, ending)
  plot(lad(:,start:ending)');
endfunction



function [stod, rose, r40, r4, s200, s40, s4] = loaddata()
  stod = load('stod.dat');# data taken at 197.91 Hz
  rose = load('rose.dat');# data taken at 19.245 Hz
				# make sure rose.dat has been `uniq'ed... there are 13 dup lines
  stod = fixtime(stod); rose = fixtime(rose);
  
  r40 = rose(1005:1044,:);
  r4 = rose(1005:1008,:);
  s200 = stod(30000:30199,:);
  s40 = stod(30000:30039,:);
  s4 = stod(30000:30003,:);
endfunction
#[stod, rose, r40, r4, s200, s40, s4] = loaddata();



function [state small roof bumper riegl] = load_recent_data()
  do_state = 0;
  do_small = 0;
  do_roof = 1;
  do_bumper = 0;
  do_riegl = 0;

  if (do_state)
    state = fixtime(load('recent/astate/state.dat'));
  else
    state = [];
  end

  if (do_small)
    small = fixtime(load('recent/ladarFeeder/ladar_Small_SICK.raw'));
    small = small(:,1:183);
  else
    small = [];
  end

  if (do_roof)
    roof  = fixtime(load('recent/ladarFeeder/ladar_Roof_SICK.raw'));
    roof = roof(:,1:183);
  else
    roof = [];
  end

  if (do_bumper)
    bumper  = fixtime(load('recent/ladarFeeder/ladar_Bumper_SICK.raw'));
    bumper = bumper(:,1:183);
  else
    bumper = [];
  end

  if (do_riegl)
    riegl  = fixtime(load('recent/ladarFeeder/ladar_Riegl.raw'));
  else
    riegl = [];
  end

endfunction
##[state small roof bumper riegl] = load_recent_data()

##  [c,l] = xcov(s1,s2,50,'none'); plot(l,c);
