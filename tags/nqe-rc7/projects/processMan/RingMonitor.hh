/**
 * RingMonitor.hh
 * Revision History:
 * 09/08/2005  hbarnor  Created
 * $Id$
 */

#ifndef RINGMONITOR_HH
#define RINGMONITOR_HH

//#define DEBUG 1


#define MAX_SLEEP 15
#define MAX_LINE 4096
#define MAX_RING_SIZE 10
#define HB_EOF -4
//#define RING_CONFIG ring.cfg
//#define BCAST_IP 192.168.0.255


#include <string>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <ctime>


#include "networking.h"
#include "HeartBeat.hh"
#include "DGCTimer.hh"

#include "DGCutils"


using namespace std;

/**
 * RingMonitor dynamically determines a ringmaster at start-up. It
 * establishes a tcp/ip link with its neighbour and monitors the
 * neighbours heartbeat. In ringmaster mode, guides new computers into
 * the ring. Restarts computers that crash with/without warning.
 * $Id$
 */ 

class RingMonitor
{

public:
  /**
   * Default Constructor. 
   */
  RingMonitor();
  /**
   * Destructor - if it was not obvious.
   */
  ~RingMonitor();
  
  /**
   * brothersKeeper - connects to the neighbouring computer in the
   * ring and listens to its heartbeat. Initiates a restart if the
   * heartbeats die out. 
   * @return the error returned by reaData.
   */
  int brothersKeeper();
  /**
   * ringMaster - this method is the RingMaster. It starts a udp
   * server that listens for WIL messages, alarm clock messages and
   * dead computer messages. It might also be responsible for supercon
   * messages.
   */
  void ringMaster();

private:
  
  static const char *  BCAST_IP; 
  static const char *  RING_CONFIG; 
  string m_ringArray[MAX_RING_SIZE];
  struct sockaddr_in m_localAddr;
  int m_Position;
  int ringTotal;
  /**
   * The file descriptor on which we listen for heartbeats.
   */
  int heartBeat_fd;
  
  /**
   * Saved copy of the last heartbeat received. 
   */
  pmRmMesg m_currentHeartBeat;

  string m_localIP;
  int heartValue;
  /**
   * Pointer to an instance of the heartbeat server. 
   */
  HeartBeat::HeartBeat * p_myHeart;
  /**
   * Watchdog timer. Resets every N-milliseconds and triggers a signal
   * which then checks if we have had a heartbeat and calls the
   * appropriate action. 
   */
  DGCTimer * p_heartMonitor;
  /**
   * Signal to be connected with the heartbeat monitor. 
   */
  sigc::signal<void> * p_resuscitate;
private:
  // private functions 

  /**
   * processWhoIs - listens for responses to WIL message. Timeouts and
   * declares itself as leader if no responses come through. 
   */
  void processWhoIs();
  
  /**
   * sendWhoIs - broadcasts a who is leader (WIL) message. 
   * @return - true if the packet is successfuly sent and false otherwise.
   */
  bool sendWhoIs();
  /**
   * setLocalIP - sets the address and address type for the localhost
   * into the struct passed to it. 
   * @param localAdd - pointer to struct to be updated with the
   * localhost information.
   * Based heavily on example in UNIX NETWORK Programming.
   */
  void setLocalIP(struct sockaddr_in * localAdd);
  /**
   * UDP listening server that listens for WIL messages, alarm clock
   * messages and dead computer messages. It might also be responsible
   * for supercon messages.
   */
  void leaderServer();
  /**
   * buildRingArray - parses the config file and creates and array
   * of IPs in the ring and it also determines its IP in the config
   * file. Stores its position in the chain and uses it to determine
   * who to connect to next. 
   * @return true - if it finds itself in the chain and false otherwise.
   */
  bool buildRingArray();
  /**
   * makeTCPConnection - connects to the specified IP address and returns
   * a status of 0 if succeeds and -1 otherwise. If it fails it sleeps
   * for a number of seconds before returning. This is to prevent too
   * many attempts at connecting to a particular port. 
   * @param IP - the ip address to connect to. 
   * @return file descriptor if it succeeds and -1 otherwise.
   */
  int makeTCPConnection(string IP);
  /**
   * nextInRing - returns the IP address of the next computer in the
   * ring. 
   * Uses the array of the ring to return the next computer in the
   * ring without returning itself as a possible in the ring.
   * @return the IP address of the next computer in the ring. NULL if
   * array is not initialized 
   */
  string nextInRing();
  /**
   * readData - reads in data from the pass file descriptor into the
   * passed memory location. 
   * This is basicaly a wrapper for Steven richars, readn function. 
   * @param filedes - the file descriptor to read from.
   * @param data - the memory location to store the data being read.
   * @param nbytes - amount of data to be read. 
   * @return the number of bytes read.
   */
  ssize_t readData(int filedes, void * data, size_t nbytes);
  /**
   * checkHeartBeat - checks if a new heartbeat has been received 
   * since the last timeOut. If it has been received - do nothing
   * else request a restart of the neighbour. 
   */
  void checkHeartBeat();
  // prints out the host of the heartbeat
  void displayHeartBeat();
};


#endif //RINGMONITOR_HH
