#include "sharemem.hh"
#include <string.h>
union semun {
    int val;              /* used for SETVAL only */
    struct semid_ds *buf; /* for IPC_STAT and IPC_SET */
    ushort *array;        /* used for GETALL and SETALL */
};

void dumpsem(char *cust, int semid, int nsem) {
#if SHM_DEBUG
	int i;
	

	printf("%s:Semval[%d]:", cust, semid) ;
	for (i = 0 ; i < nsem; i++)
	    printf( "%d, ", semctl(semid, i, GETVAL));
	printf("\n");
#endif
}

int ShmGetKey(char *fname, int cr, key_t &key) {
	FILE *f_desc;
	
	while ((access(fname, F_OK) == -1)&&(!cr)) {
		printf("Waiting for key file %s to be created by master.\n", fname);
		sleep(1);
	}
	
	if ( (f_desc = fopen(fname,"w")) == NULL) {
		perror(fname);
		return SHM_ERROR; 
	}
	else { 
		fclose(f_desc);
		chmod(fname, S_IRUSR | S_IWUSR  | S_IRGRP  | S_IROTH);
	}
	
	if ((key = ftok(fname, 'J')) == -1) {
	        perror("ftok");
		return SHM_ERROR;
	}

	return SHM_SUCCESS;
}

int ShmSetSemVal(int semid, int semnum, int val) {
 	union semun arg;
	arg.val = val;
	
	if (semctl(semid, semnum, SETVAL, arg) == -1) {
		perror("sem_unlock_err");
		return SHM_ERROR;
	}
		    
	return SHM_SUCCESS;
}

int ShmLockSem(int semid, int semnum) { 
	struct sembuf sop_buf = { semnum, -1 , 0};
		
	if (semop(semid, &sop_buf, 1) == -1) {
	          perror("sem_lock_err");
		  return SHM_ERROR;
	}
	
	return SHM_SUCCESS;
}


int ShmUnlockSem(int semid, int semnum) {
	struct sembuf sop_buf = { semnum, 1 , 0};
		
	if (semop(semid, &sop_buf, 1) == -1) {
	          perror("semop");
		  return SHM_ERROR;
	}
	
	return SHM_SUCCESS;

}

int ShmWaitSemLock(int semid, int semnum) {
	struct sembuf sop_buf = { semnum, 0 , 0};
		
	if (semop(semid, &sop_buf, 1) == -1) {
	          perror("semop");
		  return SHM_ERROR;
	}
	
	return SHM_SUCCESS;

}

int ShmConnectMem(key_t key, int &semid, int numsem, int &shmid, int shmsize, int alloc) {
	int sh_flags;
	int i;
	sh_flags = alloc?0666|IPC_CREAT:0;
	
	//obtain a semaphore
	if ((semid = semget(key, numsem, sh_flags)) == -1) {
		perror("semget failed");
		return SHM_ERROR;
	}
	
	if (alloc) {
		for ( i = 0 ; i < numsem -1 ; i++) 
			if (ShmSetSemVal(semid, i, 1) == SHM_ERROR) return SHM_ERROR;
	}
	
	//obtain the mem 
	if ((shmid = shmget(key, shmsize, sh_flags)) == -1) {
		perror("shared mem fialed");
		return SHM_ERROR;
	}
	
	return SHM_SUCCESS;
}

int ShmDisconnectMem(char* key_file, int semid, int shmid, int dealloc ) { 
	if (dealloc) {
		if (remove(key_file) == -1) { 
			perror("remove_file");
			return SHM_ERROR;
		}
		
		if ( semctl(semid, 0, IPC_RMID) == -1)  {
			perror("semctl_delete");
			return SHM_ERROR;
		}

		if ( shmctl(shmid, IPC_RMID, NULL) == -1) {
			perror("shmctk_delete");
			return SHM_ERROR;
		}
	}
			
	return SHM_SUCCESS;
}

int ShmSh2MemCpy(int shmid, void *buffer, int size) {
	void *data;
	
	data = shmat(shmid, (void *)0, 0);
	
	if (data == (char *)(-1)) {
		perror("shmat");
		return SHM_ERROR;
	}
	
	memcpy(buffer, data, size);

	if (shmdt(data) == -1) {
		perror("shmdt");
		return SHM_ERROR;
	}
 
	return SHM_SUCCESS;
}

int ShmMem2ShCpy(int shmid, void *buffer, int size) {
	void *data;
	
	data = shmat(shmid, (void *)0, 0);
	
	if (data == (char *)(-1)) {
		perror("shmat");
		return SHM_ERROR;
	}
	
	memcpy(data,buffer, size);

    if (shmdt(data) == -1) {
       perror("shmdt");
       return SHM_ERROR;
	}
	return SHM_SUCCESS;
}

//-------------------------------------------------------------------------------
CShmMaster::CShmMaster() { 
 	semid = -1;
	shmid = -1;
}

CShmMaster::~CShmMaster() {
	DeInit();
}

int CShmMaster::Init(char *key_file, int size) { 
	if (ShmGetKey(key_file, 1, key) == SHM_ERROR) return SHM_ERROR;
	strcpy(key_fname, key_file);
	
	if (!ShmConnectMem(key, semid, SHM_NUM_SEM, shmid, size, 1)) return SHM_ERROR;
	
	ShmSetSemVal(semid, SHM_READ_SEM, 0);
	ShmSetSemVal(semid, SHM_WRITE_SEM, 1);
	
	dumpsem("init", semid, 2);	
	return SHM_SUCCESS;

}

int CShmMaster::DeInit() { 

	if (!ShmDisconnectMem(key_fname, semid, shmid, 1))
		return SHM_ERROR;
	printf("Semaphores and sharemem destroyed\n");
	semid = -1;
	shmid = -1;
	return SHM_SUCCESS;

}



int CShmMaster::Request() { 

	dumpsem("reqstart", semid, 2);	
	//lock the reading adn allow for writting 
	if (!ShmUnlockSem(semid, SHM_READ_SEM)) return SHM_ERROR;
	if (!ShmLockSem(semid, SHM_WRITE_SEM)) return SHM_ERROR;
	
	dumpsem("reqbblock", semid, 2);	
	//block trying second lock on the reading(writter should unlock for reading)
	if (!ShmWaitSemLock(semid, SHM_READ_SEM)) return SHM_ERROR;
	
	dumpsem("reqend", semid, 2);	
	return SHM_SUCCESS;
}

int CShmMaster::Sh2MemCpy(void *buffer, int size) { 
	return ShmSh2MemCpy(shmid, buffer, size);
}

void* CShmMaster::MemConnect() {
	shm_ptr = shmat(shmid, (void *)0, 0);
	
	if (shm_ptr == (char *)(-1)) {
		perror("shmat");
		return NULL;
	}
	
	return shm_ptr;
}

int CShmMaster::MemDisconnect() {
    if (shmdt(shm_ptr) == -1) {
       perror("shmdt");
       return SHM_ERROR;
	}
	return SHM_SUCCESS;

}

//-------------------------------------------------------------------------------
CShmSlave::CShmSlave() { 
 	semid = -1;
	shmid = -1;
}

CShmSlave::~CShmSlave() {
	DeInit();
}


int CShmSlave::Init(char *key_file, int size) { 
	if (!ShmGetKey(key_file, 0, key)) return SHM_ERROR;
	if (!ShmConnectMem(key, semid, SHM_NUM_SEM, shmid, size, 0)) return SHM_ERROR;

	return SHM_SUCCESS;
}

int CShmSlave::DeInit() {
	if (!ShmDisconnectMem(NULL, semid, shmid, 0))
		return SHM_ERROR;
	
	semid = -1;
	shmid = -1;
	return SHM_SUCCESS;
}


int CShmSlave::WaitForRequest() {
	//try to lock for writting, block waiting
	dumpsem("waitfr", semid, 2);	
	if (!ShmWaitSemLock(semid, SHM_WRITE_SEM)) return SHM_ERROR;
	if (!ShmUnlockSem(semid, SHM_WRITE_SEM)) return SHM_ERROR;
	dumpsem("waitfr-e", semid, 2);	

	return SHM_SUCCESS;
}

int CShmSlave::RequestComplete() {
	//unblock the reading 
	dumpsem("complete", semid, 2);	
	if (!ShmLockSem(semid, SHM_READ_SEM)) return SHM_ERROR;
	dumpsem("complete-e", semid, 2);	

	return SHM_SUCCESS;
}

int CShmSlave::Mem2ShCpy(void *buffer, int size) { 
	//ensure that we have lock for writting 
	return ShmMem2ShCpy(shmid, buffer, size);
}

void* CShmSlave::MemConnect() {
	shm_ptr = shmat(shmid, (void *)0, 0);
	
	if (shm_ptr == (char *)(-1)) {
		perror("shmat");
		return NULL;
	}
	
	return shm_ptr;
}

int CShmSlave::MemDisconnect() {
    if (shmdt(shm_ptr) == -1) {
       perror("shmdt");
       return SHM_ERROR;
	}
	return SHM_SUCCESS;

}
