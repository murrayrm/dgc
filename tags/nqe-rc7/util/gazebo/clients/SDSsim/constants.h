#include <stdlib.h>
#define DGC_ALICE_BRAKE_MINPOS  0
#define DGC_ALICE_BRAKE_MAXPOS  15
#define DGC_ALICE_BRAKE_ON      DGC_ALICE_BRAKE_MAXPOS
#define DGC_ALICE_BRAKE_OFF     DGC_ALICE_BRAKE_MINPOS
#define DGC_ALICE_BRAKE_READ    'R'
#define DGC_ALICE_BRAKE_COMMAND 'C'
#define DGC_ALICE_BRAKE_MAXDELAY        100000  /* in microseconds */

#define THROTTLE_IDLE 0
#define THROTTLE_MAX  1
#define THROTTLE_OUTPUT_LENGTH 6
#define THROTTLE_INPUT_LENGTH 4
#define START_CHARACTER 'B'
#define THROTTLE_START  0
#define APPS1 1
#define APPS2 2
#define APPS3 3
#define THROTTLE_END 4
#define END_CHARACTER 'E'

#define THROTTLE_GET_POSITION 'S'

//values to output for idle
#define APPS1_IDLE 206
#define APPS2_IDLE 76
#define APPS3_IDLE 48

//values to output for full throttle
#define APPS1_FT 51 
#define APPS2_FT 193
#define APPS3_FT 165

//ranges, inclusive of idle and full throttle
#define APPS1_RANGE 155
#define APPS2_RANGE 117
#define APPS3_RANGE 117
