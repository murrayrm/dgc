#include "CRddfPrep.hh"

using namespace std;

/* CRddfPrep no arg constructor */
CRddfPrep::CRddfPrep(int skynet_key)
  : CSkynetContainer(MODrddfPrep, skynet_key), RDDF(RDDF_FILE, false)
{
  pd("CRddfPrep::CRddfPrep start", rmsg::fncall, 1);
  CRddfPrepInit();  
  pd("CRddfPrep::CRddfPrep finish", rmsg::fncall, 1);
}



/* CRddfPrep one arg constructor */
CRddfPrep::CRddfPrep(int skynet_key, char* rddf_file)
  : CSkynetContainer(MODrddfPrep, skynet_key), RDDF(rddf_file, false)
{
  pd("CRddfPrep::CRddfPrep start", rmsg::fncall, 1);
  CRddfPrepInit();  
  pd("CRddfPrep::CRddfPrep finish", rmsg::fncall, 1);
}



/* Destructor */
CRddfPrep::~CRddfPrep()
{
  pd("CRddfPrep::~CRddfPrep start", rmsg::fncall, 1);
  pd("CRddfPrep::~CRddfPrep finish", rmsg::fncall, 1);
}



/* constructor helper */
void CRddfPrep::CRddfPrepInit()
{
  pd("CRddfPrep::CRddfPrepInit start", rmsg::fncall, 1);
  pd("CRddfPrep::CRddfPrepInit finish", rmsg::fncall, 1);
}



/* see header for description */
void CRddfPrep::prependRDDFWithCurrentState()
{
  pd("CRddfPrep::prependRDDFWithCurrentState start", rmsg::fncall, 1);
  /** create the first waypoint from our current state */
  RDDFData temp_point;
  temp_point.number = 0;
  temp_point.distFromStart = 0;

  cout << endl << "FYI, state looks something like..." << endl;
  for (int i = 1; i < WAIT_CYCLES; i++)
    {
      WaitForNewState();
      printf("N = %f,  E = %f\n", m_state.Northing, m_state.Easting);
    }
  temp_point.Northing = m_state.Northing;
  temp_point.Easting = m_state.Easting;

  /** remove the old prepended point, if it exists */
  if (is_startup_prepended_rddf)
    {
      // targetPoints.pop_front();
      // doesn't work... so do 'erase'
      targetPoints.erase(targetPoints.begin());
      numTargetPoints--;
    }

  /** make sure the current RDDF is not empty */
  if (numTargetPoints < 1)
    {
      pd("CRddfPrep::prependRDDFWithCurrentState RDDF is empty!!!", rmsg::error, -1);
      exit(-1);
    }

  /** use the LBO and speed from the old first waypoint */
  temp_point.maxSpeed = targetPoints[0].maxSpeed;
  temp_point.offset = targetPoints[0].offset;
  temp_point.radius = temp_point.offset;

  /** stick it on the beginning of the RDDFVector */
  //  targetPoints.push_front(temp_point);
  // doesn't work.. so we'll use 'insert'
  targetPoints.insert(targetPoints.begin(), temp_point);
  numTargetPoints++;
  
  /** update the number and distFromStart of the rest of the RDDF */
  double first_cor_dist = hypot(targetPoints[1].Northing - targetPoints[0].Northing,
				targetPoints[1].Easting - targetPoints[0].Easting);
  for (int i = 1; i < numTargetPoints; i++)
    {
      targetPoints[i].number++;
      targetPoints[i].distFromStart += first_cor_dist;
    }

  is_startup_prepended_rddf = true;

  pd("CRddfPrep::prependRDDFWithCurrentState finish", rmsg::fncall, 1);
}
