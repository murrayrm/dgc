//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_OnOff.hh"

#ifdef SKYNET
#include "sparrowhawk.hh"
#endif

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// is sun apparently in view, causing blooming on CCD?
/// test is to find a column of nearly all saturated pixels.  some temporal
/// filtering also done.

int UD_OnOff::sun_saturation(IplImage *im)
{
  int i, j, colsat, num_dilations;
  float imheight, colsatfrac, maxfrac;

  cvCmpS(im, 255, saturation_mask, CV_CMP_GE);
  num_dilations = 1;   // default structuring element is 3 x 3 
  for (i = 0; i < num_dilations; i++)
    cvDilate(saturation_mask, saturation_mask);

  imheight = (float) im->height;
  for (i = 0, maxfrac = 0.0; i < im->width; i++) {
    for (j = 0, colsat = 0; j < im->height; j++)
      if ((int) UCHAR_IMXY(saturation_mask, i, j))
	colsat++;
    colsatfrac = (float) colsat / imheight;
    if (colsatfrac > maxfrac)
      maxfrac = colsatfrac;
  }

  SAT_now = maxfrac;
  
  SAT_history[SAT_index] = SAT_now;

  if (SAT_index < SAT_time_window - 1)
    SAT_index++;
  else {
    SAT_history_full = TRUE;
    SAT_index = 0;
  }

  // threshold and filter based on past history

  int num_SAT;
  float SAT_confidence;

  if (!SAT_history_full) {
    for (i = 0, num_SAT = 0; i < SAT_index; i++)
      if (SAT_history[i] >= SAT_threshold)
	num_SAT++;
    SAT_confidence = (float) num_SAT / (float) SAT_index;
  }
  else {
    for (i = 0, num_SAT = 0; i < SAT_time_window; i++)
      if (SAT_history[(i + SAT_index) % SAT_time_window] >= SAT_threshold)
	num_SAT++;
    SAT_confidence = (float) num_SAT / (float) SAT_time_window;
  }

  //  printf("SAT now = %f, frac = %f\n", SAT_now, SAT_confidence);

  /*
  for (i = 0; i < SAT_time_window; i++) {
    if (i == SAT_index)
      printf("* %i %f\n", i, SAT_history[i]);
    else
      printf("  %i %f\n", i, SAT_history[i]);
  }
  printf("\n");
  */

  return SAT_confidence >= SAT_majority_fraction;   
}

//----------------------------------------------------------------------------

/// is the sun at or below the horizon?

int UD_OnOff::too_dark(double sunalt)
{
  return (sunalt < DEG2RAD(5.0));
}

//----------------------------------------------------------------------------

/// is the sun is very low and right behind us?  plus some temporal filtering

/// current criterion is purely geometric: just cut off road-following output entirely when VP estimate is 
/// within some distance of shadow tip under assumption that it's possibly biased by the shadow.

/// other possibilities involve image processing: (1) don't let shadow pixels vote (would screw up KL threshold
/// for non-road), or (2) inhibit particle likelihoods around tip of shadow region so that VP prefers
/// candidates elsewhere (but what if the true VP coincides with shadow tip?).

int UD_OnOff::possible_shadow(double direction_error, double heading, double sunalt, double sunazi)
{
  double shadow, vp_heading, shadow_diff;
  int i;

  shadow = sunazi + PI;   // shadow direction 180 degs. from sun direction
  if (shadow < 0)
    shadow += 2 * PI;
  vp_heading = direction_error + heading;
  if (vp_heading < 0)
    vp_heading += 2 * PI;

  shadow_diff = fabs(shadow - vp_heading);

  //  printf("vp = %lf (%lf), shadow = %lf, diff = %lf\n", RAD2DEG(vp_heading), RAD2DEG(direction_error), RAD2DEG(shadow), RAD2DEG(shadow_diff));

  SHAD_azi_history[SHAD_index] = shadow_diff;
  SHAD_alt_history[SHAD_index] = sunalt;

  if (SHAD_index < SHAD_time_window - 1)
    SHAD_index++;
  else {
    SHAD_history_full = TRUE;
    SHAD_index = 0;
  }

  // threshold and filter based on past history

  int num_SHAD;
  float SHAD_confidence;

  if (!SHAD_history_full) {
    for (i = 0, num_SHAD = 0; i < SHAD_index; i++)
      if (SHAD_alt_history[i] < SHAD_alt_threshold && SHAD_azi_history[i] < SHAD_azi_threshold)
	num_SHAD++;
    SHAD_confidence = (float) num_SHAD / (float) SHAD_index;
  }
  else {
    for (i = 0, num_SHAD = 0; i < SHAD_time_window; i++)
      if (SHAD_alt_history[(i + SHAD_index) % SHAD_time_window] < SHAD_alt_threshold &&
	  SHAD_azi_history[(i + SHAD_index) % SHAD_time_window] < SHAD_azi_threshold)
	num_SHAD++;
    SHAD_confidence = (float) num_SHAD / (float) SHAD_time_window;
  }

  return SHAD_confidence >= SHAD_majority_fraction;   

  /*
  return ((sunalt < DEG2RAD(20.0)) &&                          
	 ((fabs(shadow1 - heading) < DEG2RAD(30.0)) ||    
	  (fabs(shadow2 - heading) < DEG2RAD(30.0))));   
  */ 
}

//----------------------------------------------------------------------------

int UD_OnOff::shadow_or_darkness(IplImage *im, double direction_error, double heading, double sunalt, double sunazi)
{
  //  return FALSE;
  return (too_dark(sunalt) || possible_shadow(direction_error, heading, sunalt, sunazi));
}

//----------------------------------------------------------------------------

/// constructor for on/off road decision class.  threshold = 0.2 is 
/// tuned for 160 x 120 images, hence check that image dimensions match--
/// remove if further tuning done for other size.  also assumes max
/// vote level of 256, which would not be correct if HAVE_GEFORCE6 is defined


UD_OnOff::UD_OnOff(UD_ImageSource *imsrc, float threshold, int time_window, float short_window_fraction, float majority_fraction)
{
  using_OnOff=true;

  #ifdef SKYNET
  SparrowHawk().rebind("OnOffOn", &using_OnOff);
  SparrowHawk().rebind("OnOffVal", &KL_now);
  SparrowHawk().rebind("OnOffThres", &KL_threshold);
  SparrowHawk().set_readonly("OnOffThres");
  #endif

  // local names

  KL_threshold = threshold;                
  KL_time_window = time_window;            
  KL_short_time_window = (int) round(short_window_fraction * (float) KL_time_window);   
  KL_majority_fraction = majority_fraction;

  SAT_threshold = 0.8;                
  SAT_time_window = 10;
  SAT_majority_fraction = 0.3;

  SAT_history = (float *) calloc(SAT_time_window, sizeof(float));
  SAT_history_full = FALSE;
  SAT_index = 0;

  SHAD_alt_threshold = DEG2RAD(15.0);                
  SHAD_azi_threshold = DEG2RAD(30.0);                
  SHAD_time_window = 10;
  SHAD_majority_fraction = 0.3;

  SHAD_alt_history = (float *) calloc(SHAD_time_window, sizeof(float));
  SHAD_azi_history = (float *) calloc(SHAD_time_window, sizeof(float));
  SHAD_history_full = FALSE;
  SHAD_index = 0;
  
  // default values

  print_onoff                      = FALSE;
  do_onoff                         = TRUE;
  onroad_state                     = TRUE;

  // body

  // threshold only valid for 160 x 120 images or 180 x 120 images

  //  if (imsrc->im->height != 120 || (imsrc->im->width != 160 && imsrc->im->width != 180)) {
  if (imsrc->im->height != 120 && imsrc->im->height != 60) {
    printf("not using on/off road test -- image size must be w x 120 or w x 60\n");
    do_onoff = FALSE;
  }

  // to redden display when we think we're off road

  redim = cvCreateImage(cvSize(imsrc->im->width, imsrc->im->height), IPL_DEPTH_8U, 3);
  cvSet(redim, cvScalar(1, 0, 0));

  // yellow display when we think there's sun saturation

  yellowim = cvCreateImage(cvSize(imsrc->im->width, imsrc->im->height), IPL_DEPTH_8U, 3);
  cvSet(yellowim, cvScalar(1, 1, 0));

  // blue display when we think there's shadow or darkness

  blueim = cvCreateImage(cvSize(imsrc->im->width, imsrc->im->height), IPL_DEPTH_8U, 3);
  cvSet(blueim, cvScalar(0, 0, 1));

  colorim = cvCreateImage(cvSize(imsrc->im->width, imsrc->im->height), IPL_DEPTH_8U, 3);

  // to detect sun in image

  saturation_mask = cvCreateImage(cvSize(imsrc->im->width, imsrc->im->height), IPL_DEPTH_8U, 1);

#ifdef HAVE_GEFORCE6
  printf("GeForce: different number of gray levels in effect\n");
  exit(1);
#endif

  // assume 256 gray levels in vote function

  KL_num_vote_levels = 256;
  KL_uniform_prob = 1.0 / (float) KL_num_vote_levels;
  KL_histogram = (float *) calloc(KL_num_vote_levels, sizeof(float));
  
  // initialize array to store last time_window KL values
  // this wraps around, so keep track of current index and whether we've wrapped

  KL_history = (float *) calloc(KL_time_window, sizeof(float));
  KL_history_full = FALSE;
  KL_index = 0;
}

//----------------------------------------------------------------------------

/// return fraction of last KL_time_window KL measurements
/// that were over KL_threshold.  it's up to caller to 
/// threshold this on KL_majority_fraction or use directly as confidence

void UD_OnOff::compute_confidence(IplImage *vote_im, IplImage *raw_im)
{
  int i, num_on, time_window;
 
  if (!do_onoff) {
    onroad_confidence = 1.0;
    onroad_state = TRUE;
    return;
  }

  // check for saturation due to sun

  if (sun_saturation(raw_im)) {
    onroad_state = FALSE;
    noroad_reason = SUNGLARE_ERROR;
//     KL_history_full = FALSE;
//     KL_index = 0;
    return;
  }
  else 
    onroad_state = TRUE;

  // record current value in history buffer (which wraps around)

  //  print_onoff = TRUE;

  KL_now = KL_from_uniform(vote_im);
  KL_history[KL_index] = KL_now;

  if (print_onoff)
    printf("%i %.2f ", KL_index, KL_history[KL_index]);
 
  if (KL_index < KL_time_window - 1)
    KL_index++;
  else {
    KL_history_full = TRUE;
    KL_index = 0;
  }

  // threshold and filter based on past history

  if (!KL_history_full) {
    for (i = 0, num_on = 0; i < KL_index; i++)
      if (KL_history[i] > KL_threshold)
	num_on++;
    onroad_confidence = (float) num_on / (float) KL_index;
  }
  else {
    if (onroad_state) 
      time_window = KL_short_time_window;
    else
      time_window = KL_time_window;

    for (i = 0, num_on = 0; i < time_window; i++)
      if (KL_history[(i + KL_index) % KL_time_window] >= KL_threshold)
	num_on++;
    onroad_confidence = (float) num_on / (float) time_window;
  }
  
  if (print_onoff)
    printf("index %i full %i, on %i conf %.2f KL %.2f\n", KL_index, KL_history_full, num_on, onroad_confidence, KL_history[KL_index-1]);

  if (using_OnOff)
    onroad_state = onroad_confidence >= KL_majority_fraction;   
  else
    onroad_state = TRUE;

  if (!onroad_state)
    noroad_reason = NOROADVP_ERROR;
}

//----------------------------------------------------------------------------

/// compute Kullback-Leibler divergence of vote function from uniform distribution

float UD_OnOff::KL_from_uniform(IplImage *im)
{
  int i, j, votes;
  float total_pixels, KL, prob, fractional_votes, total_votes;

  // compute probability of each vote total in this image

  for (i = 0; i < KL_num_vote_levels; i++)
    KL_histogram[i] = 0;

  for (j = 0, total_pixels = total_votes = 0.0; j < im->height; j++)
    for (i = 0; i < im->width; i++) {
      fractional_votes = FLOAT_IMXY(im, i, j);   // this is a number b/t 0 and 1, inclusive
      votes = (int) ((float) (KL_num_vote_levels - 1) * fractional_votes);
      total_votes += votes;
      KL_histogram[votes]++;
      total_pixels++;
    }

  // compute divergence from uniform distribution

  for (i = 0, KL = 0; i < KL_num_vote_levels; i++) {
    prob = KL_histogram[i] / total_pixels;  	
    //prob = KL_histogram[i] / total_votes;  	
    //    if (print_onoff)
    //printf("%i %f\n", i, prob);
    if (prob) 
      KL += KL_uniform_prob * (log(prob / KL_uniform_prob));
  }

  return -KL;
}

//----------------------------------------------------------------------------

/// process command-line flags relevant to on/off road decision calculations

void UD_OnOff::process_command_line_flags(int argc, char **argv) 
{
  int i;

  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i],                             "-klthresh"))
      KL_threshold = atof(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-klthreshold"))
      KL_threshold = atof(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-klmajority"))
      KL_majority_fraction = atof(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-printonoff"))
      print_onoff = TRUE;
    else if (!strcmp(argv[i],                        "-noonoff"))
      do_onoff = FALSE;
  }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
