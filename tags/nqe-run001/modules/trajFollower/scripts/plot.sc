#!/bin/sh -x

rm -f state path yerr FF_FB


#state is the places we've been, path is the
# part of the trajectory that sn_TrajFollower has come closest to

paste $FILE | awk '{print $16, $17}' > state
paste $FILE | awk '{print $1, $2}' > path

#echo "plot 'path', 'state' with dots" | gnuplot -persist

# plotting the perpendicular y-error as a function of some arbitrary time
# variable...

paste $FILE | awk '{print NR,$3}' > yerr
paste $FILE | awk '{print NR,$6*$7}' > FF_FB

#echo "plot 'yerr' with dots" | gnuplot -persist
#echo "plot 'FF_FB' with dots" | gnuplot -persist

echo "set terminal pdf; set output \"AvsD.pdf\"; plot 'path','state' with dots" | gnuplot -persist
echo "set terminal pdf; set output \"yerr.pdf\"; plot 'yerr' with dots" | gnuplot -persist
echo "set terminal pdf; set output \"FF_FB.pdf\"; plot 'FF_FB' with dots" | gnuplot -persist

mv AvsD.pdf $FILE\_AvsD.pdf
mv yerr.pdf $FILE\_yerr.pdf
mv FF_FB.pdf $FILE\_FF_FB.pdf

rm yerr FF_FB state path
