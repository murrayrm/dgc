function calculatePhi()
     file = 'sl070';
     input = load(file);

     
#wheelbase
     L = 3.55;
     time = input(:,1);

     steerAct = input(:,3);
     steerCmd = input(:,4);

     dN = input(:,28);
     dE = input(:,29);

     dYaw = input(:,39);

     vel = sqrt(dN.*dN+dE.*dE);

     temp = L*dYaw./vel;

     phi = asin(temp);

     result = [time,steerAct,steerCmd,dYaw,vel,phi];

 save -ascii sl070.calc result
