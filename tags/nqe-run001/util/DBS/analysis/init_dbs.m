##
## For line fitting stuff
##
1;

global manual_data;
global norm;
manual_data = [];

function addpoints(mat, start, finish, timestart, timeend, bumpiness)
  global norm;
  global manual_data;
  temp = [];
  for i = 1:size(mat,1)
    if (mat(i,1)./1000000 >= timestart & mat(i,1)./1000000 <= timeend)
      temp = [temp;
	      mat(i,1:2), sum(mat(i,168:187)), sum(mat(i,328:347)), sum(mat(i,168:187)), sum(mat(i,328:347)), bumpiness];
    end
  end
  ## max
  ##  temp = ones(size(temp,1), 1) * max(temp);
  ##  temp = max(temp);
  ## norm
  ## uncomment to use speed weighting
##  temp(:,3:size(temp,2)-1) = temp(:,3:size(temp,2)-1) .* ((1.5658 .* (.6 ./ (1 + exp(- (temp(:,2) - 8) ./ 2)) + .2))  *  ones(1,size(temp,2)-3));
#  disp(size(temp(:,5:6)))
#  disp(sum(temp(:,5:6) .^ norm))
#  disp(size(temp(:,5:6),1))
  norm1 = 1;
  tempnorm1 = (sum(temp(:,[1:4]) .^ norm1) ./ size(temp(:,[1:4]),1)) .^ (1/norm1); ## norm1 cols 1:4
  tempnorm = (sum(temp(:,5:6) .^ norm) ./ size(temp(:,5:6),1)) .^ (1/norm); ## norm just cols 5,6
  tempstddev = std(temp(:,3:4));
  tempnorm9 = (sum(temp(:,[7]) .^ norm1) ./ size(temp(:,[7]),1)) .^ (1/norm1); ## norm1 col 7
#  temp = [tempnorm1 tempnorm tempstddev tempnorm9];
  temp = [tempnorm1 tempnorm tempnorm9];

  manual_data = [manual_data; temp];
end

addpoints(long{2}, 328, 347, 1126386729, 1126386748, .6);
addpoints(long{2}, 328, 347, 1126386755, 1126386762, .75);
addpoints(long{2}, 328, 347, 1126386816-6, 1126386816+6, .75);
addpoints(long{2}, 328, 347, 1126386826-4, 1126386826, 1);
addpoints(long{2}, 328, 347, 1126386854, 1126386865, .5);
addpoints(long{2}, 328, 347, 1126386883, 1126386903, .6);
addpoints(long{2}, 328, 347, 1126386952, 1126386966, .8);
addpoints(long{2}, 328, 347, 1126387071, 1126387081, .6);
addpoints(long{2}, 328, 347, 1126387167, 1126387174, .75);
addpoints(long{2}, 328, 347, 1126387298-3, 1126387298, .85);
addpoints(long{2}, 328, 347, 1126387338, 1126387344, .45);
addpoints(long{2}, 328, 347, 1126387353+7, 1126387364+2, .9);
addpoints(long{2}, 328, 347, 1126387452, 1126387464, .5);
addpoints(long{2}, 328, 347, 1126387473, 1126387482, .8);
addpoints(long{2}, 328, 347, 1126387525, 1126387540, .4);
addpoints(long{2}, 328, 347, 1126387541-4, 1126387541+2, .3);
addpoints(long{2}, 328, 347, 1126387546, 1126387546+6, 1);
addpoints(long{2}, 328, 347, 1126387560-6, 1126387560+1, 1);
addpoints(long{3}, 328, 347, 1126388205, 1126388246, .3);
addpoints(long{3}, 328, 347, 1126388320, 1126388320+20, .35);
addpoints(long{4}, 328, 347, 1126388397, 1126388433, .1);
addpoints(long{5}, 328, 347, 0, 1e20, .625);
addpoints(long{6}, 328, 347, 0,1e20, .8);
addpoints(long{7}, 328, 347, 1126413491, 1126413500-4, 1);
addpoints(long{8}, 328, 347, 0,1e20, .8);
addpoints(long{9}, 328, 347, 1126413997+20, 1126414031, 1);
