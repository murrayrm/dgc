/*
 *  Gazebo - Outdoor Multi-Robot Simulator
 *  Copyright (C) 2003  
 *     Nate Koenig & Andrew Howard
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
/* Desc: Model for a ClodBuster
 * Author: Pranav Srivastava
 * Date: 4 Sep 2003
 * CVS: $Id: ClodBuster.cc,v 1.19 2004/06/01 22:26:24 inspectorg Exp $
 * Comment: The model is accurate to physical dimensions and implements front steering 4wd  */

#include <assert.h>
#include "gazebo.h"
#include "World.hh"
#include "WorldFile.hh"

#include "Body.hh"
#include "BoxGeom.hh"
#include "SphereGeom.hh"
#include "CylinderGeom.hh"
#include "WheelGeom.hh"
#include "HingeJoint.hh"

#include "ClodBuster.hh"
#include <stdio.h>
#include <ode/ode.h>
#define sign(x) (x/fabs(x))
#define RMIN 1.000
const double RBIG=1e6; 
const double kSteer=5; 

//////////////////////////////////////////////////////////////////////////////
// Creation function
Model *NewClodBuster( World *world )
{
  return new ClodBuster(world);
}


//////////////////////////////////////////////////////////////////////////////
// Constructor
ClodBuster::ClodBuster( World *world )
  : Model( world )
{
  this->encoders[0] = 0;
  this->encoders[1] = 0;
  return;
}


//////////////////////////////////////////////////////////////////////////////
// Destructor
ClodBuster::~ClodBuster()
{
  return;
}


//////////////////////////////////////////////////////////////////////////////
// Load the model
int ClodBuster::Load( WorldFile *file, WorldFileNode *node )
{
  // Create the ODE objects
  if (this->OdeLoad(file, node) != 0)
    return -1;

  // Model id
  
  this->raw_encoder_position = node->GetBool( "raw_encoder_position", false );

  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Initialize the model
int ClodBuster::Init( WorldFile *file, WorldFileNode *node )
{
  // Initialize ODE objects
  if (this->OdeInit(file, node) != 0)
    return -1;

  // Initialize external interface
  if (this->IfaceInit() != 0)
    return -1;
  
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Load ODE objects
int ClodBuster::OdeLoad( WorldFile *file, WorldFileNode *node )
{
  int i;
  Geom *geom;

  // Create a model where the top level of the roof is exactly 
  // 75.75 inch = 1.92405 meters from the ground. 
  // Wheel suspension and softness might screw up the precision here
  
  // The wheels
  this->wheelSep = 0.50; // how high above the ground the body is ?
  this->wheelDiam = 0.50;  // meters
  const double wheelThick = 0.3; // meters
  const double wheelMass = 1; // kilos (clodbuster has 0.5)
  
  // the "main" part of the chassi
  const double mainLength = 4.9; // meters
  const double mainWidth = 2.38; // meters
  const double mainHeight = .42405 + this->wheelDiam * 0.5; // meters
  const double mainMass = 5; // kilos (clodbuster is 3kg)
  
  // the top part of the chassi
  const double topLength = 0.6 * mainLength; // meters
  const double topWidth = 0.9 * mainWidth;   // meters 
  const double topHeight = 1.0;              // meters
  const double topMass = 2; // kilos
    
  // Create the main body of the robot
  this->body = new Body( this->world );

  // the main part of the chassi
  this->bodyGeoms[0] = new BoxGeom( this->modelSpaceId, 
                                    mainLength, mainWidth, mainHeight);
  this->bodyGeoms[0]->SetRelativePosition( GzVectorSet(0, 0, 0) );
  this->bodyGeoms[0]->SetMass( mainMass );
  this->bodyGeoms[0]->SetColor( GzColor(0.9, 0.9, 0.9) ); // almost pure white
  this->body->AttachGeom( this->bodyGeoms[0] ); 

  // the top part of the chassi:
  this->bodyGeoms[1] = new BoxGeom( this->modelSpaceId, 
                                    topLength, topWidth, topHeight); 
  this->bodyGeoms[1]->SetRelativePosition(GzVectorSet(-0.2*mainLength, 0, 0.5*topHeight));
  this->bodyGeoms[1]->SetMass( topMass );
  this->bodyGeoms[1]->SetColor( GzColor(0.9, 0.9, 0.9) ); // almost pure white 
  this->body->AttachGeom( this->bodyGeoms[1] );

  this->AddBody( this->body, true );
   
  // Create the tires
  for (i = 0; i < 4; i++)
  {
    this->tires[i] = new Body( this->world );
    geom = new WheelGeom( this->modelSpaceId, 0.5 * this->wheelDiam, 0.5 * wheelThick);
    geom->SetRelativePosition(GzVectorSet(0, 0, 0));
    geom->SetMass( wheelMass * 0.5 );
    geom->SetColor( GzColor(0.3, 0.3, 0.3) ); // dark grey
    this->tires[i]->AttachGeom( geom );

    geom = new BoxGeom( this->modelSpaceId, 0.5 * this->wheelDiam, 
                        0.5 * this->wheelDiam, 0.02);
    geom->SetRelativePosition(GzVectorSet(0, 0, wheelThick / 2));
    geom->SetMass( wheelMass * 0.45 );
    geom->SetColor( GzColor(1.0, 1.0, 1.0) ); // white
    if(i==0||i==1)
      geom->SetFriction1(0.1);
    this->tires[i]->AttachGeom( geom );

    this->AddBody( this->tires[i] );
  }

  // position the tires:
  this->tires[0]->SetPosition(
      GzVectorSet(0.4*mainLength, +0.5*mainWidth+.03, -0.5*mainHeight));
  this->tires[1]->SetPosition(
      GzVectorSet(0.4*mainLength, -0.5*mainWidth-.03, -0.5*mainHeight));
  this->tires[2]->SetPosition(
      GzVectorSet(-0.4*mainLength, +0.5*mainWidth+.03, -0.5*mainHeight));
  this->tires[3]->SetPosition(
      GzVectorSet(-0.4*mainLength, -0.5*mainWidth-.03, -0.5*mainHeight));
  
  this->tires[0]->SetRotation(GzQuaternFromAxis(1, 0, 0, -M_PI / 2));
  this->tires[1]->SetRotation(GzQuaternFromAxis(1, 0, 0, +M_PI / 2));
  this->tires[2]->SetRotation(GzQuaternFromAxis(1, 0, 0, -M_PI / 2));
  this->tires[3]->SetRotation(GzQuaternFromAxis(1, 0, 0, +M_PI / 2));
  
  // create the black rubber tires
  for (i = 0; i < 4; i++)
  {
    this->stubs[i] = new Body( this->world );

    geom = new SphereGeom( this->modelSpaceId, 0.01);

    geom->SetRelativePosition(GzVectorSet(0, 0, 0));
    geom->SetMass( wheelMass * 0.05 ); // consists mostly of air :)
    geom->SetColor( GzColor(0.1, 0.1, 0.1) ); // almost black
    this->stubs[i]->AttachGeom( geom );
    this->AddBody(this->stubs[i]);  
  }

  this->stubs[0]->SetPosition(
    GzVectorSet(0.4*mainLength, +0.5*mainWidth-.03, -0.1));
  this->stubs[1]->SetPosition(
    GzVectorSet(0.4*mainLength, -0.5*mainWidth+.03, -0.1));
  this->stubs[2]->SetPosition(
    GzVectorSet(-0.4*mainLength, +0.5*mainWidth-.03, -0.1));
  this->stubs[3]->SetPosition(
    GzVectorSet(-0.4*mainLength, -0.5*mainWidth+.03, -0.1));

  // Attach the tires to the body
  for (i = 0; i < 4; i++)
  {
    this->sjoints[i] = new HingeJoint( this->world->worldId );
    this->sjoints[i]->Attach(this->stubs[i], this->body );
    GzVector ab = stubs[i]->GetPosition();
    this->sjoints[i]->SetAnchor( ab.x, ab.y, ab.z );
    this->sjoints[i]->SetAxis(0,0,1);

    this->wjoints[i] = new HingeJoint( this->world->worldId );
    this->wjoints[i]->Attach(this->tires[i],this->stubs[i]);
    GzVector a = tires[i]->GetPosition();
    this->wjoints[i]->SetAnchor( a.x, a.y, a.z );
    this->wjoints[i]->SetAxis(0,1,0);

    //   this->joints[i]->SetParam( dParamSuspensionERP, 0.2 ); // 0.4
    //  this->joints[i]->SetParam( dParamSuspensionCFM, 0.8 ); //0.8
      
    this->sjoints[i]->SetParam( dParamLoStop,-M_PI/6);
    this->sjoints[i]->SetParam( dParamHiStop,+M_PI/6);
  }
  
  // fix rear wheels for now
  this->sjoints[2]->SetParam( dParamLoStop,0);
  this->sjoints[2]->SetParam( dParamHiStop,0);
  this->sjoints[3]->SetParam( dParamLoStop,0);
  this->sjoints[3]->SetParam( dParamHiStop,0);

  this->body->SetFiniteRotationMode(1);  
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Initialize ODE
int ClodBuster::OdeInit( WorldFile *file, WorldFileNode *node )
{
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Finalize the model
int ClodBuster::Fini()
{
  // Finalize external interface  
  this->IfaceFini();

  // Finalize ODE objects
  this->OdeFini();

  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Finalize ODE
int ClodBuster::OdeFini()
{
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Update model
void ClodBuster::Update( double step )
{
  // Get commands from the external interface
  this->IfaceGetCmd();
   
  double length = 4.62;
  double v = this->wheelSpeed[0];// + this->wheelSpeed[1] * this->wheelSep;
;
  double rTurn;
  // Radius of the Turn
  if(this->wheelSpeed[1]==0.0 || this->wheelSpeed[0]==0.0)
    rTurn=RBIG;
  else
    rTurn= this->wheelSpeed[0]/this->wheelSpeed[1];

  if(fabs(rTurn)>RBIG)
    rTurn=fabs(rTurn)*RBIG/rTurn; //ftb
 
  if(fabs(rTurn) < RMIN)
    rTurn=fabs(rTurn)*RMIN/rTurn;
  
  double d=this->wheelSep/2.0;
  double steerR=atan(length/(rTurn-d));
  double steerL=atan(length/(rTurn+d));

  double vLB = v* ( rTurn-d)/(rTurn);
  double vRB=  v* ( rTurn+d)/(rTurn);
  double vLF = v* sqrt(( rTurn-d)*(rTurn-d)+length*length)/fabs(rTurn);
  double vRF = v* sqrt(( rTurn+d)*(rTurn+d)+length*length)/fabs(rTurn);
  
  //printf("vr= %f va= %f , rTurn=%f w0=%f w1=%f\n",this->wheelSpeed[0],this->wheelSpeed[1],rTurn,w0,w1);
 
   double angleL = this->sjoints[0]->GetAngle();
   double angleR = this->sjoints[1]->GetAngle();
   //printf("steerL=%f angleL=%f steerR=%f angleR=%f\n",steerL,angleL,steerR,angleR);
  
  this->sjoints[0]->SetParam(dParamVel,kSteer*(steerL-angleL));
  this->sjoints[1]->SetParam(dParamVel,kSteer*(steerR-angleR));

  this->sjoints[2]->SetParam(dParamVel, 0);
  this->sjoints[3]->SetParam(dParamVel, 0);

  this->sjoints[0]->SetParam( dParamFMax, 1.1 ); 
  this->sjoints[1]->SetParam( dParamFMax, 1.1 ); 
  this->sjoints[2]->SetParam( dParamFMax, 1.8 ); 
  this->sjoints[3]->SetParam( dParamFMax, 1.8 );

  this->wjoints[0]->SetParam( dParamVel,vLF / this->wheelDiam);
  this->wjoints[1]->SetParam( dParamVel,vRF / this->wheelDiam);

  this->wjoints[2]->SetParam( dParamVel,vLB / this->wheelDiam); 
  this->wjoints[3]->SetParam( dParamVel,vRB / this->wheelDiam); 
 
  //printf("angle rates: axis1 %f %f",this->joints[0]->GetAngle1Rate(),this->joints[2]->GetAngle1Rate());
 
  //printf(" axis2 %f %f\n",this->joints[0]->GetAngle2Rate(),this->joints[3]->GetAngle2Rate());
  this->wjoints[0]->SetParam( dParamFMax, 1.1 ); //1.1
  this->wjoints[1]->SetParam( dParamFMax, 1.1 ); // 1.1
  this->wjoints[2]->SetParam( dParamFMax, 1.1 ); // 1.1
  this->wjoints[3]->SetParam( dParamFMax, 1.1 ); // 1.1
  
  // Update the odometry
  this->UpdateOdometry( step );

  // Update the interface
  this->IfacePutData();

  return;
}


//////////////////////////////////////////////////////////////////////////////
// Update the odometry

void ClodBuster::UpdateOdometry( double step )
{
  double wd, ws;
  double d1, d2;
  double dr, da;

  wd = this->wheelDiam;
  ws = this->wheelSep;

  // Average distance travelled by left and right wheels
  //d1 = step * wd * (joints[0]->GetAngle2Rate() + joints[2]->GetAngle2Rate()) / 2;
  //d2 = step * wd * (joints[1]->GetAngle2Rate() + joints[3]->GetAngle2Rate()) / 2;
  d1 = step * wd * wjoints[2]->GetAngleRate() / 2;
  d2 = step * wd * wjoints[3]->GetAngleRate() / 2;

  dr = (d1 + d2) / 2;
  da = (d1-d2)/2*ws;  
  // Compute odometric pose
  this->odomPose[0] += dr * cos( this->odomPose[2] );
  this->odomPose[1] += dr * sin( this->odomPose[2] );
  this->odomPose[2] += da;
 
  //update encoder counts
  this->encoders[0] += d1*408/M_PI/wd;
  this->encoders[1] += d2*408/M_PI/wd;

  /*
    GzVector pos;
    
    pos=this->body->GetRelPointPos(0,0,0);
    //  GzVector p0,p1;
    //  p0=this->body->GetRelPointPos(0,0,0);
    //  p1=this->body->GetRelPointPos(1,0,0);
    
    GzQuatern angles=this->body->GetRotation();
    double r,p,y;
    GzQuaternToEuler(&r,&p,&y,angles);
    
    // if(y<0)
    //  y=y+2*M_PI;
    // printf(" dy=%f dx=%f\n",(p1.y-p0.y),(p1.x-p0.x));
    
    this->odomPose[0] =pos.x;
    this->odomPose[1] =pos.y;
    this->odomPose[2] =y; //rad? yep
    //(180/M_PI)*(atan2((p1.y-p0.y),(p1.x-p0.x)));
    //printf("yaw = %f, %f\n",(180/M_PI)*y,this->odomPose[2]);
  */
  return;
}


// Initialize the external interface
int ClodBuster::IfaceInit()
{
  this->iface = gz_position_alloc();
  assert(this->iface);

  if (gz_position_create(this->iface, this->world->gz_server, this->GetId()) != 0)
    return -1;
  
  return 0;
}


// Finalize the external interface
int ClodBuster::IfaceFini()
{
  gz_position_destroy( this->iface );
  gz_position_free( this->iface );
  this->iface = NULL;
  
  return 0;
}


// Get commands from the external interface
void ClodBuster::IfaceGetCmd()
{
  double vr, va;

  vr = this->iface->data->cmd_vel_pos[0];
  va = this->iface->data->cmd_vel_rot[2];

  // printf("vr=%f va=%f \n",vr,va);  
  this->wheelSpeed[0] = vr ;
  this->wheelSpeed[1] = va;
  
  return;
}

// Update the data in the erinterface
void ClodBuster::IfacePutData()
{
  // Data timestamp
  this->iface->data->time = this->world->GetSimTime();
  if(this->raw_encoder_position)
  {
    this->iface->data->pos[0] = this->encoders[0]*1e-3;
    this->iface->data->pos[1] = this->encoders[1]*1e-3;
    this->iface->data->rot[2] = NAN;
  }
  else 
  {
    this->iface->data->pos[0] = ( this->odomPose[0]);
    this->iface->data->pos[1] = ( this->odomPose[1]);
    this->iface->data->rot[2] = ( this->odomPose[2]);
    // printf("CLOD DUMP %f,%f,%f\n",this->odomPose[0],this->odomPose[1],this->odomPose[2]);
  }
  return;
}


