
/*
        Modified version 
        2002/11/11 David Camhy

        New functions:
        void gnuplot_savetops(gnuplot_ctrl *handle, char *filename); 
        void gnuplot_showonscreen(gnuplot_ctrl *handle);
        void gnuplot_plotrange_x( gnuplot_ctrl * handle, double * d,int from, int to, char * title);
        void gnuplot_plotrange_xy( gnuplot_ctrl * handle, double * x, double * y, int  from, int to, char * title);
        void gnuplot_set_grid(gnuplot_ctrl *h);
        void gnuplot_set_nogrid(gnuplot_ctrl *h);
        void gnuplot_set_xrange(gnuplot_ctrl *g, double from, double to);
        void gnuplot_set_yrange(gnuplot_ctrl *g, double from, double to);
        void gnuplot_set_zrange(gnuplot_ctrl *g, double from, double to);
        void gnuplot_set_title(gnuplot_ctrl * h, char * label);
        void gnuplot_set_zlabel(gnuplot_ctrl * h, char * label);
        void gnuplot_plot_xyz(gnuplot_ctrl * handle,double * x, double * y, double*z, int n, char * title);
        void gnuplot_plotrange_xyz(gnuplot_ctrl * handle,double * x, double * y, double*z, int from, int to, char * title);
        void gnuplot_surf(gnuplot_ctrl *handle, double **z, double xdim, double ydim,char *title);
        void gnuplot_surfrange(gnuplot_ctrl *handle, double **z, double xfrom, double xto, double yfrom, double yto, char*title);

 */

/*-------------------------------------------------------------------------*/
/**
  @file		gnuplot_i.c
  @author	N. Devillard
  @date	Sep 1998
  @version	$Revision: 2.6 $
  @brief	C interface to gnuplot.

  gnuplot is a freely available, command-driven graphical display tool for
  Unix. It compiles and works quite well on a number of Unix flavours as
  well as other operating systems. The following module enables sending
  display requests to gnuplot through simple C calls.
  
*/

/*--------------------------------------------------------------------------*/

/*
	$Id: gnuplot_i.c,v 2.6 2001/10/23 12:38:28 ndevilla Exp $
	$Author: ndevilla $
	$Date: 2001/10/23 12:38:28 $
	$Revision: 2.6 $
 */



/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/

#include "gnuplot_i.h"

/*---------------------------------------------------------------------------
                                Defines
 ---------------------------------------------------------------------------*/

/** Maximal size of a gnuplot command */
#define GP_CMD_SIZE     	2048
/** Maximal size of a plot title */
#define GP_TITLE_SIZE   	80
/** Maximal size for an equation */
#define GP_EQ_SIZE      	512
/** Maximal size of a name in the PATH */
#define PATH_MAXNAMESZ       4096

/** Define P_tmpdir if not defined (this is normally a POSIX symbol) */
#ifndef P_tmpdir
#define P_tmpdir "."
#endif

/*---------------------------------------------------------------------------
                            Function codes
 ---------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------*/
/**
  @brief	Find out where a command lives in your PATH.
  @param	pname Name of the program to look for.
  @return	pointer to statically allocated character string.

  This is the C equivalent to the 'which' command in Unix. It parses
  out your PATH environment variable to find out where a command
  lives. The returned character string is statically allocated within
  this function, i.e. there is no need to free it. Beware that the
  contents of this string will change from one call to the next,
  though (as all static variables in a function).

  The input character string must be the name of a command without
  prefixing path of any kind, i.e. only the command name. The returned
  string is the path in which a command matching the same name was
  found.

  Examples (assuming there is a prog named 'hello' in the cwd):

  @verbatim
  gnuplot_get_program_path("hello") returns "."
  gnuplot_get_program_path("ls") returns "/bin"
  gnuplot_get_program_path("csh") returns "/usr/bin"
  gnuplot_get_program_path("/bin/ls") returns NULL
  @endverbatim
  
 */
/*-------------------------------------------------------------------------*/
char * gnuplot_get_program_path(char * pname)
{
    int         i, j, lg;
    char    *   path;
    static char buf[PATH_MAXNAMESZ];

    /* Trivial case: try in CWD */
    sprintf(buf, "./%s", pname);
    if (access(buf, X_OK)==0) {
        sprintf(buf, ".");
        return buf;
    }
    /* Try out in all paths given in the PATH variable */
    buf[0] = 0;
    path = getenv("PATH");
    if (path!=NULL) {
        for (i=0; path[i]; ) {
            for (j=i; (path[j]) && (path[j]!=':'); j++);
            lg = j - i;
            strncpy(buf, path + i, lg);
            if (lg == 0) buf[lg++] = '.';
            buf[lg++] = '/';
            strcpy(buf + lg, pname);
            if (access(buf, X_OK) == 0) {
                /* Found it! */
                break;
            }
            buf[0] = 0;
            i = j;
            if (path[i] == ':') i++;
        }
    } else {
		fprintf(stderr, "PATH variable not set\n");
	}
    /* If the buffer is still empty, the command was not found */
    if (buf[0] == 0) return NULL;
    /* Otherwise truncate the command name to yield path only */
    lg = strlen(buf) - 1;
    while (buf[lg]!='/') {
        buf[lg]=0;
        lg --;
    }
    buf[lg] = 0;
    return buf;
}



/*-------------------------------------------------------------------------*/
/**
  @brief	Opens up a gnuplot session, ready to receive commands.
  @return	Newly allocated gnuplot control structure.

  This opens up a new gnuplot session, ready for input. The struct
  controlling a gnuplot session should remain opaque and only be
  accessed through the provided functions.

  The session must be closed using gnuplot_close().
 */
/*--------------------------------------------------------------------------*/

gnuplot_ctrl * gnuplot_init(void)
{
    gnuplot_ctrl *  handle;

    if (getenv("DISPLAY") == NULL) {
        fprintf(stderr, "cannot find DISPLAY variable: is it set?\n");
    }
	if (gnuplot_get_program_path("gnuplot")==NULL) {
		fprintf(stderr, "cannot find gnuplot in your PATH");
		return NULL;
	}

    /* 
     * Structure initialization:
     */
    handle = (gnuplot_ctrl*)malloc(sizeof(gnuplot_ctrl));
    handle->nplots = 0;
    gnuplot_setstyle(handle, "points");
    handle->ntmp = 0;

    handle->gnucmd = popen("gnuplot -noraise", "w");
    if (handle->gnucmd == NULL) {
        fprintf(stderr, "error starting gnuplot\n");
        free(handle);
        return NULL;
    }
    return handle;
}
/*-------------------------------------------------------------------------*/
/**
  @brief	Sets terminal type to x11
  @param	handle Gnuplot session control handle.
  @return	void
 */

void gnuplot_showonscreen(gnuplot_ctrl *handle) {
  gnuplot_cmd(handle,"set terminal x11");
}
/*-------------------------------------------------------------------------*/
/**
  @brief	Saves a gnuplot session to a postscript file
  @param	handle Gnuplot session control handle.
  @param        filename
  @return	void
 */

void gnuplot_savetops(gnuplot_ctrl *handle, char * filename) {
  char    command[GP_CMD_SIZE];
  char    command2[GP_CMD_SIZE];

  sprintf(command,"set terminal postscript color");
  sprintf(command2,"set output \"%s\"",filename);
  gnuplot_cmd(handle, command);
  gnuplot_cmd(handle, command2);
}
/*--------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------*/
/**
  @brief	Closes a gnuplot session previously opened by gnuplot_init()
  @param	handle Gnuplot session control handle.
  @return	void

  Kills the child PID and deletes all opened temporary files.
  It is mandatory to call this function to close the handle, otherwise
  temporary files are not cleaned and child process might survive.

 */
/*--------------------------------------------------------------------------*/

void gnuplot_close(gnuplot_ctrl * handle)
{
    int     i;
	
    if (pclose(handle->gnucmd) == -1) {
        fprintf(stderr, "problem closing communication to gnuplot\n");
        return;
    }
    if (handle->ntmp) {
        for (i=0; i<handle->ntmp; i++) {
            remove(handle->to_delete[i]);
        }
    }
    free(handle);
    return;
}


/*-------------------------------------------------------------------------*/
/**
  @brief	Sends a command to an active gnuplot session.
  @param	handle Gnuplot session control handle
  @param	cmd    Command to send, same as a printf statement.

  This sends a string to an active gnuplot session, to be executed.
  There is strictly no way to know if the command has been
  successfully executed or not.
  The command syntax is the same as printf.

  Examples:

  @code
  gnuplot_cmd(g, "plot %d*x", 23.0);
  gnuplot_cmd(g, "plot %g * cos(%g * x)", 32.0, -3.0);
  @endcode

  Since the communication to the gnuplot process is run through
  a standard Unix pipe, it is only unidirectional. This means that
  it is not possible for this interface to query an error status
  back from gnuplot.
 */
/*--------------------------------------------------------------------------*/

void gnuplot_cmd(gnuplot_ctrl *  handle, char *  cmd, ...)
{
    va_list ap;
    char    local_cmd[GP_CMD_SIZE];

    va_start(ap, cmd);
    vsprintf(local_cmd, cmd, ap);
    va_end(ap);

    strcat(local_cmd, "\n");

    fputs(local_cmd, handle->gnucmd);
    fflush(handle->gnucmd);
    return;
}


/*-------------------------------------------------------------------------*/
/**
  @brief	Change the plotting style of a gnuplot session.
  @param	h Gnuplot session control handle
  @param	plot_style Plotting-style to use (character string)
  @return	void

  The provided plotting style is a character string. It must be one of
  the following:

  - lines
  - points
  - linespoints
  - impulses
  - dots
  - steps
  - errorbars
  - boxes
  - boxeserrorbars
 */
/*--------------------------------------------------------------------------*/

void gnuplot_setstyle(gnuplot_ctrl * h, char * plot_style) 
{
    if (strcmp(plot_style, "lines") &&
        strcmp(plot_style, "points") &&
        strcmp(plot_style, "linespoints") &&
        strcmp(plot_style, "impulses") &&
        strcmp(plot_style, "dots") &&
        strcmp(plot_style, "steps") &&
        strcmp(plot_style, "errorbars") &&
        strcmp(plot_style, "boxes") &&
        strcmp(plot_style, "boxerrorbars")) {
        fprintf(stderr, "warning: unknown requested style: using points\n");
        strcpy(h->pstyle, "points");
    } else {
        strcpy(h->pstyle, plot_style);
    }
    return;
}

/*-------------------------------------------------------------------------*/
/**
  @brief	Turns grid on
  @param	h Gnuplot session control handle.
  @return	void

  Sets grid
 */
/*--------------------------------------------------------------------------*/

void gnuplot_set_grid(gnuplot_ctrl * h)
{
    char    cmd[GP_CMD_SIZE];

    sprintf(cmd, "set grid");
    gnuplot_cmd(h, cmd);
    return;
}

/*-------------------------------------------------------------------------*/
/**
  @brief	Turns grid off
  @param	h Gnuplot session control handle.
  @return	void

  Sets grid
 */
/*--------------------------------------------------------------------------*/

void gnuplot_set_nogrid(gnuplot_ctrl * h)
{
    char    cmd[GP_CMD_SIZE];

    sprintf(cmd, "set nogrid");
    gnuplot_cmd(h, cmd);
    return;
}
/*-------------------------------------------------------------------------*/
/**
  @brief	Sets the x label of a gnuplot session.
  @param	h Gnuplot session control handle.
  @param	label Character string to use for X label.
  @return	void

  Sets the x label for a gnuplot session.
 */
/*--------------------------------------------------------------------------*/

void gnuplot_set_xlabel(gnuplot_ctrl * h, char * label)
{
    char    cmd[GP_CMD_SIZE];

    sprintf(cmd, "set xlabel \"%s\"", label);
    gnuplot_cmd(h, cmd);
    return;
}

/*-------------------------------------------------------------------------*/
/**
  @brief	Sets the x range of a gnuplot session.
  @param	h Gnuplot session control handle.
  @param	from Start
  @param        to End
  @return	void

  Sets the x range for a gnuplot session.
 */
/*--------------------------------------------------------------------------*/

void gnuplot_set_xrange(gnuplot_ctrl * h, double from, double to)
{
    char    cmd[GP_CMD_SIZE];

    sprintf(cmd, "set xrange [%f:%f]", from, to);
    gnuplot_cmd(h, cmd);
    return;
}
/*-------------------------------------------------------------------------*/
/**
  @brief	Sets the y range of a gnuplot session.
  @param	h Gnuplot session control handle.
  @param	from Start
  @param        to End
  @return	void

  Sets the y range for a gnuplot session.
 */
/*--------------------------------------------------------------------------*/

void gnuplot_set_yrange(gnuplot_ctrl * h, double from, double to)
{
    char    cmd[GP_CMD_SIZE];

    sprintf(cmd, "set yrange [%f:%f]", from, to);
    gnuplot_cmd(h, cmd);
    return;
}
/*-------------------------------------------------------------------------*/
/**
  @brief	Sets the z range of a gnuplot session.
  @param	h Gnuplot session control handle.
  @param	from Start
  @param        to End
  @return	void

  Sets the z range for a gnuplot session.
 */
/*--------------------------------------------------------------------------*/

void gnuplot_set_zrange(gnuplot_ctrl * h, double from, double to)
{
    char    cmd[GP_CMD_SIZE];

    sprintf(cmd, "set zrange [%f:%f]", from, to);
    gnuplot_cmd(h, cmd);
    return;
}
/*-------------------------------------------------------------------------*/
/**
  @brief	Sets the y label of a gnuplot session.
  @param	h Gnuplot session control handle.
  @param	label Character string to use for Y label.
  @return	void

  Sets the y label for a gnuplot session.
 */
/*--------------------------------------------------------------------------*/

void gnuplot_set_ylabel(gnuplot_ctrl * h, char * label)
{
    char    cmd[GP_CMD_SIZE];

    sprintf(cmd, "set ylabel \"%s\"", label);
    gnuplot_cmd(h, cmd);
    return;
}

/*-------------------------------------------------------------------------*/
/**
  @brief	Sets the z label of a gnuplot session.
  @param	h Gnuplot session control handle.
  @param	label Character string to use for Z label.
  @return	void

  Sets the z label for a gnuplot session.
 */
/*--------------------------------------------------------------------------*/

void gnuplot_set_zlabel(gnuplot_ctrl * h, char * label)
{
    char    cmd[GP_CMD_SIZE];

    sprintf(cmd, "set zlabel \"%s\"", label);
    gnuplot_cmd(h, cmd);
    return;
}
/*-------------------------------------------------------------------------*/
/**
  @brief	Sets the title of a gnuplot session.
  @param	h Gnuplot session control handle.
  @param	label Character string to use for title.
  @return	void

  Sets the title for a gnuplot session.
 */
/*--------------------------------------------------------------------------*/

void gnuplot_set_title(gnuplot_ctrl * h, char * label)
{
    char    cmd[GP_CMD_SIZE];

    sprintf(cmd, "set title \"%s\"", label);
    gnuplot_cmd(h, cmd);
    return;
}

/*-------------------------------------------------------------------------*/
/**
  @brief	Resets a gnuplot session (next plot will erase previous ones).
  @param	h Gnuplot session control handle.
  @return	void

  Resets a gnuplot session, i.e. the next plot will erase all previous
  ones.
 */
/*--------------------------------------------------------------------------*/

void gnuplot_resetplot(gnuplot_ctrl * h)
{
    int     i;
    if (h->ntmp) {
        for (i=0; i<h->ntmp; i++) {
            remove(h->to_delete[i]);
        }
    }
    h->ntmp = 0;
    h->nplots = 0;
    return;
}

/*-------------------------------------------------------------------------*/
/**
  @brief	Plots a 3d graph from a list of doubles.
  @param	handle	Gnuplot session control handle.
  @param	x		Array of doubles.
  @param        y               Array of doubles.
  @param        z               Array of doubles.
  @param	n		Number of values in the passed arrays.
  @param	title	Title of the plot.
  @return	void

  Plots out a 3d graph from a lists of doubles.

 */
/*--------------------------------------------------------------------------*/
void gnuplot_plot_xyz(
    gnuplot_ctrl    *   handle,
    double	    *	x,
    double	    *	y,
    double          *   z,
    int                 n,
    char            *   title
)
{
    int     i;
    int	    tmpfd;
    char    name[128];
    char    cmd[2048];
    char    line[2048];

    if (handle==NULL || x==NULL || y==NULL || (n<1)) return;
    gnuplot_cmd(handle,"set parametric");
    /* Open one more temporary file? */
    if (handle->ntmp == GP_MAX_TMP_FILES - 1) {
        fprintf(stderr,
                "maximum # of temporary files reached (%d): cannot open more",
                GP_MAX_TMP_FILES);
        return;
    }

    /* Open temporary file for output   */
	sprintf(name, "%s/gnuplot-i-XXXXXX", P_tmpdir);
    if ((tmpfd=mkstemp(name))==-1) {
        fprintf(stderr,"cannot create temporary file: exiting plot");
        return;
    }
    /* Store file name in array for future deletion */
    strcpy(handle->to_delete[handle->ntmp], name);
    handle->ntmp ++;

    /* Write data to this file  */
    for (i=0; i<n; i++) {
        sprintf(line, "%g %g %g\n", x[i], y[i], z[i]);
		write(tmpfd, line, strlen(line));
    }
    close(tmpfd);
  
    /* Command to be sent to gnuplot    */
    if (handle->nplots > 0) {
        strcpy(cmd, "replot");
    } else {
        strcpy(cmd, "splot");
    }
    
    if (title == NULL) {
        sprintf(line, "%s \"%s\" with %s", cmd, name, handle->pstyle);
    } else {
        sprintf(line, "%s \"%s\" title \"%s\" with %s", cmd, name,
                      title, handle->pstyle);
    }

    /* send command to gnuplot  */
    gnuplot_cmd(handle, line);
    handle->nplots++;
    return;
}
/*-------------------------------------------------------------------------*/
/**
  @brief	Plots a surface plot from a matrix of doubles.
  @param	handle	Gnuplot session control handle.
  @param	z		Matrix
  @param        xdim            first dimension
  @param        ydim            second dimension
  @param	title	Title of the plot.
  @return	void

  Plots out a surface plot from a matrix of doubles.

 */
/*--------------------------------------------------------------------------*/
void gnuplot_surf(gnuplot_ctrl *handle, double **z, int xdim, int ydim,char *title) 
{
    int     i;
    int     j;
    int	    tmpfd;
    char    name[128];
    char    cmd[2048];
    char    line[20048];

    if (handle==NULL || z==NULL || (xdim<1) || (ydim<1)) return;
    //gnuplot_cmd(handle,"set nodgrid3d");
    /* Open one more temporary file? */
    if (handle->ntmp == GP_MAX_TMP_FILES - 1) {
        fprintf(stderr,
                "maximum # of temporary files reached (%d): cannot open more",
                GP_MAX_TMP_FILES);
        return;
    }

    /* Open temporary file for output   */
	sprintf(name, "%s/gnuplot-i-XXXXXX", P_tmpdir);
    if ((tmpfd=mkstemp(name))==-1) {
        fprintf(stderr,"cannot create temporary file: exiting plot");
        return;
    }
    /* Store file name in array for future deletion */
    strcpy(handle->to_delete[handle->ntmp], name);
    handle->ntmp ++;

    /* Write data to this file  */
    for (i=0; i<xdim; i++) 
    {
      sprintf(line,"");
      for (j=0; j<ydim; j++) 
      {
          sprintf(line, "%s %g ",line, z[i][j]);
      }

      sprintf(line,"%s\n",line);
      write(tmpfd, line, strlen(line));
    }

    close(tmpfd);

    /* Command to be sent to gnuplot    */
    if (handle->nplots > 0) {
        strcpy(cmd, "replot");
    } else {
        strcpy(cmd, "splot");
    }
   
    if (title == NULL) {
        sprintf(line, "%s \"%s\" matrix with %s", cmd, name, handle->pstyle);
    } else {
        sprintf(line, "%s \"%s\" matrix title \"%s\" with %s", cmd, name,
                      title, handle->pstyle);
    }

    /* send command to gnuplot  */
    gnuplot_cmd(handle, line);
    handle->nplots++;
    return;
}
/*-------------------------------------------------------------------------*/
/**
  @brief	Plots a surface plot from a matrix of doubles.
  @param	handle	Gnuplot session control handle.
  @param	z		Matrix
  @param        xfrom           first dimension start index
  @param        xto             first dimension end index
  @param        yfrom           second dimension start index
  @param        yto             second dimension end index
  @param	title	Title of the plot.
  @return	void

  Plots out a surface plot from a matrix of doubles.

 */
/*----------------------------------------------------------------------------*/
void gnuplot_surfrange(gnuplot_ctrl *handle, double *z[], 
                       int xfrom, int xto, 
                       int yfrom, int yto, char *title) 
{
    int   i,j;  
    char  name[128];
    char  cmd[2048];
    /* Changed to slightly speed things up. -LBC */
    char  line[20048];
    char  elem[7];
    /* Changed to significantly speed things up. -LBC */
    FILE* tmpfd;
    char* fifoname;      
    
    if (handle==NULL || z==NULL || (xto<1) || (yto<1)) return;
  
    /* Open one more temporary file? */
    if (handle->ntmp == GP_MAX_TMP_FILES - 1) {
        fprintf(stderr,
                "maximum # of temporary files reached (%d): cannot open more",
                GP_MAX_TMP_FILES);
        return;
    }

    /* Open temporary file for output   */
    sprintf(name, "%s/gnuplot-i-XXXXXX", P_tmpdir);

    /* Make it a named pipe instead of a real file. -LBC */
    if((fifoname = mktemp(name)) == NULL) 
    {
      fprintf(stderr,"cannot create temporary file: exiting plot");
      return;
    }
    if(mkfifo(fifoname, 0666) != 0)
    {
      fprintf(stderr,"mkfifo creation failed: exiting plot");
      return;
    }
    /* Open temp fifo for writing. */
    tmpfd = fopen( fifoname, "w+" );

    /* Store file name in array for future deletion */
    strcpy(handle->to_delete[handle->ntmp], name);
    handle->ntmp ++;

    /* Command to be sent to gnuplot */
    if (handle->nplots > 0) {
        strcpy(cmd, "replot");
    } else {
        strcpy(cmd, "splot");
    }
   
    if (title == NULL) {
        sprintf(line, "%s \"%s\" matrix with %s", 
                      cmd, fifoname, handle->pstyle);
    } else {
        sprintf(line, "%s \"%s\" matrix title \"%s\" with %s", 
                      cmd, fifoname, title, handle->pstyle);
    }

    /* send command to gnuplot  */
    gnuplot_cmd(handle, line);

    /* Write data to this file  */
    for (i=xfrom; i<=xto; i++) 
    {
      sprintf(line,"");
      for (j=yfrom; j<=yto; j++) 
      {
        sprintf( elem, "%6.3f ", z[i][j] );
        strcat( line, elem );
      }
      strcat( line, "\n" );
      
      fprintf(tmpfd, "%s", line);
      fflush(tmpfd);
    }

    fclose(tmpfd);
    
    handle->nplots++;
    return;
}


/*-------------------------------------------------------------------------*/
/**
  @brief	Plots a 3d graph from a list of doubles.
  @param	handle	Gnuplot session control handle.
  @param	x		Array of doubles.
  @param        y               Array of doubles.
  @param        z               Array of doubles.
  @param	from		start index
  @param        to              end index
  @param	title	Title of the plot.
  @return	void

  Plots out a 3d graph from a lists of doubles.

 */
/*--------------------------------------------------------------------------*/
void gnuplot_plotrange_xyz(
    gnuplot_ctrl    *   handle,
    double	    *	x,
    double	    *	y,
    double          *   z,
    int                 from,
    int                 to,
    char            *   title
)
{
    int     i;
    int	    tmpfd;
    char    name[128];
    char    cmd[2048];
    char    line[2048];

    if (handle==NULL || x==NULL || y==NULL || (from<0)||(to<1)) return;
    gnuplot_cmd(handle,"set parametric");
    //gnuplot_cmd(handle,"set nodgrid3d");
    /* Open one more temporary file? */
    if (handle->ntmp == GP_MAX_TMP_FILES - 1) {
        fprintf(stderr,
                "maximum # of temporary files reached (%d): cannot open more",
                GP_MAX_TMP_FILES);
        return;
    }

    /* Open temporary file for output   */
	sprintf(name, "%s/gnuplot-i-XXXXXX", P_tmpdir);
    if ((tmpfd=mkstemp(name))==-1) {
        fprintf(stderr,"cannot create temporary file: exiting plot");
        return;
    }
    /* Store file name in array for future deletion */
    strcpy(handle->to_delete[handle->ntmp], name);
    handle->ntmp ++;

    /* Write data to this file  */
    for (i=from; i<=to; i++) {
        sprintf(line, "%g %g %g\n", x[i], y[i], z[i]);
		write(tmpfd, line, strlen(line));
    }
    close(tmpfd);
  
    /* Command to be sent to gnuplot    */
    if (handle->nplots > 0) {
        strcpy(cmd, "replot");
    } else {
        strcpy(cmd, "splot");
    }
    
    if (title == NULL) {
        sprintf(line, "%s \"%s\" with %s", cmd, name, handle->pstyle);
    } else {
        sprintf(line, "%s \"%s\" title \"%s\" with %s", cmd, name,
                      title, handle->pstyle);
    }

    /* send command to gnuplot  */
    gnuplot_cmd(handle, line);
    handle->nplots++;
    return;
}

/*-------------------------------------------------------------------------*/
/**
  @brief	Plots a 2d graph from a list of doubles.
  @param	handle	Gnuplot session control handle.
  @param	d		Array of doubles.
  @param	n		Number of values in the passed array.
  @param	title	Title of the plot.
  @return	void

  Plots out a 2d graph from a list of doubles. The x-coordinate is the
  index of the double in the list, the y coordinate is the double in
  the list.

  Example:

  @code
    gnuplot_ctrl    *h;
    double          d[50];
    int             i;

    h = gnuplot_init();
    for (i=0; i<50; i++) {
        d[i] = (double)(i*i);
    }
    gnuplot_plot_x(h, d, 50, "parabola");
    sleep(2);
    gnuplot_close(h);
  @endcode
 */
/*--------------------------------------------------------------------------*/

void gnuplot_plot_x(
    gnuplot_ctrl    *   handle,
    double          *   d,
    int                 n,
    char            *   title
)
{
    int     i;
	int		tmpfd;
    char    name[128];
    char    cmd[GP_CMD_SIZE];
    char    line[GP_CMD_SIZE];


	if (handle==NULL || d==NULL || (n<1)) return;

    /* Open one more temporary file? */
    if (handle->ntmp == GP_MAX_TMP_FILES - 1) {
        fprintf(stderr,
                "maximum # of temporary files reached (%d): cannot open more",
                GP_MAX_TMP_FILES);
        return;
    }

    /* Open temporary file for output   */
	sprintf(name, "%s/gnuplot-i-XXXXXX", P_tmpdir);
    if ((tmpfd=mkstemp(name))==-1) {
        fprintf(stderr,"cannot create temporary file: exiting plot");
        return;
    }

    /* Store file name in array for future deletion */
    strcpy(handle->to_delete[handle->ntmp], name);
    handle->ntmp ++;
    /* Write data to this file  */
    for (i=0; i<n; i++) {
		sprintf(line, "%g\n", d[i]);
		write(tmpfd, line, strlen(line));
    }
    close(tmpfd);

    /* Command to be sent to gnuplot    */
    if (handle->nplots > 0) {
        strcpy(cmd, "replot");
    } else {
        strcpy(cmd, "plot");
    }
    
    if (title == NULL) {
        sprintf(line, "%s \"%s\" with %s", cmd, name, handle->pstyle);
    } else {
        sprintf(line, "%s \"%s\" title \"%s\" with %s", cmd, name,
                      title, handle->pstyle);
    }

    /* send command to gnuplot  */
    gnuplot_cmd(handle, line);
    handle->nplots++;
    return;
}

/*-------------------------------------------------------------------------*/
/**
  @brief	Plots a 2d graph from a list of doubles.
  @param	handle	Gnuplot session control handle.
  @param	d		Array of doubles.
  @param        from            Start of array index
  @param	to		End of array index
  @param	title	Title of the plot.
  @return	void

  Plots out a 2d graph from a list of doubles. The x-coordinate is the
  index of the double in the list, the y coordinate is the double in
  the list.

  Example:

  @code
    gnuplot_ctrl    *h;
    double          d[50];
    int             i;

    h = gnuplot_init();
    for (i=0; i<50; i++) {
        d[i] = (double)(i*i);
    }
    gnuplot_plotrange_x(h, d, 10, 40, "index 10 to 40");
    sleep(2);
    gnuplot_close(h);
  @endcode
 */
void gnuplot_plotrange_x(
    gnuplot_ctrl    *   handle,
    double          *   d,
    int                 from,
    int                 to,
    char            *   title
)
{
    int     i;
	int		tmpfd;
    char    name[128];
    char    cmd[GP_CMD_SIZE];
    char    line[GP_CMD_SIZE];


	if (handle==NULL || d==NULL || (to<1) || (from<0)) return;

    /* Open one more temporary file? */
    if (handle->ntmp == GP_MAX_TMP_FILES - 1) {
        fprintf(stderr,
                "maximum # of temporary files reached (%d): cannot open more",
                GP_MAX_TMP_FILES);
        return;
    }

    /* Open temporary file for output   */
	sprintf(name, "%s/gnuplot-i-XXXXXX", P_tmpdir);
    if ((tmpfd=mkstemp(name))==-1) {
        fprintf(stderr,"cannot create temporary file: exiting plot");
        return;
    }

    /* Store file name in array for future deletion */
    strcpy(handle->to_delete[handle->ntmp], name);
    handle->ntmp ++;
    /* Write data to this file  */
    for (i=from; i<=to; i++) {
		sprintf(line, "%g\n", d[i]);
		write(tmpfd, line, strlen(line));
    }
    close(tmpfd);

    /* Command to be sent to gnuplot    */
    if (handle->nplots > 0) {
        strcpy(cmd, "replot");
    } else {
        strcpy(cmd, "plot");
    }
    
    if (title == NULL) {
        sprintf(line, "%s \"%s\" with %s", cmd, name, handle->pstyle);
    } else {
        sprintf(line, "%s \"%s\" title \"%s\" with %s", cmd, name,
                      title, handle->pstyle);
    }

    /* send command to gnuplot  */
    gnuplot_cmd(handle, line);
    handle->nplots++;
    return;
}
/*-------------------------------------------------------------------------*/
/**
  @brief	Plot a 2d graph from a list of points.
  @param	handle		Gnuplot session control handle.
  @param	x			Pointer to a list of x coordinates.
  @param	y			Pointer to a list of y coordinates.
  @param	n			Number of doubles in x (assumed the same as in y).
  @param	title		Title of the plot.
  @return	void

  Plots out a 2d graph from a list of points. Provide points through a list
  of x and a list of y coordinates. Both provided arrays are assumed to
  contain the same number of values.

  @code
    gnuplot_ctrl    *h;
	double			x[50];
	double			y[50];
    int             i;

    h = gnuplot_init();
    for (i=0; i<50; i++) {
        x[i] = (double)(i)/10.0;
        y[i] = x[i] * x[i];
    }
    gnuplot_plot1_xy(h, x, y, 50, "parabola");
    sleep(2);
    gnuplot_close(h);
  @endcode
 */
/*--------------------------------------------------------------------------*/

void gnuplot_plot_xy(
    gnuplot_ctrl    *   handle,
	double			*	x,
	double			*	y,
    int                 n,
    char            *   title
)
{
    int     i;
	int		tmpfd;
    char    name[128];
    char    cmd[GP_CMD_SIZE];
    char    line[GP_CMD_SIZE];

	if (handle==NULL || x==NULL || y==NULL || (n<1)) return;

    /* Open one more temporary file? */
    if (handle->ntmp == GP_MAX_TMP_FILES - 1) {
        fprintf(stderr,
                "maximum # of temporary files reached (%d): cannot open more",
                GP_MAX_TMP_FILES);
        return;
    }

    /* Open temporary file for output   */
	sprintf(name, "%s/gnuplot-i-XXXXXX", P_tmpdir);
    if ((tmpfd=mkstemp(name))==-1) {
        fprintf(stderr,"cannot create temporary file: exiting plot");
        return;
    }
    /* Store file name in array for future deletion */
    strcpy(handle->to_delete[handle->ntmp], name);
    handle->ntmp ++;

    /* Write data to this file  */
    for (i=0; i<n; i++) {
        sprintf(line, "%g %g\n", x[i], y[i]);
		write(tmpfd, line, strlen(line));
    }
    close(tmpfd);

    /* Command to be sent to gnuplot    */
    if (handle->nplots > 0) {
        strcpy(cmd, "replot");
    } else {
        strcpy(cmd, "plot");
    }
    
    if (title == NULL) {
        sprintf(line, "%s \"%s\" with %s", cmd, name, handle->pstyle);
    } else {
        sprintf(line, "%s \"%s\" title \"%s\" with %s", cmd, name,
                      title, handle->pstyle);
    }

    /* send command to gnuplot  */
    gnuplot_cmd(handle, line);
    handle->nplots++;
    return;
}
/*-------------------------------------------------------------------------*/
/**
  @brief	Plot a 2d graph from a list of points.
  @param	handle		Gnuplot session control handle.
  @param	x			Pointer to a list of x coordinates.
  @param	y			Pointer to a list of y coordinates.
  @param	from			Start of array index of x and y coordinates
  @param        to                      End of array index of x and y coordinates
  @param	title		Title of the plot.
  @return	void

  Plots out a 2d graph from a list of points. Provide points through a list
  of x and a list of y coordinates. Both provided arrays are assumed to
  contain the same number of values.

  @code
    gnuplot_ctrl    *h;
	double			x[50];
	double			y[50];
    int             i;

    h = gnuplot_init();
    for (i=0; i<50; i++) {
        x[i] = (double)(i)/10.0;
        y[i] = x[i] * x[i];
    }
    gnuplot_plotrange_xy(h, x, y, 10, 40, "index 10 to 40");
    sleep(2);
    gnuplot_close(h);
  @endcode
 */
/*--------------------------------------------------------------------------*/

void gnuplot_plotrange_xy(
    gnuplot_ctrl    *   handle,
	double			*	x,
	double			*	y,
    int                 from,
    int                 to,
    char            *   title
)
{
    int     i;
	int		tmpfd;
    char    name[128];
    char    cmd[GP_CMD_SIZE];
    char    line[GP_CMD_SIZE];

	if (handle==NULL || x==NULL || y==NULL || (to<1) || (from<0)) return;

    /* Open one more temporary file? */
    if (handle->ntmp == GP_MAX_TMP_FILES - 1) {
        fprintf(stderr,
                "maximum # of temporary files reached (%d): cannot open more",
                GP_MAX_TMP_FILES);
        return;
    }

    /* Open temporary file for output   */
	sprintf(name, "%s/gnuplot-i-XXXXXX", P_tmpdir);
    if ((tmpfd=mkstemp(name))==-1) {
        fprintf(stderr,"cannot create temporary file: exiting plot");
        return;
    }
    /* Store file name in array for future deletion */
    strcpy(handle->to_delete[handle->ntmp], name);
    handle->ntmp ++;

    /* Write data to this file  */
    for (i=from; i<=to; i++) {
        sprintf(line, "%g %g\n", x[i], y[i]);
		write(tmpfd, line, strlen(line));
    }
    close(tmpfd);

    /* Command to be sent to gnuplot    */
    if (handle->nplots > 0) {
        strcpy(cmd, "replot");
    } else {
        strcpy(cmd, "plot");
    }
    
    if (title == NULL) {
        sprintf(line, "%s \"%s\" with %s", cmd, name, handle->pstyle);
    } else {
        sprintf(line, "%s \"%s\" title \"%s\" with %s", cmd, name,
                      title, handle->pstyle);
    }

    /* send command to gnuplot  */
    gnuplot_cmd(handle, line);
    handle->nplots++;
    return;
}


/*-------------------------------------------------------------------------*/
/**
  @brief	Open a new session, plot a signal, close the session.
  @param	title	Plot title
  @param	style	Plot style
  @param	label_x	Label for X
  @param	label_y	Label for Y
  @param	x		Array of X coordinates
  @param	y		Array of Y coordinates (can be NULL)
  @param	n		Number of values in x and y.
  @return

  This function opens a new gnuplot session, plots the provided
  signal as an X or XY signal depending on a provided y, waits for
  a carriage return on stdin and closes the session.

  It is Ok to provide an empty title, empty style, or empty labels for
  X and Y. Defaults are provided in this case.
 */
/*--------------------------------------------------------------------------*/

void gnuplot_plot_once(
	char	*	title,
	char	*	style,
	char	*	label_x,
	char	*	label_y,
	double	*	x,
	double	*	y,
	int			n
)
{
	gnuplot_ctrl	*	handle;

	if (x==NULL || n<1) return;

	handle = gnuplot_init();
	if (style!=NULL) {
		gnuplot_setstyle(handle, style);
	} else {
		gnuplot_setstyle(handle, "lines");
	}
	if (label_x!=NULL) {
		gnuplot_set_xlabel(handle, label_x);
	} else {
		gnuplot_set_xlabel(handle, "X");
	}
	if (label_y!=NULL) {
		gnuplot_set_ylabel(handle, label_y);
	} else {
		gnuplot_set_ylabel(handle, "Y");
	}
	if (y==NULL) {
		gnuplot_plot_x(handle, x, n, title);
	} else {
		gnuplot_plot_xy(handle, x, y, n, title);
	}
	printf("press ENTER to continue\n");
	while (getchar()!='\n') {}
	gnuplot_close(handle);
	return;
}




/*-------------------------------------------------------------------------*/
/**
  @brief	Plot a slope on a gnuplot session.
  @param	handle		Gnuplot session control handle.
  @param	a			Slope.
  @param	b			Intercept.
  @param	title		Title of the plot.
  @return	void
  @doc

  Plot a slope on a gnuplot session. The provided slope has an
  equation of the form y=ax+b

  Example:

  @code
    gnuplot_ctrl    *   h;
    double              a, b;

    h = gnuplot_init();
    gnuplot_plot_slope(h, 1.0, 0.0, "unity slope");
    sleep(2);
    gnuplot_close(h);
  @endcode
 */
/*--------------------------------------------------------------------------*/
    

void gnuplot_plot_slope(
    gnuplot_ctrl    *   handle,
    double              a,
    double              b,
    char            *   title
)
{
    char    stitle[GP_TITLE_SIZE];
    char    cmd[GP_CMD_SIZE];

    if (title == NULL) {
        strcpy(stitle, "no title");
    } else {
        strcpy(stitle, title);
    }

    if (handle->nplots > 0) {
        sprintf(cmd, "replot %g * x + %g title \"%s\" with %s",
                      a, b, title, handle->pstyle);
    } else {
        sprintf(cmd, "plot %g * x + %g title \"%s\" with %s",
                      a, b, title, handle->pstyle);
    }
    gnuplot_cmd(handle, cmd);
    handle->nplots++;
    return;
}


/*-------------------------------------------------------------------------*/
/**
  @brief	Plot a curve of given equation y=f(x).
  @param	h			Gnuplot session control handle.
  @param	equation	Equation to plot.
  @param	title		Title of the plot.
  @return	void

  Plots out a curve of given equation. The general form of the
  equation is y=f(x), you only provide the f(x) side of the equation.

  Example:

  @code
        gnuplot_ctrl    *h;
        char            eq[80];

        h = gnuplot_init();
        strcpy(eq, "sin(x) * cos(2*x)");
        gnuplot_plot_equation(h, eq, "sine wave", normal);
        gnuplot_close(h);
  @endcode
 */
/*--------------------------------------------------------------------------*/

void gnuplot_plot_equation(
    gnuplot_ctrl    *   h,
    char            *   equation,
    char            *   title
)
{
    char    cmd[GP_CMD_SIZE];
    char    plot_str[GP_EQ_SIZE];
    char    title_str[GP_TITLE_SIZE];

    if (title == NULL) {
        strcpy(title_str, "no title");
    } else {
        strcpy(title_str, title);
    }
    if (h->nplots > 0) {
        strcpy(plot_str, "replot");
    } else {
        strcpy(plot_str, "plot");
    }

    sprintf(cmd, "%s %s title \"%s\" with %s", 
                  plot_str, equation, title_str, h->pstyle);
    gnuplot_cmd(h, cmd);
    h->nplots++;
    return;
}

