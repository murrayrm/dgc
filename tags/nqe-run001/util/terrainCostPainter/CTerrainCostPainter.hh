#ifndef __CTERRAINCOSTPAINTER_HH__
#define __CTERRAINCOSTPAINTER_HH__

#include <stdlib.h>
#include <math.h>
#include <fstream>
#include <sstream>

#include "CMap.hh"
#include "CMapPlus.hh"
#include "CElevationFuser.hh"
#include "MapConstants.h"
#include "CCostFuser.hh"
#include "DGCutils"

#define MAX_INPUT_ELEV_LAYERS 7
#define MAX_KERNEL_SIZE 17

#define OPTIONSLIST(_)	\
  _(int, kernelSize, 2)													\
		_(double, stdDevThreshold, 0.15)						\
																								\
		_(bool, paintInsideRDDFOnly, 1)							\
																								\
		_(int, minNumSupportingCells, 3)						\
		_(int, veloGenAlgNum, 3)										\
																								\
		_(double, zeroGradSpeed, 5.0)								\
		_(double, unpassableObsHeight, 0.4)					\
		_(double, veloTweak1, 0.3)									\
		_(double, veloTweak2, 4.0)									\
		_(double, veloTweak3, 0.0)                  \
		_(double, maxSpeed, 10.0)										\
		_(double, obsDistLimit, 999.0)							\
		_(double, distantObsSpeed, 2.0)							\
		_(double, sigma, 1.0)												\
		_(double, costGenerationRate, 0.0)					\
		_(int, averagingKernelSize, 0)							\
		_(double, relativeWeight, 1.0)

#define DEFINEOPT(type, name, val) \
type name;

#define DEFINEOPTS OPTIONSLIST(DEFINEOPT)

using namespace std;

struct CTerrainCostPainterOptions {
  DEFINEOPTS;

	double filterKernel[MAX_KERNEL_SIZE*MAX_KERNEL_SIZE];
};

class CTerrainCostPainter {
 public:
	
  CTerrainCostPainter(CMapPlus* map,  CCostFuser* costFuser
);
	~CTerrainCostPainter();

	enum STATUS {
		OK,
		ERROR
	};

	int addElevLayer(int elevLayer, CTerrainCostPainterOptions* layerOptions, bool makeCostUseDeltas = false, bool useDefaults = true);

	unsigned long long paintCell(int layerIndex, double UTMNorthing, double UTMEasting, int costFuserLayerIndex); 

	STATUS markChanges(int layerIndex, double UTMNorthing, double UTMEasting, CElevationFuser::STATUS forgetSurrounding);

	unsigned long long paintChanges(int layerIndex, int costFuserLayerIndex);

	STATUS repaintAll(int layerIndex, int costFuserLayerIndex);

	STATUS recomputeKernel(int layerIndex);

	STATUS readOptionsFromFile(int layerIndex, const char* filename);

  double generateVeloFromGrad3 (double gradient, double zeroGradSpeed, double unpassableObsHeight,
				double tweak1, double tweak2);
		
	double atan(double val, double xCenter, double yCenter, double verticalHalfRange, double steepness, double multiplier,
							double shifter);

	int _numLayers;
	int _elevLayerNums[MAX_INPUT_ELEV_LAYERS];
	int _costLayerNums[MAX_INPUT_ELEV_LAYERS];
	int _changesLayerNums[MAX_INPUT_ELEV_LAYERS];
	CTerrainCostPainterOptions* _options[MAX_INPUT_ELEV_LAYERS];

	CCostFuser* _costFuser;

	CMapPlus* _inputMap;
	double _resRows, _resCols;
	int _numRows, _numCols;

  //	int _layerNumRDDF;
};

#endif
