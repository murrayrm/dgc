#ifndef _STATECLIENT_H_
#define _STATECLIENT_H_

#include "SkynetContainer.h"
#include "VehicleState.hh"
#include "ActuatorState.hh"
#include <pthread.h>
#include <unistd.h>

class CStateClient : virtual public CSkynetContainer
{
  /*! This is the state that is received from skynet. This is actually locked
    and received. UpdateState() copies this data into m_stateBuffer */
  VehicleState    m_receivedState;

	/* Same as above for the actuatorstate */
  ActuatorState   m_receivedActuatorstate;

  /*! The mutices to protect the data */
  pthread_mutex_t m_stateBufferMutex;
  pthread_mutex_t m_actuatorstateMutex;

  /*! The function which continually updates the state when any is received from
    the network */
  void getStateThread();

  /*! Same as above, but for the actuator state */
	void getActuatorStateThread();


  void state_Interpolate(double,double,double,double,unsigned long long, unsigned long long, unsigned long long,double&,double&,double&);

  VehicleState   state_buffer[100];
  int state_index;

  /*! The mutex and condition necessary to block on state being filled. */
  pthread_mutex_t m_filledStateConditionMutex;
  pthread_cond_t  m_filledStateCondition;
  bool            m_bFilledState;

  /*! The mutex and condition necessary to block on a state request in the future. */
  pthread_mutex_t m_futureStateConditionMutex;
  pthread_cond_t  m_futureStateCondition;
  bool            m_bFutureStateDone;
	unsigned long long m_requestedTime;
protected:
  /*! The state itself. This is the copy of the state data that's coming
    in. This is visible from the derived classes */
  VehicleState m_state;

  /*! same as above */
  ActuatorState m_actuatorState;

public:
  CStateClient();
  ~CStateClient();

  void UpdateState(void);
  void UpdateState(unsigned long long);

  void UpdateActuatorState(void);
};

#endif // _STATECLIENT_H_
