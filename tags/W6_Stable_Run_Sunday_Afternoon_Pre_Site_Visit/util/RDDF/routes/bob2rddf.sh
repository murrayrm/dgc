# vim: syntax=awk

#bob2rddf.sh
#by alan somers, 31-jan-2004
#added % as a comment   somers    21-jan-2005
#Used a temporary file (./tmp) instead of a pipe to fix memory
#error for large files in OSX	pickett	07-march-2005

#this script converts a waypoint file in the BOB format to one in the
#RDDF format.  Input is stdin, Output is stdout
#Usage: ./bob2rddf.sh < outfile > infile 
#Note: if there is a "./tmp" file in use this script may fail

#requires a make install in team/latlon
#requires team/bin to be in the path
#do PATH=/home/username/team/bin:$PATH

export UTM_ZONE=11  #UTM zone is hard coded in our globalplanner 
export UTM_LETTER=S 

gawk '
    BEGIN {FS=" "}
    function utm2latlon(E, N){
        OFS=" ";
        print "test-utm-to-latlon $UTM_ZONE $UTM_LETTER", E, N | "/bin/sh";
        close("/bin/sh");
         return 0}
    /^[^%#]/ {print ""; print $1; print $4; print $5; utm2latlon($2, $3)}'  > ./tmp
gawk '
    function m2ft(meters) {
        return meters*3.280833333}
    function mps2mph(mps) {
        return mps*2.23693182}
    BEGIN {FS="\n"; RS=""; OFS=","}
    { split($8, lat, " ");
      split($9, long, " ");
      print NR-1, lat[4], long[4], m2ft($2), mps2mph($3), "####", "####", "####"}' < ./tmp

rm ./tmp

