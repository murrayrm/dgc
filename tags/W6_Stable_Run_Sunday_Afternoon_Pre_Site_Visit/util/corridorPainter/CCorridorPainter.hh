#ifndef __CCORRIDORPAINTER_HH__
#define __CCORRIDORPAINTER_HH__


#include <stdlib.h>
#include <CMap.hh>
#include <rddf.hh>

#define CP_PERCENT_SWITCH 0.1

enum {
  CP_INSIDE_CORRIDOR=1,
  CP_OUTSIDE_CORRIDOR=0,
  CP_OUTSIDE_MAP=-1,
  CP_TRACKLINE_VAL = 1,
  CP_EDGE_VAL = 1,

  CP_NUM_TYPES
};

class CCorridorPainter {
public:
  CCorridorPainter();
  ~CCorridorPainter();

  int initPainter(CMap* inputMap,
		  int layerNum,
		  RDDF* inputRDDF,
		  double insideCorridorVal = CP_INSIDE_CORRIDOR,
		  double outsideCorridorVal = CP_OUTSIDE_CORRIDOR,
		  double tracklineVal = CP_TRACKLINE_VAL,
		  double edgeVal = CP_EDGE_VAL,
		  double percentSwitch = CP_PERCENT_SWITCH,
		  int useDelta=0);
  int paintChanges(NEcoord rowBox[], NEcoord colBox[]);
#ifdef CP_OBS
  int paintChanges();
  int paintChanges_TracklineSlope();
#endif

private:
  CMap* _inputMap;
  int _layerNum;
  RDDF* _inputRDDF;

  double _insideCorridorVal;
  double _outsideCorridorVal;
  double _tracklineVal;
  double _edgeVal;
  double _percentSwitch;

  int _prevBottomLeftRowMultiple;
  int _prevBottomLeftColMultiple;

  int _behindWaypointNum;
  int _aheadWaypointNum;

  int _useDelta;
};


#endif //__CCORRIDORPAINTER_HH__
