#include "CMap.hh"
/*
int CMap::getExposedRowBoxWin(int vertices[]) {
  for(int i=0; i<4; i++) {
    vertices[i] = _exposedRowBoxWin[i];
  }

  return CM_OK;
}

int CMap::getExposedColBoxWin(int vertices[]) {
  for(int i=0; i<4; i++) {
    vertices[i] = _exposedColBoxWin[i];
  }

  return CM_OK;
}
*/

int CMap::getExposedRowBoxUTM(NEcoord vertices[]) {
  for(int i=0; i<4; i++) {
    vertices[i] = _exposedRowBoxUTM[i];
  }

  return CM_OK;
}


int CMap::getExposedColBoxUTM(NEcoord vertices[]) {
  for(int i=0; i<4; i++) {
    vertices[i] = _exposedColBoxUTM[i];
  }

  return CM_OK;
}
  



int CMap::getDeltaSize(void* serializedDelta) {
  return *(int*)((char*)serializedDelta);
}


int CMap::resetDelta(int layerNum) {
  _numCells[layerNum]=0;
  clearLayer(_deltaLayerID);

  return CM_OK;
}

int CMap::deltaSize(int layerNum) {
  return _numCells[layerNum];
}

CMap::CMap() {
  _numLayers = 0;
  _layerAddrs = (BaseLayer**)malloc(0);
  _deltaAddrs = (void**)malloc(0);
  _maxCells = (int*)malloc(0);
  _numCells = (int*)malloc(0);
  _cellSize = (int*)malloc(0);
}


CMap::~CMap() {

}


int CMap::initMap(double vehLocUTMNorthing, double vehLocUTMEasting, int numRows, int numCols, double resRows, double resCols, int verboseLevel) {
  _numRows = numRows;
  _numCols = numCols;
  _resRows = resRows;
  _resCols = resCols;

  _verboseLevel = verboseLevel;

  //Initialize the vehicle location
  _vehLocUTMNorthing = vehLocUTMNorthing;
  _vehLocUTMEasting = vehLocUTMEasting;

  //Put the vehicle in the center cell of the map
  _vehLocMemRow = (int)floor((float)numRows/2);
  _vehLocMemCol = (int)floor((float)numCols/2);

  //Put the vehicle in the center cell of the window
  _vehLocWinRow = (int)floor((float)_numRows/2);
  _vehLocWinCol = (int)floor((float)_numCols/2);

  _windowBottomLeftMemRow = 0;
  _windowBottomLeftMemCol = 0;

  _windowBottomLeftUTMNorthingRowResMultiple = (long int)floor(vehLocUTMNorthing/_resRows - _numRows/2);
  _windowBottomLeftUTMEastingColResMultiple = (long int)floor(vehLocUTMEasting/_resCols - _numCols/2);

  //the vehicle's cell is at floor(vehLocUTMNorthing/_rowRes)*_rowRes - _rowRes*floor((float)_numRows/2)
  _windowBottomLeftUTMNorthing = floor(vehLocUTMNorthing/_resRows)*_resRows - _resRows*floor((float)_numRows/2);
  _windowBottomLeftUTMEasting = floor(vehLocUTMEasting/_resCols)*_resCols - _resCols*floor((float)_numCols/2);

  _windowTopRightUTMNorthing = _windowBottomLeftUTMNorthing + _numRows*_resRows;
  _windowTopRightUTMEasting = _windowBottomLeftUTMEasting + _numCols*_resCols;

  _deltaLayerID = addLayer<int>(-1, -2);

  return CM_OK;
}

int CMap::initMap(char* fileName) {
// open the file and parse through it
  

ifstream file;
string line;

double  UTMNorthing;
double  UTMEasting;
int  numRows;
int  numCols;
double  resRows;
double  resCols;
int  verboseLevel; 

file.open(fileName,ios::in);
  
if(!file)
{
  cout<< "UNABLE TO OPEN FILE, PLEASE CHECK THE PATH";
  exit(1);
}



getline(file,line,',');
UTMNorthing = atof(line.c_str());

getline(file,line,',');
UTMEasting   = atof(line.c_str());
 
getline(file,line,',');
numRows = atoi(line.c_str());

getline(file,line,',');
numCols = atoi(line.c_str());
 
getline(file,line,',');
resRows = atof(line.c_str());
 
getline(file,line,',');
resCols = atof(line.c_str());
 
getline(file,line,',');
verboseLevel = atoi(line.c_str());
 

initMap( UTMNorthing, UTMEasting, numRows,  numCols, resRows, resCols, verboseLevel);

return CM_OK;

}


int CMap::clearMap() {
  for(int i=0; i<_numLayers; i++) {
    if(clearLayer(i) != CM_OK) {
      printf("%s [%d]: Error clearing layer %d!  Quitting...\n", __FILE__, __LINE__, i);
      exit(1);
    }
  }

  return CM_OK;
}


int CMap::clearLayer(int layerNum) {
  for(int row=0; row<_numRows; row++) {
    if(clearLayerMemRow(layerNum, row)!=CM_OK) {
      printf("%s [%d]: Error clearing row %d of layer %d!  Quitting...\n", __FILE__, __LINE__, row, layerNum);
      exit(1);
    }
  }

  return CM_OK;
}


int CMap::clearMapWinRow(int winRow) {
  for(int i=0; i<_numLayers; i++) {
    if(clearLayerWinRow(i, winRow) != CM_OK) {
      printf("%s [%d]: Error clearing row %d of layer %d!  Quitting...\n", __FILE__, __LINE__, winRow, i);
      exit(1);
    }
  }

  return CM_OK;
}


int CMap::clearMapWinCol(int winCol) {
  for(int i=0; i<_numLayers; i++) {
    if(clearLayerWinCol(i, winCol) != CM_OK) {
      printf("%s [%d]: Error clearing col %d of layer %d!  Quitting...\n", __FILE__, __LINE__, winCol, i);
      exit(1);
    }
  }

  return CM_OK;
}


int CMap::clearMapMemRow(int row) {
  for(int i=0; i<_numLayers; i++) {
    if(clearLayerMemRow(i, row) != CM_OK) {
      printf("%s [%d]: Error clearing row %d of layer %d!  Quitting...\n", __FILE__, __LINE__, row, i);
      exit(1);
    }
  }

  return CM_OK;
}


int CMap::clearMapMemCol(int col) {
  for(int i=0; i<_numLayers; i++) {
    if(clearLayerMemCol(i, col) != CM_OK) {
      printf("%s [%d]: Error clearing col %d of layer %d!  Quitting...\n", __FILE__, __LINE__, col, i);
      exit(1);
    }
  }

  return CM_OK;
}


int CMap::clearLayerWinRow(int layerNum, int winRow) {
  int memRow, memCol;

  Win2Mem(winRow, 0, &memRow, &memCol);
  clearLayerMemRow(layerNum, memRow);

  return CM_OK;
}


int CMap::clearLayerWinCol(int layerNum, int winCol) {
  int memRow, memCol;

  Win2Mem(0, winCol, &memRow, &memCol);
  clearLayerMemCol(layerNum, memCol);
  return CM_OK;
}


int CMap::clearLayerMemRow(int layerNum, int row) {
  _layerAddrs[layerNum]->clearMemRow(row);
  return CM_OK;
}    



int CMap::clearLayerMemCol(int layerNum, int col) {
  _layerAddrs[layerNum]->clearMemCol(col);
  return CM_OK;
}    


int CMap::updateVehicleLoc(double vehLocUTMNorthing, double vehLocUTMEasting) {
  int newWindowBottomLeftUTMNorthingRowResMultiple, newWindowBottomLeftUTMEastingColResMultiple;
  double deltaNorth, deltaEast;
  double sgnDeltaNorth=1, sgnDeltaEast=1;
  int deltaRows, deltaCols;
  int winRow, col;

  //printf("Shifting to %lf, %lf\n", vehLocUTMNorthing, vehLocUTMEasting);

  newWindowBottomLeftUTMNorthingRowResMultiple = (long int)floor(vehLocUTMNorthing/_resRows - _numRows/2);
  newWindowBottomLeftUTMEastingColResMultiple = (long int)floor(vehLocUTMEasting/_resCols - _numCols/2);

  deltaRows = newWindowBottomLeftUTMNorthingRowResMultiple - _windowBottomLeftUTMNorthingRowResMultiple;
  deltaCols = newWindowBottomLeftUTMEastingColResMultiple - _windowBottomLeftUTMEastingColResMultiple;

  deltaNorth = ((double)deltaRows)*_resRows;
  deltaEast  = ((double)deltaCols)*_resCols;

  if(deltaRows < 0) sgnDeltaNorth = -1;
  if(deltaCols < 0) sgnDeltaEast  = -1;

  if(deltaRows < -1*_numRows || deltaRows > _numRows ||
     deltaCols < -1*_numCols || deltaCols > _numCols) {

    clearMap();
    _vehLocMemRow = _vehLocWinRow;
    _vehLocMemCol = _vehLocWinCol;
  } else {
    if(deltaRows >= 0) {
      //erase to the south
      for(winRow = 0; winRow < deltaRows; winRow++) {
	clearMapWinRow(winRow);
      }
    } else {
      //erase to the north
      for(winRow = _numRows-1; winRow > _numRows-1+deltaRows; winRow--) {
	clearMapWinRow(winRow);
      }      
    }

    if(deltaCols >=0) {
      //erase to the east
      for(col = 0; col < deltaCols; col++) {
	clearMapWinCol(col);
      }
    } else {
      //erase to the west
      for(col = _numCols-1; col > _numCols-1+deltaCols; col--) {
	clearMapWinCol(col);
      }      
    }

    _vehLocMemRow = (_vehLocMemRow + deltaRows)%_numRows;
    _vehLocMemCol = (_vehLocMemCol + deltaCols)%_numCols;

    if(_vehLocMemRow < 0) _vehLocMemRow+=_numRows;
    if(_vehLocMemCol < 0) _vehLocMemCol+=_numCols;
  }

  _windowBottomLeftMemRow = _vehLocMemRow - _vehLocWinRow;
  _windowBottomLeftMemCol = _vehLocMemCol - _vehLocWinCol;

  if(_windowBottomLeftMemRow < 0) _windowBottomLeftMemRow+=_numRows;
  if(_windowBottomLeftMemCol < 0) _windowBottomLeftMemCol+=_numCols;

  _windowTopRightMemRow = _windowBottomLeftMemRow - 1;
  _windowTopRightMemCol = _windowBottomLeftMemCol - 1;

  if(_windowTopRightMemRow < 0) _windowTopRightMemRow+=_numRows;
  if(_windowTopRightMemCol < 0) _windowTopRightMemCol+=_numCols;

  _windowBottomLeftUTMNorthingRowResMultiple = newWindowBottomLeftUTMNorthingRowResMultiple;
  _windowBottomLeftUTMEastingColResMultiple = newWindowBottomLeftUTMEastingColResMultiple;

  _windowBottomLeftUTMNorthing = ((double)_windowBottomLeftUTMNorthingRowResMultiple)*_resRows;
  _windowBottomLeftUTMEasting = ((double)_windowBottomLeftUTMEastingColResMultiple)*_resCols;

  _windowTopRightUTMNorthing = _windowBottomLeftUTMNorthing + _numRows*_resRows;
  _windowTopRightUTMEasting = _windowBottomLeftUTMEasting + _numCols*_resCols;

  _vehLocUTMNorthing = vehLocUTMNorthing;
  _vehLocUTMEasting = vehLocUTMEasting;


  if(deltaRows >= getNumRows() ||
     deltaCols >= getNumCols()) {
    _exposedRowBoxUTM[0] = NEcoord(((double)_windowBottomLeftUTMNorthingRowResMultiple)*_resRows, 
				   ((double)_windowBottomLeftUTMEastingColResMultiple)*_resCols);
    _exposedRowBoxUTM[1] = NEcoord(((double)_windowBottomLeftUTMNorthingRowResMultiple)*_resRows + (_numRows)*_resRows,
				   ((double)_windowBottomLeftUTMEastingColResMultiple)*_resCols);
    _exposedRowBoxUTM[2] = NEcoord(((double)_windowBottomLeftUTMNorthingRowResMultiple)*_resRows + (_numRows)*_resRows,
				   ((double)_windowBottomLeftUTMEastingColResMultiple)*_resCols + (_numCols)*_resCols);
    _exposedRowBoxUTM[3] = NEcoord(((double)_windowBottomLeftUTMNorthingRowResMultiple)*_resRows,
				   ((double)_windowBottomLeftUTMEastingColResMultiple)*_resCols + (_numCols)*_resCols);

    for(int i=0; i<4; i++) {
      _exposedColBoxUTM[i] = NEcoord(((double)_windowBottomLeftUTMNorthingRowResMultiple)*_resRows, 
				   ((double)_windowBottomLeftUTMEastingColResMultiple)*_resCols);
    }
  } else {
    if(deltaRows>=0) {
      //we moved north, empty area is at top
      //printf("FM North\n");
      _exposedRowBoxUTM[0].N = _windowBottomLeftUTMNorthing + _resRows*(_numRows-deltaRows);
      _exposedRowBoxUTM[1].N = _windowTopRightUTMNorthing;
      _exposedRowBoxUTM[2].N = _windowTopRightUTMNorthing;
      _exposedRowBoxUTM[3].N = _windowBottomLeftUTMNorthing + _resRows*(_numRows-deltaRows);

      _exposedColBoxUTM[0].N = _windowBottomLeftUTMNorthing;
      _exposedColBoxUTM[1].N = _windowBottomLeftUTMNorthing + _resRows*(_numRows-deltaRows);
      _exposedColBoxUTM[2].N = _windowBottomLeftUTMNorthing + _resRows*(_numRows-deltaRows);
      _exposedColBoxUTM[3].N = _windowBottomLeftUTMNorthing;
    } else {
      //printf("FM South\n");
      //moved south, empty at bottom
      _exposedRowBoxUTM[0].N = _windowBottomLeftUTMNorthing;
      _exposedRowBoxUTM[1].N = _windowBottomLeftUTMNorthing - _resRows*deltaRows;
      _exposedRowBoxUTM[2].N = _windowBottomLeftUTMNorthing - _resRows*deltaRows;
      _exposedRowBoxUTM[3].N = _windowBottomLeftUTMNorthing;

      _exposedColBoxUTM[0].N = _windowBottomLeftUTMNorthing - _resRows*deltaRows;
      _exposedColBoxUTM[1].N = _windowTopRightUTMNorthing;
      _exposedColBoxUTM[2].N = _windowTopRightUTMNorthing;
      _exposedColBoxUTM[3].N = _windowBottomLeftUTMNorthing - _resRows*deltaRows;
    }
    if(deltaCols>=0) {
      //printf("FM East\n");
      //we moved east, empty area is at right
      _exposedRowBoxUTM[0].E = _windowBottomLeftUTMEasting;
      _exposedRowBoxUTM[1].E = _windowBottomLeftUTMEasting;
      _exposedRowBoxUTM[2].E = _windowTopRightUTMEasting;
      _exposedRowBoxUTM[3].E = _windowTopRightUTMEasting;

      _exposedColBoxUTM[0].E = _windowBottomLeftUTMEasting + _resCols*(_numCols-deltaCols);
      _exposedColBoxUTM[1].E = _windowBottomLeftUTMEasting + _resCols*(_numCols-deltaCols);
      _exposedColBoxUTM[2].E = _windowTopRightUTMEasting;
      _exposedColBoxUTM[3].E = _windowTopRightUTMEasting;
    } else {
      //printf("FM West\n");
      //moved west, empty at left
      _exposedRowBoxUTM[0].E = _windowBottomLeftUTMEasting;
      _exposedRowBoxUTM[1].E = _windowBottomLeftUTMEasting;
      _exposedRowBoxUTM[2].E = _windowTopRightUTMEasting;
      _exposedRowBoxUTM[3].E = _windowTopRightUTMEasting;

      _exposedColBoxUTM[0].E = _windowBottomLeftUTMEasting;
      _exposedColBoxUTM[1].E = _windowBottomLeftUTMEasting;
      _exposedColBoxUTM[2].E = _windowBottomLeftUTMEasting - _resCols*deltaCols;
      _exposedColBoxUTM[3].E = _windowBottomLeftUTMEasting - _resCols*deltaCols;
    }

  }
  
  //printf("FM %d %d\n", deltaRows, deltaCols);

  return CM_OK;
}


int CMap::Mem2Win(int memRow, int memCol, int *winRow, int *winCol) {
  //Bounds checking
  //int inBounds = checkBoundsMem(memRow, memCol);

  //if(inBounds != CM_OK) {
    //printf("%s [%d]: Error during bounds check in UTM2Win!\n", __FILE__, __LINE__);
  //return inBounds;
  //}

  *winRow = memRow - _windowBottomLeftMemRow;
  *winCol = memCol - _windowBottomLeftMemCol;

  if(*winRow < 0) *winRow+=_numRows;
  if(*winCol < 0) *winCol+=_numCols;

  return CM_OK;
}


int CMap::Win2Mem(int winRow, int winCol, int *memRow, int *memCol) {
  //Bounds checking
  //int inBounds = checkBoundsWin(winRow, winCol);

  //if(inBounds != CM_OK) {
    //printf("%s [%d]: Error during bounds check in UTM2Win!\n", __FILE__, __LINE__);
    //return inBounds;
  //}

  *memRow = winRow + _windowBottomLeftMemRow;
  *memCol = winCol + _windowBottomLeftMemCol;

  if(*memRow >= _numRows) *memRow-=_numRows;
  if(*memCol >= _numCols) *memCol-=_numCols;

  return CM_OK;
}


int CMap::Win2UTM(int winRow, int winCol, double *UTMNorthing, double *UTMEasting) {
  //do bounds checking!

  *UTMNorthing = (winRow + _windowBottomLeftUTMNorthingRowResMultiple)*_resRows;
  *UTMEasting = (winCol + _windowBottomLeftUTMEastingColResMultiple)*_resCols;

  return CM_OK;
}


int CMap::Mem2UTM(int memRow, int memCol, double *UTMNorthing, double *UTMEasting) {
  int winRow, winCol;
  Mem2Win(memRow, memCol, &winRow, &winCol);
  Win2UTM(winRow, winCol, UTMNorthing, UTMEasting);

  return CM_OK;
}


int CMap::UTM2Win(double UTMNorthing, double UTMEasting, int *winRow, int *winCol) {
  int inBounds = checkBoundsUTM(UTMNorthing, UTMEasting);

//   if(inBounds != CM_OK) {
//     //printf("%s [%d]: Error during bounds check in UTM2Win!\n", __FILE__, __LINE__);
//     return inBounds;
//   }

  *winRow = (int)floor(UTMNorthing/_resRows) - _windowBottomLeftUTMNorthingRowResMultiple;
  *winCol = (int)floor(UTMEasting/_resCols) - _windowBottomLeftUTMEastingColResMultiple;

  return inBounds;
}


int CMap::UTM2Mem(double UTMNorthing, double UTMEasting, int *memRow, int *memCol) {
  int winRow, winCol;
  int returnStatus;

  returnStatus = UTM2Win(UTMNorthing, UTMEasting, &winRow, &winCol);
  if(returnStatus != CM_OK) {
    //printf("%s [%d]: Error during call to UTM2Win from within UTM2Mem!\n", __FILE__, __LINE__);
    return returnStatus;
  }
  returnStatus = Win2Mem(winRow, winCol, memRow, memCol);
  if(returnStatus != CM_OK) {
    //printf("%s [%d]: Error during call to Win2Mem from within UTM2Mem!\n", __FILE__, __LINE__);
    return returnStatus;
  }

  return CM_OK;
}


int CMap::checkBoundsMem(int memRow, int memCol) {
  if(memRow < 0 || memRow >= _numRows) {
    printf("%s [%d]: Memory row %d is out of bounds [0, %d]!\n", 
	   __FILE__, __LINE__, memRow, _numRows-1);
    return CM_OUT_OF_BOUNDS;
  }
  if(memCol < 0 || memCol >= _numCols) {
    printf("%s [%d]: Memory col %d is out of bounds [0, %d]!\n", 
	   __FILE__, __LINE__, memCol, _numCols-1);    
    return CM_OUT_OF_BOUNDS;
  }

  return CM_OK;
}


int CMap::checkBoundsWin(int winRow, int winCol) {
  if(winRow >= _numRows || winRow < 0) {
//     printf("%s [%d]: Window row %d is out of bounds [%d, %d]\n",
// 	   __FILE__, __LINE__, winRow, (int)floor((float)(_numRows-1/-2)), (int)floor((float)(_numRows-1)/2));
    return CM_OUT_OF_BOUNDS;
  }
  if(winCol >= _numCols || winCol < 0) {
//     printf("%s [%d]: Window col %d is out of bounds [%d, %d]\n",
// 	   __FILE__, __LINE__, winCol, (int)floor((float)(_numCols-1/-2)), (int)floor((float)(_numCols-1)/2));
    return CM_OUT_OF_BOUNDS;
  }

  return CM_OK;
}


int CMap::checkBoundsUTM(double UTMNorthing, double UTMEasting) {
  if(UTMNorthing >= _windowTopRightUTMNorthing || UTMNorthing < _windowBottomLeftUTMNorthing ||
     UTMEasting >= _windowTopRightUTMEasting || UTMEasting < _windowBottomLeftUTMEasting) {
    /*printf("%s [%d]: UTM Northing %lf is out of bounds [%lf, %lf)\n",
	   __FILE__, __LINE__, UTMNorthing, _windowOriginUTMNorthing + floor((float)(_numCols)/-2)*_resRows,
	   _windowOriginUTMNorthing + floor((float)(_numCols)/2)*_resRows);*/
    return CM_OUT_OF_BOUNDS;
  }

  //if(UTMEasting >= _windowTopRightUTMEasting || UTMEasting < _windowBottomLeftUTMEasting) {
    /*printf("%s [%d]: UTM Easting %lf is out of bounds [%lf, %lf)\n",
	   __FILE__, __LINE__, UTMEasting, _windowOriginUTMEasting + floor((float)(_numCols)/-2)*_resCols,
	   _windowOriginUTMEasting + floor((float)(_numCols)/2)*_resCols);*/
    //return CM_OUT_OF_BOUNDS;
  //}

  return CM_OK;
}


int CMap::roundUTMToCellBottomLeft(double UTMNorthing, double UTMEasting, double* RoundedUTMNorthing, double* RoundedUTMEasting) {
  *RoundedUTMNorthing = floor(UTMNorthing/_resRows)*_resRows;
  *RoundedUTMEasting = floor(UTMEasting/_resCols)*_resCols;

  return CM_OK;
}
