#include "Sim.hh"
#include "DGCutils"

SIM_DATUM d;

int QUIT_PRESSED = 0;
int PAUSED       = 1;
int RESET        = 0;

asim::asim(int sn_key, bool bSimpleModel, double steerLag, double steerRateLimit)
	: m_skynet(SNasim, sn_key),
		m_bSimpleModel(bSimpleModel),
		m_steerLag(steerLag),
		m_steerRateLimit(steerRateLimit)
{
  // clear the state struct and actuators
  d.accel_cmd = 0;
  d.steer_cmd = 0;

	m_pVehicle = new vehicle_t;

	SimInit();

	DGCcreateMutex(&m_stateMutex);

	broadcast_statesock = m_skynet.get_send_sock(SNstate);
}

asim::~asim()
{
	delete m_pVehicle;
}

void asim::SimInit(void)
{
	DGCgettime(d.lastUpdate);
	simState.readFile("simInitState.dat");

#if 0
	double ststate[]=
{3834528.7036177786067, 1.9625494408429147164, -0.12031789308008045136,
442624.57282297825441, 0.75617731768330342934, 0.21415203500229329503,
0.1120598116244396758, 0.18951424501995356442}
;

	simState.n   = ststate[0];
	simState.e   = ststate[3];
	simState.nd  = ststate[1];
	simState.ed  = ststate[4];
	simState.ndd = ststate[2];
	simState.edd = ststate[5];
	simState.yaw = ststate[6];
	simState.yawd = ststate[7];
	simState.phi = atan(simState.yawd * VEHICLE_WHEELBASE / hypot(ststate[1], ststate[4]));
#endif


	if(m_bSimpleModel)
	{
		simEngineSimple.Init(simState, 0.0);
		// this sets the front state to match the simState. The large argument
		// effectively sets ndd and edd to 0
		simEngineSimple.set_front_state(1.0e10);
	}
	else
	{
		simEngine.Init(simState, 0.0, m_steerLag, m_steerRateLimit);
		// this sets the front state to match the simState. The large argument
		// effectively sets ndd and edd to 0
		simEngine.set_front_state(1.0e10);
	}
}

void asim::broadcast()
{
	if (m_skynet.send_msg(broadcast_statesock,
				&d.SS,
				sizeof(d.SS),
				0,
				&m_stateMutex)
			!= sizeof(d.SS))
	{
		cerr << "asim::broadcast(): didn't send right size state message" << endl;
	}
}

void asim::Active() 
{
  unsigned long long dt;
  unsigned long long biasedTime;
  cout << "entering asim::Active loop" << endl;
  
  while (true) 
  {
    usleep(10000);
    
    // handle time update
    DGCgettime(dt);    
    dt -= d.lastUpdate;
    DGCgettime(d.lastUpdate);

		d.accel_cmd = m_pVehicle->actuator[ACTUATOR_GAS].command - m_pVehicle->actuator[ACTUATOR_BRAKE].command;
    // The actuator command is normalized [-1,1], as is steer_cmd
    d.steer_cmd = m_pVehicle->actuator[ACTUATOR_STEER].command;

		// now perform state update with known time step
		if(m_bSimpleModel)
			simEngineSimple.SetCommands(d.steer_cmd, d.accel_cmd);
		else
			simEngine.SetCommands(d.steer_cmd, d.accel_cmd);

    if( !PAUSED ) 
    {
			if(m_bSimpleModel)
				simEngineSimple.RunSim(DGCtimetosec(dt));
			else
				simEngine.RunSim(DGCtimetosec(dt));
    } 
    if( RESET )
    {
      SimInit();
      RESET = 0;
    }
		updateState();
		
		// now set the actuator outputs (simulate them!)
		m_pVehicle->actuator[ACTUATOR_STEER].position = d.phi / VEHICLE_MAX_AVG_STEER;
		m_pVehicle->actuator[ACTUATOR_GAS]  .position = d.accel_cmd > 0.0 ?  d.accel_cmd : 0.0;
		m_pVehicle->actuator[ACTUATOR_BRAKE].position = d.accel_cmd < 0.0 ? -d.accel_cmd : 0.0;
    broadcast();
  }
}

void asim::updateState(void)
{
	if(m_bSimpleModel)
		simState = simEngineSimple.GetFrontState();
	else
		simState = simEngine.GetFrontState();

  // Updating the state struct
	DGClockMutex(&m_stateMutex);
  d.SS.Northing = simState.n;
  d.SS.Easting  = simState.e;
  d.SS.Vel_N    = simState.nd;
  d.SS.Vel_E    = simState.ed;
  d.SS.Acc_N    = simState.ndd;
  d.SS.Acc_E    = simState.edd;
	d.SS.YawRate  = simState.yawd;	
	d.SS.Yaw      = simState.yaw;
	d.phi         = simState.phi;

	d.SS.Altitude = 0.0;
	d.SS.Acc_D    = 0.0;
	d.SS.YawAcc   = 0.0;
	d.SS.RollAcc  = 0.0;
	d.SS.PitchAcc = 0.0;
	d.SS.Vel_D    = 0.0;
  d.SS.PitchRate= 0.0;
  d.SS.RollRate = 0.0;
  d.SS.Pitch    = 0.0;
  d.SS.Roll     = 0.0;

  DGCgettime(d.SS.Timestamp);
	DGCunlockMutex(&m_stateMutex);
}
