/*!* This file holds standardized function calls from adrive
 * which in turn call the individual driver for actuators. */

#include <pthread.h>
#include <stdio.h>

#include "actuators.h"
#include "adrive.h"
#include "brake.h"
#include "parker_steer.h"
#include "throttle.h"
#include "estop.h"

// Look for the vehicle structure. 
extern struct vehicle_t my_vehicle;
extern int simulation_flag;
extern char simulator_IP;

/******************Brake Function Calls *****************
 * are below */
/*
 * functionalitiy with multithreaded adrive.  Basically standardized function
 * calls that call functions already defined in the existing code.  
 *  Tully Foote
 * 
 ************************************************************************/
/*! The standadized function call to execute brake command*/
int execute_brake_command( double command ) 
{
  double brake_command;
  brake_command = command;
  //printf("Executing Brake Command %d\n", command);
  if (brake_command >= 0 && brake_command <= 1)
    {
      brake_setposition(brake_command);    
      return 1;
    }
  else
    return 0;
}

/*! The standadized function call to execute brake status*/
int execute_brake_status()
{
  int retval = 0;
  //printf("Executing Brake Status \n");
  /* No feedback for this either yet.  Set to commanded position. */
  //my_vehicle.actuator[ACTUATOR_BRAKE].position = my_vehicle.actuator[ACTUATOR_BRAKE].command;
  // I have reimplimented the brake feedback loop since it should now be functional.
  
  //  brake_getposition();
  my_vehicle.actuator[ACTUATOR_BRAKE].position = brake_getposition();
  //usleep(10000000);
  my_vehicle.actuator[ACTUATOR_BRAKE].pressure = brake_getpressure(); 
  

  //printf("%g",my_vehicle.actuator[ACTUATOR_BRAKE].pressure);
  return retval;
}

/*! The standadized function call to execute brake initialization*/
void execute_brake_init()
{
  printf("Executing Brake Init \n");
  if (simulation_flag)
    {
      //      printf("Sim");
      simulator_brake_open(my_vehicle.actuator[ACTUATOR_BRAKE].port);
    }
  else if(brake_open(my_vehicle.actuator[ACTUATOR_BRAKE].port) != TRUE)
    {
      //printf("Brake open failed\n");
    }
  else
    {
      //printf("Brake OK\n");
    }
}



/******************Trans Function Calls *****************
 * are below */
/**********************These are placeholder functions */

/*! The standadized function call to execute transmission command*/
int execute_trans_command( double command ) 
{
  //printf("Executing Trans Command %d\n", command);
  return 1;
}

/*! The standadized function call to execute transmission status*/
int execute_trans_status()
{
  //printf("Executing Trans Status \n");
  return 1;
}

/*! The standadized function call to execute transmission initialization*/
void execute_trans_init()
{
  //printf("Executing Trans Init \n");
}



/******************Estop Function Calls *****************
 * are below */
/**********************These are placeholder functions */
/*! The standadized function call to execute estop command*/
int execute_estop_command( double command ) 
{
  printf("Executing Estop Command %g, you should not be doing this!\n", command);
  return 1;
}

/*! The standadized function call to execute estop status*/
int execute_estop_status()
{
  //  printf("about to read status");
  int  current_estop_status =  estop_status();
  //printf("i have read status %d, replacing %d\n", current_estop_status, my_vehicle.actuator[ACTUATOR_ESTOP].position);

  //printf("Executing Estop Status \n");

  /* This is a level of estop that adrive will react to a disable without commands
   * coming down the chain.  
   *
   * In case of disable adrive will:
   * Put the brakes on full
   * Set the throttle to zero
   * Center the steering wheel
   *
   */
  if ( current_estop_status == 0)
    {
      //Apply Full Brakes
      pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_BRAKE].mutex );
      my_vehicle.actuator[ACTUATOR_BRAKE].command = 1; 
      pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_BRAKE].mutex );
      pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_BRAKE].cond_flag );
      
      // Center the steering
      pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_STEER].mutex );
      my_vehicle.actuator[ACTUATOR_STEER].command = 0; 
      pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_STEER].mutex );
      pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_STEER].cond_flag );

      // Zero the gas
       pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_GAS].mutex );
       my_vehicle.actuator[ACTUATOR_GAS].command = 0;
       pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_GAS].mutex );
       pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_GAS].cond_flag );

    }


  // Deal with the 5 second delay required coming out of pause into run.  
  //   Including the possibility of jumping straight out of disable.  

  else if (my_vehicle.actuator[ACTUATOR_ESTOP].position != 2 &&  current_estop_status == 2)
    {
      // printf("about to sleep for 5");
      sleep (5);
      //  printf("i have slept for 5");
    }
  my_vehicle.actuator[ACTUATOR_ESTOP].position =  current_estop_status;
  return 1;
}

/*! The standadized function call to execute estop initialization*/
void execute_estop_init()
{
  //printf("Executing Estop Init \n");
  if(estop_open(my_vehicle.actuator[ACTUATOR_ESTOP].port) == 0)
    {
      // If it fails set the status to 0.  
      my_vehicle.actuator[ACTUATOR_ESTOP].status = 0;
    }
}


/******************Steer Function Calls *****************
 * are below */

/*! The standadized function call to execute steer command*/
int execute_steer_command( double command ) 
{
  double steer_command = command;
  //printf("Executing Steer Command %d\n", steer_command);
  if (steer_command <= 1 && steer_command >= -1)
    {
      steer_heading(steer_command);
      return 1;
    }
  else 
    return 0;
}

/*! The standadized function call to execute steer status*/
int execute_steer_status()
{
  int retval;
  //  printf("Executing Steer Status \n");
  steer_state_update(FALSE, FALSE);
  usleep(100000);
#warning THis sleep is very bad! Required due to  SDS bug.  
  my_vehicle.actuator[ACTUATOR_STEER].position = (double) steer_getheading();
  return retval;
}

/*! The standadized function call to execute steer initialization*/
void execute_steer_init()
{
  //printf("Executing Steer Init \n");

  if(simulation_flag)
    {
      simulator_steer_open(my_vehicle.actuator[ACTUATOR_STEER].port);
      pthread_cond_broadcast(&steer_calibrated_flag);
    }
  
  else if(steer_open(my_vehicle.actuator[ACTUATOR_STEER].port) != TRUE)
    {


      // printf("Steer open failed, trying calibration\n");
      if(steer_calibrate() != TRUE)
	{
	  //printf("steer calibrate failed ... we are hosed\n");
	  abort();
	}
      else
	{
	  //printf("Steer OK\n");
	  pthread_cond_broadcast(&steer_calibrated_flag);
	}
    }
  else
	{
	  //printf("Steer_opened properly\n");
	  pthread_cond_broadcast(&steer_calibrated_flag);
	  //printf("I ihave sent the wakeup\n");
	}
}






/******************Gas Function Calls *****************
 * are below */

/*! The standadized function call to execute gas initialization*/
int execute_gas_command( double command ) 
{
  double throttle_command;
  throttle_command = command;
  //  printf("Executing Gas Command %f\n", throttle_command);
  if (throttle_command <= 1 && throttle_command >=0)
    {
      throttle_setposition(throttle_command);
      return 1;
    }
  return 0;
}

/*! The standadized function call to execute gas status*/
int execute_gas_status()
{
  int retval = 0;
  //printf("Executing Gas Status \n");
  my_vehicle.actuator[ACTUATOR_GAS].position = throttle_getposition();
  //printf("Throttle get_position() returns %d\n", throttle_getposition());
  return 1;
}

/*! The standadized function call to execute gas initialization*/
void execute_gas_init()
{
  //printf("Executing Gas Init \n");
  if(simulation_flag)
    {
      simulator_throttle_open(my_vehicle.actuator[ACTUATOR_GAS].port);
    }
  else if(throttle_open(my_vehicle.actuator[ACTUATOR_GAS].port) != TRUE)
    {
      printf("Throttle open failed!!!!!!\n");
      exit(1);
    }
  else
    {
      //printf("Throttle OK\n");
    }
}
