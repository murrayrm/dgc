/**
 * MapDisplaySN.hh
 * This class updates the mapconfig datastructure, 
 * allowing for display of RDDF, paths and cost maps on the 
 * skynetdisplay. Messaging is done using the skynet API.
 * Revision History:
 * 02/05/2005  hbarnor  Created
 */

#ifndef MAPDISPLAYSN_HH
#define MAPDISPLAYSN_HH

#include <list>

#include "sn_msg.hh"
#include "DGCutils"
#include "VehicleState.hh"
#include "StateClient.h"
#include "TrajTalker.h"

#include "MapDisplay/MapDisplay.hh" // for MapConfig
#include "CMapPlus.hh"

#define PATHIDX_HISTORY					0
#define PATHIDX_PLANNERSEED			1
#define PATHIDX_PLANNERINTERM		2
#define PATHIDX_PLANNED					3
#define PATHIDX_NUMPATHS				4

using namespace std;

//Values for finding the layer ID of a given layer in the map
enum {
  SNGUI_LAYER_FUSIONMAP_001 = 0,
  SNGUI_LAYER_STEREO_001    = 1,
  SNGUI_LAYER_LADAR_001     = 2,
  SNGUI_LAYER_STATIC_001    = 3,
  SNGUI_LAYER_LADAR_002     = 4,

  SNGUI_LAYER_TOTAL         = 5
};


class MapDisplaySN : public CStateClient, public CTrajTalker
{
public:
  /**
   * Constructor
   * Sets the statestruct of the mapconfig,
   * sets the CMAP to the first waypoint
   */
  MapDisplaySN(MapConfig * mapConfig, int sn_key);
  /**
   * Destructor to handle dynamic
   * memory - currently empty
   */
  ~MapDisplaySN();
  /** 
   * Init - updates the state data and ensures 
   * the state is valid(northing != 0). 
   */
  void init();
  /**
   * Active - contains the loop for listening to
   * incoming skynet messages.
   */
  void active();

	/** These functions read in a new traj/mapdelta/state and update the map **/
	void getTrajThread(void* pArg);
	void getMapDeltaThread(void* pArg);
	void getStateThread(void);

private:
  // Declare the Map class.
  CMapPlus myCMap; // the Map
  int layerNum[SNGUI_LAYER_TOTAL];  // how do we actually display multiple layers in the stupid mapdisplay POS?
  CTraj *curPath; // the path received from PathFollower

  MapConfig * m_mapConfig;

	sn_msg m_pathTypes[PATHIDX_NUMPATHS];
	modulename m_pathSources[PATHIDX_NUMPATHS];
	RGBA_COLOR m_pathColors[PATHIDX_NUMPATHS];

  sn_msg m_mapTypes[SNGUI_LAYER_TOTAL];
  modulename m_mapSources[SNGUI_LAYER_TOTAL];

	// planned_path, path_history
  PATH paths[PATHIDX_NUMPATHS];
  char* m_pMapDelta[SNGUI_LAYER_TOTAL];
};

#endif
