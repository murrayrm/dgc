/*----------------------------------------------------------------------------
--                                                                          --
--                        U N C L A S S I F I E D                           --
--                                                                          --
--       N O R T H R O P   G R U M M A N   P R O P R I E T A R Y            --
--                                                                          --
-- Copyright (c) 2002 Northrop Grumman Corp.                                --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------
--                                                                          --
--    The code contained herein is proprietary to Northrop Grumman Corp.    --
--    and is subject to any non-disclosure agreements and proprietary       --
--    information exchange agreements between Northrop Grumman and the      --
--    and the receiving party.  The code contained herein should be         --
--    considered technical data and is subject to all relevant export       --
--    control regulations.                                                  --
--                                                                          --
----------------------------------------------------------------------------*/

/* File: Imu.c
 * Date: 12/29/03
 */

#ifndef ImuH
#define ImuH

/****************************************************************************
 * Quaternion Type Definition
 ****************************************************************************/

typedef struct{
  double q1, q2, q3, q4;
} TQuaternion;

/****************************************************************************
 * Quaternion Buffer Type Definition
 ****************************************************************************/

typedef struct{
  TQuaternion   qs[10];
  TQuaternion   qmid;
  TQuaternion   ql[10];
} TQbuf;

/****************************************************************************
 * Boresight Quaternion Type Definition
 ****************************************************************************/

typedef struct  {
 TQuaternion YawQuat;
 TQuaternion PitchQuat;
 TQuaternion RollQuat;
} BSQuat;

/****************************************************************************
 * Direction Cosine Type Definition
 ****************************************************************************/

typedef struct {
  double dc11, dc12, dc13;
  double dc21, dc22, dc23;
  double dc31, dc32, dc33;
} TDirCos;

/****************************************************************************
 * Global Function Definitions
 ****************************************************************************/

void imuInit(void);
void imuAcceptKalmanCorr(void);
void imuAcceptTiltCorr(double tiltx, double tilty, double tiltz);
void imuAcceptNavTiltCorr(double tiltx, double tilty, double tiltz);
void imuDataSummation(void);
void imuCompGyroData(void);
void imuCompAccelData(void);

void imuInitQuaternion(void);
void imuAcceptQuatDeltas(double dq1, double dq2, double dq3, double dq4);
void imuQuaternionMult(TQuaternion *qa, TQuaternion *qb, TQuaternion *qab);
void imuSetQuaternion(double q1, double q2, double q3, double q4, TQuaternion *q);

void imuQuatNG(double alpha);
void imuQuatRotate(void);
void imuQuatBuf(void);
void imuQuaternionInt(void);
void imuSyncQuat(int inQMsgNum, double inQTLatency, int inQMode, TQuaternion *ql);

void imuDirCosines(void);
void imuQuatToDirCos(TQuaternion *q, TDirCos *dc);
void imuDeltaVelTransform(void);
void imuEulerAngles(void);
void imuAcceptBoresight(double yaw, double elev, double roll, int validity);
void imuAcceptOrient(int indx, int validity); 

void imuComputeQOG(void);
void imuAcceptQLG(double q1, double q2, double q3, double q4);
void imuAcceptQLB(double q1, double q2, double q3, double q4);

/****************************************************************************
 * End
 ****************************************************************************/
#endif

/* Informational:  The following are the quaternions for the defined oreintations.  To initialize, set
 * Orient_Index in the intialization to the value of the desired orientation prior to calling imu_init
 * Alex Fax 1/22/04 
  {    0.0,     0.0,     0.0,     1.0}, // Orientation 0
  {    0.0,     0.0,  SQ2OV2,  SQ2OV2}, // Orientation 1
  {    0.0,     0.0,     1.0,     0.0}, // Orientation 2
  {    0.0,     0.0, -SQ2OV2,  SQ2OV2}, // Orientation 3
  {    1.0,     0.0,     0.0,     0.0}, // Orientation 4
  { SQ2OV2,  SQ2OV2,     0.0,     0.0}, // Orientation 5
  {    0.0,     1.0,     0.0,     0.0}, // Orientation 6
  { SQ2OV2, -SQ2OV2,     0.0,     0.0}, // Orientation 7
  { SQ2OV2,     0.0,     0.0,  SQ2OV2}, // Orientation 8
  {    0.5,     0.5,     0.5,     0.5}, // Orientation 9
  {    0.0,  SQ2OV2,  SQ2OV2,     0.0}, // Orientation 10
  {    0.5,    -0.5,    -0.5,     0.5}, // Orientation 11
  {-SQ2OV2,     0.0,     0.0,  SQ2OV2}, // Orientation 12
  {   -0.5,    -0.5,     0.5,     0.5}, // Orientation 13
  {    0.0,  SQ2OV2, -SQ2OV2,     0.0}, // Orientation 14
  {   -0.5,     0.5,    -0.5,     0.5}, // Orientation 15
  { SQ2OV2,     0.0,  SQ2OV2,     0.0}, // Orientation 16
  {   -0.5,    -0.5,    -0.5,     0.5}, // Orientation 17
  {    0.0, -SQ2OV2,     0.0,  SQ2OV2}, // Orientation 18
  {    0.5,    -0.5,     0.5,     0.5}, // Orientation 19
  {    0.0,  SQ2OV2,     0.0,  SQ2OV2}, // Orientation 20
  {   -0.5,     0.5,     0.5,     0.5}, // Orientation 21
  { SQ2OV2,     0.0, -SQ2OV2,     0.0}, // Orientation 22
  {    0.5,     0.5,    -0.5,     0.5}, // Orientation 23   */
