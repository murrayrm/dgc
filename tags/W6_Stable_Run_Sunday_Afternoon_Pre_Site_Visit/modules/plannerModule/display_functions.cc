#include "display_functions.hh"

extern CMap* globalCMapPtr;
extern int globalNumLayers;
extern int globalCurrentLayer;
extern int changedWinRow;
extern int changedWinCol;
extern int ACTIVE_LAYER;


#define TIC_SPACING 80.0
#define DISPLAY_PIXEL_WIDTH 250
#define DISPLAY_PIXEL_HEIGHT 500

#define DISP_TO_MAP_FACTOR_ROW DEFAULT_MAP_NUM_ROWS/DISPLAY_PIXEL_HEIGHT
#define DISP_TO_MAP_FACTOR_COL DEFAULT_MAP_NUM_COLS/DISPLAY_PIXEL_WIDTH

#define MAP_TO_DISP_FACTOR_ROW DISPLAY_PIXEL_HEIGHT/DEFAULT_MAP_NUM_ROWS
#define MAP_TO_DISP_FACTOR_COL DISPLAY_PIXEL_WIDTH/DEFAULT_MAP_NUM_COLS

GLubyte verticalTic[1][10][3];
GLubyte horizontalTic[10][1][3];
GLubyte colorMap[256][25][3];
GLubyte clearLine[8][13][3];

GLubyte globalMapArray[DISPLAY_PIXEL_HEIGHT][DISPLAY_PIXEL_WIDTH][3];

void bitmap_output(int x, int y, int z, char *string, void *font)
{  int len, i;
 glRasterPos2i(x, y);            // Locate Raster Position in 3-space


   len = (int) strlen(string);        // Find length of string
   for (i = 0; i < len; i++)         // Loop through plotting all characters 
   {                                // in font style
       glutBitmapCharacter(font, string[i]);
   }
}


void global_cmap_display() {
  //Timeval before, after;
  //before = TVNow();
  //printf("Displayin layer %d\n", globalCurrentLayer);
  int width, height;
  double zoomx, zoomy;

  width = glutGet(GLUT_WINDOW_WIDTH);
  height = glutGet(GLUT_WINDOW_HEIGHT);
  zoomx = ((double)width-200)/((double)DISPLAY_PIXEL_WIDTH);
  zoomy = ((double)height-100)/((double)DISPLAY_PIXEL_HEIGHT);
  if(zoomx > zoomy) {
    zoomx=zoomy;
  } else {
    zoomy=zoomx;
  }

  double val;
  int c;
  int mapRow, mapCol;
  for(int row=0; row < DISPLAY_PIXEL_HEIGHT; row++) {
    for(int col=0; col < DISPLAY_PIXEL_WIDTH; col++) {
      mapRow = (int)((double)row * DISP_TO_MAP_FACTOR_ROW);
      mapCol = (int)((double)col * DISP_TO_MAP_FACTOR_COL);
      //mapRow = (int)(((double)row*globalCMapPtr->getNumRows())/((double)DISPLAY_PIXEL_WIDTH));
      //mapCol = (int)(((double)col*globalCMapPtr->getNumCols())/((double)DISPLAY_PIXEL_HEIGHT));
      val = globalCMapPtr->getDataWin<double>(ACTIVE_LAYER, mapRow, mapCol);
      c = (int)(((double)(val+2))*255.0/4.0);

      globalMapArray[row][col][0] = colorMap[c][0][0];
      globalMapArray[row][col][1] = colorMap[c][0][1];
      globalMapArray[row][col][2] = colorMap[c][0][2];
    }
  }
  

  /*
  val = globalCMapPtr->getDataWin<double>(globalCurrentLayer, changedWinRow, changedWinCol);
  c = (int)(((double)(val+2))*255.0/4.0);
  
  globalMapArray[changedWinRow][changedWinCol][0] = colorMap[c][0][0];
  globalMapArray[changedWinRow][changedWinCol][1] = colorMap[c][0][1];
  globalMapArray[changedWinRow][changedWinCol][2] = colorMap[c][0][2];
  */

  glClear(GL_COLOR_BUFFER_BIT);
  glRasterPos2i(50, (int)(height-50-zoomy*(DISPLAY_PIXEL_HEIGHT)));
  glPixelZoom(zoomx, zoomy);
  glDrawPixels(DISPLAY_PIXEL_WIDTH, 
	       DISPLAY_PIXEL_HEIGHT, GL_RGB, GL_UNSIGNED_BYTE, globalMapArray);
  //glFlush();
  char tic[256];
  glPixelZoom(1.0, 1.0);

  int winBottomLeftRow = globalCMapPtr->getWindowBottomLeftUTMNorthingRowResMultiple();
  int startUTMRow=0, startWinRow=0;
  int ticSpacingRow = (int)floor(TIC_SPACING/((double)globalCMapPtr->getResRows()));
  int numTicsRow = (globalCMapPtr->getNumRows())/ticSpacingRow;


  for(startWinRow = 0; startWinRow < globalCMapPtr->getNumRows(); startWinRow++) {
    startUTMRow = winBottomLeftRow + startWinRow;// - layerDisplayUTMNorthingRowOffset[layerNum];
    if(startUTMRow% ticSpacingRow == 0) {
	break;
      }
    }
  for(int i=0; i<=numTicsRow; i++) {
    sprintf(tic, "%.2lf", (startUTMRow + i*ticSpacingRow)*globalCMapPtr->getResCols());
    bitmap_output((int)(50+15+(zoomx*DISPLAY_PIXEL_WIDTH)),(int)(50+(double)MAP_TO_DISP_FACTOR_ROW*zoomy*(double)(startWinRow + i*ticSpacingRow))-4,0, tic, GLUT_BITMAP_8_BY_13);
    glRasterPos2f(50+(DISPLAY_PIXEL_WIDTH)*zoomx, 50+(double)MAP_TO_DISP_FACTOR_ROW*zoomy*(startWinRow + i*ticSpacingRow));
    glDrawPixels(10,1, GL_RGB, GL_UNSIGNED_BYTE, horizontalTic);
  }

  int winBottomLeftCol = globalCMapPtr->getWindowBottomLeftUTMEastingColResMultiple();
  int startUTMCol=0, startWinCol=0;
  int ticSpacingCol = (int)floor(TIC_SPACING/((double)globalCMapPtr->getResCols()));
  int numTicsCol = (globalCMapPtr->getNumCols())/ticSpacingCol;

  for(startWinCol = 0; startWinCol < globalCMapPtr->getNumCols(); startWinCol++) {
    startUTMCol = startWinCol + winBottomLeftCol;// - layerDisplayUTMNorthingColOffset[layerNum];
    if(startUTMCol% ticSpacingCol == 0) {
	break;
      }
    }
  for(int i=0; i<=numTicsCol; i++) {
    sprintf(tic, "%.2lf", (startUTMCol + i*ticSpacingCol)*globalCMapPtr->getResCols());
    bitmap_output((int)(50-16+(startWinCol+i*ticSpacingCol)*zoomx*MAP_TO_DISP_FACTOR_COL),(int)(height-50-zoomy*(DISPLAY_PIXEL_HEIGHT)-25),0, tic, GLUT_BITMAP_8_BY_13);
    glRasterPos2f(50+(startWinCol+i*ticSpacingCol)*zoomx*MAP_TO_DISP_FACTOR_COL, height-50-zoomy*(DISPLAY_PIXEL_HEIGHT)-10);
    glDrawPixels(1,10, GL_RGB, GL_UNSIGNED_BYTE, verticalTic);
  }

  glRasterPos2i(width-50, (int)(height-50-zoomy*DISPLAY_PIXEL_HEIGHT));
  glPixelZoom(1.0, ((double)(zoomy*DISPLAY_PIXEL_HEIGHT))/256.0);
  glDrawPixels(25,256, GL_RGB, GL_UNSIGNED_BYTE, colorMap);
  glPixelZoom(1.0, 1.0);

  //printf("Done Displayin layer %d, took %lf\n", globalCurrentLayer, (TVNow()-before).dbl());

}


void global_cmap_reshape(int w, int h) {
  glViewport(0,0, (GLsizei)w, (GLsizei)h);
  glLoadIdentity();
  gluOrtho2D(0.0, (GLdouble)w, 0.0, (GLdouble)h);
}


void global_cmap_mouse(int x, int y)
{

  char val[256];
  glRasterPos2i(0,0);
  for(int i=0; i<8; i++) {
    for(int j=0; j<13; j++) {
      clearLine[i][j][0] = 128;
    }
  }


  sprintf(val, "X: %.4d, Y: %.4d, ", x, y);
  glColor3f(0.5,0.5,0.5);
  glRecti(0,0,8*strlen(val)+10, 13+10);
  glColor3f(1,1,1);
  bitmap_output(5,5,0, val, GLUT_BITMAP_8_BY_13);

}


void global_cmap_init() {
  //make the tics
  for(int i=0; i<10; i++) {
    verticalTic[0][i][0] = 0;
    verticalTic[0][i][1] = 0;
    verticalTic[0][i][2] = 0;
    horizontalTic[i][0][0] = 0;
    horizontalTic[i][0][1] = 0;
    horizontalTic[i][0][2] = 0;
  }

  int c;
  double blue_before_val, red_before_val, green_before_val;
  double blue_after_val, red_after_val, green_after_val;
  double blue_peak, red_peak, green_peak;
  double blue_peak_val, red_peak_val, green_peak_val;
  double blue_start, red_start, green_start;
  double blue_start_val, red_start_val, green_start_val;
  double blue_end, red_end, green_end;
  double blue_end_val, red_end_val, green_end_val;
  blue_before_val = 0;
  blue_start = 0;
  blue_start_val = 0;
  blue_peak =0.25;
  blue_peak_val = 255;
  blue_end = 0.5;
  blue_end_val = 0;
  blue_after_val = 0;

  red_before_val = 0;
  red_start = 0;
  red_start_val = 0.25;
  red_peak =0.75;
  red_peak_val = 255;
  red_end = 1;
  red_end_val = 255;
  red_after_val = 255;

  green_before_val = 0;
  green_start = 0.5;
  green_start_val = 0;
  green_peak =1;
  green_peak_val = 255;
  green_end = 1;
  green_end_val = 255;
  green_after_val = 0;


  for(int i=0; i<256; i++) {
    for(int j=0; j<25; j++) {
      c = i;
      if(c < red_start*256) {
	colorMap[i][j][0] = (GLubyte) red_before_val;
      } else if(c < red_peak*256) {
	colorMap[i][j][0] = (GLubyte)(c*((red_peak_val-red_start_val)/(256*red_peak-256*red_start)) +
					(red_peak_val - 256*red_peak*((red_peak_val - red_start_val)/(256*red_peak-256*red_start))));
      } else if(c < red_end*256) {
	colorMap[i][j][0] = (GLubyte)(c*((red_end_val-red_peak_val)/(256*red_end-256*red_peak)) +
					(red_end_val - 256*red_end*((red_end_val - red_peak_val)/(256*red_end-256*red_peak))));
      } else {
	colorMap[i][j][0] = (GLubyte) red_after_val;
      }
      
      if(c < green_start*256) {
	colorMap[i][j][1] = (GLubyte) green_before_val;
      } else if(c < green_peak*256) {
	colorMap[i][j][1] = (GLubyte)(c*((green_peak_val-green_start_val)/(256*green_peak-256*green_start)) +
					(green_peak_val - 256*green_peak*((green_peak_val - green_start_val)/(256*green_peak-256*green_start))));
      } else if(c < green_end*256) {
	colorMap[i][j][1] = (GLubyte)(c*((green_end_val-green_peak_val)/(256*green_end-256*green_peak)) +
					(green_end_val - 256*green_end*((green_end_val - green_peak_val)/(256*green_end-256*green_peak))));
      } else {
	colorMap[i][j][1] = (GLubyte) green_after_val;
      }

      if(c < blue_start*256) {
	colorMap[i][j][2] = (GLubyte) blue_before_val;
      } else if(c < blue_peak*256) {
	colorMap[i][j][2] = (GLubyte)(c*((blue_peak_val-blue_start_val)/(256*blue_peak-256*blue_start)) +
					(blue_peak_val - 256*blue_peak*((blue_peak_val - blue_start_val)/(256*blue_peak-256*blue_start))));
      } else if(c < blue_end*256) {
	colorMap[i][j][2] = (GLubyte)(c*((blue_end_val-blue_peak_val)/(256*blue_end-256*blue_peak)) +
					(blue_end_val - 256*blue_end*((blue_end_val - blue_peak_val)/(256*blue_end-256*blue_peak))));
      } else {
	colorMap[i][j][2] = (GLubyte) blue_after_val;
      }
    }
  }





}


void* global_cmap_start(void* arg) {
  if(globalCMapPtr == NULL) {
    printf("%s [%d]: globalCMapPtr not set - gonna crash!\n", __FILE__, __LINE__);
    printf("AIIIIIIEEEEEEEE!\n");
    exit(1);
  }

  int dummy=0;

  glutInit(&dummy, NULL);
  glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
  printf("Creating a window of size %d by %d\n",
	 DISPLAY_PIXEL_HEIGHT, DISPLAY_PIXEL_HEIGHT);
  glutInitWindowSize(DISPLAY_PIXEL_HEIGHT+200, DISPLAY_PIXEL_HEIGHT+100);
  glutInitWindowPosition(100, 100);

  global_cmap_init();

  glutCreateWindow("CMap Window Display");

  glClearColor(0.5,0.5,0.5,0);
  glShadeModel(GL_FLAT);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glutDisplayFunc(global_cmap_display);
  glutReshapeFunc(global_cmap_reshape);
  glutPassiveMotionFunc(global_cmap_mouse);

  glutMainLoop();

  return NULL;
}
