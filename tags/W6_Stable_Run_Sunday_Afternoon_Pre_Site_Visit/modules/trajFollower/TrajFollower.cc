#include <unistd.h>

#include <sys/time.h>
#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;

#include "TrajFollower.hh"
#include "sn_msg.hh"
#include "adrive_skynet_interface_types.h"
#include "DGCutils"
#include "find_force.hh"

int QUIT_PRESSED = 0;

// These extern's relate to the '--' command line options for TrajFollower (see TrajFollowerMain.cc for set-up details)
extern int           NOSTATE;
extern int           NODISPLAY;
extern int           USEMODEMAN;       // Use ModeManModule
extern int           SIM;
extern unsigned long SLEEP_TIME;
extern int           A_HEADING_REAR;
extern int           A_HEADING_FRONT;
extern int           A_YAW;
extern int           Y_REAR;
extern int           Y_FRONT;
extern int           LATERAL_FF_OFF;

#define MIN_SPEED 0.01


/******************************************************************************/
/******************************************************************************/
TrajFollower::TrajFollower(int sn_key) 
	: CSkynetContainer(SNtrajfollower, sn_key)
{
  cout << "Starting TrajFollower(...)" << endl;

	DGCcreateMutex(&m_trajMutex);
	m_drivesocket    = m_skynet.get_send_sock(SNdrivecmd);

  m_trajCount = 0;
  m_nActiveCount = 0;
	m_steer_cmd = 0.0;
	m_accel_cmd = 0.0;
	DGCgettime(m_timeBegin);


	EYerrorside yerrorside = (Y_FRONT==1 ? YERR_FRONT : YERR_BACK);
	EAerrorside aerrorside = (A_HEADING_FRONT==1 ? AERR_FRONT : ( A_HEADING_REAR==1 ? AERR_BACK : AERR_YAW));

  m_pPIDcontroller = new CPID_Controller(yerrorside,
																				 aerrorside,
																				 LATERAL_FF_OFF == 0);
 
	// 3rd order traj.
	m_pTraj   = new CTraj(3);

  char stateFileName[256];
  char plansFileName[256];
	char planStartsFileName[256];
	char testFileName[256];
	
  char stateFilePath[256];
  char plansFilePath[256];
	char planStartsFilePath[256];
	char testFilePath[256];
	  
  char lnCmd[256];

  time_t t = time(NULL);
  tm *local;
  local = localtime(&t);

  sprintf(stateFileName, "state_%04d%02d%02d_%02d%02d%02d.dat", 
					local->tm_year+1900, local->tm_mon+1, local->tm_mday,
					local->tm_hour, local->tm_min, local->tm_sec);

	sprintf(testFileName, "test_%04d%02d%02d_%02d%02d%02d.dat", 
					local->tm_year+1900, local->tm_mon+1, local->tm_mday,
					local->tm_hour, local->tm_min, local->tm_sec);

  sprintf(plansFileName, "plans_%04d%02d%02d_%02d%02d%02d.dat", 
					local->tm_year+1900, local->tm_mon+1, local->tm_mday,
					local->tm_hour, local->tm_min, local->tm_sec);

  sprintf(planStartsFileName, "planstarts_%04d%02d%02d_%02d%02d%02d.dat", 
					local->tm_year+1900, local->tm_mon+1, local->tm_mday,
					local->tm_hour, local->tm_min, local->tm_sec);

  sprintf(stateFilePath, "logs/%s", stateFileName);
  sprintf(testFilePath, "logs/%s", testFileName);
  sprintf(plansFilePath, "logs/%s", plansFileName);
  sprintf(planStartsFilePath, "logs/%s", planStartsFileName);

	m_outputPlanStarts.open(planStartsFilePath);
  m_outputPlans.open(plansFilePath);
  m_outputState.open(stateFilePath);
  m_outputTest.open(testFilePath);

	// make state.dat, plans.dat, planstarts.dat links to the latest data files
	lnCmd[0] = '\0';
	strcat(lnCmd, "cd logs; ln -fs ");
	strcat(lnCmd, stateFileName);
	strcat(lnCmd, " state.dat; cd ..");
	system(lnCmd);
	lnCmd[0] = '\0';
	strcat(lnCmd, "cd logs; ln -fs ");
	strcat(lnCmd, plansFileName);
	strcat(lnCmd, " plans.dat; cd ..");
	system(lnCmd);
	lnCmd[0] = '\0';
	strcat(lnCmd, "cd logs; ln -fs ");
	strcat(lnCmd, planStartsFileName);
	strcat(lnCmd, " planstarts.dat; cd ..");
	system(lnCmd);
	lnCmd[0] = '\0';
	strcat(lnCmd, "cd logs; ln -fs ");
	strcat(lnCmd, testFileName);
	strcat(lnCmd, " test.dat; cd ..");
	system(lnCmd);

  m_outputState << setprecision(20);
  m_outputPlans << setprecision(20);
  m_outputPlanStarts << setprecision(20);
  m_outputTest<<setprecision(20);

  // print data format information in a header at the top of the file
  // Note that this header now reflects the or
//  cout << "% Time[sec] | Northing[m] | Easting[m] | Altitude[m]"
//       << " | Vel_N[m/s] | Vel_E[m/s] | Vel_D[m/s]"
//       << " | Acc_N[m/s] | Acc_E[m/s] | Acc_D[m/s]"
//       << " | Roll[rad] | Pitch[rad] | Yaw[rad]"
//       << " | RollRate[rad/s] | PitchRate[rad/s] | YawRate[rad/s]"
//       << " | RollAcc[rad/s] | PitchAcc[rad/s] | YawAcc[rad/s]"

  // leave this at the end 
  cout << "TrajFollower(...)  Finished" << endl;
}

TrajFollower::~TrajFollower() 
{
  delete m_pTraj;
  delete m_pPIDcontroller;
}

/******************************************************************************/
/******************************************************************************/
void TrajFollower::Active() 
{
	// timers to make sure our control frequency is what we set (not
	// affected by how long it takes to compute each cycle)
	unsigned long long timestamp1, timestamp2;

  cout << "Starting Active Function" << endl;

	drivecmd_t my_command;
	memset(&my_command, 0, sizeof(my_command)); // init the cmd to 0 to appease the memprofiler gods

  while(!QUIT_PRESSED) 
  {
		// time checkpoint at the start of the cycle
		DGCgettime(timestamp1);

    // increment module counter
    m_nActiveCount++;

    // Get state from the simulator or vstate 
		/* This sets m_state */
		UpdateState();
		UpdateActuatorState();

		// if our speed was too low, set it as if we're moving in the same direction
		// at the minimum speed WITH THE WHEELS POINTED STRAIGHT
		if( m_state.Speed2() < MIN_SPEED )
		{
			m_state.Vel_N = MIN_SPEED * cos(m_state.Yaw);
			m_state.Vel_E = MIN_SPEED * sin(m_state.Yaw);
		}

    /* Compute the control inputs. */
		DGClockMutex(&m_trajMutex);
		m_pPIDcontroller->getControlVariables(&m_state, m_actuatorState.m_steerpos * VEHICLE_MAX_AVG_STEER, &m_accel_cmd, &m_steer_cmd);
		DGCunlockMutex(&m_trajMutex);

		double steer_Norm, accel_Norm, brake_Norm, gas_Norm;
		
		steer_Norm = m_steer_cmd/VEHICLE_MAX_AVG_STEER;
		steer_Norm = fmax(fmin(steer_Norm, 1.0), -1.0);

		accel_Norm = get_pedal(m_state.Speed2(), m_accel_cmd*VEHICLE_MASS, 1);
		//if (accel_Norm > 1) accel_Norm = 1;
		//if (accel_Norm < -1) accel_Norm = -1;

		/* no longer needed, as adrive takes -1 to 1 for accel    
			 if(accel_Norm >=0.0) {
			 brake_Norm = 0.0;
			 gas_Norm = accel_Norm;
			 }
			 else // if(accel_Norm < 0.0) 
			 {
			 gas_Norm = 0.0;
			 brake_Norm = -accel_Norm;
			 }

		*/

		my_command.my_command_type = set_position;

		//actuator command testing functions//
		/*
			if((m_nActiveCount/10) % 4 == 0) {
			steer_Norm = -.2;
			brake_Norm = 0;
			gas_Norm = 0;
			}
			if((m_nActiveCount/10) % 4 == 1) {
			gas_Norm = 0;
			steer_Norm = 0;
			brake_Norm = .2;
			}
			if((m_nActiveCount/10) % 4 == 2){
			gas_Norm = 0;
			steer_Norm = .2;
			brake_Norm = .4;
			}
			if((m_nActiveCount/10) % 4 == 3){
			gas_Norm = 0;
			steer_Norm = 0;
			brake_Norm = .2;
			}	 
		*/
		    	    
		my_command.my_actuator = steer;
		my_command.number_arg = steer_Norm;
		m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);
		my_command.my_actuator = accel;
		my_command.number_arg = accel_Norm;
		m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);		    

		/* no longer needed
			 my_command.my_actuator = brake;
			 my_command.number_arg = brake_Norm;
			 m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);

			 my_command.my_actuator = gas;
			 my_command.number_arg = gas_Norm;
			 m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);
		********/
		
		//strictly speaking, don't need to do this right now
		/*

		my_command.my_command_type = get_position;

		double steer_act, gas_act, brake_act;
		    
		my_command.my_actuator = steer;
		steer_act = send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);
		    
		my_command.my_actuator = gas;
		gas_act = send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);
		my_command.my_actuator = brake;
		brake_act = send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);

		m_outputTest <<"steer, gas, brake actual settings: "<<steer_act<<' '<<gas_act<<' '<<brake_act<<endl;
		*/

  
    // output the state to the log
    // Note that this logs state ONLY now, in the order
    // that it is in VehicleState.
    m_outputState <<
			DGCtimetosec(m_state.Timestamp) << ' ' <<
			m_state.Northing << ' ' <<
			m_state.Easting << ' ' <<
			m_state.Altitude << ' ' <<
			m_state.Vel_N << ' ' <<
			m_state.Vel_E << ' ' <<
			m_state.Vel_D << ' ' <<
			m_state.Acc_N << ' ' <<
			m_state.Acc_E << ' ' <<
			m_state.Acc_D << ' ' <<
			m_state.Roll << ' ' <<
			m_state.Pitch << ' ' <<
			m_state.Yaw << ' ' <<
			m_state.RollRate << ' ' <<
			m_state.PitchRate << ' ' <<
			m_state.YawRate << ' ' <<
			m_state.RollAcc << ' ' <<
			m_state.PitchAcc << ' ' <<
			m_state.YawAcc << ' ' <<
			m_state.Northing_rear() << ' ' <<                //Northing position of center of *REAR* axle
			m_state.Easting_rear() << ' ' <<                 //Easting position of center of *REAR* axle
			m_state.Vel_N_rear() << ' ' <<                   //1st diff of Northing at center of *REAR* axle
			m_state.Vel_E_rear() << ' ' <<                   //1st diff of Easting at center of *REAR* axle
      
			endl;

		// time checkpoint at the end of the cycle
		DGCgettime(timestamp2);

		// now sleep for SLEEP_TIME usec less the amount of time the
		// computation took
		int delaytime = SLEEP_TIME - (timestamp2 - timestamp1);
		if(delaytime > 0)
			usleep(delaytime);
  } // end while(!QUIT_PRESSED) 

  cout << "Finished Active State" << endl;
}

/******************************************************************************/
/******************************************************************************/
void TrajFollower::Comm()
{
	int	trajSocket;
	int bytesLeft;
	int bytesReceived;
	char *pInTraj;
	int i;

	if (USEMODEMAN) {
	  trajSocket = m_skynet.listen(SNmodemantraj, SNModeMan);
	}
	else {
	  trajSocket = m_skynet.listen(SNtraj, SNplanner);
	}

	while(!QUIT_PRESSED)
	{
		RecvTraj(trajSocket, m_pTraj);

		/* commenting this out as we're logging plans WAY too quickly.
		** will make this an option soon, this is just a quick fix
		m_pTraj->print(m_outputPlans);
		m_outputPlans << endl;
		m_outputPlanStarts << m_pTraj->getNorthing(0) << ' '
											 << m_pTraj->getEasting(0) << endl;
		*/

		DGClockMutex(&m_trajMutex);
		m_pPIDcontroller->SetNewPath(m_pTraj);
		DGCunlockMutex(&m_trajMutex);

		m_trajCount++;
	}
}
// end TrajFollower.cc
