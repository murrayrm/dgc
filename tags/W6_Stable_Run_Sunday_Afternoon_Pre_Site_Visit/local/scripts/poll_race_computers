#!/bin/bash

# This script checks to see if ntp processes are running on the race
# machines.  It's useful to have run distribute_sshkeys.sh before
# running this.

COMPUTERS="skynet1 skynet2 skynet4 skynet5 skynet6 skynet7 grandchallenge gcdev tantalum cobalt nickel moscow"
WARN_MSG="Alice will be disconnected from the network very soon!\
          Save your work and logout"

if [ $# -eq 0 ] ; then

  echo
  echo "  Usage: $0 option"
  echo
  echo "  Your options are..."
  echo
  echo "    <command>: Run custom command (no spaces allowed)."
  echo "    warn: Do a 'wall' on each machine, warning of imminent shutdown."
  echo "    date: Display date on each race machine."
  echo "    ntp: Display ntp processes on each race machine."
  echo "    ntpq: Display ntpq output on each race machine."
  echo "    ping: Test each race machine for network connectivity."
  echo "    psd: Display output of ps aux | grep skynetd on each machine."
  echo "    psm: Display output of ps aux | grep moduleStarter on each machine."
  echo "    uptime: Display uptime of each race machine."
  echo "    users: List active users on each race machine."
  exit 1

fi

echo

for comp in $COMPUTERS
  do
  case "$1" in
      "warn" )
	  echo "Warning users on $comp"
	  ssh $comp "wall $WARN_MSG"
	  ;;
      "date" )
	  echo -n "Date on $comp is "
	  ssh $comp date
	  echo
	  ;;
      "ntp" )
	  echo "Checking for ntpd and ntpclient on $comp..."
	  ssh $comp ps aux | grep ntp
	  echo
	  ;;
      "ntpq" )
	  echo "Displaying output of ntpq on $comp..."
	  ssh $comp ntpq -p
	  echo
	  ;;
      "ping" )
	  echo "Sending 2 pings to $comp..."
	  ping -c 2 -w 5 $comp
	  echo
	  ;;
      "psd" )
	  echo "Checking for skynetd on $comp..."
	  ssh $comp ps aux | grep skynetd
	  echo
	  ;;
      "psm" )
	  echo "Checking for moduleStarter on $comp..."
	  ssh $comp ps aux | grep moduleStarter
	  echo
	  ;;
      "uptime" )
	  echo -n "Uptime for $comp... "
	  ssh $comp uptime
	  ;;
      "users" )
	  echo -n "Users currently on $comp... "
	  ssh $comp users
	  echo
	  ;;
      * )
	  echo "  $1 on $comp... "
	  ssh $comp $1
	  echo
  esac
done
