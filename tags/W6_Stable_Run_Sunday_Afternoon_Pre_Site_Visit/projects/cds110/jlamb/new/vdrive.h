/*
 * vdrive.h - header file for vdrive.c
 *
 * RMM/WH, 21 Feb 04
 *
 * This file contains the function and variable definitions for anything
 * in vdrive.c that might be accessed externally.
 *
 */

/* vdrive mode switching comments */
int mode_manual(long); 
int mode_remote(long); 
int mode_off(long);
int mode_autonomous(long);
int mode_direct(long); /* Direct control of vehicle acceleration */
int mode_test(long);
int mode_estop(long);

int user_cap_toggle(long arg);

int user_quit(long arg);
int user_estop_toggle(long arg);
int user_estop_pause(), user_estop_resume();

int full_stop(long arg);

int steer_inc(long arg);
int steer_dec(long arg);
int accel_inc(long arg);
int accel_dec(long arg);

/*
 * Parameter definitions 
 *
 */
#define MAX_VEL_THRESHOLD 3.0		/* stomp on brakes below 3 m/s */
