#ifndef LADARFINDER_HH
#define LADARFINDER_HH

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "LadarFinderDatum.hh"

#include "frames/frames.hh" // for XYcoord
#include "rddf.hh"
#include "ppspline.hh"
#include "traj.h"

// LadarFinder will generate a list of control points, and this defines
// the spacing (in meters, generally along the trackline) between them.
#define CONTROL_POINT_SPACING  20.0
// Define the maximum distance (meters along the trackline) to put control 
// points. This and the spacing will define the number of points actually used
#define CONTROL_POINT_MAXDIST 1000
// To make it easy, let's just fix the maximum number of control points
#define MAX_CONTROL_POINTS 100
// Define a nominal speed (in m/s) to be going along the spline.  This will be 
// used to set the breaks of the spline at reasonable intervals.
#define NOMINAL_SPEED  5.0

using namespace std;
using namespace boost;

class LadarFinder : public DGC_MODULE 
{
  public:
    // and change the constructors
    LadarFinder();
    ~LadarFinder();

    // The states in the state machine.
    // Uncomment these if you wish to define these functions.

    void Init();
    void Active();
    //  void Standby();
    void Shutdown();
    //  void Restart();

    // The message handlers. 
    //  void InMailHandler(Mail & ml);
    //  Mail QueryMailHandler(Mail & ml);

    // the status function.
    //string Status();

    // Any functions which run separate threads

    // Method protocols
    int  InitLadar();
    void LadarGetMeasurements();
    void LadarShutdown();

    /*! Sends a query message for a new state struct and update the datum with 
     ** the response. */
    void UpdateState();
    
    void SparrowDisplayLoop();
    void UpdateSparrowVariablesLoop();

  private:
    // and a datum named d.
    LadarFinderDatum d;

    /*! The RDDF class handles loading of the RDDF file. */
    RDDF rddf;

    /*! The vector waypoints will hold all of the RDDF information. */
    RDDFVector waypoints;
};

// enumerate all module messages that could be received
namespace LadarFinderMessages 
{
  enum 
  {
    // we are not yet receiving messages.
    // A place holder
    NumMessages
  };
};






#endif
