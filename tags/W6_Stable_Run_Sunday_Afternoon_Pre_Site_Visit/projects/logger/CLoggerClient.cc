#include "CLoggerClient.hh"

using namespace std;


CLoggerClient::CLoggerClient()
{
  /** make some mutexes (before we can use our functions) */
  DGCcreateMutex(&logger_state_mutex);
  DGCcreateMutex(&module_name_mutex);
  DGCcreateMutex(&debug_level_mutex);
  DGCcreateMutex(&server_last_seen_mutex);
  DGCcreateMutex(&local_logging_mutex);

  setDebugLevel(DEFAULT_DEBUG);
  printDebug("Begin CLoggerClient Constructor", 2);

  module_name = new char[MAX_MODULENAME_LENGTH];
  setModuleName(DEFAULT_MODULENAME);

  log_directory_base = new char[MAX_PATH_LENGTH];
  string dir_string = getLogDirectoryBase();
  cout << "string is " << dir_string << endl;
  strcpy(log_directory_base, dir_string.c_str());
  setLocalLogging(false);

  setLastSeen(0); // we haven't seen the server yet

  session_timestamp = new char[MAX_TIMESTAMP_LENGTH];
  session_timestamp[0] = '\0';
  logging_enabled = false;

  /** start the receive thread */
  DGCstartMemberFunctionThread(this, &CLoggerClient::RecvThread);

  printDebug("End CLoggerClient Constructor", 2);
}



CLoggerClient::~CLoggerClient()
{
  /** delete the mutexes */
  DGCdeleteMutex(&logger_state_mutex);
  DGCdeleteMutex(&module_name_mutex);
  DGCdeleteMutex(&debug_level_mutex);
  DGCdeleteMutex(&server_last_seen_mutex);
  DGCdeleteMutex(&local_logging_mutex);

  delete module_name;
  delete log_directory_base;
  delete session_timestamp;
}



string CLoggerClient::getLogDir()
{
  string ret;

  /** verify that we have a server connection */
  if (!connectionStatus() && getLoggingEnabled())
    // we just lost the connection (it used to be on)
    // do the same thing as "got server off", but logging was on
    {
      lockState();
      logging_enabled = false;
      session_timestamp[0] = '\0';
      unlockState();
    }

  if (getLoggingEnabled())
    // directory already exists
    ret = getDirFromTimestamp();
  else
    // logging is off
    {
      lockState();
      char temp = session_timestamp[0];
      unlockState();
      if( temp == '\0')
	// give them a new fake directory
	{
	  //store fake timestamp
	  unsigned long long time;
	  DGCgettime(time);
	  unsigned long long the_time_round = llround(DGCtimetosec(time));
	  lockState();
	  session_timestamp[0] = '\0';
	  sprintf(session_timestamp, "%lli", the_time_round);
	  strcat(session_timestamp, FAKE_TIMESTAMP_MODIFIER);
	  unlockState();

	  createDirFromTimestamp();

	  ret = getDirFromTimestamp();
	}
      else
	{
	  ret = getDirFromTimestamp();
	}
    }

  return ret;
}



bool CLoggerClient::connectionStatus()
{
  unsigned long long time, ull_server_delay;
  DGCgettime(time);
  ull_server_delay = time - getLastSeen();
  double server_delay = DGCtimetosec(ull_server_delay);
  //  printDebug("server_delay is " + (string)string_server_delay, 2);

  return (server_delay < MAX_SERVER_DELAY);
}



string CLoggerClient::getUser()
{
  char* user = new char[MESSAGE_LENGTH];
  user[0] = '\0';
  passwd* p_user_passwd = getpwuid(geteuid());
  strcat(user, p_user_passwd->pw_name);
  string ret(user);
  delete user;
  return ret;
}



string CLoggerClient::getIPString()
{
  char* ip = new char[MESSAGE_LENGTH];
  ip[0] = '\0';


  // half-hacked
  char* iface;
#warning "hack... hard coded eth0... won't work for macs or wireless..."
  iface = "eth0";

  // blatantly copied from skynet_static something or other
  struct ifaddrs *myaddrs, *ifa;
  struct sockaddr_in *s4 = NULL;
  int status;
  
  status = getifaddrs(&myaddrs);
  if (status != 0)
    {
      perror("getifaddrs");
      exit(1);
    }
  
  for (ifa = myaddrs; ifa != NULL; ifa = ifa->ifa_next)
    {
      if ((ifa->ifa_addr != NULL) && ((ifa->ifa_flags & IFF_UP) != 0))
	{
	  if ( strcmp(ifa->ifa_name, iface) == 0)
	    {
	      s4 = (struct sockaddr_in *)(ifa->ifa_addr);
	    }
	}
    }
  if( s4 == NULL)
    {
      cout << "CLoggerClient::storeMyIp eth0 not found.  Network unconfigured?\n" << endl;
      exit(1);
    }
  freeifaddrs(myaddrs);
  uint32_t ip_as_uint =  ntohl((s4->sin_addr).s_addr);

  //###  
  //  printf("%d.%d.%d.%d", ip_as_uint >> 24, (ip_as_uint >> 16) & 0xff, 
  //	 (ip_as_uint >> 8) & 0xff, ip_as_uint & 0xff);
  //  cout << endl;
  //###

  // they must have allocated it before
  sprintf(ip, "%d.%d.%d.%d", ip_as_uint >> 24, (ip_as_uint >> 16) & 0xff, 
	  (ip_as_uint >> 8) & 0xff, ip_as_uint & 0xff);

  string ret(ip);
  delete ip;
  return ret;
}



//
// DEPRACATED FUNCTION
/**
 * cat the directory name onto the end of the dest
 */
/*
void CLoggerClient::catLogDirName(char* dest, bool fake_it)
{  
  // the folder MAY OR MAY NOT EXIST AT THIS POINT!
  double server_delay;
  unsigned long long time, ull_server_delay;
  DGCgettime(time);
  DGClockMutex(&server_last_seen_mutex);
  ull_server_delay = time - server_last_seen;
  DGCunlockMutex(&server_last_seen_mutex);
  server_delay = DGCtimetosec(ull_server_delay);
  if (server_delay > MAX_SERVER_DELAY) // haven't seen server for a while (or ever)
      cout << "NOT USING LOGGER!!!" << endl;

  strcat(dest, dgc_dir);
  strcat(dest, "/");
  strcat(dest, LOG_DIRECTORY);
  strcat(dest, "/");

  if (fake_it)
    {
      if (loggingEnabled())
	{
	  DGClockMutex(&session_timestamp_mutex);
	  strcat(dest, session_timestamp);   //must already exist
	  DGCunlockMutex(&session_timestamp_mutex);
	}
      else
	{
	  DGClockMutex(&session_timestamp_mutex);
	  long long int the_time_round = llround(DGCtimetosec(time));
	  session_timestamp[0] = '\0';
	  sprintf(session_timestamp, "%lli", the_time_round);
	  strcat(session_timestamp, FAKE_TIMESTAMP_MODIFIER);
	  strcat(dest, session_timestamp);
	  DGCunlockMutex(&session_timestamp_mutex);
	}
    }
  else
    {
      while (!loggingEnabled())
	{
	  cout << "Waiting for Logger to start log..." << endl;
	  usleep(1000000);
	}
      DGClockMutex(&session_timestamp_mutex);
      strcat(dest, session_timestamp);
      DGCunlockMutex(&session_timestamp_mutex);
    }

  strcat(dest, "/");
  DGClockMutex(&module_name_mutex);
  strcat(dest, module_name);
  DGCunlockMutex(&module_name_mutex);
  strcat(dest, "/");
}
*/



/** 
 * State flow hierarchy:
 * Get message -> update server_last_seen
 *
 * message is client message (sent by us): ignore
 * message is "server, logging on"
 *   (logging_enabled):
 *     (timestamp has changed): change to new timestamp, create new dir
 *     (timestamp is same): nothing
 *   (!logging_enabled): turn on logging, change session_timestamp, create dir
 * message is "server, logging off"
 *   (logging_enabled): clear session_timestamp, turn off logging
 *   (!logging_enabled): nothing
 *
 * there, thats' not so bad...
 */
void CLoggerClient::RecvThread()
{
  printDebug("CLoggerClient::RecvThread() has been called", 2);

  int recvSocket = m_skynet.listen(SNloggerstring, ALLMODULES);
  char* message = new char[MESSAGE_LENGTH];
  int length = 0;


  while (true)
    {
      // get the message
      printDebug("trying to receive message...", 2);
      RecvLoggerString(recvSocket, message);
      printDebug("(CLoggerClient received \"" + (string)message + "\")", 1);
      
      // update server_last_seen
      lockLastSeen();
      DGCgettime(server_last_seen);
      unlockLastSeen();

      length = strlen(message);
      lockState(); // state should be locked for this whole section!
      if (length >= TAGLENGTH)
	{

	  if ( (strncmp(message, TAG_LOGTIMEON, TAGLENGTH) == 0) )
	    // got "server, log on"
	    {
	      if (logging_enabled)
		{
		  // get timestamp from this message
		  char* time_from_msg = message + TAGLENGTH;
		  if (strcmp(time_from_msg, session_timestamp) != 0)
		    // got a new timestamp, so start NEW RUN
		    {
		      strcpy(session_timestamp, time_from_msg);
		      createDirFromTimestamp();
		    }
		  //  delete time_from_msg; // do i need to delete this???
		}
	      else
		// logging is off, so turn it on
		{
		  //turn on, create dir, change timestamp
		  char* time_from_msg = message + TAGLENGTH;
		  logging_enabled = true; // might be wrong...need to clear session_timestamp???
		  strcpy(session_timestamp, time_from_msg);
		  createDirFromTimestamp();
		  // delete time_from_msg;
		}
	    }

	  else if ( (strncmp(message, TAG_LOGOFF, TAGLENGTH) == 0) )
	    // got "server, log off"
	    {
	      if (logging_enabled)
		// turn logging off
		{
		  logging_enabled = false;
		  session_timestamp[0] = '\0';
		}
	    }
	}
      else
	printDebug("Message was unusually short: \"" + (string)message
		   + "\"", 2);
      unlockState();
    } // end of while (true)
     
  delete message;
}



/** prints out debug messages */
void CLoggerClient::printDebug(string message, int req_level, bool newline)
{
  if (getDebugLevel() >= req_level)
    {
      cout << message;
      if (newline)
	cout << endl;
    }
}



/** creates a new dir for logs
 * IMPORTANT:  the state mutex must be locked for the duration
 * of this function.
 */
void CLoggerClient::createDirFromTimestamp()
{
  char* sys_command = new char[MAX_COMMAND_LENGTH];
  sys_command[0] = '\0';
  strcat(sys_command, CMD_MKDIR);
  strcat(sys_command, " ");
  strcat(sys_command, log_directory_base); // defined by constructor
  strcat(sys_command, session_timestamp);
  strcat(sys_command, "/");
  strcat(sys_command, module_name);
  strcat(sys_command, "/");
  int result = system(sys_command); // run command
  printDebug("Creating directory with command \""
	     + (string)sys_command + "\"", 1);
  if (result != 0)
    cerr << "*******************************************\n"
	 << "* Couldn't create directory using command:\n"
	 << "*   " << (string)sys_command << "\n"
	 << "* \n"
	 << "* Logging won't work!\n"
	 << "*******************************************" << endl;
  delete sys_command;
}



string CLoggerClient::getDirFromTimestamp()
{
  string ret = "";
  lockState();
  ret += (string)log_directory_base + session_timestamp + "/" + module_name + "/";
  unlockState();
  return ret;
}



/**
 * Attempts to read in the DGCDIRFILE in the current directory to get 
 * the path to the root (usually will be "/home/user/dgc")
 */
string CLoggerClient::getLogDirectoryBase()
{
  printDebug("getLogDirectoryBase() called.", 2);
  string ret = "";

  ifstream file(DGCDIRFILE);
  if (file.is_open())
    {
      string from_file;
      file >> from_file;
      printDebug("From file \"" + (string)DGCDIRFILE + "\", read \"" + from_file + "\"", 1);
      ret = from_file;
      if (ret[ret.length() - 1] != '/') // make sure last char is a "/"
	ret += "/";
      ret += (string)LOG_DIRECTORY + "/";
      setLocalLogging(false); // this is good
    }
  else
    {
      setLocalLogging(true); // this is discouraged

      char* working_dir = new char[MAX_PATH_LENGTH];
      working_dir[0] = '\0';
      getcwd(working_dir, MAX_PATH_LENGTH);
      ret = (string)working_dir;
      ret += "/" + (string)LOG_DIRECTORY + "/";
      delete working_dir;

      cerr << "*******************************************\n"
	   << "* Couldn't open file:\n"
	   << "*   " << DGCDIRFILE << "\n"
	   << "* \n"
	   << "* Logs will be place in " << ret << "\n"
	   << "* instead of the proper place!\n"
	   << "* Remote might not work!\n"
	   << "* See the README for how to create this file.\n"
	   << "*******************************************" << endl;
    }

#warning "can you do file.close() if it's not open?"
  file.close();

  return ret;
}



void CLoggerClient::addRelFolder(string rel_folder_name)
{
  printDebug("addRelFolder called with string \"" + rel_folder_name + "\"", 2);
  char* complete_path = new char[MAX_PATH_LENGTH];
  complete_path[0] = '\0';

  string user = getUser();   //get username and ip
  string ip = getIPString();

  strcat(complete_path, user.c_str());
  strcat(complete_path, "@");
  strcat(complete_path, ip.c_str());
  strcat(complete_path, ":");

  strcat(complete_path, log_directory_base);
  if (rel_folder_name != ".") // don't do anything if they added the current dir
    strcat(complete_path, rel_folder_name.c_str());
  if (complete_path[strlen(complete_path) - 1] != '/')
    strcat(complete_path, "/"); // make sure it has the last "/" on it
  
  //create the complete message to send
  char* message = new char[MESSAGE_LENGTH];
  message[0] = '\0';
  strcat(message, TAG_ADDFOLDER);
  strcat(message, complete_path);

  SendLoggerString(sendSocket, message);

  delete complete_path;
  delete message;
}



/** Accessors */
int CLoggerClient::getDebugLevel()
{
  lockDebug();
  int ret = debug_level;
  unlockDebug();
  return ret;
}

string CLoggerClient::getModuleName()
{
  lockName();
  string ret = module_name;
  unlockName();
  return ret;
}

bool CLoggerClient::getLoggingEnabled()
{
  lockState();
  bool ret = logging_enabled;
  unlockState();
  return ret;
}


/** Mutators */
void CLoggerClient::setDebugLevel(int level)
{
  lockDebug();
  debug_level = level;
  unlockDebug();
}


void CLoggerClient::setModuleName(string des_name)
{
  lockName();
  module_name[0] = '\0';
  strcpy(module_name, des_name.c_str());
  unlockName();
}



/** Bunch of mutex lock'ers and unlock'ers */
void CLoggerClient::lockState() { DGClockMutex(&logger_state_mutex); }
void CLoggerClient::unlockState() { DGCunlockMutex(&logger_state_mutex); }
void CLoggerClient::lockName() { DGClockMutex(&module_name_mutex); }
void CLoggerClient::unlockName() { DGCunlockMutex(&module_name_mutex); }
void CLoggerClient::lockDebug() { DGClockMutex(&debug_level_mutex); }
void CLoggerClient::unlockDebug() { DGCunlockMutex(&debug_level_mutex); }
void CLoggerClient::lockLastSeen() { DGClockMutex(&server_last_seen_mutex); }
void CLoggerClient::unlockLastSeen() { DGCunlockMutex(&server_last_seen_mutex); }
void CLoggerClient::lockLocalLogging() { DGClockMutex(&local_logging_mutex); }
void CLoggerClient::unlockLocalLogging() { DGCunlockMutex(&local_logging_mutex); }


/** a few private accessors and mutators */
unsigned long long CLoggerClient::getLastSeen()
{
  lockLastSeen();
  unsigned long long ret = server_last_seen;
  unlockLastSeen();
  return ret;
}

void CLoggerClient::setLastSeen(unsigned long long des_time)
{
  lockLastSeen();
  server_last_seen = des_time;
  unlockLastSeen();
}

bool CLoggerClient::getLocalLogging()
{
  lockLocalLogging();
  bool ret = local_logging;
  unlockLocalLogging();
  return ret;
}

void CLoggerClient::setLocalLogging(bool value)
{
  lockLocalLogging();
  local_logging = value;
  unlockLocalLogging();
}
