#include <iostream>
#include <iomanip>
#include "DGCutils"
#include "CLogger.hh"
#include "LoggerConfig.hh"

using namespace std;


int main(int argc, char** argv)
{
  int skynet_key;
  if ( (skynet_key = atoi(getenv("SKYNET_KEY"))) )
    cout << "Using skynet key: " << skynet_key << endl;
  else
    {
      cout << "You need to set a skynet key!" << endl;
      return 1;
    }


  CLogger loggerObj(skynet_key);
  loggerObj.LoggerActiveLoop();


  /* All of the code below is commented out! */


  /*
  string str;
  str = loggerObj.getTimestamp();
  cout << "Timestamp is \"" << str << "\"" << endl;

  while (true)
    {
      if (false)
	{
	  cout << "sending message...";
	  cout.flush();
	  loggerObj.sendDebug();
	  cout << "done." << endl;
	}
      usleep(500000);
    }
  */
  /*
  modulename myself = SNlogger;
  modulename sender = ALLMODULES;
  skynet skynetObj(myself, skynet_key);
  printf("My module ID: %u\n", skynetObj.get_id());
  sn_msg myinput = SNtraj;

  int sock1 = skynetObj.listen(myinput, sender);
  char* databuf[BUFSIZE];
  unsigned int datalen = skynetObj.get_msg(sock1, databuf, BUFSIZE, 0);
  printf("%s\n", (char*)databuf);
  */

  /*
  modulename myself = SNlogger;
  modulename sender = ALLMODULES;
  sn_msg myinput = SNtraj;
  void* databuf = malloc(MAX_DATALEN);
  bzero(databuf, MAX_DATALEN);
  skynet Skynet(myself, sn_key);

  int sock1 = Skynet.listen(myinput, sender);
  unsigned int datalen = Skynet.get_msg(sock1, databuf, MAX_DATALEN, 0);

  printf("got msg.  datalen= %u\n", datalen);
  */
  
  /*print data*/
  /*  
  uint i;
  for(i = 0; i < datalen / sizeof(int); i++)
    {
      printf("%u\t",*((int*)databuf) + i);
      if (i % 10 == 0)
	cout << endl;
    }
  printf("\n");
  free(databuf);
  */

  return 0;
}
