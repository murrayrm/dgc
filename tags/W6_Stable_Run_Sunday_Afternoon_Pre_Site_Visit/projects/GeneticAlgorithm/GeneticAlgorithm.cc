/**
 * $Id$
 */
#include "GeneticAlgorithm.hh"
#include "WeightsIndividual.hh"
#include "Arbiter2/IO.hh"
#include <iostream>  // cout
#include <ctime>     // time()
#include <algorithm> // swap
using namespace std;

//################ Global rand function (better) #######################################

/** variable used in my_rand */
int nextValue; 

int my_rand(void)
{
  const long int multiplier = 16807;
  const long int quotient = 127773; // MY_RAND_MAX / multiplier
  const long int remainder = 2836;  // MY_RAND_MAX % multiplier
  long int temp = multiplier * (nextValue % quotient) - remainder * (nextValue / quotient);
  nextValue = (temp > 0) ? temp : temp + MY_RAND_MAX;
  return (unsigned int) nextValue;
}

void my_srand(unsigned int seed)
{
  nextValue = seed;
}

//################ Other global stuff #######################################################

/** The list of voters, exactly as the Arbiter class would generate this list */
list<Voter> voters;

/** A vector that tells if a Voter is enabled of not (indexes as of ArbiterInput) */
vector<bool> enabled_voters;

/** A VoterHandler, this is the object that takes the decision about which arc
 * to pick, and therefore the class we use for the fitness evaluation */
VoterHandler vHandler(false);

/** Help class to write the calculated weights to log (global since it's used
 * in the GeneticAlgorithm object too) */
WeightsWriter weightLog;

/** The struct used to store steer/speed */
VDrive_CmdMotionMsg humanCmd;

//################ GeneticAlgorithm class ###############################################

template <class T>
GeneticAlgorithm<T>::GeneticAlgorithm()
{
  population = new Population();
  for(int i = 0 ; i < POP_SIZE ; i++)
  {
    population->add(new T());
  }
  population->evaluate(); // calculate initial fitness
  my_srand(time(NULL)); // initialize my global random number generator
}

template <class T>
GeneticAlgorithm<T>::~GeneticAlgorithm() 
{
  delete population;
}

template <class T>
void GeneticAlgorithm<T>::evolveUntilStable()
{
  cout << endl
  << "evolving starts" << endl;

  // every time we get the same fitness as last time,
  // without having any other between - increase this:
  int nbrSameFitness = 0;
  double lastSameFitness = -10e38;
  double p;
  Population* newPopulation;
  Individual* ind;
  int popCounter = 0;
  int patternCounter = POP_SIZE; // assuming that we don't get any duplicate patterns from start
  int outputFilter = 0;
  do
  {
    newPopulation = new Population();
    // mutate/crossover/clone for each individual in the population
    for(int i = 0 ; i < POP_SIZE ; i++)
    {
      ind = population->select()->clone();

      p = (double) my_rand() / ((double) MY_RAND_MAX +1);
      // chance of Mutating
      if(p > 0 && p <= CHANCE_MUTATE)
      {
        ind->mutate();
        patternCounter++; // assuming that we don't get any we'd had before
      }
      // chance of Cross-Over
      else if(p > CHANCE_MUTATE && p <= (CHANCE_MUTATE+CHANCE_CROSSOVER))
      {
        ind->crossOver(population->select());
        patternCounter++; // assuming that we don't get any we'd had before
      }
      // if not Mutating or Cross-Over - Clone is chosen
      // (change nothing since the clone is already performed)
      // Then add the new individual to the new population
      newPopulation->add(ind);
    }
    // evaluate the whole population
    newPopulation->evaluate(); // calculates fitness for each individual
    
    // print some progress information
    if(outputFilter % FILTER_OUTPUT == 0) // to get less printed to the console
      printf("#%d \tCurFitness = %f \tNewFitness = %f", popCounter, population->getMaxFitness(), newPopulation->getMaxFitness());

    // if we're within the "same" tolerance, increase the nbrSameFitness counter
    if(newPopulation->getMaxFitness() < population->getMaxFitness() * (1 + FITNESS_TOLERANCE) &&
        newPopulation->getMaxFitness() > population->getMaxFitness() * (1 - FITNESS_TOLERANCE)   )
    {
      if(outputFilter % FILTER_OUTPUT == 0) // to get less printed to the console
        printf("\t(almost same or same fitness)");

      if(population->getMaxFitness() != lastSameFitness)
      {
        nbrSameFitness = 1; // then this is a new same
      }
      else
      {
        nbrSameFitness++;
      }
      lastSameFitness = newPopulation->getMaxFitness();
    }
    if(outputFilter % FILTER_OUTPUT == 0) // to get less printed to the console
      printf("\n");
    outputFilter++;

    // replace the old population with the new one
    swap(population, newPopulation);
    popCounter++;
    delete newPopulation; // this is now the old population

  }
  while(nbrSameFitness < NBR_SAME_FITNESS); // stop when we have same fitness many times

  cout << endl
  << "Finished evolving, now we have a fitness of " << lastSameFitness << endl
  << "It took " << popCounter-- << " generations and " << patternCounter << " different patterns" << endl
   << endl;

  WeightsIndividual* fittest =
               static_cast<WeightsIndividual*>(population->getFittestIndividual());
  weightLog.writeWeights(fittest->getWeights());

}

int main(int argc, char *argv[])
{
  char* arbiterlog; 
  char* voterloglist;

  // Argument handling
  if(argc !=3) // the executable is one argument
  {
    cout << endl 
	 << "Usage: ./GeneticAlgorithm arbiterlog voterloglist" << endl
	 << "   arbiterlog   : the name of the arbiter state logfile" << endl
	 << "   voterloglist : a text file that contains a list over the voter log files to be used" << endl;
    exit(0);
  }
  else
  {
    arbiterlog = argv[1];
    voterloglist = argv[2];
    cout << "Starting GeneticAlgorithm with " << endl
        << " arbiter log : " << arbiterlog << endl
        << " votelog list: " << voterloglist << endl;
  }

  // Lets run!
  HumanLog humanLog(arbiterlog);
  VoterLogHandler voterLogHandler(voterloglist);
  // must read the enabled/disabled voters config file before starting GA
  enabled_voters = IO::readGeneticAlgorithmConfig("config");
  GeneticAlgorithm<WeightsIndividual> ga;
 
  // Do one run with the GeneticAlgorithm for each decision in the log files
  // (whichever of the voterLog or the arbiter log that has the fewest rows, 
  // although they should be the same).
  while(voterLogHandler.hasMoreVoterLists() && humanLog.hasMore()) {
    printf("# voter lists   : %d\n", voterLogHandler.getNbrLists());
    printf("# human commands: %d\n", humanLog.getNbrCmds());
    humanCmd = humanLog.getCommand(); // read a human commanded steering+velo
    voters = voterLogHandler.getVoterList(); // read a voter list and then run!
    ga.evolveUntilStable();
  }
  
  // finally calculate what the average weights are:
  weightLog.writeAverageWeights(weightLog.getWeightLogFilename());

  cout << endl
       << "#########################################################" << endl
       << "              GeneticAlgorithm finished" << endl
       << "#########################################################" << endl;
  
       
  return 0;
}


