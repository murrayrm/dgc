#ifndef GENETICALGORITHM_HH
#define GENETICALGORITHM_HH
#include "Population.hh"
#include "WeightsWriter.hh"
#include "HumanLog.hh" // to read human steer/speed values
#include "LogPlayer/VoterLogHandler.hh" // to get lists of Voters
#include "Arbiter2/VoterHandler.hh" // to evaluate a voter set

//################ Global stuff #######################################################

/** number of individuals per population */
const int POP_SIZE = 200;

/** how close to be called "same fitness" */
const double FITNESS_TOLERANCE = 0.00000001;

/** required number of same fitness until we stop evolving a population */
const int NBR_SAME_FITNESS = 10;

/** how many generations between each output print */
const int FILTER_OUTPUT = 4;

/** the chance of mutating for an individual */
const double CHANCE_MUTATE = 0.15;

/** the chance of cross-over for an individual */
const double CHANCE_CROSSOVER = 0.15;

/** global random functions, better than cstdlib rand() */
int my_rand();

/** initalizes my_rand() */
void my_srand(unsigned int);

/** Max number for my_rand() */
const long int MY_RAND_MAX = 2147483647; // 2^31 - 1

//################ End of Global stuff #######################################################

/** This class implements a templated Genetic Algorithm application. It was 
 * originally intended to be a solution for a programming assignment in a 
 * AI course at Lund Institute of Technology in Sweden. I remade a new 
 * Individual class and made minor changes to the other code to be able using
 * it for calculating weights for the Arbiter.
 * The original assignment is available at: 
 * http://www.cs.lth.se/DAT011/Assignment4.pdf
 * $Id$
 */
template <class T> class GeneticAlgorithm
{
public:
  /**
   * Creates a Genetic Algorithm class that can run with class T. Class T
   * must inherit from the Individual superclass.
   * An initial population of POP_SIZE individuals is created and evaluated once.
   */
  GeneticAlgorithm();

  /**
   * Destructor. Deleted the population object.
   */
  ~GeneticAlgorithm();

  /**
  * Evolves until we have reached our ending criteria (a fitness within
  * FITNESS_TOLERANCE has appeared NBR_SAME_FITNESS times in a row).
  * Evolving takes place as following:
  * - as long as we don't have the same a best fitness NBR_SAME_FITNESS times
  * - mutate/crossover/clone each individual in the population
  * - evaluate the population and calculate max fitness
  */
  void evolveUntilStable();

private:
  /** Voting simulation class to feed the arbiter with votes from a log */
  VoterLogHandler* voterLogHandler;
  /** A population of Individual objects */
  Population* population;
};

#endif


