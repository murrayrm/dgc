function result = plotTerrain(terrain, params)
    if nargin==2
        result = plot(terrain(:,1), terrain(:,2), params);
    else
        result = plot(terrain(:,1), terrain(:,2));
    end