//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// CR_Image
//----------------------------------------------------------------------------

#ifndef CR_IMAGE_DECS

//----------------------------------------------------------------------------

#define CR_IMAGE_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "cv.h"
#include "highgui.h"

// Intel performance library includes

//#include <ipl.h>
//#include <ipp.h>
//#include <ippcv.h>
//#include <ijl.h>
//#include <rl.h>
//#include <mkl.h>
//#include <cv.h>

// my stuff 

#include "CR_Memory.hh"
#include "CR_Error.hh"
//#include "CR_Window.hh"
//#include "CR_IplImage.hh"
//#include "CR_Matrix.hh"
//#include "CR_Postscript.hh"

//#include "CR_Demo3.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

// single-channel images

#define FLOAT_IMXY(im, x, y)  (((float*)(im->iplim->imageData + im->iplim->widthStep*y))[x])
#define UCHAR_IMXY(im, x, y)  (((unsigned char*)(im->iplim->imageData + im->iplim->widthStep*y))[x])

// 3-channel images (8 bits per channel)

#define UCHAR_B_IMXY(im, x, y)  (((unsigned char*)(im->iplim->imageData + im->iplim->widthStep*y))[3*x])
#define UCHAR_G_IMXY(im, x, y)  (((unsigned char*)(im->iplim->imageData + im->iplim->widthStep*y))[3*x+1])
#define UCHAR_R_IMXY(im, x, y)  (((unsigned char*)(im->iplim->imageData + im->iplim->widthStep*y))[3*x+2])



#define IMXY_R(im, x, y)      (im->data[(y) * im->wstep + 3 * (x)])
#define IMXY_G(im, x, y)      (im->data[(y) * im->wstep + 3 * (x) + 1])
#define IMXY_B(im, x, y)      (im->data[(y) * im->wstep + 3 * (x) + 2])

//#define RGB_TYPE          0
//#define YUV_TYPE          1

//#define IMXY_R(im, x, y)     (IPLXY_R(im->iplim, x, y))
// #define IMXY_G(im, x, y)     (IPLXY_G(im->iplim, x, y))
// #define IMXY_B(im, x, y)     (IPLXY_B(im->iplim, x, y))


// channel is argument: R = 0, G = 1, B = 2

#define IMXY_N(im, x, y, n)      (im->data[(y) * im->wstep + 3 * (x) + (n)])

#define GRAYFP_IMXY(im, x, y) (((float *) &(im->data[(y) * im->wstep + 4 * (x)]))[0])
 
// quick and dirty
//#define IMXY_I(im, x, y)     (IMXY_G(im, x, y))
#define IMXY_I(im, x, y)     (0.3 * IMXY_R(im, x, y) + 0.6 * IMXY_G(im, x, y) + 0.1 * IMXY_B(im, x, y))

//#define IMXY_PIX(im, x, y, r, g, b)       (IMXY_R(im, x, y) = r, IMXY_G(im, x, y) = g, IMXY_B(im, x, y) = b) 

#define IN_IMAGE_HORIZ(im, x)    (((x) >= 0) && ((x) < im->w))
//#define IN_IMAGE_HORIZ(im, x)    (((x) >= 50) && ((x) < (im->wstep - 50)))
#define IN_IMAGE_VERT(im, y)     (((y) >= 0) && ((y) < im->h))
#define IN_IMAGE(im, x, y)       (IN_IMAGE_HORIZ(im, (x)) && IN_IMAGE_VERT(im, (y)))

#define IN_IMAGE_HORIZ_MARG(im, x, xm)    (((x) >= xm) && ((x) < im->w - xm))
#define IN_IMAGE_VERT_MARG(im, y, ym)     (((y) >= ym) && ((y) < im->h - ym))
#define IN_IMAGE_MARG(im, x, y, xm, ym)       (IN_IMAGE_HORIZ_MARG(im, (x), (xm)) && IN_IMAGE_VERT_MARG(im, (y), (ym)))

#define CR_UNKNOWN           0
#define CR_IMAGE_PPM         1
#define CR_IMAGE_JPEG        2
#define CR_IMAGE_BITMAP      3
#define CR_IMAGE_DEMO3_RIF   4
#define CR_IMAGE_EPS         5
#define CR_IMAGE_PFM         6

//-------------------------------------------------
// structure
//-------------------------------------------------

//typedef void (*CR_Image_filter) (CR_Image *, CR_Image *);

typedef struct crimage
{
  IplImage *iplim, *temp_iplim;    // the latter is used for display

  int bpp;   // bytes per pixel

  int type;
  int w, h;

  int buffsize;   // number of bytes occupied by image data

  double ratio;   // of image size to display window size

  int wstep;      // actual width of image (with padding)

  // where the image is stored...so that we can do hardware-assisted image processing
  
  unsigned char *data;
  unsigned char *temprow;   // for vertical flipping

} CR_Image;


//-------------------------------------------------
// functions
//-------------------------------------------------

CR_Image *make_CR_Image(int, int, int);
void free_CR_Image(CR_Image *);
CR_Image *read_JPEG_to_CR_Image(char *filename);
CR_Image *read_CR_Image(char *);
CR_Image *read_PPM_to_CR_Image(char *);
int determine_type_CR_Image(char *);
//void ip_copy_CR_Image(CR_Image *, CR_Image *);
void write_CR_Image_to_PPM(char *, CR_Image *);
void write_CR_Image(char *, CR_Image *);
void decimate_CR_Image(CR_Image *, CR_Image *, int);
//void vflip_CR_Image(CR_Image *);

void pyramid_down_CR_Image(CR_Image *, CR_Image *);
void convert_to_gray_fp_CR_Image(CR_Image *, CR_Image *, CR_Image *);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
