//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_VanishingPoint.hh"

//----------------------------------------------------------------------------

extern IplImage        *gabor_max_response_index;
extern IplImage        *gabor_max_response_value;

extern int              gabor_size;                      
extern int              gabor_num_orientations;
extern float            gabor_delta_orientation;  

//----------------------------------------------------------------------------

IplImage               *candidate_image;             // where the votes get tallied
IplImage               *voter_ray_x, *voter_ray_y;   // line segments representing dominant orientation rays

int support_ray_delta                 = 5;           // spacing of rays along image perimeter
float support_angle_diff_thresh       = 0.75;        // max allowable angular discrepancy along ray from VP (radians)  
                                                     // 0.75 works well for 36 orientations   
int support_max_rays;
int *support_x, *support_y;                          // endpoints of support rays on image perimeter
float *support_angle_diff;                          
float support_mean_dx, support_mean_dy;              // "target" angle of estimated road centerline               
float support_cur_dx, support_cur_dy;                // actual angle of estimated road centerline               
float support_alpha                   = 0.1;
int show_support                      = FALSE;
int show_mean_line                    = TRUE;

int show_vanishing_point              = FALSE; 
int do_vanishing_point                = TRUE;
int print_max_candidate               = FALSE;

CvPoint pmax, pmin;

//----------------------------------------------------------------------------

// how much to "shrink" borders of dominant orientation image to indicate
// which voters don't vote because (a) they're too close to the image edge or 
// (b) they're a priori unlikely to belong to the road

// these numbers (in units of pixels) should all be >= 0

int voter_lateral_margin;
int voter_top_margin;
int voter_bottom_margin;

float voter_ray_length;
float *voter_ray_dx;
float *voter_ray_dy;

// limits of which candidates voters are voting for.  margin = 0 means use image
// borders, margin > 0 means shrink , margin < 0 means grow
// these are specified in scale of dominant orientation image

int candidate_lateral_margin;
int candidate_top_margin;
int candidate_bottom_margin;

float candidate_x_offset;
float candidate_y_offset;

int candidate_width;
int candidate_height;

int fcandidate_width;
int fcandidate_height;

float inv_candidate_scale;

//----------------------------------------------------------------------------

//float voter_sky_fraction               = 0.25;  // vertical fraction (from top) of image to ignore as assumed sky (only used for first frame?)
float voter_sky_fraction               = 0.0;  // vertical fraction (from top) of image to ignore as assumed sky (only used for first frame?)

float candidate_scale                  = 1.0;   // how much "resolution" to use in candidate region.  2 => 2x2=4 pixels in size of 1 
                                                // D.O. pixel, 4 => 4x4=16, and so on (horiz. and vert. directions scale equally)

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void draw_vanishing_point(int win_w, int win_h)
{
  glPointSize(5);
  glColor3f(1, 0, 0);
  glBegin(GL_POINTS);
  glVertex2f(pmax.x, win_h - pmax.y);
  glEnd();
}

//----------------------------------------------------------------------------

// compute angle differences along a line through the dominant orientations
// p1 is vanishing point, p2 is point along ray below

// median might be better

float mean_line_angle_diff(IplImage *image, CvPoint p1, CvPoint p2, float *udx, float *udy)
{
  float sum, angle, alpha, dx, dy;
  CvLineIterator iterator;
  int count, i;

  dy = p1.y - p2.y;
  dx = p1.x - p2.x;
  angle = atan2(-dy, dx);   // note negation of dy
  count = cvInitLineIterator(image, p1, p2, &iterator, 8);
  *udx = -dx / (float) count;
  *udy = -dy / (float) count;

  for (i = 0, sum = 0; i < count; i++) {
    alpha = DEG2RAD(gabor_delta_orientation) * (float) iterator.ptr[0];
    sum += fabs(angle - alpha);
    CV_NEXT_LINE_POINT(iterator);
  }

  return sum / (float) count;
}

//----------------------------------------------------------------------------

// these are "rays" extending down from vanishing point.  on the road,
// we expect that the dominant orientations will be aligned with them.
// thresholding on the fraction of dominant orientations that are within
// some angle delta of the ray orientation gives us our "support" rays

// the "mean line" is the mean angle over the support rays in this image; the
// estimate which is actually drawn is temporally filtered to make its
// motion smoother

// the offset from w/2 of the intersection of the mean line with y = h 
// (the bottom of the image) is proportional to the (estimated) lateral offset error

// the same difference for the vanishing point is proportional to the direction error
 
void draw_vanishing_point_support(float vx, float vy, int w, int h)
{
  int i;
  float dx, dy;

  compute_vanishing_point_support(vx, vy);

  // yellow lines

  if (show_support) {

    glColor3f(0, 1, 0);

    glBegin(GL_LINES);
  
    for (i = 0; i < support_max_rays; i++) { 
      if (support_angle_diff[i] < support_angle_diff_thresh) {
	glColor3f(0, 1, 0);
      
	if (support_y[i] >= vy) {
	  glVertex2f(vx, h - vy);
	  if (support_x[i] > 0)
	    glVertex2f(support_x[i] + 1, h - support_y[i] - 1);
	  else
	    glVertex2f(support_x[i], h - support_y[i] - 1);
	}
      }
    }
    glEnd();
  }

  // crude tracking: move part of the way toward the target each iteration
	
  support_cur_dx += support_alpha * (support_mean_dx - support_cur_dx);
  support_cur_dy += support_alpha * (support_mean_dy - support_cur_dy);

  // make line long enough to span image

  dx = support_cur_dx * 2 * (float) h;
  dy = support_cur_dy * 2 * (float) h;

  if (show_mean_line) {

    glColor3f(1, 0, 1);
    glLineWidth(3);

    glBegin(GL_LINES);
  
        glVertex2f(vx, h - vy);
        glVertex2f(vx + dx, h - (vy + dy));
     

    glEnd();
 
    glLineWidth(1);
  }
}

//----------------------------------------------------------------------------

float compute_vanishing_point_support(float vx, float vy)
{
  int i;
  float num_rays, num_good_rays, sum, dx, dy, udx, udy;

  for (i = 0, sum = num_rays = num_good_rays = dx = dy = 0; i < support_max_rays; i++) 
    if (support_y[i] > vy + support_ray_delta / 2) {
      support_angle_diff[i] = mean_line_angle_diff(gabor_max_response_index, cvPoint(vx, vy), cvPoint(support_x[i], support_y[i]), &udx, &udy);

    // don't weight each ray equally?
    if (support_angle_diff[i] < support_angle_diff_thresh) {
      dx += udx;
      dy += udy;
      num_good_rays++;
    }
      sum += support_angle_diff[i];
      num_rays++;
    }
    else
      support_angle_diff[i] = MAX_ANGLE_DIFF;

  if (num_good_rays) {
    support_mean_dx = dx / num_good_rays;
    support_mean_dy = dy / num_good_rays;
  }

  return sum / num_rays;
}

//----------------------------------------------------------------------------

// identify road region by which pixels voted for the vanishing point

void initialize_vanishing_point_support(int w, int h)
{
  int x, y, cur_ray;

  support_max_rays = 0;

  // count number of possible rays (max when VP is at top of image)

  for (x = support_ray_delta/2; x < w; x += support_ray_delta) 
    support_max_rays++;

  for (y = h - 1 - support_ray_delta/2; y > support_ray_delta/2; y -= support_ray_delta) 
    support_max_rays += 2;

  // allocate LUT for ray endpoints

  support_x = (int *) calloc(support_max_rays, sizeof(int));
  support_y = (int *) calloc(support_max_rays, sizeof(int));
  support_angle_diff = (float *) calloc(support_max_rays, sizeof(float));

  // fill it in

  cur_ray = 0;

  for (x = support_ray_delta/2; x < w; x += support_ray_delta) {
    support_x[cur_ray] = x;
    support_y[cur_ray] = h - 1;

    cur_ray++;
  }

  for (y = h - 1 - support_ray_delta/2; y > support_ray_delta/2; y -= support_ray_delta) {
    support_x[cur_ray] = 0;
    support_y[cur_ray] = y;
   
    cur_ray++;
 
    support_x[cur_ray] = w - 1;
    support_y[cur_ray] = y;

    cur_ray++;
    
  }
}

//----------------------------------------------------------------------------

void initialize_vanishing_point_estimation(int w, int h)
{
  int i, vx, vy;
  float theta;

  if (!do_vanishing_point)
    return;

  // candidates

  candidate_top_margin = 0;
  candidate_bottom_margin = 0;
  candidate_lateral_margin = 0;

  candidate_x_offset = candidate_lateral_margin;
  candidate_y_offset = candidate_top_margin;
  inv_candidate_scale = 1 / candidate_scale;

  candidate_width = (int) candidate_scale * (w - 2 * candidate_lateral_margin);
  candidate_height = (int) candidate_scale * (h - candidate_top_margin - candidate_bottom_margin);

  fcandidate_width = candidate_width;
  fcandidate_height = candidate_height;

  candidate_image = cvCreateImage(cvSize(candidate_width, candidate_height), IPL_DEPTH_32F, 1);

  // voters

  voter_top_margin = (int) ceil(MAX2(voter_sky_fraction * (float) h, gabor_size / 2 + 1));
  voter_bottom_margin = gabor_size / 2 + 1;
  voter_lateral_margin = gabor_size / 2 + 1;

  // endpoints of voter ray lines

  voter_ray_x = cvCreateImage(cvSize(w, h), IPL_DEPTH_32F, 1);
  voter_ray_y = cvCreateImage(cvSize(w, h), IPL_DEPTH_32F, 1);

  // fixed endpoint (in candidate coordinates)

  for (vy = 0; vy < h; vy++)
    for (vx = 0; vx < w; vx++) {
      FLOAT_IMXY(voter_ray_x, vx, vy) = V2C_X(vx);
      FLOAT_IMXY(voter_ray_y, vx, vy) = V2C_Y(vy);
    }

  // each line must be sufficiently long that it will reach across the candidate region
  // no matter where in the voter region it starts or what its orientation.
  // use distance from bottom left of voter region to top right of candidate region as max
  // (will get clipped if too long)


  float max_dx, max_dy;

  max_dx = C2V_X(candidate_width) - 0;
  max_dy = C2V_Y(0) - h;
  voter_ray_length = sqrt(SQUARE(max_dx) + SQUARE(max_dy));

  // initialize arrays of x, y ray components  (0 deg. = pointing horizontal right, 90 = vertical up, 180 = horizontal left
  // (in candidate coordinate system)

  voter_ray_dx = (float *) calloc(gabor_num_orientations, sizeof(float));
  voter_ray_dy = (float *) calloc(gabor_num_orientations, sizeof(float));

  for (i = 0; i < gabor_num_orientations; i++) {
    theta = DEG2RAD(gabor_delta_orientation) * (float) i;
    voter_ray_dx[i] = V2C_X(cos(theta) * voter_ray_length);
    voter_ray_dy[i] = -V2C_Y(sin(theta) * voter_ray_length); 
    //    printf("dx = %f, dy = %f\n", voter_ray_dx[i], voter_ray_dy[i]);
  }

  // opengl (for opengl voting)

  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT);
  glColor3f(1, 1, 1);
  glLineWidth(1);
  glBlendFunc(GL_ONE, GL_ONE);

#ifdef HAVE_GEFORCE6
  init_fp_blend(candidate_width, candidate_height);
#endif
  
  // region support

  initialize_vanishing_point_support(w, h);
}

//----------------------------------------------------------------------------

void compute_vanishing_point(int w, int h)
{
  if (!do_vanishing_point)
    return;

  opengl_linear_vanishing_point_estimation(w, h);
}

//----------------------------------------------------------------------------

// Maximum of 256 votes per candidate without HAVE_GEFORCE6; max of 2048 with

void opengl_linear_vanishing_point_estimation(int w, int h)
{
  int vx, vy, i_max;
  float cx, cy, val_max;

  // set color somewhere before here

  // vote

  glClear(GL_COLOR_BUFFER_BIT);

#ifdef HAVE_GEFORCE6
  enter_fp_blend(candidate_width, candidate_height);
#endif

  glEnable(GL_BLEND);

  glColor3f(ONE_OVER_256, ONE_OVER_256, ONE_OVER_256);

  glBegin(GL_LINES);

  for (vy = voter_top_margin; vy < h - voter_bottom_margin; vy++)
    for (vx = voter_lateral_margin; vx < w - voter_lateral_margin; vx++) {

      i_max = UCHAR_IMXY(gabor_max_response_index, vx, vy);

      // exclude near-horizontal lines
      if (i_max > 1 && i_max < gabor_num_orientations - 2) {	

	cx = FLOAT_IMXY(voter_ray_x, vx, vy);
	cy = FLOAT_IMXY(voter_ray_y, vx, vy);
	
	glVertex2f(cx, cy);
	glVertex2f(cx + voter_ray_dx[i_max], cy + voter_ray_dy[i_max]);
       
      }
  }

  glEnd();

  glDisable(GL_BLEND);

#ifdef HAVE_GEFORCE6
  exit_fp_blend();
#endif

  // read it back

  glReadPixels(0, 0, candidate_width, candidate_height, GL_RED, GL_FLOAT, candidate_image->imageData);

  // count votes for all candidates (not necessary for particle filtering)

  if (print_max_candidate) {
    double maxval, minval;
    cvMinMaxLoc(candidate_image, &minval, &maxval, &pmin, &pmax);
    printf("max val %lf at (%i, %i)\n", maxval, pmax.x, pmax.y);
  }
}

//----------------------------------------------------------------------------

// various voting, tracking/winner-selection schemes

void process_vanishing_point_command_line_flags(int argc, char **argv) 
{
  int i;

  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i],                             "-sky"))
      voter_sky_fraction = atof(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-showvan"))
      show_vanishing_point = TRUE;
    else if (!strcmp(argv[i],                        "-novan"))
      do_vanishing_point = FALSE;
    else if (!strcmp(argv[i],                        "-nomean"))
      show_mean_line = FALSE;
    else if (!strcmp(argv[i],                        "-printmax"))
      print_max_candidate = TRUE;
    else if (!strcmp(argv[i],                        "-showsupp"))
      show_support = TRUE;
    else if (!strcmp(argv[i],                        "-raydelta"))
      support_ray_delta = atoi(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-thresh"))
      support_angle_diff_thresh = atof(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-alpha"))
      support_alpha = atof(argv[i + 1]);
  }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#ifdef HAVE_GEFORCE6

// Enabling a higher vote total per candidate with 16-bit floating point 
// blending

#include "glx.h"
#include "glext.h"
#include "glxtokens.h"

Display *ogl_disp;
GLXDrawable ogl_draw;
GLXPbuffer ogl_pbuffer;
GLXContext ogl_context, ogl_oldcontext;

//----------------------------------------------------------------------------

void init_fp_blend(int width, int height)
{
  int i, n;

  // initialize pbuffer

  ogl_disp = glXGetCurrentDisplay();
  ogl_draw = glXGetCurrentDrawable();
  ogl_oldcontext = glXGetCurrentContext();

  //  width = height = 128;

  int pfAttribList[] = {
    GLX_RED_SIZE,               16,
    GLX_GREEN_SIZE,             16,
    GLX_BLUE_SIZE,              16,
    GLX_ALPHA_SIZE,             16,
    GLX_FLOAT_COMPONENTS_NV,    true,
    GLX_RENDER_TYPE_SGIX,
    GLX_RGBA_BIT_SGIX,
    GLX_DRAWABLE_TYPE_SGIX,
    GLX_PBUFFER_BIT_SGIX,
    0,
  };

  GLXFBConfig *glxConfig = glXChooseFBConfigSGIX(ogl_disp, 0, pfAttribList, &n);
  if (!glxConfig)
    CR_error("pbuffer creation error: glXChooseFBConfigSGIX() failed\n");

  int pbAttribList[] = {
    GLX_LARGEST_PBUFFER, true,
    GLX_PRESERVED_CONTENTS, true,
    0,
  };
    
  ogl_pbuffer = glXCreateGLXPbufferSGIX(ogl_disp, glxConfig[0], width, height, pbAttribList);  
  if (!ogl_pbuffer)
    CR_error("pbuffer creation error:  glXCreatePbufferSGIX() failed\n");

  ogl_context = glXCreateContextWithConfigSGIX(ogl_disp, glxConfig[0], GLX_RGBA_TYPE, ogl_oldcontext, true);
  if (!ogl_context)
    CR_error("pbuffer creation error:  glXCreateContextWithConfigSGIX() failed\n");
}

//----------------------------------------------------------------------------

void enter_fp_blend(int width, int height)
{
  // activate pbuffer

  glXMakeCurrent(ogl_disp, ogl_pbuffer, ogl_context);

  // draw on it

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, width, 0, height);

  glClearColor(0.0, 0.0, 0.0, 0.0);
  glClear(GL_COLOR_BUFFER_BIT);

  glColor3f(1, 1, 1);
  glPointSize(1);
  glLineWidth(1);

  glBlendFunc(GL_ONE, GL_ONE);
}

//----------------------------------------------------------------------------

void exit_fp_blend()
{
  // deactivate pbuffer

  //  glXMakeCurrent(ogl_disp, ogl_draw, ogl_oldcontext);
}

//----------------------------------------------------------------------------

void fp_blend_test()
{
  int width, height, i, j;

  //  print_opengl_info();
  
  //  half hbuffer;
  //  HALF_FLOAT_NV h;
  //  GLhalfNV h;

  width = 160;
  height = 120;

  float *testbuff = (float *) calloc(width * height, sizeof(float));

  init_fp_blend(width, height);
  enter_fp_blend(width, height);

  for (j = 0; j < 10; j++) {

  glClear(GL_COLOR_BUFFER_BIT);

  glEnable(GL_BLEND);

  glBegin(GL_POINTS);
  for (i = 0; i < 536; i++) 
  glVertex2f(0, 0);
  glEnd();

  glDisable(GL_BLEND);


  glReadPixels(0, 0, width, height, GL_RED, GL_FLOAT, testbuff);
  for (i = 0; i < width * height; i++)
    if (testbuff[i])
      printf("%f\n", testbuff[i]);
  }

  // deactivate pbuffer

  //  glXMakeCurrent(ogl_disp, ogl_draw, ogl_oldcontext);

  // end here


  exit(1);
}


#endif

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// experimental

//----------------------------------------------------------------------------

CvMat                  *ground2cameraTransform;

// put this into initialization routine...
//  ground2cameraTransform = cvCreateMat(3, 3, CV_32FC1);

// and this in main loop...
//  float x, y;
//  get_tracker_position(&x, &y);
//  compute_ground_to_camera_transform(0, y, target_im->width, y, ground2cameraTransform);

//----------------------------------------------------------------------------

// given two points on horizon line

// affine rectification of plane with horizon line (a, b, c) is given by:
// ( 1 0 0 )
// ( 0 1 0 )
// ( a b c )

// inverse of this takes ground plane points to camera view

void compute_ground_to_camera_transform(float hx1, float hy1, float hx2, float hy2, CvMat *H)
{
  int i;
  CvMat p1, p2, l, invH;

  // compute line 

  float a[] = { hx1, hy1, 1 };
  float b[] = { hx2, hy2, 1 };
  float c[] = { 0, 0, 0 };

  for (i = 0; i < 3; i++)
    printf("%f ", a[i]);
  printf("\n");

  for (i = 0; i < 3; i++)
    printf("%f ", b[i]);
  printf("\n");

  
  cvInitMatHeader(&p1, 3, 1, CV_32FC1, a);
  cvInitMatHeader(&p2, 3, 1, CV_32FC1, b);
  cvInitMatHeader(&l, 3, 1, CV_32FC1, c);

  cvCrossProduct(&p1, &p2, &l);

  // construct homography 

  float h[] = { 1, 0, 0,
		0, 1, 0,
		0, 0, 0 };

  cvInitMatHeader(&invH, 3, 3, CV_32FC1, h);
  cvmSet(&invH, 2, 0, cvmGet(&l, 0, 0));
  cvmSet(&invH, 2, 1, cvmGet(&l, 1, 0));
  cvmSet(&invH, 2, 2, cvmGet(&l, 2, 0));

  for (i = 0; i < 9; i++)
    printf("%f ", ((float *) invH.data.fl)[i]);
  printf("\n");

  // invert

  cvInvert(&invH, H);

  // clean up

  // don't need to free p1, p2, l, h?

  for (i = 0; i < 9; i++)
    printf("%f ", ((float *) H->data.fl)[i]);
  printf("\n");

  exit(1);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
