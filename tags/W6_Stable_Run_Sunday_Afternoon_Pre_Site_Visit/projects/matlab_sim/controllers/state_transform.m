function newstate = state_transform(state)
% newstate = state_transform(state)
% This function transforms the state from the c.g. of the vehicle into the
% state of the center of the front axle of the vehicle.  The incoming state
% is a 7-vector that is of the form [thd vlat vlon x y th phi], and the
% outgoing state is [x xd y yd th thd phi]

global L_f L_r

[thd, vlat, vlon, x, y, theta, phi] = set_states(state);
newstate = zeros(7, 1);

newstate(1) = x + L_f * cos(theta);
newstate(2) = vlon * cos(theta) - (vlat + L_f * thd) * sin(theta);
newstate(3) = y + L_f * sin(theta);
newstate(4) = vlon * sin(theta) + (vlat + L_f * thd) * cos(theta);
newstate(5) = theta;
newstate(6) = thd;
newstate(7) = phi;
