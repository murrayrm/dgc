/**
 * LadarClient.cc
 * Player client for ladars
 * Revision History:
 * 03/22/05 hbarnor created
 */

#include "LadarClient.hh"

LadarClient::LadarClient()
{
  numScanPoints = 0;
}

LadarClient::~LadarClient()
{
  unsubscribe();
  disconnect();
  delete alice;
}

int LadarClient::init()
{
  readConfig();
  connect();
  subscribe();
  return -1;
}

void LadarClient::readConfig()
{
  // set the defaults:
  host = PLAYER_GAZEBO_HOST;
  port = PLAYER_GAZEBO_PORT;

  cout << "LadarClient::readConfig: reading file '" << CONFIG_FILENAME 
       << "'" <<endl;
  ifstream file(CONFIG_FILENAME);
  if(!file.is_open())
  {
    cout << "LadarClient::readConfig: Cannot read file '" << CONFIG_FILENAME
    << "', using defaults = " << host << ":" << port << endl;
    return;
  }
  // if a file was found, use those settings instead:
  string line;
  getline(file, line);
  host = line;
  getline(file, line);
  port = atoi(line.c_str());
  cout << "LadarClient::readConfig: read host:port = "
  << host << ":" << port << endl;
}

void LadarClient::connect()
{
  cout << "LadarClient.Connect: connecting to " << host
  << ":" << port << endl;
  
  // Create a PlayerClient and connect it
  alice = new PlayerClient(host.c_str(), port);     
  // Connect the to the Player server if not already connected
  while(!alice->Connected())
    {
    if(alice->Connect(host.c_str(),port) != 0) 
      {
	cout << "LadarClient.Connect: connect failed, trying again in ";
	for(int i = 5; i >= 0; i--)
	  {
	    usleep(1000000); // sleep 1 sec
	    cout << i << " " << flush;
	  }
	cout << endl;
      }
    }
  cout << "LadarClient.Connect: connect successful!" << endl;  
  // Set the data mode
  // With this mode we get only data when we ask for it
  cout << "LadarClient.SetDataMode: setting data mode" << endl;
  if(alice->SetDataMode(DATA_MODE) != 0)
  {
    cout << "LadarClient.SetDataMode: failed! " << endl;
    exit(-1);
  }
  cout << "LadarClient.SetDataMode: successful! " << endl; 
}

void LadarClient::disconnect()
{
  // disconnect the from alice
  cout << "disconnecting the LadarClient" << endl;
  if(alice->Disconnect() != 0)
    {
      cout << "disconnect failed!" << endl;
    }
  else
    {
      cout << "disconnect successful!" << endl;
    }
}

void LadarClient::subscribe()
{
  unsigned char access;
  unsigned char mode = PLAYER_READ_MODE;
  // establish two  camera proxies, initially closed and unconnected
  playerLadar = new LaserProxy(alice,1,'c');
  //roofLadar   = new LaserProxy(alice,0,'c');
  cout << "playerLadar: Subscribing (read only)" << endl;
  if(playerLadar->ChangeAccess(mode,&access) < 0 || (access != mode))
    {
      cout << "playerLadar: error subscribing driver: " << playerLadar->driver_name << endl
	   << "playerLadar: access: " << access << endl;
      exit(-1);
    }
  cout << "playerLadar: Subscribed successfully driver : " << playerLadar->driver_name << endl;

  if ( playerLadar->SetLaserState(1) < 0 )
    {
      cerr << " playerLadar: couldn't turn laser on." <<endl;
    }
  
  //   cout << "RoofLadar: Subscribing (read only)" << endl;
  //   if(roofLadar->ChangeAccess(mode,&access) < 0 || (access != mode))
  //     {
  //       cout << "RoofLadar: error subscribing driver: " << roofLadar->driver_name << endl
  // 	   << "RoofLadar: access: " << access << endl;
  //       exit(-1);
  //     }
  //  cout << "RoofLadar: Subscribed successfully driver : " << rightCamera->driver_name << endl;
}

void LadarClient::unsubscribe()
{
  unsigned char access;
  //unsubscribe the two cameras
  if((playerLadar->ChangeAccess(PLAYER_CLOSE_MODE, &access) < 0) ||
     (access != PLAYER_CLOSE_MODE))
    {
      cout << "playerLadar: failed to unsubscribe " << endl;
    }
  else
    {
      cout << "playerLadar: unsubscribed successfully " << endl;;
      delete playerLadar;
    }

  //    if((roofLadar->ChangeAccess(PLAYER_CLOSE_MODE, &access) < 0) ||
  //      (access != PLAYER_CLOSE_MODE))
  //     {
  //       cout << "RoofLadar: failed to unsubscribe " << endl;
  //     }
  //   else
  //     {
  //       cout << "RoofLadar: unsubscribed successfully " << endl;;
  //       delete roofLadar;
  //     }
}


void LadarClient::grab()
{
  //cout << "read" << endl;
  alice->Read();
  //cout << "yar" << endl;
  // see:  http://playerstage.sourceforge.net/doc/Player-1.6.2-html/player/classLaserProxy.html
  //  double *ladarReadings;
  numScanPoints = playerLadar->scan_count;
  scanRanges = (double *) malloc(numScanPoints * sizeof(double));
  // range (m) and bearing (radians)
  // double LaserProxy::scan[PLAYER_LASER_MAX_SAMPLES][2] 
  //*ladarReadings = roofLadar->scan;
  for (int i = 0; i < numScanPoints; i++)
    {
      scanRanges[i] = playerLadar->scan[i][0];
      //cout << playerLadar->scan[i][2] << ", ";
    }
  //cout << endl;
}
