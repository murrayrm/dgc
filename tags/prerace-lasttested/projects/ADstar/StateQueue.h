#include <list>
#include <State.h>

class StateQueue
{

 private:

//NOTE: This compare algorithm is intentionally the REVERSE of the one used in the example in the STL.  This is because the STL priority queue is sorted such that the HIGHEST priority item is popped first.  However, the priority queue needed in the AD* algorithm must be sorted such that the LOWEST priority item is popped first.  Therefore, by inverting the compare algorithm that the STL priority queue uses to sort the items, we can insure that the queue will actually be implemented such that the LOWEST priority item is popped first, in accordance with the AD* algorithm requirements.

  struct StateCompare : public binary_function<State*, State*, bool>
  {
    bool operator()(State* s1, State*s2) const
    {
      return ((*(s1->getKey()) < *(s2->getKey()))); //returns FALSE if s1 has lower priority than s2
    }
  };

  list<State*> Q;

 public:

  StateQueue();

  void push(State* s);

  State* peek();

  State* peek_back();

  State* pop();

  State* pop_back();

  void clear();

  void remove(State* s);

  void sort();

  bool isEmpty();

  int size();


  //Returns an iterator to the front of the queue of State*'s
  list<State*>::iterator  getQueueIterator();

  void iterate();

  ~StateQueue();
};
