#include <sn_msg.hh>
#include <sys/time.h>
#include <iostream>

#define us_p_s  1000000 // 4294967296
#define max_msg_size 102400 //100KB.  maybe should increase later

using namespace std;
int main(int argc, char** argv)
{
  int key; 
  if (argc < 2)
  {
    cerr << "This program should only be called from snm-main.sh" << endl;
    cerr << "usage: single-throughput-slave key" << endl;
    cerr << "key is the skynet key" << endl;
    exit(2);
  }
  else
  {
    key = atoi(argv[1]);
  }
  //converg msg_rate into messages per second
  skynet Skynet(MODmark, key);
  int chan = Skynet.listen(SNmark1, ALLMODULES);
  int echochan = Skynet.get_send_sock(SNmark2);
  char buffer[max_msg_size];
  buffer[0] = 0;                            //continue character
  unsigned int n = 0;
  while(true)
  {
    ssize_t msgsize = Skynet.get_msg(chan, buffer, max_msg_size, 0);
    if(buffer[0] == 120)
			break;
		if(buffer[0] != n%100)
			cerr << "didn't match!" << endl;

    Skynet.send_msg(echochan, buffer, msgsize, 0);  //echo in kind
    n++;
  }

  //cerr << "slave messages= " << n << endl; 
  //cout << n << endl;
  return 0;
}
