#ifndef REACTIVEMODULE_HH
#define REACTIVEMODULE_HH

#include <pthread.h>
#include <fstream>
#include "StateClient.h"
#include "TrajTalker.h"
#include "MapdeltaTalker.h"
#include "CMapPlus.hh"
#include "rddf.hh"
#include "CTimberClient.hh"
#include "GlobalConstants.h"
#include "RefinementStage.h"
#include "reactiveTabSpecs.hh"
#include "reactivePlanner.hh"
#include "TabClient.h"
#include "MapConstants.h"

using namespace std;

#define BLUR_SIZE 1.  //  m

class reactiveModule : public CStateClient, public CTrajTalker,
    public CMapdeltaTalker, public CTimberClient, public CModuleTabClient
{
  CMapPlus				 	m_map;
  int								m_mapLayer, m_blurLayer;
  RDDF							m_rddf;
	char*							m_pMapDelta;

	/*! Whether at least one delta was received */
	bool							m_bReceivedAtLeastOneDelta;
	pthread_mutex_t		m_deltaReceivedMutex;
	pthread_cond_t		m_deltaReceivedCond;

	/*! The mutex to protect the map we're planning on */
	pthread_mutex_t		m_mapMutex;

	/*! The skynet socket for sending trajectories (to mode managements) */
	int								m_trajSocket;

  ofstream outfile;
  
  /*!Used for guiTab.*/
  SreactiveTabInput  m_input;
  SreactiveTabOutput m_output;

  //Declare the refinement stage for the planner required when using Dima's
  //MakeTrajC2 function
  CRefinementStage rS;

public:
  reactiveModule(int sn_key);
  ~reactiveModule();

  /*! Method to run in separate thread.  This function runs the dd_loop() 
   *  Sparrow method to update the screen and take keyboard commands. */
  void SparrowDisplayLoop();

  /*! Method to update the variables that are to be displayed in the Sparrow
   *  interface.  This method does not run a loop. */
  void UpdateSparrowVariablesLoop();

	/*! This is the function that continually runs the planner in a loop */
	void planningLoop(void);

	/*! this is the function that continually reads map deltas and updates the
		map */
	void getMapDeltasThread();
	
  /*! Grows obsticles in a square.  This function will probably be depreciated
      once growing is implemented in fusionMapper.*/
	void blurMap();
};

#endif
