#ifndef TIMBER_HH
#define TIMBER_HH

#include "sn_msg.hh"
#include <iostream>
#include <iomanip>
#include <math.h>
#include <string>
#include <fstream>
#include <set>
#include <sstream>
#include <map>
#include "DGCutils"
#include "CTimberTalker.hh"
#include "TimberConfig.hh"


using namespace std;

class CTimber : public CTimberTalker
{
public:
  CTimber(int des_skynet_key, string playback_location);
  ~CTimber();

  void TimberActiveLoop();

  /** Accessors */
  string getTimestamp();
  int getDebugLevel();

  /** Mutators */
  void setDebugLevel(int value);

  int sendDebug();

  enum GUI_MSG_TYPES {
    RESUME,
    PAUSE,
    REPLAY,
    START,
    STOP,
    RESTART
  };

private:
  void RecvThread();
  void RecvFromGuiThread();
  void SendThread();
  void setupNewSession(unsigned long long the_time = 0);
  void getCommand(char* command, char* message);
  void catDGCDirFromFile(char* dest);

  set<string> todoSet;
  int skynet_key;

  map<string, int> MAPNAME;
  int LEVEL_ARRAY_NAME[timber_types::LAST];
  bool logging_on;
  pthread_mutex_t logging_on_mutex;

  bool shutdown;
  pthread_mutex_t shutdown_mutex;

  int debug_level;
  pthread_mutex_t debug_level_mutex;

  ofstream todoListFile;
  pthread_mutex_t todoListFile_mutex;

  char* session_timestamp;
  pthread_mutex_t session_timestamp_mutex;

  /** A struct to keep track of everything related to logging playback.*/
  struct PlaybackState
  {
    /** real_time_point and playback_time_point are the two coordinates that 
     * represent a point in the 2 space of real time and playback time.  For example
     * if real_time_point = 1030 and playback_time_point = 30, then at an actual time
     * of 1030, our playback time is 30.  In reality, these numbers would be much
     * bigger, because they're unsigned long longs, and an increment of one in
     * either number corrosponds to an increase of one microsecond.*/
    unsigned long long real_time_point;
    unsigned long long playback_time_point;  /**< see documenation for real_time_point */

    /** playback_speed is the relative speed of our playback time with respect to
     * actual (computer clock) time.  If playback_speed = .5 for instance, then we're
     * playing back data at half speed.*/
    double playback_speed;
    
    /** string containing the location of the playback directory being used */
    string playback_location;
  };

  /** Keeps track of everything related to logging playback.  Basically this is 
   * just a wrapper for a point-slope representation of how the actual (computer)
   * time and the playback time are related. */
  PlaybackState playback_state;
  void lockPlaybackState();
  void unlockPlaybackState();
  pthread_mutex_t playback_state_mutex;

  void printStatus();
  string bool2str(bool thebool);
  void setNewSpeedNow(double new_speed);

  /** takes in a big nasty directory string and extracts the unix timestamp
   * from it */
  long long humanTime2Unix(string dir_string);
  void printDebug(string message, double req_level, bool newline = true);
  string unixTime2Human(string unix_time);
};

#endif  // TIMBER_HH
