//SUPERCON STRATEGY TITLE: Lone Ranger - SOMETIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyLoneRanger.hh"
#include "StrategyHelpers.hh"

bool CStrategyLoneRanger::lturnReverse_trans_condition( bool checkBool, const SCdiagnostic *pdiag, const char* StrategySpecificStr ) {
  
  if( checkBool == false ) {
    
    //This should NOT evaluate to TRUE very frequently (if ever) as StrategyLturnReverse checks whether pln->NFP through superCon and reverses again if necessary
    checkBool = trans_LturnReverse( (*pdiag), StrategySpecificStr );
  }  
  return checkBool;
}

void CStrategyLoneRanger::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE  
  currentStageName = loneranger_stage_names_asString( loneranger_stage_names(stgItr.nextStage()) );    

  switch( loneranger_stage_names(stgItr.nextStage()) ) {

  case s_loneranger::estop_pause: 
    //verify input conditions & e-stop pause for gear change (--> drive)
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* SHOULD BE EVALUATED *ABOVE* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface,  m_pdiag, strprintf( "LoneRanger: %s", currentStageName.c_str() ) );
      skipStageOps = lturnReverse_trans_condition( skipStageOps, m_pdiag, strprintf( "LoneRanger: %s", currentStageName.c_str() ) );
      skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );      
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LoneRanger: stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;
    
  case s_loneranger::gear_drive: 
    //change gear into drive so that Lone-Ranger investigation can begin
    {

      /* CONDITIONS GENERIC TO *ALL STRATEGIES* SHOULD BE EVALUATED *ABOVE* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface,  m_pdiag, strprintf( "LoneRanger: %s", currentStageName.c_str() ) );
      skipStageOps = lturnReverse_trans_condition( skipStageOps, m_pdiag, strprintf( "LoneRanger: %s", currentStageName.c_str() ) );
      skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->superConPaused == false ) {
	//NOT superCon paused - need to be to change gear, don't send gear change
	//command until Alice is paused

	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "LoneRanger: superCon e-stop pause not yet in place -> will poll");
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );
      }


      if( m_pdiag->safeGearChangeConds == false ) {
	//conditions NOT yet safe for Alice to change gear -> don't send gear change command yet

	skipStageOps = true;	
	SuperConLog().log( STR, WARNING_MSG, "LoneRanger: alice not yet stationary (safeGearChangeConds == false), can't change gear -> will poll");
	checkForPersistLoop( this, persist_loop_waitstationary, stgItr.currentStageCount() );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_changeGear(sc_interface::drive_gear);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LoneRanger: stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;

  case s_loneranger::trajF_forwards: 
    //send change-mode command to trajFollower ?-->FORWARDS-MODE
    {

      //CONDITIONS GENERIC TO *ALL STRATEGIES* SHOULD BE EVALUATED *ABOVE* THIS POINT
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface,  m_pdiag, strprintf( "LoneRanger: %s", currentStageName.c_str() ) );
      skipStageOps = lturnReverse_trans_condition( skipStageOps, m_pdiag, strprintf( "LoneRanger: %s", currentStageName.c_str() ) );
      skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->adriveGearDrive == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "LoneRanger - Alice not yet in drive gear - will poll");
	checkForPersistLoop( this, persist_loop_gearChange, stgItr.currentStageCount() );
      }


      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_changeTFmode( tf_forwards, 0 );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LoneRanger: stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;

  case s_loneranger::add_speedcap: 
    //send command to trajFollower to add a MIN (& MAX for safety to deal with the case in which the planners
    //have died and are not sending any more trajectories, and trajSelector is sending the same trajectory (even if
    //it's speed is > the cell-speed for any point in the map) speed-caps to the trajFollower
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* SHOULD BE EVALUATED *ABOVE* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface,  m_pdiag, strprintf( "LoneRanger: %s", currentStageName.c_str() ) );
      skipStageOps = lturnReverse_trans_condition( skipStageOps, m_pdiag, strprintf( "LoneRanger: %s", currentStageName.c_str() ) );
      skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->trajFForwards == false ) {
	SuperConLog().log( STR, WARNING_MSG, "Lone Ranger: TrajFollower not yet in forwards mode - will poll");
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_trajfModeChange, stgItr.currentStageCount() );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_modifyTrajFSpeedCap( add_min_speedcap, LONE_RANGER_MIN_SPEED_MS );
	m_pStrategyInterface->stage_modifyTrajFSpeedCap( add_max_speedcap, ( LONE_RANGER_MIN_SPEED_MS + 1 ) );
#warning setting MAX speedCap to (MIN+1) in LoneRanger strategy (explore) is arbitrary and could cause issues with trajFollower - need to test on Alice
	SuperConLog().log( STR, WARNING_MSG, "WARNING: LoneRanger - setting MAX speedCap to LONE_RANGER_MIN_SPEED+1 is arbitrary" ); 
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LoneRanger: stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;

  case s_loneranger::estop_run: 
    //superCon e-stop run Alice now that she is ready to drive FORWARDS
    {

      /* CONDITIONS GENERIC TO *ALL STRATEGIES* SHOULD BE EVALUATED *ABOVE* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface,  m_pdiag, strprintf( "LoneRanger: %s", currentStageName.c_str() ) );
      skipStageOps = lturnReverse_trans_condition( skipStageOps, m_pdiag, strprintf( "LoneRanger: %s", currentStageName.c_str() ) );
      skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->trajFLoneRspeedCap == false ) {
	SuperConLog().log( STR, WARNING_MSG, "LoneRanger speed-cap not yet applied in trajF - will poll");
	skipStageOps = true;
	//no conditional test, as this is NOT the next stage after taking
	//the action
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_run);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LoneRanger - HI-HO SILVER! - explore me some terrain matey! - yarr!");
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LoneRanger: stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;
    
  case s_loneranger::explore_terrain: 
    //stay in this LoneRanger stage whilst exploring the terrain - can't return to nominal as this
    //would clear the speed-cap, and all required transition/termination checks are conducted here
    //NOTE: stgItr.nextStage() is NOT incremented at the end of this stage, as this stage should be polled
    {

      /* CONDITIONS GENERIC TO *ALL STRATEGIES* SHOULD BE EVALUATED *ABOVE* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface,  m_pdiag, strprintf( "LoneRanger: %s", currentStageName.c_str() ) );
      skipStageOps = lturnReverse_trans_condition( skipStageOps, m_pdiag, strprintf( "LoneRanger: %s", currentStageName.c_str() ) );
      skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LoneRanger - Exploring! - shiver-me-timbers, yarr!! (is Jeremy G. wrong?)");
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LoneRanger: stage - %s completed", currentStageName.c_str() );
	//DO NOT UPDATE stgItr.nextStage() here - should wait (poll) in this stage until ready to leave
	//LoneRanger strategy
      }
    }
    break;

    /* END OF STRATEGY */

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyLoneRanger::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyLoneRanger::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: LoneRanger - default stage reached!");
    }
  }
} //end of stepForward()


void CStrategyLoneRanger::leave( CStrategyInterface *m_pStrategyInterface )
{
  skipStageOps.reset(); //resets to FALSE
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "LoneRanger - Exiting");
}


void CStrategyLoneRanger::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset();
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "LoneRanger - Entering");
}
