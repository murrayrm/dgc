#ifndef INTERFACE_SUPERCON_PLN_HH
#define INTERFACE_SUPERCON_PLN_HH

//FILE DESCRIPTION: Header file that contains the definitions of data structures that are
//used in direct communication between SuperCon and the plannerModule 

namespace sc_interface {

/* Use the scMessage method */
//Used for planner --> superCon (conditional) messages *using the scMessage method/message
//ONLY* - these enums should be used in the msgID field of the scMessage, ALSO
//the msgID field only takes an integer - HENCE you should RECAST one of the
//enums (type: scMsgID_PLN) - to an integer (you should recast whichever member
//of the enum details what you want to do - superCon will then perform the inverse
//recast statement so that the code on either side is cleaner - there are reasons
//why this has to be done)
#define PLN_SC_MSGIDS_LIST(_) \
  _( trajInfo, = 0 )
DEFINE_ENUM( scMsgID_PLN, PLN_SC_MSGIDS_LIST )

#define SC_TRAJ_STATUS_LIST(_) \
  _( pln_output_valid_traj, = 0 ) \
  _( pln_failed_deceleration_constraint, )
DEFINE_ENUM( traj_status_type, SC_TRAJ_STATUS_LIST )

}

#endif
