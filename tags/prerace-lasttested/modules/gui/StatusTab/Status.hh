/**
 * Status.hh
 * Revision History:
 * 05/21/2005  hbarnor  Created
 */

#ifndef STATUS_HH
#define STATUS_HH



#include <gtkmm/eventbox.h>
#include <gtkmm/frame.h>
//#include <gtkmm/rc.h>
//#include <glibmm/ustring.h>
#define countdown 2
using namespace std;

/**
 * Status - This is a colored Gtk Frame with a label for displaying 
 * the status of a skynet module. Its green when the module is active 
 * and red otherwise. It also encapsulates the data about the module
 * like its location(computer) and its name(modulename).
 */
class Status : public Gtk::Frame
{

public:
  /**
   * Constructor with string
   *
   */
  Status(const Glib::ustring &label, string host, bool mnemonic=true);
  /**
   * constructor with alignment
   *
   */
  Status(const Glib::ustring &label, Gtk::AlignmentEnum xalign, Gtk::AlignmentEnum yalign, string host, bool mnemonic=true);
  /**
   * Destructor
   */
  ~Status();
  /**
   *
   * accessor methods for the active flags
   */
  void setActive(int newState);
  int getActive();
  /**
   * Counts n-timeouts then,if active makes non-active 
   * without changing colors. If it shows up as not-active
   * then changes color
   */
  bool on_timeout(int count);
private:
  /**
   * do initialization stuff for 
   * label like set colors
   */
  void init();
  /**
   *The label instance inside the fame
   *
   */
  Gtk::EventBox myLabel;
  /**
   * holds the hostname of the module's location
   */
  string m_host;
  /**
   * holds the moduleName
   */
  Glib::ustring name;
  /**
   * flag for updating te color of the 
   * label. True is green and false is red
   */
  int active;
  /**
   * variable to remember state, last time 
   * it was updated
   */
  int oldState;
  /**
   * counter for number of time sto timeout
   */
  int counter;
};

#endif
