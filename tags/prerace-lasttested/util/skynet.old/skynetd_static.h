//void init_data(int argc, char** argv);

void parsecli(int argc, char** argv);

void usage();

void init_key(int argc, char**argv);

void init_soc_filename();

void init_sock_list();

uint32_t get_my_ip();

int mk_modcom_sock();

int mk_chirp_modlist_sock();

int mk_rmulti_listen_sock();
