#include <time.h>
#include <unistd.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>  
#include "DGCutils"

#include "MapDisplay.hh"
// vehicle-independent state struct location.
#include "VehicleState.hh"
// Include the Map class here.
#include "CMapPlus.hh"


#define PATHIDX_HISTORY					0
#define PATHIDX_PLANNED					1
#define PATHIDX_NUMPATHS				2

VehicleState SS;

bool within_waypoint(const RDDFData & waypoint, const UTM_POINT & vehicle_pos)
{
  double dx = waypoint.Easting - vehicle_pos.E;
  double dy = waypoint.Northing - vehicle_pos.N;

  if( pow(dx*dx + dy*dy, 0.5) < waypoint.radius ) return true;
  else return false; 
}

UTM_POINT update_pos( const UTM_POINT & car_pos, const UTM_POINT & dest )
{
  double dx = dest.E - car_pos.E;
  double dy = dest.N - car_pos.N;

  double theta = atan2( dy, dx );

  SS.Yaw = M_PI/2.0 - theta;

  return car_pos + UTM_POINT( 0.01* cos(theta ), 0.01 * sin(theta ));
}

CTraj list_UTM_POINT_to_CTraj( list<UTM_POINT> & history )
{
  CTraj empty;
  return empty;
}


int main () 
{
  // get a state struct 
  SS.Northing = SS.Easting = 0;

  // Initialize the Map class that we will use
  CMap my_cmap;

  my_cmap.initMap(0,0, 10, 10, 1.0, 1.0, 0);
  // Add a layer to the map
  int ladarID = my_cmap.addLayer<double>(-11, -12);

  // The map display thread class
  MapDisplayThread * mdt;

	pthread_mutex_t my_cmap_mutex;
	DGCcreateMutex(&my_cmap_mutex);

  RGBA_COLOR m_pathColors[PATHIDX_NUMPATHS];

  // planned_path, path_history
  PATH paths[PATHIDX_NUMPATHS];

  for(int i=0; i<PATHIDX_NUMPATHS; i++)
  {
    paths[i].points.startDataInput();
  }

  // Read in the RDDF File
  RDDF race_rddf("../RDDF/routes/DARPA_RACE_RDDF.rddf"); 
  RDDFVector waypoints = race_rddf.getTargetPoints();

  cout << "race_rddf has " << race_rddf.getNumTargetPoints() << " points." << endl;

  // MAP DISPLAY STUFF ----------------------------------
  {
    // Initialize the MapDisplayThread with a new MapConfig Struct
    mdt = new MapDisplayThread( "MapDisplayExample", new MapConfig );

    // Pass it the CMap and state struct
    mdt->get_map_config()->set_cmap( &my_cmap, &my_cmap_mutex );
    mdt->get_map_config()->set_state_struct( & SS );

    // and start the thread
    mdt->start_thread();

    mdt->get_map_config()->set_rddf( waypoints );
    cout << "Grabbed " << waypoints.size() << " waypoints." << endl;


    // Set the color for our path history to red.
    m_pathColors[PATHIDX_HISTORY] = RGBA_COLOR(1.0, 0.0, 0.0); // red
    m_pathColors[PATHIDX_PLANNED] = RGBA_COLOR(0.0, 1.0, 0.0); // green
  }

  if (waypoints.size() < 2 )
  {
    cout << "Not enough waypoints" << endl;
    return 0;
  } 

  RDDFVector::iterator x = waypoints.begin();
  RDDFVector::iterator stop = waypoints.end();
  RDDFVector::iterator y;

  SS.Easting  = x->Easting;
  SS.Northing = x->Northing;

  for( ; x != stop; x++ )
  {
    UTM_POINT car_utm(SS.Easting, SS.Northing);

    while( ! within_waypoint( *x, car_utm ) )
    {
      int i;
      for( i=0; i<3 && !within_waypoint(*x, car_utm ); i++ )
      {
        //cout << "Heading for " << x->utm.E << ", " << x->utm.N << endl;

        // head towards the next waypoint
        car_utm = update_pos( car_utm , UTM_POINT( x->Easting, x->Northing) );
        SS.Easting  = car_utm.E;
        SS.Northing = car_utm.N;
      }

      // Add our current position to the path history. If our history has too 
      // many points in it, we need to shift them and add
      int num_points;
      if( (num_points = paths[PATHIDX_HISTORY].points.getNumPoints()) > TRAJ_MAX_LEN-2 )
      {
        paths[PATHIDX_HISTORY].points.shiftNoDiffs(1);
      }
      paths[PATHIDX_HISTORY].points.inputNoDiffs(car_utm.N, car_utm.E);  


      // create a "planned" path which simply includes the next couple of waypoints
      paths[PATHIDX_PLANNED].points.startDataInput();
      paths[PATHIDX_PLANNED].points.inputNoDiffs( car_utm.N, car_utm.E );

      for( i=0, y=x; i<5; i++, y++ ) 
        paths[PATHIDX_PLANNED].points.inputNoDiffs( y->Northing, y->Easting );

      mdt->get_map_config()->set_paths( paths, PATHIDX_NUMPATHS);

      // Update the CMap center
      my_cmap.updateVehicleLoc( SS.Northing, SS.Easting );


      // draw some random data on the map
      static bool drawn_already = false;
      if( !drawn_already )
      {
        for(i = 1; i<100; i=i+3)
        {
          //double theta = M_PI/2.0 - SS.Yaw + theta_scale * random() - 
          //theta_mid;
          //double radius = 5.0; //scale * random();
          drawn_already = true;
          my_cmap.setDataWin<double>( ladarID, (int) (i/10), (int)(i%10), i );
        }
      }

      mdt->notify_update();
      usleep(500000);
    }
    static int ctr= 0;
    cout << "Reached waypoint " << ctr++ << endl;
  }

	DGCdeleteMutex(&my_cmap_mutex);
  return 0;

};





