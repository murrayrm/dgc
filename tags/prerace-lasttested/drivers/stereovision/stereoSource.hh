#ifndef __STEREOSOURCE_H__
#define __STEREOSOURCE_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libraw1394/raw1394.h>
#include <libdc1394/dc1394_control.h>

#include <opencv/cv.h>
#include <opencv/cvaux.h>
#include <opencv/highgui.h>

#include "pid.hh"
#include <Magick++.h>

#include "DGCutils"

/*-----------------------------------------------------------------------------*/
/**
  @brief A struct to contain pointers to left and right images in memory
*/
/*-----------------------------------------------------------------------------*/
typedef struct{
  //! A pointer to the left image of a stereo pair
  unsigned char* left;

  //! A pointer to the right image of a stereo pair
  unsigned char* right;
} stereoPair;


enum {
  stereoSource_OK,
  stereoSource_UNKNOWN_SOURCE,
  stereoSource_NO_FILE,
  stereoSource_UNKNOWN_ERROR,
  stereoSource_NOT_IMPLEMENTED,

  stereoSource_COUNT
};


enum {
  SOURCE_UNASSIGNED,
  SOURCE_CAMERAS,
  SOURCE_FILES,

  NUM_SOURCE_TYPES
};


enum {
  LEFT_CAM,
  RIGHT_CAM,

  MAX_CAMERAS
};

/*-----------------------------------------------------------------------------*/
/**
  @brief A general stereoSource class for grabbing images from firewire

  stereoSource is a class for grabbing images from firewire cameras, as well as
  doing some minimal processing (e.g. converting color images to grayscale) and
  saving/reading images from files.  At present stereoSource only supports
  working with two cameras (or two sets of logged images) at once - there's no
  real way to make it work with only one camera.
  
  Possible future improvements (please remove these as they are completed):
  - Making a separate imageSource class which wraps around most of the firewire
  stuff, and making stereoSource use instances of it to do the stereo driver
  stuff.  
  - Fixing the exposure control system to make it more intelligent (e.g. make it
  a PID controller instead of a simple proportional controller).  You could
  maybe use the PID class that has been implemented to accomplish this.
  - Cleanup of code to make sure there are no memory leaks, uninitialized
  variables, etc.
  - Cleanup of code to unify error messages.
  - (Note that online calibration isn't here, as it really belongs more to the
  stereoProcess class.)

*/
/*-----------------------------------------------------------------------------*/
class stereoSource {
public:
  //! A basic constructor, initializes various internal variables
  stereoSource();

  //! A basic destructor - doesn't do any clean up yet!
  ~stereoSource();
  /*---------------------------------------------------------------------------*/
  /**

    @brief  Initializes the stereoSource object to grab images from cameras
    @param  verboseLevel  A verbosity level (not actually implemented)
    @param  camIDFilename The filename of the file to read parameters from
    @param  logFilename   The base filename to log to
    @param  logFileType   The filetype to log
    @return Standard stereoSource status code

    This method initializes the stereoSource object, and MUST be called before
    you try to do anything else with the object (unless you use the other
    initialization method).  You can give it a verbosity level which would in
    theory tell the object what level of error or status messages to print out.
    (At present, this isn't implemented.)  You MUST give it a camIDFilename,
    which tells it where to find the file that contains the various parameters
    about the cameras (e.g. ID string, frame rate, color/mono, etc.).
    Optionally, if you want to log images automatically, you can give the object
    a base filename and filetype.  Images will then be logged in the format
    described in the documentation for the grab(char*, char*, int) function.  

  */
  /*---------------------------------------------------------------------------*/
  int init(int verboseLevel, char *camIDFilename, char *logFilename = "", char
	   *logFileType = "", char *svsFilename= "");

  /*---------------------------------------------------------------------------*/
  /**

    @brief  Initializes the stereoSource object to read from files
    @param  verboseLevel A verbosity level (not actually implemented)
    @param  width        The width of the images to read (pixels)
    @param  height       The height of the images to read (pixels)
    @param  baseFilename The base filename of the logged images
    @param  baseFileType The filetype of the logged images
    @param  start        The starting frame number to read

    This method initializes a stereoSource object to read logged images from
    files, and MUST be called BEFORE you try to do anything else with the object
    (unless you use the other initialization method).  You can give it a
    verbosity level which would in theory tell the object what level of error or
    status messages to print out.  (At present, this isn't implemented.)  You
    also have to give it the dimensions of the logged images (width and height),
    as well as the base filename of the images and their type.  Images will then
    be read if their filenames correspond to the format described in the
    grab(char*, char*, int) function.  (You can also optionally give this method
    a frame number to start reading from - the default is 0.)

  */
  /*---------------------------------------------------------------------------*/
    int init(int verboseLevel, int width, int height, char *baseFilename, char
    *baseFileType, int start = 1);

  /*---------------------------------------------------------------------------*/
  /**
    @brief Stops the cameras to prepare them for shutdown
    @return Standard stereoSource status code

    This hasn't actually been implemented yet.

  */
  /*---------------------------------------------------------------------------*/
  int stop();

  /*---------------------------------------------------------------------------*/
  /**
    @brief Grabs an image pair from the cameras and stores it internally
    @return Standard stereoSource status code

    This must be called to load an image pair from the cameras.
  */
  /*---------------------------------------------------------------------------*/
  unsigned long long grab();
  
  /*---------------------------------------------------------------------------*/
  /**
    @brief Grabs an image pair from file and stores it internally
    @param  baseFilename A string describing the base filename
    @param  baseFileType A string describing the filetype
    @param  num          Which frame number to load
    @return Standard stereoSource status code

    This must be called to load an image pair from file.  Filenames must be of
    the form baseFilenameXXXX-L.filetype and baseFilenameXXXX-R.filetype, where
    the baseFilename is just some baseFilename, XXXX corresponds to the frame
    number (with leading zeroes), and the filetype is an image filetype.
    (Currently supported filetypes are bmp and jpg, although using JPGs is not
    recommended as they are lossy and can't be used for stereovision
    processing.  (Also, note that you don't need to give the method leading
    zeroes when you pass it the num argument - just give it a normal integer.) 

  */
  /*---------------------------------------------------------------------------*/
  unsigned long long grab(char *baseFilename, char *baseFileType, int num);
  
  /*---------------------------------------------------------------------------*/
  /**
    @brief  Saves the latest grabbed images to file
    @return Standard stereoSource status code
    
    This function can be used to quick-save the latest grabbed images to file.
    It can only be used if you gave the initialization method the optional base
    filename and base filetype arguments.  If you did, then this method will use
    those filenames/file types to save the images with the filename format
    specified in the grab(char*, char*, int) function.  If you did not give
    those arguments, it will return an error.

  */
  /*---------------------------------------------------------------------------*/
  int save();

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Saves the latest grabbed images to a specific file
    @param  baseFilename The base filename of the image pair to save
    @param  baseFileType The filetype to save the image pair as
    @param  num          The frame number to save to
    @return Standard stereoSource status code

    This function can be used to explicitly save the latest grabbed images to a
    specific file.  It will use the same filename format as described in the
    documentation for the grab(char*, char*, int) function.

  */
  /*---------------------------------------------------------------------------*/
  int save(char *baseFilename, char *baseFileType, int num);

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Returns a pointer to the left image in memory
    @return Pointer to the left image of the stereo pair

    This method returns a pointer to the left black and white raw image data in
    memory. It's basically just an array stored linearly in memory.  For
    example, to access the pixel in the 3rd row and the 8th column, you would
    do:
    
    @code
    unsigned char* imagePtr = mySourceObject.left();
    int width = mySoureObject.width();
    char myPixel = imagePtr[3*width+8];
    @endcode

    If you modify the memory pointed to by this pointer, this WILL modify the
    image stored in the sourceObject's memory, and so if you save the image
    after modifying it, it wil save your changes.

  */
  /*---------------------------------------------------------------------------*/    
  unsigned char* left();

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Returns a pointer to the right image in memory
    @return Pointer to the right image of the stereo pair

    This method returns a pointer to the right black and white raw image data in
    memory.  It is exactly the same as the left() accessor, except it returns the
    right image, not the left.  For more information, see the documentation for
    the left() accessor.
  */
  /*---------------------------------------------------------------------------*/
  unsigned char* right();

  /*---------------------------------------------------------------------------*/
  /**
    @brief  Returns a stereoPair object for the latest grabbed images in memory
    @return A stereoPair object pointing to the pair in memory

    This method returns a stereoPair object, containing pointers to the left and
    white grayscale images in memory.  For more information on how to work with
    the images in this raw mode, see the documentation for the left() accessor.
  */
  /*---------------------------------------------------------------------------*/
  stereoPair pair();

  /*---------------------------------------------------------------------------*/
  /**
    @brief Returns the source type.
    @return Current source type

    The source type is either SOURCE_CAMERAS, SOURCE_FILES, or SOURCE_UNASSIGNED

  */
  /*---------------------------------------------------------------------------*/
  int currentSourceType();

  //! Returns the frame number of the latest grabbed image pair
  int pairIndex();

  //! Returns the width of the image in pixels
  int width();

  //! Returns the height of the image in pixels
  int height();

  //! Resets the frame number index to 0
  int resetPairIndex();

  int startExposureControl(CvRect imageROI);
  int stopExposureControl();

  int setFeatureEnableAutoShutter(int status);
  int setFeatureEnableAutoExposure(int status);
  int setFeatureEnableAutoGain(int status);

  int setFeatureValShutter(double scale);
  int setFeatureValExposure(double scale, int camera_nbr);
  int setFeatureValGain(double scale);

  double getFeatureValShutter();
  double getFeatureValExposure(int camera_nbr);
  double getFeatureValGain();

  double getAvgBrightness(int camera, int x_start, int x_stop, int y_start, int y_stop);

  double getAvgROIBrightness(int camera_nbr);

  int getExposureControlStatus() {return _exposureControlStatus;};
  double getExposureScaleValue(int camera_nbr);
  double getExposureRefValue() {return _exposureRefVal;};
  void setExposureRefValue( double refVal ){ _exposureRefVal = refVal;};

  int setExposureControlGain(double gain);

  int setExposureControl(double p, double i, double d, double s);

  int startPidControl();
private:
  pthread_mutex_t cameraMutex;
  dc1394_cameracapture cameras[MAX_CAMERAS];
  raw1394handle_t handle;
  int node2cam[MAX_CAMERAS];

  IplImage* _images[MAX_CAMERAS];
  IplImage* _colorImages[MAX_CAMERAS];
  IplImage* _temp[MAX_CAMERAS];
  CvSize pairSize;

  CvRect _imageROI;
  CvRect  _dazzleSubwindow[20];

  char _currentFilename[256];
  char _currentFileType[10];

  int _currentSourceType;
  int _pairIndex;

  int _verboseLevel;

  int _exposureControlStatus;
  int _pidControlStatus;
  
  double _exposureControlGain;
  Cpid _pidControl;

  double _exposureRefVal;
  double _exposureVal_left, _exposureVal_right;
};

#endif  //__STEREOSOURCE_H__
