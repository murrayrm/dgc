The Generic Map Class:
a Documentation File from Adam Craig Productions

Introduction: The purpose of the generic map class is to provide a standardized, reusable structure in which to store data about the immediate surroundings of the challenge vehicle. 
One advantage of this approach is that, because each subsystem concerned with navigation and obstacle avoidance will need to store similar types of data about the environment in which the vehicle is operating and will need to access and process those data in similar ways to generate votes to the arbiter, it prevents duplication of code for all such systems to be able to use a uniform map object with predefined functions that perform the manipulations not specific to any given sensor or other raw data source. 

To use the genMap class in a program, one includes the genMap.hh file and numbers this among the directories to include in the makefile. 
Examples of how one might do this appear in Thyago Consort's test.c and Makefile in the directory genMap.
This program demonstrates many of the useful features of the genMap class.
To compile it, one types make test, and, to run it one types ./test -x, where x is the number of the method to demonstrate:

1. Constructor: when declaring an object of type genMap, one should have 5 arguments in parentheses: one of type VState_GetStateMsg in which to store the initial state of the vehicle, two of type int, representing the number of rows and the number of columns respectively, and two of type double, representing the row resolution in meters on a side of a cell and column resolution in the same units respectively. 
One can also declare a map with the empty constructor or with the copy constructor.

2. RowColFromDeltaRowCol: this member function takes a variable of type RowCol that represents the location of a cell on the map and another RowCol that represents a displacement of a certain number of rows and columns and returns the RowCol that resultst from that displacement from the initial location.
This function demonstrates the wrap-around property of genMap. 
The data in genMap represents a rectangular window of set dimensions with the vehicle at its center. 
However, the vehicle is not always at the center of the matrix of map data in memory. 
Instead, the edges of the map wrap around, so that if, for instance, the vehicle is near the upper right corner of the matrix, the borders of the window are two perpendicular lines that intersect in the lower left corner. 
All points below the horizontal line are, in effect, above the horizontal line representing the top edge of the matrix, and all points to the left of the vertical line are, for the purposes of the window, to the right of the vertical line representing the right edge of the matrix.

3. RowColFromDeltaPosition: This is a very similar function, but it also performs the transformation from the UTM coordinates of the point that the user enters to the RowCol of the cell containing that point.

4. FrameRef2RowCol: Given a pair of coordinates of a point relative to the position and heading of the vehicle, this function transforms them first to UTM coordinates, then to the RowCol of the cell containing the point.

5. UTM2RowCol: Given a pair of UTM coordinates, this function returns the RowCol of the cell containing that point.

6. EvaluatePathCost: EvaluatePathCost accepts a double representing the steering angle and uses it to determine over which cells the vehicle will pass if it continues forward a certain distance with that steering angle. 
It then computes the goodness cost from traversing those cells.
The current method of estimating goodness cost treats the value member of the struct CellData as the elevation, based on the premise that it is more difficult to traverse areas with steeper inclines in elevation.
The algorithm quantifies this by taking the sum of the maximum difference between each cell and the cells adjacent to it.
When no function call has assigned data to a given cell, it has a certain constant value. This value is currently 0, but will probably be different in future versions. New methods of evaluating cost of traversal are currently in developement, including methods specifically designed for areas where unknown cells are densely distributed among the cells with data.

7. set/get CellDataRC: These functions access the value member of the cell at the row and column in the arguments if it is in the window.

8. set/get CellDataUTM: This pair of functions works in much the same way but performs the transform from the pair of UTM coordinates to the RowCol of the cell that contains them.

9. set/get CellDataFrameRef: Like the previous pairs, this pair accesses the value member of a cell, but the coordinates it accepts are in the referrence frame of the vehicle.

*lists
Recently, we added a member cellList to the struct CellData, which is an array of doubles. One can add a value to this list with any of the three setCellListData"" functions. Because some sensors try to assign multiple values to the same cell, we decided on this approach so that it could store all of the values assigned to a cell in a single sensor sweep. One can then call another function, evaluateCellsSV(), to assign the highest elevation value in the list to the value member and to clear the list, so that one can repeat the process at the next time step.

10. Generate takes an array of steering angles, evaluates the cost of the path that each one determines, and returns an array of votes that one can send to the arbiter. 
These votes are on a scale of 0 to 100, representing goodness costs ranging from untraversable to completely open to traversal.

11. updateFrame: This function updates the state data of the vehicle as stored in the map object and shifts the window, so that the vehicle remains at its center. 
Instead of shifting the data in each cell, this function utilizes the wrap-around characteristic of the map. 
It adjusts the location of the border of the map within the matrix. 
Cells that were initially on one side of the border that then are on the other side cease to represent the the areas that were in the window but that have since moved out of the window and become cells representing the areas that have moved into the window. As such, the function clears these cells.

12. setCellListDataUTM: Please see *lists.  

13. saveMATfile: this function takes three arguments: the file name to which to save the map data, the name of the MATLAB variable in which to save it, and a message, preferrably with a '%' preceeding each line, as a header. It saves the current genMap window in the MATLAB format.

We hope you have enjoyed this tour of the magical world of the generic map class. Thank you and good night.

