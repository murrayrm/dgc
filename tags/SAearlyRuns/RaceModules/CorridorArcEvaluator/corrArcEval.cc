#include <sys/time.h>
#include <iomanip>
#include <algorithm>  // for min
#include <stdio.h>

#include "CorridorArcEvaluator.hh"

#include "corrArcEval.hh"

// TODO: Remove this hack added 04 Mar 04 to put an obstacle into the map
// at the center of the santaanita_figureeight
//#define HACK_DEE_HACK_HACK

using namespace std;

int CorridorArcEvaluator::InitCorrArcEval() 
{
  // this is where everything gets started up
  cout << "[InitCorrArcEval] Starting InitCorrArcEval()" << endl;

  //Initialize the map
  d.corrArcEmap.initMap(DEFAULT_CORRARCEMAP_NUM_ROWS, 
		        DEFAULT_CORRARCEMAP_NUM_COLS, 
			DEFAULT_CORRARCEMAP_ROW_RES, 
			DEFAULT_CORRARCEMAP_COL_RES, true);
  //d.corrArcEmap.initPathEvaluationCriteria(false,false,false,true,false);
  d.corrArcEmap.initPathEvaluationCriteria(false,false,false,false,false,false,true,false);

  //printf("InitCorrArcEval:d.corrArcEmap.getRowRes() = %f\n", d.corrArcEmap.getRowRes() );  
}

void CorridorArcEvaluator::CorrArcEvalUpdateVotes() 
{
  // This is where we send votes to the arbiter

  // TODO: Remove this hack added 04 Mar 04 to put an obstacle into the map
  // at the center of the santaanita_figureeight
#ifdef HACK_DEE_HACK_HACK
  RDDFVector targetPoints;
  double northing,easting;
  int numTargetPoints;
  // set a cell 1 meter high, of color zero (?), normal, and zero time stamp 
  CellData boink(-1, 0, NORMAL_CELL, 0);  
  // Getting the target points
  numTargetPoints = d.corrArcEmap.getNumTargetPoints();
  targetPoints = d.corrArcEmap.getTargetPoints();
  // This is the approximate location of the center of the figure eight
  for(int i=0;i < (numTargetPoints-1); i++){
    northing = (targetPoints[i].Northing + targetPoints[i+1].Northing)/2;
    easting = (targetPoints[i].Easting + targetPoints[i+1].Easting)/2;
    d.corrArcEmap.setCellDataUTM(northing,easting, boink );
  }
#endif

/*  Uber-pseudo code: 
 *  - instantiate a new instance of genMap
 *  - Overlay corridor into map
 *     - Use methods in genMap?
 *  - Evaluate arcs
 *     - ArcEvaluator?
 *  - Send results to arbiter 
 *     - goodness based on length of arc from _front_ of vehicle to boundary
 *     - veloctiy also based on length of arc from front of vehicle to boundary 
 */

/*  // *  - instantiate a new instance of genMap
  //  cout << "waypoints" << endl;
  RowCol currentWP = d.corrArcEmap.UTM2RowCol(d.wp.get_current().easting, d.wp.get_current().northing);
  RowCol nextWP = d.corrArcEmap.UTM2RowCol(d.wp.get_next().easting, d.wp.get_next().northing);

  // *  - Overlay corridor into map
  //  cout << "overlay corridor into map" << endl;
  RowCol deltaWP;
  deltaWP.first = nextWP.first - currentWP.first;
  deltaWP.second = nextWP.second - currentWP.second;
  d.corrArcEmap.paintCorridor(currentWP, deltaWP);
*/
  //  cout << "starting steerAngle" << endl;
  vector<double> steerAngle;
  steerAngle.reserve(NUMARCS);
  bool firstLoop = true;    
  int i;

  if(firstLoop){
    for(i = 0; i < NUMARCS; i++) {
    //    steerAngle[i] = GetPhi(i);
      steerAngle.push_back(GetPhi(i));
      //    cout << "i: " << steerAngle[i];
    }
    firstLoop = false;
  }
  else{
    for(i = 0; i < NUMARCS; i++) {
      steerAngle[i] = GetPhi(i);
    }
  }

  
  // *  - Evaluate arcs
  //  cout << "evaluate arcs" << endl;
  vector< pair<double, double> > votes;
  //  cout << "get votes.." << endl;
  votes = d.corrArcEmap.generateArbiterVotes(NUMARCS, steerAngle);
 
  //cout << "current pos: (" << d.SS.Northing << ", " << d.SS.Easting << ")" <<  endl;

  // *  - Send results to arbiter 
  //cout << "sending votes to arbiter" << endl;
  for (int i=0; i<NUMARCS; i++) {
    d.corrArcEval.Votes[i].Goodness = votes[i].second;
//    cout << "Arc: " << i << ", Goodness: " << d.corrArcEval.Votes[i].Goodness;//votes[i].first;
    d.corrArcEval.Votes[i].Velo = votes[i].first;
  //  cout << ", Velocity: " << d.corrArcEval.Votes[i].Velo << endl;//votes[i].second << endl;
  }
  
} // end CorrArcEvalUpdateVotes


void CorridorArcEvaluator::CorrArcEvalShutdown() 
{

}
