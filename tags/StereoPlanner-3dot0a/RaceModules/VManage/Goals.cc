#include "VManage.hh"

#include <iostream>
using namespace std;

extern int DEBUG_ON;
extern int ERR_OUT;


int DecelerationStarted = false;
double DecelerationStartSpeed;
Timeval DecelerationStartTime;


int LoopTime = 30000; // 30ms per loop
double deltaT = ((double) LoopTime) / 1000000.000;



void VManage::GoalsLoop() {

  while( !ShuttingDown() ) {

    // Set the allowed velocity.
    switch( d.s.vdrive.estop.EStop ) {
    case 'p': case 'P':
    case 'd': case 'D':
      {
	d.s.vdrive.max_velo = 0.0; // If we are not using decelration mode, just stop the car immediately.
      }
      break;
    default:
      // If we are not in any of the EStop Modes (pause, disable), 
      // set velocity so that car can go.
      d.s.vdrive.max_velo = 99.0; // higher than any value Arbiter would give.
      DecelerationStarted = false;
    }

    // Check transmission shifting
    if( d.g.SetTransmissionGear != d.s.transmission.Gear ) {
      d.TransmissionEStopOn = true;
      
      switch( d.s.vdrive.estop.EStop ) {
      case 'p': case 'P':
      case 'd': case 'D':
	// If the EStop is set and the car is not moving, 
	// then we can switch gears
	if( d.s.obd.valid == true && d.s.obd.Velocity == 0.0 ) {
	  sleep(2);
	  trans_setposition(d.g.SetTransmissionGear);
          d.s.transmission.Gear = d.g.SetTransmissionGear;
	  d.TransmissionEStopOn = false;
	  d.EStopGoTime = TVNow();
	}
	break;
 
      }
    } else if( d.TransmissionEStopOn ) {
      Lock mylock( d.VMDatumMutex );
      d.TransmissionEStopOn = false;
      d.EStopGoTime = TVNow();
    }

    usleep( LoopTime );
    // make decisions
  }
}


