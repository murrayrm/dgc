#include "VManage.hh"

#include <iostream>

#include "vehlib/estop.h"
using namespace std;

extern int DEBUG_ON;
extern int ERR_OUT;

void VManage::EStopLoop() {

  d.EStopValid = true;

#ifndef ESTOP
  // No-Estop code (remove when estop is working
  // update the datum
  {
    Lock mylock( d.VMDatumMutex );
    d.EStopMode = 'o';
    d.EStopGoTime = TVNow(); // set time of "un-pause"
  }
  // end No-EStop Code
#else
  // Initialize EStop
  //  cout << "ESTOP OPEN PORT = " << d.EStopPort << endl;
  estop_open( d.EStopPort);
  //  cout << "ESTOP OPEN PORT COMPLETE = " << endl;
#endif

  char estopval = 'O';

  while( !ShuttingDown() ) {

    usleep( 100000 );

    //estop = GetEStop()
#ifdef ESTOP
    int s = estop_status();
    //    cout << "ESTOP STATUS = " << s << endl;
    switch( s) {
    case -1:  
      //      cout << "ERROR READING ESTOP -- SETTING PAUSE" << endl;
      d.EStopValid = false;
      estopval = 'p';
      break;
    case 0: estopval = 'o';  d.EStopValid = true;   break;
    case 1: estopval = 'p';  d.EStopValid = true;   break;
    case 2: estopval = 'd';  d.EStopValid = true;   break;
    default: cout << "GET ESTOP RETURN VALUE INVALID -- " << estopval << endl;
    }
#endif 


    // update the datum
    {
      Lock mylock( d.VMDatumMutex );
      
      // make estop, and datum-Estop lowercase
      if( estopval < 'a' ) estopval += 'a'- 'A';
      if( d.EStopMode < 'a' ) d.EStopMode += 'a' - 'A';

      //      cout << "estopval = " << estopval 
      //	   << ", " << d.EStopMode << endl;

      // check to see if we want to make a change to the EStop variables
      if( estopval != d.EStopMode ) {
	switch( estopval ) {
	case 'p':  d.EStopMode = 'p';  break;
	case 'd':  d.EStopMode = 'd';  break;
	case 'o':    
	  d.EStopMode = 'o';
	  d.EStopGoTime = TVNow(); 
	  break;
	default:
	  cout << "Invalid ESTOP MODE -" << estopval 
	       << "- SETTING PAUSE FOR NOW" << endl;
	  d.EStopMode = 'p';    break;
	}
      }
    }
  }
}


