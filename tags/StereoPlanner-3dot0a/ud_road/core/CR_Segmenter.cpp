//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_Segmenter.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

//extern int gabor_filters_initialized;

extern int left_down;

CR_KalmanFilter *fish_kal;

CR_Condensation *seg_con;
CR_Matrix *seg_cov;

int *seg_voter_disc_left_edge_col;
int *seg_voter_disc_right_edge_col;
float *seg_voter_disc_edge;
int fish_lateral_frame, fish_top, fish_left_bottom, fish_right_bottom;
CR_Matrix *fish_in, *fish_out;
int fish_in_index, fish_out_index;
CR_Vector *fish_right_line, *fish_left_line;
CR_Matrix *fish_left_edge_points, *fish_right_edge_points;
int fish_right_edge_row, fish_left_edge_row;
CR_Vector *fish_p1, *fish_p2, *fish_line;
int fish_left_xt, fish_left_yt, fish_left_xb, fish_left_yb;
int fish_right_xt, fish_right_yt, fish_right_xb, fish_right_yb;
CR_Vector *fish_vp;

float *seg_voter_disc;
//CR_Matrix *seg_voter_disc;
float disc_R, disc_G, disc_B;
CR_Vector *fish_disc;

double seg_prior_covalpha;
CR_Matrix *seg_prior_cov, *seg_prior_covsqrt;
CR_Vector *seg_prior_mean;

extern int planx_voter_h, planx_voter_w;

int seg_voter_left, seg_voter_right, seg_voter_top, seg_voter_bottom;
int seg_voter_dx, seg_voter_dy, seg_voter_w, seg_voter_h;

extern int planx_candidate_left, planx_candidate_right, planx_candidate_top, planx_candidate_bottom;
extern int planx_candidate_dx, planx_candidate_dy;

extern int planx_w, planx_h;                  // raw image dimensions
extern float fplanx_w, fplanx_h;

float hist_x, hist_y, hist_w, hist_h, hist_num_bins;
float hist_y_top, hist_y_bottom, hist_w_bottom, hist_slope, hist_w_top;

CR_Vector *r_histogram, *g_histogram, *b_histogram;
double hist_product_thresh, hist_and_thresh;
float *hist_seg_class;
//CR_Matrix *uv_histogram;

double seg_prior_x, seg_prior_y;

//----------------------------------------------------------------------------

void init_seg_voter_region(int voter_top, int voter_bottom, int voter_left, int voter_right, 
			   int voter_dx, int voter_dy)
{
  int i, j, t, x, y;

  // voters

  seg_voter_left = voter_left;
  seg_voter_right = voter_right;
  seg_voter_top = voter_top;
  seg_voter_bottom = voter_bottom;
  seg_voter_dx = voter_dx;  
  seg_voter_dy = voter_dy;  

  for (y = voter_top, seg_voter_h = 0; y < voter_bottom; y += voter_dy, seg_voter_h++)
    for (x = voter_left, seg_voter_w = 0; x < voter_right; x += voter_dx, seg_voter_w++)
      ;


}

//----------------------------------------------------------------------------

void init_seg_kalman()
{
  CR_Matrix *X, *F, *H, *Q, *R, *X0, *P0;

  X = make_CR_Matrix("X", 3, 1);
  X0 = make_CR_Matrix("X", 3, 1);
  MATRC(X0, 0, 0) = 0.0;
  MATRC(X0, 1, 0) = 0.0;
  MATRC(X0, 2, 0) = 1.0;
  F = make_CR_Matrix("X", 3, 1);
  F = init_diagonal_CR_Matrix("F", 3, 1.0, 1.0, 1.0); 
  H = init_diagonal_CR_Matrix("H", 3, 1.0, 1.0, 1.0); 
  Q = init_diagonal_CR_Matrix("Q", 3, 0.001, 0.001, 0.001);    // dynamics covariance
  R = init_diagonal_CR_Matrix("R", 3, 0.0001, 0.0001, 0.0001);    // measurement covariance
  P0 = init_diagonal_CR_Matrix("P0", 3, 0.0001, 0.0001, 0.0001);  // initial state covariance

  //  print_CR_Matrix(Q);
  //  exit(1);

  fish_kal = make_CR_KalmanFilter(X, F, H, Q, R, X0, P0, NULL);

}

//----------------------------------------------------------------------------

// samp has road region geometric parameters embedded
// samp->x[0] = vp x
// samp->x[1] = vp y
// samp->x[2] = theta center -- measured counterclockwise from pointing left along horizon
// samp->x[3] = theta width

void initialize_segmenter()
{
  int i;

  init_seg_kalman();

  fish_lateral_frame = 10;
  fish_top = planx_w / 4;
//   fish_right_bottom = 3 * planx_h / 4;
//   fish_left_bottom = 2 * planx_h / 4;
//   fish_right_bottom = 3 * planx_h / 4;
//   fish_left_bottom = 3 * planx_h / 4;
  fish_right_bottom = planx_h;
  fish_left_bottom = planx_h;

  fish_in = make_CR_Matrix("fish in", planx_w * planx_h, 3);
  fish_out = make_CR_Matrix("fish in", planx_w * planx_h, 3);
  fish_disc = make_CR_Vector("fish disc", 3);
  fish_disc->x[0] = 0.0;
  fish_disc->x[1] = 0.0;
  fish_disc->x[2] = 1.0;

  init_seg_fisher(fish_in, fish_out, fish_disc);

  //  if (!gabor_filters_initialized)
  //   CR_error("gabor filters must be initialized first");

  seg_prior_x = 5;
  seg_prior_y = 5;

//   disc_R = 0.333;
//   disc_G = 0.333;
//   disc_B = 0.333;
  disc_R = 0.0;
  disc_G = 0.0;
  disc_B = 1.0;

  fish_right_edge_points = make_CR_Matrix("right edge points", planx_h, 2);
  fish_left_edge_points = make_CR_Matrix("left edge points", planx_h, 2);
  fish_right_line = make_CR_Vector("right line", 3);
  fish_left_line = make_CR_Vector("left line", 3);
  fish_p1 = make_CR_Vector("p1", 3);
  fish_p2 = make_CR_Vector("p2", 3);
  fish_p1->x[2] = fish_p2->x[2] = 1.0;
  fish_line = make_CR_Vector("line", 3);
  fish_vp = make_CR_Vector("vp", 3);

  seg_voter_disc = (float *) CR_calloc(planx_w * planx_h, sizeof(float));
  seg_voter_disc_edge = (float *) CR_calloc(planx_w * planx_h, sizeof(float));
  seg_voter_disc_left_edge_col = (int *) CR_calloc(planx_h, sizeof(int));
  seg_voter_disc_right_edge_col = (int *) CR_calloc(planx_h, sizeof(int));
  for (i = 0; i < planx_w * planx_h; i++) {
    seg_voter_disc[i] = 0.0;
    seg_voter_disc_edge[i] = 0.0;
  }
  //  seg_voter_disc = make_CR_Matrix("seg_voter_disc", seg_voter_h, seg_voter_w);

  //  seg_prior_mean = init_CR_Vector("seg_prior_mean", 4, planx_w / 2, planx_h / 3, PI/2, PI/2);
  seg_prior_mean = make_CR_Vector("seg_prior_mean", 4);
  seg_prior_mean->x[0] = planx_w / 2;
  seg_prior_mean->x[1] = planx_w / 3;
  seg_prior_mean->x[2] = PI / 2;
  seg_prior_mean->x[3] = PI / 2;

  // var = sigma^2, 95% within 2*sigma
  // horizontal: 2*sigma = planx_w / 4 => sigma = planx_w / 8 
  // vertical: 2*sigma = planx_h / 6 => sigma = planx_h / 12 
  // center: 2*sigma = PI/6 => sigma = PI/12
  // width: 2*sigma = PI/4 => sigma = PI/8

//   seg_prior_cov = init_diagonal_CR_Matrix("seg_prior_cov", 4, 
// 					  SQUARE(planx_w / 8), SQUARE(planx_h / 12), 
// 					  SQUARE(PI / 12), SQUARE(PI / 8));

  seg_prior_cov = make_all_CR_Matrix("seg_prior_cov", 4, 4, 0.0);
  MATRC(seg_prior_cov, 0, 0) = SQUARE(planx_w / 8);
  MATRC(seg_prior_cov, 1, 1) = SQUARE(planx_h / 12);
  //  MATRC(seg_prior_cov, 2, 2) = SQUARE(PI / 12);
  //  MATRC(seg_prior_cov, 3, 3) = SQUARE(PI / 8);
  MATRC(seg_prior_cov, 2, 2) = 0.02;
  MATRC(seg_prior_cov, 3, 3) = 0.02;

//   print_CR_Vector(seg_prior_mean);
//   print_CR_Matrix(seg_prior_cov);
//   exit(1);

  seg_prior_covsqrt = copy_CR_Matrix(seg_prior_cov);
  square_root_CR_Matrix(seg_prior_cov, seg_prior_covsqrt);
  seg_prior_covalpha = gaussian_alpha_CR_Matrix(seg_prior_cov);

  initialize_seg_particlefilter();

  /*
  int i;

  for (i = 0; i < seg_con->n; i++) {
    if (!legal_roadregion(seg_con->s[i])) 
      printf("bad: ");
    else 
      printf("good: ");
    print_CR_Vector(seg_con->s[i]);
  }
  exit(1);
  */

  // histogram stuff

  hist_num_bins = 8;

  r_histogram = make_CR_Vector("r_histogram", hist_num_bins);
  g_histogram = make_CR_Vector("g_histogram", hist_num_bins);
  b_histogram = make_CR_Vector("b_histogram", hist_num_bins);

  hist_product_thresh = 0.1;
  hist_and_thresh = 0.333;

  hist_seg_class = (float *) CR_calloc(planx_w * planx_h, sizeof(float));

  //uv_histogram = make_CR_Matrix("uv_histogram", hist_num_bins, hist_num_bins);

  hist_x = 0.50 * fplanx_w;

  //  hist_y = 85 * planx_h / 100;  
  //  hist_w = planx_w / 2;
  //  hist_h = planx_h / 4; 
		
  hist_y_top = 0.50 * fplanx_h;
  hist_y_bottom = 0.95 * fplanx_h;
  hist_w_bottom = 0.50 * fplanx_w;
  hist_slope = 1.5;

  //  hist_y_top = 100;
  //  hist_y_bottom = 150;
  //  hist_w_bottom = 100;
  //  hist_slope = 2.0;

  hist_w_top = hist_w_bottom - 2.0*(hist_y_bottom-hist_y_top)/hist_slope;
  //  printf("y_top = %f, y_bot = %f, w_top = %f, w_bot = %f, m = %f\n", hist_y_top, hist_y_bottom, hist_w_top, hist_w_bottom, hist_slope);
  //  exit(1);
}

//----------------------------------------------------------------------------

void draw_hist_segmenter(CR_Image *im)
{
  int x, y;
  double r, g, b;

  if (!im) {
  glColor3f(0,0,1);
  glLineWidth(1);
  
  glBegin(GL_LINE_LOOP);
  glVertex2f(hist_x - hist_w_top / 2, fplanx_h - hist_y_top);
  glVertex2f(hist_x + hist_w_top / 2, fplanx_h - hist_y_top);
  glVertex2f(hist_x + hist_w_bottom / 2, fplanx_h - hist_y_bottom);
  glVertex2f(hist_x - hist_w_bottom / 2, fplanx_h - hist_y_bottom);
  glEnd();
  }
  else {
    /*
    histogram_trapezoid_color_CR_Image(hist_x, hist_y_top, hist_y_bottom, hist_w_bottom, hist_slope,
				       r_histogram, g_histogram, b_histogram, 
				       //				     uv_histogram, 
				       0, 255,
				       im);
    */

    for (y = 0; y < im->h; y++)
      for (x = 0; x < im->w; x++) {

	// read in color value

	/*
	r = (double) ((unsigned char) IMXY_R(im, x, y));
	g = (double) ((unsigned char) IMXY_G(im, x, y));
	b = (double) ((unsigned char) IMXY_B(im, x, y));
	*/

	// figure out which bin each channel goes into

	// decide whether pixel is road or not

	// write result to another image/array

	IMXY_R(im, x, y) = (unsigned char) 255;
	IMXY_G(im, x, y) = (unsigned char) 255;
	IMXY_B(im, x, y) = (unsigned char) 0;
      }
  }

}

//----------------------------------------------------------------------------

// cross product v3 = v1 x v2

void VectCross(CR_Vector *v1, CR_Vector *v2, CR_Vector *v3)
{
  v3->x[0] = v1->x[1]*v2->x[2] - v1->x[2]*v2->x[1];
  v3->x[1] = v1->x[2]*v2->x[0] - v1->x[0]*v2->x[2];
  v3->x[2] = v1->x[0]*v2->x[1] - v1->x[1]*v2->x[0];

}

//----------------------------------------------------------------------------

double VectDotProd(CR_Vector *v1, CR_Vector *v2)
{
  return v1->x[0]*v2->x[0] + v1->x[1]*v2->x[1] + v1->x[2]*v2->x[2];
}

//----------------------------------------------------------------------------

// these are 2-D lines

void ransac_fish(CR_Vector *line, CR_Matrix *points, int left)
{
  double threshold, result;
  int i, j, pairs, p1, p2, support, max_support, xt, yt, xb, yb;

  threshold = 2.5;
  pairs = 20; 

  // iterate over recommended number of pairs of points
  // for each pair:

  max_support = 0;

  for (i = 0; i < pairs; i++) {
   
    // pick random pair (not the same one twice)

    p1 = ranged_uniform_int_CR_Random(0, points->rows - 1);
    do {
      p2 = ranged_uniform_int_CR_Random(0, points->rows - 1);
    } while (p2 == p1);

    // form line (cross product of homogeneous forms of points)

    fish_p1->x[0] = MATRC(points, p1, 0);
    fish_p1->x[1] = MATRC(points, p1, 1);

    fish_p2->x[0] = MATRC(points, p2, 0);
    fish_p2->x[1] = MATRC(points, p2, 1);

    VectCross(fish_p1, fish_p2, fish_line);

    // count inliers for this line (threshold on each dot product of point and line)

    for (j = 0, support = 0, yt = planx_h, yb = 0; j < points->rows; j++) {

      fish_p1->x[0] = MATRC(points, j, 0);
      fish_p1->x[1] = MATRC(points, j, 1);

      if (fabs(VectDotProd(fish_p1, fish_line)) < threshold) {
	support++;
	if (fish_p1->x[1] > yb) {
	  yb = fish_p1->x[1];
	  xb = fish_p1->x[0];
	}
	if (fish_p1->x[1] < yt) {
	  yt = fish_p1->x[1];
	  xt = fish_p1->x[0];
	}
      }
      
    }

    // keep track of which pair has most support

    if (support > max_support) {

      line->x[0] = fish_line->x[0];
      line->x[1] = fish_line->x[1];
      line->x[2] = fish_line->x[2];

      max_support = support;

      if (left) {
	fish_left_xt = xt;
	fish_left_yt = yt;
	fish_left_xb = xb;
	fish_left_yb = yb;
      }
      else {
	fish_right_xt = xt;
	fish_right_yt = yt;
	fish_right_xb = xb;
	fish_right_yb = yb;
      }

    }
  }

  // should fit line to all inliers of winning sample
}

//----------------------------------------------------------------------------

void update_segmenter(CR_Image *im)
{
  int y;

  // Fisher stuff

  // compute discriminants over image, put in matrix

  compute_discriminants(im);

  // maximize objective function

  fish_in_index = fish_out_index = 0;
  fish_right_edge_row = fish_left_edge_row = 0;

  for (y = fish_top; y < fish_left_bottom; y++)
    fit_left_step_edge(y, im);

  for (y = fish_top; y < fish_right_bottom; y++)
    fit_right_step_edge(y, im);

  //  printf("in = %i, out = %i\n", fish_in_index, fish_out_index);
  
  fish_in->rows = fish_in_index;
  fish_out->rows = fish_out_index;
  //seg_fisher(fish_in, fish_out, fish_disc);

  /*
// this doesn't seem to be working right now--always gives the same state
// values no matter what covariances I plug in

  CR_Matrix *Z;

  Z = make_CR_Matrix("Z", 3, 1);
  MATRC(Z, 0, 0) = fish_disc->x[0];
  MATRC(Z, 1, 0) = fish_disc->x[1];
  MATRC(Z, 2, 0) = fish_disc->x[2];
  
  update_CR_KalmanFilter(Z, fish_kal);
#ifdef DEBUG
  printf("%f %f %f\n", MATRC(fish_kal->x, 0, 0), MATRC(fish_kal->x, 1, 0), MATRC(fish_kal->x, 2, 0));
#endif
  */

  fish_left_edge_points->rows = fish_left_edge_row;
  fish_right_edge_points->rows = fish_right_edge_row;

  ransac_fish(fish_right_line, fish_right_edge_points, FALSE);
  ransac_fish(fish_left_line, fish_left_edge_points, TRUE);

  VectCross(fish_left_line, fish_right_line, fish_vp);
  fish_vp->x[0] /= fish_vp->x[2];
  fish_vp->x[1] /= fish_vp->x[2];

  //  update_seg_particlefilter();


  // update discriminant via Fisher's linear discriminant method
  // (use mean of particle filter as current best fit)


  // histogram stuff

  // histogram chrominance values u, v of assumed-road area

  /*
  histogram_trapezoid_color_CR_Image(hist_x, hist_y_top, hist_y_bottom, hist_w_bottom, hist_slope,
				     r_histogram, g_histogram, b_histogram, 
				     im);

  // classify entire image based on assumed-road color histogram

  hist_segment_image(im);
  */

  /*
  print_CR_Vector(r_histogram);
  print_CR_Vector(g_histogram);
  print_CR_Vector(b_histogram);
  printf("\n");
  */
}

//----------------------------------------------------------------------------

void hist_segment_image(CR_Image *im)
{
  double r, g, b, hist_interval, hist_range, r_prob, g_prob, b_prob;
  int x, y, i;

  hist_range = 255.0;
  hist_interval = hist_range / (double) r_histogram->rows;

  for (y = 0, i = 0; y < im->h; y++)
    for (x = 0; x < im->w; x++) {

      r = (double) ((unsigned char) IMXY_R(im, x, y));
      g = (double) ((unsigned char) IMXY_G(im, x, y));
      b = (double) ((unsigned char) IMXY_B(im, x, y));

      r_prob = r_histogram->x[(int) (r / hist_interval)];
      g_prob = g_histogram->x[(int) (g / hist_interval)];
      b_prob = b_histogram->x[(int) (b / hist_interval)];

      // decision rule

      hist_seg_class[i] = r_prob + g_prob + b_prob;
      /*
      if (r_prob * g_prob * b_prob >= hist_product_thresh)
      //      if (r_prob >= hist_and_thresh && g_prob >= hist_and_thresh && b_prob >= hist_and_thresh)
	hist_seg_class[i] = 1.0;
      else 
	hist_seg_class[i] = 0.0;
      */

      i++;
    }
}

//----------------------------------------------------------------------------

void update_seg_particlefilter()
{
  update_CR_XCondensation(NULL, seg_con);
}

//----------------------------------------------------------------------------

void draw_fish_segmenter()
{
  int y;
  
  glColor3ub(255, 255, 0);
  glPointSize(2);
  glBegin(GL_POINTS);
  
  for (y = fish_top; y < fish_left_bottom; y++) 
    if (seg_voter_disc_left_edge_col[y] != planx_w / 2 - fish_lateral_frame - 1 &&
	seg_voter_disc_left_edge_col[y] != fish_lateral_frame)
      glVertex2i(seg_voter_disc_left_edge_col[y], planx_h - y);

  for (y = fish_top; y < fish_right_bottom; y++)
    if (seg_voter_disc_right_edge_col[y] != planx_w - fish_lateral_frame - 1 &&
	seg_voter_disc_right_edge_col[y] != planx_w / 2 + fish_lateral_frame)
      glVertex2i(seg_voter_disc_right_edge_col[y], planx_h - y);

  glEnd();

  if (left_down) {
    glColor3ub(0, 255, 0);
    glLineWidth(2);
    glBegin(GL_LINES);
    
    glVertex2i(fish_left_xt, planx_h - fish_left_yt);
    glVertex2i(fish_left_xb, planx_h - fish_left_yb);
    
    glVertex2i(fish_right_xt, planx_h - fish_right_yt);
    glVertex2i(fish_right_xb, planx_h - fish_right_yb);
    
    glEnd();

    glColor3ub(0, 255, 0);
    glPointSize(5);
    glBegin(GL_POINTS);
    
    glVertex2f(fish_vp->x[0], planx_h - fish_vp->x[1]);
    
    glEnd();
  }
}

//----------------------------------------------------------------------------

//int big_i = 0;

void draw_seg_particlefilter()
{
  int i;
  double tan_theta_left, tan_theta_right, left_x, right_x, left_dx, right_dx, vp_x, vp_y, theta_left, theta_right, r_left, r_right;

  //  glClear(GL_COLOR_BUFFER_BIT);

  // samples

  /*
  glColor3ub(255, 255, 0);
  glLineWidth(1);
  glBegin(GL_LINES);

  for (i = 0; i < seg_con->n; i++) {

    theta_left = seg_con->s[i]->x[2] - seg_con->s[i]->x[3]/2;
    theta_right = seg_con->s[i]->x[2] + seg_con->s[i]->x[3]/2;

    if (seg_con->pi->x[i] && theta_left > 0 && theta_right < PI) {

      //      tan_theta_left = tan(seg_con->s[i]->x[2] - seg_con->s[i]->x[3]/2);   // = y / x
      //     tan_theta_right = tan(seg_con->s[i]->x[2] + seg_con->s[i]->x[3]/2);
      
      vp_x = seg_con->s[i]->x[0];
      vp_y =  planx_h - seg_con->s[i]->x[1];
      
      r_left = -vp_y / sin(theta_left);
      r_right = -vp_y / sin(theta_right);

      left_dx = r_left * cos(theta_left);
      right_dx = r_right * cos(theta_right);

      //      left_dx = vp_y / tan_theta_left;
      //      right_dx = vp_y / tan_theta_right;
      
      left_x = vp_x + left_dx;
      right_x = vp_x + right_dx;
      
      // vp to left point
      glVertex2f(vp_x, vp_y);
      glVertex2f(left_x, 0);
      
      // vp to right point
      glVertex2f(vp_x, vp_y);
      glVertex2f(right_x, 0);
    }
  }
  glEnd();
  */

  glColor3ub(0, 255, 0);
  glLineWidth(3);

  glBegin(GL_LINES);

  // state

  theta_left = seg_con->x->x[2] - seg_con->x->x[3]/2;
  theta_right = seg_con->x->x[2] + seg_con->x->x[3]/2;

  //  tan_theta_left = tan(seg_con->x->x[2] - seg_con->x->x[3]/2);   // = y / x
  //  tan_theta_right = tan(seg_con->x->x[2] + seg_con->x->x[3]/2);
  
  vp_x = seg_con->x->x[0];
  vp_y =  planx_h - seg_con->x->x[1];

  r_left = -vp_y / sin(theta_left);
  r_right = -vp_y / sin(theta_right);

  left_dx = r_left * cos(theta_left);
  right_dx = r_right * cos(theta_right);
  
  //  left_dx = vp_y / tan_theta_left;
  //  right_dx = vp_y / tan_theta_right;
  
  left_x = vp_x + left_dx;
  right_x = vp_x + right_dx;
  
  // vp to left point
  glVertex2f(vp_x, vp_y);
  glVertex2f(left_x, 0);
  
  // vp to right point
  glVertex2f(vp_x, vp_y);
  glVertex2f(right_x, 0);

  glEnd();

  //  big_i++;

  // just vp's

  /*
  glPointSize(1);
  glBegin(GL_POINTS);


  for (i = 0; i < seg_con->n; i++) 
    glVertex2f(seg_con->s[i]->x[0], planx_h - seg_con->s[i]->x[1]);

  glEnd();
  */

  // draw particles as polygons

  // draw weighted mean as different-colored polygon
}

//----------------------------------------------------------------------------

// current discriminant function is global -- assume it's just dR*R+dG*G+dB*B

void compute_discriminants(CR_Image *im)
{
  int x, y, r, c;
  float red, green, blue;

  for (y = seg_voter_top, r = 0; y < seg_voter_bottom; y += seg_voter_dy, r++)
    for (x = seg_voter_left, c = 0; x < seg_voter_right; x += seg_voter_dx, c++) {
      red = (float) ((unsigned char) IMXY_R(im, x, y)) / 255.0;
      green = (float) ((unsigned char) IMXY_G(im, x, y)) / 255.0;
      blue = (float) ((unsigned char) IMXY_B(im, x, y)) / 255.0;
      //      if (!(x % 100))
      //	printf("r = %f, g = %f, b = %f\n", red, green, blue);
      //      MATRC(seg_voter_disc, r, c) = disc_R * red + disc_G * green + disc_B * blue;
      //      seg_voter_disc[planx_w * y + x] = disc_R * red + disc_G * green + disc_B * blue;
      seg_voter_disc[planx_w * y + x] = 
	fish_disc->x[0] * red + 
	fish_disc->x[1] * green + 
	fish_disc->x[2] * blue;

      // threshold
      /*
      if (seg_voter_disc[planx_w * y + x] > 0.5)
	seg_voter_disc[planx_w * y + x] = 1.0;
      else
	seg_voter_disc[planx_w * y + x] = 0.0;
      */
    }
}

//----------------------------------------------------------------------------

// y = 0 is top of image

// uses seg_voter_disc

int fit_right_step_edge(int y, CR_Image *im)
{
  int e, x;
  float in_disc_total, out_disc_total, in_num, out_num, diff, max_diff, max_e, in_disc_mean, out_disc_mean;

  for (max_e = e = planx_w / 2 + fish_lateral_frame, max_diff = 0; e < planx_w - fish_lateral_frame; e++) {

    in_disc_total = out_disc_total = in_num = out_num = 0;

    for (x = planx_w / 2; x < planx_w; x++) {

      if (x <= e) {
	  in_disc_total += seg_voter_disc[planx_w * y + x];
	  in_num++;
	}
	else {
	  out_disc_total += seg_voter_disc[planx_w * y + x];
	  out_num++;
	}
    }

    in_disc_mean = in_disc_total / in_num;
    out_disc_mean = out_disc_total / out_num;

    diff = fabs(in_disc_mean - out_disc_mean);
    seg_voter_disc_edge[planx_w * y + e] = diff;
 
    if (diff > max_diff) {
      max_diff = diff;
      max_e = e;
    }
  }
    
  seg_voter_disc_right_edge_col[y] = max_e;

  if (max_e != planx_w - fish_lateral_frame - 1 && max_e != planx_w / 2 + fish_lateral_frame) {

    MATRC(fish_right_edge_points, fish_right_edge_row, 0) = max_e;
    MATRC(fish_right_edge_points, fish_right_edge_row, 1) = y;
    fish_right_edge_row++;

    for (x = planx_w / 2; x <= max_e; x++) {
      MATRC(fish_in, fish_in_index, 0) = (float) ((unsigned char) IMXY_R(im, x, y)) / 255.0;
      MATRC(fish_in, fish_in_index, 1) = (float) ((unsigned char) IMXY_G(im, x, y)) / 255.0;
      MATRC(fish_in, fish_in_index, 2) = (float) ((unsigned char) IMXY_B(im, x, y)) / 255.0;
      fish_in_index++;
    }
    for (x = max_e + 1; x < planx_w; x++) {
      MATRC(fish_out, fish_out_index, 0) = (float) ((unsigned char) IMXY_R(im, x, y)) / 255.0;
      MATRC(fish_out, fish_out_index, 1) = (float) ((unsigned char) IMXY_G(im, x, y)) / 255.0;
      MATRC(fish_out, fish_out_index, 2) = (float) ((unsigned char) IMXY_B(im, x, y)) / 255.0;
      fish_out_index++;
    }
  }

  return max_e;
}

//----------------------------------------------------------------------------

// y = 0 is top of image

// uses seg_voter_disc

int fit_left_step_edge(int y, CR_Image *im)
{
  int e, x;
  float in_disc_total, out_disc_total, in_num, out_num, diff, max_diff, max_e, in_disc_mean, out_disc_mean;

  for (max_e = e = fish_lateral_frame, max_diff = 0; e < planx_w / 2 - fish_lateral_frame; e++) {

    in_disc_total = out_disc_total = in_num = out_num = 0;

    for (x = 0; x < planx_w / 2; x++) {

      if (x < e) {
	  out_disc_total += seg_voter_disc[planx_w * y + x];
	  out_num++;
	}
	else {
	  in_disc_total += seg_voter_disc[planx_w * y + x];
	  in_num++;
	}
    }

    in_disc_mean = in_disc_total / in_num;
    out_disc_mean = out_disc_total / out_num;

    diff = fabs(in_disc_mean - out_disc_mean);
    seg_voter_disc_edge[planx_w * y + e] = diff;
 
    if (diff > max_diff) {
      max_diff = diff;
      max_e = e;
    }
  }
    
  seg_voter_disc_left_edge_col[y] = max_e;

  if (max_e != planx_w / 2 - fish_lateral_frame - 1 && max_e != fish_lateral_frame) {

    MATRC(fish_left_edge_points, fish_left_edge_row, 0) = max_e;
    MATRC(fish_left_edge_points, fish_left_edge_row, 1) = y;
    fish_left_edge_row++;

    for (x = 0; x < max_e; x++) {
      MATRC(fish_out, fish_out_index, 0) = (float) ((unsigned char) IMXY_R(im, x, y)) / 255.0;
      MATRC(fish_out, fish_out_index, 1) = (float) ((unsigned char) IMXY_G(im, x, y)) / 255.0;
      MATRC(fish_out, fish_out_index, 2) = (float) ((unsigned char) IMXY_B(im, x, y)) / 255.0;
      fish_out_index++;
    }
    for (x = max_e; x < planx_w / 2; x++) {
      MATRC(fish_in, fish_in_index, 0) = (float) ((unsigned char) IMXY_R(im, x, y)) / 255.0;
      MATRC(fish_in, fish_in_index, 1) = (float) ((unsigned char) IMXY_G(im, x, y)) / 255.0;
      MATRC(fish_in, fish_in_index, 2) = (float) ((unsigned char) IMXY_B(im, x, y)) / 255.0;
      fish_in_index++;
    }
  }

  return max_e;
}

//----------------------------------------------------------------------------

void initialize_seg_particlefilter()
{
  int num_samps;

  // initialize particle filter

  num_samps = 500;  // 500
  //  seg_cov = make_diagonal_all_CR_Matrix("seg_cov", 4, 10.0);
  //seg_cov = make_diagonal_all_CR_Matrix("seg_cov", 4, 0.001);

  // var = sigma^2, 95% within 2*sigma
  // horizontal: 2*sigma = planx_w / 40 => sigma = planx_w / 80 

  seg_cov = make_all_CR_Matrix("seg_cov", 4, 4, 0.0);
  MATRC(seg_cov, 0, 0) = SQUARE(planx_w / 80);
  MATRC(seg_cov, 1, 1) = SQUARE(planx_w / 114);
  MATRC(seg_cov, 2, 2) = 0.02;
  MATRC(seg_cov, 3, 3) = 0.02;

  seg_con = make_CR_XCondensation(num_samps, make_CR_Vector("seg_state", 4), seg_cov,
				  seg_condprob_zx, seg_samp_prior, seg_samp_state, seg_dyn_state);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// where the particles start

void seg_samp_prior(CR_Vector *samp)
{
  sample_gaussian_CR_Matrix(seg_prior_covalpha, samp, seg_prior_mean, seg_prior_covsqrt);
  //samp->x[2] = PI / 2;
  //  samp->x[3] = PI / 2;

  return;

  double x, y, delta;

  delta = 5;

  // put in uniform distribution over image

  if (seg_prior_x >= planx_w) {
    seg_prior_x = delta/2;
    seg_prior_y += delta;
  }

  samp->x[0] = seg_prior_x;
  samp->x[1] = seg_prior_y;

  seg_prior_x += delta;


}

//----------------------------------------------------------------------------

// random diffusion

void seg_samp_state(CR_Vector *old_samp, CR_Vector *new_samp)
{
  // nuthin' fancy...

  sample_gaussian_CR_Matrix(seg_con->Covalpha, new_samp, old_samp, seg_con->Covsqrt);
  //  new_samp->x[2] = PI / 2;
  //  new_samp->x[3] = PI / 2;

}

//----------------------------------------------------------------------------

// deterministic drift

void seg_dyn_state(CR_Vector *old_samp, CR_Vector *new_samp)
{
  // identity transform for now

  copy_CR_Vector(old_samp, new_samp);
}

//----------------------------------------------------------------------------

// samp has road region geometric parameters embedded
// samp->x[0] = vp x
// samp->x[1] = vp y
// samp->x[2] = theta center -- measured counterclockwise from pointing left along horizon
// samp->x[3] = theta width

int legal_roadregion(CR_Vector *samp)
{
  double theta_left, theta_right;

  // vp in a reasonable place

  //  print_CR_Vector(samp);

  //  printf("vp x %lf [%lf, %lf]\n", samp->x[0], planx_candidate_left, planx_candidate_right);
  //  if (samp->x[0] > planx_candidate_right || samp->x[0] < planx_candidate_left)
  if (samp->x[0] < 0.2 * fplanx_w || samp->x[0] > 0.8 * fplanx_w) {
    //   printf("lateral oob\n");
    return FALSE;
  }

  //  printf("vp y %lf [%lf, %lf]\n", samp->x[1], planx_candidate_top, planx_candidate_bottom);
  //  if (samp->x[1] < planx_candidate_top || samp->x[1] > planx_candidate_bottom)
  if (samp->x[1] < 0.1 * fplanx_h || samp->x[1] > 0.6 * fplanx_h) {
    //printf("vertical oob\n");
    return FALSE;
  }

  //  return TRUE;

  // minimum width 45 degs., max width 120 degs.

  //  printf("min width %lf [%lf]\n", samp->x[3], PI/4);
  if (samp->x[3] < PI/4 || samp->x[3] > 2 * PI / 3) {
    //printf("width oob\n");
    return FALSE;
  }

  // left and right edges no closer than 18 degs to horizon

  theta_left = samp->x[2] - samp->x[3]/2;
  theta_right = samp->x[2] + samp->x[3]/2;

  //  printf("abs left %lf, right %lf [%lf, %lf]\n", theta_left, theta_right, PI/6, 5*PI/6);
  if (theta_left < 0.1 * PI || theta_right > 0.9 * PI) {
    //printf("edge(s) oob\n");
    return FALSE;
  }

  return TRUE;
}

//----------------------------------------------------------------------------

int below_horizon(double x, double y, CR_Vector *samp)
{
  return (y >= samp->x[1]);
}


//----------------------------------------------------------------------------

int inside_roadregion(double x, double y, CR_Vector *samp)
{
  double dx, dy, theta, theta_left, theta_right;
  int result;

  // already know (x, y) is in image and below horizon, so don't check for that
  
  // origin (0, 0) is upper-left corner of image; theta is measured counterclockwise from (-1, 0) direction

  // convert (x, y) to polar coordinates where vp is origin, just check that it's between
  // left and right edges

  theta = PI - atan2(y - samp->x[1], x - samp->x[0]);

  dx = x - samp->x[0];
  dy = y - samp->x[1];

  theta_left = samp->x[2] - samp->x[3]/2;
  theta_right = samp->x[2] + samp->x[3]/2;

  result = (theta >= theta_left && theta <= theta_right);

  //  printf("%i: dx = %lf, dy = %lf, theta = %lf (left = %lf, right = %lf)\n", result, dx, dy, RAD2DEG(theta), RAD2DEG(theta_left), RAD2DEG(theta_right));

  return result;
}

//----------------------------------------------------------------------------

// objective function on hypothetical road region

double seg_condprob_zx(void *Z, CR_Vector *samp)
{
  int x, y, r, c, i;
  double in_disc_total, out_disc_total, in_num, out_num, in_disc_mean, out_disc_mean;

  // return 0 if road region geometry doesn't meet constraints

  if (!legal_roadregion(samp)) {
//     printf("bad: ");
//     print_CR_Vector(samp);

/*
    for (y = seg_voter_top, r = 0; y < seg_voter_bottom; y += seg_voter_dy, r++)
      for (x = seg_voter_left, c = 0; x < seg_voter_right; x += seg_voter_dx, c++) {
	if (below_horizon(x, y, samp))     // don't count pixels above horizon
	  seg_voter_disc[planx_w * y + x] = 0.0;
	else 
	  seg_voter_disc[planx_w * y + x] = 1.0;
    }
 */
  
    return 0;
  }
  
  //  return ranged_uniform_CR_Random(0.0, 1.0);

  /*
  printf("good: ");
  print_CR_Vector(samp);
  */

  // get mean value of discriminant inside road region
  // get mean value of discriminant outside road region
  // return difference

  in_disc_total = out_disc_total = in_num = out_num = 0;

  for (y = seg_voter_top, r = 0; y < seg_voter_bottom; y += seg_voter_dy, r++)
    for (x = seg_voter_left, c = 0; x < seg_voter_right; x += seg_voter_dx, c++) {
      if (below_horizon(x, y, samp)) {    // don't count pixels above horizon
	if (inside_roadregion(x, y, samp)) {
	  in_disc_total += seg_voter_disc[planx_w * y + x];
	  //	  in_disc_total += MATRC(seg_voter_disc, r, c);
	  in_num++;
	}
	else {
	  out_disc_total += seg_voter_disc[planx_w * y + x];
	  //	  out_disc_total += MATRC(seg_voter_disc, r, c);
	  out_num++;
	}
      }
    }

  in_disc_mean = in_disc_total / in_num;
  out_disc_mean = out_disc_total / out_num;

  //  return fabs(in_disc_mean - out_disc_mean);
  return MAX2((in_disc_mean - out_disc_mean), 0);


  /*
  for (y = seg_voter_top, r = 0; y < seg_voter_bottom; y += seg_voter_dy, r++)
    for (x = seg_voter_left, c = 0; x < seg_voter_right; x += seg_voter_dx, c++) {
      if (below_horizon(x, y, samp)) {    // don't count pixels above horizon
	if (inside_roadregion(x, y, samp)) 
	  seg_voter_disc[planx_w * y + x] = 1.0;
	else 
	  seg_voter_disc[planx_w * y + x] = 0.0;
      }
      else 
	seg_voter_disc[planx_w * y + x] = 0.5;
    }
  */

  //  return 0;
}

//----------------------------------------------------------------------------

// fisher's linear discriminant on two classes, projection to 1-D

// each row is a data point
// disc is set of weights, one for each dim of a data point

// function S = scatter(x);
// % SCATTER : scatter matrix for samples x
// % S = scatter(x)
// %	x - data
// %	S - scatter matrix
// 
// % Copyright (c) 1995 Frank Dellaert
// % All rights Reserved
// 
// % get dimensions of data
// [d,n] = size(x);
// 
// % within-class scatter matrix is proportional to covariance matrix
// S = cov(x')*(n-1);

//----------------------------------------------------------------------------

// roadclass, roadclass_t, road_mean, roadclass_scatter;
// same for off
// scatter_within, inv_scatter_within
// mean_diff

CR_Matrix *fish_in_T, *fish_in_scatter;
CR_Matrix *fish_out_T, *fish_out_scatter;
CR_Matrix *fish_scatter_within, *fish_inv_scatter_within;
CR_Vector *fish_in_mean, *fish_out_mean, *fish_mean_diff;
 
void init_seg_fisher(CR_Matrix *in, CR_Matrix *out, CR_Vector *fish_disc)
{
  fish_in_T = make_CR_Matrix("fish_in_T", in->cols, in->rows);
  fish_in_scatter = make_CR_Matrix("fish_in_scatter", in->cols, in->cols);

  fish_out_T = make_CR_Matrix("fish_out_T", out->cols, out->rows);
  fish_out_scatter = make_CR_Matrix("fish_in_scatter", in->cols, in->cols);

  fish_scatter_within = make_CR_Matrix("fish_scatter_within", in->cols, in->cols);
  fish_inv_scatter_within = make_CR_Matrix("fish_inv_scatter_within", in->cols, in->cols);

  fish_in_mean = make_CR_Vector("fish_in_mean", in->cols);
  fish_out_mean = make_CR_Vector("fish_out_mean", out->cols);
  fish_mean_diff = make_CR_Vector("fish_mean_diff", in->cols);
}

void seg_fisher(CR_Matrix *in, CR_Matrix *out, CR_Vector *fish_disc)
{
  double len;

  // in mean & fish_scatter

  fish_in_T->cols = in->rows;
  mean_cov_CR_Matrix(in, fish_in_T, fish_in_mean, fish_in_scatter);
  scale_CR_Matrix(in->rows - 1, fish_in_scatter);

  //  print_CR_Vector(fish_in_mean);

  // out mean & fish_scatter matrix

  mean_cov_CR_Matrix(out, fish_out_T, fish_out_mean, fish_out_scatter);
  scale_CR_Matrix(out->rows - 1, fish_out_scatter);

  //  print_CR_Vector(fish_out_mean);

  // inverted within-class fish_scatter

  sum_CR_Matrix(fish_in_scatter, fish_out_scatter, fish_scatter_within);
  inverse_CR_Matrix(fish_scatter_within, fish_inv_scatter_within);


  // mean difference

  difference_CR_Vector(fish_in_mean, fish_out_mean, fish_mean_diff);

  //  print_CR_Vector(fish_mean_diff);

  // new discriminant

  //  product_CR_Matrix(fish_inv_scatter_within, fish_mean_diff, fish_disc);
  matrix_vector_product_CR_Matrix(fish_inv_scatter_within, fish_mean_diff, fish_disc);
  len = sqrt(SQUARE(fish_disc->x[0]) + SQUARE(fish_disc->x[1]) + SQUARE(fish_disc->x[2]));
  fish_disc->x[0] /= len;
  fish_disc->x[1] /= len;
  fish_disc->x[2] /= len;

#ifdef DEBUG
  print_CR_Vector(fish_disc);
#endif

  //  printf("\n");
  
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
