//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_Source_CG.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void PlanX_grab_CR_CG(CR_Image *);
void PlanX_init_per_image(int);
void PlanX_init_per_run();
void PlanX_initialize_gabor_filters(int);
void Planx_initialize_multiscale_PCA_orientation();
void who_voted_for_the_VP_PlanX_grab_CR_CG(CR_Image *);
void where_is_the_VP_PlanX_grab_CR_CG(CR_Image *);
void pf_where_is_the_VP_PlanX_grab_CR_CG(CR_Image *);
void strip_pf_where_is_the_VP_PlanX_grab_CR_CG(CR_Image *);
void movie_strip_pf_where_is_the_VP_PlanX_grab_CR_CG(CR_Image *);
void pf_tracking_movie_PlanX_grab_CR_CG(CR_Image *);
void strip_pf_tracking_movie_PlanX_grab_CR_CG(CR_Image *);
void stripwrite_where_is_the_VP_PlanX_grab_CR_CG(CR_Image *);
double pf_PlanX_vote(double, double, int, int, int);
void old_PlanX_grab_CR_CG(CR_Image *);

extern int planx_x, planx_y;
int cursor_flag;

//#define BADROAD_REINIT

// test 1

GLUquadricObj *glcircle;

// OrderParamTask1, OrderParamTask3

CR_Vector *target_prob, *background_prob;

CR_Image *grf_im, *scratch_grf_im;

CR_Random_Field *grf_cumprob;
CR_Random_Field *grf_mean;
CR_Random_Field *grf_covsqrt;
CR_Random_Field *grf_val;
CR_Matrix *grf_alpha;

CR_Random_Field *grf_target_cumprob;
CR_Random_Field *grf_target_mean;
CR_Random_Field *grf_target_covsqrt;
CR_Random_Field *grf_target_val;
CR_Matrix *grf_target_alpha;

double task3_shift;
double task3_scale;

int no_pause = TRUE;
//int no_pause = FALSE;
int pause_time = 2.0;  // in seconds, i believe

double total_time = 0;
double time_now = 0;
int background_visible = FALSE;

int num_tiles, sqrt_num_tiles, tile_w, tile_h, horiz_num_tiles, vert_num_tiles;
int target_tile_x, target_tile_y;   // just for OrderParamTask3

int max_trials = 500000;
int num_trials = 0;
int num_matching_distractors = 0;
int num_errors = 0;

// Road1

double road_fovy = 30;

double horizon_dist;              // all units are Earth radii

double disk_rad;
double disk_dist;

double earth_radius = 1;          // distance of surface from center of planet 
                                  // units are Earth radii

double eye_radius;                // distance of eye from center of planet
                                  // units are Earth radii

double eye_altitude;              // 0 = tangential to surface, PI_2 = straight up, etc. (radians)
double eye_roll;                  // 0 = right-side up, PI_2 = left-side up, etc.

double eye_latitude;              // location on planet in spherical coordinates (radians)
double eye_longitude;
double eye_azimuth;               // local heading (radians)

#define M2R(x)      ((x) / 6370000.0)  // meters to earth radii

double road_t;   // parametric "distance along road" variable so we can generate geometry functionally
int road_b;
double road_r;
double road_r_delta;

// FisherInfo1

double fisher_mean_squared_error;
double fisher_mean_error;
int fisher_trials;

// Track1

int track_target_x, track_target_y;
CR_Vector *track_target_mean, *new_mean;
CR_Matrix *track_target_cov, *track_target_covsqrt;
double track_target_alpha;
CR_Matrix *track_target_Z;
CR_Matrix **track_target_Z_array;
CR_Kalman *track_target_Kal;

// Track2

double center_a, center_b, center_c, center_width, center_k;
double left_a, left_b, left_c, right_a, right_b, right_c;
double *left_x, *left_y, *right_x, *right_y, *center_x, *center_y;
double track2_updates;
int horizon_y, below_horizon_scans;

// Track3

CR_Movie *roadmov;
CR_Image *roadim;
int track3_startrow, track3_endrow, track3_interval, track3_scans;
CR_Image *edge_im;
int track3_num_work_im;
CR_Image **track3_work_im;
int track3_edge_window_radius;
int track3_thresh_edge_strength;
int track3_left_margin, track3_right_margin;
CR_Matrix *track3_IW1, *track3_LW21, *track3_LW32;
CR_Vector *track3_B1, *track3_B2, *track3_B3;
CR_Vector *track3_O1, *track3_O2, *track3_O3, *track3_I2, *track3_I3, *track3_I4;
CR_Vector *track3_Input;
int track3_tracking_started;
CR_Vector *track3_demo3_vars;
int track3_x, track3_y;
CR_Matrix *track3_mstate;
CR_Matrix *track3_statecov, *track3_statecovsqrt;
CR_Vector *track3_state, *track3_candidate_state;
double track3_alpha;

CR_Image *minfunc_im;
int minfunc_calls;
CR_Vector *Track3_minfunc_resolution;
int track3_nn_x_interval, track3_nn_y_interval;
int track3_nn_rows, track3_nn_cols;
CR_Matrix *track3_nn, *track3_nn_cc;
CR_Vector *left_nn_x, *left_nn_y, *right_nn_x, *right_nn_y;
CR_Vector *left_goodrow, *right_goodrow;
CR_Matrix *track_target_Z_nn;
CR_Matrix **track_target_Z_nn_array;
CR_Kalman *track_target_Kal_nn;
CR_Matrix *nn_quad_fit;
CR_Movie *track3_mov;

// clothoid

double clothoid_scale, clothoid_scale_delta;

// 3D quadratic

int track_3d_reinit_counter;
int track_3d_reinit_flag = FALSE;
CR_Vector *track_3d_quadratic_vars;
double *t3d;
CR_Kalman *track_3d_quadratic_kal, *initial_track_3d_quadratic_kal;
extern CR_FuncMatrix *initial_X0;
CR_FuncMatrix *current_X0;
CR_Matrix *track_3d_quadratic_Z;
CR_Matrix **track_3d_quadratic_Z_array;

CR_Kalman *track_3d_quadratic_kal_nn;
CR_Matrix *track_3d_quadratic_Z_nn;
CR_Matrix **track_3d_quadratic_Z_nn_array;

int track_3d_quadratic_edge_window_radius;
int track_3d_quadratic_thresh_edge_strength;
int track_3d_left_margin, track_3d_right_margin;
int track_3d_top_margin, track_3d_bottom_margin;
CR_Vector *track_3d_quadratic_good_Z;
int track_3d_quadratic_num_good_Z;
double track_3d_window_radius;
int track_3d_depth_dependent_search;

CR_Matrix *track_3d_nn, *track_3d_nn_cc;
int track_3d_nn_x_interval, track_3d_nn_y_interval;
int track_3d_nn_rows, track_3d_nn_cols;
CR_Vector *track_3d_input;
CR_Vector *left_3d_nn_x, *left_3d_nn_z, *right_3d_nn_x, *right_3d_nn_z, *left_3d_nn_z_ground, *right_3d_nn_z_ground;
CR_Vector *center_3d_nn_params, *left_3d_nn_params, *right_3d_nn_params;
int track_3d_frame;
static char track_3d_framestring[80];
CR_Movie *track_3d_mov;
int track_3d_switch_frame;
int good_road, filtered_good_road, good_road_check_interval, good_road_initialized;
FILE *good_road_fp;
CR_SVM *good_road_svm;
CR_Kalman *good_road_kal;
CR_Matrix *good_road_Z;
CR_Matrix **good_road_Z_array;
CR_Vector *good_road_svm_input;
int track_3d_first_frame;

CR_SVM *road_nonroad_svm;
CR_Vector *road_nonroad_svm_input;
CR_Matrix *track_3d_svm;

CR_Kalman *plane_from_laser_kal;

double *road3d_xc, *road3d_yc, *road3d_xl, *road3d_yl, *road3d_xr, *road3d_yr;
int num_road3d_pts;
double road3d_a, road3d_b, road3d_c, road3d_delta, road3d_t;
//double zz[20] = { 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.66, 9.33, 10, 11, 12, 13.33, 14.66, 16, 18, 20, 23, 27, 32 };
double zz[20] = { 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.66, 9.33, 10, 11, 12, 13.33, 14.66, 16, 18, 20, 23, 27, 32 };

// laser-camera calibration

CR_Movie *calibmov;
CR_Image *calibim, *meancalibim, *diffcalibim, *threshcalibim, *flippedthreshcalibim, *morphcalibim, *morph2calibim;
int calib_frame, grab_calls, drape_iters;
CR_Vector ***calib_mean;
CR_Matrix *threshcalibmat, *cccalibmat;
CR_Vector *drape, *drape_velocity, *drape_force, *blurred_drape, *super_blurred_drape;
int first_people_frame;
FILE *drape_file;
int last_init_frame;
CR_Image *zoomcalibim;
int zoom_factor;

// texture flow 

CR_Image *texflowim, *flippedtexflowim;
CR_Condensation *texflowcon;

// calibration

extern int accept_flag, accept_correspondences;

// ITG

int itg_display = TRUE;
int itg_nml = TRUE;
int itg_do_live = FALSE;
int itg_capture_interval = 0;
int itg_frame;

// CVPR 2003

CR_SVM *road_svm;
CR_Vector *road_svm_input;
CR_Matrix *road_svm_output;

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void demo3_colorlaser_map(char *map_filename, char *laser_movie_name, char *camera_movie_name, char *log_filename, int map_interval)
{
  int x, y, i, j, k, index, xc, yc;
  FILE *mapfp, *logfp;
  double tilt, xw, yw, zw;
  Demo3_NavData *navdat;
  CR_Movie *laser_movie, *camera_movie;
  int log_filesize, bytes_per_camera_frame, num_camera_frames, start_camera_flag, time_seconds;
  int time_microseconds, total_frames_written, total_incomplete_frames, total_seo_facets_written;
  double start_laser_time, start_camera_time, this_time, laser_timestamp, laser_timestamp_last, camera_timestamp, xl, yl, zl;
;
  CR_Vector *camframetimes;

  // random things to initialize

  demo3_initialize_gall();
  //  init_laser2camera(360, 240);

  // read log file

  logfp = fopen(log_filename, "rb");

  bytes_per_camera_frame = 20;
  fseek(logfp, 0, SEEK_END);
  log_filesize = ftell(logfp);
  num_camera_frames = log_filesize / bytes_per_camera_frame;

  start_camera_flag = FALSE;
  camframetimes = make_CR_Vector("camframetimes", num_camera_frames);

  for (i = 0; i < num_camera_frames; i++) {

    fseek(logfp, i * bytes_per_camera_frame, SEEK_SET);
    fread(&time_seconds, 4, 1, logfp);
    fread(&time_microseconds, 4, 1, logfp); 
    fread(&total_frames_written, 4, 1, logfp);
    fread(&total_incomplete_frames, 4, 1, logfp);
    
    this_time = time_seconds + 0.000001 * time_microseconds;
    
    if (!start_camera_flag) {
      start_camera_time = this_time;
      start_camera_flag = TRUE;
    }

    camera_timestamp = this_time - start_camera_time;
    camframetimes->x[i] = camera_timestamp;
  }

  // open laser movie to first frame

  laser_movie = read_demo3_laser_to_CR_Movie(laser_movie_name);
  laser_movie->currentframe = 0;

  start_laser_time = laser_movie->laserim[0].navdat[0].time;

  // open camera movie to first frame, which we assume is in sync with first laser frame

  camera_movie = open_CR_Movie(camera_movie_name);
  camera_movie->currentframe = 0;
  //  get_frame_CR_Movie(camera_movie, camera_movie->currentframe);

  init_laser2camera(camera_movie->im->w, camera_movie->im->h);

  // open map file and start writing...

  mapfp = fopen(map_filename, "w");
  
  do {

    if (!(laser_movie->currentframe % map_interval)) {

      // get next laser frame and associated nav info

      get_frame_CR_Movie(laser_movie, laser_movie->currentframe);

      navdat = &laser_movie->laserim[laser_movie->currentframe].navdat[0];
      tilt = laser_movie->laserim[laser_movie->currentframe].frameHeaders[0].tiltAngle;

      laser_timestamp = 0.001 * (laser_movie->laserim[laser_movie->currentframe].navdat[0].time - start_laser_time);

      // get synched camera frame
      
      for ( ; camera_movie->currentframe < camera_movie->lastframe && 
	      camframetimes->x[camera_movie->currentframe] < laser_timestamp; 
	    camera_movie->currentframe++);
      if (camera_movie->currentframe > 0 &&
	  fabs(camframetimes->x[camera_movie->currentframe] - laser_timestamp) > 
	  fabs(camframetimes->x[camera_movie->currentframe - 1] - laser_timestamp))
	camera_movie->currentframe--;

      printf("l = %i, c = %i\n", laser_movie->currentframe, camera_movie->currentframe);

      get_frame_CR_Movie(camera_movie, camera_movie->currentframe);

      // color laser pixels and map them

      for (y = 0, index = 0; y < D3_LASER_HEIGHT; y++)
	for (x = 0; x < D3_LASER_WIDTH; x++) {

	  // filter out sky, bad facets

	  if (laser_movie->laserim[laser_movie->currentframe].data[index] < 1000 &&
	      laser_movie->laserim[laser_movie->currentframe].data[index] > 0) {	    
	
	    // project to image

	    laser2camera(x, y, laser_movie->laserim[laser_movie->currentframe].data[index], 
			 &xc, &yc, &xl, &yl, &zl, tilt);

	    // project to world

	    laser2world(x, y, laser_movie->laserim[laser_movie->currentframe].data[index], 
			&xw, &yw, &zw,
			tilt, 
			navdat->rot.s, navdat->rot.x, navdat->rot.y, navdat->rot.z,
			navdat->tranRel.x, navdat->tranRel.y, navdat->tranRel.z);
	    
	    // in image -> write with color

	    if (IN_IMAGE_MARG(camera_movie->im, xc, yc, 5, 0))
	      fprintf(mapfp, "%i,1,%lf,%lf,%lf,%i,%i,%i\n", laser_movie->currentframe, xw, yw, zw,
		      (unsigned char) IMXY_R(camera_movie->im, xc, yc),
		      (unsigned char) IMXY_G(camera_movie->im, xc, yc),
		      (unsigned char) IMXY_B(camera_movie->im, xc, yc));

	    // not in image -> write without color 
	
	    else
	      fprintf(mapfp, "%i,0,%lf,%lf,%lf,128,128,128\n", laser_movie->currentframe, xw, yw, zw);
	  }
	  index++;
	}
    }

    laser_movie->currentframe++;
    
  } while (laser_movie->currentframe < laser_movie->lastframe);
  
  // ...done
  
  fclose(mapfp);
}

//----------------------------------------------------------------------------

void demo3_laser_map(char *map_filename, char *laser_movie_name, int map_interval, int last_map_frame)
{
  int x, y, i, j, k, index;
  FILE *mapfp;
  double tilt, xw, yw, zw;
  Demo3_NavData *navdat;
  CR_Movie *laser_movie;

  // random things to initialize

  demo3_initialize_gall();
  init_laser2camera(360, 240);

  // open laser movie to first frame

  laser_movie = read_demo3_laser_to_CR_Movie(laser_movie_name);
  laser_movie->currentframe = 0;

  if (last_map_frame > laser_movie->lastframe)
    CR_error("demo3_laser_map(): last map frame out of range");

  // open map file and start writing...

  mapfp = fopen(map_filename, "w");
  
  do {

    if (!(laser_movie->currentframe % map_interval)) {

      printf("%i\n", laser_movie->currentframe);

      get_frame_CR_Movie(laser_movie, laser_movie->currentframe);

      navdat = &laser_movie->laserim[laser_movie->currentframe].navdat[0];
      tilt = laser_movie->laserim[laser_movie->currentframe].frameHeaders[0].tiltAngle;
      
      for (y = 0, index = 0; y < D3_LASER_HEIGHT; y++)
	for (x = 0; x < D3_LASER_WIDTH; x++) {
	
	  laser2world(x, y, laser_movie->laserim[laser_movie->currentframe].data[index], 
		      &xw, &yw, &zw,
		      tilt, 
		      navdat->rot.s, navdat->rot.x, navdat->rot.y, navdat->rot.z,
		      navdat->tranRel.x, navdat->tranRel.y, navdat->tranRel.z);
		
	  // write after filtering out sky, bad facets
	
	  if (laser_movie->laserim[laser_movie->currentframe].data[index] < 1000 &&
	      laser_movie->laserim[laser_movie->currentframe].data[index] > 0) 	    
	    fprintf(mapfp, "%i,%lf,%lf,%lf\n", laser_movie->currentframe, xw, yw, zw);
	
	  index++;
	}
    }

      //    get_frame_CR_Movie(laser_movie, laser_movie->currentframe++);
    laser_movie->currentframe++;
    
    //  } while (laser_movie->currentframe < laser_movie->lastframe);
  } while (laser_movie->currentframe < last_map_frame);
  
  // ...done
  
  fclose(mapfp);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Image *CVPR_2003_init_CR_CG(char *name)
{
  int num_random_frames = 60;

  char pick_read_directory[128] =                           // where Pick looks for the .avi, fw, and .log files from
    "//D/Documents/software/cvpr2003_data/";                // (must be in this format for shell "ls" command to work)

  char pick_write_filename[128] =                           // where Pick writes the frame IDs it has randomly chosen
    "D:\\Documents\\software\\cvpr2003_data\\pick.txt";  


  NIST_Road_pick_random_frames(num_random_frames, pick_read_directory, pick_write_filename);
  exit(1);


  return NULL;
}

//----------------------------------------------------------------------------

void CVPR_2003_grab_CR_CG(CR_Image *im)
{
}

//----------------------------------------------------------------------------

void CVPR_2003_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Image *test1_init_CR_CG(char *name)
{
  CR_Image *im;
  
  im = make_CR_Image(320, 240, 0);
  sprintf(name, "test1");

  glcircle = gluNewQuadric();
  //  gluQuadricDrawStyle(glcircle, GLU_LINE);
  gluQuadricDrawStyle(glcircle, GLU_FILL);

  return im;
}

//----------------------------------------------------------------------------

void test1_grab_CR_CG(CR_Image *im)
{
  double viewwidth, viewheight;
  double ellipse_vertical_radius, ellipse_horizontal_radius;
  int distractors, i, j;

  viewwidth = (double) im->w;
  viewheight = (double) im->h;

  ellipse_vertical_radius = 20;  
  ellipse_horizontal_radius = 20;

  //  glPushMatrix();

  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT);
  
  distractors = 10;

  for (i = 0; i < distractors; i++) {

//   for (j = 0; j < 1; j++)
//     for (i = 0; i < 1; i++) {

    glPushMatrix();
    
    glColor3f(1, 1, 1);
    
    //    glTranslatef(32 + i * 64, 24 + j * 48, 0);
    glTranslatef(ranged_uniform_int_CR_Random(0, im->w - 1), 
    		 ranged_uniform_int_CR_Random(0, im->h - 1), 
		 //    glTranslatef(ranged_uniform_int_CR_Random(0, viewwidth - 1), 
		 // ranged_uniform_int_CR_Random(0, viewheight - 1), 
     		 0.0);
  
    glScalef(ellipse_horizontal_radius, ellipse_vertical_radius, 1);
    
    gluDisk(glcircle, 0, 1, 50, 4);
    
    glPopMatrix();
  }

  //  glPopMatrix();

  //  glFinish();
  //  glutSwapBuffers();
}

//----------------------------------------------------------------------------

void test1_end_CR_CG(CR_Image *im)
{

}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Image *FisherInfo1_init_CR_CG(char *name)
{
  CR_Image *im;
  CR_Vector *meanmean, *targetmeanmean;
  CR_Matrix *meancov, *cov, *targetmeancov, *targetcov;
  double b_mean, b_var, t_mean, t_var;
  CR_ProbDist *b, *t;
  double bhat, logQ, K, J, N, error1_lower, error1_upper, error_lower, error_upper, error2;

  im = make_CR_Image(128, 1, 0);
  //  im = make_CR_Image(21, 21, 0);
  sprintf(name, "FisherInfo1");

  tile_w = 1;
  tile_h = 1;

//   target_tile_x = 0;
//   target_tile_y = 0;
//   num_tiles = 9;
//   sqrt_num_tiles = (int) sqrt(num_tiles);
//   tile_w = im->w / sqrt_num_tiles;
//   tile_h = im->h / sqrt_num_tiles;
//   N = (double) (tile_w * tile_h);
//   logQ = log2(num_tiles - 1) / N;  // normally regular log

  grf_im = make_CR_Image(im->w, im->h, 0);
  scratch_grf_im = make_CR_Image(im->w, im->h, 0);

  // discrete

  task3_shift = 0.0;
  task3_scale = 255.0;

  background_prob = init_CR_Vector("background_prob", 2, 0.6, 0.4);
  target_prob = init_CR_Vector("target_prob", 2, 0.1, 0.9);

  b = make_distribution_CR_Info(background_prob);
  t = make_distribution_CR_Info(target_prob);

//   bhat = bhattacharyya_bound_CR_Info(b, t);
//   K = 2 * bhat - logQ;
//   J = (double) background_prob->rows;  // alphabet size
//   error1_lower = pow(N + 1, -SQUARE(J));
//   error1_upper = pow(N + 1, SQUARE(J));
//   error2 = pow(2, -N * K);
//   error_lower = error1_lower * error2;
//   error_upper = error1_upper * error2;
//   printf("K = %lf, error1_lower = %lf, error1_upper = %lf, error2 = %lf, error_lower = %lf, error_upper = %lf\n", K, error1_lower, error1_upper, error2, error_lower, error_upper);
//   printf("-N * K = %lf, N = %lf, bhat = %lf, logQ = %lf, J = %i\n", -N * K, N, bhat, logQ, J);
//   printf("-N * 2 * bhat = %lf, 2^that = %lf\n", -N * 2 * bhat, pow(2, -N * 2 * bhat));
//   printf("cher(b, t) = %lf, kb(b, t) = %lf, kb(t, b) = %lf\n", chernoff_bound_CR_Info(b, t), kullback_leibler_distance_CR_Info(b, t), kullback_leibler_distance_CR_Info(t, b));
// 
//   printf("\n");
//   print_CR_Vector(target_prob);
//   print_CR_Vector(background_prob);
//   printf("image w = %i, image h = %i\n", grf_im->w, grf_im->h);
//   printf("num_tiles = %i\n", num_tiles);
//   printf("tile w = %i, tile h = %i\n\n", tile_w, tile_h);
  //  CR_exit(1);

  // background
  
//   meanmean = init_CR_Vector("meanmean", 1, b_mean); // 100.0
//   meancov = init_CR_Matrix("meancov", 1, 1, 1.0);  // 400.0
//   cov = init_CR_Matrix("cov", 1, 1, b_var);  // 20.0

  grf_cumprob = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_mean = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_covsqrt = make_CR_Random_Field(CR_MATRIX, im->h, im->w);
  grf_val = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_alpha = make_CR_Matrix("grf_alpha", im->h, im->w);

  //  make_gaussian_CR_Random_Field(im->w, im->h, meanmean, NULL, cov, grf_alpha, grf_mean, grf_covsqrt, grf_val);
  make_discrete_CR_Random_Field(im->w, im->h, task3_shift, task3_scale, background_prob, grf_cumprob, grf_val);

  
  // background-sized target

//   targetmeanmean = init_CR_Vector("targetmeanmean", 1, t_mean); // 200.0
//   targetmeancov = init_CR_Matrix("targetmeancov", 1, 1, 1.0);  // 400.0
//   targetcov = init_CR_Matrix("targetcov", 1, 1, t_var);  // 20.0

  grf_target_cumprob = make_CR_Random_Field(CR_VECTOR, tile_h, tile_w);
  grf_target_mean = make_CR_Random_Field(CR_VECTOR, tile_h, tile_w);
  grf_target_covsqrt = make_CR_Random_Field(CR_MATRIX, tile_h, tile_w);
  grf_target_val = make_CR_Random_Field(CR_VECTOR, tile_h, tile_w);
  grf_target_alpha = make_CR_Matrix("grf_target_alpha", tile_h, tile_w);

  //  make_gaussian_CR_Random_Field(tile_w, tile_h, targetmeanmean, NULL, targetcov, grf_target_alpha, grf_target_mean, grf_target_covsqrt, grf_target_val);
  make_discrete_CR_Random_Field(tile_w, tile_h, task3_shift, task3_scale, target_prob, grf_target_cumprob, grf_target_val);

  CR_difftime();

  fisher_mean_squared_error = 0.0;
  fisher_mean_error = 0.0;
  fisher_trials = 0;

  // finish

  return im;
}

//----------------------------------------------------------------------------

void FisherInfo1_grab_CR_CG(CR_Image *im)
{
  int x, y, i, j, init_x, init_y;
  double loglike_tb, target_loglike_tb, max_loglike_tb, error, squared_error;
  int max_i, max_j;
  int row, leftcol, rightcol;

  // change target tile location?

  time_now += CR_difftime();
  fisher_trials++;

  //  if (no_pause || time_now > pause_time) {
    //    time_now = 0;
//     target_tile_x = ranged_uniform_int_CR_Random(0, sqrt_num_tiles - 1);
//     target_tile_y = ranged_uniform_int_CR_Random(0, sqrt_num_tiles - 1);
    //    printf("moving\n");
  //  }

  // get x, y of target tile upper left hand corner from its tile coords.

  x = im->w / 2 - tile_w / 2;
  y = im->h / 2 - tile_h / 2;

  // draw appropriate image

  sample_discrete_CR_Random_Field(task3_shift, task3_scale, grf_cumprob, grf_val);
  sample_discrete_CR_Random_Field(task3_shift, task3_scale, grf_target_cumprob, grf_target_val);
  //  sample_gaussian_CR_Random_Field(grf_alpha, grf_mean, grf_covsqrt, grf_val);              // calc. background appearance
  //  sample_gaussian_CR_Random_Field(grf_target_alpha, grf_target_mean, grf_target_covsqrt, grf_target_val); // calc. target appearance
  overlay_CR_Random_Field(x, y, grf_target_val, grf_val);    // overlay target on background at appropriate tile
  convert_to_image_CR_Random_Field(grf_val, grf_im);            // draw background & target

//   if (no_pause || time_now == 0) {   // just moved target
// 
//     matching_distractors = 0;
// 
  //  target_loglike_tb = log_likelihood_ratio_discrete_CR_Random_Field(x, y, tile_w, tile_h, grf_val, task3_shift, task3_scale, target_prob, task3_shift, task3_scale, background_prob); 
  //  printf("target_loglike_tb = %lf\n", target_loglike_tb);

  init_x = ranged_uniform_int_CR_Random(0, im->w - tile_w);
  init_y = ranged_uniform_int_CR_Random(0, im->h - tile_h);

  max_i = init_x;
  max_j = init_y;

  //  CR_flush_printf("init_x = %i, init_y = %i\n", init_x, init_y);
  max_loglike_tb = log_likelihood_ratio_discrete_CR_Random_Field(init_x, init_y, tile_w, tile_h, grf_val, task3_shift, task3_scale, target_prob, task3_shift, task3_scale, background_prob); 

  for (j = 0; j < im->h - tile_h + 1; j++)
    for (i = 0; i < im->w - tile_w + 1; i++) {
      //      CR_flush_printf("i = %i, j = %i\n", i, j);
      if (j != init_y || i != init_x) {
	loglike_tb = log_likelihood_ratio_discrete_CR_Random_Field(i, j, tile_w, tile_h, grf_val, task3_shift, task3_scale, target_prob, task3_shift, task3_scale, background_prob); 
	//	printf("loglike = %lf\n", loglike_tb);
	if (loglike_tb > max_loglike_tb) {
	  max_i = i;
	  max_j = j;
	  max_loglike_tb = loglike_tb;
	}
      }
    }

  printf("max i = %i, max j = %i (x = %i, y = %i)\n", max_i, max_j, x, y);
  //  error = (double) (SQUARE(x - max_i) + SQUARE(y - max_j));
  squared_error = (double) (SQUARE(x - max_i) + SQUARE(y - max_j));
  fisher_mean_squared_error += squared_error;

  printf("%i: MSE = %lf\n", fisher_trials, fisher_mean_squared_error / (double) fisher_trials);

  // Fisher info

  double fishsum, leftp1, leftp2, rightp1, rightp2;

  leftcol = x - 1;
  rightcol = x + tile_w - 1;

  for (row = 0, fishsum = 0; row < im->h; row++) {
    leftp1 = evaluate_discrete_probability(grf_val->vrc[row][leftcol]->x[0], task3_shift, task3_scale, background_prob);
    leftp2 = evaluate_discrete_probability(grf_val->vrc[row][leftcol]->x[0], task3_shift, task3_scale, target_prob);
    //    rightp1 = evaluate_discrete_probability(grf_val->vrc[row][rightcol]->x[0], task3_shift, task3_scale, target_prob);
    //    rightp2 = evaluate_discrete_probability(grf_val->vrc[row][rightcol]->x[0], task3_shift, task3_scale, background_prob);
    printf("left (back) %i = %lf\n", row, leftp1 * SQUARE(log(leftp1) - log(leftp2)));
    //    printf("right (targ) %i = %lf\n", row, rightp1 * SQUARE(log(rightp1) - log(rightp2)));
    //    fishsum += leftp1 * SQUARE(log(leftp1) - log(leftp2)) + rightp1 * SQUARE(log(rightp1) - log(rightp2));
    fishsum += leftp1 * SQUARE(log(leftp1) - log(leftp2));

  }
  printf("fishsum = %lf, pred lower bound on MSE = %lf\n", fishsum, 1 / fishsum);

//     num_trials++;
//     num_matching_distractors += matching_distractors;
//     if (matching_distractors > 0)
//       num_errors++; 
//     if (!(num_trials % 10000)) {  // print report at intervals
//       printf("num trials = %i\n", num_trials);
//       printf("num errors = %i\n", num_errors);
//       printf("num_matching_distractors = %i\n", num_matching_distractors);
//       printf("error rate = %lf, mean matching distractors = %lf\n\n", (double) num_errors / (double) num_trials, (double) num_matching_distractors / (double) num_trials);
//     }
//     if (num_trials == max_trials)    // stop
//       CR_exit(1);
//   }

  // flip image vertically for proper opengl display and then draw

  vflip_CR_Image(grf_im, scratch_grf_im);
  glDrawPixels(grf_im->w, grf_im->h, GL_RGB, GL_UNSIGNED_BYTE, scratch_grf_im->data);

}

//----------------------------------------------------------------------------

void FisherInfo1_end_CR_CG(CR_Image *im)
{

}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Image *Track1_init_CR_CG(char *name)
{
  CR_Image *im;
  CR_Vector *meanmean, *targetmeanmean;
  CR_Matrix *meancov, *cov, *targetmeancov, *targetcov;
  double b_mean, b_var, t_mean, t_var;
  CR_ProbDist *b, *t;
  double bhat, logQ, K, J, N, error1_lower, error1_upper, error_lower, error_upper, error2;

  im = make_CR_Image(128, 128, 0);
  //  im = make_CR_Image(21, 21, 0);
  sprintf(name, "Track1");

  tile_w = 4;
  tile_h = 4;

  grf_im = make_CR_Image(im->w, im->h, 0);
  scratch_grf_im = make_CR_Image(im->w, im->h, 0);

  // discrete

  task3_shift = 0.0;
  task3_scale = 255.0;

  background_prob = init_CR_Vector("background_prob", 2, 0.9, 0.1);
  target_prob = init_CR_Vector("target_prob", 2, 0.1, 0.9);

  b = make_distribution_CR_Info(background_prob);
  t = make_distribution_CR_Info(target_prob);

  // background
  
  grf_cumprob = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_mean = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_covsqrt = make_CR_Random_Field(CR_MATRIX, im->h, im->w);
  grf_val = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_alpha = make_CR_Matrix("grf_alpha", im->h, im->w);

  //  make_gaussian_CR_Random_Field(im->w, im->h, meanmean, NULL, cov, grf_alpha, grf_mean, grf_covsqrt, grf_val);
  make_discrete_CR_Random_Field(im->w, im->h, task3_shift, task3_scale, background_prob, grf_cumprob, grf_val);

  
  // background-sized target

  grf_target_cumprob = make_CR_Random_Field(CR_VECTOR, tile_h, tile_w);
  grf_target_mean = make_CR_Random_Field(CR_VECTOR, tile_h, tile_w);
  grf_target_covsqrt = make_CR_Random_Field(CR_MATRIX, tile_h, tile_w);
  grf_target_val = make_CR_Random_Field(CR_VECTOR, tile_h, tile_w);
  grf_target_alpha = make_CR_Matrix("grf_target_alpha", tile_h, tile_w);

  //  make_gaussian_CR_Random_Field(tile_w, tile_h, targetmeanmean, NULL, targetcov, grf_target_alpha, grf_target_mean, grf_target_covsqrt, grf_target_val);
  make_discrete_CR_Random_Field(tile_w, tile_h, task3_shift, task3_scale, target_prob, grf_target_cumprob, grf_target_val);

  CR_difftime();

  fisher_mean_squared_error = 0.0;
  fisher_mean_error = 0.0;
  fisher_trials = 0;

  // target position, dynamics

  track_target_mean = init_CR_Vector("track_target_mean", 2,  
				     (double) (im->w / 2 - tile_w / 2),
				     (double) (im->w / 2 - tile_w / 2));
  new_mean = make_CR_Vector("new_mean", 2);
  track_target_cov = init_CR_Matrix("track_target_cov", 2, 2,
				    5.0, 0.0,
				    0.0, 5.0);
  track_target_covsqrt = make_CR_Matrix("track_target_covsqrt", track_target_cov->rows, track_target_cov->cols);
  square_root_CR_Matrix(track_target_cov, track_target_covsqrt);
  track_target_alpha = gaussian_alpha_CR_Matrix(track_target_cov);

  track_target_x = im->w / 2 - tile_w / 2;
  track_target_y = im->h / 2 - tile_h / 2;

  // set up tracking filter

  CR_FuncMatrix *F, *X, *H, *Q, *R, *X0, *P0;
  FILE *fp;

  fp = fopen("track1.s", "r");

  X = parse_CR_FuncMatrix(fp);
  X0 = parse_CR_FuncMatrix(fp);
  F = parse_CR_FuncMatrix(fp);
  H = parse_CR_FuncMatrix(fp);
  R = parse_CR_FuncMatrix(fp);
  Q = parse_CR_FuncMatrix(fp);
  P0 = parse_CR_FuncMatrix(fp);

  fclose(fp);

  track_target_Kal = make_nonlinear_CR_Kalman(X, F, H, Q, R, X0, P0, NULL);  // last param is Input

//   print_CR_FuncMatrix(X);
//   print_CR_FuncMatrix(X0);
//   print_CR_FuncMatrix(F);
//   print_CR_FuncMatrix(H);
//   print_CR_FuncMatrix(R);
//   print_CR_FuncMatrix(Q);
//   print_CR_FuncMatrix(P0);

  track_target_Z = make_CR_Matrix("Z", 2, 1);
  track_target_Z_array = make_CR_Matrix_array(1);
  track_target_Z_array[0] = track_target_Z;

  // finish

  return im;
}

//----------------------------------------------------------------------------

void Track1_grab_CR_CG(CR_Image *im)
{
  int x, y, i, j, init_x, init_y;
  double loglike_tb, target_loglike_tb, max_loglike_tb, error, squared_error;
  int max_i, max_j;
  int row, leftcol, rightcol;
  double kalx, kaly;

  // change target tile location?

  time_now += CR_difftime();
  fisher_trials++;

  if (no_pause || time_now > pause_time) {
    time_now = 0;
//     target_tile_x = ranged_uniform_int_CR_Random(0, sqrt_num_tiles - 1);
    //     target_tile_y = ranged_uniform_int_CR_Random(0, sqrt_num_tiles - 1);
    do {
      sample_gaussian_CR_Matrix(track_target_alpha, new_mean, track_target_mean, track_target_covsqrt);
    } while (new_mean->x[0] < 0 || new_mean->x[0] >= im->w - tile_w + 1|| 
	     new_mean->x[1] < 0 || new_mean->x[1] >= im->h - tile_h + 1);
    copy_CR_Vector(new_mean, track_target_mean);
    //    track_target_x = ranged_uniform_int_CR_Random(0, im->w - tile_w);
    //    track_target_y = ranged_uniform_int_CR_Random(0, im->h - tile_h);
  }

  // get x, y of target tile upper left hand corner from its tile coords.

  //  x = im->w / 2 - tile_w / 2;
  //  y = im->h / 2 - tile_h / 2;
  //  x = track_target_x;
  //  y = track_target_y;
  x = track_target_mean->x[0];
  y = track_target_mean->x[1];

  // draw appropriate image

  sample_discrete_CR_Random_Field(task3_shift, task3_scale, grf_cumprob, grf_val);
  sample_discrete_CR_Random_Field(task3_shift, task3_scale, grf_target_cumprob, grf_target_val);
  //  sample_gaussian_CR_Random_Field(grf_alpha, grf_mean, grf_covsqrt, grf_val);              // calc. background appearance
  //  sample_gaussian_CR_Random_Field(grf_target_alpha, grf_target_mean, grf_target_covsqrt, grf_target_val); // calc. target appearance
  overlay_CR_Random_Field(x, y, grf_target_val, grf_val);    // overlay target on background at appropriate tile
  convert_to_image_CR_Random_Field(grf_val, grf_im);            // draw background & target

   if (no_pause || time_now == 0) {   // just moved target

     MATRC(track_target_Z, 0, 0) = x;
     MATRC(track_target_Z, 1, 0) = y;

     update_nonlinear_CR_Kalman(track_target_Z_array, 1, track_target_Kal);
     //     print_CR_Kalman(track_target_Kal);

// 
//     matching_distractors = 0;
// 
  //  target_loglike_tb = log_likelihood_ratio_discrete_CR_Random_Field(x, y, tile_w, tile_h, grf_val, task3_shift, task3_scale, target_prob, task3_shift, task3_scale, background_prob); 
  //  printf("target_loglike_tb = %lf\n", target_loglike_tb);

  /*
  init_x = ranged_uniform_int_CR_Random(0, im->w - tile_w);
  init_y = ranged_uniform_int_CR_Random(0, im->h - tile_h);

  max_i = init_x;
  max_j = init_y;

  //  CR_flush_printf("init_x = %i, init_y = %i\n", init_x, init_y);
  max_loglike_tb = log_likelihood_ratio_discrete_CR_Random_Field(init_x, init_y, tile_w, tile_h, grf_val, task3_shift, task3_scale, target_prob, task3_shift, task3_scale, background_prob); 

  for (j = 0; j < im->h - tile_h + 1; j++)
    for (i = 0; i < im->w - tile_w + 1; i++) {
      //      CR_flush_printf("i = %i, j = %i\n", i, j);
      if (j != init_y || i != init_x) {
	loglike_tb = log_likelihood_ratio_discrete_CR_Random_Field(i, j, tile_w, tile_h, grf_val, task3_shift, task3_scale, target_prob, task3_shift, task3_scale, background_prob); 
	//	printf("loglike = %lf\n", loglike_tb);
	if (loglike_tb > max_loglike_tb) {
	  max_i = i;
	  max_j = j;
	  max_loglike_tb = loglike_tb;
	}
      }
  */
   }

   /*
  printf("max i = %i, max j = %i (x = %i, y = %i)\n", max_i, max_j, x, y);
  //  error = (double) (SQUARE(x - max_i) + SQUARE(y - max_j));
  //  squared_error = (double) (SQUARE(x - max_i) + SQUARE(y - max_j));
  //  fisher_mean_squared_error += squared_error;
  */

  // flip image vertically for proper opengl display and then draw

  vflip_CR_Image(grf_im, scratch_grf_im);
  glDrawPixels(grf_im->w, grf_im->h, GL_RGB, GL_UNSIGNED_BYTE, scratch_grf_im->data);

  // do overlays here

   if (no_pause || time_now == 0) {   // just moved target

     kalx = MATRC(track_target_Kal->x, 0, 0);
     kaly = MATRC(track_target_Kal->x, 1, 0);

     ogl_draw_rectangle(kalx - 2, im->h - (kaly - 2), 
			kalx + tile_w + 2, im->h - (kaly + tile_h + 2), 0, 255, 0);
   }
}

//----------------------------------------------------------------------------

void Track1_end_CR_CG(CR_Image *im)
{

}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Image *Track2_init_CR_CG(char *name)
{
  CR_Image *im;
  CR_Vector *meanmean, *targetmeanmean;
  CR_Matrix *meancov, *cov, *targetmeancov, *targetcov;
  double b_mean, b_var, t_mean, t_var;
  CR_ProbDist *b, *t;
  double bhat, logQ, K, J, N, error1_lower, error1_upper, error_lower, error_upper, error2;

  im = make_CR_Image(320, 240, 0);
  //  im = make_CR_Image(21, 21, 0);
  sprintf(name, "Track2");

  tile_w = 4;
  tile_h = 4;

  grf_im = make_CR_Image(im->w, im->h, 0);
  scratch_grf_im = make_CR_Image(im->w, im->h, 0);

  // discrete

  task3_shift = 0.0;
  task3_scale = 255.0;

  background_prob = init_CR_Vector("background_prob", 2, 0.9, 0.1);
  target_prob = init_CR_Vector("target_prob", 2, 0.1, 0.9);

  b = make_distribution_CR_Info(background_prob);
  t = make_distribution_CR_Info(target_prob);

  // background
  
  grf_cumprob = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_mean = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_covsqrt = make_CR_Random_Field(CR_MATRIX, im->h, im->w);
  grf_val = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_alpha = make_CR_Matrix("grf_alpha", im->h, im->w);

  //  make_gaussian_CR_Random_Field(im->w, im->h, meanmean, NULL, cov, grf_alpha, grf_mean, grf_covsqrt, grf_val);
  make_discrete_CR_Random_Field(im->w, im->h, task3_shift, task3_scale, background_prob, grf_cumprob, grf_val);
  
  // background-sized target

  grf_target_cumprob = make_CR_Random_Field(CR_VECTOR, tile_h, tile_w);
  grf_target_mean = make_CR_Random_Field(CR_VECTOR, tile_h, tile_w);
  grf_target_covsqrt = make_CR_Random_Field(CR_MATRIX, tile_h, tile_w);
  grf_target_val = make_CR_Random_Field(CR_VECTOR, tile_h, tile_w);
  grf_target_alpha = make_CR_Matrix("grf_target_alpha", tile_h, tile_w);

  //  make_gaussian_CR_Random_Field(tile_w, tile_h, targetmeanmean, NULL, targetcov, grf_target_alpha, grf_target_mean, grf_target_covsqrt, grf_target_val);
  make_discrete_CR_Random_Field(tile_w, tile_h, task3_shift, task3_scale, target_prob, grf_target_cumprob, grf_target_val);

  CR_difftime();

  // target position, dynamics

  horizon_y = im->h / 4;
  below_horizon_scans = im->h - horizon_y;

  center_x = (double *) CR_calloc(below_horizon_scans, sizeof(double));
  center_y = (double *) CR_calloc(below_horizon_scans, sizeof(double));

  left_x = (double *) CR_calloc(below_horizon_scans, sizeof(double));
  left_y = (double *) CR_calloc(below_horizon_scans, sizeof(double));
  right_x = (double *) CR_calloc(below_horizon_scans, sizeof(double));
  right_y = (double *) CR_calloc(below_horizon_scans, sizeof(double));

  center_a = 0;
  center_b = 0;
  center_c = im->w / 2;  // 160
  center_width = 200;    // width in pixels of road at bottom of image

  track_target_mean = init_CR_Vector("track_target_mean", 2,  
				     (double) (im->w / 2 - tile_w / 2),
				     (double) (im->w / 2 - tile_w / 2));
  new_mean = make_CR_Vector("new_mean", 2);
  track_target_cov = init_CR_Matrix("track_target_cov", 2, 2,
				    5.0, 0.0,
				    0.0, 5.0);
  track_target_covsqrt = make_CR_Matrix("track_target_covsqrt", track_target_cov->rows, track_target_cov->cols);
  square_root_CR_Matrix(track_target_cov, track_target_covsqrt);
  track_target_alpha = gaussian_alpha_CR_Matrix(track_target_cov);

  track_target_x = im->w / 2 - tile_w / 2;
  track_target_y = im->h / 2 - tile_h / 2;

  // set up tracking filter

  //  track_target_Kal = load_CR_Kalman("track2.s");
  track_target_Kal = load_CR_Kalman("ktrack2.s");

  track_target_Z = make_CR_Matrix("Z", 28, 1);
  track_target_Z_array = make_CR_Matrix_array(1);
  track_target_Z_array[0] = track_target_Z;

  no_pause = TRUE;
  track2_updates = 0;

  // finish

  return im;
}

//----------------------------------------------------------------------------

void Track2_grab_CR_CG(CR_Image *im)
{
  int x, y, i, j, init_x, init_y;
  double loglike_tb, target_loglike_tb, max_loglike_tb, error, squared_error;
  int max_i, max_j;
  int row, leftcol, rightcol;
  double kalx, kaly;
  double ly, ry;
  int xl, xr, imxl, imxr;

  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT);

  // change target tile location?

  time_now += CR_difftime();

  if (no_pause || time_now > pause_time) {

    //    printf("updates = %lf\n", track2_updates);

    center_a = 0.005 * sin(0.01 * track2_updates);  // 0.002
    center_b = 0.2 * sin(0.01 * track2_updates);
    center_c = im->w / 2 + 100.0 * sin(0.02 * track2_updates);  // 20.0
    center_width = 200 + 15 * sin(0.01 * track2_updates);

    track2_updates++;

//     do {
//       sample_gaussian_CR_Matrix(track_target_alpha, new_mean, track_target_mean, track_target_covsqrt);
//     } while (new_mean->x[0] < 0 || new_mean->x[0] >= im->w - tile_w + 1|| 
// 	     new_mean->x[1] < 0 || new_mean->x[1] >= im->h - tile_h + 1);
//     copy_CR_Vector(new_mean, track_target_mean);

    time_now = 0;

  }

  // get x, y of target tile upper left hand corner from its tile coords.

//   x = track_target_mean->x[0];
//   y = track_target_mean->x[1];

  // draw appropriate image

//   sample_discrete_CR_Random_Field(task3_shift, task3_scale, grf_cumprob, grf_val);
//   sample_discrete_CR_Random_Field(task3_shift, task3_scale, grf_target_cumprob, grf_target_val);
//   overlay_CR_Random_Field(x, y, grf_target_val, grf_val);    // overlay target on background at appropriate tile
//   convert_to_image_CR_Random_Field(grf_val, grf_im);            // draw background & target

   if (no_pause || time_now == 0) {   // just moved target

     //     MATRC(track_target_Z, 0, 0) = x;
     //     MATRC(track_target_Z, 1, 0) = y;

     //     update_nonlinear_CR_Kalman(track_target_Z_array, 1, track_target_Kal);
     //     print_CR_Kalman(track_target_Kal);

// 
//     matching_distractors = 0;
// 
  //  target_loglike_tb = log_likelihood_ratio_discrete_CR_Random_Field(x, y, tile_w, tile_h, grf_val, task3_shift, task3_scale, target_prob, task3_shift, task3_scale, background_prob); 
  //  printf("target_loglike_tb = %lf\n", target_loglike_tb);

  /*
  init_x = ranged_uniform_int_CR_Random(0, im->w - tile_w);
  init_y = ranged_uniform_int_CR_Random(0, im->h - tile_h);

  max_i = init_x;
  max_j = init_y;

  //  CR_flush_printf("init_x = %i, init_y = %i\n", init_x, init_y);
  max_loglike_tb = log_likelihood_ratio_discrete_CR_Random_Field(init_x, init_y, tile_w, tile_h, grf_val, task3_shift, task3_scale, target_prob, task3_shift, task3_scale, background_prob); 

  for (j = 0; j < im->h - tile_h + 1; j++)
    for (i = 0; i < im->w - tile_w + 1; i++) {
      //      CR_flush_printf("i = %i, j = %i\n", i, j);
      if (j != init_y || i != init_x) {
	loglike_tb = log_likelihood_ratio_discrete_CR_Random_Field(i, j, tile_w, tile_h, grf_val, task3_shift, task3_scale, target_prob, task3_shift, task3_scale, background_prob); 
	//	printf("loglike = %lf\n", loglike_tb);
	if (loglike_tb > max_loglike_tb) {
	  max_i = i;
	  max_j = j;
	  max_loglike_tb = loglike_tb;
	}
      }
  */
   }

   /*
  printf("max i = %i, max j = %i (x = %i, y = %i)\n", max_i, max_j, x, y);
  //  error = (double) (SQUARE(x - max_i) + SQUARE(y - max_j));
  //  squared_error = (double) (SQUARE(x - max_i) + SQUARE(y - max_j));
  //  fisher_mean_squared_error += squared_error;
  */

  // flip image vertically for proper opengl display and then draw

   //  vflip_CR_Image(grf_im, scratch_grf_im);
   //  glDrawPixels(grf_im->w, grf_im->h, GL_RGB, GL_UNSIGNED_BYTE, scratch_grf_im->data);

  // do overlays here

   if (no_pause || time_now == 0) {   // just moved target

     //     kalx = MATRC(track_target_Kal->x, 0, 0);
     //     kaly = MATRC(track_target_Kal->x, 1, 0);

     //     ogl_draw_rectangle(kalx - 2, im->h - (kaly - 2), 
     //			kalx + tile_w + 2, im->h - (kaly + tile_h + 2), 0, 255, 0);

     for (i = im->h - 1, j = 0; i >= horizon_y; i--, j++) {

       ly = im->h - 1 - i;

       center_x[j] = center_b * ly + center_a * SQUARE(ly) + center_c;
       center_y[j] = im->h - 1 - i;

       left_x[j] = (center_b + 0.5) * ly + center_a * SQUARE(ly) + (center_c - center_width / 2);
       left_y[j] = im->h - 1 - i;

       right_x[j] = (center_b - 0.5) * ly + center_a * SQUARE(ly) + (center_c + center_width / 2);
       right_y[j] = im->h - 1 - i;

       if (!(j % 10) && j >= 10 && j <= 140) {
	 xl = 2 * j / 10 - 2;
	 xr = 2 * j / 10 - 1;
	 MATRC(track_target_Z, xl, 0) = left_x[j] + normal_sample_CR_Random(0, 20);
	 MATRC(track_target_Z, xr, 0) = right_x[j] + normal_sample_CR_Random(0, 20);

	 imxl = (int) CR_round(MATRC(track_target_Z, xl, 0));
	 imxr = (int) CR_round(MATRC(track_target_Z, xr, 0));
	 if (!IN_IMAGE_HORIZ(im, imxl)) {
	   printf("left %i out: %i %i ==> x = %lf (%lf)\n", xl, imxl, j, MATRC(track_target_Kal->z_pred, xl, 0), left_x[j]);
	   MATRC(track_target_Z, xl, 0) = MATRC(track_target_Kal->z_pred, xl, 0);
	 }
	 if (!IN_IMAGE_HORIZ(im, imxr)) {
	   printf("right %i out: %i %i ==> x = %lf (%lf)\n", xr, imxr, j, MATRC(track_target_Kal->z_pred, xr, 0), right_x[j]);
	   MATRC(track_target_Z, xr, 0) = MATRC(track_target_Kal->z_pred, xr, 0);
	 }
       }
     }
     
     // draw road edges

     ogl_draw_line_strip_d(center_x, center_y, below_horizon_scans, 255, 255, 255);   
     ogl_draw_line_strip_d(left_x, left_y, below_horizon_scans, 255, 255, 255);   
     ogl_draw_line_strip_d(right_x, right_y, below_horizon_scans, 255, 255, 255);

     // draw horizon line

     ogl_draw_line(0, im->h - horizon_y, im->w, im->h - horizon_y, 255, 255, 255);

     //     ogl_draw_line(50, 0, 50, im->h, 255, 255, 255);
     //     ogl_draw_line(270, 0, 270, im->h, 255, 255, 255);

     // draw measurement points

     for (i = 10, j = 0; i <= 140; i += 10, j++) {

       left_x[j] = MATRC(track_target_Z, 2 * j, 0);
       left_y[j] = i;

       right_x[j] = MATRC(track_target_Z, 2 * j + 1, 0);
       right_y[j] = i;
     }
     ogl_draw_points_d(left_x, left_y, 2, 14, 255, 255, 0);
     ogl_draw_points_d(right_x, right_y, 2, 14, 255, 255, 0);

     // draw state

     //     print_CR_Matrix(track_target_Kal->z_pred);
     //     print_CR_Matrix(track_target_Z);
     update_nonlinear_CR_Kalman(track_target_Z_array, 1, track_target_Kal);
     //     print_CR_Matrix(track_target_Kal->x);

     //     printf("----------------------------------------------------\n");

     for (i = im->h - 1, j = 0; i >= horizon_y; i--, j++) {

       ly = im->h - 1 - i;

       left_x[j] = 
	 (MATRC(track_target_Kal->x, 1, 0) + MATRC(track_target_Kal->x, 4, 0)) * ly + 
	 MATRC(track_target_Kal->x, 0, 0) * SQUARE(ly) + 
	 (MATRC(track_target_Kal->x, 2, 0) - MATRC(track_target_Kal->x, 3, 0) / 2);

       left_y[j] = im->h - 1 - i;

       right_x[j] = 
	 (MATRC(track_target_Kal->x, 1, 0) - MATRC(track_target_Kal->x, 4, 0)) * ly + 
	 MATRC(track_target_Kal->x, 0, 0) * SQUARE(ly) + 
	 (MATRC(track_target_Kal->x, 2, 0) + MATRC(track_target_Kal->x, 3, 0) / 2);

       right_y[j] = im->h - 1 - i;
     }

     ogl_draw_line_strip_d(left_x, left_y, below_horizon_scans, 0, 255, 0);   
     ogl_draw_line_strip_d(right_x, right_y, below_horizon_scans, 0, 255, 0);

   }
}

//----------------------------------------------------------------------------

void Track2_end_CR_CG(CR_Image *im)
{

}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void Track3_initialize_neural_net();
double Track3_simulate_neural_net(CR_Vector *);
void Track3_fill_input_neural_net(int, int, CR_Vector *, CR_Image *);
void run_neural_net(CR_Image *, CR_Matrix *, int, int, int, int, int, int, int); 
void Track3_extract_nn_edge(CR_Image *, CR_Matrix *, int, int, int, int, int);

void run_svm(CR_Image *, CR_Matrix *, int, int, int, int, int, int, int); 

CR_Image *Track3_init_CR_CG(char *name)
{
  CR_Image *im;
  char *filename;
  int i, j;

  //    filename = choose_open_file_dialog_win32("Movies (*.mpg; *.avi; *.mov; fw*; *.rif)\0*.mpg;*.m1v;*.avi;*.mov;fw*;*.rif\0");

  roadmov = open_CR_Movie("C:\\InputData\\road_data\\nist_051401.avi");
  roadmov->currentframe = 5500;  // 5500 is canonical start position
  get_frame_CR_Movie(roadmov, roadmov->currentframe);

  //  roadmov = open_CR_Movie("C:\\InputData\\road_data\\cl900\\cl900-100.rif");
  //  roadmov = open_CR_Movie("D:\\Documents\\software\\fusion\\cl500-001.rif");

  im = make_CR_Image(roadmov->im->w, roadmov->im->h, 0);
  roadim = make_CR_Image(roadmov->im->w, roadmov->im->h, 0);
  sprintf(name, "Track3");

  CR_difftime();

  // set up tracking filter

  track3_startrow = 160;   // from bottom
  track3_endrow = 350;     // from bottom
  track3_interval = 10;
  track3_scans = 1 + (track3_endrow - track3_startrow) / track3_interval;   // should be 20 right now
  track3_left_margin = 16;
  track3_right_margin = im->w - 1 - 6;

  horizon_y = im->h - track3_endrow - 1;
  below_horizon_scans = im->h - horizon_y;

  // storage for drawing edge curves

  left_x = (double *) CR_calloc(below_horizon_scans, sizeof(double));
  left_y = (double *) CR_calloc(below_horizon_scans, sizeof(double));
  right_x = (double *) CR_calloc(below_horizon_scans, sizeof(double));
  right_y = (double *) CR_calloc(below_horizon_scans, sizeof(double));

  track_target_Z = make_CR_Matrix("Z", 2 * track3_scans, 1);
  track_target_Z_array = make_CR_Matrix_array(1);
  track_target_Z_array[0] = track_target_Z;

  track_target_Z_nn = make_CR_Matrix("Z_nn", 2 * track3_scans, 1);
  track_target_Z_nn_array = make_CR_Matrix_array(1);
  track_target_Z_nn_array[0] = track_target_Z_nn;

  edge_im = copy_CR_Image(im);
  track3_num_work_im = 5;
  track3_work_im = (CR_Image **) CR_calloc(track3_num_work_im, sizeof(CR_Image *));
  for (i = 0; i < track3_num_work_im; i++)
    track3_work_im[i] = make_FP_CR_Image(im->w, im->h, 0);

  track3_edge_window_radius = 20;
  track3_thresh_edge_strength = 600;

  // neural net stuff

  Track3_initialize_neural_net();

  // finish

  track3_tracking_started = FALSE;
  track3_x = 100;
  track3_y = 100;
  track3_mstate = make_CR_Matrix("track3_mstate", 6, 1);
  track3_statecov = init_diagonal_CR_Matrix("track3_statecov", 6, 0.00000001, 0.001, 100.0, 0.00000001, 0.001, 100.0);
  track3_statecovsqrt = copy_header_CR_Matrix(track3_statecov);
  square_root_CR_Matrix(track3_statecov, track3_statecovsqrt);
  track3_alpha = gaussian_alpha_CR_Matrix(track3_statecov);
  track3_state = make_CR_Vector("track3_state", 6);
  track3_candidate_state = make_CR_Vector("track3_candstate", 6);

  Track3_minfunc_resolution = init_CR_Vector("res", 6, 0.00001, 0.01, 1.0, 0.00001, 0.01, 1.0);
  //  Track3_minfunc_resolution = init_CR_Vector("res", 6, 0.001, 0.1, 1.0, 0.001, 0.1, 1.0);

//   track3_demo3_vars = init_CR_Vector("demo3_vars", 6, 
// 				     0.002, 0.928, -342.838,
// 				     0.000, -1.136, 806.850);
//  
//   track_target_Kal = demo3_load_kalman("track3_moon.s", track3_demo3_vars);

  // set up matrix for connected components of neural network output

  track3_nn_x_interval = 10;
  track3_nn_y_interval = 10;

  if (track3_interval != track3_nn_y_interval) 
    CR_error("assuming track3_interval = track3_nn_y_interval in use of track3_scans for goodrows");

  track3_nn_rows = track3_nn_cols = 0;
  for (j = im->h - track3_endrow; j <= im->h - track3_startrow; j += track3_nn_y_interval) 
    track3_nn_rows++;
  for (i = track3_left_margin; i <= track3_right_margin; i += track3_nn_x_interval) 
    track3_nn_cols++;

  track3_nn = make_CR_Matrix("track3_nn", track3_nn_rows, track3_nn_cols);
  track3_nn_cc = make_CR_Matrix("track3_nn_cc", track3_nn_rows, track3_nn_cols);

  left_nn_x = NULL;
  left_nn_y = NULL;
  right_nn_x = NULL;
  right_nn_y = NULL;
  left_goodrow = make_CR_Vector("left_goodrow", track3_scans);
  right_goodrow = make_CR_Vector("right_goodrow", track3_scans);

  //  track3_mov = write_avi_initialize_CR_Movie(NULL, im->w, im->h, 30);

  return im;
}

//----------------------------------------------------------------------------

int CR_clamp(int x, int a, int b)      
{
  if (x < a) 
    return a; 
  else if (x > b) 
    return b; 
  else
    return x;
}

//----------------------------------------------------------------------------

void Track3_initialize_neural_net()
{
  // read various weights and biases

  // annapurna

//   track3_IW1 = read_dlm_CR_Matrix("C:\\Documents\\software\\fusion\\nn\\IW1.txt", 20, 27);
//   track3_LW21 = read_dlm_CR_Matrix("C:\\Documents\\software\\fusion\\nn\\LW21.txt", 20, 20);
//   track3_LW32 = read_dlm_CR_Matrix("C:\\Documents\\software\\fusion\\nn\\LW32.txt", 1, 20);
//   track3_B1 = read_dlm_CR_Vector("C:\\Documents\\software\\fusion\\nn\\b1.txt", 20);
//   track3_B2 = read_dlm_CR_Vector("C:\\Documents\\software\\fusion\\nn\\b2.txt", 20);
//   track3_B3 = read_dlm_CR_Vector("C:\\Documents\\software\\fusion\\nn\\b3.txt", 1);

  // gasherbrum

  track3_IW1 = read_dlm_CR_Matrix("D:\\Documents\\software\\fusion\\nn\\IW1.txt", 20, 27);
  track3_LW21 = read_dlm_CR_Matrix("D:\\Documents\\software\\fusion\\nn\\LW21.txt", 20, 20);
  track3_LW32 = read_dlm_CR_Matrix("D:\\Documents\\software\\fusion\\nn\\LW32.txt", 1, 20);
  track3_B1 = read_dlm_CR_Vector("D:\\Documents\\software\\fusion\\nn\\b1.txt", 20);
  track3_B2 = read_dlm_CR_Vector("D:\\Documents\\software\\fusion\\nn\\b2.txt", 20);
  track3_B3 = read_dlm_CR_Vector("D:\\Documents\\software\\fusion\\nn\\b3.txt", 1);

//   track3_IW1 = read_dlm_CR_Matrix("C:\\InputData\\road_data\\filter_responses\\RGB_3x3_cl900_100_IW1.txt", 20, 27);
//   track3_LW21 = read_dlm_CR_Matrix("C:\\InputData\\road_data\\filter_responses\\RGB_3x3_cl900_100_LW21.txt", 20, 20);
//   track3_LW32 = read_dlm_CR_Matrix("C:\\InputData\\road_data\\filter_responses\\RGB_3x3_cl900_100_LW32.txt", 1, 20);
//   track3_B1 = read_dlm_CR_Vector("C:\\InputData\\road_data\\filter_responses\\RGB_3x3_cl900_100_b1.txt", 20);
//   track3_B2 = read_dlm_CR_Vector("C:\\InputData\\road_data\\filter_responses\\RGB_3x3_cl900_100_b2.txt", 20);
//   track3_B3 = read_dlm_CR_Vector("C:\\InputData\\road_data\\filter_responses\\RGB_3x3_cl900_100_b3.txt", 1);

  track3_O1 = make_CR_Vector("O1", 20);
  track3_O2 = make_CR_Vector("O2", 20);
  track3_O3 = make_CR_Vector("O3", 1);
  track3_I2 = make_CR_Vector("I2", 20);
  track3_I3 = make_CR_Vector("I3", 20);
  track3_I4 = make_CR_Vector("I4", 1);

  track3_Input = make_CR_Vector("Input", 27);

  // print 'em

  /*
  print_CR_Matrix(IW1);
  print_CR_Matrix(LW21);
  print_CR_Matrix(LW32);
  print_CR_Vector(B1);
  print_CR_Vector(B2);
  print_CR_Vector(B3);
  */
}

//----------------------------------------------------------------------------

// Input should be a 27-ary vector

// O1, I2 = 20 x 1
// O2, I3 = 20 x 1
// O3, I4 = 1 x 1

double Track3_simulate_neural_net(CR_Vector *Input)
{
  // O1 = IW1 * Input + B1

  product_sum_CR_Matrix(1.0, track3_IW1, Input, 1.0, track3_B1, track3_O1);

  // I2 = tansig(O1)

  tanh_CR_Vector(track3_O1, track3_I2);

  // O2 = LW21 * I2 + B2

  product_sum_CR_Matrix(1.0, track3_LW21, track3_I2, 1.0, track3_B2, track3_O2);

  // I3 = tansig(O2)

  tanh_CR_Vector(track3_O2, track3_I3);

  // O3 = LW32 * I3 + B3

  product_sum_CR_Matrix(1.0, track3_LW32, track3_I3, 1.0, track3_B3, track3_O3);

  // I4 = tansig(O3)

  tanh_CR_Vector(track3_O3, track3_I4);

  // return I4->x[0]

//   print_CR_Vector(track3_O1);
//   print_CR_Vector(track3_I2);
//   print_CR_Vector(track3_O2);
//   print_CR_Vector(track3_I3);
//   print_CR_Vector(track3_O3);
//   print_CR_Vector(track3_I4);

  return track3_I4->x[0];
}

//----------------------------------------------------------------------------

// puts RGB values of 3x3 neighborhood centered on (x, y) into Input (27-ary vector)

void Track3_fill_input_neural_net(int x, int y, CR_Vector *Input, CR_Image *im)
{
  Input->x[0] = (unsigned char) IMXY_R(im, x-1, y-1);
  Input->x[1] = (unsigned char) IMXY_G(im, x-1, y-1);
  Input->x[2] = (unsigned char) IMXY_B(im, x-1, y-1);

  Input->x[3] = (unsigned char) IMXY_R(im, x, y-1);
  Input->x[4] = (unsigned char) IMXY_G(im, x, y-1);
  Input->x[5] = (unsigned char) IMXY_B(im, x, y-1);
  
  Input->x[6] = (unsigned char) IMXY_R(im, x+1, y-1);
  Input->x[7] = (unsigned char) IMXY_G(im, x+1, y-1);
  Input->x[8] = (unsigned char) IMXY_B(im, x+1, y-1);
  
  Input->x[9] = (unsigned char) IMXY_R(im, x-1, y);
  Input->x[10] = (unsigned char) IMXY_G(im, x-1, y);
  Input->x[11] = (unsigned char) IMXY_B(im, x-1, y);
  
  Input->x[12] = (unsigned char) IMXY_R(im, x, y);
  Input->x[13] = (unsigned char) IMXY_G(im, x, y);
  Input->x[14] = (unsigned char) IMXY_B(im, x, y);
  
  Input->x[15] = (unsigned char) IMXY_R(im, x+1, y);
  Input->x[16] = (unsigned char) IMXY_G(im, x+1, y);
  Input->x[17] = (unsigned char) IMXY_B(im, x+1, y);
  
  Input->x[18] = (unsigned char) IMXY_R(im, x-1, y+1);
  Input->x[19] = (unsigned char) IMXY_G(im, x-1, y+1);
  Input->x[20] = (unsigned char) IMXY_B(im, x-1, y+1);
  
  Input->x[21] = (unsigned char) IMXY_R(im, x, y+1);
  Input->x[22] = (unsigned char) IMXY_G(im, x, y+1);
  Input->x[23] = (unsigned char) IMXY_B(im, x, y+1);
  
  Input->x[24] = (unsigned char) IMXY_R(im, x+1, y+1);
  Input->x[25] = (unsigned char) IMXY_G(im, x+1, y+1);
  Input->x[26] = (unsigned char) IMXY_B(im, x+1, y+1);
}

//----------------------------------------------------------------------------

void Track3_get_nn_measurements(CR_Image *im)
{
  int i, lj, rj;

  // run image through neural net, thresholding output to get "road" pixels

  run_neural_net(im, track3_nn, track3_left_margin, track3_right_margin, im->h - track3_endrow, im->h - track3_startrow, track3_nn_x_interval, track3_nn_y_interval, TRUE);

  // throw out all but biggest cc of road pixels

  biggest_connected_component_CR_Matrix(TRUE, track3_nn, track3_nn_cc);

  // find edges of biggest cc

  Track3_extract_nn_edge(im, track3_nn_cc, track3_left_margin, im->h - track3_endrow, track3_nn_x_interval, track3_nn_y_interval, FALSE);

  // format found edges for passing to Kalman filter

//   print_CR_Vector(left_nn_x);
//   print_CR_Vector(left_goodrow);

  for (i = 0, lj = left_nn_x->rows - 1, rj = right_nn_x->rows - 1; i < track_target_Z_nn->rows; i += 2) {

    if (left_goodrow->x[track3_scans - i / 2 - 1]) {
      MATRC(track_target_Z_nn, i, 0) = left_nn_x->x[lj];  
      lj--;
    }
    else
      MATRC(track_target_Z_nn, i, 0) = MATRC(track_target_Kal_nn->z_pred, i, 0);

    if (right_goodrow->x[track3_scans - i / 2 - 1]) {
      MATRC(track_target_Z_nn, i + 1, 0) = right_nn_x->x[rj];  
      rj--;
    }
    else 
      MATRC(track_target_Z_nn, i + 1, 0) = MATRC(track_target_Kal_nn->z_pred, i + 1, 0);
  }

  //  print_CR_Matrix(track_target_Z_nn);
  //  CR_exit(1);
}

//----------------------------------------------------------------------------

void Track3_get_measurements(CR_Image *im)
{
  int i, j, k, i_delta;
  int left, right, center, start, stop;
  int edge_strength, max_edge_strength, max_x;

  // sobel 

  /*
  iplScaleFP(im->iplim, track3_work_im[0]->iplim, 0, 255);
  iplFixedFilter(track3_work_im[0]->iplim, track3_work_im[1]->iplim, IPL_SOBEL_3x3_V);
  iplFixedFilter(track3_work_im[0]->iplim, track3_work_im[2]->iplim, IPL_SOBEL_3x3_H);
  iplAbs(track3_work_im[1]->iplim, track3_work_im[3]->iplim);
  iplAbs(track3_work_im[2]->iplim, track3_work_im[4]->iplim);
  iplAdd(track3_work_im[3]->iplim, track3_work_im[4]->iplim, track3_work_im[1]->iplim);
  iplScaleFP(track3_work_im[1]->iplim, edge_im->iplim, 0, 255);
  */

  //  write_CR_Image("xxx.ppm", edge_im);

  for (i = 0; i < track_target_Kal->z_pred->rows; i += 2) {

    j = im->h - (track3_startrow + track3_interval * i / 2);

    // both sides of road

    for (i_delta = 0; i_delta <= 1; i_delta++) {

      center = (int) CR_round(MATRC(track_target_Kal->z_pred, i + i_delta, 0));

      start = CR_clamp(center - track3_edge_window_radius, track3_left_margin, track3_right_margin);
      stop = CR_clamp(center + track3_edge_window_radius, track3_left_margin, track3_right_margin);

      // check whether edge window is entirely outside image; if so, use predicted measurement
      if (start == stop) {
	//	printf("out pred meas %i %i\n", i_delta, j);
	MATRC(track_target_Z, i + i_delta, 0) = MATRC(track_target_Kal->z_pred, i + i_delta, 0);
      }
      else {
	max_x = start;
	//	max_edge_strength = IMXY_R(im, start, j) + IMXY_G(im, start, j) + IMXY_B(im, start, j);
	max_edge_strength = thresholded_point_sobel_3x3(start, j, track3_thresh_edge_strength, im);
	// find strongest edge; use as measurement
	for (k = start + 1; k <= stop; k++) {
	  //	  edge_strength = IMXY_R(im, k, j) + IMXY_G(im, k, j) + IMXY_B(im, k, j);
	  edge_strength = thresholded_point_sobel_3x3(k, j, track3_thresh_edge_strength, im);
	  if (edge_strength > max_edge_strength) {
	    max_x = k;
	    max_edge_strength = edge_strength;
	  }
	}
	// strongest edge is measurement...
	//	if (max_edge_strength >= track3_thresh_edge_strength)
	if (max_edge_strength) {
	  //	  printf("good meas %i %i %i\n", i_delta, j, max_edge_strength);
	  MATRC(track_target_Z, i + i_delta, 0) = (double) max_x;
	}
	// ...unless no good edge is found, use predicted measurement
	else {
	  //	  printf("bad pred meas %i %i %i\n", i_delta, j, max_edge_strength);
	  MATRC(track_target_Z, i + i_delta, 0) = MATRC(track_target_Kal->z_pred, i + i_delta, 0);
	}
      }
    }
  }
//   print_CR_Matrix(track_target_Z);
//   CR_exit(1);
}

//----------------------------------------------------------------------------

// assuming state = (al, bl, cl, ar, br, cr)
// ystart should be >= ystop

// left_y, right_y are global arrays

void Track3_draw_quadratic(int ystart, int ystop, int xoffset, int imheight, CR_Matrix *state, int r, int g, int b)
{
  int i, j;
  double ly;

  for (i = ystart, j = 0; i >= ystop; i--, j++) {

    ly = imheight - 1 - i;
    
    left_x[j] = 
      MATRC(state, 2, 0) + MATRC(state, 1, 0) * ly + MATRC(state, 0, 0) * SQUARE(ly) + xoffset;
    
    left_y[j] = imheight - 1 - i;
    
    right_x[j] = 
      MATRC(state, 5, 0) + MATRC(state, 4, 0) * ly + MATRC(state, 3, 0) * SQUARE(ly) - xoffset;
    
    right_y[j] = imheight - 1 - i;
  }

  ogl_draw_line_strip_d(left_x, left_y, ystart - ystop + 1, r, g, b);   
  ogl_draw_line_strip_d(right_x, right_y, ystart - ystop + 1, r, g, b);
}

//----------------------------------------------------------------------------

// How good is the region defined by state?  Higher numbers are better, 
// zero is lowest possible

// state = (al, bl, cl, ar, br, cr)
// ystart should be >= ystop and bigger x's on the right side of the image

double Track3_region_objective_function(int ystart, int ystop, CR_Vector *state, CR_Image *im)
{
  int i, j, k;
  double ly, leftx, rightx;
  double totalarea, totalscore, scanwidth;
  int totalpixarea, scanpixwidth;
  double nn_result;
  int xsub, ysub;

  xsub = 5;
  ysub = 5;

  // first make sure all of left curve is to the left of all of right curve

  for (i = ystart, j = 0, totalarea = 0, totalscore = 0, totalpixarea = 0; i >= ystop; i--, j++) {

    //    printf("i = %i\n", i);

    ly = im->h - 1 - i;
    leftx = state->x[2] + state->x[1] * ly + state->x[0] * SQUARE(ly);
    rightx = state->x[5] + state->x[4] * ly + state->x[3] * SQUARE(ly);

    scanwidth = rightx - leftx;
    scanpixwidth = (int) rightx - (int) leftx + 1;

    if (scanwidth <= 0)
      return 0;
    else {
      totalarea += scanwidth;
      totalpixarea += scanpixwidth;

      if (!(j % ysub)) {
	for (k = (int) leftx; k <= (int) rightx; k += xsub) {

	  //    print_CR_Vector(state);
	  //  printf("left = %i, right = %i\n", (int) leftx, (int) rightx);

	  //	  printf("x = %i, y = %i\n", k, i);

	  if (k >= track3_left_margin && k <= track3_right_margin) {
	  
	    //	    ogl_draw_point(k, im->h - i, 2, 0, 0, 255);
	    
	    Track3_fill_input_neural_net(k, i, track3_Input, roadmov->im);
	    nn_result = Track3_simulate_neural_net(track3_Input);
	    if (nn_result >= 0.5) {
	      totalscore += 1;
	      ogl_draw_point(k, im->h - i, 2, 255, 0, 0);
	    }
	    else 
	      totalscore -= 1;
	  }
	}
      }
    }
  }

  printf("%lf\n", totalscore);

  return totalscore;
}

//----------------------------------------------------------------------------

// objective function

double Track3_minfunc(CR_Vector *x)
{
  minfunc_calls++;

  // note negation because we're achieving maximization of f(x) by minimizing -f(x)

  CR_flush_printf("minfunc!\n");
  print_CR_Vector(x);
  return -Track3_region_objective_function(minfunc_im->h - track3_startrow, 
					   minfunc_im->h - track3_endrow, 
					   x, 
					   minfunc_im);
}

//----------------------------------------------------------------------------

// approximate gradient of objective function

void Track3_dminfunc(CR_Vector *x, CR_Vector *dx)
{
  CR_Vector *minusmat, *plusmat;
  int i;
  double plus_score, minus_score;

  // set up

  minusmat = make_CR_Vector("minus", x->rows);
  plusmat = make_CR_Vector("plus", x->rows);

  // meat

  for (i = 0; i < x->rows; i++) {

    copy_CR_Vector(x, minusmat);
    copy_CR_Vector(x, plusmat);
    minusmat->x[i] -= Track3_minfunc_resolution->x[i];
    plusmat->x[i] += Track3_minfunc_resolution->x[i];

    minfunc_calls += 2;

    plus_score = Track3_region_objective_function(minfunc_im->h - track3_startrow, 
						  minfunc_im->h - track3_endrow, 
						  plusmat, 
						  minfunc_im);
    minus_score = Track3_region_objective_function(minfunc_im->h - track3_startrow, 
						   minfunc_im->h - track3_endrow, 
						   minusmat, 
						   minfunc_im);

    printf("i = %i, plus = %lf, minus = %lf, res = %lf\n", i, plus_score, minus_score, Track3_minfunc_resolution->x[i]);

    // note negation because we're achieving maximization of f(x) by minimizing -f(x)

    dx->x[i] = -(plus_score - minus_score) / (2 * Track3_minfunc_resolution->x[i]);      
  }

  print_CR_Vector(x);
  print_CR_Vector(dx);

  // clean up

  free_CR_Vector(minusmat);
  free_CR_Vector(plusmat);
}

//----------------------------------------------------------------------------

void Track3_powell(CR_Vector *samp, double *sampscore, CR_Image *im)
{
  int iter;
  double fret;
  CR_Matrix *xi;

  minfunc_calls = 0;
  xi = make_diagonal_all_CR_Matrix("xi", samp->rows, 1.0);
  minfunc_im = im;
  powell_CR_Matrix(samp, xi, 0.1, &iter, &fret, Track3_minfunc);
  free_CR_Matrix(xi);
  printf("%i function calls in Powell\n", minfunc_calls);
  *sampscore = -fret;  // negate because we're maximizing f(x) by minimizing -f(x)
}

//----------------------------------------------------------------------------

// if pr, use Polak-Ribiere; else use Fletcher-Reeves

void Track3_conjugate_gradient(int pr, CR_Vector *samp, double *sampscore, CR_Image *im)
{
  int iter;
  double fret;
  CR_Matrix *xi;

  minfunc_calls = 0;
  minfunc_im = im;
  frprmn_CR_Matrix(pr, samp, 0.1, &iter, &fret, Track3_minfunc, Track3_dminfunc);
  if (pr)
    printf("%i function calls in Polak-Ribiere\n", minfunc_calls);
  else
    printf("%i function calls in Fletcher-Reeves\n", minfunc_calls);
  *sampscore = -fret;  // negate because we're maximizing f(x) by minimizing -f(x)
}

//----------------------------------------------------------------------------

// track3 parameter values
// start_y = im->h - track3_endrow
// stop_y = im->h - track3_startrow
// start_x = track3_left_margin
// stop_x = track3_right_margin
// x_delta = track3_nn_x_interval
// y_delta = track3_nn_y_interval

void run_neural_net(CR_Image *im, CR_Matrix *nn_output, int start_x, int stop_x, int start_y, int stop_y, int x_delta, int y_delta, int do_draw) 
{
  int i, j, row, col;
  double nn_result;

  for (j = start_y, row = 0; j <= stop_y; j += y_delta, row++)
    for (i = start_x, col = 0; i <= stop_x; i += x_delta, col++) {
      Track3_fill_input_neural_net(i, j, track3_Input, im);
      nn_result = Track3_simulate_neural_net(track3_Input);
      if (nn_result >= 0.5) {
	MATRC(nn_output, row, col) = 1.0;
// 	if (do_draw)
// 	  ogl_draw_point(i, im->h - j, 3, 255, 0, 255);
      }
      else
	MATRC(nn_output, row, col) = 0.0;
    }
}

//----------------------------------------------------------------------------

// puts results in global vectors left_nn_x, left_nn_y, right_nn_x, right_nn_y

// assuming global left_goodrow, right_goodrow exist and are same size as nn_output->rows

// if do_image_y = FALSE, computed y values are in drawing coordinates, not image coordinates

void Track3_extract_nn_edge(CR_Image *im, CR_Matrix *nn_output, int start_x, int start_y, int x_delta, int y_delta, int do_image_y)
{
  int x, y, i, length;
  double rowsum;

  // left
  
  if (left_nn_x) {  // assuming these are created and freed in tandem
    free_CR_Vector(left_nn_x);
    free_CR_Vector(left_nn_y);
  }
    
  for (y = 0, length = nn_output->rows; y < nn_output->rows; y++) {
    if (MATRC(nn_output, y, 0)) {
      length--;
      left_goodrow->x[y] = 0.0;
    }
    else {
      for (x = 1, rowsum = 0.0; x < nn_output->cols; x++)
	rowsum += MATRC(nn_output, y, x);
      if (!rowsum) {
	length--;
	left_goodrow->x[y] = 0.0;
      }
      else
	left_goodrow->x[y] = 1.0;
    }
  }

//   print_CR_Vector(left_goodrow);
//   exit(1);

  //  printf("left length = %i\n", length);

  left_nn_x = make_CR_Vector("left_x", length);
  left_nn_y = make_CR_Vector("left_y", length);

  for (y = 0, i = 0; y < nn_output->rows; y++)
    if (left_goodrow->x[y]) {
      for (x = 1; !MATRC(nn_output, y, x) && x < nn_output->cols; x++);
      left_nn_x->x[i] = (double) (start_x + x_delta * x);
      if (!do_image_y)
	left_nn_y->x[i] = (double) (im->h - start_y - (y_delta * y));
      else
	left_nn_y->x[i] = (double) (start_y + (y_delta * y));	
      //      left_nn_x->x[i] = (double) (track3_left_margin + track3_nn_x_interval * x);
      //      left_nn_y->x[i] = (double) (track3_endrow - (track3_nn_y_interval * y));
      i++;
    }

  // right

  if (right_nn_x) {  // assuming these are created and freed in tandem
    free_CR_Vector(right_nn_x);
    free_CR_Vector(right_nn_y);
  }
  
  for (y = 0, length = nn_output->rows; y < nn_output->rows; y++) {
    if (MATRC(nn_output, y, nn_output->cols - 1)) {
      length--;
      right_goodrow->x[y] = 0.0;
    }
    else {
      for (x = nn_output->cols - 2, rowsum = 0.0; x >= 0; x--)
	rowsum += MATRC(nn_output, y, x);
      if (!rowsum) {
	length--;
	right_goodrow->x[y] = 0.0;
      }
      else
	right_goodrow->x[y] = 1.0;
    }
  }

  //  printf("right length = %i\n", length);

  right_nn_x = make_CR_Vector("right_x", length);
  right_nn_y = make_CR_Vector("right_y", length);

  for (y = 0, i = 0; y < nn_output->rows; y++)
    if (right_goodrow->x[y]) {
      for (x = nn_output->cols - 2; !MATRC(nn_output, y, x) && x >= 0; x--);
      right_nn_x->x[i] = (double) (start_x + x_delta * x);
      if (!do_image_y)
	right_nn_y->x[i] = (double) (im->h - start_y - (y_delta * y));
      else
	right_nn_y->x[i] = (double) (start_y + (y_delta * y));	
      //      right_nn_x->x[i] = (double) (track3_left_margin + track3_nn_x_interval * x);
      //      right_nn_y->x[i] = (double) (track3_endrow - (track3_nn_y_interval * y));
      i++;
    }

//   print_CR_Vector(left_nn_x);
//   print_CR_Vector(left_nn_y);
//   print_CR_Vector(right_nn_x);
//   print_CR_Vector(right_nn_y);
}

//----------------------------------------------------------------------------

void Track3_grab_CR_CG(CR_Image *im)
{
  int x, y, i, j, row, col;
  double maxobj_result, candobj_result, sampscore;
  CR_Vector *left_nn, *right_nn;

  // still initializing

  if (!track3_tracking_started) {

    // draw current image
    
    vflip_CR_Image(roadmov->im, roadim);
    glDrawPixels(roadim->w, roadim->h, GL_RGB, GL_UNSIGNED_BYTE, roadim->data);
    
    // draw image processing limits
    
    ogl_draw_rectangle(track3_left_margin, track3_startrow, track3_right_margin, track3_endrow, 255, 255, 255);
        
    // initialize state manually

//     track3_demo3_vars = init_CR_Vector("demo3_vars", 6, 
// 				       0.002, 0.928, -342.838,
// 				       0.000, -1.136, 806.850);

    // initialize state automatically with cc of nn method

    
    run_neural_net(roadmov->im, track3_nn, track3_left_margin, track3_right_margin, im->h - track3_endrow, im->h - track3_startrow, track3_nn_x_interval, track3_nn_y_interval, TRUE);

    biggest_connected_component_CR_Matrix(TRUE, track3_nn, track3_nn_cc);

    Track3_extract_nn_edge(roadmov->im, track3_nn_cc, track3_left_margin, roadmov->im->h - track3_endrow, track3_nn_x_interval, track3_nn_y_interval, FALSE);

    left_nn = polyfit_CR_Matrix(2, left_nn_y, left_nn_x);
    right_nn = polyfit_CR_Matrix(2, right_nn_y, right_nn_x);

//     print_CR_Vector(left_nn);
//     print_CR_Vector(right_nn);

    ogl_draw_CR_Matrix(15, im->h - 30, im->h, track3_nn);
    ogl_draw_CR_Matrix(100, im->h - 30, im->h, track3_nn_cc);

    track3_demo3_vars = init_CR_Vector("demo3_vars", 6, 
				       left_nn->x[2], left_nn->x[1], left_nn->x[0],
				       right_nn->x[2], right_nn->x[1], right_nn->x[0]);


    // show the fit 

    nn_quad_fit = init_CR_Matrix("nn_quad", 6, 1, 
				       left_nn->x[2], left_nn->x[1], left_nn->x[0],
				       right_nn->x[2], right_nn->x[1], right_nn->x[0]);

    Track3_draw_quadratic(im->h - 1, horizon_y, 0, im->h, nn_quad_fit, 0, 255, 255);

    // initialize Kalman filter

    track_target_Kal = demo3_load_kalman("track3_moon.s", track3_demo3_vars);
    track_target_Kal_nn = demo3_load_kalman("track3_moon.s", track3_demo3_vars);

    // finish

    track3_tracking_started = TRUE;
    
    //     Track3_grab_CR_CG(im);
    
    /*
      track3_tracking_started = TRUE;
      Track3_grab_CR_CG(im);
      
      set_CR_Vector(track3_state, 0.0, 0.4, 200.0, 0.0, -0.4, 520.0);
      
      //     Track3_conjugate_gradient(FALSE, track3_state, &sampscore, roadmov->im);
      Track3_powell(track3_state, &sampscore, roadmov->im);
      print_CR_Vector(track3_state);
      CR_exit(1);
      
      //     obj_result = Track3_region_objective_function(im->h - 1, horizon_y, state, roadmov->im);
      //     printf("obj_result = %lf\n", obj_result);
      //     CR_exit(1);
      
      maxobj_result = Track3_region_objective_function(im->h - track3_startrow, im->h - track3_endrow, track3_state, roadmov->im);
      
      for (i = 0; i < 1000; i++) {
      
      // draw current image
      
      vflip_CR_Image(roadmov->im, roadim);
      glDrawPixels(roadim->w, roadim->h, GL_RGB, GL_UNSIGNED_BYTE, roadim->data);
      
      // meat
      
      ogl_draw_rectangle(track3_left_margin, track3_endrow, track3_right_margin, track3_startrow, 255, 255, 255);
      
      for (j = 0; j < 6; j++)
      MATRC(track3_mstate, j, 0) = track3_state->x[j];
      Track3_draw_quadratic(im->h - track3_startrow, im->h - track3_endrow, 0, im->h, track3_mstate, 0, 255, 0);
      
      sample_gaussian_CR_Matrix(track3_alpha, track3_candidate_state, track3_state, track3_statecovsqrt);
      print_CR_Vector(track3_state);
      print_CR_Vector(track3_candidate_state);
      
      candobj_result = Track3_region_objective_function(im->h - track3_startrow, im->h - track3_endrow, track3_candidate_state, roadmov->im);
      
      for (j = 0; j < 6; j++)
      MATRC(track3_mstate, j, 0) = track3_candidate_state->x[j];
      Track3_draw_quadratic(im->h - track3_startrow, im->h - track3_endrow, 0, im->h, track3_mstate, 255, 255, 0);
      
      if (candobj_result > maxobj_result) {
      maxobj_result = candobj_result;
      copy_CR_Vector(track3_candidate_state, track3_state);
      }
      
      printf("result = %lf\n", maxobj_result);
      
      //      CR_exit(1);
      
      glutSwapBuffers();
      
      }
      //     Track3_grab_CR_CG(im);
    */
  }

  // tracking started
  
  else {
    
    // draw current image
    
    vflip_CR_Image(roadmov->im, roadim);
    glDrawPixels(roadim->w, roadim->h, GL_RGB, GL_UNSIGNED_BYTE, roadim->data);
    
    // draw image processing limits
    
    ogl_draw_rectangle(track3_left_margin, track3_startrow, track3_right_margin, track3_endrow, 255, 255, 255);
    
    // -------------------------------
    // *** Region-based neural net ***
    // -------------------------------
    
    // get measurements

    Track3_get_nn_measurements(roadmov->im);

    // draw measurements

    ogl_draw_CR_Matrix(15, im->h - 30, im->h, track3_nn);
    ogl_draw_CR_Matrix(100, im->h - 30, im->h, track3_nn_cc);

    // update state
    
    update_nonlinear_CR_Kalman(track_target_Z_nn_array, 1, track_target_Kal_nn);

    // draw state
    
    Track3_draw_quadratic(im->h - 1, horizon_y, 0, im->h, track_target_Kal_nn->x, 0, 255, 255);
    
 // ------------
    // *** Edge ***
    // ------------

    // get measurements
    
    Track3_get_measurements(roadmov->im);
    
    // update state
    
    update_nonlinear_CR_Kalman(track_target_Z_array, 1, track_target_Kal);
    
    // draw state
    
    Track3_draw_quadratic(im->h - 1, horizon_y, 0, im->h, track_target_Kal->x, 0, 255, 0);
    
    // draw edge search limits
    
    Track3_draw_quadratic(im->h - track3_startrow, im->h - track3_endrow, track3_edge_window_radius, im->h, track_target_Kal->x, 255, 0, 255);
    Track3_draw_quadratic(im->h - track3_startrow, im->h - track3_endrow, -track3_edge_window_radius, im->h, track_target_Kal->x, 255, 0, 255);
    
    // draw found edges
    
    for (i = track3_startrow, j = 0; i <= track3_endrow; i += track3_interval, j++) {
      
      left_x[j] = MATRC(track_target_Z, 2 * j, 0);
      left_y[j] = i;
      
      right_x[j] = MATRC(track_target_Z, 2 * j + 1, 0);
      right_y[j] = i;
    }
    ogl_draw_points_d(left_x, left_y, 3, track3_scans, 255, 255, 0);
    ogl_draw_points_d(right_x, right_y, 3, track3_scans, 255, 255, 0);

    // -------------------
    // grab the next image
    // -------------------
    
    if (roadmov->currentframe < roadmov->lastframe) 
      get_frame_CR_Movie(roadmov, roadmov->currentframe++);
  }
}

//----------------------------------------------------------------------------

void Track3_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  track3_mov = write_avi_initialize_CR_Movie(NULL, im->w, im->h, 30);
  //  write_avi_finish_CR_Movie(track3_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Image *Track3DClothoid_init_CR_CG(char *name)
{
  CR_Image *im;
  char *filename;
  int i, j;

//   roadmov = open_CR_Movie("C:\\InputData\\road_data\\nist_051401.avi");
//   roadmov->currentframe = 5500;  // 5500 is canonical start position
//   get_frame_CR_Movie(roadmov, roadmov->currentframe);
// 
//   im = make_CR_Image(roadmov->im->w, roadmov->im->h, 0);
//   roadim = make_CR_Image(roadmov->im->w, roadmov->im->h, 0);
//   sprintf(name, "Track3DClothoid");

  im =  make_CR_Image(720, 480, 0);

  clothoid_scale = 100;
  clothoid_scale_delta = 1;

  return im;
}

//----------------------------------------------------------------------------

void Track3DClothoid_grab_CR_CG(CR_Image *im)
{
  // -------------------
  // draw current image
  // -------------------
  
//   vflip_CR_Image(roadmov->im, roadim);
//   glDrawPixels(roadim->w, roadim->h, GL_RGB, GL_UNSIGNED_BYTE, roadim->data);
    
  // -------------------
  // do other good stuff
  // -------------------

  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT);

  // Fresnel experimentation

  ogl_draw_clothoid2(0.0, 0.0001, 0.0, 100.0, PI/2, im->w / 2, im->h / 2, 50, 255, 255, 255);
  //  ogl_draw_fresnel_spiral(clothoid_scale, 0.0, 50.0, 5000, im->w / 2, im->h / 2, 255, 255, 255);
  //  ogl_draw_approx_clothoid(0.0, 0.001, 0.0, 10.0, im->w / 2, im->h / 2, 10, 255, 255, 255);
  //  ogl_draw_approx_clothoid(0.001, 0.001, 0.0, 100.0, im->w / 2, im->h / 2, 10, 255, 255, 255);
  //  ogl_draw_clothoid(0.0001, 0.0001, 0.0, 100.0, 0.0, im->w / 2, im->h / 2, 5000, 255, 255, 255);

  //  ogl_draw_parametric_quadratic(-0.1, 0.0, 100.0, 0.0, 2.0, 0.0, -100.0, 100.0, im->w / 2, im->h / 2, 0.0, 100, 255, 0, 0);

  //  clothoid_scale += clothoid_scale_delta;

  // -------------------
  // grab the next image
  // -------------------
    
//   if (roadmov->currentframe < roadmov->lastframe) 
//     get_frame_CR_Movie(roadmov, roadmov->currentframe++);
}

//----------------------------------------------------------------------------

void Track3DClothoid_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  write_avi_finish_CR_Movie(track3_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void Track3DQuadratic_project_to_ground_plane(double, double, double *, double *, double *);
void Track3DQuadratic_project_to_ground_plane_CR_Vector(CR_Vector *, CR_Vector *, CR_Vector *, CR_Vector *, CR_Vector *);

CR_Kalman *initialize_edge_tracker_from_nn(char *, CR_Image *);

CR_Image *Track3DQuadratic_init_CR_CG(char *name)
{
  CR_Image *im;
  char *filename;
  int i, j;

  /*
  track_3d_quadratic_vars = init_CR_Vector("track_3d_quadratic_vars", 13, 0.0, 0.0, -1.0, 0.0, 2.0, 0.0,3.0, 2.0, 0.24, 375, 375, 360, 240);       // image width, height

  plane_from_laser_kal = demo3_load_kalman("plane_from_laser.s", track_3d_quadratic_vars);
  //  track_3d_quadratic_kal = demo3_load_kalman("planar_z_track3dquad.s", NULL);

  CR_exit(1);
  */

  int tmpFlag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
  tmpFlag &= ~_CRTDBG_ALLOC_MEM_DF;
  _CrtSetDbgFlag(tmpFlag);

#ifdef BADROAD_REINIT
  //  track_3d_first_frame = 1500; // 5500 is canonical start position, 1500 is kinda fun, too
  track_3d_first_frame = 4818; // 5500 is canonical start position, 1500 is kinda fun, too
  //  track_3d_first_frame = 5500; // 5500 is canonical start position, 1500 is kinda fun, too
#else
  //  track_3d_first_frame = 2300; // 5500 is canonical start position, 1500 is kinda fun, too
  //track_3d_first_frame = 1500; // 5500 is canonical start position, 1500 is kinda fun, too
  track_3d_first_frame = 5500; // 5500 is canonical start position, 1500 is kinda fun, too
#endif

  track_3d_first_frame = 0; 

  //  track_3d_first_frame = 3140;  // for sequence 183 

  //roadmov = open_CR_Movie("C:\\InputData\\road_data\\nist_051401.avi");
  // commented for cvpr2003
  // last good
  //  roadmov = open_CR_Movie("C:\\InputData\\road_data\\reduced_nist_051401.avi");
  roadmov = open_CR_Movie("C:\\Documents\\software\\fusion\\cm_frames\\iv2003_data\\t_intersection.avi");
  //  roadmov = open_CR_Movie("D:\\Documents\\software\\cvpr2003_data\\huff_half_dvData_183_.avi");
  roadmov->currentframe = track_3d_first_frame;  
  get_frame_CR_Movie(roadmov, roadmov->currentframe);

  im = make_CR_Image(roadmov->im->w, roadmov->im->h, 0);
  roadim = make_CR_Image(roadmov->im->w, roadmov->im->h, 0);
  sprintf(name, "Track3DQuadratic");

  //  track_3d_quadratic_edge_window_radius = 40;
  //iv2003
  track_3d_quadratic_edge_window_radius = 10;
  //  track_3d_quadratic_edge_window_radius = 20 * im->w / 720;
  track_3d_quadratic_thresh_edge_strength = 100;  // 500

  // for fake measurements
//   track_3d_left_margin = 16;
//   track_3d_right_margin = im->w - 1 - 6;
//   track_3d_top_margin = 5;
//   track_3d_bottom_margin = im->h - 1;

  // for real images
 track_3d_left_margin = 16 * im->w / 720;
 //iv2003
 track_3d_right_margin = im->w - 1 - 20 * im->w / 720;
 // track_3d_right_margin = im->w - 1 - 16 * im->w / 720;
 track_3d_top_margin = im->h - 400 * im->h / 480; // 400
 //iv2003
 track_3d_bottom_margin = im->h - 110 * im->h / 480;
 // track_3d_bottom_margin = im->h - 160 * im->h / 480;

  // for nn testing
//   track_3d_left_margin = 1;
//   track_3d_right_margin = im->w - 2;
//   track_3d_top_margin = 1; // 350
//   track_3d_bottom_margin = im->h - 2;

 // track_3d_window_radius = 0.5;
 track_3d_window_radius = 0.33;

 track_3d_depth_dependent_search = FALSE;  // make edge search window width inversely proportional to depth?
 //  track_3d_depth_dependent_search = TRUE;  // make edge search window width inversely proportional to depth?

  // load and initialize Kalman filter road model

 //iv2003
  track_3d_quadratic_vars = init_CR_Vector("track_3d_quadratic_vars", 13,
					   0.0, 0.0, -1.0,      // ax, bx, cx = -1.0 
					   0.0, 2.0, 0.0,       // az, bz, cz
					   4.0,                 // 3.75 road radius (meters)
					   1.94,                 // 2.0 camera height off ground plane (meters)
					   //DEG2RAD(13.0),         // camera tilt: 0 level, + down, - up (radians)
					   DEG2RAD(8.5),                // 0.24 camera tilt: 0 level, + down, - up (radians)
					   //0.24,                // 0.24 camera tilt: 0 level, + down, - up (radians)
					   //					   750.0, 750.0,        // horizontal, vertical focal length
					   820.0*0.6, 820.0*0.6,        // horizontal, vertical focal length
					   //					   500.0 * im->w / 720, 500.0 * im->w / 720,        // horizontal, vertical focal length
					   //					   (double) im->w, (double) im->h);       // image width, height
					   (double) im->w, (double) im->h);       // image width, height

//   track_3d_quadratic_vars = init_CR_Vector("track_3d_quadratic_vars", 13,
// 					   0.0, 0.0, -1.0,      // ax, bx, cx = -1.0 
// 					   0.0, 2.0, 0.0,       // az, bz, cz
// 					   3.0,                 // 3.0 road radius (meters)
// 					   2.0,                 // 2.0 camera height off ground plane (meters)
// 					   //DEG2RAD(13.0),         // camera tilt: 0 level, + down, - up (radians)
// 					   0.24,                // 0.24 camera tilt: 0 level, + down, - up (radians)
// 					   //0.24,                // 0.24 camera tilt: 0 level, + down, - up (radians)
// 					   //					   750.0, 750.0,        // horizontal, vertical focal length
// 					   750.0 * im->w / 720, 750.0 * im->w / 720,        // horizontal, vertical focal length
// 					   (double) im->w, (double) im->h);       // image width, height
  t3d = track_3d_quadratic_vars->x;

  printf("first vars\n");
  print_CR_Vector(track_3d_quadratic_vars);

  // set up matrix for connected components of neural network output

  // iv2003
  //  Track3_initialize_neural_net();

//   track_3d_nn_x_interval = 8;  // 10
//   track_3d_nn_y_interval = 16;  // 10
  track_3d_nn_x_interval = 10;  // 10
  track_3d_nn_y_interval = 10;  // 10

  track_3d_nn_rows = track_3d_nn_cols = 0;
  for (j = track_3d_top_margin; j <= track_3d_bottom_margin; j += track_3d_nn_y_interval) 
    track_3d_nn_rows++;
  for (i = track_3d_left_margin; i <= track_3d_right_margin; i += track_3d_nn_x_interval) 
    track_3d_nn_cols++;

  track_3d_nn = make_CR_Matrix("track_3d_nn", track_3d_nn_rows, track_3d_nn_cols);
  track_3d_nn_cc = make_CR_Matrix("track_3d_nn_cc", track_3d_nn_rows, track_3d_nn_cols);

  left_nn_x = left_nn_y = right_nn_x = right_nn_y = NULL;

  left_3d_nn_x = left_3d_nn_z = right_3d_nn_x = right_3d_nn_z = NULL;
  left_3d_nn_z_ground = right_3d_nn_z_ground = NULL;

  left_goodrow = make_CR_Vector("left_goodrow", track_3d_nn_rows);
  right_goodrow = make_CR_Vector("right_goodrow", track_3d_nn_rows);

  // initialize with biggest cc of neural network method

  CR_flush_printf("a\n");

  // iv2003
  track_3d_quadratic_kal = demo3_load_kalman("planar_z_track3dquad.s", track_3d_quadratic_vars);

  CR_flush_printf("b\n");

  //  track_3d_quadratic_kal = initialize_edge_tracker_from_nn("planar_z_track3dquad.s", roadmov->im);
  //  initial_track_3d_quadratic_kal = copy_CR_Kalman(track_3d_quadratic_kal);

  //  track_3d_quadratic_kal = demo3_load_kalman("z2track3dquad.s", track_3d_quadratic_vars);
  //  track_3d_quadratic_kal = demo3_load_kalman("ztrack3dquad.s", track_3d_quadratic_vars);
  //  track_3d_quadratic_kal = demo3_load_kalman("track3dquad.s", track_3d_quadratic_vars);
  //  track_3d_quadratic_kal = demo3_load_kalman("track3dquadreduced.s", track_3d_quadratic_vars);
  
  // iv2003
  track_3d_quadratic_Z = make_CR_Matrix("Z", track_3d_quadratic_kal->z_pred->rows, track_3d_quadratic_kal->z_pred->cols);
  track_3d_quadratic_Z_array = make_CR_Matrix_array(1);
  track_3d_quadratic_Z_array[0] = track_3d_quadratic_Z;
  track_3d_quadratic_good_Z = make_CR_Vector("good_Z", track_3d_quadratic_Z->rows);

  CR_flush_printf("c\n");

  //iv2003
//   track_3d_quadratic_kal_nn = demo3_load_kalman("planar_z_track3dquad.s", track_3d_quadratic_vars);
// 
//   CR_flush_printf("d\n");
// 
//   track_3d_quadratic_Z_nn = make_CR_Matrix("Z_nn", track_3d_quadratic_kal_nn->z_pred->rows, track_3d_quadratic_kal_nn->z_pred->cols);
//   track_3d_quadratic_Z_nn_array = make_CR_Matrix_array(1);
//   track_3d_quadratic_Z_nn_array[0] = track_3d_quadratic_Z_nn;

  CR_flush_printf("e\n");

  // (32 - 5) / 100 = 
  num_road3d_pts = 100; // 500
  //  num_road3d_pts = 20;
  road3d_delta = 0.27;  // 0.32
  road3d_xc = (double *) CR_calloc(num_road3d_pts, sizeof(double));
  road3d_yc = (double *) CR_calloc(num_road3d_pts, sizeof(double));
  road3d_xl = (double *) CR_calloc(num_road3d_pts, sizeof(double));
  road3d_yl = (double *) CR_calloc(num_road3d_pts, sizeof(double));
  road3d_xr = (double *) CR_calloc(num_road3d_pts, sizeof(double));
  road3d_yr = (double *) CR_calloc(num_road3d_pts, sizeof(double));
  road3d_a = t3d[0];
  road3d_b = t3d[1];
  road3d_c = t3d[2];
  road3d_t = 0.0;

  CR_flush_printf("f");

  track_3d_frame = 0;
  track_3d_switch_frame = 20;

  good_road = TRUE;
  filtered_good_road = TRUE;
  // iv2003
  //  good_road_initialized = FALSE;
  good_road_initialized = TRUE;
  good_road_check_interval = 5;

  good_road_kal = load_CR_Kalman("good_road.s");

  good_road_Z = make_CR_Matrix("Z", 1, 1);
  good_road_Z_array = make_CR_Matrix_array(1);
  good_road_Z_array[0] = good_road_Z;

  CR_flush_printf("to here a");

  //  good_road_fp = fopen("good_road_rbf_0_03.out", "r");
//   good_road_fp = fopen("good_road.dat", "w");
//   fprintf(good_road_fp, "# movie = %s, first frame = %i, switch frame = %i, interval = %i, nn rows = %i, cols = %i\n", roadmov->name, track_3d_first_frame, track_3d_switch_frame, good_road_check_interval, track_3d_nn_rows, track_3d_nn_cols);

  // iv2003
  track_3d_mov = write_avi_initialize_CR_Movie(NULL, im->w, im->h, 30);

  // SVM stuff

  // iv2003
  /*
  good_road_svm = load_svmlight_model_SVM("good_road_rbf_0_03.mod");
  good_road_svm_input = make_CR_Vector("good_road_svm_input", track_3d_nn_rows * track_3d_nn_cols);

  road_nonroad_svm = load_svmlight_model_SVM("svm_road_train_rbf0.001.mod");
  road_nonroad_svm_input = make_CR_Vector("road_nonroad_svm_input", 27);

  track_3d_svm = make_CR_Matrix("track_3d_svm", track_3d_nn_rows, track_3d_nn_cols);
  */

  // finish
  
  return im;
}

//----------------------------------------------------------------------------

/*
void draw_top_view(int x, int y, CR_Kalman *kal)
{
  int vwidth, vlength;

  vwidth = 20;
  vlength = 40;

  ogl_draw_rectangle(int x1, int y1, int x2, int y2, int r, int g, int b)

}
*/

//----------------------------------------------------------------------------

CR_Kalman *initialize_edge_tracker_from_nn(char *filename, CR_Image *im)
{
  int i;
  CR_Kalman *k;

  run_neural_net(im, track_3d_nn, track_3d_left_margin, track_3d_right_margin, track_3d_top_margin, track_3d_bottom_margin, track_3d_nn_x_interval, track_3d_nn_y_interval, TRUE);

  biggest_connected_component_CR_Matrix(TRUE, track_3d_nn, track_3d_nn_cc);

  Track3_extract_nn_edge(im, track_3d_nn_cc, track_3d_left_margin, track_3d_top_margin, track_3d_nn_x_interval, track_3d_nn_y_interval, TRUE);

  if (left_3d_nn_x) {  // assuming these are created and freed four at a time
    free_CR_Vector(left_3d_nn_x);
    free_CR_Vector(left_3d_nn_z);
    free_CR_Vector(right_3d_nn_x);
    free_CR_Vector(right_3d_nn_z);
    free_CR_Vector(left_3d_nn_z_ground);
    free_CR_Vector(right_3d_nn_z_ground);
  }

  left_3d_nn_x = copy_header_CR_Vector(left_nn_x);
  left_3d_nn_z = copy_header_CR_Vector(left_nn_y);
  right_3d_nn_x = copy_header_CR_Vector(right_nn_x);
  right_3d_nn_z = copy_header_CR_Vector(right_nn_y);
  left_3d_nn_z_ground = copy_header_CR_Vector(left_nn_y);
  right_3d_nn_z_ground = copy_header_CR_Vector(right_nn_y);

  Track3DQuadratic_project_to_ground_plane_CR_Vector(left_nn_x, left_nn_y, left_3d_nn_x, left_3d_nn_z, left_3d_nn_z_ground);
  Track3DQuadratic_project_to_ground_plane_CR_Vector(right_nn_x, right_nn_y, right_3d_nn_x, right_3d_nn_z, right_3d_nn_z_ground);

  left_3d_nn_params = polyfit_CR_Matrix(2, left_3d_nn_z_ground, left_3d_nn_x);
  right_3d_nn_params = polyfit_CR_Matrix(2, right_3d_nn_z_ground, right_3d_nn_x);
  center_3d_nn_params = copy_header_CR_Vector(left_3d_nn_params);

  for (i = 0; i < center_3d_nn_params->rows; i++)
    center_3d_nn_params->x[i] = 0.5 * (left_3d_nn_params->x[i] + right_3d_nn_params->x[i]);

  track_3d_quadratic_vars->x[0] = center_3d_nn_params->x[2];
  track_3d_quadratic_vars->x[1] = center_3d_nn_params->x[1];
  track_3d_quadratic_vars->x[2] = center_3d_nn_params->x[0];

  // initialize Kalman filter

  printf("second vars\n");
  print_CR_Vector(track_3d_quadratic_vars);

  if (filename) {
    current_X0 = NULL;
    return demo3_load_kalman(filename, track_3d_quadratic_vars);
  }
  else {
      k = copy_CR_Kalman(initial_track_3d_quadratic_kal);
      free_CR_FuncMatrix(current_X0);
      current_X0 = copy_CR_FuncMatrix(initial_X0);
      demo3_replace_ampvars_CR_FuncMatrix(current_X0, track_3d_quadratic_vars);
      free_CR_Matrix(k->x_last);
      k->x_last = copy_CR_Matrix_from_linear_CR_FuncMatrix(current_X0);
      //      demo3_replace_ampvars_CR_FuncMatrix(H, track_3d_quadratic_vars);
      return k;
  }
}

//----------------------------------------------------------------------------

// X = ax * z * z + bx * z + cx
// Y = -t3d[7] * cos(t3d[8]) + z * sin(t3d[8])
// Z = t3d[7] * sin(t3d[8]) + z * cos(t3d[8])

// x_im = 0.5 * t3d[11] + t3d[9] * X / Z
// y_im = 0.5 * t3d[12] + t3d[10] * Y / Z

// X = Z * (x_im - 0.5 * t3d[11]) / t3d[9]
// x_ground = x_3d

void Track3DQuadratic_project_to_ground_plane(double x_im, double y_im, double *x_3d, double *z_3d, double *z_ground)
{
  *z_ground = -t3d[7] * (y_im * sin(t3d[8]) - 0.5 * t3d[12] * sin(t3d[8]) - t3d[10] * cos(t3d[8])) / 
    (y_im * cos(t3d[8]) - 0.5 * t3d[12] * cos(t3d[8]) + t3d[10] * sin(t3d[8]));
  *z_3d = t3d[7] * sin(t3d[8]) + *z_ground * cos(t3d[8]);
  *x_3d = *z_3d * (x_im - 0.5 * t3d[11]) / t3d[9];

  //  CR_flush_printf("ground z = %lf, x = %lf, z = %lf\n", *z_ground, *x_3d, *z_3d);
}

//----------------------------------------------------------------------------

void Track3DQuadratic_project_to_ground_plane_CR_Vector(CR_Vector *x_im, CR_Vector *y_im, CR_Vector *x_3d, CR_Vector *z_3d, CR_Vector *z_ground)
{
  int i;

  for (i = 0; i < x_im->rows; i++) 
    Track3DQuadratic_project_to_ground_plane(x_im->x[i], y_im->x[i], &(x_3d->x[i]), &(z_3d->x[i]), &(z_ground->x[i]));
}

//----------------------------------------------------------------------------

void Track3DQuadratic_preprocess_nn_measurements(CR_Image *im, int do_draw)
{
  int i, lj, rj;

  // run image through neural net, thresholding output to get "road" pixels

  run_neural_net(roadmov->im, track_3d_nn, track_3d_left_margin, track_3d_right_margin, track_3d_top_margin, track_3d_bottom_margin, track_3d_nn_x_interval, track_3d_nn_y_interval, do_draw);  // TRUE

  // throw out all but biggest cc of road pixels

  biggest_connected_component_CR_Matrix(TRUE, track_3d_nn, track_3d_nn_cc);

  // find edges of biggest cc

  Track3_extract_nn_edge(roadmov->im, track_3d_nn_cc, track_3d_left_margin, track_3d_top_margin, track_3d_nn_x_interval, track_3d_nn_y_interval, TRUE);

  // fit quadratics to edges

  /*
  if (left_3d_nn_x) {  // assuming these are created and freed four at a time
    free_CR_Vector(left_3d_nn_x);
    free_CR_Vector(left_3d_nn_z);
    free_CR_Vector(right_3d_nn_x);
    free_CR_Vector(right_3d_nn_z);
    free_CR_Vector(left_3d_nn_z_ground);
    free_CR_Vector(right_3d_nn_z_ground);
  }

  left_3d_nn_x = copy_header_CR_Vector(left_nn_x);
  left_3d_nn_z = copy_header_CR_Vector(left_nn_y);
  right_3d_nn_x = copy_header_CR_Vector(right_nn_x);
  right_3d_nn_z = copy_header_CR_Vector(right_nn_y);
  left_3d_nn_z_ground = copy_header_CR_Vector(left_nn_y);
  right_3d_nn_z_ground = copy_header_CR_Vector(right_nn_y);
  
  Track3DQuadratic_project_to_ground_plane_CR_Vector(left_nn_x, left_nn_y, left_3d_nn_x, left_3d_nn_z, left_3d_nn_z_ground);
  Track3DQuadratic_project_to_ground_plane_CR_Vector(right_nn_x, right_nn_y, right_3d_nn_x, right_3d_nn_z, right_3d_nn_z_ground);

//   print_CR_Vector(left_3d_nn_x);
//   print_CR_Vector(left_3d_nn_z_ground);
//   print_CR_Vector(right_3d_nn_x);
//   print_CR_Vector(right_3d_nn_z_ground);

  left_3d_nn_params = polyfit_CR_Matrix(2, left_3d_nn_z_ground, left_3d_nn_x);
  right_3d_nn_params = polyfit_CR_Matrix(2, right_3d_nn_z_ground, right_3d_nn_x);

  center_3d_nn_params = copy_header_CR_Vector(left_3d_nn_params);

  for (i = 0; i < center_3d_nn_params->rows; i++)
    center_3d_nn_params->x[i] = 0.5 * (left_3d_nn_params->x[i] + right_3d_nn_params->x[i]);

  print_CR_Vector(center_3d_nn_params);
  //  print_CR_Vector(right_3d_nn_params);

  double ax, bx, cx, z;

  ax = center_3d_nn_params->x[2];
  bx = center_3d_nn_params->x[1];
  cx = center_3d_nn_params->x[0];

  for (i = 0; i < num_road3d_pts; i++) {
    //    z = road3d_delta * (double) i;
    z = zz[i];

    road3d_xc[i] = t3d[11] / 2 + t3d[9] * (ax * z * z + bx * z + cx) / (t3d[7] * sin(t3d[8]) + z * cos(t3d[8]));
    road3d_yc[i] = t3d[12] / 2 + t3d[10] * (-t3d[7] * cos(t3d[8]) + z * sin(t3d[8])) /
      (t3d[7] * sin(t3d[8]) + z * cos(t3d[8]));

//     double x_3d, z_3d;
// 
//     Track3DQuadratic_project_to_ground_plane(road3d_xc[i], t3d[12] - road3d_yc[i], &x_3d, &z_3d, &z_ground);
//     printf("real ground z = %lf\n", z);

    road3d_xl[i] = t3d[11] / 2 + t3d[9] * (ax * z * z + bx * z + cx - t3d[6] * 1 / sqrt(pow(2 * ax * z + bx, 2) + 1)) /
      (t3d[7] * sin(t3d[8]) + (z + t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * cos(t3d[8]));

    road3d_yl[i] = t3d[12] / 2 + t3d[10] * (-t3d[7] * cos(t3d[8]) + (z + t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * sin(t3d[8])) /
      (t3d[7] * sin(t3d[8]) + (z + t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * cos(t3d[8]));

    road3d_xr[i] = t3d[11] / 2 + t3d[9] * (ax * z * z + bx * z + cx + t3d[6] * 1 / sqrt(pow(2 * ax * z + bx, 2) + 1)) /
      (t3d[7] * sin(t3d[8]) + (z - t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * cos(t3d[8]));

    road3d_yr[i] = t3d[12] / 2 + t3d[10] * (-t3d[7] * cos(t3d[8]) + (z - t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * sin(t3d[8])) /
      (t3d[7] * sin(t3d[8]) + (z - t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * cos(t3d[8]));

    //    printf("%i: cy = %lf, ly = %lf\n", i, road3d_yc[i], road3d_yl[i]);

    //    ff = sym('rad * (2 * ax * z + bx) / sqrt((2 * ax * z + bx)^2 + 1)');
  }
  */
    
//   ogl_draw_line_strip_d(road3d_xc, road3d_yc, num_road3d_pts, 0, 255, 0);
//   ogl_draw_line_strip_d(road3d_xl, road3d_yl, num_road3d_pts, 255, 0, 0);
//   ogl_draw_line_strip_d(road3d_xr, road3d_yr, num_road3d_pts, 0, 0, 255);

  // format found edges for passing to Kalman filter

//   print_CR_Vector(left_nn_x);
//   print_CR_Vector(left_goodrow);

  //  for (i = 0; i < track_3d_quadratic_kal_nn->z_pred->rows; i += 4) {

  /*
  for (i = 0, lj = left_nn_x->rows - 1, rj = right_nn_x->rows - 1; i < track_target_Z_nn->rows; i += 2) {

    if (left_goodrow->x[track3_scans - i / 2 - 1]) {
      MATRC(track_target_Z_nn, i, 0) = left_nn_x->x[lj];  
      lj--;
    }
    else
      MATRC(track_target_Z_nn, i, 0) = MATRC(track_target_Kal_nn->z_pred, i, 0);

    if (right_goodrow->x[track3_scans - i / 2 - 1]) {
      MATRC(track_target_Z_nn, i + 1, 0) = right_nn_x->x[rj];  
      rj--;
    }
    else 
      MATRC(track_target_Z_nn, i + 1, 0) = MATRC(track_target_Kal_nn->z_pred, i + 1, 0);
  }
  */

  //  print_CR_Matrix(track_target_Z_nn);
  //  CR_exit(1);
}

//----------------------------------------------------------------------------

void Track3DQuadratic_get_nn_measurements(CR_Image *im)
{
  int i, j, k, i_delta;
  int left, right, center_x, center_y, start_x, stop_x;
  int edge_strength, max_edge_strength, test_x, max_x;
  double frac;
  double x, z, imx, imy, search_window_radius, z_ground;

  CR_error("Track3DQuadratic_get_nn_measurements(): not yet ready");

  // these are bogus numbers
  start_x = stop_x = 0;

  for (i = 0; i < track_3d_quadratic_kal_nn->z_pred->rows; i += 4) {

    // both sides of road

    for (i_delta = 0; i_delta <= 2; i_delta += 2) {

      center_x = (int) CR_round(MATRC(track_3d_quadratic_kal_nn->z_pred, i + i_delta, 0));
      center_y = (int) CR_round(MATRC(track_3d_quadratic_kal_nn->z_pred, i + 1 + i_delta, 0));

      CR_error("fill in correct rows for goodrow");

      // check whether current scanline is within margins and is a "good row"; else, use predicted measurement
      if (center_y < track_3d_top_margin || center_y > track_3d_bottom_margin ||
	  (i_delta == 0 && !left_goodrow->x[0]) || (i_delta == 2 && !right_goodrow->x[0])) {

	MATRC(track_3d_quadratic_Z, i + i_delta, 0) = MATRC(track_3d_quadratic_kal_nn->z_pred, i + i_delta, 0);
	MATRC(track_3d_quadratic_Z, i + 1 + i_delta, 0) = MATRC(track_3d_quadratic_kal_nn->z_pred, i + 1 + i_delta, 0);
      }
      else {  // find edge on scanline center_y (which we already know is there from goodrow)

	for (j = 0; j < num_road3d_pts - 1; j++) {

	  if (road3d_yl[j] >= center_y && road3d_yl[j + 1] <= center_y) {
	    //  printf("left %i: yhigh = %lf, ylow = %lf, xhigh = %lf, xlow = %lf\n", j, road3d_yl[j], road3d_yl[j + 1], road3d_xl[j], road3d_xl[j + 1]);
	    frac = ((double) center_y - road3d_yl[j + 1]) / (road3d_yl[j] - road3d_yl[j + 1]);
	    test_x = (int) CR_round(road3d_xl[j + 1] + frac * (road3d_xl[j] - road3d_xl[j + 1]));
	    //	    printf("frac = %lf, x = %i\n", frac, test_x);
	    if (test_x >= start_x && test_x <= stop_x) {
	      max_x = test_x;
	      max_edge_strength = 1;
	    }
	  }

	  if (road3d_yr[j] >= center_y && road3d_yr[j + 1] <= center_y) {
	    //	    printf("\nright %i: yhigh = %lf, ylow = %lf, xhigh = %lf, xlow = %lf\n", j, road3d_yr[j], road3d_yr[j + 1], road3d_xr[j], road3d_xr[j + 1]);
	    frac = ((double) center_y - road3d_yr[j + 1]) / (road3d_yr[j] - road3d_yr[j + 1]);
	    test_x = (int) CR_round(road3d_xr[j + 1] + frac * (road3d_xr[j] - road3d_xr[j + 1]));
	    //	    printf("frac = %lf, x = %i\n", frac, test_x);
	    if (test_x >= start_x && test_x <= stop_x) {
	      max_x = test_x;
	      max_edge_strength = 1;
	    }
	  }
	}
	//	printf("--------------------\n");

	// strongest edge is measurement...
	if (max_edge_strength) {
	  MATRC(track_3d_quadratic_Z, i + i_delta, 0) = (double) max_x;
	  MATRC(track_3d_quadratic_Z, i + 1 + i_delta, 0) = (double) center_y;
	}
	// ...unless no good edge is found, use predicted measurement
	else {
	  MATRC(track_3d_quadratic_Z, i + i_delta, 0) = MATRC(track_3d_quadratic_kal_nn->z_pred, i + i_delta, 0);
	  MATRC(track_3d_quadratic_Z, i + 1 + i_delta, 0) = MATRC(track_3d_quadratic_kal_nn->z_pred, i + 1 + i_delta, 0);
	}
      }
    }
  }

}

//----------------------------------------------------------------------------

/*
  for (i = 0; i < Z->rows / 4; i++) {

    next = i + 1;
    last = i - 1;
    if (i == 0)
      last = i;
    else if (i == Z->rows / 4 - 1)
      next = i;

    xdelta = MATRC(track_3d_quadratic_kal->z_pred, 4 * next, 0) - MATRC(track_3d_quadratic_kal->z_pred, 4 * last, 0);
    ydelta = MATRC(track_3d_quadratic_kal->z_pred, 4 * next + 1, 0) - MATRC(track_3d_quadratic_kal->z_pred, 4 * last + 1, 0);
    mag = sqrt(xdelta * xdelta + ydelta * ydelta);
    dx = ydelta / mag;
    dy = -xdelta / mag;

    xledge[2 * i] = MATRC(track_3d_quadratic_kal->z_pred, 4 * i, 0) - dx * track_3d_quadratic_edge_window_radius;
    yledge[2 * i] = im->h - (MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 1, 0) - dy * track_3d_quadratic_edge_window_radius);
    xledge[2 * i + 1] = MATRC(track_3d_quadratic_kal->z_pred, 4 * i, 0) + dx * track_3d_quadratic_edge_window_radius;
    yledge[2 * i + 1] = im->h - (MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 1, 0) + dy * track_3d_quadratic_edge_window_radius);

    xdelta = MATRC(track_3d_quadratic_kal->z_pred, 4 * next + 2, 0) - MATRC(track_3d_quadratic_kal->z_pred, 4 * last + 2, 0);
    ydelta = MATRC(track_3d_quadratic_kal->z_pred, 4 * next + 3, 0) - MATRC(track_3d_quadratic_kal->z_pred, 4 * last + 3, 0);
    mag = sqrt(xdelta * xdelta + ydelta * ydelta);
    dx = ydelta / mag;
    dy = -xdelta / mag;

    xredge[2 * i] = MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 2, 0) - dx * track_3d_quadratic_edge_window_radius;
    yredge[2 * i] = im->h - (MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 3, 0) - dy * track_3d_quadratic_edge_window_radius);
    xredge[2 * i + 1] = MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 2, 0) + dx * track_3d_quadratic_edge_window_radius;
    yredge[2 * i + 1] = im->h - (MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 3, 0) + dy * track_3d_quadratic_edge_window_radius);
  }

  ogl_draw_lines_d(xledge, yledge, Z->rows, 255, 255, 255);
  ogl_draw_lines_d(xredge, yredge, Z->rows, 255, 255, 255);
*/

// strongest edge in search window orthogonal to curve

void Track3DQuadratic_get_ortho_measurements(CR_Image *im)
{
  int i, j, k, i_delta;
  int center_x, center_y, start_x, stop_x, start_y, stop_y;
  int edge_strength, max_edge_strength, max_x, max_y, curmax;
  int next, last, left, right, top, bottom, part_in, threshold;
  double r, x, y, xdelta, ydelta, mag, dx, dy, sign, x_3d, z_3d, z_ground;

  track_3d_quadratic_num_good_Z = 0;

  r = track_3d_quadratic_edge_window_radius;
  threshold = track_3d_quadratic_thresh_edge_strength;
  left = track_3d_left_margin;
  right = track_3d_right_margin;
  top = track_3d_top_margin;
  bottom = track_3d_bottom_margin;

  for (i = 0; i < track_3d_quadratic_kal->z_pred->rows; i += 4) {

    for (i_delta = 0; i_delta <= 2; i_delta += 2) {  // i_delta = 0 is left side, i_delta = 2 is right 

      //      CR_flush_printf("i = %i, i_delta = %i\n", i, i_delta);

      // estimate normal to curve 

      next = i + i_delta + 4;
      last = i + i_delta - 4;
      if (i == 0)
	last = i + i_delta;
      //      else if (i == track_3d_quadratic_kal->z_pred->rows - (4 - i_delta))
      else if (i == track_3d_quadratic_kal->z_pred->rows - 4)
	next = i + i_delta;

      //      CR_flush_printf("next = %i, last = %i\n", next, last);

      xdelta = MATRC(track_3d_quadratic_kal->z_pred, next, 0) - MATRC(track_3d_quadratic_kal->z_pred, last, 0);
      ydelta = MATRC(track_3d_quadratic_kal->z_pred, next + 1, 0) - MATRC(track_3d_quadratic_kal->z_pred, last + 1, 0);
      mag = sqrt(xdelta * xdelta + ydelta * ydelta);

      //      CR_flush_printf("mag = %lf\n", mag);

      dx = ydelta / mag;
      dy = -xdelta / mag;

      // set endpoints of line for edge processing

      x = MATRC(track_3d_quadratic_kal->z_pred, i + i_delta, 0);
      y = MATRC(track_3d_quadratic_kal->z_pred, i + i_delta + 1, 0);

      if (track_3d_depth_dependent_search) {
	Track3DQuadratic_project_to_ground_plane(x, y, &x_3d, &z_3d, &z_ground);
	r = t3d[9] * (double) track_3d_window_radius / z_3d;
	//	printf("z_3d = %lf, t3d[9] = %lf, rad = %lf, r = %lf\n", z_3d, t3d[9], (double) track_3d_window_radius,  r);
      }

      // start end of line should be in road region, stop end should be outside road region

      sign = i_delta ? -1.0 : 1.0;

      start_x = CR_round(x - sign * dx * r);
      start_y = CR_round(y - sign * dy * r);
      stop_x = CR_round(x + sign * dx * r);
      stop_y = CR_round(y + sign * dy * r);

      //      CR_flush_printf("clipping\n");

      part_in = clip_line_to_rect_CR_Image(&start_x, &start_y, &stop_x, &stop_y, left, top, right, bottom);

      //      CR_flush_printf("clipped\n");

      // check whether edge window is entirely outside image; if so, use predicted measurement

      if (!part_in) {
 	MATRC(track_3d_quadratic_Z, i + i_delta, 0) = MATRC(track_3d_quadratic_kal->z_pred, i + i_delta, 0);
 	MATRC(track_3d_quadratic_Z, i + 1 + i_delta, 0) = MATRC(track_3d_quadratic_kal->z_pred, i + 1 + i_delta, 0);
 	track_3d_quadratic_good_Z->x[i + i_delta] = track_3d_quadratic_good_Z->x[i + 1 + i_delta] = FALSE;
      }
      else {

	best_sobel_edge_along_line_CR_Image(start_x, start_y, stop_x, stop_y, im, &curmax, &max_x, &max_y, threshold);

	// strongest edge over threshold is measurement...

	if (curmax) {
	  MATRC(track_3d_quadratic_Z, i + i_delta, 0) = (double) max_x;
	  MATRC(track_3d_quadratic_Z, i + 1 + i_delta, 0) = (double) max_y;
	  track_3d_quadratic_good_Z->x[i + i_delta] = track_3d_quadratic_good_Z->x[i + 1 + i_delta] = TRUE;
	  track_3d_quadratic_num_good_Z++;
	}

	// ...unless no good edge is found, use predicted measurement

	else {
	  MATRC(track_3d_quadratic_Z, i + i_delta, 0) = MATRC(track_3d_quadratic_kal->z_pred, i + i_delta, 0);
	  MATRC(track_3d_quadratic_Z, i + 1 + i_delta, 0) = MATRC(track_3d_quadratic_kal->z_pred, i + 1 + i_delta, 0);
	  track_3d_quadratic_good_Z->x[i + i_delta] = track_3d_quadratic_good_Z->x[i + 1 + i_delta] = FALSE;
	}
      }
    }
  }
}

//----------------------------------------------------------------------------

// strongest edge in horizontal search window

void Track3DQuadratic_get_measurements(CR_Image *im)
{
  int i, j, k, i_delta;
  int left, right, center_x, center_y, start_x, stop_x;
  int edge_strength, max_edge_strength, max_x;

  track_3d_quadratic_num_good_Z = 0;

  //  print_CR_Matrix(track_3d_quadratic_kal->z_pred);

  // sobel 

  for (i = 0; i < track_3d_quadratic_kal->z_pred->rows; i += 4) {

    //    CR_flush_printf("i = %i\n", i);

    // both sides of road

    for (i_delta = 0; i_delta <= 2; i_delta += 2) {

      //      CR_flush_printf("i_delta = %i\n", i_delta);

      center_x = (int) CR_round(MATRC(track_3d_quadratic_kal->z_pred, i + i_delta, 0));
      center_y = (int) CR_round(MATRC(track_3d_quadratic_kal->z_pred, i + 1 + i_delta, 0));

      // use best_sobel_edge_along_line_CR_Image()???

      //      CR_flush_printf("center_x = %i, center_y = %i\n", center_x, center_y);

      start_x = CR_clamp(center_x - track_3d_quadratic_edge_window_radius, track_3d_left_margin, track_3d_right_margin);
      stop_x = CR_clamp(center_x + track_3d_quadratic_edge_window_radius, track_3d_left_margin, track_3d_right_margin);

      //      CR_flush_printf("start_x = %i, stop_x = %i\n", start_x, stop_x);

      // check whether edge window is entirely outside image; if so, use predicted measurement
      if (start_x == stop_x || center_y < track_3d_top_margin || center_y > track_3d_bottom_margin) {
	//	CR_flush_printf("all outside\n");
	MATRC(track_3d_quadratic_Z, i + i_delta, 0) = MATRC(track_3d_quadratic_kal->z_pred, i + i_delta, 0);
	MATRC(track_3d_quadratic_Z, i + 1 + i_delta, 0) = MATRC(track_3d_quadratic_kal->z_pred, i + 1 + i_delta, 0);
	track_3d_quadratic_good_Z->x[i + i_delta] = track_3d_quadratic_good_Z->x[i + 1 + i_delta] = FALSE;
      }
      else {
	//	CR_flush_printf("some inside\n");
	max_x = start_x;
	max_edge_strength = thresholded_point_sobel_3x3(start_x, center_y, track_3d_quadratic_thresh_edge_strength, im);
	// find strongest edge; use as measurement
	for (k = start_x + 1; k <= stop_x; k++) {
	  edge_strength = thresholded_point_sobel_3x3(k, center_y, track_3d_quadratic_thresh_edge_strength, im);
	  if (edge_strength > max_edge_strength) {
	    max_x = k;
	    max_edge_strength = edge_strength;
	  }
	}
	// strongest edge is measurement...
	if (max_edge_strength) {
	  MATRC(track_3d_quadratic_Z, i + i_delta, 0) = (double) max_x;
	  MATRC(track_3d_quadratic_Z, i + 1 + i_delta, 0) = (double) center_y;
	  track_3d_quadratic_good_Z->x[i + i_delta] = track_3d_quadratic_good_Z->x[i + 1 + i_delta] = TRUE;
	  track_3d_quadratic_num_good_Z++;
	}
	// ...unless no good edge is found, use predicted measurement
	else {
	  MATRC(track_3d_quadratic_Z, i + i_delta, 0) = MATRC(track_3d_quadratic_kal->z_pred, i + i_delta, 0);
	  MATRC(track_3d_quadratic_Z, i + 1 + i_delta, 0) = MATRC(track_3d_quadratic_kal->z_pred, i + 1 + i_delta, 0);
	  track_3d_quadratic_good_Z->x[i + i_delta] = track_3d_quadratic_good_Z->x[i + 1 + i_delta] = FALSE;
	}
      }
    }
  }
}

//----------------------------------------------------------------------------

void Track3DQuadratic_get_fake_measurements(CR_Image *im)
{
  int i, j, k, i_delta;
  int left, right, center_x, center_y, start_x, stop_x;
  int edge_strength, max_edge_strength, test_x, max_x;
  double frac;
  double x, z, imx, imy, search_window_radius, z_ground;

  track_3d_quadratic_num_good_Z = 0;

  //  print_CR_Matrix(track_3d_quadratic_kal->z_pred);

  // sobel 
  
  //  printf("pred rows = %i\n", track_3d_quadratic_kal->z_pred->rows);

  for (i = 0; i < track_3d_quadratic_kal->z_pred->rows; i += 4) {

    //    CR_flush_printf("i = %i\n", i);

    // both sides of road

    for (i_delta = 0; i_delta <= 2; i_delta += 2) {

      //      CR_flush_printf("i_delta = %i\n", i_delta);

      center_x = (int) CR_round(MATRC(track_3d_quadratic_kal->z_pred, i + i_delta, 0));
      center_y = (int) CR_round(MATRC(track_3d_quadratic_kal->z_pred, i + 1 + i_delta, 0));

      // use best_sobel_edge_along_line_CR_Image()???

      //      CR_flush_printf("center_x = %i, center_y = %i\n", center_x, center_y);

      if (track_3d_depth_dependent_search) {
	imx = MATRC(track_3d_quadratic_kal->z_pred, i + i_delta, 0);
	imy = MATRC(track_3d_quadratic_kal->z_pred, i + 1 + i_delta, 0);
	Track3DQuadratic_project_to_ground_plane(imx, imy, &x, &z, &z_ground);
	//	z = -t3d[7] * (imy * sin(t3d[8]) - 0.5 * t3d[12] * sin(t3d[8]) - t3d[10] * cos(t3d[8])) / 
	//	  (imy * cos(t3d[8]) - 0.5 * t3d[12] * cos(t3d[8]) + t3d[10] * sin(t3d[8]));
	search_window_radius = t3d[9] * track_3d_window_radius / z;
      }
      else
	search_window_radius = track_3d_quadratic_edge_window_radius;

      start_x = CR_clamp(center_x - search_window_radius, track_3d_left_margin, track_3d_right_margin);
      stop_x = CR_clamp(center_x + search_window_radius, track_3d_left_margin, track_3d_right_margin);

      //      CR_flush_printf("start_x = %i, stop_x = %i\n", start_x, stop_x);

      // check whether edge window is entirely outside image; if so, use predicted measurement
      if (start_x == stop_x || center_y < track_3d_top_margin || center_y > track_3d_bottom_margin) {

	//	printf("all outside y = %i between %i and %i\n", center_y, start_x, stop_x);
	//	printf("--------------------\n");

	//	CR_flush_printf("all outside\n");
	MATRC(track_3d_quadratic_Z, i + i_delta, 0) = MATRC(track_3d_quadratic_kal->z_pred, i + i_delta, 0);
	MATRC(track_3d_quadratic_Z, i + 1 + i_delta, 0) = MATRC(track_3d_quadratic_kal->z_pred, i + 1 + i_delta, 0);
	track_3d_quadratic_good_Z->x[i + i_delta] = track_3d_quadratic_good_Z->x[i + 1 + i_delta] = FALSE;
      }
      else {  // find edge at x = max_x in interval [start_x, stop_x] on scanline center_y; if no edge found, max_edge_strength = 0
	// ???
	max_edge_strength = 0;

	//	printf("searching y = %i between %i and %i\n", center_y, start_x, stop_x);
	for (j = 0; j < num_road3d_pts - 1; j++) {

	  if (road3d_yl[j] >= center_y && road3d_yl[j + 1] <= center_y) {
	    //  printf("left %i: yhigh = %lf, ylow = %lf, xhigh = %lf, xlow = %lf\n", j, road3d_yl[j], road3d_yl[j + 1], road3d_xl[j], road3d_xl[j + 1]);
	    frac = ((double) center_y - road3d_yl[j + 1]) / (road3d_yl[j] - road3d_yl[j + 1]);
	    test_x = (int) CR_round(road3d_xl[j + 1] + frac * (road3d_xl[j] - road3d_xl[j + 1]));
	    //	    printf("frac = %lf, x = %i\n", frac, test_x);
	    if (test_x >= start_x && test_x <= stop_x) {
	      max_x = test_x;
	      max_edge_strength = 1;
	    }
	  }

	  if (road3d_yr[j] >= center_y && road3d_yr[j + 1] <= center_y) {
	    //	    printf("\nright %i: yhigh = %lf, ylow = %lf, xhigh = %lf, xlow = %lf\n", j, road3d_yr[j], road3d_yr[j + 1], road3d_xr[j], road3d_xr[j + 1]);
	    frac = ((double) center_y - road3d_yr[j + 1]) / (road3d_yr[j] - road3d_yr[j + 1]);
	    test_x = (int) CR_round(road3d_xr[j + 1] + frac * (road3d_xr[j] - road3d_xr[j + 1]));
	    //	    printf("frac = %lf, x = %i\n", frac, test_x);
	    if (test_x >= start_x && test_x <= stop_x) {
	      max_x = test_x;
	      max_edge_strength = 1;
	    }
	  }
	}
	//	printf("--------------------\n");

	// strongest edge is measurement...
	if (max_edge_strength) {
	  MATRC(track_3d_quadratic_Z, i + i_delta, 0) = (double) max_x;
	  MATRC(track_3d_quadratic_Z, i + 1 + i_delta, 0) = (double) center_y;
	  track_3d_quadratic_good_Z->x[i + i_delta] = track_3d_quadratic_good_Z->x[i + 1 + i_delta] = TRUE;
	  track_3d_quadratic_num_good_Z++;
	}
	// ...unless no good edge is found, use predicted measurement
	else {
	  MATRC(track_3d_quadratic_Z, i + i_delta, 0) = MATRC(track_3d_quadratic_kal->z_pred, i + i_delta, 0);
	  MATRC(track_3d_quadratic_Z, i + 1 + i_delta, 0) = MATRC(track_3d_quadratic_kal->z_pred, i + 1 + i_delta, 0);
	  track_3d_quadratic_good_Z->x[i + i_delta] = track_3d_quadratic_good_Z->x[i + 1 + i_delta] = FALSE;
	}
      }
    }
  }
}

//----------------------------------------------------------------------------

void Track3DQuadratic_draw_measurements(CR_Image *im, CR_Matrix *Z, CR_Vector *good_Z)
{
  double *x, *y, *xledge, *yledge, *xredge, *yredge;
  int i, num_bads, num_goods, next, last;
  double xdelta, ydelta, dx, dy, mag;
  double imxl, imxr, imyl, imyr, zl, zr, search_window_radius, zc, imyc, xl, xr, zl_ground, zr_ground;
  double imx, imy, x_3d, z_3d, z_ground, r;

  r = track_3d_quadratic_edge_window_radius;

  // draw linear search windows that dots below came from

  xledge = (double *) CR_calloc(Z->rows, sizeof(double));
  yledge = (double *) CR_calloc(Z->rows, sizeof(double));
  xredge = (double *) CR_calloc(Z->rows, sizeof(double));
  yredge = (double *) CR_calloc(Z->rows, sizeof(double));

  /*

  for (i = 0; i < Z->rows / 4; i++) {

    if (track_3d_depth_dependent_search) {
      imxl = MATRC(track_3d_quadratic_kal->z_pred, 4 * i, 0);
      imyl = MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 1, 0);
      Track3DQuadratic_project_to_ground_plane(imxl, imyl, &xl, &zl, &zl_ground);
      search_window_radius = t3d[9] * track_3d_window_radius / zl;
    }
    else
      search_window_radius = r;

      imyc = road3d_yc[i];
      zc = -t3d[7] * (imyc * sin(t3d[8]) - 0.5 * t3d[12] * sin(t3d[8]) - t3d[10] * cos(t3d[8])) / 
	(imyc * cos(t3d[8]) - 0.5 * t3d[12] * cos(t3d[8]) + t3d[10] * sin(t3d[8]));

    xledge[2 * i] = MATRC(track_3d_quadratic_kal->z_pred, 4 * i, 0) - search_window_radius;
    yledge[2 * i] = im->h - MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 1, 0);
    xledge[2 * i + 1] = MATRC(track_3d_quadratic_kal->z_pred, 4 * i, 0) + search_window_radius;
    yledge[2 * i + 1] = im->h - MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 1, 0);

    if (track_3d_depth_dependent_search) {
      imxr = MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 2, 0);
      imyr = MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 3, 0);
      Track3DQuadratic_project_to_ground_plane(imxr, imyr, &xr, &zr, &zr_ground);
      search_window_radius = t3d[9] * track_3d_window_radius / zr;
    }
    else
      search_window_radius = r;

    xredge[2 * i] = MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 2, 0) - search_window_radius;
    yredge[2 * i] = im->h - MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 3, 0);
    xredge[2 * i + 1] = MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 2, 0) + search_window_radius;
    yredge[2 * i + 1] = im->h - MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 3, 0);
  }

  ogl_draw_lines_d(xledge, yledge, Z->rows, 255, 255, 255);
  ogl_draw_lines_d(xredge, yredge, Z->rows, 255, 255, 255);
  */

  // lines orthogonal to road edge curves

  for (i = 0; i < Z->rows / 4; i++) {

    next = i + 1;
    last = i - 1;
    if (i == 0)
      last = i;
    else if (i == Z->rows / 4 - 1)
      next = i;

    xdelta = MATRC(track_3d_quadratic_kal->z_pred, 4 * next, 0) - MATRC(track_3d_quadratic_kal->z_pred, 4 * last, 0);
    ydelta = MATRC(track_3d_quadratic_kal->z_pred, 4 * next + 1, 0) - MATRC(track_3d_quadratic_kal->z_pred, 4 * last + 1, 0);
    mag = sqrt(xdelta * xdelta + ydelta * ydelta);
    dx = ydelta / mag;
    dy = -xdelta / mag;

    imx = MATRC(track_3d_quadratic_kal->z_pred, 4 * i, 0);
    imy = MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 1, 0);

    if (track_3d_depth_dependent_search) {
      Track3DQuadratic_project_to_ground_plane(imx, imy, &x_3d, &z_3d, &z_ground);
      r = t3d[9] * (double) track_3d_window_radius / z_3d;
    }

    xledge[2 * i] = MATRC(track_3d_quadratic_kal->z_pred, 4 * i, 0) - dx * r;
    yledge[2 * i] = im->h - (MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 1, 0) - dy * r);
    xledge[2 * i + 1] = MATRC(track_3d_quadratic_kal->z_pred, 4 * i, 0) + dx * r;
    yledge[2 * i + 1] = im->h - (MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 1, 0) + dy * r);

    xdelta = MATRC(track_3d_quadratic_kal->z_pred, 4 * next + 2, 0) - MATRC(track_3d_quadratic_kal->z_pred, 4 * last + 2, 0);
    ydelta = MATRC(track_3d_quadratic_kal->z_pred, 4 * next + 3, 0) - MATRC(track_3d_quadratic_kal->z_pred, 4 * last + 3, 0);
    mag = sqrt(xdelta * xdelta + ydelta * ydelta);
    dx = ydelta / mag;
    dy = -xdelta / mag;

    imx = MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 2, 0);
    imy = MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 3, 0);

    if (track_3d_depth_dependent_search) {
      Track3DQuadratic_project_to_ground_plane(imx, imy, &x_3d, &z_3d, &z_ground);
      r = t3d[9] * (double) track_3d_window_radius / z_3d;
    }

    xredge[2 * i] = MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 2, 0) - dx * r;
    yredge[2 * i] = im->h - (MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 3, 0) - dy * r);
    xredge[2 * i + 1] = MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 2, 0) + dx * r;
    yredge[2 * i + 1] = im->h - (MATRC(track_3d_quadratic_kal->z_pred, 4 * i + 3, 0) + dy * r);
  }

  ogl_draw_lines_d(xledge, yledge, Z->rows, 255, 255, 255);
  ogl_draw_lines_d(xredge, yredge, Z->rows, 255, 255, 255);

  // stop lines orthogonal to road edge curves

  // draw found edge locations as green dots (good) or red dots (bad)

  x = (double *) CR_calloc(Z->rows / 2, sizeof(double));
  y = (double *) CR_calloc(Z->rows / 2, sizeof(double));

  for (i = 0, num_bads = 0, num_goods = 0; i < Z->rows / 2; i++) {

    if (track_3d_quadratic_good_Z->x[2 * i]) {
      x[num_goods] = MATRC(Z, 2 * i, 0);
      y[num_goods] = im->h - MATRC(Z, 2 * i + 1, 0);
      num_goods++;
    }
    else {
      x[track_3d_quadratic_num_good_Z + num_bads] = MATRC(Z, 2 * i, 0);
      y[track_3d_quadratic_num_good_Z + num_bads] = im->h - MATRC(Z, 2 * i + 1, 0);
      num_bads++;
    }
  }

  ogl_draw_points_d(x, y, 4, track_3d_quadratic_num_good_Z, 0, 255, 0);
  ogl_draw_points_d(&x[track_3d_quadratic_num_good_Z], &y[track_3d_quadratic_num_good_Z], 4, num_bads, 255, 0, 0);

  //  ogl_draw_points_d(x, y, 3, track_3d_quadratic_num_good_Z, 0, 255, 0);


//   for (i = 0; i < Z->rows / 2; i++) {
//     x[i] = MATRC(track_3d_quadratic_kal->z_pred, 2 * i, 0);
//     y[i] = im->h - MATRC(track_3d_quadratic_kal->z_pred, 2 * i + 1, 0);
//   }
// 
//   ogl_draw_lines_d(x, y, Z->rows / 2, 255, 255, 0);

  // clean up

  // search windows
 CR_free_calloc(xledge, Z->rows, sizeof(double));
 CR_free_calloc(yledge, Z->rows, sizeof(double));
 CR_free_calloc(xredge, Z->rows, sizeof(double));
 CR_free_calloc(yredge, Z->rows, sizeof(double));

  // found edges
  CR_free_calloc(x, Z->rows / 2, sizeof(double));
  CR_free_calloc(y, Z->rows / 2, sizeof(double));
}

//----------------------------------------------------------------------------

void run_svm(CR_Image *im, CR_Matrix *svm_output, int start_x, int stop_x, int start_y, int stop_y, int x_delta, int y_delta, int do_draw) 
{
  int i, j, row, col;
  double svm_result;

  for (j = start_y, row = 0; j <= stop_y; j += y_delta, row++)
    for (i = start_x, col = 0; i <= stop_x; i += x_delta, col++) {
      Track3_fill_input_neural_net(i, j, road_nonroad_svm_input, im);
      svm_result = classify_pattern_SVM(road_nonroad_svm_input, road_nonroad_svm);
      if (svm_result >= 0) {
	MATRC(svm_output, row, col) = 1.0;
	if (do_draw)
	  ogl_draw_point(i, im->h - j, 3, 255, 128, 0);
      }
      else
	MATRC(svm_output, row, col) = 0.0;
    }
}

void canned_run_svm()
{
  int i, j, x;

  //  CR_Vector *track_3d_canned_svm = read_dlm_CR_Vector("svm3.cla", 13824);  // dirt road crossing
  CR_Vector *track_3d_canned_svm = read_dlm_CR_Vector("svm1.cla", 13824);  // first frame
  //  print_CR_Vector(track_3d_canned_svm);
  
  x = 0;
  for (j = 1; j < 480; j += 5)
    for (i = 1; i < 720; i += 5) {
      if (track_3d_canned_svm->x[x] >= 0)
	ogl_draw_point(i, 480 - j, 3, 255, 0, 255);
      x++;
    }
  //  printf("x = %i\n", x);
  //  exit(1);
}

//----------------------------------------------------------------------------

void Track3DQuadratic_draw_state_CR_CG(CR_Image *im, CR_Kalman *kal)
{
  int i;
  double z, ax, bx, cx;

  ax = MATRC(kal->x, 0, 0);
  bx = MATRC(kal->x, 1, 0);
  cx = MATRC(kal->x, 2, 0);

  for (i = 0; i < num_road3d_pts; i++) {
    z = 5.0 + road3d_delta * (double) i;  // + 1
    //z = zz[i];

    road3d_xc[i] = t3d[11] / 2 + t3d[9] * (ax * z * z + bx * z + cx) / (t3d[7] * sin(t3d[8]) + z * cos(t3d[8]));
    road3d_yc[i] = t3d[12] / 2 + t3d[10] * (-t3d[7] * cos(t3d[8]) + z * sin(t3d[8])) /
      (t3d[7] * sin(t3d[8]) + z * cos(t3d[8]));
    
    road3d_xl[i] = t3d[11] / 2 + t3d[9] * (ax * z * z + bx * z + cx - t3d[6] * 1 / sqrt(pow(2 * ax * z + bx, 2) + 1)) /
      (t3d[7] * sin(t3d[8]) + (z + t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * cos(t3d[8]));
    
    road3d_yl[i] = t3d[12] / 2 + t3d[10] * (-t3d[7] * cos(t3d[8]) + (z + t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * sin(t3d[8])) /
      (t3d[7] * sin(t3d[8]) + (z + t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * cos(t3d[8]));
    
    road3d_xr[i] = t3d[11] / 2 + t3d[9] * (ax * z * z + bx * z + cx + t3d[6] * 1 / sqrt(pow(2 * ax * z + bx, 2) + 1)) /
      (t3d[7] * sin(t3d[8]) + (z - t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * cos(t3d[8]));
    
    road3d_yr[i] = t3d[12] / 2 + t3d[10] * (-t3d[7] * cos(t3d[8]) + (z - t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * sin(t3d[8])) /
      (t3d[7] * sin(t3d[8]) + (z - t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * cos(t3d[8]));
  }

  for (i = 0; i < num_road3d_pts; i++) {

    road3d_xl[i] += -82.5;
    road3d_yl[i] += 15.5;
    road3d_xr[i] += -82.5;
    road3d_yr[i] += 15.5;

  }

  //  ogl_draw_line_strip_d(road3d_xc, road3d_yc, num_road3d_pts, 0, 255, 0);
//   ogl_draw_thick_line_strip_d(road3d_xl, road3d_yl, num_road3d_pts, 2, 255, 255, 255);
//   ogl_draw_thick_line_strip_d(road3d_xr, road3d_yr, num_road3d_pts, 2, 255, 255, 255);

  ogl_draw_thick_line_strip_d(road3d_xl, road3d_yl, num_road3d_pts, 4, 255, 255, 255);
  ogl_draw_thick_line_strip_d(road3d_xr, road3d_yr, num_road3d_pts, 4, 255, 255, 255);

  ogl_draw_thick_line_strip_d(road3d_xl, road3d_yl, num_road3d_pts, 2, 0, 0, 0);
  ogl_draw_thick_line_strip_d(road3d_xr, road3d_yr, num_road3d_pts, 2, 0, 0, 0);

}

//----------------------------------------------------------------------------

void Track3DQuadratic_grab_CR_CG(CR_Image *im)
{
  int i, j, index;
  double x, z, ax, bx, cx, imy, z_ground, good_road_result;
  double *v;

  // -------------------
  // draw current image
  // -------------------
  
  vflip_CR_Image(roadmov->im, roadim);
  glDrawPixels(roadim->w, roadim->h, GL_RGB, GL_UNSIGNED_BYTE, roadim->data);

  // -------------------
  // draw fake road
  // -------------------

  /*
  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT);
    
  //ax = road3d_a;
  ax = road3d_a + 0.01 * sin(road3d_t);
  bx = road3d_b;
  //  bx = road3d_b + 0.5 * sin(road3d_t);
  //  cx = road3d_c;
  cx = road3d_c + 5 * sin(road3d_t);
  //  printf("ax = %lf, bx = %lf, cx = %lf\n", ax, bx, cx);

  for (i = 0; i < num_road3d_pts; i++) {
    z = road3d_delta * (double) i;
    //z = zz[i];

    road3d_xc[i] = t3d[11] / 2 + t3d[9] * (ax * z * z + bx * z + cx) / (t3d[7] * sin(t3d[8]) + z * cos(t3d[8]));
    road3d_yc[i] = t3d[12] / 2 + t3d[10] * (-t3d[7] * cos(t3d[8]) + z * sin(t3d[8])) /
      (t3d[7] * sin(t3d[8]) + z * cos(t3d[8]));

//     double x_3d, z_3d;
// 
//     Track3DQuadratic_project_to_ground_plane(road3d_xc[i], t3d[12] - road3d_yc[i], &x_3d, &z_3d, &z_ground);
//     printf("real ground z = %lf\n", z);

    road3d_xl[i] = t3d[11] / 2 + t3d[9] * (ax * z * z + bx * z + cx - t3d[6] * 1 / sqrt(pow(2 * ax * z + bx, 2) + 1)) /
      (t3d[7] * sin(t3d[8]) + (z + t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * cos(t3d[8]));

    road3d_yl[i] = t3d[12] / 2 + t3d[10] * (-t3d[7] * cos(t3d[8]) + (z + t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * sin(t3d[8])) /
      (t3d[7] * sin(t3d[8]) + (z + t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * cos(t3d[8]));

    road3d_xr[i] = t3d[11] / 2 + t3d[9] * (ax * z * z + bx * z + cx + t3d[6] * 1 / sqrt(pow(2 * ax * z + bx, 2) + 1)) /
      (t3d[7] * sin(t3d[8]) + (z - t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * cos(t3d[8]));

    road3d_yr[i] = t3d[12] / 2 + t3d[10] * (-t3d[7] * cos(t3d[8]) + (z - t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * sin(t3d[8])) /
      (t3d[7] * sin(t3d[8]) + (z - t3d[6] * (2 * ax * z + bx) / sqrt(pow(2 * ax * z + bx, 2) + 1)) * cos(t3d[8]));

    //    printf("%i: cy = %lf, ly = %lf\n", i, road3d_yc[i], road3d_yl[i]);

    //    ff = sym('rad * (2 * ax * z + bx) / sqrt((2 * ax * z + bx)^2 + 1)');
  }
    
  ogl_draw_line_strip_d(road3d_xc, road3d_yc, num_road3d_pts, 0, 255, 0);
  ogl_draw_line_strip_d(road3d_xl, road3d_yl, num_road3d_pts, 255, 0, 0);
  ogl_draw_line_strip_d(road3d_xr, road3d_yr, num_road3d_pts, 0, 0, 255);

  // change from opengl drawing coords to image coords by flipping y coord.

  for (i = 0; i < num_road3d_pts; i++) {
    road3d_yc[i] = t3d[12] - road3d_yc[i]; 
    road3d_yl[i] = t3d[12] - road3d_yl[i]; 
    road3d_yr[i] = t3d[12] - road3d_yr[i]; 
    //    if (!(i % 5))
    //     printf("i = %i, xl = %.2lf, yl = %.2lf, xr = %.2lf, yr = %.2lf\n", i, road3d_xl[i], road3d_yl[i], road3d_xr[i], road3d_yr[i]);
  }

  road3d_t += 0.01;  // 0.01

  double imh;

  imh = (double) im->h;
  */
  
//   for (imy = 0; imy < imh; imy++) {
// 
//     z = (track_3d_quadratic_vars->x[7] * (-imy * sin(track_3d_quadratic_vars->x[8]) + track_3d_quadratic_vars->x[10] * cos(track_3d_quadratic_vars->x[8])) + 0.5 * imh) / (imy * cos(track_3d_quadratic_vars->x[8]) + track_3d_quadratic_vars->x[10] * sin(track_3d_quadratic_vars->x[8]));
//     if (!(((int) imy) % 5))
//       printf("imy = %lf, Z = %lf\n", imy, z);
// 
//   }

  //  exit(1);

  // ----------------------------
  // draw image processing limits
  // ----------------------------
    
  ogl_draw_rectangle(track_3d_left_margin, im->h - track_3d_top_margin, track_3d_right_margin, im->h - track_3d_bottom_margin, 255, 255, 255);

  // ----------------------
  // neural net stuff
  // ----------------------

  //  canned_run_svm();

  // get measurements
  
  int row, col;
  double road_type;

  //  CR_flush_printf("to here d");

  if (track_3d_frame >= track_3d_switch_frame && !(track_3d_frame % good_road_check_interval)) {

    good_road_initialized = TRUE;

    /*    
    fscanf(good_road_fp, "%lf\n", &road_type);
    if (road_type >= 0)
      //      printf("good road\n");
      good_road = TRUE;
    else
      //      printf("bad road\n");
      good_road = FALSE;

    if (!good_road)
    */

    // iv2003
    //    run_neural_net(roadmov->im, track_3d_nn, track_3d_left_margin, track_3d_right_margin, track_3d_top_margin, track_3d_bottom_margin, track_3d_nn_x_interval, track_3d_nn_y_interval, TRUE);  // TRUE

    //    run_svm(roadmov->im, track_3d_svm, track_3d_left_margin, track_3d_right_margin, track_3d_top_margin, track_3d_bottom_margin, track_3d_nn_x_interval, track_3d_nn_y_interval, TRUE);  // TRUE

    // iv2003
//     for (j = 0, index = 0; j < track_3d_nn->rows; j++)
//       for (i = 0; i < track_3d_nn->cols; i++)
// 	good_road_svm_input->x[index++] = MATRC(track_3d_nn, j, i);
//    
//     //    print_CR_Vector(good_road_svm_input);
// 
//     good_road_result = classify_pattern_SVM(good_road_svm_input, good_road_svm);
// 
//     if (good_road_result < 0)
//       good_road = FALSE;
//     else
//       good_road = TRUE;
// 
//     MATRC(good_road_Z, 0, 0) = good_road_result;
//     update_nonlinear_CR_Kalman(good_road_Z_array, 1, good_road_kal);
//     if (MATRC(good_road_kal->x, 0, 0) < 0.0)
//       filtered_good_road = FALSE;
//     else
//       filtered_good_road = TRUE;

    // collect data
    /*

    run_neural_net(roadmov->im, track_3d_nn, track_3d_left_margin, track_3d_right_margin, track_3d_top_margin, track_3d_bottom_margin, track_3d_nn_x_interval, track_3d_nn_y_interval, FALSE);  // TRUE
    //    ogl_draw_CR_Matrix(15, im->h - 30, im->h, track_3d_nn);  // 30

    if (good_road) {
      printf("good road!\n");
      fprintf(good_road_fp, "1 ");
    }
    else {
      printf("bad road!\n");
      fprintf(good_road_fp, "-1 ");
    }
    
    for (row = 0, i = 0; row < track_3d_nn->rows; row++)
      for (col = 0; col < track_3d_nn->cols; col++) {
	fprintf(good_road_fp, "%i:%.1lf ", i + 1, MATRC(track_3d_nn, row, col));
	i++;
      }
    fprintf(good_road_fp, "\n");
    */

  }

  if (good_road_initialized) {

    //  CR_flush_printf("to here e");

    // unfiltered classification

//     if (!good_road) 
//       ogl_draw_point(20, im->h - 20, 20, 255, 0, 0);
//     else 
//       ogl_draw_point(20, im->h - 20, 20, 0, 255, 0);

    // filtered classification

    //    int vheight = 20;
    int vheight = 200;

    // draw switching-related stuff

    /*
    if (!filtered_good_road) 
      ogl_draw_point(25, im->h - vheight, 20, 255, 0, 0);
    else {
      if (!track_3d_reinit_flag || track_3d_reinit_counter >= 30) 
	ogl_draw_point(25, im->h - vheight, 20, 0, 255, 0);
      else
	ogl_draw_point(25, im->h - vheight, 20, 255, 255, 0);
    }

    //    ogl_draw_CR_Matrix(40, vheight - 5, im->h, track_3d_nn);  // 30
    ogl_draw_scaled_CR_Matrix(3, 50, 185, im->h, track_3d_nn);  // 30
    //    ogl_draw_CR_Matrix(110, 15, im->h, track_3d_svm);  // 30
    */

  }

//   if (!(track_3d_frame % 10))
  // iv2003
//   if (track_3d_frame < track_3d_switch_frame)
//     Track3DQuadratic_preprocess_nn_measurements(roadmov->im, TRUE);

//  CR_flush_printf("to here f");

  /*
  //  if (track_3d_frame < 30) {
    for (j = track_3d_top_margin, row = 0; j <= track_3d_bottom_margin; j += track_3d_nn_y_interval, row++)
      for (i = track_3d_left_margin, col = 0; i <= track_3d_right_margin; i += track_3d_nn_x_interval, col++) 
	if (MATRC(track_3d_nn_cc, row, col))
	  ogl_draw_point(i, im->h - j, 3, 255, 0, 255);
    //  }
  */

  //  Track3DQuadratic_get_nn_measurements(roadmov->im);

  // update state

  //  update_nonlinear_CR_Kalman(track_3d_quadratic_Z_nn_array, 1, track_3d_quadratic_kal_nn);
  
  /*
  // draw state

  ogl_draw_CR_Matrix(15, im->h - 30, im->h, track_3d_nn);  // 30
  ogl_draw_CR_Matrix(120, im->h - 30, im->h, track_3d_nn_cc);
  */

  //  ogl_draw_CR_Matrix(15, im->h - track_3d_nn->rows - 10, im->h, track_3d_nn);  // 30

  // -------------------
  // edge stuff
  // -------------------

  // get measurements

#ifdef BADROAD_REINIT
  if (good_road_initialized && filtered_good_road) { 
#else
  if (good_road_initialized) { 
#endif

    if (track_3d_quadratic_kal) {

      //  Track3DQuadratic_get_measurements(roadmov->im);
      Track3DQuadratic_get_ortho_measurements(roadmov->im);
      
      //Track3DQuadratic_get_fake_measurements(im);
      
      // update state
      
      update_nonlinear_CR_Kalman(track_3d_quadratic_Z_array, 1, track_3d_quadratic_kal);
      
      //  print_CR_Matrix(track_3d_quadratic_kal->x);
      
      // draw state and measurements
      

      //  if (good_road) {
      track_3d_reinit_counter++;
      if (track_3d_frame >= track_3d_switch_frame) {
	if (!track_3d_reinit_flag || track_3d_reinit_counter >= 30) {
	Track3DQuadratic_draw_state_CR_CG(im, track_3d_quadratic_kal);
	Track3DQuadratic_draw_measurements(im, track_3d_quadratic_Z, track_3d_quadratic_good_Z);
	}
      }
    }
#ifdef BADROAD_REINIT
    else {
      printf("reinitializing edge kalman filter\n");
      track_3d_reinit_counter = 0;
      track_3d_reinit_flag = TRUE;
      //      track_3d_quadratic_kal = initialize_edge_tracker_from_nn("planar_z_track3dquad.s", roadmov->im);
      track_3d_quadratic_kal = initialize_edge_tracker_from_nn(NULL, roadmov->im);
    }
#endif
  }
#ifdef BADROAD_REINIT
  else if (good_road_initialized) {
    if (track_3d_quadratic_kal) {
      printf("killing edge kalman filter\n");
      free_CR_Kalman(track_3d_quadratic_kal);
      track_3d_quadratic_kal = NULL;
    }
  }
#endif

  printf("%i\n", track_3d_frame);
//   sprintf(track_3d_framestring, "%i", track_3d_frame);
//   ogl_draw_string2(track_3d_framestring, 10, 20, 255, 255, 0); 

  // -------------------
  // grab the next image
  // -------------------
  
  track_3d_frame++;
  
  //  printf("frame = %i\n", track_3d_frame);
  
  if (track_3d_frame >= track_3d_switch_frame) {
    if (roadmov->currentframe < roadmov->lastframe) 
      get_frame_CR_Movie(roadmov, roadmov->currentframe++);
  }
}

//----------------------------------------------------------------------------

void Track3DQuadratic_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  fclose(good_road_fp);
  write_avi_finish_CR_Movie(track_3d_mov);
}
 
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#define READ_DRAPE
 
CR_Vector *initialize_drape(double, CR_Image *);
CR_Vector *initialize_drape_velocity(CR_Image *);
CR_Vector *initialize_drape_force(CR_Image *);
void draw_drape(CR_Vector *, int, int, int, int, int);

double grav_force, spring_force, im_force;
double diff_thresh;

CR_Vector *max_drape_x, *max_drape_y;
int *max_drape_label;

FILE *camlaserfile;
 
CR_Image *DrapeCamera_init_CR_CG(char *name)
{
  CR_Image *im;
  char *filename;
  int i, j, w, h;

  // calibmov = open_CR_Movie("C:\\InputData\\itg_July17_2001\\xuv_laser_camera_calib.avi");
  calibmov = open_CR_Movie("C:\\InputData\\itg_July17_2001\\cal2-001.fw");
  calibmov->currentframe = 0;  
  get_frame_CR_Movie(calibmov, calibmov->currentframe);
  
  w = calibmov->im->w;
  h = calibmov->im->h;
  
  //   calibmov = (CR_Movie *) CR_malloc(sizeof(CR_Movie));
  //   calibmov->name = (char *) CR_calloc(CR_MAX_FILENAME, sizeof(char));
  //   calibmov->currentframe = 0;  
  //   calibmov->lastframe = 3000;
  //   w = 720;
  //   h = 480;
  
  im = make_CR_Image(w, h, 0);
  calibim = make_CR_Image(w, h, 0);
  meancalibim = make_CR_Image(w, h, 0);
  diffcalibim = make_CR_Image(w, h, 0);
  threshcalibim = make_CR_Image(w, h, 0);
  flippedthreshcalibim = make_CR_Image(w, h, 0);
  morphcalibim = make_CR_Image(w, h, 0);
  morph2calibim = make_CR_Image(w, h, 0);

  threshcalibmat = make_all_CR_Matrix("threshcalibmat", h, w, 0);
  cccalibmat = make_all_CR_Matrix("cccalibmat", h, w, 0);
  
  im->w *= 4;
  drape = initialize_drape(200, im);
  im->w /= 4;
  drape_velocity = initialize_drape_velocity(im);
  drape_force = initialize_drape_force(im);

  blurred_drape = initialize_drape(200, im);
  super_blurred_drape = initialize_drape(200, im);

  sprintf(name, "DrapeCamera");

  calib_frame = calibmov->currentframe;
  grab_calls = 0;
  drape_iters = 250;
  //  first_people_frame = 520;
  //  first_people_frame = 2800;
  //  first_people_frame = 320;
  first_people_frame = 1800;
  last_init_frame = 50;  // 400

  grav_force = 0.01;
  spring_force = 0.005;
  im_force = -0.025;
  diff_thresh = 10.0;

  max_drape_x = initialize_drape(0.0, im);
  max_drape_y = initialize_drape(0.0, im);
  max_drape_label = (int *) CR_calloc(max_drape_x->rows, sizeof(int));

  // read drape 

#ifdef READ_DRAPE
  //  drape_file = fopen("C:\\InputData\\itg_July17_2001\\cameradrape2.txt", "r");
  drape_file = fopen("C:\\InputData\\itg_July17_2001\\laserdrape2.txt", "r");
  fscanf(drape_file, "# grav = %f, spring = %f, image = %f, thresh = %f, drape_iters = %i, movie = '%s', last_init_frame = %i, first_people_frame = %i\n", &grav_force, &spring_force, &im_force, &diff_thresh, &drape_iters, calibmov->name, &last_init_frame, &first_people_frame);

  printf("# grav = %f, spring = %f, image = %f, thresh = %f, drape_iters = %i, movie = '%s', last_init_frame = %i, first_people_frame = %i\n", grav_force, spring_force, im_force, diff_thresh, drape_iters, calibmov->name, last_init_frame, first_people_frame);

  //  camlaserfile = fopen("camfile.txt", "w");
  camlaserfile = fopen("laserfile.txt", "w");

  int temp;

  //  calib_frame = 321;
  calib_frame = 1801;

  //  calibmov->currentframe = first_people_frame - 1;  
  //calibmov->currentframe = first_people_frame;  
  //  get_frame_CR_Movie(calibmov, calibmov->currentframe);

#else

  // write drape

  drape_file = fopen("cameradrape.txt", "w");
  fprintf(drape_file, "# grav = %f, spring = %f, image = %f, thresh = %f, drape_iters = %i, movie = '%s', last_init_frame = %i, first_people_frame = %i\n", grav_force, spring_force, im_force, diff_thresh, drape_iters, calibmov->name, last_init_frame, first_people_frame);
  fflush(drape_file);

#endif

  // initialize data structures

  calib_mean = (CR_Vector ***) CR_calloc(im->h, sizeof(CR_Vector **));
  for (j = 0; j < im->h; j++) {
    calib_mean[j] = (CR_Vector **) CR_calloc(im->w, sizeof(CR_Vector *));
    for (i = 0; i < im->w; i++) {
      calib_mean[j][i] = make_all_CR_Vector("calib_mean", 3, 0.0);
    }
  }


  // finish

  return im;
}

//----------------------------------------------------------------------------

#define LEFT_LABEL     1
#define CENTER_LABEL   2
#define RIGHT_LABEL    3

//----------------------------------------------------------------------------

double kmeans_mean_drape(int which_label, CR_Vector *drape_x, int *label)
{
  int i;
  double mean, members;

  for (i = 0, mean = 0.0, members = 0.0; i < drape_x->rows; i++)
    if (label[i] == which_label) {
      mean += drape_x->x[i];
      members++;
    }
  
  return mean / members;
}

//----------------------------------------------------------------------------

int kmeans_label_drape(double x, int *label, double left_mean, double center_mean, double right_mean)
{
  int i, current_label;
  double left_dist, center_dist, right_dist;

  current_label = *label;

  left_dist = fabs(x - left_mean);
  center_dist = fabs(x - center_mean);
  right_dist = fabs(x - right_mean);
  
  printf("x = %lf, right_mean = %lf\n", x, right_mean);
  printf("l = %lf, c = %lf, r = %lf\n", left_dist, center_dist, right_dist);

  if (left_dist <= center_dist && left_dist <= right_dist) 
    *label = LEFT_LABEL;
  else if (center_dist <= right_dist)  // because we know left > center or left > right already
    *label = CENTER_LABEL;
  else {
    printf("gotta right\n");
    *label = RIGHT_LABEL;
  }

  if (*label == current_label)
    return 1;
  else
    return 0;
}

//----------------------------------------------------------------------------

void kmeans_drape(int max_iters, CR_Vector *drape_x, int *label)
{
  int left, center, right, i, iters, label_changes, min_center;
  double left_mean, center_mean, right_mean, total_mean, total_variance, within_variance, goodness;
  double min_dist, dist;

  // pick initial centers and use them as means

  //  left = ranged_uniform_int_CR_Random(0, drape_x->rows - 1);
  left = 0;
  //  printf("left = %i\n", left);

  //  center = drape_x->rows / 2;
  // camera
  //  min_dist = fabs(drape_x->x[0] - 360);
  min_dist = fabs(drape_x->x[0] - 90);
  min_center = 0;
  for (i = 1; i < drape_x->rows; i++) {
    //    dist = fabs(drape_x->x[i] - 360);
    dist = fabs(drape_x->x[i] - 90);
    if (dist < min_dist) {
      min_dist = dist;
      min_center = i;
    }
  }
  center = min_center;

//   do {
//     center = ranged_uniform_int_CR_Random(0, drape_x->rows - 1);
//   } while (center == left);
  //  printf("center = %i\n", center);

  right = drape_x->rows - 1;
//   do {
//     right = ranged_uniform_int_CR_Random(0, drape_x->rows - 1);
//     //  printf("prelim right = %i\n", right);
// 
//   } while (right == center || right == left);
  printf("right = %i\n", right);
  print_CR_Vector(drape_x);

  left_mean = drape_x->x[left];
  center_mean = drape_x->x[center];
  right_mean = drape_x->x[right];

  for (iters = 0; iters < max_iters; iters++) {

    //    printf("iters = %i\n", iters);

    // assign labels

    for (i = 0, label_changes = 0; i < drape_x->rows; i++)
      label_changes += kmeans_label_drape(drape_x->x[i], &label[i], left_mean, center_mean, right_mean);

    //    exit(1);

    if (!label_changes)
      return;
    //      CR_error("no change");

    // compute means

    left_mean = kmeans_mean_drape(LEFT_LABEL, drape_x, label);
    center_mean = kmeans_mean_drape(CENTER_LABEL, drape_x, label);
    right_mean = kmeans_mean_drape(RIGHT_LABEL, drape_x, label);
    
  }

  // compute goodness
  
  for (i = 0, total_mean = 0; i < drape_x->rows; i++)
    total_mean += drape_x->x[i];

  total_mean /= (double) drape_x->rows;

  for (i = 0, total_variance = within_variance = 0; i < drape_x->rows; i++) {
    
    total_variance += SQUARE(drape_x->x[i] - total_mean);
    if (label[i] == LEFT_LABEL)
      within_variance += SQUARE(drape_x->x[i] - left_mean);
    else if (label[i] == CENTER_LABEL)
      within_variance += SQUARE(drape_x->x[i] - center_mean);
    else
      within_variance += SQUARE(drape_x->x[i] - right_mean);
  }

  total_variance /= (double) drape_x->rows;
  within_variance /= (double) drape_x->rows;

  goodness = within_variance / total_variance;

  //  printf("goodness = %lf\n", goodness);
}

//----------------------------------------------------------------------------

void draw_drape(CR_Vector *drape, int h, int size, int r, int g, int b)
{
  int i, maxima, temp1, temp2;
  double min_x_left, min_x_center, min_x_right, min_y_left, min_y_center, min_y_right;
  int imin_x_left, imin_x_center, imin_x_right, imin_y_left, imin_y_center, imin_y_right;
  int left_y, center_y, right_y, left_r, center_r, right_r;

  //  printf("h = %i, drape rows = %i\n", h, drape->rows);

  // camera
  /*
  for (i = 0, maxima = 0; i < drape->rows; i++) {
    if (i > 1 && i < drape->rows - 2 && drape->x[i] < drape->x[i - 1] && drape->x[i] < drape->x[i + 1] &&
	drape->x[i] < drape->x[i - 2] && drape->x[i] < drape->x[i + 2] && 
	drape->x[i] < 380) {
      max_drape_x->x[maxima] = (double) i;
      max_drape_y->x[maxima] = drape->x[i];
      //      ogl_draw_point(i, h - (int) CR_round(drape->x[i]) - 1, size, 0, 255, 0);
      maxima++;
    }
    //    else
    //    ogl_draw_point(i, h - (int) CR_round(drape->x[i]) - 1, size, r, g, b);
  }
  */

  // laser

  for (i = 0, maxima = 0; i < drape->rows; i += 4) {
//     if (i > 4 && i < drape->rows - 5 && drape->x[i] < drape->x[i - 4] && drape->x[i] < drape->x[i + 4] &&
// 	drape->x[i] < drape->x[i - 8] && drape->x[i] < drape->x[i + 8] && 
// 	drape->x[i] < 80) {
    if (i >= 4 && i <= drape->rows - 5 && drape->x[i] < drape->x[i - 4] && drape->x[i] < drape->x[i + 4] && drape->x[i] < 90) {
      max_drape_x->x[maxima] = 0.25 * (double) i;
      max_drape_y->x[maxima] = 0.25 * drape->x[i];
      //      ogl_draw_point(i / 4, h - (int) CR_round(0.25 * drape->x[i]) - 1, size, 0, 255, 0);
      maxima++;
    }
    //    else
    //    ogl_draw_point(i, h - (int) CR_round(drape->x[i]) - 1, size, r, g, b);
  }

  printf("maxima = %i\n", maxima);

  max_drape_x->rows = maxima;
  max_drape_y->rows = maxima;
  //  printf("maxima = %i / %i\n", maxima, drape->rows);

  if (maxima > 3) {

    kmeans_drape(100, max_drape_x, max_drape_label);

    min_y_left = min_y_center = min_y_right = 1000;

    for (i = 0; i < max_drape_x->rows; i++) {

      if (max_drape_label[i] == LEFT_LABEL) {
	if (max_drape_y->x[i] < min_y_left) {
	  min_y_left = max_drape_y->x[i];
	  min_x_left = max_drape_x->x[i];
	}
      }
      else if (max_drape_label[i] == CENTER_LABEL) {
	if (max_drape_y->x[i] < min_y_center) {
	  min_y_center = max_drape_y->x[i];
	  min_x_center = max_drape_x->x[i];
	}
      }
      else {
	printf("max_drape_y->x[%i] = %lf\n", i, max_drape_y->x[i]);
	if (max_drape_y->x[i] < min_y_right) {
	  min_y_right = max_drape_y->x[i];
	  min_x_right = max_drape_x->x[i];
	}
      }

      if (max_drape_label[i] == LEFT_LABEL)
 	ogl_draw_point(max_drape_x->x[i], h - (int) CR_round(max_drape_y->x[i]) - 1, size, 255, 0, 0);
      else if (max_drape_label[i] == CENTER_LABEL)
	ogl_draw_point(max_drape_x->x[i], h - (int) CR_round(max_drape_y->x[i]) - 1, size, 0, 255, 0);
      else
	ogl_draw_point(max_drape_x->x[i], h - (int) CR_round(max_drape_y->x[i]) - 1, size, 0, 0, 255);
    }

//     fprintf(camlaserfile, "%i %i %i %i %i %i\n", 
// 	    (int) CR_round(min_x_left), (int) CR_round(min_y_left),
// 	    (int) CR_round(min_x_center), (int) CR_round(min_y_center),
// 	    (int) CR_round(min_x_right), (int) CR_round(min_y_right));
    imin_x_left = (int) CR_round(min_x_left);
    imin_y_left = (int) CR_round(min_y_left);
    imin_x_center = (int) CR_round(min_x_center);
    imin_y_center = (int) CR_round(min_y_center);
    imin_x_right = (int) CR_round(min_x_right);
    imin_y_right = (int) CR_round(min_y_right);

    
     ogl_draw_cross(imin_x_left, h - imin_y_left - 1, 5, 255, 255, 255);
     ogl_draw_cross(imin_x_center, h - imin_y_center - 1, 5, 255, 255, 255);
     ogl_draw_cross(imin_x_right, h - imin_y_right - 1, 5, 255, 255, 255);

     //void ogl_draw_cross(int x, int y, int scale, int r, int g, int b)

    printf("frame = %i\n", calib_frame); 

    for (i = MAX2(0, imin_y_left - 4); i <= MIN2(30, imin_y_left + 4); i++) {
      temp1 = (int) calibmov->laserim[calib_frame].data[D3_LASER_WIDTH * i + imin_x_left];
      temp2 = (int) calibmov->laserim[calib_frame].data[D3_LASER_WIDTH * (i + 1) + imin_x_left];
      printf("left %i: temp1 = %i, temp2 = %i\n", i, temp1, temp2);
      if (temp1 == 1021 && temp2 != 1021) {
	left_y = i + 1;
	left_r = temp2;
	break;
      }
    }

    for (i = MAX2(0, imin_y_center - 4); i <= MIN2(30, imin_y_center + 4); i++) {
      temp1 = (int) calibmov->laserim[calib_frame].data[D3_LASER_WIDTH * i + imin_x_center];
      temp2 = (int) calibmov->laserim[calib_frame].data[D3_LASER_WIDTH * (i + 1) + imin_x_center];
      printf("center %i: temp1 = %i, temp2 = %i\n", i, temp1, temp2);
      if (temp1 == 1021 && temp2 != 1021) {
	center_y = i + 1;
	center_r = temp2;
	break;
      }
    }

    printf("imin_y_right = %i\n", imin_y_right);

    for (i = MAX2(0, imin_y_right - 4); i <= MIN2(30, imin_y_right + 4); i++) {
      temp1 = (int) calibmov->laserim[calib_frame].data[D3_LASER_WIDTH * i + imin_x_right];
      temp2 = (int) calibmov->laserim[calib_frame].data[D3_LASER_WIDTH * (i + 1) + imin_x_right];
      printf("right %i: temp1 = %i, temp2 = %i\n", i, temp1, temp2);
      if (temp1 == 1021 && temp2 != 1021) {
	right_y = i + 1;
	right_r = temp2;
	break;
      }
    }

    printf("\n");
    fprintf(camlaserfile, "%i %i %i %i %i %i %i %i %i\n", 
	    imin_x_left, left_y, left_r, imin_x_center, center_y, center_r, imin_x_right, right_y, right_r);
    fflush(camlaserfile);

//     ogl_draw_point(min_x_left, h - min_y_left - 1, 5, 255, 255, 255);
//     ogl_draw_point(min_x_center, h - min_y_center - 1, 5, 255, 255, 255);
//     ogl_draw_point(min_x_right, h - min_y_right - 1, 5, 255, 255, 255);
// 
//     ogl_draw_point(min_x_left, h - min_y_left - 1, 3, 255, 0, 0);
//     ogl_draw_point(min_x_center, h - min_y_center - 1, 3, 0, 255, 0);
//     ogl_draw_point(min_x_right, h - min_y_right - 1, 3, 0, 0, 255);

  }
}

//----------------------------------------------------------------------------

// run box filter over drape

void blur_drape(CR_Vector *drape, CR_Vector *blurred_drape, int blur_radius)
{
  int i, j;
  double total, n;

  for (i = 0; i < drape->rows; i++) {
    for (j = MAX2(i - blur_radius, 0), n = 0, total = 0; j < MIN2(i + blur_radius, drape->rows - 1); j++, n++) 
      total += drape->x[j];
    blurred_drape->x[i] = total / n;
  }
      
}

//----------------------------------------------------------------------------

// assuming points have unit mass, so force = accel (because F = ma, of course)

// using following kinematic equations: 
// (1) x = x_0 + v_0 * t + 0.5 * a * t^2
// (2) v = v_0 + a * t

// assuming unit time increment, this yields:
// (1) x = x_0 + v_0 + 0.5 * a
// (2) v = v_0 + a

void compute_drape(CR_Vector *drape, CR_Vector *drape_velocity, CR_Vector *drape_force, CR_Matrix *mat, int dy_rad, int floor_height)
{
  int i, j, y;
  double dy, max_dy;

  // zero forces

  for (i = 0; i < drape_force->rows; i++)
    drape_force->x[i] = 0;

  // zero velocities

  for (i = 0; i < drape_velocity->rows; i++)
    drape_velocity->x[i] *= 0.95;
	
  // gravitational force

  for (i = 0; i < drape_force->rows; i++)
    drape_force->x[i] += grav_force;
 
  // image force

  for (i = 0; i < drape_force->rows; i++) {
    y = (int) CR_round(drape->x[i]);
    for (j = MAX2(y - dy_rad, 0), dy = 0; j < MIN2(mat->rows - 1, y + dy_rad); j++) {
      dy += MATRC(mat, j, i);
    }
    drape_force->x[i] += im_force * dy;
  }

  // spring force

  // ends of drape

  drape_force->x[0] += spring_force * fabs(drape->x[1] - drape->x[0]);
  drape_force->x[drape_force->rows - 1] += spring_force * fabs(drape->x[drape_force->rows - 2] - drape->x[drape_force->rows - 1]);

  // middle of drape

  for (i = 1; i < drape_force->rows - 1; i++) 
    drape_force->x[i] += spring_force * (drape->x[i - 1] + drape->x[i + 1] - 2 * drape->x[i]);

  // position

  for (i = 0; i < drape->rows; i++)
    if (drape->x[i] <= mat->rows - 1 - floor_height)
      drape->x[i] += drape_velocity->x[i] + 0.5 * drape_force->x[i];
    else
      drape->x[i] = mat->rows - 1 - floor_height;

  // velocity

  for (i = 0; i < drape_velocity->rows; i++)
    drape_velocity->x[i] += drape_force->x[i];

}

//----------------------------------------------------------------------------

CR_Vector *initialize_drape(double startval, CR_Image *im)
{
  // for first_people_frame = 520
  //  return make_all_CR_Vector("drape", im->w, 200);
  // for first_people_frame = 2800
  //  return make_all_CR_Vector("drape", im->w, 250);

  return make_all_CR_Vector("drape", im->w, startval);
}

//----------------------------------------------------------------------------

CR_Vector *initialize_drape_velocity(CR_Image *im)
{
  return make_all_CR_Vector("drape_velocity", im->w, 0);
}

//----------------------------------------------------------------------------

CR_Vector *initialize_drape_force(CR_Image *im)
{
  return make_all_CR_Vector("drape_force", im->w, 0);
}

//----------------------------------------------------------------------------

void DrapeCamera_grab_CR_CG(CR_Image *im)
{
  int i, j, temp;
  int num_ccs;

  //  if (calib_frame < 1608) {
  if (calib_frame < 2229) {
    //  for (calib_frame = 1801; calib_frame < 2229; calib_frame++) {
    get_frame_CR_Movie(calibmov, calib_frame);

    vflip_CR_Image(calibmov->im, calibim);
    glDrawPixels(calibim->w, calibim->h, GL_RGB, GL_UNSIGNED_BYTE, calibim->data);

    fprintf(camlaserfile, "%i ", calib_frame);

    fscanf(drape_file, "%i\n", &temp);
    fread_CR_Vector(drape_file, drape);
    //    printf("new drape rows = %i\n", drape->rows);
    draw_drape(drape, im->h, 2, 255, 0, 0);

    calib_frame++;

    return;
  }
  else {
    fclose(camlaserfile);
    exit(1);
  }

#ifndef READ_DRAPE
  if (!(grab_calls % drape_iters)) 
#endif
    printf("frame = %i\n", calib_frame);

  /*
#ifdef READ_DRAPE

  vflip_CR_Image(calibmov->im, calibim);
  glDrawPixels(calibim->w, calibim->h, GL_RGB, GL_UNSIGNED_BYTE, calibim->data);

//   glClearColor(0, 0, 0, 0);
//   glClear(GL_COLOR_BUFFER_BIT);

  fscanf(drape_file, "%i\n", &temp);
  //  printf("temp = %i\n", temp);
  fread_CR_Vector(drape_file, drape);
  //  print_CR_Vector(drape);
  //  draw_drape(drape, calibmov->im->h, 2, 255, 0, 0);
  draw_drape(drape, im->h, 2, 255, 0, 0);

#else
  */

  // -----------------------------
  // initializing background model
  // -----------------------------

  if (calib_frame < last_init_frame) {    // 400

    // draw current image

    vflip_CR_Image(calibmov->im, calibim);
    glDrawPixels(calibim->w, calibim->h, GL_RGB, GL_UNSIGNED_BYTE, calibim->data);

    // calculate mean

    for (j = 0; j < calibmov->im->h; j++)
      for (i = 0; i < calibmov->im->w; i++) {
	calib_mean[j][i]->x[0] = (calib_mean[j][i]->x[0] * (double) calib_frame + (unsigned char) IMXY_R(calibmov->im, i, j)) / (double) (calib_frame + 1);
	calib_mean[j][i]->x[1] = (calib_mean[j][i]->x[1] * (double) calib_frame + (unsigned char) IMXY_G(calibmov->im, i, j)) / (double) (calib_frame + 1);
	calib_mean[j][i]->x[2] = (calib_mean[j][i]->x[2] * (double) calib_frame + (unsigned char) IMXY_B(calibmov->im, i, j)) / (double) (calib_frame + 1);
      }	
  }

  // -----------------------------
  // tracking tops of heads
  // -----------------------------

  else {

    //    vflip_CR_Image(calibmov->im, calibim);
    //    glDrawPixels(calibim->w, calibim->h, GL_RGB, GL_UNSIGNED_BYTE, calibim->data);

    // make mean image

    if (calib_frame == last_init_frame) {

    for (j = 0; j < calibim->h; j++)
      for (i = 0; i < calibim->w; i++) { 
	IMXY_R(meancalibim, i, j) = (unsigned char) CR_round(calib_mean[j][i]->x[0]);
	IMXY_G(meancalibim, i, j) = (unsigned char) CR_round(calib_mean[j][i]->x[1]);
	IMXY_B(meancalibim, i, j) = (unsigned char) CR_round(calib_mean[j][i]->x[2]);

	calib_frame = calibmov->currentframe = first_people_frame;
	
// 	printf("getting %i\n", calibmov->currentframe); 
// 	get_frame_CR_Movie(calibmov, calibmov->currentframe++);

	//	print_CR_Vector(calib_mean[j][i]);

      }
    }

    // draw mean image

    //    vflip_CR_Image(calibmov->im, calibim);
    //    glDrawPixels(meancalibim->w, meancalibim->h, GL_RGB, GL_UNSIGNED_BYTE, meancalibim->data);

//    iplSubtract(meancalibim->iplim, calibmov->im->iplim, diffcalibim->iplim);

#ifdef READ_DRAPE
    if (TRUE) {
#else
    if (!(grab_calls % drape_iters)) {
#endif

      for (j = 0; j < calibim->h; j++)
	for (i = 0; i < calibim->w; i++) { 
	  
	  IMXY_R(diffcalibim, i, j) = (unsigned char) fabs((int) ((unsigned char) IMXY_R(meancalibim, i, j)) - (int) ((unsigned char) IMXY_R(calibmov->im, i, j)));
	  IMXY_G(diffcalibim, i, j) = (unsigned char) fabs((int) ((unsigned char) IMXY_G(meancalibim, i, j)) - (int) ((unsigned char) IMXY_G(calibmov->im, i, j)));
	  IMXY_B(diffcalibim, i, j) = (unsigned char) fabs((int) ((unsigned char) IMXY_B(meancalibim, i, j)) - (int) ((unsigned char) IMXY_B(calibmov->im, i, j)));
	  
	}

#ifdef READ_DRAPE
   
      for (j = 0; j < calibim->h; j++)
	for (i = 0; i < calibim->w; i++) { 	  
	  temp = (int) CR_round(0.5773502692 * sqrt(
				     SQUARE((double) ((unsigned char) IMXY_R(diffcalibim, i, j))) +
				     SQUARE((double) ((unsigned char) IMXY_G(diffcalibim, i, j))) +
				     SQUARE((double) ((unsigned char) IMXY_B(diffcalibim, i, j)))));
	    IMXY_R(threshcalibim, i, j) = (unsigned char) temp;
	    IMXY_G(threshcalibim, i, j) = (unsigned char) temp;
	    IMXY_B(threshcalibim, i, j) = (unsigned char) temp;
	}

#else

      for (j = 0; j < calibim->h; j++)
	for (i = 0; i < calibim->w; i++) { 	  
	  if (sqrt(SQUARE((double) ((unsigned char) IMXY_R(diffcalibim, i, j))) +
		   SQUARE((double) ((unsigned char) IMXY_G(diffcalibim, i, j))) +
		   SQUARE((double) ((unsigned char) IMXY_B(diffcalibim, i, j)))) >= diff_thresh) { 
	    IMXY_R(threshcalibim, i, j) = (unsigned char) 255;
	    IMXY_G(threshcalibim, i, j) = (unsigned char) 255;
	    IMXY_B(threshcalibim, i, j) = (unsigned char) 255;
	  }
	  else {
	    IMXY_R(threshcalibim, i, j) = (unsigned char) 0;
	    IMXY_G(threshcalibim, i, j) = (unsigned char) 0;
	    IMXY_B(threshcalibim, i, j) = (unsigned char) 0;
	  }
	}

      //    iplErode(threshcalibim->iplim, morphcalibim->iplim, 2);
      //    iplDilate(threshcalibim->iplim, morphcalibim->iplim, 5);
      
      iplOpen(threshcalibim->iplim, morphcalibim->iplim, 4);
      iplClose(morphcalibim->iplim, morph2calibim->iplim, 4);

      for (j = 0; j < calibim->h; j++)
	for (i = 0; i < calibim->w; i++) { 
	  if (j < 160)
	    MATRC(threshcalibmat, j, i) = 0;
	  else {
	    if ((unsigned char) IMXY_R(morph2calibim, i, j) > 0) 
	      MATRC(threshcalibmat, j, i) = 1;
	    else
	      MATRC(threshcalibmat, j, i) = 0;	
	  }
	}

   
    //    biggest_connected_component_CR_Matrix(TRUE, threshcalibmat, cccalibmat);
    //    draw_CR_Matrix_CR_Image(1, cccalibmat, threshcalibim, FALSE);
    //num_ccs = connected_components_CR_Matrix(TRUE, threshcalibmat, cccalibmat);
    //draw_CR_Matrix_CR_Image(num_ccs, cccalibmat, threshcalibim, FALSE);
    

    draw_CR_Matrix_CR_Image(1, threshcalibmat, threshcalibim, FALSE);

    //    vflip_CR_Image(calibmov->im, calibim);
    vflip_CR_Image(threshcalibim, flippedthreshcalibim);

#endif

    }

    if (calib_frame > first_people_frame) {

#ifdef READ_DRAPE

      vflip_CR_Image(calibmov->im, flippedthreshcalibim);
      glDrawPixels(flippedthreshcalibim->w, flippedthreshcalibim->h, GL_RGB, GL_UNSIGNED_BYTE, flippedthreshcalibim->data);
   
      fprintf(camlaserfile, "%i ", calibmov->currentframe - 1);

      fscanf(drape_file, "%i\n", &temp);
      fread_CR_Vector(drape_file, drape);
      draw_drape(drape, im->h, 2, 255, 0, 0);


#else

      if (!(grab_calls % drape_iters)) {
	fprintf(drape_file, "%i\n", calib_frame);
	fprint_CR_Vector(drape_file, drape);
	fflush(drape_file);
      }

      //      glDrawPixels(calibim->w, calibim->h, GL_RGB, GL_UNSIGNED_BYTE, calibim->data);
      glDrawPixels(flippedthreshcalibim->w, flippedthreshcalibim->h, GL_RGB, GL_UNSIGNED_BYTE, flippedthreshcalibim->data);
      
      compute_drape(drape, drape_velocity, drape_force, threshcalibmat, 1, 10);
      blur_drape(drape, blurred_drape, 10);
      //      blur_drape(drape, super_blurred_drape, 50);
      draw_drape(drape, threshcalibim->h, 2, 255, 0, 0);
      draw_drape(blurred_drape, threshcalibim->h, 2, 0, 255, 0);
      //      draw_drape(super_blurred_drape, threshcalibim->h, 2, 0, 0, 255);

#endif

    }


    grab_calls++;

//    vflip_CR_Image(morphcalibim, flippedthreshcalibim);
//    glDrawPixels(flippedthreshcalibim->w, flippedthreshcalibim->h, GL_RGB, GL_UNSIGNED_BYTE, flippedthreshcalibim->data);
    

    //    CR_exit(1);
  }

  //#endif

  // -------------------
  // grab the next image
  // -------------------

#ifndef READ_DRAPE
  if (!(grab_calls % drape_iters)) {
#endif
    calib_frame++;

//     if (calibmov->currentframe++ < calibmov->lastframe) 
//       ;
    if (calibmov->currentframe < calibmov->lastframe) {
      printf("got %i\n", calibmov->currentframe); 
      printf("----------------------\n");
      get_frame_CR_Movie(calibmov, calibmov->currentframe++);
    }
    else
      CR_exit(1);

#ifndef READ_DRAPE
  }
#endif
}

//----------------------------------------------------------------------------

void DrapeCamera_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  fclose(good_road_fp);
  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Image *DrapeLaser_init_CR_CG(char *name)
{
  CR_Image *im;
  char *filename;
  int i, j, w, h;

  calibmov = open_CR_Movie("C:\\InputData\\itg_July17_2001\\cal2-001.fw");
  calibmov->currentframe = 0;  
  get_frame_CR_Movie(calibmov, calibmov->currentframe);

  zoom_factor = 4;

  w = calibmov->im->w;
  h = calibmov->im->h;
  w *= zoom_factor;
  h *= zoom_factor;

  im = make_CR_Image(w, h, 0);
  calibim = make_CR_Image(w, h, 0);
  meancalibim = make_CR_Image(w, h, 0);
  diffcalibim = make_CR_Image(w, h, 0);
  threshcalibim = make_CR_Image(w, h, 0);
  flippedthreshcalibim = make_CR_Image(w, h, 0);
  morphcalibim = make_CR_Image(w, h, 0);
  morph2calibim = make_CR_Image(w, h, 0);

  zoomcalibim = make_CR_Image(w, h, 0);

  threshcalibmat = make_all_CR_Matrix("threshcalibmat", h, w, 0);
  cccalibmat = make_all_CR_Matrix("cccalibmat", h, w, 0);

  drape = initialize_drape(10 * zoom_factor, im);
  drape_velocity = initialize_drape_velocity(im);
  drape_force = initialize_drape_force(im);

  blurred_drape = initialize_drape(10 * zoom_factor, im);
  super_blurred_drape = initialize_drape(10 * zoom_factor, im);

  sprintf(name, "DrapeLaser");

  calib_frame = calibmov->currentframe;
  grab_calls = 0;
  drape_iters = 250;
  first_people_frame = 1800;  // 320
  // I disappear at 1569
  last_init_frame = 100;  // 400

//   grav_force = 0.01;
//   spring_force = 0.005;
//   im_force = -0.025;
//   diff_thresh = 10.0;

  grav_force = 0.01;
  spring_force = 0.005;
  im_force = -0.025;
  diff_thresh = 10.0;

  // read drape 

  /*
  drape_file = fopen("drape1.txt", "r");
  fscanf(drape_file, "# grav = %f, spring = %f, image = %f, thresh = %f, drape_iters = %i, movie = '%s', last_init_frame = %i, first_people_frame = %i\n", &grav_force, &spring_force, &im_force, &diff_thresh, &drape_iters, calibmov->name, &last_init_frame, &first_people_frame);

  printf("# grav = %f, spring = %f, image = %f, thresh = %f, drape_iters = %i, movie = '%s', last_init_frame = %i, first_people_frame = %i\n", grav_force, spring_force, im_force, diff_thresh, drape_iters, calibmov->name, last_init_frame, first_people_frame);

  //  calibmov->currentframe = first_people_frame + 1;  
  calibmov->currentframe = first_people_frame;  
  get_frame_CR_Movie(calibmov, calibmov->currentframe);
  */

  // write drape

  drape_file = fopen("laserdrape.txt", "w");
  fprintf(drape_file, "# grav = %f, spring = %f, image = %f, thresh = %f, drape_iters = %i, movie = '%s', last_init_frame = %i, first_people_frame = %i, zoom_factor = %i\n", grav_force, spring_force, im_force, diff_thresh, drape_iters, calibmov->name, last_init_frame, first_people_frame, zoom_factor);
  fflush(drape_file);

  // initialize data structures

  calib_mean = (CR_Vector ***) CR_calloc(im->h, sizeof(CR_Vector **));
  for (j = 0; j < im->h; j++) {
    calib_mean[j] = (CR_Vector **) CR_calloc(im->w, sizeof(CR_Vector *));
    for (i = 0; i < im->w; i++) 
      calib_mean[j][i] = make_all_CR_Vector("calib_mean", 3, 0.0);
  }

  // finish

  return im;
  //return zoomcalibim;
}

//----------------------------------------------------------------------------

void DrapeLaser_grab_CR_CG(CR_Image *im)
{
  int i, j, temp, w, h;
  int num_ccs;
  CR_Image *src_im;

  if (!(grab_calls % drape_iters)) 
    printf("frame = %i\n", calib_frame);

  /*
  vflip_CR_Image(calibmov->im, calibim);
  glDrawPixels(calibim->w, calibim->h, GL_RGB, GL_UNSIGNED_BYTE, calibim->data);

  fscanf(drape_file, "%i\n", &temp);
  printf("temp = %i\n", temp);
  fread_CR_Vector(drape_file, drape);
  //  print_CR_Vector(drape);
  draw_drape(drape, calibmov->im->h, 2, 255, 0, 0);
  */

  w = calibmov->im->w;
  h = calibmov->im->h;

  // no zoom

  //src_im = calibmov->im;

  // zoom

  w *= zoom_factor;
  h *= zoom_factor;
  src_im = zoomcalibim;

  // -----------------------------
  // initializing background model
  // -----------------------------

  if (calib_frame < last_init_frame) {    // 400

    // draw current image

    iplZoom(calibmov->im->iplim, zoomcalibim->iplim, zoom_factor, 1, zoom_factor, 1, IPL_INTER_CUBIC);
    //vflip_CR_Image(calibmov->im, calibim);
    vflip_CR_Image(zoomcalibim, calibim);
    glDrawPixels(calibim->w, calibim->h, GL_RGB, GL_UNSIGNED_BYTE, calibim->data);

    // calculate mean

    for (j = 0; j < h; j++)
      for (i = 0; i < w; i++) {
	calib_mean[j][i]->x[0] = (calib_mean[j][i]->x[0] * (double) calib_frame + (unsigned char) IMXY_R(src_im, i, j)) / (double) (calib_frame + 1);
	calib_mean[j][i]->x[1] = (calib_mean[j][i]->x[1] * (double) calib_frame + (unsigned char) IMXY_G(src_im, i, j)) / (double) (calib_frame + 1);
	calib_mean[j][i]->x[2] = (calib_mean[j][i]->x[2] * (double) calib_frame + (unsigned char) IMXY_B(src_im, i, j)) / (double) (calib_frame + 1);
      }	
  }

  // -----------------------------
  // tracking tops of heads
  // -----------------------------

  else {

    iplZoom(calibmov->im->iplim, zoomcalibim->iplim, zoom_factor, 1, zoom_factor, 1, IPL_INTER_CUBIC);

    //    vflip_CR_Image(calibmov->im, calibim);
    //    glDrawPixels(calibim->w, calibim->h, GL_RGB, GL_UNSIGNED_BYTE, calibim->data);

    // make mean image

    if (calib_frame == last_init_frame) {

    for (j = 0; j < calibim->h; j++)
      for (i = 0; i < calibim->w; i++) { 
	IMXY_R(meancalibim, i, j) = (unsigned char) CR_round(calib_mean[j][i]->x[0]);
	IMXY_G(meancalibim, i, j) = (unsigned char) CR_round(calib_mean[j][i]->x[1]);
	IMXY_B(meancalibim, i, j) = (unsigned char) CR_round(calib_mean[j][i]->x[2]);

	calib_frame = calibmov->currentframe = first_people_frame;
	
	//	print_CR_Vector(calib_mean[j][i]);

      }
    }

    // draw mean image

    //    vflip_CR_Image(calibmov->im, calibim);
    //    glDrawPixels(meancalibim->w, meancalibim->h, GL_RGB, GL_UNSIGNED_BYTE, meancalibim->data);

//    iplSubtract(meancalibim->iplim, calibmov->im->iplim, diffcalibim->iplim);

    if (!(grab_calls % drape_iters)) {

      for (j = 0; j < calibim->h; j++)
	for (i = 0; i < calibim->w; i++) { 
	  
	  IMXY_R(diffcalibim, i, j) = (unsigned char) fabs((int) ((unsigned char) IMXY_R(meancalibim, i, j)) - (int) ((unsigned char) IMXY_R(src_im, i, j)));
	  IMXY_G(diffcalibim, i, j) = (unsigned char) fabs((int) ((unsigned char) IMXY_G(meancalibim, i, j)) - (int) ((unsigned char) IMXY_G(src_im, i, j)));
	  IMXY_B(diffcalibim, i, j) = (unsigned char) fabs((int) ((unsigned char) IMXY_B(meancalibim, i, j)) - (int) ((unsigned char) IMXY_B(src_im, i, j)));
	  
	}
      
      for (j = 0; j < calibim->h; j++)
	for (i = 0; i < calibim->w; i++) { 
	  
	  // 	if (0.2 * (double) ((unsigned char) IMXY_R(diffcalibim, i, j)) +
	  // 	    0.7 * (double) ((unsigned char) IMXY_G(diffcalibim, i, j)) +
	  // 	    0.1 * (double) ((unsigned char) IMXY_B(diffcalibim, i, j)) >= 25) {
	  
	  if (sqrt(SQUARE((double) ((unsigned char) IMXY_R(diffcalibim, i, j))) +
		   SQUARE((double) ((unsigned char) IMXY_G(diffcalibim, i, j))) +
		   SQUARE((double) ((unsigned char) IMXY_B(diffcalibim, i, j)))) >= diff_thresh) { 
	    //	    MATRC(threshcalibmat, j, i) = 1;
	    IMXY_R(threshcalibim, i, j) = (unsigned char) 255;
	    IMXY_G(threshcalibim, i, j) = (unsigned char) 255;
	    IMXY_B(threshcalibim, i, j) = (unsigned char) 255;
	  }
	  else {
	    //	    MATRC(threshcalibmat, j, i) = 0;
	    IMXY_R(threshcalibim, i, j) = (unsigned char) 0;
	    IMXY_G(threshcalibim, i, j) = (unsigned char) 0;
	    IMXY_B(threshcalibim, i, j) = (unsigned char) 0;
	  }
	}

      //    iplErode(threshcalibim->iplim, morphcalibim->iplim, 2);
      //    iplDilate(threshcalibim->iplim, morphcalibim->iplim, 5);
      
      
      iplOpen(threshcalibim->iplim, morphcalibim->iplim, zoom_factor);  // 1 if no zoom
      iplClose(morphcalibim->iplim, morph2calibim->iplim, zoom_factor); // 1 if no zoom

      for (j = 0; j < calibim->h; j++)
	for (i = 0; i < calibim->w; i++) { 
	  if ((unsigned char) IMXY_R(morph2calibim, i, j) > 0) 
	    MATRC(threshcalibmat, j, i) = 1;
	  else
	    MATRC(threshcalibmat, j, i) = 0;	
	}
   
    //    biggest_connected_component_CR_Matrix(TRUE, threshcalibmat, cccalibmat);
    //    draw_CR_Matrix_CR_Image(1, cccalibmat, threshcalibim, FALSE);
    //num_ccs = connected_components_CR_Matrix(TRUE, threshcalibmat, cccalibmat);
    //draw_CR_Matrix_CR_Image(num_ccs, cccalibmat, threshcalibim, FALSE);
    

    draw_CR_Matrix_CR_Image(1, threshcalibmat, threshcalibim, FALSE);

    //    vflip_CR_Image(src_im, flippedthreshcalibim);
    vflip_CR_Image(threshcalibim, flippedthreshcalibim);

    }

    if (calib_frame > first_people_frame) {

      if (!(grab_calls % drape_iters)) {
	fprintf(drape_file, "%i\n", calib_frame);
	fprint_CR_Vector(drape_file, drape);
	fflush(drape_file);
      }

      //      glDrawPixels(src_im->w, src_im->h, GL_RGB, GL_UNSIGNED_BYTE, src_im->data);
      glDrawPixels(flippedthreshcalibim->w, flippedthreshcalibim->h, GL_RGB, GL_UNSIGNED_BYTE, flippedthreshcalibim->data);
            
      compute_drape(drape, drape_velocity, drape_force, threshcalibmat, 1, 3 * zoom_factor);
      blur_drape(drape, blurred_drape, 3 * zoom_factor);
      //      blur_drape(drape, super_blurred_drape, 50);
      draw_drape(drape, threshcalibim->h, 2, 255, 0, 0);
      draw_drape(blurred_drape, threshcalibim->h, 2, 0, 255, 0);
      //      draw_drape(super_blurred_drape, threshcalibim->h, 2, 0, 0, 255);

    }

    grab_calls++;

//    vflip_CR_Image(morphcalibim, flippedthreshcalibim);
//    glDrawPixels(flippedthreshcalibim->w, flippedthreshcalibim->h, GL_RGB, GL_UNSIGNED_BYTE, flippedthreshcalibim->data);
    

    //    CR_exit(1);
  }

  // -------------------
  // grab the next image
  // -------------------

  if (!(grab_calls % drape_iters)) {

    calib_frame++;

    if (calibmov->currentframe < calibmov->lastframe) 
      get_frame_CR_Movie(calibmov, calibmov->currentframe++);
    else
      CR_exit(1);

  }
}

//----------------------------------------------------------------------------

void DrapeLaser_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  fclose(good_road_fp);
  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Movie *laser_movie, *camera_movie;
CR_Image *laser_image, *camera_image;

FILE *laserpointfile, *camerapointfile, *logfile;
int time_seconds, time_microseconds, total_frames_written, total_incomplete_frames, total_seo_facets_written;
int logfile_size, bytes_per_camera_frame, num_camera_frames, start_camera_flag;
FILE *lasertimefile, *camtimefile, *masterfile, *annotated_masterfile, *clean_masterfile;
int num_laser_points, num_camera_points, num_master_points, minindex;
CR_Matrix *campoints, *laserpoints, *masterpoints;
double mincamtime, mindifftime;
int laser_frame, camera_frame;

double start_laser_time, start_camera_time;
CR_Vector *camframetimes;

//----------------------------------------------------------------------------

int poly_cmp(const void *left, const void *right)
{
  double leftval, rightval;

  leftval = MATRC((*((CR_Matrix ***) left))[0], 0, 0);
  rightval = MATRC((*((CR_Matrix ***) right))[0], 0, 0);

  //  printf("left = %lf, right = %lf\n", leftval, rightval);

  if (leftval < rightval)
    return -1;
  else if (leftval > rightval)
    return 1;
  else 
    return 0;
}

CR_Matrix ***load_roadpolys(char *directoryname, int *num_polys)
{
  int i, j, num_lines;
  CR_Matrix ***polyinfo;
  FILE *fp, *fp2;
  char c, c1, c2;
  char listmfiles[128];
  char *scanline, *scanline2;

  sprintf(listmfiles, "ls %s*.m | grep roadpoly > temp.txt", directoryname);
  system(listmfiles);

  fp = fopen("temp.txt", "r");
  fp2 = fopen("temp2.txt", "w+");

  // count 'em and allocate
  
  num_lines = -1;
  scanline = (char *) CR_calloc(80, sizeof(char));
  scanline2 = (char *) CR_calloc(80, sizeof(char));
  do {
    scanline = fgets(scanline, 80, fp);
    num_lines++;
  } while (scanline);
  
  printf("num_polys = %i\n", num_lines);

  *num_polys = num_lines;

  // read 'em in

  polyinfo = (CR_Matrix ***) CR_calloc(*num_polys, sizeof(CR_Matrix **));
  for (i = 0; i < *num_polys; i++)
    polyinfo[i] = make_CR_Matrix_array(2);

  rewind(fp);

  do {
    c1 = fgetc(fp);
    if (c1 == EOF)
      break;
    if (c1 != '\n') {
      c2 = fgetc(fp);
      if (c1 == '/' && c2 == '/') {
	c = fgetc(fp);
	fputc(c, fp2);
	fputc(':', fp2);
      }
      else if (c1 == '/') {
	fputc('\\', fp2);
	fputc('\\', fp2);
	fputc(c2, fp2);
      }
      else {
	fputc(c1, fp2);
	fputc(c2, fp2);
      }
    }
    else
      fputc(c1, fp2);
  } while (c1 != EOF && c2 != EOF);

  rewind(fp2);

  for (i = 0; i < num_lines; i++) {
    fgets(scanline2, 80, fp2);
    scanline2[strlen(scanline2) - 1] = '\0';
    polyinfo[i][1] = read_CR_Matrix(scanline2);
    scanline2[strlen(scanline2) - 2] = '\0';
    for (j = strlen(scanline2) - 8; j < strlen(scanline2); j++)
      if (isdigit(scanline2[j])) {
	polyinfo[i][0] = init_CR_Matrix("poly", 1, 1, (double) atoi(&scanline2[j]));
	print_CR_Matrix(polyinfo[i][0]);
	break;
      }
  }

  fclose(fp);
  fclose(fp2);

  // sort polys by frame number

  qsort((void *) polyinfo, *num_polys, sizeof(CR_Matrix **), poly_cmp);
  for (i = 0; i < *num_polys; i++)
    print_CR_Matrix(polyinfo[i][0]);

  // finish

  return polyinfo;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

int TestCalibration_zoom_factor;
int camera_startframe;

int tc_filter_size, tc_filter_radius;
int tc_left, tc_right, tc_top, tc_bottom;
// int tc_filter_radius = 7;
// int tc_filter_size = 15;
// int tc_left = 14;
// int tc_right = 348;
// int tc_top = 70;
// int tc_bottom = 215;
int tc_x_count, tc_y_count, tc_count;
CR_Matrix *tc_laser_bin_count, *tc_laser_bin_x, *tc_laser_bin_y, *tc_laser_bin_z;
CR_Vector *laser_bin_x, *laser_bin_y, *laser_bin_z, *laser_bin_roadness;
CR_Vector ***tc_hist_r, ***tc_hist_g, ***tc_hist_b;

CR_Matrix ***polyinfo;
int num_polys, current_poly, tc_hist_rgb_num_bins;
FILE *tc_data_fp;
double tc_hist_rgb_binsize, tc_filter_inverse_area;

int tc_write_data;

CR_Image *TestCalibration_init_CR_CG(char *name)
{
  CR_Image *im;
  double this_time, laser_timestamp, laser_timestamp_last, camera_timestamp;
  int i, j;

  //  tc_write_data = TRUE;
  tc_write_data = FALSE;

  polyinfo = load_roadpolys("//C/InputData/itg_July17_2001/morning/polys", &num_polys);
  //  polyarray = load_roadpolys("C:\\InputData\\itg_July17_2001\\morning\\", NULL, &num_polys);
    
  // open camera movie

  //  camera_movie = open_CR_Movie("C:\\InputData\\itg_July17_2001\\xuv_laser_camera_calib.avi");
  camera_movie = open_CR_Movie("C:\\InputData\\itg_July17_2001\\morning\\cut_road1-001.avi");
  camera_movie->currentframe = 0;
  get_frame_CR_Movie(camera_movie, camera_movie->currentframe);

  camera_image = make_CR_Image(camera_movie->im->w, camera_movie->im->h, 0);

  // initialize window size and laser to camera transformation

  TestCalibration_zoom_factor = camera_movie->width / D3_LASER_WIDTH;
  printf("tc zoom = %i\n", TestCalibration_zoom_factor);
  im = make_CR_Image(camera_movie->width, camera_movie->height + D3_LASER_HEIGHT * TestCalibration_zoom_factor, 0);
  init_laser2camera(camera_movie->im->w, camera_movie->im->h);

  // check logfile

  //  logfile = fopen("cal2-001.log", "rb");
  logfile = fopen("C:\\InputData\\itg_July17_2001\\morning\\road1-001.log", "rb");
  bytes_per_camera_frame = 20;
  fseek(logfile, 0, SEEK_END);
  logfile_size = ftell(logfile);
  num_camera_frames = logfile_size / bytes_per_camera_frame;

  start_camera_flag = FALSE;
  //  camframetimes = make_CR_Vector("camframetimes", camera_movie->lastframe + 1);
  camframetimes = make_CR_Vector("camframetimes", num_camera_frames);

  //  for (i = 0; i <= camera_movie->lastframe; i++) {
  for (i = 0; i < num_camera_frames; i++) {

    fseek(logfile, i * bytes_per_camera_frame, SEEK_SET);
    fread(&time_seconds, 4, 1, logfile);
    fread(&time_microseconds, 4, 1, logfile); 
    fread(&total_frames_written, 4, 1, logfile);
    fread(&total_incomplete_frames, 4, 1, logfile);
    
    this_time = time_seconds + 0.000001 * time_microseconds;
    
    if (!start_camera_flag) {
      start_camera_time = this_time;
      start_camera_flag = TRUE;
    }

    camera_timestamp = this_time - start_camera_time;
    camframetimes->x[i] = camera_timestamp;

//     if (i < 5000)
//       printf("%i %i: %lf\n", i, total_frames_written, camera_timestamp); 
    
    //    if (i > 4700 && i < 5000)
    //      printf("camframe %i time = %lf\n", i, camframetimes->x[i]);
  }

  //  printf("num_camera_frames = %i\n", num_camera_frames);
  camera_startframe = 4016;
  printf("camframe %i time = %lf\n", camera_startframe, camframetimes->x[camera_startframe]);

  // open laser movie

  //  laser_movie = read_demo3_laser_to_CR_Movie("C:\\InputData\\itg_July17_2001\\cal2-001.fw");
  laser_movie = read_demo3_laser_to_CR_Movie("C:\\InputData\\itg_July17_2001\\morning\\road1-001.fw");

//   printf("first = %i, last = %i\n", laser_movie->firstframe, laser_movie->lastframe);
//   write_CR_Movie_to_demo3_laser("test.fw", laser_movie->firstframe, laser_movie->lastframe, laser_movie);
// 
//   exit(1);

  start_laser_time = laser_movie->laserim[0].navdat[0].time;

  laser_movie->currentframe = 0;
  do {
    get_frame_CR_Movie(laser_movie, laser_movie->currentframe);
    laser_timestamp = 0.001 * (laser_movie->laserim[laser_movie->currentframe].navdat[0].time - start_laser_time);
    laser_movie->currentframe++;
  } while (laser_timestamp < camframetimes->x[camera_startframe]);

  laser_timestamp_last = 0.001 * (laser_movie->laserim[laser_movie->currentframe - 2].navdat[0].time - start_laser_time);

  if (fabs(laser_timestamp - camframetimes->x[camera_startframe]) <
      fabs(laser_timestamp_last - camframetimes->x[camera_startframe]))
    laser_movie->currentframe = laser_movie->currentframe - 1;
  else
    laser_movie->currentframe = laser_movie->currentframe - 2;

  printf("laser current %i = %lf\n", laser_movie->currentframe, 0.001 * (laser_movie->laserim[laser_movie->currentframe].navdat[0].time - start_laser_time));

  laser_image = make_CR_Image(laser_movie->im->w, laser_movie->im->h, 0);

  // filter bins

  tc_hist_rgb_num_bins = 10;
  tc_hist_rgb_binsize = 255 / (double) tc_hist_rgb_num_bins;
  tc_filter_inverse_area = 1.0 / (double) (tc_filter_size * tc_filter_size);

  for (j = tc_top, tc_x_count = tc_y_count = 0; j <= tc_bottom; j += tc_filter_size) {
    for (i = tc_left; i <= tc_right; i += tc_filter_size) {
      if (j == tc_top)
	tc_x_count++;
    }
    tc_y_count++;
  }
  tc_count = tc_x_count * tc_y_count;
  printf("x = %i, y = %i, total = %i\n", tc_x_count, tc_y_count, tc_count);
  tc_laser_bin_count = make_all_CR_Matrix("laser_bin_count", tc_y_count, tc_x_count, 0.0);
  tc_laser_bin_x = make_all_CR_Matrix("laser_bin_x", tc_y_count, tc_x_count, 0.0);
  tc_laser_bin_y = make_all_CR_Matrix("laser_bin_y", tc_y_count, tc_x_count, 0.0);
  tc_laser_bin_z = make_all_CR_Matrix("laser_bin_z", tc_y_count, tc_x_count, 0.0);
  tc_hist_r = (CR_Vector ***) CR_calloc(tc_y_count, sizeof(CR_Vector **));
  tc_hist_g = (CR_Vector ***) CR_calloc(tc_y_count, sizeof(CR_Vector **));
  tc_hist_b = (CR_Vector ***) CR_calloc(tc_y_count, sizeof(CR_Vector **));
  for (j = 0; j < tc_y_count; j++) {
    tc_hist_r[j] = (CR_Vector **) CR_calloc(tc_x_count, sizeof(CR_Vector *));
    tc_hist_g[j] = (CR_Vector **) CR_calloc(tc_x_count, sizeof(CR_Vector *));
    tc_hist_b[j] = (CR_Vector **) CR_calloc(tc_x_count, sizeof(CR_Vector *));
    for (i = 0; i < tc_x_count; i++) {
      tc_hist_r[j][i] = make_CR_Vector("hist_r", tc_hist_rgb_num_bins);
      tc_hist_g[j][i] = make_CR_Vector("hist_g", tc_hist_rgb_num_bins);
      tc_hist_b[j][i] = make_CR_Vector("hist_b", tc_hist_rgb_num_bins);
    }
  }

  current_poly = 0;

  if (tc_write_data) {
    tc_data_fp = fopen("tc_fulldata.README", "w");
    fprintf(tc_data_fp, "bin size is 15 x 15\n");
    fprintf(tc_data_fp, "label, x, y, camera_x, camera_y, camera_z, 10 bins of R, 10 bins of G, 10 bins of B\n");
    fclose(tc_data_fp);
    tc_data_fp = fopen("tc_fulldata.dat", "w");
  }

  // finish

  return im;
}

//----------------------------------------------------------------------------

void TestCalibration_grab_CR_CG(CR_Image *im)
{
  int i, j, r, g, b, xc, yc, index, temp, xbin, ybin, u, v, x, y;
  static char laser_frame[80], camera_frame[80], laser_time[80], camera_time[80];
  double laser_timestamp, xcam, ycam, zcam, count;
  
//   glClearColor(0, 0, 0, 0);
//   glClear(GL_COLOR_BUFFER_BIT);

  // draw laser image

  glPixelZoom(TestCalibration_zoom_factor, TestCalibration_zoom_factor);
  glRasterPos2i(0, camera_movie->im->h);
  
  vflip_CR_Image(laser_movie->im, laser_image);
  glDrawPixels(laser_image->w, laser_image->h, GL_RGB, GL_UNSIGNED_BYTE, laser_image->data);

  sprintf(laser_frame, "%i", laser_movie->currentframe);
  ogl_draw_string(laser_frame, 10, camera_movie->im->h + 20, 255, 255, 0); 

  laser_timestamp = 0.001 * (laser_movie->laserim[laser_movie->currentframe].navdat[0].time - start_laser_time);
  sprintf(laser_time, "%.3lf", laser_timestamp);
  ogl_draw_string(laser_time, 10, camera_movie->im->h + 8, 255, 255, 0); 

//   sprintf(laser_time, "tilt = %.3lf, h = %.3lf", laser_movie->laserim[laser_movie->currentframe].frameHeaders[0].tiltAngle, laser_movie->laserim[laser_movie->currentframe].navdat[0].h);
//   ogl_draw_string(laser_time, camera_movie->im->w - 180, camera_movie->im->h + 20, 255, 255, 0); 
// 
//   sprintf(laser_time, "r = %.3lf, p = %.3lf", laser_movie->laserim[laser_movie->currentframe].navdat[0].roll, laser_movie->laserim[laser_movie->currentframe].navdat[0].pitch);
//   ogl_draw_string(laser_time, camera_movie->im->w - 180, camera_movie->im->h + 8, 255, 255, 0); 

  // sync camera image with laser image

  //  printf("current camera frame = %i, time = %lf, laser time = %lf\n", camera_movie->currentframe, camframetimes->x[camera_movie->currentframe + camera_startframe], laser_timestamp);

  for ( ; camera_movie->currentframe < camera_movie->lastframe && camframetimes->x[camera_movie->currentframe + camera_startframe] < laser_timestamp; camera_movie->currentframe++);
  if (camera_movie->currentframe > 0 &&
      fabs(camframetimes->x[camera_movie->currentframe + camera_startframe] - laser_timestamp) > 
      fabs(camframetimes->x[camera_movie->currentframe + camera_startframe - 1] - laser_timestamp))
    camera_movie->currentframe = camera_movie->currentframe - 1; 
  get_frame_CR_Movie(camera_movie, camera_movie->currentframe);

  // draw camera image

  glPixelZoom(1, 1);
  glRasterPos2i(0, 0);
   
  vflip_CR_Image(camera_movie->im, camera_image);
  glDrawPixels(camera_image->w, camera_image->h, GL_RGB, GL_UNSIGNED_BYTE, camera_image->data);

  sprintf(camera_frame, "%i", camera_movie->currentframe);
  ogl_draw_string(camera_frame, 10, 20, 255, 255, 0); 

  sprintf(camera_time, "%.3lf", camframetimes->x[camera_movie->currentframe]);
  ogl_draw_string(camera_time, 10, 8, 255, 255, 0); 

  // draw laser2camera

  if (!tc_write_data || 
      current_poly < num_polys && camera_movie->currentframe == MATRC(polyinfo[current_poly][0], 0, 0)) {

    if (tc_write_data)
      printf("%i %i\n", current_poly, camera_movie->currentframe);

    // zero out filter responses/histograms

    set_all_CR_Matrix(0.0, tc_laser_bin_count);
    set_all_CR_Matrix(0.0, tc_laser_bin_x);
    set_all_CR_Matrix(0.0, tc_laser_bin_y);
    set_all_CR_Matrix(0.0, tc_laser_bin_z);
    for (j = 0; j < tc_laser_bin_count->rows; j++)
      for (i = 0; i < tc_laser_bin_count->cols; i++) {
	set_all_CR_Vector(0.0, tc_hist_r[j][i]);
	set_all_CR_Vector(0.0, tc_hist_g[j][i]);
	set_all_CR_Vector(0.0, tc_hist_b[j][i]);
      }

    for (j = 0, index = 0; j < D3_LASER_HEIGHT; j++)
      for (i = 0; i < D3_LASER_WIDTH; i++) {
	
	//      old_laser2camera(i, j, laser_movie->laserim[laser_movie->currentframe].data[index], &xc, &yc);
	laser2camera(i, j, laser_movie->laserim[laser_movie->currentframe].data[index], &xc, &yc, &xcam, &ycam, &zcam, laser_movie->laserim[laser_movie->currentframe].frameHeaders[0].tiltAngle);
	
	//      printf("xcam = %lf, ycam = %lf, zcam = %lf\n", xcam, ycam, zcam);
	
	// draw laser point
	
	temp = 255 - laser_movie->laserim[laser_movie->currentframe].data[index] / 4;
	ogl_draw_point(xc, camera_movie->height - 1 - yc, 2, temp, temp, temp);
	
	// bin laser point
	
	xbin = (xc - tc_left) / tc_filter_size;
	ybin = (yc - tc_top) / tc_filter_size;
	//      ybin = (yc - tc_top) / tc_filter_size;
	//      printf("xc = %i, yc = %i, xbin = %i, ybin = %i\n", xc, yc, xbin, ybin);
	if (xbin >= 0 && ybin >= 0 && xbin < tc_laser_bin_count->cols && ybin < tc_laser_bin_count->rows) {
	  MATRC(tc_laser_bin_count, ybin, xbin) = MATRC(tc_laser_bin_count, ybin, xbin) + 1.0;
	  MATRC(tc_laser_bin_x, ybin, xbin) = MATRC(tc_laser_bin_x, ybin, xbin) + xcam;
	  MATRC(tc_laser_bin_y, ybin, xbin) = MATRC(tc_laser_bin_y, ybin, xbin) + ycam;
	  MATRC(tc_laser_bin_z, ybin, xbin) = MATRC(tc_laser_bin_z, ybin, xbin) + zcam;
	}

	// update index 

	index++;
      }
 
    // filter locations
    
    for (j = 0; j < tc_laser_bin_count->rows; j++) 
      for (i = 0; i < tc_laser_bin_count->cols; i++) {

	MATRC(tc_laser_bin_x, j, i) = MATRC(tc_laser_bin_x, j, i) / MATRC(tc_laser_bin_count, j, i);
	MATRC(tc_laser_bin_y, j, i) = MATRC(tc_laser_bin_y, j, i) / MATRC(tc_laser_bin_count, j, i);
	MATRC(tc_laser_bin_z, j, i) = MATRC(tc_laser_bin_z, j, i) / MATRC(tc_laser_bin_count, j, i);

	x = tc_left + i * tc_filter_size;
	y = tc_top + j * tc_filter_size;

	for (v = y - tc_filter_radius; v <= y + tc_filter_radius; v++)
	  for (u = x - tc_filter_radius; u <= x + tc_filter_radius; u++) {
	    tc_hist_r[j][i]->x[(int) ((double) ((unsigned char) IMXY_R(camera_movie->im, u, v)) / tc_hist_rgb_binsize)]++;
	    tc_hist_g[j][i]->x[(int) ((double) ((unsigned char) IMXY_G(camera_movie->im, u, v)) / tc_hist_rgb_binsize)]++;
	    tc_hist_b[j][i]->x[(int) ((double) ((unsigned char) IMXY_B(camera_movie->im, u, v)) / tc_hist_rgb_binsize)]++;
	  }
	scale_CR_Vector(tc_filter_inverse_area, tc_hist_r[j][i]);
	scale_CR_Vector(tc_filter_inverse_area, tc_hist_g[j][i]);
	scale_CR_Vector(tc_filter_inverse_area, tc_hist_b[j][i]);

	if (tc_write_data) {

	  // output bin label
	  if (point_in_poly_CR_Matrix((double) x, (double) y, polyinfo[current_poly][1])) {
	    fprintf(tc_data_fp, "1.0,");
	    ogl_draw_point(x, im->h - (64 + y), 5, 0, 255, 0);
	  }
	  else {
	    fprintf(tc_data_fp, "0.0,");
	    ogl_draw_point(x, im->h - (64 + y), 5, 255, 0, 0);
	  }

	  // output laser features

	  fprintf(tc_data_fp, "%lf,%lf,%lf,%lf,%lf,", (double) x, (double) y, MATRC(tc_laser_bin_x, j, i), MATRC(tc_laser_bin_y, j, i), MATRC(tc_laser_bin_z, j, i));
	  
	// output color histogram features

	  for (u = 0; u < tc_hist_rgb_num_bins; u++)
	    fprintf(tc_data_fp, "%lf,", tc_hist_r[j][i]->x[u]);
	  for (u = 0; u < tc_hist_rgb_num_bins; u++)
	    fprintf(tc_data_fp, "%lf,", tc_hist_g[j][i]->x[u]);
	  for (u = 0; u < tc_hist_rgb_num_bins - 1; u++)
	    fprintf(tc_data_fp, "%lf,", tc_hist_b[j][i]->x[u]);
	  fprintf(tc_data_fp, "%lf\n", tc_hist_b[j][i]->x[tc_hist_rgb_num_bins - 1]);
	}
      }

//     for (j = tc_top; j <= tc_bottom; j += tc_filter_size) 
//       for (i = tc_left; i <= tc_right; i += tc_filter_size) {       
// 	ogl_draw_rectangle(i - tc_filter_radius, im->h - (64 + j) - tc_filter_radius, 
// 			   i + tc_filter_radius, im->h - (64 + j) + tc_filter_radius, 
// 			   255, 255, 0);
//       }

    current_poly++;
  }
  else if (current_poly >= num_polys && tc_write_data) {
    fclose(tc_data_fp);
    CR_exit(1);
  }

  // advance frame

  if (laser_movie->currentframe < laser_movie->lastframe &&
      camera_movie->currentframe < camera_movie->lastframe) 
    get_frame_CR_Movie(laser_movie, laser_movie->currentframe++);
}

//----------------------------------------------------------------------------

void TestCalibration_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  fclose(good_road_fp);
  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_NeuralNet *dirtnet;
CR_Vector *dirt_input;
int dirt_color_offset;
CR_Matrix *dirt_output, *dirt_output_cc, *dirt_roadness;
CR_Vector *road_quadsurf;

double start_tranRel_flag, start_tranRelx, start_tranRely, start_tranRelz;

//----------------------------------------------------------------------------

void TrackDirtRoad_initialize_filters(int filter_size, int left, int top, int right, int bottom)
{
  int i, j;

  tc_filter_size = filter_size;
  tc_filter_radius = (filter_size - 1) / 2;
  tc_top = top;
  tc_bottom = bottom;
  tc_left = left;
  tc_right = right;

  tc_hist_rgb_num_bins = 10;
  tc_hist_rgb_binsize = 255 / (double) tc_hist_rgb_num_bins;
  tc_filter_inverse_area = 1.0 / (double) (tc_filter_size * tc_filter_size);

  for (j = tc_top, tc_x_count = tc_y_count = 0; j <= tc_bottom; j += tc_filter_size) {
    for (i = tc_left; i <= tc_right; i += tc_filter_size) {
      if (j == tc_top)
	tc_x_count++;
    }
    tc_y_count++;
  }
  tc_count = tc_x_count * tc_y_count;
  printf("x = %i, y = %i, total = %i\n", tc_x_count, tc_y_count, tc_count);
  tc_laser_bin_count = make_all_CR_Matrix("laser_bin_count", tc_y_count, tc_x_count, 0.0);
  tc_laser_bin_x = make_all_CR_Matrix("laser_bin_x", tc_y_count, tc_x_count, 0.0);
  tc_laser_bin_y = make_all_CR_Matrix("laser_bin_y", tc_y_count, tc_x_count, 0.0);
  tc_laser_bin_z = make_all_CR_Matrix("laser_bin_z", tc_y_count, tc_x_count, 0.0);
  laser_bin_x = make_CR_Vector("laser_x", tc_y_count * tc_x_count);
  laser_bin_y = make_CR_Vector("laser_y", tc_y_count * tc_x_count);
  laser_bin_z = make_CR_Vector("laser_z", tc_y_count * tc_x_count);
  laser_bin_roadness = make_CR_Vector("laser_roadness", tc_y_count * tc_x_count);
  tc_hist_r = (CR_Vector ***) CR_calloc(tc_y_count, sizeof(CR_Vector **));
  tc_hist_g = (CR_Vector ***) CR_calloc(tc_y_count, sizeof(CR_Vector **));
  tc_hist_b = (CR_Vector ***) CR_calloc(tc_y_count, sizeof(CR_Vector **));
  for (j = 0; j < tc_y_count; j++) {
    tc_hist_r[j] = (CR_Vector **) CR_calloc(tc_x_count, sizeof(CR_Vector *));
    tc_hist_g[j] = (CR_Vector **) CR_calloc(tc_x_count, sizeof(CR_Vector *));
    tc_hist_b[j] = (CR_Vector **) CR_calloc(tc_x_count, sizeof(CR_Vector *));
    for (i = 0; i < tc_x_count; i++) {
      tc_hist_r[j][i] = make_CR_Vector("hist_r", tc_hist_rgb_num_bins);
      tc_hist_g[j][i] = make_CR_Vector("hist_g", tc_hist_rgb_num_bins);
      tc_hist_b[j][i] = make_CR_Vector("hist_b", tc_hist_rgb_num_bins);
    }
  }
}

//----------------------------------------------------------------------------

// zero out filter responses/histograms

void TrackDirtRoad_reset_filters()
{
  int i, j;

  set_all_CR_Matrix(0.0, tc_laser_bin_count);
  set_all_CR_Matrix(0.0, tc_laser_bin_x);
  set_all_CR_Matrix(0.0, tc_laser_bin_y);
  set_all_CR_Matrix(0.0, tc_laser_bin_z);
  for (j = 0; j < tc_laser_bin_count->rows; j++)
    for (i = 0; i < tc_laser_bin_count->cols; i++) {
      set_all_CR_Vector(0.0, tc_hist_r[j][i]);
      set_all_CR_Vector(0.0, tc_hist_g[j][i]);
      set_all_CR_Vector(0.0, tc_hist_b[j][i]);
    }
}

//----------------------------------------------------------------------------

// can't just plug in movie->laserim[laser_movie->currentframe].data because it's
// an array of shorts and itg_frame_data is an array of ints

void TrackDirtRoad_filter_and_classify(double tilt, CR_Image *camera_im, int *laser_frame_data)
{
  int i, j, r, g, b, xc, yc, index, temp, xbin, ybin, u, v, x, y;
  double xcam, ycam, zcam, count;

  TrackDirtRoad_reset_filters();

  // run filters and classify

  for (j = 0, index = 0; j < D3_LASER_HEIGHT; j++)
    for (i = 0; i < D3_LASER_WIDTH; i++) {
      
      laser2camera(i, j, laser_frame_data[index], &xc, &yc, &xcam, &ycam, &zcam, tilt);
      
      // draw laser point
      
      //       temp = 255 - laser_movie->laserim[laser_movie->currentframe].data[index] / 4;
      //       ogl_draw_point(xc, camera_movie->height - 1 - yc, 2, temp, temp, temp);
      
      // bin laser point

      xbin = (xc - tc_left) / tc_filter_size;
      ybin = (yc - tc_top) / tc_filter_size;
      
      if (xbin >= 0 && ybin >= 0 && xbin < tc_laser_bin_count->cols && ybin < tc_laser_bin_count->rows) {
	MATRC(tc_laser_bin_count, ybin, xbin) = MATRC(tc_laser_bin_count, ybin, xbin) + 1.0;
	MATRC(tc_laser_bin_x, ybin, xbin) = MATRC(tc_laser_bin_x, ybin, xbin) + xcam;
	MATRC(tc_laser_bin_y, ybin, xbin) = MATRC(tc_laser_bin_y, ybin, xbin) + ycam;
	MATRC(tc_laser_bin_z, ybin, xbin) = MATRC(tc_laser_bin_z, ybin, xbin) + zcam;
      }

      // update index 
      
      index++;
    }

  // change laser bins from counts to means and histogram color
    
  for (j = 0; j < tc_laser_bin_count->rows; j++) 
    for (i = 0; i < tc_laser_bin_count->cols; i++) {
	
      MATRC(tc_laser_bin_x, j, i) = MATRC(tc_laser_bin_x, j, i) / MATRC(tc_laser_bin_count, j, i);
      MATRC(tc_laser_bin_y, j, i) = MATRC(tc_laser_bin_y, j, i) / MATRC(tc_laser_bin_count, j, i);
      MATRC(tc_laser_bin_z, j, i) = MATRC(tc_laser_bin_z, j, i) / MATRC(tc_laser_bin_count, j, i);
      
      x = tc_left + i * tc_filter_size;
      y = tc_top + j * tc_filter_size;
      
      for (v = y - tc_filter_radius; v <= y + tc_filter_radius; v++)
	for (u = x - tc_filter_radius; u <= x + tc_filter_radius; u++) {
	  tc_hist_r[j][i]->x[(int) ((double) ((unsigned char) IMXY_R(camera_im, u, v)) / tc_hist_rgb_binsize)]++;
	  tc_hist_g[j][i]->x[(int) ((double) ((unsigned char) IMXY_G(camera_im, u, v)) / tc_hist_rgb_binsize)]++;
	  tc_hist_b[j][i]->x[(int) ((double) ((unsigned char) IMXY_B(camera_im, u, v)) / tc_hist_rgb_binsize)]++;
	}
      scale_CR_Vector(tc_filter_inverse_area, tc_hist_r[j][i]);
      scale_CR_Vector(tc_filter_inverse_area, tc_hist_g[j][i]);
      scale_CR_Vector(tc_filter_inverse_area, tc_hist_b[j][i]);
      
      // put laser features in NN input vector
      
      dirt_input->x[0] = MATRC(tc_laser_bin_y, j, i);
      
      
//	dirt_input->x[0] = MATRC(tc_laser_bin_x, j, i);
//	dirt_input->x[1] = MATRC(tc_laser_bin_y, j, i);
//	dirt_input->x[2] = MATRC(tc_laser_bin_z, j, i);
      
      
      // put color histogram features in NN input vector
      
      for (u = 0; u < tc_hist_rgb_num_bins; u++)
	dirt_input->x[dirt_color_offset + u] = tc_hist_r[j][i]->x[u];
      for (u = 0; u < tc_hist_rgb_num_bins; u++)
	dirt_input->x[dirt_color_offset + tc_hist_rgb_num_bins + u] = tc_hist_g[j][i]->x[u];
      for (u = 0; u < tc_hist_rgb_num_bins; u++)
	dirt_input->x[dirt_color_offset + 2 * tc_hist_rgb_num_bins + u] = tc_hist_b[j][i]->x[u];
      
      // run NN, get label
      
      simulate_CR_NeuralNet(dirt_input, dirtnet);
      
      // draw label (road => green point, non-road => nothing)
      
      MATRC(dirt_roadness, j, i) = dirtnet->I[dirtnet->numlayers - 1]->x[0];

      if (MATRC(dirt_roadness, j, i) >= 0.5) 
	MATRC(dirt_output, j, i) = 1.0;
      else 
	MATRC(dirt_output, j, i) = 0.0;
    }
}

//----------------------------------------------------------------------------

// char TrackDirtRoad_camera_movie_name[256] = "C:\\InputData\\itg_July17_2001\\morning\\cut_road1-001.avi";
// char TrackDirtRoad_laser_movie_name[256] = "C:\\InputData\\itg_July17_2001\\morning\\road1-001.fw";
// char TrackDirtRoad_log_name[256] = "C:\\InputData\\itg_July17_2001\\morning\\road1-001.log";
// int TrackDirtRoad_camera_start_frame = 4016;

char TrackDirtRoad_camera_movie_name[256] = "C:\\InputData\\itg_July17_2001\\morning\\half_road3-002.avi";
char TrackDirtRoad_laser_movie_name[256] = "C:\\InputData\\itg_July17_2001\\morning\\road3-002.fw";
char TrackDirtRoad_log_name[256] = "C:\\InputData\\itg_July17_2001\\morning\\road3-002.log";
int TrackDirtRoad_camera_start_frame = 0;

int *itg_good_facet;
int *itg_frame_data;

FILE *run_fp;
int run_num;
char *itg_capture_string;

CR_Image *TrackDirtRoad_init_CR_CG(char *name)
{
  CR_Image *im;
  double this_time, laser_timestamp, laser_timestamp_last, camera_timestamp;
  int i, j;
  int num_logs;
  
  if (itg_capture_interval) {
    run_fp = fopen("captures\\run.txt", "r");
    fscanf(run_fp, "%i", &run_num);
    fclose(run_fp);
    run_fp = fopen("captures\\run.txt", "w");
    fprintf(run_fp, "%i\n", run_num + 1);
    fclose(run_fp);
    itg_capture_string = (char *) CR_calloc(128, sizeof(char));
  }

  itg_frame = 0;

  itg_good_facet = (int *) CR_calloc(D3_LASER_NUM_SCANLINES, sizeof(int));
  for (i = 0; i < D3_LASER_NUM_SCANLINES; i++) {
    itg_good_facet[i] = TRUE;
  }

  itg_frame_data = (int *) CR_calloc(D3_LASER_WIDTH * D3_LASER_HEIGHT, sizeof(int));


  //  itg_display = FALSE;
  //  itg_nml = FALSE;

//   itg_display = TRUE;
//   itg_nml = TRUE;

  //  NIST_Road_pick_random_frames(100, "//C/InputData/itg_July17_2001/morning/", "itg_July17_2001_morning_100.txt");
  //  exit(1);

  // load neural network

  //  dirtnet = read_CR_NeuralNet("road1_laser_15.net");
  //  dirtnet = read_CR_NeuralNet("road1_color_15.net");

//   dirtnet = read_CR_NeuralNet("road1_bigcolor_15.net");
//   dirt_color_offset = 0;

  dirtnet = read_CR_NeuralNet("road1_bigboth_15.net");
  dirt_color_offset = 1;

  //  dirtnet = read_CR_NeuralNet("road1_both_15.net");
  //  dirt_color_offset = 3;
  dirt_input = make_CR_Vector("dirt_input", dirtnet->W[0]->cols);

  road_quadsurf = make_CR_Vector("road_quadsurf", 6);

  // open camera movie

  //  camera_movie = open_CR_Movie("C:\\InputData\\itg_July17_2001\\xuv_laser_camera_calib.avi");
  //  camera_movie = open_CR_Movie("C:\\InputData\\itg_July17_2001\\morning\\cut_road1-001.avi");
  camera_movie = open_CR_Movie(TrackDirtRoad_camera_movie_name);

  camera_movie->currentframe = 0;
  get_frame_CR_Movie(camera_movie, camera_movie->currentframe);

  camera_image = make_CR_Image(camera_movie->im->w, camera_movie->im->h, 0);

  // initialize window size and laser to camera transformation

  TestCalibration_zoom_factor = camera_movie->width / D3_LASER_WIDTH;
  printf("tc zoom = %i\n", TestCalibration_zoom_factor);
  im = make_CR_Image(camera_movie->width, camera_movie->height + D3_LASER_HEIGHT * TestCalibration_zoom_factor, 0);
  init_laser2camera(camera_movie->im->w, camera_movie->im->h);

  // check logfile

  //  logfile = fopen("cal2-001.log", "rb");
  //  logfile = fopen("C:\\InputData\\itg_July17_2001\\morning\\road1-001.log", "rb");
  logfile = fopen(TrackDirtRoad_log_name, "rb");

  bytes_per_camera_frame = 20;
  fseek(logfile, 0, SEEK_END);
  logfile_size = ftell(logfile);
  num_camera_frames = logfile_size / bytes_per_camera_frame;

  start_camera_flag = FALSE;
  //  camframetimes = make_CR_Vector("camframetimes", camera_movie->lastframe + 1);
  camframetimes = make_CR_Vector("camframetimes", num_camera_frames);

  //  for (i = 0; i <= camera_movie->lastframe; i++) {
  for (i = 0; i < num_camera_frames; i++) {

    fseek(logfile, i * bytes_per_camera_frame, SEEK_SET);
    fread(&time_seconds, 4, 1, logfile);
    fread(&time_microseconds, 4, 1, logfile); 
    fread(&total_frames_written, 4, 1, logfile);
    fread(&total_incomplete_frames, 4, 1, logfile);
    
    this_time = time_seconds + 0.000001 * time_microseconds;
    
    if (!start_camera_flag) {
      start_camera_time = this_time;
      start_camera_flag = TRUE;
    }

    camera_timestamp = this_time - start_camera_time;
    camframetimes->x[i] = camera_timestamp;

//     if (i < 5000)
//       printf("%i %i: %lf\n", i, total_frames_written, camera_timestamp); 
    
    //    if (i > 4700 && i < 5000)
    //      printf("camframe %i time = %lf\n", i, camframetimes->x[i]);
  }

  //  printf("num_camera_frames = %i\n", num_camera_frames);
  //  camera_startframe = 4016;
  camera_startframe = TrackDirtRoad_camera_start_frame;

  printf("camframe %i time = %lf\n", camera_startframe, camframetimes->x[camera_startframe]);

  // open laser movie

  //  laser_movie = read_demo3_laser_to_CR_Movie("C:\\InputData\\itg_July17_2001\\cal2-001.fw");
  //  laser_movie = read_demo3_laser_to_CR_Movie("C:\\InputData\\itg_July17_2001\\morning\\road1-001.fw");
  laser_movie = read_demo3_laser_to_CR_Movie(TrackDirtRoad_laser_movie_name);

//   printf("first = %i, last = %i\n", laser_movie->firstframe, laser_movie->lastframe);
//   write_CR_Movie_to_demo3_laser("test.fw", laser_movie->firstframe, laser_movie->lastframe, laser_movie);
// 
//   exit(1);

  start_laser_time = laser_movie->laserim[0].navdat[0].time;

  laser_movie->currentframe = 0;
  do {
    get_frame_CR_Movie(laser_movie, laser_movie->currentframe);
    laser_timestamp = 0.001 * (laser_movie->laserim[laser_movie->currentframe].navdat[0].time - start_laser_time);
    laser_movie->currentframe++;
  } while (laser_timestamp < camframetimes->x[camera_startframe]);

  CR_flush_printf("frame = %i\n", laser_movie->currentframe);

  if (laser_movie->currentframe >= 2) {

    laser_timestamp_last = 0.001 * (laser_movie->laserim[laser_movie->currentframe - 2].navdat[0].time - start_laser_time);
    
    if (fabs(laser_timestamp - camframetimes->x[camera_startframe]) <
	fabs(laser_timestamp_last - camframetimes->x[camera_startframe]))
      laser_movie->currentframe = laser_movie->currentframe - 1;
    else
      laser_movie->currentframe = laser_movie->currentframe - 2;
  }
  else
    laser_movie->currentframe = 0;

  printf("laser current %i = %lf\n", laser_movie->currentframe, 0.001 * (laser_movie->laserim[laser_movie->currentframe].navdat[0].time - start_laser_time));

  laser_image = make_CR_Image(laser_movie->im->w, laser_movie->im->h, 0);

  // filter bins

  TrackDirtRoad_initialize_filters(15, 14, 70, 348, 215);

  dirt_roadness = make_CR_Matrix("dirt_roadness", tc_y_count, tc_x_count);
  dirt_output = make_CR_Matrix("dirt_output", tc_y_count, tc_x_count);
  dirt_output_cc = make_CR_Matrix("dirt_output_cc", tc_y_count, tc_x_count);

  // map

#ifndef DUMB_HACK
  start_tranRel_flag = TRUE;
  if (itg_nml)
    demo3_tsaimap_initialize();
#endif

  // finish

  //  track_3d_mov = write_avi_initialize_CR_Movie(NULL, im->w, im->h, 30);

  CR_flush_printf("done initializing\n");

  return im;
}

//----------------------------------------------------------------------------

// constructs a filename of the form "<prefix>num<suffix>", where num
// is formatted to occupy 3 digits, padded with leading 0's if necessary

char *construct_4_digit_filename(int num, char *prefix, char *suffix)
{
  char *result;

  if (num < 0 || num > 9999) {
    CR_flush_printf("%i\n", num);
    CR_error("construct_4_digit_filename(): number out of range");
  }

  result = (char *) CR_calloc(strlen(prefix) + strlen(suffix) + 4, sizeof(char));

  if (num < 10)
    sprintf(result, "%s000%i%s", prefix, num, suffix);
  else if (num < 100)
    sprintf(result, "%s00%i%s", prefix, num, suffix);
  else if (num < 1000)
    sprintf(result, "%s0%i%s", prefix, num, suffix);
  else 
    sprintf(result, "%s%i%s", prefix, num, suffix);

  return result;
}

//----------------------------------------------------------------------------

void ITG_capture_camera_and_laser(int frame, CR_Image *camera_im, int *frame_data, int *good_facet, 
				  double time, double tilt, 
				  double tranRelx, double tranRely, double tranRelz, 
				  double tranAbsx, double tranAbsy, double tranAbsz, 
				  double rots, double rotx, double roty, double rotz)
{
  FILE *nav_fp;
  int i, j, index;

  // laser and navigation data go in one file...

  sprintf(itg_capture_string, "captures\\navlaser_%i_", run_num);
  nav_fp = fopen(construct_4_digit_filename(frame, itg_capture_string, ".txt"), "w");
  fprintf(nav_fp, "time = %lf\ntilt = %lf\nRx = %lf, Ry = %lf, Rz = %lf\nAx = %lf, Ay = %lf, Az = %lf\nrots = %lf, rotx = %lf, roty = %lf, rotz = %lf\n",
	  time, tilt, tranRelx, tranRely, tranRelz, tranAbsx, tranAbsy, tranAbsz, rots, rotx, roty, rotz);
  for (i = 0; i < D3_LASER_NUM_SCANLINES; i++)
    fprintf(nav_fp, "%i %i\n", i, good_facet[i]);
  for (j = 0, index = 0; j < D3_LASER_HEIGHT; j++) {
    for (i = 0; i < D3_LASER_WIDTH; i++) {
      fprintf(nav_fp, "%i ", frame_data[index]);
      index++;
    }
    fprintf(nav_fp, "\n");
  }

  fclose(nav_fp);

  // ...and camera image goes in another

  sprintf(itg_capture_string, "captures\\camera_%i_", run_num);
  write_CR_Image(construct_4_digit_filename(frame, itg_capture_string, ".ppm"), camera_im);
}

//----------------------------------------------------------------------------

void TrackDirtRoad_grab_CR_CG(CR_Image *im)
{
  int i, j, r, g, b, xc, yc, index, temp, xbin, ybin, u, v, x, y, num_road_points;
  static char laser_frame[80], camera_frame[80], laser_time[80], camera_time[80];
  double laser_timestamp, xcam, ycam, zcam, count;

  //  CR_sleep(0.6);
  
  // draw laser image

  if (itg_display) {
    glPixelZoom(TestCalibration_zoom_factor, TestCalibration_zoom_factor);
    glRasterPos2i(0, camera_movie->im->h);
  
    vflip_CR_Image(laser_movie->im, laser_image);
    glDrawPixels(laser_image->w, laser_image->h, GL_RGB, GL_UNSIGNED_BYTE, laser_image->data);
  }

  sprintf(laser_frame, "%i", laser_movie->currentframe);
  if (itg_display)
    ogl_draw_string(laser_frame, 10, camera_movie->im->h + 20, 255, 255, 0); 

  laser_timestamp = 0.001 * (laser_movie->laserim[laser_movie->currentframe].navdat[0].time - start_laser_time);
  sprintf(laser_time, "%.3lf", laser_timestamp);
  if (itg_display)
    ogl_draw_string(laser_time, 10, camera_movie->im->h + 8, 255, 255, 0); 

  // sync camera image with laser image

  for ( ; camera_movie->currentframe < camera_movie->lastframe && camframetimes->x[camera_movie->currentframe + camera_startframe] < laser_timestamp; camera_movie->currentframe++);
  if (camera_movie->currentframe > 0 &&
      fabs(camframetimes->x[camera_movie->currentframe + camera_startframe] - laser_timestamp) > 
      fabs(camframetimes->x[camera_movie->currentframe + camera_startframe - 1] - laser_timestamp))
    camera_movie->currentframe = camera_movie->currentframe - 1; 
  get_frame_CR_Movie(camera_movie, camera_movie->currentframe);

  // draw camera image

  if (itg_display) {
    glPixelZoom(1, 1);
    glRasterPos2i(0, 0);
   
    vflip_CR_Image(camera_movie->im, camera_image);
    glDrawPixels(camera_image->w, camera_image->h, GL_RGB, GL_UNSIGNED_BYTE, camera_image->data);
  }

  sprintf(camera_frame, "%i", camera_movie->currentframe);
  if (itg_display)
    ogl_draw_string(camera_frame, 10, 20, 255, 255, 0); 

  sprintf(camera_time, "%.3lf", camframetimes->x[camera_movie->currentframe]);
  if (itg_display)
    ogl_draw_string(camera_time, 10, 8, 255, 255, 0); 
  
  TrackDirtRoad_reset_filters();

  // project laser to image and put them in bins

  // image, laser data, tilt angle

  for (j = 0, index = 0; j < D3_LASER_HEIGHT; j++)
    for (i = 0; i < D3_LASER_WIDTH; i++) {

      laser2camera(i, j, laser_movie->laserim[laser_movie->currentframe].data[index], 
		   &xc, &yc, &xcam, &ycam, &zcam, 
		   laser_movie->laserim[laser_movie->currentframe].frameHeaders[0].tiltAngle);

      // draw laser point
      
      //       temp = 255 - laser_movie->laserim[laser_movie->currentframe].data[index] / 4;
      //       ogl_draw_point(xc, camera_movie->height - 1 - yc, 2, temp, temp, temp);
      
      // bin laser point
      
      xbin = (xc - tc_left) / tc_filter_size;
      ybin = (yc - tc_top) / tc_filter_size;
      
      if (xbin >= 0 && ybin >= 0 && xbin < tc_laser_bin_count->cols && ybin < tc_laser_bin_count->rows) {
	MATRC(tc_laser_bin_count, ybin, xbin) = MATRC(tc_laser_bin_count, ybin, xbin) + 1.0;
	MATRC(tc_laser_bin_x, ybin, xbin) = MATRC(tc_laser_bin_x, ybin, xbin) + xcam;
	MATRC(tc_laser_bin_y, ybin, xbin) = MATRC(tc_laser_bin_y, ybin, xbin) + ycam;
	MATRC(tc_laser_bin_z, ybin, xbin) = MATRC(tc_laser_bin_z, ybin, xbin) + zcam;
      }

      // update index 
      
      index++;
    }
 
  // change laser bins from counts to means and histogram color
    
  for (j = 0; j < tc_laser_bin_count->rows; j++) 
    for (i = 0; i < tc_laser_bin_count->cols; i++) {
	
      MATRC(tc_laser_bin_x, j, i) = MATRC(tc_laser_bin_x, j, i) / MATRC(tc_laser_bin_count, j, i);
      MATRC(tc_laser_bin_y, j, i) = MATRC(tc_laser_bin_y, j, i) / MATRC(tc_laser_bin_count, j, i);
      MATRC(tc_laser_bin_z, j, i) = MATRC(tc_laser_bin_z, j, i) / MATRC(tc_laser_bin_count, j, i);
      
      x = tc_left + i * tc_filter_size;
      y = tc_top + j * tc_filter_size;
      
      for (v = y - tc_filter_radius; v <= y + tc_filter_radius; v++)
	for (u = x - tc_filter_radius; u <= x + tc_filter_radius; u++) {
	  tc_hist_r[j][i]->x[(int) ((double) ((unsigned char) IMXY_R(camera_movie->im, u, v)) / tc_hist_rgb_binsize)]++;
	  tc_hist_g[j][i]->x[(int) ((double) ((unsigned char) IMXY_G(camera_movie->im, u, v)) / tc_hist_rgb_binsize)]++;
	  tc_hist_b[j][i]->x[(int) ((double) ((unsigned char) IMXY_B(camera_movie->im, u, v)) / tc_hist_rgb_binsize)]++;
	}
      scale_CR_Vector(tc_filter_inverse_area, tc_hist_r[j][i]);
      scale_CR_Vector(tc_filter_inverse_area, tc_hist_g[j][i]);
      scale_CR_Vector(tc_filter_inverse_area, tc_hist_b[j][i]);
      
      // put laser features in NN input vector
      
      dirt_input->x[0] = MATRC(tc_laser_bin_y, j, i);
      
      /*
	dirt_input->x[0] = MATRC(tc_laser_bin_x, j, i);
	dirt_input->x[1] = MATRC(tc_laser_bin_y, j, i);
	dirt_input->x[2] = MATRC(tc_laser_bin_z, j, i);
      */
      
      // put color histogram features in NN input vector
      
      for (u = 0; u < tc_hist_rgb_num_bins; u++)
	dirt_input->x[dirt_color_offset + u] = tc_hist_r[j][i]->x[u];
      for (u = 0; u < tc_hist_rgb_num_bins; u++)
	dirt_input->x[dirt_color_offset + tc_hist_rgb_num_bins + u] = tc_hist_g[j][i]->x[u];
      for (u = 0; u < tc_hist_rgb_num_bins; u++)
	dirt_input->x[dirt_color_offset + 2 * tc_hist_rgb_num_bins + u] = tc_hist_b[j][i]->x[u];
      
      // run NN, get label
      
      simulate_CR_NeuralNet(dirt_input, dirtnet);
      
      // draw label (road => green point, non-road => nothing)

      //      MATRC(dirt_roadness, j, i) = dirtnet->I[dirtnet->numlayers - 1]->x[0];
      MATRC(dirt_roadness, j, i) = 1.0;
      
      int w = 2;

      //      if (dirtnet->I[dirtnet->numlayers - 1]->x[0] >= 0.5) {
      if (TRUE) {
	  MATRC(dirt_output, j, i) = 1.0;
	if (itg_display)
	  ogl_draw_rectangle(x - w, camera_movie->im->h - 1 - y - w, x + w, camera_movie->im->h - 1 - y + w, 0, 255, 0);
	//	ogl_draw_point(x, camera_movie->im->h - 1 - y, 5, 0, 255, 0);
      }
      else {
	MATRC(dirt_output, j, i) = 0.0;
	if (itg_display)
	  ogl_draw_rectangle(x - w, camera_movie->im->h - 1 - y - w, x + w, camera_movie->im->h - 1 - y + w, 255, 0, 0);
	//	ogl_draw_point(x, camera_movie->im->h - 1 - y, 2, 255, 0, 0);
      }
    }

  // estimate 3-D road shape

  biggest_connected_component_CR_Matrix(TRUE, dirt_output, dirt_output_cc);
  // 	ogl_draw_CR_Matrix(15, 64 + 15, im->h, dirt_output);
  if (itg_display)
    ogl_draw_CR_Matrix(50, 64 + 15, im->h, dirt_output_cc);
    
  for (j = 0, num_road_points = 0; j < tc_laser_bin_count->rows; j++) 
    for (i = 0; i < tc_laser_bin_count->cols; i++) 
      if (MATRC(dirt_output_cc, j, i)) {
	laser_bin_x->x[num_road_points] = MATRC(tc_laser_bin_x, j, i); 
	laser_bin_y->x[num_road_points] = MATRC(tc_laser_bin_y, j, i); 
	laser_bin_z->x[num_road_points] = MATRC(tc_laser_bin_z, j, i); 
	laser_bin_roadness->x[num_road_points] = MATRC(dirt_roadness, j, i);
	num_road_points++; 
      }

  laser_bin_x->rows = laser_bin_y->rows = laser_bin_z->rows = num_road_points;
  
  //  quadratic_surface_fit_CR_Matrix(laser_bin_x, laser_bin_y, laser_bin_z, road_quadsurf);
  //  print_CR_Vector(road_quadsurf);

  //  if (itg_display)
//  draw_road_quadsurf_mesh(-3, 3, 0.5, 0, 50, 0.5, road_quadsurf);

  if (itg_capture_interval && !(itg_frame % itg_capture_interval)) {
    for (i = 0; i < D3_LASER_WIDTH * D3_LASER_HEIGHT; i++)
      itg_frame_data[i] = (int) laser_movie->laserim[laser_movie->currentframe].data[i];
    ITG_capture_camera_and_laser(itg_frame, camera_movie->im, itg_frame_data, itg_good_facet,
				 laser_movie->laserim[laser_movie->currentframe].navdat[0].time, 
				 laser_movie->laserim[laser_movie->currentframe].frameHeaders[0].tiltAngle, 
				 laser_movie->laserim[laser_movie->currentframe].navdat[0].tranRel.x, 
				 laser_movie->laserim[laser_movie->currentframe].navdat[0].tranRel.y, 
				 laser_movie->laserim[laser_movie->currentframe].navdat[0].tranRel.z,
				 laser_movie->laserim[laser_movie->currentframe].navdat[0].tranAbs.x, 
				 laser_movie->laserim[laser_movie->currentframe].navdat[0].tranAbs.y, 
				 laser_movie->laserim[laser_movie->currentframe].navdat[0].tranAbs.z,
				 laser_movie->laserim[laser_movie->currentframe].navdat[0].rot.s, 
				 laser_movie->laserim[laser_movie->currentframe].navdat[0].rot.x, 
				 laser_movie->laserim[laser_movie->currentframe].navdat[0].rot.y, 
				 laser_movie->laserim[laser_movie->currentframe].navdat[0].rot.z);
  }
  itg_frame++;

#ifndef DUMB_HACK
  if (start_tranRel_flag) {
    start_tranRelx = laser_movie->laserim[laser_movie->currentframe].navdat[0].tranRel.x;
    start_tranRely = laser_movie->laserim[laser_movie->currentframe].navdat[0].tranRel.y;
    start_tranRelz = laser_movie->laserim[laser_movie->currentframe].navdat[0].tranRel.z;
    start_tranRel_flag = FALSE;
  }
  if (itg_nml)
    demo3_tsaimap_send_road(laser_bin_x, laser_bin_y, laser_bin_z, laser_bin_roadness,
			laser_movie->laserim[laser_movie->currentframe].navdat[0].time,
			// 		      laser_movie->laserim[laser_movie->currentframe].navdat[0].tranRel.x - start_tranRelx, 
			// 		      laser_movie->laserim[laser_movie->currentframe].navdat[0].tranRel.y - start_tranRely, 
			// 		      laser_movie->laserim[laser_movie->currentframe].navdat[0].tranRel.z - start_tranRelz,
			laser_movie->laserim[laser_movie->currentframe].navdat[0].tranRel.x, 
			laser_movie->laserim[laser_movie->currentframe].navdat[0].tranRel.y, 
			laser_movie->laserim[laser_movie->currentframe].navdat[0].tranRel.z,
			laser_movie->laserim[laser_movie->currentframe].navdat[0].rot.s, 
			laser_movie->laserim[laser_movie->currentframe].navdat[0].rot.x, 
			laser_movie->laserim[laser_movie->currentframe].navdat[0].rot.y, 
			laser_movie->laserim[laser_movie->currentframe].navdat[0].rot.z); 
#endif

  // advance laser frame
  
  if (laser_movie->currentframe < laser_movie->lastframe &&
      camera_movie->currentframe < camera_movie->lastframe) 
    get_frame_CR_Movie(laser_movie, laser_movie->currentframe++);
}

//----------------------------------------------------------------------------

void TrackDirtRoad_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  fclose(good_road_fp);
  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#define MAX_RAW_LASER_DATA_LENGTH       (16*1200)
#define BYTES_PER_FACET                 1022
    
unsigned char *raw_laser_data;
int raw_laser_data_length, num_raw_laser_data_read;

Demo3_NavData *itg_navdat;
Demo3_LaserHeader *itg_frameheader;
short **itg_facet_data;
int itg_facets_per_message, itg_framestart_seqno, itg_framestart_flag, itg_last_seqno, itg_sequence_start_flag;
CR_Image *itg_laser_im, *itg_flipped_laser_im, *itg_camera_im, *itg_flipped_camera_im;
int itg_zoom, itg_camera_w, itg_camera_h;
CR_Live *itg_live;
double itg_last_tilt, itg_last_time;
double itg_last_tranRelx, itg_last_tranRely, itg_last_tranRelz, itg_last_tranAbsx, itg_last_tranAbsy, itg_last_tranAbsz, itg_last_rots, itg_last_rotx, itg_last_roty, itg_last_rotz;

int itg_rowmap[18] = { 12, 20, 0, 28, 16, 12, 24,  0, 0,  0, 8, 16, 4, 24, 20,  8, 28,  4 };
int itg_colmap[18] = {  0, 90, 0, 90,  0, 90,  0, 90, 0, 90, 0, 90, 0, 90,  0, 90,  0, 90 };

//----------------------------------------------------------------------------

// this is nutty, but it seems that every variable in these structs is being padded to the length of a double
// therefore we cannot simply cast the byte array to the various laser structs...how annoying

#define NAVDATA_SIZE      (36 * 8)
#define LASERHEADER_SIZE  (11 * 8)
#define FACETDATA_SIZE    (D3_LASER_HEIGHT * D3_LASER_WIDTH * sizeof(unsigned short) / D3_LASER_NUM_FACETS)

#define FACET_SIZE        (NAVDATA_SIZE + LASERHEADER_SIZE + FACETDATA_SIZE)

#define TIME_BYTE         (0)
#define SPEED_BYTE        (TIME_BYTE + 60)
#define HEADING_BYTE      (SPEED_BYTE + 104)
#define ODOMETER_BYTE     (HEADING_BYTE + 64)
#define SEQNO_BYTE        (ODOMETER_BYTE + 26)
#define SCANLINE_BYTE     (SEQNO_BYTE + 12)
#define TILT_BYTE         (SEQNO_BYTE + 24)
#define FACETDATA_BYTE    (TILT_BYTE + 24)
#define TRANREL_X_BYTE    (TIME_BYTE + 8)
#define TRANREL_Y_BYTE    (TIME_BYTE + 16)
#define TRANREL_Z_BYTE    (TIME_BYTE + 24)
#define TRANABS_X_BYTE    (TIME_BYTE + 32)
#define TRANABS_Y_BYTE    (TIME_BYTE + 40)
#define TRANABS_Z_BYTE    (TIME_BYTE + 48)
#define ROT_S_BYTE        (SPEED_BYTE + 32)
#define ROT_X_BYTE        (SPEED_BYTE + 40)
#define ROT_Y_BYTE        (SPEED_BYTE + 48)
#define ROT_Z_BYTE        (SPEED_BYTE + 56)

// x run grab_facet() and print a few nav variables
// x draw after every 16 facets -- ergo buffer (seqNo goes up by 16?)
// x get live camera input going and display laser + camera in combined window
// x get VNC installed so that we can remotely control the on-XUV laptop
// x test peer-to-peer with 802.11b PC cards
// x pump JPL on output NML channel
 
// correct for tilt
// set up map test mode with fw, avi as input
// draw live "map" overlay
// call TestNML_init_CR_CG() and TestNML_grab_CR_CG() from test_code() so they can be used from the command line
//        flags: track mode/capture mode (just like Linux capability), learn mode (get random snapshots of camera/laser frame                 pairs), with/without display window, etc.
// see how VNC works for video in window over 802.11b
// MORE LEARNING!!!

CR_Image *TestNML_init_CR_CG(char *name)
{
  CR_Image *im;
  FILE *fp;
  int i;
  
  if (itg_capture_interval) {
    run_fp = fopen("captures\\run.txt", "r");
    fscanf(run_fp, "%i", &run_num);
    fclose(run_fp);
    run_fp = fopen("captures\\run.txt", "w");
    fprintf(run_fp, "%i\n", run_num + 1);
    fclose(run_fp);
    itg_capture_string = (char *) CR_calloc(128, sizeof(char));
  }

//   itg_display = TRUE;
//   itg_nml = TRUE;

  // general

  itg_zoom = 2;   // relative to base laser dimensions: 1 => quarter-size camera image, 2 => half-size, 4 => full-size
  itg_camera_w = itg_zoom * 180;  
  // intel
  //  itg_camera_w = itg_zoom * 160;  
  itg_camera_h = itg_zoom * 120;  

  im = make_CR_Image(itg_zoom * D3_LASER_WIDTH, itg_camera_h + itg_zoom * D3_LASER_HEIGHT, 0); 

  // laser

#ifndef DUMB_HACK
  initialize_laser_CR_NML();
#endif

  raw_laser_data = (unsigned char *) CR_calloc(MAX_RAW_LASER_DATA_LENGTH, sizeof(unsigned char));
  num_raw_laser_data_read = 0;

  itg_facets_per_message = 16;  // 5

  itg_frame_data = (int *) CR_calloc(D3_LASER_WIDTH * D3_LASER_HEIGHT, sizeof(int));
  itg_facet_data = (short **) CR_calloc(D3_LASER_NUM_SCANLINES, sizeof(short *));
  itg_good_facet = (int *) CR_calloc(D3_LASER_NUM_SCANLINES, sizeof(int));
  for (i = 0; i < D3_LASER_NUM_SCANLINES; i++) {
    itg_facet_data[i] = (short *) CR_calloc(D3_LASER_FACET_ROWS * D3_LASER_FACET_COLS, sizeof(short));
    itg_good_facet[i] = FALSE;
  }

  itg_framestart_flag = TRUE;
  itg_sequence_start_flag = TRUE;

  itg_laser_im = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, 0);
  itg_flipped_laser_im = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, 0);

  // camera

  // fake camera --- AVI

  if (!itg_do_live) {
    camera_movie = open_CR_Movie(TrackDirtRoad_camera_movie_name);
    
    camera_movie->currentframe = 0;
    get_frame_CR_Movie(camera_movie, camera_movie->currentframe);
  }

  // assuming there's only one camera attached to system
  else
    itg_live = initialize_live_source(itg_camera_w, itg_camera_h, 0, "Microsoft DV Camera and VCR");
  // intel
  //  itg_live = initialize_live_source(itg_camera_w, itg_camera_h, 0, "Intel PC Camera Pro");

  itg_camera_im = make_CR_Image(itg_camera_w, itg_camera_h, 0);
  itg_flipped_camera_im = make_CR_Image(itg_camera_w, itg_camera_h, 0);

  init_laser2camera(itg_camera_im->w, itg_camera_im->h);

  itg_frame = 0;

  // classifier

  dirtnet = read_CR_NeuralNet("road1_bigboth_15.net");
  dirt_color_offset = 1;  
  dirt_input = make_CR_Vector("dirt_input", dirtnet->W[0]->cols);

  TrackDirtRoad_initialize_filters(15, 
				   7 * itg_zoom, 35 * itg_zoom, 
				   174 * itg_zoom, 108 * itg_zoom);

  dirt_roadness = make_CR_Matrix("dirt_roadness", tc_y_count, tc_x_count);
  dirt_output = make_CR_Matrix("dirt_output", tc_y_count, tc_x_count);
  dirt_output_cc = make_CR_Matrix("dirt_output_cc", tc_y_count, tc_x_count);

  // map

#ifndef DUMB_HACK
  if (itg_nml)
    demo3_tsaimap_initialize();
#endif

  // finish

  return im;
}

//----------------------------------------------------------------------------

// copy laser data from raw NML byte array to facet array to frame array (skip middle step?)

void ITG_grab_facet(int scanline, unsigned char *raw_laser_data, short *facet_data, int *frame_data)
{
  int j, row, col, startrow, startcol, index;

  // copy from byte array to facet 

  //  for (j = 0; j < D3_LASER_FACET_ROWS * D3_LASER_FACET_COLS; j++)
  //    facet_data[j] = convert_bigshort(*((short *) &raw_laser_data[2 * j + FACETDATA_BYTE]));

  // copy from facet to correct position in frame

  startrow = itg_rowmap[scanline - 1];
  startcol = itg_colmap[scanline - 1];

  for (row = startrow, index = 0; row < (startrow + 4); row++) 
    for (col = startcol; col < (startcol + 90); col++) {
      //      frame_data[row * D3_LASER_WIDTH + col] = (int) facet_data[index++];
      frame_data[row * D3_LASER_WIDTH + col] = (int) convert_bigshort(*((short *) &raw_laser_data[2 * index + FACETDATA_BYTE]));
      index++;
    }
}

//----------------------------------------------------------------------------

void ITG_draw_camera_CR_Image(CR_Image *im, CR_Image *flipped_im)
{
  glPixelZoom(1, 1);
  glRasterPos2i(0, 0);
   
  vflip_CR_Image(im, flipped_im);
  glDrawPixels(im->w, im->h, GL_RGB, GL_UNSIGNED_BYTE, flipped_im->data);
}

//----------------------------------------------------------------------------

void ITG_draw_laser_CR_Image(int zoom, int h, int *frame_data, CR_Image *im, CR_Image *flipped_im)
{
  unsigned char pixel[3];
  int i, temp1, temp2, x, y;

  // set proper drawing position and scale

  glPixelZoom(zoom, zoom);
  glRasterPos2i(0, h);

  // convert to image

  i = 0;
  for (y = 0; y < D3_LASER_HEIGHT; y++)
    for (x = 0; x < D3_LASER_WIDTH; x++) {

      // displaying far as dark, reducing resolution fourfold 
      //temp1 = (1024 - frame_data[i]) / 4;
      //temp2 = MIN2(temp1, 255);

      // displaying far as dark, keeping full resolution out to 1/4 max possible,
      // clamping everything beyond that to 1/4 max
      temp2 = 255 - MIN2(frame_data[i], 255);

      // displaying far as dark, increasing resolution twofold
      //      temp2 = 255 - MIN2(2 * frame_data[i], 255);

      pixel[IPL_RED] = (unsigned char) temp2;
      pixel[IPL_GREEN] = (unsigned char) temp2;
      pixel[IPL_BLUE] = (unsigned char) temp2;

      set_pixel_CR_Image(x, y, pixel, im);

      i++;
    }


  // flip image and draw it

  vflip_CR_Image(im, flipped_im);
  glDrawPixels(im->w, im->h, GL_RGB, GL_UNSIGNED_BYTE, flipped_im->data);      
}

//----------------------------------------------------------------------------

void ITG_segment_road(double tilt, CR_Image *camera_im, int *laser_frame_data)
{
  int i, j, x, y, num_road_points;

  // run filters

  TrackDirtRoad_filter_and_classify(tilt, camera_im, laser_frame_data);

  // display

  for (j = 0; j < dirt_output->rows; j++)
    for (i = 0; i < dirt_output->cols; i++) {

      x = tc_left + i * tc_filter_size;
      y = tc_top + j * tc_filter_size;

      if (itg_display) {
	if (MATRC(dirt_output, j, i))
	  ogl_draw_point(x, camera_im->h - 1 - y, 5, 0, 255, 0);    
	else 
	  ogl_draw_point(x, camera_im->h - 1 - y, 2, 255, 0, 0);
      }
    }

  // further processing (i.e., connected components, etc.)

  // count road points

  for (j = 0, num_road_points = 0; j < tc_laser_bin_count->rows; j++) 
    for (i = 0; i < tc_laser_bin_count->cols; i++) 
      if (MATRC(dirt_output, j, i)) {
	laser_bin_x->x[num_road_points] = MATRC(tc_laser_bin_x, j, i); 
	laser_bin_y->x[num_road_points] = MATRC(tc_laser_bin_y, j, i); 
	laser_bin_z->x[num_road_points] = MATRC(tc_laser_bin_z, j, i); 
	laser_bin_roadness->x[num_road_points] = MATRC(dirt_roadness, j, i);
	num_road_points++; 
      }

  laser_bin_x->rows = laser_bin_y->rows = laser_bin_z->rows = laser_bin_roadness->rows = num_road_points;

  //  print_CR_Vector(laser_bin_roadness);

  //  printf("num_road_points = %i\n", num_road_points);
}

//----------------------------------------------------------------------------

void TestNML_grab_CR_CG(CR_Image *im)
{
  int i, j, scanline, seqno, seqnodiff, dropped_facets;
  double tilt, time, tranRelx, tranRely, tranRelz, tranAbsx, tranAbsy, tranAbsz, rots, rotx, roty, rotz;

  // fake movie --- AVI

  if (!itg_do_live) {
    get_frame_CR_Movie(camera_movie, camera_movie->currentframe++);
    ip_copy_CR_Image(camera_movie->im, itg_camera_im);
  }

  // get latest incoming camera data
  else
    grab_CR_Live(itg_camera_im, itg_live);

  if (itg_display) {
    ITG_draw_camera_CR_Image(itg_camera_im, itg_flipped_camera_im);
    ogl_draw_rectangle(tc_left, itg_camera_im->h - 1 - tc_top, tc_right, itg_camera_im->h - 1 - tc_bottom, 255, 255, 0);
  }

  // get latest incoming laser data

#ifndef DUMB_HACK
    new_read_laser_CR_NML(raw_laser_data, &raw_laser_data_length);
#endif

  // for each facet in message:

  for (i = 0; i < itg_facets_per_message; i++) {

    // which facet is this?

    seqno = convert_bigint(*((int *) &raw_laser_data[BYTES_PER_FACET * i + SEQNO_BYTE]));
    scanline = convert_bigint(*((int *) &raw_laser_data[BYTES_PER_FACET * i + SCANLINE_BYTE]));
    tilt = convert_bigdouble(*((double *) &raw_laser_data[BYTES_PER_FACET * i + TILT_BYTE]));
    time = convert_bigdouble(*((double *) &raw_laser_data[BYTES_PER_FACET * i + TIME_BYTE]));
    tranRelx = convert_bigdouble(*((double *) &raw_laser_data[BYTES_PER_FACET * i + TRANREL_X_BYTE]));
    tranRely = convert_bigdouble(*((double *) &raw_laser_data[BYTES_PER_FACET * i + TRANREL_Y_BYTE]));
    tranRelz = convert_bigdouble(*((double *) &raw_laser_data[BYTES_PER_FACET * i + TRANREL_Z_BYTE]));
    tranAbsx = convert_bigdouble(*((double *) &raw_laser_data[BYTES_PER_FACET * i + TRANABS_X_BYTE]));
    tranAbsy = convert_bigdouble(*((double *) &raw_laser_data[BYTES_PER_FACET * i + TRANABS_Y_BYTE]));
    tranAbsz = convert_bigdouble(*((double *) &raw_laser_data[BYTES_PER_FACET * i + TRANABS_Z_BYTE]));
    rots = convert_bigdouble(*((double *) &raw_laser_data[BYTES_PER_FACET * i + ROT_S_BYTE]));
    rotx = convert_bigdouble(*((double *) &raw_laser_data[BYTES_PER_FACET * i + ROT_X_BYTE]));
    roty = convert_bigdouble(*((double *) &raw_laser_data[BYTES_PER_FACET * i + ROT_Y_BYTE]));
    rotz = convert_bigdouble(*((double *) &raw_laser_data[BYTES_PER_FACET * i + ROT_Z_BYTE]));

    itg_good_facet[scanline - 1] = TRUE;

    //    printf("scanline = %i, seqNo = %i\n", scanline, seqno);

    // have any facets been dropped since the last one?

    if (itg_sequence_start_flag) {
      itg_last_seqno = seqno - 1;
      itg_last_tilt = tilt;
      itg_last_time = time;
      itg_last_tranRelx = tranRelx;
      itg_last_tranRely = tranRely;
      itg_last_tranRelz = tranRelz;
      itg_last_tranAbsx = tranAbsx;
      itg_last_tranAbsy = tranAbsy;
      itg_last_tranAbsz = tranAbsz;
      itg_last_rots = rots;
      itg_last_rotx = rotx;
      itg_last_roty = roty;
      itg_last_rotz = rotz;
      itg_sequence_start_flag = FALSE;
    }

    dropped_facets = seqno - itg_last_seqno - 1;
        if (dropped_facets)
          printf("dropped %i facets !!!!!!!!!!!!!!!!!!!!!!!\n", dropped_facets);
    itg_last_seqno = seqno;

    // draw/process if we have a full frame (including dropped facets)
    // drawing bad facets doesn't really matter, but don't do processing on them--they may be quite stale time-wise

    if (itg_framestart_flag) {
      itg_framestart_seqno = seqno;
      itg_framestart_flag = FALSE;
    }

    seqnodiff = seqno - itg_framestart_seqno;

    if (seqnodiff > 15) {
      ITG_segment_road(tilt, itg_camera_im, itg_frame_data);
      if (itg_display)
	ITG_draw_laser_CR_Image(itg_zoom, itg_camera_h, itg_frame_data, itg_laser_im, itg_flipped_laser_im);
#ifndef DUMB_HACK
      if (itg_nml)
	demo3_tsaimap_send_road(laser_bin_x, laser_bin_y, laser_bin_z, laser_bin_roadness,
			    time, tranRelx, tranRely, tranRelz, rots, rotx, roty, rotz);
#endif
      if (itg_capture_interval && !(itg_frame % itg_capture_interval))
	ITG_capture_camera_and_laser(itg_frame, itg_camera_im, itg_frame_data, itg_good_facet,
				     time, tilt, 
				     tranRelx, tranRely, tranRelz, 
				     tranAbsx, tranAbsy, tranAbsz, 
				     rots, rotx, roty, rotz);
      itg_frame++;
    }

    ITG_grab_facet(scanline, &raw_laser_data[BYTES_PER_FACET * i], itg_facet_data[scanline - 1], itg_frame_data);

    if (seqnodiff == 15) {
      ITG_segment_road(tilt, itg_camera_im, itg_frame_data);
      if (itg_display)
	ITG_draw_laser_CR_Image(itg_zoom, itg_camera_h, itg_frame_data, itg_laser_im, itg_flipped_laser_im);
#ifndef DUMB_HACK
      if (itg_nml)
	demo3_tsaimap_send_road(laser_bin_x, laser_bin_y, laser_bin_z, laser_bin_roadness,
			    itg_last_time, itg_last_tranRelx, itg_last_tranRely, itg_last_tranRelz, 
			    itg_last_rots, itg_last_rotx, itg_last_roty, itg_last_rotz);
#endif
      if (itg_capture_interval && !(itg_frame % itg_capture_interval))
	ITG_capture_camera_and_laser(itg_frame, itg_camera_im, itg_frame_data, itg_good_facet,
				     itg_last_time, itg_last_tilt, 
				     itg_last_tranRelx, itg_last_tranRely, itg_last_tranRelz, 
				     itg_last_tranAbsx, itg_last_tranAbsy, itg_last_tranAbsz, 
				     itg_last_rots, itg_last_rotx, itg_last_roty, itg_last_rotz);
      itg_frame++;
    }

    if (seqnodiff >= 15) {
      itg_framestart_flag = TRUE;
      for (j = 0; j < D3_LASER_NUM_SCANLINES; j++) 
	itg_good_facet[j] = FALSE;
    }

    itg_last_tilt = tilt;
    itg_last_time = time;
    itg_last_tranRelx = tranRelx;
    itg_last_tranRely = tranRely;
    itg_last_tranRelz = tranRelz;
    itg_last_rots = rots;
    itg_last_rotx = rotx;
    itg_last_roty = roty;
    itg_last_rotz = rotz;
  }
}

//----------------------------------------------------------------------------

void TestNML_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  fclose(good_road_fp);
  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------

void ITG_laser2world(double xl, double yl, double zl, double xw, double yw, double zw)
{
  //---------------------------------------------------------
  // get 3-D values in laser coordinate system
  //---------------------------------------------------------

  //  get_demo3_laser_xyz(i_laser, j_laser, range_laser, &x_laser, &y_laser, &z_laser);
  //  demo3_get_gall_laser_xyz(i_laser, j_laser, range_laser, &x_laser, &y_laser, &z_laser);

  //---------------------------------------------------------
  // convert to vehicle coordinates at time of laser image
  //---------------------------------------------------------
  
  // undo laser tilt (assuming laser rotating about its center--which i doubt is really the case)

//   demo3_x_axis_rotation(laser_navdat->cr_tiltangle,
// 			x_laser, y_laser, z_laser, 
// 			&xprime_laser, &yprime_laser, &zprime_laser);

  // back translate to vehicle center from laser center

//   x_vehicle = xprime_laser + D3_VEHICLE_LASER_OFFSET_X;
//   y_vehicle = yprime_laser + D3_VEHICLE_LASER_OFFSET_Y;
//   z_vehicle = zprime_laser + D3_VEHICLE_LASER_OFFSET_Z;

  //---------------------------------------------------------
  // convert to world coordinates at time of laser image
  //---------------------------------------------------------

  // undo vehicle roll, pitch, heading

//   demo3_quaternion_rotation(laser_navdat->rot.s, laser_navdat->rot.x, laser_navdat->rot.y, laser_navdat->rot.z, z_vehicle, x_vehicle, -y_vehicle, &qzprime_vehicle, &qxprime_vehicle, &qyprime_vehicle);
//   qyprime_vehicle = -qyprime_vehicle;

  // back translate to world center from vehicle center

//   qx_world = qxprime_vehicle + laser_navdat->tranRel.y;
//   qy_world = qyprime_vehicle - laser_navdat->tranRel.z;
//   qz_world = qzprime_vehicle + laser_navdat->tranRel.x;
}

//----------------------------------------------------------------------------

//#define ITG_NOCAMERA

// annapurna
// char ITG_camera_movie_name[256] = "E:\\NIST_data\\itg_July17_2001\\morning\\half_road08-002.avi";
// char ITG_laser_movie_name[256] = "E:\\NIST_data\\itg_July17_2001\\morning\\road08-002.fw";
// char ITG_log_name[256] = "E:\\NIST_data\\itg_July17_2001\\morning\\road08-002.log";
char ITG_camera_movie_name[256] = "D:\\NIST_data\\itg_July17_2001\\morning\\half_road08-002.avi";
char ITG_laser_movie_name[256] = "D:\\NIST_data\\itg_July17_2001\\morning\\road08-002.fw";
char ITG_log_name[256] = "D:\\NIST_data\\itg_July17_2001\\morning\\road08-002.log";

int ITG_camera_start_frame = 0;
int ITG_zoom_factor;
int *ITG_laser_data;

CR_NeuralNet *ITG_net;
CR_Vector *ITG_input;
CR_Vector *ITG_output;

extern CR_Matrix *ts_feature_vectors;
extern int ts_left, ts_top, ts_right, ts_bottom, ts_x_delta, ts_y_delta, ts_num_feature_vectors, ts_num_kernels;
extern int ts_rows, ts_cols;

CR_Matrix *ITG_position;
int ITG_position_num;

FILE *relfp;
char ppmname[256];

CR_Image *ITG_init_CR_CG(char *name)
{
  CR_Image *im;
  int i, j, k;
  double this_time, laser_timestamp, laser_timestamp_last, camera_timestamp;

  relfp = fopen("relpos.txt", "w");

  //  Itg_position = make_CR_Matrix("ITG_position", 1825, 3);
  //  ITG_position = make_CR_Matrix("ITG_position", 25 *  D3_LASER_HEIGHT *  D3_LASER_WIDTH, 3);
  ITG_position = make_CR_Matrix("ITG_position", 200 * 640, 4);
  ITG_position_num = 0;

  // filtering initialization

  NIST_Road_initialize_feature_vectors_CR_Image(360, 240);
  ITG_laser_data = (int *) CR_calloc(D3_LASER_WIDTH * D3_LASER_HEIGHT, sizeof(int));

  // neural net initialization

  //  ITG_net = read_CR_NeuralNet("plane_digest_NN_20_ss_3.net");   // laser alone
  ITG_net = read_CR_NeuralNet("plane_digest_NN_20_ss_1.net");   // color alone
  //  ITG_net = read_CR_NeuralNet("plane2_digest_NN_20_ss_4.net");

  ITG_input = make_CR_Vector("ITG_input", ITG_net->W[0]->cols);
  ITG_output = make_CR_Vector("ITG_output", 640);

//   camera_movie = open_CR_Movie("E:\\NIST_data\\itg_July17_2001\\morning\\half_road08-002.avi");

#ifndef ITG_NOCAMERA
  //  camera_movie = open_CR_Movie("D:\\NIST_data\\itg_July17_2001\\morning\\half_road08-002.avi");
  camera_movie = open_CR_Movie(ITG_camera_movie_name);

  camera_movie->currentframe = 0;
  get_frame_CR_Movie(camera_movie, camera_movie->currentframe);

  camera_image = make_CR_Image(camera_movie->im->w, camera_movie->im->h, 0);

  ITG_zoom_factor = camera_movie->width / D3_LASER_WIDTH;
  im = make_CR_Image(camera_movie->width, camera_movie->height + D3_LASER_HEIGHT * ITG_zoom_factor, 0);

  init_laser2camera(camera_movie->im->w, camera_movie->im->h);
#else
  im = make_CR_Image(360, 64, 0);
  ITG_zoom_factor = 2;
#endif

  // read logfile

  logfile = fopen(ITG_log_name, "rb");

  bytes_per_camera_frame = 20;
  fseek(logfile, 0, SEEK_END);
  logfile_size = ftell(logfile);
  num_camera_frames = logfile_size / bytes_per_camera_frame;

  start_camera_flag = FALSE;
  camframetimes = make_CR_Vector("camframetimes", num_camera_frames);

  for (i = 0; i < num_camera_frames; i++) {

    fseek(logfile, i * bytes_per_camera_frame, SEEK_SET);
    fread(&time_seconds, 4, 1, logfile);
    fread(&time_microseconds, 4, 1, logfile); 
    fread(&total_frames_written, 4, 1, logfile);
    fread(&total_incomplete_frames, 4, 1, logfile);
    
    this_time = time_seconds + 0.000001 * time_microseconds;
    
    if (!start_camera_flag) {
      start_camera_time = this_time;
      start_camera_flag = TRUE;
    }

    camera_timestamp = this_time - start_camera_time;
    camframetimes->x[i] = camera_timestamp;
  }

  camera_startframe = ITG_camera_start_frame;

  printf("camframe %i time = %lf\n", camera_startframe, camframetimes->x[camera_startframe]);

  // open laser movie

  laser_movie = read_demo3_laser_to_CR_Movie(ITG_laser_movie_name);

  start_laser_time = laser_movie->laserim[0].navdat[0].time;

  laser_movie->currentframe = 0;
  do {
    get_frame_CR_Movie(laser_movie, laser_movie->currentframe);
    laser_timestamp = 0.001 * (laser_movie->laserim[laser_movie->currentframe].navdat[0].time - start_laser_time);
    laser_movie->currentframe++;
  } while (laser_timestamp < camframetimes->x[camera_startframe]);

  CR_flush_printf("frame = %i\n", laser_movie->currentframe);

  if (laser_movie->currentframe >= 2) {

    laser_timestamp_last = 0.001 * (laser_movie->laserim[laser_movie->currentframe - 2].navdat[0].time - start_laser_time);
    
    if (fabs(laser_timestamp - camframetimes->x[camera_startframe]) <
	fabs(laser_timestamp_last - camframetimes->x[camera_startframe]))
      laser_movie->currentframe = laser_movie->currentframe - 1;
    else
      laser_movie->currentframe = laser_movie->currentframe - 2;
  }
  else
    laser_movie->currentframe = 0;

  printf("laser current %i = %lf\n", laser_movie->currentframe, 0.001 * (laser_movie->laserim[laser_movie->currentframe].navdat[0].time - start_laser_time));

  laser_image = make_CR_Image(laser_movie->im->w, laser_movie->im->h, 0);

  // finish up

  track_3d_mov = write_avi_initialize_CR_Movie("c2.avi", im->w, im->h, 30);

  return im;
}

//----------------------------------------------------------------------------

void ITG_end_CR_CG(CR_Image *);

void ITG_grab_CR_CG(CR_Image *im)
{
  int i, j, k, x, y, index, index2, xc, yc;
  static char laser_frame[80], camera_frame[80], laser_time[80], camera_time[80];
  double laser_timestamp, tilt, xw, yw, zw, sum, xl, yl, zl, xw2, yw2, zw2;
  Demo3_NavData *navdat;

  glPixelZoom(ITG_zoom_factor, ITG_zoom_factor);
#ifndef ITG_NOCAMERA
  glRasterPos2i(0, camera_movie->im->h);
#else
  glRasterPos2i(0, 240);
#endif
  
  vflip_CR_Image(laser_movie->im, laser_image);
  glDrawPixels(laser_image->w, laser_image->h, GL_RGB, GL_UNSIGNED_BYTE, laser_image->data);

//   sprintf(laser_frame, "%i", laser_movie->currentframe);
//   ogl_draw_string(laser_frame, 10, camera_movie->im->h + 20, 255, 255, 0); 

  laser_timestamp = 0.001 * (laser_movie->laserim[laser_movie->currentframe].navdat[0].time - start_laser_time);
//   sprintf(laser_time, "%.3lf", laser_timestamp);
//   ogl_draw_string(laser_time, 10, camera_movie->im->h + 8, 255, 255, 0); 

  // sync camera image with laser image

#ifndef ITG_NOCAMERA
  for ( ; camera_movie->currentframe < camera_movie->lastframe && camframetimes->x[camera_movie->currentframe + camera_startframe] < laser_timestamp; camera_movie->currentframe++);
  if (camera_movie->currentframe > 0 &&
      fabs(camframetimes->x[camera_movie->currentframe + camera_startframe] - laser_timestamp) > 
      fabs(camframetimes->x[camera_movie->currentframe + camera_startframe - 1] - laser_timestamp))
    camera_movie->currentframe = camera_movie->currentframe - 1; 
  get_frame_CR_Movie(camera_movie, camera_movie->currentframe);

  // draw camera image

  glPixelZoom(1, 1);
  glRasterPos2i(0, 0);
  
  vflip_CR_Image(camera_movie->im, camera_image);
  glDrawPixels(camera_image->w, camera_image->h, GL_RGB, GL_UNSIGNED_BYTE, camera_image->data);

//   sprintf(camera_frame, "%i", camera_movie->currentframe);
//   ogl_draw_string(camera_frame, 10, 20, 255, 255, 0); 
// 
//   sprintf(camera_time, "%.3lf", camframetimes->x[camera_movie->currentframe]);
//   ogl_draw_string(camera_time, 10, 8, 255, 255, 0); 
#endif

  // run net

  /*

  for (j = 0; j < ITG_output->rows; j++) {

    index = 0;

    // laser 

//     if (MATRC(ts_feature_vectors, j, 18) > 1) {
//       ITG_input->x[0] = MATRC(ts_feature_vectors, j, 20);
//       ITG_input->x[1] = MATRC(ts_feature_vectors, j, 26);
//       index = 2;

    // color 

    for (i = 79; i <= 102; i++) 
      ITG_input->x[index++] = MATRC(ts_feature_vectors, j, i);   

    // texture

//     for (i = 751; i <= 1326; i += 3) 
//       ITG_input->x[index++] = MATRC(ts_feature_vectors, j, i);
//     for (i = 752; i <= 1326; i += 3) 
//       ITG_input->x[index++] = MATRC(ts_feature_vectors, j, i);

    simulate_CR_NeuralNet(ITG_input, ITG_net);

    ITG_output->x[j] = ITG_net->I[ITG_net->numlayers - 1]->x[0];
    //    }
//     else
//       ITG_output->x[j] = 666;

  }
  */

  // display road overlay

  /*
  for (y = ts_top, index = 0; y <= ts_bottom; y += ts_y_delta)
    for (x = ts_left; x <= ts_right; x += ts_x_delta) {
//       if (ITG_output->x[index] == 666) {
//  	ogl_draw_point(x, 239 - y, 5, 255, 255, 255);
//       }      
      //      else 
      if (ITG_output->x[index] >= 0.5) {
 	ogl_draw_point(x, 239 - y, 5, 0, 255, 0);
      }
      else {
	ogl_draw_point(x, 239 - y, 5, 255, 0, 0);
      }
      index++;
    }
  */

  // map it

  navdat = &laser_movie->laserim[laser_movie->currentframe].navdat[0];
  tilt = laser_movie->laserim[laser_movie->currentframe].frameHeaders[0].tiltAngle;

  if (!(laser_movie->currentframe % 10)) {

    if (laser_movie->currentframe >= 1820) {
      ITG_end_CR_CG(im);
      exit(1);
    }

    /*
    printf("writing %i\n", laser_movie->currentframe);
//     sprintf(ppmname, "cmap_frame_%i.ppm\0", laser_movie->currentframe);
//     printf("%s\n", ppmname);
//     write_CR_Image(ppmname, camera_movie->im);

   ulaser2world(0, 0, 0, 
		 &xw, &yw, &zw,
		 navdat->rot.s, navdat->rot.x, navdat->rot.y, navdat->rot.z,
		 navdat->tranRel.x, navdat->tranRel.y, navdat->tranRel.z);

   ulaser2world(0, 0, 2.0, 
		 &xw2, &yw2, &zw2,
		 navdat->rot.s, navdat->rot.x, navdat->rot.y, navdat->rot.z,
		 navdat->tranRel.x, navdat->tranRel.y, navdat->tranRel.z);

   fprintf(relfp, "%i,%lf,%lf,%lf,%lf,%lf,%lf\n", laser_movie->currentframe, xw, yw, zw, xw2, yw2, zw2);
    */

    // filter

    for (i = 0; i < D3_LASER_WIDTH * D3_LASER_HEIGHT; i++) 
      ITG_laser_data[i] = (int) laser_movie->laserim[laser_movie->currentframe].data[i];

    NIST_Road_compute_feature_vectors_CR_Image(NULL, 0, 0, camera_movie->im, ITG_laser_data, laser_movie->laserim[laser_movie->currentframe].frameHeaders[0].tiltAngle, NULL);
    
    printf("writing %i\n", laser_movie->currentframe);

    for (j = 0, index = 0; j < 20; j++)
      for (i = 0; i < 32; i++) {

	// only necessary when mapping or laser is in feature vector
	if (MATRC(ts_feature_vectors, index, 18) > 1 &&    // enough laser points
 	    MATRC(ts_feature_vectors, index, 21) < 70) { // &&   // no sky
// 	    MATRC(ts_feature_vectors, index, 21) > 1) {     // no missing facets

	  // laser correction?
	  if (i < 16)
	    xl = -MATRC(ts_feature_vectors, index, 19);
	  else
	    xl = MATRC(ts_feature_vectors, index, 19);

	  ulaser2world(xl, 
		       MATRC(ts_feature_vectors, index, 20), 
		       MATRC(ts_feature_vectors, index, 21), 
		       &xw, &yw, &zw,
		       navdat->rot.s, navdat->rot.x, navdat->rot.y, navdat->rot.z,
		       navdat->tranRel.x, navdat->tranRel.y, navdat->tranRel.z);

	  MATRC(ITG_position, ITG_position_num, 1) = xw;
	  MATRC(ITG_position, ITG_position_num, 2) = yw;
	  MATRC(ITG_position, ITG_position_num, 3) = zw;

	  // run net to get label

	  for (k = 79, index2 = 0; k <= 102; k++) 
	    ITG_input->x[index2++] = MATRC(ts_feature_vectors, index, k);   

// 	  for (k = 751; k <= 1326; k += 3) 
// 	    ITG_input->x[index2++] = MATRC(ts_feature_vectors, index, k);
// 	  for (k = 752; k <= 1326; k += 3) 
// 	    ITG_input->x[index2++] = MATRC(ts_feature_vectors, index, k);

	  simulate_CR_NeuralNet(ITG_input, ITG_net);

	  if (ITG_net->I[ITG_net->numlayers - 1]->x[0] >= 0.5)
	    ITG_output->x[index] = 1.0;
	  else
	    ITG_output->x[index] = 0.0;

	  //	  printf("%lf %lf %lf\n", MATRC(ts_feature_vectors, index, 19), MATRC(ts_feature_vectors, index, 20), MATRC(ts_feature_vectors, index, 21));

	  MATRC(ITG_position, ITG_position_num, 0) = ITG_output->x[index];

	  // increment data point number

	  ITG_position_num++;

      }
 	else 
	  // 	  ITG_output->x[index] = -1.0;
 	  ITG_output->x[index] = 0.0;

	index++;
      }

    for (y = ts_top, index = 0; y <= ts_bottom; y += ts_y_delta)
      for (i = 0, x = ts_left; x <= ts_right; i++, x += ts_x_delta) {
	//	if (i >= 16) {
	if (ITG_output->x[index] == 1.0) 
	  ogl_draw_point(x, 239 - y, 5, 0, 255, 0);
	else if (ITG_output->x[index] == 0.0) 
	  ogl_draw_point(x, 239 - y, 3, 255, 0, 0);
	//	}
	index++;
      }

    /*
    for (j = 0, index = 0; j < D3_LASER_HEIGHT; j++)
      for (i = 0; i < D3_LASER_WIDTH; i++) {

	// filter out bad facets, sky

	if (laser_movie->laserim[laser_movie->currentframe].data[index] != 0 &&
	    laser_movie->laserim[laser_movie->currentframe].data[index] < 1021) {

	  laser2camera(i, j, laser_movie->laserim[laser_movie->currentframe].data[index], 
		       &xc, &yc, &xl, &yl, &zl, tilt);

	  printf("%i %i\n", xc, yc);

	  ulaser2world(xl, yl, zl,
		       &xw, &yw, &zw,
		       navdat->rot.s, navdat->rot.x, navdat->rot.y, navdat->rot.z,
		       navdat->tranRel.x, navdat->tranRel.y, navdat->tranRel.z);

// 	  laser2world(i, j, laser_movie->laserim[laser_movie->currentframe].data[index], 
// 		      &xw, &yw, &zw,
// 		      tilt, 
// 		      navdat->rot.s, navdat->rot.x, navdat->rot.y, navdat->rot.z,
// 		      navdat->tranRel.x, navdat->tranRel.y, navdat->tranRel.z);

	  //	  if (i < 90) {
	  MATRC(ITG_position, ITG_position_num, 1) = xw;
	  MATRC(ITG_position, ITG_position_num, 2) = yw;
	  MATRC(ITG_position, ITG_position_num, 3) = zw;
	  ITG_position_num++;
	  // }
	}
	index++;
      }
    */
  }

  ITG_position->rows = ITG_position_num;

//   printf("%i\n", laser_movie->currentframe);
//   printf("rx = %lf, ry = %lf, rz = %lf\n", navdat->tranRel.x, navdat->tranRel.y, navdat->tranRel.z);
  //  printf("ax = %lf, ay = %lf, az = %lf\n", navdat->tranAbs.x, navdat->tranAbs.y, navdat->tranAbs.z);
  //  printf("s = %lf, x = %lf, y = %lf, z = %lf\n", navdat->rot.s, navdat->rot.x, navdat->rot.y, navdat->rot.z);

//   navdat->tranRel.x, navdat->tranRel.y, navdat->tranRel.z, 
//     navdat->tranAbs.x, navdat->tranAbs.y, navdat->tranAbs.z, 
//     navdat->rot.s, navdat->rot.x, navdat->rot.y, navdat->rot.z

  // advance laser frame
  
#ifndef ITG_NOCAMERA
  if (laser_movie->currentframe < laser_movie->lastframe &&
      camera_movie->currentframe < camera_movie->lastframe) 
    get_frame_CR_Movie(laser_movie, laser_movie->currentframe++);
#else
  if (laser_movie->currentframe < laser_movie->lastframe)
    get_frame_CR_Movie(laser_movie, laser_movie->currentframe++);
#endif

  /*
  // draw camera image

  glPixelZoom(1, 1);
  glRasterPos2i(0, 0);
   
  vflip_CR_Image(camera_movie->im, camera_image);
  glDrawPixels(camera_image->w, camera_image->h, GL_RGB, GL_UNSIGNED_BYTE, camera_image->data);
 
  // advance camera frame

  if (camera_movie->currentframe < camera_movie->lastframe) 
    get_frame_CR_Movie(camera_movie, camera_movie->currentframe++);
  */

}

//----------------------------------------------------------------------------

void ITG_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  write_dlm_CR_Matrix("ctlaserfix.dlm", ITG_position);
  fclose(relfp);

  //  fclose(good_road_fp);
  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

int pt_w, pt_h;

//----------------------------------------------------------------------------

#define JET_CMAP    1
#define HSV_CMAP    2
#define COOL_CMAP   3

CR_Matrix *jet_cmap, *hsv_cmap, *cool_cmap;

// load colormap files

void initialize_laser_colormap()
{
  jet_cmap = read_dlm_CR_Matrix("C:\\Documents\\software\\fusion\\jet1024.m", 1024, 3);
  hsv_cmap = read_dlm_CR_Matrix("C:\\Documents\\software\\fusion\\hsv1024.m", 1024, 3);
  cool_cmap = read_dlm_CR_Matrix("C:\\Documents\\software\\fusion\\cool1024.m", 1024, 3);
}

//----------------------------------------------------------------------------

void convert_laser_colormap(int cmap_type, CR_Movie *laser_movie, CR_Image *im)
{
  int x, y, index, i, j;
  CR_Matrix *cmap;

  if (cmap_type == JET_CMAP) 
    cmap = jet_cmap;
  else if (cmap_type == HSV_CMAP)
    cmap = hsv_cmap;
  else
    cmap = cool_cmap;

  for (y = 0, index = 0; y < pt_h; y++)
    for (x = 0; x < pt_w; x++) {
      i = (int) laser_movie->laserim[laser_movie->currentframe].data[index];
      set_pixel_rgb_CR_Image(x, y, MATRC(cmap, i, 0), MATRC(cmap, i, 1), MATRC(cmap, i, 2), im); 
      index++;
    }
}

//----------------------------------------------------------------------------

CR_Image *poletrunk_im, *poletrunk_fp_im, *poletrunk_vert1_fp_im, *poletrunk_vert2_fp_im, *poletrunk_vert3_fp_im, *poletrunk_horiz1_fp_im, *poletrunk_horiz2_fp_im, *poletrunk_horiz3_fp_im, *poletrunk_thresh_fp_im, *poletrunk_image, *poletrunk_flipimage; 
int poletrunk_num_kernels;
CR_Image_Kernel **poletrunk_gabor_kernel;
CR_Matrix **poletrunk_gabor_kernel_matrix;
CR_Movie *poletrunk_movie;

CR_Image **poletrunk_vert_fp_im, **poletrunk_scaled_im, **poletrunk_display_im;
CR_Image *poletrunk_mean_im, *poletrunk_cmap_im;
double *poletrunk_mean;

CR_Image *PoleTrunkFinder_init_CR_CG(char *name)
{
  CR_Image *im;
  int i, w, h;
  char *filename;

  // load movie

  filename = choose_open_file_dialog_win32("Movies (*.mpg; *.avi; *.mov; fw*; *.rif; *.ppm)\0*.mpg;*.m1v;*.avi;*.mov;fw*;*.rif;*.ppm\0");
  poletrunk_movie = open_CR_Movie(filename);
  pt_w = poletrunk_movie->im->w;
  pt_h = poletrunk_movie->im->h;
  poletrunk_image = make_CR_Image(pt_w, pt_h, 0);
  poletrunk_flipimage = make_CR_Image(pt_w, pt_h, 0);
  im = make_CR_Image(pt_w, 7 * pt_h, 0);

  // initialize Gabor filters (vertical bar finder at various scales)

  poletrunk_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_8U);
  poletrunk_fp_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);
  poletrunk_thresh_fp_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);
  poletrunk_vert1_fp_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);
  poletrunk_vert2_fp_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);
  poletrunk_vert3_fp_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);
  poletrunk_horiz1_fp_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);
  poletrunk_horiz2_fp_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);
  poletrunk_horiz3_fp_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);

  // set up filter bank (odd-numbered indices are the bar-finding kernels)

  poletrunk_gabor_kernel_matrix = make_gabor_filter_bank_CR_Matrix_array(2.0, 0.5, 5, 90.0, 1, &poletrunk_num_kernels);
  poletrunk_gabor_kernel = (CR_Image_Kernel **) CR_calloc(poletrunk_num_kernels, sizeof(CR_Image_Kernel *));
  for (i = 0; i < poletrunk_num_kernels; i++) {
    poletrunk_gabor_kernel[i] = make_CR_Image_Kernel(poletrunk_gabor_kernel_matrix[i]);
//     if (!(i % 2) && i <= 8 ) {
//     printf("%i\n", i);
//     print_CR_Matrix(poletrunk_gabor_kernel_matrix[i]);
//     }
  }
  printf("num_kernels = %i\n", poletrunk_num_kernels);

  // allocate images

  poletrunk_vert_fp_im = (CR_Image **) CR_calloc(poletrunk_num_kernels, sizeof(CR_Image *));
  poletrunk_scaled_im = (CR_Image **) CR_calloc(poletrunk_num_kernels, sizeof(CR_Image *));
  poletrunk_display_im = (CR_Image **) CR_calloc(poletrunk_num_kernels, sizeof(CR_Image *));
  for (i = 0; i < poletrunk_num_kernels; i++) {
    poletrunk_vert_fp_im[i] = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);
    poletrunk_scaled_im[i] = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_8U);
    poletrunk_display_im[i] = make_CR_Image(pt_w, pt_h, 0);
  }
  poletrunk_mean = (double *) CR_calloc(pt_w, sizeof(double));
  poletrunk_mean_im = make_CR_Image(pt_w, pt_h, 0);
  poletrunk_cmap_im = make_CR_Image(pt_w, pt_h, 0);

  initialize_laser_colormap();

  // poletrunk_vert1_fp_im -> poletrunk_vert_fp_im[i]  32F
  // poletrunk_im -> poletrunk_scaled_im[i]  8U
  // poletrunk_image -> poletrunk_display_im[i]  color

  // finish

  return im;
}

//----------------------------------------------------------------------------

void PoleTrunkFinder_grab_CR_CG(CR_Image *im)
{
  int x, y, index, i, j;

  // filter image

  // first convert laser image to IPL FP image, then convolve by vert line filters

  for (y = 0, index = 0; y < pt_h; y++)
    for (x = 0; x < pt_w; x++)
      //      GRAYFP_IMXY(poletrunk_fp_im, x, y) = 0.25 * (float) poletrunk_movie->laserim[poletrunk_movie->currentframe].data[index++];
      // subtract here to make closer points larger--this is for the Gabor filter
      GRAYFP_IMXY(poletrunk_fp_im, x, y) = 255.0 - 0.25 * (float) poletrunk_movie->laserim[poletrunk_movie->currentframe].data[index++];

  //  convolve by vert line Gabor filters

  for (i = 0; i < poletrunk_num_kernels; i++) {
    convolve_CR_Image(poletrunk_gabor_kernel[i], poletrunk_fp_im, poletrunk_vert_fp_im[i]);  
    iplScaleFP(poletrunk_vert_fp_im[i]->iplim, poletrunk_scaled_im[i]->iplim, 0, 255);
    iplGrayToColor(poletrunk_scaled_im[i]->iplim, poletrunk_display_im[i]->iplim, 1, 1, 1);
  }

//   convolve_CR_Image(poletrunk_gabor_kernel[1], poletrunk_fp_im, poletrunk_vert1_fp_im);  
//   iplScaleFP(poletrunk_vert1_fp_im->iplim, poletrunk_im->iplim, 0, 255);
//   iplGrayToColor(poletrunk_im->iplim, poletrunk_image->iplim, 1, 1, 1);

  // compute mean of each column, convert to image

  int temp;

  for (i = 0; i < pt_w; i++) {
    poletrunk_mean[i] = 0.0; 
    for (j = 0; j < pt_h; j++)
      poletrunk_mean[i] += (unsigned char) IMXY_R(poletrunk_display_im[7], i, j);  // 4th of 5 scales
    poletrunk_mean[i] /= (double) pt_h;
    temp = CR_round(poletrunk_mean[i]);
    for (j = 0; j < pt_h; j++)
      set_pixel_rgb_CR_Image(i, j, temp, temp, temp, poletrunk_mean_im);
  }

  // draw raw image

  glPixelZoom(1, 1);

  glRasterPos2i(0, 6 * pt_h);
  convert_laser_colormap(JET_CMAP, poletrunk_movie, poletrunk_cmap_im);
  //  colormap_CR_Image(poletrunk_movie->im, poletrunk_cmap_im);
  vflip_CR_Image(poletrunk_cmap_im, poletrunk_flipimage);
  //  vflip_CR_Image(poletrunk_movie->im, poletrunk_flipimage);
  glDrawPixels(poletrunk_flipimage->w, poletrunk_flipimage->h, GL_RGB, GL_UNSIGNED_BYTE, poletrunk_flipimage->data);

  // draw filtered images
  
  //  for (i = 0; i < poletrunk_num_kernels; i++) {
  for (i = 0; i < 5; i++) {
    glRasterPos2i(0, (5 - i) * pt_h);
    vflip_CR_Image(poletrunk_display_im[2 * i + 1], poletrunk_flipimage);
    glDrawPixels(poletrunk_flipimage->w, poletrunk_flipimage->h, GL_RGB, GL_UNSIGNED_BYTE, poletrunk_flipimage->data);
  }

  // draw column means

    glRasterPos2i(0, 0);
    vflip_CR_Image(poletrunk_mean_im, poletrunk_flipimage);
    glDrawPixels(poletrunk_flipimage->w, poletrunk_flipimage->h, GL_RGB, GL_UNSIGNED_BYTE, poletrunk_flipimage->data);

  // draw filtered image #1

//   glRasterPos2i(0, 4 * pt_h);
//   vflip_CR_Image(poletrunk_image, poletrunk_flipimage);
//   glDrawPixels(poletrunk_flipimage->w, poletrunk_flipimage->h, GL_RGB, GL_UNSIGNED_BYTE, poletrunk_flipimage->data);

  //  glDrawPixels(poletrunk_image->w, poletrunk_image->h, GL_RGB, GL_UNSIGNED_BYTE, poletrunk_image->data);

  // draw filtered image #2

//   glRasterPos2i(0, 3 * pt_h);
//   glDrawPixels(poletrunk_image->w, poletrunk_image->h, GL_RGB, GL_UNSIGNED_BYTE, poletrunk_image->data);

  // advance to next movie frame

  if (poletrunk_movie->currentframe < poletrunk_movie->lastframe)
    get_frame_CR_Movie(poletrunk_movie, poletrunk_movie->currentframe++);
}

//----------------------------------------------------------------------------

void PoleTrunkFinder_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  fclose(mapfp);
  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Image *flatsign_im, *flatsign_fp_im, *flatsign_vert1_fp_im, *flatsign_vert2_fp_im, *flatsign_vert3_fp_im, *flatsign_horiz1_fp_im, *flatsign_horiz2_fp_im, *flatsign_horiz3_fp_im, *flatsign_thresh_fp_im, *flatsign_image, *flatsign_flipimage; 
int flatsign_num_kernels;
CR_Image_Kernel **flatsign_gabor_kernel;
CR_Matrix **flatsign_gabor_kernel_matrix;
CR_Movie *flatsign_movie;

CR_Image **flatsign_vert_fp_im, **flatsign_scaled_im, **flatsign_display_im;
CR_Image *flatsign_z_im, *flatsign_cmap_im;
double *flatsign_mean;
CR_Matrix *flatsign_z_mat;
CR_Matrix **flatsign_z_var_mat;

CR_Image *FlatSignFinder_init_CR_CG(char *name)
{
  CR_Image *im;
  int i, w, h;
  char *filename;

  // load movie

  filename = choose_open_file_dialog_win32("Movies (*.mpg; *.avi; *.mov; fw*; *.rif; *.ppm)\0*.mpg;*.m1v;*.avi;*.mov;fw*;*.rif;*.ppm\0");
  flatsign_movie = open_CR_Movie(filename);
  pt_w = flatsign_movie->im->w;
  pt_h = flatsign_movie->im->h;
  flatsign_image = make_CR_Image(pt_w, pt_h, 0);
  flatsign_flipimage = make_CR_Image(pt_w, pt_h, 0);
  im = make_CR_Image(pt_w, 7 * pt_h, 0);

  // initialize Gabor filters (vertical bar finder at various scales)

  flatsign_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_8U);
  flatsign_fp_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);
  flatsign_thresh_fp_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);
  flatsign_vert1_fp_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);
  flatsign_vert2_fp_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);
  flatsign_vert3_fp_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);
  flatsign_horiz1_fp_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);
  flatsign_horiz2_fp_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);
  flatsign_horiz3_fp_im = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);

  // set up filter bank (odd-numbered indices are the bar-finding kernels)

  /*
  flatsign_gabor_kernel_matrix = make_gabor_filter_bank_CR_Matrix_array(2.0, 0.5, 5, 90.0, 1, &flatsign_num_kernels);
  flatsign_gabor_kernel = (CR_Image_Kernel **) CR_calloc(flatsign_num_kernels, sizeof(CR_Image_Kernel *));
  for (i = 0; i < flatsign_num_kernels; i++) {
    flatsign_gabor_kernel[i] = make_CR_Image_Kernel(flatsign_gabor_kernel_matrix[i]);
//     if (!(i % 2) && i <= 8 ) {
//     printf("%i\n", i);
//     print_CR_Matrix(flatsign_gabor_kernel_matrix[i]);
//     }
  }
  printf("num_kernels = %i\n", flatsign_num_kernels);
  */

  flatsign_num_kernels = 3;

  flatsign_z_im = make_CR_Image(pt_w, pt_h, 0);
  flatsign_z_mat = make_CR_Matrix("z_mat", pt_h, pt_w);
  flatsign_z_var_mat = (CR_Matrix **) CR_calloc(flatsign_num_kernels, sizeof(CR_Matrix *));
  for (i = 0; i < flatsign_num_kernels; i++)
    flatsign_z_var_mat[i] = make_CR_Matrix("z_var_mat", pt_h, pt_w);

  // allocate images

  flatsign_vert_fp_im = (CR_Image **) CR_calloc(flatsign_num_kernels, sizeof(CR_Image *));
  flatsign_scaled_im = (CR_Image **) CR_calloc(flatsign_num_kernels, sizeof(CR_Image *));
  flatsign_display_im = (CR_Image **) CR_calloc(flatsign_num_kernels, sizeof(CR_Image *));
  for (i = 0; i < flatsign_num_kernels; i++) {
    flatsign_vert_fp_im[i] = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_32F);
    flatsign_scaled_im[i] = make_CR_Image(pt_w, pt_h, IPL_TYPE_GRAY_8U);
    flatsign_display_im[i] = make_CR_Image(pt_w, pt_h, 0);
  }
  flatsign_mean = (double *) CR_calloc(pt_w, sizeof(double));
  flatsign_cmap_im = make_CR_Image(pt_w, pt_h, 0);

  initialize_laser_colormap();

  // flatsign_vert1_fp_im -> flatsign_vert_fp_im[i]  32F
  // flatsign_im -> flatsign_scaled_im[i]  8U
  // flatsign_image -> flatsign_display_im[i]  color

  // finish

  return im;
}

//----------------------------------------------------------------------------

// compute variance of src over square size x size kernel
// assuming size is odd

void variance_filter_CR_Matrix(int size, CR_Matrix *src, CR_Matrix *dst)
{
  int r, c, i, j, offset, pixels;
  double mean, variance;

  offset = (size - 1) / 2;
  for (r = 0; r < src->rows; r++)
    for (c = 0; c < src->cols; c++) {

      // compute mean

      for (mean = 0, pixels = 0, j = r - offset; j <= r + offset; j++)
	for (i = c - offset; i <= c + offset; i++)
	  if (IN_MAT(src, j, i)) {
	    mean += MATRC(src, j, i);
	    pixels++;
	  }
      mean /= (double) pixels;

      // compute variance

      for (variance = 0, j = r - offset; j <= r + offset; j++)
	for (i = c - offset; i <= c + offset; i++)
	  if (IN_MAT(src, j, i)) 
	    variance += SQUARE(MATRC(src, j, i) - mean);
      MATRC(dst, r, c) = variance / (double) pixels;

    }
}

//----------------------------------------------------------------------------

void FlatSignFinder_grab_CR_CG(CR_Image *im)
{
  int x, y, index, i, j, range, z_intensity;
  double x_l, y_l, z_l;

  // filter image

  // convert laser image to IPL FP image and compute z-value of each pixel

  for (y = 0, index = 0; y < pt_h; y++)
    for (x = 0; x < pt_w; x++) {
      range = flatsign_movie->laserim[flatsign_movie->currentframe].data[index++];
      // range image
      GRAYFP_IMXY(flatsign_fp_im, x, y) = 0.25 * (float) range;
      // z_l image -- 76.8 m is max range, so it's also max z_l
      demo3_get_gall_laser_xyz(x, y, range, &x_l, &y_l, &z_l);
      MATRC(flatsign_z_mat, y, x) = z_l;
      z_intensity = 3.32 * z_l;
      set_pixel_rgb_CR_Image(x, y, z_intensity, z_intensity, z_intensity, flatsign_z_im);
    }

  // convolve by variance filters

  for (i = 0; i < flatsign_num_kernels; i++) {
    variance_filter_CR_Matrix(pow(2, i + 2) - 1, flatsign_z_mat, flatsign_z_var_mat[i]);
    draw_CR_Matrix_CR_Image(2.0, flatsign_z_var_mat[i], flatsign_display_im[i], TRUE);
  }

  // draw raw image

  glPixelZoom(1, 1);

  glRasterPos2i(0, 6 * pt_h);
  convert_laser_colormap(JET_CMAP, flatsign_movie, flatsign_cmap_im);
  vflip_CR_Image(flatsign_cmap_im, flatsign_flipimage);
  glDrawPixels(flatsign_flipimage->w, flatsign_flipimage->h, GL_RGB, GL_UNSIGNED_BYTE, flatsign_flipimage->data);

  // draw filtered images

  //  for (i = 0; i < flatsign_num_kernels; i++) {
  for (i = 0; i < 3; i++) {
    glRasterPos2i(0, (4 - i) * pt_h);
    vflip_CR_Image(flatsign_display_im[i], flatsign_flipimage);
    glDrawPixels(flatsign_flipimage->w, flatsign_flipimage->h, GL_RGB, GL_UNSIGNED_BYTE, flatsign_flipimage->data);
  }
  
  // draw z image

  glRasterPos2i(0, 5 * pt_h);
  vflip_CR_Image(flatsign_z_im, flatsign_flipimage);
  glDrawPixels(flatsign_flipimage->w, flatsign_flipimage->h, GL_RGB, GL_UNSIGNED_BYTE, flatsign_flipimage->data);

  // draw filtered image #1

//   glRasterPos2i(0, 4 * pt_h);
//   vflip_CR_Image(flatsign_image, flatsign_flipimage);
//   glDrawPixels(flatsign_flipimage->w, flatsign_flipimage->h, GL_RGB, GL_UNSIGNED_BYTE, flatsign_flipimage->data);

  //  glDrawPixels(flatsign_image->w, flatsign_image->h, GL_RGB, GL_UNSIGNED_BYTE, flatsign_image->data);

  // draw filtered image #2

//   glRasterPos2i(0, 3 * pt_h);
//   glDrawPixels(flatsign_image->w, flatsign_image->h, GL_RGB, GL_UNSIGNED_BYTE, flatsign_image->data);

  // advance to next movie frame

  if (flatsign_movie->currentframe < flatsign_movie->lastframe)
    get_frame_CR_Movie(flatsign_movie, flatsign_movie->currentframe++);
}

//----------------------------------------------------------------------------

void FlatSignFinder_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  fclose(mapfp);
  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Vector *yellow_Mean;
CR_Matrix *yellow_Cov, *yellow_InvCov, *yellow_Dist;
CR_Vector *orange_Mean;
CR_Matrix *orange_Cov, *orange_InvCov, *orange_Dist;

CR_Image *butterfly_im, *butterfly_fp_im, *butterfly_vert1_fp_im, *butterfly_vert2_fp_im, *butterfly_vert3_fp_im, *butterfly_horiz1_fp_im, *butterfly_horiz2_fp_im, *butterfly_horiz3_fp_im, *butterfly_thresh_fp_im; 
int butterfly_num_kernels;
CR_Image_Kernel **butterfly_gabor_kernel;
CR_Matrix **butterfly_gabor_kernel_matrix;

FILE *mapfp, *mapfp2;

CR_Image *Butterfly_init_CR_CG(char *name)
{
  CR_Image *im;
  int i, j, k;
  double this_time, laser_timestamp, laser_timestamp_last, camera_timestamp;
  CR_Image *orange_im, *yellow_im;

  //  mapfp = fopen("butterfly_map_laserandcolor.dlm", "w");

  // color

  yellow_Mean = make_CR_Vector("Mean", 3);
  yellow_Cov = make_CR_Matrix("Cov", 3, 3);
  yellow_InvCov = make_CR_Matrix("InvCov", 3, 3);
  yellow_Dist = make_CR_Matrix("Dist", 240, 360);

  orange_Mean = make_CR_Vector("Mean", 3);
  orange_Cov = make_CR_Matrix("Cov", 3, 3);
  orange_InvCov = make_CR_Matrix("InvCov", 3, 3);
  orange_Dist = make_CR_Matrix("Dist", 240, 360);

  orange_im = read_CR_Image("orange.ppm");
  yellow_im = read_CR_Image("yellow.ppm");

  rgb_mean_and_covariance_CR_Image(yellow_im, OUTLIER_NONE, yellow_Mean, yellow_Cov);
  inverse_CR_Matrix(yellow_Cov, yellow_InvCov);

  rgb_mean_and_covariance_CR_Image(orange_im, OUTLIER_NONE, orange_Mean, orange_Cov);
  inverse_CR_Matrix(orange_Cov, orange_InvCov);

  // laser filtering

  // initialize Gabor filters (vert. line finder, horiz. edge finder at various scales)

  butterfly_im = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, IPL_TYPE_GRAY_8U);
  butterfly_fp_im = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, IPL_TYPE_GRAY_32F);
  butterfly_thresh_fp_im = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, IPL_TYPE_GRAY_32F);
  butterfly_vert1_fp_im = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, IPL_TYPE_GRAY_32F);
  butterfly_vert2_fp_im = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, IPL_TYPE_GRAY_32F);
  butterfly_vert3_fp_im = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, IPL_TYPE_GRAY_32F);
  butterfly_horiz1_fp_im = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, IPL_TYPE_GRAY_32F);
  butterfly_horiz2_fp_im = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, IPL_TYPE_GRAY_32F);
  butterfly_horiz3_fp_im = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, IPL_TYPE_GRAY_32F);

  // set up filter bank

  butterfly_gabor_kernel_matrix = make_gabor_filter_bank_CR_Matrix_array(2.0, 1.0, 3, 0.0, 4, &butterfly_num_kernels);
  butterfly_gabor_kernel = (CR_Image_Kernel **) CR_calloc(butterfly_num_kernels, sizeof(CR_Image_Kernel *));
  for (i = 0; i < butterfly_num_kernels; i++) {
    butterfly_gabor_kernel[i] = make_CR_Image_Kernel(butterfly_gabor_kernel_matrix[i]);
    //    printf("%i\n", i);
    //    print_CR_Matrix(butterfly_gabor_kernel_matrix[i]);
  }
  printf("num_kernels = %i\n", butterfly_num_kernels);

  //  relfp = fopen("relpos.txt", "w");

  ITG_position = make_CR_Matrix("ITG_position", 200 * 640, 4);
  ITG_position_num = 0;

  // filtering initialization

  //  NIST_Road_initialize_feature_vectors_CR_Image(360, 240);
  ITG_laser_data = (int *) CR_calloc(D3_LASER_WIDTH * D3_LASER_HEIGHT, sizeof(int));

  // neural net initialization

  //  ITG_net = read_CR_NeuralNet("plane_digest_NN_20_ss_3.net");   // laser alone
  //  ITG_net = read_CR_NeuralNet("plane_digest_NN_20_ss_1.net");   // color alone
//   ITG_net = read_CR_NeuralNet("plane2_digest_NN_20_ss_4.net");
//   ITG_input = make_CR_Vector("ITG_input", ITG_net->W[0]->cols);
//   ITG_output = make_CR_Vector("ITG_output", 640);

//   camera_movie = open_CR_Movie("E:\\NIST_data\\itg_July17_2001\\morning\\half_road08-002.avi");

#ifndef ITG_NOCAMERA
  //  camera_movie = open_CR_Movie("D:\\NIST_data\\itg_July17_2001\\morning\\half_road08-002.avi");
  camera_movie = open_CR_Movie(ITG_camera_movie_name);

  camera_movie->currentframe = 0;
  get_frame_CR_Movie(camera_movie, camera_movie->currentframe);

  camera_image = make_CR_Image(camera_movie->im->w, camera_movie->im->h, 0);

  ITG_zoom_factor = camera_movie->width / D3_LASER_WIDTH;
  im = make_CR_Image(2 * camera_movie->width, camera_movie->height + D3_LASER_HEIGHT * ITG_zoom_factor, 0);

  init_laser2camera(camera_movie->im->w, camera_movie->im->h);
#else
  im = make_CR_Image(360, 64, 0);
  ITG_zoom_factor = 2;
#endif

  // read logfile

  logfile = fopen(ITG_log_name, "rb");

  bytes_per_camera_frame = 20;
  fseek(logfile, 0, SEEK_END);
  logfile_size = ftell(logfile);
  num_camera_frames = logfile_size / bytes_per_camera_frame;

  start_camera_flag = FALSE;
  camframetimes = make_CR_Vector("camframetimes", num_camera_frames);

  for (i = 0; i < num_camera_frames; i++) {

    fseek(logfile, i * bytes_per_camera_frame, SEEK_SET);
    fread(&time_seconds, 4, 1, logfile);
    fread(&time_microseconds, 4, 1, logfile); 
    fread(&total_frames_written, 4, 1, logfile);
    fread(&total_incomplete_frames, 4, 1, logfile);
    
    this_time = time_seconds + 0.000001 * time_microseconds;
    
    if (!start_camera_flag) {
      start_camera_time = this_time;
      start_camera_flag = TRUE;
    }

    camera_timestamp = this_time - start_camera_time;
    camframetimes->x[i] = camera_timestamp;
  }

  camera_startframe = ITG_camera_start_frame;

  printf("camframe %i time = %lf\n", camera_startframe, camframetimes->x[camera_startframe]);

  // open laser movie

  laser_movie = read_demo3_laser_to_CR_Movie(ITG_laser_movie_name);

  start_laser_time = laser_movie->laserim[0].navdat[0].time;

  laser_movie->currentframe = 0;
  do {
    get_frame_CR_Movie(laser_movie, laser_movie->currentframe);
    laser_timestamp = 0.001 * (laser_movie->laserim[laser_movie->currentframe].navdat[0].time - start_laser_time);
    laser_movie->currentframe++;
  } while (laser_timestamp < camframetimes->x[camera_startframe]);

  CR_flush_printf("frame = %i\n", laser_movie->currentframe);

  if (laser_movie->currentframe >= 2) {

    laser_timestamp_last = 0.001 * (laser_movie->laserim[laser_movie->currentframe - 2].navdat[0].time - start_laser_time);
    
    if (fabs(laser_timestamp - camframetimes->x[camera_startframe]) <
	fabs(laser_timestamp_last - camframetimes->x[camera_startframe]))
      laser_movie->currentframe = laser_movie->currentframe - 1;
    else
      laser_movie->currentframe = laser_movie->currentframe - 2;
  }
  else
    laser_movie->currentframe = 0;

  printf("laser current %i = %lf\n", laser_movie->currentframe, 0.001 * (laser_movie->laserim[laser_movie->currentframe].navdat[0].time - start_laser_time));

  laser_image = make_CR_Image(laser_movie->im->w, laser_movie->im->h, 0);

  // finish up

  //  track_3d_mov = write_avi_initialize_CR_Movie(NULL, im->w, im->h, 30);

  return im;
}

//----------------------------------------------------------------------------

double min_color_distance(int x, int y, int radius, CR_Matrix *color_dist)
{
  int r, c;
  double mindist;

  mindist = MATRC(color_dist, y, x);
  for (r = y - radius; r <= y + radius; r++)
    for (c = x - radius; c <= x + radius; c++)
      if (r >= 0 && r < color_dist->rows && c >= 0 && c < color_dist->cols &&
	  MATRC(color_dist, r, c) < mindist)
	mindist = MATRC(color_dist, r, c);
  return mindist;
}

//----------------------------------------------------------------------------

void Butterfly_grab_CR_CG(CR_Image *im)
{
  int i, j, k, x, y, index, index2, xc, yc;
  static char laser_frame[80], camera_frame[80], laser_time[80], camera_time[80];
  double laser_timestamp, tilt, xw, yw, zw, sum, xl, yl, zl, xw2, yw2, zw2, xcam, ycam, zcam;
  Demo3_NavData *navdat;

   if (laser_movie->currentframe == 1265) {
    mapfp2 = CR_emitEPS_setupfile("butterfly_squares2.ps", 0, 0, 360, 240);

    emitEPS_CR_Image(mapfp2, camera_movie->im);

//     write_CR_Image("butterfly_raw_laser.ppm", laser_movie->im);
//     write_CR_Image("butterfly_raw_color.ppm", camera_movie->im);
//     exit(1);
   }

  glPixelZoom(ITG_zoom_factor, ITG_zoom_factor);
#ifndef ITG_NOCAMERA
  glRasterPos2i(0, camera_movie->im->h);
#else
  glRasterPos2i(0, 240);
#endif

  //  vflip_CR_Image(laser_movie->im, laser_image);
  //  glDrawPixels(laser_image->w, laser_image->h, GL_RGB, GL_UNSIGNED_BYTE, laser_image->data);

  // filter laser image

  // first convert laser image to IPL FP image, then convolve by vert line filters


  for (y = 0, index = 0; y < D3_LASER_HEIGHT; y++)
    for (x = 0; x < D3_LASER_WIDTH; x++)
      GRAYFP_IMXY(butterfly_fp_im, x, y) = 0.25 * (float) laser_movie->laserim[laser_movie->currentframe].data[index++];

//   iplColorToGray(im->iplim, ts_gray_im->iplim);
//   iplScaleFP(butterfly_gray_im->iplim, butterfly_gray_fp_im->iplim, 0, 255);

  //  convolve by vert line Gabor filters

  //  convolve_CR_Image(butterfly_gabor_kernel[3], butterfly_fp_im, butterfly_vert2_fp_im);
  convolve_CR_Image(butterfly_gabor_kernel[10], butterfly_fp_im, butterfly_vert2_fp_im);  // 11
  convolve_CR_Image(butterfly_gabor_kernel[14], butterfly_fp_im, butterfly_vert1_fp_im);  // 11
  //  convolve_CR_Image(butterfly_gabor_kernel[18], butterfly_fp_im, butterfly_vert3_fp_im);



  // convolve output of first round by horiz edge Gabor filters  (0 and 8)

  //  convolve_CR_Image(butterfly_gabor_kernel[8], butterfly_fp_im, butterfly_horiz2_fp_im);
  //  convolve_CR_Image(butterfly_gabor_kernel[8], butterfly_vert2_fp_im, butterfly_horiz2_fp_im);

  // convert to displayable form

  //iplScaleFP(butterfly_horiz2_fp_im->iplim, butterfly_im->iplim, 0, 255);
  //  iplScaleFP(butterfly_vert2_fp_im->iplim, butterfly_im->iplim, 0, 255);

  for (y = 0, index = 0; y < D3_LASER_HEIGHT; y++)
    for (x = 0; x < D3_LASER_WIDTH; x++) {

      // left side

      if (GRAYFP_IMXY(butterfly_vert2_fp_im, x, y) >= 200.0) {   // 100.0 was good, but still noise
	//	GRAYFP_IMXY(butterfly_thresh_fp_im, x, y) = GRAYFP_IMXY(butterfly_vert2_fp_im, x, y);
	set_pixel_rgb_CR_Image(x, y, 255, 0, 0, laser_movie->im);
      }
      else {
	GRAYFP_IMXY(butterfly_thresh_fp_im, x, y) = 0.0;
	//	set_pixel_rgb_CR_Image(x, y, 255, 0, 0, laser_movie->im);
	//	GRAYFP_IMXY(butterfly_thresh_fp_im, x, y) = GRAYFP_IMXY(butterfly_fp_im, x, y);
      }

      // right side

      if (GRAYFP_IMXY(butterfly_vert1_fp_im, x, y) >= 200.0) {   // 100.0 was good, but still noise
	set_pixel_rgb_CR_Image(x, y, 0, 0, 255, laser_movie->im);
      }
      //      else {
      //	GRAYFP_IMXY(butterfly_thresh_fp_im, x, y) = 0.0;
	//	set_pixel_rgb_CR_Image(x, y, 255, 0, 0, laser_movie->im);
	//	GRAYFP_IMXY(butterfly_thresh_fp_im, x, y) = GRAYFP_IMXY(butterfly_fp_im, x, y);
      //      }

    }

  for (y = 0, index = 0; y < D3_LASER_HEIGHT; y++)
    for (x = 0; x < D3_LASER_WIDTH; x++) 
	GRAYFP_IMXY(butterfly_thresh_fp_im, x, y) = 0;

  for (y = 0, index = 0; y < D3_LASER_HEIGHT; y++)
    for (x = 0; x < D3_LASER_WIDTH; x++) {

      // adjacent

      if ((GRAYFP_IMXY(butterfly_vert2_fp_im, x, y) >= 200.0 && 
	   GRAYFP_IMXY(butterfly_vert1_fp_im, x + 1, y) >= 200.0) ||
	  (GRAYFP_IMXY(butterfly_vert2_fp_im, x - 1, y) >= 200.0 && 
	   GRAYFP_IMXY(butterfly_vert1_fp_im, x, y) >= 200.0))
	GRAYFP_IMXY(butterfly_thresh_fp_im, x, y) = 255;

      // one pix sep

      if (GRAYFP_IMXY(butterfly_vert2_fp_im, x - 1, y) >= 200.0 && 
	  GRAYFP_IMXY(butterfly_vert1_fp_im, x + 1, y) >= 200.0)
	GRAYFP_IMXY(butterfly_thresh_fp_im, x, y) = 255;

      // two pix sep

      if ((GRAYFP_IMXY(butterfly_vert2_fp_im, x, y) >= 200.0 && 
	   GRAYFP_IMXY(butterfly_vert1_fp_im, x + 2, y) >= 200.0) ||
	  (GRAYFP_IMXY(butterfly_vert2_fp_im, x - 2, y) >= 200.0 && 
	   GRAYFP_IMXY(butterfly_vert1_fp_im, x, y) >= 200.0))
	GRAYFP_IMXY(butterfly_thresh_fp_im, x, y) = 255;

      // three pix sep

      if (GRAYFP_IMXY(butterfly_vert2_fp_im, x - 2, y) >= 200.0 && 
	  GRAYFP_IMXY(butterfly_vert1_fp_im, x + 2, y) >= 200.0)
	GRAYFP_IMXY(butterfly_thresh_fp_im, x, y) = 255;

    }

//   if (laser_movie->currentframe == 1265) {
//     write_CR_Image("butterfly_laser_bluered.ppm", laser_movie->im);
//   }

  vflip_CR_Image(laser_movie->im, laser_image);
  glDrawPixels(laser_image->w, laser_image->h, GL_RGB, GL_UNSIGNED_BYTE, laser_image->data);

  iplScaleFP(butterfly_thresh_fp_im->iplim, butterfly_im->iplim, 0, 255);
  iplGrayToColor(butterfly_im->iplim, laser_movie->im->iplim, 1, 1, 1);

  if (laser_movie->currentframe == 1265) {
  //  CR_emitEPS_closefile(fp);
//     write_CR_Image("butterfly_laser_cooccurrence.ppm", laser_movie->im);
//    
//     iplScaleFP(butterfly_vert2_fp_im->iplim, butterfly_im->iplim, 0, 255);
//     iplGrayToColor(butterfly_im->iplim, laser_movie->im->iplim, 1, 1, 1);
//     
//     write_CR_Image("butterfly_gabor_left.ppm", laser_movie->im);
//     
//     iplScaleFP(butterfly_vert1_fp_im->iplim, butterfly_im->iplim, 0, 255);
//     iplGrayToColor(butterfly_im->iplim, laser_movie->im->iplim, 1, 1, 1);
//     
//     write_CR_Image("butterfly_gabor_right.ppm", laser_movie->im);


//     draw_CR_Matrix_CR_Image(15, orange_Dist, camera_movie->im, TRUE);
//     write_CR_Image("butterfly_color_dist_15.ppm", camera_movie->im);
// 
//     mapfp = fopen("butterfly_vehicle.dlm", "w");
// 
//   navdat = &laser_movie->laserim[laser_movie->currentframe].navdat[0];
//   tilt = laser_movie->laserim[laser_movie->currentframe].frameHeaders[0].tiltAngle;
// 
//     ulaser2world(0, 0, 0, 
// 		 &xw, &yw, &zw,
// 		 navdat->rot.s, navdat->rot.x, navdat->rot.y, navdat->rot.z,
// 		 navdat->tranRel.x, navdat->tranRel.y, navdat->tranRel.z);
//     
//     ulaser2world(0, 0, 2.0, 
// 		 &xw2, &yw2, &zw2,
// 		 navdat->rot.s, navdat->rot.x, navdat->rot.y, navdat->rot.z,
// 		 navdat->tranRel.x, navdat->tranRel.y, navdat->tranRel.z);
//     
//     fprintf(mapfp, "%i,%lf,%lf,%lf,%lf,%lf,%lf\n", laser_movie->currentframe, xw, yw, zw, xw2, yw2, zw2);
//     fclose(mapfp);

  }

  /*
  for (i = 0; i < butterfly_num_kernels / 2; i++) {

    // simple odd, even responses

    convolve_CR_Image(butterfly_gabor_kernel[2 * i], butterfly_fp_im, butterfly_odd_fp_im);
    convolve_CR_Image(butterfly_gabor_kernel[2 * i + 1], butterfly_fp_im, butterfly_even_fp_im);
  }
  */

  glRasterPos2i(360, 240);
  
  vflip_CR_Image(laser_movie->im, laser_image);
  glDrawPixels(laser_image->w, laser_image->h, GL_RGB, GL_UNSIGNED_BYTE, laser_image->data);



//   sprintf(laser_frame, "%i", laser_movie->currentframe);
//   ogl_draw_string(laser_frame, 10, camera_movie->im->h + 20, 255, 255, 0); 

  laser_timestamp = 0.001 * (laser_movie->laserim[laser_movie->currentframe].navdat[0].time - start_laser_time);
//   sprintf(laser_time, "%.3lf", laser_timestamp);
//   ogl_draw_string(laser_time, 10, camera_movie->im->h + 8, 255, 255, 0); 

  // sync camera image with laser image

#ifndef ITG_NOCAMERA
  for ( ; camera_movie->currentframe < camera_movie->lastframe && camframetimes->x[camera_movie->currentframe + camera_startframe] < laser_timestamp; camera_movie->currentframe++);
  if (camera_movie->currentframe > 0 &&
      fabs(camframetimes->x[camera_movie->currentframe + camera_startframe] - laser_timestamp) > 
      fabs(camframetimes->x[camera_movie->currentframe + camera_startframe - 1] - laser_timestamp))
    camera_movie->currentframe = camera_movie->currentframe - 1; 
  get_frame_CR_Movie(camera_movie, camera_movie->currentframe);

  // image processing

  set_all_CR_Matrix(10, yellow_Dist);
  rgb_mahalanobis_distance_CR_Image(camera_movie->im, yellow_Dist, yellow_Mean, yellow_InvCov); 

  set_all_CR_Matrix(10, orange_Dist);
  rgb_mahalanobis_distance_CR_Image(camera_movie->im, orange_Dist, orange_Mean, orange_InvCov); 

  //  draw_CR_Matrix_CR_Image(5, yellow_Dist, camera_movie->im, TRUE);
  //  draw_CR_Matrix_CR_Image(10, orange_Dist, camera_movie->im, TRUE);

  // draw camera image

  glPixelZoom(1, 1);
  glRasterPos2i(0, 0);

  // draw raw camera image

  vflip_CR_Image(camera_movie->im, camera_image);
  glDrawPixels(camera_image->w, camera_image->h, GL_RGB, GL_UNSIGNED_BYTE, camera_image->data);

  // draw processed camera image

//   draw_CR_Matrix_CR_Image(10, orange_Dist, camera_movie->im, TRUE);
//   glRasterPos2i(360, 0);
//   vflip_CR_Image(camera_movie->im, camera_image);
//   glDrawPixels(camera_image->w, camera_image->h, GL_RGB, GL_UNSIGNED_BYTE, camera_image->data);

  // project laser to camera

  //  set_all_CR_Matrix(0, orange_Dist);

      //  draw_CR_Matrix_CR_Image(100, orange_Dist, camera_movie->im, FALSE);
  glRasterPos2i(360, 0);
  vflip_CR_Image(camera_movie->im, camera_image);
  glDrawPixels(camera_image->w, camera_image->h, GL_RGB, GL_UNSIGNED_BYTE, camera_image->data);

  int ss = 10;
  double mindist_orange, mindist_yellow;

  navdat = &laser_movie->laserim[laser_movie->currentframe].navdat[0];
  tilt = laser_movie->laserim[laser_movie->currentframe].frameHeaders[0].tiltAngle;

  for (j = 0, index = 0; j < D3_LASER_HEIGHT; j++)
    for (i = 0; i < D3_LASER_WIDTH; i++) {
	
      if (GRAYFP_IMXY(butterfly_thresh_fp_im, i, j)) {
	laser2camera(i, j, laser_movie->laserim[laser_movie->currentframe].data[index], &xc, &yc, &xcam, &ycam, &zcam, laser_movie->laserim[laser_movie->currentframe].frameHeaders[0].tiltAngle);

	/*
	printf("%i\n", laser_movie->laserim[laser_movie->currentframe].data[index]);

	if (laser_movie->laserim[laser_movie->currentframe].data[index] < 1000) {
	  laser2world(i, j, laser_movie->laserim[laser_movie->currentframe].data[index], 
		      &xw, &yw, &zw,
		      tilt, 
		      navdat->rot.s, navdat->rot.x, navdat->rot.y, navdat->rot.z,
		      navdat->tranRel.x, navdat->tranRel.y, navdat->tranRel.z);
	  
	  fprintf(mapfp, "%i,%lf,%lf,%lf\n", laser_movie->currentframe, xw, yw, zw);
	}
	*/

	if (IN_IMAGE(camera_image, xc, yc)) {
	  mindist_orange = min_color_distance(xc, yc, ss, orange_Dist);
	  //	  printf("o: %lf\n", mindist_orange);
// 	  mindist_yellow = min_color_distance(xc, yc, ss, yellow_Dist);
// 	  printf("y: %lf\n", mindist_yellow);
	  if (mindist_orange < 6.0) { // confirmed!
	    ogl_draw_rectangle(360 + xc - ss, 239 - (yc - ss), 360 + xc + ss, 239 - (yc + ss), 255, 0, 0);

	    if (laser_movie->currentframe == 1265) {
	      CR_emitEPS_rectangle(mapfp2, xc, 240 - yc, 2 * ss, 2 * ss, 0.0, 1, 0, 0, 0);

	    }

	    //	    printf("%i\n", laser_movie->currentframe);

	    // map it

// 	    if (laser_movie->laserim[laser_movie->currentframe].data[index] < 1000) {
//  
// 	    laser2world(i, j, laser_movie->laserim[laser_movie->currentframe].data[index], 
// 			&xw, &yw, &zw,
// 			tilt, 
// 			navdat->rot.s, navdat->rot.x, navdat->rot.y, navdat->rot.z,
// 			navdat->tranRel.x, navdat->tranRel.y, navdat->tranRel.z);
// 
// 	    fprintf(mapfp, "%i,%lf,%lf,%lf\n", laser_movie->currentframe, xw, yw, zw);
// 	    }
	  }
	}
   //	  set_pixel_rgb_CR_Image(xc, yc, 255, 0, 0, camera_movie->im);

	  //	  MATRC(orange_Dist, yc, xc) = GRAYFP_IMXY(butterfly_vert2_fp_im, i, j);
	  //  GRAYFP_IMXY(butterfly_fp_im, i, j) = GRAYFP_IMXY(butterfly_vert2_fp_im, i, j);
	  //	GRAYFP_IMXY(butterfly_fp_im, i, j) = GRAYFP_IMXY(butterfly_vert2_fp_im, i, j) / MATRC(orange_Dist, yc, xc); 
// 	else
// 	  GRAYFP_IMXY(butterfly_fp_im, i, j) = 0;
      }
      index++;
    }

  if (laser_movie->currentframe == 1265) {
    CR_emitEPS_closefile(mapfp2);
    exit(1);
  }

  //  printf("------------------------------------\n");

//   iplScaleFP(butterfly_fp_im->iplim, butterfly_im->iplim, 0, 255);
//   iplGrayToColor(butterfly_im->iplim, laser_movie->im->iplim, 1, 1, 1);
// 
//   glPixelZoom(ITG_zoom_factor, ITG_zoom_factor);
//   glRasterPos2i(360, 240);
// 
//   vflip_CR_Image(laser_movie->im, laser_image);
//   glDrawPixels(laser_image->w, laser_image->h, GL_RGB, GL_UNSIGNED_BYTE, laser_image->data);

  // other stuff

//   sprintf(camera_frame, "%i", camera_movie->currentframe);
//   ogl_draw_string(camera_frame, 10, 20, 255, 255, 0); 
// 
//   sprintf(camera_time, "%.3lf", camframetimes->x[camera_movie->currentframe]);
//   ogl_draw_string(camera_time, 10, 8, 255, 255, 0); 
#endif

  // map it

  /*
  navdat = &laser_movie->laserim[laser_movie->currentframe].navdat[0];
  tilt = laser_movie->laserim[laser_movie->currentframe].frameHeaders[0].tiltAngle;

  if (!(laser_movie->currentframe % 50)) {

    printf("writing %i\n", laser_movie->currentframe);
//     sprintf(ppmname, "cmap_frame_%i.ppm\0", laser_movie->currentframe);
//     printf("%s\n", ppmname);
//     write_CR_Image(ppmname, camera_movie->im);

   ulaser2world(0, 0, 0, 
		 &xw, &yw, &zw,
		 navdat->rot.s, navdat->rot.x, navdat->rot.y, navdat->rot.z,
		 navdat->tranRel.x, navdat->tranRel.y, navdat->tranRel.z);

   ulaser2world(0, 0, 2.0, 
		 &xw2, &yw2, &zw2,
		 navdat->rot.s, navdat->rot.x, navdat->rot.y, navdat->rot.z,
		 navdat->tranRel.x, navdat->tranRel.y, navdat->tranRel.z);

   fprintf(relfp, "%i,%lf,%lf,%lf,%lf,%lf,%lf\n", laser_movie->currentframe, xw, yw, zw, xw2, yw2, zw2);
  */

//  ITG_position->rows = ITG_position_num;

  // advance laser frame
  
#ifndef ITG_NOCAMERA
  if (laser_movie->currentframe < laser_movie->lastframe &&
      camera_movie->currentframe < camera_movie->lastframe) 
    get_frame_CR_Movie(laser_movie, laser_movie->currentframe++);
#else
  if (laser_movie->currentframe < laser_movie->lastframe)
    get_frame_CR_Movie(laser_movie, laser_movie->currentframe++);
#endif

  /*
  // draw camera image

  glPixelZoom(1, 1);
  glRasterPos2i(0, 0);
   
  vflip_CR_Image(camera_movie->im, camera_image);
  glDrawPixels(camera_image->w, camera_image->h, GL_RGB, GL_UNSIGNED_BYTE, camera_image->data);
 
  // advance camera frame

  if (camera_movie->currentframe < camera_movie->lastframe) 
    get_frame_CR_Movie(camera_movie, camera_movie->currentframe++);
  */

}

//----------------------------------------------------------------------------

void Butterfly_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  fclose(mapfp);
  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Image *puddle_im, *tpuddle_im, *puddle_im1, *puddle_im2, *puddle_im3; 
CR_Matrix *puddle_xi;
CR_Vector *puddle_samp;

FILE *puddlefp, *puddleboxfp, *lpuddleboxfp;

CR_Matrix *puddleA, *puddleB, *puddleC;
int *is_puddle, *puddle_range, *min_x, *min_y;
double *puddle_height;

CR_Image *Puddle_init_CR_CG(char *name)
{
  CR_Image *im;
  int i, j, k;
  double this_time, laser_timestamp, laser_timestamp_last, camera_timestamp;

  puddlefp = fopen("puddle_map.dlm", "w");

  puddle_xi = make_diagonal_all_CR_Matrix("xi", 1, 1.0);
  puddle_samp = make_CR_Vector("psamp", 1);

  init_laser2camera(360, 240);

  //  mapfp = fopen("butterfly_map_laserandcolor.dlm", "w");

  // laser filtering

  // initialize Gabor filters (vert. line finder, horiz. edge finder at various scales)

  puddle_im = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, 0);

  tpuddle_im = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, 0);
  //  puddle_im = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, IPL_TYPE_GRAY_8U);
  puddle_im1 = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, IPL_TYPE_GRAY_8U);
  puddle_im2 = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, IPL_TYPE_GRAY_8U);

  //  puddle_im3 = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, IPL_TYPE_GRAY_32F);

  im = make_CR_Image(360, 64, 0);
  ITG_zoom_factor = 2;

  // open laser movie

  laser_movie = read_demo3_laser_to_CR_Movie("fw080");
  laser_movie->currentframe = 0;
  get_frame_CR_Movie(laser_movie, laser_movie->currentframe);

  laser_image = make_CR_Image(laser_movie->im->w, laser_movie->im->h, 0);

  // finish up

  puddleA = make_CR_Matrix("A", D3_LASER_HEIGHT, D3_LASER_WIDTH);
  puddleB = make_CR_Matrix("B", D3_LASER_HEIGHT, D3_LASER_WIDTH);
  puddleC = make_CR_Matrix("C", D3_LASER_HEIGHT, D3_LASER_WIDTH);
  is_puddle = (int *) CR_calloc(180 * 32, sizeof(int));
  min_x = (int *) CR_calloc(180 * 32, sizeof(int));
  min_y = (int *) CR_calloc(180 * 32, sizeof(int));
  puddle_height = (double *) CR_calloc(180 * 32, sizeof(double));
  puddle_range = (int *) CR_calloc(180 * 32, sizeof(int));

  //  track_3d_mov = write_avi_initialize_CR_Movie(NULL, im->w, im->h, 30);

  return im;
}

//----------------------------------------------------------------------------

double puddle_minheight;
int puddle_x, puddle_y, puddle_minrange, puddle_minfunc_calls;
Demo3_NavData *puddle_navdat;
double puddle_tilt;

double puddle_minfunc(CR_Vector *samp)
{
  double xw, yw, zw;

  puddle_minfunc_calls++;

  // note negation because we're achieving maximization of f(x) by minimizing -f(x)

  dlaser2world(puddle_x, puddle_y, samp->x[0],
	      &xw, &yw, &zw,
	      puddle_tilt, 
	      puddle_navdat->rot.s, puddle_navdat->rot.x, puddle_navdat->rot.y, puddle_navdat->rot.z,
	      puddle_navdat->tranRel.x, puddle_navdat->tranRel.y, puddle_navdat->tranRel.z);

  //  printf("eval = %lf (%lf)\n", fabs(puddle_minheight - yw), samp->x[0]);
  return fabs(puddle_minheight - yw);
}

//----------------------------------------------------------------------------

double puddle_powell_range(int x, int y, int start_range, CR_Movie *laser_movie)
{
  int iter;
  double fret, minfret;
  double i;
  CR_Vector *samp;
  double minrange;

  puddle_navdat = &laser_movie->laserim[laser_movie->currentframe].navdat[0];
  puddle_tilt = laser_movie->laserim[laser_movie->currentframe].frameHeaders[0].tiltAngle;

  puddle_x = x;
  puddle_y = y;

  //  puddle_samp->x[0] = (double) (start_range + 5);
  puddle_samp->x[0] = (double) (start_range);

  minfret = 10000000;
  for (puddle_samp->x[0] = 0; puddle_samp->x[0] <= 1000; puddle_samp->x[0] += 1) {
    fret = puddle_minfunc(puddle_samp);
    if (fret < minfret) {
      minfret = fret;
      minrange = puddle_samp->x[0];
      //   printf("%lf = %lf\n", puddle_samp->x[0], fret);
      
    }
  }

   return minrange;
   // exit(1);

  puddle_minfunc_calls = 0;
  powell_CR_Matrix(puddle_samp, puddle_xi, 0.01, &iter, &fret, puddle_minfunc);
  //  printf("%i function calls in Powell\n", puddle_minfunc_calls);  

  //  printf("start = %i, fret = %lf, samp = %lf\n", start_range, fret, puddle_samp->x[0]);
  return CR_round(puddle_samp->x[0]);
}

//----------------------------------------------------------------------------

double quickheight(int x, int y, int index, CR_Movie *laser_movie)
{
  Demo3_NavData *navdat;
  double tilt, xw, yw, zw;
  
  //  printf("quickcall\n");

  navdat = &laser_movie->laserim[laser_movie->currentframe].navdat[0];
  tilt = laser_movie->laserim[laser_movie->currentframe].frameHeaders[0].tiltAngle;

  laser2world(x, y, laser_movie->laserim[laser_movie->currentframe].data[index], 
	      &xw, &yw, &zw,
	      tilt, 
	      navdat->rot.s, navdat->rot.x, navdat->rot.y, navdat->rot.z,
	      navdat->tranRel.x, navdat->tranRel.y, navdat->tranRel.z);

  return yw;
}

//----------------------------------------------------------------------------

void get_puddle_ranges(double cc, CR_Matrix *A, CR_Movie *laser_movie, double puddle_h, int puddle_r, CR_Matrix *B)
{
  int r, c, xc, yc, index, minr, minc, maxr, maxc, minindex, maxindex, minx, miny, maxx, maxy, minrange, maxrange;
  int x11, y11, x21, y21, x12, y12, x22, y22;
  double range;
  Demo3_NavData *navdat;
  double tilt, xw, yw, zw, xl, yl, zl;
  CR_Matrix *myB;

  myB = make_CR_Matrix("B", A->rows, B->rows);

  navdat = &laser_movie->laserim[laser_movie->currentframe].navdat[0];
  tilt = laser_movie->laserim[laser_movie->currentframe].frameHeaders[0].tiltAngle;

  printf("---------------------------------------------------\n");

  for (r = 0; r < A->rows; r++) {
    set_pixel_rgb_CR_Image(0, r, 0, 0, 0, tpuddle_im);
    set_pixel_rgb_CR_Image(A->cols - 1, r, 0, 0, 0, tpuddle_im);
  }
  for (c = 0; c < A->cols; c++) {
    set_pixel_rgb_CR_Image(c, 0, 0, 0, 0, tpuddle_im);
    set_pixel_rgb_CR_Image(c, A->rows - 1, 0, 0, 0, tpuddle_im);
  }

  minr=minc=100000;
  maxr=maxc=-100000;
  
  for (r = 0, index = 0; r < A->rows; r++)
    for (c = 0; c < A->cols; c++) {
      if ((r >= 1 && r < A->rows - 1 && c >= 1 && c < A->cols - 1) && 
	  (MATRC(A, r, c) == cc) &&
	  ((unsigned char) IMXY_R(laser_movie->im, c, r) == 0)) {

	puddle_minheight = puddle_h;
	range = puddle_powell_range(c, r, puddle_r, laser_movie);

	MATRC(myB, r, c) = range;

	if (c < minc && r < minr) {
	  minc = c;
	  minr = r;
 	  minrange = (int) CR_round(range);
	}
	else if (c > maxc && r > maxr) {
	  maxc = c;
	  maxr = r;
 	  maxrange = (int) CR_round(range);

	}

	if (c < minc) {
	  minc = c;
// 	  minrange = (int) CR_round(range);
// 	  minindex = index;
	}
	else if (c > maxc) {
	  maxc = c;
// 	  maxrange = (int) CR_round(range);
// 	  maxindex = index;
	}

	if (r < minr) {
	  minr = r;
// 	  minrange = (int) CR_round(range);
// 	  minindex = index;
	}
	else if (r > maxr) {
	  maxr = r;
// 	  maxrange = (int) CR_round(range);
// 	  maxindex = index;
	}

	set_pixel_rgb_CR_Image(c, r, 255-range/4, 255-range/4, 255-range/4, tpuddle_im);

	dlaser2world(c, r, range,
		     &xw, &yw, &zw,
		     tilt, 
		     navdat->rot.s, navdat->rot.x, navdat->rot.y, navdat->rot.z,
		     navdat->tranRel.x, navdat->tranRel.y, navdat->tranRel.z);

	fprintf(puddlefp, "%i,2,%lf,%lf,%lf,0,0,0\n", laser_movie->currentframe, xw, yw, zw);

	//	printf("prange = %i, range = %lf\n", puddle_range, range);
      }
      index++;
    }

  /*
  do {
    minr--;
  } while ((unsigned char) IMXY_R(laser_movie->im, minc, minr) != 0);
  do { 
    maxr++;
  } while ((unsigned char) IMXY_R(laser_movie->im, maxc, maxr) != 0);
  do { 
  minc--;
  } while ((unsigned char) IMXY_R(laser_movie->im, minc, minr) != 0);
  do { 
  maxc++;
  } while ((unsigned char) IMXY_R(laser_movie->im, maxc, maxr) != 0);

  for (r = 0, index = 0; r < A->rows; r++)
    for (c = 0; c < A->cols; c++) {

      if (r == minr && c == minc) {
	if (MATRC(A, r, c) == cc)
	  laser2camera(c, r, laser_movie->laserim[laser_movie->currentframe].data[index],
		       &x11, &y11, &xl, &yl, &zl, tilt);
// 	else
// 	  laser2camera(minc, minr, MATRC(myB, r, c),
// 		       &x11, &y11, &xl, &yl, &zl, tilt);
      }

      else if (r == minr && c == maxc) {
	if (MATRC(A, r, c) == cc)
	  laser2camera(c, r, laser_movie->laserim[laser_movie->currentframe].data[index],
		       &x21, &y21, &xl, &yl, &zl, tilt);
// 	else
// 	  laser2camera(maxc, minr, MATRC(myB, r, c),
// 		       &x21, &y21, &xl, &yl, &zl, tilt);
      }

      else if (r == maxr && c == minc) {
	if (MATRC(A, r, c) == cc)
	  laser2camera(c, r, laser_movie->laserim[laser_movie->currentframe].data[index],
		       &x12, &y12, &xl, &yl, &zl, tilt);
// 	else
// 	  laser2camera(minc, maxr, MATRC(myB, r, c),
// 		       &x12, &y12, &xl, &yl, &zl, tilt);
      }

      else if (r == maxr && c == maxc) {
	if (MATRC(A, r, c) == cc)
	  laser2camera(c, r, laser_movie->laserim[laser_movie->currentframe].data[index],
		       &x22, &y22, &xl, &yl, &zl, tilt);
// 	else
// 	  laser2camera(maxc, maxr, MATRC(myB, r, c),
// 		       &x22, &y22, &xl, &yl, &zl, tilt);
      }

      index++;
    }
  */

  laser2camera(minc, minr, minrange,
	       &minx, &miny, &xl, &yl, &zl, tilt);
  laser2camera(maxc, maxr, maxrange,
	       &maxx, &maxy, &xl, &yl, &zl, tilt);

//   printf("minc = %i, minr = %i\n", minc, minr);
//   printf("maxc = %i, maxr = %i\n", maxc, maxr);
// 
//   printf("x11 = %i, y11 = %i\n", x11, y11);
//   printf("x21 = %i, y21 = %i\n", x21, y21);
//   printf("x12 = %i, y12 = %i\n", x12, y12);
//   printf("x22 = %i, y22 = %i\n", x22, y22);

  //  CR_emitEPS_rectangle(lpuddleboxfp, (minc+maxc)/2, 32 - (minr+maxr)/2, maxc-minc, maxr-minr, 0.0, 1, 0, 255, 0);
  CR_emitEPS_rectangle(puddleboxfp, (minx+maxx)/2, 240 - (miny+maxy)/2, maxx-minx, maxy-miny, 0.0, 1, 0, 255, 0);

//   minx = MIN2(x11, x12);
//   maxx = MIN2(x21, x22);
//   miny = MIN2(y11, y21);
//   maxy = MIN2(y12, y22);
// 
//   CR_emitEPS_rectangle(puddleboxfp, (minx+maxx)/2, 240 - (miny+maxy)/2, maxx-minx, maxy-miny, 0.0, 1, 0, 255, 0);
}

//----------------------------------------------------------------------------

// is puddle if no pixel of cc is in first row

int verify_puddle_and_get_height(double cc, CR_Matrix *A, CR_Movie *laser_movie, double *height, int *range, int *mc, int *mr)
{
  int r, c, isgood, index, minr, minc;
  double h;

  // first verify

  for (c = 0, isgood = TRUE; c < A->cols; c++)
    if (MATRC(A, 0, c) == cc) {
      isgood = FALSE;
      break;
    }
  
  if (!isgood)
    return FALSE;
//   else
//     return TRUE;

  // verified, so compute height

  puddle_minheight = 1000000;

  printf("cc = %i\n", cc);

  for (r = 0, index = 0; r < A->rows; r++)
    for (c = 0; c < A->cols; c++) {
      //      printf("again\n");
      //      if ((r >= 1 && r < A->rows - 1 && c >= 1 && c < A->cols - 1) && MATRC(A, r, c) == 0) {
      if ((r >= 1 && r < A->rows - 1 && c >= 1 && c < A->cols - 1) && ((unsigned char) IMXY_R(laser_movie->im, c, r) > 0) && (MATRC(A, r, c) == 0)) {
	h = quickheight(c, r, index, laser_movie);
// 	if (r == 21 && c == 80) {
// 	  printf("cc = %lf\n", MATRC(A, r, c));
// 
// 	  printf("u = %lf\n", MATRC(A, r-1, c));
// 	  printf("d = %lf\n", MATRC(A, r+1, c));
// 	  printf("l = %lf\n", MATRC(A, r, c-1));
// 	  printf("r = %lf\n", MATRC(A, r, c+1));
// 	  
// 	  printf("h = %lf, minh = %lf\n", h, puddle_minheight);
// 	}
	// up
	if (MATRC(A, r - 1, c) == cc && h < puddle_minheight) {
	  //	  printf("u\n");
	  minr = r;
	  minc = c;
	  puddle_minheight = h;
	  puddle_minrange = laser_movie->laserim[laser_movie->currentframe].data[index];
	}
	// down
	if (MATRC(A, r + 1, c) == cc && h < puddle_minheight) {
	  //	  printf("d\n");
	  minr = r;
	  minc = c;
	  puddle_minheight = h;
	  puddle_minrange = laser_movie->laserim[laser_movie->currentframe].data[index];
	}
	// left
	if (MATRC(A, r, c - 1) == cc && h < puddle_minheight) {
	  //printf("l\n");

	  minr = r;
	  minc = c;
	  puddle_minheight = h;
	  puddle_minrange = laser_movie->laserim[laser_movie->currentframe].data[index];
	}
	// right
	if (MATRC(A, r, c + 1) == cc && h < puddle_minheight) {
	  //printf("r\n");

	  minr = r;
	  minc = c;
	  puddle_minheight = h;
	  puddle_minrange = laser_movie->laserim[laser_movie->currentframe].data[index];
	}
// 	if (r == 21 && c == 80) 
// 	  exit(1);
      }
      index++;
    }

  *height = puddle_minheight;
  *range = puddle_minrange;
  *mr = minr;
  *mc = minc;
  return TRUE;
}

//----------------------------------------------------------------------------

void Puddle_grab_CR_CG(CR_Image *im)
{
  int i, j, k, x, y, index, index2, xc, yc;
  static char laser_frame[80], camera_frame[80], laser_time[80], camera_time[80];
  double laser_timestamp, tilt, xw, yw, zw, sum, xl, yl, zl, xw2, yw2, zw2, xcam, ycam, zcam;
  Demo3_NavData *navdat;
  int numccs;

  glPixelZoom(ITG_zoom_factor, ITG_zoom_factor);
  //  glRasterPos2i(0, 240);

//   iplColorToGray(im->iplim, ts_gray_im->iplim);


  for (y = 0, index = 0; y < D3_LASER_HEIGHT; y++)
    for (x = 0; x < D3_LASER_WIDTH; x++) {

      if (laser_movie->laserim[laser_movie->currentframe].data[index] >= 1000)
	set_pixel_rgb_CR_Image(x, y, 0, 0, 0, laser_movie->im);   
      else 
	set_pixel_rgb_CR_Image(x, y, 255, 255, 255, laser_movie->im);

      index++;
    }


  //  if (laser_movie->currentframe >= 0) {
    //  if (laser_movie->currentframe >= 135 && laser_movie->currentframe < 200) {
  if (laser_movie->currentframe == 139) {

    puddleboxfp = CR_emitEPS_setupfile("puddle_squares.ps", 0, 0, 360, 240);
    lpuddleboxfp = CR_emitEPS_setupfile("puddle_lsquares.ps", 0, 0, 180, 32);

    set_CR_Image(255, tpuddle_im);
    
    //    write_CR_Image("waterandsky_080.ppm", laser_movie->im);
    iplClose(laser_movie->im->iplim, puddle_im->iplim, 1);
    //ip_copy_CR_Image(laser_movie->im, puddle_im);
    
    for (y = 0; y < D3_LASER_HEIGHT; y++)
      for (x = 0; x < D3_LASER_WIDTH; x++) {
	if (((unsigned char) IMXY_R(puddle_im, x, y)) > 0)
	  MATRC(puddleA, y, x) = 0;
	else
	  MATRC(puddleA, y, x) = 1;
      }

//     draw_CR_Matrix_CR_Image(1, puddleA, puddle_im1, FALSE);
//     write_CR_Image("pud.ppm", puddle_im);
    //     write_CR_Image("pud1.ppm", puddle_im1);
    //     exit(1);
    
    numccs = connected_components_CR_Matrix(TRUE, puddleA, puddleB);
    
    //     draw_CR_Matrix_CR_Image(3, puddleB, tpuddle_im, FALSE);
    //     write_CR_Image("pudcc.ppm", tpuddle_im);
    //    exit(1);
    
    for (i = 0; i < numccs; i++) {
      is_puddle[i] = verify_puddle_and_get_height(i+1, puddleB, laser_movie, &puddle_height[i], &puddle_range[i], &min_x[i], &min_y[i]);
      //printf("%i: %i\n", i, is_puddle[i]);
      printf("%i: %i -> %lf, %i (x = %i, y = %i)\n", i, is_puddle[i], puddle_height[i], puddle_range[i], min_x[i], min_y[i]);
      
      if (is_puddle[i])
 	get_puddle_ranges(i+1, puddleB, laser_movie, puddle_height[i], puddle_range[i], puddleC);


    }
    //    printf("numccs = %i\n", numccs);
    
//     for (i = 0; i < numccs; i++) {
//       if (is_puddle[i]) {
// 	set_pixel_rgb_CR_Image(min_x[i], min_y[i], 255, 0, 0, tpuddle_im);
// 
//       }
//     }

//     write_CR_Image("puddlepoints.ppm", tpuddle_im);
//     exit(1);

    CR_emitEPS_closefile(puddleboxfp);
    CR_emitEPS_closefile(lpuddleboxfp);

    //    write_CR_Image("puddle_range.ppm", tpuddle_im);
  }

  vflip_CR_Image(puddle_im, laser_image);
  glDrawPixels(laser_image->w, laser_image->h, GL_RGB, GL_UNSIGNED_BYTE, laser_image->data);

  // project laser to camera

//   navdat = &laser_movie->laserim[laser_movie->currentframe].navdat[0];
//   tilt = laser_movie->laserim[laser_movie->currentframe].frameHeaders[0].tiltAngle;
// 
//   for (j = 0, index = 0; j < D3_LASER_HEIGHT; j++)
//     for (i = 0; i < D3_LASER_WIDTH; i++) {
// 	
//       if (GRAYFP_IMXY(butterfly_thresh_fp_im, i, j)) {
// 	laser2camera(i, j, laser_movie->laserim[laser_movie->currentframe].data[index], &xc, &yc, &xcam, &ycam, &zcam, laser_movie->laserim[laser_movie->currentframe].frameHeaders[0].tiltAngle);
// 
//       }
//       index++;
//     }
// 
//   if (laser_movie->currentframe == 1265) {
//     CR_emitEPS_closefile(mapfp2);
//     exit(1);
//   }

  // advance laser frame
  
  if (laser_movie->currentframe < laser_movie->lastframe)
    get_frame_CR_Movie(laser_movie, laser_movie->currentframe++);
}

//----------------------------------------------------------------------------

void Puddle_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");

  fclose(puddlefp);
  //  fclose(mapfp);
  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Vector *super_rect, *super_texture;
CR_Matrix *super_homography, *super_homography_inv;

double super_w, super_h;
double super_res;

//----------------------------------------------------------------------------

// A, B are 8-vectors listing vertices of rectangles as (x1, x2, x3, x4, y1, y2, y3, y4)'
// H is a 3 x 3 matrix

void compute_homography_CR_Matrix(CR_Vector *A, CR_Vector *B, CR_Matrix *H)
{
  CR_Matrix *S, *Sinv;
  CR_Vector *HV;

  S = init_CR_Matrix("S", 8, 8,
		     A->x[0], A->x[4], 1.0, 0.0, 0.0, 0.0, -A->x[0] * B->x[0], -A->x[4] * B->x[0],
		     A->x[1], A->x[5], 1.0, 0.0, 0.0, 0.0, -A->x[1] * B->x[1], -A->x[5] * B->x[1],
		     A->x[2], A->x[6], 1.0, 0.0, 0.0, 0.0, -A->x[2] * B->x[2], -A->x[6] * B->x[2],
		     A->x[3], A->x[7], 1.0, 0.0, 0.0, 0.0, -A->x[3] * B->x[3], -A->x[7] * B->x[3],
		     0.0, 0.0, 0.0, A->x[0], A->x[4], 1.0, -A->x[0] * B->x[4], -A->x[4] * B->x[4],
		     0.0, 0.0, 0.0, A->x[1], A->x[5], 1.0, -A->x[1] * B->x[5], -A->x[5] * B->x[5],
		     0.0, 0.0, 0.0, A->x[2], A->x[6], 1.0, -A->x[2] * B->x[6], -A->x[6] * B->x[6],
		     0.0, 0.0, 0.0, A->x[3], A->x[7], 1.0, -A->x[3] * B->x[7], -A->x[7] * B->x[7]);

//   print_CR_Vector(A);
//   print_CR_Vector(B);
//   print_CR_Matrix(S);

  //  Sinv = svd_inverse_CR_Matrix(S);
  Sinv = inverse_CR_Matrix(S);

  //  print_CR_Matrix(Sinv);

  //  print_CR_Matrix(product_CR_Matrix(S, Sinv));

  HV = make_CR_Vector("HV", 8);

  product_sum_CR_Matrix(1.0, Sinv, B, 0.0, NULL, HV);

  set_CR_Matrix(H, 
		HV->x[0], HV->x[1], HV->x[2],
		HV->x[3], HV->x[4], HV->x[5],
		HV->x[6], HV->x[7], 1.0);

  free_CR_Vector(HV);
  free_CR_Matrix(Sinv);
  free_CR_Matrix(S);
}

//----------------------------------------------------------------------------

// assumes V1, V2 are 2-D points in homogeneous coordinates (x1, y1, 1), (x2, y2, 1)

void transform_homography_CR_Matrix(CR_Matrix *H, CR_Vector *V1, CR_Vector *V2)
{
  product_sum_CR_Matrix(1.0, H, V1, 0.0, NULL, V2);
  V2->x[0] /= V2->x[2];
  V2->x[1] /= V2->x[2];
  V2->x[2] /= V2->x[2];
}

//----------------------------------------------------------------------------

// try to determine the corners of a highway sign starting from a point assumed
// to be inside it

// corner coordinates are listed in (x1, x2, x3, x4, y1, y2, y3, y4)' format,
// where the corners are numbered as shown below:
//
//         1 ___________ 2
//          |           |
//          |           |
//          |___________|
//         4             3

void find_sign_corners(CR_Vector *old_corners, CR_Vector *new_corners, CR_Image *im)
{
  // note: assuming a fairly frontal view, the area of the quad defined by the old corners
  // should give us some idea of the correct scale to use
  // idea 1: look for strongest grayscale corner in neighborhood of each old corner
  // idea 2: look for "color edges."  complicated by fact that some signs have white borders
  // around them
  // idea 3: segment whole sign by color, look for corners of its mask

}

//----------------------------------------------------------------------------

void compute_motion_templates(CR_Image *im)
{
  int x, y, dx, dy;
  CR_Image *grad_x, *grad_y;
  CR_Vector *dxhist, *dyhist;

  // allocate images, etc.

  // compute gradient (of intensity image)

  grad_x = make_CR_Image(im->w, im->h, 0);
  grad_y = make_CR_Image(im->w, im->h, 0);

  dxhist = make_all_CR_Vector("dxhist", 512, 0);
  dyhist = make_all_CR_Vector("dyhist", 512, 0);

  for (y = 0; y < im->h - 1; y++)
    for (x = 0; x < im->w - 1; x++) {

      // horizontal

      IMXY_R(grad_x, x, y) = (unsigned char) 0;
      IMXY_G(grad_x, x, y) = (unsigned char) 0;
      IMXY_B(grad_x, x, y) = (unsigned char) 0;

      dx = IMXY_I(im, x + 1, y) - IMXY_I(im, x, y);

      if (dx < 0)
	IMXY_R(grad_x, x, y) = (unsigned char) -dx;
      else if (dx > 0)
	IMXY_G(grad_x, x, y) = (unsigned char) dx;

      // vertical

      IMXY_R(grad_y, x, y) = (unsigned char) 0;
      IMXY_G(grad_y, x, y) = (unsigned char) 0;
      IMXY_B(grad_y, x, y) = (unsigned char) 0;

      dy = IMXY_I(im, x, y + 1) - IMXY_I(im, x, y);

      if (dy < 0)
	IMXY_R(grad_y, x, y) = (unsigned char) -dy;
      else if (dy > 0)
	IMXY_G(grad_y, x, y) = (unsigned char) dy;

//       dxhist->x[256 + dx]++;
//       dyhist->x[256 + dy]++;
    }

//   scale_CR_Vector(1.0 / (720.0 * 480.0), dxhist);
//   scale_CR_Vector(1.0 / (720.0 * 480.0), dyhist);

//   print_CR_Vector(dxhist);
//   print_CR_Vector(dyhist);

  write_CR_Image("grad_x.ppm", grad_x);
  write_CR_Image("grad_y.ppm", grad_y);

  exit(1);

  // compute motion field

  // compose, and create motion template images

}

//----------------------------------------------------------------------------

#define AV_STATE_POLY   1
#define AV_STATE_CURVE  2
#define AV_STATE_NONE   3

#define AV_MAX_POINTS   100   // maximum number of points per object
#define AV_MAX_OBJECTS  32    // maximum number of objects per frame

typedef struct avobject
{
  int saved;        // has this object really been entered or is it still just a placeholder?
  int shape;        // poly or curve
  char *type;       // "Vehicle", "Pedestrian", "Lane line", etc.

  int num_points;
  double *point_x, *point_y;

} AV_Object;

int av_flag = FALSE;
int av_w, av_h;

extern GLUI *glui;
GLUI_Spinner *segment_spinner;
GLUI_Listbox *av_type;
GLUI_Button *jump_button, *play_button, *back_button, *forward_button, *start_button, *change_button, *poly_button, *curve_button, *stop_button;
GLUI_EditText *skip_edittext, *jump_edittext, *object_edittext;

char *av_filename, *av_notename;
FILE *av_note_read_fp, *av_note_write_fp;
CR_Movie *av_movie;
CR_Window *av_window;
CR_Image *av_scratch_im;
int av_frameskip = 1;
static char framestring[16];
int av_jumpframe;
int av_state = AV_STATE_NONE;

int av_overlay_num_points = 0;
double *av_overlay_point_x, *av_overlay_point_y;

AV_Object **av_obj;
int av_obj_num = 0;  // overall object counter for this movie
int av_highest_obj_num = 0;

int av_playing = FALSE;

#define AV_NUM_OBJECT_TYPES  12

char obj_types[AV_NUM_OBJECT_TYPES][64] = { 
    "Lane_line",
    "Road_edge",
    "Other_marking",
    "Traffic_light",
    "Road_sign",
    "Pole/lamppost",
    "Barrel/obstacle",
    "Vehicle",
    "Pedestrian",
    "Bush/tree",
    "Gate/tollbooth",
    "Shadow/highlight" 
};

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Image *AnnotateVideo_init_CR_CG(char *name)
{
  CR_Image *im;

  av_w = 200;
  av_h = 50;

  //  im = make_CR_Image(av_w, av_h, 0);
  im = make_CR_Image(1, 1, 0);

  return im;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void av_draw_object_shape(AV_Object *obj)
{
  int i;

  if (obj->shape == AV_STATE_CURVE) {
    //    printf("drawing curve with %i points\n", obj->num_points);
    //    for (i = 0; i < obj->num_points; i++) 
    //      printf("%i: x = %lf, y = %lf\n", i, obj->point_x[i], obj->point_y[i]);
    ogl_draw_thick_line_strip_d(obj->point_x, obj->point_y, obj->num_points, 3, 0, 0, 0);
    ogl_draw_thick_line_strip_d(obj->point_x, obj->point_y, obj->num_points, 1, 255, 255, 0);
    ogl_draw_points_d(obj->point_x, obj->point_y, 5, obj->num_points, 0, 0, 0);
    ogl_draw_points_d(obj->point_x, obj->point_y, 3, obj->num_points, 0, 255, 0);
  }
  else if (obj->shape == AV_STATE_POLY) {
    //    printf("drawing poly\n");
    ogl_draw_thick_line_loop_d(obj->point_x, obj->point_y, obj->num_points, 3, 0, 0, 0); 
    ogl_draw_thick_line_loop_d(obj->point_x, obj->point_y, obj->num_points, 1, 255, 255, 0); 
    ogl_draw_points_d(obj->point_x, obj->point_y, 5, obj->num_points, 0, 0, 0);
    ogl_draw_points_d(obj->point_x, obj->point_y, 3, obj->num_points, 255, 0, 0);
  } 

}

//----------------------------------------------------------------------------

void av_draw_objects_in_frame(int frame_num)
{
  int i, j, x, y;

  //  printf("draw objs in frame %i\n", frame_num);

  for (i = 0; i < AV_MAX_OBJECTS; i++) {
    if (av_obj[frame_num][i].saved) {

      //      printf("drawing object %i\n", i);

      x = av_obj[frame_num][i].point_x[0];
      y = av_obj[frame_num][i].point_y[0];

      sprintf(framestring, "%i: %s", i, av_obj[frame_num][i].type);
      if (i < 10) {
	//	ogl_draw_thick_line(x - 22, y + 16, x - 10, y + 16, 18, 0, 0, 0);
	ogl_draw_string2(framestring, x - 15, y + 5, 255, 255, 0); 
      }
      else {
	//	ogl_draw_thick_line(x - 32, y + 16, x - 10, y + 16, 18, 0, 0, 0);
	ogl_draw_string2(framestring, x - 25, y + 5, 255, 255, 0); 
      }

      av_draw_object_shape(&av_obj[frame_num][i]);

    }
  }
}

//----------------------------------------------------------------------------

void av_display()
{
  float rp[3];

  glutSetWindow(av_window->glut_id);

//   glGetFloatv(GL_CURRENT_RASTER_POSITION, rp);
//   printf("%f, %f, %f\n", rp[0], rp[1], rp[2]);

//   glPixelZoom(1, 1);
//   glRasterPos2i(0, 0);

//   glClearColor(0, 0, 0, 0);
//   glClear(GL_COLOR_BUFFER_BIT);

  // draw image

  vflip_CR_Image(av_movie->im, av_scratch_im);
  glDrawPixels(av_movie->width, av_movie->height, GL_RGB, GL_UNSIGNED_BYTE, av_scratch_im->data);

  // draw overlays

  // draw saved objects in this frame

  if (av_obj[av_movie->currentframe] != NULL) 
    av_draw_objects_in_frame(av_movie->currentframe);

  //  printf("av_state = %i\n", av_state);

  // draw object we are currently specifying

  if (av_state == AV_STATE_CURVE) {
    ogl_draw_thick_line_strip_d(av_overlay_point_x, av_overlay_point_y, av_overlay_num_points, 3, 0, 0, 0);
    ogl_draw_thick_line_strip_d(av_overlay_point_x, av_overlay_point_y, av_overlay_num_points, 1, 255, 255, 0);
    ogl_draw_points_d(av_overlay_point_x, av_overlay_point_y, 5, av_overlay_num_points, 0, 0, 0);
    ogl_draw_points_d(av_overlay_point_x, av_overlay_point_y, 3, av_overlay_num_points, 0, 255, 0);
  }
  else if (av_state == AV_STATE_POLY) {
    ogl_draw_thick_line_loop_d(av_overlay_point_x, av_overlay_point_y, av_overlay_num_points, 3, 0, 0, 0); 
    ogl_draw_thick_line_loop_d(av_overlay_point_x, av_overlay_point_y, av_overlay_num_points, 1, 255, 255, 0); 
    ogl_draw_points_d(av_overlay_point_x, av_overlay_point_y, 5, av_overlay_num_points, 0, 0, 0);
    ogl_draw_points_d(av_overlay_point_x, av_overlay_point_y, 3, av_overlay_num_points, 255, 0, 0);
  } 

  //  if (av_state == AV_STATE_CURVE || av_state == AV_STATE_POLY) {
  ogl_dummy();
  //  sprintf(framestring, "%i", av_movie->currentframe);
  //   printf("%s\n", framestring);
  //  ogl_draw_string2(framestring, 10, 10, 255, 255, 0); 
    //  }

  // make back buffer visible

  glutSwapBuffers();
}

//----------------------------------------------------------------------------

void av_reshape(int w, int h)
{
}

//----------------------------------------------------------------------------

// x, y are in top-left coordinate system, but OpenGL uses a bottom-left 
// system to draw

// this means a mouse button is pressed --- which one we don't know right now

void av_motion(int x, int y)
{
}

//----------------------------------------------------------------------------

// x, y are in top-left coordinate system, but OpenGL uses a bottom-left 
// system to draw

void av_mouse(int button, int state, int x, int y)
{
  /*
  if (av_state == AV_STATE_POLY)
    printf("poly ");
  else if (av_state == AV_STATE_CURVE) 
    printf("curve ");
  else
    printf("none ");

  if (state == GLUT_DOWN && button == GLUT_LEFT_BUTTON)
    printf("left mouse down at x = %i, y = %i\n", x, y);
  else if (state == GLUT_DOWN && button == GLUT_RIGHT_BUTTON)
    printf("right mouse down at x = %i, y = %i\n", x, y);
  else if (state == GLUT_UP && button == GLUT_LEFT_BUTTON)
    printf("left mouse up at x = %i, y = %i\n", x, y);
  else if (state == GLUT_UP && button == GLUT_RIGHT_BUTTON)
    printf("right mouse up at x = %i, y = %i\n", x, y);
  */

  if (state == GLUT_DOWN && button == GLUT_LEFT_BUTTON) {

    if (av_state == AV_STATE_POLY || av_state == AV_STATE_CURVE) {

      av_overlay_point_x[av_overlay_num_points] = x;
      av_overlay_point_y[av_overlay_num_points] = av_window->h - y - 1;
      av_overlay_num_points++;
      
      refresh_CR_Window(av_window);
    }
  }
}

//----------------------------------------------------------------------------

// x, y are in top-left coordinate system, but OpenGL uses a bottom-left 
// system to draw

void av_keyboard(unsigned char key, int x, int y)
{
}

//----------------------------------------------------------------------------

void av_special(int key, int x, int y)
{
}

//----------------------------------------------------------------------------

void av_visible(int vis)
{
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

AV_Object **av_read_all(FILE *);
void av_plus();
void av_minus();

void av_load()
{
  int i;

  av_filename = choose_open_file_dialog_win32("Movies (*.mpg; *.avi; *.mov; fw*; *.rif)\0*.mpg;*.m1v;*.avi;*.mov;fw*;*.rif\0");
  printf("%s\n", av_filename);

  av_notename = (char *) CR_calloc(strlen(av_filename), sizeof(char));
  strcpy(av_notename, av_filename);
  sprintf(&(av_notename[strlen(av_notename) - 3]), "not");
  av_note_read_fp = fopen(av_notename, "r");

  av_movie = open_CR_Movie(av_filename);
  //  av_movie = read_avi_to_CR_Movie(av_filename);
  av_scratch_im = make_CR_Image(av_movie->width, av_movie->height, 0);
  printf("w = %i, h = %i\n", av_movie->width, av_movie->height);
  av_window = open_CR_Window(av_filename, av_movie->width, av_movie->height);
  jump_edittext->set_int_limits(0, av_movie->lastframe - 1);

  av_overlay_num_points = 0;
  av_overlay_point_x = (double *) CR_calloc(AV_MAX_POINTS, sizeof(double));
  av_overlay_point_y = (double *) CR_calloc(AV_MAX_POINTS, sizeof(double));

  if (!av_note_read_fp) {
    av_obj = (AV_Object **) CR_calloc(av_movie->numframes, sizeof(AV_Object *));
    for (i = 0; i < av_movie->numframes; i++)
      av_obj[i] = NULL;
  }
  else 
    av_obj = av_read_all(av_note_read_fp);

  set_visibility_callback_CR_Window(av_window, av_visible);
  set_display_callback_CR_Window(av_window, av_display);
  set_reshape_callback_CR_Window(av_window, av_reshape);
  set_mouse_callback_CR_Window(av_window, av_mouse);
  set_keyboard_callback_CR_Window(av_window, av_keyboard);
  set_motion_callback_CR_Window(av_window, av_motion);
  set_special_callback_CR_Window(av_window, av_special);

  refresh_CR_Window(av_window);
}

//----------------------------------------------------------------------------

// when movie is loaded, an array with one entry per frame is created.  
// initially, each entry is the NULL pointer, but whenever an object is
// created in that frame, an array of object structs is allocated and hung
// off of that entry

void av_save_object_in_frame(int obj_num, int frame_num)
{
  int i;

  //  printf("SAVING OBJECT IN FRAME!!!!\n");

  if (obj_num >= AV_MAX_OBJECTS)
    CR_error("too many objects in this frame");

  if (av_obj[frame_num] == NULL) {
    av_obj[frame_num] = (AV_Object *) CR_calloc(AV_MAX_OBJECTS, sizeof(AV_Object));
    for (i = 0; i < AV_MAX_OBJECTS; i++) 
      av_obj[frame_num][i].saved = FALSE;
  }

  av_obj[frame_num][obj_num].saved = TRUE;
  av_obj[frame_num][obj_num].shape = av_state;

  av_obj[frame_num][obj_num].type = (char *) CR_calloc(strlen(obj_types[av_type->get_int_val()]), sizeof(char));
  strcpy(av_obj[frame_num][obj_num].type, obj_types[av_type->get_int_val()]);

  av_obj[frame_num][obj_num].num_points = av_overlay_num_points;
  av_obj[frame_num][obj_num].point_x = (double *) CR_calloc(av_overlay_num_points, sizeof(double));
  av_obj[frame_num][obj_num].point_y = (double *) CR_calloc(av_overlay_num_points, sizeof(double));
  for (i = 0; i < av_overlay_num_points; i++) {
    av_obj[frame_num][obj_num].point_x[i] = av_overlay_point_x[i];
    av_obj[frame_num][obj_num].point_y[i] = av_overlay_point_y[i];
  }

  av_overlay_num_points = 0;
}

//----------------------------------------------------------------------------

// are we in an object-entering state and has enough information been entered for this frame?

int av_good_obj(int state, int points)
{
  return (state != AV_STATE_NONE && (state != AV_STATE_CURVE || points >= 2) && (state != AV_STATE_POLY || points >= 3));
}

//----------------------------------------------------------------------------

char *av_scanline;

int av_is_another_obj(FILE *fp)
{
  int pos, result;
  char *scanline;

  // remember where we are

  pos = ftell(fp);

  // peek at what's ahead

  av_scanline = fgets(av_scanline, 80, fp);
  //  CR_flush_printf("obj %s\n", av_scanline);
  result = !strncmp(av_scanline, "<Start object", 13);

  // go back to where we were and return result

  fseek(fp, pos, SEEK_SET);

  return result;
}

//----------------------------------------------------------------------------

int av_is_another_frame(FILE *fp)
{
  int pos, result;
  char *scanline;

  // remember where we are

  pos = ftell(fp);

  // peek at what's ahead

  av_scanline = fgets(av_scanline, 80, fp);
  //  CR_flush_printf("frame %s\n", av_scanline);
  if (av_scanline)
    result = !strncmp(av_scanline, "<Start frame", 12);
  else
    result = FALSE;

  // go back to where we were and return result

  fseek(fp, pos, SEEK_SET);

  return result;
}

//----------------------------------------------------------------------------

void av_read_obj(FILE *fp, AV_Object *obj)
{
  int dummy, obj_num, i;
  char *shape;

  obj->saved = TRUE;

  shape = (char *) CR_calloc(256, sizeof(char));
  obj->type = (char *) CR_calloc(256, sizeof(char));
  
  fscanf(fp, "\t<Start object %i>\n", &obj_num);

  fscanf(fp, "\t\t<Shape = %s>\n", shape);
  shape[strlen(shape) - 1] = '\0';
  //  printf("my shape = %s\n", shape);
  if (!strcmp(shape, "Polygon")) 
    obj->shape = AV_STATE_POLY;
  else 
    obj->shape = AV_STATE_CURVE;

  fscanf(fp, "\t\t< Type = \"%s\">\n", obj->type);
  obj->type[strlen(obj->type) - 2] = '\0';
  //  CR_flush_printf("obj type = %s\n", obj->type);

  fscanf(fp, "\t\t<Numpoints = %i>\n", &obj->num_points);

  obj->point_x = (double *) CR_calloc(obj->num_points, sizeof(double));
  obj->point_y = (double *) CR_calloc(obj->num_points, sizeof(double));

  //  printf("read num points = %i\n", obj->num_points);

  for (i = 0; i < obj->num_points; i++) {
    fscanf(fp, "\t\t\t<Point %i: x = %lf, y = %lf>\n", &dummy, &(obj->point_x[i]), &(obj->point_y[i]));
    //    printf("read x = %lf, y = %lf\n", obj->point_x[i], obj->point_y[i]);
  }

  fscanf(fp, "\t<End object %i>\n", &obj_num);
}

//----------------------------------------------------------------------------

void av_read_frame(FILE *fp, AV_Object **obj_array)
{
  int i, frame_num;

  // figure out frame number

  fscanf(fp, "<Start frame %i>\n", &frame_num);

  // allocate object array
  
  obj_array[frame_num] = (AV_Object *) CR_calloc(AV_MAX_OBJECTS, sizeof(AV_Object));
  for (i = 0; i < AV_MAX_OBJECTS; i++) 
    obj_array[frame_num][i].saved = FALSE;

  // read objects

  for (i = 0 ; av_is_another_obj(fp); av_read_obj(fp, &obj_array[frame_num][i++])) {
    if (i > av_highest_obj_num)
      av_highest_obj_num = i;
    //    printf("about to read object %i\n", i);
  }

  // finish

  fscanf(fp, "<End frame %i>\n", &frame_num);
}

//----------------------------------------------------------------------------

AV_Object **av_read_all(FILE *fp)
{
  AV_Object **obj_array;
  int i, numframes, frame_num;

  av_scanline = (char *) CR_calloc(80, sizeof(char));

  fscanf(fp, "<Numframes = %i>\n", &numframes);

  obj_array = (AV_Object **) CR_calloc(numframes, sizeof(AV_Object *));
  for (i = 0; i < numframes; i++) {
    obj_array[i] = NULL;
  }

  for ( ; av_is_another_frame(fp); av_read_frame(fp, obj_array));

  //  printf("highest = %i\n", av_highest_obj_num);
  av_obj_num = av_highest_obj_num + 1;
  object_edittext->set_int_val(av_obj_num);

  return obj_array;
}

//----------------------------------------------------------------------------

// write: object number, object shape, object type name, number of points, point coordinates

void av_write_obj(FILE *fp, int obj_num, AV_Object *obj)
{
  int i;

  fprintf(fp, "\t<Start object %i>\n", obj_num);

  if (obj->shape == AV_STATE_POLY)
    fprintf(fp, "\t\t<Shape = Polygon>\n");
  else if (obj->shape == AV_STATE_CURVE)
    fprintf(fp, "\t\t<Shape = Curve>\n");
  else
    fprintf(fp, "\t\t<Shape = Unknown>\n");

  fprintf(fp, "\t\t<Type = \"%s\">\n", obj->type);

  fprintf(fp, "\t\t<Numpoints = %i>\n", obj->num_points);

  for (i = 0; i < obj->num_points; i++)
    fprintf(fp, "\t\t\t<Point %i: x = %lf, y = %lf>\n", i, obj->point_x[i], obj->point_y[i]);

  fprintf(fp, "\t<End object %i>\n", obj_num);
}

//----------------------------------------------------------------------------

void av_write_all(FILE *fp)
{
  int frame_num, i;
  fprintf(fp, "<Numframes = %i>\n", av_movie->numframes);

  // iterate through movie frames, only write if we have annotations for that frame

  for (frame_num = 0; frame_num < av_movie->numframes; frame_num++) 
    if (av_obj[frame_num]) {
      
      fprintf(fp, "<Start frame %i>\n", frame_num);
      
      // for each non-NULL frame, only write those objects which appear in this frame
      
      for (i = 0; i < AV_MAX_OBJECTS; i++)
	if (av_obj[frame_num][i].saved) 
	  av_write_obj(fp, i, &av_obj[frame_num][i]);
      
      fprintf(fp, "<End frame %i>\n", frame_num);
    }
}

//----------------------------------------------------------------------------

void av_print()
{
  av_write_all(stdout);
}

//----------------------------------------------------------------------------

// filename is automatically derived from name of movie we're annotating
// add ".not" suffix
// we will overwrite any previous version as if we are writing for the first time
 
// if in the middle of adding an object (i.e., neither "End object" nor "Start <another object>"
// has been pressed), everything including the current object will be saved anyway
// provided that the current object has had enough points specified for its type to be legal

void av_save()
{
  FILE *fp;

  if ((av_state == AV_STATE_NONE) || 
      (av_state == AV_STATE_CURVE && av_overlay_num_points >= 2) || 
      (av_state == AV_STATE_POLY && av_overlay_num_points >= 3)) {

    printf("Saving %s\n", av_notename);
  
    if (av_state != AV_STATE_NONE)
      av_save_object_in_frame(av_obj_num, av_movie->currentframe);

    // open file for writing

    av_note_write_fp = fopen(av_notename, "w");

    // write it

    av_write_all(av_note_write_fp);

    // close file

    fclose(av_note_write_fp);
  }
  else
    printf("Can not save now\n");
}

//----------------------------------------------------------------------------

void av_goto_end()
{
  if (av_movie->currentframe != av_movie->lastframe - 1) {
    if (av_good_obj(av_state, av_overlay_num_points)) 
      av_save_object_in_frame(av_obj_num, av_movie->currentframe);
    av_movie->currentframe = av_movie->lastframe - 1;
    jump_edittext->set_int_val(av_movie->currentframe);
    get_frame_CR_Movie(av_movie, av_movie->currentframe);
    refresh_CR_Window(av_window);
  }
}

//----------------------------------------------------------------------------

void av_goto_start()
{
  if (av_movie->currentframe != 0) {
    if (av_good_obj(av_state, av_overlay_num_points)) 
      av_save_object_in_frame(av_obj_num, av_movie->currentframe);
    av_movie->currentframe = 0;
    jump_edittext->set_int_val(av_movie->currentframe);
    get_frame_CR_Movie(av_movie, av_movie->currentframe);
    refresh_CR_Window(av_window);
  }
}

//----------------------------------------------------------------------------

void av_jump()
{
  av_jumpframe = jump_edittext->get_int_val();

  if (av_movie->currentframe != av_jumpframe) {
    if (av_good_obj(av_state, av_overlay_num_points)) 
      av_save_object_in_frame(av_obj_num, av_movie->currentframe);
    av_movie->currentframe = av_jumpframe;
    get_frame_CR_Movie(av_movie, av_movie->currentframe);
    refresh_CR_Window(av_window);
  }
}

//----------------------------------------------------------------------------

void av_pauseplay();

void av_plus()
{
  av_frameskip = skip_edittext->get_int_val();

  if (av_movie->currentframe + av_frameskip < av_movie->lastframe) {
    if (av_good_obj(av_state, av_overlay_num_points)) 
      av_save_object_in_frame(av_obj_num, av_movie->currentframe);
    av_movie->currentframe = av_movie->currentframe + av_frameskip;
    jump_edittext->set_int_val(av_movie->currentframe);
    get_frame_CR_Movie(av_movie, av_movie->currentframe);
    refresh_CR_Window(av_window);
  }
  else if (av_playing)
    av_pauseplay();
}

//----------------------------------------------------------------------------

void av_minus()
{
  av_frameskip = skip_edittext->get_int_val();

  if (av_movie->currentframe - av_frameskip >= 0) {
    if (av_good_obj(av_state, av_overlay_num_points)) 
      av_save_object_in_frame(av_obj_num, av_movie->currentframe);
    av_movie->currentframe = av_movie->currentframe - av_frameskip;
    jump_edittext->set_int_val(av_movie->currentframe);
    get_frame_CR_Movie(av_movie, av_movie->currentframe);
    refresh_CR_Window(av_window);
  }
}

//----------------------------------------------------------------------------

void av_state_change(int new_state)
{
  if (av_good_obj(av_state, av_overlay_num_points)) {
    av_save_object_in_frame(av_obj_num, av_movie->currentframe);
    av_obj_num++;
    object_edittext->set_int_val(av_obj_num);
  }
  else {
    if (av_obj[av_movie->currentframe])
      av_obj[av_movie->currentframe][av_obj_num].saved = FALSE;
  }

  av_state = new_state;

  refresh_CR_Window(av_window);
}

//----------------------------------------------------------------------------

void av_change_object()
{
  av_obj_num = object_edittext->get_int_val();
}

//----------------------------------------------------------------------------

void av_pauseplay()
{
  if (av_playing) {
    av_playing = FALSE;
    //    printf("Stopped\n");
  }
  else {
    av_playing = TRUE;
    //    printf("Playing\n");
  }
}

//----------------------------------------------------------------------------

// meaningless if an object hasn't been started already or if there aren't enough
// points entered in this frame

void av_end_object()
{
  if (av_good_obj(av_state, av_overlay_num_points)) {
    av_save_object_in_frame(av_obj_num, av_movie->currentframe);
    av_obj_num++;
    object_edittext->set_int_val(av_obj_num);
    av_state = AV_STATE_NONE;

    refresh_CR_Window(av_window);
  }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void AnnotateVideo_grab_CR_CG(CR_Image *im)
{
  int i, glut_id;
  int wireframe, segments;

  if (!av_flag) {
    glut_id = glutGetWindow();

    wireframe = 0;
    segments = 8;

    glui = GLUI_Master.create_glui("GLUI");
    
    glui->add_button("Load movie", 0, (GLUI_Update_CB) av_load);
    glui->add_button("Save annotations", 0, (GLUI_Update_CB) av_save);
    glui->add_separator();
    av_type = glui->add_listbox("Obj. type");
    for (i = 0; i < AV_NUM_OBJECT_TYPES; i++)
      av_type->add_item(i, obj_types[i]);
    /*
    av_type->add_item(1, "Lane line");
    av_type->add_item(2, "Road edge");
    av_type->add_item(3, "Other marking");
    av_type->add_item(4, "Traffic light");
    av_type->add_item(5, "Road sign");
    av_type->add_item(6, "Pole/lamppost");
    av_type->add_item(7, "Barrel/obstacle");
    av_type->add_item(8, "Vehicle");
    av_type->add_item(9, "Pedestrian");
    av_type->add_item(10, "Bush/tree");
    av_type->add_item(11, "Gate/tollbooth");
    av_type->add_item(12, "Shadow/highlight");
    */
    av_type->set_alignment(GLUI_ALIGN_LEFT);
    glui->add_column(true);
    skip_edittext = glui->add_edittext("Frame skip", GLUI_EDITTEXT_INT);
    skip_edittext->set_w(60);
    skip_edittext->set_int_val(av_frameskip);
    skip_edittext->set_alignment(GLUI_ALIGN_LEFT);
    jump_edittext = glui->add_edittext("Frame", GLUI_EDITTEXT_INT);
    jump_edittext->set_int_val(0);
    jump_edittext->set_w(60);
    jump_edittext->set_alignment(GLUI_ALIGN_LEFT);
    glui->add_separator();
    object_edittext = glui->add_edittext("Object", GLUI_EDITTEXT_INT);
    object_edittext->set_w(60);
    object_edittext->set_int_val(av_obj_num);
    object_edittext->set_alignment(GLUI_ALIGN_LEFT);

    //    object_name->set_alignment(GLUI_ALIGN_CENTER);
    glui->add_column(true);
    play_button = glui->add_button("Pause/Play", 0, (GLUI_Update_CB) av_pauseplay);
    play_button->set_w(40);
    //    back_button = glui->add_button("-");
    jump_button = glui->add_button("Goto frame", 0, (GLUI_Update_CB) av_jump);
    jump_button->set_w(40);
    glui->add_separator();
    change_button = glui->add_button("Change object", 0, (GLUI_Update_CB) av_change_object);
    change_button->set_w(40);
    glui->add_column(true);
    forward_button = glui->add_button("+ frame", 0, (GLUI_Update_CB) av_plus);
    forward_button->set_w(40);
    //    forward_button = glui->add_button("+");
    back_button = glui->add_button("- frame", 0, (GLUI_Update_CB) av_minus);
    back_button->set_w(40);
    glui->add_separator();
    start_button = glui->add_button("Rewind", 0, (GLUI_Update_CB) av_goto_start);
    start_button->set_w(40);
    glui->add_column(true);
    poly_button = glui->add_button("Start Poly", AV_STATE_POLY, (GLUI_Update_CB) av_state_change);
    poly_button->set_w(60);
    curve_button = glui->add_button("Start Curve", AV_STATE_CURVE, (GLUI_Update_CB) av_state_change);
    curve_button->set_w(60);
    glui->add_separator();
    stop_button = glui->add_button("End Object", 0, (GLUI_Update_CB) av_end_object);
    stop_button->set_w(60);
//     glui->add_checkbox( "Wireframe", &wireframe);
//     segment_spinner = glui->add_spinner( "Segments:", GLUI_SPINNER_INT, &segments);
//     segment_spinner->set_int_limits( 3, 60 ); 
   
    glui->set_main_gfx_window(glut_id);

    glutSetWindow(glut_id);
    glutHideWindow();

  // We register the idle callback with GLUI, *not* with GLUT 
    //    glutIdleFunc(NULL);
    GLUI_Master.set_glutIdleFunc(splash_idle); 
    //GLUI_Master.set_glutIdleFunc(NULL); 

    av_flag = TRUE;
  }
}

//----------------------------------------------------------------------------

void AnnotateVideo_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");

  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Matrix *im_thresh, *im_cc;
double *corner_n, *corner_x, *corner_y, *corner_r, *corner_g, *corner_b;
int *corner_id;
CR_Vector *base_rect, *poly_quad, *poly_v1, *poly_v2, *poly_bbox;

int *poly_corners_found;
CR_Vector **poly_corners;
CR_Matrix ***poly_homography;
int poly_i, poly_j;
CR_Matrix *poly_h_left, *poly_h_right;
CR_Matrix *poly_h_left_inv, *poly_h_right_inv;

int poly_n_live;
CR_Live **poly_live;
CR_Image **poly_liveim;
CR_Image *poly_im, *poly_flipim;

// panorama look-up tables

int **poly_left_x, **poly_left_y, **poly_right_x, **poly_right_y;

//----------------------------------------------------------------------------

void CalibratePolyCam_threshold(int thresh, CR_Image *im, CR_Matrix *im_thresh)
{
  int i, j;

  for (j = 0; j < im->h; j++) 
    for (i = 0; i < im->w; i++) {
      if ((unsigned char) IMXY_R(im, i, j) >= thresh ||
	  (unsigned char) IMXY_G(im, i, j) >= thresh ||
	  (unsigned char) IMXY_B(im, i, j) >= thresh)
	MATRC(im_thresh, j, i) = 1;
      else
	MATRC(im_thresh, j, i) = 0;
    }
}

//----------------------------------------------------------------------------

// corner coordinates are listed in (x1, x2, x3, x4, y1, y2, y3, y4)' format,
// where the corners are numbered as shown below:
//
//  (255, 255, 0)   (0, 255, 0)
//      yellow         green
//         1 ___________ 2
//          |           |
//          |           |
//          |___________|
//         4             3
//       green          red
//    (0, 255, 0)   (255, 0, 0)

int corner_map[4];

void CalibratePolyCam_identify_corner_colors(CR_Vector *corners, double *x, double *y, double *r, double *g, double *b)
{
  int i, j, k, l, count, upper_right, mincount, temp;
  double score[24], minscore, ux, uy, vx, vy, tempx, tempy;

  // ye olde maximume likelihoode

  for (i = 0, count = 0; i < 4; i++) 
    for (j = 0; j < 4; j++) 
      if (j != i) 
	for (k = 0; k < 4; k++) 
	  if (k != j && k != i) 
	    for (l = 0; l < 4; l++)
	      if (l != k && l != j && l != i) {

		score[count] = sqrt(SQUARE(r[i] - 255) + SQUARE(g[i] - 255) + SQUARE(b[i] - 0));
		score[count] += sqrt(SQUARE(r[j] - 0) + SQUARE(g[j] - 255) + SQUARE(b[j] - 0));
		score[count] += sqrt(SQUARE(r[k] - 255) + SQUARE(g[k] - 0) + SQUARE(b[k] - 0));
		score[count] += sqrt(SQUARE(r[l] - 0) + SQUARE(g[l] - 255) + SQUARE(b[l] - 0));

		if (count == 0 || score[count] < minscore) {
		  minscore = score[count];
		  mincount = count;
		  corner_map[0] = i;
		  corner_map[1] = j;
		  corner_map[2] = k;
		  corner_map[3] = l;
		}

		//		printf("score %i = %lf (%i %i %i %i)\n", count, score[count], i, j, k, l);

		count++;
	      }

  // see if we need to swap the green corners

  ux = x[corner_map[2]] - x[corner_map[0]];
  uy = y[corner_map[2]] - y[corner_map[0]];
  vx = x[corner_map[1]] - x[corner_map[0]];
  vy = y[corner_map[1]] - y[corner_map[0]];
  //  printf("ux = %lf, uy = %lf, vx = %lf, vy = %lf, z comp of cross = %lf\n", ux, uy, vx, vy, ux * vy - uy * vx);
  if (ux * vy - uy * vx > 0) {
    //    printf("swapping greens!\n");
    temp = corner_map[1];
    corner_map[1] = corner_map[3];
    corner_map[3] = temp;
  }

  //  printf("x1 = %lf, y1 = %lf, x2 = %lf, y2 = %lf, x3 = %lf, y3 = %lf, x4 = %lf, y4 = %lf\n", x[corner_map[0]], y[corner_map[0]], x[corner_map[1]], y[corner_map[1]], x[corner_map[2]], y[corner_map[2]], x[corner_map[3]], y[corner_map[3]]);

  // // corner coordinates are listed in (x1, x2, x3, x4, y1, y2, y3, y4)' format,

  corners->x[0] = x[corner_map[0]];
  corners->x[1] = x[corner_map[1]];
  corners->x[2] = x[corner_map[2]];
  corners->x[3] = x[corner_map[3]];
  corners->x[4] = y[corner_map[0]];
  corners->x[5] = y[corner_map[1]];
  corners->x[6] = y[corner_map[2]];
  corners->x[7] = y[corner_map[3]];
}

//----------------------------------------------------------------------------

// assuming four corner regions here

void CalibratePolyCam_corner_centroids_and_colors(CR_Image *im, CR_Matrix *im_cc, double *n, double *x, double *y, double *r, double *g, double *b)
{
  int i, j, which_cc;
  
  // initialize

  for (i = 0; i < 4; i++) {
    n[i] = 0;
    x[i] = 0;
    y[i] = 0;
    r[i] = 0;
    g[i] = 0;
    b[i] = 0;
  }

  // total

  for (j = 0; j < im_thresh->rows; j++)
    for (i = 0; i < im_thresh->cols; i++)
      if (MATRC(im_cc, j, i)) {
	which_cc = (int) MATRC(im_cc, j, i) - 1;
	n[which_cc] += 1;
	x[which_cc] += i;
	y[which_cc] += j;
	r[which_cc] += (unsigned char) IMXY_R(im, i, j);
	g[which_cc] += (unsigned char) IMXY_G(im, i, j);
	b[which_cc] += (unsigned char) IMXY_B(im, i, j);
      }

  // average

  for (i = 0; i < 4; i++) {
    x[i] /= n[i];
    y[i] /= n[i];
    r[i] /= n[i];
    g[i] /= n[i];
    b[i] /= n[i];
  }
}

//----------------------------------------------------------------------------

// corner coordinates are listed in (x1, x2, x3, x4, y1, y2, y3, y4)' format,
// where the corners are numbered as shown below:
//
//         1 ___________ 2
//          |           |
//          |           |
//          |___________|
//         4             3

// corners is an 8-vector listing vertices of rectangle as (x1, x2, x3, x4, y1, y2, y3, y4)'
// returns TRUE if all four corners found; FALSE otherwise

int imnum = 0;

int CalibratePolyCam_find_corners(CR_Image *im, CR_Vector *corners)
{
  int i, n;

  // threshold, convert to matrix

  CalibratePolyCam_threshold(150, im, im_thresh);

  // connected components

  n = connected_components_CR_Matrix(TRUE, im_thresh, im_cc);
    

  printf("found %i regions\n", n);

  if (n != 4)
    return FALSE;
  else {

    CalibratePolyCam_corner_centroids_and_colors(im, im_cc, corner_n, corner_x, corner_y, corner_r, corner_g, corner_b);
    //    draw_CR_Matrix_CR_Image(1, im_thresh, im, FALSE);
    //    write_CR_Image(num_suffix_plus_extension("imthresh", imnum++, ".ppm"), im);
    //    for (i = 0; i < 4; i++) 
    //      printf("%i: r = %lf, g = %lf, b = %lf\n", i, corner_r[i], corner_g[i], corner_b[i]); 
    CalibratePolyCam_identify_corner_colors(corners, corner_x, corner_y, corner_r, corner_g, corner_b);
    print_CR_Vector(corners);

    return TRUE;
  }
}

//----------------------------------------------------------------------------

void compute_pair_bbox(CR_Vector *rect1, CR_Vector *rect2, CR_Vector *bbox)
{
  double minx, miny, maxx, maxy;
  int i;

  minx = rect1->x[0];
  miny = rect1->x[4];
  maxx = rect1->x[0];
  maxy = rect1->x[4];
  
  for (i = 0; i < 4; i++) {

    if (rect1->x[i] < minx)
      minx = rect1->x[i];
    if (rect2->x[i] < minx)
      minx = rect2->x[i];

    if (rect1->x[i + 4] < miny)
      miny = rect1->x[i + 4];
    if (rect2->x[i + 4] < miny)
      miny = rect2->x[i + 4];

    if (rect1->x[i] > maxx)
      maxx = rect1->x[i];
    if (rect2->x[i] > maxx)
      maxx = rect2->x[i];

    if (rect1->x[i + 4] > maxy)
      maxy = rect1->x[i + 4];
    if (rect2->x[i + 4] > maxy)
      maxy = rect2->x[i + 4];
  }

  bbox->x[0] = minx;
  bbox->x[4] = miny;
  bbox->x[1] = maxx;
  bbox->x[5] = miny;
  bbox->x[2] = maxx;
  bbox->x[6] = maxy;
  bbox->x[3] = minx;
  bbox->x[7] = maxy;
}

//----------------------------------------------------------------------------

void transform_homography_rect(CR_Matrix *homography, CR_Vector *src_rect, CR_Vector *dst_rect)
{
  int i;

  for (i = 0; i < 4; i++) {
    poly_v1->x[0] = src_rect->x[i];
    poly_v1->x[1] = src_rect->x[i + 4];
    transform_homography_CR_Matrix(homography, poly_v1, poly_v2);
    dst_rect->x[i] = poly_v2->x[0];
    dst_rect->x[i + 4] = poly_v2->x[1];
  }
}

//----------------------------------------------------------------------------

void poly_compute_bbox(CR_Vector *bbox, CR_Matrix *h1, CR_Matrix *h2)
{
  // figure out a generous bounding box for mosaic so we can open a window

  print_CR_Vector(base_rect);

  print_CR_Matrix(h1);
  print_CR_Matrix(h2);

  transform_homography_rect(h1, base_rect, poly_quad);
  print_CR_Vector(poly_quad);
  compute_pair_bbox(base_rect, poly_quad, bbox);

  print_CR_Vector(bbox);

  transform_homography_rect(h2, base_rect, poly_quad);
  print_CR_Vector(poly_quad);
  compute_pair_bbox(bbox, poly_quad, bbox);
	  
  print_CR_Vector(bbox);

  //  exit(1);
}

//----------------------------------------------------------------------------

void poly_compute_homography(int n_live, int *corners_found, CR_Vector **corners, CR_Matrix ***homography)
{
  int i, j;

  for (i = 0; i < n_live; i++) 
    if (corners_found[i])
      for (j = i + 1; j < n_live; j++) 
	if (corners_found[j]) {
	  
	  // should undistort the corners using each camera's internal calibration first
	  compute_homography_CR_Matrix(corners[i], corners[j], homography[i][j]);  // homography will get averaged in loop
	  printf("%i -> %i\n", i, j);
	  print_CR_Matrix(homography[i][j]);
	  
	  write_dlm_CR_Matrix(num_suffix_plus_extension("undistorted_homography_", 10 * i + j, ".mat"), homography[i][j]);

	  // jump out of loop
	  
	  return;

	  // to put in grab: 
	  // undistort both images (should be implemented as a lookup table)
	  // transform 1st image with homography (should be implemented as a lookup table)
	  // cross-fade images within overlap and display mosaic 
	  
	  // assumes V1, V2 are 2-D points in homogeneous coordinates (x1, y1, 1), (x2, y2, 1)
	  
	  /*
	    eps_h = 1000;
	    fp = CR_emitEPS_setupfile("polycam.ps", -500, -eps_h / 2, 500, eps_h / 2);
	    
	    v->x[0] = 0;
	    v->x[1] = 0;
	    transform_homography_CR_Matrix(homography[i][j], v, v1);
	    printf("(0, 0)\n");
	    print_CR_Vector(v1);
	    
	    v->x[0] = 319;
	    v->x[1] = 0;
	    transform_homography_CR_Matrix(homography[i][j], v, v2);
	    printf("(319, 0)\n");
	    print_CR_Vector(v2);
	    
	    v->x[0] = 319;
	    v->x[1] = 239;
	    transform_homography_CR_Matrix(homography[i][j], v, v3);
	    printf("(319, 239)\n");
	    print_CR_Vector(v3);

	    v->x[0] = 0;
	    v->x[1] = 239;
	    transform_homography_CR_Matrix(homography[i][j], v, v4);
	    printf("(0, 239)\n");
	    print_CR_Vector(v4);

	    CR_emitEPS_draw_line(fp, v1->x[0], -v1->x[1], v2->x[0], -v2->x[1], 1, 0, 255, 0);
	    CR_emitEPS_draw_line(fp, v2->x[0], -v2->x[1], v3->x[0], -v3->x[1], 1, 0, 255, 0);
	    CR_emitEPS_draw_line(fp, v3->x[0], -v3->x[1], v4->x[0], -v4->x[1], 1, 0, 255, 0);
	    CR_emitEPS_draw_line(fp, v4->x[0], -v4->x[1], v1->x[0], -v1->x[1], 1, 0, 255, 0);



	    CR_emitEPS_draw_line(fp, 0, -0, 319, -0, 1, 255, 0, 0);
	    CR_emitEPS_draw_line(fp, 319, -0, 319, -239, 1, 255, 0, 0);
	    CR_emitEPS_draw_line(fp, 319, -239, 0, -239, 1, 255, 0, 0);
	    CR_emitEPS_draw_line(fp, 0, -239, 0, -0, 1, 255, 0, 0);
	    CR_emitEPS_closefile(fp);
	    */
	}
}

//----------------------------------------------------------------------------

CR_Matrix *drawn, *drawn2;
CR_Image *ld_im, *rd_im, *cd_im, *lu_im, *ru_im, *cu_im;
CR_Image *drawn_im;

CR_Image *CalibratePolyCam_init_CR_CG(char *name)
{
  int i, j, ii, jj, num_corners_found, eps_h;
  CR_Image *im;
  CR_Vector *v, *v1, *v2, *v3, *v4;
  FILE *fp;
  CR_Movie *polymov;
  CR_Calibration *Cleft, *Cright, *Ccenter;

  ld_im = read_CR_Image("poly35_left.ppm");
  rd_im = read_CR_Image("poly35_right.ppm");
  cd_im = read_CR_Image("poly35_center.ppm");
  lu_im = copy_CR_Image(ld_im);
  ru_im = copy_CR_Image(rd_im);
  cu_im = copy_CR_Image(cd_im);
 
  Cleft = initialize_CR_Calibration("camera1.cal");
  Cright = initialize_CR_Calibration("camera2.cal");
  Ccenter = initialize_CR_Calibration("camera1.cal");

  undistort_image_CR_Calibration(lu_im, ld_im, Cleft);
  undistort_image_CR_Calibration(ru_im, rd_im, Cright);
  undistort_image_CR_Calibration(cu_im, cd_im, Ccenter);

  //  polymov = open_CR_Movie("C:\\InputData\\signfinding2\\poly_35_left.avi");
  //  polymov = open_CR_Movie("F:\\signfinding\\poly_35_center.avi");
  //polymov = open_CR_Movie("F:\\signfinding\\poly_35_right.avi");
  //  polymov->currentframe = 750;  
//   get_frame_CR_Movie(polymov, polymov->currentframe);
//   write_CR_Image("poly35_center.ppm", polymov->im);
  //  write_CR_Image("poly35_right.ppm", polymov->im);
  //write_CR_Image("poly35_left.ppm", polymov->im);
  //  exit(1);

  poly_quad = make_CR_Vector("poly_quad", 8);
  base_rect = make_CR_Vector("base_rect", 8);
  poly_bbox = make_CR_Vector("poly_bbox", 8);
  poly_v1 = make_CR_Vector("poly_v1", 3);
  poly_v2 = make_CR_Vector("poly_v2", 3);
  poly_v1->x[2] = poly_v2->x[2] = 1.0;

  base_rect->x[0] = 0;
  base_rect->x[4] = 0;
  base_rect->x[1] = 319;
  base_rect->x[5] = 0;
  base_rect->x[2] = 319;
  base_rect->x[6] = 239;
  base_rect->x[3] = 0;
  base_rect->x[7] = 239;

  poly_liveim = (CR_Image **) CR_calloc(3, sizeof(CR_Image *));

  // initialize all of the attached cameras

  // canned images
  poly_n_live = 3;
  /*
  poly_n_live = report_num_live_sources_CR_Live();

  if (poly_n_live < 2)
    CR_error("CalibratePolyCam_init_CR_CG(): must be two or more cameras");
  else if (poly_n_live > 3)  // ignore any cameras past the first 3
    poly_n_live = 3; 
  */

  poly_live = (CR_Live **) CR_calloc(poly_n_live, sizeof(CR_Live *));
//  poly_liveim = (CR_Image **) CR_calloc(poly_n_live, sizeof(CR_Image *));

  im_thresh = make_CR_Matrix("im_thresh", 240, 320);
  im_cc = make_CR_Matrix("im_cc", 240, 320);
  corner_n = (double *) CR_calloc(4, sizeof(double));
  corner_x = (double *) CR_calloc(4, sizeof(double));
  corner_y = (double *) CR_calloc(4, sizeof(double));
  corner_r = (double *) CR_calloc(4, sizeof(double));
  corner_g = (double *) CR_calloc(4, sizeof(double));
  corner_b = (double *) CR_calloc(4, sizeof(double));
  corner_id = (int *) CR_calloc(4, sizeof(int));

  poly_corners = (CR_Vector **) CR_calloc(poly_n_live, sizeof(CR_Vector *));
  poly_corners_found = (int *) CR_calloc(poly_n_live, sizeof(int));
  poly_homography = (CR_Matrix ***) CR_calloc(poly_n_live, sizeof(CR_Matrix **));
  for (i = 0; i < poly_n_live; i++) {
    //    poly_live[i] = initialize_live_source(320, 240, i, report_live_source_name_CR_Live(i));

    // canned image display
    /*
    if (i == 0)
      //      poly_live[i] = initialize_live_source(320, 240, i, report_live_source_name_CR_Live(i), "cameracenter.cal");
      poly_live[i] = initialize_live_source(320, 240, i, report_live_source_name_CR_Live(i), "camera1.cal");
    else if (i == 1)
      poly_live[i] = initialize_live_source(320, 240, i, report_live_source_name_CR_Live(i), "camera2.cal");
    else if (i == 2)
      poly_live[i] = initialize_live_source(320, 240, i, report_live_source_name_CR_Live(i), "camera1.cal");
    */

    poly_liveim[i] = make_CR_Image(320, 240, 0);
    poly_corners[i] = make_CR_Vector("poly_corners", 8);
    poly_homography[i] = (CR_Matrix **) CR_calloc(poly_n_live, sizeof(CR_Matrix *));
    for (j = 0; j < poly_n_live; j++) 
      poly_homography[i][j] = make_CR_Matrix("poly_homography", 3, 3);
  }

  v1 = make_CR_Vector("v1", 3);
  v2 = make_CR_Vector("v2", 3);
  v3 = make_CR_Vector("v3", 3);
  v4 = make_CR_Vector("v4", 3);
  v = make_CR_Vector("v", 3);
  v1->x[2] = v2->x[2] = v3->x[2] = v4->x[2] = v->x[2] = 1;

  // START WRITE -----------------------------------------------------------------

  // this will go in some sort of loop when we want to deal with movement of the 
  // target or cameras.  that's when, of course, you really want the images to be
  // synchronized

  // canned images
  /*
  for (i = 0; i < poly_n_live; i++) {
    grab_CR_Live(poly_liveim[i], poly_live[i]);
    CR_flush_printf("grabbed %i\n", i);
    // taking this line out seems to break things---what the f**k ?!?
    //    write_CR_Image(num_suffix_plus_extension("im", i, ".ppm"), poly_liveim[i]);
  }
  */

  //  exit(1);

  // automatic

  /*
  for (i = 0, num_corners_found = 0; i < poly_n_live; i++) {
    poly_corners_found[i] = CalibratePolyCam_find_corners(poly_liveim[i], poly_corners[i]);
    num_corners_found += poly_corners_found[i];
  }
  */

  // manual

  /*

  num_corners_found = 2;

  // center left

  poly_corners_found[0] = 4;
  poly_corners_found[1] = 4;
  poly_corners_found[2] = 0;

  // center

  poly_corners[0]->x[0] = Ccenter->LUT_x[40][16]; 
  poly_corners[0]->x[1] = Ccenter->LUT_x[44][91]; 
  poly_corners[0]->x[2] = Ccenter->LUT_x[139][93];
  poly_corners[0]->x[3] = Ccenter->LUT_x[141][16]; 
  poly_corners[0]->x[4] = Ccenter->LUT_y[40][16];
  poly_corners[0]->x[5] = Ccenter->LUT_y[44][91];
  poly_corners[0]->x[6] = Ccenter->LUT_y[139][93];
  poly_corners[0]->x[7] = Ccenter->LUT_y[141][16];

  // left

  poly_corners[1]->x[0] = Cleft->LUT_x[30][230]; 
  poly_corners[1]->x[1] = Cleft->LUT_x[28][307]; 
  poly_corners[1]->x[2] = Cleft->LUT_x[128][311];
  poly_corners[1]->x[3] = Cleft->LUT_x[129][232]; 
  poly_corners[1]->x[4] = Cleft->LUT_y[30][230];
  poly_corners[1]->x[5] = Cleft->LUT_y[28][307];
  poly_corners[1]->x[6] = Cleft->LUT_y[128][311];
  poly_corners[1]->x[7] = Cleft->LUT_y[129][232];

  // center right

  poly_corners_found[0] = 4;
  poly_corners_found[1] = 0;
  poly_corners_found[2] = 4;

  // center

  poly_corners[0]->x[0] = Ccenter->LUT_x[56][230]; 
  poly_corners[0]->x[1] = Ccenter->LUT_x[55][304]; 
  poly_corners[0]->x[2] = Ccenter->LUT_x[153][307];
  poly_corners[0]->x[3] = Ccenter->LUT_x[151][230]; 
  poly_corners[0]->x[4] = Ccenter->LUT_y[56][230];
  poly_corners[0]->x[5] = Ccenter->LUT_y[55][304];
  poly_corners[0]->x[6] = Ccenter->LUT_y[153][307];
  poly_corners[0]->x[7] = Ccenter->LUT_y[151][230];

  // right

  poly_corners[2]->x[0] = Cright->LUT_x[34][6]; 
  poly_corners[2]->x[1] = Cright->LUT_x[36][82]; 
  poly_corners[2]->x[2] = Cright->LUT_x[134][82];
  poly_corners[2]->x[3] = Cright->LUT_x[134][4]; 
  poly_corners[2]->x[4] = Cright->LUT_y[34][6];
  poly_corners[2]->x[5] = Cright->LUT_y[36][82];
  poly_corners[2]->x[6] = Cright->LUT_y[134][82];
  poly_corners[2]->x[7] = Cright->LUT_y[134][4];

  // if the rect is visible in two or more images simultaneously

  if (num_corners_found > 1) 
    poly_compute_homography(poly_n_live, poly_corners_found, poly_corners, poly_homography);

  im = NULL;

  exit(1);
  */

  // END WRITE -------------------------------------------------------------------

  // START READ ------------------------------------------------------------------

  // load homographies

  // left to center transformation???
  //  poly_h_left = read_dlm_CR_Matrix("homography_01.mat", 3, 3);
  poly_h_left = read_dlm_CR_Matrix("undistorted_homography_01.mat", 3, 3);
  poly_h_left_inv = inverse_CR_Matrix(poly_h_left);

  // right to center transformation???
  //  poly_h_right = read_dlm_CR_Matrix("homography_02.mat", 3, 3);
  poly_h_right = read_dlm_CR_Matrix("undistorted_homography_02.mat", 3, 3);
  poly_h_right_inv = inverse_CR_Matrix(poly_h_right);

  //  poly_compute_bbox(poly_bbox, poly_h_left, poly_h_right);
  poly_compute_bbox(poly_bbox, poly_h_left_inv, poly_h_right_inv);

  // create window the size of bbox

  im = make_CR_Image(poly_bbox->x[1] - poly_bbox->x[0], poly_bbox->x[7] - poly_bbox->x[4], 0);
  poly_im = make_CR_Image(im->w, im->h, 0);
  poly_flipim = make_CR_Image(im->w, im->h, 0);

  drawn = make_CR_Matrix("drawn", im->h, im->w);
  drawn2 = make_CR_Matrix("drawn2", im->h, im->w);
  
  drawn_im = make_CR_Image(im->w, im->h, 0);

  // look-up tables for faster mosaicing

  poly_left_x = (int **) CR_calloc(240, sizeof(int *));
  poly_left_y = (int **) CR_calloc(240, sizeof(int *));
  poly_right_x = (int **) CR_calloc(240, sizeof(int *));
  poly_right_y = (int **) CR_calloc(240, sizeof(int *));

  for (j = 0; j < 240; j++) {
    poly_left_x[j] = (int *) CR_calloc(320, sizeof(int));
    poly_left_y[j] = (int *) CR_calloc(320, sizeof(int));
    poly_right_x[j] = (int *) CR_calloc(320, sizeof(int));
    poly_right_y[j] = (int *) CR_calloc(320, sizeof(int));

    for (i = 0; i < 320; i++) {

      poly_v1->x[0] = i;
      poly_v1->x[1] = j;

      // left...

      transform_homography_CR_Matrix(poly_h_left_inv, poly_v1, poly_v2);

      poly_left_x[j][i] = CR_clamp(poly_v2->x[0] - poly_bbox->x[0], 0, poly_im->w - 1);
      poly_left_y[j][i] = CR_clamp(poly_im->h - (poly_v2->x[1] - poly_bbox->x[4]), 0, poly_im->h - 1);

      // ...and right

      transform_homography_CR_Matrix(poly_h_right_inv, poly_v1, poly_v2);

      poly_right_x[j][i] = CR_clamp(poly_v2->x[0] - poly_bbox->x[0], 0, poly_im->w - 1);
      poly_right_y[j][i] = CR_clamp(poly_im->h - (poly_v2->x[1] - poly_bbox->x[4]), 0, poly_im->h - 1);
    }
  }

//   poly_liveim[0] = read_CR_Image("im0.ppm");
//   poly_liveim[1] = read_CR_Image("im1.ppm");
//   poly_liveim[2] = read_CR_Image("im2.ppm");

  // the im returned should be the one displaying a mosaic, so everything else
  // should be done in this function beforehand

  // END READ --------------------------------------------------------------------

  // START EXTRA CRAP

//   ld_im = read_CR_Image("poly35_left.ppm");
//   rd_im = read_CR_Image("poly35_right.ppm");
//   cd_im = read_CR_Image("poly35_center.ppm");
//   lu_im = copy_CR_Image(ld_im);
//   ru_im = copy_CR_Image(rd_im);
//   cu_im = copy_CR_Image(cd_im);
// 
//   undistort_image_CR_Calibration(lu_im, ld_im, initialize_CR_Calibration("camera1.cal"));
//   undistort_image_CR_Calibration(ru_im, rd_im, initialize_CR_Calibration("camera2.cal"));
//   undistort_image_CR_Calibration(cu_im, cd_im, initialize_CR_Calibration("camera1.cal"));
// 
//   write_CR_Image("poly35_left_undistorted.ppm", lu_im);
//   write_CR_Image("poly35_right_undistorted.ppm", ru_im);
//   write_CR_Image("poly35_center_undistorted.ppm", cu_im);

  // END EXTRA CRAP

  return im;
}

//----------------------------------------------------------------------------

int read_flag = FALSE;

void CalibratePolyCam_grab_CR_CG(CR_Image *im)
{
  int i, j, ii, jj;
  double left_dist, right_dist, center_dist, left_coeff, centerleft_coeff, centerright_coeff, right_coeff;
  int cx, cy, lx, ly, rx, ry, passes;

  // make background black

//   glClearColor(0, 0, 0, 0);
//   glClear(GL_COLOR_BUFFER_BIT);

  // now...
  // just keep showing two image quadrilaterals in panoramic window on black background

  // draw base (center) rect (remember to account for offset--old (0, 0) isn't upper left any more)

  /*
  ogl_draw_quadrilateral(base_rect->x[0] - poly_bbox->x[0], poly_im->h - (base_rect->x[4] - poly_bbox->x[4]),
			 base_rect->x[1] - poly_bbox->x[0], poly_im->h - (base_rect->x[5] - poly_bbox->x[4]),
			 base_rect->x[2] - poly_bbox->x[0], poly_im->h - (base_rect->x[6] - poly_bbox->x[4]),
			 base_rect->x[3] - poly_bbox->x[0], poly_im->h - (base_rect->x[7] - poly_bbox->x[4]),
			 255, 255, 255);

  // transform left rect to base coords (plus offset) and draw

  //  transform_homography_rect(poly_h_left, base_rect, poly_quad);
  transform_homography_rect(poly_h_left_inv, base_rect, poly_quad);

  ogl_draw_quadrilateral(poly_quad->x[0] - poly_bbox->x[0], poly_im->h - (poly_quad->x[4] - poly_bbox->x[4]),
			 poly_quad->x[1] - poly_bbox->x[0], poly_im->h - (poly_quad->x[5] - poly_bbox->x[4]),
			 poly_quad->x[2] - poly_bbox->x[0], poly_im->h - (poly_quad->x[6] - poly_bbox->x[4]),
			 poly_quad->x[3] - poly_bbox->x[0], poly_im->h - (poly_quad->x[7] - poly_bbox->x[4]),
			 255, 255, 255);

  // transform right rect to base coords (plus offset) and draw

  //  transform_homography_rect(poly_h_right, base_rect, poly_quad);
  transform_homography_rect(poly_h_right_inv, base_rect, poly_quad);

  ogl_draw_quadrilateral(poly_quad->x[0] - poly_bbox->x[0], poly_im->h - (poly_quad->x[4] - poly_bbox->x[4]),
			 poly_quad->x[1] - poly_bbox->x[0], poly_im->h - (poly_quad->x[5] - poly_bbox->x[4]),
			 poly_quad->x[2] - poly_bbox->x[0], poly_im->h - (poly_quad->x[6] - poly_bbox->x[4]),
			 poly_quad->x[3] - poly_bbox->x[0], poly_im->h - (poly_quad->x[7] - poly_bbox->x[4]),
			 255, 255, 255);
  */

  // later...
  // grab the images 

  set_all_CR_Matrix(0.0, drawn);
  set_all_CR_Matrix(0.0, drawn2);

  // read canned images

  if (!read_flag) {
    poly_liveim[1] = read_CR_Image("poly35_left_undistorted.ppm");
    poly_liveim[2] = read_CR_Image("poly35_right_undistorted.ppm");
    poly_liveim[0] = read_CR_Image("poly35_center_undistorted.ppm");
  }
  
  // read live images

  /*
  for (i = 0; i < poly_n_live; i++) {
    //    grab_CR_Live(poly_liveim[i], poly_live[i]);
    grab_CR_Live(poly_liveim[i], poly_live[i], TRUE);
    // taking this line out seems to break things---what the f**k ?!?
    //    write_CR_Image(num_suffix_plus_extension("im", i, ".ppm"), poly_liveim[i]);
  }
  */

  // draw left, right, and center images in the center coordinate system
  // eventually we'll want to cross-fade overlapping pixels 

  for (j = 0; j < poly_im->h; j++)
    for (i = 0; i < poly_im->w; i++) {
      IMXY_R(poly_im, i, j) = (unsigned char) 0;
      IMXY_G(poly_im, i, j) = (unsigned char) 0;
      IMXY_B(poly_im, i, j) = (unsigned char) 0;
    }

  // figure out center points

//   cx = CR_clamp(160 - poly_bbox->x[0], 0, poly_im->w - 1);
//   cy = CR_clamp(poly_im->h - (120 - poly_bbox->x[4]), 0, poly_im->h - 1);
//   lx = poly_left_x[120][160];
//   ly = poly_left_y[120][160];
//   rx = poly_right_x[120][160];
//   ry = poly_right_y[120][160];

  centerleft_coeff = centerright_coeff = right_coeff = left_coeff = 0.5;

  // draw left and right

  for (j = 0; j < poly_liveim[0]->h; j++)
    for (i = 0; i < poly_liveim[0]->w; i++) {

      poly_v1->x[0] = i;
      poly_v1->x[1] = j;

      ii = CR_clamp(i - poly_bbox->x[0], 0, poly_im->w - 1);
      jj = CR_clamp(poly_im->h - (j - poly_bbox->x[4]), 0, poly_im->h - 1);
      
//       center_dist = sqrt(SQUARE(ii - cx) + SQUARE(jj - cy));
//       left_dist = sqrt(SQUARE(poly_left_x[j][i] - lx) + SQUARE(poly_left_y[j][i] - ly));
//       right_dist = sqrt(SQUARE(poly_right_x[j][i] - rx) + SQUARE(poly_right_y[j][i] - ry));
// 
//       centerleft_coeff = left_dist / (1 + left_dist + center_dist);
//       left_coeff = center_dist / (1 + left_dist + center_dist);
// 
//       centerright_coeff = right_dist / (1 + right_dist + center_dist);
//       right_coeff = center_dist / (1 + right_dist + center_dist);

      // draw center image on top

//       IMXY_R(poly_im, ii, jj) = (unsigned char) IMXY_R(poly_liveim[0], i, j);
//       IMXY_G(poly_im, ii, jj) = (unsigned char) IMXY_G(poly_liveim[0], i, j);
//       IMXY_B(poly_im, ii, jj) = (unsigned char) IMXY_B(poly_liveim[0], i, j);
// 
//       MATRC(drawn, jj, ii) = 1.0;

      // left...

      /*
      //      transform_homography_CR_Matrix(poly_h_left, poly_v1, poly_v2);
      transform_homography_CR_Matrix(poly_h_left_inv, poly_v1, poly_v2);

      ii = CR_clamp(poly_v2->x[0] - poly_bbox->x[0], 0, poly_im->w - 1);
      jj = CR_clamp(poly_im->h - (poly_v2->x[1] - poly_bbox->x[4]), 0, poly_im->h - 1);

      IMXY_R(poly_im, ii, jj) = (unsigned char) IMXY_R(poly_liveim[1], i, j);
      IMXY_G(poly_im, ii, jj) = (unsigned char) IMXY_G(poly_liveim[1], i, j);
      IMXY_B(poly_im, ii, jj) = (unsigned char) IMXY_B(poly_liveim[1], i, j);
      */

      MATRC(drawn, poly_left_y[j][i], poly_left_x[j][i]) = 1.0;
      MATRC(drawn, poly_right_y[j][i], poly_right_x[j][i]) = 1.0;

//       if (MATRC(drawn, poly_left_y[j][i], poly_left_x[j][i])) {
// 	IMXY_R(poly_im, poly_left_x[j][i], poly_left_y[j][i]) = (left_coeff * (unsigned char) IMXY_R(poly_im, poly_left_x[j][i], poly_left_y[j][i]) + centerleft_coeff * (unsigned char) IMXY_R(poly_liveim[1], i, j));
// 	IMXY_G(poly_im, poly_left_x[j][i], poly_left_y[j][i]) = (left_coeff * (unsigned char) IMXY_G(poly_im, poly_left_x[j][i], poly_left_y[j][i]) + centerleft_coeff * (unsigned char) IMXY_G(poly_liveim[1], i, j));
// 	IMXY_B(poly_im, poly_left_x[j][i], poly_left_y[j][i]) = (left_coeff * (unsigned char) IMXY_B(poly_im, poly_left_x[j][i], poly_left_y[j][i]) + centerleft_coeff * (unsigned char) IMXY_B(poly_liveim[1], i, j));
//       }
//       else {
	IMXY_R(poly_im, poly_left_x[j][i], poly_left_y[j][i]) = (unsigned char) IMXY_R(poly_liveim[1], i, j);
	IMXY_G(poly_im, poly_left_x[j][i], poly_left_y[j][i]) = (unsigned char) IMXY_G(poly_liveim[1], i, j);
	IMXY_B(poly_im, poly_left_x[j][i], poly_left_y[j][i]) = (unsigned char) IMXY_B(poly_liveim[1], i, j);
	//      }

      // ...and right

      /*
      //      transform_homography_CR_Matrix(poly_h_right, poly_v1, poly_v2);
      transform_homography_CR_Matrix(poly_h_right_inv, poly_v1, poly_v2);

      ii = CR_clamp(poly_v2->x[0] - poly_bbox->x[0], 0, poly_im->w - 1);
      jj = CR_clamp(poly_im->h - (poly_v2->x[1] - poly_bbox->x[4]), 0, poly_im->h - 1);

      IMXY_R(poly_im, ii, jj) = (unsigned char) IMXY_R(poly_liveim[2], i, j);
      IMXY_G(poly_im, ii, jj) = (unsigned char) IMXY_G(poly_liveim[2], i, j);
      IMXY_B(poly_im, ii, jj) = (unsigned char) IMXY_B(poly_liveim[2], i, j);
      */

//       if (MATRC(drawn, poly_right_y[j][i], poly_right_x[j][i])) {
// 	IMXY_R(poly_im, poly_right_x[j][i], poly_right_y[j][i]) = (right_coeff * (unsigned char) IMXY_R(poly_im, poly_right_x[j][i], poly_right_y[j][i]) + centerright_coeff * (unsigned char) IMXY_R(poly_liveim[2], i, j));
// 	IMXY_G(poly_im, poly_right_x[j][i], poly_right_y[j][i]) = (right_coeff * (unsigned char) IMXY_G(poly_im, poly_right_x[j][i], poly_right_y[j][i]) + centerright_coeff * (unsigned char) IMXY_G(poly_liveim[2], i, j));
// 	IMXY_B(poly_im, poly_right_x[j][i], poly_right_y[j][i]) = (right_coeff * (unsigned char) IMXY_B(poly_im, poly_right_x[j][i], poly_right_y[j][i]) + centerright_coeff * (unsigned char) IMXY_B(poly_liveim[2], i, j));
//       }
//       else {
	IMXY_R(poly_im, poly_right_x[j][i], poly_right_y[j][i]) = (unsigned char) IMXY_R(poly_liveim[2], i, j);
	IMXY_G(poly_im, poly_right_x[j][i], poly_right_y[j][i]) = (unsigned char) IMXY_G(poly_liveim[2], i, j);
	IMXY_B(poly_im, poly_right_x[j][i], poly_right_y[j][i]) = (unsigned char) IMXY_B(poly_liveim[2], i, j);
	//      }

//       IMXY_R(poly_im, poly_right_x[j][i], poly_right_y[j][i]) = (unsigned char) IMXY_R(poly_liveim[2], i, j);
//       IMXY_G(poly_im, poly_right_x[j][i], poly_right_y[j][i]) = (unsigned char) IMXY_G(poly_liveim[2], i, j);
//       IMXY_B(poly_im, poly_right_x[j][i], poly_right_y[j][i]) = (unsigned char) IMXY_B(poly_liveim[2], i, j);

    }

//   draw_CR_Matrix_CR_Image(1.0, drawn, drawn_im, FALSE);
//   write_CR_Image("drawn1.ppm", drawn_im);

  // fill in holes

  int neighbors;
  int l, r, u, d, lr, lg, lb, rr, rg, rb, ur, ug, ub, dr, dg, db;

  for (passes = 0; passes < 2; passes++) {

    for (j = 1; j < poly_im->h - 1; j++)
      for (i = 1; i < poly_im->w - 1; i++) {
	if (!MATRC(drawn, j, i)) {
	  neighbors = (MATRC(drawn, j - 1, i) + MATRC(drawn, j + 1, i) + MATRC(drawn, j, i - 1) + MATRC(drawn, j, i + 1));
	  if (neighbors) {
	    u = (int) MATRC(drawn, j - 1, i);
	    d = (int) MATRC(drawn, j + 1, i);
	    l = (int) MATRC(drawn, j, i - 1);
	    r = (int) MATRC(drawn, j, i + 1);
	    ur = (unsigned char) IMXY_R(poly_im, i, j - 1);
	    ug = (unsigned char) IMXY_G(poly_im, i, j - 1);
	    ub = (unsigned char) IMXY_B(poly_im, i, j - 1);
	    dr = (unsigned char) IMXY_R(poly_im, i, j + 1);
	    dg = (unsigned char) IMXY_G(poly_im, i, j + 1);
	    db = (unsigned char) IMXY_B(poly_im, i, j + 1);
	    lr = (unsigned char) IMXY_R(poly_im, i - 1, j);
	    lg = (unsigned char) IMXY_G(poly_im, i - 1, j);
	    lb = (unsigned char) IMXY_B(poly_im, i - 1, j);
	    rr = (unsigned char) IMXY_R(poly_im, i + 1, j);
	    rg = (unsigned char) IMXY_G(poly_im, i + 1, j);
	    rb = (unsigned char) IMXY_B(poly_im, i + 1, j);
	    
	    IMXY_R(poly_im, i, j) = (u * ur + d + dr + l * lr + r * rr) / neighbors;
	    IMXY_G(poly_im, i, j) = (u * ug + d + dg + l * lg + r * rg) / neighbors;
	    IMXY_B(poly_im, i, j) = (u * ub + d + db + l * lb + r * rb) / neighbors;
	    MATRC(drawn2, j, i) = 1.0;
	  }
	}
      }
    
    for (j = 1; j < poly_im->h - 1; j++)
      for (i = 1; i < poly_im->w - 1; i++) 
	if (MATRC(drawn2, j, i))
	  MATRC(drawn, j, i) = 1.0;
  }

//   draw_CR_Matrix_CR_Image(1.0, drawn, drawn_im, FALSE);
//   write_CR_Image("drawn2.ppm", drawn_im);
  
  // draw center image

  for (j = 0; j < poly_liveim[0]->h; j++)
    for (i = 0; i < poly_liveim[0]->w; i++) {

      poly_v1->x[0] = i;
      poly_v1->x[1] = j;

      ii = CR_clamp(i - poly_bbox->x[0], 0, poly_im->w - 1);
      jj = CR_clamp(poly_im->h - (j - poly_bbox->x[4]), 0, poly_im->h - 1);

      // draw center image on top

      //      if (MATRC(drawn, poly_left_y[j][i], poly_left_x[j][i])) {
      if (MATRC(drawn, jj, ii)) {
  	IMXY_R(poly_im, ii, jj) = (0.5 * (unsigned char) IMXY_R(poly_im, ii, jj) + 0.5 * (unsigned char) IMXY_R(poly_liveim[0], i, j));
  	IMXY_G(poly_im, ii, jj) = (0.5 * (unsigned char) IMXY_G(poly_im, ii, jj) + 0.5 * (unsigned char) IMXY_G(poly_liveim[0], i, j));
  	IMXY_B(poly_im, ii, jj) = (0.5 * (unsigned char) IMXY_B(poly_im, ii, jj) + 0.5 * (unsigned char) IMXY_B(poly_liveim[0], i, j));
      }
      else {
	IMXY_R(poly_im, ii, jj) = (unsigned char) IMXY_R(poly_liveim[0], i, j);
	IMXY_G(poly_im, ii, jj) = (unsigned char) IMXY_G(poly_liveim[0], i, j);
	IMXY_B(poly_im, ii, jj) = (unsigned char) IMXY_B(poly_liveim[0], i, j);
      }

      MATRC(drawn, jj, ii) = 1.0;
    }

  for (j = 0; j < poly_im->h; j++)
    for (i = 0; i < poly_im->w; i++) 
      if (!MATRC(drawn, j, i))
	set_pixel_rgb_CR_Image(i, j, 255, 255, 255, poly_im);

  vflip_CR_Image(poly_im, drawn_im);
  write_CR_Image("polycam.ppm", drawn_im);


//   draw_CR_Matrix_CR_Image(1.0, drawn, drawn_im, FALSE);
//   write_CR_Image("drawn3.ppm", drawn_im);
//   exit(1);

//   write_CR_Image("test.ppm", poly_im);
//   exit(1);

    // Draw the latest image

  glDrawPixels(poly_im->w, poly_im->h, GL_RGB, GL_UNSIGNED_BYTE, poly_im->data);
}

//----------------------------------------------------------------------------

void CalibratePolyCam_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Image *SuperResSigns1_init_CR_CG(char *name)
{
  CR_Image *im;
  double w0, h0;
  int i, j;
  CR_Image *tmap;

  // initialize

  super_rect = make_CR_Vector("rect", 8);
  super_texture = make_CR_Vector("texture", 8);
  super_homography = make_CR_Matrix("homography", 3, 3);

  // open movie

  camera_movie = open_CR_Movie("D:\\Documents\\software\\highway_sign1.avi");
  //  camera_movie->currentframe = 0;  // 0 is canonical start position
  camera_movie->currentframe = 90;  // 0 is canonical start position
  get_frame_CR_Movie(camera_movie, camera_movie->currentframe);

  im = make_CR_Image(camera_movie->width, camera_movie->height, 0);
  roadim = make_CR_Image(camera_movie->im->w, camera_movie->im->h, 0);

  // detect sign 

  // highway_sign1.avi, frame 0 corners: manual

  /*
  super_rect->x[0] = 464;
  super_rect->x[1] = 502; 
  super_rect->x[2] = 500; 
  super_rect->x[3] = 461;
  super_rect->x[4] = 228;
  super_rect->x[5] = 230;
  super_rect->x[6] = 254;
  super_rect->x[7] = 251;
  */

  // highway_sign1.avi, frame 90 corners: manual

  super_rect->x[0] = 563;
  super_rect->x[1] = 636; 
  super_rect->x[2] = 634; 
  super_rect->x[3] = 562;
  super_rect->x[4] = 198;
  super_rect->x[5] = 200;
  super_rect->x[6] = 245;
  super_rect->x[7] = 243;

  // estimate width, height by assuming a more-or-less frontal view

  w0 = 0.5 * (sqrt(SQUARE(super_rect->x[1] - super_rect->x[0]) + SQUARE(super_rect->x[5] - super_rect->x[4])) +
	      sqrt(SQUARE(super_rect->x[2] - super_rect->x[3]) + SQUARE(super_rect->x[6] - super_rect->x[7])));
  h0 = 0.5 * (sqrt(SQUARE(super_rect->x[3] - super_rect->x[0]) + SQUARE(super_rect->x[7] - super_rect->x[4])) +
	      sqrt(SQUARE(super_rect->x[2] - super_rect->x[1]) + SQUARE(super_rect->x[6] - super_rect->x[5])));

  super_res = 4;

  super_w = super_res * w0;
  super_h = super_res * h0;

  super_texture->x[0] = 0;
  super_texture->x[1] = super_w;
  super_texture->x[2] = super_w;
  super_texture->x[3] = 0;
  super_texture->x[4] = 0;
  super_texture->x[5] = 0;
  super_texture->x[6] = super_h;
  super_texture->x[7] = super_h;

  compute_homography_CR_Matrix(super_rect, super_texture, super_homography);
  super_homography_inv = inverse_CR_Matrix(super_homography);

  printf("w0 = %lf, h0 = %lf\n", w0, h0);
  printf("sw = %lf, sh = %lf\n", super_w, super_h);
  print_CR_Matrix(super_homography);
  
  CR_Vector *V_image, *V_tmap;

  V_image = make_CR_Vector("V_image", 3);
  V_tmap = make_CR_Vector("V_tmap", 3);
  //  V_image = init_CR_Vector("UL", 3,
			     //			     464.0, 228.0, 1.0);
  //502.0, 230.0, 1.0);
  //			     500.0, 254.0, 1.0);

//   V_image = init_CR_Vector("UL", 3,
// 			     0.0, 0.0, 1.0);

  //  transform_homography_CR_Matrix(super_homography, V_image, V_tmap);

  //  print_CR_Vector(V_tmap);

  // initialize pose, appearance trackers

  // fill texture map, write to ppm, exit

  tmap = make_CR_Image(ceil(super_w), ceil(super_h), 0);
  printf("tmap w = %i, tmap h = %i\n", tmap->w, tmap->h);

  for (j = 0; j < tmap->h; j++)
    for (i = 0; i < tmap->w; i++) {

      V_tmap->x[0] = (double) i;
      V_tmap->x[1] = (double) j;
      V_tmap->x[2] = 1.0;

      transform_homography_CR_Matrix(super_homography_inv, V_tmap, V_image);

      IMXY_R(tmap, i, j) = (unsigned char) IMXY_R(camera_movie->im, (int) CR_round(V_image->x[0]), (int) CR_round(V_image->x[1]));
      IMXY_G(tmap, i, j) = (unsigned char) IMXY_G(camera_movie->im, (int) CR_round(V_image->x[0]), (int) CR_round(V_image->x[1]));
      IMXY_B(tmap, i, j) = (unsigned char) IMXY_B(camera_movie->im, (int) CR_round(V_image->x[0]), (int) CR_round(V_image->x[1]));
    }

  //  write_CR_Image("tmap.ppm", tmap);
  //  exit(1);

  // how do we know (a) what shape, and (b) what resolution to make appearance image?

  // finish

  return im;
}

//----------------------------------------------------------------------------

void SuperResSigns1_grab_CR_CG(CR_Image *im)
{
  // draw the latest image

  vflip_CR_Image(camera_movie->im, roadim);
  glDrawPixels(roadim->w, roadim->h, GL_RGB, GL_UNSIGNED_BYTE, roadim->data);

  // update pose

  CR_flush_printf("writing\n");

  write_CR_Image("test.ppm", camera_movie->im);
  compute_motion_templates(camera_movie->im);

  // update appearance

  // advance camera frame

  if (camera_movie->currentframe < camera_movie->lastframe) 
    get_frame_CR_Movie(camera_movie, camera_movie->currentframe++);
}

//----------------------------------------------------------------------------

void SuperResSigns1_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");

  //  fclose(puddlefp);
  //  fclose(mapfp);
  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#define NOT_MAPPED    12345.67
double maproad_minmin, maproad_maxmax;
CR_Matrix *min_mat, *max_mat, *avg_mat;

CR_Image *MapRoad_init_CR_CG(char *name)
{
  CR_Image *im;
  FILE *fp_min, *fp_max, *fp_avg;
  int i, j, w, h;
  double temp;

  fp_avg = fopen("map.avg", "r");
  fp_min = fopen("map.min", "r");
  fp_max = fopen("map.max", "r");

  fscanf(fp_avg, "%i\n%i\n", &w, &h);
  fscanf(fp_min, "%i\n%i\n", &w, &h);
  fscanf(fp_max, "%i\n%i\n", &w, &h);

  im = make_CR_Image(w, h, 0);

  min_mat = make_CR_Matrix("min", h, w);
  max_mat = make_CR_Matrix("max", h, w);
  avg_mat = make_CR_Matrix("avg", h, w);

  for (j = 0; j < h; j++) {
    for (i = 0; i < w; i++) {
      fscanf(fp_avg, "%lf ", &temp);
      MATRC(avg_mat, j, i) = temp;
    }
    fscanf(fp_avg, "\n");
  }

  maproad_minmin = 1000000000;

  for (j = 0; j < h; j++) {
    for (i = 0; i < w; i++) {
      fscanf(fp_min, "%lf ", &temp);
      MATRC(min_mat, j, i) = temp;
      if (temp < maproad_minmin && temp != NOT_MAPPED)
	maproad_minmin = temp;
    }
    fscanf(fp_min, "\n");
  }

  maproad_maxmax = -1000000000;
  
  for (j = 0; j < h; j++) {
    for (i = 0; i < w; i++) {
      fscanf(fp_max, "%lf ", &temp);
      MATRC(max_mat, j, i) = temp;
      if (temp > maproad_maxmax && temp != NOT_MAPPED)
	maproad_maxmax = temp;
    }
    fscanf(fp_max, "\n");
  }

  printf("maproad_minmin = %lf, maproad_maxmax = %lf\n", maproad_minmin, maproad_maxmax);
  
  return im;
}

//----------------------------------------------------------------------------

void MapRoad_grab_CR_CG(CR_Image *im)
{
  int i, j, intensity;

  for (j = 0; j < 100; j++)
    for (i = 0; i < 100; i++) {
      intensity = (int) CR_round(255.0 * (MATRC(avg_mat, j, i) - maproad_minmin) / (maproad_maxmax - maproad_minmin));
      ogl_draw_point(i, j, 1, intensity, intensity, intensity);
    }

      //  ogl_draw_CR_Matrix(15, 64 + 15, im->h, dirt_output);
}

//----------------------------------------------------------------------------

void MapRoad_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  fclose(good_road_fp);
  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Image *CalibrateLaserCamera_init_CR_CG(char *name)
{
  CR_Image *im;
  char *filename;
  int i, j, k, w, h;
  int num_laser_triplets, num_camera_triplets, frame;
  CR_Movie *lasermov;
  double laser_time_left, laser_time_center, laser_time_right;
  double total_time, total_seo_frames_expected, total_seo_frames_received, approximate_seo_performance;
  int facet_left, facet_center, facet_right, laser_r_left, laser_r_center, laser_r_right, laser_x_left, laser_y_left, laser_x_center, laser_y_center, laser_x_right, laser_y_right, camera_x_left, camera_y_left, camera_x_center, camera_y_center, camera_x_right, camera_y_right;
  int temp_facet;
  double temp_time, this_time, last_time, diff_time, timel, timec, temptime, tempx, tempy;
  int xl, yl, rl, xc, yc;
  double x_l, y_l, z_l;
  double center_margin;

  im = make_CR_Image(720, 480, 0);

  // start calibration

  // from ITG calibration on July 17, 2001 (see "itg_calib_results.txt")

//   fc_x = 922.30444;
//   fc_y = 833.72305;
//   cc_x = 339.61916;
//   cc_y = 238.93843;
//   kc_1 = -0.23725;
//   kc_2 = 0.30011;
//   kc_3 = 0.00169; 
//   kc_4 = 0.00137;
//   kc_5 = 0.00000;

  //  exit(1);

  // stop calibration

  // start write clean master file (no laser points in the overlapping center region)

  masterfile = fopen("calib_new_clean_5_master.dat", "r");
  clean_masterfile = fopen("calib_new_clean_5_xyz_master.dat", "w");

  center_margin = 5;

  num_master_points = count_lines(masterfile);
  
  for (i = 0; i < num_master_points; i++) {

    fscanf(masterfile, "%i, %i, %i, %i, %i, %i, %i\n", &laser_frame, &xl, &yl, &rl, &camera_frame, &xc, &yc);
    //    if (xl < (D3_LASER_WIDTH / 2 - (center_margin - 1)) || xl > (D3_LASER_WIDTH / 2 + center_margin))
    //      fprintf(clean_masterfile, "%i, %i, %i, %i, %i, %i, %i\n", laser_frame, xl, yl, rl, camera_frame, xc, yc);
    //    else
    //      printf("kicking out laser frame = %i, xl = %i\n", laser_frame, xl);

    demo3_get_gall_laser_xyz(xl, yl, rl, &x_l, &y_l, &z_l);
    fprintf(clean_masterfile, "%i, %lf, %lf, %lf, %i, %i, %i\n", laser_frame, x_l, y_l, z_l, camera_frame, xc, yc);
  }

  exit(1);

  /*

  // stop write clean master file

  // start write the master file

  lasertimefile = fopen("calib_laser.txt", "r");
  camtimefile = fopen("calib_camera.txt", "r");
  masterfile = fopen("calib_master.dat", "w");
  annotated_masterfile = fopen("calib_annotated_master.txt", "w");

  num_laser_points = count_lines(lasertimefile);
  num_camera_points = count_lines(camtimefile);

  campoints = make_CR_Matrix("campoints", num_camera_points, 3);
  for (i = 0; i < num_camera_points; i++) {
    fscanf(camtimefile, "%lf %lf %lf\n", &temptime, &tempx, &tempy);
    MATRC(campoints, i, 0) = temptime;
    MATRC(campoints, i, 1) = tempx;
    MATRC(campoints, i, 2) = tempy;
  }
  fclose(camtimefile);
  //  print_CR_Matrix(campoints);
  //  exit(1);

  for (i = 0; i < num_laser_points; i += 3) {

    for (j = 0; j < 3; j++) {

      fscanf(lasertimefile, "%lf %i %i %i\n", &timel, &xl, &yl, &rl);
      for (k = j, mincamtime = MATRC(campoints, j, 0), mindifftime = fabs(timel - mincamtime), minindex = j; k < num_camera_points; k += 3) {
	if (fabs(timel - MATRC(campoints, k, 0)) < mindifftime) {
	  mincamtime = MATRC(campoints, k, 0);
	  mindifftime = fabs(timel - mincamtime);
	  minindex = k;
	}
      }
      if (mindifftime <= 0.1) {  // threshold on synchronicity of laser and camera frames
	fprintf(annotated_masterfile, "j = %i, laser time %lf, camera time %lf, diff time = %lf: %i, %i, %i, %i, %i\n", j, timel, mincamtime, mindifftime, (int) xl, (int) yl, (int) rl, (int) MATRC(campoints, minindex, 1), (int) MATRC(campoints, minindex, 2));
	fprintf(masterfile, "%i, %i, %i, %i, %i\n", (int) xl, (int) yl, (int) rl, (int) MATRC(campoints, minindex, 1), (int) MATRC(campoints, minindex, 2));
      }
    }
  }

  exit(1);
  */

  // stop write the master file

  lasermov = read_demo3_laser_to_CR_Movie("C:\\InputData\\itg_July17_2001\\cal2-001.fw");
  start_laser_time = lasermov->laserim[0].navdat[0].time;
  printf("start laser time = %lf\n", start_laser_time);
  //  exit(1);

  laserpointfile = fopen("laserfile.txt", "r");
  camerapointfile = fopen("camfile.txt", "r");

  num_laser_triplets = count_lines(laserpointfile);
  num_camera_triplets = count_lines(camerapointfile);
  
  lasertimefile = fopen("calib_laser.txt", "w");

  printf("laser triplets = %i, camera triplets = %i\n", num_laser_triplets, num_camera_triplets);

  for (i = 0; i < num_laser_triplets; i++) {

    fscanf(laserpointfile, "%i %i %i %i %i %i %i %i %i %i\n", &laser_frame, &laser_x_left, &laser_y_left, &laser_r_left, &laser_x_center, &laser_y_center, &laser_r_center, &laser_x_right, &laser_y_right, &laser_r_right);

    facet_left = smart_demo3_get_laser_facet(laser_x_left, laser_y_left, &(lasermov->laserim[laser_frame]));
    facet_center = smart_demo3_get_laser_facet(laser_x_center, laser_y_center, &(lasermov->laserim[laser_frame]));
    facet_right = smart_demo3_get_laser_facet(laser_x_right, laser_y_right, &(lasermov->laserim[laser_frame]));

    laser_time_left = 0.001 * (lasermov->laserim[laser_frame].navdat[facet_left].time - start_laser_time);
    laser_time_center = 0.001 * (lasermov->laserim[laser_frame].navdat[facet_center].time - start_laser_time);
    laser_time_right = 0.001 * (lasermov->laserim[laser_frame].navdat[facet_right].time - start_laser_time);

    fprintf(lasertimefile, "%i %lf %i %i %i\n%i %lf %i %i %i\n%i %lf %i %i %i\n", 
	    laser_frame, laser_time_left, laser_x_left, laser_y_left, laser_r_left,
	    laser_frame, laser_time_center, laser_x_center, laser_y_center, laser_r_center,
	    laser_frame, laser_time_right, laser_x_right, laser_y_right, laser_r_right);

//     temp_facet = smart_demo3_get_laser_facet(laser_x_left, laser_y_left, &(lasermov->laserim[laser_frame]));
//     temp_time = lasermov->laserim[laser_frame].navdat[temp_facet].time - start_laser_time;

    //    printf("%i %i %i: dumb facet = %i, time = %lf; smart facet = %i, time = %lf\n", i, laser_frame, num_laser_triplets, facet_left, laser_time_left, temp_facet, temp_time);

  }

  fclose(lasertimefile);

  // exit(1);

  // logfile stuff

  start_camera_flag = FALSE;
  camframetimes = make_all_CR_Vector("frametimes", 5000, 0.0);

  camtimefile = fopen("calib_camera.txt", "w");

  logfile = fopen("cal2-001.log", "rb");

  bytes_per_camera_frame = 20;

  fseek(logfile, 0, SEEK_END);
  logfile_size = ftell(logfile);
  num_camera_frames = logfile_size / bytes_per_camera_frame;

  for (frame = 0; frame < num_camera_frames; frame++) {

    fseek(logfile, frame * bytes_per_camera_frame, SEEK_SET);

    //    printf("frame %i\n", frame);
    //    printf("%i ", frame);
    
    // time

    fread(&time_seconds, 4, 1, logfile);
    fread(&time_microseconds, 4, 1, logfile); 
    
    // video data 
    
    fread(&total_frames_written, 4, 1, logfile);
    fread(&total_incomplete_frames, 4, 1, logfile);
    
    // laser data 
    
    fread(&total_seo_facets_written, 4, 1, logfile);
    
    // stats

    total_time = ((double)(frame + 1) / 30);
    total_seo_frames_expected = (double)(((double)(frame + 1) / 30) * 20);
    total_seo_frames_received = ((double)total_seo_facets_written / 16);
    approximate_seo_performance = ((double)(total_seo_frames_received / total_seo_frames_expected) * 100);

    // report 

    this_time = time_seconds + 0.000001 * time_microseconds;

    if (!start_camera_flag) {
      start_camera_time = this_time;
      start_camera_flag = TRUE;
    }

    diff_time = this_time - start_camera_time;
    //    last_time = this_time;
    //    fprintf(camtimefile, "%lf %i %i", diff_time);
    camframetimes->x[frame] = diff_time;

    //    printf("secs = %i, microsecs = %i\n", time_seconds, time_microseconds);
//     printf("camera total frames = %i, dropped frames = %i\n", total_frames_written, total_incomplete_frames);
//     printf("laser total facets = %i\n", total_seo_facets_written);
//     
//     printf("time = %lf\n", total_time);
//     printf("laser expected = %lf, received = %lf\n", total_seo_frames_expected, total_seo_frames_received);
    //    printf("seo performance = %lf\n", approximate_seo_performance);

    //    printf("\n");

//     if (frame == 50)
//       exit(1);
  }

  for (i = 0; i < num_camera_triplets; i++) {

    fscanf(camerapointfile, "%i %i %i %i %i %i %i\n", &camera_frame, &camera_x_left, &camera_y_left, &camera_x_center, &camera_y_center, &camera_x_right, &camera_y_right);

    fprintf(camtimefile, "%i %lf %i %i\n%i %lf %i %i\n%i %lf %i %i\n", camera_frame, camframetimes->x[camera_frame], camera_x_left, camera_y_left, camera_frame, camframetimes->x[camera_frame], camera_x_center, camera_y_center, camera_frame, camframetimes->x[camera_frame], camera_x_right, camera_y_right);

  }

  fclose(camtimefile);

  exit(1);

  return im;
}

//----------------------------------------------------------------------------

void CalibrateLaserCamera_grab_CR_CG(CR_Image *im)
{

}

//----------------------------------------------------------------------------

void CalibrateLaserCamera_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  fclose(good_road_fp);
  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

int master_point;

//----------------------------------------------------------------------------

CR_Image *CalibrateLaserCamera2_init_CR_CG(char *name)
{
  CR_Image *im;
  char *filename;
  int i, j, k, w, h, laserpoint;
  double this_time, camera_timestamp;
  double temptime, tempx, tempy, tempr, tempx2, tempy2;
  int tempframe, tempframe2;

  im = make_CR_Image(720, 608, 0);

  laser_movie = read_demo3_laser_to_CR_Movie("C:\\InputData\\itg_July17_2001\\cal2-001.fw");
  laser_movie->currentframe = 0;
  get_frame_CR_Movie(laser_movie, laser_movie->currentframe);

  laser_image = make_CR_Image(laser_movie->im->w, laser_movie->im->h, 0);

  start_laser_time = laser_movie->laserim[0].navdat[0].time;
  
  camera_movie = open_CR_Movie("C:\\InputData\\itg_July17_2001\\xuv_laser_camera_calib.avi");
  camera_movie->currentframe = 0;
  get_frame_CR_Movie(camera_movie, camera_movie->currentframe);

  camera_image = make_CR_Image(camera_movie->im->w, camera_movie->im->h, 0);

  // logfile

  start_camera_flag = FALSE;
  camframetimes = make_CR_Vector("camframetimes", camera_movie->lastframe + 1);

  logfile = fopen("cal2-001.log", "rb");
  bytes_per_camera_frame = 20;
  fseek(logfile, 0, SEEK_END);
  logfile_size = ftell(logfile);
  num_camera_frames = logfile_size / bytes_per_camera_frame;

  for (i = 0; i <= camera_movie->lastframe; i++) {

    fseek(logfile, i * bytes_per_camera_frame, SEEK_SET);
    fread(&time_seconds, 4, 1, logfile);
    fread(&time_microseconds, 4, 1, logfile); 
    fread(&total_frames_written, 4, 1, logfile);
    fread(&total_incomplete_frames, 4, 1, logfile);
    
    this_time = time_seconds + 0.000001 * time_microseconds;
    
    if (!start_camera_flag) {
      start_camera_time = this_time;
      start_camera_flag = TRUE;
    }

    camera_timestamp = this_time - start_camera_time;
    camframetimes->x[i] = camera_timestamp;
  }

  // correspondences

  //  lasertimefile = fopen("calib_laser.txt", "r");
  //  camtimefile = fopen("calib_camera.txt", "r");
  masterfile = fopen("calib_new_clean_master.dat", "r");

  //  num_laser_points = count_lines(lasertimefile);
  //  num_camera_points = count_lines(camtimefile);
  num_master_points = count_lines(masterfile);

//   campoints = make_CR_Matrix("campoints", num_camera_points, 4);
//   for (i = 0; i < num_camera_points; i++) {
//     fscanf(camtimefile, "%i %lf %lf %lf\n", &tempframe, &temptime, &tempx, &tempy);
//     MATRC(campoints, i, 0) = (double) tempframe;
//     MATRC(campoints, i, 1) = temptime;
//     MATRC(campoints, i, 2) = tempx;
//     MATRC(campoints, i, 3) = tempy;
//   }
//   fclose(camtimefile);
// 
//   laserpoints = make_CR_Matrix("laserpoints", num_laser_points, 5);
//   for (i = 0; i < num_laser_points; i++) {
//     fscanf(lasertimefile, "%i %lf %lf %lf %lf\n", &tempframe, &temptime, &tempx, &tempy, &tempr);
//     MATRC(laserpoints, i, 0) = tempframe;
//     MATRC(laserpoints, i, 1) = temptime;
//     MATRC(laserpoints, i, 2) = tempx;
//     MATRC(laserpoints, i, 3) = tempy;
//     MATRC(laserpoints, i, 4) = tempr;
//   }
//   fclose(lasertimefile);

  masterpoints = make_CR_Matrix("masterpoints", num_master_points, 7);
  for (i = 0; i < num_master_points; i++) {
    fscanf(masterfile, "%i, %lf, %lf, %lf, %i, %lf, %lf\n", &tempframe, &tempx, &tempy, &tempr, &tempframe2, &tempx2, &tempy2);
    MATRC(masterpoints, i, 0) = tempframe;
    MATRC(masterpoints, i, 1) = tempx;
    MATRC(masterpoints, i, 2) = tempy;
    MATRC(masterpoints, i, 3) = tempr;
    MATRC(masterpoints, i, 4) = tempframe2;
    MATRC(masterpoints, i, 5) = tempx2;
    MATRC(masterpoints, i, 6) = tempy2;
  }
  fclose(masterfile);

  /*
  masterfile = fopen("calib_new_master.dat", "w");
  for (i = 0; i < num_laser_points; i += 3) {
    for (j = 0; j < 3; j++) {
      for (k = j, mincamtime = MATRC(campoints, j, 1), mindifftime = fabs(MATRC(laserpoints, i + j, 1) - mincamtime), minindex = j; k < num_camera_points; k += 3) {
	if (fabs(MATRC(laserpoints, i + j, 1) - MATRC(campoints, k, 1)) < mindifftime) {
	  mincamtime = MATRC(campoints, k, 1);
	  mindifftime = fabs(MATRC(laserpoints, i + j, 1) - mincamtime);
	  minindex = k;
	}
      }
      if (mindifftime <= 0.1)   // threshold on synchronicity of laser and camera frames
	fprintf(masterfile, "%i, %i, %i, %i, %i, %i, %i\n", 
		(int) MATRC(laserpoints, i + j, 0), (int) MATRC(laserpoints, i + j, 2), (int) MATRC(laserpoints, i + j, 3), (int) MATRC(laserpoints, i + j, 4), 
		(int) MATRC(campoints, minindex, 0), (int) MATRC(campoints, minindex, 2), (int) MATRC(campoints, minindex, 3));
    }
  }
  fclose(masterfile);

  exit(1);
  */

  master_point = 0;

  //  clean_masterfile = fopen("calib_new_clean_master.dat", "w");

  // finish

  return im;
}

//----------------------------------------------------------------------------

void CalibrateLaserCamera2_grab_CR_CG(CR_Image *im)
{
  static char laser_frame[80], camera_frame[80], laser_time[80], camera_time[80];
  double laser_timestamp;
  char c;

  // goto next laser image

  laser_movie->currentframe = MATRC(masterpoints, master_point, 0);
  if (laser_movie->currentframe < laser_movie->lastframe) 
    get_frame_CR_Movie(laser_movie, laser_movie->currentframe);

  // draw laser image

  glPixelZoom(4, 4);
  glRasterPos2i(0, 480);
  
  vflip_CR_Image(laser_movie->im, laser_image);
  glDrawPixels(laser_image->w, laser_image->h, GL_RGB, GL_UNSIGNED_BYTE, laser_image->data);

  sprintf(laser_frame, "%i", laser_movie->currentframe);
  ogl_draw_string(laser_frame, 10, 500, 255, 255, 0); 

  laser_timestamp = 0.001 * (laser_movie->laserim[laser_movie->currentframe].navdat[0].time - start_laser_time);
  sprintf(laser_time, "%.3lf", laser_timestamp);
  ogl_draw_string(laser_time, 10, 488, 255, 255, 0); 

  // sync camera image with laser image

  camera_movie->currentframe = MATRC(masterpoints, master_point, 4);
  if (camera_movie->currentframe < camera_movie->lastframe) 
    get_frame_CR_Movie(camera_movie, camera_movie->currentframe);

  /*  
  for ( ; camera_movie->currentframe < camera_movie->lastframe && camframetimes->x[camera_movie->currentframe] < laser_timestamp; camera_movie->currentframe++);
  if (camera_movie->currentframe > 0 &&
      fabs(camframetimes->x[camera_movie->currentframe] - laser_timestamp) > 
      fabs(camframetimes->x[camera_movie->currentframe - 1] - laser_timestamp))
    camera_movie->currentframe = camera_movie->currentframe - 1;
  get_frame_CR_Movie(camera_movie, camera_movie->currentframe);
  */

  // draw camera image

  glPixelZoom(1, 1);
  glRasterPos2i(0, 0);

  vflip_CR_Image(camera_movie->im, camera_image);
  glDrawPixels(camera_image->w, camera_image->h, GL_RGB, GL_UNSIGNED_BYTE, camera_image->data);

  sprintf(camera_frame, "%i", camera_movie->currentframe);
  ogl_draw_string(camera_frame, 10, 20, 255, 255, 0); 

  sprintf(camera_time, "%.3lf", camframetimes->x[camera_movie->currentframe]);
  ogl_draw_string(camera_time, 10, 8, 255, 255, 0); 

  // draw correspondences

  ogl_draw_line(4 * MATRC(masterpoints, master_point, 1), im->h - 4 * MATRC(masterpoints, master_point, 2), MATRC(masterpoints, master_point, 5), im->h - 128 - MATRC(masterpoints, master_point, 6), 255, 0, 0);
  ogl_draw_line(4 * MATRC(masterpoints, master_point + 1, 1), im->h - 4 * MATRC(masterpoints, master_point + 1, 2), MATRC(masterpoints, master_point + 1, 5), im->h - 128 - MATRC(masterpoints, master_point + 1, 6), 0, 255, 0);
  ogl_draw_line(4 * MATRC(masterpoints, master_point + 2, 1), im->h - 4 * MATRC(masterpoints, master_point + 2, 2), MATRC(masterpoints, master_point + 2, 5), im->h - 128 - MATRC(masterpoints, master_point + 2, 6), 0, 0, 255);

  /*
  if (accept_flag) {
    accept_flag = FALSE;
    if (accept_correspondences) {
      printf("%i\n", master_point);
      fprintf(clean_masterfile, "%i, %i, %i, %i, %i, %i, %i\n", 
	      (int) MATRC(masterpoints, master_point, 0), (int) MATRC(masterpoints, master_point, 1),
	      (int) MATRC(masterpoints, master_point, 2), (int) MATRC(masterpoints, master_point, 3),
	      (int) MATRC(masterpoints, master_point, 4), (int) MATRC(masterpoints, master_point, 5),
	      (int) MATRC(masterpoints, master_point, 6));
      fprintf(clean_masterfile, "%i, %i, %i, %i, %i, %i, %i\n", 
	      (int) MATRC(masterpoints, master_point + 1, 0), (int) MATRC(masterpoints, master_point + 1, 1),
	      (int) MATRC(masterpoints, master_point + 1, 2), (int) MATRC(masterpoints, master_point + 1, 3),
	      (int) MATRC(masterpoints, master_point + 1, 4), (int) MATRC(masterpoints, master_point + 1, 5),
	      (int) MATRC(masterpoints, master_point + 1, 6));
      fprintf(clean_masterfile, "%i, %i, %i, %i, %i, %i, %i\n", 
	      (int) MATRC(masterpoints, master_point + 2, 0), (int) MATRC(masterpoints, master_point + 2, 1),
	      (int) MATRC(masterpoints, master_point + 2, 2), (int) MATRC(masterpoints, master_point + 2, 3),
	      (int) MATRC(masterpoints, master_point + 2, 4), (int) MATRC(masterpoints, master_point + 2, 5),
	      (int) MATRC(masterpoints, master_point + 2, 6));
      fflush(clean_masterfile);  
    }
  }
  */

  // advance 

  if (master_point < num_master_points)
    master_point += 3;
  /*
  else {
    fclose(clean_masterfile);
    exit(1);
  }
  */

  //  if (laser_movie->currentframe < laser_movie->lastframe) 
  //    get_frame_CR_Movie(laser_movie, laser_movie->currentframe++);

  // advance camera image

//   if (camera_movie->currentframe < camera_movie->lastframe) 
//     get_frame_CR_Movie(camera_movie, camera_movie->currentframe++);
}

//----------------------------------------------------------------------------

void CalibrateLaserCamera2_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  fclose(good_road_fp);
  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Matrix *Q, *Qsqrt;  // covariance of process noise and its square root
double Qalpha;         // coefficient of Gaussian process noise

CR_Vector *mean;       // mean of Gaussian that initially describes prior p(x)
CR_Matrix *Cov, *Covsqrt;// covariance of Gaussian that initially describes p(x) and its square root
double Covalpha;       // coefficient of Gaussian prior

CR_Vector *prior_samp, *state_samp_zero_mean, *state_samp;
double texflow_length;

//----------------------------------------------------------------------------

double grad_norm(int x, int y, CR_Image *im)
{
  double grad_x, grad_y;

  grad_x = IMXY_I(im, x + 1, y) - IMXY_I(im, x, y);
  grad_y = IMXY_I(im, x, y + 1) - IMXY_I(im, x, y);

  return sqrt((double) (grad_x * grad_x + grad_y * grad_y));
}

//----------------------------------------------------------------------------

// stays away from edges to avoid off-image differences

CR_Vector *histogram_grad_norm_CR_Image(int bins, double lower, double upper, CR_Image *im)
{
  int x, y, margin, i;
  double gnorm, n, bin_size, total_gnorm, mean_gnorm;
  CR_Vector *histogram;

  margin = 10;

  histogram = make_all_CR_Vector("hist", bins, 0.0);
  bin_size = (upper - lower) / (double) bins;

  printf("binsize = %lf\n", bin_size);

  // fill bins

  for (y = margin, n = 0, total_gnorm = 0; y < im->h - margin; y++)
    for (x = margin; x < im->w - margin; x++) {
      gnorm = grad_norm(x, y, im);
      total_gnorm += gnorm;
      //      printf("gnorm = %lf\n", gnorm);
      if (gnorm < lower)
	histogram->x[0]++;
      else if (gnorm > upper)
	histogram->x[bins - 1]++;
      else {
	//	printf("filling bin %lf\n", (gnorm - lower) / bin_size);
	histogram->x[(int) CR_round((gnorm - lower) / bin_size)]++;
      }
      n++;
    }

  // normalize histogram

  mean_gnorm = total_gnorm / n;
  printf("mean gnorm = %lf\n", mean_gnorm);
  //  for (i = 0; i < bins; i++)
  //    histogram->x[i] = exp(-(double) i / mean_gnorm);
  scale_CR_Vector(1 / n, histogram);

  // finish

  return histogram;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void TextureFlow_init_samplers(CR_Image *im)
{
  double angular_variance;
  double prior_x_variance, prior_y_variance, prior_theta_variance;

  angular_variance = SQUARE(0.1);
  //angular_variance = SQUARE(0.05);

  prior_x_variance = 4 * im->w;
  prior_y_variance = 5;
  prior_theta_variance = SQUARE(PI / 6);
  //prior_theta_variance = SQUARE(PI / 10);

  texflow_length = 1.0;

  // initialize Q

  Q = init_CR_Matrix("Q", 1, 1, angular_variance);
  Qsqrt = copy_CR_Matrix(Q);
  square_root_CR_Matrix(Q, Qsqrt);
  Qalpha = gaussian_alpha_CR_Matrix(Q);

  // initialize mean, Cov

  mean = init_CR_Vector("mean", 3, (double) im->w / 2, 0.9 * (double) im->h, PI / 2);
  Cov = init_CR_Matrix("Cov", 3, 3, prior_x_variance, 0.0, 0.0, 0.0, prior_y_variance, 0.0, 0.0, 0.0, prior_theta_variance);
  Covsqrt = copy_CR_Matrix(Cov);
  square_root_CR_Matrix(Cov, Covsqrt);
  Covalpha = gaussian_alpha_CR_Matrix(Cov);

  prior_samp = make_CR_Vector("prior_samp", 3);

  state_samp_zero_mean = init_CR_Vector("state_samp_zero_mean", 1, 0.0);
  state_samp = make_CR_Vector("state_samp", 1);

//   print_CR_Matrix(Q);
  print_CR_Vector(mean);
  print_CR_Matrix(Cov);
//   exit(1);
}

//----------------------------------------------------------------------------

void TextureFlow_samp_prior(CR_Vector *samp)
{
  double x_diff, y_diff, x, y, a;

  // pick a random position and direction...

  sample_gaussian_CR_Matrix(Covalpha, prior_samp, mean, Covsqrt);
  a = prior_samp->x[2];

  // ...and make it into a vector

  x_diff = 1;
  y_diff = 0;

  x =  x_diff * cos(a) + y_diff * sin(a);
  y = -x_diff * sin(a) + y_diff * cos(a);

  samp->x[0] = prior_samp->x[0] + x;
  samp->x[1] = prior_samp->x[1] + y;
  samp->x[2] = prior_samp->x[0];
  samp->x[3] = prior_samp->x[1];
}

//----------------------------------------------------------------------------

void TextureFlow_samp_state(CR_Vector *old_samp, CR_Vector *new_samp)
{
  double x_diff, y_diff, x, y, a;

  // pick a random rotation...

  sample_gaussian_CR_Matrix(Qalpha, state_samp, state_samp_zero_mean, Qsqrt);
  a = state_samp->x[0];

  // ...and rotate the old sample by it to get the new one

  x_diff = old_samp->x[0] - old_samp->x[2];
  y_diff = old_samp->x[1] - old_samp->x[3];

  x =  x_diff * cos(a) + y_diff * sin(a);
  y = -x_diff * sin(a) + y_diff * cos(a);

  new_samp->x[0] = old_samp->x[0] + x;
  new_samp->x[1] = old_samp->x[1] + y;
  new_samp->x[2] = old_samp->x[0];
  new_samp->x[3] = old_samp->x[1];
}

//----------------------------------------------------------------------------

double TextureFlow_condprob_zx(void *Z, CR_Vector *samp)
{
  CR_Image *im;
  double x, y;
  int i, j, grad_x, grad_y;
  double grad_theta, grad_mag, samp_theta;

  im = (CR_Image *) Z;

  x = samp->x[0];
  y = samp->x[1];

  // make sure sample is in image

  if (x < 0 || x >= im->w || y < 0 || y >= im->h)
    return 0.0;

  else {

    // compute gradient magnitude, angle at sample position

    i = (int) CR_round(x);
    j = (int) CR_round(y);

    // Sobel-style

    /*
    grad_x = 
      -IMXY_I(im, i - 1, j - 1) +     IMXY_I(im, i + 1, j - 1) +
      -2 * IMXY_I(im, i - 1, j)     + 2 * IMXY_I(im, i + 1, j) +
      -IMXY_I(im, i - 1, j + 1) +     IMXY_I(im, i + 1, j + 1);
 
    grad_y = 
      IMXY_I(im, i - 1, j - 1) + 2 * IMXY_I(im, i, j - 1) + IMXY_I(im, i + 1, j - 1) +
      -IMXY_I(im, i - 1, j + 1) - 2 * IMXY_I(im, i, j + 1) - IMXY_I(im, i + 1, j + 1);
    */

    // Standard gradient

    grad_x = IMXY_I(im, i + 1, j) - IMXY_I(im, i, j);
    grad_y = IMXY_I(im, i, j + 1) - IMXY_I(im, i, j);

    grad_mag = sqrt((double) (grad_x * grad_x + grad_y * grad_y));
    grad_theta = atan2(grad_y, grad_x);

    // finish

    samp_theta = atan2(samp->x[1] - samp->x[3], samp->x[0] - samp->x[2]);

    //    return exp(-fabs(samp_theta - grad_theta - PI / 2) / grad_mag);

    //    return exp(-fabs(samp_theta - grad_theta + PI / 2));
    //    return exp(-fabs(samp_theta - grad_theta + PI / 2) / (1 + grad_mag));
    //    return exp(-fabs(samp_theta - grad_theta + PI / 2)) / exp(-grad_mag / 22.4);

    //    return exp(-fabs(samp_theta - grad_theta + PI / 2) / grad_mag) / exp(-grad_mag / 22.4);
    //    return exp(-fabs(samp_theta - grad_theta + PI / 2) / grad_mag);

    return gaussian_CR_Random(fabs(samp_theta - grad_theta + PI / 2), 0, 1 / grad_mag);
    //    return 
    //      gaussian_CR_Random(fabs(samp_theta - grad_theta + PI / 2), 0, 1.36 * 1.36 / grad_mag) /
    //      exp(-grad_mag / 22.4);

    //   return 1.9;

    //    return exp(-1 / (1 + grad_mag));

    //    return 0.5;
  }
}

//----------------------------------------------------------------------------

CR_Image *TextureFlow_init_CR_CG(char *name)
{
  CR_Image *im;
  char *filename;
  int i, j, w, h;
  CR_Vector *grad_norm_hist;
  FILE *hist_fp;

  //  texflowim = read_CR_Image("circ.ppm");

  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_1.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_2.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_3.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_4.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_5.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_6.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_7.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_8.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_9.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_10.ppm");

  flippedtexflowim = copy_CR_Image(texflowim);
  vflip_CR_Image(texflowim, flippedtexflowim);

  im = make_CR_Image(texflowim->w, texflowim->h, 0);

  TextureFlow_init_samplers(im);
  texflowcon = load_CR_Condensation("texflow.con", TextureFlow_condprob_zx, TextureFlow_samp_prior, TextureFlow_samp_state);

  grad_norm_hist = histogram_grad_norm_CR_Image(360, 0.0, 360.0, texflowim);
  //  hist_fp = fopen("hist.txt", "w");
  //  fprint_CR_Vector(hist_fp, grad_norm_hist);
  //  fclose(hist_fp);
  write_excel_CR_Vector("hist.txt", grad_norm_hist);
  //  exit(1);

  //  print_CR_Condensation(texflowcon);
  //  exit(1);

  // finish

  return im;
}

void TextureFlow_grab_CR_CG(CR_Image *im)
{
  int i, j;

  // draw image

  glDrawPixels(flippedtexflowim->w, flippedtexflowim->h, GL_RGB, GL_UNSIGNED_BYTE, flippedtexflowim->data);

  // update particles 

  update_CR_Condensation(texflowim, texflowcon);

  // draw particles

  for (i = 0; i < texflowcon->n; i++) {
    ogl_draw_point(texflowcon->s[i]->x[2], im->h - 1 - texflowcon->s[i]->x[3], 2, 255, 255, 0);
    ogl_draw_line(texflowcon->s[i]->x[2], im->h - 1 - texflowcon->s[i]->x[3],
		  texflowcon->s[i]->x[0], im->h - 1 - texflowcon->s[i]->x[1],
		  255, 255, 0);
  }

  // draw mean state

  ogl_draw_point(texflowcon->x->x[2], im->h - 1 - texflowcon->x->x[3], 5, 255, 0, 0);

//   ogl_draw_line(texflowcon->x->x[2], im->h - 1 - texflowcon->x->x[3],
// 		texflowcon->x->x[0], im->h - 1 - texflowcon->x->x[1],
// 		0, 255, 0);

}

//----------------------------------------------------------------------------

void TextureFlow_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  fclose(good_road_fp);
  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

double texflow_combradius;
int texflow_left, texflow_right, texflow_top, texflow_bottom;
CR_Vector *mean_x, *mean_y;
CR_Vector **shot_x, **shot_y;
int max_means, num_means, means_full, max_shots, num_shots;

//----------------------------------------------------------------------------

void TextureFlow2_init_samplers(CR_Image *im)
{
  double angular_variance;
  double prior_x_variance, prior_y_variance, prior_theta_variance;

  //  angular_variance = SQUARE(0.1);
  angular_variance = SQUARE(0.1);

  prior_x_variance = 4 * im->w;
  prior_y_variance = 5;
  prior_theta_variance = SQUARE(PI / 6);

  texflow_length = 1.0;

  texflow_combradius = 10;   // 10

  texflow_left = 10;
  texflow_top = 10;
  texflow_right = im->w - 10;
  texflow_bottom = im->h - 10;

  // initialize Q

  Q = init_CR_Matrix("Q", 1, 1, angular_variance);
  Qsqrt = copy_CR_Matrix(Q);
  square_root_CR_Matrix(Q, Qsqrt);
  Qalpha = gaussian_alpha_CR_Matrix(Q);

  // initialize mean, Cov

  mean = init_CR_Vector("mean", 3, (double) im->w / 2, 0.9 * (double) im->h, PI / 2);
  Cov = init_CR_Matrix("Cov", 3, 3, prior_x_variance, 0.0, 0.0, 0.0, prior_y_variance, 0.0, 0.0, 0.0, prior_theta_variance);
  Covsqrt = copy_CR_Matrix(Cov);
  square_root_CR_Matrix(Cov, Covsqrt);
  Covalpha = gaussian_alpha_CR_Matrix(Cov);

  prior_samp = make_CR_Vector("prior_samp", 3);

  state_samp_zero_mean = init_CR_Vector("state_samp_zero_mean", 1, 0.0);
  state_samp = make_CR_Vector("state_samp", 1);

//   print_CR_Matrix(Q);
  print_CR_Vector(mean);
  print_CR_Matrix(Cov);
//   exit(1);
}

//----------------------------------------------------------------------------

void TextureFlow2_samp_prior(CR_Vector *samp)
{
  double x_diff, y_diff, x, y, a;

  // pick a random position and direction...

  sample_gaussian_CR_Matrix(Covalpha, prior_samp, mean, Covsqrt);
  a = prior_samp->x[2];

  // ...and make it into a vector

  x_diff = 1;
  y_diff = 0;

  x =  x_diff * cos(a) + y_diff * sin(a);
  y = -x_diff * sin(a) + y_diff * cos(a);

  samp->x[0] = prior_samp->x[0] + x;
  samp->x[1] = prior_samp->x[1] + y;
  samp->x[2] = prior_samp->x[0];
  samp->x[3] = prior_samp->x[1];
}

//----------------------------------------------------------------------------

void TextureFlow2_samp_state(CR_Vector *old_samp, CR_Vector *new_samp)
{
  double x_diff, y_diff, x, y, a;

  // pick a random rotation...

  sample_gaussian_CR_Matrix(Qalpha, state_samp, state_samp_zero_mean, Qsqrt);
  a = state_samp->x[0];

  // ...and rotate the old sample by it to get the new one

  x_diff = old_samp->x[0] - old_samp->x[2];
  y_diff = old_samp->x[1] - old_samp->x[3];

  x =  x_diff * cos(a) + y_diff * sin(a);
  y = -x_diff * sin(a) + y_diff * cos(a);

  new_samp->x[0] = old_samp->x[0] + x;
  new_samp->x[1] = old_samp->x[1] + y;
  new_samp->x[2] = old_samp->x[0];
  new_samp->x[3] = old_samp->x[1];
}

//----------------------------------------------------------------------------

//  x =  y_diff;
//  y = -x_diff

double TextureFlow2_condprob_zx(void *Z, CR_Vector *samp)
{
  CR_Image *im;
  int x, y, grad_x, grad_y, xminus, yminus, xplus, yplus, part_in;
  double grad_theta, grad_mag, samp_theta, xorth, yorth, mean_theta_diff, mean_mag;

  im = (CR_Image *) Z;

  xorth = samp->x[1] - samp->x[3] / texflow_length;
  yorth = -(samp->x[0] - samp->x[2]) / texflow_length;

  xminus = (int) CR_round(samp->x[0] - texflow_combradius * xorth);
  yminus = (int) CR_round(samp->x[1] - texflow_combradius * yorth);

  xplus = (int) CR_round(samp->x[0] + texflow_combradius * xorth);
  yplus = (int) CR_round(samp->x[1] + texflow_combradius * yorth);

  // make sure sample is in image

  part_in = clip_line_to_rect_CR_Image(&xminus, &yminus, &xplus, &yplus, texflow_left, texflow_top, texflow_right, texflow_bottom);

  if (!part_in)
    return 0.0;

  else {

    samp_theta = atan2(samp->x[1] - samp->x[3], samp->x[0] - samp->x[2]);

    mean_gradient_along_line_CR_Image(xminus, yminus, xplus, yplus, im, samp_theta, &mean_theta_diff, &mean_mag);

    // compute gradient magnitude, angle along "comb" line orthogonal to sample

//     grad_x = IMXY_I(im, x + 1, y) - IMXY_I(im, x, y);
//     grad_y = IMXY_I(im, x, y + 1) - IMXY_I(im, x, y);
// 
//     grad_mag = sqrt((double) (grad_x * grad_x + grad_y * grad_y));
//     grad_theta = atan2(grad_y, grad_x);

    // finish

    //    return gaussian_CR_Random(fabs(samp_theta - grad_theta + PI / 2), 0, 1 / grad_mag);

    return gaussian_CR_Random(mean_theta_diff, 0, 1 / mean_mag);
    //    return gaussian_CR_Random(mean_theta_diff, 0, 1);

  }
}

//----------------------------------------------------------------------------

void TextureFlow2_draw_particles(CR_Condensation *con, CR_Image *im)
{
  int i;
  int xminus, yminus, xplus, yplus;
  double xorth, yorth;

  for (i = 0; i < texflowcon->n; i++) {
    
    xorth = texflowcon->s[i]->x[1] - texflowcon->s[i]->x[3] / texflow_length;
    yorth = -(texflowcon->s[i]->x[0] - texflowcon->s[i]->x[2]) / texflow_length;
    
    xminus = (int) CR_round(texflowcon->s[i]->x[0] - texflow_combradius * xorth);
    yminus = (int) CR_round(texflowcon->s[i]->x[1] - texflow_combradius * yorth);

    xplus = (int) CR_round(texflowcon->s[i]->x[0] + texflow_combradius * xorth);
    yplus = (int) CR_round(texflowcon->s[i]->x[1] + texflow_combradius * yorth);

    ogl_draw_point(texflowcon->s[i]->x[2], im->h - 1 - texflowcon->s[i]->x[3], 2, 255, 255, 0);
    ogl_draw_line(texflowcon->s[i]->x[2], im->h - 1 - texflowcon->s[i]->x[3],
		  texflowcon->s[i]->x[0], im->h - 1 - texflowcon->s[i]->x[1],
		  255, 255, 0);
    ogl_draw_line(xminus, im->h - 1 - yminus,
		  xplus, im->h - 1 - yplus,
		  0, 0, 255);

  }
}

//----------------------------------------------------------------------------

CR_Image *TextureFlow2_init_CR_CG(char *name)
{
  CR_Image *im;
  char *filename;
  int i, j, w, h;
  CR_Vector *grad_norm_hist;
  FILE *hist_fp;

  //  texflowim = read_CR_Image("circ.ppm");

  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\big4.ppm");

  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_1.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_2.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_3.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_4.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_5.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_6.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_7.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_8.ppm");
  //texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_9.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_10.ppm");

  flippedtexflowim = copy_CR_Image(texflowim);
  vflip_CR_Image(texflowim, flippedtexflowim);

  im = make_CR_Image(texflowim->w, texflowim->h, 0);

  TextureFlow2_init_samplers(im);
  texflowcon = load_CR_Condensation("texflow2.con", TextureFlow2_condprob_zx, TextureFlow2_samp_prior, TextureFlow2_samp_state);

  max_shots = 100;
  max_means = 100;
  shot_x = (CR_Vector **) CR_calloc(max_shots, sizeof(CR_Vector *));
  shot_y = (CR_Vector **) CR_calloc(max_shots, sizeof(CR_Vector *));
  for (i = 0; i < max_shots; i++) {
    shot_x[i] = make_CR_Vector("shot_x", max_means);
    shot_y[i] = make_CR_Vector("shot_y", max_means);
  }
  num_means = num_shots = 0;

  //  grad_norm_hist = histogram_grad_norm_CR_Image(360, 0.0, 360.0, texflowim);
  //  hist_fp = fopen("hist.txt", "w");
  //  fprint_CR_Vector(hist_fp, grad_norm_hist);
  //  fclose(hist_fp);
  //  write_excel_CR_Vector("hist.txt", grad_norm_hist);
  //  exit(1);

  //  print_CR_Condensation(texflowcon);
  //  exit(1);

  // finish

  return im;
}

//----------------------------------------------------------------------------

int not_updated = TRUE;

void TextureFlow2_grab_CR_CG(CR_Image *im)
{
  int i, j;

  if (num_shots >= max_shots)
    return;

  // draw image

  glDrawPixels(flippedtexflowim->w, flippedtexflowim->h, GL_RGB, GL_UNSIGNED_BYTE, flippedtexflowim->data);

  if (not_updated) {
    TextureFlow2_draw_particles(texflowcon, im);
    not_updated = FALSE;
  }

  // update particles 

  update_CR_Condensation(texflowim, texflowcon);

  // draw particles

  TextureFlow2_draw_particles(texflowcon, im);

  // draw mean state

  shot_x[num_shots]->x[num_means] = texflowcon->x->x[0];
  shot_y[num_shots]->x[num_means] = im->h - 1 - texflowcon->x->x[1];
  num_means++;
//   num_means = (num_means + 1) % max_means;
//   if (num_means == max_means - 1)
//     means_full = TRUE;
  //    exit(1);

  for (j = 0; j <= num_shots; j++) {
    if (j < num_shots) {
      for (i = 0; i < max_means; i++)
	ogl_draw_point(shot_x[j]->x[i], shot_y[j]->x[i], 2, 255, 0, 0);
    }
    else {
      for (i = 0; i < num_means; i++)
	ogl_draw_point(shot_x[j]->x[i], shot_y[j]->x[i], 2, 255, 0, 0);
    }
  }

  if (num_means == max_means) {
    num_means = 0;
    num_shots++;
    for (i = 0; i < texflowcon->n; i++) {
      texflowcon->samp_prior(texflowcon->s[i]);
      copy_CR_Vector(texflowcon->s[i], texflowcon->s_new[i]);
    }    
  }

  //  ogl_draw_point(texflowcon->x->x[2], im->h - 1 - texflowcon->x->x[3], 5, 255, 0, 0);

//   ogl_draw_line(texflowcon->x->x[2], im->h - 1 - texflowcon->x->x[3],
// 		texflowcon->x->x[0], im->h - 1 - texflowcon->x->x[1],
// 		0, 255, 0);

}

//----------------------------------------------------------------------------

void TextureFlow2_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  fclose(good_road_fp);
  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

int texflow_stripes, non_stripe_params;
double prior_l_variance;
double texflow_startdistance;

//----------------------------------------------------------------------------

void TextureFlow3_init_samplers(CR_Image *im)
{
  double prior_x_variance, prior_y_variance, prior_theta_variance, q_theta_variance;
  int i;

  non_stripe_params = 2;

  prior_x_variance = 2.0 * (double) im->w;
  prior_y_variance = 0.2 * (double) im->h;
  //  prior_theta_variance = SQUARE(PI / 6);
  prior_theta_variance = SQUARE(0.01);
  //  q_theta_variance = SQUARE(0.01);
  //  prior_l_variance = 10.0;

  texflow_startdistance = 300.0;

  //  texflow_length = -texflow_startdistance;  // 1.0
  texflow_length = 1.0;  // 1.0

  texflow_combradius = 10;   // 10

  texflow_left = 10;
  texflow_top = 10;
  texflow_right = im->w - 10;
  texflow_bottom = im->h - 10;

  texflow_stripes = 5;

  // initialize Q

  Q = make_all_CR_Matrix("Q", non_stripe_params + texflow_stripes, non_stripe_params + texflow_stripes, 0.0);
//   MATRC(Q, 0, 0) = prior_x_variance;
//   MATRC(Q, 1, 1) = prior_y_variance;
  MATRC(Q, 0, 0) = 10;
  MATRC(Q, 1, 1) = 10;
  for (i = non_stripe_params; i < non_stripe_params + texflow_stripes; i++)
    MATRC(Q, i, i) = prior_theta_variance;

  Qsqrt = copy_CR_Matrix(Q);
  square_root_CR_Matrix(Q, Qsqrt);
  Qalpha = gaussian_alpha_CR_Matrix(Q);

  // initialize mean, Cov

  mean = make_CR_Vector("mean", non_stripe_params + texflow_stripes);
  mean->x[0] = 0.5 * (double) im->w;
  mean->x[1] = 0.2 * (double) im->h;
  //  mean->x[2] = 300.0;
  for (i = non_stripe_params; i < non_stripe_params + texflow_stripes; i++)
    mean->x[i] = 4 + 0.2 * (double) (i - non_stripe_params);

  Cov = make_all_CR_Matrix("Cov", non_stripe_params + texflow_stripes, non_stripe_params + texflow_stripes, 0.0);
  MATRC(Cov, 0, 0) = prior_x_variance;
  MATRC(Cov, 1, 1) = prior_y_variance;
  //  MATRC(Cov, 2, 2) = prior_l_variance;
  for (i = non_stripe_params; i < non_stripe_params + texflow_stripes; i++)
    MATRC(Cov, i, i) = prior_theta_variance;

  Covsqrt = copy_CR_Matrix(Cov);
  square_root_CR_Matrix(Cov, Covsqrt);
  Covalpha = gaussian_alpha_CR_Matrix(Cov);

  prior_samp = make_CR_Vector("prior_samp", non_stripe_params + texflow_stripes);

  state_samp_zero_mean = make_all_CR_Vector("state_samp_zero_mean", non_stripe_params + texflow_stripes, 0.0);
  state_samp = make_CR_Vector("state_samp", non_stripe_params + texflow_stripes);

//   print_CR_Matrix(Q);
  print_CR_Vector(mean);
  print_CR_Matrix(Cov);
//   exit(1);
}

//----------------------------------------------------------------------------

void TextureFlow3_samp_prior(CR_Vector *samp)
{
  double x_diff, y_diff, x, y, a;
  int i;

  // pick a random vanishing point position and set of directions...

  sample_gaussian_CR_Matrix(Covalpha, prior_samp, mean, Covsqrt);

//   printf("Cov samp\n");
//   print_CR_Vector(prior_samp);

  // ...and make it into a state vector

  for (i = 0; i < texflow_stripes; i++) {

    a = prior_samp->x[non_stripe_params + i];

    // x_i

    x_diff = texflow_startdistance;
    y_diff = 0;
    
    x =  x_diff * cos(a) + y_diff * sin(a);
    y = -x_diff * sin(a) + y_diff * cos(a);

    samp->x[4 * i] = prior_samp->x[0] + x;
    samp->x[4 * i + 1] = prior_samp->x[1] + y;

    // x_i-1

    x_diff = texflow_startdistance + texflow_length;
    y_diff = 0;
    
    x =  x_diff * cos(a) + y_diff * sin(a);
    y = -x_diff * sin(a) + y_diff * cos(a);

    samp->x[4 * i + 2] = prior_samp->x[0] + x;
    samp->x[4 * i + 3] = prior_samp->x[1] + y;
  }

//   printf("before\n");
//   print_CR_Vector(prior_samp);

  //  printf("* %lf, %lf\n", prior_samp->x[0], prior_samp->x[1]);

}

//----------------------------------------------------------------------------

void line_intersection(double Ax, double Ay, double Bx, double By, double Cx, double Cy, double Dx, double Dy, double *Px, double *Py)
{
  double r, s;
            
  r = ((Ay-Cy) * (Dx-Cx) - (Ax-Cx) * (Dy-Cy)) / ((Bx-Ax) * (Dy-Cy) - (By-Ay) * (Dx-Cx));
  s = ((Ay-Cy) * (Bx-Ax) - (Ax-Cx) * (By-Ay)) / ((Bx-Ax) * (Dy-Cy) - (By-Ay) * (Dx-Cx));

  *Px = Ax + r * (Bx - Ax);
  *Py = Ay + r * (By - Ay);
}

//----------------------------------------------------------------------------

void TextureFlow3_samp_state(CR_Vector *old_samp, CR_Vector *new_samp)
{
  double x_diff, y_diff, x, y, a, dist;
  int i;

  // derive vanishing point and directions for old_samp

  line_intersection(old_samp->x[0], old_samp->x[1], old_samp->x[2], old_samp->x[3],
		    old_samp->x[4 * (texflow_stripes - 1)], old_samp->x[4 * (texflow_stripes - 1) + 1], 
		    old_samp->x[4 * (texflow_stripes - 1) + 2], old_samp->x[4 * (texflow_stripes - 1) + 3],
		    &prior_samp->x[0], &prior_samp->x[1]);

  for (i = 0; i < texflow_stripes; i++) {
    if (old_samp->x[4 * i + 3] > old_samp->x[4 * i + 1])
      prior_samp->x[non_stripe_params + i] = 2 * PI - acos((old_samp->x[4 * i + 2] - old_samp->x[4 * i]) / texflow_length);
    else
      prior_samp->x[non_stripe_params + i] = acos((old_samp->x[4 * i + 2] - old_samp->x[4 * i]) / texflow_length);
  }

//   printf("after\n");
//   print_CR_Vector(prior_samp);

  //  printf("px = %lf, py = %lf\n", px, py);

  // ...

  // pick a random vanishing point offset and set of direction offsets

  sample_gaussian_CR_Matrix(Qalpha, state_samp, state_samp_zero_mean, Qsqrt);

//   print_CR_Vector(state_samp_zero_mean);
//   print_CR_Vector(state_samp);

  // add them together

  for (i = 0; i < non_stripe_params + texflow_stripes; i++) 
    state_samp->x[i] += prior_samp->x[i];

  // convert results into new_samp

  for (i = 0; i < texflow_stripes; i++) {
    
    a = state_samp->x[non_stripe_params + i];
    
    // x_i
    
    x_diff = texflow_startdistance;
    y_diff = 0;
    
    x =  x_diff * cos(a) + y_diff * sin(a);
    y = -x_diff * sin(a) + y_diff * cos(a);
    
    new_samp->x[4 * i] = state_samp->x[0] + x;
    new_samp->x[4 * i + 1] = state_samp->x[1] + y;
    
    // x_i-1

    x_diff = texflow_startdistance + texflow_length;
    y_diff = 0;
    
    x =  x_diff * cos(a) + y_diff * sin(a);
    y = -x_diff * sin(a) + y_diff * cos(a);

    new_samp->x[4 * i + 2] = state_samp->x[0] + x;
    new_samp->x[4 * i + 3] = state_samp->x[1] + y;
  }
}

//----------------------------------------------------------------------------

//  x =  y_diff;
//  y = -x_diff

double TextureFlow3_condprob_zx(void *Z, CR_Vector *samp)
{
  CR_Image *im;
  int x, y, grad_x, grad_y, part_in, i, x1, y1, x2, y2;
  double grad_theta, grad_mag, samp_theta, mean_theta_diff, mean_mag;

  im = (CR_Image *) Z;

  // make sure sample is in image

  for (i = 0, mean_theta_diff = 0.0, mean_mag = 0.0; i < texflow_stripes; i++) {
    
    x1 = samp->x[4 * i];
    y1 = samp->x[4 * i + 1];
    x2 = samp->x[4 * i + 2];
    y2 = samp->x[4 * i + 3];

    part_in = clip_line_to_rect_CR_Image(&x1, &y1, &x2, &y2, texflow_left, texflow_top, texflow_right, texflow_bottom);
    
    if (!part_in)
      return 0.0;

    else {

      samp_theta = atan2(samp->x[4 * i + 1] - samp->x[4 * i + 3], samp->x[4 * i] - samp->x[4 * i + 2]);
      
      x = (int) CR_round(samp->x[4 * i]);
      y = (int) CR_round(samp->x[4 * i + 1]);
      
      grad_x = IMXY_I(im, x + 1, y) - IMXY_I(im, x, y);
      grad_y = IMXY_I(im, x, y + 1) - IMXY_I(im, x, y);
      
      mean_mag += sqrt((double) (grad_x * grad_x + grad_y * grad_y));
      grad_theta = atan2(grad_y, grad_x);
      
      mean_theta_diff += fabs(samp_theta - grad_theta + PI / 2);
    }    
  }

  mean_theta_diff /= (double) texflow_stripes;
  mean_mag /= (double) texflow_stripes;
  
  return gaussian_CR_Random(mean_theta_diff, 0, 1 / mean_mag);

}

//----------------------------------------------------------------------------

void TextureFlow3_draw_particles(CR_Condensation *con, CR_Image *im)
{
  int i, j;
  double px, py;

  for (j = 0; j < con->n; j++) {

    line_intersection(con->s[j]->x[0], con->s[j]->x[1], con->s[j]->x[2], con->s[j]->x[3],
		      con->s[j]->x[4 * (texflow_stripes - 1)], con->s[j]->x[4 * (texflow_stripes - 1) + 1], 
		      con->s[j]->x[4 * (texflow_stripes - 1) + 2], con->s[j]->x[4 * (texflow_stripes - 1) + 3],
		      &px, &py);

    for (i = 0; i < texflow_stripes; i++) {
      ogl_draw_line(con->s[j]->x[4 * i], im->h - con->s[j]->x[4 * i + 1],
		    px, im->h - py,
		    0, 255, 0);
    }
  }
}

//----------------------------------------------------------------------------

CR_Image *TextureFlow3_init_CR_CG(char *name)
{
  CR_Image *im;
  char *filename;
  int i, j, w, h;
  CR_Vector *grad_norm_hist;
  FILE *hist_fp;

  //  texflowim = read_CR_Image("circ.ppm");

  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\big2.ppm");

  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_1.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_2.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_3.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_4.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_5.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_6.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_7.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_8.ppm");
  //texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_9.ppm");
  //  texflowim = read_CR_Image("C:\\InputData\\itg_July17_2001\\seq1_10.ppm");

  flippedtexflowim = copy_CR_Image(texflowim);
  vflip_CR_Image(texflowim, flippedtexflowim);

  im = make_CR_Image(texflowim->w, texflowim->h, 0);

  TextureFlow3_init_samplers(im);
  texflowcon = load_CR_Condensation("texflow3.con", TextureFlow3_condprob_zx, TextureFlow3_samp_prior, TextureFlow3_samp_state);

  /*
  max_shots = 100;
  max_means = 100;
  shot_x = (CR_Vector **) CR_calloc(max_shots, sizeof(CR_Vector *));
  shot_y = (CR_Vector **) CR_calloc(max_shots, sizeof(CR_Vector *));
  for (i = 0; i < max_shots; i++) {
    shot_x[i] = make_CR_Vector("shot_x", max_means);
    shot_y[i] = make_CR_Vector("shot_y", max_means);
  }
  num_means = num_shots = 0;
  */

  //  grad_norm_hist = histogram_grad_norm_CR_Image(360, 0.0, 360.0, texflowim);
  //  hist_fp = fopen("hist.txt", "w");
  //  fprint_CR_Vector(hist_fp, grad_norm_hist);
  //  fclose(hist_fp);
  //  write_excel_CR_Vector("hist.txt", grad_norm_hist);
  //  exit(1);

  //  print_CR_Condensation(texflowcon);
  //  exit(1);

  // finish

  return im;
}

//----------------------------------------------------------------------------

void TextureFlow3_grab_CR_CG(CR_Image *im)
{
  int i, j;

  //  if (num_shots >= max_shots)
  //    return;
  
  // draw image

  //  TextureFlow3_samp_prior(texflowcon->s[0]);
  
  glDrawPixels(flippedtexflowim->w, flippedtexflowim->h, GL_RGB, GL_UNSIGNED_BYTE, flippedtexflowim->data);

//   if (not_updated) {
//     TextureFlow3_draw_particles(texflowcon, im);
//     not_updated = FALSE;
//   }

  // update particles 

  update_CR_Condensation(texflowim, texflowcon);

  // draw particles

  TextureFlow3_draw_particles(texflowcon, im);

  // draw mean state


  //  print_CR_Vector(texflowcon->s[0]);
  //  printf("\n");

//   for (i = 0; i < texflow_stripes; i++) {
//     ogl_draw_line(texflowcon->s[0]->x[4 * i], im->h - texflowcon->s[0]->x[4 * i + 1],
// 		  texflowcon->s[0]->x[4 * i + 2], im->h - texflowcon->s[0]->x[4 * i + 3],
// 		  0, 255, 0);
//   }
}

//----------------------------------------------------------------------------

void TextureFlow3_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  fclose(good_road_fp);
  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void draw_road()
{
  int i;
  double road_width = 8.0;  // meters
  double lane_width = 0.20; // meters
  double x, r;

  // following black top

  glColor3ub(0, 0, 0);

  glBegin(GL_TRIANGLE_STRIP);

  //  r = 1;
  for (i = -1000; i < 1000; i++) {
    //    x = road_t + .02 * (double) i;
    x = road_t + .02 * (double) i;
    glVertex3d(road_r * cos(x) - road_r * cos(road_t) + 0.5 * M2R(road_width), M2R(i), M2R(0));
    glVertex3d(road_r * cos(x) - road_r * cos(road_t) - 0.5 * M2R(road_width), M2R(i), M2R(0));
    //    glVertex3d(r * (sin(x) + cos(x)) - r * (sin(road_t) + cos(road_t)) + 0.5 * M2R(road_width), M2R(i), M2R(0));
    //   glVertex3d(r * (sin(x) + cos(x)) - r * (sin(road_t) + cos(road_t)) - 0.5 * M2R(road_width), M2R(i), M2R(0));
  }

  glEnd();

  // sine lane line

  glBegin(GL_TRIANGLE_STRIP);

  for (i = -1000; i < 1000; i++) {
    if (!((i+road_b) % 8) || !((i+road_b-1) % 8) || !((i+road_b-2) % 8) || !((i+road_b-3) % 8)) {
      glColor3ub(255, 255, 0);
    x = road_t + .02 * (double) i;
    glVertex3d(road_r * cos(x) - road_r * cos(road_t) + 0.5 * M2R(lane_width), M2R(i), M2R(0));
    glVertex3d(road_r * cos(x) - road_r * cos(road_t) - 0.5 * M2R(lane_width), M2R(i), M2R(0));
    }
    else {
      glColor3ub(0, 0, 0);
    x = road_t + .02 * (double) i;
    glVertex3d(road_r * cos(x) - road_r * cos(road_t) + 0.5 * M2R(lane_width), M2R(i), M2R(0));
    glVertex3d(road_r * cos(x) - road_r * cos(road_t) - 0.5 * M2R(lane_width), M2R(i), M2R(0));
    }
  }

  glEnd();

  road_b++;

  // straight black top

  /*
  glColor3ub(0, 0, 0);

  glBegin(GL_TRIANGLE_STRIP);

  for (i = -1000; i < 1000; i++) {
    glVertex3d(0.5 * M2R(road_width), M2R(i), M2R(0));
    glVertex3d(-0.5 * M2R(road_width), M2R(i), M2R(0));
  }

  glEnd();

  // straight lane line

  glBegin(GL_TRIANGLE_STRIP);

  for (i = -1000; i < 1000; i++) {
    if (!((i+road_b) % 8) || !((i+road_b-1) % 8) || !((i+road_b-2) % 8) || !((i+road_b-3) % 8)) {
      glColor3ub(255, 255, 0);
      glVertex3d(0.5*M2R(lane_width),M2R(i), M2R(0));
      glVertex3d(-0.5*M2R(lane_width), M2R(i), M2R(0));
    }
    else {
      glColor3ub(0, 0, 0);
      glVertex3d(0.5*M2R(lane_width),M2R(i), M2R(0));
      glVertex3d(-0.5*M2R(lane_width), M2R(i), M2R(0));
    }
  }

  glEnd();

  road_b++;
  */

  /*
  // sine black top

  glColor3ub(0, 0, 0);

  glBegin(GL_TRIANGLE_STRIP);

  for (i = -1000; i < 1000; i++) {
    glVertex3d(cos(road_t + .02 * (double) i) * M2R(-road_width / 2)+0.5*M2R(road_width),M2R(i), M2R(0));
    glVertex3d(cos(road_t + .02 * (double) i) * M2R(-road_width / 2) -0.5*M2R(road_width), M2R(i), M2R(0));
  }

  glEnd();


  // sine lane line

  glBegin(GL_TRIANGLE_STRIP);

  for (i = -1000; i < 1000; i++) {
    if (!((i+road_b) % 8) || !((i+road_b-1) % 8) || !((i+road_b-2) % 8) || !((i+road_b-3) % 8)) {
      glColor3ub(255, 255, 0);
      glVertex3d(cos(road_t + .02 * (double) i) * M2R(-road_width / 2)+0.5*M2R(lane_width),M2R(i), M2R(0));
      glVertex3d(cos(road_t + .02 * (double) i) * M2R(-road_width / 2) -0.5*M2R(lane_width), M2R(i), M2R(0));
    }
    else {
      glColor3ub(0, 0, 0);
      glVertex3d(cos(road_t + .02 * (double) i) * M2R(-road_width / 2)+0.5*M2R(lane_width),M2R(i), M2R(0));
      glVertex3d(cos(road_t + .02 * (double) i) * M2R(-road_width / 2) -0.5*M2R(lane_width), M2R(i), M2R(0));
    }
  }

  glEnd();

  road_b++;
  */

  // t is meters along the road, so we're controlling speed here

  road_t += 0.0926;  // this is equivalent to going 10 km /hour at 30 frames per second
  //  road_t += road_r * (-sin(road_t));

//   if (road_r > 10)
//     road_r_delta = -road_r_delta;
  if (road_t > 3)
    road_r += road_r_delta;

  //  printf("road_t = %lf\n", road_t);

}

//----------------------------------------------------------------------------

CR_Image *Road1_init_CR_CG(char *name)
{
  CR_Image *im;

  //  im = make_CR_Image(640, 480, 0);
  im = make_CR_Image(640, 240, 0);
  //im = make_CR_Image(640, 480, 0);
  sprintf(name, "road1");

  glcircle = gluNewQuadric();
  gluQuadricDrawStyle(glcircle, GLU_FILL);
  //gluQuadricDrawStyle(glcircle, GLU_LINE);


  eye_radius = 1.0 + M2R(1.5);  
  //  eye_radius = 1.0 + M2R(50);  
  //  eye_radius = 1.00000027846;  // eyes of a 6'1" person (3" below top of head), assuming 1 = Earth rad.
  //  eye_altitude = DEG2RAD(14);         // 0 = tangential to surface, PI_2 = straight up, etc.
  eye_altitude = DEG2RAD(-10); // -10         // 0 = tangential to surface, PI_2 = straight up, etc.
  eye_roll = 0;                // 0 = right-side up, PI_2 = left-side up, etc.

  eye_latitude = 0;  // 0
  eye_longitude = 0;
  eye_azimuth = 0;           // local heading.  at (lat, long) = (0, 0), 0 = north and PI_2 = east

  horizon_dist = sqrt(eye_radius * eye_radius - earth_radius * earth_radius);
  disk_rad = earth_radius * horizon_dist / eye_radius;
  disk_dist = sqrt(horizon_dist * horizon_dist - disk_rad * disk_rad);

  //  glEnable(GL_POLYGON_SMOOTH);

  road_t = 0;
  road_b = 0;
  //  road_r = M2R(10);
  road_r = M2R(0);
  road_r_delta = M2R(0.04);

  return im;
}

//----------------------------------------------------------------------------

void Road1_grab_CR_CG(CR_Image *im)
{
  glViewport(0, 0, im->w, im->h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  // fovy, aspect, near, far
  gluPerspective(road_fovy, (double) im->w / (double) im->h, 0.00000001, 1000);  // 40
  glMatrixMode(GL_MODELVIEW);
  // eyex, eyey, eyez, centerx, centery, centerz, upx, upy, upz
  gluLookAt(0, 0, 0, 0, 0, 1, 0, 1, 0);

  //  glClearColor(0, 0, .5, 0);
  //  glClear(GL_COLOR_BUFFER_BIT);

  glPushMatrix();
  
  glRotatef(RAD2DEG(eye_altitude + PI_2), 1, 0, 0);
  glRotatef(RAD2DEG(eye_roll), 0, 1, 0);
  //  glRotatef(RAD2DEG(-eye_azimuth), 0, 0, 1);
  glRotatef(RAD2DEG(road_r * (-sin(road_t))), 0, 0, 1);
  //  glRotatef(RAD2DEG(M2R(2) * (cos(road_t) - sin(road_t))), 0, 0, 1);

  //  printf("%lf\n", road_r * (-sin(road_t)));

  glTranslatef(0, 0, disk_dist);

  glPushMatrix();

  glScalef(disk_rad, disk_rad, 1);

  glColor3f(0.545, 0.323, 0.169);
  // inner, outer, slices, loops
  gluDisk(glcircle, 0, 1, 50, 50);

  glPopMatrix();

  draw_road();

  //  glVertex3d(0, 0, 0);
  //  glVertex3d(0, -10, 0);

  glPopMatrix();

}

//----------------------------------------------------------------------------

void Road1_end_CR_CG(CR_Image *im)
{

}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

int cues_w = 50;
int cues_h = 50;
int cues_target_w = 10;
int cues_target_h = 10;
int cues_h_tiles = 1;
int cues_v_tiles = 1;
double cues_r_var = 1000;                           // Image noise for red color channel 1000
double cues_g_var = 1000;                           // Image noise for green color channel 1000
double cues_b_var = 1000;                           // Image noise for blue color channel 1000     
CR_Image *cues_background_im;                        // Background layer image
CR_Image *cues_target_im;                            // Target layer
CR_Image *cues_final_im, *cues_scratch_final_im;     // Background + target layer + noise
CR_Image *cues_split_im, *cues_scratch_split_im;
CR_Image *cues_r_im, *cues_g_im, *cues_b_im;
int cues_x_targ, cues_y_targ;
double cues_alpha;
CR_Matrix *cues_cov, *cues_invcov;
CR_Vector *cues_v, *cues_vmean;

//----------------------------------------------------------------------------

int targ_ssd, back_ssd;
double targ_p, back_p;

double Cues1_single_channel_log_likelihood_ratio(int, int, int, double, CR_Image *, CR_Image *, CR_Image *);

double Cues1_single_channel_maximum_log_likelihood_ratio_estimate(int *x, int *y, int channel, double noise_variance, CR_Image *im, CR_Image *target_im, CR_Image *background_im)
{
  int i, j, max_targ_ssd, max_back_ssd;
  double likelihood, max_likelihood, max_targ_p, max_back_p;

  max_likelihood = Cues1_single_channel_log_likelihood_ratio(0, 0, channel, noise_variance, im, target_im, background_im);
  max_targ_ssd = targ_ssd;
  max_back_ssd = back_ssd;
  max_targ_p = targ_p;
  max_back_p = back_p;
  *x = 0;
  *y = 0;
  for (i = 0; i <= im->w - target_im->w; i++) 
    for (j = 0; j <= im->h - target_im->h; j++) {
      likelihood = Cues1_single_channel_log_likelihood_ratio(i, j, channel, noise_variance, im, target_im, background_im);
      if (i == cues_x_targ && j == cues_y_targ)
	//	printf("t (channel %i) like = %lf, rat_ssd = %lf, targ_ssd = %i, back_ssd = %i, targ_p = %lf, back_p = %lf\n", channel, likelihood, (double) targ_ssd / (double) back_ssd, targ_ssd, back_ssd, targ_p, back_p);
      printf("true: like = %lf, ssd_ratio = %lf\n", likelihood, (double) targ_ssd / (double) back_ssd);
      if (likelihood > max_likelihood) {
	max_likelihood = likelihood;
	max_targ_ssd = targ_ssd;
	max_back_ssd = back_ssd;
	max_targ_p = targ_p;
	max_back_p = back_p;
	*x = i;
	*y = j;
      }
    }
  
  printf("max: like = %lf, ssd_ratio = %lf\n", max_likelihood, (double) max_targ_ssd / (double) max_back_ssd);
  //  printf("max like = %lf, x = %i, y = %i, rat_ssd = %lf, targ_ssd = %i, back_ssd = %i, targ_p = %lf, back_p = %lf\n", max_likelihood, *x, *y, (double) max_targ_ssd / (double) max_back_ssd, max_targ_ssd, max_back_ssd, max_targ_p, max_back_p);
  return max_likelihood;
}

//----------------------------------------------------------------------------

// assumes (x, y) and target_im dimensions are such that the target is entirely
// within the background image

// channel = 0 => red, channel = 1 => green, channel = 2 => blue

double Cues1_single_channel_log_likelihood_ratio(int x, int y, int channel, double noise_variance, CR_Image *im, CR_Image *target_im, CR_Image *background_im)
{
  int i, j, u, v;
  double p_target, p_background, log_likelihood_ratio, total_log_likelihood_ratio;

  targ_ssd = back_ssd = 0;
  targ_p = back_p = 1.0;
  total_log_likelihood_ratio = 0.0;
  for (i = x, u = 0; i < x + target_im->w; i++, u++)
    for (j = y, v = 0; j < y + target_im->h; j++, v++) {
      targ_ssd += fabs((unsigned char) IMXY_N(im, i, j, channel) - (unsigned char) IMXY_N(target_im, u, v, channel));
      back_ssd += fabs((unsigned char) IMXY_N(im, i, j, channel) - (unsigned char) IMXY_N(background_im, i, j, channel));
      p_target = gaussian_CR_Random((unsigned char) IMXY_N(im, i, j, channel), (unsigned char) IMXY_N(target_im, u, v, channel), noise_variance);
      p_background = gaussian_CR_Random((unsigned char) IMXY_N(im, i, j, channel), (unsigned char) IMXY_N(background_im, i, j, channel), noise_variance);
      targ_p *= p_target;
      back_p *= p_background;
      log_likelihood_ratio = log(p_target / p_background);
      total_log_likelihood_ratio += log_likelihood_ratio;
    }

  return total_log_likelihood_ratio;
}

//----------------------------------------------------------------------------

// maximum_likelihood for all channels (i.e., color)


double Cues1_color_log_likelihood_ratio(int, int, double, CR_Matrix *, CR_Image *, CR_Image *, CR_Image *);

double Cues1_color_maximum_log_likelihood_ratio_estimate(int *x, int *y, double alpha, CR_Matrix *invcov, CR_Image *im, CR_Image *target_im, CR_Image *background_im)
{
  int i, j;
  double likelihood, max_likelihood;

  max_likelihood = Cues1_color_log_likelihood_ratio(0, 0, alpha, invcov, im, target_im, background_im);
  *x = 0;
  *y = 0;
  for (i = 0; i <= im->w - target_im->w; i++) 
    for (j = 0; j <= im->h - target_im->h; j++) {
      likelihood = Cues1_color_log_likelihood_ratio(i, j, alpha, invcov, im, target_im, background_im);
      if (likelihood > max_likelihood) {
	max_likelihood = likelihood;
	*x = i;
	*y = j;
      }
    }
  
  return max_likelihood;
}

//----------------------------------------------------------------------------

// gaussian_CR_Matrix(alpha1, Tval->vrc[j][i], Mean1, InvCov1)
// double gaussian_CR_Matrix(double alpha, CR_Vector *V, CR_Vector *VMean, CR_Matrix *MInvCov)

// likelihood_ratio for all channels (i.e., color)

double Cues1_color_log_likelihood_ratio(int x, int y, double alpha, CR_Matrix *invcov, CR_Image *im, CR_Image *target_im, CR_Image *background_im)
{
  int i, j, u, v;
  double p_target, p_background, log_likelihood_ratio, total_log_likelihood_ratio;

  total_log_likelihood_ratio = 0.0;
  for (i = x, u = 0; i < x + target_im->w; i++, u++)
    for (j = y, v = 0; j < y + target_im->h; j++, v++) {

      // set up V from im

      cues_v->x[0] = (unsigned char) IMXY_R(im, i, j);
      cues_v->x[1] = (unsigned char) IMXY_G(im, i, j);
      cues_v->x[2] = (unsigned char) IMXY_B(im, i, j);

      // set up VMean from target

      cues_vmean->x[0] = (unsigned char) IMXY_R(target_im, u, v);
      cues_vmean->x[1] = (unsigned char) IMXY_G(target_im, u, v);
      cues_vmean->x[2] = (unsigned char) IMXY_B(target_im, u, v);

      // target likelihood

      p_target = gaussian_CR_Matrix(alpha, cues_v, cues_vmean, invcov);

      // set up VMean from background

      cues_vmean->x[0] = (unsigned char) IMXY_R(background_im, i, j);
      cues_vmean->x[1] = (unsigned char) IMXY_G(background_im, i, j);
      cues_vmean->x[2] = (unsigned char) IMXY_B(background_im, i, j);

      // background likelihood

      p_background = gaussian_CR_Matrix(alpha, cues_v, cues_vmean, invcov);

      log_likelihood_ratio = log(p_target / p_background);
      total_log_likelihood_ratio += log_likelihood_ratio;
    }

  return total_log_likelihood_ratio;
}

//----------------------------------------------------------------------------

// create grid of randomly-colored tiles
// color values are chosen uniformly and independently on [0, 255] for each channel

CR_Image *create_random_tiled_CR_Image(int w, int h, int h_tiles, int v_tiles)
{
  int i, j, u, v, tile_w, tile_h, r, g, b;
  CR_Image *im;
  
  if (w % h_tiles || h % v_tiles)
    CR_error("create_random_tiled_CR_Image(): number of tiles per dimension should evenly divide length");

  im = make_CR_Image(w, h, 0);

  tile_w = w / h_tiles;
  tile_h = h / v_tiles;

  for (u = 0; u < h_tiles; u++)
    for (v = 0; v < v_tiles; v++) {

      r = ranged_uniform_int_CR_Random(0, 255);
      g = ranged_uniform_int_CR_Random(0, 255);
      b = ranged_uniform_int_CR_Random(0, 255);

      for (i = u * tile_w; i < (u + 1) * tile_w; i++)
	for (j = v * tile_h; j < (v + 1) * tile_h; j++) {

	  IMXY_R(im, i, j) = (unsigned char) r;
	  IMXY_G(im, i, j) = (unsigned char) g;
	  IMXY_B(im, i, j) = (unsigned char) b;
	}
    }

  return im;
}

//----------------------------------------------------------------------------

// add gaussian noise to each pixel in im
// clamps to [0, 255]

void ip_noisify_CR_Image(CR_Image *im, double r_var, double g_var, double b_var)
{
  int i, j, r, g, b;

  for (i = 0; i < im->w; i++)
    for (j = 0; j < im->h; j++) {

      r = CR_clamp(normal_sample_CR_Random((unsigned char) IMXY_R(im, i, j), r_var), 0, 255);
      g = CR_clamp(normal_sample_CR_Random((unsigned char) IMXY_G(im, i, j), g_var), 0, 255);
      b = CR_clamp(normal_sample_CR_Random((unsigned char) IMXY_B(im, i, j), b_var), 0, 255);

      IMXY_R(im, i, j) = (unsigned char) r;
      IMXY_G(im, i, j) = (unsigned char) g;
      IMXY_B(im, i, j) = (unsigned char) b;
    }
}

//----------------------------------------------------------------------------

void split_channels_CR_Image(CR_Image *im, CR_Image *r_im, CR_Image *g_im, CR_Image *b_im)
{
  int i, j;

  for (i = 0; i < im->w; i++)
    for (j = 0; j < im->h; j++) {

      IMXY_R(r_im, i, j) = (unsigned char) IMXY_R(im, i, j);
      IMXY_G(r_im, i, j) = (unsigned char) IMXY_R(im, i, j);
      IMXY_B(r_im, i, j) = (unsigned char) IMXY_R(im, i, j);

      IMXY_R(g_im, i, j) = (unsigned char) IMXY_G(im, i, j);
      IMXY_G(g_im, i, j) = (unsigned char) IMXY_G(im, i, j);
      IMXY_B(g_im, i, j) = (unsigned char) IMXY_G(im, i, j);

      IMXY_R(b_im, i, j) = (unsigned char) IMXY_B(im, i, j);
      IMXY_G(b_im, i, j) = (unsigned char) IMXY_B(im, i, j);
      IMXY_B(b_im, i, j) = (unsigned char) IMXY_B(im, i, j);

    }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Image *Cues1_init_CR_CG(char *name)
{
  CR_Image *im;

  im = make_CR_Image(4 * cues_w, cues_h, 0);
  sprintf(name, "Cues1");

  cues_background_im = create_random_tiled_CR_Image(cues_w, cues_h, cues_h_tiles, cues_v_tiles);
  cues_final_im = make_CR_Image(cues_w, cues_h, 0);
  cues_scratch_final_im = make_CR_Image(cues_w, cues_h, 0);

  cues_target_im = create_random_tiled_CR_Image(cues_target_w, cues_target_h, 1, 1);

  cues_r_im = make_CR_Image(cues_w, cues_h, 0);
  cues_g_im = make_CR_Image(cues_w, cues_h, 0);
  cues_b_im = make_CR_Image(cues_w, cues_h, 0);

  // to draw color and the separate channels all at once

  cues_split_im = make_CR_Image(4 * cues_w, cues_h, 0);
  cues_scratch_split_im = make_CR_Image(4 * cues_w, cues_h, 0);

  // color covariance

  cues_v = make_CR_Vector("cues_v", 3);
  cues_vmean = make_CR_Vector("cues_vmean", 3);

  cues_cov = init_CR_Matrix("cues_cov", 3, 3,
			    cues_r_var, 0.0, 0.0,
			    0.0, cues_g_var, 0.0,
			    0.0, 0.0, cues_b_var);
  cues_invcov = inverse_CR_Matrix(cues_cov);
  cues_alpha = gaussian_alpha_CR_Matrix(cues_cov);

  return im;
}

//----------------------------------------------------------------------------

void Cues1_grab_CR_CG(CR_Image *im)
{
  int i, j, r_max_x, r_max_y, g_max_x, g_max_y, b_max_x, b_max_y, color_max_x, color_max_y;
  double r_max_like, g_max_like, b_max_like, color_max_like;

  // final image = background 

  ip_copy_CR_Image(cues_background_im, cues_final_im);

  // final image += target

  cues_x_targ = ranged_uniform_int_CR_Random(0, cues_final_im->w - cues_target_im->w);
  cues_y_targ = ranged_uniform_int_CR_Random(0, cues_final_im->h - cues_target_im->h);
  composite_CR_Image(cues_x_targ, cues_y_targ, cues_target_im, cues_final_im);

  // final image += noise

  ip_noisify_CR_Image(cues_final_im, cues_r_var, cues_g_var, cues_b_var);

  // try to detect the target

  printf("    target: r = %i, g = %i, b = %i\n", (unsigned char) IMXY_R(cues_target_im, 0, 0), (unsigned char) IMXY_G(cues_target_im, 0, 0), (unsigned char) IMXY_B(cues_target_im, 0, 0)); 
  printf("background: r = %i, g = %i, b = %i\n", (unsigned char) IMXY_R(cues_background_im, 0, 0), (unsigned char) IMXY_G(cues_background_im, 0, 0), (unsigned char) IMXY_B(cues_background_im, 0, 0)); 

  r_max_like = Cues1_single_channel_maximum_log_likelihood_ratio_estimate(&r_max_x, &r_max_y, 0, cues_r_var, cues_final_im, cues_target_im, cues_background_im);
  printf("********************************************* R error: x = %i, y = %i\n", r_max_x - cues_x_targ, r_max_y - cues_y_targ);  

  g_max_like = Cues1_single_channel_maximum_log_likelihood_ratio_estimate(&g_max_x, &g_max_y, 1, cues_g_var, cues_final_im, cues_target_im, cues_background_im);
  printf("********************************************* G error: x = %i, y = %i\n", g_max_x - cues_x_targ, g_max_y - cues_y_targ);  

  b_max_like = Cues1_single_channel_maximum_log_likelihood_ratio_estimate(&b_max_x, &b_max_y, 2, cues_b_var, cues_final_im, cues_target_im, cues_background_im);
  printf("********************************************* B error: x = %i, y = %i\n", b_max_x - cues_x_targ, b_max_y - cues_y_targ);  

//   color_max_like = Cues1_color_maximum_log_likelihood_ratio_estimate(&color_max_x, &color_max_y, cues_alpha, cues_invcov, cues_final_im, cues_target_im, cues_background_im);
//   printf("Color error: x = %i, y = %i\n", color_max_x - cues_x_targ, color_max_y - cues_y_targ);  

  printf("-----------------------------------------\n");

  // flip image vertically for proper opengl display and then draw

  split_channels_CR_Image(cues_final_im, cues_r_im, cues_g_im, cues_b_im);
  composite_CR_Image(0, 0, cues_final_im, cues_split_im);
  composite_CR_Image(cues_final_im->w, 0, cues_r_im, cues_split_im);
  composite_CR_Image(2 * cues_final_im->w, 0, cues_g_im, cues_split_im);
  composite_CR_Image(3 * cues_final_im->w, 0, cues_b_im, cues_split_im);

  //  draw_rect_CR_Image(color_max_x, color_max_y, cues_target_im->w, cues_target_im->h, 255, 255, 255, cues_split_im);
  draw_rect_CR_Image(cues_final_im->w + r_max_x, r_max_y, cues_target_im->w, cues_target_im->h, 255, 255, 255, cues_split_im);
  draw_rect_CR_Image(2 * cues_final_im->w + g_max_x, g_max_y, cues_target_im->w, cues_target_im->h, 255, 255, 255, cues_split_im);
  draw_rect_CR_Image(3 * cues_final_im->w + b_max_x, b_max_y, cues_target_im->w, cues_target_im->h, 255, 255, 255, cues_split_im);

  vflip_CR_Image(cues_split_im, cues_scratch_split_im);
  glDrawPixels(cues_split_im->w, cues_final_im->h, GL_RGB, GL_UNSIGNED_BYTE, cues_scratch_split_im->data);

//   vflip_CR_Image(cues_final_im, cues_scratch_final_im);
//   glDrawPixels(cues_final_im->w, cues_final_im->h, GL_RGB, GL_UNSIGNED_BYTE, cues_scratch_final_im->data);
}

//----------------------------------------------------------------------------

void Cues1_end_CR_CG(CR_Image *im)
{

}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// One of n tiles is the target--the others are distractors

CR_Image *OrderParamTask3_init_CR_CG(char *name)
{
  CR_Image *im;
  CR_Vector *meanmean, *targetmeanmean;
  CR_Matrix *meancov, *cov, *targetmeancov, *targetcov;
  double b_mean, b_var, t_mean, t_var;
  CR_ProbDist *b, *t;
  double bhat, logQ, K, J, N, error1_lower, error1_upper, error_lower, error_upper, error2;

  //  im = make_CR_Image(128, 128, 0);
  im = make_CR_Image(21, 21, 0);
  sprintf(name, "OrderParamTask3");

  target_tile_x = 0;
  target_tile_y = 0;
  num_tiles = 9;
  sqrt_num_tiles = (int) sqrt(num_tiles);
  tile_w = im->w / sqrt_num_tiles;
  tile_h = im->h / sqrt_num_tiles;
  N = (double) (tile_w * tile_h);
  logQ = log2(num_tiles - 1) / N;  // normally regular log

  grf_im = make_CR_Image(im->w, im->h, 0);
  scratch_grf_im = make_CR_Image(im->w, im->h, 0);

  // gaussian

  b_mean = 100.0;     // 100
  b_var = 20.0;  // 20

  t_mean = 200.0;         // 200
  t_var = 20.0;      // 20

  // discrete

  task3_shift = 0.0;
  task3_scale = 255.0;

  //background_prob = init_CR_Vector("background_prob", 2, 0.167, 0.833);
  //target_prob = init_CR_Vector("target_prob", 2, 0.8, 0.2);
  background_prob = init_CR_Vector("background_prob", 2, 0.375, 0.625);
  target_prob = init_CR_Vector("target_prob", 2, 0.667, 0.333);
  //background_prob = init_CR_Vector("background_prob", 2, 0.5, 0.5);
  //target_prob = init_CR_Vector("target_prob", 2, 0.6, 0.4);
  b = make_distribution_CR_Info(background_prob);
  t = make_distribution_CR_Info(target_prob);
  bhat = bhattacharyya_bound_CR_Info(b, t);
  K = 2 * bhat - logQ;
  J = (double) background_prob->rows;  // alphabet size
  error1_lower = pow(N + 1, -SQUARE(J));
  error1_upper = pow(N + 1, SQUARE(J));
  error2 = pow(2, -N * K);
  error_lower = error1_lower * error2;
  error_upper = error1_upper * error2;
  printf("K = %lf, error1_lower = %lf, error1_upper = %lf, error2 = %lf, error_lower = %lf, error_upper = %lf\n", K, error1_lower, error1_upper, error2, error_lower, error_upper);
  printf("-N * K = %lf, N = %lf, bhat = %lf, logQ = %lf, J = %i\n", -N * K, N, bhat, logQ, J);
  printf("-N * 2 * bhat = %lf, 2^that = %lf\n", -N * 2 * bhat, pow(2, -N * 2 * bhat));
  printf("cher(b, t) = %lf, kb(b, t) = %lf, kb(t, b) = %lf\n", chernoff_bound_CR_Info(b, t), kullback_leibler_distance_CR_Info(b, t), kullback_leibler_distance_CR_Info(t, b));

  printf("\n");
  print_CR_Vector(target_prob);
  print_CR_Vector(background_prob);
  printf("image w = %i, image h = %i\n", grf_im->w, grf_im->h);
  printf("num_tiles = %i\n", num_tiles);
  printf("tile w = %i, tile h = %i\n\n", tile_w, tile_h);
  //  CR_exit(1);

  // background
  
  meanmean = init_CR_Vector("meanmean", 1, b_mean); // 100.0
  meancov = init_CR_Matrix("meancov", 1, 1, 1.0);  // 400.0
  cov = init_CR_Matrix("cov", 1, 1, b_var);  // 20.0

  grf_cumprob = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_mean = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_covsqrt = make_CR_Random_Field(CR_MATRIX, im->h, im->w);
  grf_val = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_alpha = make_CR_Matrix("grf_alpha", im->h, im->w);

  //  make_gaussian_CR_Random_Field(im->w, im->h, meanmean, NULL, cov, grf_alpha, grf_mean, grf_covsqrt, grf_val);
  make_discrete_CR_Random_Field(im->w, im->h, task3_shift, task3_scale, background_prob, grf_cumprob, grf_val);

  
  // background-sized target

  targetmeanmean = init_CR_Vector("targetmeanmean", 1, t_mean); // 200.0
  targetmeancov = init_CR_Matrix("targetmeancov", 1, 1, 1.0);  // 400.0
  targetcov = init_CR_Matrix("targetcov", 1, 1, t_var);  // 20.0

  grf_target_cumprob = make_CR_Random_Field(CR_VECTOR, tile_h, tile_w);
  grf_target_mean = make_CR_Random_Field(CR_VECTOR, tile_h, tile_w);
  grf_target_covsqrt = make_CR_Random_Field(CR_MATRIX, tile_h, tile_w);
  grf_target_val = make_CR_Random_Field(CR_VECTOR, tile_h, tile_w);
  grf_target_alpha = make_CR_Matrix("grf_target_alpha", tile_h, tile_w);

  //  make_gaussian_CR_Random_Field(tile_w, tile_h, targetmeanmean, NULL, targetcov, grf_target_alpha, grf_target_mean, grf_target_covsqrt, grf_target_val);
  make_discrete_CR_Random_Field(tile_w, tile_h, task3_shift, task3_scale, target_prob, grf_target_cumprob, grf_target_val);

  CR_difftime();

  // finish

  return im;
}

//----------------------------------------------------------------------------

void OrderParamTask3_grab_CR_CG(CR_Image *im)
{
  int x, y, i, j;
  double loglike_tb, target_loglike_tb;
  int matching_distractors;

  // change target tile location?

  time_now += CR_difftime();
  if (no_pause || time_now > pause_time) {
    time_now = 0;
    target_tile_x = ranged_uniform_int_CR_Random(0, sqrt_num_tiles - 1);
    target_tile_y = ranged_uniform_int_CR_Random(0, sqrt_num_tiles - 1);
  }

  // get x, y of target tile upper left hand corner from its tile coords.

  x = tile_w * target_tile_x;
  y = tile_h * target_tile_y;

  // draw appropriate image

  sample_discrete_CR_Random_Field(task3_shift, task3_scale, grf_cumprob, grf_val);
  sample_discrete_CR_Random_Field(task3_shift, task3_scale, grf_target_cumprob, grf_target_val);
  //  sample_gaussian_CR_Random_Field(grf_alpha, grf_mean, grf_covsqrt, grf_val);              // calc. background appearance
  //  sample_gaussian_CR_Random_Field(grf_target_alpha, grf_target_mean, grf_target_covsqrt, grf_target_val); // calc. target appearance
  overlay_CR_Random_Field(x, y, grf_target_val, grf_val);    // overlay target on background at appropriate tile
  convert_to_image_CR_Random_Field(grf_val, grf_im);            // draw background & target

  if (no_pause || time_now == 0) {   // just moved target

    matching_distractors = 0;

    target_loglike_tb = log_likelihood_ratio_discrete_CR_Random_Field(tile_w * target_tile_x, tile_h * target_tile_y, tile_w, tile_h, grf_val, task3_shift, task3_scale, target_prob, task3_shift, task3_scale, background_prob); 

    for (j = 0; j < sqrt_num_tiles; j++)
      for (i = 0; i < sqrt_num_tiles; i++) {
	if (j != target_tile_y || i != target_tile_x) {
	  loglike_tb = log_likelihood_ratio_discrete_CR_Random_Field(tile_w * i, tile_h * j, tile_w, tile_h, grf_val, task3_shift, task3_scale, target_prob, task3_shift, task3_scale, background_prob); 
	  if (loglike_tb > target_loglike_tb)
	    matching_distractors++;
	}
      }
    num_trials++;
    num_matching_distractors += matching_distractors;
    if (matching_distractors > 0)
      num_errors++; 
    if (!(num_trials % 10000)) {  // print report at intervals
      printf("num trials = %i\n", num_trials);
      printf("num errors = %i\n", num_errors);
      printf("num_matching_distractors = %i\n", num_matching_distractors);
      printf("error rate = %lf, mean matching distractors = %lf\n\n", (double) num_errors / (double) num_trials, (double) num_matching_distractors / (double) num_trials);
    }
    if (num_trials == max_trials)    // stop
      CR_exit(1);
  }

  // flip image vertically for proper opengl display and then draw

  vflip_CR_Image(grf_im, scratch_grf_im);
  glDrawPixels(grf_im->w, grf_im->h, GL_RGB, GL_UNSIGNED_BYTE, scratch_grf_im->data);
}

//----------------------------------------------------------------------------

void OrderParamTask3_end_CR_CG(CR_Image *im)
{

}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// One of 2 tiles is the target--the other one is a distractor

CR_Image *OrderParamTask2_init_CR_CG(char *name)
{
  CR_Image *im;
  CR_Vector *meanmean, *targetmeanmean;
  CR_Matrix *meancov, *cov, *targetmeancov, *targetcov;
  double b_mean, b_var, t_mean, t_var;
  CR_ProbDist *b, *t;
  double bhat, K, J, N, error1_lower, error1_upper, error_lower, error_upper, error2;

  //  im = make_CR_Image(128, 64, 0);
  im = make_CR_Image(14, 7, 0);
  sprintf(name, "OrderParamTask2");

  target_tile_x = 0;
  target_tile_y = 0;
  horiz_num_tiles = 2;
  vert_num_tiles = 1;
  num_tiles = horiz_num_tiles * vert_num_tiles;
  tile_w = im->w / horiz_num_tiles;
  tile_h = im->h / vert_num_tiles;
  N = (double) (tile_w * tile_h);

  grf_im = make_CR_Image(im->w, im->h, 0);
  scratch_grf_im = make_CR_Image(im->w, im->h, 0);

  // gaussian

  b_mean = 100.0;     // 100
  b_var = 20.0;  // 20

  t_mean = 200.0;         // 200
  t_var = 20.0;      // 20

  // discrete

  task3_shift = 0.0;
  task3_scale = 255.0;

  //background_prob = init_CR_Vector("background_prob", 2, 0.167, 0.833);
  //target_prob = init_CR_Vector("target_prob", 2, 0.8, 0.2);
  //background_prob = init_CR_Vector("background_prob", 2, 0.375, 0.625);
  //target_prob = init_CR_Vector("target_prob", 2, 0.667, 0.333);
  background_prob = init_CR_Vector("background_prob", 2, 0.5, 0.5);
  target_prob = init_CR_Vector("target_prob", 2, 0.6, 0.4);
  b = make_distribution_CR_Info(background_prob);
  t = make_distribution_CR_Info(target_prob);
  bhat = bhattacharyya_bound_CR_Info(b, t);
  K = 2 * bhat;
  J = (double) background_prob->rows;  // alphabet size
  error1_lower = pow(N + 1, -SQUARE(J));
  error1_upper = pow(N + 1, SQUARE(J));
  error2 = pow(2, -N * K);
  error_lower = error1_lower * error2;
  error_upper = error1_upper * error2;
  printf("K = %lf, error1_lower = %lf, error1_upper = %lf, error2 = %lf, error_lower = %lf, error_upper = %lf\n", K, error1_lower, error1_upper, error2, error_lower, error_upper);
  printf("-N * K = %lf, N = %lf, bhat = %lf, J = %lf\n", -N * K, N, bhat, J);

  printf("\n");
  print_CR_Vector(target_prob);
  print_CR_Vector(background_prob);
  printf("image w = %i, image h = %i\n", grf_im->w, grf_im->h);
  printf("num_tiles = %i\n", num_tiles);
  printf("tile w = %i, tile h = %i\n\n", tile_w, tile_h);
  //  CR_exit(1);

  // background
  
  meanmean = init_CR_Vector("meanmean", 1, b_mean); // 100.0
  meancov = init_CR_Matrix("meancov", 1, 1, 1.0);  // 400.0
  cov = init_CR_Matrix("cov", 1, 1, b_var);  // 20.0

  grf_cumprob = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_mean = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_covsqrt = make_CR_Random_Field(CR_MATRIX, im->h, im->w);
  grf_val = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_alpha = make_CR_Matrix("grf_alpha", im->h, im->w);

  //  make_gaussian_CR_Random_Field(im->w, im->h, meanmean, NULL, cov, grf_alpha, grf_mean, grf_covsqrt, grf_val);
  make_discrete_CR_Random_Field(im->w, im->h, task3_shift, task3_scale, background_prob, grf_cumprob, grf_val);

  
  // background-sized target

  targetmeanmean = init_CR_Vector("targetmeanmean", 1, t_mean); // 200.0
  targetmeancov = init_CR_Matrix("targetmeancov", 1, 1, 1.0);  // 400.0
  targetcov = init_CR_Matrix("targetcov", 1, 1, t_var);  // 20.0

  grf_target_cumprob = make_CR_Random_Field(CR_VECTOR, tile_h, tile_w);
  grf_target_mean = make_CR_Random_Field(CR_VECTOR, tile_h, tile_w);
  grf_target_covsqrt = make_CR_Random_Field(CR_MATRIX, tile_h, tile_w);
  grf_target_val = make_CR_Random_Field(CR_VECTOR, tile_h, tile_w);
  grf_target_alpha = make_CR_Matrix("grf_target_alpha", tile_h, tile_w);

  //  make_gaussian_CR_Random_Field(tile_w, tile_h, targetmeanmean, NULL, targetcov, grf_target_alpha, grf_target_mean, grf_target_covsqrt, grf_target_val);
  make_discrete_CR_Random_Field(tile_w, tile_h, task3_shift, task3_scale, target_prob, grf_target_cumprob, grf_target_val);

  CR_difftime();

  // finish

  return im;
}

//----------------------------------------------------------------------------

void OrderParamTask2_grab_CR_CG(CR_Image *im)
{
  int x, y, i, j;
  double loglike_tb, target_loglike_tb;
  int matching_distractors;

  // change target tile location?

  time_now += CR_difftime();
  if (no_pause || time_now > pause_time) {
    time_now = 0;
    target_tile_x = ranged_uniform_int_CR_Random(0, horiz_num_tiles - 1);
    target_tile_y = ranged_uniform_int_CR_Random(0, vert_num_tiles - 1);
  }

  // get x, y of target tile upper left hand corner from its tile coords.

  x = tile_w * target_tile_x;
  y = tile_h * target_tile_y;

  // draw appropriate image

  sample_discrete_CR_Random_Field(task3_shift, task3_scale, grf_cumprob, grf_val);
  sample_discrete_CR_Random_Field(task3_shift, task3_scale, grf_target_cumprob, grf_target_val);
  //  sample_gaussian_CR_Random_Field(grf_alpha, grf_mean, grf_covsqrt, grf_val);              // calc. background appearance
  //  sample_gaussian_CR_Random_Field(grf_target_alpha, grf_target_mean, grf_target_covsqrt, grf_target_val); // calc. target appearance
  overlay_CR_Random_Field(x, y, grf_target_val, grf_val);    // overlay target on background at appropriate tile
  convert_to_image_CR_Random_Field(grf_val, grf_im);            // draw background & target

  if (no_pause || time_now == 0) {   // just moved target

    matching_distractors = 0;

    target_loglike_tb = log_likelihood_ratio_discrete_CR_Random_Field(tile_w * target_tile_x, tile_h * target_tile_y, tile_w, tile_h, grf_val, task3_shift, task3_scale, target_prob, task3_shift, task3_scale, background_prob); 

    for (j = 0; j < vert_num_tiles; j++)
      for (i = 0; i < horiz_num_tiles; i++) {
	if (j != target_tile_y || i != target_tile_x) {
	  loglike_tb = log_likelihood_ratio_discrete_CR_Random_Field(tile_w * i, tile_h * j, tile_w, tile_h, grf_val, task3_shift, task3_scale, target_prob, task3_shift, task3_scale, background_prob); 
	  if (loglike_tb > target_loglike_tb)
	    matching_distractors++;
	}
      }
    num_trials++;
    num_matching_distractors += matching_distractors;
    if (matching_distractors > 0)
      num_errors++; 
    if (!(num_trials % 10000)) {  // print report at intervals
      printf("num trials = %i\n", num_trials);
      printf("num errors = %i\n", num_errors);
      printf("num_matching_distractors = %i\n", num_matching_distractors);
      printf("error rate = %lf, mean matching distractors = %lf\n\n", (double) num_errors / (double) num_trials, (double) num_matching_distractors / (double) num_trials);
    }
    if (num_trials == max_trials)    // stop
      CR_exit(1);
  }

  // flip image vertically for proper opengl display and then draw

  vflip_CR_Image(grf_im, scratch_grf_im);
  glDrawPixels(grf_im->w, grf_im->h, GL_RGB, GL_UNSIGNED_BYTE, scratch_grf_im->data);
}

//----------------------------------------------------------------------------

void OrderParamTask2_end_CR_CG(CR_Image *im)
{

}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Image *planx_im;      // decimated size (320 x 240 or 360 x 240)
CR_Image *planx_big_im;  // original size (640 x 480 or 720 x 480)
CR_Image *planx_scratch_im;

CR_Image *planx_voter_im; 
CR_Image *planx_voter_scratch_im;

CR_Matrix **planx_voter_dlm;

int planx_do_orientation_interpolation;
int planx_do_anisotropy;
int planx_do_strength;
int planx_do_point_line_dist;
int planx_do_no_inv_dist;
int planx_do_halfsize;  // decimate image for speed?  (720x480 -> 360x240)

int **planx_voter_max;  // for each wavelength, index of dominant orientation (DO) at pixel (x, y)
int **planx_voter_max2;  // for each wavelength, index of runner-up for dominant orientation (DO) at pixel (x, y)
CR_Matrix **planx_voter_interp_nx;
CR_Matrix **planx_voter_interp_ny;
CR_Matrix **planx_voter_strength;  // for each wavelength, filter response at DO at pixel (x, y), normalized by filter area
CR_Matrix **planx_voter_anisotropy;  // for each wavelength, fraction of total Gabor power of DO at pixel (x, y)
CR_Matrix **planx_voter_inv_strength;  
CR_Matrix **planx_voter_inv_anisotropy;  
double *planx_nx;     // vector pointing in direction of orientation
double *planx_ny;
double *planx_filter_area;

int planx_num_orientations;    // denoted "theta"
double planx_start_theta;      // first orientation
double planx_delta_theta;      // orientation interval (in degrees)

int planx_num_wavelengths;     // denoted "lambda"
double planx_start_lambda;     // first wavelength
double planx_delta_lambda;     // wavelength interval (1 = 1 octave)

int planx_num_filters;
int planx_num_kernels;

#define PLANX_GRAB_WHO_VOTED_FOR_THE_VP       1
#define PLANX_GRAB_STRIPWRITE_WHERE_IS_THE_VP 2
#define PLANX_GRAB_WHERE_IS_THE_VP            3
#define PLANX_GRAB_PF_WHERE_IS_THE_VP         4
#define PLANX_GRAB_PF_TRACKING_MOVIE          5
#define PLANX_GRAB_STRIP_PF_WHERE_IS_THE_VP   6
#define PLANX_GRAB_STRIP_PF_TRACKING_MOVIE    7
#define PLANX_GRAB_MOVIE_STRIP_PF_WHERE_IS_THE_VP   8

int planx_grab_type;           // what should the grab function do?
int planx_avi_input;           // are we reading from an AVI?
char planx_avi_filename[128] = "C:\\Documents\\tex\\planx\\images\\input\\dvData_23_clip1.avi";
char planx_avi_vps_filename[128] = "C:\\Documents\\tex\\planx\\images\\avi\\dvData_25_clip1\\all_vps.dlm";
char planx_avi_pf_samples_filename[128] = "C:\\Documents\\tex\\planx\\images\\avi\\dvData_25_clip1\\pf_samples.dlm";
CR_Movie *planx_movie;

// DO(x,y) = dominant orientation at a pixel (x,y)
// VP(x,y) = direction to vanishing point from pixel (x,y)

int planx_w, planx_h;                  // raw image dimensions

int planx_voter_w, planx_voter_h;          // dimensions of subsampled image for voters
int planx_voter_left, planx_voter_right, planx_voter_top, planx_voter_bottom;
int planx_voter_dx, planx_voter_dy;
int planx_voter_strip_height;
int planx_voter_current_top;
CR_Matrix **planx_wavelength_voters;  // one entry for every voter
CR_Matrix *planx_vp_locations;   // rows are images, each pair of cols is (r, c) of vanishing point pixel (y=r-1,x=c-1) for a wavelength
CR_Matrix *planx_voter_slices;  // what angle relative to VP did voters come from?  
                                // rows=wavelengths, col pairs=slices: (1) yes votes in this slice, (2)total voters in this slice
int planx_num_voter_slices;   // how many slices of 180 degrees will voters be divided into?
double planx_voter_slices_delta_theta;  // thickness of each slice

int planx_candidate_w, planx_candidate_h;       // dimensions of subsampled image for VP candidates
int planx_candidate_left, planx_candidate_right, planx_candidate_top, planx_candidate_bottom;
int planx_candidate_dx, planx_candidate_dy;
CR_Matrix *planx_candidate_votes;
CR_Image *planx_candidate_votes_im;
CR_Matrix **planx_candidate_wavelength_votes;
CR_Image **planx_candidate_wavelength_votes_im;

double planx_cos_threshold;            // cosine over this number indicates angle b/t DO(x,y) & VP(x,y) less than angular resolution of filters

CR_Image *planx_gray_im, *planx_gray_fp_im, *planx_odd_gabor_fp_im, *planx_even_gabor_fp_im;
CR_Image *planx_squared_odd_gabor_fp_im, *planx_squared_even_gabor_fp_im, *planx_complex_gabor_fp_im;
CR_Matrix **planx_gabor_kernel_matrix;
CR_Matrix *planx_gabor_kernel_info;
CR_Image_Kernel **planx_gabor_kernel;
CR_Image *planx_gabor_kernel_im;
char *planx_kernel_name;

CR_Image **planx_pyr_im, **planx_pyr_dx_im, **planx_pyr_dy_im, **planx_pyr_smoothed_im, **planx_pyr_littlesmoothed_im;
int *planx_pyr_w, *planx_pyr_h;
int planx_pyr_levels;
int planx_pyr_blocksize, planx_pyr_blockframe, planx_pyr_blockarea;
int planx_pyr_svd_dx, planx_pyr_svd_dy;
double planx_pyr_voter_R_value_threshold;
CR_Matrix **planx_pyr_voter_dx, **planx_pyr_voter_dy, **planx_pyr_voter_R_value, **planx_pyr_voter_gradient_magnitude;
CR_Matrix *planx_pyr_M, *planx_pyr_U, *planx_pyr_VT;
double *planx_pyr_S;

CR_Condensation *planx_con;
CR_Matrix *planx_cov;

//double kernel_max, kernel_min;
// int left, right, top, bottom, deltax, deltay, new_w, new_h;

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

//int planx_num_filenames = 17;
int planx_num_filenames = 2;

int planx_filename_index;
int planx_filename_start_index, planx_filename_stop_index;

char planx_in_filename[18][128] = {"C:\\Documents\\tex\\planx\\images\\input\\unsurfaced_straight_640_480.ppm",    // 0
				   "C:\\Documents\\tex\\planx\\images\\input\\dv12_1.ppm",                         // 1
				   "C:\\Documents\\tex\\planx\\images\\input\\dv13_1.ppm",                         // 2
				   "C:\\Documents\\tex\\planx\\images\\input\\dv14_1.ppm",                         // 3
				   "C:\\Documents\\tex\\planx\\images\\input\\dv23_1.ppm",                         // 4
				   "C:\\Documents\\tex\\planx\\images\\input\\dv23_2.ppm",                         // 5
				   "C:\\Documents\\tex\\planx\\images\\input\\dv23_3.ppm",                         // 6
				   "C:\\Documents\\tex\\planx\\images\\input\\dv23_4.ppm",                         // 7
				   "C:\\Documents\\tex\\planx\\images\\input\\dv25_1.ppm",                         // 8
				   "C:\\Documents\\tex\\planx\\images\\input\\dv25_2.ppm",                         // 9
				   "C:\\Documents\\tex\\planx\\images\\input\\dv25_3.ppm",                         // 10
				   "C:\\Documents\\tex\\planx\\images\\input\\dv25_4.ppm",                         // 11
				   "C:\\Documents\\tex\\planx\\images\\input\\dv25_5.ppm",                         // 12
				   "C:\\Documents\\tex\\planx\\images\\input\\dv25_6.ppm",                         // 13
				   "C:\\Documents\\tex\\planx\\images\\input\\dv25_7.ppm",                         // 14
				   "C:\\Documents\\tex\\planx\\images\\input\\dv34_1.ppm",                         // 15
				   "C:\\Documents\\tex\\planx\\images\\input\\dv34_2.ppm",                         // 16
				   "C:\\Documents\\tex\\planx\\images\\input\\newhope.ppm"};                       // 17

char planx_out_filename[17][128] = {"C:\\Documents\\tex\\planx\\images\\small_strip_results\\unsurf_straight\\unsurfaced_straight_640_480_",
				    "C:\\Documents\\tex\\planx\\images\\small_strip_results\\dv12_1\\dv12_1_720_480_",
				    "C:\\Documents\\tex\\planx\\images\\small_strip_results\\dv13_1\\dv13_1_720_480_",
				    "C:\\Documents\\tex\\planx\\images\\small_strip_results\\dv14_1\\dv14_1_720_480_", 
				    "C:\\Documents\\tex\\planx\\images\\small_strip_results\\dv23_1\\dv23_1_720_480_", 
				    "C:\\Documents\\tex\\planx\\images\\small_strip_results\\dv23_2\\dv23_2_720_480_", 
				    "C:\\Documents\\tex\\planx\\images\\small_strip_results\\dv23_3\\dv23_3_720_480_", 
				    "C:\\Documents\\tex\\planx\\images\\small_strip_results\\dv23_4\\dv23_4_720_480_", 
				    "C:\\Documents\\tex\\planx\\images\\small_strip_results\\dv25_1\\dv25_1_720_480_", 
				    "C:\\Documents\\tex\\planx\\images\\small_strip_results\\dv25_2\\dv25_2_720_480_", 
				    "C:\\Documents\\tex\\planx\\images\\small_strip_results\\dv25_3\\dv25_3_720_480_", 
				    "C:\\Documents\\tex\\planx\\images\\small_strip_results\\dv25_4\\dv25_4_720_480_", 
				    "C:\\Documents\\tex\\planx\\images\\small_strip_results\\dv25_5\\dv25_5_720_480_", 
				    "C:\\Documents\\tex\\planx\\images\\small_strip_results\\dv25_6\\dv25_6_720_480_", 
				    "C:\\Documents\\tex\\planx\\images\\small_strip_results\\dv25_7\\dv25_7_720_480_", 
				    "C:\\Documents\\tex\\planx\\images\\small_strip_results\\dv34_1\\dv34_1_720_480_", 
				    "C:\\Documents\\tex\\planx\\images\\small_strip_results\\dv34_2\\dv34_2_720_480_" };

char planx_voter_filename[17][128] = {"C:\\Documents\\tex\\planx\\images\\small_voter2_results\\unsurf_straight_640_480_",
				      "C:\\Documents\\tex\\planx\\images\\small_voter2_results\\dv12_1_720_480_",
				      "C:\\Documents\\tex\\planx\\images\\small_voter2_results\\dv13_1_720_480_",
				      "C:\\Documents\\tex\\planx\\images\\small_voter2_results\\dv14_1_720_480_",
				      "C:\\Documents\\tex\\planx\\images\\small_voter2_results\\dv23_1_720_480_",
				      "C:\\Documents\\tex\\planx\\images\\small_voter2_results\\dv23_2_720_480_",
				      "C:\\Documents\\tex\\planx\\images\\small_voter2_results\\dv23_3_720_480_",
				      "C:\\Documents\\tex\\planx\\images\\small_voter2_results\\dv23_4_720_480_",
				      "C:\\Documents\\tex\\planx\\images\\small_voter2_results\\dv25_1_720_480_",
				      "C:\\Documents\\tex\\planx\\images\\small_voter2_results\\dv25_2_720_480_",
				      "C:\\Documents\\tex\\planx\\images\\small_voter2_results\\dv25_3_720_480_",
				      "C:\\Documents\\tex\\planx\\images\\small_voter2_results\\dv25_4_720_480_",
				      "C:\\Documents\\tex\\planx\\images\\small_voter2_results\\dv25_5_720_480_",
				      "C:\\Documents\\tex\\planx\\images\\small_voter2_results\\dv25_6_720_480_",
				      "C:\\Documents\\tex\\planx\\images\\small_voter2_results\\dv25_7_720_480_",
				      "C:\\Documents\\tex\\planx\\images\\small_voter2_results\\dv34_1_720_480_",
				      "C:\\Documents\\tex\\planx\\images\\small_voter2_results\\dv34_2_720_480_",};

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void drawcross(CR_Image *im, int x, int y, int radius, int r, int g, int b)
{
  int i, j;

  for (i = -radius; i <= radius; i++) {
    IMXY_R(im, x+i, y) = (unsigned char) r;
    IMXY_G(im, x+i, y) = (unsigned char) g;
    IMXY_B(im, x+i, y) = (unsigned char) b;

    IMXY_R(im, x, y+i) = (unsigned char) r;
    IMXY_G(im, x, y+i) = (unsigned char) g;
    IMXY_B(im, x, y+i) = (unsigned char) b;
  }
}

void drawcross(CR_Image *im, int x, int y, int radius, int thickness, int r, int g, int b)
{
  int i, j;

  for (j = -thickness; j <= thickness; j++)
    for (i = -radius; i <= radius; i++) {
      IMXY_R(im, x+i, y+j) = (unsigned char) r;
      IMXY_G(im, x+i, y+j) = (unsigned char) g;
      IMXY_B(im, x+i, y+j) = (unsigned char) b;
      
      IMXY_R(im, x+j, y+i) = (unsigned char) r;
      IMXY_G(im, x+j, y+i) = (unsigned char) g;
      IMXY_B(im, x+j, y+i) = (unsigned char) b;
    }
}

CR_Image *PlanX_init_CR_CG(char *name)
{
  int i;

  /*
  // the crap you gotta do...

  int x = 367;
  int y = 160;

  planx_im = read_CR_Image("C:\\Documents\\tex\\planx\\images\\input\\dv25_2.ppm");

  drawcross(planx_im, x, y, 23, 4, 0, 0, 0);
  drawcross(planx_im, x, y, 20, 1, 255, 255, 255);
  write_CR_Image("C:\\Documents\\tex\\planx\\images\\paper\\dv25_2_wav3_max.ppm", planx_im);


  FILE *fp;

  int l = 23;
  int t = 8;

  fp = CR_emitEPS_setupfile("C:\\Documents\\tex\\planx\\images\\paper\\dv25_2_wav3_max.ps", 0, 0, 719, 479);
  emitEPS_CR_Image(fp, planx_im);

  CR_emitEPS_draw_line(fp, x - l, 480-y, x + l, 480-y, t, 0, 0, 0);
  CR_emitEPS_draw_line(fp, x, 480-y - l, x, 480-y + l, t, 0, 0, 0);

  l = 20;
  t = 2;

  CR_emitEPS_draw_line(fp, x - l, 480-y, x + l, 480-y, t, 255, 255, 255);
  CR_emitEPS_draw_line(fp, x, 480-y - l, x, 480-y + l, t, 255, 255, 255);

  CR_emitEPS_closefile(fp);

  exit(1);
  */

  // draw cross at (380, 38)
  // write

  // get image source

  printf("reading images...");

  //  planx_avi_input = TRUE;
  planx_avi_input = FALSE;
  planx_do_halfsize = TRUE;
  //  planx_do_halfsize = FALSE;

  if (!planx_avi_input) {
    planx_filename_start_index = 0;  // if we start at 0, then we switch from a 640 x 480 to 720 x 480 images--doesn't that screw up all the support matrices?
    planx_filename_stop_index = 0;
    
    // pre-Nov. 13 experiments
    //  planx_im = read_CR_Image(planx_in_filename[planx_filename_index]);

    // Nov. 13 experiments
    planx_big_im = read_CR_Image(planx_in_filename[planx_filename_start_index]);
  }
  else {
    planx_movie = open_CR_Movie(planx_avi_filename);
    planx_filename_start_index = 0;  
    printf("numframes = %i\n", planx_movie->numframes);
    //planx_filename_stop_index = planx_movie->numframes - 2;
    planx_filename_stop_index = 2;   // 629 for dvData_25_clip2.avi
    //    planx_movie->currentframe = 0;  
    get_frame_CR_Movie(planx_movie, planx_movie->currentframe);
    planx_movie->currentframe++;
    planx_big_im = planx_movie->im;
  }

  // "small" experiments
  if (planx_do_halfsize) {
    planx_im = make_CR_Image(planx_big_im->w / 2, planx_big_im->h / 2, 0);
    iplDecimateBlur(planx_big_im->iplim, planx_im->iplim, 1, 2, 1, 2, IPL_INTER_LINEAR, 3, 3);
  }
  else 
    planx_im = planx_big_im;

//   write_CR_Image("small.ppm", planx_im);
//   exit(1);

  // "smaller" experiments
  //  planx_im = make_CR_Image(planx_big_im->w / 4, planx_big_im->h / 4, 0);
  //  iplDecimateBlur(planx_big_im->iplim, planx_im->iplim, 1, 4, 1, 4, IPL_INTER_LINEAR, 5, 5);
  //  write_CR_Image("smaller.ppm", planx_im);
  //  exit(1);

  planx_w = planx_im->w;
  planx_h = planx_im->h;

  // shrink image

  //  iplDecimate(planx_im->iplim, planx_small_im->iplim, 1, 2, 1, 2, IPL_INTER_LINEAR);
  //  write_CR_Image("test1.ppm", planx_small_im);
  //  exit(1);

  planx_scratch_im = make_CR_Image(planx_w, planx_h, 0);
  ip_copy_CR_Image(planx_im, planx_scratch_im);

  printf("done\n");

  // initialize algorithm data structures

  PlanX_init_per_run();

  // slap a name on the venture

  sprintf(name, "PlanX");

  // opengl stuff

  //  glClearAccum(0.0,0.0,0.0,0.0); 
  glClearColor(0.0, 0.0, 0.0, 0.0);

  /*
  cursor_flag = FALSE;

  // select clearing color


  // initialize viewing values  

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, planx_voter_im->w, 0, planx_voter_im->h, 0, 10);
  */

  // finish

  return planx_im;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void PlanX_init_per_run()
{
  int i, j, t, x, y;
  double theta;
  char *filename;

  //-------------------------------------------------
  // disable some annoying warning crap
  //-------------------------------------------------

  int tmpFlag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
  tmpFlag &= ~_CRTDBG_ALLOC_MEM_DF;
  _CrtSetDbgFlag(tmpFlag);

  //-------------------------------------------------
  // set the key numbers
  //-------------------------------------------------

  // all experiments before Nov. 13:
  // planx_num_orientations = 72;
  // planx_num_wavelengths = 4;
  // int max_filter_frame = 50;
  // planx_voter_dx = 2;
  // planx_voter_dy = 2;

  // "small" experiments on Nov. 13
  // planx_num_orientations = 36, 72
  // planx_num_wavelengths = 3;
  // int max_filter_frame = 25;
  // planx_voter_dx, dy = 1, 2
  
  // "smaller" experiments on Nov. 13
  // planx_num_orientations = 36, 72
  // planx_num_wavelengths = 2;
  // int max_filter_frame = 12;
  // planx_voter_dx, dy = 1, 2

  planx_grab_type = PLANX_GRAB_MOVIE_STRIP_PF_WHERE_IS_THE_VP;
  //  planx_grab_type = PLANX_GRAB_STRIP_PF_WHERE_IS_THE_VP;
  //planx_grab_type = PLANX_GRAB_STRIP_PF_TRACKING_MOVIE;

  //  planx_num_orientations = 8;
  planx_num_orientations = 72;   // 72 // best results so far with 72
  planx_start_theta = 0.0;
  //  planx_delta_theta = 22.5;          // 180 / planx_num_orientations, but it doesn't have to be
  planx_delta_theta = 180.0 / (double) planx_num_orientations;          

  planx_num_wavelengths = 3;     // 4    // was 4 for all of the more recent stuff; was 5 for all of the full-res unsurfaced_straight experiments
  planx_start_lambda = 4.0;          // 4 = 12 x 12 kernel at first level
  planx_delta_lambda = 1.0;          // 1 = kernel size basically doubles in each dimension

  int max_filter_frame = 25;    
  //  int max_filter_frame = 50; // for a top kernel size of 101 x 101

  planx_voter_left = max_filter_frame;
  planx_voter_right = planx_w - max_filter_frame;
  planx_voter_top = max_filter_frame;
  //  planx_voter_top = 350; 
  planx_voter_bottom = planx_h - max_filter_frame;
  planx_voter_dx = 10;   // 2 standard
  planx_voter_dy = 10;   // 2 standard
  planx_voter_strip_height = 50;
  
  planx_candidate_left = 0;                   // max_filter_frame;
  planx_candidate_right = planx_w;        // planx_w - max_filter_frame;
  planx_candidate_top = 0;
  planx_candidate_bottom = 3 * planx_h /4;
  planx_candidate_dx = 1;  // 1
  planx_candidate_dy = 1;  // 1

  planx_do_orientation_interpolation = FALSE;
  planx_do_anisotropy = FALSE;
  planx_do_strength = FALSE;
  planx_do_point_line_dist = FALSE;
  planx_do_no_inv_dist = TRUE;

  planx_pyr_levels = 6;
  planx_pyr_blocksize = 3;  // 5 x 5 blocks of gradient values
  planx_pyr_svd_dx = planx_pyr_svd_dy = 1;

  planx_num_voter_slices = 180;   // how many
  planx_voter_slices_delta_theta = 180.0 / (double) planx_num_voter_slices;

//   filename = (char *) calloc(256, sizeof(char));
//   if (planx_do_point_line_dist)
//     sprintf(filename, "C:\\Documents\\tex\\planx\\images\\results\\unsurfaced_straight_640_480_%i_%i_POINT_LINE_DIST_%i_%i_from_%i.dlm", 
// 	    planx_num_orientations, planx_num_wavelengths, 
// 	    planx_do_strength, planx_do_anisotropy,
// 	    planx_voter_top);
//   else
//     sprintf(filename, "C:\\Documents\\tex\\planx\\images\\results\\unsurfaced_straight_640_480_%i_%i_INV_DIST_%i_%i_from_%i.dlm", 
// 	    planx_num_orientations, planx_num_wavelengths, 
// 	    planx_do_strength, planx_do_anisotropy,
// 	    planx_voter_top);
//   printf("%s\n", filename);
//   exit(1);

  //-------------------------------------------------
  // compute vectors along possible dominant orientation directions
  //-------------------------------------------------

  planx_cos_threshold = cos(0.5 * DEG2RAD(planx_delta_theta));

  planx_nx = (double *) calloc(planx_num_orientations, sizeof(double));
  planx_ny = (double *) calloc(planx_num_orientations, sizeof(double));

  for (i = 0; i < planx_num_orientations; i++) {
    theta = DEG2RAD(planx_delta_theta) * (double) i;
    planx_nx[i] = cos(theta);
    planx_ny[i] = sin(theta);
  }

  planx_filter_area = (double *) calloc(planx_num_wavelengths, sizeof(double));

  //-------------------------------------------------
  // voting, dominant orientation, anisotropy data structures
  //-------------------------------------------------

  // candidates

  for (y = planx_candidate_top, planx_candidate_h = 0; y < planx_candidate_bottom; y += planx_candidate_dy, planx_candidate_h++)
    for (x = planx_candidate_left, planx_candidate_w = 0; x < planx_candidate_right; x += planx_candidate_dx, planx_candidate_w++)
      ;

  planx_candidate_votes = make_CR_Matrix("votes", planx_candidate_h, planx_candidate_w);
  planx_candidate_votes_im = make_CR_Image(planx_candidate_w, planx_candidate_h, 0);
  planx_candidate_wavelength_votes = (CR_Matrix **) calloc(planx_num_wavelengths, sizeof(CR_Matrix *));
  planx_candidate_wavelength_votes_im = (CR_Image **) calloc(planx_num_wavelengths, sizeof(CR_Image *));
  for (t = 0; t < planx_num_wavelengths; t++) {
    planx_candidate_wavelength_votes[t] = make_CR_Matrix("candidate wavelength_votes", planx_candidate_h, planx_candidate_w);
    planx_candidate_wavelength_votes_im[t] = make_CR_Image(planx_candidate_w, planx_candidate_h, 0);
  }

  // voters

  for (y = planx_voter_top, planx_voter_h = 0; y < planx_voter_bottom; y += planx_voter_dy, planx_voter_h++)
    for (x = planx_voter_left, planx_voter_w = 0; x < planx_voter_right; x += planx_voter_dx, planx_voter_w++)
      ;

  if (!planx_avi_input) {
    planx_vp_locations = read_dlm_CR_Matrix("C:\\Documents\\tex\\planx\\images\\matlab_results\\all_vps.dlm", 17, planx_num_wavelengths*2);
    print_CR_Matrix(planx_vp_locations);
  }
  else 
    planx_vp_locations = make_CR_Matrix("vp_locations", planx_filename_stop_index + 1, 2 * (planx_num_wavelengths + 1));

  planx_voter_slices = make_all_CR_Matrix("voter_slices", planx_num_wavelengths, 2*planx_num_voter_slices, 0.0);

  planx_wavelength_voters = (CR_Matrix **) calloc(planx_num_wavelengths, sizeof(CR_Matrix *));
  for (t = 0; t < planx_num_wavelengths; t++) 
    planx_wavelength_voters[t] = make_CR_Matrix("wavelength_voters", planx_voter_h, planx_voter_w);

  planx_voter_max = (int **) calloc(planx_num_wavelengths, sizeof(int *));
  planx_voter_max2 = (int **) calloc(planx_num_wavelengths, sizeof(int *));
  planx_voter_interp_nx = (CR_Matrix **) calloc(planx_num_wavelengths, sizeof(CR_Matrix *));
  planx_voter_interp_ny = (CR_Matrix **) calloc(planx_num_wavelengths, sizeof(CR_Matrix *));
  planx_voter_strength = (CR_Matrix **) calloc(planx_num_wavelengths, sizeof(CR_Matrix *));
  planx_voter_anisotropy = (CR_Matrix **) calloc(planx_num_wavelengths, sizeof(CR_Matrix *));
  planx_voter_inv_strength = (CR_Matrix **) calloc(planx_num_wavelengths, sizeof(CR_Matrix *));
  planx_voter_inv_anisotropy = (CR_Matrix **) calloc(planx_num_wavelengths, sizeof(CR_Matrix *));
  for (t = 0; t < planx_num_wavelengths; t++) {
    planx_voter_max[t] = (int *) calloc(planx_voter_w * planx_voter_h, sizeof(int));
    planx_voter_max2[t] = (int *) calloc(planx_voter_w * planx_voter_h, sizeof(int));
    planx_voter_interp_nx[t] = make_CR_Matrix("voter_interp_nx", planx_voter_h, planx_voter_w);
    planx_voter_interp_ny[t] = make_CR_Matrix("voter_interp_ny", planx_voter_h, planx_voter_w);
    planx_voter_strength[t] = make_CR_Matrix("voter_strength", planx_voter_h, planx_voter_w);
    planx_voter_anisotropy[t] = make_CR_Matrix("voter_anisotropy", planx_voter_h, planx_voter_w);
    planx_voter_inv_strength[t] = make_CR_Matrix("voter_inv_strength", planx_voter_h, planx_voter_w);
    planx_voter_inv_anisotropy[t] = make_CR_Matrix("voter_inv_anisotropy", planx_voter_h, planx_voter_w);
  }

  //-------------------------------------------------
  // make the filters
  //-------------------------------------------------

  planx_gray_im = make_CR_Image(planx_w, planx_h, IPL_TYPE_GRAY_8U);

  //  Planx_initialize_multiscale_PCA_orientation();

  PlanX_initialize_gabor_filters(TRUE);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void Planx_initialize_multiscale_PCA_orientation()
{
  int i, x, y, vx, vy;

  if (!(planx_pyr_blocksize % 2))
    CR_error("block side length should be odd for multiscale PCA");
  //  planx_pyr_block_M = make_CR_Matrix("block", planx_pyr_blocksize * planx_pyr_blocksize, 2);
  planx_pyr_blockframe = (planx_pyr_blocksize - 1) / 2;
  planx_pyr_blockarea = SQUARE(planx_pyr_blocksize);
//   planx_pyr_block_M = matrix(1, SQUARE(planx_pyr_blocksize), 1, 2);
//   planx_pyr_svd_w = vector(1, 2);
//   planx_pyr_svd_v = matrix(1, 2, 1, 2); 
  planx_pyr_M = make_CR_Matrix("M", planx_pyr_blockarea, 2);
  planx_pyr_U = make_CR_Matrix("U", planx_pyr_blockarea, planx_pyr_blockarea);
  planx_pyr_S = (double *) CR_calloc(2, sizeof(double));
  planx_pyr_VT = make_CR_Matrix("VT", 2, 2);
  init_svd(planx_pyr_blockarea);  // so svd doesn't keep allocating and de-allocating work space

  // R* = 0.25 for 95%% confidence with N = 25 (5 x 5 blocksize)

  if (planx_pyr_blocksize == 5)
    planx_pyr_voter_R_value_threshold = 0.25;
  else if (planx_pyr_blocksize == 3)
    planx_pyr_voter_R_value_threshold = 0.43;
  else
    CR_error("can't handle blocksizes other than 3 and 5 right now");

  planx_pyr_voter_dx = (CR_Matrix **) calloc(planx_pyr_levels, sizeof(CR_Matrix *));
  planx_pyr_voter_dy = (CR_Matrix **) calloc(planx_pyr_levels, sizeof(CR_Matrix *));
  planx_pyr_voter_R_value = (CR_Matrix **) calloc(planx_pyr_levels, sizeof(CR_Matrix *));
  planx_pyr_voter_gradient_magnitude = (CR_Matrix **) calloc(planx_pyr_levels, sizeof(CR_Matrix *));

  // initialize convolution filters and images for intermediate filter stages

  planx_pyr_im = (CR_Image **) calloc(planx_pyr_levels, sizeof(CR_Image *));
  planx_pyr_dx_im = (CR_Image **) calloc(planx_pyr_levels, sizeof(CR_Image *));
  planx_pyr_dy_im = (CR_Image **) calloc(planx_pyr_levels, sizeof(CR_Image *));
  planx_pyr_smoothed_im = (CR_Image **) calloc(planx_pyr_levels, sizeof(CR_Image *));
  planx_pyr_littlesmoothed_im = (CR_Image **) calloc(planx_pyr_levels, sizeof(CR_Image *));
  planx_pyr_w = (int *) calloc(planx_pyr_levels, sizeof(int));
  planx_pyr_h = (int *) calloc(planx_pyr_levels, sizeof(int));

  for (i = 0; i < planx_pyr_levels; i++) {

    planx_pyr_w[i] = (int) (pow(2, -i) * (double) planx_w);
    planx_pyr_h[i] = (int) (pow(2, -i) * (double) planx_h);

    for (y = planx_pyr_blockframe, vy = 0; y < planx_pyr_h[i] - planx_pyr_blockframe; y += planx_pyr_svd_dy, vy++)
      for (x = planx_pyr_blockframe, vx = 0; x < planx_pyr_w[i] - planx_pyr_blockframe; x += planx_pyr_svd_dx, vx++) 
	;

    planx_pyr_voter_dx[i] = make_CR_Matrix("pyr_voter_dx", vy, vx);
    planx_pyr_voter_dy[i] = make_CR_Matrix("pyr_voter_dy", vy, vx);
    planx_pyr_voter_R_value[i] = make_CR_Matrix("pyr_voter_R_value", vy, vx);
    planx_pyr_voter_gradient_magnitude[i] = make_CR_Matrix("pyr_voter_R_gradmag", vy, vx);

    printf("%i: w = %i, h = %i\n", i, planx_pyr_w[i], planx_pyr_h[i]);
    printf("%i: vw = %i, vh = %i\n", i, vx, vy);

    planx_pyr_im[i] = make_CR_Image(planx_pyr_w[i], planx_pyr_h[i], IPL_TYPE_GRAY_32F);
    planx_pyr_dx_im[i] = make_CR_Image(planx_pyr_w[i], planx_pyr_h[i], IPL_TYPE_GRAY_32F);
    planx_pyr_dy_im[i] = make_CR_Image(planx_pyr_w[i], planx_pyr_h[i], IPL_TYPE_GRAY_32F);
    planx_pyr_smoothed_im[i] = make_CR_Image(planx_pyr_w[i], planx_pyr_h[i], IPL_TYPE_GRAY_32F);
    planx_pyr_littlesmoothed_im[i] = make_CR_Image(planx_pyr_w[i], planx_pyr_h[i], IPL_TYPE_GRAY_32F);
  }
}

//----------------------------------------------------------------------------
 
void Planx_run_multiscale_PCA_orientation()
{
  int i, x, y, u, v, t, vx, vy;
  double time_elapsed, total_time;
  float dx, dy, sum;

  //  filename = (char *) calloc(256, sizeof(char));

  CR_difftime();
  total_time = 0;

  iplColorToGray(planx_im->iplim, planx_gray_im->iplim);
  iplScaleFP(planx_gray_im->iplim, planx_pyr_im[0]->iplim, 0, 255);

  // make Gaussian pyramid of image

  for (i = 0; i < planx_pyr_levels; i++) {

    printf("multiscale PCA: running level %i\n", i);

    // reduce (subsample) pyr_smoothed_im[i - 1] to get pyr_im[i]

    if (i > 0) {
      for (y = 0; y < planx_pyr_h[i]; y++)
	for (x = 0; x < planx_pyr_w[i]; x++)
	  GRAYFP_IMXY(planx_pyr_im[i], x, y) = GRAYFP_IMXY(planx_pyr_im[i - 1], 2*x, 2*y);
    }

    // run smoothing filter

    iplFixedFilter(planx_pyr_im[i]->iplim, planx_pyr_smoothed_im[i]->iplim, IPL_GAUSSIAN_5x5);
    iplFixedFilter(planx_pyr_im[i]->iplim, planx_pyr_littlesmoothed_im[i]->iplim, IPL_GAUSSIAN_3x3);

    // run dx, dy filters

//     iplFixedFilter(planx_pyr_im[i]->iplim, planx_pyr_dx_im[i]->iplim, IPL_SOBEL_3x3_V);
//     iplFixedFilter(planx_pyr_im[i]->iplim, planx_pyr_dy_im[i]->iplim, IPL_SOBEL_3x3_H);
    iplFixedFilter(planx_pyr_littlesmoothed_im[i]->iplim, planx_pyr_dx_im[i]->iplim, IPL_SOBEL_3x3_V);
    iplFixedFilter(planx_pyr_littlesmoothed_im[i]->iplim, planx_pyr_dy_im[i]->iplim, IPL_SOBEL_3x3_H);

    // iterate over blocks, collecting gradient values (assuming blocksize is odd)

//     CR_Matrix *mydx = make_CR_Matrix("mydx", planx_pyr_voter_dx[i]->rows, planx_pyr_voter_dx[i]->cols);
//     CR_Matrix *mydy = make_CR_Matrix("mydy", planx_pyr_voter_dx[i]->rows, planx_pyr_voter_dx[i]->cols);

    for (y = planx_pyr_blockframe, vy = 0; y < planx_pyr_h[i] - planx_pyr_blockframe; y += planx_pyr_svd_dy, vy++) {
      printf("y = %i\n", y);
      for (x = planx_pyr_blockframe, vx = 0; x < planx_pyr_w[i] - planx_pyr_blockframe; x += planx_pyr_svd_dx, vx++) {
	for (v = y - planx_pyr_blockframe, t = 0, sum = 0; v <= y + planx_pyr_blockframe; v++)
	  for (u = x - planx_pyr_blockframe; u <= x + planx_pyr_blockframe; u++) {
	    
	    dx = GRAYFP_IMXY(planx_pyr_dx_im[i], u, v);
	    dy = GRAYFP_IMXY(planx_pyr_dy_im[i], u, v);

	    CM_MATRC(planx_pyr_M, t, 0) = dx;
	    CM_MATRC(planx_pyr_M, t, 1) = dy;

	    sum += dx*dx + dy*dy;

	    t++;
	  }
	

	// run svd on collected gradient values and save results, including something proportional to condition number
	
	MATRC(planx_pyr_voter_gradient_magnitude[i], vy, vx) = sum;

	if (MATRC(planx_pyr_voter_gradient_magnitude[i], vy, vx) < 0.01)
	  MATRC(planx_pyr_voter_R_value[i], vy, vx) = 0.0;
	else {
	  clapack_svd_CR_Matrix(planx_pyr_M, planx_pyr_U, planx_pyr_S, planx_pyr_VT);
	  
	  MATRC(planx_pyr_voter_R_value[i], vy, vx) = (planx_pyr_S[0] - planx_pyr_S[1]) / (planx_pyr_S[0] + planx_pyr_S[1]);
	
	  MATRC(planx_pyr_voter_dx[i], vy, vx) = CM_MATRC(planx_pyr_VT, 0, 0);
	  MATRC(planx_pyr_voter_dy[i], vy, vx) = CM_MATRC(planx_pyr_VT, 1, 0);
	}
      }
    }

    // save these to files and check
    
    /*
      sprintf(filename, "C:\\Documents\\tex\\planx\\images\\planx_pyr_%i.pgm", i);
    write_CR_Image(filename, planx_pyr_im[i]); 

    sprintf(filename, "C:\\Documents\\tex\\planx\\images\\planx_pyr_%i_smoothed.pgm", i);
    write_CR_Image(filename, planx_pyr_smoothed_im[i]); 

    sprintf(filename, "C:\\Documents\\tex\\planx\\images\\planx_pyr_%i_dx.pgm", i);
    write_CR_Image(filename, planx_pyr_dx_im[i]); 

    sprintf(filename, "C:\\Documents\\tex\\planx\\images\\planx_pyr_%i_dy.pgm", i);
    write_CR_Image(filename, planx_pyr_dy_im[i]); 
    */

    time_elapsed = CR_difftime();
    total_time += time_elapsed;
    printf("time = %lf / %lf\n", time_elapsed, total_time);
  }

  time_elapsed = CR_difftime();
  total_time += time_elapsed;
  printf("TOTAL time = %lf\n", total_time);

  //  exit(1);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void PlanX_initialize_gabor_filters(int new_way)
{
  FILE *fp;
  char *filename, *full_name, *voter_name;
  int i, t;

  planx_num_filters = planx_num_orientations * planx_num_wavelengths;

  // subsampled filter responses (i.e., "voters") will go here

  planx_voter_dlm = (CR_Matrix **) calloc(planx_num_filters, sizeof(CR_Matrix *));
  for (t = 0; t < planx_num_filters; t++) 
    planx_voter_dlm[t] = make_CR_Matrix("planx_voter_dlm", planx_voter_h, planx_voter_w);

  //-------------------------------------------------
  // the old way: read from already-written dlm files
  //-------------------------------------------------

  if (!new_way) {

    fp = fopen("C:\\Documents\\tex\\planx\\images\\filtered_dlm\\dlms.txt", "r");
    
    full_name = (char *) calloc(256, sizeof(char));
    voter_name = (char *) calloc(256, sizeof(char));
    filename = (char *) calloc(256, sizeof(char));
    
    for (t = 0; t < planx_num_filters; t++) {  // planx_num_filters lines in dlms.txt
      
      fscanf(fp, "%s\n", filename);
      sprintf(voter_name, "C:\\Documents\\tex\\planx\\images\\sub_filtered_dlm\\%s", filename);
      
      printf("%s\n", voter_name);
      
      read_dlm_CR_Matrix(voter_name, planx_voter_dlm[t]);    
    }    
    
    fclose(fp);
  }

  //-------------------------------------------------
  // the new way: do it for real (don't write dlms)
  //-------------------------------------------------

  else {

    planx_num_kernels = planx_num_filters * 2;

    printf("making kernels...\n");

    planx_kernel_name = (char *) CR_calloc(256, sizeof(char));
    planx_gabor_kernel_info = make_CR_Matrix("kernel_info", planx_num_kernels, 4); // wavelength, orientation, phase, size  
    planx_gabor_kernel_matrix = make_gabor_filter_bank_CR_Matrix_array(planx_start_lambda, planx_delta_lambda, planx_num_wavelengths, 
								       planx_start_theta, planx_delta_theta, planx_num_orientations, 
								       planx_gabor_kernel_info);

    for (i = 0; i < planx_num_wavelengths; i++) 
      planx_filter_area[i] = SQUARE(planx_gabor_kernel_matrix[2 * planx_num_orientations * i]->rows);

    planx_gabor_kernel = (CR_Image_Kernel **) CR_calloc(planx_num_kernels, sizeof(CR_Image_Kernel *));

    for (i = 0; i < planx_num_kernels; i++) { 
      
      planx_gabor_kernel[i] = make_CR_Image_Kernel(planx_gabor_kernel_matrix[i]);

      //     lambda = MATRC(planx_gabor_kernel_info, i, 0); 
      //     orientation = MATRC(planx_gabor_kernel_info, i, 1); 
      //     even = MATRC(planx_gabor_kernel_info, i, 2);
      //     size = MATRC(planx_gabor_kernel_info, i, 3);
      
      /*
	planx_gabor_kernel_im = make_CR_Image(planx_gabor_kernel_matrix[i]->cols, planx_gabor_kernel_matrix[i]->rows, 0);
	interp_draw_CR_Matrix_CR_Image(planx_gabor_kernel_matrix[i], planx_gabor_kernel_im);
	sprintf(kernel_name, "C:\\Documents\\tex\\planx\\images\\kernels\\%03i_%.2lf_%.2lf_%.0lf_%.0lf.ppm", i, lambda, orientation, even, size);
	write_CR_Image(kernel_name, planx_gabor_kernel_im);
	free_CR_Image(planx_gabor_kernel_im);
      */

      //    write_pfm_CR_Matrix(kernel_name, planx_gabor_kernel_matrix[i]);
    }
    
    printf("done\n");

    //-------------------------------------------------
    // prepare storage for convolution results
    //-------------------------------------------------
    
    // initialize images for filter stages

    planx_gray_fp_im = make_CR_Image(planx_w, planx_h, IPL_TYPE_GRAY_32F);
    planx_odd_gabor_fp_im = make_CR_Image(planx_w, planx_h, IPL_TYPE_GRAY_32F);
    planx_even_gabor_fp_im = make_CR_Image(planx_w, planx_h, IPL_TYPE_GRAY_32F);
    planx_squared_odd_gabor_fp_im = make_CR_Image(planx_w, planx_h, IPL_TYPE_GRAY_32F);
    planx_squared_even_gabor_fp_im = make_CR_Image(planx_w, planx_h, IPL_TYPE_GRAY_32F);
    planx_complex_gabor_fp_im = make_CR_Image(planx_w, planx_h, IPL_TYPE_GRAY_32F);

  }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void PlanX_init_per_image(int new_way)
{
  int i, j, i_lambda, i_theta, i_max, i_max2, r, c, x, y;
  double s_max, s_max2, s;
  CR_Matrix *mat;
  double lambda, orientation, even, size, nx, ny, nmag;
  double time_elapsed, total_time;
  CR_Image *im;

  // get new image

  //  printf("index = %i\n", planx_filename_index);

  if (planx_filename_index != 0){
    //    free_CR_Image(planx_im);
    //    planx_im = read_CR_Image(planx_in_filename[planx_filename_index]);
    
    if (!planx_avi_input) {
      free_CR_Image(planx_big_im);
      planx_big_im = read_CR_Image(planx_in_filename[planx_filename_index]);
    }
    else {
      get_frame_CR_Movie(planx_movie, planx_movie->currentframe);
      planx_movie->currentframe++;
      planx_big_im = planx_movie->im;
    }

    if (planx_do_halfsize)
      iplDecimateBlur(planx_big_im->iplim, planx_im->iplim, 1, 2, 1, 2, IPL_INTER_LINEAR, 3, 3);
    else
      planx_im = planx_big_im;
  }

  //  return;

  //-------------------------------------------------
  // run filters
  //-------------------------------------------------

  //  Planx_run_multiscale_PCA_orientation();
  //  return;

  if (new_way) {

    // convert current image to gray, scale to FP

    iplColorToGray(planx_im->iplim, planx_gray_im->iplim);
    iplScaleFP(planx_gray_im->iplim, planx_gray_fp_im->iplim, 0, 255);

    // run Gabor filters on image and histogram

    CR_difftime();
    total_time = 0;

    for (i = 0; i < planx_num_kernels / 2; i++) {

      lambda = MATRC(planx_gabor_kernel_info, 2 * i, 0); 
      orientation = MATRC(planx_gabor_kernel_info, 2 * i, 1); 
      even = MATRC(planx_gabor_kernel_info, 2 * i, 2);
      size = MATRC(planx_gabor_kernel_info, 2 * i, 3);
  
      printf("%i: convolving with %.2lf %.2lf %.0lf %.0lf\n", 2 * i, lambda, orientation, even, size);
      printf("%i: convolving with %.2lf %.2lf %.0lf %.0lf \n", 2 * i + 1, lambda, orientation, even + 1, size);

      // simple odd, even responses

      convolve_CR_Image(planx_gabor_kernel[2 * i], planx_gray_fp_im, planx_odd_gabor_fp_im);
      convolve_CR_Image(planx_gabor_kernel[2 * i + 1], planx_gray_fp_im, planx_even_gabor_fp_im);

      // complex response/power modulus

      iplSquare(planx_odd_gabor_fp_im->iplim, planx_squared_odd_gabor_fp_im->iplim);
      iplSquare(planx_even_gabor_fp_im->iplim, planx_squared_even_gabor_fp_im->iplim);
      iplAdd(planx_squared_odd_gabor_fp_im->iplim, planx_squared_even_gabor_fp_im->iplim, planx_complex_gabor_fp_im->iplim);

      for (y = 0; y < planx_h; y++)
	for (x = 0; x < planx_w; x++)
	  GRAYFP_IMXY(planx_complex_gabor_fp_im, x, y) = sqrt(GRAYFP_IMXY(planx_complex_gabor_fp_im, x, y));

      time_elapsed = CR_difftime();
      total_time += time_elapsed;
      printf("time = %lf / %lf\n", time_elapsed, total_time);

      // write filter responses

      im = planx_complex_gabor_fp_im;
      mat = planx_voter_dlm[i];

      for (y = planx_voter_top, r = 0; y < planx_voter_bottom; y += planx_voter_dy, r++)
	for (x = planx_voter_left, c = 0; x < planx_voter_right; x += planx_voter_dx, c++)
	  MATRC(mat, r, c) = ((float *) &(im->iplim->imageData[y * im->iplim->widthStep + 4 * x]))[0];

      /*
      sprintf(kernel_name, "C:\\Documents\\tex\\planx\\images\\filtered\\unsurf_straight_%03i_%.2lf_%.2lf_complex_%.0lf.pgm", 2 * i, lambda, orientation, size);
      write_CR_Image(kernel_name, planx_complex_gabor_fp_im);
      sprintf(kernel_name, "C:\\Documents\\tex\\planx\\images\\filtered_dlm\\unsurf_straight_%03i_%.2lf_%.2lf_complex_%.0lf.dlm", 2 * i, lambda, orientation, size);
      write_dlm_CR_Image(kernel_name, planx_complex_gabor_fp_im);
      */
      
    }
  }

  //-------------------------------------------------
  // reset vote arrays
  //-------------------------------------------------
      
  for (r = 0; r < planx_candidate_h; r++) 
    for (c = 0; c < planx_candidate_w; c++) {
      MATRC(planx_candidate_votes, r, c) = 0.0;
      for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++)     // iterate over scales
	MATRC(planx_candidate_wavelength_votes[i_lambda], r, c) = 0.0;
    }

  //-------------------------------------------------
  // compute dominant orientations
  //-------------------------------------------------

  printf("calculating dominant orientations, strength, anisotropy factors...");

  for (j = 0; j < planx_voter_h; j++)
    for (i = 0; i < planx_voter_w; i++) 
      for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++) {                // iterate over scales

	MATRC(planx_voter_anisotropy[i_lambda], j, i) = 0.0;
	for (i_theta = 0, s_max = s_max2 = -1.0; i_theta < planx_num_orientations; i_theta++) {  // iterate over orientations
	  mat = planx_voter_dlm[i_theta + planx_num_orientations * i_lambda];
	  s = MATRC(mat, j, i);
	  MATRC(planx_voter_anisotropy[i_lambda], j, i) += s;
	  //	  planx_voter_anisotropy[i_lambda][i + planx_voter_w * j] += s;
	  if (s > s_max) {
	    s_max2 = s_max;
	    i_max2 = i_max;
	    s_max = s;
	    i_max = i_theta;
	  }
	  else if (s > s_max2) {
	    s_max2 = s;
	    i_max2 = i_theta;
	  }
	}
	if (s_max == -1.0)
	  CR_error("bluh");
	planx_voter_max[i_lambda][i + planx_voter_w * j] = i_max;
	planx_voter_max2[i_lambda][i + planx_voter_w * j] = i_max2;
	nx = s_max * planx_nx[i_max] + s_max2 * planx_nx[i_max2];
	ny = s_max * planx_ny[i_max] + s_max2 * planx_ny[i_max2];
	nmag = sqrt(SQUARE(nx) + SQUARE(ny));
	nx /= nmag;
	ny /= nmag;
	MATRC(planx_voter_interp_nx[i_lambda], j, i) = nx;
	MATRC(planx_voter_interp_ny[i_lambda], j, i) = ny;
	MATRC(planx_voter_strength[i_lambda], j, i) = s_max / planx_filter_area[i_lambda];
	MATRC(planx_voter_inv_strength[i_lambda], j, i) = 1 / MATRC(planx_voter_strength[i_lambda], j, i);
	if (s_max) {
	  MATRC(planx_voter_anisotropy[i_lambda], j, i) /= s_max;
	  MATRC(planx_voter_inv_anisotropy[i_lambda], j, i) = 1 / MATRC(planx_voter_anisotropy[i_lambda], j, i);
	}
	//	  planx_voter_anisotropy[i_lambda][i + planx_voter_w * j] /= s_max;
      }

  printf("done\n");
}

//----------------------------------------------------------------------------

void draw_gabor_orientations(int interp)
{
  int r, c, r_start, i_max;
  double vx, vy, vy_start;
  CR_Matrix *mat;
  double length, leg, nx, ny;
  int i_lambda;
  double x1, y1, x2, y2;

  FILE *fp;

  int l = 23;
  int t = 8;

  fp = CR_emitEPS_setupfile("C:\\Documents\\tex\\planx\\images\\paper\\orientations.ps", 0, 0, 359, 239);
  emitEPS_CR_Image(fp, planx_im);


  vy_start = planx_voter_top;
  r_start = 0;

  glPushMatrix();
  /*
  if (i_lambda == 0)
    glColor3ub(255, 0, 0);
  if (i_lambda == 1)
    glColor3ub(255, 255, 0);
  else if (i_lambda == 2)
    glColor3ub(0, 255, 0);
  else if (i_lambda == 3)
    glColor3ub(0, 0, 255);
  else
    glColor3ub(0, 255, 255);
  */

  glLineWidth(1);

  length = 5;  // 4

  glBegin(GL_LINES);

  for (vy = vy_start, r = r_start; vy <= planx_voter_bottom; vy += planx_voter_dy, r++)
    for (vx = planx_voter_left, c = 0; vx <= planx_voter_right; vx += planx_voter_dx, c++) {

      i_lambda = 2;

      //      for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++) {    // iterate over scales

	if (interp) {
	  if (i_lambda == 0)
	    glColor3ub(192, 0, 0);
	  else if (i_lambda == 1)
	    glColor3ub(192, 192, 0);
	  else if (i_lambda == 2)
	    glColor3ub(0, 192, 0);
	  else if (i_lambda == 3)
	    glColor3ub(0, 0, 192);

	}
	else {
	if (i_lambda == 0)
	  glColor3ub(255, 0, 0);
	else if (i_lambda == 1)
	  glColor3ub(255, 255, 0);
	else if (i_lambda == 2)
	  glColor3ub(0, 255, 0);
	else if (i_lambda == 3)
	  glColor3ub(0, 0, 255);

	}
// 	else {
// 	  printf("lambda = %i\n", i_lambda);
// 	  exit(1);
// 	  glColor3ub(0, 255, 255);
// 	}

	if (!interp) {
	  i_max = planx_voter_max[i_lambda][c + planx_voter_w * r];
	  nx = planx_nx[i_max];
	  ny = planx_ny[i_max];
	}
	else {
	  nx = MATRC(planx_voter_interp_nx[i_lambda], r, c);
	  ny = MATRC(planx_voter_interp_ny[i_lambda], r, c);
	}

	//      length = MATRC(planx_voter_strength[i_lambda], r, c);

	//      glVertex2f(vx, planx_h - vy);
	//      glVertex2f(vx + length * nx, planx_h - (vy + length * planx_ny[i_max]));

	length = 5;

	x1 = vx + length * nx;
	y1 = planx_h - (vy - length * ny);
	x2 = vx - length * nx;
	y2 = planx_h - (vy + length * ny);

	glVertex2f(vx + length * nx, planx_h - (vy - length * ny));
	glVertex2f(vx - length * nx, planx_h - (vy + length * ny));

	CR_emitEPS_draw_line(fp, x1, y1, x2, y2, 3, 0, 0, 0);

	length = 4;

	x1 = vx + length * nx;
	y1 = planx_h - (vy - length * ny);
	x2 = vx - length * nx;
	y2 = planx_h - (vy + length * ny);

	CR_emitEPS_draw_line(fp, x1, y1, x2, y2, 1, 255, 0, 0);


	// }
    }
      
  glEnd();

  /*
  length = 30;  // 4
  leg = length * tan(0.5 * DEG2RAD(planx_delta_theta));

  glBegin(GL_TRIANGLES);

  for (vy = vy_start, r = r_start; vy < planx_voter_bottom; vy += planx_voter_dy, r++)
    for (vx = planx_voter_left, c = 0; vx < planx_voter_right; vx += planx_voter_dx, c++) {

      i_max = planx_voter_max[i_lambda][c + planx_voter_w * r];


      glVertex2f(vx, planx_h - vy);
      glVertex2f(vx - leg * planx_ny[i_max] + length * planx_nx[i_max], planx_h - (vy - leg * planx_nx[i_max] - length * planx_ny[i_max]));
      glVertex2f(vx + leg * planx_ny[i_max] + length * planx_nx[i_max], planx_h - (vy + leg * planx_nx[i_max] - length * planx_ny[i_max]));

      glVertex2f(vx, planx_h - vy);
      glVertex2f(vx - leg * planx_ny[i_max] - length * planx_nx[i_max], planx_h - (vy - leg * planx_nx[i_max] + length * planx_ny[i_max]));
      glVertex2f(vx + leg * planx_ny[i_max] - length * planx_nx[i_max], planx_h - (vy + leg * planx_nx[i_max] + length * planx_ny[i_max]));

      //      glVertex2f(vx + length * planx_nx[i_max], planx_h - (vy - length * planx_ny[i_max]));
      //      glVertex2f(vx - length * planx_nx[i_max], planx_h - (vy + length * planx_ny[i_max]));
    }
      
  glEnd();
  */

  glPopMatrix();


  CR_emitEPS_closefile(fp);

  exit(1);


}

//----------------------------------------------------------------------------

// tally voters in a particular wavelength i_lambda for a particular VP (x, y)

void PlanX_voters(double x, double y, int i_lambda)
{
  int r, c, r_start, i_max, slice_num;
  double yes_votes, vx, vy, vy_start, vpx, vpy, vpmag, inv_vpmag, vp_cos, angle;
  CR_Matrix *mat;

  yes_votes = 0;

  vy_start = MAX2(floor(y + 1), planx_voter_top);
  if (vy_start > planx_voter_bottom)
    return;
  r_start = (vy_start - planx_voter_top) / planx_voter_dy;

  for (vy = vy_start, r = r_start; vy < planx_voter_bottom; vy += planx_voter_dy, r++)
    for (vx = planx_voter_left, c = 0; vx < planx_voter_right; vx += planx_voter_dx, c++) {

      vpx = vx - x;
      vpy = vy - y;
      vpmag = sqrt(SQUARE(vpx) + SQUARE(vpy));
      inv_vpmag = 1.0 / vpmag;
      vpx *= inv_vpmag;
      vpy *= inv_vpmag;

      // what slice are we in?
      angle = RAD2DEG(atan2(vpy,vpx));
      slice_num = (int) floor(angle/planx_voter_slices_delta_theta);
      //      if (i_lambda == 3)
      //	printf("%lf %lf ang %lf slice %i (vx %lf vy %lf x %lf y %lf)\n", vpx, vpy, angle, slice_num, vx, vy, x, y);

      // update total in this slice
      MATRC(planx_voter_slices, i_lambda, 2*slice_num + 1) = MATRC(planx_voter_slices, i_lambda, 2*slice_num + 1) + 1;
      
      i_max = planx_voter_max[i_lambda][c + planx_voter_w * r];
      
      // still don't know why the minus sign below is necessary
      vp_cos = -vpx * planx_nx[i_max] + vpy * planx_ny[i_max];  
      
      yes_votes += fabs(vp_cos);

      MATRC(planx_wavelength_voters[i_lambda], r, c) = fabs(vp_cos);
      MATRC(planx_voter_slices, i_lambda, 2*slice_num) = MATRC(planx_voter_slices, i_lambda, 2*slice_num) + fabs(vp_cos);

//       MATRC(planx_wavelength_voters_anisotropy[i_lambda], r, c) = fabs(vp_cos);
//       MATRC(planx_wavelength_voters_strength[i_lambda], r, c) = fabs(vp_cos);

      /*
      if (fabs(vp_cos) >= planx_cos_threshold) {

	yes_votes++;

	MATRC(planx_wavelength_voters[i_lambda], r, c) = 1;
	
	// update yes votes in this slice
	MATRC(planx_voter_slices, i_lambda, 2*slice_num) = MATRC(planx_voter_slices, i_lambda, 2*slice_num) + 1;
      }
      */
    }

  printf("VP: x = %lf, y = %lf (i_lambda = %i)...yes votes = %lf\n", x, y, i_lambda, yes_votes);

}

//----------------------------------------------------------------------------

// particle filtering version

double pf_PlanX_vote(double x, double y, int voter_top, int voter_bottom, int i_lambda)
{
  int r, c, r_start, i_max;
  double votes, vx, vy, vy_start, vpx, vpy, vpmag, inv_vpmag, vp_cos;

  if (x < planx_candidate_left || x >= planx_candidate_right || y < planx_candidate_top || y >= planx_candidate_bottom)
    return 0;

  vy_start = MAX2(floor(y + 1), voter_top);
  if (vy_start > voter_bottom)
    return 0;
  r_start = (vy_start - planx_voter_top) / planx_voter_dy;

  votes = 0;

  for (vy = vy_start, r = r_start; vy < voter_bottom; vy += planx_voter_dy, r++)
    for (vx = planx_voter_left, c = 0; vx < planx_voter_right; vx += planx_voter_dx, c++) {

      vpx = vx - x;
      vpy = vy - y;
      vpmag = sqrt(SQUARE(vpx) + SQUARE(vpy));
      inv_vpmag = 1.0 / vpmag;
      vpx *= inv_vpmag;
      vpy *= inv_vpmag;
      
      i_max = planx_voter_max[i_lambda][c + planx_voter_w * r];
	
      // still don't know why the minus sign below is necessary
      vp_cos = -vpx * planx_nx[i_max] + vpy * planx_ny[i_max];  

      if (fabs(vp_cos) >= planx_cos_threshold) 
	votes++;
    }

  return votes;
}

//----------------------------------------------------------------------------

// strip version

void PlanX_vote_DIST(double x, double y, int cand_r, int cand_c, int voter_top, int voter_bottom)
{
  int r, c, r_start, i_max, i_lambda;
  double votes, vx, vy, vy_start, vpx, vpy, vpmag, inv_vpmag, vp_cos;
  CR_Matrix *mat;

  vy_start = MAX2(floor(y + 1), voter_top);
  if (vy_start > voter_bottom)
    return;
  r_start = (vy_start - planx_voter_top) / planx_voter_dy;

  if (x == 100) 
    printf("y = %.1lf, voter_top = %i, vy_start = %.1lf, r_start = %i\n", y, voter_top, vy_start, r_start);

  for (vy = vy_start, r = r_start; vy < voter_bottom; vy += planx_voter_dy, r++)
    for (vx = planx_voter_left, c = 0; vx < planx_voter_right; vx += planx_voter_dx, c++) {

      vpx = vx - x;
      vpy = vy - y;
      vpmag = sqrt(SQUARE(vpx) + SQUARE(vpy));
      inv_vpmag = 1.0 / vpmag;
      vpx *= inv_vpmag;
      vpy *= inv_vpmag;
      
      for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++) {    // iterate over scales

	// assuming no orientation interpolation
	i_max = planx_voter_max[i_lambda][c + planx_voter_w * r];
	
	// still don't know why the minus sign below is necessary
	vp_cos = -vpx * planx_nx[i_max] + vpy * planx_ny[i_max];  

	if (fabs(vp_cos) >= planx_cos_threshold) { 
	  
	  // assuming no inv dist
	  MATRC(planx_candidate_wavelength_votes[i_lambda], cand_r, cand_c) = MATRC(planx_candidate_wavelength_votes[i_lambda], cand_r, cand_c) + 1;
	  MATRC(planx_candidate_votes, cand_r, cand_c) = MATRC(planx_candidate_votes, cand_r, cand_c) + 1;
	}
      }
    }
}


//----------------------------------------------------------------------------

// full version

void PlanX_vote_DIST(double x, double y, int cand_r, int cand_c)
{
  int r, c, r_start, i_max, i_lambda;
  double votes, vx, vy, vy_start, vpx, vpy, vpmag, inv_vpmag, vp_cos;
  CR_Matrix *mat;

  vy_start = MAX2(floor(y + 1), planx_voter_top);
  if (vy_start > planx_voter_bottom)
    return;
  r_start = (vy_start - planx_voter_top) / planx_voter_dy;

  if (x == 100) 
    printf("y = %.1lf, voter_top = %i, vy_start = %.1lf, r_start = %i\n", y, planx_voter_top, vy_start, r_start);

  for (vy = vy_start, r = r_start; vy < planx_voter_bottom; vy += planx_voter_dy, r++)
    for (vx = planx_voter_left, c = 0; vx < planx_voter_right; vx += planx_voter_dx, c++) {

      vpx = vx - x;
      vpy = vy - y;
      vpmag = sqrt(SQUARE(vpx) + SQUARE(vpy));
      inv_vpmag = 1.0 / vpmag;
      vpx *= inv_vpmag;
      vpy *= inv_vpmag;
      
      for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++) {    // iterate over scales

	if (planx_do_orientation_interpolation) {
	  // still don't know why the minus sign below is necessary
	  vp_cos = -vpx * MATRC(planx_voter_interp_nx[i_lambda], r, c) + vpy * MATRC(planx_voter_interp_ny[i_lambda], r, c);
	}
	else {
	  i_max = planx_voter_max[i_lambda][c + planx_voter_w * r];
	
	  // still don't know why the minus sign below is necessary
	  vp_cos = -vpx * planx_nx[i_max] + vpy * planx_ny[i_max];  
	}

	if (fabs(vp_cos) >= planx_cos_threshold) { 

	  // assuming no inv dist
	  inv_vpmag = 1.0;
	  if (planx_do_anisotropy)
	    inv_vpmag *= MATRC(planx_voter_anisotropy[i_lambda], r, c);
	  if (planx_do_strength)
	    inv_vpmag *= MATRC(planx_voter_strength[i_lambda], r, c);

	  MATRC(planx_candidate_wavelength_votes[i_lambda], cand_r, cand_c) = MATRC(planx_candidate_wavelength_votes[i_lambda], cand_r, cand_c) + inv_vpmag;
	}
      }
    }
}

//----------------------------------------------------------------------------

// vanishing point election :)

//   Votes(v) = 0
//   For all pixel locations p such that p != v
//       Compute vector vp = v - p, normalize vp
//       Fetch/computed stored vector n(s) in direction of dominant orientation at p at anisotropic scale s
//       Compute dot(s) = vp . n(s)
//       If abs(dot(s)) >= cos(half of filter angular resolution = 11.5 degrees) => Votes(v)++

// info needed: 
// (4) dominant orientation indices planx_voter_max

double PlanX_vote_INV_DIST(double x, double y)
{
  int r, c, r_start, i_max, i_lambda;
  double votes, vx, vy, vy_start, vpx, vpy, vpmag, inv_vpmag, vp_cos;
  //  double s, r_minus_y, c_minus_x;
  CR_Matrix *mat;

  votes = 0;

  vy_start = MAX2(floor(y + 1), planx_voter_top);
  if (vy_start > planx_voter_bottom)
    return 0;
  r_start = (vy_start - planx_voter_top) / planx_voter_dy;

  //  for (r = iy + 1, r_minus_y = -y + (double) (iy + 1); r < planx_voter_h; r++, r_minus_y++)
  //    for (c = 0, c_minus_x = -x; c < planx_voter_w; c++, c_minus_x++) {
  if (x == 200) 
    printf("y = %.1lf, voter_top = %i, vy_start = %.1lf, r_start = %i\n", y, planx_voter_top, vy_start, r_start);

  for (vy = vy_start, r = r_start; vy < planx_voter_bottom; vy += planx_voter_dy, r++)
    for (vx = planx_voter_left, c = 0; vx < planx_voter_right; vx += planx_voter_dx, c++) {

      vpx = vx - x;
      vpy = vy - y;
      vpmag = sqrt(SQUARE(vpx) + SQUARE(vpy));
      inv_vpmag = 1.0 / vpmag;
      vpx *= inv_vpmag;
      vpy *= inv_vpmag;
      
      for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++) {    // iterate over scales

	if (planx_do_orientation_interpolation) {
	  // still don't know why the minus sign below is necessary
	  vp_cos = -vpx * MATRC(planx_voter_interp_nx[i_lambda], r, c) + vpy * MATRC(planx_voter_interp_ny[i_lambda], r, c);
	}
	else {
	  i_max = planx_voter_max[i_lambda][c + planx_voter_w * r];
	
	  // still don't know why the minus sign below is necessary
	  vp_cos = -vpx * planx_nx[i_max] + vpy * planx_ny[i_max];  
	}

	if (fabs(vp_cos) >= planx_cos_threshold) { 

	  if (planx_do_no_inv_dist)
	    inv_vpmag = 1.0;
	  if (planx_do_anisotropy)
	    inv_vpmag *= MATRC(planx_voter_anisotropy[i_lambda], r, c);
	  if (planx_do_strength)
	    inv_vpmag *= MATRC(planx_voter_strength[i_lambda], r, c);

	  votes += inv_vpmag;
	}
      }
    }

  return votes;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

double PlanX_vote_POINT_LINE_DIST(double x, double y)
{
  int r, c, r_start, i_max, i_lambda;
  double votes, vx, vy, vy_start, vpmag;
  double bx, by, la, lb, lc;

  votes = 0;

  vy_start = MAX2(floor(y + 1), planx_voter_top);
  if (vy_start > planx_voter_bottom)
    return 0;
  r_start = (vy_start - planx_voter_top) / planx_voter_dy;

  for (vy = vy_start, r = r_start; vy < planx_voter_bottom; vy += planx_voter_dy, r++)
    for (vx = planx_voter_left, c = 0; vx < planx_voter_right; vx += planx_voter_dx, c++) {

      //      ax = (double) i;
      //      ay = (double) j;
 
      for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++) {    // iterate over scales

	i_max = planx_voter_max[i_lambda][c + planx_voter_w * r];
	
	// still don't know why the minus sign below is necessary
 	bx = vx - planx_nx[i_max];
 	by = vy + planx_ny[i_max];
 	      
	la = vy - by;
	lb = bx - vx;
	lc = vx*by - vy*bx;
	vpmag = fabs(x*la + y*lb + lc);

	if (planx_do_anisotropy)
	  vpmag *= MATRC(planx_voter_inv_anisotropy[i_lambda], r, c);
	if (planx_do_strength)
	  vpmag *= MATRC(planx_voter_inv_strength[i_lambda], r, c);

	votes += 1 / (1 + vpmag);

	//	votes += fabs(planx_nx[i_max]*j_plus_y + planx_ny[i_max]*i_minus_x);

	//	votes += 1 / (1 + fabs(planx_nx[i_max]*j_plus_y + planx_ny[i_max]*i_minus_x));
	//	votes += 1 / (1 + fabs(planx_ny[i_max]*j_plus_y + planx_nx[i_max]*i_minus_x));

	//	votes += 1 / (1 + vpmag);
      }
    }

  return votes;
}

//----------------------------------------------------------------------------

void PlanX_grab_CR_CG(CR_Image *im)
{
  old_PlanX_grab_CR_CG(im);
  return;

  if (planx_grab_type == PLANX_GRAB_WHO_VOTED_FOR_THE_VP)
    who_voted_for_the_VP_PlanX_grab_CR_CG(im);
  else if (planx_grab_type == PLANX_GRAB_STRIPWRITE_WHERE_IS_THE_VP)
    stripwrite_where_is_the_VP_PlanX_grab_CR_CG(im);
  else if (planx_grab_type == PLANX_GRAB_WHERE_IS_THE_VP)
    where_is_the_VP_PlanX_grab_CR_CG(im);
  else if (planx_grab_type == PLANX_GRAB_PF_WHERE_IS_THE_VP)
    pf_where_is_the_VP_PlanX_grab_CR_CG(im);
  else if (planx_grab_type == PLANX_GRAB_STRIP_PF_WHERE_IS_THE_VP)
    strip_pf_where_is_the_VP_PlanX_grab_CR_CG(im);
  else if (planx_grab_type == PLANX_GRAB_MOVIE_STRIP_PF_WHERE_IS_THE_VP)
    movie_strip_pf_where_is_the_VP_PlanX_grab_CR_CG(im);
  else if (planx_grab_type == PLANX_GRAB_PF_TRACKING_MOVIE)
    pf_tracking_movie_PlanX_grab_CR_CG(im);
  else if (planx_grab_type == PLANX_GRAB_STRIP_PF_TRACKING_MOVIE)
    strip_pf_tracking_movie_PlanX_grab_CR_CG(im);
  else {
    printf("unknown planx grab type\n");
    exit(1);
  }
}

//----------------------------------------------------------------------------

// this one's for figuring out who voted for the winning VP

void who_voted_for_the_VP_PlanX_grab_CR_CG(CR_Image *im)
{
  int r, c, i_lambda;
  double x, y, total, total_time, time_elapsed;
  char *filename;

  filename = (char *) calloc(256, sizeof(char));

  // image initialization

  for (planx_filename_index = planx_filename_start_index; planx_filename_index <= planx_filename_stop_index; planx_filename_index++) {

    PlanX_init_per_image(TRUE);

    // search for VP's voters

    printf("tallying voters for %i\n", planx_filename_index);
  
    CR_difftime();
    total_time = 0;
    
    // reset vote arrays
    
    for (r = 0; r < planx_voter_h; r++) 
      for (c = 0; c < planx_voter_w; c++) 
	for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++)     // iterate over scales
	  MATRC(planx_wavelength_voters[i_lambda], r, c) = 0.0;
    
    // tally voters for each wavelength

    PlanX_voters(MATRC(planx_vp_locations, planx_filename_index, 1)-1, MATRC(planx_vp_locations, planx_filename_index, 0)-1, 0);
    PlanX_voters(MATRC(planx_vp_locations, planx_filename_index, 3)-1, MATRC(planx_vp_locations, planx_filename_index, 2)-1, 1);
    PlanX_voters(MATRC(planx_vp_locations, planx_filename_index, 5)-1, MATRC(planx_vp_locations, planx_filename_index, 4)-1, 2);
    PlanX_voters(MATRC(planx_vp_locations, planx_filename_index, 7)-1, MATRC(planx_vp_locations, planx_filename_index, 6)-1, 3);
    
    time_elapsed = CR_difftime();
    total_time += time_elapsed;

    // write results (winner is max location in planx_candidate_votes)

    for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++) {
      
      sprintf(filename, "%s%i_%i_VOTERS_wav%i.dlm", 
	      planx_voter_filename[planx_filename_index],
	      planx_num_orientations, planx_num_wavelengths, 
	      i_lambda);
      
      write_dlm_CR_Matrix(filename, planx_wavelength_voters[i_lambda]);

      sprintf(filename, "%s%i_%i_VOTERS_ANISO_wav%i.dlm", 
	      planx_voter_filename[planx_filename_index],
	      planx_num_orientations, planx_num_wavelengths, 
	      i_lambda);
      
      write_dlm_CR_Matrix(filename, planx_voter_anisotropy[i_lambda]);
      
      sprintf(filename, "%s%i_%i_VOTERS_STRENGTH_wav%i.dlm", 
	      planx_voter_filename[planx_filename_index],
	      planx_num_orientations, planx_num_wavelengths, 
	      i_lambda);
      
      write_dlm_CR_Matrix(filename, planx_voter_strength[i_lambda]);
    }
    
    /*
    sprintf(filename, "%s%i_%i_VOTERS_per_slice_%i.dlm", 
	    planx_voter_filename[planx_filename_index],
	    planx_num_orientations, planx_num_wavelengths, 
	    planx_num_voter_slices);
      
    // angle goes from clockwise here
    write_dlm_CR_Matrix(filename, planx_voter_slices);
    */
  }

  
  exit(1);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

double planx_prior_x = 5;
double planx_prior_y = 5;

// where the particles start

void PlanX_samp_prior(CR_Vector *samp)
{
  double x, y, w, h, delta;

  w = 360;
  h = 180;
  delta = 10;

  // put in uniform distribution over image

  if (planx_prior_x >= w) {
    planx_prior_x = delta/2;
    planx_prior_y += delta;
  }

  samp->x[0] = planx_prior_x;
  samp->x[1] = planx_prior_y;

  planx_prior_x += delta;

    /*
  for (y = delta/2; y < h; y += delta)
    for (x = delta/2; x < w; x += delta) {
      samp->x[0] = x;
      samp->x[1] = y;
    }
    */
  //  printf("leaving samp prior\n");
  //  fflush(stdout);
}

//----------------------------------------------------------------------------

// random diffusion

void PlanX_samp_state(CR_Vector *old_samp, CR_Vector *new_samp)
{
  // nuthin' fancy...

  sample_gaussian_CR_Matrix(planx_con->Covalpha, new_samp, old_samp, planx_con->Covsqrt);
}

//----------------------------------------------------------------------------

// deterministic drift

void PlanX_dyn_state(CR_Vector *old_samp, CR_Vector *new_samp)
{
  // identity transform for now

  copy_CR_Vector(old_samp, new_samp);
}

//----------------------------------------------------------------------------

// bigger is better :)

//int counter = 0;

double PlanX_condprob_zx(void *Z, CR_Vector *samp)
{
  //  printf("%i: %lf %lf\n", counter++, samp->x[0], samp->x[1]);

  return pf_PlanX_vote(samp->x[0], samp->x[1], planx_voter_top, planx_voter_bottom, 2);  // last argument is wavelength number--more funky tricks later :)
}

//----------------------------------------------------------------------------

// bigger is better :)

//int counter = 0;

double PlanX_strip_condprob_zx(void *Z, CR_Vector *samp)
{
  return pf_PlanX_vote(samp->x[0], samp->x[1], planx_voter_current_top, planx_voter_current_top + planx_voter_strip_height, 2);  
  //  return 
  //   pf_PlanX_vote(samp->x[0], samp->x[1], planx_voter_current_top, planx_voter_current_top + planx_voter_strip_height, 1) +
  //   pf_PlanX_vote(samp->x[0], samp->x[1], planx_voter_current_top, planx_voter_current_top + planx_voter_strip_height, 2);  
}

//----------------------------------------------------------------------------

void strip_pf_tracking_movie_PlanX_grab_CR_CG(CR_Image *im)
{
  CR_Movie *mov;
  CR_Matrix *M;
  int i, s, x, y, x1, y1, x2, y2;
  double xmean, ymean, num_samps, dx, dy;
  CR_Matrix *flow_x, *flow_y, *vp_x, *vp_y;
  int num_flows, flow_delta_x, flow_start_x;
  int ct;

  FILE *fp;
  int length, thickness;

  ct = 0;

  vp_x = make_all_CR_Matrix("vp_x", 480, 1, 0.0);
  vp_y = make_all_CR_Matrix("vp_y", 480, 1, 0.0);
  
  num_flows = 9;
  flow_delta_x = 40;
  flow_start_x = 20;
  flow_x = make_all_CR_Matrix("flow_x", 480, num_flows, 0.0);
  flow_y = make_all_CR_Matrix("flow_y", 480, num_flows, 0.0);
  for (i = 0; i < num_flows; i++) {
    MATRC(flow_x, 0, i) = flow_start_x + flow_delta_x * i;
    MATRC(flow_y, 0, i) = planx_voter_bottom - planx_voter_strip_height/2 - ct;
  }

  num_samps = 360*180/100;

  M = read_dlm_CR_Matrix("C:\\Documents\\tex\\planx\\images\\striptrack\\strip_pf_half_dv23_1_50_1_wav2_samples.dlm", 142, 1944);
  planx_filename_start_index = 0;
  planx_filename_stop_index = 140;  // 140


  if (!planx_do_halfsize)
    CR_error("does not work unless halfsize");

  for (planx_voter_current_top = planx_voter_bottom - planx_voter_strip_height, planx_filename_index = 0;
       planx_voter_current_top >= planx_voter_top;
       planx_voter_current_top--, planx_filename_index++) {

    printf("%i\n", planx_voter_current_top);

      // open samples dlm
      
      if (planx_voter_current_top != planx_voter_bottom - planx_voter_strip_height) 
	ip_copy_CR_Image(planx_scratch_im, planx_im);
      else {
	// open avi for writing
	mov = write_avi_initialize_CR_Movie("C:\\Documents\\tex\\planx\\images\\striptrack\\strip_pf_half_dv23_1_50_1_wav2_newstate.avi", planx_im->w, planx_im->h, 30);
	fp = CR_emitEPS_setupfile("C:\\Documents\\tex\\planx\\images\\striptrack\\strip_pf_half_dv23_1_50_1_wav2_newstate.ps", 0, 0, 719, 479);
	emitEPS_CR_Image(fp, planx_big_im);

      }

      // figure out mean sample point location

      for (i = 0, xmean = ymean = 0; i < num_samps; i++) {

	xmean += MATRC(M, planx_filename_index, 3*i+2)*MATRC(M, planx_filename_index, 3*i);
	ymean += MATRC(M, planx_filename_index, 3*i+2)*MATRC(M, planx_filename_index, 3*i+1);
	
	/*
	x = (int) CR_round(MATRC(M, planx_filename_index, 3*i));
	y = (int) CR_round(MATRC(M, planx_filename_index, 3*i+1));

	IMXY_R(planx_im, x, y) = (unsigned char) 255;
	IMXY_G(planx_im, x, y) = (unsigned char) 255;
	IMXY_B(planx_im, x, y) = (unsigned char) 0;
	*/

      }

      printf("ymean = %lf, cur top = %i\n", ymean, planx_voter_current_top);

      drawcross(planx_im, CR_round(xmean), CR_round(ymean), 10, 255, 255, 0);

//       y = planx_voter_current_top;
//       for (i = 0; i < planx_w; i++) {
// 	IMXY_R(planx_im, i, y) = (unsigned char) 0;
// 	IMXY_G(planx_im, i, y) = (unsigned char) 255;
// 	IMXY_B(planx_im, i, y) = (unsigned char) 0;
//       }

      y = planx_voter_current_top + planx_voter_strip_height/2;
      for (i = 0; i < planx_w; i++) {
	IMXY_R(planx_im, i, y) = (unsigned char) 0;
	IMXY_G(planx_im, i, y) = (unsigned char) 255;
	IMXY_B(planx_im, i, y) = (unsigned char) 0;
      }


// draw it on image
 
      // write planx_im to avi
      if (planx_filename_index >= ct) {

	MATRC(vp_x, planx_filename_index-ct, 0) = xmean;
	MATRC(vp_y, planx_filename_index-ct, 0) = ymean;

	// update 
	for (i = 0; i < num_flows; i++) 
	  if (planx_filename_index > 0) {
	    
	    dx = xmean - MATRC(flow_x, planx_filename_index-ct - 1, i);
	    dy = ymean - MATRC(flow_y, planx_filename_index-ct - 1, i);
	    MATRC(flow_x, planx_filename_index-ct, i) = MATRC(flow_x, planx_filename_index-ct - 1, i) - dx/dy;
	    //	    MATRC(flow_x, planx_filename_index-ct, i) = MATRC(flow_x, planx_filename_index-ct - 1, i);
	    MATRC(flow_y, planx_filename_index-ct, i) = MATRC(flow_y, planx_filename_index-ct - 1, i) - 1;
	  }
	    
	// draw
	for (s = 0; s <= planx_filename_index-ct; s++) 
	  for (i = 0; i < num_flows; i++) {

	    x = MATRC(flow_x, s, i);
	    y = MATRC(flow_y, s, i);
	    IMXY_R(planx_im, x, y) = (unsigned char) 255;
	    IMXY_G(planx_im, x, y) = (unsigned char) 0;
	    IMXY_B(planx_im, x, y) = (unsigned char) 0;
	  
	  }
      
	write_avi_addframe_CR_Movie(planx_filename_index, planx_im, mov);
      }

      if ((planx_voter_current_top +  planx_voter_strip_height/2) < ymean)
	break;

  }

  length = 23;
  thickness = 10;

  //  int sstart = 1;
  int sstart = 10;

  for (s = sstart; s < planx_filename_index-ct; s++) 
    for (i = 0; i < num_flows; i++) {

      x1 = MATRC(flow_x, s-1, i);
      y1 = MATRC(flow_y, s-1, i);
      x2 = MATRC(flow_x, s, i);
      y2 = MATRC(flow_y, s, i);

      CR_emitEPS_draw_line(fp, 2*x1, 2*(240-y1), 2*x2, 2*(240-y2), 6, 0, 0, 0);
    }

  for (s = sstart; s < planx_filename_index-ct; s++) 
    for (i = 0; i < num_flows; i++) {

      x1 = MATRC(flow_x, s-1, i);
      y1 = MATRC(flow_y, s-1, i);
      x2 = MATRC(flow_x, s, i);
      y2 = MATRC(flow_y, s, i);

      CR_emitEPS_draw_line(fp, 2*x1, 2*(240-y1), 2*x2, 2*(240-y2), 2, 0, 255, 0);
    }

  for (s = sstart; s < planx_filename_index-ct; s++) {

      x1 = MATRC(vp_x, s-1, 0);
      y1 = MATRC(vp_y, s-1, 0);
      x2 = MATRC(vp_x, s, 0);
      y2 = MATRC(vp_y, s, 0);

      CR_emitEPS_draw_line(fp, 2*x1, 2*(240-y1), 2*x2, 2*(240-y2), 6, 0, 0, 0);

  }

  for (s = sstart; s < planx_filename_index-ct; s++) {

      x1 = MATRC(vp_x, s-1, 0);
      y1 = MATRC(vp_y, s-1, 0);
      x2 = MATRC(vp_x, s, 0);
      y2 = MATRC(vp_y, s, 0);

      CR_emitEPS_draw_line(fp, 2*x1, 2*(240-y1), 2*x2, 2*(240-y2), 2, 255, 255, 0);

  }

//   CR_emitEPS_draw_line(fp, x - length, 480-y, x + length, 480-y, thickness, 0, 0, 0);
//   CR_emitEPS_draw_line(fp, x, 480-y - length, x, 480-y + length, thickness, 0, 0, 0);
// 
//   length = 20;
//   thickness = 2;
// 
//   CR_emitEPS_draw_line(fp, x - length, 480-y, x + length, 480-y, thickness, 255, 255, 255);
//   CR_emitEPS_draw_line(fp, x, 480-y - length, x, 480-y + length, thickness, 255, 255, 255);

  CR_emitEPS_closefile(fp);

  // close avi
  write_avi_finish_CR_Movie(mov);

  exit(1);
}

//----------------------------------------------------------------------------

void pf_tracking_movie_PlanX_grab_CR_CG(CR_Image *im)
{
  CR_Movie *mov;
  CR_Matrix *M;
  int i, x, y;
  double xmean, ymean, num_samps;

  num_samps = 360*180/100;

  //  M = read_dlm_CR_Matrix("C:\\Documents\\tex\\planx\\images\\avi\\dvData_25_clip2\\pf_samples.dlm", 631, 1944);
  //  planx_filename_stop_index = 629;

  M = read_dlm_CR_Matrix("C:\\Documents\\tex\\planx\\images\\striptrack\\strip_pf_half_dv23_2_50_1_samples.dlm", 142, 1944);
  planx_filename_start_index = 0;
  planx_filename_stop_index = 140;  // 140


  if (!planx_do_halfsize)
    CR_error("does not work unless halfsize");

  for (planx_filename_index = planx_filename_start_index; planx_filename_index <= planx_filename_stop_index; planx_filename_index++) {
    {
      printf("%i\n", planx_filename_index);

      // open samples dlm
      
      if (planx_filename_index != 0) {

	if (planx_avi_input) {
	  get_frame_CR_Movie(planx_movie, planx_movie->currentframe);
	  planx_movie->currentframe++;
	  planx_big_im = planx_movie->im;
	
	  iplDecimateBlur(planx_big_im->iplim, planx_im->iplim, 1, 2, 1, 2, IPL_INTER_LINEAR, 3, 3);
	}
	else
	  ip_copy_CR_Image(planx_scratch_im, planx_im);
      }
      else {
	// open avi for writing
	//	mov = write_avi_initialize_CR_Movie("C:\\Documents\\tex\\planx\\images\\avi\\dvData_25_clip2\\samps.avi", planx_im->w, planx_im->h, 30);
	mov = write_avi_initialize_CR_Movie("C:\\Documents\\tex\\planx\\images\\striptrack\\strip_pf_half_dv23_1_50_1_sample.avi", planx_im->w, planx_im->h, 30);
      }

      // figure out mean sample point location

      for (i = 0, xmean = ymean = 0; i < num_samps; i++) {
	//	xmean += MATRC(planx_filename_index

	
	x = (int) CR_round(MATRC(M, planx_filename_index, 3*i));
	y = (int) CR_round(MATRC(M, planx_filename_index, 3*i+1));

	IMXY_R(planx_im, x, y) = (unsigned char) 255;
	IMXY_G(planx_im, x, y) = (unsigned char) 255;
	IMXY_B(planx_im, x, y) = (unsigned char) 0;
	

	//	set_pixel_rgb_CR_Image(x, y, 255, 255, 0, planx_im);

      }



// weightedx=x.*w;
// weightedy=y.*w;
// xmean=sum(weightedx');
// ymean=sum(weightedy');

// draw it on image
 
      // write planx_im to avi
      write_avi_addframe_CR_Movie(planx_filename_index, planx_im, mov);
    }
  }
  // close avi
  write_avi_finish_CR_Movie(mov);

  exit(1);
}

//----------------------------------------------------------------------------

// particle filtering to track the VP from strip to strip of an image

void strip_pf_where_is_the_VP_PlanX_grab_CR_CG(CR_Image *im)
{
  double time_elapsed;
  FILE *fp;
  CR_Matrix *planx_con_samps;
  int i, num_samps;

  // initialize particle filter

  num_samps = 360*180/100;
  planx_cov = make_diagonal_all_CR_Matrix("planx_cov", 2, 10.0);
  // planx_cov = init_diagonal_CR_Matrix("planx_cov", 10.0, 10.0);
  planx_con = make_CR_XCondensation(num_samps, make_CR_Vector("planx_state", 2), planx_cov,
				    PlanX_strip_condprob_zx, PlanX_samp_prior, PlanX_samp_state, PlanX_dyn_state);
  planx_con_samps = make_CR_Matrix("con_samps", 1, 3*planx_con->n);

  //  fp = fopen(planx_avi_pf_samples_filename, "w");
  fp = fopen("C:\\Documents\\tex\\planx\\images\\striptrack\\strip_pf_half_newhope_50_1_wav2_samples.dlm", "w");

  for (i = 0; i < planx_con->n; i++) {
    MATRC(planx_con_samps, 0, 3*i) = planx_con->s[i]->x[0];
    MATRC(planx_con_samps, 0, 3*i+1) = planx_con->s[i]->x[1];
    MATRC(planx_con_samps, 0, 3*i+2) = 1.0/(double)planx_con->n;
  }
  write_dlm_CR_Matrix(fp, planx_con_samps);
  fflush(fp);

  // end initialize particle filter

  // image initialization--only have to do this once for the whole image
    
  PlanX_init_per_image(TRUE);

  for (planx_voter_current_top = planx_voter_bottom - planx_voter_strip_height;
       planx_voter_current_top >= planx_voter_top;
       planx_voter_current_top--) {

    CR_difftime();

    // update particle filter

    update_CR_XCondensation(NULL, planx_con);

    for (i = 0; i < planx_con->n; i++) {
      MATRC(planx_con_samps, 0, 3*i) = planx_con->s[i]->x[0];
      MATRC(planx_con_samps, 0, 3*i+1) = planx_con->s[i]->x[1];
      MATRC(planx_con_samps, 0, 3*i+2) = planx_con->pi->x[i];
    }
    write_dlm_CR_Matrix(fp, planx_con_samps);
    fflush(fp);

    // end particle filter

    time_elapsed = CR_difftime();
    printf("%i (%i to %i) %lf\n", planx_voter_current_top, planx_voter_bottom - planx_voter_strip_height, planx_voter_top, time_elapsed);
  }
    
  exit(1);
}

//----------------------------------------------------------------------------

void update_constate(CR_Condensation *con, CR_Matrix *con_states, int update)
{
  int i;
  double x, y;

  x = 0;
  y = 0;

  for (i = 0; i < con->n; i++) {
    x += planx_con->s[i]->x[0] * planx_con->pi->x[i];
    y += planx_con->s[i]->x[1] * planx_con->pi->x[i];
  }

  MATRC(con_states, 0, 2*update) = x;
  MATRC(con_states, 0, 2*update+1) = y;
}

// particle filtering to track the VP from strip to strip of an image, and then from image to image

void movie_strip_pf_where_is_the_VP_PlanX_grab_CR_CG(CR_Image *im)
{
  double time_elapsed;
  FILE *fp;
  CR_Matrix *planx_con_states;
  int i, num_samps, update;
  double x, y;

  // initialize particle filter

  update = 0;

  num_samps = 360*180/100;
  planx_cov = make_diagonal_all_CR_Matrix("planx_cov", 2, 10.0);
  // planx_cov = init_diagonal_CR_Matrix("planx_cov", 10.0, 10.0);
  planx_con = make_CR_XCondensation(num_samps, make_CR_Vector("planx_state", 2), planx_cov,
				    PlanX_strip_condprob_zx, PlanX_samp_prior, PlanX_samp_state, PlanX_dyn_state);
  planx_con_states = make_all_CR_Matrix("con_states", 1, 480*2, 0.0);

  //  fp = fopen(planx_avi_pf_samples_filename, "w");
  fp = fopen("C:\\Documents\\tex\\planx\\images\\striptrack\\strip_pf_half_movie_wav2_states.dlm", "w");

  //  update_constate(planx_con, planx_con_states, update++);
  //  write_dlm_CR_Matrix(fp, planx_con_samps);
  //  fflush(fp);

  // end initialize particle filter

  for (planx_filename_index = planx_filename_start_index; planx_filename_index <= planx_filename_stop_index; planx_filename_index++) {

    // image initialization
    
    // image initialization--only have to do this once for the whole image
    
    PlanX_init_per_image(TRUE);
    
    for (planx_voter_current_top = planx_voter_bottom - planx_voter_strip_height;
	 planx_voter_current_top >= planx_voter_top;
	 planx_voter_current_top--) {
      
      CR_difftime();
      
      // update particle filter
      
      update_CR_XCondensation(NULL, planx_con);
      
      update_constate(planx_con, planx_con_states, update++);
      //    fflush(fp);
      
      // end particle filter
      
      time_elapsed = CR_difftime();
      printf("[%i] %i (%i to %i) %lf\n", planx_filename_index, planx_voter_current_top, planx_voter_bottom - planx_voter_strip_height, planx_voter_top, time_elapsed);
    }

    write_dlm_CR_Matrix(fp, planx_con_states);
    fflush(fp);
  }
  exit(1);
}

//----------------------------------------------------------------------------

// particle filtering to track the VP from frame to frame of an avi

void pf_where_is_the_VP_PlanX_grab_CR_CG(CR_Image *im)
{
  double time_elapsed;
  FILE *fp;
  CR_Matrix *planx_con_samps;
  int i, num_samps;

  // initialize particle filter

  num_samps = 360*180/100;
  planx_cov = make_diagonal_all_CR_Matrix("planx_cov", 2, 10.0);
  // planx_cov = init_diagonal_CR_Matrix("planx_cov", 10.0, 10.0);
  planx_con = make_CR_XCondensation(num_samps, make_CR_Vector("planx_state", 2), planx_cov,
				    PlanX_condprob_zx, PlanX_samp_prior, PlanX_samp_state, PlanX_dyn_state);
  planx_con_samps = make_CR_Matrix("con_samps", 1, 3*planx_con->n);

  fp = fopen(planx_avi_pf_samples_filename, "w");

  for (i = 0; i < planx_con->n; i++) {
    MATRC(planx_con_samps, 0, 3*i) = planx_con->s[i]->x[0];
    MATRC(planx_con_samps, 0, 3*i+1) = planx_con->s[i]->x[1];
    MATRC(planx_con_samps, 0, 3*i+2) = 1.0/(double)planx_con->n;
  }
  write_dlm_CR_Matrix(fp, planx_con_samps);
  fflush(fp);

  // end initialize particle filter

  for (planx_filename_index = planx_filename_start_index; planx_filename_index <= planx_filename_stop_index; planx_filename_index++) {

    // image initialization
    
    PlanX_init_per_image(TRUE);
    
    CR_difftime();

    // update particle filter

    update_CR_XCondensation(NULL, planx_con);

    for (i = 0; i < planx_con->n; i++) {
      MATRC(planx_con_samps, 0, 3*i) = planx_con->s[i]->x[0];
      MATRC(planx_con_samps, 0, 3*i+1) = planx_con->s[i]->x[1];
      MATRC(planx_con_samps, 0, 3*i+2) = planx_con->pi->x[i];
    }
    write_dlm_CR_Matrix(fp, planx_con_samps);
    fflush(fp);

    // end particle filter

    time_elapsed = CR_difftime();
    printf("%lf\n", time_elapsed);

    //    write_dlm_CR_Matrix(planx_avi_vps_filename, planx_vp_locations);
  }
    
  exit(1);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// this one's for iterating over the WHOLE image and counting votes per VP candidate
// the max is actually found

void where_is_the_VP_PlanX_grab_CR_CG(CR_Image *im)
{
  int r, c, i_lambda;
  double x, y, total, total_time, time_elapsed, maxval, minval;
  char *filename;

  filename = (char *) calloc(256, sizeof(char));

  for (planx_filename_index = planx_filename_start_index; planx_filename_index <= planx_filename_stop_index; planx_filename_index++) {

    // image initialization
    
    PlanX_init_per_image(TRUE);
    
    // search for best VP candidate
    
    printf("tallying votes for %i\n", planx_filename_index);
        
    CR_difftime();
    total_time = 0;
    
    // vote arrays get reset once per image and that's good enough for us
      
    // tally votes

    for (r = 0, y = planx_candidate_top; r < planx_candidate_h; r++, y += planx_candidate_dy) {
      for (c = 0, x = planx_candidate_left; c < planx_candidate_w; c++, x += planx_candidate_dx) 
	PlanX_vote_DIST(x, y, r, c, planx_voter_top, planx_voter_bottom);

      time_elapsed = CR_difftime();
      total_time += time_elapsed;
      printf("y = %i (%lf / %lf)\n", r, time_elapsed, total_time);
    }

    // write maxes out

    max_CR_Matrix(planx_candidate_votes, &maxval, &r, &c);
    MATRC(planx_vp_locations, planx_filename_index, 0) = r;
    MATRC(planx_vp_locations, planx_filename_index, 1) = c;
    sprintf(filename, "C:\\Documents\\tex\\planx\\images\\avi\\dvData_25_clip2\\wavsum_%i.pgm", planx_filename_index);
    interp_draw_CR_Matrix_CR_Image(planx_candidate_votes, planx_candidate_votes_im);
    write_CR_Image(filename, planx_candidate_votes_im); // (so we can make an AVI later)

    for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++) {
      max_CR_Matrix(planx_candidate_wavelength_votes[i_lambda], &maxval, &r, &c);
      MATRC(planx_vp_locations, planx_filename_index, 2 + 2*i_lambda) = r;
      MATRC(planx_vp_locations, planx_filename_index, 3 + 2*i_lambda) = c;
      sprintf(filename, "C:\\Documents\\tex\\planx\\images\\avi\\dvData_25_clip2\\wav%i_%i.pgm", i_lambda, planx_filename_index);
      interp_draw_CR_Matrix_CR_Image(planx_candidate_wavelength_votes[i_lambda], planx_candidate_wavelength_votes_im[i_lambda]);
      write_CR_Image(filename, planx_candidate_wavelength_votes_im[i_lambda]); // (so we can make an AVI later)
    }

    write_dlm_CR_Matrix(planx_avi_vps_filename, planx_vp_locations);
  }
    
  exit(1);
}

//----------------------------------------------------------------------------

// this one's for iterating over the strips and counting votes per VP candidate

void stripwrite_where_is_the_VP_PlanX_grab_CR_CG(CR_Image *im)
{
  int r, c, i_lambda;
  double x, y, total, total_time, time_elapsed;
  char *filename;

  for (planx_filename_index = planx_filename_start_index; planx_filename_index <= planx_filename_stop_index; planx_filename_index++) {

    // image initialization
    
    PlanX_init_per_image(TRUE);
    
    // search for best VP candidate
    
    printf("tallying votes for %i\n", planx_filename_index);
    
    filename = (char *) calloc(256, sizeof(char));
    
    CR_difftime();
    total_time = 0;
    
    /*
    for (planx_voter_current_top = planx_voter_top; 
	 planx_voter_current_top <= planx_voter_bottom - planx_voter_strip_height; 
	 planx_voter_current_top += planx_voter_strip_height) {
      
      // reset vote arrays
      
      for (r = 0; r < planx_candidate_h; r++) 
	for (c = 0; c < planx_candidate_w; c++) {
	  MATRC(planx_candidate_votes, r, c) = 0.0;
	  for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++)     // iterate over scales
	    MATRC(planx_candidate_wavelength_votes[i_lambda], r, c) = 0.0;
	}
      
      // tally votes
      
      for (r = 0, y = planx_candidate_top; r < planx_candidate_h; r++, y++) {
	for (c = 0, x = planx_candidate_left; c < planx_candidate_w; c++, x += planx_candidate_dx) 
	  PlanX_vote_DIST(x, y, r, c, planx_voter_current_top, planx_voter_current_top + planx_voter_strip_height);
	
	time_elapsed = CR_difftime();
	total_time += time_elapsed;
	printf("strip %i: y = %i (%lf / %lf)\n", planx_voter_current_top, r, time_elapsed, total_time);
      }
      
      // write results (winner is max location in planx_candidate_votes)
      
      for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++) {
	
	//	sprintf(filename, "%s%i_%i_DIST_%i_%i_%i_%i_from_%i_to_%i_wav%i.dlm", 
	sprintf(filename, "%s%i_%i_%i_from_%i_to_%i_wav%i.dlm", 
		planx_out_filename[planx_filename_index],
		//		planx_voter_filename[planx_filename_index],
		planx_num_orientations, planx_num_wavelengths, planx_voter_dx,
		planx_voter_current_top, planx_voter_current_top + planx_voter_strip_height, i_lambda);
	
	write_dlm_CR_Matrix(filename, planx_candidate_wavelength_votes[i_lambda]);
	
	//      printf("%s\n", filename);
      }
    }
    */
  }
  
  exit(1);
}

//----------------------------------------------------------------------------

int drew_once = FALSE;
int gabor_level = 0;
int interp = 0;

void old_PlanX_grab_CR_CG(CR_Image *im)
{
  int r, c, i_lambda;
  double x, y, total, total_time, time_elapsed;
  char *filename;

  // flip image vertically for proper opengl display and then draw

  //  glClear(GL_ACCUM_BUFFER_BIT); 


  vflip_CR_Image(planx_im, planx_scratch_im);
  glDrawPixels(planx_im->w, planx_im->h, GL_RGB, GL_UNSIGNED_BYTE, planx_scratch_im->data);

  // image initialization

  if (!drew_once)
    PlanX_init_per_image(TRUE);

  // draw gabor orientations


  //  glClear(GL_COLOR_BUFFER_BIT);

  draw_gabor_orientations(FALSE);
  //  draw_gabor_orientations(interp);
  if (interp)
    interp = FALSE;
  else
    interp = TRUE;

//   if (gabor_level < planx_num_wavelengths - 1)
//     gabor_level++;
//   else
//     gabor_level = 0;


  drew_once = TRUE;
  return;
  

  // draw multiscale PCA stuff

  /*
  int level = 3;
  int scale = pow(2, level);

  ogl_draw_orientation_CR_Matrices(scale*planx_pyr_blockframe, scale*planx_pyr_blockframe, scale, 480,
				   planx_pyr_voter_dx[level], planx_pyr_voter_dy[level], 
				   planx_pyr_voter_R_value[level], planx_pyr_voter_gradient_magnitude[level]);

  //  ogl_draw_scaled_CR_Matrix(8, planx_pyr_blockframe, planx_pyr_blockframe, 480, planx_pyr_voter_R_value[3]);

  drew_once = TRUE;

  //  for (r = 0; r < planx_pyr_voter_R_value[0]->rows; r++)
  //  for (c = 0; c < planx_pyr_voter_R_value[0]->cols; c++)

  return;
  */

  // search for best VP candidate

  printf("tallying votes...\n");

  filename = (char *) calloc(256, sizeof(char));

//   for (planx_do_strength = 0; planx_do_strength <= 0; planx_do_strength++)
//     //    for (planx_voter_top = 100; planx_voter_top <= 200; planx_voter_top += 50) 
//       for (planx_do_point_line_dist = 1; planx_do_point_line_dist <= 1; planx_do_point_line_dist++)
// 	for (planx_do_anisotropy = 0; planx_do_anisotropy <= 0; planx_do_anisotropy++) {

	  // figure out filename

	  // <path><input filename>_<num orientations>_<num wavelengths>_<POINT_LINE_DIST or INV_DIST>_<strength>_<anisotropy>_from_<voter top>.dlm

	  if (planx_do_point_line_dist)
	    sprintf(filename, "C:\\Documents\\tex\\planx\\images\\results\\unsurfaced_straight_640_480_%i_%i_POINT_LINE_DIST_%i_%i_%i_%i_from_%i_to_%i.dlm", 
		    planx_num_orientations, planx_num_wavelengths, 
		    planx_do_strength, planx_do_anisotropy, planx_do_no_inv_dist, planx_do_orientation_interpolation,
		    planx_voter_top, planx_voter_bottom);
	  else if (!planx_do_no_inv_dist)
	    sprintf(filename, "C:\\Documents\\tex\\planx\\images\\results\\unsurfaced_straight_640_480_%i_%i_INV_DIST_%i_%i_%i_%i_from_%i_to_%i.dlm", 
		    planx_num_orientations, planx_num_wavelengths, 
		    planx_do_strength, planx_do_anisotropy, planx_do_no_inv_dist, planx_do_orientation_interpolation,
		    planx_voter_top, planx_voter_bottom);


	  //	  CR_error("have to figure out filename");

	  // reset number of votes
	  
	  for (r = 0; r < planx_candidate_h; r++) 
	    for (c = 0; c < planx_candidate_w; c++) {
	      MATRC(planx_candidate_votes, r, c) = 0.0;
	      for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++)     // iterate over scales
		MATRC(planx_candidate_wavelength_votes[i_lambda], r, c) = 0.0;
	    }
	  
	  CR_difftime();
	  total_time = 0;
	  
	  for (r = 0, y = planx_candidate_top; r < planx_candidate_h; r++, y++) {
	    for (c = 0, x = planx_candidate_left; c < planx_candidate_w; c++, x += planx_candidate_dx) {
	      if (planx_do_point_line_dist) 
		CR_error("no point line dist allowed right now");
	      //		MATRC(planx_candidate_votes, r, c) = PlanX_vote_POINT_LINE_DIST(x, y);  
	      else
		//		MATRC(planx_candidate_votes, r, c) = PlanX_vote_INV_DIST(x, y);
		PlanX_vote_DIST(x, y, r, c);
	    }
	    time_elapsed = CR_difftime();
	    total_time += time_elapsed;
	    printf("y = %i (%lf / %lf)\n", r, time_elapsed, total_time);
	  }
	  
	  total_time += CR_difftime();
	  printf("FINAL time = %lf\n", total_time);
	  
	  printf("candidate w = %i, candidate h = %i (%i total)\n", planx_candidate_w, planx_candidate_h, planx_candidate_w * planx_candidate_h);
	  printf("voter w = %i, voter h = %i (%i total)\n", planx_voter_w, planx_voter_h, planx_voter_w * planx_voter_h);
	  
	  // winner is max location in planx_candidate_votes

	  if (planx_do_no_inv_dist) {
	    for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++) {

	      sprintf(filename, "C:\\Documents\\tex\\planx\\images\\results\\unsurfaced_straight_640_480_%i_%i_DIST_%i_%i_%i_%i_from_%i_to_%i_wav%i.dlm", 
		    planx_num_orientations, planx_num_wavelengths, 
		    planx_do_strength, planx_do_anisotropy, planx_do_no_inv_dist, planx_do_orientation_interpolation,
		    planx_voter_top, planx_voter_bottom, i_lambda);

	      write_dlm_CR_Matrix(filename, planx_candidate_wavelength_votes[i_lambda]);
	    }
	  }
	  else
	    write_dlm_CR_Matrix(filename, planx_candidate_votes);
	  //	write_dlm_CR_Matrix("C:\\Documents\\tex\\planx\\images\\results\\unsurf_straight_sub_5.0_deg_BELOW_INV_DIST_640_480_strength.dlm", planx_candidate_votes);
	  //	}

	  exit(1);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void oldold_PlanX_grab_CR_CG(CR_Image *im)
{
  int x, y, w, h, t;
  double s;
  int i, j;
  double s1, s2;
  int i1, i2;
  double vpx, vpy, vpmag, inv_vpmag, s_max;
  int i_max, i_lambda, i_theta;
  double vp_cos;
  double vote_side;
  CR_Matrix *mat;
  double p[3], l[3], a[3], b[3];
  double vote;

  CR_error("don't use old_grab() anymore");

  /*
  w = planx_voter_im->w;
  h = planx_voter_im->h;

  // flip image vertically for proper opengl display and then draw

  vflip_CR_Image(planx_voter_im, planx_voter_scratch_im);
  glDrawPixels(planx_voter_im->w, planx_voter_im->h, GL_RGB, GL_UNSIGNED_BYTE, planx_voter_scratch_im->data);

  // compute maxes

  printf("calculating maxes...\n");

  for (j = 0; j < h; j++)
    for (i = 0; i < w; i++) 
      for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++) {                // iterate over scales
	planx_voter_anisotropy[i_lambda][i + 301 * j] = 0.0;
	// NO_HORIZONTAL option
	//	for (i_theta = 1, s_max = -1.0; i_theta < planx_num_orientations; i_theta++) {  // iterate over orientations
	for (i_theta = 0, s_max = -1.0; i_theta < planx_num_orientations; i_theta++) {  // iterate over orientations
	  mat = planx_voter_dlm[i_theta + planx_num_orientations * i_lambda];
	  s = MATRC(mat, j, i);
	  planx_voter_anisotropy[i_lambda][i + 301 * j] += s;
	  if (s > s_max) {
	    s_max = s;
	    i_max = i_theta;
	  }
	}
	if (s_max == -1.0)
	  CR_error("bluh");
	planx_voter_max[i_lambda][i + 301 * j] = i_max;
	if (s_max)
	  planx_voter_anisotropy[i_lambda][i + 301 * j] /= s_max;
      }
    

  printf("tallying votes...\n");

  // vanishing point election :)
  
  //   Votes(v) = 0
  //   For all pixel locations p such that p != v
  //       Compute vector vp = v - p, normalize vp
  //       Fetch/computed stored vector n(s) in direction of dominant orientation at p at anisotropic scale s
  //       Compute dot(s) = vp . n(s)
  //       If abs(dot(s)) >= cos(half of filter angular resolution = 11.5 degrees) => Votes(v)++
  
  //  planx_cos_threshold = cos(0.5 * planx_delta_theta);  // DEG2RAD bug 
  planx_cos_threshold = cos(0.5 * DEG2RAD(planx_delta_theta));
  //  planx_cos_threshold = cos(DEG2RAD(1.0));
  
  for (y = 0; y < h; y++)
    for (x = 0; x < w; x++) 
      MATRC(planx_candidate_votes, y, x) = 0;

  // POINT_LINE_DIST option  
   p[2] = 1.0;
   a[2] = 1.0;
   b[2] = 1.0;

  //  for (y = 0; y < h; y++)
  for (y = 52; y <= 52; y++)
    for (x = 171; x <= 171; x++) {
      //  for (y = 0; y <= 300; y++)
      // for (x = 0; x < w; x++) {

  // POINT_LINE_DIST option  
       p[0] = (double) x;
       p[1] = (double) y;
      
      printf("x = %i, y = %i\n", x, y);

      // the BELOW option -- assumes horizontal horizon line, of course
      for (j = y + 1; j < h; j++)
	//      for (j = 0; j < h; j++)
	for (i = 0; i < w; i++) {
	  //	  if (i != x || j != y) {

// 	  vpx = (double) (i - x);
// 	  vpy = (double) (j - y);
// 	  vpmag = sqrt(SQUARE(vpx) + SQUARE(vpy));
// 	  inv_vpmag = 1.0 / vpmag;
// 	  vpx *= inv_vpmag;
// 	  vpy *= inv_vpmag;


	      // POINT_LINE_DIST option
 	    a[0] = (double) i;
 	    a[1] = (double) j;
 
	  for (i_lambda = 0; i_lambda < planx_num_wavelengths; i_lambda++) {    // iterate over scales
	    i_max = planx_voter_max[i_lambda][i + 301 * j];
	      
	      // POINT_LINE_DIST option
 	      //b[0] = a[0] + nx[i_max];
 	      b[0] = a[0] - planx_nx[i_max];
 	      b[1] = a[1] + planx_ny[i_max];
 	      
 	      l[0] = a[1]*b[2]-a[2]*b[1];
 	      l[1] = a[2]*b[0]-a[0]*b[2];
 	      l[2] = a[0]*b[1]-a[1]*b[0];
 	      vpmag = fabs(p[0]*l[0] + p[1]*l[1] + l[2]);
// 	      //	      printf("vpmap = %lf\n", vpmag);
 	      vote = 1 / (1 + vpmag);
// 	      //	      printf("vp %lf vote %lf\n", vpmag, vote);
// 	      MATRC(planx_candidate_votes, y, x) = MATRC(planx_candidate_votes, y, x) + vote;
// 	      MATRC(planx_candidate_votes, j, i) = MATRC(planx_candidate_votes, j, i) + vote;

	      // i don't know why i need the first negative here
	      // not POINT_LINE_DIST option
	      vp_cos = -vpx * planx_nx[i_max] + vpy * planx_ny[i_max];  
	      // vp_cos = vpx * cos + vpy * sin
	      // rotation bug
	      //	      vp_cos = -vpx * planx_ny[i_max] + vpy * planx_nx[i_max];  // rotated version
	      // vp_cos = -vpx * cos + vpy * sin

	      // next skip the threshold and just make the vote strength the inverse of 1 + the
	      // distance between the VP candidate point and the line defined by the dominant orientation
	      // incorporating anisotropy: let every orientation at each pixel vote, weighted by its fraction of the sum of the filter responses over all orientations at that scale.  
	      // still no use of total magnitude of filter responses at a pixel, though (because the range is different for different scales--how to account for this?)
	      
	      // not POINT_LINE_DIST option
	
	      if (fabs(vp_cos) >= planx_cos_threshold) {
		//		inv_vpmag *= planx_voter_anisotropy[i_lambda][i + 301 * j];

		// INV_DIST option
	      // not POINT_LINE_DIST option
		//		MATRC(planx_candidate_votes, y, x) = MATRC(planx_candidate_votes, y, x) + inv_vpmag; 
		MATRC(planx_candidate_votes, j, i) = MATRC(planx_candidate_votes, j, i) + inv_vpmag; 
		//		MATRC(planx_candidate_votes, y, x) = MATRC(planx_candidate_votes, y, x) + 1; 
	      } 
	
	    }
	}

      printf("%lf\n", MATRC(planx_candidate_votes, y, x));
      
//       vote_side = 0.5 * sqrt(MATRC(planx_candidate_votes, y, x));
//       glColor3f(1.0, 0.0, 0.0);
//       glRectf(x - vote_side, h - (y - vote_side), x + vote_side, h - (y + vote_side));
    }

  printf("done!\n");
  //  write_dlm_CR_Matrix("C:\\Documents\\tex\\planx\\images\\votetally\\unsurf_straight_sub_11.25_deg_BELOW_INV_DIST_x171_y52.dlm", planx_candidate_votes);
  exit(1);
*/

  // dominant orientation display

  if (cursor_flag) {

    // show best and 2nd best orientations at this location

    x = planx_x;
    y = planx_y;
    s = 10.0;

    /*
    s1 = s2 = 0.0;
    i1 = i2 = -1;

    for (i = 0; i < 8; i++) {
      s = 0.5 * MATRC(planx_voter_dlm[i+16], y, x);
      if (s > s1) {
	s2 = s1;
	i2 = i1;
	s1 = s;
	i1 = i;
      }
      else if (s > s2) {
	s2 = s;
	i2 = i;
      }
    }

    glColor3f(1.0, 0.0, 0.0);
    glRecti(x - 3, h - (y - 3), x + 3, h - (y + 3));

    glBegin(GL_LINES);

    // rotated for some reason looks right

    glColor3f(0.0, 1.0, 0.0);
    glVertex2f(x - s1*planx_ny[i1], h - (y + s1*planx_nx[i1]));
    glVertex2f(x + s1*planx_ny[i1], h - (y - s1*planx_nx[i1]));

    glColor3f(0.0, 0.0, 1.0);
    glVertex2f(x - s2*planx_ny[i2], h - (y + s2*planx_nx[i2]));
    glVertex2f(x + s2*planx_ny[i2], h - (y - s2*planx_nx[i2]));

    // unrotated

//     for (i = 0; i < 8; i++) {
//       s = 0.5 * MATRC(planx_voter_dlm[i+24], y, x);
// //       glVertex2f(x + s*planx_nx[i], h - (y + s*planx_ny[i]));
// //       glVertex2f(x - s*planx_nx[i], h - (y - s*planx_ny[i]));
// 
//       glVertex2f(x - s*planx_ny[i], h - (y + s*planx_nx[i]));
//       glVertex2f(x + s*planx_ny[i], h - (y - s*planx_nx[i]));
    }

    glEnd();
*/
  }
}

//----------------------------------------------------------------------------

void PlanX_end_CR_CG(CR_Image *im)
{

}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// The image alternates between showing the target and showing a distractor

CR_Image *OrderParamTask1_init_CR_CG(char *name)
{
  CR_Image *im;
  CR_Vector *meanmean, *targetmeanmean;
  CR_Matrix *meancov, *cov, *targetmeancov, *targetcov;
  double b_mean, b_var, t_mean, t_var;

  //  im = make_CR_Image(100, 100, 0);
  im = make_CR_Image(128, 128, 0);
  sprintf(name, "OrderParamTask1");

  grf_im = make_CR_Image(im->w, im->h, 0);
  scratch_grf_im = make_CR_Image(im->w, im->h, 0);

  b_mean = 145.0;     // 100
  b_var = 20.0;  // 20

  t_mean = 155.0;         // 200
  t_var = 20.0;      // 20

  // background
  
  meanmean = init_CR_Vector("meanmean", 1, b_mean); // 100.0
  meancov = init_CR_Matrix("meancov", 1, 1, 1.0);  // 400.0
  cov = init_CR_Matrix("cov", 1, 1, b_var);  // 20.0

  grf_mean = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_covsqrt = make_CR_Random_Field(CR_MATRIX, im->h, im->w);
  grf_val = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_alpha = make_CR_Matrix("grf_alpha", im->h, im->w);

  //  make_CR_Random_Field(im->w, im->h, meanmean, meancov, cov, grf_mean, grf_cov, grf_val);
  make_gaussian_CR_Random_Field(im->w, im->h, meanmean, NULL, cov, grf_alpha, grf_mean, grf_covsqrt, grf_val);

  // background-sized target

  targetmeanmean = init_CR_Vector("targetmeanmean", 1, t_mean); // 200.0
  targetmeancov = init_CR_Matrix("targetmeancov", 1, 1, 1.0);  // 400.0
  targetcov = init_CR_Matrix("targetcov", 1, 1, t_var);  // 20.0

  grf_target_mean = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_target_covsqrt = make_CR_Random_Field(CR_MATRIX, im->h, im->w);
  grf_target_val = make_CR_Random_Field(CR_VECTOR, im->h, im->w);
  grf_target_alpha = make_CR_Matrix("grf_target_alpha", im->h, im->w);

  //  make_CR_Random_Field(im->w, im->h, targetmeanmean, targetmeancov, targetcov, grf_target_mean, grf_target_cov, grf_target_val);
  make_gaussian_CR_Random_Field(im->w, im->h, targetmeanmean, NULL, targetcov, grf_target_alpha, grf_target_mean, grf_target_covsqrt, grf_target_val);

  CR_difftime();

  // finish

  return im;
}

//----------------------------------------------------------------------------

void OrderParamTask1_grab_CR_CG(CR_Image *im)
{
  // change visibility state?

  time_now += CR_difftime();
  if (time_now > 2.0) {
    time_now = 0;
    background_visible = !background_visible;
  }

  // draw appropriate image

  if (background_visible) {
    sample_gaussian_CR_Random_Field(grf_alpha, grf_mean, grf_covsqrt, grf_val);                      // calc. background appearance
    convert_to_image_CR_Random_Field(grf_val, grf_im);                    // make background visible
  }
  else {
    sample_gaussian_CR_Random_Field(grf_target_alpha, grf_target_mean, grf_target_covsqrt, grf_target_val); // calc. target appearance
    convert_to_image_CR_Random_Field(grf_target_val, grf_im);             // make target visible
  }

  // flip image vertically for proper opengl display and then draw

  vflip_CR_Image(grf_im, scratch_grf_im);
  glDrawPixels(grf_im->w, grf_im->h, GL_RGB, GL_UNSIGNED_BYTE, scratch_grf_im->data);
}

//----------------------------------------------------------------------------

void OrderParamTask1_end_CR_CG(CR_Image *im)
{

}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// list CG function options
// keep the menu indices > 16 to not interfere with other 
// kinds of sources listed on the same menu

#define CG_MENU_BASE     17

void add_submenu_CR_CG()
{
  glutAddMenuEntry("test1", CG_MENU_BASE);
  glutAddMenuEntry("OrderParamTask1", CG_MENU_BASE + 1);
  glutAddMenuEntry("OrderParamTask2", CG_MENU_BASE + 2);
  glutAddMenuEntry("OrderParamTask3", CG_MENU_BASE + 3);
  glutAddMenuEntry("FisherInfo1", CG_MENU_BASE + 4);
  glutAddMenuEntry("Road1", CG_MENU_BASE + 5);
  glutAddMenuEntry("Track1", CG_MENU_BASE + 6);
  glutAddMenuEntry("Track2", CG_MENU_BASE + 7);
  glutAddMenuEntry("TrackQuadratics", CG_MENU_BASE + 8);
  glutAddMenuEntry("Track3DQuadratic", CG_MENU_BASE + 9);
  glutAddMenuEntry("Track3DClothoid", CG_MENU_BASE + 10);
  glutAddMenuEntry("DrapeCamera", CG_MENU_BASE + 11);
  glutAddMenuEntry("DrapeLaser", CG_MENU_BASE + 12);
  glutAddMenuEntry("TextureFlow", CG_MENU_BASE + 13);
  glutAddMenuEntry("TextureFlow2", CG_MENU_BASE + 14);
  glutAddMenuEntry("TextureFlow3", CG_MENU_BASE + 15);
  glutAddMenuEntry("CalibrateLaserCamera", CG_MENU_BASE + 16);
  glutAddMenuEntry("CalibrateLaserCamera2", CG_MENU_BASE + 17);
  glutAddMenuEntry("TestCalibration", CG_MENU_BASE + 18);
  glutAddMenuEntry("TrackDirtRoad", CG_MENU_BASE + 19);
  glutAddMenuEntry("TestNML", CG_MENU_BASE + 20);
  glutAddMenuEntry("ITG", CG_MENU_BASE + 21);
  glutAddMenuEntry("MapRoad", CG_MENU_BASE + 22);
  glutAddMenuEntry("UNUSED", CG_MENU_BASE + 23);
  glutAddMenuEntry("Butterfly", CG_MENU_BASE + 24);
  glutAddMenuEntry("Puddle", CG_MENU_BASE + 25);
  glutAddMenuEntry("SuperResSigns1", CG_MENU_BASE + 26);
  glutAddMenuEntry("AnnotateVideo", CG_MENU_BASE + 27);
  glutAddMenuEntry("Cues1", CG_MENU_BASE + 28);
  glutAddMenuEntry("CalibratePolyCam", CG_MENU_BASE + 29);
  glutAddMenuEntry("PoleTrunkFinder", CG_MENU_BASE + 30);
  glutAddMenuEntry("FlatSignFinder", CG_MENU_BASE + 31);
  glutAddMenuEntry("NIST_Road_Pick_Random_Frames", CG_MENU_BASE + 32);
  glutAddMenuEntry("NIST_Road_Extract_Picked_Frames", CG_MENU_BASE + 33);
  glutAddMenuEntry("NIST_Road_Compute_Features", CG_MENU_BASE + 34);
  glutAddMenuEntry("NIST_Road_Segment", CG_MENU_BASE + 35);
  glutAddMenuEntry("CVPR_2003", CG_MENU_BASE + 36);
  glutAddMenuEntry("CVPR_2003_Track3DQuadratic", CG_MENU_BASE + 37);
  glutAddMenuEntry("Laser_to_AVI", CG_MENU_BASE + 38);
  glutAddMenuEntry("IV2003", CG_MENU_BASE + 39);
  glutAddMenuEntry("PlanX", CG_MENU_BASE + 40);
}

//----------------------------------------------------------------------------

// invoke chosen CG function -- needs to match menu index assigned above

void handle_submenu_CR_CG(int value, CG_init *init, CG_grab *grab, CG_end *end)
{
  if (value == CG_MENU_BASE) {
    *init = test1_init_CR_CG; *grab = test1_grab_CR_CG; *end = test1_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 1) {
    *init = OrderParamTask1_init_CR_CG; *grab = OrderParamTask1_grab_CR_CG; *end = OrderParamTask1_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 2) {
    *init = OrderParamTask2_init_CR_CG; *grab = OrderParamTask2_grab_CR_CG; *end = OrderParamTask2_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 3) {
    *init = OrderParamTask3_init_CR_CG; *grab = OrderParamTask3_grab_CR_CG; *end = OrderParamTask3_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 4) {
    *init = FisherInfo1_init_CR_CG; *grab = FisherInfo1_grab_CR_CG; *end = FisherInfo1_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 5) {
    *init = Road1_init_CR_CG; *grab = Road1_grab_CR_CG; *end = Road1_end_CR_CG;
 }
  else if (value == CG_MENU_BASE + 6) {
    *init = Track1_init_CR_CG; *grab = Track1_grab_CR_CG; *end = Track1_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 7) {
    *init = Track2_init_CR_CG; *grab = Track2_grab_CR_CG; *end = Track2_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 8) {
    *init = Track3_init_CR_CG; *grab = Track3_grab_CR_CG; *end = Track3_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 9) {
    *init = Track3DQuadratic_init_CR_CG; *grab = Track3DQuadratic_grab_CR_CG; *end = Track3DQuadratic_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 10) {
    *init = Track3DClothoid_init_CR_CG; *grab = Track3DClothoid_grab_CR_CG; *end = Track3DClothoid_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 11) {
    *init = DrapeCamera_init_CR_CG; *grab = DrapeCamera_grab_CR_CG; *end = DrapeCamera_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 12) {
    *init = DrapeLaser_init_CR_CG; *grab = DrapeLaser_grab_CR_CG; *end = DrapeLaser_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 13) {
    *init = TextureFlow_init_CR_CG; *grab = TextureFlow_grab_CR_CG; *end = TextureFlow_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 14) {
    *init = TextureFlow2_init_CR_CG; *grab = TextureFlow2_grab_CR_CG; *end = TextureFlow2_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 15) {
    *init = TextureFlow3_init_CR_CG; *grab = TextureFlow3_grab_CR_CG; *end = TextureFlow3_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 16) {
    *init = CalibrateLaserCamera_init_CR_CG; *grab = CalibrateLaserCamera_grab_CR_CG; *end = CalibrateLaserCamera_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 17) {
    *init = CalibrateLaserCamera2_init_CR_CG; *grab = CalibrateLaserCamera2_grab_CR_CG; *end = CalibrateLaserCamera2_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 18) {
    *init = TestCalibration_init_CR_CG; *grab = TestCalibration_grab_CR_CG; *end = TestCalibration_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 19) {
    *init = TrackDirtRoad_init_CR_CG; *grab = TrackDirtRoad_grab_CR_CG; *end = TrackDirtRoad_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 20) {
    *init = TestNML_init_CR_CG; *grab = TestNML_grab_CR_CG; *end = TestNML_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 21) {
    *init = ITG_init_CR_CG; *grab = ITG_grab_CR_CG; *end = ITG_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 22) {
    *init = MapRoad_init_CR_CG; *grab = MapRoad_grab_CR_CG; *end = MapRoad_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 23) {
    *init = NIST_Road_Compute_Features_init_CR_CG; *grab = NIST_Road_Compute_Features_grab_CR_CG; *end = NIST_Road_Compute_Features_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 24) {
    *init = Butterfly_init_CR_CG; *grab = Butterfly_grab_CR_CG; *end = Butterfly_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 25) {
    *init = Puddle_init_CR_CG; *grab = Puddle_grab_CR_CG; *end = Puddle_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 26) {
    *init = SuperResSigns1_init_CR_CG; *grab = SuperResSigns1_grab_CR_CG; *end = SuperResSigns1_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 27) {
    *init = AnnotateVideo_init_CR_CG; *grab = AnnotateVideo_grab_CR_CG; *end = AnnotateVideo_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 28) {
    *init = Cues1_init_CR_CG; *grab = Cues1_grab_CR_CG; *end = Cues1_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 29) {
    *init = CalibratePolyCam_init_CR_CG; *grab = CalibratePolyCam_grab_CR_CG; *end = CalibratePolyCam_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 30) {
    *init = PoleTrunkFinder_init_CR_CG; *grab = PoleTrunkFinder_grab_CR_CG; *end = PoleTrunkFinder_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 31) {
    *init = FlatSignFinder_init_CR_CG; *grab = FlatSignFinder_grab_CR_CG; *end = FlatSignFinder_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 32) {
    *init = NIST_Road_Pick_Random_Frames_init_CR_CG; *grab = NIST_Road_Pick_Random_Frames_grab_CR_CG; *end = NIST_Road_Pick_Random_Frames_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 33) {
    *init = NIST_Road_Extract_Picked_Frames_init_CR_CG; *grab = NIST_Road_Extract_Picked_Frames_grab_CR_CG; *end = NIST_Road_Extract_Picked_Frames_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 34) {
    *init = NIST_Road_Compute_Features_init_CR_CG; *grab = NIST_Road_Compute_Features_grab_CR_CG; *end = NIST_Road_Compute_Features_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 35) {
    *init = NIST_Road_Segment_init_CR_CG; *grab = NIST_Road_Segment_grab_CR_CG; *end = NIST_Road_Segment_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 36) {
    *init = CVPR_2003_init_CR_CG; *grab = CVPR_2003_grab_CR_CG; *end = CVPR_2003_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 37) {
    *init = CVPR_2003_Track3DQuadratic_init_CR_CG; *grab = CVPR_2003_Track3DQuadratic_grab_CR_CG; *end = CVPR_2003_Track3DQuadratic_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 38) {
    *init = Laser_to_AVI_init_CR_CG; *grab = Laser_to_AVI_grab_CR_CG; *end = Laser_to_AVI_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 39) {
    *init = IV2003_init_CR_CG; *grab = IV2003_grab_CR_CG; *end = IV2003_end_CR_CG;
  }
  else if (value == CG_MENU_BASE + 40) {
    *init = PlanX_init_CR_CG; *grab = PlanX_grab_CR_CG; *end = PlanX_end_CR_CG;
  }
}

//----------------------------------------------------------------------------

int handle_source_menu_CR_CG(int value, CR_CG *cg)
{
  int handled = FALSE;

  switch (value) {

    // Play (if movie)
    
  case CR_SOURCE_MENU_PLAY:
    cg->state = CR_CG_PLAY;
    handled = TRUE;
    break;
    
    // Stop (if movie)
    
  case CR_SOURCE_MENU_STOP:
    cg->state = CR_CG_STOP;
    handled = TRUE;
    break;
    
    // Increment frame
    
  case CR_SOURCE_MENU_INCREMENT:
    get_frame_CR_CG(cg);
    cg->state = CR_CG_STOP;
    handled = TRUE;
    break;    
  }

  return handled;
}

//----------------------------------------------------------------------------

void add_source_menu_CR_CG()
{
  glutAddMenuEntry("Play", CR_SOURCE_MENU_PLAY);
  glutAddMenuEntry("Stop", CR_SOURCE_MENU_STOP);
  glutAddMenuEntry("Advance 1 frame", CR_SOURCE_MENU_INCREMENT);
  glutAddMenuEntry("--------------------------", CR_SOURCE_MENU_DIVIDER);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void get_frame_CR_CG(CR_CG *cg)
{
  //  //  cg->im->ratio = cg->win_im_ratio;

  // draw frame buffer and immediately save to image (-1 b/c diff b/t vflip & glPixelZoom alignment)
  
  cg->grab(cg->im);

  //  //  glutSwapBuffers();

  //  //  glRasterPos2i(0, 0);
  ////   glPixelZoom(1.0 / cg->im->ratio, -1.0 / cg->im->ratio);

  //  glPixelStorei(GL_PACK_ALIGNMENT, 1);

  glReadPixels(0, 0, cg->im->w, cg->im->h, GL_RGB, GL_UNSIGNED_BYTE, cg->scratch_im->data);
    
  // flip image for proper orientation
    
  vflip_CR_Image(cg->scratch_im, cg->im);

  //  //  write_CR_Image("scratch.ppm", cg->scratch_im);
  //  //  write_CR_Image("im.ppm", cg->im);

  if (cg->state == CR_CG_START)
    cg->state = CR_CG_STOP;
}

//----------------------------------------------------------------------------

CR_CG *make_CR_CG(CG_init init_cg, CG_grab grab_cg, CG_end end_cg)
{
  CR_CG *cg;

  cg = (CR_CG *) CR_malloc(sizeof(CR_CG));
  cg->name = (char *) CR_calloc(CR_CG_MAXNAME, sizeof(char));

  cg->init = init_cg;
  cg->grab = grab_cg;
  cg->end = end_cg;

  cg->state = CR_CG_START;

  cg->im = cg->init(cg->name);

  return cg;
}

//----------------------------------------------------------------------------

void free_CR_CG(CR_CG *cg)
{
  CR_free_calloc(cg->name, CR_CG_MAXNAME, sizeof(char));

  cg->end(cg->im);

  free_CR_Image(cg->im);
  CR_free_malloc(cg, sizeof(CR_CG));
}


//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void special_batch_job()
{
  int i, j, r, c;
  CR_ProbDist *b, *t;
  double bhat;
  int im_w, im_h, shift, scale, w_tiles, h_tiles;
  int targ_side, prob_dist_num; 
  int max_positions, num_positions, tiling;
  double num_position_errors, num_old_position_errors;
  int num_better_positions, num_tied_positions, tied_positions, better_positions;
  int target_x, target_y, target_w, target_h, target_size;
  double t_prob1, b_prob1;
  double targ_loglike_tb, loglike_tb, mean_targ_loglike_tb, var_targ_loglike_tb, mean_back_loglike_tb, var_back_loglike_tb, diff_loglike_tb;
  int num_targ_loglike_tb, num_back_loglike_tb;
  double exp_errors, pred_error_rate, coeff, logQ, N;
  FILE *batch_fp;
  time_t ltime;
  struct tm *today;
  char startstr[128];
  CR_Matrix *fisher;
  double targ_tc_prob, temp, pred_lower_num_tied_positions, pred_upper_num_tied_positions;

  batch_fp = fopen("batch.txt", "w");
  fprintf(batch_fp, "--------------------------------------------------------------\n");

  max_positions = 1000; // 10000

  im_w = 64;
  im_h = 64;
  //  im_w = 16;
  //  im_h = 16;

  shift = 0.0;
  scale = 255.0;

  target_prob = make_CR_Vector("target_prob", 2);
  background_prob = make_CR_Vector("background_prob", 2);

  t = make_distribution_CR_Info(target_prob);
  b = make_distribution_CR_Info(background_prob);

  // target

  grf_target_cumprob = make_CR_Random_Field(CR_VECTOR, im_w, im_h);
  grf_target_val = make_CR_Random_Field(CR_VECTOR, im_w, im_h);
  make_discrete_CR_Random_Field(im_w, im_h, target_prob, grf_target_cumprob, grf_target_val);

  // background

  grf_cumprob = make_CR_Random_Field(CR_VECTOR, im_h, im_w);
  grf_val = make_CR_Random_Field(CR_VECTOR, im_h, im_w);
  make_discrete_CR_Random_Field(im_w, im_h, background_prob, grf_cumprob, grf_val);

  fisher = make_CR_Matrix("fisher", 2, 2);

  for (targ_side = 2; targ_side <= im_w / 2; targ_side *= 2) {
  //  for (targ_side = 16; targ_side <= 32; targ_side *= 2) {
    N = targ_side * targ_side;
    w_tiles = im_w / targ_side;
    h_tiles = im_h / targ_side;
    coeff = pow(N + 1, 4);  // raised to J^2, where J is alphabet size
    for (t_prob1 = 0.1; t_prob1 <= 0.5; t_prob1 += 0.1) {  // 0.1
      set_CR_Vector(target_prob, t_prob1, 1 - t_prob1);
      set_distribution_CR_Info(t, target_prob);
      for (b_prob1 = 0.9; b_prob1 >= 0.5; b_prob1 -= 0.1) {  // 0.9
	set_CR_Vector(background_prob, b_prob1, 1 - b_prob1);
	set_distribution_CR_Info(b, background_prob);
	bhat = bhattacharyya_bound_CR_Info(b, t);
       
	set_discrete_CR_Random_Field(targ_side, targ_side, shift, scale, target_prob, grf_target_cumprob, grf_target_val);
	set_discrete_CR_Random_Field(im_w, im_h, shift, scale, background_prob, grf_cumprob, grf_val);

	for (tiling = 1; tiling >= 0; tiling--) {

	  num_targ_loglike_tb = 1; num_back_loglike_tb = 1;
	  mean_targ_loglike_tb = 0; var_targ_loglike_tb = 0;
	  mean_back_loglike_tb = 0; var_back_loglike_tb = 0;

	  num_position_errors = num_old_position_errors = 0;
	  num_better_positions = num_tied_positions = 0;
	  pred_lower_num_tied_positions = pred_upper_num_tied_positions = 0;
	  for (num_positions = 0; num_positions < max_positions; num_positions++) {
	    
	    // generate image
	    
	    sample_discrete_CR_Random_Field(shift, scale, grf_target_cumprob, grf_target_val);
	    sample_discrete_CR_Random_Field(shift, scale, grf_cumprob, grf_val);
	    
	    // no tiling

	    if (!tiling) {
	      target_x = ranged_uniform_int_CR_Random(0, im_w - targ_side);
	      target_y = ranged_uniform_int_CR_Random(0, im_h - targ_side);

// 	      for (r = 0; r < fisher->rows; r++)
// 		for (c = 0; c < fisher->cols; c++) 
// 		  MATRC(fisher, r, c) = rc_fisher_information_CR_Random_Field(r, c, 
// 									      targ_side, targ_side, 
// 									      grf_val, 
// 									      shift, scale, 
// 									      target_prob, 
// 									      shift, scale, 
// 									      background_prob);
// 	      print_CR_Matrix(fisher);
	    }

	    // tiling
	    
	    else {
	      target_x = targ_side * ranged_uniform_int_CR_Random(0, w_tiles - 1);
	      target_y = targ_side * ranged_uniform_int_CR_Random(0, h_tiles - 1);	      

	      logQ = log2((double) (w_tiles * h_tiles - 1)) / N;
	      pred_error_rate = pow(2, -N * (2 * bhat - logQ));
	    }

	    overlay_CR_Random_Field(target_x, target_y, grf_target_val, grf_val); 
	    
	    // test image
	    
	    better_positions = tied_positions = 0;
	    
	    targ_loglike_tb = log_likelihood_ratio_discrete_CR_Random_Field(target_x, target_y, 
									    targ_side, targ_side,
									    grf_val, 
									    shift, scale, target_prob, 
									    shift, scale, background_prob); 
	    diff_loglike_tb = targ_loglike_tb - mean_targ_loglike_tb;
	    mean_targ_loglike_tb += diff_loglike_tb / (double) num_targ_loglike_tb;
	    var_targ_loglike_tb += diff_loglike_tb * (targ_loglike_tb - mean_targ_loglike_tb);
	    num_targ_loglike_tb++;

	    if (!tiling) {

	      for (j = 0; j < im_h - targ_side; j++)
		for (i = 0; i < im_w - targ_side; i++) {
		  if (j != target_y || i != target_x) {
		    loglike_tb = log_likelihood_ratio_discrete_CR_Random_Field(i, j, 
									       targ_side, targ_side, 
									       grf_val, 
									       shift, scale, target_prob, 
									       shift, scale, background_prob); 
		    diff_loglike_tb = loglike_tb - mean_back_loglike_tb;
		    mean_back_loglike_tb += diff_loglike_tb / (double) num_back_loglike_tb;
		    var_back_loglike_tb += diff_loglike_tb * (loglike_tb - mean_back_loglike_tb);
		    num_back_loglike_tb++;

		    if (CR_EQUAL(loglike_tb, targ_loglike_tb))
		      tied_positions++;
		    else if (loglike_tb > targ_loglike_tb)
		      better_positions++;
		  }
		}
	    }
	    else {
	      for (j = 0; j < h_tiles; j++)
		for (i = 0; i < w_tiles; i++) {
		  if (j * targ_side != target_y || i * targ_side != target_x) {
		    loglike_tb = log_likelihood_ratio_discrete_CR_Random_Field(targ_side * i, targ_side * j, 
									       targ_side, targ_side, 
									       grf_val, 
									       shift, scale, target_prob, 
									       shift, scale, background_prob); 
		    diff_loglike_tb = loglike_tb - mean_back_loglike_tb;
		    mean_back_loglike_tb += diff_loglike_tb / (double) num_back_loglike_tb;
		    var_back_loglike_tb += diff_loglike_tb * (loglike_tb - mean_back_loglike_tb);
		    num_back_loglike_tb++;

		    if (CR_EQUAL(loglike_tb, targ_loglike_tb))
		      tied_positions++;
		    else if (loglike_tb > targ_loglike_tb)
		      better_positions++;
		  }
		}
	    }

	    num_better_positions += better_positions;
	    num_tied_positions += tied_positions;
	    // if something's better, we're definitely going to be wrong
	    if (better_positions > 0) {   
	      num_position_errors++; 
	      num_old_position_errors++;
	    }
	    // if nothing's better but something's tied, choose uniformly (and possibly be wrong)
	    else if (tied_positions > 0) 
	      num_position_errors += (double) (tied_positions) / (double) (tied_positions + 1); 

	    if (tiling) {
	      targ_tc_prob = type_class_probability_discrete_CR_Random_Field(target_x, target_y, 
									     targ_side, targ_side,
									     grf_val,
									     shift, scale, background_prob);
	      //	      print_CR_Vector(target_prob);
	      //	      printf("num ties = %i out of Q = %i\n", tied_positions, h_tiles * w_tiles - 1);
	      //	      temp = 1 / pow(targ_side * targ_side + 1, background_prob->rows);
	      //	      printf("lower targ type class prob = %lf, Q * lower prob = %lf\n", temp * targ_tc_prob, temp * targ_tc_prob * (double) (h_tiles * w_tiles - 1));
	      
	      //	      printf("upper targ type class prob = %lf, Q * prob = %lf\n-------------------------------------\n", targ_tc_prob, targ_tc_prob * (double) (h_tiles * w_tiles - 1));
	      pred_lower_num_tied_positions += (1 / pow(targ_side * targ_side + 1, background_prob->rows)) * targ_tc_prob * (double) (h_tiles * w_tiles - 1);
	      pred_upper_num_tied_positions += targ_tc_prob * (double) (h_tiles * w_tiles - 1);
	      //	      printf("pred lower tied positions = %lf, pred upper tied positions = %lf\n", pred_lower_num_tied_positions, pred_upper_num_tied_positions);
	    }
	  }

	  // report
	  
	  time(&ltime);
	  today = localtime(&ltime);
	  strftime(startstr, 128, "%I:%M:%S %p on %A, %B %d, %Y", today);
	  fprintf(batch_fp, "%s\n", startstr);
	  fprintf(batch_fp, "--------------------------------------------------------------\n");
	  if (tiling)
	    fprintf(batch_fp, "TILING!\n");
	  else
	    fprintf(batch_fp, "NOT tiling!\n");
	  fprintf(batch_fp, "target: w = %i, h = %i (image: %i x %i)\n", targ_side, targ_side, im_w, im_h);
	  fprint_CR_Vector(batch_fp, target_prob);
	  fprint_CR_Vector(batch_fp, background_prob);
	  fprintf(batch_fp, "B = bhattacharrya bound (log2) = %lf", bhat);
	  if (tiling) {
	    fprintf(batch_fp, ", log2(Q) = %lf\n", logQ);
	    fprintf(batch_fp, "X = (N + 1)^(J^2) = %lf\n", coeff);
	    fprintf(batch_fp, "Y = 2^(-N * (2 B - log2(Q))) = %lf\n", pred_error_rate);
	    fprintf(batch_fp, "X * Y = %lf\n", coeff * pred_error_rate);
	    fprintf(batch_fp, "number of possible target tiles in image = %i\n", w_tiles * h_tiles);
	    fprintf(batch_fp, "mean target loglike = %lf, var target loglike = %lf\n", mean_targ_loglike_tb, var_targ_loglike_tb / (double) (num_targ_loglike_tb - 1));
	    fprintf(batch_fp, "mean background loglike = %lf, var background loglike = %lf\n", mean_back_loglike_tb, var_back_loglike_tb / (double) (num_back_loglike_tb - 1));
	    fprintf(batch_fp, "num actual targs = %i, num pred targs = %i, num actual backs = %i, num pred backs = %i\n", num_targ_loglike_tb - 1, num_positions, num_back_loglike_tb - 1, (h_tiles * w_tiles - 1) * num_positions);
	  }
	  else {
	    fprintf(batch_fp, "\n");
	    //	    fprint_CR_Matrix(batch_fp, fisher);
	    fprintf(batch_fp, "number of possible target positions in image = %i\n", (im_w - targ_side + 1) * (im_h - targ_side + 1));
	    fprintf(batch_fp, "mean target loglike = %lf, var target loglike = %lf\n", mean_targ_loglike_tb, var_targ_loglike_tb / (double) (num_targ_loglike_tb - 1));
	    fprintf(batch_fp, "mean background loglike = %lf, var background loglike = %lf\n", mean_back_loglike_tb, var_back_loglike_tb / (double) (num_back_loglike_tb - 1));
	    fprintf(batch_fp, "num actual targs = %i, num pred targs = %i, num actual backs = %i, num pred backs = %i\n", num_targ_loglike_tb - 1, num_positions, num_back_loglike_tb - 1, ((im_h - targ_side) * (im_w - targ_side) - 1) * num_positions);
	  }
	  fprintf(batch_fp, "num target positions = %i\n", num_positions);
	  fprintf(batch_fp, "num errors = %lf, num old errors = %lf\n", num_position_errors, num_old_position_errors);
	  fprintf(batch_fp, "num_better_positions = %i, num_tied_positions = %i\n", num_better_positions, num_tied_positions);
	  if (tiling) {
	    fprintf(batch_fp, "pred lower tied positions = %lf, pred upper tied positions = %lf\n", pred_lower_num_tied_positions, pred_upper_num_tied_positions);
	    fprintf(batch_fp, "pred error rate (Y / num_tiles) = %lf\n", pred_error_rate / (double) (w_tiles * h_tiles));
	  }
	  fprintf(batch_fp, "error rate = %lf, old error rate = %lf\nmean better positions = %lf, mean tied positions = %lf\n", num_position_errors / (double) num_positions, num_old_position_errors / (double) num_positions, (double) num_better_positions / (double) num_positions, (double) num_tied_positions / (double) num_positions);
	  fprintf(batch_fp, "--------------------------------------------------------------\n");
	  fflush(batch_fp);
	}
      }
    }
  }

  // done

  fclose(batch_fp);
  CR_exit(1);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// Initialize: Xbar=X(1), SS=0
// Update for subsequent 2<=i<=n: D=X(i)-Xbar, Xbar=Xbar+D/i, SS=SS+D*(X(i)-Xbar)
  
// Weights:  w(1),...,w(n)
// Data:     x(1),...,x(n)
// 
// Initialize:
// v(1) = w(1)
// s(1) = w(1) * x(1)
// q(1) = 0
// 
// Advance: for j=1,..., n-1
// v(j+1) = v(j) + w(j+1)
// s(j+1) = s(j) + w(j+1) * x(j+1)
// q(j+1) = q(j) + (x(j+1) - s(j+1) / v(j+1))^2 * w(j+1) / v(j)
// 
// Conclude:
// a(n) = s(n) / v(n)
// s(n) = q(n) / v(n)
// 
// Then a(n) is the weighted average,
//      s(n) is the weighted variance.


// for diagnosing problems with symbolic differentiation

  //  print_CR_Matrix(track_3d_quadratic_kal->H);
  //  CR_exit(1);

  //  print_CR_FuncMatrix(track_3d_quadratic_kal->F_H);
  //  print_CR_FuncMatrix(track_3d_quadratic_kal->Jac_F_H);

  /*
  track_3d_quadratic_kal->x_pred = evaluate_CR_FuncMatrix_to_CR_Matrix(track_3d_quadratic_kal->F_F, track_3d_quadratic_kal->x_last);       
  track_3d_quadratic_kal->z_pred = evaluate_CR_FuncMatrix_to_CR_Matrix(track_3d_quadratic_kal->F_H, track_3d_quadratic_kal->x_pred);  

  //  evaluate_CR_FuncMatrix_to_CR_Matrix(track_3d_quadratic_kal->Jac_F_H, track_3d_quadratic_kal->x_pred, track_3d_quadratic_kal->H);

  double *V;
  int numV, i;

  V = (double *) CR_calloc(track_3d_quadratic_kal->x_pred->rows, sizeof(double));
  numV = track_3d_quadratic_kal->x_pred->rows;
  for (i = 0; i < track_3d_quadratic_kal->x_pred->rows; i++)
    V[i] = MATRC(track_3d_quadratic_kal->x_pred, i, 0);

  printf("syms ax bx cx az bz cz;\n");
  matlab_print_CR_Function(track_3d_quadratic_kal->F_H->rc[0][0]);
  matlab_print_CR_Function(track_3d_quadratic_kal->Jac_F_H->rc[0][0]);
  printf("simpfdiff = simple(diff(f, ax));\n");
  printf("simpfprime = simple(fprime);\n");
  printf("ax = %lf;\nbx = %lf;\ncx = %lf;\naz = %lf;\nbz = %lf;\ncz = %lf;\n", track_3d_quadratic_vars->x[0],track_3d_quadratic_vars->x[1],track_3d_quadratic_vars->x[2],track_3d_quadratic_vars->x[3],track_3d_quadratic_vars->x[4],track_3d_quadratic_vars->x[5]);
 
  for (i = 0; i < numV; i++)
    printf("V[%i] = %lf\n", i, V[i]);
  printf("H[0][0] = %lf\n", evaluate_CR_Function(track_3d_quadratic_kal->Jac_F_H->rc[0][0], V, numV));

	 //  print_CR_Matrix(track_3d_quadratic_kal->H);

  exit(1);
  */

  //  print_CR_Kalman(track_3d_quadratic_kal);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// from TestNML_grab_CR_CG()

    /*
    if (seqnodiff == 15) {

      itg_framestart_flag = TRUE;

      // get facet data and draw/process frame

      for (j = 0; j < D3_LASER_FACET_ROWS * D3_LASER_FACET_COLS; j++)
 	itg_facet_data[scanline - 1][j] = convert_bigshort(*((short *) &raw_laser_data[BYTES_PER_FACET * i + 2 * j + FACETDATA_BYTE]));
      ITG_add_facet(scanline, itg_facet_data[scanline - 1], itg_frame_data);

      ITG_draw_laser_CR_Image(itg_frame_data, itg_laser_im, itg_flipped_laser_im);
    }
    else if (seqnodiff > 15) {

      // draw/process and get facet data

      ITG_draw_laser_CR_Image(itg_frame_data, itg_laser_im, itg_flipped_laser_im);

//       ITG_get_laser_CR_Image(itg_frame_data, itg_laser_im);
//       vflip_CR_Image(itg_laser_im, itg_flipped_laser_im);
//       glDrawPixels(itg_laser_im->w, itg_laser_im->h, GL_RGB, GL_UNSIGNED_BYTE, itg_flipped_laser_im->data);

      itg_framestart_flag = TRUE;

      ITG_grab_facet(scanline, &raw_laser_data[BYTES_PER_FACET * i], itg_facet_data[scanline - 1], itg_frame_data);
      
//       for (j = 0; j < D3_LASER_FACET_ROWS * D3_LASER_FACET_COLS; j++)
// 	itg_facet_data[scanline - 1][j] = convert_bigshort(*((short *) &raw_laser_data[BYTES_PER_FACET * i + 2 * j + FACETDATA_BYTE]));
//       ITG_add_facet(scanline, itg_facet_data[scanline - 1], itg_frame_data);

    }
    else {

      // get facet data and do nothing

      ITG_grab_facet(scanline, &raw_laser_data[BYTES_PER_FACET * i], itg_facet_data[scanline - 1], itg_frame_data);

//       for (j = 0; j < D3_LASER_FACET_ROWS * D3_LASER_FACET_COLS; j++)
// 	itg_facet_data[scanline - 1][j] = 
// 	  convert_bigshort(*((short *) &raw_laser_data[BYTES_PER_FACET * i + 2 * j + FACETDATA_BYTE]));
// 
//       ITG_add_facet(scanline, itg_facet_data[scanline - 1], itg_frame_data);
    }
    */

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

extern CR_Vector *ts_r_histogram, *ts_g_histogram, *ts_b_histogram;
extern int ts_initialized;

CR_Movie *lasermov;

//-----------------------------------------------------------------------

// label/feature vector elements (see feature_vector.txt)

// 1: label
// 1: sequence number -- code indicating what sequence this point came from
// 1: frame number -- what frame in that sequence did this point come from
// 2: x, y -- where in that frame
// 27: 3 x 3 RGB values (3 channels x 9 pixels)
// 24: 31 x 31 color histogram (3 channels x 8 bins)
// ----
// 1 label + 55 features = 56

#define CVPR2003_NUM_HEADERS          4  
#define CVPR2003_NUM_3X3S             27
#define CVPR2003_NUM_HISTOGRAMS       24     

#define CVPR2003_NUM_FEATURES         (CVPR2003_NUM_HEADERS + CVPR2003_NUM_3X3S + CVPR2003_NUM_HISTOGRAMS)

#define CVPR2003_FIRST_HEADER         1   
#define CVPR2003_FIRST_3X3            (CVPR2003_NUM_HEADERS + CVPR2003_FIRST_HEADER)   
#define CVPR2003_FIRST_HISTOGRAM      (CVPR2003_NUM_3X3S + CVPR2003_FIRST_3X3)   

#define CVPR2003_NUM_COLOR_BINS       8
#define CVPR2003_NUM_COLOR_CHANNELS   3

void CVPR_2003_initialize_feature_vectors_CR_Image(int w, int h)
{
  int i, x, y;

  CR_flush_printf("initializing...");

  // general
  
  // for classification
//   ts_left = 23;
//   ts_top = 20;
//   ts_right = 337;
//   ts_bottom = 219;
  
  // for tracking
  ts_left = 15;
  ts_top = 20;
  ts_right = 344;
  ts_bottom = 224;

  //  ts_x_delta = ts_y_delta = 10;
  ts_x_delta = ts_y_delta = 5;
  //ts_x_delta = ts_y_delta = 10;

  for (y = ts_top, ts_rows = 0; y <= ts_bottom; y += ts_y_delta) {
    for (x = ts_left, ts_cols = 0; x <= ts_right; x += ts_x_delta) 
      ts_cols++;
    ts_rows++;
  }

  printf("ts cols = %i, rows = %i\n", ts_cols, ts_rows);

  ts_num_feature_vectors = (1 + (ts_right - ts_left) / ts_x_delta) * (1 + (ts_bottom - ts_top) / ts_y_delta);
  ts_feature_vectors = make_all_CR_Matrix("feature_vectors", ts_num_feature_vectors, CVPR2003_NUM_FEATURES + 1, 0.0);

  // color

  ts_r_histogram = make_CR_Vector("r_hist", CVPR2003_NUM_COLOR_BINS);
  ts_g_histogram = make_CR_Vector("g_hist", CVPR2003_NUM_COLOR_BINS);
  ts_b_histogram = make_CR_Vector("b_hist", CVPR2003_NUM_COLOR_BINS);

  // done
  
  ts_initialized = TRUE;

  CR_flush_printf("done\n");
}

//-----------------------------------------------------------------------

void CVPR_2003_compute_feature_vectors_CR_Image(char *filename, int sequence_number, int frame_number, CR_Image *im, CR_Matrix *roadpoly, CR_SVM *svm, CR_Vector *svm_input, CR_Matrix *svm_output)
{
  int i, j, x, y, w, h, index, index_3x3, offset, row, col;
  int gabor_unit, color_unit, laser_unit, xc, yc, xleft, xright, ytop, ybottom, total, laser_hits;
  FILE *fp;
  double result;
 
  if (!ts_initialized)
    CR_error("CVPR_2003_compute_feature_vectors_CR_Image(): must initialize");

  // >>>>>>>>>>>>>>>>>>>> HEADER <<<<<<<<<<<<<<<<<<<

  for (y = ts_top, index = 0; y <= ts_bottom; y += ts_y_delta)
    for (x = ts_left; x <= ts_right; x += ts_x_delta) {
      MATRC(ts_feature_vectors, index, 1) = sequence_number;
      MATRC(ts_feature_vectors, index, 2) = frame_number;
      MATRC(ts_feature_vectors, index, 3) = x;
      MATRC(ts_feature_vectors, index, 4) = y;
      index++;
    }

  // >>>>>>>>>>>>>>>>>>>> FEATURES <<<<<<<<<<<<<<<<<

  // record RGB values of 3 x 3 neighborhood around feature point

  /*
    for (y = ts_top, index = 0; y <= ts_bottom; y += ts_y_delta)
      for (x = ts_left; x <= ts_right; x += ts_x_delta) {
	for (j = y - 1, index_3x3 = 0; j <= y + 1; j++) 
	  for (i = x - 1; i <= x + 1; i++) {
	    MATRC(ts_feature_vectors, index, CVPR2003_FIRST_3X3 + index_3x3) = (unsigned char) IMXY_R(im, i, j);
	    MATRC(ts_feature_vectors, index, CVPR2003_FIRST_3X3 + 1 + index_3x3) = (unsigned char) IMXY_G(im, i, j);
	    MATRC(ts_feature_vectors, index, CVPR2003_FIRST_3X3 + 2 + index_3x3) = (unsigned char) IMXY_B(im, i, j);
	    index_3x3 += CVPR2003_NUM_COLOR_CHANNELS;
	  }
	index++;
      }
  */

  // independently histogram color over 31 x 31 subimage

    w = h = 31;
    for (y = ts_top, index = 0; y <= ts_bottom; y += ts_y_delta)
      for (x = ts_left; x <= ts_right; x += ts_x_delta) {
	histogram_color_CR_Image(x, y, w, h, ts_r_histogram, ts_g_histogram, ts_b_histogram, 0, 255, im);
	for (j = 0; j < CVPR2003_NUM_COLOR_BINS; j++) {
	  MATRC(ts_feature_vectors, index, CVPR2003_FIRST_HISTOGRAM + j) = 
	    ts_r_histogram->x[j];
	  MATRC(ts_feature_vectors, index, CVPR2003_FIRST_HISTOGRAM + CVPR2003_NUM_COLOR_BINS + j) = 
	    ts_g_histogram->x[j];
	  MATRC(ts_feature_vectors, index, CVPR2003_FIRST_HISTOGRAM + 2 * CVPR2003_NUM_COLOR_BINS + j) = 
	    ts_b_histogram->x[j];
	}
	index++;
      }

  // >>>>>>>>>>>>>>>>>>>> LABELS <<<<<<<<<<<<<<<<<<<

  // if labels are provided...

  if (roadpoly) {
    for (y = ts_top, index = 0; y <= ts_bottom; y += ts_y_delta)
      for (x = ts_left; x <= ts_right; x += ts_x_delta) {
	if (point_in_poly_CR_Matrix(x, y, roadpoly))
	  MATRC(ts_feature_vectors, index, 0) = 1.0;
	else
	  MATRC(ts_feature_vectors, index, 0) = 0.0;
	index++;
      }
  }

  // ...otherwise we must classify

  else {

    // 3x3
    if (svm_input->rows == 27) 
      offset = CVPR2003_FIRST_3X3;
    // histogram
    else 
      offset = CVPR2003_FIRST_HISTOGRAM;

    for (y = ts_top, row = 0, index = 0; y <= ts_bottom; y += ts_y_delta, row++)
      for (x = ts_left, col = 0; x <= ts_right; x += ts_x_delta, col++) {

	// copy appropriate range of features into svm_input (use svm_input length to determine which feature set)

	// assuming ynorm_histogram here
	svm_input->x[0] = (double) y / 240.0;
	for (i = 0; i < svm_input->rows; i++)
	  svm_input->x[i + 1] = MATRC(ts_feature_vectors, index, offset + i);

	// classify

	result = classify_pattern_SVM(svm_input, svm);

	MATRC(svm_output, row, col) = result;

	/*
  	if (result < 0) {
  	  MATRC(ts_feature_vectors, index, 0) = 0.0;
  	  MATRC(svm_output, row, col) = 0.0;
  	}
  	else {
  	  MATRC(ts_feature_vectors, index, 0) = 1.0;
  	  MATRC(svm_output, row, col) = 1.0;
  	}
	*/

	index++;
      }
  }

  // >>>>>>>>>>>>>>>>>>>> END <<<<<<<<<<<<<<<<<<<<

  if (filename) {
    CR_flush_printf("writing %s...", filename);

    // write feature vectors
    
    write_dlm_CR_Matrix(filename, ts_feature_vectors);
    
    CR_flush_printf("done\n");
  }
}

//----------------------------------------------------------------------------

// this is for generating .dat files to be used in classification

void CVPR_2003_Compute_Features_single_frame(char *feature_filename, char *image_filename, char *poly_filename)
{
  CR_Image *im;
  CR_Matrix *roadpoly;
  int frame;

  roadpoly = read_CR_Matrix(poly_filename);
  im = read_CR_Image(image_filename);

  CVPR_2003_compute_feature_vectors_CR_Image(feature_filename, 0, 0, im, roadpoly, NULL, NULL, NULL);
}

//----------------------------------------------------------------------------

void draw_svm_segmentation(CR_Matrix *svm_output, int imheight, int x_start, int x_stop, int x_delta, int y_start, int y_stop, int y_delta)
{
  int x, y, row, col;

  for (y = y_start, row = 0; y <= y_stop; y += y_delta, row++)
    for (x = x_start, col = 0; x <= x_stop; x += x_delta, col++) 
      if (MATRC(svm_output, row, col)) 
	ogl_draw_point(x, imheight - y, 3, 255, 128, 0);
}

//----------------------------------------------------------------------------

CR_Image *CVPR_2003_Track3DQuadratic_init_CR_CG(char *name)
{
  CR_Image *im, *svm_im;
  char *filename, *framename, *imname, *featurename, *polyname;
  int i, j, cumframe, seqno, seqframe, num_picks;
  CR_Movie *seqmov, *seq181, *seq182, *seq183, *seq185;
  FILE *fp;
  CR_Matrix *picks;
  CR_SVM *svm_3x3, *svm_hist, *svm_ynorm_hist;

  int tmpFlag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
  tmpFlag &= ~_CRTDBG_ALLOC_MEM_DF;
  _CrtSetDbgFlag(tmpFlag);
 
  CVPR_2003_initialize_feature_vectors_CR_Image(360, 240);

  framename = (char *) CR_calloc(256, sizeof(char));

  featurename = (char *) CR_calloc(256, sizeof(char));
  imname = (char *) CR_calloc(256, sizeof(char));
  polyname = (char *) CR_calloc(256, sizeof(char));

  num_picks = 40;

  //----------------------------------------------------------------------------
  // randomly pick frames and then extract them
  //----------------------------------------------------------------------------

  /*
  seq181 = open_CR_Movie("D:\\Documents\\software\\cvpr2003_data\\huff_half_dvData_181_.avi");
  seq182 = open_CR_Movie("D:\\Documents\\software\\cvpr2003_data\\huff_half_dvData_182_.avi");
  seq183 = open_CR_Movie("D:\\Documents\\software\\cvpr2003_data\\huff_half_dvData_183_.avi");
  seq185 = open_CR_Movie("D:\\Documents\\software\\cvpr2003_data\\huff_half_dvData_185_.avi");
  
  fp = fopen("D:\\Documents\\software\\cvpr2003_data\\random_frames\\picks.txt", "w");

//   seqmov = seq182;
//   seqno = 182;
//   seqframe = 100;
//   exit(1);

  for (i = 0; i < num_picks; i++) {

    cumframe = ranged_uniform_int_CR_Random(0, 23681);

    if (cumframe <= 7253) {
      seqno = 181;
      seqmov = seq181;
      seqframe = cumframe;
    }
    else if (cumframe <= 15420) {
      seqno = 182;
      seqmov = seq182;      
      seqframe = cumframe - 7253;
    }
    else if (cumframe <= 21328) {
      seqno = 183;
      seqmov = seq183;
      seqframe = cumframe - 15420;
    }
    else {
      seqno = 185;
      seqmov = seq185;
      seqframe = cumframe - 21328 + 3700;
    }

    fprintf(fp, "%i,\t%i,\t%i,\t%i,\t%i\n", i, !(i % 2), cumframe, seqno, seqframe);

    get_frame_CR_Movie(seqmov, seqframe);
    if (!(i % 2))
      sprintf(framename, "D:\\Documents\\software\\cvpr2003_data\\random_frames\\train_seq%i_%i.ppm\0", seqno, seqframe);
    else
      sprintf(framename, "D:\\Documents\\software\\cvpr2003_data\\random_frames\\test_seq%i_%i.ppm\0", seqno, seqframe);
    write_CR_Image(framename, seqmov->im);

  }

  fclose(fp);
  */

  //----------------------------------------------------------------------------
  // compute feature vectors for extracted frames
  //----------------------------------------------------------------------------

  /*
  // read picks.txt
  picks = read_dlm_CR_Matrix("D:\\Documents\\software\\cvpr2003_data\\random_frames\\picks.txt", num_picks, 5);
  //  print_CR_Matrix(picks);
  //  exit(1);

  // iterate over lines of picks.txt, generating filenames from seq and frame numbers, and calling...
  for (i = 0; i < num_picks; i++)
    if (MATRC(picks, i, 1))  { // then in training set
      
      seqno = (int) MATRC(picks, i, 3);
      seqframe = (int) MATRC(picks, i, 4);

      CR_flush_printf("%i %i %i ...", i, seqno, seqframe);

      sprintf(featurename, "D:\\Documents\\software\\cvpr2003_data\\random_frames\\train_features\\train_seq%i_%i.dat\0", seqno, seqframe);
      sprintf(imname, "D:\\Documents\\software\\cvpr2003_data\\random_frames\\train\\train_seq%i_%i.ppm\0", seqno, seqframe);
      sprintf(polyname, "D:\\Documents\\software\\cvpr2003_data\\random_frames\\train\\train_seq%i_%i.pol\0", seqno, seqframe);

      CVPR_2003_Compute_Features_single_frame(featurename, imname, polyname);
      
      CR_flush_printf("done\n");
    }
  */

  //----------------------------------------------------------------------------

  picks = read_dlm_CR_Matrix("C:\\Documents\\software\\cvpr2003_data\\random_frames\\picks.txt", num_picks, 5);
  i = 0;
  seqno = (int) MATRC(picks, i, 3);
  seqframe = (int) MATRC(picks, i, 4);

  svm_im = make_CR_Image(ts_cols, ts_rows, 0);

  // load svm model

  //  svm_3x3 = load_svmlight_model_SVM("C:\\Documents\\software\\cvpr2003_data\\random_frames\\train_features\\svm_3x3_gamma_0.001.mod");
  //  svm_hist = load_svmlight_model_SVM("C:\\Documents\\software\\cvpr2003_data\\random_frames\\train_features\\svm_histogram_gamma_0.1.mod");
  svm_ynorm_hist = load_svmlight_model_SVM("C:\\Documents\\software\\cvpr2003_data\\random_frames\\train_features\\svm_ynorm_histogram_gamma_10.0.mod");

//   road_svm = svm_3x3;
  //road_svm = svm_hist;
  road_svm = svm_ynorm_hist;
  road_svm_input = make_CR_Vector("svm_input", road_svm->num_features);
  road_svm_output = make_CR_Matrix("svm_output", ts_rows, ts_cols);

  // load image

  /*
  sprintf(imname, "C:\\Documents\\software\\cvpr2003_data\\random_frames\\train\\train_seq%i_%i.ppm\0", seqno, seqframe);
  im = read_CR_Image(imname);

  // computer image features and classify
  CVPR_2003_compute_feature_vectors_CR_Image(NULL, 0, 0, im, NULL, svm, svm_input, svm_output);

  // display result
  print_CR_Matrix(svm_output);
  draw_CR_Matrix_CR_Image(1.0, svm_output, svm_im, FALSE);
  write_CR_Image("svm_hist.ppm", svm_im);
  */

  //  draw_svm_segmentation(svm_output, im, ts_left, ts_right, ts_x_delta, ts_top, ts_bottom, ts_y_delta);

  //----------------------------------------------------------------------------


  //roadmov = open_CR_Movie("C:\\Documents\\software\\cvpr2003_data\\huff_half_dvData_181_.avi");
  roadmov = open_CR_Movie("C:\\Documents\\software\\cvpr2003_data\\huff_half_dvData_183_.avi");
  get_frame_CR_Movie(roadmov, roadmov->currentframe + 4800);
  //get_frame_CR_Movie(roadmov, roadmov->currentframe);

  im = make_CR_Image(roadmov->im->w, roadmov->im->h, 0);
  roadim = make_CR_Image(roadmov->im->w, roadmov->im->h, 0);
  sprintf(name, "CVPR_2003_Track3DQuadratic");

  /*
  init_laser2camera(roadmov->im->w, roadmov->im->h);

  lasermov = open_CR_Movie("C:\\Documents\\software\\cvpr2003_data\\fw080");
  //  lasermov = open_CR_Movie("C:\\Documents\\software\\cvpr2003_data\\fw_181");
  get_frame_CR_Movie(lasermov, lasermov->currentframe);
  */

  //----------------------------------------------------------------------------

  /*
  track_3d_quadratic_vars = init_CR_Vector("track_3d_quadratic_vars", 13, 0.0, 0.0, -1.0, 0.0, 2.0, 0.0,3.0, 2.0, 0.24, 375, 375, 360, 240);       // image width, height

  plane_from_laser_kal = demo3_load_kalman("plane_from_laser.s", track_3d_quadratic_vars);
  //  track_3d_quadratic_kal = demo3_load_kalman("planar_z_track3dquad.s", NULL);

  CR_exit(1);
  */

  //  track_3d_first_frame = 3140;  // for sequence 183 

  /*
  roadmov = open_CR_Movie("C:\\Documents\\software\\cvpr2003_data\\huff_half_dvData_183_.avi");
  roadmov->currentframe = track_3d_first_frame;  
  get_frame_CR_Movie(roadmov, roadmov->currentframe);

  im = make_CR_Image(roadmov->im->w, roadmov->im->h, 0);
  roadim = make_CR_Image(roadmov->im->w, roadmov->im->h, 0);
  sprintf(name, "Track3DQuadratic");
  */

  /*
  track_3d_quadratic_edge_window_radius = 20 * im->w / 720;
  track_3d_quadratic_thresh_edge_strength = 100;  // 500

  // for real images
  track_3d_left_margin = 16 * im->w / 720;
  track_3d_right_margin = im->w - 1 - 16 * im->w / 720;
  track_3d_top_margin = im->h - 400 * im->h / 480; // 400
  track_3d_bottom_margin = im->h - 160 * im->h / 480;

  track_3d_window_radius = 0.33;

  track_3d_depth_dependent_search = FALSE;  // make edge search window width inversely proportional to depth?

  // load and initialize Kalman filter road model

  track_3d_quadratic_vars = init_CR_Vector("track_3d_quadratic_vars", 13,
					   0.0, 0.0, -1.0,      // ax, bx, cx = -1.0 
					   0.0, 2.0, 0.0,       // az, bz, cz
					   3.0,                 // 3.0 road radius (meters)
					   2.0,                 // 2.0 camera height off ground plane (meters)
					   //DEG2RAD(13.0),         // camera tilt: 0 level, + down, - up (radians)
					   0.24,                // 0.24 camera tilt: 0 level, + down, - up (radians)
					   //0.24,                // 0.24 camera tilt: 0 level, + down, - up (radians)
					   //					   750.0, 750.0,        // horizontal, vertical focal length
					   750.0 * im->w / 720, 750.0 * im->w / 720,        // horizontal, vertical focal length
					   (double) im->w, (double) im->h);       // image width, height
  t3d = track_3d_quadratic_vars->x;

  printf("first vars\n");
  print_CR_Vector(track_3d_quadratic_vars);

  // set up matrix for connected components of neural network output

  Track3_initialize_neural_net();

  track_3d_nn_x_interval = 10;  // 10
  track_3d_nn_y_interval = 10;  // 10

  track_3d_nn_rows = track_3d_nn_cols = 0;
  for (j = track_3d_top_margin; j <= track_3d_bottom_margin; j += track_3d_nn_y_interval) 
    track_3d_nn_rows++;
  for (i = track_3d_left_margin; i <= track_3d_right_margin; i += track_3d_nn_x_interval) 
    track_3d_nn_cols++;

  track_3d_nn = make_CR_Matrix("track_3d_nn", track_3d_nn_rows, track_3d_nn_cols);
  track_3d_nn_cc = make_CR_Matrix("track_3d_nn_cc", track_3d_nn_rows, track_3d_nn_cols);

  left_nn_x = left_nn_y = right_nn_x = right_nn_y = NULL;

  left_3d_nn_x = left_3d_nn_z = right_3d_nn_x = right_3d_nn_z = NULL;
  left_3d_nn_z_ground = right_3d_nn_z_ground = NULL;

  left_goodrow = make_CR_Vector("left_goodrow", track_3d_nn_rows);
  right_goodrow = make_CR_Vector("right_goodrow", track_3d_nn_rows);

  // initialize with biggest cc of neural network method

  track_3d_quadratic_kal = initialize_edge_tracker_from_nn("planar_z_track3dquad.s", roadmov->im);
  initial_track_3d_quadratic_kal = copy_CR_Kalman(track_3d_quadratic_kal);

  track_3d_quadratic_Z = make_CR_Matrix("Z", track_3d_quadratic_kal->z_pred->rows, track_3d_quadratic_kal->z_pred->cols);
  track_3d_quadratic_Z_array = make_CR_Matrix_array(1);
  track_3d_quadratic_Z_array[0] = track_3d_quadratic_Z;
  track_3d_quadratic_good_Z = make_CR_Vector("good_Z", track_3d_quadratic_Z->rows);

  track_3d_quadratic_kal_nn = demo3_load_kalman("planar_z_track3dquad.s", track_3d_quadratic_vars);

  track_3d_quadratic_Z_nn = make_CR_Matrix("Z_nn", track_3d_quadratic_kal_nn->z_pred->rows, track_3d_quadratic_kal_nn->z_pred->cols);
  track_3d_quadratic_Z_nn_array = make_CR_Matrix_array(1);
  track_3d_quadratic_Z_nn_array[0] = track_3d_quadratic_Z_nn;

  num_road3d_pts = 100; // 500
  road3d_delta = 0.27;  // 0.32

  road3d_xc = (double *) CR_calloc(num_road3d_pts, sizeof(double));
  road3d_yc = (double *) CR_calloc(num_road3d_pts, sizeof(double));
  road3d_xl = (double *) CR_calloc(num_road3d_pts, sizeof(double));
  road3d_yl = (double *) CR_calloc(num_road3d_pts, sizeof(double));
  road3d_xr = (double *) CR_calloc(num_road3d_pts, sizeof(double));
  road3d_yr = (double *) CR_calloc(num_road3d_pts, sizeof(double));
  road3d_a = t3d[0];
  road3d_b = t3d[1];
  road3d_c = t3d[2];
  road3d_t = 0.0;

  track_3d_frame = 0;
  track_3d_switch_frame = 20;

  good_road = TRUE;
  filtered_good_road = TRUE;
  good_road_initialized = FALSE;
  good_road_check_interval = 5;

  good_road_kal = load_CR_Kalman("good_road.s");

  good_road_Z = make_CR_Matrix("Z", 1, 1);
  good_road_Z_array = make_CR_Matrix_array(1);
  good_road_Z_array[0] = good_road_Z;

  track_3d_mov = write_avi_initialize_CR_Movie(NULL, im->w, im->h, 30);

  // SVM stuff

  good_road_svm = load_svmlight_model_SVM("good_road_rbf_0_03.mod");
  good_road_svm_input = make_CR_Vector("good_road_svm_input", track_3d_nn_rows * track_3d_nn_cols);

  road_nonroad_svm = load_svmlight_model_SVM("svm_road_train_rbf0.001.mod");
  road_nonroad_svm_input = make_CR_Vector("road_nonroad_svm_input", 27);

  track_3d_svm = make_CR_Matrix("track_3d_svm", track_3d_nn_rows, track_3d_nn_cols);
  */

  // finish

  return im;
}

//----------------------------------------------------------------------------

void CVPR_2003_Track3DQuadratic_grab_CR_CG(CR_Image *im)
{
  int i, j, index, xc, yc, temp;
  double x, z, ax, bx, cx, imy, z_ground, good_road_result;
  double *v;
  double xlaser, ylaser, zlaser;

  // -------------------
  // draw current image
  // -------------------

//   write_CR_Image("roadim.ppm", roadmov->im);
//   exit(1);

  vflip_CR_Image(roadmov->im, roadim);
  //  set_CR_Image(0, roadim);
  glDrawPixels(roadim->w, roadim->h, GL_RGB, GL_UNSIGNED_BYTE, roadim->data);

  // ---------------------------- 
  // draw image processing limits
  // ----------------------------
    
  //  ogl_draw_rectangle(track_3d_left_margin, im->h - track_3d_top_margin, track_3d_right_margin, im->h - track_3d_bottom_margin, 255, 255, 255);

  //  ogl_draw_rectangle(ts_left, im->h - ts_top, ts_right, im->h - ts_bottom, 255, 255, 255);
  //  ogl_draw_rectangle(ts_left - 4, im->h - ts_top, ts_right, im->h - ts_bottom, 255, 255, 255);

  // ----------------------
  // draw laser
  // ----------------------

  // map current laser image to camera image

  /*
  for (j = 0, index = 0; j < D3_LASER_HEIGHT; j++)
    for (i = 0; i < D3_LASER_WIDTH; i++) {

      laser2camera(i, j, lasermov->laserim[lasermov->currentframe].data[index], 
		   &xc, &yc, &xlaser, &ylaser, &zlaser, 
		   lasermov->laserim[lasermov->currentframe].frameHeaders[0].tiltAngle);


      temp = 255 - lasermov->laserim[lasermov->currentframe].data[index] / 4;
      ogl_draw_point(xc, roadmov->im->h - yc, 2, temp, temp, temp);

      index++;
    }

  lasermov->currentframe++;
  */

  //  lasermov->laserim[lasermov->currentframe]
  //      laser2camera(i, j, laser_data[index], &xc, &yc, &xlaser, &ylaser, &zlaser, tilt);
  // draw dots
  //  exit(1);

  // ----------------------
  // svm segmentation
  // ----------------------

  // xxx
  // compute image features and classify
  //  CVPR_2003_compute_feature_vectors_CR_Image(NULL, 0, 0, roadmov->im, NULL, road_svm, road_svm_input, road_svm_output);

  // display result
  //  print_CR_Matrix(svm_output);
  //  draw_CR_Matrix_CR_Image(1.0, svm_output, svm_im, FALSE);
  //  write_CR_Image("svm_hist.ppm", svm_im);

  //  write_dlm_CR_Matrix("dd_road_svm_output.txt", road_svm_output);
  //  exit(1);

  //  draw_svm_segmentation(road_svm_output, roadmov->im->h, ts_left, ts_right, ts_x_delta, ts_top, ts_bottom, ts_y_delta);

  /*
  // ----------------------
  // neural net stuff
  // ----------------------

  // get measurements
  
  int row, col;
  double road_type;
  
  if (track_3d_frame >= track_3d_switch_frame && !(track_3d_frame % good_road_check_interval)) {
    
    good_road_initialized = TRUE;

    run_neural_net(roadmov->im, track_3d_nn, track_3d_left_margin, track_3d_right_margin, track_3d_top_margin, track_3d_bottom_margin, track_3d_nn_x_interval, track_3d_nn_y_interval, TRUE);  // TRUE
    
    //    run_svm(roadmov->im, track_3d_svm, track_3d_left_margin, track_3d_right_margin, track_3d_top_margin, track_3d_bottom_margin, track_3d_nn_x_interval, track_3d_nn_y_interval, TRUE);  // TRUE

    for (j = 0, index = 0; j < track_3d_nn->rows; j++)
      for (i = 0; i < track_3d_nn->cols; i++)
	good_road_svm_input->x[index++] = MATRC(track_3d_nn, j, i);
   
    good_road_result = classify_pattern_SVM(good_road_svm_input, good_road_svm);

    if (good_road_result < 0)
      good_road = FALSE;
    else
      good_road = TRUE;
    
    MATRC(good_road_Z, 0, 0) = good_road_result;
    update_nonlinear_CR_Kalman(good_road_Z_array, 1, good_road_kal);
    if (MATRC(good_road_kal->x, 0, 0) < 0.0)
      filtered_good_road = FALSE;
    else
      filtered_good_road = TRUE;
  }

  if (track_3d_frame < track_3d_switch_frame)
    Track3DQuadratic_preprocess_nn_measurements(roadmov->im, TRUE);

  // -------------------
  // edge stuff
  // -------------------

  // get measurements

#ifdef BADROAD_REINIT
  if (good_road_initialized && filtered_good_road) { 
#else
  if (good_road_initialized) { 
#endif

    if (track_3d_quadratic_kal) {

      Track3DQuadratic_get_ortho_measurements(roadmov->im);
      
      // update state
      
      update_nonlinear_CR_Kalman(track_3d_quadratic_Z_array, 1, track_3d_quadratic_kal);
      
      // draw state and measurements
      
      track_3d_reinit_counter++;
      if (track_3d_frame >= track_3d_switch_frame) {
	if (!track_3d_reinit_flag || track_3d_reinit_counter >= 30) {
	  Track3DQuadratic_draw_state_CR_CG(im, track_3d_quadratic_kal);
	  Track3DQuadratic_draw_measurements(im, track_3d_quadratic_Z, track_3d_quadratic_good_Z);
	}
      }
    }
#ifdef BADROAD_REINIT
    else {
      printf("reinitializing edge kalman filter\n");
      track_3d_reinit_counter = 0;
      track_3d_reinit_flag = TRUE;
      track_3d_quadratic_kal = initialize_edge_tracker_from_nn(NULL, roadmov->im);
    }
#endif
  }
#ifdef BADROAD_REINIT
  else if (good_road_initialized) {
    if (track_3d_quadratic_kal) {
      printf("killing edge kalman filter\n");
      free_CR_Kalman(track_3d_quadratic_kal);
      track_3d_quadratic_kal = NULL;
    }
  }
#endif

  printf("%i\n", track_3d_frame);
  */

  //  glutSwapBuffers();

  // -------------------
  // grab the next image
  // -------------------
  
  //  track_3d_frame++;

  roadmov->currentframe++;
  //roadmov->currentframe += 50;
  
  //  if (track_3d_frame >= track_3d_switch_frame) {
    if (roadmov->currentframe < roadmov->lastframe) 
      //      get_frame_CR_Movie(roadmov, roadmov->currentframe++);
      get_frame_CR_Movie(roadmov, roadmov->currentframe);
    //  }
}
  
//----------------------------------------------------------------------------
  
void CVPR_2003_Track3DQuadratic_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

double iv_timedelta()
{
    static long begin = 0;
    static long finish, difference;

#if defined(_WIN32)
    static struct timeb tb;
    ftime(&tb);
    finish = tb.time * 1000 + tb.millitm;
#else
    static struct tms tb;
    finish = times(&tb);
#endif

    difference = finish - begin;
    begin = finish;

    return (double) difference / (double) CLK_TCK;
}

CR_Image *IV2003_init_CR_CG(char *name)
{
  CR_Image *im;
  int result, i, j, index, x, y, row, col;
  CR_Movie *mov;
  CR_SVM *center_svm;
  CR_SVM *left_svm;
  CR_SVM *right_svm;
  CR_Vector *svm_input;
  CR_Matrix *svm_output;
  char *framename;

  int tmpFlag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
  tmpFlag &= ~_CRTDBG_ALLOC_MEM_DF;
  _CrtSetDbgFlag(tmpFlag);

  framename = (char *) CR_calloc(128, sizeof(char));

  // load svm models

  center_svm = load_svmlight_model_SVM("cm_frames\\center\\All_Center_16_55_40_3_18_2003.mod");
  left_svm = load_svmlight_model_SVM("cm_frames\\left\\All_Left_16_55_40_3_18_2003.mod");
  right_svm = load_svmlight_model_SVM("cm_frames\\right\\All_Right_16_55_40_3_18_2003.mod");

  // open CAN file
  
  mov = open_CR_Movie("D:\\capture\\16_55_40_3_18_2003.can");

  // initialize feature vectors

  //  IV2003_initialize_feature_vectors_CR_Image(320, 240, 5);
  IV2003_initialize_feature_vectors_CR_Image(320, 240, 5);
  svm_input = make_CR_Vector("svm_input", center_svm->num_features);
  svm_output = make_CR_Matrix("svm_output", ts_rows, ts_cols);

  iv_timedelta();

  //  for (i = 1035; i <= 7184; i++) {
  for (i = 373; i <= 373; i++) {

    get_ncamera_frame_CR_Movie(mov, i);

    // calculate feature vectors for left, center, & right camera at 5 x 5 resolution

    // center
    IV2003_compute_feature_vectors_CR_Image(NULL, 0, i, mov->ncam_im[0], NULL, 0, NULL);

    // classify center
    
    for (y = ts_top, row = 0, index = 0; y <= ts_bottom; y += ts_y_delta, row++)
      for (x = ts_left, col = 0; x <= ts_right; x += ts_x_delta, col++) {
	for (j = 0; j < svm_input->rows; j++)
	  svm_input->x[j] = MATRC(ts_feature_vectors, index, j + 79);
	MATRC(svm_output, row, col) = classify_pattern_SVM(svm_input, center_svm);
	index++;
      }
    
    // write center

    //    sprintf(framename, "cm_frames\\left\\segmented\\%i_left_16_55_40_3_18_2003.seg", i);
    sprintf(framename, "cm_frames\\%i_center_16_55_40_3_18_2003.seg", i);
    write_dlm_CR_Matrix(framename, svm_output);

    // left
    IV2003_compute_feature_vectors_CR_Image(NULL, 0, i, mov->ncam_im[1], NULL, 0, NULL);

    // classify left
    
    for (y = ts_top, row = 0, index = 0; y <= ts_bottom; y += ts_y_delta, row++)
      for (x = ts_left, col = 0; x <= ts_right; x += ts_x_delta, col++) {
	for (j = 0; j < svm_input->rows; j++)
	  svm_input->x[j] = MATRC(ts_feature_vectors, index, j + 79);
	MATRC(svm_output, row, col) = classify_pattern_SVM(svm_input, left_svm);
	index++;
      }
    
    // write left

    //    sprintf(framename, "cm_frames\\left\\segmented\\%i_left_16_55_40_3_18_2003.seg", i);
    sprintf(framename, "cm_frames\\%i_left_16_55_40_3_18_2003.seg", i);
    write_dlm_CR_Matrix(framename, svm_output);

    // right
    IV2003_compute_feature_vectors_CR_Image(NULL, 0, i, mov->ncam_im[2], NULL, 0, NULL);

    // classify right
    
    for (y = ts_top, row = 0, index = 0; y <= ts_bottom; y += ts_y_delta, row++)
      for (x = ts_left, col = 0; x <= ts_right; x += ts_x_delta, col++) {
	for (j = 0; j < svm_input->rows; j++)
	  svm_input->x[j] = MATRC(ts_feature_vectors, index, j + 79);
	MATRC(svm_output, row, col) = classify_pattern_SVM(svm_input, right_svm);
	index++;
      }
    
    // write right

    //    sprintf(framename, "cm_frames\\right\\segmented\\%i_right_16_55_40_3_18_2003.seg", i);
    sprintf(framename, "cm_frames\\%i_right_16_55_40_3_18_2003.seg", i);
    write_dlm_CR_Matrix(framename, svm_output);

    // bilinear interpolation of unclassified pixels
    
    // display... 
    
    // ...or write to CAN movie file as grayscale image where 0 is low end of SVM response & 255 is high end
    // and 128 is decision line (put in luminance channel)
    
  }

  printf("%lf fps\n", (double) (7184-1035+1) / iv_timedelta());

  // finish

  exit(1);

  return im;
}

//----------------------------------------------------------------------------

void IV2003_grab_CR_CG(CR_Image *im)
{
}
  
//----------------------------------------------------------------------------
  
void IV2003_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
  //  write_avi_finish_CR_Movie(track_3d_mov);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

CR_Movie *laseravi;

CR_Image *Laser_to_AVI_init_CR_CG(char *name)
{
  CR_Image *im;
  char *filename;
  int i, j, framenum, index, red, green, blue, temp;

  // create image

  im = make_CR_Image(D3_LASER_WIDTH, D3_LASER_HEIGHT, 0);

  // user selects laser movie

  filename = choose_open_file_dialog_win32("Movies (fw*)\0fw*\0");

  // open laser movie

  lasermov = open_CR_Movie(filename);
  get_frame_CR_Movie(lasermov, lasermov->currentframe);

  // start avi

  laseravi = write_avi_initialize_CR_Movie("laser.avi", im->w, im->h, 30);

  // play laser movie and write avi frames

  for (framenum = 0; framenum < lasermov->numframes; framenum++) {
    
    CR_flush_printf("writing frame %i...", framenum);
    // get im from current laser frame

    // red = 0 -> [0, 512], 255 -> [513, 1024]
    // green = 0 -> lower 256 of range indicated by red value, 255 -> upper 256 of that range
    // blue = value within 256-wide range specified by red and green

    for (j = 0, index = 0; j < D3_LASER_HEIGHT; j++)
      for (i = 0; i < D3_LASER_WIDTH; i++) {
	//	red = 255 * (lasermov->laserim[framenum].data[index] / 2);
	//	green = 255 * ((lasermov->laserim[framenum].data[index] / 4) % 2);
	//	blue = 	lasermov->laserim[framenum].data[index] % 256;
	temp = lasermov->laserim[framenum].data[index];
	if (temp < 256) {
	  red = temp;
	  green = 0;
	  blue = 0;
	}
	else if (temp < 512) {
	  red = 255;
	  green = temp - 256;
	  blue = 0;
	}
	else if (temp < 768) {
	  red = 255;
	  green = 255;
	  blue = temp - 512;
	}
	else {
	  red = 255;
	  green = 255;
	  blue = 255;
	}

	IMXY_R(im, i, j) = (unsigned char) red;
	IMXY_G(im, i, j) = (unsigned char) green;
	IMXY_B(im, i, j) = (unsigned char) blue;

	index++;
      }

    write_avi_addframe_CR_Movie(framenum, im, laseravi);
    CR_flush_printf("done\n");
  }

  // finish

  write_avi_finish_CR_Movie(laseravi);

  exit(1);

  return im;
}

//----------------------------------------------------------------------------

void Laser_to_AVI_grab_CR_CG(CR_Image *im)
{
}
  
//----------------------------------------------------------------------------
  
void Laser_to_AVI_end_CR_CG(CR_Image *im)
{
  printf("ending!\n");
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
