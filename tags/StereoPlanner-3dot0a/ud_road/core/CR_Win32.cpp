//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_Win32.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

HWND toplevel_hWnd;

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void initialize_win32()
{
  HINSTANCE hInstance;

  hInstance = GetModuleHandle(NULL);

  LoadIcon(hInstance, TEXT("FusionIcon"));
  
  // make a dummy window

  toplevel_hWnd = CreateWindowEx(WS_EX_CLIENTEDGE,
				 "win32",
				 "win32",
				 WS_OVERLAPPEDWINDOW,
				 CW_USEDEFAULT, CW_USEDEFAULT, 50, 50,
				 NULL, NULL, hInstance, NULL);

  if (CoInitialize(NULL) != NOERROR) {
    printf("initialize_win32(): couldn't initialize COM\n");
    exit(1);
  }
}

//----------------------------------------------------------------------------

// open dialog box so user can pick input file

char *choose_open_file_dialog_win32(char *filter)
{
  OPENFILENAME ofn;
  char *filename;

  filename = (char *) calloc(MAX_PATH, sizeof(char));

  ZeroMemory(&ofn, sizeof(ofn));
  filename[0] = 0;
  
  ofn.lStructSize = sizeof(ofn);
  ofn.hwndOwner = toplevel_hWnd;
  ofn.lpstrFilter = filter;
  //  ofn.lpstrFilter = "All Files (*.*)\0*.*\0Images (*.bmp; *.ppm; *.jpg)\0*.bmp;*.ppm;*.jpg\0Movies (*.mpg; *.avi; *.mov)\0*.mpg;*.m1v;*.avi;*.mov\0";
  ofn.lpstrFile = filename;
  ofn.nMaxFile = MAX_PATH;
  ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
  ofn.lpstrDefExt = "txt";
  
  if (GetOpenFileName(&ofn))
    return filename;
  else
    return NULL;  
}

//----------------------------------------------------------------------------

// open dialog box so user can pick output file

char *choose_save_file_dialog_win32(char *filter)
{
  OPENFILENAME ofn;
  char *filename;

  filename = (char *) calloc(MAX_PATH, sizeof(char));

  ZeroMemory(&ofn, sizeof(ofn));
  filename[0] = 0;
  
  ofn.lStructSize = sizeof(ofn);
  ofn.hwndOwner = toplevel_hWnd;
  ofn.lpstrFilter = filter;
  //  ofn.lpstrFilter = "All Files (*.*)\0*.*\0Images (*.bmp; *.ppm; *.jpg)\0*.bmp;*.ppm;*.jpg\0Movies (*.mpg; *.avi; *.mov)\0*.mpg;*.m1v;*.avi;*.mov\0";
  ofn.lpstrFile = filename;
  ofn.nMaxFile = MAX_PATH;
  ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
  ofn.lpstrDefExt = "txt";
  
  if (GetSaveFileName(&ofn))
    return filename;
  else
    return NULL;  
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
