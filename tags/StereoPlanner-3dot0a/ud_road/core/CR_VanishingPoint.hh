//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// CR_VanishingPoint
//----------------------------------------------------------------------------

#ifndef CR_VANISHINGPOINT_DECS

//----------------------------------------------------------------------------

#define CR_VANISHINGPOINT_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>

#include <fftw3.h>

#include "CR_Matrix.hh"
#include "CR_Image.hh"
#include "CR_Memory.hh"
#include "CR_ParticleFilter.hh"
#include "CR_OpenGL.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

//-------------------------------------------------
// functions
//-------------------------------------------------

void votes_midpoint_line(float, float, float, float, float, float *, int, int);

double myround(double);
void PlanX_samp_prior(CR_Vector *);
void PlanX_samp_state(CR_Vector *, CR_Vector *);
void PlanX_dyn_state(CR_Vector *, CR_Vector *);
double PlanX_condprob_zx(void *, CR_Vector *);
double PlanX_strip_condprob_zx(void *, CR_Vector *);
double pf_PlanX_vote(double, double, int, int, int);
double gl_pf_PlanX_vote(double, double, int, int, int);
void initialize_vp_particlefilter();
void update_vp_particlefilter();
void draw_vp_particlefilter(CR_Image *);
void reset_vp_particlefilter();

void PlanX_vote_DIST(double, double, int, int, int, int);
void tally_votes(CR_Image *);
void initialize_voting();
void init_candidate_and_voter_regions(int, int, int, int, int, int, int, int, int, int, int, int, int);
void compute_dominant_orientations();
void PlanX_run_gabor_filters(CR_Image *);
void fft_run_gabor_filters(CR_Image *);
void PlanX_initialize_gabor_filters(double, double, int, double, double, int, CR_Image *);

void draw_vp_votes(CR_Image *);
void compute_vp_votes(CR_Image *);
void compute_moving_vp_votes(int, CR_Image *);


//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif
