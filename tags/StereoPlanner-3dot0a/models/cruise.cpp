/***************************************************************************************/
//FILE: 	cruise.cpp
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file has the implementation of the methods
//
/***************************************************************************************/

#include "cruise.h"
#include <iostream>

using namespace std;

// Default constructor for the bobDataWrapper class
cruiseDataWrapper::cruiseDataWrapper() {

}

// Default destructor for the bobDataWrapper class
cruiseDataWrapper::~cruiseDataWrapper() {

}

// Prints the data in a cruiseDataWrapper object to the screen
void cruiseDataWrapper::print_to_screen() {

	// Print 10 decimal places (arbitrary!) to show we have accuracy
	cout << setprecision(10);
	cout << "\nx position: " << data.err;
	cout << "\ny position: " << data.derr;
	cout << "\nvelocity  : " << data.refVel;
}

// This is exactly the same code as the above print_to_screen function, only out_file has replaced cout
void cruiseDataWrapper::write_to_file(char *filename) {

	// Open the file for appending
	ofstream out_file(filename, ios::out | ios::app | ios::binary);

	// And then do the exact same stuff as above
	out_file << setprecision(10);
	out_file << "\nx position: " << data.err;
	out_file << "\ny position: " << data.derr;
	out_file << "\nvelocity  : " << data.refVel;
	out_file << "\n";
}

// This method gets the values
void cruiseDataWrapper::get_data(cruiseData &buffer){
  data.err = data.err;
  buffer.derr = data.derr;
  buffer.refVel = data.refVel;
  buffer.actuator = data.actuator;
}

// This method updates the data
void cruiseDataWrapper::update_data(cruiseData buffer){
  data.err = buffer.err;
  data.derr = buffer.derr;
  data.refVel = buffer.refVel;
  data.actuator = buffer.actuator;
}
