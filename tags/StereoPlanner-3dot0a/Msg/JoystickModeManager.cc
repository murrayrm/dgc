// dgc_msg_main.cc 

#include "dgc_mta.hh"
#include "dgc_msg.h"
#include "dgc_socket.h"
#include "dgc_messages.h"
#include "dgc_const.h"
#include "dgc_mta_main.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> 

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <iostream>
#include <list>
#include <time.h>


struct steer_struct {

  float x, y;
  int buttons;
};


int shutdown_flag;
using namespace std;

int main(int argc, char* argv[])
{
  steer_struct mySS;

  shutdown_flag = FALSE;
  cout << endl << endl << "Registering" << endl;
  dgc_msg_Register(dgcs_ModeManager);
  dgc_MsgHeader myMH;

  while ( TRUE ) { 

    usleep ( 30000 );
    if ( dgc_PollMessage(myMH) == TRUE) {
      switch (myMH.MsgType) {
      case dgcm_JoystickStatePacket:
	// Get the message
	if ( dgc_GetMessage(myMH, &mySS) == TRUE ) {

	  // Forward the gas/brake pedal portion to the gas/brake driver
	  myMH.Destination = 0;
	  myMH.DestinationType = dgcs_Jeremy;
	  myMH.MsgType = dgcm_AccelStatePacket;
	  myMH.MsgSize = sizeof(float);
	  dgc_SendMessage(myMH, & mySS.y);
	  
	  // Forward the gas/brake pedal portion to the gas/brake driver 
	  myMH.Destination = 0;
	  myMH.DestinationType = dgcs_SteeringDriver;
	  myMH.MsgType = dgcm_SteeringStatePacket;
	  myMH.MsgSize = sizeof(float);
	  dgc_SendMessage(myMH, & mySS.x);

	  
	  cout << "Msg Received: x = " << mySS.x << endl;
	  cout << "              y = " << mySS.y << endl;
	  cout << "        buttons = " << mySS.buttons << endl;
	  cout << endl;
	  
	}
      }
    }
  }
  cout << endl << endl << "Unregistering" << endl;
  dgc_msg_Unregister();

  return 0;
}














