#ifndef dgc_socket_H
#define dgc_socket_H

#include <sys/socket.h>

int dgc_OpenCSocket(int port);

int dgc_OpenSSocket(int port);

int dgc_AcceptSSocket(int fd, sockaddr & sa, socklen_t & sl);


int dgc_CloseSocket(int fd);

int dgc_ReadSocket(int fd, void * buffer, int sz, int timeout=0);

int dgc_WriteSocket(int fd, void * buffer, int sz, int timeout=0);


#endif


