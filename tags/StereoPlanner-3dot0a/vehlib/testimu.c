#include <stdio.h>
#include <math.h>
#include "kfilter.h"
#define IMU_LEN 8
#include "Kalmano.h"
#include "Global.h"

extern int New_GPS_data_flag;
extern int New_Mag_data_flag;
extern TSaveNavData NavBuffer[XRATIO2];
extern int NavBufferIndex;


extern double gps_lat, gps_lon, gps_alt;
extern double gps_vn, gps_ve, gps_vu;
extern double gps_time;
extern double imu_time;


extern double imu_dvx,imu_dvy,imu_dvz; // Delta-v's m/s (should be)
extern double imu_dtx,imu_dty,imu_dtz; // Delta-Thetas,should be rads

extern double mag_hdg;
extern double ExtGPSPosMeas_x, ExtGPSPosMeas_y, ExtGPSPosMeas_z;
extern double ExtGPSVelMeas_x, ExtGPSVelMeas_y, ExtGPSVelMeas_z;
extern double kp[NUM_ST+1][NUM_ST+1];
extern double kx[NUM_ST+1];
extern double tot_kabx,tot_kaby,tot_kabz;
extern double tot_kasfx,tot_kasfy,tot_kasfz;
extern double tot_kgbx,tot_kgby,tot_kgbz;
extern double tot_kgsfx;
TSaveNavData navnow;
int main() 

{
  /* Read in data  */
  FILE* datafile; 
FILE*  writefile;
FILE*  writefile2;
int flag,i;
double time;
double gps_lat_reading = 0.595892;
double gps_lon_reading = -2.06165;
double gps_alt_reading = 100;
 double mag_hdg_reading = 3.2812189;
int gps_ctr = 0;

DGCNavInit();
imu_time = 0.0;
 writefile = fopen( "param2.dat", "w");
 writefile2 = fopen( "nav2.dat", "w");
for(i=0;i<1000;i++){
  printf("%d\n",i);
datafile = fopen( "imu4.dat" , "r");


flag =  fscanf(datafile,"%le%le%le%le%le%le%le",&time,&imu_dvx,&imu_dvy,&imu_dvz,&imu_dtx,&imu_dty,&imu_dtz);
while (flag > 0){
//printf("%.9e %.9e %.9e %.9e %.9e %.9e %.9e\n",time,imu_dvx,imu_dvy,imu_dvz,imu_dtx,imu_dty,imu_dtz);
if (gps_ctr == 400) {
if (New_GPS_data_flag == 0) {
New_GPS_data_flag = 1;
gps_lat = gps_lat_reading;
gps_lon = gps_lon_reading;
gps_alt = gps_alt_reading;
 gps_vn = 0.0;
 gps_ve = 0.0;
 gps_vu = 0.0;
gps_time = imu_time - 0.0012;
//printf("More GPS Data!\n");


}
if (New_Mag_data_flag == 0) {
New_Mag_data_flag = 1;
mag_hdg = mag_hdg_reading;
}
gps_ctr = 0;
 navnow = NavBuffer[NavBufferIndex];
printf("Pos: %f %f %f\n",NavBuffer[NavBufferIndex].lat,NavBuffer[NavBufferIndex].lon,NavBuffer[NavBufferIndex].alt);
printf("Angle:%f %f %f\n",NavBuffer[NavBufferIndex].roll,NavBuffer[NavBufferIndex].pitch,NavBuffer[NavBufferIndex].yaw);
//printf("%f\n",NavBuffer[NavBufferIndex].calpha);
//printf("%f\n",NavBuffer[NavBufferIndex].time);
printf("Vel: %f %f %f\n",NavBuffer[NavBufferIndex].vx,NavBuffer[NavBufferIndex].vy,NavBuffer[NavBufferIndex].vz);

//printf("Resid: %f %f %f\n",ExtGPSPosMeas_x,ExtGPSPosMeas_y,ExtGPSPosMeas_z);
//printf("Resid: %f %f %f\n",ExtGPSVelMeas_x,ExtGPSVelMeas_y,ExtGPSVelMeas_z);
// printf("Accel SF P: %f %f %f\n",kx[17],kx[18],kx[19]);
printf("Accel B: %f %f %f \n",tot_kabx,tot_kaby,tot_kabz);
// printf("Gyro SF: %e \n",tot_kgsfx);
// printf("Gyro B: %e %e %e \n",tot_kgbx,tot_kgby,tot_kgbz);
 fprintf(writefile,"%e %e %e %e %e %e %e %e %e %e\n",tot_kabx,tot_kaby,tot_kabz,tot_kasfx,tot_kasfy,tot_kasfz,tot_kgbx,tot_kgby,tot_kgbz,tot_kasfx);
 fprintf(writefile2,"%le %le %le %le %le %le %le %le %le\n",navnow.lat,navnow.lon,navnow.alt,navnow.calpha,navnow.roll,navnow.pitch,navnow.yaw,navnow.vx,navnow.vy,navnow.vz);
}
gps_ctr++;

DGCNavRun();
imu_time = imu_time + 0.0025;

flag =  fscanf(datafile,"%le%le%le%le%le%le%le",&time,&imu_dvx,&imu_dvy,&imu_dvz,&imu_dtx,&imu_dty,&imu_dtz);
 imu_dvx = -imu_dvx;
 imu_dvy = -imu_dvy;
 imu_dtx = -imu_dtx;
 imu_dty = -imu_dty; 
}

fclose(datafile);

}
fclose(writefile);
 fclose(writefile2);

}
