# Makefile for vdrive 
# RMM, 29 Dec 03
#
# Usage:
#   make			make working copy of all programs
#   make install		install development copy (pgm-dev)
#   make REV=rev isntall	install tagged copy (pgm-rev)
#
# For revised versions of files, edit Changes to show when tag was created
# and use "cvs tag rev" to tag files with version number.
#

# Revision name and release directory
REV = dev
RELDIR = /usr/local/dgc

PGMS = vdrive vstate vehlib.a kfstate #faxstate 
INCS = serial.h imu.h gps.h magnetometer.h VDrive.hh VState.hh \
	 StateStructMsg.hh

DGC = ..
CDD = $(DGC)/bin/cdd
INC = -I$(DGC)/include
LIB = $(DGC)/lib/kfilter.a ${DGC}/lib/frames.a $(DGC)/lib/sparrow.a -ltermcap -lcurses -lpthread -lboost_thread

MTALIB = $(DGC)/lib/mta2_1.a 
MTAINC = $(DGC)/include

CC = g++
CFLAGS = -g $(INC) -Wno-deprecated
CPPFLAGS = $(CFLAGS)

all: $(PGMS) vehlib.html vdrive.ini vdrive.dev TAGS
install: all $(DGC)/bin $(DGC)/doc $(DGC)/lib $(DGC)/include/vehlib
	install -s vdrive $(DGC)/bin/vdrive-$(REV)
	install -s vstate $(DGC)/bin/vstate-$(REV)
	install -m 644 vehlib.html $(DGC)/doc/vehlib.html
	install -m 644 vehlib.a $(DGC)/lib/vehlib-$(REV).a
	install -m 644 $(INCS) $(DGC)/include/vehlib

# Release the code to the public
release: all
	make REV=$(REV) DGC=$(RELDIR) install
	echo "Remember to tag cvs files at this point"

$(DGC)/bin:; mkdir $@
$(DGC)/doc:; mkdir $@
$(DGC)/lib:; mkdir $@
$(DGC)/include/vehlib:; mkdir $@

# vehlib library
SRC = joystick.c parallel.c imu.cc gps.cc serial.cc rc32.cc steer.cc brake.c \
	accel.c magnetometer.c cruise.cc LUtable.cc \
	statefilter.cc LatLong.cpp strdisp.c estop.c
OBJ = joystick.o parallel.o imu.o gps.o serial.o crc32.o steer.o brake.o \
	accel.o magnetometer.o cruise.o LUtable.o \
	statefilter.o LatLong.o strdisp.o estop.o

VEHLIB = vehlib.a

$(VEHLIB): $(OBJ)
	ar r $@ $?

# vdrive program
vdrive: vdrive.o $(VEHLIB) vdmta.o $(MTALIB)
	$(CC) $(CFLAGS) -o $@ vdrive.o vdmta.o $(VEHLIB) $(MTALIB) $(LIB)

# if initialiation and device configuration files don't exist, create them
vdrive.ini:;	cp vdrive.ini-def vdrive.ini
vdrive.dev:;	cp vdrive.dev-def vdrive.dev

# vstate program
vstate: vstate.o $(VEHLIB) vsmta.o $(MTALIB)
	$(CC) $(CFLAGS) -o $@ vstate.o vsmta.o $(VEHLIB) $(MTALIB) $(LIB)

faxstate: faxstate.o $(VEHLIB) vsmta.o $(MTALIB)
	$(CC) $(CFLAGS) -o $@ faxstate.o vsmta.o $(VEHLIB) $(MTALIB) $(LIB)

kfstate: kfstate.o  $(VEHLIB) vsmta.o $(MTALIB)
	$(CC) $(CFLAGS) -o $@ kfstate.o vsmta.o $(VEHLIB) $(MTALIB) $(LIB)

# Display files
vddisp.h: vddisp.dd $(CDD);	$(CDD) -o $@ vddisp.dd
vsdisp.h: vsdisp.dd $(CDD);	$(CDD) -o $@ vsdisp.dd
actdev.h: actdev.dd $(CDD);	$(CDD) -o $@ actdev.dd
strdisp.h: strdisp.dd $(CDD);	$(CDD) -o $@ strdisp.dd
kfdisp.h: kfdisp.dd $(CDD);	$(CDD) -o $@ kfdisp.dd

#
# Documentation
#
TXISRC = vehlib.txi steer.txi vdrive.txi
vehlib.html: $(TXISRC)
	makeinfo --html --no-split -o $@ vehlib.txi

vehlib.info: $(TXISRC)
	makeinfo --no-split -o $@ vehlib.txi

clean:
	/bin/rm -f *.o *~ Makefile.bak $(PGMS)

depend:
	makedepend -Y $(CFLAGS) $(SRC) vdrive.c vstate.cc vdmta.cc

# Generate a TAGS file
TAGS: *.c *.h *.cc
	etags *.[ch] *.cc

# Files not picked up by makedepend (because the may not exist when it is run)
vdrive.o: vddisp.h actdev.h
vstate.o: vsdisp.h
faxstate.o: kfdisp.h
kfstate.o: kfdisp.h
strdisp.o: strdisp.h

### Testing brake sensors and throttle actuator
moveTest: moveTest.o $(VEHLIB) parallel.o brake.o accel.o $(MTALIB)
	$(CC) $(CFLAGS) -o $@ moveTest.o parallel.o brake.o accel.o  $(VEHLIB) $(MTALIB) $(LIB)
moveTest.o: moveTest.cpp parallel.h brake.h accel.h
###

# DO NOT DELETE

joystick.o: joystick.h parallel.h ../include/sparrow/dbglib.h
parallel.o: parallel.h ../include/sparrow/dbglib.h
imu.o: crc32.h imu_net.h imu.h ../include/sparrow/dbglib.h vehports.h
gps.o: gps.h serial.h
serial.o: ../include/sparrow/dbglib.h serial.h
steer.o: steer.h serial.h ../include/sparrow/dbglib.h
steer.o: ../include/sparrow/display.h strdisp.h
brake.o: vehports.h brake.h serial.h parallel.h accel.h
brake.o: ../include/sparrow/dbglib.h
accel.o: accel.h parallel.h vehports.h ../include/sparrow/dbglib.h
magnetometer.o: magnetometer.h serial.h ../include/sparrow/dbglib.h
cruise.o: cruise.h LUtable.h
LUtable.o: LUtable.h
statefilter.o: VState.hh ../include/MTA/Misc/Time/Timeval.hh
statefilter.o: ../include/MTA/Misc/Mail/Mail.hh ../include/MTA/KernelDef.hh
statefilter.o: ../include/MTA/Misc/File/File.hh
statefilter.o: ../include/MTA/Misc/Time/Timeval.hh
statefilter.o: ../include/MTA/Misc/Pipes/BufferedPipe.hh
statefilter.o: ../include/MTA/Misc/Pipes/Pipe.hh
statefilter.o: ../include/MTA/Misc/Memory/SharedMemoryContainer.hh
statefilter.o: ../include/MTA/Misc/Structures/IP.hh
statefilter.o: ../include/MTA/Misc/Structures/MailAddress.hh
statefilter.o: ../include/MTA/Misc/Structures/IP.hh imu.h gps.h serial.h
statefilter.o: magnetometer.h LatLong.h 
statefilter.o: ../include/sparrow/dbglib.h ../include/sparrow/display.h
statefilter.o: statefilter.hh
LatLong.o: LatLong.h
vdrive.o: ../include/sparrow/display.h ../include/sparrow/dbglib.h
vdrive.o: ../include/sparrow/channel.h ../include/sparrow/flag.h
vdrive.o: ../include/sparrow/keymap.h joystick.h gps.h serial.h imu.h steer.h
vdrive.o: brake.h parallel.h accel.h cruise.h LUtable.h vehports.h
vdrive.o: ../include/MTA/Modules.hh vdmta.hh ../include/MTA/DGC_MODULE.hh
vdrive.o: ../include/MTA/Misc/Mail/Mail.hh ../include/MTA/KernelDef.hh
vdrive.o: ../include/MTA/Misc/File/File.hh
vdrive.o: ../include/MTA/Misc/Time/Timeval.hh
vdrive.o: ../include/MTA/Misc/Pipes/BufferedPipe.hh
vdrive.o: ../include/MTA/Misc/Pipes/Pipe.hh
vdrive.o: ../include/MTA/Misc/Memory/SharedMemoryContainer.hh
vdrive.o: ../include/MTA/Misc/Structures/IP.hh
vdrive.o: ../include/MTA/Misc/Structures/MailAddress.hh
vdrive.o: ../include/MTA/Misc/Structures/IP.hh
vdrive.o: ../include/MTA/Misc/Structures/ModuleInfo.hh
vdrive.o: ../include/MTA/Misc/Time/Timeval.hh VDrive.hh
vdrive.o: ../include/MTA/Misc/Mail/Mail.hh ../include/MTA/Kernel.hh
vdrive.o: ../include/MTA/Friends.hh ../include/MTA/Modules.hh
vdrive.o: ../include/MTA/DGC_MODULE.hh ../include/MTA/KernelDef.hh
vdrive.o: ../include/MTA/Misc/Socket/Socket.hh
vdrive.o: ../include/MTA/Misc/File/File.hh
vdrive.o: ../include/MTA/Misc/Time/Timeval.hh ../include/MTA/Debug.hh
vdrive.o: VState.hh magnetometer.h actdev.h vddisp.h
vstate.o: ../include/sparrow/display.h ../include/sparrow/dbglib.h
vstate.o: ../include/sparrow/channel.h gps.h serial.h imu.h magnetometer.h
vstate.o:  vehports.h ../include/MTA/Modules.hh
vstate.o: vsmta.hh ../include/MTA/DGC_MODULE.hh
vstate.o: ../include/MTA/Misc/Mail/Mail.hh ../include/MTA/KernelDef.hh
vstate.o: ../include/MTA/Misc/File/File.hh
vstate.o: ../include/MTA/Misc/Time/Timeval.hh
vstate.o: ../include/MTA/Misc/Pipes/BufferedPipe.hh
vstate.o: ../include/MTA/Misc/Pipes/Pipe.hh
vstate.o: ../include/MTA/Misc/Memory/SharedMemoryContainer.hh
vstate.o: ../include/MTA/Misc/Structures/IP.hh
vstate.o: ../include/MTA/Misc/Structures/MailAddress.hh
vstate.o: ../include/MTA/Misc/Structures/IP.hh
vstate.o: ../include/MTA/Misc/Structures/ModuleInfo.hh
vstate.o: ../include/MTA/Misc/Time/Timeval.hh VState.hh
vstate.o: ../include/MTA/Misc/Mail/Mail.hh ../include/MTA/Kernel.hh
vstate.o: ../include/MTA/Friends.hh ../include/MTA/Modules.hh
vstate.o: ../include/MTA/DGC_MODULE.hh ../include/MTA/KernelDef.hh
vstate.o: ../include/MTA/Misc/Socket/Socket.hh
vstate.o: ../include/MTA/Misc/File/File.hh
vstate.o: ../include/MTA/Misc/Time/Timeval.hh ../include/MTA/Debug.hh
vstate.o: statefilter.hh vsdisp.h
vdmta.o: ../include/MTA/Kernel.hh ../include/MTA/Friends.hh
vdmta.o: ../include/MTA/Modules.hh ../include/MTA/DGC_MODULE.hh
vdmta.o: ../include/MTA/KernelDef.hh
vdmta.o: ../include/MTA/Misc/Structures/ModuleInfo.hh
vdmta.o: ../include/MTA/Misc/Socket/Socket.hh
vdmta.o: ../include/MTA/Misc/File/File.hh ../include/MTA/Misc/Time/Timeval.hh
vdmta.o: ../include/MTA/Misc/File/File.hh ../include/MTA/Misc/Mail/Mail.hh
vdmta.o: ../include/MTA/KernelDef.hh
vdmta.o: ../include/MTA/Misc/Pipes/BufferedPipe.hh
vdmta.o: ../include/MTA/Misc/Pipes/Pipe.hh
vdmta.o: ../include/MTA/Misc/Memory/SharedMemoryContainer.hh
vdmta.o: ../include/MTA/Misc/Structures/IP.hh
vdmta.o: ../include/MTA/Misc/Structures/MailAddress.hh
vdmta.o: ../include/MTA/Misc/Structures/IP.hh
vdmta.o: ../include/MTA/Misc/Time/Timeval.hh ../include/MTA/Debug.hh
vdmta.o: ../include/MTA/Misc/Time/Time.hh ../include/sparrow/dbglib.h
vdmta.o: vdmta.hh ../include/MTA/DGC_MODULE.hh
vdmta.o: ../include/MTA/Misc/Time/Timeval.hh VDrive.hh
vdmta.o: ../include/MTA/Misc/Mail/Mail.hh VState.hh imu.h gps.h serial.h
vdmta.o: magnetometer.h
