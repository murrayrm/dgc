#ifndef __CRC32_H_
#define __CRC32_H_

#define CRC32_SIZE	4		/* sizeof crc32 in bytes */

void crc32_big_endian (const void *data, unsigned len, unsigned char *crc32_out);

#endif
