#ifndef WAYPOINTNAV_HH
#define WAYPOINTNAV_HH


#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "WaypointNavDatum.hh"


using namespace std;
using namespace boost;


class WaypointNav : public DGC_MODULE {
public:
  // and change the constructors
  WaypointNav();
  ~WaypointNav();

  // The states in the state machine.
  // Uncomment these if you wish to define these functions.

  void Init();
  void Active();
  //  void Standby();
  void Shutdown();
  //  void Restart();

  // The message handlers. 
  //  void InMailHandler(Mail & ml);
  //  Mail QueryMailHandler(Mail & ml);

  // the status function.
  //string Status();


  // Any functions which run separate threads
  
  // Method protocols
  void GrabVStateLoop();

  void InitGlobal();
  void GlobalLoop();
  
  int  InitDFE();
  void DFELoop();
  
  int  InitArbiter();
  void ArbiterLoop();

  int  InitLadar();
  void LadarLoop();
	
  int  InitStereo();
  void StereoLoop();

  void SimInit();
  void SimUpdate(float deltaT);

  void SparrowDisplayLoop();

private:
  // and a datum named d.
  WaypointNavDatum d;
};



// enumerate all module messages that could be received
namespace WaypointNavMessages {
  enum {
    // we are not yet receiving messages.
    // A place holder
    NumMessages
  };
};






#endif
