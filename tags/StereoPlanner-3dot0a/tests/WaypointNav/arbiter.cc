/*
** arbiter.cc
**
** Copied over from waypointnav/accel.cc
** Date: 1/3/04
**
**
**
*/

#include "WaypointNav.hh" // standard includes, arbiter, datum, ...

#include <iomanip>

extern int SIM;
extern int PRINT_VOTES;

// ADDED BY MIKE THIELMAN for Sparrow display
extern double waydisp_arbiter_p[];
extern double waydisp_arbiter_v[];
extern int    waydisp_arbiter_g[];

int WaypointNav::InitArbiter() {
 
  return true; // success

} // end WaypointNav::InitArbiter()


// This function takes in the state data of the vehicle and commands
// actuators. In case of waypoint following (added 11/30/2003) the function
// will commannd both steering and throttle/brake actuators. 
void WaypointNav::ArbiterLoop() {

  // for vote display in Sparrow
  Arbiter::combinedVoteListType combinedVotes;
  
  while( !ShuttingDown() ) {

    // Run arbiter with the current Voter values: do what runArb() does
    (d.demoArb).getVotes();
    (d.demoArb).combineVotes();
    (d.demoArb).pickBestArc();

    // ADDED BY LARS CREMEAN for Sparrow display
    // VERIFIED that these are the same as the votes that come from
    // printCombinedVotes
    int i=0; 
    combinedVotes = (d.demoArb).getCombinedVoteList(); 
    for( Arbiter::combinedVoteListType::iterator 
		    iter = combinedVotes.begin(); 
		    iter != combinedVotes.end(); ++iter, ++i ) {
      waydisp_arbiter_p[i] = iter->phi*180.0/M_PI;
      waydisp_arbiter_v[i] = iter->velo;
      waydisp_arbiter_g[i] = iter->goodness;
    }

    // Finally update command values of this method
    d.Steer = ( (d.demoArb).getBestPhi() ) / MAX_STEER;
    d.TargetVelocity = (d.demoArb).getBestVelo();

    // Send command to steering actuator
    if( d.Steer < -1.0) {
      d.Steer = -1.0;
    } else if ( d.Steer > 1.0) {
      d.Steer = 1.0;
    }

    if( ! SIM) {
      Mail msg = NewOutMessage(MyAddress(), MODULES::VDrive, VDriveMessages::CmdMotion); 
      VDrive_CmdMotionMsg vdcmm;
      vdcmm.Timestamp    = TVNow();
      vdcmm.velocity_cmd = d.TargetVelocity;
      vdcmm.steer_cmd    = d.Steer;
      msg.QueueTo(&vdcmm, sizeof(vdcmm));
      SendMail(msg);
    }

    // for the simulator, calculate what the throttle should be 
    //d.Accel = cruise(d.SS.Speed, d.TargetVelocity, d.Accel);    
    //    cout << "Accel Now Set To " << d.Accel << endl;
    d.Accel = 0.05;

    usleep(100000);

  }  // end while(!ShuttingDown())

  if( ! SIM) {
    Mail msg = NewOutMessage(MyAddress(), MODULES::VDrive, VDriveMessages::CmdMotion); 
    VDrive_CmdMotionMsg vdcmm;
    vdcmm.Timestamp    = TVNow();
    vdcmm.velocity_cmd = 0;
    vdcmm.steer_cmd    = 0;
    msg.QueueTo(&vdcmm, sizeof(vdcmm));
    SendMail(msg);
  }
} // end WaypointNav::ArbiterLoop()

