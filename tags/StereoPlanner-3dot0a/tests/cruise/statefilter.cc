#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>


#include "cruisemain.hh"
#include "../../latlong/LatLong.h"

void FilterState(state_struct &ss);


int InitStateFilter(DATUM &d) {

  d.stateValid = FALSE;
  d.stateNewData = FALSE;
  d.SS.speed = 0.0;
  cout << "Finished initializing State Filter" << endl;
  return TRUE; // success
}

void StateFilterLoop(DATUM &d) {

  LatLong latlong(0, 0);
  while(d.shutdown_flag == FALSE) {
    if(d.gpsValid) {// && d.magValid) {
      if(d.gpsNewData) {
	// lock gps and state
	boost::recursive_mutex::scoped_lock slGPS(d.gpsLock);
	boost::recursive_mutex::scoped_lock slSTATE(d.stateLock);
	latlong.set_latlon(d.gpsDW.data.lat, d.gpsDW.data.lng);
	
	latlong.get_UTM(&d.SS.easting, &d.SS.northing);  // convert to UTM
	d.SS.altitude = d.gpsDW.data.altitude;
	d.SS.vel_n    = d.gpsDW.data.vel_n;
	d.SS.vel_e    = d.gpsDW.data.vel_e;
	d.SS.vel_u    = d.gpsDW.data.vel_u;
	// get magnitude of velocity (in 2d only)
	d.SS.speed = sqrt(d.SS.vel_n * d.SS.vel_n + d.SS.vel_e * d.SS.vel_e);
	//      ss.timestamp = timestamp;

	d.stateValid = TRUE;
	//FilterState(d.SS);
	d.stateNewData = TRUE; // set state flag
	d.gpsNewData = FALSE;  // reset GPS flag
	continue; // start the loop over
      }
      if(FALSE ) {//d.magNewData)	{
	// lock mag and state
	boost::recursive_mutex::scoped_lock slMAG(d.magLock);
	boost::recursive_mutex::scoped_lock slSTATE(d.stateLock);
	
	d.SS.heading = d.magBuffer[0];
	d.SS.pitch   = d.magBuffer[1];
	d.SS.roll    = d.magBuffer[2];
	
	FilterState(d.SS);
	d.stateNewData = TRUE;
	d.magNewData = FALSE; // reset 
	continue; // start the loop over
      }      
      // if we get here, then there was no new gps or mag data
      // so sleep for 10ms and try again
      usleep_for(10000);
    } else {
      // gps or mag not valid
      d.stateValid == FALSE;
      usleep_for(1000000); // wait to do anything until we have data
    }
  }
}



void FilterState(state_struct &ss) {
  // double speed; 
   // this speed is obtained from integrating east and north velocity
   double gps_heading; //heading calculated from gps
   float radHeading = ss.heading * M_PI / 180.0;
   //if(ss.vel_n < VEL_N_VALID_CUTOFF) ss.vel_n=0; 
   //if(ss.vel_e < VEL_E_VALID_CUTOFF) ss.vel_e=0; 
   //if(ss.vel_u < VEL_U_VALID_CUTOFF) ss.vel_u=0;
 

   // if the speed of the car is faster than cutoff, 
   // declare magnetometer reading invalid and use GPS values
   if( ss.speed > MAGNET_VALID_CUTOFF)
   {
	gps_heading = atan(ss.vel_e/ss.vel_n); // get results from -pi to pi
        if(gps_heading >= 0)
           gps_heading = (gps_heading/M_PI)  * 180.0; // convert to degrees
	else
           gps_heading = (gps_heading/M_PI)  * 180.0 + 360.0; // convert to degrees

 	ss.heading = gps_heading;  // update heading and heading vectors
	ss.heading_n = ss.vel_n;  
	ss.heading_e = ss.vel_e;
   }else{ // generate new heading vectors based on magnetometer heading and GPS speed
     // gps gives fairly good magnitude of speed but bad direction at low speeds
     // so use magnitude of gps and angles of magnetometer to generate new n/e velocities
     ss.heading_n = cos(radHeading);
     ss.heading_e = sin(radHeading);
     ss.vel_n = ss.speed * cos(radHeading);
     ss.vel_e = ss.speed * sin(radHeading);
   }
       
}
