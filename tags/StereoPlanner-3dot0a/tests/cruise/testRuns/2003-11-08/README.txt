Guide to tests conducted on Nov8th, 2003 at Santa Anita.
Most runs conducted on 1st gear unless otherwise noted
A = accel
B = brake
C = coast

Accel#.dat contains the throttle/brake settings (-1 to 1) and when they change
since the accel setting is constant between changes they are not logged

Trial	Type	Notes	Direction
4 Low			
1	A	20s @ .1	W
2	A	40s @ .1	E
3	A	15 s @ .2	
4	A	"3s @ .05, .1, .14, 10s @ .2"	
5	A	same as above	
6	A	same as above	

4 High			
7	A	"3s @ .05, .1, .14, 10s @ .2"	
8	A	30s @ .1	
9	A	30s .05	
10	A	"30s @ .075, OBD2 failure"	
11	A	30s @ .075	
12	A	15s @ .15	
13	AC	20s @ .15	
14	A	"25s @ .15, 5s coast"	
15	A	"steering for 2 min @ .1, OBD2 failure"	
16	AC	"30s @ .075, 1 min @ .1, 30s @ .075"	
17	AC	"15s @ .075, coast"	SW
18	AC	"15s @ .1, 30 coast"	N/NE
19	AC	"15s @ .1, coast"	SW
20	AC	"15 @ .1, coast"	N 
21	AC	"10 @ .2, coast"	S 
22	AC	same as above	E
23	AC	"5 @ .3, coast"	W
24	AC	"30 @ .2, tully accels to 20, brake fails"	W
25	AC	"30 @ .2, tully accels to 20 "	"E, then N"
26	AC	"5 @ .3, coast"	SW
27	AC	"10 s @ .3, coast "	NE
28	AC	"10 s @ .3, coast "	NE
29	AC	"10 s @ .3, cancelled"	SW
30	AC	"10 @ .35, coast "	NE
31	AC	"10 @ .3, coast "	SW
32	AC	"5s @ .4, coast (3rd gear, didn't shift)"	NE
33	AC	"10s @ .4, coast, shifted to 2nd"	SW
34	AC	"5s @ .5, coast, no shift"	NE
35	AC	"10s @ .5, coast, brake at end"	SW
36	B 	20s @ -.5	SW
37	B 	20 @ -.75	NE

Start at 25 mph			
38	B 	30s @ -.75	SW
39	B 	30s @ -.8	NE
40	B 	20s @ -.9	SW
41	B 	20s @ -.85	NE
42	B 	15 s @ -.95	SW
43	B 	10s @ -1	NE
44	B 	20s @ -.7	SW
45	B 	20s @ -.65	NE
46	B 	25s @ -.6	SW
47	B 	"30s @ -.55, throttled at end"	SW
48	B 	"30 s @ -.55, emergency brake at end"	NE
49	B 	"30s @ -.55, throttled at end"	SW
50	B 	"30s @ -.5, brake at end"	NE
51	B 	30s @ -.5	SW
