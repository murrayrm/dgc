/**
 * 12/6/2003   For demo today, we have a fixed set of arcs for every cycle.
 **/

#include "waypointnav.hh"
using namespace std;

int SIM;     // this is a global variable that modules use to keep track of 
             //  whether the code should be run in simulation mode
int DISPLAY; // this is a switch that determines whether we're using 
             //  ncurses display mode
int PRINT_VOTES; // whether each of the voters will call printVotes
                 // functionality not yet implemented              
 
// Declare a DATUM
DATUM d;

// BS for steering to work -- this stuff is actually used in steer functions
// WILL THESE BE OVERWRITTEN BY THE ACTUAL RESULT OF INIT_STEERING?
// OR SHOULD THESE ACTUALLY REFERENCE SOMETHING ELSE?
//  I DON'T THINK THESE ARE OVERWRITTEN BY VALUES FROM INIT_STEERING.
// WILL C. OR IKE SHOULD KNOW...  - SUE ANN (12/19/2003)
int negCountLimit        =    -60000;
int posCountLimit        =     60000;
int verbosity_exec_cmd   =      TRUE;
int steer_errno          =  ERR_NONE;
string lastSteeringCommanded;

// used for starting threads
boost::recursive_mutex StarterMutex;
int StarterCount;
void Starter();


// the "kill switch"
void shutdown(int signal) {

  cout << "Signal Received" << endl;
  d.shutdown_flag = TRUE;
  // boost::recursive_mutex::scoped_lock scoped_lock(WF.brakeMutex);
  //set_brake_abs_pos_pot(0.8, brakeVelo, brakeVelo);
  cout << "Set Abort Flag" << endl;
  //sleep_for(10);
  //exit(EXIT_SUCCESS);
}


int main(int argc, char **argv) {

  cout << "argc = " << argc << endl;
   
  SIM = 0;
  DISPLAY = 0;
  PRINT_VOTES = 0;
  ////////////////////////////
  // do some argument checking
  if( argc > 1 ) { // if we have arguments
  
    for( int i=1; i < argc; i++ ) { 
      // go through each of the arguments and check...
      cout << "argv[ " << i << "] = " << argv[i] << endl;

      // if the "-sim" argument was specified
      if( strcmp(argv[i], "-sim") == 0 ) {
        cout << "!!! -sim flag acknowledged, using simulation environment !!!" << endl << endl;
        SIM = 1;
      }
      // if the "-pv" argument was specified
      if( strcmp(argv[i], "-pv") == 0 ) {
        cout << "!!! -pv flag acknowledged, will print votes slowly !!!" << endl << endl;
        PRINT_VOTES = 1;
      }
      // if the "-disp" argument was specified
      if( strcmp(argv[i], "-disp") == 0 ) {
        cout << "!!! -disp flag acknowledged, about to use curses display environment !!!" << endl << endl;
        DISPLAY = 1;
      }
    }
  }
  // Display a message if we don't see some options set  
  if( SIM == 0 ) {
    cout << endl;
    cout << "!!! No -sim flag detected, using real environment !!!" << endl;
    cout << "    You can Ctrl-c here if you forgot to use -sim " << endl 
	 << endl;
  }
  if( DISPLAY == 0 ) {
      cout << endl;
      cout << "!!! No -disp flag detected, using stdout for display !!!" 
	   << endl;
      cout << "    You can Ctrl-c here if you forgot to use -disp " << endl 
	   << endl;
      DISPLAY = 0;
  }
  // Done doing argument checking.
  ////////////////////////////////
  

  cout << "Starting Main" << endl;
  // For Data Logging
  cout << "What test number? " << endl;
  cin >> d.TestNumber;
  // echo the test number entered
  cout << endl << d.TestNumber << endl;  

  if( d.TestNumber <= 0) {
    cout << "Invalid Test Number" << endl;
    exit(EXIT_SUCCESS);
  }

  strstream strStream;
  strStream << d.TestNumber;
  strStream >> d.TestNumberStr;

  // setup signals for shutdown
  d.shutdown_flag = FALSE;
  signal(SIGINT, &shutdown);
  signal(SIGTERM, &shutdown);

  // set desired velocity to ...
  // This is the default value, but will be overwritten in accel.cc
  d.targetVelocity = 1.5; //  m/s

  // Initialize all devices before spawning threads
  cout << "Initializing GPS... ";
  InitGPS(d);
  cout << "GPS init done." << endl;
  cout << "Initializing IMU... ";
  InitIMU(d);
  cout << "IMU init done." << endl;
//  cout << FILE_NAME << " - Initializing OBD II... ";
//  InitOBD(d);
//  cout << FILE_NAME << " - OBDII init done." << endl;
  cout << "Initializing Accel... ";
  InitAccel(d);    
  if( !SIM ) { // don't initialize steering if we're simulating 
  //if( 0 ) { // DEBUG cheat and don't init steering
    cout << "Initializing Steer... ";
    init_steer(); // Needed for waypoint following
  }
  else {
    cout << "Faking the steering initialization... ";
    // Fake the steering initialization
  }

  // Initialize Voter/evaluators
  int voterCounter = 0;
  /*  if (voterCounter >= NUM_VOTERS) {
    cout << "Too many Voters?" << endl;
    exit(-1);
  }
  else { */
    d.globalNum = voterCounter; // = 0  
    // }
  voterCounter++;
  /*  if (voterCounter >= NUM_VOTERS) {
    cout << "Too many Voters? " << voterCounter << endl;
    exit(-1);
  }
  else { */
    d.DFEnum =  voterCounter; // = 1
    //  }
  voterCounter++;
  /*  if (voterCounter >= NUM_VOTERS) {
    cout << "Too many Voters? " << voterCounter << endl;
    exit(-1);
  }
  else { */
    d.LadarNum =  voterCounter; // = 2
    //  }
  voterCounter++;
/*
  if (voterCounter >= NUM_VOTERS) {
    cout << "Too many Voters? " << voterCounter << endl;
    exit(-1);
  }
  else {*/ 
  //d.LocalNum =  voterCounter; // = 3
    //  }
  voterCounter++;

  // Let's define num_voters here
  d.numVoters = voterCounter;
  d.voterArray = new Voter[d.numVoters];    // delete down there 

  // this must be done after the voter array is created in main
  cout << "Initializing Ladar ..." << endl;
// TODO: FIX THIS BAD THING
  //if( !SIM ) { // don't init the ladar if we're running in simulation
  if( 1 ) { // cheat and use ladar unit anyway
    while (!InitLadar(d)) {
      cout << "Stuck trying to init ladar" << endl;
      sleep(4);
    } 
  }
  // this must be done after the global voter is added in main
  cout << "Initializing Global..." << endl;
  InitGlobal(d);  // Needed for waypoint following
  // this must be done after the global voter is added in main
  cout << "Initializing DFE..." << endl;
  InitDFE(d);
  cout << "Initializing Local..." << endl;
  //InitLocal(d);                         // Needed for stereo vision
  cout << "Finished Initializing!!!! Hooray!" << endl;

  gettimeofday(&d.TimeZero, NULL);

  // spawn threads
  boost::thread_group thrds;

  StarterCount = 0;
  thrds.create_thread(Starter);  // GrabVstateLoop
  thrds.create_thread(Starter);  // Arbiter & Accel
  thrds.create_thread(Starter);  // GrabOBD
  thrds.create_thread(Starter);  // Global
  thrds.create_thread(Starter);  // DFE
  thrds.create_thread(Starter);  // LADAR
  thrds.create_thread(Starter);  // curses display
  // please use this one even if SIM is not true, we can wrap
  // if(SIM) below and make this thread not do anything
  // doing this here might screw up thread order 
  thrds.create_thread(Starter);  // Simulator
  thrds.create_thread(Starter);  // Local

  thrds.join_all();

  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;

  delete [] d.voterArray;        // Free the Voter array in d

  return 0;

} // end main(int argc, char **argv)


void Starter() {
  // Determine which thread we are 
  int myThread;
  if( TRUE ) {
    boost::recursive_mutex::scoped_lock sl(StarterMutex);
    myThread = StarterCount;
    StarterCount ++;
  }

  switch( myThread ) {
  case 0:
// MOVING TO VSTATE FRAMEWORK!
	  cout << "Starting GrabVstateLoop" << endl;
//	  GrabVstateLoop(d);
    break;
  case 1:
    cout << "Starting AccelLoop" << endl;
    AccelLoop(d);
    break;
  case 2:
  //  cout << "Starting Grab OBD loop" <<endl;
  //  GrabOBDLoop(d);
    break;
  case 3:
    cout << "Starting Global loop" << endl;
    GlobalLoop(d);
    break;
  case 4:
    cout << "Starting DFE loop" << endl;
    DFELoop(d);
    break;
  case 5:
    cout << "Starting LADAR loop" << endl;
    LadarLoop(d);
    break;
  case 6:
    if( DISPLAY ) {
      cout << "Starting sparrow display" << endl;
      WayDisp(d); 
    }
    break;
  case 7:
    if( SIM ) {
      cout << "Starting sim loop" << endl;
      SimLoop(d);
    }
    break;
  case 8:
    cout << "Starting Local loop" << endl;
// DON'T USE THIS YET! IT CAUSES A SEGFAULT AND IS DEFINITELY
// NOT READY FOR PRIME TIME
    cout << "Psyche!" << endl;
//    LocalLoop(d);
    break;
  default:
    cout << "Starter error, default reached" << endl;
  }
}
