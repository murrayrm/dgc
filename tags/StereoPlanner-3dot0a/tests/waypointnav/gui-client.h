#ifndef __GUI_CLIENT_db_H_
#define __GUI_CLIENT_db_H_

#ifdef  __cplusplus
extern "C" {
#endif

#include <stdio.h>

/* Open a logfile named "module_name-pid.log". (pid is the process id number).
   Then fork off a child process which will connect to the
   remote GUI application.  It will copy the log file off to the GUI.

   It will attempt to connect to the remote gui.
   If that fails (or if the connection fails later), it will sleep a few seconds
   and try again.
   
   This behavior is modified by the following environment variables:
   
      GUI_HOST        Tells the remote host (by name or IP) to connect to.
      GUI_PORT_BASE   Tells the remote port number offset.  This defaults to 11000.
                      The various modules have different port numbers relative
		      to this.  For example, the LADAR has port offset 1,
		      so its default port is 11001.
      GUI_LOG_DIRECTORY
                      The directory to write log files in instead of '.'.
      GUI_SUPPRESS_LOGGING
                      If set to 1, the returned file will point to /dev/null instead.
      GUI_SUPPRESS_CONNECTING
                      If set to 1, the logfile will be created, but no
                      remote connections will be done.
      */

FILE *gui_client_create_log_file (const char *module_name);

#ifdef  __cplusplus
}
#endif

#endif
