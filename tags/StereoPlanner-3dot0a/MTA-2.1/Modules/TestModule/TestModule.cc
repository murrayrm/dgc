#include "TestModule.hh"

#include "Kernel.hh"
#include <time.h>
#include <iostream>

#include <unistd.h>
#include <signal.h>
#include <stdio.h>

#include "Misc/Time/Time.hh"

using namespace std;
Timeval dispBefore, dispAfter;

pid_t mainPID;

struct TestStruct {
  int x;
  float y;
  double z;
};





TestModule::TestModule() : DGC_MODULE(MODULES::TestModule, "Test Module", 0) {
}

TestModule::~TestModule() {

}

void TestModule::Init() {

  // do your init in here
  // i.e. open serial ports etc,

  //  mainPID = getpid();

  cout << ModuleName() << ": Init" << endl;

  D.MyCounter = 0;
  dispBefore = TVNow();
  int i;


  // go to active state after initialization
  SetNextState(STATES::ACTIVE);



}



void TestModule::Active() {

  


  while( ContinueInState() ) {  // while stay active

    D.MyCounter ++;

    Mail m = NewOutMessage(MyAddress(), MODULES::EchoServer, TestModuleMessages::PAUSE);

    TestStruct s;
    s.x = rand();
    s.y = 2.13;
    s.z = 0.213;

    m << D.MyCounter << s;

    SendMail(m);

    //    Mail m = NewQueryMessage(MyAddress(), MODULES::Echo, TestModuleMessages::PAUSE);
    //Mail reply = SendQuery(m);


    sleep(1);
  }
} 


void TestModule::Standby() {
  while( ContinueInState() ) {  // while stay active


    Mail m = NewOutMessage(MyAddress(), MODULES::EchoServer, TestModuleMessages::GO);
    SendMail(m);
    usleep(1000000);
    //    pause();
    /*
    dispAfter = TVNow();
    if( (dispAfter - dispBefore) > Timeval( 3, 0)) {
      cout << "D.MyCounter = " << D.MyCounter << endl;
      dispBefore = dispAfter;
    }
    */
    //sleep(1); // dont increment the counter while in standby

    //cout << "Send time = " << D.Before << endl;

    //    D.MyCounter ++;
    //    cout << ModuleName() << ": Standby - _Not_ Incrementing Counter = " << D.MyCounter << endl;
  }
} 


void TestModule::Shutdown() {
  // do whatever closing of files, etc
  cout << ModuleName() << ": Shutdown" << endl;

}

void TestModule::Restart() {
  // you may want to do something better here, but i'll just call shutdown and init
  cout << ModuleName() <<": Restart" << endl;
  Shutdown();
  Init();
}

string TestModule::Status() {
  string x = ModuleName() + ": is in state ";
  switch ( GetCurrentState() ) {
  case STATES::INIT:
    x+= "Init";       break;
  case STATES::ACTIVE:
    x+= "Active";     break;
  case STATES::SHUTDOWN:
    x+= "Shutdown";   break;
  case STATES::STANDBY:
    x+= "Standby";    break;
  case STATES::RESTART:
    x+= "Restart";    break;
  default:
    x+= "Unknown state - (Something is very wrong)";
  }
  return x;
}
 
void TestModule::InMailHandler(Mail& ml) {

  //  kill( mainPID, SIGUSR1);

  TestStruct back;

  int counter;
  ml >> counter >> back;
  cout << counter << ":" << back.x << " " 
       << back.y << " " << back.z << endl;
  
  switch(ml.MsgType()) {  // for mail functions check out Misc/Mail/Mail.hh

  case TestModuleMessages::REINIT:
    //    SetNextState(STATES::RESTART);
    break;

  case TestModuleMessages::SHUTDOWN:
    // force the Kernel to shutdown
    ForceShutdown();
    break;

  case TestModuleMessages::PAUSE:
    //    SetNextState(STATES::STANDBY);
    break;

  case TestModuleMessages::GO:
    //    SetNextState(STATES::ACTIVE);
    break;

  case TestModuleMessages::SETCOUNTER:
    // check to see that an int was attached to the message
    if( ml.Size() >= sizeof(int)) {
      int x;
      ml >> x;
      D.MyCounter = x;
    }
    break;

  default:
    cout << ModuleName() << ": Unknown InMessage" << endl;
  }
}


Mail TestModule::QueryMailHandler(Mail& msg) {
  // figure out what we are responding to

  Mail reply = msg; // A hack !!! dont do this :)
  int x;

  switch(msg.MsgType()) {  

  case TestModuleMessages::GETCOUNTER:
    reply = ReplyToQuery(msg);
    reply << D.MyCounter;
    break;

  case TestModuleMessages::GETSTATE:
    reply = ReplyToQuery(msg);
    x = GetCurrentState();
    reply << x;
    break;

  default:
    // send back a "bad Message"
    reply = Mail( msg.From(), msg.To(), 0, MailFlags::BadMessage);
  }

  return reply;
}


