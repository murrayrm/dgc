

#include <time.h>
#include <sys/time.h>
#include "Time.hh"
#include <unistd.h>



double timeval_subtract(const timeval & t1, const timeval & t2) {

  double dt1 = ((double) t1.tv_sec) + (((double) t1.tv_usec) / 1000000);
  double dt2 = ((double) t2.tv_sec) + (((double) t2.tv_usec) / 1000000);

  return dt1 - dt2;
}


void usleep_for(int useconds) {
  
  double seconds = ((double) useconds) / 1000000.0; 
  double timeleft = seconds;
  timeval before, after;
  gettimeofday(&before, NULL);

  do {
    usleep((int) (timeleft * 1000000));
    gettimeofday(&after, NULL);
    timeleft = seconds - timeval_subtract(after,before);

  } while(timeleft > 0.0);

}



void sleep_for(int seconds) {
  
  double timeleft = (double) seconds;
  timeval before, after;
  gettimeofday(&before, NULL);

  do {
    usleep((int) (timeleft * 1000000));
    gettimeofday(&after, NULL);
    timeleft = ((double) seconds) - timeval_subtract(after,before);

  } while(timeleft > 0.0);
}
