#ifndef Timeval_HH
#define Timeval_HH

#include <time.h>
#include <sys/time.h>
#include <iostream>

using namespace std;

class Timeval {
public:
  Timeval();
  Timeval(long seconds, long microseconds);
  Timeval(const timeval & tv);
  Timeval(const Timeval & tv);

  Timeval operator - (const Timeval &rhs);

  int operator >  (const Timeval &rhs) const;
  int operator <= (const Timeval &rhs) const;

  int operator <  (const Timeval &rhs) const;
  int operator >= (const Timeval &rhs) const;

  int operator == (const Timeval &rhs) const;

  long sec();
  long usec();
  double dbl();

  timeval TV();


  int OK();

private:
  timeval myTV;

};

Timeval TVNow();
Timeval BadTimeval();
void Sleep(Timeval t);


ostream& operator << (ostream& os, Timeval tv);

#endif
