#include "ThreadsForFunctions.hh"

// ------------- Implementation --------------------------
// ------------- Implementation --------------------------
// ------------- Implementation --------------------------

void RunFunctionInNewThreadLoader( void* arg ) {

  void (*f) (void*);   // function  pointer
  void *f_arg;      // arguments pointer

  memcpy ( &f, arg,                     sizeof(f));
  memcpy ( &f_arg, (char*)arg + sizeof(f), sizeof(f_arg));

  f(f_arg);
  delete (char*) arg;
}


void RunFunctionInNewThread(void (*f)(void*), void *arg ) {
  
  ThreadReconstructPointers toEnqueue;
  toEnqueue.p_fun = &(RunFunctionInNewThreadLoader);
  toEnqueue.p_arg = (void*) new char[ sizeof(f) + sizeof(arg) ]; 

  memcpy ( toEnqueue.p_arg,                      &f, sizeof(f));
  memcpy ( (char*)toEnqueue.p_arg + sizeof(f), &arg, sizeof(arg));

  EnqueueFunctionToExecute(toEnqueue);

}

