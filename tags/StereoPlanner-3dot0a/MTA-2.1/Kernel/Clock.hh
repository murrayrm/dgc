#ifndef _CLOCK_HH_
#define _CLOCK_HH_

#include "Kernel.hh"


class Clock : public DGC_MODULE {
public:
  Clock(): DGC_MODULE( MODULES::Clock, "Clock Server", 0) {}
  void Active();

  void InMailHandler(Mail &ml);
  Mail QueryMailHandler(Mail &ml);


  static Timeval GetTime(MailAddress& ma);
  static void Reset(MailAddress& ma);


};


namespace ClockMessages {

  enum {
    GetTime,        // Returns a timeval

    Reset,

    NumMessages     // Place-holder
  };
};



#endif
