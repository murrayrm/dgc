#ifdef __cplusplus
extern "C" {
#endif

#include "../parallel/parallel.h"

#ifndef TRANSMISSION_H
#define TRANSMISSION_H

#define TRANSPPORT PP_TRANS

/* shift gear definition for trans_shift_gear() */
#define TRANS_NEUTRAL	0
#define TRANS_FREE	1
#define TRANS_FIRST	2
#define TRANS_SECOND	3
#define TRANS_THIRD	4
#define TRANS_REVERSE	5
#define TRANS_HUMAN	6  /* driven by humans, look at switch to determine gear */

  /* old system */
  /*#define TRANS_DB0	PP_PIN02
#define TRANS_DB1	PP_PIN03
#define TRANS_DB2	PP_PIN04
  */
/* new system */
#define TRANS_DB0	PP_PIN06
#define TRANS_DB1	PP_PIN05    
#define TRANS_DB2	PP_PIN04    


/* Define the pins set high and low for each gear*/

#define TRANS_NEUTRAL_PIN_HIGH	0
#define TRANS_NEUTRAL_PIN_LOW	TRANS_DB0 | TRANS_DB1 | TRANS_DB2

#define TRANS_FREE_PIN_HIGH	TRANS_DB0
#define TRANS_FREE_PIN_LOW	TRANS_DB1 | TRANS_DB2

#define TRANS_FIRST_PIN_HIGH	TRANS_DB1 
#define TRANS_FIRST_PIN_LOW	TRANS_DB0 | TRANS_DB2

#define TRANS_SECOND_PIN_HIGH	TRANS_DB0 | TRANS_DB1 
#define TRANS_SECOND_PIN_LOW	TRANS_DB2

#define TRANS_THIRD_PIN_HIGH	TRANS_DB2
#define TRANS_THIRD_PIN_LOW	TRANS_DB0 | TRANS_DB1

#define TRANS_REVERSE_PIN_HIGH	TRANS_DB0 | TRANS_DB2
#define TRANS_REVERSE_PIN_LOW	TRANS_DB1 

#define TRANS_HUMAN_PIN_HIGH	TRANS_DB0 | TRANS_DB1 | TRANS_DB2
#define TRANS_HUMAN_PIN_LOW	0 

void trans_shift_gear(int gear);

#endif

#ifdef __cplusplus
}
#endif

