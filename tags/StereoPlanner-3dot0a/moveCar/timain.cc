// Main loop for transmission and ignition
// g++ timain.cc ignition.c transmission.c ../parallel/parallel.c ../OBDII/OBDII.cc ../serial/serial.c -o ti

#include <string>
#include <iostream>
#include "transmission.h"
#include "ignition.h"
#include "../OBDII/OBDII.h"

using namespace std;

int main () {

  cout << "Welcome to transmission/ignition system." << endl;

  if (initIgTrans() != 0) {
    cout << "Error initializing trans/ig port." << endl;
    return -1;
  }
  OBD_Init(OBDSerialPort);

  char *act = "c";
  while (strcmp(act , "q") != 0) {
    cout << "Enter action (h for help): " << endl;
    cin >> act;

    if (strcmp(act, "h") == 0) {
      cout << " start : start car engine" << endl;
      cout << " con   : car on (Run)" << endl;
      cout << " coff  : car off" << endl;
      cout << " n     : neutral gear" << endl;
      cout << " f     : free float (trans)" << endl;
      cout << " 1     : first gear " << endl;
      cout << " 2     : second gear " << endl;
      cout << " 3     : third gear " << endl;
      cout << " r     : reverse gear " << endl;
      cout << " human : human control (trans)" << endl;
      //cout << " acon  : AC on" << endl;
      //cout << " acoff : AC off" << endl;
      cout << " q     : quit" << endl;
    }
    else if (strcmp(act, "start") == 0) {
      startCar();
    }
    else if (strcmp(act, "con") == 0) {
      ignition(PULL_RUN_HIGH);
    }
    else if (strcmp(act, "coff") == 0) {
      ignition(PULL_RUN_LOW);
    }
    else if (strcmp(act, "n") == 0) {
      trans_shift_gear(TRANS_NEUTRAL);
    }
    else if (strcmp(act, "f") == 0) {
      trans_shift_gear(TRANS_FREE);
    }
    else if (strcmp(act, "1") == 0) {
      trans_shift_gear(TRANS_FIRST);
    }
    else if (strcmp(act, "2") == 0) {
      trans_shift_gear(TRANS_SECOND);
    }
    else if (strcmp(act, "3") == 0) {
      trans_shift_gear(TRANS_THIRD);
    }
    else if (strcmp(act, "r") == 0) {
      trans_shift_gear(TRANS_REVERSE);
    }
    else if (strcmp(act, "human") == 0) {
      trans_shift_gear(TRANS_HUMAN);
      }
    /*
    else if (strcmp(act, "acon") == 0) {
    }
    else if (strcmp(act, "acoff") == 0) {
    }
    */
  }
  return 0;
}
