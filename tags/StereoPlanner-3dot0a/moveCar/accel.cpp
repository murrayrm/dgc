#include "accel.h"

//A global variable for saving the last position of the gas actuator (in digipot units)
int last_pos = MAX_POT_VAL;
static int accelpport = 2; //PP_GASBR;

//A function for initializing the parallel port controlling gas, transmission, and ignition
//as well as for initializing those actuators
//~~NEED TO CHK THE PARPORT ALREADY_OPEN STUFF, OTHERWISE MAY NOT INIT PROPERLY
int init_accel() {
  //~~if(pp_init(PP_GASBR, PP_DIR_OUT) >= 0) {  

  if(pp_init(accelpport, PP_DIR_IN) >= 0) {
    calib_accel();
    
    printf("\nAccel actuator init-ed on port %d\n", accelpport);

    return 1;
  } else {
    printf("\nError opening parallel port %d\n", accelpport);
    return -1;
  }
}

//A function for calibrating the gas actuator by moving it all the way out (theoretically, this is the engine idling position)
int calib_accel() {
  //Set last_pos so that the actuator moves double what it should
  last_pos=255;
  //Set the actuator to the fully out position
  set_accel_abs_pos(0.0);
}


//Returns the gas actuator's position on the 0-1 scale
float get_accel_pos() {
  return (((float) (last_pos - MIN_POT_VAL)) / (MAX_POT_VAL - MIN_POT_VAL));
}

//Sets the gas actuator's position based on a float, which is on the 0-1 scale
//0 sets the actuator to let the engine idle
//1 is full throttle
int set_accel_abs_pos(float position) {
  //  printf("\nSetting position to %f\n", position);
	if(position == 0) {
	  //If the position is 0, decrement for twice the normal amount just to be sure
	  change_digipot_by(-255);
	} else if(position == 1) {
	  //If the position is 1, increment for twice the normal amount just to be sure
	  change_digipot_by(255);
	} else {
	  //Otherwise, just convert from the 0-1 scale to the 0-127 scale, and call the function to change the digipot
	  int digipot_pos;
	  digipot_pos = ((int) ((MAX_POT_VAL - MIN_POT_VAL) * position)) + MIN_POT_VAL;
	  change_digipot_by(digipot_pos-last_pos);
	}
	
  return 0;
}

//Changes the digipot by pos_change, on the 0-127 scale
int change_digipot_by(int pos_change) {
  pp_set_bits(accelpport, 0, ACCEL_CS);
  //Update last_pos, and make sure it stays within range
  last_pos+=pos_change;
  last_pos=in_range(last_pos, MIN_POT_VAL, MAX_POT_VAL);


  //If the desired digipot value is higher...
  if(pos_change > 0) {
    //Set UD high to indicate incrementing
    //~~  pp_set_bits(PP_GASBR, ACCEL_UD, 0);
    pp_set_bits(accelpport, ACCEL_UD, 0);
    //Cycle the clock until the desired pos_change is reached
    while(pos_change > 0) {
      //~~  pp_set_bits(PP_GASBR, ACCEL_CLK, 0);
      //~~  pp_set_bits(PP_GASBR, 0, ACCEL_CLK);
      pp_set_bits(accelpport, ACCEL_CLK, 0);
      pp_set_bits(accelpport, 0, ACCEL_CLK);
      pos_change--;
    }
    //Else if the desired digipot value is lower...
  } else if(pos_change < 0) {
    //Set UD low to indicate incrementing
    //~~    pp_set_bits(PP_GASBR, 0, ACCEL_UD);
    pp_set_bits(accelpport, 0, ACCEL_UD);
    //Cycle the clock until the desired pos_change is reached
    while(pos_change < 0) {
      //~~  pp_set_bits(PP_GASBR, ACCEL_CLK, 0);
      //~~  pp_set_bits(PP_GASBR, 0, ACCEL_CLK);
      pp_set_bits(accelpport, ACCEL_CLK, 0);
      pp_set_bits(accelpport, 0, ACCEL_CLK);
      pos_change++;
    }
  }
  pp_set_bits(accelpport, ACCEL_CS, 0);
  return 0;
}

//Function to make sure min_val<=val<=max_val, and if that's not true, set val properly
int in_range(int val, int min_val, int max_val) {
	if(val<min_val) return min_val;
	if(val>max_val) return max_val;
	return val;
}


