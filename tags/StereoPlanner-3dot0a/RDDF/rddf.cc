/**********************************************************************
 **********************************************************************
 * +FILE:        rddf.cc
 *    
 * +DESCRIPTION: rddf class methods
 *
 * 
 **********************************************************************
 *********************************************************************/

#include "rddf.hh"

// Constructor
RDDF::RDDF(){
  loadFile(RDDF_FILE);
}
RDDF::RDDF(char *pFileName){
  loadFile(pFileName);
}
// Destructor
RDDF::~RDDF(){
}

RDDF &RDDF::operator=(const RDDF &other)
{
  numTargetPoints = other.getNumTargetPoints();
  targetPoints = other.getTargetPoints();
  return *this;
}

/****************************************************************************************
 * +DESCRIPTION: return the UTM coordinates of  the waypoint we are looking for
 *    
 * +PARAMETERS 
 *
 *   - fileName      : the rddf file
 *
 * +RETURN: 
 *   - int : 0 if OK
 ***************************************************************************************/
int RDDF::loadFile(char* fileName){
  int count;
  ifstream file;
  string line;
  RDDFData eachWaypoint;

  file.open(fileName,ios::in);
  if(!file){
    cout << "UNABLE TO OPEN THE FILE /team/RDDF/rddf.dat" << endl;
    cout << "PLEASE ADD IN YOUR MAKEFILE:" << endl;
    cout << "( ln -fs $(DGC)/RDDF/rddf.dat $(DGC)/YOUR_DIRECTORY/rddf.dat)" << endl;
    exit(1);
  }
  count = 0;
  while(!file.eof()){
    GisCoordLatLon latlon;
    GisCoordUTM utm;

    // Number
    getline(file,line,RDDF_DELIMITER);
    eachWaypoint.number = atoi(line.c_str());
    // Latitude
    getline(file,line,RDDF_DELIMITER);
    latlon.latitude = atof(line.c_str());
    
    // Longitude
    getline(file,line,RDDF_DELIMITER);
    latlon.longitude = atof(line.c_str());

    // Converting to UTM
    if(!gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL)){ 
      cerr << "Error converting coordinate " << eachWaypoint.number << " to utm\n";
      exit(1); 
    }
    eachWaypoint.Northing = utm.n;
    eachWaypoint.Easting = utm.e;

    // Boundary offset and radius
    getline(file,line,RDDF_DELIMITER);
    eachWaypoint.offset = atof(line.c_str())*meters_per_foot;
    eachWaypoint.radius = eachWaypoint.offset;
    // Speed limit
    getline(file,line,RDDF_DELIMITER);
    eachWaypoint.maxSpeed = atof(line.c_str())*mps_per_mph;
    // Hour
    getline(file,line,RDDF_DELIMITER);
    if(line.find("#") != string::npos){
      eachWaypoint.hour = 0;
    }
    else{
      eachWaypoint.hour = atoi(line.c_str());
    }
    // Minute
    getline(file,line,RDDF_DELIMITER);
    if(line.find("#") != string::npos){
      eachWaypoint.min = 0;
    }
    else{
      eachWaypoint.min = atoi(line.c_str());
    }
    // Second
    file >> line;
    if(line.find("#") != string::npos){
      eachWaypoint.sec = 0;
    }
    else{
      eachWaypoint.sec = atoi(line.c_str());
    }

    if(count == eachWaypoint.number){
      targetPoints.push_back(eachWaypoint);
      count++;
    }
  }
  numTargetPoints = count;
  // debug
  //cout << "RDDF: " << numTargetPoints << " target points read !!!" << endl;
  return(0);
}

// Evaluate the distance betwwen a point and a plane in R^3
/* double distancePoint2Line(NorthEastDown onLine_a, NorthEastDown onLine_b, NorthEastDown outLine);
 * +DESCRIPTION: This method receive the two points that define the line
 *    and a point outside it and returns the minimum distance between them
 * +PARAMETERS
 *   - a, b: points on the line that defines it
 *   - Po: point outside the line
 * +RETURN: the distance of the projection
 */
pair<bool,double> RDDF::distancePoint2Line(NorthEastDown a, NorthEastDown b, 
					     NorthEastDown Po)
{
  NorthEastDown v;           // vector on the line direction
  NorthEastDown n;           // vector normal to the line through Po
  NorthEastDown Pl;          // point on the line resulted from the projection
  double to;       // line parametrization in terms of to
  double lineLen; // the compriment on the line
  double dist_a2Pl, dist_b2Pl;
  double distance;
  bool doesItCross=false;
  pair <bool,double> ret;

  v = a - b;
  lineLen = sqrt((b.Northing-a.Northing)*(b.Northing-a.Northing) + (b.Easting-a.Easting)*(b.Easting-a.Easting) + (b.Downing-a.Downing)*(b.Downing-a.Downing) );
  v.norm();
  to = -( v.Northing*(a.Northing - Po.Northing) + v.Easting*(a.Easting - Po.Easting) + v.Downing*(a.Downing - Po.Downing) );
  Pl = v*to + a;
  dist_a2Pl = sqrt((a.Northing-Pl.Northing)*(a.Northing-Pl.Northing) + (a.Easting-Pl.Easting)*(a.Easting-Pl.Easting) + (a.Downing-Pl.Downing)*(a.Downing-Pl.Downing) );
  dist_b2Pl = sqrt((b.Northing-Pl.Northing)*(b.Northing-Pl.Northing) + (b.Easting-Pl.Easting)*(b.Easting-Pl.Easting) + (b.Downing-Pl.Downing)*(b.Downing-Pl.Downing) );
  if( (dist_a2Pl <= lineLen) && (dist_b2Pl <= lineLen) ){
    doesItCross = true;    
  }
  n = Pl - Po;
  distance = sqrt(n*n);
  ret.first  = doesItCross;
  ret.second = distance;
  return(ret);
}

/*****************************************************************************/
/***************************************************************************** 
 * + DESCRIPTION: 
 *     Given a waypoint description and position of the vehicle,
 *     determine and return the index of the current waypoint.
 * + ARGUMENTS: 
 *     - RDDFVector: a list of waypoints 
 *     - Northing  : Northing of the vehicle 
 *     - Easting   : Easting of the vehicle
 * + RETURN:
 *     - int: index of the current waypoint 
 *            (the one that we should be going to)
 *             
 *****************************************************************************/
/*****************************************************************************/
int RDDF::getCurrentWaypointNumber( double Northing, double Easting )
{
  // set the current position vector
  Vector curPos( Northing, Easting );
  // declare waypoint vectors
  Vector curWay, nextWay;

  // minimum found distance to waypoint
  double minDist = 123456789;
  int minDistWay = 0;
  
  // cycle through all of the waypoints
  for( int i = 0; i < numTargetPoints-1; i++ ) 
  {
    curWay.N = targetPoints[i].Northing;
    curWay.E = targetPoints[i].Easting;
    nextWay.N = targetPoints[i+1].Northing;
    nextWay.E = targetPoints[i+1].Easting;
 
    // check to see if we are within the corridor segment
    // if we are, just return with the index of the segment
    if( withinCorridor( curPos, curWay, nextWay, targetPoints[i].radius ) ) 
    {
     printf("------ I am within corridor segment %d -------\n", i ); 
      return i;
    }

    // if this waypoint is closer than any other we've found,
    // then remember so 
    if( distVector(curPos, curWay) < minDist ) 
    {
      minDist = distVector(curPos, curWay); 
      minDistWay = i;
    }
  } 
  printf("------ I am NOT within the corridor -------\n" ); 
  
  // if we get to this point in the function, it means that we are
  // **not** within the corridor.  In this case, return the index of
  // the waypoint that is closest to us
  return minDistWay;
}

/*****************************************************************************/
/***************************************************************************** 
 * + DESCRIPTION: 
 *     Given a waypoint description and position of the vehicle,
 *     determine and return the index of the current waypoint.
 * + ARGUMENTS: 
 *     - Northing     : Northing of the FRONT of vehicle 
 *     - Easting      : Easting of the FRONT of vehicle
 *     - nextWaypoint : he waypoint we were looking for
 * + RETURN:
 *     - int: index of the current waypoint 
 *            (the one that we should be going to)
 *             
 *****************************************************************************/
/*****************************************************************************/
int RDDF::getNextWaypointNumber(double Northing, double Easting, int nextWaypoint){
  int newNextWaypoint,indWaypoint, lastWaypoint;
  pair<double,double> NorthEast;
  pair<bool,double> pointLine, nextPointLine;
  bool crossTrackLine,crossNextTrackLine, changeWaypoint;
  NorthEastDown a,b,point;
  double distLastWaypoint, distNextWaypoint, distNextTrackLine;
  double lastWaypointRadius=0;
  double trackLen, displacement;

  // FRONT VEICLE POSITIONTO CHECK
  point.Northing = Northing;
  point.Easting = Easting;
  point.Downing = 0;
  // Next waypoint
  b.Northing = targetPoints[nextWaypoint].Northing;
  b.Easting = targetPoints[nextWaypoint].Easting;
  b.Downing = 0;

  // CHECKNG IF IT COSSES THE TRACK LINE WE ARE NOW
  // IF NEXT WAYPOINT = 0,NO PRESENT TRACK LINE
  // IF HIGHER OR EQUAL THAN THE TOTAL, WE COMPLETED THE COURSE
  crossTrackLine = false;
  if( (nextWaypoint) && (nextWaypoint < numTargetPoints-1) ){
    // Last waypoint
    lastWaypoint = nextWaypoint-1;
    a.Northing = targetPoints[lastWaypoint].Northing;
    a.Easting = targetPoints[lastWaypoint].Easting;
    a.Downing = 0;
    // Distance between point and the track line
    pointLine = distancePoint2Line(a,b,point);
    // Check if it is inside the last waypoint
    distLastWaypoint = sqrt( (point.Northing - a.Northing)*(point.Northing - a.Northing) + (point.Easting - a.Easting)*(point.Easting - a.Easting) );
    lastWaypointRadius=0;
    if( lastWaypoint ) { // THE BIGGESTOF THE TWO
      lastWaypointRadius = targetPoints[lastWaypoint-1].radius;
    }

    if( (pointLine.first) || (distLastWaypoint  < fmax(targetPoints[lastWaypoint].radius, lastWaypointRadius)) ){
      crossTrackLine = true;
    }
  }

  // IF WE HIT HE NEX WAYPOINT O THE NEXT CORIDOR, INCRREMENT IT
  distNextWaypoint = sqrt( (point.Northing - b.Northing)*(point.Northing - b.Northing) + (point.Easting - b.Easting)*(point.Easting - b.Easting) );
  lastWaypointRadius=0;
  if( nextWaypoint ) { // THE BIGGESTOF THE TWO
    lastWaypointRadius = targetPoints[nextWaypoint-1].radius;
  }
  if(distNextWaypoint  < fmax(targetPoints[nextWaypoint].radius, lastWaypointRadius)){
    changeWaypoint = true;
  }

  // CHECKNG IF IT CROSSES THE NEXT TRACK LINE
  crossNextTrackLine = false;
  if(nextWaypoint < (numTargetPoints-1)){
    // Next Waypoint
    a.Northing = targetPoints[nextWaypoint].Northing;
    a.Easting = targetPoints[nextWaypoint].Easting;
    a.Downing = 0;
    // Following waypoint
    b.Northing = targetPoints[nextWaypoint+1].Northing;
    b.Easting = targetPoints[nextWaypoint+1].Easting;
    b.Downing = 0;
    // Distance between point and lines
    nextPointLine = distancePoint2Line(a,b,point);
    crossNextTrackLine = nextPointLine.first;
    distNextTrackLine = nextPointLine.second;
  }
  if( (crossNextTrackLine) && (distNextTrackLine < targetPoints[nextWaypoint].radius) ){
    changeWaypoint = true;
  }
  newNextWaypoint = nextWaypoint;
  if(changeWaypoint){
    newNextWaypoint =  nextWaypoint+1;
  }

  // CHECKNG IF IT CROSSES NEXT TRACK LINES
  // IF HIGHER OR EQUAL THAN THE TOTAL MINUS 1, WE COMPLETED THE COURSE
  crossNextTrackLine = false;
  if( (!crossTrackLine) && (!changeWaypoint) ){
    indWaypoint = nextWaypoint;
    displacement=0;
    while( ((indWaypoint < (numTargetPoints-1)) && (displacement < MAX_STATE_DISPLACEMENT)) ){
      // Next Waypoint
      a.Northing = targetPoints[indWaypoint].Northing;
      a.Easting = targetPoints[indWaypoint].Easting;
      a.Downing = 0;
      // Following waypoint
      b.Northing = targetPoints[indWaypoint+1].Northing;
      b.Easting = targetPoints[indWaypoint+1].Easting;
      b.Downing = 0;

      trackLen = sqrt( (a.Northing - b.Northing)*(a.Northing - b.Northing) + (a.Easting - b.Easting)*(a.Easting - b.Easting) );
      nextPointLine = distancePoint2Line(a,b,point);
      crossNextTrackLine = nextPointLine.first;
      indWaypoint++;
      displacement += trackLen;
      if(crossNextTrackLine){
        newNextWaypoint = indWaypoint;
      }
    }
  }
  return(newNextWaypoint);
}
/****************************************************************************************
 * +DESCRIPTION: saves the file in the bob format for matlab plots
 *               PROBLEM WITH PRECISION .....
 * +PARAMETERS 
 *
 *   - fileName : file name
 *
 * +RETURN: 
 *   - int : 0 if OK
 ***************************************************************************************/
int RDDF::createBobFile(char* fileName){
  ofstream file;
  int ind=0;
  file.open(fileName);
  if(!file){
    cout << "UNABLE TO OPEN THE FILE" << endl;
    return(BAD_BOB_FILE);
  }
  file << "# This is a Bob format waypoint specification file.\n";
  file << "# The column format here is: \n";
  file << "# 1. character [ignored]\n";
  file << "# 2. Easting[m]\n";
  file << "# 3. Northing[m] \n";
  file << "# 4. lateral_offset[m] \n";
  file << "# 5. speed_limit [m/s] \n";
  file << "# NOTE THAT THESE ARE IN SI AS OPPOSED TO THE RDDF SPEC.\n#\n";
  for(ind=0;ind < numTargetPoints;ind++){

    file << "N   " << targetPoints[ind].Northing << " " << targetPoints[ind].Easting 
         << " " << targetPoints[ind].offset << " " << targetPoints[ind].maxSpeed << "\n"; 
  }
  return(0);
}
