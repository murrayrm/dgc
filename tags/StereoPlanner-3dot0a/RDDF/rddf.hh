/**********************************************************************
 **********************************************************************
 * +FILE:        rddf.hh 
 *    
 * +DESCRIPTION: class to read the rddf file and return the waypoints
 *
 * +AUTHOR     : Thyago Consort 
 **********************************************************************
 *********************************************************************/

#ifndef __RDDF_HH__
#define __RDDF_HH__

#include <stdio.h>
#include <string.h>
#include <iostream.h>
#include <fstream.h>
#include <math.h>
#include <vector>
#include "Constants.h"
#include "ggis.h"
#include "global_functions.hh" // for withinWaypoint, ...

#define RDDF_DELIMITER ','
#define BAD_BOB_FILE -1

#define MAX_STATE_DISPLACEMENT 10 // in meters

using namespace std;

// Used a lot on the new obstacle avoidance
typedef struct NorthEastDown {
  int row,col;
  double Northing;
  double Easting;
  double Downing;

  // Constructor
  NorthEastDown(double pNorth=0.0,double pEast=0.0, double pDown=0.0,
		int bottomLeftRow = 0, int bottomLeftCol = 0,
		double rowRes = 1.0, double colRes = 1.0, int numRows = 1,
		int numCols = 1){ 
    Northing = pNorth;
    Easting = pEast;
    Downing = pDown;
    fix_RC(bottomLeftRow, bottomLeftCol, rowRes, colRes, numRows, numCols);
  }

  // Copy Constructor
  NorthEastDown(const NorthEastDown &other) { 
    Northing = other.Northing;
    row = other.row; 
    Easting = other.Easting;
    col = other.col;
    Downing = other.Downing;
  }

  // Assignment Operator
  NorthEastDown &operator=(const NorthEastDown &other) { 
    if (this != &other) {
      Northing = other.Northing;
      Easting = other.Easting;
      Downing = other.Downing;
      row = other.row;
      col = other.col;
    }
    return *this;
  }

  // + and - operators
  NorthEastDown operator+ (NorthEastDown param) {
    NorthEastDown temp;
    temp.Northing = Northing + param.Northing;
    temp.Easting = Easting + param.Easting;
    temp.Downing = Downing + param.Downing;
    return (temp);
  }

  NorthEastDown operator- (NorthEastDown param) {
    NorthEastDown temp;
    temp.Northing = Northing - param.Northing;
    temp.Easting = Easting - param.Easting;
    temp.Downing = Downing - param.Downing;
    return (temp);
  }

  // Produtc by a constant
  NorthEastDown operator* (double param){
    NorthEastDown ret;
    ret.Northing = Northing*param;
    ret.Easting = Easting*param;
    ret.Downing = Downing*param;
    return (ret);
  }

  // Internal product operator
  double operator* (NorthEastDown param){
    double ret;
    ret = (Northing * param.Northing) + (Easting * param.Easting) + (Downing * param.Downing);
    return (ret);
  }

  // Vectorial product operator
  NorthEastDown operator^ (NorthEastDown param){
    NorthEastDown temp;
    temp.Northing = (Easting * param.Downing) - (Downing * param.Easting);
    temp.Easting = (Downing * param.Easting) - (Northing * param.Downing);
    temp.Downing = (Easting * param.Easting) - (Easting * param.Easting);
    return (temp);
  }

  // Norma
  void norm(){
    double modulus = 0;
    modulus = sqrt(Northing*Northing + Easting*Easting + Downing*Downing);
    if(modulus){
      Northing /= modulus;
      Easting /= modulus;
      Downing /= modulus;
    }
  }

  void fix_RC(int bottomLeftRow, int bottomLeftCol, double rowRes, double colRes, int numRows, int numCols){
    row = (bottomLeftRow + (int)floor(Northing/rowRes))%numRows;
    col = (bottomLeftCol + (int)floor(Easting/colRes))%numCols;
  }

// Display
  void display(){
    printf("(Northing,Easting,Downin) = (%10.2f,%10.2f,%10.2f)\n",Northing,Easting,Downing);
  }
  
  // Destructor
  ~NorthEastDown() {}
};

// waypoints
typedef struct RDDFData{
  int number;       // number of the wayoint

  double Northing;
  double Easting;

  double maxSpeed;  // speed limit m/s
  double offset;    // boundary offset of the corridor to the next waypoint
  double radius;    // waypoint radius

  double hour;      // phase line - hour
  double min;       // phase line - min
  double sec;       // phase line - sec

  void display(){
    printf("N=%d North=%10.3f East=%10.3f VEL=%.2f Off= %.2f R=%.2f hour=%d min=%d sec=%d\n",
		    number,Northing,Easting,maxSpeed,offset,radius,hour,min,sec);
  }
};

typedef std::vector<RDDFData> RDDFVector;

class RDDF {
private:

  int numTargetPoints;
  RDDFVector targetPoints;

public:

  // Constructors
  RDDF();
  RDDF(char*pFileName);
  RDDF(const RDDF &other) {numTargetPoints = other.getNumTargetPoints(); targetPoints = other.getTargetPoints(); }
  // Destructor
  ~RDDF();

  RDDF &operator=(const RDDF &other);
  // Accessors
  int getNumTargetPoints() const {return numTargetPoints; }
  RDDFVector getTargetPoints() const {return targetPoints; }

  // Method to automatically find the current waypoint 
  // (the one that we are going to)
  int getCurrentWaypointNumber( double Northing, double Easting );

  // Evaluate a simple distance
  /* double distancePoint2Line(NorthEastDown,NorthEastDown,NorthEastDown);
   *  This method evaluates the distance between a point and a line and verify if the 
   *  projection of the off line point crosses the line
   */
  pair<bool,double> distancePoint2Line(NorthEastDown,NorthEastDown,NorthEastDown);

  // Method to automatically find the current waypoint 
  // (the one that we are going to)
  int getNextWaypointNumber( double Northing, double Easting, int nextWaypoint );
  
  // Save the file at the bob format
  int createBobFile(char*);

  // Load data from file
  int loadFile(char*);
};

#endif //__RDDF_HH__
