/*----------------------------------------------------------------------------
--                                                                          --
--                        U N C L A S S I F I E D                           --
--                                                                          --
--                  L I T T O N   P R O P R I E T A R Y                     --
--                                                                          --
-- Copyright (c) 1999, Guidance & Control Systems Division,                 --
--                     Litton Systems, Inc.                                 --
--                                                                          --
------------------------------------------------------------------------------
--                                                                          --
--                       RESTRICTED RIGHTS LEGEND                           --
--                                                                          --
-- Use, duplication or disclosure is subject to restrictions stated in      --
-- Contract No. (see Software Version Description Document)                 --
-- with Litton Systems, Inc., Guidance and Control Systems Division.        --
--                                                                          --
----------------------------------------------------------------------------*/
/****************************************************************************

 [Name]    ModeCtl

 [Purpose] Mode Control.

*****************************************************************************/
/****************************************************************************
                        [REVISION HISTORY]

  Date            Engineer         Description
==============================================================================
07/28/99          T. Varty         creation
08/11/99          D. Wiener        changes per Code Review memo
02/08/00          T. Varty         add modeCtlRestart
09/18/00          T. Varty         add modeCtlAcceptWowPb
09/27/00          T. Varty         update pos and hdg source moding
10/09/00          T. Varty         added vel source moding
10/26/00          T. Varty         added modeCtlAcceptGpsLvrArms
*****************************************************************************/
#ifndef ModeCtlH
#define ModeCtlH

#ifdef __BORLANDC__
#pragma option -b
#endif

/****************************************************************************
 * Mode Commands
 ****************************************************************************/

typedef enum {
  MC_INT, MC_LVL, MC_GRA, MC_GCA, MC_TRN,
  MC_FRE, MC_GPS, MC_BIT, MC_AUT, NUM_CMD
} ModeCmds;

/****************************************************************************
 * Operating Modes
 ****************************************************************************/

typedef enum {
  OM_INT, OM_LVL, OM_GRA, OM_GCA, OM_TRN, OM_FRE, OM_GPS, OM_BIT, NUM_MODE
} OpModes;

/****************************************************************************
 * INU Modes
 ****************************************************************************/

typedef enum {
  LEVELING, ALIGNMENT, NAVIGATION
} InuModes;

/****************************************************************************
 * Align Modes
 ****************************************************************************/

typedef enum {
  COARSE_GROUND, FINE_GROUND, INTER_COARSE_GROUND, INTER_FINE_GROUND
} AlignModes;

/****************************************************************************
 * Initial Position Definition
 ****************************************************************************/

typedef struct {
  double          lat;
  double          lon;
  double          alt;
  double          accuracy;
  int             latLonValid;
  int             altValid;
} TInitialPos;

/****************************************************************************
 * Initial Heading Definition
 ****************************************************************************/

typedef struct {
  double          hdg;
  double          accuracy;
  int             valid;
} TInitialHdg;

/****************************************************************************
 * Initial Velocity Definition
 ****************************************************************************/

typedef struct {
  double          vn;
  double          ve;
  double          vz;
  double          accuracy;
  int             valid;
} TInitialVel;

/****************************************************************************
 * Position Source Definitions
 ****************************************************************************/

#define PS_INPUT_MSG    0               // pos source is external message
#define PS_GPS          1               // pos source is GPS data
#define PS_STORED       2               // pos source is previously-stored data
#define PS_NOT_AVAIL    3               // pos source data is unavailable

/****************************************************************************
 * Heading Source Definitions
 ****************************************************************************/

#define HS_QUAT         0               // hdg source is quaternion data
#define HS_INPUT_MSG    1               // hdg source is external message
#define HS_GPS          2               // hdg source is GPS data
#define HS_STORED       3               // hdg source is previously-stored data
#define HS_NOT_AVAIL    4               // hdg source data is unavailable

/****************************************************************************
 * Velocity Source Definitions
 ****************************************************************************/

#define VS_INPUT_MSG    0               // vel source is external message
#define VS_GPS          1               // vel source is GPS data

/****************************************************************************
 * Function Definitions
 ****************************************************************************/

void modeCtlInit(void);
void modeCtl(void);

int modeCtlRestart(void);

void modeCtlAcceptPos(double lat, double lon, double alt, double accuracy,
                      int latLonValid, int altValid, int altType, int posSrc);
void modeCtlAcceptHdg(double hdg, double accuracy,
                      int hdgValid, int hdgType, int hdgSrc);
void modeCtlAcceptVel(double vn, double ve, double vz, double accuracy,
                      int valid, int velSrc);
void modeCtlAcceptQuat(double qLG1, double qLG2,
                       double qLG3, double qLG4, int qLGValid,
                       double qLB1, double qLB2,
                       double qLB3, double qLB4, int qLBValid);
void modeCtlAcceptLGeom(double L1, double L2, double L3, double L4, int valid);
void modeCtlAcceptGpsLvrArms(double ax, double ay, double az,
                             double bx, double by, double bz,
                             int validA, int validB);
void modeCtlAcceptWowPb(int wow, int pBrake, int pBrakeValid);

/****************************************************************************
 * End
 ****************************************************************************/
#endif
