// SimulatorConstants.h

// UTM coordinates arround Pasadena
/* This pair is 5m north of the first waypoint in the tennis lot */
//#define SIMULATOR_INITIAL_NORTHING 3777630.0
//#define SIMULATOR_INITIAL_EASTING  396260.0
//#define SIMULATOR_INITIAL_YAW      3.14       

// INITIAL CONDITIONS FOR GEN MAP TEST 1 and 2
//#define SIMULATOR_INITIAL_NORTHING 3777000.0
//#define SIMULATOR_INITIAL_EASTING  396000.0
//#define SIMULATOR_INITIAL_YAW      0

// Initial conditions for qid_translated, waypoints_caltechlot,
// dogleg_right, ...
//#define SIMULATOR_INITIAL_NORTHING 3777604.0
//#define SIMULATOR_INITIAL_EASTING  396260.0
//#define SIMULATOR_INITIAL_YAW      M_PI/2

// initial conditions for darpa sample file
//#define SIMULATOR_INITIAL_NORTHING 3761045.461
//#define SIMULATOR_INITIAL_EASTING   544625.494
//#define SIMULATOR_INITIAL_YAW      0

// initial conditions for rddf qid file
//#define SIMULATOR_INITIAL_NORTHING 3772159.586
//#define SIMULATOR_INITIAL_EASTING   454061.519
//#define SIMULATOR_INITIAL_YAW      0

// initial conditions for sinusoid_50
#define SIMULATOR_INITIAL_NORTHING 3777260
#define SIMULATOR_INITIAL_EASTING   396625
#define SIMULATOR_INITIAL_YAW      0

//INITIAL CONDITIONS FOR SANTA ANITA GRABBED
//#define SIMULATOR_INITIAL_NORTHING 3777426.0
//#define SIMULATOR_INITIAL_EASTING  403533.0
//#define SIMULATOR_INITIAL_YAW      -3*M_PI/4

// Initial conditions for FenderPlanner Testing
//#define SIMULATOR_INITIAL_NORTHING 3772159.586
//#define SIMULATOR_INITIAL_EASTING  454076.519
//#define SIMULATOR_INITIAL_YAW      M_PI_2
