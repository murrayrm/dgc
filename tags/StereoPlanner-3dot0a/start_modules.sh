#!/bin/bash

# start_modules.sh
# Changes:
#   01/23/2004 - Lars Cremean - created

if [ $# -lt 1 ]; then
  echo
  echo "This program will start a series of modules"
  echo "  G = GlobalPlanner"
  echo "  L = LADARPlanner"
  echo "  A = ArbiterModule"
  echo "  D = DFEPlanner"
  echo "  S = SimVStateVDrive"
  echo "  V = StereoPlanner (Vision)"
  echo "  P = PathEvaluation"
  echo "  M = MatlabDisplay" 
  echo "  C = CorridorArcEvaluator" 
  echo
  echo "Usage example: ./start_modules.sh GADS"
  echo
  echo "Approximate locations of windows..."
  echo "+-----------------+-----------------+"
  echo "| ArbiterModule   | SimVStateVDrive |"
  echo "+-----------------+-----------------+"
  echo "| LADARPlanner    | StereoPlanner   |"
  echo "+-----------------+-----------------+"
  echo "| GlobalPlanner/  | DFEPlanner/     |"
  echo "| CorridorArcEval | MatlabDisplay   |"
  echo "+-----------------+-----------------+"
  echo
  exit 127
fi

echo

# Start the MatlabDisplay if specified
if [ `expr index $1 M` -gt 0 ]; then
  echo "Starting MatlabDisplay..."   
  # start a new xterm (bottom right) and start the MatlabDisplay within it
  xterm -sl 9999 -geometry 100x30-0-20 -e "cd DevModules/MatlabDisplay; ./MatlabDisplay; sleep 3" & 
  echo " done."
  echo
else
  echo " Did not detect M for MatlabDisplay."
fi

# Start the simulator if specified
if [ `expr index $1 S` -gt 0 ]; then
  echo "Starting SimVStateVDrive with logging..."
  # start a new xterm (top right) and start the simulator within it
  xterm -sl 9999 -geometry 100x30-0+0 -e "cd DevModules/SimVStateVDrive; ./SimVStateVDrive -l" & 
  echo " done."
else
  echo " Did not detect S for SimVStateVDrive."
fi
echo 

sleep 1

# Start the GlobalPlanner if specified
if [ `expr index $1 G` -gt 0 ]; then
  echo "Starting GlobalPlanner..."
  # start a new xterm (bottom left) and start the GlobalPlanner within it
  xterm -sl 9999 -geometry 100x35+0-20 -e "cd RaceModules/GlobalPlanner; ./GlobalPlanner" & 
  echo " done."
else
  echo " Did not detect G for GlobalPlanner."
fi
echo

# Start the CorridorArcEvaluator if specified
if [ `expr index $1 C` -gt 0 ]; then
  echo "Starting CorridorArcEvaluator..."
  # start a new xterm (bottom left) and start the GlobalPlanner within it
  xterm -sl 9999 -geometry 100x35+0-20 -e "cd RaceModules/CorridorArcEvaluator; ./CorridorArcEvaluator" & 
  echo " done."
else
  echo " Did not detect C for CorridorArcEvaluator."
fi
echo

# Start the GlobalPlanner if specified
if [ `expr index $1 P` -gt 0 ]; then
  echo "Starting PathEvaluation..."
  # start a new xterm (bottom left) and start the GlobalPlanner within it
  xterm -sl 9999 -geometry 100x50+0-20 -e "cd DevModules/PathEvaluation; ./PathEvaluation" & 
  echo " done."
else
  echo " Did not detect P for PathEvaluation."
fi
echo

# Start the DFEPlanner if specified
if [ `expr index $1 D` -gt 0 ]; then
  echo "Starting DFEPlanner..."   
  # start a new xterm (bottom right) and start the DFEPlanner within it
  xterm -sl 9999 -geometry 100x30-0-20 -e "cd RaceModules/DFEPlanner; ./DFEPlanner" & 
  echo " done."
  echo
else
  echo " Did not detect D for DFEPlanner."
fi

# Start the LADARMapper if specified
if [ `expr index $1 L` -gt 0 ]; then
  echo "Starting LADARMapper..." 
  # start a new xterm (middle left) and start the LADARMapper within it
  xterm -sl 9999 -geometry 100x30-0-200 -e "cd RaceModules/LADARMapper; ./LADARMapper" & 
  echo " done."
  echo
else
  echo " Did not detect L for LADARMapper."
fi

# Start the StereoPlanner if specified
if [ `expr index $1 V` -gt 0 ]; then
  echo "Starting StereoPlanner..."   
  # start a new xterm (middle right) and start the StereoPlanner within it
  xterm -sl 9999 -geometry 100x30-0-200 -e "cd RaceModules/StereoPlanner; ./StereoPlanner" & 
  echo " done."
  echo
else
  echo " Did not detect V for StereoPlanner (Vision)."
fi

# make the arbiter wait until the voters initialize
sleep 5

# Start the ArbiterModule if specified
if [ `expr index $1 A` -gt 0 ]; then
  echo "Starting ArbiterModule..."   
  # start a new xterm (top left) and start the ArbiterModule within it
  xterm -sl 9999 -geometry 120x48+0+0 -e "cd RaceModules/Arbiter;
  ./ArbiterModule -noglobal" & 
  echo " done."
  echo
else
  echo " Did not detect A for ArbiterModule."
fi


# xterm -sl 9999 -geometry 100x30+0+0 &  # open top left xterm
# xterm -sl 9999 -geometry 100x30-0+0  & # open top right xterm
# xterm -sl 9999 -geometry 100x30+0-20 & # open bottom left xterm
# xterm -sl 9999 -geometry 100x30-0-20 & # open bottom right xterm
