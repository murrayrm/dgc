#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <list>

#include "Vector.hh"
#include "state/state.h"
#include "gps/gps.h"
#include "magnetometer/magnetometer.h"
#include "latlong/LatLong.h"
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include "Msg/dgc_time.h"
#include "Msg/dgc_const.h"
#include <fstream>


int shutdown_flag;
using namespace std;

int main(int argc, char* argv[])
{
  state_struct mySS;

  int counter = 0;  
  int shutdown_flag = FALSE;
  char myOption;
  ofstream outfile ( "waypoints.gps", ios::binary);

  // initialize gps at 10hz, and magnetometer
  init_gps(DEFAULT_GPS_SERIAL_PORT, 10);
  cout << "Initializing Magnetometer" << endl;
  mm_init(DEFAULT_MAGNETOMETER_SERIAL_PORT);

  cout << setiosflags(ios::fixed) << setprecision(2);

  while ( shutdown_flag == FALSE ) { 
    
    cout << "Capture (c), Quit (q):" << endl;
    cin >> myOption; // = (char) getc();
    if( myOption == 'c' || myOption == 'C') {
      // Request a message from State
      mySS = get_state();      
      outfile << mySS.easting << " " << mySS.northing << endl;
      cout << "Grabbed N = " << mySS.northing << ", E=" << mySS.easting << endl;
      counter ++;

    } else if( myOption == 'q' || myOption == 'Q') {
      cout << "Exiting" << endl;
      cout << "Grabbed " << counter << " waypoints." << endl;
      shutdown_flag = TRUE;
    } else {
      cout << "Unknown Command" << endl;
    }

  }

  return 0;
}














