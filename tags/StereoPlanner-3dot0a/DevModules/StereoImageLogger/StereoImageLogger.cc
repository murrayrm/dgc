
#include "StereoImageLogger.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include <unistd.h>
#include <iostream>

extern int SIM;
extern int DISPLAY;
extern int PRINT_VOTES;


StereoImageLogger::StereoImageLogger() 
  : DGC_MODULE(MODULES::StereoImageLogger, "Stereo ImageLogger", 0) {

}

StereoImageLogger::~StereoImageLogger() {
}


void StereoImageLogger::Init() {
  // call the parent init first
  DGC_MODULE::Init();

  InitStereo();
  cout << "Module Init Finished" << endl;
}

void StereoImageLogger::Active() {

  cout << "Starting Active Funcion" << endl;
  while( ContinueInState() ) {
    // update the state so we know where the vehicle is at
    //printf("Got to line %d in %s\n", __LINE__, __FILE__);
    UpdateState();
    //printf("Got to line %d in %s\n", __LINE__, __FILE__);

    // Update the votes ... 
    StereoUpdateVotes();
    //printf("Got to line %d in %s\n", __LINE__, __FILE__);
    // and send them to the arbiter
    //Mail m = NewOutMessage(MyAddress(), MODULES::Arbiter, ArbiterMessages::Update);
    // Send the arbiter our "ID" and the vote struct.  The weights are taken care of
    // by the arbiter.
    //int myID = ArbiterInput::Stereo;
    //m << myID << d.stereo;
    //SendMail(m);

    // and sleep for a bit
    //usleep(100000);
  }

  cout << "Finished Active State" << endl;

}


void StereoImageLogger::Shutdown() {

  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;
  StereoShutdown();

}



