#ifndef REACTIONTIME_HH
#define REACTIONTIME_HH

#define MAX_VEL 10.0
#define DELTAV 0.30  // Consider us stopped when velocity is < this number

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "ReactionTimeDatum.hh"

using namespace std;
using namespace boost;

class ReactionTime : public DGC_MODULE {
public:
  // and change the constructors
  ReactionTime();
  ~ReactionTime();

  // The states in the state machine.
  // Uncomment these if you wish to define these functions.

  void Init();
  void Active();
  //  void Standby();
  void Shutdown();
  //  void Restart();

  // The message handlers. 
  //  void InMailHandler(Mail & ml);
  //  Mail QueryMailHandler(Mail & ml);

  // the status function.
  //string Status();


  // Any functions which run separate threads
  
  // Method protocols
  void ReactionUpdateVotes();
  void ReactionShutdown();

  void UpdateState();

  void SparrowDisplayLoop();
  void UpdateSparrowVariablesLoop();

private:
  // and a datum named d.
  ReactionTimeDatum d;
};



// enumerate all module messages that could be received
namespace WaypointNavMessages {
  enum {
    // we are not yet receiving messages.
    // A place holder
    NumMessages
  };
};

#endif
