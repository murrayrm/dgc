#include "ReactionTime.hh"
#include <getopt.h>

using namespace std;

#define SLEEP_OPT 10
#define LOG_OPT 11
#define VEL_OPT 12
#define HELP_OPT 14

unsigned long SLEEP_TIME = 10000; // How long in us to sleep between votes
char *TESTNAME = 0;               // id for test to be part of log file name
double DESIRED_VELOCITY = 0.0;    // Speed to run vehicle at for test in m/s
int NOSTATE = 0;                  // Do not talk to vstate if = 1

int main(int argc, char **argv) {

  int c;
  int digit_optind = 0;

  while (1) {
    int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    static struct option long_options[] = {
      {"log",       1, 0, LOG_OPT},
      {"sleep",     1, 0, SLEEP_OPT},
      {"velocity",  1, 0, VEL_OPT},
      {"help",      0, 0, HELP_OPT},
      {"nostate",   0, &NOSTATE, 1},
      {0,0,0,0}
    };

    c = getopt_long_only (argc, argv, "", long_options, &option_index);
    if(c == -1) break;

    if(c != '?') {
      switch(c) {
        case SLEEP_OPT:
          SLEEP_TIME = (unsigned long) atol(optarg);
          break;
        case VEL_OPT:
          DESIRED_VELOCITY = atof(optarg);
          break;
        case LOG_OPT:
          TESTNAME = strdup(optarg);
          break;
        case HELP_OPT:
          cout << endl << "ReactionTime Usage:" << endl << endl;
          cout << "ReactionTime -log testname -velocity v";
          cout << endl;
          cout << "             [-sleep us] [-nostate] [-help]";
          cout << endl << endl;;

          cout << "     -log testname Log all state and test results in testLogs subdir" << endl;
          cout << "                   Log files will be named <testname>_<type>_<timestamp>" << endl;
          cout << "                   where <type> can be \"state\" or \"reaction\"" << endl;
          cout << "     -sleep us     Specifies number of us to sleep between updates." << endl;
          cout << "     -velocity v   Command vehicle to velocity v when START button pressed" << endl;
          cout << "     -nostate      Will not attempt to read state" << endl;
          cout << "     -help         Displays this help" << endl << endl;
          exit(0);

      }
      cout << "option " << long_options[option_index].name;
      if(optarg) cout << " with arg " << optarg;
      cout << endl;
    }
  }

  if(DESIRED_VELOCITY == 0.0) {
    cout << endl << "YOU MUST SPECIFY A VELOCITY with -velocity" << endl;
    exit(-1);
  }
  if(DESIRED_VELOCITY > MAX_VEL) {
    cout << endl << "VELOCITY LIMITED TO " << MAX_VEL << endl;
    exit(-1);
  }
  if(TESTNAME == 0) {
    cout << endl << "YOU MUST SPECIFY A TEST NAME WITH -log" << endl;
    exit(-1);
  }

  cout << endl << "Argument summary:" << endl;
  cout << "  TEST NAME: " << TESTNAME << endl;
  cout << "  SLEEP: " << SLEEP_TIME << "us" << endl;
  cout << "  DESIRED VELOCITY: " << DESIRED_VELOCITY << endl; 
  cout << "  NOSTATE: " << NOSTATE << endl; 
  // Done doing argument checking.
  ////////////////////////////////

  cout << endl;
  cout << "VERIFY THAT YOU HAVE SPECIFIED THE CORRECT SWITCHES!" << endl;
  cout << "Do you wish to proceed? (type 'yes', anything else exits)" << endl;
  char answer[100];
  fgets(answer,sizeof(answer),stdin);
  if(strcmp(answer, "yes\n")) {
    exit(-1);
  }
  
  // Start the MTA By registering a module
  // and then starting the kernel

  Register(shared_ptr<DGC_MODULE>(new ReactionTime));
  StartKernel();

  return 0;
}
