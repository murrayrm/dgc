load statelog.dat;
kflat = statelog(:,13);
kflng = statelog(:,14);

% go down until we find that the KF has started using GPS locations to
% generate data
i = 1;
while kflat(i) == 0
    i = i +1;
end
kflat = kflat(i:end);
kflng = kflng(i:end);

%plot data 
figure(1)
hold on
plot(kflng, kflat, '.b')
plot(kflng, kflat, 'r')
hold off

%get gps data
load gpslog.dat;
gpslat = gpslog(:,4);
gpslng = gpslog(:,5);       

figure(2)
hold on
plot(gpslng, gpslat, '.g')
plot(gpslng, gpslat, 'k')
hold off

figure(3)
hold on
plot(kflng, kflat, '.b')
plot(kflng, kflat, 'r')
plot(gpslng, gpslat, '.g')
plot(gpslng, gpslat, 'k')
hold off