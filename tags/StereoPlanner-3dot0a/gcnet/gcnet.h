/* gcnet.h: Grandchallenge Packet-Oriented Network Protocol C Interface. */

/* This code allows you to implement a bidirectional packet-oriented protocol.
 *
 * The server always operated on both UDP and TCP.
 * The TCP encapsulation is:
 *    Packet-Data-Length [32 bit big-endian int]
 *    CRC [32 bit big-endian int]
 *    Packet Data Body [Packet-Data-Length bytes]
 * The UDP encapsulation is:
 *    CRC [32 bit big-endian int]
 *    Packet Data Body [Remainder of the UDP packet]
 */

#ifndef _GCNET_H_
#define _GCNET_H_

typedef void (*GcnetReceiveFunc) (const void *packet_body,
                                  unsigned    packet_length,
				  void       *user_data);

typedef struct _Gcnet Gcnet;

#ifdef __cplusplus
extern "C" {
#endif

Gcnet *gcnet_new_client          (const char      *host,
                                  unsigned         port);
Gcnet *gcnet_new_server          (unsigned         port);
void   gcnet_transmit            (Gcnet           *connection,
                                  const void      *packet_body,
                                  unsigned         packet_length,
      	                          int              reliable);
void   gcnet_set_receive_handler (Gcnet           *connection,
      	                          GcnetReceiveFunc receive_func,
      	                          void            *user_data);
void   gcnet_destroy             (Gcnet           *connection);

#ifdef __cplusplus
}
#endif

#endif
