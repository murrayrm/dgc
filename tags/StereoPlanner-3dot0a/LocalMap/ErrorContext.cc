/* ErrorContext.cc
   Author: J.D. Salazar
   Created: 7-24-03
   
   ErrorContext is an object used to accumulate errors during tests and produce output for these tests.
*/

#include "ErrorContext.h"
#include <iostream>
#include <sstream>
#include <set>

using std::endl;

// Writes the header to a stream
ErrorContext::ErrorContext(std::ostream &stream, std::string file)
  : os(stream),
    file(file),
    passedTests(0),
    totalTests(0),
    lastLine(0),
    skipLine(false)
{
  os << "line: ";
  os.width(65);
  os.setf(std::ios::left, std::ios::adjustfield);
  os << "description" << " result" << endl;
  os.width(78);
  os.fill('~');
  os << "~" << endl;
  os.fill(' ');
  os.setf(std::ios::right, std::ios::adjustfield);
}


// Writes a line/description
void ErrorContext::desc(std::string msg, int line)
{
  // Check if a line should be skipped.
  if( (lastLine != 0) ||
      ((msg[0] == '-') && skipLine) )
    {
      os << std::endl;
    }

  os.width(4);
  os << line << ": ";
  os.width(65);
  os.setf(std::ios::left, std::ios::adjustfield);
  os << msg << " ";
  os.setf(std::ios::right, std::ios::adjustfield);
  os.flush();

  lastLine = line;
  skipLine = true;
}

// Write test result.
void ErrorContext::result(bool good)
{
  if(good)
    {
      os << "ok";
      ++passedTests;
    }
  else
    {
      os << "ERROR";
      badLines.insert(lastLine);
    }

  os << endl;
  ++totalTests;
  lastLine = 0;
}

// True iff all tests are passed.
bool ErrorContext::passedAllTests() const
{
  return passedTests == totalTests;
}

// Write summary info.
ErrorContext::~ErrorContext()
{
  os << endl 
     << "Passed " << passedTests << "/" << totalTests << " tests." 
     << endl
     << endl;

  if (badLines.size() > 0)
    {
      os << "For more information, please consult: " 
	 << endl;
      
      for( std::set<int>::const_iterator it = badLines.begin();
	   it != badLines.end();
	   ++it )
	{
	  os << " " << file << ", line " << *it << 
	    endl;
	}
      os << endl;
    }
}
