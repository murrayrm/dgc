Gregory West, 9/10/2003.

README version 1.0 - initial description for file purposes, vague inputs and 
			outputs description to be refined and elaborated in
			future versions.

This file documents usage for the files:

tiff_to_1km.cpp
merge_1km_fragments.cpp
(force_1km_merge.cpp)


These files are not intended for use in the DARPA vehicle, but serve roles in
converting the original Digital Orthophoto Quads (DOQs) downloaded from the 
internet in the .tiff or GeoTiff format into 1-kilometer, 1000x1000 bitmaps
whose UTM pixel locations can be derived from the pixel location and filename.
Each of these files compiles as a stand-alone with its own main function and no
other code outside of <vector> or other typical inclusions.  THESE FILES MAY
INCLUDE unistd.h AND fcntl.h AND SHOULD ONLY BE RUN IN AN ENVIRONMENT THAT
SUPPORTS THESE FILE IO OPERATIONS (By my understanding, this means Linux.  I do
not know whether/what OSs are precluded).

This conversion is a two-step process:

The first step uses the executable from tiff_to_1km.cpp.  This step translates 
a .tiff image and the information from its header into a set of 1-km maps.  
The 1-km maps generated are not of an arbitrary location, but are lined up so
that the hundreds, test, and ones digit of the UTM eastings and northings of 
the lowwer right pixel are all 0.  Thus every lower-left pixel in every 1-km 
map is justified to be xyz000 eastings and abcd000 nortings, with no overlap between any 1-km maps.
Consequently, this executable has 2 bitmap files outputs: the complete maps for
all these given regions that lie entirely with in the given TIFF image, and
incomplete bitmaps from 1-km regions that did not lie entirely within the given
TIFF image and would need to be merged to be complete.  These 'fragments' are
designated as such in their filename.  This filename should not be changed, or 
the executable in the second step will be unable to identify it.

The second step uses the executable from merge_1km_fragments.cpp.  It merges 
the fragment files output from the first step to result in to have the entire 
area with map data populated by merged maps.  CURRENTLY, SEVERAL ERROR-CHECKING
CRITERIA MUST OCCUR FOR THE MERGE TO TAKE PLACE, AND NO MERGED FRAGMENTS ARE 
DELETED AFTER A SUCCESSFUL MERGE.  For now a merge is not performed if one of 
the following conditions is met:
	1.  A file with the name that would be the output of the merge already
	exits.  The code is intended to be compatable with an input of any of
	the possible fragment naming conventions (each one of which specifies
	the exact corner or edge of the completed map where the frament
	belongs).  Hence the simplest script for mass-performing this step
	might try to perform the merge 2 or 4 times for the same region, so for
	simplicity this assumes if a file already exists it shoudl be left in
	tact.
	2.  Currently, a file will not merge if all the other files it would 
	need to merge with cannot be found.  An edge expects to find 2 edges
	total to merge, and a corner 4 corners total.  Code is in place to 
	default a cell value to unknown if the merge can't produce a fragment 
	that has a value for that pixel, but for now this catch is in place
	until the completeness of available downloads can be confirmed.

	SPECIAL CASES TO NOTE: 
	(3) Some merges will not need to occur at all.  There is a large amount
	of overlap between the TIFF files, so there may in practice emerge a
	large number of cases where one image generates a fragment but the 
	adjacent image has the complete 1-km (in which case it almost certainly
	generates another needless fragment).  This error would likely fall 
	under both the above cases (pre-existing file and absent fragments).
	(4) A case where 2 corners are opposite a complete edge is not handled,
	and will fail a merge.  I do not know how likely this is to occur in
	practice because I am not closely familiar with the overlap size and
	variation in origins of the TIFF soruce images.  If I recall my test
	cases correctly, the closest I saw to this case in practice was an edge
	overlapping with a corner, possibly with another corner fragment also
	existing, and I believe in that case a complete 1-km map had already
	been produced by the 4th TIFF involved.


Note that the third file, force_1km_merge.cpp, is NOT INVOLVED in any of the 
primary steps outlined above.  It is a slightly altered file for merging 
fragments primarily intended for outputting human-readable outputs.  While the
normal merging function overwrites smaller consequtive fragments with larger
ones to maximize continuous regions and hopefully reduce error, this function
reverses the order to maximize the visible over-lap and interference.  Its
output is intended to be human-read 1-km maps intended to give the user an idea
of how closely the maps align in that region.  THIS EXECUTABLE MAY ALLOW OR 
FORBID A MERGE UNDER DIFFERENT CIRCUMSTANCES THAN THE MERGE ERROR CONDITIONS 
DESCRIBED ABOVE (as of README version 1.0).


I will outline usage, file inputs and outputs and typical or error-indicative 
messages in the next version of this README.  Briefly for the initial version:


The executable to extrack 1-km maps and fragments from a .tiff is run at the
commandline with the following syntax:

tiff_extractor_name.exe [HEADER file name]

NOTE THAT THE INPUT MUST BE THE HEADER FILE NAME FOR THE TIFF IMAGE AND NOT THE
IMAGE ITSELF.  The tiff name is easily derived from the header, but different
tiffs use different conventions to name their header.  All output messages to 
the user go to cout.


The executable to merge fragments is called with the following syntax:

merger_name.exe [fragment bitmap filename]

The executable will automatically check, given any fragment, for the expected
associated fragment names (2 files if an edge, 4 files if a corner) and perform
the merge (version 1.0) IFF they are all present.  Merged fragments are not 
deleted.  No fragment output by the tiff extractor can have its file name 
changed or it will become unrecognizable to the extraction code.
