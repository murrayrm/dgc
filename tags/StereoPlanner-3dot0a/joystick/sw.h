#ifdef __cplusplus
extern "C" {
#endif


#ifndef SW_H
#define SW_H

#include "parallel/parallel.h"

// Pin definition for polling X, Y, Button
#define	STEER_X_PIN		PP_PIN14	
#define	STEER_Y_PIN		PP_PIN16	
#define	STEER_BUTTON_PIN	PP_PIN17	

// Mask for buttons
#define STEER_BUTTON_A		1<<3	
#define STEER_BUTTON_B		1<<2	
#define STEER_BUTTON_C		1<<1	
#define STEER_BUTTON_D		1<<0
#define STEER_CALIBRATE		(0xFFFFFFF0 | (STEER_BUTTON_A | STEER_BUTTON_B | STEER_BUTTON_C | STEER_BUTTON_D))
// When all 4 buttons are pressed, calibrate the steering wheel and pedals.
#define STEER_TURN_ON		(0xFFFFFFF0 | (STEER_BUTTON_A | STEER_BUTTON_D))
// When buttons A and D are pressed, steering wheel is turned on to drive the car

#define STEER_ON	1
#define STEER_OFF	0

/* When the program is initialized, benchmark the system and figure out the system speed
 * when a busy loop is executed because a busy loop will be used to delay when waiting
 * for output.
 */

#define BUSY_LOOP_NUM		30	// use the busy loop this many times to take the average delay
#define BUSY_LOOP_COUNT		500000 	// in each busy loop, loop from 0 to this number to measure the delay

#define STEER_POLL_NUM		2	// polling steering wheel this number of times
					// before returning a reading to the system
#define STEER_POLL_DELAY	1	// delay between each polling in us

#define	STEER_ON_UPDATE_PER_SEC	30	// number of update in one second when joystick is in use
#define	STEER_OFF_UPDATE_PER_SEC	10	// number of update in one second when joystick is not used
#define STEER_ON_UPDATE_DELAY_US	(200000/STEER_ON_UPDATE_PER_SEC) // should be 1000000/STEER_UPDATE_PER_SEC, but linux may add more delay when using usleep, so change 1000000 to 500000
#define STEER_OFF_UPDATE_DELAY_US	(500000/STEER_OFF_UPDATE_PER_SEC)

#define STEER_INPUT_DELAY	4000	// number of micro seconds of output delay for uC
#define STEER_WAIT		10	// wait this us before requesting another output
#define STEER_ERROR_DELAY       10    // wait this long if an error occurs

#define STEER_READ_X_TOLERANCE	80	// if difference of successive readings of steering 
#define STEER_READ_Y_TOLERANCE	30	// if difference of successive readings of steering 
					// wheel are within this number,then this set of 
					// readings are valid.

/* After testing steering wheel, steering wheel returns between 8 and 243 with 124 as
 * center (steering wheel in center position. When you press brake, it changes between 
 * 120 and 164. When you press gas
 * it changes between 85 and 38 with 85 as center( nothing pressed).
 */

/* Here defines the offset for gas and brake. When I tested gas and brake, gas goes
 * from center (about 85) to 38, and goes straight to 8 even if you are not pressing
 * gas all the way down. So an offset is needed. Also, the brake pot won't really turn  
 * until you press brake half down. And by the time the pot turns, the brake returns 120, which
 * is another offset that will be used to determine whether a brake has been pressed. Since
 * gas starts reacting once you press the gas pedal, a safe margin is needed so that in case
 * the y-axis center moves upward due to temperature change, the car wouldn't think the gas
 * pedal is pressed even if nobody is touching it.
 */
 
/* It is stronly suggested that the driver calibrates the steering wheel and pedals by 
 * pressing all 4 buttons before driving. The defulat here may move due to the environment
 */



#define STEER_GAS_SAFE_MARGIN	4	// We wouldn't tell the car to move unless values less than (STEER_DEFAULT_Y_CENTER-STEER_GAS_SAFE_MARGIN) are returned from steering wheel
#define STEER_GAS_PRESS		38	// gas offset before going all the way down to 8
#define STEER_GAS_UNPRESS	86	// value returned when gas and brake are not pressed

#define STEER_BRAKE_UNPRESS	119	// value returned once brake is pressed
#define STEER_BRAKE_PRESS	162	// value returned when brake is pressed all the way down

#define STEER_DEFAULT_Y_CENTER	85	// y-axis center, nothing pressed
#define STEER_DEFAULT_X_CENTER	126	// x-axis center, steering wheel is centered

#define STEER_RIGHT_END		243	// value returned when you turned steering wheel all the way to the right
#define STEER_LEFT_END		10	// value returned when you turned steering wheel all the way to the left
#define STEER_GAS_RANGE		(STEER_GAS_UNPRESS-STEER_GAS_SAFE_MARGIN-STEER_GAS_PRESS)	
#define STEER_BRAKE_RANGE	(STEER_BRAKE_PRESS-STEER_BRAKE_UNPRESS)	

struct	steer_struct
{
	float x;
	float y;
	int buttons;
};

/* call init_steering(port) with the number of parallel port steering wheel is connected
 * to to initiate steering wheel
 */
int init_steering(int port);

/* call this function with a steer_struct pointer to get x, y, and buttons information
 * x, and y are returned between -1 and 1. for x, positive is right and negative is left
 * 1 for x is all the way to the right, and -1 is all the way to the left. 
 * For y, 1 is full gas, and -1 is full brake.
 */
void get_steering(struct steer_struct *st);

/* When done with steering wheel, call end_steering() to finish steering wheel
 */
int end_steering();

#endif

#ifdef __cplusplus
}
#endif

