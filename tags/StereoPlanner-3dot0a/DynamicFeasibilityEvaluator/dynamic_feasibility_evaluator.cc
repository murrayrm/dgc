
#include "vehicle_constants.h"
#include "rollover_functions.h"
#include "dynamic_feasibility_evaluator.h"

void dynamic_feasibility_evaluator( const double v, 
        const double yaw, const double pitch, const double roll, 
        const double T_roll,
        const double *steering_angle, const unsigned int n_steering_angles,
        double *c_roll, double *min_v, double *max_v)
{
    /*
         * Inputs:
         *  - v         : [m/s] forward velocity
         *
         *  - yaw, pitch, roll  : [radians] angles defining orientation of vehicle to inertial reference frame
         *                          (i.e., from these we know the slope of the terrain)
         *
         *  - steering_angle    : [radians] array of steering angles defining the arcs to be evaluated
         *
         *  - n_steering_angles : number of entries in the array
         *
         *  - T_roll    : [unitless] rollover safety threshold, bounded from 0 to 1. At a value of 1, the vehicle
         *
         *  Outputs:
         *  - c_roll    : [unitless] array of rollover coefficients for the evalutated arcs
         *                  for the current forward velocity.
         *                  c_roll = 
         *                      0 if vehicle is perfectly stable
         *                      magnitude 1 if on the verge of rolling over
         *                      magnitue > 1 if there is a rollover torque present
         *                      c_roll < 0 if leaning to the right
         *                      c_roll > 0 if leaning to the left
         *                      so c_roll < -1 => rolling rightward
         *
         * - min_v      : [m/s] array of minimum velocity for each arc such that the rollover coefficient is 
         *                  within the rollover safety threshold T_roll
         *
         * - max_v      : [m/s] array of maximum velocity for each arc such that the rollover coefficient is 
         *                  within the rollover safety threshold T_roll
     */
     
    /* Given yaw, pitch, roll, figure out vertical and lateral components of gravity in the 
     * vehicle reference frame */
    double g_z, g_y, g_x;
    gravity_components_in_vehicle_ref_frame(yaw, pitch, roll, &g_z, &g_y, &g_z);


    /* Given rollover safety threshold compute min and max lateral accelerations */
    double a_R, a_L;
    min_max_lateral_acceleration_for_rollover_safety(g_z, g_y, T_roll, &a_R, &a_L);

    /* TODO: we may want to change the representation of arcs from a simple array of steering angles ???
    /* For each possible arc (steering angle), evaluate:
     *  - rollover coefficient for current forward velocity
     *  - minimum and maximum forward velocities permissible to not exceed rollover threshold
     */

    double a_T; // [m/s^2] the total lateral acceleration of the vehicle, due to following the specified trajectory
                // at the specified speed

    for (unsigned int i=0; i<n_steering_angles; i++)
    {
        double s_angle = steering_angle[i];

        double arc_radius = steering_angle_2_arc_radius( s_angle );
        // arc_radius is positive for rightward turns, negative for leftwards turns,
        // and ZERO for moving straigh ahead (more useful than infinity)
        
        if (s_angle == 0)
        {
            // special case of moving straight ahead
            a_T = 0;
        }
        else
        {
            a_T = v*v/arc_radius;
        }

        // given the current velocity and arc (ie current lateral acceleration)
        // compute the rollover coefficient
        c_roll[i] = rollover_coefficient(a_T, g_z, g_y, c_roll[i]);

        min_max_velocity_for_rollover_safety(a_R, a_L, arc_radius, min_v[i], max_v[i]);
    }
}

