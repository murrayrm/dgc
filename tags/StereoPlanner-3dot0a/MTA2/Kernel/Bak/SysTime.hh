#ifndef SysTime_HH
#define SysTime_HH

#include <Misc/Mod/ModAddress.hh>

class SysTime {
public:
  // The Kernel will synchronize Time with the system master
  SysTime(ModAddress systemMasterAddress);
  ~SysTime();

  // accessors
  int OK(); // checks to see if updating time properly
  TimeVal GetTime(); // gets current system time
  

  // mutators
  // Forces system time refresh 
  void RefreshTime();
  void NewSysMas(ModAddress sysMasAddress);

private:

  // put all member variables in a struct 
  // to keep them apart
  struct {
    int status; // status flag
    TimeVal timeZero;
    ModAddress SysMasAddress;
    boost::recursive_mutex mutex;
  } m;

};

#endif
