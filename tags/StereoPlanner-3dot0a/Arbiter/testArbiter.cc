/* testArbiter.cc
   Author: J.D. Salazar
   Created: 8-25-03

   This program tests the Arbiter class. */

#include <iostream>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <functional>
#include "Arbiter.h"
#include "RandomVoter.h"
#include "Vote.h"
#include "../LocalMap/ErrorContext.h"


// Reliably reproducible pseudorandom numbers:
int rnd() { return int(lrand48()); }
double drnd(double max) { return double(drand48() * max); }
int rnd(int max) { return int(drand48() * max); }

const double PI = 3.1415;

void voteTests(ErrorContext &ec, int testLevel)
{
  {
    std::ostringstream oss;
    oss << "--- Vote tests ---";
    ec.DESC(oss.str());
  }

  // Check default constructor
  {
    Vote v;

    {
      std::ostringstream oss;
      oss << "Default constructor sets 'goodness' to 'Vote::DEFAULT_GOODNESS'";
      ec.DESC(oss.str());
    }
    ec.result( v.goodness == Vote::DEFAULT_GOODNESS );


    {
      std::ostringstream oss;
      oss << "Default constructor sets 'phi' to 'Vote::DEFAULT_PHI'";
      ec.DESC(oss.str());
    }
    ec.result( v.phi == Vote::DEFAULT_PHI );
  }

  // Check basic constructor
  for(int pass = 1; pass <= testLevel; ++pass)
    {
      int rndGoodness = rnd(testLevel * testLevel + 1);
      double rndPhi = drnd( 3 * PI ) - drnd( 3 * PI );



      {
	std::ostringstream oss;
        oss << "Basic constructor sets 'phi' to "
            << rndPhi
            << " (pass=" << pass << ")";
        ec.DESC(oss.str());

        Vote v(rndPhi);
        ec.result( v.phi == rndPhi );
      }

      {
	std::ostringstream oss;
        oss << "Basic constructor sets 'goodness' to "
            << rndGoodness
            << " (pass=" << pass << ")";
        ec.DESC(oss.str());

        Vote v(rndPhi, rndGoodness);
        ec.result( v.goodness == rndGoodness );
      }
    }

  // Test == and != operators
  {
    for(int pass = 1; pass <= testLevel; ++pass)
      {
        int rndGoodness = rnd(testLevel * testLevel + 1);
        double rndPhi = drnd( 3 * PI ) - drnd( 3 * PI );

        Vote v1(rndPhi, rndGoodness);
        Vote v2(rndPhi, rndGoodness);

	std::ostringstream oss;
        oss << "== and != operators with two equal 'Vote's"
            << " (pass=" << pass << ")";
        ec.DESC(oss.str());
        ec.result( v1 == v2 && !(v1 != v2) );
      }

    for(int pass = 1; pass <= testLevel; ++pass)
      {
        int rndGoodness1, rndGoodness2;
        double rndPhi1, rndPhi2;
        do
          {
            rndGoodness1 = rnd(testLevel * testLevel + 1);
            rndPhi1 = drnd( 3 * PI ) - drnd( 3 * PI );

            rndGoodness2 = rnd(testLevel * testLevel + 1);
            rndPhi2 = drnd( 3 * PI ) - drnd( 3 * PI );
          }
        while(rndGoodness1 == rndGoodness2 && rndPhi1 == rndPhi2);

        Vote v1(rndPhi1, rndGoodness1);
        Vote v2(rndPhi2, rndGoodness2);

	std::ostringstream oss;
        oss << "== and != operators with two unequal 'Vote's"
            << " (pass=" << pass << ")";
        ec.DESC(oss.str());
        ec.result( v1 != v2 && !(v1 == v2) );
      }
  }

  // Check Copy Constructor
  for(int pass = 1; pass <= testLevel; ++pass)
    {
      int rndGoodness = rnd(testLevel * testLevel + 1);
      double rndPhi = drnd( 3 * PI ) - drnd( 3 * PI );

      Vote v1(rndPhi, rndGoodness);
      Vote v2 = v1;

      std::ostringstream oss;
      oss << "copy constructor"
          << " (pass=" << pass << ")";
      ec.DESC(oss.str());
      ec.result( v1 == v2);
    }

  // = operator
  for(int pass = 1; pass <= testLevel; ++pass)
    {
      int rndGoodness = rnd(testLevel * testLevel + 1);
      double rndPhi = drnd( 3 * PI ) - drnd( 3 * PI );

      Vote v1(rndPhi, rndGoodness);
      Vote v2;

      v2 = v1;

      std::ostringstream oss;
      oss << "= operator"
          << " (pass=" << pass << ")";
      ec.DESC(oss.str());
      ec.result( v1 == v2);
    }
}

void voterTests(ErrorContext &ec, int testLevel)
{
  {
    std::ostringstream oss;
    oss << "--- Voter tests (using RandomVoter) ---";
    ec.DESC(oss.str());
  }

  // Check default values
  {
    std::ostringstream oss;
    oss << "'DEFAULT_NUMBER_OF_ARCS' is positive";
    ec.DESC(oss.str());
  }
  ec.result( Voter::DEFAULT_NUMBER_OF_ARCS > 0 );


  {
    std::ostringstream oss;
    oss << "'DEFAULT_MAX_VOTE' is non-negative";
    ec.DESC(oss.str());
  }
  ec.result( Voter::DEFAULT_MAX_VOTE >= 0 );

  {
    std::ostringstream oss;
    oss << "'DEFAULT_LEFT_PHI' is less than 'DEFAULT_RIGHT_PHI'";
    ec.DESC(oss.str());
  }
  ec.result( Voter::DEFAULT_LEFT_PHI <= Voter::DEFAULT_RIGHT_PHI );


  // Check default constructor
  {
    RandomVoter v;

    {
      std::ostringstream oss;
      oss << "Default constructor sets the number of arcs to default";
      ec.DESC(oss.str());
    }
    ec.result( v.getNumberOfArcs() == Voter::DEFAULT_NUMBER_OF_ARCS );

    {
      std::ostringstream oss;
      oss << "Default constructor sets the maximum vote to default";
      ec.DESC(oss.str());
    }
    ec.result( v.getMaxVote() == Voter::DEFAULT_MAX_VOTE );

    {
      std::ostringstream oss;
      oss << "Default constructor sets the left phi to the default";
      ec.DESC(oss.str());
    }
    ec.result( v.getLeftPhi() == Voter::DEFAULT_LEFT_PHI );


    {
      std::ostringstream oss;
      oss << "Default constructor sets the right phi to the default";
      ec.DESC(oss.str());
    }
    ec.result( v.getRightPhi() == Voter::DEFAULT_RIGHT_PHI );
  }

  // Check basic constructor
  for(int pass=1; pass <= testLevel; ++pass)
    {
      int rndNumberOfArcs, rndMaxVote;
      double rndLeftPhi, rndRightPhi;

      do
        {
          rndNumberOfArcs = rnd(testLevel) + 1;
          rndMaxVote = rnd(testLevel);
          rndLeftPhi = drnd( 3 * PI ) - drnd( 3 * PI );
          rndRightPhi = drnd( 3 * PI ) - drnd( 3 * PI );
        }
      while( rndLeftPhi > rndRightPhi );



      {
	std::ostringstream oss;
        oss << "Basic constructor set number of arcs to "
            << rndNumberOfArcs
            << " (pass=" << pass << ")";
        ec.DESC(oss.str());

        RandomVoter v(rndNumberOfArcs);
        ec.result( v.getNumberOfArcs() == rndNumberOfArcs );
      }


      {
	std::ostringstream oss;
        oss << "Basic constructor set max vote to "
            << rndMaxVote
            << " (pass=" << pass << ")";
        ec.DESC(oss.str());

        RandomVoter v(rndNumberOfArcs, rndMaxVote);
        ec.result( v.getNumberOfArcs() == rndNumberOfArcs );
      }


      {
	std::ostringstream oss;
        oss << "Basic constructor set left phi to "
            << rndLeftPhi
            << " (pass=" << pass << ")";
        ec.DESC(oss.str());

      }
      v.setRightPhi(rndRightPhi);
      ec.result( rndRightPhi == v.getRightPhi() );
    }

// Check basic functionality of 'getVotes'
for(int pass = 1; pass <= testLevel; ++pass)
{
  RandomVoter v;

  {
    std::ostringstream oss;
    oss << "'getVotes' returns a structure with vaild vote values"
	<< " (pass=" << pass << ")";
    ec.DESC(oss.str());
  }
  Voter::voteListType votes = v.getVotes();

  bool wasTestSuccessful = true;
  for(Voter::voteListType::iterator iter = votes.begin(); iter != votes.end()\
	; ++iter)
    {
        wasTestSuccessful = wasTestSuccessful
          && (*iter).goodness >= 0
          && (*iter).goodness <= v.getMaxVote();
    }
  ec.result( wasTestSuccessful);
}

}

int main(int argc, const char *argv[])
{
// Use the first argument, if any, as the test level.
int testLevel = ( (argc > 1) ? atoi(argv[1]) : 0);

if( testLevel <= 0 )
{
  // User gave an invalid argument or didn't give an argument so tell them \
how to
  // use the program.
  std::cout << "\n\nTest level may be specified as a positivie integer argu\
ment,"
	    << " where a higher level yields more tests.\n"
	    << "\nMiminum level needed for specific tests are:\n "
	    << "\t1 - Vote struct tests\n"
	    << "\t2 - Voter class tests\n\n";

 return 0;
}

// Set everything up
 std::cout << "\n\nPerforming a level " << testLevel
	   << " check of Arbiter.\n\n";
 ErrorContext ec( std::cout, __FILE__ );
 srand48(testLevel);

 // Perform tests
 if( testLevel >= 1 )
   {
     voteTests(ec, testLevel);
   }
 if( testLevel >= 2)
   {
     voterTests(ec, testLevel);
   }

 // Return 0 (success) if all tests were passed, 1 otherwise
 return !ec.passedAllTests();
}
