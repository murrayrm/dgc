/*
 * display test file
 *
 * RMM 12/2/87
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "display.h"

/* Global variables */
int time_spent;			/* Seconds spent inside program */
int ival, ival_c;		/* integer values (orig and callback) */
double dval, dval_c;		/* double values (orig and callback) */

/* Now include display table (after globals are declared) */
#include "dispexmp.h"		/* display table (generated by cdd) */

int main(int argc, char **argv)
{
    /* Initialize the display package */
    if (dd_open() < 0) exit(1);		/* initialize the database, screen */
    dd_bindkey('Q', user_quit);
    dd_usetbl(display);			/* set display description table */

    /* User initialization goes here */

    /* Pass control to the display manager */
    dd_loop();				/* Start the display manager */
    dd_close();				/* clear the screen and free memory */

    /* User cleanup goes here */
}

/* Callbacks */
int toggle_onoff(long arg) { return 0; }
int user_quit(long arg) { return DD_EXIT_LOOP; }
int graph_data(long arg) { return 0; }
int capture_data(long arg) { return 0; }
int dump_data(long arg) { return 0; }

int ival_callback(long arg) { ival_c = ival; return 0; }
int dval_callback(long arg) { dval_c = dval; return 0; }

/* Display function for time spent in program */
int user_clock(DD_ACTION action, int arg)
{
    static int init = 0;
    static long start_time, cur_time;

    /* Initialize the first time through */
    if (!init) {
	/* get the current value of system clock */
	start_time = time(NULL);

	++init;
    }

    /* update the current clock value */
    cur_time = time(NULL);
    time_spent = (int) (cur_time - start_time);

    /* now just use the usual integer display routine */
    return dd_short(action, arg);
}
