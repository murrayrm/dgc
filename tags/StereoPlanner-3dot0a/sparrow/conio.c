/*
 * conio.c - mimic Borland console routines
 *
 * Richard M. Murray
 * 27 Jul 92
 *
 * This file converts Borland C console calls into termcap calls
 *
 */

#include <stdio.h>
#include <term.h>
#include <curses.h>
#include "conio.h"
#include "tclib.h"
#include "termio.h"

int dd_getch()		{ return tc_getc(); }
void putch(int ch)	{ outc((int) ch); outf(); }
int kbhit() 	{ return cready(); }
int clreol() 	{ tc_clear_to_eol(0); return 0; }
void clrscr()	{ tc_clear(24); }


/* Set the cursor type (ignored) */
void _setcursortype(int type) { curs_set(type); }

/* Get information about the display */
void gettextinfo(struct text_info *tip)
{
    tip->screenheight = tgetnum("li");
    tip->screenwidth = 80;
}

/* Goto an x, y location */
int gotoxy(int x, int y)
{
    tc_amove(y-1, x-1);
    return 0;
}

/* Change the foreground/background colors; key off of background */
void textcolor(int color) {
#ifdef COLOR
  if (tc_color) tc_setfg(color);
#endif
}

void textbackground(int color)
{
#ifdef COLOR
  if (tc_color) tc_setbg(color);
  else
#endif
    switch (color) {
    case 0:				/* assume BLACK = 0 */
	tc_normal();
	break;
    default:
	tc_reverse();
	break;
    }
}
