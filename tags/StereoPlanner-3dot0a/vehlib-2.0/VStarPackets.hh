#ifndef __VSTARPACKETS_HH___
#define __VSTARPACKETS_HH___


// This include file contains definitions for all MTA messages 
// used to communicate between V* Modules, and their respective
// data structures. Note that although these data structures are
// used to communicate between modules, they are not guaranteed
// to be used internally by the V* programs when storing the data
// for its own use.
// VDrive, VState, VManage and VRemote

// Created 2/19/04 - Ike Gremmer

/* ----------------------- READ THIS --------------------------
 * All messages sent to vdrive, etc, have 2 basic parts defined below.
 * The first is the data packet that gets sent (i.e. VState_Packet) and 
 * the second is a helper function to gather this information.  For MTA 
 * purposes, the first argument is the MailAddress of the sender (whichever
 * program is requesting the data) and the second argument is a struct (the packet)
 * that will be filled by the helper function.  You can use the MyAddress() 
 * function to grab the MailAddress of the sender.  The helper function
 * will return 0 if successful and <0 if it failed.  If the operation failed,
 * the contents of the packet structure will not be changed.
 *
 * For example:
 *
 * struct VM_for_VD_Packet; // (defined below)
 * // this packet is stuff that vdrive will be interested in getting
 * // therefore in vdrive we would do the following to get it
 *
 * ---- in vdrive.cc or wherever ---------
 * VM_for_VD_Packet myPacket;
 * if ( Get_VM_for_VD_Packet( MyAddress(), myPacket ) == 0 ) {
 *    cout << "We got the packet from vstate :)" << endl;
 * } else {
 *    cout << "Requesting VM_for_VD_Packet failed." << endl
 *         << "Contents of myPacket not changed."   << endl;
 * }
 *
 * ----------------------------------------------------------------
 * ----------------------------------------------------------------
 * ----------------------------------------------------------------
 * ----------------------------------------------------------------
 * Packets defined in this file are broken into sections:
 *
 * Section 1: Hardware Devices
 *            A module can request an info packet for a particular device
 *            --VManage Devices (Transmission, Ignition, EStop, OBD2)
 *            --VDrive  Devices (Steer, Throttle, Brake)
 *            --VRemote Devices (Joystick)
 *            --Note: VState Devices currently undefined
 *
 * Section 2: Remote Data/Command Packets
 *            Packets designed so that a module can control features
 *            in another module, or get data from them.
 *            --VM_for_VD_Packet: VManage info for VDrive such as estop
 *                                and drive modes.
 *            --VM_EStop_Behavior_Packet: Sets behavior of an EStop.
 *            --VState_Packet:    The usual state data
 *
 * Section 3: Display Packets
 *            One packet for each module containing everything that VRemote
 *            could possibly want to display.  Note these are called stacks
 *            instead of packets.
 *            --VManage_Stack
 *            --VDrive_Stack
 *            --VState_Stack
 *
 *
 *


*/

#include "MTA/Kernel.hh"
#include "VStarDevices.hh"
#include "VState.hh"
#include "VDrive.hh"


//*************************************************************************************
//*************************************************************************************
//*************************************************************************************
//*************************************************************************************
//*************************************************************************************

/*
 * Section 1: Hardware Devices
 *            A module can request an info packet for a particular device
 *            --VManage Devices (Transmission, Ignition, EStop, OBD2)
 *            --VDrive  Devices (Steer, Throttle, Brake)
 *            --VRemote Devices (Joystick)
 *            --Note: VState Devices currently undefined
 */


/*  Packets for VManage Devices
 *  Transmission
 *  Ignition
 *  EStop
 *  OBD2
 */


// The transmission -- Can Set or Get Transmission Packet
//*************************************************************************************
struct Transmission_Packet {
  int Gear; // Gear = -1 for reverse, 0 for neutral, 1,2,3 for drive gear settings.
};
int Get_Transmission_Packet( const MailAddress& myAddress, Transmission_Packet& tp);
int Set_Transmission       ( const MailAddress& myAddress, Transmission_Packet& tp);
// Set_Transmission -- will attempt to set gear based on tp.Gear;




// The Ignition -- Can Set or Get Ignition with Packet
//*************************************************************************************
struct Ignition_Packet {
  int Enabled; // Gear = -1 for reverse, 0 for neutral, 1,2,3 for drive gear settings.
};
int Get_Ignition_Packet( const MailAddress& myAddress, Transmission_Packet& tp);
int Set_Ignition       ( const MailAddress& myAddress, Transmission_Packet& tp);
// Set_Ignition_Packet options:
// will start ignition if engine not started and tp.Enabled = true;
// will kill engine if engine running and tp.Enabled = false




// EStop packet (receive estop info)
//*************************************************************************************
struct EStop_Packet {
  char EStop; // can be off(o), pause(p), disable(d) -- Vmanage will take care of any delay,
              // so if estop = 'o' then you can assume that you are free to go.
};
// and a function to retrieve this information
int Get_EStop_Packet( const MailAddress& me, EStop_Packet &fillme);







// OBD-II packet (read info)
//*************************************************************************************
struct OBD_Packet {
  double Velocity, RPM;
  int OBDUpdateCounter;
  int valid; // valid = true if you can trust this data, otherwise false.
};
// and a function to retrieve this information
int Get_OBD_Packet( const MailAddress& me, OBD_Packet &fillme);
int Set_Keyboard_Pause( const MailAddress& me, int val ); // true for pause, false for "not-pause" or resume







/*  Packets for VDrive Devices
 *  Steer
 *  Throttle
 *  Brake
 */



// Steer packet
//*************************************************************************************
struct Steer_Packet {
  double Steer;           // value of steer [-1, 1]
  int SteerCmdCounter;    // how many times have we commanded the steering?
  int SteerState;         // One of "DeviceStates"

};
// and a function to retrieve this information
int Get_Steer_Packet( const MailAddress& me, Steer_Packet &fillme);
int Set_Steer_Packet(Steer_Packet &fillme);



// Throttle packet
//*************************************************************************************
struct Throttle_Packet {
  double Position;            // value of accel [0, 1]
  int ThrottleCmdCounter;     // how many times have we commanded the throttle?
  int ThrottleState;          // one of "DeviceStates"
};
// and a function to retrieve this information
int Get_Throttle_Packet( const MailAddress& me, Throttle_Packet &fillme);
int Set_Throttle_Packet(Throttle_Packet &fillme);



// Brake packet
//*************************************************************************************
struct Brake_Packet {
  int BrakeCase;          // what type of brake action are we doing
  int BrakeCmdCounter;    // how many times have we commanded the brake?
  double BrakePot;        // value of brake pot (actual position of brake).
  int BrakeState;         // one of "DeviceStates"
  double Position;	  // commanded position for the brake
  double BrakeVelocity;
  double BrakeAcceleration;
};
// and a function to retrieve this information
int Get_Brake_Packet( const MailAddress& me, Brake_Packet &fillme);
int Set_Brake_Packet( Brake_Packet &fillme);





/*  Packets for VRemote Devices
 *  Joystick
 */


// Joystick packet
//*************************************************************************************
struct Joystick_Packet {
  int X, Y, Buttons;          // x,y axis and buttons
  int count;  // how many times have we read the joystick
};
// and a function to retrieve this information
int Get_Joystick_Packet( const MailAddress& me, Joystick_Packet &fillme);
int Set_Joystick_Packet(Joystick_Packet &fillme);




// end of section 1.









//*************************************************************************************
//*************************************************************************************
//*************************************************************************************
//*************************************************************************************
//*************************************************************************************

/*
 * Section 2: Remote Data/Command Packets
 *            Packets designed so that a module can control features
 *            in another module, or get data from them.
 *            --VM_for_VD_Packet: VManage info for VDrive such as estop
 *                                and drive modes.
 *            --VM_EStop_Behavior_Packet: controls what happens on an estop
 *            --VState_Packet: Contains the usual state data.
 */


// VManage data that vdrive will want to request (in one convenient packet)
//*************************************************************************************
struct VM_for_VD_Packet { 

  char smode;         // steer mode.
                      // can be: off(o), auto(a), manual(m), remote(r)
  char amode;         // accel mode
  EStop_Packet estop; // defined above

  int calibrate[VDriveDevices::Num]; // flag set to 1 if we want to calibrate this device, 0 otherwise
                                     // i.e. if( calibrate[VDriveDevices::throttle] ) throttle_calibrate();

  double max_velo; // max velocity -- vmanage can use this to slow down the vehicle.
};
// and a function for Vdrive to get this packet
int Get_VM_for_VD_Packet( const MailAddress& me, VM_for_VD_Packet  &inserthere ); // returns < 0 for error otherwise 0
int Set_Steer_Mode( const MailAddress& me, char mode ); // returns true if it found VManage, otherwise false
int Set_Accel_Mode( const MailAddress& me, char mode ); // same



// VManage estop control (either slam on the brakes or provide const deceleration)
//*************************************************************************************
struct VM_EStop_Behavior_Packet {
  double deceleration_rate; // Const deceleration rate to use 
  int enable_for_estop;     // Enable Const Deceleration Mode if we get an real Estop pause
  int enable_for_keyboard;  // Enable Const Decel for a keyboard EStop.
                            // NOTE: If either of these is negative it will not change the 
                            // value currently set (so you can set one without setting the other).

};
int Set_VM_EStop_Behavior( const MailAddress& me, VM_EStop_Behavior_Packet& behavior );




// VState packet (get vstate info)
//*************************************************************************************
typedef VState_GetStateMsg VState_Packet;
/* -- Defined in VState.hh ----------
struct VState_GetStateMsg
{
  Timeval Timestamp;   // time stamp when GPS data is received

  double Easting;      // UTM easting in meters
  double Northing;     // UTM northing in meters
  float  Altitude;     // altitude in meters (from what reference?)

  double kf_lat;       // latitude from KF
  double kf_lng;       // longitude from KF

  float  Vel_E;        // east velocity in m/s
  float  Vel_N;        // north velocity in m/s
  float  Vel_U;        // up velocity in m/s

  float  Speed;        // m/s 
  double Accel;	       // m/s^2 

  float  Pitch;        // pitch in radians
  float  Roll;         // roll in radians
  float  Yaw;          // heading in radians, north=0 rads 
                       // clockwise increase
  
  // currently not implemented
  float  PitchRate;    
  float  RollRate;
  float  YawRate;

  // flags for sensors, 1 means they're up and running, 0 or -1 means they're inactive
  int gps_enabled;
  int imu_enabled;
  int mag_enabled;

  // gps pvt has a solution or not
  int gps_pvt_valid;

  struct gpsData gpsdata;	// raw GPS data
  struct imu_data imudata;	// raw IMU data
  struct mag_data magreading;   // raw magnetometer data
};
*/ // defined in VState.hh
// helper function to get VState packet
int Get_VState_Packet(const MailAddress& myAddress, VState_GetStateMsg & fillme);


// end of section 2.








//*************************************************************************************
//*************************************************************************************
//*************************************************************************************


/*
 * Section 3: Display Packets
 *            One packet for each module containing everything that VRemote
 *            could possibly want to display.  Note these are called stacks
 *            instead of packets.
 *            --VManage_Stack
 *            --VDrive_Stack
 *            --VState_Stack
 */


// Everything VManage could possibly want to display remotely 
struct VManageStack {

  VM_for_VD_Packet    vdrive;
  OBD_Packet          obd;
  VState_Packet       vstate;
  Transmission_Packet transmission;
  Ignition_Packet     ignition;
  //  EStop_Packet        estop; contained in VM_for_VD_Packet.

  Timeval             uptime;          // uptime of vmanage
};
// and the accessor function
int Get_VManageStack( MailAddress & ma, VManageStack& fill);
int Set_VManageStack( VManageStack& fill);



// Everything VDrive could possibly want to display remotely 
struct VDriveStack {

  VM_for_VD_Packet    vdrive;

  Steer_Packet        steer;
  Throttle_Packet     throttle;
  Brake_Packet        brake;
  Joystick_Packet     joystick;

  /* Internal variables that vremote might want access to */
  int mode_steer, mode_drive;
  int cruise_flag;

  Timeval uptime;                   // uptime of vmanage
};
// and the accessor function
int Get_VDriveStack( MailAddress & ma, VDriveStack& fill);
int Set_VDriveStack( VDriveStack& fill);


// Everything VState could possibly want to display remotely 
struct VStateStack {

  VState_Packet vstate;

  Timeval       uptime;                   // uptime of vstate
};
// and the accessor function
int Get_VStateStack( MailAddress & ma, VStateStack& fill);





// end of section 3.




#endif
