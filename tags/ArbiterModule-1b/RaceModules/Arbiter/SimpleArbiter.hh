#ifndef SIMPLEARBITER_HH
#define SIMPLEARBITER_HH

/* SimpleArbiter.hh

1/11/04 - Created from Arbiter by Ike
1/12/04 - Added Comments (Ike)
1/16/04 - More comments for your pleasure (Sue Ann)
2/26/04  Sue Ann Hong   Added constants used by vote-combining logic
3/4/04   Sue Ann Hong   Added GetArcNum(double phi)

*/

#include "Vote.hh"
#include <list>
#include <math.h>
#include <time.h>

#include <Constants.h>

using namespace std;

//---Constants: Arbiter should dictate these parameters----------//
//----Constants shared by all Voters-----------------------------//
const int    NUMARCS      = 25;             // will crash if numarcs < 2
// maximum steering angle in radians (25 degrees)
//const double MAX_STEER    = 25.0*M_PI/180.0;
const double MAX_STEER = USE_MAX_STEER;
const double LEFTPHI      = -1.0 * MAX_STEER;
const double RIGHTPHI     = MAX_STEER;
const double MAX_GOODNESS = 100.0;          // Max goodness of a Vote

//----Constants used in vote combing logic----------------------//
// NO_VOTE_VELO = initial value of the velocity in evaluation that will be
//  overwritten by any voter's velocity vote that's lower.
// MAX_SPEED=Defined as the max speed we want to go on this particular course
// Arbiter wants to drive at 100mph.
const double NO_VOTE_VELO = 50; 
// The goodness value indicating that the Voter's abstaining its Vote for the
// angle.
const double NO_CONFIDENCE = 102;
// The goodness value indicating that LADAR is resetting and hence not sending
// any intelligent votes.
const double LADAR_RESET = 103;
// Constants to indicate whether an arc has been vetoed by a module or not.
// The way veto values are defined lets us multiply different kinds of vetos
// for an arc to find out whether DFE and/or non-DFE modules have vetoed down
// an arc.
const int NO_VETO = 1;
//const int DFE_VETO = -1;
const int NORMAL_VETO = 0;
///---Tweak-able constants for vote combining/picking logic-----//
// Threshold under which goodness from a voter will be considered a veto vote.
const double GOODNESS_THRESHOLD = 5.0;
const double DFE_GOODNESS_THRESHOLD = 20.0;
// For DFE, we give a buffer to what the DFE actually sends us, since
// the velocity we get from the DFE are absolute minimum, just above the
// roll-over velocities.
const double DFE_VELO_THRESHOLD = 1.0;
// A buffer to stop the car from oscillating horribly when state jumps around.
// If the car moves around by 5 arcs left or right, then the difference
// could be up to 10. 
// TODO: DO THIS A LITTLE MORE INTELLIGENTLY
const int STREAK_SIZE_DIFF_THRESHOLD = 5;  


// A voter - every input will fill one of these up.
// A simple data structure with a copy operator
struct Voter {
  double VoterWeight;
  Vote Votes[NUMARCS];
  int ID;

  // Watch out when using this operator, since it overwrites the Voterweight
  // and Voters sent by planner modules don't know their own weights.
  // May want to update Voterweight using the ID and InputWeights[] in 
  // ArbiterModuleDatum.hh.
   Voter& operator= (const Voter rhs) {
    ID = rhs.ID;
    VoterWeight = rhs.VoterWeight;
    for( int i=0; i<NUMARCS; i++) {
      Votes[i] = rhs.Votes[i];
    }
  }
};

// desired commands sent to actuators
struct ActuatorCommand {
  double phi; // steering angle
  double vel; // velocity
};


struct ArbiterBestResult {
  Voter combined;
  ActuatorCommand cmd;
};


/* GetPhi(int arcnum): returns the steering angle for the (arcnum + 1)th arc.
 *   Helper function to determine phi based on left and right phi.
 *   Because NUMARCS is constant along with Left and Right Phi, we can
 *   determine the steering angle of a vote based on its index.
 *   (Votes[0] has angle of LeftPhi and Votes[NUMARCS-1] has angle RightPhi)
 *  arcnum ranges from 0 to NUMARCS-1),
 *  so arcnum=0 -> hard left,
 *     arcnum=NUMARCS-1 -> hard right
 */
double GetPhi(int arcnum);

/* GetArcNum(int arcnum): returns the arc number (index) for the input 
 *  steering angle (in radians). Inverse of GetPhi(int arcnum).
 * Returns -1 if the angle is out of range.
 */
int GetArcNum( double phi );

/* PickBest(list<Voter> voter_inputs, double lastPhi) :  
 *  Takes all Voters, combines their
 *  votes, picks the best arc based on the combined goodness values,
 *  and returns the best steering and velocity commands - i.e., calls
 *  CombineVotes() and PickBestCombinedArc() defined in SimpleArbiter.cc.
 *  Note: Instead of Arbiter Class we now have this one function.
 *  Input: An arbitrary list of voters (each voter has NUMARCS votes)
 */
ArbiterBestResult PickBest( list<Voter> voter_inputs, double lastPhi);


#endif
