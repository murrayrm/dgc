#include <ladar/ladarSource.hh>

int main(int argc, char *argv[]) 
{
  ladarSource mySource;
  mySource.init(ladarSource::SOURCE_LADAR, "config/ladar/LadarCal.ini.riegl");
  for(int i=0; i<20; i++) 
  {
    fstream logfile("temp.pts", fstream::out);
    for(int i=0; i<20; i++) 
    {
      fstream logfile("temp.pts", fstream::out);

      printf("Grab %d\n", i);
      mySource.scan();
      printf("Got %d points\n", mySource.numPoints());
      for(int i=0; i<mySource.numPoints(); i++) 
      {
        logfile << mySource.UTMPoint(i).X << " " 
                << mySource.UTMPoint(i).Y << " " 
                << mySource.UTMPoint(i).Z << endl;
      }
      logfile.close();

    }
  }
  mySource.stop();
  return 0;
}

