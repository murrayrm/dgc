#ifndef STAGE_ITERATOR_HH
#define STAGE_ITERATOR_HH

//Defines a class that tracks the stores an integer denoting the NEXT
//strategy stage to be executed, AND another integer which increments
//after each execution of the CURRENT strategy stage (the latter is 
//used in conjunction with persist-count #defines to determine whether
//to resend a command/control message sent by (the) previous strategy

class stage_iterator
{

public:
  
  /* CONSTRUCTORS  */
  stage_iterator() : nextStage_cnt(1), currentStageCount_cnt(0) {}

  //Increment the nextStage counter, as the current stage is complete
  //note that this also clears the stage counter (as the current stage
  //is being left, and hence how many times it has been repeatedly
  //looped through is now unimportant)
  void incrStage() {
    nextStage_cnt++;
    currentStageCount_cnt = 0;
  }  

  //Increment the current stage loop counter (do NOT update the
  //nextStage counter, as we are NOT moving to a new stage
  void incrCnt() {
    currentStageCount_cnt++;
  }

  void setStage( int newStage ) {
    nextStage_cnt = newStage;
  }
  
  void setCnt( int newCnt ) {
    currentStageCount_cnt = newCnt;
  }

  void reset() { nextStage_cnt = 1; currentStageCount_cnt = 0; }
  int nextStage() { return nextStage_cnt; }
  int currentStageCount() { return currentStageCount_cnt; }

  
private:
  
  //The NEXT stage in the (current) strategy to be executed
  //Stage #'s START at 1 (i.e. when
  //entering a new strategy nextStage = 1) - this is stored internally
  //by any strategy class instance so the information does not have to
  //be passed in stepForward(...)
  int nextStage_cnt;
  
  //The # of times the CURRENT stage in the (current) strategy has
  //been looped through (used in conjunction with persistence-count
  //#defines to determine whether we are 
  int currentStageCount_cnt;
  
};

#endif //STAGE_ITERATOR_HH
