#ifndef INTERFACE_SUPERCON_FMAP_HH
#define INTERFACE_SUPERCON_FMAP_HH

//FILE DESCRIPTION: Header file that contains the definitions of data structures that are
//used in direct communication between SuperCon and fusionMapper & #defines associated
//with their communications

//NOTE: superCon map deltas are sent using the SNsuperConMapDelta message type

namespace sc_interface {

/* Use scMessages */
#define FMAP_RESPONSE_LIST(_) \
  _( clear_maps_complete, = 0 ) \
  _( broaden_rddf_complete, )
DEFINE_ENUM(scMsgID_FMP, FMAP_RESPONSE_LIST)


/* Use the standard skynet message type: SNsuperConMapAction */
#define MAP_ACTION_CMDS_LIST(_) \
  _( clear_all_maps_everywhere, = 0 ) \
  _( broaden_rddf_one_map, )
DEFINE_ENUM(mapActionCmdType, MAP_ACTION_CMDS_LIST)

  /* struct passed in the message type SNsuperConMapAction
   *
   * dblArg key:
   * mapActionCmd = clear_all_maps_everywhere --> dblArg = NOT USED
   * mapActionCmd = broaden_rddf_one_map --> dblArg = distance IN METERS to increase the width
   *                (measured from the centerline of the RDDF to one side (assumed symmetry))
   *                of the RDDF by.
   */
  struct superCon_map_action {
    mapActionCmdType mapActionCmd;    
    double dblArg;
  };

}

#endif
