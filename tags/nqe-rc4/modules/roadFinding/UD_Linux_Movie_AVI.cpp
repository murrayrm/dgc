//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_Linux_Movie_AVI.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

UD_Movie *open_UD_Movie(char *filename)
{
  int movie_type;
  UD_Movie *mov;

  //  printf("%s\n", filename);

  // get filename

  movie_type = determine_type_UD_Movie(filename);
  if (movie_type == UD_MOVIE_AVI)
    mov = read_avi_to_UD_Movie(filename);
  else 
    UD_error("can't yet handle that kind of movie");

  printf("opened %s; w = %i, h = %i\n", filename, mov->width, mov->height);

  // set up

  mov->name = (char *) calloc(UD_MAX_FILENAME, sizeof(char));
  strcpy(mov->name, filename);

  mov->currentframe = 0;

  // finish

  return mov;
}

//----------------------------------------------------------------------------

// free resources and close file

void close_UD_Movie(UD_Movie *mov)
{
  int len;

  UD_error("no close");

//xxx   if (mov->type == UD_MOVIE_AVI) {
//xxx     AVIFileRelease(mov->AVI_pAviFile);    
//xxx   }

  cvReleaseImage(&mov->im);
  //  free_UD_Image(mov->im);
  free(mov->name);
  free(mov);
}

//----------------------------------------------------------------------------

int determine_type_UD_Movie(char *filename)
{
  char suffix[4];
  int len;

  len = strlen(filename);
  suffix[0] = filename[len - 3];
  suffix[1] = filename[len - 2];
  suffix[2] = filename[len - 1];
  suffix[3] = '\0';

  if (!strcmp(suffix, "avi") || !strcmp(suffix, "AVI"))
    return UD_MOVIE_AVI;

  // Unknown

  else 
    return UD_UNKNOWN;
}

/*
//----------------------------------------------------------------------------

// rate is frames per second

UD_Movie *write_avi_initialize_UD_Movie(char *filename, int width, int height, int rate)
{
  UD_Movie *mov;
  AVISTREAMINFO streaminfo;
  int buffsize;
  AVICOMPRESSOPTIONS opts;
  AVICOMPRESSOPTIONS FAR *aopts[1] = {&opts};

  // initialize movie

  mov = (UD_Movie *) malloc(sizeof(UD_Movie));
  mov->type = UD_MOVIE_AVI;
  mov->interleaved_DV = FALSE;

  mov->AVI_pAviFile = NULL;
  mov->AVI_pVideo = NULL;
  mov->compressed_AVI_pVideo = NULL;

  // allocate space for an image-sized DIB

  buffsize = 3 * width * height;

  mov->pBmpInfoHeader = (LPBITMAPINFOHEADER) malloc(sizeof(BITMAPINFOHEADER) + buffsize);

  mov->pBmpInfoHeader->biSize = sizeof(BITMAPINFOHEADER);
  mov->pBmpInfoHeader->biWidth = width;
  mov->pBmpInfoHeader->biHeight = height;
  mov->pBmpInfoHeader->biPlanes = 1;
  mov->pBmpInfoHeader->biBitCount = 24;
  mov->pBmpInfoHeader->biCompression = 0;  // same as BI_RGB?
  mov->pBmpInfoHeader->biSizeImage = buffsize;
  mov->pBmpInfoHeader->biClrUsed = 0;
  mov->pBmpInfoHeader->biClrImportant = 0;

  // initialize AVI file

  AVIFileInit();

  //  if (AVIFileOpen(&mov->AVI_pAviFile, filename, OF_WRITE | OF_CREATE, NULL))

  if (!filename) {  // get filename from user later; use temporary name for now
    if (AVIFileOpen(&mov->AVI_pAviFile, "temp.avi", OF_CREATE, NULL))
      UD_error("write_avi_initialize_UD_Movie(): error opening avi file");
    mov->need_filename = TRUE;
  }
  else {
    if (AVIFileOpen(&mov->AVI_pAviFile, filename, OF_CREATE, NULL))
      UD_error("write_avi_initialize_UD_Movie(): error opening avi file");
    mov->need_filename = FALSE;
  }

  // start video stream

  memset(&streaminfo, 0, sizeof(AVISTREAMINFO));
  streaminfo.fccType = streamtypeVIDEO;
  streaminfo.fccHandler = 0;
  streaminfo.dwScale = 1;
  streaminfo.dwRate = rate;
  streaminfo.dwSuggestedBufferSize = buffsize;
  SetRect(&streaminfo.rcFrame, 0, 0, width, height);

  if (AVIFileCreateStream(mov->AVI_pAviFile, &mov->AVI_pVideo, &streaminfo))
    UD_error("write_avi_initialize_UD_Movie(): error initializing video stream");

  // write video stream info (including compression)

  memset(&opts, 0, sizeof(opts));

  // keep putting up compression dialog box until user picks a codec
  //  while (!AVISaveOptions(NULL, 0, 1, &mov->AVI_pVideo, (LPAVICOMPRESSOPTIONS FAR *) &aopts));

  // automatically choose no compression at all (since other codecs seem not to work)

  memset(&opts, 0, sizeof(AVICOMPRESSOPTIONS));
  opts.dwFlags = 8;

  if (AVIMakeCompressedStream(&mov->compressed_AVI_pVideo, mov->AVI_pVideo, &opts, NULL))
    UD_error("write_avi_initialize_UD_Movie(): error making compressed stream");

  if (AVIStreamSetFormat(mov->compressed_AVI_pVideo, 0, mov->pBmpInfoHeader, mov->pBmpInfoHeader->biSize))
    UD_error("write_avi_initialize_UD_Movie(): error setting stream format");

  return mov;
}

//----------------------------------------------------------------------------

void write_avi_addframe_UD_Movie(int framenumber, UD_Image *im, UD_Movie *mov)
{
  // convert to UD_Image
  
  //  fprintf((FILE *) mov->AVI_pVideo, "hello");

  ip_convert_UD_Image_to_DIB(im, mov->pBmpInfoHeader);
//   ip_convert_DIB_to_UD_Image(mov->pBmpInfoHeader, im);
//   write_UD_Image("xxx.ppm", im);
//   UD_exit(1);

  // write it
  
  //  if (AVIStreamWrite(mov->AVI_pVideo, framenumber, 1, (unsigned char *) (mov->pBmpInfoHeader + mov->pBmpInfoHeader->biSize), mov->pBmpInfoHeader->biSizeImage, AVIIF_KEYFRAME, NULL, NULL))
  if (AVIStreamWrite(mov->AVI_pVideo, framenumber, 1, (LPBYTE) mov->pBmpInfoHeader + mov->pBmpInfoHeader->biSize, mov->pBmpInfoHeader->biSizeImage, AVIIF_KEYFRAME, NULL, NULL))
    UD_error("write_avi_addframe_UD_Movie(): error writing to video stream");
}

//----------------------------------------------------------------------------

void write_avi_finish_UD_Movie(UD_Movie *mov)
{
  char *filename;
  char mvstr[128];
  char tempfilename[128] = "//C/Documents/software/fusion/temp.avi";

  AVIStreamClose(mov->AVI_pVideo);
  AVIStreamClose(mov->compressed_AVI_pVideo);
  AVIFileClose(mov->AVI_pAviFile);

  // maybe do this before we start recording?  it would be so much easier
  if (mov->need_filename) {
//     filename = choose_save_file_dialog_win32("Microsoft AVI File (*.avi)\0\0");
//     printf("filename = %s\n", filename);
//     sprintf(mvstr, "mv %s %s\n", tempfilename, filename);
//     printf("%s\n", mvstr);
//     //    system(mvstr);
  }
}
*/

//----------------------------------------------------------------------------

UD_Movie *read_avi_to_UD_Movie(char *filename)
{
  UD_Movie *mov;

  // initialize movie

  mov = (UD_Movie *) malloc(sizeof(UD_Movie));
  mov->type = UD_MOVIE_AVI;
  mov->interleaved_DV = FALSE;

  mov->AviFile = CreateIAviReadFile(filename);
  mov->AviStream = mov->AviFile->GetStream(0, AviStream::Video);

  mov->info = mov->AviStream->GetStreamInfo();

  // allocate space for current image

  mov->AviStream->GetVideoFormat(&mov->bh, sizeof(mov->bh));

  //  printf("w = %i, h = %i\n", mov->bh.biWidth, mov->bh.biHeight);
  printf("AVI: w = %i, h = %i, n = %i\n", mov->info->GetVideoWidth(), mov->info->GetVideoHeight(), mov->info->GetStreamFrames());
  
  mov->width = mov->info->GetVideoWidth();
  mov->height = mov->info->GetVideoHeight();
  mov->im = cvCreateImage(cvSize(mov->width, mov->height), IPL_DEPTH_8U, 3);
  mov->temp_im = cvCreateImage(cvSize(mov->width, mov->height), IPL_DEPTH_8U, 3);

  // length of movie?

  mov->numframes = mov->info->GetStreamFrames();
  mov->currentframe = mov->firstframe = 0;
  mov->lastframe = mov->numframes - 1;

  mov->AviStream->StartStreaming();

  // load first image in

  get_avi_frame_UD_Movie(mov, mov->currentframe);

  // return

  return mov;
}

//----------------------------------------------------------------------------

// ignores framenumber--just gets next frame from stream

void get_avi_frame_UD_Movie(UD_Movie *mov, int framenumber)
{
  CImage *cim;

  cim = mov->AviStream->GetFrame(true);

//   if (cim->Format() != 24)    // I think this means the image is RGB, so swap to keep OpenCV happy
  cim->ByteSwap();

  // I believe right now the images are coming in RGB, so color-specific OpenCV functions (which assume BGR) may break

  if (cim->Direction()) {    // this means the image is upside-down
    mov->temp_im->imageData = (char *) cim->m_pPlane[0];
    cvConvertImage(mov->temp_im, mov->im, CV_CVTIMG_FLIP);
  }
  else  
    mov->im->imageData = (char *) cim->m_pPlane[0];

  // only do this for UD_Linux_Image, not UD_OpenCV_Image
  /*

  if (cim->Format() == 24)    // this means the image is BGR
    cim->ByteSwap();

  mov->im->data = cim->m_pPlane[0];
  if (cim->Direction())
    vflip_UD_Image(mov->im);  // flip mov->im vertically
  */

  mov->currentframe++;
}

//----------------------------------------------------------------------------

void get_frame_UD_Movie(UD_Movie *mov, int framenumber)
{
  if (mov->type == UD_MOVIE_AVI)
    get_avi_frame_UD_Movie(mov, framenumber);
  else
    UD_error("get_frame_UD_Movie(): unknown movie type");
}

//----------------------------------------------------------------------------

// constructs a filename of the form "<prefix>num<suffix>", where num
// is formatted to occupy n digits, padded with leading 0's if necessary.
// num must be positive

char *construct_n_digit_filename(int numdigits, int num, char *prefix, char *suffix)
{
  char *result, *zeroes;
  int numzeroes, totlen, i;

  if (num < 0 || num > pow(10, numdigits) - 1) {
    UD_flush_printf("%i\n", num);
    UD_error("construct_n_digit_filename(): number out of range");
  }

  //  printf("pre len = %i, suf len = %i\n", strlen(prefix), strlen(suffix));

  totlen = strlen(prefix) + strlen(suffix) + numdigits + 1;
  result = (char *) calloc(totlen, sizeof(char));
  numzeroes = numdigits - (int) floor(log10((float) num)) - 1;

  if (numzeroes) {
    zeroes = (char *) calloc(numzeroes, sizeof(char));
    for (i = 0; i < numzeroes; i++)
      zeroes[i] = '0';

    sprintf(result, "%s%s%i%s", prefix, zeroes, num, suffix);

    free(zeroes);
  }
  else {

    //  UD_flush_printf("prefix = %s, num = %i, suffix = %s, totlen = %i\n", prefix, num, suffix, totlen);
  //  exit(1);

    sprintf(result, "%s%i%s", prefix, num, suffix);
  }

  //  UD_flush_printf("result = %s\n", result);
  //  exit(1);

  //  free(result);

  return result;
}

//----------------------------------------------------------------------------

// constructs a filename of the form "<prefix>num<suffix>", where num
// is formatted to occupy 3 digits, padded with leading 0's if necessary

char *construct_3_digit_filename(int num, char *prefix, char *suffix)
{
  char *result;

  if (num < 0 || num > 999) {
    UD_flush_printf("%i\n", num);
    UD_error("construct_3_digit_filename(): number out of range");
  }

  //  printf("pre len = %i, suf len = %i\n", strlen(prefix), strlen(suffix));

  result = (char *) calloc(strlen(prefix) + strlen(suffix) + 3, sizeof(char));

  if (num < 10)
    sprintf(result, "%s00%i%s", prefix, num, suffix);
  else if (num < 100)
    sprintf(result, "%s0%i%s", prefix, num, suffix);
  else
    sprintf(result, "%s%i%s", prefix, num, suffix);

  return result;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
           
