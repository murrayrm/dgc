This is the traj directory of Team Caltech's DGC repository.  It is useful for 
storing and manipulating discrete implementations of trajectories in UTM 
coordinates, which in general have first and second (or possibly more) 
derivatives associated with each point on the trajectory.

The main class represented here is CTraj.  In our official terminology (which 
is sometimes abused), a /path/ is just a sequence of UTM coordinates (typically 
a discrete implementation of a twice differentiable parameterized curve in 2D).  
A /trajectory/ is a generalization of a path in which velocity (first 
derivative) and acceleration (second derivative) vectors are assigned at each UTM
coordinate.

Documentation for the CTraj class is available at 
http://gc.caltech.edu/doc/dgc-api/html/classCTraj.html

To compile: "make"
To install: "make install"
To test: ./test
