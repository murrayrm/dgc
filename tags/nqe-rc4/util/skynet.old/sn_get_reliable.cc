#include "sn_msg.hh"
#include <string.h>
#include <stdlib.h>
#include <memory.h>
#define MAX_DATALEN 256000

int main(int argc, char** argv)
{
  int sn_key;
  char* default_key = getenv("SKYNET_KEY");
  if (2 == argc)
  {
    sn_key  = atoi(argv[1]);
  }
  else if (default_key != NULL )
  {
    sn_key = atoi(default_key);
  }
  else
  {
    printf("usage: sn_get_reliable key\nDefault key is $SKYNET_KEY\n");
    exit(2);
  }

  modulename myself = SNfusionmapper;
  modulename sender = ALLMODULES;
  sn_msg myinput = SNpointcloud;
  void* databuf = malloc(MAX_DATALEN);
  bzero(databuf, MAX_DATALEN); 
  skynet Skynet(myself, sn_key);

  int sock1 = Skynet.listen(myinput, sender);
  unsigned int datalen = Skynet.get_msg(sock1, databuf, MAX_DATALEN, 0);

  printf("got msg.  datalen= %u\n", datalen);
  
  /*print data*/
  /*uint i;
  for(i = 0; i < datalen / sizeof(int); i++)
  {
    printf("%u\t",*((int*)databuf) + i);
  }
  printf("\n");*/
  free(databuf);
  return 0;
}
