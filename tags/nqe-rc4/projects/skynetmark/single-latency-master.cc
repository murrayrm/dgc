#include <sn_msg.hh>
#include <sys/time.h>
#include <iostream>
#include <fstream>
#include "DGCutils"
#include <math.h>

#define us_p_s  1000000 // 4294967296

using namespace std;
int main(int argc, char** argv)
{
  int key; 
  unsigned int msg_size;
  //double msg_rate;  //Bytes per second
  unsigned int testlen;
  if (argc < 4)
  {
    cerr << "This program should only be called from snm-main.sh" << endl;
    cerr << "usage: single-latency-master key msg_size time" << endl;
    cerr << "key is the skynet key, ";
    cerr << "msg_size is in bytes, ";
    cerr << "rate is bytes per second, time is test time in seconds" << endl;
      
    exit(2);
  }
  else
  {
    key = atoi(argv[1]);
    msg_size = atoi(argv[2]);
    testlen = atoi(argv[3]); 
    //cout << key << " " << msg_size << " " << msg_rate << " " << testlen << endl ;
  }
  unsigned long long int starttime, mytime;
  skynet Skynet(MODmark, key);
  int send_chan = Skynet.get_send_sock(SNmark1);
  int recv_chan = Skynet.listen(SNmark2, ALLMODULES);
  char buffer[msg_size];
  buffer[0] = 0;                            //continue character
  unsigned long long int newmytime;
  unsigned long long int echotime;

	double echotimesec;
  double sum, sumsq, mean, stddev, max;
  sum = 0;
  sumsq = 0;
  max = 0;
  
  unsigned int n = 0;

	ofstream dat("dat");
  DGCgettime(starttime);
  do
  {
		buffer[0] = (n % 100);
    DGCgettime(mytime);
    Skynet.send_msg(send_chan, buffer, msg_size, 0); //send ping
    Skynet.get_msg(recv_chan, buffer, msg_size, 0);  //get echo
    DGCgettime(newmytime);
    echotime = newmytime - mytime;              //latency

		echotimesec = DGCtimetosec(echotime);
    sum += echotimesec;
    sumsq += echotimesec*echotimesec; 
    max = max>echotimesec ? max : echotimesec;
    dat << echotimesec << endl;
    n++;
  } while((newmytime - starttime) < testlen * 1000000);
  mean = sum/n;
  stddev = sqrt((sumsq - n * mean*mean) / (n - 1));       //divide by n-1 gives unbiased estimator
  double totaltime = (double)(newmytime - starttime)/1000000;
  //now kill the slave.  Send multiple kill signals
  buffer[0] = 120;                          //kill character

  usleep(1000);                             //wait a while
  Skynet.send_msg(send_chan, buffer, 1, 0); //send 1 char

  //cerr << "messages= " << n << "\ttime= " << totaltime ;
  double real_rate = (double)(n * msg_size) / totaltime;
  //cerr << "\trate= " << real_rate << endl;
  cout << n << " " << real_rate << " " << totaltime << " " << mean*1e3 << " " << stddev*1e3 << " " << max*1e3 << endl; 
  return 0;
}
