/**
 * RingMonitor.cc
 * Revision History:
 * 09/08/05 hbarnor created
 * Networking code based heavily on examples in unix network
 * programming by W. Richard Stevens.
 * $Id$
 */

#include "RingMonitor.hh"

//broadcast IP address
const char * RingMonitor::BCAST_IP="192.168.0.255";
const char * RingMonitor::RING_CONFIG="ring.cfg";

RingMonitor::RingMonitor()  
{

  //set up signal and timer to handle heartbeat failure
  p_resuscitate = new sigc::signal<void>();
  p_heartMonitor = new DGCTimer(HEART_PERIOD*1000,p_resuscitate);
  p_resuscitate->connect( sigc::mem_fun(this,&RingMonitor::checkHeartBeat) );  
  // clear the current heartbeat 
  bzero(&m_currentHeartBeat, sizeof(m_currentHeartBeat));
  //initialize the random number generator
  srand( (unsigned)time( NULL ) );
  //masterRing();
  // get the local IP address
  setLocalIP(&m_localAddr);
  // build an array representation of ring
  // and check if we are in the ring, exit otherwise
  if(!buildRingArray())
    {
      cerr << " I am not in ring. \n Exiting ... " << endl;
      exit(1);
    }  
  // start leader server thread
  DGCstartMemberFunctionThread(this, &RingMonitor::processWhoIs);
  // send whois 
  sendWhoIs();
  // initialize heart beat to my info.
  bcopy(&m_localAddr, &m_currentHeartBeat.addr, sizeof(m_localAddr));
  m_currentHeartBeat.mesgType = HEARTBEAT;
  p_myHeart = new HeartBeat::HeartBeat(m_localIP, &m_currentHeartBeat);
  int retVal;
  p_heartMonitor->start();
  while(true)
    {
      if((retVal = brothersKeeper()) == HB_EOF)
	{
	  cerr << " Do an ICMP check " << endl;
	  cerr << "Signal leader for wake " << endl;
	}
      sleep(2);
    }
}

RingMonitor::~RingMonitor()
{
  close(heartBeat_fd);
  cout << " Closing fd" << endl;
  delete p_myHeart;
  delete p_heartMonitor;
  delete p_resuscitate;
}



bool RingMonitor::sendWhoIs()
{
  int sockfd; 
  struct sockaddr_in servaddr;
  //clear the address structure 
  bzero(&servaddr, sizeof(servaddr));
  //set the protocol
  servaddr.sin_family = AF_INET;
  //set the 
  servaddr.sin_port = htons(WHOIS_PORT);
  // WARN: check for error return 
  int err = inet_pton(AF_INET, BCAST_IP, &servaddr.sin_addr);
  if(err < 1)
    {
      cout << "Presentation to network error" << endl;
      return false;
    }
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  // variable use to turn on options 
  const int on = 1;
  // enable broadcasts on socket level
  setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on));  
  // create a WIL message
  struct pmRmMesg mesg1;
  mesg1.mesgType = WIL;
  memcpy(&mesg1.addr, &m_localAddr, sizeof(mesg1.addr));
  //setLocalIP(&mesg1.addr);
  mesg1.addr.sin_port = htons(VOTE_PORT);
  sendto(sockfd,&mesg1 , sizeof(mesg1), 0, (struct sockaddr *) &servaddr, sizeof(servaddr));
  return true;
}


void RingMonitor::processWhoIs()
{
  int sockfd;
  struct sockaddr_in servaddr;
  struct sockaddr_in cliaddr;
  struct timeval timeout;
  // set timeout for 2*INDEX in chain seconds
  timeout.tv_sec = 2*(m_Position+1);
  timeout.tv_usec = 0;
  
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  //set timeout option on socket
  setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));
  
  bzero(&servaddr, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servaddr.sin_port = htons(VOTE_PORT);
  
  bind(sockfd, (struct sockaddr *) &servaddr,sizeof(servaddr));
  struct pmRmMesg mesg;
  socklen_t len;
  int recvStatus;
  do
    {
      recvStatus = recvfrom(sockfd, &mesg, sizeof(mesg), 0, (struct sockaddr *) &cliaddr, &len);
      if( recvStatus < 0)
	{
	  if(errno == EWOULDBLOCK)
	    {
	      cout << " I am leader " << endl;
	      //DGCstartMemberFunctionThread(this, &RingMonitor::ringMaster);
	      while(true)
		{
		  sleep(10);
		  cout << " iteration " << endl;
		}
	    }
	  else
	    {
	      cout << "receive error " << endl;
	    }
	}
      else
	{
	  char str[INET_ADDRSTRLEN];
	  const char * ptr;
	  ptr = inet_ntop(mesg.addr.sin_family, &(mesg.addr.sin_addr), str, sizeof(str));
	  cout << "Message received is: " << mesg.mesgType << " : " << str<< endl;
	  
	}
    }
  while(true);
}

void RingMonitor::setLocalIP(struct sockaddr_in * localAdd)
{
  struct hostent *hptr;
  struct utsname myname;
  if(uname(&myname) < 0)
    {
      cerr << " Uname failed " << endl;
      return ;
    }
  
#ifdef DEBUG
  cout << "sysnane : " << myname.sysname << ", ";
  cout << "nodename : " << myname.nodename << ", ";
  cout << "machine : " << myname.machine << endl;
#endif

  if ( (hptr = gethostbyname(myname.nodename)) == NULL)
    {
      cerr << " Get host-byname failed " << endl;
      return ;
    }
  bzero(localAdd, sizeof(struct sockaddr_in));
  localAdd->sin_family = hptr->h_addrtype;
  struct in_addr **pptr = (struct in_addr **) hptr->h_addr_list;
#ifdef DEBUG
  struct in_addr **pptr2 = (struct in_addr **) hptr->h_addr_list;
  char str2[INET_ADDRSTRLEN];
  const char * ptr2;
  while(*pptr2 != NULL)
    {        
      ptr2 = inet_ntop(hptr->h_addrtype,*pptr2, str2, sizeof(str2));
      cout << "Ip is " << str2 << endl;
      if(!ptr2)
	{
	  cerr << " Failed to determine string version of Local-IP. Continuing ... " << endl;
	} 
      pptr2++;
    }
#endif  
  //this is kinda hackish 
  // i am assuming that the first IP is the 127.0.0.1 and that I
  // always want the second one.
  //memcpy(&(localAdd->sin_addr), *++pptr, sizeof(struct in_addr));  
  memcpy(&(localAdd->sin_addr), *++pptr, sizeof(struct in_addr));  
  
  char str[INET_ADDRSTRLEN];
  const char * ptr;
  ptr = inet_ntop(localAdd->sin_family, &(localAdd->sin_addr), str, sizeof(str));
  m_localIP.assign(str);
#ifdef DEBUG
  cout << " Str is " << str;
  cout << " Local IP is " << m_localIP << endl;
#endif
  if(!ptr)
    {
      cerr << " Failed to determine string version of Local-IP. Continuing ... " << endl;
    }
}

void RingMonitor::ringMaster()
{
  leaderServer();
  
}

void RingMonitor::leaderServer()
{
  int error;
  int sockfd;
  struct sockaddr_in servaddr;
  struct sockaddr_in cliaddr;
    
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
   
  bzero(&servaddr, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servaddr.sin_port = htons(VOTE_PORT);
  
  error = bind(sockfd, (struct sockaddr *) &servaddr,sizeof(servaddr));   
  if( error < 0 )
    {
      cerr << " Please server is not working " << endl;
      if(errno == EADDRINUSE)
	{
	  cerr << "somebody has a lock on this addr" << endl;
	}
    }
  socklen_t len;
  //leaderServer(sockfd, (struct sockaddr *) &cliaddr, &len);


  struct pmRmMesg myReceived;
  while(true)
    {
      cout << "listening " << endl;
      error = recvfrom(sockfd, &myReceived, sizeof(myReceived), 0, (struct sockaddr *) &cliaddr, &len);
      if( error < 0)
	{
	  cerr << " leaderServer: Error in receiving datagram. " << endl; 
	}
      switch(myReceived.mesgType)
	{
	case WIL:
	  cout << "Received WIL message" << endl;
	  break;
	case IAL: 
	  cout << "asas Plaakdka da" << endl;
	  break;
	default:
	  cout << " Def " << endl; 
	}
    }      
} 

bool RingMonitor::buildRingArray()
{
  bool found = false;
  ifstream inFile(RING_CONFIG);
  if(! inFile)
    {
      cerr << " Error opening ring config file: " << RING_CONFIG << endl;
      cerr << " Exiting ... " << endl;
      exit(1);
    }
  string line;
  int n = 0;
  while (! inFile.eof() )
    {
      getline(inFile, line, '\n');
      if( (!line.empty()) && (line[0] != '#') ) // # at beginning of line denotes comment
	  {
#ifdef DEBUG
	    cout << "Line " << n << ": " << line <<  " : " << line.size() <<endl;
#endif 
	    if( n < MAX_RING_SIZE)
	      {
		m_ringArray[n] = line;
		if(line.compare(m_localIP) == 0)
		  {
		    m_Position = n;
		    found = true;
#ifdef DEBUG		    
		    cout << "found it " << endl;
#endif
		  }
		n++;
	      }
	    else
	      {
		cerr << "Ring limit achieved. Please either increase limit or decrease ring size" << endl; 
	      }	    
	  }
    }
  //clean up
  inFile.close();

#ifdef DEBUG
  cout << " ring is " << endl;
  for(int i = 0; i < MAX_RING_SIZE; i++)
    {
      cout << "Ring at " << i << " : " << m_ringArray[i].c_str() << endl;
    }
#endif
  // error checking 
  if(n < 1)
    {
      cerr << " Warning: Ring is empty " << endl;
    } 
  ringTotal = n;
  return found;
}


int RingMonitor::brothersKeeper()
{
  //int sockfd;
  do
    {  
      heartBeat_fd = makeTCPConnection(nextInRing());
    }
  while(heartBeat_fd < 0);

  ssize_t bRead;
  do
    {
      if( (bRead = readData(heartBeat_fd, &m_currentHeartBeat, sizeof(struct pmRmMesg))) < 0)
	{
	  //#ifdef DEBUG
	  switch(bRead)
	    {
	    case HB_EOF:
	      cerr << " File Descriptor closed. Start rejoin ring procedure." << endl;	  
	      break;
	    default:
	      cerr << " Read error on client. Incomplete heartbeat. " << endl;
	    }
	  //#endif DEBUG
	}
      else
	{
	  switch(m_currentHeartBeat.mesgType)
	    {
	    case HEARTBEAT:
	      p_heartMonitor->reset();
	      displayHeartBeat();
	      break;
	    default:
	      cout << " Received not a heartbeat" << endl;
	    }
	}
    }
  while(bRead != HB_EOF);
  return bRead;
}

int RingMonitor::makeTCPConnection(string ip)
{
#ifdef DEBUG
  cout << " Trying to connect to: " << ip << endl;
#endif  
  int sockfd;
  // address of server i am connecting to
  struct sockaddr_in servAddr; 
  // get a socket for the connection
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  // clear the server struct 
  bzero(&servAddr, sizeof(servAddr));
  servAddr.sin_family = AF_INET;
  servAddr.sin_port = htons(HEARTBEAT_PORT);
  int err = inet_pton(AF_INET, ip.c_str(), &servAddr.sin_addr);
  if(err != 1)
    {
      cerr << "Error setting server IP: " << ip << endl;
    }
  err = connect(sockfd, (struct sockaddr *) &servAddr, sizeof(servAddr));
  if(err == -1)
    {
      if(errno == ECONNREFUSED)
	{
	  cerr << "No server running on host:  "  << ip << "  Add code to use ICMP to test liveness.  " << endl;
	  usleep(5000000); // sleep for 5 seconds 
	  return -1;
	}
      if(errno == EHOSTUNREACH)
	{
	  cerr << " Host " << ip << " is not alive. " << endl;
	  usleep(5000000); // sleep for 5 seconds 
	  return -1;
	}
    }
  return sockfd;
}

string RingMonitor::nextInRing()
{
  static int curNext = m_Position;
#ifdef DEBUG
  cout << "curNext is: " << curNext << endl;
#endif 
  if(curNext+1 < ringTotal)
    {
      curNext++;
    }
  else
    {
      curNext = 0;
#ifdef DEBUG
  cout << "curNext reset to: " << curNext << endl;
#endif 
    }
  if(curNext == m_Position)
    {
#ifdef DEBUG
      cout << "curNext is myself. Incrementing ... next: " << curNext << endl;
#endif 
      return nextInRing();
    }
  return m_ringArray[curNext];
}

ssize_t RingMonitor::readData(int filedes, void * data, size_t nbytes)
{
  size_t nleft = nbytes;
  ssize_t nread;
  char * ptr = (char *)data;
  while(nleft > 0 )
    {
      if( (nread=read(filedes, ptr, nleft)) < 0 )
	{
	  if(errno == EINTR)
	    {
	      nread = 0;
	    }
	  else
	    {
	      return -1;
	    }
	}
      else
	{
	  if(nread == 0 )
	    {
	      return HB_EOF; //EOF
	    }
	}
      nleft -= nread;
      ptr += nread;
    }
  return (nbytes - nleft);
}

void RingMonitor::checkHeartBeat()
{
  switch(m_currentHeartBeat.mesgType)
    {
    case HEARTBEAT:
      
      break;
    default:
      cout << " received not a heartbeat" << endl;
    }
  cout << " Calling check heartbeat" << endl;
}

void RingMonitor::displayHeartBeat()
{
  char hostName[NI_MAXHOST];
  char service[NI_MAXSERV];
  getnameinfo((struct sockaddr *)&m_currentHeartBeat.addr, sizeof(m_currentHeartBeat.addr), hostName, NI_MAXHOST, service, NI_MAXSERV, 0);
  cerr << "Heartbeat is from " <<  hostName << " " << service << endl;
}
