/*
 * vdrive - vehicle driving program
 *
 * Richard M. Murray
 * 30 Dec 03
 *
 * This program is a simple front end for the vehlib library.  It allows
 * you to control the vehicle through the joystick or remotely and displays
 * the vehicle state.
 */

#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"
#include "sparrow/channel.h"
#include "sparrow/flag.h"
#include "sparrow/keymap.h"
#include "joystick.h"
#include "gps.h"
#include "imu.h"
#include "steer.h"
#include "brake.h"
#include "accel.h"
#include "cruise.h"
#include "vehports.h"			/* default port numbers */
#include "estop.h"

#include "MTA/Modules.hh"
#include "vdmta.hh"
#include "MTA/Kernel.hh"
#include <boost/shared_ptr.hpp>

#include "VState.hh"		/* grab message format for VState */

/* Functions defined in this file */
int user_quit(long);
static int user_load_acttbl(long), user_save_acttbl(long);
int veh_gps_init(long), veh_gps_process(long);
int vdrive_mode(DD_ACTION, int);

/* Variables used in this file */
int remote_flag = 0;			/* grab data remotely */
int mta_flag = 1;			/* run MTA */
int steer_port = STEER_SERIAL_PORT;     /* copy of port number */
int brake_port = BRAKE_SERIAL_PORT;     /* copy of port number */
int accel_port = PP_GASBR;              /* copy of port number */
struct VState_GetStateMsg vehstate;	/* current vehicle state */
struct VDrive_CmdMotionMsg mtacmd;      /* commanded motion from MTA */
char config_file[FILENAME_MAX] =	/* configuration file */
  "vdrive.ini"; 
char dumpfile[FILENAME_MAX] = "vdrive.dat";
DEV_LOOKUP chn_devlut[] = {
  {"virtual", virtual_driver},
  {NULL, NULL}
};

/* Vehicle mode */
enum veh_mode {
  modeOFF = 'o', modeMANUAL = 'm', modeREMOTE = 'r', modeAUTONOMOUS = 'a'
};
enum veh_mode mode_steer = modeOFF;
enum veh_mode mode_drive = modeOFF;
int mode_manual(long), mode_remote(long), mode_off(long), mode_autonomous(long);

/* Usage message */
char *usage = "\
Usage: %s [-v] [options]\n\
  -b	disable braking subsystem\n\
  -e    disable e-stop subsystem\n\
  -h	print this message\n\
  -j    turn off joystick\n\
  -l file \n\
	log debugging messages to a file\n\
  -m	disable MTA subsystem\n\
  -s	disable steering subsystem\n\
  -t	disable throttle subsystem\n\
  -v	turn on verbose error messages\n\
";

/* Define variables for E-Stop subsystem */
int estop_flag = 0;			/* flag to control e-stop subsystem */
int estop_value;                        /* estop status variable */

#ifdef GPS
/* Define variables for the GPS thread */
int veh_gps_com = GPS_SERIAL_PORT;	/* serial port for GPS */
int veh_gps_rate = 1;			/* GPS rate (Hz) */
gpsDataWrapper gpsdata;			/* GPS data */
static char *gps_buffer;		/* buffer for holding GPS data */
int gps_enabled = 0;			/* flag for enabling GPS */
#endif

/* Define variables for joystick thread */
pthread_t joystick_thread;		/* Joystick thread */
void *joystick_start(void *);		/* Joystick startup routine */
struct joy_data joy;			/* Joystick data */
int joy_enabled = 0;		        /* flag for enabling joystick */
int joy_count = 0;		        /* counter for joystick loop */

#ifdef IMU
/* Define variable for IMU thread */
pthread_t imu_thread;			/* IMU thread */
void *imu_start(void *);		/* IMU startup routine */
IMU_DATA imudata;			/* IMU data */
int imu_enabled = 0;			/* flag for enabling IMU */
int imu_count = 0;			/* keep track of IMU reads */
#endif

#ifdef KFILTER
/* Define variables for Kalman filter thread */
pthread_t kf_thread;			/* KF thread */
void *kf_start(void *);			/* KF startup routine */
double kf_vel = 0, kf_acc = 0;		/* velocity and accleration */
int kf_enabled = 0;			/* enable Kalman filter */
int kf_count = 0;			/* keep track of IMU reads */
#endif

/* Steering thread */
pthread_t steer_thread;			/* steering thread */
void *steer_start(void *);		/* steering startup routine */
double str_angle;			/* steering angle */
int str_enabled = 0;			/* flag for enabling steering */
int str_count = 0;			/* keep track of steer writes */
#define STEER_EPS 0.01			/* amount to increment steering */
int steer_inc(long), steer_dec(long);	/* callbacks for changing angle */

/* Acceleration (gas + brake) thread */
pthread_t accel_thread;			/* throttle/break thread */
void *accel_start(void *);
pthread_t brake_thread;			/* braking thread */
void *start_brake_control_loop(void *);
double brk_pos, thr_pos;		/* brake and accelerator position */
int brk_enabled = 0;			/* flag for enabling braking */
int thr_enabled = 0;			/* flag for enabling accelerator */
int acc_count = 0;			/* keep track of accel writes */

double ACCEL_EPS = 0.01;		/* amount to increment braking */
double BRAKE_VEL = 0.01;		/* brake velocity during motion */
double BRAKE_ACC = 0.1;			/* brake acceleration during motion */

double MAX_BRAKE=0.25;			/* max brake position for full stop */
int full_stop(long);            	/* callback for emergency full stop */
int accel_inc(long), accel_dec(long);	/* callbacks for changing accel */

/* Data capture */
pthread_t cap_thread;			/* data capture thread */
void *capture_start(void *);		/* capture startup routine */

/* Reference values */
double ref_steer = 0;			/* steering angle */
double ref_speed = 0;			/* velocity */
double ref_accel = 0;			/* acceleration */

double cruise_gain = 1;			/* gain for cruise control */
int cruise_enabled = 0;			/* cruise control flag */

#include "actdev.h"			/* actuation device display */
#include "vddisp.h"
pthread_t dd_thread;			/* display thread */

int main(int argc, char **argv)
{
    int c, errflg = 0, error = 0;

    /* Turn on error processing during startup */
    dbg_all = dbg_outf = 1;
    dbg_flag = 0;			/* set with -v if you want output */

    /* Parse command line arguments */
    while ((c = getopt(argc, argv, "vrjethbmsl:?")) != EOF)
      switch (c) {
      case 'v':		dbg_flag = 1;			break;
      case 'm':		mta_flag = 0;			break;
      case 'j':         joy_enabled = -1;               break;
      case 'e':         estop_flag = -1;		break;
      case 'r':	        remote_flag = 1;		break;
      case 's':		str_enabled = -1;		break;
      case 't':		thr_enabled = -1;		break;
      case 'b':		brk_enabled = -1;		break;
      case 'h':		errflg++;			break;
      case 'l':				/* open a log file */
	dbg_openlog(optarg, "a");
	break;
	
      default:		errflg++;			break;
	break;
      }

    /* Print an error message if anything went wrong */
    if (errflg || argc < optind) {
      fprintf(stderr, usage, argv[0]);
      exit(1);
    }

    /* If the MTA flag is turned on, automatically turn on the remote flag */
    if (mta_flag) remote_flag = 1;

    /* Load initialization file */
    if (dd_tbl_load(config_file, acttbl) < 0) {
      dbg_error("couldn't load vdrive conifiguration file: %s\n", config_file);
      ++error;
    }

    /* Initalize anything that might cause an error */
    dbg_info("joystick: %d", joy_enabled);
    if (joy_enabled == 0) {
      if (joy_open(JOYSTICK_PORT, &joy) < 0) ++error; else ++joy_enabled;
    }

#   ifdef ESTOP
    dbg_info("estop: %d", estop_flag);
    if (estop_flag == 0)
      if (estop_init() <  0) {
	dbg_error("estop init failed"); 
	++error;
      }
#   endif

    dbg_info("steering: %d", str_enabled);
    if (str_enabled == 0) 
      if (init_steer() < 0) ++error; else ++str_enabled;

    dbg_info("accel: %d", thr_enabled);
    if (thr_enabled == 0) {
      if (init_accel()<0) {
	dbg_error("accelerator initialization failed\n");
	++error; 
      } else 
	thr_enabled = 1;;
    }

    dbg_info("brake: %d", brk_enabled);
    if (brk_enabled == 0) {
      if (init_brake() < 0) { 
	++error; 
	dbg_error("brake initialization failed\n");
      } else {   
	/* 
	 * Need to initialize the pot reader 
	 *
	 * This essentially initialized the parallel port, which is
	 * also used by the throttle.  Hence if throttle is enabled,
	 * we don't need to re-initialize.
	 */
	if (thr_enabled != 1 &&  initBrakeSensors() < 0) {
	  dbg_error("brake sensor initialization failed\n");
	  ++error; 	  
	} else {
	  /* Move brake to initial position */
	  dbg_info("setting brake position to zero...");
	  set_brake_abs_pos_pot(0.0, 0.5, 1.0);
	  usleep(3000000);    // Give time for actuator to finish moving
	  brk_enabled = 1;
	}
      }
    }

#   ifdef GPS
    /* Initialize the sensing system only if -r flag is not specified */
    if (!remote_flag && veh_gps_init((long) 0) != 0) 
      ++error; 
    else 
      ++gps_enabled;
#   endif

#   ifdef IMU
    if (!remote_flag && InitIMU(&imudata) < 0) {
      fprintf(stdout, "IMU error\n");
      ++error; 
    } else
      imu_enabled = 1;
#   endif

    /* Initialize sparrow channel structures */
    if (chn_config("vdrive.dev") < 0) {
      dbg_error("can't open vdrive.dev");
      ++error;
    } else if (chn_init() < 0) {
      dbg_error("error initializing channel interface");
      ++error;
    }

    /* Pause if there are any errors or verbose */
    dbg_info("[error = %d], steer = %d, brake = %d, throttle = %d\n", 
	     error, str_enabled, brk_enabled, thr_enabled);
    if (dbg_flag || error) {
      if (error) fprintf(stdout, "\nErrors on startup; continue (y/n)? ");
      if (!error && dbg_flag) fprintf(stderr, "\nContinue (y/n)? ");
      if (getchar() != 'y') exit(1);
    }

    /* Turn off printf while display is running */
    dbg_all = 0;
    //	  dbg_add_module("imu.cc");
    //    dbg_add_module("brake.c");
    //    dbg_add_module("parallel.c");

    /* Initialize the display package */
    if (dd_open() < 0) exit(1);		/* initialize the database, screen */
    dd_bindkey('Q', user_quit);
    dd_bindkey('M', mode_manual);
    dd_bindkey('A', mode_autonomous);
    dd_bindkey('R', mode_remote);
    dd_bindkey('O', mode_off);
    dd_bindkey('j', steer_inc);
    dd_bindkey('k', steer_dec);
    dd_bindkey(K_HOME, accel_inc);
    dd_bindkey('c', user_cap_toggle);
    dd_bindkey('m', accel_dec);
    dd_bindkey(' ', full_stop);
    dd_usetbl(vddisp);		/* set display description table */

    /* Start up threads for execution */
    fprintf(stderr, "starting threads\n");
    if (brk_enabled == 1) {
      /* Start the brake control while loop in brake.c */
      if (pthread_create(&brake_thread, NULL, start_brake_control_loop, 
			 (void *) &brk_enabled) < 0) {
	perror("brake thread");
      }
    }
    pthread_create(&joystick_thread, NULL, joystick_start, (void *) NULL);
    pthread_create(&steer_thread, NULL, steer_start, (void *) NULL);
    if (pthread_create(&accel_thread, NULL, accel_start, (void *) NULL) < 0) {
      perror("accel");
      exit(1);
    }
#   ifdef IMU
    if (pthread_create(&imu_thread, NULL, imu_start, (void *) NULL) < 0) {
      perror("imu");
      exit(1);
    }
#   endif
#   ifdef KFILTER
    if (kf_enabled == 0)
      pthread_create(&kf_thread, NULL, kf_start, (void *) NULL);
#   endif
    pthread_create(&cap_thread, NULL, capture_start, NULL);

    /* Run the display manager as a thread */
    fprintf(stderr, "starting dd_loop\n");
    pthread_create(&dd_thread, NULL, dd_loop_thread, (void *) NULL);
    if (!mta_flag)
      /* If we aren't running MTA, wait until dd_loop ends */
      pthread_join(dd_thread, (void **) NULL);
    else {
      /* Otherwise, pass control to MTA */
      Register(shared_ptr<DGC_MODULE>( new VDrive ));
      StartKernel();
    }
    dd_close();				/* clear the screen and free memory */

    /* User cleanup goes here */
}

/*
 * Mode switching
 *
 */

int mode_manual(long arg) { 
  mode_steer = modeMANUAL; 		/* manual steering */
  mode_drive = modeOFF; 		/* brake and throttle via pedals */
  return 0; 
}
int mode_remote(long arg) { mode_steer = mode_drive = modeREMOTE; return 0; }
int mode_off(long arg) { mode_steer = mode_drive = modeOFF; return 0; }
int mode_autonomous(long arg) { mode_steer = mode_drive = modeAUTONOMOUS; return 0; }

/* Process threads */

void *joystick_start(void *arg)
{
  /* Thread just reads the joystick port forever */
  while (1) {
    if (joy_enabled == 1) {
      joy_read(&joy);
      ++joy_count;
    }
  }
  return NULL;
}

#ifdef IMU
void *imu_start(void *arg)
{
  /* Thread just reads the joystick port forever */
  while (1) 
    if (imu_enabled) {
      if (!remote_flag) {
	/* Read the data locally */
	IMURead(&imudata);

	/* If we are maintaining data locally, update the states */
	/* TBD: need to reconcile with KF running locally */
	vehstate.Speed += imudata.dvx;
	vehstate.Accel = 0.0;
      } else {
	/* Copy the data from VState module (MTA) */
	imudata = vehstate.imudata;
	usleep(10000);			/* sleep for a bit */
      }
      ++imu_count;
    } else
      usleep(10000);			/* nothing to do but sleep */
  return NULL;
}
#endif

#ifdef KFILTER
void *kf_start(void *arg)
{
  while (1) {
    if (kf_enabled) {
      kf_vel = vehstate.Speed;
      kf_acc = vehstate.Accel;
    }
  }
  return NULL;
}
#endif

/* This is the thread setting throttle and brake values. */
void *accel_start(void *arg)
{
  while (1) {
    double cmd_acc;			/* commanded accleration */
    double cmd_vel;			/* commanded velocity */

#   ifdef ESTOP
    if (estop_flag == 1) estop_value = estop_status();
#   endif
    
    switch (mode_drive) {
    case modeMANUAL:
      /* Use the joystick to command motion */
      cmd_acc = (joy.data[1] - joy.offset[1])/joy.scale[1];
      if (cmd_acc > 0) cmd_acc *= cruise_gain;
      break;

    case modeREMOTE:
      /* Use reference acceleration to command motion */
      cmd_acc = ref_accel;
      break;

    case modeAUTONOMOUS:
      /* Get data from MTA */
      ref_speed = mtacmd.velocity_cmd;
      break;

    case modeOFF:
      thr_pos = 0;
      brk_pos = 0;
      break;
    }

    /* See if we should run the cruise control loop */
    if (cruise_enabled) {
      /* Call the cruise control function (PD controller) and add
	 reference acceleration (offset) */
      cmd_acc = cruise_gain * cruise(ref_speed, vehstate.Speed, 
				     vehstate.Accel) + ref_accel;
    }

    /* Check limits and allocate out the command */
    if (cmd_acc > 1) cmd_acc = 1;
    if (cmd_acc < -1) cmd_acc = -1;
    thr_pos = (cmd_acc < 0) ? 0 : cmd_acc;
    brk_pos = (cmd_acc < 0) ? -cmd_acc : 0;

    /* Send command to the accelerator */
    if (thr_enabled == 1) {
      ++acc_count;
      // Ike says that setting the position to zero then desired works
      // Doesn't seem to do all that much better
      set_accel_abs_pos(0.0);
      set_accel_abs_pos(thr_pos);
    }

    /* Send command to the brake */
    if (brk_enabled == 1) {
      ++acc_count;
      brk_lock = 1;
      // calling new quick response brake code  from brake.c 
      // set_brake_abs_pos_limited(brk_pos, BRAKE_VEL, BRAKE_ACC);
      // cout << "[vdrive.c] Setting brake position to " << brk_pos << endl;
      set_brake_abs_pos_vel(brk_pos, BRAKE_VEL, BRAKE_ACC);
      brk_lock = 0;
    }

    /* Sleep for 100 msec to allow everyone to keep up */
    usleep(100000);
  }
  return NULL;
}

int accel_inc(long arg) { ref_accel += ACCEL_EPS; return 0; }
int accel_dec(long arg) { ref_accel -= ACCEL_EPS; return 0; }

/* Execute a full stop */
int full_stop(long arg) {
  int count = 0;

  /* First, push down the break: will get picked up in accel thread */
  if (brk_enabled) {
    /* Check if brake loop is active */
    while (brk_lock and count++ < 10) usleep(10000);

    /* Turn off the brake */
    brk_enabled = 0;			/* turn off brake loop */
    brk_pos = MAX_BRAKE;		/* just in case loop restarts */
    set_brake_abs_pos(brk_pos, BRAKE_VEL, BRAKE_ACC);
  }

  /* Now turn off the accelerator */
  if (thr_enabled) thr_pos = 0; 

  /* Beep so that we know this all happened */
  DD_BEEP();

  return 0;
} 

void *steer_start(void *arg)
{
  /* Thread just reads the joystick port forever */
  while (1) {
    switch (mode_steer) {
    case modeMANUAL:
      /* Use the joystick to command the angle */
      str_angle = (joy.data[0] - joy.offset[0])/joy.scale[0];
      if (str_angle < -1) str_angle = -1;
      if (str_angle >  1) str_angle =  1;
      break;

    case modeREMOTE:
      /* Use the reference value to command the position */
      str_angle = ref_steer;
      if (str_angle < -1) str_angle = -1;
      if (str_angle >  1) str_angle =  1;
      break;

    case modeAUTONOMOUS:
      /* Get data from MTA */
      str_angle = mtacmd.steer_cmd;
      break;

    case modeOFF:
      str_angle = 0;
      break;
    }

    if (str_enabled == 1) {
      /* Write the data to the steering controller */
      if (steer_heading(str_angle) < 0) {
	str_count = -1;
      } else
	++str_count;

      /* Update the steering state (needed?) */
      /* steer_state_update(0, 0); */

      usleep(50000);			/* 20 Hz update rate should be fine */
    }
  }
  return NULL;
}

int steer_inc(long arg) { ref_steer += STEER_EPS; return 0; }
int steer_dec(long arg) { ref_steer -= STEER_EPS; return 0; }

/* Data capture */
void *capture_start(void *arg)
{
  struct timeval tv;
  static int start = 0;

  /* Initialize the starting time */
  gettimeofday(&tv, NULL); start = tv.tv_sec;

  while (1) {
    /* Save the time the data was taken */
    gettimeofday(&tv, NULL);
    chn_data(0) = (double) (tv.tv_sec - start) + ((double) tv.tv_usec) / 1e6;

    /* Fill up the rets of the data array with the data we want to capture */
    /* Note that # of channels must correspond to config.dev */
    chn_data(1) = vehstate.kf_lat;
    chn_data(2) = vehstate.kf_lng;
    chn_data(3) = vehstate.Pitch;
    chn_data(4) = vehstate.Roll;
    chn_data(5) = vehstate.Yaw;
    chn_data(6) = vehstate.Speed;
    chn_data(7) = vehstate.PitchRate;
    chn_data(8) = vehstate.YawRate;
    chn_data(9) = brk_pos;
    chn_data(10) = thr_pos;
    chn_data(11) = str_angle;
    chn_data(12) = 0;
    chn_data(13) = 0;
    chn_data(14) = 0;
    chn_data(15) = 0;

    chn_write();			/* write data to memory */
    usleep(10000);			/* take data ~100 Hz */
  }
}


/*
 * Callbacks to link to vehicle library
 *
 */

#ifdef GPS
/* Initalize the GPS unit */
int veh_gps_init(long arg)
{
  /* Initialize the GPS unit */
  if (init_gps(veh_gps_com, veh_gps_rate) != 1) {
    fprintf(stderr, "GPS: initialization failure\n");
    return -1;
  }
  if ((gps_buffer = (char *) malloc(2000)) == NULL) {
    fprintf(stderr, "GPS: memory allocation error\n");
    return -1;
  }
  return 0;
}

/* Read messages from the GPS unit and put them into a buffer */
int veh_gps_process(long arg)
{
  if (gps_enabled) {
    get_gps_msg(gps_buffer, 1000);
    switch (get_gps_msg_type(gps_buffer)) {
    case GPS_MSG_PVT:
      gpsdata.update_gps_data(gps_buffer);
      break;
    
    default:
      /* print_gps_msg(gps_buffer) */
      break;
    }
  }
}
#endif

/* Quit the dfan program */
int user_quit(long arg)
{
  int count = 0;

  /* Turn off the brake actuator */
  // char *parameters = "0 0 0 0 ";
  // send_brake_command(15, parameters, strlen(parameters));

  /* Tell MTA to shut down */
  ForceKernelShutdown();

  /* Wait for 1 second to make sure it is shut down */
  while (ShuttingDown()) {
    if (count++ > 1000) break;
    usleep(1000);
  }
  if (!ShuttingDown()) fprintf(stderr, "\nMTA Kernel shut down\n");

  /* Tell dd_loop to abort now */
  return DD_EXIT_LOOP;
}

/* Toggle capture mode */
int user_cap_toggle(long arg)
{
  return chn_capture_flag ? chn_capture_off() : chn_capture_on();
}

/* Save the actuation parameters */
int user_save_acttbl(long arg)
{
  if (dd_tbl_save(config_file, acttbl) < 0) {
    dd_text_prompt("could not print data");
    dd_beep((long) 0);
  }
  return 0;
}

/* Save the actuation parameters */
int user_load_acttbl(long arg)
{
  if (dd_tbl_load(config_file, acttbl) < 0) {
    dd_text_prompt("could not print data");
    dd_beep((long) 0);
  }
  return 0;
}

/* Special display manager function for handling the vehicle mode */
int vdrive_mode(DD_ACTION action, int id)
{
    DD_IDENT *dd = ddtbl + id;
    char *value = (char *)dd->value;
    char itmp;
    
    switch (action) {
    case Input:
      if (DD_SCANF("Mode: ", "%c", &itmp) == 1)
	*value = (short) itmp;
      break;
	
    default:			/* default = use dd_short */
      return dd_short(action, id);
    }
    return 0;
}
