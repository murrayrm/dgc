/*****************************************************************************
        PARALLEL INTERFACE

 ****************************************************************************/

/* allow C++ code to use this header */
#ifdef __cplusplus
extern "C" {
#endif

/* only use this header once */
#ifndef PARALLEL_H
#define PARALLEL_H


/* Function PROTOTYPES */
int pp_get_data(int port);
char pp_get_bit(int port, int pin);
int pp_init (int port, int dir);
int pp_set_bits(int port, int pin_high, int pin_low);
int pp_set_data(int port, unsigned char val);
int pp_end (int port);

struct pp_struct {
int dir;                 /* Direction of data transfer */
int fd;                  /* File */
unsigned char data_val;           /* last values written to data lines*/
unsigned char ctrl_val;           /* last values written to control lines*/
};


/* Directions that the parallel port can go */
#define PP_DIR_IN       1       /* INPUT from the port*/ 
#define PP_DIR_OUT      0       /* OUTPUT to the port */

/* Set pin definitions on the parallel port */
#define PP_PIN01 1<<0
#define PP_PIN02 1<<1
#define PP_PIN03 1<<2
#define PP_PIN04 1<<3
#define PP_PIN05 1<<4
#define PP_PIN06 1<<5
#define PP_PIN07 1<<6
#define PP_PIN08 1<<7
#define PP_PIN09 1<<8
#define PP_PIN10 1<<9
#define PP_PIN11 1<<10
#define PP_PIN12 1<<11
#define PP_PIN13 1<<12
#define PP_PIN14 1<<13
#define PP_PIN15 1<<14
#define PP_PIN16 1<<15
#define PP_PIN17 1<<16

/* More definitions for data pins */
#define PP_DATA0        PP_PIN02
#define PP_DATA1        PP_PIN03
#define PP_DATA2        PP_PIN04
#define PP_DATA3        PP_PIN05
#define PP_DATA4        PP_PIN06
#define PP_DATA5        PP_PIN07
#define PP_DATA6        PP_PIN08
#define PP_DATA7        PP_PIN09

/* More definitions for control pins*/
#define PP_CTRL_STROBE  PP_PIN01
#define PP_CTRL_AUTOFD  PP_PIN14
#define PP_CTRL_INIT    PP_PIN16
#define PP_CTRL_SELECT  PP_PIN17

/* More definitions for status pins*/
#define PP_STAT_ACK     PP_PIN10
#define PP_STAT_BUSY    PP_PIN11
#define PP_STAT_POPE    PP_PIN12
#define PP_STAT_SELECT  PP_PIN13
#define PP_STAT_ERROR   PP_PIN15

/* Masks for data, control, status lines */
#define PP_DATA         (PP_DATA0 | PP_DATA1 | PP_DATA2 | PP_DATA3 | PP_DATA4 | PP_DATA5 | PP_DATA6 | PP_DATA7)
#define PP_CTRL         (PP_PIN01 | PP_PIN14 | PP_PIN16 | PP_PIN17)
#define PP_STATUS       (PP_PIN10 | PP_PIN11 | PP_PIN12 | PP_PIN13 | PP_PIN15)

#define PP_STAT15 1<<3
#define PP_STAT13 1<<4
#define PP_STAT12 1<<5
#define PP_STAT10 1<<6
#define PP_STAT11 1<<7

#endif

/* allow C++ code to use this header */
#ifdef __cplusplus
}
#endif
