/*
 * cruise.cc - velocity control computation
 *
 * Version 1.0
 * 11/13/03 Haomiao Huang
 * File Creation
 *
 * 11/14/03 Haomiao Huang
 * Added force lookup control
 *
 */

#include "cruise.h"

/* Compute the amount of force to apply to the actuator */
double cruise(double targetVelo, double curVelo, double curAccel) 
{
  return accel_lookup_control(curVelo, targetVelo, curAccel);
}

/* First basic version of cruise controller, uses simple
proportional/derivative gains to generate a change in the controller setting
which should be added to the current control setting (assuming -1 to 1 control
setting).  Characterized by slow rise times, smooth, gentle accelerations.
Will primarily run the throttle to control speed, reactions to large changes
in desired speed will be slow.  Best for maintaining a cruise speed when
already close to it.  A more aggressive controller will be implemented in v2.0 */

/*v.2 2003/11/22 THYAGO CONSORT
	CREATED:
	+ Feed fowared from reference velocity to throttle position thru lookUp table "V_TO_T"
	+ Mapping from control signal to throttle position thru lookUp table "CTR_TO_T"
	MODIFICATION:
	+Parameters
		- Gp = 0.75
		- Gd = 1.25
	+Functions
		- accel_lookup_control
*/

#include "cruise.h"

#define V_TO_A_FILENAME "v_to_a_lookup"
#define A_TO_T_FILENAME "a_to_t_lookup"
#define V_TO_T_FILENAME "v_to_t_lookup"
#define CTR_TO_T_FILENAME "ctr_to_t_lookup"

// Constant definitions
const double Gp = 0.75; // Proportional Gain
const double Gd = 1.25; // Derivative Gain
const int avg_N = 5;  // number of time steps to average errors over
const double Upper_Cutoff = .3; // Maximum throttle setting to use
const double Lower_Cutoff = -0.8; // Maximum brake setting to use
const double Gp_lookup = .5; // Proportional Gain for force lookup control
const double Gd_lookup = .05; // Derivative Gain for force lookup control

// Global variable declaration
double last_error = 0.0; // stores previous error to calculate change in velocity error, initializes to 0
LUtable v_to_a_Table(V_TO_A_FILENAME);
LUtable a_to_t_Table(A_TO_T_FILENAME);
LUtable v_to_t_Table(V_TO_T_FILENAME);
LUtable ctr_to_t_Table(CTR_TO_T_FILENAME);

// Cruise controller initialization, currently empty
void cruise_init()
{
  return;
}

// Integral_Control
// Wrapper for delta throttle, sets saturation limits and does the addition
// of the delta value to the old throttle value
double Integral_Control(double vcurrent, double vdesired, double current_throttle)
{
  double throttle; // new throttle setting

  throttle = current_throttle + delta_throttle(vcurrent, vdesired);

  // sets saturation on actuation settings
  if (throttle >= Upper_Cutoff)
    throttle = Upper_Cutoff;
  else if (throttle <= Lower_Cutoff)
    throttle = Lower_Cutoff;

  return throttle;

}

//  delta_throttle
//  Generates a change in throttle setting based on the following algorithm:
//  dthrottle = Gp (vdesired - vcurrent) + Gd (verr - last_error)
double delta_throttle(double vcurrent, double vdesired)
{
  double verr =  vdesired - vcurrent;

  double dthrot = Gp * verr + Gd * (verr - last_error);

  last_error = verr;

  return dthrot;
}

// accel_lookup cruise control
// general idea:
// Uses a map of the accelerations from different throttle/brake settings
// to get a steady state acceleration needed to balance out the acceleration
// from drag.  Then generates a delta_accel based on the current error in
// velocity using a PD controller.  Remaps the desired accel as a throttle/
// brake setting (-1 to 1).
double accel_lookup_control(double vcurrent, double vdesired, double acurrent)
{
  // variable definitions
  double verr;  // error in velocity
  //double derror; // change in the error
  double adesired;  // desired acceleration
  double aerr;  // error in acceleration
  double control;  // output of the PD control
  double throttle;  // throttle setting

  // Compute the current velocity error
  verr = vdesired - vcurrent;

  // Look up the desired acceleration and compute current
  //   acceleration error
  adesired = Accel_Lookup(vdesired);
  aerr = adesired - acurrent;
  //derror = verr - last_error;

  // Generate the Proportional-Derivative control signal
  control = Gp * verr + Gd * aerr;
  
  // Obtain the throttle/brake setting
  throttle = Vref_Throttle_Lookup(vdesired) + Ctr_Throttle_Lookup(control);

  // sets saturation on actuation settings
  if (throttle >= Upper_Cutoff)
    throttle = Upper_Cutoff;
  else if (throttle <= Lower_Cutoff)
    throttle = Lower_Cutoff;

  return throttle;
}


// Accel_Lookup
// Looks up the acceleration required to overcome drag at a certain
// steady state velocity and returns this acceleration
double Accel_Lookup(double v_steady)
{
  double a; // the desired acceleration

  a = v_to_a_Table.getValue(v_steady);

  return a;
}

// Throttle_Lookup
// Looks up the throttle or brake setting necessary to give the car the passed
// acceleration
double Throttle_Lookup(double acceleration)
{
  double t; // throttle setting

  t = a_to_t_Table.getValue(acceleration);

  return t;
}

// Ctr_Throttle_Lookup
// Looks up the throttle position required to cancel the error
// control and returns a throttle position
double Ctr_Throttle_Lookup(double control)
{
  double t; // the desired throttle position

  t = ctr_to_t_Table.getValue(control);

  return t;
}

// Vref_Throttle_Lookup
// Looks up the throttle position required to keep the velocity reference
// desired velocity and returns a throttle position
double Vref_Throttle_Lookup(double velRef)
{
  double t; // the desired throttle position

  t = v_to_t_Table.getValue(velRef);

  return t;
}
