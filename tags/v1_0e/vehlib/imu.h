#ifndef IMU_H
#define IMU_H

typedef struct imu_data {
  double dvx;
  double dvy;
  double dvz;
  double dtx;
  double dty;
  double dtz;
} IMU_DATA;

int InitIMU(IMU_DATA *);
int IMURead(IMU_DATA *);

#endif
