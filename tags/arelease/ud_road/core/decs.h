//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------------

// General

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <signal.h>

#include <crtdbg.h>

// // Windows 
// 
//#include <windows.h>
// #include <ddraw.h>

// Intel

//#include <mkl.h>

// OpenGL/GLUT includes

#include <GL/glut.h>

#include <fftw3.h>

// My stuff

#include "CR_Memory.hh"
#include "CR_Random.hh"
#include "CR_Time.hh"
#include "CR_Error.hh"
#include "CR_IplImage.hh"
#include "CR_Movie_AVI.hh"
#include "CR_Matrix.hh"

#include "CR_VanishingPoint.hh"
#include "CR_Segmenter.hh"
#include "CR_Socket.hh"

//----------------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------------

// general 

#ifndef PI
#define PI      3.14159265358979323846
#endif

#ifndef PI_2
#define PI_2    1.57079632679489661923
#endif

#ifndef TWOPI
#define TWOPI   (PI * 2.0)
#endif

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

// be CAREFUL with sending functions to macros, because
// they'll probably get evaluated more times than you 
// want.  In particular, don't send rand() in the x and
// y slots, or the i slot, because it'll be a goddamn mess.
// assign the result of the function call to a variable,
// then pass it along.

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))
#define MAX3(A, B, C)         (((A) > (B)) ? MAX2((A), (C)) : MAX2((B), (C)))
#define MIN3(A, B, C)         (((A) < (B)) ? MIN2((A), (C)) : MIN2((B), (C)))

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
