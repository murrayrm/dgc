//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// CRIpl
//----------------------------------------------------------------------------

#ifndef CRIPL_DECS

//----------------------------------------------------------------------------

#define CRIPL_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include <math.h>

#include <ipl.h>
#include <ijl.h>

#include "CR_Error.hh"
//#include "CR_Old_Matrix.hh"
//#include "CR_Demo3.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#define IPL_RED          0
#define IPL_GREEN        1
#define IPL_BLUE         2

#define IPLXY_R(im, x, y)     (im->imageData[y * im->width + x])
#define IPLXY_G(im, x, y)     (im->imageData[y * im->width + x + 1])
#define IPLXY_B(im, x, y)     (im->imageData[y * im->width + x + 2])

#define COLOR_LEVELS        255

#define OUTLIER_NONE              1
#define OUTLIER_SATURATED         2

#define IPL_TYPE_COLOR_8U         0
#define IPL_TYPE_COLOR_8U_YUV     1
#define IPL_TYPE_GRAY_8U          2
#define IPL_TYPE_COLOR_32F        3
#define IPL_TYPE_GRAY_32F         4

//-------------------------------------------------
// functions
//-------------------------------------------------

void free_IplImage(IplImage *);

IplImage *make_IplImage(int, int, int);
IplImage *make_FP_IplImage(int, int, int);
IplImage *read_IplImage(char *);
void write_IplImage(char *, IplImage *, ...);
IplImage *read_PPM_to_IplImage(char *);
void read_PPM_to_IplImage(char *, IplImage *);
void write_IplImage_to_PPM(char *, IplImage *);
void write_IplImage_to_PFM(char *, IplImage *);
void write_IplImage_to_RIF(char *, IplImage *);
void print_IplImage_properties(IplImage *);

IplImage*CreateImageFromJPEG(LPCSTR); 
IplImage* CreateImageHeaderFromIJL(const JPEG_CORE_PROPERTIES*);
bool WriteImageToJPEG(IplImage*, LPCSTR);
bool SetJPEGProperties(JPEG_CORE_PROPERTIES&, const IplImage*);
void print_JPEG_properties(JPEG_CORE_PROPERTIES *);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif
