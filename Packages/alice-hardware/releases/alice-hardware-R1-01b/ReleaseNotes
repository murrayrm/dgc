              Release Notes for "alice-hardware" package

Release R1-01b (Sun Mar 18  9:36:31 2007):
	Minor update (still pre-test): had to rebuild stereofeeder.

Release R1-01a (Sun Mar 18  9:28:51 2007):
	This is the pre-test release for the 'alice-hardware' package.  This
	is what we will test in field test 2.  Once the test is complete, we
	will post updated module releases and tag as stable.

	 ReleaseNotes extracts from adrive module
	 ........................................

   Release R1-00g (Sat Mar 17 19:46:47 2007):
	Added better error logging (added cerr output to logger function)
	and also fixed a latent bug in the way gas commands are handled
	(wasn't checking if transmission was enabled before looking at
	whether we are in park; doesn't matter unless interlocks are on).

	This version should be ready for field testing.  Make sure that
	interlocks are off in the config file (this is the default),
	otherwise you won't be able to command throttle.

   Release R1-00f (Sat Mar  3  9:29:32 2007):
	Updated to use --skynet-wkey + latest version of interfaces

   Release R1-00e (Tue Feb 20 15:45:27 2007):
	Fixed include statements so this revision of adrive compiles against
	revision R2-00a of the interfaces and the skynet modules 


	 ReleaseNotes extracts from astate module
	 ........................................

   Release R1-00f (Tue Feb 20 15:51:26 2007):
	Fixed include statements so this revision of astate compiles against revision R2-00a of the interfaces and the skynet modules


	 ReleaseNotes extracts from dgcutils module
	 ........................................

   Release R1-00n (Fri Mar 16 10:07:04 2007):
	Added a RDDF.clear() function to clear out the contents of an RDDF.
	This is something that OCPtSpecs needed in order to receive RDDFs
	properly. 

   Release R1-00m (Mon Mar 12 15:32:51 2007):
        Added functions to set Mutex condition to
        FALSE. waitForConditionFalse has not been added because it seems to
        be useless. 
	

	 ReleaseNotes extracts from frames module
	 ........................................

   Release R1-00o (Sat Mar 17  0:32:14 2007):
	added split and get_bound_box functions to point2arr class

   Release R1-00m (Wed Mar 14 15:53:16 2007):
	added some 2d functionality to point2.cc/hh to support new map
	queries

   Release R1-00l (Wed Mar 14 10:07:12 2007):
	Added cut_front, cut_back, intersect other geometric functions to
	point2arr.

   Release R1-00k (Tue Mar 13 15:55:16 2007):
		added simple line handling and geometric functionality to
	point2.cc.  Cleaned up test_point2.cc.

   Release R1-00j-ahoward (Wed Mar 14 22:52:14 2007):
  Minor tweaks to reduce build warnings.

   Release R1-00j (Mon Mar 12 22:42:54 2007):
  Added some matrix operations.

   Release R1-00i (Thu Mar  8 18:27:46 2007):
	updated point2 and point2arr to work with new tplanner release.  
Need to debug insert function for tplanner.  

   Release R1-00h (Wed Mar  7 15:26:56 2007):
	added insert(unsigned int index, point2 pt) function to point2arr and point2arr_uncertain.

   Release R1-00g (Sun Mar  4 17:08:48 2007):
	Added == and != operators to point2 and point2arr.  Both work
	transparently with point2_uncertain and point2arr_uncertain.

   Release R1-00f (Fri Mar  2 18:14:13 2007):
	Fixed the include statements in point2.cc and point2_uncertain.cc so they compile.

   Release R1-00e (Thu Mar  1 19:33:22 2007):
		added point2arr and point2arr_uncertain classes to point2.hh and
	point2_uncertain.hh.  Basically these are wrappers for a vector of
	points.  They also contain geometric analysis functions such as
	get_side which returns which side of a line a given point lies and
	get_offset which offsets line points to either side for lane line
	initialization or polygon growth and contraction.


   Release R1-00d (Wed Feb 21 19:41:23 2007):
	changed names of 2d points and poses to make them more intuitive.  Added somee functionality to point2 and point2_uncertain.  


	 ReleaseNotes extracts from interfaces module
	 ........................................

   Release R4-00g (Sun Mar 18  8:42:46 2007):
	Minor update in preparation for field test.  StereoImageBlob.h
	needed to include vec3.h and pose3.h from the frames module. 

   Release R4-00f (Fri Mar 16 13:06:15 2007):
	Added GcModuleInterfaces that is independent of gcmodule and
	contains definitions needed by SegGoals and Status." 

   Release R4-00e (Fri Mar 16 10:37:56 2007):
	Added functions to get the inverse transformations from local ->
	vehicle -> sensor -> image frames in StereoImageBlob.h


   Release R4-00d (Fri Mar 16  7:56:26 2007):
	Fixed the serialize() function in SegGoals.

   Release R4-00c (Thu Mar 15 13:12:27 2007):
        Added the header file that provide the interface between tplanner
        and dplanner	 

   Release R4-00b (Thu Mar 15 10:16:44 2007):
	Fix for StereoImageBlob accessor functions.

   Release R4-00a (Mon Mar 12 22:58:01 2007):
	<* Pleaase insert a release notes entry here. If none, then please
	delete these lines *> 

   Release R3-00d (Mon Mar 12  3:59:39 2007):
	reverting order of sensors in the enum so old logs will still work

   Release R3-00c (Sun Mar 11 12:11:01 2007):
	Updated MapElementMsg.h to include plot_color and plot_value fields

   Release R3-00b (Fri Mar  9 19:31:14 2007):
	Added message types SNocpObstacles and SNocpParams to sn_types.h

   Release R3-00a (Fri Mar  9 16:48:11 2007):
	"Moved SegmentTypes into SegGoals struct to avoid conflict with some definition in mplanner. Added ReasonForFailure to SegGoalsStatus."

   Release R2-00o (Fri Mar  9 16:38:01 2007):
	Minor changes to ObsMapMsg.h; I removed unnecessary header files and
	cmap row/column definitions.

   Release R2-00n (Fri Mar  9 12:21:02 2007):
	Moved MapElement.hh to map module.  This is part of a general
	restructuring which moves MapElement functionality into map.  
	MapElementMsg still lives in interfaces.  MapElementTalker has also
	been moved from skynettalker to map.  All users of the
	MapElementTalker will need to include map/MapElementTalker.hh and
	map/MapElement.hh instead of skynettalker/MapElementTalker and
	interfaces/MapElement.hh.

   Release R2-00m (Tue Mar  6 23:34:06 2007):
  Support for long-range stereo.

   Release R2-00l (Sat Mar  3 15:02:33 2007):
  Minor tweak.

   Release R2-00k (Sat Mar  3 14:04:19 2007):
	Minor update to Makefile allow compilation of rddfPlanner in trunk

   Release R2-00j (Sat Mar  3 13:52:13 2007):
Figured out how to send deltas using the CMapDeltaTalker class; this makes the CostMapMsg interface obsolete. These 
changes reflect that. 

   Release R2-00i (Sat Mar  3 11:58:26 2007):
  Change the stereo blob structure to make it more future proof.  Old stereo
  log files will need to be converted to the new format.

   Release R2-00g (Fri Mar  2 11:24:45 2007):
	added the CostMapMsg.h header file; this defines the message 
interface that tplanner will use to send map deltas to dplanner; 
originally cmap class was supposed to send deltas across the network and 
apply them but because of unsuccessful attempts to get the 
send/receive-deltas functions working, we've (temporarily) resorted to 
defining our own delta-message which this file is for; if in the event 
that jeremy gillula (or someone else) can help us figure out how to 
correctly use the send/receive deltas functions, this header file will not be 
necessary and can be removed. 

   Release R2-00f (Thu Mar  1 19:32:06 2007):
	updated MapElement.hh to work with new frames point2 functions.

   Release R2-00e (Thu Feb 22 10:56:18 2007):
  Added another stereo module id.

   Release R2-00d (Wed Feb 21 23:39:10 2007):
  Added ObsMapMsg.hh for obstacle map deltas.

   Release R2-00c (Wed Feb 21 19:53:54 2007):
Updated MapElement functionality.  

   Release R2-00b (Wed Feb 21 13:08:14 2007):
	Added tplanner-dplanner interfaces to sn_types. Modified Status.hh to fix the conflicting problems.

   Release R2-00a (Tue Feb 20 15:36:03 2007):
	Moved all the talkers (including StateClient) which made the interfaces module depend on the skynet module to the skynettalker module. Also 
added sn_types and enumstring (moved from the skynet module).

   Release R1-00u (Sun Feb 18 21:28:34 2007):
	This release fixes a minor namespace issue in MapElement.hh


	 ReleaseNotes extracts from map module
	 ........................................

   Release R1-01q (Sat Mar 17 11:21:09 2007):
	changed color enums from COLOR_RED to MAP_COLOR_RED to avoid some
	collision when compiling lineperceptor

   Release R1-01p (Sat Mar 17  0:47:19 2007):
	Added getLaneCenterPoint, getLaneRightPoint, and getLaneLeftPoint
	functions for tplanner queries.  Added functions to test map
	queries and geometry operations.

   Release R1-01o (Fri Mar 16  0:27:27 2007):
	added new getHeading function which gets an estimate of the lane
	heading at some point if that point is inside valid lane bounds.

   Release R1-01n (Thu Mar 15 13:59:49 2007):
	added addElement function which adds an element if the id is new
	and updates the element if it has been seen.

   Release R1-01m (Wed Mar 14 15:57:56 2007):
	added getTransitionBounds and getBounds functions which are more
	functional and useful for tplanner than existing versions.  Added
	getHeading to get the heading angle at a waypoint.  Added
	getBoundsReverse for getting lanebounds in reverse.

   Release R1-01l (Wed Mar 14 10:14:55 2007):
	Added getObstaclePoint(offset) function for tplanner use which
	returns a point offset from closest obstacle in lane by specified
	amount. Added more geometric test functions

   Release R1-01k (Tue Mar 13 15:45:32 2007):
		Added testGeometry function to test and visualize 2D geometric
	functions.  Implemented getObstacleDist() which given a point in
	local frame returns the distance to nearest obstacle in current
	lane in front of Alice.

   Release R1-01j (Mon Mar 12 15:22:34 2007):
	added getNextStopline function.  Need to debug an RNDF mirroring
	issue but this should work for short term planner testing.
	
   Release R1-01i (Sun Mar 11 11:49:20 2007):
	Added clear and state messages to map elements. added test programs testMapElement, and testMapId.

   Release R1-01h (Sat Mar 10 17:22:19 2007):
	Fixed bug in getStopline.

   Release R1-01g (Fri Mar  9 16:33:35 2007):
	added isStop and isExit query functions to map.

   Release R1-01f (Fri Mar  9 12:15:10 2007):
	Moved MapElementTalker, testSendMapElement, testRecvMapElement, and
	MapElement.hh to map module.  This is part of a general
	restructuring which moves MapElement functionality into map.  All
	users of the MapElementTalker will need to include
	map/MapElementTalker.hh and map/MapElement.hh instead of
	skynettalker/MapElementTalker and interfaces/MapElement.hh.  Also
	-lmap should be added to LIBS in the Makefile.yam if it isn't there
	already 

   Release R1-01e (Thu Mar  8 18:33:06 2007):
	Updated some map functions for use in new tplanner (Sam).

   Release R1-01d (Sat Mar  3 15:55:33 2007):
	Updated getStopline function to take only a point in space not a
	stopline label.

   Release R1-01c (Thu Mar  1 19:34:02 2007):
	implemented first cut of getTransitionBounds and added test_map
	program to test map queries and plot the results.	Implemented the getLeftBound and getRightBound functions.

   Release R1-01b (Tue Feb 27  8:39:07 2007):
	Complete overhaul of map module.  Moved new map structure from mapper into map module.  Map now parses RNDF files with an internal method and has a very different internal structure.  Still need to implement query functions. 

   Release R1-01a (Sun Feb 25 12:56:28 2007):
	Updated to work with new SkynetContainer and interfaces paths


	 ReleaseNotes extracts from mplanner module
	 ........................................

   Release R2-01e (Sat Mar 17 13:39:20 2007):
	Fixed bug in uturn when map is received from mapper.

   Release R2-01d (Sat Mar 17  2:38:37 2007):
	Moved the function for inserting obstacle to Graph. Recursively remove all the edges in the segment that is blocked and find a uturn point 
that is at least 22m from current position.

   Release R2-01c (Fri Mar 16 13:10:06 2007):
	Fixed include statement due to the changes in gcmodule"

   Release R2-01b (Fri Mar 16 12:17:33 2007):
	Added more information to log file.

   Release R2-01a (Fri Mar 16  1:06:27 2007):
	Mission planner that uses GcInterface for message handling (sending directive, receiving status and paring up directive	and status), so it 
uses SkynetTalker instead of SegGoalsTalker to communicate with traffic planner. It can also log data (mission plan, graph, etc) and search for 
rndf/mdf in the DGC_CONFIG_PATH directory. (Un)fixed the yaw angle so it ranges from 0 to 2pi (which is what astate is using right now).

   Release R2-00h (Wed Mar 14 18:05:50 2007):
	Reduced the number of threads in RoutePlanner by combining the	sending and receiving message threads into one thread. Made the dummyTPlanner 
a little smarter. This is the last release of the mission planner that uses SegGoalsTalker to communicate with the traffic planner. 
The next release will use the generic talker.

   Release R2-00g (Tue Mar 13 21:18:05 2007):
	Mission planner that is completely in canonical structure. All the modules are derived from GcModule. Still waiting for automatic message 
handling in GcModule. Nok (, Daniele and Josh) are happy.

   Release R2-00f (Mon Mar 12 19:30:25 2007):
	The RoutePlanner in the canonical structure but not yet derived from gcmodule. Fixed some bugs in MissionControl.

   Release R2-00e (Fri Mar  9 16:37:48 2007):
	Fixed the heading angle computation. Added the default constructor for MissionControl. Fixed Makefile. Combined the functions 
planFromCurrentPosition and planNextMission in RoutePlanner.
	This release of mplanner is only compatible with release R3 (or newer) of the interfaces module.

   Release R2-00d (Wed Mar  7 20:56:13 2007):
	All the rndf dependencies were moved to the graph estimation class (for unit test only). This version of mplanner only commands uturn if 
there are obstacles that block the road."

   Release R2-00c (Wed Mar  7 11:30:23 2007):
	Fixed segfault when DEBUG_LEVEL>4 and removed that mutex that protects the map because only one thread in RoutePlanner needs it now.

   Release R2-00b (Tue Mar  6 21:20:29 2007):
	Fixed bug in the findRoute function. Getting closer to remove the rndf dependency. The route planner now only needs RNDF when it 
determines which edges the manually inserted obstacles block. This function should belong to the map class but I'll leave it here for now for unit 
test purpose.

   Release R2-00a (Mon Mar  5 19:47:20 2007):
	Mission planner that plans route without using rndf information. Got the stupid segfault problem figured out. (Thanks to Daniele again.) 
   Changed the release number because as in the Release R1-00g, this mission planner uses different interfaces to communicate with the mapper.

   Release R1-00g (Sat Mar  3 20:29:19 2007):
	Moved all the graph functionality (including for doing uturn) from the MissionPlanner class to the TraversibilityGraphEstimator class. This 
version of mplanner receives a graph from mapper instead of an rndf object. Need to run the whole loop to verify that the uturn is working correctly. 
(This version of mplanner determines the current lane and segment from state instead of getting it from tplanner so it's harder to run a unit test 
where tplanner fails such that a uturn is needed because the vehicle does not move from the initial position unless we actually run the whole 
planning loop.)

   Release R1-00f (Thu Mar  1 22:31:53 2007):
	Nok's first attempt to untangle the mission planner. Moved the graph structure to the travgraph module and moved relevant functions to the 
travgraph and rndf modules.

   Release R1-00e (Wed Feb 21 22:39:00 2007):
	Updated to work with the new Status.hh in revision R2-00b of interfaces

   Release R1-00d (Tue Feb 20 17:17:42 2007):
	Modified mission planner so it compiles against revision R2-00a of interfaces and skynet


	 ReleaseNotes extracts from rddfplanner module
	 ........................................

   Release R1-00h (Sat Mar 17 13:36:54 2007):
	Fixed the way endpoints are handled for corridors with only two
	waypoints (beginning and end).  These are currently used for U-turn
	segments, so we simply replace the RDDF that is sent with the start
	and end point.  Works correctly with tplanner and trajfollower.

   Release R1-00g (Fri Mar 16 10:18:27 2007):
	This version of rddfplanner has several new features to allow it to
	be used as a temporary substitute for dplanner:

	* The --use-endpoints flag reads the endpoint information sent by
          tplanner to modify the trajectories to start and end at a
          specified location.  This is needed for stoplines, intersections,
          etc.   We use the terminal velocity for the endpoint, but ignore
          initial velocity.  You can turn off the use of terminal velocity
          using the --ignore-end-velocity flag.

	* The feasiblize function was not being called properly and was not
          computing accelerations properly.  This is now fixed, although it
          doesn't really change that much.  Note that this version of
          rddfplanner does not take into account curvature limits of the
          vehicle (the dgc/trunk version did, but needed lots of code that
          we don't want to import into YaM)

	* There is now a --verbose option that can be used to turn on
          different levels of status messages.  The number of messages
          printed depend on the level of verbosity.  Roughly, verbose = 1
          will generate RddfPlanner messages that tell you stuff is
          happening.  Setting verbose = 2 will generate more detailed
          messages and will also turn on messages in OCPtSpecs (at level
          verbose-1).

        Things that are still missing: we don't set the angle of the vehicle
        at the end of the path.  This may be needed for U-turns.

   Release R1-00f (Wed Mar 14 17:02:10 2007):
	Fixed a small bug that put is in the opposite mode as asked (forward
	vs reverse) 

   Release R1-00e (Wed Mar 14 16:53:11 2007):
	Updated makefile to fix some linking errors.

   Release R1-00d (Sun Mar 11 21:45:22 2007):
	This version of rddfplanner has hooks for planning in reverse.  This
	consists of two main changes: (1) added an interface to tplanner
	through the ocpspecs library that can receive mode changes from
	tplanner and (2) added an interface to trajfollower that can command
	a change in direction via the superCon (pseudocon) interface.

	This code has not been tested yet, mainly because we don't yet have
	a way to command reverse (that I know of) and the latest release of
	trajfollower doesn't appear to have reverse capability yet.  The
	code has been tested when no reverse command is sent and works as
	before. 

	The code here can serve as a starting point for the equivalent
	capability in dplanner.  See lines 132-162 of RddfPlanner.cc.

   Release R1-00c (Sat Mar  3 19:11:41 2007):
	Found and fixed a bug which was causing a seg fault.  This version
	should now work equivalently to the dgc/trunk version with option
	'feasibilize' (not that no path type is required for the yam
	version).


	 ReleaseNotes extracts from rndf module
	 ........................................

   Release R1-00k (Sat Mar 17 12:53:22 2007):
	Added solid_yellow to lane boundary type

   Release R1-00j (Fri Mar 16 14:17:47 2007):
	Take into account the discontinuity of atan2

   Release R1-00i (Tue Mar 13 21:12:05 2007):
	Fixed findClosestGPSPoint function so it takes into account the direction of the lane

   Release R1-00h (Sat Mar 10 13:48:04 2007):
	Resolved the memory issues. (Valgrind does not complain about
	possible memory leak in RNDF anymore.) The core dump problem should
	be fixed as well.

   Release R1-00g (Sat Mar 10 11:32:41 2007):
	Fixed a problem in the deconstructor for RNDFs.  Memory was getting
	freed (delete'd) twice and this was causing core dumps in asim,
	where we create a temporary RNDF to set the initial condition.  For
	now I commented out the delete's, which I think were all
	duplicates.  I left a warning for someone to go and check to make
	sure we don't have a memory leak (and created a bug).

   Release R1-00f (Fri Mar  9 15:35:44 2007):
	Fixed the heading angle computation.

   Release R1-00e (Sat Mar  3 20:21:41 2007):
	Added a function for getting all the lanes in opposite direction.

   Release R1-00d (Thu Mar  1 22:16:55 2007):
	Added functions needed by the travgraph module but were previously internal to the mission planner.

   Release R1-00c (Sun Feb 25 21:05:32 2007):
	Moved all the RNDF and MDF files to the appropriate routes-* modules

   Release R1-00b (Tue Feb 20 17:11:38 2007):
	Fixed gloNavMap so it compiles against revision R2-00a of interfaces and skynet


	 ReleaseNotes extracts from sensnet module
	 ........................................

   Release R1-00n (Mon Mar 12 22:47:29 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

   Release R1-00m (Sat Feb 24 21:30:41 2007):
	Implemented sensnet_log_seek().

   Release R1-00l (Fri Feb 23 17:42:59 2007):
	Re-added some changes lost in the last release

   Release R1-00k (Fri Feb 23 17:33:10 2007):
	Moved sensnet-replay to its own module.
	Fixed sensnet_log_has_next().

   Release R1-00j (Fri Feb 23  0:14:22 2007):
  Lost some changes in the merge; put them back in.

   Release R1-00i (Wed Feb 21 23:44:28 2007):
  Tweaked replay API with addition of a "step" function.  Modules using "wait"
  should use "step" instead when running in replay mode.

   Release R1-00h (Mon Feb 19 21:58:20 2007):
	Added sensnet-replay, a tool to load multiple logs and replay
	them synchronized. It reads all the blobs from the specified
	logs and it sends them through sensnet.


	 ReleaseNotes extracts from skynet module
	 ........................................

   Release R2-00f (Fri Mar 16 20:14:37 2007):
        Modified skynet-logger to log a subset of messages  (fewer threads created). You can change the list of logged messages in the file skynet-logger.c
	c.
   Release R2-00e (Fri Mar 16 16:05:52 2007):
	Added skynet-logger and skynet-player programs.  These are copies of
	the 'author' and 'logplayer' programs from dgc/trunk, with headers
	updated to match YaM locations.  Stefano is going to work on getting
	these to be useful for logging and playback of skynet messages.

   Release R2-00d (Sat Mar 10 17:29:22 2007):
        Added conditional definition in the header file.	

   Release R2-00c (Sat Mar  3 14:02:41 2007):
	Minor update to fix Makefile for trunk/rddfplanner

   Release R2-00b (Sat Mar  3  9:25:51 2007):
	Added processing of --skynet-key and -S, for compatibility with
	command line option standards (bug 3151).  Everything is backward
	compatible, so --snkey is still processed.

   Release R2-00a (Tue Feb 20 15:26:57 2007):
	Moved sn_types to interfaces. Added SkynetContainer and got rid of unnecessary dependencies.


	 ReleaseNotes extracts from sparrow module
	 ........................................

   Release R1-01d (Sat Mar  3  8:20:59 2007):
	Minor release to fix Makefile problem that was causing warnings
	when new packages were created.  The 'cdd' program was incorrectly
	listed in the BIN_LINKS list.  No changes to interface or
	functionality.


	 ReleaseNotes extracts from stereofeeder module
	 ........................................

   Release R2-00c (Sun Mar 18  8:44:58 2007):
  Minor fix: -lm was missing for stereo_log_test in Makefile.yam

   Release R2-00b (Wed Mar 14 23:05:39 2007):
  Add mini-distro script for stereo log test.

   Release R2-00a (Mon Mar 12 23:21:04 2007):
  Big revision for new StereoImageBlob.

   Release R1-00q (Tue Mar  6 23:39:46 2007):
  Partially fixed PGR camera code for de-Bayering.  Also added the
  calibrated camera models and configuration files for the roof stereo
  pairs.  The camera-to-vehicle transform has not been done yet.

   Release R1-00p (Tue Mar  6  9:40:31 2007):
  Re-enabled raw image logging; useful when the cameras are not fully 
  calibrated.

   Release R1-00o (Mon Mar  5 16:38:49 2007):
  Minor release with improved FB display.  AA support has been deprecated.

   Release R1-00n (Sat Mar  3 12:06:29 2007):
  Added shutter/gain feedback and tweaked camera configuration files.
  Also removed the bogus type-casts from the previous release.

   Release R1-00m (Sat Feb 24 21:55:47 2007):
		Added explicit type casting from char * to (unsigned char *)  to
	some function calls in BBStereoCarera.cc and PGRSterreoCamera.cc. 
	I was getting compile errors on gclab which this fixed.

   Release R1-00l (Fri Feb 23  0:38:44 2007):
  Foo.

   Release R1-00j (Thu Feb 22 10:19:02 2007):
	Dummy release to get around YAM.

   Release R1-00i (Wed Feb 21 21:55:40 2007):
	Updating to work with new skynet update


	 ReleaseNotes extracts from tplanner module
	 ........................................

   Release R2-01d (Sun Mar 18  8:53:27 2007):
	Changed switching from uturn state so that we always switch from that state, no
	matter what traffic state we are in. It just checks what the current seggoal is,
	and if it is not uturn, and we have completed the uturn, then we transition.	

   Release R2-01c (Sat Mar 17 20:26:48 2007):
	Made tplanner more robust in intersections. Tested with complicated sequence and 
	works for the most part - still some issue with switching out of the uturn state, 
	but I believe that is because we end to close to an intersection in the opposite dir.

   Release R2-01b (Sat Mar 17 13:48:46 2007):
	Debugged intersection functions. added mplanner-tplanner comm during uturn. 
	uturn needs more debugging.	

   Release R2-01a (Fri Mar 16 12:07:52 2007):
	Traffic planner that uses skynettalker to communite with mission planner.

   Release R2-00d-ndutoit (Fri Mar 16 23:02:41 2007):
	Merging with new release of tplanner.

   Release R2-00d (Fri Mar 16 11:16:01 2007):
	Added robust goal and traffic state distance checking 
(based on hyper plane now, not just dist). Also adjusted 
stop.cc vel profile. Checked middle of segment (non stop) end 
of mission/checkpt crossing - seems ok now. Tested with new 
rddfplanner (planning to the end pts) and that also seems ok.

   Release R2-00c (Thu Mar 15 21:16:01 2007):
		turned on talker to listen to obstacle messages from map.  removed
	hard coded obstacle.

   Release R2-00b (Thu Mar 15 14:19:18 2007):
	Created testCState.cc to check one control state (specified) 
at a time. Updated UTurn.cc - now commands a reverse traj, and then 
a fwd traj to test switching of the stack. Removed dependency on 
OCPtSpecs by using new interfaces file (TpDpInterface.hh). 

   Release R2-00a (Tue Mar 13 15:19:07 2007):
	Added ocpParams to tplanner, and this struct is now populated in the planner.
	Implemented interface for ocpParams, and did initial testing of send function
	by running testOCPSpecs function (which receives the message).

   Release R2-00 (Tue Mar 13  0:18:57 2007):
	Updated state machines to separate transitions and mission complete 
checks. Also, tracked down a bug related to the changed segGoals interface.	

   Release R1-00i (Fri Mar  9 12:23:09 2007):
Updated tplanner to include MapElementTalker.hh in the new
	location.  No change in functionality.

   Release R1-00h (Fri Mar  9 10:35:21 2007):
	This is the new tplanner!!! The traffic and control state 
machines have been implemented. It uses the new map. The interface 
between mapper and tplanner has NOT been implemented. The interfaces 
with mplanner and dplanner has not changed. I do not use the GloNavMap 
anymore, but just obtain that data from map (which is instantiated from 
an rndf locally and not updated). 
	This version of the tplanner is stable for the following case 
only: drive down a road segment, come to a stop at an intersection, 
continue STRAIGHT through the intersection, and then drive down the road 
again. The files necessary to run this case are included in the release: 
tplanner_unittest.rndf, tplanner_unittest_straight.mdf, rddf.rddf (for 
asim). There are still bugs, including checking for mission complete 
(interaction with traffic state transitions), end of mission handling, 
and some others. 
	This version SHOULD be able to make left and right turns at the 
intersection, but that depends on map capability for the short term. 
Thus it has not been tested, and there are no guarentees. 
	
   Release R1-00g (Wed Feb 21 13:13:53 2007):
	Fixed TrafficPlanner.cc so it compiles against revision R2-00b of interfaces.

   Release R1-00f (Tue Feb 20 17:25:22 2007):
	Fixed include statements so this version of tplanner compiles against revision R2-00a of interfaces and skynet


	 ReleaseNotes extracts from trajfollower module
	 ........................................

   Release R1-01c (Sat Mar 17  1:21:27 2007):
	Trajfollower now sends the Estop pause and resume commands when it 
	tells adrive to shift.  Adrive needed these to work correctly.

   Release R1-01b (Thu Mar 15 13:33:30 2007):
	Fixed something related to switching between fwd and reverse 
(ask Chris and Tom for details).	

   Release R1-01a (Mon Mar 12 15:12:41 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

   Release R1-00e (Wed Mar  7 22:21:40 2007):
	Trajfollower changed to listen to old SuperCon commands with 
	respect to changing state (forward vs reverse) and I removed the 
	past history buffer overhead and made it listen for a reverse 
	trajectories just like it listens for forward trajectories.

   Release R1-00d (Sun Feb 25  8:22:58 2007):
	Copied over some of the (deprecated) superCon include files for use
	in debugging (and eventually replacing) this interface.  The files
	are not currently used, but are put into the include directory so
	that pseudocon can access them.  No change to trajfollower
	functionality. 

   Release R1-00c (Tue Feb 20 17:31:26 2007):
	Fixed include statements so this version trajfollower compiles
	against revision R2-00a of interfaces and skynet


	 ReleaseNotes extracts from trajutils module
	 ........................................

   Release R1-00d (Thu Mar 15 11:50:17 2007):
	Updated the Traj::feasiblize function to do a better (and correct)
	job of computing accelerations.  Previous code was computing either
	longitudianl or lateral acceleration, but not the proper vector
	acceleration.  Current code does a (crude) vector calculation.  This
	should give better accelerations for use in feedfoward computation
	in trajfollower.  Checked by hand on sample path, but hasn't yet been
	run in full loop.

   Release R1-00c (Tue Feb 20 17:28:30 2007):
	Fixed CPath and TrajTalker so they compile against revision R2-00a
	of interfaces and skynet


Release R1-00 (Tue Feb 13  9:01:39 2007):
	Created.
