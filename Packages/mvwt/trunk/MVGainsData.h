///
/// \file MVGainsData.hh
/// \brief Gains data 
/// 
/// \author Vanessa Jonsson 
/// \date 11 Feb 2008
/// 
/// Gains packet specification
/// 

#ifndef __MVGAINSDATA_H__
#define __MVGAINSDATA_H__


typedef struct
{
  
  // Rather then send architecture-dependent floating point values over the
  // network, we're using the ieee754 standard

  // First fan gains 
   float x1;
   float y1;
   float t1;
  
   float xdot1;
   float ydot1;
   float tdot1;
  
  // Second fan gains
   float x2;
   float y2;
   float t2;
  
   float xdot2;
   float ydot2;
   float tdot2;
  
} GainsData;

#endif
