#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <ieee754.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "MvwtFollower.h"
#include "../../include/mvwt/MVParameters.h"
#include "../../include/mvwt/MVControlPacket.h"
#include "../../include/falcon/traj.h"


/// Global Variables

/// Output file names

FILE* m_logFP;
FILE* m_mvwtfp;
FILE* m_ECSFP;

/// The current trajectory
TRAJ_DATA* m_traj;

/// Hostname 
const char *m_host; 

/// Vehicle ID number
int m_vehicleId;

/// Vision socket number
int m_visionSocket;

/// Vision port
int m_visionPort;

/// Command socket number
int m_commandSocket;

/// The current vision packet
float m_state[STDLENGTH];

/// The control forces and numbers for logging
float m_control[10];

/// The current gyro data 
float m_gyroData; 

/// Whether we are in the first run 
int m_firstRun = 1;  

/// The initial condion for the estimator  
float m_initCond[11];  

/// The initial condion for the estimator  
double m_lastInput[3];  

/// The control packet to be sent to the hovercraft 
ControlPacket m_cp; 

/// Observer matrices 
MATRIX *Q, *Rw, *RwInv, *P,*old_P, *dP, *xhat;
MATRIX *A, *B, *C, *Ct, *L, *y, *F;
MATRIX *old_xhat, *dxhat, *cor;

/// The errors in state with
/// with respect to projection
/// onto current trajectory
float m_trajError[STDLENGTH];

/// Pointers to the data in the point on the trajectory deemed closest to the hovercraft
double m_ptrajVector[STDLENGTH];

/// THe data in the point on the traj deemed most closest to us
double m_trajVector[STDLENGTH];

/// The integral errors
/// they build up over time
/// and are reset when within a threshold of zero
/// right now only using integrator on velocity.
float m_integralErrorRight[STDLENGTH];
float m_integralErrorLeft[STDLENGTH];


///  The thresholds for reseting the integral errors
float m_windupThreshold[STDLENGTH];  

/// The current point on the trajectory that
/// nearest to
float m_trajPoint[STDLENGTH];

/// Index of nearest point- remembered to avoid going backwards
int m_nearestTrajIndex;

/// Proportional Gains data
GainsData m_gains;

/// Integral Gains Data
GainsData m_integralGains;

/// Controller type
int m_ctrlType;

/// File descriptor
fd_set m_readfds;

/// Last time update of the control loop 
time_t m_lastTime; 

/// Time step 
MATRIX* dT; 

int initialize() {

  int i;
  /* Initialize variables */
  m_lastInput[0]= 0;
  m_lastInput[1]= 0;
  m_lastInput[2]= 0;

  for(i=0; i <9; i++) {
    m_integralErrorRight[i] = 0;
    m_integralErrorLeft[i] = 0;
  }
  
  m_nearestTrajIndex = 0;      

  if (initVisionSocket(m_visionPort) == -1) {
    return 1;
  }

  FD_ZERO(&m_readfds);
  FD_SET(m_visionSocket, &m_readfds);
  printf("Initialized the vision_socket.\n");       

  initializeCommSocket();
  printf("Initialized comm socket.\n");       
  initializeObserver();
  printf("Initialized observer.\n");       

}


int initializeCommSocket() {

  struct sockaddr_in sa;
  struct hostent *he;

  if ((m_commandSocket = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
    fprintf(stderr, "FATAL: socket %s\nCould not create socket for host %s\n",
	    strerror(errno), m_host);
    return -1;
  }
  
  if ((he = (struct hostent *)gethostbyname(m_host)) == NULL) {
    fprintf(stderr, "FATAL: gethostbyname %s\nCould not lookup host %s\n",
	    strerror(errno), m_host);
    return -1;
  }

  sa.sin_family = AF_INET;
  sa.sin_port = htons(DEFAULT_VEHICLE_PORT);
  sa.sin_addr = *(struct in_addr *)he->h_addr_list[0];
  
  if (connect(m_commandSocket, (struct sockaddr *)&sa, sizeof(sa)) == -1) {
    fprintf(stderr, "FATAL: connect: %s\nCould not set connect to address for commands.\n",
	    strerror(errno));
    return -1;
  }
}


int initializeObserver() {

  MATRIX *initErrorT;
  double thetahat , sigX, sigY, sigTHETA, sigXDOT, sigYDOT, sigTHETADOT; 

  A = mat_create();       mat_set_name(A, "A");
  B = mat_create();       mat_set_name(B, "B");
  C = mat_create();       mat_set_name(C, "C");
  
  Q = mat_create();       mat_set_name(Q, "Q");
  Rw = mat_create();      mat_set_name(Rw, "Rw");
  RwInv = mat_create();   mat_set_name(RwInv, "RwInv");

  P = mat_create();       mat_set_name(P, "P");
  dP = mat_create();      mat_set_name(dP, "dP");

  //dT = mat_create();      mat_set_name(dT, "dT");

  L = mat_create();       mat_set_name(L, "L");
  xhat = mat_create();       mat_set_name(xhat, "xhat");
  old_xhat = mat_create();   mat_set_name(old_xhat, "old_xhat");
  old_P = mat_create(); mat_set_name(old_P, "old_P");
  dxhat = mat_create();   mat_set_name(dxhat, "dxhat");
  cor = mat_create();   mat_set_name(cor, "cor");
  
  y = mat_create();     mat_set_name(y, "y");
  F = mat_create();     mat_set_name(F, "F");

  mat_resize(A, 6, 6);

  /* C: Output matrix 6x6 matrix only x, y, theta states are measured */
  mat_resize(C, 6, 6);
  mat_element_set(C, 0, 0, 1.0);
  mat_element_set(C, 1, 1, 1.0);
  mat_element_set(C, 2, 2, 1.0);
  mat_element_set(C, 3, 3, 1.0);
  mat_element_set(C, 4, 4, 1.0);
  mat_element_set(C, 5, 5, 1.0);

  /* Ct: Output matrix 6x3 matrix */
  mat_resize(Ct, 6, 6);
  mat_transpose(Ct,C);

  /** P, old_P: Covariance  matrix 6x6 matrix  **/
  mat_resize(P, 6, 6);
  mat_resize(old_P, 6, 6);

  /** dT, time increment matrix **/
  //mat_resize(dT, 1, 1);
  //mat_element_set(dT, 0, 0, 0.2);

  sigX = 0.00029827*0.00029827;
  sigY = 0.00006082*0.00006082;
  sigTHETA = 0.0029033*0.0029033;
  sigXDOT = 0.02566*0.02566;
  sigYDOT = 0.0079263*0.0079263;
  sigTHETADOT = 0.24625*0.24625;

  /* Rw */
  mat_resize(Rw, 6, 6);
  mat_element_set(Rw, 0, 0, sigX);
  mat_element_set(Rw, 1, 1, sigY);
  mat_element_set(Rw, 2, 2, sigTHETA);
  mat_element_set(Rw, 3, 3, sigXDOT);
  mat_element_set(Rw, 4, 4, sigYDOT);
  mat_element_set(Rw, 5, 5, sigTHETADOT);

  /* RwInv */
  mat_resize(RwInv, 6, 6);
  mat_inverse(RwInv, Rw);

  mat_resize(dP, 6, 6);
  mat_resize(xhat, 6, 1);
  mat_resize(old_xhat, 6, 1);
  mat_resize(dxhat, 6, 1);
  mat_resize(cor, 6, 1);
  mat_resize(y, 6, 1);
  mat_resize(F, 6, 2); 

  mat_reset(dP);
  mat_reset(xhat);
  mat_reset(old_xhat);
  mat_reset(dxhat);
  mat_reset(cor);
  mat_reset(y);
  mat_reset(F);

}

void run() {

  getMeasurements();
  correctState();
  //control(); 
  predictState();
  
}
int deinitialize() {
  
  // Close any sockets that are open
  if (m_commandSocket != -1)
    close(m_commandSocket);
  
  if (m_visionSocket != -1)
    close(m_visionSocket);
  
  
   if(Q != NULL) mat_free(Q);
   if(Rw != NULL) mat_free(Rw);
   if(RwInv != NULL) mat_free(RwInv);
   if(P != NULL) mat_free(P);
   if(dP != NULL) mat_free(dP);
   if(xhat != NULL) mat_free(xhat);
   if(old_xhat != NULL) mat_free(old_xhat);
   if(dxhat != NULL) mat_free(dxhat);
   if(A != NULL) mat_free(A);
   if(B != NULL) mat_free(B);
   if(C != NULL) mat_free(C);
   if(Ct != NULL) mat_free(Ct);
   if(L != NULL) mat_free(L);
   if(y != NULL) mat_free(y);
   if(F != NULL) mat_free(F);
   
}

void control() {

  m_state[X] = mat_element_get(y,0, 0);
  m_state[Y] = mat_element_get(y,1, 0);
  m_state[THETA] = mat_element_get(y,2, 0);
  m_state[XDOT] = mat_element_get(y,3, 0);
  m_state[YDOT] = mat_element_get(y,4, 0);
  m_state[THETADOT] = mat_element_get(y,5, 0);

  if (LAT == m_ctrlType) {
    calculateStateError();
    lateralControl();
    sendControlPacket();
    logTelemetry(m_logFP);
  }
}

void lateralControl() {

  float u1_p, u2_p, u1_i, u2_i, u1_ff, u2_ff, u1, u2;
  float forces[2];

  int deadband_offset = 55;

  u1_p = -m_gains.y1*m_trajError[Y] - m_gains.t1*m_trajError[THETA] - m_gains.ydot1*m_trajError[YDOT]- 
    m_gains.tdot1*m_trajError[THETADOT];

  u2_p = -m_gains.y2*m_trajError[Y] - m_gains.t2*m_trajError[THETA] - m_gains.ydot2*m_trajError[YDOT]- 
    m_gains.tdot2*m_trajError[THETADOT];

  u1_i =  -m_integralGains.ydot1*m_integralErrorRight[YDOT];
  u2_i =  -m_integralGains.ydot2*m_integralErrorLeft[YDOT]; 
  
  u1_ff = m_trajError[U1FF];
  u2_ff = m_trajError[U2FF];
  
  // total the forces yes integral control
  //u1 = u1_p + u1_i + u1_ff;
  //u2 = u2_p + u2_i + u2_ff;
  // no integral control

  //FIX add ff term 
  u1 = u1_p + u1_ff + 0.2;
  u2 = u2_p + u2_ff + 0.2; 
  
  if ( (maximum(0, minimum(u1,FMAX)) - u1) == 0)  // the right actuator shouldn't be saturated
  updateIntegralErrorRight();

  if ( (maximum(0, minimum(u2,FMAX)) - u2) == 0)  // the left actuator shouldn't be saturated
  updateIntegralErrorLeft();

  // normalize the commands (still in units of Newtons)
  forces[0] = maximum(0, minimum(u1,FMAX));
  forces[1] = maximum(0, minimum(u2,FMAX));

  
  m_control[0] = u1_p;
  m_control[1] = u2_p;
  m_control[2] = u1_i;
  m_control[3] = u2_i;
  m_control[4] = u1_ff;
  m_control[5] = u2_ff;
  m_control[6] = forces[0];
  m_control[7] = forces[1];
  m_control[8] = u1;
  m_control[9] = u2;
  
  m_lastInput[0] = forces[0]; 
  m_lastInput[1] = forces[1];
  m_lastInput[2] = LIFT_ON;

  sleep(0.2);
 }

void sendControlPacket() {

  int num_bytes;

  m_cp.leftFan = m_lastInput[0]; 
  m_cp.rightFan = m_lastInput[1];
  m_cp.liftFan =  m_lastInput[2];
  
  printf("Init comm sock OK\n");
  
  if ((num_bytes = send(m_commandSocket, &m_cp, sizeof(m_cp), 0)) == -1) {
    fprintf(stderr, "send: %s\nCould not send command to robot.\n",
	    strerror(errno));
    return;
  }
  
  printf("Sent command %f %f %f\n", m_cp.leftFan, m_cp.rightFan, m_cp.liftFan);
  
  if (num_bytes < sizeof(m_cp)){
    fprintf(stderr, "Could not send whole command.\n");
    return;
  }
}


int closestTrajPoint(int previousClosestIndex) {

  int nearestIndex = 0;
  double  nearestDistSq;
  double  currentDistSq;
  double rowVal[9];
  double time;
  int i = 0; 

  // Compute the nearest point on the trajectory to where we currently are
  nearestDistSq = 1.0e20; //FIX what is this? 
  for(i=previousClosestIndex; i<(*traj_rows)(m_traj); i++) 
  {
    (*traj_row)(m_traj, i, &time, rowVal);
    currentDistSq =
      pow(m_state[X] - rowVal[X-1],2.0) +
      pow(m_state[Y] - rowVal[Y-1] ,2.0);
    
    if(currentDistSq < nearestDistSq) {
      nearestDistSq = currentDistSq;
      nearestIndex = i;
    }
  }
    return nearestIndex; 
}


void calculateStateError() {

  int i, status, trajpoint; 
  double trajTime;
  float refVel, ownVel; 

  /// Find the nearest point on the desired trajectory 
  /// normal  to the current vehicle orientation.

  // Get the trajectory entry for the segment we are on
  m_nearestTrajIndex = closestTrajPoint(m_nearestTrajIndex);
  

  // Get the start point and end point
  (*traj_row)(m_traj, m_nearestTrajIndex, &trajTime, m_trajVector);
  
  // Shift all the values becase the traj_row function strips 
  // off the time column of the trajectory row. 
  m_trajVector[U2FF] = m_trajVector[U1FF];
  m_trajVector[U1FF] = m_trajVector[THETADOT];
  m_trajVector[THETADOT] = m_trajVector[YDOT];
  m_trajVector[YDOT] = m_trajVector[XDOT];
  m_trajVector[XDOT] = m_trajVector[THETA];
  m_trajVector[THETA] = m_trajVector[Y];
  m_trajVector[Y] = m_trajVector[X];
  m_trajVector[X] = m_trajVector[T];
  m_trajVector[T] = 0;
  

  
  printf(" m_trajVector = (%8f, %8f, %8f, %8f, %8f, %8f, %8f, %8f, %8f)\n",
	 m_trajVector[T], m_trajVector[X], m_trajVector[Y], m_trajVector[THETA], m_trajVector[XDOT], m_trajVector[YDOT], 
	 m_trajVector[THETADOT], m_trajVector[U1FF], m_trajVector[U2FF]);
  
  // Compute lateral distance in body coordinates
  m_trajError[T] = 0;
  m_trajError[X] = 0;


  //Compute y error 
  m_trajError[Y] =
   - (m_state[X] - m_trajVector[X])*sin(m_trajVector[THETA]) + 
   (m_state[Y] - m_trajVector[Y])*cos(m_trajVector[THETA]);

  // Compute error in heading versus angle along the path
  m_trajError[THETA] = m_state[THETA] - m_trajVector[THETA];

  // Figure out the velocity along the path

  refVel = sqrt(pow(m_trajVector[XDOT], 2.0) +
  		     pow(m_trajVector[YDOT], 2.0));

  ownVel = sqrt(pow(m_state[XDOT], 2.0) +
  		     pow(m_state[YDOT], 2.0));

  // Compute the lateral velocity error in body coordinates
  m_trajError[XDOT] = 0;
  m_trajError[YDOT] = ownVel - (refVel * sin(m_trajError[THETA]));

  // Compute heading rate error along the path
  m_trajError[THETADOT] = m_state[THETADOT] - m_trajVector[THETADOT];

  m_trajError[U1FF] = m_trajVector[U1FF];
  m_trajError[U2FF] = m_trajVector[U2FF];

 printf(" trajError = (%8f, %8f, %8f, %8f, %8f, %8f, %8f, %8f)\n",
	 m_trajError[X], m_trajError[Y], m_trajError[THETA], m_trajError[XDOT], m_trajError[YDOT], 
	 m_trajError[THETADOT], m_trajError[U1FF] , m_trajError[U2FF] );  
}



void updateIntegralErrorRight()
{
    float t_step = 0.2;
    m_integralErrorRight[X] += t_step*m_trajError[X];
    m_integralErrorRight[Y] += t_step*m_trajError[Y];
    m_integralErrorRight[THETA] += t_step*m_trajError[THETA];
    m_integralErrorRight[XDOT] += t_step*m_trajError[XDOT];
    m_integralErrorRight[YDOT] += t_step*m_trajError[YDOT];
    m_integralErrorRight[THETADOT] += t_step*m_trajError[THETADOT];
}

void updateIntegralErrorLeft()
{ 
    float t_step = 0.2;
    m_integralErrorLeft[X] += t_step*m_trajError[X];
    m_integralErrorLeft[Y] += t_step*m_trajError[Y];
    m_integralErrorLeft[THETA] += t_step*m_trajError[THETA];
    m_integralErrorLeft[XDOT] += t_step*m_trajError[XDOT];
    m_integralErrorLeft[YDOT] += t_step*m_trajError[YDOT];
    m_integralErrorLeft[THETADOT] += t_step*m_trajError[THETADOT];
}  


void getMeasurements() {
  if (FD_ISSET(m_visionSocket, &m_readfds)) {
    printf("Vision socket is set.\n");       
    receiveVisionData();
  }
}


void estimateInitialState() {
  int i = 0;
  //while(i!= 1000) {
    getMeasurements();
    correctState();
    i++;
    //}
}

void correctState() {

  MATRIX *tmp1, *tmp2, *tmp3, *tmp4, *tmp5, *tmp6, *tmp7;
  MATRIX *At, *old_xhatT;

  double thetahat, theta;
  double dT = 0.2;


  tmp1 = mat_create(); 
  tmp2 = mat_create(); 
  tmp3 = mat_create(); 
  tmp4 = mat_create();
  tmp5 = mat_create(); 
  tmp6 = mat_create(); 
  tmp7 = mat_create(); 

  At = mat_create();
 
  if (m_firstRun) {
    old_xhatT = mat_create(); 
    mat_copy(old_xhat, y);
    mat_transpose(old_xhatT, old_xhat);
    mat_mult(old_P, old_xhat, old_xhatT);
    printf("INITIAL COVARIANCE P\n");
    mat_print(old_P);
    printf("INITIAL STATE XHAT\n");
    mat_print(old_xhat);

  }

  thetahat = mat_element_get(old_xhat, THETA-1, 0);    

  /* Linearize the dynamics wrt the last predicted state */
  
  mat_reset(A);
  mat_element_set(A, 0, 3, 1.0);
  mat_element_set(A, 1, 4, 1.0);
  mat_element_set(A, 2, 5, 1.0);
  mat_element_set(A, 3, 2, -(sin(thetahat))/HMASS);
  mat_element_set(A, 3, 3, -LINFRIC/HMASS);
  mat_element_set(A, 4, 2, (cos(thetahat))/HMASS);
  mat_element_set(A, 4, 4, -LINFRIC/HMASS);
  mat_element_set(A, 5, 5, -ROTFRIC/HMASS);


  /* Calculate dP based on predicted covariance.  
      dP = A * old_P + old_P * A' - old_P * C' * inv(Rw) * C * old_P */

  mat_mult(tmp1, A, old_P);
  mat_transpose(At, A);
  mat_mult(tmp2, old_P,At);
  mat_add(tmp3,tmp1,tmp2);
   
  mat_reset(tmp1);
  mat_reset(tmp2);

  mat_mult(tmp4,old_P, Ct); 
  mat_mult(tmp5,tmp4, RwInv); 
  mat_mult(tmp6,tmp5,C); 


  // mat_reset(tmp1);
  //mat_reset(tmp2);

  mat_mult(tmp7,tmp6,old_P); 
  mat_subtract(dP,tmp3,tmp7);
 
  mat_print(dP);

  mat_reset(tmp1);

  /* Calculate the corrected covariance by Euler integration
      P = old_P + dT*dP */

  mat_scale(tmp1,dP,dT);
  mat_add(P,old_P, tmp1);

  /** Calculate L based on the corrected covariance L = A*P*C' (Rw + C*P*C' ) **/

  /** Reset tmp matrices **/

  mat_reset(tmp1);
  mat_reset(tmp2);
  mat_reset(tmp3);

  mat_mult(tmp1,P,Ct);
  mat_mult(tmp2,C,tmp1);
  mat_add(tmp3, Rw, tmp2);
  mat_mult(tmp2, A, tmp1);
  mat_mult(L,tmp2,tmp3);

  // printf("OPT OBSERVER GAIN L\n");
  //mat_print(L);

  /** Reset tmp matrices **/

  mat_reset(tmp1);
  mat_reset(tmp2);
  mat_reset(tmp3);


  /** Calculate corrected xhat to use in control xhat = old_xhat + L *(y - C*old_xhat) **/

  mat_mult(tmp1, C, old_xhat);
  mat_subtract(tmp2, y, tmp1);
  mat_mult(tmp3, L, tmp2);
  mat_add(xhat, old_xhat, tmp3);


  /*Make sure theta is in the 2*Pi range */

  theta = mat_element_get(xhat, 2,0);

  while(theta > 2*PI) {
    theta = theta-2*PI; 
  }

  if (theta < 0)
    theta = theta + 2*PI; 

  theta = mat_element_set(xhat, 2,0,theta);
  m_firstRun = 0;

  printf("CORRECTED STATE\n");
  mat_print(xhat);

  printf("CORRECTED COVARIANCE MATRIX\n");
  mat_print(P);

  mat_free(tmp1);
  mat_free(tmp2);
  mat_free(tmp3);
  mat_free(tmp4);
  mat_free(tmp5);
  mat_free(tmp6);
  mat_free(tmp7);
}
 
void predictState() {

  /* Predict state using non linear dynamics and last control input  */
  double u1, u2, X_, Y_, THETA_, ddX, ddY, ddTHETA, dX, dY, dTHETA, X, Y, THETA; 

  MATRIX *xhatFRIC,*tmp1,*tmp2,*tmp3,*tmp4,*tmp5,*tmp6,*tmp7; 
  MATRIX *At;
  double dT= 0.2;
  double theta;
 
  xhatFRIC = mat_create();       

  tmp1 = mat_create();       
  tmp2 = mat_create();       
  tmp3 = mat_create();       
  At = mat_create();

  u1 = m_lastInput[0];
  u2 = m_lastInput[1];
 
  mat_scale(xhatFRIC,xhat,LINFRIC);

  printf("PREDICTION STEP xhat ");
  mat_print(xhat);

  ddX = ((u1 + u2)*cos(mat_element_get(xhat,2,0)) - mat_element_get(xhatFRIC,3,0))/HMASS;  
  ddY = ((u1 + u2)*sin(mat_element_get(xhat,2,0)) - mat_element_get(xhatFRIC,4,0))/HMASS;  
  ddTHETA = (u1 - u2)*ROTFRIC - ROTINERT* mat_element_get(xhat,5,0);

  /* Calculate the corrected covariance by Euler integration */
  mat_reset(old_xhat);

  dX = mat_element_get(xhat,3,0) + dT*ddX;
  dY = mat_element_get(xhat,4,0) + dT*ddY;
  dTHETA = mat_element_get(xhat,5,0) + dT*ddTHETA;

  X_ = mat_element_get(xhat,0,0) + dT*dX;
  Y_ = mat_element_get(xhat,1,0) + dT*dY;
  THETA_ = mat_element_get(xhat,2,0) + dT*dTHETA;

  printf("ddX, ddY, ddTHETA_%f,%f,%f \n",ddX, ddY, ddTHETA);
  printf("dX, dY, dTHETA_%f,%f,%f \n",dX, dY, dTHETA);
  printf("X_, Y_, THETA_%f,%f,%f \n", X_, Y_, THETA_);

  mat_element_set(old_xhat, 0, 0, X_);
  mat_element_set(old_xhat, 1,0, Y_);
  mat_element_set(old_xhat, 2,0, THETA_);

  mat_element_set(old_xhat, 3,0, dX);
  mat_element_set(old_xhat, 4,0, dY);
  mat_element_set(old_xhat, 5,0, dTHETA);


  theta = mat_element_get(old_xhat, 2,0);

  while(theta > 2*PI) {
    theta = theta-2*PI; 
  }

  if (theta < 0)
    theta = theta + 2*PI; 

  mat_element_set(old_xhat, 2,0,theta);

  printf("PREDICTED xhat:");
  mat_print(old_xhat);

  /* Predict covariance using non-linear dynamics */

  /* Linearize A wrt predicted state */
  mat_reset(A);
  mat_element_set(A, 0, 3, 1.0);
  mat_element_set(A, 1, 4, 1.0);
  mat_element_set(A, 2, 5, 1.0);
  mat_element_set(A, 3, 2, -(sin(THETA_))/HMASS);
  mat_element_set(A, 3, 3, -LINFRIC/HMASS);
  mat_element_set(A, 4, 2, (cos(THETA_))/HMASS);
  mat_element_set(A, 4, 4, -LINFRIC/HMASS);
  mat_element_set(A, 5, 5, -ROTFRIC/HMASS);


  /* Calculate dP based on corrected covariance.
     dP = A * P + P * A' - P * C' * inv(Rw) * C * P */

  /* A * P + P * A' */
  mat_mult(tmp1, A, P);
  mat_transpose(At, A);
  mat_mult(tmp2, P,At);
  mat_add(tmp3,tmp1,tmp2);
   
  mat_reset(tmp1);
  mat_reset(tmp2);

  /* P * C' * inv(Rw) * C * P */
  mat_mult(tmp1,P, Ct); 
  mat_mult(tmp2,tmp1, RwInv);
  mat_reset(tmp1);
  mat_mult(tmp1,tmp2,C); 
  mat_reset(tmp2);
  mat_mult(tmp2,tmp1,P);

  mat_print(A); 

  /* dP = A * P + P * A' - P * C' * inv(Rw) * C * P */
  mat_subtract(dP,tmp3,tmp2);
 
  /** Calculate the prediction covariance by Euler integration
      old_P = P + m_dT*dP **/

  mat_reset(tmp1);
  mat_scale(tmp1,dP, dT);
  mat_add(old_P,P, tmp1);


}

int initVisionSocket()
{
  struct sockaddr_in sa;

  // Create the vision socket
  if ((m_visionSocket = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
      fprintf(stderr, "FATAL: socket: %s\nCould not create vision socket.\n",
	      strerror(errno));
      return -1;
    }

  // We will receive commands on the local port m_visionPort
  sa.sin_family = AF_INET;
  sa.sin_port = htons(m_visionPort);
  sa.sin_addr.s_addr = htonl(INADDR_ANY);

  // Bind the socket so we can receive commands
  if (bind(m_visionSocket, (struct sockaddr *)&sa, sizeof(sa)) == -1)
    {
      fprintf(stderr, "FATAL: bind: %s\nCould not bind vision socket to local port %d.\n",
	      strerror(errno), m_visionPort);
      return -1;
    }

  return 0;
}

int receiveVisionData() {


  int num_bytes;
  int sret;
  float fanforces[2];
  VisionPacket vp;	
  struct timeval tv;

  static char visible = 0;
  static uint8_t leftlevel = 0; 
  static uint8_t rightlevel = 0; 
  static uint8_t liftlevel = 0;
  static int dropped = 0;

  do
    {
      // Read the next packets-worth of data off the network interface
      if ((num_bytes = recv(m_visionSocket, &vp, sizeof(vp), MSG_WAITALL)) == -1)
	{
	  fprintf(stderr, "recv: %s\nCould not receive vision packet\n");
	  return 0;
	}

      printf("Received packets of vision data\n");

      // Make sure that we got a whole packet
      if (num_bytes < sizeof(vp))
	{
	  fprintf(stderr, "Incomplete vision packet dropped. Size = %d\n", num_bytes);
	  return 0;
	}


      // If we've lost a packet,
      if (((union ieee754_float *)(&vp.vehicle[m_vehicleId].x))->f == 0)
	{
	  // Use the last theta as long as we haven't lost too many
	  if (dropped < MAX_FRAME_SKIP)
	    dropped++;
	  // If we have lost too many, we've become invisible
	  else
	    visible = 0;
	}

      // Otherwise, extract this robot's current angle from the vision packet
      //else
      //{

      // Copy the new vision data to where the controller can use it
      // If it is zero, then use the last state

      if ((0 != vp.vehicle[m_vehicleId].x) && (0 != vp.vehicle[m_vehicleId].y) 
	  && (0 != vp.vehicle[m_vehicleId].theta) && (0 != vp.vehicle[m_vehicleId].xdot) 
	  && (0 != vp.vehicle[m_vehicleId].ydot) && (0 != vp.vehicle[m_vehicleId].thetadot)) {

	mat_element_set(y, 0,0,vp.vehicle[m_vehicleId].x);
	mat_element_set(y, 1,0,vp.vehicle[m_vehicleId].y);
	mat_element_set(y, 2,0,vp.vehicle[m_vehicleId].theta);
	mat_element_set(y, 3,0,vp.vehicle[m_vehicleId].xdot);
	mat_element_set(y, 4,0,vp.vehicle[m_vehicleId].ydot);
	mat_element_set(y, 5,0,vp.vehicle[m_vehicleId].thetadot);

      }

      printf("MEASUREMENT\n");
      mat_print(y);

      // So we've dropped zero consecutive packets and the vehicle is visible
      dropped = 0;
      visible = 1;

      sret = 0;
	 
      // Now do a select with a zero-time timeout. This will effectively test
      // to see if there any data to be read on the vision socket.

	 
      /* 	 FD_ZERO(&m_readfds); */
      /* 	 FD_SET(m_visionSocket, &m_readfds); */
      /* 	 tv.tv_sec = 0; */
      /* 	 tv.tv_usec = 0; */

      /* 	 if ((sret = select(m_visionSocket + 1, &m_readfds, NULL, NULL, &tv)) == -1) */
      /* 	 { */
      /* 	     fprintf(stderr, "FATAL: select %s\nSelect failed.\n", strerror(errno)); */
      /* 	     return -1; */
      /* 	 } */

      // Loop and get the next packet if it has already arrived
    } while (sret);
}

int isTrajComplete() {
  /* Calculate the current state and compare to 
     the end point of the receding horizon trajectory */
  //printf("isTrajComplete() FALSE");
  return 0; //false
}


float minimum( float a, float b ) {
  
  if (a < b) return a;
  else return b;

}

float maximum( float a, float b ) {

  if (a > b) return a;
  else return b;

}


int logTelemetry(FILE *fp)

{
  int i;
  for (i=1; i<9; i++){
    fprintf(fp, "%f  ",(m_state[i]));
  }
  fprintf(fp, "999 ");
  for (i=1; i<9; i++){
    fprintf(fp, "%f  ",(m_trajError[i]));
  }
   fprintf(fp, "999 ");
  for (i=1; i<9; i++){
    fprintf(fp, "%f  ",(m_integralErrorRight[i]));
  }
  fprintf(fp, "999 ");
  for (i=1; i<9; i++){
    fprintf(fp, "%f  ",(m_integralErrorLeft[i]));
  }
  fprintf(fp, "999 ");
  for (i=1; i<9; i++){
    fprintf(fp, "%f  ",(m_trajVector[i]));
   }
  fprintf(fp, "999 ");
  for (i=0; i<10; i++){
    fprintf(fp, "%f  ",(m_control[i]));
  }
  
  
  fprintf(fp,"\n");
  return 0;
}



