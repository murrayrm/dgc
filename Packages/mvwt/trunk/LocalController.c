#include <ieee754.h>
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <linux/serial.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "bathw.h"
#include <sys/timeb.h>

#include"../../include/mvwt/MVControlPacket.h"

#include "LocalController.h"


// We'll use this just for now. Maybe we'll come up with a more robust command type later
typedef struct 
{
    uint8_t cmd;
    uint8_t n_args __attribute__ ((packed));
    uint8_t arg __attribute__ ((packed));
} FanCommand;

int serialFD = -1;
int net_fd = -1;
float gyroValue = 0;

struct termios *oldTTYserial = NULL;
uint8_t lift_val;




int initialize() {

    int fdflags;
    if (initializeSerial(DEFAULT_BAUD) == -1) {
	deinitialize();
	return 1;
    }

    if ((fdflags = fcntl(serialFD, F_GETFL)) == -1) {
	fprintf(stderr, "Could not get file flags\n");
	return 1;
    }

    if (enableGyro() == -1) {
	deinitialize();
	return 1;
    }

    if (initializeNetwork(DEFAULT_COMMAND_PORT) == -1) {
	deinitialize();
	return 1;
    }

    sendCommand('l', 0);
    sendCommand('r', 0);
    sendCommand('u', 0);

}

void deinitialize()
{
    if (oldTTYserial != NULL)
	if (tcsetattr(serialFD, TCSANOW, oldTTYserial) == -1)
	    fprintf(stderr, "tcsetattr: %s\nCould not restore serial port settings.\n", 
		    strerror(errno));

    if (net_fd != -1)
	close(net_fd);

    if (serialFD != -1)
	close(serialFD);
}

int run() {

    int n;
    int width;
    fd_set readfds;
    fd_set exfds;

    FD_ZERO(&readfds);
    FD_SET(net_fd, &readfds);
    FD_SET(serialFD, &readfds);
    
    FD_ZERO(&exfds);
    FD_SET(net_fd, &exfds);
    FD_SET(serialFD, &exfds);
    
    width = (net_fd > serialFD ? net_fd : serialFD) + 1;
    while (1)
    {

	printf("Selecting\n");
	if (select(width, &readfds, NULL, &exfds, NULL) == -1)
	{
	    fprintf(stderr, "FATAL: select: %s\nSelect failed.\n",
		    strerror(errno));
	    return 1;
	}
	printf("Done selecting\n");

	if (FD_ISSET(net_fd, &exfds))
	    fprintf(stderr, "Exception on net_fd\n");

	if (FD_ISSET(serialFD, &exfds))
	    fprintf(stderr, "Exception on serialFD\n");

	if (FD_ISSET(net_fd, &readfds) &&
		handleNetwork() == -1)
	    break;

	printf("about to get gyro measurement\n");

	getGyroMeasurement();

	printf("Gyro measurement: %f\n", gyroValue);
	if (FD_ISSET(serialFD, &readfds) &&
		discardSensors() == -1)
	    break;
    }
    printf("\n");

    sendCommand('l', 0);
    sendCommand('r', 0);
    sendCommand('u', 0);

    deinitialize();

}

int enableGyro() {

  if(bathw_init(DEFAULT_COMM_DEVICE, DEFAULT_COMM_SPEED) == -1) {
      fprintf(stderr, "Could not initialize Bat hardware: %s\n", bathw_strerror());
      return 1;
  }

  printf("Enabling gyro, please wait....\n");
  
  if(bathw_enable_gyro("gyro_calib.data", &gyroValue) == -1) {
      fprintf(stderr, "Could not enable gyro sensor: %s\n", bathw_strerror());
      return 1;
  }
	
  printf("Done!\n");

}

int initializeSerial(speed_t baud)
{
    static struct termios currTTYserial;
    struct termios newTTYserial;

    // Open the serial port
    if ((serialFD = open("/dev/ttyS0", O_RDWR)) == -1)
    {
	fprintf(stderr, "FATAL: open: %s\nCould not open serial port.\n",
		strerror(errno));
	return -1;
    }

    // Fetch current settings
    tcgetattr(serialFD, &currTTYserial);
    tcgetattr(serialFD, &newTTYserial);

    // Save old settings
    oldTTYserial = &currTTYserial;

    printf("serial settings: IXON %d\nIXOFF %d\nHUPCL %d\nCLOCAL %d\nCRTSCTS %d\n",
	    oldTTYserial->c_iflag & IXON,
	    oldTTYserial->c_iflag & IXOFF,
	    oldTTYserial->c_cflag & HUPCL,
	    oldTTYserial->c_cflag & CLOCAL,
	    oldTTYserial->c_cflag & CRTSCTS );

    // Make new settings
    newTTYserial.c_iflag &= ~IXON;	    // disable XON/XOFF flow control
    newTTYserial.c_iflag &= ~IXOFF;
    newTTYserial.c_cflag &= ~PARENB;	    // disable parity bit
    newTTYserial.c_cflag &= ~CSTOPB;	    // use only one stop bit
    newTTYserial.c_cflag &= ~HUPCL;	    // don't hang up the modem lines on close
    newTTYserial.c_cflag |=  CLOCAL;	    // ignore all modem control lines
    newTTYserial.c_cflag &= ~CRTSCTS;	    // disable modem flow control
    newTTYserial.c_lflag &= ~ICANON;	    // disable line buffering
    newTTYserial.c_lflag &= ~ECHO;	    // disable local echo

    newTTYserial.c_cflag  = (newTTYserial.c_cflag & ~CSIZE) | CS8;	// set data size to 8 bits
    if (    cfsetispeed(&newTTYserial, baud) == -1 || 
	    cfsetospeed(&newTTYserial, baud) == -1)
    {
	fprintf(stderr, "FATAL: cfsetspeed: %s\nCould not set serial baud.\n",
		strerror(errno));
	return -1;
    }

    // Try to envoke the new settings
    if (tcsetattr(serialFD, TCSANOW, &newTTYserial) == -1)
    {
	fprintf(stderr, "FATAL: tcsetattr: %s\nCould not change serial terminal settings.\n",
		strerror(errno));
	return -1;
    }

    return 0;
}

int initializeNetwork(short port) {
    struct sockaddr_in sa;

    // Create command socket
    if ((net_fd = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
	fprintf(stderr, "FATAL: socket: %s\nCould not create command socket.\n",
		strerror(errno));
	return -1;
    }

    // We will receive commands on the local port "port"
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    sa.sin_addr.s_addr = htonl(INADDR_ANY);

    // Bind the socket so we can receive commands
    if (bind(net_fd, (struct sockaddr *)&sa, sizeof(sa)) == -1)
    {
	fprintf(stderr, "FATAL: bind: %s\nCould not bind command socket to local port %d.\n",
		strerror(errno), port);
	return -1;
    }
    
    printf("Bound net socket to port%d\n",port);

    return 0;
}


void sendCommand(uint8_t cmd, uint8_t arg)
{

  static FanCommand fc = { 0, 1, 0 };	    // read as: uninit, 1, uninit
  
  fc.cmd = cmd;
  fc.arg = arg;

  printf("About to send the fan command\n");  
  if (write(serialFD, &fc, sizeof(fc)) < sizeof(fc)) {
    fprintf(stderr, "write: %s\nCould not send command to motors.\n",
	    strerror(errno));
  }
  printf("I sent the bloody command!\n", cmd, arg);
}


int getGyroMeasurement() {

  long int counter1 = 0;
  time_t start_time;
  struct timeb tp;
  FILE* log;

  log = fopen("gyro.log", "w");
  
  while(counter1 <= 2000)
    {
      //printf("Start updating...\n");
      bathw_update_sensors();
      printf("Gyro values = %f \n", gyroValue);
      // log the gyro value and a number of seconds since the variable start_time
      // was set, so that we know how fast this is running
      ftime(&tp);
      fprintf(log, "%lf, %lf\n", (tp.time - start_time) + (tp.millitm / 1000), gyroValue);
      counter1++;
    }
}

int handleNetwork()
{

    int num_bytes;
    ControlPacket cp;

    printf("Reading network stuff\n");

    if ((num_bytes = recv(net_fd, &cp, sizeof(cp), MSG_WAITALL)) == -1)
    {
	fprintf(stderr, "recv: %s\nCould not receive command packet.\n",
		strerror(errno));
	return 0;
    }

    // Make sure we got that byte
    if (num_bytes < sizeof(cp))
    {
	fprintf(stderr, "Incomplete command packet dropped. Size = %d\n", num_bytes);
	return 0;
    }

    printf("Recvd control: left: %f right: %f lift: %f\n", cp.leftFan, cp.rightFan, cp.liftFan);

    fflush(stdout);
    return 0;
}

// This is code to throw away the sensor data that we're getting on the serial
// port. I shouldn't really need to do this, though.
int discardSensors()
{
    char stat;
    int num_bytes;
    uint8_t header;
    uint8_t size = 0;
    uint8_t trash[256];
    struct serial_struct ss;
    
    printf("Discarding sensor data\n");
    if ((num_bytes = read(serialFD, &header, sizeof(header))) == -1)
    {
	fprintf(stderr, "FATAL: read: %s\nCould not read sensor data.\n",
		strerror(errno));

	// Fetch tty info
	ioctl(serialFD, TIOCGSERIAL, &ss);
	printf("Serial info: type %x  - line %x - flags %x\n",
		ss.type, ss.line, ss.flags);

	return 0;
    }
    else if (num_bytes < sizeof(header))
    {
	fprintf(stderr, "Only %d bytes read for header\n", num_bytes);
	return 0;
    }
    printf("Read sensor data\n");
    
    switch (header)
    {
	case 'G':
	    size = 2;
	    break;

	case 'H':
	    size = 2;
	    break;

	case 'A':
	    size = 3;
	    break;

	default:
	    printf("got header: '%c' (%d)\n", header, header);
    }

    if (size)
    {
	if ((num_bytes = read(serialFD, trash, (size_t)size)) == -1)
	{
	    fprintf(stderr, "FATAL: read: %s\nCould not read sensor data.\n",
		    strerror(errno));
	    return 0;
	}
	else if (num_bytes < size)
	{
	    fprintf(stderr, "Only %d bytes read for body (should be %d)\n", num_bytes, size);
	    return 0;
	}
    }
    
    switch (header)
    {
	case 'G':
	    printf("Got gyro data: %d\n", trash[0] + 256 * trash[1]);
	    break;

	case 'H':
	    printf("Got heading data: %d %d\n", trash[0], trash[1]);
	    break;

	case 'A':
	    printf("Got accel data: %d/%d %d/%d\n", trash[0], trash[2], trash[1], trash[2]);
	    break;

	default:
	    break;
    }

    printf("Done discarding sensor data\n");
    return 0;
}

