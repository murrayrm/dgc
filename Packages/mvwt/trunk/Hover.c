#include "MvwtFollower.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/times.h>

#include <time.h>
#include "traj.h"

#define USHRT_MAX  100


static void printUsage() {

  printf("HoverHw [-h] [-c COMMAND PORT] [-v VISION PORT ] [-n VEHICLE NUMBER] [-g GAINS FILE] [-t TRAJ FILE]\n"); 
  printf("-c Select the port on which commands are to be received\n"); 
  printf("-v Select the port on which vision data is to be received\n"); 
  printf("-n Select the vehicle number, see the vision computer\n"); 
  printf("-g Select the gains file for the controller \n"); 
  printf("-t Select the trajectory file to follow\n"); 
  printf("-s Select the file to print state data to\n"); 
  printf("-e Select the file to print traj error data to\n"); 
  printf("-f Select the file to print forces to\n"); 
  printf("-h Display usage message \n");
  exit(0);
}


static void printOptions(){

  printf("HoverHw ");
  // printf(sim?"SIMULATION":"HARDWARE \n");
  printf("COMMAND PORT: %d \n",m_commandSocket);  
  printf("VISION PORT: %d \n",m_visionPort); 
  printf("VEHICLE NUMBER: %d \n",m_vehicleId ); 
  printf("end of print options \n" ); 
  //FIX how to print char*
  //printf("GAINS FILE: %c \n",*gainsFile ); 
  //printf("TRAJECTORY  FILE : %c \n",*trajFile); 
}


int main(int argc, char **argv) {

  // Set the default ports to be overwritten if command-line options are used
  int c, numRows;
  char* sarg;
  int iarg, targ;
  char *endptr, *trajFile, *gainsFile, *stateFile, *errFile, *forceFile;
  //char c;
  int sim = 0; //false

  char *mvwtFile, *str1, *str2, *str3; 

  char* m_logFile;
  char* m_logECSFile;

  time_t timeNow; 

  printf("In main \n");
  m_visionPort = DEFAULT_VISION_PORT;
  m_commandSocket = DEFAULT_COMMAND_PORT;
  m_vehicleId = 6; //Based on current vision hat in operation

  printf("About to get cmd line args \n");

  // Get the command-line arguments
  while ((c = getopt(argc, argv, "l:s:e:c:v:n:g:t:h:z")) != -1) {

    switch (c) {
     case 'c':
       iarg = strtol(optarg, &endptr, 0);
      
       if (*endptr || targ < 0 || targ > USHRT_MAX)
 	printf("Invalid port %s ignored\n", c);
       else
	 m_commandSocket = targ;
       break;
      
     case 'l':
       m_logFile = optarg;
       if ((m_logFP = fopen(m_logFile, "w")) == NULL)
	 return -1;
       break;
     case 'k':
       //m_logECSFile = optarg;
       //if ((m_logFP = fopen(m_ECSFile, "w")) == NULL)
       // return -1;
       break;

     case 'v':
       iarg = strtol(optarg, &endptr, 0);
      
       if (*endptr || targ < 0 || targ > USHRT_MAX)
 	printf("Invalid port %s ignored\n", c);
       else
 	m_visionPort = targ;
       break;

     case 'n':
       targ = strtol(optarg, &endptr, 0);
      
       if (*endptr || iarg < 0 || targ > USHRT_MAX)
 	printf("Invalid vehicle number %s ignored\n", c);
       else
 	m_vehicleId = targ;
       break;

     case 'g':
       gainsFile = optarg;
       break;

     case 't':
       trajFile = optarg;
       break;
      
     case 'h':
       printUsage();
       return -1;
       break;

    case 'z':
      printf("Host Name\n %s", argv);
       m_host = argv;
       break;
      
     case '?':
       printf("Unrecognized option '%c' ignored.\n", c);
       break;
      
     default:
       break;
     }
   }

  m_host="192.168.1.210";

  str1 = "MvwtFollower_";
  str2 = ctime(&timeNow);
  str3 = ".log";

  printf("Logging to MvwtFollower_ %s%s", str2,str3);
 
  printOptions();

  //Decrement vehicle id for vision system 
  m_vehicleId --;

  printf("Traj file %s, \n", trajFile);

  m_gains.y1 = 5;
  // m_gains.y1 = 1.5;
  m_gains.t1 = .1;
  m_gains.ydot1 = 2;
  m_gains.tdot1 = 0;
  
  //m_gains.y2 = -1.5;
  m_gains.y2 = -5;
  m_gains.t2 = -.1;
  m_gains.ydot2 = -2;
  m_gains.tdot2 = 0;

  m_integralGains.y1 = .05;
  m_integralGains.t1 = 0;
  m_integralGains.ydot1 = 0.1;
  m_integralGains.tdot1 = 0;

  m_integralGains.y2 = -.05;
  m_integralGains.t2 = -0;
  m_integralGains.ydot2 = -0.1;
  m_integralGains.tdot2 = -0;
  
  if (NULL == trajFile)
    return 1; 

  m_traj = traj_load(trajFile);

  m_ctrlType = LAT;

  printf("TRAJ numRows= %d \n",numRows);

  initialize();

  estimateInitialState();

  while(1) {
    run();
  }

  printf("Goodbye Hovercraft!\n");
    
  return 0; 
}
