///
/// \file MVParameters.h
/// \brief Trajectory following program using closed loop controllers.
/// 
/// \author Vanessa Carson 
/// \date 17 Feb 2008
/// 
/// Gains packet specification
/// 

#ifndef __MVPARAMETERS_H__
#define __MVPARAMETERS_H__



#define NUMINP 8
#define NUMOUT 2
#define NUMMEAS 3

// Length of state or error vectors (t, x, y, theta, xdot, ydot, thetadot, u1ff, u2ff)
#define STDLENGTH 9

#define THETA_NOISE			0.087266463	// radians = 5 degrees
#define VELOCITY_NOISE			0.05		// m/s
#define VELOCITY_ZERO			0.0001		// m/s
#define LIFT_ON                         200
#define DEFAULT_VEHICLE_PORT	        2020

extern double FMAX;
extern double HMASS;
extern double ROTFRIC;
extern double LINFRIC;
extern double ROTINERT;

double FMAX =0.7;             // the maximum of the force allowed by fans
double HMASS =0.75;       // the mass of the hovercraft in kg
double ROTFRIC =0.005;        // the rotational friction coefficient
double LINFRIC =0.15;    // the linear friction coefficient
double ROTINERT = .0036;    // the rotational inertia 
double PI = 3.1416;    // the rotational inertia 


enum TRAJ  {T, X, Y, THETA, XDOT, YDOT, THETADOT, U1FF, U2FF};

#endif
