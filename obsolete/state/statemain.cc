#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>


#include "statemain.hh"

using namespace std;

// setting this flag disables GPS
const int IMUonly = 1;

// declare a DATUM
DATUM d;


// used for starting threads
boost::recursive_mutex StarterMutex;
int StarterCount;
void Starter();


// the "kill switch"
void shutdown(int signal) {
  d.shutdown_flag = TRUE;
  cout << "Set Abort Flag" << endl;
  exit(EXIT_SUCCESS);
}


int main() {
  cout << "Starting Main" << endl;

  // setup signals for shutdown
  d.shutdown_flag = FALSE;
  signal(SIGINT, &shutdown);
  signal(SIGTERM, &shutdown);

  // initialize all devices before spawning threads
  cout << "Initializing GPS... ";
  if (!IMUonly)  
    InitGPS(d);
  cout << "done." << endl;

  //IMU
    cout<<"Initializing IMU"<<endl;
    InitIMU(d);
    cout<<"done"<<endl;

//  cout << "Initializing OBD II... ";
//  InitOBD(d);
//  cout << "done." << endl;

  // spawn threads
  boost::thread_group thrds;

  StarterCount = 0;
  thrds.create_thread(Starter); 
  thrds.create_thread(Starter); 

  thrds.join_all();
  cout << "Shutting Down" << endl;
  

  return 0;
}


void Starter() {
  // determine which thread we are 
  int myThread;
  if( TRUE ) {
    boost::recursive_mutex::scoped_lock sl(StarterMutex);
    myThread = StarterCount;
    StarterCount ++;
  }

  switch( myThread ) {
  case 0:
    cout << "Starting GrabGPS Loop" << endl;
    if (!IMUonly){
      GrabGPSLoop(d);
    }else {
      GPSsimstub(d);
    }
    break;
  case 1:
    cout << "Starting GrabIMU Loop" << endl;
    GrabIMULoop(d);
    break;
  case 2:
  //  cout << "Starting Grab OBD loop" <<endl;
  //  GrabOBDLoop(d);
    break;
  default:
    cout << "Starter error, default reached" << endl;
  }
}
