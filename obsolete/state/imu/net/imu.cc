#include <sys/types.h>
#include <sys/socket.h>
#include <iostream.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>		/* for sockaddr_in */
#include <netdb.h>		/* for gethostbyname() */
#include <unistd.h>		/* for read(), write() */
#include <assert.h>
#include <pthread.h>
#include <sys/time.h>		/* unix: for gettimeofday() */
#include "crc32.h"
#include "imu_net.h"


#ifndef WIN32
#define SOCKET int
#define closesocket close
#endif

FILE *logfile = NULL;		// log file


static int
setup_addr (struct sockaddr *addr,
	     unsigned port)
{
  struct sockaddr_in addr_in;
  memset (&addr_in, 0, sizeof (addr_in));
  addr_in.sin_family = AF_INET;
  addr_in.sin_port = htons (port);
  memcpy (addr, &addr_in, sizeof (struct sockaddr_in));
  return 1;
}

int main (int argc, char **argv)
{
  struct sockaddr addr;
  SOCKET sock;
  
  float pitch = 0;
  float roll = 0;
  float heading = 0;

  int count = 0;

  if (argc != 2)
    {
      fprintf (stderr, "usage: %s PORT\n"
	       "\nFake the arbiter.\n",
	       argv[0]);
      return 1;
    }
  if (!setup_addr (&addr, atoi (argv[1])))
    {
      return 1;
    }

  /* Send data to standard output (add cmd line arg later) */
  logfile = stdout;

  sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (bind (sock, &addr, sizeof (addr)) < 0)
    fprintf(stderr, "error binding to port %u\n", atoi (argv[1]));

  while (1)
    {
      char buf[256];
      int len = recv(sock, buf, sizeof(buf), 0);
      if (len == 4 + sizeof (IMUReading))
	{
	  unsigned char crc32[4];
	  IMUReading *reading = (IMUReading*)(buf + 4);
	  crc32_big_endian (buf + 4, len - 4, crc32);
	  if (memcmp (crc32, buf, 4) != 0)
	    fprintf (stderr, "CRC error (corrupted packet)\n");
	  else
	    {
	      static long start_time = 0;
	      struct timeval tv; struct timezone tz;

	      /* Get the time */
	      gettimeofday(&tv, &tz);
	      if (start_time == 0) start_time = tv.tv_sec;

	      if (logfile != NULL) 
		fprintf(logfile, "%ld %ld %g %g %g %g %g %g %ld %ld\n",
			tv.tv_sec - start_time, tv.tv_usec,
			reading->dvx, reading->dvy, reading->dvz, 
			reading->dtx, reading->dty, reading->dtz,
			reading->tv_sec, reading->tv_usec);
#ifdef UNUSED
				       
	      //	     fprintf (stderr, "got packet '%f %f %f %f %f %f'\n", reading->dvx, reading->dvy, reading->dvz, reading->dtx, reading->dty, reading->dtz);
	fprintf(stderr, "\r%7.5f  %8.5f  %8.5f     ", reading->dvx, reading->dvy, reading->dvz);
	     pitch += reading->dty;
	     roll += reading->dtx;
	     heading += reading->dtz;
	     if (count == 400)
	       {
		 count = 0;
		 cout<<"\nPitch: "<<pitch* 180/3.14<<" roll: "<<roll*180/3.14<<" heading: "<<heading*180/3.14<<endl;
	       }else{
		 count++;
	       }
#endif
	    }
	}
    }
}
