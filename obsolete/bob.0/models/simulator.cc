/***************************************************************************************/
//FILE: 	simulator.cc
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file contains the implementation of the methods from the simulator 
//              class
//
/***************************************************************************************/

#include "simulator.hh"
#include "VehicleConstants.h" // get VEHICLE_AXLE_DISTANCE from here

//***********PRIVATE FUNCTIONS***********//

// This function gives back the next state after applying the runge-kutta 4 method
//	order	: the number of differential equations
//	x	: derive the state over x
//	y	: state space
//	ss	: function that evfaluates x' = f(x,t)
//	input	: input of the system like a force or a steering position,
//		  it's a vector to  allow more than one input
void Simulator::diffSolver(int order, double x, double *y, double step, int switchFnc, double *input){

  
  int i;
  double h=step/2.0;
  double k1[MAX_ORDER], k2[MAX_ORDER], k3[MAX_ORDER], k4[MAX_ORDER];
  double Yaux[MAX_ORDER];

  // HACK TO SWITCH BETWEEN FUNCTIONS
  // I COULDN'T FIND A WAY TO ASSIGN A POINTER OF A METHOD TO A VARIABLE
  // PAY ATTENTION ON THE SWITCHS AND THE SWITCHFNC PASSED

  // First step
  switch(switchFnc){
  case LAG_KEY:    ss_lag(x,y,k1,input);    break;
  case LONG_KEY: ss_long(x,y,k1,input); break;
  case PLANAR_KEY_REAR: ss_planar_rear(x,y,k1,input); break;
  case PLANAR_KEY_FRONT: ss_planar_front(x,y,k1,input); break;
  } 

  for(i = 0;i < order; i++){
    k1[i]  = step*k1[i];
  }

  // Second step
  for(i = 0;i < order; i++){
    Yaux[i] = y[i] + k1[i]/2;
  }
  switch(switchFnc){
  case LAG_KEY:    ss_lag(x+h,Yaux,k2,input);    break;
  case LONG_KEY: ss_long(x+h,Yaux,k2,input); break;
  case PLANAR_KEY_REAR: ss_planar_rear(x+h,Yaux,k2,input); break;
  case PLANAR_KEY_FRONT: ss_planar_front(x+h,Yaux,k2,input); break;
  } 

  for(i = 0;i < order; i++){
    k2[i]  = step*k2[i];
  }

  // Third step
  for(i = 0;i < order; i++){
    Yaux[i] = y[i] + k2[i]/2;
  }
  switch(switchFnc){
  case LAG_KEY:    ss_lag(x+h,Yaux,k3,input);    break;
  case LONG_KEY: ss_long(x+h,Yaux,k3,input); break;
  case PLANAR_KEY_REAR: ss_planar_rear(x+h,Yaux,k3,input); break;
  case PLANAR_KEY_FRONT: ss_planar_front(x+h,Yaux,k3,input); break;
  } 

  for(i = 0;i < order; i++){
    k3[i]  = step*k3[i];
  }

  // Fourth step
  for(i = 0;i < order; i++){
    Yaux[i] = y[i] + k3[i];
  }
  switch(switchFnc){
  case LAG_KEY:    ss_lag(x+step,Yaux,k4,input);    break;
  case LONG_KEY: ss_long(x+step,Yaux,k4,input); break;
  case PLANAR_KEY_REAR: ss_planar_rear(x+step,Yaux,k4,input); break;
  case PLANAR_KEY_FRONT: ss_planar_front(x+step,Yaux,k4,input); break;
  }

  for(i = 0;i < order; i++){
    k4[i]  = step*k4[i];
  }

  // y for the next step of time
  for(i = 0;i < order; i++){
    y[i] = y[i]+(k1[i]+2*k2[i]+2*k3[i]+k4[i])/6.0;
    //cout << "y[" << i << "]=" << y[i] << " ";
  }
}

// dz/dt = f(z,u)
// z = [v]
// This function evaluates the state space of the longitudinal dynamics of 
// the vehicle (simple model).
//	dv/dt = (1/m)*(-b*v + F)
//	v	: velocity
//	b	: drag coeficient
//	F	: Force of brakes or throttle
//	input	: the input is one dymensional
void Simulator::ss_long(double x, double *y, double *k, double*force){
  k[0] = (double)((double)1/VEHICLE_MASS)*(-DRAG*y[0] + force[0]);
}

// dz/dt = f(z,u)
// z = [x, y, theta]
// This function evaluates the state space of the planar position
// where the origin is at the center of the rear axle
//	dx/dt = v*cos(theta)
//	dy/dt = v*sin(theta)
//	dtheta/dt = (v/L)*tan(phi)
//      v       : speed of center of rear axle
//	x	: x position
//	y	: y position
//	theta	: heading
//	phi	: steering angle
//	L	: distance between the front and rear axles
//      input   : {steering angle, vehicle speed}
void Simulator::ss_planar_rear(double x, double *y, double *k, double*input){
  k[0] = input[1]*sin(y[2]);
  k[1] = input[1]*cos(y[2]);
  k[2] = (double)((double)input[1]/VEHICLE_AXLE_DISTANCE)*tan(input[0]);
}

// dz/dt = f(z,u)
// z = [x, y, theta]
// This function evaluates the state space of the planar position
// where the origin is at the center of the front axle
//	dx/dt = v*cos(theta + phi)
//	dy/dt = v*sin(theta + phi)
//	dtheta/dt = (v/L)*sin(phi)
//      v       : speed of center of front axle
//	x	: x position
//	y	: y position
//	theta	: heading
//	phi	: steering angle
//	L	: distance between the front and rear axles
void Simulator::ss_planar_front(double x, double *y, double *k, double*input){
  k[0] = input[1]*sin(y[2] + input[0]);
  k[1] = input[1]*cos(y[2] + input[0]);
  k[2] = (double)((double)input[1]/VEHICLE_AXLE_DISTANCE)*sin(input[0]);
}

// dz/dt = f(z,u)
// z = [tau]
// This function evaluates the state space of a first order lag
//	dTau/dt = (lag)*(Tau + u)
//	Tau	: engine torque
//	lag	: delay parameter
//	input	: the input is two dymensional, one to the lag parameter and the other to the reference
void Simulator::ss_lag(double x, double *y, double *k, double*input){
  k[0] = (input[0])*(-y[0] + input[1]);
}

// This functions convert from throttle [0 1] into total torque applied
// at vehicle axles.  THIS SHOULD BE CHECKED AGAINST ACTUAL DATA.
// y = k*u + b
double Simulator::map_acc(double u)
{
  double k1 = 1200;
  double b1 = 0;
  double k2 = 5350/4;
  double b2 = -321;
  double y,differential;

  differential = 10;
  if(u < 0){
    // It's a brake actuation
    y = 0;
  }
  else{  
    if(u < 0.2){
      y = k1*u + b1;
    }
    else
    {
      y = k2*u + b2;
    }
  }
  return(y*differential);
}

// This functions convert from brake [-1 0] into total torque applied at
// vehicle axles.  THIS MODEL SHOULD BE CHECKED AGAINST ACTUAL DATA.
// y = k*u + b
double Simulator::map_brake(double u, double vel){
  // Brake parameters 
  double k1 = 0.096;
  double b1 = 0;
  double k2 = 1.41;
  double b2 = -0.657;
  double fmax = 3000;

  double y;
  double in;
  
  in = -u;
  
  if(in < 0){
    // If its a throttle actuation
    y = 0;
  }
  else{
    if(in < 0.5){
      y = k1*in + b1;
    }
    else
    {
      y = k2*in + b2;
    }
  }
  
  // The direction depends on the velocity
  if(vel > 0){
    y = -y*fmax;
  }
  if(vel < 0){
    y = y*fmax;
  }
  if(vel == 0){
    y = 0;
  }
  return(y);  
}

// This functions converts the torque at the vehicle axles into force
// applied to the tires by the ground (and vice versa)
double Simulator::map_tire(double accTorque,double brakeTorque){
  double force;
  
  force = (accTorque + brakeTorque)*VEHICLE_TIRE_RADIUS;
  return(force);
}

//***********PUBLIC FUNCTIONS***********//

// Default Constructor
Simulator::Simulator(){
  simData newSimData;
  setState(newSimData);
  step = 0;
  time = 0;
  steer = 0;
  accel = 0;
}

// Default destructor
Simulator::~Simulator(){

}

// Constructor
Simulator::Simulator(simData oldData,double newStep){
  setState(oldData);
  step = newStep;
  time = 0;
  steer = 0;
  accel = 0;
  engineState[0] = 0;
  steeringState[0] = 0;
  longState[0] = 0;
  planarState[0] = 0;
  planarState[1] = 0;
  planarState[2] = 0;
}

// Initializing the state
void Simulator::initState(simData firstData){
  setState(firstData);
}

// Updating time, steer and accel
void Simulator::setSim(double pTime, double pAccel, double pSteer)
{
  time = pTime;
  steer = pSteer;
  accel = pAccel;
}

// Updating with the old state, yaw and vel is not necessary (evaluated inside from e_vel and n_vel!!!!)
void Simulator::setState(simData newState){
	memcpy(&state, &newState, sizeof(state));
}


// Numerical integration of model (2004/2005 version)
void Simulator::simulate()
{
#ifdef NODIMA
  simData newState;
  double n, e, yaw, vel, phi;

  double longInput[1], planarInput[2], engineInput[2], steeringInput[2];
  double throttleOutput, brakeOutput;

  // Getting actual state
  planarState[0] = state.n;
  planarState[1] = state.e;
  planarState[2] = state.yaw;

  // Getting the acceleration torque
  // This is the conversion of the commanded acceleration (set by setSim()) to
  // torque applied at vehicle axles due to the throttle.
  throttleOutput = map_acc(accel);
      
  // Getting the brake torque (total torque applied at vehicle axles due to 
  // braking)
  brakeOutput = map_brake(accel,state.vel);
      
  // Passing thru the engine lag 
  engineInput[0] = ENGINE_LAG;      // lag 
  engineInput[1] = throttleOutput;  // input of the system
  diffSolver(LAG_ORDER, time, engineState, step,LAG_KEY, engineInput);

  // Converting the torques into forces
  longInput[0] = map_tire(engineState[0],brakeOutput);
  
  // Computing next velocity (first order lag model for longitudinal dynamics)
  diffSolver(LONG_ORDER, time, longState, step, LONG_KEY, longInput);
  vel = longState[0];
  if(vel < 0) vel = 0;

  // Passing thru the steering wheel lag
  steeringInput[0] = STEERING_LAG;
  steeringInput[1] = SIM_MAX_STEER*steer;
  diffSolver(LAG_ORDER, time, steeringState, step,LAG_KEY, steeringInput);
  phi = steeringState[0];
  
  // Computing next state of the planar movement
  planarInput[0] = phi;
  planarInput[1] = vel;
  diffSolver(PLANAR_ORDER, time, planarState,step, PLANAR_KEY_REAR, planarInput);
  n   = planarState[0];
  e   = planarState[1];
  yaw = planarState[2];

  // Updating state
  newState.n     = n;
  newState.e     = e;
  newState.vel   = vel;

  // Update the acceleration based on the newly calculated speed.
#warning "Only longitudinal acceleration is set here." 
  double newAccel = (newState.vel - state.vel) / step;
  newState.n_acc = newAccel*cos(yaw);
  newState.e_acc = newAccel*sin(yaw);

  newState.n_vel = vel*cos(yaw);
  newState.e_vel = vel*sin(yaw);
  newState.u_vel = 0;
  newState.yaw   = yaw;
  newState.phi   = phi;

  setState(newState);
#else
  simulate_planar_only();
#endif
}


// Numerical integration of model (Dima's version)
void Simulator::simulate_planar_only()
{
	double phidot;

	// saturate the phi
	steer = fmax(-VEHICLE_MAX_AVG_STEER, fmin(VEHICLE_MAX_AVG_STEER, steer));

	// make sure that the acceleration is with physically possible bounds
	// (these bounds set semi-arbitrarily here)
	accel = fmax(-6.0, fmin(2.0, accel));

	// the following makes sure that the steering angle doesn't change faster that
	// the wheel can be turned (assumed to be 3.0 seconds rail to rail)
	phidot = (steer - state.phi) / step;
	if(phidot > 2.0*VEHICLE_MAX_AVG_STEER/3.0)
		phidot = 2.0*VEHICLE_MAX_AVG_STEER/3.0;
	else if(phidot < -2.0*VEHICLE_MAX_AVG_STEER/3.0)
		phidot = -2.0*VEHICLE_MAX_AVG_STEER/3.0;

	state.n			+= step * state.n_vel;
	state.e			+= step * state.e_vel;
	state.vel		+= step * accel;
	state.vel = fmax(0.0, state.vel);

	double n_vel_new, e_vel_new;
	n_vel_new = state.vel * cos(state.yaw);
	e_vel_new = state.vel * sin(state.yaw);
	state.n_acc = (n_vel_new - state.n_vel) / step;
	state.e_acc = (e_vel_new - state.e_vel) / step;
	state.n_vel = n_vel_new;
	state.e_vel = e_vel_new;
	state.yaw		+= step * state.vel / VEHICLE_WHEELBASE * tan(state.phi);
  state.phi   += phidot * step;
}


// Set the time step
void Simulator::setStep(double pStep){
  step = pStep;
}

// Returns the state
simData Simulator::getState(){
  return(state);
}
