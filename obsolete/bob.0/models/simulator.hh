/***************************************************************************************/
//FILE: 	simulator.h
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This contains file the simulator class and defines constants to 
//              simulator.cpp
//
/***************************************************************************************/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream.h>
#include "Constants.h"

#define TRUE 1
#define FALSE 0
#define DRAG 63
#define ENGINE_LAG 0.5
#define STEERING_LAG 0.5

#define MAX_ORDER 5 //Maximum of differential equations, just to simplify the programm
#define LONG_ORDER 1
#define PLANAR_ORDER 3
#define LAG_ORDER 1

#define LONG_KEY 1
#define LAG_KEY    2
#define PLANAR_KEY_REAR 3
#define PLANAR_KEY_FRONT 4

typedef struct simData{
  double n;     // distance to north direction
  double e;     // distance to east direction
  double vel;   // velocity ligned to the heading
  double n_vel; // north velocity
  double e_vel; // east velocity
  double u_vel; // upwards velocity
  double n_acc; // north acceleration
  double e_acc; // east acceleration
  double yaw;   // angle of the heading(yaw), north is 0 and east is pi/2
  double phi;   // steering angle [-MAX_STEER MAX_STEER], positive is to the right


  simData(){
    n = SIMULATOR_INITIAL_NORTHING;
    e = SIMULATOR_INITIAL_EASTING;
    vel = 0;
    n_vel = 0;
    e_vel = 0;
		n_acc = 0.0;
		e_acc = 0.0;
    u_vel = 0;
    yaw = SIMULATOR_INITIAL_YAW;
    phi = 0;
  }

  simData(const simData &copy){
    n = copy.n;
    e = copy.e;
    vel = copy.vel;
    n_vel = copy.n_vel;
    e_vel = copy.e_vel;
    n_acc = copy.n_acc;
    e_acc = copy.e_acc;
    u_vel = copy.u_vel;
    yaw = copy.yaw;
    phi = copy.phi;
  }
};

class Simulator {

 private:

  // ATTRIBUTES OF THE CLASS
  simData state;
  double step;    // Time stamp
  double time;    // current time in seconds
  double steer;   // stearing wheel command (rad)
  double accel;   // throtle command (m/s/s)
  double engineState[1];
  double longState[1]; // state for longitudinal dynamics
  double planarState[3];
  double steeringState[1];

  // METHODS TO BE USED ONLY INSIDE THE CLASS

  // Differential equation solver
  void diffSolver(int, double, double*, double, int, double*);

  // Evaluate the state space of the cruise control
  void ss_long(double, double*, double*, double*);

  // Evaluate the state space of the planar dynamics
  // using the origin as the center of the rear axle
  void ss_planar_rear(double, double*, double*, double*);

  // Evaluate the state space of the planar dynamics
  // using the origin as the center of the front axle
  void ss_planar_front(double, double*, double*, double*);

  // Evaluate the state space of a simple lag
  void ss_lag(double, double*, double*, double*);

  // Evaluate the static map between throttle actuator and torque
  double map_acc(double);

  // Evaluate the static map between brake actuator and torque
  double map_brake(double,double);

  // Evaluate the static map between acc and brake torques and force applied on the car
  double map_tire(double,double);

 public:

  // Constructor and destructor
  Simulator();
  ~Simulator();

  // Other Constructor
  Simulator(simData,double);

  // Updating time, accel and steer actuator
  void setSim(double,double,double);

  // Initializing the state
  void initState(simData);

  // Updating the time step
  void setStep(double);

  /*! This new version of the simulator simulates the longitudinal and lateral 
   ** dynamics of the vehicle and takes acceleration and steering angle as 
   ** inputs. */
  void simulate();

  /*! This is the gutted (Newton integration) simulator of only the planar 
   ** kinematics of the vehicle.  Longitudinal dynamics are not simulated here.  
   ** */
  void simulate_planar_only();

  // Updating with the old state, yaw and vel is not necessary (evaluated inside from e_vel and n_vel!!!!)
  void setState(simData);

  // Returns the state
  simData getState();
};
