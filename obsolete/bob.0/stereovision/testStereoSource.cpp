#include <stdio.h>
#include <stdlib.h>

#include "stereoSource.hh"


int main(int argc, char *argv[]) {
  stereoSource mySource;
  mySource.init(0, "../calibration/CamID.ini.short_color", "temp", "bmp");
  //mySource.init(0, 640, 480, "temp", "bmp");

  //Fl_Image *image = new Fl_Image(640, 480, 0);

  //printf("%s [%d]: \n", __FILE__, __LINE__);

  for(int i=0; i<20; i++) {
    printf("Grab %d\n", i);
    //printf("%s [%d]: \n", __FILE__, __LINE__);
    mySource.grab();
    //printf("%s [%d]: \n", __FILE__, __LINE__);
    mySource.save();
    //printf("%s [%d]: \n", __FILE__, __LINE__);
    //fl_draw_image_mono(mySource.left(), 0, 0, 640, 480);
    //image->data((char * const*)mySource.left(), 1);
  }

  mySource.stop();
  return 0;
}

