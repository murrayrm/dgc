#include <stdio.h>
#include <stdlib.h>

#include "stereoSource.hh"
#include "stereoProcess.hh"


int main(int argc, char *argv[]) {
  frames myFrame;
  XYZcoord offset(0, 0, -2);
  XYZcoord start_point(1.4142136, 0, 0);
  XYZcoord end_point(0, 0, 0);
  double pitch=-0.78539816, roll=0, yaw=0;
  VState_GetStateMsg state;

  myFrame.initFrames(offset, pitch, roll, yaw);

  state.Easting = state.Northing = state.Altitude = state.Pitch = state.Roll = state.Yaw = 0;

  myFrame.updateState(state);

  end_point = myFrame.transformS2B(start_point);

  printf("Offset: (%lf, %lf, %lf)\n", offset.x, offset.y, offset.z);
  printf("PRY: (%lf, %lf, %lf)\n", pitch, roll, yaw);
  printf("State: %lf, %lf, %lf, %lf %lf %lf\n", state.Easting, state.Northing, state.Altitude, state.Pitch, state.Roll, state.Yaw);
  printf("Start: (%lf, %lf, %lf)\n", start_point.x, start_point.y, start_point.z);
  printf("End: (%lf, %lf, %lf)\n", end_point.x, end_point.y, end_point.z);


  /*
  stereoSource mySource;
  stereoProcess myProcess;
  VState_GetStateMsg currentState;

  mySource.init(0, "CamID.ini.short", "temp", "bmp");
  myProcess.init(0, "../calibration/SVSCal.ini.short",
		 "../calibration/SVSParams.ini.short",
		 "../calibration/CamCal.ini.short",
		 "temp",
		 "bmp");

  for(int i=0; i<50; i++) {
    mySource.grab();
    mySource.save();
    myProcess.loadPair(mySource.pair(), currentState);
    myProcess.calcRect();
    //myProcess.saveRect();
    myProcess.calcDisp();
    myProcess.saveDisp();

  
  }

  mySource.stop();
  */
  return 0;
}

