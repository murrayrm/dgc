#ifndef __SVSSTEREOWRAPPER_HH__
#define __SVSSTEREOWRAPPER_HH__

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <stereovision/getpair.h>
#include <videre/svsclass.h>
#include <frames/frames.hh>
#include <cv.h>
#include <cvaux.h>
#include <highgui.h>
#include <vehlib/VState.hh>

#define MAX_CAMERAS 2

enum {
  svsSW_OK, 
  svsSW_NO_FILE,
  svsSW_NO_CAMERAS,
  svsSW_NO_IMAGES, 
  svsSW_NO_RECT,
  svsSW_NO_DISP,
  svsSW_NO_3D,
  svsSW_UNKNOWN_ERROR,

  svsSW_COUNT
};


class svsStereoWrapper {
public:
  svsStereoWrapper();
  ~svsStereoWrapper();

  int init(int whichPair, char* SVSCalFile, char* SVSParamsFile, char* CamParamsFile, bool useCameras, bool useVerbose);

  int grabCameraPair(VState_GetStateMsg currentState);
  int grabFilePair(char* leftFilename, char* rightFilename, char* logFilename, int lineNum);

  int snapCameraPair();
  int loadCameraPair(VState_GetStateMsg currentState);

  int saveStereoPair(char* leftFilename, char* rightFilename);
  int saveDispImage(char* dispFilename);

  int processPair();
  int numPoints();
  bool validPoint(int i);
  XYZcoord UTMPoint(int i);
  XYZcoord Point(int i);
  bool SinglePoint(int x, int y, XYZcoord* resultPoint);
  bool SingleRelativePoint(int x, int y, XYZcoord* resultPoint);

  VState_GetStateMsg getCurrentState();

  int numMinPoints;
  VState_GetStateMsg SS;

  CvRect subWindow;

private:
  //Camera variables
  dc1394_cameracapture cameras[MAX_CAMERAS];
  raw1394handle_t handle;
  int node2cam[MAX_CAMERAS];

  //OpenCV variables
  IplImage* stereoImages[MAX_CAMERAS];
  IplImage* tempImages[MAX_CAMERAS];
  CvSize correctedSize;
  CvSize imageSize;

  //SVS variables
  svsStoredImages *videoObject;
  svsStereoProcess *processObject;
  svsStereoImage *imageObject;

  //Transformation variables
  frames cameraFrame;

  //General variables
  bool camerasReady;
  bool imagesLoaded;
  bool verboseMode;
};


#endif //__SVSSTEREOWRAPPER_HH__
