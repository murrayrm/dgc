/*
** get_genmaps.cc
**
**  
** 
**
*/

#include "MatlabDisplay.hh" // standard includes, arbiter, datum, ...

#include <iostream>
using namespace std;
/*
void MatlabDisplay::ReceiveLADARgenMap()
{ 
  // Do the fancy MTA stuff
  Mail msg = NewQueryMessage( MyAddress(), 
		              MODULES::LADARPlanner, // from MTA/Modules.hh
			      StereoMessages::Get_genMapStruct);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> _d.LADAR_genMapStruct; // download the new genMap
  } else {
    cout << "Failed to get LADAR genMap" << endl;
  }

}

void MatlabDisplay::ReceiveSVgenMap()
{ 
  // Do the fancy MTA stuff
  Mail msg = NewQueryMessage( MyAddress(), 
		              MODULES::StereoPlanner, // from MTA/Modules.hh
			      StereoMessages::Get_genMapStruct);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> _d.SV_genMapStruct; // download the new genMap
  } else {
    cout << "Failed to get SV genMap" << endl;
  }

}
*/
void MatlabDisplay::ReceiveCAEgenMap()
{ 
  // Do the fancy MTA stuff
  Mail msg = NewQueryMessage( MyAddress(), 
		              MODULES::CorridorArcEvaluator, 
			      CorridorArcEvaluatorMessages::Get_genMapStruct);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> _d.CAE_genMapStruct; // download the new genMap
  } else {
    cout << "Failed to get genMap" << endl;
  }

}

void MatlabDisplay::UpdateCAEgenMap()
{ 
  // request the genMap over MTA
  ReceiveCAEgenMap();
  
  // DEBUG messages to let us know whether the fancy MTA stuff worked
  printf("------------MatlabDisplay::UpdateCAEgenMap---------\n");
  printf("---------------Getting the genMapStruct------------\n");
  printf("-------------you can ignore this for now-----------\n");
  printf("  row_res = %f\n", _d.CAE_genMapStruct.row_res );
  printf("  col_res = %f\n", _d.CAE_genMapStruct.col_res );
  printf("  leftBottomUTM_Northing = %f\n", _d.CAE_genMapStruct.leftBottomUTM_Northing );
  printf("  leftBottomUTM_Easting  = %f\n", _d.CAE_genMapStruct.leftBottomUTM_Easting  );
  printf("  numRows  = %d\n", _d.CAE_genMapStruct.numRows  );
  printf("  numCols  = %d\n", _d.CAE_genMapStruct.numCols  );

  // Assign the Matlab workspace-related variables for our genMap... 
  for (int i = 0; i < _d.CAE_genMapStruct.numRows; i++) {
    for (int j = 0; j < _d.CAE_genMapStruct.numCols; j++) {
      // make sure that we fill the maps properly 
      // MATLAB references it's arrays from the top left
      // For simplicity, make it a one-dimensional array that contains 
      // the map data as read from top-down, left-to-right.
      // TODO [minor]: Figure out how to do this with 2D arrays
      _mapData_C[_d.CAE_genMapStruct.numCols*(_d.CAE_genMapStruct.numRows-1-i) + j]
	      = *(_d.CAE_genMapStruct.cell(i,j));
    }
  }
  *_mapNumRows_C       = (double)_d.CAE_genMapStruct.numRows;
  *_mapNumCols_C       = (double)_d.CAE_genMapStruct.numCols;
  *_mapRowRes_C        = (double)_d.CAE_genMapStruct.row_res;
  *_mapColRes_C        = (double)_d.CAE_genMapStruct.col_res;
  *_mapLeftBottomRow_C = (double)_d.CAE_genMapStruct.leftBottomRow;
  *_mapLeftBottomCol_C = (double)_d.CAE_genMapStruct.leftBottomCol;
  *_mapLB_Northing_C   = (double)_d.CAE_genMapStruct.leftBottomUTM_Northing;
  *_mapLB_Easting_C    = (double)_d.CAE_genMapStruct.leftBottomUTM_Easting;

  // Put the variables in the MATLAB workspace
  engPutVariable(_ep, "mapData",       _mapData_M );
  engPutVariable(_ep, "mapNumRows",    _mapNumRows_M );
  engPutVariable(_ep, "mapNumCols",    _mapNumCols_M );
  engPutVariable(_ep, "mapRowRes",     _mapRowRes_M );
  engPutVariable(_ep, "mapColRes",     _mapColRes_M );
  engPutVariable(_ep, "mapLBRow",      _mapLeftBottomRow_M );
  engPutVariable(_ep, "mapLBCol",      _mapLeftBottomCol_M );
  engPutVariable(_ep, "mapLBNorthing", _mapLB_Northing_M );
  engPutVariable(_ep, "mapLBEasting",  _mapLB_Easting_M );

  // DEBUG code follows
  printf(" We just put the MATLAB variables\n");
  engEvalString(_ep, "mapNumCols,mapLBRow,mapLBCol    ");
  printf("%s:%d: %s\n", __FILE__, __LINE__, buffer+2);

  // the reshape command reshape(var,numCols,numRows)' will give the 2D array
  engEvalString(_ep, "mapArray = reshape(mapData,mapNumCols,mapNumRows)';   ");
  // The flipud command flips the map vertically to account for the way in which
  // the cells are assigned (rows are referenced from the top in MATLAB)
  // The circshift accounts for the fact that the cell corresponding to the 
  // south-west most region is not necessarily the bottom left one
  engEvalString(_ep, "mapArray = circshift( flipud(mapArray), [-mapLBRow -mapLBCol]); ");
  
  // Echo the output from the command.  First two characters
  // are always the double prompt (>>).
  printf("%s:%d: %s\n", __FILE__, __LINE__, buffer+2);

  printf(" Done with UpdateCAEgenMap\n");


} // end UpdateCAEgenMap() 
