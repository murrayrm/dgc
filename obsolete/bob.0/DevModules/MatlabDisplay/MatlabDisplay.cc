/*
 * MatlabDisplay.cc
 */

#include "MatlabDisplay.hh"

extern int    PLAYBACK_DATA;
extern int    PLOT_DEFAULT_PATH;
extern int    GENMAP_DISPLAY;
extern int    NO_SPARROW_DISP;
extern unsigned long SLEEP_TIME;
extern char * LOGFILE_NAME;

char * timestamp = NULL;
char * testname = NULL;
// strings containing the names of the log files
char arbiterVar[255];
char votes00Var[255];
char votes01Var[255];
char votes02Var[255];
char votes03Var[255];
char votes04Var[255];
char votes05Var[255];
char votes06Var[255];
char votes07Var[255];
// strings containing the names of the load commands
char loadArbiterLog[255];
char loadVotes00Log[255];
char loadVotes01Log[255];
char loadVotes02Log[255];
char loadVotes03Log[255];
char loadVotes04Log[255];
char loadVotes05Log[255];
char loadVotes06Log[255];
char loadVotes07Log[255];

// originally we had thought about reading in these files within C++
// instead, we will load them directly into the Matlab workspace
/*
FILE * logfile;         // logfile is for state and command logs
// These files are for vote logging
vector<FILE *> voteLogFileVector;
*/

int PAUSED       = 0;
int CLEAR_FIGURE = 0;
int QUIT_PRESSED = 0;
int DRAW_FIGURE  = 1;


MatlabDisplay::MatlabDisplay() : DGC_MODULE(MODULES::MatlabDisplay, 
					    "Matlab Display", 0)
{
}

MatlabDisplay::~MatlabDisplay()
{
}


/******************************************************************************/
/******************************************************************************/
void MatlabDisplay::Init() 
{
    // call the parent init first
    DGC_MODULE::Init();

    /*
    * Use engOutputBuffer to capture MATLAB output, so we can
    * echo it back.
    */
    engOutputBuffer(_ep, buffer, BUFSIZE);	

    if( GENMAP_DISPLAY )
    {
      // Insert code to initialize MatlabDisplay here
      // First things first... receive the genMap over the MTA to 
      // make sure we know how much to allocate for the array
      // This assigns our genMapStruct in the datum.
      // TODO: Things will go bad if this first one is not received;
      // add some error handling 
      ReceiveCAEgenMap();
    }

    /////////////////OPEN LOGFILES (COPY FROM LADAR)/////////////
    // Actually, we won't open the logfiles here, but we will infer 
    // the names of the logfiles from the LOGFILE_NAME variable 
    if( PLAYBACK_DATA ) 
    {
      if( LOGFILE_NAME == NULL ) {
	fprintf(stderr, "ERROR! LOGFILE_NAME is NULL! \n");
	exit(-1);
      }
      // the filename specified should be the one that 
      // logs the state and the arbiter commands
      printf("LOGFILE_NAME: %s\n", LOGFILE_NAME);

      //p = strstr(file, "_scans_"); // Mike's original
      char * p = strstr(LOGFILE_NAME, "_arbiter_");
      timestamp = strdup(p+9);
      *p = 0;
      // set testname to be the prefix (before "_arbiter_") of the log filename
      testname = strdup(LOGFILE_NAME);
      p = strstr(timestamp,".log");
      *p = 0;

      printf("testname: %s\n", testname);
      printf("timestamp: %s\n", timestamp);

      // set the Matlab workspace names
      sprintf(arbiterVar, "%s_arbiter_%s", testname, timestamp );
      sprintf(votes00Var, "%s_votes00_%s", testname, timestamp );
      sprintf(votes01Var, "%s_votes01_%s", testname, timestamp );
      sprintf(votes02Var, "%s_votes02_%s", testname, timestamp );
      sprintf(votes03Var, "%s_votes03_%s", testname, timestamp );
      sprintf(votes04Var, "%s_votes04_%s", testname, timestamp );
      sprintf(votes05Var, "%s_votes05_%s", testname, timestamp );
      sprintf(votes06Var, "%s_votes06_%s", testname, timestamp );
      sprintf(votes07Var, "%s_votes07_%s", testname, timestamp );

      // set the Matlab command to open the log files
      sprintf(loadArbiterLog, "load %s.log", arbiterVar );
      sprintf(loadVotes00Log, "load %s.log", votes00Var );
      sprintf(loadVotes01Log, "load %s.log", votes01Var );
      sprintf(loadVotes02Log, "load %s.log", votes02Var );
      sprintf(loadVotes03Log, "load %s.log", votes03Var );
      sprintf(loadVotes04Log, "load %s.log", votes04Var );
      sprintf(loadVotes05Log, "load %s.log", votes05Var );
      sprintf(loadVotes06Log, "load %s.log", votes06Var );
      sprintf(loadVotes07Log, "load %s.log", votes07Var );

    }
    
    // LG: code from DFEPlanner_Matlab/DFEPlanner.cc
    // Initialize the Matlab engine

    _counter = 0; // setup our fake data generator, remove once message passing 
                  // with real data is available

    // start the Matlab Engine !!!
    // NOTE: see the Matlab External Interfaces Guide for more details.
    //       It is available online at: 
    //          External Interfaces / API:
    //  http://www.mathworks.com/access/helpdesk/help/techdoc/matlab_external/matlab_external.shtml
    //          External Interfaces / API Reference
    //  http://www.mathworks.com/access/helpdesk/help/techdoc/apiref/apiref.shtml
    {
      fprintf(stderr,"Starting the matlab engine...\n");
      if (!(_ep = engOpen("matlab -nojvm -nosplash"))) 
      {
	fprintf(stderr, "\nCan't start MATLAB engine\n");
        fprintf(stderr, "This is probably because one of the following reasons:\n");
        fprintf(stderr, " * If you're using the student version:\n");
        fprintf(stderr, "   - You don't have the Matlab documentation CD in the CD drive\n");
        fprintf(stderr, "   - If the CD is in the drive, it's not mounted (mount /dev/cdrom /mnt/cdrom as root)\n");
        fprintf(stderr, " * If you're using a licence server version:\n");
        fprintf(stderr, "   - You can't connect to the licence server (check internet connection)\n");
        exit(-1);
      }
      fprintf(stderr,"Matlab engine started.\n");
    }

    // ADD CODE HERE: Initialize the matlab variables
    _votes_M  = mxCreateDoubleMatrix(1, NUMARCS, mxREAL);
    _vel_M    = mxCreateDoubleMatrix(1, NUMARCS, mxREAL);
    // special array approximately matches the state struct.
    // Easting, Northing, Altitude, Vel_E, Vel_N, Vel_D (1-6)
    // Pitch, Roll, Yaw, PitchRate, RollRate, YawRate   (7-12)
    // Speed                                            (13)
    _currSS_M = mxCreateDoubleMatrix(1, 13, mxREAL);

    if( GENMAP_DISPLAY )
    {
      // set the variable that represents the genMap sent
      _mapNumRows_M       = mxCreateDoubleMatrix(1, 1, mxREAL);
      _mapNumCols_M       = mxCreateDoubleMatrix(1, 1, mxREAL);
      _mapRowRes_M        = mxCreateDoubleMatrix(1, 1, mxREAL);
      _mapColRes_M        = mxCreateDoubleMatrix(1, 1, mxREAL);
      _mapLeftBottomRow_M = mxCreateDoubleMatrix(1, 1, mxREAL);
      _mapLeftBottomCol_M = mxCreateDoubleMatrix(1, 1, mxREAL);
      _mapLB_Northing_M   = mxCreateDoubleMatrix(1, 1, mxREAL);
      _mapLB_Easting_M    = mxCreateDoubleMatrix(1, 1, mxREAL);
      // TODO: This is where things go bad if we didn't receive 
      // the genMap above
      _mapData_M = mxCreateDoubleMatrix(1, 
                   _d.CAE_genMapStruct.numRows * _d.CAE_genMapStruct.numCols, 
                   mxREAL);
    }

    // initially for the logged files, but may be generally useful
    _counter_M = mxCreateDoubleMatrix(1, 1, mxREAL);
    _counter_C = mxGetPr(_counter_M);
    
    // ADD CODE HERE: Initialize the C counterparts that point to the actual array of data
    _votes_C  = mxGetPr(_votes_M); // mxGetPr means "Get 'P'ointer to 'r'eal 
                                  // (as opposed to complex) data
    _vel_C    = mxGetPr(_vel_M);
    // C pointer to state array
    _currSS_C = mxGetPr(_currSS_M);

    if( GENMAP_DISPLAY )
    {
      _mapData_C          = mxGetPr(_mapData_M);    
      _mapNumRows_C       = mxGetPr(_mapNumRows_M);    
      _mapNumCols_C       = mxGetPr(_mapNumCols_M);    
      _mapRowRes_C        = mxGetPr(_mapRowRes_M);    
      _mapColRes_C        = mxGetPr(_mapColRes_M);    
      _mapLeftBottomRow_C = mxGetPr(_mapLeftBottomRow_M);    
      _mapLeftBottomCol_C = mxGetPr(_mapLeftBottomCol_M);    
      _mapLB_Northing_C   = mxGetPr(_mapLB_Northing_M);    
      _mapLB_Easting_C    = mxGetPr(_mapLB_Easting_M);    
    }

    // Initialize the Matlab variables for displaying module votes
    _voters_goodness_M = mxCreateDoubleMatrix(ArbiterInput::Count, NUMARCS, mxREAL);
    _voters_velocity_M = mxCreateDoubleMatrix(ArbiterInput::Count, NUMARCS, mxREAL);

    _combined_goodness_M = mxCreateDoubleMatrix(1, NUMARCS, mxREAL);
    _combined_velocity_M = mxCreateDoubleMatrix(1, NUMARCS, mxREAL);

    _arbiter_best_arc_M = mxCreateDoubleMatrix(1, 1, mxREAL);

    // Initialize C pointer to array in Matlab variables
    _voters_goodness_C = mxGetPr(_voters_goodness_M);
    _voters_velocity_C = mxGetPr(_voters_velocity_M);

    _combined_goodness_C = mxGetPr(_combined_goodness_M);
    _combined_velocity_C = mxGetPr(_combined_velocity_M);

    _arbiter_best_arc_C = mxGetPr(_arbiter_best_arc_M);


    // ADD CODE HERE: Set up workspace variables that we will use again
    // The Log variables are character strings that contain the Matlab 
    // commands load the arbiter log files
    if( PLAYBACK_DATA ) 
    {
      printf(" Loading log files...\n");
      printf("  ...arbiter (%s)\n", loadArbiterLog);
      /*
      * Echo the output from the command.  First two characters
      * are always the double prompt (>>).
      */
      printf("%s:%d: %s\n", __FILE__, __LINE__, buffer+2);
      engEvalString(_ep, loadArbiterLog );
      printf("  ...votes00 (%s)\n", loadVotes00Log);
      engEvalString(_ep, loadVotes00Log );
      printf("  ...votes01 (%s)\n", loadVotes01Log);
      engEvalString(_ep, loadVotes01Log );
      printf("  ...votes02 (%s)\n", loadVotes02Log);
      engEvalString(_ep, loadVotes02Log );
      printf("  ...votes03 (%s)\n", loadVotes03Log);
      engEvalString(_ep, loadVotes03Log );
      printf("  ...votes04 (%s)\n", loadVotes04Log);
      engEvalString(_ep, loadVotes04Log );
      printf("  ...votes05 (%s)\n", loadVotes05Log);
      engEvalString(_ep, loadVotes05Log );
      printf("  ...votes06 (%s)\n", loadVotes06Log);
      engEvalString(_ep, loadVotes06Log );
      printf("  ...votes07 (%s)\n", loadVotes07Log);
      engEvalString(_ep, loadVotes07Log );
      /*
      * Echo the output from the command.  First two characters
      * are always the double prompt (>>).
      */
      printf("%s:%d: %s\n", __FILE__, __LINE__, buffer+2);
      printf(" ...done loading log files.\n");

      // DEBUG
      engEvalString(_ep, "who" );
      /*
      * Echo the output from the command.  First two characters
      * are always the double prompt (>>).
      */
      printf("%s:%d: %s\n", __FILE__, __LINE__, buffer+2);
      printf(" Loaded all of the log files.\n");
      engEvalString(_ep, "save logfiles.mat  ");
    }

    // ADD CODE HERE: Set up plot figures with graphics handles
    {
      // open figure and setup for quick plotting
      /*      Ordinarily, you would just plot data with a 
              plot(x,y) command.
              However, if you keep track of the plotted element's handle
              h_line = plot(x,y);
              Then you can replot new data much more quickly by doing
              set(h_line, 'XData', x_new, 'YData', y_new);
              drawnow;
              */
      /* This was the initial sample figure from Luis
         engEvalString(_ep, " figure(100);                        ");
         engEvalString(_ep, " subplot(2,1,1)                      ");
      // h_votes is a handle to the line plot of votes
      // later we can efficiently replot it by replacing the
      // data that h_votes uses. 
      engEvalString(_ep, " h_votes = plot(zeros(1,25),'.-');   ");
      engEvalString(_ep, " xlabel('votes')                     ");

      engEvalString(_ep, " subplot(2,1,2)                      ");
      // likewise, h_vel is a handle to the line plot of velocities
      engEvalString(_ep, " h_vel = plot(zeros(1,25),'.-');     ");
      engEvalString(_ep, " xlabel('vel')                       ");
      // force rendering of plots
      engEvalString(_ep, " drawnow;                            "); 
      */

      // initialize the figure in order to draw the vehicle path
      engEvalString(_ep, 
                      "fh = figure('Position',[-100 0 1000 384]); hold on  ");
      engEvalString(_ep, "set(gcf,'DoubleBuffer','on')                   ");
      engEvalString(_ep, "figure(fh), subplot(1,2,2)                     ");
      engEvalString(_ep, "wp = plot_corridor_file('bob.dat');                 ");
      if( PLOT_DEFAULT_PATH )
      {
        engEvalString(_ep, "gen_rddf_path('bob.dat')                       ");
      }
      /* New feature.  Try me! */
      //engEvalString(_ep, "gen_rddf_path('bob.dat')                  ");
      engEvalString(_ep, "draw_vehicle('veh',wp(1,1),wp(1,2),0,0,'k',1)  ");

      /*
      * Echo the output from the command.  First two characters
      * are always the double prompt (>>).
      */
      printf("%s:%d: %s\n", __FILE__, __LINE__, buffer+2);

      // Initialize figure that plots all the Votes the Arbiter sees
      // The indices of the subplots are in initialize_voters_figure
      engEvalString(_ep, "initialize_voters_figure;                      ");

      /*
      * Echo the output from the command.  First two characters
      * are always the double prompt (>>).
      */
      printf("%s:%d: %s\n", __FILE__, __LINE__, buffer+2);


      // Get the genMap from CorridorArcEvaluator
      // This assigns our _d.map and the variables associated with it.
      //UpdateCAEgenMap();

      // initialize the figure to draw the genMaps

      if( GENMAP_DISPLAY )
      {
        engEvalString(_ep, 
                        "fh2 = figure('Position',[200 400 512 384]); hold on       ");
        engEvalString(_ep, "set(gcf,'DoubleBuffer','on')             ");
        engEvalString(_ep, 
                        "h_imagesc = imagesc(mapArray), axis tight, axis equal     ");

        // TODO: What the heck is this stuff?
        // TODO: Just do imagesc at first.
        // the results from this are too funky
        //engEvalString(_ep, 
        //  "plot_gmstruct_2d(mapData,[mapRowRes mapColRes],[mapLBRow mapLBCol],[mapLBNorthing mapLBEasting])");
        //engEvalString(_ep, 
        //  "hgm = plot_genmap_2d('CAEgenMap',[wp(1,2) wp(1,1)]);");
      }


    }

    fprintf(stderr, "Sleeping for 1 seconds... this is what the plots look like after initialization\n");
    usleep(1000000);

    // start Sparrow Display
    if( !NO_SPARROW_DISP )
    {
      RunMethodInNewThread<MatlabDisplay>( this, 
                      &MatlabDisplay::SparrowDisplayLoop);
      RunMethodInNewThread<MatlabDisplay>( this, 
                      &MatlabDisplay::UpdateSparrowVariablesLoop);
    }

    cout << ModuleName() << "Init Finished" << endl;
}

/******************************************************************************/
/******************************************************************************/
void MatlabDisplay::Active()
{
  cout << "MatlabDisplay: Starting Active Funcion" << endl;

  int counter = 0;
  
  while( ContinueInState() ) 
  {
    /* DEBUG */
    //printf("At beginning of Active function.\n");

    counter++;
    // update the pointer to our Matlab array
    _counter_C[0] = (double)counter;
    // with this we can choose which data row of the log files to plot
    engPutVariable(_ep, "frame", _counter_M);

    // ADD CODE HERE: Get messages from other modules
    // update the state so we know where the vehicle is at
    // This assigns our _d.SS
    // We don't need to assign this if we are reviewing logged
    // data because we work directly in the Matlab workspace in that case
    if( !PLAYBACK_DATA ) 
    {
      /* DEBUG */
      //printf("Running UpdateState...\n");
      UpdateState();
      if(_d.SS.Northing == 0)
      {
        printf("\nUnable to communicate with vstate (Northing is zero).\n");
        printf("Run with --nostate option if this is OK.\n\n");
      }

      // This assigns all of the votes that we receive from the arbiter
      /* DEBUG */
      //printf("Running UpdateArbiterVoters...\n");
      UpdateArbiterVoters();

      if( GENMAP_DISPLAY && DRAW_FIGURE )
      {
        //ReceiveCAEgenMap();

        // Update the genMap we receive from CorridorArcEvaluator
        // This assigns the proper variables and puts the appropriate
        // Matlab variables in the workspace
        /* DEBUG */
        //printf("Running UpdateCAEgenMap...\n");
        UpdateCAEgenMap();

        // PlotGenMap will take the new data we have from the above call to 
        // UpdateCAEgenMap and will plot it to figure fh2
        /* DEBUG */
        //printf("Running PlotGenMap...\n");
        PlotGenMap();

        // TODO: What the heck is this?
        //engEvalString(_ep, "plot_gmstruct_2d(mapArray,[mapRowRes mapColRes],[mapLBRow mapLBCol],[mapLBNorthing mapLBEasting]);  ");
        // DEBUG:
        // Echo the output from the command.  First two characters
        // are always the double prompt (>>).
        printf("%s:%d: %s\n", __FILE__, __LINE__, buffer+2);
      }

      for (int i=0; i<ArbiterInput::Count; i++) {
	for (int j=0; j<NUMARCS; j++) {
	  _voters_goodness_C[i + ArbiterInput::Count*j] = 
		  _d.arbiterVoters[i].Votes[j].Goodness;
	  _voters_velocity_C[i + ArbiterInput::Count*j] = 
		  _d.arbiterVoters[i].Votes[j].Velo;
	}
      }

      for (int j=0; j<NUMARCS; j++)
      {
	_combined_goodness_C[j] = _d.combinedVote.Votes[j].Goodness;
	_combined_velocity_C[j] = _d.combinedVote.Votes[j].Velo;
      }

      // initialize the arbiter best_arc variable too
      // first set it to the commanded angle in radians
      _arbiter_best_arc_C[0] = _d.arbiter_cmd.steer_cmd * USE_MAX_STEER;
      //printf(" _arbiter_best_arc rad = %f\n", _arbiter_best_arc_C[0] );
      // then reset it to the arc index
      _arbiter_best_arc_C[0] = (double)GetArcNum(_arbiter_best_arc_C[0]);
      //printf(" _arbiter_best_arc index = %d\n", round(_arbiter_best_arc_C[0]) ); 

      // put the votes in the Matlab workspace 
      engPutVariable(_ep, "voter_goodness", _voters_goodness_M );
      engPutVariable(_ep, "voter_velocity", _voters_velocity_M );

      engPutVariable(_ep, "combined_goodness", _combined_goodness_M );
      engPutVariable(_ep, "combined_velocity", _combined_velocity_M );

      // arbiter_best_arc is the 0-indexed index of the arbiter's chosen arc
      engPutVariable(_ep, "arbiter_best_arc", _arbiter_best_arc_M );
    } 
    else // loggedDataRun is true
    {
      // TODO: Assign me here, boo.
      // assign the proper variables in the Matlab workspace 
      // (the same ones as above)
      //engEvalString(_ep, " voter_goodness = ...(frame,1:25)...                        ");
      //engEvalString(_ep, " voter_velocity = ...(frame,1:25)...                        ");
    }

    // update the matlab voters figure
    engEvalString(_ep, "update_voters_figure;                        ");


    // For now we just make up some data
    /*
       for(int i=0; i<NUMARCS; i++) 
       {
       _d.matlabdisplay.Votes[i].Goodness = 15* sin( (_counter + i)*3.14/180.0 );
       _d.matlabdisplay.Votes[i].Velo = 15* cos( (_counter + i)*3.14/180.0 );

       _counter += 1;
       }
       */

    // ADD CODE HERE: Copy variables for matlab display
    // NOTE that since _votes_C points inside _votes_M, we've copied the
    // data to the matlab variable.
    /*
       for(int i=0; i<NUMARCS; i++) 
       {
       _votes_C[i] = _d.matlabdisplay.Votes[i].Goodness;
       _vel_C[i] = _d.matlabdisplay.Votes[i].Velo;
       }
       */

    if( !PLAYBACK_DATA ) 
    {
      // assign the pointer array elements
      // special array approximately matches the state struct.
      // Easting, Northing, Altitude, Vel_E, Vel_N, Vel_D (1-6)
      // Pitch, Roll, Yaw, PitchRate, RollRate, YawRate   (7-12)
      // Speed                                            (13)
      _currSS_C[0]  = _d.SS.Easting;
      _currSS_C[1]  = _d.SS.Northing;
      _currSS_C[2]  = _d.SS.Altitude;
      _currSS_C[3]  = _d.SS.Vel_E;
      _currSS_C[4]  = _d.SS.Vel_N;
      _currSS_C[5]  = _d.SS.Vel_D;
      _currSS_C[6]  = _d.SS.Pitch;
      _currSS_C[7]  = _d.SS.Roll;
      _currSS_C[8]  = _d.SS.Yaw;
      _currSS_C[9]  = _d.SS.PitchRate;
      _currSS_C[10] = _d.SS.RollRate;
      _currSS_C[11] = _d.SS.YawRate;
      _currSS_C[12] = _d.SS.Speed();

      // ADD CODE HERE: Place the matlab variables into the MATLAB workspace.
      // example code
      /*
	 engPutVariable(_ep, "votes", _votes_M);
	 engPutVariable(_ep, "vel", _vel_M);
	 */

      // put the state array in the workspace 
      engPutVariable(_ep, "currSS", _currSS_M );
    }
    else 
    {
      // do the Matlab commands to update the state of the vehicle 
      // in the plot
      // TODO: Put code right here, fool.
      //engEvalString(_ep, " currSS = ...(frame,1:25)...                        ");
    }

    // ADD CODE HERE: Call matlab plot commands
    // for the fastest replotting, we just reset the 
    // Y-axis data that each line plot handle uses
    // (same number of elements, in vector, same XData)

    /*
    // sample code
    engEvalString(_ep, " figure(100);                        ");
    engEvalString(_ep, " set(h_votes,'YData',votes);         ");
    engEvalString(_ep, " set(h_vel  ,'YData',vel  );         ");
    engEvalString(_ep, " drawnow;                            "); // force rendering of plots
    */     

    // now update the state plot on the right hand side
    // we want to do this whether we're reviewing logged data or not, since
    // it's in the Matlab workspace
    // wait until we get a nonzero value
    if( _d.SS.Easting > 0 ) 
    {	
      //printf("Got here and _d.SS.Easting = %10.3f\n", _d.SS.Easting );

      engEvalString(_ep, "figure(fh); hold on; subplot(2,2,[2 4])          ");
      // plot the position relative to the first waypoint
      // DEBUG:
      engEvalString(_ep, "currSS(1),wp(1,1),currSS(2),wp(1,2)  "); 
      
      /* Echo the output from the command.  First two characters
       * are always the double prompt (>>). */
      //printf("%s:%d: %s\n", __FILE__, __LINE__, buffer+2);

      engEvalString(_ep, "plot(currSS(1)-wp(1,1), currSS(2)-wp(1,2), '.')  "); 

      /* Echo the output from the command.  First two characters
       * are always the double prompt (>>). */
      //      printf("%s:%d: %s\n", __FILE__, __LINE__, buffer+2);

      engEvalString(_ep,
                      "draw_vehicle('veh',currSS(1)-wp(1,1),currSS(2)-wp(1,2),currSS(9),0,'k',1,arbiter_best_arc) ");
      // make the axes follow the vehicle

      /* Echo the output from the command.  First two characters
       * are always the double prompt (>>). */
      //      printf("%s:%d: %s\n", __FILE__, __LINE__, buffer+2);

      /* DEBUG */
      //engEvalString(_ep, "axis([-100 100 -100 100]");
      engEvalString(_ep, 
   "axis([currSS(1)-wp(1,1)-50 currSS(1)+50-wp(1,1) currSS(2)-wp(1,2)-50 currSS(2)-wp(1,2)+50]) ");

      //engEvalString(_ep, "axis tight; axis equal               "); 
      // force rendering of plots      
      engEvalString(_ep, "drawnow;                             "); 
      
      /* Echo the output from the command.  First two characters
       * are always the double prompt (>>). */
      //printf("%s:%d: %s\n", __FILE__, __LINE__, buffer+2);

    }

    /* DEBUG */
    //printf("Sleeping for %ld usec...", SLEEP_TIME);
    // and sleep for a bit
    usleep( SLEEP_TIME );
    /* DEBUG */
    //printf("...done sleeping.\n");

  } // end while 

  cout << "Finished Active State" << endl;
 }

/******************************************************************************/
/******************************************************************************/
void MatlabDisplay::Shutdown()
{

    // if we get to here a break has been detected 
    cout << "Shutting Down" << endl;

    //Insert code to shut down MatlabDisplay here

    // ADD CODE HERE: release matlab variables
    // example variables
    mxDestroyArray(_votes_M);
    mxDestroyArray(_vel_M);
    // special state array
    mxDestroyArray(_currSS_M);

    // close the Matlab Engine
    engClose(_ep);

}

/******************************************************************************/
/******************************************************************************/
void MatlabDisplay::PlotGenMap()
{
  engEvalString(_ep, "figure(fh2)                 ");
  //engEvalString(_ep, "plot_gmstruct_2d(mapArray, [mapRowRes mapColRes], [mapLBNorth-wp(1,2) mapLBEast-wp(1,1)]), colormap cool;  ");
  //engEvalString(_ep, "set(h_imagesc, ,'CData', mapArray), drawnow");
  engEvalString(_ep, "set(h_imagesc, 'CData', rand(200,200))");
	  
  /* Echo the output from the command.  First two characters
   * are always the double prompt (>>). */
  printf("%s:%d: %s\n", __FILE__, __LINE__, buffer+2);

  // This is now done in UpdateCAEgenMap
  //engEvalString(_ep, "imagesc( circshift(flipud(mapArray), [-mapLBRow -mapLBCol]) )");
  //engEvalString(_ep, "axis([1 mapNumCols 1 mapNumRows]), axis equal              ");
  //engEvalString(_ep, "drawnow                                                 ");
  //engEvalString(_ep, "figure(3), plot([1:100],sin([1:100])),  drawnow               ");

  // DEBUG
  //printf(" got an image!\n");
  //engEvalString(_ep, "mapArray                                          ");
  //engEvalString(_ep, "save CtoMat.mat                                    ");

  /*
  * Echo the output from the command.  First two characters
  * are always the double prompt (>>).
  */
  //printf("%d %s", __LINE__, buffer+2);
}
