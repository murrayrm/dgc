#include "MatlabDisplay.hh"

using namespace std;

// Give the options distinct values.
const int PLAY_OPT   = 10;
const int SLEEP_OPT  = 11;

// Set the default values for option arguments.
int NO_SPARROW_DISP      = 0;        // Whether or not to show the display
unsigned long SLEEP_TIME = 50000;    // How long in us to sleep between updates
int PLAYBACK_DATA        = 0;        // Whether we're viewing logged data.
int PLOT_DEFAULT_PATH    = 0;        // Whether to plot the default path.
int GENMAP_DISPLAY       = 0;        // Whether to additionally display genMaps
char *LOGFILE_NAME       = 0;

/* The name of this program. */
const char * program_name;

/* Prints usage information for this program to STREAM (typically stdout 
   or stderr), and exit the program with EXIT_CODE. Does not return. */
void print_usage (FILE* stream, int exit_code)
{
  fprintf( stream, "Usage:  %s [options]\n", program_name );
  fprintf( stream,
           "  --nodisp          Do not run Sparrow display.\n"
           "  --sleep us        Specifies us sleep time in Active function.\n"  
           "  --path            Plot the default PathFollower path.\n"
           "  --play <file>     Play back logged data (not implemented).\n"
           "                    When implemented, <file> should be the full\n"
           "                    filename of the arbiter log.\n"
           "  --mapdisp         Additionally, display map figure (caution:\n"
           "                    this is alpha and may make things run slow!\n"
           "  --help, -h        Display this message.\n\n" );      
  exit(exit_code);
}

int main(int argc, char **argv) 
{
  //---Argument Checking---------------------------------------------------//
  int ch;
  /* An array describing valid long options. */
  static struct option long_options[] = 
  {
    // first: long option (--option) string
    // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
    // third: if pointer, set variable to value of fourth argument
    //        if NULL, getopt_long returns fourth argument
    {"nodisp",     0, &NO_SPARROW_DISP,   1},
    {"mapdisp",    0, &GENMAP_DISPLAY,    1},
    {"path",       0, &PLOT_DEFAULT_PATH, 1},
    {"play",       1, NULL,               PLAY_OPT},
    {"sleep",      1, NULL,               SLEEP_OPT},
    {"help",       0, NULL,               'h'},
    {NULL,         0, NULL,               0}
  };

  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  program_name = argv[0];
  int option_index = 0;
  printf("\n");

  // Loop through and process all of the command-line input options.
  while((ch = getopt_long(argc, argv, "h", long_options, &option_index)) != -1)
  {
    switch(ch)
    {
      case PLAY_OPT:
        { PLAYBACK_DATA = 1;
          LOGFILE_NAME = strdup(optarg); }
          break;

      case SLEEP_OPT:
        SLEEP_TIME = (unsigned long) atol(optarg);
        break;
     
      case 'h':
        /* User has requested usage information. Print it to standard
           output, and exit with exit code zero (normal 
           termination). */
        print_usage(stdout, 0);

      case '?': /* The user specified an invalid option. */
        /* Print usage information to standard error, and exit with exit
           code one (indicating abnormal termination). */
        print_usage(stderr, 1);

      case -1: /* Done with options. */
        break;

    } // end switch
  } // end while

  //--Start the MTA By registering a module--------------------------------//
  // and then starting the kernel
  Register(shared_ptr<DGC_MODULE>(new MatlabDisplay));
  StartKernel();

  return 0;
}
