
//Sim.cc

#include "Sim.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"
#include "MTA/Misc/Time/Time.hh"

extern int LOG_DATA; // used to determine log data

SIM_DATUM d; // to be shared by VState and VDrive

int QUIT_PRESSED = 0;
int PAUSED       = 0;
int RESET        = 0;

//int ServedState=0;
//int CmdMotionCount=0;

SimVDrive::SimVDrive() 
  : DGC_MODULE(MODULES::SimVDrive, "VDrive Simulator", 0) { }

void SimVDrive::Active() 
{
  Timeval stActive = TVNow();
  time_t curTime;
  struct tm* localTime;
  string filename;
  int ret;
  char year[5], month[3], day[3], hour[3], min[3], sec[3];
  ofstream accelFile;

  // data log if
  if(LOG_DATA)
  {
    // Get the current time
    curTime = time(NULL);

    // Convert it to local time representation
    localTime = localtime (&curTime);
    ret = sprintf(year,"%d",localTime->tm_year + 1900);
    ret = sprintf(month,"%d",localTime->tm_mon + 1);
    ret = sprintf(day,"%d",localTime->tm_mday);
    ret = sprintf(hour,"%d",localTime->tm_hour);
    ret = sprintf(min,"%d",localTime->tm_min);
    ret = sprintf(sec,"%d",localTime->tm_sec);

    filename = LOG_FILE_DIR;
    filename += LOG_FILE_NAME;
    filename += year;
    filename += "-";
    filename += month;
    filename += "-";
    filename += day;
    filename += "_";
    filename += hour;
    filename += "h";
    filename += min;
    filename += "m";
    filename += sec;
    filename += "s";
    filename += LOG_FILE_EXT;

    accelFile.open(filename.c_str());
    accelFile << setiosflags(ios::showpoint) << setprecision(10);
    accelFile << "% Time(s)\taccel([-1,1])\tsteer(rad)\tSpeed(m/s)"
	      << "\tEasting\tNorthing\tAltitude\tEastVel\tNorthVel\tVertVel"
	      << "\tOBD Speed\tOBD RPM\tYaw(rad)" << endl;

  } //end of data log if
  while( ContinueInState() && !QUIT_PRESSED ) 
  {
    usleep(250000);

    // this conditional defines the logging rate
    if( (TVNow() - stActive) > Timeval(0,100000) ) 
    {
      stActive = TVNow();
#if 0
			// the old state struct
   cout << "SimVDrive: Pos-E,N(" 
	   << d.SS.Easting << ", " 
	   << d.SS.Northing << ") Vel["
	   << d.SS.Speed << "]E,N("
	   << d.SS.Vel_E << ", " 
	   << d.SS.Vel_N << "), Yaw("
	   << d.SS.Yaw << "), Srv("
	   << d.ServedState << ") Rcv(" 
	   << d.CmdMotionCount << ") Last("
	   << d.cmd.accel_cmd << ", "
	   << d.cmd.steer_cmd << ")"
	   << endl;
#endif

      // data log if
      if(LOG_DATA)
      {
				// only the old state struct
        accelFile << (TVNow() - d.startTime) << "\t"
                  << d.cmd.accel_cmd << "\t"
                  << -d.cmd.steer_cmd << "\t" // steering has opposite sign 
		                              // convention in simulator
									<< d.SS.Speed << "\t"
                  << d.SS.Easting << "\t"
                  << d.SS.Northing << "\t"
                  << 0 << "\t" //Altitude, not generated
                  << d.SS.Vel_E << "\t"
                  << d.SS.Vel_N << "\t"
                  << 0 << "\t" // Vel_U, not generated
                  << 0 << "\t" // odbSpeed, not generated
                  << 0 << "\t" // RPM, not generated
                  << d.SS.Yaw << "\t"  // Yaw, from north, measured CW
                  << endl;

      }//end data log if

    }

  }
}

void SimVDrive::InMailHandler(Mail& msg) 
{
  switch (msg.MsgType()) 
  {
    case VDriveMessages::CmdDirectDrive:
      /* Get the motion data and store it locally */
      d.CmdMotionCount++;
      msg >> d.cmd;
      //cout << "Got command message!" << endl;
      break;

    default:
      // ignore
      break;
  }
}



SimVState::SimVState() : DGC_MODULE(MODULES::VState, "VState Simulator", 0) 
{

}


void SimVState::Init() 
{
  DGC_MODULE::Init();

  d.startTime = TVNow();

  // clear the state struct and actuators
  d.cmd.accel_cmd = 0;
  d.cmd.steer_cmd    = 0;

  d.ServedState    = 0;
  d.ServedNewState    = 0;
  d.CmdMotionCount = 0;
  
  // INITIAL CONDITIONS ARE NOW SET IN INCLUDE/CONSTANTS.H
  // (MUST) REMAKE THE SIMULATOR MODULE

  d.lastUpdate = TVNow();

  SimInit();

  // start Sparrow Display
  RunMethodInNewThread<SimVState>( this, &SimVState::SparrowDisplayLoop);
  RunMethodInNewThread<SimVState>( this, &SimVState::UpdateSparrowVariablesLoop);
 
  cout << ModuleName() << "Init Finished" << endl;

}


void SimVState::Active() 
{
  while ( ContinueInState() ) 
  {
    usleep_for(10000);
    Timeval deltaT = (TVNow() - d.lastUpdate);
    if( !PAUSED ) 
    {
      SimUpdate( (double) deltaT.sec() + (((double) deltaT.usec()) / 1.0e6));
    } 
    if( RESET )
    {
      SimInit();
      RESET = 0;
    }
    
    d.lastUpdate = TVNow();
  }
}


Mail SimVState::QueryMailHandler(Mail& msg) 
{
  Mail reply;			// allocate space for the reply

  // figure out what we are responding to
  switch(msg.MsgType()) {  
  case VStateMessages::GetState:  
    d.ServedState++;
    reply = ReplyToQuery(msg);
    reply << d.SS;
    break;

  case VStateMessages::GetVehicleState:  
    d.ServedNewState++;
    reply = ReplyToQuery(msg);
    reply << d.SS_new;
    break;

  default:
    // let the parent mail handler handle it.
    reply = DGC_MODULE::QueryMailHandler(msg);
    break;
  }

  return reply;
}
