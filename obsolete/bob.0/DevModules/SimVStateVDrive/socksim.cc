#include <iostream>
using namespace std;

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include "simulator.hh"
#include <fstream>
#include <iomanip>
using namespace std;

#define SERVERPORT			5555

#define BACKLOG					10		 // how many pending connections queue will hold

#define GETCMD_DELAY								125000000
#define SIM_DELAY										1000000
struct SState
{
	double n[3];
	double e[3];
	double yaw;
};

#define sn   state.n[0]
#define snd  state.n[1]
#define sndd state.n[2]
#define se   state.e[0]
#define sed  state.e[1]
#define sedd state.e[2]
#define sy   state.yaw
struct SCmd
{
	double accel;
	double phi;
};

int sendsock(int datafd, char* buf, int len)
{
	int numsent, numlefttosend;
	numlefttosend = len;

	do
	{
		numsent = send(datafd, buf + len - numlefttosend,
									 numlefttosend, 0);
		numlefttosend -= numsent;

		if(numsent == -1)
		{
			cerr << "Couldn't send data\n";
			return -1;
		}

		if(numsent == 0)
		{
			cerr << "no data sent\n";
			return -2;
		}
	} while(numlefttosend != 0);

	return 0;
}

int recvsock(int datafd, char* buf, int len)
{
	int numreceived, numlefttorecv;
	numlefttorecv = len;

	do
	{
		numreceived = recv(datafd, buf + len - numlefttorecv,
									 numlefttorecv, 0);
		numlefttorecv -= numreceived;

		if(numreceived == -1)
		{
			cerr << "Couldn't recv data\n";
			return -1;
		}

		if(numreceived == 0)
		{
			cerr << "No data received: \n";
			return -2;
		}
	} while(numlefttorecv != 0);

	return 0;
}

SCmd cmd;
simData simstate;
int setupSockets(void)
{
	int datafd, socketfd;
	union
	{
		struct sockaddr_in addr_in;
		struct sockaddr 	 addr;
	};

	socklen_t sin_size;
	int yes = 1;


	socketfd = socket(AF_INET, SOCK_STREAM, 0);
	if(socketfd == -1)
	{
		cerr << "Couldn't open server socket\n";
		return -1;
	}

	if(setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1)
	{
		cerr << "Couldn't reuse the socket\n";
		close(socketfd);
		return -1;
	}

	memset(&addr, '\0', sizeof(addr));
	addr_in.sin_family = AF_INET;
	addr_in.sin_port = htons(SERVERPORT);
	addr_in.sin_addr.s_addr = INADDR_ANY;

	if(bind(socketfd, &addr, sizeof(addr)) == -1)
	{
		cerr << "Couldn't bind the socket\n";
		close(socketfd);
		return -1;
	}

	if(listen(socketfd, BACKLOG) == -1)
	{
		cerr << "Couldn't listen to the socket\n";
		close(socketfd);
		return -1;
	}

	sin_size = sizeof(addr);
	if ((datafd = accept(socketfd, &addr, &sin_size)) == -1)
	{
		cerr << "Couldn't accept the socket to the client\n";
		close(socketfd);
		return -1;
	}

	close(socketfd);
	return datafd;
}

void* getcmd_thread_func(void *pArg)
{
	SState state;

	int datafd = (int)pArg;

	timespec sleeptime;

	while(true)
	{
		sn   = simstate.n;
		se   = simstate.e;
		snd  = simstate.n_vel;
		sed  = simstate.e_vel;
		sndd = simstate.n_acc;
		sedd = simstate.e_acc;
		sy   = simstate.yaw;
		if(sendsock(datafd, (char*)&state, sizeof(state)) != 0)
		{
			close(datafd);
			return NULL;
		}

		if(recvsock(datafd, (char*)&cmd, sizeof(cmd)) != 0)
		{
			close(datafd);
			return NULL;
		}

		sleeptime.tv_sec = 0;
		sleeptime.tv_nsec = GETCMD_DELAY;
		nanosleep(&sleeptime, NULL);
	}
}


int main(int argc, char **argv)
{
	ofstream outfile("ff.traj");
	outfile << setprecision(10);

	pthread_t getcmd_thread_id;
	int datafd = setupSockets();
	timespec sleeptime;
	timeval oldtime;
	timeval currtime;
	double deltaT;

	if(datafd < 0)
		return 0;

	Simulator sim;

	cmd.accel = 0.0;
	cmd.phi = 0.0;
  simstate.n = SIMULATOR_INITIAL_NORTHING;
  simstate.e = SIMULATOR_INITIAL_EASTING;
  simstate.vel = 0.0;
  simstate.n_vel = 0.0;
  simstate.e_vel = 0.0;
  simstate.u_vel = 0.0;
  simstate.n_acc = 0.0;
  simstate.e_acc = 0.0;
  simstate.yaw = SIMULATOR_INITIAL_YAW;
  sim.initState(simstate);
	gettimeofday(&oldtime, NULL);




// 	while(true)
// 	{
// 		deltaT = 0.01;
// 		sim.setStep(deltaT);
// 		sim.setSim(0.0, 0.04,0.0);
// 		sim.simulate();
// 	}







	if(pthread_create(&getcmd_thread_id, NULL, &getcmd_thread_func, (void*)datafd) != 0)
	{
		cerr << "Couldn't create getcmd thread\n";
		close(datafd);
		return 0;
	}

	while(true)
	{
		sleeptime.tv_sec = 0;
		sleeptime.tv_nsec = SIM_DELAY;
		nanosleep(&sleeptime, NULL);

		gettimeofday(&currtime, NULL);
		deltaT = (double)(currtime.tv_sec - oldtime.tv_sec) + 1.0e-6*(currtime.tv_usec - oldtime.tv_usec);

		sim.setStep(deltaT);
		sim.setSim(0.0, cmd.accel, cmd.phi);
		sim.simulate();
		simstate = sim.getState();
		memcpy(&oldtime, &currtime, sizeof(currtime));

		outfile
			<< simstate.n << ' ' << simstate.n_vel << ' ' << simstate.n_acc << ' '
			<< simstate.e << ' ' << simstate.e_vel << ' ' << simstate.e_acc << endl;
	}

	close(datafd);
	return 0;
}


