/***************************************************************************************/
//FILE: 	sim_loop.cc
//
//AUTHOR:	Thyago Consort
//
//DESCRIPTION:	This file has the loop that will run in one of the threads
//
/***************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <list>
#include <algorithm>

#include "Sim.hh"
#include "latlong/ggis.h"
#include "latlong/contest-info.h"
#include "Constants.h"
#include "simulator.hh"
#include "DGCutils"

// Yaw uncertainty in percentage of the real one, 0.1 = 10%
#define YAW_UNCERTAINTY 0.00
//#define YAW_UNCERTAINTY 0.05
// Position Uncertainty between 0 and 30cm (0.6 = 30cm)
#define POSITION_UNCERTAINTY 0.0
//#define POSITION_UNCERTAINTY 0.6

using namespace std;

simData simState;
Simulator simEngine;
double t;

extern SIM_DATUM d;

#include "vehlib/VState.hh"		/* grab message format for VState */

void SimVState::SimInit() 
{
  // Initializing
  // TODO: Initialize variables.
  t = 0.0;

  // Updating initial state
  simState.n = SIMULATOR_INITIAL_NORTHING;
  simState.e = SIMULATOR_INITIAL_EASTING;
  simState.vel = 0.0;
  simState.n_vel = 0.0;
  simState.e_vel = 0.0;
  simState.u_vel = 0.0;
  simState.n_acc = 0.0;
  simState.e_acc = 0.0;
  simState.yaw = SIMULATOR_INITIAL_YAW;
  simEngine.initState(simState);

	// seed the random number generator
	srand(time(NULL));
}
  
void SimVState::SimUpdate(float deltaT) 
{
#if 0
	// only the old state struct
	ifstream infile("state");
	infile >> d.SS.Northing >> d.SS.Easting >> d.SS.Speed >> d.SS.Yaw >> d.SS.PitchRate >> d.SS.RollRate;
	infile.close();

	d.SS.Vel_N    = d.SS.Speed * cos(d.SS.Yaw);
  d.SS.Vel_E    = d.SS.Speed * sin(d.SS.Yaw);
  d.SS.Vel_U    = 0.0;
  d.SS.Pitch = 0.0;
  d.SS.Roll = 0.0;


	return;
#endif

  // Generating randomic numbers
  double deltaYaw, deltaPosition;
  deltaYaw = ( (double)rand() / (double)(RAND_MAX+1) );
  deltaPosition = ( (double)rand() / (double)(RAND_MAX+1) );

  // Step
  simEngine.setStep(deltaT);

  // Adding commands to the simulator
  simEngine.setSim(t,d.cmd.accel_cmd,d.cmd.steer_cmd);

  // Simulating (alternatives: simulate_with_cc() (2004 version), 
  // simulate_planar_only() (Dima's version)
  simEngine.simulate();
  simState = simEngine.getState();

  //////////??DEBUG/////////////////////////
  /*
  cout << "new simState: "
       << simState.n << ' ' 
       << simState.e << ' '
       << simState.yaw << ' '
       << simState.n_acc << ' '
       << simState.e_acc << endl;
  */

  //Updating state
  d.SS.Northing = simState.n + deltaPosition*POSITION_UNCERTAINTY + POSITION_UNCERTAINTY/2.0; // noise
  d.SS.Easting  = simState.e + deltaPosition*POSITION_UNCERTAINTY + POSITION_UNCERTAINTY/2.0; // noise
  d.SS.Speed    = simState.vel;
  d.SS.Vel_N    = simState.n_vel;
  d.SS.Vel_E    = simState.e_vel;
  d.SS.Vel_U    = simState.u_vel;
  d.SS.PitchRate= simState.n_acc;     // FOR DISPLAY ONLY. SHOULD BE 0.
  d.SS.RollRate = simState.e_acc;     // FOR DISPLAY ONLY. SHOULD BE 0.
  d.SS.Yaw    = simState.yaw*(1.0 + deltaYaw*YAW_UNCERTAINTY); // noise
  d.SS.Pitch = 0;
  d.SS.Roll = 0;

  // Normalize angle to be between [-pi,pi] and
  d.SS.Yaw = atan2( sin(d.SS.Yaw), cos(d.SS.Yaw) );
  d.SS.YawRate = d.SS.Speed / VEHICLE_WHEELBASE * tan(simState.phi);
  
  // Other updates
  d.SS.Altitude = 0;
  d.SS.Timestamp = TVNow();


  //Updating the new state struct
  d.SS_new.Northing = simState.n + deltaPosition*POSITION_UNCERTAINTY + POSITION_UNCERTAINTY/2.0; // noise
  d.SS_new.Easting  = simState.e + deltaPosition*POSITION_UNCERTAINTY + POSITION_UNCERTAINTY/2.0; // noise
  d.SS_new.Vel_N    = simState.n_vel;
  d.SS_new.Vel_E    = simState.e_vel;
  d.SS_new.Vel_D    = simState.u_vel;
  d.SS_new.Acc_N    = simState.n_acc;
  d.SS_new.Acc_E    = simState.e_acc;

  d.SS_new.PitchRate= 0;      // who uses this stuff anyway?
  d.SS_new.RollRate = 0;      // who uses this stuff anyway?
  d.SS_new.Yaw    = simState.yaw*(1.0 + deltaYaw*YAW_UNCERTAINTY); // noise
  d.SS_new.Pitch = 0;
  d.SS_new.Roll = 0;
  d.SS_new.PitchAcc = 0;
  d.SS_new.RollAcc = 0;
  d.SS_new.YawAcc = 0;

  // Normalize angle to be between [-pi,pi] and
  d.SS_new.Yaw = atan2( sin(d.SS.Yaw), cos(d.SS.Yaw) );
  d.SS_new.YawRate = d.SS.Speed / VEHICLE_WHEELBASE * tan(simState.phi);
  
  // Other updates
  d.SS_new.Altitude = 0;
  DGCgettime(d.SS_new.Timestamp);


  ///////////?DEBUG?///////////////////////////
  /*
  cout << "new struct: "
       << d.SS_new.Northing << ' ' 
       << d.SS_new.Easting << ' '
    // Speed commented out because the new VState struct doesn't have speed.
    //				 << d.SS.Speed << ' '   
       << d.SS_new.Yaw << ' '
       << d.SS_new.Acc_N << ' '
       << d.SS_new.Acc_E << endl;
  cout << "old struct: "
       << d.SS.Northing << ' ' 
       << d.SS.Easting << ' '
    // Speed commented out because the new VState struct doesn't have speed.
    //				 << d.SS.Speed << ' '   
       << d.SS.Yaw << ' '
       << d.SS.PitchRate << ' '
       << d.SS.RollRate << endl;  
  */

  t += deltaT;
} // end SimLoop()
