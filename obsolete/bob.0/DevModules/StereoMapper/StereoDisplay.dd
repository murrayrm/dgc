#include "StereoPlanner.hh"

%%
StereoPlanner v%ver_str     Rate: %rte Hz (%tm ms)      |PHI   | VEL    GOOD |
						        +--------------------+
Vote:           %vo                   |TIMING    |   0 L|%a0   | %b0    %c0  | 
Source:         %source               +----------+   1  |%a1   | %b1    %c1  | 
Pair:           %pair		      |LCK %tlc  |   2  |%a2   | %b2    %c2  | 
Size:           %wd x%ht              |CAP %tcp  |   3  |%a3   | %b3    %c3  | 
Current Frame:  %cur_frame            |RCT %trc  |   4  |%a4   | %b4    %c4  | 
SourceFileName: %sourceFilenamePrefix |DSP %tds  |   5  |%a5   | %b5    %c5  | 
LogFileName:    %logFilenamePrefix    |FLL %tfl  |   6  |%a6   | %b6    %c6  | 
Format:         %format               |EVL %tev  |   7  |%a7   | %b7    %c7  | 
		                      |VTE %tvt  |   8  |%a8   | %b8    %c8  | 
CorrSize:   %corrsize 		      |TTL %ttl  |   9  |%a9   | %b9    %c9  | 
Threshold:  %thresh   		      |DBG %tdb  |   10 |%a10  | %b10   %c10 | 
NDisp:     %ndisp     		      |FTL %tft  |   11 |%a11  | %b11   %c11 | 
OffX:       %offx     			 	     12C|%a12  | %b12   %c12 | 
LR Check:    %lr_check	|LOGGING                 |   13 |%a13  | %b13   %c13 | 
Multi:       %multi   	+-----------+------------+   14 |%a14  | %b14   %c14 | 
x_off:     %xo	      	|Frames: %lf| Rect:   %lr|   15 |%a15  | %b15   %c15 | 
y_off      %yo	      	|State:  %ls| Maps:   %lm|   16 |%a16  | %b16   %c16 | 
width:     %sw	        |Disp:   %ld| Time:   %lt|   17 |%a17  | %b17   %c17 | 
height:    %sh        	|Votes:  %lv| Points: %lp|   18 |%a18  | %b18   %c18 | 
                                                     19 |%a19  | %b19   %c19 | 
[LOG: %LAL| %LNNE| %LBSE| %LQDBG      | %LADBG   ]   20 |%a20  | %b20   %c20 | 
				                     21 |%a21  | %b21   %c21 | 
[DISPLAY: %DAL| %DREC| %DDS]			     22 |%a22  | %b22   %c22 | 
						     23 |%a23  | %b23   %c23 | 
[%QUIT| %PAUS | %REST]                               24R|%a24  | %b24   %c24 | 
%%

tblname: vddtable;
bufname: vddbuf;

string: %ver_str version_str "%s" -ro;

short: %vo optVote "%1d";

string: %source new_source "%s" -callback=change;
string: %pair cam_pair "%s" -ro;
string: %logFilenamePrefix logFilenamePrefix "%s";
string: %sourceFilenamePrefix sourceFilenamePrefix "%s";
string: %format new_format "%s";

short:  %wd source_width "%4d" -ro;
short:  %ht source_height "%4d" -ro;
short:  %cur_frame current_frame "%4d" -ro;

short:	%ndisp ndisp "%3d" -callback=change;
short:  %corrsize corrsize "%2d" -callback=change;
short:  %thresh thresh "%2d" -callback=change;
short:  %offx offx "%2d" -callback=change;
short:  %lr_check lr "%1d" -callback=change;
short:  %multi multi "%1d" -callback=change;

short:  %xo subwindow_x_off "%3d" -ro;
short:  %yo subwindow_y_off "%3d" -callback=change;
short:  %sw subwindow_width "%3d" -ro;
short:  %sh subwindow_height "%3d" -callback=change;

double: %rte rate "%3.1f" -ro;
double: %tm time_ms "%3.0f" -ro;

double: %tlc time_lock "%3.2f" -ro;
double: %tcp time_cap "%3.2f" -ro;
double: %trc time_rect "%3.2f" -ro;
double: %tds time_disp "%3.2f" -ro;
double: %tfl time_fill "%3.2f" -ro;
double: %tev time_eval "%3.2f" -ro;
double: %tvt time_vote "%3.2f" -ro;
double: %ttl time_total "%3.2f" -ro;
double: %tdb time_debug "%3.2f" -ro;
double: %tft time_full "%3.2f" -ro;

short: %lf logFrames "%1d";
short: %lr logRect "%1d";
short: %ld logDisp "%1d";
short: %lm logMaps "%1d";
short: %ls logState "%1d";
short: %lt logTime "%1d";
short: %lv logVotes "%1d";
short: %lp logPoints "%1d";

button: %DAL "ALL" display_all;
button: %DREC "RECT" display_rect;
button: %DDS "DISP" display_disp;

button: %LAL "ALL" log_all;
button: %LBSE "BASE" log_base;
button: %LQDBG "QUICK DEBUG" log_quickdebug;
button: %LADBG "ALL DEBUG" log_alldebug;
button: %LNNE "NONE" log_none;

button: %QUIT "QUIT" user_quit;
button: %PAUS "PAUSE" user_pause;
button: %REST "RESET" user_reset;

double: %a0	PHI[0]				"%2.2f" -ro;
double: %b0	stereo.Votes[0].Velo		"%2.2f" -ro;
double:	  %c0	stereo.Votes[0].Goodness	"%3.1f" -ro;

double: %a1	PHI[1]				"%2.2f" -ro;
double: %b1	stereo.Votes[1].Velo		"%2.2f" -ro;
double:	  %c1	stereo.Votes[1].Goodness	"%3.1f" -ro;

double: %a2	PHI[2]				"%2.2f" -ro;
double: %b2	stereo.Votes[2].Velo		"%2.2f" -ro;
double:	  %c2	stereo.Votes[2].Goodness	"%3.1f" -ro;

double: %a3	PHI[3]				"%2.2f" -ro;
double: %b3	stereo.Votes[3].Velo		"%2.2f" -ro;
double:	  %c3	stereo.Votes[3].Goodness	"%3.1f" -ro;

double: %a4	PHI[4]				"%2.2f" -ro;
double: %b4	stereo.Votes[4].Velo		"%2.2f" -ro;
double:	  %c4	stereo.Votes[4].Goodness	"%3.1f" -ro;

double: %a5	PHI[5]				"%2.2f" -ro;
double: %b5	stereo.Votes[5].Velo		"%2.2f" -ro;
double:	  %c5	stereo.Votes[5].Goodness	"%3.1f" -ro;

double: %a6	PHI[6]				"%2.2f" -ro;
double: %b6	stereo.Votes[6].Velo		"%2.2f" -ro;
double:	  %c6	stereo.Votes[6].Goodness	"%3.1f" -ro;

double: %a7	PHI[7]				"%2.2f" -ro;
double: %b7	stereo.Votes[7].Velo		"%2.2f" -ro;
double:	  %c7	stereo.Votes[7].Goodness	"%3.1f" -ro;

double: %a8	PHI[8]				"%2.2f" -ro;
double: %b8	stereo.Votes[8].Velo		"%2.2f" -ro;
double:	  %c8	stereo.Votes[8].Goodness	"%3.1f" -ro;

double: %a9	PHI[9]				"%2.2f" -ro;
double: %b9	stereo.Votes[9].Velo		"%2.2f" -ro;
double:	  %c9	stereo.Votes[9].Goodness	"%3.1f" -ro;

double: %a10	PHI[10]				"%2.2f" -ro;
double: %b10	stereo.Votes[10].Velo		"%2.2f" -ro;
double:	  %c10	stereo.Votes[10].Goodness	"%3.1f" -ro;

double: %a11	PHI[11]				"%2.2f" -ro;
double: %b11	stereo.Votes[11].Velo		"%2.2f" -ro;
double:	  %c11	stereo.Votes[11].Goodness	"%3.1f" -ro;

double: %a12	PHI[12]				"%2.2f" -ro;
double: %b12	stereo.Votes[12].Velo		"%2.2f" -ro;
double:	  %c12	stereo.Votes[12].Goodness	"%3.1f" -ro;

double: %a13	PHI[13]				"%2.2f" -ro;
double: %b13	stereo.Votes[13].Velo		"%2.2f" -ro;
double:	  %c13	stereo.Votes[13].Goodness	"%3.1f" -ro;

double: %a14	PHI[14]				"%2.2f" -ro;
double: %b14	stereo.Votes[14].Velo		"%2.2f" -ro;
double:	  %c14	stereo.Votes[14].Goodness	"%3.1f" -ro;

double: %a15	PHI[15]				"%2.2f" -ro;
double: %b15	stereo.Votes[15].Velo		"%2.2f" -ro;
double:	  %c15	stereo.Votes[15].Goodness	"%3.1f" -ro;

double: %a16	PHI[16]				"%2.2f" -ro;
double: %b16	stereo.Votes[16].Velo		"%2.2f" -ro;
double:	  %c16	stereo.Votes[16].Goodness	"%3.1f" -ro;

double: %a17	PHI[17]				"%2.2f" -ro;
double: %b17	stereo.Votes[17].Velo		"%2.2f" -ro;
double:	  %c17	stereo.Votes[17].Goodness	"%3.1f" -ro;

double: %a18	PHI[18]				"%2.2f" -ro;
double: %b18	stereo.Votes[18].Velo		"%2.2f" -ro;
double:	  %c18	stereo.Votes[18].Goodness	"%3.1f" -ro;

double: %a19	PHI[19]				"%2.2f" -ro;
double: %b19	stereo.Votes[19].Velo		"%2.2f" -ro;
double:	  %c19	stereo.Votes[19].Goodness	"%3.1f" -ro;

double: %a20	PHI[20]				"%2.2f" -ro;
double: %b20	stereo.Votes[20].Velo		"%2.2f" -ro;
double:	  %c20	stereo.Votes[20].Goodness	"%3.1f" -ro;

double: %a21	PHI[21]				"%2.2f" -ro;
double: %b21	stereo.Votes[21].Velo		"%2.2f" -ro;
double:	  %c21	stereo.Votes[21].Goodness	"%3.1f" -ro;

double: %a22	PHI[22]				"%2.2f" -ro;
double: %b22	stereo.Votes[22].Velo		"%2.2f" -ro;
double:	  %c22	stereo.Votes[22].Goodness	"%3.1f" -ro;

double: %a23	PHI[23]				"%2.2f" -ro;
double: %b23	stereo.Votes[23].Velo		"%2.2f" -ro;
double:	  %c23	stereo.Votes[23].Goodness	"%3.1f" -ro;

double: %a24	PHI[24]				"%2.2f" -ro;
double: %b24	stereo.Votes[24].Velo		"%2.2f" -ro;
double:	  %c24	stereo.Votes[24].Goodness	"%3.1f" -ro;

