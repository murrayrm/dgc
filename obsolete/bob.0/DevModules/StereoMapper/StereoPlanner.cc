#include "StereoPlanner.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include <unistd.h>
#include <iostream>

int QUIT=0;
int PAUSE=0;

double time_lock;
double time_cap;
double time_rect;
double time_disp;
double time_fill;
double time_eval;
double time_vote;
double time_total;
double time_debug;
double time_full;

extern int optSparrow, optVote, optPair;

StereoPlanner::StereoPlanner() 
  : DGC_MODULE(MODULES::StereoPlanner, "Stereo Planner", 0) {

}

StereoPlanner::~StereoPlanner() {
}


void StereoPlanner::Init() {
  DGC_MODULE::Init();

  cout << "Module Init Finished" << endl;

  if(optSparrow) {
    RunMethodInNewThread<StereoPlanner>( this, &StereoPlanner::SparrowDisplayLoop);
    RunMethodInNewThread<StereoPlanner>( this, &StereoPlanner::UpdateSparrowVariablesLoop);
  }
}

void StereoPlanner::Active() {
  RunMethodInNewThread<StereoPlanner>(this, &StereoPlanner::StereoCaptureThread);

  cout << "Starting Active Funcion" << endl;
  while( ContinueInState() && !QUIT) {

    //UpdateState();

    if(optSparrow) {
      UpdateSparrowVariablesLoop();
    }

    // Update the votes ... 
    StereoUpdateVotes();
    // and send them to the arbiter
    Mail m = NewOutMessage(MyAddress(), MODULES::Arbiter, ArbiterMessages::Update);
    // Send the arbiter our "ID" and the vote struct.  The weights are taken care of
    // by the arbiter.
    int myID = -1;  

    if(optPair == SHORT_RANGE || optPair == SHORT_COLOR || optPair == HOMER) {
      myID = ArbiterInput::Stereo;
    } else if(optPair == LONG_RANGE) {
      myID = ArbiterInput::StereoLR;
    }

    m << myID << d.stereo;
    if(StereoNotPointedAtSun(d.SS.Yaw)) {
      if(optVote && !PAUSE) {
	SendMail(m);
      }
    }
  }

  cout << "Finished Active State" << endl;

}

bool StereoNotPointedAtSun(double Yaw) {
  tm *current_time_struct;
  time_t current_time_val;
  int current_mil_time;

  current_time_val = time(NULL);
  current_time_struct = localtime(&current_time_val);
  current_mil_time = (*current_time_struct).tm_hour*100 + (*current_time_struct).tm_min;

  if(current_mil_time < LATE_MIL_TIME_LIMIT && current_mil_time > EARLY_MIL_TIME_LIMIT) {
    if(Yaw < UPPER_YAW_LIMIT && Yaw > LOWER_YAW_LIMIT) {
      cout << "NO VOTES SENT since the time is " << current_mil_time << " and our yaw is " << Yaw << endl;
      cout << "We reject votes between " << EARLY_MIL_TIME_LIMIT << " and " << LATE_MIL_TIME_LIMIT << endl;
      cout << "And yaws between " << LOWER_YAW_LIMIT << " and " << UPPER_YAW_LIMIT << endl;
      return false;
    }
  }

  return true;
}


void StereoPlanner::Shutdown() {
  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;
  StereoShutdown();
  exit(0);
}



