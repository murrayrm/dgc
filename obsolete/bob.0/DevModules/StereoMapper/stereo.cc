#include <sys/time.h>
#include <iomanip>
#include <algorithm>  // for min
#include <stdio.h>

#include "StereoPlanner.hh"
#include "stereo.hh"

//svMap Default Parameters
#define DEFAULT_SVMAP_NUM_ROWS 250
#define DEFAULT_SVMAP_NUM_COLS 250
#define DEFAULT_SVMAP_ROW_RES  0.2
#define DEFAULT_SVMAP_COL_RES  0.2

#define LONG_DEFAULT_SVMAP_NUM_ROWS 250
#define LONG_DEFAULT_SVMAP_NUM_COLS 250
#define LONG_DEFAULT_SVMAP_ROW_RES  0.4
#define LONG_DEFAULT_SVMAP_COL_RES  0.4

#define X_LIMIT DEFAULT_SVMAP_NUM_ROWS*DEFAULT_SVMAP_ROW_RES*0.5
#define Y_LIMIT DEFAULT_SVMAP_NUM_COLS*DEFAULT_SVMAP_COL_RES*0.5
#define Z_LIMIT 1000

#define LONG_X_LIMIT LONG_DEFAULT_SVMAP_NUM_ROWS*DEFAULT_SVMAP_ROW_RES*0.5
#define LONG_Y_LIMIT LONG_DEFAULT_SVMAP_NUM_COLS*DEFAULT_SVMAP_COL_RES*0.5
#define LONG_Z_LIMIT 1000

#define CALIB_PATH_PREFIX "../../calibration/"

#define CALIB_SVSCAL_NAME    "SVSCal.ini"
#define CALIB_SVSPARAMS_NAME "SVSParams.ini"
#define CALIB_CAMCAL_NAME    "CamCal.ini"
#define CALIB_CAMID_NAME     "CamID.ini"
#define CALIB_GENMAP_NAME    "GenMapParams.ini"

#define CALIB_SHORT_RANGE_SUFFIX ".short"
#define CALIB_LONG_RANGE_SUFFIX  ".long"
#define CALIB_SHORT_COLOR_SUFFIX ".short_color"
#define CALIB_HOMER_SUFFIX       ".homer"

#define OVERPASS_HEIGHT -3.3

#define LR_MIN_DISTANCE 20.0

#define which_pair SHORT_RANGE

using namespace std;

extern int QUIT;
extern int PAUSE;

extern double time_lock;
extern double time_cap;
extern double time_rect;
extern double time_disp;
extern double time_fill;
extern double time_eval;
extern double time_vote;
extern double time_total;
extern double time_debug;
extern double time_full;

extern char debugFilenamePrefix[200];
extern char logFilenamePrefix[200];
extern char sourceFilenamePrefix[200];
extern char optFormat[10];
extern int logFrames, logRect, logDisp, logState, logMaps, logTime, logVotes;
extern int optDisplay, optPair, optUseCameras, optSim, optFile, optSparrow, optMaxFrames, optMaxLoops;

double time_ms = 0;
double rate = 0;

int pairIndex;

int StereoPlanner::InitStereo() {
  //Initialize our calibration file stuff to defined defaults
  sprintf(d.calibFileObject[SHORT_RANGE].SVSCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSCAL_NAME, CALIB_SHORT_RANGE_SUFFIX);
  sprintf(d.calibFileObject[SHORT_RANGE].SVSParamFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSPARAMS_NAME, CALIB_SHORT_RANGE_SUFFIX);
  sprintf(d.calibFileObject[SHORT_RANGE].CamCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMCAL_NAME, CALIB_SHORT_RANGE_SUFFIX);
  sprintf(d.calibFileObject[SHORT_RANGE].CamIDFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMID_NAME, CALIB_SHORT_RANGE_SUFFIX);
  sprintf(d.calibFileObject[SHORT_RANGE].GenMapFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_GENMAP_NAME, CALIB_SHORT_RANGE_SUFFIX);

  sprintf(d.calibFileObject[LONG_RANGE].SVSCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSCAL_NAME, CALIB_LONG_RANGE_SUFFIX);
  sprintf(d.calibFileObject[LONG_RANGE].SVSParamFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSPARAMS_NAME, CALIB_LONG_RANGE_SUFFIX);
  sprintf(d.calibFileObject[LONG_RANGE].CamCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMCAL_NAME, CALIB_LONG_RANGE_SUFFIX);
  sprintf(d.calibFileObject[LONG_RANGE].CamIDFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMID_NAME, CALIB_LONG_RANGE_SUFFIX);
  sprintf(d.calibFileObject[LONG_RANGE].GenMapFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_GENMAP_NAME, CALIB_LONG_RANGE_SUFFIX);

  sprintf(d.calibFileObject[SHORT_COLOR].SVSCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSCAL_NAME, CALIB_SHORT_COLOR_SUFFIX);
  sprintf(d.calibFileObject[SHORT_COLOR].SVSParamFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSPARAMS_NAME, CALIB_SHORT_COLOR_SUFFIX);
  sprintf(d.calibFileObject[SHORT_COLOR].CamCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMCAL_NAME, CALIB_SHORT_COLOR_SUFFIX);
  sprintf(d.calibFileObject[SHORT_COLOR].CamIDFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMID_NAME, CALIB_SHORT_COLOR_SUFFIX);
  sprintf(d.calibFileObject[SHORT_COLOR].GenMapFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_GENMAP_NAME, CALIB_SHORT_COLOR_SUFFIX);

  sprintf(d.calibFileObject[HOMER].SVSCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSCAL_NAME, CALIB_HOMER_SUFFIX);
  sprintf(d.calibFileObject[HOMER].SVSParamFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSPARAMS_NAME, CALIB_HOMER_SUFFIX);
  sprintf(d.calibFileObject[HOMER].CamCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMCAL_NAME, CALIB_HOMER_SUFFIX);
  sprintf(d.calibFileObject[HOMER].CamIDFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMID_NAME, CALIB_HOMER_SUFFIX);
  sprintf(d.calibFileObject[HOMER].GenMapFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_GENMAP_NAME, CALIB_HOMER_SUFFIX);

  if(optUseCameras) {
    d.sourceObject.init(0, d.calibFileObject[optPair].CamIDFilename, logFilenamePrefix, optFormat);
  }

  d.processObject.init(0,
		       d.calibFileObject[optPair].SVSCalFilename,
		       d.calibFileObject[optPair].SVSParamFilename,
		       d.calibFileObject[optPair].CamCalFilename,
		       logFilenamePrefix,
		       optFormat);

  if(optSim || optFile) {
    //d.sourceObject.stop();
    d.sourceObject.init(0, d.processObject.imageSize.width, d.processObject.imageSize.height, sourceFilenamePrefix, optFormat, 1);
  }

  //Initialize the map
  if(optPair == SHORT_RANGE || optPair==SHORT_COLOR || optPair == HOMER) {
    d.svMap.initMap(DEFAULT_SVMAP_NUM_ROWS, DEFAULT_SVMAP_NUM_COLS, DEFAULT_SVMAP_ROW_RES, DEFAULT_SVMAP_COL_RES, FALSE); 
  } else if(optPair == LONG_RANGE) {
    d.svMap.initMap(LONG_DEFAULT_SVMAP_NUM_ROWS, LONG_DEFAULT_SVMAP_NUM_COLS, LONG_DEFAULT_SVMAP_ROW_RES, LONG_DEFAULT_SVMAP_COL_RES, FALSE); 
  }
  d.svMap.initPathEvaluationCriteria(false,false,false, false,false, true, true);
  //d.svMap.initPathEvaluationCriteria(false,false,false, true,false, true, false, false);
  d.svMap.initThresholds(70, 1, TIRE_OFF_THRESHOLD, VECTOR_PROD_THRESHOLD, GROW_VEHICLE_LENGTH, 1.2);

// FIXME: THIS is needed to work with the new genMap...
//   Be sure to create a new version of StereoPlanner and indicate that
//   it is using the new velocity controls.

  if(optPair == SHORT_RANGE || optPair == HOMER || optPair == SHORT_COLOR) {
    d.svMap.initVelocityThresholds(SR_MAX_SPEED,SR_MIN_SPEED,
				   SR_DIST_TO_OBSTACLE_ON_ARC_HALT,
				   SR_DIST_TO_OBSTACLE_ANYWHERE_HALT,
				   SR_DIST_TO_OBSTACLE_ON_ARC_MAX_SPEED,
				   SR_DIST_TO_OBSTACLE_ANYWHERE_MAX_SPEED);
  } else if(optPair == LONG_RANGE) {
    d.svMap.initVelocityThresholds(LR_MAX_SPEED,LR_MIN_SPEED,
				   LR_DIST_TO_OBSTACLE_ON_ARC_HALT,
				   LR_DIST_TO_OBSTACLE_ANYWHERE_HALT,
				   LR_DIST_TO_OBSTACLE_ON_ARC_MAX_SPEED,
				   LR_DIST_TO_OBSTACLE_ANYWHERE_MAX_SPEED);
  }    


  if(optDisplay) {
    d.processObject.show();
  }

  return TRUE;
}

void StereoPlanner::StereoUpdateVotes() {
  if(!PAUSE) {
    if(optMaxLoops == 0) {
      if(optSparrow) {
	user_quit(0);
	sleep(1);
      }
      QUIT = 1;
      return;
    }

    if(optMaxLoops>0) optMaxLoops--;

    Timeval startFull, endFull, resultFull;
    startFull = TVNow();
    
    double steerAngle;
    double xUpLimit, yUpLimit, zUpLimit;
    double xLowLimit, yLowLimit, zLowLimit;
    XYZcoord UTMPoint, Point;
    Timeval start, end, result;
    Timeval startTotal, endTotal, resultTotal;
    
    vector< pair<double, double> > voteList;
    vector<double> steeringAngleList;
    steeringAngleList.reserve(NUMARCS);
    voteList.reserve(NUMARCS);
    
    start = TVNow();
    startTotal = TVNow();
    {
      Lock mylock(d.CameraLock);

      if(optFile) {
	if(d.sourceObject.grab() == stereoSource_OK) {
	  FILE *stateLogFile = NULL;
	  char stateLogFilename[256];
	  sprintf(stateLogFilename, "%s.state", sourceFilenamePrefix);
	  stateLogFile = fopen(stateLogFilename, "r");
	  if(stateLogFile != NULL) {
	    int lineNum=-1;
	    double doubleTime;
	    long int sec, usec;
	    while(lineNum != d.sourceObject.pairIndex()-1) {
	      fscanf(stateLogFile, "%d: %lf %lf %lf %f %f %f %f %f %lf %f %f %f %f %f %f\n",
		     &lineNum,
		     &doubleTime,
		     &d.tempState.Easting, &d.tempState.Northing, &d.tempState.Altitude, 
		     &d.tempState.Vel_E, &d.tempState.Vel_N, &d.tempState.Vel_U, 
		     &d.tempState.Speed, &d.tempState.Accel, 
		     &d.tempState.Pitch, &d.tempState.Roll, &d.tempState.Yaw, 
		     &d.tempState.PitchRate, &d.tempState.RollRate, &d.tempState.YawRate );
	    }
	    sec = (int)doubleTime;
	    usec = (int)((doubleTime - floor(doubleTime)) * 1000000);
	    Timeval timevalTime(sec, usec);
	    d.tempState.Timestamp = timevalTime;
	    
	    fclose(stateLogFile);
	  }
	} else {
	  if(optSparrow) {
	    user_quit(0);
	    sleep(1);
	  }
	  QUIT = 1;
	  return;
	}
      }
      
      end = TVNow();
      result = end-start;
      time_lock = result.dbl()*1000;
      start = TVNow();
      if(stereoProcess_OK != d.processObject.loadPair(d.sourceObject.pair(), d.tempState)) {
	//printf("Error while grabbing images from cameras at line %d in %s!  Quitting...\n", __LINE__, __FILE__);
	return;
      }
      pairIndex = d.sourceObject.pairIndex();
    }
    
    d.SS = d.processObject.currentState();
    end = TVNow();
    result=end-start;
    time_cap = result.dbl()*1000;
    //printf("C %.3lf, ", result.dbl()*1000);
    start = TVNow();
    
    d.processObject.calcRect();
    end = TVNow();
    result=end-start;
    time_rect = result.dbl()*1000;
    //printf("R %.3lf, ", result.dbl()*1000);
    start = TVNow();
    
    
    d.processObject.calcDisp();
    end = TVNow();
    result=end-start;
    time_disp = result.dbl()*1000;
    //printf("D %.3lf, ", result.dbl()*1000);
    start = TVNow();
    
    d.svMap.updateFrame(d.SS, TRUE, d.SS.Timestamp.dbl());
    
    if(which_pair == SHORT_RANGE) {
      xUpLimit = X_LIMIT + d.SS.Northing;
      yUpLimit = Y_LIMIT + d.SS.Easting;
      zUpLimit = Z_LIMIT;
      xLowLimit = d.SS.Northing - X_LIMIT;
      yLowLimit = d.SS.Easting -Y_LIMIT;
      zLowLimit = -Z_LIMIT;
    } else if(which_pair == LONG_RANGE) {
      xUpLimit = LONG_X_LIMIT + d.SS.Northing;
      yUpLimit = LONG_Y_LIMIT + d.SS.Easting;
      zUpLimit = LONG_Z_LIMIT;
      xLowLimit = d.SS.Northing - LONG_X_LIMIT;
      yLowLimit = d.SS.Easting -LONG_Y_LIMIT;
      zLowLimit = -LONG_Z_LIMIT;
    }
    
    XYZcoord RelativePoint;
    
    for(int y=d.processObject.subWindow.y; y<d.processObject.subWindow.height; y++) {
      for(int x=d.processObject.subWindow.x; x<d.processObject.subWindow.width; x++) {
	if(d.processObject.SinglePoint(x, y, &UTMPoint)) {
	  if(which_pair == LONG_RANGE) {
	    d.processObject.SingleRelativePoint(x, y, &RelativePoint);
	    if(RelativePoint.X > LR_MIN_DISTANCE) {
	      d.svMap.setCellListDataUTM(UTMPoint.X, UTMPoint.Y, UTMPoint.Z);
	    }
	  } else if(which_pair == SHORT_RANGE) {
	    d.svMap.setCellListDataUTM(UTMPoint.X, UTMPoint.Y, UTMPoint.Z);
	  }
	}
      }
    }
    end = TVNow();
    result=end-start;
    time_fill = result.dbl()*1000;
    //printf("F %.3lf, ", result.dbl()*1000);
    start = TVNow();
    
    d.svMap.evaluateCellsSV(d.processObject.numMinPoints, OVERPASS_HEIGHT);
    
    end = TVNow();
    result=end-start;
    time_eval = result.dbl()*1000;
    //printf("E %.3lf, ", result.dbl()*1000);
    start = TVNow();
    
    //Get the votes
    for(int i = 0; i < NUMARCS; i++) {
      steerAngle = GetPhi(i);
      steeringAngleList.push_back(steerAngle);
    }
    
    voteList = d.svMap.generateArbiterVotes(NUMARCS, steeringAngleList);
    
    for(int i=0; i < NUMARCS; i++) {
      d.stereo.Votes[i].Goodness = voteList[i].second;
      d.stereo.Votes[i].Velo = voteList[i].first;
    }
    
    end = TVNow();
    result=end-start;
    time_vote = result.dbl()*1000;
    //printf("V %.3lf, ", result.dbl()*1000);
    start = TVNow();
    
    endTotal = TVNow();
    resultTotal = endTotal-startTotal;  
    time_total = resultTotal.dbl()*1000;
    //printf("T %.3lf, ", resultTotal.dbl()*1000);
    
    if(logRect) d.processObject.saveRect(logFilenamePrefix, optFormat, pairIndex);
    if(logDisp) d.processObject.saveDisp(logFilenamePrefix, optFormat, pairIndex);
    if(logMaps) {
      char logMapsFilename[256];
      sprintf(logMapsFilename, "%s%.4d", logFilenamePrefix, pairIndex);
      d.svMap.saveMATFile(logMapsFilename, "StereoVision genMap data", "SVmap");
    }
    
    if(logVotes) {
      FILE *voteLogFile = NULL;
      char voteLogFilename[256];
      sprintf(voteLogFilename, "%s.votes", logFilenamePrefix);
      
      voteLogFile = fopen(voteLogFilename, "a");
      if(voteLogFile) {
	fprintf(voteLogFile, "%.4d: ", pairIndex);
	for(int i=0; i < NUMARCS; i++) {
	  fprintf(voteLogFile, "%lf ", d.stereo.Votes[i].Goodness);
	}
	fprintf(voteLogFile, "\n");
	fclose(voteLogFile);
      }
    }

    time_ms = resultTotal.dbl()*1000;
    rate = 1/resultTotal.dbl();
    
    end = TVNow();
    result = end-start;
    time_debug = result.dbl()*1000;
    
    endFull = TVNow();
    resultFull = endFull-startFull;
    time_full = resultFull.dbl()*1000;
    //printf("FT %.3lf\n", resultFull.dbl()*1000);

    if(logTime) {
      FILE *timeLogFile = NULL;
      char timeLogFilename[256];
      sprintf(timeLogFilename, "%s.time", logFilenamePrefix);
      
      timeLogFile = fopen(timeLogFilename, "a");
      if(timeLogFile) {
	fprintf(timeLogFile, "%.4d: %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
		pairIndex,
		time_lock,
		time_cap,
		time_rect,
		time_disp,
		time_fill,
		time_eval,
		time_vote,
		time_total,
		time_debug,
		time_full);
	fclose(timeLogFile);
      }
    }
  }
}


void StereoPlanner::StereoShutdown() {
  
  d.sourceObject.stop();
}

void StereoPlanner::StereoCaptureThread() {
  while(true && !QUIT) {
    if(!PAUSE && (optUseCameras || optSim)) {
      if(optMaxFrames == 0) {
	if(optSparrow) {
	  user_quit(0);
	  sleep(1);
	}
	QUIT = 1;
	return;
      }
      
      if(optMaxFrames>0) optMaxFrames--;
      
      {
	Lock mylock(d.CameraLock);
	if(optUseCameras) {
	  d.sourceObject.grab();
	  UpdateState();
	} else if(optSim) {
	  usleep(33000);
	  if(d.sourceObject.grab() == stereoSource_OK) {
	    FILE *stateLogFile = NULL;
	    char stateLogFilename[256];
	    sprintf(stateLogFilename, "%s.state", sourceFilenamePrefix);
	    stateLogFile = fopen(stateLogFilename, "r");
	    if(stateLogFile != NULL) {
	      int lineNum=-1;
	      double doubleTime;
	      long int sec, usec;
	      while(lineNum != d.sourceObject.pairIndex()-1) {
		fscanf(stateLogFile, "%d: %lf %lf %lf %f %f %f %f %f %lf %f %f %f %f %f %f\n",
		       &lineNum,
		       &doubleTime,
		       &d.tempState.Easting, &d.tempState.Northing, &d.tempState.Altitude, 
		       &d.tempState.Vel_E, &d.tempState.Vel_N, &d.tempState.Vel_U, 
		       &d.tempState.Speed, &d.tempState.Accel, 
		       &d.tempState.Pitch, &d.tempState.Roll, &d.tempState.Yaw, 
		       &d.tempState.PitchRate, &d.tempState.RollRate, &d.tempState.YawRate );
	      }
	      sec = (int)doubleTime;
	      usec = (int)((doubleTime - floor(doubleTime)) * 1000000);
	      Timeval timevalTime(sec, usec);
	      d.tempState.Timestamp = timevalTime;
	      
	      fclose(stateLogFile);
	    }
	  } else {
	    if(optSparrow) {
	      user_quit(0);
	      sleep(1);
	    }
	    QUIT = 1;
	    return;
	  }
	}
      }

	if(logFrames) d.sourceObject.save(logFilenamePrefix, optFormat, d.sourceObject.pairIndex());
	if(logState) {
	  FILE *stateLogFile = NULL;
	  char stateLogFilename[256];
	  sprintf(stateLogFilename, "%s.state", logFilenamePrefix);

	  stateLogFile = fopen(stateLogFilename, "a");
	  if(stateLogFile) {
	    fprintf(stateLogFile, "%.4d: %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",
		    d.sourceObject.pairIndex(),
		    (double)(d.tempState.Timestamp.sec()+d.tempState.Timestamp.usec()/1000000.0),
		    d.tempState.Easting, d.tempState.Northing, d.tempState.Altitude, 
		    d.tempState.Vel_E, d.tempState.Vel_N, d.tempState.Vel_U, 
		    d.tempState.Speed, d.tempState.Accel, 
		    d.tempState.Pitch, d.tempState.Roll, d.tempState.Yaw, 
		    d.tempState.PitchRate, d.tempState.RollRate, d.tempState.YawRate );
	    fclose(stateLogFile);
	  }
	}
      usleep(100);
    }
  }
}
