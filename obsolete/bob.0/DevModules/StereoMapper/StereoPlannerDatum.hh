#ifndef STEREOPLANNERDATUM_HH
#define STEREOPLANNERDATUM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "Arbiter2/Vote.hh"
#include "Arbiter2/Voter.hh"
#include "Arbiter2/ArbiterDatum.hh"
//#include "../../RaceModules/Arbiter/SimpleArbiter.hh"
//#include "../../RaceModules/Arbiter/ArbiterModule.hh"

#include <Constants.h> //for vehicle constants, ie serial ports, etc.

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"
#include "vehlib/VDrive.hh"

#include <stereovision/stereoSource.hh>
#include <stereovision/stereoProcess.hh>
#include <frames/frames.hh>
#include <genMap.hh>

using namespace std;

enum {
  NO_CAMERAS,
  SHORT_RANGE,
  LONG_RANGE,
  SHORT_COLOR,
  HOMER,

  NUM_CAMERA_TYPES
};

typedef struct{
  char SVSCalFilename[256];
  char SVSParamFilename[256];
  char CamCalFilename[256];
  char CamIDFilename[256];
  char GenMapFilename[256];
} calibFileStruct;

// All data that needs to be shared by common threads
struct StereoPlannerDatum {

  Voter stereo; // voter struct for the Arbiter

  // State struct from VState
  boost::recursive_mutex StateStructLock;
  VState_GetStateMsg SS;
  VState_GetStateMsg tempState;

  boost::recursive_mutex CameraLock;
  bool *stopCapture;
  stereoSource sourceObject;
  stereoProcess processObject;

  genMap svMap;

  calibFileStruct calibFileObject[NUM_CAMERA_TYPES];
};


#endif
