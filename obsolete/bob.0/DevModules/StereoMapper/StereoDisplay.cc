#include "StereoPlanner.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

double PHI[NUMARCS];
/*
extern int SIM;
extern int SEND_VOTES;
extern unsigned long SLEEP_TIME;
extern int LOG_SCANS;
extern int HALT_VEHICLE;
extern int SAVE_GENMAP;
extern int CLEAR_GENMAP;
*/
extern int QUIT;
extern int PAUSE;
int RESET=0;

extern double time_lock;
extern double time_cap;
extern double time_rect;
extern double time_disp;
extern double time_fill;
extern double time_eval;
extern double time_vote;
extern double time_total;
extern double time_debug;
extern double time_full;

char version_str[9]="1.0.0";

char source[15]="Cameras";
char cam_pair[15]="Short";
char new_source[15]="Cameras";
char new_format[10];

extern char optFormat[10];
extern char logFilenamePrefix[200];
extern char sourceFilenamePrefix[200];

int source_width;
int source_height;

int corrsize;
int thresh;
int ndisp;
int offx;
int lr;
int multi;

int subwindow_x_off;
int subwindow_y_off;
int subwindow_width;
int subwindow_height;

extern double time_ms;
extern double rate;

extern int logFrames, logRect, logDisp, logState, logMaps, logTime, logVotes, logPoints;
extern int optVote, optPair;

Voter stereo;

int CHANGED=0;
int DISPLAY_DISP=0;
int DISPLAY_RECT=0;

int current_frame;

int UpdateCount = 0;
int TimeSec = 0;
int TimeUSec = 0;

double state_pitch,state_yaw,state_roll;
double state_e,state_n,state_a;
double state_speed;

#include "vddtable.h"

void StereoPlanner::UpdateSparrowVariablesLoop() {
  for(int i = 0; i < NUMARCS; i++) PHI[i] = GetPhi(i);

  stereo = d.stereo;

  TimeSec = d.SS.Timestamp.sec();
  TimeUSec = d.SS.Timestamp.usec();

  if(CHANGED==1) {
    d.processObject.setSVSParams(corrsize, thresh, ndisp, offx, lr, multi, subwindow_x_off, subwindow_y_off, subwindow_width, subwindow_height);
    if(strcmp(new_source, "cam")==0 || strcmp(new_source, "c")==0 ||
       strcmp(new_source, "Cam")==0 || strcmp(new_source, "C")==0) {
      sprintf(source, "Cameras");
    } else if(strcmp(new_source, "sim")==0 || strcmp(new_source, "s")==0) {
      sprintf(source, "File");
    }

    if(optPair == SHORT_RANGE) {
      sprintf(cam_pair, "Short Range");
    } else if(optPair == LONG_RANGE) {
      sprintf(cam_pair, "Long Range");
    } else if(optPair == HOMER) {
      sprintf(cam_pair, "Homer");
    } else if(optPair == SHORT_COLOR) {
      sprintf(cam_pair, "Short Color");
    }

    if(strcmp(new_format, "bmp")==0 || strcmp(new_format, "b")) {
      sprintf(optFormat, "bmp");
    } else if(strcmp(new_format, "jpg")==0 || strcmp(new_format, "j")) {
      sprintf(optFormat, "jpg");
    }
    CHANGED=0;
  }

  sprintf(new_source, "%s", source);
  sprintf(new_format, "%s", optFormat);

  if(DISPLAY_DISP==1) {
    d.processObject.showDisp();
    DISPLAY_DISP = 0;
  }

  if(DISPLAY_RECT == 1) {
    d.processObject.showRect();
    DISPLAY_RECT = 0;
  }

  if(RESET == 1) {
    d.sourceObject.resetPairIndex();
    d.processObject.resetPairIndex();
    RESET = 0;
  }

  source_width = d.sourceObject.width();
  source_height = d.sourceObject.height();

  corrsize = d.processObject.corrsize();
  thresh = d.processObject.thresh();
  ndisp = d.processObject.ndisp();
  offx = d.processObject.offx();
  lr = d.processObject.lr();
  multi = d.processObject.multi();

  subwindow_x_off = d.processObject.subWindow.x;
  subwindow_y_off = d.processObject.subWindow.y;
  subwindow_width = d.processObject.subWindow.width;
  subwindow_height = d.processObject.subWindow.height;

  state_pitch = (d.SS.Pitch / (2 * M_PI)) * 360.0;
  state_yaw = (d.SS.Yaw / (2 * M_PI)) * 360.0;
  state_roll = (d.SS.Roll / (2 * M_PI)) * 360.0;
  state_n = d.SS.Northing;
  state_e = d.SS.Easting;
  state_a = d.SS.Altitude;
  state_speed = d.SS.Speed;

  current_frame = d.sourceObject.pairIndex();

  UpdateCount ++;
}

void StereoPlanner::SparrowDisplayLoop() {

  dbg_all = 0;

  if (dd_open() < 0) exit(1);
  dd_bindkey('Q', user_quit);
  dd_bindkey('r', user_reset);
  dd_bindkey('R', user_reset);
  dd_bindkey('p', user_pause);
  dd_bindkey('P', user_pause);
  dd_bindkey('l', log_base);
  dd_bindkey('L', log_base);
  dd_bindkey('d', display_all);
  dd_bindkey('D', display_all);

  dd_usetbl(vddtable);

  cout << "Sparrow display should show up in 1 sec" << endl;
  sleep(1); // Wait a bit, because other threads will print some stuff out
  dd_loop();
  dd_close();
  PAUSE = 1;
  sleep(1);
  QUIT = 1;
}


int user_quit(long arg) {
  PAUSE = 1;
  sleep(1);
  return DD_EXIT_LOOP;
}


int user_pause(long arg) {
  if(PAUSE) {
    PAUSE = 0;
  } else {
    PAUSE = 1;
  }
}


int user_reset(long arg) {
  RESET = 1;
}

int change(long arg) {
  CHANGED = 1;
}


int display_all(long arg) {
  display_rect(arg);
  display_disp(arg);
}


int display_disp(long arg) {
  DISPLAY_DISP = 1;
}


int display_rect(long arg) {
  DISPLAY_RECT = 1;
}


int log_all(long arg) {
  if(!logFrames || 
     !logRect || 
     !logDisp || 
     !logState ||
     !logMaps ||
     !logTime ||
     !logVotes ||
     !logPoints) {
    logFrames = 1;
    logRect = 1;
    logDisp = 1;
    logState = 1;
    logMaps = 1;
    logTime = 1;
    logVotes = 1;
    logPoints = 1;
  } else {
    logFrames = 0;
    logRect = 0;
    logDisp = 0;
    logState = 0;
    logMaps = 0;
    logTime = 0;
    logVotes = 0;
    logPoints = 0;
  }
}


int log_base(long arg) {
  if(!logFrames || 
     !logState) {
    logFrames = 1;
    logState = 1;
  } else {
    logFrames = 0;
    logState = 0;
  }
}


int log_quickdebug(long arg) {
  if(!logDisp || 
     !logVotes) {
    logDisp = 1;
    logVotes = 1;
  } else {
    logDisp = 0;
    logVotes = 0;
  }
}


int log_alldebug(long arg) {
  if(!logRect || 
     !logDisp || 
     !logMaps ||
     !logTime ||
     !logVotes ||
     !logPoints) {
    logRect = 1;
    logDisp = 1;
    logMaps = 1;
    logTime = 1;
    logVotes = 1;
    logPoints = 1;
  } else {
    logRect = 0;
    logDisp = 0;
    logMaps = 0;
    logTime = 0;
    logVotes = 0;
    logPoints = 0;
  }
}


int log_none(long arg) {
  logFrames = 0;
  logRect = 0;
  logDisp = 0;
  logState = 0;
  logMaps = 0;
  logTime = 0;
  logVotes = 0;
  logPoints = 0;
}

