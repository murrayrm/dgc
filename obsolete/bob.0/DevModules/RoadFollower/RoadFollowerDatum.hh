#ifndef ROADFOLLOWERDATUM_HH
#define ROADFOLLOWERDATUM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <netinet/in.h>

#include "Arbiter2/Vote.hh"
#include "Arbiter2/Voter.hh"
#include "Arbiter2/ArbiterDatum.hh"
//#include "RaceModules/Arbiter/Vote.hh"
//#include "RaceModules/Arbiter/SimpleArbiter.hh"
//#include "RaceModules/Arbiter/ArbiterModule.hh"

#include "Constants.h" //for vehicle constants, ie serial ports, etc.

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"
#include "vehlib/VDrive.hh"


#include <genMap.hh>


using namespace std;

// All data that needs to be shared by common threads
struct RoadFollowerDatum {

  Voter roadfollower; // voter struct for the Arbiter

  // State struct from VState
  boost::recursive_mutex StateStructLock;
  VState_GetStateMsg SS;

  //Socket communication variables
  int cont,create_socket,new_socket;
  socklen_t addrlen;
  int bufsize;
  char *buffer;
  struct sockaddr_in address;

};


#endif
