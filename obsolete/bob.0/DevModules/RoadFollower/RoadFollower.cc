#include "RoadFollower.hh"

#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include <iostream>

#define SOCKET_ERROR -1
#define RECORD_DATA 1
#define NO_RECORD 0

extern int SIM;
extern int DISPLAY;
extern int PRINT_VOTES;

int counter = 0;

RoadFollower::RoadFollower() 
  : DGC_MODULE(MODULES::RoadFollower, "RoadFollower Planner", 0) {

}

RoadFollower::~RoadFollower() {
}


void RoadFollower::Init() {
  // call the parent init first
  DGC_MODULE::Init();


  cout << "Module Init Finished" << endl;
}

void RoadFollower::Active() {
  int i;
  double steerangle;
  double currentSteeringAngle = 123.4567;
  //  int modeFlag = NO_RECORD;
   int modeFlag = RECORD_DATA;

  cout << "Starting Active Funcion" << endl;
  while( ContinueInState() ) {

    // update the state so we know where the vehicle is at
    UpdateState();


    //insert stuff to get votes here!


    // Update the votes ... 
    for(int i = 0; i < NUMARCS; i++) {
      steerangle = GetPhi(i);
      d.roadfollower.Votes[i].Goodness = MAX_GOODNESS/2;
      d.roadfollower.Votes[i].Velo = 0;
    }


    // and send them to the arbiter
    Mail m = NewOutMessage(MyAddress(), MODULES::Arbiter, ArbiterMessages::Update);
    // Send the arbiter our "ID" and the vote struct.  The weights are taken care of
    // by the arbiter.
    int myID = ArbiterInput::RoadFollower;
    m << myID << d.roadfollower;
    SendMail(m);

    // and sleep for a bit
    ///usleep(100000);
  }

  cout << "Finished Active State" << endl;

}


void RoadFollower::Shutdown() {

  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;

  //Insert code to shut down RoadFollower here

}



