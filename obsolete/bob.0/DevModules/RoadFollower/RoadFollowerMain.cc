#include "RoadFollower.hh"

using namespace std;

int main(int argc, char **argv) {

  cout << "argc = " << argc << endl;
   
  ////////////////////////////
  // do some argument checking
  if( argc > 1 ) { // if we have arguments
  
    for( int i=1; i < argc; i++ ) { 
      // go through each of the arguments and check...
      cout << "argv[ " << i << "] = " << argv[i] << endl;

      }
    }
  
  // Start the MTA By registering a module
  // and then starting the kernel

  Register(shared_ptr<DGC_MODULE>(new RoadFollower));
  StartKernel();

  return 0;
}
