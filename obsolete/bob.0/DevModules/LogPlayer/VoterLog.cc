/**
 * $Id$
 */
#include "VoterLog.hh"
#include "Arbiter2/Vote.hh" // NUMARCS
#include "Arbiter2/IO.hh" // readVoterLog
#include <fstream>  // ifstream
#include <iostream> // cout
#include <string>
using namespace std;

#define DEBUG true // enable for debug printouts

VoterLog::VoterLog(const char* name, const char* logfilename) 
{
  voterName = name;
  voterData = IO::readVoterLog(logfilename);
}

Voter VoterLog::getVoter()
{
  Voter result;
  // so we don't return something undefined
  if(voterData.size() > 0)
  {
    result = voterData.front();
    voterData.pop_front();
  }
  else 
  {
    cout << "VoterLog::getVoter(): returning empty Voter" << endl;
  }
  return result;
}

void VoterLog::printVoterData()
{
  cout << endl << "--------- printing voterData ---------" << endl;
  for(unsigned int i = 0; i < voterData.size(); i++)
  {
    cout << "voting sequence number " << i+1 << ":" << endl;
    cout << "goodness:     speed:" << endl
         << "=========     ======" << endl;
    for(int j = 0; j < NUMARCS; j++)
    {
      cout << "  " << voterData.at(i).Votes[j].Goodness
           << "       " << voterData.at(i).Votes[j].Velo << endl;
    }
    cout << endl;
  }
  cout << "-------------- finished ---------" << endl;
}
