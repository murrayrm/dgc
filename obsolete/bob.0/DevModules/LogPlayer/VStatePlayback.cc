/**
 * $Id$
 */
#include "VStatePlayback.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp" //RunMethodInNewThread
#include "Arbiter2/IO.hh"         // readArbiterLog
using namespace std;

#define DEBUG false // enable to get more debug printf

/** A global reference to find out if the user has quitted through the sparrow
 * user interface */
extern bool SPARROW_QUIT;

/** A global reference to how many states has been delivered */
extern int stateCount;

/** A global reference to how many times the arbiter has asked for state */
extern int arbiterStateRequestCount;

/** A global variable from the sparrow display that keeps track of if the user
 * has paused playback or not */
extern bool isPaused;

/** If we're going to display the sparrow display or not */
extern bool dispSparrow;

VStatePlayback::VStatePlayback(const char* arbiterlog_filename, 
                               VoterPlayback* voterPlay)
    : DGC_MODULE(MODULES::VState, "VState Log Playback", 0)
{
  // (we're faking the VState module)
  
  voterPlayback = voterPlay;
  arbiterLog = IO::readArbiterLog(arbiterlog_filename);
}

void VStatePlayback::Init()
{
  DGC_MODULE::Init();
  cout << ModuleName() << ":Init Finished" << endl;
  
  if(dispSparrow) {
    RunMethodInNewThread<VStatePlayback>( this, &VStatePlayback::SparrowDisplayLoop);
    RunMethodInNewThread<VStatePlayback>( this, &VStatePlayback::UpdateSparrowVariablesLoop);
  }
}

void VStatePlayback::Active()
{
  while ( ContinueInState() && !SPARROW_QUIT)
  {
    // Doing nothing since the arbiter is made to ask for the 
    // state so we cannot just deliver it like that.
    sleep(1); 
  }
}

Mail VStatePlayback::QueryMailHandler(Mail& msg)
{
  //Timeval tv;
  //tv = TVNow();
  
  Mail reply; // allocate space for the reply
  
  // figure out what we are responding to
  switch(msg.MsgType())
  {
  case VStateMessages::GetState:  // we shall deliver a fresh state
    arbiterStateRequestCount++; // statistics

    // If we're paused, we just do nothing (delivering an empty reply)
    if(!isPaused) 
    {
      if(DEBUG) printf("\nVStatePlayback::QueryMailHandler: Got VStateMessages::GetState request\n");
      reply = ReplyToQuery(msg);
      updateState();
      
      // Would be nice if we could call the sendVotes method without blocking 
      // somehow. But right now I don't know how...
      //RunMethodInNewThread<VoterPlayback>( voterPlayback, &VoterPlayback::sendVotes());
      voterPlayback->sendVotes();
      
      stateCount++;
      reply << currState;
      //printState(currState); // keep commented to avoid spamming
    }        
    break;
  default:
    // let the parent mail handler handle it.
    reply = DGC_MODULE::QueryMailHandler(msg);
    break;
  }
  //cout << " QueryMailHandler took " << (TVNow() - tv).usec() << " us" << endl;
  return reply;
}

void VStatePlayback::updateState() 
{
  // First check that we have lines left in the log:
  if(arbiterLog.at(0).size() <= 0)
  {
    printf("VStatePlayback: Have no more states to deliver, log is empty. \n");
    printf("VStatePlayback: Shutting down...\n");
    //voterPlayback->Shutdown();
    Shutdown();
  }
  
  // Read the timestamp:
  double t = arbiterLog.at(0).front();
  int sec = (int)t; // just drops the decimals
  int usec = (int)(t - (double)sec)*1000000;

  // Read position and put all data into currState
  currState.Timestamp = Timeval(sec, usec);
  currState.Easting   = arbiterLog.at(1).front();
  currState.Northing  = arbiterLog.at(2).front();
  currState.Altitude  = arbiterLog.at(3).front();
  currState.Vel_E     = arbiterLog.at(4).front();
  currState.Vel_N     = arbiterLog.at(5).front();
  currState.Vel_U     = arbiterLog.at(6).front();
  currState.Speed     = arbiterLog.at(7).front();
  currState.Accel     = arbiterLog.at(8).front();
  currState.Pitch     = arbiterLog.at(9).front();
  currState.Roll      = arbiterLog.at(10).front();
  currState.Yaw       = arbiterLog.at(11).front();
  currState.PitchRate = arbiterLog.at(12).front();
  currState.RollRate  = arbiterLog.at(13).front();
  currState.YawRate   = arbiterLog.at(14).front();
  
  // Remove all the front elements:
  for(vector<deque<double> >::iterator i = arbiterLog.begin();
      i != arbiterLog.end(); i++)
  {
    (*i).pop_front();
  }      
}

void VStatePlayback::printState(VState_GetStateMsg &state)
{
  cout << "_________________________________________" << endl
  << "Easting  : "
  << setw(12) << setiosflags(ios::fixed) << setprecision(1)
    << state.Easting  << " m" << endl
    << "Northing : "
    << setw(12) << setiosflags(ios::fixed) << setprecision(1)
      << state.Northing << " m" << endl
      << "Speed    : " << state.Speed    << " m/s" << endl
      << "Vel.north: " << state.Vel_N   << " m/s" << endl
      << "Vel.east : " << state.Vel_E   << " m/s" << endl
      << "Yaw angle: " << state.Yaw << " rad" << endl
      << "Timestamp: " << state.Timestamp << " sec" << endl;
  cout << "_________________________________________" << endl;
}

void VStatePlayback::Shutdown()
{
  printf("VoterPlayback: Shutting down...\n");
  exit(0);  
}
