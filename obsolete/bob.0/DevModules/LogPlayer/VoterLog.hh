#ifndef VOTERLOG_HH
#define VOTERLOG_HH

#include "Arbiter2/Voter.hh"
#include <deque>
using namespace std;

/** The VoterLog class simulates the voting behaviour of a real voter.
 * It reads a bunch of votes from a log file and then every time 
 * getVoter is called, one Voter set is returned (with contains NUMARCS of 
 * goodness and velocity values). 
 * $Id$
 */
class VoterLog {
public:
  /** Constructor.
   * @param name the name of this simulated voter 
   */
  VoterLog(const char* name, const char* logfilename);
  
  /** Returns a Voter from the voterData deque.
   * If voterData is empty, an empty Voter is returned 
   * @return A voter from the deque
   */
  Voter getVoter();
  
  /** Returns true if there are more voterSets in the voterData deque 
   * @return true if we have more votersSets, otherwise false
   */
  bool hasMoreVoterSets() { return voterData.size() > 0; }

  /** just prints the voterData to stdout, for debugging purposes */
  void printVoterData();

  /** Returns how many voters we have left in the log
   * @return number of voters left
   */
  int getSize() {return voterData.size(); }

  /** Returns the name of this voter 
   * @return the name
   */
  const char* getName() { return voterName; }
  
private:
  /** the name of this voter (i.e. GlobalPlanner, LADARMapper etc) */
  const char* voterName;

  /** A vector where each element contains of a 
   * vector containing one vote-set. In the vote-set
   * each pair contains goodness as first element
   * and velocity as second element. */
  deque<Voter> voterData;
};

#endif
