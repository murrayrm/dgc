#include "VoterSim.hh"

/*! To launch VoterSim you must give an argument which points at
 * a file that contains a list of the voter logfiles. If you don't have
 * such a file, do like this:
 * <ul>
 *   <li>Put your logfiles into a directory which has no other files
 *   <li>Do: ls >> loglist.txt
 *   <li>Move the logfiles and loglist.txt to the dir where
 *       your VoterSim executable is.
 *   <li>Launch VoterSim in that dir with this command: ./VoterSim loglist.txt
 * </ul>
 */
int main(int argc, char *argv[])
{
  //-----------------------------------------------------------------------//
  // Do some argument handling
  if(argc !=2)
  {
    cout << endl << "Usage: VoterSim loglist_filename" << endl << endl
    << "The loglist_filename argument is required!" << endl
    << "It must be the filename of a text file that contains a listing" << endl
    << "of the logfiles you want to use!" << endl
    << "If you don't have such a file, do like this:" << endl
    << " - Put your logfiles into a directory which has no other files" << endl
    << " - In that dir, do: ls >> loglist.txt" << endl
    << " - Move the logfiles and loglist.txt to the dir where" << endl
    << "   your VoterSim executable is." << endl    
    << " - Launch VoterSim with this command: ./VoterSim loglist.txt" << endl;
    exit(0);
  }

  Register(shared_ptr<DGC_MODULE>(new VoterSim(argv[1]))); // argv[1] is the argument
  StartKernel();

  return 0;
}
