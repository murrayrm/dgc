#ifndef VOTERSIMMODULE_H
#define VOTERSIMMODULE_H
#include "MTA/Kernel.hh"    // The MTA Module Kernel
#include "VoterLogHandler.hh"
#include "Arbiter2/ArbiterDatum.hh"  // ArbiterInput
using namespace std;

/** Used to not completely crap out MTA with sending many votes in a very short
 * period of time. Time is in microseconds */
const long SLEEP_BETWEEN_VOTES = 500;

/** VoterPlayback is a playback app of all the voters. It uses logfiles created
 * by the arbiter with all vote data from different voters. It parses this
 * data and sends Voter messages over MTA to the Arbiter just as the real 
 * devices do. All Voters are sent at the same time, which might be bad viewed 
 * from a MTA perspective. A small delay has been added between each to make
 * it nicer for MTA.
 * $Id$
 */
class VoterPlayback : public DGC_MODULE {
public:
  /**
   * Constructor.
   * @param loglist_filename the filename of the logfile list textfile
   */
  VoterPlayback(char* loglist_filename);
  
  /** Destructor. Deletes the created VoterLogHandler object */
  ~VoterPlayback();
  
  /** Init (MTA method) */
  void Init();
  
  /** Contains the main loop. (MTA method) */
  void Active();

  /** Shutdown method, called from VStatePlayback when it's state log is
   * empty, or if our voter logs become empty (MTA method) */
  void Shutdown();

  /** Takes a voter set from each voter in the voters vector and sends 
   * a Mail to the Arbiter, just as all voters did that */
  void sendVotes();
  
  /** How many sets of votes that has been sent of each voter */
  int votesSent[ArbiterInput::Count];
  
private:

  /** the VoterPlayback object */
  VoterLogHandler* voterLogHandler;
};

#endif
