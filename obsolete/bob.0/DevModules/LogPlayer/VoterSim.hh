#ifndef VOTERSIM_H
#define VOTERSIM_H
#include "VoterLog.hh"
#include <vector>
#include <list>
using namespace std;

/** VoterSim is a simulation of all the voters. It uses logfiles created
 * by the arbiter with all vote data from different voters. It parses this
 * data and sends Voter messages when getVoterList() is called.
 * To use VoterSim you must give an argument which points at
 * a file that contains a list of the voter logfiles. If you don't have
 * such a file, do like this:
 * 
 * - Put your logfiles into a directory which has no other files
 * - Do: ls > loglist.txt (watch out here, some linux distributions
 *   put in extra control characters here. So you might want to check
 *   your textfile in an editor first before using it.
 * - Move the logfiles and loglist.txt to the dir where
 *   your executable is.
 * $Id$
 */
class VoterSim {
public:

  VoterSim(char* loglist_filename);
  
  /** Gets a voter list. Alternative approach to get votes if you don't
   * want to send them through MTA 
   * @return the list of voters
   */
  list<Voter> getVoterList();

  /** Checks if there's more lists 
   * @return true if we have more voter lists left
   */
  bool hasMoreVoterLists() { return voterLogs.at(0).getSize() > 0; }

  /** Gets the number of voter lists that are left 
   * @return the number of voter lists
   */
  int getNbrLists() { return voterLogs.at(0).getSize(); }

private:
  /** The internal vector of all voterLogs */
  vector<VoterLog> voterLogs;

  /** A vector that contains the log filenames */
  vector<string> logFilenames;
  
  /** Parses the list of logfiles filenames.
   * Exits this program if something goes wrong (like invalid filename, 
   * wrong filenames etc). 
   * @param the filename
   */
  void parseLogFilenames(char* loglist_filename);
};

#endif
