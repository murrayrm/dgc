/**
 * $Id$
 */
#include "VoterPlayback.hh"
#include "VoterPlayback.hh"
#include <iostream>
#include <list>

#define DEBUG false // if we're gonna have debug printouts or not
using namespace std;

VoterPlayback::VoterPlayback(char* loglist_filename) : 
  DGC_MODULE(MODULES::VoterSim, "Voter Playback", 0)
{
  // create the VoterPlayback object
  voterLogHandler = new VoterLogHandler(loglist_filename);
  // initialize the statistics array:
  for(int i = 0; i < ArbiterInput::Count; i++)
  {
    votesSent[i] = 0;
  }
}

VoterPlayback::~VoterPlayback()
{
  // clean up the VoterPlayback object
  delete voterLogHandler;
}

void VoterPlayback::Init()
{
  // call the parent init first
  DGC_MODULE::Init();
  cout << ModuleName() << ":Init Finished" << endl;
}


void VoterPlayback::Active()
{
  // Now all log files are read and we can start
  printf("VoterPlayback starting Active()\n");
  
  while(ContinueInState() && voterLogHandler->hasMoreVoterLists() )
  {
    // We're only listening for sendVotes calls from VStatePlayback
    sleep(1); 
  }
  printf("\nVoterPlayback finished (no more votes to get from the logfiles)\n");
}

void VoterPlayback::sendVotes()
{
  // first check if we have more lists
  if(!voterLogHandler->hasMoreVoterLists())
  {
    printf("VoterPlayback: Have no more votes to deliver, log is empty.\n");
    Shutdown();
  }
  
  Timeval tv = TVNow();
  
  // pick a list of Voters from the VoterPlayback (which picks from VoterLog)
  list<Voter> voterList = voterLogHandler->getVoterList();
  
  for(int i = 0; i < ArbiterInput::Count; i++)
  {
    Mail m = NewOutMessage(MyAddress(), MODULES::Arbiter, ArbiterMessages::Update);
    // The ID sent to the arbiter is the same as i in the for loop.
    // (as the ArbiterInput enum starts counting at 0
    Voter v = voterList.front();
    voterList.pop_front();
    votesSent[i]++; // lets save some statistics to show in the sparrow display
    m << i << v;
    SendMail(m);
    usleep(SLEEP_BETWEEN_VOTES); // sleep a little while to give MTA a chance
  }
  
  if(DEBUG) printf("VoterPlayback: Sending all votes took %ld us (of which %ld was sleeping)\n", 
                   (TVNow() - tv).usec(), SLEEP_BETWEEN_VOTES * ArbiterInput::Count);
}

void VoterPlayback::Shutdown()
{
  printf("VoterPlayback: Shutting down...\n");
  exit(0);  
}
