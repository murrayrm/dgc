#include "VoterPlayback.hh"
#include "VStatePlayback.hh"
#include <getopt.h>

// Defines for the command line option flags 
#define ARB_LOG      (1<<0)
#define VOTER_LOG    (1<<1)
#define NO_DISP      (1<<2)
#define HELP         (1<<3)

bool dispSparrow = true; // if we're going to use sparrow display or not
  
/** 
 * LogPlayer is meant to playback votes and state.
 * Every time the Arbiter asks for state a logged state is delivered
 * (LogPlayer's VStatePlayback module pretends to be VState and listens to 
 * the VStateMessages::GetState message).
 * Together with the state, a set of votes from each voter log is also sent.
 *
 * IMPORTANT: Make sure NOT to run the Arbiter (or any other module) on the same
 *            computer as LogPlayer, since that will totally crap up the MTA.
 * IMPORTANT: Also, make sure you're running this module ON a race computer!
 *            Running it on another network for example, seems to totally lag
 *            the networking of MTA.
 * $Id$
 */
int main(int argc, char *argv[])
{
  // Argument handling
  int c;
  char* arbiterlog = "testlogs/arbiter.log";
  char* voterloglist = "testloglist";
  
  while (1)
  {
    /* getopt_long stores the option index here. */
    int option_index = 0;
    static struct option long_options[] =
      { // name         argument?          flag  value
        {"arbiterlog",  required_argument, NULL, ARB_LOG},
        {"voterloglist",    required_argument, NULL, VOTER_LOG},
        {"nodisp",      no_argument      , NULL, NO_DISP},
        {"help",        no_argument      , NULL, HELP},
        {0, 0, 0, 0}
      };

    char short_options[] = "a:v:d";
    c = getopt_long_only (argc, argv, short_options, long_options, &option_index);
    /* Detect the end of the options. */
    if(c == -1)
      break;

    switch(c)
    {
    case 'a':
    case ARB_LOG:
      arbiterlog = strdup(optarg);
      break;
    
    case 'v':
    case VOTER_LOG:
      voterloglist = strdup(optarg);
      break;
    
    case 'd':
    case NO_DISP:
      dispSparrow = false;
      break;
        
    case 'h':
    case HELP:
      cout << endl 
      << "Usage: ./LogPlayer [OPTION] [FILENAME] " << endl 
      << endl
      << "-a, --arbiterlog filename    An arbiter log" << endl
      << "-v, --voterloglist filename  A textfile which lists the voter files" << endl
      << "-d, --nodisp                 Disable sparrow display" << endl
      << "-h, --help                   Displays this text" << endl
      << endl
      << "Brief description:" << endl
      << "    LogPlayer will deliver votes and state every" << endl
      << "    time the Arbiter asks for state (LogPlayer's VStatePlayback " << endl
      << "    object pretends to be VState and listens to the " << endl
      << "    VStateMessages::GetState message, which activates state delivery " << endl
      << "    and also the votes delivery). " << endl
      << endl
      << "NOTE:" << endl
      << "The text file must contain a listing of the voter logfiles you want to " << endl
      << "use (you may have more filenames in the list but only those with " << endl
      << "\"votes\" in their filename will be taken into account)." << endl
      << "If you don't have such a file, do like this for example:" << endl
      << " - Go to a directory with arbiter logs" << endl
      << " - ls > loglist.txt" << endl
      << " - Move the logfiles and loglist.txt to the dir where" << endl
      << "   your LogPlayer executable is." << endl;
      return 0;
      
    case '?': // invalid options makes us not want to start
      return 1;
    
    default:
       printf ("?? returned character code 0%o ??\n", c);
       return 1;          
    }
  } // end while(1)

  // Done parsing arguments, let's begin...
  
  printf("Starting LogPlayer with arguments:\n");
  printf("  Arbiterlog  : %s\n", arbiterlog);
  printf("  Voterloglist: %s\n", voterloglist);
  printf("  Use sparrow : ");
  if(dispSparrow) printf("yes\n"); else printf("no\n");
  printf("\n");
  printf("NOTE: You should REALLY run this module on a race computer,\n");
  printf("      specially if you're on another switch than them.\n");
  
  VoterPlayback* voterPlayback = new VoterPlayback(voterloglist);
  Register(shared_ptr<DGC_MODULE>(voterPlayback)); 
  Register(shared_ptr<DGC_MODULE>(new VStatePlayback(arbiterlog, voterPlayback)));
  StartKernel();

  delete voterPlayback; // since we did new to create on this object
  return 0;
}
