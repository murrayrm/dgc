/*! 
 * $Id: navdisplay.cc 4559 2004-10-15 23:28:13Z henrik $
 */
#include "VStatePlayback.hh"
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

double timeNow;

int updateCount = 0;              // number of times display has been updated
int arbiterStateRequestCount = 0; // number of times the arbiter has asked for state
int stateCount = 0;               // number of states we've sent

int LadarVotesCount = 0;
int GlobalVotesCount = 0;
int StereoVotesCount = 0;
int StereoLRVotesCount = 0;
int DFEVotesCount = 0;
int PathEvaluationVotesCount = 0;
int CorrArcEvalVotesCount = 0;
int FenderPlannerVotesCount = 0;
int RoadFollowerVotesCount = 0;

// changeable attributes:
bool isPaused = false;  // if we've paused playback or not. Start paused

bool SPARROW_QUIT; // variable to check if the user has requested to quit

// must be included after the declarations above:
#include "vddtable.h"

void VStatePlayback::UpdateSparrowVariablesLoop() 
{
  while( !ShuttingDown() ) {
    timeNow = currState.Timestamp.dbl();
       
    LadarVotesCount          = voterPlayback->votesSent[ArbiterInput::LADAR];
    StereoVotesCount         = voterPlayback->votesSent[ArbiterInput::Stereo];
    StereoLRVotesCount       = voterPlayback->votesSent[ArbiterInput::StereoLR];
    GlobalVotesCount         = voterPlayback->votesSent[ArbiterInput::Global];
    DFEVotesCount            = voterPlayback->votesSent[ArbiterInput::DFE];
    CorrArcEvalVotesCount    = voterPlayback->votesSent[ArbiterInput::CorrArcEval];
    FenderPlannerVotesCount  = voterPlayback->votesSent[ArbiterInput::FenderPlanner];
    RoadFollowerVotesCount   = voterPlayback->votesSent[ArbiterInput::RoadFollower];
    PathEvaluationVotesCount = voterPlayback->votesSent[ArbiterInput::PathEvaluation];

    updateCount++;
   // dd_setcolor(PLAYPAUSE, BLACK, isPaused ? RED : GREEN);
    usleep(200000); // sleep 200ms so other threads can work too
  }
}

void VStatePlayback::SparrowDisplayLoop() 
{
  dbg_all = 0;
  SPARROW_QUIT = false;
  if (dd_open() < 0) exit(1);
  dd_bindkey('q', user_quit);
  dd_bindkey('Q', user_quit);
  dd_usetbl(vddtable);
  usleep(500000); // Wait a bit, because other threads will print some stuff out
  dd_loop();
  dd_close();
}

/* Callbacks */
int toggle_playpause(long arg) {
  isPaused = !isPaused;
  dd_setcolor(PLAYPAUSE, BLACK, isPaused ? RED : GREEN);
  return 0;
}

int user_quit(long arg)
{
  SPARROW_QUIT = true;
  return DD_EXIT_LOOP;
}
