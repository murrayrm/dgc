#ifndef VSTATESIM_HH
#define VSTATESIM_HH

#include "MTA/Kernel.hh"    // The MTA Module Kernel
#include "vehlib/VState.hh" // The State struct sent via MTA
#include "VoterPlayback.hh"
#include <deque>
#include <vector>
using namespace std;

/** This class fetches the state a from a log file once each time it is 
 * requested and fills in the VState_GetStateMsg struct with the latest 
 * information. VStatePlayback acts as the master of the two modules as it
 * handles the sparrow interface and tells VoterPlayback when to deliver votes.
 * $Id$
 */
class VStatePlayback : public DGC_MODULE {
public:
  /** 
   * Constructor
   * @param arbiterlog_filename   filename of the arbiter logfile
   * @param voterPlayback         a pointer to the VoterPlayback object
   */
  VStatePlayback(const char* arbiterlog_filename, VoterPlayback* voterPlay);

  /** Connects to the PlayerClient and initializes its devices */
  void Init();

  /** This method contains the main loop for the PlayerSendCmd module. */
  void Active();

  /** Shutdown method, called from when state log is empty (MTA method) */
  void Shutdown();
    
  /**
   * MTA messaging method
   * This method should only recieve VDriveMessages from Arbiter about velocity
   * and steering.
   * \param msg the MTA mail message
   */
  Mail QueryMailHandler(Mail & ml);

private:
  /** Pointer to the VoterPlayback, so we can command it to send votes when 
   * the arbiter asks for an update
   */
  VoterPlayback* voterPlayback;
  
  /** The latest updated state */
  VState_GetStateMsg currState;

  /** Reads a state from the top of the log into our state struct */
  void updateState();
  
  vector<deque<double> > arbiterLog;
  
  /** Prints the contents of a VState_GetStateMsg to cout */
  void printState(VState_GetStateMsg &state);
    
  /** Creates a sparrow display */
  void SparrowDisplayLoop();

  /** The update loop for the sparrow display */
  void UpdateSparrowVariablesLoop();
};

#endif
