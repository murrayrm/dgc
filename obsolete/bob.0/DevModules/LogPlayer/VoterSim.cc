#include "VoterSim.hh" 
#include "VoterLog.hh"
#include "../Voter.hh"
#include "../ArbiterDatum.hh"  // ArbiterInput
#include <iostream>

#define DEBUG false // if we're gonna have debug printouts or not
using namespace std;

VoterSim::VoterSim(char* loglist_filename)
{
  parseLogFilenames(loglist_filename);
  for(int i = 0; i < ArbiterInput::Count; i++)
  {
    char votername[64];
    sprintf(votername, ArbiterInputStrings[i]);
    voterLogs.push_back(VoterLog(votername));
    voterLogs.at(i).readLogFile(logFilenames.at(i).c_str());
    
    //Debug: print the voter's first voter sets
    if(DEBUG) cout << "Voter name: " << voterLogs.at(i).getName() << endl;
    if(DEBUG) cout << voterLogs.at(i).getVoter();
  } 
  if(DEBUG) cout << "VoterSim created" << endl;
}


list<Voter> VoterSim::getVoterList() {
  list<Voter> voters(ArbiterInput::Count);

  for(int i = 0; i < ArbiterInput::Count; i++)
  {
    voters.push_back(voterLogs.at(i).getVoter());
  }
  return voters;
}

void VoterSim::parseLogFilenames(char* loglist_filename)
{
  if(DEBUG) cout << "VoterSim::parseLogFilenames: reading file " << loglist_filename << endl;
  ifstream file(loglist_filename);
  if(!file.is_open())
  {
    cout << "VoterSim::parseLogFilenames: Cannot read file " << loglist_filename << endl;
    exit(1);
  }
  
  string line;
  while(getline(file, line))
  {
    if(line.find("votes") < line.size()) // we only want to add voter logfiles
    {
      logFilenames.push_back(line);
      if(DEBUG) cout << "added " << line << endl;
    }
  }
  // do some checks
  if(logFilenames.size() != ArbiterInput::Count) {
    cout << "VoterSim::parseLogFilenames: Invalid number of voter files" 
	 << "(" << logFilenames.size() << ")" << endl
    << " (must match with ArbiterInput::Count and appear in the same" << endl
    << "  order as the Arbiter creates the logs" << endl;
    exit(1);
  }
  if(DEBUG) cout << "finished parsing log filenames" << endl;
}
