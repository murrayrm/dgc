#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"


class GPSCap : public DGC_MODULE {
public:
  GPSCap();

  void Active();

};

GPSCap::GPSCap(): DGC_MODULE( MODULES::Passive, "Passive GPS Capture Program", 0){}

void GPSCap::Active() {

  VState_GetStateMsg mySS;

  int counter = 0;  
  int shutdown_flag = false;
  char myOption;
  ofstream outfile ( "waypoints.gps" );

  outfile << setiosflags(ios::fixed);

  outfile << "# Waypoint file modified to BOB format" << endl;
  outfile << "# Type\tEasting\t\tNorthing\tRadius\tVelocity" << endl;

  while ( shutdown_flag == false ) { 
    
    cout << "Capture (c), Quit (q):" << endl;
    cin >> myOption; // = (char) getc();
    if( myOption == 'c' || myOption == 'C') {
      // Request a message from State
      Mail msg = NewQueryMessage(MyAddress(), MODULES::VState, VStateMessages::GetState);
      Mail reply = SendQuery(msg);
      reply >> mySS;
      outfile << "  0" << setprecision(8) 
	      << "\t"  << mySS.Easting << "\t" << mySS.Northing 
	      << "\t"  << setprecision(2) << 5.0    // Waypoint Radius 
	      << "\t"  << setprecision(3) << 10.0   // Acceptable Velocity
	      << endl;
      // cout default precision is crap!
      // cout << "Grabbed E = " << mySS.Easting << ", N = " << mySS.Northing << endl;
      printf( "Grabbed E = %10.3f, N = %10.3f\n", mySS.Easting, mySS.Northing );
      counter ++;

    } else if( myOption == 'q' || myOption == 'Q') {
      cout << "Exiting" << endl;
      cout << "Grabbed " << counter << " waypoints." << endl;
      shutdown_flag = true;
    } else {
      cout << "Unknown Command" << endl;
    }
  }

  ForceKernelShutdown();
}

int main(int argc, char* argv[])
{
  
  Register( shared_ptr<DGC_MODULE> ( new GPSCap));
  StartKernel();

  return 0;
}














