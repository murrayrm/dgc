#include "PlayerGetState.hh"
#include "PlayerInterfaceDatum.hh"
#include "DGCutils"		// DGCgettime()
#include <fstream> // ifstream
#include <string>
#include <boost/tokenizer.hpp> // parsing waypoints
using namespace std;

extern PI_DATUM d;

#define DEBUG false // set to true for more debug printing

// ##########################################################################
//  PlayerGetState module
// ##########################################################################

PlayerGetState::PlayerGetState()
    : DGC_MODULE(MODULES::VState, "VState Player Simulator", 0)
{
  // using existing VState to identify
}

PlayerGetState::~PlayerGetState()
{
  unsubscribe();
  disconnect();
  delete d.client;
}  

void PlayerGetState::Init()
{
  DGC_MODULE::Init();

  readConfig();
  connect();
  subscribe();
  
  // Parse the first waypoint as an offset
  char rddf[128];
  strcpy(rddf, getenv("HOME"));
  strcat(rddf, "/dgc/RDDF/bob.dat");
  printf("Reading starting RDDF waypoint offset from %s\n", rddf);
  parseWaypointOffset(rddf, easting_offset, northing_offset);

  d.startTime = TVNow();
  // clear the state struct and actuators
  d.cmd.velocity_cmd = 0;
  d.cmd.steer_cmd    = 0;
  d.ServedState    = 0;
  d.CmdMotionCount = 0;
  d.CmdDirectDriveCount = 0;
  d.lastUpdate = TVNow() - d.startTime;

  //d.velCmd = 0;

  cout << ModuleName() << " Init Finished" << endl;

}

void PlayerGetState::Active()
{
  Timeval period(0,PLAYERINTERFACE_PERIOD);
  Timeval t, duration; // for the proper sleep

  while ( ContinueInState() )
  {
    t = TVNow();
    
      
    // Only read state if the position 3d interface is connected
    if(d.pos3d) 
    {
      getState();
      if(DEBUG) printf("getState took %ld us\n", (TVNow() - t).usec() );
      //printState(d.SS); // keep commented to avoid spamming
    }
    
    // Do a proper thread sleep the remaining time of the period
    t = t + period;
    duration = t - TVNow();
    if(duration > Timeval(0,0)) {
      usleep(duration.usec());
      if(DEBUG) printf("Slept %ld us\n", duration.usec());
    }
    else {
      if(DEBUG) printf("No time to sleep! (duration <= 0 since last getState() took %ld)\n", lastGetStateTime);
    }
  }
}

Mail PlayerGetState::QueryMailHandler(Mail& msg)
{
  Mail reply; // allocate space for the reply
  
  // figure out what we are responding to
  switch(msg.MsgType())
  {
  case VStateMessages::GetState:  // we shall deliver a fresh state
    d.ServedState++;
    reply = ReplyToQuery(msg);
    reply << d.SS;
    printf("Sending state (%ld us old)\n", (TVNow() - d.SS.Timestamp).usec() );    
    //printState(d.SS); // keep commented to avoid spamming
    break;
  case VStateMessages::GetVehicleState:  // we shall deliver a fresh state
    d.ServedState++;
    reply = ReplyToQuery(msg);
    reply << d.SS_new;
    printf("Sending state (%ld us old)\n", (TVNow() - d.SS.Timestamp).usec() );    
    //printState(d.SS); // keep commented to avoid spamming
    break;
  default:
    // let the parent mail handler handle it.
    reply = DGC_MODULE::QueryMailHandler(msg);
    break;
  }
  return reply;
}

void PlayerGetState::getState() 
{
  Timeval now;
    
  // Do the actual reading
  if(DEBUG) now = TVNow();
  if(d.client->Read() < 0)
  {
    printf("PlayerClient.Read() failed! Will disconnect and try to connect again\n");
    unsubscribe();
    disconnect();
    connect(); // this method retries until it connects again
    subscribe();
  }
  lastGetStateTime = (TVNow() - now).usec(); // save for debug printout
  if(DEBUG) printf("  Read() took %ld us\n", lastGetStateTime);

  // Easting and Northing must be adjusted since on our vehicle the GPS
  // transmitter is not in the center of the truck. It is approx GPS_DIFF meters 
  // more back than the center
    d.SS.Easting  = d.pos3d->Xpos() + easting_offset - GPS_DIFF * cos(d.pos3d->Yaw());
  d.SS.Northing = d.pos3d->Ypos() + northing_offset - GPS_DIFF * sin(d.pos3d->Yaw());;
  d.SS.Altitude = d.pos3d->Zpos();
  d.SS.Vel_E    = d.pos3d->XSpeed();
  d.SS.Vel_N    = d.pos3d->YSpeed();
  d.SS.Vel_U    = d.pos3d->ZSpeed();
  d.SS.Pitch    = d.pos3d->Pitch(); 
  d.SS.Roll     = d.pos3d->Roll(); 
  d.SS.Yaw      = convertYaw(d.pos3d->Yaw()); // convert to Bob's angles
  // calculate the speed in ground plane, I didn't bother to calculate true 3D speed
  d.SS.Speed = sqrt(d.SS.Vel_E * d.SS.Vel_E + d.SS.Vel_N * d.SS.Vel_N);
  d.SS.Timestamp = d.lastUpdate = TVNow();

  // set new data struct
    d.SS_new.Easting  = d.pos3d->Xpos() + easting_offset - GPS_DIFF * cos(d.pos3d->Yaw());
  d.SS_new.Northing = d.pos3d->Ypos() + northing_offset - GPS_DIFF * sin(d.pos3d->Yaw());;
  d.SS_new.Altitude = d.pos3d->Zpos();
  d.SS_new.Vel_E    = d.pos3d->XSpeed(); d.SS_new.Acc_E = 0;
  d.SS_new.Vel_N    = d.pos3d->YSpeed(); d.SS_new.Acc_N = 0;
  d.SS_new.Vel_D    = -d.pos3d->ZSpeed(); d.SS_new.Acc_D = 0;
  d.SS_new.Pitch    = d.pos3d->Pitch();  
  d.SS_new.PitchRate = 0;   d.SS_new.PitchAcc = 0;
  d.SS_new.Roll     = d.pos3d->Roll(); 
  d.SS_new.RollRate = 0;  d.SS_new.RollAcc = 0;
  d.SS_new.Yaw      = convertYaw(d.pos3d->Yaw()); // convert to Bob's angles
  d.SS_new.YawRate = 0; d.SS_new.YawAcc = 0;
  DGCgettime(d.SS_new.Timestamp);
}

void PlayerGetState::printState(VState_GetStateMsg &state)
{    
  printf("_________________________________________\n");
  printf("Easting  : %8.2lf m\n", state.Easting);
  printf("Northing : %8.2lf m\n", state.Northing);
  printf("Speed    : %8.2f m/s\n", state.Speed);
  printf("Vel.east : %8.2f m/s\n", state.Vel_E);
  printf("Vel.north: %8.2f m/s\n", state.Vel_N);
  printf("Pitch    : %8.2f rad (%4.1f degrees)\n", state.Pitch, RTOD(state.Pitch));
  printf("Roll     : %8.2f rad (%4.1f degrees)\n", state.Roll, RTOD(state.Roll));
  printf("Yaw      : %8.2f rad (%4.1f degrees)\n", state.Yaw, RTOD(state.Yaw));
  printf("Timestamp: %ld sec\n", state.Timestamp.sec());
  //d.pos->Print();   // might be interesting too
  //d.gps->Print();   // might be interesting too
  //d.truth->Print(); // might be interesting too
  printf("_________________________________________");
}

void PlayerGetState::connect()
{
  cout << "PlayerClient.Connect: connecting to " << host
  << ":" << port << endl;
  
  // Create a PlayerClient and connect it
  d.client = new PlayerClient(host.c_str(), port, PLAYER_PROTOCOL); 
    
  // Connect the to the Player server if not already connected
  while(!d.client->Connected())
  {
    if(d.client->Connect(host.c_str(),port) != 0) {
      cout << "PlayerClient.Connect: connect failed, trying again in ";
      for(int i = 5; i >= 0; i--)
      {
        usleep(1000000); // sleep 1 sec
        cout << i << " " << flush;
      }
      cout << endl;
    }
  }
  cout << "PlayerClient.Connect: connect successful!" << endl;
  
  // Set the data mode
  // With this mode we get only data when we ask for it
  cout << "PlayerClient.SetDataMode: setting data mode" << endl;
  if(d.client->SetDataMode(DATA_MODE) != 0)
  {
    cout << "PlayerClient.SetDataMode: failed! " << endl;
    exit(-1);
  }
  cout << "PlayerClient.SetDataMode: successful! " << endl;
  
  // Set the frequency we want Player to deliver (since we use PUSH mode) the
  // data.
  printf("PlayerClient.SetFrequency: setting data delivery frequency to %d Hz\n", 
        PLAYER_DELIVERY_FREQUENCY);
  if(d.client->SetFrequency(PLAYER_DELIVERY_FREQUENCY) != 0)
  {
    cout << "PlayerClient.SetFrequency: failed! " << endl;
    exit(-1);
  }
  cout << "PlayerClient.SetFrequency: successful! " << endl;
  
}


void PlayerGetState::disconnect()
{
  // disconnect the playerclient
  cout << "disconnecting the playerclient" << endl;
  if(d.client->Disconnect() != 0)
    cout << "disconnect failed!" << endl;
  else
    cout << "disconnect successful!" << endl;
}

void PlayerGetState::subscribe()
{
  unsigned char access;
  unsigned char mode;
  // ################# Position ###################################
  // beware that index 0 must be the one in the .world/.cfg file
  d.pos = new PositionProxy(d.client, 0, 'c');
  cout << "Pos: subscribing (read/write)" << endl;
  mode = PLAYER_ALL_MODE;
  if((d.pos->ChangeAccess(mode, &access) < 0) ||
       (access != mode))
  {
    cout << "Pos: error subscribing driver: " << d.pos->driver_name << endl
         << "Pos: access: " << access << endl;
    exit(-1);
  }
  cout << "Pos: subscribed successfully driver: " << d.pos->driver_name << endl;
  //d.pos->Print();

  if(d.pos->SetMotorState(1) < 0) {
    cout << "Failed enabling motors" << endl;
    return;
  }
  cout << "Pos: motors enabled" << endl;

  // ################# Position3D ###################################
  // beware that index 0 must be the one in the .world/.cfg file
  d.pos3d = new Position3DProxy(d.client, 0, 'c');
  cout << "Pos3D: subscribing (read/write)" << endl;
  mode = PLAYER_ALL_MODE;
  if((d.pos3d->ChangeAccess(mode, &access) < 0) ||
       (access != mode))
  {
    cout << "Pos3D: error subscribing driver: " << d.pos3d->driver_name << endl
         << "Pos3D: access: " << access << endl;
    exit(-1);
  }
  cout << "Pos3D: subscribed successfully driver: " << d.pos3d->driver_name << endl;
  //d.pos->Print();
  
  // ################# Truth #########################
  d.truth = new TruthProxy(d.client, 0, 'c');
  cout << "Truth: subscribing (read)" << endl;
  mode = PLAYER_READ_MODE;
  if((d.truth->ChangeAccess(mode,&access) < 0) ||
     (access != mode))
    {
      cout << "Truth: error subscribing driver: " << d.truth->driver_name << endl
           << "Truth: access: " << access << endl;
      return;
    }
  cout << "Truth: subscribed successfully driver: " << d.truth->driver_name << endl;

  // ################## GPS ##########################
  d.gps = new GpsProxy(d.client, 0, 'c');
  cout << "GPS: subscribing (read)" << endl;
  mode = PLAYER_READ_MODE;
  if((d.gps->ChangeAccess(mode, &access) < 0) ||
     (access != mode))
    {
      cout << "GPS: error subscribing driver: " << d.gps->driver_name << endl
           << "GPS: access: " << access << endl;
      return;
    }
  cout << "GPS: subscribed successfully driver: " << d.gps->driver_name << endl;
  //d.gps->Print();
}


void PlayerGetState::unsubscribe()
{
  unsigned char access;
  
  // unsubscribe the position driver
  if(d.pos)
  {
    if((d.pos->ChangeAccess(PLAYER_CLOSE_MODE, &access) < 0) ||
     (access != PLAYER_CLOSE_MODE))
    {
      printf("failed to unsubscribe position\n");
    }
    else
    {
      printf("unsubscribed position\n");
      delete d.pos;
    }
  }

  // unsubscribe the position3d driver
  if(d.pos3d)
  {
    if((d.pos3d->ChangeAccess(PLAYER_CLOSE_MODE,&access) < 0) ||
     (access != PLAYER_CLOSE_MODE))
    {
      printf("failed to unsubscribe position3d\n");
    }
    else
    {
      printf("unsubscribed position3d\n");
      delete d.pos3d;
    }
  }
  // unsubscribe the truth driver
  if(d.truth)
  {
    if((d.truth->ChangeAccess(PLAYER_CLOSE_MODE,&access) < 0) ||
     (access != PLAYER_CLOSE_MODE))
    {
      printf("failed to unsubscribe truth\n");
    }
    else
    {
      printf("unsubscribed truth\n");
      delete d.truth;
    }    
  }
  // unsubscribe the gps
  if(d.gps)
  {
    if((d.gps->ChangeAccess(PLAYER_CLOSE_MODE,&access) < 0) ||
     (access != PLAYER_CLOSE_MODE))
    {
      printf("failed to unsubscribe gps\n");
    }
    else
    {
      printf("unsubscribed gps\n");
      delete d.gps;
    }
  }
}

void PlayerGetState::readConfig()
{
  // set the defaults:
  host = PLAYER_GAZEBO_HOST;
  port = PLAYER_GAZEBO_PORT;

  cout << "PlayerGetState::readConfig: reading file '" << CONFIG_FILENAME 
       << "'" <<endl;
  ifstream file(CONFIG_FILENAME);
  if(!file.is_open())
  {
    cout << "PlayerGetState::readConfig: Cannot read file '" << CONFIG_FILENAME
    << "', using defaults = " << host << ":" << port << endl;
    return;
  }
  // if a file was found, use those settings instead:
  string line;
  getline(file, line);
  host = line;
  getline(file, line);
  port = atoi(line.c_str());

  cout << "PlayerGetState::readConfig: read host:port = "
  << host << ":" << port << endl;
}

void PlayerGetState::parseWaypointOffset(char* filename, double &east_offset, double &north_offset)
{
  printf("parseWaypointOffset: reading file '%s'\n", filename);
  ifstream file(filename);
  if(!file.is_open())
  {
    printf("parseWaypointOffset: Cannot read file '%s'\n", filename);
    exit(1);
  }
  
  char buffer[1024];
  while(file.peek() == '#') // read through all the remaining commented lines
  {
    file.getline(buffer, 1024);
  }

  string line;
  getline(file, line); // first line of log data
  typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
  boost::char_separator<char> sep(" \t"); // spaces or tabs
  
  tokenizer tok(line, sep);
  int col = 0;
  // Parse element 2 and 3 of the first waypoint line
  for(tokenizer::iterator tok_iter = tok.begin(); tok_iter != tok.end();++tok_iter)
  {
    if(col == 1)
      east_offset = atof((*tok_iter).c_str());
    if(col == 2)
      north_offset = atof((*tok_iter).c_str());
    col++;
  }
  printf("parseWaypointOffset: parsed easting_offset: %lf  northing_offset: %lf\n", east_offset, north_offset);
}
