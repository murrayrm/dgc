#include "PlayerSendCmd.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"
#include "PlayerInterfaceDatum.hh"
//#include <limits.h>
using namespace std;

// ##########################################################################
//    Global things
// ##########################################################################

PI_DATUM d; // to be shared by PlayerSendCmd and PlayerGetState

double convertYaw(double angle) {
  // normalize within [-pi, pi]
  angle = atan2(sin(angle), cos(angle));

  if(-M_PI <= angle && angle < -M_PI*0.5)
    return -angle - M_PI*1.5;
  else
    return -angle + M_PI*0.5;
}

// ##########################################################################
//    PlayerSendCmd module
// ##########################################################################

PlayerSendCmd::PlayerSendCmd()
  : DGC_MODULE(MODULES::VDrive, "PlayerInterface VDrive Simulator", 0) {
}


void PlayerSendCmd::Init()
{
  DGC_MODULE::Init();

  d.velCmd = 0;
}

void PlayerSendCmd::Active()
{
  while( ContinueInState() ) 
  {
    // PlayerSendCmd does nothing except sending commands when it gets
    // CmdMotion messages...
    usleep(250000); // 0.25 sec
  }
}

void PlayerSendCmd::InMailHandler(Mail& msg) {
  double turnRate = 0;
  Timeval deltaTimeval;
  double deltaTime; //in seconds
  Timeval currentTime;

  switch (msg.MsgType())
    {
    case VDriveMessages::CmdDirectDrive:
      d.CmdDirectDriveCount++;
      msg >> d.steerAccelCmd;
      
      //Get the current time
      currentTime = TVNow();

      //Get the time difference
      deltaTimeval = currentTime - d.lastCommandTime;
      
      //Convert to a double in sec
      deltaTime = deltaTimeval.dbl();
      
      //Record this time as the last commanded time
      d.lastCommandTime = currentTime;

      //d.steerAccelCmd.accel_cmd = 1;

      //Integrate
      d.velCmd += d.steerAccelCmd.accel_cmd*deltaTime;
      if (d.velCmd < 0) {
	d.velCmd = 0;
      }

      //d.velCmd = 7;
          
      turnRate = calcTurnRateForSteerAccel(d.velCmd, d.steerAccelCmd.steer_cmd);       
      cout << "Setting accel. Acc: " << d.steerAccelCmd.accel_cmd
	    << "m/s^2. Vel = " << d.velCmd
           << " m/s. Desired Yaw angle: " << d.steerAccelCmd.steer_cmd
           << " radians (became turnrate " << turnRate << " rad/sec)" 
	   << endl;

      if(d.pos) 
      {
        d.pos->SetSpeed((double)d.velCmd, turnRate);
      }
      else
      {
        printf("PlayerSendCmd: Not connected to position proxy, CmdMotion aborted.\n");
      }
       break;

    case VDriveMessages::CmdMotion:
      /* Get the motion data and store it locally */
      d.CmdMotionCount++;
      msg >> d.cmd;
	cout << "Got mail: velocity_cmd: " << d.cmd.velocity_cmd << " steer cmd: " << d.cmd.steer_cmd << endl;

      // d.cmd now contains velocity and the steering angle relative to the
      // current angle.

      // calculate turnrate because SetSpeed uses rad/sec for turning
      // (is has a turn rate instead of an absolute angle to aim for)
      turnRate = calcTurnRate(d.cmd.velocity_cmd, d.cmd.steer_cmd);

      cout << "Setting speed. Vel: " << d.cmd.velocity_cmd
           << " m/s. Desired Yaw angle: " << d.cmd.steer_cmd
           << " radians (became turnrate " << turnRate << " rad/sec)" << endl;

      if(d.pos) 
      {
        d.pos->SetSpeed(d.cmd.velocity_cmd, turnRate);
      }
      else
      {
        printf("PlayerSendCmd: Not connected to position proxy, CmdMotion aborted.\n");
      }
      break;

    default:
      // ignore
      break;
  }
}

Mail PlayerSendCmd::QueryMailHandler(Mail& msg) {
  // When we're simulating VDrive, we don't need to do 
  // anything with the Query messages VDrive normally gets.
  Mail reply;
  return reply; // return empty message
}

double PlayerSendCmd::calcTurnRate(double velocity, double steering_angle) {
  // normalize within [-pi, pi]
  steering_angle = atan2(sin(steering_angle), cos(steering_angle));
  // must change the sign since gazebo has opposite reference than bob's
  // planning software has
  return -(velocity/WHEELBASE) * sin(steering_angle);
}


double PlayerSendCmd::calcTurnRateForSteerAccel(double velocity, double steering_angle) {
  // must change the sign since gazebo has opposite reference than bob's
  // planning software has
  cout << "sin(steering_angle) = " << sin(steering_angle) 
       << ", steering_angle  = " << steering_angle 
       << ", vel = " << velocity
       << ", wheelbase = " << WHEELBASE << endl;

  return -(velocity/WHEELBASE) * sin(steering_angle);
}
