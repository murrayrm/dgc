/**
 * PlayerInterface is a link between the Player/Gazebo simulation engine and
 * our planning software. It's supposed to be executed like this:
 * - Gazebo running dgc/gazebo/world/clodbuster2.world
 * - Player running dgc/gazebo/config/gazebo2.cfg
 * - PlayerInterface MTA module
 * - Arbiter MTA module
 * - GlobalPlanner MTA module using the gazebo.rddf file in dgc/bob/RDDF
 * - LADARMapper with the -sim flag (also compiled with _PLAYER_ defined)
 * 
 * PlayerInterface tries by default to connect a Player server at localhost:6665
 * To connect to a remote server, create a file named 'config' in the same
 * directory as the PlayerInterface binary. The file shall contain a hostname
 * or IP address at the first line and a port number at the second line in
 * order for PlayerInterface to read it correctly.
 * <br><br>
 * Compilation:
 * To compile the PlayerInterface module you will need to have
 * Player compiled because PlayerInterface requires the playerc
 * library to be in your LD_LIBRARY_PATH when compiling with the
 * -lplayerc flag.
 *
 * $Id$
 */
#define HELP     (1<<0)
 
#include <getopt.h>
#include "PlayerGetState.hh"
#include "PlayerSendCmd.hh"
using namespace std;

int main(int argc, char *argv[])
{
  // Argument handling
  int c;  
  while (1)
  {
    /* getopt_long stores the option index here. */
    int option_index = 0;
    static struct option long_options[] =
      { // name         argument?          flag  value
        {"help",        no_argument      , NULL, HELP},
        {0, 0, 0, 0}
      };

    char short_options[] = "lh";
    c = getopt_long_only (argc, argv, short_options, long_options, &option_index);
    /* Detect the end of the options. */
    if(c == -1)
      break;

    switch(c)
    {
    case 'h':
    case HELP:
      cout << endl 
      << "Usage: ./PlayerInterface " << endl 
      << endl
      << "There are currently no switches..." << endl;
      return 0;
    case '?': // invalid options makes us not want to start
      return 1;
    
    default:
       printf ("?? returned character code 0%o ??\n", c);
       return 1;          
    }
  } // end while(1)

  // Done parsing arguments, let's begin...
  printf("Starting PlayerInterface\n");
  
  // Start the MTA By registering a module
  // and then starting the kernel
  Register(shared_ptr<DGC_MODULE>(new PlayerGetState() ));
  Register(shared_ptr<DGC_MODULE>(new PlayerSendCmd() ));
  StartKernel();

  return 0;
}
