#ifndef PLAYERGETSTATE_HH
#define PLAYERGETSTATE_HH

#include "MTA/Kernel.hh" // The MTA Module Kernel
#include "vehlib/VState.hh" // The State struct sent via MTA
using namespace std;

// ##########################################################################
//  PlayerGetState module
// ##########################################################################

/** This class fetches the state a specific amount of times each second and
 *  fills in the VState_GetStateMsg struct with the latest information.
 *  It also handles all the connection details using PlayerClient library.
 *  To get the state, a position3d device is used (since we want to know
 *  6 DOF (position: x,y,z, velocity: x,y,z and rotation: pitch, roll, yaw).
 *
 *  As Player with our Tahoe model gives us coordinates relative to the first
 *  waypoint, this class parses the dgc/bob/RDDF/bob.dat file to check the 
 *  coordintates for the first waypoint, then this offset is added to all
 *  Easting/Northing data that is recieved from Player.
 */
class PlayerGetState : public DGC_MODULE {
public:
  /** Constructor */
  PlayerGetState();

  /** Destructor. Unsubscribes the devices and disconnects the Playerclient */
  ~PlayerGetState();

  /** Connects to the PlayerClient and initializes its devices */
  void Init();

  /** This method contains the main loop for the PlayerSendCmd module. */
  void Active();

  /**
   * MTA messaging method
   * This method should only recieve VDriveMessages from Arbiter about velocity
   * and steering. And then reply with the state.
   * @param msg the MTA mail message
   * @return the reply MTA mail message
   */
  Mail QueryMailHandler(Mail & ml);

private:
  /** Reads the state from Player/Gazebo */
  void getState();

  /** Reads the config file with host and port */
  void readConfig();

  /** Tries to connect to the Player server until it succeedes */
  void connect();

  /** Disconnects the player client */
  void disconnect();

  /* Subscribes to the different devices, you must run connect() first */
  void subscribe();

  /* Unsubscribes the different devices, good to do before disconnect() */
  void unsubscribe();
    
  /** Prints the contents of a VState_GetStateMsg to cout 
   * @param state the state to print 
   */
  void printState(VState_GetStateMsg &state);

  /** the host to connect to the Player Server at */
  string host;

  /** the port to connect to the Player Server at */
  int port;
  
  /** Easting and Northing offset. In Gazebo, we use the first waypoint
   * as (0,0) and then translate all the other waypoints relative to that one.
   * Although, when we are going to deliver state, we must deliver the true
   * coordinate, therefore we must save away this offset and add it when 
   * delivering state */
  double easting_offset, northing_offset;
  
  /** parses the first waypoint coordinates out of the given RDDF file
   * @param filename the name of the RDDF file to parse
   * @param east_offset  the easting value will be filled into this double
   * @param north_offset the northing value will be filled into this double
   */
  void parseWaypointOffset(char* filename, double &east_offset, double &north_offset);
  
  /** for debug purposes */
  long lastGetStateTime;
};

#endif
