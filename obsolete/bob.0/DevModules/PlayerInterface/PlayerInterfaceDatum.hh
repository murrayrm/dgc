#ifndef PLAYER_INTERFACE_DATUM_HH
#define PLAYER_INTERFACE_DATUM_HH

/** the name of the config file containing custom host and port */
#define CONFIG_FILENAME "config"
/** the host to connect to the Player Server at (if no config file found) */
#define PLAYER_GAZEBO_HOST "localhost"
/** the port to connect to the Player Server at (if no config file found) */
#define PLAYER_GAZEBO_PORT 6665

#include <iostream> // cout
#include <fstream> // write log file
#include <cmath> // sin, cos, atan2, M_PI
#include <string> // hostname
#include "VehicleState.hh"	// new format of vehicle state
#include "vehlib/VState.hh" // The State struct sent via MTA
#include "vehlib/VDrive.hh" // The Command motion struct sent via MTA
#include "playerclient.h" // PlayerClient C++ library (interface to Player/Gazebo)
using namespace std;

/** The period that decides the update rate we send commands to Gazebo (us)
 * It is crucial that this sleep is smaller than 1/PLAYER_DELIVERY_FREQUENCY 
 * seconds or we will start lagging behind getting the state. The lower the 
 * better but you might want to adjust this if you're experiencing heavy load
 * and is sharing the CPU. PlayerInterface shouldn't take much CPU since
 * most of the time is spent waiting for the Read() call and that should 
 * definitivly not require much CPU.
 */
const long PLAYERINTERFACE_PERIOD = 10000;

/** The period we want Player to deliver position data (default is 10Hz) 
 * Increasing this value seems dangerous since from all the tests I've made, 
 * the state PlayerGetState delivers gets older. I guess some buffer grows too
 * much in Player if you raise this value too high. I'd recommend the default
 * (10Hz).
 */
const unsigned short PLAYER_DELIVERY_FREQUENCY = 10; // Hz

/** The data mode we use for player communication */
const unsigned char DATA_MODE = PLAYER_DATAMODE_PUSH_NEW;

/** The protocol we want to recieve data from Player with */
const int PLAYER_PROTOCOL = PLAYER_TRANSPORT_TCP;

/** estimation of wheel base of the vehicle (meters). Used when calculating
 * the turn rate */
const double WHEELBASE = 1.6;

/** The distance from the center of the vehicle to the GPS reciever (meter) */
const double GPS_DIFF = 2.0;

/** struct with data to be shared between 
 * PlayerSendCmd and PlayerGetState modules
 */
class PI_DATUM {
public:
  /** The state message, contains all info about the current state */
  VState_GetStateMsg SS;
  VehicleState SS_new;		// new format of message
  /** The command messsage, contains all info about which velocity and 
   * steering angle the arbiter commands. */
  VDrive_CmdMotionMsg cmd;
  VDrive_CmdSteerAccel steerAccelCmd;

  double velCmd;
    
  // I let these stay from the SimVStateVDrive struct
  int ServedState;
  int CmdMotionCount;
  int CmdDirectDriveCount;
  
  /** Timestamp for the last update */
  Timeval lastUpdate;
  /** Timestamp for the startup time */
  Timeval startTime;

  /** Timestamp for the last time we sent a command **/
  Timeval lastCommandTime;

  /** PlayerClient, our link to Player/Gazebo */
  PlayerClient *client;
  
  /** Position device, used for commanding the robot's velocity and steering */
  PositionProxy *pos;
  
  /** Position3D device, used for reading state as position, yaw, roll and 
   * theta */
  Position3DProxy *pos3d;
  
  /** GPS simulated device */
  GpsProxy *gps;
  
  /** Device used for retrieving the true position+yaw of the robot */
  TruthProxy *truth;
};

/** Converts between gazebo's yaw angle representation and Bob's
 * (unit = radians)
 * Bob has north as 0 radians and CW positive and CCW negative.
 * Gazebo has east as 0 radians and CW positive and CCW negative.
 * @param angle the angle to convert.
 * @return the converted angle
 */
double convertYaw(double angle); // radians

#endif
