#ifndef PLAYERSENDCMD_HH
#define PLAYERSENDCMD_HH

#include "MTA/Kernel.hh" // The MTA Module Kernel
using namespace std;

// ##########################################################################
//  PlayerSendCmd module
// ##########################################################################

/** This class gets commands from the arbiter and sends them to the
 * Player/Gazebo simulator. It uses a position device for sending the velocity
 * and steering commands.
 */
class PlayerSendCmd : public DGC_MODULE {
public:
  /** Constructor */
  PlayerSendCmd();

  void Init();

  /**
   * This method contains the main loop for the PlayerSendCmd module.
   * It just logs state data if loggin is enabled.
   */
  void Active();

  /** MTA message handling method. Recieves velocity+steering command from
   * the arbiter, converts them to Gazebo-useful commands and tell the
   * simulation to execute them
   */
  void InMailHandler(Mail & ml);

  /**
   * MTA messaging method
   * This method should never be called since we only want to give
   * orders to VDrive.
   * \param ml the MTA mail message
   */
  Mail QueryMailHandler(Mail & ml);

private:
  /** Just opens and initializes the logfile with date stamp  */
  void initLogFile(ofstream &file);
  
  /**
   * Calculates turnrate out of which desired angle we have.
   * \param velocity velocity in m/s
   * \param steering_angle global steering_angle in [-1,1]
   */
  double calcTurnRate(double velocity, double steering_angle);

  /**
   * Calculates turnrate out of which desired angle we have.
   * \param velocity velocity in m/s
   * \param steering_angle global steering_angle in radians
   */
  double calcTurnRateForSteerAccel(double velocity, double steering_angle);
};

#endif
