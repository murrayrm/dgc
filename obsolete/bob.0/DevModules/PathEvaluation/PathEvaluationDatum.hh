#ifndef PATHEVALUATIONDATUM_HH
#define PATHEVALUATIONDATUM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <vector>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "RaceModules/Arbiter/Vote.hh"
#include "RaceModules/Arbiter/SimpleArbiter.hh"
#include "RaceModules/Arbiter/ArbiterModule.hh"

#include "Constants.h" //for vehicle constants, ie serial ports, etc.

// genMap
#include "genMap.hh"

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"
#include "vehlib/VDrive.hh"

#define NUM_LINES 1000
#define MAP_RESOLUTION 0.2

using namespace std;

// All data that needs to be shared by common threads
struct PathEvaluationDatum {

  Voter pathEvaluation; // voter struct for the Arbiter

  // State struct from VState
  boost::recursive_mutex StateStructLock;
  VState_GetStateMsg SS;

  genMap localMap;

};


#endif
