#include "MTA/Kernel.hh"
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include "VState.hh"
#include "VehicleState.hh"

class vstate_test : public DGC_MODULE {

 public:
  vstate_test();
  ~vstate_test();

  void Active();
  void InMailHandler(Mail & ml);
  Mail QueryMailHandler(Mail & ml);

private:

  VehicleState vehiclestate;
  GetMetaStateMsg metastate;
  VState_GetStateMsg vehstate;

};

namespace vstate_test_messages {

  enum {
    Num // A place holder
  };

};
