#include "vstate_test.hh"
#include <stdio.h>
#include <fstream>

fstream testlogfile;
fstream pathlogfile;
char *testlog = new char[100];
char *pathlog = new char[100];
double timecalc;
Timeval start;


vstate_test::vstate_test() : DGC_MODULE( MODULES::vstate_test, "Module to test the new version of vstate", 0) {
   	testlogfile.open(testlog, fstream::out|fstream::app);
	testlogfile << "Time\tgps_enabled\timu_enabled\tnorth\teast\talt\tVel_N\tVel_E\tVel_U\tacc_n\tacc_e\tacc_u\tRoll\tPitch\tYaw\trollrate\tpitchrate\tyawrate\trollacc\tpitchacc\tyawacc\n\r";
	testlogfile.close();
}

vstate_test::~vstate_test() {
}

void vstate_test::Active() {
	start = TVNow();

  while( ContinueInState() ) {

    usleep(100000);

	Mail msg = NewQueryMessage( MyAddress(), MODULES::VState, VStateMessages::GetVehicleState );
	Mail reply = SendQuery(msg);
	
	if (reply.OK()) {
		reply >> vehiclestate;
	}


	msg = NewQueryMessage( MyAddress(), MODULES::VState, VStateMessages::GetMetaState );
	reply = SendQuery(msg);
	
	if (reply.OK()) {
		reply >> metastate;
	}
    timecalc = (double) (vehiclestate.Timestamp - 1000000*start.sec() - start.usec()) / 1.0e6;

    testlogfile.open(testlog, fstream::out|fstream::app);
    testlogfile.precision(10);
    testlogfile <<  timecalc << '\t';
    testlogfile <<  metastate.gps_enabled << '\t' << metastate.imu_enabled << "\t";
    testlogfile <<  vehiclestate.Northing << '\t' << vehiclestate.Easting << '\t' << vehiclestate.Altitude << "\t";
    testlogfile <<  vehiclestate.Vel_N << '\t' << vehiclestate.Vel_E << '\t' << vehiclestate.Vel_D << "\t";
    testlogfile <<  vehiclestate.Acc_N << '\t' << vehiclestate.Acc_E << '\t' << vehiclestate.Vel_D << "\t";
    testlogfile <<  vehiclestate.Roll << '\t' << vehiclestate.Pitch << '\t' << vehiclestate.Yaw << "\t";
    testlogfile <<  vehiclestate.RollRate << '\t' << vehiclestate.PitchRate << '\t' << vehiclestate.YawRate << "\t";
    testlogfile <<  vehiclestate.RollAcc << '\t' << vehiclestate.PitchAcc << '\t' << vehiclestate.YawAcc << "\r\n";
	testlogfile.close();

    pathlogfile.open(pathlog, fstream::out|fstream::app);
    pathlogfile.precision(10);
	pathlogfile << vehiclestate.Northing << '\t' << vehiclestate.Vel_N << '\t' << vehiclestate.Acc_N << '\t';
	pathlogfile << vehiclestate.Easting << '\t' << vehiclestate.Vel_E << '\t' << vehiclestate.Acc_E << '\t';
	pathlogfile << vehiclestate.Altitude << '\t' << vehiclestate.Vel_D << '\t' << vehiclestate.Acc_D << "\r\n";
	pathlogfile.close();
  }

}

void vstate_test::InMailHandler(Mail & msg) {

  // Shouldn't be receiving messages...

  switch ( msg.MsgType() ) {

  default:
    // let the parent class handle the message
    DGC_MODULE::InMailHandler(msg);
  }
}

Mail vstate_test::QueryMailHandler(Mail & msg) {

  // Nor should we be receiving Query's...

  Mail reply = ReplyToQuery(msg); // this sets up the return message

  switch ( msg.MsgType() ) {

  default:
    // let the parent class handle the message
    reply = DGC_MODULE::QueryMailHandler(msg);
  }

  return reply;

}

int main(int argc, char **argv) {

  sprintf(testlog,"%s_full.dat",argv[1]);
  sprintf(pathlog,"%s_path.dat",argv[1]);

  Register(shared_ptr<DGC_MODULE>(new vstate_test));
  StartKernel();

  return 0;
}
