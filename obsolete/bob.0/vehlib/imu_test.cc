#include "imu.h"

#include <stdio.h>

int main(void)
{
  int result;
  // Open the IMU (return 0 if success).
  printf("Calling IMU_open...\n");
  result = IMU_open();
  printf("  open result = %d\n", result);

  // Read the IMU
  IMU_DATA data;
  printf("Calling IMU_read...\n");
  result = IMU_read(&data);
  printf("read result = %d\n", result);

  return 0;
}
