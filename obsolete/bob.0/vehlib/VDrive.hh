/* VDrive.hh - header file for communicating with vdrive MTA interface
 *
 * Richard Murray
 * 3 January 2004
 * 
 * This file defines the format for motion command that is sent
 * to Vdrive.
 *
 */

#ifndef VDRIVE_HH
#define VDRIVE_HH

#ifndef NOMTA
#include "MTA/Misc/Time/Timeval.hh"
#include "MTA/Misc/Mail/Mail.hh"
#endif

/*! Old school struct for commanding motion (speed and steering). */
struct VDrive_CmdMotionMsg
{
  /*! Time stamp when data was sent (unix time). */
#ifndef NOMTA
  Timeval Timestamp;
#else
	unsigned long long Timestamp;
#endif
  /*! Desired speed (m/s). */
  double velocity_cmd;
  /*! Commanded NORMALIZED steering angle ([-1,1]). */
  double steer_cmd;
};

/*! New struct for direct control of steering and accel via MTA. */
struct VDrive_CmdSteerAccel
{
  /*! time stamp when data was sent */
#ifndef NOMTA
  Timeval Timestamp;	
#else
	unsigned long long Timestamp;
#endif
  /*! desired acceleration (m/s^2) */
  double accel_cmd;		
  /*! commanded steering angle ->IN RADIANS<- */
  double steer_cmd;		
};

/*! Enumerates all module messages that could be received by vdrive. */
namespace VDriveMessages 
{
  enum 
  {
    //Input Commands
    CmdMotion,			// Command a given motion
    CmdDirectDrive,	// Command a given motion directly (steer and accel)
    SetAccelOff,		// Set accel_off
    SetSteerOff,		// Set steer_off
    SetSpeedRef,		// Set speed_ref
    SetCruiseGain,		// Set cruise_gain

    //Calibrate commands
    ThrottleCalib,		// Performs a throttle calibration
    BrakeCalib,			// Performs a brake calibration
    SteerCalib,			// Performs a throttle calibration
    
    //Reset commands
    ThrottleReset,		// Perform a throttle reset
    BrakeReset,			// Perform a brake reset
    SteerReset,			// Perform a steer reset

    //Enable commands
    CaptureEnable,                  // Sets the capture mode on or off
    ThrottleEnable,		// Sets the throttle actuator on or off
    BrakeEnable,		// Sets the brake actuator on or off
    SteerEnable,		// Sets the steering actuator on or off
    CruiseEnable,		// Sets the cruise controller on or off

    //Misc commands
    SetMode,			// Set mode to off, auto, manual, etc
    DoEstop,			// Performs an estop_pause
    DumpFilename,		// Inputs name of dumpfile
    SteerInc,			// Performs a steer_inc
    SteerDec,			// Performs a steer_dec
    AccelInc,			// Performs an accel_inc
    AccelDec,			// Performs an accel_dec
    FullStop,			// Performs a full_stop
    UserQuit,			// Performs a user_quit

    // Added by Ike (to get VStarPackets.cc to compile
    // ignore for now)
    Get_Steer,         // returns a steer packet
    Get_Throttle,      // returns a throttle packet
    Get_Brake,

    Get_VDriveStack,   // grabs a vdrive display stack

    NumMessages			// Placeholder (not used)
  };
};

#endif

