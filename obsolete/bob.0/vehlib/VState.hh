/* VState.hh - message format for state data
 *
 * Lars Cremean, Ike Gremmer
 * 2 January 2004
 * 
 * This file defines the format for state data that is requested
 * from VState.  It will probably go in a file with a different name,
 * but for now it is here.
 *
 * HH 3 Jan 04
 * changed attitude angles to radians, added magnetometer
 * 
 * JML 12 Dec 04
 * Added new clean structs for State and Meta data respectively
 * Old Struct left unmodified for now for backwards compatability
 *
 */

#ifndef VSTATE_HH
#define VSTATE_HH

#include "MTA/Misc/Time/Timeval.hh"
#include "MTA/Misc/Mail/Mail.hh"

#include "imu.h"
#include "gps.h"
#include "magnetometer.h"

#warning "VState_GetStateMsg is deprecated.  Use the platform-independent VehicleState instead (defined in dgc/include/VehicleState.hh)"
/*! This struct describes the state the vechicle is in at a specific timestamp */
struct VState_GetStateMsg
{
  /*! time stamp when GPS data is received */
  Timeval Timestamp;    



  /*! UTM easting in meters */
  double Easting;
  /*! UTM northing in meters */
  double Northing;
  /*! altitude fed to sensing (always 0) */
  float  Altitude;      

  // float real_Altitude;  //actual altitude

  /*! latitude from KF */
  double kf_lat;        
  /*! longitude from KF */
  double kf_lng;        
  /*! east velocity in m/s */
  float  Vel_E;         
  /*! north velocity in m/s */
  float  Vel_N;         
  /*! up velocity in m/s */
  float  Vel_U;         
  /*! m/s */
  float  Speed;         
  /*! m/s^2 */
  double Accel;        
 
  /*! pitch in radians */
  float  Pitch;         
  /*! roll in radians */
  float  Roll;          
  /*!  heading in radians, north=0 rads, clockwise increase */
  float  Yaw;          
  
  /*! currently not implemented*/
  float  PitchRate;    
  /*! currently not implemented */
  float  RollRate;
  /*! currently not implemented */
  float  YawRate;

   
  /*! flag for sensors, 1 means it's up and running, 0 or -1 means it's inactive */
  int gps_enabled;
  /*! flag for sensors, 1 means it's up and running, 0 or -1 means it's inactive */
  int imu_enabled;
  /*! flag for sensors, 1 means it's up and running, 0 or -1 means it's inactive */
  int mag_enabled;

   
  /*! gps pvt has a solution or not */
  int gps_pvt_valid;

  /*! raw GPS data */
  struct gpsData gpsdata; 
  /*! raw IMU data */
  struct imu_data imudata;
  /*! raw magnetometer data */
  struct mag_data magreading;    
  
  /*! the actual altitude of the vehicle */
  double actual_Altitude;  

  /*! jumped after extended gps outage */
  int ext_jump_flag;  

};


/* New Clean struct for Vehicle State Messages */
/* NOTE: GetVehicleStateMsg no longer exists!  References to it should be 
 ** replaced with "VehicleState", thevehicle state struct now defined in 
 ** dgc/include/VehicleState.hh. */


/* New Clean struct for Meta State Messages */
struct GetMetaStateMsg
{
    Timeval Timestamp;    

    int gps_enabled;
    int imu_enabled;
    int mag_enabled;
    
    int gps_pvt_valid;
    int ext_jump_flag;  
};

// enumerate all module messages that could be received
namespace VStateMessages {
  enum {
    GetState,			// Read the current state from the file

    // Added by ike -- just ignore
    Get_VStateStack, 

    KILL,

    GetVehicleState,
    GetMetaState,

    NumMessages			// Placeholder (not used)
  };
};

#endif
