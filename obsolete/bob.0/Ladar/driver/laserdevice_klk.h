/*********************************************************************************/
/* SICK LASER DRIVER V0.9                                                        */
/* Copyright (C) 2000 - Kasper Stoy - USC Robotics Labs (See README for details) */
/*********************************************************************************/
#ifndef LASERDEVICE
#define LASERDEVICE
#include <pthread.h>

// Status byte returned by GetData() is an int
// the low 8 bits are the status returned by the scanner
// the high 8 bits are as below
#define LADAR_STATUS_DAZZLED       0x0100
#define LADAR_STATUS_CRC_ERROR     0x0200
#define LADAR_READING_TOO_BIG      0x0400
#define LADAR_CHANNEL1_ERROR       0x0800
#define LADAR_SNR_ERROR            0x1000
#define LADAR_OVERFLOW             0x2000
#define LADAR_DAZZLING             0x4000
#define LADAR_INVALID_MEASUREMENT  0x8000

#define LADAR_DATA_GOOD            0
#define LADAR_DATA_BAD             1
#define LADAR_DATA_NORETURN        2

#define LADAR_BYTES_PER_SCANPOINT  2
#define LADAR_STATUS_LEN           1
#define LADAR_CRC_LEN              2
#define LADAR_HEADER_LEN           7

class CDevice {
private:
  pthread_mutex_t dataAccessMutex;
  pthread_mutex_t setupMutex;

  bool firstdata;

public:
  CDevice();
  virtual ~CDevice();

  int LockNGetData( unsigned char * );
  int LockNGetData();
  void LockNPutData( unsigned char *, int );
  int LockNShutdown();
  virtual int Setup(char *port,int,double,int) = 0;

  /* these should really be private but the run thread needs access to them */
  virtual int Shutdown() = 0;
  virtual int GetData() = 0;
  virtual int GetData( unsigned char * ) = 0;
  virtual void PutData( unsigned char *, int ) = 0;
  virtual int getDataLen() = 0;
  virtual int getTotalPacketLen() = 0;
  virtual int getNumDataPoints() = 0;
};

class CLaserDevice:public CDevice {
 private:
  pthread_t thread;           // the thread that continuously reads from the laser 
  unsigned char *data;        // array holding the most recent laser scan
  int data_status;            // status byte returned by scanner
  int debuglevel;             // debuglevel 0=none, 1=basic, 2=everything


  ssize_t WriteToLaser( unsigned char *data, ssize_t len ); 

  /* methods used to decode the response from the laser */
  ssize_t ReceiveAck(int len);
  bool CheckDatagram( unsigned char *datagram, int len );

  /* methods used to configure the laser */
  int Request38k();
  int SetAngleResolution(int, double);
  int RequestData();
  int ChangeMode( );

 public:

  CLaserDevice(void);
  void DecodeStatusByte( unsigned char byte );
  int Setup(char *port,int angle,double res,int);
  int Reset(int);
  int isResetting();
  void Run();
  int Shutdown();
  int GetData();
  int GetData( unsigned char * );
  void PutData( unsigned char *, int );
  int GetPoint();
  int UpdateScan() {return LockNGetData();}
  int isError(int status);
  int isDataError(int status,int scan);
  int getDataLen();
  int getTotalPacketLen();
  int getNumDataPoints();

  /* these should really be private but the run thread needs access to them */
  int laser_fd;               // laser device file descriptor
  char *LASER_SERIAL_PORT;    // device used to communicate with the laser

  /* Calculates CRC for a telegram */
  unsigned short CreateCRC( unsigned char* commData, unsigned int uLen );
  int Index;
  int *Scan; // accessable array  

  bool ladarResetting;
  int ladarResetCount;

  int myBumperID; // Used by parallel port code to reset power
  int scanAngle; // can be 100 or 180
  double scanResolution; // can be 1.0, 0.5, or 0.25
};


#endif
