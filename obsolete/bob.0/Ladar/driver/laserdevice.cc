/******************************************************************************/
/* SICK LASER DRIVER V0.9                                                     */
/* Copyright (C) 2000 - Kasper Stoy - USC Robotics Labs                       */
/******************************************************************************/
#include <fcntl.h>
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
#include <unistd.h>
#include <sys/times.h>
#include <assert.h>

#include "ladar_power.h"
#include "laserdevice.h"
#define MKSHORT(a,b) ((unsigned short)(a)|((unsigned short)(b)<<8))

#define CRC16_GEN_POL 0x8005

unsigned char startString[] = { 0x02, 0x80, 0xd6, 0x02, 0xb0, 0x69, 0x01 };
const unsigned char ACK[] = { 0x02, 0x80, 0x03, 0x00, 0xa0 };
const unsigned char NACK[] = { 0x02, 0x80, 0x03, 0x00, 0x92 };
const unsigned char STX = 0x02;

CLaserDevice::CLaserDevice(void) {
  puts("CLaserDevice Constructor called.");
  debuglevel=0;
  puts("SICK LASER DRIVER V0.9, COPYRIGHT (C) 2000");
  puts("KASPER STOY - USC ROBOTICS LABS");
}

int CLaserDevice::getNumDataPoints()
{
  return (int) ( (double) scanAngle / scanResolution) + 1;
}

int CLaserDevice::getDataLen()
{
  return getNumDataPoints() * LADAR_BYTES_PER_SCANPOINT;
}

int CLaserDevice::getTotalPacketLen()
{
  return getDataLen() + LADAR_HEADER_LEN + LADAR_STATUS_LEN + LADAR_CRC_LEN;
}
unsigned short CLaserDevice::CreateCRC( unsigned char* commData, unsigned int uLen )
{
  unsigned short uCrc16;
  unsigned char abData[2];
  
  uCrc16 = 0;
  abData[0] = 0;
  
  while(uLen-- ) {
    abData[1] = abData[0];
    abData[0] = *commData++;
    
    if( uCrc16 & 0x8000 ) {
      uCrc16 = (uCrc16 & 0x7fff) << 1;
      uCrc16 ^= CRC16_GEN_POL;
    }
    else {
      uCrc16 <<= 1;
    }
    uCrc16 ^= MKSHORT (abData[0],abData[1]);
  }
  return( uCrc16); 
}

ssize_t CLaserDevice::WriteToLaser( unsigned char *data, ssize_t len ) {
  unsigned char *datagram;
  unsigned short crc;
  ssize_t sz;

  datagram = new unsigned char[len+6];

  datagram[0] = STX; /* start byte */
  datagram[1] = 0x00; /* LMS address */
  datagram[2] = 0x0F & len; /* LSB - number of bytes to follow excluding crc */
  datagram[3] = 0xF0 & len; /* MSB - number of bytes to follow excluding crc */

  memcpy( (void *) &datagram[4], (void *) data, len );

  /* insert CRC */
  len += 4;
  crc = CreateCRC( datagram, len );
  datagram[len] = crc & 0x00FF;
  datagram[len+1] = (crc & 0xFF00) >> 8;
  len += 2;

  if (debuglevel>1) {
    printf("\nSending: ");
    for(int i=0; i<len; i++) {
      printf("%.2xh/", datagram[i]);
    }
    printf("\n");
  }

  sz = write( laser_fd, datagram, len);

  delete datagram;
  return (sz);
}

int CLaserDevice::isDataError(int status, int scan)
{
  if((status & 0x00FF) != 0x0010) return LADAR_DATA_BAD;
  else if(scan == 0x1FFF) return LADAR_DATA_NORETURN;
  else if(scan >= 0x1FF7) return LADAR_DATA_BAD;
  else return LADAR_DATA_GOOD;
}

int CLaserDevice::isError(int status) {
  if(status != 0x0010) return true;
  return false;
}

void CLaserDevice::DecodeStatusByte( unsigned char byte ) {
  unsigned short code;

  /* print laser status */
  fprintf(stderr,"Laser Staus: ");
  code = byte & 0x07;
  switch(code) {
  case 0:
    fprintf(stderr,"no error ");
    break;
  case 1:
    fprintf(stderr,"info ");
    break;
  case 2:
    fprintf(stderr,"warning ");
    break;
  case 3:
    fprintf(stderr,"error ");
    break;
  case 4:
    fprintf(stderr,"fatal error ");
    break;
  default:
    fprintf(stderr,"unknown code ");
    break;
  }
 
  code = (byte >> 3) & 0x03;
  switch(code) {
  case 0:
    fprintf(stderr,"LMS -xx1 to -xx4 ");
    break;
  case 1:
    fprintf(stderr,"LMI ");
    break;
  case 2:
    fprintf(stderr,"LMS -xx6 ");
    break;
  case 3:
    fprintf(stderr,"reserved ");
    break;
  default:
    fprintf(stderr,"unknown device ");
    break;
  }

  fprintf(stderr,"restart:%d ", (byte >> 5) & 0x01);

  if ( ( byte >> 6 ) & 0x01 ) 
    fprintf(stderr,"Implausible measured value ");

  if ( ( byte >> 7 ) & 0x01 ) 
    fprintf(stderr,"Pollution ");

  fprintf(stderr,"\n");
}

bool CLaserDevice::CheckDatagram( unsigned char *datagram, int len ) {
  unsigned short crc;

  if (debuglevel>1) {
    printf("\nReceived: ");
    for(int i=0; i<len+2; i++) {
      printf("%.2xh/", datagram[i]); fflush(stdout);
    }
    printf("\n");fflush(stdout);
  }

  /* check CRC */
  crc = CreateCRC( &datagram[0], len );
  if ( datagram[len] != (crc & 0x00FF) || datagram[len+1] != ((crc & 0xFF00) >> 8) ) {
    if (debuglevel>1) {
      printf("\ncrc incorrect: expected 0x%.2x 0x%.2x got 0x%.2x 0x%.2x\n",
	     ((crc & 0xFF00) >> 8), (crc & 0x00FF), datagram[len], datagram[len+1] );
    }
    return(false);
  }

  /* decode message */
  if (strncmp( (const char *) datagram, (const char *) ACK, 5 ) == 0 ) {
    switch( datagram[5] ) {
    case 0x00: 
      puts("ok");
      break;
    case 0x01:
      puts("request denied - incorrect password");
      break;
    case 0x02:
      puts("request denied - LMI fault");
      break;
    }
  }
  else if (strncmp( (const char *) datagram, (const char *) NACK, 5 ) == 0 ) {
    puts("not acknowledged");
  }

  return(true);
} 

int CLaserDevice::isResetting() {
  if(ladarResetting) return ladarResetCount;
  else return 0;
}

int CLaserDevice::Reset(int bumperID) {
  struct termios term;

  ladarResetCount++;
  ladarResetting = true;

  ladar_off(bumperID);
  sleep(10);
  ladar_on(bumperID);
  sleep(60);
  ladarResetting = false;

  // set the serial port speed to 9600 to match the laser
  // later we can ramp the speed up to the SICK's 38K
  if( tcgetattr( laser_fd, &term ) < 0 )
    printf( "get attributes error\n" );
  
  cfmakeraw( &term );
  cfsetispeed( &term, B9600 );
  cfsetospeed( &term, B9600 );
  
  if( tcsetattr( laser_fd, TCSAFLUSH, &term ) < 0 )
    printf( "set attributes error\n" );

  if( ChangeMode() != 0 ) {
    puts("Change mode failed most likely because the laser");
    puts("is running 38K..switching terminal to 38k");
  }
  else if( Request38k() != 0 ) {
    puts("CLaserDevice:Setup: Couldn't change laser speed to 38k");
    return( 1 );
  }

  // set serial port speed to 38k
  if( tcgetattr( laser_fd, &term ) < 0 )
    printf( "get attributes error\n" );
  cfmakeraw( &term );
  cfsetispeed( &term, B38400 );
  cfsetospeed( &term, B38400 );
  if( tcsetattr( laser_fd, TCSAFLUSH, &term ) < 0 )
    printf( "set attributes error\n" );

  if( SetAngleResolution(scanAngle,scanResolution) != 0 ) {
    printf("Couldn't set angle/resolution\n");
    return( 1 );
  }
  if( RequestData() != 0 ) {
    return( 1 );
  }
  return(0);
}

/**** OLD RESET 
int CLaserDevice::Reset(bool clear) {
  unsigned char request[1];
  int len;

  ladarResetCount++;
  ladarResetting = true;

  if(clear) {
    len = 0;
    fprintf(stderr, "Dumping buffer\n");
    while( (len < 10000) && (read(laser_fd, &request, 1) == 1)) {
      len++;
//      fprintf(stderr, "%X\n", request[0]);
    }
    fprintf(stderr, "dumped %d bytes\n", len);
  }

  printf("Send reset command\n");
  // Then we send the reset command

  request[0] = 0x10; 

  len = 1;

  printf("Sending reset request to laser..\n\n"); fflush(stdout);
  if ( (len = WriteToLaser( request, len)) < 0 ) {
    ladarResetting = false;
    perror("Reset");
    return(1);
  }
  sleep(40);
  ladarResetting = false;
  return(0);
}
******/

ssize_t CLaserDevice::ReceiveAck(int len) {
  unsigned char *datagram;
  ssize_t sz;

  datagram = new unsigned char[len+2];

  /* laser sends acknowledge within 60ms therefore 
     with 70ms we are on the safe side */
  usleep(700000);
  
  while(1) {
    if ( (sz = read( laser_fd, &datagram[len+1], 1 )) < 0 ) {
      puts("no acknowledge received");
      return(sz);
    }
    
    if (datagram[0]==STX && CheckDatagram( datagram,len ) )
      break;
    else
      for(int i=0;i<len+1;i++) datagram[i]=datagram[i+1];
  }

  return (sz);
}


int CLaserDevice::ChangeMode(  )
{ 
  ssize_t len;
  unsigned char request[20];

  request[0] = 0x20; /* mode change command */
  request[1] = 0x00; /* configuration mode */
  request[2] = 0x53; // S - the password 
  request[3] = 0x49; // I
  request[4] = 0x43; // C
  request[5] = 0x4B; // K
  request[6] = 0x5F; // _
  request[7] = 0x4C; // L
  request[8] = 0x4D; // M
  request[9] = 0x53; // S

  len = 10;
  
  printf("Sending configuration mode request to laser.."); fflush(stdout);
  if ( (len = WriteToLaser( request, len)) < 0 ) {
    perror("ChangeMode");
    return(1);
  }

  if ( ( len = ReceiveAck(LADAR_HEADER_LEN) ) < 0 ) {
    perror("ChangeMode");
    return(1);
  }

  return 0;
}

int CLaserDevice::Request38k()
{ 
  ssize_t len;
  unsigned char request[20];

  request[0] = 0x20; /* mode change command */
  request[1] = 0x40; /* 38k */

  len = 2;
  
  printf("Sending 38k request to laser.."); fflush(stdout);
  if ( (len = WriteToLaser( request, len)) < 0 ) {
    perror("Request38k");
    return(1);
  }

  if ( ( len = ReceiveAck(LADAR_HEADER_LEN) ) < 0 ) {
    perror("Request38k");
    return(1);
  }

  return 0;
}

int CLaserDevice::SetAngleResolution(int angle,double res)
{ 
  ssize_t len;
  unsigned char request[20];

  assert( ((angle == 100) && (res == 1.0))  ||
          ((angle == 100) && (res == 0.5))  ||
          ((angle == 100) && (res == 0.25)) ||
          ((angle == 180) && (res == 1.0))  ||
          ((angle == 180) && (res == 0.5)) );

  scanAngle = angle;
  scanResolution = res;

  request[0] = 0x3B;
  request[2] = 0x00;
  request[4] = 0x00;
  startString[0] = 0x02;
  startString[1] = 0x80;
  startString[4] = 0xB0;
  if((angle == 100) && (res == 1.0)) {
    request[1] = 0x64;
    request[3] = 0x64;
    startString[2] = 0xCE;
    startString[3] = 0x00;
    startString[5] = 0x65;
    startString[6] = 0x00;
  } else if((angle == 100) && (res == 0.5)) {
    request[1] = 0x64;
    request[3] = 0x32;
    startString[2] = 0x96;
    startString[3] = 0x01;
    startString[5] = 0xC9;
    startString[6] = 0x00;
  } else if((angle == 100) && (res == 0.25)) {
    request[1] = 0x64;
    request[3] = 0x19;
    startString[2] = 0x26;
    startString[3] = 0x03;
    startString[5] = 0x91;
    startString[6] = 0x01;
  } else if((angle == 180) && (res == 1.0)) {
    request[1] = 0xB4;
    request[3] = 0x64;
    startString[2] = 0x6E;
    startString[3] = 0x01;
    startString[5] = 0xB5;
    startString[6] = 0x00;
  } else if((angle == 180) && (res == 0.5)) {
    request[1] = 0xB4;
    request[3] = 0x32;
    startString[2] = 0xD6;
    startString[3] = 0x02;
    startString[5] = 0x69;
    startString[6] = 0x01;
  }

  len = 5;
  
  printf("Sending angle/res request to laser.."); fflush(stdout);
  if ( (len = WriteToLaser( request, len)) < 0 ) {
    perror("SetAngleResolution");
    return(1);
  }

  if ( !ReceiveAck(11) ) {
    perror("SetAngleResolution");
    return(1);
  }

  return 0;
}

int CLaserDevice::RequestData()
{ 
  ssize_t len;
  unsigned char request[20];

  request[0] = 0x20; /* mode change command */
  request[1] = 0x24; /* request data */

  len = 2;
  
  printf("Sending data request to laser.."); fflush(stdout);
  if ( (len = WriteToLaser( request, len)) < 0 ) {
    perror("RequestData");
    return(1);
  }

  if ( ( len = ReceiveAck(LADAR_HEADER_LEN) ) < 0 ) {
    perror("RequestData");
    return(1);
  }

  return 0;
}

int CLaserDevice::Setup(char *port,int angle,double res,int bumperID) {
  struct termios term;

  ladarResetting = false;
  ladarResetCount = 0;

  myBumperID = bumperID;
  scanAngle = angle;
  scanResolution = res;

  data = (unsigned char *) malloc(getTotalPacketLen());
  bzero(data, getTotalPacketLen());

  Scan = (int *) malloc(getNumDataPoints() * sizeof(int));
  printf("Attempting to open port %s\n", port);
  LASER_SERIAL_PORT = strdup(port);

  if( (laser_fd = open( LASER_SERIAL_PORT, O_RDWR | O_SYNC | O_NONBLOCK , S_IRUSR | S_IWUSR )) < 0 ) {
    perror("CLaserDevice:Setup");
    return(1);
  }  
 
  // set the serial port speed to 9600 to match the laser
  // later we can ramp the speed up to the SICK's 38K
  if( tcgetattr( laser_fd, &term ) < 0 )
    printf( "get attributes error\n" );
  
  cfmakeraw( &term );
  cfsetispeed( &term, B9600 );
  cfsetospeed( &term, B9600 );
  
  if( tcsetattr( laser_fd, TCSAFLUSH, &term ) < 0 )
    printf( "set attributes error\n" );

  if( ChangeMode() != 0 ) {
    puts("Change mode failed most likely because the laser");
    puts("is running 38K..switching terminal to 38k");
  }
  else if( Request38k() != 0 ) {
    puts("CLaserDevice:Setup: Couldn't change laser speed to 38k");
    return( 1 );
  }

  // set serial port speed to 38k
  if( tcgetattr( laser_fd, &term ) < 0 )
    printf( "get attributes error\n" );
  cfmakeraw( &term );
  cfsetispeed( &term, B38400 );
  cfsetospeed( &term, B38400 );
  if( tcsetattr( laser_fd, TCSAFLUSH, &term ) < 0 )
    printf( "set attributes error\n" );

  if( SetAngleResolution(angle,res) != 0 ) {
    printf("Couldn't set angle/resolution\n");
    return( 1 );
  }

  if( RequestData() != 0 ) {
    printf("Couldn't request data most likely because the laser\nis not connected or if connected not to %s\n", LASER_SERIAL_PORT);
    return( 1 );
  }

  puts("Laser ready");
  fflush(stdout);
  /* success: start the "Run" thread and report success */

  close(laser_fd);

  // 1/4/03 this function is causing a segfault in our code! 
  // (it overwrites the DATUM d of WaypointNav.hh) -Lars
  // crc incorrect: expected 0x05 0x6f got 0x39 0x0f - ignoring scan
  // crc incorrect: expected 0x3d 0x0d got 0x0b 0xf1 - ignoring scan
  Run();

  return(0);

} // end CLaserDevice::Setup()

int CLaserDevice::Shutdown() {
  /* shutdown laser device */
  close(laser_fd);
  pthread_cancel( thread );
  puts("Laser has been shutdown");

  return(0);
}

void *RunThread( void *laserdevice ) {
  unsigned char *local_data;
  int n,c;
  int bytes = 0;
  unsigned short crc;
  int status = 0,val;
  fd_set rfds;
  struct timeval tv;

  CLaserDevice *ld = (CLaserDevice *) laserdevice;

  int data_status_crc_len = ld->getDataLen() + LADAR_STATUS_LEN + LADAR_CRC_LEN;
  int header_data_status_len = LADAR_HEADER_LEN + ld->getDataLen() + LADAR_STATUS_LEN;
  int header_data_len = LADAR_HEADER_LEN + ld->getDataLen();
//  int data_status_len = ld->getDataLen() + LADAR_STATUS_LEN;

  local_data = (unsigned char *) malloc(ld->getTotalPacketLen());
  pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, NULL);
  memset( local_data, 0, LADAR_HEADER_LEN );

  if( (ld->laser_fd = open( ld->LASER_SERIAL_PORT, O_RDWR | O_SYNC , S_IRUSR | S_IWUSR )) < 0 ) {
    perror("CLaserDevice:RunThread");
    pthread_exit(0);
  }  

  while(1) {
    /* test if we are supposed to cancel */
    pthread_testcancel();

    /* get laser scans one at a time */

    // read the stream of bytes one by one until we see the message header

    // move the first 7 bytes in the local_data buffer one place left
    for( n=0; n<LADAR_HEADER_LEN-1; n++ ) {
      local_data[n] = local_data[n+1];
    }
    // Used for call to select, timeout if no values read from LADAR for 0.1s
    FD_ZERO(&rfds);
    FD_SET(ld->laser_fd, &rfds);
    tv.tv_sec = 1;
    tv.tv_usec = 0;
    switch(select(ld->laser_fd+1, &rfds, NULL, NULL, &tv)) {
      case -1:
        fprintf(stderr, "laserdevice.cc: select() returned error\n");
        break;
      case 0:
        fprintf(stderr, "laserdevice.cc: LADAR stopped responding\n");
        ld->Reset(ld->myBumperID);
        break;
      default:
        // LADAR has data to be read
        break;
    }

    bytes += read( ld->laser_fd, &local_data[LADAR_HEADER_LEN-1], 1 );

    // The header will vary depending on the mode we are in
    // startString is set in the setAngleResolution function

    if( strncmp( (char *) local_data, (char *) startString, LADAR_HEADER_LEN ) == 0 ) // ok we've got the start of the local_data 
      {
	// now read the measured values (361*2 bytes) plus status (1 byte) and crc (2 bytes)
	bytes = 0;
	while(bytes<data_status_crc_len) {
	  bytes += read( ld->laser_fd, &local_data[bytes+LADAR_HEADER_LEN], data_status_crc_len-bytes );
	}

	crc = ld->CreateCRC( local_data, header_data_status_len );

	if ( local_data[header_data_status_len] != (crc & 0x00FF) || local_data[header_data_status_len+1] != ((crc & 0xFF00) >> 8) ) {
	  printf("crc incorrect: expected 0x%.2x 0x%.2x got 0x%.2x 0x%.2x - ignoring scan\n",
		 ((crc & 0xFF00) >> 8), (crc & 0x00FF), local_data[header_data_status_len], local_data[header_data_status_len+1] );
          status |= LADAR_STATUS_CRC_ERROR;
//	  continue;
	}

	for( c=LADAR_HEADER_LEN; c<header_data_len; c+=2 ) {
	  // check to see if laser is dazzled
	  if ( (local_data[c+1] & 0x20) == 0x20 ) {
	    puts("Laser dazzled - ignoring scan");
            status |= LADAR_STATUS_DAZZLED;
//	    continue;
	  }

	  // mask b to strip off the status bits 13-15
	  local_data[c+1] &= 0x1F;     

          val = local_data[c+1] << 8;
          val += local_data[c];
          if(val == 0x1FF7) {
            status |= LADAR_READING_TOO_BIG;
          } else if(val == 0x1FFA) {
            status |= LADAR_CHANNEL1_ERROR;
          } else if(val == 0x1FFB) {
            status |= LADAR_SNR_ERROR;
          } else if(val == 0x1FFD) {
            status |= LADAR_OVERFLOW;
          } else if(val == 0x1FFE) {
            status |= LADAR_DAZZLING;
          } else if(val == 0x1FFF) {
            status |= LADAR_INVALID_MEASUREMENT;
          }
	}
	/* test if we are supported to cancel */
	pthread_testcancel();

        status |= local_data[header_data_len]; // Status byte returned by scanner

	ld->LockNPutData( local_data, status );
      }
  }
  free(local_data);
}

void CLaserDevice::Run() {

  pthread_attr_t attr;
  
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
  pthread_create( &thread, &attr, &RunThread, this );
}

int CLaserDevice::GetData( unsigned char *dest ) {
  memcpy( dest, data, getDataLen() + 1 );  
  return data_status;
}

int CLaserDevice::GetData() {
  int j= 0;
  for(int i=1;i < getDataLen() + 1;i+=2) {
    Scan[j] =(unsigned short int)data[i] +
      ((unsigned short int)data[i+1] << 8) ;
    j++;
  }
  return data_status;
}

void CLaserDevice::PutData( unsigned char *src, int status ) {
  data[0]='l';
  memcpy( &data[1], &src[LADAR_HEADER_LEN], getDataLen() + LADAR_STATUS_LEN - 1 );
  data_status = status;
}


CDevice::CDevice() {
  /* the mutex's are defined to be default type (NULL) which is fast */
  pthread_mutex_init( &dataAccessMutex, NULL );
  pthread_mutex_init( &setupMutex, NULL );

  /* lock setup mutex */
  pthread_mutex_lock( &setupMutex );
  firstdata = true;
}

CDevice::~CDevice() {
  pthread_mutex_destroy( &dataAccessMutex );
  pthread_mutex_destroy( &setupMutex );
}

int CDevice::LockNShutdown() {
  if ( firstdata == false ) {
    // this means that the setup went okay and the call to lock will not be blocked
    pthread_mutex_lock( &setupMutex );
  }
  firstdata = true;
  return( Shutdown() );
}

int CDevice::LockNGetData( unsigned char *dest ) {
  int status = 0;
  pthread_mutex_lock( &setupMutex );
  pthread_mutex_lock( &dataAccessMutex );
  status = GetData(dest);
  pthread_mutex_unlock( &dataAccessMutex );
  pthread_mutex_unlock( &setupMutex );
  return status;
}

int CDevice::LockNGetData() {
  int status = 0;
  pthread_mutex_lock( &setupMutex );
  pthread_mutex_lock( &dataAccessMutex );
  status = GetData();
  pthread_mutex_unlock( &dataAccessMutex );
  pthread_mutex_unlock( &setupMutex );
  return status;
}

void CDevice::LockNPutData( unsigned char *dest, int status ) {
  pthread_mutex_lock( &dataAccessMutex );
  PutData(dest,status);
  pthread_mutex_unlock( &dataAccessMutex );
  if (firstdata) pthread_mutex_unlock( &setupMutex );
  firstdata=false;
}
