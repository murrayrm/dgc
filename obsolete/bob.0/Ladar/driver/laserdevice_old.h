/*********************************************************************************/
/* SICK LASER DRIVER V0.9                                                        */
/* Copyright (C) 2000 - Kasper Stoy - USC Robotics Labs (See README for details) */
/*********************************************************************************/
#ifndef LASERDEVICE
#define LASERDEVICE
#include <pthread.h>

// Status byte returned by GetData() is an int
// the low 8 bits are the status returned by the scanner
// the high 8 bits are as below
#define STATUS_DAZZLED   0x0100
#define STATUS_CRC_ERROR 0x0200

class CDevice {
private:
  pthread_mutex_t dataAccessMutex;
  pthread_mutex_t setupMutex;

  bool firstdata;

public:
  CDevice();
  virtual ~CDevice();

  int LockNGetData( unsigned char * );
  int LockNGetData();
  void LockNPutData( unsigned char *, int );
  int LockNShutdown();
  virtual int Setup(char *port) = 0;

  /* these should really be private but the run thread needs access to them */
  virtual int Shutdown() = 0;
  virtual int GetData() = 0;
  virtual int GetData( unsigned char * ) = 0;
  virtual void PutData( unsigned char *, int ) = 0;
};

class CLaserDevice:public CDevice {
 private:
  pthread_t thread;           // the thread that continuously reads from the laser 
  unsigned char data[1024];   // array holding the most recent laser scan
  int data_status;            // status byte returned by scanner
  int debuglevel;             // debuglevel 0=none, 1=basic, 2=everything


  ssize_t WriteToLaser( unsigned char *data, ssize_t len ); 

  /* methods used to decode the response from the laser */
  ssize_t RecieveAck();
  bool CheckDatagram( unsigned char *datagram );

  /* methods used to configure the laser */
  int Request38k();
  int RequestData();
  int ChangeMode( );

 public:

  CLaserDevice(void);
  void DecodeStatusByte( unsigned char byte );
  int Setup(char *port);
  void Run();
  int Shutdown();
  int GetData();
  int GetData( unsigned char * );
  void PutData( unsigned char *, int );
  int GetPoint();
  int UpdateScan() {return LockNGetData();}
  int isError(int status);

  /* these should really be private but the run thread needs access to them */
  int laser_fd;               // laser device file descriptor
  char LASER_SERIAL_PORT[11]; // device used to communicate with the laser

  /* Calculates CRC for a telegram */
  unsigned short CreateCRC( unsigned char* commData, unsigned int uLen );
  int Index;
  int Scan[361]; // accessable array  
};


#endif
