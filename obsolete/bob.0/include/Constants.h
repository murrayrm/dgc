#ifndef CONSTANTS_H
#define CONSTANTS_H

// constants only relevant to the ladar
#include "LadarConstants.h"
#include "VehicleConstants.h"
#include "SimulatorConstants.h"

// INCLUDE ONLY ONE OF THESE - these define maximum allowed speed, max
//   speed when an obstacle is detected, how close to allow obstacles to
//   get, etc...
// #include "QIDConstants.h"
#include "RACEConstants.h"
// #include "TESTConstants.h"

/* These constants have been MOVED to dgc/include/GlobalConstants.h since
 * they are completely platform independent (i.e. aren't specific to the 
 * implementation of code on Bob: METERS_PER_FOOT, MPS_PER_MPH, RDDF_FILE */

#define LADAR_ROOF_SERIAL_PORT 0
#define LADAR_BUMPER_SERIAL_PORT 5

/* These ones are actually defined now in dgc/bob/vehlib/vehports.h. */
//#define GPS_SERIAL_PORT 4
//#define OBD_SERIAL_PORT 6
//#define MAG_SERIAL_PORT 8
//#define STEER_SERIAL_PORT 0
//#define BRAKE_SERIAL_PORT 5

// Distance between the gps in the middle of the rear axle and the middle of the front of the car
#define GPS_2_FRONT_DISTANCE 3.0

// Maximum deceleration (how long it takes to slow down under **full braking**)
// This value is used in GlobalPlanner2 (defunct) and was previously used in 
// the generateArbiterVotes method of genMap.cc, but not anymore
#define MAX_DECEL 3.0 // m/s^2

// Deceleration value used for determining what speed we want to 
// go given an obstacle is some distance in front of us.  This value is used
// in the generateArbiterVotes method of genMap.cc
#define  NOMINAL_DECELERATION  1.0 // m/s/s 

// Maximum steer deflection in radians, from straight, assumed symmetric
// Only used by the simulator. Other modules (Planners) use MAX_STEER defined 
// in RaceModules/Arbiter/SimpleArbiter.hh
#define SIM_MAX_STEER 25.0*M_PI/180.0               

// Maximum steer deflection in radians, from straight, assumed symmetric
// Used by global_functions.[cc/hh] because it causes great headaches to use
// the const double MAX_STEER defined in
// team/RaceModules/Arbiter/SimpleArbiter.hh
//#define USE_MAX_STEER 25.0*M_PI/180.0               
#define  USE_MAX_STEER  VEHICLE_MAX_AVG_STEER

// Vehicle state estimate default variances
// TODO: These are complete guesses right now, 
// and should be properly estimated.
// Northing and Easting variance based on 
//   -> 25cm sigma = 0.0625 m^2
// Downing variance assumed zero (actually rather large because state
// estimator always sends zero altitude right now)
#define DEFAULT_VEHICLE_EASTING_VARIANCE  0.0625
#define DEFAULT_VEHICLE_NORTHING_VARIANCE 0.0625
#define DEFAULT_VEHICLE_DOWNING_VARIANCE  0.0625
// Pitch and roll variance based on 
//   -> 2 degree sigma = 0.00120 rad^2
//   -> 1 degree sigma = 0.00030 rad^2
#define DEFAULT_VEHICLE_PITCH_VARIANCE    0.00120
#define DEFAULT_VEHICLE_ROLL_VARIANCE     0.00120
#define DEFAULT_VEHICLE_YAW_VARIANCE      0.00030

// Default ladar measurement variances 
// Range variance based on
//   -> 10cm sigma = 0.0100 m^2
#define DEFAULT_LADAR_RANGE_VARIANCE      0.0100
// Azimuth variance based on 
//   -> 0.1 degree sigma = 0.000003 rad^2
#define DEFAULT_LADAR_AZIMUTH_VARIANCE    0.000003 
// NOTE: This variable will not affect the results as it is assumed zero
// in the derivation of the code.
#define DEFAULT_LADAR_ELEVATION_VARIANCE  0.0

// PATH EVALUATOR CONSTANTS
#define VECTOR_PROD_THRESHOLD 0.866 // cos(30) = 0.866
#define HEIGHT_DIFF_THRESHOLD 0.5   // 0.5m difference between cells
#define TIRE_OFF_THRESHOLD 1        // 1m from a tire and the plane determined by the other three
#define OBSTACLE_THRESHOLD 70       // bellow this threshold it's considered an obstacle

#define EXP_PARAM 2.0               // Defines the exponential parameter that models the votes
#define MIN_VOTE_RANGE 0.0
#define MAX_VOTE_RANGE 100.0
#define MAX_REACTION_TIME_BETWEEN_SENSOR_SWEEP_AND_BEGINNING_DECELLERATION_FOR_EVALUATING_VELOCITY 0.7 //The velocity evaluattion in getArbiterVotes uses this to evaluate the velocity at which the vehicle can travel along a given arc. We need to arrive at a more accurate estimate of this value.

#endif
