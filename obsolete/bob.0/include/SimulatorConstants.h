// SimulatorConstants.h

// UTM coordinates arround Pasadena
//* This pair is 5m north of the first waypoint in the tennis lot */
//#define SIMULATOR_INITIAL_NORTHING 3777630.0
//#define SIMULATOR_INITIAL_EASTING  396260.0
//#define SIMULATOR_INITIAL_YAW      3.14       

// INITIAL CONDITIONS FOR GEN MAP TEST 1 and 2
//#define SIMULATOR_INITIAL_NORTHING 3777000.0
//#define SIMULATOR_INITIAL_EASTING  396000.0
//#define SIMULATOR_INITIAL_YAW      0

// Initial conditions for qid_translated, waypoints_caltechlot,
// dogleg_right, ...
//#define SIMULATOR_INITIAL_NORTHING 3777604.0
//#define SIMULATOR_INITIAL_EASTING  396260.0
//#define SIMULATOR_INITIAL_YAW      M_PI/2

// initial conditions for darpa sample file
//#define SIMULATOR_INITIAL_NORTHING 3761045.461
//#define SIMULATOR_INITIAL_EASTING   544625.494
//#define SIMULATOR_INITIAL_YAW      0

// initial conditions for rddf qid file
//#define SIMULATOR_INITIAL_NORTHING 3772159.586
//#define SIMULATOR_INITIAL_EASTING   454061.519
//#define SIMULATOR_INITIAL_YAW      0

// initial conditions for sinusoid_50
//#define SIMULATOR_INITIAL_NORTHING 3777260
//#define SIMULATOR_INITIAL_EASTING   396625
//#define SIMULATOR_INITIAL_YAW      0

// initial conditions for race
//--------------------------------------------------
// #define SIMULATOR_INITIAL_NORTHING 3846503
// #define SIMULATOR_INITIAL_EASTING   499015
// #define SIMULATOR_INITIAL_YAW      0
//-------------------------------------------------- 

// initial condition for elmirage_qid first waypoint
//#define SIMULATOR_INITIAL_NORTHING 3834760.000
//#define SIMULATOR_INITIAL_EASTING   442528.000
//#define SIMULATOR_INITIAL_YAW            2.8585

// initial condition for elmirage_qid second waypoint
//#define SIMULATOR_INITIAL_NORTHING 3834650.000
//#define SIMULATOR_INITIAL_EASTING   442560.000
//#define SIMULATOR_INITIAL_YAW            2.8585

// initial condition for santaanita_figureeight.rddf 1st WP
//* #define SIMULATOR_INITIAL_NORTHING  3777419.000  */
//* #define SIMULATOR_INITIAL_EASTING    403530.000 */
//* #define SIMULATOR_INITIAL_YAW            2.8585 */

//#define SIMULATOR_INITIAL_NORTHING 3777260
//#define SIMULATOR_INITIAL_EASTING   396625
//#define SIMULATOR_INITIAL_YAW      0

//INITIAL CONDITIONS FOR SANTA ANITA GRABBED
//#define SIMULATOR_INITIAL_NORTHING 3777426.0
//#define SIMULATOR_INITIAL_EASTING  454072.519
//#define SIMULATOR_INITIAL_NORTHING 3772152.586
//#define SIMULATOR_INITIAL_EASTING  403533.0
//#define SIMULATOR_INITIAL_YAW      -3*M_PI/4

// El Mirage Rectanglular Loop
//#define SIMULATOR_INITIAL_NORTHING 3834760.0
//#define SIMULATOR_INITIAL_EASTING  442528.0
//#define SIMULATOR_INITIAL_YAW      3.14

// Initial conditions for FenderPlanner Testing
//#define SIMULATOR_INITIAL_NORTHING 454076.519
//#define SIMULATOR_INITIAL_EASTING  3772159.586
//#define SIMULATOR_INITIAL_YAW      M_PI_2

// Initial conditions for the DGC QID course
//#define SIMULATOR_INITIAL_NORTHING 3772159.0
//#define SIMULATOR_INITIAL_EASTING  454071.0
//#define SIMULATOR_INITIAL_NORTHING 3772179.0
//#define SIMULATOR_INITIAL_EASTING  453571.0
//#define SIMULATOR_INITIAL_YAW      -M_PI_2

// Initial conditions for the DGC QID course
//#define SIMULATOR_INITIAL_NORTHING 499015.281
//#define SIMULATOR_INITIAL_EASTING  3846508.750
//#define SIMULATOR_INITIAL_YAW      -M_PI_2

//Initial conditons for the CDS110 - Team 4 lateral PID controller
//SA_curve_50cm.traj (Santa Anita) - created on 30/12/04
//#define SIMULATOR_INITIAL_NORTHING 3777313.0
//#define SIMULATOR_INITIAL_EASTING  403407.0
//#define SIMULATOR_INITIAL_YAW      1.6

//Initial conditions for the elmqid.path path - present in the PathFollower
//directory
//#define SIMULATOR_INITIAL_NORTHING   3834759
//#define SIMULATOR_INITIAL_EASTING    442528
//#define SIMULATOR_INITIAL_YAW        3.1

//Initial conditions for the default.traj - in PathFollower Directory
#define SIMULATOR_INITIAL_NORTHING     3777395
#define SIMULATOR_INITIAL_EASTING      403533
#define SIMULATOR_INITIAL_YAW          4.7
