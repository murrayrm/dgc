/****************************************************************************
 *
 * INS/GPS, Global Definitions
 *
 * File: global.h
 * Date: 06/21/00
 *
 ****************************************************************************/
#ifndef GlobalH
#define GlobalH

#define TRUE    1
#define FALSE   0

#define _LOCAL

/* System Definitions *******************************************************/

#define PI        3.141592653589793     // Pi
#define MET2FT    (1.0 / 0.3048)        // Meters to feet conversion
#define A_MET     6378137.0             // A in meters
#define B_MET     6356752.3142          // B in meters
#define RAD_MEAN  6371000.79            // Mean Radius=(A_MET*A_MET*B_MET)^(1/3) 
#define A         (A_MET * MET2FT)      // Earth rad at 0 deg lat, ft
#define B         (B_MET * MET2FT)      // ft (wgs-84)
#define ESQ       0.00669437999013      // wgs-84
#define INV_RAD   (1.0 / A)             // inv rad at 0 deg lat, 1/ft
#define INV_RAD_M (1.0 / A_MET)         // inv rad at 0 deg lat, 1/met
#define FEARTH    (0.00335281066474)    // Earth flattening
#define OMEGA     0.0000729211506       // Earth rate (rad/sec)
#define GM        3986005e8 * MET2FT * MET2FT * MET2FT // GM in ft**3/sec**2
#define KM        3986005e+8            // WGS-84 KM, m**3/sec**2
#define J2        sqrt(5.0) * 484.16685e-6 // J2

#define DT1KHZ    (1.0 / 1000.0)        // 1 /  1 KHz
#define DT500HZ   (1.0 / 500.0)         // 1 /  500 Hz
#define DT100HZ   (1.0 / 100.0)         // 1 /  100 Hz
#define DT1HZ     (1.0 /  1.0)          // 1 /   1 Hz
#define RPS2DPH   ((180.0 / PI) * 3600.0) // rad/sec to deg/hr conv
#define DEG2RAD   (PI / 180.0)          // deg to rad conversion
#define RAD2DEG   (180.0 / PI)          // rad to deg conversion
#define DPH2RPS   ((PI / 180.0) / 3600.0) // deg/hr to rad/sec conv
#define FPSSQ2UG  (1000000.0 / 32.14)   // ft/sec**2 to ug conversion
#define UG2FPSSQ  (32.14 / 1000000.0)   // ug to ft/sec**2 conversion
#define MPSSQ2UG  (1000000.0 / 9.796)   // m/sec**2 to ug conversion
#define UG2MPSSQ  (9.796 / 1000000.0)   // ug to m/sec**2 conversion
#define KF_DT     1.0                   // Kalman Filter Dt
#define UPD       (3600.0/KF_DT)        // Kalman updates per hour
#define GPSMEASDT 1.0                   // GPS measurement interval

#define GWH       9.79667               // local gravity, met/sec**2
#define LATWH     34.1706944            // local latitude, deg
#define LONWH     -118.58736111         // local longitude, deg
#define ALTWH     265.0                 // local altitude, met

#define CLK_RATE  1000.0                // clock rate, intrps/sec

/* Mode Controller Definitions **********************************************/

//#define LEVELING   0                    // inu modes
//#define ALIGNMENT  1
//#define NAVIGATION 2

//#define COARSE_GROUND       0           // align modes
//#define FINE_GROUND         1
//#define INTER_COARSE_GROUND 2
//#define INTER_FINE_GROUND   3

//#define SPARE1         0                // commanded modes
//#define AUTO           1
//#define SLAVED_HEADING 2
//#define FREE_HEADING   3
//#define GC_ALIGN       4
//#define CMD_LEVELING   5
//#define SPARE2         6
//#define IBIT           7

/* Commanded Vertical Channel Status Definitions ****************************/

//#define SLAVED 0
//#define FREE   1

/* Motion Detector Status Definitions ***************************************/

//#define STATIONARY 0
//#define TAXIING    1
//#define FLYING     2

/* Navigation Processor Position Correction Definitions *********************/

//#define NAV_POS 0
//#define DIR_COS 1
//#define NAV_ANG 2

/* GPS Definitions **********************************************************/

//#define UNKNOWN   0
//#define NOT_KEYED 1
//#define KEYED     2

/* System Error Estimator State Definitions *********************************/

//#define NOT_LEVELED     0
//#define PRE_LEVELED     1
//#define LEVELED         2
//#define COARSE_ALIGN    3
//#define FINE_ALIGN      4

/* Kalman Mode Definitions **************************************************/

//#define KALMAN_STARTUP     0
//#define KALMAN_COARSE      1
//#define KALMAN_COARSE_FINE 2
//#define KALMAN_FINE        3
//#define KALMAN_REVERSION   4

/* Kalman Filter State Definitions ******************************************/

#define NUM_ST 29                       // Number of Kalman filter states
//#define NUM_GPS_ST 13                 // Number of GPS Kalman filter states
//#define KAL_MOI_CNT 250
//#define KAL_EOI_CNT 500
//#define KAL_ZV_MEAS_TIME 500          // Time of zero vel measurement
//#define MAX_NO_DATA 500
//#define BARO_MIN_NOISE 30.48          // meters

#define IPX    1
#define IPY    2
#define IH     3
#define IVX    4
#define IVY    5
#define IVZ    6
#define ITX    7
#define ITY    8
#define ITZ    9
#define IGBX   10
#define IGBY   11
#define IGBZ   12
#define IGSFX  13
#define IABX   14
#define IABY   15
#define IABZ   16
#define IASFX  17
#define IASFY  18
#define IASFZ  19
#define ICPH   20
#define ICF    21
#define ICGX   22
#define ICGY   23
#define ICGZ   24
#define IBB    25
#define IBSF   26
#define IBSX   27
#define IBSY   28
#define IBSZ   29

#define IWAERX 10
#define IWAERY 11

//#define IGPSPX  1
//#define IGPSPY  2
//#define IGPSH   3
//#define IGPSVX  4
//#define IGPSVY  5
//#define IGPSVZ  6
//#define IGPSAX  7
//#define IGPSAY  8
//#define IGPSAZ  9
//#define IGPSCPH 10
//#define IGPSCF  11
//#define IGPSBB  12
//#define IGPSBSF 13

/****************************************************************************
 * End
 ****************************************************************************/
#endif
