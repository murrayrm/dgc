#ifndef LADARBUMPER_HH
#define LADARBUMPER_HH


#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include "MTA/Misc/Time/Time.hh"
#include "LADARBumperDatum.hh"

#ifdef _PLAYER_ // build with gazebo simulation support
#include "playerclient.h"
#endif

using namespace std;
using namespace boost;


class LADARBumper : public DGC_MODULE {
public:
  // and change the constructors
  LADARBumper();
  ~LADARBumper();

  // The states in the state machine.
  // Uncomment these if you wish to define these functions.

  void Init();
  void Active();
  //  void Standby();
  void Shutdown();
  //  void Restart();

  // The message handlers. 
  //  void InMailHandler(Mail & ml);
  //  Mail QueryMailHandler(Mail & ml);

  // the status function.
  //string Status();


  // Any functions which run separate threads
  
  // Method protocols
  int  InitLadar();
  void LadarUpdateVotes();
  void LadarShutdown();

  void UpdateState();

  void SparrowDisplayLoop();
  void UpdateSparrowVariablesLoop();

private:
  // and a datum named d.
  LADARBumperDatum d;
  
#ifdef _PLAYER_
  // For simulation
  PlayerClient* playerClient;
  LaserProxy* ladarSim;
  /*! Initializes the Player client and connects to the player server
   * \return true if all went ok. */
  bool initPlayer();
  /*! Reads the config file with host and port */
  void readConfig();
  /*! the host to connect to the Player Server at */
  string host;
  /*! the port to connect to the Player Server at */
  int port;
#endif

};



// enumerate all module messages that could be received
namespace WaypointNavMessages {
  enum {
    // we are not yet receiving messages.
    // A place holder
    NumMessages
  };
};






#endif
