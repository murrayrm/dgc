#include "LADARBumper.hh"
//#include "ladar.hh"
#include "laserdevice.h"
#include "ladar_power.h" // for LADAR_BUMPER
#include "Arbiter2/ArbiterDatum.hh" // for GetPhi
#include "frames/frames.hh"
#include "Constants.h" //serial ports, other constants
#include <iostream.h>
#include <fstream.h>
#include <float.h>

// FIXME: Eventually we want to load these from a config file or
// something Note: some of the code below will probably break if the
// scan_angle is not 180!!!!
#define BUMPER_LADAR_SCAN_ANGLE 180
#define BUMPER_LADAR_RESOLUTION 0.5
#define BUMPER_LADAR_UNITS 0.01

// the radius we will grow each scanpoint by.
// FIXME: maybe make this some function of vehicle constants??
#define POINT_GROW_RADIUS 1.5 //[m]

// the distance along an arc we have to be able to drive to set 100 goodness.
#define ARC_MAX_DIST BUMPER_LADAR_MAX_RANGE //[m]
#define EPSILON .0000000001  // 10^-10.  assume below this is zero.

double arc_goodness_based_on_point_vehicle( double, double*, int );
void larsGrowObsCode( double*, double*, int);
double goodnessByCircCircInt( double, double*, int);
double steerAngle2ArcRadius( double );

extern int SIM;         // get the simulation flag from the main function
extern int LOG_SCANS;
extern int RUNTYPE;
extern int QUIT_PRESSED;
extern char *TTY;
extern char *TESTNAME;
extern int GROW_OBSTACLES; // do we want to grow the obstacles

//extern int PRINT_VOTES; // get the print_votes flag from the main function

FILE* scanlog;           // Log files for various data
FILE* errorlog;          // Records any errors reported by the scanner
FILE* statelog;
FILE* votelog;

// LADAR Driver class
CLaserDevice bumper_laser;

// Constants for printing the Gazebo simulated rays
const int NUMROWS = 20;
const int NUMCOLS = 20;

/*****************************************************************************/
/*****************************************************************************/
int LADARBumper::InitLadar() {
  int result;
  char filename[255];
  char datestamp[255];

  // Open log file if LOG_SCANS is set
  if( LOG_SCANS ) {
    // filename will be TESTNAME_ddmmyyyy_hhmmss.log
    time_t t = time(NULL);
    tm *local;
    local = localtime(&t);
    int i;

    sprintf(datestamp, "%02d%02d%04d_%02d%02d%02d",
            local->tm_mon+1, local->tm_mday, local->tm_year+1900,
            local->tm_hour, local->tm_min, local->tm_sec);

    sprintf(filename, "testRuns/%s_scans_%s.log", TESTNAME, datestamp);
    printf("Logging scans to file: %s\n", filename);
    scanlog=fopen(filename,"w");
    if ( scanlog == NULL ) {
      printf("Unable to open scan log!!!\n");
      exit(-1);
    }
    // print data format information in a header at the top of the scanlog file
    fprintf(scanlog, "%% angle = %d\n", BUMPER_LADAR_SCAN_ANGLE);
    fprintf(scanlog, "%% resolution = %lf\n", BUMPER_LADAR_RESOLUTION);
    fprintf(scanlog, "%% units = %lf\n", BUMPER_LADAR_UNITS);
    fprintf(scanlog, "%% Time[sec] | scan points\n");

    sprintf(filename, "testRuns/%s_state_%s.log", TESTNAME, datestamp);
    printf("Logging state to file: %s\n", filename);
    statelog=fopen(filename,"w");
    if ( statelog == NULL ) {
      printf("Unable to open state log!!!\n");
      exit(-1);
    }
    // print data format information in a header at the top of the state file
    fprintf(statelog, "%% Time[sec] | Easting[m] | Northing[m] | Altitude[m]");
    fprintf(statelog, " | Vel_E[m/s] | Vel_N[m/s] | Vel_U[m/s]" );
    fprintf(statelog, " | Speed[m/s] | Accel[m/s/s]" );
    fprintf(statelog, " | Pitch[rad] | Roll[rad] | Yaw[rad]" );
    fprintf(statelog, " | PitchRate[rad/s] | RollRate[rad/s] | YawRate[rad/s]");
    fprintf(statelog, "\n");

    sprintf(filename, "testRuns/%s_votes_%s.log", TESTNAME, datestamp);
    printf("Logging votes to file: %s\n", filename);
    votelog=fopen(filename,"w");
    if (votelog==NULL) {
      printf("Unable to open vote log!!!\n");
      exit(-1);
    }

    fprintf(votelog, "%% phi ");
    for( i=0; i<NUMARCS; i++) {
      fprintf(votelog, "%f ", GetPhi(i));
    }
    fprintf(votelog, "\n");
    fprintf(votelog, "%% tstamp (v1,g1) (v2,g2) .... (vn,gn)\n");
    
    sprintf(filename, "testRuns/%s_errors_%s.log", TESTNAME, datestamp);
    printf("Logging errors to file: %s\n", filename);
    errorlog=fopen(filename,"w");
    if ( errorlog == NULL ) {
      printf("Unable to open error log!!!\n");
      exit(-1);
    }
  }

  // Ladar initialization tasks
  if( !SIM ) { // use this if you truly want to simulate
    while( bumper_laser.Setup(TTY,BUMPER_LADAR_SCAN_ANGLE,
                              BUMPER_LADAR_RESOLUTION, LADAR_BUMPER) != 0 ) {
      sleep(1);
    }
    return true;
  } 
  else {
    #ifdef _PLAYER_
    return initPlayer();    
    #else
    printf("You must compile with _PLAYER_ flag to use simulation with Gazebo\n");
    printf("Exiting...");
    exit(-1);
    #endif
  }  
  return true;
  
} // end LADARBumper::InitLadar()

/*****************************************************************************/
/*****************************************************************************/
void LADARBumper::LadarUpdateVotes() {
  double *laserdata;
  double *growndata;
  int i;
  double tstamp;
  int numScanPoints;
  
  if(SIM) {
#ifdef _PLAYER_
    numScanPoints = ladarSim->scan_count;
#else
    numScanPoints =  (int)(BUMPER_LADAR_SCAN_ANGLE / BUMPER_LADAR_RESOLUTION + 1);
#endif
  } else {
    numScanPoints = bumper_laser.getNumDataPoints();
  }
  
  laserdata = (double *) malloc(numScanPoints * sizeof(double));
  growndata = (double *) malloc(numScanPoints * sizeof(double));
  
  /* Steps - 1. Take a scan from the ladar.
     2. Grow the obstacles if necessary.
     3. Compute the votes for the various steering angles
  */
  // Log the vehicle state
  if (LOG_SCANS) {
    tstamp = (double)(d.SS.Timestamp.sec()) + d.SS.Timestamp.usec()/1000000.0;
    fprintf(scanlog, "%f ", tstamp);
    // Print state      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 
    fprintf(statelog, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",
	    tstamp, d.SS.Easting, d.SS.Northing, d.SS.Altitude,
	    d.SS.Vel_E, d.SS.Vel_N, d.SS.Vel_U,
	    d.SS.Speed, d.SS.Accel, 
	    d.SS.Pitch, d.SS.Roll, d.SS.Yaw, 
	    d.SS.PitchRate, d.SS.RollRate, d.SS.YawRate );
    fflush(statelog);
  }
  
  // Step 1 - Take a scan
  if( !SIM ) { 
    int status = bumper_laser.UpdateScan();
    if (LOG_SCANS) {
      fprintf(errorlog,"%f 0x%04X\n", tstamp, status);
      fflush(errorlog);
    }
    // update local data buffer
    for(int i = 0; i < numScanPoints; i++) {
      if (LOG_SCANS) {
        fprintf(scanlog,"%d ", bumper_laser.Scan[i]);
      }
      if(bumper_laser.isDataError(status,bumper_laser.Scan[i]) == LADAR_DATA_BAD) {
	laserdata[i] = LADAR_BAD_SCANPOINT;
	growndata[i] = laserdata[i];
      }
      else {
	laserdata[i] = (bumper_laser.Scan[i] * BUMPER_LADAR_UNITS);
	growndata[i] = laserdata[i];
      }
    }
  }
  // or simulate a scan with Gazebo
  else {
#ifdef _PLAYER_
    if(playerClient->Read() < 0) // Read new data from Gazebo
    {
      cout << "PlayerClient.Read() failed! Terminating..." << endl;
      exit(-1);
    }
    for(int i=0; i < numScanPoints ;i++)  
    {
      laserdata[i] = ladarSim->scan[i][0];
      growndata[i] = laserdata[i];
    }
    //ladarSim->Print(); // For debug printout of config and the rays.
        
    // Debug: print ladar rays in a simple ascii interface.
    // for display stuff
    system("clear"); // clear the screen
    printf("readCount: %d\n", d.readCount++);
    // go through each line and print graphical display of ladar scans
    int scannum = 0; // which ladar scanpoint do we want to display?
    int slength = 0; // length of scanpoint display for each row
    for( int i = NUMROWS-1; i >= 0; i-- ) {
      // determine which scan approximates the row we're printing to
      scannum = (int)round( (double)i / (NUMROWS - 1) * numScanPoints );
      // determine how many characters to display for each scanpoint
      slength = (int)round( laserdata[scannum] / 25 * NUMCOLS ); 
      for( int j = 0; j < slength; j++ ) {
        printf("*");
      }
      printf("\n");
    }
    
    if (LOG_SCANS) {
      for (int i = 0; i < numScanPoints; i++) {
        fprintf(scanlog,"%d ", (int) (laserdata[i] * 100.0) );
      }
    }
#endif
  }
   // end getting new scan
  if (LOG_SCANS) fprintf(scanlog,"\n");
  if (LOG_SCANS) fflush(scanlog);
  
  /* ***************************************************** */
  /* ***************************************************** */
  //Step 2 - IF WE WANT TO GROW THE OBSTACLES
  if(GROW_OBSTACLES) {
    larsGrowObsCode(laserdata, growndata, numScanPoints);
  } // END IF WE WANT TO GROW THE OBSTACLES
  
  /* ***************************************************** */
  /* ***************************************************** */
  
  // Step 3 - figure out the votes...
  
  if(LOG_SCANS) {
    fprintf(votelog, "%f ", tstamp);
  }
  
  for( i=0; i<NUMARCS; i++) {
    d.ladar.Votes[i].Goodness = arc_goodness_based_on_point_vehicle( GetPhi(i), 
								     growndata, 
								     numScanPoints );
    //d.ladar.Votes[i].Goodness = goodnessByCircCircInt(GetPhi(i), laserdata, numScanPoints);

    
    d.ladar.Votes[i].Velo     = (d.ladar.Votes[i].Goodness / MAX_GOODNESS) * MAX_SPEED; 
    if(LOG_SCANS) {
      fprintf(votelog, "(%lf,%lf) ", d.ladar.Votes[i].Velo, d.ladar.Votes[i].Goodness);
    }
  } // end iterating over votes/arcs
  
  if(LOG_SCANS) fprintf(votelog, "\n");
  if(LOG_SCANS) fflush(votelog);
  
  free(laserdata);
  free(growndata);
} // end LADARBumper::LadarUpdateVotes()


void LADARBumper::LadarShutdown() {
  fclose(scanlog);
  fclose(statelog);
  bumper_laser.LockNShutdown();  // new way  
}

/*****************************************************************************/
/*****************************************************************************/
/* this method considers each scanpoint one at a time.  It takes the
   circle with that scanpoint at the center with a radius defined by
   some size parameter of the vehicle and the circle defined by a
   steering angle.  If these circles intersect then we would run into
   that point.  If a steering arc doesn't intersect any of these
   circles then that arc is great!
*/
double goodnessByCircCircInt(double phi, double* laserdata, int numScanPoints) 
{
  double goodness;                      // the value we will return
  double arcRadius;                     // r0
  double offset;
  double dist, a, h, dy, dx, theta, scanangle, arcTravelDist;
  XYZcoord scanpoint(0,0,0);            // P1
  XYZcoord arcCenter(0,0,0);            // P0
  XYZcoord tmp(0,0,0);                  // P2
  XYZcoord n(0,0,0);                    // for the intersection points
  
  int scanFlag;                         // flag that is 0 if we dont have
				        // any good scanpoints.
  arcRadius = steerAngle2ArcRadius( phi );       // r0
  // Starting angle of scan
  offset = ((180.0 - BUMPER_LADAR_SCAN_ANGLE) / 2.0) * M_PI / 180.0;

  // set goodness to zero and modify it below
  goodness = 0;
  arcTravelDist = DBL_MAX;

  if ( arcRadius == 0 ) {
    // straight ahead we have to intersect line and circle instead
    for ( int i = 0; i < numScanPoints; i++ ) {
      scanangle = offset + (i * BUMPER_LADAR_RESOLUTION * M_PI) / 180.0;
      // the perp dist from scanpt to centerline (will be negative for
      // pts to left of center)
      h = laserdata[i] * cos(scanangle); 
      if ( fabs(h) > POINT_GROW_RADIUS || laserdata[i] == LADAR_BAD_SCANPOINT) {
	//this point is far enough away to miss it.
	//or its bad data
	continue;
      }
      a = sqrt( pow(POINT_GROW_RADIUS,2) - pow(h,2) );
      dist = sqrt( pow(laserdata[i],2) - pow(h,2) );
      arcTravelDist = fmin(dist - a, arcTravelDist);
    } // end looping through scanpoints
  } // end if arcRadius == 0
  
  else {
    // see if the steering arc intersects the grown point
    for( int i = 0; i < numScanPoints; i++ ) {
      if ( laserdata[i] > BUMPER_LADAR_MAX_RANGE ) {
	continue;
      }
      scanangle = offset + (i * BUMPER_LADAR_RESOLUTION * M_PI) / 180.0;
      arcCenter.x = 0;                           // P0
      arcCenter.y = arcRadius;                   // y is to the right, +radius is rt turn.
      scanpoint.x = laserdata[i] * sin(scanangle);     // P1
      scanpoint.y = laserdata[i] * cos(scanangle);
      
      // the distance between the circle centers
      dist = sqrt( pow( (arcCenter.x - scanpoint.x),2) + 
		   pow( (arcCenter.y - scanpoint.y),2) );
      
      a = ( pow(arcRadius,2) - pow(POINT_GROW_RADIUS,2) + pow(dist,2) ) 
	/ ( 2 * dist ); 
      tmp.x = arcCenter.x + a * ( scanpoint.x - arcCenter.x ) / dist;
      tmp.y = arcCenter.y + a * ( scanpoint.y - arcCenter.y ) / dist;
      h = sqrt( pow(arcRadius,2) + pow(a,2) );
      
      if ( dist > fabs(arcRadius) + POINT_GROW_RADIUS ) {
	// the point is too far away for the arc to intersect the
	// grown point.  skip this one and go to the next point.
	continue;
      }      
      else if ( dist == fabs(arcRadius) + POINT_GROW_RADIUS ) {
	// the circles are tangent to each other -> one point of
	// intersection
	n.x = tmp.x;
	n.y = tmp.y;
	dx = n.x;
	dy = n.y - arcCenter.y;
	theta = atan2(dx,dy);  //yes, x and y are backwards here

	if ( dx < 0 ) {
	  // the intersection point is _behind_ the sensor this would
	  // be really odd with only one pt of intersection...
	  return goodness = 0.0;
	}
	arcTravelDist = fmin(arcTravelDist, fabs(arcRadius) * theta);
       	
      }  // end single intersection point.
      else {
	//they intersect at 2 points.
	for ( int j = 1; j <= 3; j +=2 ) {
	  n.x = tmp.x - (j-2) * h * ( scanpoint.y - arcCenter.y ) / dist;
	  n.y = tmp.y + (j-2) * h * ( scanpoint.x - arcCenter.x ) / dist;
	  dx = n.x;
	  dy = n.y - arcCenter.y;
	  theta = atan2(dx,dy);  //yes, x and y are backwards here
	  if ( dx < 0 ) {
	    // the intersection point is _behind_ the sensor this would
	    // be really odd with only one pt of intersection...
	    return goodness = 0.0;
	  }
	  arcTravelDist = fmin(arcTravelDist, fabs(arcRadius) * theta);
	}
      }  // end 2 intersection points
    } // end looping thru each scanpoint
  } // end if arcRadius==0; else...
  // Here we should have the minimum arcTravelDist
  if ( arcTravelDist == DBL_MAX ) {
    return goodness = MAX_GOODNESS;
  }
  else {
    return goodness = fmin( (arcTravelDist / ARC_MAX_DIST), 1.0 ) * MAX_GOODNESS;
  }
}

/*****************************************************************************/
/*****************************************************************************/
void larsGrowObsCode( double* laserdata, double* growndata, int numScanPoints) {
  
  // we don't want to assume that the car is a point, so here is our way
  // to grow obstacles. 
  double d_grow = 1.0;
  double alpha_grow; // the angle (radians) in each direction that we grow the obstacle
  // this will depend on the distance of the obstacle 
  int alpha_grow_num;  // number of scanpoints that correspond to a given angle
  int index_a, index_b;
  
  double bleed_fraction = 0.2; // fraction of alpha_grow_num that we want to further grow 
  // obstacles, but fade them out, so that it's better to be farther from an obstacle
  int index_fl, index_fr; // far left and far right scanpoint indices for growing obstacles
  double fade_rate = 0.1; // rate (in meters per scanline) to fade the obstacle
  
  // here is the code that (effectively) grows the obstacles
  // iterate through each scanpoint and modify the neighboring scanpoints 
  //for(int i = 0; i < numScanPoints; i++) {
  // HACK: Had problem that farthest right scanpoint was reading a small value and 
  // growing this obstacle all the way past straight (laserdata[0]=0.90)
  // hack is to iterate from scanpoint 1 instead...
  for(int i = 1; i < numScanPoints; i++) {
    
    // scan distance is in laserdata[i]. Note by klk 2004-03-03: The
    // newest driver now can give info if the scanpoint is bad.
    // Above, we set laserdata[i] = -1 if it is LADAR_DATA_BAD.  This
    // code should still work, since alpha_grow_num will = 0, and it
    // won't grow the point at all.  growndata[i] will probably end up
    // -1 as well...  but I'm not sure of that one.


    if( laserdata[i] > 0 ) {
      
      // alpha_grow is the angle on either side of the obstacle that
      // we want the obstacle to grow to
      alpha_grow =  d_grow / laserdata[i]; // radians
      // number of scanpoints on each side to potentially modify
      alpha_grow_num = (int)((alpha_grow / BUMPER_LADAR_SCAN_ANGLE) * numScanPoints );
    }
    else {
      alpha_grow_num = 0;
    }
    // set the indices of the farthest neighbors that scan "i" will affect 
    index_a = max( 0, i - alpha_grow_num ); 
    index_b = min( numScanPoints, i + alpha_grow_num ); 
    
    // STILL IN DEVELOPMENT
    index_fr = max( 0, (int)(i - (int)alpha_grow_num*(1+bleed_fraction)) );
    index_fl = min( numScanPoints, (int)(i + (int)alpha_grow_num*(1+bleed_fraction)) ); 
    
    for( int j = index_a; j < index_b; j++ ) {
      // original growth algorithm
      growndata[j] = fmin( laserdata[i], growndata[j] ); 
      // new algorithm... let obstacles fade away as they're grown
      // this did not work very well!
      //growndata[j] = fmin( laserdata[i]+fade_rate*abs(i-j), growndata[j] ); 
    }
    
    //STILL IN DEVELOPMENT
    
    // make the obstacles fade away linearly
    // for the scanlines at far right
    for( int j = index_fr; j < index_a; j++ ) {
      growndata[j] = fmin( laserdata[i]+fade_rate*abs(j-index_a), growndata[j] );
    }
    // for the scanlines at far left
    for( int j = index_b; j < index_fl; j++ ) {
      growndata[j] = fmin( laserdata[i]+fade_rate*abs(j-index_b), growndata[j] );
    }
    /**/
    //printf("alpha_grow[%d] = %4.2f   and   alpha_grow_num[%d] = %d\n", 
    //       i, alpha_grow, i, alpha_grow_num );
    
  } // end iterating over scanpoints
  
}
/*****************************************************************************/
/*****************************************************************************/
double arc_goodness_based_on_point_vehicle( double phi, double * rangedata, int numScanPoints ) {

  // DEBUGGING ONLY
  int midpoint = (numScanPoints - 1)/2;
  int ix = (numScanPoints - 1)/2;
  
  double arc_radius = steerAngle2ArcRadius(phi);
  // arc_radius is positive for right turns, neg for left turns,
  // and ZERO straight ahead

  // set the default to be the maximum goodness
  double goodness;
  int flag;
  // the range of scanline angle is [0,pi] (radians)
  // and is measured zero directly to the right
  // THIS WILL NOT WORK IF YOUR ANGULAR FOV IS NOT PI
  double scanline_angle;
  double arc_scan_intersection = 0;
  double arc_drive_dist = 10000; // set to some large value

  // TODO: note - some of the numbers below depend on what mode
  // the ladar is in, ie the number of scanlines depends on if you
  // are in .5deg mode, 1deg mode, or .25 deg mode.  these should
  // probably be #defined somewhere, but for now they'll just he
  // hardcoded in here.

  // set goodnes to max, and we will reduce it appropriately if
  // we can't drive the whole arc
  goodness = MAX_GOODNESS;
  flag = 0;
  
  // straight ahead, special case
  if ( arc_radius == 0 ) {
    if ( rangedata[midpoint] == LADAR_BAD_SCANPOINT ) {
      return goodness = NO_CONFIDENCE;
    }
    //printf("arc_radius==0 flag==1\n");
    flag = 1;
    if ( rangedata[midpoint] < BUMPER_LADAR_MAX_RANGE ) {
      // set the goodness proportional to the scan distance straight ahead
      goodness = fmin( (rangedata[midpoint] / BUMPER_LADAR_MAX_RANGE), 1.0 ) * MAX_GOODNESS;
    }
    
  }
  else if ( arc_radius > 0 ) {
    // right turn, so only use scan lines on the right side scan
    // line # 181 is the "middle" -> index *180* since arrays start
    // at zero

    // start at the middle and work outwards so as soon as we find
    // a point that is too close, we can break
    for ( int i = (midpoint-1); i >= 0; i-- ) {

      if ( rangedata[i] == LADAR_BAD_SCANPOINT ) {
	// go to the next scanpoint.  if we go thru them all, goodness
	// will be NO_CONFIDENCE
	continue;
      }
      //if we got to here, we found a good scan to look at for this
      //arc.  Set a flag.
      flag = 1;
      // find the distance at which the scan line intersects the
      // steering arc
      scanline_angle = i * BUMPER_LADAR_RESOLUTION;
      // from the law of cosines, c^2 = a^2 + b^2 - 2*a*b*cos(C)
      arc_scan_intersection = sqrt(2.0)*arc_radius*sqrt(1-cos(M_PI-2*scanline_angle));

     // there's a scan return between us and where the arc takes us
      // modify the goodness appropriately...
      if ( arc_scan_intersection >= rangedata[i] ) {

	// make the goodness proportional to the direct range to the obstacle along the arc
	// one choice of goodness assignment.  others may be better.
	goodness = fmin( (rangedata[i] / BUMPER_LADAR_MAX_RANGE), 1.0 ) * MAX_GOODNESS;

	// DEBUGGING
	ix = i;

	break;
      }

    } //end for ( int i = (midpoint-1); i >= 0; i-- )

  } // end else if ( arc_radius > 0 )
  else {
    // left turn, so only use scan lines on the left side
    // note, arc_radius will be negative here.

    for ( int i = (midpoint+1); i < numScanPoints; i++ ) {
      // same as above basically, start at middle and work
      // outwards with the scanlines.
      
      if ( rangedata[i] == LADAR_BAD_SCANPOINT ) {
	// go to the next scanpoint.  if we go thru them all, goodness
	// will be NO_CONFIDENCE
	continue;
      }
      //if we got to here, we found a good scan to look at for this
      //arc.  Set a flag.
      flag = 1;

      // on the left hand side the sla is calculated slightly differently
      scanline_angle = M_PI - i * BUMPER_LADAR_RESOLUTION;
      // from the law of cosines, c^2 = a^2 + b^2 - 2*a*b*cos(C)
      arc_scan_intersection = sqrt(2.0)*fabs(arc_radius)*sqrt(1-cos(M_PI-2*scanline_angle));

      //printf("i=%d, sla = %6.3f, arc_scan_ix= %6.3f, arc_rad= %6.3f,
      //phi=%6.3f \n", i, scanline_angle, arc_scan_intersection,
      //arc_radius );
      
      // there's something between us and where the arc takes us
      // modify the goodness appropriately...
      if ( arc_scan_intersection >= rangedata[i] ) {
	// make the goodness proportional to the direct range to the obstacle along the arc
	// one choice of goodness assignment.  others may be better.
	goodness = fmin( (rangedata[i] / BUMPER_LADAR_MAX_RANGE), 1.0 ) * MAX_GOODNESS;
	// DEBUGGUNG
	ix = i;

	break;
      }

    } //end looping thru left half scan lines

  } //end the if arc_radius == or <= or >= checks
  
  if ( flag == 0 ) {
    //for this arc, all the scans were LADAR_BAD_SCANPOINT
    goodness = NO_CONFIDENCE;
  }
  
  return goodness;
  
} // end int arc_goodness_based_on_point_vehicle(...)

double steerAngle2ArcRadius( double phi )
{
  // NOTE : requirement: for steering angle of 0 (moving straight ahead,
  //          instead of returning an infinite arc radius,
  //          return a value of ZERO

  // radius is positive for right turns, neg for left turns, and ZERO
  // straight ahead
  
  if (fabs(phi) < EPSILON )
    return 0;
  else
    {
      return VEHICLE_AXLE_DISTANCE / tan(phi);
    }
  return 0;
}

#ifdef _PLAYER_
bool LADARBumper::initPlayer() 
{
    // Initialize Gazebo LADAR simulation
    readConfig();
    // Connect the PlayerClient
    playerClient = new PlayerClient(); 
    cout << "PlayerClient.Connect: connecting" << endl;
    if(playerClient->Connect(host.c_str(),port) != 0)
    {
      cout << "PlayerClient.Connect: connect failed" << endl;
      return false;
    }
    cout << "PlayerClient.Connect: connect successful!" << endl;
    // Set the data mode to PLAYER_DATAMODE_PUSH_ALL
    // With this mode we better make sure we read from Player faster than 10Hz
    // or we will start filling up the buffers.
    cout << "PlayerClient.SetDataMode: setting data mode = PLAYER_DATAMODE_PUSH_ALL" << endl;
    if(playerClient->SetDataMode(PLAYER_DATAMODE_PUSH_ALL))
    {
      cout << "PlayerClient.SetDataMode: failed! " << endl;
    }
    cout << "PlayerClient.SetDataMode: successful! " << endl;
    
    // Now startup the LaserProxy
    ladarSim = new LaserProxy(playerClient, 0, 'c');
    unsigned char access;
    cout << "LADAR: subscribing (read)" << endl;
    if((ladarSim->ChangeAccess(PLAYER_READ_MODE,&access) < 0) ||
       (access != PLAYER_READ_MODE))
    {
       cout << "LADAR: DRIVER: " << ladarSim->driver_name << endl
       << "LADAR: access: " << access << endl;
       return false;
    }
    cout << "LADAR: DRIVER: " << ladarSim->driver_name << endl;
    ladarSim->Print();
    return true;
}
#endif
