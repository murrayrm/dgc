#include "LADARBumper.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include <unistd.h>
#include <iostream>
#include <fstream> //ifstream

int QUIT_PRESSED = 0;

extern int SEND_VOTES;
extern unsigned long SLEEP_TIME;
extern int SPARROW_DISP;
extern int NOSTATE;
extern int SIM;

LADARBumper::LADARBumper() 
  : DGC_MODULE(MODULES::LADARBumper, "LADAR Bumper", 0) {}

LADARBumper::~LADARBumper() 
{
#ifdef _PLAYER_
  if(SIM) 
  {
    // unsubscribe the ladar
    unsigned char access;
    cout << "unsubscribing ladar (read)" << endl;
    if((ladarSim->ChangeAccess(PLAYER_CLOSE_MODE,&access) < 0) ||
        (access != PLAYER_CLOSE_MODE))
      cout << "failed to unsubscribe ladar" << endl;
    else
      cout << "successfully unsubscribed ladar" << endl;
    delete ladarSim;
    delete playerClient;
  }
#endif
}


void LADARBumper::Init() {
  // call the parent init first
  DGC_MODULE::Init();
  if(InitLadar() != true) 
  {
    exit(-1);
  }
  cout << "Module Init Finished" << endl;

  cout << "About to start Sparrow display" << endl;
  if(SPARROW_DISP) {
    cout << "Starting Sparrow display" << endl;
    RunMethodInNewThread<LADARBumper>( this, &LADARBumper::SparrowDisplayLoop);
    RunMethodInNewThread<LADARBumper>( this, &LADARBumper::UpdateSparrowVariablesLoop);
  }
}

void LADARBumper::Active() {

  cout << "Starting Active Function" << endl;
  while( ContinueInState() && !QUIT_PRESSED) {
    // update the State Struct
    if(!NOSTATE) {
      UpdateState();
      if(d.SS.Northing == 0) {
        cout << endl << "UNABLE TO COMMUNICATE WITH VSTATE" << endl;
	cout << "NORTHING IS ZERO, PLEASE CHECK OR" << endl;
        cout << "RUN WITH -nostate SWITCH IF THIS IS OK" << endl << endl;
        exit(-1);
      }
    }
    // Update the votes and send them to the arbiter
    LadarUpdateVotes();
    
    if(SEND_VOTES) {
      Mail m = NewOutMessage(MyAddress(), 
			     MODULES::Arbiter, 
			     ArbiterMessages::Update);
      // Send the arbiter our "ID" and the vote struct.  The weights
      // are taken care of by the arbiter.
      int myID = ArbiterInput::LADAR;
      m << myID << d.ladar;
      SendMail(m);
    }
    
    if(SPARROW_DISP) UpdateSparrowVariablesLoop();

    // and sleep for a bit
    usleep_for(SLEEP_TIME);
  }

  cout << "Finished Active State" << endl;

}

void LADARBumper::Shutdown() {
  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;
  LadarShutdown();

}
#ifdef _PLAYER_
void LADARBumper::readConfig() {
  // set the defaults:
  host = PLAYER_GAZEBO_HOST;
  port = PLAYER_GAZEBO_PORT;
  
  cout << "LADARBumper::readConfig: reading file " << CONFIG_FILENAME << endl;
  ifstream file(CONFIG_FILENAME);
  if(!file.is_open())
  {
    cout << "LADARBumper::readConfig: Cannot read file " << CONFIG_FILENAME
         << "using defaults = " << host << ":" << port << endl;
    return;
  }
  // if a file was found, use those settings instead:
  string line;
  getline(file, line);
  host = line;
  getline(file, line);
  port = atoi(line.c_str());

  cout << "LADARBumper::readConfig: read host:port = "
       << host << ":" << port << endl;
}
#endif
