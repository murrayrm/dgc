#include "LADARBumper.hh"
#include "ladar.hh"
#include <getopt.h>

using namespace std;

#define SLEEP_OPT 10
#define TTY_OPT 11
#define NAME_OPT 12
#define HELP_OPT 13

int SIM = 0;                        // this is a global variable that modules use to keep track of 
                                    //     whether the code should be run in simulation mode
int LOG_SCANS = 0;                  // whether or not the scan points will be dumped to a log file 
int SEND_VOTES = 1;                 // whether or not to send votes to arbiter
int GROW_OBSTACLES = 1;             // whether or not we want to grow obstacles 
unsigned long SLEEP_TIME = 50000;  // How long in us to sleep between votes
int SPARROW_DISP = 0;
int NOSTATE = 0;                    // Don't update state if = 1.
int NOCONFIRM = 0;                  // Don't ask for confirmation of options
char *TTY;                          // serial port for scanner
char *TESTNAME;                     // id for test to be part of log file name

int main(int argc, char **argv) {

  int c;
  int digit_optind = 0;

  TTY = strdup("/dev/ttyS5");
  //sprintf(TTY,"/dev/ttyS%d",LADAR_BUMPER_SERIAL_PORT);
  //printf("tty: %s",TTY);
  while (1) {
    int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    static struct option long_options[] = {
      {"sim",       0, &SIM, 1},
      {"novote",    0, &SEND_VOTES, 0},
      {"noconfirm", 0, &NOCONFIRM, 1},
      {"log",       1, &LOG_SCANS, 1},
      {"name",      1, 0, NAME_OPT},
      {"sleep",     1, 0, SLEEP_OPT},
      {"nogrow",    0, &GROW_OBSTACLES, 0},
      {"disp",      0, &SPARROW_DISP, 1},
      {"tty",       1, 0, TTY_OPT},
      {"help",      0, 0, HELP_OPT},
      {"nostate",   0, &NOSTATE, 1},
      {0,0,0,0}
    };

    c = getopt_long_only (argc, argv, "", long_options, &option_index);
    if(c == -1) break;

    if(c != '?') {
      switch(c) {
        case SLEEP_OPT:
          SLEEP_TIME = (unsigned long) atol(optarg);
          break;
        case TTY_OPT:
          TTY = strdup(optarg);
          break;

        case NAME_OPT:
          TESTNAME = strdup(optarg);
          break;
        case HELP_OPT:
          cout << endl << "LADARBumper Usage:" << endl << endl;
          cout << "LADARBumper [-sim] [-novote] [-log testname] [-sleep us]";
          cout << endl;
          cout << "            [-noconfirm] [-nogrow] [-disp]";
          cout << endl;
          cout << "            [-tty tty] [-help]" << endl << endl;

          cout << "     -sim          Sets simulation mode.  In this mode, the LADAR will" << endl;
          cout << "                   not be accessed, but votes will still be sent." << endl;
          cout << "                   Use -novote if you wish to not send votes." << endl;
          cout << "     -novote       Do not send votes to the arbiter." << endl;
          cout << "     -name testname Log files will be named <testname>_<type>_<timestamp>" << endl;
          cout << "                   where <type> is scan, state, or vote" << endl;
	  cout << "     -log          Log all scan points, state, and votes in testLogs subdir" << endl;
          cout << "     -sleep us     Specifies number of us to sleep between updates." << endl;
          cout << "     -nogrow       Do not run obstacle growth code" << endl;
          cout << "     -disp         Run sparrow display" << endl;
          cout << "     -tty tty      Specifies tty port to use for scanner.  Example: /dev/ttyS8" << endl;
          cout << "     -help         Displays this help" << endl << endl;
          exit(0);
      }
      cout << "option " << long_options[option_index].name;
      if(optarg) cout << " with arg " << optarg;
      cout << endl;
    }
  }

  cout << endl << "Argument summary:" << endl;
  cout << "  SIM:            " << SIM << endl;
  cout << "  GROW_OBSTACLES: " << GROW_OBSTACLES << endl;
  cout << "  LOG_SCANS:      " << LOG_SCANS << endl;
  if( TESTNAME )
    cout << "  TESTNAME:       " << TESTNAME << endl;
  cout << "  SEND_VOTES:     " << SEND_VOTES << endl;
  cout << "  SLEEP:          " << SLEEP_TIME << "us" << endl;
  cout << "  SPARROW_DISP:   " << SPARROW_DISP << endl;
  cout << "  TTY:            " << TTY << endl;
  cout << "  NOSTATE:        " << NOSTATE << endl;

  // Done doing argument checking.
  ////////////////////////////////

  if(TESTNAME == 0) {
    cout << endl << endl;
    cout << "-name switch MUST be specified" << endl;
    exit(-1);
  }

  if(!NOCONFIRM) {
    cout << endl;
    cout << "VERIFY THAT YOU HAVE SPECIFIED THE CORRECT SWITCHES!" << endl;
    cout << "Do you wish to proceed? (type 'yes', anything else exits)" << endl;
    char answer[100];
    fgets(answer,sizeof(answer),stdin);
    if(strcmp(answer, "yes\n")) {
      exit(-1);
    }
  }
  
  // Start the MTA By registering a module
  // and then starting the kernel

  Register(shared_ptr<DGC_MODULE>(new LADARBumper));
  StartKernel();

  return 0;
}
