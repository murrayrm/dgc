DOCUMENTATION FOR FENDERPLANNER
================================
Written 3/28/04 by Rocky Velez

Files included in FenderPlanner:
fender.(cc/hh)
FenderUpdateVotes.cc
FenderPlanner.(cc/hh)
FenderDDF.(cc/hh)
FenderPlannerDatum.hh
FenderPlannerMain.cc
get_vstate.cc
staticFender2RDDF stuff - see Alan Somers' documentation

My job was to (a) make FenderPlanner a functional, voting-to-the-arbiter module
in the MTA and (b) write the code for the FenderDDF class, which includes the 
necessary functions required to implement FenderPlanner's functionality given a
priori data.

In order for FenderPlanner to be an adequate, voting module in the MTA, the 
following files were implemented just like similar files in the various other 
modules:

fender.(cc/hh)
FenderPlanner.(cc/hh)
FenderPlannerDatum.hh
FenderPlannerMain.cc
get_vstate.cc

See the documentation on the MTA for more information about these files and 
other MTA modules.

===============================================================================

The real gut of FenderPlanner lies in FenderDDF.(cc/hh) and 
FenderUpdateVotes.cc.  Alan Somers and I both worked on both of these, and 
towards the end of the race, parts of the structure may have been changed, but 
those changes will not be discussed here, as they were made by Alan.  The 
following documentation only covers the material I worked on up until the first
day of the QID, when it was decided that FenderPlanner was too risky to use 
during the race.  

Note: the type staticFender can be found in staticFender2RDDF.hh.

FenderDDF.(cc/hh) (.cc basedon version 1.18, .hh based on version 1.8)
----------------------------------------------------------------------
The main purpose of FenderDDF.(cc/hh) (heretofore simply FenderDDF) is to read 
the fenderpoint rddf-like file created by staticFender2RDDF (see Alan Somers' 
documentation) and create a class consisting of all the fenderpoints classified
by the segment number in which they would be found, as per the RDDF.

Two main typdef structs exist in FenderDDF:

- fenderpoint, which retains information about the northing, easting, distance 
to waypoint, and speed of each fenderpoint, gathered from the Fender data 
collected by Tony Fender et al on various trips through the desert.

- segmentData, which retains information about the segment number, number of 
fenderpoints, northing and easting of the target waypoint (same number as the 
segment number), radius of the waypoint before the target waypoint (waypoint 
for waypoint radius = waypoint for northing and easting minus 1), and an array 
of type fenderpoint (called fplist), which is just a listing of all the fenderpoints in that 
segment.

Within the class, the following methods are defined:

Note: In the private section, the class has an area to record the number of 
segments and an array of type segmentData (called segmentFPdata), which has each segment for the race,
complete with all pertinent information about the segment (see definition of 
segmentData above), including all the fenderpoints and their information in 
each segment.

PUBLIC:

Constructors:
- fenderDDF()
  sets numSegments to 0 and calls readFDDF(...) to read the standard 
fenderDDF.dat, created by Alan's staticFender2RDDF.

- fenderDDF(char *pFileName)
  the same as fenderDDF(), but calls readFDDF(...) to read the .dat file 
specified in the parameter.

Destructor:
- ~fenderDDF() 
  doesn't really do anything; it's more of a formality.

Accessors:
- bool fenderpointsInSegment(int segmentNum)
  returns true if there's more than one fenderpoint in the given segment.
  
- staticFender findCentroid(int segmentNum, staticFender Pos)
  given a particular segment number and position of the vehicle, findCentroid 
is supposed to determine the speed-weighted centroid of all the fenderpoints 
located in front of the vehicle.  Unfortunately, after some testing, we 
realized that this method does not work as planned.  Furthermore, given our 
arbiter-based decision making, GlobalPlanner and FenderPlanner tend to disagree
greatly, which creates major conflicts in path planning.  For future use, I 
would recommend looking for the closest fenderpoint, not the centroid, though 
the conflicts between FenderPlanner and GlobalPlanner in the arbiter would 
still be present.  

- int getCurrentSegment(double Northing, double Easting)
  if the vehicle is within a segment within the course, it returns that 
segment.  Otherwise, if the vehicle is outside the course (i.e. outside the 
corridors of all the segments), it returns the closest segment to the vehicle. 
It's exactly the same code as getCurrentWaypoint(...), found in RDDF/rddf.cc, 
though modified to fit functions and parameters found only in fenderDDF (like 
staticFender, as opposed to Vector).

- int getNumSegments()
  returns the number of segments in the course.

- bool withinCentroid(staticFender pos, staticFender centroid, double threshold)
  returns true if the vehicle is within the threshold of the centroid.  This 
isn't used, because hypothetically the car will never reach the centroid...  
However, if we were to discard the centroid idea and stick with looking only 
towards the closest fenderpoint, then this function could be useful (though it 
would be called withinFenderpoint(...)).

PRIVATE:

- int readFDDF(char *fileName)
  reads the fenderDDF (heretofore FDDF), as created by staticFender2RDDF.  
readFDDF distributes the information in the FDDF into segmentFPdata and 
eventually into each segment's fplist.

- double distBOB2WP(const double &WPnorthing, const double &WPeasting, 
                    const staticFender &pos)
  returns the distance from the vehicle to the upcoming waypoint.



FenderUpdateVotes.cc
---------------------

FenderUpdateVotes.cc is part of FenderPlanner.(cc/hh).  It generates the votes 
(goodnesses and velocities) that are sent to the arbiter.  In 
FenderUpdateVotes.cc are the following methods:

- double calc_steer_command(staticFender curPos, staticFender curCentroid, 
			    double curHeading) 
  returns steer command given the vehicle's current position, heading, and 
centroid to which it's aiming.  

- void FenderPlanner::FenderUpdateVotes()
  refer to MTA documentation for detailed information about this function.  In 
particular to FenderPlanner, goodnesses are based on the calculated steer 
command (from calc_steer_command(...)), as well as predefined constants found in
team/include/Constants.h.  The method for calculating goodnesses mimics that of 
GlobalUpdateVotes.  Velocities are constant accross the board, considering 
FenderPlanner points in one general direction, without really considering each 
individual arc, as the other modules do.


== end documentation

Extra stuff:

Looking over Alan's changes, a lot of them are pretty good, but as he said, in 
order to effectively implement FenderPlanner, we'd need to change GlobalPlanner 
and the arbiter.  As we begin season 2, we will proabably want to completely 
revamp the structure of the higher-level software, changing and/or removing the 
arbiter.  It may be most useful if we can concretely decide what types of 
modules we want to implement towards the beginning of the season, instead of 
trying to create risky modules at the last minute.  For example, if we had 
thought about FenderPlanner way in the beginning, we probably could have 
actually used it.