/*! 
 * $Id$
 */
#include "Brain.hh"
#include "Log.hh"
using namespace std;

extern Log logger;

//##########################################################################
//   Constants
//##########################################################################

/*! Not really sleeping, but used for waiting before going into REVERSE
 * (seconds)
 */
const double SLEEP_WAIT_BEFORE_REVERSE = 12.0;

/*! How many times we want to get zero command decisions before going reverse.
 * The duration of a PAUSE before REVERSE >= how long the arbiter
 * sleeps in total per cycle * MAX_PAUSE_COUNT = SLEEP_WAIT_BEFORE_REVERSE *
 * MAX_PAUSE_COUNT.
 */
const int MAX_PAUSE_COUNT = (int)round( SLEEP_WAIT_BEFORE_REVERSE /
                                        ( (double)SLEEP_ACTIVE_LOOP / 1000000.0 ) );

/*! How long we command REVERSE commands to Vdrive for.
 * However, currently there's a delay in switching to reverse in embedded
 * level becuase we use the e-stop mechanism to do it. This delay is about
 * 5 seconds, so the actual reverse time it takes to stop the vehicle is
 * REVERSE_ONE_PERIOD - 5                 */
const Timeval REVERSE_ONE_PERIOD(8,0); // 8 seconds

/*! REVERSE_STOP_TIME - REVERSE_ONE_PERIOD is the amount of time we stop for
 * between each period of reversing. This is defined to slow down the reversing
 * so that we don't go out of the corridor, etc, not being able to see behind. */
const Timeval REVERSE_STOP_TIME(11,0); // 11 seconds

/*! Inching forward slowly (for example when no obstacle avoidance available) */
const double INCHING_SPEED = MIN_SPEED;

/*! Reversing (slowly because we have no sensing!) */
const double REVERSE_SPEED = -MIN_SPEED;

/*! If we're going to use the stop behavoiur when going reverse */
const bool REVERSE_STOP_ENABLED = false; 

//##########################################################################
//   Brain class
//##########################################################################
Brain::Brain(ArbiterDatum& datum) {
  d = &datum;
  voterHandler = new VoterHandler(d->shapeVotesEnabled);
}

Brain::~ Brain() {
  delete voterHandler;
}
  
VDrive_CmdMotionMsg Brain::calculateCommand(list<Voter> &voters){
  
  //if voters are empty we don't calculate any new command, just stop:
  if(voters.empty()) 
  {
    if(setMovement(MOVEMENT::PAUSED))
      logger << d->lastUpdateState << " Entering PAUSED movement mode" 
	     << " because we have no valid voters!" << endl;
  }    
  // Only Decide on Arcs if:
  // - Global input hasn't timed out (or noglobal flag is set)
  // - State input hasn't timed out  (or nostate flag is set)
  else if((d->now - d->lastUpdate[ArbiterInput::Global]) > INPUT_TIMEOUT[ArbiterInput::Global] && d->globalAvailable)
  {
    if(setMovement(MOVEMENT::PAUSED))
      logger << d->lastUpdateState << " Entering PAUSED movement mode" 
	     << " because GlobalPlanner has timed out!" << endl;
  }
  else if((d->now - d->lastUpdateState) > UPDATE_STATE_TIMEOUT && d->stateAvailable)
  {
    if(setMovement(MOVEMENT::PAUSED))
      logger << d->lastUpdateState << " Entering PAUSED movement mode" 
	     << " because State has timed out!" << endl;
  }
  else 
  {
    // Normally, we should reach this part: We have recent GlobalPlanner votes
    // and a recent updated state.

    // The Voters in the list are copies, so no need to lock Voters for evaluation.
    cmd = voterHandler->calculateCommand(voters, 
                    d->lastCommand[d->currCommandIndex].steer_cmd * MAX_STEER);

    // Check obstacle sensor readings for timeouts or just enter anyway 
    // if we don't require obstacles
    if(!d->obstacleAvailable ||
       (d->now - d->lastUpdate[ArbiterInput::Stereo])   < INPUT_TIMEOUT[ArbiterInput::Stereo] ||
       (d->now - d->lastUpdate[ArbiterInput::StereoLR]) < INPUT_TIMEOUT[ArbiterInput::StereoLR] ||
       (d->now - d->lastUpdate[ArbiterInput::LADAR])    < INPUT_TIMEOUT[ArbiterInput::LADAR]) 
    {
      // We have recent obstacle information
      // Check if we've hit a deadend/moving obstacle/going too fast.
      // This will be showed by a combined vote with velocity = 0.
      if (cmd.velocity_cmd <= 0 && d->obstacleAvailable) 
      {
        if(setMovement(MOVEMENT::PAUSED))
	  logger << d->lastUpdateState << " Entering PAUSED movement mode" 
		 << " because of dead end/moving obstacle/going too fast" 
		 << endl;
      }
      else 
      { // Not a 'deadend'/moving obstacle
        if(setMovement(MOVEMENT::FORWARD))
	  logger << d->lastUpdateState << " Entering FORWARD movement mode" << endl;
      }
    }
    // We don't have any obstacle avoidance sensor data 
    // Inch forward until it comes back.
    else
    {
      if(setMovement(MOVEMENT::INCHING))
      {
        logger << d->lastUpdateState << " Entering INCHING movement mode" 
               << " because obstacle voters timed out" << endl;
        sprintf(d->infoText, " Inching slowly because no recent obstacle data");
      }
    }
    
  }
  
  handleMovement();
    
  // Update the combined votes field in the datum for display
  d->combined = voterHandler->getLastCombinedVoter();
  return cmd;
}

void Brain::handleMovement() {
  switch(d->state.movement) {
    case MOVEMENT::FORWARD: 
      // do nothing (since the speed and sterring is already set)
      break;
    case MOVEMENT::INCHING:
      // set the velocity to slow
      cmd.velocity_cmd = INCHING_SPEED;
      break;
    case MOVEMENT::PAUSED:
      // Zero the velocity so the vehicle stops.
      cmd.velocity_cmd = 0;

      // Check if we've been paused for quite a while, then go into reverse instead
      // We check for reverse so we don't enter reverse again from the reverse mode
      // Since then we would overwrite the reverseBeginPeriodTime attribute
      d->pauseCounter++;
      if (d->reverseEnabled &&
	  d->pauseCounter >= MAX_PAUSE_COUNT 
	  && d->state.movement != MOVEMENT::REVERSE) 
      {
        d->pauseCounter = 0; // reset the pausecounter for next time.
        // no need for time-since-arbiter-start on this one
        d->reverseBeginPeriodTime = TVNow(); 
        setMovement(MOVEMENT::REVERSE);
        logger << d->lastUpdateState << " Entering REVERSE movement mode"
        << " because we've been paused for a while" << endl;
      }
      // no break here since we want to handle the reverse instantly
    case MOVEMENT::REVERSE:
      reverse();
      break;
    default:
      logger << d->lastUpdateState << " Unknown movement mode!" << endl; // should never happen
  }
}

void Brain::reverse() 
{
  // Creeping backwards slowly until we get good votes forward (hopefully)
  // Steering angle is the same as previous using history.
  // We first go reverse a while, then stop, then reverse again 
  // and so on to keep the vehicle from moving backwards too fast.
  if((TVNow() - d->reverseBeginPeriodTime) < REVERSE_ONE_PERIOD) 
  {
    cmd.velocity_cmd = REVERSE_SPEED;
    // Command the last steering command
    cmd.steer_cmd = 
        d->lastCommand[d->currCommandIndex].steer_cmd * MAX_STEER;
    logger << d->lastUpdateState 
	   << "   Reversing: Slow reverse (angle: " << cmd.steer_cmd << ")" << endl;
  }
  else if(REVERSE_STOP_ENABLED && (TVNow() - d->reverseBeginPeriodTime) < REVERSE_STOP_TIME) 
  {
    cmd.velocity_cmd = 0;
    // Preserve the steering angle
    cmd.steer_cmd = 
        d->lastCommand[d->currCommandIndex].steer_cmd * MAX_STEER;
    logger << d->lastUpdateState << "   Reversing: Stopped and waiting" << endl;
  }
}

bool Brain::setMovement(int state) 
{
  bool stateOK = false;
  // verify that the state is a legal one for movement
  for(int i=0; i < MOVEMENT::COUNT ;i++)
  {
    if(state == i)
    {
      stateOK = true;
    }
  }

  if(!stateOK) 
  {
    logger << "Tried to set invalid movement state!" << endl;
    return false;
  }

  // to avoid spamming in the event log
  // only set the new state if it's different
  if(d->state.movement != state) 
  {
    d->state.movement = state; 
    return true;
  }
  
  // return false so we can have a way to check if the state changed
  // and send a log message if that's the case, for example.
  return false;
}
