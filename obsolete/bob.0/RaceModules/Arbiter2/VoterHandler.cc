/** 
 * $Id$
 */
#include "VoterHandler.hh"
#include "ArbiterDatum.hh" // ArbiterInput enum
#include "Log.hh"
#include <iostream> // during testing
using namespace std; // during testing

extern Log logger;
#define DEBUG false

//##########################################################################
//   Global things
//##########################################################################

///---Tweak-able constants for vote combining/picking logic-----//
/** Threshold under which goodness from a voter will be considered a veto vote.
 */
const double GOODNESS_THRESHOLD = 5.0;

/** Threshold under which goodness from DFE will be considered a veto vote. */
const double DFE_GOODNESS_THRESHOLD = 20.0;

/** For DFE, we give a buffer to what the DFE actually sends us, since
 * the velocity we get from the DFE are absolute minimum, just above the
 * roll-over velocities. */
const double DFE_VELO_THRESHOLD = 1.0;

/** A buffer to stop the car from oscillating horribly when state jumps around.
 * If the car moves around by 5 arcs left or right, then the difference
 * could be up to 10. 
 * TODO: DO THIS A LITTLE MORE INTELLIGENTLY 
 */
const int STREAK_SIZE_DIFF_THRESHOLD = 5;

/** Constants to indicate whether an arc has been vetoed by a module or not.
 * The way veto values are defined lets us multiply different kinds of vetos
 * for an arc to find out whether DFE and/or non-DFE modules have vetoed down
 * an arc. */
const int NO_VETO = 1;
const int NORMAL_VETO = 0;

/** The number of the middle arc (the one straight forward, will not be 
  * straight forward if we have an even number of arcs).
  * NOTE: arc indexes starts at 0 and ends at NUMARCS-1 */
const int MIDDLE_ARC = (NUMARCS/2);

/** The number of arcs around the middle arc to smooth the steering command 
 * (currently averaging the last and the current command)
 */
const int SMOOTH_SIZE = 1;

double GetPhi( int arcnum )
{
  return ( ((double) arcnum) / ((double) (NUMARCS-1)) ) * (RIGHTPHI - LEFTPHI)
         + LEFTPHI;
}

int GetArcNum( double phi )
{
  // cap the values
  if(phi < LEFTPHI)
    phi = LEFTPHI;
  else if(phi > RIGHTPHI)
    phi = RIGHTPHI;
  // calculate
  int arcNum =
    (int)round((phi - LEFTPHI)/(RIGHTPHI - LEFTPHI) * (double)(NUMARCS-1));
  if (arcNum < 0 || arcNum > NUMARCS)
  {
    return -1;
  }
  else
  {
    return arcNum;
  }
}

//##########################################################################
//   VoterHandler class
//##########################################################################

//#####################
//   Public methods
//#####################
VDrive_CmdMotionMsg VoterHandler::calculateCommand(
                            list<Voter> &current_voters, double lastPhi)
{
  VDrive_CmdMotionMsg cmd;
  VDrive_CmdMotionMsg cmd_noDFE;
  VDrive_CmdMotionMsg cmd_withDFE;
  bool DONT_USE_DFE = false;
  bool USE_DFE = true;

  // create a "combined" output voter without DFE taken into account
  Voter combinedVoterNoDFE = combineVotes(current_voters, DONT_USE_DFE);
  cmd_noDFE = pickBestArc(combinedVoterNoDFE, lastPhi);

  // create a "combined" output voter with DFE taken into account
  Voter combinedVoterWithDFE = combineVotes(current_voters, USE_DFE);
  cmd_withDFE = pickBestArc(combinedVoterWithDFE, lastPhi);

  // We want the steering angle determined from using DFE
  // and the velocity determined from not using DFE.
  // This is so that if all non-DFE modules decide to go on a path defined
  // untrasversable by DFE, then we still want to steer the angle determined
  // with DFE but we don't go too fast (e.g. DFE's velocity for going straight)
  // so that we can never turn.
  combinedVoter = combinedVoterWithDFE;  // Only used for display purposes
  cmd.steer_cmd = cmd_withDFE.steer_cmd;
  cmd.velocity_cmd = min(cmd_noDFE.velocity_cmd, cmd_withDFE.velocity_cmd);
  
  // Smooth the output if it's almost straight forward
  smoothCmd(cmd, lastPhi);
  return cmd;
}


//#####################
//   Private methods
//#####################

Voter VoterHandler::combineVotes(list<Voter> &current_voters, 
                                 bool USE_DFE)
{
  Voter ret; // the "Combined" voter
  int veto[NUMARCS];
  int dfe_veto[NUMARCS];
  double numValidVoters[NUMARCS];
  ret.clearVotes();

  for(int i=0; i<NUMARCS; i++)
  {   // reset arrays
    veto[i] = NO_VETO;
    dfe_veto[i] = NO_VETO;
    numValidVoters[i] = 0;
  }

  list<Voter>::iterator x;
  // Traverse through all voters and calculate combined weight
  for( x = current_voters.begin(); x != current_voters.end(); x++)
  {
    // Vote-shaping: wasn't used in race
    if(doShapeVotes && ((*x).ID == ArbiterInput::Stereo ||
                        (*x).ID == ArbiterInput::StereoLR ||
                        (*x).ID == ArbiterInput::LADAR  ||
                        (*x).ID == ArbiterInput::CorrArcEval))
    {
      shapeVotes(*x, 0.0, 0.5);
    }
    // We treat DFE different from other modules, since its Votes depend
    // purely on dynamic reasons and not pathplanning.
    if ((*x).ID == ArbiterInput::DFE)
    {
      // For each possible arc:
      for(int j = 0; j<NUMARCS; j++)
      {
        //We only look at DFE_VETOs if the user specifies us to (USE_DFE==TRUE)
        if (USE_DFE)
        {
          // Goodness: multiply and sum (weighted sum)
          if ((*x).Votes[j].Goodness <= DFE_GOODNESS_THRESHOLD)
          {
            dfe_veto[j] = NORMAL_VETO;
            // numValidVoters shouldn't matter, since veto'd down.
            numValidVoters[j] += (*x).VoterWeight;
            // Zero goodness, so zero velocity.
            ret.Votes[j].Velo = 0.0;
          }
          // (3/5/04) Put in round(goodness) as a possible fix for LADAR's
          // sending goodnesses that look like '100.0' but aren't being
          // treated as <= MAX_GOODNESS. This really shouldn't be the case...
          else if ( round((*x).Votes[j].Goodness) > MAX_GOODNESS ||
                    round((*x).Votes[j].Goodness) < 0 )
          {
            // Currently (2/22/04) we're doing nothing.
            // DFE shouldn't ever give NO_CONFIDENCE.
            if ((*x).Votes[j].Goodness == NO_CONFIDENCE) 
            {
              logger << "Got NO_CONFIDENCE from DFE voter" << endl;
            }
          }
          else
          {
            // DFE, above threshold -> good, not affecting the overall arc
            //  goodness
            // Note that if DFE is the only Voter, arbiter commands max
            //  speed, straight, if -noglobal -noobstacle specified.
          }
        } // end if(USE_DFE)

        // Take the minimum acceptable velocity:
        // For DFE, we give a buffer to what the DFE actually sends us, since
        // the velocity we get from the DFE are absolute minimum, just above
        // the roll-over velocities.
        // Note that USE_DFE==false still lets DFE cut down the velocity.
        ret.Votes[j].Velo =
          min( ret.Votes[j].Velo, (*x).Votes[j].Velo - DFE_VELO_THRESHOLD );
      } // end for NUMARCS
    } // end if DFE

    else
    { // A non-DFE Voter
      // For each possible arc:
      for(int j = 0; j<NUMARCS; j++)
      {
        // Goodness: multiply and sum (weighted sum)
        if ((*x).Votes[j].Goodness <= GOODNESS_THRESHOLD)
        {
          veto[j] = NORMAL_VETO;
          numValidVoters[j] += (*x).VoterWeight;
          // Zero goodness, so zero velocity.
          ret.Votes[j].Velo = 0.0;
          //cout << "-";
        }
        // (3/5/04) Put in round(goodness) as a possible fix for LADAR's
        // sending goodnesses that look like '100.0' but aren't being
        // treated as <= MAX_GOODNESS
        else if(round((*x).Votes[j].Goodness) > MAX_GOODNESS ||
                round((*x).Votes[j].Goodness) < 0 )
        {
          if ((*x).Votes[j].Goodness == NO_CONFIDENCE) 
          {
            logger << "Got NO_CONFIDENCE from " << ArbiterInputStrings[(*x).ID] 
                  << " voter " << endl;
          }
          // Currently (2/22/04) we're doing nothing. Also not taking
          // this voter's velocity into account.
          //cout << "out of range" << endl;
        }
        else
        {
          ret.Votes[j].Goodness += (*x).Votes[j].Goodness * (*x).VoterWeight;
          numValidVoters[j] += (*x).VoterWeight;
          // take the minimum acceptable velocity
          ret.Votes[j].Velo = min( ret.Votes[j].Velo, (*x).Votes[j].Velo );
          //cout << "+";
        }
      } // end for NUMARCS
    } // end "non-DFE voter"
    //cout << " end of voter" << endl;
  } // end of for(current_voters...)

  bool path_exists = false;   // Keeps track of whether a good path exists or not

  // Check for any vetoed down angles and normalize.
  for(int i = 0; i<NUMARCS; i++)
  {
    //cout << "vote[" << i << "] veto: " << veto[i] << " dfe_veto: " 
    //   << dfe_veto[i] << " numValidVoters: " << numValidVoters[i] << endl;
    if (numValidVoters[i] > 0)
    {
      ret.Votes[i].Goodness =
        (ret.Votes[i].Goodness * veto[i] * dfe_veto[i]) / numValidVoters[i];
      if (ret.Votes[i].Goodness > 0)
      {
        path_exists = true;
      }
    }
    else if (numValidVoters[i] == 0.0)
    {
      ret.Votes[i].Goodness = 0;
    }
  }

  /* If no good path exists, make the velocities all zero, so we know that
   * there's no good path. In pickBestCombinedArc, all-zero velocity will 
   * trigger assigning the old steering angle to the return steering value. */
  if (!path_exists)
  {
    for(int i = 0; i<NUMARCS; i++)
    {
      ret.Votes[i].Velo =0;
    }
  }
  return ret;
}

VDrive_CmdMotionMsg VoterHandler::pickBestArc(Voter& voter, double lastPhi)
{
  double bestGoodness = 0.0;
  bool inSequence = false;
  list<list<int> > sequences; // to store sequences of equal votes
  // check if we have a single best arc, then pick that one
  for(int i=0; i<NUMARCS; i++)
  {
    if(voter.Votes[i].Goodness > bestGoodness)
    {
      sequences.clear();
      bestGoodness = voter.Votes[i].Goodness;
      if(DEBUG) logger << "new sequence, arc " << i << endl;
      sequences.push_back(list<int>());
      sequences.front().push_back(i);
      inSequence = true;
    }
    else if(voter.Votes[i].Goodness == bestGoodness)
    {
      if(inSequence)
      {
        if(DEBUG) logger << "continue sequence, arc " << i << endl;
        sequences.front().push_back(i);
      }
      else // start new sequence of equal goodness:
      {
        if(DEBUG) logger << "new sequence (equal), arc " << i << endl;
        sequences.push_back(list<int>());
        sequences.front().push_back(i);
        inSequence = true;
      }
    }
    else // a Vote with lower goodness (means end of strike)
    {
      inSequence = false;
    }
  }

  // now we analyze our sequences (may be size == 1) and picks the
  // best arc (the one in the middle, and the once closest to the last
  // selected direction of multiple fits)
  int returnIndex;

  // If we have a single sequence with all the max goodness, pick the middle
  // element of those. The second criteria in the if case takes care of the
  // special case when all votes have zero goodness (then we want to pick the
  // arc closest to lastPhi)
  if(sequences.size() == 1 && sequences.front().front() != 0)
  {  // if we have only one sequence, pick the middle arc of that one.
    returnIndex = sequences.front().front() + sequences.front().size()/2;
  }
  else
  {  // more than one sequences of arcs that are equal.
    double minDiff= 1e25;
    // initialize the minDiffIndex to the straigt-forward arc, even though
    // it should be impossible to not find an arc below:
    int minDiffIndex = MIDDLE_ARC;
    list<list<int> >::const_iterator i;
    list<int>::const_iterator j;
    for( i = sequences.begin(); i != sequences.end(); i++)
    {
      for( j = (*i).begin(); j != (*i).end(); j++)
      {
        if(fabs(GetPhi(*j) - lastPhi) < minDiff)
        {
          minDiffIndex = (*j);
          minDiff = fabs(GetPhi(*j) - lastPhi);
        }
      }
    }
    // only one arc can have the smallest diff, return that one
    returnIndex = minDiffIndex;
  }
  // Now we know which arc, create a command from this and return it.
  VDrive_CmdMotionMsg cmd;
  cmd.steer_cmd = GetPhi(returnIndex);
  cmd.velocity_cmd = voter.Votes[returnIndex].Velo;
  return cmd;
}

void VoterHandler::smoothCmd(VDrive_CmdMotionMsg &cmd, double lastPhi) 
{
  // Yes I know here's a conversion back to arcs again but this gives an easy
  // optimization for someone interested later...
  int arcNbr = GetArcNum(cmd.steer_cmd);
  int lastArcNbr = GetArcNum(lastPhi);
  // check if both commands are in the center 3 arcs
  if( arcNbr != lastArcNbr &&
         arcNbr >= MIDDLE_ARC - SMOOTH_SIZE &&     arcNbr <= MIDDLE_ARC + SMOOTH_SIZE &&
     lastArcNbr >= MIDDLE_ARC - SMOOTH_SIZE && lastArcNbr <= MIDDLE_ARC + SMOOTH_SIZE)
  { 
    // the we want to smooth the result, let's do an average between this and
    // the old arc
    logger << "smoothCmd: cmd.steer_cmd = arc " << arcNbr << ", lastPhi = arc " << lastArcNbr << endl;
    cmd.steer_cmd = cmd.steer_cmd + lastPhi;
  }
}

//####################################################################
//  Wasn't used for the race:
//####################################################################

// (3/5/04) Put in round(goodness) as a possible fix for LADAR's
// sending goodnesses that look like '100.0' but aren't being
// treated as <= MAX_GOODNESS. This really shouldn't be the case...
void VoterHandler::shapeVotes(Voter &v, double threshold, 
                              double favor_high_factor)
{
  double newGoodness[NUMARCS];

  // Initialize newGoodness[]
  for(int i = 0; i < NUMARCS; i++ )
  {
    newGoodness[i] = v.Votes[i].Goodness;
  }

  for(int i = 0; i < NUMARCS; i++ )
  {
    if (i > 0)
    {
      // Can compare to i-1
      if(round(v.Votes[i].Goodness) <= MAX_GOODNESS &&
          round(v.Votes[i].Goodness) >= 0 &&
          round(v.Votes[i-1].Goodness) <= MAX_GOODNESS &&
          round(v.Votes[i-1].Goodness) >= 0 &&
          v.Votes[i].Goodness - v.Votes[i-1].Goodness >= threshold)
      {

        newGoodness[i] =
          (favor_high_factor*v.Votes[i].Goodness +
           v.Votes[i-1].Goodness) / (1.0 + favor_high_factor);
      }
    }
    if(i < NUMARCS-1)
    {
      // Can compare to i+1
      if(round(v.Votes[i].Goodness) <= MAX_GOODNESS &&
          round(v.Votes[i].Goodness) >= 0 &&
          round(v.Votes[i+1].Goodness) <= MAX_GOODNESS &&
          round(v.Votes[i+1].Goodness) >= 0 &&
          v.Votes[i].Goodness - v.Votes[i+1].Goodness >= threshold)
      {

        newGoodness[i] =
          (((favor_high_factor*v.Votes[i].Goodness +
             v.Votes[i+1].Goodness) / (1.0 + favor_high_factor))
           + newGoodness[i]) / 2;
      }
    }
  } // end of for NUMARCS

  // update goodness for all votes
  for (int i = 0; i < NUMARCS; i++ )
  {
    v.Votes[i].Goodness = newGoodness[i];
  }
}

