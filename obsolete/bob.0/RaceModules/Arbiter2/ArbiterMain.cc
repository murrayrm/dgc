/**
 * For complete documentation about the Arbiter, see the Arbiter class.
 * $Id$
 */
#include "Arbiter.hh"
#include "Log.hh"
#ifdef MACOSX
  /* Install libgnugetopt for getopt_long_only support in Mac OS X */
  #include <gnugetopt/getopt.h>
#else
  #include <getopt.h>
#endif

using namespace std;

char *testname;

extern Log logger;

char* getMode(int mode)
{
  if(mode)
    return "yes";
  else
    return "no";
}

int main(int argc, char **argv)
{
  // Argument handling
  int c;
  int flags = 0; // Arbiter option flags will be masked into this
  while (1)
  {
    /* getopt_long stores the option index here. */
    int option_index = 0;
    static struct option long_options[] =
      { // name         argument?          flag  value
        {"log",         required_argument, NULL, LOG},
        {"noglobal",    no_argument      , NULL, NO_GLOBAL},
        {"nostate",     no_argument      , NULL, NO_STATE},
        {"noobstacle",  no_argument      , NULL, NO_OBSTACLE},
        {"novdrive",    no_argument      , NULL, NO_VDRIVE},
        {"noreverse",   no_argument      , NULL, NO_REVERSE},
        {"shapevotes",  no_argument      , NULL, SHAPEVOTES},
        {"nodisp",      no_argument      , NULL, NO_DISP},
        {"help",        no_argument      , NULL, HELP},
        {0, 0, 0, 0}
      };

    char short_options[] = "l:gsovrsdh";
    c = getopt_long_only (argc, argv, short_options, long_options, &option_index);
    /* Detect the end of the options. */
    if(c == -1)
      break;

    switch(c)
    {
    case 'l':
    case LOG:
      flags |= LOG;
      testname = strdup(optarg);
      break;
    case 'h':
    case HELP:
      cout << endl
      << "Usage: ./Arbiter2 [OPTIONS]" << endl << endl
      << "Mandatory arguments to long options are mandatory for short options too." << endl
      << " -l, --log testname  Log state and commands in logs dir" << endl
      << " -g, --noglobal      Don't require GlobalPlanner" << endl
      << " -s, --nostate       Don't require VState information" << endl
      << " -o, --noobstacle    Don't require obstacle data" << endl
      << " -v, --novdrive      Run without sending commands to VDrive" << endl
      << " -r, --noreverse     Disable reverse feature" << endl
      << " -x, --shapevotes    Enable vote shaping (feature)" << endl
      << " -d, --nodisp        Disable sparrow display" << endl
      << " -h, --help          Displays this text" << endl
      << endl;
      exit(0);
      break;
      
    case 'g':
    case NO_GLOBAL:
      flags |= NO_GLOBAL;
      break;
    case 's':
    case NO_STATE:
      flags |= NO_STATE;
      break;
    case 'o':
    case NO_OBSTACLE:
      flags |= NO_OBSTACLE;
      break;
    case 'v':
    case NO_VDRIVE:
      flags |= NO_VDRIVE;
      break;
    case 'r':
    case NO_REVERSE:
      flags |= NO_REVERSE;
      break;
    case 'x':
    case SHAPEVOTES:
      flags |= SHAPEVOTES;
      break;       
    case 'd':
    case NO_DISP:
      flags |= NO_DISP;
      break;
     case '?': // invalid options makes us not want to start
       return 1;
     default:
       printf ("?? returned character code 0%o ??\n", c);
       return 1;           
    }
  } // end while(1)

  cout << endl << "Argument summary:" << endl;
  cout << "  Logging enabled         : " << getMode(flags & LOG) << endl;
  if(flags & LOG)
    cout << "  Log files prefix: " << testname << endl;
  cout << "  Using GlobalPlanner     : " << getMode(!(flags & NO_GLOBAL)) << endl;
  cout << "  Using State information : " << getMode(!(flags & NO_STATE)) << endl;
  cout << "  Using Obstacle sensors  : " << getMode(!(flags & NO_OBSTACLE)) << endl;
  cout << "  Send commands to VDrive : " << getMode(!(flags & NO_VDRIVE)) << endl;
  cout << "  Reverse enabled         : " << getMode(!(flags & NO_REVERSE)) << endl;
  cout << "  Shapevotes enabled      : " << getMode(flags & SHAPEVOTES) << endl;
  cout << "  Sparrow display enabled : " << getMode(!(flags & NO_DISP)) << endl;

  // Also put this info in the log if logging is enabled:
  if(flags & LOG)
  {
    logger.init(true, testname);
    
    logger << endl << "Argument summary:" << endl;
    logger << "  Logging enabled         : " << getMode(flags & LOG) << endl;
    if(flags & LOG)
      logger << "  Log files prefix: " << testname << endl;
    logger << "  Using GlobalPlanner     : " << getMode(!(flags & NO_GLOBAL)) << endl;
    logger << "  Using State information : " << getMode(!(flags & NO_STATE)) << endl;
    logger << "  Using Obstacle sensors  : " << getMode(!(flags & NO_OBSTACLE)) << endl;
    logger << "  Send commands to VDrive : " << getMode(!(flags & NO_VDRIVE)) << endl;
    logger << "  Reverse enabled         : " << getMode(!(flags & NO_REVERSE)) << endl;
    logger << "  Shapevotes enabled      : " << getMode(flags & SHAPEVOTES) << endl;  
    logger << "  Sparrow display enabled : " << getMode(!(flags & NO_DISP)) << endl;
  }
  
  // Start the MTA By registering a module
  // and then starting the kernel
  Register(shared_ptr<DGC_MODULE>(new Arbiter(flags)));
  StartKernel();

  return 0;
}
