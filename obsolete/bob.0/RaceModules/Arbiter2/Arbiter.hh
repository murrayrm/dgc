#ifndef ARBITER_HH
#define ARBITER_HH

#include "ArbiterDatum.hh"
#include "MTA/Kernel.hh"    // The MTA Module Kernel
#include "Brain.hh"
#include "Voter.hh"
using namespace std;
using namespace boost;

// Defines for the command line option flags 
#define LOG         (1<<0)
#define HELP        (1<<1)
#define NO_GLOBAL   (1<<2)
#define NO_STATE    (1<<3)
#define NO_OBSTACLE (1<<4)
#define NO_VDRIVE   (1<<5)
#define NO_REVERSE     (1<<6)
#define SHAPEVOTES  (1<<7)
#define NO_DISP     (1<<8)


/** Arbiter class. The Arbiter gets votes from different votering modules (like
 * GlobalPlanner, LADARMapper, SteroeMapper, DFEPlanner, CorridorArcEvaluator etc.),
 * analyzes them and creates a combined vote from which the decision is taken.
 * The decision is then sent to VDrive as a VDrive_CmdMotionMsg struct (which
 * contains the steering angle and the desired velocity).
 * The Arbiter is an MTA module and uses MTA for it's interprocess
 * communication.
 * NOTE: The state the arbiter retrieves from VState is not used for anything
 *       except logging and display.
 * $Id$
 */
class Arbiter : public DGC_MODULE {
public:
  /** Arbiter constructor.
   * @param flags Different flags can be set depending on the wanted 
   *              configuration. See the #define lines above  */
  Arbiter(int flags);
  
  /** Destructor. Just deletes the created objects */
  ~Arbiter();

  /** Initializes the input weights, the datum, the brain and all voters.
   * Also starts up the sparrow display when the rest has finished */
  void Init();
  
  /** The main loop of the arbiter. Checks for valid voters, calculates a
   * command and ships it to VDrive. */
  void Active();
  
  /** Standby function, does nothing but sleeping */
  void Standby();
  
  /** Shuts down the arbiter: closes the logs */
  void Shutdown();

  // The message handlers. 
  /** Handles incoming MTA messages. */
  void InMailHandler(Mail & ml);
  /** Handles MTA query messages and replies to them */
  Mail QueryMailHandler(Mail & ml);

private:
  // and a datum named d.
  ArbiterDatum d;
  Brain* brain;

  /** Weights of each Voter, ranging from 0 to 1. */
  vector<double> InputWeights;

  /** The list of current voters */
  list<Voter> voterList;

  /** Calls VDrive for the current state. 
   * IMPORTANT TO KNOW: This state is only used for logging and display 
   *                    purposes, nothing else!
   */
  void updateState();
  
  /** Updates the votes at all Voters */
  void updateVoterList();
  
  /** Prepare a VDrive command, puts timestamps, rescale the steering_cmd
   *  stores into command history and more... */
  void prepareVDriveCommand(VDrive_CmdMotionMsg& cmd);

  /** Wait for modules (if not the special arguments that enables running
   *  without certain modules are set)
   */
  void waitForModules();
  
  /** Creates a sparrow display */
  void SparrowDisplayLoop();

  /** The update loop for the sparrow display */
  void UpdateSparrowVariablesLoop();
  
  /** If we're going to draw the sparrow display or not */
  bool useSparrowDisplay;
};  

#endif
