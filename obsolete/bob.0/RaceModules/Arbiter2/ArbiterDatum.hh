/** ArbiterDatum contains common data structures and some constans that is 
 * required my several classes (and the voters). It also contains the 
 * Messaging structs, Arbiter Status struct and defines the different available 
 * inputs (voters) to the Arbiter.
 * $Id$
 */
#ifndef ARBITERDATUM_HH
#define ARBITERDATUM_HH

#include <string>
#include <vector>
#include "VoterHandler.hh" // includes Voter.hh and Vote.hh
#include "vehlib/VState.hh" // VState_GetStateMsg sent via MTA
#include "vehlib/VDrive.hh" // VDrive_CmdMotionMsg sent to actuators
#include <Constants.h> // MIN_SPEED, MAX_SPEED
using namespace std;

/** How much we sleep for in each iteration of the Active() loop.
 *  Mostly for sharing CPU with other processes and not frying MTA/Vdrive
 * (50ms = 20Hz)
 */
const long SLEEP_ACTIVE_LOOP = 50000;

/** 
 *Timeval is the struct we use for time calculations 
 */
const Timeval ACTIVE_PERIOD(0, SLEEP_ACTIVE_LOOP);

/** Number of past commands to remember. We remember past commands so we can
 * reverse a little intelligently, though this may get us stuck in an 
 * infinite loop of going back and forth on the same path.      */
const int NUM2REMEMBER = 10;

// Constants shared by all Voters:

/** maximum steering angle in radians */
const double MAX_STEER = USE_MAX_STEER;
/** maximum left steering angle in radians */
const double LEFTPHI      = -1.0 * MAX_STEER;
/** maximum right steering angle in radians */
const double RIGHTPHI     = MAX_STEER;

/** Convert radians to degrees 
 * @param the value in radians
 * @return the value in degrees
 */
inline double RTOD(double r) 
{
  return (r * 180 / M_PI);
}

/** Convert degrees to radians 
 * @param the value in degrees 
 * @return the value in radians
 */
inline double DTOR(double d) 
{
  return (d * M_PI / 180);
}

/** enumerate all module messages that could be received */
namespace ArbiterMessages
{
  enum {
    Update, // takes int (vote source) and Voter
    GetVotes, // MatlabDisplay asks for velocity/steering command
    WaypointReached, // GlobalPlanner tells that a waypoint has been reached
    NumMessages // A place holder
  };
};

/** Enumerate all arbiter inputs
 * PLEASE do not change the order of these, the order is used to 
 * identify log files.  Add new ones at the end instead.
 * IMPORTANT: if you add more inputs here, be sure to update 
 * ArbiterInputStrings below too.
 */
namespace ArbiterInput
{
  enum {
    LADAR,
    Stereo,
    StereoLR,
    Global,
    DFE,
    CorrArcEval,
    FenderPlanner,
    RoadFollower,
    PathEvaluation,
    // if you add more here, don't forget to update ArbiterInputStrings below!
    
    Count    // [place holder] the total number of inputs.
  };
};

/** Just strings to be able printing different voters */
static const char* const ArbiterInputStrings[] =
  {
    "LADAR",
    "Stereo",
    "StereoLR",
    "Global",
    "DFE",
    "CorrArcEval",
    "FenderPlanner",
    "RoadFollower",
    "PathEvaluation",
    0 // place holder
  };

/** MOVEMENT descibes the current desired movement
 *  <ul>
 *    <li>FORWARD, we want to move forward as normal
 *    <li>INCHING, move really slow forward
 *    <li>PAUSED, stop (and wait)
 *    <li>REVERSE, moving backwards.
 *  </ul>*/
namespace MOVEMENT 
{ 
enum { FORWARD, INCHING, PAUSED, REVERSE, COUNT };
};

/** TERRAIN describes the terrain type, can be
 *  <ul>
 *    <li>LIGHT_TERRAIN: roads, flat terrain with not too many obstacles
 *    <li>ROUGH_TERRAIN: offroad, terrain with more elevation differences
 *        and obstacles
 *  </ul> */
namespace TERRAIN 
{ 
  enum { LIGHT_TERRAIN, ROUGH_TERRAIN, COUNT };
};

/** LIGHTING describes good or bad lighting conditions, can be:
 * <ul>
 *   <li>GOOD_LIGHTING. Clear view
 *   <li>BAD_LIGHTING. For example dusk, dawn and sun in front of us. 
 * </ul>
 * These conditions affects how much to trust stereo vision votes. */
namespace LIGHTING 
{ 
enum { GOOD_LIGHTING, BAD_LIGHTING, COUNT };
};

/** DUSTYNESS describes the dust conditions:
 * <ul>
 *   <li>DUSTY, don't trust LADAR since it's inaccurate!
 *   <li>NOT_DUSTY, normal - trust LADAR
 * </ul>
 * These conditions highly affects how much to trust LADAR votes */
namespace DUSTYNESS 
{ 
enum { DUSTY, NOT_DUSTY, COUNT };
};

/** Definition of the state of the arbiter.
 * The state consists of a collection of different enums which can changed */
struct ArbiterState
{
  int movement;
  int terrain;
  int lighting;
  int dustyness;
};

/** The arbiter datum, common data structures for the different classes
 * of the arbiter */
class ArbiterDatum
{
public:
  /** A state that describes all info about what state the arbiter thinks
   * it is in at the moment. Used to decide different weight-sets */
  ArbiterState state;
  
  // for data logging
  int TestNumber;
  string TestNumberStr;

  /** Zero time, set when the Arbiter is launched, used for calculation of
   * times relative to start-time */
  Timeval TimeZero;
  
  /** The most current timestamp, is set in the Active loop. Realtive to 
   * TimeZero. */
  Timeval now;
  
  /** The timestamp of the last update from VState (relative to TimeZero) */
  Timeval lastUpdateState;
  
  /** The array of all voters */
  Voter allVoters[ArbiterInput::Count];
  /** A timestamp when all voters were last updated (relative to TimeZero) */
  Timeval lastUpdate[ArbiterInput::Count];
  /** A count of how many times each voter has been updated */
  int UpdateCount[ArbiterInput::Count];
  
  /** The combined resulting vote of all voters */
  Voter combined;
  
  /** Counts how many times we've updated the state */
  int UpdateCountState;
  /** Counts how many times we've created a combined voters */
  int CombinedUpdateCount;

  /** Keeping track of how long we've been in PAUSE mode.*/
  int pauseCounter;
  
  /** Keeping track of commands, for REVERSE */
  int currCommandIndex;
  /** Keeping track of commands, for REVERSE */
  VDrive_CmdMotionMsg lastCommand[NUM2REMEMBER];
  /** Timestamp of when we start going reverse */
  Timeval reverseBeginPeriodTime;
  
  // and a lock for the votes timestamps.
  boost::recursive_mutex voterLock;

  /** The most recent command to be send/that has been sent to VDrive */
  VDrive_CmdMotionMsg toVDrive;
  
  /** The current vehicle state, only for display and logging purposes */
  VState_GetStateMsg SS;

  //---Options for the arbiter------------------------------------------------//
  /** whether or not we want the lack of Global to stop Arbiter non-zero commands. */
  bool globalAvailable;

  /** Whether we care or not that we have state data.
   * The arbiter doesn't really care about state (as far
   * as code goes at least), but other modules will be
   * unreliable w/o up-to-date & correct state data.
   */
  bool stateAvailable;

  /** whether or not we want the lack of obstacle
  * detection (Stereo/LADAR) to stop Arbiter from
  * sending non-zero commands.
  */
  bool obstacleAvailable;

  /** whether we want to go into REVERSE mode ever */
  bool reverseEnabled;

  /** whether we want to send commands to VDrive or not.
   * (for example if you want to only use the arbiter for logging and have
   * some other module commanding VDrive). */
  bool vdriveEnabled;  
   
  /** wheter use the shapevotes feature or not */
  bool shapeVotesEnabled;
  
  /** the debug line in the sparrow display, to eaily output information to 
  * the user. */
  char infoText[256];
};

#endif
