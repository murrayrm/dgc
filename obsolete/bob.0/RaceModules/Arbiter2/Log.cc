/*! 
 * $Id$
 */
#include "Log.hh"
#include "MTA/Kernel.hh" // timestamp info
#include "ArbiterDatum.hh" // ArbiterInput

// ##########################################################################
//    Global things
// ##########################################################################

Log logger; // create the global log here!

// ##########################################################################
//    Logger class
// ##########################################################################

void Log::init(bool isEnabled, char* file_prefix)
{
  enabled = isEnabled;
  if(enabled)
  {
    time_t t = time(NULL);
    tm *local;
    local = localtime(&t);
    char filename[255]; // output log file name

    //=========================================================================
    // State log
    //=========================================================================
    // create a log of the state and arbiter commands
    // filename will be prefix_arbiter_yyyy-mm-dd_hh.mm.ss.log
    sprintf(filename, "logs/%s_arbiter_%04d-%02d-%02d_%02d.%02d.%02d.log",
            file_prefix,
            local->tm_year+1900, local->tm_mon+1, local->tm_mday, // year, month, day
            local->tm_hour, local->tm_min, local->tm_sec); // hour, min, sec
    statefile = fopen(filename,"w");
    if ( statefile == NULL )
    {
      printf("Logger::initLogs: Unable to open state log file '%s'\n", filename);
      exit(-1);
    }
    else
    {
      printf("Logger::initLogs: State log: %s\n", filename);
    }
    // print data format information in a header at the top of the file
    fprintf(statefile, "%% Time[sec] | Easting[m] | Northing[m] | Altitude[m]");
    fprintf(statefile, " | Vel_E[m/s] | Vel_N[m/s] | Vel_U[m/s]" );
    fprintf(statefile, " | Speed[m/s] | Accel[m/s/s]" );
    fprintf(statefile, " | Pitch[rad] | Roll[rad] | Yaw[rad]" );
    fprintf(statefile, " | PitchRate[rad/s] | RollRate[rad/s] | YawRate[rad/s]");
    fprintf(statefile, " | PhiDes[rad] | VelDes[m/s] | PhiManual[rad]\n");

    //=========================================================================
    // Event log
    //=========================================================================
    // create a log for events
    // filename will be prefix_events_yyyy-mm-dd_hh.mm.ss.log
    sprintf(filename, "logs/%s_events_%04d-%02d-%02d_%02d.%02d.%02d.log",
            file_prefix,
            local->tm_year+1900, local->tm_mon+1, local->tm_mday, // year, month, day
            local->tm_hour, local->tm_min, local->tm_sec); // hour, min, sec
    open(filename, ofstream::out);
    if (!is_open())
    {
      printf("Logger::initLogs: unable to open event log file '%s'\n", filename);
      exit(-1);
    }
    else
    {
      printf("Logger::initLogs: Event log: %s\n", filename);
      char buf[128];
      sprintf(buf, " Event log  (created %04d-%02d-%02d %02d:%02d)\n", 
              local->tm_year+1900, local->tm_mon+1, local->tm_mday,
              local->tm_hour, local->tm_min);
      *this << buf;
    }

    //=========================================================================
    // Voter logs
    //=========================================================================
    // reserve the space for our voter logs
    voteLogFileVector.reserve(ArbiterInput::Count);

    // additionally, create a log file for each voter
    for( int i = 0; i<ArbiterInput::Count; i++)
    {
      // filename will be prefix_votesnn_[votername]_yyyy-mm-dd_hh.mm.ss.log
      sprintf(filename, "logs/%s_votes%02d_%s_%04d-%02d-%02d_%02d.%02d.%02d.log",
              file_prefix, i, ArbiterInputStrings[i],
              local->tm_year+1900, local->tm_mon+1, local->tm_mday, // year, month, day
              local->tm_hour, local->tm_min, local->tm_sec); // hour, min, sec
      voteLogFileVector.push_back( fopen(filename,"w") );
      if ( voteLogFileVector[i] == NULL )
      {
        printf("Logger::initLogs: Unable to open voter log file '%s'\n", filename);
        exit(-1);
      }
      else
      {
        printf("Logger::initLogs: Voter log: %s\n", filename);
      }
      // print data format information in a header at the top of the file
      fprintf(voteLogFileVector[i], "%% Vote log file %02d (", i );
      fprintf(voteLogFileVector[i], "%s", ArbiterInputStrings[i]);
      fprintf(voteLogFileVector[i], ")\n");
      fprintf(voteLogFileVector[i],
              "%% Votes are goodnesses and then velocities for each\n");
      fprintf(voteLogFileVector[i], "%% arc from full left to full right.\n");
      fprintf(voteLogFileVector[i], "%% first number on each line is the timestamp\n");
      fprintf(voteLogFileVector[i], "%% NUMARCS = %d\n", NUMARCS );
    } // end for
  } // end if logging enabled
}

void Log::logState(VDrive_CmdMotionMsg& cmd, 
		   VState_GetStateMsg& SS, 
		   Voter* voters,
		   Steer_Packet& sp)
{
  // log data if the switch was set
  if (enabled)
  {

    //=========================================================================
    // State log
    //=========================================================================
    /* a timestamp shared between state and voter logs for synchronization */
    double timestamp = (double)(SS.Timestamp.sec()+SS.Timestamp.usec()/1000000.0);
    fprintf(statefile, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",
            timestamp,
            SS.Easting,
            SS.Northing,
            SS.Altitude,
            SS.Vel_E,
            SS.Vel_N,
            SS.Vel_U,
            SS.Speed,
            SS.Accel,
            SS.Pitch,
            SS.Roll,
            SS.Yaw,
            SS.PitchRate,
            SS.RollRate,
            SS.YawRate,
            cmd.steer_cmd,
            cmd.velocity_cmd,
            sp.ActualSteer * MAX_STEER); // For human driving logging: steering command
    fflush(statefile);

    //=========================================================================
    // Voter logs
    //=========================================================================
    for( int i = 0; i<ArbiterInput::Count; i++)
    {
      // write timestamp first
      fprintf(voteLogFileVector[i], "%f\t", timestamp);

      // write the goodness votes to the file
      for( int j = 0; j<NUMARCS; j++ )
      {
        fprintf(voteLogFileVector[i], "%5.1f\t",
                voters[i].Votes[j].Goodness );
      }
      // write the speed votes to the file
      for( int j = 0; j<NUMARCS; j++ )
      {
        fprintf(voteLogFileVector[i], "%5.2f\t",
                voters[i].Votes[j].Velo );
      }
      fprintf(voteLogFileVector[i], "\n");
    }
  }
}

void Log::closeLogs()
{
  if(enabled)
  {
    // State log 
    fclose(statefile);
    // Event log
    *this << "Closing event log..." << endl;
    close();
    // Voter logs
    for( int i = 0; i<ArbiterInput::Count; i++)
    {
      fclose( voteLogFileVector[i] );
    }
  }
}
