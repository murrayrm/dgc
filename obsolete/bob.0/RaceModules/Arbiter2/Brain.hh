#ifndef BRAIN_HH
#define BRAIN_HH
#include "ArbiterDatum.hh"
using namespace std;

//##########################################################################
//   Constants
//##########################################################################

/*! If an input has not been updated in a certain period of time it
 * will be considered invalid. */
const Timeval INPUT_TIMEOUT[] = {
  Timeval(1 ,200000), // LADAR (added 200ms so it's not the same as Stereo)
  Timeval(1, 0), // Stereo
  Timeval(1, 0), // StereoLR
  Timeval(1, 0), // Global
  Timeval(1, 0), // DFE
  Timeval(1, 0), // CorrArcEval
  Timeval(1, 0), // FenderPlanner
  Timeval(1, 0), // RoadFollower
  Timeval(1, 0) // PathEvaluation
};

/*! Timeout for update of state from VState */
const Timeval UPDATE_STATE_TIMEOUT(1,0);

//##########################################################################
//   Brain class
//##########################################################################

/*! Brain. Has responsibility to decide the command depending on the different 
 * modes of circumstances and the eventual timeouts for voters etc.
 * $Id$
 */
class Brain
{
public:
  /*! Constructor */
  Brain(ArbiterDatum& datum);
  
  ~Brain();

  /*! Calculates the steering command
   * \param current_voters   A list of (only) the current valid voters
   * \param now              The time value we're going to check for timeouts against
   *                         (shall be time elapsed since the start of arbiter)
   * \return                 The resulting steering command.
   */
  VDrive_CmdMotionMsg calculateCommand(list<Voter> &current_voters);

  /*! Returns the most recent combined voter (for MatlabDisplay) */
  Voter getLastCombinedVote() { return voterHandler->getLastCombinedVoter(); }

private:
  ArbiterDatum* d;
  VoterHandler* voterHandler;

  /*! The current VDrive_CmdMotionMsg we're working on internally
   * in this class */
  VDrive_CmdMotionMsg cmd;

  /*! Decide which direction to go in and how to do that */
  void handleMovement();

  /*! Help method for the reverse handling */
  void reverse();

  /*! Help method for cleaner code and to avoid state spamming in the eventlog
   * \param state the new state
   * \return true if the state changed, false otherwise */
  bool setMovement(int state);
};
#endif
