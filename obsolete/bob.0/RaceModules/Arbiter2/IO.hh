#ifndef IO_HH
#define IO_HH

#include <vector> // readInputWeights
#include <deque> // readArbiterLog
#include "Voter.hh" 
using namespace std;

/** IO is a helper class for reading/writing and parsing the inputweights and 
 *  the arbiter log (for use in VoterSim).
 *  $Id$
 */
class IO {
public:
  /** let unit tests gain access to private methods */
  friend class TestIO; 
  
  /** Reads a vector of input weights from a text file.
   * It is important that all names are exactly as in ArbiterInput of the
   * ArbiterDatum and in the same order! You are allowed to have lines
   * starting with a # for comments.
   * Static so you don't need to create an IO object first 
   * @param filename the filename of the weights file to read from
   * @return the vector of weights
   */
  static vector<double> readInputWeights(const char* filename);
  
  /** writes a vector of input weights to a file.
   *  Static so you don't need to create an IO object first 
   * @param filename the filename to write to
   * @param weights the weights to write
   */
  static void writeInputWeights(const char* filename, const vector<double> weights);

  /** testing purposes only. This assumes the weights is in the
   * correct order according to ArbiterInput 
   * @param weights the weights to print
   */
  static void printWeights(const vector<double> weights);

  /** Reads a vector of deques from an arbiter log.
   * Each vector corresponds to a column in the log, so the first one
   * is Time[sec], the second one is Easting[m] and so on. 
   * @param filename the filename of the arbiter log to read from
   * @return the log
   */
  static vector<deque<double> > readArbiterLog(const char* filename);

  /** Reads a deque of Voters from a voter log. Each line in the log file
   * will become an element in the deque.
   * @param filename the filename of the file to read from
   * @return the deque of Voters
   */  
  static deque<Voter> readVoterLog(const char* filename);
  
  /** Reads a vector of deques from logged weights (used by GeneticAlgorithm).
   * Each vector corresponds to a column in the log. 
   * @param filename the filename of the file to read
   * @return the log
   */
  static vector<deque<double> > readWeightsLog(const char* filename);
  
  /** Reads a vector of booleans that tells if the voters are enabled of not.
   * This is performed because we only want the genetic algorithm to work with
   * the enabled voters, so it doesn't get confused by the voters that weren't 
   * active (thinking that they voted 0 for some unknown reason).
   * @param filename the filename of the file to read
   * @return vector of ArbiterInput indexes, for each voter true/false enabled.
   */
  static vector<bool> readGeneticAlgorithmConfig(const char* filename);

};

#endif
