/**
 * $Id$
 */
#include "IO.hh"
#include "ArbiterDatum.hh" // ArbiterInput struct

#include <iostream> // cout
#include <fstream> // ifstream
#include <string>
#include <boost/tokenizer.hpp>
using namespace std;

#define DEBUG false // true if you want more printouts

vector<double> IO::readInputWeights(const char* filename)
{
  ifstream file(filename);
  if(!file.is_open())
  {
    printf("IO::readInputWeights: Cannot read file '%s'\n", filename);
    exit(1);
  }
  vector<double> weights;
  string line;
  while(getline(file, line))
  {
    // don't read commented line or empty lines
    if(line[0] != '#' && line.size() > 0)
    {
      int spaceIndex = line.find_first_of(" ");
      weights.push_back(atof(line.substr(spaceIndex+1, line.size()).c_str()));
    }
  }
  file.close();
  return weights;
}

//#############################################################################

void IO::writeInputWeights(const char* filename, const vector<double> weights)
{
  ofstream file(filename);
  if(!file.is_open())
  {
    printf("IO::writeInputWeights: Cannot open file for writing: '%s'\n", filename);
    exit(1);
  }

  // check that the size of the vector is correct first:
  if(weights.size() != ArbiterInput::Count)
  {
    printf("IO::writeInputWeights: Invalid weight vector length: %d\n",  weights.size());
    return;
  }

  for(unsigned int i = 0; i != weights.size(); i++)
  {
    file << ArbiterInputStrings[i] << " " << weights.at(i) << endl;
  }
  file << endl; // finish with an extra endline

  file.close();
}

//#############################################################################

void IO::printWeights(const vector<double> weights)
{
  printf("Printing input weights (size = %d)\n", weights.size());
  for(unsigned int i = 0; i < weights.size(); i++)
  {
    cout << left << setw(15) << ArbiterInputStrings[i]
    << setw(6) << setiosflags(ios::fixed) << setprecision(1) << weights.at(i) << endl;
  }
}
//#############################################################################

vector<deque<double> > IO::readArbiterLog(const char* filename)
{
  if(DEBUG) printf("IO::readArbiterLog: reading file '%s'\n", filename);
  ifstream file(filename);
  if(!file.is_open())
  {
    printf("IO::readArbiterLog: Cannot read file '%s'\n", filename);
    exit(1);
  }

  char buffer[1024];
  while(file.peek() == '%') // read through all the remaining commented lines
  {
    file.getline(buffer, 1024);
    if(DEBUG) printf("%s\n", buffer);
  }

  // count number of columns
  int nbrCols = 0;
  string line;
  getline(file, line); // first line of log data
  typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
  boost::char_separator<char> sep(" \t"); // spaces or tabs
  tokenizer tok(line, sep);
  for(tokenizer::iterator tok_iter = tok.begin(); tok_iter != tok.end();++tok_iter)
  {
    nbrCols++;
  }
  if(DEBUG) cout << "Number of columns: " << nbrCols << endl;

  // now start reading the values
  vector<deque<double> > arbiterLog(nbrCols);

  do // because the first line is already read
  {
    tok.assign(line, sep); // feed a new line to the tokenizer
    int col = 0;
    for(tokenizer::iterator tok_iter = tok.begin(); tok_iter != tok.end();++tok_iter)
    {
      if(DEBUG) cout << " pushing back " << *tok_iter << " at col " << col << endl;
      arbiterLog.at(col++).push_back(atof((*tok_iter).c_str()));
    }
    if(DEBUG) cout << endl;
  }
  while(getline(file, line));
  if(DEBUG) cout << " returning arbiterLog with " << arbiterLog.size()
    << " columns " << endl;
  return arbiterLog;
}

//#############################################################################

deque<Voter> IO::readVoterLog(const char* filename)
{
  if(DEBUG) printf("IO::readVoterLog: reading file '%s'\n", filename);
  ifstream file(filename);
  if(!file.is_open())
  {
     printf("IO::readVoterLog: Cannot read file '%s'\n", filename);
    exit(1);
  }

  string line;
  while(file.peek() == '%') // read through all the commented lines
  {
    getline(file, line);
    if(DEBUG) cout << line << endl;
  }

   // last line shall contain NUMARCS constant
  // lets parse it
  int index = line.find_first_of("=");
  int nbrArcs = atoi(line.substr(index+1, line.size()).c_str());
  if(nbrArcs != NUMARCS)
  {
    cout << "NUMARCS from file doesn't match with current implementation"
    << " of NUMARCS in source file, cannot continue (read " << nbrArcs
    << " from file)" << endl;
    exit(1);
  }
  if(DEBUG) cout << "nbrArcs: " << nbrArcs << endl;
  
  // now start reading the values
  deque<Voter> voterData;
  Voter voter;
  typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
  boost::char_separator<char> sep(" \t"); // spaces or tabs
  tokenizer tok(line, sep);
  
  while(getline(file, line))
  {
    if(line.size() > 1) { // skip empty lines
      tok.assign(line, sep); // feed a new line to the tokenizer
      int col = 0;
      for(tokenizer::iterator tok_iter = tok.begin(); tok_iter != tok.end();++tok_iter)
      {
        // Value 0: a timestamp
        // Value 1 to nbrArcs: Goodness values
        // Value nbrArcs+1 to 2*nbrArcs: Speed values
        if(col > 0 && col <= nbrArcs)
        {
          voter.Votes[col-1].Goodness = atof((*tok_iter).c_str()); // goodness value
          if(DEBUG) cout << " Goodness: " << *tok_iter << " at col " << col << endl;
        }
        else if(col > nbrArcs && col <= 2*nbrArcs)
        // after the goodnesses comes the speeds (nbrArcs values)
        {
          voter.Votes[col-1-nbrArcs].Velo = atof((*tok_iter).c_str());
          if(DEBUG) cout << " Speed: " << *tok_iter << " at col " << col << endl;
        }
        col++;
      }  
      // now put this voter into the voterData dequeue
      voterData.push_back(voter);
    }
  }
  

  if(DEBUG) cout << "IO::readVoterLog: returning voterData with " 
    << voterData.size() << " rows " << endl;
  return voterData;
}

//#############################################################################

vector<deque<double> > IO::readWeightsLog(const char* filename)
{
  if(DEBUG) printf("IO::readWeightsLog: reading file '%s'\n", filename);
  ifstream file(filename);
  if(!file.is_open())
  {
    printf("IO::readWeightsLog: Cannot read file '%s'\n", filename);
    exit(1);
  }


  int nbrCols = 0;
  char buffer[1024];
  while(file.peek() == '%') // read through all the remaining commented lines
  {
    file.getline(buffer, 1024);
    if(DEBUG) cout << buffer << endl;
  }

  // count number of columns
  string line;
  getline(file, line); // first line of log data
  typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
  boost::char_separator<char> sep(" \t");
  tokenizer tok(line, sep);
  for(tokenizer::iterator tok_iter = tok.begin(); tok_iter != tok.end();++tok_iter)
  {
    nbrCols++;
  }
  cout << endl;

  if(DEBUG) printf("Number of columns: %d\n", nbrCols);

  // now start reading the values
  vector<deque<double> > weightsLog(nbrCols);

  do // because the first line is already read
  {
    tok.assign(line, sep); // feed a new line to the tokenizer
    int col = 0;
    for(tokenizer::iterator tok_iter = tok.begin(); tok_iter != tok.end();++tok_iter)
    {
      if(DEBUG) cout << " pushing back " << *tok_iter << " at col " << col << endl;
      weightsLog.at(col++).push_back(atof((*tok_iter).c_str()));
    }
    if(DEBUG) cout << endl;
  }
  while(getline(file, line));
  if(DEBUG) cout << " returning weightsLog with " << weightsLog.size()
    << " columns " << endl;
  return weightsLog;
}

//#############################################################################

vector<bool> IO::readGeneticAlgorithmConfig(const char* filename)
{
  ifstream file(filename);
  if(!file.is_open())
  {
    printf("IO::readGeneticAlgorithmConfig: Cannot read file '%s'\n", filename);
    exit(1);
  }
  vector<bool> result;
  string line;
  while(getline(file, line))
  {
    // don't read commented line or empty lines
    if(line[0] != '#' && line.size() > 0)
    {
      int spaceIndex = line.find_first_of(" ");
      result.push_back(atoi(line.substr(spaceIndex+1, line.size()).c_str()));
    }
  }
  file.close();
  // Print the read config to the stdout:
  printf("Genetic Algorithm config:\n");
  printf("=========================\n");
  for(unsigned int i = 0; i < result.size(); i++)
  {
    printf("%14s: %d\n", ArbiterInputStrings[i], (int)result[i]);
  }
  printf("\n");
  return result;
}
