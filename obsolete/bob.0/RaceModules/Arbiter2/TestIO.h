/*! 
 * $Id$
 */
#include <cxxtest/TestSuite.h>
#include "IO.hh"
#include "ArbiterDatum.hh"
#include "MTA/Misc/Time/Timeval.hh"
#include "MTA/Misc/Time/Time.hh"

#include <iostream> // cout
using namespace std;

class TestIO : public CxxTest::TestSuite
{

private:
  ArbiterDatum datum;
  
public:
  void setUp()
  {
  }
  
  void tearDown() 
  {
  }

  void testReadArbiterLog()
  {
    vector<deque<double> > log = IO::readArbiterLog("tests/test_readArbiterLog.log");
  }  
  
  void testReadGeneticAlgorithmConfig() 
  {
    vector<bool> enabled_voters = IO::readGeneticAlgorithmConfig("GeneticAlgorithm/config");
    for(vector<bool>::const_iterator i = enabled_voters.begin(); i != enabled_voters.end(); i++) 
    {
      if(*i)
        printf("1\n");
      else
        printf("0\n");
     }
  }
};
