#ifndef VOTERHANDLER_HH
#define VOTERHANDLER_HH

#include "Voter.hh"
#include "vehlib/VDrive.hh" // VDrive_CmdMotionMsg sent to actuators
#include <list>
using namespace std;

//##########################################################################
//   Global things
//##########################################################################

/** Returns the steering angle for the (arcnum + 1)th arc.
 *  Helper function to determine phi based on left and right phi.
 *  Because NUMARCS is constant along with Left and Right Phi, we can
 *  determine the steering angle of a vote based on its index.
 *  (Votes[0] has angle of LeftPhi and Votes[NUMARCS-1] has angle RightPhi)
 *  arcnum ranges from 0 to NUMARCS-1), so arcnum=0 -> hard left,
 *  arcnum=NUMARCS-1 -> hard right
 * @param the arc number
 * @return the steering angle
 */
double GetPhi(int arcnum);

/** Returns the arc number (index) for the input 
 * steering angle (in radians). Inverse of GetPhi(int arcnum).
 * @param steering angle in radians
 * @return The arc's number, -1 if the angle is out of range.
 */
int GetArcNum( double phi );

//##########################################################################
//   VoterHandler class
//##########################################################################

/** VoterHandler is responsible for combining the votes and pick the proper
 * command depending on what the last commanded steering was and some other 
 * factors. It has logic to select the best of a set of equal voters.
 * $Id$
 */
class VoterHandler {
public:
  friend class TestVoterHandler; // to let unit tests gain access to private methods

  /** Constructor */
  VoterHandler(bool shapeVotesEnabled) : doShapeVotes(shapeVotesEnabled) {}
  
  /** Takes all Voters, combines their votes, picks the best arc based on
   *  the combined goodness values, and returns the best steering and velocity
   *  commands.
   * @param current_voters  A list of (only) the current valid voters
   * @param lastPhi         the last steering angle
   * @return                The resulting steering command.   
   */
  VDrive_CmdMotionMsg calculateCommand(list<Voter> &current_voters, double lastPhi);

  /** Returns a reference to the combined voter */
  Voter& getLastCombinedVoter() { return combinedVoter; }
  
private: 

  /** If we're going to use shapeVotes or not */
  bool doShapeVotes;

  /** The combined voter is kept in this class, it can be retrieved, for example
   *  when we want to display it in the sparrow interface/MatlabDisplay */
  Voter combinedVoter;
     
  /** For each possible arc, this function
   * will compare the different input sources and combine them into one.
   * For now this just takes a weighted sum of the input sources.
   * @param voters   A list of (only) the current valid voters
   * @param USE_DFE  whether to use DFE_veto for combining votes or not.
   * @return a Voter that contains the "best value" for the given arcs.
   */
  Voter combineVotes(list<Voter> &current_voters, bool USE_DFE);
  
  /** Picks the highest scored arc out of all considered in the input Voter and 
   * returns it in the format to be sent to the driving module.
   * - If more than one arc has the same goodness, the center arc out of that subset 
   *   is returned.
   * - If two or more spread out arcs have the same goodness, the one
   *   closest to lastPhi is picked
   * - If all arcs have zero goodness, the one closest to lastPhi is picked
   * @param combined the combined vote
   * @param lastPhi the last commanded steering angle
   * @return the "best" velocity + steering angle
   */
  VDrive_CmdMotionMsg pickBestArc(Voter &combined, double lastPhi);

  /** This method looks at the commanded phi and the last commanded phi. If
  * the last phi is one of the center 3 arcs, the commanded phi gets smooted
  * to we avoid zig-zagging as we would do otherwise because the angle step 
  * difference is so large between the arcs.
  * @param cmd the cmd we shall inspect and maybe alter
  * @param lastPhi the last commanded steering command
  */
  void smoothCmd(VDrive_CmdMotionMsg &cmd, double lastPhi);
  
  //##########################################################################
  //  Wasn't used in the RACE 2004:
  //##########################################################################
  
  /** For each arc of the given Voter's vote, this function assigns a new
   * goodness. The new goodness for an arc is the weighted average of the arc's
   * goodness and its neighbor arcs' goodnesses. First we compare the arc to its
   * left neighbor, take the weighted average if its goodness is greater than
   * that of the neighbor. Then do the same with the right neighbor, and take
   * the average of the two weighted averages. Note that an arc's goodness is
   * only modified if it's higher than those of the neighboring arcs'
   * goodnesses by the given threshold.
   *
   * Note: We did not use ShapeVotes in the race/QID (March, 2004) due to the
   *       lack of performance testing with it. See ARBITER_SPEC.txt for more.
   *
   * @param &v  the Voter whose votes we're shaping.
   * @param threshold if the difference between the goodness of this arc
   *                  and that of the neighbor is less than this parameter,
   *                  then we do not modify the goodness of this arc.
   * @param favor_high_factor    when taking the weighted average, how much more
   *            weight is given to this arc which has a higher goodness value than the
   *            neighbor we're comparing it to. Since this factor is used twice
   *            (once with the left neighbor and once with the right), to get the average
   *            of three adjacent arcs for the new goodness of the middle arc, use
   *            favor_high_factor = 1/2, threshold = 0.
   */
  void shapeVotes(Voter &v, double threshold, double favor_high_factor);
};

#endif
