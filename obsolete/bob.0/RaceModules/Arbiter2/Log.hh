#ifndef LOG_HH
#define LOG_HH

#include <vector> // voteLogFileVector
#include <fstream>
#include "vehlib/VStarPackets.hh" // Steer_Packet
#include "vehlib/VState.hh" // VState_GetStateMsg sent via MTA
#include "vehlib/VDrive.hh" // VDrive_CmdMotionMsg sent to actuators
#include "Voter.hh"
using namespace std;

/*! The Log class handles three different kinds of logging:
 * 1. State logging: logging of all the arbiter state information and 
 *    the commanded steering and velocity
 * 2. Voter logging: logging of all the voters voting numbers
 * 3. Event logging: logging of events, such as reached waypoints and debug info.
 *    A good way to see what's really going on in the arbiter is to do 
 *    "tail -f eventlogfilename.log" in a terminal window while running the arbiter.
 */
/*! Log is intented to be used as a global logger class (use extern Logger in 
 * each file that is supposed to use it). Log uses several different log files:
 * <ul>
 *   <li>State log: Logs the Arbiter state in each Arbiter loop
 *   <li>Event log: Logs everything that is sent to Logger with the << operator
 *   <li>Voter logs: One log file for each voter, logs all it's votes
 * </ul>
 * $Id$
 */class Log : public ofstream
{
public:
  /*! Constructor. The log is disabled by default */
  Log() { enabled = false; }

  /*! Initializes the state, event and the Voter logfiles 
   * \param datum the arbiter datum
   * \param file_prefix  the prefix to be used for the filenames (except debug.log) */
  void init(bool isEnabled, char* file_prefix = "test");
  
  /*! Logs the arbiters current state, commands and all the voters votes.
   * Currently called from the main Arbiter loop when a decision is taken.  */
  void logState(VDrive_CmdMotionMsg& cmd, 
		VState_GetStateMsg& SS, 
		Voter* voters,
		Steer_Packet& sp);
  
  /*! Closes the log files. Meant to be called before shutdown */
  void closeLogs();
    
private:
  /*! state and command log */
  FILE * statefile;
  
  /*! Voter logs */
  vector<FILE *> voteLogFileVector;

  /*! If the logger is enabled or disabled */
  int enabled;
};

/*! Declare itself as a global class */
extern Log logger;

#endif
