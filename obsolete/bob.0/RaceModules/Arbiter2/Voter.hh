/*! 
 * $Id$
 */
#ifndef VOTER_HH
#define VOTER_HH

#include "Vote.hh"
#include <ostream> // ostream
using namespace std;

const int    NUMARCS      = 25;             // will crash if numarcs < 2

/*! A voter - every input will fill one of these up.
 * A simple data structure with a copy operator */
class Voter {

public:
  /*! The votes for the different arcs (goodness and speed) */
  Vote Votes[NUMARCS];
  int ID;
  
  /*! The weight to be taken into account when checking how important
   * this voter is. */
  double VoterWeight;
  
  /*! Constructor, initializes the weight to a default 1.0 althought
   * the constructor with id and weight should always be used! */
  Voter() : ID(0), VoterWeight(1.0) {}
  
  /*! Constructor. Creates a Voter object
   * \param id     the voter ID (defined in ArbiterInput struct)
   * \param weight the voter's weight. Will be taken into account in 
   *               vote combining. */
  Voter(int id, double weight) : ID(id), VoterWeight(weight) {}
  
  /*! clears out the votes, sets the goodness to 0 and velocity to NO_VOTE_VELO \
   * (which is higher than any possible speed). */
  void clearVotes();

  /*! Watch out when using this operator, since it overwrites the Voterweight
   * and Voters sent by planner modules don't know their own weights.
   * May want to update Voterweight using the ID and InputWeights[] in 
   * ArbiterDatum.hh. */
  Voter& operator= (const Voter rhs);

  /*! Operator to enable sending a voter to for example cout */
  friend ostream& operator<< (ostream& os, const Voter& v);

};

#endif
