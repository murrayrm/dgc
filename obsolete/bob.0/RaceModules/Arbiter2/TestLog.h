/*! 
 * $Id$
 */
#include <cxxtest/TestSuite.h>
#include "Log.hh"
#include "MTA/Misc/Time/Timeval.hh"
#include "MTA/Misc/Time/Time.hh"

#include <iostream> // cout
using namespace std;

extern Log logger;

class TestLog : public CxxTest::TestSuite
{

private:
  ArbiterDatum datum;
  //Log logger;
  
public:
  void setUp()
  {
    // sleep for one second so next test doesn't overwrite the logs
    usleep(1000000); 
    logger.init(true, "test");
  }
  
  void tearDown() 
  {
    logger.closeLogs();
  }

  void testOperator()
  {
    logger << "testOperator<<" << 123 << " hello  " << endl;
    // insert here some way to verify this, parsing the log? seems hard
  }
  
  void testLogState() 
  {
    VState_GetStateMsg SS;
    Voter voters[2];
                  
    VDrive_CmdMotionMsg cmd;
    cmd.velocity_cmd = 123;
    cmd.steer_cmd = 0.1;
    Steer_Packet sp;
    sp.Steer = 123;
    logger.logState(cmd, SS, voters, sp);  
    // insert here some way to verify this, parsing the log? seems hard
  }
};
