/*! 
 * $Id$
 */
#include <cxxtest/TestSuite.h>
#include "VoterHandler.hh"
#include "MTA/Misc/Time/Timeval.hh"
#include "MTA/Misc/Time/Time.hh"
#include "ArbiterDatum.hh"

#include <iostream> // cout
using namespace std;

class TestVoterHandler : public CxxTest::TestSuite
{

private:
  VoterHandler* vh; // we dont care about shapevotes for now
  Voter v;
  VDrive_CmdMotionMsg cmd;

public:
  void setUp()
  {
    vh = new VoterHandler(false);
    // first check that NUMARCS is 25:
    TS_ASSERT_EQUALS(NUMARCS, 25);
    zeroVotes(v);
  }

  void tearDown() 
  {
    delete vh;
  }
  
  void zeroVotes(Voter& v)
  {
    for(int i=0; i < NUMARCS ;i++)
    {
      v.Votes[i] = Vote(0.0, 0.0);
    }

  }

  void testGetPhi()
  {
    // have to allow some difference because I cant save the exact double.
    double delta = 0.0001;
    double correct[] = {
                         -0.3568,-0.327067,-0.297333,-0.2676,-0.237867,
                         -0.208133,-0.1784,-0.148667,-0.118933,-0.0892,
                         -0.0594667,-0.0297333,0,0.0297333,0.0594667,
                         0.0892, 0.118933,0.148667,0.1784,0.208133,
                         0.237867,0.2676,0.297333,0.327067,0.3568};
    double phi;
    for(int i = 0; i < NUMARCS; i++)
    {
      phi = GetPhi(i);
      TS_ASSERT(correct[i] - delta < phi && phi < correct[i] + delta);
    }
  }

  void testGetArcNum()
  {
    int arc = 0;
    // left side
    for(double d = LEFTPHI; d <= 0; d += ((RIGHTPHI - LEFTPHI)/NUMARCS))
    {
      TS_ASSERT_EQUALS(GetArcNum(d), arc);
      arc++;
    }

    // right side
    arc = 12;
    // have to allow some difference because I cant compare double values exactly.
    double delta = 0.0001;
    for(double d = 0; d <= RIGHTPHI+delta; d += ((RIGHTPHI - LEFTPHI)/NUMARCS))
    {
      TS_ASSERT_EQUALS(GetArcNum(d), arc);
      arc++;
    }
  }

  /*! Test endpoint: set 0 to be a good arc */
  void testPickBestCombinedArc1()
  {
    v.Votes[0] = Vote(100, 10);
    // assert it picks arc number 0
    cmd = vh->pickBestArc(v, 0);
    TS_ASSERT_EQUALS(GetArcNum(cmd.steer_cmd), 0);
    TS_ASSERT_EQUALS(cmd.velocity_cmd, 10.0);
  }

  /*! Test a simple point: set arc 9 to be a good arc */
  void testPickBestCombinedArc2()
  {
    v.Votes[9] = Vote(10,1.12);
    // assert it picks arc number 9
    cmd = vh->pickBestArc(v, 0);
    TS_ASSERT_EQUALS(GetArcNum(cmd.steer_cmd), 9);
    TS_ASSERT_EQUALS(cmd.velocity_cmd, 1.12);
  }

  /*! Test endpoint: set arc 24 to be a good arc */
  void testPickBestCombinedArc3()
  {
    v.Votes[24] = Vote(10,1.12);
    // assert it picks arc number 24
    cmd = vh->pickBestArc(v, 0);
    TS_ASSERT_EQUALS(GetArcNum(cmd.steer_cmd), 24);
    TS_ASSERT_EQUALS(cmd.velocity_cmd, 1.12);
  }

  /*! test that it actually returns the center of the
   * arcs if more than one has same goodness: */
  void testPickBestCombinedArc4()
  {
    v.Votes[4] = Vote(10,1.12);
    v.Votes[5] = Vote(10,1.12);
    v.Votes[6] = Vote(10,1.12);
    v.Votes[7] = Vote(10,1.12);
    v.Votes[8] = Vote(10,1.12);
    // assert it picks arc number 6 now
    cmd = vh->pickBestArc(v, 0);
    TS_ASSERT_EQUALS(GetArcNum(cmd.steer_cmd), 6);
    TS_ASSERT_EQUALS(cmd.velocity_cmd, 1.12);
  }

  /*! Test that it actually returns the best vote when
   * there's other votes around  */
  void testPickBestCombinedArc5()
  {
    v.Votes[0]  = Vote(100,1.12);
    v.Votes[9]  = Vote(10,1.12);
    v.Votes[10] = Vote(10,1.12);
    v.Votes[11] = Vote(10,1.12);
    v.Votes[12] = Vote(10,1.12);
    v.Votes[13] = Vote(10,1.12);
    // assert it picks arc number 0 now
    cmd = vh->pickBestArc(v, 0);
    TS_ASSERT_EQUALS(GetArcNum(cmd.steer_cmd), 0);
    TS_ASSERT_EQUALS(cmd.velocity_cmd, 1.12);
    zeroVotes(v); // restore all Votes to 0
  }

  /*! Test that it actually returns the center of the
   * arcs if more than one has same goodness: */
  void testPickBestCombinedArc6()
  {
    v.Votes[10] = Vote(10,1.12);
    v.Votes[11] = Vote(10,1.12);
    v.Votes[12] = Vote(10,1.12);
    v.Votes[13] = Vote(10,1.12);
    v.Votes[14] = Vote(10,1.12);

    // assert it picks arc number 12 now
    cmd = vh->pickBestArc(v, 0);
    TS_ASSERT_EQUALS(GetArcNum(cmd.steer_cmd), 12);
    TS_ASSERT_EQUALS(cmd.velocity_cmd, 1.12);
  }

  /*! Test that it picks the arc closest to the last steering
   * angle when there's multiple arcs with the same goodness around  */
  void testPickBestCombinedArc7()
  {
    v.Votes[0]  = Vote(100,1.12);
    v.Votes[24] = Vote(100,1.34);
    // assert it picks arc number 24 if we put in a positive
    // lastPhi here
    cmd = vh->pickBestArc(v, GetPhi(20));
    TS_ASSERT_EQUALS(GetArcNum(cmd.steer_cmd), 24);
    TS_ASSERT_EQUALS(cmd.velocity_cmd, 1.34);
  }

  /*! Check that it picks the closest to the last if all votes are have 
   * 0 goodness. */
  void testPickBestCombinedArc8()
  {
    // assert it picks arc number 5
    cmd = vh->pickBestArc(v, GetPhi(5));
    TS_ASSERT_EQUALS(GetArcNum(cmd.steer_cmd), 5);
    TS_ASSERT_EQUALS(cmd.velocity_cmd, 0.0);
  }

  /*! Test several spread out arcs with same goodness  */
  void testPickBestCombinedArc9()
  {
    v.Votes[0] = Vote(10, 1.12);
    v.Votes[1] = Vote(10, 1.12);
    v.Votes[2] = Vote(10, 1.12);
    v.Votes[13] = Vote(10, 1.12);
    v.Votes[14] = Vote(10, 1.12);
    v.Votes[23] = Vote(10, 1.12);
    v.Votes[24] = Vote(10, 1.12);
    // assert it picks arc number 14 since that one is
    // closest to lastPhi = arc 15
    cmd = vh->pickBestArc(v, GetPhi(15));
    TS_ASSERT_EQUALS(GetArcNum(cmd.steer_cmd), 14);
    TS_ASSERT_EQUALS(cmd.velocity_cmd, 1.12);
  }

  /* Test pickBestArc performance */
  void testPickBestArcPerformance() 
  {
    // Fill in some arcs in different sequences
    v.Votes[0] = Vote(10, 1.12);
    v.Votes[1] = Vote(11, 1.12);
    v.Votes[2] = Vote(12, 1.12);
    v.Votes[5] = Vote(60, 12.12);
    v.Votes[6] = Vote(61, 12.12);
    v.Votes[7] = Vote(61, 12.12);
    v.Votes[8] = Vote(60, 12.12);
    v.Votes[9] = Vote(60, 12.12);
    v.Votes[10] = Vote(59, 12.12);
    v.Votes[13] = Vote(89, 1.0);
    v.Votes[14] = Vote(89, 1.0);
    v.Votes[18] = Vote(99, 20.0);
    v.Votes[23] = Vote(1, 1.0);
    v.Votes[24] = Vote(1, 1.0);
  
    Timeval now = TVNow();
    cmd = vh->pickBestArc(v, GetPhi(8));
    // Assert it took less than 30 micro seconds
    long elapsed_time = (TVNow() - now).usec();
    TS_ASSERT(elapsed_time < 30);
    //cout << "Doing one pickBestArc call took " << elapsed_time << " us" << endl;
  
  }
  
  /*! Tests combining of a couple of votes.
   * Verifies that it picks the lowest speed and that the goodness is an average. */
  void testCombineVotes1()
  {
    list<Voter> voters;
    // add two voters
    voters.push_back(Voter());
    voters.push_back(Voter());

    list<Voter>::iterator iter = voters.begin();
    for(int i = 0; i < NUMARCS ;i++)
    {
      (*iter).Votes[i] = Vote(10, 2.0);
    }
    iter++; // move forward to the second voter
    for(int i = 0; i < NUMARCS ;i++)
    {
      (*iter).Votes[i] = Vote(20, 4.0);
    }
    // Print votes for debugging
    /*iter--;
    cout << (*iter);
    iter++;
    cout << (*iter);
    */
    
    // DFE voter is number 5 (index 4) but it doesn't matter
    // if we set USE_DFE to true:
    Voter combined = vh->combineVotes(voters, true);
    //cout << combined << endl;
    // using round since it's hard to compare doubles exact
    TS_ASSERT_EQUALS(round(combined.Votes[0].Goodness), 15.0);
    TS_ASSERT_EQUALS(round(combined.Votes[0].Velo), 2.0);
    TS_ASSERT_EQUALS(round(combined.Votes[1].Goodness), 15.0);
    TS_ASSERT_EQUALS(round(combined.Votes[1].Velo), 2.0);
    TS_ASSERT_EQUALS(round(combined.Votes[2].Goodness), 15.0);
    TS_ASSERT_EQUALS(round(combined.Votes[2].Velo), 2.0);
  }

  void testCombineVotesPerformance()
  {
    list<Voter> voters;
    // add six voters
    for(int i=0; i < 6 ;i++)
    {
      voters.push_back(Voter());
    }

    // fill their votes
    list<Voter>::iterator iter;
    for(iter = voters.begin(); iter != voters.end(); iter++)
    {
      for(int i = 0; i < NUMARCS ;i++)
      {
        (*iter).Votes[i] = Vote(i*2, i);
      }
    }

    Voter combined;
    Timeval now = TVNow();
    combined = vh->combineVotes(voters, true);
    // Assert it took less than 30 micro seconds
    long elapsed_time = (TVNow() - now).usec();
    TS_ASSERT(elapsed_time < 30);
    //cout << "Doing one combineVotes call took " << elapsed_time << " us" << endl;
  }
};
