/*! 
 * $Id$
 */
#include "Arbiter.hh"
#include "IO.hh" // readInputWeights
#include "MTA/Misc/Thread/ThreadsForClasses.hpp" //RunMethodInNewThread
#include "MTA/Misc/Time/Time.hh" //RunMethodInNewThread
#include "vehlib/VStarPackets.hh" // Steer_Packet
#include "waypoints.hh"
#include "Log.hh"
#include <string>
#include <boost/thread/recursive_mutex.hpp>
using namespace std;

#define DEBUG false // set to true for more debug logging

/** A global reference to the logger object */
extern Log logger;

/** A global reference to the number of requested States for display stats */
extern int requestedStateCount;

/** A global reference to find out if the user has quitted through the sparrow
 * user interface */
extern bool SPARROW_QUIT;


/*! Sleep time for each loop when waiting for modules.
 * We don't use this for missing obstacle avoidance modules, since we want
 * to check for them as quickly as possible, because we don't stop the
 * vehicle in this case as in others.
 * (1 second = 1Hz) */
const long SLEEP_WAIT_FOR_MISSING_MODULES = 1000000; // 1 second

Arbiter::Arbiter(int flags)
    : DGC_MODULE(MODULES::Arbiter, "Arbiter", 0)
{
  // split the flag int to the Datum variables
  d.globalAvailable = (flags & NO_GLOBAL) == 0 ;
  d.stateAvailable = (flags & NO_STATE) == 0;
  d.obstacleAvailable = (flags & NO_OBSTACLE) == 0;
  d.reverseEnabled = (flags & NO_REVERSE) == 0;
  d.vdriveEnabled = (flags & NO_VDRIVE) == 0;
  d.shapeVotesEnabled = (flags & SHAPEVOTES) != 0;
  useSparrowDisplay = (flags & NO_DISP) == 0;
  /*logger << "d.globalAvailable : " << d.globalAvailable << endl;
  logger << "d.stateAvailable : " << d.stateAvailable << endl;
  logger << "d.obstacleAvailable : " << d.obstacleAvailable << endl;
  logger << "d.reverseEnabled : " << d.reverseEnabled << endl;
  logger << "d.vdriveEnabled : " << d.vdriveEnabled << endl;
  logger << "d.shapeVotesEnabled : " << d.shapeVotesEnabled << endl;
  */
}

Arbiter::~Arbiter()
{
  delete brain;
}

void Arbiter::Init()
{

  // MTA init stuff
  DGC_MODULE::Init();
  
  // read voter input weights from file
  InputWeights = IO::readInputWeights("input_weights");
  IO::printWeights(InputWeights);
  // Log the input weights to the event log
  logger << "Using input weights:" << endl;
  for(unsigned int i=0; i < InputWeights.size() ;i++)
  {
    logger << "  " << ArbiterInputStrings[i] << " : " << InputWeights[i] << endl;
  }
  
  // Brain initialization
  brain = new Brain(d);

  // Arbiter Datum Initialization
  // Initialize the voter weight that we will use to that defined in
  // ArbiterDatum
  for( int i = 0; i < ArbiterInput::Count; i++)
  {
    d.allVoters[i].VoterWeight = InputWeights[i];
  }
  // initalize the state struct
  // (assumes good conditions, bad contitions has to be detected)
  d.state.movement = MOVEMENT::FORWARD;
  d.state.terrain = TERRAIN::LIGHT_TERRAIN;
  d.state.lighting = LIGHTING::GOOD_LIGHTING;
  d.state.dustyness = DUSTYNESS::NOT_DUSTY;

  d.currCommandIndex = 0;
  d.lastCommand[d.currCommandIndex].steer_cmd = 0;
  d.lastCommand[d.currCommandIndex].velocity_cmd = 0;

  logger << d.now << " Arbiter Init Finished" << endl;

  printf("Arbiter init finished\n");      
  
  if(useSparrowDisplay) 
  {
    RunMethodInNewThread<Arbiter>( this, &Arbiter::SparrowDisplayLoop);
    RunMethodInNewThread<Arbiter>( this, &Arbiter::UpdateSparrowVariablesLoop);
  }
}

void Arbiter::Active()
{
  d.TimeZero = TVNow();
  Timeval duration; // for the periodic thread sleeping

  // reset the update counters
  for(int i= 0; i<ArbiterInput::Count; i++)
  {
    d.UpdateCount[i] = 0;
  }
  d.UpdateCountState = 0;
  d.CombinedUpdateCount = 0;
  
  // The main command loop
  // EVENTUALLY code inside the loop below should be a method/methods called
  // in Update message handler, with no sleep (for synchronization with Voters)
  // or for faster processing with LADAR (if optimized). If not, well...
  while( ContinueInState() && !SPARROW_QUIT )
  {
    d.now = TVNow() - d.TimeZero; // time relative to start time
        
    updateState(); // update the state from VState    
    waitForModules(); // wait for state, global, ostacle avoidance
    updateVoterList();
    
    // calculate command
    d.toVDrive = brain->calculateCommand(voterList);

    // Send command to VDrive (unless disabled)
    if(d.vdriveEnabled)
    {
      // then prepare a new command
      prepareVDriveCommand(d.toVDrive);

      // And ship it off to VDrive
      Mail msg =
        NewOutMessage( MyAddress(), MODULES::VDrive, VDriveMessages::CmdMotion);
      msg << d.toVDrive;
      SendMail(msg);

      /* // Old simulation, not used anymore
      Mail msg2 = NewOutMessage( MyAddress(), MODULES::SimVDrive,
				 VDriveMessages::CmdMotion);
      msg2 << d.toVDrive;
      SendMail(msg2);
      */
    }
    
    // Sleeping because we share CPU with Vdrive, DFEPlanner, GlobalPlanner
    // NOTE: Currently hitting 20Hz, may want to sleep less to utilize faster
    // Voters' votes and make quicker decisions in the future. Right now,
    // path evaluation is too slow for us to send commands any faster.

    // The following is a proper thread sleep the remaining time of the period
    d.now = d.now + ACTIVE_PERIOD;
    duration = d.now - (TVNow() - d.TimeZero);
    if(duration > Timeval(0,0)) { // means we have time left in our window, sleep that time
      usleep(duration.usec());
      //logger << "Slept " << duration.usec() << " us" << endl; // debug
    }

    // Log data
    Steer_Packet sp;
    // for checking, since we once had problems with this packet not filled in
    sp.ActualSteer = -999.9;
    Get_Steer_Packet(MyAddress(), sp);
    if(sp.ActualSteer == -999.9)
    {
      logger << d.now << " Get_Steer_Packet didn't change the Steer_Packet!!!" << endl;
    }
    logger.logState(d.toVDrive, d.SS, d.allVoters, sp);
  } // end of while( ContinueInState() )

  if(SPARROW_QUIT)
  {
    printf("Got quit request by user...\n");
  }
  // Stop the car at the end of a demo
  d.toVDrive.velocity_cmd = 0;
  d.toVDrive.steer_cmd = 0;
  prepareVDriveCommand(d.toVDrive);
  Mail msg =
    NewOutMessage( MyAddress(), MODULES::VDrive, VDriveMessages::CmdMotion);
  msg << d.toVDrive;
  SendMail(msg);
  printf("Sent stop command to VDrive, shutting down...\n");
  Shutdown();
}

void Arbiter::updateState()
{
  Mail msg = NewQueryMessage( MyAddress(), MODULES::VState,
                              VStateMessages::GetState);
  Timeval tv = TVNow();
  Mail reply = SendQuery(msg);
  requestedStateCount++; // for stats
  if(DEBUG) logger << " SendQuery for state took " << (TVNow() - tv).usec() << " us ";
  if (reply.OK() )
  {
    if(DEBUG) logger << " (OK state)" << endl;
    reply >> d.SS; // download the new state
    d.lastUpdateState = TVNow() - d.TimeZero;
    d.UpdateCountState++;
  }
  else
  {
    if(DEBUG) logger << " (empty state)" << endl;
  }
}

void Arbiter::Standby()
{
  while( ContinueInState() )
  {
    usleep(100000); // just wait until we are ready to go (not currently used)
  }
}

void Arbiter::Shutdown()
{
  logger << d.now << " ---------Arbiter: Shutting Down-------" << endl;
  logger.closeLogs();
  printf("Logs closed, exiting program...\n");
  exit(0);
}

Mail Arbiter::QueryMailHandler(Mail& msg)
{
  Mail reply;      // allocate space for the reply

  // figure out what we are responding to
  switch(msg.MsgType())
  {
  case ArbiterMessages::GetVotes: // called only from MatlabDisplay
    {
      reply = ReplyToQuery(msg);
      Voter ms;
      for(int i=0; i<ArbiterInput::Count; i++)
      {
        ms = d.allVoters[i];
        reply << ms;
      }
      Voter combined = brain->getLastCombinedVote();
      reply << combined;
      reply << d.lastCommand[d.currCommandIndex];
      //logger << " Sending d.toVDrive.velocity_cmd = " << d.toVDrive.velocity_cmd << endl;
      //logger << " Sending d.toVDrive.steer_cmd = " <<  d.toVDrive.steer_cmd << endl;
      break;
    }
  default:
    reply = DGC_MODULE::QueryMailHandler(msg);
  } // switch
  return reply;
}

void Arbiter::InMailHandler(Mail & msg)
{
  switch (msg.MsgType())
  {
  case ArbiterMessages::Update:
    {
      // Update the votes for an input source
      int id;
      Voter vtr;
      msg >> id >> vtr;

      // Check voter index for safety
      if( 0 <= id && id < ArbiterInput::Count)
      {
        // update the datum
        if(DEBUG) logger << d.now << " Update from " << ArbiterInputStrings[id] << endl;
        boost::recursive_mutex::scoped_lock s(d.voterLock);
        d.UpdateCount[id]++;
        
        // This was the original way to handle arbiter vote update messages
        // It overwrites the weight that we are using for our voter (bad.)
        //d.allVoters[id] = vtr;
        // Here's a new way that only overwrites the votes
        for( int i = 0; i < NUMARCS; i++ )
        {
          d.allVoters[id].Votes[i] = vtr.Votes[i];
        }
        // Can't hurt to set the ID as well
        d.allVoters[id].ID = vtr.ID;

        // Currently, we only use the clock on Arbiter's computer
        // set the lastUpdate time to the relative time compared to the 
        // arbiter startup timestamp.
        d.lastUpdate[id] = TVNow() - d.TimeZero;
      }
      else
      {
        logger << d.now << " Invalid voter ID: " << id << endl;
      }
      break;
    } // case ArbiterMessages::Update:
  case ArbiterMessages::WaypointReached:
    {
      // Update the votes for an input source
      int id;
      waypoint wp;
      msg >> id >> wp;
      // Check voter id for safety
      if( id != ArbiterInput::Global)
      {
        logger << d.now << " Invalid voter ID: " << id 
        << " (should be Global)" << endl;
      }

      logger << d.now << " Waypoint " << wp.num 
      << " (N:" << wp.northing 
      << " E:" << wp.easting
      << ") reached:! At arbiter state N:" << d.SS.Northing 
      << " E: " << d.SS.Easting << endl;
      break;
    } // case ArbiterMessages::WaypointReached
  default:
    // let the parent mail handler handle it
    DGC_MODULE::InMailHandler(msg);
    break;
  } // switch
}

void Arbiter::updateVoterList()
{
  // clear out any old voters from the list
  voterList.clear();

  { // lock the vote sources
    boost::recursive_mutex::scoped_lock s(d.voterLock);

    // Update Voter attributes for this cycle
    for(int i = 0; i<ArbiterInput::Count; i++)
    {
      if( (d.now - d.lastUpdate[i]) > INPUT_TIMEOUT[i])
      {
        // It has been too long since this input has been updated: ignore it.
        // Reinitialize this Voter's votes and weight, mostly for display
        // purposes.
        d.allVoters[i].VoterWeight = 0.0;
        for( int phiInd=0; phiInd<NUMARCS; phiInd++)
        {
          d.allVoters[i].Votes[phiInd].Goodness = 0.0;
          d.allVoters[i].Votes[phiInd].Velo = 0.0;
        }
      }
      else
      { // Input not timed-out

        // If this Voter's LADAR is resetting, don't use it
        if (d.allVoters[i].Votes[NUMARCS/2].Goodness == LADAR_RESET)
        {
          d.allVoters[i].VoterWeight = 0.0;
        }
        else
        {
          // A valid Voter.
          // Set the weight properly and add it to the list of valid inputs
          d.allVoters[i].VoterWeight = InputWeights[i];
          d.allVoters[i].ID = i;
          voterList.push_back( d.allVoters[i] );
        }
      }
    } // end of for(arbiter inputs)
  } // Finish locking Voters.
}


void Arbiter::prepareVDriveCommand(VDrive_CmdMotionMsg& cmd)
{
  d.CombinedUpdateCount++; // update the counter

  // Store into datum
  // NOTE: May want to change to system time instead of local
  // Currently, we only use the clock on Arbiter's computer. Unless we want
  // to make sure other modules are sending the latest data, or Vdrive is
  // using the universal system clock to check if arbiter's commands are
  // recent, we have no reason to store the universal system time.
  // Is the universal system time even being used anywhere?
  cmd.Timestamp = TVNow();
  // steer_cmd in [-1,1]
  cmd.steer_cmd = cmd.steer_cmd / MAX_STEER;

  ////////////DEBUG--BECAUSE THIS SHOULDN'T HAPPEN//////////////////////
  if (fabs(cmd.steer_cmd) > 1.0)
  {
    logger << d.now << " Steer command out of range: " << cmd.steer_cmd << endl;
    if (cmd.steer_cmd < 0)
      cmd.steer_cmd = -1.0;
    else
      cmd.steer_cmd = 1.0;
  }

  // Store the command into history - to be used for arc-picking & REVERSE
  d.currCommandIndex = (d.currCommandIndex + 1) % NUM2REMEMBER;
  d.lastCommand[d.currCommandIndex].Timestamp = cmd.Timestamp;
  d.lastCommand[d.currCommandIndex].velocity_cmd = cmd.velocity_cmd;
  d.lastCommand[d.currCommandIndex].steer_cmd = cmd.steer_cmd;
}

void Arbiter::waitForModules()
{
  bool waitState = false;
  bool waitGlobal = false;
  
  // waiting for state from VState
  if( d.stateAvailable && d.UpdateCountState < 1)
  {
    waitState = true;
  }

  // waiting for GlobalPlanner
  if( d.globalAvailable && d.UpdateCount[ArbiterInput::Global] < 1 )
  {
    waitGlobal = true;
  }
  
  if(waitState || waitGlobal) 
  {
    sprintf(d.infoText, " Waiting for ");
    if(waitState)
    {
      strcat(d.infoText, "STATE");
      if(waitGlobal)
        strcat(d.infoText, " and GLOBAL");
    }
    else if(waitGlobal)
      strcat(d.infoText, "GLOBAL");

    // append this text also
    sprintf(d.infoText, "%s: sleeping %ld s each loop (sending 0 votes)", 
            d.infoText, 1000000/SLEEP_WAIT_FOR_MISSING_MODULES);
    logger << d.now << d.infoText << endl;  
    usleep(SLEEP_WAIT_FOR_MISSING_MODULES);
  }
  else 
  {
    // Clear the text so don't keep printing an old one
    sprintf(d.infoText, " ");
  }
}
