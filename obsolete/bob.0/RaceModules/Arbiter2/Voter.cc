/*! 
 * $Id$
 */
#include "Voter.hh"
#include <iomanip>
using namespace std;

Voter& Voter::operator= (const Voter rhs)
{
  ID = rhs.ID;
  VoterWeight = rhs.VoterWeight;
  for( int i=0; i<NUMARCS; i++)
  {
    Votes[i] = rhs.Votes[i];
  }
  return *this;
}

/*! Print this voter. Has to be global */
ostream& operator << (ostream& os, const Voter& v)
{
  os << " Vote no.   Goodness   Velocity" << endl
     << " ========   ========   ========" << endl;
  for(int i = 0; i < NUMARCS; i++)
  {
    os << setw(6) << i
    << "     " << setw(8) << v.Votes[i].Goodness
    << "     " << setw(8) << v.Votes[i].Velo << endl;
  }
  return os;
}

void Voter::clearVotes()
{
  // clear out the votes
  for(int i=0; i<NUMARCS; i++)
  {
    Votes[i].Goodness = 0;
    Votes[i].Velo = NO_VOTE_VELO;  // greater than any possible speed
  }
}

