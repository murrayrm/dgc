# Script that opens the latest arbiter eventlog
# If less is given as argument it uses less instead of tail to view the log!

#!/bin/bash

cd ~/dgc/bob/RaceModules/Arbiter2/logs

# list with newest event log files first
FILES=$(ls -t *event*)
LOG=$(echo $FILES | awk '{ print $1 }')

if test -e $LOG; then
    if [ "$1" == "less" ]; then
	echo Opening $LOG with less...
	less $LOG
    else
	echo Opening $LOG with tail...
	tail -n 100 -f $LOG
    fi
else
    echo No event file found!
fi

exit 0;
