#ifndef LADARPLANNERDATUM_HH
#define LADARPLANNERDATUM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "Arbiter2/ArbiterDatum.hh"
#include "Constants.h" //vehicle constants- serial ports, ladar consts,etc

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"
#include "vehlib/VDrive.hh"

#include <genMap.hh>

/*! the name of the config file */
#define CONFIG_FILENAME "config"
/*! the host to connect to the Player Server at (if no config file found) */
#define PLAYER_GAZEBO_HOST "localhost"
/*! the port to connect to the Player Server at (if no config file found) */
#define PLAYER_GAZEBO_PORT 6665

using namespace std;

// All data that needs to be shared by common threads
struct LADARMapperDatum {

  Voter ladar; // voter struct for the Arbiter
  VState_GetStateMsg SS;

  genMap ladarTerMap; 
};


#endif
