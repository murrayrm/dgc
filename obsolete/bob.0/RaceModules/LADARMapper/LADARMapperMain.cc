#include "LADARMapper.hh"
#include "ladar.hh"
#include <getopt.h>

using namespace std;

#define SLEEP_OPT 10
#define ROOF_TTY_OPT 11
#define NAME_OPT 12
#define HELP_OPT 13
#define GENMAP_HEIGHT_OPT 14
#define BUMPER_TTY_OPT 15

int SIM = 0;                        // this is a global variable that modules use to keep track of 
                                    //     whether the code should be run in simulation mode
int LOG_SCANS = 1;                  // whether or not the scan points will be dumped to a log file 
int SEND_VOTES = 1;                 // whether or not to send votes to arbiter
unsigned long SLEEP_TIME = 50000;   // How long in us to sleep between votes
int SPARROW_DISP = 0;               // Whether or not to show the display
int NOSTATE = 1;                    // Don't update state if = 1.
int NOCONFIRM = 1;                  // Don't ask for confirmation of options
int USE_BUMPER = 1;                 // Use the bumper ladar as well if = 1
int NOPITCH = 0;                    // If 1, will use/log zeros for pitch
int NOROLL = 0;                     // If 1, will use/log zeros for roll
int LADAR_POWER_CYCLE = 0;
int TEST_DISP = 0;                  // Display a test display
char *ROOF_TTY;                     // serial port for scanner on the roof
char *BUMPER_TTY;                   // serial port for scanner on the bumper
char *TESTNAME = 0;                 // id for test to be part of log file name

// These are used to override the default path evaluator constants in genMap
double GENMAP_OBS_THRESH = OBSTACLE_THRESHOLD;
double GENMAP_HEIGHT_THRESH = 1.0;
double GENMAP_VECT_THRESH = VECTOR_PROD_THRESHOLD;
double GENMAP_TIRE_THRESH = TIRE_OFF_THRESHOLD;

int main(int argc, char **argv) {

  int c;
  int digit_optind = 0;

  // keep the strdup because it allocates the memory.  sprintf
  // doesn't.
  ROOF_TTY = strdup("/dev/ttyS0");
  BUMPER_TTY = strdup("/dev/ttyS5");
  TESTNAME = strdup("qid");
  sprintf(ROOF_TTY,"/dev/ttyS%d",LADAR_ROOF_SERIAL_PORT);
  sprintf(BUMPER_TTY,"/dev/ttyS%d",LADAR_BUMPER_SERIAL_PORT);
  while (1) {
    int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    static struct option long_options[] = {
      {"sim",        0, &SIM, 1},
      {"novote",     0, &SEND_VOTES, 0},
      {"nobumper",  0, &USE_BUMPER, 0},
      {"noconfirm",  0, &NOCONFIRM, 1},
      {"nopitch",  0, &NOPITCH, 1},
      {"noroll",  0, &NOROLL, 1},
      {"log",        0, &LOG_SCANS, 1},
      {"name",       1, 0, NAME_OPT},
      {"sleep",      1, 0, SLEEP_OPT},
      {"disp",       0, &SPARROW_DISP, 1},
      {"roof-tty",       1, 0, ROOF_TTY_OPT},
      {"bumper-tty",       1, 0, BUMPER_TTY_OPT},
      {"help",       0, 0, HELP_OPT},
      {"nostate",    0, &NOSTATE, 1},
      {"hthresh",    1, 0, GENMAP_HEIGHT_OPT},
      {"powercycle", 0, &LADAR_POWER_CYCLE, 1},
      {"testdisp",   0, &TEST_DISP, 1},
      {0,0,0,0}
    };

    c = getopt_long_only (argc, argv, "", long_options, &option_index);
    if(c == -1) break;

    if(c != '?') {
      switch(c) {
        case GENMAP_HEIGHT_OPT:
          GENMAP_HEIGHT_THRESH = atof(optarg);
          break;
        case SLEEP_OPT:
          SLEEP_TIME = (unsigned long) atol(optarg);
          break;
        case ROOF_TTY_OPT:
          ROOF_TTY = strdup(optarg);
          break;
        case BUMPER_TTY_OPT:
          BUMPER_TTY = strdup(optarg);
          break;
         case NAME_OPT:
          TESTNAME = strdup(optarg);
          break;
        case HELP_OPT:
          cout << endl << "LADARMapper Usage:" << endl << endl;
          cout << "LADARMapper [-sim] [-novote] [-log testname] [-sleep us]";
          cout << endl;
          cout << "            [-noconfirm] [-disp] [-hthresh m]";
          cout << endl;
          cout << "            [-roof-tty tty] [-bumper-tty tty] [-help]" << endl << endl;
          cout << "     -sim            Sets simulation mode.  In this mode, the LADAR will" << endl;
          cout << "                     not be accessed, but votes will still be sent." << endl;
          cout << "                     Use -novote if you wish to not send votes." << endl;
          cout << "     -novote         Do not send votes to the arbiter." << endl;
          cout << "     -log            Log all scan points, state, and votes in testLogs subdir," << endl;
	  cout << "                     OBSERVE: name must be specified with -name or it will default to " << TESTNAME  << endl;
          cout << "     -name testname  Name log files <testname>_<type>_<timestamp>" << endl;
          cout << "                     where <type> is scan, state, vote, or errors" << endl;
          cout << "     -sleep us       Specifies number of us to sleep between updates." << endl;
          cout << "     -noconfirm      Do not ask for confirmation" << endl;
          cout << "     -nobumper      Use the bumper ladar in addition" << endl;
          cout << "     -disp           Run sparrow display" << endl;
          cout << "     -hthresh m      Tells genMap to ignore obstacles smaller than this" << endl;
          cout << "     -roof-tty tty   Specifies tty port to use for scanner on the roof." << endl;
	  cout << "                     Example: /dev/ttyS8" << endl;
          cout << "     -bumper-tty tty Specifies tty port to use for scanner on the bumper." << endl;
	  cout << "                     Example: /dev/ttyS8" << endl;
          cout << "     -testdisp       Display a test screen, cannot be used if -disp is given" << endl;
          cout << "     -help           Displays this help" << endl << endl;
          exit(0);
      }
      cout << "option " << long_options[option_index].name;
      if(optarg) cout << " with arg " << optarg;
      cout << endl;
    }
  }

  // We don't support the bumper LADAR in Gazebo simulation, set it to 
  // disabled not taking into account if -nobumper is used or not.
  // (this must be performed because if we use the bumper LADAR the readings
  // from the roof LADAR will be screwed and all votes 0 it seems)
  if(SIM)
    USE_BUMPER = 0; 
  
  cout << endl << "Argument summary:" << endl;
  cout << "  SIM:          " << SIM << endl;
  cout << "  USE_BUMPER:   " << USE_BUMPER << endl;
  cout << "  LOG_SCANS:    " << LOG_SCANS << endl;
  if(TESTNAME)
    cout << "  TESTNAME:     " << TESTNAME << endl;
  cout << "  SEND_VOTES:   " << SEND_VOTES << endl;
  cout << "  SLEEP:        " << SLEEP_TIME << "us" << endl;
  cout << "  SPARROW_DISP: " << SPARROW_DISP << endl;
  cout << "  ROOF_TTY:          " << ROOF_TTY << endl; 
  cout << "  BUMPER_TTY:          " << BUMPER_TTY << endl; 
  cout << "  NOSTATE:      " << NOSTATE << endl;
  cout << "  OBSTACLE HEIGHT THRESHOLD: " << GENMAP_HEIGHT_THRESH << endl;
  cout << "  TEST DISPLAY: " << TEST_DISP << endl;
 
  // Done doing argument checking.
  ////////////////////////////////

  if(TESTNAME == 0) {
    cout << endl << endl;
    cout << "-name switch MUST be specified" << endl;
    exit(-1);
  }

  if(!NOCONFIRM) {
    cout << endl;
    cout << "VERIFY THAT YOU HAVE SPECIFIED THE CORRECT SWITCHES!" << endl;
    cout << "Do you wish to proceed? (type 'yes', anything else exits)" << endl;
    char answer[100];
    fgets(answer,sizeof(answer),stdin);
    if(strcmp(answer, "yes\n")) {
      exit(-1);
    }
  }
  
  // Start the MTA By registering a module
  // and then starting the kernel

  Register(shared_ptr<DGC_MODULE>(new LADARMapper));
  StartKernel();

  return 0;
}
