#ifndef LADARPLANNER_HH
#define LADARPLANNER_HH


#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include "LADARMapperDatum.hh"

#ifdef _PLAYER_ // build with gazebo simulation support
#include "playerclient.h"
#endif

using namespace std;
using namespace boost;

/** LADARMapper is the planning module using LADAR rays to percept the 
 * environment with it's obstacles. It creates votes of where it thinks is the 
 * best way to travel next.
 *
 * When using the -sim switch, make sure to also use the -nobumper and 
 * optionally -testdisp if you want a realtime display of what the LADAR sees.
 * Each iteration in the LADAR loop takes about 400ms which is the reason 
 * LADARMapper uses the data mode called PLAYER_DATAMODE_PULL_NEW, which means
 * we get only new data when we ask for it.
 */
class LADARMapper : public DGC_MODULE {
public:
  // and change the constructors
  LADARMapper();
  ~LADARMapper();

  // The states in the state machine.
  // Uncomment these if you wish to define these functions.

  void Init();
  void Active();
  //  void Standby();
  void Shutdown();
  //  void Restart();

  // The message handlers. 
  //  void InMailHandler(Mail & ml);
  //  Mail QueryMailHandler(Mail & ml);

  // the status function.
  //string Status();


  // Any functions which run separate threads
  
  // Method protocols
  int  InitLadar();
  void LadarUpdateVotes();
  void LadarShutdown();

  void UpdateState();

  void SparrowDisplayLoop();
  void UpdateSparrowVariablesLoop();

  void SaveGenMap();
  void ClearGenMap();

private:
  // and a datum named d.
  LADARMapperDatum d;

  /*! Displays an ascii diagram over the reading results */
  void showTestDisplay(double* scanPoints, int nbrPoints);

#ifdef _PLAYER_
  // For simulation
  PlayerClient* playerClient;
  LaserProxy* ladarSim;
  /*! Initializes the Player client and connects to the player server
   * \return true if all went ok. */
  bool initPlayer();
  /*! Reads the config file with host and port */
  void readConfig();
  /*! the host to connect to the Player Server at */
  string host;
  /*! the port to connect to the Player Server at */
  int port;  
  
#endif
};



// enumerate all module messages that could be received
namespace WaypointNavMessages {
  enum {
    // we are not yet receiving messages.
    // A place holder
    NumMessages
  };
};






#endif
