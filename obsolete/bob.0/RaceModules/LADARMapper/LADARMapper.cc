#include "LADARMapper.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"
#include "MTA/Misc/Time/Time.hh"

#include <unistd.h>
#include <iostream>

int QUIT_PRESSED = 0;

extern int SEND_VOTES;
extern unsigned long SLEEP_TIME;
extern int SPARROW_DISP;
extern int NOSTATE;

LADARMapper::LADARMapper()
    : DGC_MODULE(MODULES::LADARMapper, "LADAR Mapper", 0)
{
}

LADARMapper::~LADARMapper()
{}


void LADARMapper::Init()
{
  // call the parent init first
  DGC_MODULE::Init();

  if(InitLadar())
  {
    cout << "Module Init Finished" << endl;
  }
  else
  {
    cout << "InitLadar() failed, exiting" << endl;
    exit(1);
  }

  cout << "About to start Sparrow display" << endl;
  if(SPARROW_DISP)
  {
    cout << "Starting Sparrow display" << endl;
    RunMethodInNewThread<LADARMapper>( this, &LADARMapper::SparrowDisplayLoop);
    RunMethodInNewThread<LADARMapper>( this, &LADARMapper::UpdateSparrowVariablesLoop);
  }
}

void LADARMapper::Active()
{
  int logGenMap = 0;

  cout << "Starting Active Function" << endl;
  while( ContinueInState() && !QUIT_PRESSED)
  {
    // update the State Struct
    if(!NOSTATE)
    {
      UpdateState();
      if(d.SS.Northing == 0)
      {
        cout << endl << "UNABLE TO COMMUNICATE WITH VSTATE" << endl;
        cout << "NORTHING IS ZERO, PLEASE CHECK OR" << endl;
        cout << "RUN WITH -nostate SWITCH IF THIS IS OK" << endl << endl;
        exit(-1);
      }
    }
    // Update the votes and send them to the arbiter
    LadarUpdateVotes();

    if(SEND_VOTES)
    {
      Mail m = NewOutMessage(MyAddress(),
                             MODULES::Arbiter,
                             ArbiterMessages::Update);
      // Send the arbiter our "ID" and the vote struct.  The weights
      // are taken care of by the arbiter.
      int myID = ArbiterInput::LADAR;
      m << myID << d.ladar;
      SendMail(m);
    }

    if(SPARROW_DISP) UpdateSparrowVariablesLoop();

    // and sleep for a bit
    usleep_for(SLEEP_TIME);

#if 0
    // logging genMap
    if (!(logGenMap % 10))
    {
      cout << "---saving genMap using saveMATFile" << endl;
      char filename[15] = "LADARgenMap";
      char filenum[5];
      sprintf(filenum, "%d", logGenMap/10);
      strcat(filename, filenum);
      d.ladarTerMapSensor.saveMATFile(filename, "LADARMapper genMap", "LADARmap");
    }
    logGenMap++;
#endif

  }
  cout << "Finished Active State" << endl;
}

void LADARMapper::Shutdown()
{
  // if we get to here a break has been detected
  cout << "Shutting Down" << endl;
  LadarShutdown();
}

#ifdef _PLAYER_
void LADARMapper::readConfig()
{
  // set the defaults:
  host = PLAYER_GAZEBO_HOST;
  port = PLAYER_GAZEBO_PORT;

  cout << "LADARBumper::readConfig: reading file " << CONFIG_FILENAME << endl;
  ifstream file(CONFIG_FILENAME);
  if(!file.is_open())
  {
    cout << "LADARBumper::readConfig: Cannot read file " << CONFIG_FILENAME
    << ". Using defaults = " << host << ":" << port << endl;
    return;
  }
  // if a file was found, use those settings instead:
  string line;
  getline(file, line);
  host = line;
  getline(file, line);
  port = atoi(line.c_str());

  cout << "LADARBumper::readConfig: read host:port = "
  << host << ":" << port << endl;
}
#endif



