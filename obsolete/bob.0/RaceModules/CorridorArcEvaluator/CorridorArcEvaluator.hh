#ifndef CORRIDORARCEVALUATOR_HH
#define CORRIDORARCEVALUATOR_HH


#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "CorridorArcEvaluatorDatum.hh"
#include "genMapStruct.hh"

using namespace std;
using namespace boost;


class CorridorArcEvaluator : public DGC_MODULE {
public:
  // and change the constructors
  CorridorArcEvaluator();
  ~CorridorArcEvaluator();

  // The states in the state machine.
  // Uncomment these if you wish to define these functions.

  void Init();
  void Active();
  //  void Standby();
  void Shutdown();
  //  void Restart();

  // The message handlers. 
  //  void InMailHandler(Mail & ml);
  // 18 Feb 04 LBC added to accept genMap mail query
  Mail QueryMailHandler(Mail & ml);

  // the status function.
  //string Status();


  // Any functions which run separate threads
  
  // Method protocols
  int  InitCorrArcEval();
  void CorrArcEvalUpdateVotes();
  void CorrArcEvalShutdown();

  void UpdateState(); // grabs from VState and stores in datum.

private:
  // and a datum named d.
  CorridorArcEvaluatorDatum d;
};



// enumerate all module messages that could be received
namespace CorridorArcEvaluatorMessages {
  enum {
    // we are not yet receiving messages.
    // We can set up the sending of a message by defining it here
    Get_genMapStruct,  // MatlabDisplay queries for this 
    NumMessages        // A place holder
  };
};


#endif
