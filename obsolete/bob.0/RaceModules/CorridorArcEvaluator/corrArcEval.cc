#include <sys/time.h>
#include <iomanip>
#include <algorithm>  // for min
#include <stdio.h>

#include "CorridorArcEvaluator.hh"
#include "corrArcEval.hh"

using namespace std;
int CorridorArcEvaluator::InitCorrArcEval() 
{
  // this is where everything gets started up
  cout << "[InitCorrArcEval] Starting InitCorrArcEval()" << endl;

  //Initialize the map
  d.corrArcEmap.initMap(DEFAULT_CORRARCEMAP_NUM_ROWS, 
		        DEFAULT_CORRARCEMAP_NUM_COLS, 
			DEFAULT_CORRARCEMAP_ROW_RES, 
			DEFAULT_CORRARCEMAP_COL_RES, true);
 
  d.corrArcEmap.initPathEvaluationCriteria(false,false,false,false,
                                           false,false,true,false);

  //printf("InitCorrArcEval:d.corrArcEmap.getRowRes() = %f\n", 
  //       d.corrArcEmap.getRowRes() );  
}

void CorridorArcEvaluator::CorrArcEvalUpdateVotes() 
{
  // This is where we send votes to the arbiter

/*  Uber-pseudo code: 
 *  - Overlay corridor into map
 *     - Use methods in genMap?
 *  - Evaluate arcs
 *     - ArcEvaluator?
 *  - Send results to arbiter 
 *     - goodness based on length of arc from _front_ of vehicle to boundary
 *     - veloctiy also based on length of arc from front of vehicle to boundary
 */

  //  cout << "starting steerAngle" << endl;
  vector<double> steerAngle;
  steerAngle.reserve(NUMARCS);
  bool firstLoop = true;    
  int i, nextWaypoint;

  if(firstLoop){
    for(i = 0; i < NUMARCS; i++) {
    //    steerAngle[i] = GetPhi(i);
      steerAngle.push_back(GetPhi(i));
      //    cout << "i: " << steerAngle[i];
    }
    firstLoop = false;
  }
  else{
    for(i = 0; i < NUMARCS; i++) {
      steerAngle[i] = GetPhi(i);
    }
  }

  // *  - Evaluate arcs
  //  cout << "evaluate arcs" << endl;
  vector< pair<double, double> > votes;
  //  cout << "get votes.." << endl;
  votes = d.corrArcEmap.generateArbiterVotes(NUMARCS, steerAngle);
 
  //cout << "current pos: (" << d.SS.Northing << ", ";
  //cout << d.SS.Easting << ")" <<  endl;

  // *  - Send results to arbiter 
  //cout << "sending votes to arbiter" << endl;
  for (int i=0; i<NUMARCS; i++) {
    d.corrArcEval.Votes[i].Goodness = votes[i].second;
//--------------------------------------------------
//     cout << "Arc: " << i << ", Goodness: ";
//     cout << d.corrArcEval.Votes[i].Goodness;
//-------------------------------------------------- 
    d.corrArcEval.Votes[i].Velo = votes[i].first;
//--------------------------------------------------
//     cout << ", Velocity: " << d.corrArcEval.Votes[i].Velo << endl;
//-------------------------------------------------- 
  }
  
} // end CorrArcEvalUpdateVotes


void CorridorArcEvaluator::CorrArcEvalShutdown() 
{

}
