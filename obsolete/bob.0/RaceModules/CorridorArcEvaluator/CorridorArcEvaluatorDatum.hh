#ifndef CORRIDORARCEVALUATORDATUM_HH
#define CORRIDORARCEVALUATORDATUM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "Arbiter2/ArbiterDatum.hh"
#include "genMap.hh"

#include "Constants.h" //for vehicle constants, ie serial ports, etc.

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"
#include "vehlib/VDrive.hh"

// The genMap struct
//#include "genMapStruct.hh"

using namespace std;

// All data that needs to be shared by common threads
struct CorridorArcEvaluatorDatum {

  Voter corrArcEval; // voter struct for the Arbiter

  // State struct from VState
  boost::recursive_mutex StateStructLock;
  VState_GetStateMsg SS;

  // instantiating genMap
  genMap corrArcEmap;

  //  Waypoints wp;

};


#endif
