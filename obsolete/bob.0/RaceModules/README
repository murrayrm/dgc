
README For Race Modules Directory

*********************************************************************
HISTORY
1/14/04 - Ike Gremmer - Initial Document
2/1/04 - Sue Ann Hong - Moved some Arbiter stuff to Arbiter/README
                        to keep info up-to-date.
3/28/04  Sue Ann Hong   Descriptions of RaceModules and some DevModules

*********************************************************************

Note: At the bottom of this document is a bug list for each module 
that should be used as a repository along with bugzilla.  Given that 
modules affect each other a centralized bug list would work best.

*********

Note: This Directory Should Only Contain Modules We Plan To Use On Race Day
There is another directory DevModules/ for any non-race modules,
such as simulators and a waypoint capture program.

*********

To run these programs with the simulator:
Start DevModules/SimVStateVDrive
Start Global
Start DFE & other modules (if you want)
Start the arbiter program
Start DevModules/MatlabDisplay (to see the simulation in real-time Matlab)

To start a module:
go into its respective directory,
cvs up,
make
./ExecutableName

Arbiter is in          RaceModules/Arbiter
LADAR                  RaceModules/LADARMapper or RaceModules/LADARBumper
Stereo                 RaceModules/StereoPlanner
StereoLR               RaceModules/StereoPlanner
Global                 RaceModules/GlobalPlanner
DFE                    RaceModules/DFEPlanner
CorrArcEval            RaceModules/CorridorArcEvaluator
FenderPlanner          RaceModules/FenderPlanner
RoadFollower           RaceModules/RoadFollower
SimVStateVDrive is in DevModules/SimVStateVDrive 
PathEvaluation  is in DevModules/PathEvaluation
MatlabDisplay   is in DevModules/MatlabDisplay
=> see READMEs in their respective directories

***************************************************************************
RaceModules:
 Arbiter  : Gets opinions of following modules and outputs a steering/speed
	    decision to Vdrive. 
 LADAR    : Obstacle avoidance using the LADAR units. Powered by genMap 
	    (i.e. scary and complicated).
 Stereo   : Obstacle avoidance using the short-range stereo camera pair. 
	    Uses genMap path evaluation methods.
 StereoLR : Obstacle avoidance using the long-range stereo camera pair.
	    Uses genMap path evaluation methods.
 Global   : Waypoint following (pure geometry) and some velocity control
	    based on track-line following.
 DFE      : Based on vehicle dynamics, prevents roll-overs by limiting speed.
 CorrArcEval : The goal of the module is to keep the vehicle inside the
	       corridors given by DARPA. Uses genMap path evaluation methods.
 FenderPlanner : Following the "good paths" from GPS points collected by
	         T. Fender & D. van Gogh out at the desert.
 RoadFollower : Following roads using the road following camera.

DevModules:
 SimVStateVDrive : Simulates Vstate & Vdrive without sensors/actuators.
 PathEvaluation : A module used for testing path evaluation/CAE methods.
 MatlabDisplay : A real-time display of vehicle simulation/run.

***************************************************************************
A Quick Note About Structure:
  Each directory should contain a Datum.  There are also Module header and 
  source files named ArbiterModule.cc/hh and *Planner.cc/hh.  In addition 
  there is a Main file.  Finally, there is one file which is specific to 
  each module, which is defined below.

  The Datum file should contain #includes and constants specific to that 
  program along with a DATUM definition, which is a struct that will be 
  shared between multiple threads during the execution of the Module.

  The Module Header and Source define the MTA functionality of the module.
  For all of the Planners, this simply defines a loop:
     LOOP Running at 10Hz(for now): GetState              (Taken care of)
                                    UpdateVotes           (Your responsibility)
                                    SendVotes to Arbiter  (Taken care of)
  Therefore all of the MTA functionality is taken care of.  All you need to
  work on is improving the one file in the directory that contains the 
  UpdateVotes function (for simulater this is sim_loop.cc, for Global this is
  global.hh and global.cc, for DFE this is DFE.cc, etc).  

  A few constants and functions to note:  
  GetPhi(int) returns the steering angle of a vote for 
  NUMARCS number of votes.  This means GetPhi(0) is full left steering angle
  and GetPhi(NUMARCS-1) is full right.  Therefore a logical loop to run is 
      for( i = 0; i< NUMARCS; i++) {
        steerangle = GetPhi(i);
	goodness = goodnessOfSteeringAngle( steerAngle );
	velocity = safeVelocity( steerangle );
	Store into vote structure
      } 
  The Vote structure is struct Vote { double Goodness, Velo; };
  (See Arbiter/Vote.hh for more.)
  Each planning module is a "Voter" and contains a Voter structure IN THE DATUM
  d of each module. (See Arbiter/SimpleArbiter.hh for more on struct Voter.)
      struct Voter {
         Vote Votes[NUMARCS];
         double VoterWeight;
      };

  The Main file simply declares an instance of the Module and Starts the MTA.
***************************************************************************


Note: The Arbiter is only "active" until when receives signals from
global. When Global finishes the arbiter will deactivate.  Therefore if all 
goes well when the global module reaches the last waypoint the vehicle is 
immediately brought to a stop.

Note: Make sure you update your MTA and Sparrow directories often.  Significant
changes have been made/will be made over the next month.

Note: There is still a bug where modules may get stuck during bootup if so
kill all programs that use MTA and wait 1 minute for sockets to timeout 
(internal to OS) and then restart.  I know this sucks, but for now it'll 
have to do.

Note: Each module only tries to "find" other modules about every 3 seconds, so
if you boot up and do not see anything happening (i.e Arbiter does not update
or simulator does not move) wait a bit and the magic should happen.




***************************************************************************
BUG LIST:

MTA:
1/10/04 - MTA hangs on initialization when a socket error occurrs.  Only hack
for now is to wait 60 seconds for sockets to timeout and then restart all modules.



DFE:
1/14/04 - All code that does DFE calculations is commented out and replaced 
by comments.  When uncommented out it was giving errors such as not-a-number
and infinite (div by zero) errors.
1/17/04 - LG: code fixed. Seems to work (checked with matlab). Still unable
to run simulator to test with it.


ARBITER:


STEREO:
1/14/04 - Nothing has been implemented regarding stereo.


LADAR:
1/14/04 - Code has not been tested at all.  See Global for example of how to update
votes.  Also, possible segfault due to LADAR and MTA combination (unconfirmed).

GLOBAL:
1/14/04 - Code to calculate deflection (in team/Global/Vector.cc) is wrong.  For one,
it assumes that Yaw is counterclockwise from East instead of clockwise from North.
Also, get_deflection(..) needs to return in branch [-pi, pi] so that steering code
will work.
