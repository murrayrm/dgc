#ifndef GLOBALPLANNERDATUM_HH
#define GLOBALPLANNERDATUM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

// Include these files if you want to use the original arbiter
//--------------------------------------------------
// #include "RaceModules/Arbiter/Vote.hh"
// #include "RaceModules/Arbiter/SimpleArbiter.hh"
// #include "RaceModules/Arbiter/ArbiterModule.hh"
//-------------------------------------------------- 
// Include these files if you want to use the new Arbiter2
#include "Arbiter2/ArbiterDatum.hh"
#include "Arbiter2/Voter.hh"

#include "Constants.h" //for vehicle constants, ie serial ports, etc.
#include "GlobalPlanner.hh" // standard includes, arbiter, datum, ...
#include "global_functions.hh" // for calc_speed_command, calc_steer_command, ...
#include "VehicleConstants.h" // for vehicle geometry like vehicle length

// for steering_angle_to_arc_radius()
#include "DynamicFeasibilityEvaluator/rollover_functions.h" 

#include "waypoints.hh"

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"
#include "vehlib/VDrive.hh"

using namespace std;

// All data that needs to be shared by common threads
struct GlobalPlannerDatum {

  Voter global; // voter struct for the Arbiter

  // State struct from VState
  boost::recursive_mutex StateStructLock;
  VState_GetStateMsg SS;

  // Global Data
  boost::recursive_mutex globalLock;
  Waypoints wp;

  // Are we done?
  bool done;

};


#endif
