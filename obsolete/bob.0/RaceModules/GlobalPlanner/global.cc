//
// global.cc
//
// Changes: 
//   Rich Petras
//   Modified by Rocky Velez, 1/17/04
//   Lars Cremean - 01/17/04 - simplified structure


#include <iostream>
#include "GlobalPlanner.hh" // standard includes, arbiter, datum, ...
#include "global_functions.hh" // for calc_speed_command, calc_steer_command, ...
#include "VehicleConstants.h" // for vehicle geometry like vehicle length

// for steering_angle_to_arc_radius()
#include "DynamicFeasibilityEvaluator/rollover_functions.h" 

using namespace std;

// Reads in waypoints from the specified file (hardcoded now)
// Waypoint file format is:  Northing  Easting
int GlobalPlanner::InitGlobal()
{
  cout << "[InitGlobal] Starting InitGlobal()" << endl;
  cout << "[InitGlobal] d.globalNum = "        << ArbiterInput::Global << endl;
  cout << "[InitGlobal] NUMARCS = "            << NUMARCS << endl;
  cout << "[InitGlobal] LEFTPHI = "            << LEFTPHI << endl;
  cout << "[InitGlobal] RIGHTPHI = "           << RIGHTPHI << endl;

  // Make sure we update our state first 
  // TODO: Make this a better, more unified, condition for determining
  // whether or not we have valid state
  while( d.SS.Timestamp == Timeval(0,0) ) 
  {
    printf("[InitGlobal] Stuck trying to update state...\n");
    UpdateState();
    // chill out.  
    usleep(500000);
  }
  
  // We want to set the current waypoint automatically given the current
  // position of the vehicle.  If you care which waypoint this function
  // returns, then make sure that you are giving it valid state.
  int wpNum = d.wp.readRDDF("rddf.dat", d.SS.Northing, d.SS.Easting );
  cout << " Done reading waypoint data" << endl;

  // get_waypoint actually **sets** the current waypoint 
  d.wp.get_waypoint( wpNum );
  
  cout << " File contains " << d.wp.get_size() << " waypoints." << endl;

  cout << " -----SHOOTING FOR WAYPOINT --- "; 
  printf("(%10.2f,%10.2f)\n",
         d.wp.get_current().easting,
         d.wp.get_current().northing);

  d.done = false;

  sleep(2);

  return true;
}


void GlobalPlanner::GlobalShutdown() 
{

}
