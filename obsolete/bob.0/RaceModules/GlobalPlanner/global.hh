#ifndef GLOBAL_HH
#define GLOBAL_HH

#include <iostream>
#include "GlobalPlanner.hh" // standard includes, arbiter, datum, ...
#include "global_functions.hh" // for calc_speed_command, calc_steer_command, ...
#include "VehicleConstants.h" // for vehicle geometry like vehicle length

// for steering_angle_to_arc_radius()
#include "DynamicFeasibilityEvaluator/rollover_functions.h" 

#endif
