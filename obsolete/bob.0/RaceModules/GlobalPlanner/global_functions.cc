#include <cmath>
#include <unistd.h>
#include <iostream>
#include "global_functions.hh"

using namespace std;

double calc_steer_command(Vector curPos, Vector curWay, double curHeading) 
{
  double desiredRelYaw;   // the desired Yaw (clockwise angle from forward) 
  double steerCommand; // the desired steering angle
  double way_angle;    // angle to the waypoint (measured from east)
  
  Vector way_rel;
  way_rel.N = curWay.N - curPos.N;
  way_rel.E = curWay.E - curPos.E;

  // get the angle between head and way 
  way_angle = atan2( way_rel.N, way_rel.E );

  //  cout << "way_angle = " << way_angle << endl;
  //  cout << "head_angle = " << head_angle << endl;

  // Desired heading in vehicle reference frame
  desiredRelYaw = curHeading - way_angle;

  // Normalize the desired heading to be in [-pi,pi]
  if( desiredRelYaw > M_PI ) desiredRelYaw = desiredRelYaw - 2.0*M_PI;
  if( desiredRelYaw < -M_PI ) desiredRelYaw = desiredRelYaw + 2.0*M_PI;

  // Should probably print this out
  // cout << "way_rel.N= " << way_rel.N << endl << "way_rel.E= " << way_rel.E << endl;

   cout << "Desired Yaw to target (deg in local frame) = " 
	<< desiredRelYaw*180.0/M_PI << endl;

  // Saturate the steering command to be in [-USE_MAX_STEER, USE_MAX_STEER]
  steerCommand = fmax( fmin( desiredRelYaw, USE_MAX_STEER ), -USE_MAX_STEER ); 
  return steerCommand;

} // end calc_steer_command(Vector curPos, Vector curWay, Vector curHead)


int farthest_wp;
int on_startup=1;
Vector far_wp_vector;
double curr_look_dist = 0;
const double MAX_LOOKOUT= 200; // look forward 200 m 

double calc_speed_command(double curYaw, Vector curPos, Vector curWay, 
    Vector nextWay, class Waypoints &wp)
{
  double outer_wp_d = 0; // distance from farthest waypoint to next one
  Vector new_outer_wp;
  // initialize to max, drop if we need to slow
  double speed = GLOBAL_FUNCTIONS_MAX_SPEED; 
  int targetWP = wp.get_current().num;
  bool not_done = true;

  if (wp.get_current().num == wp.get_size() - 1){
    // going to the last waypoint, going full speed ahead!!
  }else{
    far_wp_vector.N = (double) wp.get_current().northing;
    far_wp_vector.E = (double) wp.get_current().easting;
    farthest_wp = wp.get_current().num;
    curr_look_dist = distVector(curPos, far_wp_vector);

    new_outer_wp.N = (double) wp.peek_waypoint(farthest_wp + 1).northing;
    new_outer_wp.E = (double) wp.peek_waypoint(farthest_wp + 1).easting;
    outer_wp_d = distVector(new_outer_wp, far_wp_vector);  

    // tack on waypoints to look out to until you get beyond the max look dist
    while ((curr_look_dist + outer_wp_d  < MAX_LOOKOUT)&& not_done) {
      if (outer_wp_d + curr_look_dist < MAX_LOOKOUT) {
	farthest_wp++;
	far_wp_vector.N = new_outer_wp.N;
	far_wp_vector.E = new_outer_wp.E;
	curr_look_dist += outer_wp_d;
      }
      // check for last waypoint each time we increment
      if (farthest_wp + 1 < wp.get_size()) {
	new_outer_wp.N = (double) wp.peek_waypoint(farthest_wp + 1).northing;
	new_outer_wp.E = (double) wp.peek_waypoint(farthest_wp + 1).easting;
	outer_wp_d = distVector(new_outer_wp, far_wp_vector);  
      } else { 
	not_done = false;
      }
    }

    // By this point, we have a waypoint that we're going to, and 
    // the number of the farthest waypoint we're looking ahead to.
    // Now we go through and assign a speed limit for each waypoint based
    // on the angle through that waypoint and the size of the corridor
    // the vehicle will be entering at that waypoint.  Beware the fuzz!
    // Then we go and calculate the max safe speed we can currently travel at
    // in order to slow to the speed limit at that waypoint, given our current
    // distance to that waypoint, and choose the slowest of these speeds.
 
    double to_next_wp = distVector(curWay, curPos);
    int numwps = farthest_wp - targetWP;
    Vector waypoint;
    Vector nextWP;
    double wp_angle, wp_speed, wp_radius, nextRadius;

    // get our max speed for the next waypoint
    nextRadius = wp.get_current().radius;
    wp_angle = get_deflection(curPos, curWay, nextWay); //TODO: Check func arg 
    wp_speed = get_waypoint_speed(wp_angle, nextRadius);
    speed = get_safe_speed(to_next_wp, wp_speed);
  
    if (numwps > 0){
      double new_speed;
      double to_wp_dist = to_next_wp;
      Vector prevWP;
      // get the max speeds for each of the waypoints
      for(int i = 1; i <= numwps; i++) {
	prevWP.N = (double) wp.peek_waypoint(targetWP + i-1).northing;
	prevWP.E = (double) wp.peek_waypoint(targetWP + i-1).easting;
	waypoint.N = (double) wp.peek_waypoint(targetWP + i).northing;
	waypoint.E = (double) wp.peek_waypoint(targetWP + i).easting;
	nextWP.N = (double) wp.peek_waypoint(targetWP + i + 1).northing;
	nextWP.E = (double) wp.peek_waypoint(targetWP + i + 1).easting;
	wp_angle = get_deflection(prevWP, waypoint, nextWP); // TODO: check
	wp_radius = (double) wp.peek_waypoint(targetWP + i).radius;
	wp_speed = get_waypoint_speed(wp_angle, wp_radius);
	to_wp_dist += distVector(prevWP, waypoint);
	new_speed = get_safe_speed(to_wp_dist, wp_speed);
	if (new_speed < speed){
	  speed = new_speed;
	}
      }
    }
  }

  // Set the commanded speed to be big when we're going in the desired 
  // direction, and small when we're not!
  double desiredRelYaw; //the desired relative Yaw (clockwise angle from forward)     
  double way_angle, curHeading;
  Vector way_rel;
  way_rel.N = curWay.N - curPos.N;
  way_rel.E = curWay.E - curPos.E;

  // get the angle between the vehicle and waypoint locations 
  way_angle = atan2( way_rel.N, way_rel.E );
  
  // Current heading
  curHeading = M_PI_2 - curYaw;
  // Desired heading in vehicle reference frame
  desiredRelYaw = curHeading - way_angle;
  double diff = fabs( desiredRelYaw / M_PI ); // normalized error in heading
  double angleErrorThreshold = 0.10; // threshold on normalized heading 
  // error, above  which we will command MIN_VELO  
  double speedCommand;                               
  
  // Normalize the desired heading to be in [-pi,pi]
  if( desiredRelYaw > M_PI ) desiredRelYaw = desiredRelYaw - 2.0*M_PI;
  if( desiredRelYaw < -M_PI ) desiredRelYaw = desiredRelYaw + 2.0*M_PI;
  
  if( diff >= angleErrorThreshold ) {
    speedCommand = GLOBAL_FUNCTIONS_MIN_SPEED;
  }
  else {
    speedCommand = GLOBAL_FUNCTIONS_MAX_SPEED - 
      (GLOBAL_FUNCTIONS_MAX_SPEED - GLOBAL_FUNCTIONS_MIN_SPEED) * 
      (diff / angleErrorThreshold );
    speed = (speedCommand > speed ? speed 
	     : speedCommand);
  }
  return speed;
  
}

double get_waypoint_speed(double angle, double radius)
{
  double r_speed;
  if (radius <= GLOBAL_FUNCTIONS_MIN_RADIUS) {
    r_speed = GLOBAL_FUNCTIONS_MIN_SPEED;
  }else if (radius > GLOBAL_FUNCTIONS_MIN_RADIUS &&
	    radius < GLOBAL_FUNCTIONS_MAX_RADIUS) {
    // if the speed is in the middle scale linearly
    r_speed = (GLOBAL_FUNCTIONS_MAX_SPEED - GLOBAL_FUNCTIONS_MIN_SPEED)
      * (radius - GLOBAL_FUNCTIONS_MIN_RADIUS) / (GLOBAL_FUNCTIONS_MAX_RADIUS
						 - GLOBAL_FUNCTIONS_MIN_RADIUS)
      + GLOBAL_FUNCTIONS_MIN_SPEED;
  }else{
    r_speed = GLOBAL_FUNCTIONS_MAX_SPEED;
  }

  double a_speed;
  angle = fabs(angle);
  // check speed based on angle
  if (angle <= GLOBAL_FUNCTIONS_MIN_ANGLE) {
   a_speed = GLOBAL_FUNCTIONS_MAX_SPEED;
  }else if (angle > GLOBAL_FUNCTIONS_MIN_ANGLE &&
	    angle < GLOBAL_FUNCTIONS_MAX_ANGLE) {
    // if the speed is in the middle scale linearly
    a_speed = (GLOBAL_FUNCTIONS_MIN_SPEED - GLOBAL_FUNCTIONS_MAX_SPEED)
      * (angle - GLOBAL_FUNCTIONS_MIN_ANGLE) / (GLOBAL_FUNCTIONS_MAX_ANGLE
						 - GLOBAL_FUNCTIONS_MIN_ANGLE)
      + GLOBAL_FUNCTIONS_MAX_SPEED;
  }else{
    a_speed = GLOBAL_FUNCTIONS_MIN_SPEED;
  }
  
  return min(a_speed, r_speed);

}

double get_safe_speed(double distance, double speed)
{
  double sp = sqrt(2 * GLOBAL_FUNCTIONS_MAX_ACCEL * distance + speed * speed);
  if (sp <= GLOBAL_FUNCTIONS_MIN_SPEED){
    sp = GLOBAL_FUNCTIONS_MIN_SPEED;
  }else if (sp >= GLOBAL_FUNCTIONS_MAX_SPEED) {
    sp = GLOBAL_FUNCTIONS_MAX_SPEED;
  }

  return sp;
}

double get_deflection(Vector prev, Vector curr, Vector next)
{
  double angle;
  Vector first_line = subVector(curr, prev);
  Vector next_line = subVector(next, curr);

  double dot = first_line.N * next_line.N + first_line.E * next_line.E;
  double mag_first = sqrt(first_line.N * first_line.N + first_line.E *
			  first_line.E);
  double mag_next = sqrt(next_line.N * next_line.N + next_line.E *
			 next_line.E);

  angle = acos(dot / mag_first / mag_next);

  return angle;
}

float distVector(Vector a, Vector b) 
{
  return sqrt((a.N-b.N)*(a.N-b.N) + (a.E-b.E)*(a.E-b.E));
}

Vector subVector(Vector a, Vector b) 
{
  a.N -= b.N; 
  a.E -= b.E;
  return (a); 
}

/* non-destructive add */
Vector addVector(Vector a, Vector b) 
{
  Vector result;
  result.N = a.N + b.N; 
  result.E = a.E + b.E;
  return result; 
}

Vector scaleVector(Vector a, float b)
{
  Vector result;
  if (distVector(a, Vector(0,0)) != 0) {
    result.N = a.N * b / distVector(a, Vector(0,0));
    result.E = a.E * b / distVector(a, Vector(0,0));
  }
  else {
    result.N = 0;
    result.E = 0;
  }
  return result;
}


bool withinWaypoint(Vector pos, Vector way, float threshold) 
{
  return threshold > sqrt((pos.N-way.N)*(pos.N-way.N) + (pos.E-way.E)*(pos.E-way.E));
}


/* withinCorridor()
 * Vector cur_pos : Should be the front of the vehicle to be more effective.
 * Vector cur_way, Vector next_way, double next_corr_radius
 * Returns a boolean indicating whether we are in the next corridor or not.
 * User may want to update the current waypoint according to the return value
 * from calling this function.
 * Note: This won't be useful for cases where the vehicle travels through the
 *       next corridor too fast, because then cur_way and next_way won't be
 *       correct. (unless the function calling this does some extra work)
 */
bool withinCorridor(Vector cur_pos, Vector cur_way,
                    Vector next_way, double next_corr_rad)
{
  bool within = false;

  // Check if within the circular part of the corridor    
  // Within the circle around the current waypoint
  // -or- within the circle around the next waypoint
  if (distVector(cur_pos, cur_way) < next_corr_rad ||
      distVector(cur_pos, next_way) < next_corr_rad) {
    within = true;
 //   cout << "~~~~~~~WITHIN THE CICULAR PART~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
  }
  //  else {
  // Check if within the rectangular part of the corridor
  // We consider the four vertices of the rectangle
  // v[1]------------------------v[2]               
  // |                              |                 
  // * <-- cur_way                  * <-- next_way  
  // |                              |               
  // v[0]------------------------v[3]               
    
  // v is an array for the vertices of the rectangle
  Vector v[4];     
  // way2way is the vector from the current to next waypoint
  Vector way2way = subVector(next_way, cur_way);
  // offset is the vector perpendicular to way2way, rotated 
  // counter-clockwise by pi/2
  Vector offset(way2way.E, -way2way.N);
  // Now scale the offset to be the vector from the waypoint to the
  // vertex of the rectangle on the waypoint radius
  // TODO: This will break if the successive waypoints are the same!
  offset = scaleVector(offset, next_corr_rad);
  // assign the vectors from the waypoints to the corresponding vertices
  v[0] = subVector(cur_way, offset);
  v[1] = addVector(cur_way, offset);
  v[2] = addVector(next_way, offset);
  v[3] = subVector(next_way, offset);

  // Now find the angles formed by the lines from the vehicle position
  // to adjacent vertices vpv = "vertex-position-vertex"
  double vpv[4];          
  
  for (int i = 0; i < 4; i++) 
  {
    vpv[i] = atan2(subVector(v[i%4], cur_pos).N, 
		   subVector(v[i%4], cur_pos).E) - 
             atan2(subVector(v[(i+1)%4], cur_pos).N, 
		   subVector(v[(i+1)%4], cur_pos).E);
    // normalize and get the acute angle absolute value
    vpv[i] = atan2( sin(vpv[i]), cos(vpv[i]) ); 
    vpv[i] = fabs( vpv[i] );
  }
  if (fabs((2*M_PI - (vpv[0] + vpv[1] + vpv[2] + vpv[3]))) < 
      PI2_ACCURACY_THRESHOLD) 
  {
    within = true; 
//    cout << "$$$$$$$$$$$$$$$$WITHIN THE RECTANGLE$$$$$$$$$$$$$$$$$$$$" << endl;
  }
  //} // end of else

  return within;
}

