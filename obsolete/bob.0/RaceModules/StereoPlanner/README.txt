IF STEREOPLANNER DOES NOT COMPILE, PLEASE SEE /team/stereovision/README
IF STEREOPLANNER DOES NOT RUN, PLEASE SEE /team/stereovision/README

For more information on StereoPlanner, please see team/doc/stereovision/StereoPlanner.txt
