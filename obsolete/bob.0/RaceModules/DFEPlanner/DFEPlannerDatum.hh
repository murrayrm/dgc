#ifndef DFEPLANNERDATUM_HH
#define DFEPLANNERDATUM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "Arbiter2/ArbiterDatum.hh"

#include "../../include/Constants.h" //for vehicle constants, ie serial ports, etc.
#include "frames/rot_matrix.hh"

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"
#include "vehlib/VDrive.hh"

using namespace std;

// All data that needs to be shared by common threads
struct DFEPlannerDatum {

  Voter dfe; // voter struct for the Arbiter

  // State struct from VState
  boost::recursive_mutex StateStructLock;
  VState_GetStateMsg SS;

};


#endif
