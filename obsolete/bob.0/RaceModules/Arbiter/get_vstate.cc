/*
** get_vstate.cc
**
** Same as that used in all modules that request state data from vstate.
** 
**
*/

#include "ArbiterModule.hh" // standard includes, arbiter, datum, ...

void Arbiter::UpdateState()
{ 
  Mail msg = NewQueryMessage( MyAddress(), MODULES::VState, 
			      VStateMessages::GetState);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> d.SS; // download the new state
    d.lastUpdateState = TVNow();
    d.UpdateCountState++;
  }

} // end Arbiter::UpdateState() 
