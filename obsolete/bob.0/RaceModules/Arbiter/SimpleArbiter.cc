// SimpleArbiter.cc
/*
  1/11/04 Created by Ike [From original Arbiter]
  1/12/04 Added comments (Ike)
  1/17/04 More comments for your pleasure (Sue Ann)
  2/19/04  Sue Ann Hong    Changed CombineVotes to let any module veto an arc.
  2/20/04  Sue Ann Hong    Changed PickBestCombinedArc to choose from a largest
                           group of max goodness.
  2/26/04  Sue Ann Hong    Changed CombineVotes to treat DFE differently.
  2/27/04  Sue Ann Hong    Changed CombineVotes to remember DFE vetos 
                           differently.
  3/3/04   Sue Ann Hong    In case of DFE_veto, slow down.
                           Tie-breaking favors last commanded steer angle.
  5/15/04  Sue Ann Hong    Added feature: if there's no good path and we stop,
                            keep the old steering angle.
*/

/* This is undesirable, but must include the datum header file for Arbiter
 * module in order to check a Voter's ID. In the current setting, the
 * Arbiter functions do not mutate the Voters at all, hence no information
 * about the number of Voters is available here, only at the MTA module level.
 * (1/23/04, Sue Ann)
 */
#include "ArbiterModuleDatum.hh"

#include "SimpleArbiter.hh"
#include <iostream>
using namespace std;


double GetPhi( int arcnum ) {
  return ( ((double) arcnum) / ((double) (NUMARCS-1)) ) * (RIGHTPHI - LEFTPHI)
    + LEFTPHI;
}

int GetArcNum( double phi ) {
  int arcNum = (int)round( (phi - LEFTPHI) / (RIGHTPHI - LEFTPHI) * ((double)(NUMARCS-1)) );
  if (arcNum < 0 || arcNum > NUMARCS) { return -1; }
  else { return arcNum; }
}


/* ShapeVotes(Voter &v, double threshold, double favor_high_scale):
 *  For each arc of the given Voter's vote, this function assigns a new
 *  goodness. The new goodness for an arc is the weighted average of the arc's
 *  goodness and its neighbor arcs' goodnesses. First we compare the arc to its
 *  left neighbor, take the weighted average if its goodness is greater than
 *  that of the neighbor. Then do the same with the right neighbor, and take
 *  the average of the two weighted averages. Note that an arc's goodness is 
 *  only modified if it's higher than those of the neighboring arcs' 
 *  goodnesses by the given threshold. 
 * Inputs:
 *  Voter &v - the Voter whose votes we're shaping.
 *  double threshold - if the difference between the goodness of this arc
 *    and that of the neighbor is less than this parameter, 
 *    then we do not modify the goodness of this arc.
 *  double favor_high_factor - when taking the weighted average, how much more
 *    weight is given to this arc which has a higher goodness value than the
 *    neighbor we're comparing it to. Since this factor is used twice 
 *    (once with the left neighbor and once with the right), to get the average
 *    of three adjacent arcs for the new goodness of the middle arc, use 
 *    favor_high_factor = 1/2, threshold = 0.
 * Note: We did not use ShapeVotes in the race/QID (March, 2004) due to the
 *       lack of performance testing with it. See ARBITER_SPEC.txt for more.
 */
void ShapeVotes(Voter &v, double threshold, double favor_high_factor);


/* CombineVotes(list<Voter> voter_inputs): For each possible arc, this function
 *   will compare the different input sources and combine them into one.
 *   For now this just takes a weighted sum of the input sources.
 *   To be called in PickBest().
 * bool USE_DFE : whether to use DFE_veto for combiing votes or not.
 *  Return value: a Voter that contains the "best value" for the given arcs.
 *  1/16/2003: sets goodness to the minimum of all votes.
 */
Voter CombineVotes(list<Voter> voter_inputs, bool USE_DFE);

/* PickBestCombinedArc(Voter& combined): Picks the highest scored arc out
 *   of all considered in the input Voter and returns it in the format to be
 *   sent to the driving module.
 *   Currently just picks the highest scored arc and returns it.  In the
 *   future it will probably pick the center arc out of a good subset of arcs.
 *   To be called in PickBest().
 *  Return value: ActuatorCommand - the "best" velocity, steering angle
 */
ActuatorCommand PickBestCombinedArc(Voter& combined, double lastPhi);


// (3/5/04) Put in round(goodness) as a possible fix for LADAR's
// sending goodnesses that look like '100.0' but aren't being
// treated as <= MAX_GOODNESS. This really shouldn't be the case...
void ShapeVotes(Voter &v, double threshold, double favor_high_factor)
{
  double newGoodness[NUMARCS];

  // Initialize newGoodness[]
  for (int i = 0; i < NUMARCS; i++ ) {
    newGoodness[i] = v.Votes[i].Goodness;
  }

  for (int i = 0; i < NUMARCS; i++ ) {    
    if (i > 0) {
      // Can compare to i-1
      if ( round(v.Votes[i].Goodness) <= MAX_GOODNESS && 
 	   round(v.Votes[i].Goodness) >= 0 && 
	   round(v.Votes[i-1].Goodness) <= MAX_GOODNESS && 
 	   round(v.Votes[i-1].Goodness) >= 0 && 
	   v.Votes[i].Goodness - v.Votes[i-1].Goodness >= threshold) {
	newGoodness[i] = (favor_high_factor*v.Votes[i].Goodness + 
			  v.Votes[i-1].Goodness) / (1.0 + favor_high_factor); 
      }
    }
    if (i < NUMARCS-1) {
      // Can compare to i+1
      if ( round(v.Votes[i].Goodness) <= MAX_GOODNESS && 
	   round(v.Votes[i].Goodness) >= 0 && 
	   round(v.Votes[i+1].Goodness) <= MAX_GOODNESS && 
 	   round(v.Votes[i+1].Goodness) >= 0 && 
	   v.Votes[i].Goodness - v.Votes[i+1].Goodness >= threshold) {
	newGoodness[i] = (((favor_high_factor*v.Votes[i].Goodness + 
			    v.Votes[i+1].Goodness) / (1.0 + favor_high_factor))
			  + newGoodness[i]) / 2;
      }
    }
  }

  for (int i = 0; i < NUMARCS; i++ ) {
    v.Votes[i].Goodness = newGoodness[i];
  }

}


ArbiterBestResult PickBest(list<Voter> voter_inputs, double lastPhi)
{
  ArbiterBestResult abr;
  ArbiterBestResult abr_noDFE;
  ArbiterBestResult abr_withDFE;
  bool DONT_USE_DFE = false;
  bool USE_DFE = true;  

  // create a "combined" output voter
  abr_noDFE.combined = CombineVotes(voter_inputs, DONT_USE_DFE);

  // and pick the best arc given these inputs
  abr_noDFE.cmd = PickBestCombinedArc(abr_noDFE.combined, lastPhi);

  // create a "combined" output voter
  abr_withDFE.combined = CombineVotes(voter_inputs, USE_DFE);

  // and pick the best arc given these inputs
  abr_withDFE.cmd = PickBestCombinedArc(abr_withDFE.combined, lastPhi);

  // We want the steering angle determined from using DFE
  // and the velocity determined from not using DFE.
  // This is so that if all non-DFE modules decide to go on a path defined
  // untrasversable by DFE, then we still want to steer the angle determined
  // with DFE but we don't go too fast (e.g. DFE's velocity for going straight)
  // so that we can never turn.
  abr.combined = abr_withDFE.combined;  // Only for display in ArbiterModule.cc
  abr.cmd.phi = abr_withDFE.cmd.phi;
  abr.cmd.vel = min(abr_noDFE.cmd.vel, abr_withDFE.cmd.vel);

  return abr;
}


Voter CombineVotes(list<Voter> voters, bool USE_DFE) {

  Voter ret; // the "Combined" voter
  int veto[NUMARCS];
  int dfe_veto[NUMARCS];
  double numValidVoters[NUMARCS];
  int i,j;
  bool path_exists = false;   // Keeps track of whether a good path exists or not

  // clear out the votes
  for(i=0; i<NUMARCS; i++) {
    ret.Votes[i].Goodness = 0;
    ret.Votes[i].Velo = NO_VOTE_VELO;  // greater than any possible speed
  }
  for(i=0; i<NUMARCS; i++) {
    veto[i] = NO_VETO;
    dfe_veto[i] = NO_VETO;
  }
  for(i=0; i<NUMARCS; i++) {
    numValidVoters[i] = 0;
  }

  list<Voter>::iterator x;
  // Traverse through all voters and calculate combined weight
  for( x = voters.begin(); x != voters.end(); x++) {

    // Vote-shaping: commented out.
    /*
    if ((*x).ID == ArbiterInput::Stereo || (*x).ID == ArbiterInput::StereoLR ||
       (*x).ID == ArbiterInput::LADAR  || (*x).ID == ArbiterInput::CorrArcEval)
    {
      ShapeVotes(*x, 0.0, 0.5);
    }
    */

    // We treat DFE differently from other modules, since its Votes depend 
    // purely on dynamic reasons and not pathplanning.
    if ((*x).ID == ArbiterInput::DFE) {
      // For each possible arc:
      for( j = 0; j<NUMARCS; j++) {

	//We only look at DFE_VETOs if the user specifies us to (USE_DFE==TRUE)
	if (USE_DFE) {
          // Goodness: multiply and sum (weighted sum)
	  if ((*x).Votes[j].Goodness <= DFE_GOODNESS_THRESHOLD) {
	    dfe_veto[j] = NORMAL_VETO;
	    // numValidVoters shouldn't matter, since veto'd down.
	    numValidVoters[j] += (*x).VoterWeight;   
	    // Zero goodness, so zero velocity.
	    ret.Votes[j].Velo = 0.0;
	  }
	  // (3/5/04) Put in round(goodness) as a possible fix for LADAR's
	  // sending goodnesses that look like '100.0' but aren't being
	  // treated as <= MAX_GOODNESS. This really shouldn't be the case...
	  else if ( round((*x).Votes[j].Goodness) > MAX_GOODNESS ||
		    round((*x).Votes[j].Goodness) < 0 ) {
	    // Currently (2/22/04) we're doing nothing.
	    // DFE shouldn't ever give NO_CONFIDENCE.
	    // if ((*x).Votes[j].Goodness == NO_CONFIDENCE) {}
	  }
	  else {
	    // DFE, above threshold -> good, not affecting the overall arc 
	    //  goodness
	    // Note that if DFE is the only Voter, arbiter commands max
	    //  speed, straight, if -noglobal -noobstacle specified.
	  }
	}

        // Take the minimum acceptable velocity:
        // For DFE, we give a buffer to what the DFE actually sends us, since
        // the velocity we get from the DFE are absolute minimum, just above
	// the roll-over velocities.
	// Note that USE_DFE==false still lets DFE cut down the velocity.
        ret.Votes[j].Velo = min( ret.Votes[j].Velo,
                                 (*x).Votes[j].Velo - DFE_VELO_THRESHOLD );
      }
    }
    else {  // A non-DFE Voter

      // For each possible arc:
      for( j = 0; j<NUMARCS; j++) {
        // Goodness: multiply and sum (weighted sum)
        if ((*x).Votes[j].Goodness <= GOODNESS_THRESHOLD) {
          veto[j] = NORMAL_VETO;
          numValidVoters[j] += (*x).VoterWeight;
          // Zero goodness, so zero velocity.
          ret.Votes[j].Velo = 0.0;
        }
	  // (3/5/04) Put in round(goodness) as a possible fix for LADAR's
	  // sending goodnesses that look like '100.0' but aren't being
	  // treated as <= MAX_GOODNESS
        else if ( round((*x).Votes[j].Goodness) > MAX_GOODNESS ||
	          round((*x).Votes[j].Goodness) < 0 ) {
          // if ((*x).Votes[j].Goodness == NO_CONFIDENCE) {
            // Currently (2/22/04) we're doing nothing. Also not taking
  	    // this voter's velocity into account.
	  // }
	  // if ((*x).Votes[j].Goodness == LADAR_RESET) {
	  // This Voter's LADAR-er and it's resetting. However, we will not
	  // get here, because ArbiterModule's Active() loop checks for this
	  // and ignores the Voter's votes.
        }
        else {
          ret.Votes[j].Goodness += (*x).Votes[j].Goodness * (*x).VoterWeight;
          numValidVoters[j] += (*x).VoterWeight;
          // take the minimum acceptable velocity
          ret.Votes[j].Velo = min( ret.Votes[j].Velo, (*x).Votes[j].Velo );
        }
      }
    }

  } // end of for(voters...)

  // Check for any vetoed down angles and normalize.
  for(i=0; i<NUMARCS; i++) {
    if (numValidVoters[i] > 0) {
      ret.Votes[i].Goodness = (ret.Votes[i].Goodness * veto[i] * dfe_veto[i]) /
                               numValidVoters[i];
      if (ret.Votes[i].Goodness > 0) {
	path_exists = true;
      }
    }
    else if (numValidVoters[i] == 0.0) {
      ret.Votes[i].Goodness = 0;
    }
  }
  
  // If no good path exists, make the velocities all zero, so we know that there's
  // no good path. In PickBestCombinedArc, all-zero velocity will trigger
  // assigning the old steering angle to the return steering value.
  if (!path_exists) {
    for(i=0; i<NUMARCS; i++) {
      ret.Votes[i].Velo =0;
    }
  }

  return ret;
}


// Combined votes have goodness values in [0, MAX_GOODNESS]
ActuatorCommand PickBestCombinedArc(Voter& combined, double lastPhi) 
{
  ActuatorCommand ret;
  double bestGoodness = -1;

  // A boolean indicating that the best arc lies in a streak.
  bool luckyStreak = false;
  // currLucky is the largest streak so far.
  int currFirstLuckyIndex = -1;
  int currLastLuckyIndex = -1;
  // tempLucky is the streak we're in right now.
  int tempFirstLuckyIndex = 0;
  int tempLastLuckyIndex = 0;

  // Pick the arc with the highest Goodness.
  // If best-goodness-valued arcs come in a row, then pick the middle one
  // out of the largest bunch. (using the "lucky streak")
  for(int i=0; i<NUMARCS; i++) {

    if( combined.Votes[i].Goodness > bestGoodness) {
      // A new winner
      tempFirstLuckyIndex = i;
      tempLastLuckyIndex = i;
      luckyStreak = false;

      ret.phi = GetPhi(i);
      ret.vel = combined.Votes[i].Velo;
      bestGoodness = combined.Votes[i].Goodness;
    }
    else if (combined.Votes[i].Goodness == bestGoodness) {
      if (tempLastLuckyIndex != i - 1) { 
        // not a part of the temp lucky streak
        // A new streak
        tempFirstLuckyIndex = i;
        tempLastLuckyIndex = i;
      }
      else { 
        // Continuing the temp lucky streak
        luckyStreak = true;
        tempLastLuckyIndex = i;
      }

      // Check if we're at the end of checking.
      // At this point, if the temp streak is better (in length, velo, etc)
      // than curr streak, then replace curr with temp.
      // factor=1/2 => 3/2 of old streak
      if (i == NUMARCS-1) {  
	if ( (tempLastLuckyIndex - tempFirstLuckyIndex) >
	     (currLastLuckyIndex - currFirstLuckyIndex) +
	     (currLastLuckyIndex - currFirstLuckyIndex)/2 )
	  //		  STREAK_SIZE_DIFF_THRESHOLD)
        {
	  // A new winner
	  currLastLuckyIndex = tempLastLuckyIndex;
	  currFirstLuckyIndex = tempFirstLuckyIndex;
	}
	// Note that if two groups' sizes are similar enough, then we
	// go ahead and see if either one is closer to the last commanded
	// steering. This is for stability of the system (no zigzags) given
	// jumping state data (for position and heading mainly).
	else if ( (tempLastLuckyIndex - tempFirstLuckyIndex) > 
		  (currLastLuckyIndex - currFirstLuckyIndex) - 
		  (currLastLuckyIndex - currFirstLuckyIndex)/2 )
	  //		  STREAK_SIZE_DIFF_THRESHOLD)
        {
	  // Also compare to the last steering command and then decide on the
	  // winner.
	  if (fabs(GetPhi((tempLastLuckyIndex+tempFirstLuckyIndex)/2)-lastPhi) 
	      <
	      fabs(GetPhi((currLastLuckyIndex+currFirstLuckyIndex)/2)-lastPhi))
	  {
	    // A new winner
	    currLastLuckyIndex = tempLastLuckyIndex;
	    currFirstLuckyIndex = tempFirstLuckyIndex;
	  }
	  else if (
             fabs(GetPhi((tempLastLuckyIndex+tempFirstLuckyIndex)/2)-lastPhi) 
	     ==
	     fabs(GetPhi((currLastLuckyIndex+currFirstLuckyIndex)/2)-lastPhi)) 
          {
	    /* // Need to check if index-1 or index+1 is ok
	    // Now compare the next criterion: adjacent arc goodnesses
	    if (combined.Votes[tempFirstLuckyIndex-1].Goodness >
		combined.Votes[currFirstLuckyIndex-1].Goodness) {
	      currLastLuckyIndex = tempLastLuckyIndex;
	      currFirstLuckyIndex = tempFirstLuckyIndex;	    
	    }
	    // yet another criterion: velocity of the middle arc
	    else if(
              combined.Votes[(tempLastLuckyIndex-tempFirstLuckyIndex)/2].Velo >
	      combined.Votes[(currLastLuckyIndex-currFirstLuckyIndex)/2].Velo) 
	    {
	      currLastLuckyIndex = tempLastLuckyIndex;
	      currFirstLuckyIndex = tempFirstLuckyIndex;	    
	    }
	    */
	  } // end of else if(same distance from prev commanded arc)
	} // end of else if(size of streaks about the same)
      } // end of if(at the last arc)
    } // end of else if(thisarc.Goodness == bestarc_sofar.Goodness)
    else if (tempLastLuckyIndex == i - 1)  // && i.goodness < bestGoodness
    {
      // Not as good as the prev one => A streak is over
      // At this point, if the temp streak is better (in length, velo, etc)
      // than curr streak, then replace curr with temp.
      // Note that if two groups' sizes are similar enough, then we
      // go ahead and see if either one is closer to the last commanded
      // steering. This is for stability of the system (no zigzags) given
      // jumping state data (for position and heading mainly).
      if ( (tempLastLuckyIndex - tempFirstLuckyIndex) > 
	   (currLastLuckyIndex - currFirstLuckyIndex) + 
	   (currLastLuckyIndex - currFirstLuckyIndex)/2 )
	//	   STREAK_SIZE_DIFF_THRESHOLD)
      {
	// A new winner
	currLastLuckyIndex = tempLastLuckyIndex;
	currFirstLuckyIndex = tempFirstLuckyIndex;
      }
      // Note that if two groups' sizes are similar enough, then we
      // go ahead and see if either one is closer to the last commanded
      // steering. This is for stability of the system (no zigzags) given
      // jumping state data (for position and heading mainly).
      else if ( (tempLastLuckyIndex - tempFirstLuckyIndex) > 
		(currLastLuckyIndex - currFirstLuckyIndex) - 
		(currLastLuckyIndex - currFirstLuckyIndex)/2 )
	// STREAK_SIZE_DIFF_THRESHOLD)
      {
	// Also compare to the last steering command and then decide on the
	// winner.
	if (fabs(GetPhi((tempLastLuckyIndex + tempFirstLuckyIndex)/2)-lastPhi)
	    <
	    fabs(GetPhi((currLastLuckyIndex + currFirstLuckyIndex)/2)-lastPhi))
	{
	  // A new winner
	  currLastLuckyIndex = tempLastLuckyIndex;
	  currFirstLuckyIndex = tempFirstLuckyIndex;
	}
	else if (
	   fabs(GetPhi((tempLastLuckyIndex + tempFirstLuckyIndex)/2)-lastPhi) 
	   ==
	   fabs(GetPhi((currLastLuckyIndex + currFirstLuckyIndex)/2)-lastPhi)) 
        {
	  /* // Need to check if index-1 or index+1 is ok
	  // Now compare the next criterion: adjacent arc goodnesses
	  if (combined.Votes[tempFirstLuckyIndex-1].Goodness >
	  combined.Votes[currFirstLuckyIndex-1].Goodness) {
	  currLastLuckyIndex = tempLastLuckyIndex;
	  currFirstLuckyIndex = tempFirstLuckyIndex;	    
	  }
	  // yet another criterion: velocity of the middle arc
	  else if(combined.Votes[(tempLastLuckyIndex-tempFirstLuckyIndex)/2].Velo
	       > combined.Votes[(currLastLuckyIndex-currFirstLuckyIndex)/2].Velo) 
	  {
	    currLastLuckyIndex = tempLastLuckyIndex;
	    currFirstLuckyIndex = tempFirstLuckyIndex;	    
	  }
	  */
	} // end of else if(same distance from prev commanded arc)
      } // end of else if(size of streaks about the same)
    } // end of else if(thisarc.Goodness < bestarc_sofar.Goodness)

  } // end of for(go through arcs)

  // Go through the lucky streak (with length >= 2)
  if (luckyStreak) {
    // The goodness for the lucky streak was the highest out of all, so we
    // must pick the center one from the streak.

    // floor (i.e. biased to left) -- TODO: might want to change this later
    int mostLucky = (int) ((currFirstLuckyIndex + currLastLuckyIndex) / 2);
    ret.phi = GetPhi(mostLucky);
    ret.vel = combined.Votes[mostLucky].Velo;
    /*
    cout << "we actually got to choosing from a lucky streak! " << endl;
    cout << "FirstLucky: " << currFirstLuckyIndex << endl;
    cout << "LastLucky: " << currLastLuckyIndex << endl;
    cout << "Size: " << currLastLuckyIndex - currFirstLuckyIndex << endl;
    */
  }

  // If the best velocity is zero, then we take that to mean that there is
  // no good path. So we keep the old steering angle. There's no mechanism
  // to assure that zero velocity => no good path, but that's how the system
  // should assign the velocity/goodness anyway.
  if (ret.vel == 0) {
    ret.phi = lastPhi;
  }

  return ret;
}


// VELOCITY DISCRIMINATION FOR CHOOSING THE BEST ARC?
/*
           // If we can go faster on i, then it's better
        if ( (ret.vel < combined.Votes[i].Velo) ||
           // Same velocity, but more straight, also better
             ( (ret.vel == combined.Votes[i].Velo &&
                 abs(ret.phi) > abs(GetPhi(i))) ) ) {
	  // A new winner
          firstLuckyIndex = i;
          lastLuckyIndex = i;
          luckyStreak = false;
          ret.phi = GetPhi(i);
          ret.vel = combined.Votes[i].Velo;        
        }
*/
