#ifndef __VOTE_H__
#define __VOTE_H__

/* Vote.hh
   Author: J.D. Salazar
   Created: 8-25-03

   Simplified by Ike Gremmer 1/11/04
   
*/

/* NOTE: If we ever want to change the number of arcs, we should also
 *       include the steering angle (double Phi) for safety
 */

struct Vote
{
  public:
  // The value of the vote
  double Goodness;
  // The velocity along the arc in m/s
  double Velo;

  // Basic constructor
  Vote(double good=0, double vel=0)
    : Goodness(good), Velo(vel) {}

  // Assignment Operator
  Vote &operator= (const Vote &other)
  {
    Goodness = other.Goodness;
    Velo = other.Velo; 
    return (*this);
  }

  // Comparison operators
  bool operator== (const Vote &other) const
  {
    return (Goodness == other.Goodness && Velo == other.Velo);
  }

  bool operator!= (const Vote &other) const { return !(*this==other); }
 
};

#endif
