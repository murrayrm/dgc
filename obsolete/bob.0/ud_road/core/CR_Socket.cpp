//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

#include "CR_Socket.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

extern int vehicle_steering_vote;
extern double vehicle_steering_goodness;
extern double vehicle_recommended_velocity;

extern double vehicle_speed;

// state message

int Timestamp_sec, Timestamp_usec;
double Northing, Easting;  // utm coordinates (same zone whole time)
float Altitude;
float Vel_E, Vel_N, Vel_U;
float Speed;
double Accel;
float Pitch, Roll, Yaw;
float PitchRate, RollRate, YawRate;

// map message

int no_map = TRUE;

int map_rows, map_cols;   // vehicle (projection of center of rear axel on ground) is roughly at center pixel
float map_vert_res, map_horiz_res;  // in meters
double map_x, map_y;   // coordinates of lower-left corner of map, in utm
float *map_data;  // rows * cols in row-major order top to bottom

// map data values are in meters, positive is down.  max point in grid square (over several time steps) 
// down to a min of -3.3 meters 

SOCKET m_socket;

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// grandchallenge.caltech.edu:/usr/local/src/cvsroot

//char sendbuf[1024];
//sprintf(sendbuf, "%d %d %lf %lf %f %f %f %f %f %lf %f %f %f %f %f %f",
//	d.SS.Timestamp.sec(), d.SS.Timestamp.usec(), 
//	d.SS.Easting, d.SS.Northing, d.SS.Altitude,
//	d.SS.Vel_E, d.SS.Vel_N, d.SS.Vel_U,
//	d.SS.Speed, d.SS.Accel,
//	d.SS.Pitch, d.SS.Roll, d.SS.Yaw,
//	d.SS.PitchRate, d.SS.RollRate, d.SS.YawRate);

// first char is "S" for state, "M" for map

DWORD WINAPI receive_thread( LPVOID lpParam ) 
{
  //  char recvbuf[32];
  char recvbuf[1024];
  char map_recvbuf[1000000];  // should handle 500 x 500 array of space-separated 4-byte floats easily
  int bytesRecv, i;
  char message_type;

  while (1) {

    //    bytesRecv = recv( m_socket, recvbuf, 32, 0 );
    //    sscanf(recvbuf, "%lf", &vehicle_speed);
    //   printf("got %s\n", recvbuf);
    //    printf("speed %lf\n", vehicle_speed);

    //    bytesRecv = recv(m_socket, recvbuf, 1, 0);
    bytesRecv = recv(m_socket, recvbuf, 1024, 0);
    //    printf("%c\n", recvbuf[0]);

    if (recvbuf[0] == 'S') {
      
      sscanf(recvbuf, "%c%d %d %lf %lf %f %f %f %f %f %lf %f %f %f %f %f %f",
	     &message_type,
	     &Timestamp_sec, &Timestamp_usec, 
	     &Easting,  &Northing,  &Altitude,
	     &Vel_E,  &Vel_N,  &Vel_U,
	     &Speed,  &Accel,
	     &Pitch,  &Roll,  &Yaw,
	     &PitchRate,  &RollRate,  &YawRate);

      printf("State: %d %d %lf %lf %f %f %f %f %f %lf %f %f %f %f %f %f\n",
	     Timestamp_sec, Timestamp_usec, 
	     Easting,  Northing,  Altitude,
	     Vel_E,  Vel_N,  Vel_U,
	     Speed,  Accel,
	     Pitch,  Roll,  Yaw,
	     PitchRate,  RollRate,  YawRate);
    }
    else {   // 'M' precedes map headers

      //      bytesRecv = recv(m_socket, recvbuf, 1024, 0);

      //  printf("%s\n", recvbuf);

      sscanf(recvbuf, "%c%d %d %f %f %lf %lf %f ",
	     &message_type,
	     &map_rows, &map_cols,
	     &map_vert_res, &map_horiz_res,
	     &map_x, &map_y,
	     &Yaw);

      if (no_map) {
	map_data = (float *) CR_calloc(map_rows * map_cols, sizeof(float));
	no_map = FALSE;
      }

      printf("Map: %d %d %f %f %lf %lf %f\n",
	     map_rows, map_cols,
	     map_vert_res, map_horiz_res,
	     map_x, map_y,
	     Yaw);

      //      printf("%i %i %i = %i\n", sizeof(float), map_rows, map_cols, (1 + sizeof(float)) * map_rows * map_cols - 1);

      bytesRecv = recv(m_socket, map_recvbuf, sizeof(float) * map_rows * map_cols, 0);

      //      memcpy(map_data, map_recvbuf, sizeof(float) * map_rows * map_cols);
      memcpy(map_data, map_recvbuf, sizeof(float) * (map_rows) * (map_cols));

      printf("%f\n", map_data[0]);

      printf("got map data\n");

      /*      for (i = 0; i < map_rows * map_cols - 1; i++)
	sscanf(map_recvbuf, "%f", &map_data[i]);
      sscanf(map_recvbuf, "%f", &map_data[map_rows * map_cols - 1]);
      */

    }

  }
}

//----------------------------------------------------------------------------

DWORD WINAPI send_thread( LPVOID lpParam ) 
{
  char sendbuf[32];
  int bytesSent;

  while (1) {

    /*
    if (vehicle_steering_vote != -1) {

      sprintf(sendbuf, "%i %lf %lf", 
	      vehicle_steering_vote, 
	      vehicle_steering_goodness, 
	      vehicle_recommended_velocity);
   
      printf("sending %s\n", sendbuf);
      bytesSent = send(m_socket, sendbuf, strlen(sendbuf), 0);
      
      if (bytesSent == -1)
	printf("%i\n", WSAGetLastError());

    }
    */

  }
}

//----------------------------------------------------------------------------

// client code

void make_connection()
{
  // Initialize Winsock.
  WSADATA wsaData;
  int iResult = WSAStartup( MAKEWORD(2,2), &wsaData );
  if ( iResult != NO_ERROR )
    printf("Error at WSAStartup()\n");
  
  // Create a socket.
  m_socket = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP );
  
  if ( m_socket == INVALID_SOCKET ) {
    printf( "Error at socket(): %ld\n", WSAGetLastError() );
    WSACleanup();
    return;
  }
  
  // Connect to a server.
  sockaddr_in clientService;
  
  clientService.sin_family = AF_INET;
  clientService.sin_addr.s_addr = inet_addr( "192.168.0.8" );  // titanium on bob
  clientService.sin_port = htons( 15001 );
  
  if ( connect( m_socket, (SOCKADDR*) &clientService, sizeof(clientService) ) == SOCKET_ERROR) {
    printf( "Failed to connect.\n" );
    WSACleanup();
    return;
  }
  
  // Send and receive data.
  //  int bytesRecv = SOCKET_ERROR;
  //  char sendbuf[32] = "Client: Sending data.";
  //  char recvbuf[32] = "";
  
  // this has to be a separate thread

 DWORD dwThreadId, dwThrdParam = 1; 
 HANDLE hThread_send, hThread_receive; 
 char szMsg[80];
 DWORD dwWaitResult; 
  
 // start write thread
 
 hThread_send = CreateThread(NULL,                          // default security attributes 
			     0,                             // use default stack size  
			     send_thread,                   // thread function 
			     &dwThrdParam,                  // argument to thread function 
			     0,                             // use default creation flags 
			     &dwThreadId);                  // returns the thread identifier 
 
 if (hThread_send == NULL) {
   wsprintf( szMsg, "CreateThread failed." ); 
   MessageBox( NULL, szMsg, "main", MB_OK );
 }

 // start read thread
 
 hThread_receive = CreateThread(NULL,                          // default security attributes 
				0,                             // use default stack size  
				receive_thread,                   // thread function 
				&dwThrdParam,                  // argument to thread function 
				0,                             // use default creation flags 
				&dwThreadId);                  // returns the thread identifier 
 
 if (hThread_receive == NULL) {
   wsprintf( szMsg, "CreateThread failed." ); 
   MessageBox( NULL, szMsg, "main", MB_OK );
 }

 return;

  /*
  while( bytesRecv == SOCKET_ERROR ) {
    bytesRecv = recv( m_socket, recvbuf, 32, 0 );
    if ( bytesRecv == 0 || bytesRecv == WSAECONNRESET ) {
      printf( "Connection Closed.\n");
      break;
    }
    if (bytesRecv < 0)
      return;
    printf( "R = %ld, \n", bytesRecv );
  }
  */
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
