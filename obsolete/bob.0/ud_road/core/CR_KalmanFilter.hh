//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// CR_Kalman
//----------------------------------------------------------------------------

#ifndef CR_KALMANFILTER_DECS

//----------------------------------------------------------------------------

#define CR_KALMANFILTER_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include "CR_Matrix.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

//-------------------------------------------------
// structure
//-------------------------------------------------

// _P = P_pred
// P_ = P_last
// _z = z_pred
// _x = x_pred
// x_ = x_last
// l_ = l_last

typedef struct crkalman 
{  
  // Kalman parameters

  CR_Matrix *F, *H;                // coefficients in process, measurement equations, respectively
  CR_Matrix *FT, *HT;              // transposes of above matrices
  CR_Matrix *Q, *R;                // covariance of process, measurement noise, respectively
  CR_Matrix *P, *P_pred, *P_last;  // current, one-step, last state error covariances
  CR_Matrix *W;                    // filter gain
  CR_Matrix *S, *Sinv;             // measurement prediction covariance, inverse ("information matrix")
  CR_Matrix *v;                    // innovation
  CR_Matrix *z, *z_pred;           // current, predicted measurement
  CR_Matrix *x, *x_pred, *x_last;  // current, one-step, last state estimates
  CR_Matrix *Input;                // control input to system

} CR_KalmanFilter;

//-------------------------------------------------
// functions
//-------------------------------------------------

CR_KalmanFilter *make_CR_KalmanFilter(CR_Matrix *, CR_Matrix *, CR_Matrix *, CR_Matrix *, CR_Matrix *, CR_Matrix *, CR_Matrix *, CR_Matrix *);
void finish_make_CR_KalmanFilter(CR_KalmanFilter *);
void update_CR_KalmanFilter(CR_Matrix *, CR_KalmanFilter *);

/*
CR_Kalman *copy_CR_Kalman(CR_Kalman *);
void free_CR_Kalman(CR_Kalman *);
CR_Kalman *load_CR_Kalman(char *);
void compute_measurement_covariance_pdaf_CR_Kalman(CR_Kalman *);
void compute_combined_innovation_pdaf_CR_Kalman(CR_Kalman *);
void update_nonlinear_CR_Kalman(CR_Matrix **, int, CR_Kalman *);
CR_Kalman *make_nonlinear_pdaf_CR_Kalman(CR_FuncMatrix *, CR_FuncMatrix *, CR_FuncMatrix *, CR_FuncMatrix *, CR_FuncMatrix *, CR_FuncMatrix *, CR_FuncMatrix *, float, float, float, float, float);
void finish_make_pdaf_CR_Kalman(CR_Kalman *, float, float, float, float, float);
CR_Kalman *make_nonlinear_CR_Kalman(CR_FuncMatrix *, CR_FuncMatrix *, CR_FuncMatrix *, CR_FuncMatrix *, CR_FuncMatrix *, CR_FuncMatrix *, CR_FuncMatrix *, CR_FuncMatrix *);
void finish_make_CR_Kalman(CR_Kalman *);
void update_pdaf_CR_Kalman(CR_Matrix **, int, CR_Kalman *);
void update_nnsf_CR_Kalman(CR_Matrix **, int, CR_Kalman *);
CR_Matrix *CR_Kalman_nearest_neighbor(CR_Matrix **, int, CR_Kalman *);
float CR_Kalman_likelihood(CR_Matrix *, CR_Kalman *);
void print_CR_Kalman(CR_Kalman *);
double CR_hypersphere_volume(double, int);
int CR_double_factorial(int);
int CR_factorial(int);
*/

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif  
