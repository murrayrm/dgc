//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// CR_Image
//----------------------------------------------------------------------------

#ifndef CR_IMAGE_DECS

//----------------------------------------------------------------------------

#define CR_IMAGE_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Intel performance library includes

#include <ipl.h>
#include <ipp.h>
//#include <ippcv.h>
#include <ijl.h>
//#include <rl.h>
//#include <mkl.h>
//#include <cv.h>

// my stuff 

#include "CR_Memory.hh"
#include "CR_Error.hh"
//#include "CR_Window.hh"
#include "CR_IplImage.hh"
#include "CR_Matrix.hh"
//#include "CR_Postscript.hh"

//#include "CR_Demo3.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

//#define RGB_TYPE          0
//#define YUV_TYPE          1

//#define IMXY_R(im, x, y)     (IPLXY_R(im->iplim, x, y))
// #define IMXY_G(im, x, y)     (IPLXY_G(im->iplim, x, y))
// #define IMXY_B(im, x, y)     (IPLXY_B(im->iplim, x, y))

#define IMXY_R(im, x, y)      (im->iplim->imageData[(y) * im->iplim->widthStep + 3 * (x)])
#define IMXY_G(im, x, y)      (im->iplim->imageData[(y) * im->iplim->widthStep + 3 * (x) + 1])
#define IMXY_B(im, x, y)      (im->iplim->imageData[(y) * im->iplim->widthStep + 3 * (x) + 2])

// channel is argument: R = 0, G = 1, B = 2

#define IMXY_N(im, x, y, n)      (im->iplim->imageData[(y) * im->iplim->widthStep + 3 * (x) + (n)])

#define GRAYFP_IMXY(im, x, y) (((float *) &(im->iplim->imageData[(y) * im->iplim->widthStep + 4 * (x)]))[0])
 
// quick and dirty
//#define IMXY_I(im, x, y)     (IMXY_G(im, x, y))
#define IMXY_I(im, x, y)     (0.3 * IMXY_R(im, x, y) + 0.6 * IMXY_G(im, x, y) + 0.1 * IMXY_B(im, x, y))

//#define IMXY_PIX(im, x, y, r, g, b)       (IMXY_R(im, x, y) = r, IMXY_G(im, x, y) = g, IMXY_B(im, x, y) = b) 

#define IN_IMAGE_HORIZ(im, x)    (((x) >= 0) && ((x) < im->w))
//#define IN_IMAGE_HORIZ(im, x)    (((x) >= 50) && ((x) < (im->w - 50)))
#define IN_IMAGE_VERT(im, y)     (((y) >= 0) && ((y) < im->h))
#define IN_IMAGE(im, x, y)       (IN_IMAGE_HORIZ(im, (x)) && IN_IMAGE_VERT(im, (y)))

#define IN_IMAGE_HORIZ_MARG(im, x, xm)    (((x) >= xm) && ((x) < im->w - xm))
#define IN_IMAGE_VERT_MARG(im, y, ym)     (((y) >= ym) && ((y) < im->h - ym))
#define IN_IMAGE_MARG(im, x, y, xm, ym)       (IN_IMAGE_HORIZ_MARG(im, (x), (xm)) && IN_IMAGE_VERT_MARG(im, (y), (ym)))

#define CR_UNKNOWN           0
#define CR_IMAGE_PPM         1
#define CR_IMAGE_JPEG        2
#define CR_IMAGE_BITMAP      3
#define CR_IMAGE_DEMO3_RIF   4
#define CR_IMAGE_EPS         5
#define CR_IMAGE_PFM         6

#define CR_IMFILTER_SVD_COLOR   1
#define CR_IMFILTER_NORM_COLOR  2
#define CR_IMFILTER_CCCI        3
#define CR_IMFILTER_RGB         4
#define CR_IMFILTER_RGB_3x3     5

//-------------------------------------------------
// structure
//-------------------------------------------------

//typedef void (*CR_Image_filter) (CR_Image *, CR_Image *);

typedef struct crimage
{
  int type;
  int w, h;
  char *data;     // actual data
  int buffsize;   // number of bits occupied by image data

  double ratio;   // of image size to display window size

  int wstep;      // actual width of image (with padding)

  // rectangular ROI

  int roi_x, roi_y;
  int roi_w, roi_h;

  // polygonal ROI

  CR_Matrix *poly_region;

  // where the image is stored...so that we can do hardware-assisted image processing

  IplImage *iplim;

} CR_Image;

typedef struct crcalibration
{
  CR_Matrix *KK, *KK_inv;  // camera matrix, inverse
  CR_Vector *KC;           // radial distortion matrix
  CR_Vector *X, *Y;        // scratch workspace for points
  int **LUT_x, **LUT_y;    // lookup table for distorted coordinates

} CR_Calibration;


//-------------------------------------------------

typedef struct crimagefilter
{
  int type;     // which of the various goodies below to actually execute
  char *name;
  int response_dimension;
  int w_margin, h_margin;   // how far from image edge filter must be to work (so kernels don't stick out, etc.)

  // SVD color model

  int svd_color_model;            // has a model been filled in yet?
  CR_Matrix *Dist, *Cov, *InvCov;
  CR_Vector *Mean;
  
} CR_Image_Filter;

//-------------------------------------------------

typedef struct crimagekernel
{
  int x, y;   // anchor point, where upper-left corner is (0, 0)
  int w, h;   // dimensions
  float *values;
  IplConvKernelFP *k;

} CR_Image_Kernel;

//-------------------------------------------------
// functions
//-------------------------------------------------

CR_Image *read_JPEG_to_CR_Image(char *filename);

CR_Calibration *initialize_CR_Calibration(char *);
CR_Calibration *initialize_CR_Calibration(double, double, double, double, double, double, double, double);
void undistort_image_CR_Calibration(CR_Image *, CR_Image *, CR_Calibration *);

void draw_rect_CR_Image(int, int, int, int, int, int, int, CR_Image *);
void composite_CR_Image(int, int, CR_Image *, CR_Image *);

void joint_histogram_color_CR_Image(int, int, int, int, CR_Vector *, int, double, double, CR_Image *);
void histogram_color_CR_Image(int, int, int, int, CR_Vector *, CR_Vector *, CR_Vector *, double, double, CR_Image *);
void histogram_trapezoid_color_CR_Image(int, int, int, int, double, CR_Vector *, CR_Vector *, CR_Vector *, CR_Image *);
void histogram_gray_CR_Image(int, int, int, int, CR_Vector *, double, double, CR_Image *);
CR_Image_Kernel *make_CR_Image_Kernel(CR_Matrix *);
void convolve_CR_Image(CR_Image_Kernel *, CR_Image *, CR_Image *);

int cohen_sutherland_clip(double *, double *, double *, double *, int, int, int, int);
int clip_line_to_rect_CR_Image(int *, int *, int *, int *, int, int, int, int);

void best_sobel_edge_along_line_CR_Image(int, int, int, int, CR_Image *, int *, int *, int *, int);
void mean_gradient_along_line_CR_Image(int, int, int, int, CR_Image *, double, double *, double *);

char *num_suffix_plus_extension(char *, int, char *);

CR_Image_Filter *make_CR_Image_Filter(CR_Image *);
void free_CR_Image_Filter(CR_Image_Filter *);
void write_region_filter_responses_CR_Image(char *, CR_Image_Filter *, CR_Matrix *, CR_Image *);
//void filter_pixel_CR_Image_Filter(int, int, CR_Image_Filter *, CR_Image *, CR_Vector *);

void change_submenu_entry_image_processing(int, int, char *);
void reset_submenu_image_processing();
void add_submenu_image_processing();
void handle_submenu_image_processing(int, int, CR_Image *, CR_Image *, CR_Image_Filter *, char *, CR_Image **, int);
int in_range_submenu_image_processing(int);

void write_CR_Image(char *, CR_Image *);
void write_CR_Image_to_EPS(char *, CR_Image *);
void emitEPS_CR_Image(FILE *, CR_Image *);

CR_Image *read_CR_Image(char *);
CR_Image *read_PPM_to_CR_Image(char *);
void read_PPM_to_CR_Image(char *, CR_Image *);
CR_Image *read_demo3_rif_to_CR_Image(char *);

CR_Image *make_CR_Image(int, int, int);
CR_Image *make_FP_CR_Image(int, int, int);
void free_CR_Image(CR_Image *);
CR_Image *copy_CR_Image(CR_Image *);
void ip_copy_CR_Image(CR_Image *, CR_Image *);
void set_CR_Image(int, CR_Image *);

int rgb_mean_and_covariance_CR_Image(CR_Image *, int, CR_Vector *, CR_Matrix *);
void rgb_mahalanobis_distance_CR_Image(CR_Image *, CR_Matrix *, CR_Vector *, CR_Matrix *);
void draw_CR_Matrix_CR_Image(double, CR_Matrix *, CR_Image *, int);
void interp_draw_CR_Matrix_CR_Image(CR_Matrix *, CR_Image *);

int determine_type_CR_Image(char *);

void ip_convert_DIB_to_CR_Image(LPBITMAPINFOHEADER, CR_Image *);
void ip_convert_CR_Image_to_DIB(CR_Image *, LPBITMAPINFOHEADER);
void set_pixel_CR_Image(int, int, unsigned char *, CR_Image *);
void set_pixel_rgb_CR_Image(int, int, int, int, int, CR_Image *);
void set_roi_CR_Image(int, int, int, int, CR_Image *);
void get_roi_CR_Image(int *, int *, int *, int *, CR_Image *);
void vflip_CR_Image(CR_Image *, CR_Image *);

int thresholded_point_sobel_3x3(int, int, int, CR_Image *);
void sobel_CR_Image(int, CR_Image *, CR_Image *);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif


