/*
 *Definitions of helper methods for corridor arc evaluator.
 */

#include "genMap.cc"

/* *METHOD isInsideCorridor
 *
 * +DESCRIPTION: This method receives a waypoint index and go to WP list to get information
 *               about this corridor such as offset boundary as retunrs wheather or not the
 *               point is inside the corridor
 *
 * +PARAMETERS
 *
 *  - waypointNumber: the index of the waypoint list
 *  - point         : a point in UTM coordinates
 *
 * +RETURN: TRUE if inside FALSE if not
 */
bool genMap::isInsideCorridor(int waypointNumber,
                              pair<double,double> NorthEast)
{
  bool isInside=false;
  bool pointCrossesLine;
  pair<bool,double> pointLine;
  double dist_point2ab=0;
  NorthEastDown a,b; // points that determine the lines between lastWaypoint to nextEaypoint to the next one o----o
  NorthEastDown point;
  if(waypointNumber+1 < WaypointList.getNumTargetPoints()){
    // Point
    point.Northing = NorthEast.first;
    point.Easting = NorthEast.second;
    point.Downing = 0;
    // Last Waypoint
    a.Northing = (WaypointList.getTargetPoints())[waypointNumber].Northing;
    a.Easting = (WaypointList.getTargetPoints())[waypointNumber].Easting;
    a.Downing = 0;
    // Next waypoint
    b.Northing = (WaypointList.getTargetPoints())[waypointNumber+1].Northing;
    b.Easting = (WaypointList.getTargetPoints())[waypointNumber+1].Easting;
    b.Downing = 0;

    // Distance between point and lines
    pointLine = distancePoint2Line(a,b,point);
    pointCrossesLine = pointLine.first;

    dist_point2ab = pointLine.second;
    if( (pointCrossesLine) && (dist_point2ab < (WaypointList.getTargetPoints())[waypointNumber].radius) ){
      isInside=true;
    }
  }
  return(isInside);
}

/* *METHOD isInsideWaypointRadius
 *
 * +DESCRIPTION: This method receives a waypoint index and go to WP list to get information
 *               about its target radius as retunrs wheather or not the point is inside the
 *               waypoint
 *
 * +PARAMETERS
 *
 *  - waypointNumber: the index of the waypoint list
 *  - point         : a point in UTM coordinates
 *
 * +RETURN: TRUE if inside FALSE if not
 */
bool genMap::isInsideWaypointRadius(int waypointNumber,
                                    pair<double,double> NorthEast)
{
  bool isInside=false;
  NorthEastDown point, center;
  double dist;
  double lastWaypointRadius=0;

  if(waypointNumber < WaypointList.getNumTargetPoints()){
    // Point
    point.Northing = NorthEast.first;
    point.Easting = NorthEast.second;
    point.Downing = 0;
    // Center of the next waypoint
    center.Northing = (WaypointList.getTargetPoints())[waypointNumber].Northing;
    center.Easting = (WaypointList.getTargetPoints())[waypointNumber].Easting;
    center.Downing = 0;

    // Evaluating the distance
    dist = sqrt( (point.Northing - center.Northing)*(point.Northing - center.Northing) + (point.Easting - center.Easting)*(\
															   point.Easting - center.Easting) );

    // if we are not at waypoint zero, consider the last waypoint radius
    if( waypointNumber ) {
      lastWaypointRadius = (WaypointList.getTargetPoints())[waypointNumber-1].radius;
    }

    // check to see if we are within the union of the discs formed by the
    // current and previous waypoint
    if(dist  < fmax((WaypointList.getTargetPoints())[waypointNumber].radius, lastWaypointRadius))
      isInside = true;
  }
  else{
    isInside = true; // final waypoint!!!
  }
  return(isInside);
}

/* *METHOD isInsidePath
 *
 * +DESCRIPTION: This method receives a UTM coordinate point and verify wheather or not the
 *               point is inside the allowed path. It uses the index of the last/next waypoints
 *
 * +PARAMETERS
 *
 *  - point         : a point in UTM coordinates
 *
 * +RETURN: TRUE if inside FALSE if not
 */
bool genMap::isInsidePath(pair<double,double> NorthEast)
{
  // the one before
  bool isInside=false;
  // look for waypoints inside the map
  bool waypointOutsideMap;
  double minNorthing, maxNorthing;
  double minEasting, maxEasting;
  double heigth, width;
  int indWaypoint;

  // Previous wayPoint, before the one we just got
  if(lastWaypoint){
    //cout << "checking waypoint " << lastWaypoint << endl;
    if( (isInsideWaypointRadius(lastWaypoint-1,NorthEast)) || (isInsideCorridor(lastWaypoint-1,NorthEast)) ){
      isInside = true;
    }
  }

  // last waypoint
  //cout << "checking waypoint " << lastWaypoint << endl;
  if( (isInsideWaypointRadius(lastWaypoint,NorthEast)) || (isInsideCorridor(lastWaypoint,NorthEast)) ){
    isInside = true;
  }

  // Deermining the Northin and Easting inside the map
  heigth = numRows*rowRes;
  width = numCols*colRes;
  minNorthing = leftBottomUTM_Northing - ENLARGE_MAP_HOW_MANY_TIMES*heigth;
  maxNorthing = minNorthing + heigth + ENLARGE_MAP_HOW_MANY_TIMES*heigth;
  minEasting = leftBottomUTM_Easting - ENLARGE_MAP_HOW_MANY_TIMES*width;
  maxEasting = minEasting + width + ENLARGE_MAP_HOW_MANY_TIMES*width;

  // Looking for next waypoints that is inside the map
  indWaypoint=nextWaypoint;
  waypointOutsideMap=false;
  while((indWaypoint < WaypointList.getNumTargetPoints()) && (!waypointOutsideMap)){
    if( (isInsideWaypointRadius(indWaypoint,NorthEast)) || (isInsideCorridor(indWaypoint,NorthEast)) ){
      isInside = true;
    }
    if(( (WaypointList.getTargetPoints())[indWaypoint].Northing <= minNorthing ) || ( (WaypointList.getTargetPoints())[indW\
														       aypoint].Northing >= maxNorthing )){
      waypointOutsideMap = true;
    }
    if(((WaypointList.getTargetPoints())[indWaypoint].Easting <= minEasting) || ((WaypointList.getTargetPoints())[indWaypoint].Easting >= maxEasting)){
      waypointOutsideMap = true;
    }
    indWaypoint++;
  }
  /* DEBUG */
  /*
  for(int i=0;i<numTargetPoints;i++){
    if( (isInsideWaypointRadius(i,NorthEast)) || (isInsideCorridor(i,NorthEast)) ){
      isInside = true;
    }
  }
  */
  return(isInside);
}



/* *METHOD paintCorridor
 *
 * +DESCRIPTION: This method receives the rows and cols to analyze and paint
 *  inside the corridor as a good terrain and outside as bad terrain
 *
 * +PARAMETERS
 *
 *  - startPoint : the pair <row,col> on the map we will start to paint the corridor
 *  - deltaPoint : the pair <Drow,Dcol> indicating how many rows and colls
 *
 * +RETURN: void
 */
void genMap::paintCorridor(RowCol startPoint,RowCol deltaPoint)
{
  pair<double,double> NorthEast;
  RowCol curPoint, incRow,incCol, finalPoint;
  // cells outside corridor
  CellData newCD = OUTSIDE_CELL;
  int signDeltaRow=1;
  int signDeltaCol=1;
  int row, col;

  if(deltaPoint.first < 0)
    signDeltaRow = -1;
  if(deltaPoint.second < 0)
    signDeltaCol = -1;

  // If the window swept more than the map size
  if( (signDeltaRow*deltaPoint.first >= numRows) ||
      (signDeltaCol*deltaPoint.second >= numCols) ){
    for(row=0; row<numRows; row++){
      for(col=0; col<numCols; col++){
        curPoint.first = row;
        curPoint.second = col;
        NorthEast = RowCol2UTM(curPoint);
        if(!isInsidePath(NorthEast)){
          setCellDataRC(curPoint.first, curPoint.second, newCD);
        }
      } // for col
    } // for row
  }
  else{
    finalPoint = RowColFromDeltaRowCol(startPoint,deltaPoint);

    // Evaluating the rows
    if(finalPoint.first != startPoint.first){
      incRow.first = 1*signDeltaRow;
      incRow.second = 0;
      curPoint = startPoint;
      do{
        for(col=0; col<numCols; col++){
          curPoint.second = col;
          NorthEast = RowCol2UTM(curPoint);
          if(!isInsidePath(NorthEast)){
            setCellDataRC(curPoint.first, curPoint.second, newCD);
          }
        }
        curPoint = RowColFromDeltaRowCol(curPoint,incRow);
      }while(curPoint.first != finalPoint.first);
    }

    // Evaluating the cols
    if(finalPoint.second != startPoint.second){
      incCol.second = 1*signDeltaCol;
      incCol.first = 0;
      curPoint = startPoint;
      do{
        for(row=0; row<numRows; row++){
          curPoint.first = row;
          NorthEast = RowCol2UTM(curPoint);
          if(!isInsidePath(NorthEast)){
            setCellDataRC(curPoint.first, curPoint.second, newCD);
          }
        }
        curPoint = RowColFromDeltaRowCol(curPoint,incCol);
      }while(curPoint.second != finalPoint.second);
    }
  } // End If the window swept more than the map size
}
