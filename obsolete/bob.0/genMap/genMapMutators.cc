/*
 * Definitions of methods declared in genMap.hh for making changes to a genMap after its initialization.
 */

#include "genMap.hh"

// Assignment Operator
genMap &genMap::operator=(const genMap &other)
{
  if (this != &other) {
    destroy();
    numRows = other.numRows;
    numCols = other.numCols;
    rowRes = other.rowRes;
    colRes = other.colRes;
    vehRow = other.vehRow;
    vehCol = other.vehCol;
    leftBottomUTM_Northing = other.leftBottomUTM_Northing;
    leftBottomUTM_Easting  = other.leftBottomUTM_Easting;
    leftBottomRow = other.leftBottomRow;
    leftBottomCol = other.leftBottomCol;
    evalCorridor = other.evalCorridor;
    pathMaxDistance = other.pathMaxDistance;
    WaypointList = other.WaypointList;
    lastWaypoint = other.lastWaypoint;
    nextWaypoint = other.nextWaypoint;
    DataLifespan = other.DataLifespan;
    Copy_VState(other.curState);
    theMap = other.theMap;
  }
  return *this;
}

//Mutators
void genMap::setLastWaypoint(int pLastWaypoint){
  lastWaypoint = pLastWaypoint;
}
void genMap::setNextWaypoint(int pNextWaypoint){
  nextWaypoint = pNextWaypoint;
}
void genMap::setNumTargetPoints(int pNumTargetPoints){
  numTargetPoints = pNumTargetPoints;
}
void genMap::setTargetPoints(RDDFVector pTargetPoints){
  targetPoints = pTargetPoints;
}


// Mutators
void genMap::setCellDataRC(int rowNumber, int colNumber, struct CellData newCD)
{
  if (rowNumber < 0 || rowNumber >= numRows) {
    cout << "row number input out of range: " << rowNumber << endl;
  }
  else if (colNumber < 0 || colNumber >= numCols) {
    cout << "col number input out of range: " << colNumber << endl;
  }
  else {
    RowCol findingIndices(rowNumber, colNumber);
    if ( theMap.find(findingIndices) == theMap.end() ) {
      // The cell doesn't exist. Create and insert
      theMap.insert(map<RowCol, CellData>::value_type(findingIndices, newCD));
    }
    else {
      // DON'T UPDATE CORRIDOR CELL
      if(((theMap.find(findingIndices))->second).type == OUTSIDE_CORRIDOR_CELL )
	{
	  newCD.type = OUTSIDE_CORRIDOR_CELL;
	}
      ((theMap.find(findingIndices))->second) = newCD;
    }

    // Updating timestamp
    ((theMap.find(findingIndices))->second).timeStamp = curTimeStamp;
  }
}

void genMap::setCellDataUTM(double Northing, double Easting,
                            struct CellData newCD)
{
  // From the left bottom UTM coord, find the row and col for the input coord.
  RowCol findingIndices = UTM2RowCol(Northing, Easting);
  setCellDataRC(findingIndices.first, findingIndices.second, newCD);
}

void genMap::setCellDataFrameRef(double dist_ahead, double dist_aside,
                                 struct CellData newCD)
{
  RowCol findingIndices, vehPos;
  vehPos.first = vehRow;
  vehPos.second = vehCol;

  findingIndices = FrameRef2RowCol(vehPos,curState.Yaw,dist_ahead,dist_aside);
  setCellDataRC(findingIndices.first, findingIndices.second, newCD);
}

void genMap::setCellListDataUTM(double Northing, double Easting,
                                double list_val)
{
  // From the left bottom UTM coord, find the row and col for the input coord.
  RowCol findingIndices = UTM2RowCol(Northing, Easting);
  setCellListDataRC(findingIndices.first, findingIndices.second, list_val);
}


void genMap::setCellListDataRC(int rowNumber, int colNumber, double list_val)
{
  if (rowNumber < 0 || rowNumber >= numRows) {
    //cout << "row number input out of range: " << rowNumber << endl;
  }
  else if (colNumber < 0 || colNumber >= numCols) {
    //cout << "col number input out of range: " << colNumber << endl;
  }
  else {
    RowCol findingIndices(rowNumber, colNumber);
    // If it's at the end of the map, then...
    if ( theMap.find(findingIndices) == theMap.end() ) {
      // Create and insert
      CellData newCD;
      pair<RowCol, CellData> insertCell(findingIndices, newCD);
      theMap.insert(insertCell);
    }

    // And then push the elevation value to the back of the list
    ((theMap.find(findingIndices))->second).cellList.push_back(list_val);
  }
}

void genMap::setCellListDataFrameRef(double dist_ahead, double dist_aside,
                                     double list_val)
{
  RowCol findingIndices, vehPos;
  vehPos.first = vehRow;
  vehPos.second = vehCol;
  findingIndices = FrameRef2RowCol(vehPos, curState.Yaw, dist_ahead,
                                   dist_aside);
  setCellListDataRC(findingIndices.first, findingIndices.second, list_val);
}


//////////////////////////////////////////////////////////////////////////////
// Cell Value Evaluation Method for LADAR Planner                     //
//////////////////////////////////////////////////////////////////////////////

void genMap::evaluateCellsSV(int numMinPoints, double verticalLimit)
{
  double front, back, vehDOWN;
  RowCol vehPos;
  vehPos.first = vehRow;
  vehPos.second = vehCol;
  MapIterator it;
  int size;
  CellData tmpCell;

  // Cycle through all the cells
  for(int rowNumber=0; rowNumber < numRows; rowNumber++) {
    for(int colNumber=0; colNumber < numCols; colNumber++) {
      RowCol findingIndices(rowNumber, colNumber);
      //If the cell exists
      it = theMap.find(findingIndices);
      if ( it != theMap.end() ) {
        //If statement to determine whether or not to expire data
        //if((TVNow() - (it->second).timeStamp) > DataLifespan && (it->second).value != insert no data constant here) {

	//Code to set cell to 0 - in effect, clearing the map
	//(it->second).value = 0;
	//Code to set cell to no data - in effect, clearing the map to no data
	//(it->second).value = insert no data constant here;

        //}

        /*
        //Below code is used to determine how many points are in
        //a given cell
        size = (it->second).cellList.size();
        if(size < 25) {
          (it->second).value = 1;
        } else if(size < 50) {
          (it->second).value = 2;
        } else if(size < 75) {
          (it->second).value = 3;
        } else if(size < 100) {
          (it->second).value = 4;
        } else {
          (it->second).value = 5;
        }
        */

        //If this cell's list is big enough
        if((it->second).cellList.size() > numMinPoints) {
          //Sort the list of elevations from lowest to highest
          (it->second).cellList.sort();

          //Since +DOWN is downward, negative values are positive obstacles
          //So pick the highest point (the lowest value)
          //Which is at the front of the list, and that's the value for the cel
          //But we want to ignore values more than 10 feet=3.3m above the
          //vehicle, so...
          //vehDOWN = ((theMap.find(vehPos))->second).value;
          front = (it->second).cellList.front();
          back = (it->second).cellList.back();
          //While the front value is less than -3.3 m
          if(back < verticalLimit) {
            (it->second).value = 0;
            (it->second).cellList.clear();
          } else {
            while(front < verticalLimit) {
              //Pop it off
              (it->second).cellList.pop_front();
              front = (it->second).cellList.front();
            }
            //Set the final value
            tmpCell.value = front;
            setCellDataRC(rowNumber,colNumber,tmpCell);
            //(it->second).value = front;

            //Below commented out code sets the average, not the max
            //double sum = 0;
            //int size = (it->second).cellList.size();
            //while(!(it->second).cellList.empty()) {
            //  sum += (it->second).cellList.front();
            //  (it->second).cellList.pop_front();
            //}
            //(it->second).value = sum/((double) size);
            //printf("Got (%d, %d, %lf)\n", rowNumber, colNumber, front);

            //Set the timestamp of when we set it
            //(it->second).timeStamp = TVNow().dbl();
          }
          //} else {
          //(it->second) = NO_DATA_CELL;
        }

        //Don't forget to empty the list!
        if(!(it->second).cellList.empty()) {
          (it->second).cellList.clear();
        }
      } // end cell exists
    }
  }
}


//////////////////////////////////////////////////////////////////////////////
// Map Window/Frame Translation Methods                                     //
//////////////////////////////////////////////////////////////////////////////

/* *METHOD updateFrame

//////////////////////////////////////////////////////////////////////////////
// Map Window/Frame Translation Methods                                     //
//////////////////////////////////////////////////////////////////////////////

/* *METHOD updateFrame
*
* +DESCRIPTION: Updates curState, vehRow, vehCol, clears out the data
*               inside cells now "new" in the updated map. Also updates
*               the cells outside the corridor, if asked. Further more
*               it updates the last and next waypoint if those are given
* +PARAMETERS
*
*  - newState      : the new state information
*  - updateWindow  : if TRUE the window will slide on the memory, otherwise only
*                     the car sweeps throught it
*  - evalCorridor  : if TRUE the corridor will be painted
*  - pLastWaypoint : index of the last waypoint
*  - pNextWaypoint : index of the next waypoint
*
* +RETURN:
*/
void genMap::updateFrame(struct VState_GetStateMsg newState, bool updateWindow, double nextTimeStamp,
                         int pLastWaypoint, int pNextWaypoint)
{
  double delta_north = newState.Northing - curState.Northing;
  double delta_east = newState.Easting - curState.Easting;
  double floor_delta_north, floor_delta_east;
  RowCol new_point,old_point,ind_point,delta_point;
  RowCol old_leftBottom_point, new_leftBottom_point, aux_point;
  RowCol orignCarPos, frontCarPos;
  RowCol startPaint, deltaPaint;
  pair<double,double> NorthEast;
  pair<bool,double> pointLine;
  bool crossTrackLine;
  NorthEastDown a,b,point;
  int delta_row, delta_col;
  int sgnDeltaNorth = 1,sgnDeltaEast = 1;

  // Updating time stamp
  curTimeStamp = nextTimeStamp;

  // FINDING THE FRONT POSITION OF THE CAR
  orignCarPos.first = vehRow;
  orignCarPos.second = vehCol;
  frontCarPos = FrameRef2RowCol(orignCarPos, curState.Yaw,
                                GPS_2_FRONT_DISTANCE, 0);
  NorthEast = RowCol2UTM(frontCarPos);
  // ADDING THE ACTUAL DISPLACEMENT TO HAVE TO MOST UPDATED POSITION OF THE
  // FRONT OF THE CAR
  NorthEast.first += delta_north;
  NorthEast.second += delta_east;
  // CHECKING IF THE CAR REACHED THE NEXT WAYPOINT OF THE NEXT CORRIDOR, IF YES
  // UPDATE WAYPOINTS
  if( (isInsideCorridor(nextWaypoint,NorthEast)) ||
      (isInsideWaypointRadius(nextWaypoint,NorthEast)) ){
    lastWaypoint = nextWaypoint;
    if(nextWaypoint+1 < WaypointList.getNumTargetPoints()){
      nextWaypoint = (WaypointList.getTargetPoints())[nextWaypoint+1].number;
    }
    else{
      nextWaypoint = WaypointList.getNumTargetPoints();
    }
  }

  // GETTING THE SIGNAL OF THE UTM DISPLACEMENT BETWEEN THE OLD CAR POSITION AND THE NEW ONE
  if(delta_north < 0)
    sgnDeltaNorth *= -1;
  if(delta_east < 0)
    sgnDeltaEast *= -1;

  // UPDATING THE STATE
  Copy_VState(newState);

  // IF THE DISPLACEMENT IS BIGGER THAN THE MAP, ERARE ALL LINES AND REDRAW CORRIDOR FOR ALL LINES
  // IF WE REACHED THE NEXT WAYPOINT ALSO, OTHERWISE WE WOULD HAVE EXPIRED CORRIDOR ON THE PATH
  if( (sgnDeltaEast*delta_east > (numCols-1)*colRes) ||
      (sgnDeltaNorth*delta_north > (numRows-1)*rowRes) )
    {
      // Vehicle position in indices
      if(floor((double)numRows/2)  < (double)numRows/2){
	vehRow = (int)floor((double)numRows/2);
      }
      else{
	vehRow = (int)floor((double)numRows/2) -1;
      }
      if(floor((double)numCols/2)  < (double)numCols/2){
	vehCol = (int)floor((double)numCols/2);
      }
      else{
	vehCol = (int)floor((double)numCols/2) -1;
      }

      // Calculate left bottom UTM coordinates
      leftBottomRow = 0;
      leftBottomCol = 0;
      if(updateWindow){
      leftBottomUTM_Northing = curState.Northing -
        (double)vehRow*rowRes - rowRes/2;
      leftBottomUTM_Easting  = curState.Easting -
        (double)vehCol*colRes - colRes/2;
      clearMap();
      if(evalCorridor){
        // Paiting the corridor
        startPaint.first = leftBottomRow;
        startPaint.second = leftBottomCol;
        deltaPaint.first = numRows;
        deltaPaint.second = numCols;
        paintCorridor(startPaint,deltaPaint);
      }
      }
    }
  //WE STILL CAN KEEP PART OF THE MAP AND UPDATE JUST THE NEW LINES
  else{
    // OLD <ROW,COL> OF THE CAR => old_point
    old_point.first = vehRow;
    old_point.second = vehCol;
    // OLD <ROW,COL> OF THE LEFT BOTTOM CORNER => old_leftBottom_point
    old_leftBottom_point.first = leftBottomRow;
    old_leftBottom_point.second = leftBottomCol;

    // THE DISPLACEMENT IS DECOUPLED AS: d = m + r
    //     - m is the part of the displacement multiple by the resolution (floor_delta_north,floor_delta_east)
    //     - r is the remaining
    floor_delta_north = sgnDeltaNorth * floor(fabs(delta_north/rowRes));
    floor_delta_east = sgnDeltaEast * floor(fabs(delta_east/colRes));

    // NEW <ROW,COL> OF THE CAR IF THERE IS NO REMAINING
    new_point = RowColFromDeltaPosition(old_point, floor_delta_north * rowRes,
                                        floor_delta_east * colRes);
    // NEW <ROW,COL> OF THE LEFT BOTTOM CORNER IF THERE IS NO REMAINING
    new_leftBottom_point = RowColFromDeltaPosition(old_leftBottom_point,
                                                   floor_delta_north*rowRes,
                                                   floor_delta_east*colRes);

    // NEW CELL UTM COORDINATES OF THE CAR IF THERE IS NO REMAINING
    // (we have the real UTM coordinates of the car at curState.Northing, curState.Easting)
    // Finding the center of the cell in UTM
    NorthEast = RowCol2UTM(old_point);
    NorthEast.first  += floor_delta_north*rowRes;
    NorthEast.second += floor_delta_east*colRes;

    /* DEBUG
    cout << "veh    old position = (" << old_point.first << "," << old_point.second << ") new position = (" << new_point.fi\
rst << "," << new_point.second << ")" << endl;
    cout << "detla_north/rowRes(" << detla_north/rowRes << ") floor(ans)(" << floor(abs(detla_north/rowRes)) << ")" <<  end\
l;
    cout << "delta_east/colRes(" << delta_east/colRes << ") floor(ans)(" << floor(abs(delta_east/colRes)) << ")" <<  endl;
    cout << "curEasting = " << curState.Easting
         << "cell_Northing = " << NorthEast.first << " rowRes/2 = " << rowRes/2 << endl;
    cout << "curNorthing = " << curState.Northing
         << "cell_Easting = " << NorthEast.second << " colRes/2 = " << colRes/2 << endl;
    cout << endl << endl;
    */

    // FYI: THE CAR IS NOT ALWAYS IN THE MIDDLE OF A CELL
    // ANALYSING r TO SEE IF WE NEED TO MOVE THE CAR TO ANY OF THE NEIGHBOOR CELLS
    // WE WILL HAVE AT MOST 1 CELL DISPLACEMENT (because it's the remaining of the resolution)
    if( (curState.Northing) < (NorthEast.first - rowRes/2) ){
      ind_point.first = -1;
    }
    if( (curState.Northing) > (NorthEast.first + rowRes/2) ){
      ind_point.first = +1;
    }
    if( (curState.Easting) < (NorthEast.second - colRes/2) ){
      ind_point.second = -1;
    }
    if( (curState.Easting) > (NorthEast.second + colRes/2) ){
      ind_point.second = 1;
    }

    // NEW <ROW,COL> OF THE CAR AFTER ANALYSED THE REMAINING
    new_point = RowColFromDeltaRowCol(new_point,ind_point);
    // NEW <ROW,COL> OF THE LEFT BOTTOM CORNER AFTER ANALYSED THE REMAINING
    new_leftBottom_point = RowColFromDeltaRowCol(new_leftBottom_point,
                                                 ind_point);
    // IF THE CAR MOVES, THE LEFT BOTTOM CORNER MOVES
    // THE ROWS/COLS THEY MOVED HAS TO BE ERASED AND PAINTED
    if(updateWindow){

      /*********************************/
      // WHICH ROW DO WE HAVE TO UPDATE?
      /*********************************/
      ind_point.first = leftBottomRow; // old left bottom row
      ind_point.second = leftBottomCol;// old left bottom col
      delta_point.first = 1*sgnDeltaNorth;
      delta_point.second = 0;
      startPaint.first = 0; startPaint.second = 0;
      deltaPaint.first = 0; deltaPaint.second = 0;

      if( (sgnDeltaNorth < 0)   // IF THE DISPLACEMENT TO THE IS SOUTH
          &&
	  // IF WE REALLY HAVE TO JUMP THE ROW
	  (new_leftBottom_point.first != leftBottomRow)
	  ){
	// AS LONG AS THE ORIGN OF THE MAP IS LEFT_BOTTOM, THE FIRST ROW TO BE ERASED
	// WILL NEVER BE THE ROW YOU USED TO BE
	ind_point = RowColFromDeltaRowCol(ind_point,delta_point);
	aux_point = RowColFromDeltaRowCol(new_leftBottom_point,delta_point);
      }
      else{
	// IF THE DISPLACEMENT IS POSITIVE THE ROW WE WERE HAS TO BE ERASED
	aux_point = new_leftBottom_point;
      }
      
      // THE ROW WE ERASE WILL HAVE TO BE PAINTED -- good idea
      startPaint.first = ind_point.first;
      while(ind_point.first != aux_point.first){
        // debug
        //cout << "ERASING THE ROW " << ind_point.first << endl;
        clearRow(ind_point.first);
        leftBottomUTM_Northing += rowRes*sgnDeltaNorth;
        ind_point = RowColFromDeltaRowCol(ind_point,delta_point);
        deltaPaint.first = deltaPaint.first + sgnDeltaNorth;
      }

      /*********************************/
      // WHICH COL DO WE HAVE TO UPDATE?
      /*********************************/
      ind_point.first = leftBottomRow; // old left bottom row
      ind_point.second = leftBottomCol;// old left bottom col
      delta_point.first = 0;
      delta_point.second = 1*sgnDeltaEast;

      if( (sgnDeltaEast < 0)      // IF THE DISPLACEMENT TO THE IS WEST
          &&
          // IF WE REALLY HAVE TO JUMP THE COL
          (new_leftBottom_point.second != leftBottomCol)
	  ){
        // AS LONG AS THE ORIGN OF THE MAP IS LEFT_BOTTOM, THE FIRST COL TO BE ERASED
        // WILL NEVER BE THE COL YOU USED TO BE
        ind_point = RowColFromDeltaRowCol(ind_point,delta_point);
        aux_point = RowColFromDeltaRowCol(new_leftBottom_point,delta_point);
      }
      else{
        // IF THE DISPLACEMENT IS POSITIVE THE COL WE WERE HAS TO BE ERASED
        aux_point = new_leftBottom_point;
      }

      startPaint.second = ind_point.second;
      while(ind_point.second != aux_point.second){
        // debug
        //cout << "ERASING THE COL " << ind_point.second << endl;
        clearCol(ind_point.second);
        leftBottomUTM_Easting += colRes*sgnDeltaEast;
        ind_point = RowColFromDeltaRowCol(ind_point,delta_point);
        deltaPaint.second = deltaPaint.second + sgnDeltaEast;
      }

      // UPDATING NEW LEFT BOTTOM <ROW,COL>
      leftBottomRow = new_leftBottom_point.first;
      leftBottomCol = new_leftBottom_point.second;
    }

    // Painting corridor on the new ros/col's after wrap arround
    if( (updateWindow) && (evalCorridor) ){
      // DEBUG
      /*
      cout << "Waypoints(" << lastWaypoint << "," << nextWaypoint << ")" << endl;
      cout << "DELTA(N,E) = (" << delta_north << "," << delta_east << ")" << endl;
      cout << "LEFTBOTTOM OLD(" << old_leftBottom_point.first << "," << old_leftBottom_point.second
           << ")   NEW(" << new_leftBottom_point.first << "," << new_leftBottom_point.second << ")" << endl;
      cout << "PAINT    START(" << startPaint.first << "," << startPaint.second
       << ") DELTA(" << deltaPaint.first << "," << deltaPaint.second << ")" << endl << endl;
      */
      paintCorridor(startPaint, deltaPaint);
    }

    // UPDATING NEW CAR <ROW,COL>
    vehRow = new_point.first;
    vehCol = new_point.second;
  }

  // update rotation matrix to reflect current state
  //framematrix.setattitude(curState.Pitch, curState.Roll, curState.Yaw);
}

void genMap::setCurrentWaypoints()
{
  int currentwpnum;
  double vehNorthing, vehEasting;
  vehNorthing = curState.Northing;
  vehEasting = curState.Easting;
  currentwpnum = WaypointList.getCurrentWaypointNumber(vehNorthing, vehEasting);
  setLastWaypoint(currentwpnum-1);
  setNextWaypoint(currentwpnum);
}

void genMap::clearMap()
{
  theMap.clear();
}

void genMap::clearColor(int colorIndex)
{
  RowCol ind;
  cout << "genMap::clearColor NOOOOOOOOOOO !!!!!!!!!!" << endl;
  for(int row = 0; row < numRows; row++){
    for(int column = 0; column < numCols; column++){
      ind.first = row%numRows;
      ind.second = column%numCols;
      // Just for the presentation
      if(theMap.find(ind) != theMap.end()){
        if ((theMap[ind]).color == colorIndex){
          (theMap[ind]).color = 0;
        }
      }
    }
  }
}

void genMap::clearRow(int row)
{
  RowCol pos;
  for(int column = 0; column < numCols; column++){
    pos.first = row%numRows;
    pos.second = column;
    if(theMap.find(pos) != theMap.end())
      theMap.erase(pos);
  }
}


void genMap::clearCol(int col)
{
  RowCol pos;
  for(int row = 0; row < numRows; row++){
    pos.first = row;
    pos.second = col%numCols;
    if(theMap.find(pos) != theMap.end())
      theMap.erase(pos);
  }
}

void genMap::Copy_VState(VState_GetStateMsg newState)
{
  curState.Timestamp = newState.Timestamp;
  curState.Easting = newState.Easting;
  curState.Northing = newState.Northing;
  curState.Altitude = newState.Altitude;
  curState.Vel_E = newState.Vel_E;
  curState.Vel_N = newState.Vel_N;
  curState.Vel_U = newState.Vel_U;
  curState.Speed = newState.Speed;
  curState.Accel = newState.Accel;
  curState.Pitch = newState.Pitch;
  curState.Roll = newState.Roll;
  curState.Yaw = newState.Yaw;
  curState.PitchRate = newState.PitchRate;
  curState.RollRate = newState.RollRate;
  curState.YawRate = newState.YawRate;
}
