/*
 *Definitions of methods that return information about the contents of a genMap
 */

#include "genMap.hh"
// Accessors
int genMap::getNumRows() { return numRows; }
int genMap::getNumCols() { return numCols; }
double genMap::getRowRes() { return rowRes; }
double genMap::getColRes() { return colRes; }
int genMap::getVehRow() { return vehRow; }
int genMap::getVehCol() { return vehCol; }
double genMap::getLeftBottomUTM_Northing(){ return leftBottomUTM_Northing; }
double genMap::getLeftBottomUTM_Easting() { return leftBottomUTM_Easting; }
double genMap::getVehNorthing(){ return leftBottomUTM_Northing + (numRows*numCols/2); }
double genMap::getVehEasting() { return leftBottomUTM_Easting + (numCols*colRes/2); }
int genMap::getLeftBottomRow() { return leftBottomRow; }
int genMap::getLeftBottomCol() { return leftBottomCol; }
double genMap::getPathMaxDistance() { return pathMaxDistance; }
int genMap::getNumTargetPoints() { return WaypointList.getNumTargetPoints(); }
int genMap::getLastWaypoint() { return lastWaypoint; }
int genMap::getNextWaypoint() { return nextWaypoint; }
RDDFVector genMap::getTargetPoints() { return WaypointList.getTargetPoints(); }


// Accessors
CellData genMap::getCellDataRC(int rowNumber, int colNumber)
{
  CellData ret;
  double dT; // time difference
  double param;

  //  CellData retCell;
  if (rowNumber < 0 || rowNumber > numRows) {
    cout << "row number input out of range: " << rowNumber << endl;
    return(NO_DATA_CELL);
  }
  else if (colNumber < 0 || colNumber > numCols) {
    cout << "col number input out of range: " << colNumber << endl;
    return(NO_DATA_CELL);
  }

  RowCol findingIndices(rowNumber, colNumber);

  map<RowCol,CellData>::iterator cell = theMap.find(findingIndices);

  if ( cell == theMap.end() ) {
    // Return the default heigh value
    //cout << "Cell (" << rowNumber << "," << colNumber << ") not found" << endl;
    return(NO_DATA_CELL);
  }
  ret = cell->second;
  // Shrinking obstacles
  dT = curTimeStamp - ret.timeStamp;
  ret.value = shrinkObstacle(ret.value,dT);
  return(ret);
}


CellData genMap::getRecentCellDataRC(int rowNumber, int colNumber)
{
  CellData ret;
  double dT; // time difference

  //  CellData retCell;
  if (rowNumber < 0 || rowNumber > numRows) {
    cout << "row number input out of range: " << rowNumber << endl;
    return(NO_DATA_CELL);
  }
  else if (colNumber < 0 || colNumber > numCols) {
    cout << "col number input out of range: " << colNumber << endl;
    return(NO_DATA_CELL);
  }
  RowCol findingIndices(rowNumber, colNumber);
  if ( theMap.find(findingIndices) == theMap.end() ) {
    // Return the default heigh value
    return(NO_DATA_CELL);
  }
  else {
    ret = (theMap.find(findingIndices))->second;
    // Shrinking obstacles
    dT = curTimeStamp - ret.timeStamp;
    //cout << "delta T = " << dT << " param*dT = " << -(double)EXP_SHRINK_PARAMETER*dT
    //     << " exp() = " << exp(-((double)EXP_SHRINK_PARAMETER)*dT) << endl;
    //cout << " ret.value = " << ret.value << endl;
    if(dT > DataLifespan)
      {
        return(NO_DATA_CELL);
      }
    return(ret);
  }
}

CellData genMap::getCellDataUTM(double Northing, double Easting)
{
  // From the left bottom UTM coord, find the row and col for the input coord.
  RowCol findingIndices = UTM2RowCol(Northing, Easting);
  return getCellDataRC(findingIndices.first, findingIndices.second);
}

CellData genMap::getRecentCellDataUTM(double Northing, double Easting)
{
  // From the left bottom UTM coord, find the row and col for the input coord.
  RowCol findingIndices = UTM2RowCol(Northing, Easting);
  return getRecentCellDataRC(findingIndices.first, findingIndices.second);
}

CellData genMap::getCellDataFrameRef(double dist_ahead, double dist_aside)
{
  RowCol findingIndices, vehPos;
  vehPos.first = vehRow;
  vehPos.second = vehCol;

  findingIndices = FrameRef2RowCol(vehPos,curState.Yaw,dist_ahead,dist_aside);
  return getCellDataRC(findingIndices.first, findingIndices.second);
}

CellData genMap::getRecentCellDataFrameRef(double dist_ahead, double dist_aside)
{
  RowCol findingIndices, vehPos;
  vehPos.first = vehRow;
  vehPos.second = vehCol;

  findingIndices = FrameRef2RowCol(vehPos,curState.Yaw,dist_ahead,dist_aside);
  return getRecentCellDataRC(findingIndices.first, findingIndices.second);
}

