/* Definitions for constructors and ititiallization methods in the generic map class designed to be used for
 * path evaluation by LADAR and stereo vision. See genMap.hh for the whole
 * class.
 *In practice, the contributions of each individual to this class involved work on different types of methods. 
 * Author: Sue Ann Hong  -  General class methods
 *         Adam Craig - Map frame manipulation methods
 *         Thyago Consort - Path evaluation methods
 *
 * Revision History:  
 *   1/15/2004 Created
 *
 *   2004/02/18  Lars Cremean  Changed all instances of leftBottomUTMx to 
 *   leftBottomUTM_Northing and leftBottomUTMy to leftBottomUTM_Easting.
 *   Please check to make sure that these are the proper assignments.
 *   Look for "TODO" comments to see places where the assignments are 
 *   questionable
 *   2004/02/19  Adam Craig    changed assignments so that use of
 *    x = Northing -> Row : y = Easting -> Col axes was consistent. 
 *   03/13-20/04 Adam Craig divided genMap.cc into several smaller files 
 *   containing the function definitions for different types of functions.
 */

#include "genMap.hh"

// Default constructor
genMap::genMap(bool init)
{
  curState.Easting = 0;
  curState.Northing = 0;
  curState.Altitude = 0;
  curState.Vel_E = 0;
  curState.Vel_N = 0;
  curState.Vel_U = 0;
  curState.Speed = 0;
  curState.Accel = 0;
  curState.Pitch = 0;
  curState.Roll = 0;
  curState.Yaw = 0;
  curState.PitchRate = 0;
  curState.RollRate = 0;
  curState.YawRate = 0;

  if(init){
    initMap(NUM_ROWS,NUM_COLS,ROW_RES,COL_RES);
  }
}

// Constructor with default values (except initState)
// No default values??
genMap::genMap(VState_GetStateMsg initState, 
	       int new_num_rows, 
	       int new_num_cols, 
	       double new_row_res, 
	       double new_col_res,
               bool pEvalCorridor, double data_lifespan)
{
  // Accessing RDDF target points
  Copy_VState(initState);
  initMap(new_num_rows,new_num_cols,new_row_res,new_col_res,pEvalCorridor,data_lifespan);
}

// Set path evaluation cost to be used
void genMap::initPathEvaluationCriteria(bool pUseCellsUnderCost, 
					bool pUseOffTireCost,
					bool pUseDotVectorCost,
					bool pUseMaxHeightDiffCost,
					bool pUseMaxInclinationCost,
                                        bool pUseObsAvoidanceVelocities,
                                        bool pUseMaxHeightDiffCostRectangle,
                                        bool pUseConfinedSpaceGoodnessVotes){
  useCellsUnderCost = pUseCellsUnderCost;
  useOffTireCost    = pUseOffTireCost;
  useDotVectorCost  = pUseDotVectorCost;
  useMaxHeightDiffCost = pUseMaxHeightDiffCost;
  useMaxInclinationCost = pUseMaxInclinationCost;
  useObsAvoidanceVelocities = pUseObsAvoidanceVelocities;
  useMaxHeightDiffCostRectangle = pUseMaxHeightDiffCostRectangle;
  useConfinedSpaceGoodnessVotes = pUseConfinedSpaceGoodnessVotes;

  // debug
  /*
  if(useCellsUnderCost) cout << "USING CELLS UNDER COST" << endl;
  if(useOffTireCost) cout << "USING OFF TIRE COST" << endl;
  if(useDotVectorCost) cout << "USING DOT VECTOR COST" << endl;
  if(useMaxHeightDiffCost) cout << "USING MAX HEIGHT DIFF COST" << endl;
  if(useMaxInclinationCost) cout << "USING MAX INCLINATION COST" << endl;
  */
}

// Set velocity evaluation thresholds
void genMap::initVelocityThresholds(double pMaxSpeed, double pMinSpeed,
                              double pDistToObstacleOnArcHalt,
                              double pDistToObstacleAnywhereHalt,
                              double pDistToObstacleOnArcMaxSpeed,
                              double pDistToObstacleAnywhereMaxSpeed){

  velMaxSpeed                    = pMaxSpeed;
  velMinSpeed                    = pMinSpeed; 
  distToObstacleOnArcHalt        = pDistToObstacleOnArcHalt;
  distToObstacleAnywhereHalt     = pDistToObstacleAnywhereHalt;
  distToObstacleOnArcMaxSpeed    = pDistToObstacleOnArcMaxSpeed;
  distToObstacleAnywhereMaxSpeed = pDistToObstacleAnywhereMaxSpeed;
}

// Set path evaluation thresholds
void genMap::initThresholds(double pObstacleThreshold,
			    double pHeightDiffThreshold,
			    double pTireOffThreshold,
			    double pVectorProdThreshold,
                            double pGrowVehicleLength,
                            double pGrowVehicleWidth){

  obstacleThreshold   = pObstacleThreshold;
  heightDiffThreshold = pHeightDiffThreshold; 
  tireOffThreshold    = pTireOffThreshold;
  vectorProdThreshold = pVectorProdThreshold;
  growVehicleLength   = pGrowVehicleLength;
  growVehicleWidth    = pGrowVehicleWidth;
}

// Set genMap, call just once
void genMap::initMap(int num_rows, int num_cols, double row_res, double col_res,bool pEvalCorridor, bool pActivateNoDataCell, double data_lifespan)
{
  RowCol startPos,deltaPos;
  double max;

  // Initializing thresholds
  initThresholds();
  // Initializing how path evaluation will evaluate cost
  initPathEvaluationCriteria();

  // Time stamp
  curTimeStamp = 0;

  // Getting RDDF file
  WaypointList = rddf;
  //cout << numTargetPoints << endl;
  //getchar();

  // Row/Col
  numRows = num_rows;
  numCols = num_cols;
  rowRes = row_res;
  colRes = col_res;

  // Vehicle position in indices
  if(floor((double)numRows/2)  < (double)numRows/2){
    vehRow = (int)floor((double)numRows/2);
  }
  else{
    vehRow = (int)floor((double)numRows/2) -1;
  }
  if(floor((double)numCols/2)  < (double)numCols/2){
    vehCol = (int)floor((double)numCols/2);
  }
  else{
    vehCol = (int)floor((double)numCols/2) -1;
  }

  //Defining the maximum path
  max = (double)numRows*rowRes;
  if(max > numCols*colRes) {
    max = (double)numCols*colRes;
  }
  pathMaxDistance = max/2 - (VEHICLE_LENGTH);

  // Calculate left bottom UTM coordinates
  leftBottomUTM_Northing = curState.Northing - (double)vehRow*rowRes - rowRes/2;
  leftBottomUTM_Easting = curState.Easting - (double)vehCol*colRes - colRes/2;
  leftBottomRow = 0;
  leftBottomCol = 0;
  NUMBER_OF_COLS_RIGHT_TO_CHECK = (int)floor(RADIUS_OF_SEARCHING_FOR_DATA/colRes);
  if(NUMBER_OF_COLS_RIGHT_TO_CHECK < 1)
    {
      NUMBER_OF_COLS_RIGHT_TO_CHECK = 1;
    }
  if(NUMBER_OF_COLS_RIGHT_TO_CHECK > MAX_NUMBER_CELLS_TO_CHECK)
    {
      NUMBER_OF_COLS_RIGHT_TO_CHECK = MAX_NUMBER_CELLS_TO_CHECK;
    }

  NUMBER_OF_CELLS_DIAGONAL_TO_CHECK = (int)floor(RADIUS_OF_SEARCHING_FOR_DATA/sqrt(colRes*colRes + rowRes*rowRes));
  if(NUMBER_OF_CELLS_DIAGONAL_TO_CHECK < 1)
    {
      NUMBER_OF_CELLS_DIAGONAL_TO_CHECK = 1;
    }
  if(NUMBER_OF_CELLS_DIAGONAL_TO_CHECK > MAX_NUMBER_CELLS_TO_CHECK)
    {
      NUMBER_OF_CELLS_DIAGONAL_TO_CHECK = MAX_NUMBER_CELLS_TO_CHECK;
    }

  NUMBER_OF_ROWS_DOWN_TO_CHECK = (int)floor(RADIUS_OF_SEARCHING_FOR_DATA/rowRes);
  if(NUMBER_OF_ROWS_DOWN_TO_CHECK < 1)
    {
      NUMBER_OF_ROWS_DOWN_TO_CHECK = 1;
    }
  if(NUMBER_OF_ROWS_DOWN_TO_CHECK > MAX_NUMBER_CELLS_TO_CHECK)
    {
      NUMBER_OF_ROWS_DOWN_TO_CHECK = MAX_NUMBER_CELLS_TO_CHECK;
    }


  // TOO SLOW
  // Adding "NULL" elements to the map
  //  RowCol posMap;
  /*for(int rowInd=0; rowInd< numRows ;rowInd++){
    for(int colInd =0; colInd< numCols; colInd++){
      posMap.first = rowInd;
      posMap.second = colInd;
      theMap[posMap] = NO_DATA_CELL;
    }
  }*/
  theMap.clear();

  // Initializing Waypoints
  if(WaypointList.getNumTargetPoints()){
    lastWaypoint = (WaypointList.getTargetPoints())[0].number;
    nextWaypoint = lastWaypoint;
  }
  lastWaypoint = 0;
  nextWaypoint = 0;
  numTargetPoints = 0;
  targetPoints.clear();

  //if(numTargetPoints > 1){
  //  nextWaypoint = targetPoints[1].number;
  //}
  DataLifespan = data_lifespan;
  activateNoDataCell = pActivateNoDataCell;

  // painting corridor as obstacles (used by CorridorArcEvaluator)
  evalCorridor = pEvalCorridor;
  if(evalCorridor){
    startPos.first = leftBottomRow;
    startPos.second = leftBottomCol;
    deltaPos.first = numRows;
    deltaPos.second = numCols;
    paintCorridor(startPos,deltaPos);
  }
}
 
// Copy Constructor
genMap::genMap(const genMap &other)
  : numRows(other.numRows), numCols(other.numCols),
    rowRes(other.rowRes), colRes(other.colRes),
    vehRow(other.vehRow), vehCol(other.vehCol),
    leftBottomUTM_Easting(other.leftBottomUTM_Easting), 
    leftBottomUTM_Northing(other.leftBottomUTM_Northing),
    leftBottomRow(other.leftBottomRow), leftBottomCol(other.leftBottomCol),
    evalCorridor(other.evalCorridor), pathMaxDistance(other.pathMaxDistance),
    numTargetPoints(other.numTargetPoints),
    targetPoints(other.targetPoints),
    lastWaypoint(other.lastWaypoint), nextWaypoint(other.nextWaypoint), DataLifespan(other.DataLifespan)
{
  Copy_VState(other.curState);
  theMap = other.theMap;
  WaypointList = other.WaypointList;
}

