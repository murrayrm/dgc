/*
 * Definitions of methods declared in genMap.hh for the evaluation of cost, distance to obstacle, and optimum velocity and 
 * for the generation of votes.
 */

#include "genMap.hh"

// Shape the goodness for the case we are outside of the corridor                                                                          
/* double genMap::goodnessShape(int side, int indArc, int numArcs, double scale)                                                           
 * +DESCRIPTION: This functions returns a goodness of a given steering angle                                                               
 *               for the votes based on the vector field to the next waypoint                                                              
 * +PARAMETERS                                                                                                                             
 *   - side    : where you want to turn, LEFT_SIDE for left and RIGHT_SIDE for right                                                       
 *   - indArc  : index of the arc                                                                                                          
 *   - numArcs : number of arcs                                                                                                            
 *   - scale   : this must be a internal product between the actual direction and                                                          
 *     [0 1]     the desired one. Used to scale how hard to turn the steering wheels                                                       
 * +RETURN:                                                                                                                                
 *   - goodness : goodnes evaluated for that arc                                                                                           
 */ 
double genMap::goodnessShape(int side, int indArc, int numArcs, double scale) 
{ 
  double a,b,c;    // y = a(x-m)^2 + b(x-m) + c ; y' = 2a(x-m) + b                                                                         
  double y,m; 
  
  if(numArcs){ 
    m = ((double)(-side*(-scale+1)))*(numArcs)/4; 
    a = -2*((double)(MAX_GOODNESS_SHAPE - MIN_GOODNESS_SHAPE))/(((double)(numArcs-1)/2)*((double)(numArcs-1)/2)); 
    b = -2*a*m; 
    c = (double)MAX_GOODNESS_SHAPE - a*m*m - b*m; 
    y = a*(indArc-((double)numArcs)/2)*(indArc-((double)numArcs)/2) + b*(indArc-((double)numArcs)/2) + c;
    if(y < MIN_GOODNESS_SHAPE) { 
      y = (double)MIN_GOODNESS_SHAPE; 
    } 
  } 
  else{ 
    y = 0; 
  } 
  //cout << " (a,b,c,m) = (" << a << "," << b << "," << c << "," << m << ")"                                                               
  //     << " y = " << y << endl;                                                                                                          
  return(y); 
} 
 
// Shape the goodness for obstacle avoidance, in case all is 100 we don't make circles                                                     
/* double genMap::obstacleGoodnessShape(int indArc, int numArcs)                                                                           
 * +DESCRIPTION: This functions returns a goodness of a given steering angle                                                               
 *               for the votes based on the obstacle avoidance                                                                             
 * +PARAMETERS                                                                                                                             
 *   - indArc  : index of the arc                                                                                                          
 *   - numArcs : number of arcs                                                                                                            
 * +RETURN:                                                                                                                                
 *   - goodness(%) [95 100]                                                                                                                
 */ 
double genMap::obstacleGoodnessShape(int indArc, int numArcs) 
{ 
  double a1,b1,a2,b2;    // y = ax + b                                                                                                     
  double y, m; 
 
  if(numArcs){ 
    m = ((double)(numArcs-1))/2; 
    a1 = ((double)(MAX_OBSTACLE_AVOIDANCE_GOODNESS-MIN_OBSTACLE_AVOIDANCE_GOODNESS))/m; 
    a2 = -a1; 
    b1 = (double)MIN_OBSTACLE_AVOIDANCE_GOODNESS; 
    b2 = (double)MAX_OBSTACLE_AVOIDANCE_GOODNESS; 
    if(indArc <= m) 
      y = a1*indArc + b1; 
    else 
      y = a2*(indArc-m) + b2; 
    if(y < MIN_OBSTACLE_AVOIDANCE_GOODNESS) { 
      y = (double)MIN_OBSTACLE_AVOIDANCE_GOODNESS; 
    } 
  } 
 
  else{ 
    y = 0; 
  }
  //cout << " (a1,a2,m) = (" << a1 << "," << a2 << "," << m << ")"                                                                         
  //     << " y = " << y << endl;                                                                                                          
  return(y); 
} 
 
// Shape the velociy to reduce the speed when the steering wheel is turned hard                                                            
/* double genMap::velShape(int indArc, int numArcs)                                                                                        
 * +DESCRIPTION: This functions returns a percentage of the given velocity,                                                                
 *               when the car is going straigth we return the velocity passed                                                              
 *               when it is turning the velocity is reduced in a quadratic way.                                                            
 *               Full right and full left returns 20% of the original one.                                                                 
 * +PARAMETERS                                                                                                                             
 *   - oldVel : velocity                                                                                                                   
 * +RETURN:                                                                                                                                
 *   - newVel : after shaping [0.2*oldVel   oldVel]                                                                                        
 */ 
double genMap::velShape(int indArc, int numArcs, double vel) 
{ 
  double a,b,c;    // y = ax^2 + bx + c ; y' = 2ax + b                                                                                     
  double y,percent; 
 
  if(numArcs){ 
    a = -((double)(PERCENT_MAX_VEL - PERCENT_MIN_VEL)) / 
      (((double)(numArcs-1)/2)*((double)(numArcs-1)/2)); 
    b = -a*((double)(numArcs-1)); 
    c = (double)PERCENT_MIN_VEL; 
    percent = a*indArc*indArc + b*indArc + c; 
  } 
  else{ 
    percent = (double)PERCENT_MAX_VEL; 
  } 
  //cout << " (a,b,c) = (" << a << "," << b << "," << c << ")"                                                                             
  //     << " percent = " << percent << endl;                                                                                              
  y = (percent/100)*vel; 
  return(y); 
} 
 
 
// Shape the costs returned by the cost methods                                                                                            
/* double genMap::costShape(double x, double alpha)                                                                                        
 * +DESCRIPTION: This function shape the cost in order to have more resoluition                                                            
 *               nearby 100 rarther than 0                                                                                                 
 *    For now it's an exponential function                                                                                                 
 * +PARAMETERS                                                                                                                             
 *   - x    : the point to be mapped [0 1]                                                                                                 
 *   - alpha: the exp parameter                                                                                                            
 * +RETURN:                                                                                                                                
 *   - y    : after mapping [0 100]                                                                                                        
 */ 
double genMap::costShape(double x, double alpha) 
{ 
  double y; 
  y = (MAX_VOTE_RANGE - MIN_VOTE_RANGE)*(exp(alpha*x) - 1)/(exp(alpha) - 1) + 
    MIN_VOTE_RANGE; 
  //cout << "costShape(" << x << "," << alpha << ") = " << y << endl;                                                                      
  return(y); 
} 
 
 
// Evaluate normal vectors                                                                                                                 
/* NorthEastDown evalNormalVector(NorthEastDown point_a,NorthEastDown point_b,NorthEastDown point_c);                                      
 * +DESCRIPTION: This method receive the three points that define the plane                                                                
 *    and return the vector normal to it                                                                                                   
 * +PARAMETERS                                                                                                                             
 *   - point_a,point_b,point_c: north east and down  coordinates in UTM of the points inside the plane                                     
 * +RETURN: north, east and down coordinates of the normal vector                                                                          
 */ 
 
NorthEastDown genMap::evalNormalVector(NorthEastDown point_a, 
                                       NorthEastDown point_b, 
                                       NorthEastDown point_c) 
{ 
  NorthEastDown normal,vec_a,vec_b; 
 
  vec_a = point_a - point_b; 
  vec_b = point_b - point_c; 
  normal = vec_a^vec_b; 
  normal.norm(); 
 
  return(normal); 
   
} 
 
// Evaluate cost associated with normal vectors                                                                                            
/* double evalNormalVectorCost(NorthEastDown normal_a, NorthEastDown normal_b, NorthEastDown normal_c, NorthEastDown normal_d)             
 * +DESCRIPTION: This method receive the four normal vectors to the planes formed by three of the                                          
 *    four tires and return a cost associated If a pair of vectors is more that VECTOR_PROD_THRESHOLD                                      
 *    the generated vote will be zero. If not the vote will be modeled by an exponential in order to                                       
 *    enfasize little variation arround the max goodness                                                                                   
 * +PARAMETERS                                                                                                                             
 *   - NorthEastDown normal_a, normal_a, normal_a, normal_a: normal vectors coordinates in UTM                                             
 * +RETURN: cost on the range [0 100]                                                                                                      
 */ 
double genMap::evalNormalVectorCost(NorthEastDown *n) 
{ 
  double int_prod[6]; // range [0 1]                                                                                                       
  double cost, min_int_prod, mean_int_prod; 
  NorthEastDown down; 
  int ind,i,j; 
 
  // Assigning the down vector                                                                                                                 
  down.Northing = 0; 
  down.Easting = 0; 
  down.Downing = 1.0; 
  // Getting the norm                                                                                                                      
  for(ind=0;ind < 4;ind++){ 
    n[ind].norm(); 
  } 
 
  // Internal product                                                                                                                      
  /*                                                                                                                                       
  ind=0;                                                                                                                                   
  for(i=0;i < 3;i++){                                                                                                                      
    for(j=(i+1);j < 4;j++){                                                                                                                
      if(i!=j){                                                                                                                            
        // Debug                                                                                                                           
        //cout <<" n[" << i << "] (north,east,down) = (" << n[i].Northing << ","  << n[i].Downing << ","  << n[i].Downing << ")" << endl;  
        //cout <<" n[" << j << "] (north,east,down) = (" << n[j].Northing << ","  << n[j].Downing << ","  << n[j].Downing << ")" << endl;  
        if( ( (n[i].Northing) && (n[i].Easting) && (n[i].Downing) ) && ( (n[j].Northing) && (n[j].Easting) && (n[j].Downing) ) )           
          int_prod[ind] = abs(n[i]*n[j]); // <n[i],n[j]>                                                                                   
        else                                                                                                                                                             
          int_prod[ind] = 1;                                                                                                               
        //cout << "int_prod[" << ind << "] = n[" << i << "] dot n[" << j << "] = " << int_prod[ind] << endl;                               
        ind++;                                                                                                                             
      }                                                                                                                                    
    }                                                                                                                                      
  }                                                                                                                                        
                                                                                                                                           
  // Finding whether one of the vectors is beyong the threshold                                                                            
  min_int_prod = 2.0;                                                                                                                      
  mean_int_prod = 0.0;                                                                                                                     
  for(ind=0; ind<6; ind++){                                                                                                                
    if(int_prod[ind] < min_int_prod){                                                                                                      
      min_int_prod = int_prod[ind];                                                                                                        
    }                                                                                                                                      
    mean_int_prod += int_prod[ind];                                                                                                        
  }                                                                                                                                        
  if(min_int_prod < vectorProdThreshold){                                                                                                  
    cost = 0;                                                                                                                              
  }                                                                                                                                        
  else{                                                                                                                                                                    
    mean_int_prod /= 6;                                                                                                                    
    cost = costShape(mean_int_prod,EXP_PARAM);                                                                                             
  }                                                                                                                                        
  */ 
 
  // Internal product between a vector and the one point down                                                                              
  for(i=0;i < 4;i++){ 
    if( ( (n[i].Northing) && (n[i].Easting) && (n[i].Downing) ) ) 
      int_prod[i] = abs(n[i]*down); // <n[i],down>                                                                                         
    else 
      int_prod[i] = 1; 
    //cout << "int_prod[" << i << "] = n[" << i << "] dot downn = " << int_prod[ind] << endl;                                              
  } 
 
  // Finding whether one of the vectors is beyong the threshold                                                                            
  min_int_prod = 2.0; 
  mean_int_prod = 0.0; 
  for(ind=0; ind<4; ind++){ 
    if(int_prod[ind] < min_int_prod){ 
      min_int_prod = int_prod[ind];  
    } 
    mean_int_prod += int_prod[ind]; 
  } 
  if(min_int_prod < vectorProdThreshold){ 
    cost = 0; 
  } 
  else{ 
    mean_int_prod /= 4; 
    cost = costShape(mean_int_prod,EXP_PARAM); 
  } 
 
  return(cost); 
} 
 
// Evaluate the distance betwwen a point and a plane in R^3                                                                                
/* double distancePoint2Plane(NorthEastDown onPlane_a, NorthEastDown onPlane_b, NorthEastDown onPlane_c, NorthEastDown outPlane);          
 * +DESCRIPTION: This method receive the three points that define the plane                                                                
 *    and a point outside it and returns the minimum distance between them                                                                 
 * +PARAMETERS                                                                                                                             
 *   - onPlane_a, onPlane_b, onPlane_c: points on the plane that defines it                                                                                   
 *   - outPlane: point outside the plane                                                                                                   
 * +RETURN: the distance of the projection                                                                                                 
 */ 
double genMap::distancePoint2Plane(NorthEastDown a, NorthEastDown b, NorthEastDown c, NorthEastDown Po) 
{ 
  NorthEastDown n;            // normal vector to the plane                                                                                
  NorthEastDown Pn;           // intersection between the plane and the line                                                               
  double d;         // independent term the the plane equation a.north + b.east + c.down = d                                               
  double to;        // parameter of the parametrized line equation in R^3 (v = to*w + wo)                                                  
  double distance; 
 
  n = evalNormalVector(a, b, c); 
  n.norm(); 
  d = (n.Northing * a.Northing) + (n.Easting * a.Easting) + (n.Northing * a.Downing); 
  to= d - ( (n.Northing * Po.Northing) + (n.Easting * Po.Easting) + (n.Downing * Po.Downing) ); 
  Pn = Po + n*to; 
  distance = sqrt((Pn-Po)*(Pn-Po)); 
 
  return(distance); 
} 
 
 
// Evaluate the distance betwwen a point and a plane in R^3                                                                                
/* double distancePoint2Line(NorthEastDown onLine_a, NorthEastDown onLine_b, NorthEastDown outLine);                                       * +DESCRIPTION: This method receive the two points that define the line                                                                   *    and a point outside it and returns the minimum distance between them                                                                
 * +PARAMETERS                                                                                                                            
 *   - a, b: points on the line that defines it                                                                                           
 *   - Po: point outside the line                                                                                                         
 * +RETURN: the distance of the projection                                                                                                
 */ 
 
pair<bool,double> genMap::distancePoint2Line(NorthEastDown a, NorthEastDown b, 
                                             NorthEastDown Po) 
{ 
  NorthEastDown v;           // vector on the line direction                                                                               
  NorthEastDown n;           // vector normal to the line through Po                                                                       
  NorthEastDown Pl;          // point on the line resulted from the projection                                                             
  double to;       // line parametrization in terms of to                                                                                  
  double lineLen; // the compriment on the line                                                                                            
  double dist_a2Pl, dist_b2Pl; 
  double distance; 
  bool doesItCross=false; 
  pair <bool,double> ret; 
 
  v = a - b; 
  lineLen = sqrt((b.Northing-a.Northing)*(b.Northing-a.Northing) + (b.Easting-a.Easting)*(b.Easting-a.Easting) + (b.Downing-a.Downing)*(b.\
																	Downing-a.Downing) ); 
  v.norm(); 
  to = -( v.Northing*(a.Northing - Po.Northing) + v.Easting*(a.Easting - Po.Easting) + v.Downing*(a.Downing - Po.Downing) ); 
  Pl = v*to + a;  
  dist_a2Pl = sqrt((a.Northing-Pl.Northing)*(a.Northing-Pl.Northing) + (a.Easting-Pl.Easting)*(a.Easting-Pl.Easting) + (a.Downing-Pl.Downing)*(a.Downing-Pl.Downing) );  
  dist_b2Pl = sqrt((b.Northing-Pl.Northing)*(b.Northing-Pl.Northing) + (b.Easting-Pl.Easting)*(b.Easting-Pl.Easting) + (b.Downing-Pl.Downing)*(b.Downing-Pl.Downing) );  
  if( (dist_a2Pl <= lineLen) && (dist_b2Pl <= lineLen) ){ 
    doesItCross = true; 
  } 
 
  n = Pl - Po; 
  distance = sqrt(n*n); 
  ret.first  = doesItCross; 
  ret.second = distance; 
  return(ret); 
}   
 
// Evaluate cost associated with the tire off the plane determined by the other three                                                      
/* double evalOffTireCost(double dist[4]);                                                                                                 
 * +DESCRIPTION: This method receive the four distances from the tire to a plane determined by the other                                   
 *    three on the ground and evaluate a cost associated with it                                                                           
 * +PARAMETERS                                                                                                                             
 *   - dist[4]: distances from a point to a plane                                                                                          
 * +RETURN: cost on the range [0 100]                                                                                                      
 */ 
double genMap::evalOffTireCost(double *dist) 
{ 
  int ind; 
  double cost, param; 
  double max_dist; 
 
  max_dist = 0; 
  for(ind=0; ind<4; ind++){ 
    if(dist[ind] > max_dist){ 
      max_dist = dist[ind]; 
    } 
  } 
 
  if(max_dist > tireOffThreshold){ 
    cost = 0; 
  } 
  else{ 
    max_dist /= tireOffThreshold; 
    param = 1-max_dist; 
    cost = costShape(param,EXP_PARAM); 
  } 
  return(cost); 
} 
 
 
// Evaluate each the cells the car would be if pointing to pYaw rad and on a certain row/col on the map                                    
/* map<RowCol,RowCol> allCellsUnder(RowCol pos,double pYaw);                                                                               
 * +DESCRIPTION: This method receive the position and yaw of the car and returns all the cells under it                                    
 *                                                                                                                                         
 * +PARAMETERS                                                                                                                             
 *   - RowCol position: row and col of the orign of the car                                                                                
 *   - double pYaw: heading the car is pointing                                                                                            
 * +RETURN: a cost [0 100]                                                                                                                 
 */ 
double genMap::allCellsUnderCost(RowCol position,double pYaw) 
{ 
  double lowerRes, delta_dist, dist_ahead, dist_aside; 
  double cost, param; 
  double height_diff; 
  double max_height_diff; // the biggest height difference between cells                                                                   
  RowCol cellUnderPos, findPos, deltaPos; 
  CellData cell, findCell; 
  int col_addition = 1, diagonal_addition = 1,row_addition = 1; 
  bool obstacleFound = false, full_cell_found = false; 
  // Evaluating the smaller resolution 
  lowerRes = colRes; 
  if (rowRes < lowerRes) 
    lowerRes = rowRes; 
 
  // Evaluating the step of the line ahead                                                                                                 
  delta_dist = 3*lowerRes/4; 
 
  // Getting all cells under the car                                                                                                       
  dist_ahead = VEHICLE_LENGTH*growVehicleLength + delta_dist; 
  max_height_diff = 0; 
  while( (!obstacleFound) && (dist_ahead >= -delta_dist) ){ 
    // Varying the car width ...                                                                                                           
    dist_aside = -VEHICLE_WIDTH*growVehicleWidth/2; 
    while( (!obstacleFound) && (dist_aside <= VEHICLE_WIDTH*growVehicleWidth/2 + delta_dist) ) { 
 
      // Finding the RowCol of the cell                                                                                                    
      cellUnderPos = FrameRef2RowCol(position,pYaw, dist_ahead, dist_aside); 
      //debug                                                                                                                              
      cell = getCellDataRC(cellUnderPos.first,cellUnderPos.second); 
      // Corridor found !!!   
      if(cell.type == OUTSIDE_CORRIDOR_CELL) 
        obstacleFound = true; 
      if(cell.type == NO_DATA_CELL.type) 
        { 
          height_diff = UNKNOWN_CELL_HEIGHT_DIFF; 
          if(height_diff > max_height_diff){ 
            max_height_diff = height_diff; 
          } 
        } 
      else 
        { 
          full_cell_found = false; 
          for(col_addition = 1; ((col_addition <= NUMBER_OF_COLS_RIGHT_TO_CHECK)&&(full_cell_found == false)); col_addition++) 
            { 
              deltaPos.first = 0; deltaPos.second = col_addition; 
              findPos = RowColFromDeltaRowCol(cellUnderPos, deltaPos); 
              //debug                                                                                                                      
              //cout << "(" << findPos.first << "," << findPos.second << ") ";                                                             
              // Getting the height of the adjacent cell                                                                                   
              findCell = getCellDataRC(findPos.first,findPos.second); 
              if(findCell.type == NO_DATA_CELL.type) 
                { 
                  height_diff = UNKNOWN_CELL_HEIGHT_DIFF; 
                  full_cell_found = false; 
                } 
              else 
                { 
                  height_diff = fabs(cell.value - findCell.value); 
                  full_cell_found = true; 
                } 
              if(height_diff > max_height_diff){ 
                max_height_diff = height_diff; 
              } 
            } 
          full_cell_found = false; 
          for(diagonal_addition = 1; ((diagonal_addition <= NUMBER_OF_CELLS_DIAGONAL_TO_CHECK)&&(full_cell_found == false)); diagonal_addi\
		tion++) 
            { 
              deltaPos.first = diagonal_addition; deltaPos.second = diagonal_addition; 
              findPos = RowColFromDeltaRowCol(cellUnderPos, deltaPos); 
              //debug                                                                                                                      
              //cout << "(" << findPos.first << "," << findPos.second << ") ";                                                             
              // Getting the height of the adjacent cell                                                                                   
              findCell = getCellDataRC(findPos.first,findPos.second); 
              if(findCell.type == NO_DATA_CELL.type) 
                { 
                  height_diff = UNKNOWN_CELL_HEIGHT_DIFF; 
                  full_cell_found = false; 
                } 
              else 
                { 
                  height_diff = fabs(cell.value - findCell.value); 
                  full_cell_found = true; 
                } 
              if(height_diff > max_height_diff){ 
                max_height_diff = height_diff; 
              } 
            } 
          full_cell_found = false; 
          for(row_addition = 1; ((row_addition <= NUMBER_OF_ROWS_DOWN_TO_CHECK)&&(full_cell_found == false)); row_addition++) 
            { 
              deltaPos.first = row_addition; deltaPos.second = 0; 
              findPos = RowColFromDeltaRowCol(cellUnderPos, deltaPos); 
              //debug                                                                                                                      
              //cout << "(" << findPos.first << "," << findPos.second << ") ";                                                             
              // Getting the height of the adjacent cell                                                                                   
              findCell = getCellDataRC(findPos.first,findPos.second); 
              if(findCell.type == NO_DATA_CELL.type) 
                { 
                  height_diff = UNKNOWN_CELL_HEIGHT_DIFF; 
                  full_cell_found = false; 
                } 
              else 
                { 
                  height_diff = fabs(cell.value - findCell.value); 
                  full_cell_found = true; 
                } 
              if(height_diff > max_height_diff){ 
                max_height_diff = height_diff; 
              }//end max_height_diff_check                                                                                                 
            }//end for loop through rows                                                                                                   
        }//end else for cell not empty                                                                                                     
      if(max_height_diff > heightDiffThreshold){ 
        obstacleFound = true; 
        //debug                                                                                                                            
        //cout << "Found obstacle\n";                                                                                                      
        //cout << "(" << cellUnderPos.first << "," << cellUnderPos.second << ") = " << cell.value << endl;                                 
        //cout << "(" << findPos.second << "," << findPos.second << ") = " << findCell.value << endl;                                      
      } 
 
      // debug obstacleAvoidance                                                                                                           
      //(theMap[cellUnderPos]).color = BLUE_INDEX;//Is this still in use?                                                                  
 
 
      dist_aside += delta_dist; 
    } 
    dist_ahead -= delta_dist; 
  } 
  // Cost ...                                                                                                                              
  // if there is a high height difference, don't go there                                                                                  
  param = 1 - max_height_diff/heightDiffThreshold; 
  if( (obstacleFound) || (param < 0) ){ 
    cost = 0; 
  } 
  else{ 
    cost = costShape(param,EXP_PARAM); 
  } 
 
  return(cost); 
} 
 
// Evaluate each the cells the car would be if pointing to pYaw rad and on a certain row/col on the map                                    
/* double maxHeightDiffCost(RowCol pos,double pYaw);                                                                                       
 * +DESCRIPTION: This method receive the position and yaw of the car and returns a cost based on height                                    
 *                                                                                                                                         
 * +PARAMETERS                                                                                                                             
 *   - RowCol position: row and col of the orign of the car                                                                                
 *   - double pYaw: heading the car is pointing                                                                                            
 * +RETURN: a cost [0 100]                                                                                                                 
 */ 
double genMap::maxHeightDiffCost(RowCol position,double pYaw) 
{ 
  double lowerRes, delta_dist, dist_ahead, dist_aside; 
  double cost, param; 
  double min_height, max_height; 
  double height_diff,max_height_diff; // the biggest height difference between cells                                                       
  RowCol cellUnderPos, findPos, deltaPos; 
  CellData cell, findCell; 
  bool obstacleFound = false; 
  bool invalidDataCell = false; 
  int numValidCellsFound = 0; 
 
  // Evaluating the smaller resolution                                                                                                     
  lowerRes = colRes; 
  if (rowRes < lowerRes) 
    lowerRes = rowRes; 
 
  // Evaluating the step of the line ahead                                                                                                 
  delta_dist = lowerRes; 
 
  // Getting all cells under the car                                                                                                       
  dist_ahead = VEHICLE_LENGTH*growVehicleLength; 
  max_height_diff = 0; 
  max_height = -9999; 
  min_height = 9999; 
  while( (!obstacleFound) && (dist_ahead >= 0) ){ 
    // Varying the car width ...                                                                                                           
    dist_aside = -VEHICLE_WIDTH*growVehicleWidth/2; 
    while( (!obstacleFound) && (dist_aside <= VEHICLE_WIDTH*growVehicleWidth/2) ) { 
 
      // Finding the RowCol of the cell                                                                                                    
      cellUnderPos = FrameRef2RowCol(position,pYaw, dist_ahead, dist_aside); 
      cell = getCellDataRC(cellUnderPos.first,cellUnderPos.second); 
 
      // Corridor found !!!                                                                                                                
      if(cell.type == OUTSIDE_CORRIDOR_CELL){ 
        obstacleFound = true; 
      } 
      invalidDataCell = false; 
      // activateNoDataCell will be true if we have to treat it differently                                                                
      if((activateNoDataCell) && (cell.type != NO_DATA_CELL_TYPE) ){ 
        invalidDataCell = true; 
      } 
      if(!invalidDataCell){ 
        if(numValidCellsFound == 0) { 
          min_height = cell.value; 
          max_height = cell.value; 
        } else { 
          if(cell.value < min_height) 
            min_height = cell.value; 
          if(cell.value > max_height) 
            max_height = cell.value; 
          height_diff = max_height - min_height; 
          if(height_diff > max_height_diff) max_height_diff = height_diff; 
        } 
        numValidCellsFound++; 
      } 
      if((max_height_diff > heightDiffThreshold)){ 
        //cout << "Found obstacle of height " << max_height_diff << endl;                                                                  
        obstacleFound = true; 
      } 
      dist_aside += delta_dist; 
 
      // debug obstacleAvoidance                                                                                                           
      //(theMap[cellUnderPos]).color = GREEN_INDEX;//Is this still in use?                                                                 
    } 
    dist_ahead -= delta_dist; 
  } 
  // Cost ...                                                                                                                              
  // if there is a high height difference, don't go there                                                                                  
  if( obstacleFound ){ 
    cost = 0; 
  } 
  else{ 
    //  param = 1 - max_height_diff/heightDiffThreshold;                                                                                       
    //  cost = costShape(param,EXP_PARAM);                                                                                                     
 
    param = max_height_diff/heightDiffThreshold; // Mike Thielman                                                                          
    cost = costShape(param,EXP_PARAM); 
    cost = MAX_VOTE_RANGE - cost; // Mike Thielman                                                                                         
  } 
  //cout << "max_height_diff: " << max_height_diff << "  cost: " << cost << endl;                                                          
  return(cost); 
} 
 
// Evaluate each the cells the car would be if pointing to pYaw rad and on a certain row/col on the map                                    
/* double maxHeightDiffCostRectangle(RowCol pos,double pYaw);                                                                              
 * +DESCRIPTION: This method receive the position and yaw of the car and returns a cost based on height                                    
 *                                                                                                                                         
 * +PARAMETERS                                                                                                                             
 *   - RowCol position: row and col of the orign of the car                                                                                
 *   - double pYaw: heading the car is pointing                                                                                            
 * +RETURN: a cost [0 100]                                                                                                                 
 */ 
double genMap::maxHeightDiffCostRectangle(RowCol position,double pYaw) 
{ 
  int i,j,ind; 
  double lowerRes; 
  double delta_dist, north_delta_dist; 
  double north_dist, east_dist; 
  double dist_ahead, dist_aside; 
  double cost, param; 
  double min_height, max_height; 
  double height_diff,max_height_diff; // the biggest height difference between cells                                                       
  CellData cell; 
  RowCol pos, delta; 
  int a,d; 
  pair<double,double> NE_aux, NE_center, NE_a, NE_b, NE_c, NE_d, NE_lower, NE_upper; 
  vector< pair<double,double> > corners; 
  double ab_slope, ac_slope, lower_slope, upper_slope; 
  int change_slope; 
  bool alignedRectangle=false; 
  bool obstacleFound = false; 
  bool invalidDataCell = false,firstTime; 
  int numValidCellsFound = 0; 
  map<RowCol,CellData>::iterator mapIt; 
 
  // Evaluating the smaller resolution                                                                                                     
  lowerRes = colRes; 
  if (rowRes < lowerRes) 
    lowerRes = rowRes; 
  delta_dist = lowerRes; 
 
  // Finding the UTM coordinates the the 4 points that determine the rectangle                                                             
  i=0;ind=0; 
  NE_center = RowCol2UTM(position); 
  corners.reserve(4); 
  firstTime=true; 
  while(i <= 1){  // ahead                                                                                                                 
    j = -1; 
    dist_ahead = ((double)i)*((double)VEHICLE_LENGTH)*growVehicleLength; 
    while(j <= 1){ // aside                                                                                                                
      dist_aside = ((double)j)*((double)VEHICLE_WIDTH)*growVehicleWidth/2; 
      north_dist = dist_ahead*cos(pYaw) - dist_aside*sin(pYaw); 
      east_dist = dist_ahead*sin(pYaw) + dist_aside*cos(pYaw); 
      NE_aux.first = NE_center.first + north_dist; 
      NE_aux.second = NE_center.second + east_dist; 
      corners.push_back(NE_aux); 
      // Keeping track of the lowest Northing corner                                                                                       
      if(firstTime){ 
        NE_a = NE_aux; 
        NE_d = NE_aux; 
        a = ind; 
        d = ind; 
        firstTime = false; 
      } 
      else{ 
        if(NE_aux.first < NE_a.first){ 
          NE_a = NE_aux; 
          a = ind; 
        } 
        if(NE_aux.first > NE_d.first){ 
          NE_d = NE_aux; 
          d = ind; 
        } 
      } 
      j+=2; 
      ind++; 
    } 
    i++; 
  } 
 
  // Find if the rectangle is aligned to the NorthEast axis and the sides                                                                  
  alignedRectangle = false;
  if( (abs(corners[0].first - corners[1].first) < DELTA_SMALL) ||
      (abs(corners[0].first - corners[2].first) < DELTA_SMALL) ||
      (abs(corners[0].first - corners[3].first) < DELTA_SMALL)
      ) { 
    alignedRectangle = true;
  } 
 
  // Putting corner "a" also as lowest easting, just in case it is aligned                                                                 
  if(alignedRectangle){ 
    for(ind=0;ind<4;ind++){
      if( (abs(corners[ind].first-NE_a.first) < DELTA_SMALL) && (corners[ind].second < NE_a.second) ){
        NE_a = corners[ind];
        a = ind;
      } 
      if( (abs(corners[ind].first-NE_d.first) < DELTA_SMALL) && (corners[ind].second > NE_d.second) ){
        NE_d = corners[ind];
        d = ind;
      }
    } 
  } 
 
  // Finding the UTM of the corners next to NE_a                                                                                           
  firstTime = true; 
  for(ind=0;ind<4;ind++){ 
    if( (ind != a) && (ind != d) ){ 
      if(firstTime){ 
        NE_b = corners[ind]; 
        NE_c = corners[ind]; 
        firstTime = false; 
      } 
      else{ 
        if(corners[ind].second > NE_b.second) 
          NE_b = corners[ind]; 
        if(corners[ind].second < NE_c.second) 
          NE_c = corners[ind]; 
      } 
    } 
  } 
 
  // debug                                                                                                                                 
  /*                                                                                                                                       
  cout << "(a,d) = (" << a << "," << d << ")" << endl;                                                                                     
  cout << "a=(" << NE_a.first << "," << NE_a.second << ")" << endl;                                                                        
  cout << "b=(" << NE_b.first << "," << NE_b.second << ")" << endl;                                                                        
  cout << "c=(" << NE_c.first << "," << NE_c.second << ")" << endl;                                                                        
  cout << "d=(" << NE_d.first << "," << NE_d.second << ")" << endl << endl;                                                                
  getchar();                                                                                                                               
  */ 
 
  max_height_diff = 0;
  max_height = -9999;
  min_height = 9999; 
  if(alignedRectangle){ 
    ab_slope = 0; 
    ac_slope = 0; 
    lower_slope = 0;
    upper_slope = 0;
 
 
    NE_lower = NE_a; 
    NE_upper = NE_d; 
    NE_aux   = NE_lower; 
  } 
  else{ 
    ab_slope = (NE_b.first-NE_a.first)/(NE_b.second-NE_a.second); 
    ac_slope = (NE_c.first-NE_a.first)/(NE_c.second-NE_a.second); 
    lower_slope = ac_slope; 
    upper_slope = ab_slope; 
    NE_lower = NE_a; 
    NE_upper = NE_a; 
    NE_aux   = NE_lower; 
  } 
 
  // Finding the cells inside the rectangle                                                                                                
  while( (!obstacleFound) && (NE_aux.first <= NE_upper.first) ){ 
    while( (!obstacleFound) && (NE_aux.second <= NE_upper.second ) ) { 
 
      pos.first    = leftBottomRow; 
      delta.first  = (int)round((NE_aux.first - leftBottomUTM_Northing)/rowRes);
      pos.second   = leftBottomCol; 
      delta.second = (int)round((NE_aux.second - leftBottomUTM_Easting)/colRes); 
      pos = RowColFromDeltaRowCol(pos,delta); 
 
      // debug                                                                                                                             
      //cell.value = 2.5;                                                                                                                  
      //setCellDataRC(pos.first, pos.second,cell);                                                                                         
 
      mapIt = theMap.find(pos); 
      if ( mapIt == theMap.end() ) { 
        cell = NO_DATA_CELL; 
      } 
      else{ 
        cell = mapIt->second; 
      } 
      cell.value = shrinkObstacle(cell.value,(curTimeStamp - cell.timeStamp)); 
 
      // Corridor found !!!                                                                                                                
      if(cell.type == OUTSIDE_CORRIDOR_CELL){ 
        obstacleFound = true; 
      } 
      invalidDataCell = false; 
      // activateNoDataCell will be true if we have to treat it differently                                                                
      if((activateNoDataCell) && (cell.type != NO_DATA_CELL_TYPE) ){ 
        invalidDataCell = true; 
      } 
      if(!invalidDataCell){ 
        if(numValidCellsFound == 0) { 
          min_height = cell.value; 
          max_height = cell.value; 
        } else { 
          if(cell.value < min_height) 
            min_height = cell.value; 
          if(cell.value > max_height) 
            max_height = cell.value; 
          height_diff = max_height - min_height; 
          if(height_diff > max_height_diff) max_height_diff = height_diff; 
        } 
        numValidCellsFound++; 
      } 
      if((max_height_diff > heightDiffThreshold)){ 
        obstacleFound = true; 
      } 
 
      // Limiting not to evaluate outside the rectangle                                                                                    
      if( (NE_aux.second < NE_upper.second) && ((NE_aux.second+delta_dist) > NE_upper.second) ) 
        NE_aux.second = NE_upper.second; 
      else 
        NE_aux.second += delta_dist; 
    } 
 
    // debug                                                                                                                               
    //cout << "NE_aux=(" << NE_aux.first << "," << NE_aux.second << ")" << endl;                                                           
    //getchar();                                                                                                                           
 
    if(alignedRectangle){ 
      if( (NE_lower.first < NE_d.first) && ((NE_lower.first+delta_dist) > NE_d.first) ){ 
        NE_lower.first = NE_d.first; 
      } 
      else{ 
        NE_lower.first += delta_dist; 
      } 
    } 
    else{ 
      // When we have pass the corner                                                                                                      
      if( (NE_lower.first < NE_d.first) && ((NE_lower.first+delta_dist) > NE_d.first) ){ 
        break; 
      } 
      else{ 
        change_slope = 0; 
        if( (NE_lower.first < NE_c.first) && ((NE_lower.first+delta_dist) > NE_c.first) ) 
          change_slope += 1; 
        if( (NE_upper.first < NE_b.first) && ((NE_upper.first+delta_dist) > NE_b.first) ) 
          change_slope += 2; 
 
        switch(change_slope){ 
        case 0: // no changes                                                                                                              
          NE_lower.first += delta_dist; 
          NE_lower.second += delta_dist/lower_slope; 
          NE_upper.first += delta_dist; 
          NE_upper.second += delta_dist/upper_slope; 
          break; 
 
        case 1: // lower_slope has to change                                                                                               
          north_delta_dist = (NE_c.first-NE_lower.first); 
          lower_slope = ab_slope; 
          NE_lower = NE_c; 
          NE_upper.first += north_delta_dist; 
          NE_upper.second += north_delta_dist/upper_slope; 
          break; 
 
        case 2: // upper_slope has to change                                                                                               
          north_delta_dist = (NE_b.first-NE_upper.first); 
          upper_slope = ac_slope; 
          NE_upper = NE_b; 
          NE_lower.first += north_delta_dist; 
          NE_lower.second += north_delta_dist/lower_slope; 
          break; 
 
        case 3: // both have to change                                                                                                     
          // LOWER                                                                                                                         
          lower_slope = ab_slope; 
          north_delta_dist = delta_dist - (NE_c.first-NE_lower.first); 
          NE_lower = NE_c; 
          NE_lower.first += north_delta_dist; 
          NE_lower.second += north_delta_dist/lower_slope; 
          // UPPER                                                                                                                         
          upper_slope = ac_slope; 
          north_delta_dist = delta_dist - (NE_b.first-NE_upper.first); 
          NE_upper = NE_b; 
          NE_upper.first += north_delta_dist; 
          NE_upper.second += north_delta_dist/upper_slope; 
          break; 
        } 
      } // end if ( (NE_lower.first < NE.d.first) && ((NE.lower.first+delta_dist) > NE_d.first) ){                                         
    } // end else (alignedRectangle)                                                                                                       
    NE_aux = NE_lower; 
  } 
 
  // Cost ...                                                                                                                              
  // if there is a high height difference, don't go there                                                                                  
  if( obstacleFound ){ 
    cost = 0; 
  } 
  else{ 
    param = max_height_diff/heightDiffThreshold; 
    cost = costShape(param,EXP_PARAM); 
    cost = MAX_VOTE_RANGE - cost; 
  } 
  return(cost); 
} 
 
// Evaluate each the cells the car would be if pointing to pYaw rad and on a certain row/col on the map                                    
/* double maxInclinationCost(RowCol pos,double pYaw);                                                                                      
 * +DESCRIPTION: This method receive the position and yaw of the car and returns a cost based on cos of the max and min cell               
 *                                                                                                                                         
 * +PARAMETERS                                                                                                                             
 *   - RowCol position: row and col of the orign of the car                                                                                
 *   - double pYaw: heading the car is pointing                                                                                            
 * +RETURN: a cost [0 100]                                                                                                                 
 */ 
double genMap::maxInclinationCost(RowCol position,double pYaw) 
{ 
  double lowerRes, delta_dist, dist_ahead, dist_aside; 
  double cost, param; 
  double min_height, max_height; 
  double cosineCost; 
  double max_height_diff; // the biggest height difference between cells                                                                   
  RowCol cellUnderPos, findPos, deltaPos; 
  RowCol maxCellPos, minCellPos; 
  NorthEastDown a,b,c, groundVector, upVector; // to evaluate the cost                                                                     
  pair<double,double> NorthEast; 
  CellData cell, findCell; 
  bool invalidDataCell = false; 
  bool obstacleFound = false; 
 
  // Evaluating the smaller resolution                                                                                                     
  lowerRes = colRes; 
  if (rowRes < lowerRes) 
    lowerRes = rowRes; 
 
  // Evaluating the step of the line ahead                                                                                                 
  delta_dist = lowerRes; 
 
  // Getting all cells under the car                                                                                                       
  dist_ahead = VEHICLE_LENGTH*growVehicleLength; 
  max_height_diff = 0; 
  max_height = -9999; 
  min_height = 9999; 
  cosineCost = 1; 
  while( (!obstacleFound) && (dist_ahead >= 0) ){ 
    // Varying the car width ...                                                                                                           
    dist_aside = -VEHICLE_WIDTH*growVehicleWidth/2; 
    while( (!obstacleFound) && (dist_aside <= VEHICLE_WIDTH*growVehicleWidth/2) ) { 
 
      // Finding the RowCol of the cell                                                                                                    
      cellUnderPos = FrameRef2RowCol(position,pYaw, dist_ahead, dist_aside); 
      cell = getCellDataRC(cellUnderPos.first,cellUnderPos.second); 
      // Corridor found !!!                                                                                                                
      if(cell.type == OUTSIDE_CORRIDOR_CELL) 
        obstacleFound = true; 
 
      invalidDataCell = false; 
      // activateNoDataCell will be true if we have to treat it differently                                                                
      if((activateNoDataCell) && (cell.type != NO_DATA_CELL_TYPE) ){ 
        invalidDataCell = true; 
      } 
      if(!invalidDataCell){ 
        if(cell.value < min_height){ 
          min_height = cell.value; 
          minCellPos = cellUnderPos; 
        } 
        if(cell.value > max_height){ 
          max_height = cell.value; 
          maxCellPos = cellUnderPos; 
        } 
        max_height_diff = max_height - min_height; 
 
        // Evaluating the 3D points                                                                                                        
        NorthEast = RowCol2UTM(minCellPos); 
        a.Northing = NorthEast.first; 
        a.Easting = NorthEast.second; 
        a.Downing = 0; 
        NorthEast = RowCol2UTM(maxCellPos); 
        b.Northing = NorthEast.first; 
        b.Easting = NorthEast.second; 
        b.Downing = 0; 
        c.Northing = NorthEast.first; 
        c.Easting = NorthEast.second; 
        c.Downing = max_height_diff; 
        // Evaluating the vectors                                                                                                          
        groundVector = b-a; 
        upVector = c-a; 
        groundVector.norm(); 
        upVector.norm(); 
        cosineCost = groundVector*upVector; 
 
      } 
      if(cosineCost < vectorProdThreshold){ 
        obstacleFound = true; 
      } 
      dist_aside += delta_dist; 
 
      // debug obstacleAvoidance                                                                                                           
      //(theMap[cellUnderPos]).color = BLUE_INDEX;//Is this still in use?                                                                  
    } 
    dist_ahead -= delta_dist; 
  } 
  // Cost ...                                                                                                                              
  // if there is a high height difference, don't go there                                                                                  
  param = cosineCost; 
  if( (obstacleFound) || (param < vectorProdThreshold) ){ 
    cost = 0; 
  } 
  else{ 
    cost = costShape(param,EXP_PARAM); 
  } 
 
  return(cost); 
} 
 

// Path Evaluation Methods v 2.0                                                                                                           
/* double obstacleAvoidance(double steer)                                                                                                  
 * +DESCRIPTION: This method receive a steering wheel angle and, based on the                                                              
 *    path the car follows on that steering angle, evaluates the distance the                                                              
 *    car can sweep until reaches a dangerous obstacle                                                                                     
 * +PARAMETERS                                                                                                                             
 *   - steer: steering angle position in radians. Right is positive and left                                                               
 *            is negative                                                                                                                  
 * +RETURN: distance in meters                                                                                                             
 */ 
//Lars Cremean's idea to have it evaluate a whole array of angles at once could save some time.                                         
pair<double,bool> genMap::obstacleAvoidance(double steer) 
{ 
  VBClock clk; 
  CellData cell; 
  int numCriteria=0; 
  double distance, cost; 
  double yaw, steerAngle; 
  double higherRes, delta_radius, PathRadius; 
  double delta_dist, dist, delta_ahead, delta_aside, max_dist, last_dist, first_delta_ahead; 
  double stepRes=1; 
  double max_phi, phi, delta_phi, last_phi; 
  int steerSgn, indTire, i,j; 
  bool obstacle=false; 
  bool noDataCell=false; 
  bool invalidDataCell = false; 
  map<RowCol,RowCol> cellsUnder; 
  pair<double,double> NorthEast; 
  RowCol carPos, oldCarPos, tirePos, curPos; 
  NorthEastDown tire[4];         // position vectors of the tires                                                                          
  NorthEastDown normal[4]; // normal vectors to the planes determined by three tires                                                       
  double offTire[4];        // distance between the plane and the 4th tire                                                                 
  double normalCost, offTireCost, underCellsCost, max_HeightDiffCost, max_InclinationCost, max_HeightDiffCostRectangle; 
  pair <double,bool> returnVal; // .first is dist .second is if obstacle                                                                   
 
  // Getting the heading                                                                                                                   
  yaw = curState.Yaw; 
 
  // Storing the sign of the steering wheel and getting the abs value                                                                      
  if(steer >= 0) 
    steerSgn = +1; 
  else 
    steerSgn = -1; 
  steerAngle = steerSgn*steer; 
 
  // vehRow and vehCol is about the middle of the rear axle                                                                                
  oldCarPos.first = vehRow; 
  oldCarPos.second = vehCol; 
  first_delta_ahead = curState.Speed*MAX_REACTION_TIME_BETWEEN_SENSOR_SWEEP_AND_BEGINNING_DECELLERATION_FOR_EVALUATING_VELOCITY/2; 
  carPos = FrameRef2RowCol(oldCarPos,curState.Yaw,first_delta_ahead,0); 
 
  // Evaluating the smaller resolution                                                                                                     
  higherRes = colRes; 
  if (rowRes > higherRes) 
    higherRes = rowRes;
 
  // Step Resolution                                                                                                                       
  stepRes = 1; 
 
  // GOING STRAIGHT !!                                                                                                                     
  obstacle = false; 
  distance = 0; 
  if(!steerAngle){ 
    // Evaluating the max distance to consider ahead                                                                                       
    max_dist = pathMaxDistance-first_delta_ahead; 
    // Step                                                                                                                                
    delta_dist = higherRes; 
    //delta_dist = max_dist/10;                                                                                                            
 
    // For each distance ahead ...                                                                                                         
    dist = 0; 
    while ( (!obstacle) && (dist < max_dist) ) { 
      last_dist = dist; 
      if(stepRes*delta_dist > VEHICLE_AXLE_DISTANCE/2) 
        dist += VEHICLE_AXLE_DISTANCE/2; 
      else 
        dist += stepRes*delta_dist; 
 
      if( (last_dist < max_dist) && (dist > max_dist) ){ 
        dist = max_dist; 
      } 
      stepRes += 1; 
 
      // Distance ahead of the car in a frame reference                                                                                    
      delta_ahead = dist; 
      delta_aside = 0; 
 
      curPos = FrameRef2RowCol(carPos,yaw, delta_ahead, delta_aside); 
 
      // Evaluating the height and cost                                                                                                    
      if(useCellsUnderCost){ 
        underCellsCost = allCellsUnderCost(curPos,yaw); 
      } 
      if(useMaxHeightDiffCost){ 
        max_HeightDiffCost = maxHeightDiffCost(curPos,yaw); 
      } 
      if(useMaxHeightDiffCostRectangle){ 
        max_HeightDiffCostRectangle = maxHeightDiffCostRectangle(curPos,yaw); 
      } 
      if(useMaxInclinationCost){ 
        max_InclinationCost = maxInclinationCost(curPos,yaw); 
      } 
 
      // Evaluating the tires position                                                                                                     
      if((useOffTireCost) || (useDotVectorCost)){ 
        indTire = 0; 
        noDataCell = false; 
        i=0; 
        while( (!noDataCell) && (i <= 1) ){  // ahead                                                                                      
          j = -1; 
          while( (!noDataCell) && (j <= 1) ){ // aside                                                                                     
            tirePos = FrameRef2RowCol(curPos, yaw, i * VEHICLE_AXLE_DISTANCE, 
				      j * VEHICLE_AVERAGE_TRACK / 2); 
            cell = getCellDataRC(tirePos.first,tirePos.second); 
            invalidDataCell = false; 
            if((activateNoDataCell) && (cell.type == NO_DATA_CELL_TYPE) ){ 
              invalidDataCell = true; 
            } 
            if(!invalidDataCell){ 
              noDataCell = true; 
              break; 
            } 
            else{ 
              NorthEast = RowCol2UTM(tirePos); 
              tire[indTire].Northing = NorthEast.first; 
              tire[indTire].Easting = NorthEast.second; 
              tire[indTire].Downing = cell.value; 
              indTire++; 
            } 
            j+=2; 
          } 
          i++; 
        } 
      } 
 
      // Evaluating the normal vectors and cost                                                                                            
      if(useDotVectorCost){ 
        if(!noDataCell){
          normal[0]   = evalNormalVector(tire[1], tire[2], tire[3]);
          normal[1]   = evalNormalVector(tire[0], tire[2], tire[3]);
          normal[2]   = evalNormalVector(tire[0], tire[1], tire[2]);
          normal[3]   = evalNormalVector(tire[0], tire[1], tire[2]);
          normalCost = evalNormalVectorCost(normal);
        } 
        else{ 
          normalCost = obstacleThreshold; 
        } 
      } 
 
      // Evaluating the tires off the plane determined by the other three,                                                                 
      // and cost                                                                                                                          
      if(useOffTireCost){
        if(!noDataCell){
          offTire[0]  =  distancePoint2Plane(tire[1], tire[2], tire[3], tire[0]);
          offTire[1]  =  distancePoint2Plane(tire[0], tire[2], tire[3], tire[1]);
          offTire[2]  =  distancePoint2Plane(tire[0], tire[1], tire[3], tire[2]); 
          offTire[3]  =  distancePoint2Plane(tire[0], tire[1], tire[2], tire[3]); 
          offTireCost = evalOffTireCost(offTire); 
        } 
        else{ 
          offTireCost = obstacleThreshold; 
        } 
      } 
      // debug                                                                                                                             
      //system("clear");                                                                                                                   
      //(theMap[curPos]).color = RED_INDEX;                                                                                                
      //display();                                                                                                                         
      //clearColor(BLUE_INDEX);//Is this still in use?                                                                                     
      //cout << "MAX_DIST=" << max_dist << "  DIST=" << dist << endl;                                                                      
      //cout << "PRESS ... " << endl;                                                                                                      
      //fflush(stdin);                                                                                                                     
      //getchar();                                                                                                                         
 
      // Averaging                                                                                                                         
      cost = 0; 
      numCriteria = 0; 
      if(useCellsUnderCost){ 
        cost += underCellsCost; 
        numCriteria++; 
      } 
      if(useMaxHeightDiffCost){ 
        cost += max_HeightDiffCost; 
        numCriteria++; 
      } 
      if(useMaxHeightDiffCostRectangle){ 
        cost += max_HeightDiffCostRectangle; 
        numCriteria++; 
      } 
      if(useMaxInclinationCost){ 
        cost += max_InclinationCost; 
        numCriteria++; 
      } 
      if(useDotVectorCost){ 
        cost += normalCost; 
        numCriteria++; 
      } 
      if(useOffTireCost){ 
        cost += offTireCost; 
        numCriteria++; 
      } 
      if(numCriteria){ 
        cost = cost/((double)numCriteria);
      } 
      else{
        cost = obstacleThreshold; 
      } 
      // Deciding if this is or not an obstacle                                                                                            
      if(cost < obstacleThreshold){
        obstacle = true; 
        distance = dist - delta_dist;
      } 
    } 
  } 
  // FOLLOWING A CURVE!!                                                                                                                   
  else { 
 
    // considering the radius of the rear tire                                                                                             
    PathRadius = abs(VEHICLE_AXLE_DISTANCE/(tan(steerAngle)));
    // Evaluating the max_phi, the maximum angle the path goes                                                                             
    max_phi = (pathMaxDistance-first_delta_ahead)/PathRadius;
    if(max_phi > MAX_PHI) {
      // we don't want to keep making circles and alnalysing the same path a                                                               
      // lot of times !!                                                                                                                   
      max_phi = MAX_PHI;
    } 
 
    // Step                                                                                                                                
    delta_radius = higherRes; 
    delta_phi = delta_radius/(PathRadius);
    //delta_phi = max_phi/10;                                                                                                              
 
    // For each phi ...                                                                                                                    
    phi = 0;
    while ( (!obstacle) && (phi < max_phi) ) { 
      last_phi = phi;
      if(stepRes*delta_phi*PathRadius > VEHICLE_AXLE_DISTANCE/2) 
        phi += VEHICLE_AXLE_DISTANCE/(2*PathRadius); 
      else 
        phi += stepRes*delta_phi; 
 
      if( (last_phi < max_phi) && (phi > max_phi) ) { 
        phi = max_phi; 
      } 
      stepRes += 1; 
      // DEBUG                                                                                                                             
      //cout << "genMap::obstacleAvoidance: \"for each phi...\" while loop " << steerAngle << ", phi = " << phi << ", max_phi = " << max_p\
	hi<< ", delta_phi = " << delta_phi<< endl;                                                                                                 
 
	// Evaluating displacement ahead and aside                                                                                           
	delta_ahead = PathRadius*sin(phi); 
	delta_aside = steerSgn*PathRadius*(1 - cos(phi)); 
	//cout << "steerAngle = " << steerAngle << endl;                                                                                     
	//cout << "delta_ahead = " << delta_ahead << " delta_aside = " << delta_aside << endl;                                               
	yaw = curState.Yaw + steerSgn*phi; 
	// Finding the RowCol of the cell                                                                                                          //cout << "old pos = (" << curPos.first << "," << curPos.second << ")" << endl;                                                      
	curPos = FrameRef2RowCol(carPos,curState.Yaw, delta_ahead, delta_aside); 
	//cout << "new pos = (" << curPos.first << "," << curPos.second << ")" << endl;                                                      
	//cout << "max path = " <<pathMaxDistance-first_delta_ahead << " delta_radius = " << delta_phi*PathRadius << endl;                   
	//getchar();                                                                                                                         
 
	// Evaluating the height and cost                                                                                                    
	if(useCellsUnderCost){ 
	  underCellsCost = allCellsUnderCost(curPos,yaw); 
	} 
	if(useMaxHeightDiffCost){ 
	  max_HeightDiffCost = maxHeightDiffCost(curPos,yaw); 
	} 
	if(useMaxHeightDiffCostRectangle){ 
	  max_HeightDiffCostRectangle = maxHeightDiffCostRectangle(curPos,yaw); 
	} 
	if(useMaxInclinationCost){ 
	  max_InclinationCost = maxInclinationCost(curPos,yaw); 
	} 
 
	// Evaluating the tires position                                                                                                     
	if((useOffTireCost) || (useDotVectorCost)){ 
	  indTire = 0; 
	  noDataCell = false; 
	  i=0; 
	  while( (!noDataCell) && (i <= 1) ){  // ahead                                                                                      
	    j = -1; 
	    while( (!noDataCell) && (j <= 1) ){ // aside                                                                                     
	      tirePos = FrameRef2RowCol(curPos, yaw, i * VEHICLE_AXLE_DISTANCE, 
					j * VEHICLE_AVERAGE_TRACK / 2); 
	      cell = getCellDataRC(tirePos.first,tirePos.second); 
	      if(cell.type == NO_DATA_CELL_TYPE){ 
		noDataCell = true; 
		break; 
	      } 
	      else{ 
		NorthEast = RowCol2UTM(tirePos); 
		tire[indTire].Northing = NorthEast.first; 
		tire[indTire].Easting = NorthEast.second; 
		tire[indTire].Downing = cell.value; 
		indTire++; 
	      } 
	      j+=2; 
	    } 
	    i++; 
	  } 
	} 
 
	// Evaluating the normal vectors and cost                                                                                            
	if(useDotVectorCost){ 
	  if(!noDataCell){ 
	    normal[0]   = evalNormalVector(tire[1], tire[2], tire[3]); 
	    normal[1]   = evalNormalVector(tire[0], tire[2], tire[3]); 
	    normal[2]   = evalNormalVector(tire[0], tire[1], tire[2]); 
	    normal[3]   = evalNormalVector(tire[0], tire[1], tire[2]); 
	    normalCost = evalNormalVectorCost(normal); 
	  } 
	  else{ 
	    normalCost = obstacleThreshold; 
	  } 
	} 
 
	// Evaluating the tires off the plane determined by the other three,                                                                 
	// and cost                                                                                                                          
	if(useOffTireCost){
	  if(!noDataCell){
	    offTire[0]  =  distancePoint2Plane(tire[1], tire[2], tire[3], tire[0]);
	    offTire[1]  =  distancePoint2Plane(tire[0], tire[2], tire[3], tire[1]);
	    offTire[2]  =  distancePoint2Plane(tire[0], tire[1], tire[3], tire[2]); 
	    offTire[3]  =  distancePoint2Plane(tire[0], tire[1], tire[2], tire[3]);
	    offTireCost = evalOffTireCost(offTire);
	  } 
	  else{ 
	    offTireCost = obstacleThreshold; 
	  } 
	} 
 
	// debug                                                                                                                             
	//system("clear");                                                                                                                         //(theMap[curPos]).color = RED_INDEX;                                                                                                
	//display();                                                                                                                         
	//clearColor(BLUE_INDEX);//Is anyone using this?                                                                                     
	//cout << "MAX_PHI=" << max_phi*180/M_PI << "  PHI=" << phi*180/M_PI << " DELTA_PHI" << delta_phi*180/M_PI<< "  RADIUS=" << PathRadi\
			    us << endl;                                                                                                                                
			    //cout << "PRESS ... " << endl;                                                                                                      
			    //fflush(stdin);                                                                                                                     
			    //getchar();                                                                                                                         
 
			    // Averaging                                                                                                                         
			    cost = 0; 
			    numCriteria = 0; 
			    if(useCellsUnderCost){ 
			      cost += underCellsCost; 
			      numCriteria++; 
			    } 
			    if(useMaxHeightDiffCost){ 
			      cost += max_HeightDiffCost; 
			      numCriteria++; 
			    } 
			    if(useMaxInclinationCost){ 
			      cost += max_InclinationCost; 
			      numCriteria++; 
			    } 
			    if(useMaxHeightDiffCostRectangle){ 
			      cost += max_HeightDiffCostRectangle; 
			      numCriteria++; 
			    } 
			    if(useDotVectorCost){ 
			      cost += normalCost; 
			      numCriteria++; 
			    } 
			    if(useOffTireCost){ 
			      cost += offTireCost; 
			      numCriteria++; 
			    } 
			    if(numCriteria){ 
			      cost = cost/((double)numCriteria); 
			    } 
			    else{
			      cost = obstacleThreshold;
			    } 
			    // Deciding if this is or not an obstacle                                                                                            
			    if(cost < obstacleThreshold){
			      obstacle = true; 
			      distance = (phi - delta_phi)*PathRadius;
			    } 
    } 
  } 
 
  returnVal.second = obstacle;
 
  // If there is no obstacles                                                                                                              
  if(!obstacle){ 
    // Following a curve sometimes it's not possible to sweep the hole pathMaxDistance                                                     
    if( (steer) && (max_phi == MAX_PHI) )
      distance = MAX_PHI*PathRadius;
    else 
      distance = pathMaxDistance;
  } 
  returnVal.first = distance; 
  return(returnVal); 
} 
 
 
// Path Evaluation Methods v 1.0                                                                                                           
/* double evalPathCost(double steer)                                                                                                       
 * +DESCRIPTION: This method receive a steering wheel angle and, based on the                                                              
 *    path the car follows on that steering angle, evaluates the path goodness                                                             
 *    based on difference of height.                                                                                                       
 * +PARAMETERS                                                                                                                             
 *   - steer: steering angle position in radians. Right is positive and left                                                               
 *            is negative                                                                                                                  
 * +RETURN: cost with no normalization                                                                                                     
 */ 
double genMap::evalPathCost(double steer) 
{ 
  double PathRadius;  // Radius of a path                                                                                                  
  double yaw;         // Yaw from the current state                                                                                        
  double north_aux, east_aux; 
  double cost;        //cost to be returned                                                                                                
 
  double delta_ahead;  // aligned to the heading of the car                                                                                
  double delta_aside;  // aligned to the side of the car, towards right                                                                    
 
  int row, col, rowInd, colInd; 
  RowCol gpsPos;      // GPS location                                                                                                      
  RowCol carPos;      // Mid front car location                                                                                            
  RowCol circCenter;  // Center of the circunference                                                                                       
  RowCol obstaclePos; // Location of a obstacle in the matrix                                                                              
 
  // Variables to evaluate cost                                                                                                            
  CellData findCell, currCell; 
  RowCol findPos, currPos, deltaPos; 
  double heightDiff, max_heightDiff; 
  double sum_max_heightDiff, numCells; 
 
  int steerSgn;       // sgn of the steering angle                                                                                         
  double steerAngle;  // Abs of steer                                                                                                      
  double lowerRes;    // The lower resolution between rows and cols                                                                        
 
  // Angles to evaluate the path given a radius                                                                                            
  double phi, delta_phi; 
  double max_phi; 
 
  // Distance to evaluate the path staight ahead                                                                                           
  double dist, delta_dist; 
  double max_dist; 
 
  // Radius to evaluate the car widht, given a phi                                                                                         
  double radius, delta_radius; 
 
  // Map of (row,col) of cells on the way of the path evaluated                                                                            
  std::map< RowCol, RowCol > pathMap; 
 
  // Getting the heading                                                                                                                   
  yaw = curState.Yaw; 
 
  // Storing the sign of the steering wheel and getting the abs value                                                                      
  if(steer >= 0) 
    steerSgn = +1; 
  else 
    steerSgn = -1; 
  steerAngle = steerSgn*steer; 
 
  // vehRow and vehCol is about the GPS location                                                                                           
  // We need to get the vehRow and vehCol for the mid point of the front of                                                                
  //  the car                                                                                                                              
  gpsPos.first = vehRow; 
  gpsPos.second = vehCol; 
  carPos = FrameRef2RowCol(gpsPos,curState.Yaw,GPS_2_FRONT_DISTANCE,0); 
 
  // Evaluating the smaller resolution                                                                                                     
  lowerRes = colRes; 
  if (rowRes < lowerRes) 
    lowerRes = rowRes; 
 
  // The step of a straight line integration is just the lower resolution                                                                  
  delta_radius = lowerRes/2 - lowerRes/4; 
 
  //*********************************************//                                                                                        
  //***GETTING THE CELLS THE CAR WILL RUN OVER***//                                                                                        
  //*********************************************//                                                                                        
 
  // GOING STRAIGHT !!                                                                                                                     
  if(!steerAngle){ 
 
    // Evaluating the step of the line ahead                                                                                               
    delta_dist = lowerRes/2 - lowerRes/4; 
 
    // Evaluating the max distance to consider ahead                                                                                       
    max_dist = pathMaxDistance; 
 
    // For each distance ahead ...                                                                                                         
    dist = 0; 
    while (dist < max_dist) { 
 
      // Distance ahead of the car in a frame reference                                                                                    
      delta_ahead = dist; 
      // Varying the car width ...                                                                                                         
      radius = -VEHICLE_WIDTH/2; 
      while(radius <= VEHICLE_WIDTH/2) { 
        // Distance aside of the car in a frame reference                                                                                  
        delta_aside = radius; 
 
        // Finding the RowCol of the cell                                                                                                  
        obstaclePos = FrameRef2RowCol(carPos,yaw, delta_ahead, delta_aside); 
 
        // debug                                                                                                                           
        (theMap[obstaclePos]).color = 1; 
 
        pathMap.insert( map<RowCol,RowCol>::value_type(obstaclePos,obstaclePos)); 
 
        radius += delta_radius; 
      } 
      dist += delta_dist; 
    } 
 
  } 
  // FOLLOWING A CURVE!!                                                                                                                   
  else { 
 
    PathRadius = VEHICLE_AXLE_DISTANCE/(tan(steerAngle)); 
 
    // Getting the center of the circunference (not sure if we need this!)                                                                 
    //north_aux =  PathRadius*cos(yaw)*sterrSgn;                                                                                           
    //east_aux = -PathRadius*sin(yaw)*steerSgn;                                                                                            
    //circCenter = RowColFromPosition(carPos,north_dist,east_dist);                                                                        
 
    // Evaluating step of phi, the step should be small enough not to lose                                                                 
    //  cells on the path evaluation (delta_phi*Radius < Res)                                                                              
    delta_phi = lowerRes/(PathRadius); 
 
    // Evaluating the max_phi, the maximum angle the path goes                                                                             
    max_phi = pathMaxDistance/PathRadius; 
 
    // For each phi ...                                                                                                                    
    phi = 0;
    while(phi <= max_phi) { 
 
      // Varying the car width ...                                                                                                         
      radius = PathRadius - VEHICLE_WIDTH/2; 
      while(radius <= (PathRadius + VEHICLE_WIDTH/2)) { 
 
        // Evaluating displacement ahead and aside                                                                                         
        delta_ahead = radius*sin(phi); 
        delta_aside = steerSgn*(PathRadius - radius*cos(phi)); 
 
        // Finding the RowCol of the cell                                                                                                  
        obstaclePos = FrameRef2RowCol(carPos,(curState.Yaw + steerSgn*phi), 
                                      delta_ahead, delta_aside); 
 
        // debug                                                                                                                           
        (theMap[obstaclePos]).color = 1; 
 
        pathMap.insert( map<RowCol,RowCol>::value_type(obstaclePos,obstaclePos)); 
 
        radius += delta_radius; 
      } 
 
      phi += delta_phi; 
    } 
  } 
 
  //*********************************************//                                                                                        
  //*************EVALUATING THE COST*************//                                                                                        
  //*********************************************//                                                                                        
  std::map<RowCol,RowCol>::iterator ind = pathMap.begin(); 
 
  numCells = 0; 
  sum_max_heightDiff = 0; 
  while(ind != pathMap.end()){ 
    row = (*ind).first.first; 
    col = (*ind).first.second; 
 
    // The cell we are analysing                                                                                                           
    currPos.first = row; 
    currPos.second = col; 
    if(theMap.find(currPos) != theMap.end()){ 
      currCell = theMap[currPos]; 
    } 
    // Getting the maximum height differente from this cell the the adjacents                                                              
    //  and summing up                                                                                                                     
    max_heightDiff = 0; 
 
    for(rowInd=-1; rowInd<=1 ;rowInd++) { //for row                                                                                        
      for(colInd =-1; colInd<=1; colInd++) { //for col                                                                                     
 
        // We don't need to compare the cell with itself                                                                                   
        if ((rowInd) || (colInd)) { //if not currCell                                                                                      
 
          // Finding the adjacent cell                                                                                                     
          deltaPos.first = rowInd; 
          deltaPos.second = colInd; 
          findPos = RowColFromDeltaRowCol(currPos, deltaPos); 
 
          // Getting the height of the adjacent cell                                                                                       
          if(theMap.find(findPos) != theMap.end()){ 
            findCell = theMap[findPos]; 
            heightDiff = fabs(currCell.value - findCell.value); 
            if(heightDiff > max_heightDiff){ 
              max_heightDiff = heightDiff; 
            } 
          } 
 
        } //end if not currCell                                                                                                            
 
      } //end for col                                                                                                                      
    } //end for row                                                                                                                        
 
    // Acumulationg the max_heightDiff and number of cells on the path                                                                     
    sum_max_heightDiff += max_heightDiff; 
    numCells++; 
    ind++; 
  } // end of cost evaluating while(ind != pathMap.end())                                                                                  
 
  // Cost ...                                                                                                                              
  cost = sum_max_heightDiff/numCells; 
 
  // debug                                                                                                                                 
  //cout << "Sum max heightDiff = " << sum_max_heightDiff << " and numCells = " << numCells << endl;                                       
 
  return(cost); 
} // end of EvalPathCost()                                                                                                                 
 
 
double genMap::findMaxHeightinRectangle(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4) 
{ 
  int x_y_is_inside = 1; 
  double minz = 1000000.0; 
  RowCol inside_cell; 
  if((((x1 <= x4)&&(y2 <= y3))&&((x2 != x1)&&(x3 != x1))) && 
     ((x4 != x2)&&(x4 != x3))) 
    { 
      for(double x = x1; x <= x4; x += rowRes) 
	{ 
	  for(double y = y2; y <= y3; y += colRes) 
	    { 
	      //Tests that x, y is inside the rectangle.                                                                                         
	      if(x <= x3) 
		{ 
		  if(y > (x - x3)*(y3 - y1)/(x3 - x1) + y3) 
		    { 
		      x_y_is_inside = 0; 
		    } 
		} 
	      else 
		{ 
		  if(y > (x - x4)*(y4 - y3)/(x4 - x3) + y4) 
		    { 
		      x_y_is_inside = 0; 
		    } 
		} 
	      if(x <= x2) 
		{ 
		  if(y < (x - x2)*(y2 - y1)/(x2 - x1) + y2) 
		    { 
		      x_y_is_inside = 0; 
		    } 
		} 
	      else
		{ 
		  if(y < (x - x4)*(y4 - y2)/(x4 - x2) + y4)
		    { 
		      x_y_is_inside = 0;
		    }
		} 
	      if(x_y_is_inside == 1)
		{ 
		  inside_cell = UTM2RowCol(x, y);
		  //Tests that inside_cell is inside window.                                                                                     
		  if(((inside_cell.first >= 0)&&(inside_cell.first < numRows)) &&
		     ((inside_cell.second >= 0)&&(inside_cell.second < numCols)))
		    { 
		      if(((theMap.find(inside_cell)->second).value) < minz)
			{ 
			  minz = (theMap.find(inside_cell)->second).value;
			} 
		    } 
		}//close conditions for point inside rectangle                                                                                   
	    }//close cycle through ys                                                                                                            
	}//close cycle through xs                                                                                                              
    }//close conditions for points in correct order                                                                                          
  else 
    { 
      if((((x1 == x2)||(x4 == x3))&&((y2 <= y3)&&(x4 >= x1)))&&(x2 != x4))
	{ 
	  for(double x = x1; x <= x4; x += rowRes)
	    { 
	      for(double y = y2; y <= y3; y += colRes)
		{ 
		  if(y >= (x - x1)*(y3 - y1)/(x3 - x1) + y1) 
		    { 
		      x_y_is_inside = 0;
		    } 
		  if(y <= (x - x2)*(y4 - y2)/(x4 - x2) + y2)
		    { 
		      x_y_is_inside = 0;
		    } 
		  if(x_y_is_inside == 1) 
		    { 
		      inside_cell = UTM2RowCol(x, y); 
		      if(((inside_cell.first >= 0)&&(inside_cell.first < numRows)) && 
			 ((inside_cell.second >= 0)&&(inside_cell.second < numCols))) 
			{ 
			  if((theMap.find(inside_cell)->second).value < minz) 
			    { 
			      minz = (theMap.find(inside_cell)->second).value; 
			    } 
			} 
		    }//end conditional for x,y is inside rectangle                                                                                 
		} //end run through ys                                                                                                             
	    }//end run through xs                                                                                                                
	}//end conditional block for points in order                                                                                           
      else 
	{ 
	  //cout << "Please rearrange your vertices.\n";                                                                                       
	} 
    } //end outer else                                                                                                                      
  return minz; 
} 
 
// translade and rotate coordinates                                                                                                        
/* void changeCoordinates(NorthEastDown,NorthEastDown,NorthEastDown,NorthEastDown,NorthEastDown,double);                                   
 * +DESCRIPTION: Put ref_ab as North (1,0) and evaluate the other vector at this new coordinate system                                     
 *                                                                                                                                         
 * +PARAMETERS                                                                                                                             
 *   - a_ref            : vector determining the position of the vector ab_ref                                                             
 *   - b_ref            : vector determining the extremity of the vector ab_ref                                                            
 *   - a_car            : vector determining the position of the vector ab_car                                                             
 *   - b_car            : vector determining the extremity of the vector ab_car                                                            
 * +RETURN: void, BUT THE PARAMETERS ARE UPDATED ISNSIDE                                                                                   
 */ 
void genMap::changeCoordinates(NorthEastDown &a_ref, NorthEastDown &b_ref, 
                               NorthEastDown &a_car, NorthEastDown &b_car, int &side){ 
  double cos_theta,sin_theta; 
  NorthEastDown ref_vec,car_vec; 
  NorthEastDown new_ref_vec,new_car_vec, aux; 
 
  // Evaluating vectors                                                                                                                    
 
  // Rotating the ref vector antiwiseclock                                                                                                 
  // [+cos +sin].[N]=[N']                                                                                                                  
  // [-sin +cos] [E] [E']                                                                                                                  
  ref_vec = b_ref - a_ref; ref_vec.norm(); 
  car_vec = b_car - a_car; car_vec.norm(); 
 
  sin_theta = ref_vec.Easting; 
  cos_theta = ref_vec.Northing; 
  new_ref_vec.Northing = cos_theta*ref_vec.Northing + sin_theta*ref_vec.Easting; 
  new_ref_vec.Easting = -sin_theta*ref_vec.Northing + cos_theta*ref_vec.Easting; 
  new_car_vec.Northing = cos_theta*car_vec.Northing + sin_theta*car_vec.Easting; 
  new_car_vec.Easting = -sin_theta*car_vec.Northing + cos_theta*car_vec.Easting; 
 
  // Finding the side                                                                                                                      
  aux.Northing = cos_theta*a_ref.Northing + sin_theta*a_ref.Easting; 
  aux.Easting = -sin_theta*a_ref.Northing + cos_theta*a_ref.Easting; 
  a_ref = aux; 
  aux.Northing = cos_theta*b_ref.Northing + sin_theta*b_ref.Easting;
  aux.Easting = -sin_theta*b_ref.Northing + cos_theta*b_ref.Easting; 
  b_ref = aux; 
  aux.Northing = cos_theta*a_car.Northing + sin_theta*a_car.Easting; 
  aux.Easting = -sin_theta*a_car.Northing + cos_theta*a_car.Easting; 
  a_car = aux; 
  aux.Northing = cos_theta*b_car.Northing + sin_theta*b_car.Easting; 
  aux.Easting = -sin_theta*b_car.Northing + cos_theta*b_car.Easting; 
  b_car = aux; 
 
  if((b_car.Easting) > b_ref.Easting){ 
    side = RIGHT_SIDE; 
  } 
  else{ 
    side = LEFT_SIDE; 
  } 
 
  // Returning values by real reference                                                                                                    
  b_ref = new_ref_vec; 
  b_car = new_car_vec; 
} 
// Evaluate the votes based on distance to the obstacle v 2.0                                                                              
/* vector<pair> generateVotes(double *steer)                                                                                               
 * +DESCRIPTION: This method receive an array of steering angle and evaluate the goodness for each                                         
 *                                                                                                                                         
 * +PARAMETERS                                                                                                                             
 *   - steer: steering angle position in radians. Right is positive and left                                                               
 *            is negative                                                                                                                  
 * +RETURN: first  = velocity                                                                                                              
 *          second = goodness                                                                                                              
 */ 
vector< pair<double,double> > genMap::generateArbiterVotes(int numArcs, vector<double> steerAngle) 
{ 
  int side, directionSteer, directionSteerNextWP, directionSteerVecField; 
  bool allCorridorVotesLow=true; 
  double delta_dist, dist, vel, PathRadius; 
  double dist_ahead, dist_aside, lowerRes; 
  double angle, angleProjection; 
  double alpha, to, crossAngle; 
  double w, d, x, offset, distanceCar2TrackLine, zeta; // for the vector field                                                             
  vector< pair<double,double> > votes,followerVotes; 
  pair <double,double> oneVote; 
  int ind, row, col; 
  RowCol rearCarPos, frontCarPos, cellUnderPos; 
  CellData cell; 
  map< RowCol, RowCol > cellsUnder; 
  std::map< RowCol, RowCol >::iterator mapIter; 
  bool corridorFound, outsideCorridor=false, wrongDirection=false; 
  bool crossTrackLine; 
  pair<bool,double> pointLine; 
  pair<double,double> NorthEast; 
  double scale, scaleSteer, scaleVel, scaleSteerNextWP, scaleVelNextWP, scaleSteerVecField, scaleVelVecField; 
  NorthEastDown v, projection, po; 
  NorthEastDown begin_track,end_track;           // track to follow                                                                        
  NorthEastDown rear_car, front_car;             // vector that defines the car                                                            
  NorthEastDown aux_begin_track, aux_end_track; 
  NorthEastDown aux_rear_car, aux_front_car; 
  NorthEastDown trackVector,carVector,aux1,aux2, projVector; 
  NorthEastDown car2track; 
  double maxRes=rowRes; 
  pair<double,bool> obsAvoid; // return value from obstacleAvoidance                                                                       
  bool obstacle; // was an obstacle detected?                                                                                              
  vector< pair<double,bool> > obsAvoidStorage; 
  double minDist,maxDist; 
  if(colRes > maxRes) 
    maxRes = colRes; 
  // Reserving room for the elements                                                                                                       
  votes.reserve(numArcs); 
  followerVotes.reserve(numArcs); 
 
  // If there is an option to follow the corridor we should also add the waypointFollower votes                                            
  if(evalCorridor){ 
    wrongDirection = false; 
    if(nextWaypoint < WaypointList.getNumTargetPoints()){ 
 
      // Car vector rear -> front                                                                                                          
      rearCarPos.first = vehRow; 
      rearCarPos.second = vehCol; 
      rear_car.Northing = curState.Northing; 
      rear_car.Easting = curState.Easting; 
      rear_car.Downing = 0; 
      front_car.Northing = curState.Northing + GPS_2_FRONT_DISTANCE*cos(curState.Yaw); 
      front_car.Easting = curState.Easting + GPS_2_FRONT_DISTANCE*sin(curState.Yaw); 
      front_car.Downing = 0; 
      carVector = front_car - rear_car; 
      // Track line vector begin -> end                                                                                                    
      begin_track.Northing = (WaypointList.getTargetPoints())[lastWaypoint].Northing; 
      begin_track.Easting  = (WaypointList.getTargetPoints())[lastWaypoint].Easting; 
      end_track.Northing = (WaypointList.getTargetPoints())[nextWaypoint].Northing; 
      end_track.Easting  = (WaypointList.getTargetPoints())[nextWaypoint].Easting; 
      trackVector = end_track - begin_track; 
 
      // Distance between the car and the trackline => pointLine.second is the distance                                                    
      pointLine = distancePoint2Line(begin_track,end_track,front_car); 
      distanceCar2TrackLine = pointLine.second; 
      crossTrackLine = pointLine.first; 
      //cout << "distanceCar2TrackLine = " << distanceCar2TrackLine << endl;                                                               
      //if(crossTrackLine) cout << "CROSS THE TRACK LINE" << endl;                                                                         
 
      // VERIFYING IF THE CAR IS OUTSIDE OF THE CORRIDOR                                                                                   
      dist_ahead = 0; 
      outsideCorridor = false; 
      while ((!outsideCorridor) && (dist_ahead <= VEHICLE_AXLE_DISTANCE) ) { 
        // Varying the car width ...                                                                                                       
        dist_aside = -VEHICLE_AVERAGE_TRACK/2; 
        while ((!outsideCorridor) && (dist_aside <= VEHICLE_AVERAGE_TRACK/2) ) { 
          // Finding the RowCol of the cell                                                                                                
          cellUnderPos = FrameRef2RowCol(rearCarPos,curState.Yaw, dist_ahead, dist_aside); 
          cell = getCellDataRC(cellUnderPos.first, cellUnderPos.second); 
          if(cell.type == OUTSIDE_CORRIDOR_CELL){ 
            outsideCorridor = true; 
          } 
          dist_aside += VEHICLE_AVERAGE_TRACK; 
        } 
        dist_ahead += VEHICLE_AXLE_DISTANCE; 
      } 
 
      /***********************************************/ 
      // EVALUATING THE COMMAND TO THE NEXT WAYPOINT //                                                                                    
      /***********************************************/ 
 
      // FINDING THE PROJECTION F THE CAR ON THE TRACK LINE                                                                                
      v = begin_track - end_track; // a - b                                                                                                
      v.norm(); 
      to = -( v.Northing*(begin_track.Northing - rear_car.Northing) + v.Easting*(begin_track.Easting - rear_car.Easting) + v.Downing*(begin_track.Downing - rear_car.Downing) ); 
      projection = v*to + begin_track; 
 
      // FINDING THE POINT ON THE TRACK LINE IN THE MIDDLE OF THE LINE FROM THERE AND THE NEXT WAYPOINT                                    
      if(nextWaypoint){ 
        // Weighting to point more to the track line than to the next waypoint                                                             
        double near=0,far=1; 
        po.Northing = (near*projection.Northing + far*end_track.Northing)/(near+far); 
        po.Easting = (near*projection.Easting + far*end_track.Easting)/(near+far); 
      } 
      else{ 
        po = end_track; 
      } 
 
      // debug                                                                                                                             
      //projection.display();                                                                                                              
      //po.display();                                                                                                                      
      //end_track.display();                                                                                                               
      //getchar();                                                                                                                         
 
      // BUILDING THE VECTOR THAT WILL BE OUR NEW NORTH                                                                                    
      begin_track = rear_car; aux_begin_track = begin_track; 
      end_track = po;  aux_end_track = po; 
      // Car vector                                                                                                                        
      aux_rear_car = rear_car; 
      aux_front_car = front_car; 
 
      // CHANGE COORDINATES TO PONT TO HALF WAY FROM THE PROJECTION AND                                                                    
      changeCoordinates(aux_begin_track,aux_end_track,aux_rear_car,aux_front_car,side); 
      trackVector = aux_end_track; 
      //trackVector.display();                                                                                                             
      carVector = aux_front_car; 
      scale = trackVector*carVector; 
      angle = abs(acos(scale)); 
 
      if(angle > USE_MAX_STEER){ 
        angle = USE_MAX_STEER; 
      } 
      //cout << "dot product = " << scale << " angle = " << 180*angle/M_PI << " USE_MAX_STEER = " << 180*USE_MAX_STEER/M_PI << endl;       
      scaleSteer = -2*angle/((double)USE_MAX_STEER) + 1; 
      //cout << "scaleSteer = " << scaleSteer << endl;                                                                                     
      scaleVel = (scale+1)/2; 
      if(carVector.Easting > 0){ 
        directionSteer = -1; 
      } 
      else{ 
        directionSteer = 1; 
      } 
      // Keeping this values in case all the votes are zero                                                                                
      scaleVelNextWP = scaleVel; 
      scaleSteerNextWP = scaleSteer; 
      directionSteerNextWP = directionSteer; 
 
      // If it is outside we've already evaluated the votes we need                                                                        
      //if(outsideCorridor){                                                                                                               
      if(!crossTrackLine){ 
        wrongDirection = true; 
      } 
      else{ 
        // POINT TO THE NEXT WAYPOINT BUT IN A FLAT WAY                                                                                    
        aux_rear_car = rear_car; 
        aux_front_car = front_car; 
        changeCoordinates(begin_track,end_track,aux_rear_car,aux_front_car,side); // Now the track is the new NORTH                        
        trackVector = end_track; 
        carVector = aux_front_car; 
        scale = trackVector*carVector; 
 
        if(scale < DELTA_SMALL){ 
          wrongDirection = true; 
          scale = -1;  // full turn                                                                                                        
          if(carVector.Easting > 0){ 
            directionSteer = -1; 
          } 
          else{ 
            directionSteer = 1; 
          } 
          scaleSteer = scale; 
          scaleVel = (scale+1)/2; 
        } 
        else{ // Analyse vector field, inside the corridor it must be flat                                                                 
          // Analysing how off we are related to the desired diraction                                                                     
          if(scale < cos(75*M_PI/180)){ 
            wrongDirection = true; 
          } 
          else{ 
            wrongDirection = false; 
          } 
          w = (double)VECTOR_FIELD_WEIGHT; 
          offset = (WaypointList.getTargetPoints())[lastWaypoint].offset; 
          d = distanceCar2TrackLine/offset;  // [-VECTOR_FIELD_WEIGHT  VECTOR_FIELD_WEIGHT]                                                
          x = ((double)side)*d*offset; 
          zeta = -(M_PI_2)*((x*x*x)/((w*offset)*(w*offset)*(w*offset))); 
          if(abs(zeta) > M_PI_2){ 
            zeta = -((double)side)*M_PI_2; 
          }  
	  crossAngle = 3*M_PI/180; 
	  if(d > 1){ 
	    zeta = -((crossAngle) + (crossAngle)*(d-1)/(w-1)); 
	  } 
	  else{ 
	    zeta = -(crossAngle)*d; 
	  } 
	  if(abs(zeta) > M_PI_2){ 
	    zeta = -((double)side)*M_PI_2; 
				  } 
	  //cout << "ZETA = " << zeta*180/M_PI << endl;                                                                                    
 
          // Building the vector field                                                                                                     
          trackVector.Northing = cos(zeta); 
          trackVector.Easting = sin(zeta); 
          trackVector.Downing = 0; 
          scale = trackVector*carVector; 
          directionSteer = 1; 
          if(carVector.Easting > 0){ 
            directionSteer = -1; 
          } 
          //scaleSteer = scale;                                                                                                            
 
          // Evaluating the angle just like global planner                                                                                 
          angle = abs(acos(scale)); // reduce so it does no wayve so much                                                                  
 
          if(angle > USE_MAX_STEER){ 
            angle = USE_MAX_STEER; 
          } 
          scaleSteer = -2*angle/((double)USE_MAX_STEER) + 1; 
          scaleVel = (scale+1)/2; 
        } 
        // Keeping this values in case all the votes are zero                                                                              
        scaleVelVecField = scaleVel; 
        scaleSteerVecField = scaleSteer; 
        directionSteerVecField = directionSteer; 
 
      } // If outside corridor                                                                                                             
    } // if next waypoint <  numTargetPoints                                                                                               
  } // if evalCorridor                                                                                                                     
 
 
  if( (!evalCorridor) && (useObsAvoidanceVelocities) ){ 
    obsAvoidStorage.reserve(numArcs); 
    minDist = DBL_MAX; 
    maxDist = DBL_MIN; 
    for(ind = 0;ind < numArcs;ind++){ 
      obsAvoid = obstacleAvoidance(steerAngle[ind]); 
      obsAvoidStorage[ind].first = obsAvoid.first; 
      obsAvoidStorage[ind].second = obsAvoid.second; 
      if(obsAvoidStorage[ind].first < minDist) minDist = obsAvoidStorage[ind].first; 
      if(obsAvoidStorage[ind].first > maxDist) maxDist = obsAvoidStorage[ind].first; 
    } 
    for(ind = 0; ind < numArcs; ind++) { 
      dist = obsAvoidStorage[ind].first; 
      obstacle = obsAvoidStorage[ind].second; 
 
      if(useConfinedSpaceGoodnessVotes) { 
        // Make the arc with the highest distance get a vote of 100                                                                        
        // All other arcs have a goodness proportional to that based on ratio of distances                                                 
        if(dist > distToObstacleOnArcMaxSpeed) oneVote.second = MAX_GOODNESS; 
        else oneVote.second = (dist / maxDist) * MAX_GOODNESS; 
      } else { 
        // Shape the goodness vote a bit                                                                                                   
        if(steerAngle[ind]){ 
          PathRadius = abs(VEHICLE_AXLE_DISTANCE/(tan(steerAngle[ind]))); 
          if(pathMaxDistance > MAX_PHI*PathRadius) 
            oneVote.second = 100*dist/(MAX_PHI*PathRadius); 
          else 
            oneVote.second = 100*dist/pathMaxDistance; 
        } 
        else{ 
          oneVote.second = 100*dist/pathMaxDistance; 
        } 
      } 
      if(oneVote.second > MAX_GOODNESS) oneVote.second = MAX_GOODNESS; 
 
      // Get the velocity                                                                                                                  
      oneVote.first = getVelocityVote(dist, obstacle, minDist, curState.Speed); 
      if(oneVote.first == 0.0) oneVote.second = 0.0; 
      votes.push_back(oneVote); 
    } 
  } else { 
    allCorridorVotesLow = true; 
    for(ind = 0;ind < numArcs;ind++){ 
      obsAvoid = obstacleAvoidance(steerAngle[ind]); 
      dist = obsAvoid.first; 
      obstacle = obsAvoid.second; 
      // EVALUATING HOW MUCH WE WOULD SWEEP BEFORE THE NEXT LOOP, GIVEN THE ACTUAL VELOCITY                                                
      delta_dist = curState.Speed*MAX_REACTION_TIME_BETWEEN_SENSOR_SWEEP_AND_BEGINNING_DECELLERATION_FOR_EVALUATING_VELOCITY; 
      dist = dist - delta_dist; 
      if(dist < 0){ 
        dist = 0; 
      } 
 
      // VOTE FROM OBSTACLE AVOIDANCE                                                                                                      
      if(steerAngle[ind]){ 
        PathRadius = abs(VEHICLE_AXLE_DISTANCE/(tan(steerAngle[ind]))); 
        if(pathMaxDistance > MAX_PHI*PathRadius) 
          oneVote.second = 100*dist/(MAX_PHI*PathRadius); 
        else 
          oneVote.second = 100*dist/pathMaxDistance; 
      } 
      else{ 
        oneVote.second = 100*dist/pathMaxDistance; 
      } 
      if(oneVote.second > MAX_GOODNESS) oneVote.second = MAX_GOODNESS; 
      if(oneVote.second > LOW_GOODNESS_THRESHOLD){ 
        allCorridorVotesLow=false; 
      } 
      // Shaping the votes to point in the middle                                                                                          
      //oneVote.second = oneVote.second*obstacleGoodnessShape(ind,numArcs)/100;                                                            
 
      // EVALUATING THE WEIGHTING BETWEEN VOTES FROM VECTOR FIELD AND PATH EVALUATION                                                      
      if(evalCorridor){ 
        if(outsideCorridor){ 
          //cout << "OUTSIDE OF THE CORRIDOR" << endl;                                                                                     
          alpha = 1; 
        } 
        else{ 
          // Going in the wrong direction, but inside the corridor                                                                         
          if(wrongDirection){ 
            //cout << "INSIDE BUT WRONG DIRECTION" << endl;                                                                                
            alpha = 0.75; 
          } 
          else{ 
            //cout << "NORMAL CASE" << endl;                                                                                               
            // normal situation, trust obstacle avoidance                                                                                  
            //if(abs(distanceCar2TrackLine - targetPoints[lastWaypoint].radius) < 0.1*targetPoints[lastWaypoint].radius)                   
            alpha = 0.25; 
          } 
        } 
      } 
      else{ 
        alpha = 0; 
      } 
 
      // SPEED EVALUATION                                                                                                                  
      if( (evalCorridor) && ((outsideCorridor) || (wrongDirection)) ){ 
        vel = CORRIDOR_SAFE_SPEED; 
      } 
      else{ 
        if(obstacle){ 
          vel = sqrt(2* NOMINAL_DECELERATION *dist); 
        } 
        else{ 
          vel = MAX_SPEED; 
        } 
      } 
      vel = velShape(ind,numArcs,vel); 
      if(vel > (WaypointList.getTargetPoints())[lastWaypoint].maxSpeed){ 
        vel = (WaypointList.getTargetPoints())[lastWaypoint].maxSpeed; 
      } 
      if(vel < MIN_SPEED) 
        vel = MIN_SPEED; 
      if(vel > MAX_SPEED) 
        vel = MAX_SPEED; 
 
      // BUILDING THE VOTES                                                                                                                
      oneVote.first = vel; 
 
      // Not change the votes if we are outside of the corridor                                                                            
      //if( (evalCorridor) && (outsideCorridor) )                                                                                          
      //  oneVote.second = 100;                                                                                                            
      //else                                                                                                                               
      oneVote.second = oneVote.second*(1 - alpha) + alpha*goodnessShape(-directionSteer,ind,numArcs,scaleSteer); 
 
      votes.push_back(oneVote); 
    } 
 
    // IF ALL VOTES FROM OBSTACLE AVOIDANCE ARE LOW, STOP !!!                                                                              
    // IF WE ARE ALREADY OUTSIDE OF THE CORRIDOR LET IT COME BACK TO THE TRACK LINE                                                        
    /*                                                                                                                                     
    if( (evalCorridor) && (!outsideCorridor) && (allCorridorVotesLow)){                                                                    
      for(ind = 0;ind < numArcs;ind++){                                                                                                    
        votes[ind].first = 0;                                                                                                              
        votes[ind].second = 0;                                                                                                             
      }                                                                                                                                    
    }                                                                                                                                      
    */ 
  } 
  return votes; 
} 
 
// Evaluate the votes based on costs                                                                                                       
/* double* generateVotes(double *steer)                                                                                                    
 * +DESCRIPTION: This method receive an array of steering angle and evaluate                                                               
 *               the goodness for each                                                                                                     
 *                                                                                                                                         
 * +PARAMETERS                                                                                                                             
 *   - steer: steering angle position in radians. Right is positive and left                                                               
 *            is negative                                                                                                                  
 * +RETURN: goodness                                                                                                                       
 */ 
 
vector<double> genMap::generateVotes(int numArcs, vector<double> steerAngle) 
{ 
  double max_cost; 
  vector<double> cost, votes; 
  int ind; 
 
  // Reserving room for the elements                                                                                                       
  cost.reserve(numArcs); 
  votes.reserve(numArcs); 
 
  // Evaluating costs                                                                                                                      
  max_cost = 0; 
  for(ind = 0;ind < numArcs;ind++){ 
    cost.push_back(evalPathCost(steerAngle[ind])); 
    if(cost[ind] > max_cost){ 
      max_cost = cost[ind]; 
    } 
  } 
 
  // Normalizing and generating votes [0 100]                                                                                              
  for(ind = 0; ind < numArcs; ind++){ 
    cost[ind] /= max_cost; 
    votes.push_back(100*(1 - exp(cost[ind]))/(1 - exp(1.0))); 
  } 
  return votes; 
} 


// velocity is determine with the equation
//   vel = (MIN_SPEED - 1.0) + e^(m * dist)
//   we want to limit the velocity such that at a distance of
//   DIST_TO_OBSTACLE_MAX_SPEED the velocity is MAX_SPEED_OBSTACLE_DETECTED
//   This functions returns the value "m" to make that possible.
// Sort of seems like this should be a constant, but oh well....
double genMap::getVelocityCurveSlope() {
  return log(velMaxSpeed - velMinSpeed + 1) / distToObstacleOnArcMaxSpeed;
}

// This returns the velocity vote for a particular arc.
// It takes the following parameters:
//   dist              Distance to nearest obstacle along that arc
//   obstacleDetected  Whether or not an obstacle was detected on ANY arc
//   minDist           Of ALL the steering arcs, the minimum distance to any
//                     obstacle
double genMap::getVelocityVote(double dist,bool obstacleDetected, double minDist, double curVehicleSpeed)
{
  double v;

  // Our sensors take roughly 1 second (worst case) to respond to the suddent
  // appearance of an obstacle.  So adjust distance to obstacle by speed * 1s.
  dist -= curVehicleSpeed;
  minDist -= curVehicleSpeed;

  if(dist < distToObstacleOnArcHalt) return 0.0;
  else if(minDist < distToObstacleAnywhereHalt) return 0.0;
  else if((dist < distToObstacleOnArcMaxSpeed) || (minDist < distToObstacleAnywhereMaxSpeed)) {
    v = (velMinSpeed - 1.0) + exp(getVelocityCurveSlope() * dist);
    if(v < MIN_SPEED) return MIN_SPEED;
    if(v > MAX_SPEED) return MAX_SPEED;
    return v;
  }
  else if(velMaxSpeed > MAX_SPEED) return MAX_SPEED;
  else if(velMaxSpeed < MIN_SPEED) return MIN_SPEED;
  //  else return velMaxSpeed;
  else return MAX_SPEED;
}
