#include <genMap.hh>


//////////////////////////////////////////////////////////////////////////////
// Frame Transformation Methods: Convert between the different coordinate systems and cell indices//
//////////////////////////////////////////////////////////////////////////////

/* *METHOD RowColDeltaRowCol*
 * +DESCRIPTION: Evaluate the new indices inside the map given the actual
 *    indices and the indices displacement
 * +PARAMETERS
 *  -oldPos  : matrix indices of the old position
 *  -deltaPos: horizontal and vertical displacement in terms of number matrix
 *             indices
 * +RETURN: RowCol of the new position in the matrix
 */
RowCol genMap::RowColFromDeltaRowCol(RowCol oldPos, RowCol deltaPos)
{
  RowCol newPos;

  if(numCols)
    newPos.first = (numRows + oldPos.first + deltaPos.first)%numRows;
  if(numRows)
    newPos.second = (numCols + oldPos.second + deltaPos.second)%numCols;

  return(newPos);
}


/* *METHOD RowColFrameDeltaPosition*
 * +DESCRIPTION: Evaluate the new indices inside the map given the actual
 *    indices and the north and east displacement in meters
 * +PARAMETERS
 *   -oldPos: matrix indices of the old position
 *   -north_dist: horizontal displacement in meters from the original position.
 *   -east_dist: vertical displacement in meters from the original position.
 * +RETURN: RowCol of the new position in the matrix
 */
RowCol genMap::RowColFromDeltaPosition(RowCol oldPos, double north_dist,
                                       double east_dist)
{
  RowCol deltaPos, newPos;

  // Verifying the range
  if( (abs(north_dist) > numRows*rowRes) || (abs(east_dist) > numCols*colRes) ){
    //cout << "genMap::RowColFromDeltaPosition  north_dist or east_dist outside of the map range, but returning the normal(wrapped around) value." << endl;
  }
  deltaPos.first = (int)round(north_dist/rowRes);
  deltaPos.second = (int)round(east_dist/colRes);

  // Finding the new indices
  newPos = RowColFromDeltaRowCol(oldPos,deltaPos);

  return(newPos);
}

/* *METHOD FramRef2RowCol*
 * +DESCRIPTION: This method is to evaluate the nwe RowCol in function of
 *    displacements in the frame reference.
 *    The spot is delta_ahead meters (ahead of the car) and delta_aside meters
 *    (towards the right fo the car).
 *    Ahead and to the right are the positive convention.
 *    Pass pYaw as zero to use the actual yaw of the car
 * +PARAMETERS
 *  -oldPos     : matrix indices of the old position
 *  -pYaw       : heading of the car
 *  -delta_ahead: horizontal displacement in meters from the original position.
 *  -delta_aside: vertical displacement in meters from the original position.
 * +RETURN: RowCol of the new position in the matrix
 */
RowCol genMap::FrameRef2RowCol(RowCol refPos, double pYaw, double delta_ahead, double delta_aside)
{
  RowCol newPos;
  double north_dist;       // aligned to east
  double east_dist;       // aligned to north
  double yaw;

  // Getting the curent yaw
  yaw = pYaw;

  // Evaluating the horizontal and vertical displacement
  north_dist = delta_ahead*cos(yaw) - delta_aside*sin(yaw);
  east_dist = delta_ahead*sin(yaw) + delta_aside*cos(yaw);

  // Calculating the RowCol
  newPos = RowColFromDeltaPosition(refPos, north_dist, east_dist);

  return (newPos);
}


pair<double,double> genMap::RowCol2UTM(RowCol pos)
{
  pair<double,double> NorthEast;
  RowCol find, delta;
  double north_disp, east_disp;

  // First, check if the RowCol position is valid
  if (pos.first < 0 || pos.first >= numRows || pos.second < 0 || pos.second >= numCols)
    {
      cout << "genMap::RowCol2UTM: invalid row/col number" << endl;
      NorthEast.first = 0;
      NorthEast.second = 0;
      return NorthEast;
    }

  find.first = leftBottomRow;
  find.second = leftBottomCol;

  // Counting the number of rows until the desired one
  delta.first = 1;
  delta.second = 0;
  north_disp = 0;
  while(find.first != pos.first)
    {
      find = RowColFromDeltaRowCol(find,delta);
      north_disp += rowRes;
    }

  // Counting the number of rows until the desired one
  delta.first = 0;
  delta.second = 1;
  east_disp = 0;
  while(find.second != pos.second)
    {
      find = RowColFromDeltaRowCol(find,delta);
      east_disp += colRes;
    }

  NorthEast.first = leftBottomUTM_Northing + north_disp + rowRes/2;
  NorthEast.second = leftBottomUTM_Easting + east_disp + colRes/2;

  return (NorthEast);
}


RowCol genMap::UTM2RowCol(double UTM_Northing, double UTM_Easting)
{
  RowCol cell_indices, leftBottomPos;
  double north_dist, east_dist;

  cell_indices.first = 0;
  cell_indices.second = 0;
  // Verifing if the UTM coordinates is inside the map
  if((leftBottomUTM_Northing > UTM_Northing) ||
     (UTM_Northing > (leftBottomUTM_Northing + numRows*rowRes)))
    {
      //cout << "genMap::UTM2RowCol UTM_Northing out of range" << endl;
      return(cell_indices);
    }
  if((leftBottomUTM_Easting > UTM_Easting) ||
     (UTM_Easting > (leftBottomUTM_Easting + numCols*colRes)))
    {
      //cout << "genMap::UTM2RowCol UTM_Easting out of range" << endl;
      return(cell_indices);
    }

  leftBottomPos.first = leftBottomRow;
  leftBottomPos.second = leftBottomCol;

  north_dist = UTM_Northing - leftBottomUTM_Northing;
  east_dist = UTM_Easting - leftBottomUTM_Easting;
  cell_indices = RowColFromDeltaPosition(leftBottomPos,north_dist,east_dist);

  return cell_indices;
}

