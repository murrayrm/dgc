#include "genMap.hh"


//////////////////////////////////////////////////////////////////////////////
// display methods, assorted helper functions, and some random things we are not even using//
//////////////////////////////////////////////////////////////////////////////


int genMap::serializedMapSize()
{
  return 0;
}


int genMap::serializeMap(char* buffer, int bufferSize)
{
  if(bufferSize >= serializedMapSize()) {
    double startRow, startCol, endRow, endCol;
    double rowStep, colStep;
    double val;

    startRow = -numRows/2*rowRes;
    endRow = numRows/2*rowRes;
    rowStep = rowRes;

    startCol = -numCols/2*colRes;
    endCol = numCols/2*colRes;
    colStep = colRes;

    //printf("Start: %lf, Stop: %lf, step: %lf\n", startRow, endRow, rowStep);

    for(double row = startRow; row <= endRow; row+=rowStep) {
      for(double col = startCol; col <= endCol; col+=colStep) {
        val = getCellDataFrameRef(row, col).value;
        //printf("At (%lf, %lf): %lf\n", row, col, val);

      }
    }

    return serializedMapSize();
  } else {
    return -1;
  }

  return -1;
}

/* double genMap::shrinkObstacle(double value,double deltaT)
 * +DESCRIPTION: This functions returns a goodness of a given steering angle
 *               for the votes based on the vector field to the next waypoint
 * +PARAMETERS
 *   - side    : where you want to turn, LEFT_SIDE for left and RIGHT_SIDE for right
 *   - indArc  : index of the arc
 *   - numArcs : number of arcs
 *   - scale   : this must be a internal product between the actual direction and
 *     [0 1]     the desired one. Used to scale how hard to turn the steering wheels
 * +RETURN:
 *   - goodness : goodnes evaluated for that arc
 */
double genMap::shrinkObstacle(double value,double deltaT){
  double y,param;

  //param = MIN_EXP_SHRINK_PARAMETER;
  //param += curState.Speed*((double)(MAX_EXP_SHRINK_PARAMETER-MIN_EXP_SHRINK_PARAMETER))/((double)MAX_SPEED);
  //y = value*exp(-param*deltaT);

  if ( deltaT > GENMAP_OBS_EXPIRE_TIME ) {
    y = 0;
  }
  else {
    y = value;
  }
  return(y);
}


// Show map
void genMap::display()
{
  int rowInd,colInd;
  double height;
  RowCol mapPos;
  RowCol vehPos;

  int ind, total;
  total = numCols;

  // Getting the position of the car
  vehPos.first = vehRow;
  vehPos.second = vehCol;
  cout << "Map reference: (row, col) or (north , east)" << endl;
  cout << "Map (" << numRows << "," << numCols << ") with resolution of ("
       << rowRes << "," << colRes << ") in meters" << endl;
  cout << "Bob (row,col) = (" << vehRow << "," << vehCol << ") UTM(north,east) = ("
       << curState.Northing << "," << curState.Easting << ")" << endl;
  cout << "Letf bottom corner in RC  is (" << leftBottomRow << "," << leftBottomCol << ")" << endl;
  cout << "Letf bottom corner in UTM is (" << leftBottomUTM_Northing << "," << leftBottomUTM_Easting << ")" << endl;
  cout << endl;
  for(rowInd = (numRows - 1); rowInd >= 0 ;rowInd--) { //for row
    for(colInd = 0; colInd < numCols; colInd++) { //for col
      mapPos.first = rowInd;
      mapPos.second = colInd;

      cout << "| ";
      if(vehPos == mapPos){
        printf("%c%sX%c%s",ESCAPE,RED_COLOR,ESCAPE,DEFAULT);
      }
      else{
        // Colorful
        if(theMap.find(mapPos) == theMap.end()){
          printf(" ");
        }
        else{
          if((theMap.find(mapPos)->second).type == OUTSIDE_CORRIDOR_CELL){
            printf("%c%sc%c%s", ESCAPE, GREEN_COLOR,ESCAPE, DEFAULT);
          }
          else{
            if((theMap[mapPos]).color == BLUE_INDEX)
              printf("%c%s%1.0f%c%s", ESCAPE, BLUE_COLOR, theMap[mapPos].value,
		     ESCAPE, DEFAULT);
            else if((theMap[mapPos]).color == RED_INDEX)
              printf("%c%s%1.0f%c%s", ESCAPE, RED_COLOR, theMap[mapPos].value,
		     ESCAPE, DEFAULT);
            else if((theMap[mapPos]).color == GREEN_INDEX)
              printf("%c%s%1.0f%c%s", ESCAPE, GREEN_COLOR, theMap[mapPos].value,
		     ESCAPE, DEFAULT);
            else // Black and white
              printf("%1.0f",theMap[mapPos].value);
          }
        }
      }
      cout << " ";
    } //for col
    cout <<  "|" << endl;
    for(ind = 0; ind < total; ind++)
      cout << "----";
    cout << "-" << endl;
  } //for row
}


// Display thresholds
void genMap::displayThresholds(){
  cout << "Obs = " << obstacleThreshold << " DH = " << heightDiffThreshold << " TIRE_OFF = " << tireOffThreshold << " VEC_PROD = " << vectorProdThreshold << endl;
}


int genMap::findMaxListLength()
{
  int max_length = 0, new_size;
  for(int row = leftBottomRow; row < leftBottomRow+numRows; row++)
    {
      for(int col = leftBottomCol; col < leftBottomCol+numCols; col++)
        {
          new_size = sizeof((getCellDataRC(row, col)).cellList);
          if(new_size > max_length)
            {
              max_length = new_size;
            }
        }
    }
  return max_length;
}

/*
 *void genMap::CutCellLine(NorthEastDown end)
 *{
 * NorthEastDown start;
 * start.Northing = curState.Northing;
 *( start.Easting = curState.Easting;
 * start.Downing = 0;
 * end.Downing = 0;
 * CutCellLine(start, end);
 *}

 *void genMap::CutCellLine(NorthEastDown start, NorthEastDown end)
 *{
 * vector<RowCol> cells_on_line = GetCellLine(start, end);
 * RowCol currentRC = cells_on_line.front();
 * RowCol backRC = cells_on_line.back();
 * while((currentRC.first != backRC.first)&&(currentRC.second != backRC.second))
 *   {
 *     if(theMap.find(currentRC) != theMap.end())
 *      {
 *        if((getCellDataRC(currentRC.first, currentRC.second)).value < end.Downing)
 *          {
 *            theMap.erase(currentRC);
 *          }
 *      }
 *     cells_on_line.pop_back();
 *     currentRC = cells_on_line.front();
 *   }
 *}
 *
 *vector<RowCol> genMap::GetCellLine(NorthEastDown start, NorthEastDown end)
 *{
 * RowCol startRC = UTM2RowCol(start.Northing, start.Easting);
 * RowCol endRC = UTM2RowCol(end.Northing, end.Easting);
 * vector<RowCol> cells_on_line;
 * double lowRes;
 * double slope = 0;
 * bool straight_east;
 * if(endRC.first == startRC.first)
 *   {
 *     straight_east = true;
 *   }
 * else
 *   {
 *     slope =(end.Easting - start.Easting)/(end.Northing - start.Northing);
 *     straight_east = false;
 *   }
 * if(rowRes < colRes)
 *   lowRes = rowRes;
 * else
 *   lowRes = colRes;
 * NorthEastDown current_point = start;
 * RowCol current_RC = startRC;
 * while((current_RC.first != endRC.first)||(current_RC.second != endRC.second))
 *   {
 *     if(straight_east == true)
 *      {
 *        if(start.Easting > end.Easting)
 *          {
 *            current_point.Easting -= colRes;
 *          }
 *        else
 *          {
 *            current_point.Easting += colRes;
 *          }
 *      }
 *     else
 *      {
 *        if(startRC.second == endRC.second)
 *          {
 *            if(start.Northing > end.Northing)
 *              {
 *                current_point.Northing -= rowRes;
 *              }
 *            else
 *              {
 *                current_point.Northing += colRes;
 *              }
 *          }
 *        else
 *          {
 *            if(fabs(slope) < 1)
 *              {
 *                if(start.Northing > end.Northing)
 *                  {
 *                    current_point.Northing -= lowRes;
 *                    current_point.Easting -= slope*lowRes;
 *                  }
 *                else
 *                  {
 *                    current_point.Northing += lowRes;
 *                    current_point.Easting += slope*lowRes;
 *                  }
 *              }
 *            else
 *              {
 *                if(start.Easting > end.Easting)
 *                  {
 *                    current_point.Northing -= lowRes/slope;
 *                    current_point.Easting -= lowRes;
 *                  }
 *                else
 *                  {
 *                    current_point.Northing += lowRes/slope;
 *                    current_point.Easting += lowRes;
 *                  }
 *              }
 *          }
 *      }
 *     current_RC = UTM2RowCol(current_point.Northing, current_point.Easting);
 *     cells_on_line.push_back(current_RC);
 *   }
 * return cells_on_line;
 *}
 */

/* *METHOD getCellsLine
 *
 * +DESCRIPTION: given a point in NorthEastDown, returns all the cells on the line
 *               from the sensor to there
 *
 * +PARAMETERS
 *
 *  - point : point to make the line through
 *
 * +RETURN: vector<RowCol>
 */
vector<RowCol> genMap::getCellsLine(NorthEastDown point){
  vector<RowCol> ret;
  NorthEastDown start, end, delta;
  RowCol curPos, startPos;
  double distance, yaw, step, dist;
  int maxNumCells;
  double minNorthing, maxNorthing;
  double minEasting, maxEasting;

  // Getting the step
  step = rowRes;
  if(step > colRes)
    step = colRes;
  step /= 2;

  // Getting the distance between the car and the point
  start.Northing = curState.Northing + GPS_2_FRONT_DISTANCE*cos(curState.Yaw);
  start.Easting = curState.Easting + GPS_2_FRONT_DISTANCE*sin(curState.Yaw);
  start.Downing = 0;
  end.Northing = point.Northing;
  end.Easting = point.Easting;
  end.Downing = 0;
  delta.Northing = end.Northing - start.Northing;
  delta.Easting = end.Easting - start.Easting;
  // Getting the yaw
  delta.norm();
  yaw = atan2(delta.Easting,delta.Northing);
  cout << "yaw = " << yaw << endl;

  // Determining weather the UTM is inside the map or not
  // If outside the map analyse the point further away but inside the map
  minNorthing = leftBottomUTM_Northing;
  maxNorthing = minNorthing + numRows*rowRes;
  minEasting = leftBottomUTM_Easting;
  maxEasting = minEasting + numCols*colRes;
  if((end.Northing <= minNorthing) || (end.Northing >= maxNorthing)){
    if(abs(yaw) > M_PI/2){//traversing up on the map
      end.Northing = minNorthing;
    }
    else{//traversing down on the map
      end.Northing = maxNorthing;
    }
    end.Easting = (end.Northing-start.Northing)*tan(yaw) + start.Easting;
  }
  else if((end.Easting <= minEasting) || (end.Easting >= maxEasting)){
    if(yaw > 0){//traversing east on the map
      end.Easting = maxEasting;
    }
    else{//traversing west on the map
      end.Easting = minEasting;
    }
    end.Northing = (end.Easting-start.Easting)/tan(yaw) + start.Easting;
  }
  end.display();
  // Evaluating again with the new boundaries
  delta.Northing = end.Northing - start.Northing;
  delta.Easting = end.Easting - start.Easting;
  //  distance = sqrt(delta.Northing*delta.Northing + delta.Easting*delta.Easting)-lowerRes;

  // How many cells to update
  maxNumCells = (int)floor(distance/step) + 1;
  cout << "maxNumCells="  << maxNumCells << endl;
  cout << "distance="  << distance << endl;
  ret.reserve(maxNumCells);

  // Building the vector
  dist = 0;
  startPos = UTM2RowCol(start.Northing,start.Easting);
  curPos = startPos;
  while(dist < distance){
    curPos = FrameRef2RowCol(startPos,yaw,dist,0);
    cout << "(Row,Col) = (" << curPos.first << "," << curPos.second << ")" << endl;
    theMap.find(curPos)->second.color = GREEN_INDEX;
    theMap.find(curPos)->second.value = 1;
    ret.push_back(curPos);
    dist += step;
  }
  return(ret);
}
