function draw_vehicle( varargin )
% Description: 
%   This MATLAB function creates a graphic of the Tahoe
%   vehicle on its first call, at a given position and
%   orientation.  On subsequent calls, it changes the 
%   location and orientation of this graphic.  It also
%   indicates the steering angle of the front wheels
%   and draws arcs indicating which arcs the vehicle is evaluating
%   by default arc drawing is disabled, add a 7th argument to enable, if 
%   you want one of the arcs highlighed green, then give it and the index
%   of the arc.  index is 0 to num_arcs-1, from left to right
% Author: Lars Cremean
% Modified from: draw_vehicle.m <-- for the MVWT
% By: Lars Cremean
% Date: 05/28/2002
%
% Usage:
%   draw_vehicle( 'str', X [Easting!], Y [Northing!], Yaw, phi, 'color' )
%     
%     'str': string identifier (should be unique for
%       each  drawn vehicle)
%     x: x position of vehicle (meters)
%     y: y position of vehicle (meters)
%     th: vehicle orientation (radians)
%     phi: steering angle (radians)
%     color: MATLAB color specification
%
% Note: 
%   You may have to run "clear draw_vehicle" or
%   "clear all", and "hold on" to get this 
%   function to work properly.  Also, "axis equal"
%   will make the aspect ratio right
%
% Example function call:
%   draw_vehicle('2r', 1, 2, pi/4, 0.43, 'b' )
% or if you wish to draw the arcs 
% draw_vehicle('2r', 1, 2, pi/4, 0.43, 'b' , 1)

% Default values for optional arguments
box_size = 0;
num_arcs = 25;
max_steer = 20.5 * pi / 180;
draw_arcs = 0;
arc_highlight = -1;
select = -1;

% Handle arguments
if( nargin < 6 )
  error('Need at least six arguments (str,X,Y,Yaw,phi,color).');
end
if ( nargin >= 6 )
  str = varargin{1};
  x   = varargin{2};
  y   = varargin{3};
  Yaw = varargin{4};
  phi = varargin{5};
  color = varargin{6};
end
if (nargin >=7)
   draw_arcs = varargin{7};
end
if (nargin ==8)
    arc_highlight = varargin{8};
end
if( nargin ==9)
  box_size = varargin{9};
end
if( nargin >= 10 )
  error('Too many arguments.')
end

% set the heading
th = pi/2 - Yaw;

if (arc_highlight >= 0)
    % set the arc to highlight
    arcindex = [1:(num_arcs-1)/2 num_arcs ((num_arcs-1)/2+1):(num_arcs-1)];
    select = arcindex(arc_highlight+1);
end 

% make persistent variables for the line elements
persistent chassisX chassisY
persistent genwheelX genwheelY % generic wheel
persistent lbwX lbwY % left back wheel
persistent rbwX rbwY % right back wheel
persistent lfwX lfwY % left front wheel
persistent rfwX rfwY % right front wheel
persistent lfwXr lfwYr % rotated left front wheel
persistent rfwXr rfwYr % rotated right front wheel
persistent boxX boxY % box around the vehicle
% declare handles to the vehicle graphics persistent so
% we don't need to recreate them every call to this function
eval(['persistent h',str,'VEH h',str,'LBW h',str,'RBW'])
eval(['persistent h',str,'LFW h',str,'RFW h',str,'BOX'])
if (draw_arcs > 0)
  persistent archandles arc_x arc_y;
end

if eval(['isempty(h',str,'VEH)'])
    
    % initialize vehicle geometry (only need to do this once)  
    % define the geometry of the vehicle here
    % take the origin of the car to be at the rear axle
    % define zero Yaw to be pointing north, but plot using
    % heading (positive CCW from East)
    % from DynamicFeasibilityEvaluator/vehicle_constants.h
    length = 5.05;
    width  = 2.002;
    wbase  = 2.946; % distance between axles
    track  = 1.66;  % distance between wheels (average front/back)

    % wheel geometry
    wheelW = 0.5;   % wheel width
    wheelD = 1.0;   % wheel diameter    
    numpoints = 100;
    angle_min = -pi/2;
    angle_max = pi/2;
    arclen = 40;

    % the geometry of each of the shapes to draw
    % the vehicle is pointing right initially (heading = 0)
    % with the center of the rear axle at the origin
    % go through the points CW from bottom left
    % make a generic wheel (rectangle) in this manner
    genwheelX = [-wheelD/2  wheelD/2 wheelD/2 -wheelD/2 -wheelD/2];
    genwheelY = [-wheelW/2 -wheelW/2 wheelW/2  wheelW/2 -wheelW/2];
    lbwX = 0        + genwheelX; % left back wheel
    lbwY = track/2  + genwheelY; % left back wheel
    rbwX = 0        + genwheelX; % right back wheel
    rbwY = -track/2 + genwheelY; % right back wheel
    lfwX = wbase    + genwheelX; % left front wheel
    lfwY = track/2  + genwheelY; % left front wheel
    rfwX = wbase    + genwheelX; % right front wheel
    rfwY = -track/2 + genwheelY; % right front wheel
    % the chassis line element
    chassisX = wbase/2 - length/2 + .3 + [0 length length 0 0];
    chassisY = [-width/2 -width/2 width/2 width/2 -width/2]; 
    % a box around the vehicle
    boxX = [-box_size/2  box_size/2  box_size/2 -box_size/2 -box_size/2];
    boxY = [-box_size/2 -box_size/2  box_size/2  box_size/2 -box_size/2]; 

    emopt = '''xor'''; % erase mode option
        
    % create the plot the vehicle (only need to do this once)
    eval(['h',str,'VEH = line(chassisX,chassisY);']); 
    % draw the wheels    
    eval(['h',str,'LBW = line(lbwX,lbwY);']);
    eval(['h',str,'RBW = line(rbwX,rbwY);']);
    eval(['h',str,'LFW = line(lfwX,lfwY);']);
    eval(['h',str,'RFW = line(rfwX,rfwY);']);
    eval(['h',str,'BOX = line(boxX,boxY);']);
    % set properties of the figure elements
    eval(['set(h',str,'VEH,''EraseMode'',',emopt,',''Color'',''k'');']);
    eval(['set(h',str,'LBW,''EraseMode'',',emopt,',''Color'',''k'');']);
    eval(['set(h',str,'RBW,''EraseMode'',',emopt,',''Color'',''k'');']);
    eval(['set(h',str,'LFW,''EraseMode'',',emopt,',''Color'',''k'');']);
    eval(['set(h',str,'RFW,''EraseMode'',',emopt,',''Color'',''k'');']);
    eval(['set(h',str,'BOX,''EraseMode'',',emopt,',''Color'',''k'');']);    

if (draw_arcs > 0)    
    handles = []; 
    xpoints = [];
    ypoints = [];
    hg = 0;
    
    % get the range of angles we will be evaluating
    arcs = linspace(-max_steer, max_steer, num_arcs);
    left_arcs = arcs(1:(num_arcs-1)/2);
    right_arcs = arcs((num_arcs-1)/2+2:end);
    
    % get the arc radii for each steering angle
    left_radii = wbase ./tan(left_arcs);
    right_radii = wbase ./ tan(right_arcs);

    % get the origins for each arc section
    left_x = left_radii * cos(hg - pi/2);
    left_y = left_radii * sin(hg - pi/2);
    right_x = right_radii * cos(hg - pi/2);  
    right_y = right_radii * sin(hg - pi/2) ;
    
    % plot left side arcs
    for i = 1:max(size(left_radii))
        if(abs(left_radii(i)) * (angle_max - angle_min) > arclen)
            angles = linspace(hg+angle_min, (hg+angle_min) + arclen/abs(left_radii(i)), numpoints);
        else
            angles = linspace(hg+angle_min, hg+angle_max, numpoints);
        end
        xp = abs(left_radii(i)) * cos(angles) + left_x(i);
        yp = abs(left_radii(i)) * sin(angles) + left_y(i);
        h =line(xp, yp);
        handles = [handles h];
        xpoints = [xpoints; xp];
        ypoints = [ypoints; yp];
    end
    
    % plot right side arcs
    for i = 1:max(size(right_radii))
        % draw the arcs to a full 180 degrees if the length is shorter than
        % arclen, otherwise cutoff at arclen
        if(abs(right_radii(i)) * (angle_max - angle_min) > arclen)
            angles = linspace((hg+angle_max) - arclen/right_radii(i), hg+angle_max, numpoints);
        else
            angles = linspace(hg+angle_min, hg+angle_max, numpoints);
        end        
        xp = abs(right_radii(i)) * cos(angles) + right_x(i);
        yp = abs(right_radii(i)) * sin(angles) + right_y(i);
        h =line(xp, yp);
        handles = [handles h];
        xpoints = [xpoints; xp];
        ypoints = [ypoints; yp];
    end
    
    % generate/plot straight ahead line
    straight_x = arclen;
    straight_y = 0;
    xp = linspace(0, straight_x, numpoints);
    yp = linspace(0, straight_y, numpoints);
    h = line(xp, yp);
    handles = [handles h];
    xpoints = [xpoints; xp];
    ypoints = [ypoints; yp];
    %ishandle(handles)
    
    archandles = handles;
    arc_x = xpoints;
    arc_y = ypoints;
end
end

% front wheels (rotate in place first)
lfwXr = genwheelX*cos(-phi) - genwheelY*sin(-phi) + lfwX;
lfwYr = genwheelY*cos(-phi) + genwheelX*sin(-phi) + lfwY;
rfwXr = genwheelX*cos(-phi) - genwheelY*sin(-phi) + rfwX;
rfwYr = genwheelY*cos(-phi) + genwheelX*sin(-phi) + rfwY;

% reset figure handle parameters
eval(['set(h',str,'VEH,''Xdata'', x + chassisX*cos(th) - chassisY*sin(th));'])
eval(['set(h',str,'VEH,''Ydata'', y + chassisY*cos(th) + chassisX*sin(th));'])

eval(['set(h',str,'LBW,''Xdata'', x + lbwX*cos(th) - lbwY*sin(th));'])
eval(['set(h',str,'LBW,''Ydata'', y + lbwY*cos(th) + lbwX*sin(th));'])
eval(['set(h',str,'RBW,''Xdata'', x + rbwX*cos(th) - rbwY*sin(th));'])
eval(['set(h',str,'RBW,''Ydata'', y + rbwY*cos(th) + rbwX*sin(th));'])
% front wheels (rotate, then translate)
eval(['set(h',str,'LFW,''Xdata'', x + lfwXr*cos(th) - lfwYr*sin(th));'])
eval(['set(h',str,'LFW,''Ydata'', y + lfwYr*cos(th) + lfwXr*sin(th));'])
eval(['set(h',str,'RFW,''Xdata'', x + rfwXr*cos(th) - rfwYr*sin(th));'])
eval(['set(h',str,'RFW,''Ydata'', y + rfwYr*cos(th) + rfwXr*sin(th));'])
% box around the vehicle
eval(['set(h',str,'BOX,''Xdata'', x + boxX);'])
eval(['set(h',str,'BOX,''Ydata'', y + boxY);'])

% draw the arcs
if (draw_arcs > 0)
    for i=1:num_arcs
        %eval(['ishandle(archandles(',num2str(i),'))'])
        eval(['set (archandles(',num2str(i),'), ''Xdata'', x + arc_x(',num2str(i),',:) * cos(th) - arc_y(',num2str(i),',:) * sin(th));']);
        eval(['set (archandles(',num2str(i),'), ''Ydata'', y + arc_x(',num2str(i),',:) * sin(th) + arc_y(',num2str(i),',:) * cos(th));']);
        if ((i == select ) && (select >= 0)) 
            eval(['set (archandles(',num2str(i),'), ''color'', ''r'');']);
        else
            eval(['set (archandles(',num2str(i),'), ''color'', ''b'');']);
        end
    end
end

% drawnow


