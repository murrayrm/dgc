function gen_rddf_path( varargin );
% 
% function gen_rddf_path( waypoint_file )
%
% Changes: 
%   09/04/04, Lars Cremean, created, loosely based on plot_corridor.m  
%
% Function:
%   This function reads a Bob format waypoint specification and writes a 
%   trajectory file that contains a once differentiable path specified by
%   a list of (x,y) points and the first and second derivatives of these
%   variables.  The generated path is written to a file, rddfpath.dat
%
% Usage example:
%   plot_corridor('waypoints.bob')
%   The waypoints file must be in Bob format!
%

hold on;

% just for fun, output the total distance along the course
td = 0;

% set the defaults for the optional arguments 

if( nargin < 1 )
	error('Need an argument (waypoint_file)');
end
if( nargin >= 1 )
	
	waypoint_file = varargin{1};
        
	% Handle waypoints file.
	% Can't use 'load' command with Bob format.
	[type,easting,northing,radius,speed] = ...
        textread( waypoint_file, '%s%f%f%f%f', 'commentstyle', 'shell');

	% waypoints array has columns {easting, northing}
	waypoints(:,1) = easting;
	waypoints(:,2) = northing;

        % return the original waypoints
	original = [easting northing];
end
if( nargin >= 2 )
	offset = varargin{2};
    e_offset = offset(1);
	n_offset = offset(2);
else
	e_offset = easting(1);
	n_offset = waypoints(1,2);
end
if( nargin >= 3 )
	error('Too many arguments!');
end
   
% format of waypoint_file
% easting northing GRRR
% make all the measurements relative to first waypoint
% waypoints(:,1) = waypoints(:,1) - e_offset;
% waypoints(:,2) = waypoints(:,2) - n_offset;

% parameters
RDDF_MAX_SPEED         = 10;   % m/s
RDDF_MAX_ACCEL         = 3;    % m/s/s
RDDF_MAX_DECEL         = -1.5; % m/s/s
RDDF_VEH_WIDTH         = 2.0;  % m 
RDDF_MAX_LATERAL_ACCEL = 2.0;  % m/s/s
RDDF_POINT_SPACING     = 0.2;  % m
VEHICLE_WHEELBASE      = 3;    % m
USE_MAX_STEER          = 0.35; % rad
VEHICLE_MIN_RADIUS     = VEHICLE_WHEELBASE / sin(USE_MAX_STEER);
VEHICLE_MAX_RADIUS     = 100;  % m, treat circles bigger than this as straight lines

% output file initialization
outfilename = 'temp.out';
fid = fopen(outfilename, 'w');
fprintf(fid, '%% This is an output file from gen_rddf_path.  This is a minimum-time\n');
fprintf(fid, '%% path based on the RDDF named %s.\n', waypoint_file);
fprintf(fid, '%% The parameters used for generating this path were\n');
fprintf(fid, '%%  RDDF_MAX_SPEED         = %4.2f;   %% m/s\n', RDDF_MAX_SPEED);
fprintf(fid, '%%  RDDF_MAX_ACCEL         = %4.2f;    %% m/s/s\n', RDDF_MAX_ACCEL);
fprintf(fid, '%%  RDDF_MAX_DECEL         = %4.2f; %% m/s/s\n', RDDF_MAX_DECEL);
fprintf(fid, '%%  RDDF_VEH_WIDTH         = %4.2f;  %% m\n', RDDF_VEH_WIDTH);
fprintf(fid, '%%  RDDF_MAX_LATERAL_ACCEL = %4.2f; %% m/s/s\n', RDDF_MAX_LATERAL_ACCEL);
fprintf(fid, '%%  RDDF_POINT_SPACING     = %4.2f;    %% m\n', RDDF_POINT_SPACING);
fprintf(fid, '%%  VEHICLE_WHEELBASE      = %4.2f;    %% m\n', VEHICLE_WHEELBASE);
fprintf(fid, '%%  USE_MAX_STEER          = %4.2f; %% rad\n', USE_MAX_STEER);
fprintf(fid, '%%  VEHICLE_MIN_RADIUS     = VEHICLE_WHEELBASE / sin(USE_MAX_STEER);\n');
fprintf(fid, '%%  VEHICLE_MAX_RADIUS     = %4.2f;  %% m\n', VEHICLE_MAX_RADIUS);
fprintf(fid, '%%  \n');
fprintf(fid, '%%  The column format of this file is {N, Ndot, Nddot, E, Edot, Eddot}\n');
fprintf(fid, '%%  -> All units are in SI (m, m/s, m/s/s)\n');

% TODO: Handle waypoint 1 separately
EoT(1,:) = waypoints(1,:);
trackAng(1) = atan2(easting(2)-easting(1), northing(2)-northing(1));

turnSpd(1) = 0.1;
pointSpd = turnSpd(1);

% Fake one waypoint after the last
tem1 = length(waypoints);
tem2 = atan2(easting(tem1)-easting(tem1-1), northing(tem1)-northing(tem1-1));
fakeLast = [waypoints(tem1,1)+cos(tem2) waypoints(tem1,2)+sin(tem2)];
waypoints = [waypoints; fakeLast];
easting = [easting; fakeLast(1)];
northing = [northing; fakeLast(2)];

% Get all of the waypoint turn radii and speeds first, in order to modify
% them so that transitioning between the turn radii can be made feasible
for i=2:length(waypoints)-1

    lastWaypoint = (i == length(waypoints)-1);

    % get the trackline angle (Yaw) for this waypoint (line to the next)
    trackAng(i) = atan2(easting(i+1)-easting(i), northing(i+1)-northing(i));
    
    distPrev(i) = norm(waypoints(i,:) - waypoints(i-1,:));
    distNext(i) = norm(waypoints(i+1,:) - waypoints(i,:));
    
    % calculate the turn radius of the next waypoint
    delta(i) = trackAng(i) - trackAng(i-1);
    % normalize to be between -pi and pi
    delta(i) = atan2( sin(delta(i)), cos(delta(i)) );
    
    % calculate the maximum turning radius
    turnRad(i) = max( (min(radius(i-1),radius(i)) - RDDF_VEH_WIDTH/2) * ...
        cos(abs(delta(i))/2) / (1 - cos(abs(delta(i))/2)), VEHICLE_MIN_RADIUS);
    
    % saturate the turning radius to be not too big
    turnRad(i) = min(turnRad(i), VEHICLE_MAX_RADIUS);
    
    % calculate the turning speed     
    turnSpd(i) = sqrt( turnRad(i) * RDDF_MAX_LATERAL_ACCEL );
    % saturate it with three values
    turnSpd(i) = min(min(min(turnSpd(i), RDDF_MAX_SPEED), speed(i-1)), speed(i));    
    
    % calculate the point on the trackline that marks the beginning-of-turn
    % bTurnFrac(i) is the fraction of the distance from waypoint i to
    % waypoint i-1 at which we should begin the turn
    %bTurnFrac(i) = min(max(turnRad(i)*tan(delta(i)/2), 0),
    %distPrev(i))/distPrev(i)
    bTurnFrac(i) = turnRad(i)*tan(abs(delta(i))/2)/distPrev(i);
    if( lastWaypoint ) bTurnFrac(i) = 0; end
    BoT(i,:) = waypoints(i,:) + bTurnFrac(i) * (waypoints(i-1,:) - waypoints(i,:));

    % calculate the point on the trackline that marks the end-of-turn
    % eTurnFrac(i) is the fraction of the distance from waypoint i to
    % waypoint i+1 at which we should end the turn
    %eTurnFrac(i) = min(max(turnRad(i)*tan(delta(i)/2), 0), distNext(i))/distNext(i)
    eTurnFrac(i) = turnRad(i)*tan(abs(delta(i))/2)/distNext(i);
    if( lastWaypoint ) eTurnFrac(i) = 1; end
    EoT(i,:) = waypoints(i,:) + eTurnFrac(i) * (waypoints(i+1,:) - waypoints(i,:));
        
end

% make sure we stop at the last actual waypoint
turnSpd(length(waypoints)-1) = 0;

% now iterate backward through the waypoints and make sure that the 
% successive speeds do not violate the maximum deceleration constraint of
% the vehicle
for i=length(waypoints)-2:-1:1

    % make sure that we can decelerate from waypoint i to i+1
    % calculate the distance between the end of turn for waypoint i and the
    % beginning of the turn for waypoint i+1 (saturate at zero if they overlap)
    tem = max( min(1-bTurnFrac(i+1),1) - max(eTurnFrac(i),0), 0);

    turnSpd(i) = min(turnSpd(i), turnSpd(i+1) - RDDF_MAX_DECEL * tem);
    
end

% now compute the points along the trajectory according to the given 
% path parameters
for i=2:length(waypoints)-1
    
    lastWaypoint = (i == length(waypoints)-1);

    % calculate the center of the tracking circle for waypoint i
    tempVec = (waypoints(i,:) - waypoints(i-1,:))/distPrev(i);
    perpVec = sign(delta(i))*[tempVec(2) -tempVec(1)];
    cirCent(i,:) = BoT(i,:) + perpVec * turnRad(i);
    
    % calculate the start-of-turn angle
    tempVec = BoT(i,:) - cirCent(i,:); 
    startAngle = atan2( tempVec(2), tempVec(1) );
    
    % calculate the end-of-turn angle
    tempVec = EoT(i,:) - cirCent(i,:); 
    endAngle = atan2( tempVec(2), tempVec(1) );
    
    sweep = atan2(sin(endAngle - startAngle), cos(endAngle - startAngle));

    if(delta(i) > 0) 
        turnAngles = [startAngle:-RDDF_POINT_SPACING/turnRad(i):startAngle+sweep]; 
    else
        turnAngles = [startAngle:RDDF_POINT_SPACING/turnRad(i):startAngle+sweep];
    end
    
    % DEBUG
    h1 = plot(cirCent(i,1) + turnRad(i)*cos(turnAngles) - e_offset, ...
        cirCent(i,2) + turnRad(i)*sin(turnAngles) - n_offset, 'b-');
    h2 = plot(BoT(i,1) - e_offset, BoT(i,2) - n_offset, 'g*');
    h3 = plot(EoT(i,1) - e_offset, EoT(i,2) - n_offset, 'rx');
    h4 = plot([EoT(i-1,1) BoT(i,1)]-e_offset, ...
                    [EoT(i-1,2) BoT(i,2)]-n_offset,'m-');
%     pause
%     delete(h1); delete(h2); delete(h3);
    
    % calculate points along straight section between the previous waypoint's
    % end-of-turn line... to the current waypoint's brake line... to the 
    % current waypoint's beginning-of-turn line, and write them to file
    
    % determine the fractions of the distance from the previous waypoint to
    % this waypoint which represent the end-of-turn line for the previous
    % waypoint and the beginning-of-turn line for the next waypoint

    % save the previous pointSpd that corresponds to the speed at the end
    % of the last waypoint traversal (for continuity of the speed profile)
    % overwrite the turn speed for the previous waypoint to be the last
    % speed actually computed for the turn
    turnSpd(i-1) = pointSpd;
    
    for j = max(eTurnFrac(i-1),0):RDDF_POINT_SPACING/distPrev(i):min(1-bTurnFrac(i),1)
        addPoint = waypoints(i-1,:) + j * (waypoints(i,:) - waypoints(i-1,:));
        
        % determine the first and second derivatives for each of these
        % points by fitting a desired speed profile
        spdAccel = 0.5*(turnSpd(i-1) + sqrt(turnSpd(i-1)^2 + 4 * RDDF_MAX_ACCEL * j));
        spdDecel = 0.5*(turnSpd(i) + sqrt(turnSpd(i)^2 - 4 * RDDF_MAX_DECEL * (1 - j)));
        % These equations were actually buggy; assume j is time rather than
        % distance along line between end-of-turn and beginning-of-turn
        %         spdAccel = turnSpd(i-1) + RDDF_MAX_ACCEL * j;
        %         spdDecel = turnSpd(i) + RDDF_MAX_DECEL * (1 - j);
        pointSpd = min(min(speed(i), spdAccel),spdDecel);

        % first derivatives (easting then northing)
        addPoint1(1) = pointSpd * cos(pi/2 - trackAng(i-1));
        addPoint1(2) = pointSpd * sin(pi/2 - trackAng(i-1));
        
        % second derivatives        
        if( pointSpd == speed(i) ) % we've hit the speed limit
            addPoint2(1) = 0;
            addPoint2(2) = 0;
        elseif( pointSpd == spdAccel ) % we should accelerate
            addPoint2(1) = RDDF_MAX_ACCEL * cos(pi/2 - trackAng(i-1));
            addPoint2(2) = RDDF_MAX_ACCEL * sin(pi/2 - trackAng(i-1));
        elseif( pointSpd == spdDecel ) % we should decelerate
            addPoint2(1) = RDDF_MAX_DECEL * cos(pi/2 - trackAng(i-1));
            addPoint2(2) = RDDF_MAX_DECEL * sin(pi/2 - trackAng(i-1));
        else
            error('Something is terribly wrong.')
        end
            
        % print to file
        fprintf(fid, '%.6f %.6f %.6f %.6f %.6f %.6f\n', ...
            addPoint(2), addPoint1(2), addPoint2(2), ...
            addPoint(1), addPoint1(1), addPoint2(1) );
        
        % DEBUG
        plot(addPoint(1)-e_offset, addPoint(2)-n_offset, 'r.');
        line(addPoint(1)+[0 addPoint1(1)]-e_offset, ...
                        addPoint(2)+[0 addPoint1(2)]-n_offset)
        line(addPoint(1)+[0 addPoint2(1)]-e_offset, ...
                        addPoint(2)+[0 addPoint2(2)]-n_offset)
    end
    
    % calculate points along curved section between beginning-of-turn 
    % line and end-of-turn line and write them to file
    for j = turnAngles
        addPoint = cirCent(i,:) + turnRad(i) * [cos(j) sin(j)];
        
        % determine the first and second derivatives for each of these
        % points according to the turnSpd(i)
         
        % first derivatives
        tempVec = (addPoint - cirCent(i,:))/norm(addPoint - cirCent(i,:)); 
        perpVec = sign(delta(i))*[tempVec(2) -tempVec(1)];
        addPoint1(1) = turnSpd(i) * perpVec(1);
        addPoint1(2) = turnSpd(i) * perpVec(2);

        % second derivatives
        tempAccel = turnSpd(i)^2 / turnRad(i);
        addPoint2(1) = -tempAccel * tempVec(1);
        addPoint2(2) = -tempAccel * tempVec(2);
        
        % print to file
        fprintf(fid, '%.6f %.6f %.6f %.6f %.6f %.6f\n', ...
            addPoint(2), addPoint1(2), addPoint2(2), ...
            addPoint(1), addPoint1(1), addPoint2(1) );
        
        % DEBUG
        plot(addPoint(1)-e_offset, addPoint(2)-n_offset, 'b.');
        line(addPoint(1)+[0 addPoint1(1)]-e_offset, ...
                        addPoint(2)+[0 addPoint1(2)]-n_offset)
        line(addPoint(1)+[0 addPoint2(1)]-e_offset, ...
                        addPoint(2)+[0 addPoint2(2)]-n_offset)
        
    end
    
     
    % increment the total distance of the course
    % (sum of track line distances)
    td = td + sqrt( (waypoints(i+1,1)-waypoints(i,1))^2 + ...
                    (waypoints(i+1,2)-waypoints(i,2))^2 );
end

fclose(fid)

%axis tight
% axis equal
% hold on
% grid on
% figure(1)

% output the total distance
td
