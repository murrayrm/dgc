function view_genmap_sequence( name, from, to )
%
% function view_genmap_sequence( name, from, to )
%
% Changes:
%
%   2004/02/24, Lars Cremean, created
%
% Function:
%
%   This function takes the first part of the filename for a sequence of
%   saved genMap files and displays them in sequence in animation.  It is
%   assumed that the MATFiles are named in a continous sequence, e.g.
%   genMap1.m, genMap2.m, ...
%
% Inputs:
%
%   name: string of the first part of the saved MATFile filenames
%   from: integer that is the first number of the sequence
%   to  : integer that is the last number of the sequence
%
% Usage:
%
%   view_genmap_sequence( 'CAEgenMap', 0, 20 )
%

clear plot_genmap_2d

for i=from:to

  str = [name num2str(i)];
  plot_genmap_2d(str);
  title(str);
  axis tight
  drawnow

end
