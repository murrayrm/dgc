function [ fps ] = calc_average_fps( state_filename )
% calc_average_fps   Calculates the average fps from a stereo vision log
% 
% This script parses the .state file given as argument and outputs the 
% average fps so you can make a movie that's realistic.
%
% Author: Henrik Kjellander
% Created: 7 Oct 2004

% Read the file
[nbr, time] = textread( state_filename,'%d: %f%*[^\n]','delimiter',' ');

% Normalize the time column
time(:,1) = time(:,1) - time(1,1);

% Calc number of images (rows):
nbr_images = length(time);

% Calculate fps
fps = nbr_images / time(nbr_images);

