% Lars Cremean
% 15 July 04
% 
% This is a support script for creating sequences of map images 
% from genMaps created by createLadarMaps.  See 
% http://grandchallenge.caltech.edu/test_logs_2003-2004/20040313_slashx/ladar/analysis/README
% if you want clues on how this script might be useful.

off = [3846508.753 499015.283];

% startline is the starting line number for the state ladar data
% (not including the comments in the header of the log)
% you should change this to be the correct value, otherwise
% the vehicle will not be drawn properly in the figures
startline = 94; %2304;

% you'll need to change this if you're processing a different file
% this file will be processed to obtain the state information of the vehicle
% for each scan.  It assumes that the maps being processed contain ALL OF THE 
% SCANS in given interval.
display(' Loading state information...')
%state = load('race_state_03132004_063108.log');
state = load('cropped_race_state_03132004_063108.log');
display( '... done.')

for i=1:99

  clf
  %str = ['race' num2str(i,'%05d')]
  str = ['racepr' num2str(i,'%05d')]
  plot_genmap_2d(str,off);
  draw_vehicle('str',state(startline+i-1,2)-off(2), ...
		  state(startline+i-1,3)-off(1), ...
		  state(startline+i-1,12), 0, 'k' )
  axis manual % freeze the axis limits
  plot_corridor('cropped_DARPA_RACE_RDDF_ZERO_BASED.bob')
  clear plot_genmap_2d draw_vehicle
  eval(['print -r100 -dtiff images/' str '.tif'])
  %pause


end
