% LG040220
% Script used by MatlabDisplay.cc 
% to initialize the figure that plots
% all the votes that the arbiter receives

VOTES_FIGURE = 101;

NUM_VOTERS = 7;
NUM_ARCS = 25;
% NOTE: I don't think there's a way around having to hard
%       code the names of all the voters, so I might as well
%       do it here (below, in the legend() command), and 
%       correspondingly define NUM_VOTERS above)

% Let's let  whatever calls this decide what axis handle to put it in. -Lars
%figure(VOTES_FIGURE); clf; 

% I'm changing the number of subplot columns to include the plot of
% the path of the vehicle into this figure -Lars
subplot(4,4,[1 2]); hold on;
h_voters_goodness = plot(zeros(NUM_ARCS, NUM_VOTERS+1)); % setup handles, one per voter
h_combined_goodness = plot(zeros(NUM_ARCS, 1));
% h_best_goodness = plot(0,0,'ks');
title('GOODNESS')
axis([1 NUM_ARCS 0 102])
grid on

% I'm changing the number of subplot columns and index of the plot to include 
% the plot of the path of the vehicle into this figure
subplot(4,4,[5 6]); hold on;
h_voters_velocity = plot(zeros(NUM_ARCS, NUM_VOTERS+1));
h_combined_velocity = plot(zeros(NUM_ARCS, 1));
% h_best_velocity = plot(0,0,'ks');
title('VELOCITY')
axis([1 NUM_ARCS 0 10])
grid on

set(h_combined_goodness(end),'LineWidth',2);
set(h_combined_velocity(end),'LineWidth',2);

set(h_combined_goodness(end),'LineStyle','--');
set(h_combined_velocity(end),'LineStyle','--');

subplot(4,4,[1 2])
legend([h_combined_goodness; h_voters_goodness], 'Combined','LADAR', 'Stereo', 'StereoLR', 'Global', 'DFE', 'CAE');

subplot(4,4,[5 6])
legend([h_combined_velocity; h_voters_velocity], 'Combined','LADAR', 'Stereo', 'StereoLR', 'Global', 'DFE', 'CAE');
