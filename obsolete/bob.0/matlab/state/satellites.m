% plots the gps easting/northing with a change in color whenever sats used
% changes
% random comment to show yinan cvs
% get gps sats
% make sure length of arrays are same length in case we had to lop off the
% beginning for kf convergence
gpssats = gpslog(:,16);
dl = length(gpssats) - length(gpsnorthing) + 1;
gpssats = gpssats(dl:end);

color = 1;
figure;
hold on;
for i = 2:length(gpseasting)
    if (gpssats(i) ~= gpssats(i-1))
        color = color + 1;
    end
    
    color = mod(color, 2);
    
    if color == 1
        plot(gpseasting(i), gpsnorthing(i), 'r.')
    else
        plot(gpseasting(i), gpsnorthing(i), 'b.')
    end
end
        

