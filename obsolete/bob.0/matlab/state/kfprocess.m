% kfprocess
% Reads in raw gps data and filtered state data and plots them

% constant definitions
qper = 4; % draw heading vectors every this many points

% set the origin to the initial position of the vehicle
originN = kfnorthing(1);
originE = kfeasting(1);
kfnorthing = kfnorthing - originN;
kfeasting = kfeasting - originE;

% generate x, y vectors for headings
kfheadingx = sin(kfheading);
kfheadingy = cos(kfheading);

% plot kf points
figure
hold on
grid on
plot(kfeasting, kfnorthing, 'ob')
plot(kfeasting, kfnorthing, 'r')

% plot heading points
%figure
%hold on
%plot(kfeasting, kfnorthing, 'ob')
%quiver(kfeasting(1:qper:end), kfnorthing(1:qper:end), kfheadingx(1:qper:end), kfheadingy(1:qper:end), 'k-');

% convert gpsmodes to usable form
gpsmode = gpsmoderaw;
for i=1:length(gpsmode)
    if (gpsmode(i) < 0) % negative modes are gps valid, do not operate on invalid
        gpsmode(i) = gpsmode(i) + 256;      % make all integers positive
        gpsmode(i) = bitand(gpsmode(i), 3); % pick out the relevant bits
    else
        gpsmode(i) = 99; % set this so that we know they weren't valid
    end
end

% set origin to initial position
gpsnorthing = gpsnorthing- originN;
gpseasting = gpseasting - originE;

%plot raw gps data
figure
hold on
grid on
i = find(gpsmode == 0);
plot(gpseasting(i), gpsnorthing(i), 'ro');
i = find(gpsmode == 1);
plot(gpseasting(i), gpsnorthing(i), 'go');
i = find(gpsmode == 2);
plot(gpseasting(i), gpsnorthing(i), 'co')
i = find(gpsmode == 3);
plot(gpseasting(i), gpsnorthing(i), 'yo')
plot(gpseasting, gpsnorthing, 'k')

        
% plot superimposed plots
figure
hold on
grid on
i = find(gpsmode == 0);
plot(gpseasting(i), gpsnorthing(i), 'ro');
i = find(gpsmode == 1);
plot(gpseasting(i), gpsnorthing(i), 'go');
i = find(gpsmode == 2);
plot(gpseasting(i), gpsnorthing(i), 'co')
i = find(gpsmode == 3);
plot(gpseasting(i), gpsnorthing(i), 'yo')
plot(gpseasting, gpsnorthing, 'k')
plot(kfeasting, kfnorthing, 'ob')
plot(kfeasting, kfnorthing, 'r')
xlabel('Easting')
ylabel('Northing')
