!K
!S

;Gemini GV6 Servo Drive Setup

;Motor Setup
DMTR 1863		;Motor ID (BE344LJ)
DMTIC 9.37		;Continuous Current (Amps-RMS)
DMTICD 38.30		;Continuous Current Derating (% derating at rated speed)
DMTKE 40.8		;Motor Ke (Volts (0-to-peak)/krpm)
DMTRES 0.49		;Motor Winding Resistance (Ohm)
DMTJ 90.380		;Motor Rotor Inertia (kg*m*m*10e-6)
DPOLE 4		;Number of Motor Pole Pairs
DMTW 50.0		;Motor Rated Speed (rev/sec)
DMTIP 28.11		;Peak Current (Amps-RMS)
DMTLMN 6.8		;Minimum Motor Inductance (mH)
DMTLMX 7.8		;Maximum Motor Inductance (mH)
DMTD 0.000135	;Motor Damping (Nm/rad/sec)
DMTRWC 0.17		;Motor Thermal Resistance (degrees Celsius/Watt)
DMTTCM 33.3		;Motor Thermal Time Constant (minutes)
DMTTCW 1.20		;Motor Winding Time Constant (minutes)
DMTAMB 40.00	;Motor Ambient Temperature (degrees Celsius)
DMTMAX 125.00	;Maximum Motor Winding Temperature (degrees Celsius)
DHALL  1		;Disable Hall Sensor Checking
DMTLQS  0		;Set Q Axis Inductance Saturation
DMTLDS  0		;Set D Axis Inductance Saturation
DTHERM  0		;Disable motor thermal switch input


;Drive Setup
DMODE 12		;Drive Control Mode
DPWM 8		;Drive PWM Frequency (kHz)
SFB 1			;Encoder Feedback
ERES 8000		;Encoder Resolution (counts/rev)
ORES 8000		;Encoder Output Resolution (counts/rev)
DMEPIT 0.00		;Electrical Pitch (mm)
SHALL 0		;Invert Hall Sensors
DMTLIM 13.4		;Torque Limit (Nm)
DMTSCL 13.4		;Torque Scaling (Nm)
DMVLIM 50.000000	;Velocity Limit (rev/sec)

;Load Setup
LJRAT 0.0		;Load-to-Rotor Inertia Ratio
LDAMP 0.0000	;Load Damping (Nm/rad/sec)

;Fault Setup
FLTDSB 1		;Fault on Drive Disable Enable
KDRIVE 0		;Disable Drive on Kill
SMPER 8000		;Maximum Allowable Position Error (counts)
SMVER 0.000000	;Maximum Allowable Velocity Error (rev/sec)
DIFOLD 0		;Current Foldback Enable

;Digital Input Setup
INFNC 1-R		;Positive End-of-Travel Input (R)
INFNC 2-S		;Negative End-of-Travel Input (S)
INFNC 3-T		;Home Limit Input (T)
INFNC 4-H		;Trigger Interrupt Input (H)
INFNC 5-A		;General Purpose Input (A)
INFNC 6-A		;General Purpose Input (A)
INFNC 7-A		;General Purpose Input (A)
INFNC 8-A		;General Purpose Input (A)
INLVL 11000000	;Input Active Level
INDEB 50		;Input Debounce Time (milliseconds)
INUFD 0		;Input User Fault Delay Time (milliseconds)
LH 0			;Hardware EOT Limits Enable
LHAD 100.0000	;Hard Limit Deceleration
LHADA 100.0000	;Hard Limit Average Deceleration

;Digital Output Setup
OUTBD 0		;Output Brake Delay Time (milliseconds)
OUTFNC 1-A		;General Purpose Output (A)
OUTFNC 2-F		;Fault Detected Output (F)
OUTFNC 3-D		;End-of-Travel Limit Hit Output (D)
OUTFNC 4-G		;Position Error Output - GV6 Only(G)
OUTFNC 5-B		;Moving/Not Moving Output (B)
OUTFNC 6-A		;General Purpose Output (A)
OUTFNC 7-F		;Fault Detected Output (F)
OUTLVL 0000000	;Output Active Level

;Analog Monitor Setup
DMONAV 0		;Analog Monitor A Variable
DMONAS 100		;Analog Monitor A Scaling (% of full scale output)
DMONBV 0		;Analog Monitor B Variable
DMONBS 100		;Analog Monitor B Scaling (% of full scale ouput)

;Servo Tuning
DIBW 500		;Current Loop Bandwidth (Hz)
DVBW 50		;Velocity Loop Bandwidth (Hz)
DPBW 20.00		;Position Loop Bandwidth (Hz)
SGPSIG 1.000	;Velocity/Position Bandwidth Ratio
SGIRAT 1.000	;Current Damping Ratio
SGVRAT 1.000	;Velocity Damping Ratio
SGPRAT 1.000	;Position Damping Ratio
DNOTAF 0		;Notch Filter A Frequency (Hz)
DNOTAQ 1.0		;Notch Filter A Quality Factor
DNOTAD 0.0000	;Notch Filter A Depth
DNOTBF 0		;Notch Filter B Frequency (Hz)
DNOTBQ 1.0		;Notch Filter B Quality Factor
DNOTBD 0.0000	;Notch Filter B Depth
DNOTLG 0		;Notch Lag Filter Break Frequency (Hz)
DNOTLD 0		;Notch Lead Filter Break Frequency (Hz)
SGINTE 1		;Integrator Option
SGVF 100		;Velocity Feedforward Gain (%)
SGAF 100		;Acceleration Feedforward Gain (%)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; The following not part of config wizard
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Homing...
HOMDF1          ;Neg direction for final homing.
HOMEDG0         ;Use the positive reference edge.
HOMBAC1         ;Enable Backup.
HOMV2           ;Home velocity.
HOMVF0.5        ;Home final velocity.

; Limit Settings.
LHAD4000
LSAD4000
