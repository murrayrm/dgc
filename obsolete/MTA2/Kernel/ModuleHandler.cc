#include "ModuleHandler.hh"


ModuleHandler::ModuleHandler(shared_ptr<DGC_MODULE> dm, int modnum) {
  p_dm = dm; 
  M_ModuleNumber = modnum;
  p_dm->SetModuleNumber(modnum);
}

void ModuleHandler::InMail(Mail & ml) {
  boost::recursive_mutex::scoped_lock sl(MailMutex);
  p_dm -> InMailHandler(ml);
}

Mail ModuleHandler::QueryMail(Mail & ml) {
  boost::recursive_mutex::scoped_lock sl(MailMutex);
  return (p_dm -> QueryMailHandler(ml));
}

int ModuleHandler::ModuleNumber() {
  return M_ModuleNumber;
}
