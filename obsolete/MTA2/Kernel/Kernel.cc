#include "Kernel.hh"


#include <iostream>
using namespace std;

extern DGC_MODULE DGCM;
extern KERNEL::SystemMap KD_SystemMap;

boost::thread_group KD_ThreadGroup;

void SignalHandler(int signal) {


} 

//Kernel::Kernel(): KD_SystemMap("") {
Kernel::Kernel(){
  KD_ShutdownFlag = false;
  KD_ActiveFlag = false;

  KD_ModulesCounter = 0;
}

Kernel::~Kernel() {
}

//void Kernel::Register(shared_ptr<DGC_MODULE> dm) {

//  boost::recursive_mutex::scoped_lock sl(KD_ModulesCounterLock);
//  KD_ModulesCounter ++;

//  int x = (KD_ModulesCounter);
  
//  cout << "Registering New Module (" << (*dm).ModuleName()  
//       << ") of type =" << (*dm).ModuleType() << ", given Mailbox = " << x << endl;
  
//  KD_Modules.push_back( shared_ptr<ModuleHandler> (new ModuleHandler(dm, x)));

//}
void Kernel::Register() {
  KD_ModulesCounter ++;
  int x = (KD_ModulesCounter);
  
  cout << "Registering New Module (" << DGCM.ModuleName()  
       << ") of type =" << DGCM.ModuleType() << ", given Mailbox = " << x << endl;
  
  DGCM.SetModuleNumber(x);
  //  KD_Modules.push_back( shared_ptr<ModuleHandler> (new ModuleHandler(dm, x)));

}

void testfunc() {}

void Kernel::Start() {
  // Setup catching of signals

  cout << "Kernel Starting " << endl;

  KD_ThreadGroup.create_thread(KD_InMessageAcceptor);
  //  KD_ThreadGroup.create_thread(KD_SystemMap);
  KD_ThreadGroup.create_thread(SYS_MAP_STARTER);
  KD_ThreadGroup.create_thread(KD_Time);
  
  // sleep for a second before starting the modules
  sleep(1);
  // now start all modules.
  //  vector<shared_ptr<ModuleHandler> >::iterator x    = KD_Modules.begin();
  //  vector<shared_ptr<ModuleHandler> >::iterator stop = KD_Modules.end();
  //  while( x != stop ) {
  //    DGC_MODULE_STARTER dms( (*(*x)).p_dm);
    //    cout << "Starting " << (*().ModuleName() << endl;
  //    KD_ThreadGroup.create_thread( dms );
  //    x++;
  //  }
  KD_ThreadGroup.create_thread( DGC_MODULE_STARTER );

  

  // Wait till we are ready to shut down
  while(!ShuttingDown()) {
    sleep(1);
  }
  // Once we are ready, we must release the server 
  // socket by issuing a local shutdown command.

  //  ...code goes here...
  
  
  KD_ThreadGroup.join_all();
}


Timeval Kernel::Now() {
  return KD_Time.Now();
}


TCP_ADDRESS Kernel::MyTCPAddress() {
  return KD_SystemMap.LocalAddress();
}

MailAddress Kernel::FindAddress(int type) {
  return KD_SystemMap.FindAddress(type);
}

int Kernel::ShuttingDown() {
  return KD_ShutdownFlag;
}

int Kernel::Active() {
  return KD_ActiveFlag;
}

void Kernel::Activate() {
  if( ! ShuttingDown() ) {
    KD_ActiveFlag = true;
  }
}

void Kernel::ForceShutdown() {
  KD_ShutdownFlag = true;
  KD_ActiveFlag = false;
}


void Kernel::SendMail(Mail& ml, File f) {
  // if we dont have a socket ready, enqueue it for delivery
  //  if( f.FD() < 0) {
  //    KD_OutMail.push_back(ml);
  //    cout << "Adding mail to send queue" << endl;
  //  } else {

    // just deliver the mail
    DeliverMail(ml, f);
    //  }
}

list<ModuleInfo> Kernel::LocalModules() {
  return KD_SystemMap.LocalModuleTypes();
}

list<ModuleInfo> Kernel::AllModules() {
  return KD_SystemMap.AllModuleTypes();
}

void Kernel::PrintModuleList(list<ModuleInfo> l) {

  list<ModuleInfo>::iterator x    = l.begin();
  list<ModuleInfo>::iterator stop = l.end();

  cout << endl;
  cout << "Listing Module Set..." << endl;
  while ( x != stop) {
    ModuleInfo mi = (*x);
    cout << "IP["    << mi.MA.TCP.IP       << "] "
         << "PORT["  << mi.MA.TCP.PORT     << "] "
	 << "MBOX["  << mi.MA.MAILBOX      << "] "
	 << "TYPE["  << mi.ModuleType      << "]." << endl;
    x++;
  }
  cout << endl;
}

void Kernel::DeliverMail(Mail &ml, File f) {
  if(ml.To().TCP.IP == 0) {
    cout << "Bad Mail Destination" << endl;
    return;
  }

  // if fd<0 open a socket
  if( f.FD() < 0) {
    f = CSocket (ml.To().TCP.IP, ml.To().TCP.PORT);
  }

  // if still < 0 just give up
  if( f.FD() < 0) {
    cout << "Socket Open Error - trying to connect to " 
	 << ml.To().TCP.IP << ":" << ml.To().TCP.PORT << endl;
    return;
  }
  
  //  cout << "Sending Mail To " << ml.To().TCP.IP
  //       << ":" << ml.To().TCP.PORT 
  //       << "[" << ml.To().MAILBOX << "]." << endl;

  // send the key to verify the message
  int myKey = DEF::ReceiveKey;
  f.BWrite((void*) &myKey, sizeof(myKey));
  ml.ToFile(f);

}

Mail Kernel::SendQuery(Mail& ml, File f) {
  File mySocket;
  if(f.FD() < 0) { 
    mySocket = CSocket(ml.To().TCP.IP, ml.To().TCP.PORT);
  } else {
    mySocket = f;
  }

  
  //  cout << "Sending Query Mail To " << ml.To().TCP.IP
  //       << ":" << ml.To().TCP.PORT << endl;

  if(mySocket.FD() < 0) {
    // error opening socket 
    // just return a "bad" message
    return Mail(ml.To(), ml.From(), ml.MsgType(), MailFlags::BadMessage); // reverse
  } else {

    // send the key to verify the message
    int myKey = DEF::ReceiveKey;
    mySocket.BWrite((void*) &myKey, sizeof(myKey));
    ml.ToFile(mySocket);
    
    // now read back the response
    
    // read the key to verify the message
    myKey = 0;
    mySocket.BRead((void*) &myKey, sizeof(myKey));
    //        cout << "SendQuery myKey = " << myKey << endl;
    if( myKey != DEF::ReceiveKey ) {
      // error - bad message
      // just return a "bad" message
      return Mail(ml.To(), ml.From(), ml.MsgType(), MailFlags::BadMessage); // reverse
    } else {
      // let the mailer download the message
      //      cout << "Response Of Query ..." ;
      return Mail(mySocket);
    }
  }
}
  


Mail Kernel::QueryMailHandler(Mail& ml) {
  //  cout << "Query Mail Arrived of type " << ml.MsgType() << endl;
  int size = ml.Size();

  switch( ml.MsgType() ) {

  case KernelMessages::ECHO: 
    // download the message, and send it back
    if (true) {
      char *p = new char[ size ];
      ml.DequeueFrom( (void*) p, size );
      Mail reply = Mail(ml.To(), ml.From(), ml.MsgType(), MailFlags::QueryMessage); // reverse
      reply.QueueTo((void*) p, size);
      delete p;
      return reply;
    }
    break;

  case KernelMessages::MODULEINFO:
    // nothing to download just return
    if( true ) {
      list<ModuleInfo> r = LocalModules();
      //      cout << "Kernel QueryHandler - Num Local Modules = " << r.size() << endl;
      list<ModuleInfo>::iterator x = r.begin();
      list<ModuleInfo>::iterator stop = r.end();
      Mail reply = Mail(ml.To(), ml.From(), ml.MsgType(), MailFlags::QueryMessage); // reverse
      while( x != stop) {
	reply.QueueTo( (void*) &(*x), sizeof(ModuleInfo));
	x++;
      }
      return reply;
    }
    break;

  default:
    cout << "Kernel: Unknown Query Message Received" << endl;
  }

  return Mail(ml.To(), ml.From(), 1, MailFlags::BadMessage); //  reverse
}

void Kernel::InMailHandler(Mail& ml) {
    cout << "In Mail Arrived of type = " << ml.MsgType() << endl;
  int size = ml.Size();

  switch( ml.MsgType() ) {
  case KernelMessages::ECHO:
    // download the message, and send it back
    if (true) {
      char *p = new char[ size ];
      ml.DequeueFrom( (void*) p, size );
      Mail reply = Mail(ml.To(), ml.From(), ml.MsgType(), 0); // reverse
      reply.QueueTo((void*) p, size);
      delete p;
      SendMail(reply);
    }
  case KernelMessages::SHUTDOWN: 
    cout << "Shutdown Message Received" << endl;
    ForceShutdown();
    break;

  default:
    cout << "Kernel: Unknown In Message Received" << endl;
  }

}


