#include "DGC_MODULE.hh"
#include "ModuleHandler.hh"

#include <boost/thread/thread.hpp>

using namespace boost;

thread_group tg;

DGC_MODULE x(1, "", 0);
ModuleHandler m(shared_ptr<DGC_MODULE>(new DGC_MODULE(1,"", 0)), 0);


int main() {

  DGC_MODULE *ptr_x = &x;
  ModuleHandler *ptr_m = &m;

  *ptr_x;
  shared_ptr<DGC_MODULE> msd = (*ptr_m).p_dm;

  ptr_x = &(*msd);
  tg.create_thread ( m  );

  return 0;

}
