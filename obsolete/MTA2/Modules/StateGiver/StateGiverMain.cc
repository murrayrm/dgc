#include "Modules.hh"
#include "StateGiver.hh"
#include "Kernel.hh"

#include <boost/shared_ptr.hpp>

using namespace boost;

Kernel K;
StateGiver DGCM(MODULES::StateGiver, "State Giver", 0);

int main() {

  K.Register();
  //  K.Register(shared_ptr<DGC_MODULE>(new StateGiver(MODULES::StateGiver, "Test Module", 0)));

  K.Start();

  return 0;
}
