#include "Perceptor.hh"
#include "../LocalPlanner/LocalPlanner.hh"
#include "Kernel.hh"
#include <time.h>
#include <iostream>

#include <stdlib.h>
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define PERCEPTOR_KEY 121
#define PERCEPTOR_SIZE 50000
//Data will be sent over MTA every SEND_RATE microseconds
#define PERCEPTOR_SEND_RATE 1000*1000

using namespace std;

extern Kernel K;
PerceptorDatum D;
//extern Perceptor testmodule;


void Perceptor::Init() {

  // do your init in here
  // i.e. open serial ports etc,

  cout << ModuleName() << ": Init" << endl;

  D.map_data = NULL;
  D.size = PERCEPTOR_SIZE;
  D.key = PERCEPTOR_KEY;

  D.shmflg = (IPC_CREAT | 0x1ff);
  if ((D.shmid = shmget(D.key, D.size, D.shmflg)) < 0) {
    cout << ModuleName() << ": DID NOT INIT PROPERLY - SHARED MEMORY ERROR";
    SetNextState(STATES::SHUTDOWN);
  } else {
    D.map_data = (void*) shmat(D.shmid, NULL, 0);
  // go to active state after initialization
  SetNextState(STATES::ACTIVE);
  }
}



void Perceptor::Active() {
  while( ContinueInState() ) {  // while stay active
    //get the map from the shared memory

    Mail m = NewOutMessage(MODULES::LocalPlanner, LocalPlannerMessages::MAPDATA);

    m.QueueTo(D.map_data, D.size);

    //K.SendMail(m);

    /*
    sleep(1); // increment the counter every second

    D.MyCounter ++;
    cout << ModuleName() << ": Active - Incrementing Counter = " << D.MyCounter << endl;
    //cout << "Sending New Out Message" << endl;
    Mail m = NewOutMessage(MODULES::Echo, PerceptorMessages::PAUSE);
    K.SendMail(m);
    */
    usleep(PERCEPTOR_SEND_RATE);
  }
} 


void Perceptor::Standby() {
  while( ContinueInState() ) {  // while stay active
    /*
    sleep(1); // dont increment the counter while in standby
    localCounter++;
    if(localCounter % 10 == 9) {
      //      cout << "Trying To Send Mail" << endl;
      Mail m = NewOutMessage(MODULES::Echo, PerceptorMessages::GO);
      K.SendMail(m);
    }

    //    D.MyCounter ++;
    cout << ModuleName() << ": Standby - _Not_ Incrementing Counter = " << D.MyCounter << endl;
    */
  }
} 


void Perceptor::Shutdown() {
  // do whatever closing of files, etc
  cout << ModuleName() << ": Shutdown" << endl;

}

void Perceptor::Restart() {
  // you may want to do something better here, but i'll just call shutdown and init
  cout << ModuleName() <<": Restart" << endl;
  Shutdown();
  Init();
}

string Perceptor::Status() {
  string x = ModuleName() + ": is in state ";
  switch ( GetCurrentState() ) {
  case STATES::INIT:
    x+= "Init";       break;
  case STATES::ACTIVE:
    x+= "Active";     break;
  case STATES::SHUTDOWN:
    x+= "Shutdown";   break;
  case STATES::STANDBY:
    x+= "Standby";    break;
  case STATES::RESTART:
    x+= "Restart";    break;
  default:
    x+= "Unknown state - (Something is very wrong)";
  }
  return x;
}
 
void Perceptor::InMailHandler(Mail& ml) {

  switch(ml.MsgType()) {  // for mail functions check out Misc/Mail/Mail.hh

  case PerceptorMessages::REINIT:
    SetNextState(STATES::RESTART);
    break;

  case PerceptorMessages::SHUTDOWN:
    // force the Kernel to shutdown
    K.ForceShutdown();
    break;

  case PerceptorMessages::PAUSE:
    SetNextState(STATES::STANDBY);
    break;

  case PerceptorMessages::GO:
    SetNextState(STATES::ACTIVE);
    break;

    /*
  case PerceptorMessages::SETCOUNTER:
    // check to see that an int was attached to the message
    if( ml.Size() >= sizeof(int)) {
      int x;
      ml >> x;
      D.MyCounter = x;
    }
    break;
    */

  default:
    cout << ModuleName() << ": Unknown InMessage" << endl;
  }
}


Mail Perceptor::QueryMailHandler(Mail& msg) {
  // figure out what we are responding to

  Mail reply = msg; // A hack !!! dont do this :)
  int x;

  switch(msg.MsgType()) {  
    /*
  case PerceptorMessages::GETCOUNTER:
    reply = ReplyToQuery(msg);
    reply << D.MyCounter;
    break;
    */

  case PerceptorMessages::GETSTATE:
    reply = ReplyToQuery(msg);
    x = GetCurrentState();
    reply << x;
    break;

  default:
    // send back a "bad Message"
    reply = Mail( msg.From(), msg.To(), 0, MailFlags::BadMessage);
  }

  return reply;
}


