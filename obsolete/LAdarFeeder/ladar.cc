#include "LadarFeeder.hh"

#include "ladar_power.h"
#include "laserdevice.h"

#include "Constants.h" //serial ports, other constants
#include "../frames/frames.hh" // TODO: change to installed location
#include <iostream.h>
#include <fstream.h>

// TODO: Change this to refer to a file in some standard -I directory
#include "../FusionMapper/mapper_functions.hh"

// FIXME: Eventually these should go in an include file.
#define ROOF_LADAR_SCAN_ANGLE 100
#define ROOF_LADAR_RESOLUTION 0.5
#define ROOF_LADAR_UNITS 0.01

#define BUMPER_LADAR_SCAN_ANGLE 180
#define BUMPER_LADAR_RESOLUTION 0.5
#define BUMPER_LADAR_UNITS 0.01

#define LADAR_VEHICLE_LENGTH_MULTIPLIER 1.0
#define LADAR_VEHICLE_WIDTH_MULTIPLIER 1.0

// global variables from main file
extern int   LADAR_POWER_CYCLE;
extern int   SIM;         // get the simulation flag from the main function
extern int   LOG_SCANS;
extern int   USE_BUMPER;  // says whether to use the bumper-mounted ladar
extern int   NO_PITCH_ROLL;
extern int   QUIT_PRESSED;
extern char* ROOF_TTY;
extern char* BUMPER_TTY;
extern char* TESTNAME;

extern int    PLAYBACK;     // Whether or not to play back a logged file
extern int    STEPTHROUGH;  // Step through scans one at a time
extern FILE*  scanlog;      // Log files for various data
extern FILE*  errorlog;     // Records any errors reported by the scanner
extern FILE*  statelog;     // Synchronized state data 
extern double start_time;   // Time to start reading logs
extern double stop_time;    // Time to stop reading logs

extern int    numLogScanPoints;
extern double scanLogUnits;

int BumperReset = 0;
int RoofReset = 0;
int BumperResetCount = 0;
int RoofResetCount = 0;
int TotalScans = 0;
int TotalErrorScans = 0;

// Coordinate system stuff.
XYZcoord roofLadarOffset(ROOF_LADAR_OFFSET_X, ROOF_LADAR_OFFSET_Y, 
                         ROOF_LADAR_OFFSET_Z);
XYZcoord bumperLadarOffset(BUMPER_LADAR_OFFSET_X, BUMPER_LADAR_OFFSET_Y, 
                           BUMPER_LADAR_OFFSET_Z);
frames roofLadarFrame;
frames bumperLadarFrame;

// LADAR Driver class
CLaserDevice roofLaser;
CLaserDevice bumperLaser;

/******************************************************************************/
/******************************************************************************/
int LadarFeeder::InitLadar() 
{
  int result;
  char * port_name;
  char filename[255];
  char datestamp[255];
  int status;
  unsigned char data[1024];
  bool good_to_go = true;
  
  // Open log file if LOG_SCANS is set
  if( LOG_SCANS ) {
    // filename will be TESTNAME_ddmmyyyy_hhmmss.log
    time_t t = time(NULL);
    tm *local;
    local = localtime(&t);
    int i;
    
    sprintf(datestamp, "%02d%02d%04d_%02d%02d%02d",
            local->tm_mon+1, local->tm_mday, local->tm_year+1900,
            local->tm_hour, local->tm_min, local->tm_sec);
    
    sprintf(filename, "testRuns/%s_scans_%s.log", TESTNAME, datestamp);
    printf("Logging scans to file: %s\n", filename);
    scanlog=fopen(filename,"w");
    if ( scanlog == NULL ) {
      printf("Unable to open scan log!!!\n");
      exit(-1);
    }
    // print data format information in a header at the top of the scanlog file
    fprintf(scanlog, "%% angle = %d\n", ROOF_LADAR_SCAN_ANGLE);
    fprintf(scanlog, "%% resolution = %lf\n", ROOF_LADAR_RESOLUTION);
    fprintf(scanlog, "%% units = %lf\n", ROOF_LADAR_UNITS);
    fprintf(scanlog, "%% Time[sec] | scan points\n");
    
    sprintf(filename, "testRuns/%s_state_%s.log", TESTNAME, datestamp);
    printf("Logging state to file: %s\n", filename);
    statelog=fopen(filename,"w");
    if ( statelog == NULL ) {
      printf("Unable to open state log!!!\n");
      exit(-1);
    }
    // print data format information in a header at the top of the state file
    fprintf(statelog, "%% Time[sec] | Easting[m] | Northing[m] | Altitude[m]");
    fprintf(statelog, " | Vel_E[m/s] | Vel_N[m/s] | Vel_U[m/s]" );
    fprintf(statelog, " | Speed[m/s] | Accel[m/s/s]" );
    fprintf(statelog, " | Pitch[rad] | Roll[rad] | Yaw[rad]" );
    fprintf(statelog, " | PitchRate[rad/s] | RollRate[rad/s] | YawRate[rad/s]");
    fprintf(statelog, "\n");
    
    sprintf(filename, "testRuns/%s_errors_%s.log", TESTNAME, datestamp);
    printf("Logging errors to file: %s\n", filename);
    errorlog=fopen(filename,"w");
    if ( errorlog == NULL ) {
      printf("Unable to open error log!!!\n");
      exit(-1);
    }
  } // end if( LOG_SCANS )
  else if( PLAYBACK )
  {
    double tmp; // throw away variable for time stamps
    int scan; // throw away variable for scans
    d.log_tstamp = 0; // initialize the log time stamp
    
    printf("Throwing away scans before --start time... ");
    
    // Cue up the correct location in the log files
    // throw away all of the log data before start_time
    while( d.log_tstamp < start_time ) {
      // suck in the state data for a scan
      fscanf(statelog, "%lf %lf %lf %f %f %f %f %lf %f %f %f %f %f %f %f\n",
             &(d.log_tstamp), &(d.SS.Easting), &(d.SS.Northing), &(d.SS.Altitude), 
             &(d.SS.Vel_E), &(d.SS.Vel_N), &(d.SS.Vel_U), 
             &(d.SS.Speed), &(d.SS.Accel), 
             &(d.SS.Pitch), &(d.SS.Roll), &(d.SS.Yaw), 
             &(d.SS.PitchRate), &(d.SS.RollRate), &(d.SS.YawRate));
      //printf("Throwing away state at time = %f sec\n", d.log_tstamp );
      
      // throw away all of the log data before start_time
      fscanf(scanlog, "%lf ",&tmp);
      // suck in a scan and save it
      for(int i = 0; i < numLogScanPoints; i++) {
        fscanf(scanlog,"%d ", &scan);
      } 
      fscanf(scanlog,"\n");
      //printf("Throwing away scan, at time = %f sec\n", tmp );
      
      if( d.log_tstamp != tmp ) {
        printf("LadarFeeder::InitLadar: Warning: Timestamps don't match!\n");
      }
    }
    
    printf("...done throwing away scans at t = %f.\n", d.log_tstamp);
  }
  
  // initialize the coordinate frames for the LADAR sensor
  roofLadarFrame.initFrames(roofLadarOffset, 
                            ROOF_LADAR_PITCH, ROOF_LADAR_ROLL, ROOF_LADAR_YAW);
  bumperLadarFrame.initFrames(bumperLadarOffset, BUMPER_LADAR_PITCH, 
                              BUMPER_LADAR_ROLL, BUMPER_LADAR_YAW);
  
  //--------------------------------------------------
  //   /* This code is intended to be used to power on and off the ladar
  //      through relays installed between the ladar units and the power box.
  //      Unfortunately, the code for this is in bob/vehlib instead of here,
  //      so just comment it out for now in order to keep LadarFeeder platform
  //      independent */
  //   // Turn on LADAR units
  //   ladar_pp_init();
  // 
  //   if(LADAR_POWER_CYCLE) {
  //     cout << "POWER CYCLING LADARS....PLEASE WAIT 105 SECONDS" << endl;
  //     ladar_off(LADAR_BUMPER);
  //     ladar_off(LADAR_ROOF);
  //     sleep(25);
  //     ladar_on(LADAR_BUMPER);
  //     ladar_on(LADAR_ROOF);
  //     sleep(10);
  //     cout << "70 seconds left" << endl;
  //     sleep(10);
  //     cout << "60 seconds left" << endl;
  //     sleep(10);
  //     cout << "50 seconds left" << endl;
  //     sleep(10);
  //     cout << "40 seconds left" << endl;
  //     sleep(10);
  //     cout << "30 seconds left" << endl;
  //     sleep(10);
  //     cout << "20 seconds left" << endl;
  //     sleep(10);
  //     cout << "10 seconds left" << endl;
  //     sleep(10);
  //   }
  //-------------------------------------------------- 
  
  // Ladar initialization tasks
  if( !SIM ) {
    while( roofLaser.Setup(ROOF_TTY, ROOF_LADAR_SCAN_ANGLE, 
                           ROOF_LADAR_RESOLUTION, LADAR_ROOF) != 0) {
      sleep(1);
    }
    if ( USE_BUMPER ) {
      while( bumperLaser.Setup(BUMPER_TTY, BUMPER_LADAR_SCAN_ANGLE, 
                               BUMPER_LADAR_RESOLUTION, LADAR_BUMPER) != 0 ) {
        sleep(1);
      }
    }
    // Do a self-test of the roof ladar
    printf("\n\nPERFORMING ROOF LADAR SELF-TEST...IF THIS DOES NOT RETURN\n");
    printf("AFTER A COUPLE OF SECONDS, THEN IT FAILED!!!!\n");
    status = roofLaser.LockNGetData( data );
    if((status & 0xFF) != 0x10) {
      printf("ROOF LADAR ERROR!  STATUS: %X\n", status);
      good_to_go = false;
    }
    if ( USE_BUMPER ) {
      printf("\n\nPERFORMING BUMPER LADAR SELF-TEST...IF THIS DOES NOT\n");
      printf("RETURN AFTER A COUPLE OF SECONDS, THEN IT FAILED!!!!\n");
      status = bumperLaser.LockNGetData( data );
      if((status & 0xFF) != 0x10) {
        printf("BUMPER LADAR ERROR!  STATUS: %X\n", status);
        good_to_go = false;
      }
    }
    if(!good_to_go) exit(-1); 
    return true;
  } 
  else {
    // Do simulation stuff here...
    return true;
  }  
} // end LadarFeeder::InitLadar()

/******************************************************************************/
/******************************************************************************/
void LadarFeeder::LadarGetMeasurements() 
{
  int i;
  double *roofscan;
  double *bumperscan;
  double tstamp;
  double phi, centDist;
  int numRoofScanPoints, numBumperScanPoints;
  int roofStatus, bumperStatus;
  XYZcoord npoint(0, 0, 0), scanpoint(0, 0, 0);
  XYZcoord vehPos(0, 0, 0);
  XYZmeas meas;
  double roofOffset;
  double bumperOffset;
  static int messageCounter = 0;
  
  // Create a new MTA message that we'll fill with measurements.
  Mail m = NewOutMessage(MyAddress(), MODULES::FusionMapper,
                         FusionMapperMessages::LadarMeasurements);
  
  Timeval tv = TVNow();
  // Starting angle of roof scan (in the sensor frame, the angle/azimuth 
  // of the first scan -- the farthest to the left)
  roofOffset = ((180.0 - ROOF_LADAR_SCAN_ANGLE) / 2.0) * M_PI / 180.0;
  bumperOffset = ((180.0 - BUMPER_LADAR_SCAN_ANGLE) / 2.0) * M_PI / 180.0;
  
  if( PLAYBACK ) 
  {
    numRoofScanPoints = numLogScanPoints; // get global variable
    numBumperScanPoints = 0;
  }
  else if(SIM) 
  {
    numRoofScanPoints = 
    (int)(ROOF_LADAR_SCAN_ANGLE / ROOF_LADAR_RESOLUTION + 1);
    numBumperScanPoints = 
      (int)(BUMPER_LADAR_SCAN_ANGLE / BUMPER_LADAR_RESOLUTION + 1);
  } 
  else 
  {
    numRoofScanPoints = roofLaser.getNumDataPoints();
    if (USE_BUMPER) numBumperScanPoints = bumperLaser.getNumDataPoints();
  }
  
  roofscan = (double *) malloc(numRoofScanPoints * sizeof(double));
  bumperscan = (double *) malloc(numBumperScanPoints * sizeof(double));
  
  /* Steps - 
    1. Take a scan from the ladar.
    2. Synchronize it with state data.
    3. Calculate mean and covariance of each measurement.
    4. Send measurements to FusionMapper
    */
  // Read in or log the vehicle state and scans
  // Important!: The PLAYBACK block format should match the LOG_SCANS block.
  if( PLAYBACK ) 
  {
    // suck in the state data for a scan
    fscanf(statelog, "%lf %lf %lf %f %f %f %f %lf %f %f %f %f %f %f %f\n",
           &(d.log_tstamp), &(d.SS.Easting), &(d.SS.Northing), &(d.SS.Altitude), 
           &(d.SS.Vel_E), &(d.SS.Vel_N), &(d.SS.Vel_U), 
           &(d.SS.Speed), &(d.SS.Accel), 
           &(d.SS.Pitch), &(d.SS.Roll), &(d.SS.Yaw), 
           &(d.SS.PitchRate), &(d.SS.RollRate), &(d.SS.YawRate));
  }
  // Important!: The format of this block should match the one above.
  else if(LOG_SCANS) 
  {
    tstamp = d.SS.Timestamp.dbl();
    fprintf(scanlog, "%f ", tstamp);
    
    // Print state      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 
    fprintf(statelog, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",
            tstamp, d.SS.Easting, d.SS.Northing, d.SS.Altitude,
            d.SS.Vel_E, d.SS.Vel_N, d.SS.Vel_U,
            d.SS.Speed, d.SS.Accel, 
            d.SS.Pitch, d.SS.Roll, d.SS.Yaw, 
            d.SS.PitchRate, d.SS.RollRate, d.SS.YawRate );
    fflush(statelog);
  }
  
  // Lets update the state right before we take a scan...
  if( !SIM ) 
  {
    UpdateState();
  }
  // TODO: Don't do this!
  if( NO_PITCH_ROLL ) 
  {
    d.SS.Pitch = 0.0;
    d.SS.Roll = 0.0;
  }
  // Update the XYZcoord for use with the new frames class.
  vehPos.X = d.SS.Northing;
  vehPos.Y = d.SS.Easting;
  vehPos.Z = 0.0;
  //--------------------------------------------------
  //   vehPos.Z = d.SS.Altitude;
  //-------------------------------------------------- 
  
  // Step 1 - Take a scan
  if( !SIM ) 
  { 
    roofStatus = roofLaser.UpdateScan();
    RoofReset = (roofLaser.isResetting() != 0);
    if(RoofReset != 0) RoofResetCount = roofLaser.isResetting();
    if ( USE_BUMPER ) 
    {
      bumperStatus = bumperLaser.UpdateScan();
      BumperReset = (bumperLaser.isResetting() != 0);
      if(BumperReset != 0) BumperResetCount = bumperLaser.isResetting();
    }
    TotalScans++;
    if(((roofStatus & 0x01FF) != 0x0010) || ((bumperStatus & 0x01FF) != 0x0010))
      TotalErrorScans++;
    if (LOG_SCANS) 
    {
      fprintf(errorlog,"%f 0x%04X\n", tstamp, roofStatus);
      fflush(errorlog);
    }
    // update local data buffer
    for(int i = 0; i < numRoofScanPoints; i++) 
    {
      if (LOG_SCANS) 
      {
        fprintf(scanlog,"%d ", roofLaser.Scan[i]);
      }
      if(roofLaser.isDataError(roofStatus,roofLaser.Scan[i]) || RoofReset) 
        roofscan[i] = -1;
      else
        roofscan[i] = (roofLaser.Scan[i] * ROOF_LADAR_UNITS);
    }
    if ( USE_BUMPER ) 
    {
      for(int i = 0; i < numBumperScanPoints; i++) 
      {
        if(bumperLaser.isDataError(bumperStatus,bumperLaser.Scan[i]) 
           || BumperReset)
          bumperscan[i] = -1;
        else
          bumperscan[i] = (bumperLaser.Scan[i] * BUMPER_LADAR_UNITS);
      }
    }
  } 
  else if( PLAYBACK ) 
  {
    int scan;
    
    // keep taking scans until the end of the log or until stop_time
    if( !feof(scanlog) && !feof(statelog) && d.log_tstamp < stop_time )
    {
      fscanf(scanlog, "%lf ",&tstamp);
      // suck in a scan and save it
      for(int i = 0; i < numLogScanPoints; i++) 
      {
        fscanf(scanlog,"%d ", &scan);
        if(scan >= 0x1FF7) 
        {
          roofscan[i] = -1.0; // Bad scan so ignore it
        }
        else roofscan[i] = (scan * scanLogUnits);
      }
      fscanf(scanlog,"\n");
    }
    else
    {
      printf("Finished reading scans at time = %f sec.\n", tstamp );
      QUIT_PRESSED = 1;
    }
  }
  // or fake a scan - TODO: should get this from Gazebo/Player!
  else 
  {
    // the distance of the center scanline to flat ground
    // assumes zero roll and zero yaw for sensor
    // and zero roll and zero pitch for vehicle...
    //printf("Fair notice - NO support for simulating 2 ladars yet!!!\n");
    centDist = ROOF_LADAR_OFFSET_Z/tan(ROOF_LADAR_PITCH);
    for (int i = 0; i < numRoofScanPoints; i++) 
    {
      phi = roofOffset + (i * ROOF_LADAR_RESOLUTION * M_PI) / 180.0;
      roofscan[i] = centDist / sin(phi);
    }
    if (LOG_SCANS) 
    {
      for (int i = 0; i < numRoofScanPoints; i++) 
      {
        fprintf(scanlog,"%d ", (int) (roofscan[i] * 100.0) );
      }
    }
  } // end getting new scan
  if (LOG_SCANS) fprintf(scanlog,"\n");
  if (LOG_SCANS) fflush(scanlog);
  
  /* ***************************************************** */
  /* ***************************************************** */
  // Step 2 - synchronize the scan with state data and calculate
  // the measurement coordinate in 3D space
  
  // update internal state of frames
  // TODO: Determine which elements of SS are actually needed for this 
  // operation, and use the minimal set!
  roofLadarFrame.updateState( vehPos, d.SS.Pitch, d.SS.Roll, d.SS.Yaw );
  bumperLadarFrame.updateState( vehPos, d.SS.Pitch, d.SS.Roll, d.SS.Yaw );
  
  /* maintain a counter of the valid measurements. */
  int numValidRoofMeasurements = numRoofScanPoints;
  /* Loop once through the scanpoints so that we can send the number of 
    measurements in the MTA message, which doesn't include the invalid 
    scans. */
  for(int i = 0; i < numRoofScanPoints; i++)
  {
    if(roofscan[i] < 0 || roofscan[i] >= ROOF_LADAR_MAX_RANGE)
    {
      // If its < 0 then we got an error from the ladar, so ignore the
      // scanpoint, and dont send it.  If its max range or
      // larger, we either got an error or we got a good scanpoint and
      // there isn't anything there.  so ignore the scanpoint, and
      // dont send the data.
      numValidRoofMeasurements--;
    }
  }
  
  if(!RoofReset) 
  {
    printf("Sending %d measurement message.\n\n", numValidRoofMeasurements);
    
    // Important!: first put the number of measurements in the MTA message
    // TODO: Make sure that the receiving end is consistent with this.
    m << numValidRoofMeasurements;
    
    // Add more elements to the message.  NOTE: The receiving end must 
    // have the same format for reading this message, so make sure it matches!
    // FusionMapper needs to know where the vehicle is so that it can 
    // update the map frame.
    m << d.SS.Northing;
    m << d.SS.Easting;
    
    // calculate measurements from the new roof scan
    for(int i = 0; i < numRoofScanPoints; i++) 
    {
      phi = roofOffset + (i * ROOF_LADAR_RESOLUTION * M_PI) / 180.0;
      if(roofscan[i] < 0 || roofscan[i] >= ROOF_LADAR_MAX_RANGE) 
      {
        // If its < 0 then we got an error from the ladar, so ignore the
        // scanpoint, and dont send it.  If its max range or
        // larger, we either got an error or we got a good scanpoint and
        // there isn't anything there.  so ignore the scanpoint, and
        // dont send the data.
        continue;
      }
      
      // convert the range into coordinates in the sensors frame
      // sensor x-axis points straight ahead, y-axis to the left, and
      // z-down, thus the scanline at 90 degrees will have a 0 y-coord.
      // All of the data will have a zero z-value in the sensors frame.
      scanpoint.X = roofscan[i]*sin(phi);
      scanpoint.Y = roofscan[i]*cos(phi);
      scanpoint.Z = 0;
      
      // convert the sensor frame coords into utm coordinates
      npoint = roofLadarFrame.transformS2N(scanpoint);
      
      //--------------------------------------------------
      //       /* Debugging */
      //       printf("vehPos = (%f, %f, %f) m\n", vehPos.X, vehPos.Y, vehPos.Z);
      //       printf("Pitch/Roll/Yaw = (%f, %f, %f) rad\n", 
      //              d.SS.Pitch, d.SS.Roll, d.SS.Yaw );
      //       printf("scanpt = (%f, %f, %f) m\n",
      //              scanpoint.X, scanpoint.Y, scanpoint.Z);
      //       printf("npoint = (%f, %f, %f) m\n", npoint.X, npoint.Y, npoint.Z);
      //       printf("Northing/Easting = (%f, %f)\n\n", d.SS.Northing, d.SS.Easting);
      //       sleep(5);
      //-------------------------------------------------- 
      
      // test the timing of the covariance calculation
      //tv = TVNow();
      
      // NOTE: Each covariance calculation (actually, everything inside this
      // loop) takes about 32us.  181 scanpoints * 32us = 5.8ms per scan
      if( true ) 
      {
        // Compute also the covariance matrix associated with the measurement.
        // TODO: Do this.
        XYZmatrix cov;
        // TODO: Add constructor to VehPoseMeas struct so that we can call
        // VehPoseMeas(d.SS)
        VehPoseMeas vehmeas; 
        vehmeas.Northing = d.SS.Northing; 
        vehmeas.Easting  = d.SS.Easting;
        vehmeas.Downing  = d.SS.Altitude;
        vehmeas.Pitch    = d.SS.Pitch;
        vehmeas.Roll     = d.SS.Roll;
        vehmeas.Yaw      = d.SS.Yaw;
        // TODO: Properly estimate these variances
        vehmeas.Northing_var = DEFAULT_VEHICLE_EASTING_VARIANCE;
        vehmeas.Easting_var  = DEFAULT_VEHICLE_NORTHING_VARIANCE;
        vehmeas.Downing_var  = DEFAULT_VEHICLE_DOWNING_VARIANCE;
        vehmeas.Pitch_var    = DEFAULT_VEHICLE_PITCH_VARIANCE;
        vehmeas.Roll_var     = DEFAULT_VEHICLE_ROLL_VARIANCE;
        vehmeas.Yaw_var      = DEFAULT_VEHICLE_YAW_VARIANCE;
        
        SphericalMeas sensormeas;
        sensormeas.range     = roofscan[i]; // meters
        sensormeas.azimuth   = phi;         // radians
        sensormeas.elevation = 0;           // radians
                                            // TODO: Properly estimate these variances
        sensormeas.range_var     = DEFAULT_LADAR_RANGE_VARIANCE;     // meters^2
        sensormeas.azimuth_var   = DEFAULT_LADAR_AZIMUTH_VARIANCE;   // rad^2
        sensormeas.elevation_var = DEFAULT_LADAR_ELEVATION_VARIANCE; // rad^2
        cov = calcCovMatrix( vehmeas, sensormeas ); 
        
        /* DEBUG */
        //printf("Cov matrix follows\n");
        //cov.printXYZmatrix();
        
        // Create and send the measurement to FusionMapper
        // The first argument is either a Newmat ColumnVector or XYZcoord
        // The second argument is either a Newmat Matrix or XYZmatrix
        // It's better practice to use an XYZcoord and XYZmatrix
        XYZmeas meas(npoint, cov);
        
        // Add the measurement to the MTA mail message.
        m << meas;
        
        //--------------------------------------------------
        //         /* Debugging. */
        //         printf("Northing/Easting/Down = (%f, %f, %f) m\n", 
        //                d.SS.Northing, d.SS.Easting, d.SS.Altitude);
        //         printf("Pitch/Roll/Yaw        = (%f, %f, %f) rad\n\n", 
        //                d.SS.Pitch, d.SS.Roll, d.SS.Yaw);
        //         sleep(1000);
        //-------------------------------------------------- 
        
      }
      
    } // end looping through new roofscan points
    
    // Send all of our measurements across MTA in one message.
    SendMail(m);
    if( PLAYBACK ) 
    {
      messageCounter++;
      printf("Mail message %d sent, log timestamp = %f sec\n", 
             messageCounter, d.log_tstamp);
      
      if( STEPTHROUGH )
      {
        printf("Paused because --step was specified (<Enter> to continue).\n");
        char answer[100];
        fgets(answer,sizeof(answer),stdin);
      }
      //--------------------------------------------------
      //       /* DEBUG */
      //       usleep(10000);
      //-------------------------------------------------- 
    }
  }
  
  // TODO: Add the above covariance calculation and MTA messaging to the 
  // bumper ladar code below.
  
  // now calculate measurements from the new bumper scan
  if ( USE_BUMPER && !BumperReset) 
  {
    for(int i = 0; i < numBumperScanPoints; i++) 
    {
      phi = bumperOffset + (i * BUMPER_LADAR_RESOLUTION * M_PI) / 180.0;
      if(bumperscan[i] < 0 || bumperscan[i] >= BUMPER_LADAR_MAX_RANGE) 
      {
        // If its < 0 then we got an error from the ladar, so ignore the
        // scanpoint, and dont send it.  If its max range or
        // larger, we either got an error or we got a good scanpoint and
        // there isn't anything there.  so ignore the scanpoint, and
        // dont send the data.
        continue;
      }
      
      // convert the range into coordinates in the sensors frame
      // sensor x-axis points straight ahead, y-axis to the left, and
      // z-down, thus the scanline at 90 degrees will have a 0 y-coord.
      // All of the data will have a zero z-value in the sensors frame.
      scanpoint.X = bumperscan[i]*sin(phi);
      scanpoint.Y = bumperscan[i]*cos(phi);
      scanpoint.Z = 0;
      
      // convert the sensor frame coords into utm coordinates 
      npoint = bumperLadarFrame.transformS2N(scanpoint);
      // We no longer have access to CellData (no mapping functions
      // should be necessary for LadarFeeder)
      //newCell.value = npoint.Z;
      
      // TODO: Fix this new stuff to be cleaner and then send the message.
      // TODO: Do the same thing as for the roof scans.
      
    } // end looping through new bumperscan points
  }
  
  /* ***************************************************** */
  /* ***************************************************** */
  
  free(roofscan);
  free(bumperscan);
  
  Timeval tv_end = TVNow();
  // as of thursday this takes 420 ms!!!
  //  fprintf(stderr, "%lf seconds elapsed\n", (tv_end.dbl() - tv.dbl()));
  
} // end LadarFeeder::LadarGetMeasurements()


void LadarFeeder::LadarShutdown() 
{
  fclose(scanlog);
  fclose(statelog);
  roofLaser.LockNShutdown();
  bumperLaser.LockNShutdown();
}


// end ladar.cc
