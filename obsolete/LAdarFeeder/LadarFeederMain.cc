#include "LadarFeeder.hh"
#include <getopt.h>
#include <float.h>              /* DBL_MAX */

using namespace std;

#define LOG_OPT        10
#define PLAY_OPT       11
#define START_OPT      12
#define STOP_OPT       13
#define ROOF_TTY_OPT   14
#define BUMPER_TTY_OPT 15
#define SLEEP_OPT      16
#define HELP_OPT       17

// global variables/flags
int SIM                  = 0;       // run in simulation mode?
int LOG_SCANS            = 0;       // dump scans/state/errors to log file?
int PLAYBACK             = 0;       // Get scans and state from log file
int STEPTHROUGH          = 0;       // Step through scans (for debugging)
int SPARROW_DISP         = 0;       // Whether or not to show the display
int NOSTATE              = 0;       // Don't update state if = 1.
int NOCONFIRM            = 0;       // Don't ask for confirmation of options
int USE_BUMPER           = 1;       // Use the bumper ladar as well if = 1
int NO_PITCH_ROLL        = 0;       // Set Pitch = Roll = zero in calculations
int LADAR_POWER_CYCLE    = 0;       // Power cycle both ladar units.
unsigned long SLEEP_TIME = 50000;   // How long in us to sleep between votes

char *ROOF_TTY          = NULL;     // serial port for scanner on the roof
char *BUMPER_TTY        = NULL;     // serial port for scanner on the bumper
char *TESTNAME          = NULL;     // id for test to be part of log file name
char *PLAYBACK_LOG      = NULL;
FILE *scanlog;                      // logfile for scans, used for playback
FILE *statelog;                     // logfile for state, used for playback
FILE *errorlog;                     // logfile for ladar errors
int numLogScanPoints    = 0;        // number of scanpoints found in logfile
double scanLogUnits     = 0;        // scan log units found in logfile
double start_time       = 0.0;      // starting timestamp for logfile
double stop_time        = DBL_MAX;  // stopping timestamp for logfile

/* The name of this program. */
const char * program_name;

/* Prints usage information for this program to STREAM (typically stdout 
   or stderr), and exit the program with EXIT_CODE. Does not return. */
void print_usage (FILE* stream, int exit_code)
{
  fprintf( stream, "Usage:  %s [options]\n", program_name );
  fprintf( stream, 
	   "  --sim             Simulation mode, faked measurements.\n"
	   "  --nobumper        Do not use the bumper ladar.\n"
	   "  --ignstate        Ignore the pitch and roll.\n"
           "  --disp            Run Sparrow display.\n"
           "  --nostate         Continue even if state data is not received.\n"
           "  --powercycle      Power cycle both LADAR units.\n"
	   "\n"
	   "  --play testname   Simulation mode, scans and state read from\n"
	   "                    log files.  <testname> should be the full\n"
	   "                    name of the LADAR *scans* log, e.g.\n"
	   "                    race_scans_03132004_063108.log.  This option\n"
	   "                    overrides the --log and --sim options.\n"
           "  --step            Step through scans one at a time (debugging).\n"
           "  --start sec       Specifies logged start time to read scans.\n"  
           "  --stop sec        Specifies logged stop time to read scans.\n"  
	   "\n"
	   "  --log testname    Log all scanpoints, state and errors in\n"  
	   "                    testLogs directory. Log files will be named\n"
	   "                    as <testname>_<type>_<date>_<time>.log\n"
           "                    where <type> is scan, state, or errors.\n"
	   "  --roof-tty tty    Specifies roof ladar TTY (e.g. /dev/ttyS8).\n"
	   "  --bumper-tty tty  Specifies bumper ladar TTY port.\n" 
           "  --sleep us        Specifies us sleep time in Active function.\n"  
	   "  --noconfirm       Do not ask for confirmation of options.\n"
           "  --help, -h        Display this message.\n" );      
           exit(exit_code);
}

int main(int argc, char **argv) 
{
  // set the default argument parameters that won't need external access.
  // keep the strdup because it allocates the memory.  sprintf doesn't.
  ROOF_TTY          = strdup("/dev/ttyS0");
  BUMPER_TTY        = strdup("/dev/ttyS5");
  TESTNAME          = strdup("temp");
  PLAYBACK_LOG      = strdup("none");
  sprintf(ROOF_TTY,"/dev/ttyS%d",LADAR_ROOF_SERIAL_PORT);
  sprintf(BUMPER_TTY,"/dev/ttyS%d",LADAR_BUMPER_SERIAL_PORT);

  int ch;
  /* A string listing valid short options letters. */
  const char* const short_options = "h";
  /* An array describing valid long options. */
  static struct option long_options[] = 
  {
    // first: long option (--option) string
    // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
    // third: if pointer, set variable to value of fourth argument
    //        if NULL, getopt_long returns fourth argument
    {"sim",        0, &SIM,		 	1},
    {"nobumper",   0, &USE_BUMPER, 		0},
    {"noconfirm",  0, &NOCONFIRM, 		1},
    {"ignstate",   0, &NO_PITCH_ROLL,           1},
    {"disp",       0, &SPARROW_DISP, 		1},
    {"nostate",    0, &NOSTATE,           	1},
    {"step",       0, &STEPTHROUGH,             1},
    {"powercycle", 0, &LADAR_POWER_CYCLE, 	1},
    {"log",        1, NULL, 			LOG_OPT},
    {"play",       1, NULL, 			PLAY_OPT},
    {"start",      1, NULL, 			START_OPT},
    {"stop",       1, NULL, 			STOP_OPT},
    {"roof-tty",   1, NULL, 			ROOF_TTY_OPT},
    {"bumper-tty", 1, NULL, 			BUMPER_TTY_OPT},
    {"sleep",      1, NULL, 			SLEEP_OPT},
    {"help",       0, NULL, 			'h'},
    {NULL,         0, NULL,                     0} /* required at end */
  };

  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  program_name = argv[0];
  int option_index = 0;
  printf("\n");
  
  // Loop through and process all of the command-line input options.
  while((ch = getopt_long(argc, argv, "h", long_options, &option_index)) != -1)
  {
    switch(ch) 
    {
      case LOG_OPT:
        { LOG_SCANS = 1;
          TESTNAME = strdup(optarg); }
	break;

      case PLAY_OPT:
        { PLAYBACK = 1;
          PLAYBACK_LOG = strdup(optarg); }
	break;

      case START_OPT:
        start_time = atof(optarg);
	break;

      case STOP_OPT:
        stop_time = atof(optarg);
	break;

      case ROOF_TTY_OPT:
        ROOF_TTY = strdup(optarg);
        break;
 
      case BUMPER_TTY_OPT:
        BUMPER_TTY = strdup(optarg);
        break;

      case SLEEP_OPT:
        SLEEP_TIME = (unsigned long) atol(optarg);
        break;

      case 'h':
        /* User has requested usage information. Print it to standard
           output, and exit with exit code zero (normal termination). */
        print_usage(stdout, 0);

      case '?': /* The user specified an invalid option. */
	/* Print usage information to standard error, and exit with exit
	   code one (indicating abnormal termination). */
	print_usage(stderr, 1);

      case -1: /* Done with options. */
	break;
    }
    cout << "  option: " << long_options[option_index].name;
    if(optarg) cout << ", with arg: " << optarg;
    cout << endl;
  } 
 
  // set up logfiles for reading if the --play option was specified
  if( PLAYBACK ) 
  {
    char *p; // character pointer for string manipulation
    char *timestamp, *testname; // temporary character arrays
    char filename[255], tmp[1000]; // temporary character arrays
    int angle; // scanning angle in degrees
    double resolution; // ladar resolution in degrees and units

    /* Break up the playback scans log name so that we can recreate
       both the scans and the state logs. */
    p = strstr(PLAYBACK_LOG, "_scans_");
    if( p == NULL ) {
      printf("Playback file name does not contain \"_scans_\".  ");
      printf("Will not continue!\n");
      exit(1);
    }
    timestamp = strdup(p+7);
    *p = 0;

    testname = strdup(PLAYBACK_LOG);
    p = strstr(timestamp,".log");
    *p = 0;

    printf("testname: %s\n", testname);
    printf("timestamp: %s\n", timestamp);
    printf("Will read scan points from (%lf,%lf) sec\n", start_time, stop_time);

    sprintf(filename, "%s_scans_%s.log", testname, timestamp );
    printf("Reading scans from file: %s\n", filename);
    scanlog=fopen(filename,"r");
    if ( scanlog == NULL ) {
      printf("Unable to open scan log!!!\n");
      exit(-1);
    }
    fscanf(scanlog, "%% angle = %d\n", &angle);
    fscanf(scanlog, "%% resolution = %lf\n", &resolution);
    fscanf(scanlog, "%% units = %lf\n", &scanLogUnits);

    fgets(tmp, 1000, scanlog); // Read and discard header

    sprintf(filename, "%s_state_%s.log", testname, timestamp );
    printf("Reading state from file: %s\n", filename);
    statelog=fopen(filename,"r");
    if ( statelog == NULL ) {
      printf("Unable to open state log!!!\n");
      exit(-1);
    }
    fgets(tmp, 1000, statelog); // Read and discard header

    printf("LADAR scanning angle: %d degrees\n", angle);
    printf("LADAR scanning resolution: %lf degrees\n", resolution);
    printf("LADAR units: %lf m\n", scanLogUnits);
    numLogScanPoints = (int)((double)angle/resolution) + 1;
    printf("Num of scan points: %d\n", numLogScanPoints);

    printf("NOTE: --play option was successfully specified.\n");
    printf(" => Setting SIM = 1, LOG_SCANS = 0, NOSTATE = 1.\n");
    SIM       = 1;
    LOG_SCANS = 0;
    NOSTATE   = 1;
  }
  
  
  // give an argument summary
  cout << endl << "Argument summary:" << endl;
  cout << "  SIM:            " << SIM << endl;
  cout << "  USE_BUMPER:     " << USE_BUMPER << endl;
  cout << "  NO_PITCH_ROLL:  " << NO_PITCH_ROLL << endl;
  cout << "  SPARROW_DISP:   " << SPARROW_DISP << endl;
  cout << "  NOSTATE:        " << NOSTATE << endl;
  cout << "  LOG_SCANS:      " << LOG_SCANS << endl;
  cout << "    TESTNAME:     " << TESTNAME << endl;
  cout << "  PLAYBACK:       " << PLAYBACK << endl;
  cout << "    PLAYBACK_LOG: " << PLAYBACK_LOG      << endl;
  cout << "  start_time:     " << start_time << endl;
  cout << "  stop_time:      " << stop_time << endl;
  cout << "  SLEEP:          " << SLEEP_TIME << "us" << endl;
  cout << "  ROOF_TTY:       " << ROOF_TTY << endl; 
  cout << "  BUMPER_TTY:     " << BUMPER_TTY << endl; 

  // check that a TESTNAME was specified
  if(TESTNAME == 0) {
    cout << endl << endl;
    cout << "--log switch must have a testname!" << endl;
    exit(-1);
  }

  // get confirmation of arguments
  if(!NOCONFIRM) {
    cout << endl;
    cout << "VERIFY THAT YOU HAVE SPECIFIED THE CORRECT SWITCHES!" << endl;
    cout << "Do you wish to proceed? (type 'yes', anything else exits)" << endl;
    char answer[100];
    fgets(answer,sizeof(answer),stdin);
    if(strcmp(answer, "yes\n")) {
      exit(-1);
    }
  }
  
  // Start the MTA By registering a module
  // and then starting the kernel
  Register(shared_ptr<DGC_MODULE>(new LadarFeeder));
  StartKernel();

  return 0;
}
