#include "LadarFeeder.hh"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"
#include "MTA/Misc/Time/Time.hh"

#include <unistd.h>
#include <iostream>

int QUIT_PRESSED = 0;

extern unsigned long SLEEP_TIME;
extern int SPARROW_DISP;
extern int NOSTATE;

LadarFeeder::LadarFeeder() : DGC_MODULE(MODULES::LadarFeeder, "LADAR Feeder", 1)
{
}

LadarFeeder::~LadarFeeder() 
{
}


void LadarFeeder::Init() 
{
  // call the parent init first
  DGC_MODULE::Init();

  InitLadar();

  if(SPARROW_DISP) 
  {
    cout << "Starting Sparrow display" << endl;
    RunMethodInNewThread<LadarFeeder>( this, 
		    &LadarFeeder::SparrowDisplayLoop);
    RunMethodInNewThread<LadarFeeder>( this, 
		    &LadarFeeder::UpdateSparrowVariablesLoop);
  }
  cout << "Module Init Finished" << endl;
}

void LadarFeeder::Active() 
{
  cout << "Starting Active Function" << endl;

  while( ContinueInState() && !QUIT_PRESSED) 
  {
    // update the State Struct
    if(!NOSTATE) 
    {
      UpdateState();
      if(d.SS.Northing == 0) 
      {
        cout << endl << "UNABLE TO COMMUNICATE WITH VSTATE" << endl;
        cout << "NORTHING IS ZERO, PLEASE CHECK OR" << endl;
        cout << "RUN WITH --nostate SWITCH IF THIS IS OK" << endl << endl;
        exit(-1);
      }
    }
    // Get a list of measurements and feed them to FusionMapper
    LadarGetMeasurements();

    if(SPARROW_DISP) UpdateSparrowVariablesLoop();

    // and sleep for a bit (subtract sleep overhead)
    usleep_for(max((int)(SLEEP_TIME - 10000), 0));
  }
  cout << "Finished Active State" << endl;
}

void LadarFeeder::Shutdown() 
{
  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;
  LadarShutdown();
}

// end LadarFeeder.cc
