/**
 * MapDisplayDatum.cc
 * Revision History:
 * 01/14/2005  hbarnor  Created
 */

#ifndef MAPDISPLAYDATUM_H
#define MAPDISPLAYDATUM_H

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
//#include "VState.hh"
//#include "VDrive.hh"
#include "VehicleState.hh"

// Map deltas
#include "CMapPlus.hh"
// The paths - same as received by Module Management
#include "traj.h"
//#include "MapDisplayMTA.h"

using namespace std;

// All data that needs to be shared by common threads
struct MapDisplayDatum 
{
  // State struct from VState
  boost::recursive_mutex stateStructLock;
  VehicleState SS;
  // Declare the Map class.
  boost::recursive_mutex mapDeltaLock; //might not actually need this but hey I am an MTA novice
  CMap myCMap; // map delta 
  
  boost::recursive_mutex pathLock; //might not actually need this but hey I am an MTA novice
  CTraj *curPath; // the path received from PathFollower
};
// enumerate all module messages that could be received
namespace MapDisplayMessages {
  enum 
    {
      /**
       * Message contains the planned traj
       */
      NewTraj
    };
};


#endif
