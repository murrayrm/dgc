/**
 * MapDisplayMTA.cc
 * Revision History:
 * 01/14/2005  hbarnor  Created
 */

#include "MapDisplayMTA.h"
#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include <unistd.h>
#include <iostream>

// define external variables
extern int           NOSTATE;

MapDisplayMTA::MapDisplayMTA(MapConfig * mapConfig) 
  : DGC_MODULE(MODULES::MapDisplayMTA, "SkynetGUI: Map Display", 0), m_mapConfig(mapConfig) 
{
  // set the state struct of the mapConfig
  m_mapConfig->set_state_struct(&d.SS);
  // set the CMAP to the first waypoint
  #warning "CMAP centered at waypoint 1"
  RDDFVector::iterator x = mapConfig->get_rddf().begin();
  d.myCMap.initMap(x->Northing,x->Easting,200,200,0.5,0.5,0);
  d.myCMap.addLayer<double>(-11, -12);
  m_mapConfig->set_cmap(&d.myCMap);
  m_mapConfig->notify_update();
  // initialize paths
  path_history.points.startDataInput();
  planned_path.points.startDataInput();  
  // Set the color for our path history to red.
  path_history.color = RGBA_COLOR(1.0, 0.0, 0.0); // red
  planned_path.color = RGBA_COLOR(0.0, 1.0, 0.0); // green
  // trajectory
  m_traj = new CTraj(1);
}

MapDisplayMTA::~MapDisplayMTA() 
{
  
}


void MapDisplayMTA::Init() 
{
  // call the parent init first
  DGC_MODULE::Init();
    // update the State Struct and check for valid state
  if(!NOSTATE)
  {
    //    #warning "Commented out state update"
    UpdateState();
    if(d.SS.Northing == 0)
      {
	printf("\nUnable to communicate with vstate (Northing is zero).\n");
	printf("Run with --nostate option if this is OK. (Exiting...)\n\n");
	exit(-1);
      }
    else
      {
	// center map display on vehicle
	d.myCMap.updateVehicleLoc(d.SS.Northing,d.SS.Easting);
      }
    cout << "Skynet Gui: MapDisplay Module Init Finished" << endl;
  }
}

void MapDisplayMTA::Active() 
{

  cout << "Starting Active: " << endl;
  while( ContinueInState() )
    {
      // Get state from the simulator or vstate 
      if( NOSTATE )
	{
	  d.SS.Northing = 0.0;
	  d.SS.Easting	= 0.0;
	  d.SS.Vel_N = 0.0;
	  d.SS.Vel_E = 0.0;
	  d.SS.Easting	= 0.0;
	  d.SS.Yaw = 0.0;
	  d.SS.YawRate	= 0.0;
	}
      else
	{
	  /* This sets d.SS */
	  UpdateState();
	}
      // and sleep for a bit
      usleep(100000);
  }

  cout << "Finished Active State" << endl;

}


void MapDisplayMTA::Shutdown() 
{
  // if we get to here a break has been detected 
  cout << "SkynetGui MapDisplay is Shutting Down" << endl;

}


void MapDisplayMTA::UpdateState()
{
  // These two lines take roughly about 400 microseconds.
  Mail msg = NewQueryMessage( MyAddress(), MODULES::VState,
                              VStateMessages::GetVehicleState);
  Mail reply = SendQuery(msg);

  // This takes about 2-3 microseconds.
  if (reply.OK() ) 
    {
      reply >> d.SS; // download the new state
      d.myCMap.updateVehicleLoc(d.SS.Northing,d.SS.Easting);
      if( m_mapConfig )
	{ 
	  m_mapConfig->notify_update();
	}
    }

//--------------------------------------------------
//   /* DEBUG */
//   // Determine the time disparity between processes.
//   // compare the timestamp we just received across the
//   // MTA to the local current time
//   printf("TVNow() - d.SS.Timestamp = %f sec\n",
//          (TVNow() - d.SS.Timestamp).dbl() );
//-------------------------------------------------- 

} // end UpdateState()



void MapDisplayMTA::InMailHandler(Mail& msg)
{
  cout << "mail received" << endl;
  switch (msg.MsgType())
    {
    case MapDisplayMessages::NewTraj:
      {
	cout << "Traj received" << endl;
	updateHistory(); // save to history
	planned_path.points.startDataInput(); // clear current pat
// 	msg.DequeueFrom(m_traj, CTraj::getHeaderSize());
// 	msg.DequeueFrom(m_traj->getNdiffarray(0), m_traj->getNumPoints() * sizeof(double));
// 	msg.DequeueFrom(m_traj->getEdiffarray(0), m_traj->getNumPoints() * sizeof(double));
// 	msg.DequeueFrom(m_traj->getNdiffarray(0), m_traj->getNumPoints() * sizeof(double));
// 	msg.DequeueFrom(m_traj->getEdiffarray(0), m_traj->getNumPoints() * sizeof(double));
	msg.DequeueFrom(&(planned_path.points), CTraj::CTraj::getHeaderSize());
	msg.DequeueFrom(planned_path.points.getNdiffarray(0), planned_path.points.getNumPoints() * sizeof(double));
 	msg.DequeueFrom(planned_path.points.getEdiffarray(0), planned_path.points.getNumPoints() * sizeof(double));
	cout << "order = " << planned_path.points.getOrder() << endl;
	cout << "num   = " << planned_path.points.getNumPoints() << endl;      
	list<PATH> paths; 
	paths.push_back(path_history);
	cout << "Added Path History" << endl;
	paths.push_back(planned_path);
	cout << " Added Planned path" << endl;
	m_mapConfig->set_paths( paths );
	cout << " map_config set paths" << endl;
	m_mapConfig->notify_update();
	cout << " map_config notify update" << endl;
      }
      break;    
    default:
      // let the parent mail handler handle it
      DGC_MODULE::InMailHandler(msg);
      break;
    }
}

void MapDisplayMTA::updateHistory()
{
  // Add our current position to the path history	  
  // If our history has too many points in it, we need to shift them and add
  int num_points;
  if( (num_points = path_history.points.getNumPoints()) > TRAJ_MAX_LEN-2 )
    {
      double *p_N = path_history.points.getNdiffarray(0);
      double *p_E = path_history.points.getEdiffarray(0);      
      int i;
      for(i=0; i<num_points-1; i++)
	{
	  p_N[i] = p_N[i+1];
	  p_E[i] = p_E[i+1];		  
	}
      p_N[i] = planned_path.points.lastN();
      p_E[i] = planned_path.points.lastE();
    }
  else
    {
      // else just use the method to add it
      path_history.points.inputNoDiffs( planned_path.points.lastN(), planned_path.points.lastE() );
    }

}
