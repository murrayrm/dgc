/**
 * MapDisplayMain.cc
 * Revision History:
 * 01/14/2005  hbarnor  Created
 */

#include "MapDisplayMTA.h"
#include "MapDisplay/MapConfig.hh"

using namespace std;

int mta_main(MapConfig * mapC) 
{
  // do argument checking here if you so wish
    
  // Start the MTA By registering a module
  // and then starting the kernel
  
  Register(shared_ptr<DGC_MODULE>(new MapDisplayMTA(mapC)));
  StartKernel();
  
  return 0;
}

