/**
 * MapDisplayMTA.h
 * Revision History:
 * 01/14/2005  hbarnor  Created
 */
#ifndef MAPDISPLAYMTA_H
#define MAPDISPLAYMTA_H


#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "MapDisplay/MapDisplay.hh" // for MapConfig
#include "MapDisplayDatum.h"


using namespace std;
using namespace boost;


class MapDisplayMTA : public DGC_MODULE {
public:
  // and change the constructors
  /**
   * Constructor
   * Sets the statestruct of the mapconfig,
   * sets the CMAP to the first waypoint
   */
  MapDisplayMTA(MapConfig * mapConfig);
  ~MapDisplayMTA();

  // The states in the state machine.
  // Uncomment these if you wish to define these functions.

  void Init();
  void Active();
  //  void Standby();
  void Shutdown();
  //  void Restart();

  // The message handlers. 
  void InMailHandler(Mail & ml);
  // Mail QueryMailHandler(Mail & ml);

  // the status function.
  //string Status();


  // Any functions which run separate threads
  
  // Method protocols
  void UpdateState(); // grabs from VState and stores in datum.
  void updateHistory(); // makes sure our history does not become too long
private:
  // and a datum named d.
  MapDisplayDatum d;
  MapConfig * m_mapConfig;
  CTraj * m_traj;
  PATH path_history; 
  PATH planned_path;

};

#endif
