/*
** get_vstate.cc
**
*/


#include "LadarFeeder.hh" // standard includes, arbiter, datum, ...

void LadarFeeder::UpdateState()
{ 
  Mail msg = NewQueryMessage( MyAddress(), 
		  MODULES::VState, VStateMessages::GetState);
  Mail reply = SendQuery(msg);

  if (reply.OK() ) {
    reply >> d.SS; // download the new state
  }

} // end LadarFeeder::UpdateState() 
