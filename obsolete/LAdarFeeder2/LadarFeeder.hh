#ifndef LADARFEEDER_HH
#define LADARFEEDER_HH

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "LadarFeederDatum.hh"

using namespace std;
using namespace boost;

class LadarFeeder : public DGC_MODULE 
{
  public:
    // and change the constructors
    LadarFeeder();
    ~LadarFeeder();

    // The states in the state machine.
    // Uncomment these if you wish to define these functions.

    void Init();
    void Active();
    //  void Standby();
    void Shutdown();
    //  void Restart();

    // The message handlers. 
    //  void InMailHandler(Mail & ml);
    //  Mail QueryMailHandler(Mail & ml);

    // the status function.
    //string Status();

    // Any functions which run separate threads

    // Method protocols
    int  InitLadar();
    void LadarGetMeasurements();
    void LadarShutdown();

    /* Sends a query message for a new state struct
       and update the datum with the response. */
    void UpdateState();
    
    void SparrowDisplayLoop();
    void UpdateSparrowVariablesLoop();

  private:
    // and a datum named d.
    LadarFeederDatum d;
};

// enumerate all module messages that could be received
namespace LadarFeederMessages 
{
  enum 
  {
    // we are not yet receiving messages.
    // A place holder
    NumMessages
  };
};






#endif
