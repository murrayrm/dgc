#ifndef LADARFEEDERDATUM_HH
#define LADARFEEDERDATUM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "Constants.h" //vehicle constants- serial ports, ladar consts,etc

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"

//CMAP!
#include "../../CMap/CMapPlus.hh"
#include "../CCorridorPainter/CCorridorPainter.hh"
#include "../CCostPainter/CCostPainter.hh"

using namespace std;

// All data that needs to be shared by common threads
struct LadarFeederDatum 
{

  int save_index;
  /// log_tstamp is the last time stamp that was read in from the log files
  /// (the timestamps should be synchronized between state and scan logs
  double log_tstamp;

  CMapPlus ladarCostMap;

  CCorridorPainter ladarRDDFPainter;

  CCostPainter ladarCostPainter;

  RDDF ladarRDDF;

  int layerIDRDDF;
  int layerIDElev;
  int layerIDCost;
  
  VState_GetStateMsg SS; // the state space struct
};


#endif
