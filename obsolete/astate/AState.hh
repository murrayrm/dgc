/* AState.hh
 *
 * JML 09 Jun 05
 *
 */

#ifndef ASTATE_HH
#define ASTATE_HH

#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <fstream>

#include "VehicleState.hh"

#include "LatLong.h"
#include "frames/frames.hh"
#include "frames/rot_matrix.hh"
#include "gps.hh"
#include "imu_fastcom.h"
#include "sn_msg.hh"
#include "sn_types.h"

#include "astateTabSpecs.hh"
#include "TabClient.h"

#include "DGCutils"

#include "kfilter/kfilter.h"
#include "kfilter/Kalmano.h"
#include "kfilter/Global.h"

#include "SensorConstants.h"
#include "SkynetContainer.h"
#include "CTimberClient.hh"

/*!  MetaState struct.  This is the struct that stores higher level information 
 *   about the status of various components of the stateserver.
 *
 *   At the moment, this struct is neither used nor broadcast.  Its contents are 
 *   still subject to change depending on what we decide fault management might 
 *   need to know.
 */
struct MetaState
{
    unsigned long long Timestamp;    

    int gps_enabled;
    int imu_enabled;
    int mag_enabled;

    int gps_active;
    int imu_active;
    int mag_active;
    
    int gps_pvt_valid;
    int ext_jump_flag;  
};


/*! AState Class.  AState is the State Estimation class for Alice.
 *  GPS, and IMU information are combined through the use of a NGC Kalman filter.
 *
 *  The Kalman filter code is in the kfilter.a library.  It is northrop grumman
 *  proprietary code.  If you need access to it, contact Richard, Jeremy L., or
 *  Alex Fax. Hopefully this code will be replace in the near future.
 *
 *
 *  Main initializes a new instance of AState referred to as ss.
 *
 *  Within the constructor of AState:
 *     First the sensors are initialized through calling:
 *        GPS_init()
 *        IMU_init()
 *        Mag_init()
 *
 *     Then, the following threads are started
 *        GPS_thread() -- Does GPS communication @ ~5 Hz
 *        IMU_thread() -- Does IMU communication @ ~400 Hz
 *        Mag_thread() -- Does Mag communication @ ~0 Hz (No Mag yet)
 *        Sparrow_thread() -- Updates sparrow display variables
 *
 *     If the Kalman filter is enabled, IMU_thread calls UpdateState().  Otherwise
 *     the GPS_thread calls UpdateState().
 *
 *     If state is updated and vehiclestate is populated with new information,
 *     UpdateState() will call Broadcast() which sends out the information to
 *     CStateclient.  CStateClient locally buffers state data and returns it
 *     when requested.  It allows users to either request the most recent state
 *     via UpdateState() or a specific state (based on interpolation between
 *     points) via UpdateState(unsigned long long).
 *
 *     NOTE: This is a DIFFERENT UpdateState() than the one called internally
 *     to AState.
 *
 *     CStateClient is located in /utils/moduleHelpers/StateClient.cc.
 *
 *  The code is broken up as follows:
 *
 *  AState_Main.cc
 *     Does user input, initializes everything, and then waits for quit to
 *     be pressed, at which point it deletes ss and exits.
 *
 *  AState.cc
 *     Constructor
 *     Broadcast()
 *
 *  AState_GPS.cc
 *     GPS_Init()
 *     GPS_Thread()
 *
 *  AState_IMU.cc
 *     IMU_Init()
 *     IMU_Thread()
 *
 *  AState_Mag.cc
 *     Mag_Init()
 *     Mag_Thread()
 *
 *  AState_Sparrow.cc
 *     All Sparrow Interface functionality
 *     Update_Sparrow_Thread()
 *     toggle_stationary and restart_astate are external to astate class
 *
 *  AState_Update.cc
 *     UpdateState() function which deals with Kalman Filter if the Kalman 
 *     Filter is enabled, and otherwise updates state to the best of its ability.
 *     This is where Broadcast() gets called.
 *
 *  \brief This is the class that implements the state server.
 */
class AState : virtual public CSkynetContainer, public CTimberClient, //Timber not in stable branch yet
                public CModuleTabClient
{

    /**************************************************************** 
     *  The following are relevent status flags that can be modified 
     *  by options passed into AState.  These options are parsed as 
     *  part of AState_Main.cc and passed into the AState Constructor.
     ****************************************************************/

    /*! snkey
     *
     *  Key that m_skynet has been initialized with.
     */
    int snkey;

    /*! kfilter_enabled
     *  
     *  Flag listing the status of the K-filter.  
     *  1 = enabled.  0 = disabled. -1 = Couldn't Start. 
     */
    int kfilter_enabled;

    /*! imu_enabled
     *
     *  Flag listing the status of the IMU.  
     *  1 = enabled.  0 = disabled. -1 = Couldn't Start.
     *
     *  This flag gets set in IMU_Init() 
     */
    int imu_enabled;

    /*! imu_enabled
     *
     *  Flag listing the status of the GPS.  
     *  1 = enabled.  0 = disabled. -1 = Couldn't Start.
     *  This flag gets set in GPS_init() 
     */
    int gps_enabled; 

    /*! mag_enabled
     *
     *  Flag listing the status of the Mag.
     *  1 = enabled.  0 = disabled. -1 = Couldn't Start.
     *  This flag gets set in Mag_init()
     */
    int mag_enabled;

    /*! verbose
     *
     *  Flag for verbose messaging
     */
    int verbose;

    /*! log_raw
     *
     *  Flag to specify that raw GPS, IMU, and Magnetometer data is being logged.
     */
    int log_raw;

    /*! replay
     *
     *  Flag to specify that raw GPS, IMU, and Magnetometer data is being logged.
     */
    int replay;

    int debug;



    /*****************************************************************
     *  The following are structs used to store state information
     *  These are broadcast from AState and retrieved via StateClient
     *****************************************************************/

    /*! vehiclestate
     *
     *  Struct to store vehicle state. 
     *  See /constants/VehicleState.hh for specification.
     */
    struct VehicleState vehiclestate;

    /*! metastate
     *
     *  Struct to store meta state. 
     *  Not Yet used or well specified.
     */
    struct MetaState	metastate;



    /*****************************************************************
     *  The following are local data structures associated with GPS,
     *  IMU, and Magnetometer data.
     *****************************************************************/

    /*! starttime
     *  
     *  Time that AState was started.
     */
    unsigned long long starttime;


    /*! my_imudata
     *  
     *  Struct used to store local copy of imu data.
     *  Defined in /drivers/imu_fastcom/imu_fastcom.h
     */
    IMU_DATA my_imudata;

    /*! imu_count
     *
     *  Counter to keep track of total number of IMU reads.
     */
    int imu_count;

    /*! my_imu_time
     *
     *  Time stamp associated with current my_imudata.
     *  Stamp placed AFTER read has been completred.
     */
    unsigned long long my_imu_time;


    /*! my_gpsdata
     *  
     *  Struct used to store local copy of gps data.
     *  Defined in /drivers/gps/gps.hh
     */
    gpsDataWrapper my_gpsdata;

    /*! gps_count
     *
     *  Counter to keep track of total number of GPS reads.
     */
    int gps_count;

    /*! my_gps_time
     *
     *  Time stamp associated with current my_gpsdata.
     *  Stamp recorded AFTER read has been completred.
     */
    unsigned long long my_gps_time;

    /*! my_gps_valid
     *
     *  Flag specifying whether the current message in my_gpsdata is valid.
     */
    int my_gps_valid;

    /*! my_gps_err
     *
     *  GPS err -- corresponds to unpacked GPS_xy_kr 
     *  Units of standard deviation in meters.
     *
     *  gps_err = sqrt(GPS_xy_kr) * EARTH_RADIUS
     *  GPS_xy_kr = pow(gps_err / EARTH_RADIUS, 2)
     */
    double my_gps_err;

    /*! my_gps_north
     *
     *  Unpacked value of gps northing -- assigned to variable to that
     *  it can be sent to sparrow without doubling lat/long to UTM conv.
     */
    double my_gps_north;

    /*! my_gps_north
     *
     *  Unpacked value of gps easting -- assigned to variable to that
     *  it can be sent to sparrow without doubling lat/long to UTM conv.
     */
    double my_gps_east;


    /*! my_mag_heading
     *  
     *  current magnetometer heading.
     */
    double my_mag_heading;

    /*! mag_count
     *
     *  Counter to keep track of total number of Magnetometer reads.
     */
    int mag_count;



    /*
     * THE FOLLOWING VARIABLES ARE GLOBAL SO THEY CAN BE ACCESSED
     * VIA SPARROW.  They are defined in AState.cc.
     *
     *
     * gps_time_offset 
     *
     * This is subtracted off of GPS time before being fed into kfilter.
     *
     *
     * gps_err_const
     *
     * If constant gps_err mode is selected, this is the value used.
     *
     *
     * gps_v_kr_const
     *
     * If constant gps_v_err mode is selected, this is the value used.
     *
     *
     * gps_xy_err_mode
     *
     * Mode for positional gps error.
     *
     *
     * gps_v_err_mode
     *
     * Mode for velocity gps error.
     */



    /*****************************************************************
     *  The following are mutexes used to lock the assorted data
     *  structures being shared between GPS, IMU, Mag, and KFilter
     *****************************************************************/

    /*! m_VehicleStateMutex
     *
     *  Mutex to be locked whenever my_vehiclestate is being accessed. 
     */
    pthread_mutex_t m_VehicleStateMutex;

    /*! m_MetaStateMutex
     *
     *  Mutex to be locked whenever my_metastate is being accessed.
     */
    pthread_mutex_t m_MetaStateMutex;

    /*! m_IMUDataMutex
     *
     *  Mutex to be locked whenever my_imudata is being accessed.
     */
    pthread_mutex_t m_IMUDataMutex;

    /*! m_GPSDataMutex
     *
     *  Mutex to be locked whenever my_gpsdata is being accessed.
     */
    pthread_mutex_t m_GPSDataMutex;

    /*! m_MagDataMutex
     *
     *  Mutex to be locked whenever my_mag_heading is being accessed.
     */
    pthread_mutex_t m_MagDataMutex;

    /*! m_KalmanMutex
     *
     *  Mutex to be locked whenever any part of kfilter code is being
     *  accessed -- either externs or DGCNavInit() or DGCNavRun()
     */
    pthread_mutex_t m_KalmanMutex;



    /*****************************************************************
     *  The following are Miscellanious variables that need to be
     *  persistant between function calls but are mostly stand alone.
     *****************************************************************/

    /*! last_nav_index
     *
     *  Index into NavBuffer from last time state was updated.
     *  This is used to prevent double-updates and acceleration zeroing. 
     */
    int last_nav_index;

    /*! update_counter
     *
     *  Counter used to make sure we only broadcast every 5 state updates.
     */
    int update_counter;

    /*! broadcast_statesock
     *
     *  Socket being used to broadcast state messages on skynet.
     */
    int broadcast_statesock;

    /*! latlong
     *
     *  Class used for lat-long / UTM conversions
     */
    LatLong latlong;
    
    /*! blank
     *
     *  Part of frames code used in reference frame transformations 
     */
    XYZcoord blank;

    /*! imu_offset
     *
     *  Part of frames code used in reference frame transformations 
     */
    XYZcoord imu_offset;

    /*! imu_vector
     *
     *  Part of frames code used in reference frame transformations 
     */
    frames imu_vector;

    /*! imu_correct
     *
     *  Part of frames code used in reference frame transformations 
     */
    NEDcoord imu_correct;



    /*****************************************************************
     *  The following variables all pertain to logging.
     *****************************************************************/

    /*! imu_log_start
     *
     *  Timestamp at beginning of raw imu log.
     */
    unsigned long long imu_log_start;

    /*! gps_log_start
     *
     *  Timestamp at beginning of raw gps log.
     */
    unsigned long long gps_log_start;

    /*
     *  The following are all of the necessary streams and buffers for
     *  Storing file names as pertaining to making logs.
     */

    fstream imu_log_stream;
    fstream imu_replay_stream;
    fstream gps_log_stream;
    fstream gps_replay_stream;
    fstream mag_log_stream;
    fstream mag_replay_stream;

    char imu_log_file[100];
    char imu_replay_file[100];
    char gps_log_file[100];
    char gps_replay_file[100];
    char mag_log_file[100];
    char mag_replay_file[100];

    fstream timberlog_stream;
    char timberlog_file[100];
    ifstream timberplayback_stream;
    char timberplayback_file[100];

    fstream debug_nav_stream;
    char debug_nav_file[100];

    fstream debug_kf_stream;
    char debug_kf_file[100];



    /*****************************************************************
     *  The following are the private methods of the AState class.
     *****************************************************************/

    /*! Function that initializes the IMU */
    void IMU_init();

    /*! Function that initializes the GPS */
    void GPS_init();

    /*! Function that initializes the Mag */
    void Mag_init(double MAG_HDG);

    /*! Function that updates the vehicle_state struct */
    void UpdateState();

    /*! Function to broadcast state data */
    void Broadcast();

    /*! Thread to read from the IMU.  Note: this is where UpdateState normally gets called since it's the fastest thread.*/
    void IMU_thread(); 

    /*! Thread to read from the GPS.*/
    void GPS_thread();

    /*! Thread to read from the Mag.*/
    void Mag_thread();

    /*! Thread to update sparrow variables*/
    void UpdateSparrow_thread();

    /*! Thread to actually update sparrow display*/
    void SparrowDisplayLoop();

    void Playback();

    /*!Used for guiTab.*/
    SastateTabInput  m_input;
    SastateTabOutput m_output;

public:
    /*! stationary
     *  
     *  Flag that gets set when Vehicle is known to be stationary.
     *  Public so that sparrow can access it.
     */
    int stationary;

    /*! AState Constructor */
    AState(int skynet_key, int USE_KF, int USE_IMU, int USE_GPS, 
		    int USE_MAG, double MAG_HDG, int USE_VERBOSE, 
		    int LOG_RAW, char* LOG_FILE, 
		    int REPLAY, char* REPLAY_FILE, int TIMBER_PLAYBACK,
		    int DEBUG, char* DEBUG_FILE);

    /*! Function to restart AState without having to stop/start the
     *  whole system
     */
    void Restart();

    ~AState();
};

#endif
