/* AState_GPS.cc - Functions for dealing with the GPS
 *
 * JML 09 Jun 05
 *
 * */

#include "AState.hh"

//Kalman external variables
extern double GPS_xy_kr;
extern double GPS_v_kr;

// Sparrow input variables
extern int gps_time_offset;	// microseconds time offset (from AState.c)
extern double gps_err_const;
extern double gps_v_kr_const;
extern int gps_xy_err_mode;
extern int gps_v_err_mode;

extern int QUIT_PRESSED;

// Struct for writing raw gps values to file
struct raw_GPS
{
    unsigned long long time;
    gpsData data;
};

void AState::GPS_init()
{
	if (gps_enabled == 0) {
		return;
	}

	//If we are in replay mode, read in first gps reading.
	if (replay == 1) {
		raw_GPS gps_in;
		gps_replay_stream.read((char *)&gps_in, sizeof(raw_GPS));

		my_gps_time = gps_in.time;
		memcpy(&(my_gpsdata.data), &(gps_in.data), sizeof(gpsData));

		gps_log_start = my_gps_time;
	} else {

#ifndef NOSDS
#warning Magic number for GPS port; replace with CONSTANT
		gps_enabled = gps_open(4);
#else
#warning Magic number for GPS port; replace with CONSTANT
		gps_enabled = gps_open(0); // GPS_open needs to return -1 on failure!
#endif

		if(gps_enabled == -1)
		{
			cerr << "AState::GPS_init(): failed to open GPS" << endl;
		}
	}
}


void AState::GPS_thread()
{
	if (gps_enabled == 0) {
		cout << "We need GPS to run! Please don't run in no-gps mode";
		return;
	}

	if (gps_enabled == -1) {
		cout << "Can't run without GPS";
		return;
	}

	//Variables for reading in gps messages
	char *gps_buffer;

	if ((gps_buffer = (char *) malloc(2000)) == NULL)
	{
		cerr << "AState::GPS_thread(): memory allocation error" << endl;
	}

	unsigned long long gps_local_time;

	//Variables for calculating things involving error
	LatLong gps_ll(0,0);
	double dn;
	double de;
	double product;
	double magnitude;
	double angle;
	double jump;

	//Temporary time variable
	unsigned long long nowtime;

	//Variables for reading in raw data
	raw_GPS gps_in;
	raw_GPS gps_out;
	unsigned long long raw_gps_time;
	
	while ( QUIT_PRESSED != 1) {

		if (replay == 1) {
			//If in replay mode read in from stream then wait
			//until we've reached time we would have read point

			gps_replay_stream.read((char *)&gps_in, sizeof(raw_GPS));
			raw_gps_time = gps_in.time - imu_log_start + starttime;

			DGCgettime(nowtime);
			while (raw_gps_time > nowtime) {
				usleep(10);
				DGCgettime(nowtime);
			}
			gps_buffer[3] = GPS_MSG_PVT; // Safe since only record valid PVT

		} else {
			//Otherwise do an actual read.
			gps_read(gps_buffer, 1000);  //Need to determine if this is blocking...
			DGCgettime(gps_local_time); // Time stamp as soon as data read.
		}

		switch (gps_msg_type(gps_buffer)) {
			case GPS_MSG_PVT:

				// LOCK THE MUTEX!
				DGClockMutex(&m_GPSDataMutex);
				if (replay == 1) {
					memcpy(&(my_gpsdata.data), &(gps_in.data), sizeof(gpsData));
					my_gps_time = raw_gps_time;

				} else {

					if (my_gpsdata.update_gps_data(gps_buffer) == -1) {
						//Ignored invalid PVT
						DGCunlockMutex(&m_GPSDataMutex);
						continue;
					}
					my_gps_time = gps_local_time;
				}
				my_gps_valid = (my_gpsdata.data.nav_mode & NAV_VALID) && 1;

				if (log_raw == 1) {
					gps_out.time = my_gps_time;
					memcpy(&(gps_out.data), &(my_gpsdata.data), sizeof(gpsData));
					gps_log_stream.write((char *)&gps_out, sizeof(raw_GPS));
				}

				if (my_gps_valid) {
					//fill in gps_east and gps_north variables
					gps_ll.set_latlon(my_gpsdata.data.lat, my_gpsdata.data.lng);
					gps_ll.get_UTM(&my_gps_east, &my_gps_north);

					DGClockMutex(&m_KalmanMutex); //Changing errors

					if (gps_xy_err_mode == 1) {

						/* 
						* Set the covariance according to the error in data measurements.  
						*
						* In addition, if we detect a large jump in GPS yaw
						* then set the covariance higher so that we don't trust 
						* the measurement as much.
						*/
	
						/* Compute the differences in current and previous position */

						de = my_gps_east - vehiclestate.Easting;
						dn = my_gps_north - vehiclestate.Northing;
						magnitude = sqrt( dn * dn + de * de );
	
						/* See how this compares to the current yaw angle */
						product = (dn * cos(vehiclestate.Yaw) + de * sin(vehiclestate.Yaw)) / magnitude;
						angle = acos(product);
	
						/* Initialize the GPS error to the current value */
						my_gps_err = sqrt(GPS_xy_kr) * EARTH_RADIUS;
	
						/* See if we have encountered a jump (in yaw) */
						if (angle >= ANGLE_CUTOFF) {
							jump = magnitude;
						} else {
							jump = 0;
						}
	
						/* If the jump is bigger than current covariance estimate
						* then recompute the gps error
						*/

						if (jump > my_gps_err) {   
							my_gps_err = magnitude * ERROR_WEIGHT;
						} else {
							/* Decrease the GPS error */
							my_gps_err *= ERROR_FALLOFF;
						}
	
						/* Put a lower bound on the size of the GPS error */
						if (my_gps_err < .2) {
							my_gps_err = .2;
						}  

						/* Now convert the GPS error to the units wanted by KF */
						GPS_xy_kr = pow(my_gps_err / EARTH_RADIUS, 2);

					} else if (gps_xy_err_mode == 2) {

						/* This code attempts to use the actual GPS error
						* as reported by the Navcom (plus a multiplicative
						* fudge factor)
						*/
						my_gps_err = (double)my_gpsdata.data.position_fom / 100.0;
						my_gps_err *= 1.0;

						/* Now convert the GPS error to the units wanted by KF */
						GPS_xy_kr = pow(my_gps_err / EARTH_RADIUS, 2);

					} else {
						my_gps_err = gps_err_const;

						/* Now convert the GPS error to the units wanted by KF */
						GPS_xy_kr = pow(my_gps_err / EARTH_RADIUS, 2);
					}


					/*
					* The following code is used when we are at rest to allow the Kalman
					* filter to rapidly converge on the IMU biases and other parameters
					* 
					* The basic idea is that if we are at rest, then we set the covariance
					* on the GPS velocity to a small number and set the velocities to 
					* identically zero.
					*/
					if (stationary == 1) {
						GPS_v_kr = .001;
						my_gpsdata.data.vel_e = 0;
						my_gpsdata.data.vel_n = 0;
						my_gpsdata.data.vel_u = 0;
					} else {
						/* The default case: if moving, set the covariance appropriately */
						GPS_v_kr = gps_v_kr_const;
					}
					DGCunlockMutex(&m_KalmanMutex);

					++gps_count;
				}
				DGCunlockMutex(&m_GPSDataMutex);
		}
		if (kfilter_enabled != 1) {
			UpdateState();
		}
	}
	free(gps_buffer);
}
