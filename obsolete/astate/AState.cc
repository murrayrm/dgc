/* AState.cc - new vehicle state estimator
 *
 * JML 09 Jun 05
 *
 * */

#include "astateTabSpecs.hh"
#include "AState.hh"

/*
 * The following variables are defined internally to the Kalman Filter
 */

extern double gps_offset_X;
extern double gps_offset_Y;
extern double gps_offset_Z;
extern int inhib_vert_vel;
extern double GPS_xy_kr;
extern double mag_hdg;

/* The following variables are parameters that can be tweaked from
 * the Sparrow display
 */

int gps_time_offset = 0;	// GPS time skew, in microseconds
double gps_err_const = 2;
double gps_v_kr_const = 5.0;
int gps_xy_err_mode = 1;
int gps_v_err_mode = 1;
int use_gps_altitude = 0;
int inhib_vert_vel_value = 0;

// Indicates that quit has been pressed -- all threads should exit nicely.
int QUIT_PRESSED = 0;


AState::AState(int skynet_key, int USE_KF = 1, int USE_IMU = 1, 
		int USE_GPS = 1, int USE_MAG = 1, double MAG_HDG = 0, 
		int USE_VERBOSE = -1, int LOG_RAW = -1, char* LOG_FILE = NULL, 
		int REPLAY = -1, char* REPLAY_FILE = NULL, int TIMBER_PLAYBACK = 0, int DEBUG = 0, char* DEBUG_FILE = NULL)
: CSkynetContainer(SNastate, skynet_key), CTimberClient(timber_types::astate),
	latlong(0,0),
	blank(0,0,0),
	imu_offset(-X_IMU, -Y_IMU, -Z_IMU),
	// These are negative because of way transformation is done
	imu_vector(imu_offset, -PITCH_IMU, -ROLL_IMU, -YAW_IMU),
  CModuleTabClient(&m_input, &m_output)
{
	// Set all of the toggle flags apropriately:
	snkey = skynet_key;
	kfilter_enabled = USE_KF;
	imu_enabled = USE_IMU;
	gps_enabled = USE_GPS;
	mag_enabled = USE_MAG;
	verbose = USE_VERBOSE;
	log_raw = LOG_RAW;
	replay = REPLAY;
	debug = DEBUG;


	// Set up files for raw logging.
	if (log_raw == 1) {
		sprintf(imu_log_file, "%s_IMU.raw", LOG_FILE);
		sprintf(gps_log_file, "%s_GPS.raw", LOG_FILE);
		sprintf(mag_log_file, "%s_MAG.raw", LOG_FILE);
		imu_log_stream.open(imu_log_file, fstream::out | fstream::binary);
		gps_log_stream.open(gps_log_file, fstream::out | fstream::binary);
		mag_log_stream.open(mag_log_file, fstream::out | fstream::binary);
	}

	if (debug == 1) {
		sprintf(debug_nav_file, "%s_nav.dbg", DEBUG_FILE);
		debug_nav_stream.open(debug_nav_file, fstream::out);

		sprintf(debug_kf_file, "%s_kf.dbg", DEBUG_FILE);
		debug_kf_stream.open(debug_kf_file, fstream::out);
	}

	// Set up files for raw replay.
	if (replay == 1) {
		sprintf(imu_replay_file, "%s_IMU.raw", REPLAY_FILE);
		sprintf(gps_replay_file, "%s_GPS.raw", REPLAY_FILE);
		sprintf(mag_replay_file, "%s_MAG.raw", REPLAY_FILE);
		imu_replay_stream.open(imu_replay_file, fstream::in | fstream::binary);
		gps_replay_stream.open(gps_replay_file, fstream::in | fstream::binary);
		mag_replay_stream.open(mag_replay_file, fstream::in | fstream::binary);
	}

	//Find startting time;
	DGCgettime(starttime);

	//Initialize EVERYTHING to be zeroed for good measure.
	vehiclestate.Timestamp = starttime;
	vehiclestate.Northing = 0;
	vehiclestate.Easting = 0;
	vehiclestate.Altitude = 0;
	vehiclestate.Vel_N = 0;
	vehiclestate.Vel_E = 0;
	vehiclestate.Vel_D = 0;
	vehiclestate.Acc_N = 0;
	vehiclestate.Acc_E = 0;
	vehiclestate.Acc_D = 0;
	vehiclestate.Roll = 0;
	vehiclestate.Pitch = 0;
	vehiclestate.Yaw = 0;
	vehiclestate.RollAcc = 0;
	vehiclestate.PitchAcc = 0;
	vehiclestate.YawAcc = 0;

	my_imudata.dvx = 0;
	my_imudata.dvy = 0;
	my_imudata.dvz = 0;
	my_imudata.dtx = 0;
	my_imudata.dty = 0;
	my_imudata.dtz = 0;

	my_gpsdata.data.altitude = 0;
	my_gpsdata.data.vel_n = 0;
	my_gpsdata.data.vel_e = 0;

	my_gps_north = 0;
	my_gps_east = 0;
	my_gps_err = 0;
	my_gps_valid = 0;

	my_mag_heading = 0;

	stationary = 0;

	imu_count = 0;
	gps_count = 0;
	mag_count = 0;

	last_nav_index = 0;
	update_counter = -1;

	//Create mutexes:
	DGCcreateMutex(&m_VehicleStateMutex);
	DGCcreateMutex(&m_MetaStateMutex);
	DGCcreateMutex(&m_GPSDataMutex);
	DGCcreateMutex(&m_IMUDataMutex);
	DGCcreateMutex(&m_KalmanMutex);

	//Set up socket for skynet broadcasting:
	broadcast_statesock = m_skynet.get_send_sock(SNstate);
	if(broadcast_statesock < 0)
		cerr << "AState::VehicleStateMsg_thread(): skynet get_send_sock returned error" << endl;

	if (TIMBER_PLAYBACK == 1) {
		DGCstartMemberFunctionThread(this, &AState::Playback);
	} else {

	//Initialize sensors:
	IMU_init(); 	   // This will set imu_enabled
	GPS_init();        // This will set gps_enabled
	Mag_init(MAG_HDG); // This will set mag_enabled


	//Initialize Kalmanfilter:
	
	// Set appropriate gps offsets.
	// These values should be GPS in IMU reference frame

	gps_offset_X = X_GPS - X_IMU;
	gps_offset_Y = Y_GPS - Y_IMU;
	gps_offset_Z = Z_GPS - Z_IMU;

	// Only need to run DGCNavInit if the Kalman filter is enabled
	if(kfilter_enabled == 1)
	    DGCNavInit();

	// Inhibit vertical velocity
    	inhib_vert_vel = inhib_vert_vel_value;

	//Populate metastate struct once we know atually gps/imu/mag status
	metastate.gps_enabled = gps_enabled;
	metastate.imu_enabled = imu_enabled;
	metastate.mag_enabled = mag_enabled;
	metastate.gps_pvt_valid = 0;
	metastate.ext_jump_flag = 0;


	//Start sensor threads
	// NOTE: IMU_thread() calls UpdateState() which calls broadcast() 
	//       which broadcasts state
	
	DGCstartMemberFunctionThread(this, &AState::GPS_thread);
	DGCstartMemberFunctionThread(this, &AState::Mag_thread);
	DGCstartMemberFunctionThread(this, &AState::IMU_thread);

	//Start sparrow threads
	DGCstartMemberFunctionThread(this, &AState::UpdateSparrow_thread);
	DGCstartMemberFunctionThread(this, &AState::SparrowDisplayLoop);
	}
}

void AState::Restart() {

	DGClockMutex(&m_GPSDataMutex);
	DGClockMutex(&m_IMUDataMutex);
	DGClockMutex(&m_MagDataMutex);
	DGClockMutex(&m_VehicleStateMutex);

	DGCgettime(starttime);

	//Initialize EVERYTHING to be zeroed for good measure.
	vehiclestate.Timestamp = starttime;

	//Save old vehicle orientation to re-initialize with.
	my_mag_heading = vehiclestate.Yaw;
  	mag_hdg = vehiclestate.Yaw;

	//Re-zero everything else
	vehiclestate.Northing = 0;
	vehiclestate.Easting = 0;
	vehiclestate.Altitude = 0;
	vehiclestate.Vel_N = 0;
	vehiclestate.Vel_E = 0;
	vehiclestate.Vel_D = 0;
	vehiclestate.Acc_N = 0;
	vehiclestate.Acc_E = 0;
	vehiclestate.Acc_D = 0;
	vehiclestate.Roll = 0;
	vehiclestate.Pitch = 0;
	vehiclestate.Yaw = 0;
	vehiclestate.RollAcc = 0;
	vehiclestate.PitchAcc = 0;
	vehiclestate.YawAcc = 0;

	my_imudata.dvx = 0;
	my_imudata.dvy = 0;
	my_imudata.dvz = 0;
	my_imudata.dtx = 0;
	my_imudata.dty = 0;
	my_imudata.dtz = 0;

	my_gps_north = 0;
	my_gps_east = 0;
	my_gpsdata.data.altitude = 0;
	my_gpsdata.data.vel_n = 0;
	my_gpsdata.data.vel_e = 0;
	my_gps_err = 0;
  	my_gps_valid = 0;
  	stationary = 0;

	imu_count = 0;
	gps_count = 0;
	mag_count = 0;

	last_nav_index = 0;
	update_counter = 0;

	//Lock the KFilter Mutex and re-init Kfilter
	DGClockMutex(&m_KalmanMutex);

	if(kfilter_enabled == 1)
	    DGCNavInit();

    	inhib_vert_vel = inhib_vert_vel_value;

	DGCunlockMutex(&m_KalmanMutex);

	DGCunlockMutex(&m_GPSDataMutex);
	DGCunlockMutex(&m_IMUDataMutex);
	DGCunlockMutex(&m_MagDataMutex);
	DGCunlockMutex(&m_VehicleStateMutex);
}

AState::~AState()
{
    if (log_raw == 1) {
        imu_log_stream.close();
        gps_log_stream.close();
        mag_log_stream.close();
    }

    if (replay == 1) {
        imu_replay_stream.close();
        gps_replay_stream.close();
        mag_replay_stream.close();
    }

    if (debug == 1) {
	    debug_nav_stream.close();
	    debug_kf_stream.close();

    }
	cout << "Thankyou for using AState..." << endl;
}

// VehicleState messaging Method.
void AState::Broadcast()
{
	pthread_mutex_t* pMutex = &m_VehicleStateMutex;
		if(m_skynet.send_msg(broadcast_statesock,
					&vehiclestate, 
					sizeof(vehiclestate), 
					0, 
					&pMutex) 
				!= sizeof(vehiclestate))
		{
			cerr << "AState::Broadcast(): didn't send right size state message" << endl;
		}
		if(getLoggingEnabled()) {
			if(checkNewDirAndReset()) {
				cout << "Opening new file" << endl;
				if (timberlog_stream.is_open()) {
					timberlog_stream.close();
				}
				sprintf(timberlog_file, "%sstate.dat",getLogDir().c_str());
				timberlog_stream.open(timberlog_file, fstream::out|fstream::app);
			}
			timberlog_stream.precision(10);
			timberlog_stream <<  vehiclestate.Timestamp << '\t';
			timberlog_stream <<  vehiclestate.Northing << '\t' << vehiclestate.Easting << '\t' << vehiclestate.Altitude << "\t";
			timberlog_stream <<  vehiclestate.Vel_N << '\t' << vehiclestate.Vel_E << '\t' << vehiclestate.Vel_D << "\t";
			timberlog_stream <<  vehiclestate.Acc_N << '\t' << vehiclestate.Acc_E << '\t' << vehiclestate.Acc_D << "\t";
			timberlog_stream <<  vehiclestate.Roll << '\t' << vehiclestate.Pitch << '\t' << vehiclestate.Yaw << "\t";
			timberlog_stream <<  vehiclestate.RollRate << '\t' << vehiclestate.PitchRate << '\t' << vehiclestate.YawRate << "\t";
			timberlog_stream <<  vehiclestate.RollAcc << '\t' << vehiclestate.PitchAcc << '\t' << vehiclestate.YawAcc << endl;
		}
}

void AState::Playback()
{
	unsigned long long playbacktime;
	sprintf(timberplayback_file, "%sstate.dat",getPlaybackDir().c_str());
	cout << "Opening file... " << timberplayback_file << endl;
	timberplayback_stream.open(timberplayback_file);
	vehiclestate.Timestamp = 0;
	unsigned long long lastTime = 0;
	while (1) {
		if(checkNewPlaybackDirAndReset()) {
			if (timberplayback_stream.is_open()) {
				timberplayback_stream.close();
			}
			sprintf(timberplayback_file, "%sstate.dat",getPlaybackDir().c_str());
			cout << "Re-Opening file... " << timberplayback_file << endl;
			timberplayback_stream.open(timberplayback_file);
		} 
		if (!timberplayback_stream) {
		  cout << __FILE__ << " [" << __LINE__ 
		       << "]: Reached the end of the playback file, or couldn't open it to begin with" << endl;
		  exit(0);
		} else {
		  unsigned long long timberTime = getPlaybackTime();
		  if(timberTime < lastTime) {
		    if(timberplayback_stream.is_open()) {
		      timberplayback_stream.close();
		    }
		    cout << __FILE__ << "[" << __LINE__ << "]: Rewinding!" << endl;
		    timberplayback_stream.open(timberplayback_file);
		    vehiclestate.Timestamp = 0;
		  }
		  while(timberTime >= vehiclestate.Timestamp && timberplayback_stream) {
		    timberplayback_stream >> vehiclestate.Timestamp;
		    timberplayback_stream >> vehiclestate.Northing;
		    timberplayback_stream >> vehiclestate.Easting;
		    timberplayback_stream >> vehiclestate.Altitude;
		    timberplayback_stream >> vehiclestate.Vel_N;
		    timberplayback_stream >> vehiclestate.Vel_E;
		    timberplayback_stream >> vehiclestate.Vel_D;
		    timberplayback_stream >> vehiclestate.Acc_N;
		    timberplayback_stream >> vehiclestate.Acc_E;
		    timberplayback_stream >> vehiclestate.Acc_D;
		    timberplayback_stream >> vehiclestate.Roll;
		    timberplayback_stream >> vehiclestate.Pitch;
		    timberplayback_stream >> vehiclestate.Yaw;
		    timberplayback_stream >> vehiclestate.RollRate;
		    timberplayback_stream >> vehiclestate.PitchRate;
		    timberplayback_stream >> vehiclestate.YawRate;
		    timberplayback_stream >> vehiclestate.RollAcc;
		    timberplayback_stream >> vehiclestate.PitchAcc;
		    timberplayback_stream >> vehiclestate.YawAcc;
		  }
		  lastTime = vehiclestate.Timestamp;
		  playbacktime = getPlaybackTime();
		  
		  if (vehiclestate.Timestamp > playbacktime) {
		    blockUntilTime(vehiclestate.Timestamp);
		    Broadcast();
		  }
		}
	}
}
