ASTATE_PATH = $(DGC)/modules/astate

ASTATE_DEPEND_LIBS = $(SKYNETLIB) \
				$(MODULEHELPERSLIB) \
				$(SERIALLIB) \
				$(SPARROWLIB) \
				$(GGISLIB) \
				$(GPSLIB) \
				$(GPSSDSLIB) \
				$(SERIALLIB) \
				$(SDSLIB) \
				$(FRAMESLIB) \
				$(IMULIB) \
				$(KFLIB)

ASTATE_DEPEND_SOURCES = $(ASTATE_PATH)/AState.cc \
		$(ASTATE_PATH)/AState.hh \
		$(ASTATE_PATH)/AState_GPS.cc \
		$(ASTATE_PATH)/AState_IMU.cc \
		$(ASTATE_PATH)/AState_Mag.cc \
		$(ASTATE_PATH)/AState_Main.cc \
		$(ASTATE_PATH)/AState_test.cc \
		$(ASTATE_PATH)/AState_test.hh \
		$(ASTATE_PATH)/astateTabSpecs.hh

ASTATE_DEPEND = $(ASTATE_DEPEND_LIBS) $(ASTATE_DEPEND_SOURCES)
