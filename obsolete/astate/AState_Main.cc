#include <getopt.h>
#include "AState.hh"
#include <stdlib.h>
#include "DGCutils"

void print_usage() {
	cout << "Usage: astate [options]" << endl;
	cout << '\t' << "--help, -h" << '\t' << "Display this message" << endl;
	cout << '\t' << "--noimu, -i" << '\t' << "Disable IMU" << endl;
	cout << '\t' << "--nogps, -g" << '\t' << "Disable GPS" << endl;
	cout << '\t' << "--nomag, -m <heading>" << '\t' << "Disable Mag and start with <heading>" << endl;
	cout << '\t' << "--nokf -k" << '\t' << "Disable the kalman filter" << endl;
	cout << '\t' << "--verbose, -v" << '\t' << "Enable verbose mode" << endl;
	cout << '\t' << "--snkey, -s <snkey>" << '\t' << "Use skynetkey <snkey> (defaults to $SKYNET_KEY)" << endl;
	cout << '\t' << "--log, -l " << '\t' << "Log raw data to /tmp/log/astate_raw/<timestamp>" << endl;
	cout << '\t' << "--replay, -r <logfile>" << '\t' << "Replay raw data from <logfile>" << endl;
	cout << '\t' << "-p" << "Start astate in timber playback mode" << endl;
	cout << '\t' << "-d" << "Log debug information" << endl;
}

const char* const short_options = "higm:kvs:lr:pd";

static struct option long_options[] =
{
	//first: long option (--option) string
	//second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
	//third: if pointer, set variable to value of fourth argument
	//	 if NULL, getopt_long returns fourth argument
	{"help",	0,	NULL,	'h'},
	{"noimu",	0,	NULL,	'i'},
	{"nogps",	0,	NULL,	'g'},
	{"nomag",	1,	NULL,	'm'},
	{"m",	1,	NULL,	'm'},
	{"nokf",	0,	NULL,	'k'},
	{"verbose",	0,	NULL,	'v'},
	{"skynet",	1,	NULL,	's'},
	{"s",	1,	NULL,	's'},
	{"log",	0,	NULL,	'l'},
	{"l",	0,	NULL,	'l'},
	{"replay",	1,	NULL,	'r'},
	{"r",	1,	NULL,	'r'},
	{"p",   0,	NULL,   'p'},
	{"d",   0,	NULL,   'd'},
	{0,0,0,0}
};

int USE_IMU = 1;
int USE_GPS = 1;
int USE_MAG = 1;
double MAG_HDG = 0;
int USE_KF = 1;
int GOT_SN = 0;
int USE_VERBOSE = 0;
int LOG_RAW = 0;
char LOG_FILE[100];
int REPLAY = 0;
char REPLAY_FILE[100];
int TIMBER_PLAYBACK = 0;
int DEBUG = 0;
char DEBUG_FILE[100];

// Pointer to astate server.
AState *ss;

// Global QUIT_PRESSED variable.
extern int QUIT_PRESSED;

int main(int argc, char **argv)
{
	int sn_key = 0;
	int options_index;

	int ch;
				time_t t = time(NULL);
				tm *local;

	while((ch = getopt_long(argc, argv, short_options, long_options, &options_index)) != -1)
	{
		switch(ch)
		{
			case 'h':
				print_usage();
				return 0;
				break;
			case 'i':
				USE_IMU = 0;
				break;
			case 'g':
				USE_GPS = 0;
				break;
			case 'm':
				USE_MAG = 0;
				MAG_HDG = atof(optarg);
				break;
			case 'k':
				USE_KF = 0;
				break;
			case 'v':
				USE_VERBOSE = 1;
				break;
			case 's':
				GOT_SN = 1;
				sn_key = atoi(optarg);
				break;
			case 'l':
				LOG_RAW = 1;
				local = localtime(&t);
				mkdir("/tmp/logs/astate_raw/", S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IXOTH);
				sprintf(LOG_FILE, "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d",
						local->tm_year+1900, local->tm_mon+1, local->tm_mday,
						local->tm_hour, local->tm_min, local->tm_sec);
				break;
			case 'd':
				DEBUG = 1;
				local = localtime(&t);
				mkdir("/tmp/logs/astate_raw/", S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IXOTH);
				sprintf(DEBUG_FILE, "/tmp/logs/astate_raw/d%04d%02d%02d_%02d%02d%02d",
						local->tm_year+1900, local->tm_mon+1, local->tm_mday,
						local->tm_hour, local->tm_min, local->tm_sec);
				break;
			case 'r':
				REPLAY = 1;
				strncpy(REPLAY_FILE, optarg, 99);
				break;
			case 'p':
				TIMBER_PLAYBACK = 1;
				break;
            default:
                break;
		}
	}

	char* pSkynetkey = getenv("SKYNET_KEY");
	if (GOT_SN == 0) {
		if ( pSkynetkey == NULL )
		{
			cerr << "SKYNET_KEY environment variable isn't set." << endl;
		} else 
		{
			sn_key = atoi(pSkynetkey);
		}
	}



	cerr << "Constructing skynet with KEY = " << sn_key << endl;

	//Create AState server:
	ss = new AState(sn_key,USE_KF,USE_IMU,USE_GPS,USE_MAG,
			MAG_HDG,USE_VERBOSE,
			LOG_RAW,LOG_FILE,REPLAY,REPLAY_FILE,TIMBER_PLAYBACK,DEBUG,DEBUG_FILE);

	// Wait for QUIT_PRESSED to be 1.
	while (QUIT_PRESSED != 1) {
		sleep(1);
	}

	// Give all other threads a chance to end:
	sleep(1);

	delete ss;

	return 0;
}
