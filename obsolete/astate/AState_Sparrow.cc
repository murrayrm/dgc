#include "AState.hh"
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

extern int QUIT_PRESSED;

extern AState *ss;

//Struct containing all possible pieces of data being outputted
struct AState_Info
{
	char   gps_stat[10];
	double gps_err;
	double gps_n;
	double gps_e;
	double gps_a;
	double gps_vn;
	double gps_ve;

	char   imu_stat[10];
	double imu_dvx;
	double imu_dvy;
	double imu_dvz;
	double imu_dtx;
	double imu_dty;
	double imu_dtz;

	char   mag_stat[10];
	double mag_head;

	char   kf_stat[10];
	int    imu_count;
	int    gps_count;
	int    mag_count;

	int    Timestamp;

	VehicleState vstate;

	int    snkey;

	int    stat;
	int nav_mode;
	char gps_mstr[128];	// buffer for message

	double ibx;
	double iby;
	double ibz;
};

extern double tot_kabx;
extern double tot_kaby;
extern double tot_kabz;

//SParrow changeable parameters:
extern int gps_time_offset;	// offset of GPS timing info
extern double gps_err_const;
extern double gps_v_kr_const;
extern double gps_xy_err_mode;
extern double gps_v_err_mode;
extern int use_gps_altitude;
extern int inhib_vert_vel_value;

AState_Info display_vars;

int toggle_stationary(long);
int restart_astate(long);

#include "astune.h"		// table for tunning params
#include "asdisp.h"		// main display table

//Populate display_vars struct
void AState::UpdateSparrow_thread() 
{
  static int old_mode = 0;

	while( QUIT_PRESSED != 1 )
	{
		if (gps_enabled == 1)
		{
			if (my_gps_valid == 1)
			{
				strcpy(display_vars.gps_stat,"valid PVT");
			} else {
				strcpy(display_vars.gps_stat,"invalid PVT");
			}
		} else {
			strcpy(display_vars.gps_stat,"GPS ERROR");
		}
		m_input.navmode = display_vars.nav_mode = my_gpsdata.data.nav_mode & 0xff;
		if (display_vars.nav_mode != old_mode) {
		  /* Update the message that is displayed */
		  strcpy(display_vars.gps_mstr, 
			 gps_mode(my_gpsdata.data.nav_mode));
		  // dd_refresh(GPSMSG);

		  /* Save the mode */
		  old_mode = display_vars.nav_mode;
		}
		//strcpy(m_input.rawGPS, display_vars.gps_stat);

		m_input.error = display_vars.gps_err = my_gps_err;
		m_input.gpsN = display_vars.gps_n = my_gps_north;
		m_input.gpsE = display_vars.gps_e = my_gps_east;
		m_input.gpsA = display_vars.gps_a = my_gpsdata.data.altitude;
		m_input.gpsVN = display_vars.gps_vn = my_gpsdata.data.vel_n;
		m_input.gpsVE = display_vars.gps_ve = my_gpsdata.data.vel_e;

		if (imu_enabled == 1) {
			strcpy(display_vars.imu_stat,"active");
		} else {
			strcpy(display_vars.imu_stat,"inactive");
		}
		//strcpy(m_input.rawIMU, display_vars.imu_stat);

		m_input.dvx = display_vars.imu_dvx = my_imudata.dvx;
		m_input.dvy = display_vars.imu_dvy = my_imudata.dvy;
		m_input.dvz = display_vars.imu_dvz = my_imudata.dvz;
		m_input.dtx = display_vars.imu_dtx = my_imudata.dtx;
		m_input.dty = display_vars.imu_dty = my_imudata.dty;
		m_input.dtz = display_vars.imu_dtz = my_imudata.dtz;

		if (mag_enabled == 1) {
			strcpy(display_vars.mag_stat,"active");
		} else {
			strcpy(display_vars.mag_stat,"inactive");
		}
		//strcpy(m_input.rawMag, display_vars.mag_stat);

		m_input.heading = display_vars.mag_head = my_mag_heading;

		if (kfilter_enabled == 1) {
			strcpy(display_vars.kf_stat,"active");
		} else {
			strcpy(display_vars.kf_stat,"inactive");
		}
		//strcpy(m_input.kFilter, display_vars.kf_stat);

		m_input.imuCount = display_vars.imu_count = imu_count;
		m_input.gpsCount = display_vars.gps_count = gps_count;
		m_input.magCount = display_vars.mag_count = mag_count;

		m_input.timestamp = display_vars.Timestamp = vehiclestate.Timestamp - starttime;
		display_vars.vstate = vehiclestate;
		m_input.N = vehiclestate.Northing;
		m_input.E = vehiclestate.Easting;
		m_input.D = vehiclestate.Altitude;
		m_input.R = vehiclestate.Roll;
		m_input.P = vehiclestate.Pitch;
		m_input.Y = vehiclestate.Yaw;
		m_input.dN = vehiclestate.Vel_N;
		m_input.dE = vehiclestate.Vel_E;
		m_input.dD = vehiclestate.Vel_D;
		m_input.dR = vehiclestate.RollRate;
		m_input.dP = vehiclestate.PitchRate;
		m_input.dY = vehiclestate.YawRate;
		m_input.ddN = vehiclestate.Acc_N;
		m_input.ddE = vehiclestate.Acc_E;
		m_input.ddD = vehiclestate.Acc_D;
		m_input.ddR = vehiclestate.RollAcc;
		m_input.ddP = vehiclestate.PitchAcc;
		m_input.ddY = vehiclestate.YawAcc;

		m_input.skynet = display_vars.snkey = snkey;

		m_input.stationary = display_vars.stat = stationary;

		m_input.ibx = display_vars.ibx = tot_kabx;
		m_input.iby = display_vars.iby = tot_kaby;
		m_input.ibz = display_vars.ibz = tot_kabz;

		usleep(100000);
	}

}

//Sparrow display loop
void AState::SparrowDisplayLoop() {
	dbg_all = 0;

	usleep(1000000);

	if (dd_open() < 0) exit(1);

	dd_usetbl(asdisp);

	dd_loop();
	dd_close();
	QUIT_PRESSED = 1;
}

//Toggle Stationary mode
int toggle_stationary(long arg) {
	if ((*ss).stationary == 1) {
		(*ss).stationary = 0;
	} else {
		(*ss).stationary = 1;
	}
	return 1;
}

int restart_astate(long arg) {
	(*ss).Restart();
	return 1;
}
