/* AState_IMU.cc - new vehicle state estimator
 *
 * JML 09 Jun 05
 *
 * */

#include "AState.hh"

struct raw_IMU
{
    unsigned long long time;
    IMU_DATA data;
};

extern int QUIT_PRESSED;

void AState::IMU_init()
{
	if(imu_enabled == 0)
	{
		return;
	}
        if (replay == 1) {
                raw_IMU imu_in;

                imu_replay_stream.read((char*)&imu_in, sizeof(raw_IMU));

		memcpy(&my_imudata, &(imu_in.data), sizeof(IMU_DATA));
                imu_log_start = imu_in.time;

        } else {

	imu_enabled = IMU_open();
	if(imu_enabled == -1)
		cerr << "AState::IMU_init(): failed to open IMU" << endl;

        }
}

void AState::IMU_thread()
{
	unsigned long long nowtime;

	raw_IMU imu_in;
	raw_IMU imu_out;

	if (imu_enabled == 0) {
		return;
	} else if (imu_enabled == -1) {
		cerr << "AState::IMU_thread(): not started because IMU failed to start";
		return;
	}
	while ( QUIT_PRESSED != 1) {
		DGClockMutex(&m_IMUDataMutex);
		if (replay == 1) 
		{
			imu_replay_stream.read((char*)&imu_in, sizeof(raw_IMU));

			memcpy(&my_imudata, &(imu_in.data), sizeof(IMU_DATA));

			my_imu_time = imu_in.time - imu_log_start + starttime;

			DGCgettime(nowtime);
			//Wait until time has caught up with raw input
			while( my_imu_time > nowtime ) {
				usleep(1);
				DGCgettime(nowtime);
			}
		} else {
			IMU_read(&my_imudata); // Need to add new code for IMU_read!
			DGCgettime(my_imu_time); // time stamp as soon as data read.
		}
		if (log_raw == 1)
		{
			imu_out.time = my_imu_time;

			memcpy(&(imu_out.data), &my_imudata, sizeof(IMU_DATA));

			imu_log_stream.write((char*)&imu_out, sizeof(raw_IMU));
		}
		++imu_count;
		DGCunlockMutex(&m_IMUDataMutex);

		if (kfilter_enabled == 1) 
		{
			//Only update state if kfilter is enabled
			UpdateState();
		}
	}
}
