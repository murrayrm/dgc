/* AState.cc - new vehicle state estimator
 *
 * JML 09 Jun 05
 *
 * */

#include "AState.hh"

// Externally defined Kalman Filter variables
extern int NavBufferIndex;
extern TSaveNavData NavBuffer[XRATIO2];

extern double gps_lat, gps_lon, gps_alt;
extern double gps_vn, gps_ve, gps_vu;
extern double gps_time;
extern double imu_time;
extern double mag_hdg;

extern int New_GPS_data_flag;   // flag set when new GPS data comes in
extern int New_Mag_data_flag;

extern double imu_dvx,imu_dvy,imu_dvz; // Delta-v's m/s (should be)
extern double imu_dtx,imu_dty,imu_dtz; // Delta-Thetas,should be rads

extern double kp[NUM_ST+1][NUM_ST+1];  // covariance matrix
extern double kx[NUM_ST+1];            // kalman gains
extern double kq[NUM_ST+1];            // kalman gains

extern double tot_kabx, tot_kaby, tot_kabz;
extern double tot_ksfx, tot_ksfy, tot_ksfz;
extern double tot_kgbx, tot_kgby, tot_kgbz;
extern double tot_kgsfx;

extern double ExtGPSPosMeas_x, ExtGPSPosMeas_y, ExtGPSPosMeas_z;
extern double ExtGPSVelMeas_x, ExtGPSVelMeas_y, ExtGPSVelMeas_z;

extern double GPS_xy_kr, GPS_h_kr, GPS_v_kr; // errors

extern int inhib_vert_vel; 

extern int gps_time_offset;

extern int use_gps_altitude;
extern int inhib_vert_vel_value;

// Method to Update State

/*
 * AN EXPLANATION OF KALMAN FILTER
 *
 * The Kalman filter is proprietary Northrop Grumman Code.  It is in an external
 * library, kfilter.a.  We interface to it by setting:
 *
 * gps_lat, gps_lon, gps_alt, gps_vn, gps_ve, gps_vu
 * imu_dvx, imu_dvy, imu_dvz, imu_dtx, imu_dty, imu_dtz
 *
 * GPS_xy_kr is the ERROR for the gps position
 * GPS_v_kr is the ERROR for the gps velocity
 *
 * UpdateState() calls DGCNavRun() which takes care of all of the strap-down
 * navigation and kalman filtering for us.
 *
 * The most recent solution is located in NavBuffer[NavBufferIndex]
 *
 * UpdateState() takes the information from the navbuffer and puts it back
 * into vehiclestate so that it can then be broadcast.
 */

void AState::UpdateState()
{
	double timediff; // Used in crude acceleration calculation

	//IF KFILTER ENABLED:
	if (kfilter_enabled == 1)
	{
		//Fill our external variables
		
		DGClockMutex(&m_IMUDataMutex);
		DGClockMutex(&m_KalmanMutex);

		imu_time = my_imu_time / 1e6; //convert from usec to sec
      		imu_dvx = my_imudata.dvx;
      		imu_dvy = my_imudata.dvy;
      		imu_dvz = my_imudata.dvz;
      		imu_dtx = my_imudata.dtx;
      		imu_dty = my_imudata.dty;
      		imu_dtz = my_imudata.dtz;

		DGCunlockMutex(&m_KalmanMutex);
		DGCunlockMutex(&m_IMUDataMutex);

		//Whenever new magdata is requested, just tell them we have it
		if (New_Mag_data_flag == 0) {
				New_Mag_data_flag = 1;
				mag_count++;
		}
	
		/*
		 * KFilter will set New_GPS_data_flag to 0 once they have
		 * used the last data that was past in.
		 *
		 * Here we check to see that new data is wanted and that
		 * the current value we have is valid.
		 */

		if ((New_GPS_data_flag == 0) && (my_gps_valid)) {

			DGClockMutex(&m_GPSDataMutex);
			DGClockMutex(&m_KalmanMutex);

	  		gps_time = (my_gps_time - gps_time_offset) / 1e6;
			
	  		gps_lat = my_gpsdata.data.lat*DEG2RAD;
	  		gps_lon = my_gpsdata.data.lng*DEG2RAD;
	  		gps_alt = my_gpsdata.data.altitude;

	    		gps_vu  = my_gpsdata.data.vel_u;
	    		gps_vn  = my_gpsdata.data.vel_n;
	    		gps_ve  = my_gpsdata.data.vel_e;

	  		New_GPS_data_flag = 1; // Notify that we have new data

			DGCunlockMutex(&m_KalmanMutex);
			DGCunlockMutex(&m_GPSDataMutex);
		}

		inhib_vert_vel = inhib_vert_vel_value; // Inhib vert velocity.


		//Run the Kalman Filter:
		DGClockMutex(&m_KalmanMutex);

		DGCNavRun();

		DGCunlockMutex(&m_KalmanMutex);

		//Put information back in vehiclestate struct
		if (++update_counter % 10 == 0) // Make sure NavBuffer Updated
		{
			if (debug) {
				debug_nav_stream.precision(20);

				//debug_stream << NavBufferIndex << '\t';

				debug_nav_stream << (double) my_imu_time / 1e6 << '\t';

				debug_nav_stream << NavBuffer[NavBufferIndex].time << '\t';

				debug_nav_stream << NavBuffer[NavBufferIndex].vx << '\t';
				debug_nav_stream << NavBuffer[NavBufferIndex].vy << '\t';
				debug_nav_stream << NavBuffer[NavBufferIndex].vz << '\t';

				debug_nav_stream << NavBuffer[NavBufferIndex].vn << '\t';
				debug_nav_stream << NavBuffer[NavBufferIndex].ve << '\t';
				debug_nav_stream << NavBuffer[NavBufferIndex].vd << '\t';

				debug_nav_stream << NavBuffer[NavBufferIndex].lat << '\t';
				debug_nav_stream << NavBuffer[NavBufferIndex].lon << '\t';
				debug_nav_stream << NavBuffer[NavBufferIndex].alt << '\t';

				debug_nav_stream << NavBuffer[NavBufferIndex].xApplyDpx << '\t';
				debug_nav_stream << NavBuffer[NavBufferIndex].xApplyDpy << '\t';
				debug_nav_stream << NavBuffer[NavBufferIndex].xApplyDh << '\t';

				debug_nav_stream << NavBuffer[NavBufferIndex].xApplyDvx << '\t';
				debug_nav_stream << NavBuffer[NavBufferIndex].xApplyDvy << '\t';
				debug_nav_stream << NavBuffer[NavBufferIndex].xApplyDvz << '\t';

				debug_nav_stream << NavBuffer[NavBufferIndex].roll << '\t';
				debug_nav_stream << NavBuffer[NavBufferIndex].pitch << '\t';
				debug_nav_stream << NavBuffer[NavBufferIndex].yaw << '\t';
				debug_nav_stream << NavBuffer[NavBufferIndex].thdg << '\t';

				debug_nav_stream << NavBuffer[NavBufferIndex].KalmanUpdate << endl;

				if (update_counter == 400) {

					debug_kf_stream.precision(20);
					
					debug_kf_stream << NavBuffer[NavBufferIndex].time << '\t';

					for (int ind = 1;ind < 30;ind++) {
						debug_kf_stream << kx[ind] << '\t';
					}

					for (int ind = 1;ind < 30;ind++) {
						debug_kf_stream << kp[ind][ind] << '\t';
					}

					for (int ind = 1;ind < 30;ind++) {
						debug_kf_stream << kq[ind] << '\t';
					}

					debug_kf_stream << endl;

					update_counter = 0;
				}

			}
			// Lock the vehiclestate mutex
			DGClockMutex(&m_VehicleStateMutex);
			// Lock the KalmanMutex since accessing NavBuffer
			DGClockMutex(&m_KalmanMutex);

			timediff = imu_time - (vehiclestate.Timestamp / 1e6);

			// Accelerations (done first since referencing prior time)
#warning "Acc values should be read directly from IMU"
			vehiclestate.Acc_N = (NavBuffer[NavBufferIndex].vn - vehiclestate.Vel_N) / timediff;
			vehiclestate.Acc_E = (NavBuffer[NavBufferIndex].ve - vehiclestate.Vel_E) / timediff;
			vehiclestate.Acc_D = (NavBuffer[NavBufferIndex].vd - vehiclestate.Vel_D) / timediff;
			vehiclestate.RollAcc = (vehiclestate.RollRate - 400*imu_dtx) / timediff;
			vehiclestate.PitchAcc = (vehiclestate.PitchRate + 400*imu_dty) / timediff;
			vehiclestate.YawAcc = (vehiclestate.YawRate + 400*imu_dtz) / timediff;

			// Velocities
#warning "Velocity values need to be transformed too!"
			vehiclestate.Vel_N = NavBuffer[NavBufferIndex].vn;
			vehiclestate.Vel_E = NavBuffer[NavBufferIndex].ve;
			vehiclestate.Vel_D = NavBuffer[NavBufferIndex].vd;

			// No tranformation necessary here since IMU lines up w/ vehicle
			vehiclestate.RollRate = my_imudata.dtx * 400;
			vehiclestate.PitchRate = my_imudata.dty * -400;
			vehiclestate.YawRate = my_imudata.dtz * -400;

			// This transformation is bad and needs to be replaced
		
			//Set Roll, Pitch, and Yaw:
			vehiclestate.Roll =  NavBuffer[NavBufferIndex].roll - ROLL_IMU;
			vehiclestate.Pitch = NavBuffer[NavBufferIndex].pitch - PITCH_IMU;
			vehiclestate.Yaw = NavBuffer[NavBufferIndex].thdg * DEG2RAD - YAW_IMU; // Yaw comes out in degrees -- ask NGC not me

			//Set Nothing, Easting, Altitude for IMU!
			latlong.set_latlon(NavBuffer[NavBufferIndex].lat / DEG2RAD, NavBuffer[NavBufferIndex].lon / DEG2RAD);
			latlong.get_UTM(&vehiclestate.Easting, &vehiclestate.Northing);

			if (use_gps_altitude == 0) {
				vehiclestate.Altitude = -NavBuffer[NavBufferIndex].alt; //We use z-down, GPS and NGC use z-up
			} else {
				vehiclestate.Altitude = -my_gpsdata.data.altitude;
			}

			// Update state to that of IMU
			NEDcoord vehPos(vehiclestate.Northing, vehiclestate.Easting, vehiclestate.Altitude);
			imu_vector.updateState(vehPos, vehiclestate.Pitch, vehiclestate.Roll, vehiclestate.Yaw);

			//Project origin back into IMU reference frame
			imu_correct = imu_vector.transformS2N(blank); 

			// Reset N/E/D values back to what they should be for vehicle
			vehiclestate.Northing = imu_correct.N;
			vehiclestate.Easting = imu_correct.E;
			vehiclestate.Altitude = imu_correct.D;

			//Use IMU time to timestamp the data.
			vehiclestate.Timestamp = my_imu_time;

			DGCunlockMutex(&m_VehicleStateMutex);
		  DGCunlockMutex(&m_KalmanMutex);
			last_nav_index = NavBufferIndex;

			//if (++update_counter == 5) {
				Broadcast();
				//update_counter = 0;
			//}
		}
	} else  // IF KFILTER IS NOT ENABLED
	{
		//Best possible guess just using GPS.
		DGClockMutex(&m_VehicleStateMutex);
		DGClockMutex(&m_GPSDataMutex);

		timediff = (my_gps_time - (vehiclestate.Timestamp / 1e6));

		vehiclestate.Timestamp = my_gps_time;

		vehiclestate.Roll = 0;
		vehiclestate.Pitch = 0;
		vehiclestate.RollRate = 0;
		vehiclestate.PitchRate = 0;
		vehiclestate.YawRate = 0;

		latlong.set_latlon(my_gpsdata.data.lat, my_gpsdata.data.lng);
		latlong.get_UTM(&vehiclestate.Easting, &vehiclestate.Northing);
		vehiclestate.Altitude = 0;
    		vehiclestate.Acc_N = (vehiclestate.Vel_N - my_gpsdata.data.vel_n) / timediff;
    		vehiclestate.Acc_E = (vehiclestate.Vel_E - my_gpsdata.data.vel_e) / timediff;
	    	vehiclestate.Vel_N  = my_gpsdata.data.vel_n;
	    	vehiclestate.Vel_E  = my_gpsdata.data.vel_e;

		vehiclestate.Vel_D = 0;

		if (vehiclestate.Speed2() > 1)
			vehiclestate.Yaw = atan2(my_gpsdata.data.vel_e, my_gpsdata.data.vel_n);

		DGCunlockMutex(&m_VehicleStateMutex);
		DGCunlockMutex(&m_GPSDataMutex);

    		Broadcast();
	}
}
