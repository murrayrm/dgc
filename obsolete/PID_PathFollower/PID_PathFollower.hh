/* this is code that's trying to become a MTA module for a PID 
 * controlled Path Follower
 */

#ifndef PID_PATHFOLLOWER_HH
#define PID_PATHFOLLOWER_HH

//includes from the MTA_DEMO file
//don't think I need them (something to try if it don' work)
//#include "MTA/Kernel.hh"`
//oops...looks like i may need to use threads...uh-oh
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

//random includes, some subset of which are actually needed
//at some point (when i know more) this should be cleaned up
#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>
#include <iomanip.h>

#include "../../MTA/Kernel/Kernel.hh"
#include "../../MTA/Kernel/DGC_MODULE.hh"

#include "../../bob/vehlib/VState.hh"
#include "../../bob/vehlib/VDrive.hh"
#include "../../bob/vehlib/VStarPackets.hh"

#include "trajController.h"

//include the vehicle constants file (file that contains all the physical
//constants for the vehicle (length, height etc) added by Alex - need to know
//the length of the vehicle for determining where the front axle is
#include "../../bob/include/VehicleConstants.h"



//defines for the PID accel controller
//at some point, need to change them from all being 1static const double 
static const double ACCEL_P = .01;
static const double ACCEL_I = .01;
static const double ACCEL_D = .01;
static const double ACCEL_TIMEOUT = 1;
static const double MAX_ACCEL = 5;
static const double MAX_DECEL = 1;
static const double ACCEL_SATURATION = 1;

//defines for the actual PID lateral controller
//yes, there are a lot of them ... 12, to be exact

static const double DEFAULT_P_Y = 0.7;
static const double DEFAULT_I_Y = 0.0;
static const double DEFAULT_D_Y = 1.0;
static const double DEFAULT_P_H = 0.0;
static const double DEFAULT_I_H = 0.0;
static const double DEFAULT_D_H = 0.0;

 //only used if using check_Radius
static const double CLOSE_ENUF = 2.5;

//used to determine the speed at which the controller tries to follow the path (in m/s)
static const double SPEED = 5;

//this was pulled out of thin air, waiting for alex to give value
//should never be more than pi/6, so .5 is prolly a good value
static const double MAX_ANGLE = 3.1415/6;
static const double INTEGRATOR_SAT_Y = 0.5;
static const double INTEGRATOR_SAT_H = 0.0;
static const double STEER_TIMEOUT = 1;

//file you want to read the traj in from
static char* INPUTFILE = "rddf.traj";
//order of the traj, depends on the input file
static const int PATH_ORDER = 3;

class PID_PathFollower : public DGC_MODULE {
  //cout<<"entered PID_PathFollower.hh"<<endl;
 public:
  PID_PathFollower();

  void Init();
  void Active();
  void UpdateState();
  void InMailHandler(Mail & msg);
  void check_Waypoint();
  void check_Radius();
  double get_Accel();
  double get_YError();
  double get_HError();
  double get_Angle(double yerr, double herr);

  //currently not dealing with querries...
  //Mail QueryMailHandler(Mail & ml);

  private:

  //At current time, laura knows nothing of threads, and thus isn't 
  //using them. yet another *sigh* thing that needs fixing in this code
  //boost::recursive_mutex mutex;

  VState_GetStateMsg SS;
  //prolly want to change this so that i'm not using direct drive,
  //since it's apparently non-standard. oh well.
  VDrive_CmdSteerAccel d;
  //VDrive_CmdMotionMsg e;

  Steer_Packet sp;

  CTraj* newPath;

  int path_index, count;

  //a hopefully useful var that tells you how far along the path in meters you are
  double dist;

  int stop;
  double east_curr, north_curr, east_next, north_next;
  double time_old, y_error_old,h_error_old;
  ofstream m_outputAngle, m_outputState, m_outputWaypoint, m_outputYerror,m_outputTest,m_outputHerror;


};

namespace PID_PathFollower_Messages {

  enum {
    // Query messages
    /*
    this was the example one. 
    At this point, there are no plans for query messages, 
    though that could always change 
    */
    //GetTime,


    // One Way Messages
    // Allows other modules to tell the pathfollower to follow new path
    NewPath,


    Num // A place holder
  };
};


#endif
