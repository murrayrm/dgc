#!/bin/sh -x

paste state.dat | awk '{print $2, $1}' > state
paste default.path | awk '{print $1, $4}' > path
paste angle.dat | awk '{print $3, $2}' > angle
paste waypoint.dat | awk '{print $4, $3}' >waypoint

paste yerror.dat | awk '{print $1, $2*.3}' > perror
paste yerror.dat | awk '{print $1, $2*.3+$4*1.0}' > cmd
paste yerror.dat | awk '{print $1, $4*.3}' > derror

paste yerror.dat | awk '{print $5, $2}' > yerror

#echo "plot 'waypoint' " |gnuplot -persist
echo "plot 'angle' with dots" | gnuplot -persist
echo "plot 'path', 'state' with dots" | gnuplot -persist
echo "plot 'perror' with dots, 'derror' with dots, 'cmd' with dots" | gnuplot -persist
echo "plot 'yerror' " | gnuplot -persist
