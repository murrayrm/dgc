#include <unistd.h>
#include <iostream>
#include <iomanip>
using namespace std;

#include "CDS110_Team4.hh"
//#include <boost/thread/thread.hpp>
//#include <boost/thread/recursive_mutex.hpp>


//initializes the file such that all functions can call it 
//this is the file that contains the commanded path
char* loadFileName = "path5.dat";

FILE *fp;
CDS110_Team4::CDS110_Team4() : DGC_MODULE( MODULES::CDS110_Team4, "Module for CDS110 lateral controller project (Laura, Alex, Jason)", 0) {

}

//Initializes the module
void CDS110_Team4::Init() {

  cout<<"Starting Module Init"<<endl;

  DGC_MODULE::Init();
  UpdateState();

  //initializing the time, used for naming output files
  //and calculating deriv/integral terms
  time_t t = time(NULL);
  tm *local;
  local = localtime(&t);

  //this allocates memory for the output file names
  /*
state file contains: northing, easting, velocity, cmd_angle
waypoint file contains: waypoints read in by check_radius. mainly useful for debugging.
yerror file contains: count and yerror calculated at each iteration of the main function
angle file contains: count and angle calculated at each iteration fo the function
test file contains any output testing desired for debugging
  */

  char stateFileName[256];
  char herrorFileName[256];
  char waypointFileName[256];  
  char yerrorFileName[256];  
  char angleFileName[256];
  char testFileName[256];
        char lnCmd[256];

//this creates the output file names as a function of time called  
sprintf(waypointFileName, "logs/waypoint_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);

sprintf(herrorFileName, "logs/herror_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);

sprintf(stateFileName, "logs/state_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);
sprintf(yerrorFileName, "logs/yerror_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);

sprintf(angleFileName, "logs/angle_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);
sprintf(testFileName, "logs/test_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);

//opens the files so that functions can write to them 
m_outputWaypoint.open(waypointFileName);
m_outputYerror.open(yerrorFileName);
m_outputState.open(stateFileName);
m_outputAngle.open(angleFileName);
 m_outputHerror.open(herrorFileName);
 m_outputTest.open(testFileName);

// make state.dat, angle.dat,yerror.dat,waypoint.dat link to newest files
//I'm not sure how/why it works, but I copied it from Dima, and it seems to work perfectly
        lnCmd[0] = '\0';
        strcat(lnCmd, "ln -f -s ");
        strcat(lnCmd, stateFileName);
        strcat(lnCmd, " state.dat");
        system(lnCmd); 
	lnCmd[0] = '\0';
        strcat(lnCmd, "ln -f -s ");
        strcat(lnCmd, herrorFileName);
        strcat(lnCmd, " herror.dat");
        system(lnCmd);
        lnCmd[0] = '\0';
        strcat(lnCmd, "ln -f -s ");
        strcat(lnCmd, angleFileName);
        strcat(lnCmd, " angle.dat");
        system(lnCmd);
        lnCmd[0] = '\0';
        strcat(lnCmd, "ln -f -s ");
        strcat(lnCmd, yerrorFileName);
        strcat(lnCmd, " yerror.dat");
        system(lnCmd);
        lnCmd[0] = '\0';
        strcat(lnCmd, "ln -f -s ");
        strcat(lnCmd, waypointFileName);
        strcat(lnCmd, " waypoint.dat");
        system(lnCmd);
        lnCmd[0] = '\0';
        strcat(lnCmd, "ln -f -s ");
        strcat(lnCmd, testFileName);
        strcat(lnCmd, " test.dat");
        system(lnCmd);

	//yet more initialization of hte output files...
m_outputWaypoint << setprecision(20);
m_outputYerror << setprecision(20);
m_outputHerror << setprecision(20);
m_outputState << setprecision(20);
m_outputAngle << setprecision(20);
m_outputTest <<setprecision(20);
 

 //opens the file from which we'll read the waypoints
 //they're in the format: waypoint_number northing easting
  fp = fopen(loadFileName, "r");

  //reads the first two waypoints in, and stores them in the 
  //global variables (north/east_curr/next)
  int wp_num;
  int wp_northing, wp_easting;  
  fscanf(fp, "%d %d %d", &wp_num, &wp_easting, &wp_northing);
  m_outputWaypoint<<wp_num<<' '<<wp_easting<<' '<<wp_northing<<endl;
  east_curr = wp_easting;
  north_curr = wp_northing;
  fscanf(fp, "%d %d %d", &wp_num, &wp_easting, &wp_northing);
  m_outputWaypoint<<wp_num<<' '<<wp_easting<<' '<<wp_northing<<endl;
  east_next = wp_easting;

  north_next = wp_northing;

  //...whew...  
  cout<<"Module Init Finished"<<endl;
}




//This function takes care of updating the state and the MTA code required to do so
void CDS110_Team4::UpdateState() {
  Mail msg = NewQueryMessage(MyAddress(), MODULES::VState,VStateMessages::GetState);
  Mail reply = SendQuery(msg);

  if(reply.OK()) {
    reply >> SS;
  }
}


void CDS110_Team4::Active() {
  // this is "main"

  // this is only a counter to make graphing the output files easier
  int count = 0;
  stop = 0;

  // wrap the loop in a while ( ContinueInState() ) 
  while( ContinueInState() && (stop == 0) ) {
    //increment count
    count++;

    //update the state every time we go though this loop




    UpdateState();

    //an attempt to keep the speed as close to 5 m/s as possible
      double accelCmd = 0;
      double steerCmd = 0;
      double desiredSpeed = SPEED;
   if(SS.Speed < desiredSpeed) {
     accelCmd = 5;
    }

    //finds y_error and heaading error
    double yerr, herr;

    yerr = get_YError();
    m_outputYerror<<SS.Easting<<' '<<yerr<<endl;   

    herr = get_HError();
    m_outputHerror<<SS.Easting<<' '<<herr<<endl;

    //calculates desired angle
    steerCmd = get_Angle(yerr,herr);
    m_outputAngle<<SS.Easting<<' '<<steerCmd<<endl;

    //creates variable to send VDrive containing speed and angle
    //need to choose a constant accel command, since we're not 
    //also responsible for the cruise controller.  
    d.Timestamp = TVNow();
    d.accel_cmd = accelCmd;
    d.steer_cmd = steerCmd;

    //sends command to VDrive
    Mail msg = NewOutMessage(MyAddress(),MODULES::VDrive,VDriveMessages::CmdDirectDrive);
    msg <<d;
    SendMail(msg);

    //also sends command to simulator
    Mail msg2 = NewOutMessage(MyAddress(),MODULES::SimVDrive,VDriveMessages::CmdDirectDrive);
    msg2 << d;
    SendMail(msg2);

    //outputs northing and easting to a file for debugging purposes
    
    m_outputState <<
      SS.Easting<< ' ' <<
      SS.Northing << ' ' <<
      SS.Speed<< ' '<<
      d.steer_cmd << endl;  

    //makes sure this doesn't run at more than 10 Hz. 
    //I should probably change this to take into account
    //how long the rest of the program has run, and subtract
    //that from 10000.
    usleep(10000);

  }
  
}

int main(int argc, char **argv) {

  Register(shared_ptr<DGC_MODULE>(new CDS110_Team4));
  StartKernel();

  return 0;
}

//Given an error in meters, this calculates the 
//desired steering angle to send to VDrive
/* *****need to define steering timeouts****** */
double CDS110_Team4::get_Angle(double yerror, double herror) {

  double angle;

  double G_phi_p_y  = DEFAULT_P_Y;
    double G_phi_i_y = DEFAULT_I_Y;
    double G_phi_d_y = DEFAULT_D_Y;
    double int_sat_y = INTEGRATOR_SAT_Y;

  double G_phi_p_h  = DEFAULT_P_H;
    double G_phi_i_h = DEFAULT_I_H;
    double G_phi_d_h = DEFAULT_D_H;
    double int_sat_h = INTEGRATOR_SAT_H;

    double steering_timeout = STEER_TIMEOUT;
    double phi_max = MAX_ANGLE;
    double steer_control; //error control signal

  timeval steerTime;   //time this function was called
  double time_new;      //current time this function was called
	//accurate to usecs, in secs


  //m_outputTest<<"gains (P, I, D): "<<G_phi_p<<' '<<G_phi_i<<' '<<G_phi_d<<endl;

  //for use when trying to read the actual steering position
  //on hold cuz alex doesn't think the pid needs it
  //double phi_actual2;
  //Get_Steer_Packet(MyAddress(), sp);
  //phi_actual2 = sp.ActualSteer;
  
  double steer_cmd_phi,phi_command,steer_act_phi,phi_actual,time_interval,y_error_p,y_error_i,y_error_d, h_error_p, h_error_i, h_error_d;
  y_error_p = 0;
  y_error_i = 0;
  y_error_d = 0;
  h_error_p = 0;
  h_error_i = 0;
  h_error_d = 0;

  /* Figure out how much time has passed */
  gettimeofday(&steerTime, NULL);  //get the current time
  double secs = (double)steerTime.tv_sec;
  double usecs = (double)steerTime.tv_usec;
  time_new = secs+usecs/1000000;
  time_interval = time_new - time_old;//this will be huge on first call.

  // Calculate the proportional error
  y_error_p = yerror; //proportional error
  h_error_p = herror;

  // If we have good history, we can get integral and derivative errors
  //otherwise, reset the integral, and get an appropriate value for the old
  //time.
  
  if ((time_interval>steering_timeout) ||
      (time_interval<=.0001)) {
    //can't take the derivative or integral, because history is unknown
    y_error_i = 0;    //reset the integral, because history is unknown
    h_error_i = 0;
    //if we timed out because it's been too long, set the old time.  this
    //will happen on the first time through, because the time will be huge
    //(number of seconds since 1980). If it hasnt been too long, we just
    //want to wait a little longer, so, don't kill the old time yet.  
    if (time_interval>steering_timeout ){
      time_old=time_new;
      //timeouts++; //for reporting to sparrow
    }
    
  } else {    // we have old data that is current    
    //take the derivative
    y_error_d = (y_error_p - y_error_old) / time_interval;    
    h_error_d = (h_error_p - h_error_old) / time_interval;

    //if positive, add the slice to the integral, (gain added later)
    y_error_i += (y_error_p + y_error_old) * time_interval / 2;
    h_error_i += (h_error_p + h_error_old) * time_interval / 2;
  

    //checking that the integral gain doesn't get too big
    if (y_error_i >  int_sat_y) y_error_i =  int_sat_y;
    if (y_error_i < -int_sat_y) y_error_i = -int_sat_y;  
    if (h_error_i >  int_sat_h) h_error_i =  int_sat_h;
    if (h_error_i < -int_sat_h) h_error_i = -int_sat_h;


    //m_outputTest<<"errors (P, I, D): "<<y_error_p<<' '<<y_error_i<<' '<<y_error_d<<endl;
//update the time
    time_old = time_new;
  }
  
  //save the old error for our next integration and derivative calc.
  y_error_old = y_error_p; //save the current steering angle error as old
  h_error_old = h_error_p;

//multiply by gains, and add, to find control signal (we are controlling
  //velocity, so this will be like an acceleration (the m/s/s kind).  

  steer_control = (G_phi_p_y * y_error_p) + (G_phi_i_y * y_error_i) +
    (G_phi_d_y * y_error_d)+ (G_phi_p_h * h_error_p) + (G_phi_i_h * h_error_i) + (G_phi_d_h * h_error_d);

  m_outputTest<<"gains for Yerror: "<<G_phi_p_y<<' '<<G_phi_i_y<<' '<<G_phi_d_y<<endl;  
  m_outputTest<<"Yerror values: "<<y_error_p<<' '<<y_error_i<<' '<<y_error_d<<endl<<endl;
  m_outputTest<<"gains for Herror: "<<G_phi_p_h<<' '<<G_phi_i_h<<' '<<G_phi_d_h<<endl;
  m_outputTest<<"Herror values: "<<h_error_p<<' '<<h_error_i<<' '<<h_error_d<<endl<<endl;
  m_outputTest<<"phi_max values: "<<phi_max<<endl;
  
  if(steer_control>phi_max) steer_control=phi_max;
  if(steer_control<-phi_max) steer_control=-phi_max;

  //I'm trusting that this is the output value in radians... 
   return steer_control;
  /*  this was for testing the interfaces

  angle = .1;
  return angle;
  */
}

double CDS110_Team4::get_HError() {
  //declaring actual E,N coordinates, actual heading, desired heading
  double east_act, north_act, heading_act, heading_des, heading_error;
  east_act = SS.Easting;
  north_act = SS.Northing;

  //desired heading is the angle between wp_curr and wp_next;
  heading_des = atan2((double)(east_next-east_curr),(double)(north_next-north_curr));
  heading_act = atan2((east_next-east_act),(north_next-north_act));
  heading_error = heading_des - heading_act;

  //output to file for debugging purposes
  m_outputHerror <<east_act<<' '<<heading_error<<endl;

  return heading_error;

}

double CDS110_Team4::get_YError() {

  double east_act, north_act, east_next_prime, north_next_prime,beta,east_act_prime, north_act_prime, yerr;
  //m_outputTest<<"get_YError entered "<<endl;
  //make sure that I'm reading in northing and easting in the right order
  //initializes the actual position coordinates
  east_act = SS.Easting;
  north_act = SS.Northing;

  //updates current and next waypoint, if necessary
  check_Waypoint();
  //check_Radius();

  //shifts coordinate frame, rotates around origin, yerror is
  //y coordinate of transformed actual position 
  east_next_prime = east_next - east_curr;
  
  north_next_prime = north_next - north_curr;
  
  beta = atan2(north_next_prime,east_next_prime);
  m_outputTest<<"beta: "<<beta<<" north_next_prime: "<<north_next_prime<<" east_next_prime: "<<east_next_prime<<endl;
  
  east_act_prime = east_act - east_curr;
  
  north_act_prime = north_act - north_curr;
  
  yerr = -east_act_prime*sin(beta) + north_act_prime*cos(beta);
  
  return yerr;

}

/*
First of two functions that determines when a waypoint has been passed. This one requires that you get with in CLOSE_ENUF 
meters to the waypoint
*/
void CDS110_Team4::check_Radius(){
 
  double radius;
  int test, wp_num;
  
  radius = sqrt(pow(SS.Easting - east_next,2)+pow(SS.Northing - north_next,2));
  //m_outputTest<<"SS coordinates (E,N): "<<SS.Easting<<' '<<SS.Northing<<endl;
  //m_outputTest<<"next coordinates (E, N): "<<east_next<<' '<<north_next<<endl;
  //m_outputTest<<"radius: "<<radius<<endl;

  if(radius < CLOSE_ENUF){
    //m_outputTest<<"Radius is small enough, if loop entered"<<endl;
    east_curr = east_next;
    north_curr = north_next;
    test = fscanf(fp, "%d %d %d", &wp_num, &east_next, &north_next);
    m_outputWaypoint<<wp_num<<' '<<east_next<<' '<<north_next<<' '<<SS.Easting<<' '<<SS.Northing<<endl;
    if(test == EOF){
      //do something if we've reached the end of the waypoint file
      stop = 1;

    }
    return;
  }
  return;
}

/* 
 *Second of two functions that determine when a waypoint 
 *has been passed.
 *This one compares distance from current waypoint to length of segment
 *connecting current and next waypoint
 */
void CDS110_Team4::check_Waypoint(){

 double east_act, north_act, east_next_prime, north_next_prime,theta,east_act_prime, north_act_prime, yerr;

 east_act = SS.Easting;
  north_act = SS.Northing;

  double radius, segLength;
  int test, wp_num;

  east_next_prime = east_next - east_curr;
  //m_outputTest<<"east_next_prime: "<<east_next_prime;
  north_next_prime = north_next - north_curr;
  //m_outputTest<<"north_next_prime: "<<north_next_prime<<endl;
  theta = atan2(north_next_prime,east_next_prime);
  //m_outputTest<<"theta: "<<theta<<endl;
  east_act_prime = east_act - east_curr;
  //m_outputTest<<"east_act_prime: "<<east_act_prime;
  north_act_prime = north_act - north_curr;
  //m_outputTest<<"north_act_prime: "<<north_act_prime<<endl;

  segLength = sqrt(pow(east_next_prime,2)+pow(north_next_prime,2));

  radius = east_act_prime*cos(theta) + north_act_prime*sin(theta);


  //this really should be changed at some point, since it assumes that you're never too far off the desired path...
  if(radius >= segLength){
    //m_outputTest<<"Radius is small enough, if loop entered"<<endl;
    east_curr = east_next;
    north_curr = north_next;
    test = fscanf(fp, "%d %d %d", &wp_num, &east_next, &north_next);
    m_outputWaypoint<<wp_num<<' '<<east_next<<' '<<north_next<<' '<<SS.Easting<<' '<<SS.Northing<<endl;
    if(test == EOF){
      //do something if we've reached the end of the waypoint file
      stop = 1;
      cout<<"we should be stopping...."<<endl;
    }
    return;
  }
  return;


}
