/*
 *this'll eventually become the code for a PID Path follower...
 * started: Dec 15 04, LEL
 */
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <sys/time.h>
#include "MapDisplayMTA/MapDisplayDatum.h"
//using namespace std;

#include "PID_PathFollower.hh"
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include "../../MTA/Kernel/Misc/Thread/ThreadsForClasses.hpp"
#include "../../MTA/Kernel/Misc/Time/Time.hh"



PID_PathFollower::PID_PathFollower() : DGC_MODULE( MODULES::PID_PathFollower, "PID implementation of a path follower", 0) {

  // do something here if you want
}

void PID_PathFollower::Init() {

  cout<<"Starting Module Init"<<endl;

  DGC_MODULE::Init();
  UpdateState();

  char* loadFileName = INPUTFILE;
  int order = PATH_ORDER;

  time_t t = time(NULL);
  tm *local;
  local = localtime(&t);

  char stateFileName[256];
  char herrorFileName[256];


  char waypointFileName[256];  
  char yerrorFileName[256];  
  char angleFileName[256];
  char testFileName[256];
        char lnCmd[256];
//this creates the output file names as a function of time called  
sprintf(waypointFileName, "logs/waypoint_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);

sprintf(herrorFileName, "logs/herror_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);

sprintf(stateFileName, "logs/state_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);
sprintf(yerrorFileName, "logs/yerror_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);

sprintf(angleFileName, "logs/angle_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);
sprintf(testFileName, "logs/test_%04d%02d%02d_%02d%02d%02d.dat",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday,
          local->tm_hour, local->tm_min, local->tm_sec);

//opens the files so that functions can write to them 
m_outputWaypoint.open(waypointFileName);
m_outputYerror.open(yerrorFileName);
m_outputState.open(stateFileName);
m_outputAngle.open(angleFileName);
m_outputHerror.open(herrorFileName);
m_outputTest.open(testFileName);

// make state.dat, angle.dat,yerror.dat,waypoint.dat link to newest files
//I'm not sure how/why it works, but I copied it from Dima, and it seems to work perfectly
        lnCmd[0] = '\0';
        strcat(lnCmd, "ln -f -s ");
        strcat(lnCmd, stateFileName);
        strcat(lnCmd, " state.dat");
        system(lnCmd); 
	lnCmd[0] = '\0';
        strcat(lnCmd, "ln -f -s ");
        strcat(lnCmd, herrorFileName);
        strcat(lnCmd, " herror.dat");
        system(lnCmd);
        lnCmd[0] = '\0';
        strcat(lnCmd, "ln -f -s ");
        strcat(lnCmd, angleFileName);
        strcat(lnCmd, " angle.dat");
        system(lnCmd);
        lnCmd[0] = '\0';
        strcat(lnCmd, "ln -f -s ");
        strcat(lnCmd, yerrorFileName);
        strcat(lnCmd, " yerror.dat");
        system(lnCmd);
        lnCmd[0] = '\0';
        strcat(lnCmd, "ln -f -s ");
        strcat(lnCmd, waypointFileName);
        strcat(lnCmd, " waypoint.dat");
        system(lnCmd);
        lnCmd[0] = '\0';
        strcat(lnCmd, "ln -f -s ");
        strcat(lnCmd, testFileName);
        strcat(lnCmd, " test.dat");
        system(lnCmd);

	//yet more initialization of hte output files...
m_outputWaypoint << setprecision(20);
m_outputYerror << setprecision(20);
m_outputHerror << setprecision(20);
m_outputState << setprecision(20);
m_outputAngle << setprecision(20);
m_outputTest <<setprecision(20);
 

 //set the new path to be whatever was specified
//again, copied from PathFollower.cc, and I'm more than slightly confused as to what's happening

//make sure to delete this newPath object in the shutdown function
 ifstream infile(loadFileName);
 //CTraj* newPath = new CTraj(3, infile);

 newPath = new CTraj(order, infile);   
 east_curr = newPath->getEasting(1);
 north_curr = newPath->getNorthing(1);
 east_next = newPath->getEasting(2);
 north_next = newPath->getNorthing(2);
 path_index = 3;
 Mail mesg = NewOutMessage(MyAddress(),MODULES::MapDisplayMTA,MapDisplayMessages::NewTraj);
 mesg.QueueTo(newPath, CTraj::getHeaderSize());
 mesg.QueueTo(newPath->getNdiffarray(0), newPath->getNumPoints() * sizeof(double));
 mesg.QueueTo(newPath->getEdiffarray(0), newPath->getNumPoints() * sizeof(double));
 SendMail(mesg);
 m_outputWaypoint<<north_curr<<' '<<east_curr<<"0 1"<<endl;
 m_outputWaypoint<<north_next<<' '<<east_next<<"0 2"<<endl;

//need to find a way to initialize the path before one actually gets passed in. default should be zero speed and not changing the current heading. 
 cout<<"Module Init Finished"<<endl;

}

void PID_PathFollower::Active() {

  // this is only a counter to make graphing the output files easier
  count = 0;
  stop = 0;
  double yerr, herr;

  // wrap the loop in a while ( ContinueInState() ) 
  while( ContinueInState() && (stop ==0)) {
    // for this example simply update a counter every 1 second
    //increment count
    count++;

    //update the state every time we go though this loop




    UpdateState();

    //an attempt to keep the speed as close to 5 m/s as possible
      double accelCmd = 0;
      double steerCmd = 0;



   // changing the above function to aim for the speed in the traj file

    //finds y_error and heaading error
  

    yerr = get_YError();
    //m_outputYerror<<SS.Easting<<' '<<yerr<<endl;   

    herr = get_HError();
    m_outputHerror<<SS.Easting<<' '<<herr<<endl;

    //calculates desired angle
    steerCmd = get_Angle(yerr,herr);
    //double ac = get_Accel();
    m_outputAngle<<SS.Easting<<' '<<steerCmd<<' '<<count<<' '<<endl;
    //accelCmd = ac;

      double desiredSpeed = SPEED;
   if(SS.Speed < desiredSpeed) {
     accelCmd = 5;
    }

    //creates variable to send VDrive containing speed and angle
    //need to choose a constant accel command, since we're not 
    //also responsible for the cruise controller.  
    d.Timestamp = TVNow();
    d.accel_cmd = accelCmd;
    d.steer_cmd = steerCmd;



    //e.Timestamp = TVNow();
    //e.velocity_cmd = accelCmd;
    //e.steer_cmd = steerCmd;

    m_outputTest<<"message sent to VDrive (time, accel, steer): "<<d.Timestamp<<' '<<d.accel_cmd<<' '<<d.steer_cmd<<endl;

    //sends command to VDrive
    //one uses directdrive (we control cruise controller) other uses normal (vdrive does cruise controller)
    Mail msg = NewOutMessage(MyAddress(),MODULES::VDrive,VDriveMessages::CmdDirectDrive);    
    //Mail msg = NewOutMessage(MyAddress(),MODULES::VDrive,VDriveMessages::CmdMotion);
    msg <<d;
    SendMail(msg);

    //also sends command to simulator
    Mail msg2 = NewOutMessage(MyAddress(),MODULES::SimVDrive,VDriveMessages::CmdDirectDrive);
    //Mail msg2 = NewOutMessage(MyAddress(),MODULES::SimVDrive,VDriveMessages::CmdMotion);
    msg2 << d;
    SendMail(msg2);

    //outputs northing and easting to a file for debugging purposes
    
    m_outputState <<
      SS.Easting<< ' ' <<
      SS.Northing << ' ' <<
      SS.Speed<< ' '<<
      d.steer_cmd << endl;  

    //makes sure this doesn't run at more than 10 Hz. 
    //I should probably change this to take into account
    //how long the rest of the program has run, and subtract
    //that from 10000.
    usleep(1000);

    //
    //boost::recursive_mutex::scoped_lock sl(mutex);
    //TimeCounter++;
    // lock automatically unlocks at scope brace "}"
  }
  
}

//This function takes care of updating the state and the MTA code required to do so
void PID_PathFollower::UpdateState() {
  Mail msg = NewQueryMessage(MyAddress(), MODULES::VState,VStateMessages::GetState);
  Mail reply = SendQuery(msg);

  if(reply.OK()) {
    reply >> SS;
  }

  //This code overwrites the original easting and northing positions which are centred
  //on the (middle of the) REAR axle, with estimates of the location of the FRONT axle
  //(although note that whether it is the front of the vehicle or the front axle is
  //determined by whether the constant is set to VEHICLE_LENGTH, or VEHICLE_WHEELBASE
  //
  //The 'new' GPS easting and northing positions which are approximately positions
  //of the front of the vehicle are calculated by taking the heading of bob and then
  //using  trigonometry to calculate where his nose is

  //Heading - SS.Yaw - remember that this is +VE CLOCKWISE FROM NORTH!
  //Northing Position = SS.Northing
  //Easting Position = SS.Easting
  
  //Define variables
  double northing_extra = 0;    //Temporary variable for extra northing
  double easting_extra = 0;     //Temporary variable for extra easting  

  //Remember that heading is measured from NORTH, and hence NORTH has COS()
  northing_extra = VEHICLE_WHEELBASE*cos(SS.Yaw);
  easting_extra = VEHICLE_WHEELBASE*sin(SS.Yaw);

  //Overwrite the original state (position) values (which are for the REAR
  //axle) with the new ones calculated for the FRONT axle
  SS.Northing = SS.Northing + northing_extra;
  SS.Easting = SS.Easting + easting_extra;

}

void PID_PathFollower::InMailHandler(Mail& msg) {

  // first, figure out what type of message we received
  switch ( msg.MsgType() ) {

    // In the InMailHandler we are concerned with all
    // one-way messages

    // the only one-way message for this module
  case PID_PathFollower_Messages::NewPath: {

    cout<<"MESSAGE RECEIVED"<<endl;

    //copied from PathFollower.cc file, not sure I understand it
    msg.DequeueFrom(newPath, CTraj::getHeaderSize());
    cout<<"first dequeue worked"<<endl;
    msg.DequeueFrom(newPath->getNdiffarray(0), (newPath->getNumPoints() * newPath->getOrder()) * sizeof(double));
    cout<<"second dequeue worked"<<endl;
    msg.DequeueFrom(newPath->getEdiffarray(0), (newPath->getNumPoints() * newPath->getOrder()) * sizeof(double));
    cout<<"third dequeue worked"<<endl;

    cout<<"northing, easting init: "<<newPath->getNorthing(1)<<' '<<newPath->getEasting(1)<<endl;
    

    // read in and set the time from the message
    // but first lock the data
    // use brackets for the scope lock
    //{
    //  boost::recursive_mutex::scoped_lock sl(mutex);
    //  msg >> TimeCounter;
    /**
     * send copy of path to MapDisplay
     */
    Mail mesg = NewOutMessage(MyAddress(),MODULES::MapDisplayMTA,MapDisplayMessages::NewTraj);
    mesg.QueueTo(newPath, CTraj::getHeaderSize());
    mesg.QueueTo(newPath->getNdiffarray(0), newPath->getNumPoints() * sizeof(double));
    mesg.QueueTo(newPath->getEdiffarray(0), newPath->getNumPoints() * sizeof(double));
    SendMail(mesg);
 }
    break; // break this case of the switch

default:
    // let the parent class handle the message
    DGC_MODULE::InMailHandler(msg);
    break;
  }
}


//not actually planning on allowing query messages at this point...
//maybe later, if somebody has a good reason
/*
Mail PID_PathFollower::QueryMailHandler(Mail & msg) {

  Mail reply = ReplyToQuery(msg); // this sets up the return message

  // first, figure out what type of message we received
  switch ( msg.MsgType() ) {

    // In the QueryMailHandler we are concerned with all
    // query messages

    // the only query message for this module
  case MTA_Demo_Messages::GetTime: 
    
    // first lock the data,
    // then append the integer onto the Mail object 
    {
      boost::recursive_mutex::scoped_lock sl(mutex);
      reply << TimeCounter;

    }
    break; // break this case of the switch

  default:
    // let the parent class handle the message
    reply = DGC_MODULE::QueryMailHandler(msg);
  }

  return reply;

}
*/



int main(int argc, char **argv) {

  Register(shared_ptr<DGC_MODULE>(new PID_PathFollower));
  StartKernel();

  return 0;
}


//Given an error in meters, this calculates the
//desired steering angle to send to VDriv

//*****PID CONTROLLERS*****//

/* *****need to define steering timeouts****** */
double PID_PathFollower::get_Angle(double yerror, double herror) {

  double angle;

  double G_phi_p_y  = DEFAULT_P_Y;
    double G_phi_i_y = DEFAULT_I_Y;
    double G_phi_d_y = DEFAULT_D_Y;
    double int_sat_y = INTEGRATOR_SAT_Y;

  double G_phi_p_h  = DEFAULT_P_H;
    double G_phi_i_h = DEFAULT_I_H;
    double G_phi_d_h = DEFAULT_D_H;
    double int_sat_h = INTEGRATOR_SAT_H;

    double steering_timeout = STEER_TIMEOUT;
    double phi_max = MAX_ANGLE;
    double steer_control; //error control signal

  timeval steerTime;   //time this function was called
  double time_new;      //current time this function was called
	//accurate to usecs, in secs


  //m_outputTest<<"gains (P, I, D): "<<G_phi_p<<' '<<G_phi_i<<' '<<G_phi_d<<endl;

  //for use when trying to read the actual steering position
  //on hold cuz alex doesn't think the pid needs it
  //double phi_actual2;
  //Get_Steer_Packet(MyAddress(), sp);
  //phi_actual2 = sp.ActualSteer;
  
  double steer_cmd_phi,phi_command,steer_act_phi,phi_actual,time_interval,y_error_p,y_error_i,y_error_d, h_error_p, h_error_i, h_error_d;

  /* Figure out how much time has passed */
  gettimeofday(&steerTime, NULL);  //get the current time
  double secs = (double)steerTime.tv_sec;
  double usecs = (double)steerTime.tv_usec;
  time_new = secs+usecs/1000000;
  time_interval = time_new - time_old;//this will be huge on first call.

  // Calculate the proportional error
  y_error_p = yerror; //proportional error
  h_error_p = herror;

  // If we have good history, we can get integral and derivative errors
  //otherwise, reset the integral, and get an appropriate value for the old
  //time.
  
  if ((time_interval>steering_timeout) ||
      (time_interval<=.0001)) {
    //can't take the derivative or integral, because history is unknown
    y_error_i = 0;    //reset the integral, because history is unknown
    h_error_i = 0;
    //if we timed out because it's been too long, set the old time.  this
    //will happen on the first time through, because the time will be huge
    //(number of seconds since 1980). If it hasnt been too long, we just
    //want to wait a little longer, so, don't kill the old time yet.  
    if (time_interval>steering_timeout ){
      time_old=time_new;
      //timeouts++; //for reporting to sparrow
    }
    
  } else {    // we have old data that is current    
    //take the derivative
    y_error_d = (y_error_p - y_error_old) / time_interval;    
    h_error_d = (h_error_p - h_error_old) / time_interval;

    //if positive, add the slice to the integral, (gain added later)
    y_error_i += (y_error_p + y_error_old) * time_interval / 2;
    h_error_i += (h_error_p + h_error_old) * time_interval / 2;
  

    //checking that the integral gain doesn't get too big
    if (y_error_i >  int_sat_y) y_error_i =  int_sat_y;
    if (y_error_i < -int_sat_y) y_error_i = -int_sat_y;  
    if (h_error_i >  int_sat_h) h_error_i =  int_sat_h;
    if (h_error_i < -int_sat_h) h_error_i = -int_sat_h;

    m_outputYerror<<count<<' '<<y_error_p<<' '<<y_error_i<<' '<<y_error_d<<' '<<dist<<endl;

    //m_outputTest<<"errors (P, I, D): "<<y_error_p<<' '<<y_error_i<<' '<<y_error_d<<endl;
//update the time
    time_old = time_new;
  }
  
  //save the old error for our next integration and derivative calc.
  y_error_old = y_error_p; //save the current steering angle error as old
  h_error_old = h_error_p;

//multiply by gains, and add, to find control signal (we are controlling
  //velocity, so this will be like an acceleration (the m/s/s kind).  

  steer_control = (G_phi_p_y * y_error_p) + (G_phi_i_y * y_error_i) +
    (G_phi_d_y * y_error_d)+ (G_phi_p_h * h_error_p) + (G_phi_i_h * h_error_i) + (G_phi_d_h * h_error_d);

  //m_outputTest<<"gains for Yerror: "<<G_phi_p_y<<' '<<G_phi_i_y<<' '<<G_phi_d_y<<endl;  
  //m_outputTest<<"Yerror values: "<<y_error_p<<' '<<y_error_i<<' '<<y_error_d<<endl<<endl;
  //m_outputTest<<"gains for Herror: "<<G_phi_p_h<<' '<<G_phi_i_h<<' '<<G_phi_d_h<<endl;
  //m_outputTest<<"Herror values: "<<h_error_p<<' '<<h_error_i<<' '<<h_error_d<<endl<<endl;
  //m_outputTest<<"phi_max values: "<<phi_max<<endl;
  
  //m_outputTest<<"un-chopped steerControl value: "<<steer_control<<endl;
  

  //******END OF PID CONTROLLERS******//
  

  //******FEED FORWARD******//
  //This feed-forward term uses the first and second order northing & easting derivatives
  //included in the traj file to determine the nominal steering angle that would be required
  //to follow the path - (calculations based upon the closest point in the traj file to 
  //the vehicles current position).  The nominal steering angle is calculated based upon
  //the nominal heading (commanded by the point in the traj file) using the FRONT AXLE
  //centred model.

  //Variable Definitions
  double current_northing = 0;                 //Current northing value
  double current_northing_first_diff = 0;      //Current northing first derivative value
  double current_northing_second_diff = 0;     //Current northing second derivative value
  double current_easting = 0;                  //Current easting value
  double current_easting_first_diff = 0;       //Current easting first derivative value 
  double current_easting_second_diff = 0;       //Current easting second derivative value

  double theta_nominal = 0;                    //nominal heading angle
  double phi_nominal = 0;                      //nominal steering angle from feed forward
  double current_traj_desired_speed = 0;       //current desired vehicle speed based on northing_dot & easting_dot in traj
  
  double asin_argument = 0;                    //asin() function argument

  //Reference the current path (and current point on the path) for its values
  //of the variables above (northing, easting & derivatives)
  current_northing = newPath->getNorthing(path_index);
  current_northing_first_diff = newPath->getNorthingDiff(path_index, 1);
  current_northing_second_diff = newPath->getNorthingDiff(path_index, 2);

  current_easting = newPath->getEasting(path_index);
  current_easting_first_diff = newPath->getEastingDiff(path_index, 1);
  current_easting_second_diff = newPath->getEastingDiff(path_index, 2);

  //Calculate the desired speed calculated from northing_dot and easting_dot of the
  //closest point on the path to the vehicles current position
  current_traj_desired_speed = sqrt(pow(current_northing_first_diff, 2) + pow(current_easting_first_diff, 2));

  //Calculate the function of N, E and their derivatives which = theta (heading) nominal
  theta_nominal = (((current_northing_first_diff * current_easting_second_diff) - (current_easting_first_diff * current_northing_second_diff))/(pow(current_traj_desired_speed, 2)));

  //Calculate the value of the argument in the asin() term - if the argument has a
  //magnitude>1, then set it =0 as the asin() function can only take an argument which is
  //within the range -1 -> +1, and if the feed-forward is giving us a theta_nominal
  //which is inaccurate then we do not wish to use it - ALTHOUGH PERHAPS THIS SHOULD CHANGE??
  asin_argument = ((VEHICLE_WHEELBASE/current_traj_desired_speed)*theta_nominal);

  if(asin_argument>1) asin_argument=0;
  if(asin_argument<-1) asin_argument=0;

  phi_nominal = asin(asin_argument);
  
  //m_outputAngle<<phi_nominal<<' '<<theta_nominal<<' '<<current_speed<<' '<<endl;

  //Update the commanded steering angle sent to vdrive so that it is the SUM of the
  //PID (y-error) control signal AND the feed-forward term
  steer_control = (steer_control + phi_nominal);

  //****END OF FEED-FORWARD****//


  //Steering saturation
  if(steer_control>phi_max) steer_control=phi_max;
  if(steer_control<-phi_max) steer_control=-phi_max;

  //I'm trusting that this is the output value in radians... 
   return steer_control;
  /*  this was for testing the interfaces

  angle = .1;
  return angle;
  */
}


double PID_PathFollower::get_HError() {
  //declaring actual E,N coordinates, actual heading, desired heading
  double east_act, north_act, heading_act, heading_des, heading_error;
  east_act = SS.Easting;
  north_act = SS.Northing;

  //desired heading is the angle between wp_curr and wp_next;
  heading_des = atan2((double)(east_next-east_curr),(double)(north_next-north_curr));
  heading_act = atan2((east_next-east_act),(north_next-north_act));
  heading_error = heading_des - heading_act;

  //output to file for debugging purposes
  m_outputHerror <<east_act<<' '<<heading_error<<endl;

  return heading_error;

}

double PID_PathFollower::get_Accel() {
 
    double G_p = ACCEL_P;
    double G_i = ACCEL_I;
    double G_d = ACCEL_D;

    double verror,v_act, v_des, n_diff, e_diff, error_old;
    double accel_command,time_interval,error_p,error_i,error_d;

    double accel_timeout = ACCEL_TIMEOUT;
    double acc_max = MAX_ACCEL;
    double dec_max = MAX_DECEL;
    double acc_sat = ACCEL_SATURATION;
    double accel, accel_control; //error control signal

  timeval accelTime;   //time this function was called
  double time_new;      //current time this function was called
	//accurate to usecs, in secs


  //m_outputTest<<"gains (P, I, D): "<<G_phi_p<<' '<<G_phi_i<<' '<<G_phi_d<<endl;

  //for use when trying to read the actual steering position
  //on hold cuz alex doesn't think the pid needs it
  //double phi_actual2;
  //Get_Steer_Packet(MyAddress(), sp);
  //phi_actual2 = sp.ActualSteer;
  

  v_act = sqrt(pow(SS.Vel_E,2) + pow(SS.Vel_N,2));
  n_diff = newPath->getNorthingDiff(path_index, 1);
  e_diff = newPath->getEastingDiff(path_index, 1);
  //taking the desired velocity to be the sqrt of E' and N' squared
  v_des = sqrt(pow(n_diff ,2)+pow(e_diff ,2));
  m_outputTest<<"N and E derivatives, v_des: "<<n_diff<<' '<<e_diff<<' '<<v_des<<endl;
  verror = v_des - v_act;

  /* Figure out how much time has passed */
  gettimeofday(&accelTime, NULL);  //get the current time
  double secs = (double)accelTime.tv_sec;
  double usecs = (double)accelTime.tv_usec;
  time_new = secs+usecs/1000000;
  time_interval = time_new - time_old;//this will be huge on first call.

  // Calculate the proportional error
  error_p = verror; //proportional error
  
  // If we have good history, we can get integral and derivative errors
  //otherwise, reset the integral, and get an appropriate value for the old
  //time.
  
  if ((time_interval>accel_timeout) ||
      (time_interval<=.0001)) {
    //can't take the derivative or integral, because history is unknown
    error_i = 0;    //reset the integral, because history is unknown

    //if we timed out because it's been too long, set the old time.  this
    //will happen on the first time through, because the time will be huge
    //(number of seconds since 1980). If it hasnt been too long, we just
    //want to wait a little longer, so, don't kill the old time yet.  
    if (time_interval>accel_timeout ){
      time_old=time_new;
      //timeouts++; //for reporting to sparrow
    }
    
  } else {    // we have old data that is current    
    //take the derivative
    error_d = (error_p - error_old) / time_interval;    

    //if positive, add the slice to the integral, (gain added later)
    error_i += (error_p + error_old) * time_interval / 2;

    //checking that the integral gain doesn't get too big
    if (error_i >  acc_sat) error_i =  acc_sat;
    if (error_i < -acc_sat) error_i = -acc_sat;  

    // need to output something else...
    //    m_outputYerror<<count<<' '<<y_error_p<<' '<<y_error_i<<' '<<y_error_d<<' '<<dist<<endl;

    m_outputTest<<"errors (P, I, D): "<<error_p<<' '<<error_i<<' '<<error_d<<endl;

    m_outputTest<<"desired, actual velocity: "<<v_des<<' '<<v_act<<endl;
//update the time
    time_old = time_new;
  }
  
  //save the old error for our next integration and derivative calc.
  error_old = error_p; //save the current steering angle error as old

//multiply by gains, and add, to find control signal (we are controlling
  //velocity, so this will be like an acceleration (the m/s/s kind).  

  accel_control = (G_p * error_p) + (G_i * error_i) + (G_d * error_d);
  //m_outputTest<<"accel control: "<<accel_control<<endl;

  //m_outputTest<<"gains for Yerror: "<<G_phi_p_y<<' '<<G_phi_i_y<<' '<<G_phi_d_y<<endl;  
  //m_outputTest<<"Yerror values: "<<y_error_p<<' '<<y_error_i<<' '<<y_error_d<<endl<<endl;
  //m_outputTest<<"gains for Herror: "<<G_phi_p_h<<' '<<G_phi_i_h<<' '<<G_phi_d_h<<endl;
  //m_outputTest<<"Herror values: "<<h_error_p<<' '<<h_error_i<<' '<<h_error_d<<endl<<endl;
  //m_outputTest<<"phi_max values: "<<phi_max<<endl;
  
  //m_outputTest<<"un-chopped steerControl value: "<<steer_control<<endl;
  
  if(accel_control>acc_max) accel_control=acc_max;
  if(accel_control<-acc_max) accel_control=-acc_max;

  //for now, we're just letting vdrive handle the cruise controller,
  //so this block of code is totally redundant, but may be used 
  //sometime.
  // return accel_control;
  return v_des;
}

double PID_PathFollower::get_YError() 
{
  /************************************ 
   * Rewritten for clarity with NEcoords. Not tested; just an example. -LBC
  NEcoord pos(SS.Northing, SS.Easting);
  // Update {north_curr, east_curr, north_next, east_next} here
  check_Radius();
  // The heading on the traj is given in the traj.
  // Why take two points on the traj?
  NEcoord ref1(north_curr, east_curr);
  NEcoord ref2(north_next, east_next);
  // Replacement for north_next_prime and east_next_prime
  // Vector from traj reference point to next point on traj
  NEcoord ref_dir = ref2 - ref1;
  // Yaw direction (pos CW) that is perpendicular to the reference line
  // (90 degrees CW from it)
  double perp_yaw = atan2(ref_dir.E, ref_dir.N) + M_PI/2;
  // Vector from the reference point on traj to vehicle pos
  NEcoord err = pos - ref1;
  // The lateral error from the line through ref1 at the desired yaw.
  // This is just the dot product of a line perpendicular to tan_dir and the 
  // error vector.  This dot product is implemented as the * operator.
  return err * NEcoord(cos(perp_yaw), sin(perp_yaw));
  ************************************/
  
  double east_act, north_act, east_next_prime, north_next_prime,beta,east_act_prime, north_act_prime, yerr;
  //m_outputTest<<"get_YError entered "<<endl;
  //make sure that I'm reading in northing and easting in the right order
  //initializes the actual position coordinates
  east_act = SS.Easting;
  north_act = SS.Northing;

  //updates current and next waypoint, if necessary
  //check_Waypoint();
  check_Radius();

  //shifts coordinate frame, rotates around origin, yerror is
  //y coordinate of transformed actual position 
  east_next_prime = east_next - east_curr;
  
  north_next_prime = north_next - north_curr;
  
  beta = atan2(north_next_prime,east_next_prime);
  //m_outputTest<<"beta: "<<beta<<" north_next_prime: "<<north_next_prime<<" east_next_prime: "<<east_next_prime<<endl;
  
  east_act_prime = east_act - east_curr;
  
  north_act_prime = north_act - north_curr;
  
  yerr = -east_act_prime*sin(beta) + north_act_prime*cos(beta);
  
  return yerr;

}

/*
First of two functions that determines when a waypoint has been passed. This one requires that you get with in CLOSE_ENUF 
meters to the waypoint
*/
void PID_PathFollower::check_Radius(){
 
  double radius1, radius2;
  int test, wp_num;
  
  radius1 = sqrt(pow(SS.Easting - east_curr,2)+pow(SS.Northing - north_curr,2));  
  radius2 = sqrt(pow(SS.Easting - east_next,2)+pow(SS.Northing - north_next,2));
  dist += sqrt(pow(east_curr - east_next,2)+pow(north_curr - north_next,2));
  //m_outputTest<<"SS coordinates (E,N): "<<SS.Easting<<' '<<SS.Northing<<endl;
  //m_outputTest<<"next coordinates (E, N): "<<east_next<<' '<<north_next<<endl;
  m_outputTest<<endl<<"radius: "<<radius1<<' '<<radius2<<endl;

   if(radius2 < radius1) {
     m_outputTest<<endl<<endl<<"***********"<<endl<<"Closer to next point: if loop entered"<<endl<<"**********"<<endl<<endl;
    east_curr = east_next;
    north_curr = north_next;
    m_outputTest<<"Path Index: "<<path_index<<endl;
    
   east_next = newPath->getEasting(path_index);
   north_next = newPath->getNorthing(path_index);

   m_outputWaypoint<<north_next<<' '<<east_next<<' '<<count<<' '<<path_index<<endl;
 
   //this is a hack, essentially throwing out most of the points in the traj file.
   //should find nicer way to do this, but it works for now
   path_index += 10;

    if(path_index > newPath->getNumPoints()) {
      stop = 1;
    return;
    }

  }
 
  return; 
}

/* 
 *Second of two functions that determine when a waypoint 
 *has been passed.
 *This one compares distance from current waypoint to length of segment
 *connecting current and next waypoint
 */
void PID_PathFollower::check_Waypoint(){

 double east_act, north_act, east_next_prime, north_next_prime,theta,east_act_prime, north_act_prime, yerr;

 east_act = SS.Easting;
  north_act = SS.Northing;

  double radius, segLength;
  int test, wp_num;

  east_next_prime = east_next - east_curr;
  //m_outputTest<<"east_next_prime: "<<east_next_prime;
  north_next_prime = north_next - north_curr;
  //m_outputTest<<"north_next_prime: "<<north_next_prime<<endl;
  theta = atan2(north_next_prime,east_next_prime);
  //m_outputTest<<"theta: "<<theta<<endl;
  east_act_prime = east_act - east_curr;
  //m_outputTest<<"east_act_prime: "<<east_act_prime;
  north_act_prime = north_act - north_curr;
  //m_outputTest<<"north_act_prime: "<<north_act_prime<<endl;

  segLength = sqrt(pow(east_next_prime,2)+pow(north_next_prime,2));

  radius = east_act_prime*cos(theta) + north_act_prime*sin(theta);


  //this really should be changed at some point, since it assumes that you're never too far off the desired path...
 
  /* ANOTHER COMMENTED OUT FUNCTION FOR TESTING
 if(radius >= segLength){
    //m_outputTest<<"Radius is small enough, if loop entered"<<endl;
    east_curr = east_next;
    north_curr = north_next;
    test = fscanf(fp, "%d %d %d", &wp_num, &east_next, &north_next);
    m_outputWaypoint<<wp_num<<' '<<east_next<<' '<<north_next<<' '<<SS.Easting<<' '<<SS.Northing<<endl;
    if(test == EOF){
      //do something if we've reached the end of the waypoint file
      stop = 1;
      cout<<"we should be stopping...."<<endl;
    }
    return;
  }

  */
  return;


}
