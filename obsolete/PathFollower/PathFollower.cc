#include <unistd.h>
#include <sys/time.h>
#include <iostream>
#include <iomanip>
using namespace std;

#include "PathFollower.hh"
#include "MapDisplayDatum.h"

#include "MTA/Misc/Thread/ThreadsForClasses.hpp"
#include "MTA/Misc/Time/Time.hh"

#include "DGCutils" // for DGCtimetosec

int QUIT_PRESSED = 0;

extern int           NOSTATE;
extern int           NODISPLAY;
extern int           SIM;
extern unsigned long SLEEP_TIME;
extern char* logNamePrefix;
extern char* loadFilename;

/******************************************************************************/
/******************************************************************************/
PathFollower::PathFollower() 
  : DGC_MODULE(MODULES::PathFollower, "Path Follower", 0) 
{
}

PathFollower::~PathFollower() 
{
}

/******************************************************************************/
/******************************************************************************/
void PathFollower::Init() 
{
  cout << "Starting Module Init" << endl;

  // call the parent init first
  DGC_MODULE::Init();

  // update the State Struct and check for valid state
  if(!NOSTATE)
  {
    UpdateState();
    if(d.SS.Northing == 0)
    {
      printf("\nUnable to communicate with vstate (Northing is zero).\n");
      printf("Run with --nostate option if this is OK. (Exiting...)\n\n");
      exit(-1);

      #warning "This needs to be changed to reference the FRONT axle, NOT the REAR, as is currently the case";
    }
  }

  m_nActiveCount = 0;
  m_timeBegin = TVNow();

  ifstream infile(loadFilename);
  m_traj   = new CTraj(3, infile);
  Mail mesg = NewOutMessage(MyAddress(),MODULES::MapDisplayMTA,MapDisplayMessages::NewTraj);
  mesg.QueueTo(m_traj, CTraj::getHeaderSize());
  mesg.QueueTo(m_traj->getNdiffarray(0), m_traj->getNumPoints() * sizeof(double));
  mesg.QueueTo(m_traj->getEdiffarray(0), m_traj->getNumPoints() * sizeof(double));
  SendMail(mesg);
#ifdef LQR_CONTROLLER  
  char* pFile = "feedback.dat";
  d.m_follow = new CTrajController(m_traj, pFile);
#endif

#ifdef PID_CONTROLLER
  d.m_follow = new PID_Controller;
  d.m_follow->Init(&d.SS, m_traj);
#endif

  m_outputPlansNum = 1;
  char stateFileName[256];
  char plansFileName[256];
	char planStartsFileName[256];
	char lnCmd[256];

  time_t t = time(NULL);
  tm *local;
  local = localtime(&t);

  sprintf(stateFileName, "logs/state_%04d%02d%02d_%02d%02d%02d.dat", 
	  local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	  local->tm_hour, local->tm_min, local->tm_sec);

  sprintf(plansFileName, "logs/plans_%04d%02d%02d_%02d%02d%02d.dat", 
	  local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	  local->tm_hour, local->tm_min, local->tm_sec);

  sprintf(planStartsFileName, "logs/planstarts_%04d%02d%02d_%02d%02d%02d.dat", 
	  local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	  local->tm_hour, local->tm_min, local->tm_sec);

	m_outputPlanStarts.open(planStartsFileName);
  m_outputPlans.open(plansFileName);
  m_outputState.open(stateFileName);

	// make state.dat, plans.dat, planstarts.dat links to the latest data files
	lnCmd[0] = '\0';
	strcat(lnCmd, "ln -f -s ");
	strcat(lnCmd, stateFileName);
	strcat(lnCmd, " state.dat");
	system(lnCmd);
	lnCmd[0] = '\0';
	strcat(lnCmd, "ln -f -s ");
	strcat(lnCmd, plansFileName);
	strcat(lnCmd, " plans.dat");
	system(lnCmd);
	lnCmd[0] = '\0';
	strcat(lnCmd, "ln -f -s ");
	strcat(lnCmd, planStartsFileName);
	strcat(lnCmd, " planstarts.dat");
	system(lnCmd);

  m_outputState << setprecision(20);
  m_outputPlans << setprecision(20);
  m_outputPlanStarts << setprecision(20);

  // print data format information in a header at the top of the file
//  cout << "% Time[sec] | Easting[m] | Northing[m] | Altitude[m]"
//       << " | Vel_E[m/s] | Vel_N[m/s] | Vel_U[m/s]"
//       << " | Speed2[m/s] | Accel[m/s/s]"
//       << " | Pitch[rad] | Roll[rad] | Yaw[rad]"
//       << " | PitchRate[rad/s] | RollRate[rad/s] | YawRate[rad/s]"
//       << " | PhiDes[rad] | VelDes[m/s] | [Unused]"
//       << " | PhidotCmd [rad/s] | AccCmd [m/s/s]" << endl;

  if( !NODISPLAY )
  {
    RunMethodInNewThread<PathFollower>( this, 
                    &PathFollower::SparrowDisplayLoop);
    RunMethodInNewThread<PathFollower>( this, 
                    &PathFollower::UpdateSparrowVariablesLoop);
  }

  // leave this at the end 
  cout << "Module Init Finished" << endl;
}


/******************************************************************************/
/******************************************************************************/
void PathFollower::Active() 
{
	// timers to make sure our control frequency is what we set (not
	// affected by how long it takes to compute each cycle)
	timeval timestamp1, timestamp2;

  cout << "Starting Active Function" << endl;

  while( ContinueInState() && !QUIT_PRESSED) 
  {
		// time checkpoint at the start of the cycle
		gettimeofday(&timestamp1, NULL);

    // increment module counter
    m_nActiveCount++;

    // Get state from the simulator or vstate 
    if( NOSTATE )
    {
			d.SS.Northing = 0.0;
			d.SS.Easting	= 0.0;
			d.SS.Vel_N = 0.0;
			d.SS.Vel_E = 0.0;
			d.SS.Easting	= 0.0;
			d.SS.Yaw			= (double)(m_nActiveCount*15 % 360) * M_PI/180.0;
			d.SS.YawRate	= 0.0;
    }
    else
    {
      /* This sets d.SS */
      UpdateState();
      if( d.SS.Speed2() < 0.01 )
      {
//         printf("Oh CRAP, d.SS.Speed2 was zero, and we divide by it!\n");
//         printf("I'm changing this value to 0.01.\n");
        d.SS.Vel_N = sqrt(pow(0.01,2)/2.0); 
        d.SS.Vel_E = sqrt(pow(0.01,2)/2.0); 
      }
		}

    /* Compute the control (linearized LQR design) */
    d.m_follow->getControlVariables(&d.SS, &d.accel, &d.phi);

#ifdef USE_DIRECT_DRIVE
		d.toVDrive.Timestamp = TVNow();
		d.toVDrive.accel_cmd = d.accel;
		d.toVDrive.steer_cmd = d.phi;

    // And ship it off to VDrive
		if (!SIM) {
    Mail msg = NewOutMessage( MyAddress(), 
                              MODULES::VDrive, 
                              VDriveMessages::CmdDirectDrive);
    msg << d.toVDrive;
    SendMail(msg);

    // Send the command to the simulation also
    Mail msg2 = NewOutMessage( MyAddress(), 
                               MODULES::SimVDrive, 
                               VDriveMessages::CmdDirectDrive);
    msg2 << d.toVDrive;
    SendMail(msg2);
		}
#else
    // TODO: Get speed and steering angle from accel and phidot!
    double speedCmd = d.SS.Speed2() + (double)(SLEEP_TIME)/1000000.0 * d.accel;
    double steerCmd = d.phi;

    /* Send drive commands (steering angle, speed) to vdrive */
    d.toVDrive.Timestamp = TVNow();
    d.toVDrive.velocity_cmd = fmax(0.0,fmin(speedCmd, MAX_SPEED)); 
    /* The steer_cmd should be a double between -1 and 1! */
    //d.toVDrive.steer_cmd = fmax(-USE_MAX_STEER, fmin(USE_MAX_STEER, steerCmd));
    d.toVDrive.steer_cmd = fmax(-1.0, fmin(1.0, steerCmd/USE_MAX_STEER)); 

		if (!SIM) {
    // And ship it off to VDrive
    Mail msg = NewOutMessage( MyAddress(), 
                              MODULES::VDrive, 
                              VDriveMessages::CmdMotion);
    msg << d.toVDrive;
    SendMail(msg);

//     // Send the command to the simulation also
//     Mail msg2 = NewOutMessage( MyAddress(), 
//                                MODULES::SimVDrive, 
//                                VDriveMessages::CmdMotion);
//     msg2 << d.toVDrive;
//     SendMail(msg2);
		}
#endif

    // output the state to the log
    m_outputState <<
            DGCtimetosec(d.SS.Timestamp) << ' ' <<
            d.SS.Easting << ' ' <<
            d.SS.Northing << ' ' <<
            d.SS.Altitude << ' ' <<
            d.SS.Vel_E << ' ' <<
            d.SS.Vel_N << ' ' <<
            d.SS.Vel_D << ' ' <<
            d.SS.Speed2() << ' ' <<
            d.SS.Accel2() << ' ' <<
            d.SS.Pitch << ' ' <<
            d.SS.Roll << ' ' <<
            d.SS.Yaw << ' ' <<
            d.SS.PitchRate << ' ' <<
            d.SS.RollRate << ' ' <<
            d.SS.YawRate << ' ' <<
            d.toVDrive.steer_cmd << ' ' <<
#ifdef USE_DIRECT_DRIVE
            d.toVDrive.accel_cmd << ' ' <<
#else
            d.toVDrive.velocity_cmd << ' ' <<
#endif
            0.0 << ' ' <<
            d.phi << ' ' <<
            d.accel << endl;

		// time checkpoint at the end of the cycle
		gettimeofday(&timestamp2, NULL);

		// now sleep for SLEEP_TIME usec less the amount of time the
		// computation took
		int delaytime =
			SLEEP_TIME -
			(
			 (timestamp2.tv_sec - timestamp1.tv_sec)*1000000 +
			 (timestamp2.tv_usec - timestamp1.tv_usec)
			);
		if(delaytime > 0)
			usleep_for(delaytime);
  } // end while( ContinueInState() && !QUIT_PRESSED) 

  cout << "Finished Active State" << endl;
}


/******************************************************************************/
/******************************************************************************/
void PathFollower::Shutdown() 
{
  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;

  delete m_traj;
  delete d.m_follow;

  cout << "Done Shutting Down" << endl;
}


/******************************************************************************/
/******************************************************************************/
void PathFollower::InMailHandler(Mail& msg)
{
  cout << "mail received" << endl;
  switch (msg.MsgType())
  {
  case PathFollowerMessages::NewPath:
    {
      cout << "Path received" << endl;

      msg.DequeueFrom(m_traj, CTraj::getHeaderSize());
			msg.DequeueFrom(m_traj->getNdiffarray(0), m_traj->getNumPoints() * sizeof(double));
			msg.DequeueFrom(m_traj->getNdiffarray(1), m_traj->getNumPoints() * sizeof(double));
			msg.DequeueFrom(m_traj->getNdiffarray(2), m_traj->getNumPoints() * sizeof(double));
			msg.DequeueFrom(m_traj->getEdiffarray(0), m_traj->getNumPoints() * sizeof(double));
			msg.DequeueFrom(m_traj->getEdiffarray(1), m_traj->getNumPoints() * sizeof(double));
			msg.DequeueFrom(m_traj->getEdiffarray(2), m_traj->getNumPoints() * sizeof(double));

      cout << "order = " << m_traj->getOrder() << endl;
      cout << "num   = " << m_traj->getNumPoints() << endl;

      
      /* DEBUG */
      printf("\n\n\n\n\n**** Just received a NEW m_traj ****\n\n\n\n\n");
//      m_traj->print();

      m_traj->print(m_outputPlans);
      m_outputPlans << endl;

			m_traj->print(m_outputPlanStarts,1);
      m_outputPlanStarts << endl;


#ifdef PID_CONTROLLER
      d.m_follow->SetNewPath(m_traj, &d.SS);
#endif

    }
    break;

  case PathFollowerMessages::FastStop:
    {
      printf("\n\n\n\n\n**** Just received a STOP SIGNAL ****\n\n\n\n\n");
      /******* TODO: STOP THE CAR BY SENDING FULL BRAKE TO VDRIVE *********/
    }
    break;

  default:
    // let the parent mail handler handle it
    DGC_MODULE::InMailHandler(msg);
    break;
  }
 
  Mail mesg = NewOutMessage(MyAddress(),MODULES::MapDisplayMTA,MapDisplayMessages::NewTraj);
  mesg.QueueTo(m_traj, CTraj::getHeaderSize());
  mesg.QueueTo(m_traj->getNdiffarray(0), m_traj->getNumPoints() * sizeof(double));
  mesg.QueueTo(m_traj->getEdiffarray(0), m_traj->getNumPoints() * sizeof(double));
  SendMail(mesg);

}


/******************************************************************************/
/******************************************************************************/
void PathFollower::UpdateState()
{
  // These two lines take roughly about 400 microseconds.
  Mail msg = NewQueryMessage( MyAddress(), MODULES::VState,
                              VStateMessages::GetVehicleState);
  Mail reply = SendQuery(msg);

  // This takes about 2-3 microseconds.
  if (reply.OK() ) {
    reply >> d.SS; // download the new state
  }

//--------------------------------------------------
//   /* DEBUG */
//   // Determine the time disparity between processes.
//   // compare the timestamp we just received across the
//   // MTA to the local current time
//   printf("TVNow() - d.SS.Timestamp = %f sec\n",
//          (TVNow() - d.SS.Timestamp).dbl() );
//-------------------------------------------------- 

} // end UpdateState()

// end PathFollower.cc
