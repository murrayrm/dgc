#ifndef PATHFOLLOWERDATUM_HH
#define PATHFOLLOWERDATUM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>              
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "Constants.h"   // vehicle constants
#include "MTA/Kernel.hh" // The MTA Module Kernel

#include "ControllerSelect.h"

// The old state struct (Bob-dependent, no accels)
#include "vehlib/VState.hh"
// VDrive commands
#include "vehlib/VDrive.hh"
// The new, platform independent state struct (with accels)
#include "VehicleState.hh"


#include "traj.h"

//#ifdef LQR_CONTROLLER
#include "trajController.h" 
//#endif

#ifdef PID_CONTROLLER
#include "PID_Controller.hh"
#endif

#define USE_DIRECT_DRIVE

using namespace std;

/*! All data that needs to be shared by common threads. */
struct PathFollowerDatum 
{
  /*! The state struct. */
  VehicleState SS;

  /*! The vdrive commands */
#ifdef USE_DIRECT_DRIVE
  VDrive_CmdSteerAccel toVDrive;
#else
  VDrive_CmdMotionMsg toVDrive;
#endif

#ifdef LQR_CONTROLLER
  /*! The trajectory controller. */
  CTrajController* m_follow;
#endif

#ifdef PID_CONTROLLER
  /* define the trajectory controller */
  PID_Controller* m_follow;
#endif 

  /*! The control commands from the trajectory controller. */
  double phi;
  double accel;
};

/*! Enumerate all module message types that could be received. */
namespace PathFollowerMessages
{
  enum
  {
    /*! Measurements received. */
    NewPath,

    /*! Stop signal message */
    FastStop,

    /*! A place holder (not used) */
    NumMessages
  };
};

#endif
