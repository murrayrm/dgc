#ifndef PATHFOLLOWER_HH
#define PATHFOLLOWER_HH

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "PathFollowerDatum.hh"

using namespace std;
using namespace boost;

class PathFollower : public DGC_MODULE 
{
public:
  // and change the constructors
  PathFollower();
  ~PathFollower();

  // The states in the state machine.
  // Uncomment these if you wish to define these functions.
  void Init();

  void Active();

  //  void Standby();

  void Shutdown();

  //  void Restart();

  // The message handlers. 
  void InMailHandler(Mail & ml);

  //Mail QueryMailHandler(Mail & ml);

  // the status function.
  //string Status();

  void UpdateState();

  void SparrowDisplayLoop();

  void UpdateSparrowVariablesLoop();

private:

  /*! The datum named d, for shared variables between threads. The state struct 
   ** is included here.*/
  PathFollowerDatum d;

  /*! how many times the while loop in the Active function has run.  */
  int m_nActiveCount;

  /*! The trajectory to follow. */
  CTraj* m_traj;

  /*! Time that module started. */
  Timeval m_timeBegin; 

  /*! Logs to output the plans and the state */
  ofstream m_outputPlans, m_outputPlanStarts, m_outputState;

  int m_outputPlansNum;

};

#endif
