#!/bin/csh -x

rm state,asdf,path,yerr,herr,cerr,verr

paste state2.dat | awk '{print $1, $2}' > state
paste state2.dat | awk '{print $4, $5}' > asdf
paste default.traj | awk '{print $1, $4}' > path
paste error.dat | awk '{print $NR, $1}' > yerr
paste error.dat | awk '{print $NR, $2}' > herr
paste error.dat | awk '{print $NR, $3}' > cerr
paste verror.dat | awk '{print $NR, $1}' > verr

echo "plot 'asdf','path', 'state' with dots" | gnuplot -persist
echo "plot 'yerr','herr','verr', 'cerr' with dots" | gnuplot -persist
