Lars Cremean
4 Sep 04

PathFollower is an MTA module that executes trajectory following using the 
CTrajController and CTraj classes.

Cast of characters:

  README               - This file.
  Makefile             - The makefile.
  PathFollowerMain.cc  - The main function.
  PathFollower.cc      - MTA implementation (Init, Active, InMailHandler)
  PathFollower.hh      - MTA implementation header (declaration of class)
  PathFollowerDatum.hh - Datum header, for variables shared between threads

= INSTALLATION/OPERATION INSTRUCTIONS =

***MAKE SURE THAT THE PATHFOLLOWER IS USING THE CONTROLLER THAT YOU
WERE EXPECTING!! - CHECK WHICH CONTROLLER IFDEF IS COMMENTED OUT IN 
ControllerSelect.h***

make in dgc
make install in dgc

# Note: These steps aren't necessary to do manually, as they're automatically 
# taken care of in the PathFollower/Makefile.
#
#make in dgc/projects/traj
#make install in dgc/projects/traj
#
#make in dgc/projects/trajController
#make install in dgc/projects/trajController
#
#make in dgc/projects/PID_Controller
#make install in dgc/projects/PID_Controller

make in dgc/projects/PathFollower
make install in dgc/projects/PathFollower

= DEFAULT TRAJECTORY =
PathFollower reads in a default trajectory (default.traj) when it starts up.
This is a symbolic link in the current directory, and typing "make" will tell you 
what the default trajectory is.  Change the trajLink rule in the Makefile if you
want to use a different tranectory (and then type "make").
You do NOT have to recompile after changing this link.


= INITIAL CONDITIONS FOR SIMULATION = 
Change the initial conditions in dgc/bob/include/SimulatorConstants.h so that 
they are close the start of the trajectory selected above

make in dgc/bob/DevModules/SimVStateVDrive
make install in dgc/bob/DevModules/SimVStateVDrive

RUN (./SimVStateVDrive) SimVStateVDrive
RUN PathFollower

et voila! - bake in the oven for 30 mins at gas mark 10 and then serve with
strawberries and garnished with vanilla pods and freshly made chocolate 
shavings


= LOGGING = 
** THIS LOGFILE FORMAT IS OUT-OF-DATE; WE SHOULD START CONFORMING TO THE
   FORMAT DEFINED IN VEHICLESTATE.HH **
Logfiles (Data Formats & what they are)
 . state.dat
    Records the state data with the following columns:
      % Time[sec] | Easting[m] | Northing[m] | Altitude[m] |
        Vel_E[m/s] | Vel_N[m/s] | Vel_U[m/s] | Speed[m/s] | Accel[m/s/s] |
		    Pitch[rad] | Roll[rad] | Yaw[rad] | PitchRate[rad/s] | 
	      RollRate[rad/s] | YawRate[rad/s] | PhiDes[rad] | VelDes[m/s] |
       [Unused] | PhidotCmd [rad/s] | AccCmd [m/s/s]

 . plans.dat
    Records all the points in all trajectories received sequentially:
     Northing  | Northing 1st deriv | Northing 2nd deriv |
     Easting   | Easting  1st deriv | Easting  2nd deriv |

 . planstarts.dat
    Records the first point of each trajectory received (double check on this).
