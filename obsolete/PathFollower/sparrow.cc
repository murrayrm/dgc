/*! 
 * $Id$
 */
#include "PathFollower.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

VehicleState dispSS;
double d_decimaltime;
double d_speed;

double PitchDeg, RollDeg, YawDeg;
double PitchRateDeg, RollRateDeg, YawRateDeg;

//#ifdef LQR_CONTROLLER
int refIndex;
STrajCtrlState* pRefState;
STrajCtrlState* pErrState;
STrajCtrlState refState;
STrajCtrlState errState;

double* pK;
// TODO: Why is this necessary?
double K0, K1, K2, K3, K4;
double K5, K6, K7;
//#endif

double yError, hError, cError;

double vRef, vTraj;
double accelFF, accelFB, accelCmd;
double steerFF, steerFB, steerCmd;

double dphiCmd;
double daccelCmd;

//double steerCommand;
double steerCommandRad;
double accelCommand;

int SparrowUpdateCount = 0;

#include "vddtable.h"
int user_quit(long arg);

void PathFollower::UpdateSparrowVariablesLoop() 
{
  while( !ShuttingDown() ) 
  {
    dispSS = d.SS;
    d_decimaltime = 0;
    d_speed = d.SS.Speed2();

    PitchDeg     = d.SS.Pitch     * 180.0 /M_PI;
    RollDeg      = d.SS.Roll      * 180.0 /M_PI;
    YawDeg       = d.SS.Yaw       * 180.0 /M_PI;
    PitchRateDeg = d.SS.PitchRate * 180.0 /M_PI;
    RollRateDeg  = d.SS.RollRate  * 180.0 /M_PI;
    YawRateDeg   = d.SS.YawRate   * 180.0 /M_PI;

#warning "Replace this with steering angle from vdrive."
    double currphi = atan(VEHICLE_WHEELBASE * -d.SS.YawRate / fmax( 0.1, d.SS.Speed2()));
    
#ifdef LQR_CONTROLLER
    /* Get a pointer to the feedback matrix */
    pK = d.m_follow->getLastFeedbackMatrix();

    /* Get the index of the reference point */
    refIndex = d.m_follow->getLastDesiredStateIndex();

    /* Get the reference and error states */
    pRefState = d.m_follow->getLastDesiredState();
    pErrState = d.m_follow->getLastErrorState();
		memcpy(&refState, pRefState, sizeof(refState));
		memcpy(&errState, pErrState, sizeof(errState));

		if(pK != NULL)
		{
			K0 = pK[0]; K1 = pK[1]; K2 = pK[2]; K3 = pK[3]; K4 = pK[4];
			K5 = pK[5]; K6 = pK[6]; K7 = pK[7];
		}

    /* TODO: Get the error state from the controller rather than here
     * so that you are debugging the controller rather than Sparrow */    
//--------------------------------------------------
//     cout << "pK[0] = " << pK[0] << endl;
//     cout << "pK[1] = " << pK[1] << endl;
//     cout << "pK[2] = " << pK[2] << endl;
//     cout << "pK[3] = " << pK[3] << endl;
//     cout << "pK[4] = " << pK[4] << endl;
//     cout << "pK[5] = " << pK[5] << endl;
//     cout << "pK[6] = " << pK[6] << endl;
//     cout << "pK[7] = " << pK[7] << endl;
//     cout << "pK[8] = " << pK[8] << endl;
//     cout << "pK[9] = " << pK[9] << endl << endl;
//-------------------------------------------------- 
#endif

    dphiCmd = d.phi;
    daccelCmd = d.accel;

    yError = d.m_follow->getYError();
    hError = d.m_follow->getHError();
    cError = d.m_follow->getCError();

    vRef = d.m_follow->getVRef();
    vTraj = d.m_follow->getVTraj();
    
    accelFF = d.m_follow->getAccelFF();
    accelFB = d.m_follow->getAccelFB();
    accelCmd = d.m_follow->getAccelCmd();

    steerFF = d.m_follow->getSteerFF();
    steerFB = d.m_follow->getSteerFB();
    steerCmd = d.m_follow->getSteerCmd();

    //steerCommand = d.toVDrive.steer_cmd; 
    steerCommandRad = d.toVDrive.steer_cmd; 
#ifdef USE_DIRECT_DRIVE
    accelCommand = d.toVDrive.accel_cmd;
#else
    accelCommand = d.toVDrive.velocity_cmd;
#endif

    SparrowUpdateCount ++;

    // Wait a bit, because other threads will print some stuff out
    usleep(500000); 
  }
}

void PathFollower::SparrowDisplayLoop() 
{
  dbg_all = 0;

  if (dd_open() < 0) exit(1);
  dd_bindkey('q', user_quit);

  dd_usetbl(vddtable);

  usleep(250000); // Wait a bit, because other threads will print some stuff out
  dd_loop();
  dd_close();

}

// TODO: Set the shutdown flag in the quit function
int user_quit(long arg)
{
  return DD_EXIT_LOOP;
}
