#include "PathFollower.hh"

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>

#define SLEEP_OPT          1
#define LOG_OPT            2
#define TRAJ_OPT           3

int           NOSTATE    = 0;        // Don't update state over MTA if = 1.
int           NODISPLAY  = 0;        // Don't start Sparrow display interface.
int           SIM  = 0;              // Don't send commands to VDrive.
unsigned long SLEEP_TIME = 100000;  // How long in us to sleep in active fn.

/* The name of this program. */
const char * program_name;
char* logNamePrefix = NULL;
char* loadFilename = strdup("default.traj");

/* Prints usage information for this program to STREAM (typically stdout
   or stderr), and exit the program with EXIT_CODE. Does not return. */
void print_usage (FILE* stream, int exit_code)
{
  fprintf( stream, "Usage:  %s [options]\n", program_name );
  fprintf( stream,
           "  --none            There are not currently any options.\n"
           "  --nostate         Continue even if state data is not received.\n"
           "  --nodisp          Don't start the Sparrow display interface.\n"
           "  --sim             Don't send commands to VDrive.\n"
           "  --sleep us        Specifies us sleep time in Active function.\n"
           "  --traj filename   Use filename as the trajectory to follow.\n"
           "  --help, -h        Display this message.\n" );
  exit(exit_code);
}

int main(int argc, char **argv) 
{
  /* Set the default arguments that won't need external access here. */
 
  /* Temporary character. */
  int ch;
  /* A string listing valid short options letters. */
  const char* const short_options = "h";
  /* An array describing valid long options. */
  static struct option long_options[] = 
  {
    // first: long option (--option) string
    // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
    // third: if pointer, set variable to value of fourth argument
    //        if NULL, getopt_long returns fourth argument
    {"nostate",    0, &NOSTATE,          1},
    {"nodisp",     0, &NODISPLAY,        1},
    {"sim",        0, &SIM,              1},
    {"sleep",      1, NULL,              SLEEP_OPT},
    {"help",       0, NULL,              'h'},
    {"log",        1, NULL,              LOG_OPT},
    {"traj",       1, NULL,              TRAJ_OPT},
    {NULL,         0, NULL,              0}
  };

  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  program_name = argv[0];
  printf("\n");

  // Loop through and process all of the command-line input options.
  while((ch = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
  {
    switch(ch)
    {
      case TRAJ_OPT:
	loadFilename = strdup(optarg);
	break;

      case LOG_OPT:
	logNamePrefix = strdup(optarg);
	break;

      case SLEEP_OPT:
        SLEEP_TIME = (unsigned long) atol(optarg);
        break;
      
      case 'h':
        /* User has requested usage information. Print it to standard
           output, and exit with exit code zero (normal 
           termination). */
        print_usage(stdout, 0);

      case '?': /* The user specified an invalid option. */
        /* Print usage information to standard error, and exit with exit
           code one (indicating abnormal termination). */
        print_usage(stderr, 1);

      case -1: /* Done with options. */
        break;
    }
  }

  /* Start the MTA by registering a module and then starting the kernel */
  Register(shared_ptr<DGC_MODULE>(new PathFollower));
  StartKernel();

  return 0;
 
} // end main()
