#include <math.h>
#include "trajController.h"

#include <fstream>
#include <iostream>
using namespace std;

#define V_MIN			0.05
#define V_MAX			20.0
#define VNUM			32

#define THMIN			0.0
#define THMAX			(2.0 * M_PI)
#define THNUM			16

// from VehicleConstants.h
#define PHMIN			(-VEHICLE_MAX_AVG_STEER)
#define PHMAX			(VEHICLE_MAX_AVG_STEER)
#define PHNUM			21

#define WEIGHT_LQR	1.0
#define WEIGHT_FF		1.0 

#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

#warning "potential improvement: use tan(phi) for everything instead of phi"

CTrajController::CTrajController(CTraj* pTraj, char* pFeedbackMatrixFile)
	: m_pTraj(pTraj)
{
	m_pLastFeedbackMatrix = NULL;

#ifdef USE_OCTAVE
	m_octaveProc.open("octave --silent --no-line-editing ");
	m_octaveProc << setprecision(20);
#else
	m_pFeedbackMatrices =
		new double[LENGTH_FEEDBACK_MATRIX * VNUM * THNUM * PHNUM];

	ifstream datafile(pFeedbackMatrixFile);
	datafile.read((char*)m_pFeedbackMatrices, LENGTH_FEEDBACK_MATRIX * VNUM * THNUM * PHNUM * sizeof(double));
#endif

#ifdef LOG
	m_log.open("trajController.log");
#endif
}

CTrajController::~CTrajController()
{
#ifndef USE_OCTAVE
	delete m_pFeedbackMatrices;
#endif
}

/*****************************************************************************/
int CTrajController::getDesiredStateIndex(VehicleState* pState)
{
	// index of the point on the trajectory that's closest to the current system
	// point, and the squared distance from the system point to that point on the
	// trajectory
	int			nearestOnPathIndex=0, i;
	double	nearestDistSq;
	double	currentDistSq;

	// Compute the nearest point on the trajectory to where we currently are
	nearestDistSq = 1.0e20;
	for(i=0; i<m_pTraj->getNumPoints(); i++)
	{
		currentDistSq =
			pow(pState->Northing - m_pTraj->getNorthing(i),2.0) +
			pow(pState->Easting	 - m_pTraj->getEasting(i) ,2.0);

		if(currentDistSq < nearestDistSq)
		{
			nearestDistSq = currentDistSq;
			nearestOnPathIndex = i;
		}
	}

	return nearestOnPathIndex;

} // end getDesiredStateIndex()


/*****************************************************************************/
void CTrajController::getDesiredState(STrajCtrlState& trajState)
{
	trajState.Nor = m_pTraj->getNorthing(m_desiredStateIndex);
	trajState.Eas = m_pTraj->getEasting(m_desiredStateIndex);
	
	trajState.Spd = 
					sqrt( pow(m_pTraj->getNorthingDiff(m_desiredStateIndex, 1), 2.0) +
								pow(m_pTraj->getEastingDiff(m_desiredStateIndex, 1),	2.0) );

	trajState.Spd = fmax( V_MIN, trajState.Spd);

	trajState.Yaw = atan2(m_pTraj->getEastingDiff(m_desiredStateIndex, 1),
												m_pTraj->getNorthingDiff(m_desiredStateIndex, 1));
} // end getDesiredState()

void CTrajController::getControlVariables(VehicleState *pState,
																					double *pA, double *pPhi)
{
	// Compute the nearest point on the trajectory to where we currently are
	m_desiredStateIndex = getDesiredStateIndex(pState);

	double aLQR,phiLQR, aFF, phiFF;
	getControlVariablesFeedForward(pState, &aFF, &phiFF);
	getControlVariablesLQR(pState, &aLQR, &phiLQR);

	*pA =
		WEIGHT_LQR * aLQR +
		WEIGHT_FF * aFF;
	*pPhi =
		WEIGHT_LQR * phiLQR +
		WEIGHT_FF * phiFF;

#ifdef LOG
	m_log
		<< aFF << ' '
		<< phiFF << ' '
		<< aLQR << ' '
		<< phiLQR << ' '
		<< *pA << ' '
		<< *pPhi << ' '
		<< endl;
#endif
}

#define xd		m_pTraj->getNorthingDiff(m_desiredStateIndex, 1)
#define xdd		m_pTraj->getNorthingDiff(m_desiredStateIndex, 2)
#define xddd	m_pTraj->getNorthingDiff(m_desiredStateIndex, 3)
#define yd		m_pTraj->getEastingDiff(m_desiredStateIndex, 1)
#define ydd		m_pTraj->getEastingDiff(m_desiredStateIndex, 2)
#define yddd	m_pTraj->getEastingDiff(m_desiredStateIndex, 3)

void CTrajController::getControlVariablesFeedForward(VehicleState *pState,
																										 double *pA, double *pPhi)
{
	double v = sqrt( xd*xd + yd*yd);
	double thetadot = (xd*ydd - xdd*yd) / v/v;
	double tanphi = VEHICLE_WHEELBASE * thetadot / v;
	*pA =	 ( xd*xdd + yd*ydd) / v;
	*pPhi = atan(tanphi);
}

void CTrajController::getControlVariablesLQR(VehicleState *pState,
																						 double *pA, double *pPhi)
{
	getDesiredState(m_desiredState);

	m_errorState.Nor = m_desiredState.Nor - pState->Northing;
	m_errorState.Eas = m_desiredState.Eas - pState->Easting;
	m_errorState.Spd = m_desiredState.Spd - pState->Speed2();
	m_errorState.Yaw = m_desiredState.Yaw - pState->Yaw;

	/* Normalize the angles to be between -pi and pi */
	m_errorState.Yaw =
	  atan2( sin(m_errorState.Yaw), cos(m_errorState.Yaw) );


#warning "Can we get this variable from vdrive?"
	double phi = atan(VEHICLE_WHEELBASE * pState->YawRate / 
									fmax( V_MIN, pState->Speed2()));
	m_pLastFeedbackMatrix = getFeedbackMatrix( pState, phi );
	
	/* DEBUG */
	/* TODO: Make sure that we can access all of these from PathFollower */
//	ofstream tem("debug.out");
//
//	tem << "Spd = " << pState->Speed2() << "; Yaw = " << pState->Yaw 
//			 << ";Phi = " << phi << endl;
//	
//	tem << "Kfb = [ " << pKfb[0] << " " << pKfb[1] << " ;" << endl;
//	tem << "			" << pKfb[2] << " " << pKfb[3] << " ;" << endl;
//	tem << "			" << pKfb[4] << " " << pKfb[5] << " ;" << endl;
//	tem << "			" << pKfb[6] << " " << pKfb[7] << " ;" << endl;
//	tem << "			" << pKfb[8] << " " << pKfb[9] << " ]" << endl;
	

	// set the control variables
#ifdef USE_OCTAVE

	*pA =
		m_pLastFeedbackMatrix[0] * m_errorState.Nor +
		m_pLastFeedbackMatrix[2] * m_errorState.Eas +
		m_pLastFeedbackMatrix[4] * m_errorState.Spd +
		m_pLastFeedbackMatrix[6] * m_errorState.Yaw;
	*pPhi =
		m_pLastFeedbackMatrix[1] * m_errorState.Nor +
		m_pLastFeedbackMatrix[3] * m_errorState.Eas +
		m_pLastFeedbackMatrix[5] * m_errorState.Spd +
		m_pLastFeedbackMatrix[7] * m_errorState.Yaw;

#else

	*pA =
		m_pLastFeedbackMatrix[0] * m_errorState.Nor +
		m_pLastFeedbackMatrix[1] * m_errorState.Eas +
		m_pLastFeedbackMatrix[2] * m_errorState.Spd +
		m_pLastFeedbackMatrix[3] * m_errorState.Yaw;
	*pPhi =
		m_pLastFeedbackMatrix[4] * m_errorState.Nor +
		m_pLastFeedbackMatrix[5] * m_errorState.Eas +
		m_pLastFeedbackMatrix[6] * m_errorState.Spd +
		m_pLastFeedbackMatrix[7] * m_errorState.Yaw;

#endif

#ifdef LOG
	m_log
		<< pState->Northing << ' '
		<< pState->Easting << ' '
		<< pState->Speed2() << ' '
		<< pState->Yaw << ' '
		<< m_desiredState.Nor << ' '
		<< m_desiredState.Eas << ' '
		<< m_desiredState.Spd << ' '
		<< m_desiredState.Yaw << ' '
		<< m_errorState.Nor << ' '
		<< m_errorState.Eas << ' '
		<< m_errorState.Spd << ' '
		<< m_errorState.Yaw << ' '
		<< m_pLastFeedbackMatrix[0] << ' ' << m_pLastFeedbackMatrix[1] << ' ' << m_pLastFeedbackMatrix[2] << ' ' << m_pLastFeedbackMatrix[3] << ' ' << m_pLastFeedbackMatrix[4] << ' ' << m_pLastFeedbackMatrix[5] << ' ' << m_pLastFeedbackMatrix[6] << ' ' << m_pLastFeedbackMatrix[7] << ' ';
#endif

} // end getControlVariables()

/*****************************************************************************/
double* CTrajController::getFeedbackMatrix(VehicleState* pState, double phi)
{

#ifdef USE_OCTAVE

 
	// run octave to compute the lqr matrix gain at the current operating point 
  // NOTE: it is very important to end this
	// with endl. Sending a newline and then calling flush() doesn't seem to work...
  // UPDATE: they both seem to work (see testoct.cc)
	m_octaveProc << "computelqr(" <<
		pState->Speed2() << "," <<
		pState->Yaw << "," <<
		phi << ")" << endl;

	m_octaveProc.ignore(10000, '\n');
	for(int i=0; i<LENGTH_FEEDBACK_MATRIX; i++)
	{
		m_octaveProc >> m_K[i];
	}
	m_octaveProc.ignore(10000, '\n');
	m_octaveProc.ignore(10000, '\n');

#warning "why am i returning a member?"
	return m_K;

#else

	int Vind	= lround((pState->Speed2() - V_MIN)	/ (V_MAX	- V_MIN)	* ((double)VNUM -1.0));
	int THind = lround((pState->Yaw		- THMIN) / (THMAX - THMIN) * ((double)THNUM-1.0));
	int PHind = lround((phi						- PHMIN) / (PHMAX - PHMIN) * ((double)PHNUM-1.0));
	Vind	= min(max(Vind,	 0), VNUM -1);
	THind = min(max(THind, 0), THNUM-1);
	PHind = min(max(PHind, 0), PHNUM-1);

	return m_pFeedbackMatrices + LENGTH_FEEDBACK_MATRIX * 
					(PHind + THind*PHNUM + Vind*THNUM*PHNUM);

#endif

} // end getFeedbackMatrix()
