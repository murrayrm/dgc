function k = computelqr(v, th, ph)
	ACCURACY_N  = 100.0;
	ACCURACY_E  = 100.0;
	ACCURACY_V  = 0.003;
	ACCURACY_TH = 0.00006;
	ACCURACY_PH = 0.00004;
	ACCURACY_A  = 0.0006;
	WEIGHT_N  = 1.0 / sqrt(ACCURACY_N);
	WEIGHT_E  = 1.0 / sqrt(ACCURACY_E);
	WEIGHT_V  = 1.0 / sqrt(ACCURACY_V);
	WEIGHT_TH = 1.0 / sqrt(ACCURACY_TH);
	WEIGHT_PH = 1.0 / sqrt(ACCURACY_PH);
	WEIGHT_A  = 1.0 / sqrt(ACCURACY_A);

  L = 2.985;

  a = [0 0 cos(th)   -v*sin(th);
       0 0 sin(th)    v*cos(th);
       0 0 0          0        ;
       0 0 tan(ph)/L  0       ];

  b = [0 0;
       0 0;
       1 0;
       0  v/(L*cos(ph)*cos(ph))];

  q = diag([WEIGHT_N, WEIGHT_E, WEIGHT_V, WEIGHT_TH]);
  r = diag([WEIGHT_A, WEIGHT_PH]);
       

# the following is a stripped down "call" to the octave lqr function
# (and the are function, which lqr calls). Stripped down to run faster
# in realtime. Old call:
#  k = lqr(a, b, q, r)';
  [d, h] = balance ([a, -(b/r)*b'; -q, -a'], "B");
  [u, s] = schur (h, "A");
  u = d * u;
	n  = 4;
  n1 = 5;
  n2 = 8;
  p = u (n1:n2, 1:n) / u (1:n, 1:n);
  k = (r\(b'*p))';
end
