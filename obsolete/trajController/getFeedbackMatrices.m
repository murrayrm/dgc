	Vall  = linspace(0.05, 20, 32);
	THall = linspace(0, 2*pi, 16);
	PHall = linspace(-0.3568, 0.3568, 21); # from VehicleConstants.h

	Ks = [];
	vidx=[];
	thidx=[];
	phidx=[];

	for v=Vall
		v
		for th=THall
			for ph=PHall
				Ks = [Ks; reshape(computelqr(v, th, ph),1,8)];
				vidx = [vidx; v];
				thidx = [thidx; th];
				phidx = [phidx; ph];

			end;
		end;
	end;

	fil = fopen("feedback.dat", "w+", "ieee-le");
	fwrite(fil, Ks', "double");
	fclose(fil);
