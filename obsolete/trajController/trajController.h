#ifndef _TRAJCONTROLLER_H_
#define _TRAJCONTROLLER_H_

#include "traj.h"
#include "pstream.h"
#include <iomanip> // For setprecision()

// New VehicleState struct is platform-independent
#include "VehicleState.hh"

// Include vehicle parameters
//#include "bob/include/VehicleConstants.h"
#include "VehicleConstants.h"

#define USE_OCTAVE
//#define LOG
#define LENGTH_FEEDBACK_MATRIX      8

struct STrajCtrlState
{
  double Nor;
  double Eas;
  double Spd;
  double Yaw;
};

class CTrajController
{
#ifdef USE_OCTAVE
  redi::pstream   m_octaveProc;
  double          m_K[LENGTH_FEEDBACK_MATRIX];
#endif

#ifdef LOG
	ofstream				m_log;
#endif

  CTraj*          m_pTraj;
  double*         m_pFeedbackMatrices;

	double*					m_pLastFeedbackMatrix;
	int							m_desiredStateIndex;
	STrajCtrlState	m_desiredState;
	STrajCtrlState	m_errorState;

public:
  CTrajController(CTraj* pTraj, char* pFeedbackMatrixFile);
  ~CTrajController();

  void getControlVariables(VehicleState *pState,
                           double *pA, double *pPhi);

  /*! Returns a pointer to the most recent feedback matrix used. Used
		for the sparrow display in the PathFollower. */
  double* getLastFeedbackMatrix(void)
	{
		return m_pLastFeedbackMatrix;
	}
  /*! The desired state index (of a point on the given Path). */
  inline int getLastDesiredStateIndex(void)
	{
		return m_desiredStateIndex;
	}
  /*! The desired state (of a point on the given Path). */
	inline STrajCtrlState* getLastDesiredState(void)
	{
		return &m_desiredState;
	}
  /*! The desired state (of a point on the given Path). */
	inline STrajCtrlState* getLastErrorState(void)
	{
		return &m_errorState;
	}

private:
  inline void getControlVariablesLQR(VehicleState *pState,
																		 double *pA, double *pPhi);

  inline void getControlVariablesFeedForward(VehicleState *pState,
																						 double *pA, double *pPhi);

  /*! Returns a pointer to the feedback matrix used to compute the 
   ** control output phi and accel.  */
  inline double* getFeedbackMatrix(VehicleState* pState, double phi);


  /*! The desired state index (of a point on the given Path) */
  inline int getDesiredStateIndex(VehicleState* pState);

  /*! The desired state (of a point on the given Path) */
	inline void getDesiredState(STrajCtrlState& trajState);
};

#endif // _TRAJCONTROLLER_H_
