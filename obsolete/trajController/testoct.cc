#include <math.h>
#include <fstream>
#include <iostream>
using namespace std;

//#define DEBUG_COUTS

#include "sys/time.h"
#include "pstream.h"

// custom function to subtract two times
double timeval_subtract( const timeval &, const timeval & );

int main(void)
{
  // How long does it take?
  timeval before, after;
  double elapsed;

#ifdef DEBUG_COUTS
  printf("Creating pstream.\n");
#endif

  redi::pstream   m_octaveProc;
  
#ifdef DEBUG_COUTS
  printf("Opening octave process.\n");
#endif

  m_octaveProc.open("octave --silent --no-line-editing");

  double m_K[10];

  for( int i = 0; i < 10; i++ )
  {

    // set the before time
    printf("[%s][%d] Timer started.\n", __FILE__, __LINE__);
    gettimeofday( &before, NULL);

#ifdef DEBUG_COUTS
    printf("Sending octave command.\n");
#endif

    // run octave to compute the lqr matrix gain at the current operating point
    m_octaveProc << "computelqr(" <<
      1.0 << "," <<
      2.0 << "," <<
#if 0
      0.0 << ")\n";
    printf("Flushing pstream.\n");
    m_octaveProc.flush();
#else
    0.0 << ")" << endl;
#endif 

#ifdef DEBUG_COUTS
    printf("Reading back output (1).\n");
#endif

    m_octaveProc.ignore(10000, '\n');

#ifdef DEBUG_COUTS
    printf("Reading back output (2).\n");
#endif

    for(int i=0; i<8; i++)
    {

#ifdef DEBUG_COUTS
      printf("Reading element %d... ", i);
#endif

      m_octaveProc >> m_K[i];

#ifdef DEBUG_COUTS
      printf("done.\n");
#endif

    }

#ifdef DEBUG_COUTS
    printf("Reading back output (3).\n");
#endif

    m_octaveProc.ignore(10000, '\n');
    m_octaveProc.ignore(10000, '\n');

#ifdef DEBUG_COUTS

    printf("Printing result.\n");
    printf("% .14e   % .14e\n",   m_K[0], m_K[1]);
    printf("% .14e   % .14e\n",   m_K[2], m_K[3]);
    printf("% .14e   % .14e\n",   m_K[4], m_K[5]);
    printf("% .14e   % .14e\n\n", m_K[6], m_K[7]);

    cout << "Result for computelqr(1,2,0) should be:" << endl
      << "-2.08073418273571e-02   -4.06650171629569e-02" << endl
      << " 4.54648713412840e-02   -1.86106523028180e-02" << endl
      << " 7.74596669241484e-01   -3.09777712011288e-17" << endl
      << "-3.63165727924092e-15    2.06566853984696e+00" << endl << endl;
#endif

    gettimeofday( &after, NULL);
    printf("[%s][%d] Timer stopped.\n", __FILE__, __LINE__);
    elapsed = timeval_subtract( after, before );
    cout << "Total elapsed time = " << elapsed << " sec" << endl;

  } // end for
  
  return 0;
}



double timeval_subtract(const timeval & t1, const timeval & t2) 
{
  double dt1 = ((double) t1.tv_sec) + (((double) t1.tv_usec) / 1000000);
  double dt2 = ((double) t2.tv_sec) + (((double) t2.tv_usec) / 1000000);

  return dt1 - dt2;
}
