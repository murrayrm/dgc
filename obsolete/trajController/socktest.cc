#include <iostream>
using namespace std;

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>
#include "trajController.h"

#define SERVERIP				"192.168.0.8"
#define SERVERPORT			5555

#define BACKLOG					10		 // how many pending connections queue will hold

#define MINSPEED        0.05
struct SState
{
	double n[3];
	double e[3];
	double yaw;
};

#define sn   state.n[0]
#define snd  state.n[1]
#define sndd state.n[2]
#define se   state.e[0]
#define sed  state.e[1]
#define sedd state.e[2]
#define sy   state.yaw
struct SCmd
{
	double accel;
	double phi;
};

int sendsock(int datafd, char* buf, int len)
{
	int numsent, numlefttosend;
	numlefttosend = len;

	do
	{
		numsent = send(datafd, buf + len - numlefttosend,
									 numlefttosend, 0);
		numlefttosend -= numsent;

		if(numsent == -1)
		{
			cerr << "Couldn't send data\n";
			return -1;
		}

		if(numsent == 0)
		{
			cerr << "no data sent\n";
			return -2;
		}
	} while(numlefttosend != 0);

	return 0;
}

int recvsock(int datafd, char* buf, int len)
{
	int numreceived, numlefttorecv;
	numlefttorecv = len;

	do
	{
		numreceived = recv(datafd, buf + len - numlefttorecv,
									 numlefttorecv, 0);
		numlefttorecv -= numreceived;

		if(numreceived == -1)
		{
			cerr << "Couldn't recv data\n";
			return -1;
		}

		if(numreceived == 0)
		{
			cerr << "no data received\n";
			return -2;
		}
	} while(numlefttorecv != 0);

	return 0;
}

int setupSockets(void)
{
	int socketfd;

	union
	{
		struct sockaddr_in addr_in;
		struct sockaddr 	 addr;
	};
		
	socklen_t sin_size;
	int yes = 1;
	

	socketfd = socket(AF_INET, SOCK_STREAM, 0);
	if(socketfd == -1)
	{
		cerr << "Couldn't open client socket\n";
		return -1;
	}

	if(setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1)
	{
		cerr << "Couldn't reuse the socket\n";
		close(socketfd);
		return -1;
	}

	memset(&addr, '\0', sizeof(addr));
	addr_in.sin_family = AF_INET;
	addr_in.sin_port = htons(SERVERPORT);
	addr_in.sin_addr.s_addr = inet_addr(SERVERIP);

	if(connect(socketfd, &addr, sizeof(addr)) == -1)
	{
		cerr << "Couldn't connect to the socket\n";
		close(socketfd);
		return -1;
	}

	return socketfd;
}

int main(int argc, char **argv)
{
	int datafd = setupSockets();

	if(datafd < 0)
		return 0;

	istream* pReftraj;
	char *pFeedbackFile;
  ifstream reftraj(argv[argc-1]);
	char defaultFileName[] = "feedback.dat";

	if(argc < 3 || !reftraj)
		pReftraj = &cin;
	else
		pReftraj = &reftraj;

	pFeedbackFile =
		(argc == 1) ? defaultFileName : argv[1];

  CTraj path(3, *pReftraj);
  CTrajController follow(&path, pFeedbackFile);

	SState state;
	VehicleState betterstate;
	SCmd cmd;

  while(true)
  {
		if(recvsock(datafd, (char*)&state, sizeof(state)) != 0)
			return 0;

		betterstate.Northing  = sn;
		betterstate.Easting   = se;
		betterstate.Yaw       = sy;

		if(betterstate.Speed() < MINSPEED)
		{
			snd = MINSPEED * cos(sy);
			sed = MINSPEED * sin(sy);
		}

		betterstate.YawRate   = (snd*sedd - sndd*sed)
			/betterstate.Speed()/betterstate.Speed();

    follow.getControlVariables( &betterstate, &cmd.accel, &cmd.phi);
		if(sendsock(datafd, (char*)&cmd, sizeof(cmd)) != 0)
			return 0;
  } 

	close(datafd);
	return 0;
}


