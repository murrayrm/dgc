#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>

#include "trajController.h"
#include "VehicleConstants.h"


// these are only used to init phi
#define xd		pTraj->getNorthingDiff(0, 1)
#define xdd		pTraj->getNorthingDiff(0, 2)
#define yd		pTraj->getEastingDiff(0, 1)
#define ydd		pTraj->getEastingDiff(0, 2)

int main(int argc, char **argv) 
{
  printf("This test file takes input file ref.traj, reads the data file\n"
          "feedback.dat, and outputs a file ff.traj which contains a crudely\n"
          "simulated state.\n");
  
  ifstream intraj("ref.traj");
  CTraj path(4, intraj);

  char* pFile = "feedback.dat";
  /* TODO: Check to see if legal. */
  CTraj* pTraj = &path;
  CTrajController follow(pTraj, pFile);

  /* TODO: The controller only uses Northing, Easting, YawRate and Speed (to
     get the steering angle), and Yaw.  Do we really need the whole struct? */
  VState_GetStateMsg state;
//   state.Northing = path.getNorthing(0)+ 5.3;
//   state.Easting  = path.getEasting(0)-1.5;
//   state.Yaw      = atan2(yd,xd)+0.1;
//   state.Speed    = sqrt(xd*xd + yd*yd)+2.0;
//   state.YawRate  = (xd*ydd-xdd*yd)/state.Speed/state.Speed;
  state.Northing = 3834760.000;
  state.Easting  = 442528.000;
  state.Yaw      = 3.1400;
  state.Speed    = 0.01;
  state.YawRate  = 0.0;

  double accel  = 0.0;
	double phi = atan(VEHICLE_WHEELBASE * (xd*ydd - xdd*yd) / pow(xd*xd + yd*yd, 3.0/2.0));

	ofstream outfile("ff.traj");
	outfile << setprecision(10);

	double t;
	double dt = 0.1;
  for(t=0.0; t<=10.0; t+=dt )
  {
    follow.getControlVariables( &state, &accel, &phi);

		state.Northing += dt*state.Speed * cos(state.Yaw);
		state.Easting  += dt*state.Speed * sin(state.Yaw);
		state.Yaw			 += dt*state.YawRate;
		state.YawRate = state.Speed * tan(phi) / VEHICLE_WHEELBASE;
		state.Speed    += dt*accel;

		outfile << state.Northing << ' ' << state.Easting << ' ' << state.Speed << endl;
  } 
  
} // end main()
