function [F, Tf, Td]  =  engine(v, th)
% F = engine(v, th)
% This function returns an engine force at the wheels based on the current
% velocity and the throttle setting.  The model used is the STI-provided
% 2nd-power RPM to torque model. 
% v - vehicle velocity (m/s)
% th - throttle setting (0 - 1)

% normalize throttle to 0 - 1
if th > 1
    th = 1;
elseif th  < 0
    th = 0;
end

% unit conversions
LBFT2NM =  1.35;

% engine parameters - as drawn from STI 
% 1996 FordClubWagonXLT_Drivetrain.vpf
K_f1 = 57.32058;
K_f2 = 2.289722;
K_f3 = -.00368453;
K_f4 = 8.314834;
K_f5 = -.1950403;
K_f6 = .0001505491;
K_t1 = 6;
K_t2 = 2;
FUDGE = 2; % fudge factor for acceleration

W_IDLE = 200 * 2*pi/60;

% gear ratios
R_tc = 1; % torque converter ratio
R_trans = 3.114; % transmission gear ratio (1st)
%R_trans = 2.216; %2nd
%R_trans = 1.545; %3rd
%R_trans = 1.000; %4th
%R_trans = 0.712;%5th
R_diff = 4.10; % differential/rear end gear ratio
R_transfer = 1.08; % transfer case ratio
R_wheel = .4; % wheel radius

% calculate the gear ratio from wheels to engine
R_we = R_tc * R_trans  * R_diff * R_transfer;

% calculate wheel speed in rad/s
w_w = v ./ R_wheel;

% calculate the engine speed based on this
w_f = max(w_w * R_we, W_IDLE);

% calculate the nonlinear throttle mapping
th_a = 1 - exp( -K_t1 * th .^(K_t2));
%th_a = th;

% calculate the forward torque and the drag torque
Tf = FUDGE * (K_f1 + K_f2  * w_f  + K_f3 * w_f.^2) * LBFT2NM;
Td = FUDGE * (K_f4 + K_f5  * w_f + K_f6 * w_f.^2)  * LBFT2NM;

Te = Tf * th_a + Td * (1 - th_a); % Calculate the total torque

T_wheel = Te * R_we; % Calculate the torque at the wheels

F = T_wheel * R_wheel; % now get the resultant force