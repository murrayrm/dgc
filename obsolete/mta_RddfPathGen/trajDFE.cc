/*
 * trajDFE
 * Created 02-28-2005 Ben Pickett
 *
 * trajDFE is Dynamic Feasibility code that modifies accelerations
 * and velocities in traj files to make them easier to follow.
 *
*/

#include "trajDFE.hh" //includes appropriate path and vector dependencies

/* ensures velocity varies continuously over dense traj
 * and assigns corresponding longitudinal accelerations */
void updateVelocity(pathstruct & path)
{
  vector<double> vCurrent(2);
  vector<double> vPrevious(2);
  if (path.numPoints > 1) //else we can't do anything substantial
    {
    for (i=1; i < path.numPoints; ++i)
      {
	//extract useful information
	//vectors are in the form <northing,easting>
	vPrevious(0) = path.vn[i-1];
	vPrevious(1) = path.ve[i-1];
	vCurrent(0) = path.vn[i];
	vCurrent(1) = path.ve[i];

	if (vecmag(vCurrent) != vecmag(vPrevious))
	  {
	  if (vecmag(vCurrent) < vecmag(vPrevious))
	    recurse_accel(path,i);
	  else
	    recurse_decel(path,i);
	  }
      }
    }
  return;
}

/* the following are helper functions for updateVelocity */
void recurse_accel(pathstruct& path, unsigned int current_point)
{
  double step_size;
  vector<double> xPrevious(2);
  vector<double> xCurrent(2);
  vector<double> vPrevious(2);
  vector<double> vCurrent(2);
  vector<double> aTangential(2);
  if (current_point)
    {
      //extract some useful info
      xPrevious(0) = path.n[current_point-1]
      xPrevious(1) = path.e[current_point-1]
      xCurrent(0) = path.n[current_point];
      xCurrent(1) = path.e[current_point];	

      //distance between points
      stepsize = vecmag(xCurrent-xPrevious);
      
      //check to see if step change in velocity is feasible
      if (vecmagsquared(vCurrent) > vecmagsquared(vPrevious) + 2*MAXACCEL*stepsize)
	{
	  setvel(path,(vPrevious/vecmag(vPrevious)) * (sqrt(vecmagsquared(vPrevious)+2*MAXACCEL*stepsize)), current_point);
	  //call function recursively
	  recurse_accel(path, current_point-1);
	}
      aTangential = (vPrevious/vecmag(vPrevious)) * (vecmagsquared(vCurrent)-vecmagsquared(vPrevious))/(2*MAXACCEL);
      //I am assuming the tangential acceleration is zero to begin with (in the origional traj file.
      //I should probably fix this to make it more robust.... at some point.
      path.an += aTangential(0);
      path.ae += aTangential(1);
    }
}

void recurse_decel (pathstruct& path, unsigned int current_point)
{
  double step_size;
  vector<double> xPrevious(2);
  vector<double> xCurrent(2);
  vector<double> vPrevious(2);
  vector<double> vCurrent(2);
  vector<double> aTangential(2);
  if (current_point)
    {
      //extract some useful info
      xPrevious(0) = path.n[current_point-1]
      xPrevious(1) = path.e[current_point-1]
      xCurrent(0) = path.n[current_point];
      xCurrent(1) = path.e[current_point];

      //distance between points
      stepsize = vecmag(xCurrent-xPrevious);
      //check to see if step change in velocity is feasible
      if (vecmagsquared(vPrevious) > vecmagsquared(vCurrent) + 2*MAXDECEL*stepsize)
	{
	  setvel(path,(vCurrent/vecmag(vCurrent)) * (sqrt(vecmagsquared(vCurrent)+2*MAXDECEL*stepsize)), current_point-1);
	  //call function recursively
	  recurse_decel(path, current_point-1);
	}
      aTangential = (vPrevious/vecmag(vPrevious)) * (vecmagsquared(vCurrent)-vecmagsquared(vPrevious))/(2*MAXACCEL);
      //I am assuming the tangential acceleration is zero to begin with (in the origional traj file.
      //I should probably fix this to make it more robust.... at some point.
      path.an += aTangential(0);
      path.ae += aTangential(1);
    }
}
