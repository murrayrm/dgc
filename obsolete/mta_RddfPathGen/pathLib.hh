// pathLib.hh contains all the helper functions for RddfPathGen
#include <iostream>
using namespace std;

#include "vectorOps.hh"
#include "traj.h"
#include "RddfPathGen.hh"
#include <math.h>
#define PI 3.1415926535898


struct pathstruct
{
  // number of points in the path.  The last point is the end point
  // of the path, so ve, vn, ae, and an are irrelevant at the last
  // point
  int numPoints;

  // the index of the point in the path that we were closest too
  // last time we checked.  it's necessary to keep track of this
  // to prevent errors due to the path crossing over
  // currentPoint is zero based
  int currentPoint;
    
  // eastings, northings, eastward velocity, northward velocity
  // eastward accel, northward accel
  vector<double> e, n, ve, vn, ae, an;
};



struct corridorstruct
{
  // number of points in corridor.  last point is endpoint.
  int numPoints;
    
  // eastings, northings, corridor widths, rddf imposed speed limits.
  // width[1] is the width of the corridor between (e[1], n[1]) and 
  // (e[2], n[2]).  speedLimit[1] is the speed limit imposed by the
  // rddf on the first corridor segment.  width[points] and
  // speedLimit[points] are irrelevant.
  vector<double> e, n, width, speedLimit;
};



void printPath(pathstruct path);
pathstruct emptyPath();

void addToPath(pathstruct & path, double e, double n, double ve, double vn, double ae, double an);
void addToPath(pathstruct & path, double e, double n, vector<double> vel, double ae, double an);
void addToPath(pathstruct & path, double e, double n, vector<double> vel, vector<double> accel);

void StoreWholePath(CTraj & traj, const corridorstruct & cor);
void StorePath(CTraj & traj, const pathstruct & path);
void StorePathFragment(CTraj & traj, const corridorstruct & cor);

void WritePath(const pathstruct & path, ostream & outstream);
void WritePathToFile(const pathstruct & path, char filename[]);

pathstruct DensifyAndChop(const pathstruct & path_whole_sparse, vector<double> location,
			  const double chopBehind, const double chopAhead);
void UpdateCurrentPoint(pathstruct & path, vector<double> & vehicle_location);
pathstruct ChopPath(const pathstruct & path, 
		    const double chop_behind, const double chop_ahead);
pathstruct DensifyPath(const pathstruct & path_sparse);

double length_limited_curve(double & theta, double & start_x, double & start_y, corridorstruct corridor_segment);
double width_limited_curve(double & theta, double & start_x, double & start_y, corridorstruct corridor_segment);
vector<double> curve2accel(corridorstruct corridor_segment, double r,double v);

pathstruct Path_From_Corridor(corridorstruct corridor);




void printPath(pathstruct path)
{
  //###
  //  cout << "Path has " << path.numPoints << " points." << endl;
  //###
  for(int i = 0; i < path.numPoints; i++)
    {
      cout << "[ " 
	   << path.e[i] << ", "
	   << path.n[i] << ", "
	   << path.ve[i] << ", "
	   << path.vn[i] << ", "
	   << path.ae[i] << ", "
	   << path.an[i] << " ]\n";
    }
}



pathstruct emptyPath()
{
  pathstruct ret;
  ret.numPoints = 0;
  ret.currentPoint = 0;
  return ret;
}



void addToPath(pathstruct & path, double e, double n, 
	       double ve, double vn, double ae, double an)
{
  path.numPoints++;
  path.e.push_back(e);
  path.n.push_back(n);
  path.ve.push_back(ve);
  path.vn.push_back(vn);
  path.ae.push_back(ae);
  path.an.push_back(an);
  //###
  //cout << "\nPath currently is:\n";
  //printPath(path);
  //###
}



void addToPath(pathstruct & path, double e, double n, 
	       vector<double> vel, double ae, double an)
{
  path.numPoints++;
  path.e.push_back(e);
  path.n.push_back(n);
  path.ve.push_back(vel[0]);
  path.vn.push_back(vel[1]);
  path.ae.push_back(ae);
  path.an.push_back(an);
  //###
  //cout << "\nPath currently is:\n";
  //printPath(path);
  //###
}



void addToPath(pathstruct & path, double e, double n, 
	       vector<double> vel, vector<double> accel)
{
  path.numPoints++;
  path.e.push_back(e);
  path.n.push_back(n);
  path.ve.push_back(vel[0]);
  path.vn.push_back(vel[1]);
  path.ae.push_back(accel[0]);
  path.an.push_back(accel[1]);
  //###
  //cout << "\nPath currently is:\n";
  //printPath(path);
  //###
}



void StoreWholePath(CTraj & traj, const corridorstruct & cor)
{
  // figure out whole path
  pathstruct path;
  path = Path_From_Corridor(cor);

  // make velocities agreeable
  // doSomethingWithVelocities(path);

  // transfer data from path to traj
  int N = path.numPoints;
  double n[3], e[3];
  traj.startDataInput();
  for(int i = 0; i < N; i++)
    {
      e[0] = path.e[i];
      e[1] = path.ve[i];
      e[2] = path.ae[i];
      n[0] = path.n[i];
      n[1] = path.vn[i];
      n[2] = path.an[i];
      traj.inputWithDiffs(n, e);
    }
  
  //###
  //  cout << "Traj that we're about to send is: " << endl;
  //  traj.print();
  //###
}



void StorePath(CTraj & traj, const pathstruct & path)
{
  // transfer data from path to traj
  int N = path.numPoints;
  double n[3], e[3];
  traj.startDataInput();
  for(int i = 0; i < N; i++)
    {
      e[0] = path.e[i];
      e[1] = path.ve[i];
      e[2] = path.ae[i];
      n[0] = path.n[i];
      n[1] = path.vn[i];
      n[2] = path.an[i];
      traj.inputWithDiffs(n, e);
    }
}



void WritePath(const pathstruct & path, ostream & outstream)
{
  // took this out per laura's request...
  //outstream << "% Generated by RddfPathGen" << endl
  //	  << "% format is N, dN, ddN, E, dE, ddE" << endl;

  int width = 12;
  for (int i = 0; i < path.numPoints; i++)
    {
      // we'll output in the traj format N, dN, ddN, E, dE, ddE
      outstream << setprecision(3) << setiosflags(ios::showpoint) << setiosflags(ios::fixed)
		<< setw(width) << path.n[i] << setw(width) << path.vn[i]
		<< setw(width) << path.an[i] << setw(width) << path.e[i]
		<< setw(width) << path.ve[i] << setw(width) << path.ae[i]
		<< endl;
    }
}



void WritePathToFile(const pathstruct & path, char filename[])
{
  ofstream output_file;
  output_file.open(filename, ios::trunc | ios::out);
  if(!output_file.is_open())
    cout << "\n ! Unable to open file " << filename << " , no data will be written!" << endl;
  else
    WritePath(path, output_file);
  
  output_file.close();
}



pathstruct DensifyAndChop(pathstruct & path_whole_sparse, vector<double> location,
			  const double chop_behind, const double chop_ahead)
{
  //
  // pathstruct DensifyAndChop(const pathstruct & path_whole_sparse,
  //                           const double & chop_behind, const double & chop_ahead)
  //
  // Changes:
  //   2-8-2004, Jason Yosinski, created
  //
  // Function:
  //   Generates from a sparse path a dense path that starts a distance
  //   chopBehind behind our current location and chopAhead ahead of
  //   our current location (determined by querying vstate)
  // 
  // Input:
  //   path_whole_dense
  //     the path we're going to be generating the dense path from
  //   chopBehind
  //     the distance behind our current locaitn that the dense path will be
  //     chopped off at
  //   chopAhead
  //     the distance ahead of our current location that the dense path will be
  //     chopped off at
  //
  // Usage example:
  //   pathstruct path_chopped_dense = DensifyAndChop(path_whole_sparse, CHOPBEHIND, CHOPAHEAD);
  //
  
  //###
  //cout << "Got into DensifyAndChop" << endl;
  //###

  // Update path_whole_sparse.currentPoint to reflect our new location
  UpdateCurrentPoint(path_whole_sparse, location);
  //  cout << "Update1 CP = " << path_whole_sparse.currentPoint;

  // Chop our current whole sparse path down to a managable size before we densify it
  pathstruct chopped_sparse_path = ChopPath(path_whole_sparse, chop_behind, chop_ahead);
  //  cout << "    Chop1 CP = " << chopped_sparse_path.currentPoint;

  // Densify the path
  pathstruct prechopped_dense_path = DensifyPath(chopped_sparse_path);

  // update the currentPoint in the dense path
  UpdateCurrentPoint(prechopped_dense_path, location);
  //  cout << "    Update2 CP = " << prechopped_dense_path.currentPoint;

  // further trim our dense path (to avoid sending a huge amount of data over the network)
  pathstruct chopped_dense_path = ChopPath(prechopped_dense_path, chop_behind, chop_ahead);
  //  cout << "    Chop2 CP = " << chopped_dense_path.currentPoint << endl;

  //###
  //cout << "About to leave DensifyAndChop" << endl;
  //###

  // just to get it to compile...
  return chopped_dense_path;
}



void UpdateCurrentPoint(pathstruct & path, vector<double> & vehicle_location)
{
  // 
  // Changes:
  //   2-11-2005, Jason Yosinski, created
  //
  // Function:
  //   Finds the point on a path closest to the current location.  The function
  //   accomplishes this by stepping along the path from the current point
  //   until a step brings it farther from location.  The funciton stores the point
  //   as the last point before it went farther away.  It works this way so it can
  //   deal with overlapping paths.
  //

  if (path.currentPoint < 0 || path.currentPoint > path.numPoints + 1)
    cerr << "UpdateCurrentPoint:  Error - path.currentPoint = " << path.currentPoint << endl;

  double current_distance, next_distance;
  vector<double> current_location(2);  // N,E format
  vector<double> next_location(2);  // N,E format

  while(true)
    {
      if (path.currentPoint >= path.numPoints - 1)
	// we've reached the last point
	break;

      current_location[0] = path.n[path.currentPoint];
      current_location[1] = path.e[path.currentPoint];
      current_distance = vecmag(current_location - vehicle_location);

      next_location[0] = path.n[path.currentPoint + 1];
      next_location[1] = path.e[path.currentPoint + 1];
      next_distance = vecmag(next_location - vehicle_location);

      if (next_distance > current_distance)
	// we're better off where we are now, so this is final current point
	break;
      else
	// we're better off at the next point, so make that our current point
	// and continue through the loop
	path.currentPoint++;
    }
}



pathstruct ChopPath(const pathstruct & path, 
		    const double chop_behind, const double chop_ahead)
{
  // 
  // Changes:
  //   2-11-2005, Jason Yosinski, created
  //
  // Function:
  //   From the path.currentPoint, Chop path will step
  //   back a total distance of chop_behind or the distance to the beginning
  //   of the path, whichever is smaller, and throw away whatever part of
  //   the path is before that point.
  //   Similarly, ChopPath will step forward a distance of chop_ahead or the
  //   distance to the end of the path, whichever is smaller, and throw the
  //   rest of the path away.
  //
  

  // Find the index of the furthest back point
  int furthest_back_index = path.currentPoint;
  if (furthest_back_index > 0)
    {
      furthest_back_index--; // step back one to account for the fact that we
                             // may actually be in the middle of a segment
      double dist_back_sofar = 0;
      vector<double> point1(2), point2(2); // (N,E) format
      while(furthest_back_index > 0 && dist_back_sofar < chop_behind)
	{
	  furthest_back_index--;
	  point2[0] = path.n[furthest_back_index+1];
	  point2[1] = path.e[furthest_back_index+1];
	  point1[0] = path.n[furthest_back_index];
	  point1[1] = path.e[furthest_back_index];
	  dist_back_sofar += vecmag(point2-point1);
	}
    }


  // Find the index of the furthest ahead point
  int furthest_ahead_index = path.currentPoint;
  int last_point_index = path.numPoints-1; // to avoid confusion
  if (furthest_ahead_index < last_point_index)
    {
      furthest_ahead_index++; // step ahead one to account for the fact that we
                             // may actually be in the middle of a segment
      double dist_ahead_sofar = 0;
      vector<double> point1(2), point2(2); // (N,E) format
      while(furthest_ahead_index < last_point_index
	    && dist_ahead_sofar < chop_ahead)
	{
	  furthest_ahead_index++;
	  point2[0] = path.n[furthest_ahead_index];
	  point2[1] = path.e[furthest_ahead_index];
	  point1[0] = path.n[furthest_ahead_index-1];
	  point1[1] = path.e[furthest_ahead_index-1];
	  dist_ahead_sofar += vecmag(point2-point1);
	}
    }


  //printf("(%d, %d, %d, %d, %d)", 0, furthest_back_index, path.currentPoint,
  //	 furthest_ahead_index, last_point_index);
  //cout.flush();

  // create and store a new path using furthest_back_index and 
  // furthest_ahead_index
  pathstruct ret = emptyPath();
  if (furthest_ahead_index > last_point_index
      || furthest_ahead_index < 0
      || furthest_back_index < 0
      || furthest_back_index > last_point_index)
    cerr << "ERROR in Chop Path:  this really shouldn't be happening..." << endl;
  for (int i = furthest_back_index; i <= furthest_ahead_index; i++)
    addToPath(ret, path.e[i], path.n[i],
	      path.ve[i], path.vn[i], path.ae[i], path.an[i]);
  int chopped_off_beginning = furthest_back_index;
  ret.currentPoint = path.currentPoint - chopped_off_beginning;


  // returtn that bad boy
  return ret;
}



pathstruct DensifyPath(const pathstruct & path_sparse)
{
  /* Ben Pickett

     this script creates a dense path from a sparse path.  I use the
     accelerations and velocities specified in the given path to determine the
     course of the path between path points.  I assume the path will be
     contiguous using the above information. ie, the velocity at point 2 will
     be achieved by maintaining the acceleration specified at point 1 until I
     get to point 2.  Under these assumptions, the path has a continuous
     time derivative and the second derivative is piecewise continuous.  All I
     have to do is fill in the spaces between points with more points and fill
     out the already smooth path.  If the path sent to this function is not
     smooth then this function will fail (probably silently).
  */

  double step = DENSITY; // defined in RddfPathGen.hh

  pathstruct path_dense = emptyPath();
  int N = path_sparse.numPoints;

  for(int i = 0; i < N-1; i++)
    {
      //###
      //cout << "In main DensifyPath loop, i = " << i << endl;
      //###
        vector<double> point0(2); point0[0] = path_sparse.e[i]; point0[1] = path_sparse.n[i];
        vector<double> velocity0(2); velocity0[0] = path_sparse.ve[i]; velocity0[1] = path_sparse.vn[i];
        vector<double> accel0(2); accel0[0] = path_sparse.ae[i]; accel0[1] = path_sparse.an[i];
	double speed0 = vecmag(velocity0);
	double accelmag = vecmag(accel0);
        vector<double> point1(2); point1[0] = path_sparse.e[i+1]; point1[1] = path_sparse.n[i+1];

	vector<double> normvel0(2);
	if (speed0 != 0)
	  normvel0 = velocity0 / speed0;  // normalize the velocity
	else
	  normvel0 = velocity0;  // velocity0 is (0,0)

	vector<double> normaccel0(2), acceltan(2), accelperp(2);
	if (accelmag != 0)
	  {
	    normaccel0 = accel0 / accelmag;  // normalize the acceleration
	    // component of accel in tangential direction
	    acceltan = accelmag * vecdot(normvel0, normaccel0) * normvel0;
	    // component of accel in perpendicular direction
	    accelperp = accel0 - acceltan;
	  }
	else
	  {
	    normaccel0 = accel0;  // accel0 is (0,0)
	    // component of accel in tangential direction (is (0,0))
	    acceltan = accel0;
	    // component of accel in perpendicular direction (is (0,0))
	    accelperp = accel0;
	  }

	

	if (vecmag(accelperp) < 1e-9)  // 1e-9 is a tolerance to figure out if path is straight or not
	  {
	    // this is a straght path segment
	    double length = vecmag(point1 - point0);
	    double sign = vecdot(normvel0, normaccel0);
	    for (double ds = 0; ds < length; ds += step)
	      {
		vector<double> point = point0 + normvel0 * ds;

		// sign: direction of accel. +1 means forward, -1 means backwards.
		// this works because normvel0 and normaccel0 have unit magnitudes and are
		// parallel.  If accelmag == 0 then either equation is valid.

		addToPath(path_dense, point[0], point[1],
			  normvel0 * sqrt(pow(speed0,2) + 2 * sign * accelmag * ds),
			  accel0);
	      }
	  }
	else
	  {
	    // this is a curved path segment
	    double radius = (speed0 * speed0) / vecmag(accelperp);
	    vector<double> center = point0 + accelperp * radius / vecmag(accelperp);
	    vector<double> ray0 = point0 - center;
	    vector<double> ray1 = point1 - center;

	    double theta_min = atan2(ray0[1], ray0[0]);
	    double theta_max = atan2(ray1[1], ray1[0]);
	    double theta_step = step / radius;

	    // force sweep angle to be between -pi and pi
	    double sweep = atan2(sin(theta_max-theta_min), cos(theta_max-theta_min));

	    if (sweep < 0) theta_step *= -1;  // case for decreasing theta

	    for (double theta = theta_min; abs(theta_min+sweep-theta) > abs(theta_step); theta += theta_step)
	      {
		//###
		//printf("theta=%f  theta_min=%f  sweep=%f  theta_step=%f  theta_min+sweep-theta=%f",
		//       theta, theta_min, sweep, theta_step, theta_min+sweep-theta);
		//cout << endl;
		//###
		vector<double> ray(2); ray[0] = cos(theta); ray[1] = sin(theta);
		vector<double> point = center + radius * ray;

		double sign;
		if (sweep > 0) sign = 1; else sign = -1;

		// add points along the circular arc
		vector<double> normray = ray / vecmag(ray);
		vector<double> normtang(2); normtang[0] = cos(theta + sign * PI/2); normtang[1] = sin(theta + sign * PI/2);
		addToPath(path_dense, point[0], point[1],
			  speed0 * normtang,
			  -accelmag * normray);
		
	      } 
	  }
    }
  return path_dense;
}



void StorePathFragment(CTraj & traj, const corridorstruct & cor)
{
  cout << "Still need to write store path fragment" << endl;
}



/* not currently implemented
corridorstruct file2corridor(ifstream & file)
{
  // 
  // function corridor = file2corridor(file)
  //
  // Changes: 
  //   12-7-2004, Jason Yosinski, created (in Matlab, some code from plot_corridor.m)
  //   12-29-2004, Jason Yosinski, ported to C++
  //
  // Function:
  //   This function reads a *.bob format RDDF file and outputs the
  //   corrosponding corridorstruct for use with functions such as
  //   Path_From_Corridor
  //
  // Input:
  //   ifstream file - must be *.bob format
  //
  // Output:
  //   corridor in the standard corridorstruct format
  //
  // Usage example:
  //   char* filename = "bobfile.bob";
  //   ofstream file(filename);
  //   corridorstruct cor = file2corridor(file);

  // read in values from the file
  // [type,eastings,northings,radii,vees] = 
  //     textread(file, '%s%f%f%f%f', 'commentstyle', 'shell');

  // corridor = [eastings, northings, radii, vees];
  cout << "Need to finish file2corridor!  Returning blank corridor instead!" << endl;

  corridorstruct ret;
  return ret;
}
*/



double length_limited_curve(double & theta,
			    double & start_x,
			    double & start_y,
			    corridorstruct corridor_segment)
{
  //
  // function [radius, theta, start_x, start_y] = length_limited_curve(corridor_segment)
  //
  // Changes:
  //   12-6-2004, Benjamin Pickett, created (in Matlab)
  //   12-28-2004, Jason Yosinski, ported to C++
  //
  // Function:
  //   Update: now returns radius. 
  //   old stuff from matlab:
  //   This function takes a corridor segment consisting of 3 points
  //       corridor_segment = [x1 y1 width1;
  //                           x2 y2 width2;
  //                           x3 y3 width3]
  // and calculates the circular arc with a radius defined by creating a
  // smooth curve (continuous first derivative) such that:
  //       1.  The arc starts tangent to the centerline between point1 and
  //           point2.
  //       2.  The arc ends tangent to the centerline between point2 and
  //           point3.
  //       3.  The start and end points are equidistant from the corner of the
  //           corridor.
  //       4.  The distance from (3) is defined to be the minimum of the two
  //           leg lengths (point1 to point2 and point2 to point3).
  //
  //   The function returns the radius of the arc in question, the x and y
  //   locations of the start of the arc (start_x and start_y), and theta.
  //   Theta is half of the exterior angle between the two line segments 
  //   joining the three points, MEASURED IN RADIANS.
  //
  // Usage example:
  //   [radius, theta, start_x, start_y] = width_limited_curve(corridor_segment)
  //
  
  // extract points from corridor_segment
  vector<double> point1(2); point1[0] = corridor_segment.e[0]; point1[1] = corridor_segment.n[0];
  vector<double> point2(2); point2[0] = corridor_segment.e[1]; point2[1] = corridor_segment.n[1];
  vector<double> point3(2); point3[0] = corridor_segment.e[2]; point3[1] = corridor_segment.n[2];

  // vectors from point1 to point2 and point2 to point3
  vector<double> ray12 = point2 - point1;
  vector<double> ray23 = point3 - point2;

  // take the half the shorter of the two distances to the corner
  double d = min(vecmag(ray12), vecmag(ray23)) / 2;

  // use half angle formula and dot product to find cos
  // ( cos = u*v/|u||v| )
  theta = .5 * acos(vecdot(ray12, ray23) / (vecmag(ray12)*vecmag(ray23)));
  
  double radius = d / tan(theta);

  // distance d from corner in the direction of v1
  vector<double> start = point2 - (d*ray12)/vecmag(ray12);
  start_x = start[0];
  start_y = start[1];

  return radius;
}



double width_limited_curve(double & theta,
			   double & start_x,
			   double & start_y, 
			   corridorstruct corridor_segment)
{
  // 
  // function [radius theta start_x start_y] = width_limited_curve(corridor_segment)
  //
  // Changes: 
  //   12-6-2004, Jason Yosinski, created (in Matlab)
  //   12-26-2004, Jason Yosinski, ported to C++
  //
  // Function:
  //   This function returns the radius
  //   It takes a corridor segment consisting of 3 points
  //       corridor_segment = [x1 y1 width1;
  //                           x2 y2 width2;
  //                           x3 y3 width3]
  //   and calculates the circular arc with the largest possible radius such
  //   that:
  //       1. The arc starts along and tangent to the centerline between
  //           point1 and point2
  //       2. The arc ends along and tangent to the centerline between point2
  //           and point3
  //       3. The arc just touches the inside corner of the two corridors
  //           (meaning if it were any larger, the path would go outside the
  //           corridor)
  //   Note that the widths are radii rather than diameters, that is, the
  //   corridor is actually 20 meters wide from outside edge to outside edge
  //   if width = 10 meters
  //
  //   The function returns the radius of the arc in question, the x and y
  //   locations of the start of the arc (start_x and start_y), and theta.
  //   Theta is half of the exterior angle between the two line segments 
  //   joining the three points, MEASURED IN RADIANS.
  //
  // Usage example:
  //   [radius theta start_x start_y] = width_limited_curve(corridor_segment)
  // extract points and widths from corridor_segment

  // useful quantities to define for manipulation
  vector<double> point1(2); point1[0] = corridor_segment.e[0]; point1[1] = corridor_segment.n[0];
  vector<double> point2(2); point2[0] = corridor_segment.e[1]; point2[1] = corridor_segment.n[1];
  vector<double> point3(2); point3[0] = corridor_segment.e[2]; point3[1] = corridor_segment.n[2];
  double width12 = corridor_segment.width[0];
  double width23 = corridor_segment.width[1];

  // rays from point1 to point2 and point2 to point3
  vector<double> ray12 = point2 - point1;
  vector<double> ray23 = point3 - point2;
//   cout << "\nDEBUG point1 = "; vecprint(point1);
//   cout << "\nDEBUG point2 = "; vecprint(point2);
//   cout << "\nDEBUG point3 = "; vecprint(point3);

  // min_width is the limiting corridor width, namely the smaller of the first
  // and second corridor widths
  double min_width = min(width12, width23);

  // calculate theta (and return it)
  theta = .5 * acos(vecdot(ray12, ray23) / (vecmag(ray12)*vecmag(ray23)));
//   cout << "\nDEBUG ray12 = "; vecprint(ray12);
//   cout << "\nDEBUG ray23 = "; vecprint(ray23);
//   cout << "\nDEBUG acos of " << vecdot(ray12, ray23) << " / " << (vecmag(ray12)*vecmag(ray23));

  // calculate the radius
  double radius = min_width / (1 - cos(theta));

  // calculate b, which is the distance from the intersection of the two
  // centerlines to the start of the curve
  double b = radius * tan(theta);
  
  // calculate the start of curve point
  vector<double> start = point2 - (ray12 / vecmag(ray12)) * b;
  
  // return the start point and radius
  start_x = start[0];
  start_y = start[1];

  return radius;
}



vector<double> curve2accel(corridorstruct corridor_segment,
			   double r,
			   double v)
{
  //
  // function accel = curve2accel( corridor_segment, radius, speed)
  //
  // Changes:
  //   12-7-2004, Benjamin Pickett, created (in Matlab)
  //   12-28-2004, Jason Yosinski, ported to C++
  //
  // Function:
  //   This function takes a corridor segment consisting of 3 points
  //       corridor_segment = [x1 y1 width1;
  //                           x2 y2 width2;
  //                           x3 y3 width3]
  // and a predefined radius of curvature for the desired path.  The function
  // returns the centripetal acceleration required to begin traveling in a
  // circular arc from a point on the first leg of path segment (x1,y1) to
  // (x2,y2).  The acceleration returned by this function is designed to be
  // one of the components needed for defining a segment of path to send to
  // path following.
  //
  // Usage example:
  //   accel = curve2accel( corridor_segment, radius, speed)
  
  // extract points from corridor_segment
  vector<double> point1(2); point1[0] = corridor_segment.e[0]; point1[1] = corridor_segment.n[0];
  vector<double> point2(2); point2[0] = corridor_segment.e[1]; point2[1] = corridor_segment.n[1];
  vector<double> point3(2); point3[0] = corridor_segment.e[2]; point3[1] = corridor_segment.n[2];

  // vectors from point1 to point2 and point2 to point3
  vector<double> ray12 = point2-point1;
  vector<double> ray23 = point3-point2;

  //project ray23 onto ray12
  vector<double> projray23ray12 = ray12 * vecdot(ray12, ray23) / pow(vecmag(ray12),2);

  //normalize orthogonal component of v23 and scale by |a| = v^2/r
  vector<double> accel = (pow(v, 2)/r) * (ray23-projray23ray12) / vecmag(ray23-projray23ray12);

  return accel;
}



pathstruct Path_From_Corridor(corridorstruct corridor)
{
  // 
  // Changes: 
  //   12-6-2004, Jason Yosinski, created (in Matlab)
  //   12-26-2004, Jason Yosinski, ported to C++
  //
  // Function:
  //   This function generates a somewhat optimal (max speed for any given
  //   segment is within a factor of sqrt(2) of the optimal max speed I
  //   think...) path from a corridor
  //
  // Input:
  //   struct corridorstruct
  //   {
  //     // number of points in corridor.  last point is endpoint.
  //     int points;
  //     
  //     // eastings, northings, corridor widths, rddf imposed speed limits.
  //     // width[1] is the width of the corridor between (e[1], n[1]) and 
  //     // (e[2], n[2]).  speedLimit[1] is the speed limit imposed by the
  //     // rddf on the first corridor segment.  width[points] and
  //     // speedLimit[points] are irrelevant.
  //     listofsomesort e, n, width, speedLimit;
  //   }
  //
  // Output:
  //   struct pathstruct
  //   {
  //     // number of points in the path.  The last point is the end point
  //     // of the path, so ve, vn, ae, and an are irrelevant at the last
  //     // point
  //     int points;
  //     
  //     // eastings, northings, eastward velocity, northward velocity
  //     // eastward accel, northward accel
  //     listofsomesort e, n, ve, vn, ae, an;
  //   }
  //
  // Usage example:
  //   see function prototype
  //
  
  
  // get number of points in corridor
  int N = corridor.numPoints;

  // create a blank path
  pathstruct path = emptyPath();
  
  
  // figure out if some l00zer passed us a messed up corridor with zero or one
  // points in it
  if (N == 0)
    {
      cout << "You fool - the corridor passed to path_from_corridor is empty!";
      return path;
    }
  
  else if (N == 1)
    {
      // corridor is just a single point, so i guess our path says we're
      // already there! (path_from_corridor is passive-agressive)
      addToPath(path, corridor.e[0], corridor.n[0], 0, 0, 0, 0);
      return path;
    }
  
  else if (N == 2)
    {
      // corridor is two points, so our path is a straight line 
      // connecting them
      
      // direction (get, then normalize)
      vector<double> direction(2);
      direction[0] = corridor.e[1] - corridor.e[0];
      direction[1] = corridor.n[1] - corridor.n[0];
      direction = direction / vecmag(direction);
      // speedLimit
      double speedLimit = corridor.speedLimit[0];

      addToPath(path, corridor.e[0], corridor.n[0], direction[0] * speedLimit,
		direction[1] * speedLimit, 0, 0);
      addToPath(path, corridor.e[1], corridor.n[1], 0, 0, 0, 0);
    }
  
  else
    {
      // *whew*... finally... a challenge - a corridor with at least 2 points
      // in it!
      
      // do first path segment (starting location to halfway down first segment)
      // direction (get, then normalize)
      vector<double> direction(2);
      direction[0] = corridor.e[1] - corridor.e[0];
      direction[1] = corridor.n[1] - corridor.n[0];
      direction = direction / vecmag(direction);
      // speedLimit
      double speedLimit = corridor.speedLimit[0];
      
      // store first path segment
      addToPath(path, corridor.e[0], corridor.n[0], direction[0] * speedLimit,
		direction[1] * speedLimit, 0, 0);
      //###
      //cout << "corridor.e " << corridor.e[0] << "  "
      //	   << "corridor.n " << corridor.n[0] << "  "
      //	   << "vx " << direction[0] * speedLimit << "  "
      //	   << "vy " << direction[1] * speedLimit << endl;
      //cout << "did first path segment, path currently is:" << endl;
      //printPath(path);
      //cout << endl;
      //###

      // do rest of path segments
      for (int i = 1; i <= N-2; i++)
	{
	  // our story starts with a particular i.  we now start along the
	  // corridor at a position equal to the midpoint of the "ith" segment
	  
	  // some useful stuff...

	  //###
	  //cout << "got here, i =" << i << endl;
	  //###

	  corridorstruct corridor_segment;
	  for (int j = 0; j <= 2; j++)
	    {
	      corridor_segment.numPoints = 3;
	      corridor_segment.e.push_back(corridor.e[i+j-1]);
	      corridor_segment.n.push_back(corridor.n[i+j-1]);
	      corridor_segment.width.push_back(corridor.width[i+j-1]);
	      corridor_segment.speedLimit.push_back(corridor.speedLimit[i+j-1]);
	    }
	  
	  //###
	  //cout << "constructed corridor_segment" << endl;
	  //###
	  
	  // extract points from corridor_segment
	  vector<double> point1(2); point1[0] = corridor_segment.e[0];
	  point1[1] = corridor_segment.n[0];
	  vector<double> point2(2); point2[0] = corridor_segment.e[1];
	  point2[1] = corridor_segment.n[1];
	  vector<double> point3(2); point3[0] = corridor_segment.e[2];
	  point3[1] = corridor_segment.n[2];
	  
	  // vectors from point1 to point2 and point2 to point3
	  vector<double> ray12 = point2-point1;
	  vector<double> ray23 = point3-point2;
	  
	  // directions (normalized rays)
	  
	  vector<double> dir12 = ray12 / vecmag(ray12);
	  vector<double> dir23 = ray23 / vecmag(ray23);
	  
	  // speed limits imposed by the rddf
	  double sl12 = corridor_segment.speedLimit[0];
	  double sl23 = corridor_segment.speedLimit[1];
	  
	  
	  // figure out whether the length of the segment or the width of the
	  // corridor limits the curvature
	  double radiusW, thetaW, start_xW, start_yW;
	  double radiusL, thetaL, start_xL, start_yL;
	  
	  //###
	  //cout << "here before curves" << endl;
	  //###
	  // get width limited data
	  radiusW = width_limited_curve(thetaW, start_xW, start_yW, corridor_segment);

	  //###
	  //cout << "width done, radius = " << radiusW << endl;
	  //###
	  // get length limited data
	  radiusL = length_limited_curve(thetaL, start_xL, start_yL, corridor_segment);
	  //###
	  //cout << "length done, radius = " << radiusL << endl;
	  //###

	  //###
	  //disp('[i, radiusL, radiusW] =');
	  //disp([i, radiusL, radiusW]);
	  //###
	  
	  // we now have two curves, each limited by a different factor.  we want
	  // to use the one with the stricter limitation, namely the one with the
	  // smaller radius of curvature.
	  
	  //###
	  //rads = [rads min(radiusL, radiusW)];
	  //###
	  if (radiusL < radiusW)
	    {
	      // the limiting factor is the LENGTH of one of the corridor
	      // segments (the shorter one), so next we'll figure out which
	      // segment it was...

	      // velocity at beginning of curve
	      vector<double> vel = dir12 * min(sl12, sl23);

	      if (vecmag(point2 - point1) < vecmag(point3 - point2))
		{
		  // the first segment limited the curve, so the path continues
		  // here with a curve
		  addToPath(path, start_xL, start_yL, vel,
			    curve2accel(corridor_segment, radiusL, min(sl12, sl23)));
		  
		  // once that curve is done, the path continues on in a straight
		  // line to at least the next midpoint
		  // end of curve point (which != next midpoint)
		  vector<double> end_of_curve_point = point2 + dir23 * radiusL * tan(thetaL);
		  addToPath(path, end_of_curve_point[0], end_of_curve_point[1],
			    dir23 * min(sl12, sl23), 0, 0);
		}
	      else
		{
		  // the second segment limited the curve, so the path continues
		  // here first with a straght segment
		  vector<double> start_of_curve_point = point1 + .5 * ray12;
		  addToPath(path, start_of_curve_point[0], start_of_curve_point[1],
			    dir12 * sl12, 0, 0);
		  
		  // then comes the curve, which ends at the next midpoint
		  addToPath(path, start_xL, start_yL, dir12 * min(sl12, sl23),
			    curve2accel(corridor_segment, radiusL, min(sl12, sl23)));
		}
	    }
	  else
	    {
	      // the limiting factor is the WIDTH of the corridor, so we need a
	      // straight segment, then a curve, then another straight piece.
	      vector<double> straight_start = point1 + .5 * ray12;
	      addToPath(path, straight_start[0], straight_start[1],
			dir12 * sl12, 0, 0);
	      addToPath(path, start_xW, start_yW, dir12 * min(sl12, sl23),
			curve2accel(corridor_segment, radiusW, min(sl12, sl23)));
	      straight_start = point2 + dir23 * radiusW * tan(thetaW);
	      addToPath(path, straight_start[0], straight_start[1], dir23 * sl23,
			0, 0);
	    }
	}
    }
  
  
  // do last actual path segment (straight line from midpoint of last
  // corridor segment to the last point in the corridor)

  // second last point
  vector<double> point1(2); point1[0] = corridor.e[N-2]; point1[1] = corridor.n[N-2];
  // last point
  vector<double> point2(2); point2[0] = corridor.e[N-1]; point2[1] = corridor.n[N-1];
  // ray from second last point to last point
  vector<double> ray12 = point2 - point1;
  // speed limit of last stretch of corridor
  double sl12 = corridor.speedLimit[N-2];
  // midpoint of last stretch of corridor
  vector<double> last_midpoint = .5 * (point1 + point2);

  //###
  //cout << "Two more addToPath's to go..." << endl;
  //###
  addToPath(path, last_midpoint[0], last_midpoint[1],
	    ray12 / vecmag(ray12) * sl12, 0, 0);

  
  // tack on a final point to the path (so everyone else knows how long the
  // last path segment should be
  addToPath(path, point2[0], point2[1], 0, 0, 0, 0);

  return path;
}
