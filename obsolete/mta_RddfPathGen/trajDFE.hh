#ifndef TRAJDFE
#define TRAJDFE
/*
 * trajDFE
 * Created 02-28-2005 Ben Pickett
 *
 * trajDFE is Dynamic Feasibility code that modifies accelerations and velocities in traj files
 * to make them easier to follow.
 *
*/

using namespace std;

#include "RddfPathGen.hh"
#include "traj.h"
#include "pathLib.hh"
#include "vectorOps.hh"

#include <VehicleConstants.hh>

/* ensures velocity varies continuously over dense traj
 * and assigns corresponding longitudinal accelerations */
void updateVelocity(pathstruct & dense_path); 

/* the following are helper functions for updateVelocity */
void recurse_accel(pathstruct& path, unsigned int current_point);
void recurse_decel(pathstruct& path, unsigned int current_point);

#endif TRAJDFE
