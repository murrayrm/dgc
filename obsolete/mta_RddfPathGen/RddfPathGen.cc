#include "pathLib.hh"
#include "RddfPathGen.hh"
#include "rddf.hh"
#include <iomanip>

#include "ModeManModule.hh"



RddfPathGen::RddfPathGen() : DGC_MODULE( MODULES::RddfPathGen, "RDDF Path Generation Module.", 0)
{
  // do something here if you want
}



corridorstruct corridor_whole;

void RddfPathGen::Active()
{
  // this is the main function which is run.  execution should be trapped
  // somewhere in this funciton during operation.

  
  // INITIALIZATION
  // set up message counter
  int message_counter = 0;

  // read the file into a corridor segment and get the waypoints in northing
  // and easging.  we'll use the nice functions in dgc/RDDF/rddf.hh
  cout << "\nReading RDDF... ";
  RDDF rddf;
  rddf.loadFile(RDDF_FILE);
  RDDFVector waypoints;
  waypoints = rddf.getTargetPoints();
  
  // extract the info from waypoints and store in corridor
  int N = rddf.getNumTargetPoints();   // number of points
  // store data in corridor
  corridor_whole.numPoints = N;
  for(int i = 0; i < N; i++)
    {
      // shouldn't get any segfaults here, but who knows...
      corridor_whole.e.push_back(waypoints[i].Easting);
      corridor_whole.n.push_back(waypoints[i].Northing);
      //shrink the corridor width to account for vehicle width, tracking error
      corridor_whole.width.push_back(waypoints[i].radius-VEHWIDTH);
      corridor_whole.speedLimit.push_back(waypoints[i].maxSpeed);

      //###
      //printf("waypoints[ %d ].Easting is %f\n", i, waypoints[i].Easting);
      //printf("waypoints[ %d ].Northing is %f\n", i, waypoints[i].Northing);
      //###
    }
  cout << "done (read " << N << " points)." << endl;
  
  // generate and store whole path (sparse path, so max of 4 path points
  // per corridor point)
  cout << "Generating complete (sparse) path... ";
  pathstruct path_whole_sparse = Path_From_Corridor(corridor_whole);
  path_whole_sparse.currentPoint = 0; // we haven't went anywhere yet, so we're still at the start of the path
  cout << "done." << endl;


  // output the sparse path to a file (in traj format)
  cout << "Writing sparse path to file " << SPARSE_PATH_FILE << " ... ";
  WritePathToFile(path_whole_sparse, SPARSE_PATH_FILE);
  cout << "done." << endl;


  // Because we had issues with integrating RddfPathGen and the path follower last
  // time, we'll generate and output the complete dense path to a file

  cout << "Densifying sparse path and saving to file " << TRAJOUTPUT << " ... ";

  // densify
  pathstruct path_whole_dense = DensifyPath(path_whole_sparse);
  WritePathToFile(path_whole_dense, TRAJOUTPUT);  // TRAJOUTPUT is defined in RddfPathGen.hh
  cout << "done." << endl;


  cout << endl;







  int usleep_time = 1000000/FREQUENCY; // set frequency in RddfPathGen.hh
  cout << "   Num        N        E            First   Cur    Last" << endl;
  
  // wrap the loop in a while ( ContinueInState() ) 
  while( ContinueInState() )
    {
      message_counter++;
      
      // chunk of code from the query mail handler below...should put this
      // in a common function
      vector<double> location(2);
      GetLocation(location);
      
      // generate blank trajectory
      CTraj * ptraj;
      ptraj = new CTraj(3);  // order 3 (by default)
      
      //###
      //cout << "About to call DensifyAndChop..." << endl;
      //###
      pathstruct path_chopped_dense = DensifyAndChop(path_whole_sparse, location,
						     CHOPBEHIND, CHOPAHEAD);
      //pathstruct path_chopped_dense = path_whole_dense;
      
      //###
      //cout << "Finished DensifyAndChop" << endl;
      //###
	  
      // fill the trajectory using helper function from pathLib.hh
      //      StorePath(*ptraj, path_chopped_dense);
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      StorePath(*ptraj, path_whole_dense);
      
      
      
      if (OUTPUT_TO_MODEMAN == 1)
	{
	  Mail msg = NewOutMessage(MyAddress(), MODULES::ModeMan,
				   ModeManMessages::UpdatePath);
	  
	  // this is for mode man
	  int myID = PathInput::RDDF;
	  
	  // this is for mode man
	  msg << myID;
	  
	  msg.QueueTo(ptraj, CTraj::getHeaderSize());
	  msg.QueueTo(ptraj->getNdiffarray(0), ptraj->getNumPoints() * sizeof(double));
	  msg.QueueTo(ptraj->getNdiffarray(1), ptraj->getNumPoints() * sizeof(double));
	  msg.QueueTo(ptraj->getNdiffarray(2), ptraj->getNumPoints() * sizeof(double));
	  msg.QueueTo(ptraj->getEdiffarray(0), ptraj->getNumPoints() * sizeof(double));
	  msg.QueueTo(ptraj->getEdiffarray(1), ptraj->getNumPoints() * sizeof(double));
	  msg.QueueTo(ptraj->getEdiffarray(2), ptraj->getNumPoints() * sizeof(double));
	  	  
	  //###
	  //cout << "Sending message " << message_counter << " to ModeMan...";
	  //###
	  SendMail(msg);
	  //###
	  //cout << "done." << endl;
	  //###
	  
	}
      
      
      delete ptraj;

      //###
      WritePathToFile(path_chopped_dense, DEBUGOUTPUT);
      //###
      printf(" %4d   %8.1f   %8.1f     %6d  %6d  %6d", message_counter, 
	     location[0], location[1], 0, path_chopped_dense.currentPoint,
	     path_chopped_dense.numPoints-1);
      cout << endl;
      
      usleep(usleep_time);
    }
}





void RddfPathGen::InMailHandler(Mail & msg)
{
  // first, figure out what type of message we received
  switch ( msg.MsgType() )
    {
      // There are no valid one-way messages for RddfPathGen, so just fall
      // through to the default case
    default:
      // let the parent class handle the message
      DGC_MODULE::InMailHandler(msg);
    }
}





Mail RddfPathGen::QueryMailHandler(Mail & msg)
{
  Mail reply = ReplyToQuery(msg); // get a return message ready
  
  // first, figure out what type of message we received
  switch ( msg.MsgType() )
    {
    case RddfPathGen_Messages::GetWholePath: 
      // extract data from message (currently no data)
      // msg >> data;

      // generate blank trajectory
      CTraj * ptraj;
      ptraj = new CTraj(3);  // order 3 (by default) (points, their first and second derivatives)

      // fill the trajectory using helper function from pathLib.hh
      StoreWholePath(*ptraj, corridor_whole);

      // attach path to reply (do we want to attach anything else??)
      // Fill and send this message to the PathFollower (got from
      // ladarplanner or something...)
      reply.QueueTo(ptraj, CTraj::getHeaderSize());
      reply.QueueTo(ptraj->getNdiffarray(0), ptraj->getNumPoints() * sizeof(double));
      reply.QueueTo(ptraj->getNdiffarray(1), ptraj->getNumPoints() * sizeof(double));
      reply.QueueTo(ptraj->getNdiffarray(2), ptraj->getNumPoints() * sizeof(double));
      reply.QueueTo(ptraj->getEdiffarray(0), ptraj->getNumPoints() * sizeof(double));
      reply.QueueTo(ptraj->getEdiffarray(1), ptraj->getNumPoints() * sizeof(double));
      reply.QueueTo(ptraj->getEdiffarray(2), ptraj->getNumPoints() * sizeof(double));

      delete ptraj;

      // break this case
      break;

    case RddfPathGen_Messages::GetPathFragment:
      // extract data from message (currently no data)
      // msg >> data;

      // generate blank trajectory
      //      CTraj traj(3);  // order 3 (points, their first and second derivatives)
      
      // fill the trajectory using helper function from pathLib.hh
      //StorePathFragment(traj);

      // attach path to reply (do we want to attach anything else??)
      reply << "nothing here yet";

      // break this case
      break;

    default:
      // let the parent class handle the message
      reply = DGC_MODULE::QueryMailHandler(msg);
    }
  
  return reply;
}

void RddfPathGen::UpdateState()
{
  Mail msg = NewQueryMessage(MyAddress(), MODULES::VState,VStateMessages::GetState);
  Mail reply = SendQuery(msg);

  if(reply.OK()) {
    reply >> SS;
    cout << "Easting: " << SS.Easting << "  Northing: " << SS.Northing << endl;
  }
  else{
    cout << "UpdateState failed (bad mail message)" << endl;
  }
}





void RddfPathGen::GetLocation(vector<double> & location)
{
  Mail msg = NewQueryMessage(MyAddress(), MODULES::VState,VStateMessages::GetState);
  Mail reply = SendQuery(msg);
  
  if(reply.OK()) {
    reply >> SS;
    location[0] = SS.Northing;
    location[1] = SS.Easting;
  }
  else{
    //###
    //cout << "UpdateState failed (bad mail message), returning location = (0,0)" << endl;
    //###
    location[0] = location[1] = 0;
  }
}




int main(int argc, char **argv)
{
  
  Register(shared_ptr<DGC_MODULE>(new RddfPathGen));
  StartKernel();
  return 0;
}
