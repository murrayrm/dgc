// Vector Operator Library
//
// Changes:
//   12-28-2004, Jason Yosinski, created
//
// Function:
//   defines +, -, *, /, vecmag, vecdot, array2vector,
//   and vecprint for the vector<double> class.
//
// Usage:
//   put #include <vectorOps.hh> and #include <vector.h> in the
//   include section of your .cc file
//

#include <vector>

// magnitude of vector
double vecmag(const vector<double> & vec);

// (magnitude of vector) ^ 2
double vecmagsquared(const vector<double> & vec);

// dot product of two vectors
double vecdot(const vector<double> & vec1, const vector<double> & vec2);

// store array2vector
vector<double> array2vector(int size, double* array);

// print vector
void vecprint(vector<double> vec);

// + operator
vector<double> operator + (const vector<double> & lhs, const vector<double> & rhs);
vector<double> operator + (const vector<double> & lhs, const double & rhs);
vector<double> operator + (const double & lhs, const vector<double> & rhs);

// - operator
vector<double> operator - (const vector<double> & lhs, const vector<double> & rhs);
vector<double> operator - (const vector<double> & lhs, const double & rhs);
vector<double> operator - (const double & lhs, const vector<double> & rhs);

// * operator
vector<double> operator * (const vector<double> & lhs, const vector<double> & rhs);
vector<double> operator * (const vector<double> & lhs, const double & rhs);
vector<double> operator * (const double & lhs, const vector<double> & rhs);

// / operator
vector<double> operator / (const vector<double> & lhs, const vector<double> & rhs);
vector<double> operator / (const vector<double> & lhs, const double & rhs);
vector<double> operator / (const double & lhs, const vector<double> & rhs);
