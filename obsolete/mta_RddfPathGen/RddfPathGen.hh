#include "MTA/Kernel.hh"
#include "../../bob/vehlib/VState.hh"

#ifndef RDDFPATHGEN_HH
#define RDDFPATHGEN_HH

#define OUTPUT_TO_MODEMAN 1
#define OUTPUT_TO_PID_PATHFOLLOWER 0
#define DENSITY .5  // meters between consecutive points in a dense path
#define VEHWIDTH 2 // this is half the width of the vehicle plus a safety factor
#define CHOPBEHIND 5 // when we chop the path, this is how far back along the path
                     // from our current location we go
#define CHOPAHEAD 100 // when we chop the path, this is how far forward along the
                      // path we go from our current location
#define RDDF_FILE "rddf.dat"

// to change which file RddfPathGen outputs the complete traj to, edit the line below
#define TRAJOUTPUT "./rddf.traj"
#define SPARSE_PATH_FILE "sparse_path.traj"
#define DEBUGOUTPUT "rddfdebug"
#define FREQUENCY 1 // frequency at which RddfPathGen sends paths to ModeMan

class RddfPathGen : public DGC_MODULE
{
public:
  RddfPathGen();
  
  void Active();
  void InMailHandler(Mail & ml);
  Mail QueryMailHandler(Mail & ml);

private:

  void UpdateState();
  void GetLocation(vector<double> & location);

  VState_GetStateMsg SS; //stores current state
 
};

namespace RddfPathGen_Messages
{
  enum
    {
      // Query messages
      // get the entire path
      GetWholePath,

      // get a part of the path
      GetPathFragment,
      
      // One Way Messages
      // -but there aren't any one way messages
      
      
      Num // A place holder
    };  
};


#endif  // RDDFPATHGEN_HH
