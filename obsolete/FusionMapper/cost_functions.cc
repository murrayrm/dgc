#include "cost_functions.hh"


int updateCostElevRDDF(CMap* inputMap, int elevLayerID, int rddfLayerID, int costLayerID, double binaryThreshold, double cellNorthing, double cellEasting) {
  int updatedCellRow;
  int updatedCellCol;

  inputMap->UTM2Win(cellNorthing, cellEasting, &updatedCellRow, &updatedCellCol);

  double maxGrad;
  double newCost;
  int costValid;

  if(updatedCellCol >=0 && updatedCellCol < inputMap->getNumCols()) {
    for(int row = updatedCellRow-1; row <= updatedCellRow+1; row++) {
      if(row >= 0 && row < inputMap->getNumRows()) {
	costValid = getMaxCellGradient(inputMap, elevLayerID, maxGrad, row, updatedCellCol);
	if(costValid == VALID_DIFF) { 
	  if(maxGrad/inputMap->getResRows() > binaryThreshold) {
	    newCost = 1;
	  } else {
	    newCost = 0 | inputMap->getDataWin<int>(rddfLayerID, row, updatedCellCol);
	  }
	  inputMap->setDataWin<double>(costLayerID, row, updatedCellCol, newCost);
	} else {
	  newCost = inputMap->getDataWin<int>(rddfLayerID, row, updatedCellCol);
	  inputMap->setDataWin<double>(costLayerID, row, updatedCellCol, newCost);
	}
      }
    }
  }

  if(updatedCellRow >= 0 && updatedCellRow < inputMap->getNumRows()) {
    for(int col = updatedCellCol-1; col <= updatedCellCol+1; col++) {
      if(col >= 0 && col < inputMap->getNumCols()) {
	costValid = getMaxCellGradient(inputMap, elevLayerID, maxGrad, updatedCellRow, col);
	if(costValid == VALID_DIFF) {
	  if(maxGrad/inputMap->getResCols() > binaryThreshold) {
	    newCost = 1;
	  } else {
	    newCost = 0 | inputMap->getDataWin<int>(rddfLayerID, updatedCellRow, col);
	  }
	  inputMap->setDataWin<double>(costLayerID, updatedCellRow, col, newCost);
	} else {
	  newCost = inputMap->getDataWin<int>(rddfLayerID, updatedCellRow, col);
	  inputMap->setDataWin<double>(costLayerID, updatedCellRow, col, newCost);
	}
      }
    }
  }


  return 0;
}


int updateCost(CMap* inputMap, int elevLayerID, int costLayerID, double binaryThreshold, double cellNorthing, double cellEasting) {
  int updatedCellRow;
  int updatedCellCol;

  inputMap->UTM2Win(cellNorthing, cellEasting, &updatedCellRow, &updatedCellCol);

  double maxGrad;
  double newCost;
  int costValid;

  for(int row = updatedCellRow-1; row <= updatedCellRow+1; row++) {
    if(row >= 0 && row < inputMap->getNumRows()) {
      costValid = getMaxCellGradient(inputMap, elevLayerID, maxGrad, row, updatedCellCol);
      if(costValid) {
	if(maxGrad/inputMap->getResRows() > binaryThreshold) {
	  newCost = 1;
	} else {
	  newCost = 0;
	}
	inputMap->setDataWin<double>(costLayerID, row, updatedCellCol, newCost);
      }
    }
  }

  for(int col = updatedCellCol-1; col <= updatedCellCol+1; col++) {
    if(col >= 0 && col < inputMap->getNumCols()) {
      costValid = getMaxCellGradient(inputMap, elevLayerID, maxGrad, updatedCellRow, col);
      if(costValid) {
	if(maxGrad/inputMap->getResCols() > binaryThreshold) {
	  newCost = 1;
	} else {
	  newCost = 0;
	}
     	inputMap->setDataWin<double>(costLayerID, updatedCellRow, col, newCost);
      }
    }
  }



  return 0;
}


int getMaxCellGradient(CMap* inputMap, int elevLayerID, double& maxGrad, int row, int col) {
  double diffNorth=0, diffSouth=0, diffEast=0, diffWest=0;
  double currentCellElev = inputMap->getDataWin<double>(elevLayerID, row, col);
  double northVal, southVal, eastVal, westVal;
  double noDataVal = inputMap->getLayerNoDataVal<double>(elevLayerID);
  double outsideMapVal = inputMap->getLayerOutsideMapVal<double>(elevLayerID);
  double maxVal;
  int useNorth=0, useSouth=0, useEast=0, useWest=0;

  if(currentCellElev == noDataVal || currentCellElev==outsideMapVal) {
    return INVALID_DIFF;
  }

  northVal = inputMap->getDataWin<double>(elevLayerID, row+1, col);
  southVal = inputMap->getDataWin<double>(elevLayerID, row-1, col);
  eastVal = inputMap->getDataWin<double>(elevLayerID, row, col-1);
  westVal = inputMap->getDataWin<double>(elevLayerID, row, col+1);

  if(northVal != noDataVal && northVal != outsideMapVal) {
    diffNorth = fabs(northVal - currentCellElev);
    useNorth = 1;
  }

  if(southVal != noDataVal && southVal != outsideMapVal) {
    diffSouth = fabs(southVal - currentCellElev);
    useSouth = 1;
  }

  if(westVal != noDataVal && westVal != outsideMapVal) {
    diffWest = fabs(westVal - currentCellElev);
    useWest = 1;
  }

  if(eastVal != noDataVal && eastVal != outsideMapVal) {
    diffEast = fabs(eastVal - currentCellElev);
    useEast = 1;
  }

  double maxNorthSouth, maxEastWest;

  maxNorthSouth = fmax(diffNorth, diffSouth);
  maxEastWest = fmax(diffEast, diffWest);
  maxVal = fmax(maxNorthSouth, maxEastWest);

  if(useNorth || useSouth || useEast || useWest) {
    maxGrad = maxVal;
    return VALID_DIFF;    
  }

  return INVALID_DIFF;
}
