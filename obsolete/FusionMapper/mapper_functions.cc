/*  mapper_functions.cc implements structs and functions for 
 *  interfacing with FusionMapper.
 * 
 * This file implements several support functions for the 
 * update of map cells and interfacing with FusionMapper.
 *
 * Changes:
 *   2004/07/19  Lars Cremean, created.
 */

#include "mapper_functions.hh"
#include "Constants.h" // for sensor pose

/* Modeled after MATLAB mvnpdf function - Multivariate normal pdf */
double mvnpdf( XYZcoord point, XYZcoord mean, XYZmatrix cov )
{
  ColumnVector CVpoint(3), CVmean(3);
  CVpoint << point.X << point.Y << point.Z;
  CVmean << mean.X << mean.Y << mean.Z;

  Matrix Mcov(3,3);
  Mcov << cov.xx << cov.xy << cov.xz
       << cov.yx << cov.yy << cov.yz
       << cov.zx << cov.zy << cov.zz;

  Matrix Minv(3,3);
  Minv = Mcov.i();
  
  Real det = Mcov.Determinant();

  Real tmp = DotProduct((CVpoint - CVmean), Minv * (CVpoint - CVmean));
  Real mvnpdf = 1.0/(pow(2.0*M_PI, 1.5)*sqrt(det)) * exp(-0.5*tmp);

//--------------------------------------------------
//   /* DEBUG */
//   printf("point = [ %f %f %f ]'\n", point.X, point.Y, point.Z );
//   printf(" mean = [ %f %f %f ]'\n", mean.X, mean.Y, mean.Z );
//   
//   printf("  cov = [ %f %f %f ;\n", cov.xx, cov.xy, cov.xz );
//   printf("          %f %f %f ;\n", cov.yx, cov.yy, cov.yz );
//   printf("          %f %f %f ]\n", cov.zx, cov.zy, cov.zz );
// 
//   printf("  inv = [ %f %f %f ;\n", Minv(1,1), Minv(1,2), Minv(1,3) );
//   printf("          %f %f %f ;\n", Minv(2,1), Minv(2,2), Minv(2,3) );
//   printf("          %f %f %f ]\n", Minv(3,1), Minv(3,2), Minv(3,3) );
// 
//   printf("  det =  %f \n", det );
//   printf("  tmp = (point - mean)' * inv(cov) * (point - mean) = %f \n", tmp );
//   printf("pow(2.0*M_PI, 1.5)*sqrt(det) = %f\n", pow(2.0*M_PI, 1.5)*sqrt(det));
//   printf("exp(-0.5*tmp) = %f\n", exp(-0.5*tmp));
//   printf("mvnpdf = 1.0/(pow(2.0*M_PI, 1.5)*sqrt(det)) * exp(-0.5*tmp))\n");
//   printf("mvnpdf = %f\n", 1.0/(pow(2.0*M_PI, 1.5)*sqrt(det)) * exp(-0.5*tmp));
//   sleep(10);
//-------------------------------------------------- 
 
  return mvnpdf;
}


XYZmatrix calcCovMatrix(VehPoseMeas vehmeas, SphericalMeas sensormeas)
{
  // declare the return Matrix
  XYZmatrix cov;
  
  // set up some convenient variables.
//--------------------------------------------------
//   /* Unneeded */
//   double xv = vehmeas.Northing; /* x-location of the vehicle */
//   double yv = vehmeas.Easting;  /* y-location of the vehicle */
//   double zv = vehmeas.Downing;  /* z-location of the vehicle */
//-------------------------------------------------- 
  double pv = vehmeas.Pitch;    /* pitch of the vehicle */
  double rv = vehmeas.Roll;     /* roll of the vehicle */
  double hv = vehmeas.Yaw;      /* yaw of the vehicle */

  // vehicle variances 
  double vxv = vehmeas.Northing_var; /* vehicle x-location variance */
  double vyv = vehmeas.Easting_var;  /* vehicle y-location variance */
  double vzv = vehmeas.Downing_var;  /* vehicle z-location variance */
  double vpv = vehmeas.Pitch_var;    /* vehicle pitch variance */
  double vrv = vehmeas.Roll_var;     /* vehicle roll variance */
  double vhv = vehmeas.Yaw_var;      /* vehicle yaw variance */

  // sensor pose (in vehicle frame)
  // NOTE: The roll and yaw of the sensor are assumed to be zero 
  // in this analysis.
  // TODO: 1. Point this to the right #defines for the roof ladar.
  //       2. Make this generic for any sensor.
  double xs = ROOF_LADAR_OFFSET_X;
  double ys = ROOF_LADAR_OFFSET_Y;
  double zs = ROOF_LADAR_OFFSET_Z;
  double ps = ROOF_LADAR_PITCH;

  // sensor measurement (in sensor frame)
  // NOTE: phi is assumed to be zero in the equations below and is not used!
  double rho   = sensormeas.range;     /* measurement range in sensor frame */
  double theta = sensormeas.azimuth;   /* meas. azimuth in sensor frame */
//--------------------------------------------------
//   /* Unneeded, assumed zero */
//   double phi   = sensormeas.elevation; /* meas. elevation in sensor frame */
//-------------------------------------------------- 

  // sensor variances
  double vrho   = sensormeas.range_var;     /* measurement range variance */
  double vtheta = sensormeas.azimuth_var;   /* azimuth range variance */
//--------------------------------------------------
//   /* Unneeded, assumed zero */
//   double vphi   = sensormeas.elevation_var; /* elevation range variance */
//-------------------------------------------------- 

  // only calculate the trig once.

  // vehicle angles (relative to global frame)
  double cpv = cos(pv); /* cosine of pitch of vehicle */
  double spv = sin(pv); /* sine of pitch of vehicle */
  double crv = cos(rv); /* cosine of roll of vehicle */
  double srv = sin(rv); /* sine of roll of vehicle */
  double chv = cos(hv); /* cosine of yaw (heading) of vehicle */
  double shv = sin(hv); /* sine of yaw (heading) of vehicle */
  
  // sensor angles (relative to vehicle frame)
  double cps = cos(ps); /* cos of pitch of sensor */
  double sps = sin(ps); /* sin of pitch of sensor */
  
  // measurement angles (relative to sensor frame)
  double cth  = cos(theta);  /* cos of measurement azimuth (yaw) */
  double sth  = sin(theta);  /* sin of measurement azimuth (yaw) */
//--------------------------------------------------
//   /* Unneeded, phi assumed zero */
//   double cphi = cos(phi);    /* cos of measurement elevation (pitch) */
//   double sphi = sin(phi);    /* sin of measurement elevation (pitch) */
//-------------------------------------------------- 
  
  // The derivation of these equations was done in Mathematica, and 
  // are available from Lars or Kristo.
  double c11 =  vxv + pow(rho,2)*vtheta*pow(shv*sps*srv*sth + 
    chv*(cth*spv*srv - cps*cpv*sth) + 
    crv*(-(cth*shv) + chv*sps*spv*sth),2) + 
    vrv*pow(srv*(chv*(-zs + rho*cth*sps)*spv + shv*(ys + rho*sth)) + 
    crv*(shv*(zs - rho*cth*sps) + chv*spv*(ys + rho*sth)),2) + 
    vpv*pow(chv,2)*pow((xs + rho*cps*cth)*spv - 
    cpv*(crv*(zs - rho*cth*sps) + srv*(ys + rho*sth)),2) + 
    vrho*pow(shv*(cth*sps*srv + crv*sth) - 
    chv*(cps*cpv*cth + spv*(-(crv*cth*sps) + srv*sth)),2) + 
    vhv*pow(chv*((-zs + rho*cth*sps)*srv + crv*(ys + rho*sth)) + 
    shv*(cpv*(xs + rho*cps*cth) + 
    spv*(crv*(zs - rho*cth*sps) + srv*(ys + rho*sth))),2);

  double c22 = vyv + vrho*pow(cps*cpv*cth*shv + crv*(-(cth*shv*sps*spv) + 
    chv*sth) + srv*(chv*cth*sps + shv*spv*sth),2) + 
    pow(rho,2)*vtheta*pow(shv*(cth*spv*srv + (-(cps*cpv) + crv*sps*spv)* 
    sth) + chv*(crv*cth - sps*srv*sth),2) + 
    vrv*pow(shv*spv*((-zs + rho*cth*sps)*srv + crv*(ys + rho*sth)) - 
    chv*(crv*(zs - rho*cth*sps) + srv*(ys + rho*sth)),2) + 
    vpv*pow(shv,2)*pow((xs + rho*cps*cth)*spv - 
    cpv*(crv*(zs - rho*cth*sps) + srv*(ys + rho*sth)),2) + 
    vhv*pow(shv*((-zs + rho*cth*sps)*srv + crv*(ys + rho*sth)) - 
    chv*(cpv*(xs + rho*cps*cth) + 
    spv*(crv*(zs - rho*cth*sps) + srv*(ys + rho*sth))),2);

  double c33 = vzv + vrv*pow(cpv,2)*pow((-zs + rho*cth*sps)*srv + 
    crv*(ys + rho*sth),2) + pow(rho,2)*vtheta*pow(cps*spv*sth + cpv*(cth*srv + 
    crv*sps*sth),2) + vrho*pow(cps*cth*spv + cpv*(crv*cth*sps - srv*sth),2) + 
    vpv*pow(cpv*(xs + rho*cps*cth) + spv*(crv*(zs - rho*cth*sps) + 
    srv*(ys + rho*sth)),2);
 
  double c21 = (vpv*sin(2*hv)*pow((xs + rho*cps*cth)*spv - 
    cpv*(crv*(zs - rho*cth*sps) + srv*(ys + rho*sth)),2))/2 + 
    pow(rho,2)*vtheta*(pow(chv,2)*(cth*spv*srv + 
    (-(cps*cpv) + crv*sps*spv)*sth)*
    (crv*cth - sps*srv*sth) - 
    pow(shv,2)*(cth*spv*srv + (-(cps*cpv) + crv*sps*spv)*
    sth)*(crv*cth - sps*srv*sth) + 
    chv*shv*(pow(cth,2)*pow(spv,2)*pow(srv,2) - 2*cps*cpv*cth*spv*srv*
    sth + (pow(cps,2)*pow(cpv,2) - pow(sps,2)*pow(srv,2))*pow(sth,2) + 
    2*crv*sps*sth*(cth*(1 + pow(spv,2))*srv - 
    cps*cpv*spv*sth) + pow(crv,2)*(-pow(cth,2) + 
    pow(sps,2)*pow(spv,2)*pow(sth,2)))) + 
    vrv*(crv*pow(shv,2)*spv*srv*(pow(ys,2) - pow(zs,2) - 
    pow(rho,2)*pow(cth,2)*pow(sps,2) + 
    pow(rho,2)*pow(sth,2)) + pow(shv,2)*spv*
    (-((zs - rho*cth*sps)*pow(srv,2)*(ys + rho*sth)) + 
    rho*sin(2*rv)*(zs*cth*sps + ys*sth)) - 
    pow(chv,2)*spv*(pow(crv,2)*(zs - rho*cth*sps)*(ys + rho*sth) - 
    (zs - rho*cth*sps)*pow(srv,2)*(ys + rho*sth) + 
    rho*sin(2*rv)*(zs*cth*sps + ys*sth) + 
    crv*srv*(pow(ys,2) - pow(zs,2) - pow(rho,2)*pow(cth,2)*pow(sps,2) + 
    pow(rho,2)*pow(sth,2))) + 
    pow(crv,2)*(spv*(rho*ys*sin(2*hv)*spv*sth + zs*pow(shv,2)*(ys + rho*sth)) - 
    rho*cth*sps*(-(zs*sin(2*hv)) + pow(shv,2)*spv*(ys + rho*sth))) - 
    chv*shv*(rho*(-3 + cos(2*pv))*crv*cth*sps*srv*(ys + rho*sth) - 
    (zs*(-3 + cos(2*pv))*sin(2*rv)*(ys + rho*sth))/2 + 
    pow(srv,2)*(-(pow(zs - rho*cth*sps,2)*pow(spv,2)) + pow(ys + rho*sth,2)) + 
    pow(crv,2)*(pow(zs,2) + pow(rho,2)*pow(cth,2)*pow(sps,2) - 
    pow(spv,2)*(pow(ys,2) + pow(rho,2)*pow(sth,2))))) - 
    vhv*(rho*xs*cps*pow(cpv,2)*cth*sin(2*hv) - 
    ys*zs*pow(crv,2)*pow(shv,2)*spv + 
    rho*ys*pow(crv,2)*cth*pow(shv,2)*sps*spv - 
    pow(ys,2)*crv*pow(shv,2)*spv*srv + 
    pow(zs,2)*crv*pow(shv,2)*spv*srv - 2*rho*zs*crv*cth*pow(shv,2)*sps*spv*
    srv + pow(rho,2)*crv*pow(cth,2)*pow(shv,2)*pow(sps,2)*spv*srv +
    rho*zs*cth*sin(2*hv)*sps*pow(srv,2) + ys*zs*pow(shv,2)*spv*pow(srv,2) - 
    rho*ys*cth*pow(shv,2)*sps*spv*pow(srv,2) - rho*zs*pow(crv,2)*pow(shv,2)*spv*
    sth + pow(rho,2)*pow(crv,2)*cth*pow(shv,2)*sps*spv*sth - 
    2*rho*ys*crv*pow(shv,2)*spv*srv*sth + rho*zs*pow(shv,2)*spv*pow(srv,2)*
    sth - pow(rho,2)*cth*pow(shv,2)*sps*spv*pow(srv,2)*sth + 
    rho*ys*sin(2*hv)*pow(spv,2)*pow(srv,2)*sth - 
    pow(rho,2)*crv*pow(shv,2)*spv*srv*
    pow(sth,2) - cpv*(xs + rho*cps*cth)*pow(shv,2)*
    ((-zs + rho*cth*sps)*srv + crv*(ys + rho*sth)) + 
    chv*shv*(pow(cpv,2)*(pow(xs,2) + pow(rho,2)*pow(cps,2)*pow(cth,2)) + 
    xs*ys*sin(2*pv)*srv + rho*ys*cps*cth*sin(2*pv)*srv - pow(zs,2)*pow(srv,2) - 
    pow(rho,2)*pow(cth,2)*pow(sps,2)*pow(srv,2) + 
    pow(ys,2)*pow(spv,2)*pow(srv,2) + ys*zs*sin(2*rv) + 
    ys*zs*pow(spv,2)*sin(2*rv) + rho*xs*sin(2*pv)*srv*sth + 
    rho*zs*sin(2*rv)*sth + rho*zs*pow(spv,2)*sin(2*rv)*sth + 
    pow(rho,2)*pow(spv,2)*pow(srv,2)*pow(sth,2) - 2*rho*cpv*cth*spv*
    (crv*(xs + rho*cps*cth)*sps - rho*cps*srv*sth) + 
    crv*(zs*(xs + rho*cps*cth)*sin(2*pv) + rho*(-3 + cos(2*pv))*cth*sps*
    srv*(ys + rho*sth)) - pow(crv,2)*(-(pow(zs - rho*cth*sps,2)*pow(spv,2)) + 
    pow(ys + rho*sth,2))) + pow(chv,2)*(cpv*(xs + rho*cps*cth)*
    ((-zs + rho*cth*sps)*srv + crv*(ys + rho*sth)) + 
    spv*(pow(crv,2)*(zs - rho*cth*sps)*(ys + rho*sth) - 
    (zs - rho*cth*sps)*pow(srv,2)*(ys + rho*sth) + 
    rho*sin(2*rv)*(zs*cth*sps + ys*sth) + 
    crv*srv*(pow(ys,2) - pow(zs,2) - pow(rho,2)*pow(cth,2)*pow(sps,2) + 
    pow(rho,2)*pow(sth,2))))) + vrho*(pow(shv,2)*(cth*sps*srv + crv*sth)*
    (-(cps*cpv*cth) + spv*(crv*cth*sps - srv*sth)) + 
    pow(chv,2)*(cth*sps*srv + crv*sth)*(cps*cpv*cth + 
    spv*(-(crv*cth*sps) + srv*sth)) + 
    chv*shv*(pow(cps,2)*pow(cpv,2)*pow(cth,2) + 2*cps*cpv*cth*spv*
    (-(crv*cth*sps) + srv*sth) + 
    pow(crv,2)*(pow(cth,2)*pow(sps,2)*pow(spv,2) - pow(sth,2)) + 
    pow(srv,2)*(-(pow(cth,2)*pow(sps,2)) + pow(spv,2)*pow(sth,2)) + 
    ((-3 + cos(2*pv))*sps*sin(2*rv)*sin(2*theta))/4));

  double c31 = -(pow(rho,2)*vtheta*(cps*spv*sth + cpv*(cth*srv + 
    crv*sps*sth))*(-(shv*sps*srv*sth) + 
    chv*(-(cth*spv*srv) + cps*cpv*sth) + 
    crv*(cth*shv - chv*sps*spv*sth))) + 
    vrv*cpv*((-zs + rho*cth*sps)*srv + crv*(ys + rho*sth))*
    (srv*(chv*(-zs + rho*cth*sps)*spv + shv*(ys + rho*sth)) + 
    crv*(shv*(zs - rho*cth*sps) + chv*spv*(ys + rho*sth))) + 
    vpv*chv*(-(crv*(xs + rho*cps*cth)*(-zs + rho*cth*sps)*pow(spv,2)) + 
    rho*xs*cps*cth*sin(2*pv) + rho*zs*pow(crv,2)*cth*sps*sin(2*pv) + 
    xs*ys*pow(spv,2)*srv + rho*ys*cps*cth*pow(spv,2)*srv + 
    rho*xs*pow(spv,2)*srv*sth + pow(rho,2)*cps*cth*pow(spv,2)*srv*sth - 
    pow(cpv,2)*(xs + rho*cps*cth)*(crv*(zs - rho*cth*sps) + 
    srv*(ys + rho*sth)) + cpv*spv*(pow(xs,2) + 
    pow(rho,2)*pow(cps,2)*pow(cth,2) - 
    pow(crv,2)*(pow(zs,2) + pow(rho,2)*pow(cth,2)*pow(sps,2)) - 
    pow(ys,2)*pow(srv,2) + 
    rho*ys*cth*sps*sin(2*rv) - 2*rho*ys*pow(srv,2)*sth - 
    pow(rho,2)*pow(srv,2)*pow(sth,2) - 
    2*crv*srv*(ys*zs + rho*(zs - rho*cth*sps)*
    sth))) - vrho*(cps*cth*spv + 
    cpv*(crv*cth*sps - srv*sth))*
    (-(shv*(cth*sps*srv + crv*sth)) + 
    chv*(cps*cpv*cth + spv*(-(crv*cth*sps) + 
    srv*sth)));

  double c32 = -(vrho*(cps*cpv*cth*shv + crv*(-(cth*shv*sps*spv) + 
    chv*sth) + srv*(chv*cth*sps + shv*spv*sth))*
    (cps*cth*spv + cpv*(crv*cth*sps - srv*sth))) + 
    pow(rho,2)*vtheta*(cps*spv*sth + 
    cpv*(cth*srv + crv*sps*sth))*
    (shv*(cth*spv*srv + (-(cps*cpv) + crv*sps*spv)*
    sth) + chv*(crv*cth - sps*srv*sth)) + 
    vrv*cpv*((-zs + rho*cth*sps)*srv + crv*(ys + rho*sth))*
    (shv*spv*((-zs + rho*cth*sps)*srv + crv*(ys + rho*sth)) - 
    chv*(crv*(zs - rho*cth*sps) + srv*(ys + rho*sth))) + 
    vpv*shv*(-(crv*(xs + rho*cps*cth)*(-zs + rho*cth*sps)*pow(spv,2)) + 
    rho*xs*cps*cth*sin(2*pv) + rho*zs*pow(crv,2)*cth*sps*sin(2*pv) + 
    xs*ys*pow(spv,2)*srv + rho*ys*cps*cth*pow(spv,2)*srv + 
    rho*xs*pow(spv,2)*srv*sth + pow(rho,2)*cps*cth*pow(spv,2)*srv*sth - 
    pow(cpv,2)*(xs + rho*cps*cth)*(crv*(zs - rho*cth*sps) + 
    srv*(ys + rho*sth)) + cpv*spv*(pow(xs,2) 
    + pow(rho,2)*pow(cps,2)*pow(cth,2) - 
    pow(crv,2)*(pow(zs,2) + pow(rho,2)*pow(cth,2)*pow(sps,2)) - 
    pow(ys,2)*pow(srv,2) + 
    rho*ys*cth*sps*sin(2*rv) - 2*rho*ys*pow(srv,2)*sth - 
    pow(rho,2)*pow(srv,2)*pow(sth,2) - 
    2*crv*srv*(ys*zs + rho*(zs - rho*cth*sps)* sth)));

    // Now finally set the covariance Matrix object.
    //                 [ c11 c12 c13 ]           [ (0,0) (0,1) (0,2) ]
    // Matlab-matrix = [ c21 c22 c23 ], Matrix = [ (1,0) (1,1) (1,2) ]
    //                 [ c31 c32 c33 ]           [ (2,0) (2,1) (2,2) ]
    
    /* Store the row-packed, lower triangular elements in the matrix. */
    cov.xx = c11; cov.xy = c21; cov.xz = c31;
    cov.yx = c21; cov.yy = c22; cov.yz = c32;
    cov.zx = c31; cov.zy = c32; cov.zz = c33;

#ifdef NOT_DEFINED
    /* Debugging */
    printf("VEH_STATE_EST(N_IDX)    = %f\n", vehmeas.Northing);
    printf("VEH_STATE_EST(E_IDX)    = %f\n", vehmeas.Easting);
    printf("VEH_STATE_EST(D_IDX)    = %f\n", vehmeas.Downing);
    printf("VEH_STATE_EST(PITCHIDX) = %f\n", vehmeas.Pitch);
    printf("VEH_STATE_EST(ROLLIDX)  = %f\n", vehmeas.Roll);
    printf("VEH_STATE_EST(YAWIDX)   = %f\n", vehmeas.Yaw);

    printf("VEH_STATE_EST_VAR(N_IDX)    = %f\n", vehmeas.Northing_var);
    printf("VEH_STATE_EST_VAR(E_IDX)    = %f\n", vehmeas.Easting_var);
    printf("VEH_STATE_EST_VAR(D_IDX)    = %f\n", vehmeas.Downing_var);
    printf("VEH_STATE_EST_VAR(PITCHIDX) = %f\n", vehmeas.Pitch_var);
    printf("VEH_STATE_EST_VAR(ROLLIDX)  = %f\n", vehmeas.Roll_var);
    printf("VEH_STATE_EST_VAR(YAWIDX)   = %f\n", vehmeas.Yaw_var);

    printf("SENSOR_STATE(N_IDX)    = %f\n", ROOF_LADAR_OFFSET_X);
    printf("SENSOR_STATE(E_IDX)    = %f\n", ROOF_LADAR_OFFSET_Y);
    printf("SENSOR_STATE(D_IDX)    = %f\n", ROOF_LADAR_OFFSET_Z);
    printf("SENSOR_STATE(PITCHIDX) = %f\n", ROOF_LADAR_PITCH);

    printf("rho   = %f\n", rho);
    printf("theta = %f\n", theta);
    printf("phi   = %f\n", phi);

    printf("SENSOR_RANGE_VAR      = %f\n", vrho);
    printf("SENSOR_SCAN_ANGLE_VAR = %f\n", vtheta);
    printf("vphi                  = %f\n", vphi);

    printf("cov    = [ %f %f %f ;\n",   cov.xx, cov.xy, cov.xz);
    printf("           %f %f %f ;\n",   cov.yx, cov.yy, cov.yz);
    printf("           %f %f %f ]\n\n", cov.zx, cov.zy, cov.zz);
#endif

    return cov;

}
