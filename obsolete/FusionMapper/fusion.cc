#include "FusionMapper.hh"

#include "Constants.h" // serial ports, other constants
#include <iostream.h>
#include <fstream.h>
#include <unistd.h>

#include "MTA/Misc/Time/Time.hh" /* usleep_for */
#include "mapper_functions.hh"
#include "cost_functions.hh"

extern int  LOG_DATA;
extern int  NOPITCH;
extern int  NOROLL;
extern int  QUIT_PRESSED;
extern int  ACTIVE_LAYER;
extern int  REFRESH_CMAP;
extern char *TESTNAME;

FILE* statelog;
FILE* votelog;

/******************************************************************************/
/******************************************************************************/
#ifdef LEGACY_CODE
void FusionMapper::InitFusionLogs() 
{
//--------------------------------------------------
//   int result;
//   char * port_name;
//   int status;
//   unsigned char data[1024];
//   bool good_to_go = true;
//-------------------------------------------------- 
  char filename[255];
  char datestamp[255];

  // Use the accessor function from the Map class to get these.
  printf("fusionMap num rows: %d\n", d.fusionMap.getNumRows() ); 
  printf("fusionMap num cols: %d\n", d.fusionMap.getNumCols() ); 

  // Open log file if LOG_DATA is set
  if( LOG_DATA ) 
  {
    // filename will be TESTNAME_ddmmyyyy_hhmmss.log
    time_t t = time(NULL);
    tm *local;
    local = localtime(&t);
    int i;

    sprintf(datestamp, "%02d%02d%04d_%02d%02d%02d",
            local->tm_mon+1, local->tm_mday, local->tm_year+1900,
            local->tm_hour, local->tm_min, local->tm_sec);

    sprintf(filename, "testRuns/%s_state_%s.log", TESTNAME, datestamp);
    printf("Logging state to file: %s\n", filename);
    statelog=fopen(filename,"w");
    if ( statelog == NULL ) 
    {
      printf("Unable to open state log!!!\n");
      exit(-1);
    }
    // print data format information in a header at the top of the state file
    fprintf(statelog, "%% Time[sec] | Easting[m] | Northing[m] | Altitude[m]");
    fprintf(statelog, " | Vel_E[m/s] | Vel_N[m/s] | Vel_U[m/s]" );
    fprintf(statelog, " | Speed[m/s] | Accel[m/s/s]" );
    fprintf(statelog, " | Pitch[rad] | Roll[rad] | Yaw[rad]" );
    fprintf(statelog, " | PitchRate[rad/s] | RollRate[rad/s] | YawRate[rad/s]");
    fprintf(statelog, "\n");

  } // end if( LOG_DATA )

} // end FusionMapper::InitFusion()


/******************************************************************************/
/******************************************************************************/
void FusionMapper::UpdateFusionLogs() 
{
//--------------------------------------------------
//   int i;
//   double phi;
//   Timeval tv = TVNow();
//-------------------------------------------------- 
  double tstamp;

 
  // Log the vehicle state
  if (LOG_DATA) 
  {
    tstamp = d.SS.Timestamp.dbl();
  
    fprintf(statelog, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",
	    tstamp, d.SS.Easting, d.SS.Northing, d.SS.Altitude,
	    d.SS.Vel_E, d.SS.Vel_N, d.SS.Vel_U,
	    d.SS.Speed, d.SS.Accel, 
	    d.SS.Pitch, d.SS.Roll, d.SS.Yaw, 
	    d.SS.PitchRate, d.SS.RollRate, d.SS.YawRate );
    fflush(statelog);
  }

//--------------------------------------------------
//   Timeval tv_end = TVNow();
//-------------------------------------------------- 
  // as of thursday this takes 420 ms!!!
  //  fprintf(stderr, "%lf seconds elapsed\n", (tv_end.dbl() - tv.dbl()));

} // end UpdateFusionLogs()
#endif //LEGACY_CODE

/******************************************************************************/
/******************************************************************************/
void FusionMapper::UpdateState()
{ 
  //--------------------------------------------------
  //   /* DEBUG */
  //   Timeval then;
  //   then = TVNow();
  //--------------------------------------------------

  // These two lines take roughly about 400 microseconds.
  Mail msg = NewQueryMessage( MyAddress(), MODULES::VState,
                              VStateMessages::GetState);
  Mail reply = SendQuery(msg);

  // This takes about 2-3 microseconds.
  if (reply.OK() ) {
    reply >> d.SS; // download the new state
  }

  //--------------------------------------------------
  //   /* DEBUG */
  //   // Determine the time disparity between processes.
  //   // compare the timestamp we just received across the
  //   // MTA to the local current time
  //   printf("TVNow() - d.SS.Timestamp = %f sec\n",
  //          (TVNow() - d.SS.Timestamp).dbl() );
  //--------------------------------------------------
 
} // end UpdateState() 

// end fusion.cc
