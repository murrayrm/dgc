#include "FusionMapper.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

extern int LOG_DATA;
extern int HALT_VEHICLE;
extern int QUIT_PRESSED;
extern unsigned long SLEEP_TIME;

extern int REFRESH_CMAP;
extern int SAVE_CMAP;
extern int CLEAR_CMAP;
extern int ACTIVE_LAYER;

int TimeSec     = 0;
int TimeUSec    = 0;
double TimeDiff = 0.0;

int SparrowUpdateCount  = 0;
int ladarMessageCount   = 0;
int ladarMeasCount      = 0;
int stereoMessageCount  = 0;
int stereoMeasCount     = 0;
int sentVotesCt         = 0;
int ddBadCovCount       = 0;
int ddOKCellUpdateCount = 0;
int ddTooSmallPDFCount  = 0;

double state_pitch,state_yaw,state_roll;
double state_e,state_n,state_a;
double state_speed;

char* currLayer;

int user_quit(long arg);
int save_CMapPlus(long arg);
int clear_CMapPlus(long arg);
int refresh_CMapPlus(long arg);
int set_nextActiveLayer(long arg);

#include "vddtable.h"

void FusionMapper::UpdateSparrowVariablesLoop() 
{
  TimeSec = d.SS.Timestamp.sec();
  TimeUSec = d.SS.Timestamp.usec();
  // POTENTIAL LEGACY CODE TimeDiff = (d.localTimestamp - d.SS.Timestamp).dbl();
  
  state_pitch = (d.SS.Pitch / (2 * M_PI)) * 360.0;
  state_yaw = (d.SS.Yaw / (2 * M_PI)) * 360.0;
  state_roll = (d.SS.Roll / (2 * M_PI)) * 360.0;
  state_n = d.SS.Northing;
  state_e = d.SS.Easting;
  state_a = d.SS.Altitude;
  state_speed = d.SS.Speed;

  SparrowUpdateCount++;
  /* POTENTIAL LEGACY CODE
  ladarMessageCount   = d.ladarMessageCount;
  ladarMeasCount      = d.ladarMeasCount;
  stereoMessageCount  = d.stereoMessageCount;
  stereoMeasCount     = d.stereoMeasCount;
  sentVotesCt         = d.sentVotesCount;
  ddBadCovCount       = BadCovCount;
  ddOKCellUpdateCount = OKCellUpdateCount;
  ddTooSmallPDFCount  = TooSmallPDFCount;
  END POTENTIAL LEGACY CODE */
}

void FusionMapper::SparrowDisplayLoop() 
{
  dbg_all = 0;

  if (dd_open() < 0) exit(1);
  dd_bindkey('q', user_quit);
  dd_bindkey('n', set_nextActiveLayer);
  dd_bindkey('s', save_CMapPlus);
  dd_bindkey('c', clear_CMapPlus);
  dd_bindkey('r', refresh_CMapPlus);

  dd_usetbl(vddtable);

  cout << "Sparrow display should show up in 1 sec" << endl;
  sleep(1); // Wait a bit, because other threads will print some stuff out
  dd_loop();
  dd_close();
  QUIT_PRESSED = 1;
}

// TODO: Set the shutdown flag in the quit function
int user_quit(long arg)
{
  QUIT_PRESSED = 1;
  return DD_EXIT_LOOP;
}

int set_nextActiveLayer(long arg)
{
  ACTIVE_LAYER = (ACTIVE_LAYER + 1) % 5;
  currLayer = MapLayerStrings[ACTIVE_LAYER];
  printf("Changed currLayer, now = %s\n", currLayer);
  return 0;
}

int refresh_CMapPlus(long arg)
{
  REFRESH_CMAP = true;
  return 0;
}

int save_CMapPlus(long arg)
{
  SAVE_CMAP = true;
  return 0;
}

int clear_CMapPlus(long arg)
{
  CLEAR_CMAP = true;
  return 0;
}
