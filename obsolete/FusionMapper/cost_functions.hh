#ifndef COST_FUNCTIONS_HH
#define COST_FUNCTIONS_HH

#include "CMap/CMapPlus.hh"

#define VALID_DIFF 1
#define INVALID_DIFF 0

int updateCostElevRDDF(CMap* inputMap, int elevLayerID, int rddfLayerID, int costLayerID, double binaryThreshold, double cellNorthing, double cellEasting);

int updateCost(CMap* inputMap, int elevLayerID, int costLayerID, double binaryThreshold, double cellNorthing, double cellEasting);


int getMaxCellGradient(CMap* inputMap, int elevLayerID, double& maxGrad, int row, int col);


#endif //COST_FUNCTIONS_HH
