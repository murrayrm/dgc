/** \brief mapper_functions.hh - defines structs and functions for interfacing 
 *  with FusionMapper.
 * 
 * This file defines the format for an XYZmeas struct that defines the input 
 * type for FusionMapper.  It also provides several support functions for the 
 * update of map cells.
 *
 * Changes:
 *   2004/07/19  Lars Cremean, created.
 */
 
#ifndef MAPPER_FUNCTIONS_HH
#define MAPPER_FUNCTIONS_HH

#define DEFAULT_FUSIONMAP_NUM_ROWS 200
#define DEFAULT_FUSIONMAP_NUM_COLS 200
#define DEFAULT_FUSIONMAP_ROW_RES  0.5
#define DEFAULT_FUSIONMAP_COL_RES  0.5
#define DEFAULT_FUSIONMAP_VARIANCE 100.0
#define FM_DOUBLE_NO_DATA          1.0 //CM_DOUBLE_NO_DATA

// TODO: Include to installed location. 
#include "../frames/rot_matrix.hh" // for XYZCoord struct

#include "Newmat/newmat.h" // for Newmat Matrix class

#include "MTA/Misc/Mail/Mail.hh" // for MTA messaging

/* Include the Map class. */
#include "CMap/CMapPlus.hh"

/*! Struct to store 3x3 matrices; companion to XYZcoord.  Uses Newmat. */
struct XYZmatrix
{
  Real xx, xy, xz, 
       yx, yy, yz, 
       zx, zy, zz;

  /*! Constructors. */
  XYZmatrix() {}

  /*! Fill elements directly */
  XYZmatrix(double xx_new, double xy_new, double xz_new,
            double yx_new, double yy_new, double yz_new,
            double zx_new, double zy_new, double zz_new) : 
            xx(xx_new), xy(xy_new), xz(xz_new),
            yx(xx_new), yy(xy_new), yz(xz_new),
            zx(xx_new), zy(xy_new), zz(xz_new) {}

  /*! Fill with a Newmat matrix */
  XYZmatrix( Matrix Newmatrix ) : 
            xx(Newmatrix(1,1)), xy(Newmatrix(1,2)), xz(Newmatrix(1,3)),
            yx(Newmatrix(2,1)), yy(Newmatrix(2,2)), yz(Newmatrix(2,3)),
            zx(Newmatrix(3,1)), zy(Newmatrix(3,2)), zz(Newmatrix(3,3)) {}

  /*! Copy Constructor. */
  XYZmatrix(const XYZmatrix &other) 
  {
    xx = other.xx; xy = other.xy; xz = other.xz;
    yx = other.yx; yy = other.yy; yz = other.yz;
    zx = other.zx; zy = other.zy; zz = other.zz;
  }

  /*! Accessor to return the Newmat matrix type. */
  Matrix getCovMatrix()
  {
    Matrix mat(3,3);
    Real a[] = {xx, xy, xz, 
                yx, yy, yz,
                zx, zy, zz};
    mat << a;
    return mat;
  }

  /*! Method to print XYZmatrix */
  void printXYZmatrix()
  {
    printf("XYZmatrix = [ %.3f %.3f %.3f ;\n",   xx, xy, xz);
    printf("              %.3f %.3f %.3f ;\n",   yx, yy, yz);
    printf("              %.3f %.3f %.3f ]\n\n", zx, zy, zz);
  }

};


/*! This struct defines a 3D point and 3D covariance matrix. */
struct XYZmeas
{
  /*! The mean/center value of the measurement; Northing/Easting/Down. */
  XYZcoord mean; 
  /*! The 3x3 covariance matrix associated with the measurement. */
  XYZmatrix cov;

  // Default constructor
  XYZmeas() {}
  // Constructor given a Newmat ColumnVector and a Newmat Matrix 
  XYZmeas( ColumnVector coord, Matrix covar ) 
  {
    mean.X = coord(1); mean.Y = coord(2); mean.Z = coord(3);
    cov.xx = covar(1,1); cov.xy = covar(1,2); cov.xz = covar(1,3);
    cov.yx = covar(2,1); cov.yy = covar(2,2); cov.yz = covar(2,3);
    cov.zx = covar(3,1); cov.zy = covar(3,2); cov.zz = covar(3,3);
  }
  // Constructor given an XYZcoord and an XYZmatrix
  XYZmeas( XYZcoord coord, XYZmatrix covar ) 
  {
    mean.X = coord.X; mean.Y = coord.Y; mean.Z = coord.Z;
    cov.xx = covar.xx; cov.xy = covar.xy; cov.xz = covar.xz;
    cov.yx = covar.yx; cov.yy = covar.yy; cov.yz = covar.yz;
    cov.zx = covar.zx; cov.zy = covar.zy; cov.zz = covar.zz;
  }
  // Constructor given a Newmat ColumnVector and an XYZmatrix
  XYZmeas( ColumnVector coord, XYZmatrix covar ) 
  {
    mean.X = coord(1); mean.Y = coord(2); mean.Z = coord(3);
    cov.xx = covar.xx; cov.xy = covar.xy; cov.xz = covar.xz;
    cov.yx = covar.yx; cov.yy = covar.yy; cov.yz = covar.yz;
    cov.zx = covar.zx; cov.zy = covar.zy; cov.zz = covar.zz;
  }
  // Constructor given an XYZcoord and a Newmat matrix
  XYZmeas( XYZcoord coord, Matrix covar ) 
  {
    mean.X = coord.X; mean.Y = coord.Y; mean.Z = coord.Z;
    cov.xx = covar(1,1); cov.xy = covar(1,2); cov.xz = covar(1,3);
    cov.yx = covar(2,1); cov.yy = covar(2,2); cov.yz = covar(2,3);
    cov.zx = covar(3,1); cov.zy = covar(3,2); cov.zz = covar(3,3);
  }
  // Copy constructor
  XYZmeas( const XYZmeas &other )
  {
    mean.X = other.mean.X; mean.Y = other.mean.Y; mean.Z = other.mean.Z;
    cov.xx = other.cov.xx; cov.xy = other.cov.xy; cov.xz = other.cov.xz;
    cov.yx = other.cov.yx; cov.yy = other.cov.yy; cov.yz = other.cov.yz;
    cov.zx = other.cov.zx; cov.zy = other.cov.zy; cov.zz = other.cov.zz;
  }
  // Accessor to return the Newmat ColumnVector type
  ColumnVector getMeanVector()
  {
    Matrix vec(3,3);
    Real a[] = {mean.X, mean.Y, mean.Z};
    vec << a;
    return vec;
  }
};

/*! This struct is a stripped down state struct that just has the pose
  * (position and orientation) of the vehicle as well as the variance in
  * its elements. */
struct VehPoseMeas
{
  /*! Vehicle Northing (meters) */
  double Northing;
  /*! Vehicle Easting (meters) */
  double Easting;
  /*! Vehicle Downing (meters) */
  double Downing;
  /*! Vehicle Pitch (radians) */
  double Pitch;
  /*! Vehicle Roll (radians) */
  double Roll;
  /*! Vehicle Yaw (radians) */
  double Yaw;

  /*! Variances in all of the above. */
  double Northing_var;
  double Easting_var;
  double Downing_var;
  double Pitch_var;
  double Roll_var;
  double Yaw_var;
};

struct SphericalMeas
{
  /*! The mean range, azimuth, and elevation of the measurement. */
  double range;
  double azimuth;
  double elevation;

  /*! The variance of the measured range, azimuth, and elevation. */
  double range_var;
  double azimuth_var;
  double elevation_var;

  /*! The default constructor. */
  SphericalMeas(){}
  
  /*! Copy constructor. */
  SphericalMeas( const SphericalMeas &other )
  {
    range = other.range;
    azimuth = other.azimuth;
    elevation = other.elevation;
    range_var = other.range_var;
    azimuth_var = other.azimuth_var;
    elevation_var = other.elevation_var;
  }
};

/*! Method to calculate the value of a multivariate normal probability
    density function at a point.  The pdf is described by a 3x3 covariance
    matrix and a 3x1 mean value. */
double mvnpdf( XYZcoord point, XYZcoord mean, XYZmatrix cov );

/*! Method to get the covariance matrix given the raw variances,
  measurement, and vehicle state.  Sensor-Vehicle tranform is assumed
  to be fixed, and those parameters #defined somewhere.  Returns a 3x3
  matrix.  */
XYZmatrix calcCovMatrix(VehPoseMeas vehmeas,  SphericalMeas sensormeas); 

/*! enumerate all module message types that could be received. */
namespace FusionMapperMessages
{
  enum
  {
    /*! Measurements received (integer number of measurements followed 
    by the measurements (each measurement is 3d mean and covariance) */
    LadarMeasurements,
    StereoMeasurements,

    /*! A place holder (not used) */
    NumMessages
  };
};

#endif
