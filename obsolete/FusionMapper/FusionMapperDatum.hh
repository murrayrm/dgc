#ifndef FUSIONMAPPERDATUM_HH
#define FUSIONMAPPERDATUM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>


/* START POTENTIAL LEGACY CODE
//--------------------------------------------------
// #include "Arbiter2/ArbiterDatum.hh"
//-------------------------------------------------- 
//--------------------------------------------------
// #include "RaceModules/Arbiter/Vote.hh"
// #include "RaceModules/Arbiter/SimpleArbiter.hh"
// #include "RaceModules/Arbiter/ArbiterModule.hh"
//-------------------------------------------------- 


#include "Constants.h" //vehicle constants
END POTENTIAL LEGACY CODE */

// The MTA Module Kernel
#include "MTA/Kernel.hh"

// The New State Struct Sent via MTA
#include "vehlib/VState.hh"
#include "vehlib/VDrive.hh"

// Include the Map class here.
#include "CMap/CMapPlus.hh"
//Include the RDDF class
#include "rddf.hh"
//Include the CCorridorPainter class
#include "CCorridorPainter.hh"
#include "CCostPainter.hh"

using namespace std;

// All data that needs to be shared by common threads
struct FusionMapperDatum 
{
  VState_GetStateMsg SS;
  
/* START POTENTIAL LEGACY CODE
  // set at the same time as SS, to monitor synchronization
  Timeval localTimestamp;
  // temporary time variables
  Timeval tmark;

  // to keep track of how many measurements we have received
  int ladarMeasCount;
  int stereoMeasCount;
  // Measurement messages generally have multiple measurements
  int ladarMessageCount;
  int stereoMessageCount;
  // Number of vote packages that FusionMapper has sent
  int sentVotesCount;
END POTENTIAL LEGACY CODE */
 
  // Declare the Map class.
  CMapPlus fusionMap;

  RDDF fusionRDDF;

  CCorridorPainter fusionCorridorPainter;
  CCostPainter fusionCostPainter;

};


#endif
