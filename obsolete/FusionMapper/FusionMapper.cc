#include <unistd.h>
#include <iostream>

#include "FusionMapper.hh"

#include "MTA/Misc/Thread/ThreadsForClasses.hpp"
#include "MTA/Misc/Time/Time.hh"

/* START POTENTIAL LEGACY CODE
#define BINARY_THRESH 1.0
END POTENTIAL LEGACY CODE */

int QUIT_PRESSED = 0;

extern int           NO_SPARROW_DISP;
extern int           NOSTATE;
extern int           SIMPLEUPDATE;
extern unsigned long SLEEP_TIME;

int SAVE_CMAP    = 0;
int CLEAR_CMAP   = 0;
int REFRESH_CMAP = 0;
int ACTIVE_LAYER = 0;

FusionMapper::FusionMapper() 
  : DGC_MODULE(MODULES::FusionMapper, "Fusion Mapper", 0) 
{
}

FusionMapper::~FusionMapper() 
{
}

void FusionMapper::Init() 
{
  // call the parent init first
  DGC_MODULE::Init();

/* START POTENTIAL LEGACY CODE
  active_counter = 0;
  time_begin = TVNow();
  // set the measurement count to zero.
  d.ladarMeasCount   = 0;
  d.stereoMeasCount  = 0;
  BadCovCount        = 0;
  OKCellUpdateCount  = 0;
  TooSmallPDFCount   = 0;
END POTENTIAL LEGACY CODE */

  // update the State Struct and check for valid state
  if(!NOSTATE) 
  {
    UpdateState();
    if(d.SS.Northing == 0) 
    {
      printf("\nUnable to communicate with vstate (Northing is zero).\n");
      printf("Run with --nostate option if this is OK. (Exiting...)\n\n");
      exit(-1);
    }
  }
  
  //Initialize the RDDF - not needed since it automatically loads rddf.dat,
  //which we should link to locally

  // Initialize the Map class that we will use, with the center of the Map, 
  // the number of rows and columns, and row and column resolution.
  //CHanged the initialization location to 0,0 to make corridor painting work
  d.fusionMap.initMap(0,0,
  //d.fusionMap.initMap( d.SS.Northing,              d.SS.Easting, 
                       DEFAULT_FUSIONMAP_NUM_ROWS, DEFAULT_FUSIONMAP_NUM_COLS,
                       DEFAULT_FUSIONMAP_ROW_RES,  DEFAULT_FUSIONMAP_COL_RES,
                       0 );
  
  // Add a layer to the map that represents the elevation computed from the 
  // Ladar measurements, and another that holds the elevation variance.
  ladarElevLayerID = d.fusionMap.addLayer<double>(FM_DOUBLE_NO_DATA, 
                                                  CM_DOUBLE_OUTSIDE_MAP);
  ladarVariLayerID = d.fusionMap.addLayer<double>(FM_DOUBLE_NO_DATA,
                                                  CM_DOUBLE_OUTSIDE_MAP);
  stereoElevLayerID = d.fusionMap.addLayer<double>(FM_DOUBLE_NO_DATA, 
                                                   CM_DOUBLE_OUTSIDE_MAP);
  rddfLayerID = d.fusionMap.addLayer<int>(CP_OUTSIDE_CORRIDOR, CP_OUTSIDE_MAP);
  costLayerID = d.fusionMap.addLayer<double>(0, CM_DOUBLE_OUTSIDE_MAP);

  //Initialize the corridor painter
  d.fusionCorridorPainter.initPainter(&(d.fusionMap), rddfLayerID, &(d.fusionRDDF));

  // Test the saving of changes to the map.
  d.fusionMap.initLayerLog( ladarElevLayerID, "booger.dat" );
  d.fusionMap.startLayerLog( ladarElevLayerID );

  
  // InitFusionLogs takes care of all of the logging (including determining
  // whether logs should be created or not)
//--------------------------------------------------
//   InitFusionLogs();
//-------------------------------------------------- 
  
  // start sparrow display in independent threads
  if(!NO_SPARROW_DISP) 
  {
    cout << "Starting Sparrow display in new thread." << endl;
    RunMethodInNewThread<FusionMapper>( this, 
		    &FusionMapper::SparrowDisplayLoop);
    UpdateSparrowVariablesLoop();
//--------------------------------------------------
//     RunMethodInNewThread<FusionMapper>( this, 
// 		    &FusionMapper::UpdateSparrowVariablesLoop);
//-------------------------------------------------- 
  }

  // leave this at the end 
  cout << "Module Init Finished" << endl;
}

void FusionMapper::Active() 
{
  cout << "Starting Active Function" << endl;

//--------------------------------------------------
//   int logCMapPlus = 0;
//-------------------------------------------------- 
// POTENTIAL LEGACY CODE  d.sentVotesCount = 0;

  while( ContinueInState() && !QUIT_PRESSED) 
  {
    // NOTE: FusionMapper now receives measurements over the MTA and
    // processes them into the map.  This is done in the InMailHandler.
    // TODO: Make sure that it is okay to do this much processing
    // within the InMailHandler.


/* START POTENTIAL LEGACY CODE
    //update state and shift the map
    int prevBottomLeftRowMultiple = d.fusionMap.getWindowBottomLeftUTMNorthingRowResMultiple();
    int prevBottomLeftColMultiple = d.fusionMap.getWindowBottomLeftUTMEastingColResMultiple();
END POTENTIAL LEGACY CODE */
    UpdateState();
    d.fusionMap.updateVehicleLoc( d.SS.Northing, d.SS.Easting );
    d.fusionCorridorPainter.paintChanges();

    /*-------------------------------------------*/
    

/* START POTENTIAL LEGACY CODE
    // increment module counter
    active_counter++;

    // set the current time
    time_now = TVNow();
    // set the difference since the last update
    time_diff = time_now - time_elapsed - time_begin;
    // set the current elapsed time
    time_elapsed = time_now - time_begin;
END POTENTIAL LEGACY CODE */     

    // update the sparrow display variables
    if(!NO_SPARROW_DISP) UpdateSparrowVariablesLoop();

    // UpdateFusionLogs contains all of the logging methods 
    // (even if we are not doing logging)
    // This also contains the key binding handling to save and clear maps.
//--------------------------------------------------
//     UpdateFusionLogs();
//-------------------------------------------------- 
  
    if(SAVE_CMAP) 
    {
      printf("Got SaveCMapPlus signal!\n");
      char* currLayer = MapLayerStrings[ACTIVE_LAYER];
      d.fusionMap.saveLayer<double>( ACTIVE_LAYER, currLayer);
      printf("Saved the layer.\n");
      SAVE_CMAP = false;
    }

    if(CLEAR_CMAP) 
    {
      printf("Got ClearCMapPlus signal!\n");
      d.fusionMap.clearMap();
      printf("Cleared the map.\n");
      CLEAR_CMAP = false;
    }


    // and sleep for a bit
    //time_now = TVNow(); // <- Debug
    usleep_for(200000);
    //printf("Time to do usleep_for is %f\n", (TVNow() - time_now).dbl() );

  } // end while( ContinueInState() && !QUIT_PRESSED) 

  cout << "Finished Active State" << endl;
}

void FusionMapper::Shutdown() 
{
  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;
  cout << "Closing (1) CMap layer log file" << endl;
  d.fusionMap.closeLayerLog( ladarElevLayerID );
}

void FusionMapper::InMailHandler(Mail& msg)
{
  switch (msg.MsgType())
  {
    case FusionMapperMessages::LadarMeasurements:
    {
/* START POTENTIAL LEGACY CODE
      // number of measurements
      int num;
      // The first item in a LadarMeasurements message is an integer that
      // tells us how many measurements are to follow in the message.
      msg >> num;
      
      // Get the vehicle Northing and Easting from the measurement
      // message so that we can re-center the map around the vehicle.
      msg >> d.SS.Northing;
      msg >> d.SS.Easting;
      //d.fusionMap.updateVehicleLoc( d.SS.Northing, d.SS.Easting );
      //d.fusionPainter.paintChanges();
        
      // create an uninitialized measurement
      XYZmeas measurement;

      // read in the rest of the MTA message (all of the measurements)
      for(int i=0; i<num; i++)
      { 
	// increment the measurement counter
	d.ladarMeasCount++;
	// process the measurement that we've just received
	msg >> measurement;

	// Now update the map with this measurement
	if( SIMPLEUPDATE )
        {
          UpdateCellsSimple( d.fusionMap, ladarElevLayerID, measurement );
        }
        else
        {
          UpdateCells273( d.fusionMap, measurement );
        }
      }
      // increment the measurement counters
      d.ladarMeasCount = d.ladarMeasCount + num;
      d.ladarMessageCount++;
    }
END POTENTIAL LEGACY CODE */
    break;

    /*
    case FusionMapperMessages::StereoMeasurements:
    {
      // number of measurements
      int num;
      long int windowRowOffset;
      long int windowColOffset;
      int tempRow, tempCol;
      double tempNorthing;
      double tempEasting;
      Timeval tv1;

      // The first item in a LadarMeasurements message is an integer that
      // tells us how many measurements are to follow in the message.
      msg >> num;
      
      // Get the vehicle Northing and Easting from the measurement
      // message so that we can re-center the map around the vehicle.
      msg >> d.SS.Northing;
      msg >> d.SS.Easting;
        
      // update the internal state of the Map class (to center the Map)

      msg >> windowRowOffset;
      msg >> windowColOffset;

      // Declare a measurement to be used 
      XYZmeas sv_meas;

      // create an uninitialized z-value
      int cellCount;
      double cellValue;

      printf("Got a stereo measurement of size %d\n", num);
      tv1 = TVNow();

      // read in the rest of the MTA message (all of the measurements)
      for(int i=0; i<num; i++)
      { 
	// process the measurement that we've just received
	msg >> tempRow;
	msg >> tempCol;
	msg >> cellCount;
	msg >> cellValue;
	tempNorthing = ((double)(tempRow + windowRowOffset)) * 
                       d.fusionMap.getResRows();
	tempEasting = ((double)(tempCol + windowColOffset)) *
                      d.fusionMap.getResCols();
	//The value is cellValue, at UTM coords (tempNorthing, tempEasting)
              
	// Now update the map with this measurement
        sv_meas.mean.X = tempNorthing;
        sv_meas.mean.Y = tempEasting;
        sv_meas.mean.Z = cellValue;
        UpdateCellsSimple( d.fusionMap, stereoElevLayerID, sv_meas );
      }
      // increment the measurement counters
      d.stereoMeasCount = d.stereoMeasCount + num;
      d.stereoMessageCount++;

      printf("Processing stereovision message took %f sec\n", 
             (TVNow() - tv1).dbl());
      printf("The last meas had mean (%f, %f, %f).\n\n", 
             sv_meas.mean.X, sv_meas.mean.Y, sv_meas.mean.Z );
      */
    }
    break;
    

    default:
      // let the parent mail handler handle it
      DGC_MODULE::InMailHandler(msg);
      break;
  }
}

#ifdef LEGACY_CODE
void FusionMapper::UpdateCellsSimple( CMapPlus& map, 
                                      int layerID, 
                                      XYZmeas meas3d )
{
  // Assign the cell the new elevation if its absolute value is bigger
  // than the absolute value of the old elevation.
  /* Return a reference to the current elevation at the mean value. */
  double currElev = map.getDataUTM<double>( layerID, 
                                            meas3d.mean.X, 
                                            meas3d.mean.Y );
  if ( currElev == CM_DOUBLE_OUTSIDE_MAP )
  {
    /* Maybe give a warning. */
    printf("currElev == CM_DOUBLE_OUTSIDE_MAP at %f, %f\n", 
           meas3d.mean.X, meas3d.mean.Y);
    printf("Window bottom left is at (%f, %f) m (N,E)\n", 
           map.getWindowBottomLeftUTMNorthing(),
           map.getWindowBottomLeftUTMEasting() );
  }
  else
  {
    if ( fabs(meas3d.mean.Z) > fabs(currElev) || 
                    currElev == FM_DOUBLE_NO_DATA ) 
    {
      /* TODO: Change data in the reference location directly (faster). */
      map.setDataUTM<double>( layerID, 
                              meas3d.mean.X, meas3d.mean.Y, meas3d.mean.Z );
      updateCostElevRDDF((CMap*)&map, layerID, rddfLayerID, costLayerID, BINARY_THRESH, meas3d.mean.X, meas3d.mean.Y);
    }
  }
}

void FusionMapper::UpdateCells273( CMapPlus& map, XYZmeas meas3d )
{
  // TODO: 1. Find the list of cells that we want to update for a given measmt.
  //       2. Find the measurement mean over these cells for meas3d.cov
  //       3. Calculate the measurement variance for each cell (ad hoc method).
  //       4. Re-assign the Cell means and variances.
  
//--------------------------------------------------
//   printf("meas3d.cov = [ % f  % f  % f ]\n", 
//          meas3d.cov(1,1), meas3d.cov(1,2), meas3d.cov(1,3) );
//   printf("             [ % f  % f  % f ]\n", 
//          meas3d.cov(2,1), meas3d.cov(2,2), meas3d.cov(2,3) );
//   printf("             [ % f  % f  % f ]\n", 
//          meas3d.cov(3,1), meas3d.cov(3,2), meas3d.cov(3,3) );
//-------------------------------------------------- 

  /* Start here with newmat implementation. */
//--------------------------------------------------
//   printf("About to check determinant ..\n");
//-------------------------------------------------- 
  // An XYZmeas stores an XYZcoord and an XYZmatrix.  Convert the latter to 
  // a Newmat matrix so that we can do fancy matrix operations.
  Matrix cv = meas3d.cov.getCovMatrix();
  Matrix Cinv(3,3);

  if( fabs(cv.Determinant()) < EPSILON ) 
  {
//--------------------------------------------------
//     printf("Covariance matrix is not invertible or (3,3) element is too\n"
//            "small.  Reverting to simple update via UpdateCellsSimple.\n");
//-------------------------------------------------- 
    /* DEBUG */
    printf("fabs(cv.Determinant()) = %f\n", fabs(cv.Determinant()));
    printf("EPSILON = %f\n", EPSILON );
    printf("Cov matrix follows, Determinant = %f\n", cv.Determinant());
    meas3d.cov.printXYZmatrix(); 
    
//--------------------------------------------------
//     UpdateCellsSimple( map, meas3d );
//-------------------------------------------------- 
    BadCovCount++;
    return;
  }
  else
  {
    Cinv = cv.i();

//--------------------------------------------------
//     /* DEBUG */
//     printf("Cinv = [ %f %f %f ;\n", Cinv(1,1), Cinv(1,2), Cinv(1,3));
//     printf("         %f %f %f ;\n", Cinv(2,1), Cinv(2,2), Cinv(2,3));
//     printf("         %f %f %f ]\n", Cinv(3,1), Cinv(3,2), Cinv(3,3));
//-------------------------------------------------- 
  }
  /* If we get this far, the covariance matrix has a valid inverse. */
  /* Catch the case where the (3,3) element is too small, as we will divide
     by it. */
  if( Cinv(3,3) < EPSILON )
  {
    /* DEBUG */
    printf("Cinv(3,3) = %f is too small.\n", Cinv(3,3));
    
    BadCovCount++;
    return;
  }
         
  /* Get the row and column resolution. */
  double resRow = map.getResRows();
  double resCol = map.getResCols();

  /* First find the Northing and Easting of the cell that the mean
     falls into. */
  double mean_cellNorthing;
  double mean_cellEasting;
  map.roundUTMToCellBottomLeft( meas3d.mean.X, meas3d.mean.Y, 
                                &mean_cellNorthing, &mean_cellEasting);
  /* Get the center UTM location of the cell. */
  mean_cellNorthing = mean_cellNorthing + resRow/2.0;
  mean_cellEasting  = mean_cellEasting  + resCol/2.0;
  
  /* Keep placeholders for the given cell Northing and Easting */
  double cellNorthing = 0.0;
  double cellEasting  = 0.0;
  
  /* placeholder for cell mean and variance based on this measurement.  */
  double cell_measVar;
  double pdf_val;

  /* placeholder for the center and mean value of a cell. */
  XYZcoord cellMeas;

  /* 1. For now, just update the cell centered around the measurement and the
     eight cells immediately surrounding it. */
  for( int i = -1; i <= 1; i++ )
  {
    for( int j = -1; j <= 1; j++ )
    {
      /* Store the cell Easting and Northing. */
      cellMeas.X = mean_cellNorthing + resRow * (double)i;
      cellMeas.Y  = mean_cellEasting + resCol * (double)j;
      
      /* get the mean *for this measurement* by finding the intersection with 
         the plane that represents the expected z value for a given (x,y) */
      /* means(i) = -1/(2*Cinv(3,3)) * ...
         ( ( CELLS_N(indices(i,1))-u(N_IDX) )*( Cinv(1,3)+Cinv(3,1) ) + ...
           ( CELLS_E(indices(i,2))-u(E_IDX) )*( Cinv(2,3)+Cinv(3,2) ) ); */
//--------------------------------------------------
//       /* TODO: Investigate why this value is so big. */
//       cellMeas.Z = -1.0 / (2.0 * Cinv(3,3)) * 
//                       ( ( cellNorthing - meas3d.mean.X)*(2*Cinv(1,3)) +
//                         ( cellEasting  - meas3d.mean.Y)*(2*Cinv(2,3)) );
//-------------------------------------------------- 
      /* DEBUG */
      cellMeas.Z = meas3d.mean.Z;
//--------------------------------------------------
//       /* DEBUG */
//       cellMeas.Z = 0.0;
//-------------------------------------------------- 
      
      /* The cell measurement variance is a function of the probability 
         density function (as described by the covariance matrix), evaluated at 
         the cell position and height given by the mean above. */
      pdf_val = mvnpdf( cellMeas, meas3d.mean, meas3d.cov );
      /* Only perform a cell update if the probability density function is
         sufficiently large at the cell location (center). */
      if( pdf_val <= EPSILON )
      {
        /* DEBUG */
        printf("pdf_val = %f, EPSILON = %f\n", pdf_val, EPSILON);
        
        TooSmallPDFCount++;
      }
      else
      {
        //printf("Actually doing variance-based elevation update\n");
        OKCellUpdateCount++;
        
        /* Then update the cell with a measurement with mean cellMeas.Z and
           variance equal to 1 / (2 * PI * pdf_val^2). */
        cell_measVar = 1.0 / (2.0 * M_PI * pow(pdf_val,2));

        /* update the cells according to the 1D KF equations (Kristo derived)
           H_EST(indices(i,2),indices(i,1)) = 
             (H_EST(indices(i,2),indices(i,1))*var(i) + ...
             h(i)*H_EST_VAR(indices(i,2),indices(i,1))) / ...
             (var(i) + H_EST_VAR(indices(i,2),indices(i,1)));

           H_EST_VAR(indices(i,2),indices(i,1)) = 
             (var(i) * H_EST_VAR(indices(i,2),indices(i,1))) / ...
             (var(i) + H_EST_VAR(indices(i,2),indices(i,1))); */
        double currElev = 
          map.getDataUTM<double>( ladarElevLayerID, cellMeas.X, cellMeas.Y );
        double currVar = 
          map.getDataUTM<double>( ladarVariLayerID, cellMeas.X, cellMeas.Y );
  
        /* Handle the case where the variance is not yet set. */
        if( currVar == FM_DOUBLE_NO_DATA ) 
        {
          currVar = DEFAULT_FUSIONMAP_VARIANCE;
          /* Set the current elevation to zero so that the update will
             set the new estimate purely according to this measurement. */
          currElev = 0.0;
        }
        
        double newHeightEst = ( currElev * cell_measVar + 
                                cellMeas.Z * currVar ) /
                              ( cell_measVar + currVar );
        
        /* DEBUG */
        if( newHeightEst != 0.0 )
        {
          //printf("newHeightEst = %f\n", newHeightEst);
        }
        if( newHeightEst != newHeightEst )
        {
          printf("newHeightEst is NaN!\n");
          printf("currElev     = %f\n", currElev);
          printf("currVar      = %f\n", currVar);
          printf("cellMeas.Z   = %f\n", cellMeas.Z);
          printf("cell_measVar = %f\n", cell_measVar);
          printf("pdf_val      = %f\n", pdf_val);
          printf("Cinv.Det()   = %f\n\n", Cinv.Determinant());
        }
        
        double newHeightVar = ( cell_measVar * currVar ) / 
                              ( cell_measVar + currVar );

        map.setDataUTM<double>( ladarElevLayerID, cellMeas.X, cellMeas.Y, 
                                newHeightEst );
        map.setDataUTM<double>( ladarVariLayerID, cellMeas.X, cellMeas.Y, 
                                newHeightVar );
      }
    } // end for (rows)
  } // end for (cols)
  
} // end UpdateCells273
#endif // LEGACY CODE

// end FusionMapper.cc
