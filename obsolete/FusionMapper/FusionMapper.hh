#ifndef FUSIONMAPPER_HH
#define FUSIONMAPPER_HH

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "FusionMapperDatum.hh" // This includes the Map class
#include "mapper_functions.hh" // XYZmeas struct, MTA message definitions
#include "cost_functions.hh"

#define EPSILON 0.00001
#define SLOPE_THRESH 0.4

using namespace std;
using namespace boost;

static char* MapLayerStrings[] =
{
  "ladarElevLayerID",
  "ladarVariLayerID",
  "stereoElevLayerID",
  "rddfLayerID",
  "costLayerID"
};

class FusionMapper : public DGC_MODULE 
{
public:
  // and change the constructors
  FusionMapper();
  ~FusionMapper();

  // The states in the state machine.
  // Uncomment these if you wish to define these functions.
  void Init();

  void Active();

  //  void Standby();

  void Shutdown();

  //  void Restart();

  // The message handlers. 
  void InMailHandler(Mail & ml);

  //Mail QueryMailHandler(Mail & ml);

  // the status function.
  //string Status();

/* START POTENTIAL LEGACY CODE
  void InitFusionLogs();

  void UpdateFusionLogs();

  void SaveCMapPlus();

  void ClearCMapPlus();
END POTENTIAL LEGACY CODE */

  void UpdateState();

private:

  /*! Simple method to calculate the cell updates for a given XYZMeas */
  // POTENTIAL LEGACY CODE - void UpdateCellsSimple( CMapPlus& map, int layerID, XYZmeas meas3d );

  /*! Method to calculate the cell updates for a given XYZMeas.  This is the
   *  method that was used in CDS 273 that runs a simple set of 1D Kalman Filter
   *  update equations to estimate the height for each cell. */
  // POTENTIAL LEGACY CODE - void UpdateCells273( CMapPlus& map, XYZmeas meas3d );

  /*! Method to run in separate thread.  This function runs the dd_loop() 
   *  Sparrow method to update the screen and take keyboard commands. */
  void SparrowDisplayLoop();

  /*! Method to update the variables that are to be displayed in the Sparrow
   *  interface.  This method does not run a loop. */
  void UpdateSparrowVariablesLoop();

  /*! The datum named d. */
  FusionMapperDatum d;

  /*! Indicates how many times the while loop in the Active function has run. 
   ** */
/* START POTENTIAL LEGACY CODE
  int active_counter;
  int BadCovCount;
  int OKCellUpdateCount;
  int TooSmallPDFCount;

  Timeval time_begin; // Time that module started
  Timeval time_now; // time at beginning of each update
  Timeval time_diff; // Time difference since last update
  Timeval time_elapsed; // Total elapsed module runtime
END POTENTIAL LEGACY CODE */


  // Map layer information
  int stereoElevLayerID;
  int ladarElevLayerID;
  int ladarVariLayerID;
  int rddfLayerID;
  int costLayerID;
  };

#endif
