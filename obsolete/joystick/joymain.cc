// joymain.cc
#include "Msg/DGC_MODULE.ct"

#include "Msg/dgc_mta.hh"
#include "Msg/dgc_msg.h"
#include "Msg/dgc_socket.h"
#include "Msg/dgc_messages.h"
#include "Msg/dgc_const.h"
#include "Msg/dgc_mta_main.h"
#include "sw.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/times.h> 

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <iostream>
#include <list>
#include <time.h>
#include <unistd.h>

using namespace std;
int myState;
int myRequestedState;


void on_state() 
{
  struct steer_struct st;
  clock_t   	start, end;
  long int 	elapse_us, wait_us, clktck=0;
  dgc_MsgHeader msg;

  clktck=sysconf(_SC_CLK_TCK);	// get system clock tick per second

  while(myState==Joystick_ON)
  {
  	start=times(NULL);
	get_steering(&st);

	msg.Destination = 0; // send by type
	msg.DestinationType = dgcs_ModeManager; // request steering control from the system master
	msg.MsgSize = sizeof(struct steer_struct);
	msg.MsgType = dgcm_JoystickStatePacket;

	dgc_SendMessage(msg, &st, 0);

	end=times(NULL);
	elapse_us = (((end-start)*1000000))/clktck; // calculate elapsed time in us
        wait_us=STEER_ON_UPDATE_DELAY_US-elapse_us;// calculate how much to wait in us
        if(wait_us<0) wait_us=0;
	//	sleep(1);
	usleep(20000);  //wait_us);
   }	
}

void off_state() {

  int exiting=FALSE;
  steer_struct mySS;
  dgc_MsgHeader myMH;

  // 2 stages
  //  1 - Check if we have a message telling us to start sending joystick commands
  //  2 - Check the joystick to see if we should request control
  

  while( ! exiting ) 
  {
    if( dgc_AnyMessages() == TRUE ) 
    {
      dgc_PollMessage(myMH);
      if ( myMH.MsgID == 0 ) 
      {
	cout << "Error: Retrieving Message" << endl;
      } else 
        {
	switch( myMH.MsgType ) 
        {
	  case dgcm_JoystickControlState:
	    if ( myMH.MsgSize != sizeof(int)) 
            {
	      cout << "Error: Message size is not what was expected" << endl;
	      // just let the MTA delete the message
	      dgc_RemoveMessage( myMH );
	    } 
	    else 
	    {
	      // get the message (size of msg is taken from the header)
	      int myNewState;
	      dgc_GetMessage(myMH, & myNewState);
	      switch( myNewState ) 
	      {
	        case Joystick_ON:
		  exiting = TRUE;
		  myState = Joystick_ON;
		  break;
	        case Joystick_OFF:
		  // we are already in the off state, do nothing
		  // except reset our request flag
		  myRequestedState = Joystick_OFF;
		  break;
	        default:
		  cout << "Error: unknown state, just ignoring" << endl;
	      }
	    }
	    break;
	  default:
	    // we are not expecting any other messages (ignore them for now)
	    // and just tell the MTA to delete the message
	    dgc_RemoveMessage( myMH );
	}// switch
	}//else
    }// if

    // Stage 2 - check steering wheel to see if we should request control
    // If we have already sent a message requesting control, then don't 
    // send another.
    if ( ! exiting && myRequestedState != Joystick_ON ) 
    {
      get_steering(&mySS);
      if (mySS.buttons == STEER_TURN_ON) 
      {
	int msg = Joystick_ON;
	myMH.Destination = 0; // send by type
	myMH.DestinationType = dgcs_ModeManager; // request steering control from the system master
	myMH.MsgSize = sizeof(int);
	myMH.MsgType = dgcm_JoystickRequestsControl;

	myRequestedState = Joystick_ON;
	dgc_SendMessage(myMH, &msg);

	// in the next iteration of the loop, messages will again be checked
	// and the system manager may allow the joystick to take control.
      }
    }
	
     usleep(STEER_OFF_UPDATE_DELAY_US); 
   } //while
}




//int main(int argc, char* argv[])
int DGC_MODULE::Run()
{
    int timetoquit = FALSE;

    dgc_msg_Register(dgcs_JoystickDriver);

  if(init_steering(PP_STEER)!=0)
	cout<< "Can't open port for steering wheel!!!"<< endl;
  sleep(1);
  myState = Joystick_ON;
  myRequestedState = Joystick_OFF;
  while( ! timetoquit ) {
    if ( myState == Joystick_OFF ) 
    { 
      off_state();
    } 
    else if (myState == Joystick_ON) 
    {
      on_state();
    } 
    else 
    {
      cout << "Error: myState is invalid, exiting" << endl;
      timetoquit = TRUE;
    }
  }

    dgc_msg_Unregister();
  end_steering();
  return 0;
}














