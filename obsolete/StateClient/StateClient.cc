#include "StateClient.h"
#include "DGCutils"
#include <iostream>
using namespace std;

CStateClient::CStateClient(modulename myname, int key)
	: m_skynet(myname, key)
{
	// initialize the mutexes and the condition variable
	DGCcreateMutex(&m_receivedStateMutex);
	DGCcreateMutex(&m_gotStateConditionMutex);
	DGCcreateCondition(&m_gotStateCondition);

	// open the send socket for the state request and listen for state from everybody.
	m_reqstatesocket = m_skynet.get_send_sock(SNreqstate);
	m_statesocket    = m_skynet.listen(SNstate, ALLMODULES);

	// initialize the gotstate variable to true because we don't start out waiting
	// for a state. This only happens when we request a state
	m_bGotState = true;

	// start the thread that continually gets the state
	DGCstartMemberFunctionThread(this, &CStateClient::getStateThread);
}

CStateClient::~CStateClient()
{
	DGCdeleteMutex(&m_receivedStateMutex);
	DGCdeleteMutex(&m_gotStateConditionMutex);
	DGCdeleteCondition(&m_gotStateCondition);
}

void CStateClient::UpdateState()
{
	char reqstatedummy;

	m_bGotState = false;

// 	cerr << "about to send request" << endl;
	while(m_skynet.send_msg(m_reqstatesocket, &reqstatedummy, sizeof(reqstatedummy), 0) != sizeof(reqstatedummy))
		cerr << "Didn't send the right number of bytes in the getstate cmd" << endl;

// 	cerr << "sent request" << endl;
  // wait until we get a state
	DGCWaitForConditionTrue(m_bGotState, m_gotStateCondition, m_gotStateConditionMutex);
// 	cerr << "condition must have turned true" << endl;
 
	// copy the received state into the state structure
	DGClockMutex(&m_receivedStateMutex);
	memcpy(&m_state, &m_receivedState, sizeof(m_state));
	DGCunlockMutex(&m_receivedStateMutex);
}

void CStateClient::getStateThread()
{
	while(true)
	{
		if(m_skynet.get_msg(m_statesocket, &m_receivedState, sizeof(m_receivedState), 0, &m_receivedStateMutex) != sizeof(m_receivedState))
			cerr << "Didn't receive the right number of bytes in the state structure" << endl;

    //cerr << "received state" << endl;
		DGCSetConditionTrue(m_bGotState, m_gotStateCondition, m_gotStateConditionMutex);
	}
}
