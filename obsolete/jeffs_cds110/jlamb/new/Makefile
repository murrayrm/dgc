# Makefile for vehlib 2.0
# RMM, 20 Jan 04
#
# This makefile will create all programs in the vehlib directory
#

# List of programs, libraries, and include files in this package
VER = dev
PGMS = vdrive vstate vtest vremote braketest ladartest brake_reset #statesim
TBLS = a_to_t_lookup ctr_to_t_lookup v_to_a_lookup v_to_t_lookup vdrive.ini \
	ctr_to_t_PID
SCRS = vdrive.sh vstate.sh vmanage.sh

INCS = imu.h gps.h magnetometer.h VState.hh StateStructMsg.hh \
       LUtable.h brake2.h crc32.h cruise.h imu_net.h \
       throttle.h vehports.h steer.h VDrive.hh joystick.h \
       serial2.hh OBDII.h VStarPackets.hh \
       VStarDevices.hh VStarMessages.hh estop.h transmission.h \
       serial.h statefilter.hh parallel.h ladar_power.h \
       lateral.h

INIS = vdrive.ini
LIBS = vehlib$(VER).a

# Location of programs, include files, libraries
DGC = ../../../..
BOB = ../../../../bob
CDD = $(DGC)/bin/cdd
INC = -I$(DGC)/include -I$(BOB)/include -I$(DGC)/include/frames
LIB = $(BOB)/lib/kfilter.a $(DGC)/lib/sparrow.a \
      -ltermcap -lcurses -lncurses -lpthread -lboost_thread

DDS = vddisp.dd vdparm.dd vsdisp.dd
DDH = $(DDS:.dd=.h)

MTALIB = $(DGC)/lib/mta2_1.a 
FRAMELIB=$(DGC)/lib/frames.a
GGISLIB=$(DGC)/lib/ggis.a

MISCINC=VState.hh serial.h gps.h imu.h magnetometer.h

CC = g++
CFLAGS = -g $(INC) #-DSERIAL2
# MTA will not compile on CDS cluster without the -pthread option 
# CDS cluster has gcc version 3.2.2 20030222 (Red Hat Linux 3.2.2-5)
# -g option is for gdb debugger and -O2 is for code optimization
CPPFLAGS+=$(CFLAGS) -pthread -O2
# Preserve the user's environment variable(s)
LDFLAGS+=


all: $(PGMS) $(INIS) $(TAGS) scripts

install: vehlib.a $(BOB)/lib $(BOB)/doc $(BOB)/include/vehlib sheader
	install -m 644 vehlib.html $(BOB)/doc
	install -m 644 vehlib.a $(BOB)/lib/vehlib.a
	ranlib $(BOB)/lib/vehlib.a
	install -m 644 ladar_power.h ${BOB}/include
	install -m 644 parallel.h ${BOB}/include

# Possible versioning method for executable backups
# Executables are NOT to be added to Subversion
install-exec: $(BOB)/bin all
	install -d $(BOB)/bin/vehlib-$(VER)
	install $(PGMS) $(BOB)/bin/vehlib-$(VER)
	install $(SCRS) $(BOB)/bin/vehlib-$(VER)
	install -m 644 $(TBLS) $(BOB)/bin/vehlib-$(VER)

$(BOB)/bin:; mkdir $@
$(BOB)/doc:; mkdir $@
$(BOB)/lib:; mkdir $@
$(BOB)/include/vehlib:; mkdir $@

# vehlib library
SRC = gps.cc magnetometer.c vstate.cc imu.cc \
	vsmta.cc crc32.cc throttle.c brake2.c cruise.cc \
	steer.cc joystick.cc LUtable.cc OBDII.cc serial2.cc \
	VStarPackets.cc estop.c transmission.c \
	serial.c statesim.cc statefilter.cc parallel.c ladar_power.cc \
	lateral.c

OBJ = gps.o magnetometer.o imu.o \
	crc32.o throttle.o brake2.o cruise.o steer.o joystick.o \
	LUtable.o OBDII.o VStarPackets.o estop.o transmission.o \
	serial.o serial2.o statefilter.o parallel.o ladar_power.o\
	lateral.o
VEHLIB = vehlib.a

$(VEHLIB): $(OBJ)
	ar r $@ $?
	ranlib $@

$(FRAMELIB): 
	cd $(DGC)/frames; make install

# vdrive program
VDOBJ= vdrive.o vdmta.o
vdrive: $(VDOBJ) $(VEHLIB) $(MTALIB)
	$(CC) $(CFLAGS) $(LDFLAGS) $^ $(LIB) -o $@ 

# if initialiation and device configuration files don't exist, create them
vdrive.ini:;	cp vdrive.ini-def vdrive.ini

# vstate program
vstate: vstate.o $(VEHLIB) vsmta.o $(MTALIB) $(FRAMELIB) $(GGISLIB)
	$(CC) $(CFLAGS) $^ $(LIB) -o $@ 

sheader: VState.hh
	install -m 644 $(INCS) $(BOB)/include/vehlib

statesim: VState.hh statesim.o $(VEHLIB)
	$(CC) $(CFLAGS) -o $@ statesim.o $(VEHLIB) $(FRAMELIB) $(MTALIB) $(LIB)

statefilter.o: $(GGISLIB)

$(GGISLIB):
	cd $(DGC)/latlong; make install
		
# vtest program
vtest: vtest.o $(MTALIB)
	$(CC) $(CFLAGS) -o $@ vtest.o $(MTALIB) $(LIB)


# vremote program
VROBJ= vremote.o
vremote: $(VROBJ) $(VEHLIB) $(MTALIB)
	$(CC) $(CFLAGS) -o $@ $(VROBJ) $(VEHLIB) $(MTALIB) $(LIB)


throttletest: throttletest.o $(VEHLIB)
	$(CC) $(CFLAGS) -o $@ throttletest.o $(VEHLIB) $(MTALIB) $(LIB)

serial2test: serial2test.cc serial2.hh serial2.cc
	$(CC) $(CFLAGS) -o serial2test serial2test.cc $(VEHLIB) $(MTALIB) $(LIB)

braketest: braketest.o brake2.o $(VEHLIB)
	$(CC) $(CFLAGS) -o braketest braketest.o brake2.o $(VEHLIB) $(MTALIB) $(LIB)

ladartest: ladartest.o ladar_power.o $(VEHLIB)
	$(CC) $(CFLAGS) -o ladartest ladartest.o ladar_power.o $(VEHLIB) $(MTALIB) $(LIB)

brake_reset: brake_reset.o brake2.o $(VEHLIB)
	$(CC) $(CFLAGS) -o brake_reset brake_reset.o brake2.o $(VEHLIB) $(MTALIB) $(LIB)

obd2: obd2_test.o $(VEHLIB)
	$(CC) $(CFLAGS) -o $@ obd2_test.o $(VEHLIB) $(MTALIB) $(LIB)

trans_test: trans_test.o transmission.o $(VEHLIB)
	$(CC) $(CFLAGS) -o $@ trans_test.o transmission.o $(VEHLIB) $(MTALIB) $(LIB)
# Display files
vddisp.h: vddisp.dd $(CDD);	$(CDD) -o $@ vddisp.dd
vdparm.h: vdparm.dd $(CDD);	$(CDD) -o $@ vdparm.dd
vsdisp.h: vsdisp.dd $(CDD);	$(CDD) -o $@ vsdisp.dd
vrdisp.h: vrdisp.dd $(CDD);	$(CDD) -o $@ vrdisp.dd

#
# Startup scripts
# 
# These scripts are used to startup the various vehlib programs.  The
# scripts are all generated from a common source template, and then 
# modified using sed.
#
# These scripts are no longer generated from a template

SCRIPTS = vdrive.sh vstate.sh vmanage.sh 
scripts: $(SCRIPTS)
#%.sh:%.sed vtemplate.sh
#	sed -f $< vtemplate.sh > $@
#	chmod +x $@

#
# Documentation
#
TXISRC = vehlib.txi steer.txi vdrive.txi
vehlib.html: $(TXISRC)
	makeinfo --html --no-split -o $@ vehlib.txi

vehlib.info: $(TXISRC)
	makeinfo --no-split -o $@ vehlib.txi

clean:
	/bin/rm -f *.o *~ Makefile.bak $(PGMS) $(DDH) *.a 

depend:
	makedepend -Y $(CFLAGS) $(SRC) vdrive.c vstate.cc vdmta.cc

# Generate a TAGS file
TAGS: *.c *.h *.cc
	etags *.[ch] *.cc

# Files not picked up by makedepend (because the may not exist when it is run)
vdrive.o: vddisp.h vdparm.h
vstate.o: vsdisp.h $(FRAMELIB)
vremote.o: vrdisp.h

#
# Dependencies - created by depend target
#
# DO NOT DELETE

gps.o: gps.h serial.h
magnetometer.o: magnetometer.h serial.h ../../../../include/sparrow/dbglib.h
vstate.o: ../../../../include/sparrow/display.h ../../../../include/sparrow/dbglib.h
vstate.o: ../../../../include/sparrow/channel.h vehports.h
vstate.o: ../../../../include/MTA/Modules.hh vsmta.hh
vstate.o: ../../../../include/MTA/DGC_MODULE.hh ../../../../include/MTA/Misc/Mail/Mail.hh
vstate.o: ../../../../include/MTA/KernelDef.hh ../../../../include/MTA/Misc/File/File.hh
vstate.o: ../../../../include/MTA/Misc/Time/Timeval.hh
vstate.o: ../../../../include/MTA/Misc/Pipes/BufferedPipe.hh
vstate.o: ../../../../include/MTA/Misc/Pipes/Pipe.hh
vstate.o: ../../../../include/MTA/Misc/Memory/SharedMemoryContainer.hh
vstate.o: ../../../../include/MTA/Misc/Structures/IP.hh
vstate.o: ../../../../include/MTA/Misc/Structures/MailAddress.hh
vstate.o: ../../../../include/MTA/Misc/Structures/IP.hh
vstate.o: ../../../../include/MTA/Misc/Structures/ModuleInfo.hh
vstate.o: ../../../../include/MTA/Misc/Structures/MailAddress.hh
vstate.o: ../../../../include/MTA/Misc/Time/Timeval.hh VState.hh
vstate.o: ../../../../include/MTA/Misc/Mail/Mail.hh imu.h gps.h serial.h
vstate.o: magnetometer.h ../../../../include/MTA/Kernel.hh
vstate.o: ../../../../include/MTA/Friends.hh ../../../../include/MTA/Modules.hh
vstate.o: ../../../../include/MTA/DGC_MODULE.hh ../../../../include/MTA/KernelDef.hh
vstate.o: ../../../../include/MTA/Misc/Socket/Socket.hh
vstate.o: ../../../../include/MTA/Misc/File/File.hh
vstate.o: ../../../../include/MTA/Misc/Time/Timeval.hh ../../../../include/MTA/Debug.hh
vstate.o: ../../../../include/MTA/SystemMap.hh
vstate.o: ../../../../include/MTA/Misc/Structures/IP.hh
vstate.o: ../../../../include/MTA/Misc/Structures/Computer.hh
vstate.o: ../../../../include/MTA/Misc/Structures/ModuleInfo.hh
vstate.o: ../../../../include/LatLong.h ../../../../bob/include/kfilter/kfilter.h
vstate.o: ../../../../bob/include/kfilter/Kalmano.h ../../../../bob/include/kfilter/Imu.h
vstate.o: ../../../../bob/include/kfilter/Global.h statefilter.hh VStarPackets.hh
vstate.o: VStarDevices.hh VDrive.hh ../../../../include/frames/rot_matrix.hh
vstate.o: ../../../../include/frames/CMatrix.hh ../../../../include/frames/frames.hh
vstate.o: OBDII.h vsdisp.h
imu.o: crc32.h imu_net.h imu.h ../../../../include/sparrow/dbglib.h vehports.h
vsmta.o: ../../../../include/MTA/Kernel.hh ../../../../include/MTA/Friends.hh
vsmta.o: ../../../../include/MTA/Modules.hh ../../../../include/MTA/DGC_MODULE.hh
vsmta.o: ../../../../include/MTA/KernelDef.hh
vsmta.o: ../../../../include/MTA/Misc/Structures/ModuleInfo.hh
vsmta.o: ../../../../include/MTA/Misc/Structures/MailAddress.hh
vsmta.o: ../../../../include/MTA/Misc/Socket/Socket.hh
vsmta.o: ../../../../include/MTA/Misc/File/File.hh
vsmta.o: ../../../../include/MTA/Misc/Time/Timeval.hh
vsmta.o: ../../../../include/MTA/Misc/File/File.hh
vsmta.o: ../../../../include/MTA/Misc/Mail/Mail.hh ../../../../include/MTA/KernelDef.hh
vsmta.o: ../../../../include/MTA/Misc/Pipes/BufferedPipe.hh
vsmta.o: ../../../../include/MTA/Misc/Pipes/Pipe.hh
vsmta.o: ../../../../include/MTA/Misc/Memory/SharedMemoryContainer.hh
vsmta.o: ../../../../include/MTA/Misc/Structures/IP.hh
vsmta.o: ../../../../include/MTA/Misc/Structures/MailAddress.hh
vsmta.o: ../../../../include/MTA/Misc/Structures/IP.hh
vsmta.o: ../../../../include/MTA/Misc/Time/Timeval.hh ../../../../include/MTA/Debug.hh
vsmta.o: ../../../../include/MTA/SystemMap.hh
vsmta.o: ../../../../include/MTA/Misc/Structures/IP.hh
vsmta.o: ../../../../include/MTA/Misc/Structures/Computer.hh
vsmta.o: ../../../../include/MTA/Misc/Structures/ModuleInfo.hh
vsmta.o: ../../../../include/MTA/Misc/Time/Time.hh ../../../../include/sparrow/dbglib.h
vsmta.o: vsmta.hh ../../../../include/MTA/DGC_MODULE.hh
vsmta.o: ../../../../include/MTA/Misc/Time/Timeval.hh VState.hh
vsmta.o: ../../../../include/MTA/Misc/Mail/Mail.hh imu.h gps.h serial.h
vsmta.o: magnetometer.h
crc32.o: crc32.h
throttle.o: vehports.h brake2.h parallel.h throttle.h
throttle.o: ../../../../include/sparrow/dbglib.h
throttle.o: ../../../../include/MTA/Misc/Time/Time.hh
brake2.o: brake2.h parallel.h throttle.h serial.h vehports.h
brake2.o: ../../../../include/sparrow/dbglib.h ../../../../include/MTA/Misc/Time/Time.hh
cruise.o: cruise.h LUtable.h VState.hh ../../../../include/MTA/Misc/Time/Timeval.hh
cruise.o: ../../../../include/MTA/Misc/Mail/Mail.hh imu.h gps.h serial.h
cruise.o: magnetometer.h
steer.o: steer.h serial.h ../../../../include/sparrow/dbglib.h
steer.o: ../../../../include/MTA/Misc/Time/Time.hh
LUtable.o: LUtable.h
OBDII.o: serial.h OBDII.h vehports.h
VStarPackets.o: VStarPackets.hh ../../../../include/MTA/Kernel.hh
VStarPackets.o: ../../../../include/MTA/Friends.hh ../../../../include/MTA/Modules.hh
VStarPackets.o: ../../../../include/MTA/DGC_MODULE.hh
VStarPackets.o: ../../../../include/MTA/KernelDef.hh
VStarPackets.o: ../../../../include/MTA/Misc/Structures/ModuleInfo.hh
VStarPackets.o: ../../../../include/MTA/Misc/Structures/MailAddress.hh
VStarPackets.o: ../../../../include/MTA/Misc/Socket/Socket.hh
VStarPackets.o: ../../../../include/MTA/Misc/File/File.hh
VStarPackets.o: ../../../../include/MTA/Misc/Time/Timeval.hh
VStarPackets.o: ../../../../include/MTA/Misc/File/File.hh
VStarPackets.o: ../../../../include/MTA/Misc/Mail/Mail.hh
VStarPackets.o: ../../../../include/MTA/KernelDef.hh
VStarPackets.o: ../../../../include/MTA/Misc/Pipes/BufferedPipe.hh
VStarPackets.o: ../../../../include/MTA/Misc/Pipes/Pipe.hh
VStarPackets.o: ../../../../include/MTA/Misc/Memory/SharedMemoryContainer.hh
VStarPackets.o: ../../../../include/MTA/Misc/Structures/IP.hh
VStarPackets.o: ../../../../include/MTA/Misc/Structures/MailAddress.hh
VStarPackets.o: ../../../../include/MTA/Misc/Structures/IP.hh
VStarPackets.o: ../../../../include/MTA/Misc/Time/Timeval.hh
VStarPackets.o: ../../../../include/MTA/Debug.hh ../../../../include/MTA/SystemMap.hh
VStarPackets.o: ../../../../include/MTA/Misc/Structures/IP.hh
VStarPackets.o: ../../../../include/MTA/Misc/Structures/Computer.hh
VStarPackets.o: ../../../../include/MTA/Misc/Structures/ModuleInfo.hh
VStarPackets.o: VStarDevices.hh VState.hh
VStarPackets.o: ../../../../include/MTA/Misc/Time/Timeval.hh
VStarPackets.o: ../../../../include/MTA/Misc/Mail/Mail.hh imu.h gps.h serial.h
VStarPackets.o: magnetometer.h VDrive.hh VStarMessages.hh
VStarPackets.o: ../../../../include/MTA/Modules.hh
estop.o: estop.h parallel.h vehports.h ../../../../include/sparrow/dbglib.h
lateral.o: lateral.h 
transmission.o: transmission.h steer.h serial.h
transmission.o: ../../../../include/sparrow/dbglib.h
transmission.o: ../../../../include/MTA/Misc/Time/Time.hh parallel.h
statesim.o: ../../../../include/MTA/Modules.hh vsmta.hh
statesim.o: ../../../../include/MTA/DGC_MODULE.hh
statesim.o: ../../../../include/MTA/Misc/Mail/Mail.hh
statesim.o: ../../../../include/MTA/KernelDef.hh
statesim.o: ../../../../include/MTA/Misc/File/File.hh
statesim.o: ../../../../include/MTA/Misc/Time/Timeval.hh
statesim.o: ../../../../include/MTA/Misc/Pipes/BufferedPipe.hh
statesim.o: ../../../../include/MTA/Misc/Pipes/Pipe.hh
statesim.o: ../../../../include/MTA/Misc/Memory/SharedMemoryContainer.hh
statesim.o: ../../../../include/MTA/Misc/Structures/IP.hh
statesim.o: ../../../../include/MTA/Misc/Structures/MailAddress.hh
statesim.o: ../../../../include/MTA/Misc/Structures/IP.hh
statesim.o: ../../../../include/MTA/Misc/Structures/ModuleInfo.hh
statesim.o: ../../../../include/MTA/Misc/Structures/MailAddress.hh
statesim.o: ../../../../include/MTA/Misc/Time/Timeval.hh VState.hh
statesim.o: ../../../../include/MTA/Misc/Mail/Mail.hh imu.h gps.h serial.h
statesim.o: magnetometer.h ../../../../include/MTA/Kernel.hh
statesim.o: ../../../../include/MTA/Friends.hh ../../../../include/MTA/Modules.hh
statesim.o: ../../../../include/MTA/DGC_MODULE.hh ../../../../include/MTA/KernelDef.hh
statesim.o: ../../../../include/MTA/Misc/Socket/Socket.hh
statesim.o: ../../../../include/MTA/Misc/File/File.hh
statesim.o: ../../../../include/MTA/Misc/Time/Timeval.hh ../../../../include/MTA/Debug.hh
statesim.o: ../../../../include/MTA/SystemMap.hh
statesim.o: ../../../../include/MTA/Misc/Structures/IP.hh
statesim.o: ../../../../include/MTA/Misc/Structures/Computer.hh
statesim.o: ../../../../include/MTA/Misc/Structures/ModuleInfo.hh
statesim.o: ../../../../include/LatLong.h ../../../../bob/include/kfilter/kfilter.h
statesim.o: ../../../../bob/include/kfilter/Kalmano.h ../../../../bob/include/kfilter/Imu.h
statesim.o: ../../../../bob/include/kfilter/Global.h statefilter.hh
statefilter.o: statefilter.hh gps.h serial.h ../../../../include/LatLong.h VState.hh
statefilter.o: ../../../../include/MTA/Misc/Time/Timeval.hh
statefilter.o: ../../../../include/MTA/Misc/Mail/Mail.hh imu.h magnetometer.h
parallel.o: parallel.h vehports.h ../../../../include/sparrow/dbglib.h
vdrive.o: vdrive.h ../../../../include/MTA/Modules.hh ../../../../include/MTA/Kernel.hh
vdrive.o: ../../../../include/MTA/Friends.hh ../../../../include/MTA/Modules.hh
vdrive.o: ../../../../include/MTA/DGC_MODULE.hh ../../../../include/MTA/KernelDef.hh
vdrive.o: ../../../../include/MTA/Misc/Structures/ModuleInfo.hh
vdrive.o: ../../../../include/MTA/Misc/Structures/MailAddress.hh
vdrive.o: ../../../../include/MTA/Misc/Socket/Socket.hh
vdrive.o: ../../../../include/MTA/Misc/File/File.hh
vdrive.o: ../../../../include/MTA/Misc/Time/Timeval.hh
vdrive.o: ../../../../include/MTA/Misc/File/File.hh
vdrive.o: ../../../../include/MTA/Misc/Mail/Mail.hh ../../../../include/MTA/KernelDef.hh
vdrive.o: ../../../../include/MTA/Misc/Pipes/BufferedPipe.hh
vdrive.o: ../../../../include/MTA/Misc/Pipes/Pipe.hh
vdrive.o: ../../../../include/MTA/Misc/Memory/SharedMemoryContainer.hh
vdrive.o: ../../../../include/MTA/Misc/Structures/IP.hh
vdrive.o: ../../../../include/MTA/Misc/Structures/MailAddress.hh
vdrive.o: ../../../../include/MTA/Misc/Structures/IP.hh
vdrive.o: ../../../../include/MTA/Misc/Time/Timeval.hh ../../../../include/MTA/Debug.hh
vdrive.o: ../../../../include/MTA/SystemMap.hh
vdrive.o: ../../../../include/MTA/Misc/Structures/IP.hh
vdrive.o: ../../../../include/MTA/Misc/Structures/Computer.hh
vdrive.o: ../../../../include/MTA/Misc/Structures/ModuleInfo.hh
vdrive.o: ../../../../include/MTA/Misc/Time/Time.hh vdmta.hh
vdrive.o: ../../../../include/MTA/DGC_MODULE.hh
vdrive.o: ../../../../include/MTA/Misc/Time/Timeval.hh VDrive.hh
vdrive.o: ../../../../include/MTA/Misc/Mail/Mail.hh ../../../../bob/include/Constants.h
vdrive.o: ../../../../bob/include/LadarConstants.h ../../../../bob/include/VehicleConstants.h
vdrive.o: ../../../../bob/include/SimulatorConstants.h ../../../../bob/include/RACEConstants.h
vdrive.o: VState.hh imu.h gps.h serial.h magnetometer.h
vdrive.o: ../../../../include/sparrow/display.h ../../../../include/sparrow/dbglib.h
vdrive.o: ../../../../include/sparrow/flag.h ../../../../include/sparrow/keymap.h
vdrive.o: vehports.h VStarPackets.hh VStarDevices.hh joystick.h
vdrive.o: transmission.h steer.h brake2.h parallel.h throttle.h cruise.h
vdrive.o: LUtable.h vddisp.h vdparm.h
vdrive.o: lateral.h
vstate.o: ../../../../include/sparrow/display.h ../../../../include/sparrow/dbglib.h
vstate.o: ../../../../include/sparrow/channel.h vehports.h
vstate.o: ../../../../include/MTA/Modules.hh vsmta.hh
vstate.o: ../../../../include/MTA/DGC_MODULE.hh ../../../../include/MTA/Misc/Mail/Mail.hh
vstate.o: ../../../../include/MTA/KernelDef.hh ../../../../include/MTA/Misc/File/File.hh
vstate.o: ../../../../include/MTA/Misc/Time/Timeval.hh
vstate.o: ../../../../include/MTA/Misc/Pipes/BufferedPipe.hh
vstate.o: ../../../../include/MTA/Misc/Pipes/Pipe.hh
vstate.o: ../../../../include/MTA/Misc/Memory/SharedMemoryContainer.hh
vstate.o: ../../../../include/MTA/Misc/Structures/IP.hh
vstate.o: ../../../../include/MTA/Misc/Structures/MailAddress.hh
vstate.o: ../../../../include/MTA/Misc/Structures/IP.hh
vstate.o: ../../../../include/MTA/Misc/Structures/ModuleInfo.hh
vstate.o: ../../../../include/MTA/Misc/Structures/MailAddress.hh
vstate.o: ../../../../include/MTA/Misc/Time/Timeval.hh VState.hh
vstate.o: ../../../../include/MTA/Misc/Mail/Mail.hh imu.h gps.h serial.h
vstate.o: magnetometer.h ../../../../include/MTA/Kernel.hh
vstate.o: ../../../../include/MTA/Friends.hh ../../../../include/MTA/Modules.hh
vstate.o: ../../../../include/MTA/DGC_MODULE.hh ../../../../include/MTA/KernelDef.hh
vstate.o: ../../../../include/MTA/Misc/Socket/Socket.hh
vstate.o: ../../../../include/MTA/Misc/File/File.hh
vstate.o: ../../../../include/MTA/Misc/Time/Timeval.hh ../../../../include/MTA/Debug.hh
vstate.o: ../../../../include/MTA/SystemMap.hh
vstate.o: ../../../../include/MTA/Misc/Structures/IP.hh
vstate.o: ../../../../include/MTA/Misc/Structures/Computer.hh
vstate.o: ../../../../include/MTA/Misc/Structures/ModuleInfo.hh
vstate.o: ../../../../include/LatLong.h ../../../../bob/include/kfilter/kfilter.h
vstate.o: ../../../../bob/include/kfilter/Kalmano.h ../../../../bob/include/kfilter/Imu.h
vstate.o: ../../../../bob/include/kfilter/Global.h statefilter.hh VStarPackets.hh
vstate.o: VStarDevices.hh VDrive.hh ../../../../include/frames/rot_matrix.hh
vstate.o: ../../../../include/frames/CMatrix.hh ../../../../include/frames/frames.hh
vstate.o: OBDII.h vsdisp.h
vdmta.o: ../../../../include/MTA/Kernel.hh ../../../../include/MTA/Friends.hh
vdmta.o: ../../../../include/MTA/Modules.hh ../../../../include/MTA/DGC_MODULE.hh
vdmta.o: ../../../../include/MTA/KernelDef.hh
vdmta.o: ../../../../include/MTA/Misc/Structures/ModuleInfo.hh
vdmta.o: ../../../../include/MTA/Misc/Structures/MailAddress.hh
vdmta.o: ../../../../include/MTA/Misc/Socket/Socket.hh
vdmta.o: ../../../../include/MTA/Misc/File/File.hh
vdmta.o: ../../../../include/MTA/Misc/Time/Timeval.hh
vdmta.o: ../../../../include/MTA/Misc/File/File.hh
vdmta.o: ../../../../include/MTA/Misc/Mail/Mail.hh ../../../../include/MTA/KernelDef.hh
vdmta.o: ../../../../include/MTA/Misc/Pipes/BufferedPipe.hh
vdmta.o: ../../../../include/MTA/Misc/Pipes/Pipe.hh
vdmta.o: ../../../../include/MTA/Misc/Memory/SharedMemoryContainer.hh
vdmta.o: ../../../../include/MTA/Misc/Structures/IP.hh
vdmta.o: ../../../../include/MTA/Misc/Structures/MailAddress.hh
vdmta.o: ../../../../include/MTA/Misc/Structures/IP.hh
vdmta.o: ../../../../include/MTA/Misc/Time/Timeval.hh ../../../../include/MTA/Debug.hh
vdmta.o: ../../../../include/MTA/SystemMap.hh
vdmta.o: ../../../../include/MTA/Misc/Structures/IP.hh
vdmta.o: ../../../../include/MTA/Misc/Structures/Computer.hh
vdmta.o: ../../../../include/MTA/Misc/Structures/ModuleInfo.hh
vdmta.o: ../../../../include/MTA/Misc/Time/Time.hh
vdmta.o: ../../../../include/MTA/Misc/Time/Timeval.hh
vdmta.o: ../../../../include/sparrow/dbglib.h vdrive.h steer.h brake2.h parallel.h
vdmta.o: throttle.h vdmta.hh ../../../../include/MTA/DGC_MODULE.hh VDrive.hh
vdmta.o: ../../../../include/MTA/Misc/Mail/Mail.hh VState.hh imu.h gps.h serial.h
vdmta.o: magnetometer.h VStarPackets.hh VStarDevices.hh
