/*
 * braketest.c - simple test program for the brakes
 *
 * RMM, 24 Feb 04
 *
 * This is a test program that is used to check out the brake device
 * driver.  Not needed for the race.
 */
#include <stdlib.h>
#include <pthread.h>
#include "brake2.h"
#include "vehports.h"
#include "parallel.h"
#include "sparrow/dbglib.h"

pthread_t brake_thread;			/* brake thread */
int brake_flag = 0;			/* enabled flag */
extern int brake_case;

main(int argc, char **argv)
{
  char inpbuf[100];
  double pos;

  /* Turn on debugging */
  dbg_flag = dbg_outf = dbg_all = 1;

  /* Open up brake/throttle parallel port */
  pp_init(PP_GASBR, PP_DIR_IN);

# ifdef BRAKE_OLD
  brake_open(5);
# else
  brake_open(6);
# endif
  /* brake_calibrate(); */

  while (1) {
    double curpos;

    printf("cur = %g, cmd: ", brake_pot_current);
    scanf("%s", inpbuf);

    switch (inpbuf[0]) {
    case 'q':				/* all done */
    case 'Q':
      brake_close();
      return 0;

    case '?':				/* check brake status */
      int status;
#ifdef BRAKE_OLD
      brake_status(&status);
#endif
      printf("brake_case = %d\n", brake_case);
      printf("brake_curpos = %g\n", brake_pot_current);
      break;

    case 'c':				/* clear polling status register */
#ifdef BRAKE_OLD
      brake_clrpsw(0xffff);
#else
      printf("not implemented");
#endif
      break;

    case 'C':
      brake_calibrate();
      break;

    case 'p':				/* print the position */
      brake_read(&curpos);
      printf("pos = %g\n", curpos);
      break;

    case 'r':
      brake_reset();
      break;

      /* 
       * Reset gas spring 
       *
       * This command uses the brakepot to reset the gas spring.  It
       * commands a slow velocity and then monitors the pot until that 
       * position is reached.  Basically the same functionality as 
       * brake_home(), except using different pot settings.
       */
#   ifndef BRAKE_OLD
    case 'G': {
#     define MAX_RESET_COUNT 100
#     define RESET_CYCLE_USEC 100000
#     define BRAKE_RESET_POS 0.75
#     define BRAKE_RESET_VEL 1000000
      int count = 0;			/* counter to avoid hanging */
      double curpos;

      brake_read(&curpos); printf("Current position = %g\n", curpos);
      printf("Move brake to %g (y/n)? ", BRAKE_RESET_POS);
      scanf("%s", inpbuf);
      if (inpbuf[0] != 'y') break;

      while (count++ < MAX_RESET_COUNT) {
	int tries = 0;
	if (brake_read(&curpos) < 0) { printf("can't read position"); break; }

	if (curpos < BRAKE_RESET_POS) {
	  /* Set the velocity of the brake to a positive value */
	  brake_move_vel(BRAKE_RESET_VEL);
	  while (brake_read(&curpos) >= 0 && curpos < BRAKE_RESET_POS &&
		 tries++ < 100) {
	    usleep(BRAKE_CYCLE_USEC);	/* let the brake move a bit */
	  }
	  break;			/* done moving */
	} else {
	  brake_move_vel(-BRAKE_RESET_VEL);
	  /* Set the velocity of the brake to a positive value */
	  while (brake_read(&curpos) >= 0 && curpos > BRAKE_RESET_POS &&
		 tries++ < 100) {
	    usleep(BRAKE_CYCLE_USEC);	/* let the brake move a bit */
	  }
	  break;			/* done moving */
	}
      }
      /* Set the velocity to zero */
      brake_move_vel(0);
      break;
    }
#endif

    case 'h':
      brake_home();
      break;

    case 't':				/* turn on servo thread */
      pthread_create(&brake_thread, NULL, brake_servo, (void *) &brake_flag);
      dbg_info("thread started");
      break;

    case 'e':      brake_flag = 1;	break;
    case 'd':      brake_flag = 0;	break;
    case 'k':	   brake_flag = -1;	break;

    default:
      pos = atof(inpbuf);
#ifdef BRAKE_OLD
      if (brake_flag != 1) {
	brake_setmav(pos, 1.0, 1.0);
      } else {
	brake_write(pos, 1.0, 1.0);
      }
#else
      printf("setting velocity to %g for 1 sec\n", pos);
      brake_move_vel(pos);
      sleep(1);
      brake_move_vel(0);
      double curpos; brake_read(&curpos);
      printf("pos = %g\n", curpos);

#endif
    }
  }

  brake_close();
  return 0;
}
