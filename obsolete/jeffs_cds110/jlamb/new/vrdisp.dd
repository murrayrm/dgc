/*
 * vrdisp.h - vremote display screen for Tahoe
 *
 * RMM, 30 Dec 03
 *
 */

/* Local function declarations */
int user_quit(long);
int user_cap_toggle(long);

extern DD_IDENT parmtbl[];	/* Parameter table */
extern int steer_cmdangle;	/* Commanded steering angle (steer.c) */
extern double brk_current_abs_pos_buf, brk_current_acc_buf, brk_current_vel_buf;
extern double brk_current_brake_pot, brk_pot_voltage;
extern int brk_case;
extern int brk_count;

%%
VREMOTE 2.1a	        J: %joyc  S: %strc  A: %accc  M: %mtac
Mode: D=%md S=%ms	

         Off    Ref   Act   Gain  Cmd	      | Joy:  %jx  %jy    %jbut
Drive   %aref  %vref %vact %dgain %gc   %bc   |
Steer   %pref        %pact %sgain %sc         |

Axis   IMU      GPS      |  KF     Vel     | Configuration files:
 X/N   %imdx    %gpx     |%kfx     %kfvx   |  Chn config: vdrive.dev
 Y/E   %imdy    %gpy     |%kfy     %kfvy   |  Dump file %dp %dumpfile
 Z/U   %imdz    %gpz     |%kfz     %kfvz   |  Brake: pos: %brkpos vel: %brkvel 
 R     %imdr    -------  |%kfr             |         cur: %brkcur cas: %brkcas
 P     %imdp    M %magn  |%kfp             |  estop: %estopval
 Y     %imdw             |%kfw             |  Do: %estop 
                                           |

Drivers	    Flag    Cmd     Cal    Zero	 Functions   Flag   
  Brake	    %bflg   %bcmd   %bcal  %bzer   Cruise    %cflg 
  Throttle  %tflg   %tcmd   %tcal        Modes
  Steer	    %sflg   %scmd   %scal  %sz     O = off     M = manual (human)
                                           R = remote  A = autonomous (MTA)
%QUIT   %ACDV   %CAPTURE
%%

# <F1> manual	    <F5> toggle capture	    <F9>  reset defaults
# <F2> remote	    <F6> dump data	    <F10> zero counters
# <F3> autonomous	    <F7> start trajectory   <ESC> control off

tblname: vddisp;
bufname: vddbuf;

#
# Labels and counters
#
# These entries describe the top line of the screen
#
# short: %joyc vd.joystick.count "%5d" -ro;
# short: %strc vd.steer.count "%5d" -ro;
# short: %accc vd.accel.acc_count "%5d" -ro;
short: %mtac mta_counter "%5d" -ro;
# short: %brk vd.brake.BrakeCounter "%5d" -ro;

#
# Control section
#
# This section of the display shows what is being commanded and
# send to the actuators.  Roughly corresponds to the control loop
# for the vehicle

# double: %aref vd.accel_off "%5.2f";	# acceleration axis offset
# double: %pref vd.steer_off "%5.2f";	# steering axis offset
# double: %vref vd.speed_ref "%5.2f";	# velocity reference

# double: %dgain vd.cruise_gain "%5.2f";	# cruise control gain
# double: %bc vd.brk_pos "%5.2f";		# commanded brake position
# double: %gc vd.thr_pos "%5.2f";		# commanded throttle position
# double: %sc vd.str_angle "%5.2f";	# commanded steering angle

#
# Joystick position
#

double:	%jx		vd.joystick.X		"%5.0f"		-ro;
double:	%jy		vd.joystick.Y		"%5.0f"		-ro;
short: %jbut		vd.joystick.Buttons	"%d"		-ro;

#
# Vstate entries
#
double: %gpx vehstate.gpsdata.lat "%7.4f" -ro;
double: %gpy vehstate.gpsdata.lng "%7.4f" -ro;
double: %gpz vehstate.gpsdata.altitude "%7.4f" -ro;

double: %imdx vehstate.imudata.dvx "%7.4f" -ro;
double: %imdy vehstate.imudata.dvy "%7.4f" -ro;
double: %imdz vehstate.imudata.dvz "%7.4f" -ro;
double: %imdr vehstate.imudata.dtx "%7.4f" -ro;
double: %imdp vehstate.imudata.dty "%7.4f" -ro;
double: %imdw vehstate.imudata.dtz "%7.4f" -ro;

double: %kfx vehstate.kf_lat "%7.2f";
double: %kfy vehstate.kf_lng "%7.2f";
float: %kfz vehstate.Altitude "%7.2f";
float: %kfvx vehstate.Vel_N "%5.2f";
float: %kfvy vehstate.Vel_E "%5.2f";
float: %kfvz vehstate.Vel_U "%5.2f";
float: %kfr vehstate.Roll "%7.2f";
float: %kfp vehstate.Pitch "%7.2f";
float: %kfw vehstate.Yaw "%7.2f";
float: %magn vehstate.magreading.heading "%5.2f";

#
#estop button
#
# button: %estop "ESTOP" user_estop_call; #button to activate estop_pause

#
# Device status 
#
short: %bflg vd.brake.BrakeState "%1d";
double: %bcmd vd.brake.Position "%5.2f";	# commanded brake position
# button: %bzer "RST" brake_reset_cbe;	# cbe for brake reset

short: %tflg vd.throttle.ThrottleState "%1d";
double: %tcmd vd.throttle.Position "%5.2f";	# commanded throttle position

# short: %cflg vd.cruise_flag "%1d";

short: %sflg vd.steer.SteerState "%1d";
double: %scmd vd.steer.Steer "%5.2f";
# button: %scal "CAL" steer_cal;
# button: %sz "ZERO" steer_setzero;

#
# File capture
#
# string: %dumpfile vd.dumpfile "%s";
# short: %dp vd.capflag "[%d]:";

#
# Button bar (bottom of screen)
#

button:	%QUIT	"QUIT"	user_quit	-idname=QUITB;
button:	%ACDV	"PARAM" dd_usetbl_cb -userarg=parmtbl;
# button: %STRBT "Steer:" dd_usetbl_cb -userarg=strtbl;
# button: %HELP "Help" dd_usetbl_cb -userarg=helptbl;
# button:	%CAPTURE "CAPTURE" user_cap_toggle;

# Operating mode
short: %ms vd.mode_steer "%c" -manager=vdrive_mode;
short: %md vd.mode_drive "%c" -manager=vdrive_mode;

#
# Debugging section
#
# This portion of the screen can be used for local debugging
# OK to delete code that is here in your local version
#




