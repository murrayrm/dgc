#include "ladar_power.h"
#include <stdlib.h>
#include <stdio.h>
#include "MTA/Misc/Time/Time.hh"
#include <iostream>
using namespace std;

int main() {
  
  ladar_pp_init();

  int i;  
  cin>>i;
  while(i!=0) {

    if (i==1) {
      ladar_off(LADAR_BUMPER);
      cout<<"Bumper ladar is off.\n"<<endl;
    }

    if (i==2) {
      ladar_on(LADAR_BUMPER);
      cout<<"Bumper ladar is on.\n"<<endl;
    }
    cin>>i;
  }
  printf("DONE\n");
  return 0;

}
