I've found Newmat to be much easier to use than the more extensive Lapack-type 
libraries for matrix operations. --Lars, 8 Oct 04.  Original README file below. 

This C++ library is intended for scientists and engineers who need to 
manipulate a variety of types of matrices using standard matrix operations. 
Emphasis is on the kind of operations needed in statistical calculations such 
as least squares, linear equation solve and eigenvalues.

- From http://www.robertnz.net/nm_intro.htm



ReadMe file for newmat11 - beta version - 19 June, 2004
-------------------------------------------------------------

This is a beta version of newmat11.

Documentation is in nm11.htm.

This library is freeware and may be freely used and distributed.

To contact the author please email robert@statsresearch.co.nz


