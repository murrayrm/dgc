@c -*- texinfo -*-

@ifnottex
@node Top,,,(DIR)
@top Vehlib Reference Manual

Vehlib is the embedded systems code for the Chevy Tahoe.  

@menu
* intro::               Introduction to the vehlib library
* vdrive::              The vdrive driving control program
* steer::               Steering subsystem
@end menu
@end ifnottex

@node intro,,,Top
@chapter Library Overview

@include vdrive.txi
@include steer.txi

@contents
@bye
