/*
 * brake.h - header file for brake actuation
 * 
 * Sue Ann Hong, Jeremy G.
 *
 * RMM, 2 Jan 04: changes to incorporate brake acutation into vdrive
 *   * merged TLL.h into this file; may split back out later
 *   * set up to use local version of serial.h; elimited Constants.h
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include "serial.h"
#include "vehports.h"

//Brake Constants
#define BRAKE_ID 16
#define BRAKEBAUD B57600
// TODO: THIS IS SLIGHTLY DIRTY.  but we want to keep all constants,
// etc in Constants.h
#define BRAKEPORT BRAKE_SERIAL_PORT
// serial port defined in include/constants.h

//Brake Message Constants
#define BRAKE_ACK "* 10\n"
//Brake Movement Constant Values
#define STOP_ENABLE_ABS 0
#define STOP_STATE_ABS 0
#define MAX_ACC 1073741.0
#define MIN_ACC 1.0
#define MAX_VEL 2147483647.0
#define MIN_VEL 1.0
/* Position constants for set_brake_abs_pos(). All should be in counts.
 * ZERO_POS: 
 * MAX_POS: Corresponds to our_max_pos, just in counts. Maximum position used
 *          in set_brake_abs_pos(). Same role as max_count used in
 *          set_brake_abs_pos_pot().
 * MIN_POS:  
 */
#define ZERO_POS -120000
#define MAX_POS -230000  
#define MIN_POS 0
#define MAX_MOV_TEN 13440   //4.2"/sec * 32000 steps/in / 10Hz =max movement in 10Hz control loop
#define BRAKE_POS_TOLERANCE .06/4  // =tolerance of .06 out of 0-4 inches, scaled to 0-1


/* send_brake_command()
 * Description: Sends the brake a command of command_num with parameters of
 *  length
 */
int send_brake_command(int command_num, char *parameters, int length);

/* receive_brake_command()
 * Return value: The response from sending a brake command
 * Description: Sends the brake a command of command_num with parameters of
 *  length, and then stores the response in buffer of length buf_length (or
 *  times out in timeout seconds)
 */
int receive_brake_command(int command_num, char *parameters, int length,
			  char *buffer, int buf_length, int timeout);

/* brake_ready()
 * Return values: 0, if the brake is ready to receive a command
 *                -1, otherwise
 * Description: Sends a polling command to see if the brake is ready
 *  after a command has been sent. Also handles holding/moving error.
 */
int brake_ready();

/* Init_brake()
 * Initializes the brake by simply initializing its serial port. If wish to
 *  use the brake pot or force meter, must call initBrakeSensors().
 */
int init_brake();

/* reset_brake()
 * Description: Resets the brake by sending the reset command
 *  and then waits until the brake is back up and running again
 *  before returning.
 * Note: THIS IS A BLOCKING FUNCTION.
 */
int reset_brake();

/* calib_brake()
 * Description: User can calibrate the brake using this function. It prompts a
 *  few options so the user can keep moving the brake actuator, set the zero
 *  position or sets the full position. Modifies min_pot, max_pot,
 *  min_counts, max_counts.
 *  We use set_brake_rel_pos in this fn, so we do not need the brake pot.
 * Calls: set_brake_rel_pos()
 * Notes: "zero position" = full-out, but no braking
 *        "full position" = fully retracted, but full braking
 */
int calib_brake();

/* start_break_control_loop()
 * Description: Forks off the brake control loop which updates the commanded 
 * brake position at 10Hz.  It also uses the velocity mode movement commands 
 * for the actuator to allow near real-time monitoring of new commanded brake
 * positions.
 */
int start_brake_control_loop(int* brake_enabled);


/* set_brake_abs_pos()
 * Inputs: abs_pos   0 = calibrated zero (no braking)
 *                   1 = fully retracted (full braking)
 *         vel, acc  0 to 1
 * Description: Sets the brake actuator, taking the first argument as the
 *  desired position in the absolute scale. The absolute scale is defined by 
 *  constants set in calib_brake().
*/
int set_brake_abs_pos(double abs_pos, double vel, double acc);

/* set_brake_abs_pos_limited()
 * This function is identical to set_brake_abs_pos(), except that 
 * it will never move more than MAX_MOV_TEN, to insure that it will return
 * quickly, making it suitable for a 10hz control loop.
 * An integrator is used to compensate for the limiting, though i may
 * eliminate the integrator in a few hours.
 */
int set_brake_abs_pos_limited(double abs_pos, double vel, double acc);

/* set_brake_abs_pos_pot()
 * Inputs: abs_pos   0 = calibrated zero (no braking)
 *                   1 = fully retracted (full braking)
 *         vel, acc  0 to 1
 * Description: Sets the brake actuator, taking the first argument as the
 *  desired position in the absolute scale, defined by constants set in 
 *  calib_brake(). 'abs_pos' is on the 0-1 scale, and goes from no braking to
 *  full braking (It does NOT go from actuator fully out to fully in, unless
 *  that's how the actuator was calibrated.)
 * Calls: set_brake_rel_pos_counts()
 */
int set_brake_abs_pos_pot(double abs_pos, double vel, double acc);

/* set_brake_abs_pos_pot_limited()
 * This function is identical to set_brake_abs_pos_pot, except that it is
 * limited in the same way as set_brake_abs_pos_limited.
 * no integrator is used*/
int set_brake_abs_pos_pot_limited(double abs_pos, double vel, double acc);

/* set_brake_abs_pos_vel()
 * Inputs: abs_pos   0 = calibrated zero (no braking)
 *                   1 = fully retracted (full braking)
 *         vel, acc  0 to 1
 * Description: Sets the brake actuator, taking the first argument as the
 *  desired position in the absolute scale, defined by constants set in
 *  calib_brake(). 'abs_pos' is on the 0-1 scale, and goes from no braking to
 *  full braking (It does NOT go from actuator fully out to fully in, unless
 *  that's how the actuator was calibrated.)  This function uses the velocity 
 *  mode movement command to move the brake so that we can get a reasonably
 *  paced actuator movement.
 */
int set_brake_abs_pos_vel(double abs_pos, double vel, double acc);

/* set_brake_rel_pos()
 * Arguments: rel_pos  -1 to 1 (negative to move out)
 *            vel, acc  0 to 1
 * Description: Sets the brake actuator, taking the first argument as the
 *  desired displacement from the current position. 
 */
int set_brake_rel_pos(double rel_pos, double vel, double acc);

/* set_brake_rel_pos_counts()
 * Arguments: rel_pos  -1 to 1 (negative to move out)
 *            vel, acc  0 to 1
 * Description: Sets the brake actuator, taking the first argument as the
 *  desired displacement from the current position. This one doesn't depend
 *  on calibrated values. Just moves the brake with the count values
 */
int set_brake_rel_pos_counts(double rel_pos, double vel, double acc);

/* get_brake_pos()
 * Description: Returns the current brake position in counts,
 *  which it gets from the brake controller,
 *  which keeps the last commanded value?? It is different from getting the
 *  pot value, but how? Pot value is physical. count value is software?
 */
int get_brake_pos();


// Helper functions for scaling and limiting numbers
double in_range_double(double val, double min_val, double max_val);
double scale_double(double val, double old_min_val, double old_max_val, double new_min_val, double new_max_val);


/****************************************************************************/
/* TLL.h - header file for TLL.cc (brake force meter, linear pot)
   Revision History: 8/29/2003  Created                      Sue Ann Hong
 */

//#include "parallel.h"
//#include "../team/parallel/parallel.h"
#include "parallel.h"        // compile with ../parallel/parallel.c

#ifndef TLL_H
#define TLL_H

/* Parallel Port set-up
 Gas:
   Data lines (in): none
   Control lines (out):
  pin14  not used
  pin01  digipot CLK
  pin16  digipot direction (UD)
  pin17  digipot CS [pin only used by this mode]
*/

/* Constants */
// Brake actuator pot control lines
static int BPOT_RC = PP_PIN01;
static int BPOT_CS = PP_PIN16;
static int BPOT_D8 = PP_PIN10;
static int BPOT_D9 = PP_PIN11;
static int BPOT_D10 = PP_PIN12;
static int BPOT_D11 = PP_PIN13;
// Brake act pot constants
static int BPOT_TBUSY = 8;               // max t_BUSY in us
static double BP_CORR_FACTOR = -0.06862; // correction for error in adc
static double BP_NUM_DIV = 2047;        // 12-bit signed ADC => 11-bit positive
static double BP_VREF_LOW = 0;          // in V, according to ADC chip spec
static double BP_VREF_HIGH = 10;        // in V, according to ADC chip spec
static double POT_VHIGH = 9.71;         // 9.73V according to multimeter
                                        // 9.71V (1988,1987) according to s/w
static double BP_NUM_HIGH = 1987;       /* Highest number output by ADC box,
                                         * Corresponds to POT_VHIGH */
/* BP_MIN & BP_MAX: Used in getBrakePot() to scale brake pot reading. 
 *                  These values provide no safety. If want safety, then should
 *                  adjust min and max values in higher brake functions like 
 *                  set_brake_pos_abs_pot(), etc. (Jan 17, 2004)
 * Current Usage: getBrakePot()
 */
static double BP_MIN = 0.0;
static double BP_MAX = 1.0;

// TLL-1K Force Sensor control lines
static int BT_OE = PP_PIN14;
static int BT_START = PP_PIN01;
static int BT_ALE = PP_PIN16;            
// TLL-1K Force Sensor constants
static double BT_CORR_FACTOR = -0.06862; /* correction for noise in amp, adc */
static double BT_VREF_HIGH = 2.5;
static double BT_VREF_LOW = 0;
static double BT_NUM_DIV = 255;                    // 8-bit ADC
static double BT_MAX_OUTPUT = 2.33*12*100/1000;   // 2.33mV/V*Vin*amp*1V/1000mV
static double BT_MAX_FORCE = 1000;

static int tllpport = PP_GASBR;

/* Reading the tension sensor & linear pot: parallel interface */
int initBrakeSensors();

/* getBrakeForce()
  Return value: (double) force on the brake pedal
*/
double getBrakeForce();

/* getBrakePot()
 * Description: Reads brake pot value from the ADC circuit and scales pot
 *              reading from [0, BP_NUM_HIGH] to [BP_MIN, BP_MAX].
 *              Any voltage constants defined above are only used to output
 *              and check if voltage read is reasonable.
 * Return value: (double) position of the brake actuator between 0 and 1
 * Revision History:  1/17/2004  Sue Ann Hong
 */
double getBrakePot();

extern int brk_lock;			/* used to lock out brake commands */
#endif /* TLL_H */
