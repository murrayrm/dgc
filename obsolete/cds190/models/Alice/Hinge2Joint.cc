/*
 *  Gazebo - Outdoor Multi-Robot Simulator
 *  Copyright (C) 2003  
 *     Nate Koenig & Andrew Howard
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
/* Desc: A hinge joint with 2 degrees of freedom
 * Author: Nate Keonig, Andrew Howard
 * Date: 21 May 2003
 * CVS: $Id: Hinge2Joint.cc,v 1.5 2003/09/14 22:40:54 natepak Exp $
 */

#include "Hinge2Joint.hh"
#include <gazebo/JointGroup.hh>


//////////////////////////////////////////////////////////////////////////////
// Constructor
Hinge2Joint::Hinge2Joint( dWorldID worldId, JointGroup *group )
  : Joint()
{
  if (group)
    this->jointId = dJointCreateHinge2( worldId, group->GetGroupId() );
  else
    this->jointId = dJointCreateHinge2( worldId, NULL );
}


//////////////////////////////////////////////////////////////////////////////
// Destructor
Hinge2Joint::~Hinge2Joint()
{
}


//////////////////////////////////////////////////////////////////////////////
// Get anchor point
void Hinge2Joint::GetAnchor1( dVector3 result ) const
{
  dJointGetHinge2Anchor( this->jointId, result );
}


//////////////////////////////////////////////////////////////////////////////
// Get the second anchor point
void Hinge2Joint::GetAnchor2( dVector3 result ) const
{
  dJointGetHinge2Anchor2( this->jointId, result );
}


//////////////////////////////////////////////////////////////////////////////
// Get first axis of rotation
void Hinge2Joint::GetAxis1( dVector3 result ) const
{
  dJointGetHinge2Axis1( this->jointId, result );
}


//////////////////////////////////////////////////////////////////////////////
// Get second axis of rotation
void Hinge2Joint::GetAxis2( dVector3 result ) const
{
  dJointGetHinge2Axis2( this->jointId, result );
}


//////////////////////////////////////////////////////////////////////////////
// Get angle of rotation about first axis
double Hinge2Joint::GetAngle1() const
{
  return dJointGetHinge2Angle1( this->jointId );
}


//////////////////////////////////////////////////////////////////////////////
// Get rate of rotation about first axis
double Hinge2Joint::GetAngle1Rate() const
{
  return dJointGetHinge2Angle1Rate( this->jointId );
}


//////////////////////////////////////////////////////////////////////////////
// Get rate of rotation about second axis
double Hinge2Joint::GetAngle2Rate() const
{
  return dJointGetHinge2Angle2Rate( this->jointId );
}


//////////////////////////////////////////////////////////////////////////////
// Get the specified parameter
double Hinge2Joint::GetParam( int parameter ) const
{
  return dJointGetHinge2Param( this->jointId, parameter );
}


//////////////////////////////////////////////////////////////////////////////
// Set the anchor point
void Hinge2Joint::SetAnchor( double x, double y, double z )
{
  dJointSetHinge2Anchor( this->jointId, x, y, z );
}


//////////////////////////////////////////////////////////////////////////////
// Set the first axis of rotation
void Hinge2Joint::SetAxis1( double x, double y, double z )
{
  dJointSetHinge2Axis1( this->jointId, x, y, z );
}


//////////////////////////////////////////////////////////////////////////////
// Set the second axis of rotation
void Hinge2Joint::SetAxis2( double x, double y, double z )
{
  dJointSetHinge2Axis2( this->jointId, x, y, z );
}


//////////////////////////////////////////////////////////////////////////////
// Set _parameter with _value
void Hinge2Joint::SetParam( int parameter, double value)
{
  dJointSetHinge2Param( this->jointId, parameter, value );
}

/////////////////////////////////////////////////////////////////////////////
// Add torques directly to Hinge2 axes
void Hinge2Joint::AddTorques( double t1, double t2 )
{
  dJointAddHinge2Torques( this->jointId, t1, t2 );
}
