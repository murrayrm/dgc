/**
 * $Id: Alice.cc 4763 2005-1-25 15:22:29Z braman $
 */

#include <assert.h>
#include <gazebo/World.hh>
#include <gazebo/WorldFile.hh>
#include <gazebo/BoxGeom.hh>
#include <gazebo/SphereGeom.hh>
#include <gazebo/CylinderGeom.hh>
#include <gazebo/WheelGeom.hh>
#include <gazebo/ModelFactory.hh>

#include "Alice.hh"
#include <stdio.h>
#include <ode/ode.h>
#define sign(x) (x/fabs(x))
#define RMIN 1.000
const double RBIG=1e6; 
const double kSteer=5; 

/////////////////////////////////////////////////////////////////////////////
// Register the model
GZ_REGISTER_PLUGIN("Alice", Alice)

//////////////////////////////////////////////////////////////////////////////
// Constructor
Alice::Alice( World *world )
  : Model( world )
{
  this->encoders[0] = 0;
  this->encoders[1] = 0;
  return;
}


//////////////////////////////////////////////////////////////////////////////
// Destructor
Alice::~Alice()
{
  return;
}


//////////////////////////////////////////////////////////////////////////////
// Load the model
int Alice::Load( WorldFile *file, WorldFileNode *node )
{
  // Create the ODE objects
  if (this->OdeLoad(file, node) != 0)
    return -1;

  // Model id
  
  this->raw_encoder_position = node->GetBool( "raw_encoder_position", false );

  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Initialize the model
int Alice::Init( WorldFile *file, WorldFileNode *node )
{
  // Initialize ODE objects
  if (this->OdeInit(file, node) != 0)
    return -1;

  // Initialize external interface
  if (this->IfaceInit() != 0)
    return -1;
  
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Load ODE objects
int Alice::OdeLoad( WorldFile *file, WorldFileNode *node )
{
  int i;
  Geom *geom;

  // Create a model where the top level of the roof is exactly 
  // 75.75 inch = 1.92405 meters from the ground. 
  // Wheel suspension and softness might screw up the precision here
  
  // The wheels
  this->wheelSep = 1.7907; //(separation?) wheel base (meters) 
  this->wheelDiam = 0.8382;  // (meters) 
  const double wheelThick = 0.3; // (meters)
  const double wheelMass = 50; // (kilos) 100lbs 
  
  // the "main" part of the chassi
  this->mainLength = 5.5; // 5.5 (meters) van (average of RB & EB)
  const double mainWidth = 2.014; // 2.014 (meters) 
  const double mainHeight = .9126 + this->wheelDiam * 0.5; // ??(meters)
  const double mainMass = 3000; // kilos (about 7500 lbs)
  
  // the top part of the chassi
  const double topLength = 0.8 * mainLength; // (meters)
//  const double topWidth = 0.9 * mainWidth;   // (meters) 
  const double topWidth =  mainWidth;   // (meters) 
  const double topHeight = 1.0;              // (meters)
  const double topMass = 1000; // (kilos) (about 2500 lbs)
    
  // Create the main body of the robot
  this->body = new Body( this->world );
  
  // the main part of the chassis
  this->bodyGeoms[0] = new BoxGeom( this->body, this->modelSpaceId, 
                                    mainLength, mainWidth, mainHeight);
  this->bodyGeoms[0]->SetRelativePosition( GzVectorSet(0, 0, 0.2 * mainHeight) );
  this->bodyGeoms[0]->SetMass( mainMass );
  this->bodyGeoms[0]->SetColor( GzColor(0.9, 0.9, 0.9) ); // almost pure white
  //this->body->AttachGeom( this->bodyGeoms[0] ); 

  // the top part of the chassi:
  this->bodyGeoms[1] = new BoxGeom( this->body, this->modelSpaceId, 
                                    topLength, topWidth, topHeight); 
//  this->bodyGeoms[1]->SetRelativePosition(GzVectorSet(-0.2*mainLength, 0, 0.7*topHeight));
  this->bodyGeoms[1]->SetRelativePosition(GzVectorSet(-0.1*mainLength, 0, 1.35*topHeight));
  this->bodyGeoms[1]->SetMass( topMass );
  this->bodyGeoms[1]->SetColor( GzColor(0.9, 0.9, 0.9) ); // almost pure white 
  //this->body->AttachGeom( this->bodyGeoms[1] );

  this->AddBody( this->body, true );
   
  // Create the wheels
  for (i = 0; i < 4; i++)
  {
    // Create a wheel
    this->wheels[i] = new Body( this->world );
    geom = new WheelGeom( this->wheels[i], this->modelSpaceId, 0.5* this->wheelDiam, 0.5 * wheelThick);
    geom->SetRelativePosition(GzVectorSet(0, 0, 0));
    geom->SetMass( wheelMass * 0.5 );
    geom->SetColor( GzColor(0.3, 0.3, 0.3) ); // dark grey
    //this->wheels[i]->AttachGeom( geom );

    // Create a white box to attach to the wheel so it's easier to see
    // when they're rotating:
    geom = new BoxGeom( this->wheels[i], this->modelSpaceId, 0.5 * this->wheelDiam, 
                        0.3 * this->wheelDiam, 0.02);
    geom->SetRelativePosition(GzVectorSet(0, 0, 0.4 * wheelThick));
    geom->SetMass( wheelMass * 0.5 );
   // geom->SetMass( wheelMass );
    geom->SetColor( GzColor(1.0, 1.0, 1.0) ); // white
    //if(i==0||i==1)
    //  geom->SetFriction1(0.1);
    //this->wheels[i]->AttachGeom( geom );

    this->AddBody( this->wheels[i] );
  }

  // position the wheels:
  this->wheels[0]->SetPosition(GzVectorSet( 0.37*mainLength, +0.5*mainWidth+0.1, -0.5*mainHeight));
  this->wheels[1]->SetPosition(GzVectorSet( 0.37*mainLength, -0.5*mainWidth-0.1, -0.5*mainHeight));
  this->wheels[2]->SetPosition(GzVectorSet(-0.37*mainLength, +0.5*mainWidth+0.1, -0.5*mainHeight));
  this->wheels[3]->SetPosition(GzVectorSet(-0.37*mainLength, -0.5*mainWidth-0.1, -0.5*mainHeight));
  
  // set the rotation for the wheels
  this->wheels[0]->SetRotation(GzQuaternFromAxis(1, 0, 0, -M_PI / 2));
  this->wheels[1]->SetRotation(GzQuaternFromAxis(1, 0, 0, +M_PI / 2));
  this->wheels[2]->SetRotation(GzQuaternFromAxis(1, 0, 0, -M_PI / 2));
  this->wheels[3]->SetRotation(GzQuaternFromAxis(1, 0, 0, +M_PI / 2));
  
  // create the stubs
/*  for (i = 0; i < 4; i++)
  {
    this->stubs[i] = new Body( this->world );
    geom = new SphereGeom( this->stubs[i], this->modelSpaceId, 0.05);
    geom->SetRelativePosition(GzVectorSet(0, 0, 0));
    geom->SetMass( wheelMass * 0.05 ); // consists mostly of air :)
    geom->SetColor( GzColor(0.1, 0.1, 0.1) ); // almost black
    //this->stubs[i]->AttachGeom( geom );
    this->AddBody(this->stubs[i]);  
  }
*/
  // position the stubs:
/*  this->stubs[0]->SetPosition(GzVectorSet( 0.37*mainLength, +0.53*mainWidth-0.1, -0.5*mainHeight));
  this->stubs[1]->SetPosition(GzVectorSet( 0.37*mainLength, -0.53*mainWidth+0.1, -0.5*mainHeight));
  this->stubs[2]->SetPosition(GzVectorSet(-0.37*mainLength, +0.53*mainWidth-0.1, -0.5*mainHeight));
  this->stubs[3]->SetPosition(GzVectorSet(-0.37*mainLength, -0.53*mainWidth+0.1, -0.5*mainHeight));
*/
  // Attach the wheels to the body
  for (i = 0; i < 4; i++)
  {
    this->sjoints[i] = new Hinge2Joint( this->world->worldId );
    //this->sjoints[i]->Attach(this->stubs[i], this->body );
    this->sjoints[i]->Attach(this->body, this->wheels[i] );
    
    GzVector ab = wheels[i]->GetPosition();
    this->sjoints[i]->SetAnchor( ab.x, ab.y, ab.z );
    this->sjoints[i]->SetAxis1(0,0,1);
    this->sjoints[i]->SetAxis2(0,1,0);

    this->sjoints[i]->SetParam( dParamSuspensionERP, 0.8 ); // 0.4
    this->sjoints[i]->SetParam( dParamSuspensionCFM, 0.0001 ); //0.8

   // this->wjoints[i] = new HingeJoint( this->world->worldId );
   // this->wjoints[i]->Attach(this->wheels[i],this->stubs[i]);
    
   // GzVector a = wheels[i]->GetPosition();
   // this->wjoints[i]->SetAnchor( a.x, a.y, a.z );
   // this->wjoints[i]->SetAxis(0,1,0);

   // this->wjoints[i]->SetParam( dParamSuspensionERP, 0.4 ); // 0.4
   // this->wjoints[i]->SetParam( dParamSuspensionCFM, 0.8 ); //0.8
      
    this->sjoints[i]->SetParam( dParamLoStop,-M_PI/6); 
    this->sjoints[i]->SetParam( dParamHiStop,+M_PI/6); 
  }
  
  // fix rear wheels for now
  this->sjoints[2]->SetParam( dParamLoStop,0);
  this->sjoints[2]->SetParam( dParamHiStop,0);
  this->sjoints[3]->SetParam( dParamLoStop,0);
  this->sjoints[3]->SetParam( dParamHiStop,0);

  this->body->SetFiniteRotationMode(1);  
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Initialize ODE
int Alice::OdeInit( WorldFile *file, WorldFileNode *node )
{
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Finalize the model
int Alice::Fini()
{
  // Finalize external interface  
  this->IfaceFini();

  // Finalize ODE objects
  this->OdeFini();

  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Finalize ODE
int Alice::OdeFini()
{
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Update model
void Alice::Update( double step )
{
  // Get commands from the external interface
  this->IfaceGetCmd();
   
  double v = this->wheelSpeed[0];// + this->wheelSpeed[1] * this->wheelSep;

  double rTurn;
  // Radius of the Turn
  if(this->wheelSpeed[1]==0.0 || this->wheelSpeed[0]==0.0)
    rTurn=RBIG;
  else
    rTurn= this->wheelSpeed[0]/this->wheelSpeed[1];

  if(fabs(rTurn)>RBIG)
    rTurn=fabs(rTurn)*RBIG/rTurn; //ftb
 
  if(fabs(rTurn) < RMIN)
    rTurn=fabs(rTurn)*RMIN/rTurn;
  
  double d=this->wheelSep/2.0;
  double steerR=atan(mainLength/(rTurn-d));
  double steerL=atan(mainLength/(rTurn+d));

  double vLB = v* ( rTurn-d)/(rTurn);
  double vRB=  v* ( rTurn+d)/(rTurn);
  double vLF = v* sqrt(( rTurn-d)*(rTurn-d)+mainLength*mainLength)/fabs(rTurn);
  double vRF = v* sqrt(( rTurn+d)*(rTurn+d)+mainLength*mainLength)/fabs(rTurn);
  
  //printf("vr= %f va= %f , rTurn=%f w0=%f w1=%f\n",this->wheelSpeed[0],this->wheelSpeed[1],rTurn,w0,w1);
 
   double angleL = this->sjoints[0]->GetAngle1();
   double angleR = this->sjoints[1]->GetAngle1();
  // printf("steerL=%f angleL=%f steerR=%f angleR=%f\n",steerL,angleL,steerR,angleR);
  
  this->sjoints[0]->SetParam(dParamVel,kSteer*(steerL-angleL));
  this->sjoints[1]->SetParam(dParamVel,kSteer*(steerR-angleR));

  this->sjoints[2]->SetParam(dParamVel, 0);
  this->sjoints[3]->SetParam(dParamVel, 0);

  // these parameters affects how easy it is to turn. The higher values, the
  // faster and sharper turning is possible.
  this->sjoints[0]->SetParam( dParamFMax, 500 ); // Clodbuster: 1.1
  this->sjoints[1]->SetParam( dParamFMax, 500 ); // Clodbuster: 1.1
  this->sjoints[2]->SetParam( dParamFMax, 580 ); // Clodbuster: 1.8
  this->sjoints[3]->SetParam( dParamFMax, 580 ); // Clodbuster: 1.8

  this->sjoints[0]->SetParam( dParamVel2,vLF / this->wheelDiam);
  this->sjoints[1]->SetParam( dParamVel2,vRF / this->wheelDiam);

  this->sjoints[2]->SetParam( dParamVel2,vLB / this->wheelDiam); 
  this->sjoints[3]->SetParam( dParamVel2,vRB / this->wheelDiam); 
 
  //printf("angle rates: axis1 %f %f",this->joints[0]->GetAngle1Rate(),this->joints[2]->GetAngle1Rate());
 
  //printf(" axis2 %f %f\n",this->joints[0]->GetAngle2Rate(),this->joints[3]->GetAngle2Rate());
  
  // These parameters seems to affect the max speed of the vehicle, the higher, the 
  // higher max speed. With high values it seems like the vehicle gets 
  // really hard to recieve new steering commands (it just keeps going in 
  // whatever direction it gained speed in, oddly enough).
  this->sjoints[0]->SetParam( dParamFMax2, 1100 ); // Clodbuster: 1.1
  this->sjoints[1]->SetParam( dParamFMax2, 1100 ); // Clodbuster: 1.1
  this->sjoints[2]->SetParam( dParamFMax2, 1100 ); // Clodbuster: 1.1
  this->sjoints[3]->SetParam( dParamFMax2, 1100 ); // Clodbuster: 1.1
  
  // Update the odometry
  this->UpdateOdometry( step );

  // Update the interface
  this->IfacePutData();

  return;
}


//////////////////////////////////////////////////////////////////////////////
// Update the odometry

void Alice::UpdateOdometry( double step )
{
  double wd, ws;
  double d1, d2;
  double dr, da;

  wd = this->wheelDiam;
  ws = this->wheelSep;

  // Average distance travelled by left and right wheels
  //d1 = step * wd * (joints[0]->GetAngle2Rate() + joints[2]->GetAngle2Rate()) / 2;
  //d2 = step * wd * (joints[1]->GetAngle2Rate() + joints[3]->GetAngle2Rate()) / 2;
//  d1 = step * wd * wjoints[2]->GetAngleRate() / 2;
//  d2 = step * wd * wjoints[3]->GetAngleRate() / 2;

  d1 = step * wd * sjoints[2]->GetAngle2Rate() / 2;
  d2 = step * wd * sjoints[3]->GetAngle2Rate() / 2;

  dr = (d1 + d2) / 2;
  da = (d1-d2)/2*ws;  
  // Compute odometric pose
  this->odomPose[0] += dr * cos( this->odomPose[2] );
  this->odomPose[1] += dr * sin( this->odomPose[2] );
  this->odomPose[2] += da;
 
  //update encoder counts
  this->encoders[0] += d1*408/M_PI/wd;
  this->encoders[1] += d2*408/M_PI/wd;   
  
  /*
    GzVector pos;
    
    pos=this->body->GetRelPointPos(0,0,0);
    //  GzVector p0,p1;
    //  p0=this->body->GetRelPointPos(0,0,0);
    //  p1=this->body->GetRelPointPos(1,0,0);
    
    GzQuatern angles=this->body->GetRotation();
    double r,p,y;
    GzQuaternToEuler(&r,&p,&y,angles);
    
    // if(y<0)
    //  y=y+2*M_PI;
    // printf(" dy=%f dx=%f\n",(p1.y-p0.y),(p1.x-p0.x));
    
    this->odomPose[0] =pos.x;
    this->odomPose[1] =pos.y;
    this->odomPose[2] =y; //rad? yep
    //(180/M_PI)*(atan2((p1.y-p0.y),(p1.x-p0.x)));
    //printf("yaw = %f, %f\n",(180/M_PI)*y,this->odomPose[2]);
  */
  return;
}


// Initialize the external interface
int Alice::IfaceInit()
{
  this->iface = gz_position_alloc();
  assert(this->iface);

  if (gz_position_create(this->iface, this->world->gz_server, this->GetId(),
                          "Alice", (int)this, (int)this->parent) != 0)
    return -1;
  
  return 0;
}


// Finalize the external interface
int Alice::IfaceFini()
{
  gz_position_destroy( this->iface );
  gz_position_free( this->iface );
  this->iface = NULL;
  
  return 0;
}


// Get commands from the external interface
void Alice::IfaceGetCmd()
{
  double vr, va;

  vr = this->iface->data->cmd_vel_pos[0];
  va = this->iface->data->cmd_vel_rot[2];

  // printf("vr=%f va=%f \n",vr,va);  
  this->wheelSpeed[0] = vr ;
  this->wheelSpeed[1] = va;
  
  return;
}

// Update the data in the interface
void Alice::IfacePutData()
{
  // Copy the old data to be able calculating speed
  double last_time = this->iface->data->time;
  
  // Set new timestamp
  this->iface->data->time = this->world->GetSimTime();
  
  double delta_time = this->iface->data->time - last_time;

  /*
  if(this->raw_encoder_position)
  {
    this->iface->data->pos[0] = this->encoders[0]*1e-3;
    this->iface->data->pos[1] = this->encoders[1]*1e-3;
    this->iface->data->rot[2] = NAN;
  }
  else 
  {
    this->iface->data->pos[0] = ( this->odomPose[0]);
    this->iface->data->pos[1] = ( this->odomPose[1]);
    this->iface->data->rot[2] = ( this->odomPose[2]);
    // printf("CLOD DUMP %f,%f,%f\n",this->odomPose[0],this->odomPose[1],this->odomPose[2]);
  }
  printf("old (x,y) = (%lf, %lf)\n", this->iface->data->pos[0], this->iface->data->pos[1]);
  */
    
  // Update positions and rotation
  GzVector pos, rot;
  pos = this->body->GetPosition();
  rot = GzQuaternToEuler(this->body->GetRotation());
  //printf("(x,y,z) = (%lf, %lf, %lf)\n", pos.x, pos.y, pos.z);        
  //printf("(p,r,y) = (%lf, %lf, %lf)\n", rot.x, rot.y, rot.z);     
  
  // calculate an approximated speed before we overwrite our old values
  this->iface->data->vel_pos[0] = fabs(this->iface->data->pos[0] - pos.x) / delta_time;
  this->iface->data->vel_pos[1] = fabs(this->iface->data->pos[1] - pos.y) / delta_time;
  this->iface->data->vel_pos[2] = fabs(this->iface->data->pos[2] - pos.z) / delta_time;

  // now let's fill in the new positioin values to the position interface.
  this->iface->data->pos[0] = pos.x; // Easting
  this->iface->data->pos[1] = pos.y; // Northing
  this->iface->data->pos[2] = pos.z; // Altitude
  this->iface->data->rot[0] = rot.x; // Pitch
  this->iface->data->rot[1] = rot.y; // Roll
  this->iface->data->rot[2] = rot.z; // Yaw
  
  //printf("new (x,y) = (%lf, %lf)\n", this->iface->data->pos[0], this->iface->data->pos[1]);
  return;
}


