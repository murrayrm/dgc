/*
 *  Gazebo - Outdoor Multi-Robot Simulator
 *  Copyright (C) 2003  
 *     Nate Koenig & Andrew Howard
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
/* Desc: A hinge joint with 2 degrees of freedom
 * Author: Nate Keonig, Andrew Howard
 * Date: 21 May 2003
 * CVS: $Id: Hinge2Joint.hh,v 1.5 2003/09/14 22:40:54 natepak Exp $
 */

#ifndef HINGE2JOINT_HH
#define HINGE2JOINT_HH

#include <gazebo/Joint.hh>

class JointGroup;

class Hinge2Joint : public Joint
{
  // Constructor
  public: Hinge2Joint( dWorldID worldId, JointGroup *group = NULL );

  // Destructor
  public: virtual ~Hinge2Joint(); 
 
  // Get anchor point
  public: void GetAnchor1(dVector3 result) const;

  // Get anchor point 2
  public: void GetAnchor2(dVector3 result) const;

  // Get first axis of rotation
  public: void GetAxis1(dVector3 result) const;

  // Get second axis of rotation
  public: void GetAxis2(dVector3 result) const;

  // Get angle of rotation about first axis
  public: double GetAngle1() const;

  // Get rate of rotation about first axis
  public: double GetAngle1Rate() const;

  // Get rate of rotation about second axis
  public: double GetAngle2Rate() const;

  // Get the specified parameter
  public: virtual double GetParam( int parameter ) const;

  // Set the anchor point
  public: void SetAnchor( double x, double y, double z );

  // Set the first axis of rotation
  public: void SetAxis1( double x, double y, double z );

  // Set the second axis of rotation
  public: void SetAxis2( double x, double y, double z );

  // Set _parameter with _value
  public: virtual void SetParam( int parameter, double value );

  // Add torques to Hinge axes
  public: void AddTorques( double t1, double t2 );

};

#endif

