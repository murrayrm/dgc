#include "FakeState.hh"
#include "DGCutils"
#include "FakeStateUpdateThread.cc"

int main(int argc, char **argv)
{
	int sn_key = 0;

	char* default_key = getenv("SKYNET_KEY");
	if (2 == argc)
	  {
	    sn_key  = atoi(argv[1]);
	  }
	else if (default_key != NULL )
	  {
	    sn_key = atoi(default_key);
	  }else
	{
		cerr << "SKYNET_KEY environment variable isn't set." << endl;
	}
	cerr << "Constructing skynet with KEY = " << sn_key << endl;
	FakeState ss(sn_key,1,1,1,1,1);

	DGCstartMemberFunctionThread(&ss, &FakeState::Update_thread);
	ss.VehicleStateMsg_thread();
	return 0;
}
