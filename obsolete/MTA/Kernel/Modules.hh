#ifndef Modules_HH
#define Modules_HH

// Add your module to the bottom of this list,
// above the place holder.

namespace MODULES 
{
  enum 
  {
    // Echo server will return any MTA message that was sent to it.
    EchoServer,   

    // Master clock. All modules can automatically synchronize 
    // their clocks with this module.
    Clock,              

    // A module that only sends query messages and does not need to 
    // receive any messages may be considered passive
    Passive,            

    // Serves a text file that tells the kernel which IP addresses 
    // correspond to active computers that may be running modules.  
    // The kernel will then port scan these IP addresses to find modules.
    SystemMap,         

    // Runs GPS, IMU, OBD-II -- serves state data via query message.
    StateEstimator,         

    // Runs steering, gas and brake -- accepts velocity and steering angle.  
    // Contains cruise controller to try to set speed appropriately.
    Actuators,          

    // Just a test module.  Not important to anyone but Ike.
    TestModule,         

    VDrive,
    VState,
    SimVDrive,

    Arbiter,
    StereoPlanner,
    GlobalPlanner,
    CorridorArcEvaluator,    // New module for genMaps/corridor following
    LADARBumper,             // This is the simple ladar bumper code
    LADARMapper,             // This ladar module does the terrain mapping
    DFEPlanner,
    RoadFollower,
    PathEvaluation, // Code to test the genMap
    FenderPlanner, // New module for using a priori data

    StereoImageLogger,

    MatlabDisplay,

    /* Commands added by RMM for vehlib-2.0 functionality */
    VTest,			// Test program for command vdrive
    VRemote,			// Remote control of vdrive/vstate

    VManage,
    PlayerSendCmd,    // PlayerInterface module, for interface to Gazebo
    VoterSim,         // Voting simulator from logfiles (arbiter)
    GeneticAlgorithm, // To find good arbiter votes 

    MTA_Demo,

    FusionMapper, // map-based sensor fusion module
    PlannerModule, // planner module that uses map deltas
    LadarFeeder, // ladar module that feeds measurements to FusionMapper

    PathFollower, // module to follow a path defined by the CTraj class

    CDS110_Team4, //module for CDS project (Laura, Alex and Jason)
    LadarFinder, // flat road finder based on LADAR data

    vstate_test,

    ModeMan,     // mode management module

    RddfPathGen,  // Jason and Ben's Rddf path generation module

    PID_PathFollower, //module trying to make CDS110_Team4 work w/ MM
    AState, //New module for state server in alice
    AState_test, //New module for testing state server in alice
    MapDisplayMTA, // New module for doing MapDisplay over MTA

    lateralController, // CDS110 project -- Eric & Jeff
    // BEGIN --- PLACE HOLDER ---- PLACE HOLDER
    // ADD NEW MODULES ABOVE THIS LINE (ABOVE THE PLACE HOLDER)
    // AND MAKE SURE TO PLACE A COMMA AFTER ITS NAME.
    NumberOfModules
    // END   --- PLACE HOLDER ---- PLACE HOLDER
  };


  static const char* const Strings[] = {
    "Echo Server",
    "Clock",
    "Passive",
    "System Map",
    "State Estimator",
    "Actuators",
    "Test Module",
    "vdrive",
    "vstate",
    "SimVDrive",
    "Arbiter",
    "Stereo Planner",
    "Global Planner",
    "Corridor Arc Evaluator",
    "LADAR Bumper",
    "LADAR Mapper",
    "DFE Planner",
    "Road Follower",
    "Path Evaluation",
    "Fender Planner",
    "Stereo Image Logger",
    "Matlab Display",
    "VTest",
    "VRemote",
    "VManage",
    "Player Send Command Simulator (Gazebo)",
    "Voter simulator (arbiter)",
    "Genetic Algorithm (arbiter)",

    "MTA Demo Module",
    "Fusion Mapper",
    "Planner Module",
    "Ladar Feeder",
    "Path Follower",
    "Path Follower for the CDS110 project",
    "Ladar Finder",

    "VState testing module",

    "Mode Manager",

    "RDDF Path Generation Module",

    "PID Path Follower",
    "AState",
    "AState_test",
    "MapDisplayMTA",

    "Lateral Controller",

    "Number Of Modules (Should Never See This - Check Kernel/Modules.hh)"
  };
  

};













#endif
