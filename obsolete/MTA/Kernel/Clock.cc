
#include "Clock.hh"
#include "Kernel.hh"

Timeval Clock::GetTime(MailAddress & ma) {

  Mail msg = NewQueryMessage(ma, MODULES::Clock, ClockMessages::GetTime);
  Mail reply = SendQuery (msg);
  Timeval ret;
  reply >> ret;
  return ret;
}

void Clock::Reset(MailAddress& ma) {

  Mail msg = NewOutMessage(ma, MODULES::Clock, ClockMessages::Reset);
  SendMail(msg);

}













