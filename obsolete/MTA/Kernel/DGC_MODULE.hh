#ifndef DGC_MODULE_HH
#define DGC_MODULE_HH

#include <string>

#include "Misc/Mail/Mail.hh"
#include "Misc/Structures/ModuleInfo.hh"
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/shared_ptr.hpp>



using namespace std;
using namespace boost;

class DGC_MODULE {

public:
  DGC_MODULE(int ModType, string ModName, int ModFlags);
  virtual ~DGC_MODULE();
private:
  DGC_MODULE(const DGC_MODULE &);
public:
  // where it all starts...
  void operator ()();

  // Force shutdown causes the main loop to exit
  // and calls the shutdown state
  void ForceShutdown();

  // States in state machine
  virtual void Init()    ;// = 0;
  virtual void Active()  ;// = 0;
  virtual void Standby() ;// = 0; 
  virtual void Shutdown();// = 0;
  virtual void Restart() ;// = 0;


  // returns "world readable status" of module as a string
  virtual string Status();
  

  // Function called when new mail has arrived
  virtual void InMailHandler(Mail & msg) ;//=0;
  virtual Mail QueryMailHandler(Mail & msg) ;//=0;


  // accessors
  int  ModuleNumber();
  MailAddress MyAddress();
  ModuleInfo MyInfo();

  int ModuleType();
  string ModuleName();

  // Functions to control state machine
protected:
  int  SetNextState( int stateID );
  int  GetCurrentState();
  int  ContinueInState();
private:
  int M_ShutdownFlag;

  void StateMachineLoop();
  void ExecuteCurrentState();

  // used by kernel only - dont use 
  //  void SetModuleNumber(int m);
  friend void Register(shared_ptr<DGC_MODULE> dm);
  friend void Unregister(int mailbox);

  // Module Low-Level Control
  int M_MyThreads;
  int M_ModuleNumber;

  // Module Definition
  int    M_ModuleType;
  string M_ModuleName;
  int    M_ModuleFlags;
  
  //  Mailbox M_Mailbox;
    
  
  // State Machine Variables
  int M_CurrentState;
  int M_NextState;
  recursive_mutex M_StateMachineLock;
  
};

// helper functions
Mail NewQueryMessage(MailAddress from, int modtype, int messageType);
Mail NewOutMessage(MailAddress from, int modtype, int messageType);


// state machine indices enumerated
namespace STATES {
  enum { INIT, ACTIVE, STANDBY, SHUTDOWN, RESTART, NUM_STATES };
};

void DGC_MODULE_STARTER();

#endif
