#ifndef Kernel_HH
#define Kernel_HH


#include "Friends.hh"
#include "Modules.hh"
#include "DGC_MODULE.hh"
#include "KernelDef.hh"
#include "Debug.hh"
#include "SystemMap.hh"

#include "Misc/Structures/ModuleInfo.hh"
#include "Misc/Socket/Socket.hh"
#include "Misc/File/File.hh"
#include "Misc/Mail/Mail.hh"
#include "Misc/Time/Timeval.hh"
#include "Misc/Structures/Computer.hh"


#include <list>
#include <map>

#include <boost/shared_ptr.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include <unistd.h>

#include <signal.h>


using namespace std;
using namespace boost;



class Kernel {
  friend class ReceiveMailHandler;
public:
  Kernel();
  ~Kernel();

private:  
  // Init Functions for the threads that will run everything
  int  ReceiveMailInit();
  int  SendMailInit();
  int  SystemMapInit(string textFile);
  int  TimeSyncInit();

  // The threads that will run everything
  void ReceiveMailThread();
  void SendMailThread();
  void SystemMapThread();
  void TimeSyncThread();
  
  // And Finally, their corresponding shutdown functions
  void ReceiveMailShutdown();
  void SendMailShutdown();
  void SystemMapShutdown();
  void TimeSyncShutdown();
public:

  // Registration - Kernel.cc
  friend void Register(shared_ptr<DGC_MODULE> dgc);
  friend void Unregister(int Mailbox);

  
  // Kernel Startup and Shutdown functions, 
  // plus a flag function - Kernel.cc
  friend void StartKernel(int); // start of program, allow module to operate
  friend void ForceKernelShutdown(); // Flags for shutdown
  friend int  ShuttingDown(); // returns ShutdownFlag
  
  // Time accessors - Time.cc
  friend Timeval Now(); // returns current time

  // System Map Functions - SystemMap.cc
  friend TCP_ADDRESS MyTCPAddress(); // returns TCP Address of current module
  friend DGC_MODULE* FindLocalModule(int mailbox_number);
  friend MailAddress FindModuleAddress(std::string moduleName);
  friend MailAddress FindModuleAddress(int ModuleType); 
  friend list<ModuleInfo> LocalModules();
  friend list<ModuleInfo> AllModules();

  // Mail Functions
  friend void  SendMail(Mail & ml, File f); // puts mail in a queue to be sent
  friend Mail  SendQuery(Mail & ml, File f); // sends mail immediately and waits for respons (returned by function)

    

private:

  int Bootstrap();

  // ***************************************
  // Time Data and private functions
     // Time Zero - For Time Sync
     Timeval M_TimeZero;
     recursive_mutex               M_TimeZeroLock;
     void SystemTimeZeroUpdate();
  // ***************************************


  // ***************************************
  // System Map Data 
     // Local TCP/IP addres
     TCP_ADDRESS M_LOCAL_TCP;  // ip and port of this server 

     // List of [local] modules registered with the kernel
     list<shared_ptr<DGC_MODULE> > M_LocalModules;
     recursive_mutex               M_LocalModulesLock;
     int                           M_LocalModulesCounter;

     // System as a whole info
     //   Computers Lookup
     recursive_mutex          M_TableOfComputersLock;
     map<string, IP_ADDRESS>  M_TableOfComputers;
     map<string, int>         M_ModuleNameToType;
     //   ...and Modules Lookup
     list<ModuleInfo>         M_TableOfModules;
     recursive_mutex          M_TableOfModulesLock;  

     list< shared_ptr<Computer> > M_Grid;


  // End - System Map Data -----------------  
  // ***************************************

  // ***************************************
  // Mail Data
     // The Socket Server to receive messages
     SSocket M_ServerSocket;
  
     // Mailboxes (inbox and outbox) plus locks
     list <Mail> M_Outbox;
     recursive_mutex M_OutboxLock;
     list <Mail> M_Inbox;
     recursive_mutex M_InboxLock;

     // and a function to deliver mail (part of kernel)
     // to prevent others from using this 
     void  DeliverNextMessage();
     void  DeliverMail(Mail &ml, File f = File() );
  
     pid_t DeliverPID;
  // ***************************************


  // Mail Handlers for the Kernel
  Mail KernelQueryMailHandler(Mail& ml);
  void KernelInMailHandler(Mail& ml);
    
  int M_ShutdownFlag;
  int M_ActiveFlag;
  
};


namespace KernelMessages {
  enum {  
    S_ECHO,       // just respond with whatever was sent

    // IN-MESSAGES
    SHUTDOWN,   // Message Declaring Shutdown
    
    
    // QUERY-MESSAGES
    
    MODULEINFO, // list what modules are 

    
    
    NumKernelMessages
  };

    






};



#endif
