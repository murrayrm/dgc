
#ifndef dgc_messages_h
#define dgc_messages_h

/* Message Header.  Every message will transmit with a header plus an 
   unknown amount of data attached.  The way to receive a message is first
   poll for the header, then pass a pointer to a function which will fill 
   that block of memory (of adequate size) with the message. */


/* THE MESSAGE HEADER */
struct dgc_MsgHeader {
  unsigned int  MsgID;        // MsgID and source combine to make a unique message 
  unsigned int  Source;       // key.  i.e. each computer will increment its message
           int  SourceID;     // IDs are the programs running on the source and destination computers
  unsigned int  SourceType;
  unsigned int  Destination;  // IP address of destination
           int  DestinationID;// ID counter, thus each message is unique.  
  unsigned int  DestinationType;
  unsigned long S_Timestamp;  // timestamp on send
  unsigned long R_Timestamp;  // timestamp on receive
  unsigned int  MsgType;      // defined in dgc_messages.h
  unsigned int  MsgSize;      // size of msg, not including header

};





enum {
  NullMsg = 0,
  /* dgc_messages.h contains all message identifiers.  
     Message ID's are simply integer values starting at 1. */

  /* constants for passing system information */
  dgcm_SetTime0,               // Set clock to 0 (start of execution)
  dgcm_Time,                   // Time in ms since program start
  dgcm_Ping,                   // ping from one system (requests return ping)
  dgcm_ReturnPing,             //
  dgcm_RequestProvides,        // all hardware/tasks that the current system can provide
  dgcm_ReturnProvides,         // return to requestor
  dgcm_DestinationUnreachable,


  /* Messages for determining hierarchy */
  dgcm_RequestNativeRank,      // each computer will have a 'natural' rank in memory, 
  dgcm_ReturnNativeRank,       // 
  dgcm_RequestRank,            // but this may change during execution.
  dgcm_ReturnRank,             // 
  dgcm_Promote,                // move up in rank
  dgcm_Demote,                 // move down in rank
  dgcm_PromoteIfAfter,         // promote if after N (say computer 3 dies, then promote everything after 3)

  /* System level time messages */
  dgcm_RequestLocalTime,       // get time from local machine
  dgcm_RequestTimeSync,        // resynchronize system clock to master computer
                               // all computers will sync on startup except the master computer
                               // which will set its own time to 0

  /* Messages for requesting frames */
  dgcm_RequestSnapshot,        // request a pair of frames from the frame server (camera data included)
  dgcm_ReturnSnapshot,         // 
  dgcm_RequestSelfSnapshot,    // If comp has fg, it will request permission to grab a frame by itself
  dgcm_ReturnSelfSnapshotOK,   // Allow the computer to capture a frame (which to grab sent in return packet)
  

  /* Messages for passing data from Perceptor to Claraty */
  dgcm_SendingPerceptorMap,    // Perceptor will automatically send map using this message
  dgcm_RequestPerceptorMap,    // Then Claraty will grab the perceptor map using this message call


  /* IMU Data messages */
  dgcm_RequestIMU,             // request IMU at __ time
  dgcm_ReturnIMU,              // 

  /* GPS */
  dgcm_RequestGPS,             // at ___ time
  dgcm_ReturnGPS, 

  /* Mode Manager Packets */
  dgcm_DriveCommandPacket,            // has steering and accel info
  dgcm_DriverTakeoverPacket,
  dgcm_DriverReleasePacket,

  /* State Estimator Packet */
  dgcm_RequestState,
  dgcm_ReturnState,


  /* Joystick commands */
  dgcm_JoystickRequestsControl,
  dgcm_JoystickStatePacket,
  dgcm_JoystickControlState,

  /* Accel Commands */
  dgcm_AccelStatePacket,
  dgcm_TransmissionStatePacket,
  dgcm_IgnitionStatePacket,
  
  /* Steering Commands */
  dgcm_SteeringStatePacket,

  /* Driver Flat Panel Message */
  dgcm_DriverMessage,

  /* and a place-holder for the last DGC message */
  dgcm_LastMessage
};



/* Flags for transporting the message */
enum {
  dgcf_NoSerial     = 1,  // default is allow both serial and ethernet
  dgcf_NoEthernet   = 2,  // trying local, then ethernet, then serial 
  dgcf_PreferSerial = 4, 

  dgcf_NoNotifyOnFailure = 8  // if a message cannot be sent, the message is returned to the 
                              // sender unless this flag is set

};





/* Destinations, relating to IP Addresses */
enum {
  dgcd_Null = 0,
  dgcd_Comp1 = 1,          // here we are creating a unique list of IDs for each computer 
  dgcd_Comp2,              // and any system.  To resolve a system, the destination computer 
  dgcd_Comp3,              // needs to be resolved along with 
  dgcd_Comp4,
  dgcd_Comp5,
  dgcd_Comp6,
  dgcd_Comp7,
  dgcd_Comp8,
  dgcd_Comp9,
  dgcd_Comp10,
  dgcd_Comp11,
  dgcd_Comp12,
  dgcd_Comp13,
  dgcd_Comp14,
  dgcd_Comp15,
  dgcd_Comp16,
  dgcd_MagicBox
};


/* System Modules */
enum {

  dgcs_MasterMTA,
  dgcs_LocalMTA,
  
  dgcs_SystemMaster,
  dgcs_ModeManager,

  dgcs_MagicBox,
  dgcs_FrameGrabber,
  dgcs_Arbiter,
  dgcs_GlobalRoutePlanner,
  dgcs_LocalRoutePlanner,
  dgcs_JoystickRoutePlanner,

  dgcs_StateDriver,
  dgcs_JoystickDriver,
  dgcs_TransmissionDriver,
  dgcs_IgnitionDriver,
  dgcs_AccelBrakeDriver,
  dgcs_SteeringDriver,
  dgcs_Jeremy,

  dgcs_FlatPanel,

  dgcs_LastMessage
};

// Joystick Enumerated return values
enum {
  Joystick_ON=1,
  Joystick_OFF
};


#endif
