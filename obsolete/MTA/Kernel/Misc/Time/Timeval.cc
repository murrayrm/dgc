#include "Timeval.hh"

#include <time.h>
#include <sys/time.h>
#include <unistd.h>

Timeval::Timeval() {

  myTV.tv_sec = 0;
  myTV.tv_usec = 0;

}

Timeval::Timeval(long seconds, long microseconds) {

  myTV.tv_sec = seconds;
  myTV.tv_usec = microseconds;

}

Timeval::Timeval(const timeval & tv2) {
  myTV.tv_sec  = tv2.tv_sec;
  myTV.tv_usec = tv2.tv_usec;
}

Timeval::Timeval(const Timeval& rhs) {

  myTV.tv_sec  = rhs.myTV.tv_sec;
  myTV.tv_usec = rhs.myTV.tv_usec;

}

Timeval Timeval::operator - (const Timeval& rhs) {
  Timeval t = (*this);
  
  if( t.myTV.tv_usec < rhs.myTV.tv_usec ) {
    // borrow from seconds
    t.myTV.tv_usec += 1000000;
    t.myTV.tv_sec -= 1;
  }

  t.myTV.tv_sec  -= rhs.myTV.tv_sec;
  t.myTV.tv_usec -= rhs.myTV.tv_usec;

  return t;
}

Timeval Timeval::operator + (const Timeval& rhs) {
  Timeval t = (*this);
  
  if( (t.myTV.tv_usec + rhs.myTV.tv_usec) >= 1000000 ) {
    // borrow from microseconds
    t.myTV.tv_usec -= 1000000;
    t.myTV.tv_sec += 1;
  }

  t.myTV.tv_sec  += rhs.myTV.tv_sec;
  t.myTV.tv_usec += rhs.myTV.tv_usec;

  return t;
}

int Timeval::operator > (const Timeval& rhs) const{
  if( myTV.tv_sec > rhs.myTV.tv_sec ) {
    return true;
  } else if ( myTV.tv_sec < rhs.myTV.tv_sec ) {
    return false;
  } else if ( myTV.tv_usec > rhs.myTV.tv_usec) {
    return true;
  } else {
    return false;
  }
}

int Timeval::operator <= (const Timeval& rhs) const{
  return ! ((*this) > rhs);
}

int Timeval::operator < (const Timeval& rhs) const{
  return (rhs > (*this));
}

int Timeval::operator >= (const Timeval& rhs) const{
  return ! ((*this) < rhs);
}


int Timeval::operator == (const Timeval& rhs) const{
  if( myTV.tv_sec  == rhs.myTV.tv_sec  &&
      myTV.tv_usec == rhs.myTV.tv_usec) {
    return true;
  } else {
    return false;
  }
}

long Timeval::sec() {
  return myTV.tv_sec;
}

long Timeval::usec() {
  return myTV.tv_usec;
}

double Timeval::dbl() {
 return ((double) myTV.tv_sec) + (( (double) myTV.tv_usec) / 1000000.0); 
}

timeval Timeval::TV() {
  return myTV;
}

void Sleep(Timeval t) {
  usleep( (t.sec()*1000000) + t.usec() );
}



int Timeval::OK() {
  if( myTV.tv_sec > 0 && myTV.tv_usec > 0) {
    return true;
  } else {
    return false;
  }
  
}

Timeval TVNow() {
  timeval t;
  gettimeofday(&t, NULL);
  return Timeval(t.tv_sec, t.tv_usec);
}

Timeval BadTimeval() {
  // for now just return a negative number
  return Timeval(-1, -1);
}


ostream& operator << (ostream& os, Timeval tv) {
  double dbl = ((double) tv.sec()) + (( (double) tv.usec()) / 1000000.0); 
  //  os << "(" << tv.sec() << ", " << tv.usec() << ")";
  os << dbl;
  return os;
}
