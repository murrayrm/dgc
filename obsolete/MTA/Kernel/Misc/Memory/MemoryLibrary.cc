#include "MemoryLibrary.hh"


MemoryLibrary::MemoryLibrary(int blocksize) {

  M_BlockSize = blocksize;

}


MemoryLibrary::~MemoryLibrary() {
  
  boost::recursive_mutex::scoped_lock s(M_TableLock);
  list<void*>::iterator x;
  for( x = M_Table.begin(); x != M_Table.end(); x++) {
    char *a = (char*) (*x);
    delete [] a;
  }
}

shared_ptr<MemoryHandle> MemoryLibrary::Checkout() {

  void* p;
  boost::recursive_mutex::scoped_lock s(M_TableLock);
  if( M_Table.size () < 1 ) {
    p = (void*) new char[M_BlockSize];
  } else {
    p = M_Table.front();
    M_Table.pop_front();
  }
  return shared_ptr<MemoryHandle>(new MemoryHandle(this, p, M_BlockSize));
}

void MemoryLibrary::Return(void* p) {
  
  boost::recursive_mutex::scoped_lock s(M_TableLock);
  M_Table.push_back(p);

}

