#include "BufferedPipe.hh"
#include "../Data/Constants.hh"
#include "../File/File.hh"
#include "../Time/Timeval.hh"
#include <time.h>
#include <sys/time.h>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include <sys/types.h>

#include <iostream>
#include <list>
#include <cstring>

using namespace std;

BufferedPipe::BufferedPipe() {
  myBlockSize = DEF::BlockSize;
}

BufferedPipe::~BufferedPipe() {
  //  destruct();
}

//void BufferedPipe::destruct() {
  //  while( myData.size() > 0 ) {
  //    myData.pop_front();
  //  }
//}

int BufferedPipe::size() {
  boost::recursive_mutex::scoped_lock s(mutex);
  list<PipeBlock>::iterator x;

  int sz=0;
  for(x = myData.begin(); x != myData.end(); x++) {
    sz += (*x).leftToRead();
  }
  return sz;
}

int BufferedPipe::blocks() {
  boost::recursive_mutex::scoped_lock s(mutex);
  return myData.size();
}

int BufferedPipe::getFromFile(File & FD, int bytes) {
  return getFromFile( FD.FD(), bytes);

}
int BufferedPipe::getFromFile(int FD, int bytes) {
  File f(FD, FALSE); // we are not the owner of the file
  boost::recursive_mutex::scoped_lock sl(mutex);

  Timeval before, after;
  before = TVNow();

  int toWrite;
  int bytes_read;
  int bytes_to_write = bytes;

  while( bytes > 0) {
    if( myData.size() == 0 || myData.back().leftToWrite() <=0) {
      myData.push_back( PipeBlock() ); // allocate a new buffer
    } 
    list<PipeBlock>::iterator x = myData.end(); x--; // last good buffer
    toWrite = (*x).leftToWrite();
    toWrite = (toWrite < bytes)? toWrite: bytes; // min( bytes left to read, block size left)
    bytes_read = f.BRead( (*x).x+(*x).right, toWrite);
    //    cout << "Bytes read = " << bytes_read << endl;
    if( bytes_read < 0 ) {
      cout << "Buffered Pipe File Read Error" << endl;
      return ERROR;
    } else if(bytes_read == 0) {
      after = TVNow();
      if( after - before > Timeval(0, 100000)) {
	return bytes_to_write - bytes;
      }
    } else { 
      (*x).right += bytes_read;
      bytes  -= bytes_read;
      before = TVNow();
    }
    
  }
  //  cout << "Buffered Pipe Returns " << bytes_to_write << " bytes." << endl;
  return bytes_to_write - bytes;
}


int BufferedPipe::flushToFile(File &FD, int bytes) {
  //  boost::recursive_mutex::scoped_lock sl(mutex);
  return flushToFile(FD.FD(), bytes);
}


int BufferedPipe::flushToFile(int FD, int bytesToWrite) {


  File f(FD, FALSE); // we are not the owner of the file
  boost::recursive_mutex::scoped_lock sl(mutex);
  if (bytesToWrite < 0) bytesToWrite = size();

  int toWrite;
  int bytes_written;
  int totalBytes = bytesToWrite;

  while( bytesToWrite > 0 && size() > 0 ) {

    list<PipeBlock>::iterator x = myData.begin(); 
    if( (*x).leftToRead() == 0 ) { // size guaranteed to be greater than zero
      myData.pop_front();
    } else {
      toWrite = (*x).leftToRead();
      toWrite = (toWrite < bytesToWrite)? toWrite: bytesToWrite;
      
      bytes_written = f.BWrite((*x).x + (*x).left, toWrite);
      (*x).left += bytes_written;
      bytesToWrite -= bytes_written;
    }
  }


  // flush the file
  //  int ret = ioctl(FD, I_FLUSH, FLUSHW);
  //  if( ret < 0 ) {
  //    cout << "Buffered Pipe::IOCTL Flush - Failed" << endl;
  //  } 

  return totalBytes-bytesToWrite;
}


int BufferedPipe::peek(void* buf, int bytes) {

  char* buffer = (char*) buf;

  size_t toWrite;
  int bytesToWrite = bytes;

  boost::recursive_mutex::scoped_lock sl(mutex);
  list<PipeBlock>::iterator x = myData.begin();

  while( bytesToWrite > 0 && x != myData.end() ) {

    // If we need more than is in this block
    if( bytesToWrite > (*x).leftToRead() ) { 
      // read all in this block and move to the next block
      memcpy( buffer, (*x).x+(*x).left, (*x).leftToRead() );
      bytesToWrite -= (*x).leftToRead();
      buffer       += (*x).leftToRead();
      x++;
    } else {
      // read what is here and we should be done
      memcpy( buffer, (*x).x+(*x).left, bytesToWrite );
      buffer       += bytesToWrite;
      bytesToWrite  = 0;
    }
  }

  return bytes-bytesToWrite;
}


int BufferedPipe::pop(void* buf, int bytes) {

  char* buffer = (char*) buf;

  size_t toWrite;
  int bytesToWrite = bytes;

  boost::recursive_mutex::scoped_lock sl(mutex);
  while( bytesToWrite > 0 && size() > 0 ) {
    list<PipeBlock>::iterator x = myData.begin();
    if( (*x).leftToRead() == 0) {
	myData.pop_front();
    } else {
      toWrite = (*x).leftToRead();
      toWrite = (toWrite < bytesToWrite)? toWrite: bytesToWrite;
      
      memcpy( buffer, (*x).x+(*x).left, toWrite);
      (*x).left += toWrite;
      bytesToWrite -= toWrite;
      buffer       += toWrite;
    }
  }

  return bytes-bytesToWrite;
}


int BufferedPipe::push(const void* mem, int bytes) {

  char* buffer = (char*) mem;
  size_t toWrite;
  boost::recursive_mutex::scoped_lock sl(mutex);

  while( bytes > 0) {
    if( myData.size() == 0 || myData.back().leftToWrite() <=0) {
      myData.push_back( PipeBlock() );
    }
    list<PipeBlock>::iterator x = myData.end(); x--;
    toWrite = (*x).leftToWrite();
    toWrite = (toWrite < bytes)? toWrite: bytes;
    memcpy( (*x).x+ (*x).right, buffer, toWrite);
    (*x).right += toWrite;
    bytes  -= toWrite;
    buffer += toWrite;
  }
  return TRUE;
}

// Empty the buffer
void BufferedPipe::dump() {

  boost::recursive_mutex::scoped_lock sl(mutex);
  
  myData.clear();

}
