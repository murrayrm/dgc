#ifndef BufferedPipe_HH
#define BufferedPipe_HH

#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include <list>
#include <stddef.h>
#include <memory>
#include "Pipe.hh"
#include "../Memory/SharedMemoryContainer.hh"
#include "../File/File.hh"
#include "../../KernelDef.hh"
using namespace std;

struct PipeBlock {

  int left, right;
  char x[DEF::BlockSize];

  PipeBlock() : left(0), right(0) {}

  int leftToWrite() { return DEF::BlockSize-right; }
  int leftToRead()  { return right-left; }

};



class BufferedPipe { //:Pipe  {
  
public:
  BufferedPipe();
  ~BufferedPipe();
  //  void destruct();
  int size();
  int blocks();
  int getFromFile(int FD,   int bytes);    // returns the # of bytes read or -1
  int getFromFile(File& FD, int bytes);    // for error
                                           
  int flushToFile(int FD,   int bytes=-1); // returns # of bytes remaining
  int flushToFile(File& FD, int bytes=-1); // after flushing "bytes" bytes. If 
                                           // bytes<0 it will attempt to flush all.

  int peek(void* buffer, const int bytes); // reads bytes into memory without taking them out of the buffered pipe
  int pop(void* buffer, const int bytes);  // reads data from buffered pipe and removes it from the pipe
  int push(const void* buffer, const int bytes); // adds data to end of buffered pipe

  void dump(); // empties the buffer

private:

  std::list<PipeBlock> myData;

  size_t mySize;
  size_t myBlockSize;
  boost::recursive_mutex mutex;
};

#endif
