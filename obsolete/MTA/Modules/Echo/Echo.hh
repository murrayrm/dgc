#ifndef Echo_HH
#define Echo_HH

#include "DGC_MODULE.hh"
#include "EchoDatum.hh"


using namespace std;
using namespace boost;

// state machine states enumerated
// defined in DGC_MODULE.hh

// namespace STATES {
//   enum { INIT, ACTIVE, STANDBY, SHUTDOWN, RESTART, NUM_STATES };
// };

class Echo : public DGC_MODULE {
public:
  Echo();
  ~Echo();

  void Init();
  void Active();

  void InMailHandler(Mail & ml);
  Mail QueryMailHandler(Mail & ml);

private:

  EchoDatum D;
};














// enumerate all module messages that could be received
namespace TestModuleMessages {
  // for echo program, just return whatever was sent
};





#endif
