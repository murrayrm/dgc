/**
 * RoadFinding.cc
 * Road Finding module
 *
 * Revision history:
 * 3/3/05 - Ryan Farmer (ryanf) - created
 * 3/8/05 - Ryan Farmer (ryanf) - cleaned up skynet code
 */

#include "RoadFinding.hh"


RoadFinding::RoadFinding(int skynetKey) : CSkynetContainer(SNroadfinding, skynetKey)
{
  m_vPoint_X = 0;
  m_vPoint_Y = 0;
  m_RoadBounds = NULL;

  m_RoadCount = 5; // number of scanlines

  m_imgWidth = 200;
  m_imgHeight = 150;
  srand((unsigned)time(0));

  /* skynet init */
  ConnectToSkynet();
}


RoadFinding::~RoadFinding()
{
  if (m_RoadBounds != NULL)
    delete [] m_RoadBounds;

}

double RoadFinding::GetVanishingPointDirection()
{
  // recompute direction--see if this is useful
  m_vPoint_X = rand() % m_imgWidth + 1;
  m_vPoint_Y = rand() % m_imgHeight + 1;  // +1 might not be needed--depends on how stereo vision indexes . . .
  return 180.0;

}

RoadBoundary *RoadFinding::GetRoadArea()
{
  /* Get Image/Propogate data here.  Instead, use random numbers */
  if (m_RoadBounds != NULL)
    delete [] m_RoadBounds;

  m_RoadBounds = new RoadBoundary[m_RoadCount];

  // need to fix--m_vPoint_Y is 0 right now or random...this will not work
  int scanline = m_vPoint_Y / m_RoadCount;
    // check if this is 0--if so, too many scanlines . . . return error
  for (int i = 0; i < m_RoadCount; i++)
  {
    // fill with garbage
    m_RoadBounds[i].left = new XYZcoord(1, 2, 40);
    m_RoadBounds[i].right = new XYZcoord(3, 2, 50);
    /*
    m_RoadBounds[i].left = new XYZcoord(2, i * scanline, 40);
    m_RoadBounds[i].right = new XYZcoord(m_imgWidth - 2, i * scanline, 50);
    */

  }


  /* end get data */

  return m_RoadBounds;


}


int RoadFinding::GetRoadCount()
{
  return m_RoadCount;
}


void RoadFinding::SendToSkynet()
{

  RoadBoundary *rB = GetRoadArea();

  int sizeSendArray = 1 + 6 * m_RoadCount;

  double sendArray[sizeSendArray];

  sendArray[0] = m_RoadCount;

  for (int i = 0; i < m_RoadCount; i++)
  {
    sendArray[1 + i * 6 + 0] = rB[i].left->X;
    sendArray[1 + i * 6 + 1] = rB[i].left->Y;
    sendArray[1 + i * 6 + 2] = rB[i].left->Z;
    sendArray[1 + i * 6 + 3] = rB[i].right->X;
    sendArray[1 + i * 6 + 4] = rB[i].right->Y;
    sendArray[1 + i * 6 + 5] = rB[i].right->Z;
  }

  m_Skynet.send_msg(m_socket, (void*)sendArray, sizeof(sendArray), 0);

}


void RoadFinding::ConnectToSkynet()
{
  // Message type
  m_skyOutput = SNroadboundary;
  
  m_socket = m_skynet.get_send_sock(m_skyOutput);

  if (m_socket < 0)
  {
    cerr << "Error connecting to skynet\n";
    m_SkynetConnected = false;
  }
  else
  {
    m_SkynetConnected = true;
  }

}
