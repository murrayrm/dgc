#include "brake.h"

#include <time.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {
  float pos;

  char buffer[1000];

  ifstream infile("command.txt");
  string cmd;

  init_brake();
  //  init_accel();
  int r;
  

  while(! infile.eof() ) {

    getline(infile, cmd);
    cmd += (char) 0x0D;
    cout << "TX: " <<cmd << endl;
    serial_write(BRAKEPORT, (char*)cmd.c_str(), cmd.length(), SerialBlock); 
    r = serial_read_until(BRAKEPORT, buffer, '\n', 1, 999);
    buffer[r-1] = 0x00;
    cout << "RC: " << buffer << endl;
    sleep(1);
    //usleep(100000);
  }
  
  return 0;

}

