#include "waypoints.hh"
#include <ctype.h>
#include <stdlib.h>
#include "ggis.h"

Waypoints::Waypoints (): _current(0),_last(0)
{
  _waypoint_list[0].t = NONE; 
  _waypoint_list[0].northing = 0.0; 
  _waypoint_list[0].easting  = 0.0; 
  _waypoint_list[0].radius   = 0.0; 
  _waypoint_list[0].max_velocity = 0.0; 
}

Waypoints::Waypoints (char * infile)
{
  read(infile);
}

// Want to do something else instead, and have this return the index
// of the waypoint to which we should be travelling.
int Waypoints::readRDDF(char* inFile, double Northing, double Easting)
// This was the old way of doing it; with it we have no way of 
// determining which waypoint we should be going to (we assume wp0)
//void Waypoints::readRDDF(char* inFile)
{
  RDDF rddf(inFile);
  RDDFVector targetPoints;
  int ind, numTargets;
  int wpNum; 

  numTargets = rddf.getNumTargetPoints();
  targetPoints = rddf.getTargetPoints();

  for(ind=0;ind < numTargets; ind++){
    _waypoint_list[ind].num          = targetPoints[ind].number;
    _waypoint_list[ind].northing      = targetPoints[ind].Northing;
    _waypoint_list[ind].easting     = targetPoints[ind].Easting;
    _waypoint_list[ind].radius       = targetPoints[ind].radius;
    _waypoint_list[ind].max_velocity = targetPoints[ind].maxSpeed;
    _waypoint_list[ind].t=NORMAL ;
  }

  _waypoint_list[0].t=START;
  _waypoint_list[ind-1].t=END;
  _waypoint_list[ind].t=NONE;
  _waypoint_list[ind].num = 10000;        //hack to correctly terminate list
  _last = ind;
  _current = 0;

  wpNum = rddf.getCurrentWaypointNumber( Northing, Easting );
  return wpNum;
}

void Waypoints::read (char * infile)
{
  ifstream wp_file(infile);
  char type_str[256];
  char dummy[256];
  int i=0;
  while (!wp_file.eof())
    {
//      _waypoint_list[i].num = i;
      double radius, speed;
      GisCoordLatLon latlon;
      GisCoordUTM utm;
//      wp_file >> _waypoint_list[i].num;
//      wp_file >> type_str;
      wp_file.getline(type_str, 256, ',');
      if (isdigit(type_str[0])) {           //skip lines begginning with #
        _waypoint_list[i].num=atoi(type_str) - 1; //get waypoint number

        wp_file.getline(type_str, 256, ',');  //get latitude
        latlon.latitude = atof(type_str);

        wp_file.getline(type_str, 256, ',');  //get longitude
        latlon.longitude = atof(type_str);

        if (!gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL)){ 
          cerr << "Error converting coordinate " << i << " to utm\n";
          exit(1); }
        _waypoint_list[i].easting = utm.e;
        _waypoint_list[i].northing = utm.n; 

        wp_file.getline(type_str, 256, ',');  //get radius
        _waypoint_list[i].radius = atof(type_str) * METERS_PER_FOOT;

        wp_file.getline(type_str, 256, ','); //get velocity limit
        _waypoint_list[i].max_velocity = atof(type_str) * MPS_PER_MPH;

        wp_file.ignore(256, ',');           //skip phase lines
        
        _waypoint_list[i++].t=NORMAL ;

        }
      else {
        wp_file.ignore(256,'\n');
        _waypoint_list[i].t=NONE; 
      }


    }
  _waypoint_list[0].t=START;
  _waypoint_list[i-1].t=END;
  _waypoint_list[i].t=NONE;
  _waypoint_list[i].num = 10000;        //hack to correctly terminate list
  _last = i;
  _current = 0;
}

int Waypoints::get_size()
{
  return _last;
}
waypoint Waypoints::get_start()
{
  _current = 0;
  return _waypoint_list[0];
}

waypoint Waypoints::get_next()
{
  if (_current < _last) _current++;
  return _waypoint_list[_current];
}

waypoint Waypoints::get_previous()
{
  if (_current > 0) _current--;
  return _waypoint_list[_current];
}

waypoint Waypoints::get_last()
{
  _current = _last;
  return _waypoint_list[_last];
}

waypoint Waypoints::get_current()
{
  return _waypoint_list[_current];
}

waypoint Waypoints::get_waypoint(int i)
{
  if (i < 0) i=0;
  if (i > _last) i=_last;
  _current = i;
  return _waypoint_list[_current];
}

waypoint Waypoints::peek_waypoint(int i)
{
  if (i < 0) i=0;
  if (i > _last) i=_last;
  return _waypoint_list[i];
}

waypoint Waypoints::peek_next()
{
  return _waypoint_list[(_current < _last)?(_current+1):(_last)];
}

waypoint Waypoints::peek_previous()
{
  return _waypoint_list[(_current > 0)?(_current-1):(0)];
}

