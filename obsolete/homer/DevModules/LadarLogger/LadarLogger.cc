#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <getopt.h>
#include <string.h>
#include <iostream.h>

#include "laserdevice_klk.h"

#define ANGLE 100
#define RES 0.50
#define UNITS 0.01

#define LOG_OPT 10
#define LADAR_TTY_OPT 11

int LOG_SCANS = 1;               // whether or not the scan points will be dumped to a log file
char *TESTNAME = NULL;           // id for test to be part of log file name
char *LADAR_TTY = NULL;          // serial port for scanner
FILE* scanlog;

/* The name of this program. */
const char * program_name;

/* Prints usage information for this program to STREAM (typically stdout 
   or stderr), and exit the program with EXIT_CODE. Does not return. */
void print_usage (FILE* stream, int exit_code)
{
  fprintf( stream, "\nUsage:  %s [options]\n", program_name );
  fprintf( stream, 
	   "  --log testname    Log all scanpoints, state and errors in\n"  
	   "                    testLogs directory. Log files will be named\n"
	   "                    as <testname>_<type>_<date>_<time>.log\n"
           "                    where <type> is scan, state, or errors.\n"
	   "  --ladar-tty tty    Specifies roof ladar TTY (e.g. /dev/ttyS8).\n"
           "  --help, -h        Display this message.\n" );      
           exit(exit_code);
}



int main(int argc, char *argv[]) {
  unsigned char data[1024];
  int result;
  int status;
  CLaserDevice laserDevice;
  timeval tv;
  double tstamp;
  char filename[255];
  char datestamp[255];
  tm *local;
  time_t t;
  
  
  
  // set the default argument parameters that won't need external access.
  // keep the strdup because it allocates the memory.  sprintf doesn't.
  LADAR_TTY          = strdup("/dev/ttyUSB0");
  TESTNAME          = strdup("temp");
  //not needed...
  //sprintf(LADAR_TTY,"/dev/ttyS%d",_SERIAL_PORT);
  
  int ch;
  /* A string listing valid short options letters. */
  const char* const short_options = "h";
  /* An array describing valid long options. */
  static struct option long_options[] = 
    {
      // first: long option (--option) string
      // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
      // third: if pointer, set variable to value of fourth argument
      //        if NULL, getopt_long returns fourth argument
      {"log",        1, NULL, 			LOG_OPT},
      {"tty",        1, NULL, 			LADAR_TTY_OPT},
      {"help",       0, NULL, 			'h'},
      {NULL,         0, NULL,                     0} /* required at end */
    };
  
  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  program_name = argv[0];
  int option_index = 0;
  printf("\n");
  
  // Loop through and process all of the command-line input options.
  while((ch = getopt_long(argc, argv, "", long_options, &option_index)) != -1)
    {
      switch(ch) 
	{
	case LOG_OPT:
	  { LOG_SCANS = 1;
	  TESTNAME = strdup(optarg); }
	  break;
	  
	case LADAR_TTY_OPT:
	  LADAR_TTY = strdup(optarg);
	  break;
	  
	case 'h':
	  /* User has requested usage information. Print it to standard
	     output, and exit with exit code zero (normal termination). */
	  print_usage(stdout, 0);
	  
	case '?': /* The user specified an invalid option. */
	  /* Print usage information to standard error, and exit with exit
	     code one (indicating abnormal termination). */
	  print_usage(stderr, 1);
	  
	case -1: /* Done with options. */
	  break;
	}
      cout << "  option: " << long_options[option_index].name;
      if(optarg) cout << ", with arg: " << optarg;
      cout << endl;
    } 
  
  
  // check that a TESTNAME was specified
  if(TESTNAME == 0) {
    cout << endl << endl;
    cout << "--log switch must have a testname!" << endl;
    exit(-1);
  }
 
  if(argc < 2) {
    printf("Serial device must be specified on cmd line (ex /dev/ttyS9)\n");
    exit(-1);
  }

  printf("Attempting to communicate with ladar on %s\n",LADAR_TTY);
  
  t = time(NULL);
  local = localtime(&t);
  // filename will be TESTNAME_ddmmyyyy_hhmmss.log
  sprintf(datestamp, "%02d%02d%04d_%02d%02d%02d",
	  local->tm_mon+1, local->tm_mday, local->tm_year+1900,
	  local->tm_hour, local->tm_min, local->tm_sec);
  sprintf(filename, "%s_scans_%s.log", TESTNAME, datestamp);
  printf("Logging scans to file: %s\n", filename);
  scanlog=fopen(filename,"w");
  if ( scanlog == NULL ) {
    printf("Unable to open scan log!!!\n");
    exit(-1);
  }
  
  result = laserDevice.Setup(LADAR_TTY, ANGLE, RES, 1);
  
  fprintf(scanlog,"%% angle = %d\n", ANGLE);
  fprintf(scanlog,"%% resolution = %lf\n", RES);
  fprintf(scanlog,"%% units = %lf\n", UNITS);
  fprintf(scanlog,"%% Time[sec] | scan points\n");

  if (result==0) { // success
    
    gettimeofday(&tv, NULL);
    status = laserDevice.LockNGetData( data );
    tstamp = tv.tv_sec + tv.tv_usec/1000000.0;
    //printf("STATUS: %04Xh\n", status);
    /* and print them */
    for(int i=1;i<laserDevice.getDataLen()+1;i+=2) {
      fprintf(scanlog, "%g ", tstamp);
      fprintf(scanlog, "%i ", (unsigned short int) data[i]
	     + ((unsigned short int) data[i+1] << 8));
    }
  }
  
  laserDevice.LockNShutdown();
  return(0);
}
