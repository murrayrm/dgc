/*
 * vddisp.h - vdrive display screen for Tahoe
 *
 * RMM, 30 Dec 03
 *
 */

/* Local function declarations */
int user_quit(long);
int user_cap_toggle(long);
int brake_calibrate_cb(long);

extern DD_IDENT parmtbl[];	/* Parameter table */
extern int steer_cmdangle;	/* Commanded steering angle (steer.c) */
extern double brk_current_brake_pot, brk_pot_voltage;

extern int steer_state, steer_write_count;

extern bool cruise_value_good; //is the time data valid?
extern double cruise_act_vel; //actual speed
extern double cruise_cmd_vel; //commanded speed
extern double cruise_cmd_out; //what cruise decides to do
extern double vel_error_p; //proportional error
extern double vel_error_i; //integral error
extern double vel_error_d; //derivative error
extern double G_vel_p; //proportional gain
extern double G_vel_i; //integral gain
extern double G_vel_i_pos;
extern double G_vel_i_neg;
extern double G_vel_d; //derivativ gain
extern double time_interval; //
extern double timeouts;

%%
VDRIVE 2.1c	J: %joyc  S: %strc  A: %accc  B: %brk   M: %mtac
D=%md S=%ms	T: %thr   S: %str   X: %trx  VM: %vma  VS: %vst

         Off    Ref   Act   Gain  Cmd	      | Joy:  %jx  %jy    %jbut
Drive   %aref  %vref %vact %dgain %gc   %bc   | In Charge: %jic
Steer   %pref        %pact %sgain %sc         | 
                                           |CR OK:%cok CMD:%ccmd SPD:%cact  
P:%gp  I:%gi  D:%gd  Ipos:%gip Ineg:%gin   |TI:%tii timeouts:%cto OUT:%cco	
					   |E: P:%ep   I:%ei   D:%ed
Axis   IMU      GPS      |  KF     Vel     |
 X/N   %imdx    %gpx     |%kfx     %kfvx   |
 Y/E   %imdy    %gpy     |%kfy     %kfvy   | Dump file %dp %dumpfile
 Z/U   %imdz    %gpz     |%kfz     %kfvz   | Brake: r=%bpos  y=%bcur  s=%bcas
 R     %imdr    -------  |%kfr             |   min=%bmin max=%bmax
 P     %imdp    M %magn  |%kfp             | %estop %estopval
 Y     %imdw             |%kfw             | Trans: %gear 
                                           | Steer: s=%ssta  w=%swrt 
MTA: vel=%mtav str=%mtas
Drivers	    Flag    Cmd     Cal    Zero	 Functions   Flag   Stop
  Brake	    %bflg   %bcmd   %bcal  %bzer   Cruise    %cflg  %cstp
  Throttle  %tflg   %tcmd   %tcal        Modes
  Steer	    %sflg   %scmd   %scal  %sz     O = off     M = manual (human)
                                           R = remote  A = autonomous (MTA)
%QUIT   %ACDV   %CAPTURE
%%

# <F1> manual	    <F5> toggle capture	    <F9>  reset defaults
# <F2> remote	    <F6> dump data	    <F10> zero counters
# <F3> autonomous	    <F7> start trajectory   <ESC> control off

tblname: vddisp;
bufname: vddbuf;

#
# Labels and counters
#
# These entries describe the top line of the screen
#
short: %joyc joy_count "%5d" -ro;
short: %strc steer_count "%5d" -ro;
short: %accc acc_count "%5d" -ro;
short: %mtac mta_counter "%5d" -ro;

#cruise


short: %vst vstate_count "%5d" -ro;
short: %vma vmanage_count "%5d" -ro;


#
# Control section
#
# This section of the display shows what is being commanded and
# send to the actuators.  Roughly corresponds to the control loop
# for the vehicle

double: %aref 	accel_off 		"%5.2f"	;	# acceleration axis offset
double: %pref 	steer_off 		"%5.2f"	;	# steering axis offset
double: %vref 	speed_ref 		"%5.2f"	;	# velocity reference
double:	%vact 	vehstate.Speed 		"%4.2f";
short: 	%cok 	cruise_value_good	"%d"	-ro; 	#is the time data valid?
double:	%cact 	cruise_act_vel 		"%4.2f"	-ro; 	#actual speed
double: %ccmd 	cruise_cmd_vel 		"%4.2f"	-ro; 	#commanded speed
double:	%ep	vel_error_p		"%4.2f" -ro; 	#P error
double: %ei 	vel_error_i		"%4.2f"	; 	#I error
double: %ed 	vel_error_d		"%4.2f" -ro; 	#D error
double: %gp 	G_vel_p 		"%4.2f"	;	#P gain
double: %gi 	G_vel_i			"%4.2f"	;	#I gain, normal
double: %gip	G_vel_i_pos		"%4.2f"	;	#I gain, positive
double: %gin	G_vel_i_neg		"%4.2f"	;	#I gain, negative
double: %gd 	G_vel_d			"%4.2f"	;	#D gain
double: %tii	time_interval		"%5.4f" -ro;	#time intergral for d and I
double: %cco	cruise_cmd_out		"%4.3f" -ro;	#the output from cruise.
short:  %cto    timeouts		"%d"	-ro;	#number of timeouts

double: %cstp  cruise_stop "%5.2f";


double: %dgain cruise_gain "%5.2f";	# cruise control gain multiplier
double: %bc brk_pos "%5.2f";		# commanded brake position
double: %gc thr_pos "%5.2f";		# commanded throttle position
double: %sc str_angle "%5.2f";		# commanded steering angle

#
# Joystick position
#

double:	%jx		joy.data[0]		"%5.0f"		-ro;
double:	%jy		joy.data[1]		"%5.0f"		-ro;
short: %jbut		joy.buttons		"%d"		-ro;

#
# Commanded position
#

double: %mtav mtacmd.velocity_cmd "%4.2f" -ro;
double: %mtas mtacmd.steer_cmd "4.2f" -ro;

#
# Vstate entries
#
double: %gpx vehstate.gpsdata.lat "%7.4f" -ro;
double: %gpy vehstate.gpsdata.lng "%7.4f" -ro;
double: %gpz vehstate.gpsdata.altitude "%7.4f" -ro;

double: %imdx vehstate.imudata.dvx "%7.4f" -ro;
double: %imdy vehstate.imudata.dvy "%7.4f" -ro;
double: %imdz vehstate.imudata.dvz "%7.4f" -ro;
double: %imdr vehstate.imudata.dtx "%7.4f" -ro;
double: %imdp vehstate.imudata.dty "%7.4f" -ro;
double: %imdw vehstate.imudata.dtz "%7.4f" -ro;

double: %kfx vehstate.kf_lat "%7.2f";
double: %kfy vehstate.kf_lng "%7.2f";
float: %kfz vehstate.Altitude "%7.2f";
float: %kfvx vehstate.Vel_N "%5.2f";
float: %kfvy vehstate.Vel_E "%5.2f";
float: %kfvz vehstate.Vel_U "%5.2f";
float: %kfr vehstate.Roll "%7.2f";
float: %kfp vehstate.Pitch "%7.2f";
float: %kfw vehstate.Yaw "%7.2f";
float: %magn vehstate.magreading.heading "%5.2f";

#
#estop button
#
button: %estop "ESTOP:" user_estop_toggle; #button to activate estop_pause
short: %estopval estop_call_flag "%d" ;

#
# Device status 
#
short: %bflg brake_flag "%1d";
double: %bcmd brk_pos "%5.2f";		# commanded brake position
button: %bcal "CAL" brake_calibrate_cb;
button: %bzer "RST" brake_reset_cbe;   #cbe for brake reset

short: %tflg throttle_flag "%1d";
double: %tcmd thr_pos "%5.2f";		# commanded throttle position

short: %cflg cruise_flag "%1d";

short: %sflg steer_flag "%1d";
double: %scmd str_angle "%5.2f";
button: %scal "CAL" steer_cal;
button: %sz "ZERO" steer_setzero;

#
# File capture
#
string: %dumpfile dumpfile "%s";
short: %dp capflag "[%d]:";

#
# Button bar (bottom of screen)
#

button:	%QUIT	"QUIT"	user_quit	-idname=QUITB;
button:	%ACDV	"PARAM" dd_usetbl_cb -userarg=parmtbl;
# button: %STRBT "Steer:" dd_usetbl_cb -userarg=strtbl;
# button: %HELP "Help" dd_usetbl_cb -userarg=helptbl;
button:	%CAPTURE "CAPTURE" user_cap_toggle;

#
# Low level device values (second line)
#
short: %str steer_cmdangle "%4d" -ro;
short: %thr brk_current_brake_pot "%4.2f" -ro;

# Operating mode
short: %ms mode_steer "%c" -manager=vdrive_mode;
short: %md mode_drive "%c" -manager=vdrive_mode;

#
# Debugging section
#
# This portion of the screen can be used for local debugging
# OK to delete code that is here in your local version
#

# Brake code
double: %bcur brk_current_brake_pot "%5.2f" -ro;
short: %gear current_gear "%d";

#joystick in charge
short: %jic JOYSTICK_IN_CHARGE "%d" -ro;

#steering debugging
short: %ssta  steer_state "%d" -ro;
short: %swrt steer_write_count "%d" -ro;
