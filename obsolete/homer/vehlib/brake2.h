//Code to run the brake actuator]

#ifndef BRAKE_H
#define BRAKE_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

//NOTE: throughout this file, variables ending in counts refer to the digipot's native
//counts, 0-255; variables without this ending refer to brake position from 0-1
//The system design dictates that the counts value of 255 is equivalent to brake
//position of 0.0, corresponding to a fully closed, or idle, brake.  The counts value
//of 0 is equivalent to a brake position of 1, corresponding to fully open, or full
//brake



//Functions general to all drivers

//Initialization function for the gas actuator and sets up the serial port used by the gas, 
//brake pot, transmission, and ignition.
//**********NOTE: brake_calibrate must be run before the first movement after vdrive starts*************
int brake_open(int port);

//Calibration function for the gas actuator
int brake_calibrate();

//Moves the brake to the zero (brake fully closed) position
int brake_zero();

//Simple status checker that returns 0 for ok and -1 for error in brake function
int brake_ok();

//Suspend brake operation upon a pause estop command
int brake_pause();

//Suspend brake operation upon a estop disable command
int brake_disable();

//Recume brake operation after a pause command
int brake_resume();

//Closes the port, unallocates memory, stops threads
int brake_close();

//Functions specific to this driver

//Returns brake position
double brake_read();

//Change the gas actuator position based on the 0-1 scale
int brake_write(double desired_pos);

//Used in the MTA-ized vdrive-2.0; just calls brake_calibrate
int brake_fault_reset();

//Helper functions

//Helper function which returns min_val if val<min_val, max_val if val>max_val, or otheriwse returns val
double brake_in_range(double val, double min_val, double max_val);

int brake_home(void);
#endif









