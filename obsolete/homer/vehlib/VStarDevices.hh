#ifndef __VSTARDEVICES_HH__
#define __VSTARDEVICES_HH__


// All hardware controlled by VManage
namespace VManageDevices {
  enum {

    Transmission,
    Ignition,
    EStop,
    OBD,    

    // Add Devices above this line.
    Num
  };
};

static const char *VManageDeviceStrings[] = {
  "Transmission",
  "Ignition",
  "Estop", 
  "OBD-II"
};




// All hardware controlled by VDrive
namespace VDriveDevices {
  enum {
    Steer,
    Throttle,
    Brake,

    // Add Devices above this line.
    Num
  };
};



static const char *VDriveDeviceStrings[] = {
  "Steering",
  "Throttle",
  "Brake"
};




// Possible States for a device to be in.
namespace DeviceStates {

  enum {
    Init,          // Opening Serial Port, etc.
    Active,        // Active State (running and setting position)
    Calibrate,     // currently Calibrating
    Inactive,      // Maybe the device was closed?
    Error,         // something bad happened
    Num
  };
};

// And display strings for these states.
static const char *DeviceStateStrings[] = {
  "Init",
  "Active",
  "Calibrate",
  "Inactive",
  "Error"
};


#endif
