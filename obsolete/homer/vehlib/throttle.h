//Code to run the throttle actuator]

#ifndef THROTTLE_H
#define THROTTLE_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

//NOTE: throughout this file, variables ending in counts refer to the digipot's native
//counts, 0-255; variables without this ending refer to throttle position from 0-1
//The system design dictates that the counts value of 255 is equivalent to throttle
//position of 0.0, corresponding to a fully closed, or idle, throttle.  The counts value
//of 0 is equivalent to a throttle position of 1, corresponding to fully open, or full
//throttle



//Functions general to all drivers

//Initialization function for the gas actuator and sets up the serial port used by the gas, 
//brake pot, transmission, and ignition.
//**********NOTE: throttle_calibrate must be run before the first movement after vdrive starts*************
int throttle_open(int port);

//Calibration function for the gas actuator
int throttle_calibrate();

//Moves the throttle to the zero (throttle fully closed) position
int throttle_zero();

//Simple status checker that returns 0 for ok and -1 for error in throttle function
int throttle_ok();

//Suspend throttle operation upon a pause estop command
int throttle_pause();

//Suspend throttle operation upon a estop disable command
int throttle_disable();

//Recume throttle operation after a pause command
int throttle_resume();

//Closes the port, unallocates memory, stops threads
int throttle_close();

//Functions specific to this driver

//Returns throttle position
double throttle_read();

//Change the gas actuator position based on the 0-1 scale
int throttle_write(double desired_pos);

//Used in the MTA-ized vdrive-2.0; just calls throttle_calibrate
int throttle_fault_reset();

//Helper functions


//Helper function which returns min_val if val<min_val, max_val if val>max_val, or otheriwse returns val
double throttle_in_range(double val, double min_val, double max_val);

#endif









