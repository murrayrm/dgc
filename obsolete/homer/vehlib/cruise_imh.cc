/*
 * cruise.cc - velocity control computation
 *
 * Version 1.0
 * 11/13/03 Haomiao Huang
 * File Creation
 *
 * 11/14/03 Haomiao Huang
 * Added force lookup control
 *
 * 11/27/2004 -- REWRITE FOR CDS110 Project - Written By Ike
 *
 *
 */

#include "cruise.h"
#include <time.h>
#include <math.h>
#include "VState.hh"
using namespace std;


// Look Up Tables
LUtable PID_A_TO_T_Table("PID_A_TO_T_LUT");
LUtable FF_V_TO_T_Table("FF_V_TO_T_LUT");


// MTA State Struct
extern struct VState_GetStateMsg vehstate;	/* current vehicle state */


////////////////////////////////////////////////////////////////////////////////
//constant definitions

#define PID_TIMEOUT             3.0        // if no data in 3 seconds, stop
#define PID_MIN_TIMESPAN        0.05       // don't call this function faster than 20Hz
  
#define MAX_CRUISE_VELOCITY    12.0        //max velocity the cruise controller will use in m/s

#define Kp                      1.0        // Proportional velocity error gain

#define Ki                      1.0        // Integral velocity error gain
#define I_SAT_MIN              -6.0        // Integrator Saturation (including gains)
#define I_SAT_MAX               3.0          

#define Kd                      1.0        // Derivative velocity error gain
#define D_SAT_MIN              -6.0        // Derivative Saturation (including gains)
#define D_SAT_MAX               3.0         


#define throttle_maximum        1.0        // maximum throttle to send
#define throttle_minimum       -1.0        // maximum brake to send



// Global variables that sparrow uses.
double cruise_act_vel = 0;  //actual speed
double cruise_cmd_vel = 0;  //commanded speed
double cruise_cmd_out = 0;  //what we output (for debugging)
double vel_error_d    = 0;  //proportional error
double vel_error_p    = 0;  //derivative error
double vel_error_i    = 0;  //integral error
double time_interval  = 0;  //time interval
int    timeouts       = 0;  //number of times the func has timed out

// HACK
bool cruise_value_good = true;
double G_vel_p = Kp;
double G_vel_i = Ki;
double G_vel_d = Kd;
double G_vel_i_pos = Ki;
double G_vel_i_neg = Ki;



// HELPER FUNCTION
double saturate(double * num, double min, double max) {

  *num = (*num > min)? *num: min;
  *num = (*num < max)? *num: max;
}


double cruise(double commanded_vel) {  

  // 1. command the absolute velocity up to a MAX Velocity. 
  commanded_vel = ( fabs(commanded_vel) < MAX_CRUISE_VELOCITY )
    ? fabs(commanded_vel)
    : MAX_CRUISE_VELOCITY;

  // Save the values that are passed for sparrow
  cruise_cmd_vel = commanded_vel;  // Argument to Function
  cruise_act_vel = vehstate.Speed; // From MTA
   
  //obtain a throttle setting from the PID controller
  double throttle = cruise_PID(commanded_vel, vehstate.Speed); 
  
  //put throttle in range
  saturate( &throttle, throttle_minimum, throttle_maximum);

  return throttle;
}




// *********************************************************************
// *********************************************************************
//    FUNCTION: CRUISE PID  
// *********************************************************************
// *********************************************************************
double cruise_PID(double vel_command, double vel_actual) {
  double        time_new;             //current time this function was called
  static double time_old = 0;         //previous call time, to usecs, in secs

  double        Error;                // Velocity Error this round
  static double Error_Old        = 0; // Previous Velocity Error (for I and D)
  static double Error_Integrator = 0; // Value of Integrator

  /* Figure out how much time has passed */
  time_interval = TVNow().dbl() - time_old;
  time_old      = TVNow().dbl();             // update old time

  // If the time difference is invalid, just return zero (something is wrong
  if( time_interval > PID_TIMEOUT || time_interval < PID_MIN_TIMESPAN ) {
    return 0.0;
  }

  // *************************************************************
  // START PID + Feed Forward
  // *************************************************************
  Error = vel_command - vel_actual;
  Error_Integrator = ( Error_Integrator // from last time
		     + Ki * (Error + Error_Old) * time_interval );

  saturate( &Error_Integrator, I_SAT_MIN, I_SAT_MAX );

  double P_Contribution = Kp * Error;
  double D_Contribution = Kd * (Error - Error_Old) / time_interval;
  double I_Contribution = Error_Integrator;

  saturate( &D_Contribution, D_SAT_MIN, D_SAT_MAX );

  double PID_Desired_Acceleration  = P_Contribution + I_Contribution + D_Contribution;

  double PID_Throttle_Contribution = PID_A_TO_T_Table.getValue(PID_Desired_Acceleration);
  double FF_Throttle_Contribution  = FF_V_TO_T_Table.getValue (vel_command);

  double Throttle_Command = PID_Throttle_Contribution + FF_Throttle_Contribution;
  saturate( &Throttle_Command, -1, 1);

  // *************************************************************
  // END PID + FF
  // *************************************************************

  // Sparrow Display variables
  vel_error_d    = D_Contribution;  
  vel_error_p    = P_Contribution;  
  vel_error_i    = I_Contribution;
  
  // Get ready for next iteration
  Error_Old      = Error;

  // Return
  return Throttle_Command;
}













////////////////////////////////////////////////////////////////////////////////
//  Internal Functions

////////////////////////////////////////////////////////////
// cruise_init
//
void cruise_init() {
  //  cruise_zero();
  return;
}

