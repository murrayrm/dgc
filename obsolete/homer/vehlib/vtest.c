/*
 * vtest - vdrive test program
 *
 * Richard M. Murray
 * 13 February 2004
 *
 * This program MTA messages to test out vdrive.  The intended usage
 * is to verify operation of vdrive-1.0, vdrive-2.0 and vdrive-MTA.
 *
 */

#include <stdlib.h>
#include <getopt.h>
#include <math.h>

/* MTA header files */
#include "MTA/Modules.hh"
#include "MTA/Kernel.hh"
#include "MTA/Misc/Time/Time.hh"
#include "MTA/Misc/Time/Timeval.hh"
#include "MTA/DGC_MODULE.hh"

#include "VDrive.hh"
struct VDrive_CmdMotionMsg vehcmd;      /* commanded motion from MTA */

#include "VState.hh"			/* grab message format for VState */
struct VState_GetStateMsg vehstate;	/* current vehicle state */

/* VTest MTA module */
class VTest : public DGC_MODULE {
public:
  VTest();				/* constructor */
  ~VTest();				/* destructor */
  void Active();
};

/* Local variables */
int vflg = 0;				/* verbose error messages */
int rflg = 0;				/* reverse mode testing */
double omega = 0.01;			/* oscillations frequency */
double accamp = 1;			/* acceleration amplitude */
#define SLEEP_TIME 100000		/* sets update rate */
#define TWOPI (2*3.1415)		/* Hz to rad/sec converstion */

/* Command line arguments */
static char *short_options = "f:a:vh?";
static struct option long_options[] = { {NULL, 0, NULL, 0} };
static char *usage = "\
Usage: %s [-v] [options]\n\
  -a amp  set amplitude for speed command\n\
  -f num  set the frequency\n\
  -r	  test reverse mode\n\
  -h	  print this message\n\
";


int main(int argc, char **argv)
{
  int c, errflg = 0;

  while ((c = getopt_long(argc, argv, short_options, long_options, NULL)) 
	 != EOF)
    switch (c) {
    case 'v':	vflg = 1;	break;
    case 'h':	errflg++;	break;
    case 'f':
      omega = atof(optarg);
      break;
    case 'a':
      accamp = atof(optarg);
      break;
    default:	errflg++;	break;
      break;
    }

  /* Print an error message if anything went wrong */
  if (errflg || argc < optind) {
    fprintf(stderr, usage, argv[0]);
    exit(1);
  }

  Register(shared_ptr<DGC_MODULE>( new VTest ));
  StartKernel();
  return 0;
}

/*
 * MTA interface
 *
 * This defines the MTA module for vtest
 *
 */

using namespace std;

VTest::VTest() : DGC_MODULE(MODULES::VTest, "Vehicle Test", 0) {}
VTest::~VTest() {}
void VTest::Active() {
  double time;
  static int start = 0;
  struct timeval tv;

  /* Initialize the starting time */
  gettimeofday(&tv, NULL); start = tv.tv_sec;
  printf("Sending commands to vdrive; rate = %g hz\n", omega);

  /* Set vdrive mode to off to test out MTA interface */
  Mail msg = NewOutMessage(MyAddress(), MODULES::VDrive, 
			   VDriveMessages::SetMode);
  int mode = 'o';
  msg << mode;
  SendMail(msg);

  while (1) {
    gettimeofday(&tv, NULL); 
    time = (double) (tv.tv_sec - start) + ((double) tv.tv_usec) / 1.0e6;

    /* Set the command to be a sinusoid */
    vehcmd.steer_cmd = cos(TWOPI * omega * time);
    vehcmd.velocity_cmd = accamp * sin(TWOPI * omega * time);

    /* If -r flag was set, screw with the velocity */
    if (rflg) {
      vehcmd.velocity_cmd = fabs(vehcmd.velocity_cmd);
      if (vehcmd.velocity_cmd > 0.97) vehcmd.velocity_cmd = -1;
    }

    /* Send the command to vdrive */
    Mail msg = NewOutMessage( MyAddress(), MODULES::VDrive, 
			      VDriveMessages::CmdMotion);
    msg << vehcmd;
    SendMail(msg);

    /* Display the command we are sending */
    printf("\rS=%5.2g V=%5.2f", vehcmd.steer_cmd, vehcmd.velocity_cmd);
    fflush(stdout);

    usleep(SLEEP_TIME);			/* wait for a while */
  }
}
