//Code to run the throttle actuator]

#ifndef BRAKE_H
#define BRAKE_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include "parallel.h"

//NOTE: throughout this file, variables ending in counts refer to the digipot's native
//counts, 0-127; variables without this ending refer to brake position from 0-1
//The system design dictates that the counts value of 127 is equivalent to throttle
//position of 0.0, corresponding to a fully closed, or idle, brake.  The counts value
//of 0 is equivalent to a brake position of 1, corresponding to fully open, or full
//brake

/* Constants determined by the minimum and maximum values the digipot can have
 *  The gas actuator has been mechanically adjusted so this represents the full
 *  range of motion of the gas pedal.  Reducing these values will limit the range 
 *  accorgingly.  Note that BRAKE_MIN_POT_VAL_COUNTS corresponds to full brake
 *  and vice versa
 *  */
#define BRAKE_MIN_POT_VAL_COUNTS 0
#define BRAKE_MAX_POT_VAL_COUNTS 127

/* Constants determining which pins on the digipot correspond to which pins on 
 * the parallel port
 */
//~~  #define ACCEL_CLK PP_PIN07
//~~  #define ACCEL_UD PP_PIN08
#define BRAKE_CLK PP_PIN01
#define BRAKE_UD PP_PIN16
#define BRAKE_CS PP_PIN17

//Functions general to all drivers

//Initialization function for the brake actuator and sets up the parallel port used by the gas, 
//brake pot, transmission, and ignition.
//**********NOTE: brake_calibrate must be run before the first movement after vdrive starts*************
int brake_open(int port);

//Calibration function for the brake actuator
int brake_calibrate();

//Moves the brake to the zero (brake fully closed) position
int brake_zero();

//Simple status checker that returns 0 for ok and -1 for error in brake function
int brake_ok();

//Arguments for brake_status, valued to facilitate using bit masks to determine which data to return
//user must simply pass in the sum of the data elements he wishes returned to the struct
#define BRAKE_POS = 1
#define BRAKE_ENABLED = 2
#define BRAKE_MOVING  = 4
#define BRAKE_ALL = BRAKE_POS + BRAKE_ENABLED + BRAKE_MOVING 

//The struct for the status return
struct brake_status_data {
  int status;
};

//Request status report of type request_type to be returned at *brake_status_data which is zeros except 
//for the data requested.  Note that this function will currently not return a struct.  Call brake_ok
//to get that info and brake_read to get a position.
int brake_status(int request_type, struct brake_status_type *brake_status_data);

//Suspend brake operation upon a pause estop command
int brake_pause();

//Suspend brake operation upon a estop disable command
int brake_disable();

//Recume brake operation after a pause command
int brake_resume();

//Closes the port, unallocates memory, stops threads
int brake_close();

//Functions specific to this driver

//Returns brake position
double brake_read();

//Change the brake actuator position based on the 0-1 scale
int brake_write(double desired_pos);

//Used in the MTA-ized vdrive-2.0; just calls brake_calibrate
int brake_fault_reset();

//Helper functions

//Change the brake actuator position based on an amount to increment or decrement on the 0-127 scale
int brake_increment(int desired_pos_change_counts);

//Helper function which returns min_val if val<min_val, max_val if val>max_val, or otheriwse returns val
double brake_in_range(double val, double min_val, double max_val);

#endif









