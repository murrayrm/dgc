/*
 * throttle.c - code to control the accelerometer position
 * note: originally named accel.c 
 * 
 * Original author: Jeremy G.
 *                  Sue Ann Hong
 * Revised: Richard Murray
 * 2 January 2003
 * Revised: Will Heltsley
 * 30 Jan 2003
 *
 */

#include "vehports.h"
#include "brake2.h"
#include "sparrow/dbglib.h"
#include "MTA/Misc/Time/Time.hh"
#include <iostream>
#include <time.h>
#include <termios.h>
#include <sys/stat.h>
#include <fcntl.h>

using namespace std;


//NOTE: throughout this file, variables ending in counts refer to the digipot's native
//counts, 0-255; variables without this ending refer to throttle position from 0-1
//The system design dictates that the counts value of 255 is equivalent to throttle
//position of 0.0, corresponding to a fully closed, or idle, throttle.  The counts value
//of 0 is equivalent to a throttle position of 1, corresponding to fully open, or full
//throttle

extern int throttleBrakePort;
extern bool bThrottleBrakeOpen;


#define BRAKE_MAGIC_VAL '\x1'

double brk_current_brake_pot;
double brakePos;


int throttleBrakeOpen(int port);
void throttleBrakeClose(void);

void brakeSendToPot(void)
{
	char buf[2];

	buf[0] = BRAKE_MAGIC_VAL;
	buf[1] = (unsigned char) (brakePos * 255.0);
	write(throttleBrakePort, buf, 2);
}





int brake_open(int port)
{
	if(bThrottleBrakeOpen)
		return 0;

	if(throttleBrakeOpen(port)==0)
	{
		bThrottleBrakeOpen = true;

    return 0;
  } 
  else {
    dbg_info("Error opening serial port\n");
    return -1;
  }
}



int brake_calibrate() {
	return 0;
}


/* Zeros the actuator to fully closed (idle) brake.  Due to the nature of the actuator and 
   controller, this function just calls brake_calibrate.
*/
int brake_zero() {
	brake_write(0.0);
}

//Since there isn't any way to check the health of any of the brake systems, ok just checks
//the brake_port_opened and brake_calibrated markers to see if these things have been done.
int brake_ok() {
  if(bThrottleBrakeOpen)
		return 0;
  else
		return -1;
}

//TBD: This will eventually return a struct full of the requested data and zeros elsewhere.  
//Doesnt do anything now
int brake_status(int request_type, struct brake_status_type *brake_status_type){
  cout<<"brake_status called, but code is not written--nothing to do.";
  return 0;
}

//Send the brake to idle
int brake_pause(){
  return 0;
}

//Sends the brake to idle via the pause function.
int brake_disable(){
  return 0;
}

//Should be ready to receive new commands vie brake_write
int brake_resume(){
  return 0;
}

int brake_close() {
	if(bThrottleBrakeOpen)
	{
		bThrottleBrakeOpen = false;
		throttleBrakeClose();

    dbg_info("Brake actuator port closed");
    return 0;
  } 
  else {
    dbg_info("Error closing port");
    return -1;
  }
}

//Returns the gas actuator's position on the 0-1 scale.  Note that this will return garbage
//that is out of range if called before brake_calibrate is run
double brake_read() {
  return brakePos;
}

//Sets the gas actuator's position based on a float, which is on the 0-1 scale
//0 sets the actuator to let the engine idle
//1 is full brake
int brake_write(double desired_pos) {
  //  printf("\nSetting position to %f\n", position);
  brakePos = brake_in_range(desired_pos, 0, 1); //ensure a valid argument
	brk_current_brake_pot = brakePos;
	brakeSendToPot();

  return 0;
}

//Used by the MTA-ized vdrive-2.0 to reset brake
int brake_fault_reset() {
  return 0;
}


//Function to make sure min_val<=val<=max_val
double brake_in_range(double val, double min_val, double max_val) {
	if(val < min_val) return min_val;
	else if(val > max_val) return max_val;
	else return val;
}

int brake_home(void)
{
	brake_write(0.0);
	return 0;
}

