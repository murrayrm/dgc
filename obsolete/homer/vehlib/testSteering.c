#include <stdlib.h>
#include <stdio.h>
#include "steer.h"
#include "MTA/Misc/Time/Time.hh"

using namespace std;

int main() {
  printf("Opening at port 0...%d\n",   steer_open(0));
  int farLeft;
  int farRight;
  int center;

  /*
  steer_exec_cmd(C_EOL, TRUE);
  steer_exec_cmd(C_EOT, TRUE);

  steer_exec_cmd(S_EOL, TRUE);
  steer_exec_cmd(S_EOT, TRUE);
  */
  //int status = steer_test_communication(TRUE);
  //printf("Comm is %d\n", status);

  //printf("Setting to zero...\n");
  //steer_setzero();

  float setTo=0;

  /*
  steer_exec_cmd(S_LH0, FALSE);
  steer_exec_cmd(S_MA0, FALSE);

  steer_setzero();
  sleep_for(2);
  //steer_exec_cmd(S_V32, FALSE);

  steer_exec_cmd(S_DRIVE0, FALSE);
  printf("Turn to far left\n");

  int dummy;

  printf("Enter 1 when ready: ");
  scanf("%d", &dummy);
  steer_exec_cmd(S_DRIVE1, FALSE);
  sleep_for(2);
  steer_state_update(FALSE, FALSE);
  sleep_for(2);
  steer_state_update(FALSE, FALSE);
  sleep_for(2);
  farLeft = steer_getposition();
  printf("Far left at %d\n", farLeft);

  steer_exec_cmd(S_DRIVE0, FALSE);
  printf("Turn to far right\n");

  printf("Enter 1 when ready: ");
  scanf("%d", &dummy);

  steer_exec_cmd(S_DRIVE1, FALSE);
  sleep_for(2);
  steer_state_update(FALSE, FALSE);
  farRight = steer_getposition();
  printf("Far Right at %d\n", farRight);

  printf("Center at %d\n", (farRight-farLeft)/2 + farLeft);

  center = (farRight-farLeft)/2 + farLeft;

  printf("Going to center: %d\n", (farRight-farLeft)/2*-1);
  steer_setposition((double)(farRight-farLeft)/2.0*-1.0);
  sleep_for(2);
  printf("Setting center to zero\n");

  steer_setzero();
  int newFarLeft;
  int newFarRight;

  newFarLeft = farLeft - center;
  newFarRight = farRight - center;

  steer_exec_cmd(S_MA1, FALSE);

  printf("Going to new farLeft %d\n", newFarLeft);
  steer_setposition((double)newFarLeft);
  sleep_for(2);
  steer_state_update(FALSE, FALSE);
  printf("We are now at %d\n", steer_getposition());
  sleep_for(2);
  printf("Going to new farRight %d\n", newFarRight);
  steer_setposition((double)newFarRight);
  sleep_for(2);
  steer_state_update(FALSE, FALSE);
  printf("We are now at %d\n", steer_getposition());
  sleep_for(2);
  printf("Going to new center: 0\n");
  steer_setposition((double)0);
  sleep_for(2);
  steer_state_update(FALSE, FALSE);
  printf("We are now at %d\n", steer_getposition());
  sleep_for(2);
  */


  //steer_exec_cmd(S_MA1, FALSE);

  while(setTo != -1.0) {
    printf("\nGoing to %f\n", setTo);
    steer_state_update(TRUE, FALSE);
    printf("Now at %d\n", steer_getposition());
    steer_heading(setTo);
    //steer_setposition(setTo);
    printf("Enter pos: ");
    scanf("%f", &setTo);
  }

  /*
  steer_exec_cmd(S_MA0, FALSE);
  steer_exec_cmd(S_DRIVE0, FALSE);

  printf("\nGoing to %f\n", setTo);
  steer_state_update(TRUE, FALSE);
  printf("Now at %d\n", steer_getposition());

  steer_exec_cmd(S_DRIVE1, FALSE);
  steer_exec_cmd(S_MA1, FALSE);

  setTo=0;
  while(setTo != -1.0) {
    printf("\nGoing to %f\n", setTo);
    steer_state_update(TRUE, FALSE);
    printf("Now at %d\n", steer_getposition());
    //steer_heading(setTo);
    steer_setposition(setTo);
    printf("Enter pos: ");
    scanf("%f", &setTo);
  }
  */

  steer_close();

  return 0;
}
