/*
 * throttle.c - code to control the accelerometer position
 * note: originally named accel.c 
 * 
 * Original author: Jeremy G.
 *                  Sue Ann Hong
 * Revised: Richard Murray
 * 2 January 2003
 * Revised: Will Heltsley
 * 30 Jan 2003
 *
 */

#include "vehports.h"
#include "throttle.h"
#include "sparrow/dbglib.h"
#include "MTA/Misc/Time/Time.hh"
#include <iostream>
#include <time.h>
#include <termios.h>
#include <sys/stat.h>
#include <fcntl.h>

using namespace std;


//NOTE: throughout this file, variables ending in counts refer to the digipot's native
//counts, 0-255; variables without this ending refer to throttle position from 0-1
//The system design dictates that the counts value of 255 is equivalent to throttle
//position of 0.0, corresponding to a fully closed, or idle, throttle.  The counts value
//of 0 is equivalent to a throttle position of 1, corresponding to fully open, or full
//throttle

int throttleBrakePort;
bool bThrottleBrakeOpen;


#define THROTTLE_MAGIC_VAL '\x0'

double throttlePos;


int throttleBrakeOpen(int port)
{
	struct termios term; // serial port control settings struct
  char SerialPathName[40]; // char array for serial path

  // create serial device path and open
  sprintf(SerialPathName, "/dev/ttyS%d", port);
  throttleBrakePort = open(SerialPathName, O_WRONLY | O_NOCTTY);

  // error check on serial open
  if (throttleBrakePort < 0) {
    cout<<"Cannot open serial port" << endl;
    return -1;
  }
  
  if (tcgetattr(throttleBrakePort, &term) < 0){
    cout<<"tcgetattr error" << endl;
    return -1;
  }

   // set i/o baud 
  cfsetospeed(&term, B115200);
  tcflush(throttleBrakePort, TCIFLUSH);

  // set control flags
  term.c_cflag &= ~CRTSCTS;  // turn off hardware control
  term.c_cflag &= ~CSTOPB;  // one stop bit

  term.c_cflag = term.c_cflag |
    B115200 |  // set baud rate
    CS8 |       // 8 bit data 
    CLOCAL |    // enable receiver
    CREAD;      // ignore modem status line

  // set output flag 
  term.c_oflag &= ~OPOST & // turn off post processing
    ~ONLCR;  // turn off  carriage return to newline translation
 
  term.c_iflag &= ~IXOFF & // no soft fow on output
    ~IXON &  // or on input 
    ~INLCR & // disable cr to nl translation
    ~INPCK & 
    ~PARMRK &
    ~ICRNL;

  term.c_iflag = term.c_iflag |
    IGNCR | // ignore carriage return
    IGNPAR | // ignore parity
    IGNBRK | // ignore breaks
    BRKINT;

  // no local modes
  term.c_lflag = 0;
  
  // set the flags and check for errors
  if (tcsetattr(throttleBrakePort, TCSANOW, &term) < 0){
    cout<<"imu_open: tcsetattr error" << endl;
    return -1;
  }

  return 0; 
}

void throttleBrakeClose(void)
{
	close(throttleBrakePort);
}

void throttleSendToPot(void)
{
	char buf[2];

	buf[0] = THROTTLE_MAGIC_VAL;
	buf[1] = (unsigned char) (throttlePos * 255.0);
	write(throttleBrakePort, buf, 2);
}





int throttle_open(int port) {
	if(bThrottleBrakeOpen)
		return 0;

	if(throttleBrakeOpen(port)==0)
	{
		bThrottleBrakeOpen = true;

    return 0;
  } 
  else {
    dbg_info("Error opening serial port\n");
    return -1;
  }
}



int throttle_calibrate() {
	return 0;
}


/* Zeros the actuator to fully closed (idle) throttle.  Due to the nature of the actuator and 
   controller, this function just calls throttle_calibrate.
*/
int throttle_zero() {
	throttle_write(0.0);
}

//Since there isn't any way to check the health of any of the throttle systems, ok just checks
//the throttle_port_opened and throttle_calibrated markers to see if these things have been done.
int throttle_ok() {
  if(bThrottleBrakeOpen)
		return 0;
  else
		return -1;
}

//TBD: This will eventually return a struct full of the requested data and zeros elsewhere.  
//Doesnt do anything now
int throttle_status(int request_type, struct throttle_status_type *throttle_status_type){
  cout<<"throttle_status called, but code is not written--nothing to do.";
  return 0;
}

//Send the throttle to idle
int throttle_pause(){
  return 0;
}

//Sends the throttle to idle via the pause function.
int throttle_disable(){
  return 0;
}

//Should be ready to receive new commands vie throttle_write
int throttle_resume(){
  return 0;
}

int throttle_close() {
	if(bThrottleBrakeOpen)
	{
		bThrottleBrakeOpen = false;
		throttleBrakeClose();

    dbg_info("Throttle actuator port closed");
    return 0;
  } 
  else {
    dbg_info("Error closing port");
    return -1;
  }
}

//Returns the gas actuator's position on the 0-1 scale.  Note that this will return garbage
//that is out of range if called before throttle_calibrate is run
double throttle_read() {
  return throttlePos;
}

//Sets the gas actuator's position based on a float, which is on the 0-1 scale
//0 sets the actuator to let the engine idle
//1 is full throttle
int throttle_write(double desired_pos) {
  //  printf("\nSetting position to %f\n", position);
  throttlePos = throttle_in_range(desired_pos, 0, 1); //ensure a valid argument

	throttleSendToPot();

  return 0;
}

//Used by the MTA-ized vdrive-2.0 to reset throttle
int throttle_fault_reset() {
  return 0;
}


//Function to make sure min_val<=val<=max_val
double throttle_in_range(double val, double min_val, double max_val) {
	if(val < min_val) return min_val;
	else if(val > max_val) return max_val;
	else return val;
}
