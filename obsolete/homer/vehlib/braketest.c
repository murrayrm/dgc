/*
 * braketest.c - simple test program for the brakes
 *
 * RMM, 24 Feb 04
 *
 * This is a test program that is used to check out the brake device
 * driver.  Not needed for the race.
 */
#include <stdlib.h>
#include <pthread.h>
#include "brake2.h"
#include "vehports.h"
#include "parallel.h"
#include "sparrow/dbglib.h"

pthread_t brake_thread;			/* brake thread */
int brake_flag = 0;			/* enabled flag */

main(int argc, char **argv)
{
  char inpbuf[100];
  double pos;

  /* Turn on debugging */
  dbg_flag = dbg_outf = dbg_all = 1;

  /* Open up brake/throttle parallel port */
  pp_init(PP_GASBR, PP_DIR_IN);

# ifdef BRAKE_OLD
  brake_open(5);
# else
  brake_open(6);
# endif
  /* brake_calibrate(); */

  while (1) {
    double curpos;

    scanf("%s", inpbuf);

    switch (inpbuf[0]) {
    case 'q':				/* all done */
    case 'Q':
      brake_close();
      return 0;

    case '?':				/* check brake status */
      int status;
#ifdef BRAKE_OLD
      brake_status(&status);
#endif
      break;

    case 'c':				/* clear polling status register */
#ifdef BRAKE_OLD
      brake_clrpsw(0xffff);
#else
      printf("not implemented");
#endif
      break;

    case 'C':
      brake_calibrate();
      break;

    case 'p':				/* print the position */
      curpos = brake_read();
      printf("pos = %g\n", curpos);
      break;

    case 'r':
      break;

      /* 
       * Reset gas spring 
       *
       * This command uses the brakepot to reset the gas spring.  It
       * commands a slow velocity and then monitors the pot until that 
       * position is reached.  Basically the same functionality as 
       * brake_home(), except using different pot settings.
       */
#   ifndef BRAKE_OLD
    case 'G': {
#     define MAX_RESET_COUNT 100
#     define RESET_CYCLE_USEC 100000
#     define BRAKE_RESET_POS 0.75
#     define BRAKE_RESET_VEL 1000000
      int count = 0;			/* counter to avoid hanging */
      double curpos;

      curpos = brake_read(); printf("Current position = %g\n", curpos);
      printf("Move brake to %g (y/n)? ", BRAKE_RESET_POS);
      scanf("%s", inpbuf);
      if (inpbuf[0] != 'y') break;

      while (count++ < MAX_RESET_COUNT) {
	int tries = 0;
	curpos = brake_read();

	if (curpos < BRAKE_RESET_POS) {
	  /* Set the velocity of the brake to a positive value */
	  while ( (curpos = brake_read(), curpos < BRAKE_RESET_POS &&
						 tries++ < 100)) {
	  }
	  break;			/* done moving */
	} else {
	  /* Set the velocity of the brake to a positive value */
	  while ((curpos=brake_read(), curpos > BRAKE_RESET_POS &&
						tries++ < 100)) {
	  }
	  break;			/* done moving */
	}
      }
      /* Set the velocity to zero */
      break;
    }
#endif

    case 'h':
      brake_home();
      break;

    case 't':				/* turn on servo thread */
      dbg_info("thread started");
      break;

    case 'e':      brake_flag = 1;	break;
    case 'd':      brake_flag = 0;	break;
    case 'k':	   brake_flag = -1;	break;

    default:
      pos = atof(inpbuf);
#ifdef BRAKE_OLD
      if (brake_flag != 1) {
	brake_setmav(pos, 1.0, 1.0);
      } else {
	brake_write(pos, 1.0, 1.0);
      }
#else
      printf("setting velocity to %g for 1 sec\n", pos);
      sleep(1);
      double curpos; curpos = brake_read();
      printf("pos = %g\n", curpos);

#endif
    }
  }

  brake_close();
  return 0;
}
