//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// CR_DominantOrientations
//----------------------------------------------------------------------------

#ifndef CR_DOMINANT_DECS

//----------------------------------------------------------------------------

#define CR_DOMINANT_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "fftw3.h"

#include "opencv_utils.hh"

// my stuff 

#include "CR_Memory.hh"
#include "CR_Error.hh"
//#include "CR_OpenCV_Image.hh"
#include "CR_Linux_Matrix.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

//-------------------------------------------------
// structure
//-------------------------------------------------


//-------------------------------------------------
// functions
//-------------------------------------------------


void initialize_dominant_orientations(int, int);
void compute_dominant_orientations(IplImage *);
void process_dominant_orientations_command_line_flags(int, char **);
float ***make_gabor_bank_fft_float(float, float, float, int, int, int, float ***, int *);
void initialize_gabor_fft_float(float ***, int, int);
void run_gabor_fft_float(IplImage *);
void initialize_gabor_opencv(float ***, int);
void run_gabor_opencv(IplImage *);
void find_gabor_max_responses();
void filter_out_weak_responses();

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
