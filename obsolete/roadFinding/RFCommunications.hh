#ifndef RFCOMMUNICATIONS_HH
#define RFCOMMUNICATIONS_HH

#include "StateClient.h"
#include "VehicleState.hh"
#include "SkynetContainer.h"
#include "sn_msg.hh"
#include "TrajTalker.h"
#include <math.h>

#include "ladar/ladarSource.hh"
#include "DGCutils"

class RFCommunications : public CStateClient, public CTrajTalker
{

private:

  void RFCalculateTraj(double dir, double lat);

  void RFReadLadar();

  //ladar data/mutex
  ladarMeasurementStruct RFLadarInfo;
  int RFrun;
  pthread_mutex_t RFmutex;

  // int RFLadarInfo.numPoints
  // double RFLadarInfo.ranges[MAX_SCAN_POINTS]
  // double RFLadarInfo.angles[MAX_SCAN_POINTS]

  // double range[MAX_SCAN_POINTS]

  sn_msg m_roadFinding;
  int rf_socket;
  int RFSkyNetKey;        //Skynet key running on
  int RFskynetguiSocket;  //gui socket 
  int RFtrajSocket;       //trajFollower socket
  int RFtrajLength;       //length of trajectory sending to trajFolllower
  int RFtrajDensity;      //See constructor
  int RFcorrectLength;

  double RFdeltaNorthing;
  double RFdeltaEasting;

  int RFmaxVelocity;      //Max speed traj will send
  int RFladarSocket;      //ladar socket

  //bools for sending traj and gui or not
  int RFsendtraj;
  int RFsendgui;
  int RFuseLadar;  // flag for using ladar--must be implemented.

public:

  double RFNorthing;  // utm
  double RFEasting;
  double RFRoll;  // rad
  double RFPitch;  // radians
  double RFYaw;  // rad
  double RFAltitude;

  //Used to fill traj--should be private
  double RFSource[2]; //Northing, Easting destination
  double RFDestination[2]; //Northing, Easting destination
  double RFVelocity[2]; //Northing, Easting velocities
  double RFAcceleration[2]; //Northing, Easting accelerations

  
  RFCommunications(int skynetkey, int traj, int gui, int ladar);
  ~RFCommunications();

  void RFUpdateState();
  void RFSendTraj(double dir, double lat);

  // send road data--see cpp file
  void RFSendRoad(double x[], double y[], double width[], int size);

  // angle is in radians, to the right is 0, front is pi/2, left is pi, range is in meters
  int RFGetLadarScan(double angle[], double range[], double &tStamp);

  // returns the max number of points we will return to you in GetLadarScan
  int RFGetMaxScanPoints();

};

#endif
