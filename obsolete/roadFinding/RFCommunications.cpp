#include "RFCommunications.hh"
#include <iostream>

using namespace std;

RFCommunications::RFCommunications(int skynetkey, int traj, int gui, int ladar) : CSkynetContainer(SNroadfinding, skynetkey)
{
  RFtrajLength = 50; //meters
  RFcorrectLength = 10; //meters
  RFtrajDensity = 4;  // n * RFtrajLength = number of traj waypoints
  RFmaxVelocity = 8; // m/s
  RFSkyNetKey = skynetkey;
  RFsendtraj = traj;
  RFsendgui = gui;
  RFuseLadar = ladar;

  //Prepare socket for sending road boundary data
  m_roadFinding = SNroadboundary;
  rf_socket = m_skynet.get_send_sock(m_roadFinding);

  //Prepare sockets to send traj and gui data
  if (RFsendgui && RFsendtraj)
    {
      RFskynetguiSocket = m_skynet.get_send_sock(SNtraj);
      RFtrajSocket = m_skynet.get_send_sock(SNRDDFtraj);
    }  
  else if (RFsendtraj)
    {
      RFtrajSocket = m_skynet.get_send_sock(SNtraj);
    }
  else if (RFsendgui)
    {
      RFskynetguiSocket = m_skynet.get_send_sock(SNtraj);
    }

  if (rf_socket < 0)
    {
      cerr << "Error connecting to Skynet!!" << endl;
    }

  // RYAN:  add ladar flag functionality here

  RFladarSocket = m_skynet.listen(SNladarmeas_roof, MODladarfeeder_roof);
  DGCcreateMutex(&RFmutex);
  RFrun = 1;
  DGCstartMemberFunctionThread(this, &RFCommunications::RFReadLadar);
}

RFCommunications::~RFCommunications()
{
  RFrun = 0;
  DGCdeleteMutex(&RFmutex);
}

void RFCommunications::RFUpdateState()
{
  UpdateState();
  
  RFNorthing = m_state.Northing;
  RFEasting = m_state.Easting;
  RFRoll = m_state.Roll;
  RFPitch = m_state.Pitch;
  RFYaw = m_state.Yaw;
  RFAltitude = m_state.Altitude;
}

void RFCommunications::RFCalculateTraj(double dir, double lat)
{
  RFUpdateState();

  dir -= .60;  // degree offset NOTE:  only because camera is yawed

  double adjustedTheta = RFYaw + dir / 180 * M_PI;

  double RFdeltaNorthing;
  double RFdeltaEasting;

  if (adjustedTheta > 2 * M_PI)
    adjustedTheta -= 2 * M_PI;

  RFSource[0] = RFNorthing - sin(RFYaw) * lat;
  RFSource[1] = RFEasting + cos(RFYaw) * lat;

  RFDestination[0] = cos(adjustedTheta) * RFtrajLength + RFNorthing;
  RFDestination[1] = sin(adjustedTheta) * RFtrajLength + RFEasting;

  RFdeltaNorthing = RFDestination[0] - RFSource[0];
  RFdeltaEasting = RFDestination[1] - RFSource[1];

  double ln = sqrt(RFdeltaNorthing * RFdeltaNorthing + RFdeltaEasting * RFdeltaEasting);

  RFVelocity[0] = RFdeltaNorthing / ln * RFmaxVelocity;
  RFVelocity[1] = RFdeltaEasting / ln * RFmaxVelocity;

  RFAcceleration[0] = 0;
  RFAcceleration[1] = 0;
}

void RFCommunications::RFSendTraj(double dir, double lat)
{
  double n[3], e[3];
  CTraj *RFtraj = new CTraj(3);

  RFCalculateTraj(dir, lat);

  RFtraj->startDataInput();

  for (int i = 0; i < RFtrajLength * RFtrajDensity; i++)
    {
      n[0] = RFSource[0] + RFdeltaNorthing * i / RFtrajDensity;
      n[1] = RFVelocity[0];
      n[2] = RFAcceleration[0];
      e[0] = RFSource[1] + RFdeltaEasting * i / RFtrajDensity;
      e[1] = RFVelocity[1];
      e[2] = RFAcceleration[1];
      RFtraj->inputWithDiffs(n, e);
    }

  if (RFsendtraj)
    {
      SendTraj(RFtrajSocket, RFtraj);
    }

  if (RFsendgui)
    {
      SendTraj(RFskynetguiSocket, RFtraj);
    }

  //cout << "Current Location N:" << RFNorthing << " E:" << RFEasting <<endl;
  //cout << "Destination Loca N:" << RFDestination[0] << " E:" << RFDestination[1] <<endl;

  delete RFtraj;
}



// Sends a road structure to fusion mapper.  The road is defined as follows:
//  x:  Meters in front of Alice, relative to the middle of the front axle.
//       In front is positive.
//  y:  Meters to the right of Alice (right hand rule, z-axis down)
//       To the right is positive
//  width:  Radius, in meters, surrounding the corresponding point above
//       (where the road is defined)
// Example:  x[0] = 0, y[0] = 0, width[0] = 5 corresponds to a road circle 5m in radius surrounding the front bumper
// size:  number of points passed
void RFCommunications::RFSendRoad(double x[], double y[], double width[], int size)
{
  // skynet message format:  int:size|size*sizeof(double)*3
  // {[number of road points == n], [n x-points, double], [n y-points, double], [n width values, double]}

  int sizeSendArray = 1 + 3 * size;

  if (sizeSendArray == 1)
    return;

  double sendArray[sizeSendArray];

  sendArray[0] = size;

  for (int i = 0; i < size; i++)
  {
    sendArray[i + 1] = x[i];
    sendArray[i + size + 1] = y[i];
    sendArray[i + 2 * size + 1] = width[i];
  }

  m_skynet.send_msg(rf_socket, (void*)sendArray, sizeof(sendArray), 0);
}


// angle and range must be allocated!  must have MAXSCANPOINTS at least
int RFCommunications::RFGetLadarScan(double angle[], double range[], double &tStamp)
{
   // ensure that ladar is working at this point (receiving data)

   // lock public var access here
  DGClockMutex(&RFmutex);
  for (int i = 0; i < RFLadarInfo.numPoints; i++)
  {
    range[i] = RFLadarInfo.ranges[i];
    angle[i] = RFLadarInfo.angles[i];
  }
  tStamp = RFLadarInfo.timeStamp;

  //cout << "RF: " << RFLadarInfo.timeStamp << endl;

  //unlock public var access here
  DGCunlockMutex(&RFmutex);

  return RFLadarInfo.numPoints; 


}


int RFCommunications::RFGetMaxScanPoints()
{
  return MAX_SCAN_POINTS;
}


// loop to populate ladar data
void RFCommunications::RFReadLadar()
{

  while (RFrun)
  {
    // lock mutex
    DGClockMutex(&RFmutex);
    int numReceived = m_skynet.get_msg(RFladarSocket, &RFLadarInfo, sizeof(ladarMeasurementStruct), 0);

    // unlock mutex
    DGCunlockMutex(&RFmutex);

    if (RFLadarInfo.numPoints > MAX_SCAN_POINTS)
      {
	// throw an error . . . too many points (should not happen)
      }

    // add a whole loop and a usleep
  }



}


