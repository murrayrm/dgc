//----------------------------------------------------------------------------
//  Road follower module
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//----------------------------------------------------------------------------

#include "decs.h"

//DARPA:BEGIN
#include "RFCommunications.hh"
#include <iostream>
#include <iomanip>

using namespace std;
//DARPA:END


//----------------------------------------------------------------------------

extern IplImage *gabor_max_response_index;
extern IplImage *gabor_max_response_intensity;
extern int show_dominant_orientations;

extern IplImage *candidate_image;
extern int show_vanishing_point;
extern int show_particles;
extern int show_support;

extern float KL_majority_fraction;

//----------------------------------------------------------------------------

int win_w, win_h;                     // window and target_im size are currently the same
int image_number;                     // how many frames processed so far
double *stage_time;                   // how many seconds spent on each stage of image processing
double image_time;                    // how many seconds spent processing this frame (sum of stage times)
IplImage **source_im;                 // directly from the image source
IplImage **gray_source_im;            // IPL_DEPTH_8U, 1 verions of source_im
IplImage *target_im;                  // image source after pyramid step
IplImage *gray_target_im;             // gray image source after pyramid step
CR_Movie *movie_source;               // if the source is a movie

int num_pyramid_levels                = 3;    // makes 640 x 480 camera image (source_im[0]) -> 160 x 120 (target_im)

int source_type                       = SOURCE_TYPE_LIVE;
//int firewire_camera_mode              = MODE_320x240_YUV422; 
int firewire_camera_mode              = MODE_640x480_MONO;
char source_path[256]                 = "../road_data/cmu_canyonsmall.avi";
int do_display                        = TRUE;
int report_interval                   = 10;
int do_timing                         = FALSE;
int print_image_number                = FALSE;
int do_live_logging                   = FALSE;

// source = images in a directory

int do_reset                          = FALSE;   // reset tracker parameters before next image is loaded?
int image_num_iterations              = 1;       // tracker iterations per image

int image_cur_iteration;
int image_imnum;
int image_startnum                    = 0;
int image_endnum;    
char *image_filename;
char logstate_filename[256];
FILE *image_filelist_fp;
FILE *logstate_fp;
int image_num;
int save_image_num                    = -1;
char *imname;

IplImage *redim;
float tracker_confidence;

// outputs to controller

float tracker_direction_error;    // in degrees (midline = 0, right = +, left = -)
float tracker_lateral_error;      // in degrees for now, but should be converted to meters when height, pitch, VFOV of cam known
float lateral_offset;
int tracker_onroad                    = TRUE;

FILE *log_fp;        // timestamps, anything else
char *log_command;
char *log_path;
char *log_timestamp;
char *log_image_filename;
char *log_filename;
int log_interval                      = 1;
long int timestamp_secs;
long int timestamp_usecs;

int cameraNumber                      = 0;

//DARPA:BEGIN

int skynet = 0;
int sendtraj = 0;
int sendgui = 0;
RFCommunications *RFComm;

//DARPA:END


//----------------------------------------------------------------------------

void initialize_live_logger()
{
  // get timestamp string

  log_timestamp = CR_datetime_string();

  // open files

  log_image_filename = (char *) calloc(256, sizeof(char));
  log_filename = (char *) calloc(256, sizeof(char));
  log_command = (char *) calloc(256, sizeof(char));

  sprintf(log_command, "mkdir %s%s", log_path, log_timestamp);
  system(log_command);
  sprintf(log_filename, "%s%s/%s.log", log_path, log_timestamp, log_timestamp);

//   printf("%s\n", log_filename);
//   exit(1);
  printf("logging %s\n", log_timestamp);

  log_fp = fopen(log_filename, "w");

  //  free(log_timestamp);
}

//----------------------------------------------------------------------------

// record timestamp info for frame about to be captured

void write_live_logger()
{
  timestamp(&timestamp_secs, &timestamp_usecs);
  fprintf(log_fp, "%i, %li, %li\n", image_number, timestamp_secs, timestamp_usecs);
  fflush(log_fp);
  if (!(image_number % 150)) {
    printf("%i, %li, %li\n", image_number, timestamp_secs, timestamp_usecs);
    fflush(stdout);
  }
}

//----------------------------------------------------------------------------

void print_output_to_controller()
{
  printf("dir %.2f degs, lat %.2f degs, onroad %i\n", tracker_direction_error, tracker_lateral_error, tracker_onroad);

  //DARPA:BEGIN
  //INSERT TRAJ HERE

  if (sendtraj)
    {
      RFComm->RFSendTraj(tracker_direction_error, tracker_lateral_error);
    }

  //DARPA:END

}

//----------------------------------------------------------------------------

void compute_output_to_controller()
{
  float x, y;

  get_tracker_position(&x, &y);
  draw_vanishing_point_support(x, y, win_w, win_h);

  tracker_direction_error = HORIZ_PIX2DEG(x - (float) win_w / 2, win_w);  // HORIZ_PIX2RAD for radians
  tracker_lateral_error = HORIZ_PIX2DEG(lateral_offset - (float) win_w / 2, win_w);  // HORIZ_PIX2RAD for radians

  // tracker_onroad flag should already be set
}

//----------------------------------------------------------------------------

void display()
{
  if (do_live_logging) {
    glDrawPixels(target_im->width, target_im->height, GL_RGB, GL_UNSIGNED_BYTE, target_im->imageData);
    glutSwapBuffers();
    return;
  }

  if (show_vanishing_point)
    glDrawPixels(candidate_image->width, candidate_image->height, GL_LUMINANCE, GL_FLOAT, candidate_image->imageData);
  else if (show_dominant_orientations)
    glDrawPixels(gabor_max_response_intensity->width, gabor_max_response_intensity->height, GL_LUMINANCE, GL_UNSIGNED_BYTE, gabor_max_response_intensity->imageData);
  else {
    if (!tracker_onroad)
      cvMul(redim, target_im, target_im);
    glDrawPixels(target_im->width, target_im->height, GL_RGB, GL_UNSIGNED_BYTE, target_im->imageData);
  }

  if (tracker_onroad) {

    // vertical line at w/2
    
    glColor3f(0,1,1);
    glLineWidth(1);
    glBegin(GL_LINES);
    glVertex2f(win_w / 2, 0);
    glVertex2f(win_w / 2, win_h);
    glEnd();

    // road support "rays"

    float x, y;
    get_tracker_position(&x, &y);
    draw_vanishing_point_support(x, y, win_w, win_h);

    // vanishing point, horizon line

    draw_tracker(win_w, win_h);           // estimate smoothed by particle filtering
  }

  glutSwapBuffers();
}

//----------------------------------------------------------------------------

void mouse(int button, int state, int x, int y)
{
  // cycle between raw image view, dominant orientation view, and vote view

  if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
    if (!show_dominant_orientations && !show_vanishing_point)
      show_dominant_orientations = TRUE;
    else if (show_dominant_orientations && !show_vanishing_point) {
      show_dominant_orientations = FALSE;
      show_vanishing_point = TRUE;
    }
    else if (!show_dominant_orientations && show_vanishing_point)
      show_vanishing_point = FALSE;
  }

  // cycle between raw image view, tracker particles, and support rays

  else if (button == GLUT_RIGHT_BUTTON && state == GLUT_UP) {
    if (!show_particles && !show_support)
      show_particles = TRUE;
    else if (show_particles && !show_support) {
      show_particles = FALSE;
      show_support = TRUE;
    }
    else if (!show_particles && show_support)
      show_support = FALSE;
  }

  // save whatever the display is currently showing to file

  else if (button == GLUT_MIDDLE_BUTTON && state == GLUT_UP) {

        sprintf(imname, "../road_data/blah_%i.png", image_number);
        cvConvertImage(target_im, gray_target_im);    // color to gray
        cvSaveImage(imname, gray_target_im);
    
    /*
    glReadPixels(0, 0, target_im->width, target_im->height, GL_RGB, GL_UNSIGNED_BYTE, target_im->imageData);
    sprintf(imname, "../road_data/cmu_%i.png", image_number);
    cvConvertImage(target_im, target_im, CV_CVTIMG_SWAP_RB | CV_CVTIMG_FLIP);  // RGB->BGR	
    cvSaveImage(imname, target_im);
    */

    //exit(1);
  }
}

//----------------------------------------------------------------------------

// when the window is hidden -- needed if you want to use OpenGL functions without a window

void fake_display() { }

//----------------------------------------------------------------------------

void process_image()
{
  if (!do_live_logging) {

    // calculate dominant orientations
    
    compute_dominant_orientations(target_im);
    
    // find vanishing point
    
    compute_vanishing_point(target_im->width, target_im->height);

    // track vanishing point
    
    update_tracker();
    
    // confidence in [0, 1] range that we are actually on or near a road

    //    tracker_confidence = compute_onoff_confidence(candidate_image);
    //    tracker_onroad = tracker_confidence >= KL_majority_fraction; 

    if (!tracker_onroad)
      reset_tracker();
    
    // talk to vehicle controller

    compute_output_to_controller();
    print_output_to_controller();
  }
  else if (!(image_number % log_interval)) {
    if (firewire_camera_mode == MODE_640x480_MONO) {

      // save a 640x480 image every 10 seconds (assuming log_interval = 1 and fps = 15), replacing lower-res for that frame

//      if (!(image_number % 150)) {   
      if ((image_number % 150) == 0 || (image_number % 150) == 1) {   
        sprintf(log_image_filename, "%s%s/%s_0_%06i.jpg", log_path, log_timestamp, log_timestamp, image_number);
        cvConvertImage(source_im[0], gray_source_im[0]);    // color to gray
        cvSaveImage(log_image_filename, gray_source_im[0]);
      }

      // save a 320x240 image every 1 second (with above assumptions), replacing lower-res for that frame

      else if ((image_number % 15) == 0 || (image_number % 15) == 1) {   
//      else if (!(image_number % 15)) {   
        sprintf(log_image_filename, "%s%s/%s_1_%06i.jpg", log_path, log_timestamp, log_timestamp, image_number);
        cvConvertImage(source_im[1], gray_source_im[1]);    // color to gray
        cvSaveImage(log_image_filename, gray_source_im[1]);
      }

      // save lowest-res version of image at 15 fps (with above assumptions)

      else {
        sprintf(log_image_filename, "%s%s/%s_%i_%06i.jpg", log_path, log_timestamp, log_timestamp, num_pyramid_levels - 1, image_number);
        cvConvertImage(target_im, gray_target_im);    // color to gray
        cvSaveImage(log_image_filename, gray_target_im);
      }
    }
// can't handle color image logging yet
//    else {
//      cvConvertImage(target_im, target_im, CV_CVTIMG_SWAP_RB | CV_CVTIMG_FLIP);  // BGR -> RGB
//      cvSaveImage(log_image_filename, target_im);
//    }
  }
}

//----------------------------------------------------------------------------

static double start, finish, difference;
struct timeval tv;

void init_per_image()
{
  int i, stage;

  // start timing

  if (do_timing && !(image_number % report_interval)) 
    start = (double) cvGetTickCount() / cvGetTickFrequency();

  // acquire image

  // next AVI frame

  if (source_type == SOURCE_TYPE_AVI) {
    if (image_number < movie_source->lastframe)
      get_frame_CR_Movie(movie_source, image_number);
    else
      finish_run();   
  }

  // live capture

  else if (source_type == SOURCE_TYPE_LIVE) {
    if (do_live_logging && !(image_number % log_interval)) 
      write_live_logger();
    capture_firewire_image(source_im[0], cameraNumber);
  }

  // images in a directory (in whatever order "ls" results in)

  else if (source_type == SOURCE_TYPE_IMAGE) {

    if (image_num_iterations < 0 || image_cur_iteration < image_num_iterations - 1) 
      image_cur_iteration++;
    else {   // get rid of old image; load new one

      if (do_reset)
	reset_tracker();
        
      cvReleaseImage(&source_im[0]);

      if (image_imnum >= image_endnum || !read_next_image(image_filelist_fp, &source_im[0])) {
	printf("done\n");
	exit(1);
      }

      image_cur_iteration = 0;
      image_imnum++;
    }
  }
  else
    CR_error("Unknown image source");

  // "pyramidize" acquired image

  for (i = 0; i < num_pyramid_levels - 1; i++) 
    cvPyrDown(source_im[i], source_im[i + 1]);

  // perform image processing and control loop

  process_image();

  // end timing

  if (do_timing && !(image_number % report_interval)) {
    finish = (double) cvGetTickCount() / cvGetTickFrequency();
    printf("%.3lf fps\n", 1000000.0/(finish - start));
  }

  if (print_image_number)
    printf("%i\n", image_number);

  image_number++;
}

//----------------------------------------------------------------------------

void compute_display_loop()
{
  //  printf("%i\n", image_number);

  init_per_image();

  if (do_display)
    glutPostRedisplay();
}

//----------------------------------------------------------------------------

void finish_run()
{
  exit(1);
}

//----------------------------------------------------------------------------

// position file pointer at beginning of line n

int goto_image(FILE *fp, int n)
{
  int i;

  for (i = 0; i < n; i++) {
    if (!fgets(image_filename, 80, fp)) {
      printf("no such line in file\n");
      exit(1);
    }
  }
}

//----------------------------------------------------------------------------

// fp is a file containing a full-path filename of an input image on each line
// assumes image_filename already allocated
// returns TRUE if filename gotten; FALSE if EOF encountered
 
int read_next_image(FILE *fp, IplImage **im)
{
  if (!fgets(image_filename, 80, fp))
    return FALSE;
  image_filename[strlen(image_filename)-1] = '\0';
  //  printf("%s\n", image_filename);

  *im = cvLoadImage(image_filename);
  //  printf("w = %i, h = %i\n", (*im)->width, (*im)->height);
  cvConvertImage(*im, *im, CV_CVTIMG_SWAP_RB);  // RGB->BGR
  return TRUE;
}

//----------------------------------------------------------------------------

void init_per_run(int argc, char **argv)
{
  int i;

  //DARPA:BEGIN

  if (skynet)
    {
      int sn_key = 0;
      char* RFSkyNetKey = getenv("SKYNET_KEY");
      if (RFSkyNetKey == NULL)
	{
	  cerr << "SKYNET_KEY environment variable is not set!!" << endl;
	}
      else
	{
	  sn_key = atoi(RFSkyNetKey);
	}
      cout << sn_key << endl;
      RFComm = new RFCommunications(sn_key, sendtraj, sendgui, 0);
    }

  //DARPA:END

  //-------------------------------------------------
  // logging
  //-------------------------------------------------

  if (do_live_logging) 
    initialize_live_logger();

  //-------------------------------------------------
  // initialize image source
  //-------------------------------------------------

  source_im = (IplImage **) calloc(num_pyramid_levels, sizeof(IplImage *));
  gray_source_im = (IplImage **) calloc(num_pyramid_levels, sizeof(IplImage *));
  
  if (source_type == SOURCE_TYPE_AVI) {
    movie_source = open_CR_Movie(source_path);
    source_im[0] = movie_source->im;
  }
  else if (source_type == SOURCE_TYPE_LIVE) {
    int w, h;
    if (!initialize_capture_firewire(&w, &h,  firewire_camera_mode)) {
      printf("no firewire cameras attached\n");
      exit(1);
    }
    source_im[0] = cvCreateImage(cvSize(w, h), IPL_DEPTH_8U, 3);
    gray_source_im[0] = cvCreateImage(cvSize(w, h), IPL_DEPTH_8U, 1);
  }
  else if (source_type == SOURCE_TYPE_IMAGE) {

    image_cur_iteration = 0;
    image_filename = (char *) calloc(256, sizeof(char));

    // generate this file with "find <path> | sort > filename.txt" (and remove first line of file)

    image_filelist_fp = fopen(source_path, "r");
    if (!image_filelist_fp) {
      printf("no such file list\n");
      exit(1);
    }

    goto_image(image_filelist_fp, image_startnum);
    image_imnum = image_startnum;
    if (!read_next_image(image_filelist_fp, &source_im[0])) {
      printf("no images in file list\n");
      exit(1);
    }
  }
  else
    CR_error("No image source set!\n");

  for (i = 1; i < num_pyramid_levels; i++) {
    source_im[i] = cvCreateImage(cvSize(source_im[i - 1]->width / 2, source_im[i - 1]->height / 2), IPL_DEPTH_8U, 3);
    gray_source_im[i] = cvCreateImage(cvSize(source_im[i - 1]->width / 2, source_im[i - 1]->height / 2), IPL_DEPTH_8U, 1);
  }	

  target_im = source_im[num_pyramid_levels - 1];
  gray_target_im = gray_source_im[num_pyramid_levels - 1];

  //-------------------------------------------------
  // miscellaneous
  //-------------------------------------------------

  imname = (char *) calloc(256, sizeof(char));

  //-------------------------------------------------
  // initialize OpenGL
  //-------------------------------------------------

  if (do_display) {

    win_w = target_im->width;
    win_h = target_im->height;

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(win_w, win_h); 
    glutInitWindowPosition(100, 100);
    glutCreateWindow("road");
    
    glutIdleFunc(compute_display_loop);

    glutDisplayFunc(display); 
    glutMouseFunc(mouse); 
    
    glPixelZoom(1, -1);
    glRasterPos2i(-1, 1);
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, win_w, 0, win_h);
  }
  else {

    win_w = target_im->width;
    win_h = target_im->height;

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(win_w, win_h); 
    glutInitWindowPosition(100, 100);
    glutCreateWindow("RoadVP");
    
    glutIdleFunc(process_image);

    glutDisplayFunc(fake_display); 
    glutHideWindow();
  }

  //-------------------------------------------------
  // timing 
  //-------------------------------------------------

  image_number = image_startnum;

  if (do_live_logging) {
    glutMainLoop();
    return;
  }

  //-------------------------------------------------
  // random number generation
  //-------------------------------------------------

  initialize_CR_Random();

  //-------------------------------------------------
  // dominant orientation estimation
  //-------------------------------------------------

  initialize_dominant_orientations(target_im->width, target_im->height);

  //-------------------------------------------------
  // vanishing point finding
  //-------------------------------------------------

  initialize_vanishing_point_estimation(target_im->width, target_im->height);

  //-------------------------------------------------
  // tracking vanishing point
  //-------------------------------------------------

  initialize_tracker();

  //-------------------------------------------------
  // on vs. off road confidence
  //-------------------------------------------------

  initialize_onoff_confidence();

  // to redden display when we think we're off road

  redim = cvCreateImage(cvSize(target_im->width, target_im->height), IPL_DEPTH_8U, 3);
  cvSet(redim, cvScalar(1, 0, 0));

  //-------------------------------------------------
  // start OpenGL GLUT
  //-------------------------------------------------

  glutMainLoop();
}

//----------------------------------------------------------------------------

void process_command_line_flags(int argc, char **argv)
{
  int i;

  // general variables

  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i],                             "-p"))
      num_pyramid_levels = atoi(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-image")) {
      source_type = SOURCE_TYPE_IMAGE;
      strcpy(source_path, argv[i + 1]);     // full path name of text file with one image name (full path) per line
      image_startnum = atoi(argv[i + 2]);   // which line in that file to start reading from
      image_endnum = atoi(argv[i + 3]);     // which line to stop reading at
    }
    else if (!strcmp(argv[i],                        "-avi")) {
      source_type = SOURCE_TYPE_AVI;
      strcpy(source_path, argv[i + 1]);
    }
    else if (!strcmp(argv[i],                        "-live")) 
      source_type = SOURCE_TYPE_LIVE;
    else if (!strcmp(argv[i],                        "-time")) 
      do_timing = TRUE;
    else if (!strcmp(argv[i],                        "-nodisplay")) {
      do_display = FALSE;
      win_w = win_h = 1;
    }
    else if (!strcmp(argv[i],                        "-printnum")) 
      print_image_number = TRUE;
    else if (!strcmp(argv[i],                        "-doreset")) 
      do_reset = TRUE;
    else if (!strcmp(argv[i],                        "-save")) 
      save_image_num = atoi(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-camnum")) 
      cameraNumber = atoi(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-loglive")) {
      do_live_logging = TRUE;
      log_interval = atoi(argv[i + 1]);
      log_path = (char *) calloc(256, sizeof(char));
      strcpy(log_path, argv[i + 2]);
    }
    else if (!strcmp(argv[i],                        "-iterations")) 
      image_num_iterations = atoi(argv[i + 1]);

    //DARPA:BEGIN
    
    else if (!strcmp(argv[i],                        "-sendtraj"))
      sendtraj = 1;

    else if (!strcmp(argv[i],                        "-sendgui"))
      sendgui = 1;

    else if (!strcmp(argv[i],                        "-skynet"))
      skynet = 1;
    
    //DARPA:END

  }

  // specialized variables

  process_dominant_orientations_command_line_flags(argc, argv);
  process_vanishing_point_command_line_flags(argc, argv);
  process_tracker_command_line_flags(argc, argv);
  process_onoff_command_line_flags(argc, argv);
}

//----------------------------------------------------------------------------

int main(int argc, char **argv)
{
  /*
  RFCommunications *RFCommi = new RFCommunications(0, 0, 0, 1);

  int maxPts = RFCommi->RFGetMaxScanPoints();

  double *pAngle = new double[maxPts];
  double *pRange = new double[maxPts];
  int poo;
  while(1)
  {
    double ts;
  int number = RFCommi->RFGetLadarScan(pAngle, pRange, ts);

  cout << "Ladar read!  " << number << " points" << endl;
  cout << "time stamp " << ts << endl;

  for (int ryan = 0; ryan < number; ryan++)
  {
    //    cout << "Angle: " << -1* (pAngle[ryan] * 180 / M_PI - 90) << ", Range: " << pRange[ryan] << endl;
    cout << "Angle: " << pAngle[ryan] * 180 / M_PI << ", Range: " << pRange[ryan] << endl;
  }
  cout << "next?";
  cin >> poo;
  }
  */
  /*
int ryan;
  
  for (int dan = 0; dan < 10; dan++)
    {
      RFCommi->RFUpdateState();

      cout << "Northing: " << RFCommi->RFNorthing << endl;
      cout << "Easting : " << RFCommi->RFEasting << endl;
      cout << "Roll    : " << RFCommi->RFRoll << endl;
      cout << "Pitch   : " << RFCommi->RFPitch << endl;
      cout << "Yaw     : " << RFCommi->RFYaw << endl;
      cout << "Altitude: " << RFCommi->RFAltitude << endl;
      cin >> ryan;
    }
  */
  process_command_line_flags(argc, argv);
  init_per_run(argc, argv);

  return 1;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
