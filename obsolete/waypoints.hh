using namespace std;

#ifndef _WAYPOINT_H
#define _WAYPOINT_H

#include <iostream>
#include <fstream>
#include "GlobalConstants.h"
#include "rddf.hh"

enum waypoint_type {START, NORMAL, CHECKPOINT, END, NONE};

struct waypoint
{
  int num;                // index number
  waypoint_type t;
  double easting;         // meters UTM zone 11
  double northing;        // meters UTM zone 11
  double radius;          // meters
  double max_velocity;    // meters/sec
};


class Waypoints
{
 public:
  Waypoints ();
  Waypoints (char * infile);

  // Read RDDF file at team/RDDF/
  int readRDDF(char * infile, double Northing, double Easting); 

  void read(char * infile);     // Read waypoint file

  int get_size();               // How many waypoints total?
  waypoint get_start();         // Jump to the first waypoint        
  waypoint get_next();          // Jump to the next wp
  waypoint get_previous();      // Jump to the previous wp
  waypoint get_last();          // Jump to the last wp
  waypoint get_current();       // Get the current wp
  waypoint get_waypoint(int i); // Jump to the specified wp
  waypoint peek_waypoint(int i);// Get the specified wp
  waypoint peek_next();         // Get the next wp without changing current
  waypoint peek_previous();     // Get the previous wp without changing current

 private:
  waypoint _waypoint_list[10000];        // Just allocate a bunch of space
  int _current;
  int _last;
};


#endif // _WAYPOINT_H
