/*
 * Created on Oct 10, 2004
 * $Id$
 */
package modulecom;

/**
 * A thread that continiuosly pings all the connected ModuleDaemons 
 * to display the result in the StatusTable of the GUI.
 * @author Henrik Kjellander (henrik.kjellander@home.se)
 */
public class PingThread extends Thread {

	/**
	 * Reference to the ModuleCom object
	 */
	private ModuleCom moduleCom;

	/**
	 * This Threads period in milliseconds
	 */
	private long period;
	
	private boolean quit = false;
	
	/**
	 * Constructor.
	 */
	public PingThread(ModuleCom moduleCom, long period) {
		super();
		this.moduleCom = moduleCom;
		this.period = period;
	}
	
	/**
	 * The main execution method for this thread
	 *
	 */
	public void run() {
		long duration; 
		long t = System.currentTimeMillis(); 
				
		while(!quit) {
			
			// ping all the online modules...
			
			// do a proper sleep the remaining time of the period:
			t = t + period; 
			duration = t - System.currentTimeMillis(); 
			if (duration > 0) { 
				try { sleep(duration); } 
				catch (InterruptedException e) {}
			}			
		}
	}
	
	public void quit() {
		quit = true;
	}
}
