/*
 * Created on Oct 10, 2004
 * $Id$
 */
package modulecom;

/**
 * A class for handling the communication with all the module deamons over TCP/IP.
 * @author Henrik Kjellander (henrik.kjellander@home.se)
 */
public class ModuleCom {
	/**
	 * Sleeptime for this thread (milliseconds)
	 */
	private final int pingthreadPeriod = 500;

	/**
	 * Thread for pinging modules periodically
	 */
	private PingThread pingThread;
	
	/**
	 * Constructor
	 */
	public ModuleCom() {
		pingThread = new PingThread(this, pingthreadPeriod);
		pingThread.start();
	}
}
