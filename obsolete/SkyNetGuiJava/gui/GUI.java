/*
 * Created Oct 9, 2004
 * $Id$
 */

package gui;

import gui.modules.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * <p>Title: GUI </p>
 * <p>Description: The GUI</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * @author Henrik Kjellander
 * @version 1.0
 */
public class GUI
    extends DefaultKeyboardFocusManager {

	/** The different tabs */
    MainTab modulesTab = new MainTab(this);
    ArbiterTab arbiterTab = new ArbiterTab(this);
    GlobalTab globalTab = new GlobalTab(this);
    LadarTab ladarTab = new LadarTab(this);
    StereoTab stereoTab = new StereoTab(this);
	
    /**
     * Constructor.
     */
    public GUI() {
    }

    /**
     * Build up the GUI.
     */
    public void initializeGUI() {
        JFrame frame = new JFrame("SkyNet v" + Main.VERSION);
        
        // Create tab container
        JTabbedPane tabs = new JTabbedPane();
        tabs.addTab("Main", modulesTab);
        tabs.addTab("Arbiter", arbiterTab);
        tabs.addTab("Global", globalTab);
        tabs.addTab("Ladar", ladarTab);
        tabs.addTab("Stereo", stereoTab);

        // Add a WindowListener that exists the system if the main window is closed
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                performExitOperations();
            }
        });

        // Set guiPanel to be the content pane of frame
        frame.getContentPane().add(tabs);

        // Pack the components of the window. The size is calculated.
        frame.pack();

        // Code to position the window at the center of the screen
        Dimension sd = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension fd = frame.getSize();
        frame.setLocation( (sd.width - fd.width) / 2 - 200,
                          (sd.height - fd.height) / 2);

        // Make the window visible
        frame.setVisible(true);

    }

 
    //##########################################################################
    //##########################################################################
    //  PRIVATE METHODS
    //##########################################################################
    //##########################################################################

    /**
     * Exit properly
     */
    private void performExitOperations() {
        System.out.print("Shutting down...");

        System.out.println("done!");
        System.exit(0);
    }
}
