/*
 * Created on Oct 9, 2004
 * $Id$
 */
package gui;

import javax.swing.*;
import javax.swing.table.*;

import java.awt.Dimension;
import java.util.*;

/**
 * The main window. Contains an overview and abilities to start and stop modules.
 * @author Henrik Kjellander (henrik.kjellander@home.se)
 */
public class MainTab extends JPanel {

	private GUI gui;
    private DebugWindow debugWindow = new DebugWindow();
    private ConfigPanel configPanel = new ConfigPanel();
	private CommandsPanel commandsPanel;
	private LogsPanel logsPanel;
	private SettingsPanel settingsPanel;
    private StatusTable statusTable;

	public MainTab(GUI gui) {
		this.gui = gui;
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		// top section of the main tab
		statusTable = new StatusTable();
		add(statusTable);

		// middle section of the main tab
		JPanel middleLeftPanel = new JPanel();
		middleLeftPanel.setLayout(new BoxLayout(middleLeftPanel, BoxLayout.Y_AXIS));
		commandsPanel = new CommandsPanel(gui);
		logsPanel = new LogsPanel(gui);
		settingsPanel = new SettingsPanel();

		middleLeftPanel.add(commandsPanel);
		middleLeftPanel.add(logsPanel);
		middleLeftPanel.add(settingsPanel);

		JPanel middlePanel = new JPanel();
		middlePanel.add(middleLeftPanel);
		middlePanel.add(configPanel);
		add(middlePanel);


		// lower section of the main tab
		add(debugWindow);
	}

	/**
	 * The status window. This class is made as an internal class because lots of thigs
	 * are common with the MainPanel: button methods etc...
	 *
	 * @author Henrik Kjellander
	 */
	public class StatusTable extends JPanel {
		private Vector columnNames;
		private Vector moduleData;
		private JTable table;

		/**
		 * Data that should be updated from some external class...
		 * Needs enumerations to identify the different fields...
		 * Or maybe we should have some data-class that wraps to this
		 * data vector?
		 */
		public Vector arbiterData, globalData, ladarData;

		public StatusTable() {
			// Create headers
			columnNames = new Vector();
			columnNames.add("Name");
			columnNames.add("State");
			columnNames.add("Computer");
			columnNames.add("Ping (ms)");
			columnNames.add("Switches");

			// Generate some fake data for display

			arbiterData = new Vector();
			arbiterData.add("Arbiter");
			arbiterData.add("ACTIVE");
			arbiterData.add("Titanium");
			arbiterData.add(new Integer(2));
			arbiterData.add("--noglobal --noobstacle");

			globalData = new Vector();
			globalData.add("Global");
			globalData.add("STANDBY");
			globalData.add("Tantalum");
			globalData.add(new Integer(4));
			globalData.add("");

			ladarData = new Vector();
			ladarData.add("Ladar");
			ladarData.add("STOPPED");
			ladarData.add("Iridium");
			ladarData.add(new Integer(1));
			ladarData.add("--roof-tty /dev/ttyS0");

			moduleData = new Vector();
			moduleData.add(arbiterData);
			moduleData.add(globalData);
			moduleData.add(ladarData);


			table = new JTable(moduleData, columnNames);
			table.setPreferredScrollableViewportSize(new Dimension(700, 120));

			// for non-editable cells, we need a custom table model:
		    table.setModel(new MyTableModel());

		    // Adjust the columns:
			TableColumn column = null;
			for (int i = 0; i < 5; i++) {
				column = table.getColumnModel().getColumn(i);
				if (i == 3) {
					column.setPreferredWidth(25); // ping column is smaller
				} else if (i == 4) {
					column.setPreferredWidth(180); // switches column is bigger
				}
			};

			//Create the scroll pane and add the table to it.
	        JScrollPane scrollPane = new JScrollPane(table);

	        //Add the scroll pane to this panel.
	        add(scrollPane);
	        setBorder(BorderFactory.createTitledBorder("Status table"));
		}

		private class MyTableModel extends AbstractTableModel {
		    public int getColumnCount() {
		        return columnNames.size();
		    }

		    public int getRowCount() {
		        return moduleData.size();
		    }

		    public Object getValueAt(int row, int col) {
		        return ((Vector)moduleData.get(row)).get(col);
		    }

		    public boolean isCellEditable(int row, int column) {
	    		return false;
	    	}

		    public String getColumnName(int col) {
		        return columnNames.get(col).toString();
		    }
		}
	}
}

