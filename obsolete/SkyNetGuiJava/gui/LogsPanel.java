/*
 * Created on Oct 9, 2004
 * $Id$
 */
package gui;

import javax.swing.*;

import java.awt.event.*;

/**
 * Commands panel
 * @author Henrik Kjellander (henrik.kjellander@home.se)
 */
public class LogsPanel
    extends JPanel {
    
	private GUI gui;

    protected JTextField testNameField = new JTextField(Main.defaultTestName);
    protected JButton transferButton = new JButton("Transfer logs");
    protected JTextField hostField = new JTextField(Main.defaultHost);
    protected JCheckBox gzipCheckBox = new JCheckBox("gzip first", false);
    protected JTextField dirField = new JTextField(Main.defaultDir);
    
    public LogsPanel(GUI g) {
        gui = g;

        // Create the first line 
        JPanel firstLine = new JPanel();
        firstLine.setLayout(new BoxLayout(firstLine, BoxLayout.X_AXIS));
        firstLine.add(new JLabel("Test name "));
        firstLine.add(testNameField);
        firstLine.add(transferButton);
        firstLine.add(Box.createHorizontalGlue());
        
        JPanel secondLine = new JPanel();
        secondLine.setLayout(new BoxLayout(secondLine, BoxLayout.X_AXIS));
        secondLine.add(new JLabel("Host "));
        secondLine.add(hostField);
        secondLine.add(Box.createHorizontalGlue());
        
        JPanel thirdLine = new JPanel();
        thirdLine.setLayout(new BoxLayout(thirdLine, BoxLayout.X_AXIS));
        thirdLine.add(new JLabel("Dir "));
        thirdLine.add(dirField);
		thirdLine.add(gzipCheckBox);
		thirdLine.add(Box.createHorizontalGlue());
		
        // Add the lines to a common layout
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS))	;
        add(firstLine);
        add(secondLine);
        add(thirdLine);
        
        setBorder(BorderFactory.createTitledBorder("Log handling"));
        // Button actions ##############################################################

        transferButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                log("Transfer button pressed");
                // send command to ModuleCom about moving all the logs
            }
        });
    }


    private void log(String text) {
        if (Main.debug) {
            System.out.println(text);
        }
    }
}
