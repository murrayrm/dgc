/*
 * Created Oct 9, 2004
 * $Id$
 */
package gui;

import java.io.*;
import javax.swing.filechooser.FileFilter;


/**
 * A filter that decides what to be shown in the FilePickWindow 
  * @author Henrik Kjellander (henrik.kjellander@home.se)
  */
public class MyFileFilter
      extends FileFilter {
   final String fileExtension = ".cfg";

   /**
    * Construktor
    */
   public MyFileFilter() {
   }

   /**
    * Method to check if a file is accepted (goes through the filter)
    * @param file  file to be checked
    * @return      true  if the file goes through
    *              false if it doesn't goes through
    */
   public boolean accept(File file) {
      if (file.isDirectory()) {
         return true;
      }
      String filename = file.getName();
      if (filename.length() > fileExtension.length() &&
          filename.substring(filename.length() - fileExtension.length()).
          equalsIgnoreCase(fileExtension)) {
         return true;
      }
      return false;
   }

   /**
    * Returns the filter description
    * @return description
    */
   public String getDescription() {
      return "(*" + fileExtension + ") Config files";
   }

}