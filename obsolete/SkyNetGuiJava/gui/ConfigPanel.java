/*
 * Created on Oct 9, 2004
 * $Id$
 */
package gui;

import gui.modules.*;

import javax.swing.*;

/**
 * Config panel. Contains configuration info about the selected module in the status table.
 * Checks which module is selected (starts by displaying config for the top one if none is selected).
 * and then loads the proper config for it.
 * @author Henrik Kjellander (henrik.kjellander@home.se)
 */
public class ConfigPanel
    extends JPanel {
   
	/**
	 * The current module config class loaded
	 */
	private ModuleConfig moduleConfig;
	
	/**
	 * Constructor.
	 *
	 * @author Henrik Kjellander (henrik.kjellander@home.se)
	 */
	public ConfigPanel() {
        // insert code to check in the statusTable which is active here...
        // for now, I just select the arbiter config
        moduleConfig = new ArbiterConfig();
        add(moduleConfig);
    }
	
	/**
	 * Returns a the configuration string that is the result of the switches available 
	 * for this module.
	 *
	 * @author Henrik Kjellander (henrik.kjellander@home.se)
	 */
	public String getConfig() {
		return moduleConfig.getConfig();
	}
}
