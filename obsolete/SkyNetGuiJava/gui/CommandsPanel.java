/*
 * Created on Oct 9, 2004
 * $Id$
 */
package gui;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

/**
 * Commands panel. Contains buttons to start, stop, standby, restart, shutdown modules...
 * @author Henrik Kjellander
 */
public class CommandsPanel
    extends JPanel {

    private static final boolean debug = true;
    
	private GUI gui;
	
    protected JButton startButton = new JButton("Start");
    protected JButton standbyButton = new JButton("Standby");
    protected JButton stopButton = new JButton("Stop");
    protected JButton shutdownButton = new JButton("Shutdown");
    protected JButton restartButton = new JButton("Restart");
    
    protected JButton startAllButton = new JButton("Start all standby");
    protected JButton stopAllButton = new JButton("Stop all active");
    
    public CommandsPanel(GUI g) {
        gui = g;

        // Create the first line of buttons
        JPanel firstLine = new JPanel();
        firstLine.add(startButton);
        firstLine.add(standbyButton);
        firstLine.add(stopButton);
        firstLine.add(shutdownButton);
        firstLine.add(restartButton);
        
        JPanel secondLine = new JPanel();
        secondLine.add(startAllButton);
        secondLine.add(stopAllButton);
        
        // Add the lines to a common layout
        setLayout(new GridLayout(2,0));
        add(firstLine);
        add(secondLine);
        
        setBorder(BorderFactory.createTitledBorder("Commands"));
        // Button actions ##############################################################

        startButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                log("Start button pressed");
                // send command to ModuleCom
            }
        });

        standbyButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                log("Standby button pressed");
                // send command to ModuleCom
            }
        });
    
        stopButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                log("Stop button pressed");
                // send command to ModuleCom
            }
        });

        shutdownButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                log("Shutdown button pressed");
                // send command to ModuleCom
            }
        });

        restartButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                log("Restart button pressed");
                // send command to ModuleCom
            }
        });

        startAllButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                log("Start ALL button pressed");
                // send command to ModuleCom
            }
        });

        stopAllButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                log("Stop ALL button pressed");
                // send command to ModuleCom
            }
        });
}


    //##########################################################################
    //##########################################################################
    //  PRIVATE METHODS
    //##########################################################################
    //##########################################################################

    private void log(String text) {
        if (debug) {
            System.out.println(text);
        }
    }
}
