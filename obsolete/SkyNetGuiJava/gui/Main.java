/*
 * Created on Oct 10, 2004
 * $Id$
 */
package gui;

import javax.swing.UIManager;

/**
 * Main method of SkyNet. Starts up the GUI and the ModuleCom class for communication with the
 * module daemons.
 * @author Henrik Kjellander (henrik.kjellander@home.se)
 */
public class Main {
	
	public static String defaultTestName = "test_1";
	public static String defaultHost = "henrik@gc.caltech.edu";
	public static String defaultDir = "~/test_logs";
	
    public static boolean debug = true;
    public static String VERSION = "0.01";

    public static void main(String[] argv) {
        try {
            /* This will select the default (Java) look and feel */
            //UIManager.setLookAndFeel(
            //UIManager.getCrossPlatformLookAndFeelClassName());
            /* Instead of that, choose the one that best matches our host system */
            UIManager.setLookAndFeel(
                UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) { }

        GUI gui = new GUI();
        gui.initializeGUI();
    }
}
