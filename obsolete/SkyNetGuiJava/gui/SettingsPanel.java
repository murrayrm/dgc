/*
 * Created on Oct 9, 2004
 * $Id$
 */
package gui;

import javax.swing.*;

import java.awt.event.*;

/**
 * Commands panel
 * @author Henrik Kjellander (henrik.kjellander@home.se)
 */
public class SettingsPanel
    extends JPanel {

    protected JButton loadButton = new JButton("Load");
    protected JButton saveButton = new JButton("Save");
    
    public SettingsPanel() {
        add(loadButton);
        add(saveButton);
        
        setBorder(BorderFactory.createTitledBorder("Settings"));
        // Button actions ##############################################################

        loadButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                log("Load button pressed");
                FilePickWindow filePick = new FilePickWindow();
                filePick.getFileDialog();
            }
        });
        
        saveButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                log("Save button pressed");
                // do the saving of settings
            }
        });
    }
    
    private void log(String text) {
        if (Main.debug) {
            System.out.println(text);
        }
    }
}