/*
 * Created Oct 9, 2004
 * $Id$
 */
package gui;

import java.io.*;
import java.awt.*;
import javax.swing.*;

/**
 * Class for selecting a file in a window
  * @author Henrik Kjellander (henrik.kjellander@home.se)
 */
public class FilePickWindow extends JDialog {
   private JPanel panel = new JPanel();
   private BorderLayout layout = new BorderLayout();
   private JFileChooser chooser = new JFileChooser(".");

   /**
    * Creates a FilePickWindow, but doesn't show the dialog yet
    */
   public FilePickWindow() {
      super(new JFrame(""), "Open config file", false);
      panel.setLayout(layout);
      getContentPane().add(panel);
      chooser.setFileFilter(new MyFileFilter());
      pack();
   }

   /**
    * Shows the file selection dialog. The user may select a file and that file
    * is returned.
    * @return    the file
    */
   public File getFileDialog() {
      if (chooser.showOpenDialog(panel) != JFileChooser.APPROVE_OPTION) {
         System.out.println("FilePickWindow: selection cancelled!");
      }
      return chooser.getSelectedFile();
   }

}