/*
 * Created on Oct 10, 2004
 * $Id$
 */
package gui;

import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;

/**
 * A debug printing window
 * @author Henrik Kjellander
 */
public class DebugWindow
    extends JPanel {
    JTextArea logWindow = new JTextArea(20, 30);
    BorderLayout borderLayout1 = new BorderLayout();
    TitledBorder titledBorder1;
    JScrollPane jScrollPane1 = new JScrollPane();

    /**
     * Constructor
     */
    public DebugWindow() {
        titledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.
            white, new Color(178, 178, 178)), "Debug log");
        logWindow.setBorder(null);
        logWindow.setEditable(false);
        logWindow.setText("");
        this.setLayout(borderLayout1);
        this.setBorder(titledBorder1);
        jScrollPane1.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        this.add(jScrollPane1, BorderLayout.CENTER);
        jScrollPane1.getViewport().add(logWindow, null);
    }

    public void log(String text) {
        //logWindow.append("\n" + text);
        logWindow.setText(logWindow.getText() + "\n" + text);
        logWindow.setCaretPosition(logWindow.getDocument().getLength());
    }
}
