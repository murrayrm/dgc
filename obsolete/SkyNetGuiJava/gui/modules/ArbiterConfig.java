/*
 * Created on Oct 9, 2004
 * $Id$
 */
package gui.modules;

import javax.swing.*;

/**
 * Config panel for the Arbiter 
 * @author Henrik Kjellander (henrik.kjellander@home.se)
 */
public class ArbiterConfig
    extends ModuleConfig {
   
	private JCheckBox noGlobal = new JCheckBox("No globalplanner", false);
	private JCheckBox noObstacle = new JCheckBox("No obstacle sensor data", false);
	private JCheckBox noState = new JCheckBox("No state information", false);
	private JCheckBox noReverse = new JCheckBox("Disable reverse", false);
	private JCheckBox noVdrive = new JCheckBox("Disable VDrive commands sent", false);
	private JCheckBox noDisp = new JCheckBox("Disable sparow display", false);	
	private JCheckBox shapeVotes = new JCheckBox("Use shapevotes feature", false);
	
	/**
	 * Constructor.
	 *
	 * @author Henrik Kjellander (henrik.kjellander@home.se)
	 */
	public ArbiterConfig() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(noGlobal);
        add(noObstacle);
        add(noState);
        add(noReverse);
        add(noVdrive);
        add(noDisp);
        add(shapeVotes);
        setBorder(BorderFactory.createTitledBorder("Arbiter config"));
 	}
	
	/**
	 * Returns a the configuration string that is the result of the options selected 
	 * for this module.
	 *
	 * @author Henrik Kjellander (henrik.kjellander@home.se)
	 */
	public String getConfig() {
		// insert code to generate a proper string here
		return "";
	}
}
