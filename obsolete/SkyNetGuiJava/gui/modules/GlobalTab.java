/*
 * Created 9 October 2004
 * $Id$
 */
package gui.modules;

import gui.*;
import javax.swing.*;

/**
 * A Module tab
 * @author Henrik Kjellander
 */
public class GlobalTab extends JPanel {

	private GUI gui;

	
	public GlobalTab(GUI gui) {
		this.gui = gui;
		add(new JLabel("Global"));
	}
}
