/*
 * Created on Oct 9, 2004
 * $Id$
 */
package gui.modules;

import javax.swing.*;

/**
 * Config panel superclass. 
 * @author Henrik Kjellander (henrik.kjellander@home.se)
 */
public abstract class ModuleConfig
    extends JPanel {
   
	/**
	 * Constructor.
	 *
	 * @author Henrik Kjellander (henrik.kjellander@home.se)
	 */
	public ModuleConfig() {
	}
	
	/**
	 * Returns a the configuration string that is the result of the options selected 
	 * for this module.
	 *
	 * @author Henrik Kjellander (henrik.kjellander@home.se)
	 */
	public abstract String getConfig();	
}
