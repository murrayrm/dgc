/*
 * Created 9 October 2004
 * $Id$
 */
package gui.modules;

import gui.*;
import javax.swing.*;

/**
 * A Module tab
 * @author Henrik Kjellander (henrik.kjellander@home.se)
 */
public class ArbiterTab extends JPanel {

	private GUI gui;

	
	public ArbiterTab(GUI gui) {
		this.gui = gui;
		add(new JLabel("Arbiter"));
	}
}
