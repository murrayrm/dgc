#include "trajController/trajController.h"
#include "CCorridorPainter.hh"
#include "CMap/CMapPlus.hh"
#include "rddf.hh"
#include "nomta/nomta.h"
#include <pthread.h>
#include <fstream>
using namespace std;


#define CONTROLPERIOD 100000 // in usec: 10Hz

void* getstate_thread_func(void *pArg)
{
	SStateAndSocket *pStateAndSocket = (SStateAndSocket *)pArg;
	pStateAndSocket->socket = setupUDP(STATEPORT);
	if(pStateAndSocket->socket < 0)
		return NULL;

	while(true)
	{
#warning "mutex here"
		recvUDP(pStateAndSocket->socket, STATEPORT,
						(char*)&pStateAndSocket->SS, sizeof(pStateAndSocket->SS));
	}
}

void* getplans_thread_func(void *pArg)
{
	SPlansAndSocket *pPlansAndSocket = (SPlansAndSocket *)pArg;
	pPlansAndSocket->socket = setupTCPserver(PLANSPORT);
	if(pPlansAndSocket->socket < 0)
		return NULL;

	while(true)
	{
#warning "mutex here"
		recvTCP(pPlansAndSocket->socket,
						(char*)&pPlansAndSocket->traj, CTraj::getHeaderSize());
		recvTCP(pPlansAndSocket->socket, (char*)pPlansAndSocket->traj.getNdiffarray(0), pPlansAndSocket->traj.getNumPoints() * sizeof(double));
		recvTCP(pPlansAndSocket->socket, (char*)pPlansAndSocket->traj.getNdiffarray(1), pPlansAndSocket->traj.getNumPoints() * sizeof(double));
		recvTCP(pPlansAndSocket->socket, (char*)pPlansAndSocket->traj.getNdiffarray(2), pPlansAndSocket->traj.getNumPoints() * sizeof(double));
		recvTCP(pPlansAndSocket->socket, (char*)pPlansAndSocket->traj.getEdiffarray(0), pPlansAndSocket->traj.getNumPoints() * sizeof(double));
		recvTCP(pPlansAndSocket->socket, (char*)pPlansAndSocket->traj.getEdiffarray(1), pPlansAndSocket->traj.getNumPoints() * sizeof(double));
		recvTCP(pPlansAndSocket->socket, (char*)pPlansAndSocket->traj.getEdiffarray(2), pPlansAndSocket->traj.getNumPoints() * sizeof(double));

		pPlansAndSocket->traj.print(pPlansAndSocket->outputPlans);
		pPlansAndSocket->outputPlans << endl;

		pPlansAndSocket->traj.print(pPlansAndSocket->outputPlanStarts, 1);
	}
}

int main(int argc, char **argv) 
{
	SStateAndSocket stateAndSocket;
	SPlansAndSocket plansAndSocket;
	SCmd cmd;
  char* pFile = "feedback.dat";
	CTrajController follow(&plansAndSocket.traj, pFile);
  ofstream outputState;

  char stateFileName[256];
  char plansFileName[256];
	char planStartsFileName[256];
	char lnCmd[256];

  time_t t = time(NULL);
  tm *local;
  local = localtime(&t);

  sprintf(stateFileName, "state_%04d%02d%02d_%02d%02d%02d.dat", 
	  local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	  local->tm_hour, local->tm_min, local->tm_sec);

  sprintf(plansFileName, "plans_%04d%02d%02d_%02d%02d%02d.dat", 
	  local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	  local->tm_hour, local->tm_min, local->tm_sec);

  sprintf(planStartsFileName, "planstarts_%04d%02d%02d_%02d%02d%02d.dat", 
	  local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	  local->tm_hour, local->tm_min, local->tm_sec);

	plansAndSocket.outputPlanStarts.open(planStartsFileName);
  plansAndSocket.outputPlans.open(plansFileName);
  outputState.open(stateFileName);

	// make state.dat, plans.dat, planstarts.dat links to the latest data files
	lnCmd[0] = '\0';
	strcat(lnCmd, "ln -f -s ");
	strcat(lnCmd, stateFileName);
	strcat(lnCmd, " state.dat");
	system(lnCmd);
	lnCmd[0] = '\0';
	strcat(lnCmd, "ln -f -s ");
	strcat(lnCmd, plansFileName);
	strcat(lnCmd, " plans.dat");
	system(lnCmd);
	lnCmd[0] = '\0';
	strcat(lnCmd, "ln -f -s ");
	strcat(lnCmd, planStartsFileName);
	strcat(lnCmd, " planstarts.dat");
	system(lnCmd);

  outputState << setprecision(20);
  plansAndSocket.outputPlans << setprecision(20);
  plansAndSocket.outputPlanStarts << setprecision(20);


	int cmdSocket = setupTCPclient(CONTROLLERIP, CMDPORT);

	pthread_t getstate_thread_id;
	if(pthread_create(&getstate_thread_id, NULL, &getstate_thread_func,
										&stateAndSocket) != 0)
	{
		cerr << "Couldn't create getstate thread\n";
		close(stateAndSocket.socket);
		close(plansAndSocket.socket);
		return 0;
	}
	pthread_t getplans_thread_id;
	if(pthread_create(&getplans_thread_id, NULL, &getplans_thread_func,
										&plansAndSocket) != 0)
	{
		cerr << "Couldn't create getplans thread\n";
		close(stateAndSocket.socket);
		close(plansAndSocket.socket);
		return 0;
	}

	while(true)
	{
		timeval timestamp1, timestamp2;
		// time checkpoint at the start of the cycle
		gettimeofday(&timestamp1, NULL);


    follow.getControlVariables( &stateAndSocket.SS, &cmd.acc, &cmd.phi);

#warning "mutex here"
		sendTCP(cmdSocket, (char*)&cmd, sizeof(cmd));
    outputState << 
			stateAndSocket.SS.Timestamp.dbl() << ' ' << 
			stateAndSocket.SS.Easting << ' ' <<
			stateAndSocket.SS.Northing << ' ' <<
			stateAndSocket.SS.Altitude << ' ' <<
			stateAndSocket.SS.Vel_E << ' ' <<
			stateAndSocket.SS.Vel_N << ' ' <<
			stateAndSocket.SS.Vel_U << ' ' <<
			stateAndSocket.SS.Speed << ' ' <<
			stateAndSocket.SS.Accel << ' ' <<
			stateAndSocket.SS.Pitch << ' ' <<
			stateAndSocket.SS.Roll << ' ' <<
			stateAndSocket.SS.Yaw << ' ' <<
			stateAndSocket.SS.PitchRate << ' ' <<
			stateAndSocket.SS.RollRate << ' ' <<
			-stateAndSocket.SS.YawRate << ' ' <<
			cmd.phi << ' ' <<
			cmd.acc << endl;


		// time checkpoint at the end of the cycle
		gettimeofday(&timestamp2, NULL);

		// now sleep for CONTROLPERIOD usec less the amount of time the
		// computation took
		int delaytime =
			CONTROLPERIOD -
			(
			 (timestamp2.tv_sec - timestamp1.tv_sec)*1000000 +
			 (timestamp2.tv_usec - timestamp1.tv_usec)
		  );
		if(delaytime > 0)
		{
			timespec sleeptime;
			sleeptime.tv_sec = 0;
			sleeptime.tv_nsec = delaytime * 1000;
			nanosleep(&sleeptime, NULL);
		}
	}

  return 0;
}
