#include "planner/planner.h"
#include "CCorridorPainter.hh"
#include "CMap/CMapPlus.hh"
#include "rddf.hh"
#include "simulator.hh"
#include "nomta/nomta.h"
#include <pthread.h>
#include <fstream>
using namespace std;

#define SIM_DELAY										10000000 // in nsec: 0.01s
#define STATESENDPERIOD							10000    // in usec: 100Hz

void* sendstate_thread_func(void *pArg)
{
	SStateAndSocket *pStateAndSocket = (SStateAndSocket *)pArg;
	pStateAndSocket->socket = setupUDP(STATEUDPDUMMY);
	if(pStateAndSocket->socket < 0)
		return NULL;

	while(true)
	{
		timeval timestamp1, timestamp2;
		// time checkpoint at the start of the cycle
		gettimeofday(&timestamp1, NULL);

#warning "mutex here"
		sendUDP(pStateAndSocket->socket, STATEPORT,
						(char*)&pStateAndSocket->SS, sizeof(pStateAndSocket->SS));
// 		cout << "sent state: " << pStateAndSocket->SS.Northing << ' ' << pStateAndSocket->SS.Easting << endl;

		// time checkpoint at the end of the cycle
		gettimeofday(&timestamp2, NULL);

		// now sleep for STATESENDPERIOD usec less the amount of time the
		// computation took
		int delaytime =
			STATESENDPERIOD -
			(
			 (timestamp2.tv_sec - timestamp1.tv_sec)*1000000 +
			 (timestamp2.tv_usec - timestamp1.tv_usec)
		  );
		if(delaytime > 0)
		{
			timespec sleeptime;
			sleeptime.tv_sec = 0;
			sleeptime.tv_nsec = delaytime * 1000;
			nanosleep(&sleeptime, NULL);
		}
	}
}

void* getcmd_thread_func(void* pArg)
{
	SCmdAndSocket *pCmdAndSocket = (SCmdAndSocket *)pArg;
	pCmdAndSocket->socket = setupTCPserver(CMDPORT);
	if(pCmdAndSocket->socket < 0)
		return NULL;

	while(true)
	{
#warning "mutex here"
		recvTCP(pCmdAndSocket->socket,
						(char*)&pCmdAndSocket->cmd, sizeof(pCmdAndSocket->cmd));
	}
}

int main(int argc, char **argv) 
{
	SStateAndSocket stateAndSocket;
	SCmdAndSocket cmdAndSocket;

	ofstream outfile("sim.log");
	outfile << setprecision(10);
	Simulator sim;
	simData simstate;

// 	simstate.n = 3834661.7097570645;
// 	simstate.e = 442556.54782862734;
// 	simstate.n_vel = 20.057861328125 * cos(2.8474113941192627);
// 	simstate.e_vel = 20.057861328125 * sin(2.8474113941192627);
// 	simstate.vel = 10.057861328125;
// 	simstate.n_acc = -0.88271725177764893;
// 	simstate.e_acc = 0.056792207062244415;
// 	simstate.yaw = atan2(simstate.e_vel, simstate.n_vel);

  simstate.n = SIMULATOR_INITIAL_NORTHING;
  simstate.e = SIMULATOR_INITIAL_EASTING;
  simstate.vel = 0.0;
  simstate.n_vel = 0.0;
  simstate.e_vel = 0.0;
  simstate.u_vel = 0.0;
  simstate.n_acc = 0.0;
  simstate.e_acc = 0.0;
  simstate.yaw = SIMULATOR_INITIAL_YAW;

	stateAndSocket.SS.Northing = simstate.n;
	stateAndSocket.SS.Easting  = simstate.e;
	stateAndSocket.SS.Speed    = simstate.vel;
	stateAndSocket.SS.Vel_N    = simstate.n_vel;
	stateAndSocket.SS.Vel_E    = simstate.e_vel;
	stateAndSocket.SS.Vel_U    = simstate.u_vel;
	stateAndSocket.SS.PitchRate= simstate.n_acc;
	stateAndSocket.SS.RollRate = simstate.e_acc;
	stateAndSocket.SS.Yaw    = simstate.yaw;
	// Normalize angle to be between [-pi,pi] and
	stateAndSocket.SS.Yaw = atan2( sin(stateAndSocket.SS.Yaw), cos(stateAndSocket.SS.Yaw) );
	stateAndSocket.SS.YawRate = stateAndSocket.SS.Speed / VEHICLE_WHEELBASE * tan(simstate.phi);

	cmdAndSocket.cmd.acc = 0.0;
	cmdAndSocket.cmd.phi = 0.0;


	pthread_t sendstate_thread_id;
	pthread_t getcmd_thread_id;
	if(pthread_create(&sendstate_thread_id, NULL, &sendstate_thread_func,
										&stateAndSocket) != 0)
	{
		cerr << "Couldn't create sendstate thread\n";
		close(stateAndSocket.socket);
		close(cmdAndSocket.socket);
		return 0;
	}
	if(pthread_create(&getcmd_thread_id, NULL, &getcmd_thread_func,
										&cmdAndSocket) != 0)
	{
		cerr << "Couldn't create getcmd thread\n";
		close(stateAndSocket.socket);
		close(cmdAndSocket.socket);
		return 0;
	}

	timeval oldtime;
  sim.initState(simstate);
	gettimeofday(&oldtime, NULL);

	while(true)
	{
		timespec sleeptime;
		timeval currtime;
		double deltaT;

		sleeptime.tv_sec = 0;
		sleeptime.tv_nsec = SIM_DELAY;
		nanosleep(&sleeptime, NULL);

		gettimeofday(&currtime, NULL);
		deltaT = (double)(currtime.tv_sec - oldtime.tv_sec) + 1.0e-6*(currtime.tv_usec - oldtime.tv_usec);

		sim.setStep(deltaT);
		sim.setSim(0.0, cmdAndSocket.cmd.acc, cmdAndSocket.cmd.phi);
// 		cout << "simulating: cmd = " << cmdAndSocket.cmd.acc << ' ' <<  cmdAndSocket.cmd.phi << endl;
		sim.simulate();
		simstate = sim.getState();

		stateAndSocket.SS.Northing = simstate.n;
		stateAndSocket.SS.Easting  = simstate.e;
		stateAndSocket.SS.Speed    = simstate.vel;
		stateAndSocket.SS.Vel_N    = simstate.n_vel;
		stateAndSocket.SS.Vel_E    = simstate.e_vel;
		stateAndSocket.SS.Vel_U    = simstate.u_vel;
		stateAndSocket.SS.PitchRate= simstate.n_acc;
		stateAndSocket.SS.RollRate = simstate.e_acc;
		stateAndSocket.SS.Yaw    = simstate.yaw;

		// Normalize angle to be between [-pi,pi] and
		stateAndSocket.SS.Yaw = atan2( sin(stateAndSocket.SS.Yaw), cos(stateAndSocket.SS.Yaw) );
		stateAndSocket.SS.YawRate = stateAndSocket.SS.Speed / VEHICLE_WHEELBASE * tan(simstate.phi);

		memcpy(&oldtime, &currtime, sizeof(currtime));

		outfile
			<< simstate.n << ' ' << simstate.n_vel << ' ' << simstate.n_acc << ' '
			<< simstate.e << ' ' << simstate.e_vel << ' ' << simstate.e_acc << endl;
	}

  return 0;
}
