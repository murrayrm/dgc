#include "planner/planner.h"
#include "CCorridorPainter.hh"
#include "CMap/CMapPlus.hh"
#include "rddf.hh"
#include "nomta/nomta.h"
#include <pthread.h>
#include <fstream>
using namespace std;



#define DEFAULT_MAP_NUM_ROWS 1000
#define DEFAULT_MAP_NUM_COLS 1000
#define DEFAULT_MAP_ROW_RES 0.2
#define DEFAULT_MAP_COL_RES 0.2
#define FM_DOUBLE_NO_DATA 0.0

void* getstate_thread_func(void *pArg)
{
	SStateAndSocket *pStateAndSocket = (SStateAndSocket *)pArg;
	pStateAndSocket->socket = setupUDP(STATEPORT);
	if(pStateAndSocket->socket < 0)
		return NULL;

	while(true)
	{
#warning "mutex here"
		recvUDP(pStateAndSocket->socket, STATEPORT,
						(char*)&pStateAndSocket->SS, sizeof(pStateAndSocket->SS));
	}
}

int main(int argc, char **argv) 
{
  CMapPlus	plannerModuleMap;
  RDDF			plannerModuleRDDF;
	CCorridorPainter plannerModulePainter;
	int ladarCostLayerID;
  int rddfLayerID;
  int costLayerID;


	SStateAndSocket stateAndSocket;

	int plansSocket = setupTCPclient(CONTROLLERIP, PLANSPORT);

	pthread_t getstate_thread_id;
	if(pthread_create(&getstate_thread_id, NULL, &getstate_thread_func,
										&stateAndSocket) != 0)
	{
		cerr << "Couldn't create getstate thread\n";
		close(stateAndSocket.socket);
		close(plansSocket);
		return 0;
	}

  plannerModuleMap.initMap(0,0,
													 DEFAULT_MAP_NUM_ROWS, DEFAULT_MAP_NUM_COLS,
													 DEFAULT_MAP_ROW_RES,  DEFAULT_MAP_COL_RES,
													 0 );
  
  // Add a layer to the map that represents the elevation computed from the 
  // Ladar measurements, and another that holds the elevation variance.
  ladarCostLayerID = plannerModuleMap.addLayer<double>(FM_DOUBLE_NO_DATA, 
																											 CM_DOUBLE_OUTSIDE_MAP);
  rddfLayerID = plannerModuleMap.addLayer<int>(CP_OUTSIDE_CORRIDOR, CP_OUTSIDE_MAP);
  costLayerID = plannerModuleMap.addLayer<double>(0, CM_DOUBLE_OUTSIDE_MAP);

  //Initialize the corridor painter
  plannerModulePainter.initPainter(&plannerModuleMap, rddfLayerID,
																	 &plannerModuleRDDF,
																	 CP_INSIDE_CORRIDOR, CP_OUTSIDE_CORRIDOR);

	CPlanner planner(&plannerModuleMap, rddfLayerID, &plannerModuleRDDF);

	while(true)
	{
		if(planner.plan(&stateAndSocket.SS))
		{
			CTraj *pTraj = planner.getTraj();

			cout << "succeeded" << endl;
			ofstream trajout("after");
			pTraj->print(trajout);

			if(plansSocket > 0)
			{
				sendTCP(plansSocket, (char*)pTraj, CTraj::getHeaderSize());
				sendTCP(plansSocket, (char*)pTraj->getNdiffarray(0), pTraj->getNumPoints() * sizeof(double));
				sendTCP(plansSocket, (char*)pTraj->getNdiffarray(1), pTraj->getNumPoints() * sizeof(double));
				sendTCP(plansSocket, (char*)pTraj->getNdiffarray(2), pTraj->getNumPoints() * sizeof(double));
				sendTCP(plansSocket, (char*)pTraj->getEdiffarray(0), pTraj->getNumPoints() * sizeof(double));
				sendTCP(plansSocket, (char*)pTraj->getEdiffarray(1), pTraj->getNumPoints() * sizeof(double));
				sendTCP(plansSocket, (char*)pTraj->getEdiffarray(2), pTraj->getNumPoints() * sizeof(double));

			}
		}
		else
			cout << "failed" << endl;
	}

  return 0;
}
