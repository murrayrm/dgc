#ifndef _NOMTA_H_
#define _NOMTA_H_

#define BACKLOG					10		 // how many pending connections queue will hold
#define STATEPORT				5555
#define STATEUDPDUMMY   6555
#define PLANSPORT				5556
#define CMDPORT					5557


#warning "define this properly"
#define CONTROLLERIP "127.0.0.1"


struct SCmd
{
	double acc, phi;
};

struct SStateAndSocket
{
	VState_GetStateMsg SS;
	int socket;
};
struct SCmdAndSocket
{
	SCmd cmd;
	int socket;
};
struct SPlansAndSocket
{
	CTraj traj;
	int socket;
	ofstream outputPlans, outputPlanStarts;
};

int setupTCPserver(int serverport);
int setupTCPclient(char* serverip, int serverport);
int sendTCP(int datafd, char* buf, int len);
int recvTCP(int datafd, char* buf, int len);

int setupUDP(int serverport);
int sendUDP(int datafd, int port, char* buf, int len);
int recvUDP(int datafd, int port, char* buf, int len);

#endif
