#include "planner/planner.h"
#include "nomta.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <iostream>
using namespace std;

int setupTCPserver(int serverport)
{
	cout << "setting up TCP server" << endl;

	int datafd, socketfd;
	union
	{
		struct sockaddr_in addr_in;
		struct sockaddr 	 addr;
	};

	socklen_t sin_size;
	int yes = 1;


	socketfd = socket(AF_INET, SOCK_STREAM, 0);
	if(socketfd == -1)
	{
		cerr << "setupTCPserver: Couldn't open server socket\n";
		return -1;
	}

	if(setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1)
	{
		cerr << "setupTCPserver: Couldn't reuse the socket\n";
		close(socketfd);
		return -1;
	}

	memset(&addr, '\0', sizeof(addr));
	addr_in.sin_family = AF_INET;
	addr_in.sin_port = htons(serverport);
	addr_in.sin_addr.s_addr = INADDR_ANY;

	if(bind(socketfd, &addr, sizeof(addr)) == -1)
	{
		cerr << "setupTCPserver: Couldn't bind the socket\n";
		close(socketfd);
		return -1;
	}

	if(listen(socketfd, BACKLOG) == -1)
	{
		cerr << "setupTCPserver: Couldn't listen to the socket\n";
		close(socketfd);
		return -1;
	}

	sin_size = sizeof(addr);
	if ((datafd = accept(socketfd, &addr, &sin_size)) == -1)
	{
		cerr << "setupTCPserver: Couldn't accept the socket to the client\n";
		close(socketfd);
		return -1;
	}

	close(socketfd);
	cout << "successfully set up TCP server" << endl;
	return datafd;
}

int setupTCPclient(char* serverip, int serverport)
{
	cout << "setting up TCP client" << endl;
	int socketfd;

	union
	{
		struct sockaddr_in addr_in;
		struct sockaddr 	 addr;
	};
		
	socklen_t sin_size;
	int yes = 1;
	

	socketfd = socket(AF_INET, SOCK_STREAM, 0);
	if(socketfd == -1)
	{
		cerr << "setupTCPclient: Couldn't open client socket\n";
		return -1;
	}

	if(setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1)
	{
		cerr << "setupTCPclient: Couldn't reuse the socket\n";
		close(socketfd);
		return -1;
	}

	memset(&addr, '\0', sizeof(addr));
	addr_in.sin_family = AF_INET;
	addr_in.sin_port = htons(serverport);
	addr_in.sin_addr.s_addr = inet_addr(serverip);

	if(connect(socketfd, &addr, sizeof(addr)) == -1)
	{
		cerr << "setupTCPclient: Couldn't connect to the socket\n";
		close(socketfd);
		return -1;
	}

	cout << "successfully set up TCP client" << endl;
	return socketfd;
}

int sendTCP(int datafd, char* buf, int len)
{
	int numsent, numlefttosend;
	numlefttosend = len;

	do
	{
		numsent = send(datafd, buf + len - numlefttosend,
									 numlefttosend, 0);
		numlefttosend -= numsent;

		if(numsent == -1)
		{
			cerr << "sendTCP: Couldn't send data\n";
			return -1;
		}

		if(numsent == 0)
		{
			cerr << "sendTCP: no data sent\n";
			return -2;
		}
	} while(numlefttosend != 0);

	return 0;
}

int recvTCP(int datafd, char* buf, int len)
{
	int numreceived, numlefttorecv;
	numlefttorecv = len;

	do
	{
		numreceived = recv(datafd, buf + len - numlefttorecv,
									 numlefttorecv, 0);
		numlefttorecv -= numreceived;

		if(numreceived == -1)
		{
			cerr << "recvTCP: Couldn't recv data\n";
			return -1;
		}

		if(numreceived == 0)
		{
			cerr << "recvTCP: No data received: \n";
			return -2;
		}
	} while(numlefttorecv != 0);

	return 0;
}

int setupUDP(int serverport)
{
	cout << "setting up UDP connection" << endl;
	int socketfd;

	union
	{
		struct sockaddr_in addr_in;
		struct sockaddr 	 addr;
	};
		
	socklen_t sin_size;
	int yes = 1;
	

	socketfd = socket(AF_INET, SOCK_DGRAM, 0);
	if(socketfd == -1)
	{
		cerr << "setupUDP: Couldn't open socket\n";
		return -1;
	}

	if(setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1)
	{
		cerr << "setupUDP: Couldn't reuse the socket\n";
		close(socketfd);
		return -1;
	}

	memset(&addr, '\0', sizeof(addr));
	addr_in.sin_family = AF_INET;
	addr_in.sin_port = htons(serverport);
	addr_in.sin_addr.s_addr = INADDR_ANY;

	if(bind(socketfd, &addr, sizeof(addr)) == -1)
	{
		cerr << "setupUDP: Couldn't bind to the socket\n";
		close(socketfd);
		return -1;
	}

	cout << "successfully set up UDP connection" << endl;
	return socketfd;
}

int sendUDP(int datafd, int port, char* buf, int len)
{
	union
	{
		struct sockaddr_in addr_in;
		struct sockaddr 	 addr;
	};
	memset(&addr, '\0', sizeof(addr));
	addr_in.sin_family = AF_INET;
	addr_in.sin_port = htons(port);
	addr_in.sin_addr.s_addr = inet_addr("127.0.0.1");

	int sent = sendto(datafd, buf, len, 0, &addr, sizeof(addr_in));
	if(sent != len)
	{
		cerr << "sendUDP: tried sending " << len << ", sent " << sent << endl;
		return -1;
	}

	return 0;
}

int recvUDP(int datafd, int port, char* buf, int len)
{
	union
	{
		struct sockaddr_in addr_in;
		struct sockaddr 	 addr;
	};
	socklen_t addrlen;

	int recvd= recvfrom(datafd, buf, len, 0, &addr, &addrlen);
	if(recvd != len)
	{
		cerr << "recvUDP: tried sending " << len << ", received " << recvd << endl;
		return -1;
	}

	return 0;
}
