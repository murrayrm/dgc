#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <stereovision/getpair.h>
#include <videre/svsclass.h>
#include <frames/frames.hh>
#include <genMap.hh>
#include <cv.h>
#include <cvaux.h>
#include <highgui.h>
#include <vehlib/VState.hh>
#include <stereovision/svsStereoWrapper.hh>

//Some defaults for the map
#define DEFAULT_SVMAP_NUM_ROWS 250
#define DEFAULT_SVMAP_NUM_COLS 250
#define DEFAULT_SVMAP_ROW_RES  0.2
#define DEFAULT_SVMAP_COL_RES  0.2

#define X_LIMIT DEFAULT_SVMAP_NUM_ROWS*DEFAULT_SVMAP_ROW_RES*0.5
#define Y_LIMIT DEFAULT_SVMAP_NUM_COLS*DEFAULT_SVMAP_COL_RES*0.5
#define Z_LIMIT 1000

//Some defaults for the svs object
#define DEFAULT_SVSCAL_FILE_NAME    "../../calibration/SVSCal.ini"
#define DEFAULT_SVSPARAMS_FILE_NAME "../../calibration/SVSParams.ini"
#define DEFAULT_CAMCAL_FILE_NAME    "../../calibration/CamCal.ini"

//Some defaults for the input
#define LEFT_FILE_NAME_PREFIX_DEFAULT    "Default"
#define RIGHT_FILE_NAME_PREFIX_DEFAULT   "Default"
#define LEFT_FILE_NAME_SUFFIX_DEFAULT    "-L"
#define RIGHT_FILE_NAME_SUFFIX_DEFAULT   "-R"
#define STATE_FILE_NAME_DEFAULT          "Default.log"
#define FILE_TYPE_DEFAULT "pgm"
#define MAX_FRAMES_DEFAULT 10


int main(int argc, char *argv[]) {
  //Variables we use for processing
  int numPoints;
  genMap svMap(FALSE);
  VState_GetStateMsg SS;
  XYZcoord UTMPoint, Point;
  svsStereoWrapper svsObject;
  int used_points;

  //Variables for various options
  int max_frames = MAX_FRAMES_DEFAULT;
  int verbose = 0;
  int prompt = 0;
  int delay = 0;
  int forever = 0;
  int test_num;
  int current_frame = 0;
  int use_cameras = 0;
  char left_file_name_prefix[200]="", right_file_name_prefix[200]="";
  char left_file_name_suffix[40],     right_file_name_suffix[40];
  char left_file_name[256]="",        right_file_name[256]="";
  char state_file_name[256];
  char pts_file_name[256];
  char utmpts_file_name[256];
  char map_file_name[256];
  char img_file_name[256];
  char disp_file_name[256];
  char file_type[10];

  double xUpLimit, yUpLimit, zUpLimit;
  double xLowLimit, yLowLimit, zLowLimit;

  double maxx, maxy, maxz, minx, miny, minz;

  FILE *pointFile, *utmPointFile;
    
  strcpy(left_file_name_prefix, LEFT_FILE_NAME_PREFIX_DEFAULT);
  strcpy(left_file_name_suffix, LEFT_FILE_NAME_SUFFIX_DEFAULT);
  strcpy(right_file_name_prefix, RIGHT_FILE_NAME_PREFIX_DEFAULT);
  strcpy(right_file_name_suffix, RIGHT_FILE_NAME_SUFFIX_DEFAULT);
  strcpy(state_file_name, STATE_FILE_NAME_DEFAULT);
  strcpy(file_type, FILE_TYPE_DEFAULT);

  //First, check arguments to see our options
  if( argc > 1 ) { // if we have arguments
  
    for( int i=1; i < argc; i++ ) { 
      // go through each of the arguments and check...
      if(strcmp(argv[i], "-help")==0 || strcmp(argv[i], "--help")==0 || strcmp(argv[i], "-h")==0) {
	printf("\nUsage instructions");
	printf("\nThere are four ways to specify the images files you want to process: ");
	printf("\nDefault:                          Default0000-L.pgm,   Default0000-R.pgm");
	printf("\n-lname leftname -rname rightname: leftname0000.pgm,    rightname0000.pgm");
	printf("\n-name somename:                   somename0000-L.pgm, somename0000-R.pgm");
	printf("\n-num 42:                          Test42-0000-L.pgm,   Test42-0000-R.pgm");
	printf("\n-cal:                             cal0000-L.bmp,       cal0000-R.bmp (-c also works)");
	printf("\n-type bmp:                        Reads the images as a .bmp (.jpg also supported, -t also works)");
	printf("\n(-b works as -type bmp, -j works as -type jpg)");
	printf("\nAdditional Commands:");
	printf("\n-start num: Starts at a number other than 1 (-s also works)");
	printf("\n-usecams:   Use the cameras - not files (-u also works)"); 
	printf("\n-max num:   Processes a maximum of num frames (-m also works)");
	printf("\n-delay num: Introduces a wait of delay ms between processes (-d also works)");
	printf("\n-forever:   Runs the processing forever");
	printf("\n            (Note that the program will quit when it encounters any non-existent files");
	printf("\n-verbose:   Runs in verbose mode (-v also works)");
	printf("\n-prompt:    Prompts the user to get each image pair (-p also works)");
	printf("\n");
	return 0;
      }
      if(strcmp(argv[i], "-usecams")==0 || strcmp(argv[i], "-u")==0) {
	use_cameras = 1;
      }
      if(strcmp(argv[i], "-start")==0 || strcmp(argv[i], "-s")==0) {
	i++;
	current_frame = atoi(argv[i])-1;
      }
      if(strcmp(argv[i], "-max")==0 || strcmp(argv[i], "-m")==0) {
	i++;
	max_frames = atoi(argv[i]);
      }
      if(strcmp(argv[i], "-delay")==0 || strcmp(argv[i], "-d")==0) {
	i++;
	delay = atoi(argv[i])*1000;
      }
      if(strcmp(argv[i], "-prompt")==0 || strcmp(argv[i], "-p")==0) {
	prompt = 1;
      }
      if(strcmp(argv[i], "-verbose")==0 || strcmp(argv[i], "-v")==0) {
	verbose = 1;
      }
      if(strcmp(argv[i], "-forever")==0 || strcmp(argv[i], "-f")==0) {
	forever = 1;
      }
      if(strcmp(argv[i], "-type") == 0 || strcmp(argv[i], "-t")==0) {
	i++;
	strcpy(file_type, argv[i]);
      }
      if(strcmp(argv[i], "-b") == 0) {
	strcpy(file_type, "bmp");
      }
      if(strcmp(argv[i], "-j") == 0) {
	strcpy(file_type, "jpg");
      }
      if(strcmp(argv[i], "-lname")==0) {
	i++;
	strcpy(left_file_name_prefix, argv[i]);
      }
      if(strcmp(argv[i], "-rname")==0) {
	i++;
	strcpy(right_file_name_prefix, argv[i]);
      }
      if(strcmp(argv[i], "-num")==0) {
	i++;
	test_num = atoi(argv[i]);
	sprintf(left_file_name_prefix, "Test%d-", test_num);
	sprintf(right_file_name_prefix, "Test%d-", test_num);
      }
      if(strcmp(argv[i], "-name")==0) {
	i++;
	sprintf(left_file_name_prefix, "%s", argv[i]);
	sprintf(right_file_name_prefix, "%s", argv[i]);
	sprintf(state_file_name, "%s.log", argv[i]);
      }
      if(strcmp(argv[i], "-cal")==0) {
	sprintf(left_file_name_prefix, "cal");
	sprintf(right_file_name_prefix, "cal");
	sprintf(file_type, "bmp");
      }
    }
  }


  //Initialize our objects
  svMap.initMap(DEFAULT_SVMAP_NUM_ROWS, DEFAULT_SVMAP_NUM_COLS, DEFAULT_SVMAP_ROW_RES, DEFAULT_SVMAP_COL_RES, FALSE);   

  if(svsObject.init(SHORT_RANGE, DEFAULT_SVSCAL_FILE_NAME, DEFAULT_SVSPARAMS_FILE_NAME, DEFAULT_CAMCAL_FILE_NAME, use_cameras, verbose) != svsSW_OK) {
    printf("Error initializing svsStereoWrapper object at line %d in %s!  Quitting...\n", __LINE__, __FILE__);
    return -1;
  }


  //Run the processing loop
  while(current_frame < max_frames || forever==1) {
    usleep(delay);
    if(prompt == 1) {
      printf("Hit enter to process next pair");
      getchar();
    }

    //Setup the file names
    current_frame++;
    sprintf(left_file_name, "%s%.4d%s.%s", left_file_name_prefix, current_frame, left_file_name_suffix, file_type);
    sprintf(right_file_name, "%s%.4d%s.%s", right_file_name_prefix, current_frame, right_file_name_suffix, file_type);
    sprintf(map_file_name, "%s%.4dMap", left_file_name_prefix, current_frame);
    sprintf(img_file_name, "%s%.4dMap.pgm", left_file_name_prefix, current_frame);
    sprintf(pts_file_name, "%s%.4dPts.mat", left_file_name_prefix, current_frame);
    sprintf(utmpts_file_name, "%s%.4dUTMPts.mat", left_file_name_prefix, current_frame);
    sprintf(disp_file_name, "%s%.4dDisp.bmp", left_file_name_prefix, current_frame);

    //Grab the images
    if(use_cameras == 0) {
      if(svsObject.grabFilePair(left_file_name, right_file_name, state_file_name, current_frame) != svsSW_OK) {
	printf("Error while reading image files at line %d in %s!  Quitting...\n", __LINE__, __FILE__);
	return -1;
      }
    } else {
      if(svsObject.grabCameraPair(SS) != svsSW_OK) {
	printf("Error while reading image files at line %d in %s!  Quitting...\n", __LINE__, __FILE__);
	return -1;
      }
    }      

    //Process the images
    if(svsSW_OK != svsObject.processPair()) {
      printf("Error while processing image pair at line %d in %s!  Quitting...\n", __LINE__, __FILE__);
      return -1;
    } else if(verbose) {
      printf("Processed pair %d OK\n", current_frame);
    }

    //Save disparity image
    if(svsObject.saveDispImage(disp_file_name) != svsSW_OK) {
      printf("Error while saving image at line %d in %s!  Quitting...\n", __LINE__, __FILE__);
      return -1;
    }

    SS = svsObject.SS;
    numPoints = svsObject.numPoints();
    used_points = 0;
    svMap.updateFrame(svsObject.getCurrentState());
    if(verbose) printf("Filling the map with %d points..\n", numPoints);
    svMap.updateFrame(svsObject.SS);
    pointFile = NULL;
    utmPointFile = NULL;
    pointFile = fopen(pts_file_name, "w");
    utmPointFile = fopen(utmpts_file_name, "w");
    UTMPoint = svsObject.UTMPoint(0);
    xUpLimit = X_LIMIT + SS.Northing;
    yUpLimit = Y_LIMIT + SS.Easting;
    zUpLimit = Z_LIMIT;
    xLowLimit = SS.Northing - X_LIMIT;
    yLowLimit = SS.Easting -Y_LIMIT;
    zLowLimit = -Z_LIMIT;
    maxx = UTMPoint.x;
    minx = UTMPoint.x;
    maxy = UTMPoint.y;
    miny = UTMPoint.y;
    maxz = UTMPoint.z;
    minz = UTMPoint.z;
    for(int i=0; i < numPoints; i++) {
      if(svsObject.validPoint(i)) {
	UTMPoint = svsObject.UTMPoint(i);
	Point = svsObject.Point(i);
	if(UTMPoint.x < xUpLimit && UTMPoint.y < yUpLimit && UTMPoint.z < zUpLimit &&
	   UTMPoint.x > xLowLimit && UTMPoint.y > yLowLimit && UTMPoint.z > zLowLimit) {
	  if(UTMPoint.x > maxx) maxx = UTMPoint.x;
	  if(UTMPoint.y > maxy) maxy = UTMPoint.y;
	  if(UTMPoint.z > maxz) maxz = UTMPoint.z;
	  if(UTMPoint.x < minx) minx = UTMPoint.x;
	  if(UTMPoint.y < miny) miny = UTMPoint.y;
	  if(UTMPoint.z < minz) minz = UTMPoint.z;
	  svMap.setCellListDataUTM(UTMPoint.x, UTMPoint.y, UTMPoint.z);
	  if(pointFile != NULL) fprintf(pointFile, "%lf %lf %lf\n", Point.x, Point.y, Point.z);
	  if(utmPointFile != NULL) fprintf(utmPointFile, "%lf %lf %lf\n", UTMPoint.x, UTMPoint.y, UTMPoint.z);
	  used_points++;
	} else {
	  //printf("Point (%ld, %ld, %ld) rejected\n", UTMPoint.x, UTMPoint.y, UTMPoint.z);
	}
      }
    }
    printf("Vehicle origin (X,Y,Z): (%lf, %lf, %lf)\n", SS.Northing, SS.Easting, SS.Altitude);
    printf("lowLimits: (%lf, %lf, %lf)\n", xLowLimit, yLowLimit, zLowLimit);
    printf("upLimits: (%lf, %lf, %lf)\n", xUpLimit, yUpLimit, zUpLimit);
    if(pointFile != NULL) fclose(pointFile);
    if(utmPointFile != NULL) fclose(utmPointFile);
    printf("Max (X,Y,Z)=(%lf, %lf, %lf)\n", maxx, maxy, maxz);
    printf("Min (X,Y,Z)=(%lf, %lf, %lf)\n", minx, miny, minz);
    printf("The car is pointing %lf\n", SS.Yaw);
    printf("Used %d points\n", used_points);
    svMap.evaluateCellsSV(svsObject.numMinPoints);
    if(verbose) printf("Saving map image...\n");
    svMap.saveIMGFile(img_file_name);
    if(verbose) printf("Saving map MAT file...\n");
    svMap.saveMATFile(map_file_name, "Generated by createStereoImages", "Elevation");
  }

  if(verbose) printf("Work complete\n");

  return 0;
}
