IF THIS MODULE DOES NOT COMPILE, PLEASE READ /team/stereovision/README
IF THIS MODULE DOES NOT RUN, PLEASE READ /team/stereovision/README

Quick Basic Command Example:

./createStereoMaps -v -j -f -name TestRun1A
This example command processes images as fast as possible forever, in jpeg format, with the name TestRun1A0001-L.jpg, TestRun1A0001-R.jpg, TestRun1A0001.log, printing out to the screen when it does so.


Detailed Usage Instructions:
There are four ways to name the specify images files you want to process: 
Default:                          Default0000-L.pgm,   Default0000-R.pgm
-lname leftname -rname rightname: leftname0000.pgm,    rightname0000.pgm
-name somename:                   somename0000-L.pgm, somename0000-R.pgm
-num 42:                          Test42-0000-L.pgm,   Test42-0000-R.pgm
-cal:                             cal0000-L.bmp,       cal0000-R.bmp (-c also works)
-type bmp:                        Reads the images as a .bmp (.jpg also supported, -t also works)
(-b works the same as -type bmp, -j works as -type jpg)
When the images are logged as bmp or jpg, the state data is automatically recorded in a seperate file
Additional Commands:
-max num:   Processes a maximum of num frames (-m also works)
-delay num: Introduces a wait of delay ms between processes (-d also works)
-forever:   Runs the processing forever, instead of only for a certain number of frames (-f also works)
	    (Note that the program will quit when it encounters any non-existent files)
-verbose:   Runs in verbose mode (-v also works)
-prompt:    Prompts the user to get each image pair (-p also works)
