#include "RoadFollower.hh"

#include "MTA/Misc/Thread/ThreadsForClasses.hpp"

#include <iostream>

#define SOCKET_ERROR -1
#define RECORD_DATA 1
#define NO_RECORD 0

extern int SIM;
extern int DISPLAY;
extern int PRINT_VOTES;

int counter = 0;

RoadFollower::RoadFollower() 
  : DGC_MODULE(MODULES::RoadFollower, "RoadFollower Planner", 0) {

}

RoadFollower::~RoadFollower() {
}


void RoadFollower::Init() {
  // call the parent init first
  DGC_MODULE::Init();

  //Insert code to initialize RoadFollower here

  d.bufsize = 1024;
  d.buffer = (char *) malloc(d.bufsize);


  if ((d.create_socket = socket(AF_INET,SOCK_STREAM,0)) > 0)    printf("The socket was created\n");
  d.address.sin_family = AF_INET;
  d.address.sin_addr.s_addr = INADDR_ANY;
  d.address.sin_port = htons(15001);
  if (bind(d.create_socket,(struct sockaddr *)&(d.address),sizeof(d.address)) == 0)
    printf("Binding Socket\n");
  //  listen(create_socket,3);

  listen(d.create_socket,1);

  while (1) {
    d.new_socket = 0;
    while (!d.new_socket)
      d.new_socket = accept(d.create_socket, NULL, NULL);
    printf("Client connected\n");
    break;
  }


  cout << "Module Init Finished" << endl;
}

void RoadFollower::Active() {
  int i;
  double steerangle;
  double currentSteeringAngle = 123.4567;
  //  int modeFlag = NO_RECORD;
   int modeFlag = RECORD_DATA;

  cout << "Starting Active Funcion" << endl;
  while( ContinueInState() ) {

    // update the state so we know where the vehicle is at
    UpdateState();
    // printf("%d %d\n", d.SS.Timestamp.sec(), d.SS.Timestamp.usec());

    //  int bytesSent;
    int bytesSent, bytesRecv;
    
    //  char sendbuf[32] = "Server: Sending Data.";
    char recvbuf[1024] = "";
    char sendbuf[1024];
    char sendbufHeader[1024];
    char sendbufData[10240];

    int bufferSize;
    char* tempBuffer;

    int vote;
    double goodness, velocity;

    /*
    genMap tempMap(d.SS, 100, 100, 0.2, 0.2, false);
    CellData tempCell;
    for(int i=0; i<100; i++) {
      for(int j=0; j<100; j++) {
	if(i<50 && j<50) tempCell.value=0.25;
	if(i<50 && j>50) tempCell.value=0.50;
	if(i>=50 && j<50) tempCell.value=0.75;
	if(i>=50 && j>=50) tempCell.value=1;
	
	//printf("At %d %d: %lf\n", i, j, tempCell.value);
      }
    }
    */

    //while (1) {
    //while (bytesRecv == SOCKET_ERROR) {
 
      bytesRecv = recv(d.new_socket, recvbuf, 32, 0);
      if (bytesRecv == 0) {
	//printf("Connection closed\n");
	break;
      }
      sscanf(recvbuf, "%i %lf %lf", &vote, &goodness, &velocity);
      // printf("%i %lf %lf\n", vote, goodness, velocity);
      //printf("received %s\n", recvbuf);
      //printf("Bytes Recv: %ld\n", bytesRecv);
      //  bytesSent = send(new_socket, sendbuf, strlen(sendbuf), 0);


      UpdateState();
      
      sprintf(sendbuf, "%d %d %lf %lf %f %f %f %f %f %lf %f %f %f %f %f %f %lf %d ",
	      	      d.SS.Timestamp.sec(), d.SS.Timestamp.usec(),
	      // counter, counter,
	      d.SS.Easting, d.SS.Northing, d.SS.Altitude,
	      d.SS.Vel_E, d.SS.Vel_N, d.SS.Vel_U,
	      d.SS.Speed, d.SS.Accel,
	      d.SS.Pitch, d.SS.Roll, d.SS.Yaw,
	      d.SS.PitchRate, d.SS.RollRate, d.SS.YawRate,
	      currentSteeringAngle,
	      modeFlag);
      /*
//      printf("%d %d %lf %lf %f %f %f %f %f %lf %f %f %f %f %f %f\n\n",
	      d.SS.Timestamp.sec(), d.SS.Timestamp.usec(), 
	      d.SS.Easting, d.SS.Northing, d.SS.Altitude,
	      d.SS.Vel_E, d.SS.Vel_N, d.SS.Vel_U,
	      d.SS.Speed, d.SS.Accel,
	      d.SS.Pitch, d.SS.Roll, d.SS.Yaw,
	      d.SS.PitchRate, d.SS.RollRate, d.SS.YawRate);
      */

      bytesSent = send(d.new_socket, sendbuf, strlen(sendbuf), 0);
      counter++;

      //      printf("%d\n", modeFlag);

      //      if (counter == 100) {
      //	modeFlag = !modeFlag;
      //	counter = 0;
      //   }

      //printf("%d is %s\n", bytesSent, sendbuf);      
      /*
      bufferSize = tempMap.serializedMapSize();
      tempBuffer=(char*)malloc(bufferSize);
      tempMap.serializeMap(tempBuffer, bufferSize);
      */
      //sprintf(sendbufHeader, "M", bufferSize);

      //bytesSent += send(d.new_socket, sendbuf, strlen(sendbufHeader), 0);

      /*
      int numRows, numCols;
      float rowRes, colRes, Yaw;
      double UTMOriginX, UTMOriginY;


      //printf("Sending %d bytes\n", bufferSize);
      memcpy(sendbufHeader,tempBuffer, bufferSize);
      */
      /*
      sscanf(sendbuf, "M%d %d %f %f %lf %lf %f", 
	    &numRows, &numCols, 
	    &rowRes, &colRes, 
	    &UTMOriginX, &UTMOriginY, 
	    &Yaw);
      printf("Sending M%d %d %f %f %lf %lf %f\n", 
	    numRows, numCols, 
	    (float)rowRes, (float)colRes, 
	    UTMOriginX, UTMOriginY, 
	    (float)Yaw);
      */
      /*
      bytesSent = send(d.new_socket, sendbufHeader, bufferSize, 0);
      free(tempBuffer);

      //bufferSize = tempMap.serializedMapSize2();
      tempBuffer=(char*)malloc(bufferSize);
      //tempMap.serializeMap2(tempBuffer, bufferSize);
      printf("Data string: %d\n", bufferSize);
      bytesSent = send(d.new_socket, tempBuffer, bufferSize, 0);

      printf("Sent %f\n", *(float*)tempBuffer);
      */

      //free(tempBuffer);

    
    


      /*
    // Update the votes ... 
    for(int i = 0; i < NUMARCS; i++) {
      steerangle = GetPhi(i);
      d.roadfollower.Votes[i].Goodness = MAX_GOODNESS/2;
      d.roadfollower.Votes[i].Velo = 0;
    }


    // and send them to the arbiter
    Mail m = NewOutMessage(MyAddress(), MODULES::Arbiter, ArbiterMessages::Update);
    // Send the arbiter our "ID" and the vote struct.  The weights are taken care of
    // by the arbiter.
    int myID = ArbiterInput::RoadFollower;
    m << myID << d.roadfollower;
    //SendMail(m);
    */
    // and sleep for a bit
    usleep(100000);
  }

  cout << "Finished Active State" << endl;

}


void RoadFollower::Shutdown() {

  // if we get to here a break has been detected 
  cout << "Shutting Down" << endl;

  //Insert code to shut down RoadFollower here

}



