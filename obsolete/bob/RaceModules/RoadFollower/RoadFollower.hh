#ifndef ROADFOLLOWER_HH
#define ROADFOLLOWER_HH


#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>

#include "RoadFollowerDatum.hh"
#include "MTA/Kernel.hh" // the MTA kernel since this is a MTA module

using namespace std;
using namespace boost;


class RoadFollower : public DGC_MODULE {
public:
  // and change the constructors
  RoadFollower();
  ~RoadFollower();

  // The states in the state machine.
  // Uncomment these if you wish to define these functions.

  void Init();
  void Active();
  //  void Standby();
  void Shutdown();
  //  void Restart();

  // The message handlers. 
  //  void InMailHandler(Mail & ml);
  //  Mail QueryMailHandler(Mail & ml);

  // the status function.
  //string Status();


  // Any functions which run separate threads
  
  // Method protocols
  int  InitRoadFollower();
  void RoadFollowerUpdateVotes();
  void RoadFollowerShutdown();

  void UpdateState(); // grabs from VState and stores in datum.

private:
  // and a datum named d.
  RoadFollowerDatum d;
};



// enumerate all module messages that could be received
namespace RoadFollowerMessages {
  enum {
    // we are not yet receiving messages.
    // A place holder
    NumMessages
  };
};






#endif
