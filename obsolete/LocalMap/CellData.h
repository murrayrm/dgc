#ifndef __CELL_DATA_H__
#define __CELL_DATA_H__

/* CellData.h
   Author: J.D. Salazar
   Created: 7-16-03
   
   This class contains the class that stores data in the map structure for the local obstacle avoidance mechanism 
   to be used by Team Caltech in the 2004 Darpa Grand Challenge.
11/11/2003 - Jeremy Gillula - Modified cell data to contain goodness, an integer where 0 is worst, 250 is best, and -123456 is unknown,
based on constants declared below.
*/

// For debugging
#include <iostream>

#include <algorithm>
// TODO!!: YET ANOTHER NASTY HACK SO WE DONT ALL HAVE TO CO PERCEPTOR TO COMPILE.  FIX ME OR YOUR SOUL WILL FOREVER BE TORMENTED
#include "../pertocla/per_to_cla.h"
#include "CellData.h"

// THIS REALLY COMES FROM ARBITER, SO SHOULDNT BE REDEFINING IT HERE!!!
#define MAX_GOOD_DATA 250

class CellData 
{
 public:

  /* Constructors */

  // Default constructor creates a cell containing NO_DATA for every entry in the cell.
  CellData();

  // Constructor used to set initial values.
  CellData(int elevation, int elevationConfidence,
	   int classification , int classificationConfidence,
	   int obstacleDataConfidence, int obstacleDataConfidence,
	   int roughness, int roughnessConfidence,
	   int timeLastModified);

  // Constructor using encoded perceptor data
  // For more information about the format perceptor data is in check ../pertocla/terrain_map_specs.txt and ../../perceptor/Utils/mapinterface.h
  CellData(int cellData, int timeLastModified);

  // Copy Constructor
  CellData(const CellData &other) { copy(other); }

  /* Destructor */
  ~CellData() { destroy(); }

  /* Accessors */
  int getElevation() const;
  int getElevationConfidence() const;
  int getClassification() const;
  int getClassificationConfidence() const;
  int getObstacleData() const;
  int getObstacleDataConfidence() const;
  int getRoughness() const;
  int getGoodness() const;
  int getRoughnessConfidence() const;
  int getTimeLastModified() const;

  /* Operators */
  CellData & operator= (const CellData &other);
  bool operator== (const CellData &other) const;
  bool operator!= (const CellData &other) const;

  /* Mutator */

  // Update data takes the data from other and combines it with the data in the cell.  This is intended to be used by sending newer data for a given
  // cell through other so that the cell can combine the old and new data in some way.
  void updateData(const CellData &other);


  // Expire data will expire confidences (and possibly data) based on how old a cell's data is.  Currently this is not really implemented
  // as the units perceptor stores times in for the map aren't clear.
  CellData expireData(int newTime) const;

  /* Constants */
  // Generic no data flag.  All 'get' functions return this flag if there is no data for that value.
  static const int NO_DATA = -123456;

  void setGoodness(int goodness);

 private:
  /* The data stored in the cell */
  int _cellData;
  int _goodness;
  int _timeLastModified;      // Stores the time from the last map that modified this cell.

  /* Private Accessors */
  int getCellData() const;

  /* Private Mutators */
  void setTimeLastModified(int time);
  void setCellData(int cellData);
  void setElevation(int elevationData);
  void setElevationConfidence(int elevationConfidence);
  void setClassification(int classificationData);
  void setClassificationConfidence(int classificationConfidence);
  void setObstacleData(int obstacleData);
  void setObstacleDataConfidence(int obstacleDataConfidence);
  void setRoughness(int roughness);
  void setRoughnessConfidence(int roughnessConfidence);

  // Used to encode data.  Look at either ../pertocla/terrain_map_specs.txt and ../../perceptor/Utils/mapinterface.h for more information.
  void encodeData(int n, unsigned int shift, unsigned int mask);
  int decodeData(unsigned int mask, unsigned int shift) const;
 
  // Copies other to this.
  void copy(const CellData &other);
  // Deallocates any memory and resets all values of this. 
  void destroy();
};

#endif
