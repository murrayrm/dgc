#ifndef __ERROR_CONTEXT_H__
#define __ERROR_CONTEXT_H__

/* ErrorContext.h
   Author: J.D. Salazar
   Created: 7-24-03
   
   ErrorContext is an object used to accumulate errors during tests and produce output for these tests.. */

#include <iostream>
#include <sstream>
#include <set>

#define DESC(x) desc(x, __LINE__)  // hack

class ErrorContext
{
 public:

  // Constructor write's header to stream
  ErrorContext(std::ostream &stream, std::string file);

  // Write line/descrtiption
  void desc(std::string message, int line);
  
  // Write test result
  void result(bool good); 

  // Write Summary info
  ~ErrorContext();

  // True iff all tests passed
  bool passedAllTests() const;
 private:
  // output stream to use
  std::ostream & os;
  // Name of test file
  std::string file;

  // The number of tests which were passed
  int passedTests;
  // The number of total tests
  int totalTests;
  // Line number of the most recent test.
  int lastLine;
  

  
  // Line number of failed tests.
  std::set<int> badLines;
  
  // Skip a line before title?
  bool skipLine;
};

#endif
