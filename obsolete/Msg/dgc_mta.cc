// dgc_mta.cc 


#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>

#include "dgc_const.h"
#include "dgc_socket.h"
#include "dgc_mta.hh"
#include "dgc_msg.h"

using namespace std;


/* **************************************************************
***************************************************************** */

int mta_LocalNode::addMsg(mta_msg theMessage)
{
  boost::recursive_mutex::scoped_lock scoped_lock(mutex);
  
  //  cout << "mta_LocalNode::addMsg - size=" << (* ((dgc_MsgHeader*) theMessage.memptr)  ).MsgSize << "." << endl;
  inbound.push_back(theMessage);

  return TRUE;

}


/* **************************************************************
***************************************************************** */

int mta_LocalNode::getLastTime()
{
  boost::recursive_mutex::scoped_lock scoped_lock(mutex);
  //  cout << "mta_LocalNode::getLastTime - time=" << my_time << "." << endl;
  return my_time;

}


/* **************************************************************
***************************************************************** */

int mta_LocalNode::getID()
{
  boost::recursive_mutex::scoped_lock scoped_lock(mutex);
  //  cout << "mta_LocalNode::getID - id=" << id << "." << endl;
  return id;

}

int mta_LocalNode::getType()
{
  boost::recursive_mutex::scoped_lock scoped_lock(mutex);
  //  cout << "mta_LocalNode::getType - type=" << type << "." << endl;
  return type;

}






/* **************************************************************
***************************************************************** */
int mta_LocalNode::anyMessages(int min, int max)
{
  boost::recursive_mutex::scoped_lock scoped_lock(mutex);
  // my_time = dgc_mta_time();

  //  cout << "mta_LocalNode:anyMessages - (" << min << ", " << max << ")" << endl;

  if( min == 0 && max == 0 ) {
    return ! inbound.empty();

  }else if ( max == 0 ) {
    //  InputIterator x = inbound.begin();
    //  InputIterator stop = inbound.end();
    std::list<mta_msg>::iterator x = inbound.begin();
    std::list<mta_msg>::iterator stop = inbound.end();
    dgc_MsgHeader * p_msgh;
    while( x != stop ) {
      p_msgh = (dgc_MsgHeader *) ((*x).memptr);

      if ( (*p_msgh).MsgType == min ) {
	return TRUE;
      }
      ++x;
    }
    return FALSE;
  }
  else {
    //    InputIterator x = inbound.begin();
    //    InputIterator stop = inbound.end();
    std::list<mta_msg>::iterator x = inbound.begin();
    std::list<mta_msg>::iterator stop = inbound.end();
    dgc_MsgHeader * p_msgh;
    while( x != stop ) {
      p_msgh = (dgc_MsgHeader *) ((*x).memptr);

      if ( min <= (*p_msgh).MsgType && (*p_msgh).MsgType <= max ) {
	return TRUE;
      }
      ++x;
    }
    return FALSE;
  }
}
    


/* **************************************************************
***************************************************************** */


/* **************************************************************
***************************************************************** */
int mta_LocalNode::pollMessage(dgc_MsgHeader &fillme, int min, int max)
{
  boost::recursive_mutex::scoped_lock scoped_lock(mutex);
  //my_time = dgc_mta_time();

  //  cout << "mta_LocalNode:pollMessage - (" << min << ", " << max << ")" << endl;

  // If there are no messages, then simply return false.
  if ( inbound.empty() ) {
    return FALSE;
  } else if( min == 0 && max == 0 ) {
    
    // InputIterator x = inbound.begin();
    std::list<mta_msg>::iterator x = inbound.begin();
    dgc_MsgHeader * p_msgh = (dgc_MsgHeader *) ((*x).memptr);
    fillme = (*p_msgh);
    return TRUE;    

  } else if ( max == 0 ) {
    //    InputIterator x = inbound.begin();
    //    InputIterator stop = inbound.end();
    std::list<mta_msg>::iterator x = inbound.begin();
    std::list<mta_msg>::iterator stop = inbound.end();
    dgc_MsgHeader * p_msgh;
    while( x != stop ) {
      p_msgh = (dgc_MsgHeader *) ((*x).memptr);

      if ( (*p_msgh).MsgType == min ) {
	fillme = (*p_msgh);
	return TRUE;
      }
      ++x;
    }
    return FALSE;
  } else {
    //    InputIterator x = inbound.begin();
    //    InputIterator stop = inbound.end();
    std::list<mta_msg>::iterator x = inbound.begin();
    std::list<mta_msg>::iterator stop = inbound.end();
    dgc_MsgHeader * p_msgh;
    while( x != stop ) {
      p_msgh = (dgc_MsgHeader *) ((*x).memptr);

      if ( min <= (*p_msgh).MsgType && (*p_msgh).MsgType <= max ) {
	fillme = (*p_msgh);
	return TRUE;
      }
      ++x;
    }
    return FALSE;
  }
}





/* **************************************************************
***************************************************************** */
int mta_LocalNode::getMsg(const dgc_MsgHeader & msgh, mta_msg & fillme)
{
  boost::recursive_mutex::scoped_lock scoped_lock(mutex);
  //  my_time = dgc_mta_time();

  //  cout << "mta_LocalNode::getMsg - size = " << msgh.MsgSize << "." << endl;
  
  //  InputIterator x = inbound.begin();
  //  InputIterator stop = inbound.end();
  std::list<mta_msg>::iterator x = inbound.begin();
  std::list<mta_msg>::iterator stop = inbound.end();
  dgc_MsgHeader * p_msgh;
  while( x != stop ) {
    p_msgh = (dgc_MsgHeader *) ((*x).memptr);

    if ( (*p_msgh).SourceID == msgh.SourceID && 
	 (*p_msgh).MsgID == msgh.MsgID &&
         (*p_msgh).MsgSize == msgh.MsgSize){
      fillme = (*x);
      inbound.erase(x);  
      return TRUE;
    }
  
    ++x;
  }

  return ERROR;  // if the message could not be found, return an error.  

}






/* **************************************************************
***************************************************************** */

/* **************************************************************
***************************************************************** */

/* **************************************************************
***************************************************************** */

/* class mta_Mail */

int mta_Mail::in_addNode(int nodeNum, int nodeType, int flags ) {

  // steps - 0 lock the object
  //         1 verify that node does not exist
  //         2 add the node

  boost::recursive_mutex::scoped_lock scoped_lock(inMutex);
  // cout << "mta_Mail::in_addNode - node = " << nodeNum << "." << endl;

  //  InputIterator x = inbox.begin();
  //  InputIterator stop = inbox.end();
  std::list<mta_LocalNode>::iterator x = inbox.begin();
  std::list<mta_LocalNode>::iterator stop = inbox.end();
  mta_LocalNode * p_ln;

  while( x != stop ) {
    p_ln =  & (*x);

    if ( (*p_ln).getID() == nodeNum ) {
      return ERROR;
    }
     ++x;
  }

  inbox.push_back( mta_LocalNode(nodeNum, nodeType));
  return nodeNum;
}


int mta_Mail::in_addNode(int nodeType) {
  return in_addNode( nextNode() , nodeType, 0 );
}

int mta_Mail::in_rmNodeByID(int nodeNum)
{
  boost::recursive_mutex::scoped_lock scoped_lock(inMutex);
  //  cout << "mta_Mail::in_rmNode - node=" << nodeNum << "." << endl;
  
  //  InputIterator x = inbox.begin();
  //  InputIterator stop = inbox.end();
  std::list<mta_LocalNode>::iterator x = inbox.begin();
  std::list<mta_LocalNode>::iterator stop = inbox.end();
  mta_LocalNode * p_ln;

  while( x != stop ) {
    p_ln = & (*x);

    if ( (*p_ln).getID() == nodeNum ) {
      inbox.erase(x);
      return TRUE;
    }
     ++x;
  }
  
  return ERROR;
}


int mta_Mail::purgeNodes() {
  boost::recursive_mutex::scoped_lock scoped_lock(inMutex);
  //  cout << "mta_Mail::purgeNodes" << endl; //nodeNum << "." << endl;
  
  //  InputIterator x = inbox.begin();
  //  InputIterator stop = inbox.end();
  std::list<mta_LocalNode>::iterator x = inbox.begin();
  std::list<mta_LocalNode>::iterator stop = inbox.end();
  mta_LocalNode * p_ln;

  /*  while( x != stop ) {
    p_ln = & (*x);

    if ( (dgc_mta_time() - (*p_ln).getLastTime()) > CLOCKS_TILL_ERASE_NODE ) {
      inbox.erase(x); 
    }
    else {
      ++x;
    }
  }
  */
  return TRUE;
}



int mta_Mail::in_addMessageByID(int node, mta_msg mm, int flags)
{
  boost::recursive_mutex::scoped_lock scoped_lock(inMutex);
  //  cout << "mta_Mail::addMessageByID - node=" << node << "." << endl;
  
  //  InputIterator x = inbox.begin();
  //  InputIterator stop = inbox.end();
  std::list<mta_LocalNode>::iterator x = inbox.begin();
  std::list<mta_LocalNode>::iterator stop = inbox.end();
  mta_LocalNode * p_ln;

  while( x != stop ) {
    p_ln = & (*x);

    if ( (*p_ln).getID() == node ) {
      (*p_ln).addMsg(mm);
      return TRUE;
    }
    else {
      ++x;
    }
  }
  
  return ERROR;
}



int mta_Mail::in_addMessageByType(int nodeType, mta_msg mm, int flags)
{
  boost::recursive_mutex::scoped_lock scoped_lock(inMutex);
  //  cout << "mta_Mail::addMessageByID - node type =" << nodeType << "." << endl;
  
  //  InputIterator x = inbox.begin();
  //  InputIterator stop = inbox.end();
  std::list<mta_LocalNode>::iterator x = inbox.begin();
  std::list<mta_LocalNode>::iterator stop = inbox.end();
  mta_LocalNode * p_ln;

  while( x != stop ) {
    p_ln = & (*x);

    if ( (*p_ln).getType() == nodeType ) {
      (*p_ln).addMsg(mm);
      return TRUE;
    }
    else {
      ++x;
    }
  }
  
  return ERROR;
}


int mta_Mail::in_anyMessages(int node, int min, int max) 
{
  boost::recursive_mutex::scoped_lock scoped_lock(inMutex);
  //  cout << "mta_Mail::anyMessages - node=" << node << "." << endl;
  
  //  InputIterator x = inbox.begin();
  //  InputIterator stop = inbox.end();
  std::list<mta_LocalNode>::iterator x = inbox.begin();
  std::list<mta_LocalNode>::iterator stop = inbox.end();
  mta_LocalNode * p_ln;

  while( x != stop ) {
    p_ln = & (*x);

    if ( (*p_ln).getID() == node ) {
      return (*p_ln).anyMessages(min, max);
    }
    else {
      ++x;
    }
  }
  
  return ERROR;

}


int mta_Mail::in_pollMessage(dgc_MsgHeader &fillme, int node, int min, int max)
{
  boost::recursive_mutex::scoped_lock scoped_lock(inMutex);
  //  cout << "mta_Mail::pollMessage - node=" << node << "." << endl;
  
  //  InputIterator x = inbox.begin();
  //  InputIterator stop = inbox.end();
  std::list<mta_LocalNode>::iterator x = inbox.begin();
  std::list<mta_LocalNode>::iterator stop = inbox.end();
  mta_LocalNode * p_ln;

  while( x != stop ) {
    p_ln = & (*x);

    if ( (*p_ln).getID() == node ) {
      return (*p_ln).pollMessage(fillme, min, max);
    }
    else {
      ++x;
    }
  }
  
  return ERROR;
}


int mta_Mail::in_getMsg(const dgc_MsgHeader & msgh, mta_msg & fillme)
{
  int node = msgh.DestinationID;
  boost::recursive_mutex::scoped_lock scoped_lock(inMutex);
  //  cout << "mta_Mail::getMessage - node=" << node << "." << endl;
  
  //  InputIterator x = inbox.begin();
  //  InputIterator stop = inbox.end();
  std::list<mta_LocalNode>::iterator x = inbox.begin();
  std::list<mta_LocalNode>::iterator stop = inbox.end();
  mta_LocalNode * p_ln;

  while( x != stop ) {
    p_ln = & (*x);

    if ( (*p_ln).getID() == node ) {
      return (*p_ln).getMsg(msgh, fillme);
    }
    else {
      ++x;
    }
  }
  
  return ERROR;
}




int mta_Mail::in_getLastMsg(int node, mta_msg & fillme, int min, int max)
{
  boost::recursive_mutex::scoped_lock scoped_lock(inMutex);
  //  cout << "mta_Mail::getMessage - node=" << node << "." << endl;
  
  //  InputIterator x = inbox.begin();
  //  InputIterator stop = inbox.end();
  std::list<mta_LocalNode>::iterator x = inbox.begin();
  std::list<mta_LocalNode>::iterator stop = inbox.end();
  mta_LocalNode * p_ln;
  int cont;
  dgc_MsgHeader myMH;

  while( x != stop ) {
    p_ln = & (*x);

    if ( (*p_ln).getID() == node ) {
      if( (*p_ln).pollMessage(myMH, min, max) == FALSE ) {
	return FALSE;
      } else {
	(*p_ln).getMsg(myMH, fillme);
	cont = TRUE;
	while(cont == TRUE) {
	  if ( (*p_ln).pollMessage(myMH, min, max) == TRUE ) {
	    delete fillme.memptr;
	    (*p_ln).getMsg(myMH, fillme);
	  } else {
	    cont = FALSE;
	  }
	}
	return TRUE;
      }
    }
    else {
      ++x;
    }
  }
  
  return ERROR;
}





int mta_Mail::in_nodeTypeExists(int nodeType)
{
  boost::recursive_mutex::scoped_lock scoped_lock(inMutex);
  //  cout << "mta_Mail::nodeTypeExists - node type =" << nodeType << "." << endl;
  
  //  InputIterator x = inbox.begin();
  //  InputIterator stop = inbox.end();
  std::list<mta_LocalNode>::iterator x = inbox.begin();
  std::list<mta_LocalNode>::iterator stop = inbox.end();
  mta_LocalNode * p_ln;

  while( x != stop ) {
    p_ln = & (*x);

    //    cout << "mta_Mail::nodeTypeExists: getType()= " << (*p_ln).getType() << endl;
    //    cout << "mta_Mail::nodeTypeExists: nodeType = " << nodeType <<endl;

    if ( (*p_ln).getType() == nodeType ) {
      //      cout << "mta_Mail::nodeTypeExists: Returning Found" <<endl;
      return (*p_ln).getID();
    }
    else {
      ++x;
    }
  }
  
  return NOT_FOUND;
}






int mta_Mail::out_pushMessage( mta_msg mm )
{
  boost::recursive_mutex::scoped_lock scoped_lock(outMutex);
  //  cout << "mta_Mail::out_pushMessage - source node=" 
  //     << (*   ((dgc_MsgHeader*) (mm.memptr))    ).SourceID << "." << endl;

  outbox.push_back(mm);
}


int mta_Mail::out_popMessage( mta_msg & fillme)
{
  boost::recursive_mutex::scoped_lock scoped_lock(outMutex);
  //  cout << "mta_Mail::out_popMessage - ";
  
  if ( ! outbox.empty() ) {
    fillme = (* (outbox.begin() ));
    //    cout << "source node=" << ( * ((dgc_MsgHeader*) (fillme.memptr))    ).SourceID 
    //	 << "." << endl;
    outbox.pop_front();
    return TRUE;
  }
  else {
    //cout << "outbox empty." << endl;
    return FALSE;
  }
}





int mta_Mail::nextNode() {
  boost::recursive_mutex::scoped_lock scoped_lock(inMutex);
  return nodeCtr++;
}


int mta_Mail::nextMsg() {
  boost::recursive_mutex::scoped_lock scoped_lock(inMutex);
  return msgCtr++;
}
