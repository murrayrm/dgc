function updateCells(indices, h, var)
%
% This updates a set cells corresponding to a single measurement
% This function should work in both 2D and 3D

globals

for i=1:length(h)
    
    % update the cells according to the 1D KF equations that Kristo derived
    H_EST(indices(i,2),indices(i,1)) = (H_EST(indices(i,2),indices(i,1))*var(i) + ...
        h(i)*H_EST_VAR(indices(i,2),indices(i,1))) / ...
        (var(i) + H_EST_VAR(indices(i,2),indices(i,1)));
    
    H_EST_VAR(indices(i,2),indices(i,1)) = (var(i) * H_EST_VAR(indices(i,2),indices(i,1))) / ...
        (var(i) + H_EST_VAR(indices(i,2),indices(i,1)));
end
