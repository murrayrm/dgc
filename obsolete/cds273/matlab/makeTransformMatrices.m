% create the rotation matrices. the standard for the team seems to be
% z-y-x euler angles.  (yaw-roll-pitch)
% first the sensor to vehicle(body) transform
cy = cos(SENSOR_STATE(YAWIDX));
sy = sin(SENSOR_STATE(YAWIDX));
cr = cos(SENSOR_STATE(ROLLIDX));
sr = sin(SENSOR_STATE(ROLLIDX));
cp = cos(SENSOR_STATE(PITCHIDX));
sp = sin(SENSOR_STATE(PITCHIDX));
R_S2B = [ cy*cp  cy*sp*sr - cr*sy  cy*cr*sp + sy*sr;
          cp*sy  sy*sp*sr + cy*cr  cr*sy*sp - cy*sr;
          -sp    cp*sr             cp*cr            ];
T_S2B = SENSOR_STATE(N_IDX:D_IDX);

% the vehicle to global transform, corrupted measurement
cy = cos(VEH_STATE_EST(YAWIDX));
sy = sin(VEH_STATE_EST(YAWIDX));
cr = cos(VEH_STATE_EST(ROLLIDX));
sr = sin(VEH_STATE_EST(ROLLIDX));
cp = cos(VEH_STATE_EST(PITCHIDX));
sp = sin(VEH_STATE_EST(PITCHIDX));
R_B2N = [ cy*cp  cy*sp*sr - cr*sy  cy*cr*sp + sy*sr;
          cp*sy  sy*sp*sr + cy*cr  cr*sy*sp - cy*sr;
          -sp    cp*sr             cp*cr            ];
T_B2N = VEH_STATE_EST(N_IDX:D_IDX);

% the vehicle to global transform, clean measurement
cy = cos(VEH_STATE_REAL(YAWIDX));
sy = sin(VEH_STATE_REAL(YAWIDX));
cr = cos(VEH_STATE_REAL(ROLLIDX));
sr = sin(VEH_STATE_REAL(ROLLIDX));
cp = cos(VEH_STATE_REAL(PITCHIDX));
sp = sin(VEH_STATE_REAL(PITCHIDX));
R_B2N_REAL = ...
        [ cy*cp  cy*sp*sr - cr*sy  cy*cr*sp + sy*sr;
          cp*sy  sy*sp*sr + cy*cr  cr*sy*sp - cy*sr;
          -sp      cp*sr                   cp*cr            ];
T_B2N_REAL = VEH_STATE_REAL(N_IDX:D_IDX);

% the sensor state in global coordinates may be useful
% TODO: Verify these equations
SENSOR_STATE_GLOBAL = [R_B2N * SENSOR_STATE(N_IDX:D_IDX) + T_B2N; ...
    VEH_STATE_EST(PITCHIDX)+SENSOR_STATE(PITCHIDX); 
    VEH_STATE_EST(ROLLIDX)+SENSOR_STATE(ROLLIDX); 
    VEH_STATE_EST(YAWIDX)+SENSOR_STATE(YAWIDX)];
% VEH_STATE_EST(PITCHIDX)+SENSOR_STATE(PITCHIDX); 0; 0];

SENSOR_STATE_GLOBAL_REAL = ...
    [R_B2N_REAL * SENSOR_STATE(N_IDX:D_IDX) + T_B2N_REAL;
    VEH_STATE_REAL(PITCHIDX)+SENSOR_STATE(PITCHIDX); 
    VEH_STATE_REAL(ROLLIDX)+SENSOR_STATE(ROLLIDX); 
    VEH_STATE_REAL(YAWIDX)+SENSOR_STATE(YAWIDX)];
%VEH_STATE_REAL(PITCHIDX)+SENSOR_STATE(PITCHIDX); 0; 0];    
