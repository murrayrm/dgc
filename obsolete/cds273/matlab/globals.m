global H_TRUTH
global H_EST
global H_EST_VAR
global N_IDX E_IDX D_IDX YAWIDX ROLLIDX PITCHIDX;
global EPSILON;
global LENGTH_N;
global LENGTH_E;
global NUM_CELLS_N;
global NUM_CELLS_E;
global RES_N;
global RES_E;
global CELLS_N;
global CELLS_E;
global SENSOR_STATE;
global SENSOR_STATE_GLOBAL;
global SENSOR_STATE_GLOBAL_REAL;
global VEH_STATE_REAL;
global VEH_STATE_EST;
global VEH_STATE_EST_VAR;
global R_S2B;
global T_S2B;
global R_B2N;
global T_B2N;
global R_B2N_REAL; % body to nav rotation for actual state info
global T_B2N_REAL; % body to nav translation for actual state info
global SENSOR_MAX_RANGE;
global SENSOR_RANGE_VAR; % the variance of the range measurement
global SENSOR_SCAN_ANGLE_VAR; % the variance of the angle you tell the
                         % sensor to scan at vs what is actually
                         % scans at.

% corrupted measurements
global VEH_STATE_CORR;
global SENSOR_STATE_CORR;
