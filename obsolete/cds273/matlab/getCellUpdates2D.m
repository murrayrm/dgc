function [indices, means, variances] = getCellUpdates2D( u, C )
%
% Changes:
%   Lars Cremean, 20 May 2004, Created
%   Lars Cremean, 21 May 2004, Functionalized
%
% This function takes the data of a sensor measurement uncertainty model
% (assumed to be a multivariate Gaussian here, described by it's center
% and covariance in the world coordinate frame) and returns a list of 
% height means and variances for a list of cells around the measurement
%
% Inputs:
%   u: the mean of the Gaussian, in global coordinates
%   C: the covariance of the Gaussian, 
%
% Outputs:
%   indices:   Nx2 matrix that represent the indices of the cells we want
%              to update
%   means:     means of the cells we want to update based on a measurement
%   variances: variances of the cells we want to update 
%

% In general, a multivariate Gaussian's pdf of dimension p is described by 
% f(x) = 1 / [(2*pi)^(p/2) det(C)^(1/2)] * exp[-(1/2)(x-u)' inv(C) (x-u) ]
% where u (a p-vector) is the mean of the pdf and C is the covariance 
% matrix.  The MATLAB command mvnpdf describes such a function

format short g

% Some basic data structures we will need...
% This calls a script that will set the global variables
globals

% an array of indices for the cells, and the x value of each of the cells
x = CELLS_N;


% The first value of the mean (the x value is the forward direction) will
% be associated with the center cell that we update
cindex = round(u(1)/RES_N) + 1; % TODO: Make this work for negative values!!

% We'll have to choose how many cells around the mean value to update.
% This will eventually depend on the size of the covariance relative to 
% the cell size, but for now let's just make it fixed
reach = 3; % 2*reach+1 cells will be updated per measurement

% get the indices of the cells that we want to update
indices = [max((cindex-reach), 1):min((cindex+reach), NUM_CELLS_N)];
indices = [indices' ones(length(indices),1)]; 

% i is the index of a cell
for i = 1:length(indices(:,1))

    % for each cell we need to figure out the 
    % measurement mean (m) and measurement variance (v) 
    
    % We'll assume also to know the pitch of the sensor ray.  From this we'll
    % get the mean value for the cells that we update.  
    means(i) = SENSOR_STATE_GLOBAL(ZIDX) - ...
            abs( x(indices(i,1))-SENSOR_STATE_GLOBAL(XIDX) ) * ...
            tan(SENSOR_STATE_GLOBAL(PITCHIDX)); 
        
    % mvnpdf is the multivariate normal pdf
    % for each of the cells, update the variance such that the peak value
    % equals 1/(sigma*sqrt(2*pi)), where the peak value is the 
    % check this equation... it's important!
    % Tricky!... overwrite the mean and covariance matrices to ignore the 
    % y dimension, and only work in the 2D of variables x and z
    u2D = u([1 3]);
    C2D = [C(1,1) C(1,3); C(3,1) C(3,3)];
    % check to see if we have perfect measurement knowledge
    if( isempty( find(eig(C2D) <= eps) ) )
      mvnpdfval = mvnpdf([x(indices(i,1)); means(i)],u2D,C2D) * RES_N;
      variances(i) = 1 / (sqrt(2*pi) * mvnpdfval);
    else 
      variances(i) = 0;
    end
end


% disp('getCellUpdates indices, pdf values and variances...')
% % print out the indices of affected cells
% max((cindex-reach), 1):min((cindex+reach), length(indices))
% % print out the value of the pdf at these cell locations (centers)
% mvnpdfval(max((cindex-reach), 1):min((cindex+reach), length(indices)))
% % print out the resultant measurement variance 
% variances(max((cindex-reach), 1):min((cindex+reach), length(indices)))
