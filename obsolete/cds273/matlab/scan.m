function range = scan(scan_yaw, corruptRange)
	scan_yaw;

	range = [];
	for yaw = scan_yaw
		range = [range, getRangeAwesome(yaw, corruptRange)];
	end

