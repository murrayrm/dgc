% script updateFigure3D

globals

clf

% Plot the ground truth and estimated terrain
mesh( CELLS_N, CELLS_E, H_EST )
grid off
hold on

% mesh( CELLS_N, CELLS_E, H_TRUTH, 'EdgeColor', ones(1,3)*0.9 )

% plot the uncorrupted vehicle location 
plot3( VEH_STATE_REAL(N_IDX), VEH_STATE_REAL(E_IDX), VEH_STATE_REAL(D_IDX), ...
    'g.', 'MarkerSize', 20, 'MarkerEdgeColor', 'k')

% plot the lever arm between the vehicle and sensor
plot3( [VEH_STATE_REAL(N_IDX) SENSOR_STATE_GLOBAL_REAL(N_IDX)], ...
    [VEH_STATE_REAL(E_IDX) SENSOR_STATE_GLOBAL_REAL(E_IDX)], ...
    [VEH_STATE_REAL(D_IDX) SENSOR_STATE_GLOBAL_REAL(D_IDX)], 'g' )
    
% plot the uncorrupted sensor location
plot3( SENSOR_STATE_GLOBAL_REAL(N_IDX), SENSOR_STATE_GLOBAL_REAL(E_IDX), ...
    SENSOR_STATE_GLOBAL_REAL(D_IDX), 'g.', 'MarkerSize', 20)

% plot the actual scan rays
for j=1:length(ranges)
    plot3( [SENSOR_STATE_GLOBAL_REAL(N_IDX) struc(j).ctr_clean(N_IDX)], ...
        [SENSOR_STATE_GLOBAL_REAL(E_IDX) struc(j).ctr_clean(E_IDX)], ...
        [SENSOR_STATE_GLOBAL_REAL(D_IDX) struc(j).ctr_clean(D_IDX)], 'g' )
    
    % plot the error ellipsoids from the uncorrupted measurements (% confidence)
    % disp('eig(cov)'), eig(cov)
    plot3( struc(j).ctr_clean(N_IDX), struc(j).ctr_clean(E_IDX), struc(j).ctr_clean(D_IDX), 'g+')
    % check to see if we have perfect measurement knowledge
    if( isempty( find(eig(struc(j).Cv_clean) <= 0) ) )
        error_ellipse(struc(j).Cv_clean, struc(j).ctr_clean,'conf',0.75)
    end

%     % plot the error ellipsoids from the corrupted measurements (% confidence)
%     % disp('eig(cov)'), eig(cov)
%     plot3( struc(j).ctr(N_IDX), struc(j).ctr(E_IDX), struc(j).ctr(D_IDX), 'b+')
%     % check to see if we have perfect measurement knowledge
%     if( isempty( find(eig(struc(j).Cv) <= 0) ) )
%         error_ellipse(struc(j).Cv, struc(j).ctr,'conf',0.75)
%     end
end

% % highlight the cells that we are updating this measurement
% plot( CELLS_N(indices(:,1)), H_EST(indices(:,1)), 'ms')
% plot( CELLS_N, H_TRUTH, 'rx', CELLS_N, H_EST, 'b*')
% 
% % plot the simplified height estimate (no uncertainty model)
% plot( CELLS_N, H_EST_SIMPLE, 'ko') 
% 
% if 1
%     % plot the error ellipse from actual measurement (99% confidence)
% %     disp('eig(cov2D_clean)'), eig(cov2D_clean)
%     plot( ctr2D_clean(1), ctr2D_clean(2), 'g+')
%     % check to see if we have perfect measurement knowledge
%     if( isempty( find(eig(cov2D_clean) <= 0) ) )
%         error_ellipse(cov2D_clean, ctr2D_clean,'conf',0.75,'style','g')
%     end
% end
% 
% 
% % plot the scan ray
% ax = axis;
% scan_yaw = 0;
% % the endpoint of the scan ray, in sensor and global coords.
% scan_end_s = SENSOR_MAX_RANGE * [cos(scan_yaw); sin(scan_yaw); 0];
% scan_end_n = R_S2B * scan_end_s + T_S2B;
% scan_end_n = R_B2N * scan_end_n + T_B2N;
% % also get the sensor posn in the global frame
% sensor_x_n = R_B2N * SENSOR_STATE(N_IDX:D_IDX) + T_B2N;
% plot( [SENSOR_STATE_GLOBAL(1) scan_end_n(1)], ...
%     [SENSOR_STATE_GLOBAL(3) scan_end_n(3)], 'g' )
% % make the source of the ladar scan stand out
% plot(SENSOR_STATE_GLOBAL(1), SENSOR_STATE_GLOBAL(3), 'g.', 'MarkerSize', 18)
% 
% % plot the individual cell "means" from a measurement
% plot(CELLS_N(indices(:,1)),means,'c^') 
% 
% % plot the error bars
% h1 = errorbar( CELLS_N', H_EST(:,1), max(min(H_EST_VAR(:,1),100),-100));
% set(h1,'Color',[1 0.5 1])
% set(h1(2), 'Marker', '*')
%    


axis equal 
% axis vis3d

	CameraPosition = [14.2649 21.7211 -14.246];
% 	CameraPositionMode = manual
	CameraTarget = [21.9204 11.0024 0.23517];
% 	CameraTargetMode = manual
	CameraUpVector = [0.42995 -0.601982 -0.672875];
% 	CameraUpVectorMode = manual
	CameraViewAngle = [83.1445];
% 	CameraViewAngleMode = manual

set(gca,  'CameraPositionMode', 'manual' )
set(gca,  'CameraTargetMode', 'manual' )
set(gca,  'CameraUpVectorMode', 'manual' )
set(gca,  'CameraViewAngleMode', 'auto' )
set(gca,  'CameraUpVector', CameraUpVector)
set(gca,  'CameraViewAngle', CameraViewAngle )
set(gca,  'CameraTarget', CameraTarget)
set(gca,  'CameraPosition', CameraPosition)

xlabel('N (meters)','FontSize',12)
ylabel('E (meters)','FontSize',12)
