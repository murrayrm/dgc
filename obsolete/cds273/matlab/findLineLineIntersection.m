function int = findLineLineIntersection(L11,L12,L21,L22)
%
% findLineLineIntersection(L11,L12,L21,L22)
%  
%  finds the intersection of the 2 lines, L1 and L2
%  Lij = point i on line j
%      = [xi yi]
%
%  returns the point of intersection,
%  [int_x int_y]

persistent errflag;
if isempty(errflag)
  errflag = 0;
end

dx12 = L11(1) - L12(1);
dx34 = L21(1) - L22(1);
dy12 = L11(2) - L12(2);
dy34 = L21(2) - L22(2);
det12 = det([L11; L12]);
det34 = det([L21; L22]);
den = det([dx12 dy12; dx34 dy34]);

%errmsg = strcat('findLineLineIntersection: lines are parallel.  No check',...
%	  'to see if they are they same line implemented!!!!');

if den == 0 
  %warning(errmsg);
  %disp(errmsg);
  msg = sprintf(['findLineLineIntersection: lines are parallel.\n',...
	   '  No check to see if they are the same line implemented!!!\n',...
	   '  Further error messages will be suppressed.\n']);
  if errflag == 0
    disp(msg);
  end
  errflag = 1;
  int = [];
  return
end

x_int = det([det12 dx12; det34 dx34])/den;
y_int = det([det12 dy12; det34 dy34])/den;
%plot(x_int,y_int,'r*');

int = [x_int y_int];