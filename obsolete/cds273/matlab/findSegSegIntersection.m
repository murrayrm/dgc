function int = findSegSegIntersection(L11,L12,L21,L22)
%
% findSegSegIntersection(L11,L12,L21,L22)
%  
%  finds the intersection (if it exists) of the 2 line segments, L1
%  and L2 Lij = endpoint j of segment i = [x y]
%
%  returns the point of intersection, if it exists,
%  [int_x int_y]
%
%  Does not handle considering the endpoints or not.  For now, can lie
%  anywhere on segment (including the endpoints)

%first see if they intersect at all
tmpint = findLineLineIntersection(L11,L12,L21,L22);

if isempty(tmpint)
  int = [];
  return;
end

% now verify that the intersection point lies on both segments
[xmaxl1, ixml1] = max([L11(1),L12(1)]);
[xmaxl2, ixml2] = max([L21(1),L22(1)]);
[ymaxl1, iyml1] = max([L11(2),L12(2)]);
[ymaxl2, iyml2] = max([L21(2),L22(2)]);
[xminl1, ixnl1] = min([L11(1),L12(1)]);
[xminl2, ixnl2] = min([L21(1),L22(1)]);
[yminl1, iynl1] = min([L11(2),L12(2)]);
[yminl2, iynl2] = min([L21(2),L22(2)]);
onl1 = 0;
onl2 = 0;

%  line 1
dx = L12(1) - L11(1);
if dx == 0 & tmpint(2) <= ymaxl1 & tmpint(2) >= yminl1
  %disp('a');
  onl1 = 1;
elseif dx ~= 0 & tmpint(1) <= xmaxl1 & tmpint(1) >= xminl1
  %disp('b');
  onl1 = 1;
else
  %disp('c');
  onl1 = 0;
end
%   check line 2 if necessary
if onl1 == 0
  %its not on first line, so no need to continue
  int = [];
  return;
end
%  so we do need to check line 2
dx = L21(1) - L22(1);
if dx == 0 & tmpint(2) <= ymaxl2 & tmpint(2) >= yminl2
  onl2 = 1;
elseif dx ~=0 & tmpint(1) <= xmaxl2 & tmpint(1) >= xminl2
  onl2 = 1;
else
  onl2 = 0;
end
if onl2 == 0
  %its not on line 2
  int = [];
  return;
end

%if we've made it this far, its on both segments!
int = tmpint;