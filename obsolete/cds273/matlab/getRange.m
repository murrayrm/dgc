% getRange.m
% Kristo Kriechbaum 2004-04-30
% 
% assumes that the bottom left corner of the map is 0,0.  the
% coordinate system is right-handed.
%
% Remember, all of our coordinate frames are x-straight ahead, y to
% the right, and z is down!!  So a positive z-value for an elevation
% is like a ditch.
%
% Definitely doesn't yet handle the case where the sensor ray enters
% through the "top" of a cell.

% global EPSILON used for comparison to "zero"

% These comments are more than likely out of date.

function range = getRange(scan_yaw)
globals;

%debug stuff
line_t = 2;
color = 'Black';
DEBUG = 0;

if DEBUG
  figure;
  hold on;
  grid on;
  xlabel('X');
  ylabel('Y');
end

% TODO: scan_end_s probably only needs the direction and not the length
% the endpoint of the scan ray, in sensor and global coords.
scan_end_s = SENSOR_MAX_RANGE * [cos(scan_yaw); sin(scan_yaw); 0];
scan_end_n = R_S2B * scan_end_s + T_S2B;
scan_end_n = R_B2N * scan_end_n + T_B2N;

% the vector from SENSOR_STATE_GLOBAL to scan_end_n
scanray = scan_end_n - SENSOR_STATE_GLOBAL(XIDX:ZIDX);

if DEBUG
  plot3(SENSOR_STATE_GLOBAL(1),SENSOR_STATE_GLOBAL(2),SENSOR_STATE_GLOBAL(3),'md','MarkerSize',10);
  plot(scan_end_n(1),scan_end_n(2),'bx','MarkerSize',8);
  line([SENSOR_STATE_GLOBAL(1) scan_end_n(1)],[SENSOR_STATE_GLOBAL(2) scan_end_n(2)],[SENSOR_STATE_GLOBAL(3) ...
		    scan_end_n(3)], 'LineWidth', line_t, 'Color', color);
end
% loop through each cell, and see if the line defined by the sensor
% coords and scan_angle intersect anything in the map.

mapints = [];
for i = 1:NUM_CELLS_X         % go through each cell in the x-dir
  for j = 1:NUM_CELLS_Y       % go through each cell in the y-dir
    
    % get the coords of the 4 corners of the cell
    corner_x = RES_X * [i-1; i];
    corner_y = RES_Y * [j-1; j];
    %plot(corner_x(1),corner_y(1),'kx');
    %plot(corner_x(2),corner_y(2),'kx');
    % go thru each side of the cell and check if the projection of the
    % sensor ray onto the plane intersects the cell side.
    sideints = [];
    sideints = [sideints; findSegSegIntersection(SENSOR_STATE_GLOBAL(1:2)', ...
						 scan_end_n(1:2)', ...
						 [corner_x(1) corner_y(1)], ...
						 [corner_x(2) corner_y(1)])];
    sideints = [sideints; findSegSegIntersection(SENSOR_STATE_GLOBAL(1:2)', ...
						 scan_end_n(1:2)', ...
						 [corner_x(1) corner_y(1)], ...
						 [corner_x(1) corner_y(2)])];
    sideints = [sideints; findSegSegIntersection(SENSOR_STATE_GLOBAL(1:2)', ...
						 scan_end_n(1:2)', ...
						 [corner_x(2) corner_y(2)], ...
						 [corner_x(2) corner_y(1)])];
    sideints = [sideints; findSegSegIntersection(SENSOR_STATE_GLOBAL(1:2)', ...
						 scan_end_n(1:2)', ...
						 [corner_x(2) corner_y(2)], ...
						 [corner_x(1) corner_y(2)])];
    if ~isempty(sideints)
      if DEBUG
	plot3(sideints(:,1),sideints(:,2),repmat(0,size(sideints,1),1),'rs');
      end
      % find the z values for these points and check which z values
      % will actually intersect the map
      for k = 1:size(sideints,1)
	if scanray(1) > EPSILON % really want scanray(1) ~= 0
	  slope = (sideints(k,1) - SENSOR_STATE_GLOBAL(1)) / scanray(1);
	elseif scanray(2) > EPSILON % really want scanray(2) ~= 0
	  slope = (sideints(k,2) - SENSOR_STATE_GLOBAL(2)) / scanray(2);
	else
	  disp('ERROR: Is the sensor pointed straight up/down??');
	end
	sideints(k,3) = SENSOR_STATE_GLOBAL(3) + slope * scanray(3);
	if sideints(k,3) > H_EST(i,j)
	  slope;
	  mapints = [mapints; sideints(k,:)];
	end
      end
      if DEBUG
	plot3(sideints(:,1),sideints(:,2),sideints(:,3),'r^','MarkerSize',7);
      end
    end % end if is not empty...
  end
end

% find the intersection point that is closest, and that will be our
% measurement
range = 10000;
intindex = [];
if ~isempty(mapints)
  if DEBUG
    plot3(mapints(:,1),mapints(:,2),mapints(:,3),'k*','MarkerSize',10);
  end
  for i = 1:size(mapints,1)
    tmprange = sqrt(sum((SENSOR_STATE_GLOBAL(XIDX:ZIDX) - mapints(i,:)').^2));
    if tmprange < range
      range = tmprange;
      intindex = i;
    end
  end
end

% if the ray doesnt intersect, do we return something empty, 0,
% or the max range?  Depends on the sensor, I guess.  The sick
% ladars will return max range if they don't get a return.

if range == 10000
  range = -1;
  return;
end

% corrupt the final range measurement by the noise on the range
range = range + normrnd(0,sqrt(SENSOR_RANGE_VAR));

if DEBUG
  plot3(mapints(intindex,1),mapints(intindex,2),mapints(intindex,3), ...
	'mo', 'MarkerSize', 12);
  % plot the map
  hold on;
  mesh(CELLS_X, CELLS_Y, H_TRUTH);
  xlabel('x');
  ylabel('y');
  colorbar;
end

