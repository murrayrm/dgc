globals
clear h1

%open('labels.fig')

% Indices into the state vectors
N_IDX = 1;
E_IDX = 2;
D_IDX = 3;
PITCHIDX = 4;
ROLLIDX = 5;
YAWIDX = 6;

% used for comparison to "zero"
EPSILON = 1e-12;

% map parameters
RES_N = 2; % dimension of each cell in the x-direction
RES_E = 2; % dimension of each cell in the y-direction  

NUM_CELLS_N = 14;   % number of cells in x-direction
NUM_CELLS_E = 20;    % number of cells in y-direction

LENGTH_N = RES_N * NUM_CELLS_N;
LENGTH_E = RES_E * NUM_CELLS_E;

% TODO: Add offset
CELLS_N = [0:RES_N:(LENGTH_N-RES_N)]';
CELLS_E = [0:RES_E:(LENGTH_E-RES_E)]';

% sensor and vehicle state variances
%                       [N  E  D  pitch roll yaw]
VEH_STATE_EST_VAR     = [0.0625; 0.0625; 0.0625; 0.00120; 0.00120; 0.00030];
SENSOR_RANGE_VAR      = 0.01;     % variance = sigma^2

% variance in the scan angle 
SENSOR_SCAN_ANGLE_VAR = 0.000003;

%          [N  E  D  pitch roll yaw]
VEH_STATE_0 = [0; 0; 0; 0; 0; 0];
VEH_STATE_REAL = VEH_STATE_0;

% sensor parameters in vehicle frame
SENSOR_STATE = [0; 0; -2; -0.3; 0; 0];
% .....DEBUG.....
%SENSOR_STATE = [0; 0; 0; 0; 0; 0];
SENSOR_MAX_RANGE = 50;    % sensor max range in meters

% set up a random map
offset = 0;
mapvar = 1;
H_TRUTH = zeros(NUM_CELLS_E,NUM_CELLS_N); 
%H_TRUTH = ones (NUM_CELLS_E,NUM_CELLS_N); 
%  H_TRUTH = normrnd(offset, sqrt(mapvar), NUM_CELLS_E, NUM_CELLS_N);
%H_TRUTH = abs( sin(2*pi*CELLS_N/LENGTH_N) ); % only 1d!

% CHANGE THIS, should be empty to start!!!
H_EST = zeros(NUM_CELLS_E,NUM_CELLS_N); % this is down value

% variance in our height estimate (will evolve)
H_EST_VAR = repmat(100,NUM_CELLS_E,NUM_CELLS_N); 
% compare to the naive method of just replacing the data with the new scan
H_EST_SIMPLE = ones(NUM_CELLS_E,NUM_CELLS_N)*NaN; 

%scan_yaw = (-50*pi/180):(1*pi/180):(50*pi/180); how many scan lines we have 
% in a single complete scan
  
CameraPosition = [10.4276 24.7834 -7.3705];
% 	CameraPositionMode = manual;
	CameraTarget = [16.7146 7.99917 0.50124];
% 	CameraTargetMode = manual;
	CameraUpVector = [0.141057 -0.376569 -0.915586];
% 	CameraUpVectorMode = manual;
	CameraViewAngle = [47.324];
% 	CameraViewAngleMode = manual;
set(gca,  'CameraPositionMode', 'manual' )
set(gca,  'CameraTargetMode', 'manual' )
set(gca,  'CameraUpVectorMode', 'manual' )
set(gca,  'CameraViewAngleMode', 'auto' )
set(gca,  'CameraUpVector', CameraUpVector)
set(gca,  'CameraViewAngle', CameraViewAngle )
set(gca,  'CameraTarget', CameraTarget)
set(gca,  'CameraPosition', CameraPosition)


NUM_SCANS = 1;
for i=1:NUM_SCANS
    % only work with a single scan measurement at first
    %     scan_yaw = normrnd(0,sqrt(SENSOR_SCAN_ANGLE_VAR));
    % TODO: The indices don't return properly for nonzero scan angles.
    % investigate.
    scan_yaw = normrnd([30*pi/180 45*pi/180],sqrt(SENSOR_SCAN_ANGLE_VAR));
%     scan_yaw = normrnd([30*pi/180],sqrt(SENSOR_SCAN_ANGLE_VAR));

    % make the vehicle move forward 10cm each measurement
    VEH_STATE_REAL(N_IDX) = VEH_STATE_0(N_IDX) + 0.2*i;
    % makeTransformMatrices needs to happen every time the vehicle changes state
    % TODO: there's probably no need to do it twice in this loop
   
	disp('corrupting scans...'), tic
	VEH_STATE_EST = normrnd(VEH_STATE_REAL, sqrt(VEH_STATE_EST_VAR));

    disp('Making transformation matrices...'), tic
    makeTransformMatrices; % to get corrupted and uncorrupted sensor state 
    %toc

    disp('Getting ranges...'), tic
    ranges = scan(scan_yaw, 1); % get a corrupted measurement for the range
    real_ranges = scan(scan_yaw, 0); % uncorrupted
    
    disp(['scan number = ', num2str(i)])
    % cycle through the scanpoints of a single scan 
    % as the laser sweeps from left to right
    for j=1:length(ranges)
        
        % get the PDF in global coordinates corresponding to t
        % the actual measurement
        disp(['Getting clean global PDF for j = ',num2str(j)]), tic
        % use uncorrupted data for the clean variables
        [struc(j).ctr_clean, struc(j).Cv_clean] = getGlobalPDF(real_ranges(j), scan_yaw(j), 0); 
%         disp(['ctr_clean'' = ', num2str(ctr_clean')])
        
        %toc
        
        % get the PDF in global coordinates corresponding to the 
        % corrupted measurement
        disp(['Getting corrupted global PDF for j = ',num2str(j)]), tic
        [struc(j).ctr, struc(j).Cv] = getGlobalPDF(ranges(j), scan_yaw(j), 1); % use corrupted data
        struc(j).Cv
        %toc
        
        % get a list of update values and variances for the cells around
        % the measurement
%         disp(['Getting cell updates (in 2D)...']), tic
%         [indices, means, var] = getCellUpdates2D(ctr, Cv);
        disp(['Getting cell updates (in 3D)...']), tic
        [indices, means, var] = getCellUpdates3D(struc(j).ctr, struc(j).Cv);
        
        % compare to the naive method of updating the cells, in which we
        % just take the measurement and replace a previous cell value
        % TODO: Extend to 3D case
%         cindex = round(ctr(1)/RES_N) + 1;
%         H_EST_SIMPLE(cindex) = ctr(3);
        
        %toc 
        disp('Updating the cells...'), tic
        updateCells(indices, means, var);
        %toc
            
    end % end cycling through scanpoints
    
    % call updateFigure at end of each scan
    updateFigure3D

%    xlabel(''), ylabel('')
%    set(gca,'XTickLabel', '', 'YTickLabel', '', 'ZTickLabel', '')


%    gorig = [0 8 0];
%    plot(gorig, gorig + [1 0 0])

        drawnow
    figure(1)
    %       pause
end
