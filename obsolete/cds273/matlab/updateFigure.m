% script updateFigure

globals

clf

% Plot the ground truth and estimated terrain
plot( CELLS_N, H_TRUTH, 'r', CELLS_N, H_EST, 'b')
hold on

% highlight the cells that we are updating this measurement
plot( CELLS_N(indices(:,1)), H_EST(indices(:,1)), 'ms')
plot( CELLS_N, H_TRUTH, 'rx', CELLS_N, H_EST, 'b*')

% plot the simplified height estimate (no uncertainty model)
plot( CELLS_N, H_EST_SIMPLE, 'ko') 

if 1
    % plot the error ellipse from actual measurement (99% confidence)
%     disp('eig(cov2D_clean)'), eig(cov2D_clean)
    plot( ctr2D_clean(1), ctr2D_clean(2), 'g+')
    % check to see if we have perfect measurement knowledge
    if( isempty( find(eig(cov2D_clean) <= 0) ) )
        error_ellipse(cov2D_clean, ctr2D_clean,'conf',0.75,'style','g')
    end
end

% plot the error ellipse from corrupted measurement (99% confidence)
% disp('eig(cov2D)'), eig(cov2D)
plot( ctr2D(1), ctr2D(2), 'b+')
% check to see if we have perfect measurement knowledge
if( isempty( find(eig(cov2D) <= 0) ) )
   error_ellipse(cov2D, ctr2D,'conf',0.75)
end

% plot the scan ray
ax = axis;
scan_yaw = 0;
% the endpoint of the scan ray, in sensor and global coords.
scan_end_s = SENSOR_MAX_RANGE * [cos(scan_yaw); sin(scan_yaw); 0];
scan_end_n = R_S2B * scan_end_s + T_S2B;
scan_end_n = R_B2N * scan_end_n + T_B2N;
% also get the sensor posn in the global frame
sensor_x_n = R_B2N * SENSOR_STATE(N_IDX:D_IDX) + T_B2N;
plot( [SENSOR_STATE_GLOBAL(1) scan_end_n(1)], ...
    [SENSOR_STATE_GLOBAL(3) scan_end_n(3)], 'g' )
% make the source of the ladar scan stand out
plot(SENSOR_STATE_GLOBAL(1), SENSOR_STATE_GLOBAL(3), 'g.', 'MarkerSize', 18)

% plot the individual cell "means" from a measurement
plot(CELLS_N(indices(:,1)),means,'c^') 

% plot the error bars
h1 = errorbar( CELLS_N', H_EST(:,1), max(min(H_EST_VAR(:,1),100),-100));
set(h1,'Color',[1 0.5 1])
set(h1(2), 'Marker', '*')
   
% set the axis to something reasonable
% axis([ax(1:2) min(ax(3), -1) max(ax(4), -1)])
axis([ax(1:2) -5 5])
axis equal 

view(0,-90)

% optional
% axis equal
% plot([0 15.655], [-2 -2],'g-.')

xlabel('N (meters)','FontSize',16)
ylabel('Z (meters)','FontSize',16)