function [indices_out, means_out, variances_out] = getCellUpdates3D( u, C )
%
% Changes:
%   Lars Cremean, 30 June 2004, Created, based on getCellUpdates2D
%
% This function takes the data of a sensor measurement uncertainty model
% (assumed to be a multivariate Gaussian here, described by it's center
% and covariance in the world coordinate frame) and returns a list of 
% height means and variances for a list of cells around the measurement
%
% Inputs:
%   u: the mean of the Gaussian, in global coordinates
%   C: the covariance of the Gaussian, 
%
% Outputs:
%   indices:   Nx2 matrix that represent the indices of the cells we want
%              to update (on the current map, with the origin being the 
%              southwest-most cell, rows represent northing and columns
%              represent easting
%   means:     means of the cells we want to update based on a measurement
%   variances: variances of the cells we want to update 
%

% In general, a multivariate Gaussian's pdf of dimension p is described by 
% f(x) = 1 / [(2*pi)^(p/2) det(C)^(1/2)] * exp[-(1/2)(x-u)' inv(C) (x-u) ]
% where u (a p-vector) is the mean of the pdf and C is the covariance 
% matrix.  The MATLAB command mvnpdf describes such a function

format short g

% Some basic data structures we will need...
% This calls a script that will set the global variables
globals

% CELLS_N is the array of Northing cell values
% CELLS_E is the array of Easting cell values

% The mean will
% be associated with the center cell that we update
indexN = round(u(1)/RES_N) + 1; % TODO: Make this work for negative values!!
indexE = round(u(2)/RES_E) + 1; % TODO: Make this work for negative values!!

% We'll have to choose how many cells around the mean value to update.
% This will eventually depend on the size of the covariance relative to 
% the cell size, but for now let's just make it fixed
reachN = 2; % 2*reachN+1 cells in Northing direction 
reachE = 2; % 2*reachE+1 cells in Easting direction

% get the indices of the cells that we want to update
indicesN = [( max((indexN-reachN), 1):min((indexN+reachN), NUM_CELLS_N) )'];
indicesE = [( max((indexE-reachE), 1):min((indexE+reachE), NUM_CELLS_E) )'];
% get an Nx2 array of [N,E] indices
indices = [ ];
% indices_out is a stripped down list of cells to update
indices_out = [ ];
means_out = [ ];
variances_out = [ ];
for i = 1:length(indicesE)
    indices = [indices; indicesN ones(length(indicesN),1)*indicesE(i)];
end

% the inverse of the covariance matrix will be useful below
Cinv = inv(C);

% go through each of the cells we have chosen to update and determine the 
% update mean and variance for each for this measurement
for i = 1:length(indices) 
    
    % for each cell we need to figure out the 
    % measurement mean (m) and measurement variance (v) 
    
    % we'll get the mean by finding the intersection with the plane that
    % represents the expected z value for a given (x,y) pair
    means(i) = -1/(2*Cinv(3,3)) * ...
        ( ( CELLS_N(indices(i,1))-u(N_IDX) )*( Cinv(1,3)+Cinv(3,1) ) + ...
          ( CELLS_E(indices(i,2))-u(E_IDX) )*( Cinv(2,3)+Cinv(3,2) ) ); 
    
    % mvnpdf is the multivariate normal pdf
    % for each of the cells, update the variance such that the peak value
    % equals 1/(sigma*sqrt(2*pi)), where the peak value is the mean given
    % above
    % check to see if we have perfect measurement knowledge
    if( isempty( find(eig(C) <= eps) ) )
      mvnpdfval = mvnpdf([CELLS_N(indices(i,1)); CELLS_E(indices(i,2)); means(i)],u,C);
      % only add this cell to the list to be updated if there is a
      % significant probability that the measurement came from that cell
      if( mvnpdfval > eps )
          indices_out = [indices_out; indices(i,:)];
          means_out = [means_out; means(i)];
          variances_out = [variances_out; 1 / (2 * pi * (mvnpdfval)^2)];
      end
    else 
      warning('One of the eigenvalues of the covariance matrix is zero!');
    end
end


% disp('getCellUpdates indices, pdf values and variances...')
% % print out the indices of affected cells
% max((indexN-reachN), 1):min((indexN+reachN), length(indices))
% % print out the value of the pdf at these cell locations (centers)
% mvnpdfval(max((indexN-reachN), 1):min((indexN+reachN), length(indices)))
% % print out the resultant measurement variance 
% variances(max((indexN-reachN), 1):min((indexN+reachN), length(indices)))
