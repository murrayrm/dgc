globals;

% Indices into the state vectors
XIDX = 1;
YIDX = 2;
ZIDX = 3;
YAWIDX = 4;
ROLLIDX = 5;
PITCHIDX = 6;

% used for comparison to "zero"
EPSILON = 1e-12;

% map parameters
LENGTH_X = 80;  % x-dimension in meters
LENGTH_Y = 80;  % y-dimension in meters
NUM_CELLS_X = 20;      % number of cells in x-direction
NUM_CELLS_Y = 20;      % number of cells in y-direction
RES_X = LENGTH_X / NUM_CELLS_X;  % cell size in x-dir (meters)
RES_Y = LENGTH_Y / NUM_CELLS_Y; % cell size in y-dir (meters)

% used only for plotting
CELLS_X = RES_X/2:RES_X:LENGTH_X;
CELLS_Y = RES_Y/2:RES_Y:LENGTH_Y;

% [x y z yaw roll pitch]
VEH_STATE = [10; 10; 0; 0; 0; 0];
VEH_STATE_EST = VEH_STATE;
VEH_STATE_EST_VAR = VEH_STATE' * VEH_STATE * 0; % make sure dimensions are correct
			
% sensor parameters in vehicle frame
SENSOR_STATE = [0; 0; -2; 0; 0; -0.3];
SENSOR_MAX_RANGE = 50;             % sensor max range in meters			
SENSOR_RANGE_VAR = .02;            % meters
SENSOR_ANGLE_VAR = deg2rad(1)^2;   % radians
% variance = sigma^2 = stdev^2

% set up a random map
offset = 0;
mapvar = 0;
H_TRUTH = normrnd(offset, mapvar, NUM_CELLS_X, NUM_CELLS_Y);
H_EST = H_TRUTH; % CHANGE THIS, should be empty to start!!!
H_EST_VAR = repmat(0,NUM_CELLS_X,NUM_CELLS_Y);

makeTransformMatrices;

range=getRange(0);
disp(strcat('Measurement range is: ', num2str(range)));
