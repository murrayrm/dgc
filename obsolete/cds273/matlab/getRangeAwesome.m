function range = getRangeAwesome(scan_yaw, corruptRange)
	globals;

% make cells_xx and cells_yy. [cells_xx; cells_yy] is a 2x(N*M) array
% of all (x,y) pairs in the grid. cells_xx_idx and cells_yy_idx are the
% same arrays only with point indices instead of coordinates
	[xx,yy] = meshgrid(CELLS_N, CELLS_E);
	cells_xx = reshape(xx,1,NUM_CELLS_N*NUM_CELLS_E);
	cells_yy = reshape(yy,1,NUM_CELLS_N*NUM_CELLS_E);

	[xx,yy] = meshgrid(1:NUM_CELLS_N, 1:NUM_CELLS_E);
	cells_xx_idx = reshape(xx,1,NUM_CELLS_N*NUM_CELLS_E);
	cells_yy_idx = reshape(yy,1,NUM_CELLS_N*NUM_CELLS_E);

% the endpoint of the scan ray, in sensor and global coords.
% TODO: Make this work in 3D!
scan_end_s = SENSOR_MAX_RANGE * [cos(scan_yaw); sin(scan_yaw); 0];
scan_end_n = R_S2B * scan_end_s + T_S2B;

% Yes! We want to get the actual range that the vehicle will measure
scan_end_n = R_B2N_REAL * scan_end_n + T_B2N_REAL;

% the vector from SENSOR_STATE_GLOBAL to scan_end_n
scanray = scan_end_n - SENSOR_STATE_GLOBAL_REAL(N_IDX:D_IDX);    

% the total yaw
totalyaw = scan_yaw + SENSOR_STATE(YAWIDX) + VEH_STATE_REAL(YAWIDX);

% coords is a matrix of all (x,y) pairs in the grid, using the sensor
% as the origin. coords_idx is the same matrix, but with indices into
% CELLS_N and CELLS_E instead of actual values;
coords = [cells_xx; cells_yy] - ...
    SENSOR_STATE_GLOBAL_REAL(N_IDX:E_IDX)*ones(1,NUM_CELLS_N*NUM_CELLS_E);
coords_idx = [cells_xx_idx; cells_yy_idx];
    
% now we determine which grid points are close to the sensing ray
% (in the ground plane)
% ray2d is the projection of the scanning ray to the ground plane
% and nsq_ray2d is  ray2d . ray2d
	ray2d = scanray(1:2);
	nsq_ray2d = norm(ray2d)^2;

% dist is the distance of each grid point along the scanning vector
% (in ray2d units)
% proj is the projection of the grid points onto the scanning ray
	dist = ray2d' * coords / nsq_ray2d;
	proj = ray2d * dist;

% err is how close each grid point comes to the scanning ray.
% insight_idx are the indices into the coord array for those grid points
% that are sufficiently close to the scanning ray (in the ground plane)
% TODO: this assumes RES_N = RES_E,pitch=0, totalyaw is in first quadrant
	err = proj - coords;
	insight_idx = find(sqrt(err(1,:).^2 + err(2,:).^2) < ...
										 (sin(totalyaw)+cos(totalyaw)) * RES_N / 2);

% sorted_idx is the index into the insight_idx array to sort the
% grid cells into the order that the scanning ray encounters them
	[dummy,sorted_idx] = sort(dist(insight_idx));

% make sorted_idx index the coords array instead
	sorted_idx = insight_idx(sorted_idx);

% gndalongscan is a vector of the height of the ground along the 
% scanning direction.
% zalongscan is a vector of the height of the sensor beam above the
% ground. This is computed by finding the height of the beam at the
% point on the beam closest to the grid center.
gndalongscan = H_TRUTH((coords_idx(2,sorted_idx)-1) * NUM_CELLS_E + coords_idx(1,sorted_idx))';
zalongscan = SENSOR_STATE_GLOBAL_REAL(D_IDX) + scanray(3) * dist(sorted_idx)';

% scannedgnd_idx is a boolean vector signifying whether the beam has
% penetrated the ground. The first element of this vector is the index
% of the first "ground sight" by the sensor
scannedgnd_idx = find(gndalongscan < zalongscan);

range = norm(scanray)*dist(sorted_idx(scannedgnd_idx(1)));

% corrupt the final range measurement by the noise on the range
if corruptRange	
    range = normrnd(range ,sqrt(SENSOR_RANGE_VAR));
end

if(range > SENSOR_MAX_RANGE)
    range = -1;
end
