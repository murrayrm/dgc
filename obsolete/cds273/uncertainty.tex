\subsection{Modeling the Sensor}
The range measurement and its associated uncertainty needs to be transformed
from the vehicle frame to the global frame.

As mentioned earlier, the LADAR is mounted on top of the vehicle and scans for
objects in planes which are at an angle $\phi$ to the horizontal.  For the
current configuration, $\phi$ is fixed ($\approx$ 6 degrees). The coordinates
of the measured data point in the vehicle frame are:

\[ \left(\begin{array}{ccc}x_v\\y_v\\z_v \end{array} \right) = 
\left(\begin{array}{ccc}{\rho}\cos{\theta}\cos{\phi}\\-{\rho}\sin{\theta}\\{\rho}\cos{\theta}\sin{\phi}\end{array} \right) +
\left(\begin{array}{ccc}x_L\\y_L\\z_L \end{array} \right)\]

\begin{figure}
\centering
\includegraphics[width=6in]{geo}
\caption{\textbf{a.} Typical configuration of the LADAR. \textbf{b.} Geometry in the scan plane.}
\end{figure}

Transforming the coordinates from the vehicle frame into the global(UTM) frame:

\[ \left(\begin{array}{ccc}x_g\\y_g\\z_g \end{array} \right) = 
R(p,h,r) \left(\begin{array}{ccc}x_v\\y_v\\z_v \end{array} \right) +
\left(\begin{array}{ccc}x\\y\\z \end{array} \right)\]

$R(p,h,r)$ is the rotation matrix which rotates coordinates from the vehicle
frame to the global frame.  It is given by:

\[R = \left(\begin{array}{ccc}\cos p\cos h&-\cos r\sin h+\cos h\sin p\sin r&\cos h\cos r\sin p + \sin h\sin r \\
\cos p\sin h &\cos h\cos r+\sin h\sin p\sin r&\cos r\sin h\sin p-\cos h\sin r \\
-\sin p&\cos p\sin r&\cos p\cos r \end{array} \right)\]

The source of uncertainty is the measurement noise as well as the noise in the
estimates that come from the GPS and the IMU.  In the noise model, both these
need to be taken into account.  A way of doing this is shown in the next two
subsections.  The methodology is based on the small angle approximation and
follows the exposition in \cite{kr}.

\subsubsection{Sensor Noise}
Assume ${\epsilon}_{\rho}$ and ${\epsilon}_{\theta}$ are independent random
variables representing the uncertainty in $\rho$ and $\theta$ respectively, i.e.

\[\rho = {\rho}^0 + {\epsilon}_{\rho}\]
\[\theta = {\theta}^0 + {\epsilon}_{\theta}\]

They are zero mean Gaussian with predetermined variances. Then:

\[x_v = ({\rho}^0 + {\epsilon}_{\rho})\cos({\theta}^0 + {\epsilon}_{\theta})\cos{\phi}\]
\[y_v = -({\rho}^0 + {\epsilon}_{\rho})\sin({\theta}^0 + {\epsilon}_{\theta})\]
\[z_v = ({\rho}^0 + {\epsilon}_{\rho})\cos({\theta}^0 + {\epsilon}_{\theta})\sin{\phi}\]

Now, assume that the variance in ${\epsilon}_{\theta}$ is very small and after
expanding the $\sin$'s and $\cos$'s, apply the small angle approximation.

\[x_v \approx {{\rho}^0}\cos{{\theta}^0}\cos{\phi} + {\epsilon}_{\rho}\cos{{\theta}^0}\cos{\phi} - {\epsilon}_{\theta}{\rho}^0\sin{\theta}^0\cos{\phi} - {\epsilon}_{\rho}{\epsilon}_{\theta}\sin{{\theta}^0}\cos{\phi}\]
\[y_v \approx -{\rho}^{0}\sin{\theta}^{0} - \epsilon_{\rho}\sin{\theta}^0 - {\epsilon}_{\theta}{\rho}^0\cos{\theta}^0 - {\epsilon_{\rho}}{\epsilon}_{\theta}\cos{\theta}^0\]

\[z_v \approx {{\rho}^0}\cos{{\theta}^0}\sin{\phi} + {\epsilon}_{\rho}\cos{{\theta}^0}\sin{\phi} -{\epsilon}_{\theta}{\rho}^0\sin{\theta}^0\sin{\phi} - {\epsilon}_{\rho}{\epsilon}_{\theta}\sin{{\theta}^0}\sin{\phi}\]

The expressions for the randomness in $(x_v,y_v,z_v)$ can be found using these
equations.  One can neglect the second order terms to make the transformations
between the random variables linear.  Doing this, we get:

\[ \left(\begin{array}{ccc}x_v\\y_v\\z_v \end{array} \right) {\approx}
\left(\begin{array}{ccc}{\rho}^0\cos{\theta}^0\sin{\phi}\\-{\rho}^0\sin{\theta}^0\\{\rho}^0\cos{\theta}^0\cos{\phi}\end{array} \right) +
\left(\begin{array}{ccc}\cos{{\theta}^0}\cos{\phi}&-{\rho}^0\sin{\theta}^0\cos{\phi}\\-\sin{\theta}^0&-{\rho}^0\cos{\theta}^0\\ \cos{{\theta}^0}\sin{\phi}& -{\rho}^0\sin{\theta}^0\sin{\phi} \end{array} \right)
\left(\begin{array}{cc}{\epsilon}_{\rho}\\{\epsilon}_{\theta}\end{array} \right)\]

\subsubsection{Estimation Noise}
The coordinate transformation from the vehicle frame to the global frame adds
to the uncertainty derived in the previous section.  Specifically, it involves a
translation and a rotation.

\[ \left(\begin{array}{ccc}x_g\\y_g\\z_g \end{array} \right) = 
R(p,h,r) \left(\begin{array}{ccc}x_v\\y_v\\z_v \end{array} \right) +
\left(\begin{array}{ccc}x\\y\\z \end{array} \right)\]

The translation term comes from the GPS.  The error in the GPS estimates can be
included in the uncertainty model as follows:

\[ \left(\begin{array}{ccc}x\\y\\z \end{array} \right) =
\left(\begin{array}{ccc}x^0\\y^0\\z^0 \end{array} \right) +
\left(\begin{array}{ccc}{\epsilon}_x\\{\epsilon}_y\\{\epsilon}_z \end{array} \right)\]

The rotation term $R(p,h,r)$ comes from the IMU.  A way to include the
uncertainty from the pitch, yaw and roll estimates is to apply the small angle
approximation and then to neglect all terms higher than first order, as
before. The consideration of noise in the pitch, yaw and roll gives the usual
expression:

\[ \left(\begin{array}{ccc}p\\h\\r \end{array} \right) =
\left(\begin{array}{ccc}p^0\\h^0\\r^0 \end{array} \right) +
\left(\begin{array}{ccc}{\epsilon}_p\\{\epsilon}_h\\{\epsilon}_r \end{array} \right)\]

and then the $R$ matrix becomes(to first order):

\[R = R^0 + A({\epsilon}_p,{\epsilon}_h,{\epsilon}_r)\]

Here A is a 3$\times$3 matrix.  Hence, it is possible to approximate the
uncertainty in each of the global coordinates as a linear combination of the
uncertainties due to the estimates from the GPS and the IMU.
