\section{Cell Update Rule} \label{sec:cellupdate}

We use Kalman Filter equations to formulate a simple update rule for an
individual cell.  Once a single measurement is broken up into multiple cells,
this update rule can be applied per cell to come up with a new estimate of the
elevation and variance of the elevation of that cell.

The (general) Kalman Filter equations are:
\begin{eqnarray}
  \text{propagation:} & & \\
  x_{k+1/k} & = & F_k x_{k/k} + G_k w_k\\
  P_{k+1/k} & = & F_k P_{k/k}F_k^T + G_k Q_k G_k^T\\
  \text{update:} & & \\
  \hat{z}_{k+1} & = & H_{k+1} x_{k+1/k}\\
  z_{k+1} & = & H_{k+1} x_{k+1/k} + \epsilon_{z_k} \\
  S_{k+1} & = & H_{k+1} P_{k+1/k} H_{k+1}^T + R_{k+1} \\
  K_{k+1} & = & P_{k+1/k} H_{k+1}^T S_{k+1}^{-1} \\
  P_{k+1/k+1} & = & P_{k+1/k} - P_{k+1/k}H_{k+1}^T S_{k+1}^{-1} H_{k+1} P_{k+1/k} \\
  x_{k+1/k+1} & = & x_{k+1/k} + K_{k+1} (z - \hat{z})
\end{eqnarray}

Where $x$ is the estimate of the state vector, and $P$ is the
covariance of that estimate.  $w_k$ and $ \epsilon_{z_k}$ are the
noise in the system dynamics and measurement respectively.  $Q_k =
E[w_k w_k^T]$ and $R_{k+1} = E[\epsilon_{z_k} \epsilon_{z_k}^T]$ The
notation $x_{k+1/k}$ means the state estimate at time $k+1$, using
measurements only up to time $k$, while $x_{k+1/k+1}$ means the state
estimate at time $k+1$ using measurements up to time $k+1$.  The
propagation equations take the current state estimate and use the
noisy system dynamics to propagate the system forward in time.  The
update equations use a noisy measurement of the system state to refine
the state estimate.  For example at time $k+1$, a mobile robot will
take a step forward ending up at $x_{k+1/k}$ with covariance
$P_{k+1/k}$.  It then takes a measurement $z_{k+1}$ of its state and
uses this measurement to better its estimate to $x_{k+1/k+1}$ with
covariance $P_{k+1/k+1}$.

In our particular case, each cell is considered on its own, so the
state x is simply the scalar elevation of the cell.  We assume the
environment is static, so this means that $F_k = 1$, and that $G_k =
0$.  In other words a cells elevation estimate and variance do not
change over time when we are not receiving measurements that lie in
that cell.  This simplifies the propagation equations to 
\begin{eqnarray}
  x_{k+1/k} & = & x_{k/k}\\
  P_{k+1/k} & = & P_{k/k}
\end{eqnarray}

After a measurement is broken up into single cells (see Section
\ref{sec:discretization}), we have a measured elevation $z_m$ and an associated
variance $\sigma_z$ of that measurement for each cell.  Since this
measurement is the elevation itself (and not some linear function of
the elevation), $H_{k+1} = 1$.

The update equations become (dropping the $k+1$ and $k$ subscripts for
now)
\begin{eqnarray}
  z & = & H x + \epsilon_{z_k}\\
  & = & z_m + \epsilon_{z_k}\\
  S & = & H P H^T + R\\
  & = & \sigma_h^2 + \sigma_z^2\\
  K & = & P H^T S^{-1}\\
  & = & \frac{\sigma_h^2}{\sigma_h^2 + \sigma_z^2}\\
  P & = & P - P H^T S^{-1} H P \\
  & = & \sigma_h^2 - \frac{\sigma_h^4}{\sigma_h^2 + \sigma_z^2}\\
  & = & \frac{\sigma_h^4 + \sigma_h^2 \sigma_z^2}{\sigma_h^2 + \sigma_z^2} 
  - \frac{\sigma_h^4}{\sigma_h^2 + \sigma_z^2}\\
  & = & \frac{\sigma_h^2 \sigma_z^2}{\sigma_h^2 + \sigma_z^2}\\
  x & = & x + K (z - \hat{z})\\
  & = & x + \frac{\sigma_h^2}{\sigma_h^2 + \sigma_z^2} (z_m - x)\\
  & = & \frac{\sigma_h^2 x + \sigma_z^2 x}{\sigma_h^2 + \sigma_z^2} 
  + \frac{\sigma_h^2 z_m - \sigma_h^2 x}{\sigma_h^2 + \sigma_z^2}\\
  & = & \frac{\sigma_h^2 z_m + \sigma_z^2 x}{\sigma_h^2 + \sigma_z^2}
\end{eqnarray}

note: our estimate of what the measurement would be, $\hat{z}$, is
simply our state in this case.

This update rule has some simple intuitive explanations as well.  If
either $\sigma_h$ or $\sigma_z$ is zero, then that means that either
our current estimate is perfect or the measurement is perfect.  In
either of these cases, the updated variance of that cell is zero.  In
terms of the updated elevation, if our measurement variance is zero,
then we will rely entirely on the measurement to update the cell.  If
on the other hand the current estimate variance is zero, we will
completely disregard the new measurement and our estimate will stay as
it is.

