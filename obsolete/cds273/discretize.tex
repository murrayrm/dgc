\section{Discretization of Measurement Uncertainty} \label{sec:discretization}

The uncertainty model resulting from the analysis of Section \ref{sec:model} will,
in general, extend over multiple cells in a gridded map for a single measurement.  
It is therefore necessary to devise an appropriate way to determine which cells 
should be updated, and how they should be updated, for each measurement.

Section \ref{sec:cellupdate} describes the method we use to update a single cell
given normally distributed cell input measurements $z_k$ that are described by
their mean $z_m$ and variance $\sigma_z$.  This section describes the way in
which a single 3-D measurement and associated uncertainty model is converted
into a number of cell input measurements.

Simplifying assumptions were made about the equations presented in Section 
\ref{sec:model} so that the uncertainty of a single measurement can be represented 
by first order statistics as a multivariate Gaussian.  The shape and orientation of 
the Gaussian for each measurement depends on the direction of the sensor 
measurement as well as the variance in sensor range measurement and variances
in vehicle position and orientation.  The measurement is parameterized by a center
$\mu \in \mathbb{R}^3$ and a covariance $C \in \mathbb{R}^{3\times 3}$, and is
represented by the equation 

\begin{equation}
p(\mathbf x) = 
\frac{1}{(2 \pi)^{p/2} \sqrt{\det C}} \exp\left[-\frac{1}{2}(\mathbf x-\mu)^T C^{-1} (\mathbf x-\mu)\right]
\end{equation}
where $p(\mathbf x) = p(x, y, z)$ is the probability that the measurement actually came from 
a surface at $\mathbf x$.  

With an elevation grid representation of the environment, each cell $C_{ij}$ in
the grid can be reasonably assigned the height probability function

\begin{equation} \label{eqn:slice}
%p_{ij}(z) = \int_{x_i-\Delta_x/2}^{x_i+\Delta_x/2} \int_{y_j-\Delta_y/2}^{y_j+\Delta_y/2}~p(x,y,z)~dy~dx.
p_{ij}(z) = \int \!\! \int_{C_{ij}}~p(x,y,z)~dy~dx.
\end{equation}

This function cannot be considered a probability density function, however,
because the total integral of $p_{ij}(z)$ is not equal to 1.  A normalization of
this function is necessary to use it as a pdf for input to the Kalman Filter
update equations of Section \ref{sec:cellupdate}.  One option for normalization
would be a scaling of the function by $\alpha_{ij}$ so that

\begin{equation}
\alpha_{ij} \int_{-\infty}^{\infty} p_{ij}(z)~dz = 1.
\end{equation}

The implementation of this method, however, has the significant drawback that
the cells far from the center of a measurement will have variances that are
similar to those of the cells near the center of the measurement.  For cells far
away from the measurement, the fact that $p_{ij}(z)$ is much smaller should
correspond to a high variance associated with the measurement.  Our solution,
then, is to find the mean $\mu_{ij}$ of $p_{ij}(z)$ and then to normalize
$p_{ij}(z)$ by setting the standard deviation of the normalized Gaussian pdf to

\begin{equation}
\sigma_{ij} = \frac{1}{p_{ij}(\mu)\sqrt{2\pi}}.
\end{equation}

Cells close to the center of the measurement, therefore, will have higher value
of $p_{ij}(\mu)$ and a lower variance.  Cells far from the center of the
measurement will have lower $p_{ij}(\mu)$ and hence a higher variance.  The
factor of $\sqrt{2\pi}$ enters because the value of a normal distribution at its
mean, $\mathcal{N}(\mu) = 1/(\sigma \sqrt{2\pi})$.

In a practical implementation, it is necessary to decide which cells are to be
updated for any given measurement. There are options for how to specify this.
One method for doing so for a Gaussian $p(\mathbf x)$ is to update any cell
whose center lies within a $\chi$-confidence ellipse. Another is to update those
cells for which the peak probability is greater than some threshold.  A third is
to say that we will update the cells whose centers lie within a specified
geometric shape (rectangle, circle, ellipse). In the implementation described in
Section \ref{sec:results}, this last method was used for the sake of ease of
implementation. Also for practical considerations, the integral in equation
\ref{eqn:slice} was approximated by

\begin{equation}
p_{ij}(z) \approx p(x_i, y_j, z) \Delta_x \Delta_y
\end{equation}

where the center of cell $C_{ij}$ is $(x_i, y_j)$ and the resolution of the grid
is specified by $\Delta_x$ and $\Delta_y$.

