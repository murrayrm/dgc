\section{Simulation Results} \label{sec:results} 

The methods presented in the last few sections have been implemented in MATLAB simulation.
The input data of the simulation are the actual terrain profile (a.k.a. {\em ground truth}), the
actual path of the vehicle (and therefore of the sensor, since we assume to know the 
transformation between sensor and vehicle coordinates), the measurement rate of the 
sensor, and the raw variances in the state elements of the vehicle and the sensor measurement.

In the simulation, the following steps are taken to implement our terrain-building methodology.
First, the known vehicle state is corrupted with noise according to the specified variances in these variables and a "corrupted" sensor orientation is generated.  Along this sensor direction, the 
actual range to the terrain profile is generated, and a noise corrupted value for this range is 
returned.

Second, a probability function is generated around the corrupted sensor measurement 
according to the sensor model, which we have assumed for now to be a multivariate 
Gaussian derived by the methods of Section \ref{sec:model}.  This probability function is 
discretized according to the method presented in Section \ref{sec:discretization}, and the 
results of this discretization are provided as inputs to the Kalman Filter-based cell update
equations of Section \ref{sec:cellupdate}.

Figure \ref{fig:stillscans} shows two snapshots of the simulation in mid-run.  The simulation
is collapsed to two dimensions $x$ (to the right) and $z$ (down) for clarity.  The figure on the
left is taken after a single scan and the one on the right after twelve scans.  Each figure 
shows the actual and estimated terrain, the corrupted sensor measurement and the 
uncertainty model of the measurement represented by an ellipse.  An initial terrain profile
estimate is flat at zero elevation, but is given a high variance.

After one scan, the 
height variance (represented by vertical error bars) is reduced the most for the cell 
associated with the center of the sensor measurement.  The three adjacent cells on each 
side of this center cell are also updated, but with larger variances.   After several iterations,
variances are reduced further with each measurement and the estimated terrain profile
approaches the expected value assuming that our model best represents the 
uncertainty of the sensor.

\begin{figure}[!t]
\begin{center}
\includegraphics[width=0.45\textwidth]{onescan.eps}
\includegraphics[width=0.45\textwidth]{twelvescans.eps}
\caption{Simulation results for a single (left) and twelve (right) scan measurements.  Even though the measurement only lies within one cell, we update adjacent cells according to a sensor uncertainty 
model.  The vehicle pitch variance is $0.02~rad$ and the sensor range variance is $0.1~m$.  The initial height estimate variance is set to $100~m$.  All other variances are set to zero.} 
\label{fig:stillscans}
\end{center}
\end{figure}

\begin{remark}
Please note that it may be possible that there may be bugs in implementation of either the 
generation and representation of the covariance matrix that were not yet identified as of the
time of this writing.  In particular, one might intuit that the principal axes of the ellipse 
generated only from variances in scan range and vehicle pitch should
be along and perpendicular to the scan direction.  Resolving these issues falls under the scope
of ongoing and future work.
\end{remark}

\begin{remark}
The implementation of the measurement uncertainty discretization assumed that the largest
principal axis of the uncertainty model was aligned with the scan line.  This assumption is 
clearly not universally satisfied, therefore improved results may be possible by following more 
closely the discretization method presented in Section \ref{sec:discretization}. 
\end{remark}

Figure \ref{fig:movingsensor} shows the result of a run with $40$ measurements, where the 
sensor moves to the right by $0.4~m$ between each measurement.  The elevation before 
$x=7m$ is never updated because no measurement's region of influence extends that 
far.  

\begin{figure}[!t]
\begin{center}
\includegraphics[width=0.7\textwidth]{movingsensor.eps}
\caption{Simulation results for 40 scan measurements where the sensor moves over the 
terrain.  Circles represent the most recent sensor reading which fell within that particular 
cell.  The same simulation parameters were used as above.}
\label{fig:movingsensor}
\end{center}
\end{figure}
