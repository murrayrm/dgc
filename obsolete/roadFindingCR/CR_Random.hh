//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2003, 2004, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// CR_Random
//----------------------------------------------------------------------------

#ifndef CR_RANDOM_DECS

//----------------------------------------------------------------------------

#define CR_RANDOM_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "CR_Linux_Time.hh"
#include "CR_Error.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE          1
#endif

#ifndef FALSE
#define FALSE         0
#endif

#define SQUARE(x)     ((x) * (x))

//-------------------------------------------------
// functions
//-------------------------------------------------

double gaussian_CR_Random(double, double, double);
double CR_round(double);
void initialize_CR_Random();
double normal_sample_CR_Random(double, double);
double uniform_CR_Random();
double normal_CR_Random();
double ranged_uniform_CR_Random(double, double);
int ranged_uniform_int_CR_Random(int, int);
int probability_CR_Random(double);
int coin_flip_CR_Random();

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif    
