#ifndef VSMTA_HH
#define VSMTA_HH

#include "MTA/DGC_MODULE.hh"
#include "MTA/Misc/Time/Timeval.hh"
#include "VState.hh"

struct VStateDatum {
  float theta;

  Timeval Before;
  Timeval After;

  int MyCounter;
};

using namespace std;
using namespace boost;

// state machine states enumerated
// defined in DGC_MODULE.hh

// namespace STATES {
//   enum { INIT, ACTIVE, STANDBY, SHUTDOWN, RESTART, NUM_STATES };
// };


class VState : public DGC_MODULE {
public:
  VState();
  ~VState();

  //  void Init();
  void Active();
  // void Standby();
  // void Shutdown();
  // void Restart();

  // string Status();

  void InMailHandler(Mail & ml);
  Mail QueryMailHandler(Mail & ml);

private:

  VStateDatum D;
};

#endif
