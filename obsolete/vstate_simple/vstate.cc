// Simple gps only vdrive for homer, using the small gps reciver.
// Most of this code was copied from dgc/peggy/sensors/gpstest.cc and
// dgc/peggy/p_main.cc

#include "VState.hh"
#include "vsmta.hh"
#include "gps.hh"
#include <fstream>
// declarations for sparrow display library
#include "sparrow/display.h"
#include "sparrow/dbglib.h"
#include "sparrow/channel.h"
#include "MTA/Kernel.hh"
#include "MTA/Modules.hh"

using namespace std;

pthread_t gps_thread;                   // thread for gps
void *gps_run(void *);                  // function for starting gps thread
int GL_GPS_enable = 0;
struct gps_data GL_GPS_gpsdata;         // struct for holding gps updates
int GL_GPS_gpscount;

// logging related declarations
ofstream gpslog; // filestreams for logging
int logflag = 0; // logging enable/disable flag
int testnum = 1; // number of test
int testdate; // date of test
char *gname = "GPS";
char *ext = ".dat";


struct VState_GetStateMsg vehstate;     // current vehicle state

// timing definitions
struct timeval GL_TV_start;             // global time start

VState *p_VState = new VState;

// Added by Ike to do process locking.
#define USE_LOCKS
#ifdef USE_LOCKS
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
boost::recursive_mutex      STATE_MUTEX;
extern int MTA_VALID;

#endif // USE_LOCKS


int main(int argc, char *argv[])
{
  int error = 0;
  int errflag;
  char c;

  if(argc != 2){
    cerr << "usage: vstate [serial port #]\n";
    return -1;
  }

  // get flags for arguments
    while ((c = getopt(argc, argv, "vhdl:?")) != EOF)
    switch (c) {
    case 'h':           errflag++;                       break;
    case 'l':           
      logflag = 1;                // enable logging
      openlogs(optarg);           // print log header      
      break;
    default:		errflag++;			break;
      break;
    }



  // global timing start
  gettimeofday(&GL_TV_start, NULL);
  
  /************************************************************
    GPS INITIALIZATION HERE
  ************************************************************/
  if (gps_open(atoi(argv[1])) == 0){
    if (gps_initialize() == 0){
      GL_GPS_enable = 1;
      GL_GPS_gpscount = 0;
      dbg_info("GPS enabled");
    }else{
      GL_GPS_enable = -1;
      error ++;
      dbg_error("Failed to enable GPS");
    }
  }else{
    GL_GPS_enable = -1;
    error++;
    dbg_error("Failed to enable GPS");
  }
  
  /******* end gps init *******/

  // Pause to report errors if any
  if (error) {
    cout<<"Errors on startup; continue (y/n)?";
    if (getchar() != 'y') exit(1);
  }
  
  // set flags for sensors active
  vehstate.gps_enabled = GL_GPS_enable;
  
  // start the gps reading thread
  if (GL_GPS_enable) {
    if (pthread_create(&gps_thread, NULL, gps_run, (void *) NULL) < 0) {
      dbg_error("Can't start GPS thread \n");
      exit(1);
    }
  }

  Register(shared_ptr<DGC_MODULE>( p_VState ));
  StartKernel();

  return 0;

}
  
// gps_run
// runs gps reading loop and process incoming gps data
void *gps_run(void *)
{
  struct timeval gps_time_read;
  double gpstime;
  
  while (GL_GPS_enable > 0) {
    gps_read(GL_GPS_gpsdata);
    GL_GPS_gpscount++;
    cout << "GL_GPS_gpscount " << GL_GPS_gpscount << endl;
    // Need to actually stuff this crap into vehstate...
    // are x,y,z in UTM?? what are the units of speed, etc?
    vehstate.Northing = GL_GPS_gpsdata.x;
    vehstate.Easting  = GL_GPS_gpsdata.y;
    vehstate.Altitude = GL_GPS_gpsdata.z;
    vehstate.Vel_N = GL_GPS_gpsdata.xd;
    vehstate.Vel_E = GL_GPS_gpsdata.yd;
    vehstate.Vel_U = GL_GPS_gpsdata.zd;
    vehstate.Speed = GL_GPS_gpsdata.speed;
    vehstate.gps_pvt_valid = GL_GPS_gpsdata.pvt_valid;
    vehstate.Yaw = GL_GPS_gpsdata.heading;    
    
    // get the time of read for gps
    gettimeofday(&gps_time_read, NULL);
    gpstime = (double)(gps_time_read.tv_sec - GL_TV_start.tv_sec +
		       (gps_time_read.tv_usec - GL_TV_start.tv_usec) / 1e6);
    
    if (logflag > 0){
      gpslog<<gpstime<<'\t'<<GL_GPS_gpsdata.x<<'\t'<<GL_GPS_gpsdata.y<<'\t'<<
	GL_GPS_gpsdata.z<<'\t'<<GL_GPS_gpsdata.speed<<'\t'<<
	GL_GPS_gpsdata.heading<<'\t'<<GL_GPS_gpsdata.vert_vel<<'\t'<<
	GL_GPS_gpsdata.utc_hour<<'\t'<<GL_GPS_gpsdata.utc_min<<'\t'<<
	GL_GPS_gpsdata.utc_sec<<'\t'<<GL_GPS_gpsdata.pvt_valid<<endl;
    }
  }
  return NULL;
}

void openlogs(char* testnum)
{
  char filename[MAX_PATH_LEN];
  char datename[MAX_PATH_LEN];
  struct tm* today;
  time_t now;
  int year, day, month;

  now = time(NULL); // get time now
  today = localtime(&now); // get mmddyy
  printf("%d %d %d\n",today->tm_year,today->tm_mon,today->tm_mday);
  year = today->tm_year + 1900; // because of stupid 04 = 1904
  month = today->tm_mon;
  day = today->tm_mday;

  sprintf(datename, "%d%.2d%.2d", year, month, day);

  // open gps file
  sprintf(filename, "%s%s%s%s", datename, gname, testnum, ext);
  gpslog.open(filename);

  // output gps log headers
  gpslog<<"%time"<<'\t'<<'x'<<'\t'<<"y"<<'\t'<<"z"<<'\t'<<"v"<<'\t'<<
    "heading"<<'\t'<<"v_vel"<<'\t'<<"hour"<<'\t'<<"min"<<'\t'<<"sec"<<
    '\t'<<"gps valid"<<endl;

  // output vehicle log headers

  return;
}
