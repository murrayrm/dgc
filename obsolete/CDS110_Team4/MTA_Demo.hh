#ifndef MTA_DEMO_HH
#define MTA_DEMO_HH

#include "MTA/Kernel.hh"
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>


class MTA_Demo : public DGC_MODULE {

 public:
  MTA_Demo();

  void Active();
  void InMailHandler(Mail & ml);
  Mail QueryMailHandler(Mail & ml);

private:

  boost::recursive_mutex mutex;

  int TimeCounter;

};

namespace MTA_Demo_Messages {

  enum {
    // Query messages
    GetTime,


    // One Way Messages
    SetTime,


    Num // A place holder
  };

};


#endif
