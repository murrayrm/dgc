#ifndef CDS110_Team4_HH
#define CDS110_Team4_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>

#include "../../MTA/Kernel/Kernel.hh"

#include "../../bob/vehlib/VState.hh"
#include "../../bob/vehlib/VDrive.hh"
#include "../../bob/vehlib/VStarPackets.hh"

//#include <boost/thread/thread.hpp>
//#include <boost/thread/recursive_mutex.hpp>
//#include "PathFollowerDatum.hh"

static const double DEFAULT_P_Y = -0.05;
static const double DEFAULT_I_Y = -0.1;
static const double DEFAULT_D_Y = -0.3;
static const double DEFAULT_P_H = 0;
static const double DEFAULT_I_H = 0;
static const double DEFAULT_D_H = 0;

//only used if using check_Radius
static const double CLOSE_ENUF = 10;

//used to determine the speed at which the controller tries to follow the path (in m/s)
static const double SPEED = 5;

//this was pulled out of thin air, waiting for alex to give value
//should never be more than pi/6, so .5 is prolly a good value
static const double MAX_ANGLE = 3.1415/6;
static const double INTEGRATOR_SAT_Y = 0.5;
static const double INTEGRATOR_SAT_H = 0.0;
static const double STEER_TIMEOUT = 1;

class CDS110_Team4 : public DGC_MODULE {

 public:
  CDS110_Team4();

  void Init();
  void UpdateState();
  void Active();

  // void InMailHandler(Mail & ml);
  void check_Waypoint();
  void check_Radius();
  double get_YError();
  double get_HError();
  double get_Angle(double yerr,double herr);
  //private:



  VState_GetStateMsg SS;
  //  VDrive_CmdMotionMsg d;
  VDrive_CmdSteerAccel d;
Steer_Packet sp; 
  int stop, east_curr, north_curr, east_next, north_next;
  double time_old, y_error_old,h_error_old;
  ofstream m_outputAngle, m_outputState, m_outputWaypoint, m_outputYerror,m_outputTest,m_outputHerror,m_outputTheta;


  //  boost::recursive_mutex mutex;
};


/* I don't ever receive messages in the current implementation, so I think I can leave this out 
namespace CDS110_Team4_Messages {

  enum {
    // Query messages
    GetState,


    // One Way Messages
    SetSteering,


    Num // A place holder
  };

};
*/ 

#endif
