#include "MTA_Demo.hh"
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>


MTA_Demo::MTA_Demo() : DGC_MODULE( MODULES::MTA_Demo, "MTA Demo Module - Not for sale.", 0) {

  // do something here if you want
}

void MTA_Demo::Active() {

  // this is "main"

  // set up the counter
  TimeCounter = 0;

  // wrap the loop in a while ( ContinueInState() ) 
  while( ContinueInState() ) {
    // for this example simply update a counter every 1 second

    sleep(1);
    // update the counter, but use a data lock for example
    // to prevent simultaneous access (in all honesty this 
    // is one time we really dont need data locking, but 
    // it is here for example).

    //
    boost::recursive_mutex::scoped_lock sl(mutex);
    TimeCounter++;
    // lock automatically unlocks at scope brace "}"
  }
  
}


void MTA_Demo::InMailHandler(Mail & msg) {

  // first, figure out what type of message we received
  switch ( msg.MsgType() ) {

    // In the InMailHandler we are concerned with all
    // one-way messages

    // the only one-way message for this module
  case MTA_Demo_Messages::SetTime: 
    
    // read in and set the time from the message
    // but first lock the data
    // use brackets for the scope lock
    {
      boost::recursive_mutex::scoped_lock sl(mutex);
      msg >> TimeCounter;

    }
    break; // break this case of the switch

  default:
    // let the parent class handle the message
    DGC_MODULE::InMailHandler(msg);
  }
}

Mail MTA_Demo::QueryMailHandler(Mail & msg) {

  Mail reply = ReplyToQuery(msg); // this sets up the return message

  // first, figure out what type of message we received
  switch ( msg.MsgType() ) {

    // In the QueryMailHandler we are concerned with all
    // query messages

    // the only query message for this module
  case MTA_Demo_Messages::GetTime: 
    
    // first lock the data,
    // then append the integer onto the Mail object 
    {
      boost::recursive_mutex::scoped_lock sl(mutex);
      reply << TimeCounter;

    }
    break; // break this case of the switch

  default:
    // let the parent class handle the message
    reply = DGC_MODULE::QueryMailHandler(msg);
  }

  return reply;

}




int main(int argc, char **argv) {

  Register(shared_ptr<DGC_MODULE>(new MTA_Demo));
  StartKernel();

  return 0;
}
