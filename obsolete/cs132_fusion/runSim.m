srStereo = generateSensor([-1 0 -3]', [-pi/8, 0, 0]', 1, [pi/4, 240, 320, 0.5, 32]);
lrStereo = generateSensor([-1 0 -3]', [0, 0, 0]', 1, [pi/4, 480, 772, 1.0, 32]);
roofLadar = generateSensor([-1 0 -3]', [atan2(-3, 30), 0, 0]', 0, [80 2.5e-5]);
bumperLadar = generateSensor([0 0 -1]', [0, 0, 0]', 0, [80 2.5e-5]);
map1 = generateMap(0, 100, 0.2, 1, [nan -100]);
map2 = generateMap(0, 100, 0.2, 0, [nan -100]);
bob = generateVehicle([5 0 0]', [0 0 0]', [0.0025 0 0.0025]', [0.000076 0 0]', {srStereo lrStereo roofLadar bumperLadar}, map1);

terrain1 = generateTerrain(0, 100, 0.01, 1, [1 (pi/15) 0]);

base_filename = 'run3_peaked_fast';

time_step = 0.4;
speed = 5;
hold off;
for i=1:round(terrain1(length(terrain1)-1,1)/(time_step*speed))
    x_coord = bob.position(1)+time_step*speed;
    temp_var = find(terrain1(:,1)>x_coord);
    z_coord = terrain1(temp_var(1)-1,2);
    temp_var = find(terrain1(:,1)>(x_coord-4));
    back_z_coord = terrain1(temp_var(1)-1,2);
    bob.orientation(1) = -atan2(z_coord - back_z_coord, x_coord-(x_coord-4));
    bob.position = [x_coord, 0, z_coord]';
    plotVehicle(bob);
    hold on
    for j=1:length(bob.sensors)
        meas = getSensorMeasurement(terrain1, bob, bob.sensors{j});    
        bob.map = updateMap(bob.map, meas.point, meas.covar, bob.sensors{j}, bob);
        map2 = updateMap(map2, meas.point, meas.covar, bob.sensors{j}, bob);
    end
    plotMap(bob.map, 1, 'r.-');
    plotMap(map2, 0, 'g.-');
    plotTerrain(terrain1);
    axis([x_coord-5 x_coord+55 -5 5]);
    pause(0.000005);
    filename = sprintf('%s-%.4d.bmp', base_filename, i);    
    print('-dbitmap', filename);
    hold off
end
