#include "ppspline.hh"

ppSpline::ppSpline()
{
  /* Set up the Octave process for I/O. */
  m_octaveProc.open("octave --silent --no-line-editing ");
  m_octaveProc << setprecision(20);

  /* Initialize number of breaks to zero. */
  m_iBreaks = 0;

  m_pN = new double[PP_MAX_BREAKS];
  m_pE = new double[PP_MAX_BREAKS];
  m_pBreaks = new double[PP_MAX_BREAKS];
  m_pCoefs = new double[(PP_MAX_BREAKS-1)*PP_DIM * PP_ORDER];
}

//ppSpline::ppSpline( int iBreaks )
//{
//  /* Set up the Octave process for I/O. */
//  m_octaveProc.open("octave --silent --no-line-editing ");
//  m_octaveProc << setprecision(20);
//
//  /* Initialize number of breaks to zero. */
//  m_iBreaks = 0;
//
//  m_pN = new double[iBreaks];
//  m_pE = new double[iBreaks];
//  m_pBreaks = new double[iBreaks];
//  m_pCoefs = new double[(iBreaks-1)*PP_DIM + PP_ORDER];
//}

ppSpline::~ppSpline()
{
  // Be sure to delete any allocated memory here.
  delete m_pN;
  delete m_pE;
  delete m_pBreaks;
  delete m_pCoefs;
}

void ppSpline::inputDataPoint( double brk, double northing, double easting )
{
#warning "Check for overflow in case user specified the number of breaks in constructor."
  m_pBreaks[m_iBreaks] = brk;
  m_pN[m_iBreaks] = northing;
  m_pE[m_iBreaks] = easting;
  
  if(m_iBreaks < PP_MAX_BREAKS-1)
  {
    m_iBreaks++;
  }
  else
  {
    cerr << "ppSpline: overflow.\n";
  }
}


double* ppSpline::getPoint( double s )
{
  // Calculate the spline point (x, xd, xdd, y, yd, ydd) 
  // at a given parameter value (relative time).
  // NOTES: The coefficient array is given by m_pCoefs.
  // The index macro into this array is IDX(dim,num,pwr)
  // The value of the spline is calculated in MATLAB as
  // % alternative to fnplt
  // for j=1:pp.pieces
  //   s = [pp.breaks(j):.1:pp.breaks(j+1)];
  //   hp(j) = plot(polyval(pp.coefs(2*j-1,:), s-pp.breaks(j)), ...
  //                polyval(pp.coefs(2*j,:),s-pp.breaks(j)),'c-.');
  // end

  // Find the index of the piece that contains the requested parameter value.
  int ipiece = 0; 

  // First check the bounds.
  if( m_pBreaks[0] <= s && s <= m_pBreaks[m_iBreaks-1] )
  {
    for( int i = 0; i < m_iBreaks; i++ )
    {
      if( s > m_pBreaks[i] )
      {
        ipiece = i;
      }
    }
  }
  else 
  {
    cerr << "Parameter is out of bounds!" << endl;
  }

  // the x-value of the spline at the given parameter value
  m_ptArray[0] = m_pCoefs[IDX(0,ipiece,3)] * pow(s - m_pBreaks[ipiece],3) +
                 m_pCoefs[IDX(0,ipiece,2)] * pow(s - m_pBreaks[ipiece],2) +
                 m_pCoefs[IDX(0,ipiece,1)] *    (s - m_pBreaks[ipiece]) +
                 m_pCoefs[IDX(0,ipiece,0)];
  // the xd-value of the spline at the given parameter value
  m_ptArray[1] = 3.0 * m_pCoefs[IDX(0,ipiece,3)] * pow(s - m_pBreaks[ipiece],2) +
                 2.0 * m_pCoefs[IDX(0,ipiece,2)] *    (s - m_pBreaks[ipiece]) +
                       m_pCoefs[IDX(0,ipiece,1)];
  // the xdd-value of the spline at the given parameter value 
  m_ptArray[2] = 6.0 * m_pCoefs[IDX(0,ipiece,3)] * (s - m_pBreaks[ipiece]) +
                 2.0 * m_pCoefs[IDX(0,ipiece,2)];
  // the y-value of the spline at the given parameter value
  m_ptArray[3] = m_pCoefs[IDX(1,ipiece,3)] * pow(s - m_pBreaks[ipiece],3) +
                 m_pCoefs[IDX(1,ipiece,2)] * pow(s - m_pBreaks[ipiece],2) +
                 m_pCoefs[IDX(1,ipiece,1)] *    (s - m_pBreaks[ipiece]) +
                 m_pCoefs[IDX(1,ipiece,0)];
  // the xd-value of the spline at the given parameter value
  m_ptArray[4] = 3.0 * m_pCoefs[IDX(1,ipiece,3)] * pow(s - m_pBreaks[ipiece],2) +
                 2.0 * m_pCoefs[IDX(1,ipiece,2)] *    (s - m_pBreaks[ipiece]) +
                       m_pCoefs[IDX(1,ipiece,1)];
  // the xdd-value of the spline at the given parameter value 
  m_ptArray[5] = 6.0 * m_pCoefs[IDX(0,ipiece,3)] * (s - m_pBreaks[ipiece]) +
                 2.0 * m_pCoefs[IDX(0,ipiece,2)];

  return m_ptArray;
}


void ppSpline::calcCoefs( void )
{
  // NOTES: Want to send Octave the right syntax command.  Cues from MATLAB:
  // First argument is the 1*m_iBreaks array of break values.
  // Second argument is array of PP_ORDER rows by m_iBreaks colums that 
  // represents the Northing, then Easting of the spline.
  // Syntax is pp = csapi( first, second );
#warning "See if this is faster if you compose the string ahead of time." 
#warning "Consider coding csapi directly in C++ to speed things up."

  // send the command to octave to compute the spline
  m_octaveProc << "format long; csapi([";
  for( int i = 0; i < m_iBreaks; i++ )
  {
    // input the break vector
    m_octaveProc << m_pBreaks[i] << " ";
  }
  m_octaveProc << "], [";
  for( int i = 0; i < m_iBreaks; i++ )
  {
    // input the northing values at the breaks
    m_octaveProc << m_pN[i] << " ";
  }
  m_octaveProc << "; ";
  for( int i = 0; i < m_iBreaks; i++ )
  {
    // input the easting values at the breaks
    m_octaveProc << m_pE[i] << " ";
  }
  m_octaveProc << "]) " << endl;

  // ignore the first four lines of text
  for( int i=0; i<4; i++ )
  {
    m_octaveProc.ignore(10000, '\n');
  }
  // read in the spline coefficients from octave
  for( int i=0; i < (m_iBreaks-1)*PP_DIM * PP_ORDER; i++ )
  {
    m_octaveProc >> m_pCoefs[i];
  }
  // ignore the remaining lines of output
  for( int i=0; i<13; i++ )
  {
    m_octaveProc.ignore(10000, '\n');
  }
}


void ppSpline::dispCoefs(ostream& outstream, int numPoints)
{
//  for( int i=0; i < (m_iBreaks-1)*PP_DIM; i++ )
//  {
//    for( int j=0; j < PP_ORDER; j++ )
//    {
//      printf("  % .4f", m_pCoefs[i*PP_ORDER+j]); 
//    }
//    cout << endl;
//  }
//  cout << endl;
 
  if(numPoints == -1) { numPoints = m_iBreaks; }

  outstream << setprecision(10);

  // i corresponds to the dimension index
  for( int i=0; i < PP_DIM; i++ )
  { 
    // j corresponds to the break number
    for( int j=0; j < numPoints-1; j++ )
    { 
      // k corresponds to the power 
      for( int k=PP_ORDER-1; k >= 0; k-- )
      {
        outstream << "  " << m_pCoefs[IDX(i,j,k)];
      }
      outstream << endl;
    }
  }
}

void ppSpline::dispBreaks(ostream& outstream, int numPoints)
{
  if(numPoints == -1) { numPoints = m_iBreaks; }

  outstream << setprecision(10);

  // j corresponds to the break number
  for( int j=0; j < numPoints-1; j++ )
  { 
    outstream << m_pBreaks[j] << endl;
  }
}
