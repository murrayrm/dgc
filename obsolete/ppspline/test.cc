#include "ppspline.hh"

#include "sys/time.h"
using namespace std;

// custom function to subtract two times
double timeval_subtract( const timeval &, const timeval & );

int main(void)
{
  /* Declare a spline. */
  ppSpline spline;

  double* array;
  
  /* Fill the spline with four points. */
  spline.inputDataPoint( 0, 0, 0 );
  spline.inputDataPoint( 1, 1, 2 );
  spline.inputDataPoint( 2, 3, 3 );
  spline.inputDataPoint( 3, 4, 2 );

  cout << "Calling csapi([0 1 2 3], [0 1 3 4; 0 2 3 2])" << endl;
 
  // How long does it take?
  timeval before, after;
  double elapsed;
  
  for( int i=0; i<2; i++ )
  {
    // set the before time
    printf("[%s][%d] Timer started.\n", __FILE__, __LINE__);
    gettimeofday( &before, NULL);
     
    spline.calcCoefs();

    gettimeofday( &after, NULL);
    printf("[%s][%d] Timer stopped.\n", __FILE__, __LINE__);
    elapsed = timeval_subtract( after, before );
    cout << "Total elapsed time = " << elapsed << " sec" << endl;
  }
  spline.dispCoefs();

  for( int i=0; i<4; i++ )
  {
    array = spline.getPoint((double)i);
    cout << array[0] << " " << array[1] << " " << array[2] << endl;
    cout << array[3] << " " << array[4] << " " << array[5] << endl << endl;
  }

  return 0;
}

double timeval_subtract(const timeval & t1, const timeval & t2)
{
  double dt1 = ((double) t1.tv_sec) + (((double) t1.tv_usec) / 1000000);
  double dt2 = ((double) t2.tv_sec) + (((double) t2.tv_usec) / 1000000);

  return dt1 - dt2;
}


  // Desired result can be checked by running manually in Octave:
  //
  // octave:2> pp = csapi([1 2 3], [0 1 2; 0 1 0])
  // pp =
  // {
  //   P =
  //
  //        0.00000   0.00000   1.00000   0.00000
  //        0.00000   0.00000   1.00000   1.00000
  //        0.33333  -2.00000   2.66667   0.00000
  //        0.33333  -1.00000  -0.33333   1.00000
  //
  //        d = 2
  //        k = 4
  //        n = 2
  //        x =
  //
  //          1
  //          2
  //          3
  //
  // }
  //                                           

