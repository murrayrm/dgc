#ifndef __PPSPLINE_HH__
#define __PPSPLINE_HH__

#include <iostream> // for cout, endl, ...
#include <iomanip> // for setprecision

#warning "Should link to some installed location for pstream.h."
#include "pstream.h"

#define PP_MAX_BREAKS  1001 // maximum number of spline breaks (control points)

#define PP_DIM    2  // planar splines
#define PP_ORDER  4  // cubic splines

/*! The IDX macro gives the coefficient index at dimension dim (either 
 ** 0=Northing or 1=Easting), power pwr (0, 1, 2 or 3), and the polynomial
 ** piece indexed by num (num \in [0, m_iBreaks-1], num is also the index of 
 ** the break that begins the polynomial piece). */
#define IDX(dim,num,pwr) ((dim)*(m_iBreaks-1)*PP_ORDER+(num)*PP_ORDER+(PP_ORDER-1-(pwr)))

using namespace std;

/*! The piecewise polynomial spline class. */
class ppSpline
{
  /*! The number of pieces in the piecewise polynomial spline. */
  int m_iBreaks;
  /*! The break values of the spline (values of the /time/ parameter). By 
   ** design, m_pBreaks will be a vector of size m_pieces + 1. */
  double* m_pBreaks;
  /*! The Northing array of input data (the control points). */
  double* m_pN;
  /*! The Easting array of input data (the control points). */
  double* m_pE;
  /*! The coefficient array of the spline.  By design, this array will be of 
   ** size PP_DIM * m_pieces number of rows and PP_ORDER number of columns. For 
   simplicity, this will be implemented as a one-dimensional array, whose 
   indices are read from left to right, starting at the top row. */
  double* m_pCoefs;

  /*! Member array to hold (x, xd, xdd, y, yd, ydd) spline point. */
  double m_ptArray[6];
  
  /*! Stream interface to an Octave process. */
  redi::pstream m_octaveProc;

  public:
    /*! The default constructor allocates the maximum amount of memory for the 
     ** data (limited by PP_MAX_BREAKS). */
    ppSpline();
    /*! The constructor given the number of breaks will allocate only the 
     ** necessary amount of memory. */
    //ppSpline( int iBreaks );
    /*! Deconstructor. */
    ~ppSpline();

    /*! Function to input a control point for the spline. */
    void inputDataPoint( double brk, double northing, double easting );
    
    /*! Accessor function to get the value of the spline at a point. This 
     ** returns a pointer to an array of [x, xd, xdd, y, yd, ydd].  This 
     ** function may be called repeatedly to fill in a CTraj. */
    double* getPoint( double param );

    /*! Function to call after all data points are input.  This will calculate 
     ** and returna  pointer to the array of spline coefficients. */
    // TODO: Make this return a pointer?
    void calcCoefs( void );
    
    /*! Print out the current coefficients to the given ostream (cout if not
     *  specified). If less than every point should be printed, that can be
     *  specified as well. */
    void dispCoefs(ostream& outputstream = cout, int numPoints = -1);

    /*! Print out the current breaks to the given ostream (cout if not
     *  specified). If less than every break should be printed, that can be
     *  specified as well. */
    void dispBreaks(ostream& outputstream = cout, int numPoints = -1);
      
  private:

};

#endif
