// p_main.hh - declarations and definitions for peggy's main code
// Haomiao Huang
// 7/8/04

#ifndef P_MAIN_HH
#define P_MAIN_HH

#define MAX_PATH_LEN 256

// vehicle state structure
// contains vehicle state information that controller needs
// nav and body frames are defined same as in dgc frames.txt document
struct state_data
{
  // state data
  // state data is given in nav frame
  float x;
  float y;
  float z;
  float xd;
  float yd;
  float zd;
  float xdd;
  float ydd;
  float zdd;

  // attitude angles and delta-angles
  float yaw;
  float pitch;
  float roll;

  float dy;
  float dp;
  float dr;

  // sideslip angle
  float beta;
  
  // time when this state was valid
  float valid_time;

};

// void openlogs(char* testnum)
// ************************************
// opens files with the test number and test date attached, i.e. if testnum
// is 5 and date is 0831 the imu log files will be 0831IMU5.dat
void openlogs(char* testnum);

// void closelogs()
// *****************************
// Closes all logfiles
void closelogs();

#endif
