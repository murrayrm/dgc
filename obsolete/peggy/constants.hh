// Constant definitions declarations
// These include things like vehicle constants, ports, etc

#ifndef CONSTANTS_HH
#define CONSTANTS_HH

// port constants
// note: these are temporary (made up), replace with the real ones 
#define IMU_COM 0
#define VEH_COM 1
#define GPS_COM 2

#endif
