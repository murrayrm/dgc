// Peggy Main Program v 1.0
// Haomiao Huang
// 7/7/04
//
// This is the main body code for peggy.  It has (currently) three threads:
// IMU - thread for communicating to the IMU
// GPS - thread for communicating to GPS
// vehicle - thread for communicating to vehicle controls
// maybe a fourth thread for running control loop?
// 
// Revision History:
// 7/7/04 - Created H Huang

#include <stdlib.h>
#include <math.h>
#include <iostream.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <time.h>
#include <fstream.h>

// declarations for sparrow display library
#include "sparrow/display.h"
#include "sparrow/dbglib.h"
#include "sparrow/channel.h"

// peggy definitions
#include "constants.hh"
#include "p_main.hh"



// Includes/declarations for GPS files
#include "gps.hh"
pthread_t gps_thread;           // thread for gps 
void *gps_run(void *);          // function for starting gps thread
int GL_GPS_enable = 0;
struct gps_data GL_GPS_gpsdata;               // struct for holding gps updates
int GL_GPS_gpscount;

// includes for imu files
#include "imu.hh"
pthread_t imu_thread;           // thread for imu
void *imu_run(void *);          // function for starting imu thread
int GL_IMU_enable = 0;
struct imu_data GL_IMU_imudata;               // struct for holding imu updates
int GL_IMU_imucount;           // counter for imu main loop
int GL_IMU_sequence_error =0;  // imu packets dropped error flag

// includes for vehicle files
#include "veh.hh"
pthread_t veh_thread;           // thread for vehicle control 
void *veh_run(void *);          // function for starting vehicle thread
int GL_VEH_enable = 0;
struct veh_data GL_VEH_vehdata; // structure for holding vehicle data
int GL_VEH_vehcount;  // counter to see if vehicle loop is running

// user defined button functions (controlled via sparrow interface)
static int user_quit(long);     // exits program

// timing definitions
struct timeval GL_TV_start; // global time start

// logging related declarations
ofstream gpslog; // filestreams for logging
ofstream imulog;
ofstream vehlog;
int logflag = 0; // logging enable/disable flag
int testnum = 1; // number of test
int testdate; // date of test
char *gname = "GPS";
char *iname = "IMU";
char *vname = "VEH";
char *ext = ".dat";

// Usage message
char *usage = "\
Usage: %s [-v] [options]\n\
-v turn on verbose error messages\n\
-h print this message\n\
-d disable display\n\
-l testnum enable logging\n\
-i disable imu\n\
-e disable embedded\n\
-g disable gps\n\
";

// sparrow display headers
// note that these lines need to be at the bottom of all declarations so that
// the display will pick up all the declared variables
int display_flag = 1;
#include "p_disp.h"  // display declarations
pthread_t dd_thread; // display thread

int main(int argc, char **argv)
{
  // initialize error flags
  int c, errflag = 0, error = 0;
  int lognum;

  // turn on debug error display during startup 
  dbg_flag = dbg_all = dbg_outf = 1;
  
  /* Parse command line arguments */
  while ((c = getopt(argc, argv, "vhdl:ieg?")) != EOF)
    switch (c) {
    case 'v':		dbg_flag = 1;			break;
    case 'd':           display_flag = 0;               break;
    case 'h':           errflag++;                       break;
    case 'l':           
      logflag = 1;                // enable logging
      openlogs(optarg);           // print log headers
      
      break;
    case 'i':           GL_IMU_enable = -1;             break;
    case 'e':           GL_VEH_enable = -1;             break;
    case 'g':           GL_GPS_enable = -1;             break;
    default:		errflag++;			break;
      break;
    }

    /* Print an error message if anything went wrong */
    if (errflag || argc < optind) {
      fprintf(stderr, usage, argv[0]);
      exit(1);
    }
    

    // global timing start
    gettimeofday(&GL_TV_start, NULL);

    /* Device Initializations
       Initialize all devices, report any errors on initialization
       IMU, GPS, vehicle
    */
    
    /************************************************************
    GPS INITIALIZATION HERE
    ************************************************************/
    if (GL_GPS_enable == 0){
      if (gps_open(GPS_COM) == 0){
	if (gps_initialize() == 0){
	  GL_GPS_enable = 1;
	  GL_GPS_gpscount = 0;
	  dbg_info("GPS enabled");
	}else{
	  GL_GPS_enable = -1;
	  error ++;
	  dbg_error("Failed to enable GPS");
	}
      }else{
	GL_GPS_enable = -1;
	error++;
	dbg_error("Failed to enable GPS");
      }
    }

    /************************************************************
    IMU INITIALIZATION HERE
    ************************************************************/
    if (GL_IMU_enable == 0){
      if(imu_open(IMU_COM) == 0) {
	GL_IMU_enable = 1;
	GL_IMU_imucount = 0;
	dbg_info("IMU enabled");
      }else{
	GL_IMU_enable = -1;
	error++;
      }
    }
      
    /***********************************************************
    VEHICLE INITIALIZATION HERE
    ***********************************************************/
    if (GL_VEH_enable > 0) {
      if (veh_open(VEH_COM) == 0) {
	GL_VEH_enable = 1;
	GL_VEH_vehcount = 0;
	dbg_info("VEH enabled");
      }else{
	GL_VEH_enable = -1;
	error++;
      }
    }

    // Pause to report errors if any
    if (error) {
      cout<<"IMU: "<<GL_IMU_enable<<" GPS: "<<GL_GPS_enable<<
	" VEH: "<<GL_VEH_enable<<endl;
      cout<<"Errors on startup; continue (y/n)?";
      if (getchar() != 'y') exit(1);
    }

    // turn off sparrrow printfs while display is running
    dbg_all = 0;

    // sparrow display setup
    if (display_flag >0) {
      /* Initialize the display package */
      if (dd_open() < 0) {
	exit(1);    /* initialize the database, screen */
	dbg_error("Error initializing display");
      }
      dd_bindkey('Q', user_quit);    // Q quits
      dd_usetbl(overview);	     /* set display description table */
    }

    // start up execution threads
    
    // start gps thread
    if (GL_GPS_enable > 0) {
      if (pthread_create(&gps_thread, NULL, gps_run, (void *) NULL) < 0) {
	dbg_error("Can't start GPS thread \n");
	exit(1);
      }
    }

    // imu thread
    if (GL_IMU_enable > 0) {
      if(pthread_create(&imu_thread, NULL, imu_run, (void *) NULL) < 0) {
	dbg_error("Can't start imu thread \n");
	exit(1);
      }      
    }
    
    // vehicle thread
    if (GL_VEH_enable > 0) {
      if(pthread_create(&veh_thread, NULL, veh_run, (void *) NULL) < 0) {
	dbg_error("Can't start vehicle thread \n");
	exit(1);
      }
    }

    if (display_flag >0) {
      /* Run the display manager as a thread */
      pthread_create(&dd_thread, NULL, dd_loop_thread, (void *) NULL);
    }

    if (display_flag > 0){
      // main display thread, doesn't exit
      pthread_join(dd_thread, (void **) NULL);
    }else{
      pthread_join(imu_thread, (void **) NULL);
    }

    // after finished executing threads, clear screen and free memory
    if (display_flag > 0) {
      dd_close();	     /* clear the screen and free memory */
    }

}

//********** Callback functions for the GUI


/* Quit the main program */
int user_quit(long arg)
{
  if (GL_IMU_enable > 0){
    // close the imu
    imu_close();
  }

  if (GL_GPS_enable > 0){
    // close the imu
    gps_close();
  }
   
  if (GL_VEH_enable > 0){
    // close the imu
    veh_close();
  }



  /* Tell dd_loop to abort now */
  return DD_EXIT_LOOP;
}

//********* Individual Threads **************

// imu_run
// runs imu reading loop and process incoming imu data
void *imu_run(void *)
{    
  uint8 checksequence;
  int firsttime = 1;
  double imutime;
  struct timeval imu_time_read;

  while (GL_IMU_enable > 0){  
   
    // read the data from the imu
    imu_read_data(GL_IMU_imudata);
    GL_IMU_imucount++;
    
    // get the time of read
    gettimeofday(&imu_time_read, NULL);
    imutime = (double)(imu_time_read.tv_sec - GL_TV_start.tv_sec + 
		       (imu_time_read.tv_usec - GL_TV_start.tv_usec) / 1e6);

    // check if imu data packets are being dropped
    if (firsttime) {
      checksequence = GL_IMU_imudata.counter;
      firsttime = 0;
    }else {
      if(GL_IMU_imudata.counter - ((1 + checksequence) % 256) != 0) {
	GL_IMU_sequence_error++;
      }
      checksequence = GL_IMU_imudata.counter;
    }

    if ((display_flag <= 0) && (GL_IMU_imucount < 100)){
      cout<<"GPS: "<<GL_GPS_gpscount<<" IMU: "<<GL_IMU_imucount<<
	" VEH: "<<GL_VEH_vehcount<<"LOG: "<<logflag<<endl;
    }
    
    if (logflag > 0) {
      imulog.precision(15);
      imulog<<imutime<<'\t'<<GL_IMU_imudata.xdd<<'\t'<<GL_IMU_imudata.ydd<<
	'\t'<<GL_IMU_imudata.zdd<<'\t'<<GL_IMU_imudata.dtx<<'\t'<<
	GL_IMU_imudata.dty<<'\t'<<GL_IMU_imudata.dtz<<'\t'<<GL_IMU_imudata.dt<<
	'\t'<<(int)GL_IMU_imudata.counter<<endl;
    }
  }

  return NULL;
}

// gps_run
// runs gps reading loop and process incoming gps data
void *gps_run(void *)
{
  struct timeval gps_time_read;
  double gpstime;
  
  while (GL_GPS_enable > 0) {
    gps_read(GL_GPS_gpsdata);
    GL_GPS_gpscount++;

    // get the time of read for gps
    gettimeofday(&gps_time_read, NULL);
    gpstime = (double)(gps_time_read.tv_sec - GL_TV_start.tv_sec + 
		       (gps_time_read.tv_usec - GL_TV_start.tv_usec) / 1e6);

    if (logflag > 0){
      gpslog.precision(15);
      gpslog<<gpstime<<'\t'<<GL_GPS_gpsdata.x<<'\t'<<GL_GPS_gpsdata.y<<'\t'<<
	GL_GPS_gpsdata.z<<'\t'<<GL_GPS_gpsdata.speed<<'\t'<<
	GL_GPS_gpsdata.heading<<'\t'<<GL_GPS_gpsdata.vert_vel<<'\t'<<
	GL_GPS_gpsdata.utc_hour<<'\t'<<GL_GPS_gpsdata.utc_min<<'\t'<<
	GL_GPS_gpsdata.utc_sec<<'\t'<<GL_GPS_gpsdata.pvt_valid<<endl;
    }

  }

  return NULL;
}

// veh_run
// runs vehicle loop reading from vehicle embedded system
void *veh_run(void *)
{
  struct timeval veh_time_read;
  double vehtime;

  while(GL_VEH_enable > 0){
    veh_read(GL_VEH_vehdata);
    GL_VEH_vehcount++;
    
    // get the time of read for gps
    gettimeofday(&veh_time_read, NULL);
    vehtime = (double)(veh_time_read.tv_sec - GL_TV_start.tv_sec + 
		       (veh_time_read.tv_usec - GL_TV_start.tv_usec) / 1e6);

    if (logflag > 0){
      vehlog<<vehtime<<'\t'<<GL_VEH_vehdata.steering<<'\t'<<
	GL_VEH_vehdata.throttle<<'\t'<<GL_VEH_vehdata.aux<<'\t'<<
	GL_VEH_vehdata.engine_vel<<'\t'<<GL_VEH_vehdata.fr_vel<<'\t'<<
	GL_VEH_vehdata.fl_vel<<'\t'<<GL_VEH_vehdata.rr_vel<<'\t'<<
	GL_VEH_vehdata.rl_vel<<endl;
    }

  }

  return NULL;
}

// void openlogs(char* testnum)
// ************************************
// opens files with the test number and date attached, i.e. if testnum
// is 5 the imu log files will be yyyymmddimu5.dat
void openlogs(char* testnum)
{
  char filename[MAX_PATH_LEN];
  char datename[MAX_PATH_LEN];
  struct tm* today;
  time_t now;
  int year, day, month;

  now = time(NULL); // get time now
  today = localtime(&now); // get mmddyy
  printf("%d %d %d\n",today->tm_year,today->tm_mon,today->tm_mday);
  year = today->tm_year + 1900; // because of stupid 04 = 1904
  month = today->tm_mon;
  day = today->tm_mday;

  sprintf(datename, "%d%.2d%.2d", year, month, day);

  // open imu file
  sprintf(filename, "%s%s%s%s", datename, iname, testnum, ext);
  imulog.open(filename);

  // open gps file
  sprintf(filename, "%s%s%s%s", datename, gname, testnum, ext);
  gpslog.open(filename);

  // open veh file
  sprintf(filename, "%s%s%s%s", datename, vname, testnum, ext);
  vehlog.open(filename);

  // output imu log headers
  imulog<<"%time"<<'\t'<<"ax"<<'\t'<<"ay"<<'\t'<<"az"<<'\t'<<
    "tx"<<'\t'<<"ty"<<'\t'<<"tz"<<'\t'<<"dt"<<'\t'<<"counter"<<endl;

  // output gps log headers
  gpslog<<"%time"<<'\t'<<'x'<<'\t'<<"y"<<'\t'<<"z"<<'\t'<<"v"<<'\t'<<
    "heading"<<'\t'<<"v_vel"<<'\t'<<"hour"<<'\t'<<"min"<<'\t'<<"sec"<<
    '\t'<<"gps valid"<<endl;

  // output vehicle log headers

  return;
}

// void closelogs()
// *****************************
// Closes all logfiles
void closelogs()
{
  imulog.close();
  gpslog.close();
  vehlog.close();
}
