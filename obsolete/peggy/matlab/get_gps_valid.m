function gpsnew = get_gps_valid(gps)
% Function truncates an array of gps data by selecting only the rows which
% have 1's in the gps_valid column (last column).  The array returned is
% the truncated array.

gpsnew = gps(find(gps(:,end)), :);