// test file for gps parse

#include "gps.hh"
#include <fstream>
using namespace std;

int main(int argc, char *argv[])
{
  struct gps_data thedata;
  char ch;
  char name[40];
  int readflag;
  int logflag = 0;
  struct timeval timeread;
  double the_time;
  int gps_device;
  int bytes_read;

  ofstream gpswrite;

  if(argc != 2)
    {
      cerr << "usage: sertest [serial port #]\n";
      return -1;
    }
  cout.precision(15);
  cout<<"Enable data logging?";
  cin>>ch;

  if ((ch =='y') || (ch == 'Y')){
    cout<<"Please enter filename:"<<endl;
    scanf("%s", name);
    gpswrite.open(name);
    gpswrite.precision(15);
    logflag++;
  }

  gps_device = gps_open(atoi(argv[1]));  

  bytes_read = gps_buffer_flush();
  cout<<"Flushed "<<bytes_read<<" bytes"<<endl;

  gps_initialize();

  if (logflag){
    gpswrite<<"%x"<<'\t'<<"y"<<'\t'<<"z"<<'\t'<<"v"<<'\t'<<"heading"<<
      '\t'<<"v_vel"<<'\t'<<"hour"<<'\t'<<"min"<<'\t'<<"sec"<<'\t'<<
      "timeofday"<<'\t'<<"gps valid"<<endl;
  } 

  while(true){
    readflag = gps_read(thedata);
    cout<<"readflag: "<<readflag<<endl;
    gettimeofday(&timeread, NULL);
    the_time = (double)(timeread.tv_sec + timeread.tv_usec / 1e6);
    cout<<thedata.x<<'\t'<<thedata.y<<'\t'<<thedata.z<<
      '\t'<<thedata.speed<<'\t'<<thedata.heading<<'\t'<<
      thedata.vert_vel<<'\t'<<thedata.utc_hour<<'\t'<<
      thedata.utc_min<<'\t'<<thedata.utc_sec<<'\t'<<
      the_time<<'\t'<<thedata.pvt_valid<<endl;
    
    if (logflag){
      gpswrite<<thedata.x<<'\t'<<thedata.y<<'\t'<<thedata.z<<
	'\t'<<thedata.speed<<'\t'<<thedata.heading<<'\t'<<
	thedata.vert_vel<<'\t'<<thedata.utc_hour<<'\t'<<
	thedata.utc_min<<'\t'<<thedata.utc_sec<<'\t'<<
	the_time<<'\t'<<thedata.pvt_valid<<endl;
    }
  }


  return 0;
}
