/*
  IMU serial interface 

  Modified from original Crista code
  H Huang 7/12/04
*/
//!IMUSerial.cpp
//! Serial packet processing for the IMU.
//  Jon Becker
//  Cloud Cap Technology 2003
//
///////////////////////////////////
// This file provided by:        //
// Cloud Cap Technology, Inc.    //
// PO Box 1500                   //
// No. 8 Fourth St.              //
// Hood River, OR, 97031         //
// +1-541-387-2120    (voice)    //
// +1-541-387-2030    (fax)      //
// jbecker@gorge.net (e-mail)    //
// http://www.uavautopilots.com  //
///////////////////////////////////

#include <string.h>
#include <time.h>

#include "Types.h"
#include "CRC.h"
//#include "SerialInterface.h"
#include "IMUExternalTypes.h"
#include "IMUSerial.h"
#include "Indices.h"
//#include "imuserialdemodialog.h"
#include "EncDec.h"
//#include "serial.h"
#include "serialimu.hh"
#include "MTA/Kernel.hh"

#define TIME_WAIT 15000


int imu_parse_char(IMUSensors_t* pIMUSensors, char* sbuffer, int packet_len, 
		   int& parsed)
//!Process the serial state machine to receive IMU packets.
//! This routine can be called multiple times to accumulate a packet.
//! \param pIMUSensors pointer to the IMUSensors structure
{

  //  Timeval timeout(0, TIME_WAIT);
  double timeout = TIME_WAIT / 1e6;
  //char sbuffer[PACKET_LEN];
  int got_message = -1; // flag for knowing when a message was received
  int i = 0;

  static uint8 state=0;  //State machine to process serial packets.
  uint8 ch;		//Current serial byte

  static uint8 MessageType;		//IMU message type
  static uint8 MessageLength;		//IMU message length
  static uint8 MessageBuffer[MAX_MESSAGE_LEN]; //Serial buffer for message body.
  static uint8 MessageIndex=0;	//Index of the current message byte.
  
  static uint8 MessageCRC16H;			//CRC16 high byte
  static uint8 MessageCRC16L;			//CRC16 low byte
  
  uint16 MessageCRC16;
  uint16 ReceivedCRC16;

  //  cout<<"Read initialized"<<endl;

  //  do {
  // got_chars = serial_read_blocking(IMU_COM, sbuffer, packet_len, timeout);
    //  cout<<"Read IMU packet of length "<<got_chars<<" ; decoding..."<<endl;
  // }while (got_chars < packet_len);
  
  parsed = 0;
  

  while ((i < packet_len) && (got_message < 0)) {
    //Get a byte from the serial port.
    //ch=GetchSerial();
      
    ch = sbuffer[i];

    switch (state)
      {
      case 0:  //Look for the first sync character
	if (ch==SYNC_SER_0)
	  {
	    state++;
	    MessageBuffer[MessageIndex++]=ch;  //Save the byte
	    //	      	      cout<<"Got first character"<<endl;
	  }
	break;
	
      case 1:  //Look for the second sync character
	if (ch==SYNC_SER_1)
	  {
	    state++;
	    MessageBuffer[MessageIndex++]=ch;  //Save the byte
	    //cout<<"Got second character"<<endl;
	  }else{  // second char not found, go back to waiting for first char
	    state = 0;  
	    MessageIndex = 0;
	    printf("imu parser: error, 2nd char not found\n");
	  }
	break;
	
      case 2:  //Get the message type
	MessageType=ch;
	MessageBuffer[MessageIndex++]=ch;  //Save the byte
	state++;
	
	break;
	
      case 3:  //Get the message length
	MessageLength=ch;
	MessageBuffer[MessageIndex++]=ch;  //Save the byte
	//If the message is too big then this packet is invalid and we need to start looking for another packet.
	if (MessageLength+MESSAGE_HEADER_LEN+2>MAX_MESSAGE_LEN)
	  {
	    state=0;
	    MessageIndex=0;
	  }
	else{
	  state++;
	  
	}
	break;
	
      case 4:  //Get the message body
	MessageBuffer[MessageIndex++]=ch;  //Save the message body bytes
	
	//If we have received all the message body bytes, go on.
	if (MessageIndex>=MessageLength+MESSAGE_HEADER_LEN)
	  {
	    state++;
	    
	  }
	
	break;
	
      case 5:  //Get the CRCH byte.
	MessageCRC16H=ch;
	state++;
	
	break;
	
      case 6:  //Get the CRCL byte.
	MessageCRC16L=ch;
	
	
	//We now have a complete packet. Verify the checksum and process it.
	//Convert individual bytes to one 16 bit checksum.
	MessageCRC16=((uint16)MessageCRC16H<<8)|(uint16)MessageCRC16L;
	
	//If the checksum passes, process the message.
	ReceivedCRC16=CRC16(MessageBuffer,MessageLength+MESSAGE_HEADER_LEN);
	if (MessageCRC16==ReceivedCRC16){
	  ReceiveIMUMessage(pIMUSensors,MessageType,MessageBuffer,
			    MessageLength);
	  got_message = MessageType;
	}else{
	  printf("CRC checksum error!\n");
	}
	
	//Regardless whether the packet was good or bad, it's time to go look for another one.
	state=0;  
	MessageIndex=0;
	
	//cout<<"Message received and processed"<<endl;
	break;
	
      } //Switch
    
   
    i++;
  } //while bytes available
  
  // record number of chars gotten through
  parsed = i;

  return got_message;

} //ProcIMUSerial


void ReceiveIMUMessage(IMUSensors_t* pIMUSensors,uint8 MessageType,uint8* pMsgBuf,uint8 MessageLength)
//!Process a complete IMU Message.
//! \param pIMUSensors pointer to the IMUSensors structure.
//! \param MessageType is enumerated by the IMUMessageTypes enumeration in IMUExternalTypes.h
//! \param pMsgBuf is a pointer to the message buffer
//! \param MessageLength
{
	sint16 vs1,vs2,vs3;
	uint16 v1,v2,v3;
	uint8 idx,seqnum;
	float32 tmpf;
	
	//	cout<<"IMU message type:"<<(int)MessageType<<endl;

	switch (MessageType)
	{
		//High speed serial packet
		case HS_SERIAL_IMU_MSG:

				//Get the gyro data
			Unstuff3VarsS16(pMsgBuf+4,&vs1,&vs2,&vs3);
			pIMUSensors->Gyro[X_AXIS]=vs1/CONV_GYRO_RESOLUTION; 
			pIMUSensors->Gyro[Y_AXIS]=vs2/CONV_GYRO_RESOLUTION; 
			pIMUSensors->Gyro[Z_AXIS]=vs3/CONV_GYRO_RESOLUTION;

			//Get the accelerometer data
			Unstuff3VarsS16(pMsgBuf+10,&vs1,&vs2,&vs3);
			pIMUSensors->Accel[X_AXIS]=vs1/CONV_ACCEL_RESOLUTION;
			pIMUSensors->Accel[Y_AXIS]=vs2/CONV_ACCEL_RESOLUTION;
			pIMUSensors->Accel[Z_AXIS]=vs3/CONV_ACCEL_RESOLUTION;

			//Get the timer ticks since PPS
			pIMUSensors->TimerTicksSincePPS=
				((uint32)pMsgBuf[16])<<24 |
				((uint32)pMsgBuf[17])<<16 |
				((uint32)pMsgBuf[18])<<8 |
				((uint32)pMsgBuf[19]);

			//Calculate the time since PPS
			pIMUSensors->TimeSincePPS_sec=(double)pIMUSensors->TimerTicksSincePPS/((double)TIMER_FREQUENCY_HZ);

			//Get the PPS counter
			pIMUSensors->PPSCounter=pMsgBuf[20];

			//Get the sequence number
			pIMUSensors->SequenceNumber=pMsgBuf[21];
				
			//Flag that the UI needs to be updated.
			//FlagUIUpdate();

		break;
		
		//Receive settings message
		case SETTINGS_IMU_MSG:
			pIMUSensors->IMUConfig.CommsMode=pMsgBuf[4];		
			pIMUSensors->IMUConfig.SampleMode=pMsgBuf[5];
			pIMUSensors->IMUConfig.OversampleRatio= ((uint16)pMsgBuf[6] << 8) | (uint16)pMsgBuf[7];
			pIMUSensors->IMUConfig.SampleRateHz=1000000.0/dec_uint32be(&pMsgBuf[8]);								
			//FlagConfigUIUpdate();
		break;

		case SERIALNUMCONFIG_IMU_MSG:
			idx=4;
			pIMUSensors->IMUConfig.SerialNum=(uint16)pMsgBuf[idx+0]<<8 | (uint16)(pMsgBuf[idx+1]);
			pIMUSensors->IMUConfig.EEPROMFormatVersion=pMsgBuf[idx+2];
			pIMUSensors->IMUConfig.HardwareRevisionMajor=pMsgBuf[idx+3];
			pIMUSensors->IMUConfig.HardwareRevisionMinor=pMsgBuf[idx+4];
			pIMUSensors->IMUConfig.ConfigNumberH=pMsgBuf[idx+5];
			pIMUSensors->IMUConfig.ConfigNumber1=pMsgBuf[idx+6];
			pIMUSensors->IMUConfig.ConfigNumberL=pMsgBuf[idx+7];
			//FlagConfigUIUpdate();
		break;

		case MFRCALDATE_IMU_MSG:
			idx=4;
			pIMUSensors->IMUConfig.MfrMonth=pMsgBuf[idx+0];
			pIMUSensors->IMUConfig.MfrDay=pMsgBuf[idx+1];
			pIMUSensors->IMUConfig.MfrYear=(uint16)pMsgBuf[idx+2]<<8 | (uint16)(pMsgBuf[idx+3]);
			pIMUSensors->IMUConfig.CalMonth=pMsgBuf[idx+4];
			pIMUSensors->IMUConfig.CalDay=pMsgBuf[idx+5];
			pIMUSensors->IMUConfig.CalYear=(uint16)pMsgBuf[idx+6]<<8 | (uint16)(pMsgBuf[idx+7]);
			////FlagConfigUIUpdate();
		break;

		case  SWVERSION_IMU_MSG:
			idx=4;
			pIMUSensors->IMUConfig.SWVersionMajor=pMsgBuf[idx+0];
			pIMUSensors->IMUConfig.SWVersionMinor=pMsgBuf[idx+1];
			pIMUSensors->IMUConfig.SWVersionSub=pMsgBuf[idx+2];
			pIMUSensors->IMUConfig.SWReleaseFlag=pMsgBuf[idx+3];
			pIMUSensors->IMUConfig.SWMonth=pMsgBuf[idx+4];
			pIMUSensors->IMUConfig.SWDay=pMsgBuf[idx+5];
			pIMUSensors->IMUConfig.SWYear=(uint16)pMsgBuf[idx+6]<<8 | (uint16)(pMsgBuf[idx+7]);
			//FlagConfigUIUpdate();
		break;

		case RAWGYRO_IMU_MSG:  
			Unstuff3VarsSeqNum((uint8*)&pMsgBuf[4],&v1,&v2,&v3,&seqnum);
			pIMUSensors->Rawgyro[X_AXIS]=v1*COUNTS_TO_VOLTS_AD16;
			pIMUSensors->Rawgyro[Y_AXIS]=v2*COUNTS_TO_VOLTS_AD16;
			pIMUSensors->Rawgyro[Z_AXIS]=v3*COUNTS_TO_VOLTS_AD16;
		break;								

		case RAWACCEL_IMU_MSG:
			Unstuff3VarsSeqNum((uint8*)&pMsgBuf[4],&v1,&v2,&v3,&seqnum);
			pIMUSensors->Rawaccel[X_AXIS]=v1*COUNTS_TO_VOLTS_AD16;
			pIMUSensors->Rawaccel[Y_AXIS]=v2*COUNTS_TO_VOLTS_AD16;
			pIMUSensors->Rawaccel[Z_AXIS]=v3*COUNTS_TO_VOLTS_AD16;
		break;

		case RAWGYROTEMPX_IMU_MSG:
			tmpf=dec_floatbe((uint8*)&pMsgBuf[4]);
			seqnum=pMsgBuf[8];
			pIMUSensors->Rawgyrotemp[X_AXIS]=tmpf;
//			pIMUSensors->GyrotempX=GetGyroTemperature(IMUSensors[NodeAddress].RawgyrotempX);

		break;
			
		case RAWGYROTEMPY_IMU_MSG:
			tmpf=dec_floatbe((uint8*)&pMsgBuf[4]);			
			pIMUSensors->Rawgyrotemp[Y_AXIS]=tmpf;
//			pIMUSensors->GyrotempY=GetGyroTemperature(IMUSensors[NodeAddress].RawgyrotempY);
		break;

		case RAWGYROTEMPZ_IMU_MSG:
			tmpf=dec_floatbe((uint8*)&pMsgBuf[4]);			
			pIMUSensors->Rawgyrotemp[Z_AXIS]=tmpf;
//			pIMUSensors->GyrotempZ=GetGyroTemperature(IMUSensors[NodeAddress].RawgyrotempZ);
		break;

				
	} //switch

}


void SendIMUSettingsSerialMessage(float32 SampleRateHz,uint16 OversampleRatio,uint8 CommsMode, uint8 SampleMode)
//!Send the IMU Settings serial message.
{
	uint8 Msg[8];  //Buffer for the message body.
	uint32 SamplePeriod; //microsec
	uint8 idx=0;

	SamplePeriod=(uint32)(1000000.0/SampleRateHz);
	
	//Form the message
	Msg[idx++]=CommsMode;
	Msg[idx++]=SampleMode;
	Msg[idx++]=(uint8)(OversampleRatio>>8);
	Msg[idx++]=(uint8)(OversampleRatio & 0xFF);

	//Encode the sample period as uint32 big endian.
	enc_uint32be(SamplePeriod, Msg+4);

	//Send the settings message.
	TransmitIMUSerialMessage(SET_SETTINGS_IMU_MSG,Msg,8);

}

void RequestIMUConfigSerialMessage(void)
//!Send the IMU Request Config serial message.
{
	uint8 Msg[8];  //Buffer for the message body.
	
	//Form the message
	Msg[0]=0;


	//Send the req config message.
	TransmitIMUSerialMessage(REQ_CONFIG_IMU_MSG,Msg,1);

}

void TransmitIMUSerialMessage(uint8 MessageType, uint8* pMessageBody,uint8 len)
//!Packetize and send a generic IMU serial message.
//! \param MessageType is enumerated in IMUExternalTypes.h
//! \param pMessageBuffer is a pointer to the message body
//! \param len is the message body length.
{
	uint8 MessageBuffer[MAX_MESSAGE_LEN];  //Buffer for the complete message including header and CRCs.
	uint8 idx=0;
	uint16 MessageCRC;
	uint8 i;

	if (len>8)  //Sanity check on message size (all messages to the IMU are 8 bytes or less).
		return;

	MessageBuffer[idx++]=SYNC_SER_0;
	MessageBuffer[idx++]=SYNC_SER_1;
	MessageBuffer[idx++]=MessageType;
	MessageBuffer[idx++]=len;

	//Copy the message body.
	memcpy(MessageBuffer+idx,pMessageBody,len);
	MessageCRC=CRC16(MessageBuffer,len+MESSAGE_HEADER_LEN);

	idx+=len;
	//Stuff the CRC
	MessageBuffer[idx++]=(uint8)(MessageCRC>>8);	//High byte
	MessageBuffer[idx++]=(uint8)(MessageCRC&0xFF);	//Low byte

	/* Original Crista serial code
	//Transmit the data
	for (i=0;i<idx;++i)
		PutchSerial(MessageBuffer[i]);

	//Flush the data out to the serial port.
	FlushSerial();
	*/

	// transmit buffer directly
	//serial_write(IMU_COM, (char*)MessageBuffer, idx);
		     
}

void SendIMUEnableBootloaderSerialMessage(void)
//!Send the message to enable the bootloader mode (danger!)
{
	uint8 Msg[8];  //Buffer for the message body.
	uint8 idx=0;
	
	//Form the message
	Msg[idx++]=0x10;
	Msg[idx++]=0x11;
	Msg[idx++]=0x12;
	Msg[idx++]=0x13;
	Msg[idx++]=0x14;
	Msg[idx++]=0x15;
	Msg[idx++]=0x16;
	Msg[idx++]=0x17;

	//Send the message.
	TransmitIMUSerialMessage(ENABLE_BOOTLOADER_IMU_MSG,Msg,8);

}


void Unstuff3VarsSeqNum(uint8* MessageData,uint16 *v1, uint16 *v2, uint16 *v3,uint8 *seqnum)
//!Unstuffs 3 uint16 variables from an 8 byte message format.
{
	uint8 i=0,tmp1,tmp2;
	//Endian independent code

	tmp1=MessageData[i++]; //MSByte
	tmp2=MessageData[i++]; //LSByte
	*v1=(uint16)((uint16)tmp1<<8 | tmp2);

	tmp1=MessageData[i++]; //MSByte
	tmp2=MessageData[i++]; //LSByte
	*v2=(uint16)((uint16)tmp1<<8 | tmp2);

	tmp1=MessageData[i++]; //MSByte
	tmp2=MessageData[i++]; //LSByte
	*v3=(uint16)((uint16)tmp1<<8 | tmp2);
	
	//Get sequence number.
	*seqnum=MessageData[i];
}// UnstuffCAN3VarsSeqNum

