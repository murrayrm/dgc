/*! \file
	\brief Enumerated indices for different types of arrays.

	This file is used to define names for the different axis in several three
	dimensional systems, including LLA (latitude, longitude, altitude), XYZ,
	Euler angles (roll, pitch, yaw), NED (North, East, Down) and SWU (South,
	West, Up).
*/

///////////////////////////////////
// This file provided by:        //
// Cloud Cap Technology, Inc.    //
// PO Box 1500                   //
// No. 8 Fourth St.              //
// Hood River, OR, 97031         //
// +1-541-387-2120    (voice)    //
// +1-541-387-2030    (fax)      //
// vaglient@gorge.net (e-mail)   //
// http://www.gorge.net/cloudcap //
///////////////////////////////////

#ifndef _INDICES_H
#define _INDICES_H

// Indices

/*!	Position indices for LLA (Latitude, Longitude, Altitude) */
enum PosIndices
{
	LAT,	//!< Latitude
	LON,	//!< Longitude
	ALT,	//!< Altitude
	NPOS	//!< Number of position indices
};

/*!	Cartesian coordinate indices for XYZ */
enum XYZIndices
{
	X_AXIS,	//!< X axis
	Y_AXIS,	//!< Y axis
	Z_AXIS,	//!< Z axis
	N3D	  	//!< Number of XYZ indices
};

/*!	Euler angle indices */
enum EulerIndices
{
	ROLL_AXIS,	//!< Roll axis
	PITCH_AXIS,	//!< Pitch axis
	YAW_AXIS,	//!< Yaw axis
	NAXIS		//!< Number of euler angles
};

enum AltEulerIndices
{
	PHI,	//!< Roll axis
	THETA,	//!< Pitch axis
	PSI	//!< Yaw axis
};

/*!	Direction indices for NED (North, East, Down) */
enum NEDIndices
{
	NORTH,	//!< North direction
	EAST,	//!< East direction
	DOWN,	//!< Down direction
	NDIR	//!< Number of directions
};

/*!	Inverted direction indices for SWU (South, West, Up) */
enum SWUIndices
{
	SOUTH,	//!< South direction
	WEST,	//!< West direction
	UP,		//!< Up direction
	NDIRINV	//!< Number of directions
};

#endif

