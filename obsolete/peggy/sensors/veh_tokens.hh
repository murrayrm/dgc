// veh_tokens.h
// Description:  This file contains the token lookup table used to parse
//               messages coming from the embedded box.


#ifndef __VEH_TOKENS_H__
#define __VEH_TOKENS_H__
					
//Define parsing tokens
#define NUM_TOKENS		5
#define TOKEN_NUMBER	0
#define TOKEN_START		1
#define TOKEN_END		2
#define TOKEN_DATA		3
#define TOKEN_LF		4

char token_table[] = {  TOKEN_DATA,    //0
						TOKEN_DATA,    //1
						TOKEN_DATA,    //2
						TOKEN_DATA,    //3
						TOKEN_DATA,    //4
						TOKEN_DATA,    //5
						TOKEN_DATA,    //6
						TOKEN_DATA,    //7
						TOKEN_DATA,    //8
						TOKEN_DATA,    //9
						TOKEN_LF,      //10 (Line Feed)
						TOKEN_DATA,    //11
						TOKEN_DATA,    //12
						TOKEN_DATA,    //13
						TOKEN_DATA,    //14
						TOKEN_DATA,    //15
						TOKEN_DATA,    //16
						TOKEN_DATA,    //17
						TOKEN_DATA,    //18
						TOKEN_DATA,    //19
						TOKEN_DATA,    //20
						TOKEN_DATA,    //21
						TOKEN_DATA,    //22
						TOKEN_DATA,    //23
						TOKEN_DATA,    //24
						TOKEN_DATA,    //25
						TOKEN_DATA,    //26
						TOKEN_DATA,    //27
						TOKEN_DATA,    //28
						TOKEN_DATA,    //29
						TOKEN_DATA,    //30
						TOKEN_DATA,    //31
						TOKEN_DATA,    //32
						TOKEN_DATA,    //33
						TOKEN_DATA,    //34
						TOKEN_DATA,    //35
						TOKEN_DATA,    //36
						TOKEN_DATA,    //37
						TOKEN_DATA,    //38
						TOKEN_DATA,    //39
						TOKEN_DATA,    //40
						TOKEN_DATA,    //41
						TOKEN_DATA,    //42
						TOKEN_DATA,    //43
						TOKEN_DATA,    //44
						TOKEN_DATA,    //45
						TOKEN_NUMBER,  //46  (.)
						TOKEN_DATA,    //47
						TOKEN_NUMBER,  //48  (0)
						TOKEN_NUMBER,  //49  (1)
						TOKEN_NUMBER,  //50  (2)
						TOKEN_NUMBER,  //51  (3)
						TOKEN_NUMBER,  //52  (4)
						TOKEN_NUMBER,  //53  (5)
						TOKEN_NUMBER,  //54  (6)
						TOKEN_NUMBER,  //55  (7)
						TOKEN_NUMBER,  //56  (8)
						TOKEN_NUMBER,  //57  (9)
						TOKEN_DATA,    //58  (:)
						TOKEN_DATA,    //59  (;)
						TOKEN_DATA,    //60  (<)
						TOKEN_DATA,    //61  (=)
						TOKEN_DATA,    //62  (>)
						TOKEN_DATA,    //63  (?)
						TOKEN_DATA,    //64  (@)
						TOKEN_DATA,	   //65  (A)
						TOKEN_DATA,    //66  (B)
						TOKEN_DATA,    //67  (C)
						TOKEN_DATA,    //68  (D)
						TOKEN_END,     //69  (E)
						TOKEN_DATA,    //70  (F)
						TOKEN_DATA,    //71  (G)
						TOKEN_DATA,    //72  (H)
						TOKEN_DATA,    //73  (I)
						TOKEN_DATA,    //74  (J)
						TOKEN_DATA,    //75  (K)
						TOKEN_DATA,    //76  (L)
						TOKEN_DATA,    //77  (M)
						TOKEN_DATA,    //78  (N)
						TOKEN_DATA,    //79  (O)
						TOKEN_DATA,    //80  (P)
						TOKEN_DATA,    //81  (Q)
						TOKEN_DATA,    //82  (R)
						TOKEN_START,   //83  (S)
						TOKEN_DATA,    //84  (T)
						TOKEN_DATA,    //85  (U)
						TOKEN_DATA,    //86  (V)
						TOKEN_DATA,    //87  (W)
						TOKEN_DATA,    //88  (X)
						TOKEN_DATA,    //89  (Y)
						TOKEN_DATA,    //90  (Z)
						TOKEN_DATA,    //91  ([)
						TOKEN_DATA,    //92  (\)
						TOKEN_DATA,    //93  (])
						TOKEN_DATA,    //94  (^)
						TOKEN_DATA,    //95  (_)
						TOKEN_DATA,    //96  (')
						TOKEN_DATA,    //97  (a)
						TOKEN_DATA,    //98  (b)
						TOKEN_DATA,    //99  (c)
						TOKEN_DATA,    //100 (d)
						TOKEN_DATA,    //101 (e)
						TOKEN_DATA,    //102 (f)
						TOKEN_DATA,    //103 (g)
						TOKEN_DATA,    //104 (h)
						TOKEN_DATA,    //105 (i)
						TOKEN_DATA,    //106 (j)
						TOKEN_DATA,    //107 (k)
						TOKEN_DATA,    //108 (l)
						TOKEN_DATA,    //109 (m)
						TOKEN_DATA,    //110 (n)
						TOKEN_DATA,    //111 (o)
						TOKEN_DATA,    //112 (p)
						TOKEN_DATA,    //113 (q)
						TOKEN_DATA,    //114 (r)
						TOKEN_DATA,    //115 (s)
						TOKEN_DATA,    //116 (t)
						TOKEN_DATA,    //117 (u)
						TOKEN_DATA,    //118 (v)
						TOKEN_DATA,    //119 (w)
						TOKEN_DATA,    //120 (x)
						TOKEN_DATA,    //121 (y)
						TOKEN_DATA,    //122 (z)
						TOKEN_DATA,		//123
						TOKEN_DATA,		//124
						TOKEN_DATA,		//125
						TOKEN_DATA,		//126
						TOKEN_DATA,		//127
						TOKEN_DATA,		//128
						TOKEN_DATA,		//129
						TOKEN_DATA,		//130
						TOKEN_DATA,		//131
						TOKEN_DATA,		//132
						TOKEN_DATA,		//133
						TOKEN_DATA,		//134
						TOKEN_DATA,		//135
						TOKEN_DATA,		//136
						TOKEN_DATA,		//137
						TOKEN_DATA,		//138
						TOKEN_DATA,		//139
						TOKEN_DATA,		//140
						TOKEN_DATA,		//141
						TOKEN_DATA,		//142
						TOKEN_DATA,		//143
						TOKEN_DATA,		//144
						TOKEN_DATA,		//145
						TOKEN_DATA,		//146
						TOKEN_DATA,		//147
						TOKEN_DATA,		//148
						TOKEN_DATA,		//149
						TOKEN_DATA,		//150
						TOKEN_DATA,		//151
						TOKEN_DATA,		//152
						TOKEN_DATA,		//153
						TOKEN_DATA,		//154
						TOKEN_DATA,		//155
						TOKEN_DATA,		//156
						TOKEN_DATA,		//157
						TOKEN_DATA,		//158
						TOKEN_DATA,		//159
						TOKEN_DATA,		//160
						TOKEN_DATA,		//161
						TOKEN_DATA,		//162
						TOKEN_DATA,		//163
						TOKEN_DATA,		//164
						TOKEN_DATA,		//165
						TOKEN_DATA,		//166
						TOKEN_DATA,		//167
						TOKEN_DATA,		//168
						TOKEN_DATA,		//169
						TOKEN_DATA,		//170
						TOKEN_DATA,		//171
						TOKEN_DATA,		//172
						TOKEN_DATA,		//173
						TOKEN_DATA,		//174
						TOKEN_DATA,		//175
						TOKEN_DATA,		//176
						TOKEN_DATA,		//177
						TOKEN_DATA,		//178
						TOKEN_DATA,		//179
						TOKEN_DATA,		//180
						TOKEN_DATA,		//181
						TOKEN_DATA,		//182
						TOKEN_DATA,		//183
						TOKEN_DATA,		//184
						TOKEN_DATA,		//185
						TOKEN_DATA,		//186
						TOKEN_DATA,		//187
						TOKEN_DATA,		//188
						TOKEN_DATA,		//189
						TOKEN_DATA,		//190
						TOKEN_DATA,		//191
						TOKEN_DATA,		//192
						TOKEN_DATA,		//193
						TOKEN_DATA,		//194
						TOKEN_DATA,		//195
						TOKEN_DATA,		//196
						TOKEN_DATA,		//197
						TOKEN_DATA,		//198
						TOKEN_DATA,		//199
						TOKEN_DATA,		//200
						TOKEN_DATA,		//201
						TOKEN_DATA,		//202
						TOKEN_DATA,		//203
						TOKEN_DATA,		//204
						TOKEN_DATA,		//205
						TOKEN_DATA,		//206
						TOKEN_DATA,		//207
						TOKEN_DATA,		//208
						TOKEN_DATA,		//209
						TOKEN_DATA,		//210
						TOKEN_DATA,		//211
						TOKEN_DATA,		//212
						TOKEN_DATA,		//213
						TOKEN_DATA,		//214
						TOKEN_DATA,		//215
						TOKEN_DATA,		//216
						TOKEN_DATA,		//217
						TOKEN_DATA,		//218
						TOKEN_DATA,		//219
						TOKEN_DATA,		//220
						TOKEN_DATA,		//221
						TOKEN_DATA,		//222
						TOKEN_DATA,		//223
						TOKEN_DATA,		//224
						TOKEN_DATA,		//225
						TOKEN_DATA,		//226
						TOKEN_DATA,		//227
						TOKEN_DATA,		//228
						TOKEN_DATA,		//229
						TOKEN_DATA,		//230
						TOKEN_DATA,		//231
						TOKEN_DATA,		//232
						TOKEN_DATA,		//233
						TOKEN_DATA,		//234
						TOKEN_DATA,		//235
						TOKEN_DATA,		//236
						TOKEN_DATA,		//237
						TOKEN_DATA,		//238
						TOKEN_DATA,		//239
						TOKEN_DATA,		//240
						TOKEN_DATA,		//241
						TOKEN_DATA,		//242
						TOKEN_DATA,		//243
						TOKEN_DATA,		//244
						TOKEN_DATA,		//245
						TOKEN_DATA,		//246
						TOKEN_DATA,		//247
						TOKEN_DATA,		//248
						TOKEN_DATA,		//249
						TOKEN_DATA,		//250
						TOKEN_DATA,		//251
						TOKEN_DATA,		//252
						TOKEN_DATA,		//253
						TOKEN_DATA,		//254
						TOKEN_DATA		//255
						};


#endif
