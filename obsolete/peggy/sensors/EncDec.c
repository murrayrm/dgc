/*! \file
	\brief  Encode and decode binary bytes into machine types.
	
	  	Useful for translating between network order and endian dependent types.

*/

//!Encdec.c
//!Encode and decode binary bytes into machine types.
//! 


#include "EncDec.h"
#include "Types.h"

//Encode floats
union convert_float {
	float f;
	uint32 i;
};

#ifdef junk
size_t enc_floatle(const float f, char *dst)
//!Encode float little-endian
{
	union convert_float c;
	c.f = f;
	return enc_uint32le(c.i, dst);

}
#endif

size_t enc_floatbe(const float f, char *dst)
//!Encode float big-endian
{
	union convert_float c;
	c.f = f;
	return enc_uint32be(c.i, (unsigned char*)dst);
}

#ifdef junk
size_t enc_uint32le(uint32 i, unsigned char *dst)
//!Encode uint32 little-endian
{
	dst[0] = i & 0xFF;
	dst[1] = (i >> 8) & 0xFF;
	dst[2] = (i >> 16) & 0xFF;
	dst[3] = (i >> 24) & 0xFF;
	return 4;
}
#endif

size_t enc_uint32be(uint32 i, unsigned char *dst)
//!Encode uint32 big-endian
{
	dst[0] = (uint8)((i >> 24) & 0xFF);
	dst[1] = (uint8)((i >> 16) & 0xFF);
	dst[2] = (uint8)((i >> 8) & 0xFF);
	dst[3] = (uint8)(i & 0xFF);
	return 4;
}

#ifdef junk
float dec_floatle(const unsigned char *src)
//!Decode float little-endian
{
	union convert_float c;
	c.i = dec_uint32le(src);
	return c.f;
}
#endif //junk

float dec_floatbe(unsigned char *src)
//!Decode float big-endian
{
	union convert_float c;
	c.i = dec_uint32be(src);
	return c.f;
}

uint32 dec_uint32be(const unsigned char *src)
//!Decode uint32 big-endian
{
	return ((uint32)src[0] << 24) | ((uint32)src[1] << 16) |
           ((uint32)src[2] << 8) | (uint32)src[3];
}


//union convert_sint16 {
//	sint16 s16;
//	uint;
//};

sint16 dec_sint16be(const unsigned char *src)
//!Decode sint16 big-endian
{
//	union convert_sint16 c;
	return (sint16)(*(src)|(sint16)(*(src+1)<<8));
}

#ifdef junk
uint32 dec_uint32le(const unsigned char *src)
//!Decode uint32 little-endian
{
	return src[0] | ((unsigned)src[1] << 8) |
           ((unsigned)src[2] << 16) | ((unsigned)src[3] << 24);
}
#endif //junk

void Unstuff3VarsS16(uint8* MessageData,sint16 *v1, sint16 *v2, sint16 *v3)
//!Unstuffs 3 sint16 variables from a 6 byte message format.
{
	uint8 i=0,tmp1,tmp2;
	//Endian independent code

	tmp1=MessageData[i++]; //MSByte
	tmp2=MessageData[i++]; //LSByte
	*v1=(sint16)((sint16)tmp1<<8 | tmp2);

	tmp1=MessageData[i++]; //MSByte
	tmp2=MessageData[i++]; //LSByte
	*v2=(sint16)((sint16)tmp1<<8 | tmp2);

	tmp1=MessageData[i++]; //MSByte
	tmp2=MessageData[i++]; //LSByte
	*v3=(sint16)((sint16)tmp1<<8 | tmp2);
		
}
