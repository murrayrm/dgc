//CRC.h

///////////////////////////////////
// This file provided by:        //
// Cloud Cap Technology, Inc.    //
// PO Box 1500                   //
// No. 8 Fourth St.              //
// Hood River, OR, 97031         //
// +1-541-387-2120    (voice)    //
// +1-541-387-2030    (fax)      //
// vaglient@gorge.net (e-mail)   //
// http://www.gorge.net/cloudcap //
///////////////////////////////////

#ifndef CRC_H
#define CRC_H

#include "Types.h"

#if defined(__cplusplus)
extern "C"
{
#endif
uint16 CRC16(const uint8* pBuf, uint32 len);
#if defined(__cplusplus)
}
#endif
void CRC16_Init( void );
void CRC16_Update( unsigned char val );
uint16 GetCRC16Value(void);

#endif // CRC_H
