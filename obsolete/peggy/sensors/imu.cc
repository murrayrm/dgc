//*******************************************
//  DGC Drivers for Crista IMU
//     Haomiao Huang
//     July 24, 2004
//
//  These drivers provide a device-specific
//  serial interface to the crista imu
//  and do not use the generalized dgc serial
//  drivers.
//
// Currently the code uses select() to do blocking
// serial reads to get data.  An interrupt-driven
// version is planned for the future.
//*******************************************

// Revision HIstory:
// Created 7/24/2004 Haomiao Huang

#include "imu.hh"
using namespace std;

// global variable declarations
int imu_device;  // file handler for imu serial port
//struct serial_buffer parse_buffer; // buffer for storing chars to parse
int lock_flag = 0;
int imu_com;

// imu_open(int com)
//**********************************************************
// This function opens a com port to the IMU and initializeds
// the file pointer to that serial port, as well as installing
// the necessary input handler to parse the incoming IMU packets.
// Returns -1 if an error occurs, 0 otherwise.. Serial settings for 
// IMU are 115200 8N1 with no flow control of any kind
int imu_open(int com)
{
#ifndef SERIAL2
  struct termios term; // serial port control settings struct
  char SerialPathName[40];
  sprintf(SerialPathName, "%s%d", SerialPath, com);

  imu_device = open(SerialPathName, O_RDWR);
  
  // error check on serial open
  if (imu_device < 0) {
    printf("imu_open: Cannot open serial port: %d\n", com);
    return -1;
  }
  
  // set serial port options
  memset(&term, '\0', sizeof(term));
  term.c_iflag = IGNBRK;
  term.c_oflag = 0;
  term.c_cflag = CLOCAL | CREAD | CS8 | IMU_BAUD;
  term.c_lflag = 0;

  term.c_cc[VMIN] = 1; // read at least 1 char before returning

  // set the flags and check for errors
  if (tcsetattr(imu_device, TCSANOW, &term) < 0){
    cout<<"imu_open: tcsetattr error"<<endl;
    return -1;
  }

  tcflush(imu_device, TCIOFLUSH);

  return 0;

#else
  imu_com = com;
  return serial_open(com, IMU_BAUD);
#endif

}



// int imu_initialize(int rate, int oversample, int messagetype, int messagecontent)
//**************************************************************************************
// this is used to initialize the IMU to the desired settings.  The function
// prepares the desired command sequence, transmits it to the IMU, then reads it back
// to make sure that the settings are correct. See Crista IMU documentation for details on settings.
// Returns -1 if an error occurs, otherwise 0.
int imu_initialize(int rate, int oversample, int messagetype, int messagecontent);

#define IMU_TIMEOUT 40000

// int imu_read_data(imu_data& read_data)
// ************************************************************************
// This function performs a blocking read on the file handler pointed to by 
// the argument imu.  The function waits until an imu high speed serial packet
// has been successfully read, parses the packet, then stores the read data in the 
// read_data struct.  If an error occurs, returns -1, otherwise returns 0.   
int imu_read_data(imu_data& read_data)
{
  char read_buffer[IMU_HS_LEN]; // buffer to be read in
  char last_buffer[IMU_HS_LEN]; 
  int bytes_read = 0;
  int total_bytes_read = 0;
  int parsed = 0;
  IMUSensors_t imu_raw;
  int get_message = -1;
#ifdef SERIAL2
  struct Timeval timeout(0, IMU_TIMEOUT);
#endif

  do {
#ifndef SERIAL2
    bytes_read = read(imu_device, read_buffer, IMU_MSG_LEN);
#else
    bytes_read = serial_read_blocking(imu_com, read_buffer, IMU_HS_LEN, 
				      timeout);
#endif 

    get_message = imu_parse_char(&imu_raw, read_buffer, bytes_read, parsed);
    //cout<<"Parsed: "<<parsed<<endl;
    
    if (parsed < bytes_read){
      // parse remaining chars, stored for later
      imu_parse_char(&imu_raw, &read_buffer[parsed], bytes_read - parsed, 
		     parsed);       
    }
  }while(get_message < 0);

  // store data into read_data
  read_data.xdd = imu_raw.Accel[X_index];
  read_data.ydd = imu_raw.Accel[Y_index];
  read_data.zdd = imu_raw.Accel[Z_index]; 
  read_data.dtx = imu_raw.Gyro[X_index];
  read_data.dty = imu_raw.Gyro[Y_index];
  read_data.dtz = imu_raw.Gyro[Z_index];
  read_data.dt = imu_raw.TimeSincePPS_sec;
  read_data.counter = imu_raw.SequenceNumber;

  gettimeofday(&read_data.time_read, NULL);

  return 0;
}



// int imu_close(int& imu_device)
// ************************************************************************
// Closes the serial port being used by the file handler imu_device and 
// sets imu_device to 0.  Returns -1 if an error occurs, otherwise 0
int imu_close()
{
#ifndef SERIAL2
  if (close(imu_device) < 0){
    cout<<"imu_close: cannot close imu"<<endl;
    return -1;
  }
#else
  serial_close(imu_com);
#endif

  return 0;
}

/*
// void imu_input_handler(int signal)
// *************************************************************
// Asynchronous serial input handler.  Is called upon serial 
// bytes being available, reads in a block of bytes and parses
// these bytes.  If a packet is completed, then this packet is 
// decoded and the imu_read routine is notified.
void imu_input_handler(int signal)
{
  char buffer[MAX_BUFF_LEN];
  struct timeval timeout={0,0}; // no wait on select
  fd_set rfds; // read file descriptor set
  int bytes_read; // bytes read from serial port

  FD_ZERO(&rfds); // zero file descriptor set 
  FD_SET(imu_device, &rfds); // set imu_device into file descripto set
  
  // select device to check on something actually happening
  select(imu_device, &rfds, NULL, NULL, &timeout);
  if (FD_ISSET(imu_device, &rfds)) {
    bytes_read = read(imu_device, buffer, MAX_BUFF_LEN); // read device
    
    if (bytes_read > 0){
      // Now store bytes just read into the parse buffer
      // If the buffer is empty, then create a new data array
      // otherwise append 
      if (parsebuffer.len == 0) {
	parsebuffer.data = (char *) malloc(bytes_read);
	memcpy(parsebuffer.data, buffer, bytes_read);
	parsebuffer.len = bytes_read;
      }else{
	parsebuffer.data = (char *) realloc(parsebuffer.data, 
					    parsebuffer.len + bytes_read);
	memcpy(parsebuffer.data + parsebuffer.len, buffer, bytes_read);
	parsebuffer.len += bytes_read;
      }
    } // if(bytes_read)
  } // if (FD_SET)

  // reset signal handler
  signal(signal, &imu_input_handler);
}
*/
    
