/*  rtparse.h */

#ifndef __PARSELOG_H__
#define __PARSELOG_H__

// data structure for vehicle data
// input data only (from vehicle)
// vehicle command data is elsewhere?
struct veh_data
{
  // vehicle data
  unsigned short steering;  // steering servo setting
  unsigned short throttle;  // throttle servo setting
  unsigned short aux;       // radio button input
  unsigned short engine_vel; // obvious
  unsigned short fr_vel;   // wheel velocities
  unsigned short fl_vel;
  unsigned short rr_vel;
  unsigned short rl_vel;

};

#define SECSPERMIN 60
#define ENG_TMR_PERIOD .000004 // 4 usecs
#define ENG_DIVIDER_CNT 1  
#define WHEEL_TMR_PERIOD .000016 // 16 usecs

void init_parser();
void act_reset();
void init_transition_table();

// actual parse function
void parse(char input, veh_data& thedata, int& update);

// action file handlers for parser
void act_nop();
void act_write_line();
void act_get_number();
void act_msg_0();
void act_msg_1();
void act_msg_2();
void act_msg_3();
void act_msg_4();
void act_msg_5();
void act_msg_6();
void act_msg_7();
void act_msg_8();
void act_msg_9();
void act_msg_10();
void act_msg_11();
void act_msg_12();
void act_msg_13();
void act_msg_14();
void act_msg_15();


#endif
