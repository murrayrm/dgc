//*******************************************
//  DGC Drivers for Crista IMU
//     Haomiao Huang
//     July 24, 2004
//
//  These drivers provide a device-specific
//  serial interface to the crista imu
//  and do not use the generalized dgc serial
//  drivers.
//*******************************************

// Revision HIstory:
// Created 7/24/2004 Haomiao Huang

#ifndef IMU_HH
#define IMU_HH

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <iostream>

// IMU specific includes here
#include "Types.h"
#include "IMUExternalTypes.h"
#include "IMUSerial.h"

#include "serialimu.hh"

// Constant definitions
#ifndef SERIAL2
#define SerialPath "/dev/ttyUSB"
#endif

#define IMU_BAUD    B115200
#define IMU_MSG_LEN 24

// index declarations for arrays from imu drivers
#define X_index     0  
#define Y_index     1
#define Z_index     2


// data structure definitions

// imu_data
// This structure is the basic struture used to share
// IMU data with other threads and anything that needs 
// it.
struct imu_data
{
  // note: imu readings are in body frame
  // imu accel readings
  double xdd;
  double ydd;
  double zdd;

  // imu delta-thetas (angular velocities)
  double dtx;
  double dty;
  double dtz;
  
  // time in seconds since last gps PPS signal
  double dt;
  
  // sequence counter - 0-255, used to make sure
  // no imu frames are being dropped
  uint8 counter;

  // time it was read
  struct timeval time_read;
};


// function headers

// imu_open(int com)
//**********************************************************
// This function opens a com port to the IMU and returns
// the file pointer to that com port; this file pointer is 
// then passed to other imu function calls to access that imu
// Returns -1 if an error occurs.
int imu_open(int com);


// imu_initialize(int imu_dev, int rate, int oversample, int messagetype, int messagecontent)
//**************************************************************************************
// this is used to initialize the IMU to the desired settings.  The function
// prepares the desired command sequence, transmits it to the IMU, then reads it back
// to make sure that the settings are correct. int imu_dev is the file identifier for transmitting
// to the serial port.  See Crista IMU documentation for details on settings.
// Returns -1 if an error occurs, otherwise 0.
int imu_initialize(int rate, int oversample, int messagetype, int messagecontent);


// int imu_read_data(int imu_dev, imu_data& read_data)
// ************************************************************************
// This function performs a blocking read on the file handler pointed to by 
// the argument imu.  The function waits until an imu high speed serial packet
// has been successfully read, parses the packet, then stores the read data in the 
// read_data struct.  If an error occurs, returns -1, otherwise returns 0.   
int imu_read_data(imu_data& read_data);

// int imu_close(int& imu_dev)
// ************************************************************************
// Closes the serial port being used by the file handler imu_dev and 
// sets imu_dev to 0.  Returns -1 if an error occurs, otherwise 0
int imu_close();

#endif
