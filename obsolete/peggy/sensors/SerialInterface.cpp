// Modified Crista code
// Gutted Crista code, put in calls to serial2 code instead
// Haomiao Huang
// 7/8/04

//!SerialInterface.cpp
//!Platform independent wrappers for platform dependent serial functions.
// Note the QextSerialPort code is somewhat platform independent (versions for Win32/Linux/other platforms exist) 
//Jon Becker
//Cloud Cap Technology 2003
///////////////////////////////////
// This file provided by:        //
// Cloud Cap Technology, Inc.    //
// PO Box 1500                   //
// No. 8 Fourth St.              //
// Hood River, OR, 97031         //
// +1-541-387-2120    (voice)    //
// +1-541-387-2030    (fax)      //
// jbecker@gorge.net (e-mail)    //
// http://www.uavautopilots.com  //
///////////////////////////////////

//Enable only one of the following.
// choose between crista defaults or dgc code
//#define QT_SERIAL_DRIVER_ENABLED
//#define CCT_SERIAL_DRIVER_ENABLED
#define DGC_SERIAL_ENABLED

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <math.h>

#include <conio.h>
#include <stdlib.h>
#include <malloc.h>

#include <stdio.h>
#include <process.h>
#include <string.h>


#ifdef QT_SERIAL_DRIVER_ENABLED
	#include "QextSerialPort\qextserialbase.h"
	#include "QextSerialPort\qextserialport.h"
#endif

#ifdef CCT_SERIAL_DRIVER_ENABLED
	#include "Win32Serial.h"
#endif

#ifdef DGC_SERIAL_ENABLED
#include "serial/serial2.hh"
#include "constants.hh"
#include "imu.hh"
#endif 

#include "Types.h"


//Serial port structure on COM1
#ifdef QT_SERIAL_DRIVER_ENABLED
	static QextSerialPort sp("COM1");
#endif
#ifdef CCT_SERIAL_DRIVER_ENABLED
	static CWin32Serial* SerialCCT;
#endif


int OpenSerial(void)
//! Platform dependent code to open a serial port.
//! returns the serial port status.
{
#ifdef QT_SERIAL_DRIVER_ENABLED
    sp.setBaudRate(BAUD115200);
    sp.setDataBits(DATA_8);
    sp.setFlowControl(FLOW_OFF);
    sp.setStopBits(STOP_1);
    sp.setParity(PAR_NONE);
	return (sp.open());
#elif defined CCT_SERIAL_DRIVER_ENABLED
	uint8 chan=0;
	uint32 baud=115200;
	uint8 parity=NOPARITY;
	uint8 data=8;
	uint32 QSize=32768;

	SerialCCT = new CWin32Serial;	// Create serial object
	if (!SerialCCT)
		return 0;
	if(SerialCCT->bOpen(chan + 1 , baud, parity, data, ONESTOPBIT, QSize, QSize))
		return 1;
	else
		return 0;

#elif defined DGC_SERIAL_ENABLED
	// use the serial command defined in serial2
	// constants are defined in imu.hh
	return(serial_open_advanced(imu_com, imu_baud, imu_options));
#else
	return 0;
#endif
}

void CloseSerial(void)
//! Closes the serial port.
{
#ifdef QT_SERIAL_DRIVER_ENABLED
	sp.close();
#endif
#ifdef CCT_SERIAL_DRIVER_ENABLED
	if (!SerialCCT)
		return;
	SerialCCT->Close();
	delete SerialCCT;
	SerialCCT = NULL;	
#endif
#ifdef DGC_SERIAL_ENABLED
	serial_close(imu_com);
#endif

}

bool BytesAvailSerial(void)
//! Returns whether there are bytes remaining in the incoming serial queue.
{
#ifdef QT_SERIAL_DRIVER_ENABLED
	return (sp.bytesWaiting()>0);
#elif defined CCT_SERIAL_DRIVER_ENABLED
//	if (SerialCCT->DataWaiting()>0)
//		return TRUE;
//	else
//		return FALSE;
	return TRUE;  //always return true, use read function which blocks.
#else
	return FALSE;
#endif
}

int GetchSerial(void)
//! Returns a byte from the serial buffer.
{
#ifdef QT_SERIAL_DRIVER_ENABLED
	return (sp.getch());
#elif defined CCT_SERIAL_DRIVER_ENABLED
	BYTE tmpbyte;
	DWORD bytecount;
	SerialCCT->bRead(&tmpbyte,1,&bytecount);
	if (bytecount!=1)
		return -1;
	else
		return (int)tmpbyte;
	
#else
	return 0;
#endif
}

void PutchSerial(int Byte)
//! Transmits a serial byte
{
#ifdef QT_SERIAL_DRIVER_ENABLED
	sp.putch(Byte);
#endif
#ifdef CCT_SERIAL_DRIVER_ENABLED
	BYTE tmpbyte;
	DWORD bytecount;
	tmpbyte=(BYTE)Byte;
	SerialCCT->bWrite(&tmpbyte,1,&bytecount);
#endif
}

void FlushSerial(void)
//! Flushes the serial output (transmits any pending characters).
{
#ifdef QT_SERIAL_DRIVER_ENABLED
	sp.flush();
#endif
#ifdef CCT_SERIAL_DRIVER_ENABLED
	//No equivalent.
#endif

}
