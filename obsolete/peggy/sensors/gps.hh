// ******************************************
//     Thales/Ashtech AC12 Sensor GPS
//     7/26/2004 Haomiao Huang
// 
// These are drivers for interfacing with the 
// AC12
// *****************************************

#ifndef GPS_HH
#define GPS_HH

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <iostream>

#include "LatLong.h"

// constant definitions and command definitions
#define GPS_BAUD B4800
#define GPS_TIME_OUT 1000000
#define GPS_DELAY_TIME 1000000
#define GPS_READ_BLOCK 64
#define SerialPath "/dev/ttyUSB"
#define MAX_MSG_LENGTH 255

// data structure for gps data
// will be used to pass gps data to kf
struct gps_data
{
  // note: all gps data are in nav frame
  // converted gps data
  double x; // northing
  double y; // easting
  double z; // altitude (note, z-down)

  double xd;
  double yd;
  double zd;

  double time;

  int pvt_valid; // validity of the packet

  int mode; // mode: 1 = differential
 
  int SV;  // number of satellites used

  // utc time of solution
  int utc_hour;
  int utc_min;
  double utc_sec;

  double latitude;
  double longitude;
  double altitude;
  
  double speed; // m/s
  double vert_vel; // vertical velocity in m/s
  double heading; // deg from north

  // dilutions of precision
  double pdop;
  double hdop;
  double vdop;
  double tdop;

  // time read
  struct timeval time_read;

};

// int gps_open(int com)
// ****************************************
// Opens the serial port of the gps for read/write
// operations.  Returns -1 if error occurs, otherwise
// 0.  The file descriptor of the gps serial port
// is a global variable
int gps_open(int com);

// int gps_initialize()
// **********************************************
// Initializes the gps unit to transmit data.  The settings
// needed are as follows:
// 1 PPS (1 ms) : default
// UTM messages from gps
// The sequence is done thus:
// 1. GPS is reset
// 2. All NMEA messages are turned off
// 3. UTM messages are enabled (to get x,y coordinates)
// 4. VTG messages are enabled (to get velocity)
int gps_initialize();

// int get_gps_acknowledge()
// *************************************************
// Reads a message from the GPS and tries to determine
// if it is an acknowledge message or not 
int get_gps_acknowledge();


// int gps_close()
// **************************************************
// Orders the gps to stop sending messages and closes
// the serial port.
int gps_close();

//int gps_readline(char* buffer)
//*********************************
// performs a blocking serial read on the gps serial port, looking for
// the \n character which delineate the end of a gps
// message string.  The buffer is filled with the string, and the function
// returns the size of the string.  Note: function assumes the passed buffer
// is much larger than any gps string it gets. Also, any data left in the 
// serial buffer after the carriage return is lost.  Basically, this 
// will work because we know that we're comanding the GPS to only
// send one line at a time  
int gps_readline(char* buffer);

// int gps_buffer_flush()
// *****************************************
// Does a large nonblocking serial read on the gps serial port
// to flush the bytes.  Normally tcflush is used for doing this,
// but for some reason tcflush is not working, so we're going to
// do this ghetto rig method instead. Return value is the number
// of bytes that were flushed
int gps_buffer_flush();


// int get_next_string(char* message, int length, char* new_string,
// int new_len, char* rest, int rest_len)
// **************************************************************
// Sets msg_start to the beginning of the message, then gives the 
// length up to the next comma.  Also sets a pointer to the rest of
// the string (after the comma) and that length;
void get_next_string(char*& msg_start, int& new_len, char*& rest, 
		     int& rest_len);

// int find_next_comma(char *message, int length)
// ***************************************************************
// Returns the index of the next comma in the sequence
int find_next_comma(char* message, int length);


// double str2num(char* number, int length)
// ****************************************
// Converts an ascii string of a given length
// into a double.
double str2num(char* number, int length);

// int gps_parse_line(gps_data& gpsdata, char* message, int length)
// *****************************************************************
// takes a string from the character parser and parses it into 
// a gps_data struct.  The message is in this format:
// $PASHR,POS,d1,d2,m1,m2,c1,m3,c2,f1,f2,f3,f4,f5,f6,f7,f8,f9,s*cc
int gps_parse_line(gps_data& gpsdata, char* message, int length);

// double get_deg(char* string, int strlen)
// *************************
// special function for dealing with longitude
// because it could be 3 digits or 2.  Picks off degree
// portion and adds minute portion.  Gets the degree portion
// by doing a modulus
double get_deg(char* string, int strlen);

// int gps_read(gps_data& gpsdata)
// ************************************************************
// does gps line reads and parses them until a valid pos message
// is found, then returns with the struct.  It returns even when
// there is no valid nav solution
int gps_read(gps_data& gpsdata);

#endif 
