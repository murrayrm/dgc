#ifndef ENCDEC_H
#define ENCDEC_H

//#include <pic18.h>
//#include <stdio.h>
//#include <stddef.h>
//#include "types.h"
#include "Types.h"

#ifndef size_t
	typedef	unsigned	size_t;		/* type yielded by sizeof */
#endif
size_t enc_floatle(const float f, char *dst);

#if defined(__cplusplus)
extern "C"
{
#endif
size_t enc_floatbe(const float f, char *dst);
#if defined(__cplusplus)
}
#endif 
size_t enc_uint32le(uint32 i, unsigned char *dst);

#if defined(__cplusplus)
extern "C"
{
#endif
size_t enc_uint32be(uint32 i, unsigned char *dst);
#if defined(__cplusplus)
}
#endif 
float dec_floatle(const unsigned char *src);
//float dec_floatbe(const unsigned char *src);


#if defined(__cplusplus)
extern "C"
{
#endif
uint32 dec_uint32be(const unsigned char *src);
#if defined(__cplusplus)
}
#endif 


uint32 dec_uint32le(const unsigned char *src);
sint16 dec_sint16be(const unsigned char *src);

#if defined(__cplusplus)
extern "C"
{
#endif
	float dec_floatbe(unsigned char *src);
#if defined(__cplusplus)
}
#endif 

#if defined(__cplusplus)
extern "C"
{
#endif
void Unstuff3VarsS16(uint8* MessageData,sint16 *v1, sint16 *v2, sint16 *v3);
#if defined(__cplusplus)
}
#endif 

#endif

