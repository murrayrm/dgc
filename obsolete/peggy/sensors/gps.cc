// ******************************************
//     Thales/Ashtech AC12 Sensor GPS
//     7/26/2004 Haomiao Huang
// 
// These are drivers for interfacing with the 
// AC12
// *****************************************

#include "gps.hh"
using namespace std;

// global declarations
int gps_device;

// command string definitions
#define GPS_RST_MSG "$PASHS,RST\r\n" // reset gps
#define GPS_RST_LEN 12
#define GPS_NMEA_OFF_MSG "$PASHS,NME,ALL,A,OFF\r\n" // turn off all messages
#define GPS_NMEA_OFF_LEN 22
#define GPS_POS_ON_MSG "$PASHS,NME,POS,A,ON,1\r\n" // start utm transmission at 1 hz
#define GPS_POS_ON_LEN 23
#define GPS_ACK_MSG "$PASHR,ACK*3D\r\n" // expected acknowledge message from GPS
#define GPS_ACK_LEN 15

struct timeval t_start;
struct timeval t_end;
double t_elapsed;

// int gps_open(int com)
// ****************************************
// Opens the serial port of the gps for read/write
// operations.  Returns -1 if error occurs, otherwise
// 0.  The file descriptor of the gps serial port
// is a global variable
int gps_open(int com)
{
  struct termios term; // serial port control settings struct
  char SerialPathName[40];
  sprintf(SerialPathName, "%s%d", SerialPath, com);

  gps_device = open(SerialPathName, O_RDWR);
  
  // error check on serial open
  if (gps_device < 0) {
    printf("gps_open: Cannot open serial port: %d\n", com);
    return -1;
  }
  
  // set serial port options
  memset(&term, '\0', sizeof(term));
  term.c_iflag = IGNBRK;
  term.c_oflag = 0;
  term.c_cflag = CLOCAL | CREAD | CS8 | GPS_BAUD | CRTSCTS;
  term.c_lflag = 0;

  term.c_cc[VMIN] = 1; // read at least 1 char before returning

  // set the flags and check for errors
  if (tcsetattr(gps_device, TCSANOW, &term) < 0){
    cout<<"gps_open: tcsetattr error"<<endl;
    return -1;
  }

  tcflush(gps_device, TCIOFLUSH);

  return 0;

}

// int gps_initialize()
// **********************************************
// Initializes the gps unit to transmit data.  The settings
// needed are as follows:
// 1 PPS (1 ms) : default
// UTM messages from gps
// The sequence is done thus:
// 1. All NMEA messages are disabled 
// 2. GPS is reset to default settings
// 3. POS messages are enabled to get position
// The many many many sleeps in the code are there because
// the gps is waaaay slow and takes forever to respond.
int gps_initialize()
{
  int error;
  char buffer[255]; // create a big bad mofo buffer

  // flush the serial buffer
  tcflush(gps_device, TCIFLUSH);
  gps_buffer_flush();

  // send the NMEA messages off signal to the GPS
  strncpy(buffer, GPS_NMEA_OFF_MSG, GPS_NMEA_OFF_LEN);
  error = write(gps_device, buffer, GPS_NMEA_OFF_LEN);

  if (error < 0){
    cout<<"gps_initialize: error turning off messages"<<endl;
    return -1;
  }
  
  // get acknowledge
  error = get_gps_acknowledge();

  if (error < 0){
    cout<<"gps_initialize: acknowledge error after nmea off"<<endl;
    return -1;
  }
  
  cout<<"messages turned off"<<endl;
  
  // send the reset signal to the GPS
  strncpy(buffer, GPS_RST_MSG, GPS_RST_LEN);
  error = write(gps_device, buffer, GPS_RST_LEN);

  if (error < 0){
    cout<<"gps_initialize: error reseting gps"<<endl;
    return -1;
  }  
  
  // get acknowledge
  error = get_gps_acknowledge();
  if (error < 0){
    cout<<"gps_initialize: acknowledge error after reset"<<endl;
    return -1;
  }

  char blah;
  cout<<"reset gps"<<endl;
  //cin>>blah;

  // gps takes a few seconds to reset
  sleep(5);

  // enable POS messages
  strncpy(buffer, GPS_POS_ON_MSG, GPS_POS_ON_LEN);
  error = write(gps_device, buffer, GPS_POS_ON_LEN);

  if (error < 0){
    cout<<"gps_initialize: error turning on POS messages"<<endl;
    return -1;
  }

  cout<<"waiting to get ack"<<endl;
  //getchar();

  // get acknowledge
  error = get_gps_acknowledge();
  if (error < 0){
    cout<<"gps_initialize: acknowledge error after POS enable"<<endl;
    return -1;
  }
  
  cout<<"pos turned on"<<endl;


  tcflush(gps_device, TCIFLUSH);

  return 0;
}

// int get_gps_acknowledge()
// *************************************************
// Reads a message from the GPS and tries to determine
// if it is an acknowledge message or not 
int get_gps_acknowledge()
{
  char buffer[255];
  int bytes_read;
  int total_bytes = 0;
  int tries_left = 5;

  while (tries_left > 0){
    bytes_read = gps_readline(buffer);      
    if ((bytes_read < 0) || 
	(strncmp(buffer, GPS_ACK_MSG, GPS_ACK_LEN) != 0)){
      cout<<"Error reading gps acknowledge"<<endl;
      cout.write(buffer, bytes_read);
      cout<<endl;
      tries_left--;
    }else{
      return 0;
    }
  }


  return 0;
}

// int gps_close()
// **************************************************
// Orders the gps to stop sending messages and closes
// the serial port.
int gps_close()
{
  char buffer[255];
  int error;

  // flush the serial buffer
  tcflush(gps_device, TCIFLUSH);
  
  close(gps_device);

  return 0;
}

//int gps_readline(char* buffer)
//*********************************
// performs a blocking serial read on the gps serial port, looking for
// the \n character which delineate the end of a gps
// message string.  The buffer is filled with the string, and the function
// returns the size of the string.  Note: function assumes the passed buffer
// is much larger than any gps string it gets. Also, any data left in the 
// serial buffer after the carriage return is lost.  Basically, this 
// will work because we know that we're setting the GPS to only
// send one line at a time  
#warning: blocking read, potential infinite loop
#warning: remaining chars after line are dropped
int gps_readline(char* buffer)
{
  int bytes_read;
  char readbuffer[255];
  int msg_bytes = 0;
  int got_string = 0;
  
  while(!got_string){
    bytes_read = read(gps_device, readbuffer, GPS_READ_BLOCK);

    // check for read error
    if (bytes_read < 0){
      cout<<"Error reading GPS"<<endl;
      return -1;
    }
    
    for (int i = 0; i < bytes_read; i++){
      if (readbuffer[i] != 0){ // strip out zeros
	// check for beginning of string
	if (readbuffer[i] == '$'){
	  msg_bytes = 0;
	  //	  gettimeofday(&t_start, NULL);
	}

	buffer[msg_bytes] = readbuffer[i];
	msg_bytes++;
	if (readbuffer[i] == '\n'){
	  got_string = 1;
	  break;
	}
      }
    }    
  }
  
  return msg_bytes;
}

// int gps_buffer_flush()
// *****************************************
// Does a large nonblocking serial read on the gps serial port
// to flush the bytes.  Normally tcflush is used for doing this,
// but for some reason tcflush is not working, so we're going to
// do this ghetto rig method instead. Return value is the number
// of bytes that were flushed
int gps_buffer_flush()
{
  int temp;
  char flushbuffer[1000]; // big old buffer to flush into
  int bytes_read;

  // first set the serial port to be nonblocking
  if((temp=fcntl(gps_device, F_GETFL, 0))<0){
    cout<<"gps_buffer_flush: fcntl F_GETFL error"<<endl;
    return -1;
  }
  temp |= O_NONBLOCK;
  if(fcntl(gps_device, F_SETFL, temp)<0){
    cout<<"gps_buffer_flush: fcntl F_SETFL error"<<endl;
    return -1;
  }
  
  // now do the flush
  bytes_read = read(gps_device, flushbuffer, 1000);

  // now return it to blocking state
  if((temp=fcntl(gps_device, F_GETFL, 0))<0){
    cout<<"gps_buffer_flush: fcntl F_GETFL error"<<endl;
    return -1;
  }
  temp &= ~O_NONBLOCK;
  if(fcntl(gps_device, F_SETFL, temp)<0){
    cout<<"gps_buffer_flush: fcntl F_SETFL error"<<endl;
    return -1;
  }

  return bytes_read;

}

  
// int gps_read(gps_data &gpsdata)
// ***********************************************************
// Reads bytes from the serial port and parses them into the 
// struct gpsdata.  Assumes that there's not more than 1 packet
// worth of gps data in the serial read buffer at a time.  Blocks
// until a packet has been accumulated 
int gps_read(gps_data &gpsdata)
{
  int bytes_read;
  char messagebuffer[MAX_MSG_LENGTH];

  // keep reading bytes and parsing them until a valid msg is received
  do {    
    bytes_read = gps_readline(messagebuffer);
  }while(gps_parse_line(gpsdata, messagebuffer, bytes_read) < 0);

  //  gettimeofday(&t_end, NULL);
  //t_elapsed = (double)(t_end.tv_sec - t_start.tv_sec + 
  //	       (t_end.tv_usec - t_start.tv_usec) / 1e6);
  //printf("Time elapsed: %5.5f", t_elapsed);

  return 0;
}



// int gps_parse_line(gps_data& gpsdata, char* message, int length)
// *****************************************************************
// takes a string from the character parser and parses it into 
// a gps_data struct.  The message is in this format:
// $PASHR,POS,d1,d2,m1,m2,c1,m3,c2,f1,f2,f3,f4,f5,f6,f7,f8,f9,s*cc
int gps_parse_line(gps_data& gpsdata, char* message, int length)
{
  char* rest_of_msg = message;
  int rest_len = length;
  int parse_len; // length to parse
  char* parse_ch; // pointer to keep track
  LatLong utm(0,0);
  
  if (message[0] != '$'){
    // not a valid string, throw out
    return -1;
  }

  // this points parse_ch to the $PASHR string
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);
  
  // now go on and get the POS string
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);

  // check to make sure we have a pos message
  if (strncmp(parse_ch, "POS", parse_len) != 0){
    return -1;
  }

  // is a POS message, so put in the time we read it
  gettimeofday(&gpsdata.time_read, NULL);
  
  // get raw/differential mode
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);

  if (parse_len == 0){
    gpsdata.pvt_valid = 0;
    // there's no valid PVT solution, so exit
    return 1;
  }else{
    // mode is one byte
    gpsdata.mode = (int)(parse_ch[0] - 48);
    gpsdata.pvt_valid = 1;
  }
  
  // get the number of SV's
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);
  gpsdata.SV = str2num(parse_ch, parse_len);

  // now get the time
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);
  
  // the UTC time is in hhmmss.ss format
  gpsdata.utc_hour = str2num(parse_ch, 2);
  
  // move to minutes
  parse_ch += 2;
  gpsdata.utc_min = str2num(parse_ch, 2);
  
  // go to seconds
  parse_ch +=2;
  gpsdata.utc_sec = str2num(parse_ch, 5);

  // done with seconds, go onto latitude
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);

  // latitude is ddmm.mmmmm
  gpsdata.latitude = str2num(parse_ch, 2);  // degrees

  // get minutes, store as degs in decimal
  parse_ch += 2;
  gpsdata.latitude += (str2num(parse_ch, parse_len - 2)) / 60;

  // get north/south
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);
  if (parse_ch[0] == 'S'){
    gpsdata.latitude *= -1;
  }

  // get longitude also ddmm.mmmmm, however, it could be
  // dddmm.mmmm, so we have a special function for it
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);
  //gpsdata.longitude = str2num(parse_ch, 2);
  //gpsdata.longitude += (str2num(parse_ch+2, parse_len - 2)) / 60;
  gpsdata.longitude = get_deg(parse_ch, parse_len);

  // get E/W
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);
  if (parse_ch[0] == 'W') {
    gpsdata.longitude *= -1;
  }

  // now convert to utm from latlong
  utm.set_latlon(gpsdata.latitude, gpsdata.longitude);
  utm.get_UTM(&gpsdata.y, &gpsdata.x);

  // get altitude in meters
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);
  gpsdata.altitude = str2num(parse_ch, parse_len);
  gpsdata.z = -gpsdata.altitude;

  // next string is unit id, grab and skip
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);
  
  // get heading in degrees
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);
  gpsdata.heading = str2num(parse_ch, parse_len);

  // get ground speed, convert from km/h to m/s
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);
  gpsdata.speed = str2num(parse_ch, parse_len) / 3600 * 1000;

  // set xd and yd
  gpsdata.xd = gpsdata.speed * sin(gpsdata.heading);
  gpsdata.yd = gpsdata.speed * cos(gpsdata.heading);

  // get vertical velocity
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);
  gpsdata.vert_vel = str2num(parse_ch, parse_len);
  gpsdata.zd = - gpsdata.vert_vel; // remember z down

  // now get the PDOP
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);
  gpsdata.pdop = str2num(parse_ch, parse_len);

  // get hdop
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);
  gpsdata.hdop = str2num(parse_ch, parse_len);

  // get vdop
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);
  gpsdata.vdop = str2num(parse_ch, parse_len);

  // get tdop
  get_next_string(parse_ch, parse_len, rest_of_msg, rest_len);
  gpsdata.tdop = str2num(parse_ch, parse_len);

  // rest of the stuff we don't care about 

  return 0;
}


// int get_next_string(char* message, int length, char* new_string,
// int new_len, char* rest, int rest_len)
// **************************************************************
// Sets msg_start to the beginning of the message, then gives the 
// length up to the next comma.  Also sets a pointer to the rest of
// the string (after the comma) and that length;
void get_next_string(char*& msg_start, int& new_len, char*& rest, 
		     int& rest_len)
{
  int comma;
  msg_start = rest;
  comma = find_next_comma(rest, rest_len);
  new_len = comma; // length is 1 less than index

  // give back the pointer and length for the rest of the message
  rest += comma + 1;
  rest_len -=  (comma + 1);

  return;
}

// int find_next_comma(char *message, int length)
// ***************************************************************
// Returns the index of the next comma in the sequence
int find_next_comma(char* message, int length)
{
  int comma;
  for(int i = 0; i < length; i++){
    if (message[i] == ','){
      comma = i;
      break;
    }
  }

  return comma;
  
}

// double str2num(char* number, int length)
// ****************************************
// Converts an ascii string of a given length
// into a double.
double str2num(char* number, int length)
{
  int period = -1;
  double result = 0;
  int i;
  int sign = 1;

  // check for plus/minus signs
  // if one is found, store it and start after the sign
  if (number[0] == '-'){
    sign = -1;
    number++;
    length--;
  }else if (number[0] == '+'){
    number++;
    length--;
  }

  // find the decimal point if there is one
  for(i = 0; i < length; i++){
    if (number[i] == '.'){
      period = i;
      break;
    }
  }
  
  // if there isn't one, just put it at the end
  if (period < 0){
    period = length;
  }else{
    // otherwise get the parts of the result < 1
    for(i = period + 1; i < length; i++){
      result += ((int)number[i] - 48) *powf(10, -1 * (i - period));
    }
  }

  // now get the > 1 parts
  for(i = 0; i < period; i++){
    result += ((int)number[i] - 48) * powf(10, period - i - 1);  
  }

  return result;
      
}

// double get_deg(char* string, int strlen)
// *************************
// special function for dealing with longitude
// because it could be 3 digits or 2.  Picks off degree
// portion and adds minute portion.  Gets the degree portion
// by doing a modulus
double get_deg(char* string, int strlen)
{
  double latitude;
  double initial;


  // format is dd(d)mm.mmmm, so first put entire thing
  // into a double
  initial = str2num(string, strlen);
  
  // now get the degrees by dividing by 100 and doing a floor
  // to get rid of the minutes which will now be behind the decimal
  latitude = floor(initial / 100);

  // now restore the minutes by subtracting out the degrees
  // and moving the decimal point back, then convert into
  // decimal degrees by dividing by 60
  initial = (initial - latitude * 100) / 60;
  
  latitude += initial;

  return latitude;
}
