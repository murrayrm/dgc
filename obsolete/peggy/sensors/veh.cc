// VEH interface code
// Haomiao Huang
// 7/8/04
#include "veh.hh"
using namespace std;

int veh_device;

// int veh_open(int com)
// ****************************************
// Opens the serial port of the veh for read/write
// operations.  Returns -1 if error occurs, otherwise
// 0.  The file descriptor of the veh serial port
// is a global variable
int veh_open(int com)
{
  struct termios term; // serial port control settings struct
  char SerialPathName[40];
  sprintf(SerialPathName, "%s%d", SerialPath, com);

  veh_device = open(SerialPathName, O_RDWR);
  
  // error check on serial open
  if (veh_device < 0) {
    printf("veh_open: Cannot open serial port: %d\n", com);
    return -1;
  }
  
  // set serial port options
  memset(&term, '\0', sizeof(term));
  term.c_iflag = IGNBRK;
  term.c_oflag = 0;
  term.c_cflag = CLOCAL | CREAD | CS8 | VEH_BAUD | CRTSCTS;
  term.c_lflag = 0;

  term.c_cc[VMIN] = 1; // read at least 1 char before returning

  // set the flags and check for errors
  if (tcsetattr(veh_device, TCSANOW, &term) < 0){
    cout<<"veh_open: tcsetattr error"<<endl;
    return -1;
  }

  tcflush(veh_device, TCIOFLUSH);

  // initialize the parser
  init_parser();

  return 0;

}

// int veh_close()
// **************************************************
// Orders the veh to stop sending messages and closes
// the serial port.
int veh_close()
{
  if (close(veh_device) < 0){
    printf("veh_close: error closing veh port\n");
  }

  return 0;
}

// int veh_read(veh_data vdata)
// *************************************************
// Reads serial port for characters and puts them through
// the parser which fills in the struct.  Is blocking 
// until a packet has been received
int veh_read(veh_data& vdata)
{
  char readbuffer[255];
  int gotpacket = 0; // flag for when we have read a packet
  int bytes_read;
  char readchar;

  while (!gotpacket) {
    bytes_read = read(veh_device, &readchar, 1);
    gotpacket = 0;
    parse(readchar, vdata, gotpacket);
  }

  return 0;

}
