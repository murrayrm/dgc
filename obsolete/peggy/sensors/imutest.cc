// IMU Reading test program
// Haomiao Huang
// 7/8/04

#include <stdlib.h>
#include <math.h>
#include <iostream.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <fstream.h>

#include "imu.hh"

int main()
{
  struct imu_data i_dat;
  int rate = 50;
  int sample = 10;
  int diff = -1;
  int error = 0;
  int my_imu;
  int skipped;
  int total = 0;
  int com;

  fstream imufile;
  char* imuname = "imufile.dat";
  int logflag;
  struct timeval imutime;

  cout<<"Choose com port:";
  cin>>com;

  cout<<"Turn logging on?";
  cin>>logflag;


  // initialize imu 
  my_imu = imu_open(com);
  cout<<"Connection to IMU initialized"<<endl;

  if (logflag){
    imufile.open(imuname, fstream::out|fstream::app);
    imufile<<"%time"<<'\t'<<"ax"<<'\t'<<"ay"<<'\t'<<"az"<<'\t'<<
      "tx"<<'\t'<<"ty"<<'\t'<<"tz"<<'\t'<<"dt"<<'\t'<<"counter"<<endl;
  }

  while(true)
    {
      //      cout<<"Begin reading imu...";
      
      // read imu
      imu_read_data(i_dat);
      //cout<<"Done"<<endl;

      if (logflag){
	gettimeofday(&imutime, NULL);
	imufile.precision(20);
	imufile<<(double)(imutime.tv_sec + imutime.tv_usec / 1e6);
	imufile.precision(10);
	imufile<<'\t'<<i_dat.xdd<<'\t'<<i_dat.ydd<<'\t'<<i_dat.zdd<<'\t'<<
	  i_dat.dtx<<'\t'<<i_dat.dty<<'\t'<<i_dat.dtz<<'\t'<<i_dat.dt<<
	  '\t'<<(int)i_dat.counter<<endl;
      }

      if (diff <0){
	diff = i_dat.counter;
      }else{
	if (i_dat.counter - ((diff + 1) % 256) != 0) {
	  error++;
	}
	
	diff = i_dat.counter;
	
      }
      
      total++;

      // dump to screen
      cout<<i_dat.xdd<<' '<<i_dat.ydd<<' '<<i_dat.zdd<<' '<<
	i_dat.dtx<<' '<<i_dat.dty<<' '<<i_dat.dtz<<' '<<i_dat.dt<<' '<<(int)i_dat.counter<<' '<<
	error<<' '<<total<<endl;
    }

  return 0;
}
