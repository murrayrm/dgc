// quick gps parser
#include "gps.hh"
#include <fstream>
#include <stdio.h>

using namespace std;

int main()
{
  char name[40];
  ifstream getfile;
  ofstream outfile;
  double thetime;
  struct gps_data thedata;
  char readbuffer[255];
  int bytes_read;

  cout<<"what file you want parsed, yo?"<<endl;
  scanf("%s", name);
  getfile.open(name);
  outfile.open("output.dat");
 
  printf("Ok then, opening file %s biatch!\n");

  outfile<<"%time"<<'\t'<<"x"<<'\t'<<"y"<<'\t'<<"z"<<'\t'<<"v"<<'\t'<<
    "heading"<<'\t'<<"v_vel"<<'\t'<<"hour"<<'\t'<<"min"<<'\t'<<
    "sec"<<'\t'<<endl;

  outfile.precision(15);
  char comma;

  printf("Now I'm gonna start parsing.\n");
  while (!getfile.eof()){
    getfile>>thetime;
    outfile<<thetime<<'\t';
    getfile>>comma; // get comma separating time from other stuff
    getfile.getline(readbuffer, 110);
    bytes_read = strlen(readbuffer);
    gps_parse_line(thedata, readbuffer, bytes_read);
    if (thedata.pvt_valid){
      printf("It was valid!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
      	outfile<<thedata.x<<'\t'<<thedata.y<<'\t'<<thedata.z<<
	'\t'<<thedata.speed<<'\t'<<thedata.heading<<'\t'<<
	thedata.vert_vel<<'\t'<<thedata.utc_hour<<'\t'<<
	thedata.utc_min<<'\t'<<thedata.utc_sec<<'\t'<<endl;
    }
  }

  return 0;

}
