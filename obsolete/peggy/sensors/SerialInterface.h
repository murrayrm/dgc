#ifndef SERIAL_INTERFACE_H
#define SERIAL_INTERFACE_H

int OpenSerial(void);
void CloseSerial(void);
bool BytesAvailSerial(void);
int GetchSerial(void);
void PutchSerial(int Byte);
void FlushSerial(void);

#endif
