// quick serial test routine


#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include<iostream>
#include<fstream>

#include "gps.hh"

using namespace std;

int main(int argc, char *argv[])
{
  int gps_device;
  char SerialPathName[40]; // char array for serial path
  char readbuffer[255];
  int bytes_read;
  char buffer[255];
  int msg_bytes = 0;
  struct timeval tread;
  double the_time;
  int logflag = 0;
  ofstream gpswrite;
  char name[40];
  const int fnamesize = 40;
  char ch;

  if(argc != 2)
    {
      cerr << "usage: sertest [serial port #]\n";
      return -1;
    }
  cout.precision(15);
  cout<<"Enable data logging?";
  cin>>ch;

  if ((ch =='y') || (ch == 'Y')){
    cout<<"Please enter filename:"<<endl;
    scanf("%s", name);
    gpswrite.open(name);
    gpswrite.precision(15);
    logflag = 1;
  }
  
  gps_device = gps_open(atoi(argv[1]));  

  bytes_read = gps_buffer_flush();
  cout<<"Flushed "<<bytes_read<<" bytes"<<endl;

  gps_initialize();

  while(true){    
    bytes_read = gps_readline(buffer);
    gettimeofday(&tread, NULL);
    the_time = (double)(tread.tv_sec + tread.tv_usec / 1e6);
    cout<<"time: "<<the_time<<endl;
    cout.write(buffer, bytes_read);
    cout<<"read "<<bytes_read<<endl;
        

    if (logflag > 0){
      gpswrite<<the_time<<',';
      gpswrite.write(buffer, bytes_read);
      gpswrite.flush();
      cout<<"logged"<<endl;      
    }
  }
  return 0;
}
