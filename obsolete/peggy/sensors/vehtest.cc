// quick serial test routine

#include <fstream>
#include <iostream>
using namespace std;

#include "veh.hh"

int main(int argc, char *argv[])
{
  char readbuffer[255];
  int bytes_read;
  char buffer[255];
  int msg_bytes = 0;
  char msg_buffer[255];
  char ch;
  struct timeval tread;
  double the_time;
  char name[40];
  const int fnamesize = 40;
  int logflag = 0;
  struct veh_data mydata;
  int updateflag = 0;
  ofstream vehwrite;
  
  if(argc != 2)
    {
      cerr << "usage: sertest [serial port #]\n";
      return -1;
    }

  

  cout.precision(15);
  cout<<"Enable data logging?";
  cin>>ch;
  if ((ch =='y') || (ch == 'Y')){
    cout<<"Please enter filename:"<<endl;
    scanf("%s", name);
    vehwrite.open(name);
    vehwrite.precision(15);
    logflag = 1;
  }

  //initialize the connection
  veh_open(atoi(argv[1]));

  
  while(true){    
    veh_read(mydata);
    cout<<mydata.steering<<' '<<mydata.throttle<<' '<<mydata.aux<<
      ' '<<mydata.engine_vel<<' '<<mydata.fr_vel<<' '<<mydata.fl_vel<<
      ' '<<mydata.rr_vel<<' '<<mydata.rl_vel<<endl;
  }
  return 0;
}
