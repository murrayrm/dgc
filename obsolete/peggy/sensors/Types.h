#ifndef TYPES_H
#define TYPES_H

//These type definitions are platform dependent.
typedef signed long	sint32;
typedef unsigned long	uint32;
typedef unsigned short	uint16;
typedef signed short	sint16;
typedef unsigned short	word;
typedef signed char	sint8;
typedef unsigned char	uint8;
typedef float	        float32;   //Non-IMU code only.
#ifndef BOOL
	typedef int			BOOL; //!< 32-bit boolean
#endif

#endif
