/*! \file
	\brief  External interface file for the IMU.

	This file contains data structures, conversion constants, message types, and other enumerations pertaining to the IMU.
*/

//!IMUExternalTypes.h
//!External types for the IMU.
#ifndef IMU_EXTERNALTYPES_H
#define IMU_EXTERNALTYPES_H
#include "Types.h"
#include "Indices.h"

//!IMU Configuration structure
typedef struct {
	uint16 SerialNum;					//!< Serial number of sensor head.
	uint8  EEPROMFormatVersion;			//!< EEPROM format version number
	uint8  HardwareRevisionMajor;		//!< Hardware revision, X in X.Y
	uint8  HardwareRevisionMinor;		//!< Hardware revision, Y in X.Y
	uint8  ConfigNumberH;				//!< Configuration number (3 bytes),
	uint8  ConfigNumber1;
	uint8  ConfigNumberL;

	uint8  MfrMonth;					//!< Manufacture month
	uint8  MfrDay;						//!< Manufacture day
	uint16 MfrYear;						//!< Manufacture year, uint16
	uint8  CalMonth;					//!< Calibration month
	uint8  CalDay;						//!< Calibration day
	uint16 CalYear;						//!< Calibration year, uint16

	uint8  SWVersionMajor;				//!< Software major version, X in X.Y.Z
	uint8  SWVersionMinor;				//!< Software minor version, Y in X.Y.Z
	uint8  SWVersionSub;				//!< Software sub version,   Z in X.Y.Z
	uint8  SWMonth;						//!< Software release month
	uint8  SWDay;						//!< Software release day
	uint16 SWYear;						//!< Software release year
	uint8  SWReleaseFlag;				//!< Software release flag (0 test version, 1 released)
	
	//Sample configurations
	float32 SampleRateHz;				//!< Sample rate in Hz
	uint16 OversampleRatio;				//!< Oversample ratio
	uint8 CommsMode;					//!< Communications mode
	uint8 SampleMode;					//!< Sample mode
	
} IMUConfig_t;

//!IMU Sensor structure
typedef struct {
	float32 Gyro[N3D];			//!< Gyro rates, 3 axes, deg/s     (indexed by N3D:X_AXIS,Y_AXIS,Z_AXIS)
	float32 Accel[N3D];			//!< Accelerations, 3 axes, m/s^2  (indexed by N3D:X_AXIS,Y_AXIS,Z_AXIS)
	uint32 TimerTicksSincePPS;  //!< Timer ticks since GPS Pulse Per Second
	double TimeSincePPS_sec;	//!< Time since GPS Pulse Per Second (sec)
	uint8 PPSCounter;			//!< GPS Pulse Per Second counter
	uint8 SequenceNumber;		//!< Sequence number
	float32 Rawgyro[N3D];		//!< Raw gyro data [volts]
	float32 Rawaccel[N3D];		//!< Raw accel data [volts]
	float32 Rawgyrotemp[N3D];	//!< Raw gyro temperature [volts]
 	IMUConfig_t IMUConfig;   	//!< IMU Configuration data (serial number etc)	
} IMUSensors_t;

//!Conversion constants (method and data subject to change)
//Constants to convert from deg/s or m/s^2 to resolution units.  
// Multiply deg/s or m/s^2 to get resolution units.  
// Divide resolution units by CONV_GYRO_RESOLUTION or CONV_ACCEL_RESOLUTION to get deg/s or m/s^2.
// The size of the resolution unit is 1/CONV_GYRO_RESOLUTION
// This converts from gyro output in deg/sec to gyro units, for encoding in an sint16.
// If you divide raw gyro resolution counts by this number, you get deg/sec.
//Units: gyro resolution units / (deg/s)
#define CONV_GYRO_RESOLUTION  (65536.0f/(300.0f*2)) //109.22666666667f   

//If you divide raw accelerometer resolution counts by this number, you get m/s^2.
//Units: accel resolution units / (m/s^2)
#define CONV_ACCEL_RESOLUTION (65536.0f/(10.0f*2.0f*9.805f)) //334.1968383478f  

//!Timer frequency (may change in future)
#define TIMER_FREQUENCY_HZ (10000000UL)

//!Conversion for raw data
#define COUNTS_TO_VOLTS_AD16 (4.096/65536.0)

//!IMU CAN/Serial Message type enumerations
enum IMUMessageTypes
{
	RAWGYRO_IMU_MSG,					//!< Raw gyro data
	RAWGYROTEMP_IMU_MSG,				//!< Raw gyro temperatures
	RAWACCEL_IMU_MSG,					//!< Raw accelerometer data
	TIMING_IMU_MSG,						//!< Timing data
	RESOLUTION_IMU_MSG,					//!< Resolution
	RESUNITS_GYRO_IMU_MSG,				//!< Gyro data in resolution units
	RESUNITS_ACCEL_IMU_MSG,				//!< Accelerometer data in resolution units
	SET_SETTINGS_IMU_MSG,				//!< Set settings IMU message
	SETTINGS_IMU_MSG,					//!< Return settings IMU message
	MFRCALDATE_IMU_MSG,					//!< Return manufacture and calibration dates
	SERIALNUMCONFIG_IMU_MSG,			//!< Return serial number and configuration data
	SWVERSION_IMU_MSG,					//!< Return software versions message
	BOARDREFERENCE_IMU_MSG,				//!< Return processor board reference data (crystal frequency 
										//!<   and 10-bit a/d ref)
	REQ_CONFIG_IMU_MSG,					//!< Request all configuration and settings messages
	SET_SENSOR_NEUTRAL_IMU_MSG,			//!< Set sensor neutral
	RESERVED0_IMU_MSG,					//!< Reserved message
	RESERVED1_IMU_MSG,					//!< Reserved message
	RESERVED2_IMU_MSG,					//!< Reserved message
	RESERVED3_IMU_MSG,					//!< Reserved message
	RESERVED4_IMU_MSG,					//!< Reserved message
	RESERVED5_IMU_MSG,					//!< Reserved message
	RESERVED6_IMU_MSG,					//!< Reserved message
	RESERVED7_IMU_MSG,					//!< Reserved message
	RAWGYROTEMPX_IMU_MSG,				//!< Raw X gyro temp, volts, float32be
	RAWGYROTEMPY_IMU_MSG,				//!< Raw Y gyro temp, volts, float32be
	RAWGYROTEMPZ_IMU_MSG,				//!< Raw Z gyro temp, volts, float32be

	ENABLE_BOOTLOADER_IMU_MSG,			//!< Enable bootloader mode (danger!)
	SENSORHEAD_CRC_STATUS_IMU_MSG,		//!< Return sensor head CRC status (pass/fail).
	
	//Add new messages before this line.
	HS_SERIAL_IMU_MSG=0xFF   			//!< High speed serial packet - This is never sent as a 										
										//!<   CAN packet, just as a packaged serial packet.
};

//!Enumerate communications modes
enum CommunicationsModes {
	RS232_COMMS_MODE=1,
	CAN_COMMS_MODE,
	BOTH_COMMS_MODE
};

//!Enumerate sample modes
enum SampleModes {
	CONVERTED_SAMPLE_MODE=1,
	RAW_SAMPLE_MODE,
	BOTH_SAMPLE_MODE
};

//!Enumerate sensor indices
enum IMUSensorIndices {
	GYROX_IDX,
	GYROY_IDX,
	GYROZ_IDX,
	ACCELX_IDX,
	ACCELY_IDX,
	ACCELZ_IDX,
	N_SENSOR_IDX
};

#endif
