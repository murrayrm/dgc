// VEH interface definitions and declarations
// Haomiao Huang
// 7/8/04

#ifndef VEH_HH
#define VEH_HH

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <iostream>

#include "vehparse.hh"

#define VEH_BAUD B38400
#define SerialPath "/dev/ttyUSB"
#define VEH_MSG_LEN 24

// vehicle data struct veh_data is defined in vehparse.hh

// int veh_open(int com)
// ****************************************
// Opens the serial port of the veh for read/write
// operations.  Returns -1 if error occurs, otherwise
// 0.  The file descriptor of the veh serial port
// is a global variable
int veh_open(int com);

// int veh_close()
// **************************************************
// Orders the veh to stop sending messages and closes
// the serial port.
int veh_close();

// int veh_read(veh_data vdata)
// *************************************************
// Reads serial port for characters and puts them through
// the parser which fills in the struct.  Is blocking 
// until a packet has been received
int veh_read(veh_data& vdata); 

#endif 
