// File: rtparse.cpp
// Description:  This file polls the serial port and parses data coming in from Peggy
//               in real time.  It displays the results as it receives them.  It uses
//               a FSM similar to the one in "parselog.cpp," but without timestamp parsing.
//               So the state table is DIFFERENT.

#include <iostream>
using namespace std;

#include "vehparse.hh"
#include "veh_tokens.hh"

#define STATE_START				0
#define STATE_GET_TIMESTAMP		1
#define STATE_GET_MESSAGE_0		2
#define STATE_GET_MESSAGE_1		3
#define STATE_GET_MESSAGE_2		4
#define STATE_GET_MESSAGE_3		5
#define STATE_GET_MESSAGE_4		6
#define STATE_GET_MESSAGE_5		7
#define STATE_GET_MESSAGE_6		8
#define STATE_GET_MESSAGE_7		9
#define STATE_GET_MESSAGE_8		10
#define STATE_GET_MESSAGE_9		11
#define STATE_GET_MESSAGE_10	12
#define STATE_GET_MESSAGE_11	13
#define STATE_GET_MESSAGE_12	14
#define STATE_GET_MESSAGE_13	15
#define STATE_GET_MESSAGE_14	16
#define STATE_GET_MESSAGE_15	17
#define STATE_GET_END			18

#define NUM_STATES				19

struct transition
{
  char state;				//state to transition to
  void (*fptr)(void);     // pointer to the function to call on transition
    
};

int gl_update_flag; // update flag for knowing when a full packet is got

struct transition state_table[NUM_STATES][NUM_TOKENS];

volatile unsigned char fsm_state;
volatile unsigned char fsm_input;

//State for timestamp reconstruction:
double timestamp;
char timestamp_string[30];
int timestamp_ref;

//Telemetry variables:
volatile unsigned short telem_steering, telem_throttle, telem_aux, telem_engine_vel, telem_fr_vel, telem_fl_vel;
volatile unsigned short telem_rr_vel, telem_rl_vel;


// void parse(char input, veh_data& thedata, int update)
// *****************************************************
// parses input one character at a time
// when a full packet has been received, the flag update
// is set so that the program that calls it knows. 
void parse(char input, veh_data& thedata, int& update)
{
	char token;
	
	// reset update flag
	gl_update_flag = update;

	fsm_input = input;
	
	//Tokenize input:
	token = token_table[fsm_input];

	//Execute transition command
	(state_table[fsm_state][token].fptr)();
	//Lookup transition and change state
	fsm_state = state_table[fsm_state][token].state;

	if (gl_update_flag > 0){
	  // a packet was completed, set the struct
	  update = 1;
	  thedata.steering = telem_steering;
	  thedata.throttle = telem_throttle;
	  thedata.aux = telem_aux;
	  //thedata.engine_vel = (unsigned short)SECSPERMIN/(telem_engine_vel * 
	  //					   ENG_TMR_PERIOD *
	  //				   ENG_DIVIDER_CNT);
	  thedata.fr_vel = (unsigned short)SECSPERMIN/(telem_fr_vel * 
						       WHEEL_TMR_PERIOD);
	  thedata.fl_vel = (unsigned short)SECSPERMIN/(telem_fl_vel * 
						       WHEEL_TMR_PERIOD);
	  thedata.rr_vel = (unsigned short)SECSPERMIN/(telem_rr_vel * 
						       WHEEL_TMR_PERIOD);
	  thedata.rl_vel = (unsigned short)SECSPERMIN/(telem_rl_vel * 
				       WHEEL_TMR_PERIOD);
	  thedata.engine_vel = telem_engine_vel;

	}

	return;
}

/***************************************************************************************
* function: init_parser()
* description: initializes shared variables used to store state of message parser
*
* Author: Lyle Chamberlain
* Ver: 1.0
* Revision History: 8/27/04 - Created (1.0)
****************************************************************************************/
void init_parser()
{
	fsm_state = STATE_START;
	init_transition_table();
	return;
}

/****************************************************************************************
* function: reset_timestamp()
* this function resets the timestamp and the state of the machinery used to reconstruct
* it from a string.
* 
* Author: Lyle Chamberlain
* Ver: 1.0
* Revision History: 8/27/04 - Created (1.0)
*****************************************************************************************/
void reset_timestamp()
{
	timestamp = 0;				  //clear timestamp
	timestamp_string[0] = '\0';   //clear string
	timestamp_ref = 0;			  //point to beginning of string again.

	return;
}

/****************************************************************************************
* function: init_transition_table()
* description: initializes the transition table for the state machine
*
* Author: Lyle Chamberlain
* Ver: 1.0
* Revision History: 8/27/04 - Created (1.0)
*****************************************************************************************/
void init_transition_table()
{
	/* State: STATE_START */
	/* Transitions: */
	state_table[STATE_START][TOKEN_NUMBER].state = STATE_START; 
	state_table[STATE_START][TOKEN_START].state = STATE_GET_MESSAGE_0;
	state_table[STATE_START][TOKEN_END].state = STATE_START;
	state_table[STATE_START][TOKEN_DATA].state = STATE_START;
	state_table[STATE_START][TOKEN_LF].state = STATE_START;
	/* Actions */
	state_table[STATE_START][TOKEN_NUMBER].fptr = act_reset;
	state_table[STATE_START][TOKEN_START].fptr = act_nop;
	state_table[STATE_START][TOKEN_END].fptr = act_reset;
	state_table[STATE_START][TOKEN_DATA].fptr = act_reset;
	state_table[STATE_START][TOKEN_LF].fptr = act_reset;

	/* State: STATE_GET_TIMESTAMP */
	/* Transitions */
	state_table[STATE_GET_TIMESTAMP][TOKEN_NUMBER].state = STATE_GET_TIMESTAMP;
	state_table[STATE_GET_TIMESTAMP][TOKEN_START].state = STATE_START;
	state_table[STATE_GET_TIMESTAMP][TOKEN_END].state = STATE_START;
	state_table[STATE_GET_TIMESTAMP][TOKEN_DATA].state = STATE_START;
	state_table[STATE_GET_TIMESTAMP][TOKEN_LF].state = STATE_START;
	/* Actions */
	state_table[STATE_GET_TIMESTAMP][TOKEN_NUMBER].fptr = act_get_number;
	state_table[STATE_GET_TIMESTAMP][TOKEN_START].fptr = act_reset;
	state_table[STATE_GET_TIMESTAMP][TOKEN_END].fptr = act_reset;
	state_table[STATE_GET_TIMESTAMP][TOKEN_DATA].fptr = act_reset;
	state_table[STATE_GET_TIMESTAMP][TOKEN_LF].fptr = act_write_line;

	/* State: STATE_GET_MESSAGE_0 */
	/* Transitions: */
	state_table[STATE_GET_MESSAGE_0][TOKEN_NUMBER].state = STATE_GET_MESSAGE_1;
	state_table[STATE_GET_MESSAGE_0][TOKEN_START].state = STATE_GET_MESSAGE_1;
	state_table[STATE_GET_MESSAGE_0][TOKEN_END].state = STATE_GET_MESSAGE_1;
	state_table[STATE_GET_MESSAGE_0][TOKEN_DATA].state = STATE_GET_MESSAGE_1;
	state_table[STATE_GET_MESSAGE_0][TOKEN_LF].state = STATE_GET_MESSAGE_1;
	/* Actions: */
	state_table[STATE_GET_MESSAGE_0][TOKEN_NUMBER].fptr = act_msg_0;
	state_table[STATE_GET_MESSAGE_0][TOKEN_START].fptr = act_msg_0;
	state_table[STATE_GET_MESSAGE_0][TOKEN_END].fptr = act_msg_0;
	state_table[STATE_GET_MESSAGE_0][TOKEN_DATA].fptr = act_msg_0;
	state_table[STATE_GET_MESSAGE_0][TOKEN_LF].fptr = act_msg_0;

	/* State: STATE_GET_MESSAGE_1 */
	/* Transitions: */
	state_table[STATE_GET_MESSAGE_1][TOKEN_NUMBER].state = STATE_GET_MESSAGE_2;
	state_table[STATE_GET_MESSAGE_1][TOKEN_START].state = STATE_GET_MESSAGE_2;
	state_table[STATE_GET_MESSAGE_1][TOKEN_END].state = STATE_GET_MESSAGE_2;
	state_table[STATE_GET_MESSAGE_1][TOKEN_DATA].state = STATE_GET_MESSAGE_2;
	state_table[STATE_GET_MESSAGE_1][TOKEN_LF].state = STATE_GET_MESSAGE_2;
	/* Actions: */
	state_table[STATE_GET_MESSAGE_1][TOKEN_NUMBER].fptr = act_msg_1;
	state_table[STATE_GET_MESSAGE_1][TOKEN_START].fptr = act_msg_1;
	state_table[STATE_GET_MESSAGE_1][TOKEN_END].fptr = act_msg_1;
	state_table[STATE_GET_MESSAGE_1][TOKEN_DATA].fptr = act_msg_1;
	state_table[STATE_GET_MESSAGE_1][TOKEN_LF].fptr = act_msg_1;

	/* State: STATE_GET_MESSAGE_2 */
	/* Transitions: */
	state_table[STATE_GET_MESSAGE_2][TOKEN_NUMBER].state = STATE_GET_MESSAGE_3;
	state_table[STATE_GET_MESSAGE_2][TOKEN_START].state = STATE_GET_MESSAGE_3;
	state_table[STATE_GET_MESSAGE_2][TOKEN_END].state = STATE_GET_MESSAGE_3;
	state_table[STATE_GET_MESSAGE_2][TOKEN_DATA].state = STATE_GET_MESSAGE_3;
	state_table[STATE_GET_MESSAGE_2][TOKEN_LF].state = STATE_GET_MESSAGE_3;
	/* Actions: */
	state_table[STATE_GET_MESSAGE_2][TOKEN_NUMBER].fptr = act_msg_2;
	state_table[STATE_GET_MESSAGE_2][TOKEN_START].fptr = act_msg_2;
	state_table[STATE_GET_MESSAGE_2][TOKEN_END].fptr = act_msg_2;
	state_table[STATE_GET_MESSAGE_2][TOKEN_DATA].fptr = act_msg_2;
	state_table[STATE_GET_MESSAGE_2][TOKEN_LF].fptr = act_msg_2;

	/* State: STATE_GET_MESSAGE_3 */
	/* Transitions: */
	state_table[STATE_GET_MESSAGE_3][TOKEN_NUMBER].state = STATE_GET_MESSAGE_4;
	state_table[STATE_GET_MESSAGE_3][TOKEN_START].state = STATE_GET_MESSAGE_4;
	state_table[STATE_GET_MESSAGE_3][TOKEN_END].state = STATE_GET_MESSAGE_4;
	state_table[STATE_GET_MESSAGE_3][TOKEN_DATA].state = STATE_GET_MESSAGE_4;
	state_table[STATE_GET_MESSAGE_3][TOKEN_LF].state = STATE_GET_MESSAGE_4;
	/* Actions: */
	state_table[STATE_GET_MESSAGE_3][TOKEN_NUMBER].fptr = act_msg_3;
	state_table[STATE_GET_MESSAGE_3][TOKEN_START].fptr = act_msg_3;
	state_table[STATE_GET_MESSAGE_3][TOKEN_END].fptr = act_msg_3;
	state_table[STATE_GET_MESSAGE_3][TOKEN_DATA].fptr = act_msg_3;
	state_table[STATE_GET_MESSAGE_3][TOKEN_LF].fptr = act_msg_3;

	/* State: STATE_GET_MESSAGE_4 */
	/* Transitions: */
	state_table[STATE_GET_MESSAGE_4][TOKEN_NUMBER].state = STATE_GET_MESSAGE_5;
	state_table[STATE_GET_MESSAGE_4][TOKEN_START].state = STATE_GET_MESSAGE_5;
	state_table[STATE_GET_MESSAGE_4][TOKEN_END].state = STATE_GET_MESSAGE_5;
	state_table[STATE_GET_MESSAGE_4][TOKEN_DATA].state = STATE_GET_MESSAGE_5;
	state_table[STATE_GET_MESSAGE_4][TOKEN_LF].state = STATE_GET_MESSAGE_5;
	/* Actions: */
	state_table[STATE_GET_MESSAGE_4][TOKEN_NUMBER].fptr = act_msg_4;
	state_table[STATE_GET_MESSAGE_4][TOKEN_START].fptr = act_msg_4;
	state_table[STATE_GET_MESSAGE_4][TOKEN_END].fptr = act_msg_4;
	state_table[STATE_GET_MESSAGE_4][TOKEN_DATA].fptr = act_msg_4;
	state_table[STATE_GET_MESSAGE_4][TOKEN_LF].fptr = act_msg_4;

	/* State: STATE_GET_MESSAGE_5 */
	/* Transitions: */
	state_table[STATE_GET_MESSAGE_5][TOKEN_NUMBER].state = STATE_GET_MESSAGE_6;
	state_table[STATE_GET_MESSAGE_5][TOKEN_START].state = STATE_GET_MESSAGE_6;
	state_table[STATE_GET_MESSAGE_5][TOKEN_END].state = STATE_GET_MESSAGE_6;
	state_table[STATE_GET_MESSAGE_5][TOKEN_DATA].state = STATE_GET_MESSAGE_6;
	state_table[STATE_GET_MESSAGE_5][TOKEN_LF].state = STATE_GET_MESSAGE_6;
	/* Actions: */
	state_table[STATE_GET_MESSAGE_5][TOKEN_NUMBER].fptr = act_msg_5;
	state_table[STATE_GET_MESSAGE_5][TOKEN_START].fptr = act_msg_5;
	state_table[STATE_GET_MESSAGE_5][TOKEN_END].fptr = act_msg_5;
	state_table[STATE_GET_MESSAGE_5][TOKEN_DATA].fptr = act_msg_5;
	state_table[STATE_GET_MESSAGE_5][TOKEN_LF].fptr = act_msg_5;

	/* State: STATE_GET_MESSAGE_6 */
	/* Transitions: */
	state_table[STATE_GET_MESSAGE_6][TOKEN_NUMBER].state = STATE_GET_MESSAGE_7;
	state_table[STATE_GET_MESSAGE_6][TOKEN_START].state = STATE_GET_MESSAGE_7;
	state_table[STATE_GET_MESSAGE_6][TOKEN_END].state = STATE_GET_MESSAGE_7;
	state_table[STATE_GET_MESSAGE_6][TOKEN_DATA].state = STATE_GET_MESSAGE_7;
	state_table[STATE_GET_MESSAGE_6][TOKEN_LF].state = STATE_GET_MESSAGE_7;
	/* Actions: */
	state_table[STATE_GET_MESSAGE_6][TOKEN_NUMBER].fptr = act_msg_6;
	state_table[STATE_GET_MESSAGE_6][TOKEN_START].fptr = act_msg_6;
	state_table[STATE_GET_MESSAGE_6][TOKEN_END].fptr = act_msg_6;
	state_table[STATE_GET_MESSAGE_6][TOKEN_DATA].fptr = act_msg_6;
	state_table[STATE_GET_MESSAGE_6][TOKEN_LF].fptr = act_msg_6;

	/* State: STATE_GET_MESSAGE_7 */
	/* Transitions: */
	state_table[STATE_GET_MESSAGE_7][TOKEN_NUMBER].state = STATE_GET_MESSAGE_8;
	state_table[STATE_GET_MESSAGE_7][TOKEN_START].state = STATE_GET_MESSAGE_8;
	state_table[STATE_GET_MESSAGE_7][TOKEN_END].state = STATE_GET_MESSAGE_8;
	state_table[STATE_GET_MESSAGE_7][TOKEN_DATA].state = STATE_GET_MESSAGE_8;
	state_table[STATE_GET_MESSAGE_7][TOKEN_LF].state = STATE_GET_MESSAGE_8;
	/* Actions: */
	state_table[STATE_GET_MESSAGE_7][TOKEN_NUMBER].fptr = act_msg_7;
	state_table[STATE_GET_MESSAGE_7][TOKEN_START].fptr = act_msg_7;
	state_table[STATE_GET_MESSAGE_7][TOKEN_END].fptr = act_msg_7;
	state_table[STATE_GET_MESSAGE_7][TOKEN_DATA].fptr = act_msg_7;
	state_table[STATE_GET_MESSAGE_7][TOKEN_LF].fptr = act_msg_7;

	/* State: STATE_GET_MESSAGE_8 */
	/* Transitions: */
	state_table[STATE_GET_MESSAGE_8][TOKEN_NUMBER].state = STATE_GET_MESSAGE_9;
	state_table[STATE_GET_MESSAGE_8][TOKEN_START].state = STATE_GET_MESSAGE_9;
	state_table[STATE_GET_MESSAGE_8][TOKEN_END].state = STATE_GET_MESSAGE_9;
	state_table[STATE_GET_MESSAGE_8][TOKEN_DATA].state = STATE_GET_MESSAGE_9;
	state_table[STATE_GET_MESSAGE_8][TOKEN_LF].state = STATE_GET_MESSAGE_9;
	/* Actions: */
	state_table[STATE_GET_MESSAGE_8][TOKEN_NUMBER].fptr = act_msg_8;
	state_table[STATE_GET_MESSAGE_8][TOKEN_START].fptr = act_msg_8;
	state_table[STATE_GET_MESSAGE_8][TOKEN_END].fptr = act_msg_8;
	state_table[STATE_GET_MESSAGE_8][TOKEN_DATA].fptr = act_msg_8;
	state_table[STATE_GET_MESSAGE_8][TOKEN_LF].fptr = act_msg_8;

	/* State: STATE_GET_MESSAGE_9 */
	/* Transitions: */
	state_table[STATE_GET_MESSAGE_9][TOKEN_NUMBER].state = STATE_GET_MESSAGE_10;
	state_table[STATE_GET_MESSAGE_9][TOKEN_START].state = STATE_GET_MESSAGE_10;
	state_table[STATE_GET_MESSAGE_9][TOKEN_END].state = STATE_GET_MESSAGE_10;
	state_table[STATE_GET_MESSAGE_9][TOKEN_DATA].state = STATE_GET_MESSAGE_10;
	state_table[STATE_GET_MESSAGE_9][TOKEN_LF].state = STATE_GET_MESSAGE_10;
	/* Actions: */
	state_table[STATE_GET_MESSAGE_9][TOKEN_NUMBER].fptr = act_msg_9;
	state_table[STATE_GET_MESSAGE_9][TOKEN_START].fptr = act_msg_9;
	state_table[STATE_GET_MESSAGE_9][TOKEN_END].fptr = act_msg_9;
	state_table[STATE_GET_MESSAGE_9][TOKEN_DATA].fptr = act_msg_9;
	state_table[STATE_GET_MESSAGE_9][TOKEN_LF].fptr = act_msg_9;

	/* State: STATE_GET_MESSAGE_10 */
	/* Transitions: */
	state_table[STATE_GET_MESSAGE_10][TOKEN_NUMBER].state = STATE_GET_MESSAGE_11;
	state_table[STATE_GET_MESSAGE_10][TOKEN_START].state = STATE_GET_MESSAGE_11;
	state_table[STATE_GET_MESSAGE_10][TOKEN_END].state = STATE_GET_MESSAGE_11;
	state_table[STATE_GET_MESSAGE_10][TOKEN_DATA].state = STATE_GET_MESSAGE_11;
	state_table[STATE_GET_MESSAGE_10][TOKEN_LF].state = STATE_GET_MESSAGE_11;
	/* Actions: */
	state_table[STATE_GET_MESSAGE_10][TOKEN_NUMBER].fptr = act_msg_10;
	state_table[STATE_GET_MESSAGE_10][TOKEN_START].fptr = act_msg_10;
	state_table[STATE_GET_MESSAGE_10][TOKEN_END].fptr = act_msg_10;
	state_table[STATE_GET_MESSAGE_10][TOKEN_DATA].fptr = act_msg_10;
	state_table[STATE_GET_MESSAGE_10][TOKEN_LF].fptr = act_msg_10;

	/* State: STATE_GET_MESSAGE_11 */
	/* Transitions: */
	state_table[STATE_GET_MESSAGE_11][TOKEN_NUMBER].state = STATE_GET_MESSAGE_12;
	state_table[STATE_GET_MESSAGE_11][TOKEN_START].state = STATE_GET_MESSAGE_12;
	state_table[STATE_GET_MESSAGE_11][TOKEN_END].state = STATE_GET_MESSAGE_12;
	state_table[STATE_GET_MESSAGE_11][TOKEN_DATA].state = STATE_GET_MESSAGE_12;
	state_table[STATE_GET_MESSAGE_11][TOKEN_LF].state = STATE_GET_MESSAGE_12;
	/* Actions: */
	state_table[STATE_GET_MESSAGE_11][TOKEN_NUMBER].fptr = act_msg_11;
	state_table[STATE_GET_MESSAGE_11][TOKEN_START].fptr = act_msg_11;
	state_table[STATE_GET_MESSAGE_11][TOKEN_END].fptr = act_msg_11;
	state_table[STATE_GET_MESSAGE_11][TOKEN_DATA].fptr = act_msg_11;
	state_table[STATE_GET_MESSAGE_11][TOKEN_LF].fptr = act_msg_11;

	/* State: STATE_GET_MESSAGE_12 */
	/* Transitions: */
	state_table[STATE_GET_MESSAGE_12][TOKEN_NUMBER].state = STATE_GET_MESSAGE_13;
	state_table[STATE_GET_MESSAGE_12][TOKEN_START].state = STATE_GET_MESSAGE_13;
	state_table[STATE_GET_MESSAGE_12][TOKEN_END].state = STATE_GET_MESSAGE_13;
	state_table[STATE_GET_MESSAGE_12][TOKEN_DATA].state = STATE_GET_MESSAGE_13;
	state_table[STATE_GET_MESSAGE_12][TOKEN_LF].state = STATE_GET_MESSAGE_13;
	/* Actions: */
	state_table[STATE_GET_MESSAGE_12][TOKEN_NUMBER].fptr = act_msg_12;
	state_table[STATE_GET_MESSAGE_12][TOKEN_START].fptr = act_msg_12;
	state_table[STATE_GET_MESSAGE_12][TOKEN_END].fptr = act_msg_12;
	state_table[STATE_GET_MESSAGE_12][TOKEN_DATA].fptr = act_msg_12;
	state_table[STATE_GET_MESSAGE_12][TOKEN_LF].fptr = act_msg_12;

	/* State: STATE_GET_MESSAGE_13 */
	/* Transitions: */
	state_table[STATE_GET_MESSAGE_13][TOKEN_NUMBER].state = STATE_GET_MESSAGE_14;
	state_table[STATE_GET_MESSAGE_13][TOKEN_START].state = STATE_GET_MESSAGE_14;
	state_table[STATE_GET_MESSAGE_13][TOKEN_END].state = STATE_GET_MESSAGE_14;
	state_table[STATE_GET_MESSAGE_13][TOKEN_DATA].state = STATE_GET_MESSAGE_14;
	state_table[STATE_GET_MESSAGE_13][TOKEN_LF].state = STATE_GET_MESSAGE_14;
	/* Actions: */
	state_table[STATE_GET_MESSAGE_13][TOKEN_NUMBER].fptr = act_msg_13;
	state_table[STATE_GET_MESSAGE_13][TOKEN_START].fptr = act_msg_13;
	state_table[STATE_GET_MESSAGE_13][TOKEN_END].fptr = act_msg_13;
	state_table[STATE_GET_MESSAGE_13][TOKEN_DATA].fptr = act_msg_13;
	state_table[STATE_GET_MESSAGE_13][TOKEN_LF].fptr = act_msg_13;

	/* State: STATE_GET_MESSAGE_14 */
	/* Transitions: */
	state_table[STATE_GET_MESSAGE_14][TOKEN_NUMBER].state = STATE_GET_MESSAGE_15;
	state_table[STATE_GET_MESSAGE_14][TOKEN_START].state = STATE_GET_MESSAGE_15;
	state_table[STATE_GET_MESSAGE_14][TOKEN_END].state = STATE_GET_MESSAGE_15;
	state_table[STATE_GET_MESSAGE_14][TOKEN_DATA].state = STATE_GET_MESSAGE_15;
	state_table[STATE_GET_MESSAGE_14][TOKEN_LF].state = STATE_GET_MESSAGE_15;
	/* Actions: */
	state_table[STATE_GET_MESSAGE_14][TOKEN_NUMBER].fptr = act_msg_14;
	state_table[STATE_GET_MESSAGE_14][TOKEN_START].fptr = act_msg_14;
	state_table[STATE_GET_MESSAGE_14][TOKEN_END].fptr = act_msg_14;
	state_table[STATE_GET_MESSAGE_14][TOKEN_DATA].fptr = act_msg_14;
	state_table[STATE_GET_MESSAGE_14][TOKEN_LF].fptr = act_msg_14;

	/* State: STATE_GET_MESSAGE_15 */
	/* Transitions: */
	state_table[STATE_GET_MESSAGE_15][TOKEN_NUMBER].state = STATE_GET_END;
	state_table[STATE_GET_MESSAGE_15][TOKEN_START].state = STATE_GET_END;
	state_table[STATE_GET_MESSAGE_15][TOKEN_END].state = STATE_GET_END;
	state_table[STATE_GET_MESSAGE_15][TOKEN_DATA].state = STATE_GET_END;
	state_table[STATE_GET_MESSAGE_15][TOKEN_LF].state = STATE_GET_END;
	/* Actions: */
	state_table[STATE_GET_MESSAGE_15][TOKEN_NUMBER].fptr = act_msg_15;
	state_table[STATE_GET_MESSAGE_15][TOKEN_START].fptr = act_msg_15;
	state_table[STATE_GET_MESSAGE_15][TOKEN_END].fptr = act_msg_15;
	state_table[STATE_GET_MESSAGE_15][TOKEN_DATA].fptr = act_msg_15;
	state_table[STATE_GET_MESSAGE_15][TOKEN_LF].fptr = act_msg_15;

	/* State: STATE_GET_END */
	/* Transitions: */
	state_table[STATE_GET_END][TOKEN_NUMBER].state = STATE_START;
	state_table[STATE_GET_END][TOKEN_START].state = STATE_START;
	state_table[STATE_GET_END][TOKEN_END].state = STATE_START;
	state_table[STATE_GET_END][TOKEN_DATA].state = STATE_START;
	state_table[STATE_GET_END][TOKEN_LF].state = STATE_START;
	/* Actions */
	state_table[STATE_GET_END][TOKEN_NUMBER].fptr = act_reset;
	state_table[STATE_GET_END][TOKEN_START].fptr = act_reset;
	state_table[STATE_GET_END][TOKEN_END].fptr = act_write_line;
	state_table[STATE_GET_END][TOKEN_DATA].fptr = act_reset;
	state_table[STATE_GET_END][TOKEN_LF].fptr = act_reset;

	return;

}

/***************************************************************************************
* Functions: act_***
*  These functions describe actions the state machine can perform.  They are referenced
*  in the transition table
****************************************************************************************/
void act_reset()
{
	reset_timestamp();
	return;
}

void act_nop()
{
	//Do nothing
	return;
}
// void act_write_line()
//****************************
// This function is called when a complete packet has been received from
// the vehicle embedded system.  It sets the gl_update_flag to let the 
// parser know this.
void act_write_line()
{
  gl_update_flag = 1;
  return;
}

void act_get_number()
{
	timestamp_string[timestamp_ref] = fsm_input;   //Store most recent character
	timestamp_ref++;							   //Increment reference		
	timestamp_string[timestamp_ref] = '\0';		   //Terminate the string with a null zero

	return;
}

void act_msg_0()
{
	//Get steering LSB
	telem_steering = (unsigned short)fsm_input;

	return;
}

void act_msg_1()
{
	//Get steering MSB
	telem_steering |= ((unsigned short)fsm_input << 8);

	return;
}

void act_msg_2()
{
	//Get throttle LSB
	telem_throttle = (unsigned short)fsm_input;

	return;
}

void act_msg_3()
{
	//Get throttle MSB
	telem_throttle |= ((unsigned short)fsm_input << 8);

	return;
}

void act_msg_4()
{
	//Get aux LSB
	telem_aux = (unsigned short)fsm_input;

	return;
}

void act_msg_5()
{
	//Get aux MSB
	telem_aux |= ((unsigned short)fsm_input << 8);

	return;
}

void act_msg_6()
{
	//Get engine_vel LSB
	telem_engine_vel = (unsigned short)fsm_input;

	return;
}

void act_msg_7()
{
	//Get engine_vel MSB
	telem_engine_vel |= ((unsigned short)fsm_input << 8);

	return;
}

void act_msg_8()
{
	//Get front right wheel velocity LSB
	telem_fr_vel = (unsigned short)fsm_input;

	return;
}

void act_msg_9()
{
	//Get front right wheel velocity MSB
	telem_fr_vel |= ((unsigned short)fsm_input << 8);

	return;
}

void act_msg_10()
{
	//Get front left wheel velocity LSB
	telem_fl_vel = (unsigned short)fsm_input;

	return;
}

void act_msg_11()
{
	//Get front left wheel velocity MSB
	telem_fl_vel |= ((unsigned short)fsm_input << 8);

	return;
}

void act_msg_12()
{
	//Get rear right wheel velocity LSB
	telem_rr_vel = (unsigned short)fsm_input;

	return;
}

void act_msg_13()
{
	//Get rear right wheel velocity MSB
	telem_rr_vel |= ((unsigned short)fsm_input << 8);

	return;
}

void act_msg_14()
{
	//Get rear left wheel velocity LSB
	telem_rl_vel = (unsigned short)fsm_input;

	return;
}

void act_msg_15()
{
	//Get rear left wheel velocity MSB
	telem_rl_vel |= ((unsigned short)fsm_input << 8);

	return;
}

