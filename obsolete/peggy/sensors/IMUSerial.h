//IMUSerial.h
//Serial packet processing for the IMU.
//Jon Becker
//Cloud Cap Technology 2003
#ifndef IMUSERIAL_H
#define IMUSERIAL_H

#include "Types.h"
#include "IMUExternalTypes.h"

//Define serial sync characters
#define SYNC_SER_0 0x55
#define SYNC_SER_1 0xAA

// serial port
//const int IMU_COM = 0;
#define IMU_HS_LEN 24

//Define message header length
#define MESSAGE_HEADER_LEN 4 //2 sync characters, type, length
//Define maximum message length in bytes, including the header
#define MAX_MESSAGE_LEN (MESSAGE_HEADER_LEN+18+2)

#if defined(__cplusplus)
extern "C"
{
#endif

int imu_parse_char(IMUSensors_t* pIMUSensors, char* sbuffer, int packet_len, int& parsed);

#if defined(__cplusplus)
}
#endif 

void ReceiveIMUMessage(IMUSensors_t* pIMUSensors,uint8 MessageType,uint8* pMsgBuf,uint8 MessageLength);

void SendIMUSettingsSerialMessage(float32 SampleRateHz,uint16 OversampleRatio,uint8 CommsMode, uint8 SampleMode);
void RequestIMUConfigSerialMessage(void);
void SendIMUEnableBootloaderSerialMessage(void);
void TransmitIMUSerialMessage(uint8 MessageType, uint8* pMessageBuffer,uint8 len);
void Unstuff3VarsSeqNum(uint8* MessageData,uint16 *v1, uint16 *v2, uint16 *v3,uint8 *seqnum);

#endif
