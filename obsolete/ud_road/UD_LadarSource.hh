//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_LadarSource
//----------------------------------------------------------------------------

#ifndef UD_LADARSOURCE_DECS

//----------------------------------------------------------------------------

#define UD_LADARSOURCE_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glut.h>

#include "UD_ImageSource.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef NONE
#define NONE    -1         // convention when end frame number or iteration must be supplied
#endif


#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))

//-------------------------------------------------
// class
//-------------------------------------------------

class UD_LadarCalibration
{
  
 public:

  // variables

  int num_rays;                        ///< number of ladar "rays" sent out in each scan
  float first_ray_theta;               ///< angle of first ray (0 = straight ahead, + = to the right) (in degrees)
  float last_ray_theta;                ///< angle of last ray 
  float ray_delta_theta;               ///< angle between adjacent rays (in degrees)

  float *sin_pan_angle;                ///< precomputed from the absolute angle for each ray
  float *cos_pan_angle;                ///< precomputed from the absolute angle for each ray

  float dx;                            ///< + = right displacement of ladar from vehicle origin (in meters)
  float dy;                            ///< + = down displacement
  float dz;                            ///< + = forward displacement

  float max_range;                     ///< highest range value that can be returned (in meters)

  float pitch_angle;                   ///< radians off horizontal (- = below horizontal)
  float sin_pitch_angle;
  float cos_pitch_angle;

  // functions

  UD_LadarCalibration(int, float, float, float, float, float, float);

};

//-------------------------------------------------

class UD_LadarSource
{
  
 public:

  // variables

  UD_LadarCalibration *cal;            ///< intrinsic and extrinsic calibration of ladar sensor
  int num_rays;                        ///< duplicate of what's in cal
  float win_dx, win_dy;                ///< translation from image coordinates to window coordinates

  float *range;                        ///< distance to ray hit point (in meters) 
  float *vehicle_x;                    ///< vehicle x coordinate of ray hit points
  float *vehicle_y;                    ///< vehicle y coordinate of ray hit points
  float *vehicle_z;                    ///< vehicle z coordinate of ray hit points
  float *image_x, *image_y;            ///< image coordinates (in specified calibrated camera) of ray hit points

  double timestamp;                    ///< when scan was captured (in seconds)
  double diff;                         ///< time discrepancy with current camera frame (in seconds)
  double max_diff;                     ///< maximum time discrepancy with camera to allow computation/display (in seconds)

  // functions

  UD_LadarSource(UD_LadarCalibration *);

  virtual void capture() = 0;
  virtual void capture(double) = 0;
  virtual void capture(UD_ImageSource *) = 0;
  void draw(UD_CameraCalibration *);

  void ladar2image(int, float, float *, float *, float *, UD_CameraCalibration *);
  void ladar2leftimage(int, float, float *, float *, float *);
  void ladar2vehicle(int, float, float *, float *, float *);
  void compute_image_positions(UD_CameraCalibration *);

};

//-------------------------------------------------

class UD_LadarSource_Skynet : public UD_LadarSource
{
  
 public:

  // variables

  FILE *scans_fp;

  // functions

  UD_LadarSource_Skynet(UD_LadarCalibration *);

  void capture();

};

//-------------------------------------------------

class UD_LadarSource_Scans : public UD_LadarSource
{
  
 public:

  // variables

  FILE *scans_fp;

  // functions

  UD_LadarSource_Scans(char *, UD_LadarCalibration *);

  void capture();
  void capture(double);
  void capture(UD_ImageSource *);

  void process_command_line_flags(int, char **);
  void eat_comments(FILE *);
  int read_scan(FILE *);
  int peek_next_scan_timestamp(FILE *);

};

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
