//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_Ladar_GapTracker.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for ladar gap tracker class derived from particle filter class

UD_Ladar_GapTracker::UD_Ladar_GapTracker(UD_LadarSource *lad, int num_particles, UD_Vector *meanstate, UD_Vector *covdiag) : UD_ParticleFilter(num_particles, meanstate, covdiag)
{
  // local names 
  
  ladsrc = lad;

  // default values

  ladar_hist_bins = 30;
  ladar_hist = (float *) calloc(ladar_hist_bins, sizeof(float));
  ladar_hist_range = 30.0;
  ladar_hist_bin_width = (float) ladar_hist_range / (float) ladar_hist_bins;

  gapwidth = 4.0;

  show_particles = TRUE;

  initialize_prior();
}

//----------------------------------------------------------------------------

void UD_Ladar_GapTracker::iterate()
{
  int i, bin;

  for (i = 0; i < ladar_hist_bins; i++) 
    ladar_hist[i] = -1;
  
  for (i = 0; i < ladsrc->num_rays; i++) {
    bin = get_bin(ladsrc->vehicle_x[i], ladsrc->vehicle_z[i]);
    //    printf("%i %f %i\n", i, ladsrc->vehicle_x[i], bin);
    if (fabs(ladsrc->vehicle_y[i]) > ladar_hist[bin])
      ladar_hist[bin] = fabs(ladsrc->vehicle_y[i]);
  }  

  //  exit(1);

//   for (i = 0; i < ladar_hist_bins; i++) 
//     printf("%i %f\n", i, ladar_hist[i]);
//   printf("\n");

  update();
}

//----------------------------------------------------------------------------

/// put particles in their a priori states, uniformly distributed over					
/// ladar angular range

void UD_Ladar_GapTracker::pf_samp_prior(UD_Vector *samp)
 {
  //  samp->x[0] = ranged_uniform_UD_Random(ladsrc->cal->first_ray_theta, ladsrc->cal->last_ray_theta);
  samp->x[0] = ranged_uniform_UD_Random(-0.5 * ladar_hist_range, 0.5 * ladar_hist_range);
}

//----------------------------------------------------------------------------

/// apply deterministic motion model to particles

void UD_Ladar_GapTracker::pf_dyn_state(UD_Vector *old_samp, UD_Vector *new_samp)
{
  copy_UD_Vector(old_samp, new_samp);
}

//----------------------------------------------------------------------------

/// random diffusion: add Gaussian noise to old state sample 

void UD_Ladar_GapTracker::pf_samp_state(UD_Vector *old_samp, UD_Vector *new_samp)
{
  sample_gaussian_diagonal_covariance(new_samp, old_samp, covdiagsqrt);
}

//----------------------------------------------------------------------------

/// evaluate likelihood of one particle given current image

double UD_Ladar_GapTracker::pf_condprob_zx(UD_Vector *samp)
{
  float x, sum, num_bins;
  int bin;

  // gate position to stay withing ladar angular limits

//   if (candidate_vp_x(samp) < 0 || candidate_vp_x(samp) >= vp->cw || candidate_vp_y(samp) < 0 || candidate_vp_y(samp) >= vp->ch)
//     return 0;
  if (samp->x[0] < -0.5 * ladar_hist_range || samp->x[0] > 0.5 * ladar_hist_range)
    return 0;

//   // bilinear interpolation would be a little better...
// 
//   ix = myround(candidate_vp_x(samp));
//   iy = myround(candidate_vp_y(samp));
// 
//   return FLOAT_IMXY(vp->candidate_image, ix, iy);

/*
  for (sum = 0, num_bins = 0, bin = get_bin(samp->x[0] - 0.5 * gapwidth); bin < get_bin(samp->x[0] + 0.5 * gapwidth); bin++)
    if (bin >= 0 && bin < ladar_hist_bins) {
      sum += ladar_hist[bin];
      num_bins++;
    }
 */

  //  printf("sum %f\n", exp(-sum/num_bins));
  //  return 1.0/(sum + 1.0);
  return 1.0;
}

//----------------------------------------------------------------------------

/// write current estimate of vanishing point POSITION (in image coordinates)
/// to file (terminal string is assumed to be newline)

void UD_Ladar_GapTracker::write(FILE *fp)
{
  //  write(fp, "\n");
}

//----------------------------------------------------------------------------

/// write current estimate of vanishing point POSITION (in image coordinates)
/// to file with an argument specifying terminal string (i.e., newline, 
/// comma-space, etc.)

void UD_Ladar_GapTracker::write(FILE *fp, char *terminal_string)
{
//   fprintf(fp, "%.2f, %.2f%s", vp_x(), vp_y(), terminal_string);
//   fflush(fp);
}

//----------------------------------------------------------------------------

/// draw road curvature tracker parameters, namely the POSITION of the
/// current road vanishing point.  assuming window dimensions are same as
/// candidate region

void UD_Ladar_GapTracker::draw()
{
  int i;

  // particles are yellow lines

  /*
  if (show_particles) {

    glColor3ub(255, 255, 0);
    glPointSize(1);

    glBegin(GL_POINTS);

    for (i = 0; i < num_samples; i++) 
      glVertex2f(sample[i]), win_h - candidate_vp_y(sample[i]));

    glEnd();
  }
  */

  // state is thick blue line

  /*
  glColor3ub(0, 0, 255);
  glPointSize(5);

  glBegin(GL_POINTS);

  glVertex2f(candidate_vp_x(), win_h - candidate_vp_y());

  glEnd();
  */
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
