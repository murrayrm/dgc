//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_OnOff
//----------------------------------------------------------------------------

#ifndef UD_ONOFF_DECS

//----------------------------------------------------------------------------

#define UD_ONOFF_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "opencv_utils.hh"

#include "UD_ImageSource.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))

//-------------------------------------------------
// class
//-------------------------------------------------

class UD_OnOff
{
  
 public:

  // variables

  float KL_threshold;                        ///< minimum KL divergence between vote function and uniform distribution to think 
                                             ///< that this image is good
  int KL_time_window;                        ///< how many past images to take into account for filtering on/off road decision 
  float KL_majority_fraction;                ///< what fraction of images in time window must be good for filtering
  int print_onoff;                           ///< print on/off road decision parameters as they are computed?
  int do_onoff;                              ///< carry out on/off road decision computations on this image?

  int KL_index;                              ///< index in time window-sized buffer that current image on/off road confidence occupies
  float *KL_history;                         ///< time window-sized buffer of image on/off road confidence values
  int KL_history_full;                       ///< flag indicating whether KL_time_window frames have been processed yet
  int KL_num_vote_levels;                    ///< maximum number of votes a particular location in vote function can receive
  float *KL_histogram;                       ///< distribution of how many locations (pixels) in vote function had how many votes
  float KL_uniform_prob;                     ///< 1 / KL_num_vote_levels in every bin of KL_histogram is uniform distribution

  float onroad_confidence;                   ///< fraction of measured KL values in time window that exceed KL_threshold
  int onroad_state;                          ///< TRUE if we believe vehicle can see a road, FALSE if not (confidence >= majority fraction) 

  IplImage *redim;                           ///< scratch image for reddening display when off-road

  // functions

  UD_OnOff(UD_ImageSource *, float, int, float);

  void compute_confidence(IplImage *);
  float KL_from_uniform(IplImage *);
  void process_command_line_flags(int, char **);

};

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
