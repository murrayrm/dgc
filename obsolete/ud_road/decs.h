//----------------------------------------------------------------------------
//  Road follower module
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Doxygen main page
//----------------------------------------------------------------------------

/// \mainpage
///
/// \htmlinclude ../manual.html

//----------------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------------

#ifndef DECS_DECS

//----------------------------------------------------------------------------

#define DECS_DECS

// General

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// OpenGL/GLUT includes

#include <GL/glut.h>
#include "glx.h"
#include "glext.h"

#include "opencv_utils.hh"

// My stuff

#include "UD_Random.hh"
#include "UD_Linux_Time.hh"
#include "UD_Error.hh"
#include "UD_Linux_Movie_AVI.hh"
#include "UD_DominantOrientations.hh"
#include "UD_VanishingPoint.hh"
#include "UD_FirewireCamera.hh"
#include "UD_RoadFollower.hh"
#include "UD_OnOff.hh"
#include "UD_LadarSource.hh"
#include "UD_ImageSource.hh"
#include "UD_Logger.hh"
#include "UD_Overhead.hh"

//----------------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------------

#define NONE    -1         // convention when end frame number or iteration must be supplied

// general 

#ifndef MAXSTRLEN
#define MAXSTRLEN  256
#endif

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

//----------------------------------------------------------------------------

void process_image();
void initialize_display(int, int, int, char **);
void display();
void keyboard(unsigned char, int, int);
void mouse(int, int, int, int);
void fake_display();
void compute_display_loop();
void init_per_run(int, char **);
void process_general_command_line_flags(int, char **);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif
