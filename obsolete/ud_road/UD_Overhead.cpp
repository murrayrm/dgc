//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_Overhead.hh"
	
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// horrible kludge to get GLUT callback functions into class

UD_Overhead *UD_Overhead::overhead = new UD_Overhead(40, 60, 4.0, NULL);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for overhead viewing class.  units are meters

UD_Overhead::UD_Overhead(float mapw, float lookahead_distance, float pixels_per_meter, UD_RoadFollower *roadfollower)
{
  // local names

  rf = roadfollower;

  pix_per_meter = pixels_per_meter;
  map_width = mapw;
  map_depth = lookahead_distance;

  // default values

  vehicle_offset = 5;

  vehicle_width                  = 2.0;
  vehicle_front_axle_to_bumper   = 0.5;
  vehicle_rear_axle_to_bumper    = 0.5;
  vehicle_wheelbase              = 4.0;

  // body 

  glcircle = gluNewQuadric();
  gluQuadricDrawStyle(glcircle, GLU_LINE);
  //gluQuadricDrawStyle(glcircle, GLU_FILL);
}

//----------------------------------------------------------------------------

/// initialize overhead display functions, open window, etc.

void UD_Overhead::initialize_display(int x_position, int y_position)
{
  width = (int) ceil(map_width * pix_per_meter);
  height = (int) ceil((vehicle_offset + map_depth) * pix_per_meter);

  glutInitWindowSize(width, height);
  glutInitWindowPosition(x_position, y_position);
  window_id = glutCreateWindow("overhead");

  glutDisplayFunc(sdisplay); 
  glutMouseFunc(smouse); 
  glutKeyboardFunc(skeyboard);

  glPixelZoom(1, -1);
  glRasterPos2i(-1, 1);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, width-1, 0, height-1);
}

//----------------------------------------------------------------------------

/// in vehicle coordinates (meters)

void UD_Overhead::draw_circle(float x, float z, float radius, int r, int g, int b)
{
  float theta, delta_theta;

  delta_theta = 0.1;

  glColor3ub(255-r, 255-g, 255-b);

  glBegin(GL_LINES);

  glVertex2f(v2o_x(x - 0.5), v2o_z(z));
  glVertex2f(v2o_x(x + 0.5), v2o_z(z));

  glVertex2f(v2o_x(x), v2o_z(z - 0.5));
  glVertex2f(v2o_x(x), v2o_z(z + 0.5));

  glEnd();

  glColor3ub(r, g, b);

  glBegin(GL_LINE_LOOP);

  for (theta = 0; theta < 2*PI; theta += delta_theta) 
    glVertex2f(v2o_x(x + radius * cos(theta)), v2o_z(z+ radius * sin(theta)));

  glEnd();
}

//----------------------------------------------------------------------------

void UD_Overhead::draw_gap()
{
  int i, bin;
  float r, g;

  draw_circle(0, 10, 2, 0, 255, 0);

  //  return;

  float dx, dy, x0, y0, x, val;

  x0 = v2o_x(0.0);
  y0 = v2o_z(0.0);
  dx = tan(rf->gap_tracker->direction_error) * map_depth;
  dy = (float) height;

//   for (i = 0; i < rf->gap_tracker->ladar_hist_bins; i++)
//     printf("%i %f\n", i, rf->gap_tracker->ladar_hist[i]);

  glBegin(GL_POINTS);

  glPointSize(1);

  for (i = 0; i < rf->ladsrc->num_rays; i++) {
    x = rf->gap_tracker->bin_project(rf->ladsrc->vehicle_x[i], rf->ladsrc->vehicle_z[i]);
    
    val = rf->ladsrc->vehicle_y[i];
    r = 5.0*val;
    g = 5.0*(1 - val);
    glColor3f(MIN2(r,1), MAX2(g,0),0);
    glVertex2f(v2o_x(x), y0);
  }

  glEnd();


  glLineWidth(1);
  glBegin(GL_LINES);

  for (i = 0; i < rf->gap_tracker->ladar_hist_bins; i++) {

    x = rf->gap_tracker->get_bin_x(i);
    val = rf->gap_tracker->ladar_hist[i];

    //    val = rf->gap_tracker->get_bin_val(x);
    
    if (val != -1 && val >= 0.5) {
      r = 5.0*val;
      g = 5.0*(1 - val);
      glColor3f(MIN2(r,1), MAX2(g,0),0);
      glVertex2f(v2o_x(x), y0);
      glVertex2f(v2o_x(x + dx), y0 + dy);
    }


      //    glColor3f(0,1,0);

    /*
    glVertex2f(v2o_x(x), v2o_z(-1.0));
    glVertex2f(v2o_x(x+rf->gap_tracker->ladar_hist_bin_width), v2o_z(-1.0));
    */

  }

  glEnd();

  return;

  // particles are yellow lines

  //  if (rf->gap_tracker->show_particles) {
  if (0) {

    glColor3ub(255, 255, 0);
    glLineWidth(1);

    glBegin(GL_LINES);

    for (i = 0; i < rf->gap_tracker->num_samples; i++) {
      glVertex2f(v2o_x(rf->gap_tracker->sample[i]->x[0] - 0.5 * rf->gap_tracker->gapwidth), v2o_z(2.0));
      glVertex2f(v2o_x(rf->gap_tracker->sample[i]->x[0] + 0.5 * rf->gap_tracker->gapwidth), v2o_z(2.0));
    }

    glEnd();
  }

  // state is thick blue line

  glColor3ub(0, 0, 255);
  glLineWidth(2);
  
  glBegin(GL_LINES);
  
  for (i = 0; i < rf->gap_tracker->num_samples; i++) {
    glVertex2f(v2o_x(rf->gap_tracker->state->x[0] - 0.5 * rf->gap_tracker->gapwidth), v2o_z(2.0));
    glVertex2f(v2o_x(rf->gap_tracker->state->x[0] + 0.5 * rf->gap_tracker->gapwidth), v2o_z(2.0));
  }
  
  glEnd();
    
  /*
  glColor3ub(0, 0, 255);
  glPointSize(5);

  glBegin(GL_POINTS);

  glVertex2f(candidate_vp_x(), win_h - candidate_vp_y());

  glEnd();
  */
}

//----------------------------------------------------------------------------

/// draw ladar hit points in vehicle coordinates, with color indicating 
/// height

void UD_Overhead::draw_ladar(UD_LadarSource *lad)
{ 
  int i;
  float r, g, x0, y0, vx, vy, vz;

  x0 = v2o_x(0.0);
  y0 = v2o_z(lad->cal->dz);

  // limits

  /*
  glColor3ub(200, 0, 0);

  glBegin(GL_LINES);

  glVertex2f(x0, y0);
  glVertex2f(v2o_x(lad->cal->max_range * sin(DEG2RAD(lad->cal->first_ray_theta))), 
	     v2o_z(lad->cal->dz + lad->cal->max_range * cos(DEG2RAD(lad->cal->first_ray_theta))));

  glVertex2f(x0, y0);
  glVertex2f(v2o_x(lad->cal->max_range * sin(DEG2RAD(lad->cal->last_ray_theta))), 
	     v2o_z(lad->cal->dz + lad->cal->max_range * cos(DEG2RAD(lad->cal->last_ray_theta))));


  glColor3ub(200, 0, 0);

  lad->ladar2vehicle(0, lad->cal->max_range, &vx, &vy, &vz);
  glVertex2f(x0, y0);
  glVertex2f(v2o_x(vx), v2o_z(vz));

  lad->ladar2vehicle(lad->num_rays - 1, lad->cal->max_range, &vx, &vy, &vz);
  glVertex2f(x0, y0);
  glVertex2f(v2o_x(vx), v2o_z(vz));

  glEnd();
  */

  // hit points

  if (lad->diff < lad->max_diff) {

    glPointSize(2);
    glBegin(GL_POINTS);

    for (i = 0; i < lad->num_rays; i++) {
      r = 5.0*fabs(lad->vehicle_y[i]);
      g = 5.0*(1 - fabs(lad->vehicle_y[i]));
      glColor3f(MIN2(r,1), MAX2(g,0),0);

      glVertex2f(v2o_x(lad->vehicle_x[i]), v2o_z(lad->vehicle_z[i]));

    }
    glEnd();
  }

  //  rf->gap_tracker->draw();
}

//----------------------------------------------------------------------------

/// draw schematic overhead view of estimates curvature, boundaries of
/// road ahead

void UD_Overhead::draw_road()
{
  float dx, dy, x0, y0, x, road_half_width, cos_theta;

  dy = (float) height;
  dx = tan(rf->direction_error) * dy;
  x0 = v2o_x(0.0);
  y0 = v2o_z(0.0);

  dx = tan(rf->direction_error) * map_depth;
  //  cos_theta = cos(rf->direction_error);
  cos_theta = 1 + tan(rf->direction_error);
  road_half_width = 2.0;

  glColor3ub(100, 100, 100);

  glBegin(GL_LINES);

  if (rf->gap_tracker) {
    for (x = -0.5 * rf->gap_tracker->ladar_hist_range; x < 0.5 * rf->gap_tracker->ladar_hist_range; x += rf->gap_tracker->ladar_hist_bin_width) {   
      glVertex2f(v2o_x(x), y0);
      glVertex2f(v2o_x(x + dx), y0 + dy);
    }
  }
  else {
    for (x = -width/2; x < 3*width/2; x += 10) {   
      glVertex2f(x, y0);
      glVertex2f(x + dx, y0 + dy);
    }
  }

  /*
  glColor3ub(255, 0, 255);

  glVertex2f(v2o_x(rf->lateral_offset), v2o_z(0.0));
  glVertex2f(v2o_x(rf->lateral_offset + dx), v2o_z(map_depth));

  glColor3ub(255, 255, 0);

  glVertex2f(v2o_x(rf->lateral_offset - cos_theta * road_half_width), v2o_z(0.0));
  glVertex2f(v2o_x(rf->lateral_offset + dx - cos_theta * road_half_width), v2o_z(map_depth));

  glVertex2f(v2o_x(rf->lateral_offset + cos_theta * road_half_width), v2o_z(0.0));
  glVertex2f(v2o_x(rf->lateral_offset + dx + cos_theta * road_half_width), v2o_z(map_depth));
  */

  glEnd();

}

//----------------------------------------------------------------------------

/// draw schematic overhead view of vehicle assuming midpoint of front axle
/// is origin of vehicle coordinate system

void UD_Overhead::draw_vehicle()
{
  glLineWidth(1);

  // line representing front of vehicle (even with front axle)

  /*
  glColor3ub(0, 0, 255);

  glBegin(GL_LINES);

  glVertex2f(0, v2o_z(0.0));
  glVertex2f(width-1, v2o_z(0.0));

  glEnd();
  */

  // rectangle representing outline of vehicle

  glColor3ub(255, 0, 255);

  glBegin(GL_LINE_LOOP);
  
  glVertex2f(v2o_x(-0.5 * vehicle_width), v2o_z(vehicle_front_axle_to_bumper));
  glVertex2f(v2o_x(0.5 * vehicle_width), v2o_z(vehicle_front_axle_to_bumper));
  glVertex2f(v2o_x(0.5 * vehicle_width), v2o_z(-(vehicle_wheelbase + vehicle_rear_axle_to_bumper)));
  glVertex2f(v2o_x(-0.5 * vehicle_width), v2o_z(-(vehicle_wheelbase + vehicle_rear_axle_to_bumper)));

  glEnd();

}

//----------------------------------------------------------------------------

void UD_Overhead::display()
{
  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT);

  if (rf && rf->ladsrc) {
    draw_gap();
    draw_ladar(rf->ladsrc);
  }
  else
    draw_road();
  draw_vehicle();

  glutSwapBuffers();
}

//----------------------------------------------------------------------------

void UD_Overhead::keyboard(unsigned char key, int x, int y)
{
  printf("overhead key\n");
}

//----------------------------------------------------------------------------

void UD_Overhead::mouse(int button, int state, int x, int y)
{
  printf("overhead mouse\n");
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
