//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_VP_VelocityTracker
//----------------------------------------------------------------------------

#ifndef UD_VP_VELOCITYTRACKER_DECS

//----------------------------------------------------------------------------

#define UD_VP_VELOCITYTRACKER_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include "UD_VP_Tracker.hh"

//-------------------------------------------------
// class
//-------------------------------------------------

class UD_VP_VelocityTracker : public UD_VP_Tracker
{
  
 public:

  float max_velocity;

  UD_VP_VelocityTracker(UD_VanishingPoint *, int, UD_Vector *, UD_Vector *, UD_Vector *);

  void draw();
  void write(FILE *);
  void write(FILE *, char *);

  void pf_samp_prior(UD_Vector *);
  void pf_samp_state(UD_Vector *, UD_Vector *);
  void pf_dyn_state(UD_Vector *, UD_Vector *);
  double pf_condprob_zx(UD_Vector *);

  void set_show_particles(int value) { show_particles = value; }

  float candidate_vp_x(UD_Vector *v) { return v->x[0]; }       ///< x coordinate of VP location in generic state vector
  float candidate_vp_y(UD_Vector *v) { return v->x[1]; }       ///< y coordinate of VP location in generic state vector 
  float candidate_vp_vx(UD_Vector *v) { return v->x[2]; }      ///< x component of VP velocity in generic state vector
  float candidate_vp_vy(UD_Vector *v) { return v->x[3]; }      ///< y component of VP velocity in generic state vector 
  float vp_x(UD_Vector *v) { return vp->can2im_x(v->x[0]); }   ///< x coordinate of VP location in generic state vector (image coords.)
  float vp_y(UD_Vector *v) { return vp->can2im_y(v->x[1]); }   ///< y coordinate of VP location in generic state vector (image coords.)
  float vp_vx(UD_Vector *v) { return v->x[2]; }                ///< x component of VP velocity in generic state vector (image coords.)
  float vp_vy(UD_Vector *v) { return v->x[3]; }                ///< y component of VP velocity in generic state vector (image coords.)

  float candidate_vp_x() { return candidate_vp_x(state); }     ///< x coordinate of estimated VP location in candidate region
  float candidate_vp_y() { return candidate_vp_y(state); }     ///< y coordinate of estimated VP location in candidate region
  float candidate_vp_vx() { return candidate_vp_vx(state); }   ///< x component of estimated VP velocity in candidate region
  float candidate_vp_vy() { return candidate_vp_vy(state); }   ///< y component of estimated VP velocity in candidate region
  float vp_x() { return vp_x(state); }                         ///< x coordinate of estimated VP location (image coords.)
  float vp_y() { return vp_y(state); }                         ///< y coordinate of estimated VP location (image coords.)
  float vp_vx() { return vp_vx(state); }                       ///< x component of estimated VP velocity (image coords.)
  float vp_vy() { return vp_vy(state); }                       ///< y component of estimated VP velocity (image coords.)
  
};

UD_VP_VelocityTracker *initialize_UD_VP_VelocityTracker(UD_VanishingPoint *);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
