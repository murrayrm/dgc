//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_VP_PositionTracker.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// wrapper around vanishing point 2-D POSITION tracker class constructor.  sets 
/// up some variables necessary for the particle filter (from which the 
/// VP position tracker class is derived) to be initialized properly--particularly 
/// in the initial distribution of particles.   uses defaults for a priori 
/// position grid spacing, sampling covariance

UD_VP_PositionTracker *initialize_UD_VP_PositionTracker(UD_VanishingPoint *vp)
{
  UD_VP_PositionTracker *pt;
  int i, num_samps;
  float fw;
  UD_Vector *initstate, *dstate, *covdiag;

  // spacing of initial grid of particles (in units of pixels)

  dstate = make_UD_Vector("pf_dstate", 2);
  dstate->x[0] = 5;
  dstate->x[1] = 5;
  
  // first position in initial grid

  initstate = make_UD_Vector("pf_state", 2);
  initstate->x[0] = 0.5 * dstate->x[0];
  initstate->x[1] = 0.5 * dstate->x[1];

  // compute and report number of grid points (one particle per)

  num_samps = vp->cw * vp->ch / (dstate->x[0] * dstate->x[1]);
  printf("num samps = %i (w = %i, h = %i, dx = %.1f, dy = %.1f)\n", num_samps, vp->cw, vp->ch, dstate->x[0], dstate->x[1]);

  // sampling covariance

  // var = sigma^2, 95% within 2*sigma
  // horizontal: 2*sigma = pf_w / 40 => sigma = pf_w / 80 
  // vertical: 2*sigma = pf_w / 57 => sigma = pf_w / 114

  fw = (float) vp->cw;
  
  covdiag = make_UD_Vector("covdiag", 2);
   covdiag->x[0] = SQUARE(fw / 80.0);
   covdiag->x[1] = SQUARE(fw / 114.0);
//    covdiag->x[0] = .1;
//    covdiag->x[1] = .01;
  
  // instantiate

  pt = new UD_VP_PositionTracker(vp, num_samps, initstate, covdiag, dstate);

  return pt;
}

//----------------------------------------------------------------------------

/// constructor for vanishing point POSITION tracker class derived from 
/// vanishing point tracker class

UD_VP_PositionTracker::UD_VP_PositionTracker(UD_VanishingPoint *van, int num_particles, UD_Vector *meanstate, UD_Vector *covdiag, UD_Vector *delta) : UD_VP_Tracker(van, num_particles, meanstate, covdiag, delta)
{
  // distribute initial samples (must be here instead of in UD_VP_Tracker constructor
  // because pf_samp_prior() is called, which is not defined by UD_VP_Tracker)
  
  initialize_prior();
}

//----------------------------------------------------------------------------

/// put particles in their a priori POSITIONS--distribute over regular grid 
/// covering candidate region

void UD_VP_PositionTracker::pf_samp_prior(UD_Vector *samp)
{
  if (state->x[0] >= vp->cw) {
    state->x[0] = 0.5 * dstate->x[0];
    state->x[1] += dstate->x[1];
  }

  samp->x[0] = state->x[0];
  samp->x[1] = state->x[1];

  state->x[0] += dstate->x[0];
}

//----------------------------------------------------------------------------

/// identity motion model.  generally, this function applies deterministic 
/// motion model to particles, but since the state is just POSITION, there
/// effectively is no motion model (other than random walking due to pf_samp_state())

void UD_VP_PositionTracker::pf_dyn_state(UD_Vector *old_samp, UD_Vector *new_samp)
{
  copy_UD_Vector(old_samp, new_samp);
}

//----------------------------------------------------------------------------

/// random diffusion: add Gaussian noise to old state sample 

void UD_VP_PositionTracker::pf_samp_state(UD_Vector *old_samp, UD_Vector *new_samp)
{
  sample_gaussian_diagonal_covariance(new_samp, old_samp, covdiagsqrt);
}

//----------------------------------------------------------------------------

/// evaluate likelihood of one particle given current image.  if position is 
/// outside candidate region, likelihood = 0.  if inside, just return number of
/// votes of nearest bucket in candidate region.  states are all POSITIONS
/// in candidate region

double UD_VP_PositionTracker::pf_condprob_zx(UD_Vector *samp)
{
  int ix, iy;

  // gate position to stay in candidate region

  if (candidate_vp_x(samp) < 0 || candidate_vp_x(samp) >= vp->cw || candidate_vp_y(samp) < 0 || candidate_vp_y(samp) >= vp->ch)
    return 0;

  // bilinear interpolation would be a little better...

  ix = myround(candidate_vp_x(samp));
  iy = myround(candidate_vp_y(samp));

  return FLOAT_IMXY(vp->candidate_image, ix, iy);
}

//----------------------------------------------------------------------------

/// write current estimate of vanishing point POSITION (in image coordinates)
/// to file (terminal string is assumed to be newline)

void UD_VP_PositionTracker::write(FILE *fp)
{
  write(fp, "\n");
}

//----------------------------------------------------------------------------

/// write current estimate of vanishing point POSITION (in image coordinates)
/// to file with an argument specifying terminal string (i.e., newline, 
/// comma-space, etc.)

void UD_VP_PositionTracker::write(FILE *fp, char *terminal_string)
{
  fprintf(fp, "%.2f, %.2f%s", vp_x(), vp_y(), terminal_string);
  fflush(fp);
}

//----------------------------------------------------------------------------

/// draw road curvature tracker parameters, namely the POSITION of the
/// current road vanishing point.  assuming window dimensions are same as
/// candidate region

void UD_VP_PositionTracker::draw()
{
  int i, win_h, win_w;

  win_w = vp->cw;
  win_h = vp->ch;

  // particles are yellow dots

  if (show_particles) {
    glColor3ub(255, 255, 0);
    glPointSize(1);

    glBegin(GL_POINTS);

    for (i = 0; i < num_samples; i++) 
      glVertex2f(candidate_vp_x(sample[i]), win_h - candidate_vp_y(sample[i]));

    glEnd();
  }

  // horizon is red line

  glColor3ub(255, 0, 0);
  glLineWidth(1);

  glBegin(GL_LINES);
  
  glVertex2f(0, win_h - candidate_vp_y());
  glVertex2f(win_w, win_h - candidate_vp_y());

  glEnd();

  // state is big blue dot

  glColor3ub(0, 0, 255);
  glPointSize(5);

  glBegin(GL_POINTS);

  glVertex2f(candidate_vp_x(), win_h - candidate_vp_y());

  glEnd();
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
