/**
 * ModeManModule.hh
 *  12/26/2004     Sue Ann Hong       Created based on ArbiterModule.hh
 **/

#ifndef MODEMANMODULE_HH
#define MODEMANMODULE_HH

#include <time.h>
#include <string>

#include "StateClient.h"
#include "TrajTalker.h"
#include "traj.h"
#include "CMapPlus.hh"
#include "trajMap.hh"
//#include "ModeMan.hh"

#include <vector> // for a vector of vote log files

using namespace std;

#define SPEEDLIMIT_RESET_AGE 10000000 //10s
#define DEFAULT_GLOBAL_SPEEDLIMIT 10.0 //m/s

#define MAX_DELTA_SIZE  250000

/* Constants */
namespace ModeManModes {
  // In order of preference.
  enum {
    FAST,         // RDDF                                                      
    NORMAL,       // NTG                                                       
    SLOW,         // reactive                                                  
    PASSTHROUGH,
    REVERSE,
    STOP,
    COUNT    // [place holder] the total number of modes.                      
  };
};

// Enumerate all mode manager inputs                                           
// PLEASE do not change the order of these, the order is used to
// identify log files.  Add new ones at the end instead.  
namespace PathInput {
  enum {
    //    THEONE,                                                            
    // ROAD,                                                                  
    RDDF,
    DELIBERATE,
    REACTIVE,
    DEFAULT,      // for testing any module that's sending SNtraj msgs
    COUNT    // [place holder] the total number of inputs.                     
  };

  // For display only.                                                         
  static const char* const Strings[] = {
    //    THEONE,                                                              
    // "ROAD",                                                                 
    "RDDF",
    "DELIBERATE",
    "REACTIVE",
    "Number of Path Inputs"    // [place holder] the total number of inputs.   
  };
};


/* ModeManModule
 *  The class of all.
 *  It contains the wrapper (ModeManModule) functions,
 *              the core (formally ModeMan) functions,
 *              sparrow functions,
 *          and the deadbaby functions.
 */
class ModeManModule : public CStateClient, public CTrajTalker {
public:
  // Constants
  static const long SLEEP_ACTIVE_LOOP = 100000;     // 10 Hz
  // Mode Management Constants
  static const int PATH_ORDER = 3;
  static const unsigned long long INPUT_TIMEOUT = 100000;  // microsec -> 100ms


  /*********** Constructors *********************************************/
  /* Default constructor does nothing and shouldn't be used */
  ModeManModule(int sn_key, bool noDisplay = false);
  /* TODO: Need an operator= overload & a copy constructor */
  // Destructor
  ~ModeManModule();

  /******* ModeManModule functions - defined in ModeManModule.cc ********/
  /* The function that loops sending paths and is called by main */
  void ModeManLoop();
  /* Loops and updates paths from input modules */
  void UpdateModulePath_planner();
  void UpdateModulePath_RddfPathGen();
  void UpdateModulePath_Reactive();
  void UpdateModulePath_default();
  void UpdateModulePath(int trajSocket, int src);
  void UpdateDBS();  
  // Update emergency variables from external fault detection
  // (i.e. sensors, actuators, computers, etc)
  void UpdateFaultDetectionVars();

  /******* Sparrow display fns - defined in navdisplay.cc *************/
  void SparrowDisplayLoop();
  void UpdateSparrowVariablesLoop();

  /******* ModeMan core functions - defined in ModeMan.cc ************/
  void InitModeMan(bool noDisplay);
  // Functions used in the main loop (ModeManModule)                           
  /* needToSendPath()                                                           
   *  Currently, the condition is if there's a new path from a module          
   *  or if there's an emergency .                                             
   */
  bool needToSendPath();
  void UpdateModeManPaths();
  void UpdateModeManFaults();
  void ManageFaults(); /* fault management - emergency var's */
  /* Given the current paths and conditions, pick a path to send to path       
     follower */
  int PickNewMode();
  CTraj * getcurPath();
  // check safety of all updated paths, update safety var                      
  int chkInputPathSafety();
  int canSwitchTo(int tryMode);
  int switchTo(int newMode);
  /* getStopPath: modifies curPath to a stopping path based on curPath,
     and returns curPath */
  CTraj *getStopPath();
  int check_global_speedlimit();                  // (RC)                      
  // modify path given to global speedlimit and and broadcast to other modules 
  int apply_global_speedlimit();  // (RC)
  
  void ModeManModule::getMapDeltasThread();

private:
  /******* ModeManModule *******************************************/
  /*! The skynet socket for sending trajectories (to path follower) */
  int modeManSocket;

  /******* ModeMan ************************************************/
  // for data logging                                                          
  int TestNumber;
  string TestNumberStr;
  // Time zero:  initialized in msg handler's ModeManMessages::Start ???       
  unsigned long long TimeZero;

  // For display                                                               
  bool noDisplay;

  /* Paths that get updated by the NewPath msgs */
  CTrajMap *new_path[PathInput::COUNT];      // just arrived from modules         
  bool newPathPresent[PathInput::COUNT];
  int numNewPaths;
  int numMsgsReceived;
  // a timestamp for all path sources (new_path) in microseconds
  unsigned long long lastUpdate[PathInput::COUNT];

  /* Paths we're choosing from */
  CTrajMap *path[PathInput::COUNT];       // paths we're evaluating on            
  double safety[PathInput::COUNT];     // safety of each path                  
  bool present[PathInput::COUNT];      // whether each module is present       
  int numPresent;                      // number of modules present            
  int pathUpdateCount[PathInput::COUNT];

  CTrajMap *curPath;            // the one we're driving on now, just a pointer   
                             // so it points to one of the paths
  double curSafety;          // safety of the curPath at the moment            
                             // frequently updated by deadbaby sensing         
  int curPathModule;         // which path module we're following

  /* Sparrow variables */
  int pathOutCount;          // how many paths have we sent to PF?         
  int updateCountState;      // how many times have we gotten state?
  int PassThroughModule;     // which module should modeman listen to in 
                             // PASSTHROUGH mode?

  int ModeManMode;
  // For each current mode (the first []), there's an array containing         
  // possible next modes, sorted in order of preference.                       
  int switchPList[ModeManModes::COUNT][ModeManModes::COUNT];
  double global_speedlimit;
  bool needToStop;

  // add'l parameters                                                          
  double paranoid;                 // if high, apply more safety               
  int resourceLevel;               // something along the line of keeping      
                                   // track of failed system components        
                                   // maybe a bitmask for critical|major|minor 

  // A count of how many times a source has updated:                           
  //  really for the peace of mind (displayed in sparrow)                      
  int UpdateCount[PathInput::COUNT];
  int CommandSentCount;
  
  //Var used by check_global_spedlimit() and its subfunctions
  double dbsLimit;
  int speedLimitAge;

  CMapPlus				 	m_map;
  int								m_layer;
	char*							m_pMapDelta;
  
  pthread_mutex_t		m_deltaReceivedMutex;
	pthread_cond_t		m_deltaReceivedCond;

	/*! The mutex to protect the map we're planning on */
	pthread_mutex_t		m_mapMutex;

  /*                                                                           
   * Locks for new_path's: each new_path is touched by two threads:            
   *  skynet message receiving thread                                          
   *  main-loop fn that copies new_path to path                                
   *  (possibly path checking, but that'll most likely be part of the          
   *   main thread -- same for path[])                                         
   */
  pthread_mutex_t newPathMutex[PathInput::COUNT];

  /**** Deadbaby sensing functions  ********************************/
  

};

#endif
