/* Applanix.hh
 * 
 * File defining the drivers for the Applanix POS-LV.
 * 
 * Stefano Di Cairano, NOV-06
 * 
 */

#ifndef APPLANIX_HH
#define APPLANIX_HH

#include <string.h>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <time.h>

#define APX_CONTROL_PORT 5601
#define APX_DATA_PORT 5602
#define APX_IP_ADDR "192.168.0.100" 
#define LENGTH_BYTE 6
#define LENGTH_OFFSET 8
#define SEL_MSG_LENGTH 20
#define NAVMOD_MSG_LENGTH 16
#define CONTROL_MSG_LENGTH 16
#define NAV_GRP_LENGTH 140
#define ACK_MSG_LENGTH 52
#define APX_BUFFER_SIZE 4096 //for now we can also set it to 140, this is for test

/*!This file contains the definition of the interfaces with the Applanix POS-LV system.
 * Data-structures and functions fto send/receive data are defined here
 */

/* ************************************
 * ********** APX_DATA STRUCT *********
 * ************************************/
 
/*! Structure contains navigation solution data. POS-LV datagroup 1
 */
 
typedef struct apxNavData
{
  char groupStart[5];      //increased by 1 because of null termination
  unsigned short ID;
  unsigned short byteCount;
	
	// time and distance -> maybe another struct
  double time1;
  double time2;
  double distanceTag;
  char timeTypes;
  char distanceType;
	
  double latitude;
  double longitude;
  double altitude;	
	
  float northVelocity;
  float eastVelocity;
  float downVelocity;
	
  double roll;
  double pitch;
  double heading;
  double wander;
	
  float trackAngle;
  float speed;
	
	
  float rollRate;
  float pitchRate;
  float yawRate;
		
  float longitudinalAcc;
  float transverseAcc;
  float downAcc;
	
  char alignStatus;
  char pad;
  unsigned short checksum;
  char groupEnd[3]; //increased by 1 because of null termination		
};



/*! Function to parse navigation data.
 * \param inBuffer is the pointer to the buffer to be parsed
 * \param size is the length of such a buffer
 * \param msg is the pointer to the the apxNavData struct in which the parsed data are stored
 */
 
void parseNavData(char* inBuffer, int size, apxNavData* msg);




/* *****************************************
 * ************* Navigation Mode ***********
 * *****************************************/


/*! Structure defining the message to switch to Navigation mode. POS-LV message 50 */

typedef struct apxNavModeMsg
{ 
  apxNavModeMsg()    //this is the constructor, it fills the standard fields
  {  
    char init[5]="$MSG";
	char end[3]="$#";
	strcpy(msgStart,init);
	strcpy(msgEnd,end);
	ID=50;
	byteCount=8;
	pad = '\0';
	checksum=0;
  }
  char msgStart[5]; //increased by 1 because of null termination-> don' t send the add byte!!!
  unsigned short ID;
  unsigned short byteCount;
  unsigned short transNumber;
  char navMode;
  char pad;
  unsigned short checksum;
  char msgEnd[3]; //increased by 1 because of null termination-> don' t send the add byte!!!
};


/*! Function to build the Navigation Mode message
 * Takes a pointer to the structure the data will be stored in and the transaction number
 */
 
void buildNavModeMsg(apxNavModeMsg* , unsigned short );



/* ! Function to serialize Navigation Mode message.
 * Takes a pointer to the structure to be serialized and a pointer to the buffer the data is going to be stored in
 */
 
void serialNavModeMsg(apxNavModeMsg* , char*);







/* *****************************************
 * ************** SELECT DATA GROUPS *******
 * *****************************************/

/*! Structure defining the message to select the data to be received from POS-LV. POS-LV message 52 */

typedef struct apxSelGroupsMsg
{
  apxSelGroupsMsg()    //this is the constructor, it fills the standard fields
  {  
    char init[5]="$MSG";
	char end[3]="$#";
	strcpy(msgStart,init);
	strcpy(msgEnd,end);
	ID=52;
	checksum=0;
  } 	
	
  char msgStart[5]; //increased by 1 because of null termination-> don' t send the add byte!!!
  unsigned short ID;
  unsigned short byteCount;
  unsigned short transNumber;
  unsigned short numberOfGroups;
  unsigned short groupID;  //I set to 10 the max number of groups we can get. This can be changed in the future
  unsigned short dataRate;
  unsigned short pad;
  unsigned short checksum;
  char msgEnd[3]; //increased by 1 because of null termination-> don' t send the add byte!!!	
};


/*! Function to build the Navigation Mode message
 * Takes a pointer to the structure the data will be stored in and the transaction number
 */

void buildSelGroupsMsg(apxSelGroupsMsg* , unsigned short );




/*! Function to serialize Navigation Mode message.
 * Takes a pointer to the structure to be serialized and a pointer to the buffer the data is going to be stored in
 */
 
void serialSelGroupsMsg(apxSelGroupsMsg* , char*, int );


/* *****************************************
 * ***** ACKNOWLEDGE MESSAGE ****************
 * *****************************************/
 
/*! Structure defining the Acknowledge message to be received from POS-LV as replay to a message. POS-LV message 0 */ 
 
typedef struct apxAckMsg
{ 
  char msgStart[5]; //increased by 1 because of null termination
  unsigned short ID;
  unsigned short byteCount;
  unsigned short transNumber;
  unsigned short recMsgID;
  unsigned short responseCode;
  char newParamStatus;
  char rejectedParamName[32];
  unsigned short pad;
  unsigned short checksum;
  char msgEnd[3]; //increased by 1 because of null termination			
};



/*! Function to parse Ack message data.
 * It takes is the pointer to the buffer to be parsed, the length of such a buffer
 * and a pointer to the apxAckMsg struct in which the parsed data are stored
 */

void parseAckMsg(char* , int , apxAckMsg* );



/* *****************************************
 * ******** CONTROL MESSAGE ****************
 * *****************************************/

/*! Structure defining the message to keep control on POS-LV. Should every at most 30 secs. POS-LV message 90 */
 
typedef struct apxControlMsg
{
  apxControlMsg()    //this is the constructor, it fills the standard fields
  {  
    char init[5]="$MSG";
	char end[3]="$#";
	strcpy(msgStart,init);
	strcpy(msgEnd,end);
	byteCount = 8;
	ID = 90;
	checksum=0;
  } 	
	
  char msgStart[5]; //increased by 1 because of null termination-> don' t send the add byte!!!
  unsigned short ID;
  unsigned short byteCount;
  unsigned short transNumber;
  unsigned short control;
  unsigned short checksum;
  char msgEnd[3]; //increased by 1 because of null termination-> don' t send the add byte!!!	
};



/*! Function to build the Navigation Mode message
 * Takes a pointer to the structure the data will be stored in and the transaction number
 */
 
void buildControlMsg(apxControlMsg* , unsigned short, unsigned short );



/*! Function to serialize Navigation Mode message.
 * Takes a pointer to the structure to be serialized and a pointer to the buffer the data is going to be stored in
 */

void serialControlMsg(apxControlMsg* , char* );



/* ****************************************************
 * ****************** CHECKSUM ************************
 * ****************************************************/

/*! Function to compute the checksum to be added at every message
 * Takes a pointer to the buffer containing the message and the length of the buffer
 */

unsigned short apxCompChecksum(char* ,int );



/*! Function to verify the checksum received with every message/datagroup
 * Takes a pointer to the buffer containing the message and the length of the buffer
 */
 
unsigned short apxVerChecksum(char* ,int );





/* ****************************************************
*  ************* TCP-IP functions *********************
*  ****************************************************/

/*! Function to send data through TCP-IP connection.
 * Takes a pointer to the socket identifier (an int)
 * a pointer to the buffer containing the data to be sent
 * and the size of the data to be sent
 */

short apxTcpSend(int* , char* , short );
// **************** RECEIVE ******************


/*! Function to receive data through TCP-IP connection.
 * Takes a pointer to the socket identifier (an int)
 * a pointer to the buffer containing the data to be received
 * and the size of the data to be received, if known (if not known set it to 0)
 */

short apxTcpReceive(int* , char* , short );

#endif //APPLANIX_HH
