#include "OBDII.hh"

///////////////////////////////////////////////////////////////////////////
//  Function: OBDIIdriver (Constructor) 
//  Inputs: None 
//  Outputs: None
//  Error Checking: None 
//  Summary: Opens the serial port and initializes it
///////////////////////////////////////////////////////////////////////////

OBDIIDriver::OBDIIDriver() 
{
	_debug_flag = 0;
#ifdef OBDII_SERIAL
#else
	_sdsport = NULL;
#endif
	_portnum = -1;

}


int OBDIIDriver::readport(string& instring)
{
	instring = "";
	char tempinarr[FULL_READSIZE+1];
	
	tempinarr[0] = '\0';
	//string tempstring = "";
	//int totalsize = 0;
	int thissize = 0;
	
#ifdef OBDII_SERIAL  
	//--------------------------------------------------
	// If we have less than FULL_READSIZE data waiting then we'll
	// hang from the blocking serial port read fn
	// This HALF_READSIZE read is a hack to combat the blocking
	// hack to serial to allow for half sized noop reads	
	//--------------------------------------------------

	if (_debug_flag > 2) {
	  cerr << "OBDIIDriver::readport calling serial read, size = " 
	       << HALF_READSIZE << endl;
	}
	_serialport.read(tempinarr, HALF_READSIZE);
	thissize = HALF_READSIZE;

	instring.append(tempinarr,HALF_READSIZE);
		if (_debug_flag > 1){
		cerr << "INITIAL INPUT = " ;
		printstring(instring);
	}
	if (!isnoop(instring)){
		_serialport.read(tempinarr,HALF_READSIZE);
		thissize = thissize+HALF_READSIZE;
		instring.append(tempinarr,HALF_READSIZE);
	
	}	


	if (!_serialport.good()){
		cerr << "OBDIIDRIVER::readport() error : bad read" << endl ;
		thissize = -1;
	}
#else // SDS

	thissize =	_sdsport->read(FULL_READSIZE,tempinarr,READ_TIMEOUT_USEC);
	if (thissize <0){
		cerr << "OBDIIDRIVER::readport() error : bad read" << endl ;
		return thissize;
	}
	else if(thissize >0){
		instring.append(tempinarr,thissize);
			
	}
		
#endif
		
	

	//--------------------------------------------------
	// print raw data received if _debug_flag is set
	//--------------------------------------------------
	if (_debug_flag){
		cerr << "INPUT = " ;
		printstring(instring);
	}
			
	if (thissize ==0){
		cerr << "OBDIIDriver::readport() error : read timed out " <<endl;
	}


	return thissize;
}


//============================================================
//
// return 0 = ok, 
//
//============================================================
int OBDIIDriver::writeport(const string& outstring)
{
	int retval = 0;
#ifdef OBDII_SERIAL  
	_serialport.clear();
	_serialport.sync();
	_serialport.write(outstring.c_str(), outstring.length());
	if (!_serialport.good()){
		retval = -1;
	}
#else // SDS
	_sdsport->clean();
	//	retval = _sdsport->write(outstring.c_str(), outstring.length());
	retval = _sdsport->write(outstring.c_str());
#endif
	if (retval <0){
		cerr << "OBDIIDRIVER::writeport() error : bad write" << endl ;
		return retval;
	}
	
	//--------------------------------------------------
	// print raw data sent if _debug_flag  is set
	//--------------------------------------------------
	if (_debug_flag){
		cerr << endl << "OUTPUT = " ;
		printstring(outstring);
	}
	return retval; 
}

int OBDIIDriver::sendcommand(const string& commandstring, string & returnstring) 
{
	int read_retval = 0;
	int write_retval = 0;
	string out;

 	if (_debug_flag) cerr << "OBDIIDriver::sendcommand calling writeport" << endl;
	write_retval = writeport(commandstring);
 	if (_debug_flag) cerr << "OBDIIDriver::sendcommand writeport returned " << write_retval << endl;
	
	if (write_retval < 0){
		return -1;
	}
	
 	if (_debug_flag) cerr << "OBDIIDriver::sendcommand calling readport" << endl;
	read_retval = readport(returnstring);
 	if (_debug_flag) cerr << "OBDIIDriver::sendcommand readport returned " << read_retval << endl;
	return read_retval;

	
}


int OBDIIDriver::connect(int port) 
{

#ifdef OBDII_SERIAL  
	if(_serialport.IsOpen())
		_serialport.Close();

	ostringstream portname;
	portname << "/dev/ttyS" << port;

	_serialport.Open(portname.str());
	if ( ! _serialport.good() )
		{
			cerr << "OBDIIDRIVER::connect() error: Could not open " << portname.str() << endl ; 
			return 0;
		}

	_serialport.clear();

	_serialport.SetBaudRate( SerialStreamBuf::BAUD_19200 ) ;
	_serialport.SetParity( SerialStreamBuf::PARITY_NONE ) ;
	_serialport.SetFlowControl( SerialStreamBuf::FLOW_CONTROL_NONE ) ;
	//_serialport.SetFlowControl( SerialStreamBuf::FLOW_CONTROL_SOFT ) ;
	_serialport.SetNumOfStopBits( 1 ) ;
	_serialport.SetCharSize( SerialStreamBuf::CHAR_SIZE_8) ;
	if ( ! _serialport.good() )
		{
			cerr << "OBDIIDRIVER::connect() error setting serial port properties." << endl ;
			return 0;
		}
	else{
		_portnum = port;
	}

#else //SDS

	if(_sdsport != NULL)
		{
			if(_sdsport->isOpen() == 1)
				{
					_sdsport->closePort();
					_portnum = -1;
				}
    
			if (_sdsport->openPort() != 0) //Try to open steering serial port
				{
					cerr << "Cannot open SDS Port " << port << endl;
				}
		}

  else
		{
			cerr << "connecting SDS" << endl;
			_sdsport = new SDSPort("192.168.0.60", port);
			if(_sdsport->isOpen() == 1)
				{
					_portnum = port;
					cerr<<"OBDII connection established"<<endl;
				}
			else
				{
					cerr << "Cannot open SDS Port " << port << endl;
				}
		}


	if(_portnum < 0){
		cerr << "OBDIIDRIVER::connect() error: couldn't connect to SDS port " << port << endl ;
		return 0;
	}

#endif 

	
#ifdef OBDII_SERIAL
		cerr << "OBDII connect to serial port successful" << endl;
#else
		cerr << "OBDII connect to SDS successful" << endl;
#endif
	

	/* Try and make sure the vehicle can connect to OBDII (can use the serial port) */





	return 1;
}

int OBDIIDriver::disconnect() 
{
	int retval = 1;
	
#ifdef OBDII_SERIAL
	_serialport.Close();
#else
	if (_sdsport != NULL){
		if (_sdsport->closePort() !=0)
			{		
				retval = 0;
			}
		delete(_sdsport);
	}
#endif
	_portnum = -1;
	return (retval);
 
}

unsigned char OBDIIDriver::checksum(const string & s)
{
	int length = 	s.length();
	int sum = 0;
	//	unsigned char twoscomp;
	unsigned char checksum = 0x00;

	for (int i = 1; i < (length-1); ++i){
		checksum = checksum + s[i];
		sum = sum + (int)s[i];
	}
	checksum = (checksum)&0xff;
	return (checksum);
	
}

// 1 if is noop response
// 0 otherwise

int OBDIIDriver::isnoop(const string & response)
{
	if (response.length() >=14){
		if (response[2] == 0x15 && response[4] == 0x30){
			return 1;
		}
	}
	return 0;
}

// 1 if is noop response
// 0 otherwise

int OBDIIDriver::isnull(const string & response)
{
	if (response.length() >=14){
		if (response[3] == 0x7f){
			return 1;
		}
	}
	return 0;
}


//============================================================
//
// OBDIIDriver::parseresponse
//
// returns :
// -1 for unparsed, 
// 0 for parsed noop(engine off) or parsed no valid response 
// 1 for parsed value
//
// for unparsed or parsed noop val is set to 0, otherwise val is 
// set from extracted data in "response" string
//
// valtype gives command parsing directions :
// 1 : 1, false , expectedlength = 1, startbyte = 5;
// 2 : 1, true  , expectedlength = 1, startbyte = 6;
// 3 : 2, false , expectedlength = 2, startbyte = 5;
// 4 : 2, true , expectedlength = 2, startbyte = 6;
// 5 : 2, signed , expectedlength = 1, startbyte = 5;
// 
//============================================================
int OBDIIDriver::parseresponse(const string & response, int & val, int valtype) 
{

	// Sam's parsing guess :
	// byte 0 : packet header 1 (always 0x40)
	// byte 1 : packet header 2 (0x88 for all commands, 0x80 for some init commands)
	// byte 2 : packet header 3 (?)
	// byte 3 : packet header 4 (0x42 for all commands, 0x16 0x41 for some init commands)
	// byte 4 : packet header 5 COMMAND TYPE?
	// byte 5 : packet header 6 (0x00 for all commands, 0x7f 0x01 for some init commands)
	// byte 6-12 : data bytes (possibly parsed as multiple numbers or as a single sum)
	// byte 13 : checksum (sum of bytes 2-13)

  val = 0;

	int length = response.length();
	int i = 0;
	//int retval = 0;
	int startbyte;
	long dataval = 0;
	long deltaval = 0;
	string correctedresponse;
	//if (DEBUG_DRIVER) cerr << "response length = " << length << endl;


	if (length <14){
		cerr << "in OBDIIDriver.cpp::parseresponse : response too short" << endl;	
		return -1;

	}
	else if(isnull(response) && response.length() >= 28)
		{
			correctedresponse =response.substr(14,14);
			if (isnull(correctedresponse)) {
				return 0;
			}
		}

	else
		{
			correctedresponse = response;
		}

	if (isnoop(correctedresponse)){
		
		//--------------------------------------------------
		// parsed successfully but it's a noop, return 0
		//--------------------------------------------------
		return 0;

	}

	unsigned char checkbyte = correctedresponse[13];
	unsigned char checksum = '\0';
		


	// sum bytes for checksum
	for (i = 1 ; i < 13 ; ++i)
		{
			checksum = checksum+correctedresponse[i];
		}
	checksum = checksum & '\xff';

	if (checkbyte != checksum)
		{
		
			cerr << "in OBDIIDriver.cpp::parseresponse : bad checksum" << endl;	
			return -1;
				
		}
	int expectedlength = 0;
	bool signedflag = false;
	//1 : 1, false , expectedlength = 1, startbyte = 5;
	//2 : 1, true  , expectedlength = 1, startbyte = 6;
	//3 : 2, false , expectedlength = 2, startbyte = 5;
	//4 : 2, true , expectedlength = 2, startbyte = 6;
	//5 : 2, signed , expectedlength = 1, startbyte = 5;
	if(valtype ==1){
		expectedlength = 1;
		startbyte = 5;
	}
	else if (valtype ==2){
		expectedlength = 1;
		startbyte = 6;
	}
	else if (valtype ==3){
		expectedlength = 2;
		startbyte = 5;
	}
	else if (valtype ==4){
		expectedlength = 2;
		startbyte = 6;
	}
	else if (valtype ==5){
		expectedlength = 2;
		startbyte = 6;
		signedflag = true;
	}
	else {
		cerr << "in OBDIIDriver.cpp::parseresponse : invalid valtype input" << endl;
		return -1;
	}
		
	
	// sum all data bytes (may need changing for two number responses)
	int bytenum = 0;
	for (i = startbyte+expectedlength-1 ; i >= startbyte; --i)
		{
			deltaval =  ((long)(correctedresponse[i])&0xff)<<(8*(bytenum));
			dataval = dataval +deltaval;
				
			bytenum = bytenum+1;
			//cerr << "dval = " << dataval << "  delta = "  << deltaval <<"   bytenum = " << bytenum << endl;
		}
	if (signedflag){
		dataval = (signed short)dataval;
	}
	
	val = (int)dataval;
	return 1;
}

//char OBDIIDriver::parseSignedResponse(const string & response)
//{
//	return response[5];
//}

//string OBDIIDriver::parsebitmappedresponse(const string & response, int expectedlength)
//{
//	string requestedresponse(1,response[6]);
//	return requestedresponse;
//}

//int OBDIIDriver::parsetwobytesignedresponse(const string & response)
//{
//	unsigned char msb = response[6];
//	unsigned char lsb = response[7];
//	signed short res = (signed short)((((unsigned short)msb) << 8) | ((unsigned short)lsb));

//	return res;
//}


string OBDIIDriver::buildtwobytecommand(char commandMSB, char commandLSB)
{
	// INIT 	
	// 70 | a2 | fb | e0 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 7d |
	// 70 | a6 | fd | 00 | fd | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | a0 |
	// 70 | a0 | 08 | 02 | 01 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | ab |
	// 70 | a0 | 08 | 02 | 01 | 01 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | ac |
	// 70 | a0 | 08 | 03 | 02 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | ad |
	// SOME COMMANDS	
	// 70 | a0 | 08 | 03 | 02 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | ad |
	// 70 | a0 | 08 | 03 | 02 | 01 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | ae |
	// 70 | a0 | 08 | 03 | 02 | 02 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | af |
	// 70 | a0 | 08 | 03 | 02 | 02 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | af |
	// 70 | a0 | 08 | 03 | 02 | 04 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | b1 |
	// 70 | a0 | 08 | 03 | 02 | 04 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | b1 |
	// 70 | a0 | 08 | 03 | 02 | 05 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | b2 |
	// 70 | a0 | 08 | 03 | 02 | 05 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | b2 |

	string outstring(15,0x00);

	outstring[0] = 0x70;
	outstring[1] = 0xa0;
	outstring[2] = 0x08;
	outstring[3] = 0x03;
	//outstring[4] = 0x01;
	outstring[4] = 0x22;
	outstring[5] = commandMSB;
	outstring[6] = commandLSB;


	unsigned char csum = checksum(outstring);
	outstring[14] = csum;
  
	return outstring;
} 

//string OBDIIDriver::buildDTCcommand(char mode)
//{
//	string outstring(15,0x00);

//	outstring[0] = 0x70;
//	outstring[1] = 0xa0;
//	outstring[2] = 0x08;
//	outstring[3] = 0x01;
//	outstring[4] = mode;

//	unsigned char csum = checksum(outstring);
//	outstring[14] = csum;

//	return outstring;
//}

string OBDIIDriver::buildcommand(char command) //identical to buildtwobytecommand except for contents of outstring[4]
{
	string outstring(15,0x00);

	outstring[0] = 0x70;
	outstring[1] = 0xa0;
	outstring[2] = 0x08;
	outstring[3] = 0x02;
	outstring[4] = 0x01;
	//outstring[4] = 0x22;
	outstring[5] = command;

	unsigned char csum = checksum(outstring);
	outstring[14] = csum;

	return outstring;
}


void OBDIIDriver::printstring(const string& s) {
	int thischar, j;
	int thissize = s.size();
	for (j = 0; j < (thissize);j++){
		thischar = (int)s[j];
		thischar = thischar&0xff;
		if (thischar <= 0x0f)
			cerr << hex << "0" << (int)thischar <<" | "; 
		else
			cerr << hex << (int) thischar <<" | "; 
		
		
	}
	if (thissize) cerr << dec<< endl;
}






// Sample function (edit parse response if the val isn't assembled correctly);
int OBDIIDriver::getRPM(double & val) 
{
	int tempval = 0;
	string returnstring = "";
	string command = buildcommand(0x0c);
	if (_debug_flag) cerr << "OBDIIDriver::getRPM: sending command" << endl;
	int retval =  sendcommand(command, returnstring);
	if (retval >=0){
		retval= parseresponse(returnstring, tempval,3);
	}
	val = (double)tempval/4.0;
	return retval;
}

int OBDIIDriver::getTimeSinceEngineStart(int & val)
{
	int tempval = 0;
	string returnstring = "";
	string command = buildcommand(0x1f);
	int retval =  sendcommand(command,returnstring);
	if (retval >=0){
		retval = parseresponse(returnstring, tempval,3);
	}
	return retval;
}

int OBDIIDriver::getVehicleSpeed(double & val)
{
	int tempval = 0;
	string returnstring = "";
	string command = buildcommand(0x0d);
	int retval =  sendcommand(command,returnstring);
	if (retval >=0){	
		retval = parseresponse(returnstring,tempval, 1);
	}
	val = ((double)tempval*1000.0)/3600.0;
	
	return retval;
}

int OBDIIDriver::getEngineCoolantTemp(double & val)
{
	int tempval = 0;
	string returnstring = "";
	string command = buildcommand(0x05);
	int retval =  sendcommand(command,returnstring);
	if (retval >=0){	
		retval = parseresponse(returnstring, tempval,1);
	}	
	val = 1.8*(double)(tempval-40) + 32.0;
	return retval;
}

int OBDIIDriver::getThrottlePosition(double & val)
{
        int tempval = 0;
	string returnstring = "";
	string command = buildtwobytecommand(0x09,0xd4);
	int retval =  sendcommand(command,returnstring);
	if (retval >=0){
		retval = parseresponse(returnstring, tempval,2);
	}
	val = (double)tempval / 2;
	return retval;
}

int OBDIIDriver::getTorque(double & val)
{
	int tempval = 0;
	string returnstring = "";
	string command = buildtwobytecommand(0x09,0xcb);
	int retval =  sendcommand(command,returnstring);
	if (retval >=0){	
		retval = parseresponse(returnstring,tempval,5);
	}
	val = (double) tempval;
	return retval;
}

int OBDIIDriver::getGlowPlugLampTime(int & val)
{
	int tempval = 0;
	string returnstring = "";
	string command = buildtwobytecommand(0x14,0x51);
	int retval = sendcommand(command,returnstring);
	if (retval >=0) {
		retval = parseresponse(returnstring,tempval, 4);
	}
	val = tempval;
return retval;
}

int OBDIIDriver::getCurrentGearRatio(double & val)
{
	int tempval = 0;
	string returnstring = "";
	string command = buildtwobytecommand(0x11,0xb7);
	int retval =  sendcommand(command,returnstring);
	if (retval >= 0) {
		retval = parseresponse(returnstring,tempval,4);
	}
	val = (double)tempval/16384.0;
	return retval;
}

int OBDIIDriver::getBatteryVoltage(double & val)
{
	int tempval = 0;
	string returnstring = "";
	string command = buildtwobytecommand(0x11,0x72);
	int retval =  sendcommand(command,returnstring);
	if (retval >=0) {
		retval = parseresponse(returnstring, tempval,2);
	}
	val = (double)tempval/16.0;
	return retval;
}


int OBDIIDriver::GlowPlugLightOn(int & val)
{
	int tempval = 0;
	string returnstring = "";
	string command = buildtwobytecommand(0x16,0x67);
	int retval =  sendcommand(command,returnstring);
	if (retval >=0) {
		retval = parseresponse(returnstring, tempval,2);
	}
	val = tempval;
	return retval;
}


int OBDIIDriver::getCurrentGear(int & val)
{
	int tempval = 0;
	string returnstring = "";
	string command = buildtwobytecommand(0x11, 0xb3);
	int retval =  sendcommand(command,returnstring);
	if (retval >=0) {
		retval = parseresponse(returnstring,tempval, 2);
	}
	val = (int)(tempval/2);
	return retval;
}

int OBDIIDriver::getTransmissionPosition(char & val)
{
	int tempval = 0;
	string returnstring = "";
	string command = buildtwobytecommand(0x11, 0xb6);
	int retval =  sendcommand(command,returnstring);
	if (retval >=0) {
		retval = parseresponse(returnstring,tempval, 2);
	}

	tempval = (int)(tempval/2);

	if(tempval  == 70) val = 'P';
	else if(tempval == 60) val = 'R';
	else if(tempval  == 50) val = 'N';
	else if(tempval>40 & tempval <50) val = 'D'; 
	else val = 'X';
	return retval;
}

int OBDIIDriver::getTorqueConvClutchDutyCycle(double & val)
{
	int tempval = 0;
	string returnstring = "";
	string command = buildtwobytecommand(0x11, 0xb0);
	int retval =  sendcommand(command,returnstring);
	if (retval >=0) {
		retval = parseresponse(returnstring,tempval, 4);
	}
	val = (double)tempval;
	return retval;
}

int OBDIIDriver::getTorqueConvControlState(int & val)
{
	int tempval = 0;
	string returnstring = "";
	string command = buildtwobytecommand(0x09, 0x71);
	int retval =  sendcommand(command,returnstring);
	if (retval >=0) {
		retval = parseresponse(returnstring, tempval, 2);
	}
	val = tempval;
	return retval;
}
