/*
 * AState_Sparrow - interface functions for sparrow
 *
 * Richard M. Murray
 * 29 December 2006
 *
 */

#include "AState.hh"

void AState::sparrow_rebind(DD_IDENT *tbl)
{
  dd_rebind_tbl("snkey", &snKey, tbl);
  
//  dd_rebind_tbl("apxActive", &apxActive, tbl);
  dd_rebind_tbl("apxStatus", &apxStatus, tbl);
  dd_rebind_tbl("apxEnabled", &apxEnabled, tbl);
  dd_rebind_tbl("apxCount", &apxCount, tbl);
  dd_rebind_tbl("stateMode", &stateMode, tbl);
  dd_rebind_tbl("timberEn", &timberEnabled, tbl);
  dd_rebind_tbl("apxActive", &apxActive, tbl);
  dd_rebind_tbl("logEn", &_options.logRaw, tbl);
  dd_rebind_tbl("repEn", &_options.stateReplay, tbl);
  
  dd_rebind_tbl("vsNorthing", &_vehicleState.Northing, tbl);
  dd_rebind_tbl("vsEasting", &_vehicleState.Easting, tbl);
  dd_rebind_tbl("vsAltitude", &_vehicleState.Altitude, tbl);
  dd_rebind_tbl("vsVel_N",  &_vehicleState.Vel_N, tbl);
  dd_rebind_tbl("vsVel_E",  &_vehicleState.Vel_E, tbl);
  dd_rebind_tbl("vsVel_D",  &_vehicleState.Vel_D, tbl);
  dd_rebind_tbl("vsAcc_N",  &_vehicleState.Acc_N, tbl);
  dd_rebind_tbl("vsAcc_E",  &_vehicleState.Acc_E, tbl);
  dd_rebind_tbl("vsAcc_D",  &_vehicleState.Acc_D, tbl);
  dd_rebind_tbl("vsRoll", &_vehicleState.Roll, tbl);
  dd_rebind_tbl("vsPitch", &_vehicleState.Pitch, tbl);
  dd_rebind_tbl("vsYaw", &_vehicleState.Yaw, tbl);
  dd_rebind_tbl("vsRollRate", &_vehicleState.RollRate, tbl);
  dd_rebind_tbl("vsPitchRate", &_vehicleState.PitchRate, tbl);
  dd_rebind_tbl("vsYawRate", &_vehicleState.YawRate, tbl);
  dd_rebind_tbl("vsRollAcc", &_vehicleState.RollAcc, tbl);
  dd_rebind_tbl("vsPitchAcc", &_vehicleState.PitchAcc, tbl);
  dd_rebind_tbl("vsYawAcc", &_vehicleState.YawAcc, tbl);
  dd_rebind_tbl("vsNorthConf", &_vehicleState.NorthConf, tbl);
  dd_rebind_tbl("vsEastConf", &_vehicleState.EastConf, tbl);
  dd_rebind_tbl("vsHeightConf", &_vehicleState.HeightConf, tbl);
  dd_rebind_tbl("vsRollConf", &_vehicleState.RollConf, tbl);
  dd_rebind_tbl("vsPitchConf", &_vehicleState.PitchConf, tbl);
  dd_rebind_tbl("vsYawConf", &_vehicleState.YawConf, tbl);
  dd_rebind_tbl("lpNorthing", &_vehicleState.localX, tbl);
  dd_rebind_tbl("lpEasting", &_vehicleState.localY, tbl);
  dd_rebind_tbl("lpAltitude", &_vehicleState.localZ, tbl);
  dd_rebind_tbl("lpVel_N",  &_vehicleState.vehicleVelX, tbl);
  dd_rebind_tbl("lpVel_E",  &_vehicleState.vehicleVelY, tbl);
  dd_rebind_tbl("lpVel_D",  &_vehicleState.vehicleVelZ, tbl);
  dd_rebind_tbl("lpAcc_N",  &_vehicleState.Acc_N, tbl);
  dd_rebind_tbl("lpAcc_E",  &_vehicleState.Acc_E, tbl);
  dd_rebind_tbl("lpAcc_D",  &_vehicleState.Acc_D, tbl);
  dd_rebind_tbl("lpRoll", &_vehicleState.localRoll, tbl);
  dd_rebind_tbl("lpPitch", &_vehicleState.localPitch, tbl);
  dd_rebind_tbl("lpYaw", &_vehicleState.localYaw, tbl);
  dd_rebind_tbl("lpRollRate", &_vehicleState.vehicleVelRoll, tbl);
  dd_rebind_tbl("lpPitchRate", &_vehicleState.vehicleVelPitch, tbl);
  dd_rebind_tbl("lpYawRate", &_vehicleState.vehicleVelYaw, tbl);
  dd_rebind_tbl("lpRollAcc", &_vehicleState.RollAcc, tbl);
  dd_rebind_tbl("lpPitchAcc", &_vehicleState.PitchAcc, tbl);
  dd_rebind_tbl("lpYawAcc", &_vehicleState.YawAcc, tbl);

}
