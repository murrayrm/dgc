/******************************************************************
 **
 **  test.cpp
 **
 **    Time-stamp: <2003-06-13 14:12:11 evo>
 **
 **    Author: Sam Pfister
 **    Created: Thu May 22 21:53:37 2003
 **
 **
 ******************************************************************
 **
 **
 **
 ******************************************************************/
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <math.h>
#include "OBDII.hh"
using namespace std;

#ifdef STEER_SERIAL
#include <SerialStream.h>
using namespace LibSerial;
#endif


int main(int argc, char* argv[])
{
 
	int debugval = 0;
	if (argc >1){
		debugval = atoi(argv[1]);
		cerr << "debugval  = " << debugval <<  endl;
	}
	OBDIIDriver obd;
		

	int portnum  = 7;	// port number for OBD
	
 	cerr << "(from obdtest) connecting " << endl;	
	
	if (obd.connect(portnum) < 0){
		cerr << "(from obdtest) Unable to open serial port "<< portnum << endl;
	}
	else{
		cerr << "(from obdtest) Serial port " << portnum << " opened"<< endl;
		
	 	obd.setdebug(debugval);
		
		double doubleval = 0.0;
		int retval = 0;
		char charval = 0x00;
		int intval = 0;	
	
		cerr << "getTransmissionPosition      "; 
		retval = obd.getTransmissionPosition(charval);
		cerr << "return val = "<< retval << "  data val = " << charval << endl;
		
		cerr << "getRPM                       ";
		retval = obd.getRPM(doubleval);
		cerr << "return val = " << retval << "  data val = " << doubleval << endl;
			
		cerr << "getTimeSinceEngineStart      " ;
		retval = obd.getTimeSinceEngineStart(intval);
		cerr << "return val = "<< retval << "  data val = " << intval << endl;

		
		cerr << "getVehicleSpeed              ";
		retval = obd.getVehicleSpeed(doubleval);
		cerr << "return val = " << retval << "  data val = "<< doubleval << endl;
		
		
		cerr << "getEngineCoolantTemp         ";
		retval = obd.getEngineCoolantTemp(doubleval);
		cerr << "return val = "<< retval << "  data val = " << doubleval << endl;
		

		cerr << "getThrottlePosition          " ;
	  retval = obd.getThrottlePosition(doubleval);
		cerr << "return val = "<< retval << "  data val = " << doubleval << endl;
		

		cerr << "getTorque                    ";
		retval = obd.getTorque(doubleval);
		cerr << "return val = " << retval << "  data val = "<< doubleval << endl;


		cerr << "getGlowPlugLampTime          " ;
		retval = obd.getGlowPlugLampTime(intval);
		cerr << "return val = "<< retval << "  data val = " << intval << endl;

		
		cerr << "getCurrentGearRatio          ";
		retval = obd.getCurrentGearRatio(doubleval);
		cerr << "return val = "<< retval << "  data val = " << doubleval << endl;


		cerr << "GlowPlugLightOn?             " ;
		retval = obd.GlowPlugLightOn(intval);
		cerr << "return val = "<< retval << "  data val = " << intval << endl;


		cerr << "getCurrentGear               ";
		retval = obd.getCurrentGear(intval);
		cerr << "return val = "<< retval << "  data val = " << intval << endl;

		
		cerr << "getTorqueConvClutchDutyCycle "; 
		retval = obd.getTorqueConvClutchDutyCycle(doubleval);
		cerr << "return val = "<< retval << "  data val = " << doubleval << endl;



		cerr << "getTorqueConvControlState    " ;
		retval = obd.getTorqueConvControlState(intval);
		cerr << "return val = "<< retval << "  data val = " << intval << endl;


		cerr << "getBatteryVoltage            " ;
		retval = obd.getBatteryVoltage(doubleval);
		cerr << "return val = "<< retval << "  data val = " << doubleval << endl;
		obd.disconnect();
	}

	return(1);
}
