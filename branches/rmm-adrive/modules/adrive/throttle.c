/*
 * throttle.c - code to control the device controlling fuel flow to the injector (throttle) 
 * 
 * Original author: Jeff Lamb
 *                  12/02/04
 * 
 *
 * Modified 2-04-05 by David Rosen for SDS
 */
// For Threaded adrive.  To protect legacy code. 
#include <iostream>
#include <sys/time.h>

#include "vehports.h"
#include "GlobalConstants.h"
#include "throttle.h"
#include "SDSPort.h"
#include "sparrow/dbglib.h"

#ifdef THROTTLE_SERIAL
#include <sstream>
#include <SerialStream.h>
using namespace std;
using namespace LibSerial;
static SerialStream serial_port;
#endif

double throttle_position;
double last_throttle_position;
char throttle_output[6];
char throttle_get_position_cmd = THROTTLE_GET_POSITION;
char throttle_read[4];
double throttle_position_read;


#ifdef THROTTLE_SDS
SDSPort* throttleport;
int throttlePortOpen = 0;
extern char simulator_IP[];
int throttle_serial_port = -1;  //not a valid serial port number, must assign
#endif //THROTTLE_SDS


/****************************************************************************
 * int throttle_open(int)
 * Author: Jeff Lamb
 * Date: 02/30/05
 * Revisions: Dima Kogan, 08/01/05
 *            Jeff Lamb, 08/04/05
 * Functionality: Opens the serial or SDS port for the throttle.
 * Returns: TRUE  (1) if port opened properly
 *          FALSE (0) if port failed to open correctly
 ****************************************************************************/
int throttle_open(int port){
  //  double throttle_read_val;
  int errval = 0;

#ifdef THROTTLE_SERIAL
  ostringstream portname;
  portname << "/dev/ttyS" << port;
  
  serial_port.Open(portname.str());
  if ( ! serial_port )
    {
      cerr << "throttle_open() error: Could not open " << portname.str() << endl ;
      return FALSE;
    }
  
  //make sure that the error bit is clear before attempting initialization.
  if (! serial_port.good() ) {
    serial_port.clear();
  }
  
  serial_port.SetBaudRate( SerialStreamBuf::BAUD_9600 ) ;
  if ( ! serial_port.good() )
    {
      cerr << "throttle_open() error: Could not set the baud rate." << endl ;
      errval++;
      //return FALSE;
    }
  serial_port.SetParity( SerialStreamBuf::PARITY_NONE ) ;
  if ( ! serial_port.good() )
    {
      cerr << "throttle_open error: Could not disable the parity." << endl ;
      errval++;
      //return FALSE;
    }
  serial_port.SetFlowControl( SerialStreamBuf::FLOW_CONTROL_NONE ) ;
  if ( ! serial_port.good() )
    {
      cerr << "throttle_open error: Could not turn off hardware flow control." << endl;
      errval++;
      //return FALSE;
    }
  serial_port.SetNumOfStopBits( 1 ) ;
  if ( ! serial_port.good() )
    {
      cerr << "throttle_open error: Could not set the number of stop bits."  << endl;
      errval++;
      //return FALSE;
    }
  serial_port.SetCharSize( SerialStreamBuf::CHAR_SIZE_8) ;
  if ( ! serial_port.good() )
    {
      cerr << "throttle_open() error: Could not set the char size." << endl ;
      errval++;
      //return FALSE;
    }
  
  if ( errval > 0 ) return FALSE;
#endif //THROTTLE_SERIAL
  
#ifdef THROTTLE_SDS 
  if(throttlePortOpen == 0)
    {
      throttleport = new SDSPort("192.168.0.60", port);
      throttlePortOpen = 1;
      throttle_serial_port = port;
    }
  
  throttleport->openPort();
  
  if(throttleport->isOpen() == 1)
    {
      throttlePortOpen = 1;
      throttle_serial_port = port;
      //return TRUE;
    }
  else
    {
      return FALSE;
    }
#endif //THROTTLE_SDS
  
  // perform a read to check that we are actually receiving good data
  if (throttle_getposition() == -1) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}


/**************************************************************************
 * void simulator_throttle_open(int)
 * Author: David Rosen
 * Functionality: Opens the throttle port for the simulator
 * Returns: None
 **************************************************************************/
void simulator_throttle_open(int port)
{
#ifdef THROTTLE_SDS
  throttleport = new SDSPort(simulator_IP, port);
  throttlePortOpen = 1;
  throttle_serial_port = port;
#endif //THROTTLE_SDS
}

/**************************************************************************
 * int throttle_close(void)
 * Author: Jeff Lamb
 * Revisions: Dima Kogan, 08/01/05
 * Functionality: Closes the throttle port
 * Returns: TRUE  (1) on success
 *          FALSE (0) on failure
 **************************************************************************/
int throttle_close(){
#ifdef THROTTLE_SERIAL
  serial_port.Close();
  return TRUE;
#endif //THROTTLE_serial
  
#ifdef THROTTLE_SDS
  if(throttleport->closePort() == 0)
    {
      throttlePortOpen = 0;
      return TRUE;
    }
  else
    {
      return FALSE;
    }
#endif //THROTTLE_SDS
}


/*************************************************************************
 * int throttle_calibrate(void)
 * NOTE: Dummy function -- returns TRUE always
 *************************************************************************/
int throttle_calibrate(){
  return TRUE;
}


/*************************************************************************
 * int throttle_zero(void)
 * Author: Jeff Lamb
 * Functionality: Sets the throttle position to zero
 * Returns: TRUE  (1) on success
 *          FALSE (0) on failure
 *************************************************************************/
int throttle_zero(){
  if (throttle_setposition(THROTTLE_IDLE) != TRUE) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}



//does nothing currently ... eventually should get some status
int throttle_ok(){
  return TRUE;
}



int throttle_pause(){
  return TRUE;
}



/*************************************************************************
 * Author: Jeff Lamb
 * Date: 12/22/04
 * Operation:  Takes an input from driving software normalized from THROTTLE_IDLE
 *             to THROTTLE_MAX (defined in throttle.h)
 * Returns: 0 if ERROR
 *          1 if executed properly
 **************************************************************************/
int throttle_setposition(double position){
  // make sure that the position is in range, if not, return error
  if(position < THROTTLE_IDLE || position > THROTTLE_MAX) return FALSE;
  throttle_position = position;
  
  // perform calculations for output characters
  double apps1_output = APPS1_IDLE - (throttle_position * APPS1_RANGE);
  double apps2_output = APPS2_IDLE + (throttle_position * APPS2_RANGE);
  double apps3_output = APPS3_IDLE + (throttle_position * APPS2_RANGE);
  throttle_output[THROTTLE_START] = START_CHARACTER;
  throttle_output[APPS1] = (char) apps1_output;
  throttle_output[APPS2] = (char) apps2_output;
  throttle_output[APPS3] = (char) apps3_output;
  throttle_output[THROTTLE_END] = END_CHARACTER;

  // cerr<<"throttle command sent: "<< throttle_output<<endl;

#ifdef THROTTLE_SERIAL
  // clear error flag before sending data.
  serial_port.clear();
  serial_port.write(throttle_output, THROTTLE_OUTPUT_LENGTH);
  if (!serial_port.good()) {
    return FALSE;
  }
  else {
    return TRUE;
  }
#endif //THROTTLE_serial

#ifdef THROTTLE_SDS
  throttleport->write(throttle_output);
  return TRUE;
#endif //THROTTLE_sds

}



/***************************************************************************
 * double throttle_getposition();
 * Author: Jeff Lamb
 * Date: 31 Dec 04
 * Revisions: None
 * Functionality:  Called with no arguments.  Will send a charcter to the
 *                 throttle interface board requesting position.  The interface
 *                 board will return with 3 chracters corresponding to APPS1,2,3.
 *                 These characters will be adjusted based on the APPS* values
 *                 present in throttle.h to return a value between 0 and 1.
 * Returns: Double between 0 and 1, return -1 on error
 * Known Bugs: None
 *************************************************************************/
double throttle_getposition(){
  throttle_position_read = -1;
#ifdef THROTTLE_SERIAL
  serial_port.clear();
  serial_port.write(&throttle_get_position_cmd, 1);
  //cerr << "write succeeded" << endl;
  if (!serial_port.good()) {
    //cerr << "Throttle_getposition write failed" << endl;
    return -1;
  }

  serial_port.read(throttle_read, THROTTLE_INPUT_LENGTH);
  //cerr << "read succeeded" << endl;
  if (!serial_port.good()) {
    //cerr << "Throttle_getposition read failed" << endl;
    return -1;
  }
#endif //THROTTLE_serial

#ifdef THROTTLE_SDS
  throttleport->clean();
  throttleport->write(&throttle_get_position_cmd);
  usleep(30000);
  throttleport->read(THROTTLE_INPUT_LENGTH, throttle_read, 10000);
#endif //THROTTLE_SDS
  
  //cerr<<"Throttle Received:"<<throttle_read<<endl;

  //fprintf(stderr, "throttle read string: %s", throttle_read);

  // perform calculations to normalize from 0-1
  double apps1_read = (APPS1_IDLE - (unsigned char) throttle_read[0]);
  apps1_read = apps1_read/APPS1_RANGE;
  double apps2_read = ((unsigned char) throttle_read[1] - APPS2_IDLE);
  apps2_read = apps2_read/APPS2_RANGE;
  double apps3_read = ((unsigned char) throttle_read[2] - APPS3_IDLE);
  apps3_read = apps3_read/APPS3_RANGE;
  throttle_position_read = (apps1_read + apps2_read + apps3_read) / 3;

  //cerr<<"Throttle Parsed:"<<throttle_position_read<<endl;


  // if throttle position read did not get set, return an ERROR
  if (throttle_position_read == -1) {
    //cerr << "Throttle_position_read == -1" << endl;
    return -1;
  }

  if (throttle_position_read < 0) throttle_position_read = 0;   //normalize to within 0-1 range
  if (throttle_position_read > 1) throttle_position_read = 1;

  //fprintf(stderr, "here it is: %0.2f", throttle_position_read);
  return throttle_position_read;
}
