#include "stereoFeeder.hh"

%%
StereoFeeder - 'Delivering quite accurate maps since Sept 2005'					    
--------------------------------------------------------------------------------------
			    |				   |
Rate: %rte Hz (%tm ms) 	    |				   |
			    |			   	   |	                      
Source:         %source	    |	SUBWINDOW:		   | 	TIMING (ms):      	
Pair:           %pair	    |	X Offset:    	%xo        | 	Lock:	 	%tlc   	
Image Size:    %wd x%ht	    |   Width:     	%sw        | 	Capture: 	%tcp   	
Current Frame: %cur_frame   |   Y Offset:      	%yo        | 	Rectify:        %trc   	
Deltas Sent:   %mess_sent   |   Height:    	%sh        |	Disparity:	%tds   	
Vehicle Paused:	 %paused    |	Pitch Control: 	%pA	   |	Pointcloud:	%tcd   
Override Pause:  %always    |	Pitch offset: 	%yoa	   |	Mapping:	%tmp   	
	         	    |				   |	Logging:	%tlg  	
PROCESSING PARAMETERS:      |	EXPOSURE CONTROL:	   |	Total: 		%ttl   			   
Window Size:   	%corrsize   |   Active:		%expTgl    |	  	      
N Disparities:  %ndisp 	    |   Left Exposure: 	%expLeft   |
X Offset: 	%offx       |   Right Exposure: %expRight  | 	LOGGING:        		    	
Threshold:  	%thresh     |   Reference Val: 	%expRef    |	SourceFileName: %sourceFilenamePrefix     
Unique:    	%unq	    |				   |	LogFileName:    %logFilenamePrefix    
Multiscale:    	%multi	    |	SUN BLOCKER:		   |	Format:         %format              
			    |   Active: 	%sunDect   |    Frames: 	%lf                    
BLOB FILTER:		    |   Tilt Width:    	%sunTilt   |	Rect:   	%lr 
Active:      	  %blbact   |   Aspect Width:  	%sunAspect |	State:  	%ls
Blob Threshold:	%blbtre	    |				   |	Maps:   	%lm 
Disparity Tol:	 %blbdis    |				   |	Disp:   	%ld
			    |				   |	Time:   	%lt
3D POINT FILTER:	    |				   |	Points: 	%lp
Min Range:  	%minrng	    |				   |
Max Range:   	%maxrng	    |				   |
Max Rel Alt: 	%maxalt	    |				   |
Min Rel Alt: 	%minalt     |				   |
			    |				   |			 
--------------------------------------------------------------------------------------

[LOG: %LAL| %LNNE| %LBSE| %LQDBG      | %LADBG   ]    
				                   
[DISPLAY: %DAL| %DREC| %DDS]			   
						   
[%QUIT| %PAUS | %STEP| %REST | %RESND | %CLER ]             
%%

tblname: vddtable;
bufname: vddbuf;

short:  %expTgl exposureToggle "%1d" -callback=change;
double: %expLeft expVal_left "%3.2f" -ro;
double: %expRight expVal_right "%3.2f" -ro;
double: %expRef expRefVal "%3.2f" -callback=change;

string: %source new_source "%s" -callback=change;
string: %pair cam_pair "%s" -ro;
string: %logFilenamePrefix logFilenamePrefix "%s";
string: %sourceFilenamePrefix sourceFilenamePrefix "%s";
string: %format new_format "%s";

short:  %wd source_width "%4d" -ro;
short:  %ht source_height "%3d" -ro;
short:  %cur_frame current_frame "%5d" -ro;
short:  %mess_sent nbrSentMessages "%5d" -ro;
short:  %paused isPaused "%3d" -ro;
short:  %always optAlways "%3d" -callback=change;

short:	%ndisp ndisp "%3d" -callback=change;
short:  %corrsize corrsize "%3d" -callback=change;
short:  %thresh thresh "%3d" -callback=change;
short:  %offx offx "%3d" -callback=change;
short:  %unq svsUnique "%3d" -callback=change;
short:  %multi multi "%3d" -callback=change;

short:	%sunDect sunDetection "%1d" -callback=change;
short:	%sunTilt tiltWidth "%2d" -callback=change;
short:	%sunAspect aspectWidth "%2d" -callback=change;

short:  %blbact blobActive "%1d" -callback=change;
short:  %blbtre blobTresh "%2d" -callback=change;
short:  %blbdis blobDispTol "%2d" -callback=change;

double: %minrng minRange "%2.2f" -callback=change;
double: %maxrng maxRange "%2.2f" -callback=change;
double: %maxalt maxRelativeAltitude "%3.2f" -callback=change;
double: %minalt minRelativeAltitude "%3.2f" -callback=change;

short:  %xo subwindow_x_off "%3d" -callback=change;
short:  %yo subwindow_y_off "%3d" -callback=change;
short:  %sw subwindow_width "%3d" -callback=change;
short:  %sh subwindow_height "%3d" -ro;
short:  %pA pitchActive "%3d" -callback=change;
short:  %yoa y_off_add "%3d" -ro;

double: %rte rate "%3.1f" -ro;
double: %tm time_ms "%3.0f" -ro;

double: %tlc time_lock "%3.0f" -ro;
double: %tcp time_cap "%3.0f" -ro;
double: %trc time_rect "%3.0f" -ro;
double: %tds time_disp "%3.0f" -ro;
double: %tcd time_cloud "%3.0f" -ro;
double: %tmp time_map "%3.0f" -ro;
double: %tlg time_log "%3.0f" -ro;
double: %ttl time_total "%3.0f" -ro;

short: %lf logFrames "%1d";
short: %lr logRect "%1d";
short: %ld logDisp "%1d";
short: %lm logMaps "%1d";
short: %ls logState "%1d";
short: %lt logTime "%1d";
short: %lp logPoints "%1d";

button: %DAL "ALL" display_all;
button: %DREC "RECT" display_rect;
button: %DDS "DISP" display_disp;

button: %LAL "ALL" log_all;
button: %LBSE "BASE" log_base;
button: %LQDBG "QUICK DEBUG" log_quickdebug;
button: %LADBG "ALL DEBUG" log_alldebug;
button: %LNNE "NONE" log_none;

button: %QUIT "QUIT" user_quit;
button: %PAUS "PAUSE" user_pause;
button: %STEP "STEP" user_step;
button: %REST "RESET" user_reset;
button: %CLER "CLEAR" user_clear;
button: %RESND "RESEND" user_resend;
