#define TABNAME planner

#define TABINPUTS(_) \
  _(unsigned,MIN_PLANNING_CYCLETIME_US, 0,   Entry, 0,0, 1,0  )	\
  _(double  ,PLANNING_L0OKAHEAD      ,   1.0 ,Label, 10,10, 2,0  )

#define TABOUTPUTS(_) \
  _(unsigned,MIN_PLANNING_CYC, 0,   Entry, 3,0, 4,0  )	\
  _(double  ,PLANNING_TEST,  1.0 ,Button, 10,10, 5,0  )

