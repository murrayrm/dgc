/* AState_Main.cc (repAstate)
 * 
 * Main function of the minimal Astate with replay capabilities
 * 
 *        Stefano Di Cairano, DEC-06
 * 
 */


#include <getopt.h>
#include "AState.hh"
#include <stdlib.h>
#include "DGCutils"


AState *ss;

void print_usage()
{
 cout<< '\t' << "Usage: ./astate [options]" << endl;
 cout<< '\t' << "--help, -h" << '\t' << "Display help message" << endl;
 cout<< '\t' << "--snkey, -s <skynet_key>" << '\t' << "Set skynet key to <skynet_key>" << endl;
 cout<< '\t' << "--nosp, -x" << '\t' << "Disable Sparrow display" << endl;
 cout<< '\t' << "--log, -l" << '\t' << "Log enabled" << endl;
 cout<< '\t' << "--replay, -r" << '\t' << "Replay state data" << endl;
}

int main(int argc, char **argv)
{
  char *pKey = getenv("SKYNET_KEY");
 // char *pUseSparrow =getenv("SPARROW"); 
 // char *pLogRaw = getenv("LOGRAW");
 // char *pStateReplay = getenv("REPLAY");
  int sn_key = 0;
  
  astateOpts options;
  options.useSparrow=1;
  options.logRaw=0;
  options.stateReplay=0;
  
  
  int options_index;
  int opt;
  const char* const short_options = "hs:lrx";
  
  static struct option long_options[] =
  {
    //first: long option (--option) string
    //second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
    //third: if pointer, set variable to value of fourth argument
    //	 if NULL, getopt_long returns fourth argument
    {"help",	0,	NULL,	'h'},
    {"snkey",	1,	NULL,	's'},
    {"s",	1,	NULL,	's'},
    {"log",	0,	NULL,	'l'},
    {"l",	0,	NULL,	'l'},
    {"replay",	0,	NULL,	'r'},
    {"r",	0,	NULL,	'r'},
    {"nosparrow", 0, NULL,  'x'},
    {"x",   0,      NULL,   'x'},
    {"nosp", 0,     NULL,   'x'},
    {0,0,0,0}
  };
  
  
  
  
  while((opt = getopt_long(argc, argv, short_options, long_options, &options_index)) != -1)
  {
  	switch(opt)
	{
        case 'h':
	  print_usage();
	  return 0;
	  break;
	case 's':
	  sn_key = atoi(optarg);
	  break;
	case 'l':
	  options.logRaw = 1;
	  break;
	case 'r':
	  options.stateReplay = 1;
	  //strncpy(inputAstateOpts.replayFile, optarg, 99);
	break;
	  case 'x':
	  options.useSparrow = 0;
	  break;
	default:
	  break;
	}
  }
  

  if (pKey != NULL)
  {
//    cerr << "Must set SKYNET_KEY environment variable for tests. Now using -1"<< endl;
//    sn_key = -1;
//  }
//  else
//  {
    sn_key = atoi(pKey);
  }
  cerr << "Constructing skynet with KEY = " << sn_key << endl;
  
/*  
  if(pUseSparrow != NULL)
  {
    int useSparrow = atoi(pUseSparrow);
    if(useSparrow == 0)
    {
      options.useSparrow = 0;
    }
  }
  
  if(pLogRaw != NULL)
  {
    int logRaw = atoi(pLogRaw);
    if(logRaw==1)
    {
      options.logRaw =1;
    }
  }

  if(pStateReplay != NULL)
  {
    int stateReplay = atoi(pStateReplay);
    if(stateReplay==1)
    {
      options.stateReplay = 1;
    }
  }*/
  
 //Create AState server:
  ss = new AState(sn_key,options);

  ss->active();

  delete ss;

  return 0;
}
