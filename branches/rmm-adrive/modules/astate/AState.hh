/* AState.hh (repAstate)
 * 
 * Definition of the Astate class
 *
 * Stefano Di Cairano DEC-06
 *
 */

#ifndef ASTATE_HH
#define ASTATE_HH

#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <fstream>

#include <sys/types.h>   //these are the libraries needed for sockets
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include "VehicleState.hh"

#include "Applanix.hh" //Applanix header file. Define here msgs structure and applnix constants
#include "ggis.h"

#include "ggis.h"
#include "sn_msg.hh"
#include "sn_types.h"
#include "Matrix.hh"

#include "DGCutils"

#include "SensorConstants.h"
#include "SkynetContainer.h"
#include "CTimberClient.hh"
#include "ActuatorState.hh"

#include "sparrowhawk.hh"

#define APX_DATA_BUFFER_SIZE 1500
#define DEBUG_LEVEL 5 





//I think we need something more (one structure for each group we read w/corresp. buffer)
struct rawApx {	
	unsigned long long time;
	apxNavData data;
};
//end 
struct astateOpts {
	int useSparrow;
	int logRaw;
	int stateReplay;
};


enum {
  PREINIT,
  INIT,
  NAV,
  FALLBACK,
  OH_SHIT,
  NUM_MODES
};

enum {
  FULL_NAV,   //this refers to table 5 in applanix Port Interface Document
  FINE_ALIGN,
  ALIGN_GPS,
  ALIGN_NOGPS,
  NOT_ALIGN_GPS,
  NOT_ALIGN_NOGPS,
  COARSE_LEVELING,
  INITIAL,
  NO_SOLUTION
};


class AState : public CTimberClient //Timber not in stable branch yet
{


  pthread_mutex_t m_VehicleStateMutex;
  // Lock these for the m_VehicleStateMutex

  struct VehicleState _vehicleState;

  // end lock list 

  pthread_mutex_t m_MetaStateMutex;

  // Lock these for the m_MetaStateMutex


  // Sockets to control and read from POS-LV, set in the constructor and never changed
  int apxDataSocket;
  //socket to broadcast vehicle state
  int broadcastStateSock;  // Should be set in constructor and never changed.
  

  pthread_mutex_t m_ApxDataMutex;

  // Lock these for the m_ApxDataMutex

  DGCcondition apxBufferFree;

  rawApx apxBuffer[APX_DATA_BUFFER_SIZE]; 
  
  int apxBufferReadInd;

  int apxBufferLastInd;


  // end lock list 


  int stateMode;
 
  //These variables are ONLY here to give sparrow access to them...

  //Outputs:
  int apxCount;
  int apxStatus;
  int apxEnabled;
  int apxActive;
  int apxDead;

  int timberEnabled;
  
 	// state estimate as obtained from APX
  double apxHeight;
  double apxNorth;
  double apxEast;
  double apxVelN;
  double apxVelE;
  double apxVelD;
  double apxAccN;
  double apxAccE;
  double apxAccD;
  double apxRoll;
  double apxPitch;
  double apxHeading;
  double apxRollRate;
  double apxPitchRate;
  double apxHeadingRate;

// estimate accuracy

  double apxHeight_accuracy;
  double apxNorth_accuracy;
  double apxEast_accuracy;
  double apxVelN_accuracy;
  double apxVelE_accuracy;
  double apxVelD_accuracy;
  double apxAccN_accuracy;
  double apxAccE_accuracy;
  double apxAccD_accuracy;
  double apxRoll_accuracy;
  double apxPitch_accuracy;
  double apxHeading_accuracy;
  double apxRollRate_accuracy;
  double apxPitchRate_accuracy;
  double apxHeadingRate_accuracy;


  DGCcondition newData;


  // Lock these for the m_HeartbeatMutex


  // end lock list


  // Variables to remain unlocked
  int quitPressed;

  int snKey;  // Should be set in AState_Main and never changed.
  astateOpts _options; 
  unsigned long long startTime; // Should be set in constructor and never changed.

  // Initial offset for bogus local pose
  bool haveInitPose;
  double initE, initN;

  fstream apxLogStream; // These four are opened & closed in constructor and destructor
  fstream stateLogStream;   
  char apxLogFile[100];
  char stateLogFile[100];
  fstream replayStateStream;  
  char replayStateFile[100];

  /*****************************************************************
   *  The following are the private methods of the AState class.
   *****************************************************************/

  
   // initialize POS-LV
//  void apxInit();
  
  /*! Function that updates the vehicle_state struct */
  void updateStateThread();

  /*! Function to broadcast state data */
  void broadcast();

  /*! Thread to read from the POS-LV. */
  void apxReadThread(); 
  
  /*! Thread to control POS-LV. */
//  void apxControlThread();

  /*! Thread to update sparrow variables*/
  void sparrowThread();

//  void metaStateThread();

  void playback();



public:
  /*! AState Constructor */
  AState(int skynetKey, astateOpts options);

  /*! Function to restart AState without having to stop/start the
   *  whole system
   */
 // void restart();
  void active();
  

  ~AState();
};

#endif
