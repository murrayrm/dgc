//to use:
//reserve memory for app_msg
//
//refill buffer and make sure to keep track of the buffer length used so far
//
//when doneFLAG == 1, read the message and reset doneFLAG to 0 (will we still work now?)
//
//when the inpbufpos == inpbufferlen, refill the buffer
//
//if the buffer isn't empty, but we've finished the message, then what?

#define APPNUMHEADERS 2
#define APPLENHEADERS 4
#define APPLENHDRLEN 2
#define APPNUMTAILS 1
#define APPLENTAILS 2
#define APPUNKNOWNMSG -1
#define APPMAXMSGLEN 140

// to be declared elsewhere
//char * inpbuffer; //assume that memory has been allocated for the input buffer
//int inpbufferpos, inpbufferlen;

class apxParser
{
public:
	void apxParser(void);
	//if parse returns non-0, you may read the message from app_message. then, reset the parser.
	int parse(char * inpbuffer, int * inpbufferpos, int inpbufferlen);
	char * app_msg;
	void reset(void);
	
private: 
	char app_headers[NUMHEADERS][LENHEADERS]; // = {{'$','G','R','P'},{'$','M','S','G'}};
	int app_msglens[NUMHEADERS]; // = {140,16};
	char app_tails[NUMTAILS][LENTAILS]; // = {{'$','#'}};
	
	int app_msglen = 0; //the length of the message already read in
	int app_thismsglen = 0; //the length of the message being waited on
	int msgtype = -1; //the type of the message (can't be greater than APPNUMHEADERS)
}


void apxParser::apxParser(void)
{
	char app_headers1 = {{'$','G','R','P'},{'$','M','S','G'}};
	int app_msglens1 = {140,16};
	char app_tails1 = {{'$','#'}};
	
	//assigning the object-wide arrays
	for (int ii=0;ii<APPNUMHEADERS;ii++) 
	{ 
		for (jj=0;jj<APPLENHEADERS;jj++) 
		{
			app_headers[ii][jj] = app_headers1[ii][jj];
		}
	}
	for (int ii=0;ii<APPNUMTAILS;ii++) 
	{ 
		for (jj=0;jj<APPLENTAILS;jj++) 
		{
			app_tails[ii][jj] = app_tails1[ii][jj];
		}
	}
	for (int ii=0;ii<NUMHEADERS;ii++)
	{
		app_msglens[ii] = app_msglens1[ii];
	}
	
		
		
	this.reset();
}

void apxParser::reset(void)
{	
	int app_msglen = 0; //the length of the message already read in
	int app_thismsglen = 0; //the length of the message being waited on
	int msgtype = -1; //the type of the message (can't be greater than APPNUMHEADERS)
}


int apxParser::parse(char * inpbuffer, int * inpbufferpos, int inpbufferlen)
{
  int amout2copy=0;
  int doneFLAG = 0;
  int inpbufpos = *inpbufferpos;

  //___main code
  
  
  //if we're done reading from the input buffer, we obviously have no more to do
  while ((inpbufpos < inpbufferlen) && (!doneFLAG))
  {
	switch (app_msglen) 
	{
	case 0:  //not found the header yet.  here, we can look for any message
	case 1:
	case 2:
	case 3: //the default message length is 4.  unfortunately, there was no good way to build this into the case statement
		/*for (int ii=0;ii<APPNUMHEADERS;ii++)
		{
			if (app_headers[ii][app_msglen]==buffer[inpbufpos])
			{
				app_msg[app_msglen] = buffer[inpbufpos];
				//we can advance the buffer position, because we've read what we want
				app_msglen++; inpbufpos++;
				break; //done with looking for a piece of message - break out of this code
			}
		}*/

		//give the benefit of the doubt
		app_msg[app_msglen] = buffer[inpbufpos];
		
			for (int ii=0;ii<APPNUMHEADERS;ii++)
			{ //check each header type
				for (int jj=0;jj<app_msglen;jj++)
				{ //look for an exact match
					if (app_headers[ii][jj] != app_msg[app_msglen+jj]) { msgtype = -1; } else { msgtype = ii; }
				}
				if (-1 != msgtype) { app_msglen++; inpbufpos++; break; }
			}
			//if we found the message, we would have broken. otherwise...
			if (-1 == msgtype)
			{ //reset the message
				cout << "bad message found by applanix parser";
				app_msglen = 0;
			}
		
		//
		break;
	
	case 4:
	case 5: //reading in the purported length of the message 
		app_msg[app_msglen] = buffer[inpbufpos];
		app_msglen++; inpbufpos++;
		break;
		
	default :
		if ((APPLENHEADERS+APPLENHDRLEN) == app_msglen) // just started the message body
		{
			//the rest goes in here
			//check for legality.  we've already made sure that we had _some_ type of message.  now,
			//let's figure out exactly which one

			//- presumably, the message length is
			//stored as a binary number, but big- or little-endian?
			//assume signed, big-endian (whatever endian the operating system is), 32-bit integer
			//app_thismsglen = app_thismsglen + (buffer[inpbufpos]<<(8*(app_msglen-4)));
			app_thismsglen = *((signed _int16 *) &(app_msg[4])); //app_msg
			if (app_msglens[msgtype] != app_thismsglen)
			{
				//print a warning
				cout << "incorrect length tag for message"; 
			}

		}
		if (app_msglen > APPMAXMSGLEN)
		{
			cout << "message far too long";
		}
			
		//if the header's already been read in, copy the rest of the available message
		if (inpbufferlen-inpbufpos > app_thismsglen-app_msglen)
		{
			amount2copy = app_thismsglen-app_msglen;
		}
		else
		{
			amount2copy = inpbufferlen-inpbufpos;
		}
		memcpy(&(app_msg[app_msglen]),&(buffer[inpbufpos]),amount2copy)	
		app_msglen += amount2copy;
		inpbufpos +=	amount2copy;
		
		if (app_thismsglen == app_msglen)
		{
			//done w/ this message
			doneFLAG = 1;
		}

		
		//if the message has grown too long....
		//if we've waited too long to copy the message...
		
		//reset all of the variables
		break;
	}
  }
  
  //reset this variable for use next time
  *inpbufferpos = inpbufpos;
  
  if (!doneflag)
  {
	return 0;
  }
  else
  {
	return app_msglen;
  } 
}
