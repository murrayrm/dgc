#include <iostream>
#include <iomanip>
#include "DGCutils"
#include "CTimber.hh"
#include "TimberConfig.hh"
#include <getopt.h>

using namespace std;


enum
  {
    OPT_HELP,
    OPT_PLAYDIR,
    OPT_LAST
  }; // enum for getopt

void printHelp();

int main(int argc, char** argv)
{
  static struct option long_options[] = 
    {
      //Options that don't require arguments:
      {"help", no_argument, 0, OPT_HELP},
      //Options that require arguments
      {"dir", required_argument, 0, OPT_PLAYDIR},
      {0,0,0,0}
    };

  // process command line options
  int the_optind, option_index, c;
  char* play_dir = new char [MAX_PATH_LENGTH];
  play_dir[0] = '\0';

  while (true)
    {
      the_optind = optind ? optind : 1;
      option_index = 0;

      c = getopt_long_only(argc, argv, "", long_options, &option_index);

      if(c == -1) break;

      switch(c)
	{
	case OPT_HELP:
	  printHelp();
	  return 0;

	case OPT_PLAYDIR:
	  sprintf(play_dir, "%s", optarg);
	  break;
	case '?':
	default:
	  cout << "Error processing options, enum value is " << c << " (char is \"" 
	       << (char)c << "\")" << endl;
	  exit(1);
	}
    }
  
  int skynet_key;
  char* p_skynet_key = getenv("SKYNET_KEY");
  if ( p_skynet_key == NULL )
    {
      cout << "You need to set a skynet key!" << endl;
      return 1;
    }
  else
    {
      skynet_key = atoi(p_skynet_key);
      cout << "Using skynet key: " << skynet_key << endl;
    }

  /** pass the string containing the initial playback dir to timber constructor */
  string str(play_dir);
  CTimber timberObj(skynet_key, play_dir);
  timberObj.TimberActiveLoop();

  return 0;
}

void printHelp()
{
  cout << "Usage       ./timber [options]\n"
       << "\n"
       << "Options     --help            Print this.\n"
       << "            --dir directory   Set the playback directory.  `directory' should be absolute,\n"
       << "                              not relative (it should start with a `/'), and should\n"
       << "                              include the timestamp at the end, but no module name.  See\n"
       << "                              example below.\n"
       << "\n"
       << "Example     To just do logging (no playback):\n"
       << "              ./timber\n"
       << "\n"
       << "            To do playback from a certain directory:\n"
       << "              ./timber --dir /tmp/logs/2005_07_21/17_59_43/" << endl;
}
