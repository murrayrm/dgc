/*----------------------------------------------------------------------------
--                                                                          --
--                        U N C L A S S I F I E D                           --
--                                                                          --
--                  L I T T O N   P R O P R I E T A R Y                     --
--                                                                          --
-- Copyright (c) 2004, Litton Systems, Inc.                                 --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------
--                                                                          --
--                       RESTRICTED RIGHTS LEGEND                           --
--                                                                          --
-- Use, duplication or disclosure is subject to restrictions stated in      --
-- Proprietary Information Agreement between Litton Systems, Inc.,          --
-- Guidance and Controll Systems Division and California Institute of       --
-- Technology.                                                              --
--                                                                          --
----------------------------------------------------------------------------*/

README - 2004/12/22

The following documents the LN-200 interface to the lords Linux box. All file,
except where noted, are contained in the "/ln-200" directory on the lords box.

File List / Description

	Makefile

		Makes example_application program.

	esccp.ko

		Local (/ln-200 dir) copy of the FastComm ESCC/P card
		device driver.

	esccpdrv.h

		ESCC/P card device driver include file for application programs.

	example_application
	example_application.c
	example_application.o

		Example application program, source, object file.

	fastcomm_esccp_setup

		Program to initialize the ESCC/P card for the LN-200.

	/lib/modules/2.6.9-rc3/kernel/drivers/char/esccp.ko

		Copy of FastComm ESCC/P card device driver in the appropriate
		system device driver directory. This is the copy of the device
		driver that is actually loaded.

System Initialization

	The following steps are required after powering up the linux box:

	1) Load the device driver.

		Execute the following statement as root:

		insmod /lib/modules/2.6.9-rc3/kernel/drivers/char/esccp.ko

	2) Initialize the device driver and ESCC/P card to receive LN-200 data.

		Execute the following statment as root:

		/ln-200/fastcomm_esccp_setup

	NOTE: Ideally, the above 2 initialization statements should be run
	automatically on system turn-on. Have the Sys Admin add the statements,
	where appropriate, to your box.

Example Application Program

	To run the example application program, execute the following statement:

		/ln-200/example_application

	To modify the example application program, edit example_application.c
	then run make on the Makefile.

	The example application program is commented to hopefully assist
	Cal Tech in creating their own application program :-)

Cabling

	The FastComm ESCC/P card installed in the lords box has terminating
	resistors R11 and R12 removed  (Channel 1 data and clock terminators).
	To use the NG supplied cabling, plug either DC-37 connector (labeled P3
	or P4) into the lords box, then plug the other DC-37 connector into
	another UN-MODIFIED ESCC/P card (installed in another linux box) or
	plug the black terminator plug (made by NG, shipped to Cal Tech with
	this documentation) into the unused DC-37 connector (the cable supplied
	with the ESCC/P card is not used with the NG cable).

Installation on another Linux Box

	1) Copy the /ln-200 directory and its contents to the other box.

	2) Copy /ln-200/esccp.ko to
	/lib/modules/2.6.9-rc3/kernel/drivers/char/esccp.ko.

	3) Add initialization statements to a Linux startup file.
	See the "System Initialization" section above.

	THIS WILL ONLY WORK IF THE NEW BOX TO INSTALL ON HAS A VERSION OF
	LINUX IDENTICAL TO THE VERSION INSTALLED ON THE LORDS BOX. OTHERWISE,
	THE DEVICE DRIVER WILL HAVE TO BE COMPILED FROM SOURCE CODE FOUND ON
	THE CD SUPPLIED BY THE MANUFACTURER OF THE ESCC/P CARD. INSTRUCTIONS
	ON HOW TO DO THIS ARE INCLUDED ON THE CD.

	If compiling the device driver, the mkdev.sh installation script must
	be modified to install the device driver in the correct directory.
	For the lords box version of Linux, change the MODPATH configuration
	line to:

		MODPATH=/lib/modules/`uname -r`/kernel/drivers/char

Trouble Shooting

	Device Driver not loaded:

		If you invoke either fastcomm_esccp_setup or example_program
		and get the following: 

			Cannot open /dev/escc0.
			No such device or address

		the device driver has not been loaded. Load the device driver
		by executing the following statement as root:

			insmod /lib/modules/2.6.9-rc3/kernel/drivers/char/esccp.ko

	Device Driver (and ESCC/P Card) not initialized:

		If you invoke example_program and get the following:

			Read failed, errno = 16.
			Device or resource busy

		the device driver and ESCC/P card have not been intitialized.
		Initialize by executing the following statement as root:

			/ln-200/fastcomm_esccp_setup

Support

	Douglas Haroldsen
	818 715-2292
	douglas.haroldsen@ngc.com
