/*
 * \file sn_msg.hh 
 * \brief Header file for skynet messaging system
 *
 * RMM, 19 Dec 06 (based on previous version)
 *
 * This is the header file for skynet messages.  It defines the basic
 * skynet class and also includes the statically defined message types.
 *
 */

#ifndef _SN_MSG_HH_
#define _SN_MSG_HH_

#include "sn_types.h"			// Statically defined message types 
#include <pthread.h>
#include <sp.h>				// Spread header file
#include <string>

using namespace std;

#define MAX_SIZE_HOSTNAME 64
struct SSkynetID
{
  char       host[MAX_SIZE_HOSTNAME];
  modulename name;
  mailbox    unique;
	int        status;
} __attribute__((packed));

/*!
 * \class skynet
 * \brief Skynet message class
 *
 * The skynet class maintains a connection with the spread server and 
 * sends messages back and forth.
 *
 */
class skynet
{
  int               m_key;		///< Skynet key 
  modulename        m_name;		///< Module (process) name
  mailbox*          m_pMailboxes;	///< Mailboxes for receiving data
  string*           m_pGroupnames;	///< List of groups we belong to
  int               m_numMailboxes;	///< Number of mailboxes

  mailbox           m_sendMailbox;	///< Mailbox for sending data
  int               m_highestOpenMailboxIndex; 

  pthread_mutex_t   m_mailboxMutex;	///< Mutex for mailbox access
  int*              m_pStatus;		///< Return status information

  /* Private functions; doxygen comments are in source code */
  mailbox           spreadConnect(void);
  void              makeGroupName(string* pGroupName, sn_msg type);

public:
  skynet(modulename myname, int key, int* pStatus=NULL);
  ~skynet();

  int listen(sn_msg type, modulename somemodule);

  int get_send_sock(sn_msg type);

  /** sn_select(mboxidx) waits for data to be available on that mailbox */
  void sn_select(int mboxidx);

  /** check to see if more messages are available */
  bool is_msg(int mboxidx);
	
  size_t get_msg(int mboxidx, void *mybuf, size_t bufsize, int options = 0);
  size_t get_msg(int mboxidx, void *mybuf, size_t bufsize, int options,
		 pthread_mutex_t** ppMutex, bool bReleaseMutexWhenDone = true, int numMutices = 1);

  size_t send_msg(int type, void* msg, size_t msgsize, int options = 0);
  size_t send_msg(int type, void* msg, size_t msgsize, int options,
		  pthread_mutex_t** ppMutex, bool bReleaseMutexWhenDone = true, int numMutices = 1);

  size_t send_msg(int type, const scatter* msgs, int options = 0);
  size_t send_msg(int type, const scatter* msgs, int options,
		  pthread_mutex_t** ppMutex, bool bReleaseMutexWhenDone = true, int numMutices = 1);

  /** Get the status of this skynet port */
  int status() { return m_pStatus == NULL ? -1 : *m_pStatus; }

  /* Accessor functions */
  modulename name() { return m_name; }
  mailbox sendMailbox() { return m_sendMailbox;	} 

  /*
   * Skynet ID functionality
   *
   * This portion of the class is used to implement the broadcast ID
   * functionality.
   */

private:
  pthread_t	    m_idthread;		///< Thread for broadcasting id

public:
  void *I_am_here(void *);
};
#endif
