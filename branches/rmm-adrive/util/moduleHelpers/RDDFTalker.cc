/*!**
 * Lars Cremean and Nok Wongpiromsarn
 * December 8, 2006
 *
 * The CRDDFTalker class provides a mechanism for sending and receiving
 * RDDFs across skynet.
 *
 * Someone should add some comments on the basic mechanism that is
 * used, epsecially the use of streams in the receive function, which
 * I don't understand (RMM, 9 Dec 06).
 */

#include <iostream>
#include <strstream>
#include <iomanip>
#include "RDDFTalker.hh"

using namespace std;

/******************************************************************************/
// Basic constructor: no threads
CRDDFTalker::CRDDFTalker() : m_receivedRddf(NULL, true), m_newRddf(NULL, true)
{
  m_pRDDFDataBuffer = new char[RDDF_DATA_PACKAGE_SIZE];
  m_bRunGetRDDFThreads = false;
  DGCcreateMutex(&m_RDDFDataBufferMutex);
}

/******************************************************************************/
// Basic constructor with option to start a talker thread using rddfSocket
CRDDFTalker::CRDDFTalker(bool runGetRDDFThread) : m_receivedRddf(NULL, true), m_newRddf(NULL, true)
{
  m_pRDDFDataBuffer = new char[RDDF_DATA_PACKAGE_SIZE];
  m_bRunGetRDDFThreads = runGetRDDFThread;
  DGCcreateMutex(&m_RDDFDataBufferMutex);
  if (m_bRunGetRDDFThreads)
  {
  	int rddfSocket = m_skynet.listen(SNrddf, MODtrafficplanner);
  	DGCcreateMutex(&m_ReceivedRDDFMutex);
  	DGCcreateCondition(&condNewRDDF);
  	condNewRDDF.bCond = false;
  	DGCstartMemberFunctionThreadWithArg(this, &CRDDFTalker::getRDDFThread, (void*)rddfSocket);
  }
}

/******************************************************************************/
// Constructor for talking across a specific socket
CRDDFTalker::CRDDFTalker(bool runGetRDDFThread, int socket) : m_receivedRddf(NULL, true), m_newRddf(NULL, true)
{
  m_pRDDFDataBuffer = new char[RDDF_DATA_PACKAGE_SIZE];
  m_bRunGetRDDFThreads = runGetRDDFThread;
  DGCcreateMutex(&m_RDDFDataBufferMutex);
  DGCcreateMutex(&m_ReceivedRDDFMutex);
  DGCcreateCondition(&condNewRDDF);
  condNewRDDF.bCond = false;
  if (m_bRunGetRDDFThreads)
  	DGCstartMemberFunctionThreadWithArg(this, &CRDDFTalker::getRDDFThread, (void*)socket);
}

/******************************************************************************/
CRDDFTalker::~CRDDFTalker()
{
  delete [] m_pRDDFDataBuffer;
  DGCdeleteMutex(&m_RDDFDataBufferMutex);
  if (m_bRunGetRDDFThreads)
  {
  	DGCdeleteMutex(&m_ReceivedRDDFMutex);
  	DGCdeleteCondition(&condNewRDDF);
  }
}

/******************************************************************************/
// Send an RDDF accross skynet
bool CRDDFTalker::SendRDDF(int rddfSocket, RDDF* pRddf, pthread_mutex_t* pMutex)
{
  int bytesToSend;
  int bytesSent;
  char* pBuffer = m_pRDDFDataBuffer;

  // Initialize
  char tmp[100] = "\0";
  pBuffer[0] = '\0';
  RDDFVector wp = pRddf->getTargetPoints();

  // Print out the RDDF so we can see what is going on
  for (int i=0; i<pRddf->getNumTargetPoints(); i++)
  {
  	/*
    sprintf(tmp, "%d,%.8f,%.8f,%.3f,%.3f\n", 
      wp[i].number, wp[i].latitude, wp[i].longitude, 
      wp[i].radius/METERS_PER_FOOT, wp[i].maxSpeed/MPS_PER_MPH);
    */  
    
    //Note that radius is in meters and maxSpeed is in MPH
    sprintf(tmp, "%d,%.8f,%.8f,%.3f,%.3f\n", 
      wp[i].number, wp[i].latitude, wp[i].longitude, 
      wp[i].radius, wp[i].maxSpeed);
  
    strncat(pBuffer, tmp, strlen(tmp));

    //cout << tmp;
  }

  //printf("\n[%s:%d] \n", __FILE__, __LINE__);
  //cout << pBuffer;

  // Lock mutexes so that we can access the RDDF data    
  DGClockMutex(&m_RDDFDataBufferMutex);
  if (pMutex != NULL)
  {
    DGClockMutex(pMutex);
  }

  // Send the RDDF accross skynet
  bytesToSend = RDDF_DATA_PACKAGE_SIZE;
  bytesSent = m_skynet.send_msg(rddfSocket, m_pRDDFDataBuffer, bytesToSend, 0);

  // Unlock mutexes
  DGCunlockMutex(&m_RDDFDataBufferMutex);
  if (pMutex != NULL)
  {
    DGCunlockMutex(pMutex);
  }

  // Check for any errors and print a message
  if (bytesSent != bytesToSend)
  {
    cerr << "CRDDFTalker::SendRDDF(): sent " << bytesSent << 
      " bytes while expected to send " << bytesToSend << " bytes" << endl;

    return false;
  }
  
  return true;
}


/******************************************************************************/
// Receive an RDDF accross skynet
bool CRDDFTalker::RecvRDDF(int rddfSocket, RDDF* pRddf, pthread_mutex_t* pMutex, 
                           string* pOutstring)
{
  int bytesToReceive;
  int bytesReceived;
  char* pBuffer = m_pRDDFDataBuffer;

  // Build the mutex list. We want to protect the data buffer
  int numMutices = 1;
  pthread_mutex_t* ppMutices[2];
  ppMutices[0] = &m_RDDFDataBufferMutex;
  if (pMutex != NULL)
  {
    ppMutices[1] = pMutex;
    numMutices++;
  }

  // Get the RDDF data from skynet. We want to receive the whole segGoals, locking
  // the requesting the mutices to be locked and NOT unlocked at the end. This
  // is done because this data is still accessed afterwards and we manually
  // unlock the mutices later.
  bytesToReceive = RDDF_DATA_PACKAGE_SIZE;
  
  bytesReceived = m_skynet.get_msg(rddfSocket, pBuffer, bytesToReceive, 
                                   0, ppMutices, false, numMutices);

  // Check to make sure we received everything correctly
  if (bytesReceived != bytesToReceive)
  {
    cerr << "CRDDFTalker::RecvRDDF(): received" << bytesReceived << 
      "bytes while expected to receive " << bytesToReceive << "bytes" << endl;

    DGCunlockMutex(&m_RDDFDataBufferMutex);
    if (pMutex != NULL)
    {
      DGCunlockMutex(pMutex);
    } 
    return false;
  }

  /*! Not sure what pOutstring does; can someone add a comment? !*/
  if (pOutstring != NULL)
  {
    pOutstring->append((char*)&bytesReceived, sizeof(bytesReceived));
    pOutstring->append(m_pRDDFDataBuffer, bytesReceived);
  }

  //printf("Raw characters received: ");
  //for (int i=0; i<RDDF_DATA_PACKAGE_SIZE; i++) 
  //{ 
  //  printf("%c",pBuffer[i]); 
  //}
  //printf("\n");

  /* Load the character array into an istream and call loadStream */
  istrstream recvstream(pBuffer, RDDF_DATA_PACKAGE_SIZE);
  
  //printf("[%s:%d] \n", __FILE__, __LINE__);
  //cout << recvstream.str() << endl;

  pRddf->loadStream(recvstream);

  DGCunlockMutex(&m_RDDFDataBufferMutex);
  if (pMutex != NULL)
  {
    DGCunlockMutex(pMutex);
  }

  return true;
}


/******************************************************************************/
bool CRDDFTalker::NewRDDF()
{
  return condNewRDDF.bCond;
}

/******************************************************************************/
void CRDDFTalker::UpdateRDDF()
{
  DGClockMutex(&m_ReceivedRDDFMutex);
  m_newRddf = m_receivedRddf;
  DGCunlockMutex(&m_ReceivedRDDFMutex);
  condNewRDDF.bCond = false;
}

/******************************************************************************/
void CRDDFTalker::WaitForRDDF()
{
  DGCWaitForConditionTrue(condNewRDDF);
  UpdateRDDF();
}

/******************************************************************************/
void CRDDFTalker::getRDDFThread(void* pArg)
{
  int rddfSocket = (int)pArg;
  while(true)
  {
  	RDDF rddf(NULL, true);
  	bool rddfReceived = RecvRDDF(rddfSocket, &rddf);
  	if (rddfReceived)
  	{
  	  DGClockMutex(&m_ReceivedRDDFMutex);
  	  m_receivedRddf = rddf;
  	  DGCunlockMutex(&m_ReceivedRDDFMutex);
      DGCSetConditionTrue(condNewRDDF);
  	}
  	usleep(100000);
  }	
}
