MATIO_PATH = $(DGC)/util/matio/

MATIO_DEPEND = \
	$(MATIO_PATH)/config.h \
	$(MATIO_PATH)/endian.c \
	$(MATIO_PATH)/inflate.c \
	$(MATIO_PATH)/io.c \
	$(MATIO_PATH)/mat5.c \
	$(MATIO_PATH)/matio.h \
	$(MATIO_PATH)/snprintf.c
