package edu.caltech.me75;

/**
 * Used to represent Latitude-Longitude coordinates.
 * 
 * @author Defense Mapping Agency
 */
class LatLon
{
    public LatLon( double lat, double lon )
    {
	this.lat = lat;
	this.lon = lon;
    }

    public LatLon( byte latDeg, float latMin, short longDeg, float longMin )
    {
	this.lat = latDeg + (latMin / 60.0);
	this.lon = longDeg + (longMin / 60.0);
    }

    public double getLat() { return lat; }
    public byte getLatDeg() { return (byte)lat; }
    public float getLatMin() { return (float)((lat - getLatDeg()) * 60.0); }
 
    public double getLong() { return lon; }
    public short getLongDeg() { return (short)lon; }
    public float getLongMin() {return (float)((lon - getLongDeg()) * 60.0); }

    public String toString()
    {
	return "Latitude: " + getLatDeg() + " deg " + getLatMin() + "'"
	    + ", Longitude: " + getLongDeg() + " deg " + getLongMin() + "'";
    }

    private double lat;
    private double lon;

    //    private byte latDeg; // north is positive
    //private float latMin;
    //    private short latMinDec;

    //private short longDeg; // east is positive
    //private float longMin;
    //    private short longMinDec;
}
