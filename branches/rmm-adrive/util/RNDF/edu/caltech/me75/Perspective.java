package edu.caltech.me75;
import java.util.LinkedList;

/**
 * A class to calculate the 
 * center and spread of the
 * waypoints
 * 
 * Used to dynamically resize the Viewer
 * so that our RNDF viewer can handle 
 * RNDF maps of all sizes
 * 
 * @author Xiang Jerry He
 */
class Perspective {
	/** a list of all waypoints in our map */
	protected LinkedList<WayPoint> globalWaypointsRaw;
	/** the geometric center of all waypoints */
	protected WayPoint center;
	/** maximum x-coordinate deviation */ 
	double maxXDev;
	/** maximum y-coordinate deviation */ 
	double maxYDev;
	/** average of all x-coordinates */
	double meanX;
	/** average of all y-coordinates */
	double meanY;
	int width, height;
	/** the ratio between the 
	 * actual distance(in degrees) 
	 * and the displayed distance(in pixels)
	 */
	double resizeRatio;
	
 /**
  * 
  * @param m       the map whose perspective we want to calculate
  * @param width   the width of the display component(in pixels)
  * @param height  the height of the display component(in pixels)
  */
	public Perspective(Map m, int width, int height) {
		int count=0;
		double sumX=0;
		double sumY = 0;
		globalWaypointsRaw = new LinkedList<WayPoint>();
		this.height = height;
		this.width = width;
		for(Segment s: m.segs) {
			for(Lane l: s.lanes) {
				for(WayPoint p: l.waypoints) {
					globalWaypointsRaw.add(p);
					sumX += p.x;
					sumY += p.y;
					count++;
				}
			}
		}
		for(Zone z: m.zones) {
			for(Spot s: z.spots) {
				globalWaypointsRaw.add(s.inPoint);
				globalWaypointsRaw.add(s.checkPoint);
				sumX += s.inPoint.x + s.checkPoint.x;
				sumY += s.inPoint.y + s.checkPoint.y;
				count += 2;
			}
			for(WayPoint p: z.perimeter.points) {
				globalWaypointsRaw.add(p);
				sumX += p.x;
				sumY += p.y;
				count++;
			}
		}
		meanX = sumX/count;
		meanY = sumY/count;
		calculateDev(globalWaypointsRaw, meanX, meanY);
	}
	
	private void calculateDev(LinkedList<WayPoint> g, double meanX, double meanY) {
		maxXDev = 0; 
		for(WayPoint p:g) {
			double devX = Math.abs(p.x - meanX);
			if(devX > maxXDev) 
				maxXDev = devX;
			double devY = Math.abs(p.y - meanY);
			if(devY > maxYDev)
				maxYDev = devY;
		}
		/** the following is exactly the same
		 * code as setPerspective
		 * inlined here for efficiency
		 */
		double xratio = width/maxXDev/2.0/1.2;
		double yratio = height/maxYDev/2.0/1.2;
		resizeRatio = (xratio > yratio? yratio:xratio); 
	}
	
	
	protected void setPerspective(int height, int width) {
		double xratio = width/maxXDev/2.0/1.2;
		double yratio = height/maxYDev/2.0/1.2;
		resizeRatio = (xratio > yratio? yratio:xratio);  
	
	}
	
	protected float cX(double x) {
		return (float)((x - meanX + maxXDev*1.2)*resizeRatio);
	}
	
	protected float cY(double y) {
		return (float)((y - meanY + maxYDev*1.2)*resizeRatio);
	}
	
	protected double pixel2Lat(int x) {
		return (double)((x/resizeRatio) + meanX - maxXDev*1.2);
	}
	
	protected double pixel2Lon(int y) {
		return (double) ((y/resizeRatio) + meanY - maxXDev*1.2);
	}
	
	/* TODO have a part that displays UTM
	 *  of current mouse position
	 public Waypoint getUTM(Waypoint w) {
		
	}*/
	
}