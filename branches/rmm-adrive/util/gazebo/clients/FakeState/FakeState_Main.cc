/*
 * FakeState_Main - argument parsing for FakeState
 *
 * JML, Dec 04
 *
 * 22 Mar 04, RMM: added argument processing for offsets
 * 23 Mar 04, KLK: cahnged to new method of pushing state out.
 */
#define DEBUG false

#include "FakeState.hh"
#include "DGCutils"
#include <fstream>
#include <iostream>
#include "rddf.hh"

#include <string>
#include <cstdio>
#define FAKESTATE_PUSH_SLEEP 5000 //changed from 5000 chris

int main(int argc, char **argv)
{
        int c, errflg = 0;
	int nflg = 0, eflg = 0;
	double northing_off, easting_off;
	int sn_key = 0;
	unsigned int startingpoint=0;
	ifstream simfile("simInitState.dat");

	string simtype;
	int startwp;
	float n, e, x, y, yaw, temp;
	string tmp;

	/* Process command line arguments */
	while ((c = getopt(argc, argv, "n:e:f:")) != EOF)
	  switch (c) {
	  case 'n':
	    northing_off = atof(optarg);
	    nflg = 1;
	    break;

	  case 'e':
	    easting_off = atof(optarg);
	    eflg = 1;
	    break;

	  default:
	    errflg++;
	    break;
	  }
	if (errflg) {
	  fprintf(stderr, 
	    "usage: %s [-n northing_offset] [-e easting offset] [sn_key]\n",
	    argv[0]); 
	  exit(2);
	}

	char* default_key = getenv("SKYNET_KEY");
	if (argc > optind)
	  {
	    sn_key  = atoi(argv[optind]);
	  }
	else if (default_key != NULL )
	  {
	    sn_key = atoi(default_key);
	  }else
	{
		cerr << "SKYNET_KEY environment variable isn't set." << endl;
	}
	cerr << "Constructing skynet with KEY = " << sn_key << endl;
	FakeState ss(sn_key,1,1,1,1,1);


	printf("Reading RDDF waypoints from rddf.dat\n");
	RDDF rddf("rddf.dat");
	RDDFData w0;
	RDDFData w1;
	// Set the offset of the Northing and Easting
	// Default = Stoddard Wells starting point

	simfile >> simtype;

	if(simtype == "relative2")
	  {
	    simfile >> startwp >> x >> y >> yaw;
	    w0 = rddf.getTargetPoints().at(startwp);
	    w1 = rddf.getTargetPoints().at(startwp+1);
	    
	    // calculate the angle to the next waypoint
	    yaw = 180.0/3.14159*(atan2( w1.Northing-w0.Northing,w1.Easting -w0.Easting)+ yaw);
	    //cout << yaw<< endl;
	    n = w0.Northing - sqrt(x*x + y*y)*sin(atan2(w1.Northing-w0.Northing,w1.Easting -w0.Easting)+ atan2(y,x));
	    e = w0.Easting  + sqrt(x*x + y*y)*cos(atan2( w1.Northing-w0.Northing,w1.Easting -w0.Easting)+ atan2(y,x));
	  }
	else if(simtype == "relative")
	  {
	    simfile >> startwp >> temp >> temp >> temp >> temp >> temp >> temp >> yaw;
	    
	    yaw= yaw*180.0/3.14159;
	    
	    w0 = rddf.getTargetPoints().at(startwp);
	    n = w0.Northing +n;
	    e = w0.Easting + e;
	  }
	else if(simtype == "absolute")
	  {
	    simfile >> n >> temp >> temp >> e >> temp >> temp >> yaw;
	    yaw= yaw*180.0/3.14159;
	  }
	else 
	  {
	    cout << "Cannot parse simInitState.dat"<< endl;
	    return 0;
	  }
	simfile.close();

	ss.Northing_Offset = nflg ? northing_off : rddf.getWaypointNorthing(0);
	ss.Easting_Offset  = eflg ? easting_off  : rddf.getWaypointEasting(0);
	//ss.Northing_Offset = nflg ? northing_off : 3777395.000;   //for Santa Anita
	//ss.Easting_Offset = eflg ? easting_off : 403533.000;      // for Santa Anita

	cout << "Starting at Northing :"<< ss.Northing_Offset<<" Easting :" << ss.Easting_Offset << endl;
	
	while (true) 
	  {
	    ss.UpdateState();
	    ss.Broadcast();
	    usleep(FAKESTATE_PUSH_SLEEP);
	  }
	return 0;
}
