#ifndef TRACKMO_HH
#define TRACKMO_HH

#include <pthread.h>
#include <fstream>
#include <unistd.h>
#include <sys/time.h>
#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <vector>

#include "sn_msg.hh"
#include "DGCutils"
#include "StateClient.h"
#include "Matrix.hh"
#include "../../../davidbolin/movingObstacle.hh"

#define PI 3.14159265
#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))

#define NUMSCANPOINTS 181

//the constants that determine the behavior of the segmentation/tracking
#define MAXTRAVEL 1.5 //max dist obj can travel btwn frames
#define MOTIMEOUT 15  //number of frames we'll track car after last seeing it
#define CARMARGIN 1.0 //if it's this close to a car, classify it as part

#define MAXNUMCARS MAX_NBR_MOVING //max num cars we can track
#define MAXNUMSENT MAX_NBR_MOVING  //max num cars we'll send to FM

#define MINCARLENGTH .50 // size thresholds for 'cars' (m)
#define MAXCARLENGTH 7.00
#define MINNUMPOINTS 3 //min # pts to consider object a 'car'
#define MINSPEED 4   //min velocity to bother sending out
#define MAXSPEED 20.0 //max velocity 
#define MINDIMENSION 2 //we're assuming this is the minimum car dimension 
                       //used when we can only see 1 side...

#define LADARXOFFSET .95 //where the LADAR is relative to vehicle origin

#define SCALINGFACTOR 4 //used in determining discontinuities for segmentation

//#warning "HACK!! FIND BETTER WAY TO DEAL W/ THIS (is it even necessary?)"
#define STDDEVANGLES .05*PI/180 //std dev, in rad, of angles
#define STDDEVRANGES .05 //standard dev, in m, of range measures

#define USE_STATE_FOR_READING //whether we want to use state from
//simulator when sending out points read from a file

#define USE_CORNER_TRACK //old tracking method, for comparison
//#define USE_PERP_TRACK //new method for finding dist btwn 2 cars

//#define CHECK_MOTION_PROBABILITY //uses scan differences to detect motion
#define CHECK_MOTION_KF_VEL //uses an objects kalman filtered velocity to classify as car

using namespace std;

/**
 * This struct is how we store all the information about a car that we ever use
**/
struct carRepresentation{

  //unique ID # for this car (used by mapping...)
  int ID;

  //for storing the car's corner point, and the endpoints of segments
  double e,de1,de2;
  double n,dn1,dn2;
  double len1, len2;

  //for storing the Car's history (mapper & prediction may want this)
  double lastE[10];
  double lastN[10];
  int histPointer; //this (mod 10) is the spot we're at in the history

  //the last scan we saw the car at, and how many times
  int lastSeen;
  int timesSeen;

  //points at which the segment starts and ends...useful for debugging, and
  //necessary for updateCar (at least for now...once we no longer use
  //trackCars for initial association, we can get rid of it, i think)
  int segStart, segEnd; 
  double dStart, dEnd;

  //whether we've actually seen front/back of car (useful for updating cars)
  int seenFront, seenBack;

  //whether we actually fit a corner to the data, or added one for our purposes
  int cornerSeen;

  //matrices for KF estimates...

  Matrix Nmu;
  Matrix Nmu_hat;
  Matrix Nsigma;
  Matrix Nsigma_hat;

  Matrix Emu;
  Matrix Emu_hat;
  Matrix Esigma;
  Matrix Esigma_hat;

};


/**
 * If we're tracking objects rather than cars, this struct is how we keep
 * track of all relevant information
 **/
struct objectRepresentation {
  //the center of mass of this object
#warning "these should probably match case w/ carRepresentation..."
  double N,E;

  //histories of the points, ranges and angles for this car
  vector<vector<NEcoord> > scans;
  vector<vector<double> > ranges;
  vector<vector<double> > angles;

  //how many times we've seen it, and when the last one was
  int timesSeen;
  int lastSeen;

  //start and end distances (+ means car is in foreground, 
  //0 means at edge of scan, - means car is in background)
  double dStart, dEnd; 

  //matrices for KF estimates...

  Matrix Nmu;
  Matrix Nmu_hat;
  Matrix Nsigma;
  Matrix Nsigma_hat;

  Matrix Emu;
  Matrix Emu_hat;
  Matrix Esigma;
  Matrix Esigma_hat;
 
};


class trackMO : public CStateClient
{

  int status_flag;

public:
  trackMO(int sn_key, char* scanFile);
  ~trackMO();

  void Active();

  /**
   *  reads scans in from file (specified on command line)
   * and processes them into proper format
   **/
  void ReadFile();

  /**
   *  listens to scans being sent from LADARs, when one is
   * received, it's put into newPoints and proceessNewScan is called
   **/
  void Comm();

  /**sets up log files**/
  void setupLogs();

  /**
   *  sends objects to fusionMapper
   * Currently, checks the velocities and decides which to send
   **/
  void sendObjects();

  /**
   * deals with incoming range data, from file or feeder 
   * (is called when new scan arrives)
   * calls the sequence of fxns that performs operations on the new data
   **/
  void processNewScan();

  /**
   * segments a single scan, using the discontinuity requirement
   * does not yet impose convex requirement...
   **/
  void segmentScan();

  /**
   * given car data (the line segment descriptors), does come calculations 
   * to get cartesian coordinates of corner and the endpoints of the line
   * segments.
   **/
  void finishCar(carRepresentation* newCar);
  void finishPCar(carRepresentation* newCar);

  /** finds perpendicular distance between a line and a point **/
  double perpDist(double x1, double x2, double y1, double y2, double theta, double rho);

  /**finds distance traveled between two 'cars'**/
  double traveledDist(carRepresentation* car1, carRepresentation* car2);

  /**
   * after scan has been segmented, converts eligible segments
   * into cars (the newCars array)
   **/
  void prepCars();

  /**
   * once eligible cars have been created, tries to match them with 
   * previously seen cars, using a function that gives the dist
   * between two cars. This dist function is weak, so trackCArs is too
   **/
  void trackCars();

  /**
   * Another option for representing objects is to keep track of their 
   * 'center of mass'. This turns segmented objects into a point,
   * and keeps track of their corresponding points/ranges/angles
   * (uses the objectRepresentation)
   **/
  void prepObjects();

  /**
   * tracks objects through frames (using their centers)
   **/
  //#warning "this needs to take into account the predicted positions, not the last-seen positions!!"
  void classifyObjects();

  /**
   * uses the most recent measurements (if they exist) to update the KF
   * */
  void KFupdate();

  /**
   * calculates mu_hat and sigma_hat, to be used in matching objects w/ 
   * new scans.
   **/
  void KFpredict();

  /** 
   * Uses probabilistic methods to try to determine if the object
   * is moving. Does this by comparing successive groups of points
   **/
  //#warning "should pass the object itself, not the index"
  void checkMotion();

  /**
   * we have an obj and checkMotion says that its moving...
   * so we need to turn it into a car
   **/
  void obj2car(int index); 

  /**
   * This takes the points and fits a car representation to them, which
   * is then stored in newCar 
   **/
  void points2car(carRepresentation* newCar, vector<NEcoord> points);

  /**
   * This function takes the points stored in the vector points, and uses
   * them to update the representation Car. 
   * Currently, this update is VERY incomplete, simply replacing the car
   * representation stored in Car with the one that comes from processing points.
   *         
   * At some point (soon) need to fix this, to deal with only partial matches,
   * and to actually update the data, rather than replace it 
   * (see Sam's thesis)
   **/
  void updateCar(carRepresentation* Car, objectRepresentation* newObj);

  void updateObject(objectRepresentation* oldObj, objectRepresentation* newObj);
  void addObject(objectRepresentation* newObj);


  /**
   * this function checks each car against every other, removing
   * any potential duplicates.
   *
   * next functionality to be added is checking last-updated time...
   * if we haven't seen an MO for some time, we don't care about it
   * needs to be made fancier, but this is a field-test hack
   **/
  void cleanCars();

  /**
   * 'removes' a car by putting it's index back in the stack
   * also, sends a SNmsg so mapping/prediction knows that this car isn't
   * current anymore.
   **/
  void removeCar(carRepresentation* oldcar, int i);

  //removes object at index
  void removeObject(int index);

  double dist(double x1, double y1, double x2, double y2) {
    return sqrt(pow(x1-x2,2)+pow(y1-y2,2));
  }

  /**
   * takes in a list of points, and fits a rectangle to them
   * fit will be an array, consisting of the fit parameters
   * [alpha, rho, a,b, alpha2, rho2, a2, b2] 
   **/
  //#warning "this should take in vector<NEcoord>, so conversions don't have to happen before"
  int fitCar(double* points, int numPoints, carRepresentation* fit, double vN, double vE);

  /**
   * fits a line (alpha,rho,a,b) to an array of points
   * for now, tries to minimise the total squared error
   **/
  double fitLine(double* points, int numPoints, double* fit);

  /**
   * fits a line (alpha, rho, a, b) to given array of points, given 
   * that it must be perpendicular to the line described by theta1
   **/
  //#warning: "do I want to allow a range around theta1 at some point??"
  double fitLine2(double* points, int numPoints, double* fit, double theta1);

  /**
   * Check which cars are actually moving, and send those to FM
   **/
  void calcVel();

  //if file specified on command line, name is stored here
  char datafile[256];

  unsigned long long m_timeBegin, m_timeEnd, m_timeDiff;

  // the current new points, received from file or ladarFeeder
  NEcoord newPoints[NUMSCANPOINTS];

private:

  int m_send_socket;

  //logging output streams (mostly used for debugging)
  ofstream m_outputPoints;
  ofstream m_outputSegments;
  ofstream m_outputCarsP; //polar representation
  ofstream m_outputCarsC; //cartesian representation
  ofstream m_outputSent;
  ofstream m_outputSentObjs;
  ofstream m_outputTest;
  ofstream m_outputObjs; //outputting the central pts of objects and their KF estimated velocity
  ofstream m_outputSentSegs; //segment start/ends that correspond to sent cars

  int numSegments; //number of objects found by segmentScan
  int numCandidates; //number of objects okayed by prepobjects
  int numCars; //how many objects we've turned into cars; used to index the cars

  int numFrames; //how many frames we've processed

  //first and last index of each object, as found by segmentScan()
  int posSegments[NUMSCANPOINTS][2]; 
  //number of pts in ith object, again as found by segmentScan()
  int sizeSegments[NUMSCANPOINTS]; 
  //ranges and angles corresponding to the points in segmentScan()
  //#warning "check the calculation of these...use m_state + ???"
  double ranges[NUMSCANPOINTS];
  double angles[NUMSCANPOINTS];
  double diffs[NUMSCANPOINTS-1];
  //the stack to keep track of cars
  vector<int> openCars;
  vector<int> usedCars;
  vector<int>::iterator carIterator;
  carRepresentation cars[MAXNUMCARS]; //Trying to define an array of car structs
 
   //the stack to keep track of objects
  vector<int> openObjs;
  vector<int> usedObjs;
  vector<int>::iterator objIterator;
  objectRepresentation objects[MAXNUMCARS];
  objectRepresentation candidates[MAXNUMCARS];
  double occlusions[MAXNUMCARS][2];

  //  Matrix A = new Matrix(2,2);
  Matrix A;//(2,2);
  Matrix C;//(2,1);
  Matrix I;//(2,2);
  Matrix R;//(2,2);
  Matrix Q;//(1,1);

};


#endif
