% takes in a follow log file and plots:
% phi vs time
% y error vs time
% the desired trajectory and the actual trajector

clear all;

% read the follow log
dlmread lateral_zagone_02.log;
logs = ans;
logs2 = logs;
clear logs;

% compute y error
yerr = logs2(:,18) - logs2(:,9);

figure;
plot(logs2(:,2),logs2(:,5));
xlabel('time (sec)');
ylabel('commanded steering angle (rad)');
grid on;
title('Commanded phi (Alice)')

figure;
plot(logs2(:,2),yerr);
xlabel('time (sec)');
ylabel('y error (m)');
grid on;
title('Y error (Alice, "jessica_run1.log")')

if 0

figure;
plot(logs2(:,18),logs2(:,17),logs2(:,9),logs2(:,8));
legend('show');
legend('desired traj','actual traj');
xlabel('Easting (m)');
ylabel('Northing (m)');
grid on;
title('Desired vs actual trajectory (Simulator)')

end