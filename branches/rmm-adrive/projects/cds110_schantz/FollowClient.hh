/*
 * follow.h - header file for follow program
 *
 * RMM, 10 Dec 05
 *
 */

#include "SkynetContainer.h"	// skynet object
#include "StateClient.h"	    // state client object
#include "TrajTalker.h"		    // trajectory receiver
#include "PID_Controller.hh"  // for speed PID control
#include "DGCutils"           // I'm sure it will be useful for something
#include "adrive_skynet_interface_types.h" //For talking to adrive
// #include "traj.h"

#include "CObserver.hh"

#include "falcon/state.h"
#include "falcon/traj.h"

/* Origin for locating ourselves on the planet */
extern double xorigin, yorigin;

/* Declare the global input, output and reference variables */
#define NUMINP 9
#define NUMOUT 2
#define NUMMEAS 3

extern double inp[2*NUMINP], out[NUMOUT], err[NUMINP], meas[NUMMEAS+NUMOUT];
extern double covar[NUMINP], correct[NUMINP];
extern double outGain[NUMOUT], outCtrl[NUMOUT], outFF[NUMOUT], outCmd[NUMOUT], outRef[NUMOUT], outState[NUMOUT];
extern int outOverride[NUMOUT];
#define ref (inp+NUMINP)

extern int NO_COMMANDS;

extern int sn_key;
extern int laneSight;  //the number of points to look ahead when averaging lane sigmas.
extern double controlRate;
extern double currentTime;
extern double actualRate;
extern double threshold;  // how we dicide is a sigma is good or not


extern char ctrlFilename[256];
extern char gainFilename[256];
extern char obsvFilename[256];
extern char trajFilename[256];

extern char statusMessage[80];

extern int disableControl(long arg);

/* Define offsets for states and reference values */
#define XPOS 0
#define YPOS 1
#define TPOS 2
#define XVEL 3
#define YVEL 4
#define TVEL 5
#define XACC 6
#define YACC 7
#define TACC 8
// More stuff for adational lane data
// Will probably end up ignoring the main lane
#define LXPOS 9
#define LYPOS 10
#define LTPOS 11
#define LXVEL 12
#define LYVEL 13 
#define LTVEL 14
#define LSIGMA 15
#define RXPOS 16
#define RYPOS 17
#define RTPOS 18
#define RXVEL 19
#define RYVEL 20 
#define RTVEL 21
#define RSIGMA 22



#define XMEASPOS 0
#define YMEASPOS 1
#define TVMEASPOS 2

#define PHI 0
#define V   1

// Define Constants for Intellegent Lane Follower
#define GOOD 0
#define BAD 1

#define GOOD2GOOD 0
#define GOOD2BAD 1
#define BAD2GOOD 2
#define BAD2BAD 3

#define RIGHT 0
#define LEFT 1
#define BOTHGOOD 2
#define BOTHBAD 3

#define NOCHANGE 0
#define BOTH 1
#define DEFAULTLEFT 2
#define MAINTAINLEFT 3
#define DEFAULTRIGHT 4
#define MAINTAINRIGHT 5


// Main skynet module
class FollowClient: public CStateClient, public CTrajTalker {
 public:
  FollowClient(int sn_key);

	void setupFiles();

  //void ReadState();
//   void ReadTraj();
  void ControlLoop();

	void writeLog();

	bool toggleLogging();

	bool useAutoLogNaming;

	char logFilename[256];
	bool loggingEnabled;

	enum ctrlStatus {
		enabled,
		paused,
		disabled
	};

	ctrlStatus controlEnabled;

	ctrlStatus obsvEnabled;

	bool setControlStatus(FollowClient::ctrlStatus status);

	bool toggleObsvStatus();

 private:
	int closestTrajPoint();
  int closestLeftLanePoint();
  int closestRightLanePoint();
	int project_error(TRAJ_DATA *, double *, double *);

  //Lane following decision functions
  int sigma_property( double sigma);
  int rightvsleft();
  int chooseLaneFollowingBehavior();
  int laneFollowBrain();  //pulls all the decision making together

  //Lane following conditioning functions
  //none right now

	CPID_Controller* m_speedController;

	STATE_SPACE* m_lateralController;

  int m_adriveMsgSocket;

	ofstream logFile;

	unsigned long long timePause;

	unsigned long long timeStart;

	TRAJ_DATA* m_traj_falcon;

// 	int m_currentTrajPoint;

// 	CTraj* m_traj_DGC;

	CObserver* m_observer;

	double gamma;

  int previousLaneCondition;
  int currentLaneCondition;

  double oldsigmaL;
  double oldsigmaR;
  double sigmaL;
  double sigmaR;

  int behaviorFlag;

  double rightMaintainDistance;
  double leftMaintainDistance;
  double rightDefaultDistance;
  double leftDefaultDistance;

  double trajLeftVector[25];
  double trajRightVector[25];

};
