#ifndef INTERFACE_SUPERCON_PLN_HH
#define INTERFACE_SUPERCON_PLN_HH

//FILE DESCRIPTION: Header file that contains the definitions of data structures that are
//used in direct communication between SuperCon and the plannerModule 

#include "../../projects/mlarsson/ccplanner/CCLandMark.h"

namespace sc_interface {

/* Use the scMessage method */
//Used for planner --> superCon (conditional) messages *using the scMessage method/message
//ONLY* - these enums should be used in the msgID field of the scMessage, ALSO
//the msgID field only takes an integer - HENCE you should RECAST one of the
//enums (type: scMsgID_PLN) - to an integer (you should recast whichever member
//of the enum details what you want to do - superCon will then perform the inverse
//recast statement so that the code on either side is cleaner - there are reasons
//why this has to be done)
#define PLN_SC_MSGIDS_LIST(_) \
  _( trajInfo, = 0 )
DEFINE_ENUM( scMsgID_PLN, PLN_SC_MSGIDS_LIST )

#define SC_TRAJ_STATUS_LIST(_) \
  _( pln_output_valid_traj, = 0 ) \
  _( pln_failed_deceleration_constraint, ) \
  _( pln_failed_find_waypoint, ) /*couldn't reach end waypoint*/ \
  _( pln_passed_find_waypoint, ) /*can reach end waypoint*/\
  _( pln_placewaypoint_complete, ) /*waypoint placed. vehicle paused when recieving this message, so validity of traj assumed to avoid strategy errors*/
DEFINE_ENUM( traj_status_type, SC_TRAJ_STATUS_LIST )

#define SC_PLN_ACTION_MSG_LIST(_) \
  _( place_next_waypt, = 0 ) \
  _( find_route, )
DEFINE_ENUM( plnActionMsgType, SC_PLN_ACTION_MSG_LIST )

  struct superCon_wpt_action {
    plnActionMsgType wptAction;
    CWayPoint WayPt;
  } __attribute__((packed)) ;;

}

#endif
