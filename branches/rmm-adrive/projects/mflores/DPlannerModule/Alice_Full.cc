/* 
	Alice_Full.cpp
	Melvin E. Flores
	Control and Dynamical Systems
	California Institute of Technology
	Created on 12-12-2006.
*/ 

#include <float.h>
#include <stdlib.h>
#include <math.h>

#include "OTG_General.h"
#include "Alice_Full.hh" 
#include "DPlannerUtilities.hh"

CAlice_Full::CAlice_Full(void)
{
	m_i = 0;
	m_j = 0;

	m_NofFlatOutputs =  6;
	m_NofParameters  = 16;

	m_Nurbs = new NURBS[m_NofFlatOutputs];

	m_Nurbs[0].NofDimensionsOfFlatOutputs = 2;
	m_Nurbs[0].SolveForControlPoints      = 1;
	m_Nurbs[0].SolveForWeights            = 0;
	m_Nurbs[0].NofPolynomials             = 5;
	m_Nurbs[0].DegreeOfPolynomials        = 3;
	m_Nurbs[0].CurveSmoothness            = 2;
	m_Nurbs[0].MaxDerivatives             = 2;

	m_Nurbs[1].NofDimensionsOfFlatOutputs = 1;
	m_Nurbs[1].SolveForControlPoints      = 1;
	m_Nurbs[1].SolveForWeights            = 0;
	m_Nurbs[1].NofPolynomials             = 5;
	m_Nurbs[1].DegreeOfPolynomials        = 3;
	m_Nurbs[1].CurveSmoothness            = 2;
	m_Nurbs[1].MaxDerivatives             = 2;

	m_Nurbs[2].NofDimensionsOfFlatOutputs = 1;
	m_Nurbs[2].SolveForControlPoints      = 1;
	m_Nurbs[2].SolveForWeights            = 0;
	m_Nurbs[2].NofPolynomials             = 5;
	m_Nurbs[2].DegreeOfPolynomials        = 3;
	m_Nurbs[2].CurveSmoothness            = 2;
	m_Nurbs[2].MaxDerivatives             = 2;

	m_Nurbs[3].NofDimensionsOfFlatOutputs = 1;
	m_Nurbs[3].SolveForControlPoints      = 1;
	m_Nurbs[3].SolveForWeights            = 0;
	m_Nurbs[3].NofPolynomials             = 1;
	m_Nurbs[3].DegreeOfPolynomials        = 0;
	m_Nurbs[3].CurveSmoothness            =-1;
	m_Nurbs[3].MaxDerivatives             = 0;

	m_Nurbs[4].NofDimensionsOfFlatOutputs = 1;
	m_Nurbs[4].SolveForControlPoints      = 1;
	m_Nurbs[4].SolveForWeights            = 0;
	m_Nurbs[4].NofPolynomials             = 5;
	m_Nurbs[4].DegreeOfPolynomials        = 3;
	m_Nurbs[4].CurveSmoothness            = 2;
	m_Nurbs[4].MaxDerivatives             = 2;

	m_Nurbs[5].NofDimensionsOfFlatOutputs = 1;
	m_Nurbs[5].SolveForControlPoints      = 1;
	m_Nurbs[5].SolveForWeights            = 0;
	m_Nurbs[5].NofPolynomials             = 5;
	m_Nurbs[5].DegreeOfPolynomials        = 3;
	m_Nurbs[5].CurveSmoothness            = 2;
	m_Nurbs[5].MaxDerivatives             = 2;
	
	m_NurbsLib.CompleteNURBSInfo(m_NofFlatOutputs, m_Nurbs);

	m_Parameters = new double[m_NofParameters];

	m_Parameters[0]  = 5.43560;		// L
	m_Parameters[1]  = 2.13360;		// W
	m_Parameters[2]  = 1.06680;		// hcg
	m_Parameters[3]  = 0.00000;		// v_min
	m_Parameters[4]  = 1.40800;		// v_max
	m_Parameters[5]  =-3.00000;		// a_min
	m_Parameters[6]  = 0.98100;		// a_max
	m_Parameters[7]  =-0.44942;		// phi_min
	m_Parameters[8]  = 0.44942;		// phi_max
	m_Parameters[9]  =-1.30900;		// phid_min
	m_Parameters[10] = 1.30900;		// phid_max
	m_Parameters[11] = 9.81000;		// g
	m_Parameters[12] = 0.35200;		// vi
	m_Parameters[13] = 0.35200;		// vf
	m_Parameters[14] = 0.00000;		// ai
	m_Parameters[15] = 0.00000;		// af

	m_NofLIC = 6;
	m_NofLFC = 6;

	m_IC = new double[m_NofLIC];
	m_IC[0] = 398903.1;
	m_IC[1] = 3781278;
	m_IC[2] = 0;
	m_IC[3] =-0.22348;
	m_IC[4] = 0;
	m_IC[5] = 0;

	m_FC = new double[m_NofLFC];
	m_FC[0] = 398907.5;
	m_FC[1] = 3781277;
	m_FC[2] = 0;
	m_FC[3] =-0.22348;
	m_FC[4] = 0;
	m_FC[5] = 0;

	m_NofRows    = 10;
	m_NofColumns =  2;

	m_A = new double*[m_NofRows];
	for(m_i = 0; m_i<m_NofRows; m_i++)
	{
		m_A[m_i] = new double[m_NofColumns];
	}
	m_b = new double[m_NofRows];

	m_A[0][0]= -0.6378459469255659E16/0.9007199254740992E16;
	m_A[1][0]= -0.4094181479406059E16/0.1801439850948198E17;
	m_A[2][0]= 0.6155754117666651E16/0.3602879701896397E17;
	m_A[3][0]= 0.3179828874850617E16/0.2251799813685248E16;
	m_A[4][0]= 0.6589740009014847E16/0.1125899906842624E16;
	m_A[5][0]= 0.6378459469255659E16/0.9007199254740992E16;
	m_A[6][0]= 0.4094181479406059E16/0.1801439850948198E17;
	m_A[7][0]= -0.6155754117666651E16/0.3602879701896397E17;
	m_A[8][0]= -0.3179828874850617E16/0.2251799813685248E16;
	m_A[9][0]= -0.6589740009014847E16/0.1125899906842624E16;

	m_A[0][1]= -1.0;
	m_A[1][1]= -1.0;
	m_A[2][1]= -1.0;
	m_A[3][1]= -1.0;
	m_A[4][1]= 1.0;
	m_A[5][1]= 1.0;
	m_A[6][1]= 1.0;
	m_A[7][1]= 1.0;
	m_A[8][1]= 1.0;
	m_A[9][1]= -1.0;

	m_b[0] =-4063758.971;
	m_b[1] =-3871935.2949;
	m_b[2] =-3713118.7757;
	m_b[3] =-3217964.7744;
	m_b[4] =6116042.0684;
	m_b[5] =4063766.6078;
	m_b[6] =3871940.296;
	m_b[7] =3713125.0984;
	m_b[8] =3217979.784;
	m_b[9] =-6115990.5632;

	m_ocpSizes = new OCPSizes;

	m_ocpSizes->NofFlatOutputs            = m_NofFlatOutputs;
	m_ocpSizes->HasICostFunction         = false;
	m_ocpSizes->HasTCostFunction         = true;
	m_ocpSizes->HasFCostFunction         = true;
	m_ocpSizes->NofILinConstraints        = m_NofLIC;
	m_ocpSizes->NofTLinConstraints        = 6 + m_NofRows;
	m_ocpSizes->NofFLinConstraints        = m_NofLFC;
	m_ocpSizes->NofINLinConstraints       = 1;
	m_ocpSizes->NofTNLinConstraints       = 6;
	m_ocpSizes->NofFNLinConstraints       = 1;
	m_ocpSizes->NofStates                 = 4;
	m_ocpSizes->NofInputs                 = 2;
	m_ocpSizes->NofParameters             = m_NofParameters;
	m_ocpSizes->HasVariableParameters     = false;
	m_ocpSizes->NofReferences             = 0;
	m_ocpSizes->HasVariableReferences     = false;
	m_ocpSizes->IntegrationType           = qt_Simpsons38;
	m_ocpSizes->NofCostCollocPoints       = 21;
	m_ocpSizes->NofConstraintCollocPoints = 31;
	m_ocpSizes->HasRecoverSystem          = true;
	m_ocpSizes->HasActiveVariables        = false;

	m_timeInterval.t0 = 0;
	m_timeInterval.tf = 1;
	
	m_NofDerivativesOfStates = new int[m_ocpSizes->NofStates];
	m_NofDerivativesOfStates[0] = 2;
	m_NofDerivativesOfStates[1] = 2;
	m_NofDerivativesOfStates[2] = 2;
	m_NofDerivativesOfStates[3] = 2;
	
	m_NofDerivativesOfInputs = new int[m_ocpSizes->NofInputs];
	m_NofDerivativesOfInputs[0] = 2;
	m_NofDerivativesOfInputs[1] = 2;
}

CAlice_Full::~CAlice_Full(void)
{
	delete m_ocpSizes;
	delete m_Nurbs;
	delete m_Parameters;

	if(m_IC != NULL)
	{
		delete[] m_IC;
	}

	if(m_FC != NULL)
	{
		delete[] m_FC;
	}

	if(m_A != NULL)
	{
		for(m_i=0; m_i<m_NofRows; m_i++)
		{
			delete[] m_A[m_i];
		}
		delete[] m_A;
	}

	if(m_b != NULL)
	{
	   delete[] m_b;	
	}
	
	delete[] m_NofDerivativesOfStates;
	delete[] m_NofDerivativesOfInputs;
}

int CAlice_Full::GetNofConstraintCollocPoints(void)
{
	return m_ocpSizes->NofConstraintCollocPoints;
}

int CAlice_Full::GetNofStates(void)
{
	return m_ocpSizes->NofStates;
}

int CAlice_Full::GetNofInputs(void)
{
	return m_ocpSizes->NofInputs;
}

int CAlice_Full::GetNofParameters(void)
{
	return m_NofParameters;
}


void CAlice_Full::GetTimeInterval(TimeInterval* const timeInterval)
{
	timeInterval->t0 = m_timeInterval.t0;
	timeInterval->tf = m_timeInterval.tf;
}

void CAlice_Full::GetOCPSizes(OCPSizes* const ocpSizes)
{
	ocpSizes->NofFlatOutputs            = m_ocpSizes->NofFlatOutputs;
	ocpSizes->HasICostFunction          = m_ocpSizes->HasICostFunction;
	ocpSizes->HasTCostFunction          = m_ocpSizes->HasTCostFunction;
	ocpSizes->HasFCostFunction          = m_ocpSizes->HasFCostFunction;
	ocpSizes->NofILinConstraints        = m_ocpSizes->NofILinConstraints;
	ocpSizes->NofTLinConstraints        = m_ocpSizes->NofTLinConstraints;
	ocpSizes->NofFLinConstraints        = m_ocpSizes->NofFLinConstraints;
	ocpSizes->NofINLinConstraints       = m_ocpSizes->NofINLinConstraints;
	ocpSizes->NofTNLinConstraints       = m_ocpSizes->NofTNLinConstraints;
	ocpSizes->NofFNLinConstraints       = m_ocpSizes->NofFNLinConstraints;
	ocpSizes->NofStates                 = m_ocpSizes->NofStates;
	ocpSizes->NofInputs                 = m_ocpSizes->NofInputs;
	ocpSizes->NofParameters             = m_ocpSizes->NofParameters;
	ocpSizes->HasVariableParameters     = m_ocpSizes->HasVariableParameters;
	ocpSizes->NofReferences             = m_ocpSizes->NofReferences;
	ocpSizes->HasVariableReferences     = m_ocpSizes->HasVariableReferences;
	ocpSizes->IntegrationType           = m_ocpSizes->IntegrationType;
	ocpSizes->NofCostCollocPoints       = m_ocpSizes->NofCostCollocPoints;
	ocpSizes->NofConstraintCollocPoints = m_ocpSizes->NofConstraintCollocPoints;
	ocpSizes->HasActiveVariables        = m_ocpSizes->HasActiveVariables;
	ocpSizes->HasRecoverSystem          = m_ocpSizes->HasRecoverSystem;

	//cout << "NofTLinConstraints " << m_ocpSizes->NofTLinConstraints << endl;
	//cout.flush();

}

void CAlice_Full::GetNURBSData(const NURBSDataType* const dataType, const int* const NofFlatOutputs, int* const Data)
{
	if(*dataType == dt_NofDimensionsOfFlatOutputs)
	{
		for(m_i=0; m_i<*NofFlatOutputs; m_i++)
		{
			Data[m_i] = m_Nurbs[m_i].NofDimensionsOfFlatOutputs;
		}
	}
	else if(*dataType == dt_SolveForControlPoints)
	{
		for(m_i=0; m_i<*NofFlatOutputs; m_i++)
		{
			Data[m_i] = m_Nurbs[m_i].SolveForControlPoints;
		}
	}
	else if(*dataType == dt_SolveForWeights)
	{
		for(m_i=0; m_i<*NofFlatOutputs; m_i++)
		{
			Data[m_i] = m_Nurbs[m_i].SolveForWeights;
		}
	}
	else if(*dataType == dt_NofPolynomials)
	{
		for(m_i=0; m_i<*NofFlatOutputs; m_i++)
		{
			Data[m_i] = m_Nurbs[m_i].NofPolynomials;
		}
	}
	else if(*dataType == dt_DegreeOfPolynomials)
	{
		for(m_i=0; m_i<*NofFlatOutputs; m_i++)
		{
			Data[m_i] = m_Nurbs[m_i].DegreeOfPolynomials;
		}
	}
	else if(*dataType == dt_CurveSmoothness)
	{
		for(m_i=0; m_i<*NofFlatOutputs; m_i++)
		{
			Data[m_i] = m_Nurbs[m_i].CurveSmoothness;
		}
	}
	else //if (*dataType == dt_MaxDerivatives)
	{
		for(m_i=0; m_i<*NofFlatOutputs; m_i++)
		{
			Data[m_i] = m_Nurbs[m_i].MaxDerivatives;
		}
	}
}

void CAlice_Full::GetBreakPoints(const TimeInterval* const timeInterval, const int* const NofFlatOutputs, const int* const NofBreakPoints, double** const BreakPoints)
{
	for(m_i=0; m_i<*NofFlatOutputs; m_i++)
	{
		CreateTimeVector(timeInterval, &NofBreakPoints[m_i], BreakPoints[m_i]);
	}
}

void CAlice_Full::GetCostCollocPoints(const TimeInterval* const timeInterval, const int* const NofCostCollocPoints, double* const CostCollocPoints)
{
	CreateTimeVector(timeInterval, NofCostCollocPoints, CostCollocPoints);
}

void CAlice_Full::GetConstraintCollocPoints(const TimeInterval* const timeInterval, const int* const NofConstraintCollocPoints, double* const ConstraintCollocPoints)
{
	CreateTimeVector(timeInterval, NofConstraintCollocPoints, ConstraintCollocPoints);
}

void CAlice_Full::GetOCPParameters(const int* const i, const double* const t, const FunctionType* const functionType, const int* const NofParameters, double* const Parameters)
{
	for(m_i=0; m_i<*NofParameters; m_i++)
	{
		Parameters[m_i] = m_Parameters[m_i];
	}
}

void CAlice_Full::GetNofDerivativesOfStates(int* NofDerivativesOfStates)
{
	for(m_i=0; m_i<m_ocpSizes->NofStates; m_i++)
	{
		NofDerivativesOfStates[m_i] = m_NofDerivativesOfStates[m_i];
	}		
}

void CAlice_Full::GetNofDerivativesOfInputs(int* NofDerivativesOfInputs)
{
	for(m_i=0; m_i<m_ocpSizes->NofInputs; m_i++)
	{
		NofDerivativesOfInputs[m_i] = m_NofDerivativesOfInputs[m_i];
	}			
}

void CAlice_Full::GetSystemsInfo(const int* const NofStates, int* const NofDerivativesOfStates, const int* const NofInputs, int* const NofDerivativesOfInputs)
{
	for(m_i=0; m_i< *NofStates; m_i++)
	{
		NofDerivativesOfStates[m_i] = m_NofDerivativesOfStates[m_i];
	}
	
	for(m_i=0; m_i< *NofInputs; m_i++)
	{
		NofDerivativesOfInputs[m_i] = m_NofDerivativesOfInputs[m_i];
	}
}

void CAlice_Full::RecoverSystem(const double* const tau, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, double* const t, const int* const NofStates, double** const x, const int* const NofInputs, double** const u)
{
		*t = *tau*z5;

	{
		double t6 = z5*z5;
		double t9 = 0.3141592653589793E1*0.3141592653589793E1;      

		x1 = z1;      
		x2 = z2;      
		x3 = z3/z5;      
		x4 = 180.0*z4/0.3141592653589793E1;      
		x1d = z1d;      
		x2d = z2d;      
		x3d = z3d/t6;      
		x4d = 32400.0*z4d/t9;      
		x1dd = z1dd;      
		x2dd = z2dd;      
		x3dd = z3dd/t6/z5;      
		x4dd = 5832000.0*z4dd/t9/0.3141592653589793E1;      
	}

	{
		double t1 = z5*z5;
		double t7 = t1*t1;      
		double t10 = 0.3141592653589793E1*0.3141592653589793E1;      

		u1 = z6/t1;      
		u2 = 180.0*z7/0.3141592653589793E1;      
		u1d = z6d/(z5*t1);      
		u2d = 32400.0*z7d/z5;      
		u1dd = z6dd/t7;      
		u2dd = 5832000.0*(z7dd/t1)/t10/0.3141592653589793E1;      
	}

}

void CAlice_Full::TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt)
{
	if(*mode == 0 || *mode == 2)
	{
		double t1 = z6*z6;
		double t2 = z5*z5;      
		double t3 = t2*t2;      
		double t6 = z7*z7;      

		*Ft  = t1/t3+t6;      
	}

	if(*mode == 1 || *mode == 2)
	{

		double t1 = z6*z6;
		double t2 = z5*z5;      
		double t3 = t2*t2;      

		DFt[12] = -4.0*t1/t3/z5;      
		DFt[13] = 2.0*z6/t3;      
		DFt[16] = 2.0*z7;      
	}
}

void CAlice_Full::FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf)
{
	if(*mode == 0 || *mode == 2)
	{
		*Ff = z5;
	}

	if(*mode == 1 || *mode == 2)
	{

		DFf[12] = 1.0;      
	}

}

void CAlice_Full::GetLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A)
{
	if(*ctype == ct_LinInitial)
	{
		A[0][0]= 1.0;
		A[1][3]= 1.0;
		A[2][6]= 1.0;
		A[2][12]= -p13;
		A[3][9]= 1.0;
		A[4][13]= 1.0;
		A[5][16]= 1.0;
	}
	
	else if(*ctype == ct_LinTrajectory)
	{
		A[0][6]= 1.0;
		A[0][12]= -p4;
		A[1][6]= 1.0;
		A[1][12]= -p5;
		A[2][16]= 1.0;
		A[3][12]= -p10;
		A[3][17]= 1.0;
		A[4][12]= -p11;
		A[4][17]= 1.0;
		A[5][12]= 1.0;

		for(m_i=6; m_i<*NofLConstraints; m_i++)
		{
			A[m_i][0] = m_A[m_i-6][0];
			A[m_i][3] = m_A[m_i-6][1];
		}
	}
	
	else /*if(*ctype == ct_LinFinal)*/
	{
		A[0][0]= 1.0;
		A[1][3]= 1.0;
		A[2][6]= 1.0;
		A[2][12]= -p14;
		A[3][9]= 1.0;
		A[4][13]= 1.0;
		A[5][16]= 1.0;
	}
	
}
void CAlice_Full::InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli)
{

	if(*mode == 0 || *mode == 2)
	{
		double t1 = z5*z5;

		Cnli[0]  = z6-p15*t1;      
	}

	if(*mode == 1 || *mode == 2)
	{
		DCnli[0][12] = -2.0*p15*z5;      
		DCnli[0][13] = 1.0;      
	}

	if(*mode == 99)
	{
		 DCnli[0][12] = 1.0; 
		 DCnli[0][13] = 1.0; 
	}

}

void CAlice_Full::TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt)
{

	if(*mode == 0 || *mode == 2)
	{
		double t1 = cos(z4);
		double t4 = sin(z4);      
		double t8 = 1/p1;      
		double t10 = tan(z7);      
		double t13 = z5*z5;      
		double t14 = 1/t13;      
		double t16 = z3*z3;      

		Cnlt[0] = z3*t1-z1d;      
		Cnlt[1] = z3*t4-z2d;      
		Cnlt[2] = z6-z3d;      
		Cnlt[3] = z3*t8*t10-z4d;      
		Cnlt[4] = z6*t14;      
		Cnlt[5] = t16*t14*t10*t8;      
	}

	if(*mode == 1 || *mode == 2)
	{
		double t1 = cos(z4);
		double t2 = sin(z4);      
		double t5 = tan(z7);      
		double t6 = 1/p1;      
		double t7 = t5*t6;      
		double t9 = t5*t5;      
		double t10 = 1.0+t9;      
		double t12 = z5*z5;      
		double t14 = 1/t12/z5;      
		double t17 = 1/t12;      
		double t21 = z3*z3;      

		DCnlt[0][1] = -1.0;      
		DCnlt[0][6] = t1;      
		DCnlt[0][9] = -z3*t2;      
		DCnlt[1][4] = -1.0;      
		DCnlt[1][6] = t2;      
		DCnlt[1][9] = z3*t1;      
		DCnlt[2][7] = -1.0;      
		DCnlt[2][13] = 1.0;      
		DCnlt[3][6] = t7;      
		DCnlt[3][10] = -1.0;      
		DCnlt[3][16] = t10*z3*t6;      
		DCnlt[4][12] = -2.0*z6*t14;      
		DCnlt[4][13] = t17;      
		DCnlt[5][6] = 2.0*z3*t17*t7;      
		DCnlt[5][12] = -2.0*t21*t14*t7;      
		DCnlt[5][16] = t21*t17*t10*t6;      
	}

	if(*mode == 99)
	{
		 DCnlt[0][1] = 1.0; 
		 DCnlt[0][6] = 1.0; 
		 DCnlt[0][9] = 1.0; 
		 DCnlt[1][4] = 1.0; 
		 DCnlt[1][6] = 1.0; 
		 DCnlt[1][9] = 1.0; 
		 DCnlt[2][7] = 1.0; 
		 DCnlt[2][13] = 1.0; 
		 DCnlt[3][6] = 1.0; 
		 DCnlt[3][10] = 1.0; 
		 DCnlt[3][16] = 1.0; 
		 DCnlt[4][12] = 1.0; 
		 DCnlt[4][13] = 1.0; 
		 DCnlt[5][6] = 1.0; 
		 DCnlt[5][12] = 1.0; 
		 DCnlt[5][16] = 1.0; 
	}

}

void CAlice_Full::FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf)
{

	if(*mode == 0 || *mode == 2)
	{
		double t1 = z5*z5;

		Cnlf[0]  = z6-p16*t1;      
	}

	if(*mode == 1 || *mode == 2)
	{
		DCnlf[0][12] = -2.0*p16*z5;      
		DCnlf[0][13] = 1.0;      
	}

	if(*mode == 99)
	{
		 DCnlf[0][12] = 1.0; 
		 DCnlf[0][13] = 1.0; 
	}

}

void CAlice_Full::GetLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const NofConstraints, double* const LUBounds)
{

	if(*ctype == ct_LinInitial)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<m_NofLIC; m_i++)
			{
				LUBounds[m_i] = m_IC[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<m_NofLIC; m_i++)
			{
				LUBounds[m_i] = m_IC[m_i];
			}
		}
	}

	if(*ctype == ct_LinTrajectory)
	{
		if(*ubtype == ubt_LowerBound)
		{
			LUBounds[0] =0;
			LUBounds[1] =-DBL_MAX;
			LUBounds[2] =-0.44942;
			LUBounds[3] =0;
			LUBounds[4] =-DBL_MAX;
			LUBounds[5] =0.01;
                        
			for(m_i=6; m_i< m_NofRows + 6; m_i++)
			{
				LUBounds[m_i] = -DBL_MAX;
			}
		}
		else
		{
			LUBounds[0] =DBL_MAX;
			LUBounds[1] =0;
			LUBounds[2] =0.44942;
			LUBounds[3] =DBL_MAX;
			LUBounds[4] =0;
			LUBounds[5] =100;

			for(m_i=6; m_i< m_NofRows + 6; m_i++)
			{
				LUBounds[m_i] = m_b[m_i-6];
			}
		}
	}

	if(*ctype == ct_LinFinal)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<m_NofLFC; m_i++)
			{
				LUBounds[m_i] = m_FC[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<m_NofLFC; m_i++)
			{
				LUBounds[m_i] = m_FC[m_i];
			}
		}
	}

	if(*ctype == ct_NLinInitial)
	{
		if(*ubtype == ubt_LowerBound)
		{
			LUBounds[0] =0;
		}
		else
		{
			LUBounds[0] =0;
		}
	}

	if(*ctype == ct_NLinTrajectory)
	{
		if(*ubtype == ubt_LowerBound)
		{
			LUBounds[0] =0;
			LUBounds[1] =0;
			LUBounds[2] =0;
			LUBounds[3] =0;
			LUBounds[4] =-3;
			LUBounds[5] =-9.81;
		}
		else
		{
			LUBounds[0] =0;
			LUBounds[1] =0;
			LUBounds[2] =0;
			LUBounds[3] =0;
			LUBounds[4] =0.981;
			LUBounds[5] =9.81;
		}
	}

	if(*ctype == ct_NLinFinal)
	{
		if(*ubtype == ubt_LowerBound)
		{
			LUBounds[0] =0;
		}
		else
		{
			LUBounds[0] =0;
		}
	}
}

int CAlice_Full::GetNofFlatOutputs(void)
{
	return m_NofFlatOutputs;
}

int* CAlice_Full::GetNofWeights(void)
{
	int* NofWeights = NULL;
	if(m_NofFlatOutputs != 0)
	{
	   NofWeights = new int[m_NofFlatOutputs];		
	   for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	   {
		   NofWeights[m_i] = m_Nurbs[m_i].NofWeights;		
	   }
	}
	return NofWeights;
}

int* CAlice_Full::GetNofDimensionsOfFlatOutputs(void)
{
	int* NofDimensionsOfFlatOutputs = NULL;
	if(m_NofFlatOutputs != 0)
	{
	   NofDimensionsOfFlatOutputs = new int[m_NofFlatOutputs];		
	   for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	   {
		   NofDimensionsOfFlatOutputs[m_i] = m_Nurbs[m_i].NofDimensionsOfFlatOutputs;
	   }
	}
	return NofDimensionsOfFlatOutputs;
}

int* CAlice_Full::GetNofControlPoints(void)
{
	int* NofControlPoints = NULL;
	if(m_NofFlatOutputs != 0)
	{
	   NofControlPoints = new int[m_NofFlatOutputs];		
	   for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	   {
		   NofControlPoints[m_i] = m_Nurbs[m_i].NofControlPoints;		
	   }
	}
	return NofControlPoints;
}

void CAlice_Full::GetInitialGuess(int start_waypoint, int stop_waypoint, RDDFVector rddfvector, double** Weights, double*** ControlPoints)
{
	ConstructInitialGuess(m_NofFlatOutputs, m_Nurbs, &m_timeInterval, start_waypoint, stop_waypoint, rddfvector, Weights, ControlPoints);
}

void CAlice_Full::SetLinearIC(double* IC)
{
	for(m_i=0; m_i<m_NofLIC; m_i++)
	{
		m_IC[m_i] = IC[m_i];
	}
}

void CAlice_Full::SetLinearFC(double* FC)
{
	for(m_i=0; m_i<m_NofLFC; m_i++)
	{
		m_FC[m_i] = FC[m_i];
	}
}

void CAlice_Full::SetFeasibleSet(int NofRows, int NofColumns, double** A, double* b)
{
	if(m_NofRows == NofRows && m_NofColumns == NofColumns)
	{
	  for(m_i=0; m_i<m_NofRows; m_i++)
	  {
		for(m_j=0; m_j<m_NofColumns; m_j++)
		{
			m_A[m_i][m_j] = A[m_i][m_j];
		}
		m_b[m_i] = b[m_i];
	  }
	}
	else if (m_NofRows != NofRows && m_NofColumns != NofColumns)
	{
		for(m_i=0; m_i<m_NofRows; m_i++)
		{
			delete[] m_A[m_i];
		}
		delete[] m_A;
		delete[] m_b;

		m_NofRows    = NofRows;
		m_NofColumns = NofColumns;

		m_A = new double*[m_NofRows];
		m_b = new double[m_NofRows];
		for(m_i=0; m_i<m_NofRows; m_i++)
		{
			m_A[m_i] = new double[m_NofColumns];
			for(m_j=0; m_j<m_NofColumns; m_j++)
			{
				m_A[m_i][m_j] = A[m_i][m_j];
			}
			m_b[m_i] = b[m_i];
		}
	}
	else if(m_NofColumns !=NofColumns)
	{
		  m_NofColumns = NofColumns;
		  for(m_i=0; m_i<m_NofRows; m_i++)
		  {
			delete[] m_A[m_i];
			m_A[m_i] = new double[m_NofColumns];
			for(m_j=0; m_j<m_NofColumns; m_j++)
			{
				m_A[m_i][m_j] = A[m_i][m_j];
			}
			m_b[m_i] = b[m_i];
		  }
	}
	else
	{
		for(m_i=0; m_i<m_NofRows; m_i++)
		{
			delete[] m_A[m_i];
		}
		delete[] m_A;
		delete[] m_b;

		m_NofRows = NofRows;

		m_A = new double*[m_NofRows];
		m_b = new double[m_NofRows];
		for(m_i=0; m_i<m_NofRows; m_i++)
		{
			m_A[m_i] = new double[m_NofColumns];
			for(m_j=0; m_j<m_NofColumns; m_j++)
			{
				m_A[m_i][m_j] = A[m_i][m_j];
			}
			m_b[m_i] = b[m_i];
		}
	}
	m_ocpSizes->NofTLinConstraints = 6 + m_NofRows;
	/*
	cout << "NofTLinConstraints " << m_ocpSizes->NofTLinConstraints << endl;
	cout.flush();
	*/
}

int CAlice_Full::GetNofLIC(void)
{
	return m_NofLIC;
}

int CAlice_Full::GetNofLFC(void)
{
	return m_NofLFC;
}

void CAlice_Full::SetParameters(double* Parameters)
{
	for(m_i=0; m_i<m_NofParameters; m_i++)
	{
		m_Parameters[m_i] = Parameters[m_i];
	}
}

