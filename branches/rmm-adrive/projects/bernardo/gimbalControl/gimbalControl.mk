GIMBALCONTROL_PATH = $(DGC)/projects/bernardo/gimbalControl

GIMBALCONTROL_DEPEND_LIBS = \
			$(FRAMESLIB) \
			$(SKYNETLIB) \
			$(TRAJLIB) \
			$(DGCUTILS) \
			$(CMAPLIB) \
			$(CMAPPLIB) \
			$(MODULEHELPERSLIB)

GIMBALCONTROL_DEPEND_SOURCE = $(GIMBACONTROL_PATH)/gimbalControl.hh\
                           $(GIMBALCONTROL_PATH)/gimbalControl.cc\
                           $(GIMBACONTROL_PATH)/gimbalControlTest.cc

GIMBALCONTROL_DEPEND = $(GIMBALCONTROL_DEPEND_LIBS) $(GIMBALCONTROL_DEPEND_SOURCE) 