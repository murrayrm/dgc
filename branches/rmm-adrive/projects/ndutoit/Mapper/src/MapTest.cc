#include "Map.hh"
#include "LogicalRoadObject.hh"

#include <iostream>
#include <vector>
using std::vector;

using namespace std;
using namespace Mapper;

int main()
{
	cout << "Start\n";
	// Create the default map
	Mapper::Map defMap = Mapper::Map();
	cout << "Created map\n";
	Mapper::Segment seg1 = defMap.getSegment(1);
	cout << "Retrieved segment\n";
	Mapper::Lane lane1 = seg1.getLane(1);
	cout << "Retrieved lane\n";
	
	Mapper::Segment seg2 = defMap.getSegment(2);
	cout << "Retrieved segment\n";
	Mapper::Lane lane1_2 = seg2.getLane(2);
	cout << "Retrieved lane\n";
	
	cout << "Lane1: ";
	lane1_2.print(); 
}
