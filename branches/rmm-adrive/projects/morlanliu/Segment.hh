#ifndef SEGMENT_HH_
#define SEGMENT_HH_
#include "Lane.hh"
#include <string>
#include <vector>
using namespace std;

/*! Segment class. The Segment class represents a segment provided by the RNDF
 *  file. Each segment is characterized by its ID, the number of lanes in it,
 *  and optionally a segment name.
 * \brief The segment class represents a segment provided by the RNDF file.
 */
class Segment
{
public:
/*! Each segment is characterized by its ID and the number of lanes in it. */
	Segment(int, int);
  virtual ~Segment();
  
/*! Returns the segment ID of THIS. */
  int getSegmentID();
  
/*! Returns the number of lanes that THIS contains. */
  int getNumOfLanes();

/*! Returns the segment name of THIS. */
  string getSegmentName();
  
/*! Sets the segment name of THIS. */
  void setSegmentName(string);
  
/*! Returns the minimum speed of THIS. */
  int getMinSpeed();
  
/*! Returns the maximum speed of THIS. */
  int getMaxSpeed();
  
/*! Returns the minimum and maximum speed limit of THIS. */
  void setSpeedLimits(int, int);
  
/*! Returns a pointer to a Lane with the lane ID passed in as an argument. */
  Lane* getLane(int);
  
/*! Adds a lane to the array of lanes contained in THIS. */
  void addLane(Lane*);
  
private:
/* Segments are identified using the form 'M'. All segments have a finite 
 * number of lanes in them. */
  int segmentID, numOfLanes;
  
/* Optionally, a segment may also have a segment name or street name. */
  string segmentName;
  
/* Each segment contains an array of lanes of size numOfLanes. */
  vector<Lane*> lanes;
  
/* Each segment has a minimum and maximum speed limit. */
  int minSpeed, maxSpeed;
};

#endif /*SEGMENT_HH_*/
