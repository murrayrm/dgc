#include "Graph.hh"
using namespace std;

Graph::Graph()
{
  vertices.clear();
}

Graph::~Graph()
{
  for(unsigned i = 0; i < vertices.size(); i++)
    delete vertices[i];
}

Vertex* Graph::getVertex(int segmentID, int laneID, int waypointID)
{
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    Vertex* temp = vertices[i];
    
    if(temp->getSegmentID() == segmentID &&
      temp->getLaneID() == laneID &&
      temp->getWaypointID() == waypointID)
      return temp;
  }
  
  return NULL;
}

 vector<Vertex*> Graph::getVertices()
{
  return vertices;
}

bool Graph::addVertex(int segmentID, int laneID, int waypointID)
{
  if(getVertex(segmentID, laneID, waypointID) != NULL)
    return false;
    
  vertices.push_back(new Vertex(segmentID, laneID, waypointID));
  
  return true;
}
  
bool Graph::addEdge(int segmentID1, int laneID1, int waypointID1,
  int segmentID2, int laneID2, int waypointID2)
{
  Vertex* vertex1 = getVertex(segmentID1, laneID1, waypointID1);
  
  if(vertex1 == NULL)
    return false;
    
  Vertex* vertex2 = getVertex(segmentID2, laneID2, waypointID2);
  
  if(vertex2 == NULL)
    return false;
    
  vertex1->addEdge(new Edge(vertex2));

  cout << "Added edge from " << segmentID1 << "." << laneID1 << "." << waypointID1 
    << " to " << segmentID2 << "." << laneID2 << "." << waypointID2 << endl;
          
  return true;
}

void Graph::print()
{
  for(unsigned i = 0; i < vertices.size(); i++)
    vertices[i]->print();
}
