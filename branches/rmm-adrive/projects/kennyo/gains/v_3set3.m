
V_0 = 3;   % speed of the vehicle- meters/sec


% loop gain
K = 0.015;
% frequency at which the derivative term rolls off
T_r = 15;
% derivative gain
T_d = 7;
% integral gain
T_i = 20;

run run_gains

save v_3set3.mat