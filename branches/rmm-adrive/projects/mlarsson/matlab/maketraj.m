% *************************
% maketraj.m
% Creates a file containing the waypoints of test RDDF
% expressed in NE coordinates.
% File name is 'rddfne'.
% *************************

% Parameters:
fname = 'rddfne';
resRows = 0.2000;
resCols = 0.2000;
blNRowMul = 19171625;
blEColMul = 2212600;

% in image (pixel) framework:
rows = [51  251  401  551  701];
cols = [76   76   76  201  376];

% Transform to NE coordinates
N = (rows - 1 + blNRowMul) * resRows;
E = (cols - 1 + blEColMul) * resCols;

% Open file
outid = fopen(fname,'w');

% Write NE coordinates to file
for i=1:length(rows)
    fprintf(outid, '%10f %10f\n', N(i), E(i) );
end

% Close file
fclose(outid);