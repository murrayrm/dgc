#include "movingObstacle.hh"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "DGCutils"
#include <sn_msg.hh>
#include <SkynetContainer.h>


#define MAX_NUM_OBSTACLES  10

class SpoofCom : virtual public CSkynetContainer{

  struct spoofObstStruct {
    NEcoord startPos;
    NEcoord vel;
    NEcoord side1;
    NEcoord side2;
    unsigned long long timestamp;
  } *m_obst;
  int m_numobst;


public:
  int _QUIT;

  SpoofCom(int sn_key);
  ~SpoofCom();
  void run();
  void loadFile(char *fname);

};

