#ifndef __VECTOR_CC__
#define __VECTOR_CC__

#include <iostream>
#include <math.h>
#include <string.h>
#include "Vector.h"
#include "IndexOutOfBounds.h"
#include "UndefinedOperation.h"

using namespace std;

Vector::Vector()
{
  dimension = 1;
  m_vector = new double[dimension];
  m_vector[0] = 0;
}

Vector::Vector(const int d)
{
  if(d < 1)
    {
      throw IndexOutOfBounds("Error: Vectors must have at least one element");
    }
  
  dimension = d;
 
  m_vector = new double[dimension];
  
  for(int i = 0; i < dimension; i++)
    {
      m_vector[i] = 0;
    }
}

Vector::Vector(const Vector& other)
{
  dimension = other.getDimension();
  m_vector = new double[dimension];
  
  for(int i = 1; i <= dimension; i++)
    {
      setElement(i, other.getElement(i));
    }
}

Vector& Vector::operator = (const Vector& b)
{
 
  if(!(&b == this)) //Check for self-assignment
    {
      double* temp = m_vector;
      dimension = b.getDimension();      

      m_vector = new double[dimension];
      
      for(int i = 1; i <= dimension; i++)
	{
	  setElement(i, b.getElement(i));
	}

      delete temp;
    }

  return (*this);
}


int Vector:: getDimension() const
{
  return dimension;
}

double Vector::getElement(const int component) const
{

  if((component < 1) ||  (component > dimension))
  {
      throw IndexOutOfBounds();
  }

  return m_vector[component - 1];
}

void Vector:: setElement(const int component, const double val)
{
if((component < 1) ||  (component > dimension))
  {
      throw IndexOutOfBounds("Vectors must have at least one element");
  }
  m_vector[component - 1] = val;
}

Vector Vector:: operator + (const Vector& b) const
{
  if(getDimension() != b.getDimension())
    {
      throw UndefinedOperation("Vector addition is undefined for vectors of different dimensions.");
    }

  Vector answer(dimension);

  for(int i = 1; i <= dimension; i++)
    {
      answer.setElement(i, getElement(i) + b.getElement(i));
    }

  return answer;
}

Vector Vector:: operator - (const Vector& b) const
{
  if(getDimension() != b.getDimension())
    {
      throw UndefinedOperation("Vector addition is undefined for vectors of different dimensions.");
    }


  Vector answer(dimension);

  for(int i = 1; i <= dimension; i++)
    {
      answer.setElement(i, getElement(i) - b.getElement(i));
    }

  return answer;
}

Vector Vector:: scalar(const double k) const
{
  Vector answer(dimension);

  for(int i = 1; i <= dimension; i++)
    {
      answer.setElement(i, getElement(i)*k);
    }

  return answer;
}

double Vector:: operator * (const Vector& b) const
{
  if(getDimension() != b.getDimension())
    {
      throw UndefinedOperation("Vector dot products are undefined for vectors of different dimensions.");
    }

  double answer = 0;

  for(int i = 1; i <= dimension; i++)
    {
      answer += getElement(i)*b.getElement(i);
    }

  return answer;
}

Vector Vector:: cross(const Vector& b) const
{
  if((dimension != 3) || (b.getDimension() != 3))
    {
      throw UndefinedOperation("Vector cross products are undefined for vectors of dimension != 3.");
    }

  Vector answer(3);

  answer.setElement(1, (*this).getElement(2)*b.getElement(3) - (*this).getElement(3)*b.getElement(2));
  answer.setElement(2, (*this).getElement(3)*b.getElement(1) - (*this).getElement(1)*b.getElement(3));
  answer.setElement(3, (*this).getElement(1)*b.getElement(2) - (*this).getElement(2)*b.getElement(1));

  return answer;
}

Vector Vector:: proj(const Vector& b) const
{
  if(dimension != b.getDimension())
    {
      throw UndefinedOperation("When taking projections, both vectors must be from the same space (have the same dimension");
    }

  double scale = ((*this)*b)/(b*b);

  return b.scalar(scale);
}


double Vector::norm() const
{
  double answer = 0;

  for(int i = 1; i <= dimension; i++)
    {
      answer += getElement(i)*getElement(i);
    }

  answer = sqrt(answer);

  return answer;

}

Vector Vector::unit() const
{
  return (*this).scalar(1/norm());
}

double Vector::angle(const Vector& b) const
{
  double dot = (*this) * b;
  double mag = norm()* b.norm();

  return acos(dot/mag);
}

bool Vector:: operator == (const Vector& b) const
{
  bool temp = true;
  if(dimension != b.getDimension())
    {
      temp = false;
    }

  for(int i = 1; i <= getDimension(); i++)
    {
      if(getElement(i) != b.getElement(i))
	{
	  temp = false;
	}
    }

  return temp;
}

bool Vector:: operator != (const Vector& b) const
{
  return !((*this) == b);
}

bool Vector:: isOrthogonal(const Vector& b) const
{
  return (((*this) * b) == 0);
}

void Vector::print() const
{

  for(int i = 1; i <= dimension; i++)
    {
	  cout<< getElement(i) <<" ";
    }
  
}

Vector::~Vector()
{
  delete [] m_vector;
}

#endif // __VECTOR_CC__
