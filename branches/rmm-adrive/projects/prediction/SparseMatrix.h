#ifndef __SPARSEMATRIX_H__
#define __SPARSEMATRIX_H__
#include "Polynomial.h"
#include "Vector.h"
#include "Matrix.h"
#include "UndefinedOperation.h"
#include <list>
#include <iostream>
#include <fstream>

using namespace std;

/**A data structure to store a column number/element value pair. */
class ColumnElement
{

 protected:
  int columnNumber;
  double value;
  
 public:
  ColumnElement()
    {
      //Default constructor, doesn't actually do anything.
    }

  ColumnElement(const int col, const double val)
    {
      columnNumber = col;
      value = val;
    }

  ColumnElement(const ColumnElement& other)
  {
    columnNumber = other.getCol();
    value = other.getVal();
  }

  void setVal(const double val)
    {
      value = val;
    }

  double getVal() const
    {
      return value;
    }

  int getCol() const
    {
      return columnNumber;
    }

  //Used for determining if this column index is less than the column index of "other".
  bool operator < (const ColumnElement other) const
    {
      return (columnNumber < other.getCol());
    }

  bool operator < (const int col) const
  {
    return (columnNumber < col);
  }

  ~ColumnElement()
    {
      //Nothing to do
    }
};








/**A data structure that represents a row of the matrix. */
class Row
{

 private:
  list<ColumnElement> columns;
  int row;

 public:

  Row(const int r)
    {
      row = r;
    }

  Row(const Row& other)
    {
      columns = other.columns;
      row = other.getRow();
    }

  //Appends a column to the END of the row.  ONLY USE THIS IF YOU ARE SURE THAT THE COLUMN YOU ARE ADDING HAS THE GREATEST COLUMN INDEX OUT OF ALL THE COLUMNS ALREADY IN THIS ROW.
  void appendColumn(const int col, const double val)
    {
      if(val != 0)
	{
	  columns.push_back(ColumnElement(col, val));
	}
    }

  void setValue(const int col, const double val)
    {
      if(columns.empty()) 
	{
	  //We WON'T append if it's zero, as we aren't explicitly representing zeros.
	  if(val != 0)
	    {
	      appendColumn(col, val);
	    }
	  //Else do nothing 
	}
      else
	{
	  //We have at least one list element, so this isn't a null pointer.
	  list<ColumnElement>::iterator iter = columns.begin();

	  int pos = 1;
	  int list_length = columns.size();
	  bool search = true;
	  int colVal;

	  //Note that iter points to an object of type ColumnElement, so *iter is an object of type ColumnElement
	  while(search && (pos <= list_length))
	    {
	      colVal = (*iter).getCol();
	      if(colVal == col)  //We've found the right column number
		{
		  search = false;
		  if(val != 0)
		    {
		      (*iter).setVal(val);
		    
		    }
		  else //val == 0, so we wan't to remove this element.
		    {
		      columns.erase(iter);
		    }
		}
	      else if (colVal > col) //We've just passed the column position that we want to insert into, so we want to insert a value into the column that precedes this one, unless, of course, it's zero.
		{
		  search = false;
		  if(val != 0)
		    {
		      columns.insert(iter, ColumnElement(col, val));
		    }
		  //Else do nothing; we're not going to insert a zero element.
		}
	      else
		{
		  //We haven't found a column value equal to or greater than what we're looking for, so keep looking.
		  iter++;
		  pos++;
		}
	    }

	  if(search && (val != 0)) //The only way we can exit the for loop with search == true is if we've run off the back end of the list, so insert this column at the back of the list, unless it's zero.
	    {
	      appendColumn(col, val);
	    }
	}
    }

  double getValue(const int col) const
    {
      if(columns.empty())
	{
	  return 0;
	}
      else
	{
	  list<ColumnElement>::const_iterator iter = columns.begin();

	  int pos = 1;
	  int list_length = columns.size();
	  bool search = true;
	  int colVal;

	  while(search && (pos <= list_length))
	    {
	      colVal = (*iter).getCol();

	      if(colVal == col) //We've found the right one, so return it's value.
		{
		  return (*iter).getVal();
		}

	      else if(colVal > col)  //We've passed where it should be, so it's not in the list
		{
		  return 0;
		}

	      else //We're not there yet, keep searching.
		{
		  iter++;
		  pos++;
		}
	    }

	  //If we're here, it means that we've run off the edge of the list, so the element we're looking for isn't in the list at all
	  return 0;
	}
    }

  //Returns a const iterator that points to the list of ColumnElements.
  list<ColumnElement>::const_iterator getIterator() const
  {
    return columns.begin();
  }

  Row operator + (const Row& other) const
    {

      if(row != other.getRow())
	{
	  throw UndefinedOperation("Error:  Only rows with the same row index (i.e., corresponding rows) may be added");
	}

      Row answer(row);
      int this_length = columns.size();
      int other_length = other.length();

      int this_pos = 1;
      int other_pos = 1;

      list<ColumnElement>::const_iterator this_list = columns.begin();
      list<ColumnElement>::const_iterator other_list = other.columns.begin();

      while((this_pos <= this_length) && (other_pos <= other_length))  //There are more elements in each list to iterate through.
	{
	  if((*this_list).getCol() == (*other_list).getCol())  //These two column elements have the same column index, so we need to add their sum to the 
	    {
	      answer.appendColumn((*this_list).getCol(), (*this_list).getVal() + (*other_list).getVal());
	      this_pos++;
	      other_pos++;
	      this_list++;
	      other_list++;
	    }
	  else if((*this_list).getCol() < (*other_list).getCol())
	    {
	      answer.appendColumn((*this_list).getCol(), (*this_list).getVal());
	      this_pos++;
	      this_list++;
	    }
	  else
	    {
	      answer.appendColumn((*other_list).getCol(), (*other_list).getVal());
	      other_pos++;
	      other_list++;
	    }
	}

      //If we're down here, one or both of the lists have run out of elements to iterate through, so just run through the elements in the remaining list.
      while(this_pos <= this_length)
	{
	  answer.appendColumn((*this_list).getCol(), (*this_list).getVal());
	  this_pos++;
	  this_list++;
	}

      while(other_pos <= other_length)
	{
	  answer.appendColumn((*other_list).getCol(), (*other_list).getVal());
	  other_pos++;
	  other_list++;
	}

      return answer;
    }

  Row operator - (const Row& other) const
    {

      if(row != other.getRow())
	{
	  throw UndefinedOperation("Error:  Only rows with the same row index (i.e., corresponding rows) may be added");
	}

      Row answer(row);
      int this_length = columns.size();
      int other_length = other.length();

      int this_pos = 1;
      int other_pos = 1;

      list<ColumnElement>::const_iterator this_list = columns.begin();
      list<ColumnElement>::const_iterator other_list = other.columns.begin();

      while((this_pos <= this_length) && (other_pos <= other_length))  //There are more elements in each list to iterate through.
	{
	  if((*this_list).getCol() == (*other_list).getCol())  //These two column elements have the same column index, so we need to add their sum to the 
	    {
	      answer.appendColumn((*this_list).getCol(), (*this_list).getVal() - (*other_list).getVal());
	      this_pos++;
	      other_pos++;
	      this_list++;
	      other_list++;
	    }
	  else if((*this_list).getCol() < (*other_list).getCol())
	    {
	      answer.appendColumn((*this_list).getCol(), (*this_list).getVal());
	      this_pos++;
	      this_list++;
	    }
	  else
	    {
	      answer.appendColumn((*other_list).getCol(), -1*(*other_list).getVal());
	      other_pos++;
	      other_list++;
	    }
	}

      //If we're down here, one or both of the lists have run out of elements to iterate through, so just run through the elements in the remaining list.
      while(this_pos <= this_length)
	{
	  answer.appendColumn((*this_list).getCol(), (*this_list).getVal());
	  this_pos++;
	  this_list++;
	}

      while(other_pos <= other_length)
	{
	  answer.appendColumn((*other_list).getCol(), -1*(*other_list).getVal());
	  other_pos++;
	  other_list++;
	}

      return answer;
    }

  Row scalar(const double k) const
    {
      Row answer(row);
      
      if(k == 0)
	{
	  return answer;
	}
      
      else
	{
	  list<ColumnElement>::const_iterator iter = columns.begin();
	  
	  int len = columns.size();
	  
	  for(int c = 1; c <= len; c++)
	    {
	      answer.appendColumn((*iter).getCol(), ((*iter).getVal())*k);
	      iter++;
	    }
	  
	  return answer;
	}
    }
  
  
  int length() const
    {
      return columns.size();
    }
  
  int getRow() const
    {
      return row;
    }
  
  bool operator < (const Row& other) const
    {
      return (row < other.getRow());
    }

  bool isEmpty()
    {
      return columns.empty();
    }

  bool operator == (const Row& other) const
    {
      if(length() != other.length())
	{
	  return false;
	}
      else
	{
	  list<ColumnElement>::const_iterator this_list = columns.begin();
	  list<ColumnElement>::const_iterator other_list = other.columns.begin();
	  int len = length();
	  int pos = 1;
	  
	  while (pos <= len)
	    {
	      if(((*this_list).getCol() != (*other_list).getCol()) || ((*this_list).getVal() != (*other_list).getVal()))
		{
		  return false;
		}
	      pos++;
	      this_list++;
	      other_list++;
	    }
	  return true;
	} 
    }
  
  bool operator != (const Row& other) const
    {
      return !((*this) == other);
    }
  
  
  
  void print() const
    {
      list<ColumnElement>::const_iterator iter = columns.begin();
      
      int length = columns.size();
      int pos = 1;
      
      while(pos <= length)
	{
	  cout<<"Column = "<<(*iter).getCol()<<", "<<"Value = "<<(*iter).getVal()<<endl;
	  iter++;
	  pos++;
	}
      
      cout<<endl<<endl;
    }
  
  ~Row()
    {
      //Nothing to be done
    }
};









/**A sparse Matrix class. */
/**Author: David Rosen */

class SparseMatrix
{
 
protected:

  int numRows, numCols;
  list<Row> rows;

 public:

  //Constructors and assignment operator


  /**Default constructor; creates a 1 x 1 matrix, initialized to 0. */
  SparseMatrix();

  /**Creates an r x c matrix, initialized to all 0's. */
  SparseMatrix(const int r, const int c);

  /**Creates an order x order matrix, initialized to 0's. */
  SparseMatrix(const int order);

  /**Copy constructor. */
  SparseMatrix(const SparseMatrix& other);

  /**Copy constructor for standard matrices. */
  SparseMatrix(const Matrix& other);

  /**Creates an n x 1 column matrix from v, where v is an n-dimensional vector. */
  SparseMatrix(const Vector& v);

  /**Assignment operator */
  SparseMatrix& operator = (const SparseMatrix& other);

  /**Assignment operator */
  SparseMatrix& operator = (const Matrix& other);


  //Accessor methods


  /**Returns the number of rows in this matrix. */
  int getRows() const;

  /**Returns the number of columns in this matrix. */
  int getColumns() const;

  /**Returns the order of (*this) (note that the order is only defined for square matrices, so this method requires that (*this) be square). */
  int getOrder() const;

  /**Returns the element in the matrix in row "row" and column "col". */
  double getElement(const int row, const int col) const;


  //Modifiers and mathematical operators


  /**Sets the element in row "row" and column "col" to the value "val". */
  void setElement(const int row, const int col, const double val);

  /**Appends an element to the matrix in row "row" and column "col".  NOTE: "row" must be >= the row index for all other elements currently entered into the matrix, and "col" must be > the column index for all other elements in row "row" for this method to function properly.  This is useful for initializing the values in a matrix in a for loop. */
  void appendElement(const int row, const int column, const double val);

  /**Appends a row to this matrix.  NOTE:  The row number of "other" must be > the row numbers of all other rows currently entered into the matrix. */
  void appendRow(const Row& other);

  /**Standard matrix addition. */
  SparseMatrix operator + (const SparseMatrix& other) const;

  /**Standard matrix addition. */
  Matrix operator + (const Matrix& other) const;

  /**Standard matrix addition, returns A + B. */
  friend Matrix operator + (const Matrix& A, const SparseMatrix& B);

  /**Standard matrix subtraction, returns (*this) - b. */
  SparseMatrix operator - (const SparseMatrix& b) const;

  /**Standard matrix subtraction, returns (*this) - b. */
  Matrix operator - (const Matrix& b) const;

  /**Standard matrix subtraction, returns A - B. */
  friend Matrix operator - (const Matrix& A, const SparseMatrix& B);

  /**Scalar multiplication; multiplies every element in the matrix by the constant k. */
  SparseMatrix scalar(const double k) const;

  /**Matrix multiplication, returns (*this) * b. */
  SparseMatrix operator * (const SparseMatrix& b) const;

  /**Matrix multiplication, returns (*this) * b. */
  Matrix operator * (const Matrix& b) const;

  /**Matrix multiplication, returns A * B. */
  friend Matrix operator * (const Matrix& A, const SparseMatrix& B);

  /**Forms the matrix product (*this) * v, where v is treated as a column matrix.  Useful if you want to treat the matrix as representing a linear transformation on a vector, and you want to be able to use methods from the Vector class on a result.  If you want to treat the result as a SparseMatrix, then convert v to a SparseMatrix by using the SparseMatrix (Vector& ) copy constructor first. */
  Vector operator * (const Vector& v) const;

  /**Returns the determinant of (*this), requires a square matrix. */
  double determinant() const;

  /**Returns the submatrix of the given matrix formed by removing row "row" and column "col". */
  SparseMatrix submatrix(const int row, const int col) const;

  /**Returns the cofactor of the given element. */
  double cofactor(const int row, const int col) const;

  /**Returns the transpose of (*this). */
  SparseMatrix transpose() const;

  /**Requires that the matrix be square. */
  SparseMatrix inverse() const;


  //Logical operators and tests


  /**Equality operator. */
  bool operator == (const SparseMatrix& b) const;

  /**Inequality operator. */
  bool operator != (const SparseMatrix& b) const;

  /**Returns true if (*this) is square. */
  bool isSquare() const;

  /**Returns true if (*this) is an orthogonal matrix (an orthogonal matrix is a real-valued unitary matrix). */
  bool isOrthogonal() const;  

  /**Returns true if (*this) is a normal matrix (a normal matrix is a real-valued Hermitian matrix). */
  bool isNormal() const;  

  /**Returns true if (*this) is symmetric. */
  bool isSymmetric() const;

  /**Returns true if (*this) is skew-symmetric. */
  bool isSkewSymmetric() const;

  /**Returns true if (*this) is the inverse of b. */
  bool isInverse(const SparseMatrix& b) const;


  //I/O methods


  /**Prints out the matrix as an array of its elements. */
  void print() const;

  /**Writes the matrix into the given file in the following format:
     rows columns

     (array of elements follows, printed in the same format as in the print() method).

  **/
  void write(ofstream& outfile) const;

  /**Reads in data written in the format from above, and returns a new matrix constructed from that data. */
  static SparseMatrix read(ifstream& infile);

  /**Prints out only the elements of the matrix that are represented internally (i.e., elements that have had memory allocated for their representation internally).  Note that there should be NO NONZERO elements that are printed out from this method.  Prints the matrix out in a list format. */
  void printMemory() const;

  /**Standard destructor. */
  ~SparseMatrix();
};


#endif //__SPARSEMATRIX_H__
