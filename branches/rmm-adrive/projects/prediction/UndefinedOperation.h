#ifndef __UNDEFINEDOPERATION_H__
#define __UNDEFINEDOPERATION_H__


class UndefinedOperation
{

protected:

  char* error;
  
public:
  
  UndefinedOperation(char* message = "Error: This operation is undefined.")
  {
    error =  message;
  }

  ~UndefinedOperation()
  {
    delete [] error;
  }
};

#endif //__UNDEFINEDOPERATION_H__
