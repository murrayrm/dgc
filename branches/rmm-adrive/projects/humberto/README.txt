!!! PLEASE READ WHOLE DOCUMENTATION BEFORE YOU START !!!

------------------------------------------------------------------------------
 Installation:
------------------------------------------------------------------------------
- See INSTALL.txt

------------------------------------------------------------------------------
 Reading Material:
------------------------------------------------------------------------------
- Road Finding wiki: http://gc.caltech.edu/wiki/index.php/Road_Finding
- Surf 06 walkthroughs: http://gc.caltech.edu/wiki/index.php/Road_Finding
- OpenCV reference:
	- cvCore http://opencvlibrary.sourceforge.net/CxCore
	- cvReference http://opencvlibrary.sourceforge.net/CxReference
	- cvAux http://opencvlibrary.sourceforge.net/CxAux
	- HighGui http://opencvlibrary.sourceforge.net/HighGui

-------------------------------------------------------------------------------
 ROADFINDING: ./roadFindingMain
-------------------------------------------------------------------------------
This main file is the road finding module itself. It is responsible for setting
up and running a 'process'. That 'process' is an object that belongs to the 
roadFinding Class. After the 'process' object is created it is initialized with
a calibration file with process.loadCalibration(). Then the stereo image objects 
inside the 'process' are loaded with process.initStereo(); 

The application then cycles through the frames by calling the function 
process.refreshStereo().

To see more about roadFindingMain's API and documentation, check 
http://gc.caltech.edu/doc/dgc-api/html/roadFindingMain_8cc.html

-------------------------------------------------------------------------------
 ROADFINDING CALIBRATION: ./roadFindingCalibration
-------------------------------------------------------------------------------
