#ifndef LOCALMAPTALKER_HH_
#define LOCALMAPTALKER_HH_

#include "pthread.h"
#include <iostream>
#include <unistd.h>
#include "SkynetContainer.h"
#include "DGCutils"
#include "Map.hh"
#include "RoadObject.hh"
#include "LogicalRoadObject.hh"

#define LOCALMAP_MAX_BUFFER_SIZE 50000

class CLocalMapTalker : virtual public CSkynetContainer {
	
public:
	CLocalMapTalker(modulename, int, bool);
	CLocalMapTalker();
	~CLocalMapTalker();

	bool SendLocalMap(Mapper::Map* localMap);
	bool RecvLocalMap(Mapper::Map* localMap, int* pSize);
	bool RecvLocalMap(int localMapSocket, Mapper::Map* localMap, int* pSize);
private:
	int localMapSocket;
	pthread_mutex_t m_localMapDataBufferMutex;
	char* m_pLocalMapDataBuffer;
	
};

#endif //  LOCALMAPTALKER_HH
