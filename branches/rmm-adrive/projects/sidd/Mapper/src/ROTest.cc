#include "RoadObject.hh"
#include <iostream>
using namespace std;
using namespace Mapper;
/* Test Protocol */
// Create a StopLine
// Create a Checkpoint
// Create a LaneBoundary
// Print out the information associated with each of the objects.

int main()
{
	// Create a StopLine
StopLine sl0("Stop Line 0", 30.0, 12.5);
// Create a Checkpoint
Checkpoint cp0("Checkpoint 0", 20.0, 12.5);
// Create a LaneBoundary
// TODO these numbers need to change into enums, SOON!
LaneBoundary lb0("Lane Boundary 0", 10.0, 10.0, LaneBoundary::DOUBLE_YELLOW);
lb0.addLocPoint(15.0, 10.0);
lb0.addLocPoint(20.0, 10.0);
lb0.addLocPoint(25.0, 10.0);
lb0.addLocPoint(30.0, 10.0);
LaneBoundary lb1("Lane Boundary 1", 10.0, 15.0, LaneBoundary::DOUBLE_YELLOW);
lb1.addLocPoint(15.0, 15.0);
lb1.addLocPoint(20.0, 15.0);
lb1.addLocPoint(25.0, 15.0);
lb1.addLocPoint(30.0, 15.0);
// Print out the information associated with each of the objects.
cout << "Just a collection of objects, for now.\n";
sl0.print();
cp0.print();
lb0.print();
lb1.print();

}
