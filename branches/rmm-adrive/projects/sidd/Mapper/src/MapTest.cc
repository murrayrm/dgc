#include "Map.hh"
#include "LogicalRoadObject.hh"
#include "RNDF.hh"

#include <iostream>
#include <vector>
using std::vector;

using namespace std;
using namespace Mapper;

int main()
{
  cout << "Start.\n";
  // Create the default map
  /*	Mapper::Map defMap = Mapper::Map();
	cout << "Created map.\n";
	cout << "Trying to retrieve segment 1.\n";
	Mapper::Segment seg1 = defMap.getSegment(1);
	cout << "Retrieved segment\n";
	cout << "Trying to retrieve lane 2.\n";
	Mapper::Lane lane1 = seg1.getLane(1);
	cout << "Retrieved lane\n";
	
	Mapper::Segment seg2 = defMap.getSegment(2);
	cout << "Retrieved segment\n";
	Mapper::Lane lane1_2 = seg2.getLane(1);
	cout << "Retrieved lane\n";
	
	cout << "Lane1: ";
	lane1_2.print(); */
  
  cout << "RNDF testing.\n";
  RNDF *testRNDF_p = new RNDF();
  testRNDF_p->loadFile("DARPA_RNDF_Default_MS.txt");
  cout << "Attempt to print an RNDF. \n";
  testRNDF_p->print();
  Mapper::Map rndfMap = Mapper::Map(testRNDF_p);
  rndfMap.print();

  testRNDF_p = new RNDF();
  testRNDF_p->loadFile("ST_LUKE_RNDF.txt");
  cout << "Attempt to print an RNDF. \n";
  testRNDF_p->print();
  rndfMap = Mapper::Map(testRNDF_p);
  rndfMap.print();
}
