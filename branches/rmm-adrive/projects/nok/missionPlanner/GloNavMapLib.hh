/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#ifndef GLONAVMAPLIB_HH
#define GLONAVMAPLIB_HH

#include <pthread.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <string>
#include "RNDF.hh"
#include "GloNavMapTalker.hh"
#include "DGCutils"


/**
 * GloNavMapLib class.
 */
class CGloNavMapLib : public CGloNavMapTalker
{
  RNDF* m_rndf;
  int m_rndfRevNum;                // RNDF revision#

  // The mutex to protect the RNDF we're planning on
  pthread_mutex_t m_GloNavMapMutex;

public:
  /*! Contstructor */
  CGloNavMapLib(int skynetKey, char* RNDFFileName);
  /*! Standard destructor */
  ~CGloNavMapLib();

  /*! This function is used to listen to the request and send the Global Navigation Map to the mission planner */
  void sendGlobalGloNavMapThread();

  /*! This function is used to listen to the request and send the Global Navigation Map to the traffic planner */
  void sendLocalGloNavMapThread();

  /*! This function is used to get the updated Global Navigation Map from the mission planner */
  void getGlobalGloNavMapThread();  
};

#endif  // GLONAVMAPLIB_HH
