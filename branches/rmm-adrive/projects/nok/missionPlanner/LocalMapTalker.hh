/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */


#ifndef LOCALMAPTALKER_HH
#define LOCALMAPTALKER_HH

#include "pthread.h"
#include <iostream>
#include <unistd.h>
#include "SkynetContainer.h"
#include "DGCutils"
#include "../../sidd/Mapper/include/Map.hh"

#define LOCALMAP_MAX_BUFFER_SIZE 50000

class CLocalMapTalker : virtual public CSkynetContainer {
  pthread_mutex_t m_localMapDataBufferMutex;
  char* m_pLocalMapDataBuffer;

public:
  CLocalMapTalker();
  ~CLocalMapTalker();

  bool SendLocalMap(int localMapSocket, Mapper::Map* localMap);
  bool RecvLocalMap(int localMapSocket, Mapper::Map* localMap, int* pSize);
};

#endif //  LOCALMAPTALKER_HH
