#ifndef STRATEGY_ENDOFMISSION_HH
#define STRATEGY_ENDOFMISSION_HH

//SUPERCON STRATEGY TITLE: END OF MISSION Strategy
//HEADER FILE (.hh)
//DESCRIPTION: Strategy to handle the END OF Mission condition
//under these conditions Alice is paused, and a VERY long time-out
//is initiated (at this point we are waiting for DARPA to pause/
//disable us or to get the new mission and restart)

#include "Strategy.hh"
#include "sc_specs.h"
#include "stage_iterator.hh"

namespace s_endofmission {

#define ENDOFMISSION_STAGE_LIST(_) \
  _(estop_pause, = 1) /*first entry must = 1 (as this is referenced against NEXTstage */ \
  _(wait_for_new_mission, )
DEFINE_ENUM(endofmission_stage_names, ENDOFMISSION_STAGE_LIST)

}

using namespace std;
using namespace s_endofmission;

class CStrategyEndOfMission : public CStrategy
{

/** METHODS **/
public:

  CStrategyEndOfMission() : CStrategy() {}  
  ~CStrategyEndOfMission() {}


  /** MUTATORS **/
  void stepForward(const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface);
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

};

#endif
