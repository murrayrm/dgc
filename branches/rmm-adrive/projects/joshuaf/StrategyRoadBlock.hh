#ifndef STRATEGY_ROADBLOCK_HH
#define STRATEGY_ROADBLOCK_HH

//SUPERCON STRATEGY TITLE: Road Block - SOMETIME STRATEGY
//HEADER FILE (.hh)
//DESCRIPTION: 
//It has been determined that Alice has run into a road block. She will update
//the Route map, telling it that the current segment is blocked. She will then
//perform a U-Turn, then return to nominal operation.

#include "Strategy.hh"
#include "sc_specs.h"

namespace s_roadblock {

#define ROADBLOCK_STAGE_LIST(_) \
  _(place_obstacle, = 1) /*first entry must = 1 (as this is referenced against NEXTstage */ \
  _(make_uturn, ) 
DEFINE_ENUM(roadblock_stage_names, ROADBLOCK_STAGE_LIST)

}

using namespace std;
using namespace s_roadblock;

class CStrategyRoadBlock : public CStrategy
{

public:

  /** CONSTRUCTORS **/
  CStrategyRoadBlock() : CStrategy() {}  

  /** DESTRUCTOR **/
  ~CStrategyRoadBlock() {}


  /** MUTATORS **/

  void stepForward(const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface);
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

};

#endif
