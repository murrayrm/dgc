#include "State.hh"
#include <cmath>
#include <cassert>

// Constructor
State::State(int id, NEcoord position, NEcoord velocity)
{
  vehicleID = id;
  // we will only set this once prediction gives information
  mode = 0; 
  
  // TODO: check to see that our position is within the correct segment
  pos = position;
  
  vel = velocity;

}


State::~State()
{
  /* currLane and destLane should be deleted in the RNDF destructor, so
     we don't have to worry about them here. */
}

// returns true if the vehicle is stopped
bool State::stopped() const
{
  if (fabs(vel.N) < STOPPED_VELOCITY && fabs(vel.E) < STOPPED_VELOCITY)
    return true;
  return false;
}

// Determines if the vehicle is queued at (or about to be queued at) 
// an intersection
bool State::inQueue(NEcoord intersection) const
{

  // Two things that will tell us if car is at intersection:
  //   proximity to stop line (or other queued vehicles)
  //   speed

  if (vel.norm()>QUEUEING_VELOCITY)
    return false;

  if (fabs(pos.N - intersection.N) > 6  &&
      fabs(pos.E - intersection.E) > 6)
    return false;
  
  // TODO: figure out a good way of checking if the other cars
  // around this one are queued as well
  
  return true;
}

bool State::inRightLane(Lane* firstRightLane) const
{
  if (currLane->getLaneID() >= firstRightLane->getLaneID())
    return true;
  return false;
}

void State::setMode(const int m) 
{ 
  mode = m; 
}

void State::setPos(const NEcoord newPos) 
{
  // TODO: check to see if we're still in the same lane
  pos = newPos;
}

void State::setPos(const double n, const double e) 
{ 
  // TODO: check to see if we're still in the same lane
  pos.setvals(n,e);
}

void State::setVel(const NEcoord newVel) 
{
  vel = newVel;
}

void State::setVel(const double n, const double e) 
{ 
  vel.setvals(n,e);
}

void State::setCurrLane(Lane* lane) 
{
  // TODO: assert that the new lane makes sense 
  currLane = lane; 
}

void State::setDestLane(Lane* lane) 
{
  // TODO: assert that the new lane makes sense
  destLane = lane; 
}
  
