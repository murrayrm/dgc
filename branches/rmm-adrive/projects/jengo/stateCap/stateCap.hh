#include <fstream>
#include "StateClient.h"

/*! \brief Takes vehicle state information from astate and stores it in a log
 * file. 
 * 
 * Currently takes in the current time, vehicle position, velocity, and
 * acceleration. Easily modified to take in any other state parameters.
 *
 * The log file is comma-delimited, with the following format:
 *
 * [time, tenths of a second] [Northing position, meters] [Easting position, m]
 * [velocity, m/s] [acceleration, m/s^2]
 *
 * It can be read into matlab with the dlmread command.
 */
class stateCap : public CStateClient {

private:

  /*! Stream to write to a log file */
  ofstream m_output;

public:

  /*! The states to be logged. */
  NEcoord position, velocity, acceleration;
  /*! The current time. */
  double current_time;

  /*! Constructor. Takes in a skynet key and creates a log file with the format
   * state_log_YYYYMMDD_HHMMSS */
  stateCap(int skynetKey);
  /*! Logs astate data until program is killed. */
  void getData();

};
