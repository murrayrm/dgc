This is for functions that I've replaced, but wanted to keep
the old version in case I ever needed it. -Jessica


/*
tplanner: navIntersection

This was my initial approach to navigating an intersection. 
It is like a "bird's-eye-view" approach, where Alice tries to keep 
track of all the cars at the interseciton, even if they are not at 
the first spot in their queue.

I wanted to do this because then we could tell if there was a fault
situation, ie, someone isn't obeying the traffic rules. But we don't 
really care about that unless a) no one's moving and it's a traffic jam
or b) we're in the intersection and they're going to hit us--neither
of which require Alice to have such a broad scope.
*/

int navIntersection(const Environment &env) {

  bool ignore1, ignore2,ignore3;
  ignore1 = ignore2 = ignore3 = false;
  // queuePos is Alice's queue position
  // The sizes give the number of cars in each intersection position
  int size0, size1, size2, size3, queuePos;
  size0 = size1 = size2 = size3 = queuePos = 0;
  // queue<int> queue0, queue1, queue2, queue3;

  cout << "Status: navigating intersection.  ";
  cin >> input; if (input=='q') exit(0);
  
  // build the intersection
  cout<<"    |"<<endl;
  cout<<"    2"<<endl;
  cout<<"    |"<<endl;
  cout<<"--3-+-1--"<<endl;
  cout<<"    |"<<endl;
  cout<<"    0"<<endl;
  cout<<"    |"<<endl;
  
  cout<<"We can ignore a segment if:"<<endl;
  cout<<" There is no road there"<<endl;
  cout<<" The road is one-way leading away from intersection"<<endl;
  cout<<" There is not a stop sign there"<<endl;

  cout<<"Ignore 1? (y/n)  ";
  cin>>input;
  if (input=='y') ignore1 = true;

  cout<<"Ignore 2? (y/n)  ";
  cin>>input;
  if (input=='y') ignore2 = true;
  
  cout<<"Ignore 3? (y/n)  ";
  cin>>input;
  if (input=='y') ignore3 = true; 
  
  cout<<endl;
  // fill in the queues
  cout<<"Num cars at 0? (not including alice)  ";
  cin>>size0;
  queuePos+=size0;

  if(!ignore1) {
    cout<<"Num cars at 1?  ";
    cin>>size1;
    // need to determine how many of these cars are in front of alice
    if (size1<=size0)
      queuePos+=size1;
    else
      queuePos+=size0;
  }

  if(!ignore2) {
    cout<<"Num cars at 2?  ";
    cin>>size2;
    if (size2<=size0)
      queuePos+=size2;
    else
      queuePos+=size0;
  }

  if(!ignore3) {
    cout<<"Num cars at 3?  ";
    cin>>size3;
    if (size3<=size0)
      queuePos+=size3;
    else
      queuePos+=size0;
  }

  if (size0==0) {
    if (size1!=0)
      queuePos++;
    if (size2!=0)
      queuePos++;
    if (size3!=0)
      queuePos++;
  }

  /* don't need to know the order of cars in other queues,
     just how many of them are in front of alice

     int i = 1;
     while(i<=totalCars) {
     
     if (queue0.size()<=size0) {
     queue0.push(i);
     cout<<"0: "<<i<<endl;
     i++;
    }

    if (!ignore1 && queue1.size() <= size1) {
      queue1.push(i);
      cout<<"1: "<<i<<endl;
      i++;
    }

    if (!ignore2 && queue2.size() <= size2) {
      queue2.push(i);
      cout<<"2: "<<endl;
      i++;
    }
    
    if (!ignore3 && queue3.size() <= size3) {
      queue3.push(i);
      cout<<"3: "<<endl;
      i++;
    }

  } //done filling initial spots

  */


  cout<<"Intersection filled."<<endl;

  //start going down the queues
  while (queuePos!=0) {
    
    for (int j=0; j<size2; j++)
      cout<<"    O"<<endl;
    for (int j=0; j<size3; j++)
      cout<<"O";
    cout<<"  +   ";
    for (int j=0; j<size1; j++)
      cout<<"O";
    cout<<endl;
    for (int j=0; j<size0; j++)
      cout<<"    O"<<endl;
    cout<<"    A"<<endl;
    cout<<endl<<"queue position:"<<queuePos+1<<"th"<<endl;

    cout<<endl<<"Continue? (y/q)  ";
    cin>>input; if (input=='q') exit(0);
   
    if (size0!=0) {
      size0--;
      queuePos--;
    }
    
    if (size0==0)
      break; // need to look @ cars one at a time now

    if (!ignore1 && size1!=0) {
      size1--;
      queuePos--;
    }

    if (!ignore2 && size2!=0) {
      size2--;
      queuePos--;
    }

    if (!ignore3 && size3!=0) {
      size3--;
      queuePos--;
    }
    
  }

  for (int j=0; j<size2; j++)
    cout<<"    O"<<endl;
  for (int j=0; j<size3; j++)
    cout<<"O";
  cout<<"  +   ";
  for (int j=0; j<size1; j++)
    cout<<"O";
  cout<<endl;
  for (int j=0; j<size0; j++)
    cout<<"    O"<<endl;
  cout<<"    A"<<endl;
  cout<<endl<<"queue position:"<<queuePos+1<<endl;
  cout<<endl<<"Continue? (y/q)  ";
  cin>>input; if (input=='q') exit(0);

  if (!ignore1 && size1!=0) {
    size1--;
    queuePos--;

    for (int j=0; j<size2; j++)
      cout<<"    O"<<endl;
    for (int j=0; j<size3; j++)
      cout<<"O";
    cout<<"  +   ";
    for (int j=0; j<size1; j++)
      cout<<"O";
    cout<<endl;
    for (int j=0; j<size0; j++)
      cout<<"    O"<<endl;
    cout<<"    A"<<endl;
    cout<<endl<<"queue position:"<<queuePos+1<<endl;
    cout<<endl<<"Continue? (y/q)  ";
    cin>>input; if (input=='q') exit(0);
  }

  if (!ignore2 && size2!=0) {
    size2--;
    queuePos--;

    for (int j=0; j<size2; j++)
      cout<<"    O"<<endl;
    for (int j=0; j<size3; j++)
      cout<<"O";
    cout<<"  +   ";
    for (int j=0; j<size1; j++)
      cout<<"O";
    cout<<endl;
    for (int j=0; j<size0; j++)
      cout<<"    O"<<endl;
    cout<<"    A"<<endl;
    cout<<endl<<"queue position:"<<queuePos+1<<endl;
    cout<<endl<<"Continue? (y/q)  ";
    cin>>input; if (input=='q') exit(0);
  }

  if (!ignore3 && size3!=0) {
    size3--;
    queuePos--;

    for (int j=0; j<size2; j++)
      cout<<"    O"<<endl;
    for (int j=0; j<size3; j++)
      cout<<"O";
    cout<<"  +   ";
    for (int j=0; j<size1; j++)
      cout<<"O";
    cout<<endl;
    for (int j=0; j<size0; j++)
      cout<<"    O"<<endl;
    cout<<"    A"<<endl;
    cout<<endl<<"queue position:"<<queuePos+1<<endl;
    cout<<endl<<"Continue? (y/q)  ";
    cin>>input; if (input=='q') exit(0);
  }

  cout<<"leaving intersection"<<endl;
 
  return 1;

}

/* end old navIntersection */





  /*
  //////////////////////
  // Bounding Diamond //
  //////////////////////

  cout<<"  Bounding Diamond test..."<<endl;
  numMatches = 0;

  for (int i=0; i<numLanes; i++) {

    // don't check lanes that didn't pass box test
    if (isInLane[i]) {
   
      vector<NEcoord> temp = laneBoundaries[i];
      
      double pmax, pmin, qmax, qmin;
      pmax = pmin = temp.at(0).N + temp.at(0).E;
      qmax = qmin = temp.at(0).E - temp.at(0).N;

      for (vector<NEcoord>::iterator it = temp.begin();
	   it != temp.end(); it++) {
	if ( (it->E + it->N) > pmax)
	  pmax = it->E + it->N;
	if ( (it->E + it->N) < pmin)
	  pmin = it->E + it->N;
	if ( (it->E - it->N) > qmax)
	  qmax = it->E - it->N;
	if ( (it->E - it->N) < qmin)
	  qmin = it->E - it->N;
      } 

      // check if point is inside box
      double p = pos.N + pos.E;
      double q = pos.E - pos.N;

      if (p >= pmin &&
	  p <= pmax &&
	  q >= qmin &&
	  q <= qmax) {
	cout<<"  Match for lane "<<i+1<<".";
	isInLane[i] = 1;
	numMatches++;
      }
      else
	isInLane[i] = 0;
    }
  }
  cout<<endl;

  // check if we've found a match 
  if (numMatches == 1) {
    cout<<"  Placing vehicle in lane ";
    for (int i=0; i<numLanes; i++) {
      if (isInLane[i]) {
	cout<<i+1<<endl;
	state.setCurrLane(currSegment->getLane(i+1));
	return;
      }
    }
  }
*/