//INCOMPLETE !!! - SEE BUG 2387

//SUPERCON STRATEGY TITLE: GPS re-acquisition - ANYTIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyGPSreAcq.hh"
#include "StrategyHelpers.hh"

void CStrategyGPSreAcq::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE

  switch( nextStage ) {

  case 1: //astate has requested superCon ready the system for re-inclusion of the
          //re-acquired GPS signal - hence superCon e-stop pause Alice
    {
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL
       * STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */

      
      // !!! CANNOT CHECK ALL trans/term as this includes checking the trans_* method for THIS strategy!!
      
	
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - Stage 1 Completed");
	nextStage++;
      }
    }
    break;
    

    //INCOMPLETE !!! - SEE BUG 2387


    /* END OF STRATEGY */

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyGPSreAcq::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyGPSreAcq::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: GPSreAcq - default stage reached!");
    }

  }
  
}


void CStrategyGPSreAcq::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "GPSreAcq - Exiting");
}

void CStrategyGPSreAcq::enter( CStrategyInterface *m_pStrategyInterface )
{
  //reset the nextStage counter (starts at ONE, NOT zero) as the
  //strategy is being entered --> first stage to execute should be
  //stage #1
  nextStage = 1;
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "GPSreAcq - Entering");
}


