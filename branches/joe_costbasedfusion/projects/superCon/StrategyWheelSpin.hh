//SUPERCON STRATEGY TITLE: Wheel-Spin - ANYTIME STRATEGY
//HEADER FILE (.hh)
//DESCRIPTION: 

#include "Strategy.hh"
#include "sc_specs.h"

class CStrategyWheelSpin : public CStrategy
{

private:

  //# of the next stage to be executed - Stage #'s START at 1 (i.e. when
  //entering a new strategy nextStage = 1) - this is stored internally
  //by any strategy class instance so the information does not have to
  //be passed in stepForward(...)
  int nextStage;
  
  //latching bool instance to track whether to skip the operations for the
  //current stage -> value (true/false) updated by the entry operations.
  //NOTE: stage operations SKIPPED, iff skipStageOps == true.
  bool_latched skipStageOps;

public:

  /** CONSTRUCTORS **/
  CStrategyWheelSpin() : CStrategy() {}  

  /** DESTRUCTOR **/
  ~CStrategyWheelSpin() {}


  /** MUTATORS **/
  void stepForward(const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface);
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

};
