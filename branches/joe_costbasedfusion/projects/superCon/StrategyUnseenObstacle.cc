//SUPERCON STRATEGY TITLE: Unseen-Obstacle - ANYTIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyUnseenObstacle.hh"
#include "StrategyHelpers.hh"

void CStrategyUnseenObstacle::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE
  
  switch( nextStage ) {

  case 1: //superCon e-stop pause Alice to prepare for a gear change as it has
          //been determined that there is an unseen obstacle in front of Alice's
          //current position
    {

      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL
       * STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */

      //This is an ANYTIME STRATEGY and hence *cannot* call checkAll_TransTerms()
      //(as this checks for the entry conditions for ALL anytime strategies 
      //(including this one) - hence must check individually only the ones
      //required - the *order* is in DECREASING PRIORITY (and is hence important)
      if( skipStageOps == false ) {
	skipStageOps = trans_EstopPausedNOTsuperCon( (*m_pdiag), "UnseenObstacle - Stage1" );
      }
      if (skipStageOps == false ) {
	skipStageOps = trans_WheelSpin( (*m_pStrategyInterface), (*m_pdiag), "UnseenObstacle - Stage1");
      }
      if( skipStageOps == false ) {
	skipStageOps = term_PlannerNotNFP( (*m_pdiag), "UnseenObstacle - Stage1" );
      }

      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */      

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_superConEstop(estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "UnseenObstacle - Stage 1 Completed");
	nextStage++;
      }
    }
    break;
    
  case 2: //Wait for Alice to be stationary, and then paint a layer of superCon
          //obstacles into the map in front of Alice
    {
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL
       * STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      if (skipStageOps == false ) {
	skipStageOps = trans_WheelSpin( (*m_pStrategyInterface), (*m_pdiag), "UnseenObstacle - Stage2");
      }
      if( skipStageOps == false ) {
	skipStageOps = term_PlannerNotNFP( (*m_pdiag), "UnseenObstacle - Stage2" );
      }

      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->superConPaused == false ) {
	//NOT superCon paused - need to be paused & stationary prior to modifying
	//the superCon layer in the map

	/* SHOULD WE RE-PLAY STAGE ONE HERE? - OR JUST WAIT FOR ADRIVE, OR SHOULD WE
	 * REPLAY STAGE 1 (SEND pause) AFTER A TIMEOUT OF REACHING THIS POINT?
	 * nextStage = 1;*/
	SuperConLog().log( STR, WARNING_MSG, "UnseenObstacle: superCon e-stop pause not yet in place -> will poll");
	skipStageOps = true;
      }
      
      if ( m_pdiag->aliceStationary == false ) {
	SuperConLog().log( STR, WARNING_MSG, "UnseenObstacle: Alice not yet stationary won't update superCon map yet -> will poll");
	skipStageOps = true;
      }

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {
	
	//add a superCon obstacle to the superCon map layer
	m_pStrategyInterface->stage_addForwardUnseenObstacle( OFFSET_OF_VEHICLE_ORIGIN_FROM_REAR_AXLE );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "UnseenObstacle - Stage 2 Completed");
	nextStage++;
      }
    }
    break;

  case 3: //wait for PLN->NFP through a superCon obstacle (verifies that the map updates have
          //been applied), then transition to the LturnReverse strategy
    {
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL
       * STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      if (skipStageOps == false ) {
	skipStageOps = trans_WheelSpin( (*m_pStrategyInterface), (*m_pdiag), "UnseenObstacle - Stage3");
      }
      if( skipStageOps == false ) {
	skipStageOps = term_PlannerNotNFP( (*m_pdiag), "UnseenObstacle - Stage3" );
      }

      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      if( m_pdiag->plnNFP_SuperC == false ) {
	//map updates not yet applied - wait
	SuperConLog().log( STR, WARNING_MSG, "UnseenObstacle: PLN !-> NFP through superCon obstacle - map updates not applied, will poll");
	skipStageOps = true;
      }
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {
	
	transitionStrategy(StrategyLturnReverse, "UnseenObstacle: added detected superCon obstacle to the map - transition to LturnReverse");
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "UnseenObstacle: added detected superCon obstacle to the map - transition to LturnReverse");
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "UnseenObstacle - Stage 3 Completed");
	nextStage++;
      }
    }
    break;

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyUnseenObstacle::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyUnseenObstacle::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: UnseenObstacle - default stage reached!");
    }
  }
}


void CStrategyUnseenObstacle::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "UnseenObstacle - Exiting");
}

void CStrategyUnseenObstacle::enter( CStrategyInterface *m_pStrategyInterface )
{
  //reset the nextStage counter (starts at ONE, NOT zero) as the
  //strategy is being entered --> first stage to execute should be
  //stage #1
  nextStage = 1;
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "UnseenObstacle - Entering");
}
