/* SuperCon.hh
**
** Description:Contains GLOBAL defines of state structure and diagnostic 
** tables (variables stored in the tables)
**
*/

#ifndef SUPERCON_HH
#define SUPERCON_HH

#include "SuperConHelpers.hh"
#include "bool_counter.hh"
#include <string>

/*
**
** SCstate declaration - This is the SuperCon state table
**
** The 4 parameters are type, var_name, default_value, description
**
*/

#define STATE_LIST(_)							\
  /*									\
  **									\
  ** astate								\
  **									\
  */									\
  _(double, northing, 0.0, "Northing")					\
    _(double, easting, 0.0, "Easting")					\
    _(double, nVel, 0.0, "Northing Velocity")				\
    _(double, eVel, 0.0, "Easting Velocity")				\
    _(double, vehicleSpeed, 0.0, "astate vehicle speed")		\
    _(double, yaw, 0.0, "Yaw")						\
    _(double, pitch, 0.0, "Pitch")					\
    _(double, northingConfidence, 0.0, "Jeremy, what is this???")	\
    _(double, eastingConfidence, 0.0, "Jeremy, what is this???")	\
    _(double, heightConfidence, 0.0, "Jeremy, what is this???")		\
    _(double, rollConfidence, 0.0, "Jeremy, what is this???")		\
    _(double, pitchConfidence, 0.0, "Jeremy, what is this???")		\
    _(double, yawConfidence, 0.0, "Jeremy, what is this???")		\
									\
  /*									\
  **									\
  ** adrive								\
  **									\
  */									\
    _(double, wheelSpeed, 0.0, "wheel speed from OBD2 in m/s")		\
    _(double, timeSinceEngineStart, 0.0, "time is s since engine start") \
    _(int, transmissionPos, 0, "current gear, enums in interface_superCon_adrive.hh") \
    _(double, forceAppliedtoWheels, 0.0, "engine force applied to the wheels in newtons") \
    _(int, estopDARPAPos, 2, "DARPA e-stop track position 0 = disable, 1= pause, 2 = run") \
    _(int, estopSuperConPos, 2, "SuperCon e-stop track position 0 = disable, 1= pause, 2 = run") \
    _(int, estopAdrivePos, 2, "adrive e-stop track position 0 = disable, 1= pause, 2 = run") \
    _(double, gasCmd, 0.0, "current *final* (actually passed to actuator) THROTTLE ONLY (normalised 0-1) command in adrive") \
    _(double, brakeCmd, 0.0, "current *final* (actually passed to actuator) BRAKE ONLY (normalised 0-1) command in adrive" ) \ 
    _(int, obd2operational, 0, "OBD2 on/off status, 0 = OFF, 1 = ON" ) \
                                                                       \
  /*									\
  **									\
  ** TrajFollower							\
  **									\
  */									\
    _(double, currentSpeedRef, 0.0, "Speed trajfollower is attempting to follow" ) \
    _(double, currentAccelCmd, 0.0, "Normalised accel (THROTTLE & BRAKE: -1-->+1) command from trajFollower to adrive") \
    _(double, currentSteerCmd, 0.0, "Normalised steering command from trajFollower to adrive") \
    _(int, currentTFmode, 0, "TrajFollower mode - reverse or FORWARDS, def in trajF_status_struct.hh") \
    _(double, trajFMAXspeedCap, 0.0, "trajFollower MAX speed-cap")	\
    _(double, trajFMINspeedCap, 0.0, "trajFollower MIN speed-cap")	\
									\
  /*									\
  **									\
  ** scThrows								\
  ** Use type scThrowInt to automatically reset value			\
  **									\
  */									\
    _(scThrowDbl, minCellSpeedOnCurrentTraj, 0.0, "lowest speed cell passed through by the current trajectory" ) \
    _(scThrowInt, tfFinishedReversing, 0, "Signals TF has successfully reversed for the distance specified by SuperCon and has now stopped the vehicle and awaits further instructions") \
    /*_(scThrowInt, astNeedToStopForGPSstabilisation, 0, "Signals astate needs Alice to stop for a period whilst GPS stabilises") */ \
    _(scThrowInt, astNeedToIncludeGPSafterReAcq, 0, "Signals astate has re-acquired GPS after an outage, and is ready to be re-included into the astate estimate - re-inclusion will result in a potentially big jump") \
    _(scThrowInt, astHappyPanda, 0, "generic confirm for astate to signal that it has no requests for action and is as happy as possible") \
									\
  /*									\
  **									\
  ** Meta states							\
  **									\
  */									\
    _(double, componentOfWeightDownHill, 0.0, "component of the vehicle weight that is directed DOWN the current incline (taken from vehicle pitch)") \
    _(double, astateINVconfidence, 0.0, "astate's confidence in its global position estimate - combined estimate") \
									\
  /*                                                                    \
  **									\
  ** superCon internal							\
  **									\
  */									\
    _(double, copyTimestamp, 0.0, "Timestamp of the state copy to diagnostics") \

  /** END OF TABLE **/


#define scThrowInt int
#define scThrowDbl double
BEGIN_DEFINE_STATES(SCstate, STATE_LIST);
#undef scThrowInt
#define scThrowInt(x) 0

#undef scThrowDbl
#define scThrowDbl(x) 0.0

END_DEFINE_STATES(SCstate, STATE_LIST);


#warning CHECK if Jeremy L. still needs this functionality _(scThrowInt, astNeedToStopForGPSstabilisation, 0, Signals astate needs Alice to stop for a period whilst GPS stabilises)


/*
**
** SCdiagnostic declaration - This is the SuperCon Diagnostic table
**
** All items are of time bool_counter
** The 2 parameters are: var_name, desc
**
*/

#define DIAGNOSTIC_LIST(_)					\
  /*								\
  **								\
  ** 'STANDARD' DIAGNOSTIC TABLE CONDITIONAL ELEMENTS		\
  **								\
  */									\
    _(engineStatus, "Engine started/off? -> engine on = TRUE")		\
    _(engineForceForIncline, "Is the engine force > force required to move Alice UP the current incline + #defined % -> actual force > required = TRUE") \
    									\
    _(astateConfidencePoor, "Is astate's confidence < BAD threshold (#define)? -> astate confidence < BAD = TRUE") \
    _(validAliceStop, "Is Alice stationary && is the traj-speed for the current traj-point < the slowest maintainable speed && throttle/brake cmd < 0 (braking) (if all conditions == true, then rule -> TRUE") \
    _(aliceStationary, "Is Alice currently stationary && is the adrive brake command >= ZERO? (braking) --> if all conditions true then -> TRUE") \
    _(superConPaused, "Is superCon e-stop == pause? -> superCon e-stop = pauseD --> = TRUE") \
    _(pausedNOTsuperCon, "Is Alice paused by DARPA/adrive e-stop NOT superCon") \
    _(trajFForwards, "Is the TrajFollower in FORWARDS mode? -> TRUE if in FORWARDS mode") \
    _(trajFReverse, "Is the TrajFollower in reverse mode? -> TRUE if in reverse mode") \
    _(trajFLoneRspeedCap, "Is the current speed-cap being used by the trajFollower a MIN speedcap && speed == LoneRanger strategy drive through obstacles speed? --> TRUE if BOTH conditions true") \
    _(tfFinishedReversing, "Signals TF has successfully reversed for the distance specified by SuperCon and has now stopped the vehicle and awaits further instructions") \
    _(adriveGearReverse, "Is the current gear = reverse? -> TRUE iff gear = reverse") \
    _(adriveGearDrive, "Is the current gear = drive? -> TRUE iff gear = drive") \
    _(adriveGearPark, "Is the current gear = par? -> TRUE iff gear = park") \
    _(vehORwheelSpeedsLessThanMinMaintainable, "Are the current vehicle speed OR (||) OBD2 wheelspeed (if OBD2 is available) < minimum maintainable Alice speeds?") \
    									\
  /*									\
  **									\
  ** STRATEGY TRANSITION CONDITIONAL ELEMENTS				\
  **									\
  ** Anytime Strategy transition rules					\
  **									\
  */									\
    _(transWheelSpin, "Wheel spin has occured")				\
    _(transUnseenObstacle, "Unseen Obstacle has occured")		\
    _(transSlowAdvance, "current plan passes through TERRAIN obstacle (&& started in NOMINAL") \
    _(transGPSreAcq, "GPS reaquired after a period of outage - will result in a state jump") \
    _(transEstopPausedNOTsuperCon, "e-stop pause from adrive/DARPA has occurred - hold in strategy until released (to RUN)" ) \
  /*									\
  **									\
  ** DIAGNOSTIC TABLE ELEMENTS THAT CARRY FORWARD STATE TABLE VARIABLES	\
  **									\
  */									\
    _(plnValidTraj, "Is the current traj output by the PLN valid (i.e. NOT NFP - *for any reason*)") \
    _(plnNFP_Terrain, "Is the current NFP traj, NFP because it passes through a terrain obstacle (and NO SC obstacles)? -> pln NFP due to terrain (only) = TRUE") \
    _(plnNFP_SuperC, "Is the current NFP traj, NFP because it passes through a SuperC. obstacle? -> pln NFP due to SC = TRUE") \
  /** END OF TABLE **/

DEFINE_DIAGNOSTICS(SCdiagnostic, DIAGNOSTIC_LIST);


/*
**
** SUPERCON STRATEGY TABLE
**
*/
enum SCStrategy
  {
    StrategyNone=0,         //"Null" value, should never be used (except internaly)
    StrategyNominal,
    StrategyUnseenObstacle,
    StrategySlowAdvance,
    StrategyLoneRanger,
    StrategyLturnReverse,
    StrategyWheelSpin,
    StrategyGPSreAcq,
    StrategyEstopPausedNOTsuperCon,
    StrategyLast
  };


#endif   //SUPERCON_HH
