/* SUPERCON STRATEGY TITLE: Nominal
 * SOURCE FILE (.cc)
 * See strategy header file for detailed documentation
 */

#include"StrategyNominal.hh"
#include "StrategyHelpers.hh"

//Step one step forward using the (new) supplied diagnostic rule results
void CStrategyNominal::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface )
{
  skipStageOps.reset(); //resets to FALSE

  switch( nextStage ) {

  case 1:
    {

      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL
       * STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      //This form is REQUIRED as trans_* & term_* perform a strategy transition/termination (if conditions equate to TRUE)

      if( skipStageOps == false ) {
	skipStageOps = trans_EstopPausedNOTsuperCon( (*m_pdiag), "Nominal - Stage1" );
      }
      if( skipStageOps == false ) {
	skipStageOps = trans_UnseenObstacle( (*m_pStrategyInterface), (*m_pdiag), "Nominal - Stage1" );
      }
      if( skipStageOps == false ) {
	skipStageOps = trans_WheelSpin( (*m_pStrategyInterface), (*m_pdiag), "Nominal - Stage1" );
      }
      if( skipStageOps == false) {
	skipStageOps = trans_GPSreAcq( (*m_pdiag), "Nominal - Stage1" );
      }
      if( skipStageOps == false ) {
	skipStageOps = trans_SlowAdvance( (*m_pdiag), "Nominal - Stage1" );
      }

      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_modifyTrajFSpeedCap( remove_max_speedcap, 0.0 );
	m_pStrategyInterface->stage_modifyTrajFSpeedCap( remove_min_speedcap, 0.0 );
	
	//Change gear to drive if we are NOT in drive when we enter the Nominal strategy
	if( m_pdiag->adriveGearDrive == false ) {
	  SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Nominal: Nominal strategy started when NOT in drive - changing to drive" );
	  m_pStrategyInterface->stage_changeGear( drive_gear );
	}
	
	//reset the superCon e-stop (c-stop) to run (as we're in Nominal we should not still be paused)
	m_pStrategyInterface->stage_superConEstop( estp_run );
		
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Nominal - NO issues detected");
	//DON'T update the nextStage variable here as Nominal should loop through
	//this stage and repeatedly check the conditions to determine whether an issue
	//has been detected that requires the use of a (known) strategy to resolve it.
      }
    }
    break;

    /* END OF STRATEGY  */

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyNominal::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyNominal::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: Nominal - default stage reached!");

      //re-entering the nominal strategy (i.e. this strategy) will reset the nextStage counter, and reset the strategy internal
      //variables - the assumption is that this is the best option to attempt to clear the problem
    }
  }
}

//We are leaving this state, do clean up (NO NEW STATE TRANSFERS)
void CStrategyNominal::leave( CStrategyInterface *m_pStrategyInterface )
{
  skipStageOps.reset(); //resets to FALSE
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Nominal - Exiting" );
}

//We are entering this state, do initialisation (NO NEW STATE TRANSFERS)
void CStrategyNominal::enter( CStrategyInterface *m_pStrategyInterface )
{
  //reset the nextStage counter (starts at ONE, NOT zero) as the
  //strategy is being entered --> first stage to execute should be
  //stage #1
  nextStage = 1; //reset to starting value
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Nominal - Entering");
}

