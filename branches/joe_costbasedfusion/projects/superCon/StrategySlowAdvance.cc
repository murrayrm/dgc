//SUPERCON STRATEGY TITLE: Slow Advance - SOMETIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategySlowAdvance.hh"
#include "StrategyHelpers.hh"

void CStrategySlowAdvance::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {
  
  skipStageOps.reset(); //resets to FALSE

  switch( nextStage ) {

  case 1: //send speed-cap to trajF = high accuracy map speed (get better view of obstacle)
    {
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL
       * STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAll_TransTerms( m_pStrategyInterface, m_pdiag, "SlowAdvance - Stage1" );
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_modifyTrajFSpeedCap( add_max_speedcap, HIGH_ACCURACY_MAPS_SPEED_MS );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "SlowAdvance - Stage 1 Completed");
	nextStage++;
      }
    }
    break;

  case 2: //wait until planner -> !NFP or Alice stops at zero-speed point on Traj
    {
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL
       * STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAll_TransTerms( m_pStrategyInterface, m_pdiag, "SlowAdvance - Stage2" );
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->validAliceStop == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "SlowAdvance: Alice not yet stopped in front of obstacle - will poll");
	//nextStage NOT updated here as conditions for proceeding with this
	//strategy have NOT yet been met
      }
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {

	//remove the superCon speed-cap imposed at the start of the SlowAdvance
	//strategy, and transition to L-turn reverse strategy as Alice is currently
	//stationary at a point in the trajectory where the traj-speed goes to zero
	//(hence she should be in front of whatever obstacle is blocked her)
	m_pStrategyInterface->stage_modifyTrajFSpeedCap( remove_max_speedcap, 0 );
	transitionStrategy( StrategyLturnReverse, "SlowAdvance complete - stationary in front of intraversible obstacle" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "SlowAdvance complete - stationary in front of intraversible obstacle");
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "SlowAdvance - Stage 2 Completed");
      }
    }      
    break;

    /** END OF STRATEGY **/
 
  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategySlowAdvance::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategySlowAdvance::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: SlowAdvance - default stage reached!");
    }
  }
}


void CStrategySlowAdvance::leave( CStrategyInterface *m_pStrategyInterface )
{
  skipStageOps.reset(); //resets to FALSE
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "SlowAdvance - Exiting");
}

void CStrategySlowAdvance::enter( CStrategyInterface *m_pStrategyInterface )
{
  //reset the nextStage counter (starts at ONE, NOT zero) as the
  //strategy is being entered --> first stage to execute should be
  //stage #1
  nextStage = 1; //reset to starting value
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "SlowAdvance - Entering");
}
