//SUPERCON STRATEGY TITLE: Lone Ranger - SOMETIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyLoneRanger.hh"
#include "StrategyHelpers.hh"

void CStrategyLoneRanger::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE  

  switch( nextStage ) {

  case 1: //verify input conditions & e-stop pause for gear change (--> drive)
    {
      
      //TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* SHOULD
      //BE EVALUATED *ABOVE* THIS POINT
      skipStageOps = checkAll_TransTerms( m_pStrategyInterface,  m_pdiag, "LoneRanger - Stage1" );

      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->plnNFP_SuperC == true ) {
// -- SHOULD HAVE A PERSISTENCE COUNT HERE!! --
	//conditions for LoneRanger not met - already tested all options ahead
	//need to reverse further
	transitionStrategy(StrategyLturnReverse, "LoneRanger - PLN->NFP due to SuperCon Obstacle -> LturnReverse");
	SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "LoneRanger - PLN->NFP due to SuperCon Obstacle -> LturnReverse");
      }
      
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_superConEstop(estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LoneRanger - Stage 1 Completed");
	nextStage++;
      }
    }
    break;
    
  case 2: //change gear into drive so that Lone-Ranger investigation can begin
    {

      //TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* SHOULD
      //BE EVALUATED *ABOVE* THIS POINT
      skipStageOps = checkAll_TransTerms( m_pStrategyInterface, m_pdiag, "LoneRanger - Stage2" );

      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->plnNFP_SuperC == true ) {
// -- SHOULD HAVE A PERSISTENCE COUNT HERE!! --
	//conditions for LoneRanger not met - already tested all options ahead
	//need to reverse further
	transitionStrategy(StrategyLturnReverse, "LoneRanger - PLN->NFP due to SuperCon Obstacle -> LturnReverse");
	SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "LoneRanger - PLN->NFP due to SuperCon Obstacle -> LturnReverse");
      }
      
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->superConPaused == false ) {
	//NOT superCon paused - need to be to change gear, don't send gear change
	//command until Alice is paused

	/* SHOULD WE RE-PLAY STAGE ONE HERE? - OR JUST WAIT FOR Adrive, OR SHOULD WE
	 * REPLAY STAGE 1 (SEND pause) AFTER A TIMEOUT OF REACHING THIS POINT?
	 * nextStage = 1;*/
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "LoneRanger: superCon e-stop pause not yet in place -> will poll");
      }


      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_changeGear(drive_gear);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LoneRanger - Stage 2 Completed");
	nextStage++;
      }
    }
    break;

  case 3: //send change-mode command to trajFollower ?-->FORWARDS-MODE
    {

      //TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* SHOULD
      //BE EVALUATED *ABOVE* THIS POINT
      skipStageOps = checkAll_TransTerms( m_pStrategyInterface, m_pdiag, "LoneRanger - Stage3" );

      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->plnNFP_SuperC == true ) {
// -- SHOULD HAVE A PERSISTENCE COUNT HERE!! --
	//conditions for LoneRanger not met - already tested all options ahead
	//need to reverse further
	transitionStrategy(StrategyLturnReverse, "LoneRanger - PLN->NFP due to SuperCon Obstacle -> LturnReverse");
	SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "LoneRanger - PLN->NFP due to SuperCon Obstacle -> LturnReverse");
      }
      
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->adriveGearDrive == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "LoneRanger - Alice not yet in drive gear - will poll");
      }


      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_changeTFmode( tf_forwards, 0 );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LoneRanger - Stage 3 Completed");
	nextStage++;
      }
    }
    break;

  case 4: //send command to trajFollower to add a MIN (& MAX for safety to deal with the case in which the planners
          //have died and are not sending any more trajectories, and trajSelector is sending the same trajectory (even if
          //it's speed is > the cell-speed for any point in the map) speed-caps to the trajFollower
    {
      
      //TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* SHOULD
      //BE EVALUATED *ABOVE* THIS POINT
      skipStageOps = checkAll_TransTerms( m_pStrategyInterface, m_pdiag, "LoneRanger - Stage4" );

      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->plnNFP_SuperC == true ) {
// -- SHOULD HAVE A PERSISTENCE COUNT HERE!! --
	//conditions for LoneRanger not met - already tested all options ahead
	//need to reverse further
	transitionStrategy(StrategyLturnReverse, "LoneRanger - PLN->NFP due to SuperCon Obstacle -> LturnReverse");
	SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "LoneRanger - PLN->NFP due to SuperCon Obstacle -> LturnReverse");
      }
      
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->trajFForwards == false ) {
	SuperConLog().log( STR, WARNING_MSG, "Lone Ranger: TrajFollower not yet in forwards mode");
	skipStageOps = true;
      }


      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_modifyTrajFSpeedCap( add_min_speedcap, LONE_RANGER_MIN_SPEED_MS );
	m_pStrategyInterface->stage_modifyTrajFSpeedCap( add_max_speedcap, ( LONE_RANGER_MIN_SPEED_MS + 1 ) );
#warning setting MAX speedCap to (MIN+1) in LoneRanger strategy (explore) is arbitrary and could cause issues with trajFollower - need to test on Alice
	SuperConLog().log( STR, WARNING_MSG, "WARNING: LoneRanger - setting MAX speedCap to LONE_RANGER_MIN_SPEED+1 is arbitrary" ); 
	SuperConLog().log( STR,  STRATEGY_STAGE_MSG, "LoneRanger - Stage 4 Completed" );
	nextStage++;
      }
    }
    break;

  case 5: //superCon e-stop run Alice now that she is ready to drive FORWARDS
    {

      //TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* SHOULD
      //BE EVALUATED *ABOVE* THIS POINT
      skipStageOps = checkAll_TransTerms( m_pStrategyInterface, m_pdiag, "LoneRanger - Stage5" );

      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->plnNFP_SuperC == true ) {
// -- SHOULD HAVE A PERSISTENCE COUNT HERE!! --
	//conditions for LoneRanger not met - already tested all options ahead
	//need to reverse further
	transitionStrategy(StrategyLturnReverse, "LoneRanger - PLN->NFP due to SuperCon Obstacle -> LturnReverse");
	SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "LoneRanger - PLN->NFP due to SuperCon Obstacle -> LturnReverse");
      }
      
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->trajFForwards == false ) {
	SuperConLog().log( STR, WARNING_MSG, "LoneRanger: TrajFollower not yet in forwards mode");
	skipStageOps = true;
      }

      if( m_pdiag->trajFLoneRspeedCap == false ) {
	SuperConLog().log( STR, WARNING_MSG, "LoneRanger speed-cap not yet applied in trajF - will poll");
	skipStageOps = true;
      }

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_superConEstop(estp_run);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LoneRanger - HI-HO SILVER! - explore me some terrain matey! - yarr!");
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LoneRanger - Stage 5 Completed");
	nextStage++;
      }
    }
    break;
    
  case 6: //stay in this LoneRanger stage whilst exploring the terrain - can't return to nominal as this
          //would clear the speed-cap, and all required transition/termination checks are conducted here
          //NOTE: nextStage is NOT incremented at the end of this stage, as this stage should be polled
    {

      //TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* SHOULD
      //BE EVALUATED *ABOVE* THIS POINT
      skipStageOps = checkAll_TransTerms( m_pStrategyInterface, m_pdiag, "LoneRanger - Stage6" );

      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->plnNFP_SuperC == true ) {
// -- SHOULD HAVE A PERSISTENCE COUNT HERE!! --
	//conditions for LoneRanger not met - already tested all options ahead
	//need to reverse further
	transitionStrategy(StrategyLturnReverse, "LoneRanger - PLN->NFP due to SuperCon Obstacle -> LturnReverse");
	SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "LoneRanger - PLN->NFP due to SuperCon Obstacle -> LturnReverse");
      }
      
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {

	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LoneRanger - Exploring! - shiver-me-timbers, yarr!! (is Jeremy G. wrong?)");
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LoneRanger - Stage 6 Completed");
	//DO NOT UPDATE nextStage here - should wait (poll) in this stage until ready to leave
	//LoneRanger strategy
      }
    }
    break;

    /* END OF STRATEGY */

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyLoneRanger::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyLoneRanger::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: LoneRanger - default stage reached!");
    }
  }
} //end of stepForward()


void CStrategyLoneRanger::leave( CStrategyInterface *m_pStrategyInterface )
{
  skipStageOps.reset(); //resets to FALSE
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "LoneRanger - Exiting");
}


void CStrategyLoneRanger::enter( CStrategyInterface *m_pStrategyInterface )
{
  //reset the nextStage counter (starts at ONE, NOT zero) as the
  //strategy is being entered --> first stage to execute should be
  //stage #1
  nextStage = 1;
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "LoneRanger - Entering");
}
