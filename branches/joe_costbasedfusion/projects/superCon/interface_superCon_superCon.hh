#ifndef INTERFACE_SUPERCON_SUPERCON_HH
#define INTERFACE_SUPERCON_SUPERCON_HH

//FILE DESCRIPTION: Header file that contains the definitions of data structures that are
//used in direct communication between SuperCon's scState update thread, and the
//diagnostic/evaluation thread which houses the SC state machine

namespace superCon {

/* Use the scMessage method */
//Used for superCon --> superCon (conditional) messages *using the scMessage method/message
//ONLY* - these enums should be used in the msgID field of the scMessage, ALSO
//the msgID field only takes an integer - HENCE you should RECAST one of the
//enums (type: scMsgID_SC) - to an integer (you should recast whichever member
//of the enum details what you want to do - superCon will then perform the inverse
//recast statement so that the code on either side is cleaner - there are reasons
//why this has to be done)
#define SUPERCON_SC_MSGIDS_LIST(_) \
  _( copy_scstate_table, = 0 ) \
  _( update_double_scstate_value, = 1 ) \
  _( update_int_scstate_value, = 2 )
DEFINE_ENUM(scMsgID_SC, SUPERCON_SC_MSGIDS_LIST)

}

#endif
