//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_RoadFollower.hh"

#ifdef SKYNET
#include "sparrowhawk.hh"
#endif

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void UD_RoadFollower::estimate_curvature()
{
  int new_top, top, bot, halfway;

  top = vp->voter_top_margin;
  bot = vp->voter_bottom_margin;

  // set smaller voter regions, compute vote functions for them, and
  // track their vanishing points

  
  new_top = h - (int) ceil(straight_tracker->vp_y());
  halfway = bot + (new_top - bot) / 3;

  printf("w = %i, h = %i, horizon y = %lf, new top = %i, halfway = %i, bot = %i\n", w, h, straight_tracker->vp_y(), new_top, halfway, bot);

  for (int i = 0; i < num_curve_pts; i++) {

    if (i == 0) {
      vp->voter_top_margin = new_top;
      vp->voter_bottom_margin = halfway;
    }
    else if (i == 1) {
      vp->voter_top_margin = halfway;
      vp->voter_bottom_margin = bot;
    }

//     vp->voter_top_margin = new_top;
//     vp->voter_bottom_margin = bot + 20;

    // revote with new voting region

    vp->compute(vp->dom->gabor_max_response_index);

    // track vanishing point for this voting region

    curve_tracker[i]->update();
  }

  vp->voter_top_margin = top;
  vp->voter_bottom_margin = bot;

}

//----------------------------------------------------------------------------

void UD_RoadFollower::initialize_road_width()
{
  num_lookahead_pts = 25;
  delta_lookahead = 2.0;
  lookahead_x = (double *) calloc(num_lookahead_pts, sizeof(double));
  lookahead_z = (double *) calloc(num_lookahead_pts, sizeof(double));
  lookahead_radius = (double *) calloc(num_lookahead_pts, sizeof(double));
  lookahead_speed = (double *) calloc(num_lookahead_pts, sizeof(double));
}

//----------------------------------------------------------------------------

void UD_RoadFollower::estimate_road_width()
{
  double d;
  int i;

  if (oo->onroad_state) {
    if (ladarray->num_ladars > 0) {    //Are we using ladars
      for (i = 0; i < num_lookahead_pts; i++) {
	d = delta_lookahead * (double) (i + 1);
	lookahead_x[i] = laser_offset + d * sin(direction_error);
	lookahead_z[i] = d * cos(direction_error);
	lookahead_radius[i] = ladarray->n_hit_radius(lookahead_x[i], lookahead_z[i], 5.0, 3); 
	lookahead_speed[i] = cos(direction_error) * cos(direction_error) * 5.0;  // meters / second
	//    printf("speed = %lf m/s (%lf)\n", lookahead_speed[i], direction_error);
      }
    } else {    //Not using ladars

      for (i = 0; i < num_lookahead_pts; i++) {
	d = delta_lookahead * (double) (i + 1);
	lookahead_x[i] = d * sin(direction_error);
	lookahead_z[i] = d * cos(direction_error);
	lookahead_radius[i] = 5.0;
	lookahead_speed[i] = 5.0;  // meters / second
      }
    }
  } else {   // No road detected
    for (i = 0; i < num_lookahead_pts; i++) 
      lookahead_radius[i] = 0.0;
  }
}

//----------------------------------------------------------------------------

// doesn't this only work off of logged data?
/*
void UD_RoadFollower::get_state()
{
  double diff;

  have_state = FALSE;
 
  if (ladarray->num_ladars > 0) {
    last_northing = northing;
    last_easting = easting;

    last_northing_velocity = northing_velocity;
    last_easting_velocity = easting_velocity;

    ladarray->ladsrc[0]->get_state(&northing, &easting, &heading);

    diff = ladarray->ladsrc[0]->timestamp - ladarray->ladsrc[0]->last_timestamp;
    if (diff > 0.000001) {
      northing_velocity = (northing - last_northing) / diff;
      easting_velocity = (easting - last_easting) / diff;
      northing_acceleration = northing_velocity - last_northing_velocity;
      easting_acceleration = easting_velocity - last_easting_velocity;
      //      printf("New: vN = %lf, vE = %lf, aN = %lf, aE = %lf, t = %lf, lt = %lf\n", northing_velocity, easting_velocity, northing_acceleration, easting_acceleration, ladarray->ladsrc[0]->timestamp, ladarray->ladsrc[0]->last_timestamp);
    }
//     else
//       printf("Old: vN = %lf, vE = %lf, aN = %lf, aE = %lf, t = %lf, lt = %lf\n", northing_velocity, easting_velocity, northing_acceleration, easting_acceleration, ladarray->ladsrc[0]->timestamp, ladarray->ladsrc[0]->last_timestamp);

    // this doesn't work because we sometimes get the same E & N in two consecutive
    // calls when we're really moving because we're just trying to sync to image frame
    // rate that's moving too fast

    //    printf("%i, %lf, %lf, %lf, %lf \n", cum_iterations, easting_velocity, northing_velocity, easting_acceleration, northing_acceleration);

    ladarray->ladsrc[0]->last_timestamp = ladarray->ladsrc[0]->timestamp;

    //    printf("state %lf %lf\n", northing, easting);
    have_state = TRUE;
}

  cum_iterations++;
}
*/

//----------------------------------------------------------------------------

/// compute one iteration of road tracker, assuming image has already been 
/// captured.  decides whether image is of a valid road, updates estimate
/// of vanishing point, computes centerline of road from dominant orientation
/// support for vanishing point

void UD_RoadFollower::run()
{
  // where are we?

  cum_iterations++;
  //  get_state();

  // do we think we are actually on or near a road?
  
  check_onroad();

  // track road direction with particle filter

  straight_tracker->update();
  compute_direction_error(cal);

  // track centerline

  //  lateral_x = compute_centerline();   // in pixels


  // track ladar gap

  if (ladarray->num_ladars > 0) {
    gap_tracker->iterate();

    // post-process to get estimated minimum road width

    laser_offset = gap_tracker->state->x[0];
  }

  estimate_road_width();

  // curvature estimate

  //  rtracer->trace(easting, northing, heading);

  //  estimate_curvature();
}

//----------------------------------------------------------------------------

int UD_RoadFollower::is_synced()
{
  return ladarray->ladsrc[0]->statesrc->is_synced();
}

//----------------------------------------------------------------------------

/// handles bookkeeping when we are doing multiple run()'s per image
/// versus just one.  does no actual computation; returns TRUE if ready for 
/// next image to be captured, FALSE if still working.  does not currently
/// reset road_support parameters based on result

int UD_RoadFollower::iterate()
{
  return straight_tracker->iterate();
}

//----------------------------------------------------------------------------

/// if there's an actual vehicle to send putative steering commands to, 
/// uses camera calibration info to convert pixel-unit road parameters such
/// as VP location and mean support ray to metric vehicle coordinate values.
/// run() should have been called already 

void UD_RoadFollower::compute_direction_error(UD_CameraCalibration *cal)
{
  float fw, fw_2, hfov, d, tan_phi;

  if (oo->onroad_state) {

    fw = (float) w;
    fw_2 = 0.5 * fw;

    if (!cal) {  
      printf("controller output: must have camera calibration\n");
      exit(1);
    }
    
    // putting in correction for apparent camera yaw of about 2 degrees
    direction_error = cal->yaw_angle + PIX2RAD(straight_tracker->vp_x() - fw_2, fw, cal->hfov);             // in radians

    //direction_error = 0.0333 + PIX2RAD(straight_tracker->vp_x() - fw_2, fw, cal->hfov);             // in radians
    //printf("%lf\n", RAD2DEG(direction_error));
    if (gap_tracker)
      gap_tracker->direction_error = direction_error;
    //    lateral_error = PIX2RAD(lateral_x - fw_2, fw, cal->hfov);        // in radians

    // -dy = height of camera
    // phi = pitch angle of ray through bottom row of image
    // d = dist to nearest visible ground point (y = bottom of image, x = w/2)--assuming planar ground

    /*
    tan_phi = tan(0.5 * cal->vfov - cal->pitch_angle);
    d = -cal->dy / tan_phi;
    lateral_offset = d * tan(lateral_error);
    */
  }

  //  print_output_to_controller();
  
  // and send it out via skynet (at least on/off flag, dir rads, lat meters)...

}

//----------------------------------------------------------------------------

/// report output parameters being sent to vehicle

void UD_RoadFollower::print_output_to_controller()
{
  /*
  if (oo->onroad_state)
    printf("ONROAD: dir %.2f degs, lat %.2f m (%.2f degs)\n", RAD2DEG(direction_error), lateral_offset, RAD2DEG(lateral_error));
  else
    printf("OFFROAD\n");
  */
}

//----------------------------------------------------------------------------

/// constructor for road follower class, which organizes a collection of different 
/// trackers such as for the road curvature and the road region based on dominant
/// orientations, as well as a road width estimator based on ladar data

UD_RoadFollower::UD_RoadFollower(UD_VanishingPoint *van, UD_OnOff *onoff, UD_LadarSourceArray *ladarr)
{
  UD_Vector *meanstate, *covdiag;

  use_bad_lighting = true;
  bad_lighting_value = false;

#ifdef SKYNET
  SparrowHawk().rebind("UseBadLighting", &use_bad_lighting);
  SparrowHawk().rebind("BadLightingValue", &bad_lighting_value);
#endif    

  // local names 
  
  vp = van;
  oo = onoff;
  w = vp->w;
  h = vp->h;
  cw = vp->cw;
  ch = vp->ch;
  ladarray = ladarr;

  // curve tracker

  northing_velocity = easting_velocity = northing_acceleration = easting_acceleration = 0;

  straight_tracker = initialize_UD_VP_PositionTracker(vp);
  //straight_tracker = initialize_UD_VP_VelocityTracker(vp);
  //straight_tracker = initialize_UD_VP_MultiTracker(vp);
  
  /*
  num_curve_pts = 2;
  curve_tracker = (UD_VP_PositionTracker **) calloc(num_curve_pts, sizeof(UD_VP_PositionTracker *));
  for (int i = 0; i < num_curve_pts; i++)
    curve_tracker[i] = initialize_UD_VP_PositionTracker(vp); 
  */

  // support region 
  
  initialize_support(vp->dom->gabor_start_orientation + vp->dom->gabor_delta_orientation, vp->dom->gabor_delta_orientation);

  // gap tracker -- state is ladar angle of center of gap (assuming constant gap width)

  if (ladarray->num_ladars > 0) {
    meanstate = make_UD_Vector("meanstate", 1);   
    covdiag = make_UD_Vector("covdiag", 1);
    covdiag->x[0] = 0.1;  // 0.5
     
    gap_tracker = new UD_Ladar_GapTracker(ladarray, 100, meanstate, covdiag);
  }
  else
    gap_tracker = NULL;

  have_state = FALSE;

  // aerial tracker

  // finish

  // should free meanstate, covdiag
  meanstate = make_UD_Vector("meanstate", 3);   
  covdiag = make_UD_Vector("covdiag", 3);
  covdiag->x[0] = 0.05;  //1 
  covdiag->x[1] = 0.05;  //1 
  covdiag->x[2] = 0.001;  
  //  rtracer = new UD_Aerial_RoadTracer(100, meanstate, covdiag);

  cum_iterations = 0;

  initialize_road_width();
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// initialize variables used to identify road region by which pixels
/// voted for the vanishing point (its "support").  start_orientation and
/// delta_orientation should be specified in DEGREES

void UD_RoadFollower::initialize_support(float start_orientation, float delta_orientation)
{
  int i;
  float theta;
  int x, y, cur_ray;

  // local variables

  support_start_orientation = DEG2RAD(start_orientation);
  support_delta_orientation = DEG2RAD(delta_orientation);

  // default values

  support_angle_diff_thresh             = 0.75;        // 0.75 radians works well for 36 orientations   
  support_alpha                         = 0.1;
  support_cur_dx                        = 0;
  support_cur_dy                        = 1;   
  show_support                          = FALSE;
  show_centerline                       = FALSE;  

  // use radial rays long enough to reach edge of image from any VP; clip against 
  // valid dominant orientation region

  support_ray_length = ceil(hypot(cw, ch));
  //  support_ray_length = ceil(sqrt(SQUARE(cw) + SQUARE(ch)));
  support_ray_min_length = 0.1 * (float) w;

  for (support_num_orientations = 0, theta = support_start_orientation; 
       theta <= PI - support_start_orientation; 
       theta += support_delta_orientation, support_num_orientations++);

  support_ray_dx = (float *) calloc(support_num_orientations, sizeof(float));
  support_ray_dy = (float *) calloc(support_num_orientations, sizeof(float));

  for (i = 0, theta = support_start_orientation; 
       i < support_num_orientations; 
       theta += support_delta_orientation, i++) {

    support_ray_dx[i] = cos(theta) * support_ray_length;
    support_ray_dy[i] = sin(theta) * support_ray_length; 
  }

  support_ray_vx = (float *) calloc(support_num_orientations, sizeof(float));
  support_ray_vy = (float *) calloc(support_num_orientations, sizeof(float));
  support_ray_ex = (float *) calloc(support_num_orientations, sizeof(float));
  support_ray_ey = (float *) calloc(support_num_orientations, sizeof(float));
  support_ray_long_enough = (int *) calloc(support_num_orientations, sizeof(int));
  support_angle_diff = (float *) calloc(support_num_orientations, sizeof(float));
}

//----------------------------------------------------------------------------

/// draw vanishing point's support rays as green lines
/// in estimated road region.  assuming window dimensions are same as
/// candidate region
  
void UD_RoadFollower::draw_support()
{
  int i;

  glColor3f(0, 1, 0);
  
  glBegin(GL_LINES);

  for (i = 0; i < support_num_orientations; i++) 
    if (support_ray_long_enough[i] && support_angle_diff[i] < support_angle_diff_thresh) {
      glVertex2f(vp->im2can_x(support_ray_vx[i]), ch - vp->im2can_y(support_ray_vy[i]));
      glVertex2f(vp->im2can_x(support_ray_ex[i]), ch - vp->im2can_y(support_ray_ey[i])); 
    }

  glEnd();

}
  
//----------------------------------------------------------------------------

/// determine which region around clip rectangle (according to Cohen-Sutherland
/// algorithm) point belongs to.  the code in this function is taken directly 
/// from Lode Vandevenne's CG tutorial site at
/// http://www.student.kuleuven.ac.be/~m0216922/CG/lineclipping.html

int UD_RoadFollower::clipcode(float x, float y, float xleft, float ytop, float xright, float ybottom)
{
  int code = 0;

  if (y >= ybottom)
    code |= 1; // bottom
  else if (y < ytop)
    code |= 2; // top
  if (x >= xright)
    code |= 4; // right
  else if (x < xleft)
    code |= 8; // left

  return code;
}

//----------------------------------------------------------------------------

/// clip line segment against rectangle using Cohen-Sutherland algorithm and 
/// return length of clipped line.  returns 0 if line segment is entirely 
/// outside rectangle.  most of the code in this function is taken directly 
/// from Lode Vandevenne's CG tutorial site at
/// http://www.student.kuleuven.ac.be/~m0216922/CG/lineclipping.html
/// (modified to allow non-zero values of clip region left and top edges)

float UD_RoadFollower::clip_support_ray(float x1, float y1, float x2, float y2, float *cx1, float *cy1, float *cx2, float *cy2,
				       float xleft, float ytop, float xright, float ybottom)
{
  int code1, code2, outcode, accept, done;
  float x, y;

  accept = done = FALSE;

  code1 = clipcode(x1, y1, xleft, ytop, xright, ybottom); // compute region outcodes for the endpoints
  code2 = clipcode(x2, y2, xleft, ytop, xright, ybottom);

  do {

    // trivial accept: both endpoints are in the rectangle

    if (!(code1 | code2)) 
      accept = done = TRUE;  

    // trivial reject: both endpoints are outside the rectangle on the same side 

    else if (code1 & code2) 
      done = TRUE;
 
    // if no trivial reject or accept, continue the loop
        
    else { 

      outcode = code1 ? code1 : code2;  // pick an endpoint that's outside the rectangle

      if (outcode & 1) {                                    // bottom
	x = x1 + (x2 - x1) * (ybottom - y1) / (y2 - y1);
	y = ybottom - 1;
      }
      else if (outcode & 2) {                               // top
	x = x1 + (x2 - x1) * (ytop - y1) / (y2 - y1);
	y = ytop;
      }
      else if (outcode & 4) {                               // right
	y = y1 + (y2 - y1) * (xright - x1) / (x2 - x1);
	x = xright - 1;
      }
      else {                                                // left
       	y = y1 + (y2 - y1) * (xleft - x1) / (x2 - x1);
	x = xleft;
      }

      if (outcode == code1) {      // first endpoint was clipped
	x1 = x; y1 = y;
	code1 = clipcode(x1, y1, xleft, ytop, xright, ybottom);
      }
      else {                       // second endpoint was clipped
	x2 = x; y2 = y;
	code2 = clipcode(x2, y2, xleft, ytop, xright, ybottom);
      }
    }
  } while (!done);
  
  if (accept) {
    *cx1 = x1; *cy1 = y1;
    *cx2 = x2; *cy2 = y2;
    return hypot(x1-x2, y1-y2);
    //    return sqrt(SQUARE(x1-x2)+SQUARE(y1-y2));  
  }
  else 
    return 0.0;
}

//----------------------------------------------------------------------------

/// compute road region's support.  support is computed	by   
/// examining "rays" radiating down from estimated vanishing point.  on the road,
/// we expect that the dominant orientations will be aligned with these rays.
/// thresholding on the fraction of dominant orientations that are within
/// some angle delta of the ray orientation gives us our "support" rays.

float UD_RoadFollower::compute_support()
{
  int i;
  float num_rays, num_good_rays, sum, dx, dy, udx, udy;

  for (i = 0, sum = num_rays = num_good_rays = dx = dy = 0; i < support_num_orientations; i++) { 

    // clip each support ray against dominant orientation image and make sure it's long enough

    support_ray_long_enough[i] = 
      clip_support_ray(straight_tracker->vp_x(), straight_tracker->vp_y(), 
		       straight_tracker->vp_x() + support_ray_dx[i], straight_tracker->vp_y() + support_ray_dy[i], 
		       &support_ray_vx[i], &support_ray_vy[i], &support_ray_ex[i], &support_ray_ey[i],
		       vp->voter_lateral_margin, vp->voter_top_margin, vp->w - vp->voter_lateral_margin, vp->h - vp->voter_bottom_margin) 
      > support_ray_min_length;

    if (support_ray_long_enough[i]) {
      support_angle_diff[i] = centerline_angle_diff(cvPoint(support_ray_vx[i], support_ray_vy[i]), 
						    cvPoint(support_ray_ex[i], support_ray_ey[i]), &udx, &udy);
      
      if (support_angle_diff[i] < support_angle_diff_thresh) {
	dx += udx;
	dy += udy;
	num_good_rays++;
      }
      sum += support_angle_diff[i];
      num_rays++;
    }
    else
      support_angle_diff[i] = MAX_ANGLE_DIFF;
  }
    
  if (num_good_rays) {
    support_mean_dx = dx / num_good_rays;
    support_mean_dy = dy / num_good_rays;
  }

  return sum / num_rays;
}

//----------------------------------------------------------------------------

/// compute estimated midline of road region as the x coordinate (in pixels) where
/// the centerline from the vanishing point crosses the bottom of the image.  
/// the instantaneous "centerline" is just the mean support ray in this image 
/// [MEDIAN MIGHT BE BETTER].  the estimate which is actually used (and drawn) is 
/// temporally filtered to make its motion smoother

float UD_RoadFollower::compute_centerline()
{
  float t;

  compute_support();

  // crude tracking: move part of the way toward the target each iteration
	
  support_cur_dx += support_alpha * (support_mean_dx - support_cur_dx);
  support_cur_dy += support_alpha * (support_mean_dy - support_cur_dy);

  // make line long enough to span image

  support_lat_dx = support_cur_dx * 2 * (float) h;
  support_lat_dy = support_cur_dy * 2 * (float) h;

  // lateral offset: x-coord. of intersection of ray w/ orig. (vp_x, vp_y), dir. (support_lat_dx, support_lat_dy) with y = h
  // h = vp_y + t * support_lat_dy (when support_lat_dy > 0), lat = vp_x + t * support_lat_dx, where t = (h - vp_y) / support_lat_dy

  if (support_lat_dy > 0) {
    t = (h - straight_tracker->vp_y()) / support_lat_dy;
    return (straight_tracker->vp_x() + t * support_lat_dx);
  }
  else 
    return 99999.0;   // i.e., NaN--shouldn't happen, though
}

//----------------------------------------------------------------------------

/// draw estimated midline of road based on dominant orientation support
/// for vanishing point.  assuming window dimensions are same as
/// candidate region

void UD_RoadFollower::draw_centerline()
{
  glColor3f(1, 0, 1);
  glLineWidth(3);
  
  glBegin(GL_LINES);
  
  glVertex2f(straight_tracker->candidate_vp_x(), ch - straight_tracker->candidate_vp_y());
  glVertex2f(straight_tracker->candidate_vp_x() + support_lat_dx, ch - (straight_tracker->candidate_vp_y() + support_lat_dy));
  
  glEnd();
  
  glLineWidth(1);
}

//----------------------------------------------------------------------------

/// compute mean angle difference [MEDIAN MIGHT BE BETTER] along a line between 
/// the dominant orientations along the line and the line's orientation.  
/// p1 is vanishing point, p2 is point along ray below 

float UD_RoadFollower::centerline_angle_diff(CvPoint p1, CvPoint p2, float *udx, float *udy)
{
  float sum, angle, alpha, dx, dy;
  CvLineIterator iterator;
  int count, i;

  dy = p1.y - p2.y;
  dx = p1.x - p2.x;
  angle = atan2(-dy, dx);   // note negation of dy
  count = cvInitLineIterator(vp->dom->gabor_max_response_index, p1, p2, &iterator, 8);
  *udx = -dx / (float) count;
  *udy = -dy / (float) count;

  for (i = 0, sum = 0; i < count; i++) {
    alpha = DEG2RAD(vp->dom->gabor_delta_orientation) * (float) iterator.ptr[0];
    sum += fabs(angle - alpha);
    CV_NEXT_LINE_POINT(iterator);
  }

  return sum / (float) count;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// sun is at or below horizon

int too_dark(double sunalt)
{
  return (sunalt < DEG2RAD(5.0));
}

//----------------------------------------------------------------------------

// sun is low and in front of us

int possible_glare(double heading, double sunalt, double sunazi)
{
  return ((sunalt < DEG2RAD(30.0)) && (fabs(sunazi - heading) < DEG2RAD(30.0)));
}

//----------------------------------------------------------------------------

// sun is very low and right behind us

int possible_shadow(double heading, double sunalt, double sunazi)
{
  return ((sunalt < DEG2RAD(20.0)) &&                          
	 ((fabs(sunazi - heading + PI) < DEG2RAD(20.0)) ||    
	  (fabs(sunazi - heading - PI) < DEG2RAD(20.0))));    
}

//----------------------------------------------------------------------------

int bad_lighting_conditions(double heading, double sunalt, double sunazi)
{
  return (too_dark(sunalt) || possible_glare(heading, sunalt, sunazi) || possible_shadow(heading, sunalt, sunazi));
}

//----------------------------------------------------------------------------

/// see whether image appears to be of a road or not.  this "instantaneous"
/// decision is temporally filtered for smoothing--we require that most 
/// recent images also be classified similarly 

void UD_RoadFollower::check_onroad()
{
  oo->compute_confidence(vp->candidate_image);
  
  // turn off road following when we think sun is blinding us or casting our own shadow in the road
  // (i.e., low and either right in front or right behind the vehicle)

#ifdef SKYNET

  if (ladarray->num_ladars > 0) {  
    bad_lighting_value = bad_lighting_conditions(ladarray->ladsrc[0]->statesrc->heading,
						 ladarray->ladsrc[0]->statesrc->sunalt,
						 ladarray->ladsrc[0]->statesrc->sunazi);

    if(bad_lighting_value && use_bad_lighting)
      oo->onroad_state = FALSE;
  }

#endif

  if (!oo->onroad_state)
    reset();      // vanishing point particle filter
}

//----------------------------------------------------------------------------

/// write current estimate of vanishing point location to file.
/// always terminated with a newline

void UD_RoadFollower::write(FILE *fp)
{
  straight_tracker->write(fp, ", ");
  fprintf(fp, "%f, %f, %f, %i, %i\n", oo->KL_now, oo->KL_threshold, oo->onroad_confidence, oo->KL_time_window, oo->onroad_state);
}

//----------------------------------------------------------------------------

/// cycle between raw image view, dominant orientations, and vote function
/// as underlying display image

void UD_RoadFollower::cycle_draw_mode_image()
{
  if (!vp->dom->show && !vp->show)
    vp->dom->show = TRUE;
  else if (vp->dom->show && !vp->show) {
    vp->dom->show = FALSE;
    vp->show = TRUE;
  }
  else if (!vp->dom->show && vp->show)
    vp->show = FALSE;
}

//----------------------------------------------------------------------------

/// cycle between nothing, tracker particles, and support rays as
/// overlay on display

void UD_RoadFollower::cycle_draw_mode_overlay()
{
  if (!straight_tracker->show_particles && !show_support)
    straight_tracker->set_show_particles(TRUE);
  else if (straight_tracker->show_particles && !show_support) {
    straight_tracker->set_show_particles(FALSE);
    show_support = TRUE;
  }
  else if (!straight_tracker->show_particles && show_support)
    show_support = FALSE;
}

//----------------------------------------------------------------------------

/// draw road tracker parameters, most importantly the current road vanishing
/// point and centerline estimates.  this also handles the image and 
/// overlay cycling described above.  assuming window dimensions are same as
/// candidate region

void UD_RoadFollower::draw(UD_ImageSource *imsrc)
{
  int i, win_h, win_w;

  // draw image for this drawing mode

  // vote function over whole candidate region

  if (vp->show) {

    glClear(GL_COLOR_BUFFER_BIT);
    glPixelZoom(1, -1);
    glRasterPos2i(0, ch - 2);  // draw image incorrectly one row lower, but it disappears if moved up when h = 160/180 (but not 80/90!)

    glDrawPixels(cw, ch, GL_LUMINANCE, GL_FLOAT, vp->candidate_image->imageData);
  }

  // dominant orientations or input image (shifted into coordinate system of candidate region)

  else {

    glClear(GL_COLOR_BUFFER_BIT);
    glPixelZoom(1, -1);
    glRasterPos2i(-vp->candidate_lateral_margin, ch + vp->candidate_top_margin - 2);

    if (vp->dom->show) 
      glDrawPixels(vp->dom->gabor_max_response_intensity->width, vp->dom->gabor_max_response_intensity->height, 
		   GL_LUMINANCE, GL_UNSIGNED_BYTE, vp->dom->gabor_max_response_intensity->imageData);
    else {
      if (!oo->onroad_state) {   // redden
	glPixelTransferf(GL_RED_SCALE, 1.0);
	glPixelTransferf(GL_GREEN_SCALE, 0.0);
	glPixelTransferf(GL_BLUE_SCALE, 0.0);
      } else {
	glPixelTransferf(GL_RED_SCALE, 1.0);
	glPixelTransferf(GL_GREEN_SCALE, 1.0);
	glPixelTransferf(GL_BLUE_SCALE, 1.0);
      }
      
      glDrawPixels(imsrc->im->width, imsrc->im->height, GL_BGR, GL_UNSIGNED_BYTE, imsrc->im->imageData);
    }
  }

  // overlay

  win_w = cw;
  win_h = ch;

  if (!oo->onroad_state) 
    return;

  // outline voter region

  //  vp->draw_voter_region();

  // vertical line at w/2 - yaw*w/fov
    
  glColor3f(0,1,1);
  glLineWidth(1);
  glBegin(GL_LINES);
  glVertex2f(win_w / 2 - imsrc->im->width * cal->yaw_angle/cal->hfov, 0);
  glVertex2f(win_w / 2 - imsrc->im->width * cal->yaw_angle/cal->hfov, imsrc->im->height);
  glEnd();

  // road support "rays"

  if (show_support) 
    draw_support();

   if (show_centerline) 
     draw_centerline();

  // curvature tracker

  straight_tracker->draw(0, 0, 255);

//   for (i = 0; i < num_curve_pts; i++)
/*
  curve_tracker[0]->draw(255, 0, 0);
  curve_tracker[1]->draw(0, 255, 0);
*/
}

//----------------------------------------------------------------------------

/// reset particle filter and redistribute particles uniformly across 
/// candidate search region in regular grid pattern

void UD_RoadFollower::reset()
{
  straight_tracker->pf_reset();
  
  support_cur_dx = 0;
  support_cur_dy = 1;   
}

//----------------------------------------------------------------------------

/// handle command-line parameters related to road tracker functions

void UD_RoadFollower::process_command_line_flags(int argc, char **argv) 
{
  int i;

  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i],                             "-centerline"))
      show_centerline = TRUE;
    else if (!strcmp(argv[i],                        "-showsupp"))
      show_support = TRUE;
    else if (!strcmp(argv[i],                        "-thresh"))
      support_angle_diff_thresh = atof(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-alpha"))
      support_alpha = atof(argv[i + 1]);
  }

  straight_tracker->process_command_line_flags(argc, argv);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
