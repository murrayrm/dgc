/**
 * ModuleStarter.hh
 * Revision History:
 * Initially written by Thiago
 * 04/06/2005  hbarnor  modified to be OOP
 *                      also added functionality to read 
 *                      command path from config
 */

#ifndef  MODULESTARTER_HH
#define  MODULESTARTER_HH

#include "sn_msg.hh"
#include <string>
#include <cstdlib>
#include <iostream>
#include <list>
#include <sys/wait.h>
#include <fstream>
#include <stdio.h>
using namespace std;

#define BUFSIZE 1024
//#define MAXPROCS 25
/**
 * A struct for the command 
 * sent over skynet
 * type - contains the SNmodname in string format
 * action - K means kill and S means Start the module
 * PID - used to hold the PID of the command when its 
 * spawned
 */
struct command
{
  char type[20];   //name of the command
  char action;     // the action to perform S is start and k is kill 
  pid_t PID;         //modules's PID if the module could be started or if it is already running. 
                   //It is 0 if module could not be started
  int compID;     // unique computer ID
};

/**
 * Struct to hold the 
 * tokens obtained from parsing the config
 * file
 */
struct cmdTokens
{
  string snModName;
  string cmd;
  string opts;
  int pause;
};

#define CONFIG_FILE_NAME "moduleStarter.config"

using namespace std;

#define DEBUG false

const char preModule[] = "Module:";
///const string CONFIG_FILE_NAME = "moduleStarter.config";

// variables
char* cmdFileName; // 
struct cmdTokens execCmd;
skynet* Skynet;
modulename myself = SNmodstart;                            //defining paramters for skynet
modulename other_mod = SNGuimodstart;
sn_msg myinput = SNmodcom;  
sn_msg myoutput = SNGuiMsg;
int msgSendSock;
const int NUM_CHAR = 256;
char message[NUM_CHAR];
list<struct command> myModules;
//struct command myModules[MAXPROCS];

/**
 * Array to hold 
 * a line for command and 
 * its arguments
 */
char cmdLineVal[1024];
/**
 * number of options you can have 
 * + 1
 */
char * my_Argv[50];
const string delim(" ");
//const char delim2 = " ";
/**
 * listens for skynet command and spawns appropriate
 * calls spawn function with the snModuleName if the 
 * message is meant for us
 */
void listen();
/** 
 * spawnModule - calls getPath with the 
 * snModuleName, forks and execs the 
 * returned path
 */
void spawnModule(char* snModuleName);
/**
 * getPath - searches the config file 
 * for the path of the snModuleName 
 * passed to it as a param
 * fill the exeCmd struct with 
 * the path to the cmd and options 
 */
bool getPath(char* snModName);
/**
 * Read the config file 
 * and find out the id of the computer
 */
int verify_configuration(); 
/**
 * 
 */
pair<string::size_type, string::size_type> getWord(string line);
/**
 * read a line of text
 */
int getLine(char* line, int max, FILE *fp);
/**
 * spawn - forks and execs the commands in the updated 
 * execCmd struct
 */
pid_t spawn(char* snModuleName);

/**
 * stopModule - kills the module passed
 */
void stopModule(char* snModuleName);
/**
 * sends a message to skynetGui
 *
 */
void sendToGui();

/**
* TODO 
*/
void parsePath();

#endif  
//MODULESTARTER_HH  
