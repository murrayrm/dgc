#include "ModuleStarter.hh"

const string space_delim(" ");
const string eol_delim("\0");

pair<string::size_type, string::size_type> getWord(string line, string delim)
{
  string::size_type begIdx = line.find_first_not_of(delim);
  string:: size_type endIdx;
  if(begIdx != string::npos)
    {
      endIdx = line.find_first_of(delim,begIdx);
      if(endIdx == string::npos)
	{
	  endIdx = line.length();
	}	
    }
  return pair<string::size_type, string::size_type>(begIdx, endIdx);
}

bool getPath(char* snModName)
{
  string line;
  //begin cplusplussifying io
  std::ifstream configfile(CONFIG_FILE_NAME);

  /**
   * i don't like this code, its ugly
   * wish we had adecent tokenizer in the stl
   */
  if (! configfile )
    { 
      cerr << "Error opening " <<  CONFIG_FILE_NAME << " file. \n Make sure its in the same directory as the executable" << endl; 
      exit (1); 
    }
  pair<string::size_type, string::size_type> word;
  while (getline(configfile, line))
    {
      //cout << "here " << !line.empty() << "  " << (line[0] != '#') << endl;
      if((!line.empty()) && (line[0] != '#')) // # at beginning of line denotes comment
	{	 
	  //  cout << "we have valid file" << endl;
	  word = getWord(line, space_delim);
	  //cout << "Word is: " << line.substr(word.first,word.second) << endl;
	  if(line.substr(word.first,word.second) == preModule)
	    {
	      //cout << "found Module" << endl;
	      //now find module name
	      //cout << "Line is: " << line << endl;
	      line.erase(word.first, (word.second-word.first));
	      //cout << "Line is now : " << line << endl;
	      word = getWord(line,space_delim);
	      if(snModName == line.substr(word.first,(word.second-word.first)))
		{
		  cout << " Found: " << line.substr(word.first,(word.second-word.first)) << endl;		  
		  line.erase(word.first, (word.second-word.first));
		  word = getWord(line,space_delim);
		  cout << "Computer is " <<   line.substr(word.first,(word.second-word.first)) << endl;
		  line.erase(word.first, (word.second-word.first));
		  word = getWord(line,space_delim);
		  cout << "Checked is " <<   line.substr(word.first,(word.second-word.first)) << endl;
		  line.erase(word.first, (word.second-word.first));
		  word = getWord(line,space_delim);
		  cout << "Pause is " <<   line.substr(word.first,(word.second-word.first)) << endl;
		  line.erase(word.first, (word.second-word.first));
		  word = getWord(line, space_delim);
		  cout << "Command is " <<   line.substr(word.first,(word.second-word.first)) << endl;
		  line.erase(word.first, (word.second-word.first));
		  string::size_type begIdx = line.find_first_of(space_delim);
		  string::size_type endIdx = line.find_first_not_of(space_delim);
		  line.erase(begIdx, endIdx-begIdx);
		  cout << "Options is " << line << endl;;
		  cout << "WHOOOOPPPPEEE" <<endl;
		}
	    }
	}      
    }
}

int main(int argc, char *argv[])
  {
    getPath("SNasim");
    getPath("SNtrajfollower");
    getPath("SNfusionmapper");
    getPath("SNplannermodule");
    getPath("SNRddfPathGen");
    getPath("MODreactive");
    getPath("SNladarfeeder");
    return 0;
  }
