#include <stdlib.h>
#include <stdio.h>

#include "fusionMapper.hh"
#include <DGCutils>

#ifdef MACOSX
/* Install libgnugetopt for getopt_long_only support in Mac OS X */
#include <gnugetopt/getopt.h>
#else
#include <getopt.h>
#endif

enum {
  OPT_NONE,
  OPT_HELP,
  OPT_SPARROW,
  OPT_WAIT,
  OPT_LADAR,
  OPT_STEREO,
  OPT_SNKEY,
  OPT_AVERAGE,
  OPT_MAX, 
  OPT_CONFIG,
  OPT_STATIC,
  OPT_ROAD,
	OPT_SUPERCON,
	OPT_ESTOP,
	OPT_BENCHMARK,

  NUM_OPTS
};


void printFusionMapperHelp();  
void benchmarkFusionMapper();

int main(int argc, char** argv) {
  //Initialize options to be passed to the FusionMapper object
  FusionMapperOptions mapperOpts;
  mapperOpts.optLadar = 1;
  mapperOpts.optStereo = 0;
  mapperOpts.optStatic = 0;
  mapperOpts.optRoad = 0;
	mapperOpts.optSupercon = 1;
	int optEStop = 1;

  mapperOpts.optSNKey = -1;
  mapperOpts.optAverage = 1;
  mapperOpts.optMax = 0;
  mapperOpts.corridorOpts.optRDDFscaling = 1.0;
  mapperOpts.corridorOpts.optMaxSpeed = 1.0;
  mapperOpts.corridorOpts.paintCenterTrackline = CCorridorPainterOptions::DONT;
  mapperOpts.corridorOpts.whatToAdjust = CCorridorPainterOptions::CENTER_TRACKLINE;
  mapperOpts.corridorOpts.adjustmentVal = 1.0;
  mapperOpts.optMaxSpeed = 10.0;
  mapperOpts.optNonBinary = 1;
  mapperOpts.optZeroGradSpeedAsRDDFPercentage = 0;
  mapperOpts.optZeroGradSpeed = 5;
  mapperOpts.optUnpassableObsHeight = 0.8;
  mapperOpts.optUseAreaGradient = 0;
  mapperOpts.optObstacleDistanceLimit = 999;
  mapperOpts.optDistantObstacleSpeed = 2;
  mapperOpts.optStaticScaling = .28;
  mapperOpts.optRoadScaling = .28;
  mapperOpts.optVeloGenerationAlg = 3;
  mapperOpts.optVeloTweak1 = .3 ;
  mapperOpts.optVeloTweak2 =  4;
  mapperOpts.optAreaGradScaling = .85;
  mapperOpts.optStdDevWeight= 0;
  mapperOpts.optStdDevScaling = 1.0;


  mapperOpts.stdDevThreshold = 0.15;
  mapperOpts.kernelSize = 2;
	mapperOpts.sigma = 1.0;
	mapperOpts.maxInterpolateCells = 1;
	mapperOpts.minNumSupportingCells = 3;
	mapperOpts.minNumSupportingCellsForInterpolation = 5;

  int optWaitForState = 1;

  sprintf(mapperOpts.configFilename, "config/fusionMapperTuning.dat");
  //Initialize local options
  int optSparrow = 1;
  int optWait = -1;

  int c;


  static struct option long_options[] = {
    //Options that don't require arguments
    {"nosparrow", no_argument, &optSparrow, 0},
    {"help",      no_argument, 0, OPT_HELP},
    {"nowait", no_argument, &optWait, 0},
    {"noladar", no_argument, &mapperOpts.optLadar, 0},
    {"nostereo", no_argument, &mapperOpts.optStereo, 0},    
    {"binary", no_argument, &mapperOpts.optNonBinary, 0},
		{"noestop", no_argument, &optEStop, 0},
		{"benchmark", no_argument, 0, OPT_BENCHMARK},
    //Options that require arguments
    {"snkey", required_argument, 0, OPT_SNKEY},
    {"skynetkey", required_argument, 0, OPT_SNKEY},
    {"config", required_argument, 0, OPT_CONFIG},
    //Options that have optional arguments
    {"wait", optional_argument, 0, OPT_WAIT},
    {"ladar", optional_argument, 0, OPT_LADAR},
    {"stereo", optional_argument, 0, OPT_STEREO},
    {"road", optional_argument, 0, OPT_ROAD},
    {"static", optional_argument, 0, OPT_STATIC},
		{"nosupercon", optional_argument, 0, OPT_SUPERCON},
		{"estop", optional_argument, 0, OPT_ESTOP},
    {"sparrow", optional_argument, 0, OPT_SPARROW},
    {"average", optional_argument, 0, OPT_AVERAGE},
    {"max", optional_argument, 0, OPT_MAX},
    {0,0,0,0}
  };
 

  while (1) {
    int option_index = 0;
    
    c = getopt_long_only(argc, argv, "", long_options, &option_index);
    if(c == -1) break;

    switch(c) {
    case '?':
    case OPT_HELP:
      printFusionMapperHelp();
      exit(1);
      break;
		case OPT_BENCHMARK:
			benchmarkFusionMapper();
			exit(1);
			break;
    case OPT_SNKEY:
      mapperOpts.optSNKey = atoi(optarg);
      break;
    case OPT_WAIT:
      if(optarg != NULL) {
				optWait=atoi(optarg);
      } else {
				optWait = 7;
      }
      break;
    case OPT_LADAR:
      if(optarg!=NULL) {
				mapperOpts.optLadar = atoi(optarg);
      } else {
				mapperOpts.optLadar = 1;
      }
      break;
    case OPT_STEREO:
      if(optarg!=NULL) {
				mapperOpts.optStereo = atoi(optarg);
      } else {
				mapperOpts.optStereo = 1;
      }
      break;
    case OPT_STATIC:
      if(optarg!=NULL) {
				mapperOpts.optStatic = atoi(optarg);
      } else {
				mapperOpts.optStatic = 1;
      }
      break;
    case OPT_ROAD:
      if(optarg!=NULL) {
				mapperOpts.optRoad = atoi(optarg);
      } else {
				mapperOpts.optRoad = 1;
      }
      break;
		case OPT_SUPERCON:
			if(optarg!=NULL) {
				mapperOpts.optSupercon = atoi(optarg);
			} else {
				mapperOpts.optSupercon = 0;
			}
			break;
		case OPT_ESTOP:
			if(optarg!=NULL) {
				optEStop = atoi(optarg);
			} else {
				optEStop = 1;
			}
			break;
    case OPT_AVERAGE:
      if(optarg!=NULL) {
				mapperOpts.optAverage = atoi(optarg);
      } else {
				mapperOpts.optAverage = 1;
      }
      break;
    case OPT_MAX:
      if(optarg!=NULL) {
				mapperOpts.optMax = atoi(optarg);
      } else {
				mapperOpts.optMax = 1;
      }
      break;
    case OPT_CONFIG:
      sprintf(mapperOpts.configFilename, "%s", optarg);
      break;
    default:
      if(c!=0) {
				printf("Unknown option %d!\n", c);
				printFusionMapperHelp();
				exit(1);
      }
    }
  }

  char* ptrSkynetKey = getenv("SKYNET_KEY");
  
  if(ptrSkynetKey == NULL && mapperOpts.optSNKey==-1) {
    printf("You have not specified a skynet key as a command-line argument (using --snkey)\n");
    printf("or as an environment variable.  To set a skynet ket, try doing:\n");
    printf("$> export SKYNET_KEY=X\n");
    printf("where X is some number.  To check your skynet key, do:\n");
    printf("$> echo $SKYNET_KEY\n");
    return 0;
  } else {
    printf("Welcome to FusionMapper\n");
    if(mapperOpts.optSNKey==-1) {
      mapperOpts.optSNKey = atoi(ptrSkynetKey);
    }
   
    printf("Option Summary - General:\n");
    printf("SkynetKey      - %d\n", mapperOpts.optSNKey);
    printf("Sparrow        - %d\n", optSparrow);
    printf("Using Ladar    - %d\n", mapperOpts.optLadar);
    printf("Using Stereo   - %d\n", mapperOpts.optStereo);
    printf("Averaging      - %d\n", mapperOpts.optAverage);
    printf("Max            - %d\n", mapperOpts.optMax);
    printf("Nonbinary maps - %d\n", mapperOpts.optNonBinary);
    printf("Using Static   - %d\n", mapperOpts.optStatic);
    printf("Using Road     - %d\n", mapperOpts.optRoad);
		printf("Using Supercon - %d\n", mapperOpts.optSupercon);
		printf("Using EStop    - %d\n", optEStop);

    printf(" == HIT CTRL+C TO CANCEL ==\n");
    char temp;
    if(optWait>=0) {
      for(int i=0; i<optWait; i++) {
				printf("Program will start in %d seconds...\r", optWait-i);
				fflush(stdout);
				sleep(1);
      }
      if(optWait==0) {
				optWaitForState = 0;
      }
    } else if(optWait==-1) {
      printf("Please review the options you have set.  Do you want to continue (y/n)? ");      
      temp = getchar();
      if(temp!='y' && temp!='Y') {
				printf("You have entered something other than 'y' - quitting...\n");
				return 0;
      }
    }


    FusionMapper fusionMapperObj(mapperOpts.optSNKey, mapperOpts, optWaitForState);

    DGCstartMemberFunctionThread(&fusionMapperObj, &FusionMapper::ReceiveDataThread_LadarRoof);
    DGCstartMemberFunctionThread(&fusionMapperObj, &FusionMapper::ReceiveDataThread_LadarBumper);
    DGCstartMemberFunctionThread(&fusionMapperObj, &FusionMapper::ReceiveDataThread_LadarRiegl);
    DGCstartMemberFunctionThread(&fusionMapperObj, &FusionMapper::ReceiveDataThread_LadarSmall);
    DGCstartMemberFunctionThread(&fusionMapperObj, &FusionMapper::PlannerListenerThread);
    //    DGCstartMemberFunctionThread(&fusionMapperObj, &FusionMapper::ReceiveDataThread_Stereo);
    DGCstartMemberFunctionThread(&fusionMapperObj, &FusionMapper::ReceiveDataThread_Road);
   
    DGCstartMemberFunctionThread(&fusionMapperObj, &FusionMapper::ReceiveDataThread_Supercon);
    
    //DGCstartMemberFunctionThread(&fusionMapperObj, &FusionMapper::SendDataThread);

		if(optEStop) {
			DGCstartMemberFunctionThread(&fusionMapperObj, &FusionMapper::ReceiveDataThread_EStop);
		}

    if(optSparrow) {
      DGCstartMemberFunctionThread(&fusionMapperObj, &FusionMapper::SparrowDisplayLoop);
      DGCstartMemberFunctionThread(&fusionMapperObj, &FusionMapper::UpdateSparrowVariablesLoop);
    }

    fusionMapperObj.ActiveLoop();
  }

  return 0;
}


void printFusionMapperHelp() {
  printf("Usage: fusionMapper [OPTION]\n");
  printf("Runs the Team Caltech DGC fusionMapper software\n\n");

  printf("  --sparrow[=N]       Sets whether or not to show the sparrow display (w/out argument shows)\n");
  printf("  --nosparrow         Does not show the sparrow display\n");
  printf("  --help              Prints this help message\n");
  printf("  --wait[=N]          Sets how long to wait before starting (w/out argument is 7 sec)\n");
  printf("  --nowait            Disables a wait before the program begins\n");
  printf("  --ladar             Sets whether or not to use ladar (w/out argument is true)\n");
  printf("                      fusionMapper runs with ladar by default\n");
  printf("  --noladar           Disables using ladar\n");
  printf("  --stereo            Sets whether or not to use stereo (w/out argument is true)\n");
  printf("                      fusionMapper runs without stereo by default\n");
  printf("  --nostereo          Disables using stereo\n");
  printf("  --static            Sets whether or not to use static (w/out argument is true)\n");
  printf("                      fusionMapper runs without static by default\n");
  printf("  --binary            Make the maps binary, default is nonbinary\n");
  printf("  --config filename   Use a specific aggressiveness tuning file on startup\n");
  printf("\n");
  printf("If you need to know the format of the aggressiveness tuning file, run fusionMapper\n");
  printf("with --config some_file_that_doesnt_exist.  It will make the file for you, which you\n");
  printf("can then edit.\n");
  printf("\n");
  printf("FusionMapper Sparrow Display Hot Keys:\n");
  printf("  Q,q                 Quits FusionMapper\n");
  printf("  P,p                 Pauses fusionMapper from processing incoming data\n");
  printf("  R,r                 Resets fusionMapper's counters and average speed estimates\n");
  printf("  S,s                 Saves all current map layers to files\n");
  printf("  A,a                 Causes fusionMapper to resend the current map as a big delta\n");
  printf("                      (Note that this doesn't involve any re-processing or re-evaluating)\n");
  printf("  E,e                 Causes fusionMapper to re-evaluate the entire current map and then send\n");
  printf("                      it, using the current painting settings.\n");
	printf("\n");
	printf("FusionMapper Sparrow Display Tweaking:\n");
	printf("To adjust the default speed of an RDDF without any sensors, change the RDDF No Data Speed.\n");
	printf("To adjust the max speed of an RDDF with sensors, change the Zero Gradient speed\n");
  printf("\n");
}

void benchmarkFusionMapper() {
	cout << __FILE__ << "[" << __LINE__ << "]: " 
			 << "Benchmarking...please wait" << endl;
	cout << __FILE__ << "[" << __LINE__ << "]: " 
			 << "Initializing variables..." << endl;
	CMapPlus sourceMap;
	CMapPlus destMap;

	sourceMap.initMap(CONFIG_FILE_DEFAULT_MAP);
	destMap.initMap(CONFIG_FILE_DEFAULT_MAP);

	int sourceCostLayer = sourceMap.addLayer<double>(CONFIG_FILE_COST, true);
	int destCostLayer = destMap.addLayer<double>(CONFIG_FILE_COST, false);

	int numRepeats = 100;
	int numCells = sourceMap.getNumRows()*sourceMap.getNumCols()/50;
	int row, col;
	double val;

	char sourceNameMemCpy[256];
	char destNameMemCpy[256];
	char sourceNameDelta[256];
	char destNameDelta[256];

	int cellsize;
	double UTMNorthing, UTMEasting, resultCell;

	fstream logFile;
	logFile.open("fusionMapperBenchmark.log", fstream::out);

	cout << __FILE__ << "[" << __LINE__ << "]: " 
			 << "Running benchmark (this could take a few minutes, please wait)..." << endl;

	for(int multiplier=4; multiplier>=1; multiplier--) {
		for(int digit=9; digit >= 1; digit--) {
			numCells = digit*(int)pow((double)10, (double)multiplier);
			unsigned long long startMemCpy, endMemCpy, fillMemCpy, totalMemCpy = 0ULL, totalFillMemCpy = 0ULL;
			unsigned long long startDelta, endDelta, fillDelta, totalDelta = 0ULL, totalFillDelta = 0ULL, resetDelta, totalResetDelta = 0ULL;
			cout << __FILE__ << "[" << __LINE__ << "]: "
					 << "Testing with " << numCells << " cells" << endl;

			for(int i=0; i<numRepeats; i++) {
				DGCgettime(startMemCpy);
				for(int j=0; j<numCells; j++) {
					row = (int)(((double)rand())/((double)RAND_MAX)*((double)sourceMap.getNumRows()));
					col = (int)(((double)rand())/((double)RAND_MAX)*((double)sourceMap.getNumCols()));
					val = ((double)rand())/((double)RAND_MAX);
					//cout << "Setting " << row << ", " << col << " to " << val << " " <<  << endl;
					sourceMap.setDataWin<double>(sourceCostLayer, row, col, val);
				}
				DGCgettime(fillMemCpy);
				destMap.copyLayer<double>(&sourceMap, sourceCostLayer, destCostLayer, 0);
				DGCgettime(endMemCpy);
				totalFillMemCpy+=(fillMemCpy-startMemCpy);
				totalMemCpy+=(endMemCpy-startMemCpy);
				// 		sprintf(sourceNameMemCpy, "source_mem_%.3d", i);
				// 		sprintf(destNameMemCpy, "dest_mem_%.3d", i) ;
				// 		sourceMap.saveLayer<double>(sourceCostLayer, sourceNameMemCpy, true);
				// 		destMap.saveLayer<double>(destCostLayer, destNameMemCpy, true);
				// 		sourceMap.saveLayer<double>(sourceCostLayer, sourceNameMemCpy, false);
				// 		destMap.saveLayer<double>(destCostLayer, destNameMemCpy, false);

				DGCgettime(startDelta);
				for(int j=0; j<numCells; j++) {
					row = (int)(((double)rand())/((double)RAND_MAX)*((double)sourceMap.getNumRows()));
					col = (int)(((double)rand())/((double)RAND_MAX)*((double)sourceMap.getNumCols()));
					val = ((double)rand())/((double)RAND_MAX);
					sourceMap.setDataWin_Delta<double>(sourceCostLayer, row, col, val);
				}
				DGCgettime(fillDelta);
				cellsize = sourceMap.getCurrentDeltaSize(sourceCostLayer);
				for(int k=0; k<cellsize; k++) {
					resultCell = sourceMap.getCurrentDeltaVal<double>(k, sourceCostLayer, &UTMNorthing, &UTMEasting);				
					destMap.setDataUTM<double>(destCostLayer, UTMNorthing, UTMEasting, resultCell);
				}
				DGCgettime(endDelta);
				sourceMap.resetDelta<double>(sourceCostLayer);
				DGCgettime(resetDelta);
				totalFillDelta+=(fillDelta - startDelta);
				totalResetDelta += (resetDelta-endDelta);
				totalDelta+=(resetDelta-startDelta);

				// 		sprintf(sourceNameDelta, "source_delta_%.3d", i);
				// 		sprintf(destNameDelta, "dest_delta_%.3d", i) ;
				// 		sourceMap.saveLayer<double>(sourceCostLayer, sourceNameDelta, true);
				// 		destMap.saveLayer<double>(destCostLayer, destNameDelta, true);
				// 		sourceMap.saveLayer<double>(sourceCostLayer, sourceNameDelta, false);
				// 		destMap.saveLayer<double>(destCostLayer, destNameDelta, false);
			}

			cout << __FILE__ << "[" << __LINE__ << "]: " 
					 << "Benchmarking complete with " << numRepeats << " repititions and " << numCells << " cells per repitition: " << endl
					 << "Memcopy: " << endl
					 << "Total (in us): " << totalMemCpy << ", mean: " << totalMemCpy/((unsigned long long)numRepeats) << endl
					 << "Filling took " << totalFillMemCpy << ", mean: " << totalFillMemCpy/((unsigned long long)numRepeats) << endl
					 << "Copying took " << totalMemCpy-totalFillMemCpy << ", mean: " << (totalMemCpy-totalFillMemCpy)/((unsigned long long)numRepeats) << endl
					 << "Delta: " << endl
					 << "Total (in us): " << totalDelta << ", mean: " << totalDelta/((unsigned long long)numRepeats) << endl
					 << "Filling took " << totalFillDelta << ", mean: " << totalFillDelta/((unsigned long long)numRepeats) << endl
					 << "Clearing took " << totalResetDelta << ", mean: " << totalResetDelta/((unsigned long long)numRepeats) << endl
					 << "Copying took " << totalDelta-totalFillDelta-totalResetDelta << ", mean: " << (totalDelta-totalFillDelta-totalResetDelta)/((unsigned long long)numRepeats) << endl;

			logFile << numCells << " " 
							<< totalMemCpy/((unsigned long long)numRepeats) << " "
							<< totalFillMemCpy/((unsigned long long)numRepeats) << " "
							<< (totalMemCpy-totalFillMemCpy)/((unsigned long long)numRepeats) << " "
							<< totalDelta/((unsigned long long)numRepeats) << " "
							<< totalFillDelta/((unsigned long long)numRepeats) << " "
							<< totalResetDelta/((unsigned long long)numRepeats) << " "
							<< (totalDelta-totalFillDelta-totalResetDelta)/((unsigned long long)numRepeats) << " "
							<< endl;
		}
	}

	cout << "A log file has been written to fusionMapperBenchmark.log in this directory.  The format is" << endl
			 << "numCells Memcpy FillMemcpy copyMemcpy totalDelta FillDelta resetDelta copyDelta" << endl
			 << "To interpret the logfile, please use the script at:" << endl
			 << "http://grandchallenge.caltech.edu/wiki/index.php/Image:Plot_benchmark_results.m" << endl;

	logFile.close();
}
