#!/bin/bash

# whether or not we want to keep processing the file after we fail
# 0 = don't continue
# 1 = continue anyway
export continue_after_failure=0

export header="   "
export num_failures=0

# make sure they entered one argument
if (( $# != 2 )) ; then
    echo "Description:"
    echo "  This script uses the todo_timber file (output by the timber module) to"
    echo "  grab all the logs that have been generated on the various computers."
    echo "  Please do not put the logs in the same spot in which they were originally"
    echo "  created - this will cause problems.  Right now, all logs are created in"
    echo "   /tmp/logs/, so don't put the logs there when we collect them."
    echo "  For more info, see the comments at the top of the script."
    echo 
    echo "Usage:"
    echo "  $0 computer-to-ssh-to where-to-put-logs"
    echo 
    echo "Example:"
    echo "  If we were running the timber module on skynet 7, and wanted"
    echo "  to put the logs from the race machines and development cluster"
    echo "  onto skynet2, in the /tmp/test_logs folder, we would run"
    echo
    echo "  user@skynet7$ $0 skynet2 /tmp/test_logs/"
    echo
    echo "PLEASE MAKE SURE TO PUT THE \"/\" ON THE END OF THE DIRECTORY!"
    exit 1
fi


if [ 0 == 0 ]; then
    export DEST_DIR=$2
#    export ME=`whoami`
    DEST_COMP=$1
# ok, finally found which grep will remove all teh commented parts of a file!!
# you have NO IDEA how long i've wanted to find this... but it always eluded me
# but now, i seem to haev gotten luck, cuz i still don't *really* see why it
# prints out the first half of the line instead of the second....but ...cool
#################################
#      egrep -o [^\#]\*         #
#################################
    
# make a temp file
#    tempfile=`mktemp /tmp/timber-todo.XXXXXX`

    echo "------------------------------------------------"
    echo "destination directory is (better end with a / !!!):"
    echo $DEST_DIR
    echo 
    
    echo "destination computer is:"
    echo $DEST_COMP
    echo "------------------------------------------------"

    if [ "$DEST_DIR" == "/tmp/logs/" ]; then
	echo "siigh...don't be silly!"
	echo "you can't use /tmp/logs/ as a location."
	echo "Run $0 with no args for more info"
	exit 1
    fi
    
#    echo "running commands in interactive mode, type \'y\' ENTER to actually run the command"
    echo

    export N=`cat todo_timber | egrep -o [^\#]\* | sed -n 'G; s/\n/&&/; /^\([ -~]*\n\).*\n\1/d; s/\n//; h; P' | wc -l`
    echo -e "\033[01;32mRead $N unique lines from todo_timber.\033[00;00m"    
    for i in `seq 1 $N`; do
	export line=`cat todo_timber | egrep -o [^\#]\* | sed -n 'G; s/\n/&&/; /^\([ -~]*\n\).*\n\1/d; s/\n//; h; P' | sed -n "${i}p"`
	echo -e "\033[01;34mProcessing line $i\033[00;00m: \"$line\""
	if [ "$line" == "" ]; then
	    echo 
	    echo "Done processing todo_timber file."
	    break
	fi
	
	# copy the file
	echo -n "${header}copying directory... "
	export line_processed=0
	echo $line \
	    | gawk "{print \"\\\"\" \"mkdir -p $DEST_DIR\" \$2 \" && cd $DEST_DIR\" \$2 \" && cd .. && scp -r \" \$1\$2 \" .\\\"\"}" \
	    | xargs -i ssh $DEST_COMP '{}' && export line_processed=1
	if test $line_processed -eq 0; then
	    echo -e "\033[01;31mfailed!\033[00;00m"
	    export num_failures=$(($num_failures + 1))
	else
	    echo -e "\033[01;32mdone.\033[00;00m"
	fi

	if test \( $line_processed -eq 0 \) -a \( $continue_after_failure -eq 0 \); then
	    break;
	fi

	# remove the file in the remote location
	echo -n "${header}removing remote log directory... "
	export line_processed_2=0
	if test $line_processed -eq 0; then
	    echo -e "\033[01;31mskipped!\033[00;00m"
	else
	    export ssh_comp=`echo $line | gawk '{print $1}' | grep -o ^.*: | cut -d : -f 1`
	    export dir_to_remove=`echo $line | cut -d : -f 2 | gawk '{print $1 $2}'`
	    ssh $ssh_comp "rm -rf $dir_to_remove" && export line_processed_2=1
	    
	    if test $line_processed_2 -eq 0; then
		export num_failures=$(($num_failures + 1))
		echo -e "\033[01;31mfailed!\033[00;00m"
	    else
		echo -e "\033[01;32mdone.\033[00;00m"
	    fi
	fi
	
	if test \( $line_processed_2 -eq 0 \) -a \( $continue_after_failure -eq 0 \); then
	    break;
	fi
    done ## end of for loop

    if test $num_failures -gt 0; then
	echo -e "\033[01;31mThere were $num_failures failures!\033[00;00m"
	echo -e "\033[01;31mPlease comment out all the lines in todo_timber that succeeded before attempting to re-run this script.\033[00;00m"
    else
	echo -e "\033[01;32mScript finished.\033[00;00m"
    fi
fi





############################
# IGNORE ALL THIS CRAP
############################

#scp jason@192.168.0.34:/home/jason/timber/logs/2005_06_03/22_54_26/rddfPathGen/ jason@192.168.0.52:/donkey/logs/2005_06_03/22_54_26/rddfPathGen/

if [ 1 == 0 ] ; then
    ssh skynet2 "mkdir -p /tmp/logs/2005_06_03/22_54_26/rddfPathGen/ && \
cd /tmp/logs/2005_06_03/22_54_26/rddfPathGen/ && \
cd .. && \
scp -r jason@192.168.0.34:/home/jason/timber/logs/2005_06_03/22_54_26/rddfPathGen/ ."
fi

if [ 1 == 0 ]; then
    ssh jason@192.168.0.34 tar -cf - /home/jason/timber/logs/2005_06_03/22_54_26/rddfPathGen/ | tar -xf - -C /tmp/logs/2005_06_03/22_54_26/rddfPathGen/
fi


if [ 1 == 0 ]; then
    for line in $(cat todo)  # Concatenate the converted files.
                         # Uses wild card in variable list.
      do
      echo $line;
    done
fi


#cat todo_timber | egrep -o .*[\#]
#gives before comemnts, with # at end

# works (for soommmem reason....?)
# basically, this gets rid of comments
# cat todo_timber | egrep -o [^\#]\*
