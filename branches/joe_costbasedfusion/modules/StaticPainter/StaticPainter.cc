#include <StaticPainter.hh>
#define CM_OK 0
#define RDDF_LIST "rddflist.dat"
#define MAX_RDDF_FILE_PATH_CHARS 512

// Default constructor
StaticPainter::StaticPainter() : CSkynetContainer(MODstaticpainter, 0), CTimberClient(timber_types::StaticPainter) {
  GPSmap = NULL;
  GPStrace = NULL;
  skynetKey = 0;
  GPSpainter = new CTrajPainter;
}

// Constructor with specified key
StaticPainter::StaticPainter(int key) : CSkynetContainer(MODstaticpainter, key), CTimberClient(timber_types::StaticPainter) {
  GPSmap = NULL;
  GPStrace = NULL;
  skynetKey = key;
  GPSpainter = new CTrajPainter;
}

// Destructor
StaticPainter::~StaticPainter() {
  // Runs through the RDDFnode linked-list structure, releasing the allocated memory.
  RDDFnode *temptrace;
  while(GPStrace != NULL) {
    temptrace = GPStrace->next;
    free(GPStrace);
    GPStrace = temptrace;
  }
  delete GPSmap;
  delete GPSpainter;
  cout << "This module will self-destruct...well, now." << endl;
}

// Initializes the staticPainter object--run this before any other
// functions.
int StaticPainter::Initialize(CMap *cmap, int layer) {
  // Assign the map and layer to the staticPainter object.
  if (cmap == NULL) {
    GPSmap = new CMap;
    GPSmap->initMap(CONFIG_FILE_DEFAULT_MAP);
    /*
    GPSmap->initMap(0,0,FUSIONMAPPER_NUMROWSCOLS,FUSIONMAPPER_NUMROWSCOLS,
                 FUSIONMAPPER_RESROWSCOLS,FUSIONMAPPER_RESROWSCOLS,0);
    */
  } else {
    GPSmap = cmap;
  }

  if (layer == -1) {
    //GPSlayer = GPSmap->addLayer<double>(0, -1, true);
    GPSlayer = GPSmap->addLayer<double>(CONFIG_FILE_STATIC, true);
  } else {
    GPSlayer = layer;
  }
  cout << "GPSlayer: " << GPSlayer << endl;

  /* When a CMap is created, it has all of these values already.
     We just print them out for double-checking. */
  numRows = GPSmap->getNumRows();
  cout << "numRows: " << numRows << endl;
  numCols = GPSmap->getNumCols();
  cout << "numCols: " << numCols << endl;
  resRows = GPSmap->getResRows();
  cout << "resRows: " << resRows << endl;
  resCols = GPSmap->getResCols();
  cout << "resCols: " << resCols << endl;

  // We'll begin by setting the blurWidth to six. Yes, it's a magic number.
  // Fix it later, if it doesn't work out.
  // As a note; this may eventually go to the standard deviation of a 
  // probabilistic distribution--dependent on the results of bug 1891.
  blurWidth = 6;
  cout << "blurWidth: " << blurWidth << endl;

  // Sets up to send/receive over skynet for sending deltas to
  // fusionMapper.
  socket_num = m_skynet.get_send_sock(SNstaticdeltamap);
  return 0;
}

int StaticPainter::InitializeRDDF(char *filename) {
  if (filename == NULL) {
    // Opens each of the files with predriven roads, and reads them into
    // an RDDF object.  These are linked together in the RDDFnode struct.
    // These are read out of the file RDDF_LIST, which is by default 
    // a file called rddflist.dat.
    char rddffile[MAX_RDDF_FILE_PATH_CHARS];
    ifstream file(RDDF_LIST);
    while(!file.eof()) {
      file.getline(rddffile, MAX_RDDF_FILE_PATH_CHARS);
      static RDDF rddf(rddffile);
      LoadRDDF(&rddf);
    }
  } else {
    static RDDF rddf(filename);
    LoadRDDF(&rddf);
  }
  return 0;
}

int StaticPainter::PaintDelta() {
  GPSpainter->PaintTraj(GPSmap, GPSlayer, &sTraj, blurWidth, true);
  return 0;
}

// Gets the socket for traj transmission
void StaticPainter::makeStaticTrajSocket() {
  trajSocket = m_skynet.get_send_sock(SNtraj);
}

int StaticPainter::RDDFtoTraj(RDDF *rddf, CTraj *traj, NEcoord loc, int optC2, double chopBehind, double chopAhead) {
  NEcoord tempLocA, tempLocB;
  double distBehind = 0;
  double distAhead = 0;
  double dist;
  int i,j;
  double n[3], e[3];
    
  int currentWaypoint = rddf->getCurrentWaypointNumberFirst(loc);

  // Work back until we've reached our "chop-behind".
  tempLocA = rddf->getWaypoint(currentWaypoint);
  for(i = currentWaypoint - 1; ((i >= 0) && (distBehind < chopBehind)); i--) {
    tempLocB = rddf->getWaypoint(i);
    distBehind += distance(tempLocA, tempLocB);
    tempLocA = tempLocB;
  }

  if (i < 0) {i = 0;}
  // And then start reading in traj points.
  traj->startDataInput();
  for(j = i; j < currentWaypoint; j++) {
    tempLocB = rddf->getWaypoint(j);
    tempLocA = rddf->getWaypoint(j+1);

    dist = distance(tempLocA, tempLocB);
    if (dist == 0) {
      continue;
    }

    n[0] = tempLocB.N;
    n[1] = rddf->getWaypointSpeed(j)*(tempLocA.N-tempLocB.N)/dist;
    e[0] = tempLocB.E;
    e[1] = rddf->getWaypointSpeed(j)*(tempLocA.E-tempLocB.E)/dist;
      
    // Let the feedback handle this, unless we c2-ify.
    n[2] = 0;
    e[2] = 0;
 

    traj->inputWithDiffs(n, e);
  }

  // Now that we have all the chop-behind, read in the chop-ahead
  // until finished.
  tempLocB = rddf->getWaypoint(currentWaypoint);
  for(i = currentWaypoint + 1; ((i < rddf->getNumTargetPoints()) && (distAhead < chopAhead)); i++) {
    tempLocA = rddf->getWaypoint(i);
    dist = distance(tempLocA, tempLocB);
    distAhead += dist;

    if (dist == 0) {
      continue;
    }

    n[0] = tempLocB.N;
    n[1] = rddf->getWaypointSpeed(j)*(tempLocA.N-tempLocB.N)/dist;
    e[0] = tempLocB.E;
    e[1] = rddf->getWaypointSpeed(j)*(tempLocA.E-tempLocB.E)/dist;
          
    // Let the feedback handle this, unless we c2-ify.
    n[2] = 0;
    e[2] = 0;

    traj->inputWithDiffs(n, e);    
    tempLocB = tempLocA;
  }
  if(optC2) {
    CRefinementStage staticStage;
    staticStage.MakeTrajC2(traj);
  } 
  return 1;
}


// Loads a preloaded RDDF from the RDDFnode list, and makes a traj
// out of it.  Returns 1 on success, 0 if the requested RDDF does
// not exist.

int StaticPainter::LoadRDDFtoTraj(int index, int optC2) {
  RDDF *trajrddf;
  RDDFnode *currentPath;
  bool doesRDDFexist = false;  
  NEcoord currentLoc;

  for (currentPath = GPStrace; currentPath != NULL; currentPath = currentPath->next) {
    if (currentPath->index == index) {
      trajrddf = currentPath->data;
      doesRDDFexist = true;
      continue;
    }
  }

  if (!doesRDDFexist) {
    cerr << "RDDF with index " << index << " does not exist." << endl;
    return 0;
  } else {
    trajIndex = index;
  }

  UpdateState();
  currentLoc.N = m_state.Northing;
  currentLoc.E = m_state.Easting;

  RDDFtoTraj(trajrddf, &sTraj, currentLoc, optC2);
  return 1;
}
  
void StaticPainter::SendStaticTraj() {
  if(!SendTraj(trajSocket, &sTraj)) {
    cerr << "Sending static traj failed!" << endl;
  }
}


// Updates the vehicle location; the updateVehicleLoc incidentally
// makes exposedBoxes which contain the area that needs to be updated.
int StaticPainter::Recenter() {
  UpdateState();
  GPSmap->updateVehicleLoc(m_state.Northing, m_state.Easting);
  return 0;
}

// When implemented, will redraw everything in the map box.
int StaticPainter::Repaint() {
  cout << "Repaint called." << endl;
  return 0;
}

// Sends the mapDelta to fusionMapper, where it is caught by a 
// receiving thread.
int StaticPainter::SendStaticDelta() {
  CDeltaList *deltaPtr = NULL;

  deltaPtr = GPSmap->serializeDelta<double>(GPSlayer);
  if (!deltaPtr->isShiftOnly()) {
    SendMapdelta(socket_num, deltaPtr);
  }
  GPSmap->resetDelta<double>(GPSlayer);
  return 0;
}

// Function to load an RDDF into the RDDFnode structure.
int StaticPainter::LoadRDDF(RDDF *myRDDF) {
  // Build 'em and give 'em sequential indices.
  cout << "LoadRDDF called." << endl;
  
  RDDFnode *newRDDFnode = new RDDFnode;
  newRDDFnode->data = myRDDF;
  newRDDFnode->next = NULL;
  
  // Preload important information about the RDDF.
  int pts = newRDDFnode->data->getNumTargetPoints();
  double maxN = newRDDFnode->data->getWaypointNorthing(0);
  double minN = maxN;
  double maxE = newRDDFnode->data->getWaypointEasting(0);
  double minE = maxE;
  for (int i = 1; i < pts; i++) {
    double testN = newRDDFnode->data->getWaypointNorthing(i);
    maxN = max(testN, maxN);
    minN = min(testN, minN);
    double testE = newRDDFnode->data->getWaypointEasting(i);
    maxE = max(testE, maxE);
    minE = min(testE, minE);
  }
  newRDDFnode->maxNorthing = maxN;
  newRDDFnode->minNorthing = minN;
  newRDDFnode->maxEasting = maxE;
  newRDDFnode->minEasting = minE;

  // Add indices to allow RDDF unloading. 
  if (GPStrace == NULL) {
    newRDDFnode->index = 1; 
    GPStrace = newRDDFnode;
  }  else {
    RDDFnode *temp = GPStrace;
    while(temp->next != NULL) {
      temp = temp->next;
    }
    newRDDFnode->index = temp->index + 1;
    temp->next = newRDDFnode;
  }
  cout << "First GPS point: (" << GPStrace->data->getWaypointNorthing(0)
       << ", " << GPStrace->data->getWaypointEasting(0) << ")." << endl;

  return newRDDFnode->index;
}

// Unload an RDDF from the linked list.
int StaticPainter::UnloadRDDF(int index) {
  RDDFnode *tempnode, *prevnode;
  for(tempnode = GPStrace; tempnode != NULL; tempnode = tempnode->next) {
    if (tempnode->index == index) {
      // Case 1: First node
      if(tempnode == GPStrace) {
	GPStrace = tempnode->next;
	delete tempnode;
	if (GPStrace == NULL) {
	  cerr << "Warning: no RDDFs loaded." << endl;
	}
	return 0;
      } else {
      // Case 2: Central or last node      
	prevnode->next = tempnode->next;
	delete tempnode;
	return 0;
      }
    }
    prevnode = tempnode;
  }
  cerr << "RDDF with this index not found." << endl;
  return 1;
}

int StaticPainter::LogTrajs() {
  bool logging_enabled;
  string logging_location;

  logging_enabled = getLoggingEnabled();

  if(logging_enabled) {  
  logging_location = getLogDir();
    
    char* buffer= new char[256];
    string file_location = logging_location + "static_traj";
    strcpy(buffer, file_location.c_str());
    
    //open file and write it
    ofstream output_file;
    output_file.open(buffer, ios::app | ios::out);
    if(!output_file.is_open()) {
      cout << "\n ! Unable to open file " << (string)buffer << " , no data will be written!" << endl;
      return -1;
    } else {
      sTraj.print(output_file);
    }
    output_file.close();
    
    delete [] buffer;
    return 1;
  }
  return 0;
}
