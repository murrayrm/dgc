#ifndef _STATICPAINTER_HH_
#define _STATICPAINTER_HH_

#include <cstdio>
#include <cmath>
#include "CMap.hh"
#include "rddf.hh"  
#include "VehicleState.hh"
#include "sn_msg.hh"
#include "GlobalConstants.h"
#include "StateClient.h"
#include "DGCutils"
#include "getopt.h"
#include "MapdeltaTalker.h"
#include "TrajTalker.h"
#include "traj.h"
#include "CTrajPainter.hh"
#include "RefinementStage.h"
#include "CTimberClient.hh"
#include "MapConstants.h"

#define STATIC_CHOP_AHEAD 100.0
#define STATIC_CHOP_BEHIND 5.0

/** RDDFnode is a linked list of RDDF files, which can be loaded and 
    unloaded as required to improve performance. These RDDF files are 
    capture files, meaning the waypoints were actually driven, and the
    speed variable corresponds to the speed the driver was going. Don't
    load regular RDDFs. Capture files are labeled as:
    gps_yyyymmdd_hhmmss.dat. */
typedef struct RDDFnode {
  /** The RDDF data--i.e. the waypoint Northing, Easting, Speed.  Other 
      RDDF features aren't used but included for file compatibility. */
  RDDF *data;

  /** Unique index assigned to each loaded RDDF.  This allows it to be
      identified for unloading. */
  int index;

  /** The largest northing value in the captured RDDF.  Used with other 
      largest/smallest values, to quickly evaluate whether an RDDF is 
      applicable or not to a given area.  */
  double maxNorthing;
  
  /** The smallest northing value in the RDDF. */
  double minNorthing;

  /** The largest easting value in the RDDF. */
  double maxEasting;

  /** The smallest easting value in the RDDF. */
  double minEasting;

  /** A pointer to the next loaded RDDF, if any exists. */
  struct RDDFnode *next;
};

/** 
    @brief A module for data collected in previous desert drives.
    
    StaticPainter takes previously captured RDDF files from drives in the
    desert, and writes them to map deltas, which fusionMapper can then use
    for the planner.  Values in the map are the speed the driver was going 
    at the time of capture, which is assumed to be a reasonable path. */

class StaticPainter : public CStateClient, public CMapdeltaTalker, 
		      public CTrajTalker, public CTimberClient {
public:
  // Constructors
  /** Creates a new StaticPainter object. No arguments taken; will require
      Initialize(CMap, layer) and at least one instance of LoadRDDF(&rddf)
      before it becomes useful.  Default skynet key of 0.*/
  StaticPainter();
  
  /** Creates a new empty StaticPainter object as above, but with a
      specified skynet key, which is the int argument. */
  StaticPainter(int snKey);

  /** Destroys a StaticPainter object. */
  // Destructor
  ~StaticPainter();
  
  // Mutators
  /** Loads the map and map layer into StaticPainter. Uses the defaults 
      from the constants directory if none specified.  Returns 0 if all went
      well. */
  int Initialize(CMap *map = NULL, int layer = -1);

  /** Loads a specified RDDF into StaticPainter.  Reads everything in from
      rddflist.dat if no argument given.  Returns 0 if all went well. */
  int InitializeRDDF(char *filename = NULL);

  /** Updates the location of the vehicle and resets the exposed box, which
      is to be painted. */
  int Recenter();
  
  /** Repaints the entire map inside the box.  Not implemented yet. */
  int Repaint();

  /** Sends the changes to the static layer out so fusionMapper can pick 
      them up. */
  int SendStaticDelta();

  /** Paints the trace of the driven path into two map deltas: one for the
      rows exposed by the last movement, and another for the columns exposed.
      Returns 0 if all went well. */
  int PaintDelta();

  /** Loads a captured RDDF into the StaticPainter object, so that if any 
      part of it appears on the map, it will be drawn in. It is possible 
      to have multiple RDDFs loaded at a time.  Each is assigned a unique 
      index at loading; this can be used to unload them as well. Returns
      the index of the loaded RDDF.  

      Paths to RDDFs to be loaded are stored in rddflist.dat, in the 
      GPSFinding directory.*/
  int LoadRDDF(RDDF *newGPStrace);

  /** Unloads a captures RDDF by index. */
  int UnloadRDDF(int oldIndex);

  /** Gets the width, in cells, of the path blurred by StaticPainter. */
  int getBlurWidth() {return blurWidth;}

  /** Sets the width, in cells, of the path blurred by StaticPainter. */
  void setBlurWidth(int width) {blurWidth = width;}

  /** Gets the socket for sending deltas to fusionMapper. */
  int getSocketNum() {return socket_num;}

  /** Sets the socket for sending deltas to fusionMapper. */
  void setSocketNum(int num) {socket_num = num;}

  /** A general utility to make a trajectory out of an RDDF file. */
  int RDDFtoTraj(RDDF *rddf, CTraj *traj, NEcoord loc, int optC2 = 0, double chopBehind = STATIC_CHOP_BEHIND, double chopAhead = STATIC_CHOP_AHEAD);
  
  /** Sets up the socket for sending trajectories to trajFollower. */
  void makeStaticTrajSocket();

  /** Loads a trajectory from the RDDFnode structure, corresponding to 
      the index of the desired RDDF.  Note that if only one RDDF is 
      loaded, the index will always be 1 for that RDDF. */
  int LoadRDDFtoTraj(int index, int optC2 = 0);

  /** Sends a trajectory to trajFollower. */
  void SendStaticTraj();

  /** Logs trajectories using timber. */
  int LogTrajs();
protected:
  // Misc

  //inline int StaticPainter::between(double middle, double left, double right) {
  //    return ((middle > left) && (middle < right)) ? true : false;
  //}                                                                                                                                           
  //#warning Should template inlines later.
  //inline double StaticPainter::max(double a, double b) {
  //  return (a > b) ? a : b;
  //}

  //inline double StaticPainter::min(double a, double b) {
  //  return (a < b) ? a : b;
  //}

  //inline double StaticPainter::sign(double a) {
  //  return (a > 0) ? a : -a;
  //}

  inline double StaticPainter::distance(NEcoord a, NEcoord b) {
    return sqrt((a.N-b.N)*(a.N-b.N) + (a.E-b.E)*(a.E-b.E));
  }

  // inline double StaticPainter::distance(NEcoord a, double N, double E) {
  //  return sqrt((a.N-N)*(a.N-N) + (a.E-E)*(a.E-E));
  //}

  CMap *GPSmap;
  int GPSlayer;
  RDDFnode *GPStrace;
  CTrajPainter *GPSpainter;

  double numRows;
  double numCols;
  double resRows;
  double resCols;

  int blurWidth;
  int skynetKey;
  int socket_num;

  CTraj sTraj;
  int trajIndex;  // Corresponds to the RDDF we're using.
  int trajSocket; // For sending trajs.
};
#endif
