%TRAJFANALYSERESULTS.M  TrajFanalyseResults.m       FUNCTION: TrajFanalyseResults
%
%This function reads in the contents of the following types of files output
%by the trajfollower, and saves the corresponding data as a set of matrics
%(one per input file type) which are then plot using the TrajFplotData m-file
%Note that this function takes as its input the date & time tag to use 
%(format: DDMMYY_HHMMSS), this is necessary as output files are saved as: 
%XXXXX_DDMMYY_HHMMSS, where XXXXX is replaced by the input data file type, 
%e.g. 'error')
%
%INPUT: Date & time tag to use in the format: 'DDMMYY_HHMMSS' (and associated
%       data files: velocity.dat, state2.dat, commands.dat, error.dat, state.dat
%                   vars.dat 
%OUTPUTS:
%  Plots Produced:
%  {planned paths, actual path}
%  {y-error, index}
%  {h-error, index}
%  {c-error, index}
%  {RMS y-error, index}
%  {RMS theta-error, index}
%  {planned speed, actual speed}
%  {speed-error, index}
%  {RMS speed-error, index}


function  TrajFreadinData(date_time_tag)

    %Read in the data from the input data file type: VELOCITY, note that
    %despite calling itself velocity, this really means SPEED
    %Input File Format:
    %COL1: SPEED REFERENCE (m/s)
    %COL2: ACTUAL SPEED (m/s)
    %COL3: SPEED ERROR [reference - actual] (m/s) 
    %COL4: INDEX
    %COL5: speed-error^2
    %COL6: Cumulative speed-error^2
    %COL7: (Cumulative) RMS speed-error
    velocity_raw = importdata(sprintf('velocity_%s.dat', date_time_tag));
    
    %Create the index for the speed matrix
    size_velocity = size(velocity_raw);
    rows_velocity = size_velocity(1,1);
    velocity_counter = 1;
    
    while velocity_counter<=rows_velocity
        
        velocity_raw(velocity_counter, 4) = velocity_counter;
        
        %COL5: speed-error^2
        velocity_raw(velocity_counter, 5) = velocity_raw(velocity_counter, 3)^2;
        
        if velocity_counter == 1;
            
            velocity_raw(velocity_counter, 6) = 0;
            velocity_raw(velocity_counter, 7) = 0;
            
        else
            
            %COL6: Cumulative speed-error^2
            velocity_raw(velocity_counter, 6) = velocity_raw((velocity_counter-1), 6) + velocity_raw(velocity_counter, 5);
            %COL7: (Cumulative) RMS speed-error
            velocity_raw(velocity_counter, 7) = sqrt(velocity_raw(velocity_counter, 6)/velocity_counter);
            
        end
        
        velocity_counter = velocity_counter + 1;
        
    end
    
    
    %Read in the data from the input data file type: STATE2 
    %Input File Format:
    %COL1: CURRENT NORTHING (of Alice) (UTM --> m)
    %COL2: CURRENT EASTING (of Alice) (UTM --> m)
    %COL3: CURRENT TRAJ INDEX (of point being followed)
    %COL4: CURRENT TRAJ POINT NORTHING (UTM --> m)
    %COL5: CURRENT TRAJ POINT EASTING (UTM --> m)
    state2_raw = importdata(sprintf('state2_%s.dat', date_time_tag));
    
    %Read in the data from the input data file type: PLANS 
    %                                         **** NEED COL HEADINGS ****
    plans_raw = importdata(sprintf('plans_%s.dat', date_time_tag));
    
    %Read in the data from the input data file type: PLANSTARTS 
    %                                         **** NEED COL HEADINGS ****
    planstarts_raw = importdata(sprintf('planstarts_%s.dat', date_time_tag));
    
    %Read in the data from the input data file type: DEFAULT TRAJ
    %NOTE: this *only* exists if RDDFPathGen was used to create the traj
    %that was followed by the TrajFollower
    %COL1: Intended NORTHING
    %COL2: Intended NORTHING-DOT
    %COL3: Intended NORTHING-DOUBLE-DOT
    %COL4: Intended EASTING
    %COL5: Intended EASTING-DOT
    %COL6: Intended EASTING-DOUBLE-DOT
    defaultTraj_raw = importdata(sprintf('default.traj', date_time_tag));
    
    %Read in the data from the input data file type: COMMANDS 
    %Input File Format:
    %COL1: STEER COMMAND                                   **** UNITS *****
    %COL2: ACCEL COMMAND (-1 --> +1)                       **** UNITS *****
    commands_raw = importdata(sprintf('commands_%s.dat', date_time_tag));
    
    %Read in the data from the input data file type: ERROR 
    %Input File Format:
    %COL1: y-error
    %COL2: theta-error
    %COL3: combined-error
    error_raw = importdata(sprintf('error_%s.dat', date_time_tag));
    
    %Determine the cumulative RMS errors for y-error & theta-error
    error_size = size(error_raw);
    error_rows = error_size(1,1);
    error_counter = 1;
     
    while error_counter<=error_rows
        
        %COL4: y-error^2 (individual for each row)
        error_raw(error_counter, 4) = (error_raw(error_counter, 1))^2;
        
        %COL7: theta-error^2 (individual for each row)
        error_raw(error_counter, 7) = (error_raw(error_counter, 2))^2;
        
        if error_counter == 1;
            
            error_raw(error_counter, 5) = 0;
            error_raw(error_counter, 6) = 0;
            error_raw(error_counter, 7) = 0;
            error_raw(error_counter, 8) = 0;
            error_raw(error_counter, 9) = 0;
            
        else
            
            %COL5: Cumulative y-error^2
            error_raw(error_counter, 5) = error_raw((error_counter-1), 5) + error_raw(error_counter, 4);
            %COL6: (Cumulative) RMS y-error
            error_raw(error_counter, 6) = sqrt(error_raw(error_counter, 5)/error_counter);
            
            %COL8: Cumulative theta-error^2
            error_raw(error_counter, 8) = error_raw((error_counter-1), 8) + error_raw(error_counter, 7);
            %COL9: (Cumulative) RMS theta-error
            error_raw(error_counter, 9) = sqrt(error_raw(error_counter, 9)/error_counter);
            
            %COL10: INDEX
            error_raw(error_counter, 10) = error_counter;
            
        end
        
        error_counter = error_counter + 1;
        
    end
    
    
    %Read in the data from the input data file type: STATE
    %Input File Format:
    %Timestamp - UNIX time-stamp
    %Northing (static variable)
	%Easting (static variable)
	%Altitude (static variable)
    %Vel_N (static variable)
	%Vel_E (static variable)
	%Vel_D (static variable)
	%Acc_N (static variable)
	%Acc_E (static variable)
	%Acc_D (static variable)
	%Roll (static variable)
	%Pitch (static variable)
	%Yaw (static variable)
	%RollRate (static variable)
	%PitchRate (static variable)
	%YawRate (static variable)
	%RollAcc (static variable)
	%PitchAcc (static variable)
	%YawAcc (static variable)
    state_raw = importdata(sprintf('state_%s.dat', date_time_tag));
    
    %Read in the data from the input data file type: TEST 
    %Input File Format:
    %COL1: COMMANDED ACCEL (m/s^2)
    %COL2: MAX_ACCEL (static variable)
    %COL3: MAX_DECEL (static variable)
    %COL4: NORTHING of current traj point (UTM --> m)
    %COL5: EASTING of current traj point (UTM --> m)
    %COL6: STEER COMMAND (delta rad)                        **** CHECK THIS ****
    %COL7: STEERING FEED-FORWARD COMPONENT                  **** UNITS *****
    %COL8: STEERING FEED-BACK COMPONENT
    test_raw = importdata(sprintf('test_%s.dat', date_time_tag));
    
    %Read in the data from the input data file type: VARS
    %Input File Format:                                     **** NEED DEFN'S****
    %MAX_ACCEL (static variable)
	%MAX_DECEL (static variable)
	%DFE_LIN_SLOPE (static variable)
	%DFE_LIN_OFFSET (static variable)
	%DFE_DELTA_SLOPE (static variable)
	%DFE_DELTA_OFFSET (static variable)
	%SPE_GAIN_PROP (static variable)
	%SPE_GAIN_INTEG (static variable)
	%SPE_GAIN_DIFF (static variable)
	%SPE_INTEG_SAT (static variable)
	%LAT_GAIN_PROP (static variable)
	%LAT_GAIN_INTEG (static variable)
	%LAT_GAIN_DIFF (static variable)
	%LAT_INTEG_SAT (static variable)
	%PERP_THETA_DIST (static variable)
	%ALPHA_GAIN (static variable)
	%BETA_GAIN (static variable)
    vars_raw = importdata(sprintf('vars_%s.dat', date_time_tag));
    
    
    %Plot the results using the plotting m-file for this code, note that
    %this m-file adds all of the required labels to the data
    TrajFplotData
    
end
