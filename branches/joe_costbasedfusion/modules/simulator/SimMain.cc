#include "Sim.hh"
#include "askynet_monitor.hh"
#include <getopt.h>

#include "DGCutils"

using namespace std;


void printSimHelp();

enum {
  OPT_NONE,
  OPT_HELP,
  OPT_LAG,
  OPT_RATELIMIT,
  OPT_SNKEY,

  NUM_OPTS
};


int main(int argc, char **argv)
{
  // defaults
  int simpleModel = false;
  int noise = 1;
  int estop = false;
  int trans = false;
  double steerLag = 0.0;
  double steerRateLimit = 0.699;
  int optSNKey = -1;

  int cmdLineOpt;

  static struct option long_options[] = {
    //Options that don't require arguments:
    //{"sparrow", no_argument, &optSparrow, 1},
    //{"nosparrow", no_argument, &optSparrow, 0},

    /** Arguments: name [char*], no_argument/required_argument/optional_argument ([int]), flag = [int*] if NULL getopt_long() 
     ** returns 'value', otherwise sets var' pointed to by flag to 'value', value = [int] value to return, 
     ** or to load into the var pointed to by flag **/
    {"help",      no_argument, 0, OPT_HELP},
    {"nonoise", no_argument, &noise, 0},
    {"estop", no_argument, &estop, true},
    {"trans", no_argument, &trans, true},
    {"simple", no_argument, &simpleModel, true},
    //Options that require arguments
    {"lag", required_argument, 0, OPT_LAG},
    {"ratelimit", required_argument, 0, OPT_RATELIMIT},
    {"snkey", required_argument, 0, OPT_SNKEY},
    {0,0,0,0}
  };
  
  while (1) {
    int option_index = 0;
    
    cmdLineOpt = getopt_long_only(argc, argv, "", long_options, &option_index);
    if(cmdLineOpt== -1) break;

    switch( cmdLineOpt ) {
    case '?':
    case OPT_HELP:
      cout << "Printing simulator help" << endl;
      printSimHelp();
      return 0;
      break;
    case OPT_LAG:
      steerLag = atoi(optarg);
      break;
    case OPT_RATELIMIT:
      steerRateLimit = atoi(optarg);
      break;
    case OPT_SNKEY:
      optSNKey = atoi(optarg);
      break;
    default:
      if(cmdLineOpt!=0) {
	cout << "Unknown option " << cmdLineOpt << endl;
	printSimHelp();
	return 0;
      }
    }
  }

  char* pSkynetkey = getenv("SKYNET_KEY");
  if( pSkynetkey == NULL ) {
    cerr << "SKYNET_KEY environment variable isn't set" << endl;
  } else if(optSNKey == -1) {
    optSNKey = atoi(pSkynetkey);
  }

  cerr << "Constructing skynet with KEY = " << optSNKey << endl;
  asim sim(optSNKey, (bool)simpleModel, steerLag, steerRateLimit, noise, (bool)estop, (bool)trans);

  DGCstartMemberFunctionThread(&sim, &asim::SparrowDisplayLoop);
  DGCstartMemberFunctionThread(&sim, &asim::UpdateSparrowVariablesLoop);

  // adrive stuff
  int run_skynet_thread = true;
  struct skynet_main_args args;
  pthread_t adriveThread;
  args.pVehicle = sim.getADriveVehicle();
  args.pRun_skynet_thread = &run_skynet_thread;
  init_vehicle_struct(*args.pVehicle);
  pthread_create( &adriveThread, NULL, &skynet_main, &args );

  sim.Active();

  return 0;
}


void printSimHelp() {
  /* User has requested usage information. Print it to standard
     output, and exit with exit code zero. */
  cout << "asim command line options:\n"
       << "--------------------------\n"
       << "-h, --help:       Prints this usage\n"
       << "--simple:         Uses the simple (kinematic integrator) vehicle mode.\n"
       << "                  No sliding, steering lags, etc.\n"
       << "--lag val:        Sets the steering first order lag timeconstant to val (s)\n"
       << "                  Default: 0.0 sec\n"
       << "--ratelimit val:  Sets the steering rate limit to val (rad/sec)\n"
       << "                  Default: 0.699 rad/sec\n"
       << "--nonoise:        don't simulate state noise\n"
       << "--estop:          accept estop commands\n"
       << "--snkey val       Use the skynet key val\n"
       << "\n"
       << "Status:\n"
       << "---------------------\n"
       << "Current RDDF:         ";
  cout.flush();
  system("readlink -f rddf.dat");
  cout << "Current SKYNET_KEY:   ";
  cout.flush();
  system("echo $SKYNET_KEY");
  cout << endl;
}
