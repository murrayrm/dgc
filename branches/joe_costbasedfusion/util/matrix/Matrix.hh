#ifndef _BLASMATRIX_HH_
#define _BLASMATRIX_HH_

/*! A generic Matrix class implementing BLAS Linear Algebra routines
    This class allows you to do pretty much anything basic operation you 
    would do with Matlab in C.

    NOTE: C-style indicies imply first row is row 0

    A word of caution, due to BLAS functionality, matrices are internally
    stored in column order, although accessors, use row, col notation.

    When using setelems, there is an implicit internal transpose so that 
    you can assign with an array of doubles in row order and things will
    work like they should.

    Example of convenient assignment of Matrix:

    Matrix foo(3,3);

    double fooelems[] = {1, 2, 3,
                         4, 5, 6,
			 7, 8, 9}

    foo.setelems(fooelems);


    Example of altenative assignment of Matrix
    
    Matrix foo(3,3);

    foo.setelem(0,0,1);
    foo.setelem(0,1,2);
    foo.setelem(0,2,3);
    foo.setelem(1,0,4);
    foo.setelem(1,1,5);
    foo.setelem(1,2,6);
    foo.setelem(2,0,7);
    foo.setelem(2,1,8);
    foo.setelem(2,2,9);

    In both cases the Matrix is stored internally as:
    {1, 4, 7, 2, 5, 8, 2, 6, 9}
    But this should only concern you if you use getelem(int) on a Matrix instead of a vector



    Example Creation of a COLUMN vector:

    Matrix bar(3);
    
    double barelems[] = {1 2 3}

    bar.setelems(barelems);

    
    Example of using multiplcation to create a row vector:

    Matrix baz = bar*foo; 
    
*/

class Matrix {

private:

  int rows;
  int cols;
  int size;
  double* data;

public:
  // Constructors

  /*! create null matrix. */
  Matrix();

  /*! create row x col matrix. */
  Matrix(int rows, int cols);

  /*! create elem length coloumn vector. */
  Matrix(int elems);

  /*! copy Constructor. */
  Matrix(const Matrix &matrix2);

  // Mutators

  /*! sets the (row, col)th element of the matrix to value. */
  void setelem(int row, int col, double value);

  /*! sets the elemth element of a vector to value. 
      This will work for either row or column vectors.
      This operation is valid on matrices but I advise
      against using it on them.*/
  void setelem(int elem, double value);

  /*! Batch set using array of doubles.  Note there is no size
      Checking so don't be retarded when you use this. 
      See above for example of use */
  void setelems(double* data);

  void clear();

  // Accessors
  /*! returns the number of rows. */
  int getrows() const  {return rows;}  

  /*! returns the number of columns. */
  int getcols() const  {return cols;}

  /*! returns the size */
  int getsize() const {return size;}

  /*! returns the (row, col)th element of the matrix. */
  double getelem(int row, int col)  const;

  /*! returns the elemth element of a vector to value.
      once more this works on Matrices but I advise
      against using it as such. */
  double getelem(int elem)  const;

  /*! returns the pointer to the data of the matrix.
      Nothing here to keep you from being stupid
      and segfaulting yourself */
  double* getdata() const;
 
  /*! Returns a transpose of the matrix matrix */
  Matrix transpose() const; 

  /*! Returns the inverse of a square matrix. */
  Matrix inverse() const; 

  /*! returns an identity matrix the size of the matrix calling it*/
  Matrix eye() const;

  /*! Returns the norm of a vector.
    Used on a matrix is treats the flattened matrix as a vector*/
  double norm() const; 


  Matrix operator+(const Matrix &matrix2) const;  
  
  Matrix operator-(const Matrix &matrix) const;  

  Matrix operator*(const Matrix &matrix2) const;  

  Matrix operator*(const double sf) const;  

  Matrix operator/(const double sf) const;  
  
  Matrix& operator=(const Matrix &matrix2);

  void print();

  /*! Default destructor. */
  ~Matrix();  
};


/*! Returns a matrix formed cross-product of a vector */
Matrix matrixCross(Matrix vect);

/*! Returns the vector corresponding to a matrix-formed cross-product */
Matrix unmatrixCross(Matrix mat);

/*! Returns a matrix which corresponds to a rotation about vector w, where amount of
   rotation corresponds to magnitude of w */
Matrix matrixDiffEqStep(Matrix w);

/*! Creates a 3x3 Body2Nav Matrix */
void makeBody2Nav(Matrix &Cnb, double roll, double pitch, double yaw);

/*! Creates an 3x3 Earth2Nav Matrix */
void makeEarth2Nav(Matrix &Cne, double lat, double lon, double alpha);

/*! Creates a 3x3 Nav2Geo Matrix */
void makeNav2Geo(Matrix &Cgn, double alpha);

/*! Creates a 4x4 Transform matrix 
    A word of advice on using makeTransform

    Transform matrices assume you are using a 4x1 COLUMN vector as
    the point or vector which you are transforming.

    Points should be of the form {x, y, z, 1}
    Vectors should be of the form {x, y, z, 0}

    This permits points to be transformed and vectors to merely be rotated

    */
void makeTransform(Matrix &Ct, double rx, double ry, double rz, double tx, double ty, double tz);

#endif
