#include <CMapPlus.hh>
#include <iostream>
using namespace std;

#include "VehicleState.hh"
#include "GlobalConstants.h"
#include "sn_types.h"
#include "sn_msg.hh"
#include "MapdeltaTalker.h"
#include "MapConstants.h"

class CLoadSpew : public CMapdeltaTalker
{
public:
	CLoadSpew(int skynetKey, VehicleState* pState, CDeltaList* deltaList)
		: CSkynetContainer(SNfusionmapper, skynetKey)
	{
		int statesock = m_skynet.get_send_sock(SNstate);
		m_skynet.send_msg(statesock, pState, sizeof(VehicleState));

		int mapsock = m_skynet.get_send_sock(SNfusiondeltamap);
		SendMapdelta(mapsock, deltaList);
	}
};

int main(int argc, char* argv[])
{
	if(argc != 3)
	{
		cerr << "Usage: loadspew state cmapfileprefix" << endl;
		cerr << "i.e.: \"loadspew st map\" will load state from \"st\" and the" << endl <<
			"map from \"map.hdr\" and \"map.pgm\"" << endl;
		return -1;
	}

	VehicleState vehstate;
	int statesize = sizeof(vehstate);
	ifstream vehstatefile(argv[1]);
	vehstatefile.read((char*)&vehstate, statesize);


	CMapPlus map;
	int mapLayer;

	map.initMap(CONFIG_FILE_DEFAULT_MAP);
	// Add the speed limit layer to the map
	mapLayer = map.addLayer<double>(CONFIG_FILE_COST, true);
	if(map.loadLayer<double>(mapLayer, argv[2], true) != 0)
	{
		cerr << "loadLayer() failed" << endl;
		return -1;
	}

	int deltaSize;
	CDeltaList *deltaPtr = map.serializeFullMapDelta<double>(mapLayer);

	int sn_key;
	cerr << "Searching for skynet KEY " << endl;
	char* pSkynetkey = getenv("SKYNET_KEY");
	if( pSkynetkey == NULL )
	{
		cerr << "SKYNET_KEY environment variable isn't set" << endl;
		return -1;
	}
	else
		sn_key = atoi(pSkynetkey);

	CLoadSpew spew(sn_key, &vehstate, deltaPtr);

	return 0;
}
