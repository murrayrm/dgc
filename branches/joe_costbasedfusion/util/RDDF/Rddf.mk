RDDF_PATH = $(DGC)/util/RDDF
RDDF_NAME = rddf.dat

RDDF_DEPEND = \
  $(CONSTANTSLIB) \
	$(RDDF_PATH)/rddf.hh \
	$(RDDF_PATH)/rddf.cc \
	$(RDDF_PATH)/Makefile
