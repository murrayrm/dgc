#ifndef __CCORRIDORPAINTER_HH__
#define __CCORRIDORPAINTER_HH__


#include <stdlib.h>
#include <iomanip>
#include "CMap.hh"
#include "rddf.hh"
#include "geometry/CLineSegment.hh"
#include "geometry/CRectangle.hh"

#define CP_PERCENT_SWITCH 0.1

enum {
  CP_INSIDE_CORRIDOR=1,
  CP_OUTSIDE_CORRIDOR=0,
  CP_OUTSIDE_MAP=-1,
  CP_TRACKLINE_VAL = 1,
  CP_EDGE_VAL = 1,

  CP_NUM_TYPES
};

struct CCorridorPainterOptions {
  double optRDDFscaling;
  double optMaxSpeed;
  
  enum PAINT_CENTER_TRACKLINE_TYPE {
    DONT = 0,
    PERCENTAGE = 1,
    SHIFT = 2
  };

  PAINT_CENTER_TRACKLINE_TYPE paintCenterTrackline;
  
  enum WHAT_TO_ADJUST_TYPE {
    RDDF = 0,
    CENTER_TRACKLINE = 1
  };

  WHAT_TO_ADJUST_TYPE whatToAdjust;

  double adjustmentVal;
};

class CCorridorPainter {
public:
  CCorridorPainter();
  ~CCorridorPainter();

  int initPainter(CMap* inputMap,
		  int layerNum,
		  RDDF* inputRDDF,
		  double insideCorridorVal = CP_INSIDE_CORRIDOR,
		  double outsideCorridorVal = CP_OUTSIDE_CORRIDOR,
		  double tracklineVal = CP_TRACKLINE_VAL,
		  double edgeVal = CP_EDGE_VAL,
		  double percentSwitch = CP_PERCENT_SWITCH,
		  int useDelta=0);

  /*
  void paintChanges(NEcoord rowBox[], NEcoord colBox[], double RDDFscaling,
		    double maxSpeed);
  */

  void paintChanges(NEcoord rowBox[], NEcoord colBox[]);

  void setPainterOptions(CCorridorPainterOptions* painterOpts);

  void repaintAll();
		  //double RDDFscaling, double maxSpeed);
  
#ifdef CP_OBS
  int paintChanges();
  int paintChanges_TracklineSlope();
  void paintChangesV2(NEcoord rowBox[], NEcoord colBox[],double RDDFscaling,
		   double maxSpeed);
#endif

private:
  CCorridorPainterOptions _painterOpts;

  CMap* _inputMap;
  int _layerNum;
  RDDF* _inputRDDF;

  int* _startingIndices;
  int* _endingIndices;
  int* _tracklineStartingIndices;
  int* _tracklineEndingIndices;
  
  double _insideCorridorVal;
  double _outsideCorridorVal;
  double _tracklineVal;
  double _edgeVal;
  double _percentSwitch;

  int _prevBottomLeftRowMultiple;
  int _prevBottomLeftColMultiple;

  int _behindWaypointNum;
  int _aheadWaypointNum;

  int _useDelta;

  int _lastUsedWayptNumberRowBox;
  int _lastUsedWayptNumberColBox;


  void findStartingIndices(NEcoord bottomPoint, NEcoord middlePoint,
			   NEcoord topPoint,
			   int minRow, int maxRow,
			   int minCol, int maxCol);

  void findEndingIndices(NEcoord bottomPoint, NEcoord middlePoint,
			 NEcoord topPoint,
			 int minRow, int maxRow,
			 int minCol, int maxCol);

  void traceRay(NEcoord start, NEcoord end, int indexList[],
		int minRow, int maxRow,
		int minCol, int maxCol, int outOnWhichSide);


  void paintRectangle(CLineSegment trackline, int i, double speed, 
		      int minRow, int maxRow,
		      int minCol, int maxCol);

  void paintCircle(CCircle circle, double speed,
		   int minRow, int maxRow,
		   int minCol, int maxCol);

  void paintTrackline(int i, double speed,
		      int minRow, int maxRow,
		      int minCol, int maxCol);
		      

  int _startRow, _endRow;
};


#endif //__CCORRIDORPAINTER_HH__
