#include "CRectangle.hh"
#include <iomanip>

CRectangle::CRectangle(NEcoord centerLineStart,
		       NEcoord centerLineEnd,
		       double width) {
  NEcoord difference = centerLineStart - centerLineEnd;

  _width = width;
  _length = difference.norm()/2.0;
  _center = centerLineStart - (difference/2.0);
  _angle = atan2(difference.E, difference.N);

  /*
  cout << "Got width=" << _width << ", _length=" << _length
       << ", angle=" << _angle << " and center at " << setprecision(10);
  _center.display();

  cout << "and diff was ";
  difference.display();
  cout << " and start was ";
  centerLineStart.display();
  */
}

CRectangle::CRectangle(NEcoord corners[]) {
  #warning "Need error checking here"
  _width = fabs(corners[0].E-corners[3].E)/2.0;
  _length = fabs(corners[1].N-corners[0].N)/2.0;
  _center = corners[0]+NEcoord(_length, _width);
  _angle = 0;
  /*
  cout << "Got width=" << _width << ", _length=" << _length
       << ", angle=" << _angle << " and center at " << setprecision(10);
  _center.display();
  */
}


CRectangle::~CRectangle() {

}

CRectangle::INTERSECTION_TYPE CRectangle::checkIntersection(const CRectangle& otherRect) {
 NEcoord A, B,   // vertices of the rotated rr2
	   C,      // center of rr2
	   BL, TR; // vertices of rr2 (bottom-left, top-right)

 if(this->_width==0 || this->_length==0 ||
    otherRect._width==0 || otherRect._length==0) {
   return INTERSECTION_NO;
 }

 double ang = this->_angle - otherRect._angle, // orientation of rotated rr1
       cosa = cos(ang),           // precalculated trigonometic -
       sina = sin(ang);           // - values for repeated use

 double t, x, a;      // temporary variables for various uses
 double dx;           // deltaX for linear equations
 double ext1, ext2;   // min/max vertical values

 // move rr2 to make rr1 cannonic
 //C = rr2->C;
 //SubVectors2D(&C, &rr1->C);
 C = otherRect._center;
 C-=this->_center;

 // rotate rr2 clockwise by rr2->ang to make rr2 axis-aligned
 //RotateVector2DClockwise(&C, rr2->ang);
 C.rotate(otherRect._angle);

 // calculate vertices of (moved and axis-aligned := 'ma') rr2
 BL = TR = C;
 //SubVectors2D(&BL, &rr2->S);
 //AddVectors2D(&TR, &rr2->S);
 BL-=NEcoord(otherRect._length, otherRect._width);
 TR+=NEcoord(otherRect._length, otherRect._width);


 // calculate vertices of (rotated := 'r') this
 A.N = -this->_width*sina; B.N = A.N; t = this->_length*cosa; A.N += t; B.N -= t;
 A.E =  this->_width*cosa; B.E = A.E; t = this->_length*sina; A.E += t; B.E -= t;

 t = sina*cosa;

 // verify that A is vertical min/max, B is horizontal min/max
 if (t < 0)
 {
  t = A.N; A.N = B.N; B.N = t;
  t = A.E; A.E = B.E; B.E = t;
 }

 // verify that B is horizontal minimum (leftest-vertex)
 if (sina < 0) { B.N = -B.N; B.E = -B.E; }

 // if rr2(ma) isn't in the horizontal range of
 // colliding with this(r), collision is impossible
 if (B.N > TR.N || B.N > -BL.N) return INTERSECTION_NO;

 // if this(r) is axis-aligned, vertical min/max are easy to get
 if (t == 0) {ext1 = A.E; ext2 = -ext1; }
 // else, find vertical min/max in the range [BL.N, TR.N]
 else
 {
  x = BL.N-A.N; a = TR.N-A.N;
  ext1 = A.E;
  // if the first vertical min/max isn't in (BL.N, TR.N), then
  // find the vertical min/max on BL.N or on TR.N
  if (a*x > 0)
  {
   dx = A.N;
   if (x < 0) { dx -= B.N; ext1 -= B.E; x = a; }
   else       { dx += B.N; ext1 += B.E; }
   ext1 *= x; ext1 /= dx; ext1 += A.E;
  }
  
  x = BL.N+A.N; a = TR.N+A.N;
  ext2 = -A.E;
  // if the second vertical min/max isn't in (BL.N, TR.N), then
  // find the local vertical min/max on BL.N or on TR.N
  if (a*x > 0)
  {
   dx = -A.N;
   if (x < 0) { dx -= B.N; ext2 -= B.E; x = a; }
   else       { dx += B.N; ext2 += B.E; }
   ext2 *= x; ext2 /= dx; ext2 -= A.E;
  }
 }

 // check whether rr2(ma) is in the vertical range of colliding with this(r)
 // (for the horizontal range of rr2)
 if(!((ext1 < BL.E && ext2 < BL.E) ||
      (ext1 > TR.E && ext2 > TR.E))) {
   return INTERSECTION_YES;
 }

 return INTERSECTION_NO;
}


CRectangle::INTERSECTION_TYPE CRectangle::checkIntersection(const CCircle& otherCircle) {
  if(_angle!=0) {
    cerr << "util/CRectangle.cc:checkIntersection hasn't been implemented yet for rectangles at angles!  Go implement it." << endl;
    exit(1);
  }

 if(this->_width==0 || this->_length==0 ||
    otherCircle._radius==0) {
   /*   
   cout << " oh no joe w=" << _width
	<< " length = " << _length 
	<< " radius = " << otherCircle._radius << endl;
   */
   return INTERSECTION_NO;
 }
 /*
 cout << setprecision(10) << "radius is " << otherCircle._radius << " center is ";
 otherCircle._center.display();
 */
 
  NEcoord bottomLeft = _center + NEcoord(-_length, -_width);
  NEcoord bottomRight = _center + NEcoord(-_length, _width);
  NEcoord topLeft = _center + NEcoord(_length, -_width);
  NEcoord topRight = _center + NEcoord(_length, _width);
  /*
  cout << "bl: ";
  bottomLeft.display();
  cout << "br :";
  bottomRight.display();
  cout << "tl: ";
  topLeft.display();
  cout << "tr: ";
  topRight.display();
  */
  if(bottomLeft.N <= otherCircle._center.N &&
     otherCircle._center.N <= topLeft.N &&
     bottomLeft.E <= otherCircle._center.E &&
     otherCircle._center.E <= bottomRight.E) {
    return INTERSECTION_YES;
  }
  if(bottomLeft.N <= otherCircle._center.N &&
     otherCircle._center.N <= topLeft.N) {
    double distToLeftSide = fabs(otherCircle._center.E - bottomLeft.E);
    double distToRightSide = fabs(otherCircle._center.E - bottomRight.E);
    if(otherCircle._center.E < bottomLeft.E &&
       otherCircle._radius >= distToLeftSide) {
      return INTERSECTION_YES;
    } else if(otherCircle._radius >= distToRightSide) {
      return INTERSECTION_YES;
    }
  }
  if(bottomLeft.E <= otherCircle._center.E &&
     otherCircle._center.E <= topRight.E) {
    double distToTopSide = fabs(otherCircle._center.N - topLeft.N);
    double distToBottomSide = fabs(otherCircle._center.N - bottomRight.N);
    if(otherCircle._center.N < bottomLeft.N &&
       otherCircle._radius >= distToBottomSide) {
      return INTERSECTION_YES;
    } else if(otherCircle._radius >= distToTopSide) {
      return INTERSECTION_YES;
    }
  }

  return INTERSECTION_NO;
}
