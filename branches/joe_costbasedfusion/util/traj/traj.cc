#include <math.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <assert.h>
using namespace std;
#include "traj.h"

#include <stdlib.h>

CTraj::CTraj(int order)
  : m_order(order)
{
  m_pN = new double[TRAJ_MAX_LEN * m_order];
  m_pE = new double[TRAJ_MAX_LEN * m_order];
  m_numPoints = 0;
	m_confidence = 0.0; // why the fuck do we have a confidence????
}

// Copy constructor
CTraj::CTraj(const CTraj & other)
{
  m_order = other.m_order;
  m_pN = new double[TRAJ_MAX_LEN * m_order];
  m_pE = new double[TRAJ_MAX_LEN * m_order];
  m_numPoints = 0;

  *this = other;
}

CTraj::CTraj(int order, istream& inputstream)
  : m_order(order)
{
	load(inputstream);
}

CTraj::CTraj(int order, char* pFilename)
  : m_order(order)
{
	ifstream inputstream(pFilename);
	load(inputstream);
}

void CTraj::load(istream& inputstream)
{
  string line;
  int d;
  
  m_numPoints = 0;
  
  // allocate memory for the data arrays
  m_pN = new double[TRAJ_MAX_LEN * m_order];
  m_pE = new double[TRAJ_MAX_LEN * m_order];
  
  
  if(!inputstream)
    {
      cerr << "CTraj couldn't read from a stream\n";
      return;
    }
  
  while(inputstream)
    {
      getline(inputstream, line, '\n');
      if(line.size() == 0)
	return;
      
      if(line[0] == '#' || line[0] == '%')
	continue;
      
      istringstream linestream(line);
      
      for(d=0; d<m_order; d++)
	linestream >> m_pN[INDEX(m_numPoints, d)];
      
      for(d=0; d<m_order; d++)
	linestream >> m_pE[INDEX(m_numPoints, d)];
      
      if(!linestream)
	return;
      
      m_numPoints++;
    }
}

CTraj &CTraj::operator=(const CTraj &other)
{
  if (&other != this) {       // avoid self-assignment
    // delete the contents of this if needed
    // copy all elements
    m_numPoints = other.m_numPoints;
    m_confidence = other.m_confidence;
    m_order = m_order;
    memcpy(m_pN, other.m_pN, sizeof(double)*m_order*TRAJ_MAX_LEN);
    memcpy(m_pE, other.m_pE, sizeof(double)*m_order*TRAJ_MAX_LEN);
  }
  
  return *this;
}

CTraj &CTraj::operator+=(const CTraj &other)
{
	if(other.m_order != m_order)
	{
		cerr << "CTraj::operator+=(): orders don't match" << endl;
		return *this;
	}
	if(other.m_numPoints + m_numPoints > TRAJ_MAX_LEN)
	{
		cerr << "CTraj::operator+=(): resulting traj would be too long" << endl;
		return *this;
	}

	for(int i=0; i<m_order; i++)
	{
		memcpy(m_pN+INDEX(m_numPoints,i), other.m_pN+INDEX(0,i), other.m_numPoints*sizeof(double));
		memcpy(m_pE+INDEX(m_numPoints,i), other.m_pE+INDEX(0,i), other.m_numPoints*sizeof(double));
	}
	m_numPoints += other.m_numPoints;

  return *this;
}


CTraj::~CTraj()
{
  delete [] m_pN;
  delete [] m_pE;
}

double CTraj::lastE()
{
  return m_pE[INDEX(m_numPoints-1, 0)];
}
double CTraj::lastN()
{
  return m_pN[INDEX(m_numPoints-1, 0)];
}

bool CTraj::inTraj(double northing, double easting)
{
  for(int i=0; i<m_numPoints; i++)
	{
		if( (northing - m_pN[INDEX(i, 0)])*(northing - m_pN[INDEX(i, 0)]) +
				(easting  - m_pE[INDEX(i, 0)])*(easting  - m_pE[INDEX(i, 0)]) <
				TRAJ_IN_PATH_RES * TRAJ_IN_PATH_RES)
      return true;
	}
  
  return false;
}

void CTraj::startDataInput(void)
{
  m_numPoints = 0;
}

void CTraj::inputNoDiffs(double northing, double easting)
{
#warning "maybe specify 'no data' for derivatives here"
  m_pN[INDEX(m_numPoints, 0)] = northing;
  m_pE[INDEX(m_numPoints, 0)] = easting;
  
  if(m_numPoints != TRAJ_MAX_LEN-1)
    m_numPoints++;
  else
    cerr << "CTraj: overflow\n";
}

void CTraj::inputWithDiffs(double* northing, double* easting)
{
  for(int i=0; i<m_order; i++)
  {
    m_pN[INDEX(m_numPoints, i)] = northing[i];
    m_pE[INDEX(m_numPoints, i)] = easting[i];
  }
  
  if(m_numPoints != TRAJ_MAX_LEN-1)
    m_numPoints++;
  else
    cerr << "CTraj: overflow\n";
}

void CTraj::shiftNoDiffs(int ptsToToss)
{
	memmove((char*)m_pN, ((char*)m_pN) + ptsToToss*sizeof(m_pN[0]), (m_numPoints-ptsToToss)*sizeof(m_pN[0]));
	memmove((char*)m_pE, ((char*)m_pE) + ptsToToss*sizeof(m_pN[0]), (m_numPoints-ptsToToss)*sizeof(m_pN[0]));
	m_numPoints -= ptsToToss;
}

// This function cuts down the current trajectory to be of length desiredDist
// (if the traj is longer), or lengthens it to be dist long
void CTraj::truncate(double desiredDist)
{
  if(m_numPoints < 2)
    return;

  int i, j;
  double d = 0.0;
  double dN, dE;
  
  for(i=0; i<m_numPoints-1; i++)
	{
		dN = m_pN[INDEX(i+1, 0)] - m_pN[INDEX(i, 0)];
		dE = m_pE[INDEX(i+1, 0)] - m_pE[INDEX(i, 0)];
		d = sqrt( dN*dN + dE*dE );
		desiredDist -= d;
      
		// we exceeded than desiredDist
		if(desiredDist < 0.0)
		{
			// we want to truncate the last segment by -desiredDist long, but along
			// the same direction
			for(j=0; j<m_order; j++)
	    {
	      m_pN[INDEX(i+1, j)] = m_pN[INDEX(i, j)] + (m_pN[INDEX(i+1, j)] - m_pN[INDEX(i, j)]) * (d+desiredDist) / d;
	      m_pE[INDEX(i+1, j)] = m_pE[INDEX(i, j)] + (m_pE[INDEX(i+1, j)] - m_pE[INDEX(i, j)]) * (d+desiredDist) / d;
	    }
	  
			// Set new point count
			m_numPoints = i + 2;
	  
			return;
		}
	}
  
  // we get here if the total length was not enough. We want to grow the last
  // segment to meet the required length. desiredDist left, so the last segment
  // should be d+desiredDist long
  for(j=0; j<m_order; j++)
	{
		m_pN[INDEX(m_numPoints-1, j)] = m_pN[INDEX(m_numPoints-2, j)] + (m_pN[INDEX(m_numPoints-1, j)] - m_pN[INDEX(m_numPoints-2, j)]) * (d+desiredDist) / d;
		m_pE[INDEX(m_numPoints-1, j)] = m_pE[INDEX(m_numPoints-2, j)] + (m_pE[INDEX(m_numPoints-1, j)] - m_pE[INDEX(m_numPoints-2, j)]) * (d+desiredDist) / d;
	}
}

// This function returns a vector of length num of the orientations of the
// vehicle along this traj, dist meters ahead. output into thetavector. bias is
// subtracted from the result.
void CTraj::getThetaVector(double dist, int num, double* thetavector, double bias)
{
	double delta = dist / (double)(num - 1);

	int i;
	int pointIndex;
	double distInSegment;

	double distLeftInSegment;
	double n0, e0, n1, e1;
	double lenSegment;
	double distLeft;

	// this keeps track of overruns. overrunBias is always an integer multiple of 2pi
	double prevTheta = 0.0;
	double overrunBias = 0.0;

	pointIndex = 0;
	distInSegment = 0.0;

	n0 = m_pN[INDEX(pointIndex  , 0)];
	n1 = m_pN[INDEX(pointIndex+1, 0)];
	e0 = m_pE[INDEX(pointIndex  , 0)];
	e1 = m_pE[INDEX(pointIndex+1, 0)];
	for(i=0; i<num; i++)
	{
		// compute the angle and force it to be in [-pi..pi] + overrunBias. 
		double theta = atan2(e1-e0, n1-n0) - bias ;
		theta = atan2(sin(theta), cos(theta)) + overrunBias;

		// now check for and correct overruns
		if(fabs(theta-prevTheta) > M_PI)
		{
			if(theta-prevTheta > M_PI)
			{
				overrunBias -= 2.0*M_PI;
				theta -= 2.0*M_PI;
			}
			else
			{
				overrunBias += 2.0*M_PI;
				theta += 2.0*M_PI;
			}
		}
		prevTheta = theta;
		thetavector[i] = theta;

		distLeft = delta;

		while(distLeft > 0.0)
		{
			n0 = m_pN[INDEX(pointIndex  , 0)];
			n1 = m_pN[INDEX(pointIndex+1, 0)];
			e0 = m_pE[INDEX(pointIndex  , 0)];
			e1 = m_pE[INDEX(pointIndex+1, 0)];
			lenSegment = hypot(n1-n0, e1-e0);

			distLeftInSegment = lenSegment - distInSegment;
			if(distLeft < distLeftInSegment)
			{
				distInSegment += distLeft;
				break;
			}

			distLeft -= distLeftInSegment;

			// don't advance if we're at the end
			pointIndex = min(pointIndex+1, m_numPoints-1);
			distInSegment = 0.0;
		}
	}
}

// This function returns a vector of length num of the speeds of the
// vehicle along this traj, dist meters ahead. output into speedvector. bias is
// subtracted from the result.
void CTraj::getSpeedVector(double dist, int num, double* speedvector, double bias)
{
	double delta = dist / (double)(num - 1);

	int i;
	int pointIndex;
	double distInSegment;

	double distLeftInSegment;
	double v0, v1;
	double n0, e0, n1, e1;
	double lenSegment;
	double distLeft;

	pointIndex = 0;
	distInSegment = 0.0;

	n0 = m_pN[INDEX(pointIndex  , 0)];
	n1 = m_pN[INDEX(pointIndex+1, 0)];
	e0 = m_pE[INDEX(pointIndex  , 0)];
	e1 = m_pE[INDEX(pointIndex+1, 0)];
	lenSegment = hypot(n1-n0, e1-e0);
	v0 = hypot(m_pN[INDEX(pointIndex  , 1)], m_pE[INDEX(pointIndex  , 1)]);
	v1 = hypot(m_pN[INDEX(pointIndex+1, 1)], m_pE[INDEX(pointIndex+1, 1)]);
	for(i=0; i<num; i++)
	{
#ifdef RECIPROCAL_SPEED
		speedvector[i] = 1.0 / (
														v0*(1.0 - distInSegment/lenSegment) +
														v1*distInSegment / lenSegment
													 ) - 1.0/bias ;
#else
		speedvector[i] =
			v0*(1.0 - distInSegment/lenSegment) +
			v1*distInSegment / lenSegment
			- bias ;
#endif

		distLeft = delta;

		while(distLeft > 0.0)
		{
			n0 = m_pN[INDEX(pointIndex  , 0)];
			n1 = m_pN[INDEX(pointIndex+1, 0)];
			e0 = m_pE[INDEX(pointIndex  , 0)];
			e1 = m_pE[INDEX(pointIndex+1, 0)];
			v0 = hypot(m_pN[INDEX(pointIndex  , 1)], m_pE[INDEX(pointIndex  , 1)]);
			v1 = hypot(m_pN[INDEX(pointIndex+1, 1)], m_pE[INDEX(pointIndex+1, 1)]);
			lenSegment = hypot(n1-n0, e1-e0);

			distLeftInSegment = lenSegment - distInSegment;
			if(distLeft < distLeftInSegment)
			{
				distInSegment += distLeft;
				break;
			}

			distLeft -= distLeftInSegment;

			// don't advance if we're at the end
			pointIndex = min(pointIndex+1, m_numPoints-1);
			distInSegment = 0.0;
		}
	}
}

double CTraj::getLength(void)
{
	return dist(0, m_numPoints-1);
}

double CTraj::dist(int from, int to)
{
	double length = 0.0;

	// forwards
	if(to > from)
	{
		for(int i=from; i<to; i++)
		{
			length += hypot(m_pN[INDEX(i+1, 0)] - m_pN[INDEX(i, 0)],
											m_pE[INDEX(i+1, 0)] - m_pE[INDEX(i, 0)]);
		}
		return length;
	}

	// backwards
	for(int i=to; i<from; i++)
	{
		length -= hypot(m_pN[INDEX(i+1, 0)] - m_pN[INDEX(i, 0)],
										m_pE[INDEX(i+1, 0)] - m_pE[INDEX(i, 0)]);
	}
	return length;
}

void CTraj::print(ostream& outstream, int numPoints)
{
  int i, d;
  
  if(numPoints == -1)
    numPoints = m_numPoints;
 
  // print a comment header at the top
  outstream << "# This is a trajectory file output by CTraj::print()." << endl;
  outstream << "# Column format is [n, nd, ndd, e, ed, edd], in SI units." << endl;

  outstream << setprecision(10);
  for(i=0; i<numPoints; i++)
    {
      for(d=0; d<m_order; d++)
	outstream << m_pN[INDEX(i, d)] << " ";
      for(d=0; d<m_order; d++)
	outstream << m_pE[INDEX(i, d)] << " ";
      outstream << endl;
    }
}

// This function prints the speed profile into the output stream specified. The
// main purpose of this function is to communicate with my (dima's) qt realtime
// plotting program. This program is a huge hack and is probably breakage-prone,
// so please tell me before changing this function
void CTraj::printSpeedProfile(ostream& outputstream)
{
  int i;

  outputstream << setprecision(5);
  for(i=0; i<m_numPoints; i+=20)
	{
		outputstream << hypot(m_pN[INDEX(i, 1)], m_pE[INDEX(i, 1)]) << '\n';
	}
	outputstream << endl;
}

//This funstion applies a speed limit to the path.
void CTraj::speed_adjust(double speedlimit,NEcoord* pState,double aspeed)
{
  const double DECEL_RATE=2;
  int nearestOnPathIndex, i;
  double nearestDistSq;
  double currentDistSq;
  //Compute the index of the point that is closest to where we are located.
  nearestDistSq = 1.0e20;
  for(i=0; i<m_numPoints; i++)
  {
    currentDistSq =
	    pow(pState->N - m_pN[INDEX(i, 0)], 2.0) +
	    pow(pState->E - m_pE[INDEX(i, 0)], 2.0);
      
    if(currentDistSq < nearestDistSq)
    {
	    nearestDistSq = currentDistSq;
	    nearestOnPathIndex = i;
    }
  }
  
  i=nearestOnPathIndex;
  double speed,t_aN,t_aE,n_aN,n_aE,distance;
  for(; i<m_numPoints; i++)
  {
    speed=
      sqrt(m_pN[INDEX(i,1)]*m_pN[INDEX(i,1)]+m_pE[INDEX(i,1)]*m_pE[INDEX(i,1)]);
    /*If the speed of the initial point is above the speed limit, then we
    need to decelerate to below the speed limit in a dynamically feasible
    way.*/
    if(aspeed>speedlimit)
    {
      //Check if the source traj is decelerating faster than our DECEL_RATE.
      if(aspeed<speed)
      {
        //Set the tangential acceleration to the decel rate.
        t_aN=-DECEL_RATE*m_pN[INDEX(i,1)]/speed;
        t_aE=-DECEL_RATE*m_pE[INDEX(i,1)]/speed;
        //Calculate acceleration that is normal to velocity.
        n_aN=
          -m_pE[INDEX(i,1)]*(m_pN[INDEX(i,1)]*m_pE[INDEX(i,2)]-
          m_pE[INDEX(i,1)]*m_pN[INDEX(i,2)])/(speed*speed);
        n_aE=
          m_pN[INDEX(i,1)]*(m_pN[INDEX(i,1)]*m_pE[INDEX(i,2)]-
          m_pE[INDEX(i,1)]*m_pN[INDEX(i,2)])/(speed*speed);
        //Reduce speed to match any deceleration from previous points.
        m_pN[INDEX(i,1)]=m_pN[INDEX(i,1)]*aspeed/speed;
        m_pE[INDEX(i,1)]=m_pE[INDEX(i,1)]*aspeed/speed;
        /*Normal acceleration is scaled down to match the new velocity and
        added to the tangential acceleration giving total acceleration*/
        m_pN[INDEX(i,2)]=t_aN+n_aN*aspeed*aspeed/(speed*speed);
        m_pE[INDEX(i,2)]=t_aE+n_aE*aspeed*aspeed/(speed*speed);
      }
      else aspeed=speed;
      //Calculate the new decelerated speed for the next point.
      if(i<m_numPoints-1)
      {
        distance=
          sqrt(pow(m_pN[INDEX(i+1,0)]-m_pN[INDEX(i,0)],2.0)+
		      pow(m_pE[INDEX(i+1,0)]-m_pE[INDEX(i,0)],2.0));
        aspeed=sqrt(max(aspeed*aspeed-2*DECEL_RATE*distance,0.));
      }
    }
    /*Once the speed is below the speed limit it is only necessary to make
    sure the path does not acellerate back above the speed limit.*/
    else if(speed>speedlimit)
    {
      //Calculate acceleration that is normal to velocity.
      n_aN=
        -m_pE[INDEX(i,1)]*(m_pN[INDEX(i,1)]*m_pE[INDEX(i,2)]-
        m_pE[INDEX(i,1)]*m_pN[INDEX(i,2)])/(speed*speed);
      n_aE=
        m_pN[INDEX(i,1)]*(m_pN[INDEX(i,1)]*m_pE[INDEX(i,2)]-
        m_pE[INDEX(i,1)]*m_pN[INDEX(i,2)])/(speed*speed);
      //The speed is reduced to the speed limit.
      m_pN[INDEX(i,1)]=m_pN[INDEX(i,1)]*speedlimit/speed;
      m_pE[INDEX(i,1)]=m_pE[INDEX(i,1)]*speedlimit/speed;
      /*Tangential acceleration is set to zero, and normal acceleration is
      scaled down to match the new velocity.*/
      m_pN[INDEX(i,2)]=n_aN*speedlimit*speedlimit/(speed*speed);
      m_pE[INDEX(i,2)]=n_aE*speedlimit*speedlimit/(speed*speed);
    }
    //Points that already have speed below the speed limit are not changed.
  }
}

// Returns the index of the closest point in this traj to the provided
// (N,E). Note: this function returns the closest (spatially) point. It doesn't
// look to see how close we actually are. So even if we're miles away from this
// traj or facing the wrong way, this function will still return just the
// spatially closest point.
int CTraj::getClosestPoint(double n, double e)
{
	int			i;

	// index of the point on the trajectory that's closest to the current system
	// point, and the squared distance from the system point to that point on the
	// trajectory
	int     nearestIndex = 0;
	double	nearestDistSq;
	double	currentDistSq;

	// Compute the nearest point on the trajectory to where we currently are
	nearestDistSq = 1.0e20;
	for(i=0; i<getNumPoints(); i++)
	{
		currentDistSq =
			pow(n - getNorthing(i),2.0) +
			pow(e	- getEasting(i) ,2.0);

		if(currentDistSq < nearestDistSq)
		{
			nearestDistSq = currentDistSq;
			nearestIndex = i;
		}
	}

	return nearestIndex;
}

// See header for documentation
int CTraj::getPointAhead(int currpoint, double dist)
{
  if( currpoint < 0 )
  {
    cerr << "CTraj::getPointAhead error: currpoint is negative" << endl;
    return 0;
  }
  if( currpoint > m_numPoints - 1 )
  {
    cerr << "CTraj::getPointAhead error: currpoint out of bounds" << endl;
    return m_numPoints-1;
  }
  if( dist < 0 )
  {
    cerr << "CTraj::getPointAhead error: dist is negative" << endl;
    return currpoint;
  }

  NEcoord here(getNorthing(currpoint), getEasting(currpoint));
  NEcoord there(getNorthing(currpoint+1), getEasting(currpoint+1));

  // initialize the return index and distance remaining
  int retind = currpoint;
  double distrem = dist;

  // While we haven't reached the desired lookahead distance and haven't 
  // exhausted the trajectory, step ahead by an index and reevaluate.
  while( distrem > 0 && retind <= getNumPoints()-2)
  {
    // subtract from the distance remaining
    distrem -= (here - there).norm();
    // step forward and reset the NEcoords to calculate distance
    retind++;
    here = there; 
    there.N = getNorthing(retind+1);
    there.E = getEasting(retind+1);
  }

  return (retind < m_numPoints) ? retind : -1 ;
}

void CTraj::prepend(CTraj* pFrom, int start, int end)
{
	int i;

	int numToPrepend = end - start + 1;
	int numToKeep = min(TRAJ_MAX_LEN - numToPrepend, m_numPoints);

	assert(numToKeep > 0);

	for(i = 0; i<m_order; i++)
	{
		memmove(&m_pN[INDEX(numToPrepend, i)], &m_pN[INDEX(0, i)], numToKeep * sizeof(double));
		memcpy(&m_pN[INDEX(0, i)],  pFrom->getNdiffarray(i)+start, numToPrepend * sizeof(double));

		memmove(&m_pE[INDEX(numToPrepend, i)], &m_pE[INDEX(0, i)], numToKeep * sizeof(double));
		memcpy(&m_pE[INDEX(0, i)],  pFrom->getEdiffarray(i)+start, numToPrepend * sizeof(double));
	}

	m_numPoints = numToKeep + numToPrepend;
}


void CTraj::append(CTraj* pFrom, int start, int end)
{
	int i;

	int numToAppend = min(TRAJ_MAX_LEN - m_numPoints, (end-start+1));

	for(i = 0; i<m_order; i++)
	{
		memcpy(&m_pN[INDEX(m_numPoints, i)], pFrom->getNdiffarray(i)+start, numToAppend * sizeof(double));
		memcpy(&m_pE[INDEX(m_numPoints, i)], pFrom->getEdiffarray(i)+start, numToAppend * sizeof(double));
	}

	m_numPoints += numToAppend;
}

TrajPoint CTraj::interpolate( NEcoord curPos )
{
  double nc,ec,nc1,ec1, nc2,ec2,dbclpo_cp, dtjnp, dtjpb,vp1,vp2,vp3,projA,projB,Nd,Ed,Ndd,Edd,A,B;
  
  int closestIndex = getClosestPoint(curPos.N, curPos.E);
 
  if (getNumPoints()==1) {  // only one point ??
    return TrajPoint(curPos.N, curPos.E, getNorthingDiff(0 ,1) , getEastingDiff(0,1) ,getNorthingDiff(0,2) , getEastingDiff(0,2));
  }


  if(closestIndex==0) {   //Closest point is the first point
    nc = getNorthing(closestIndex);
    //east cordniate closeset indes
    ec = getEasting(closestIndex);
    // north cordinate on curpos+1
    nc1 = getNorthing(closestIndex+1);
    // east cordinate on curpos+1
    ec1 = getEasting(closestIndex+1);
    
    vp2=(( ec1)- (ec))*( curPos.E-(ec))+((nc1)- (nc))*( curPos.N- (nc));
    if (vp2<=0){   // we are before the first point
      return TrajPoint(curPos.N, curPos.E, getNorthingDiff(closestIndex,1) , getEastingDiff(closestIndex,1) ,getNorthingDiff(closestIndex,2) , getEastingDiff(closestIndex,2));
    }
    else {         // we are between first and second point
      A=sqrt(pow(curPos.N - (nc),2.0) + pow(curPos.E - (ec),2.0));
      B = sqrt(pow(curPos.N - (nc1),2.0) + pow(curPos.E - (ec1),2.0));
      Nd= (A/(A+B))*getNorthingDiff(closestIndex,1) + (B/(A+B))*getNorthingDiff(closestIndex+1,1);
      Ed = (A/(A+B))*getEastingDiff(closestIndex,1) + (B/(A+B))*getEastingDiff(closestIndex+1,1);
      Ndd= (A/(A+B))*getNorthingDiff(closestIndex,2) + (B/(A+B))*getNorthingDiff(closestIndex+1,2);
      Edd = (A/(A+B))*getEastingDiff(closestIndex,2) + (B/(A+B))*getEastingDiff(closestIndex+1,2);
      return TrajPoint(curPos.N, curPos.E, Nd, Ed, Ndd, Edd);
    }
    
  }
  else if( closestIndex==getNumPoints()-1) {   // closestPoint is the lastPoint
    nc = getNorthing(closestIndex);
    //east cordniate closeset indes
    ec = getEasting(closestIndex);
    //north cordinate on curpos-1
    nc2 = getNorthing(closestIndex-1);
    //esat cordinate on curpos-1
    ec2 = getEasting(closestIndex-1);
    vp1= (( ec2)- (ec))*( curPos.E-(ec))+((nc2)- (nc))*( curPos.N- (nc));
    if (vp1<=0){    // we are after last point
      return TrajPoint(curPos.N, curPos.E, getNorthingDiff(closestIndex,1) , getEastingDiff(closestIndex,1) ,getNorthingDiff(closestIndex,2) , getEastingDiff(closestIndex,2));
    }
    else {          //we are between last and second last point
      A=sqrt(pow(curPos.N - (nc),2.0) + pow(curPos.E - (ec),2.0));
      B = sqrt(pow(curPos.N - (nc2),2.0) + pow(curPos.E - (ec2),2.0));
      Nd= (A/(A+B))*getNorthingDiff(closestIndex,1) + (B/(A+B))*getNorthingDiff(closestIndex-1,1);
      Ed = (A/(A+B))*getEastingDiff(closestIndex,1) + (B/(A+B))*getEastingDiff(closestIndex-1,1);
      Ndd= (A/(A+B))*getNorthingDiff(closestIndex,2) + (B/(A+B))*getNorthingDiff(closestIndex-1,2);
      Edd = (A/(A+B))*getEastingDiff(closestIndex,2) + (B/(A+B))*getEastingDiff(closestIndex-1,2);
      return TrajPoint(curPos.N, curPos.E, Nd, Ed, Ndd, Edd);
    }
  }
  
  
  // general case
  
  
    // north cordinate closestIndex
  nc = getNorthing(closestIndex);
  //east cordniate closeset indes
  ec = getEasting(closestIndex);
  // north cordinate on curpos+1
  nc1 = getNorthing(closestIndex+1);
  // east cordinate on curpos+1
  ec1 = getEasting(closestIndex+1);
  
  
  //north cordinate on curpos-1
  nc2 = getNorthing(closestIndex-1);
  //esat cordinate on curpos-1
  ec2 = getEasting(closestIndex-1);
  
  //distance between closest point and Curpos  
  dbclpo_cp = sqrt(pow(curPos.N - (nc),2.0) + pow(curPos.E - (ec),2.0));
  //distance between trajpoint in the middle and the index after.  
  dtjnp =  sqrt(pow((nc1) - (nc),2.0) + pow((ec1) - (nc),2.0));
  // distance between trajpoint in the middle and the index before.  
  dtjpb =  sqrt(pow((nc2) - (nc),2.0) + pow((nc2) - (nc),2.0));
  //vektor produkt between AB and AC
  vp1= (( ec2)- (ec))*( curPos.E-(ec))+((nc2)- (nc))*( curPos.N- (nc));
  //vector product BC*BD 
  vp2=(( ec1)- (ec))*( curPos.E-(ec))+((nc1)- (nc))*( curPos.N- (nc));
  //BD*BA
  vp3 = (( ec)- (ec1))*( (ec)-(ec2))+(( nc)- (nc1))*( (nc)-(nc2));
  projA = sqrt(pow(dbclpo_cp,2.0)-pow(vp1/dtjnp,2.0));
  projB = sqrt(pow(dbclpo_cp,2.0)-pow(vp2/dtjpb,2.0));
  A = dbclpo_cp;
  if (projA <= projB && vp2 >=0) {  
    
    //Give the right value on B for this circumstances, the proj os on the nextP
    B = sqrt(pow(curPos.N - (nc1),2.0) + pow(curPos.E - (ec1),2.0));
    
    Nd= (A/(A+B))*getNorthingDiff(closestIndex,1) + (B/(A+B))*getNorthingDiff(closestIndex+1,1);
    Ed = (A/(A+B))*getEastingDiff(closestIndex,1) + (B/(A+B))*getEastingDiff(closestIndex+1,1);
    Ndd= (A/(A+B))*getNorthingDiff(closestIndex,2) + (B/(A+B))*getNorthingDiff(closestIndex+1,2);
    Edd = (A/(A+B))*getEastingDiff(closestIndex,2) + (B/(A+B))*getEastingDiff(closestIndex+1,2);
  }
  
  else if (projA >= projB && vp1 >=0) {
    
    //Give the right value for b for this circumstances, the proj is on the Pbefore
    
    B = sqrt(pow(curPos.N - (nc2),2.0) + pow(curPos.E - (ec2),2.0));
        Nd= (A/(A+B))*getNorthingDiff(closestIndex,1) + (B/(A+B))*getNorthingDiff(closestIndex-1,1);
    Ed = (A/(A+B))*getEastingDiff(closestIndex,1) + (B/(A+B))*getEastingDiff(closestIndex-1,1);
    Ndd= (A/(A+B))*getNorthingDiff(closestIndex,2) + (B/(A+B))*getNorthingDiff(closestIndex-1,2);
    Edd = (A/(A+B))*getEastingDiff(closestIndex,2) + (B/(A+B))*getEastingDiff(closestIndex-1,2);
  }
  
  else {
      // return curpos om ej ngn projektion    
    
    Nd = getNorthingDiff(closestIndex,1);
    Ed = getEastingDiff(closestIndex,1);
    Ndd = getNorthingDiff(closestIndex,2);
    Edd = getEastingDiff(closestIndex,2);
    
  }
  return TrajPoint(curPos.N, curPos.E, Nd, Ed, Ndd, Edd);
  
}
  
TrajPoint::TrajPoint(double nt, double et, double ndt, double edt, double nddt, double eddt) {
  n=nt;
  e=et;
  nd=ndt;
  ed=edt;
  ndd=nddt;
  edd=eddt;
}




