/*********************************************************************************/
/* SICK LASER DRIVER V0.9                                                        */
/* Copyright (C) 2000 - Kasper Stoy - USC Robotics Labs (See README for details) */
/*********************************************************************************/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define ANGLE 100
#define RES 0.50

#include "sick_driver.h"

int main(int argc, char *argv[]) {
  unsigned char data[1024];
  int result;
  int status;

  if(argc < 2) {
    printf("Serial device must be specified on cmd line (ex /dev/ttyS9)\n");
    exit(-1);
  }

  printf("Attempting to communicated with LADAR on %s\n",argv[1]);

  SickLMS200 laserDevice; /* change this */

  result = laserDevice.Setup(argv[1]);

  printf("numDataPoints(): %d\n", laserDevice.getNumDataPoints());

  if (result==0) { // success

      status = laserDevice.LockNGetData( data );

      printf("STATUS: %04Xh\n", status);
      /* and print them */
      for(int i=1;i<laserDevice.getDataLen()+1;i+=2) {
	// print the returned values in hex
	//printf("%X ", (unsigned short int) data[i]
	//       + ((unsigned short int) data[i+1] << 8));
	// print the returned values in human-readable numbers
	printf("%hu ", (unsigned short int) data[i] 
	       + ((unsigned short int) data[i+1] << 8));
      }
  }

  laserDevice.LockNShutdown();
  return(0);
}
