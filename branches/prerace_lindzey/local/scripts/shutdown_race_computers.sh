#!/bin/bash

# script to shut down the race computers
# REQUIREMENTS: this script will need the shutdown executable in /sbin to be chmodded to allow
# the user that runs the script to run it (instead of only root).
# If chmod a+x /sbin/shutdown is executed as root on all the race computers, this script should work.
#
# Created by: Henrik Kjellander 28 Oct 2004

USER=team
COMPUTERS="titanium tantalum osmium magnesium iridium cobalt"
CMD_STRING="echo "All race computers are about to be shutdown NOW! Sorry." | wall; shutdown -h 0"

if [ ! "$1" ]; then
  echo "Script for shutting down the race computers"
  echo "Usage: ./shutdown_race_computers.sh [username]"
  echo "- username: this is the username you will use to login (default = $USER)"
  echo "NOTE: You must edit this script if you don't want to login with: $USER"
  exit 0;
fi

for comp in $COMPUTERS
do
  xterm -sl 9999 -e "ssh $USER@$comp '$CMD_STRING';"
done

echo finished!

exit 0;
