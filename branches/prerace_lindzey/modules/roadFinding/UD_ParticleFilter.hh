//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_PARTICLEFILTER
//----------------------------------------------------------------------------

#ifndef UD_PARTICLEFILTER_DECS

//----------------------------------------------------------------------------

#define UD_PARTICLEFILTER_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "UD_Error.hh"
#include "UD_Random.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef MAXSTRLEN
#define MAXSTRLEN  256
#endif

//-------------------------------------------------
// struct/class
//-------------------------------------------------

// 1-D of doubles

typedef struct 
{
  int rows;                  // currently used rows
  double *x;
  double buffsize;           // rows * sizeof(double)
  char *name;

} UD_Vector;

//-------------------------------------------------

class UD_ParticleFilter
{
  
 public:

  // variables
  
  int updates;                  ///< how many iterations have run so far

  int num_samples;               ///< number of samples  
  UD_Vector **sample;            ///< array of states sampled from prior p(state)
  UD_Vector **new_sample;        ///< resampled and updated samples
  UD_Vector **weighted_sample;   ///< weighted samples (for state estimation)
  UD_Vector *state;              ///< estimated state (sum of weighted_sample array)
  UD_Vector *moved_sample;       ///< sample after deterministic motion
  UD_Vector *weight;             ///< weights on samples (computed from p(image|state)) 
  double weight_sum;             ///< sum of p(image|state) over all samples
  UD_Vector *cumulative_weight;  ///< cumulative sum of weights: c_1 = weight_1; for i > i, c_i = c_{i-1} + weight_i 

  UD_Vector *covdiagsqrt;        ///< square roots of diagonal elements of sampling covariance matrix 
                                 ///< (i.e., covariance's off-diagonal elements are all 0)

  // functions

  UD_ParticleFilter(int, UD_Vector *, UD_Vector *);

  void initialize_prior();
  void reset();
  void update();
  int interval_membership(int, int, double);
  void print();

  // these will be implemented by derived classes only

  virtual double pf_condprob_zx(UD_Vector *) = 0;                    ///< measures "fitness" of each state
  virtual void pf_samp_prior(UD_Vector *) = 0;                       ///< generates initial distribution of states
  virtual void pf_samp_state(UD_Vector *, UD_Vector *) = 0;          ///< stochastic update of state
  virtual void pf_dyn_state(UD_Vector *, UD_Vector *) = 0;           ///< deterministic update of state

};

//-------------------------------------------------
// functions
//-------------------------------------------------

UD_Vector *make_UD_Vector(char *, int);
UD_Vector *copy_UD_Vector(UD_Vector *);
void copy_UD_Vector(UD_Vector *, UD_Vector *);
UD_Vector *copy_header_UD_Vector(UD_Vector *);
void scale_UD_Vector(double, UD_Vector *);
void sum_UD_Vector_array(UD_Vector **, int, UD_Vector *);
void print_UD_Vector(UD_Vector *);
void sample_gaussian_diagonal_covariance(UD_Vector *, UD_Vector *, UD_Vector *);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif
