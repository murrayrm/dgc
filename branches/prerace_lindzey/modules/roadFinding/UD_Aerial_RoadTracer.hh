//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_Aerial_RoadTracer
//----------------------------------------------------------------------------

#ifndef UD_AERIAL_ROADTRACER_DECS

//----------------------------------------------------------------------------

#define UD_AERIAL_ROADTRACER_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glut.h>

#include "opencv_utils.hh"

// my stuff 

#include "UD_ParticleFilter.hh"
//#include "UD_Aerial.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))

// camera calibration data; in degrees

#define DEFAULT_HFOV        45.0
#define PIX2RAD(x, w, hfov) ((hfov)*(x)/(w))     // can also use y, h, vfov

//-------------------------------------------------
// class
//-------------------------------------------------

//class UD_Aerial;

class UD_Aerial_RoadTracer : public UD_ParticleFilter
{
  
 public:

  // variables

  double easting, northing, heading;
  float init_position_variance, init_heading_variance;
  int show_particles;
  double speed;  // m / s
  int path_length;
  double **path_x_samp, **path_y_samp;
  double *path_x_state, *path_y_state;

  // Functions

  //  UD_Aerial_RoadTracer(UD_Aerial *, int, UD_Vector *, UD_Vector *);
  UD_Aerial_RoadTracer(int, UD_Vector *, UD_Vector *);

  void trace(double, double, double);

  void pf_samp_prior(UD_Vector *);
  void pf_samp_state(UD_Vector *, UD_Vector *);
  void pf_dyn_state(UD_Vector *, UD_Vector *);
  double pf_condprob_zx(UD_Vector *);


  void draw();
  void write(FILE *);
  void write(FILE *, char *);
  void iterate();

  float get_easting(UD_Vector *v)  { return v->x[0]; }  
  float get_northing(UD_Vector *v) { return v->x[1]; }  
  float get_heading(UD_Vector *v)  { return v->x[2]; }  

  float set_easting(float x, UD_Vector *v)  { v->x[0] = x; }  
  float set_northing(float x, UD_Vector *v) { v->x[1] = x; }  
  float set_heading(float x, UD_Vector *v)  { v->x[2] = x; }  

};

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
