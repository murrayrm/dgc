/**
 * DGCTab.hh
 * Revision History:
 * 07/07/2005  hbarnor  Created
 * $Id: DGCTab.hh 8530 2005-07-11 06:26:58Z hbarnor $
 */

#ifndef DGCTAB_HH
#define DGCTAB_HH

#include <gtkmm/frame.h>
#include <gtkmm/box.h>
#include <gtkmm/table.h>
#include <gtkmm/buttonbox.h>
#include <sstream>

#include "sn_msg.hh"
#include "DGCButton.hh"
#include "DGCEntry.hh"
#include "DGCLabel.hh"


#include "../plot/DGCPlot.hh"
#include "../plot/DGCPlotData.hh"

#define MAXCOLS 6
#define MAXROWS 20

using namespace std;

/**
 * Super Tab class for module Tab API. 
 * This class is the super class for creating tabs for modules. It
 * creates a 6 by 20 array of cells that the user fills with one the
 * three available options of widgets: textField, textLabel or
 * textButtons. TextFields automatically have labels and will end up
 * using two cells. All the others will only have one label.
 *
 */
class DGCTab : public Gtk::VBox
{

public:
  /**
   * Default Constructor.
   * Creates a horizontal box widget containing a 6 by 20 array of
   * cells.
   */ 
  //  DGCTab();
  
  /**
   * Constructor with a pointer to skynet instance.
   * Creates a horizontal box widget containing a 6 by 20 array of
   * cells.
   */ 
  DGCTab(skynet* messenger);
  /**
   * Destructor.
   * Handles dynamically allocated memory. Like you did not know that
   * already. 
   */
  virtual ~DGCTab();
  /**
   * Perform initializations.
   * Any extra initialization that could not be done in the
   * constructor goes here. 
   */
  virtual void init();

protected:
  /**
   * Add a single textLabel to the table.
   * Add a textLabel to the table at the
   * specified  position. 
   * @param name - not used for this call
   * @param defValue - the text to be displayed on the label
   * @param col1 - not used
   * @param row1 - not used
   * @param row - the x position for the widget 
   * @param col - the y position for the widget
   */
  void addSLabelWidget(string name, string defValue, guint col1, guint row1, guint col2, guint row2);
  /**
   * Add a pair of textLabels to the table.
   * Add a textLabel to the table at the
   * specified  position. 
   * @param name - the string displayed as the name of the value.
   * @param defvalue - the default value for the value being displayed
   * @param row1 - the x position for the name label
   * @param col1 - the y position for the name label
   * @param row - the x position for the value label
   * @param col - the y position for the value label
   */
  void addLabelWidget(string name, string defValue, guint col1, guint row1, guint col2, guint row2);
  /**
   * Add a textButton to the table.
   * Add a textButton to the table at the
   * specified  position. 
   * @param wType - the type of widget to be inserted
   * @param name - the text to be displayed on the button or label
   * @param row - the x position for the widget 
   * @param col - the y position for the widget
   */
  void addButtonWidget(string name, string defValue, guint col1, guint row1, guint col2, guint row2);
  /**
   * Add a textField without a label to the table.
   * Add a textField to the table at the specified
   * position. 
   * @param name - not used
   * @param defValue - the default value for the textField
   * @param row1 - not used
   * @param col1 - not used
   * @param row2 - the x position for the field 
   * @param col2 - the y position for the field
   */
  void addSEntryWidget(string name, string defValue, guint col1, guint row1, guint col2, guint row2);
  /**
   * Add a textField to the table.
   * Add a textField to the table at the specified
   * position. 
   * @param name - the text to displayed in the label
   * @param defValue - the default value for the textField
   * @param row1 - the x position for the label 
   * @param col1 - the y position for the label
   * @param row2 - the x position for the field 
   * @param col2 - the y position for the field
   */
  void addEntryWidget(string name, string defValue, guint col1, guint row1, guint col2, guint row2);

  /**
   * Update the text in a textField. 
   * Look-up the cell specified, if it is a textField or textLabel,
   * then update its text.
   * @param newValue - the new value to be displayed
   * @param row - the row of the cell to be changed
   * @param col - the column of the cell to be changed 
   * @return true if an update was made and false otherwise
   */
  void setWidgetValue(stringstream& newValue, guint col, guint row);

  /**
   * Set the value of the widget.
   * Get the widget at the specified position and set its value to the
   * new value.
   */
  void getWidgetValue(stringstream&  stream, guint col, guint row);
  /**
   * Add a plot to the widget. 
   * We are assuming only one plot for now.
   * @param title - the title for the plot.
   * @param xLabel - label for the x-axis of the plot.
   * @param yLabel - label for the y-axis of the plot.
   * @param (x/y)Min, (x/y)Max - the minimum and maximum ranges for the
   * x/y-axis.
   * @param major(X/Y)Ticks - increment value for major ticks on the
   * x/y-axis.
   * @param numMinor(X/Y)Ticks - number of minor ticks between two
   * consecutive major ticks.
   * @param autoscale - if set to true, the plot rescales and ignores
   * all the range information.
   * @param legend - when set, alegend is added with this text. 
   */
  void addPlot(string title, string xLabel, string yLabel, double xMin, double xMax, double majorXTick, int numMinorXTicks, double yMin, double yMax, double majorYTick, int numMinorYTicks, bool autoscale, string legend); 

  /**
   * Update the tab.
   * Updates each of the labels and entry fields with data from the
   * input struct. Superclass implementation is empty. All sub-classes
   * will need to override if they want something useful to happen.
   */
  virtual void updateTab()
  {
    // nothing to be done
  };
  /**
   * Update the client process.
   * Copy all values from the tab to the struct and send the struct
   * over the messaging sub-system. Superclass implementation is
   * empty. All sub-classes will need to override if they want something
   * useful to happen.
   */
  virtual void updateClient()
  {
    // nothing to do here
  };
  /**
   * Update plots.
   * Updates the plots with new points. Data is expected over
   * skynet. Superclass implementation is empty. All sub-classes will
   * need to override if they want something useful to happen. 
   */
  virtual void updatePlot()
  {
    // nothing to do here.
  };
private:
  /**
   * The number of rows in the table.
   */
  const guint NUM_ROWS;
  /**
   * The number of columns in the table.
   */
  const guint NUM_COLS;
  /**
   * 2-D Array of pointers to the widget.
   */
  DGCWidget* m_shadowTable[MAXCOLS][MAXROWS];
protected:
  /**
   * Dynamic array of possible plots. 
   * This is mostly here to enable memory management. 
   */
  DGCPlot *m_plotArray;
  /**
   * A table to handle placement of widgets.
   * A Gtk::Table used to organize use configured widgets.
   */
  Gtk::Table m_userTable;
  /**
   * ButtonBox to hold send button.
   */
  Gtk::HButtonBox m_hBtnBox;
  /**
   * Send Button.
   */
  Gtk::Button m_sendBtn;
  /**
   * skynet - the tabs connection to the world.
   * A pointer to a skynet object, used to create a socket for sending
   * and receiving the structs.
   */
  skynet* m_skynet;
  /**
   * Socket for sending to the world. 
   */
  int m_outputSocket;
  /**
   * Socket for receiving from the world. 
   */
  int m_inputSocket;
  /**
   * Socket for receiving plot data. 
   */
  int m_plotSocket;
 private:
  /**
   * Add a widget to the table.
   * Adds the widget passed to the table at the specified position. 
   * @param widget - the widget to be added to the table. 
   * @param posX - the x position of the widget 
   * @param posY - the y position for the widget 
   */
  void addToTable(Widget* widget, guint posX, guint posY);
    /**
   * Add a widget to the shadow table.
   * Adds the widget passed to the table at the specified position. 
   * The shadow table is a 2-D array that holds pointers to the
   * widgets. 
   * @param widget - the widget to be added to the table. 
   * @param posX - the x position of the widget 
   * @param posY - the y position for the widget 
   */
  void addToShadow(DGCWidget* widget, guint posX, guint posY);  

};

#endif
