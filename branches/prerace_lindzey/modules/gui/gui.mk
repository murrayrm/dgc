GUI_PATH=$(DGC)/modules/gui

GUI_DEPEND_LIBS = \
	$(ADRIVELIB) \
	$(CMAPLIB) \
	$(CMAPPLIB) \
	$(GGISLIB) \
	$(MAPDISPLAYLIB) \
	$(RDDFLIB) \
	$(SKYNETLIB) \
	$(TRAJLIB) \
	$(MODULEHELPERSLIB) \
	$(ROADPAINTERLIB) \
	$(PROFILERLIB) \
	$(CCOSTFUSERLIB) \
	$(CELEVFUSERLIB)

GUI_DEPEND_SOURCES = \
	$(GUI_PATH)/MapDisplaySN/MapDisplaySN.cc \
	$(GUI_PATH)/MapDisplaySN/MapDisplaySN.hh \
	\
	$(GUI_PATH)/MainDisplay/gui.h \
	$(GUI_PATH)/MainDisplay/gui.cc \
	\
	$(GUI_PATH)/MainDisplay/GuiThread.cc \
	$(GUI_PATH)/MainDisplay/GuiThread.h \
	\
	$(GUI_PATH)/StatusTab/StartTab.cc \
	$(GUI_PATH)/StatusTab/StartTab.hh \
	\
	$(GUI_PATH)/StatusTab/StatusTab.cc \
	$(GUI_PATH)/StatusTab/StatusTab.hh \
	$(GUI_PATH)/StatusTab/MultiFrame.hh \
	$(GUI_PATH)/StatusTab/MultiFrame.cc \
	$(GUI_PATH)/StatusTab/Status.hh \
	$(GUI_PATH)/StatusTab/Status.cc \
	\
	$(GUI_PATH)/guiHelpers/AllModuleTabs.hh \
	$(GUI_PATH)/guiHelpers/ModuleTab.hh \
	$(GUI_PATH)/guiHelpers/DGCButton.hh \
	$(GUI_PATH)/guiHelpers/DGCButton.cc \
	$(GUI_PATH)/guiHelpers/DGCEntry.hh \
	$(GUI_PATH)/guiHelpers/DGCEntry.cc \
	$(GUI_PATH)/guiHelpers/DGCLabel.hh \
	$(GUI_PATH)/guiHelpers/DGCLabel.cc \
	$(GUI_PATH)/guiHelpers/DGCTab.hh \
	$(GUI_PATH)/guiHelpers/DGCTab.cc \
	$(GUI_PATH)/guiHelpers/DGCWidget.hh \
	$(GUI_PATH)/guiHelpers/DGCWidget.cc \
	\
	$(GUI_PATH)/plot/DGCPlot.hh \
	$(GUI_PATH)/plot/DGCPlot.cc \
	\
	$(GUI_PATH)/MainDisplay/MainTab.h \
	$(GUI_PATH)/MainDisplay/MainTab.cc \
	\
	$(GUI_PATH)/TimberBox/CTimberBox.hh \
	$(GUI_PATH)/TimberBox/CTimberBox.cc \
	\
	$(GUI_PATH)/main.cc 

GUI_DEPEND = $(GUI_DEPEND_LIBS) $(GUI_DEPEND_SOURCES) 

#gtkmm cflags and libraries
# to avoid weird conflicts, prepending with GUI
GUI_GTKGLEXTMM_VER=1.2
GUI_GTKEXTRA_VER=2.0
GUI_GTKCPPFLAGS=`pkg-config gtkglextmm-x11-$(GUI_GTKGLEXTMM_VER) --cflags` `pkg-config gtkextra-$(GUI_GTKEXTRA_VER) --cflags`
GUI_GTKLIBS= `pkg-config gtkglextmm-x11-$(GUI_GTKGLEXTMM_VER) --libs` `pkg-config gtkextra-$(GUI_GTKEXTRA_VER) --libs`


