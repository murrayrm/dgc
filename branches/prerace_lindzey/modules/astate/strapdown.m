function [posbuffer, Xbuffer] = strapdown(imuin,gpsin)

% Pseudocode:
% 
% Variables:
% 
% Outputs:
%
% lat
% lon
% alt
%
% roll
% pitch
% yaw
%
% vgn
% vge
% vgd
%
% alpha
%
% vnx
% vny
% vnz
%
% vn = [vnx; vny; vnz];
%
% wb_ib = [dtx; dty; dtz]
% 
% Rotation Matrices:
%
% Cen (lat, lon, alpha, height)
% Cnb (roll, pitch, yaw)
% 
% Functions:
% 
% Inputs:
% dtx
% dty
% dtz
% dvx
% dvy
% dvz
% glat
% glon
% galt
% 
% Differential eqns
% fn = Cnb * [dvx dvy dvz]'                   --  Transform dv's into nav frame
% gn = Cne * calc-grav(lat,long,height)       --  Find gravity vector in nav frame
% wne = [vy/Ry, -vx/Rx, 0]'                   --  Find transport rate vector
% wni = Cne * wie                             --  Find earth rate vector
% wnb = -Cbn*( wne + wni ) + wbi
%
% wie = [0 0 2*pi/(24*60*60)]
% 
% Equations: (In some order)
% 
% Update Cne according to:
% Cne_dot = - W(wne) * Cne
% 
% Update vn according to:
% vn_dot = fn - ( W(wne) + 2W(wni) )*vn + gn
% 
% Update Cnb according to:
% Cnb_dot = - W(wnb)Cnb
% 
% 
% Unpack variables:
% Lat = 
% Lon = 
% Alt = 
% Alpha = 
% 
% Roll = 
% Pitch = 
% Yaw = 

% KF declarations;

MinR = 1.56786504e-07;


KF_X = zeros(10,1); %Initial erros -- best assumption is zero
KF_Xe = zeros(10,1); %Initial estimate -- similarly zero
KF_P = diag([MinR^2, MinR^2, 1, .05^2, .05^2, .05^2,.01^2,.01^2,.01^2,.05^2]); %Tune these later
KF_Pe = zeros(10,1); %Initial Covar Estimate (not used so initialize 0)
KF_K = zeros(10,4); %Initial Kalman Gains (not used so initialize 0)
KF_Q = diag([.001,.001,.001,.001,.001,.001,.001,.001,.001,.001]); %Tune Process Noise Later
KF_A = zeros(10,10); %Calculated based on state

KF_R = diag([MinR^2, MinR^2, 1, MinR^2, MinR^2, MinR^2]);

KF_H = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0;
	0, 1, 0, 0, 0, 0, 0, 0, 0, 0;
    0, 0, 1, 0, 0, 0, 0, 0, 0, 0;
    0, 0, 0, 1, 0, 0, 0, 0, 0, 0;
    0, 0, 0, 0, 1, 0, 0, 0, 0, 0;
	0, 0, 0, 0, 0, 1, 0, 0, 0, 0];

KF_Z = [0, 0, 0]';

%Z = [dTheta(x), dTheta(y), dH]';

% Frequency at which IMU inputs are summed:
updatefreq = 5;
correctfreq = 400;

% Time-diff for summed IMU inputs:
dt = updatefreq / 400;

% Rotation rate of earth relative to inertial frame in earth frame:
we_ie = [0 0 (2*pi/(24*60*60))]';

% Initial velocities in nav frame:
vn = [0 0 0]';

% Average first 10 gps reads to get initial position
lat = mean(gpsin(1:10,2)) * pi/180;
lon = mean(gpsin(1:10,3)) * pi/180;
height = mean(gpsin(1:10,5));
alpha = 0;

% Average first 1000 imu values to find:

% Gtmp = G in body frame
gtmp = mean(imuin(1:1000,2:4))';
% Wtmp = Earth rotation rate in body frame
wtmp = mean(imuin(1:1000,5:7))';

% Work backwards to find roll, pitch, yaw from these.
pitch = asin(gtmp(1) / norm(gtmp));
roll = asin(-gtmp(2) / (norm(gtmp)*cos(pitch)));
ctmp = nav2body(roll,pitch,0);
wtmp2 = ctmp' * wtmp;
yaw = -alpha + -atan2(wtmp2(2), wtmp2(1));

% Construct nav 2 body and body 2 nav rotation matrices:
Cbn = nav2body(roll,pitch,yaw);
Cnb = Cbn';

% Construct earth 2 nav and nav 2 earth rotation matrices:
Cne = earth2nav(lat,lon,alpha);
Cen = Cne';

% Extract lat,lon,alpha,roll,pitch,yaw from rotation matrices.
%lat = asin( - Cne(3,3) );
%lon =  atan2( -Cne(3,2), -Cne(3,1) );
%alpha = atan2( -Cne(2,3), Cne(1,3) );
%pitch = asin( - Cbn(1,3));
%yaw = atan2( Cbn(1,2), Cbn(1,1) );
%roll = atan2( Cbn(2,3), Cbn(3,3) );

gpsind = 1;
gpslastind = 0;
newGPS = 0;
correctErrors = 0;
propagateState = 0;


% ALlocate empty buffer.
posbuffer = [];
Xbuffer = [];

% Initialize imu sums to 0:
dvx = 0;
dvy = 0;
dvz = 0;
dtx = 0;
dty = 0;
dtz = 0;

%for i=3000:10000,
for i=3000:size(imuin,1),

	%DATA READS:

	%Sum up IMU values
	imutime = imuin(i,1);
	dvx = dvx + imuin(i,2);
	dvy = dvy + imuin(i,3);
	dvz = dvz + imuin(i,4);
	dtx = dtx + imuin(i,5);
	dty = dty + imuin(i,6);
	dtz = dtz + imuin(i,7);
	
	
	while ((gpsin(gpsind,1) < imutime) && gpsind < size(gpsin,1))
		gpsind = gpsind + 1;
	end;
    
    	gpsind = gpsind - 1;

    	if (gpsind <= 0)
        	gpsind = 1;
    	end;

	if (gpsind > gpslastind),
    		gpstime = gpsin(gpsind,1);
    		glat = gpsin(gpsind,2) * pi/180;
    		glon = gpsin(gpsind,3) * pi/180;
    		gheight = gpsin(gpsind,5);
            gpstmp =  [cos(alpha)   sin(alpha)   0     
                       -sin(alpha)  cos(alpha)   0
                       0            0            1]*[gpsin(gpsind,6); gpsin(gpsind,7); gpsin(gpsind,8)];
            gvn = gpstmp(1);
            gve = gpstmp(2);
            gvd = gpstmp(3);
            ggdop = gpsin(gpsind,9);
		newGPS = 1;
		gpslastind = gpsind;
	end;

	if (mod(i,updatefreq) == 0),
		propagateState = 1;
	end;

	if (mod(i,correctfreq) == 0),
		correctErrors = 1;
	end;
	
    	%Only update stuff on updatefreq sums
	if (propagateState == 1),
	
        	% fn is effective accelerations in nav frame
		fn = Cnb * [dvx dvy dvz]' / dt;
		
        	% Rotation of body frame relative to inertial frame in body frame
		wb_ib = [dtx dty dtz]' / dt;
		
        	% Calculate gravity vector:
		gn = calcGrav(lat, height);

        	% Calculate transport rate:
		wn_en = calcwne(lat,height,alpha,vn(1),vn(2));
		if sum(imag(wn_en) ~= 0),
			display 'wn_en output'
			wn_en
			lat
			height
			alpha
			vn(1)
			vn(2)
			break
		end;
		
		
        	% Find earth rate in nav frame:
		wn_ie = Cne * we_ie;
		
        	% Calculate rotations rates in different frames:
		wn_in = wn_ie + wn_en;
		
		wn_bn = wn_in - Cnb * wb_ib; 
		
		% Now, update vn, Cne, and Cnb according to:
        	%
		%vn_dot = fn - (W(wne) + 2 * W(wni) )*vn + gn;
		%Cne_dot = - W(wne) * Cne;
		%Cnb_dot = - W(wnb) * Cnb;
		
		vdot = fn - (W(wn_en) + 2 * W(wn_ie) )*vn + gn;
		
		vn  = vn + dt*( vdot );
		
        	height = height + dt * vn(3);

		Cne = MatDiffEqStep(wn_en * dt) * Cne;
		Cen = Cne';
		
		Cnb = MatDiffEqStep(wn_bn * dt) * Cnb;
		Cbn = Cnb';

		if or(sum(imag(Cen) ~= 0), sum(imag(Cnb) ~= 0)),
			display 'In propagate state'
			wn_en
			Cen
			wn_bn
			Cnb
			break
		end;

		%Cne
		%Cbn
		
        	% Extract information from rotation matrices
		lat = asin( - Cne(3,3) );
		lon =  atan2( -Cne(3,2), -Cne(3,1) );
		alpha = atan2( -Cne(2,3), Cne(1,3) );
		pitch = asin( - Cbn(1,3));
		yaw = atan2( Cbn(1,2), Cbn(1,1) );
		roll = atan2( Cbn(2,3), Cbn(3,3) );

		if sum(imag(lat) ~= 0),
			display 'lat output'
			lat
			Cne
			break
		end;

		R = 6378137; %This should not be CONSTANT!!!!!
        
		%Calculate KF_A:
		vx = vn(1);
		vy = vn(2);
		vz = vn(3);
		px = wn_en(1);
		py = wn_en(2);
		pz = wn_en(3);
		wx = wn_in(1);
		wy = wn_in(2);
		wz = wn_in(3);
		ox = wn_ie(1);
		oy = wn_ie(2);
		oz = wn_ie(3);
		fx = fn(1);
		fy = fn(2);
		fz = fn(3);
		magg = norm(gn);
		

		dXdt = [
			0, 0, vy/R^2, 0, -1/R, 0, 0, 0, 0, -py;
			0, 0, -vx/R^2, 1/R, 0, 0, 0, 0, 0, px;
			0, 0, 0, 0, 0, 1, 0, 0, 0, 0;
			-2*(vy*oy + vz*oz), 2*vy*ox, -vz*vx/R^2, -vz/R, 2*oz, -(py + 2*oy), 0, -fz, fy, 2*vz*ox;
			2*vx*oy, -2*(vx*ox + vz*oz), -vy*vz/R^2, -2*oz, -vz/R, (px + 2*ox), fz, 0, -fx, 2*vz*oy;
			2*vx*oz, 2*vy*oz, (vx^2 + vy^2)/R^2 + 2*magg/R, 2*(py + oy), -2*(px + ox), 0, -fy, fx, 0, -2*(vx*ox + vy*oy);
			0, -oz, vy/R^2, 0, -1/R, 0, 0, oz, -wy, oy;
			oz, 0, -vx/R^2, 1/R, 0, 0, -oz, 0, wx, -ox;
			-oy, ox, 0, 0, 0, 0, wy, -wx, 0, 0;
			py, -px, 0, 0, 0, 0, 0, 0, 0, 0; ];


 		KF_A = dXdt * dt + eye(10);

		% Propagate KF Terms:
		KF_P = KF_A * KF_P * KF_A' + KF_Q;

		KF_X = KF_A * KF_X;
		
        	% Fill buffer:
		posbuffer = [posbuffer; [imutime,gpstime,glat,glon,gheight,lat,lon,alpha,height,roll,pitch,yaw,vn(1),vn(2),vn(3)]];
		Xbuffer = [Xbuffer; KF_X'];
          
        	% Zero sums:
		dvx = 0;
		dvy = 0;
		dvz = 0;
		dtx = 0;
		dty = 0;
		dtz = 0;

		propagateState = 0;
    end;
    
	if (newGPS == 1),

		% Meas Updates
		%KF_Z = Calculation of errors here based on more black magic...
		%Position Errors:
		CneMeas = earth2nav(glat,glon,alpha);
		CenMeas = CneMeas';
        tmp = eye(3) - (Cne * CenMeas);
        dTheta = UnW(tmp);
        dVn = [gvn - vn(1); gve - vn(2); gvd - vn(3)];
        
		dH = gheight - height;

		KF_Z = [dTheta(1), dTheta(2),  dH, dVn(1), dVn(2), dVn(3)]';

		KF_R = diag([MinR^2, MinR^2, 1, MinR^2, MinR^2, MinR^2]); %Read this out of GPS in future

        KF_K = KF_P * KF_H' * (KF_H * KF_P * KF_H' + KF_R)^-1;
        
		KF_X = KF_X + KF_K * (KF_Z - KF_H * KF_X);

		KF_P = (eye(10) - KF_K * KF_H)*KF_P;

		newGPS = 0;
	end;

	%correctErrors = 0;
    
	if (correctErrors == 1),
		dTheta = [KF_X(1); KF_X(2); KF_X(10)];
		dPhi   = [KF_X(7); KF_X(8); KF_X(9)];
		dH = KF_X(3);
		dVn = [KF_X(4); KF_X(5); KF_X(6)];
        
        Cne = MatDiffEqStep(-dTheta)*Cne;
        Cen = Cne'; 
        
		if (isnan(dTheta))
            break;
        end;

		height = height + dH;

 		vn = vn + dVn;

		lat = asin( - Cne(3,3) );
		lon =  atan2( -Cne(3,2), -Cne(3,1) );
		alpha = atan2( -Cne(2,3), Cne(1,3) );
		pitch = asin( - Cbn(1,3));
		yaw = atan2( Cbn(1,2), Cbn(1,1) );
		roll = atan2( Cbn(2,3), Cbn(3,3) );

        KF_X = [0; 0; 0; 0; 0; 0; KF_X(7); KF_X(8); KF_X(9); 0]; 
        
		correctErrors = 0;
	end;

end;

%-------------------------------------------------------------------------%
% FUNCTIONS
%-------------------------------------------------------------------------%

function output = W(x),
output = [0, -x(3, 1), x(2,1);
          x(3,1), 0, -x(1,1);
          -x(2,1), x(1,1), 0];

function output = UnW(x),
output = [-x(2,3),x(1,3),-x(1,2)]';

function m = earth2nav(lat,lon,alpha),

rotlon = [
     cos(lon)    sin(lon)       0
    -sin(lon)    cos(lon)       0
        0           0           1
    ];

rotlat = [
     cos(lat)       0        sin(lat)
        0           1           0
    -sin(lat)       0        cos(lat)
    ];

chngaxis = [
        0           0           1
        0           1           0
       -1           0           0
    ];

 rotalpha =  [cos(alpha)   sin(alpha)   0     
             -sin(alpha)  cos(alpha)   0
              0            0            1];
                   

m = rotalpha * chngaxis * rotlat * rotlon;


function m = nav2body(roll,pitch,yaw),

rotyaw = [
     cos(yaw)    sin(yaw)       0
    -sin(yaw)    cos(yaw)       0
        0           0           1
    ];

rotpitch = [
     cos(pitch)     0      -sin(pitch)
        0           1           0
     sin(pitch)     0       cos(pitch)
    ];

rotroll = [
        1           0           0
        0       cos(roll)   sin(roll)
        0      -sin(roll)   cos(roll)
    ];

m = rotroll * rotpitch * rotyaw;

function g = calcGrav(lat, height)
    epsilon = 0.0818191908426;
    gWGS0 = 9.7803267714;
    gWGS1 = 0.00193185138639;
    Re = 6378137.;
    g0 = gWGS0*(1+gWGS1*sin(lat)^2)/sqrt(1 - epsilon^2*sin(lat)^2);
    g = [0, 0, g0*(1+2*height/Re)]';
   
function wneout = calcwne(lat, height, alpha, vx, vy)
    epsilon = 0.0818191908426;
    Re = 6378137.;
    Rmeridian = Re*(1-epsilon^2)/(1-epsilon^2*sin(lat)^2)^(3/2);
    Rnormal = Re/sqrt(1-epsilon^2*sin(lat)^2);
    rhox = vy*(cos(alpha)^2/(Rnormal + height) + sin(alpha)^2/(Rmeridian + height)) - vx*sin(alpha)*cos(alpha)*(1/(Rmeridian + height) - 1/(Rnormal + height));
    rhoy = -vx*(sin(alpha)^2/(Rnormal + height) + cos(alpha)^2/(Rmeridian + height)) + vy*sin(alpha)*cos(alpha)*(1/(Rmeridian + height) - 1/(Rnormal + height));
    wneout = [rhox, rhoy, 0]';
 
function rot = MatDiffEqStep(w),
    if (norm(w) > 0),
        nw = norm(w);
        w = w / nw;
        what = [0, -w(3), w(2);
                w(3), 0, -w(1);
                -w(2), w(1), 0;];
        rot = eye(3) + what * sin(-nw) + what^2 * (1 - cos(-nw));
    else,
        rot = eye(3);
    end;

%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% Only comments below this line!
%
% SCRATCH TEST CODE:    
% Cne = earth2nav(pi/4, pi/4, 0);
% Cbn = nav2body(pi/8, pi/8, pi/4)
% 
% figure(1);
% close;
% 
% %Plot Earth Axes:
% [x,y,z] = sphere(10);
% surfl(x*3,y*3,z*3)
% 
% hold on;
% plot3([-4,4],[0,0],[0,0],'r--');
% plot3([0,0],[-4,4],[0,0],'g--');
% plot3([0,0],[0,0],[-4,4],'b--');
% hold off;
% 
% 
% Pt0 = [0;0;-3.5];
% Pt1 = [1;0;0];
% Pt2 = [0;1;0];
% Pt3 = [0;0;1];
% 
% Pt0 = Cne'*Pt0
% Pt1 = Cne'*Pt1 + Pt0
% Pt2 = Cne'*Pt2 + Pt0
% Pt3 = Cne'*Pt3 + Pt0
% 
% hold on;
% plot3([Pt0(1),Pt1(1)],[Pt0(2),Pt1(2)],[Pt0(3),Pt1(3)],'r-');
% plot3([Pt0(1),Pt2(1)],[Pt0(2),Pt2(2)],[Pt0(3),Pt2(3)],'g-');
% plot3([Pt0(1),Pt3(1)],[Pt0(2),Pt3(2)],[Pt0(3),Pt3(3)],'b-');
% hold off;
% 
% axis equal;
