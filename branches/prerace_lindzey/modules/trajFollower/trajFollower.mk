TRAJFOLLOWER_PATH = $(DGC)/modules/trajFollower

TRAJFOLLOWER_DEPEND_LIBS = $(SPARROWLIB) \
                           $(TRAJLIB) \
                           $(PIDCONTROLLERLIB) \
                           $(CPIDLIB) \
                           $(SKYNETLIB) \
                           $(MODULEHELPERSLIB) \
                           $(ADRIVELIB) \
                           $(TIMBERLIB) \
                           $(RDDFLIB) \
                           $(RDDFPATHGENLIB) \
			   $(SUPERCONCLIENTLIB)

TRAJFOLLOWER_DEPEND_SOURCE = \
                           $(TRAJFOLLOWER_PATH)/TrajFollower.hh \
                           $(TRAJFOLLOWER_PATH)/TrajFollowerMain.cc \
                           $(TRAJFOLLOWER_PATH)/sparrow.cc \
                           $(TRAJFOLLOWER_PATH)/sparrow.dd \
                           $(TRAJFOLLOWER_PATH)/trajFollowerTabSpecs.hh

TRAJFOLLOWER_DEPEND = $(TRAJFOLLOWER_DEPEND_LIBS) \
                      $(TRAJFOLLOWER_DEPEND_SOURCE)
