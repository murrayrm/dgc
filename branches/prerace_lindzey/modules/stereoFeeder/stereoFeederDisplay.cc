#include "stereoFeeder.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

double exposureTest;
int exposureToggle;
double expVal_left, expVal_right;
double expRefVal;
double avgBrightness;

extern int QUIT;
extern int PAUSE;
extern int STEP;
int RESET=0;
extern int CLEAR;
extern int RESEND;

extern double time_lock;
extern double time_cap;
extern double time_rect;
extern double time_disp;
extern double time_cloud;
extern double time_map;
extern double time_log;
extern double time_total;

char source[15]="Cameras";
char cam_pair[15]="Short";
char new_source[15]="Cameras";
char new_format[10];

extern char optFormat[10];
extern char logFilenamePrefix[200];
extern char sourceFilenamePrefix[200];

int source_width;
int source_height;

int corrsize;
int thresh;
int ndisp;
int offx;
int svsUnique;
int multi;

int blobActive;
int blobTresh;
int blobDispTol;

double minRange, maxRange, maxRelativeAltitude, minRelativeAltitude;

int sunDetection;
int tiltWidth;
int aspectWidth;

int subwindow_x_off;
int subwindow_y_off;
int subwindow_width;
int subwindow_height;
int y_off_add;
int pitchActive;

extern double time_ms;
extern double rate;

extern int logFrames, logRect, logDisp, logState, logMaps, logTime, logPoints;
extern int optPair;
extern int optDisplay;
extern int nbrSentMessages;
extern int isPaused;
extern int optUseCameras;

int CHANGED=0;
int DISPLAY_DISP=0;
int DISPLAY_RECT=0;

int current_frame;

int UpdateCount = 0;
int TimeSec = 0;
int TimeUSec = 0;

#include "vddtable.h"

void StereoFeeder::UpdateSparrowVariablesLoop() {

  TimeSec = (int)(((double)m_state.Timestamp)/1000000);
  TimeUSec = m_state.Timestamp - TimeSec;

  if(CHANGED==1) {
    processObject.setSVSParams(corrsize, thresh, ndisp, offx, svsUnique, multi, subwindow_x_off, subwindow_y_off+y_off_add, subwindow_width, subwindow_height);
    if(strcmp(new_source, "cam")==0 || strcmp(new_source, "c")==0 ||
       strcmp(new_source, "Cam")==0 || strcmp(new_source, "C")==0) {
      sprintf(source, "Cameras");
    } 
    else if(strcmp(new_source, "sim")==0 || strcmp(new_source, "s")==0) {
      sprintf(source, "File");
    }

    if(strcmp(new_format, "bmp")==0 || strcmp(new_format, "b")) {
      sprintf(optFormat, "bmp");
    } 
    else if(strcmp(new_format, "jpg")==0 || strcmp(new_format, "j")) {
      sprintf(optFormat, "jpg");
    }

    if(exposureToggle) {
      int exp_add = processObject.getExposureYOff();
      int exp_y = subwindow_y_off + exp_add;
      int exp_h =480 -exp_y;
      if (exp_h > subwindow_height){
	exp_h = subwindow_height;
      }
      sourceObject.startExposureControl(cvRect(subwindow_x_off, exp_y, subwindow_width, exp_h));
    } 
    else {
      sourceObject.stopExposureControl();
    }
	
    processObject.setNormalYOff(subwindow_y_off);
    processObject.setPitchActive(pitchActive);	
    processObject.setBlobParams(blobActive, blobTresh, blobDispTol);
    processObject.setCutoffParams(minRange, maxRange, maxRelativeAltitude, minRelativeAltitude);
    processObject.setSunDetectionParams(sunDetection, aspectWidth, tiltWidth);
    sourceObject.setExposureRefValue(expRefVal);

    CHANGED=0;
  }

  if(optPair == SHORT_RANGE) {
    sprintf(cam_pair, "Short Range");
  } 
  else if(optPair == LONG_RANGE) {
    sprintf(cam_pair, "Long Range");
  }
  
  exposureToggle = sourceObject.getExposureControlStatus();
  expVal_left = sourceObject.getExposureScaleValue(LEFT_CAM);
  expVal_right = sourceObject.getExposureScaleValue(RIGHT_CAM);  
  expRefVal = sourceObject.getExposureRefValue();

  sprintf(new_source, "%s", source);
  sprintf(new_format, "%s", optFormat);

  if(DISPLAY_DISP==1 && optDisplay==1) {
    processObject.showDisp();
    DISPLAY_DISP = 0;
  }

  if(DISPLAY_RECT == 1 && optDisplay==1) {
    processObject.showRect();
    DISPLAY_RECT = 0;
  }

  if(RESET == 1) {
    sourceObject.resetPairIndex();
    processObject.resetPairIndex();
    RESET = 0;
  }
  
  if(RESEND == 1) {
    int socket_num_mean = m_skynet.get_send_sock(SNstereoDeltaMapMean);
    int socketNumFused  = m_skynet.get_send_sock(SNstereoDeltaMapFused);
    unsigned long long timestamp;
    DGCgettime(timestamp);
    DGClockMutex(&mapMutex);
    CDeltaList* deltaList = stereoMap.serializeFullMapDelta<double>(layerID_stereoAvgDbl, timestamp);
    ++nbrSentMessages;
    SendMapdelta(socket_num_mean, deltaList);
    stereoMap.resetDelta<double>(layerID_stereoAvgDbl);
    deltaList = stereoMap.serializeDelta<CElevationFuser>(layerID_fusedElev);
    SendMapdelta(socketNumFused, deltaList);
    stereoMap.resetDelta<CElevationFuser>(layerID_fusedElev);
    DGCunlockMutex(&mapMutex);		
    RESEND = 0;
  }
  
  if(CLEAR == 1) {
    DGClockMutex(&mapMutex);
    stereoMap.clearMap();
    DGCunlockMutex(&mapMutex);
    CLEAR = 0;
  }
  
  source_width = sourceObject.width();
  source_height = sourceObject.height();

  corrsize = processObject.corrsize();
  thresh = processObject.thresh();
  ndisp = processObject.ndisp();
  offx = processObject.offx();
  svsUnique = processObject.svsUnique();
  multi = processObject.multi();

  blobActive = processObject.blobActive();
  blobTresh = processObject.blobTresh();
  blobDispTol = processObject.blobDispTol();

  minRange = processObject.getMinRange();
  maxRange = processObject.getMaxRange();
  maxRelativeAltitude = processObject.getMaxRelativeAltitude();
  minRelativeAltitude = processObject.getMinRelativeAltitude();

  sunDetection = processObject.sunDetection();
  tiltWidth = processObject.tiltWidth();
  aspectWidth = processObject.aspectWidth();

  pitchActive = processObject.getPitchActive();
  y_off_add = processObject.getSubwindowYOff();
  subwindow_x_off = processObject.subWindow.x;
  subwindow_y_off = processObject.getNormalYOff();
  subwindow_width = processObject.subWindow.width;
  subwindow_height = processObject.subWindow.height;

  current_frame = sourceObject.pairIndex();

  UpdateCount ++;
}

void StereoFeeder::SparrowDisplayLoop() {

  dbg_all = 0;

  if (dd_open() < 0) exit(1);
  dd_bindkey('Q', user_quit);
  dd_bindkey('r', user_reset);
  dd_bindkey('R', user_reset);
  dd_bindkey('S', user_step);
  dd_bindkey('s', user_step);
  dd_bindkey('p', user_pause);
  dd_bindkey('P', user_pause);
  dd_bindkey('l', log_base);
  dd_bindkey('L', log_base);
  dd_bindkey('d', display_all);
  dd_bindkey('D', display_all);
  dd_bindkey('C', user_clear);
  dd_bindkey('c', user_clear);

  dd_usetbl(vddtable);

  cout << "Sparrow display should show up in 1 sec" << endl;
  sleep(1); // Wait a bit, because other threads will print some stuff out
  dd_loop();
  dd_close();
  PAUSE = 1;
  sleep(1);
  QUIT = 1;
}


int user_quit(long arg) {
  PAUSE = 1;
  sleep(1);
  return DD_EXIT_LOOP;
}


int user_pause(long arg) {
  if(PAUSE) {
    PAUSE = 0;
  } 
  else {
    PAUSE = 1;
  }
  return 1;
}


int user_step(long arg) {
  STEP=0;
  return 1;
}

int user_reset(long arg) {
  RESET = 1;
  return 1;
}

int change(long arg) {
  CHANGED = 1;
  return 1;
}


int user_clear(long arg) {
  CLEAR = 1;
  return 1;
}


int user_resend(long arg) {
  RESEND = 1;
  return 1;
}


int display_all(long arg) {
  display_rect(arg);
  display_disp(arg);
  return 1;
}


int display_disp(long arg) {
  DISPLAY_DISP = 1;
  return 1;
}


int display_rect(long arg) {
  DISPLAY_RECT = 1;
  return 1;
}


int log_all(long arg) {
  if(!logFrames || 
     !logRect || 
     !logDisp || 
     !logState ||
     !logMaps ||
     !logTime ||
     !logPoints) {
    logFrames = 1;
    logRect = 1;
    logDisp = 1;
    logState = 1;
    logMaps = 1;
    logTime = 1;
    logPoints = 1;
  } 
  else {
    logFrames = 0;
    logRect = 0;
    logDisp = 0;
    logState = 0;
    logMaps = 0;
    logTime = 0;
    logPoints = 0;
  }
  return 1;
}


int log_base(long arg) {
  if(!logFrames || 
     !logState) {
    logFrames = 1;
    logState = 1;
  } 
  else {
    logFrames = 0;
    logState = 0;
  }
  return 1;
}


int log_quickdebug(long arg) {
  if(!logDisp) {
    logDisp = 1;
  } 
  else {
    logDisp = 0;
  }
  return 1;
}


int log_alldebug(long arg) {
  if(!logRect || 
     !logDisp || 
     !logMaps ||
     !logTime ||
     !logPoints) {
    logRect = 1;
    logDisp = 1;
    logMaps = 1;
    logTime = 1;
    logPoints = 1;
  } 
  else {
    logRect = 0;
    logDisp = 0;
    logMaps = 0;
    logTime = 0;
    logPoints = 0;
  }
  return 1;
}


int log_none(long arg) {
  logFrames = 0;
  logRect = 0;
  logDisp = 0;
  logState = 0;
  logMaps = 0;
  logTime = 0;
  logPoints = 0;
  return 1;
}

