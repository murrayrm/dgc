#include "sn_msg.hh"
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#define BUFSIZE 1024

int main(int argc, char** argv){
  int sn_key;
  char* default_key = getenv("SKYNET_KEY");
  if (2 == argc)
  {
    sn_key  = atoi(argv[1]);
  }
  else if (default_key != NULL )
  {
    sn_key = atoi(default_key);
  }
  else
  {
    printf("usage: sn_get_test key\nDefault key is $SKYNET_KEY\n");
    exit(2);
  }
  
  modulename myself = MODmodlist;
  modulename sender = ALLMODULES;
  skynet Skynet(myself, sn_key);
  sn_msg myinput = SNmodlist;
 
  alarm(2 * CHIRP_PERIOD / 1000000);
    
  int sock1 = Skynet.listen(myinput, sender);
  char* buffer[BUFSIZE];
  long long starttime, now;
  SNgettime(&starttime);
  now = starttime;
  printf("name\t\tlocation\tid\t\tkey\n");
  while(now - starttime < CHIRP_PERIOD)
  {
    int size = Skynet.get_msg(sock1, buffer, BUFSIZE, 0);
    unsigned int i;
    SNgettime(&now);
    if (now - starttime < CHIRP_PERIOD)
    {
      for(i=0; i<size/sizeof(ml_elem) ;i++)
      {
        ml_elem* mod = ((ml_elem*)buffer + i);
        struct in_addr addr;
        addr.s_addr = (in_addr_t)htonl((mod->location));
        printf("%s\t%s\t%u\t%u\n", modulename_asString(mod->name), inet_ntoa(addr), mod->id, sn_key); 
      }
    }
  }
  return 0;
  }
