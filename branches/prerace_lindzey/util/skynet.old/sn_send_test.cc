#include "sn_msg.hh"
#include <string.h>
#include <stdlib.h>

int main(int argc, char** argv){
  int sn_key;
  char* default_key = getenv("SKYNET_KEY");
  if (2 == argc)
  {
    sn_key  = atoi(argv[1]);
  }
  else if (default_key != NULL )
  {
    sn_key = atoi(default_key);
  }
  else
  {
    printf("usage: sn_send_test key\nDefault key is $SKYNET_KEY\n");
    exit(2);
  }
  
  modulename myself = SNstereo;
  sn_msg myoutput = SNpointcloud;
 
  skynet Skynet(myself, sn_key);
  printf("My module id is: %u\n", Skynet.get_id());
  int socket = Skynet.get_send_sock(myoutput);
  char* mymsg = "Hello, World!";
  Skynet.send_msg(socket, (void*)mymsg, strlen(mymsg)+1, 0);
  return 0;
  }
