#include <CTrajPainter.hh>
#include "fusionMapper.hh"
#include "CCostFuser.hh"

#define MAX_PRECISION 1e-8

// Construct things.
CTrajPainter::CTrajPainter()
  : costFuser(0)
{
  // If we need to construct it, it goes here.
}

// Destruct things.
CTrajPainter::~CTrajPainter() {
  // If we need to destroy it, it goes here.
}

// Note: paints the entire map!
int CTrajPainter::PaintTraj(CMap *cmap, int layer, CTraj *pTraj, double width, bool useDeltas) {
  NEcoord map1, map2, traj1, traj2;
  double speed, ve, vn;
  int numPoints = pTraj->getNumPoints();
  
  trajLayer = layer;
  trajMap = cmap;
  useDeltasFlag = useDeltas;
  
  map1.N = trajMap->getWindowBottomLeftUTMNorthing();
  map1.E = trajMap->getWindowBottomLeftUTMEasting();
  map2.N = trajMap->getWindowTopRightUTMNorthing();
  map2.E = trajMap->getWindowTopRightUTMEasting();

  for(int i = 0; i < (numPoints-1); i++) {
    // Read in each waypoint series from the traj
    traj1.N = pTraj->getNorthing(i);
    traj1.E = pTraj->getEasting(i);
    traj2.N = pTraj->getNorthing(i+1);
    traj2.E = pTraj->getEasting(i+1);

    vn = pTraj->getNorthingDiff(i,1);
    ve = pTraj->getEastingDiff(i,1);
    speed = sqrt(ve*ve+vn*vn);

    // Next, run checking to ensure that the trajectory segments
    // are actually inside the map.

    // Eliminate segments not nearby first.
    if (((traj1.N < map1.N) && (traj2.N < map1.N)) ||
	((traj1.N > map2.N) && (traj2.N > map2.N)) ||
	((traj1.E < map1.E) && (traj2.E < map1.E)) ||
	((traj1.E > map2.E) && (traj2.E > map2.E))) { 
      continue;
    }
	  
    // Next, draw ones with at least one end inside.
    if (((between(traj1.N, map1.N, map2.N))&&(between(traj1.E, map1.E, map2.E))) ||
	((between(traj2.N, map1.N, map2.N))&&(between(traj2.E, map1.E, map2.E)))) {
      DrawSegment(map1, map2, traj1, traj2, speed, width);
      continue;
    }
    
    // Lastly, check the difficult ones to see if they cross the box or not.
    if (LineCross(map1, map2, traj1, traj2) == true) {
      DrawSegment(map1, map2, traj1, traj2, speed, width);
    }
  }
  return 0;
}

// A lower level function, this is called by PaintDelta() to
// draw a given segment in cells.
int CTrajPainter::DrawSegment(NEcoord map1, NEcoord map2,
			       NEcoord traj1, NEcoord traj2, double speed, double width) {
  // Used to want to define MAX_PRECISION as 1/max(double), but then the case 1
  // and case 2 will never get any use, and they're good enough for what we need.
  
  double resRows = trajMap->getResRows();
  double resCols = trajMap->getResCols();

  // Prepare parts of slope.
  double dy = traj2.N-traj1.N;
  double dx = traj2.E-traj1.E;
  
  // Case 1: Horizontal segment.
  if (fabs(dy) < MAX_PRECISION) {
    if (traj2.E > traj1.E) {
      for (double i = traj1.E; i <= traj2.E; i += resCols) { 
	if(between(i, map1.E, map2.E) || i == map1.E || i == map2.E) {  
	  PaintBlurredPoint(traj1.N, i, dy, dx, width, speed);
	}
      } 
    }
    else {
      for(double i = traj2.E; i <= traj1.E; i += resCols) {
	if(between(i, map1.E, map2.E) || i == map1.E || i == map2.E) { 
	  PaintBlurredPoint(traj1.N, i, dy, dx, width, speed);
	}
      }
    }
    return 0;
  }

  // Case 2: Vertical segment
  if (fabs(dx) < MAX_PRECISION) {    
    if(traj2.N > traj1.N) {
      for(double i = traj1.N; i <= traj2.N; i += resRows) {
	if(between(i, map1.N, map2.N) || i == map1.N || i == map2.N) {
	  PaintBlurredPoint(i, traj1.E, dy, dx, width, speed);
	}
      } 
    }
    else {
      for(double i = traj2.N; i <= traj1.N; i += resRows) {
	if(between(i, map1.N, map2.N) || i == map1.N || i == map2.N) {  
	  PaintBlurredPoint(i, traj1.E, dy, dx, width, speed);
	}
      }
    }
    return 0;
  }
  
  // Case 3: Other slant
  // (This is the general case.)
  double m = dy/dx; 
  double xShift = resCols/m;
  double yShift = resRows*m;
  double signN = (traj1.N>traj2.N? -1: 1);
  double signE = (traj1.E>traj2.E? -1: 1);
  
  
  // Find intersection point with 1st row and column--these determine the
  // offset that is combined with the shift above.
  NEcoord firstNIntercept, firstEIntercept;
  
  trajMap->roundUTMToCellBottomLeft(traj1.N, traj1.E, &firstNIntercept.N, &firstEIntercept.E);
  firstNIntercept.N += (dy > 0 ? resRows : 0);
  firstEIntercept.E += (dx > 0 ? resCols : 0);
  firstNIntercept.E = (firstNIntercept.N - traj1.N)/m + traj1.E;
  firstEIntercept.N = m*(firstEIntercept.E - traj1.E) + traj1.N;
  
  int xCounter = 0, yCounter = 0;
  double t, tx, ty;
  NEcoord xPoint, yPoint;
  
  // This is an algorithm to paint as a line intersects a grid.  See
  // bug 1450 for a fuller explanation.
  for(t = 0; t <= distance(traj1, traj2);) {
    xPoint.N = firstEIntercept.N + signE*xCounter*yShift;
    xPoint.E = firstEIntercept.E + signE*xCounter*resCols;
    yPoint.N = firstNIntercept.N + signN*yCounter*resRows;
    yPoint.E = firstNIntercept.E + signN*yCounter*xShift;
    tx = distance(traj1, xPoint);
    ty = distance(traj1, yPoint);
    if (tx < ty) {
      t = tx;
      if(isPointInBox(xPoint, map1, map2)) {
	PaintBlurredPoint((xPoint.N + resRows/2), xPoint.E, dy, dx, width, speed);
      }
      xCounter++;
    } else {
      t = ty;
      if(isPointInBox(yPoint, map1, map2)) {
	PaintBlurredPoint(yPoint.N, (yPoint.E + resCols/2), dy, dx, width, speed);
      }
      yCounter++;
    }
  }   
  return 0;
}  

// A short function to tell if a point is in a box or not.
int CTrajPainter::isPointInBox(NEcoord point, NEcoord map1, NEcoord map2) {
  if(((point.N > map1.N) && (point.N > map2.N)) ||
     ((point.N < map1.N) && (point.N < map2.N)) ||
     ((point.E > map1.E) && (point.E > map2.E)) ||
     ((point.E < map1.E) && (point.E < map2.E))) {
    return 0;
  }
  return 1;
}

int CTrajPainter::LineCross(NEcoord map1, NEcoord map2,
			     NEcoord traj1, NEcoord traj2) {
  // This algorithm is used to determine whether a line with *both* ends 
  // outside a box intersects that box.  Simply, it intersects the box 
  // iff it intersects the diagonals of the box.  Thus, we take an 
  // algorithm that determines if two lines intersect, and run it twice.

  // Parameterize each line segment as a line, with the parameters s and t
  // varying from 0 to 1 inside the segment boundaries and other values
  // outside.
  // Algorithm found at vb-helper.com, adapted.  Thanks, Google!
  
  double dMapN = map2.N-map1.N;
  double dMapE = map2.E-map1.E;
  double dTrajN = traj2.N-traj1.N;
  double dTrajE = traj2.E-traj1.E;
  double swwd = dMapN*dTrajE-dTrajN*dMapE; // Slope Written Without Divisions
  // (We do this to avoid divide-by-zero issues later.)

  // Do the first diagonal, from the bottom left to the top right.
  
  if (swwd != 0) { // parallel lines
    double s = (dTrajN*(map1.E-traj1.E) - dTrajE*(map1.N-traj1.N))/swwd;
    double t = (dMapN*(map1.E-traj1.E) - dMapE*(map1.N-traj1.N))/swwd;
    if ((s >= 0) && (s <= 1) && (t >= 0) && (t <= 1)) {
      return true;
    }
  }

  // Do the second diagonal, from the top left to the bottom right.
  
  double temp = map2.E;
  map2.E = map1.E;
  map1.E = temp;

  temp = map2.N;
  map2.N = map1.N;
  map1.N = temp;

  dMapN = map2.N-map1.N;
  dMapE = map2.E-map1.E;
  dTrajN = traj2.N-traj1.N;
  dTrajE = traj2.E-traj1.E;
  swwd = dMapN*dTrajE-dTrajN*dMapE;
  
  if (swwd != 0) { // parallel lines
    double s = (dTrajN*(map1.E-traj1.E) - dTrajE*(map1.N-traj1.N))/swwd;
    double t = (dMapN*(map1.E-traj1.E) - dMapE*(map1.N-traj1.N))/swwd;
    if ((s >= 0) && (s <= 1) && (t >= 0) && (t <= 1)) {
      return true;
    }
  }

  // No intersections found, then we..
  return false;
}

// The lowest level function, this sends a point as deltas to the map.
// It also blurs the point in a T shape, with the orientation of the T
// dependent of direction.  The blur is currently linear; this can be
// modified in the future.
int CTrajPainter::PaintBlurredPoint(double N, double E, double dy, double dx, double width, double speed) {
  // Paint the point itself.  First, however, check to see that we're not 
  // overwriting a high speed with a lower blurred speed--that introduces
  // artifacts.

  double resRows = trajMap->getResRows();
  double resCols = trajMap->getResCols();

  double current = trajMap->getDataUTM<double>(trajLayer, N, E);
  double desired = speed;
  if (current < desired) {
    if(useDeltasFlag) {
      trajMap->setDataUTM_Delta(trajLayer, N, E, desired);
    } else {
      trajMap->setDataUTM(trajLayer, N, E, desired);
    }
    updatePoint(N, E);
  }

  // Case 1: Horizontal Segment
  if (fabs(dy) < MAX_PRECISION) {
    for(double j = 1; j <= width; j += resCols) {
      desired = speed*(1-2.*j/3/width);

      current = trajMap->getDataUTM<double>(trajLayer, N + j*resRows, E);
      if (current < desired) {
	if(useDeltasFlag) {
	  trajMap->setDataUTM_Delta(trajLayer, N + j*resRows, E, desired);
	} else {
	  trajMap->setDataUTM(trajLayer, N + j*resRows, E, desired);
	}
	updatePoint(N+j*resRows, E);
      }
      current = trajMap->getDataUTM<double>(trajLayer, N - j*resRows, E);
      if (current < desired) {
	if(useDeltasFlag) {
	  trajMap->setDataUTM_Delta(trajLayer, N - j*resRows, E, desired);
	} else {
	  trajMap->setDataUTM(trajLayer, N - j*resRows, E, desired);
	}
	updatePoint(N-j*resRows, E);
      }
    }
    return 0;
  }

  // Case 2: Vertical Segment
  if (fabs(dx) < MAX_PRECISION) {
    for(double j = 1; j <= width; j += resRows) {
      desired = speed*(1-2.*j/3/width);
      
      current = trajMap->getDataUTM<double>(trajLayer, N, E + resCols*j);
      if(current < desired) {
	if(useDeltasFlag) {
	  trajMap->setDataUTM_Delta(trajLayer, N, E + resCols*j, desired);
	} else {
	  trajMap->setDataUTM(trajLayer, N, E + resCols*j, desired);
	}
	updatePoint(N, E+resCols*j);
      }
      current = trajMap->getDataUTM<double>(trajLayer, N, E - resCols*j);
      if(current < desired) {
	if(useDeltasFlag) {
	  trajMap->setDataUTM_Delta(trajLayer, N, E - resCols*j, desired);
	} else {
	  trajMap->setDataUTM(trajLayer, N, E - resCols*j, desired);
	}
	updatePoint(N, E-resCols*j);
      }
    }
    return 0;
  }

  // Case 3: Other
  double m = dy/dx;

  for(double j = 1; j <= width; j += (resRows+resCols)/2) {
    desired = speed*(1-2.*j/3/width);
    if (fabs(m) < 1) {
      current = trajMap->getDataUTM<double>(trajLayer, N + j*resRows, E);
      if (current < desired) {
	if(useDeltasFlag) {
	  trajMap->setDataUTM_Delta(trajLayer, N + j*resRows, E, desired);
	} else {
	  trajMap->setDataUTM(trajLayer, N + j*resRows, E, desired);
	}
	updatePoint(N+j*resRows, E);
      }
      current = trajMap->getDataUTM<double>(trajLayer, N - j*resRows, E);
      if (current < desired) {
	if(useDeltasFlag) {
	  trajMap->setDataUTM_Delta(trajLayer, N - j*resRows, E, desired); 
	} else {
	  trajMap->setDataUTM(trajLayer, N - j*resRows, E, desired); 
	}
	updatePoint(N-j*resRows, E);
      }
      if (dx > 0) {
	current = trajMap->getDataUTM<double>(trajLayer, N, E + resCols*j);
	if (current < desired) {
	  if(useDeltasFlag) {
	    trajMap->setDataUTM_Delta(trajLayer, N, E + resCols*j, desired);
	  } else {
	    trajMap->setDataUTM(trajLayer, N, E + resCols*j, desired);
	  }
	  updatePoint(N, E+resCols*j);
	}
      } else {
	current = trajMap->getDataUTM<double>(trajLayer, N, E - resCols*j);
	if (current < desired) {
	  if(useDeltasFlag) {
	    trajMap->setDataUTM_Delta(trajLayer, N, E - resCols*j, desired);
	  } else {
	    trajMap->setDataUTM(trajLayer, N, E - resCols*j, desired);
	  }
	  updatePoint(N, E-resCols*j);
	}
      }
    }
    if (fabs(m) > 1) {
      current = trajMap->getDataUTM<double>(trajLayer, N, E + resCols*j);
      if (current < desired) {
	if(useDeltasFlag) {
	  trajMap->setDataUTM_Delta(trajLayer, N, E + resCols*j, desired);
	} else {
	  trajMap->setDataUTM(trajLayer, N, E + resCols*j, desired);
	}
	updatePoint(N, E+resCols*j);
      }
      current = trajMap->getDataUTM<double>(trajLayer, N, E - resCols*j);
      if (current < desired) {
	if(useDeltasFlag) {
	  trajMap->setDataUTM_Delta(trajLayer, N, E - resCols*j, desired);	  
	} else {
	  trajMap->setDataUTM(trajLayer, N, E - resCols*j, desired);	  
	}
	updatePoint(N, E-resCols*j);
      }
      if (dy > 0) {
	current = trajMap->getDataUTM<double>(trajLayer, N + j*resRows, E);
	if (current < desired) {
	  if(useDeltasFlag) {
	    trajMap->setDataUTM_Delta(trajLayer, N + j*resRows, E, desired);
	  } else {
	    trajMap->setDataUTM(trajLayer, N + j*resRows, E, desired);
	  }
	  updatePoint(N+j*resRows, E);
	}
      } else {
	current = trajMap->getDataUTM<double>(trajLayer, N - j*resRows, E);
	if (current < desired) {
	  if(useDeltasFlag) {
	    trajMap->setDataUTM_Delta(trajLayer, N - j*resRows, E, desired);
	  } else {	
	    trajMap->setDataUTM(trajLayer, N - j*resRows, E, desired);
	  }
	  updatePoint(N-j*resRows, E);
	}
      }
    }
  }
  return 0;
}

void CTrajPainter::updatePoint(double N, double E)
{
  if(!costFuser)
    return;

  costFuser->markChangesCost(NEcoord(N, E));
}

