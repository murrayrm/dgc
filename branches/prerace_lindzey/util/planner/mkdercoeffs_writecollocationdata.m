tc = linspace(0,1,(NTHETA-1)*MTHETA + 1)';
tck = floor(tc * (NTHETA-1));
tck(find(tc == 1))--;
tcx = tc * (NTHETA-1) - tck;

# at first, valcoeffs * coefficients = collocationvalues
valcoeffs = zeros(length(tc), 3*(NTHETA-1));
d1coeffs  = zeros(length(tc), 3*(NTHETA-1));
d2coeffs  = zeros(length(tc), 3*(NTHETA-1));

# this is terrible
for i=1:length(tc)
	valcoeffs(i,3*tck(i)+1) = 1;
	valcoeffs(i,3*tck(i)+2) = tcx(i);
	valcoeffs(i,3*tck(i)+3) = tcx(i)^2;

	d1coeffs(i,3*tck(i)+2) = (NTHETA-1) * 1;
	d1coeffs(i,3*tck(i)+3) = (NTHETA-1) * 2 * tcx(i);

	d2coeffs(i,3*tck(i)+3) = (NTHETA-1)^2 * 2;
end

W = [eye(NTHETA-1)              zeros(NTHETA-1,1) zeros(NTHETA-1,1); \
		 zeros(NTHETA-1,1)          eye(NTHETA-1)     zeros(NTHETA-1,1); \
		 zeros(NTHETA-2, NTHETA)    zeros(NTHETA-2,1); \
		 zeros(1, NTHETA)           1];

# make it so, valcoeffs * y = collocationvalues
valcoeffs = valcoeffs * inv(a) * W;
d1coeffs  = d1coeffs  * inv(a) * W;
d2coeffs  = d2coeffs  * inv(a) * W;

# write out the data. don't include the last column (offset), since it's trivial
fil = fopen(filename, "w+b", "ieee-le");
#fwrite(fil, valcoeffs(:,2:(NTHETA+1)) , "double");
#fwrite(fil, d1coeffs (:,2:(NTHETA+1)) , "double");
#fwrite(fil, d2coeffs (:,2:(NTHETA+1)) , "double");
fwrite(fil, valcoeffs(:,1:NTHETA) , "double");
fwrite(fil, d1coeffs (:,1:NTHETA) , "double");
fwrite(fil, d2coeffs (:,1:NTHETA) , "double");



# temporal part:
valcoeffs_speed = zeros(length(tc), NSPEED);
d1coeffs_speed  = zeros(length(tc), NSPEED);

for i=1:NSPEED
	for j=1:MSPEED
		if i != 1
			valcoeffs_speed((i-1)*MSPEED + j, i-1) = 1.0 - (j-1)/MSPEED;
		end

		valcoeffs_speed((i-1)*MSPEED + j, i  ) =  (j-1)/MSPEED;


		if j != 1 || (j==1 && i==1)
			if i != 1
				d1coeffs_speed ((i-1)*MSPEED + j, i-1) = -1.0 * NSPEED;
			end
			d1coeffs_speed ((i-1)*MSPEED + j, i  ) =  1.0 * NSPEED;
		else
			if i == 2
				d1coeffs_speed ((i-1)*MSPEED + j, i  )  +=  0.5 * NSPEED;
			else
				d1coeffs_speed ((i-1)*MSPEED + j, i-2)  += -0.5 * NSPEED;
				d1coeffs_speed ((i-1)*MSPEED + j, i  )  +=  0.5 * NSPEED;
			end
		end
	end
end

valcoeffs_speed(NSPEED*MSPEED + 1, NSPEED-1) = 0.0;
valcoeffs_speed(NSPEED*MSPEED + 1, NSPEED  ) = 1.0;
d1coeffs_speed (NSPEED*MSPEED + 1, NSPEED-1) = -1.0 * NSPEED;
d1coeffs_speed (NSPEED*MSPEED + 1, NSPEED  ) =  1.0 * NSPEED;

fwrite(fil, valcoeffs_speed, "double");
fwrite(fil,  d1coeffs_speed, "double");
fclose(fil);
