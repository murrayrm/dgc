#include "sharemem.hh"
#include "test_class.hh"

#include <unistd.h>
#include <signal.h>
#include <stdlib.h>

CShmMaster reader;

void sigint(int bla) {
//	writer.DeInit();
	exit(1);
}

int main(int argc, char *argv[]) {
	CShmTest test; 
	
	int data;
	signal(SIGINT, sigint);
	

	if (!reader.Init("/tmp/shmdemo.pid", sizeof(int))) return 0;
	

	while (1) { 
		reader.Request();
		reader.Sh2MemCpy(&test,sizeof(CShmTest));
//		printf("Updated with %d \n", data);
		test.Print();
		
		if ((argc == 2)&&( argv[1][0] == 'w')) 
			getchar();
		
		if (rand()%1000 == 1) {
			sleep(2);
		}
	}
	
	return 0;
}
