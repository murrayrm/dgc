//Author: David Rosen
//Used to create an "SDS port" object (to represent one of the 32 SDS ports) for sending/receiving commands/data to and from serial devices via the SDS/Ethernet

//Created 1-1-05.  This is only a first run version,so I cannot be held responsible...

//1-1-05:  SDSPort objects can send messages to serial devices (steering controller) accross SDS.  Still working on receiving data...

//1-1-05  SDSPort objects can now send AND receive transmissions.  At this point, it's a functional class (although I'm going to continue to make revisions).

//1-3-05:  Added additional methods (request and rpt_request) and modified read method so that it now 1) only writes to the buffer if it actually receives data successfully, and 2) returns 0 for successful receipt of data or -1 for an error.   For some reason, the SDS sometimes freezes when it's attempting to receive input from a device.  So far, transmissions have worked every time I've attempted.

//1-5-05:  Revised read() method to use select() call for timing out requests (should hopefully prevent the SDS connection from "freezing" on reading calls).

//1-8-05:  Tested revised read() method; connection no longer "hangs up".  At this point, I think the SDS is ready for service.  Of course, if anybody finds any lingering bugs, please let me know.

//1-21-05: Found out that this code does not provide some functionality required for race day... will revise after winter W3 field test.

//1-27-05: Modified SDSPort class so that all read commands (read(), request(), rpt_request() ) will ALWAYS write to the buffer, whether or not they receive complete responses.

//2-5-05: Added openPort(), closePort(), and isOpen() methods for better port control.  NOTE: As of right now, the closePort() method DOES NOTHING because the way the socket connections are currently configured, it is only possible to connect to a given address once per program execution.

//2-15-05: Added peek() and read_until() methods.  Modified the write() method to return the number of bytes written on a successful write.

#ifndef SDSPORT_H
#define SDSPORT_H

#include <iostream>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/types.h>

class SDSPort
{

private:

  int sock, port, isConnected;
  struct sockaddr_in SDS;
  struct sockaddr_in* copy;

  //For select() calls
  fd_set rfds;
  struct timeval timeout;
  
public:

  //SDSPort: Creates an object representing an sds port and establishes a connection to port "portNum" (1-32) on the sds with an IP address of "ip"

  SDSPort(char* ip, int portNum);

  //write: Attempts to write buffer "data" to the port.  If the port's connection is closed, attempts to open it before writing the data.  Returns the number of bytes written for a successful write or -1 on an error.

  int write(const char* data);

  int write(const char* data, const int bytesToWrite);

  //read: Attempts to read bytesToRead bytes of data back from the port into buffer "buffer".  If the port's connection isn't open, attempts to open a connection to the port first.  Times out after "time" microseconds; returns the number of bytes read or -1 on an error or if the read times out.

  int read(const int bytesToRead, char* buffer, const int time);

  //peek:  Functions exactly like a read() call, except that it does not remove the data read from the buffer.

  int peek(const int bytesToRead, char* buffer, const int time);

  //read_until:  Attempts to read a message until a specified character "find"  is encountered (inclusive), up to a length of bytesToRead bytes.  Returns number of bytes actually read, or -1 on error or timeout.

  int read_until(const int maxBytesToRead, char* buffer, const int time, char find);

  //request: Sends a command to request data, then attempts to read back the first bytesToRead bytes from the port into buffer "buffer" using the read() method, times out after "time" microseconds; returns the number of bytes read if successful or -1 on an error or if the read times out.

  int request(const char* requestString, const int bytesToRead, char* buffer, const int time);

  //rpt_request: Sends a command to request data, then attempts to read back the first bytesToRead from the port into buffer "buffer".  If there is an error with the response(i.e., the read times out or less than the expected number of bytes were received), it will resend the request for data up to numTries times until an appropriate response is successfully received. Note that EACH INDIVIDUAL REQUEST times out after "time" microseconds.  Returns bytesToRead if one of the read requests received a complete response, a smaller positive number if only partial responses were received (this is the number of bytes that were received on the last read attempt), or -1 if the last read attempt resulted in an error or timed out.

  int rpt_request(const char* requestString, const int bytesToRead, char* buffer, const int time,  int numTries);

  void clean(); //Clears the SDSPort buffer

  //Attempts to open a connection to the SDS port.  Returns 0 on success or -1 on error.
  int openPort();

  //Closes a connection to the SDS port.  Returns 0 if the port closed properly, or -1 if something funny happened.  NOTE: This method will ALWAYS close the port; -1 means that there was probably unread data in the port's buffer when it closed.
  int closePort();

  //Checks the current state of the port's connection: returns 1 if it's currently connected or 0 if it's not.
  int isOpen();

  ~SDSPort();

};

#endif
