/**
 * DGCTimer.hh
 * Revision History:
 * 25/08/2005 hbarnor created
 * $Id$
 * Watchdog timer class.
 */

#ifndef DGCTIMER_HH
#define DGCTIMER_HH

#define MILLITONANO 0.000001

#include <sigc++/sigc++.h>

#include "DGCutils"

//#include <string>
#include <iostream>
#include <ctime>

using namespace std;

/**
 * DGCTimer - Watchdog timer class. Uses setitimer and getitimer to
 * generate timer interrupts. Handles the interrupt by firing a
 * libsigc signal which is connected to the appropriate function. 
 */
class DGCTimer
{
public:
  /**
   * DGCTImer - constructor. 
   * @param timeOut - number of milliseconds between timeouts. 
   * @param action - pointer libsigc signal. This signal is invoked
   * each timeout. 
   */
  DGCTimer(double timeOut, sigc::signal<void> * action);
  /**
   * Destructor - if it was not obvious.
   */
  ~DGCTimer();
  /**
   * start - starts the timer. 
   */
  void start();
  /**
   * stop - stops the timer. 
   */
  void stop();
  /**
   * run - where the actual sleeping and code execution is done. 
   */
  void run();
  /**
   * reset - resets the counter to the beginning. So that it doesn't
   * time out. 
   */
  void reset();
private:
  /**
   * Mutex to prevent possible race condition between the run thread
   * and the reset.
   *
   */
  pthread_mutex_t resetMutex;

  /**
   * if true, then there has been no reset and we should fire the
   * action signal. Else, we just go back to sleep.
   */
  bool resetReceived;
  /**
   * boolean value used to control the clock. 
   */
  bool enable; 
  /**
   * structure to be used in sleeping. 
   */
  struct timespec timeVal;
  /**
   * structure to store remaining time in case of an interrupt.
   */
  struct timespec remValue;
  /**
   * The action that is invoked on a timeout. 
   */
  sigc::signal<void> * m_action;
  /**
   * Number of milliseconds between timeouts. 
   */
  double m_timeOut;
};

#endif //DGCTIMER_HH
