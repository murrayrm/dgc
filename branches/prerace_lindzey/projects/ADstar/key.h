//Creates a key class to be used as the indices for the states
//in the queues in the AD* algorithm.

class key
{

 private:

  double k1, k2;

 public:

  key(double first, double second);

  double getK1();

  double getK2();

  double setK1(double newK1);

  double setK2(double newK2);

  ~key();
};
