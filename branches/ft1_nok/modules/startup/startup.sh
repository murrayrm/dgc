#!/bin/bash

# You can change these if you know what your doing.
# Global account for all processes
ACCOUNT=team

# Set KEEP_ALIVE=1 to restart processes that crash
KEEP_ALIVE=0

# Global Skynet key
SKYNET_KEY=10

# When NOW is set, all processes run with --now option
NOW=--now

# Mode=0 is quiet, Mode=1 is display
MODE=1

#Don't change these ever
TAB_COUNT=0

setup()
{
    if [ $MODE = 0 ] ; then
        SCMODE="-D -m"
	GEOMETRY=100x3
    else
        SCMODE=""
	GEOMETRY=100x36
    fi

    if [ $TAB_COUNT = 0 ] ; then
        NEW_WINDOW=" --window --geometry $GEOMETRY --hide-menubar "
    else
        NEW_WINDOW=" --tab "
    fi

    # Test if PROC is still up on HOST
    PROC_UP=`ssh -x $ACCOUNT@$HOST ps -au $ACCOUNT |grep $PROC `

    if [ "$PROC_UP" ] ; then
        # Attach to old screen
        echo $PROC is up
        NEW_CMD=" $NEW_WINDOW -t $NAME -e \
                 \" ssh -x -t $ACCOUNT@$HOST screen -x -S $NAME \" "
    else
        echo $PROC is not up
	NEW_CMD=" $NEW_WINDOW -t $NAME -e \
                 \" ssh -x -t $ACCOUNT@$HOST screen $SCMODE -S $NAME /home/team/dgc/bin/run_here.sh $SKYNET_KEY $KEEP_ALIVE ./$PROC $ARGS \" "
    fi

    CMD="$CMD $NEW_CMD "
    # Reset parameters
    HOST=
    NAME=
    PROC=
    ARGS=

    TAB_COUNT=1
}

watch()
{
    # We want watch calls to use the larger windows
    GEOMETRY=100x36
    if [ $TAB_COUNT = 0 ] ; then
	CMD=" --window --geometry $GEOMETRY --hide-menubar -t $NAME -e \
                 \" ssh -x -t $ACCOUNT@$HOST screen -S $NAME -x \" "
    else
	NEW_CMD=" --tab -t $NAME -e \
                 \" ssh -x -t $ACCOUNT@$HOST screen -S $NAME -x \" "
	CMD="$CMD $NEW_CMD "
    fi
    TAB_COUNT=1

    # Reset parameters
    HOST=
    NAME=
    PROC=
    ARGS=
}

Astate()
{
    HOST=skynet5
    NAME=Astate
    PROC=astate
    ARGS="-c $NOW"
}

roofLadar()
{
    HOST=skynet5
    NAME=roofLadar
    PROC=ladarFeeder
    ARGS="--zero --ladar roof --noestop $NOW"
}

rieglLadar()
{
    HOST=skynet5
    NAME=rieglLadar
    PROC=ladarFeeder
    ARGS="--zero --ladar riegl --noestop $NOW"
}

smallLadar()
{
    HOST=skynet5
    NAME=smallLadar
    PROC=ladarFeeder
    ARGS="--ladar small $NOW"
}

frontLadar()
{
    HOST=skynet5
    NAME=frontLadar
    PROC=ladarFeeder
    ARGS="--ladar front $NOW"
}

bumperLadar()
{
    HOST=skynet5
    NAME=bumperLadar
    PROC=ladarFeeder
    ARGS="--ladar bumper --noestop $NOW"
}

fusionMapper()
{
    HOST=sitka 
    NAME=fusionMapper 
    PROC=fusionMapper 
    ARGS="--noestop $NOW"
}

rddfPathGen()
{
    HOST=skynet1
    NAME=rddfPathGen
    PROC=rddfPathGen
    ARGS="c2"
}

planner()
{
    HOST=sitka
    NAME=Planner 
    PROC=plannerModule
    ARGS=
}

longStereoFeeder()
{
    HOST=skynet3 
    NAME=longStereoFeeder 
    PROC=stereoFeeder 
    ARGS="--pair long $NOW" 
}

shortStereoFeeder()
{
    HOST=skynet6
    NAME=shortStereoFeeder 
    PROC=stereoFeader 
    ARGS="--pair short $NOW" 
}

aDrive()
{
    HOST=skynet1 
    NAME=Adrive
    PROC=adrive
    ARGS=
}

DBS()
{
    HOST=skynet1
    NAME=DBS 
    PROC=DBS
    ARGS=
}

trajectoryFollower()
{
    HOST=skynet1
    NAME=TrajectorFollower 
    PROC=trajFollower
    ARGS=
}

superCon()
{
    HOST=skynet1
    NAME=SuperCon 
    PROC=superCon 
    ARGS="--log 6"
}

timber()
{
    HOST=skynet1
    NAME=Timber
    PROC=timber
    ARGS=
}

start_skynet5()
{
    echo Staring all processes
    echo Use Ctrl-\\ to terminate remote processes
    #SKYNET 5 Modules in separate window
    Astate 
    setup
    # Ladar Feaders   
    roofLadar 
    setup
    rieglLadar 
    setup
#    smallLadar 
#    setup
#    frontLadar 
#    setup
#    bumperLadar 
#    setup
    `echo $CMD | xargs gnome-terminal` &
    #Reset tab count
    TAB_COUNT=0

    echo Waiting 30 seconds  
    echo If astate is not running, Ctrl-C now.
#    sleep 30
}

start_other()
{
    echo Copying /home/team/dgc/bin/rddf.dat to sitka
    scp /home/team/dgc/bin/rddf.dat team@sitka:/race/dgc/bin
    scp /home/team/dgc/bin/rddf.dat team@sitka:/home/team/dgc/bin

    # Start sitka processes
    fusionMapper
    setup

    planner
    setup

    # StereoFeeders typically run on skynet 3 & 6
#    longStereoFeeder
#    setup
#    shortStereoFeeder
#    setup

    #SKYNET 7 Modules
    # -SBG- Don't know about this one
    # -SBG- road may need to start on skynet 7 X console
    #HOST=skynet7
    #PROC=road setup

    # Run Sitka, Skynet 3, 6 & 7 in one window
    `echo $CMD | xargs gnome-terminal ` &
    #Reset Tab Count
    TAB_COUNT=0
}

start_skynet1()
{
    #SKYNET 1 Modules
#    aDrive
#    setup
#    gnome-terminal --window --geometry 100x36 --hide-menubar -t $NAME -e "ssh -x -t $ACCOUNT@$HOST /home/team/dgc/bin/run_here.sh $SKYNET_KEY $KEEP_ALIVE ./$PROC $ARGS "  &
#    DBS
#    setup
#    rddfPathGen
#    setup
    trajectoryFollower
    setup
#    superCon
#    setup
#    timber
#    setup
    #Start SKYNET 1 Modules in single window
    `echo $CMD | xargs gnome-terminal ` &
    #Reset Tab Count
    TAB_COUNT=0
}

attach()
{
    HOST=skynet1
    NAME=TOP
    PROC=top
    ARGS=

    setup
    echo $CMD
}

for arg in $*
do
        case "${arg}" in
        stop)
                stop
                ;;
        watch)
                attach
                ;;
        skynet5)
	        start_skynet5
		;;
        skynet1)
		start_skynet1
		;;
	other)
		start_other
		;;
        start)
# When running for real, start_skynet5 must go first
               start_skynet5	
	       start_other
               start_skynet1
                ;;
	restart)
		restart
                ;;
	esac
done
