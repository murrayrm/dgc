
/* 
 * Desc: Sink for ladar blob data
 * Date: 14 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <GL/glu.h>
#include <pose3.h>
#include <AliceConstants.h>

#include "LadarSink.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stdout, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
LadarSink::LadarSink()
{
  memset(this, 0, sizeof(*this));

  this->sensorId = SENSNET_NULL_SENSOR;
  this->blobId = -1;

  int i;
  float d;

  // Rainbow color scale
  for (i = 0; i < 0x10000; i++)
  {
    d = 4 * ((double) i / 0x10000);

    if (d >= 0 && d < 1.0)
    {
      this->rainbow[i][0] = 0x00;
      this->rainbow[i][1] = (int) (d * 0xFF);
      this->rainbow[i][2] = 0xFF;
    }
    else if (d < 2.0)
    {
      d -= 1.0;
      this->rainbow[i][0] = 0x00;
      this->rainbow[i][1] = 0xFF;
      this->rainbow[i][2] = (int) ((1 - d) * 0xFF);
    }
    else if (d < 3.0)
    {
      d -= 2.0;
      this->rainbow[i][0] = (int) (d * 0xFF);
      this->rainbow[i][1] = 0xFF;
      this->rainbow[i][2] = 0x00;
    }
    else if (d < 4.0)
    {
      d -= 3.0;
      this->rainbow[i][0] = 0xFF;
      this->rainbow[i][1] = (int) ((1 - d) * 0xFF);
      this->rainbow[i][2] = 0x00;
    }
    else
    {
      this->rainbow[i][0] = 0xFF;
      this->rainbow[i][1] = 0x00;
      this->rainbow[i][2] = 0x00;
    }
  }
  
  return;
}


// Update with current sensnet data
int LadarSink::sensnetUpdate(sensnet_t *sensnet)
{
#ifdef USE_SENSNET
  int blobId, blobLen;

  // Look at the latest data
  if (sensnet_peek(sensnet, this->sensorId, this->blobType, &blobId, &blobLen) != 0)
    return -1;
    
  // Is the data valid?
  if (blobId < 0)
    return 0;
    
  // Is the data new?
  if (blobId == this->blobId)
    return 0;
    
  if (sensnet_read(sensnet, this->sensorId, this->blobType,
                   blobId, sizeof(this->blob), &this->blob) != 0)
    return -1;

  this->blobId = blobId;
  this->dirty = true;
#endif
  
  return 0;
}


#if USE_LADAR_LOG

// Open log file
int LadarSink::logOpen(const char *filename)
{
  ladar_log_header_t header;
  
  this->log = ladar_log_alloc();
  assert(this->log);

  MSG("opening %s", filename);
  if (ladar_log_open_read(this->log, filename, &header) != 0)
    return ERROR("unable to open log file");

  return 0;
}


// Close log file
int LadarSink::logClose()
{
  ladar_log_close(this->log);
  ladar_log_free(this->log);
  this->log = NULL;
  
  return 0;
}


// Update with next frame from log
int LadarSink::logUpdate()
{
  int i;
  ladar_log_scan_t scan;
  sensnet_ladar_blob_t *blob;

  if (!this->log)
    return 0;
  
  // Read scan from log
  if (ladar_log_read(this->log, &scan) != 0)
    return ERROR("unable to read log");

  blob = &this->blob;

  // Header data
  blob->timestamp = scan.timestamp;
  blob->scanid = scan.scanid;

  // State data
  blob->state.timestamp = scan.state.timestamp;
  memcpy(blob->state.pose_local, scan.state.pose_local, sizeof(blob->state.pose_local));
  memcpy(blob->state.pose_global, scan.state.pose_global, sizeof(blob->state.pose_global));
  memcpy(blob->state.vel_vehicle, scan.state.vel_vehicle, sizeof(blob->state.vel_vehicle));
  memcpy(blob->sens2veh, scan.sens2veh, sizeof(blob->sens2veh));

  // Read scan data
  for (i = 0; i < scan.num_points; i++)
  {
    assert(i < (int) (sizeof(blob->points) / sizeof(blob->points[0])));
    assert(i < (int) (sizeof(scan.points) / sizeof(scan.points[0])));
    blob->points[i][0] = scan.points[i][0];
    blob->points[i][1] = scan.points[i][1];
  }
  blob->num_points = i;

  this->blobId = blob->scanid;
  this->dirty = true;

  return 0;
}

#endif



// HACK
// Switch to given frame (homogeneous transform)
void pushFrame(float m[4][4])
{
  int i, j;
  float t[16];

  // Transpose to column-major order for GL
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      t[i*4+j] = m[j][i];  
  glPushMatrix();
  glMultMatrixf(t);

  return;
}

// HACK
// Switch to the given frame.
void pushFrame(pose3_t pose)
{
  float m[4][4];
  pose3_to_mat44f(pose, m);  
  return pushFrame(m);
}


// HACK
// Switch to the given frame.
void pushFrame(double pose[6])
{
  pose3_t npose;
  float m[4][4];
  npose.pos = vec3_set(pose[0], pose[1], pose[2]);
  npose.rot = quat_from_rpy(pose[3], pose[4], pose[5]);  
  pose3_to_mat44f(npose, m);  
  return pushFrame(m);
}


// Generate range point cloud (local frame)
void LadarSink::predrawPointCloud()
{
  int list;
  int i, k;
  float pa, pr, px, py, pz;
  sensnet_ladar_blob_t *blob;

  // TESTING
  this->maxClouds = 750;
  
  if (this->cloudList == 0)
  {
    this->cloudList = glGenLists(this->maxClouds);
    if (this->cloudList == 0)
    {
      ERROR("unable to create display lists");
      return;
    }
  }

  blob = &this->blob;

  // Pick a list to use based on the scan id.
  list = this->cloudList + this->blob.scanid % this->maxClouds;
  
  glNewList(list, GL_COMPILE);

  glPushAttrib(GL_POINT_BIT);
  glPointSize(2.0);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glColor3f(0, 0, 1);

  // HACK TODO
  pushFrame(blob->state.pose_local);

  glBegin(GL_POINTS);
  
  for (i = 0; i < blob->num_points; i++)
  {
    pa = blob->points[i][0];
    pr = blob->points[i][1];
    
    if (pr > 0 && pr < 60.0) // MAGIC 
    {
      sensnet_ladar_blob_polar_to_vehicle(blob, pa, pr, &px, &py, &pz);      
      k = (int) (-0x10000 * pz / 2.0); // MAGIC
      if (k < 0x0000) k = 0x0000;
      if (k > 0xFFFF) k = 0xFFFF;
      glColor3ub(this->rainbow[k][0], this->rainbow[k][1], this->rainbow[k][2]);
      glVertex3f(px, py, pz);
    }
  }

  glEnd();

  glPopMatrix();

  glPopAttrib();

  glEndList();
    
  return;
}


// Predraw sensor footprint
void LadarSink::predrawFootprint()
{
  int i;
  float a, b, d;
  float pa, pr;
  float px, py, pz;

  if (this->blobId < 0)
    return;

  if (this->footList == 0)
    this->footList = glGenLists(1);
  glNewList(this->footList, GL_COMPILE);

  a = this->blob.sens2veh[2][0];
  b = this->blob.sens2veh[2][1];
  d = this->blob.sens2veh[2][3] - VEHICLE_TIRE_RADIUS;

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glColor4f(0, 0, 1, 0.50);
  glBegin(GL_POLYGON);
  
  glVertex3f(0, 0, 0);  
  for (i = -90; i < +90; i++)
  {
    // Range and bearing of point on the ground (sensor frame)
    pa = i * M_PI/180;
    pr = -d / (a * cos(pa) + b * sin(pa));
    if (pr < 0 || pr > 80 || !finite(pr)) // MAGIC
      pr = 80;
    sensnet_ladar_blob_polar_to_sensor(&this->blob, pa, pr, &px, &py, &pz);
    glVertex3f(px, py, pz);
  }
  
  glEnd();
  glDisable(GL_BLEND);
  
  glEndList();
  
  return;
}
