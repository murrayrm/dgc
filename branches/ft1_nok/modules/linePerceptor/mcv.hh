#ifndef MCV_HH_
#define MCV_HH_

#include "cv.h"
 
//constant definitions  
#define FLOAT_MAT_TYPE CV_32FC1
#define FLOAT_MAT_ELEM_TYPE float

#define INT_MAT_TYPE CV_8UC1
#define INT_MAT_ELEM_TYPE unsigned char

#define FLOAT_IMAGE_TYPE IPL_DEPTH_32F
#define FLOAT_IMAGE_ELEM_TYPE float

#define INT_IMAGE_TYPE IPL_DEPTH_8U
#define INT_IMAGE_ELEM_TYPE unsigned char

#define FLOAT_POINT2D CvPoint2D32f
#define FLOAT_POINT2D_F cvPoint2D632f

#define FLOAT float
#define INT int
#define SHORT_INT unsigned char



//some helper functions for debugging
void SHOW_MAT(const CvMat *pmat, char str[]="Matrix");

void SHOT_MAT_TYPE(const CvMat *pmat);

void SHOW_IMAGE(const CvMat *pmat, char str[]="Window");

void SHOW_POINT(const FLOAT_POINT2D pt, char str[]="Point:");


/**
 * This function scales the input image to have values 0->1
 * 
 * \param inImage the input image
 * \param outImage hte output iamge
 */
void mcvScaleMat(const CvMat *inImage, CvMat *outMat);

#endif /*MCV_HH_*/
