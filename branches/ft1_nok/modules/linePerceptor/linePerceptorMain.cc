
/* 
 * Desc: Line perceptor run-time 
 * Date: 26 November 2006
 * Author: Andrew Howard, Mohamed Aly
 * CVS: $Id$
*/

#include <assert.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

#include <AliceConstants.h>
#include <pose3.h>

// Sensnet/Skynet support
#include <sn_types.h>
#include <sensnet.h>
#include <sensnet_stereo.h>
#include <skynet_roadline.h>

// Sparrow CLI support
#include <sparrow/display.h>

// Cmd-line handling
#include "linePerceptorOpt.h"

// Stop-line detection
#include <vector>
using namespace std;
#include <cv.h>
#include <highgui.h>
#include "mcv.hh"
#include "inversePerspectiveMapping.hh"
#include "stopLinePerceptor.hh"
//#include "stopLinePerceptorOpt.h"
//#include "cameraInfoOpt.h"

// Line perceptor module
class LinePerceptor
{
  public:

  // Constructor
  LinePerceptor();

  // Destructor
  ~LinePerceptor();

  public:
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);

  public:

  // Initialize sparrow display
  int initDisplay();

  // Finalize sparrow display
  int finiDisplay();

  // Sparrow callback; occurs in sparrow thread
  static int onUserQuit(long);

  // Sparrow callbacks; occurs in sparrow thread
  static int onUserPause(long);

  // Sparrow callbacks; occurs in sparrow thread
  static int onUserFake(long);
  
  // Sparrow thread
  pthread_t spThread;

  // Mutex for thead sync
  //pthread_mutex_t spMutex;

  public:
  
  // Initialize sensnet
  int initSensnet();

  // Clean up sensnet
  int finiSensnet();

  public:

  // Find stop lines
  int findStopLines();

  public:
  
  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon; int skynetKey;

  // Our module id (SkyNet)
  modulename moduleId;
  char *moduleName;

  // Source sensor id
  sensnet_id_t sensorId;
  char *sensorName;

  // Sensnet module
  sensnet_t *sensnet;

  // Should we quit?
  bool quit;
  
  // Should we pause?
  bool pause;

  // Should we generate a fake line?
  bool fake;

  // Current (incoming) stereo blob
  sensnet_stereo_blob_t stereoBlob;

  // Current (outgoing) line message
  skynet_roadline_msg_t lineMsg;

  // Diagnostics on stop lines
  int numStops, totalStops, totalFakes;
  
  /* TODO
  // Sparrow CLI data
  static char spSpreadDaemon[80];
  static int spSpreadKey;
  static char spModuleName[80];
  static int spFrameId;
  static double spLatency;
  static double spRunCycles, spRunTime, spRunPeriod, spRunFreq;
  static bool spFake;
  static int spNumFakes;
  */
  
  //stop line conf strucuture
  StopLinePerceptorConf stopLineConf;
  //camera info strucure
  CameraInfo cameraInfo;
  //line score
  float lineScore;

};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stdout, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Pointer to the one instance of this class.  Sparrow
// needs this in order to work correctly.
static LinePerceptor *self;



// Default constructor
LinePerceptor::LinePerceptor()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Default destructor
LinePerceptor::~LinePerceptor()
{
  return;
}


// Parse the command line
int LinePerceptor::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleName = this->options.module_id_arg;
  this->moduleId = modulenamefromString(this->moduleName);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->moduleName);
    
  // Fill out (source) sensor id
  this->sensorName = this->options.sensor_id_arg;
  this->sensorId = sensnet_id_from_name(options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", options.sensor_id_arg);

  //read the stop line perceptor conf
  mcvInitStopLinePerceptorConf("stopLinePerceptor.conf", &this->stopLineConf);
  //read camera info
  mcvInitCameraInfo("cameraInfo.conf", &this->cameraInfo);    
  
  return 0;
}


// Initialize sparrow display
int LinePerceptor::initDisplay()
{
  // This looks weird, but we include the display table here so that
  // the callbacks are initialized correctly.  
  #include "linePerceptorSp.h"
  
  // Initialize the CLI
  if (dd_open() < 0)
    return ERROR("unable to open display");
  dd_usetbl(display);

  // Initialize key-bindings
  dd_bindkey('q', onUserQuit);
  dd_bindkey('Q', onUserQuit);
  dd_bindkey('p', onUserPause);
  dd_bindkey('P', onUserPause);
  dd_bindkey('f', onUserFake);
  dd_bindkey('F', onUserFake);
  
  // Kick off CLI thread
  pthread_create(&this->spThread, NULL, (void* (*) (void*)) dd_loop, NULL);

  return 0;
}


// Finalize sparrow display
int LinePerceptor::finiDisplay()
{
  // Clean up display
  pthread_join(this->spThread, NULL);
  dd_close();

  return 0;
}


// Initialize sensnet
int LinePerceptor::initSensnet()
{
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;
  
  // Join stereo group
  if (sensnet_join(this->sensnet, this->sensorId,
                   SENSNET_STEREO_BLOB, sizeof(sensnet_stereo_blob_t), 5) != 0)
    return -1;

  return 0;
}


// Clean up sensnet
int LinePerceptor::finiSensnet()
{
  sensnet_leave(this->sensnet, this->sensorId, SENSNET_STEREO_BLOB);
  sensnet_free(this->sensnet);
  
  return 0;
}


// Handle quit button
int LinePerceptor::onUserQuit(long arg)
{
  self->quit = true;
  return DD_EXIT_LOOP;
}


// Handle pause button
int LinePerceptor::onUserPause(long arg)
{
  self->pause = !self->pause;
  return 0;
}


// Handle fake stop button
int LinePerceptor::onUserFake(long arg)
{
  self->fake = true;
  return 0;
}


// Find stop lines
int LinePerceptor::findStopLines()
{
  int i, j;
  uint8_t *pix;
  CvMat *im2m;
  //CameraInfo cameraInfo;
  FLOAT_POINT2D focalLength, opticalCenter;
  vector<Line> stopLines;
  vector<float> lineScores;

  // Create float matrix for holding image
  im2m = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, FLOAT_MAT_TYPE);
  
  // Copy data into float matrix.
  // TODO this only works properly for mono images.
  pix = this->stereoBlob.color_data;  
  for (j = 0; j < this->stereoBlob.rows; j++)
  {
    for (i = 0; i < this->stereoBlob.cols; i++)
    {
      cvmSet(im2m, j, i, (float) pix[0] / 255);
      pix += this->stereoBlob.color_channels;
    }
  }

  // Construct camera parameters
  focalLength = cvPoint2D32f(this->stereoBlob.sx, this->stereoBlob.sy);
  opticalCenter = cvPoint2D32f(this->stereoBlob.cx, this->stereoBlob.cy);
  this->cameraInfo.focalLength = focalLength;
  this->cameraInfo.opticalCenter = opticalCenter;
  this->cameraInfo.imageWidth = this->stereoBlob.cols;
  this->cameraInfo.imageHeight = this->stereoBlob.rows;

  // Assume that camera height is in mm.
  // TODO: check that the yaw convention is correct.
  pose3_t pose;
  double rx, ry, rz;
  pose = pose3_from_mat44f(this->stereoBlob.sens2veh);
  quat_to_rpy(pose.rot, &rx, &ry, &rz);
  this->cameraInfo.cameraHeight = -1000 * pose.pos.z;
  this->cameraInfo.pitch = -ry;
  this->cameraInfo.yaw = rz;
  
  // Search for lines
  mcvGetStopLines(im2m, &stopLines, &lineScores,  &cameraInfo, &this->stopLineConf);
  
  //get line score
  if((int) lineScores.size()>0)
    lineScore = lineScores[0];
  else
    lineScore = -1;
    
  Line line;
  int pi, pj, pd;
  float px, py, pz;
  float qx, qy, qz;
  float m[4][4];

  assert(sizeof(m) == sizeof(this->stereoBlob.sens2veh));
  memcpy(m, this->stereoBlob.sens2veh, sizeof(m));
    
  for (i = 0; i < (int) stopLines.size(); i++)
  {
    if (i >= (int) (sizeof(this->lineMsg.lines) / sizeof(this->lineMsg.lines[0])))
      break;
    
    line = stopLines[i];

    // Get point in image
    pi = (int) line.startPoint.x;
    pj = (int) line.startPoint.y;
    pd = sensnet_stereo_get_disp(&this->stereoBlob, pi, pj);

    // Compute point in the sensor frame
    sensnet_stereo_crd_to_xyz(&this->stereoBlob, pi, pj, pd, &px, &py, &pz);

    // Convert to vehicle frame
    qx = m[0][0] * px + m[0][1] * py + m[0][2] * pz + m[0][3];
    qy = m[1][0] * px + m[1][1] * py + m[1][2] * pz + m[1][3];
    qz = m[2][0] * px + m[2][1] * py + m[2][2] * pz + m[2][3];
      
    this->lineMsg.lines[i].a[0] = qx;
    this->lineMsg.lines[i].a[1] = qy;
    this->lineMsg.lines[i].a[2] = qz;

    // Get point in image
    pi = (int) line.endPoint.x;
    pj = (int) line.endPoint.y;
    pd = sensnet_stereo_get_disp(&this->stereoBlob, pi, pj);

    // Compute point in the sensor frame
    sensnet_stereo_crd_to_xyz(&this->stereoBlob, pi, pj, pd, &px, &py, &pz);

    // Convert to vehicle frame
    qx = m[0][0] * px + m[0][1] * py + m[0][2] * pz + m[0][3];
    qy = m[1][0] * px + m[1][1] * py + m[1][2] * pz + m[1][3];
    qz = m[2][0] * px + m[2][1] * py + m[2][2] * pz + m[2][3];
      
    this->lineMsg.lines[i].b[0] = qx;
    this->lineMsg.lines[i].b[1] = qy;
    this->lineMsg.lines[i].b[2] = qz;
    this->lineMsg.num_lines++;
  }

  // Record some stats
  this->numStops = stopLines.size();
  this->totalStops += stopLines.size();

  cvReleaseMat(&im2m);
  
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  int blobType, blobId, oldBlobId, blobLen;
    
  self = new LinePerceptor();
  assert(self);

  // Parse command line options
  if (self->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (self->initSensnet() != 0)
    return -1;

  // Initialize display
  if (!self->options.disable_sparrow_flag)
    if (self->initDisplay() != 0)
      return -1;
  
  blobType = SENSNET_STEREO_BLOB;
  blobId = -1;
  blobLen = -1;
  oldBlobId = -1;

  while (!self->quit)
  {
    // If we are paused, dont do anything
    if (self->pause)
    {
      usleep(0);
      continue;
    }

    // Read the current blob
    if (sensnet_peek(self->sensnet, self->sensorId, blobType, &blobId, &blobLen) != 0)
      break;

    // Do we have new data?
    if (blobId < 0 || blobId == oldBlobId)
    {
      usleep(0);
      continue;
    }
    
    // Read the current blob
    if (sensnet_read(self->sensnet, self->sensorId,
                     blobType, blobId, blobLen, &self->stereoBlob) != 0)
      break;
    oldBlobId = blobId;

    // Update CLI
    //::spLatency = dgc_gettimeofday() - blob.timestamp;
    
    // Prepare the roadline message
    memset(&self->lineMsg, 0, sizeof(self->lineMsg));
    self->lineMsg.msg_type = SNroadLine;
    self->lineMsg.frameid = self->stereoBlob.frameid;
    self->lineMsg.timestamp = self->stereoBlob.timestamp;
    self->lineMsg.state = self->stereoBlob.state;
    self->lineMsg.num_lines = 0;

    // Use some fake data
    if (self->fake)
    {
      // MAGIC
      self->lineMsg.lines[0].a[0] = VEHICLE_LENGTH + 3;
      self->lineMsg.lines[0].a[1] = -2;
      self->lineMsg.lines[0].a[2] = 0.5;
      self->lineMsg.lines[0].b[0] = VEHICLE_LENGTH + 3;
      self->lineMsg.lines[0].b[1] = +2;
      self->lineMsg.lines[0].b[2] = 0.5;
      self->lineMsg.num_lines = 1;
      self->fake = false;
      self->totalFakes += 1;
    }
    else
    {
      // Process the current blob
      //runTime = -dgc_gettimeofday();
      self->findStopLines();
      //usleep(100000);
      //runTime += dgc_gettimeofday();
    }
    
    // Send line data
    if (self->lineMsg.num_lines > 0)
    {
      sensnet_write(self->sensnet, SENSNET_SKYNET_SENSOR,
                    self->lineMsg.msg_type, 0, sizeof(self->lineMsg), &self->lineMsg);
    }
    
    // Update CLI
    /* TODO
       ::spRunTime += runTime;
       ::spRunCycles += 1;
       ::spRunFreq = ::spRunCycles / ::spRunTime;
       ::spRunPeriod = ::spRunTime / ::spRunCycles;
       ::spFrameId = blobId;
    */
  }

  if (!self->options.disable_sparrow_flag)
    self->finiDisplay();
  self->finiSensnet();

  MSG("exited cleanly");
  
  return 0;
}



