// AState_OBDII.cc - OBD II processing for astate
// Jeremy Leibs, 2005
//
// This file contains the code for access the OBD II information that
// is sent out across skynet.  It basically copies the data into
// buffers that can be accessed by astate.  Supports replay mode as well.

#include "AState.hh"

void AState::obdInit()
{
  obdCount = -2;		// re-initialize for debugging

  if (_astateOpts.useReplay == 1) {
    rawObd obdIn;

    obdReplayStream.read((char*)&obdIn, sizeof(rawObd));
    DGClockMutex(&m_OBDDataMutex);
    obdLogStart = obdIn.time;
    DGCunlockMutex(&m_OBDDataMutex);

    DGClockMutex(&m_MetaStateMutex);
    _metaState.obdEnabled = 1;
    DGCunlockMutex(&m_MetaStateMutex);
  } else {
    DGClockMutex(&m_MetaStateMutex);
    _metaState.obdEnabled = 1;
    DGCunlockMutex(&m_MetaStateMutex);
  }
}

void AState::obdThread()
{
  unsigned long long nowTime;
  unsigned long long rawObdTime;

  rawObd obdIn;
  rawObd obdOut;

  int actuatorStateSock = m_skynet.listen(SNactuatorstate, ALLMODULES);
  ActuatorState obdState;

  obdCount = 0;			// re-initialize for debugging

  while (!quitPressed) {
    if (_astateOpts.useReplay == 1) {
      if (!obdReplayStream) {
	quitPressed = 1;
	return;
      }
      obdReplayStream.read((char*)&obdIn, sizeof(rawObd));
      memcpy(&obdState, &(obdIn.data), sizeof(ActuatorState));
      rawObdTime = obdIn.time - obdLogStart + startTime;
      //Wait until time has caught up with raw input
      if (_astateOpts.fastMode == 0) {
	DGCgettime(nowTime);
	while( rawObdTime > nowTime ) {
	  usleep(1);
	  DGCgettime(nowTime);
	}
      }
    } else {
      // Not in replay mode - get the actual OBD data
      if(m_skynet.get_msg(actuatorStateSock, &obdState, 
			  sizeof(ActuatorState), 0) != sizeof(ActuatorState))
	{
	  cerr << "Didn't receive the right number of bytes in the actuatorstate structure" << endl;
	  continue;
	}
      /*
       * See whether we got the OBD II information
       *
       * Originally this also checked obdState.m_brakestatus to see if we
       * were in autonomous mode (I think), but we decided on the 26
       * Nov field test that this wasn't needed.
       *
       */
       if(!obdState.m_obdiistatus) {
	obdCount = -1;
	continue;
      }
      DGCgettime(rawObdTime); // time stamp as soon as data read.
      
    }
    if (_astateOpts.logRaw == 1) {
      obdOut.time = rawObdTime;

      memcpy(&(obdOut.data), &obdState, sizeof(ActuatorState));

      obdLogStream.write((char*)&obdOut, sizeof(rawObd));
    }

    DGClockMutex(&m_HeartbeatMutex);
     _heartbeat.obd = true;
    DGCunlockMutex(&m_HeartbeatMutex);

    DGClockMutex(&m_OBDDataMutex);


    if (((obdBufferReadInd + 1) % OBD_BUFFER_SIZE) == (obdBufferLastInd % OBD_BUFFER_SIZE)) {
      obdBufferFree.bCond = false;

      DGCunlockMutex(&m_OBDDataMutex);

      DGCWaitForConditionTrue(obdBufferFree);


      DGClockMutex(&m_OBDDataMutex);
    }

    ++obdBufferReadInd;


    memcpy(&(obdIn.data), &obdState, sizeof(ActuatorState));
    obdIn.time = rawObdTime;


    memcpy(&obdBuffer[obdBufferReadInd % OBD_BUFFER_SIZE], &obdIn, sizeof(rawObd));


    DGCunlockMutex(&m_OBDDataMutex);

    DGCSetConditionTrue(newData);

    if (obdBufferReadInd % 500 == 1 && _astateOpts.logRaw) {
      obdLogStream.flush();
    }
  }
}
