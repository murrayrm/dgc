//#warning "this is a weird header file"
//#warning "this is a hack header file"


switch(newModule)
{
  case SNplanner:
    newTabMutex->lock();
    newTab = manage(new SSampleTab(&m_skynet));
    tabName = "Sample Tab";
    m_addTab();
    break;   
  case SNasim:
    newTabMutex->lock();
    newTab = manage(new SasimTab(&m_skynet));
    tabName = "simulator";
    m_addTab();
    break;
  case SNtrajfollower:
    newTabMutex->lock();
    newTab = manage(new StrajfollowerTab(&m_skynet));
    tabName = "trajFollower";
    m_addTab();
    break;
  case SNastate:
    newTabMutex->lock();
    newTab = manage(new SastateTab(&m_skynet));
    tabName = "astate";
    m_addTab();
    break;
  // copy the following lines and update appropriatey
  // newTabMutex->lock();
  // newTab = manage(new S<insert tabname here>Tab(&m_skynet));
  // tabName = "<insert what you want to call your tab here>"
  // m_addTab();
  //break;
  default:
    newTabMutex->lock();
    newTab = NULL;
    tabName = "";      
    cout << "Failed to find module. No tab added." << endl;   
    newTabMutex->unlock(); //unlock it ourselves since we do not signal an add. 
}

