#ifndef STRATEGY_LONERANGER_HH
#define STRATEGY_LONERANGER_HH

//SUPERCON STRATEGY TITLE: Lone Ranger - SOMETIME STRATEGY
//HEADER FILE (.hh)
//DESCRIPTION: 

#include "Strategy.hh"
#include "sc_specs.h"

namespace s_loneranger {

#define LONERANGER_STAGE_LIST(_) \
  _(estop_pause, = 1) /*first entry must = 1 (as this is referenced against NEXTstage */ \
  _(gear_drive, ) \
  _(trajF_forwards, ) \
  _(add_speedcap, ) \
  _(estop_run, ) \
  _(explore_terrain, )
DEFINE_ENUM(loneranger_stage_names, LONERANGER_STAGE_LIST)

}

using namespace std;
using namespace s_loneranger;

class CStrategyLoneRanger : public CStrategy
{

/*** METHODS ***/
public:

  CStrategyLoneRanger() : CStrategy() {}  
  ~CStrategyLoneRanger() {}

  void stepForward(const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface );
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

private:
  //conditional test for transition to LturnReverse
  bool lturnReverse_trans_condition( bool checkBool, const SCdiagnostic *pdiag, const char* StrategySpecificStr );

};

#endif
