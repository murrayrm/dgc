//----------------------------------------------------------------------------
//
//  Road following module
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_FirewireCamera.hh"
 
//----------------------------------------------------------------------------

/// constructor for firewire camera base class.  makes connection to every 
/// camera on every port.  returns number of cameras attached; 0 indicates failure 
/// (possibly because there are no cameras attached to the machine)

UD_FirewireCamera::UD_FirewireCamera(int camera_mode)
{
  unsigned int channel;
  unsigned int speed;
  raw1394handle_t raw_handle;
  struct raw1394_portinfo ports[MAX_PORTS];
  int p, i, j, numPortCams;
  nodeid_t *camera_nodes;

  printf("in firewire constructor\n");

  // defaults

  fps = FRAMERATE_30;

  // body

  mode = camera_mode;

  if (mode == MODE_640x480_MONO || mode == MODE_640x480_YUV422) {
    width = 640;
    height = 480;
  }
  else if (mode == MODE_320x240_YUV422) {
    width = 320;
    height = 240;
  }
  else {
    printf("unsupported firewire camera mode\n");
    exit(1);
  }
  
  clear_channel();

  // get the number of ports (cards) 
  
  raw_handle = raw1394_new_handle();
  if (raw_handle==NULL) {
    printf("Unable to aquire a raw1394 handle\n");
    printf("did you load the drivers?\n");
    exit(1);
  }
  numPorts = raw1394_get_port_info(raw_handle, ports, MAX_PORTS);
  raw1394_destroy_handle(raw_handle);
  
  printf("number of firewire ports = %d\n", numPorts);

  ////////////////////////////////////////////////
  //
  // * Get dc1394 handle to each port
  // * For each camera found, set up for capture by:
  //    * Snagging the ISO channel/speed
  //    * Configuring the channel for DMA x-fer
  //    * Start ISO x-fers
  //

  for (p = 0, i = 0, numCams = 0; p < numPorts; p++) {
            
    // Get the camera nodes and describe them as we find them 
    
    raw_handle = raw1394_new_handle();
    raw1394_set_port(raw_handle, p);
    camera_nodes = dc1394_get_camera_nodes(raw_handle, &numPortCams, 0);
    raw1394_destroy_handle(raw_handle);
    
    numCams += numPortCams;

    printf("Number of cameras = %i on port %i\n", numPortCams, p);
    
    // setup cameras for capture 

    for (j = 0; i < numCams; i++, j++) {	

      printf("setting up camera %i\n", i);

      handles[i] = dc1394_create_handle(p);
      if (handles[i]==NULL) {
	printf("Unable to aquire a raw1394 handle\n");
	cleanup();
	exit(1);
      }

      cameras[i].node = camera_nodes[j];
      
      if(dc1394_get_camera_feature_set(handles[i], 
				       cameras[i].node, 
				       &(features[i])) !=DC1394_SUCCESS) {
	printf("unable to get feature set\n");
	cleanup();
	exit(1);
      }
      
      //      dc1394_print_feature_set(&(features[i]));
      
      if (dc1394_get_iso_channel_and_speed(handles[i], 
					   cameras[i].node,
					   &channel, &speed) 
	  != DC1394_SUCCESS) {
	printf("unable to get the iso channel number\n");
	cleanup();
	exit(1);
      }

      dc1394_auto_on_off(handles[i], cameras[i].node, FEATURE_EXPOSURE, 1);
      printf("resetting exposure\n");
      //dc1394_set_exposure(handles[i], cameras[i].node, 800);

      if (dc1394_dma_setup_capture(handles[i], 
				   cameras[i].node, 
				   i+1, // channel
 				   FORMAT_VGA_NONCOMPRESSED, 
				   mode,
  				   SPEED_400, 
				   fps, 
				   NUM_BUFFERS, 
				   //				   0, 
				   DROP_FRAMES,
  				   FIREWIRE_DEVICE_NAME, 
				   &cameras[i]) 
	  != DC1394_SUCCESS)  {
 	printf("unable to setup camera- check line %d of %s to make sure\n", __LINE__,__FILE__);
	printf("that the video mode,framerate and format are supported is one supported by your camera\n");
	cleanup();
	exit(1);
      }
      
      // Have the camera start sending us data

      if (dc1394_start_iso_transmission(handles[i], cameras[i].node) !=DC1394_SUCCESS) {
	printf("unable to start camera iso transmission\n");
	cleanup();
	exit(1);
      }
    }

    dc1394_free_camera_nodes(camera_nodes);
  }

  if (numCams == 0) {
    printf("no cameras attached\n");
    exit(1);
  }
}

//----------------------------------------------------------------------------

/// reset firewire device

void UD_FirewireCamera::clear_channel(void)
{
  for (int channel = 0; channel < MAX_CAMERAS; channel++) 
    ioctl(open(FIREWIRE_DEVICE_NAME,O_RDONLY),VIDEO1394_IOC_UNLISTEN_CHANNEL,&channel);
}

//----------------------------------------------------------------------------

/// deallocate memory and release connections to firewire devices 

void UD_FirewireCamera::cleanup()
{
  int i;

  for (i=0; i < numCams; i++) {
    dc1394_dma_unlisten( handles[i], &cameras[i] );
    dc1394_dma_release_camera( handles[i], &cameras[i]);
    dc1394_destroy_handle(handles[i]);
  }

  //  exit(1);
}

//----------------------------------------------------------------------------

/// capture one image into buffer without any conversion (i.e., YUV->RGB).
/// saves some time and can save space (for example, when source is 16-bit)
/// when logging captured images

int UD_FirewireCamera::capture_raw(char *buffer, int size, int camNum)
{
  if (camNum < 0 || camNum >= numCams) {
    printf("invalid camera number\n");
    return 0;
  }

  // captures all attached cameras with one call

  //  dc1394_dma_multi_capture(cameras, numCams);

  // capture just one specific camera

  //dc1394_dma_single_capture(&cameras[camNum]);

  // assuming YUV422 is capture format

  memcpy((unsigned char *) buffer, (unsigned char *)cameras[camNum].capture_buffer, size); 

  // unlock the capture buffer

  if (dc1394_dma_done_with_buffer(&cameras[camNum]) < 0)
    perror("DWB failed:");

  return 1;

}

//----------------------------------------------------------------------------

/// capture one image from specified camera and convert to RGB 

int UD_FirewireCamera::capture(IplImage *im, int camNum)
{
  if (camNum < 0 || camNum >= numCams) {
    printf("invalid camera number\n");
    return 0;
  }

  // captures all attached cameras with one call

  //  dc1394_dma_multi_capture(cameras, numCams);

  // capture just one specific camera

  dc1394_dma_single_capture(&cameras[camNum]);

  if (mode == MODE_640x480_YUV422 || mode == MODE_320x240_YUV422) {
    // printf("bayer conversion\n");
    BayerNearestNeighbor((unsigned char *)cameras[camNum].capture_buffer, 
			 (unsigned char *) im->imageData, 
			 im->width, im->height, BAYER_PATTERN_BGGR);
    /*
    IplImage *testim = cvCreateImage(cvSize(640, 480), IPL_DEPTH_8U, 3);

    BayerNearestNeighbor((unsigned char *)cameras[camNum].capture_buffer, 
			 (unsigned char *) testim->imageData, 
			 testim->width, testim->height, BAYER_PATTERN_BGGR);
    cvSaveImage("test.jpg", testim);
    exit(1);
    */
  //    cvSetData(m_image, m_image_buf, 640*3); 
    //    uyvy2rgb ((unsigned char *)cameras[camNum].capture_buffer, (unsigned char *) im->imageData, im->width * im->height);
  }
  else if (mode == MODE_640x480_MONO) 
    y2rgb ((unsigned char *)cameras[camNum].capture_buffer, (unsigned char *) im->imageData, im->width * im->height);

  // unlock the capture buffer

  if (dc1394_dma_done_with_buffer(&cameras[camNum]) < 0)
    perror("DWB failed:");

  return 1;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
