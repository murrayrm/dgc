//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_Movie
//----------------------------------------------------------------------------

#ifndef UD_MOVIE_DECS

//----------------------------------------------------------------------------

#define UD_MOVIE_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "avifile.h"

#include "cv.h"
#include "highgui.h"

// my stuff 

#include "UD_Error.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#ifndef streamtypeDV
#define streamtypeDV           1937138025
#endif

#define UD_MAX_FILENAME        256

#define UD_UNKNOWN             0
#define UD_MOVIE_AVI           2

//-------------------------------------------------
// structure
//-------------------------------------------------

typedef struct 
{
  char *name;
  int type;
  int width, height;
  //  IplImage *im;       // current image
  IplImage *im, *temp_im;
  int numframes, currentframe, previousframe, firstframe, lastframe;
  int state;  // playing or stopped?

  // AVI stuff

  int interleaved_DV;

  IAviReadFile *AviFile;
  IAviReadStream *AviStream;
  BITMAPINFOHEADER bh;
  StreamInfo *info;

  /*
  PAVIFILE AVI_pAviFile;
  PGETFRAME AVI_pgf;
  PAVISTREAM AVI_pVideo, compressed_AVI_pVideo;  // for writing
  LPBITMAPINFOHEADER pBmpInfoHeader;             // for writing
  int need_filename;                             // for writing
  */

} UD_Movie;

//-------------------------------------------------
// functions
//-------------------------------------------------

// UD_Movie *read_ppm_to_UD_Movie(char *);
// void get_ppm_frame_UD_Movie(UD_Movie *, int);
// 
// UD_Movie *write_avi_initialize_UD_Movie(char *, int, int, int);
// void write_avi_addframe_UD_Movie(int, UD_Image *, UD_Movie *);
// void write_avi_finish_UD_Movie(UD_Movie *);

UD_Movie *open_UD_Movie(char *);
void close_UD_Movie(UD_Movie *);
int determine_type_UD_Movie(char *);

char *construct_n_digit_filename(int, int, char *, char *);
char *construct_3_digit_filename(int, char *, char *);
void get_frame_UD_Movie(UD_Movie *, int);

// AVI

UD_Movie *read_avi_to_UD_Movie(char *);
void get_avi_frame_UD_Movie(UD_Movie *, int);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

     
