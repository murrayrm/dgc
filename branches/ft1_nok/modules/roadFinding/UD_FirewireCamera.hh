//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_FirewireCamera
//----------------------------------------------------------------------------

#ifndef UD_FIREWIRECAMERA_DECS

//----------------------------------------------------------------------------

#define UD_FIREWIRECAMERA_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Firewire-related headers

#include <sys/ioctl.h>
#include <fcntl.h> 

#include <libraw1394/raw1394.h>
#include <libraw1394/ieee1394.h>
#include <libdc1394/dc1394_control.h>
#include <ieee1394-ioctl.h>

#include "unistd.h"

#include "conversions.h"   // just for YUV2RGB

#include "cv.h"
#include "highgui.h"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif


#define MAX_PORTS   4
#define MAX_CAMERAS 8
#define NUM_BUFFERS 32
#define DROP_FRAMES 1

#define FIREWIRE_DEVICE_NAME   "/dev/video1394/0"

#define  CV_CAST_8U(t)  (uchar)(!((t) & ~255) ? (t) : (t) > 0 ? 255 : 0)

//-------------------------------------------------
// class
//-------------------------------------------------

class UD_FirewireCamera
{
  
 public:

  // variables

  int numPorts;                                           ///< how many firewire "plugs" are on this machine
  int numCams;                                            ///< how many total firewire cameras are attached to this machine
  raw1394handle_t handles[MAX_CAMERAS];                   ///< array of pointers, one for each camera, necessary for init and cleanup
  dc1394_cameracapture cameras[MAX_CAMERAS];              ///< array of structs, one for each camera, containing firewire parameters
  dc1394_feature_set features[MAX_CAMERAS];               ///< interface for reading & writing camera parameters such as auto-gain, WB, etc.
  
  int mode;                                               ///< format of captured images written to memory (bytes per pixel, codec, etc.)
  int width, height;                                      ///< dimensions of captured images
  int fps;                                                ///< target capture rate (frames per second)
  
  // functions

  UD_FirewireCamera(int);

  int capture(IplImage *, int);
  int capture_raw(char *, int, int);
  void clear_channel(void);
  void cleanup();

};

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
