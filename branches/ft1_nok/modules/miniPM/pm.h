/**
 * pm.h
 * Revision History:
 * 10/09/2005 hbarnor created
 * $Id$
 * Header for general stuff for processman, like directories, 
 * etc
 */

#ifndef PM_H
#define PM_H

#include <sys/types.h>
#include <string>
/*
 * The run directory where we store run information. 
 */
static const std::string RUN_DIR("/var/dgc/run");
/**
 * The log directory where logging information is stored. 
 */
static const std::string LOG_DIR("/var/dgc/log");

static const time_t sleep_secs = 30;

static const long sleep_nsecs = 0;

#endif //PM_H
