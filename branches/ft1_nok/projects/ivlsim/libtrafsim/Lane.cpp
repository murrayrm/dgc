#include "Lane.h"
#include <cassert>
#include <GL/gl.h>

namespace TrafSim
{

typedef std::map<Object*, float>::iterator OtfIter;
typedef std::multimap<float, Object*>::iterator FtoIter;
typedef std::multimap<float, LaneInterferenceDesc>::iterator InterfIter;

Lane::Lane() {}

Lane::Lane(Spline const& _spline) 
{
	spline = new Spline();
	*spline = _spline;
}

Lane::~Lane()
{
	for (OtfIter i = objToT.begin(); i != objToT.end(); ++i)
		delete i->first;
}

Lane* Lane::downcast(Object* source)
{
	return dynamic_cast<Lane*>(source);
}

void Lane::initialize(Spline const& _spline)
{
	spline = new Spline;
	*spline = _spline;
}

bool Lane::addObject(Object* object, float t)
{
	objToT[object] = t;
	tToObj.insert(std::make_pair(t, object));
	
	return true;
}

bool Lane::removeObject(Object* object)
{
	OtfIter i = objToT.find(object);

	if (i != objToT.end())
	{
		float t = i->second;
		objToT.erase(i);
		
		std::pair<FtoIter, FtoIter> range = tToObj.equal_range(t);
		for (FtoIter j = range.first; j != range.second; ++j)
			if (j->second == object)
			{
				tToObj.erase(j);
				return true;
			}
			
		assert(false);
	}
	
	return false;
}

bool Lane::setT(Object* object, float newT)
{
	OtfIter i = objToT.find(object);
	if (i == objToT.end())
		return false;
			
	std::pair<FtoIter, FtoIter> range = tToObj.equal_range(i->second);
	for (FtoIter j = range.first; j != range.second; ++j)
		if (j->second == object)
		{
			tToObj.erase(j);
			break;
		}
		
	tToObj.insert(std::make_pair(newT, object));
	i->second = newT;
	
	return true;
}
	
float Lane::getT(Object* object)
{
	OtfIter i = objToT.find(object);
	if (i == objToT.end())
		return -1.0f;
	return i->second;
}
			
float Lane::mapT(Lane* otherLane, float t)
{
	return otherLane->auxMapT(this, t);
}

float Lane::mapTangent(Lane* otherLane, float t, float mappedT)
{
	Vector tan1 = spline->evalNormal(mappedT).perpendicular();
	Vector tan2 = otherLane->spline->evalNormal(t).perpendicular();
	
	return Vector::dot(tan1, tan2);
}
			
bool Lane::simulate(float ticks)
{
	for (OtfIter i = objToT.begin(); i != objToT.end();)
	{
		Object* obj = (i++)->first; // make sure the iterator isn't invalidated!
		obj->simulate(ticks);
	}

	return true;
}

void Lane::draw()
{
	/*spline->color = Color(1.0f, 1.0f, 0.0f, 1.0f);
	spline->draw();*/
	
	for (OtfIter i = objToT.begin(); i != objToT.end(); ++i)
		i->first->draw();
	/*
	spline->color = Color(1.0f, 0.0f, 0.0f, 1.0f);
	for (InterfIter i = interferences.begin(); i != interferences.end(); ++i)
	{
		Lane* otherLane = i->second.otherSeg.segment;
		otherLane->spline->draw( 
			i->second.otherSeg.t_start, i->second.otherSeg.t_end );
		for (OtfIter j = otherLane->objToT.begin(); j != otherLane->objToT.end(); ++j)
		{
			if (j->second > i->second.otherSeg.t_start &&
				j->second < i->second.otherSeg.t_end)
			{
				glPushMatrix();
				Vector disp = spline->evalCoordinate(mapT(otherLane, j->second)) -
							  otherLane->spline->evalCoordinate(j->second);
				glTranslatef(disp.x, disp.y, 0.0f);
				j->first->draw();
				glPopMatrix();
			}
		}
	}*/
}

bool Lane::addInterference(Lane* otherLane, float threshold, unsigned int subResolution)
{
	if (interferingLanes.empty())
		for (InterfIter i = interferences.begin(); i != interferences.end(); ++i)
			interferingLanes.insert( i->second.otherSeg.segment );
	
	if (otherLane == this || interferingLanes.find(otherLane) != interferingLanes.end())
		return false;
	
	typedef std::vector<SplineInterferenceDesc> SpIntVec;
	
	SpIntVec splineInterf;
	otherLane->spline->getInterference(*spline, splineInterf, threshold, subResolution);
	
	if (splineInterf.empty())
		return false;
	
	for (SpIntVec::iterator i = splineInterf.begin();
		 i != splineInterf.end(); ++i)
	{
		JoinDesc myCoords    = i->second;
		JoinDesc otherCoords = i->first;
		
		float lengthNumerator   = spline->evalLength(myCoords.t_start, myCoords.t_end),
			  lengthDenominator = otherLane->spline->evalLength(otherCoords.t_start,
			  												   otherCoords.t_end);
		float ratio;	
		if (lengthDenominator == 0.0f)
			ratio = 1000000000000000.0f;
		else
			ratio = lengthNumerator / lengthDenominator;
			
		LaneSegment seg1(this, myCoords.t_start, myCoords.t_end);
		LaneSegment seg2(otherLane, otherCoords.t_start, otherCoords.t_end);
		
		if (seg1.t_start > seg1.t_end)
			std::swap(seg1.t_start, seg1.t_end);
		if (seg2.t_start > seg2.t_end)
			std::swap(seg2.t_start, seg2.t_end);
		
		interferences.insert( std::make_pair( seg1.t_start,
			LaneInterferenceDesc( seg1, seg2, ratio ) ) );

		//otherLane->interferences.insert( std::make_pair( seg2.t_start,
		//	LaneInterferenceDesc( seg2, seg1, 1.0f / ratio ) ) );
	}
	
	interferingLanes.insert(otherLane);
	//otherLane->interferingLanes.insert(this);
	otherLane->addInterference(this, threshold, subResolution);

	return true;
}

LaneInterferenceDesc* Lane::getInterferenceAfter(Lane* otherLane, float t)
{
	InterfIter start = interferences.upper_bound(t);
	
	for (InterfIter i = start; i != interferences.end(); ++i)
		if (otherLane->contains(i->second.otherSeg.segment))
			return &i->second;
	
	return 0;
}

bool Lane::interferesWith(Lane* otherLane)
{
	return interferingLanes.find(otherLane) != interferingLanes.end();
}

bool Lane::contains(Lane* otherLane)
{
	return otherLane == this;
}

float Lane::mapInterferenceT(LaneInterferenceDesc const& idesc, float t)
{
	assert( t >= idesc.otherSeg.t_start &&
	        t <= idesc.otherSeg.t_end );
	
	if (idesc.otherSeg.t_start == idesc.otherSeg.t_end)
		return idesc.mySeg.t_start;
		
	float displacement = (idesc.lengthRatio > 0) ?
		idesc.otherSeg.segment->spline->evalLength( idesc.otherSeg.t_start, t) * idesc.lengthRatio :
		idesc.otherSeg.segment->spline->evalLength( t, idesc.otherSeg.t_end ) * -idesc.lengthRatio;
	
	return idesc.mySeg.segment->spline->evalDisplacement( 
		idesc.mySeg.t_start, displacement);
}

std::pair<float, Object*> Lane::getObjectBefore(float t, std::string const& classID, 
												bool interference)
{
	std::pair<float, Object*> null(-1.0f, (Object*)0);
	std::pair<float, Object*> closest;
	
	// grab the *first* object that's *not less than* t
	FtoIter closestIter = tToObj.lower_bound(t);
	// now get the *last* object that *is* less than t, if there is such an object
	if (closestIter != tToObj.begin())
		--closestIter;
	else
		closestIter = tToObj.end();
	
	// make sure it's got the correct classID
	if (classID != "object" && closestIter != tToObj.end())
		while (closestIter != tToObj.begin() && 
			   closestIter->second->getClassID() != classID)
			--closestIter;
	
	if (closestIter != tToObj.end() && 
		(closestIter->second->getClassID() == classID || classID == "object"))
		closest = *closestIter;
	else  
		closest = null;
	
	if (interference)
	{
		// grab the *first* interference interval that's out of consideration
		InterfIter interfBound = interferences.upper_bound(t);
	
		// iterate through each interval to be considered (that is, ones containing objects
		// that would appear before t on the local lane)
		for (InterfIter i = interferences.begin(); i != interfBound; ++i)
		{
			LaneInterferenceDesc idesc = i->second;
			// get the interval on the other lane that may contain interfering cars
			FtoIter segStart = idesc.otherSeg.segment->tToObj.lower_bound( idesc.otherSeg.t_start );
			FtoIter segEnd   = idesc.otherSeg.segment->tToObj.lower_bound( idesc.otherSeg.t_end );
			
			// and find any objects that are closer, but still less than the input t
			for (FtoIter j = segStart; j != segEnd; ++j)
			{
				float mappedT = mapInterferenceT(idesc, j->first);
				
				if (mappedT > closest.first && mappedT < t &&  // *
					(j->second->getClassID() == classID || classID == "object")) 
					closest = std::make_pair(mappedT, j->second);
			} // end for object in interval
		} // end for interval in interference intervals
	} // end if interference scanning is requested
	
	// the following condition occurs only when no interfering cars were found (note line marked *)
	// so "closestIter" still contains the data of the result to be returned. So if there really 
	// are no objects to be found before the given t, then we can just return a null result.
	while (closest.first >= t || closest == null)
	{
		if (closestIter == tToObj.begin())
			return null;
		else
			closest = *--closestIter;
	}

	// closest should now contain the closest object to t, that's still strictly less than t
	// if no such object exists, it will contain the null value
	return closest;
}

std::pair<float, Object*> Lane::getObjectAfter(float t, std::string const& classID, 
												bool interference)
{
	std::pair<float, Object*> auxNull(2.0f, (Object*)0);
	std::pair<float, Object*> null(-1.0f, (Object*)0);
	std::pair<float, Object*> closest;
	
	// grab the *first* object that's *greater than* t
	FtoIter closestIter = tToObj.upper_bound(t);
	// make sure it's got the correct classID
	if (classID != "object" && closestIter != tToObj.end())
		while (closestIter != tToObj.end() &&
			   closestIter->second->getClassID() != classID)
			++closestIter;
					
	if (closestIter != tToObj.end())
		closest = *closestIter;
	else  
		closest = auxNull;

	if (interference)
	{
		// grab the *first* interference interval that's in consideration
		InterfIter interfBound = interferences.lower_bound(t);
		
		// iterate through each interval to be considered (that is, ones containing objects
		// that would appear after t on the local lane)
		for (InterfIter i = interfBound; i != interferences.end(); ++i)
		{
			LaneInterferenceDesc idesc = i->second;
			// get the interval on the other lane that may contain interfering cars
			FtoIter segStart = idesc.otherSeg.segment->tToObj.lower_bound( idesc.otherSeg.t_start );
			FtoIter segEnd   = idesc.otherSeg.segment->tToObj.lower_bound( idesc.otherSeg.t_end );
			
			// and find any objects that are closer, but still greater than the input t
			for (FtoIter j = segStart; j != segEnd; ++j)
			{	
				float mappedT = mapInterferenceT(idesc, j->first);
				if (mappedT < closest.first && mappedT > t && 
					(j->second->getClassID() == classID || classID == "object"))
					closest = std::make_pair(mappedT, j->second);
			} // end for object in interval
		} // end for interval in interference intervals
	} // end if interference scanning is requested

	// closest should now contain the closest object to t, that's still strictly greater than t
	// if no such object exists, it will contain the null value
	
	if (closest == auxNull)
		return null;
	else
		return closest;
}

float Lane::auxMapT(Lane* otherLane, float t)
{
	// mapping backwards from normal lane to normal lane
	if (otherLane == this)
		return t;
		
	for (InterfIter i = otherLane->interferences.begin();
		 i != otherLane->interferences.end(); ++i)
	{
		if (i->second.otherSeg.segment == this &&
			i->second.otherSeg.t_start <= t && i->second.otherSeg.t_end >= t)
			return otherLane->mapInterferenceT(i->second, t);
	}
	
	return -1.0f;
}

std::ostream& operator<<(std::ostream& os, LaneInterferenceDesc const& desc)
{
	return os << "InterferenceDesc( " << desc.mySeg << " , " << desc.otherSeg << " , " 
			  << desc.lengthRatio << " )\n";
}

std::istream& operator>>(std::istream& is, LaneInterferenceDesc& desc)
{
	is.exceptions ( std::istream::eofbit | std::istream::failbit | std::istream::badbit );
	
	try
	{
		matchString(is, "InterferenceDesc");
		matchString(is, "(");
		is >> desc.mySeg;
		matchString(is, ",");
		is >> desc.otherSeg;
		matchString(is, ",");
		is >> desc.lengthRatio;
		matchString(is, ")");
	}
	catch(ParseError& e)
	{
		e.info = "Failed to parse LaneInterferenceDesc:\n" + e.info;
		throw;
	}
	catch(std::exception& e)
	{
		throw ParseError("Failed to parse LaneInterferenceDesc:\n" + std::string(e.what()) );
	}
	
	return is;
}

void Lane::serialize(std::ostream& os) const
{
	os << "spline --> " << spline;
	//os << "objects --> " << objToT;
	os << "interferences --> " << interferences;	
}

void Lane::deserialize(std::istream& is)
{
	Object* splineObj;
	DESERIALIZE_RN(is, "spline", splineObj);
	spline = (Spline*)splineObj;
	
	//DESERIALIZE_RN(is, "objects", objToT);
	objToT.clear();
	DESERIALIZE(is, interferences);	
	
	tToObj.clear();
	interferingLanes.clear();
	
	for (OtfIter i = objToT.begin(); i != objToT.end(); ++i)
		tToObj.insert( std::make_pair(i->second, i->first) );
	for (InterfIter i = interferences.begin(); i != interferences.end(); ++i)
	{
		i->second.mySeg.segment = this;
		this->_addReference(i->second.otherSeg.segmentName, 
							(Object**)&(i->second.otherSeg.segment));
	}
}
	
typedef std::vector<LaneSegment>::iterator SubLaneIter;
typedef std::vector<LaneSegment>::const_iterator ConstSubLaneIter;

MultiLane::MultiLane(): totalSize(0.0f) {}

MultiLane::MultiLane(std::vector<LaneSegment> const& subLanes): totalSize(0.0f)
{
	initialize(subLanes);
}

MultiLane::~MultiLane()
{
}

MultiLane* MultiLane::downcast(Object* source)
{
	return dynamic_cast<MultiLane*>(source);
}

void MultiLane::initialize(std::vector<LaneSegment> const& subLanes)
{
	this->subLanes = subLanes;
	
	std::vector<JoinDesc> jdv;
	for (ConstSubLaneIter i = subLanes.begin(); i != subLanes.end(); ++i)
	{
		jdv.push_back( JoinDesc(i->segment->spline, i->t_start, i->t_end) );
		totalSize += i->t_end - i->t_start;
	}
	
	spline = new Spline();
	chunks = spline->initialize(jdv);
}

bool MultiLane::addObject(Object* object, float t)
{	
	std::pair<unsigned int, float> mapped = mapToSubLane(t);
	return subLanes[mapped.first].segment->addObject(object, mapped.second);
}

bool MultiLane::removeObject(Object* object)
{
	for (SubLaneIter i = subLanes.begin(); i != subLanes.end(); ++i)
		if (i->segment->removeObject(object))
			return true;

	return false;
}

bool MultiLane::setT(Object* object, float newT)
{
	return removeObject(object) && addObject(object, newT);
}
	
float MultiLane::getT(Object* object)
{
	for (unsigned int i = 0; i != subLanes.size(); ++i)
	{
		OtfIter j = subLanes[i].segment->objToT.find(object);
		if (j != subLanes[i].segment->objToT.end())
			return mapFromSubLane(i, j->second);
	}
	
	assert(false);
}
			
float MultiLane::mapT(Lane* otherLane, float t)
{
	for (unsigned int i = 0; i != subLanes.size(); ++i)
	{
		float mappedT = otherLane->auxMapT(subLanes[i].segment, t);
		if (mappedT != -1.0f)
			return mapFromSubLane(i, mappedT);
	}
	
	return -1.0f;
}

float MultiLane::mapT(MultiLane* otherLane, float t)
{	
	std::pair<unsigned int, float> mapped = otherLane->mapToSubLane(t);	 
	return mapT(otherLane->subLanes[mapped.first].segment, mapped.second);
}

bool MultiLane::simulate(float ticks)
{
	// multilanes are "virtual", so they don't simulate.
	return true;
}

void MultiLane::draw()
{
	// multilanes are "virtual", so they don't draw.
}

bool MultiLane::addInterference(Lane* otherLane, float threshold, unsigned int subResolution)
{
	// multilanes are "virtual", so you can't add interferences to them directly
	assert(false);
	return false;
}

LaneInterferenceDesc* MultiLane::getInterferenceAfter(Lane* otherLane, float t)
{
	std::pair<unsigned int, float> mapped = mapToSubLane(t);
	LaneInterferenceDesc* result = 0;
	
	while (!result && mapped.first < subLanes.size())
	{
		result = subLanes[mapped.first].segment->getInterferenceAfter(otherLane, mapped.second);
		
		++mapped.first;
		mapped.second = -1.0f;
	}
	
	return result;
}

bool MultiLane::contains(Lane* otherLane)
{
	if (otherLane == this)
		return true;
		
	for (unsigned int i = 0; i < subLanes.size(); ++i)
		if (subLanes[i].segment == otherLane)
			return true;
			
	return false;
}

bool MultiLane::interferesWith(Lane* otherLane)
{
	for (unsigned int i = 0; i < subLanes.size(); ++i)
		if (subLanes[i].segment->interferesWith(otherLane))
			return true;
			
	return false;
}

std::pair<float, Object*> MultiLane::getObjectBefore(float t, std::string const& classID, 
													  bool interference)
{
	std::pair<unsigned int, float> mapped = mapToSubLane(t);	
	std::pair<float, Object*> result = std::make_pair(-1.0f, (Object*)0);
	
	while (!result.second && mapped.first < subLanes.size()) // damn unsigned ints won't let me check for <= 0
	{
		result = subLanes[mapped.first].segment->getObjectBefore(mapped.second, classID, interference);
		result.first = mapFromSubLane(mapped.first, result.first);
		
		--mapped.first;
		mapped.second = 2.0f;
	}
	
	return result;
}
	
std::pair<float, Object*> MultiLane::getObjectAfter(float t, std::string const& classID, 
													bool interference)
{
	std::pair<unsigned int, float> mapped = mapToSubLane(t);
	std::pair<float, Object*> result = std::make_pair(-1.0f, (Object*)0);
	
	while (!result.second && mapped.first < subLanes.size())
	{
		result = subLanes[mapped.first].segment->getObjectAfter(mapped.second, classID, interference);
		result.first = mapFromSubLane(mapped.first, result.first);
		
		++mapped.first;
		mapped.second = -1.0f;
	}
	
	return result;
}

float MultiLane::auxMapT(Lane* otherLane, float t)
{
	// mapping backwards from multi-lane to normal lane
	std::pair<unsigned int, float> mapped = mapToSubLane(t);
	return subLanes[mapped.first].segment->auxMapT(otherLane, mapped.second);
}

std::pair<unsigned int, float> MultiLane::mapToSubLane(float t)
{
	if (t >= 1.0f)
		return std::make_pair(subLanes.size() - 1, 1.0f);
	else if (t <= 0.0f)
		return std::make_pair(0, 0.0f);
		
	std::pair<unsigned int, float> indexInfo = spline->_mapTCoord(t);
	
	for (unsigned int i = 0; i < subLanes.size(); ++i)
	{
		if (indexInfo.first >= chunks[i])
		{
			indexInfo.first -= chunks[i];
			continue;
		}
		
		return std::make_pair(i, subLanes[i].t_start 
			+ ((float)indexInfo.first + indexInfo.second 
				+ ((i == 0)?0:1) ) / float(subLanes[i].segment->spline->getResolution()));
	}
	
	assert(false);
	return std::make_pair(0, 0.0f);
}

float MultiLane::mapFromSubLane(unsigned int index, float t)
{
	assert(index < chunks.size());
	
	std::pair<unsigned int, float> indexInfo = 
		subLanes[index].segment->spline->_mapTCoord(t);
	for (unsigned int i = 0; i < index; ++i)
		indexInfo.first += chunks[i];
	
	indexInfo.first -= subLanes[index].segment->spline->_mapTCoord(
						subLanes[index].t_start).first;
	if (index != 0) --indexInfo.first; 
	
	return ((float)indexInfo.first + indexInfo.second) / spline->getResolution();
}

void MultiLane::serialize(std::ostream& os) const
{
	os << "spline --> " << spline;
	os << "totalSize --> " << totalSize;
	os << "subLanes --> " << subLanes;
}

void MultiLane::deserialize(std::istream& is)
{
	Object* splineObj;
	DESERIALIZE_RN(is, "spline", splineObj);
	spline = (Spline*)splineObj;
	
	DESERIALIZE(is, totalSize);
	DESERIALIZE(is, subLanes);	
	
	for (SubLaneIter i = subLanes.begin(); i != subLanes.end(); ++i)
		this->_addReference(i->segmentName, (Object**)&(i->segment));
}
	
}
