#include "roadFinding.hh"

#include <iostream>
#include <fstream>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <opencv/cv.h>
#include <opencv/cvaux.h>
#include <opencv/highgui.h>

#include "DGCutils"

#include "stereovision/stereoSource.hh"
#include "stereovision/stereoProcess.hh"
#include "frames/frames.hh"

extern bool waitForState;

using namespace std;

/*!
	BEFORE GOING ANY FURTHER, you should be acquainted with the open computer vision library
	The OpenCV library provides image support for computing that ranges from low level storing 
	and processing to high level analysis and functions. http://opencvlibrary.sourceforge.net/
*/
 
	
/*---------------- PUBLIC ----------------------------------------*/
//! PUBLIC
/*---------------- PUBLIC ----------------------------------------*/

/*----------------------------------------------------------------*/
//! CONSTRUCTOR AND DESTRUCTOR
/*----------------------------------------------------------------*/

roadFinding::roadFinding(int skynetKey): CSkynetContainer(MODsurfRoad, skynetKey) {
	
	//! by default use cameras
	_source=USE_CAMERAS;
	_calibration = 0;
	refreshCalFrame=0;
	
	//! start in frame 0
	iFrame=0;
	
	//! debug and logging default options
	_debug = 0;
	_autoSaveImages = 1;			
	
	//! Image properties 
	imageWidth = 640;
	imageHeight = 480;

	//! Ignore alice from bottom of image
	_ignoreAlice = 1;
	//! Number of pixels for Alice on bottom
	aliceYOffset = 70;

	//! initialize image objects init
	CvSize pairSize;
	pairSize = cvSize(imageWidth, imageHeight);

	//! Creates image header and allocates data
 	cvLeftImage = cvCreateImage(pairSize, IPL_DEPTH_8U, 1);
	cvRightImage = cvCreateImage(pairSize, IPL_DEPTH_8U, 1);
	cvProcessImage = cvCreateImage(pairSize, IPL_DEPTH_8U, 1);
	cvBufferImage = cvCreateImage(pairSize, IPL_DEPTH_8U, 1);
	
	cout << endl << "SKYNET_KEY=" << skynetKey << endl<< endl;
}


roadFinding::~roadFinding() {
	//! roadFinding images
	cvReleaseImage(&cvLeftImage);
	cvReleaseImage(&cvRightImage);
	cvReleaseImage(&cvBufferImage);
	cvReleaseImage(&cvProcessImage); 

	//! calibration images
 	cvReleaseImage(&cvCalLeft);
	cvReleaseImage(&cvCalRight);
	cvReleaseImage(&cvCalEdges);
	cvReleaseImage(&cvCalLanes);
	
	if (_debug) cout << "Images released" << endl << "Program terminated" << endl;
}

/*----------------------------------------------------------------*/
//! ROAD FINDING INITIALIZATION FUNCTIONS
/*----------------------------------------------------------------*/

int roadFinding::initStereo() {
	//! If no parameters were provided do default
	initStereo("/home/tallberto/dgc/projects/humberto/images/", "stereo", "bmp");
	return 1;
}

int roadFinding::initStereo(char *dir, char *base, char *ext) {
	char directory[100];
	char extension[4];
	
	sprintf(directory, "%s%s", dir, base);
	sprintf(extension, "%s", ext);
	
	//! If we are using cameras, innitialize camera parameters from configuration
	if (_source==USE_CAMERAS) { 
		if(stereoImageSource.init(0, "../../drivers/stereovision/config/stereovision/CamID.ini.short", "tmp", "bmp", "../../drivers/stereovision/config/stereovision/SVSParams.ini.short")!=stereoSource_OK) {
			cout << "Error while initializing cameras!  Quitting...\n";
			exit(1);
		}
	}
	
	//! If we are using cameras or logged files, innitialize stereovision parameters
	stereoImageProcess.init(0, "../../drivers/stereovision/config/stereovision/SVSCal.ini.short",
							 "../../drivers/stereovision/config/stereovision/SVSParams.ini.short",
							 "../../drivers/stereovision/config/stereovision/CamCal.ini.short",
							 "temp",
							 "bmp",
							 0,
							 "../../drivers/stereovision/config/stereovision/SunParams.ini.short",
							 "../../drivers/stereovision/config/stereovision/PointCutoffs.ini.short");

	//! If we are using files, innitialize file image parameters
	if (_source==USE_FILES) {
		stereoImageSource.init(0, imageWidth, imageHeight, directory, extension);
	}
	
	//! calibration files init
	if (_calibration) {
		initCalibrationImages();
	}
	
	
	//! exposure control
	if (_source==USE_CAMERAS) {
		stereoImageSource.startExposureControl(stereoImageProcess.subWindow);	
	}

	DGCcreateMutex(&cameraMutex);

	return 1;
}

/*------------------------------------------------------------------------------*/
//! ROAD FINDING LOOP FUNCTION
/*------------------------------------------------------------------------------*/

int roadFinding::loopStereo(int maxFrames, int maxJump) {
	iFunction = "refreshStereo";
    if (_debug) cout << iFunction << endl;
	
	int pairIndex=0, prevIndex=0; 
	
	//! while there hasnt been an abnormal jump in frames And we havent processed enough frames as those requested (0 for endless loop)
	while( (pairIndex - prevIndex <= maxJump) && (iFrame < maxFrames || !maxFrames)) {

		/*! 
			Refresh the frame source (stereoImageSource object) only if required
			- if we're operating on a race situation
			- if we're in a calibration and asked for it (we dont more calibration adjustment, just a new image)
		*/
		if (!_calibration || (_calibration && refreshCalFrame)) {
			//! Lock the mutex
			DGClockMutex(&cameraMutex);
			pairIndex = stereoImageSource.pairIndex();	
		
			//! Try to grab another frame. If we're not succesfull, try again
			if (!stereoImageSource.grab()) {
				DGCunlockMutex(&cameraMutex);
				continue;
			}
			
			//! Reset the frame refresh flag for calibration
			refreshCalFrame=0;
			iFrame ++;
		
			UpdateState();
			iState.Northing = m_state.Northing;
			iState.Easting = m_state.Easting;
			iState.Altitude = m_state.Altitude;
			
			cout << "Frame " << iFrame << endl;
		}
		
		// use an empty state for the stereoprocess to return local precise (floating point) coordinates
		VehicleState local;
		stereoImageProcess.loadPair(stereoImageSource.pair(), local);
		
		/*!
			This will use the UTM coordinate, however if using this the stereoprocess will output coordinates rounded to the meter
			which is not enough for the calculations we need
		*/
		// stereoImageProcess.loadPair(stereoImageSource.pair(), m_state);
		
		
		//! Stage 1
		/*
			Create OpenCV image objects to store left and right source images from the files or cameras
		*/
	 	createImageObjects(stereoImageSource.left(),stereoImageSource.right());
		
		//! Stereosource not needed anymore
		DGCunlockMutex(&cameraMutex);
		
		//! Doing stereo processing on the raw images: pitch control
		stereoImageProcess.pitchControl();
		//! Doing stereo processing on the raw images: pitch control
		stereoImageProcess.calcRect();
		stereoImageProcess.calcDisp();

		//! Stage 3
		edgeFinding();

		/*! 
			Stage 3.5 - unexisting
			should provide an approximation of the road
			based on last iteration of road finding, ladar and stereovision
		*/
			
		//! Stage 4
		laneMarker();
			
		//! Stage 5 (left camera)
		/*!
			Right now the real translation of the perspective of the roads 
			is being done by the laneMarker itself, this function is only
			for knowing which points actually have a 3D coordinate translation
			
		*/
		perspectiveTranslation();
		
		//! Stage 6
		interface(); 	

		
		prevIndex = pairIndex;
		
		if (_calibration) return iFrame;
 	}
	
	//! Violation of frame skip
	if (pairIndex - prevIndex > maxJump) cout << "More than " << maxJump << " frames have been skipped. End of feed" << endl;
 
 	return iFrame;
}

/*-----------------------------------------------------------------*/
//! CALIBRATION FUNCTIONS
/*-----------------------------------------------------------------*/

int roadFinding::initCalibrationImages() {
	CvSize pairSize;
	pairSize = cvSize(imageWidth, imageHeight);
	
	//! creates the image files and allocates size
 	cvCalLeft = cvCreateImage(pairSize, IPL_DEPTH_8U, 1);
	cvCalRight = cvCreateImage(pairSize, IPL_DEPTH_8U, 1);
	cvCalEdges = cvCreateImage(pairSize, IPL_DEPTH_8U, 1);
	cvCalLanes = cvCreateImage(pairSize, IPL_DEPTH_8U, 1);

	return 1;
}	

int roadFinding::refreshCalibrationParameters(int line_link_pos, int main_line_pos, int acc_pos, int min_line_length_pos, int max_gap_pos, int distance_res_pos, int angle_res_pos, int size_tol_pos, int angle_tol_pos, int distance_tol_pos) {
	//! Edge finding parameters
	EFlineLink = line_link_pos;
	EFmainLine = main_line_pos;

	//! Lane marker parameters
	LMacc = acc_pos;
	LMminLineLength = min_line_length_pos;
	LMmaxGap = max_gap_pos;
	LMdistanceResolution = distance_res_pos;
	LMangleResolution = angle_res_pos;
	LMsizeTol = (((float) (size_tol_pos))/10.0);
	LMangleTol = (((float) (angle_tol_pos))/10.0);
	LMdistanceTol = (((float) (distance_tol_pos))/10.0);

	return 1;
}

/*-----------------------------------------------------------------*/
//! HELPER FUNCTIONS
/*-----------------------------------------------------------------*/

int roadFinding::loadCalibration() {
	loadCalibration("roadFinding.cal");
	return 1;
} 

int roadFinding::loadCalibration(char* fromFile) {
	char *next;
	string line;
	
	ifstream calibration(fromFile);
	
	//* Loops the lines looking for the parameters
	while (!calibration.eof()) {
		getline(calibration, line);
		if (line == "# Image: width; height") { 
			next = "width"; continue;
		} 
		if (next == "width") {
			imageWidth = atoi(line.c_str());
			next = "height"; continue;
		} 
		if (next == "height") {
			imageHeight = atoi(line.c_str());
			next = "null"; continue;
		} 
		if (line == "# EdgeFinding thresholds: Line Linking; Main Line") { 
			next = "EFlineLink"; continue;
		}
		if (next == "EFlineLink") {
			EFlineLink = atoi(line.c_str());
			next = "EFmainLine"; continue;
		}
		if (next == "EFmainLine") {
			EFmainLine = atoi(line.c_str());
			next = "null"; continue;
		}
		if (line == "# LaneMarker: Accumulator; Min Line Length; Max Gap; Distance Resolution; Angle Resolution; Size Tolerance; Angle Tol.; Distance Tol. ") { 
			next = "LMacc"; continue;
		}
		if (next == "LMacc") {
			LMacc = atoi(line.c_str());
			next = "LMminLineLength"; continue;
		}
		if (next == "LMminLineLength") {
			LMminLineLength = atoi(line.c_str());
			next = "LMmaxGap"; continue;
		}
		if (next == "LMmaxGap") {
			LMmaxGap = atoi(line.c_str());
			next = "LMdistanceResolution"; continue;
		}
		if (next == "LMdistanceResolution") {
			LMdistanceResolution = atoi(line.c_str());
			next = "LMangleResolution"; continue;
		}
		if (next == "LMangleResolution") {
			LMangleResolution = atoi(line.c_str());
			next = "LMsizeTol"; continue;
		}
		if (next == "LMsizeTol") {
			LMsizeTol = atof(line.c_str());
			next = "LMangleTol"; continue;
		}
		if (next == "LMangleTol") {
			LMangleTol = atof(line.c_str());
			next = "LMdistanceTol"; continue;
		}
		if (next == "LMdistanceTol") {
			LMdistanceTol = atof(line.c_str());
			next = "null"; continue;
		}
	}
	calibration.close();
	return 1;
	
}
	

/*---------------- PRIVATE ----------------------------------------*/
//! PRIVATE
/*---------------- PRIVATE ----------------------------------------*/


/*-----------------------------------------------------------------*/
//! Road Finding stage functions
/*-----------------------------------------------------------------*/

int roadFinding::createImageObjects(unsigned char* leftImagePtr, unsigned char* rightImagePtr) {
	
	//! setting the data from the pointer
	cvSetData(cvLeftImage, (char*)leftImagePtr, imageWidth);
	cvSetData(cvRightImage, (char*)rightImagePtr, imageWidth);

	//! set Region Of Interest to ignore Alice
	if (_ignoreAlice) cvSetImageROI(cvLeftImage, cvRect(0,0,imageWidth,imageHeight-aliceYOffset));
	if (_ignoreAlice) cvSetImageROI(cvRightImage, cvRect(0,0,imageWidth,imageHeight-aliceYOffset));

	//! if we're doing a calibration, lets fill some duplicates for storing the results
	if (_calibration) {
		cvCalLeft = cvCloneImage(cvLeftImage);
		cvCalRight = cvCloneImage(cvRightImage);
	}

	if (_autoSaveImages || saveCalIteration) {
		cvBufferImage = cvCloneImage(cvLeftImage);
		iFunction="left";
		saveImage();
		cvBufferImage = cvCloneImage(cvRightImage);
		iFunction="right";
		saveImage();
	}
	
	//! image that will carry the whole process, being successfully refined
	cvProcessImage = cvCloneImage(cvLeftImage);
	return 0;
}

int roadFinding::edgeFinding() {
	//! calls with parameters
	edgeFinding(EFlineLink,EFmainLine);
	
	return 1;
}

int roadFinding::edgeFinding(int lineLinkThreshold, int mainLineThreshold) {
	iFunction = "edgeFinding";
 	if (_debug) cout << iFunction << endl;  	

	//! Canny edge-detection with sobel kernel size 3
	//! Always re-evaluate calibration as algs get more refined
	cvCanny(cvProcessImage, cvProcessImage, lineLinkThreshold, mainLineThreshold, 3);

	//! if we're doing a calbration, use another image
	if (_calibration) {
		cvCalEdges = cvCloneImage(cvProcessImage);
	}

	//! 	
	if (_autoSaveImages || saveCalIteration) { 
		cvBufferImage = cvCloneImage(cvProcessImage);
		saveImage();
	}
	
	return 0;
}

int roadFinding::laneMarker() {
	//! calls with parameters
	laneMarker(LMacc,LMminLineLength,LMmaxGap,LMdistanceResolution,LMangleResolution, LMsizeTol, LMangleTol, LMdistanceTol);
	return 1;
}

int roadFinding::laneMarker(int accThreshold, int minLineLengthThreshold, int maxGapThreshold, int distanceResolution, int angleResolution, float sizeTol, float angleTol, float distanceTol) {
	iFunction = "laneMarker";
	if (_debug) cout << iFunction << endl;

   int i,j;

	CvSize pairSize;
	pairSize = cvSize(imageWidth, imageHeight);
	
	IplImage* cvColorImage = cvCreateImage(pairSize, IPL_DEPTH_8U, 3 );
	if (_ignoreAlice) cvSetImageROI(cvColorImage, cvRect(0,0,imageWidth,imageHeight-aliceYOffset));
   
   CvMemStorage* storage = cvCreateMemStorage(0);
   CvSeq* lines = 0;
   
   cvCvtColor(cvProcessImage, cvColorImage, CV_GRAY2BGR );

	//! lines are not stored in any particular reason in HOUGH_PROBABILISTIC
   lines = cvHoughLines2(cvProcessImage, storage, CV_HOUGH_PROBABILISTIC, distanceResolution, angleResolution*CV_PI/180, accThreshold, minLineLengthThreshold, maxGapThreshold);
	
	if (_debug) cout << "lines found: "<< lines->total << endl;

	//! vector to hold the classification of each line
	/*! 
		Classification:
		0 line not yet processed
		1 unimportant line
		2 big isolated line
		3 smaller line of a pair of lines
		4 bigger line of a pair of lines
	*/
	vector<int> lineCat(lines->total,0);

 	for( i = 0; i < lines->total; i++ ) {
   	//! ignore this line if it has been considered by a parallel one
   	if (lineCat[i] != 0) continue;
   	
   	CvPoint* line = (CvPoint*) cvGetSeqElem(lines,i);
		CvPoint* line2;	
		
   	int hasPair=0;
   	
		// check if there is a double line
		for (j=i+1; j< lines->total; j++) {
			line2 = (CvPoint*) cvGetSeqElem(lines,j);	
			hasPair = checkDouble((CvPoint) line[0],(CvPoint) line[1],(CvPoint) line2[0],(CvPoint) line2[1],sizeTol,angleTol,distanceTol);
			if (hasPair) break;
		}
    	
    	//! this could be prettier, checking all and not one possible pair for each line and grouping closer pairs
 		//! lines with pairs  
    	if (hasPair) { 
    		if( distance((CvPoint) line[0], (CvPoint) line[1]) > distance((CvPoint) line2[0], (CvPoint) line2[1]) ) {
    			lineCat[i]=4;
    			lineCat[j]=3;
    		} else {
    			lineCat[i]=3;
    			lineCat[j]=4;
    		}
    	//! if line is big enough, let's still take it on caution
    	} else if (distance((CvPoint) line[0], (CvPoint) line[1]) > 100) {
    			lineCat[i]=2;
     	} else {
    	//! remaining lines are considered unimportant
    			lineCat[i]=1;
    	}
	}

	// !m ake this pretty. render less important lines below
 	for( i = 0; i < lines->total; i++ ) {
   	CvPoint* line = (CvPoint*) cvGetSeqElem(lines,i);
		if (lineCat[i]==1) cvLine( cvColorImage, line[0], line[1], CV_RGB(255,0,0), 1, 8 );	
	}
 	for( i = 0; i < lines->total; i++ ) {
   	CvPoint* line = (CvPoint*) cvGetSeqElem(lines,i);
		if (lineCat[i]==2) cvLine( cvColorImage, line[0], line[1], CV_RGB(0,0,255), 1, 8 );	
	}	
 	for( i = 0; i < lines->total; i++ ) {
   	CvPoint* line = (CvPoint*) cvGetSeqElem(lines,i);
		if (lineCat[i]==3) cvLine( cvColorImage, line[0], line[1], CV_RGB(255,255,0), 1, 8 );	
	}
 	for( i = 0; i < lines->total; i++ ) {
   	CvPoint* line = (CvPoint*) cvGetSeqElem(lines,i);
		if (lineCat[i]==4) cvLine( cvColorImage, line[0], line[1], CV_RGB(0,255,0), 1, 8 );	
	}

	// vanishing point
	// ... to be implemented
	
	NEDcoord coord1;
	NEDcoord coord2;

	char file[50];
	char* dir = "output";
				
	if (_autoSaveImages || saveCalIteration) {
		if (_calibration) {
			sprintf(file, "%s/RF_cal-%05d.lines", dir, iFrame);
		} else {
			sprintf(file, "%s/RF-%05d.lines", dir, iFrame);
		}	
		ofstream realLines(file,ofstream::trunc);
		realLines << "N: " << iState.Northing << ", E: " << iState.Easting << ", A: " << iState.Altitude << endl;
		realLines.close();
	}					
	
	for(i = 0; i < lines->total; i++ ){
		int isLine=0;
		
		CvPoint* line = (CvPoint*) cvGetSeqElem(lines,i);
		//! for these types of lines
		if (lineCat[i]==4 || lineCat[i]==2) {

			int x1,y1,x2,y2;
			
			//! now we want at least 2 3D points for each line, so that we can render them
			// vertical line, x is constant
			if (line[0].x==line[1].x) {
				x1 = line[0].x;

				for (y1=(line[0].y<line[1].y ? line[0].y:line[1].y); !isLine && y1 <= (line[0].y<line[1].y ? line[1].y:line[0].y); y1++) {

					if (stereoImageProcess.SinglePoint(x1,y1,&coord1)) {
						for (y2=(line[0].y<line[1].y ? line[1].y:line[0].y); !isLine && y2 >= (line[0].y<line[1].y ? line[0].y:line[1].y); y2--) {
							if (stereoImageProcess.SinglePoint(x1,y2,&coord2))	{
								isLine=1;
							}
						}
					}
				}

			// non vertical line
			} else {

				// define y=m*x+b equation
				double m = (double) (line[1].y-line[0].y)/(line[1].x-line[0].x);
				double b = line[0].y-m*line[0].x;

				for ( x1=line[0].x ; !isLine && x1<=line[1].x ; x1++ ) {

					y1 = (int) round((double) m*x1+b);	
					if (stereoImageProcess.SinglePoint(x1,y1,&coord1)) {

						for ( x2=line[1].x ; !isLine && x2>=line[0].x ; x2-- ) {
			
							y2 = (int) round((double) m*x2+b);	
							if (stereoImageProcess.SinglePoint(x2,y2,&coord2)) {	
								isLine=1;
								
							}
						}
					}
				}

			}

			if (isLine) {
				float mod = sqrt(pow((double) (coord1.N-coord2.N),2)+pow((double) (coord1.E-coord2.E),2)+pow((double) (coord1.D-coord2.D),2));
				
				//! coord1 and coord2 reached the same unique point on the line; forget this line
				if (mod == 0.0) continue;

				//! start point on parametric line
				CvPoint3D64f S;
				S.x = (double) coord1.N;
				S.y = (double) coord1.E;
				S.z = (double) coord1.D;
				

				//! resolution of output (distance of points in same lines) in meters
				float scale = 0.5;


				//! unity (1 meter) vector of parametric line * sclae				
				// doubles
				CvPoint3D64f U;
				U.x = (double) scale*(coord2.N-coord1.N)/mod;
				U.y = (double) scale*(coord2.E-coord1.E)/mod;
				U.z = (double) scale*(coord2.D-coord1.D)/mod;
				
				//! steps. use parametric line to get points
				for (int k=0;k<=(int)(mod/scale);k++) {
					CvPoint3D64f L;					
					L.x=S.x+k*U.x;
					L.y=S.y+k*U.y;
					L.z=S.z+k*U.z;
					
					if (_autoSaveImages || saveCalIteration) {
						ofstream realLines(file,ofstream::app);
						realLines << i << "," << L.x << "," << L.y << "," << L.z << endl;
						realLines.close();
					}
				}			
			}
		}
	}
	
	cvProcessImage = cvCloneImage(cvColorImage);	
	
	//! if we're doing a calbration, use another image
	if (_calibration) {
		cvCalLanes = cvCloneImage(cvProcessImage);
	}
	
	if (_autoSaveImages || saveCalIteration) {
		cvBufferImage = cvCloneImage(cvColorImage);
		saveImage();
		saveCalIteration=0;
	}

	cvReleaseImage(& cvColorImage);
   return 0;
}

int roadFinding::perspectiveTranslation() {
	iFunction = "perspectiveTranslation";
	if (_debug) cout << iFunction << endl;

	NEDcoord myCoord;
	int numValidPixels=0;
	
	CvSize pairSize;
	pairSize = cvSize(imageWidth, imageHeight);
	
	IplImage* cvImage1 = cvCreateImage(pairSize, IPL_DEPTH_8U, 3);
	if (_ignoreAlice) cvSetImageROI(cvImage1, cvRect(0,0,imageWidth,imageHeight-aliceYOffset));
	
	cvCvtColor(cvLeftImage, cvImage1, CV_GRAY2BGR);

	//! this process works on the left camera
	for(int x=0; x<imageWidth; x++) {
		//! ignore alice from bottom of image. rewrite with ROI (region of interest)
	   for(int y=0; y<imageHeight-aliceYOffset; y++) { 
			//! if we have a valid point disparity, that is, we can get 3D coordinates from it
			if(stereoImageProcess.SinglePoint(x,y,&myCoord)) {
				numValidPixels++;
				((uchar *)(cvImage1->imageData + y*cvImage1->widthStep))[x*cvImage1->nChannels + 0]=0; // Blue
				((uchar *)(cvImage1->imageData + y*cvImage1->widthStep))[x*cvImage1->nChannels + 1]=0; // Green
				((uchar *)(cvImage1->imageData + y*cvImage1->widthStep))[x*cvImage1->nChannels + 2]=255; // Red
				if (_debug) cout << "FrameNum: " << stereoImageSource.pairIndex() - 1 << " " << x << ", " << y << " Point: " << myCoord.N << ", " << myCoord.E << ", " << myCoord.D << endl;
			}
		}
	}

	if (_autoSaveImages) {
		cvBufferImage = cvCloneImage(cvImage1);		
		saveImage();
	}

	if (_debug) cout << "FrameNum: " << stereoImageSource.pairIndex() - 1 << " " << numValidPixels << " valid pixels"  << endl;
	
	cvReleaseImage(&cvImage1);
	
	return 0;
}
	
int roadFinding::interface() {

	return 0;
}

/*---------------------------------------------*/
//! HELPER FUNCTIONS
/*---------------------------------------------*/
int roadFinding::saveImage() {
	saveImage("output", iFunction);
	return 1;
}

int roadFinding::saveImage(char* dir, char* function) {
	char file[50];
	
	if (function == "") function = iFunction;
	if (dir == "") dir = "output";
	
	if (_calibration) {
		sprintf(file, "%s/RF_cal_%s-%05d.bmp", dir, function, iFrame);
		cout << "Files for this iteration were saved" << endl;
	} else {
		sprintf(file, "%s/RF_%s-%05d.bmp", dir, function, iFrame);
	}
	
	cvSaveImage(file, cvBufferImage);
	
	return 0;
}

float roadFinding::getLineAngle(CvPoint start, CvPoint end) {
	float angle=0.0;
	
	//! starting point always has lower or equal X coordinate
	if (start.x == end.x) {
		angle = 90.0;
	} else {
		angle = 180/CV_PI * atan((float) (end.y-start.y)/(end.x-start.x));
	}

	return angle;
}

float roadFinding::distance(CvPoint start, CvPoint end) {
	return sqrt(pow((double) (start.x-end.x),2)+pow((double) (start.y-end.y),2));
}

float roadFinding::distance(float startx,float starty, float endx, float endy) {
	return sqrt(pow((double) (startx-endx),2)+pow((double) (starty-endy),2));
}

int roadFinding::checkDouble(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float sizeTol, float angleTol, float distanceTol) {
		
	//! size tolerance trial
	float size1 = distance(start1, end1);
	float size2 = distance(start2, end2);
	if ((size1 > size2 ? size1/size2 : size2/size1) > sizeTol) {
		return 0;
	}

	//! angle tolerance trial
	if (!parallel(start1, end1, start2, end2, angleTol)) {
		return 0;
	} 

	//! line convergence and alignment	
	if (!lineMatching(start1, end1, start2, end2, distanceTol)) {
		return 0;
	}

	// All trials passed. brillo
		
	return 1;
}

int roadFinding::parallel(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float angleTol) {
	float angle1 = getLineAngle(start1, end1);
	float angle2 = getLineAngle(start2, end2);
	float angleDiff = fabs(angle1-angle2);
	if (!(angleDiff<=angleTol || (180.0-angleDiff)<=angleTol)) return 0;

	return 1;
	
}

int roadFinding::lineMatching(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float distanceTol) {
	/*
		What we know: 
			a. Points in a line are ordered by lower X.
			a1. start1.x > end1.x
			a2. start2.x > end2.x
			b. line1 and line2 are almost parallel (this is after the angle tolerance trial)
	*/

	//! extreme 1 and 2 distance
	//! closer points at extremes
	float dE1, dE2;
	//! the bigger and smaller of dE1 and dE2
	float dMax, dMin;
	
	//! angles from -90 exclusive to +90 inclusive
	float angle1 = getLineAngle(start1, end1);
	float angle2 = getLineAngle(start2, end2);
	
	//! they are practically parallel. if the diference of angles is >90, we have a /\ or \/ tricky situation
	if (fabs(angle1-angle2) > 90) {
			dE1 = distance(start1,end2);
			dE2 = distance(start2,end1);	
	} else {
	//! if not, than just calculate normal distances
			dE1 = distance(start1,start2);
			dE2 = distance(end1,end2);	
	}
	
	dMax = dE1 >= dE2 ? dE1 : dE2;
	dMin = dMax == dE1 ? dE2 : dE1;

	if (dMin > distanceTol)	return 0;
	
	
	/*!
		There is another method that calculates the matching lengths of two parallel lines.
		It is somewhat more accurate in predicting some broken lines, but is still buggy
		For road lines it is not very important.
		The code is in deprecated_utils folder
	*/
	
	return 1;
}
