#ifndef __ROADFINDING_HH__
#define __ROADFINDING_HH__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cv.h>

#include "DGCutils"
#include "stereovision/stereoSource.hh"
#include "stereovision/stereoProcess.hh"
#include "GlobalConstants.h"
#include "frames/frames.hh"
#include "StateClient.h"
#include "sn_msg.hh"

#define USE_FILES 1
#define USE_CAMERAS 0

using namespace std;

//! The roadFinding Class.
/*! 
	This class is responsible for all functions related to creating, setting and running a roadFinding process.
*/

class roadFinding: public CStateClient {
public:
	//! The constructor.
	//! Initializes internal variables and global run properties.
	roadFinding(int skynetKey);

	//! The destructor.
	//! Does cleanup of OpenCV image objects.
	~roadFinding();
	
	//! ROAD FINDING INITIALIZATION FUNCTIONS

	//! Initializes stereoSource and stereoProcess objects with default settings.
    /*!
		\return 1 if success
		\sa _source()
	*/	
	int initStereo();
	
	//! Initializes stereoSource and stereoProcess objects.
    /*!
		Initializes with specific files and folders.
		If running from logged data, the images at dir/baseXXXXX-L.ext and dir/baseXXXXX-R.ext will loaded.
		\param dir Directory to get logged images from.
		\param base Base filename for images to get.
		\param ext Extension of images to get. See OpenCV doc for supported extensions.
		\return 1 if success
		\sa _source()
	 */	
	int initStereo(char *dir, char* base, char *ext);
	
	//! ROAD FINDING LOOP FUNCTION

	//! Repeatedly gets left and right frame and calls RoadFinding module stages
    /*!
		\param maxFrames Maximum number of real Frames to be captured.
		\param maxJump Maximum number of frames that can be skipped between two captured frames.
		\return number of successfull frames, if the process was correctly computed
		\return -1, if more than maxJump frames were skipped.
		\return -1, if frame grab failed.
		\sa _source(), refreshCalFrame()
	 */		
	int loopStereo(int maxFrames, int maxJump);

	//! CALIBRATION FUNCTIONS
	//! These functions are not supposed to be called within a race situation. 
	
	//! Initialized global images for storing a roadFinding process iteration's images.
    /*!
		This function is not supposed to be called within a race situation. 
		Loading and handling extra images, except for the pointer, is not optimal.
		\return 1
	 */		
	int initCalibrationImages();	

	//! Updates parameters for the road finding algorithms.
    /*!
		This function is not supposed to be called within a race situation. 
		\param line_link_pos Canny threshold used for line-linking. Should be smaller than main_line_pos, or their meanings swap.
		\param main_line_pos Canny threslhold used for finding initial segments of strong lines
		\param acc_pos Probabilistic Hough transform Accumulator threshold.
		\param min_line_length_pos P.H.T. minimum length in pixels of a detected line.
		\param max_gap_pos P.H.T. maximum gap allowed between segments to be considered the same line.
		\param distance_res_pos P.H.T. distance resolution in pixels.
		\param angle_res_pos P.H.T. angle resolution in pixels.
		\param size_tol_pos Line selector size tolerance between two parallel lines.
		\param angle_tol_pos Line selector angle tolerance between two parallel lines.
		\param distance_tol_pos Line selector distance tolerance between two parallel lines.
		\return 1
	 */			
	int refreshCalibrationParameters(int line_link_pos, int main_line_pos, int acc_pos, int min_line_length_pos, int max_gap_pos, int distance_res_pos, int angle_res_pos, int size_tol_pos, int angle_tol_pos, int distance_tol_pos);

	//! CALIBRATION VARIABLES
	
	//! Calibration images that hold stages output
 	IplImage* cvCalLeft;
	IplImage* cvCalRight;
	IplImage* cvCalEdges;
	IplImage* cvCalLanes;

	//! Are we calibrating
	int _calibration;
	//! Do we need to refresh the frame, or only refresh output for different parameters - the original images are the same.
	int refreshCalFrame;
	//! Do we want to save this iterations output
	int saveCalIteration;
	
	//! HELPER FUNCTIONS

	//! Loads calibration parameters from default location
	/*!
		\return 1
	 */	
	int loadCalibration();

	//! Loads calibration from specific file
	/*!
		\param file File to load algorithm parameters from
		\return 1
	 */		
	int loadCalibration(char* file);
			
	//! GLOBAL VARIABLES
	
	//! Vehicle state
	VehicleState iState;
	
	//! Frame being processed
	int iFrame;
	
	//! Debugging handler
	int _debug;

	//! Autosave images handler
	int _autoSaveImages;

	//! Determine images source. USE_FILES for logged files and USE_CAMERAS for live images
	int _source;

	//! Stop variable
	int _stop;
	
	//! image settings
	int imageWidth;
	int imageHeight;

	//! ignore alice from bottom of image
	int _ignoreAlice;
	
	//! ignore alice from bottom number of pixels
	int aliceYOffset; 

private:

	//! ROAD FINDING STAGE FUNCTIONS
	//! These functions pick the image from the previous stage and do their magic

	//! Loads images from the stereosource object (logged files or live camera) to OpenCV image objects, for later processing
	/*!
		\param leftImagePtr Pointer to the left image
		\param rightImagePtr Pointer to the right image
		\return 1
	 */		 
	int createImageObjects(unsigned char* leftImagePtr, unsigned char* rightImagePtr);

	//! Applies edge detection algorithms to the image with default parameters
	/*!
		\return 1
	 */		 
	int edgeFinding();

	//! Applies edge detection algorithms to the image with Canny parameters
	/*!
		\param lineLinkThreshold Canny threshold used for line-linking. Should be smaller than main_line_pos, or their meanings swap.
		\param mainLineThreshold Canny threslhold used for finding initial segments of strong lines
		\return 1
		\sa refreshCalibrationParameters()
	 */
	int edgeFinding(int lineLinkThreshold, int mainLineThreshold);
	
	//! Collects the points from the left image for which 3D information is available
	/*!
		This is not really being used at the time
		\return 1
	 */		 	
	int perspectiveTranslation();

	//! Applies lane finding algorithms to the image with default parameters
	/*!
		\return 1
	 */		 
	int laneMarker();

	//!	Applies lane finding algorithms to the image with parameters. Also calls line classification functions
	/*!
		\param accThreshold Probabilistic Hough transform Accumulator threshold.
		\param minLineLengthThreshold P.H.T. minimum length in pixels of a detected line.
		\param maxGapThreshold P.H.T. maximum gap allowed between segments to be considered the same line.
		\param distanceResolution P.H.T. distance resolution in pixels.
		\param angleResolution P.H.T. angle resolution in pixels.
		\param sizeTol Line selector size tolerance between two parallel lines.
		\param angleTol Line selector angle tolerance between two parallel lines.
		\param distanceTol Line selector distance tolerance between two parallel lines.		\return 1
		\sa refreshCalibrationParameters()
	 */
	int laneMarker(int accThreshold, int minLineLengthThreshold, int maxGapThreshold, int distanceResolution, int angleResolution, float sizeTol, float angleTol, float distanceTol);
	
	//! Interfaces road features to be used in other modules.
	/*!
		Not doing anything at the moment
		\return 1
	 */
	int interface();

	//! HELPER FUNCTIONS
	
	//! Save current stage image with default parameters
	/*!
		Saves with name of function that called it in standard "output" directory.
		\return 1
	 */
	int saveImage();
	
	//! Save current stage image in special directory and with custom name 
	/*!
		\param dir Directory to save image to
		\param function Base name to use in the image - number and extension will be added
		\return 1
	 */
	int saveImage(char* dir, char* function);
	
	//! Checks if we have a pair of parallel lines
	/*!
		According to a number of tests, including size, angle and alignment	
		\param start1 Line 1 start point, of type CvPoint (Open CV). start->X <= end->x
		\param end1 Line 1 end point, of type CvPoint
		\param start2 Line 2 start point, of type CvPoint (Open CV). start->X <= end->x
		\param end2 Line 2 end point, of type CvPoint
		\param sizeTol Maximum ratio in size between the biggest to the smallest line (sizeTol>=1)
		\param angleTol Maximum difference between angles in degrees
		\param distanceTol Maximum difference of the closest points between the two lines
		\return 1 if lines are double
		\return 0 if not
		\sa getLineAngle(), distance(), parallel(), lineMatching()
	 */
	int checkDouble(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float sizeTol, float angleTol, float distanceTol);
	
	//! Calculates angle of line defined by two points 
	/*!
		The angle is given in the range -90 (excluding) to +90 (including)
		\param start Line start point, of type CvPoint (Open CV). start->X <= end->x
		\param end Line end point, of type CvPoint
		\return the angle in degrees
	 */
	float getLineAngle(CvPoint start, CvPoint end);
	
	//! Calculates the distance between two points 
	/*!
		\param start Line start point, of type CvPoint (Open CV). start->X <= end->x
		\param end Line end point, of type CvPoint
		\return the distance in pixels
	 */	
	float distance(CvPoint start, CvPoint end);

	//! Calculates the distance between two points 
	/*!
		\param startx Line start point coordinate x
		\param starty Line start point coordinate y
		\param endx Line end point coordinate x
		\param endy Line end point coordinate y
		\return the distance in pixels
	 */		
	float distance(float startx,float starty, float endx, float endy);

	//! Checks if lines are parallel
	/*!
		\param start1 Line 1 start point, of type CvPoint (Open CV). start->X <= end->x
		\param end1 Line 1 end point, of type CvPoint
		\param start2 Line 2 start point, of type CvPoint (Open CV). start->X <= end->x
		\param end2 Line 2 end point, of type CvPoint
 		\param angleTol Maximum angle difference in degrees 
		\return 1 if lines are parallel
 		\return 0 if lines are not parallel
	 */
	int parallel(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float angleTol);

	//! Checks if lines converge and are at close distance
	/*!
		\param start1 Line 1 start point, of type CvPoint (Open CV). start->X <= end->x
		\param end1 Line 1 end point, of type CvPoint
		\param start2 Line 2 start point, of type CvPoint (Open CV). start->X <= end->x
		\param end2 Line 2 end point, of type CvPoint
		\param distanceTol Maximum difference between the closest extreme points of the two lines
		\return 1 if lines match
		\return 0 if lines dont match
	 */
	int lineMatching(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float distanceTol);
	
	//! PRIVATE VARIABLES

	//! Stereosource object to store and handle grabed stereo images from logged files or camera
	stereoSource stereoImageSource;

	//! Stereoprocess object to store and transform stereosource stereo images
	stereoProcess stereoImageProcess;

	//! Mutex for the access to the cameras
	pthread_mutex_t cameraMutex;
	
	//! Opencv left image object.
	IplImage* cvLeftImage;
	//! Opencv right image object.
	IplImage* cvRightImage;
	//! Store and pass processed images between roadFinding Stages
	IplImage* cvProcessImage;
	//! Multipurpose image for storing semi-elaborate images for non-Stage operations and saving
	IplImage* cvBufferImage; 	
		
	//! RoadFinding state, holds the function we're in for saving and debugging purposes only
	char* iFunction;

	//! STAGES ALGORITHMS CALIBRATION PARAMETERS
	
	//! edgeFinding
	int EFlineLink;
	int EFmainLine;

	//! laneMarker
	int LMacc;
	int LMminLineLength;
	int LMmaxGap;
	int LMdistanceResolution;
	int LMangleResolution;
 	float LMsizeTol;
 	float LMangleTol;
 	float LMdistanceTol;

};

#endif  //__ROADFINDING_HH__
