#include <fstream>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctime>

#include "DGCutils"
#include "roadFinding.hh"

bool waitForState = false;

void printHelp();

//! The main file. Creates a roadFinding class object and sets it to work.

int main(int argc, char *argv[]) {
	//! The main function. Sets and activates the roadFinding object.
	
	//0 for endless loop
	int maxFrames=10; 
	int realFrames=0;
	
	//! Creates the object "process" that belongs to the roadFinding Class.
	roadFinding process(14785236);

	/*! Handles arguments. 
		-files is needed for working with logged files.
		-cameras is needed for working with live camera images.
	*/
	if (argc == 1)	{
		cout << "Wrong number of arguments. Type './roadFindingCalibration -help' for help\n" << endl;
		exit(0);
	}
	if (argc == 2 ) {
	  string option=argv[1];
		if (option=="-files") {
			process._source=USE_FILES;
			cout << "Working from files\n" << endl;
		} 
		if (option=="-cameras") {
			process._source=USE_CAMERAS;
			cout << "Working from cameras\n" << endl;
		}
		if (option=="-help") {
			printHelp();
			exit(0);
		}
	}
	//! If optional 3rd parameter is specified, it tells the number of frames to capture
	if (argc == 3)	{
		maxFrames = atoi(argv[2]);
		if (maxFrames > 0) {
			cout << "Capturing " << maxFrames << " frames." <<endl; 
		} else {
			cout << "Endless loop. Press Q/q to quit." <<endl;
		}
		
	}
	
	//! Time variables for operation length measuring.
	long int startTime = clock();	
	double timeTaken;
		
	//! Initialize the "process": loads algorithms calibration
	process.loadCalibration();
	//! Initialize the "process": initializes stereo pair
	process.initStereo();
	
	//! Sets logging and UI options. Warning: _debug=1 is much slower (tens of times)
	process._debug = 0;
	process._autoSaveImages = 1;
	
	//! Activate the loop for frame capturing and processing
	//! Allows only a maximum frame skip of 100 frames.
	realFrames = process.loopStereo(maxFrames,100);
	
	cout << endl;
	//! Handles errors.
	if (realFrames == -1) {
		cout << "No frames were captured. Some big problem occured"<<endl;
		exit(1);
	} 

	//! Calculate time taken for the whole process.
	timeTaken = (double)(clock() - startTime)/((double)CLOCKS_PER_SEC);	
	
	//! Handles errors and prints statistics for this run.
	if (timeTaken == 0.0 || realFrames == 0) {
		cout << "No frames processed or processing time is null." << endl << "Something is definitely wrong" << endl;
	} else {
		cout << "Actual frames processed: " << realFrames << " frames" << endl;
		cout << "Average frame processing time: " << timeTaken / realFrames << " sec" << endl;
		cout << "Frame rate: " << realFrames /timeTaken << " fps" << endl << endl;
	}
		
	return 0;
}

void printHelp() {
	//! prints help.
   printf( "\n Usage: ./roadFinding -source [-maxFrames]\n"
       "\t -source:\n"
       "\t\t '-files' for images in 'images' folder\n" 
       "\t\t '-cameras' for online images from short pair cameras\n" 
       "\t [-maxFrames]:\n"
       "\t\t n, capture n frames\n" 
       "\t\t 0, endless loop\n" 
       );       
}
