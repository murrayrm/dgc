/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#include <getopt.h>
#include <iostream>
#include "MissionPlanner.hh"
#include "DGCutils"
#include"sparrowhawk.hh"

enum {
  OPT_NONE,
  OPT_HELP,
  OPT_RNDF,
  OPT_MDF,
  NUM_OPTS
};

using namespace std;

// Default options
int NOWAIT = 0;                        // wait for state data to fill
int NOMAP = 0;                         // use gloNavMapLib
int NOSPARROW = 0;                     // use sparrow
char* RNDFFileName = "DARPA_RNDF.txt"; // name of RNDF file
char* MDFFileName = "DARPA_MDF.txt";   // name of MDF file

/* The name of this program. */
const char * program_name;

/* Prints usage information for this program to STREAM (typically stdout 
   or stderr), and exit the program with EXIT_CODE. Does not return. */
void print_usage (FILE* stream, int exit_code)
{
  fprintf( stream, "Usage:  %s [options]\n", program_name );
  fprintf( stream, "  --nowait \t\tDo not wait for state to fill, plan from vehicle state.\n"); 
  fprintf( stream, "  --nomap \t\tDo not use gloNavMapLib.\n"); 
  fprintf( stream, "  --nosparrow, --nosp \t\tDisable sparrow.\n");   
  fprintf( stream, "  --rndf RNDFFileName \tSpecifies the filename RNDFFileName for RNDF. Do not listen to gloNavMapLib\n");    
  fprintf( stream, "  --mdf MDFFileName \tSpecifies the filename MDFFileName for MDF.\n");
  exit(exit_code);
}

int main(int argc, char **argv) 
{
  int ch;
  int option_index = 0;
  bool usingGloNavMapLib = true;
  /* An array describing valid long options. */
  static struct option long_options[] = 
  {
    // first: long option (--option) string
    // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
    // third: if pointer, set variable to value of fourth argument
    //        if NULL, getopt_long returns fourth argument
    {"nowait",     no_argument,       &NOWAIT,        1},
    {"nomap",      no_argument,       &NOMAP,         1},
    {"nosparrow",  no_argument,       &NOSPARROW,     1},
    {"nosp",       no_argument,       &NOSPARROW,     1},
    {"help",       no_argument,       0,           OPT_HELP},
    {"rndf",       required_argument, 0,           OPT_RNDF},
    {"mdf",        required_argument, 0,           OPT_MDF},
    {0,0,0,0}
  };

  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  program_name = argv[0];
  printf("\n");

  // Loop through and process all of the command-line input options.
  while((ch = getopt_long_only(argc, argv, "", long_options, &option_index)) != -1)
  {
    switch(ch)
    {
      case OPT_HELP:
        /* User has requested usage information. Print it to standard
           output, and exit with exit code zero (normal 
           termination). */
        print_usage(stdout, 0);
		break;

      case '?': /* The user specified an invalid option. */
        /* Print usage information to standard error, and exit with exit
           code one (indicating abnormal termination). */
        print_usage(stderr, 1);
		break;

      case OPT_RNDF:
		usingGloNavMapLib = false;
		RNDFFileName = optarg;
		break;

      case OPT_MDF:
		MDFFileName = optarg;
		break;

      case -1: /* Done with options. */
        break;

    }
  }
  
  if (NOMAP == 1)
  	usingGloNavMapLib = false;

  int sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if( pSkynetkey == NULL )
  {
    cerr << "SKYNET_KEY environment variable isn't set" << endl;
  }
  else
    sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;
  if (!usingGloNavMapLib)
  	cerr << "RNDF file: " << RNDFFileName << endl;
  cerr << "MDF file: " << MDFFileName << endl;

  CMissionPlanner* pMissionPlanner = new CMissionPlanner(sn_key, NOWAIT == 0, NOSPARROW == 1, RNDFFileName, MDFFileName, usingGloNavMapLib);
  

  DGCstartMemberFunctionThread(pMissionPlanner, &CMissionPlanner::getTPlannerStatusThread);
  DGCstartMemberFunctionThread(pMissionPlanner, &CMissionPlanner::sendSegGoalsThread);
  if (NOSPARROW == 0)
  {
    DGCstartMemberFunctionThread(pMissionPlanner, &CMissionPlanner::SparrowDisplayLoop);
    DGCstartMemberFunctionThread(pMissionPlanner, &CMissionPlanner::UpdateSparrowVariablesLoop);
  }
  pMissionPlanner->MPlanningLoop();

  return 0;
}
