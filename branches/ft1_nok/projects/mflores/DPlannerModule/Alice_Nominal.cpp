/*
 *  Alice_Nominal.cpp
 *  DPlannerModule
 *
 *  Created by Melvin E. Flores on 11/21/06.
 *  Control and Dynamical Systems
 *	California Institute of Technology
 *
 *  Copyright 2006. All rights reserved.
 */

#include <float.h>
#include <stdlib.h>
#include <math.h>

#include "Alice_Nominal.h" 

CAlice_Nominal::CAlice_Nominal(void)
{
	m_i = 0;
	m_j = 0;
	
	m_NofParameters = 11;
	m_Parameters = new double[m_NofParameters];
	
	m_Parameters[0]  =   5.43560;
	m_Parameters[1]  =   2.13360;
	m_Parameters[2]  =   1.06680;
	m_Parameters[3]  =  26.82240;
	m_Parameters[4]  =  -3.00000;
	m_Parameters[5]  =   1.00000;
	m_Parameters[6]  =  -0.44942;
	m_Parameters[7]  =   0.44942;
	m_Parameters[8]  =  -1.30900;
	m_Parameters[9]  =   1.30900;
	m_Parameters[10] =   9.81000;
}

CAlice_Nominal::~CAlice_Nominal(void)
{
	delete[] m_Parameters;
}

void CAlice_Nominal::GetTimeInterval(TimeInterval* const timeInterval)
{
	timeInterval->t0 = 0;
	timeInterval->tf = 1;
}

void CAlice_Nominal::GetOCPSizes(OCPSizes* const ocpSizes)
{
	ocpSizes->NofFlatOutputs            = 3;
	ocpSizes->HaveICostFunction         = false;
	ocpSizes->HaveTCostFunction         = true;
	ocpSizes->HaveFCostFunction         = true;
	ocpSizes->NofILinConstraints        = 2;
	ocpSizes->NofTLinConstraints        = 3;
	ocpSizes->NofFLinConstraints        = 2;
	ocpSizes->NofINLinConstraints       = 4;
	ocpSizes->NofTNLinConstraints       = 9;
	ocpSizes->NofFNLinConstraints       = 4;
	ocpSizes->NofParameters             = m_NofParameters;
	ocpSizes->NofReferences             = 0;
	ocpSizes->IntegrationType           = qt_Simpsons38;
	ocpSizes->NofCostCollocPoints       = 21;
	ocpSizes->NofConstraintCollocPoints = 31;
	ocpSizes->HaveActiveVariables       = false;
}

void CAlice_Nominal::GetNURBSData(const NURBSDataType* const dataType, const int* const NofFlatOutputs, int* const Data)
{
	if(*dataType == dt_NofDimensionsOfFlatOutputs)
	{
		Data[0] = 1;
		Data[1] = 1;
		Data[2] = 1;
	}
	else if(*dataType == dt_SolveForControlPoints)
	{
		Data[0] = 1;
		Data[1] = 1;
		Data[2] = 1;
	}
	else if(*dataType == dt_SolveForWeights)
	{
		Data[0] = 0;
		Data[1] = 0;
		Data[2] = 0;
	}
	else if(*dataType == dt_NofPolynomials)
	{
		Data[0] = 2;
		Data[1] = 2;
		Data[2] = 1;
	}
	else if(*dataType == dt_DegreeOfPolynomials)
	{
		Data[0] = 5;
		Data[1] = 5;
		Data[2] = 0;
	}
	else if(*dataType == dt_CurveSmoothness)
	{
		Data[0] = 3;
		Data[1] = 3;
		Data[2] = -1;
	}
	else //if (*dataType == dt_MaxDerivatives)
	{
		Data[0] = 3;
		Data[1] = 3;
		Data[2] = 0;
	}
}

void CAlice_Nominal::GetBreakPoints(const TimeInterval* const timeInterval, const int* const NofFlatOutputs, const int* const NofBreakPoints, double** const BreakPoints)
{
	for(m_i=0; m_i<*NofFlatOutputs; m_i++)
	{
		CreateTimeVector(timeInterval, &NofBreakPoints[m_i], BreakPoints[m_i]);
	}
}

void CAlice_Nominal::GetCostCollocPoints(const TimeInterval* const timeInterval, const int* const NofCostCollocPoints, double* const CostCollocPoints)
{
	CreateTimeVector(timeInterval, NofCostCollocPoints, CostCollocPoints);
}

void CAlice_Nominal::GetConstraintCollocPoints(const TimeInterval* const timeInterval, const int* const NofConstraintCollocPoints, double* const ConstraintCollocPoints)
{
	CreateTimeVector(timeInterval, NofConstraintCollocPoints, ConstraintCollocPoints);
}

void CAlice_Nominal::GetOCPParameters(const int* const i, const double* const t, const FunctionType* const functionType, const int* const NofParameters, double* const Parameters)
{
	p1  = m_Parameters[0];
	p2  = m_Parameters[1];
	p3  = m_Parameters[2];
	p4  = m_Parameters[3];
	p5  = m_Parameters[4];
	p6  = m_Parameters[5];
	p7  = m_Parameters[6];
	p8  = m_Parameters[7];
	p9  = m_Parameters[8];
	p10 = m_Parameters[9];
	p11 = m_Parameters[10];
}

void CAlice_Nominal::RecoverSystem(const double* const tau, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, double* const t, const int* const NofStates, double* const x, const int* const NofInputs, double* const u)
{

		*t = *tau*z3;

	{
		double t1 = z1d*z1d;
		double t2 = z2d*z2d;      
		double t4 = sqrt(t1+t2);      
		double t5 = atan2(z2d,z1d);      

		x[0] = z1;      
		x[1] = z2;      
		x[2] = t4;      
		x[3] = 0.1007958012753983E16/17592186044416.0*t5;      
	}

	{
		double t4 = z1d*z1d;
		double t5 = z2d*z2d;      
		double t6 = t4+t5;      
		double t7 = sqrt(t6);      
		double t10 = z3*z3;      
		double t18 = atan2((z1dd*z2d-z1d*z2dd)*p1,t7*t6);      

		u[0] = (z1d*z1dd+z2d*z2dd)/t7/t10;      
		u[1] = 0.1007958012753983E16/17592186044416.0*t18;      
	}

}

void CAlice_Nominal::TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt)
{

	if(*mode == 0 || *mode == 2)
	{
		double t3 = z1d*z1dd+z2d*z2dd;
		double t4 = t3*t3;      
		double t5 = z1d*z1d;      
		double t6 = z2d*z2d;      
		double t7 = t5+t6;      
		double t8 = 1/t7;      
		double t10 = z3*z3;      
		double t11 = t10*t10;      
		double t16 = z1dd*z2d-z1d*z2dd;      
		double t19 = sqrt(t7);      
		double t20 = atan2(t16*t8*p1,t19);      
		double t21 = t20*t20;      
		double t22 = p1*p1;      
		double t35 = pow(1/t19*(z1ddd*z2d-z1d*z2ddd-2.0*t3*t16*t8)-t3/t19/t7*t16,2.0);      
		double t37 = t7*t7;      

		*Ft  = t4*t8/t11+t21+t22*t35/t37/t10;      
	}

	if(*mode == 1 || *mode == 2)
	{

		double t3 = z1d*z1dd+z2d*z2dd;
		double t4 = z1d*z1d;      
		double t5 = z2d*z2d;      
		double t6 = t4+t5;      
		double t7 = 1/t6;      
		double t8 = t3*t7;      
		double t9 = z3*z3;      
		double t10 = t9*t9;      
		double t11 = 1/t10;      
		double t15 = t3*t3;      
		double t16 = t6*t6;      
		double t17 = 1/t16;      
		double t18 = t15*t17;      
		double t19 = t11*z1d;      
		double t24 = z1dd*z2d-z1d*z2dd;      
		double t27 = sqrt(t6);      
		double t28 = atan2(t24*t7*p1,t27);      
		double t30 = 1/t27/t6;      
		double t31 = z2dd*t30;      
		double t34 = 1/t27/t16;      
		double t35 = t24*t34;      
		double t41 = t24*t24;      
		double t43 = 1/t16/t6;      
		double t45 = p1*p1;      
		double t48 = 1/(1.0+t41*t43*t45);      
		double t51 = 1/t27;      
		double t54 = 2.0*t3*t24;      
		double t56 = z1ddd*z2d-z1d*z2ddd-t54*t7;      
		double t58 = t3*t30;      
		double t60 = t51*t56-t58*t24;      
		double t61 = t45*t60;      
		double t62 = 1/t9;      
		double t63 = t17*t62;      
		double t64 = t30*t56;      
		double t76 = z1dd*t30;      
		double t78 = t3*t34;      
		double t79 = t24*z1d;      
		double t87 = t60*t60;      
		double t88 = t45*t87;      
		double t89 = t43*t62;      
		double t97 = t30*p1*t48;      
		double t112 = t34*t62;      
		double t119 = t11*z2d;      
		double t142 = t24*z2d;      

		DFt[1] = 2.0*t8*t11*z1dd-2.0*t18*t19+2.0*t28*(-t31*p1-3.0*t35*p1*z1d)*t48+2.0*t61*t63*(-t64*z1d+t51*(-z2ddd-2.0*z1dd*t24*t7+2.0*t3*z2dd*t7+2.0*t54*t17*z1d)-t76*t24+3.0*t78*t79+t58*z2dd)-4.0*t88*t89*z1d;      
		DFt[2] = 2.0*t8*t19+2.0*t28*z2d*t97+2.0*t61*t63*(t51*(-2.0*t79*t7-2.0*t3*z2d*t7)-z1d*t30*t24-t58*z2d);      
		DFt[3] = 2.0*t61*t112*z2d;      
		DFt[5] = 2.0*t8*t11*z2dd-2.0*t18*t119+2.0*t28*(t76*p1-3.0*t35*p1*z2d)*t48+2.0*t61*t63*(-t64*z2d+t51*(z1ddd-2.0*z2dd*t24*t7-2.0*t3*z1dd*t7+2.0*t54*t17*z2d)-t31*t24+3.0*t78*t142-t58*z1dd)-4.0*t88*t89*z2d;      
		DFt[6] = 2.0*t8*t119-2.0*t28*z1d*t97+2.0*t61*t63*(t51*(-2.0*t142*t7+2.0*t3*z1d*t7)-z2d*t30*t24+t58*z1d);      
		DFt[7] = -2.0*t61*t112*z1d;      
		DFt[8] = -4.0*t15*t7/t10/z3-2.0*t88*t17/t9/z3;      
	}

}

void CAlice_Nominal::FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf)
{

	if(*mode == 0 || *mode == 2)
	{
		*Ff = z3;
	}

	if(*mode == 1 || *mode == 2)
	{

		DFf[8] = 1.0;      
	}

}

void CAlice_Nominal::GetLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A)
{
	if(*ctype == ct_LinInitial)
	{
		A[0][0]= 1.0;
		A[1][4]= 1.0;
	}
	
	else if(*ctype == ct_LinTrajectory)
	{
		A[0][0]= 1.0;
		A[1][4]= 1.0;
		A[2][8]= 1.0;
	}
	
	else /*if(*ctype == ct_LinFinal)*/
	{
		A[0][0]= 1.0;
		A[1][4]= 1.0;
	}
	
}
void CAlice_Nominal::InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli)
{

	if(*mode == 0 || *mode == 2)
	{
		double t1 = z1d*z1d;
		double t2 = z2d*z2d;      
		double t3 = t1+t2;      
		double t4 = sqrt(t3);      
		double t5 = atan2(z2d,z1d);      
		double t11 = z3*z3;      
		double t19 = atan2((z1dd*z2d-z1d*z2dd)*p1,t4*t3);      

		Cnli[0] = t4;      
		Cnli[1] = t5;      
		Cnli[2] = (z1d*z1dd+z2d*z2dd)/t4/t11;      
		Cnli[3] = t19;      
	}

	if(*mode == 1 || *mode == 2)
	{
		double t1 = z1d*z1d;
		double t2 = z2d*z2d;      
		double t3 = t1+t2;      
		double t4 = sqrt(t3);      
		double t5 = 1/t4;      
		double t6 = t5*z1d;      
		double t7 = t5*z2d;      
		double t8 = 1/t1;      
		double t12 = 1/(1.0+t2*t8);      
		double t17 = z3*z3;      
		double t18 = 1/t17;      
		double t22 = z1d*z1dd+z2d*z2dd;      
		double t24 = 1/t4/t3;      
		double t25 = t22*t24;      
		double t45 = z1dd*z2d-z1d*z2dd;      
		double t46 = t3*t3;      
		double t49 = t45/t4/t46;      
		double t54 = t45*t45;      
		double t58 = p1*p1;      
		double t61 = 1/(1.0+t54/t46/t3*t58);      
		double t64 = p1*t61;      

		DCnli[0][1] = t6;      
		DCnli[0][5] = t7;      
		DCnli[1][1] = -z2d*t8*t12;      
		DCnli[1][5] = 1/z1d*t12;      
		DCnli[2][1] = z1dd*t5*t18-t25*t18*z1d;      
		DCnli[2][2] = t6*t18;      
		DCnli[2][5] = z2dd*t5*t18-t25*t18*z2d;      
		DCnli[2][6] = t7*t18;      
		DCnli[2][8] = -2.0*t22*t5/t17/z3;      
		DCnli[3][1] = (-z2dd*t24*p1-3.0*t49*p1*z1d)*t61;      
		DCnli[3][2] = z2d*t24*t64;      
		DCnli[3][5] = (z1dd*t24*p1-3.0*t49*p1*z2d)*t61;      
		DCnli[3][6] = -z1d*t24*t64;      
	}

	if(*mode == 99)
	{
		 DCnli[0][1] = 1.0; 
		 DCnli[0][5] = 1.0; 
		 DCnli[1][1] = 1.0; 
		 DCnli[1][5] = 1.0; 
		 DCnli[2][1] = 1.0; 
		 DCnli[2][2] = 1.0; 
		 DCnli[2][5] = 1.0; 
		 DCnli[2][6] = 1.0; 
		 DCnli[2][8] = 1.0; 
		 DCnli[3][1] = 1.0; 
		 DCnli[3][2] = 1.0; 
		 DCnli[3][5] = 1.0; 
		 DCnli[3][6] = 1.0; 
	}

}

void CAlice_Nominal::TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt)
{

	if(*mode == 0 || *mode == 2)
	{
		double t1 = z1d*z1d;
		double t2 = z2d*z2d;      
		double t3 = t1+t2;      
		double t4 = sqrt(t3);      
		double t7 = 1/z3;      
		double t11 = z1d*z1dd+z2d*z2dd;      
		double t12 = 1/t4;      
		double t14 = z3*z3;      
		double t15 = 1/t14;      
		double t19 = z1dd*z2d-z1d*z2dd;      
		double t21 = t4*t3;      
		double t22 = atan2(t19*p1,t21);      
		double t26 = 1/t3;      
		double t40 = pow(z1-8.0,2.0);      
		double t42 = pow(z2-2.0,2.0);      
		double t45 = pow(z1-5.0,2.0);      
		double t47 = pow(z2-3.0,2.0);      
		double t50 = pow(z1-2.0,2.0);      
		double t52 = pow(z2-6.0,2.0);      
		double t55 = pow(z1-7.0,2.0);      
		double t57 = pow(z2-5.0,2.0);      

		Cnlt[0] = t4/p4*t7;      
		Cnlt[1] = t11*t12*t15;      
		Cnlt[2] = t22;      
		Cnlt[3] = p1*(t12*(z1ddd*z2d-z1d*z2ddd-2.0*t11*t19*t26)-t11/t21*t19)*t26*t7;      
		Cnlt[4] = t12*t19*t15;      
		Cnlt[5] = t40+t42;      
		Cnlt[6] = t45+t47;      
		Cnlt[7] = t50+t52;      
		Cnlt[8] = t55+t57;      
	}

	if(*mode == 1 || *mode == 2)
	{
		double t1 = z1d*z1d;
		double t2 = z2d*z2d;      
		double t3 = t1+t2;      
		double t4 = sqrt(t3);      
		double t5 = 1/t4;      
		double t6 = 1/p4;      
		double t7 = t5*t6;      
		double t8 = 1/z3;      
		double t9 = t8*z1d;      
		double t11 = t8*z2d;      
		double t14 = z3*z3;      
		double t15 = 1/t14;      
		double t18 = z1dd*t5*t15;      
		double t21 = z1d*z1dd+z2d*z2dd;      
		double t23 = 1/t4/t3;      
		double t24 = t21*t23;      
		double t25 = t15*z1d;      
		double t29 = z1d*t5*t15;      
		double t31 = z2dd*t5*t15;      
		double t32 = t15*z2d;      
		double t36 = z2d*t5*t15;      
		double t39 = 1/t14/z3;      
		double t42 = z2dd*t23;      
		double t46 = z1dd*z2d-z1d*z2dd;      
		double t47 = t3*t3;      
		double t49 = 1/t4/t47;      
		double t50 = t46*t49;      
		double t55 = t46*t46;      
		double t59 = p1*p1;      
		double t62 = 1/(1.0+t55/t47/t3*t59);      
		double t64 = z2d*t23;      
		double t65 = p1*t62;      
		double t67 = z1dd*t23;      
		double t74 = z1d*t23;      
		double t78 = 2.0*t21*t46;      
		double t79 = 1/t3;      
		double t81 = z1ddd*z2d-z1d*z2ddd-t78*t79;      
		double t82 = t23*t81;      
		double t89 = 1/t47;      
		double t96 = t21*t49;      
		double t97 = t46*z1d;      
		double t103 = t79*t8;      
		double t108 = p1*(t5*t81-t24*t46);      
		double t109 = t89*t8;      
		double t125 = p1*t23;      
		double t139 = t46*z2d;      
		double t164 = t23*t46;      
		double t172 = 2.0*z1;      
		double t174 = 2.0*z2;      

		DCnlt[0][1] = t7*t9;      
		DCnlt[0][5] = t7*t11;      
		DCnlt[0][8] = -t4*t6*t15;      
		DCnlt[1][1] = t18-t24*t25;      
		DCnlt[1][2] = t29;      
		DCnlt[1][5] = t31-t24*t32;      
		DCnlt[1][6] = t36;      
		DCnlt[1][8] = -2.0*t21*t5*t39;      
		DCnlt[2][1] = (-t42*p1-3.0*t50*p1*z1d)*t62;      
		DCnlt[2][2] = t64*t65;      
		DCnlt[2][5] = (t67*p1-3.0*t50*p1*z2d)*t62;      
		DCnlt[2][6] = -t74*t65;      
		DCnlt[3][1] = p1*(-t82*z1d+t5*(-z2ddd-2.0*z1dd*t46*t79+2.0*t21*z2dd*t79+2.0*t78*t89*z1d)-t67*t46+3.0*t96*t97+t24*z2dd)*t103-2.0*t108*t109*z1d;      
		DCnlt[3][2] = p1*(t5*(-2.0*t97*t79-2.0*t21*z2d*t79)-t74*t46-t24*z2d)*t103;      
		DCnlt[3][3] = t125*t11;      
		DCnlt[3][5] = p1*(-t82*z2d+t5*(z1ddd-2.0*z2dd*t46*t79-2.0*t21*z1dd*t79+2.0*t78*t89*z2d)-t42*t46+3.0*t96*t139-t24*z1dd)*t103-2.0*t108*t109*z2d;      
		DCnlt[3][6] = p1*(t5*(-2.0*t139*t79+2.0*t21*z1d*t79)-t64*t46+t24*z1d)*t103;      
		DCnlt[3][7] = -t125*t9;      
		DCnlt[3][8] = -t108*t79*t15;      
		DCnlt[4][1] = -t164*t25-t31;      
		DCnlt[4][2] = t36;      
		DCnlt[4][5] = -t164*t32+t18;      
		DCnlt[4][6] = -t29;      
		DCnlt[4][8] = -2.0*t5*t46*t39;      
		DCnlt[5][0] = t172-16.0;      
		DCnlt[5][4] = t174-4.0;      
		DCnlt[6][0] = t172-10.0;      
		DCnlt[6][4] = t174-6.0;      
		DCnlt[7][0] = t172-4.0;      
		DCnlt[7][4] = t174-12.0;      
		DCnlt[8][0] = t172-14.0;      
		DCnlt[8][4] = t174-10.0;      
	}

	if(*mode == 99)
	{
		 DCnlt[0][1] = 1.0; 
		 DCnlt[0][5] = 1.0; 
		 DCnlt[0][8] = 1.0; 
		 DCnlt[1][1] = 1.0; 
		 DCnlt[1][2] = 1.0; 
		 DCnlt[1][5] = 1.0; 
		 DCnlt[1][6] = 1.0; 
		 DCnlt[1][8] = 1.0; 
		 DCnlt[2][1] = 1.0; 
		 DCnlt[2][2] = 1.0; 
		 DCnlt[2][5] = 1.0; 
		 DCnlt[2][6] = 1.0; 
		 DCnlt[3][1] = 1.0; 
		 DCnlt[3][2] = 1.0; 
		 DCnlt[3][3] = 1.0; 
		 DCnlt[3][5] = 1.0; 
		 DCnlt[3][6] = 1.0; 
		 DCnlt[3][7] = 1.0; 
		 DCnlt[3][8] = 1.0; 
		 DCnlt[4][1] = 1.0; 
		 DCnlt[4][2] = 1.0; 
		 DCnlt[4][5] = 1.0; 
		 DCnlt[4][6] = 1.0; 
		 DCnlt[4][8] = 1.0; 
		 DCnlt[5][0] = 1.0; 
		 DCnlt[5][4] = 1.0; 
		 DCnlt[6][0] = 1.0; 
		 DCnlt[6][4] = 1.0; 
		 DCnlt[7][0] = 1.0; 
		 DCnlt[7][4] = 1.0; 
		 DCnlt[8][0] = 1.0; 
		 DCnlt[8][4] = 1.0; 
	}

}

void CAlice_Nominal::FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf)
{

	if(*mode == 0 || *mode == 2)
	{
		double t1 = z1d*z1d;
		double t2 = z2d*z2d;      
		double t3 = t1+t2;      
		double t4 = sqrt(t3);      
		double t5 = atan2(z2d,z1d);      
		double t11 = z3*z3;      
		double t19 = atan2((z1dd*z2d-z1d*z2dd)*p1,t4*t3);      

		Cnlf[0] = t4;      
		Cnlf[1] = t5;      
		Cnlf[2] = (z1d*z1dd+z2d*z2dd)/t4/t11;      
		Cnlf[3] = t19;      
	}

	if(*mode == 1 || *mode == 2)
	{
		double t1 = z1d*z1d;
		double t2 = z2d*z2d;      
		double t3 = t1+t2;      
		double t4 = sqrt(t3);      
		double t5 = 1/t4;      
		double t6 = t5*z1d;      
		double t7 = t5*z2d;      
		double t8 = 1/t1;      
		double t12 = 1/(1.0+t2*t8);      
		double t17 = z3*z3;      
		double t18 = 1/t17;      
		double t22 = z1d*z1dd+z2d*z2dd;      
		double t24 = 1/t4/t3;      
		double t25 = t22*t24;      
		double t45 = z1dd*z2d-z1d*z2dd;      
		double t46 = t3*t3;      
		double t49 = t45/t4/t46;      
		double t54 = t45*t45;      
		double t58 = p1*p1;      
		double t61 = 1/(1.0+t54/t46/t3*t58);      
		double t64 = p1*t61;      

		DCnlf[0][1] = t6;      
		DCnlf[0][5] = t7;      
		DCnlf[1][1] = -z2d*t8*t12;      
		DCnlf[1][5] = 1/z1d*t12;      
		DCnlf[2][1] = z1dd*t5*t18-t25*t18*z1d;      
		DCnlf[2][2] = t6*t18;      
		DCnlf[2][5] = z2dd*t5*t18-t25*t18*z2d;      
		DCnlf[2][6] = t7*t18;      
		DCnlf[2][8] = -2.0*t22*t5/t17/z3;      
		DCnlf[3][1] = (-z2dd*t24*p1-3.0*t49*p1*z1d)*t61;      
		DCnlf[3][2] = z2d*t24*t64;      
		DCnlf[3][5] = (z1dd*t24*p1-3.0*t49*p1*z2d)*t61;      
		DCnlf[3][6] = -z1d*t24*t64;      
	}

	if(*mode == 99)
	{
		 DCnlf[0][1] = 1.0; 
		 DCnlf[0][5] = 1.0; 
		 DCnlf[1][1] = 1.0; 
		 DCnlf[1][5] = 1.0; 
		 DCnlf[2][1] = 1.0; 
		 DCnlf[2][2] = 1.0; 
		 DCnlf[2][5] = 1.0; 
		 DCnlf[2][6] = 1.0; 
		 DCnlf[2][8] = 1.0; 
		 DCnlf[3][1] = 1.0; 
		 DCnlf[3][2] = 1.0; 
		 DCnlf[3][5] = 1.0; 
		 DCnlf[3][6] = 1.0; 
	}

}

void CAlice_Nominal::GetLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const NofConstraints, double* const LUBounds)
{

	if(*ctype == ct_LinInitial)
	{
		if(*ubtype == ubt_LowerBound)
		{
			LUBounds[0] =0;
			LUBounds[1] =0;
		}
		else
		{
			LUBounds[0] =0;
			LUBounds[1] =0;
		}
	}

	if(*ctype == ct_LinTrajectory)
	{
		if(*ubtype == ubt_LowerBound)
		{
			LUBounds[0] =0;
			LUBounds[1] =0;
			LUBounds[2] =0.01;
		}
		else
		{
			LUBounds[0] =9;
			LUBounds[1] =6;
			LUBounds[2] =35;
		}
	}

	if(*ctype == ct_LinFinal)
	{
		if(*ubtype == ubt_LowerBound)
		{
			LUBounds[0] =9;
			LUBounds[1] =6;
		}
		else
		{
			LUBounds[0] =9;
			LUBounds[1] =6;
		}
	}

	if(*ctype == ct_NLinInitial)
	{
		if(*ubtype == ubt_LowerBound)
		{
			LUBounds[0] =0.1;
			LUBounds[1] =0.0017453;
			LUBounds[2] =0;
			LUBounds[3] =0.0017453;
		}
		else
		{
			LUBounds[0] =0.1;
			LUBounds[1] =0.0017453;
			LUBounds[2] =0;
			LUBounds[3] =0.0017453;
		}
	}

	if(*ctype == ct_NLinTrajectory)
	{
		if(*ubtype == ubt_LowerBound)
		{
			LUBounds[0] =0;
			LUBounds[1] =-3;
			LUBounds[2] =-0.44942;
			LUBounds[3] =-1.309;
			LUBounds[4] =-9.81;
			LUBounds[5] =0.25;
			LUBounds[6] =0.25;
			LUBounds[7] =0.25;
			LUBounds[8] =0.25;
		}
		else
		{
			LUBounds[0] =1;
			LUBounds[1] =1;
			LUBounds[2] =0.44942;
			LUBounds[3] =1.309;
			LUBounds[4] =9.81;
			LUBounds[5] =DBL_MAX;
			LUBounds[6] =DBL_MAX;
			LUBounds[7] =DBL_MAX;
			LUBounds[8] =DBL_MAX;
		}
	}

	if(*ctype == ct_NLinFinal)
	{
		if(*ubtype == ubt_LowerBound)
		{
			LUBounds[0] =0.1;
			LUBounds[1] =0.0017453;
			LUBounds[2] =0;
			LUBounds[3] =0.0017453;
		}
		else
		{
			LUBounds[0] =0.1;
			LUBounds[1] =0.0017453;
			LUBounds[2] =0;
			LUBounds[3] =0.0017453;
		}
	}

}

