#include "LocalMapTalker.hh"

using namespace std;

CLocalMapTalker::CLocalMapTalker() {
  m_pLocalMapDataBuffer = new char[sizeof(Map)];
  DGCcreateMutex(&m_localMapDataBufferMutex);
}

CLocalMapTalker::~CLocalMapTalker() {
  delete [] m_pLocalMapDataBuffer;  
  DGCdeleteMutex(&m_localMapDataBufferMutex);
}

bool CLocalMapTalker::RecvLocalMap(int localMapSocket, Map* localMap, int* pSize) {
  int bytesToReceive;
  char* pBuffer = m_pLocalMapDataBuffer;

  // Build the mutex list. We want to protect the data buffer.
  int numMutices = 1;
  pthread_mutex_t* ppMutices[numMutices];
  ppMutices[0] = &m_localMapDataBufferMutex;

  // Get the localMap data from skynet. We want to receive the whole Map object, locking
  // the requesting the mutices to be locked and NOT unlocked at the end. This
  // is done because this data is still accessed afterwards and we manually
  // unlock the mutices later.
  bytesToReceive = sizeof(Map);
  *pSize = m_skynet.get_msg(localMapSocket, m_pLocalMapDataBuffer, bytesToReceive, 0, ppMutices, false, numMutices);
  if(*pSize == bytesToReceive)
  {
    memcpy(localMap, pBuffer, sizeof(Map));
    DGCunlockMutex(&m_localMapDataBufferMutex);
    return true;
  }

  DGCunlockMutex(&m_localMapDataBufferMutex);  
  return false;
}


bool CLocalMapTalker::SendLocalMap(int localMapSocket, Map* localMap) {
  int bytesToSend;
  int bytesSent;
  char* pBuffer = m_pLocalMapDataBuffer;
  
  DGClockMutex(&m_localMapDataBufferMutex);  
  memcpy(pBuffer, (char*)localMap, sizeof(Map));

  bytesToSend = sizeof(RNDF);

  bytesSent = m_skynet.send_msg(localMapSocket, m_pLocalMapDataBuffer, bytesToSend, 0);
  
  DGCunlockMutex(&m_localMapDataBufferMutex);  
  
  if(bytesSent != bytesToSend)
  {
    cerr << "CLocalMapTalker::SendLocalMap(): sent " << bytesSent << " bytes while expected to send " << bytesToSend << " bytes" << endl;
    return false;
  }

  return true;
}
