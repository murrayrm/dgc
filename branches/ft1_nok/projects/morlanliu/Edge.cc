#include "Edge.hh"
using namespace std;

Edge::Edge(Vertex* next)
{
  this->next = next;
  this->weight = 0;
}

Edge::~Edge()
{
}

double Edge::getWeight()
{
  return weight;
}

Vertex* Edge::getNext()
{
  return next;
}

void Edge::setWeight(double weight)
{
  this->weight = weight;
}

void Edge::setNext(Vertex* next)
{
  this->next = next;
}
