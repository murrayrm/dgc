#include "Vertex.hh"
using namespace std;

Vertex::Vertex(int segmentID, int laneID, int waypointID)
{
  this->segmentID = segmentID;
  this->laneID = laneID;
  this->waypointID = waypointID;
}

Vertex::~Vertex()
{
  for(unsigned i = 0; i < edges.size(); i++)
    delete edges[i];
}

int Vertex::getSegmentID()
{
  return segmentID;
}

int Vertex::getLaneID()
{
  return laneID;
}

int Vertex::getWaypointID()
{
  return waypointID;
}

vector<Edge*>* Vertex::getEdges()
{
  return &edges;
}

void Vertex::addEdge(Edge* edge)
{
  edges.push_back(edge);
}

void Vertex::print()
{
  cout << segmentID << "." << laneID << "." << waypointID << endl;
}
