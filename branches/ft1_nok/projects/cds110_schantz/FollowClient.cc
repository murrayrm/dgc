/*
 * FollowClient.cc - skynet module for follow
 *
 * RMM, 10 Dec 05
 *
 */

#include <math.h>
#include "FollowClient.hh"

extern int PROJ_ERR;		// use projected error?
extern int trajpoint;		// current trajectory point
int project_error(TRAJ_DATA *, double *, double *);

FollowClient::FollowClient(int sn_key) 
	: CSkynetContainer(MODfollow, sn_key)
{
  cerr << "FollowClient initalized on key " << sn_key << "\n";

	int Y_FRONT = 1;
	int A_HEADING_FRONT = 0;
	int A_HEADING_REAR = 0;

	//Start direct copying from TrajFollower constructor
  EYerrorside yerrorside = (Y_FRONT==1 ? YERR_FRONT : YERR_BACK);
  EAerrorside aerrorside = (A_HEADING_FRONT==1 ? AERR_FRONT : ( A_HEADING_REAR==1 ? AERR_BACK : AERR_YAW));
	//End direct copying from TrajFollower constructor
	m_speedController = new CPID_Controller(yerrorside, aerrorside);

// 	cerr << "Loading controller from file " << ctrlFilename << endl;

// 	mat_t* pMat = Mat_Open(ctrlFilename, MAT_ACC_RDONLY);

	//MATFile* pMat = matOpen(ctrlFilename, "r");
	m_lateralController = NULL;

 	m_traj_falcon = NULL;

// 	m_traj_DGC = NULL;

 	m_observer = new CObserver();

// 	m_currentTrajPoint = 0;

	setupFiles();

	controlRate = 300.0;

	logFilename[0] = 0;	// reset log filename

	loggingEnabled = false;
	useAutoLogNaming = true;
	controlEnabled = disabled;
	obsvEnabled = disabled;

  threshold = .5;  //threshold when to reject sigmas if over this.
  laneSight = 10;  //Average sigmas over the 10 next points in each lane.

  // being optimistic here...
  previousLaneCondition = BOTHGOOD;
  currentLaneCondition = BOTHGOOD;

  oldsigmaL = 0;
  oldsigmaR = 0;
  sigmaL = 0;
  sigmaR = 0;

  behaviorFlag = BOTH;

  rightMaintainDistance = 0;
  leftMaintainDistance = 0;
  rightDefaultDistance = 1.75;
  leftDefaultDistance = -1.75;
  
}

/* 
 * Member function for reading state
 *
 */
// void FollowClient::ReadState()
// {
//   cerr << "Calling ReadState() function\n";

//   while (1) {
//     // Update the current state (via broadcast message)
//     UpdateState();
// 		UpdateActuatorState();

//     // Put the data in a simple array for use by the controller
//     inp[XPOS] = m_state.Northing - xorigin;
//     inp[YPOS] = m_state.Easting - yorigin;
//     inp[TPOS] = m_state.Yaw;

//     inp[XVEL] = m_state.Vel_N;
//     inp[YVEL] = m_state.Vel_E;
//     inp[TVEL] = m_state.YawRate;
// 		sleep(1);
//   }
// }

/*
 * Member function for reading trajectories
 *
 */
// void FollowClient::ReadTraj()
// {
//   cerr << "Calling ReadTraj() function\n";

//   while (1) {
//     sleep(1);
//   }
// }

/*
 * Member function for executing control loop
 *
 */
void FollowClient::ControlLoop()
{
//   cerr << "Calling ControlLoop() function\n";

	double accelCmd, phi, vRef = 10, accel_Norm, steer_Norm;

  drivecmd_t my_command;
  m_adriveMsgSocket = m_skynet.get_send_sock(SNdrivecmd);

	string logs_location = "";

	double* controllerOutput;

	double* estimatorOutput;

	unsigned long long timeNow, timeDiff;

	double numSecTotal;

	double tempTime;

	unsigned long long numMicroSecTotal;
	unsigned long long microSecStartProcessing;
	unsigned long long microSecStopProcessing;
	double trajVector[25];
  
  

  while (1) {
    DGCgettime(microSecStartProcessing);		

    // Update the current state (via broadcast message)
    UpdateState();
    UpdateActuatorState();

    // Figure out the current time
    // If paused, the current time stays unchanged
    if(controlEnabled == enabled) {
      DGCgettime(timeNow);
      timeDiff = timeNow-timeStart;
      currentTime = DGCtimetosec(timeDiff);
    } else if(controlEnabled == disabled) {
      currentTime = 0.0;
    }

    /*
     * Update the state estimate
     *
     * This code updates the current state estimate using either astate
     * or the internal estimator, depending on flaga settings.
     *
     */

    // Store the raw measurements
    meas[XMEASPOS] = m_state.GPS_Northing; // - xorigin;
    meas[YMEASPOS] = m_state.GPS_Easting; //- yorigin;
    meas[TVMEASPOS] = m_state.raw_YawRate;
    meas[3] = outState[V];	// Use inputs from last iteration
    meas[4] = outState[PHI];

    gamma = m_state.gamma;

    if(obsvEnabled == disabled) {
      // Put the data in a simple array for use by the controller
      inp[XPOS] = m_state.Northing - xorigin;
      inp[YPOS] = m_state.Easting - yorigin;
      inp[TPOS] = m_state.Yaw;
			
      inp[XVEL] = m_state.Vel_N;
      inp[YVEL] = m_state.Vel_E;
      inp[TVEL] = m_state.YawRate;
			
      inp[XACC] = m_state.Acc_N;
      inp[YACC] = m_state.Acc_E;
      inp[TACC] = m_state.YawAcc;

    } else {			
      estimatorOutput = m_observer->obs_compute(meas, gamma);

      inp[XPOS] = estimatorOutput[0] - xorigin;
      inp[YPOS] = estimatorOutput[1] - yorigin;
      inp[TPOS] = estimatorOutput[2];
			
      inp[XVEL] = estimatorOutput[3];
      inp[YVEL] = estimatorOutput[4];
      inp[TVEL] = estimatorOutput[5];
			
      inp[XACC] = 0.0;
      inp[YACC] = 0.0;
      inp[TACC] = 0.0;

      covar[XPOS] = m_observer->covar[0];
      covar[YPOS] = m_observer->covar[7];
      covar[TPOS] = m_observer->covar[14];

      covar[XVEL] = m_observer->covar[21];
      covar[YVEL] = m_observer->covar[28];
      covar[TVEL] = m_observer->covar[35];

      correct[XPOS] = mat_element_get(m_observer->cor, 0, 0);
      correct[YPOS] = mat_element_get(m_observer->cor, 1, 0);
      correct[TPOS] = mat_element_get(m_observer->cor, 2, 0);

      correct[XVEL] = mat_element_get(m_observer->cor, 3, 0);
      correct[YVEL] = mat_element_get(m_observer->cor, 4, 0);
      correct[TVEL] = mat_element_get(m_observer->cor, 5, 0);
    }

    /*
     * Get the current trajectory point
     *
     * Read the trajectory file and figure out where we are supposed
     * to be at.  Also use this to copmute the feedforward forces.
     *
     */

    // Compute error based on error projection flag
    int status = PROJ_ERR ? 
      project_error(m_traj_falcon, inp, trajVector) :
      traj_read(m_traj_falcon, trajVector, currentTime);

    // Check status and make sure everything is OK
    if (status == 2) {
      disableControl(0);
    } else {
      outFF[V] = trajVector[0];
      outFF[PHI] = trajVector[1];

      for(int i=0; i<NUMINP; i++) {
	inp[NUMINP+i] = trajVector[2+i];
      }
      // Reorigin the desired stuff
      inp[NUMINP+XPOS] -= xorigin;
      inp[NUMINP+YPOS] -= yorigin;
    }

    /*
     * Now run the controller
     *
     * The control action is split up into lateral control and speed
     * control.  The lateral controller is implemented in falcon, the
     * spead controller is pulled from trajFollower.
     *
     */
		//Perform velocity control
		if(outOverride[V])
			vRef = outRef[V];
		else
			vRef = outFF[V];
		m_speedController->getVelocityControl_NoErrorChecking(&m_state, &m_actuatorState, &accelCmd, &phi, vRef);

		outCtrl[V] = accelCmd;
// 		outFF[V] = ;
		outCmd[V] = outGain[V]*(outCtrl[V]);// + outFF[V]);
		outState[V] = m_state.Speed2();

		accel_Norm = fmax(-1.0, fmin(outCmd[V], 1.0));
		my_command.my_actuator = accel;
		if(controlEnabled == enabled) 
			my_command.number_arg = accel_Norm;
		else
			my_command.number_arg = -1.0;
		if(NO_COMMANDS == 0) 
			m_skynet.send_msg(m_adriveMsgSocket, &my_command, sizeof(my_command), 0);

		outState[PHI] = m_actuatorState.m_steerpos*VEHICLE_MAX_AVG_STEER;
		//Perform steering control
		if(controlEnabled == enabled) {
			controllerOutput = ss_compute(m_lateralController, inp);
			outCtrl[PHI] = controllerOutput[0];
			//			outFF[PHI] = 0;
			outCmd[PHI] = outGain[PHI]*(outCtrl[PHI] + outFF[PHI]);
			
			steer_Norm = outCmd[PHI]/VEHICLE_MAX_AVG_STEER;
			steer_Norm = fmax(fmin(steer_Norm, 1.0), -1.0);
		}

		my_command.my_actuator = steer;
		if(controlEnabled == enabled) 
			my_command.number_arg = steer_Norm;
		else
			my_command.number_arg = 0.0;
		if(NO_COMMANDS == 0) 
			m_skynet.send_msg(m_adriveMsgSocket, &my_command, sizeof(my_command), 0);

		//Calculate Errors
		for(int i=0; i<NUMINP; i++) {
			err[i] = inp[i] - inp[i+NUMINP];
		}

		if(loggingEnabled) writeLog();


		numSecTotal = 1.0/controlRate;
		numMicroSecTotal = (unsigned long long)(numSecTotal*1.0e6);


		/* 
		 * Go to sleep until the end of the requested period
		 *
		 */

		DGCgettime(microSecStopProcessing);



		if(numMicroSecTotal - 1000 > (microSecStopProcessing - microSecStartProcessing)) {
			unsigned long long sleepTime;
			DGCusleep(numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing) - 1000);
			DGCgettime(sleepTime);
			actualRate = 1.0e6/((double)(sleepTime - microSecStartProcessing));
// 			if(((sleepTime-microSecStopProcessing) > (numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing)))) {
// 				timeDiffFoo = ((sleepTime-microSecStopProcessing) - (numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing)));
// 			} else {
// 				timeDiffFoo = ((numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing)) - (sleepTime-microSecStopProcessing));
// 			}
// 			sprintf(statusMessage, "%llu, %llu, should sleep for: %llu, slept for: %llu, diff is %llu", 
// 							numMicroSecTotal, microSecStopProcessing-microSecStartProcessing, 
// 							numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing),
// 							sleepTime-microSecStopProcessing,
// 							timeDiffFoo);
// 							(sleepTime-microSecStopProcessing) - (numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing)));
		} else {
			actualRate = 1.0e6/((double)(microSecStopProcessing - microSecStartProcessing));
		}
			 
  }
}



void FollowClient::writeLog() {
	if(logFile.is_open()) {
		unsigned long long actualTime;
		DGCgettime(actualTime);
		logFile << actualTime 
						<< " " << currentTime 
						<< " " << controlEnabled 
						<< " " << outState[V] 
						<< " " << outState[PHI]
						<< " " << outCmd[V]
						<< " " << outCmd[PHI];
		logFile << " " << setprecision(10) << inp[XPOS]+xorigin
						<< " " << setprecision(10) << inp[YPOS]+yorigin;

		for(int i=2; i<NUMINP; i++) {
			logFile << " " << setprecision(10) << inp[i];
		}

		logFile << " " << setprecision(10) << inp[NUMINP+XPOS]+xorigin
						<< " " << setprecision(10) << inp[NUMINP+YPOS]+yorigin;

		for(int i=2+NUMINP; i<NUMINP*2; i++) {
			logFile << " " << setprecision(10) << inp[i];
		}

		logFile << " " << obsvEnabled
						<< " " << setprecision(10) << meas[XMEASPOS]
						<< " " << setprecision(10) << meas[YMEASPOS]
						<< " " << setprecision(10) << meas[TVMEASPOS]
						<< " " << setprecision(10) << gamma;

		logFile << " " << m_state.Northing
						<< " " << m_state.Easting
						<< " " << m_state.Yaw			
						<< " " << m_state.Vel_N
						<< " " << m_state.Vel_E
						<< " " << m_state.YawRate
						<< " " << m_state.Acc_N
						<< " " << m_state.Acc_E
						<< " " << m_state.YawAcc;

		for(int i=0; i<NUMINP; i++) {
			logFile << " " << covar[i];
		}

		logFile<<" "<<outFF[PHI];

		logFile << endl;
	} else {
		cout << "file ainb't open" << endl;
	}
}


bool FollowClient::toggleLogging() {
	if(loggingEnabled) {
		logFile.close();
		loggingEnabled = false;
	} else {
		loggingEnabled = true;
		if(useAutoLogNaming) {
			time_t* currentTime;
			time(currentTime);
			tm* tmstruct = localtime(currentTime);
			sprintf(logFilename, "%s%04d%02d%02d_%02d%02d%02d.log", ctrlFilename, tmstruct->tm_year + 1900, 
							tmstruct->tm_mon + 1, tmstruct->tm_mday, tmstruct->tm_hour, 
							tmstruct->tm_min, tmstruct->tm_sec); 
		}

		if(logFile.is_open())
			logFile.close();
		logFile.open(logFilename, ofstream::out | ofstream::app);
	}

	return loggingEnabled;
}


bool FollowClient::setControlStatus(FollowClient::ctrlStatus status) {
	bool returnVal = false;

// 	//Anytime we toggle control status, we should figure out what the closest point on the traj is
	

	switch(controlEnabled) {
	case enabled:
		switch(status) {
		case enabled:
			break;
		case paused:
			controlEnabled = status;
			DGCgettime(timePause);
			returnVal = true;
			break;
		default:
		case disabled:
			controlEnabled = status;
			returnVal = true;
			break;
		}
		break;
	case paused:
		switch(status) {
		case enabled:
			unsigned long long timeResume;
			DGCgettime(timeResume);
			timeStart = timeStart + timeResume - timePause;
			controlEnabled = status;
			returnVal = true;
			break;
		case paused:
			break;
		default:
		case disabled:
			controlEnabled = status;
			returnVal = true;
			break;
		}
		break;
	default:
	case disabled:
		switch(status) {
		case enabled:
			if((m_traj_falcon != NULL &&
				 m_lateralController != NULL) ||
				 false) {
				DGCgettime(timeStart);
				controlEnabled = status;
				traj_reset(m_traj_falcon);
				returnVal = true;
			} else {
				sprintf(statusMessage, "Could not resume control since file is missing!");
			}
			break;
		case paused:
			break;
		default:
		case disabled:
			break;
		}
		break;
	}

	return returnVal;
}


void FollowClient::setupFiles() {
	if(m_lateralController != NULL)
		ss_free(m_lateralController);

	if(m_traj_falcon != NULL)
		traj_free(m_traj_falcon);

// 	if(m_traj_DGC != NULL)
// 		delete m_traj_DGC;

	m_lateralController = ss_load(ctrlFilename);
	if(m_lateralController == NULL) {
		sprintf(statusMessage, "Error loading controller file '%s'", ctrlFilename);
	} else {
		sprintf(statusMessage, "Controller loaded!");
	}
		

// 	cerr << "Loading traj from file " << trajFilename << endl;
	m_traj_falcon = traj_load(trajFilename);
	if(m_traj_falcon == NULL) {
		sprintf(statusMessage, "%s, Error loading trajectory file '%s'", statusMessage, trajFilename);
	} else {
		sprintf(statusMessage, "%s, Trajectory loaded!", statusMessage);
// 		m_traj_DGC = new CTraj();
// 		double rowVal[25];
// 		double time;

// 		for(int i=0; i<traj_rowsm(_traj_falcon); i++) {
// 			traj_row(m_traj_falcon, i, &time, rowVal);
// 			m_traj_DGC->addPoint(rowVal[2], rowVal[5], rowVal[8],
// 													 rowVal[3], rowVal[6], rowVal[9]);
// 		}

// 		ofstream trajLog("temp.txt");
// 		if(trajLog.is_open()) {
// 			m_traj_DGC->print(trajLog);
// 		}
// 		trajLog.close();

	}
 

	if(!(m_observer->loadFile(obsvFilename))) {
		sprintf(statusMessage, "%s, Error loading observer file '%s'", statusMessage, obsvFilename);
	} else {
		sprintf(statusMessage, "%s, Observer loaded!", statusMessage);
	}

	

	
// 	cerr << "Traj loaded successfully!" << endl;

// 	cerr << "Controller loaded succesfully!" << endl;

	for(int i=0; i<NUMOUT; i++) {
		outGain[i] = 1.0;
		outCtrl[i] = 0.0;
		outFF[i] = 0.0;
		outCmd[i] = 0.0;
		outOverride[i] = 0;
		outRef[i] = 0.0;
		outState[i] = 0.0;
	}


}


bool FollowClient::toggleObsvStatus() {
	if(obsvEnabled == disabled) {
		double initial_conds[6];
		for(int i=0; i<6; i++) {
			initial_conds[i] = inp[i];
		}

		initial_conds[XPOS] += xorigin;
		initial_conds[YPOS] += yorigin;

		m_observer->set_initial_conditions(initial_conds);
		obsvEnabled = enabled;
	} else {
		obsvEnabled = disabled;
	}

	return true;
}


int FollowClient::closestTrajPoint() {
  static int     nearestIndex = 0; // save value to avoid going backwards
	double  nearestDistSq;
	double  currentDistSq;
	double rowVal[25];
	double time;
	
	// Compute the nearest point on the trajectory to where we currently are
	nearestDistSq = 1.0e20;
	for(int i=nearestIndex; i<traj_rows(m_traj_falcon); i++) {
			traj_row(m_traj_falcon, i, &time, rowVal);
			currentDistSq =
				pow(inp[XPOS]+xorigin - rowVal[2+XPOS],2.0) +
				pow(inp[YPOS]+yorigin   - rowVal[2+YPOS] ,2.0);
			
			if(currentDistSq < nearestDistSq) {
					nearestDistSq = currentDistSq;
					nearestIndex = i;
			}
	}

//If we're near the end, round up so we'll stop
// 	if(nearestIndex == traj_rows(m_traj_falcon)-1)
// 		nearestIndex++;

// 	cout << "CLOSEST IS " << nearestIndex << "/" << traj_rows(m_traj_falcon) << " at " << sqrt(nearestDistSq) << endl;
	
	return nearestIndex;
}

int FollowClient::closestLeftLanePoint() {
  static int     nearestLeftIndex = 0; // save value to avoid going backwards
	double  nearestDistSq;
	double  currentDistSq;
	double rowVal[25];
	double time;
	
	// Compute the nearest point on the trajectory to where we currently are
	nearestDistSq = 1.0e20;
	for(int i=nearestLeftIndex; i<traj_rows(m_traj_falcon); i++) {
			traj_row(m_traj_falcon, i, &time, rowVal);
			currentDistSq =
				pow(inp[XPOS]+xorigin - rowVal[2+LXPOS],2.0) +
				pow(inp[YPOS]+yorigin   - rowVal[2+LYPOS] ,2.0);
			
			if(currentDistSq < nearestDistSq) {
					nearestDistSq = currentDistSq;
					nearestLeftIndex = i;
			}
	}

//If we're near the end, round up so we'll stop
// 	if(nearestIndex == traj_rows(m_traj_falcon)-1)
// 		nearestIndex++;

// 	cout << "CLOSEST IS " << nearestIndex << "/" << traj_rows(m_traj_falcon) << " at " << sqrt(nearestDistSq) << endl;
	
	return nearestLeftIndex;
}

int FollowClient::closestRightLanePoint() {
  static int     nearestRightIndex = 0; // save value to avoid going backwards
	double  nearestDistSq;
	double  currentDistSq;
	double rowVal[25];
	double time;
	
	// Compute the nearest point on the trajectory to where we currently are
	nearestDistSq = 1.0e20;
	for(int i=nearestRightIndex; i<traj_rows(m_traj_falcon); i++) {
			traj_row(m_traj_falcon, i, &time, rowVal);
			currentDistSq =
				pow(inp[XPOS]+xorigin - rowVal[2+RXPOS],2.0) +
				pow(inp[YPOS]+yorigin   - rowVal[2+RYPOS] ,2.0);
			
			if(currentDistSq < nearestDistSq) {
					nearestDistSq = currentDistSq;
					nearestRightIndex = i;
			}
	}

//If we're near the end, round up so we'll stop
// 	if(nearestIndex == traj_rows(m_traj_falcon)-1)
// 		nearestIndex++;

// 	cout << "CLOSEST IS " << nearestIndex << "/" << traj_rows(m_traj_falcon) << " at " << sqrt(nearestDistSq) << endl;
	
	return nearestRightIndex;
}



/*
 * project_error - compute distance to trajectory
 *
 * This function computes the error between the current state and the nearest 
 * point on a desired trajectory.  It stores the resulting data such that
 * when subtracted from the current state, the resulting error is based
 * on the nearest point to the trajectory.
 *
 * Return status (set to match traj_read):
 *
 *   -1 on an error
 *    0 if at or before the start of the matrix
 *    1 if between time values
 *    2 if at or after the end of the matrix
 *
 */

int FollowClient::project_error(
	TRAJ_DATA *m_traj_falcon,	// Falcon trajectory object
	double *state,			// current state
	double *trajVector)		// traj to modify and return
  
{
  int i, status;
  extern double trajerr[NUMINP];
  double trajStartTime, trajEndTime, trajTime, trajTempTime;
  double trajLeftTime, trajRightTime;
  double trajLeftStartVector[25], trajLeftEndVector[25];
  double trajRightStartVector[25], trajRightEndVector[25];
  double trajFractionLeft, trajFractionRight;
  double trajTempVector[25];

  /*
   * Find the nearest point on the desired trajectory
   *
   * The first thing that we must do is fine the point on the give
   * trajectory that is nearest to our current position.  We do this
   * by looking for the nearest trajectory point perpindicular to the
   * current vehicle orientation.
   *
   */

  // Get the trajectory entry for the segment we are on
  int startLeftPoint = closestLeftLanePoint();
  int startRightPoint = closestRightLanePoint();

  // If it's the last point, we are done
  if ((startLeftPoint == traj_rows(m_traj_falcon)-laneSight - 1)|| 
    (startRightPoint == traj_rows(m_traj_falcon)-laneSight - 1))
    return 2;

  // Get the start point and end point
  traj_row(m_traj_falcon, startLeftPoint, &trajLeftTime, trajLeftStartVector);
  traj_row(m_traj_falcon, startLeftPoint+1, &trajLeftTime, trajLeftEndVector);
  traj_row(m_traj_falcon, startRightPoint, &trajRightTime, trajRightStartVector);
  traj_row(m_traj_falcon, startRightPoint+1, &trajRightTime, trajRightEndVector);

  //get average sigmas of the left and right lanes out laneSight number of points


  double tempLeft = 0;
  double tempRight = 0;
  for (int i = 0; i < laneSight; i++)
  {
    traj_row(m_traj_falcon, (startLeftPoint + i), &trajTempTime, trajTempVector);
    tempLeft += trajTempVector[LSIGMA];
    traj_row(m_traj_falcon, (startRightPoint + i), &trajTempTime, trajTempVector);
    tempRight += trajTempVector[RSIGMA];
  }
  sigmaL = tempLeft / laneSight;
  sigmaR = tempRight / laneSight;

  //Decide our stuff behaviors and things
  //Also updates olssigmas, behavior flags, previousLaneConditions...
  int tempDecision = laneFollowBrain();
  
  // Now figure out the closest location along the segment.  We do
  // this by taking the inner product between the trajectory segment
  // and the vector from start point to current point, then normalize
  // by the length of the trajectory segment
  //
  // Note: this doesn't take current angle into account, so not quite
  // hat we originally wanted (but perhaps good enough for now)
  //
  
  
  //I am sure this code isn't being used. so I got rid of it



  // Get the trajectory for the closest point 
  traj_read(m_traj_falcon, trajLeftVector, trajLeftTime);
  traj_read(m_traj_falcon, trajRightVector, trajRightTime);
  trajTime = (trajLeftTime + trajRightTime) / 2;
  traj_read(m_traj_falcon, trajVector, trajTime);

  /* 
   * Construct the desired error
   *
   * We now must construct a desired trajectory such that when we
   * subtract the state, we get the desired error vector.  We use the
   * following rules for each state variable:
   *
   * x_err = 0
   * y_err = lateral distance to nearest point
   * t_err = angle between trajectory segment and vehicle angle
   * xvel_err = 0
   * yvel_err = current lateral velocity
   * tvel_err = relative rate error
   * accels = vehicle accels (assumes constant velocity segments)
   *
   */
  // Compute lateral distance in body coordinates
  trajerr[XPOS] = 0;
  //This is the old version, used in CDS110b, 2006 (computes error 
  // perpendicular to Alice)
  //    trajerr[YPOS] = 
  //    ((state[XPOS]+xorigin - trajVector[2+XPOS]) * sin(state[TPOS]) +
  //    (state[YPOS]+yorigin - trajVector[2+YPOS]) * cos(state[TPOS]));
  
  //This is the new version, which computes y-error the same way 
  //trajFollower does (perpendicular to the traj)
  
  double leftYerr = - (state[XPOS]+xorigin - trajLeftVector[2+LXPOS])*sin(trajLeftVector[2+LTPOS]) + 
   (state[YPOS]+yorigin - trajLeftVector[2+LYPOS])*cos(trajLeftVector[2+LTPOS]);
  double rightYerr = - (state[XPOS]+xorigin - trajRightVector[2+RXPOS])*sin(trajRightVector[2+RTPOS]) + 
   (state[YPOS]+yorigin - trajRightVector[2+RYPOS])*cos(trajRightVector[2+RTPOS]);
  
  if (tempDecision == MAINTAINRIGHT)
    rightMaintainDistance = rightYerr;
  if (tempDecision == MAINTAINLEFT)
    leftMaintainDistance = leftYerr;

  //trajerr[YPOS] =

  if (behaviorFlag == BOTH)
    trajerr[YPOS] = (rightYerr + leftYerr) / 2;            //treat both equally good.
  if (behaviorFlag == MAINTAINRIGHT)
    trajerr[YPOS] = (rightYerr - rightMaintainDistance);
  if (behaviorFlag == DEFAULTRIGHT)
    trajerr[YPOS] = (rightYerr - rightDefaultDistance);
  if (behaviorFlag == MAINTAINLEFT)
    trajerr[YPOS] = (leftYerr - leftMaintainDistance);
  if (behaviorFlag == DEFAULTRIGHT)
    trajerr[YPOS] = (leftYerr - leftDefaultDistance);
   

  // Compute error in heading versus angle along the path
  // FOr now we just average the lane theta errors
  double leftThetaPosError = state[TPOS] - trajLeftVector[2+LTPOS];
  double rightThetaPosError = state[TPOS] - trajRightVector[2+RTPOS];
  trajerr[TPOS] = (leftThetaPosError + rightThetaPosError) / 2;

  // Figure out the velocity along the path
  double vdesLeft = sqrt(pow(trajLeftVector[2+LXVEL], 2.0) +
		     pow(trajLeftVector[2+LYVEL], 2.0));
  double vdesRight = sqrt(pow(trajRightVector[2+RXVEL], 2.0) +
		     pow(trajRightVector[2+RYVEL], 2.0));

  double vdes = (vdesLeft + vdesRight) / 2;

  // Compute the lateral velocity error in body coordinates
  trajerr[XVEL] = 0;
  trajerr[YVEL] = -vdes * sin(trajerr[TPOS]);

  // Compute heading rate error along the path
  double leftThetaVelError = state[TVEL] - trajLeftVector[2+LTVEL];
  double rightThetaVelError = state[TVEL] - trajRightVector[2+RTVEL];
  trajerr[TVEL] = (leftThetaVelError + rightThetaVelError) / 2;
    
			
  trajerr[XACC] = state[XACC];;
  trajerr[YACC] = state[YACC];;
  trajerr[TACC] = state[TACC];;

  /* 
   * Construct the desired trajectory
   *
   * Finally, we construct the desired trajectory such that when subtracted
   * from the current state, you get the desired error.
   *
   */
  for (i = 0; i < NUMINP; ++i) trajVector[2+i] = state[i] - trajerr[i];
  trajVector[2] += xorigin;
  trajVector[3] += yorigin;

  return status;
}


int FollowClient::laneFollowBrain() //Assumes sigmas are all set up.  Will update old sigmas
{
  currentLaneCondition = rightvsleft();
  double decision = chooseLaneFollowingBehavior();
  previousLaneCondition = currentLaneCondition;
  oldsigmaL = sigmaL;
  oldsigmaR = sigmaR;
  if (decision == NOCHANGE)
    return decision;
  behaviorFlag = decision;
  return decision;    
}


int FollowClient::sigma_property(double sigma)
{
  if (sigma < threshold)
    return GOOD;
  return BAD;
}


int FollowClient::rightvsleft()
{
  // get the goodnes of the sigmas around us
  
  int oldsigmaRprop = sigma_property(oldsigmaR);
  int oldsigmaLprop = sigma_property(oldsigmaL);
  int sigmaRprop = sigma_property(sigmaR);
  int sigmaLprop = sigma_property(sigmaL);
  int rProp, lProp, laneCondition;

  // Decide how the goodnesses of the lanes are changing

  if (oldsigmaRprop == sigmaRprop)
  {
    if (oldsigmaRprop == GOOD)
        rProp = GOOD2GOOD;
    else
        rProp = BAD2BAD;
  }
  else
  {
    if (oldsigmaRprop == GOOD)
        rProp = GOOD2BAD;
    else
        rProp = BAD2GOOD;
  }
  if (oldsigmaLprop == sigmaLprop)
  {
    if (oldsigmaLprop == GOOD)
        lProp = GOOD2GOOD;
    else
        lProp = BAD2BAD;
  }
  else
  {
    if (oldsigmaRprop == GOOD)
        lProp = GOOD2BAD;
    else
        lProp = BAD2GOOD;
  }


//This loop will implement the first decision block.
//It decides the conditions of the lanes

    
  if (lProp == rProp)
  {
    if (lProp == GOOD2GOOD)
      laneCondition = BOTHGOOD;
    else if (lProp == GOOD2BAD)
      laneCondition = BOTHBAD;
    else if (lProp == BAD2GOOD)
      laneCondition = BOTHGOOD;
    else
      laneCondition = BOTHBAD;
  }
  else
  {
    if (lProp == GOOD2GOOD)
    {
      if (rProp == GOOD2BAD)
        laneCondition = LEFT;
      else if (rProp ==BAD2GOOD)
        laneCondition = BOTHGOOD;
      else
        laneCondition = LEFT;
    }
    else if (lProp == GOOD2BAD)
    {
      if (rProp == GOOD2GOOD)
        laneCondition = RIGHT;
      else if (rProp ==BAD2GOOD)
        laneCondition = RIGHT;
      else
        laneCondition = BOTHBAD;
    }
    else if (lProp == BAD2GOOD)
    {
      if (rProp == GOOD2GOOD)
        laneCondition = BOTHGOOD;
      else if (rProp ==GOOD2BAD)
        laneCondition = LEFT;
      else
        laneCondition = LEFT;
    }
    else
    {
      if (rProp == GOOD2GOOD)
        laneCondition = RIGHT;
      else if (rProp ==GOOD2BAD)
        laneCondition = BOTHBAD;
      else
        laneCondition = RIGHT;
    }        
  }
  return laneCondition;
}

//This function picks the behavior to follow. such as treat both lanes equally, maintain current
// distance to left lane...

int FollowClient::chooseLaneFollowingBehavior()
{
  int behavior;
  if ((currentLaneCondition == BOTHGOOD)||(currentLaneCondition == BOTHBAD))
    behavior = BOTH;
  else if (currentLaneCondition == RIGHT)
  {
    if (previousLaneCondition == BOTHGOOD)
      behavior = MAINTAINRIGHT;
    else if (previousLaneCondition == RIGHT)
      behavior = NOCHANGE;
    else
      behavior = DEFAULTRIGHT;
  }
  else  
  {
    if (previousLaneCondition == BOTHGOOD)
      behavior = MAINTAINLEFT;
    else if (previousLaneCondition == LEFT)
      behavior = NOCHANGE;
    else
      behavior = DEFAULTLEFT;
  }
  return behavior;
}

