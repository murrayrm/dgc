#ifndef __PARTICLEFILTER_H__
#define __PARTICLEFILTER_H__

#include "Vector.h"
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

using namespace std;

class ParticleFilter
{

 private:
  /**This Vector array contains the particles that we're looking at.  The particles will be 9-dimensional Vectors representing a possible state of the vehicle (6 variables), plus an importance weight.  The Vectors will contain the following information

  Row 1: x (current vehicle position)
  Row 2: y (current vehicle position)
  Row 3: theta (current vehicle heading)
  Row 4: a (linear tangential acceleration, + = forward, - = reverse)
  Row 5: v (linear tangential velocity, + = forward, - = reverse)
  Row 6: K (curvature of vehicle spatial path)
  Row 9: W (importance weight, for resampling step)
  */
  Vector* particles;

  /**The number of particles we have. */
  int numParticles; 

  /**For noise generation.*/

  gsl_rng* rand;

  /**Noise standard deviations. */
  double accel_noise, steer_noise, length_noise, width_noise, dist_measure_noise;

  /**Propagates the particle "previous" through elapsed time "elapsed_time", and stores it as "next". */
  void propagateParticle(const Vector& previous, Vector& next, const double elapsed_time) const;

  
  

public:

  /**Initializes the particle filter by using the PDF approximated by the particle set "initial_set"*/
  ParticleFilter(const Vector* initial_set, const int parts, const double accel, const double steer, const double length, const double width, const double dist);

  /**Propagates the current particle set forward "elapsed_time" seconds according to our robot motion model.*/
  Vector* propagatePDF(const double elapsed_time) const;
  
  /**Generates a new set of particles to represent the posterior belief after incorporating new sensor measurements.
     Sensor measurements are of the form (x, y).
  */
  void updatePDF(const double elapsed_time, const double* sensor_data);

  void resample(Vector* sample_set);

  Vector* getPDF() const;

  /**Generates a set of particle sets that represent the propagation of the PDF for this vehicle's position, with each set corresponding to a moment in time.  There are "numsteps" such sets, each separated by "interval" seconds in time.*/
  Vector** generatePrediction(const int numsteps, const double interval);


  ~ParticleFilter();
};

#endif //__PARTICLEFILTER_H__
