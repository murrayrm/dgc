#ifndef __MATRIX_CC__
#define __MATRIX_CC__

#include <iostream>
#include <fstream>
#include <assert.h>
#include <math.h>
#include "Matrix.h"
#include "GeneralMatrix.h"
#include "Polynomial.h"
#include "IndexOutOfBounds.h"
#include "UndefinedOperation.h"

using namespace std;

Matrix::Matrix()
{
  matrix = new double[1];
  matrix[0] = 0;
}



Matrix::Matrix(const int r, const int c)
{
  if((r < 1) || (c < 1))
    {
      throw IndexOutOfBounds("Error: Matrices must be at least 1 x 1.");
    }

  rows = r;
  columns = c;
 
  matrix = new double[rows*columns];
  
  for(int i = 0; i < rows*columns; i++)
    {
      matrix[i] = 0;
    }
}

Matrix::Matrix(const int order)
{
  if(order < 1)
    {
      throw IndexOutOfBounds("Error: Matrices must be at least 1 x 1.");
    }

  rows = order;
  columns = order;
  matrix = new double[rows*columns];
 
 for(int i = 0; i < rows*columns; i++)
    {
      matrix[i] = 0;
    }
}

Matrix::Matrix(const Matrix& other)
{
  rows = other.getRows();
  columns = other.getColumns();
  matrix = new double[rows*columns];
  
  for(int r = 1; r<= rows; r++)
    {
      for(int c = 1; c <= columns; c++)
	  setElement(r,c, other.getElement(r,c));
    }
}

Matrix::Matrix(const Vector& v)
{
  rows = v.getDimension();
  columns = 1;
  matrix = new double[rows];

  for(int r = 1; r <= rows; r++)
    {
      setElement(r, 1, v.getElement(r));
    }
}

Matrix Matrix::identity(const int order)
    {
      Matrix ident(order);
      
      for(int d = 1; d <= order; d++)
	{
	  ident.setElement(d, d, 1);
	}
      
      return ident;
    }

Matrix& Matrix::operator = (const Matrix& b)
{
 
  if(&b != this) //Check for self-assignment
    {
      double* temp = matrix;
      rows = b.getRows();
      columns = b.getColumns();
      matrix = new double[rows*columns];
      
      for(int r = 1; r<= rows; r++)
	{
	  for(int c = 1; c <= columns; c++)
	    {
	      setElement(r,c, b.getElement(r,c));
	    }
	}
      delete [] temp;
    }

  return (*this);
}


int Matrix:: getRows() const
{
  return rows;
}

int Matrix:: getColumns() const
{
  return columns;
}

int Matrix:: getOrder() const
{
  if(!isSquare())
    {
      throw UndefinedOperation("Error:  The order property is only defined for square matrices.");
    }
  return rows;
}

double Matrix::getElement(const int row, const int col) const
{
  if((row < 1) || (row >  rows) ||  (col < 1) ||  (col >  columns))
    {
      throw IndexOutOfBounds();
    }

  return matrix[(row - 1)*columns + (col-1)];
}

void Matrix:: setElement(const int row, const int col, const double val)
{
 if((row < 1) || (row >  rows) ||  (col < 1) ||  (col >  columns))
    {
      throw IndexOutOfBounds();
    }

  matrix[(row - 1)*columns  + (col-1)] = val;
}

Matrix Matrix:: operator + (const Matrix& b) const
{
  if((getRows() != b.getRows()) ||  (getColumns() != b.getColumns()))
    {
      throw UndefinedOperation("Error: Matrix addition is only defined for matrices having the same dimensions.");
    }

  Matrix answer(getRows(), getColumns());
  for(int r = 1; r <= getRows(); r++)
    {
      for(int c = 1; c <= getColumns(); c++)
	{
	  answer.setElement(r,c, getElement(r,c) + b.getElement(r,c));
	}
    }
  return answer;
}

Matrix Matrix:: operator - (const Matrix& b) const
{
  if((getRows() != b.getRows()) ||  (getColumns() != b.getColumns()))
    {
      throw UndefinedOperation("Error: Matrix subtraction is only defined for matrices having the same dimensions.");
    }

  Matrix answer(getRows(), getColumns());
  for(int r = 1; r <= getRows(); r++)
    {
      for(int c = 1; c <= getColumns(); c++)
	{
	  answer.setElement(r,c, getElement(r,c) - b.getElement(r,c));
	}
    }
  return answer;
}

Matrix Matrix:: scalar(const double k) const
{
  Matrix answer(getRows(), getColumns());
  for(int r = 1; r<= getRows(); r++)
    {
      for(int c = 1; c<= getColumns(); c++)
	  answer.setElement(r,c, getElement(r,c)*k);
    }
  return answer;
}

Matrix Matrix:: operator * (const Matrix& b) const
{
  if(getColumns() != b.getRows())
    {
      throw UndefinedOperation("Error: The matrix product AB is only defined if A has the same number of columns as B has rows.");
    }

  Matrix answer(getRows(), b.getColumns());
  for(int r = 1; r<= getRows(); r++)
    {
      for(int c = 1; c<= b.getColumns(); c++)
	{
	  double product = 0;
	  for(int i = 1; i<= getColumns(); i++)
	    {
	      product += getElement(r,i)*b.getElement(i,c);
	    }
	  answer.setElement(r,c,product);
	}
    }
  return answer;
}

Vector Matrix:: operator * (const Vector& v) const
{

  if(columns != v.getDimension())
    {
      throw UndefinedOperation("Error:  The product of a matrix and a vector is only defined if the number of columns of the matrix is equal to the dimension of the vector.");
    }

  Vector answer(rows);

  double inner;

  for(int r = 1; r<= rows; r++)
    {
      inner = 0;

      for(int c = 1; c<= columns; c++)
	{
	  inner += getElement(r, c)*v.getElement(c);
	}
      answer.setElement(r, inner);
    }
  return answer;
}

double Matrix::determinant() const
{
  if(!isSquare())
    {
      throw UndefinedOperation("Error: Determinants are only defined for square matrices.");
    }

  if(getOrder() == 1)
    {
      return getElement(1,1);
    }
  
  else
    {
      Matrix temp = (*this);
      double val, multiple, eliminate, newVal;
      double determinant = 1;

      int interchanges = 0;

      //Convert the matrix to an upper triangular matrix.
      for(int d = 1; d < getOrder(); d++)
	{
	  val = temp.getElement(d,d);
	  //Check to see if this element is zero; if it is, we need to interchange this row with one that does not have a zero in this column and that is below rows d, provided that such a row exists.

	  int k = d+1;

	  while(k <= getRows() && val == 0)
	    {
	      if(temp.getElement(k,d) != 0) //We've found the row we need.
		{
		  temp.interchange_rows(d, k);  //Switch the two rows
		  interchanges++;
		  val = temp.getElement(d, d);
		}
	      k++;
	    }

	  for(int r = d+1; r<= getOrder(); r++)
	    {
	      eliminate = temp.getElement(r,d);
	      if((eliminate != 0) && (val !=0))
		{
		  multiple = -eliminate/val;
		  for(int c = d; c<= getOrder(); c++)
		    {
		      newVal = multiple*temp.getElement(d,c)+temp.getElement(r,c);
		      temp.setElement(r,c,newVal);
		    }
		}
	    }
	}
      

      //The determinant is now the product of all the diagonal elements.
      for(int d = 1; d <= getOrder(); d++)
	{
	  determinant *= temp.getElement(d,d);
	}
      
      determinant *= pow(-1, interchanges);
      return determinant;
    }
}

Matrix Matrix:: rref() const
{
  Matrix temp = (*this);
  double val, multiple, eliminate, newVal;

  int l;

  //Set l = min (columns - 1, rows)
  if(columns - 1 <= rows)
    {
      l = columns - 1;
    }
  else
    {
      l = rows;
    }

  //First, eliminate all nonzero entries below the main diagonal.
  for(int d = 1; d < l; d++)
    {
      val = temp.getElement(d,d);

      int k = d+1;
      
      //If val == 0, we need to find the first row > d such that the element from that row in column d is not zero and interchange these two rows.
      while(k <= getRows() && val == 0)
	{
	  if(temp.getElement(k,d) != 0) //We've found the row we need.
	    {
	      temp.interchange_rows(d, k);  //Switch the two rows
	      val = temp.getElement(d, d);
	    }
	  k++;
	}
      
      for(int r = d+1; r <= getRows(); r++)
	{
	  eliminate = temp.getElement(r,d);
	  if((eliminate != 0) && (val !=0))
	    {
	      multiple = -eliminate/val;
	    
	      for(int c = 1; c <= getColumns(); c++)
		{
		  newVal = multiple*temp.getElement(d,c)+temp.getElement(r,c);
		  temp.setElement(r,c,newVal);
		}
	    }
	}
    }

  //Eliminate all nonzero elements above the main diagonal.
  for(int d = l; d > 1; d--)
    {
      val = temp.getElement(d,d);
      for(int r = d-1; r >= 1; r--)
	{
	  eliminate = temp.getElement(r,d);
	  if((eliminate != 0) && (val !=0))
	    {
	      multiple = -eliminate/val;
	    
	      for(int c = 1; c <= getColumns(); c++)
		{
		  newVal = multiple*temp.getElement(d,c)+temp.getElement(r,c);
		  temp.setElement(r,c,newVal);
		}
	    }
	}
    }

  //Now the matrix should have an l-th order diagonal matrix as in its upper-left hand corner.

  for(int d = 1; d <= l; d++)
  {
    val = temp.getElement(d, d);

    if(val != 0)
      {
	multiple = 1 / val;
      
	for(int c = d; c <= getColumns(); c++)
	  {
	    temp.setElement(d, c, temp.getElement(d, c)*multiple);
	  }
      }
  }

  return temp;
}

Polynomial Matrix::CharacteristicPolynomial() const
{
  if(!isSquare())
    {
      throw UndefinedOperation("Error: Characteristic Polynomials are only defined for square matrices.");
    }

  GeneralMatrix<Polynomial> temp(rows, columns);
  double* coeffs = new double[2];
  for(int r = 1; r<= rows; r++)
    {
      for(int c = 1; c<= columns; c++)
	{
	  if(r == c)
	    {
	      coeffs[0] = -1*getElement(r,c);
	      coeffs[1] = 1;
	      temp.setElement(r,c, Polynomial(1,coeffs));
	    }
	  else
	    {
	      temp.setElement(r,c, Polynomial(-1*getElement(r,c)));
	    }
	}
    }

  delete [] coeffs;

  return temp.determinant();
}

double* Matrix::eigenvalues(const double guess, const double tolerance, const double step, const int maxLoop, int& numfound) const
{
  if(!isSquare())
    {
      throw UndefinedOperation("Error: Eigenvalues are only defined for square matrices.");
    }

  Polynomial charpol = CharacteristicPolynomial();

  double* answer = charpol.findRoots(guess, tolerance, step, maxLoop, numfound);

  return answer;
}



Matrix Matrix::submatrix(const int row, const int column) const
{
  if((row < 1) || (row >  rows) ||  (column < 1) ||  (column >  columns))
    {
      throw IndexOutOfBounds();
    }
  Matrix temp(getRows() - 1, getColumns() - 1);
  int a = 1;
  int b;
  for(int r = 1; r <= getRows(); r++)
    {
      if(r != row)
	{
	  b = 1;
	  for(int c = 1; c <= getColumns(); c++)
	    {
	      if(c != column)
		{
		  temp.setElement(a,b, getElement(r,c));
		  b++;
		}
	    }
	  a++;
	}
    }

  return temp;
}

double Matrix:: cofactor(const int row, const int col) const
{

  if((row < 1) || (row >  rows) ||  (col < 1) ||  (col >  columns))
    {
      throw IndexOutOfBounds();
    }

  if(!isSquare())
    {
     throw UndefinedOperation("Error: Cofactors of elements in a matrix are only defined for square matrices."); 
    }
     
  return submatrix(row, col).determinant()*pow(-1, row+col);
}

Matrix Matrix:: transpose() const
{
  Matrix temp(getRows(), getColumns());
  for(int r = 1; r <= getRows(); r++)
    {
      for(int c = 1; c <= getColumns(); c++)
	{
	  temp.setElement(c, r, getElement(r,c));
	}
    }

  return temp;
}

Matrix Matrix:: inverse() const
{
  if(!isSquare())
    {
      throw UndefinedOperation("Error: Only square matrices can have inverses."); 
    }

  double det = determinant();

  if(det == 0)
    {
      throw UndefinedOperation("Error: Singular matrices do not have inverses.");
    }

  Matrix temp(getOrder());
  for(int r = 1; r <= getOrder(); r++)
    {
      for(int c = 1; c <= getOrder(); c++)
	{
	  temp.setElement(r,c, cofactor(r,c));
	}
    }
  temp = temp.transpose();
  temp = temp.scalar(1/det);
  return temp;
}

bool Matrix:: operator == (const Matrix& b) const
{
  bool temp = true;
  if((getRows() != b.getRows()) || (getColumns() != b.getColumns()))
    {
      temp = false;
    }

  else
    {
      for(int r = 1; r <= getRows(); r++)
	{
	  for(int c = 1; c <= getColumns(); c++)
	    {
	      if(getElement(r,c) != b.getElement(r,c))
		{
		  temp = false;
		}
	    }
	}
    }
  return temp;
}

bool Matrix::operator != (const Matrix& b) const
{
  return !((*this) == b);
}

Matrix Matrix::operator ^ (const int power) const
{
  if(!isSquare())
    {
      throw UndefinedOperation("Error: Powers of matrices are only defined for square matrices");
    }


  Matrix answer = identity(getOrder());

  if(power > 1)
    {
      for(int p = power; p >= 1; p--)
	{
	  answer = answer * (*this);
	}
    }

  else if(power < 1)
    {

      Matrix I = inverse();

      for(int p = power; p <= -1; p++)
	{
	  answer = answer * I;
	}
    }

  return answer;
}

bool Matrix:: isSquare() const
{
  return (getRows() == getColumns());
}

bool Matrix:: isOrthogonal() const
{
  if(!isSquare())
    {
      return false;
    }
  else
    {
      return ((*this).inverse() == (*this).transpose());
    }
}

bool Matrix:: isNormal() const
{
  if(!isSquare())
    {
      return false;
    }
  else
    {
      return ((*this)*(*this).transpose() == (*this).transpose()*(*this));
    }
}

bool Matrix:: isSymmetric() const
{
  bool temp;
  if(!isSquare())
    {
      temp =  false;
    }
  else
    {
      temp = ((*this) == (*this).transpose());
    }
  return temp;
}

bool Matrix:: isSkewSymmetric() const
{
  bool temp;
  if(!isSquare())
    {
      temp = false;
    }
  else
    {
      temp = ((*this).scalar(-1) == (*this).transpose());
    }
  return temp;
}

bool Matrix:: isInverse(const Matrix& b) const
{
  if(!isSquare())
    {
      return false;
    }
  else
    {
      return ((*this) == b.inverse());
    }
}

void Matrix::interchange_rows(int a, int b)
{
  double temp [getColumns()];

  //Copy elements from row a into this array
  for(int i = 0; i < getColumns(); i++)
    {
      temp[i] = getElement(a, i + 1);
    }

  //Copy elements from row b into row a
  for(int c = 1; c <= getColumns(); c++)
    {
      setElement(a, c, getElement(b, c));
    }

  //Copy elements from the array (a's old elements) into b
  for(int c = 1; c <= getColumns(); c++)
    {
      setElement(b, c, temp[c-1]);
    }
}

void Matrix::print() const
{
  for(int r = 1; r <= rows; r++)
    {
      for(int c = 1; c <= columns; c++)
	{
	  cout<< getElement(r, c) <<" ";
	}
      cout<<endl;
    }
}

void Matrix::write(ofstream& outfile) const
{
  outfile <<getRows()<<" "<<getColumns()<<endl;

  for(int r = 1; r <= rows; r++)
    {
      for(int c = 1; c <= columns; c++)
	{
	  outfile<< getElement(r, c) <<" ";
	}
      outfile<<endl;
    }
}

Matrix Matrix::read(ifstream& infile)
{
  int rows, cols;
  double element;

  infile >> rows;
  infile >> cols;

  Matrix answer(rows, cols);

  for(int r = 1; r <= rows; r++)
    {
      for(int c = 1; c <= cols; c++)
	{
	  infile >> element;
	  answer.setElement(r, c, element);
	}
    }
  return answer;
}


Matrix::~Matrix()
{
  delete [] matrix;
}

#endif // __MATRIX_CC__
