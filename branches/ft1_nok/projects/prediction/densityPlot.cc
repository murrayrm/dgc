#include <fstream>
#include <iostream>
#include <sstream>
#include <math.h>

using namespace std;

int main (void)
{

  int tracktimesteps, predtimesteps, numparticles;

  double Nlower, Nupper, Elower, Eupper, Nres, Eres;

  cout << "Input the number of tracking timesteps from the last simulation."<<endl;
  cout << "Number of tracking timesteps: ";
  cin >> tracktimesteps;
  cout << "Input the number of prediction-only timesteps from the last simulation."<<endl;
  cout << "Number of prediction-only timesteps: ";
  cin >>predtimesteps;




  cout << "Input N range of data (in meters)."<<endl;
  cout<< "Lower bound on N: ";
  cin >> Nlower;
  cout<<"Upper bound on N: ";
  cin >> Nupper;
  cout<<"Lower bound on E: ";
  cin >> Elower;
  cout<<"Upper bound on E: ";
  cin >> Eupper;

  cout << "Input grid size in N-direction (in meters): ";
  cin >>Nres;
  cout << "Input grid size in E-direction (in meters): ";
  cin >> Eres;

  cout << "Input the number of particles per particle set: ";
  cin >> numparticles;

  int Nboxes = (int) ((Nupper - Nlower)/Nres);
  int Eboxes = (int) ((Eupper - Elower)/Eres);

  double** grid = new double*[Nboxes];

  for(int i = 0; i < Nboxes; i++)
    {
      grid[i] = new double[Eboxes];
    }

  cout<<"Initializing grid."<<endl;
  //Initialize all grid cells to contain zero particles.
  for(int N = 0; N < Nboxes; N++)
    {
      for(int E = 0; E < Eboxes; E++)
	{
	  grid[N][E] = 0;
	}
    }

  //Ok, now load up the files and start reading in stuff.
  double Npos, Epos,  waste;
  int Nbox, Ebox;

  for(int i = 1; i <= tracktimesteps; i++)
    {
      //Ok, we need to load up each particle set file and read it in.
      //filename formatting shit.
      stringstream ss; //So we can convert numbers to strings.
      ss <<"unittest_posterior"<<i<<".dat";
      string intermediate;
      ss >> intermediate;
      char post [25];
      strcpy(post, intermediate.c_str()); //To copy the string intermediate into filename as a char*;
      

      cout << "Loading input file "<<post<<endl;
      ifstream priorfile (post);
      char header [80];



      priorfile >> header >> header >> waste;  //To get rid of the header line and the whitespace line.

      for(int p = 1; p <= numparticles; p++)
	{
	  //Extract the NE location from the file.

	  priorfile >> Npos;
	  priorfile >> Epos;

	  if(p ==1 && i == 1)
	    {
	      cout<<"First (N,E) position = ("<<Npos<<", "<<Epos<<")"<<endl;
	    }

	   if(p ==2 && i ==1)
	    {
	      cout<<"Second (N,E) position = ("<<Npos<<", "<<Epos<<")"<<endl;
	    }

	  //Read the other four numbers and discard to advance the file pointer to the correct position.

	  for(int n = 1; n <= 4; n++)
	    {
	      priorfile >> waste;
	    }

	  //Now increment the appropriate grid cell in which this particle is found.

	  //Note:  Need to cast ENTIRE QUANTITY EXPLICITYLY (without parens, you end up just casting the quantity (Npos - Nlower), which is not what we want.
	  if (Nbox < 0)
	    {
	      Nbox = 0;
	    }
	  if(Nbox >= Nboxes)
	    {
	      Nbox = Nboxes;
	    }
	  if(Ebox < 0)
	    {
	      Ebox = 0;
	    }
	  if(Ebox >= Eboxes)
	    {
	      Ebox = Eboxes;
	    }
	  Nbox = (int) ((Npos - Nlower)/Nres);
	  Ebox = (int) ((Epos - Elower)/Eres);

	  grid[Nbox][Ebox] += 1;
	}

      priorfile.close();
    }
  

  //Now load up the predictions file.

  for(int i = 1; i <= predtimesteps; i++)
    {
      char predfilename[] = "unittest_predictions.dat";

      cout << "Loading input file "<<predfilename<<endl;
      ifstream predfile (predfilename);
      char header [80];

      for(int p = 1; p <= numparticles; p++)
	{
	  //Extract the NE location from the file.

	  predfile >> Npos;
	  predfile >> Epos;

	  if(p ==1 && i == 1)
	    {
	      cout<<"First (N,E) position = ("<<Npos<<", "<<Epos<<")"<<endl;
	    }

	   if(p ==2 && i ==1)
	    {
	      cout<<"Second (N,E) position = ("<<Npos<<", "<<Epos<<")"<<endl;
	    }

	  //Read the other four numbers and discard to advance the file pointer to the correct position.

	  for(int n = 1; n <= 4; n++)
	    {
	      predfile >> waste;
	    }

	  //Now increment the appropriate grid cell in which this particle is found.

	  //Note:  Need to cast ENTIRE QUANTITY EXPLICITYLY (without parens, you end up just casting the quantity (Npos - Nlower), which is not what we want.
	  if (Nbox < 0)
	    {
	      Nbox = 0;
	    }
	  if(Nbox >= Nboxes)
	    {
	      Nbox = Nboxes;
	    }
	  if(Ebox < 0)
	    {
	      Ebox = 0;
	    }
	  if(Ebox >= Eboxes)
	    {
	      Ebox = Eboxes;
	    }
	  Nbox = (int) ((Npos - Nlower)/Nres);
	  Ebox = (int) ((Epos - Elower)/Eres);

	  grid[Nbox][Ebox] += 1;
	}

      predfile.close();
    }

  //Ok, now normalize all particle counts by dividing by the total number of particles in the particle set.

  cout<<"Normalizing particle grid counts."<<endl;

  ofstream outfile ("DensityPlot.dat");
  ofstream outfile2 ("DensityPlotNZ.dat");  //Nonzero parts of DensityPlot.dat
  
  for(int N = 0 ; N < Nboxes; N++)
    {
      for(int E = 0; E < Eboxes; E++)
	{
	  grid[N][E] /= numparticles;
	}
    }

  outfile << "#Northing          Easting          Probability Value"<<endl<<endl;
  

  for(int N = 0 ; N < Nboxes; N++)
    {
      for(int E = 0; E < Eboxes; E++)
	{
	      outfile <<N*Nres + Nlower<<" "<<E*Eres+Elower<<" "<<grid[N][E]<<endl;
	      if(grid[N][E] > .001)  //Add it to the nonzero list, DensityPlotNZ.dat
		{
		  outfile2 <<N*Nres + Nlower<<" "<<E*Eres+Elower<<" "<<grid[N][E]<<endl;
		}
	}
      #warning:"Remove this before using this file as Matlab input! It will throw off formatting!"
      outfile<<endl;
    }

  outfile.close();

}
