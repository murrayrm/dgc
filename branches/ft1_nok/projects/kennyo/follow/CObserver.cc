#include "CObserver.hh"

CObserver::CObserver() {
	Q = NULL;
	R = NULL;
	P = NULL;
	dP = NULL;
	x_hat = NULL;
	old_x_hat = NULL;
	dx_hat = NULL;
	cor = NULL;
	A = NULL;
	B = NULL;
	C = NULL;
	F = NULL;
	L = NULL;
	y = NULL;

// 	char logFilename[256];

// 	time_t* currentTime;
// 	time(currentTime);
// 	tm* tmstruct = localtime(currentTime);
// 	sprintf(logFilename, "%04d%02d%02d_%02d%02d%02d.log", tmstruct->tm_year + 1900, 
// 					tmstruct->tm_mon + 1, tmstruct->tm_mday, tmstruct->tm_hour, 
// 					tmstruct->tm_min, tmstruct->tm_sec); 
	
// 	if(logFile.is_open())
// 		logFile.close();
// 	logFile.open(logFilename, ofstream::out | ofstream::app);

	first_run = true;
}


CObserver::~CObserver() {
	if(Q != NULL) mat_free(Q);
	if(R != NULL) mat_free(R);
	if(P != NULL) mat_free(P);
	if(dP != NULL) mat_free(dP);
	if(x_hat != NULL) mat_free(x_hat);
	if(old_x_hat != NULL) mat_free(old_x_hat);
	if(dx_hat != NULL) mat_free(dx_hat);
	if(A != NULL) mat_free(A);
	if(B != NULL) mat_free(B);
	if(C != NULL) mat_free(C);
	if(L != NULL) mat_free(L);
	if(y != NULL) mat_free(y);
	if(F != NULL) mat_free(F);

// 	logFile.close();
}


bool CObserver::loadFile(char* filename) {
	MATRIX *list;
  list = mat_load(filename);

	if(!obs_create()) return false;

	if(!obs_setup(list)) {
		mat_list_free(list);
		return false;
	}
	mat_list_free(list);


	return true;
}


double* CObserver::obs_compute(double* input, double gamma) {
	unsigned long long current_time, time_diff;

	if(first_run) {
		DGCgettime(current_time);
		last_update = current_time;

		for(int i=0; i<6; i++) {
			output[i] = mat_element_get(x_hat, i, 0);
		}
		
		for(int i=0; i<6; i++) {
			for(int j=0; j<6; j++) {
				covar[6*i+j] = mat_element_get(P, i, j);
			}
		}
		
		last_v = input[3];
		last_phi = input[4];

		first_run = false;
		return output;
	}

	MATRIX *tmp, *tmp2;

	tmp = mat_create();
	tmp2 = mat_create();

	mat_copy(old_x_hat, x_hat);

	for(int i=0; i<3; i++) {
		mat_element_set(y, i, 0, input[i]);
	}
	//HACK!
	double v = input[3];
	double phi = input[4];
	double l=VEHICLE_WHEELBASE;
	double theta_hat = mat_element_get(old_x_hat, 2, 0);
	double theta_dot_hat = mat_element_get(old_x_hat, 5, 0);

	DGCgettime(current_time);
	time_diff = current_time-last_update;

	double dT = DGCtimetosec(time_diff, true);
	
// 	logFile << "time: " << current_time << ", dT: " << dT << endl;

	double v_dot = (v-last_v)/dT;
	double phi_dot = (phi-last_phi)/dT;

// 	logFile << "v: " << v << ", last_v: " << last_v << ", v_dot: " << v_dot << endl;
// 	logFile << "phi: " << phi << ", last_phi: " << last_phi << ", phi_dot: " << phi_dot << endl;
// 	logFile << "theta_hat: " << theta_hat << ", theta_dot_hat: " << theta_dot_hat << endl;

	//Setup A
	mat_reset(A);
	mat_element_set(A, 0, 3, 1.0);
	mat_element_set(A, 1, 4, 1.0);
	mat_element_set(A, 2, 5, 1.0);
	mat_element_set(A, 3, 5, -sin(theta_hat)*v_dot - cos(theta_hat)*v*theta_dot_hat);
	mat_element_set(A, 4, 5,  cos(theta_hat)*v_dot - sin(theta_hat)*v*theta_dot_hat);

	//Setup F
	mat_reset(F);
	//This F was for disturbances in inputs
	mat_element_set(F, 3, 0, -1.0*sin(theta_hat)*theta_dot_hat);
	mat_element_set(F, 4, 0,      cos(theta_hat)*theta_dot_hat);
	mat_element_set(F, 5, 0, phi_dot/(l*cos(phi)*cos(phi)));
	mat_element_set(F, 5, 1, v_dot/(l*cos(phi)*cos(phi))+2.0*v*phi_dot*tan(phi)/(cos(phi)*cos(phi)));

	//This F is for additive disturbances in states
// 	mat_element_set(F, 0, 0, 1.0);
// 	mat_element_set(F, 1, 1, 1.0);
// 	mat_element_set(F, 2, 2, 1.0);
// 	mat_element_set(F, 3, 2, -v*sin(theta_hat));
// 	mat_element_set(F, 3, 3, 1.0);
// 	mat_element_set(F, 4, 2,  v*cos(theta_hat));
// 	mat_element_set(F, 4, 4, 1.0);
// 	mat_element_set(F, 5, 5, 1.0);


	//Get the initial prediction
	mat_element_set(dx_hat, 0, 0, mat_element_get(old_x_hat, 3, 0));
	mat_element_set(dx_hat, 1, 0, mat_element_get(old_x_hat, 4, 0));
	mat_element_set(dx_hat, 2, 0, mat_element_get(old_x_hat, 5, 0));
	mat_element_set(dx_hat, 3, 0, v_dot*cos(theta_hat) - v*sin(theta_hat)*theta_dot_hat);
	mat_element_set(dx_hat, 4, 0, v_dot*sin(theta_hat) + v*cos(theta_hat)*theta_dot_hat);
	mat_element_set(dx_hat, 5, 0, v_dot/l*tan(phi) + (v/l)*(1.0/cos(phi))*(1.0/cos(phi))*phi_dot);


// 	logFile << "dx_hat: ";
// 	for(int i=0; i<6; i++) {
// 		logFile << mat_element_get(dx_hat, i, 0) << " ";
// 	}
// 	logFile << endl;

// 	logFile << "[" << __LINE__ << "] F has " << mat_rows(F) << " rows and " << mat_columns(F) << " cols: " << endl;
// 	for(int i=0; i<6; i++) {
// 		for(int j=0; j<2; j++) {
// 			logFile << setprecision(10) << mat_element_get(F, i, j) << " ";
// 		}
// 		logFile << endl;
// 	}
// 	logFile << endl;


// 	logFile << "[" << __LINE__ << "] Q has " << mat_rows(Q) << " rows and " << mat_columns(Q) << " cols: " << endl;
// 	for(int i=0; i<2; i++) {
// 		for(int j=0; j<2; j++) {
// 			logFile << setprecision(10) << mat_element_get(Q, i, j) << " ";
// 		}
// 		logFile << endl;
// 	}
// 	logFile << endl;


	//dP = A*P
	mat_mult(dP, A, P);
	//tmp = A'
	mat_transpose(tmp, A);
	//tmp = P*tmp = P*A'
	mat_mult(tmp, P, tmp);
	//dP = dP + tmp = A*P + P*A'
	mat_add(dP, dP, tmp);
	//tmp2 = R^-1
 	mat_inverse(tmp2, R);
	//tmp = C'
	mat_transpose(tmp,C);
	//tmp = P*tmp = P*C'
	mat_mult(tmp, P, tmp);
	//tmp = tmp*tmp2 = P*C'*R^-1
	mat_mult(tmp, tmp, tmp2);
	//tmp = tmp * C = P*C'*R^-1*C
	mat_mult(tmp, tmp, C);
	//tmp = tmp * P = P*C'*R^-1*C*P
	mat_mult(tmp, tmp, P);
	//tmp = -1*tmp = -1*P*C'*R^-1*C*P
	mat_scale(tmp, tmp, -1.0);
	//dP = dP + tmp = A*P + P*A' - P*C'*R^-1*C*P
	mat_add(dP, dP, tmp);
	//tmp = F'
	mat_transpose(tmp, F);

// 	logFile << "[" << __LINE__ << "] F' has " << mat_rows(tmp) << " rows and " << mat_columns(tmp) << " cols: " << endl;
// 	for(int i=0; i<2; i++) {
// 		for(int j=0; j<6; j++) {
// 			logFile << setprecision(10) << mat_element_get(tmp, i, j) << " ";
// 		}
// 		logFile << endl;
// 	}
// 	logFile << endl;


	//tmp = Q*tmp = Q*F'
	mat_mult(tmp, Q, tmp);
	//tmp = F*tmp = F*Q*F'
	mat_mult(tmp, F, tmp);
	//dP = dP + tmp = A*P + P*A' - P*C'*R^-1*C*P + F*Q*F'
	mat_add(dP, dP, tmp);

// 	logFile << "[" << __LINE__ << "] FQF has " << mat_rows(tmp) << " rows and " << mat_columns(tmp) << " cols: " << endl;
// 	for(int i=0; i<6; i++) {
// 		for(int j=0; j<6; j++) {
// 			logFile << setprecision(10) << mat_element_get(tmp, i, j) << " ";
// 		}
// 		logFile << endl;
// 	}
// 	logFile << endl;


	//tmp = C'
	mat_transpose(tmp, C);

// 	logFile << "[" << __LINE__ << "] C' has " << mat_rows(tmp) << " rows and " << mat_columns(tmp) << " cols: " << endl;
// 	for(int i=0; i<6; i++) {
// 		for(int j=0; j<3; j++) {
// 			logFile << setprecision(10) << mat_element_get(tmp, i, j) << " ";
// 		}
// 		logFile << endl;
// 	}
// 	logFile << endl;



// 	logFile << "[" << __LINE__ << "] P has " << mat_rows(P) << " rows and " << mat_columns(P) << " cols: " << endl;
// 	for(int i=0; i<6; i++) {
// 		for(int j=0; j<6; j++) {
// 			logFile << setprecision(10) << mat_element_get(P, i, j) << " ";
// 		}
// 		logFile << endl;
// 	}
// 	logFile << endl;


	//L = P * tmp = P*C'
	mat_mult(L, P, tmp);
	//tmp2 = R^-1
 	mat_inverse(tmp2, R);

// 	logFile << "[" << __LINE__ << "] R^-1 has " << mat_rows(tmp2) << " rows and " << mat_columns(tmp2) << " cols: " << endl;
// 	for(int i=0; i<3; i++) {
// 		for(int j=0; j<3; j++) {
// 			logFile << setprecision(10) << mat_element_get(tmp2, i, j) << " ";
// 		}
// 		logFile << endl;
// 	}
// 	logFile << endl;


	//L = L*tmp2 = P*C'*R^-1
	mat_mult(L, L, tmp2);

// 	logFile << "[" << __LINE__ << "] L has " << mat_rows(L) << " rows and " << mat_columns(L) << " cols: " << endl;
// 	for(int i=0; i<6; i++) {
// 		for(int j=0; j<3; j++) {
// 			logFile << setprecision(10) << mat_element_get(L, i, j) << " ";
// 		}
// 		logFile << endl;
// 	}
// 	logFile << endl;
	

	//tmp = C*old_x_hat;
	mat_mult(tmp, C, old_x_hat);

// 	logFile << "[" << __LINE__ << "] C*old_x_hat has " << mat_rows(tmp) << " rows and " << mat_columns(tmp) << " cols: ";
// 	for(int i=0; i<3; i++) {
// 		logFile << setprecision(10) << mat_element_get(tmp, i, 0) << " ";
// 	}
// 	logFile << endl;


	//tmp = -1*tmp = -C*old_x_hat
	mat_scale(tmp, tmp, -1.0);
	//tmp = y + tmp = y - C*old_x_hat
	mat_add(tmp, y, tmp);

// 	logFile << "[" << __LINE__ << "] y has " << mat_rows(y) << " rows and " << mat_columns(y) << " cols: ";
// 	for(int i=0; i<3; i++) {
// 		logFile << setprecision(10) << mat_element_get(y, i, 0) << " ";
// 	}
// 	logFile << endl;


// 	logFile << "[" << __LINE__ << "] y-C*old_x_hat has " << mat_rows(tmp) << " rows and " << mat_columns(tmp) << " cols: ";
// 	for(int i=0; i<3; i++) {
// 		logFile << setprecision(10) << mat_element_get(tmp, i, 0) << " ";
// 	}
// 	logFile << endl;

	//correction_terms = L*tmp = L*(y - C*old_x_hat)
	mat_mult(cor, L, tmp);


// 	logFile << "[" << __LINE__ << "] dx_hat: ";
// 	for(int i=0; i<6; i++) {
// 		logFile << mat_element_get(dx_hat, i, 0) << " ";
// 	}
// 	logFile << endl;

// 	logFile << "[" << __LINE__ << "] cor: ";
// 	for(int i=0; i<6; i++) {
// 		logFile << mat_element_get(cor, i, 0) << " ";
// 	}
// 	logFile << endl;


	//dx_hat = dx_hat + cor
	mat_add(dx_hat, dx_hat, cor);
	//dx_hat = dx_hat*dT = dT*(dx_hat + cor)
	mat_scale(dx_hat, dx_hat, dT);
	//x_hat = old_x_hat + dT*(dx_hat + cor)
	mat_add(x_hat, old_x_hat, dx_hat);


// 	logFile << "dx_hat*dT: ";
// 	for(int i=0; i<6; i++) {
// 		logFile << mat_element_get(dx_hat, i, 0) << " ";
// 	}
// 	logFile << endl;




// 	logFile << "line [" << __LINE__ << "] x_hat: ";
// 	for(int i=0; i<6; i++) {
// 		logFile << mat_element_get(x_hat, i, 0) << " ";
// 	}
// 	logFile << endl;


// 	logFile << __LINE__ << " ";
// 	for(int i=0; i<

// 	logFile << "[" << __LINE__ << "] dP: ";
// 	for(int i=0; i<6; i++) {
// 		for(int j=0; j<6; j++) {
// 			logFile << mat_element_get(dP, i, j) << " ";
// 		}
// 		logFile << endl;
// 	}
// 	logFile << endl;
	

	//dP = dP*dT;
	mat_scale(dP, dP, dT);

// 	logFile << "[" << __LINE__ << "] dP*dT: ";
// 	for(int i=0; i<6; i++) {
// 		for(int j=0; j<6; j++) {
// 			logFile << mat_element_get(dP, i, j) << " ";
// 		}
// 		logFile << endl;
// 	}
// 	logFile << endl;

	//P = P + dP = P + dP*dT
	mat_add(P, P, dP);

	last_update = current_time;

	for(int i=0; i<6; i++) {
		output[i] = mat_element_get(x_hat, i, 0);
	}

	for(int i=0; i<6; i++) {
		for(int j=0; j<6; j++) {
// RMM, 12 Mar 06: commented out; OK for elements of P to be negative
//			if(mat_element_get(P, i, j) < 0.0)
//				mat_element_set(P, i, j, 0.0);
//JHG, 16 Mar 06: Not OK for diagonals to be negative
// 			if(i==j && mat_element_get(P, i, j) < 0.0)
// 				mat_element_set(P, i, j, 0.0);
			covar[6*i+j] = mat_element_get(P, i, j);
		}
	}

	last_v = v;
	last_phi = phi;


// 	logFile << "[" << __LINE__ << "] x_hat has " << mat_rows(x_hat) << " rows and " << mat_columns(x_hat) << " cols" << endl;

// 	logFile << "[" << __LINE__ << "] output: ";
// 	for(int i=0; i<6; i++) {
// 		logFile << output[i] << " ";
// 	}

// 	logFile << endl;
// 	logFile << endl;

	return output;
}


bool CObserver::loaded() {
	return ((Q != NULL) && (R != NULL));
}



bool CObserver::obs_create() {
  A = mat_create();       mat_set_name(A, "A");
  B = mat_create();       mat_set_name(B, "B");
  C = mat_create();       mat_set_name(C, "C");

  Q = mat_create();       // mat_set_name(Q, "Q");
  R = mat_create();       // mat_set_name(R, "R");
  P = mat_create();       mat_set_name(P, "P");
  dP = mat_create();       mat_set_name(dP, "dP");

  L = mat_create();       mat_set_name(L, "L");
  x_hat = mat_create();       mat_set_name(x_hat, "x_hat");
	old_x_hat = mat_create();   mat_set_name(old_x_hat, "old_x_hat");
	dx_hat = mat_create();   mat_set_name(dx_hat, "dx_hat");
	cor = mat_create();   mat_set_name(cor, "cor");

	y = mat_create();     mat_set_name(y, "y");

	F = mat_create();     mat_set_name(F, "F");

	return true;
}


bool CObserver::obs_setup(MATRIX* list) {
  MATRIX *tmp = NULL;

  if ( (tmp = mat_find(list,"Q")) == NULL) return false;  mat_copy(Q, tmp);
  if ( (tmp = mat_find(list,"R")) == NULL) return false;  mat_copy(R, tmp);

	mat_resize(A, 6, 6);
// 	mat_resize(B, 
	mat_resize(C, 3, 6);
	mat_element_set(C, 0, 0, 1.0);
	mat_element_set(C, 1, 1, 1.0);
	mat_element_set(C, 2, 5, 1.0);


	mat_resize(P, 6, 6);
	mat_resize(dP, 6, 6);
	mat_resize(x_hat, 6, 1);
	mat_resize(old_x_hat, 6, 1);
	mat_resize(dx_hat, 6, 1);
	mat_resize(cor, 6, 1);
	mat_resize(y, 3, 1);
	mat_resize(F, 6, 2);

// 	mat_resize(F, 

// 	if(tmp != NULL) mat_free(tmp);
 
	return true;
}


void CObserver::set_initial_conditions(double* x_hat_ic) {
	mat_reset(x_hat);

	for(int i=0; i<6; i++) {
		mat_element_set(x_hat, i, 0, x_hat_ic[i]);
	}
// 	for(int i=0; i<6; i++) {
// 		mat_element_set(P, i, i, x_hat_ic[i]*x_hat_ic[i]);
// 	}

	mat_reset(P);

	mat_element_set(P, 0, 0, mat_element_get(R, 0, 0));
	mat_element_set(P, 1, 1, mat_element_get(R, 1, 1));
	mat_element_set(P, 5, 5, mat_element_get(R, 2, 2));

	first_run = true;

}
