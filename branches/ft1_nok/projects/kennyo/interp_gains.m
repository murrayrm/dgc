% an attempt to interpolate between 2 gains

des_speed = 5;

lower_speed = 3;
lower_speed_dir = 'gains';
lower_speed_file = 'v_3set1';

upper_speed = 7;
upper_speed_dir = 'gains';
upper_speed_file = 'v_7set2';


% get the lower gains
cd(lower_speed_dir)
run(lower_speed_file)
cd ../

A_l = A;
B_l = B;
C_l = C;
D_l = D;

% get the upper gains
pwd
cd(upper_speed_dir)
run(upper_speed_file)
cd ../

A_h = A;
B_h = B;
C_h = C;
D_h = D;

% get the weighting of the velocities
delta_v_l_h = upper_speed - lower_speed;
lower_weight = (upper_speed - des_speed) / delta_v_l_h
upper_weight = (des_speed - lower_speed) / delta_v_l_h

A = lower_weight * A_l + upper_weight * A_h
B = lower_weight * B_l + upper_weight * B_h
C = lower_weight * C_l + upper_weight * C_h
D = lower_weight * D_l + upper_weight * D_h






