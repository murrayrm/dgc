
#include "stdio.h"
#include "assert.h"

#include "../../ahoward/sensing/stereoFeeder/stereo_log.h"
#include "cv.h"
#include "highgui.h"

int main(int argc, char** argv)
{
  char *filename;
  stereo_log_t *log;
  stereo_log_header_t header;
  stereo_log_tag_t tag;
  int image_size;
  uint8_t *left, *right;

  if (argc < 3)
  {
    printf("usage: %s <FILENAME> <FILE_PREFIX>\n", argv[0]);
    return -1;
  }
  filename = argv[1];

  log = stereo_log_alloc();
  assert(log);

  if (stereo_log_open_read(log, filename, &header) != 0)
    return -1;

  printf("version: %X\n", header.version);
  printf("image: %d %d %d\n", header.cols, header.rows, header.channels);

  image_size = header.cols * header.rows * header.channels;
  left = (uint8_t*)malloc(image_size);
  right = (uint8_t*)malloc(image_size);

  int key;
  CvMat image;
  int index=0;
  char fileName[256];

  while (true)
  {
    if (stereo_log_read(log, &tag, image_size, left, right) == 0)
    {
      //show the image
      image = cvMat(header.rows, header.cols, CV_8UC1, left);
      cvNamedWindow("window",CV_WINDOW_AUTOSIZE);
      cvShowImage("window", &image);
      key = cvWaitKey(0);
      cvDestroyWindow("window");
      //cvDestroyHeader
      
      printf("Key:%d ", key);

      //check key
      switch(key)
      {
      case 32: //next image
	break;
      case 101: //save image
	sprintf(fileName, "%s-%03d.png", argv[2], index++);
	cvSaveImage(fileName, &image);
	printf("Written image: %s\n", fileName);
	break;
      }      
    }
    else
    {
      printf("Can't open file\n");
      break;
    }
    printf("image %d %.3f\n",
           tag.frameid, tag.timestamp);
  }

  free(left);
  free(right);
  
  stereo_log_free(log);
  
  return 0;
}
