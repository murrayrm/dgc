/* 
 * Desc: SensNet message types.
 * Date: 09 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#ifndef DGC_SENSNET_TYPES_H
#define DGC_SENSNET_TYPES_H

#include <stdint.h>
#include <string.h>

/** @file

Enumerations and structures for all SensNet blobs.

*/

/// @brief Sensor IDs.
typedef enum _dgc_sensorid_t
{
  /// Null sensor
  DGC_NULL_SENSOR = 0,

  /// "Skynet" sensor (for reading skynet messages through sensnet).
  DGC_SKYNET_SENSOR = 1,

  /// Left-front short range stereo
  DGC_LF_SHORT_STEREO,

  /// Right-front short range stereo
  DGC_RF_SHORT_STEREO,

  /// Middle-front medium range stereo
  DGC_MF_MED_STEREO,

  /// Left-front medium range ladar
  DGC_LF_MED_LADAR,

  /// Right-front medium range ladar
  DGC_RF_MED_LADAR,

  /// Middle-front medium range ladar
  DGC_MF_MED_LADAR,

  /// End-of-list marker
  DGC_LAST_SENSOR
  
} dgc_sensorid_t;


// Macros for creating enum:string lookup tables.
// Based on code on the GnuCash mailing lists.
#define DGC_AS_STRING_CASE(name) case name: return #name;
#define DGC_FROM_STRING_CASE(name) if (strcmp(str, #name) == 0) return name;
#define DGC_DEFINE_LOOKUP(type, name, list)          \
   static const char* name ## _to_string(type n) __attribute__((unused));\
   static const char* name ## _to_string(type n) \
   {                                       \
     switch (n)                            \
     {                                     \
       list(DGC_AS_STRING_CASE)            \
       default: return "";                 \
     }                                     \
   }                                       \
   static type name ## _from_string(const char* str) __attribute__((unused));\
   static type name ## _from_string(const char* str) \
   {                                       \
     list(DGC_FROM_STRING_CASE)            \
     return (type)-1;                      \
   }                                       


/// @brief Convert a sensor id to a string description.
static const char* dgc_sensorid_to_string(dgc_sensorid_t n) __attribute__((unused));

/// @brief Convert a string description to a sensor id
static dgc_sensorid_t dgc_sensorid_from_string(const char *str) __attribute__((unused));

// Lookup table for mapping ids to strings.
#define SENSORID_LIST(_)  \
  _(DGC_NULL_SENSOR)      \
  _(DGC_LF_SHORT_STEREO)  \
  _(DGC_RF_SHORT_STEREO)  \
  _(DGC_MF_MED_STEREO)    \
  _(DGC_LF_MED_LADAR)     \
  _(DGC_RF_MED_LADAR)     \
  _(DGC_MF_MED_LADAR)     
DGC_DEFINE_LOOKUP(dgc_sensorid_t, dgc_sensorid, SENSORID_LIST)


/// @brief Blob types.
typedef enum _dgc_blobtype_t
{
  /// Dummy blob type; used for testing only.
  DGC_NULL_BLOB,
  
  /// Scan data from ladar.
  DGC_LADAR_BLOB,

  /// Combined color/disparity image from stereo.
  DGC_STEREO_BLOB
  
} dgc_blobtype_t;


/// @brief Vehicle state data, added to most blobs.
typedef struct
{
  /// System timestamp: number of seconds since the epoch
  double timestamp;
  
  /// Vehicle pose in local frame.  Stored as (x,y,z,roll,pitch,yaw).
  double pose_local[6];

  /// Vehicle pose in global frame.  Stored as (x,y,z,roll,pitch,yaw).
  double pose_global[6];
  
} dgc_state_t;


/// Maximum image dimensions  
#define DGC_STEREO_BLOB_MAX_COLS 640
#define DGC_STEREO_BLOB_MAX_ROWS 480
  

/// @brief Stereo image blob data.
///
/// The blob contains both color and disparity images for the left camera.
typedef struct
{  
  /// Blob type (must be DGC_STEREO_BLOB)
  int blob_type;

  /// Sensor ID for originating sensor
  int sensor_id;

  /// Frame id
  int frameid;

  /// Image timestamp
  double timestamp;

  /// Vehicle state data.  Note that the state data may not be
  /// perfectly sync'ed with the image data, and that the image
  /// timestamp may differ slightly from the state timestamp.
  dgc_state_t state;

  /// Camera calibration data: (cx, cy) is the image center in pixels,
  /// (sx, sy) is the focal length in pixels.
  float cx, cy, sx, sy;
  
  // Stereo baseline (m).
  float baseline;

  /// Camera-to-vehicle transformation (homogeneous matrix)
  float cam2veh[4][4];

  /// Disparity scaling factor
  float disp_scale;
  
  /// Image dimensions 
  int cols, rows;

  /// Number of color channels (1 for mono, 3 for RGB)
  int color_channels;

  /// Size of color image data (8-bits/pixel)
  uint32_t color_size;
  
  /// Color/grayscale image data
  uint8_t color_data[DGC_STEREO_BLOB_MAX_COLS * DGC_STEREO_BLOB_MAX_ROWS * 3];

  /// Size of disparity image (16-bits/pixel)
  uint32_t disp_size;

  /// Disparity data
  uint16_t disp_data[DGC_STEREO_BLOB_MAX_COLS * DGC_STEREO_BLOB_MAX_ROWS * 1];
  
} __attribute__((packed)) dgc_stereo_blob_t;

  
/// @brief Fast conversion from disparity to range.
static  __inline__ \
void dgc_stereo_blob_crd_to_xyz(dgc_stereo_blob_t *self,
                                int c, int r, uint16_t d, float *x, float *y, float *z)
{
  *z = self->sx / ((float) (int) d) * self->disp_scale * self->baseline;
  *x = (c - self->cx) / self->sx * *z;
  *y = (r - self->cy) / self->sy * *z;
  return;
}


/// @brief Skynet message with road line data.
///
typedef struct
{
  /// Skynet message type (must be SNroadLine)
  int msg_type;

  /// Image frame id (from the sensor).
  int frameid;

  /// Image timestamp (from the sensor).
  double timestamp;

  /// Vehicle state data
  dgc_state_t state;

  /// List of detected lines, represented by their end-points
  /// in the vehicle frame.
  int num_lines;
  struct {float a[3], b[3];} lines[16];
  
} dgc_roadline_msg_t;



#endif
