/* 
 * Desc: SensNet utility functions
 * Date: 09 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#ifndef DGC_UTILS_H
#define DGC_UTILS_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <sys/time.h>


/** @file

Miscellaneous useful functions and macros.

*/

  
/// @brief Message macro
#define MSG(fmt, ...) \
  (fprintf(stdout, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

/// @brief Error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


/// @brief Get the system time in seconds.
static __inline__ double dgc_gettimeofday() __attribute__((unused));
static __inline__ double dgc_gettimeofday()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);  
  return (double) tv.tv_sec + (double) tv.tv_usec * 1e-6;
}

  
#ifdef __cplusplus
}
#endif

#endif
