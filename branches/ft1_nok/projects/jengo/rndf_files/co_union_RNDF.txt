RNDF_name	co_hill_union_RNDF
/* This goes from Colorado/Michigan to Colorado/Hill to Hill/Union to Union/Michigan (but does not cover Michigan street itself yet) */
num_segments	3
num_zones	0
segment	1		
num_lanes	5		
segment_name	Colorado_Blvd
lane	1.1	/* north-most lane */
num_waypoints	0		
left_boundary	broken_white	
end_lane			
lane		1.2	
num_waypoints	0	
left_boundary	double_yellow
right_boundary	broken_white
end_lane					
lane	1.3	/* this is actually the left-turn lane. going to group it with the lanes below, however. */
num_waypoints	1
left_boundary	double_yellow
right_boundary	broken_white
stop		1.3.1
1.3.1		34.14596	-118.12152
end_lane
lane	1.4	/* the lane we plan to drive in */
num_waypoints	3
left_boundary	broken_white
right_boundary	broken_white
checkpoint	1.4.2	1
exit		1.4.3	2.3.1
exit		1.4.3	2.4.1
1.4.1		34.14591	-118.12596
1.4.2		34.14592	-118.12354
1.4.3		34.14593	-118.12152
end_lane
lane		1.5
num_waypoints	2
left_boundary	broken_white
stop		1.5.2
1.5.1		34.14588	-118.12589
1.5.2		34.14588	-118.12152
end_lane
end_segment
segment		2
num_lanes	4
segment_name	Hill_Ave
lane		2.1	/* western lane */
num_waypoints	0
left_boundary	broken_white
end_lane
lane	2.2
num_waypoints	0
left_boundary	double_yellow
right_boundary	broken_white
end_lane
lane	2.3
num_waypoints	4
left_boundary	double_yellow
right_boundary	broken_white
stop		2.3.4
exit		2.3.4	3.1.1
exit		2.3.4	3.2.1
exit		2.3.4	3.3.1
2.3.1		34.14613	-118.12132
2.3.2		34.14664	-118.12133
2.3.3		34.14732	-118.12133
2.3.4		34.14792	-118.12137
end_lane
lane	2.4
num_waypoints	2
left_boundary	broken_white
stop		2.4.2
2.4.1		34.14612	-118.12127
2.4.2		34.14792	-118.12129
end_lane
end_segment
segment		3
num_lanes	3
segment_name	Union_St
lane		3.1	/* northern lane */
num_waypoints	4
left_boundary	broken_white
checkpoint	3.1.4	2
3.1.1		34.14809	-118.12152
3.1.2		34.14813	-118.12283
3.1.3		34.14815	-118.12416
3.1.4		34.14816	-118.12532
end_lane
lane	3.2
num_waypoints	2
left_boundary	broken_white
right_boundary	broken_white
3.2.1		34.14805	-118.12152
3.2.2		34.14813	-118.12534
end_lane
lane	3.3
num_waypoints	2
right_boundary	broken_white
3.3.1		34.14801	-118.12153
3.3.2		34.14810	-118.12533
end_lane
end_segment
end_file