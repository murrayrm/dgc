#include <iostream>
#include <cassert>
#include "Environment.hh"
using namespace std;

  ///////////
  // Setup //
  ///////////

Environment::Environment(char* rndf_filename)
{
  cout<<"** Environment constructor. **"<<endl;

  this->rndf_filename = rndf_filename;

  states = new double[NUM_STATES];
  
  // all states are possible until proven otherwise
  states[0]=0;
  for (int i=1; i<NUM_STATES; i++) {
    states[i] = 1;
  }

  // everything else is set up in setup()
}

Environment::~Environment() 
{
  cout<<"** Environment destructor **"<<endl;
  
  delete[] rndf_filename;
  cout<<"rndf deleted"<<endl;
  delete ourState;
  cout<<"ourstate deleted"<<endl;
  delete[] states;
  cout<<"states deleted"<<endl;
  delete[] laneBoundaries;
  cout<<"lane boundaries deleted"<<endl;
  /* currSegment, firstRightLane, and currGoal should all be taken care
     of in the RNDF destructor. */
}


void Environment::calcRightLanes()
{

  cout<<endl<<"Calculating right lanes... "<<endl;

  ///////////////////
  // One-Way Roads //
  ///////////////////
  if (isOneWay(currSegment)) {
    firstRightLane = currSegment->getLane(1);
    cout<<"First right lane: lane 1"<<endl;

    // if there's only one lane, the orientation is obvious
    if (currSegment->getNumOfLanes() == 1) {
      leftToRight = true;
      cout<<"Lane numbering is left-to-right."<<endl;
    }

    // need to look at road orientation to determine lane numbering.
    // see 2.3.3 of the DARPA RNDF definition doc for more info
    NEcoord temp = currGoal->getNEcoord();
    temp -= ourState->getPos();
    if (temp.E > 0) { /* -pi/2 < orientation < pi/2 */
      leftToRight = true;
      cout<<"Lane numbering is left-to-right."<<endl;
    }
    else if (temp.E < 0) { /* pi/2 < orientation < -pi/2 */
      leftToRight = false;
      cout<<"Lane numbering is right-to-left."<<endl;
    }
    else if (temp.E == 0) { 
      if (temp.N >= 0) { /* orientation = pi/2 */
	leftToRight = true;
	cout<<"Lane numbering is left-to-right."<<endl;
      }
      else { /* orientation = -pi/2 */
	leftToRight = false;
	cout<<"Lane numbering is right-to-left."<<endl;
      }
    }
  }

  ///////////////////
  // Two-Way Roads //
  ///////////////////
  else {
    
    // look until we find two adjacent lanes with left boundary as double
    // yellow. Then the first lane w/this boundary is the "pivot"
    int pivot = 0;
  
    if (currSegment->getNumOfLanes() == 2) {
      if (currSegment->getLane(1)->getLeftBoundary() == "double_yellow" &&
	  currSegment->getLane(2)->getLeftBoundary() == "double_yellow")
	pivot = 1;
    }
    else {
      for (int i=1; i<currSegment->getNumOfLanes(); i++) {
	if (currSegment->getLane(i)->getLeftBoundary() == "double_yellow" &&
	    currSegment->getLane(i+1)->getLeftBoundary() == "double_yellow") {
	  pivot = i;
	  break;
	}
      }
    }

    if (ourState->getCurrLane()->getLaneID() <= pivot) {
      firstRightLane = currSegment->getLane(pivot);
      leftToRight = false;
      cout<<"First right lane: lane "<<pivot<<endl;
      cout<<"Lane numbering is right-to-left."<<endl;
    }
    else {
      firstRightLane = currSegment->getLane(pivot+1);
      leftToRight = true;
      cout<<"First right lane: lane "<<pivot + 1<<endl;
      cout<<"Lane numbering is left-to-right."<<endl;
    }
  }
}

  //////////////////////
  // Road Information //
  //////////////////////

void Environment::printRoad()
{

  /*
  State ourState = env.getOurState();
    
  cout<<"Segment "<<env.getSegment().getSegmentID()<<":"<<endl;
  
  cout<<endl<<"Northing->"<<endl;

  for (int i=0; i<100; i++) {
    if (i==9) cout<<"1";
    else if (i==19) cout<<"2";
    else if (i==29) cout<<"3";
    else if (i==39) cout<<"4";
    else if (i==49) cout<<"5";
    else if (i==59) cout<<"6";
    else if (i==69) cout<<"7";
    else if (i==79) cout<<"8";
    else if (i==89) cout<<"9";
    else if (i%10==0) cout<<"0";
    else cout<<" ";
  }

  cout<<endl;
  for (int i=0; i<100; i++)
    cout<<"-";

  // lane 1
  cout<<endl;
  for (int i=0; i<100; i++) {

    // alice
    if (ourState.getCurrLane().getLaneID()==1 &&
	ourState.getPos().N==i) {
      cout<<"A->"<<setprecision(2)<<ourState.getVel().N;
      i+=4;
    }

    // other cars
    for (vector<State>::iterator it = env.getVehicles().begin();
	 it != env.getVehicles().end(); it++) {
      if (((State)*it).getCurrLane().getLaneID()==1 &&
	  ((int)((State)*it).getPos().N)==i) {			   
	cout<<((State)*it).getID()<<"->";
	if (((State)*it).stopped()) 
	  cout<<"s";
	else if (((State)*it).inQueue(env.getGoalPos()))
	  cout<<"q";
	else {
	  cout<<setprecision(2)<<((State)*it).getVel().N; i++; 
	}
	i+=3;
      }
    }

    // current goal
    if (env.getGoal().getLaneID() == 1 &&
	env.getGoalPos().N == i) {
      if (env.getGoal().isStopSign())
	cout<<"|exit";
      else // is checkpoint
	cout<<"|C|";
    }
    else 
      cout<<" ";
  }

  for (int j=2; j<=env.getSegment().getNumOfLanes(); j++) {
  
    cout<<endl;
    if (env.getFirstRightLane().getLaneID() == j) {
      for (int i=0; i<100; i++)
	cout<<"=";
    }
    else {
      for (int i=0; i<50; i++)
	cout<<"- ";
    }
    
    // lane j
    cout<<endl;
    for (int i=0; i<100; i++) {

      // alice
      if (ourState.getCurrLane().getLaneID()==j &&
	  ourState.getPos().N==i) {
	cout<<"A->"<<setprecision(2)<<ourState.getVel().N;
	i+=4;
      }

      // other cars
      for (vector<State>::iterator it = env.getVehicles().begin();
	   it != env.getVehicles().end(); it++) {
	if (((State)*it).getCurrLane().getLaneID()==j &&
	    ((int)((State)*it).getPos().N)==i) {
	  cout<<((State)*it).getID()<<"->";
	  if (((State)*it).stopped())
	    cout<<"s";
	  else if (((State)*it).inQueue(env.getGoalPos()))
	    cout<<"q";
	  else {
	    cout<<setprecision(2)<<((State)*it).getVel().N; i++;
	  }
	  i+=3;
	}
      }


      // current goal
      if (env.getGoal().getLaneID() == j &&
	  env.getGoalPos().N == i) {
	if (env.getGoal().isStopSign())
	  cout<<"|exit";
	else // is checkpoint
	  cout<<"|C|";
      }
      else
	cout<<" ";
    }

  }
  
  cout<<endl;
  for (int i=0; i<100; i++)
    cout<<"-";

  cout<<endl<<endl;

  */


}


Lane* Environment::getLeftLane()
{

  int laneID = ourState->getCurrLane()->getLaneID(); 

  // check lane numbering
  if (leftToRight) {
    laneID--;
    if (laneID > 0)
      return currSegment->getLane(laneID);
    else 
      return NULL;
  }

  // right to left
  else {
    laneID++;
    if (laneID <= currSegment->getNumOfLanes())
      return currSegment->getLane(laneID);
    else 
      return NULL;
  }

}

Lane* Environment::getRightLane()
{
  int laneID = ourState->getCurrLane()->getLaneID(); 

  // check lane numbering
  if (leftToRight) {
    laneID++;
    if (laneID <= currSegment->getNumOfLanes())
      return currSegment->getLane(laneID);
    else 
      return NULL;
  }

  // right to left
  else {
    laneID--;
    if (laneID > 0)
      return currSegment->getLane(laneID);
    else 
      return NULL;
  }

}


bool Environment::isOneWay(Segment* segment)
{

  if (segment->getNumOfLanes() == 1)
    return true; 

  for (int i=1; i <= segment->getNumOfLanes(); i++)
    if (segment->getLane(i)->getLeftBoundary() == "double_yellow")
      return false;
  
  return true;

}

bool Environment::isTerminalWaypoint(Waypoint* waypoint)
{

  // check if the it's the last waypoint
  if (waypoint->getWaypointID() == 1)
    return true;

  Lane* lane = rndf.getLane(waypoint->getSegmentID(),waypoint->getLaneID());

  // check if it's the first waypoint
  if (waypoint->getWaypointID() == lane->getNumOfWaypoints())
    return true;

  return false;
}


bool Environment::isLegalLane(Lane* lane)
{
  // check lane numbering
  if (leftToRight) {
    if (lane->getLaneID() >= firstRightLane->getLaneID())
      return true;
    else return false;
  }

  // right to left
  else {
    if (lane->getLaneID() <= firstRightLane->getLaneID())
      return true;
    else return false;

  }
}

bool Environment::toTheRight(Lane* lane)
{

  // check lane numbering
  if (leftToRight) {
    if (lane->getLaneID() >= ourState->getCurrLane()->getLaneID())
      return true;
    else 
      return false;
  }

  else {
    if (lane->getLaneID() <= ourState->getCurrLane()->getLaneID())
      return true;
    else
      return false;
  }

}

bool Environment::toTheRight(int laneID)
{

  // check lane numbering
  if (leftToRight) {
    if (laneID >= ourState->getCurrLane()->getLaneID())
      return true;
    else 
      return false;
  }

  else {
    if (laneID <= ourState->getCurrLane()->getLaneID())
      return true;
    else
      return false;
  }
}


  //////////////
  // Updating //
  //////////////



void Environment::removeVehicle(int ID)
{

  for (vector<State>::iterator it = vehicles.begin();
       it != vehicles.end(); it++) {
    cout<<it->getID();
    if (it->getID() == ID) {
      vehicles.erase(it);
      cout<<"Deleting vehicle "<<ID<<endl;
    }
  }

}


int Environment::calcLane(State& state)
{

  cout<<endl<<"Calculating vehicle lane... "<<endl;

  /* Run a series of tests to see if the state's position is inside
   * a certain lane. Begin with bounding box, then bounding diamond,
   * and finally the crossing test. */

  // the position we're testing
  NEcoord pos = state.getPos();

  int numLanes = currSegment->getNumOfLanes();
  /* stores whether or not a point may be in a lane */
  int isInLane[numLanes];
  for (int i=0; i<numLanes; i++)
    isInLane[i] = 0;
  /* the number of lanes we think the vehicle is in */
  int numMatches = 0;

  //////////////////
  // Bounding Box //
  /////////////////

  cout<<"  Bounding Box test..."<<endl;

  for (int i=0; i<numLanes; i++) {

    vector<NEcoord> temp = laneBoundaries[i];

    double xmin, xmax, ymin, ymax;
    xmin = xmax = temp.at(0).E;
    ymin = ymax = temp.at(0).N;
  
    for (vector<NEcoord>::iterator it = temp.begin();
	 it != temp.end(); it++) {
      if (it->E > xmax)
	xmax = it->E;
      if (it->E < xmin)
	xmin = it->E;
      if (it->N > ymax)
	ymax = it->N;
      if (it->N < ymin)
	ymin = it->N;
    }

    // check if point is inside box
    if (pos.N >= ymin &&
	pos.N <= ymax &&
	pos.E >= xmin &&
	pos.E <= xmax) {
      cout<<"   Match for lane "<<i+1<<".";
      isInLane[i] = 1;
      numMatches++;
    }
  }
  cout<<endl;

  /* check if we've found a match */
  if (numMatches == 0) {
    cout<<"  No lanes found for vehicle "<<state.getID()<<". Assuming the ";
    cout<<"vehicle is off the road."<<endl;
    removeVehicle(state.getID());
    return 0;
  }

  ///////////////////
  // Crossing Test //
  ///////////////////

  /* Note: in this test, we send a ray from the position straight 
   * downwards (in the negative northing direction) and use that to test. */

  cout<<"  Crossing Test..."<<endl;

  for (int i=0; i<numLanes; i++) {
    // don't check lanes that didn't pass bounding test
    if (isInLane[i]) {

      int numCrossings = 0;

      // check each edge of the polygon
      vector<NEcoord> temp = laneBoundaries[i];
      NEcoord point1, point2, prevPoint;
      vector<NEcoord>::iterator it = temp.begin();

      while (true) {
	
	// set the two points
	point1 = *it;
	prevPoint = *(it-1);
	it++;
	if (it != temp.end()) 
	  point2 = *it;
	else {
	  point2 = *temp.begin();
	  it = temp.begin();
	}

	// check for crossing

	// simple case: in between the points
	if (point1.E < pos.E && pos.E < point2.E ||
	    point1.E > pos.E && pos.E > point2.E) {

	  /* the coordinate of the northing crossing */
	  double t = (pos.E - point2.E) / (point1.E - point2.E);
	  double n = t * point1.N + (1-t) * point2.N;
	  if (pos.N == n) { /* we're on boundary, which is good enough */
	    cout<<"    Placing vehicle in lane "<<i+1<<"."<<endl;
	    state.setCurrLane(currSegment->getLane(i+1));
	    return 1;
	  }
	  if (pos.N > n)
	    numCrossings++;
	}
	
	// check for other boundary cases
	if (point1.E == pos.E && point1.N <= pos.N) {
	  if (point1.N == pos.N) {
	    /* we're on a boundary, which is good enough */
	    cout<<"    Placing vehicle in lane "<<i+1<<"."<<endl;
	    state.setCurrLane(currSegment->getLane(i+1));
	    return 1;
	  }
	  if (point2.E == pos.E) {
	    /* possible vertical line */
	    if (point1.N <= pos.N && pos.N <= point2.N ||
		point1.N >= pos.N && pos.N >= point2.N) {
	      /* we're on a boundary, which is good enough */
	      cout<<"    Placing vehicle in lane "<<i+1<<"."<<endl;
	      state.setCurrLane(currSegment->getLane(i+1));
	      return 1;
	    }
	  }
	  if (point2.E > pos.E || prevPoint.E > pos.E)
	    numCrossings++;
	  
	}
	// check if we're done
	if (it == temp.begin())
	  break;
       
      }
      
      // check if the crossing was odd
      if (numCrossings % 2 == 1) {
	cout<<"Placing vehicle in lane "<<i+1<<"."<<endl;
	state.setCurrLane(currSegment->getLane(i+1));
	return 1;
      }
    }
  }

  cout<<"No lanes found for vehicle "<<state.getID()<<". Assuming the ";
  cout<<"vehicle is off the road."<<endl;
  cout<<"Deleting vehicle "<<state.getID()<<endl;
  removeVehicle(state.getID());
  return 0;
}


////////////////
// Evaluating //
////////////////

bool Environment::atGoal()
{
  if (ourState->getPos().distanceTo(currGoal->getNEcoord()) <= 1) {
    cout<<"Goal waypoint ("<<currGoal->getSegmentID()<<".";
    cout<<currGoal->getLaneID()<<"."<<currGoal->getWaypointID();
    cout<<") reached."<<endl;
    return true;
  }

  return false;
  
}


double* Environment::getPossibleStates() 
{ 
  evaluate();
  return states; 
}


void Environment::evaluate()
{

  cout<<"evaluating..."<<endl;

  // want to save a lot of variables that we'll use over and over again
  Lane* currLane = ourState->getCurrLane();
  int currLaneID = currLane->getLaneID();
  int numLanes = currSegment->getNumOfLanes();
  NEcoord ourPos = ourState->getPos();
  NEcoord ourVel = ourState->getVel();
  NEcoord currGoalPos = currGoal->getNEcoord();

  ///////////////////////
  // Initialize states //
  ///////////////////////
  cout<<"Initializing states... "<<endl;

  // all states are possible until proven otherwise
  states[0]=0; // 0 is the "unknown" state
  for (int i=1; i<NUM_STATES; i++) {
    states[i] = 1;
  }

  // unlike the other modes, which are possible until proven
  // otherwise, passing is undesirable until there are no alternatives
  states[5] = 0;

  ///////////////////////
  // Lane restrictions //
  ///////////////////////
  cout<<"setting lane restrictions..."<<endl;

  // if we're in the leftmost or rightmost lane, can't change over
 
  if (currLaneID == numLanes) {
    if (leftToRight)
      states[4] = 0;
    else
      states[3] = 0;
  }

  else if (currLaneID == 1) {
    if (leftToRight)
      states[3] = 0;
    else 
      states[4] = 0;
  }
  
  else if (currLaneID == firstRightLane->getLaneID())
    states[3] = 0;

  ///////////////////////
  // Goal Restrictions //
  ///////////////////////
  cout<<"Setting goal restrictions... "<<endl;

  // check if the current goal is an intersection
  if (currGoal->isStopSign()) {

    // check if we're at the intersection
    if (fabs(currGoalPos.N - ourPos.N) > 1 ||
	fabs(currGoalPos.E - ourPos.E) > 1) {
      // not time to stop yet
      states[1]=0;
    }
  }

  // how close is the goal?  
  // if it is w/in 50 meters, don't change lanes or pass
  // (unless we're not in that lane yet!)
  if (currGoalPos.distanceTo(ourPos) < 50) {
    
    if (currLaneID == currGoal->getLaneID()) {
      states[3] = decOneHalf(states[3]);
      states[4] = decOneHalf(states[4]);
    }
    else if (toTheRight(currGoal->getLaneID())) {
      // need to be in current lane or move to the right
      states[2] = decOneThird(states[2]);
      states[3] = decOneHalf(decOneHalf(states[3]));
    }
    else { /* goal is to the left */
      // need to be in current lane or move to the left
      states[2] = decOneThird(states[2]);
      states[4] = decOneHalf(decOneHalf(states[4]));
    }
  }


  //////////////////////////
  // Vehicle Restrictions //
  //////////////////////////
  cout<<"Setting vehicle restrictions... "<<endl;

  vector<State>::iterator it = vehicles.begin(); 
  while (it!= vehicles.end()) {
    State s = *it; // the vehicle state
    Lane* vehicleLane = s.getCurrLane();
    NEcoord vehiclePos = s.getPos();
    NEcoord vehicleVel = s.getVel();

    // does the vehicle even matter? (is it in a right lane)
    // TODO: take vehicles in left lanes into account somehow
    // really only important when we're passing
    while (!isLegalLane(vehicleLane)) {
      // it's in the left lane, so we don't have to worry about it
      // move on to next vehicle
      it++;
      break;
    }
    if (it == vehicles.end()) break;

    //////////////////////////
    // Vehicle in our lane //
    //////////////////////////
    if (currLaneID == vehicleLane->getLaneID()) {
     
      // if it's in our lane, we don't care if it's behind us
      // TODO: fix this for any orientation, not just North!
      if (ourPos.N < vehiclePos.N) {
	
	// how close we are to the vehicle
	// how fast the vehicle is going
	if (ourPos.distanceTo(vehiclePos) < 30) {
	  
	  if (ourPos.distanceTo(vehiclePos) < 15) {
	    // we really need to move
	    states[2] = decOneHalf(decOneHalf(states[2]));
	  }
	
	  // need to pass
	  if (ourPos.distanceTo(vehiclePos) <= 10 &&
	      currLane->getLaneID() == firstRightLane->getLaneID() &&
	      s.stopped()) {
	    // need to pass the vehicle
	    states[5] = 1;
	  }
	  
	  if (s.inQueue(currGoalPos) &&
	      ourPos.distanceTo(vehiclePos)<5) {
	    // need to queue up behind this car
	    states[1] = 1;
	  }
	  
	  if (ourVel.norm() > (vehicleVel.norm() + 5)) {
	    // we're going much faster than they are
	    states[2] = decOneHalf(states[2]);
	  }
	}
      }
    }

    /////////////////////////////////////////////
    // Vehicle in lane immediately to the left //
    /////////////////////////////////////////////
    else if ( getLeftLane()->getLaneID() == vehicleLane->getLaneID()) {

      // don't change lanes if the vehicle is too close
      if (ourPos.distanceTo(vehiclePos) < 10) {
	states[3] = decOneHalf(decOneHalf(states[3]));
	states[5] = decOneHalf(decOneHalf(states[5]));
      }

      // if the vehicle is close and moving faster than us
      else if (ourVel.norm() < (vehicleVel.norm() - 10)) {
	states[3] = decOneHalf(decOneHalf(states[3]));
        states[5] = decOneHalf(decOneHalf(states[5]));
      }
    }

    //////////////////////////////////////////////
    // Vehicle in lane immediately to the right //
    //////////////////////////////////////////////
    else if ( getRightLane()->getLaneID() == vehicleLane->getLaneID()) {
      
      // don't change lanes if the vehicle is too close and we're behind it
      if (ourPos.distanceTo(vehiclePos) < 15 &&
	  ourPos.N < vehiclePos.N) {
        states[4] = decOneHalf(decOneHalf(states[4]));
      }

      else if (ourPos.distanceTo(vehiclePos) < 10)
	states[4] = decOneHalf(states[4]);

      // if the vehicle is close and moving faster than us
      else if (ourVel.norm() < (vehicleVel.norm() - 10)) {
        states[4] = decOneHalf(decOneHalf(states[4]));
      }
      
    }
    
    // move to next vehicle
    it++;
  }

}

