function [numCars, carIndices, carPositions] = trackCars(filename)
% This functin takes in the bumper ladar raw data file, and outputs
% how many "cars" it has found, and where they are.
% Note: in the data output, any "car" is an untraversable object, but 
%     not necessarily a car (can't tell definitively yet, and would rather
%     have false positives than miss a moving obstacle
% It calls a function called segmentImage(frame) which breaks a ladar image
% up into separate objects


maxTravel = 150;  % maximum distance (in cm) that a car can travel between
                 % two frames and still be considered the same car

raw = load(filename);

[row,col] = size(raw);
timings = raw(:,1);
ranges = raw(:,3:col);

[row,col] = size(ranges);
temp = [1:col];
angles = 2*pi*temp/360;

%for some files, I already know the region of interest
if strcmp(filename,'acr4.raw')
  ranges = ranges(400:700,:);
  [row,col] = size(ranges);
elseif strcmp(filename,'tow2.raw')
    ranges = ranges(800:row,:);
    [row,col] = size(ranges);
elseif strcmp(filename,'2carAcr_1.raw')
    ranges = ranges(3000:3600,:);
    [row,col] = size(ranges);
elseif strcmp(filename,'2carAcr_3.raw')
    ranges = ranges(800:1500,:);
    [row,col] = size(ranges);
elseif strcmp(filename,'2carTow1.raw')
    ranges = ranges(1200:2000,:);
    [row,col] = size(ranges);
elseif strcmp(filename,'2carTow3.raw')
    ranges = ranges(1400:2300,:);
%    ranges = ranges(1400:1401,:);    
    [row,col] = size(ranges);
     

end

%the no-data value is 8191...
for i=1:row
    for j=1:col
        if ranges(i,j)==8191 
            ranges(i,j)=0;
        end
    end
end

carIndices = [1];     % col 1: how many frames car has been in
                     % col 2: last frame it was in
carPositions = [];   % each row gives the locations a car has been identified at
carVelocities = [];  % gives x and y components of car's velocity 
                     % assuming constant velocity model (last - first)

isNew = 1;           % start w/ the assumption that it's a new car
numCars = 0;         % how many identified cars we already have

% so as not to remove functionality...the following are used with the
% convex hull representation of objects, rather than the midpoint
chObjs = [];
chNum = 0;

spaceOccupied = [];
spaceUnknown = []; 


% how many frames we're going to look through
numFrames = row;
for i=1:1:numFrames
%for i=1:1:2
    
    pause(0.05);   % the plotting has issues if it runs too quickly
    [newCars, posCars] = segmentImage(ranges(i,:));
    [numNew, foo] = size(newCars);
    tempNumCars = numCars; %how many new cars in this image
    
    for j=1:1:numNew    % for each car in new frame, want to classify it
        isNew = 1;      % until matched otherwise, assume car is new

        for k=1:1:numCars % want to check new car against each old one
                          % Indices and Positions are initialized when car
                          % added (when isNew == 1)
            oldIndex = carIndices(k,1);   % what slot in the kth row to look for old positions in
            oldRPos = [carPositions(k,4*oldIndex-3), carPositions(k,4*oldIndex-2)];
            oldLPos = [carPositions(k,4*oldIndex-1), carPositions(k,4*oldIndex)];
            newRPos = newCars(j,1:2);
            newLPos = newCars(j,3:4);
            distR = sum((newRPos-oldRPos).^2).^0.5; 
            distL = sum((newLPos-oldLPos).^2).^0.5; 
            
            distTraveled = max(distR,distL);
            
            if distTraveled < maxTravel %they're the same vehicle, so...
                tempcount = carIndices(k,1) + 1;
                carIndices(k,1) = tempcount;   % update the count of how many frames
                carIndices(k,2) = i;                     % update the last-seen frame
                carPositions(k,4*tempcount - 3) = newCars(j,1);   % add in new location
                carPositions(k,4*tempcount - 2) = newCars(j,2);
                carPositions(k,4*tempcount - 1) = newCars(j,3);
                carPositions(k,4*tempcount) = newCars(j,4);
                isNew = 0;                             %this car isn't new
                
            end
        
        end

        if isNew == 1    % if this car doesn't match one we've seen before
            tempNumCars = tempNumCars + 1;   % add to our total of cars that we're tracking
            carIndices(tempNumCars,1) = 1;    % initialize carIndices for this "car"
            carIndices(tempNumCars,2) = i;
            carPositions(tempNumCars,1) = newCars(j,1); % add it to the list
            carPositions(tempNumCars,2) = newCars(j,2);
            carPositions(tempNumCars,3) = newCars(j,3);
            carPositions(tempNumCars,4) = newCars(j,4);

        end
    end
    numCars = tempNumCars; % don't want to update num cars, since the for k=1:1:numCars 
                           % should only loop over cars from PREVIOUS
                           % images
                           

    % ...and, time to plot the "cars" we've tracked
    for l=1:1:numCars
        figure(1);
        hold on;
        carI = carIndices(l,1); % index of the lth car
        carLX = carPositions(l,4*carI-1);  % position of lth car
        carLY = carPositions(l,4*carI);
        carRX = carPositions(l,4*carI-3);
        carRY = carPositions(l,4*carI-2);        
        axis([-6000,6000,0,6000]);
        plot(carLX,carLY,'bo', carRX, carRY, 'ro');
        hold off;
    end
                           
end

%FIXME: obstacles need to expire after a certain amount of time (if they
%haven't been updated in a few seconds, they shouldn't be looked for

%FIXME: add basic velocity/prediction calculations

%FIXME: need to find way to group points moving together OR ignore points
%from the more oblique side of the car (currently, it can decide which
%corner is the endpoint, and jumps back and forth)

end