function debugSent()

sent = load('sent.dat');
points = load('points.dat');

aviobj = avifile('09_01_1845.avi','fps',10);

[frames,objs] = size(sent)
colors = rand(objs/2,3);
figure();
%hold on;
for i=1:1:frames
%   pause(.1);   
   xpts = points(2*i - 1,:);
   ypts = points(2*i,:);
%   if mod(i,20)==1 
      plot(xpts,ypts,'k.');
%   end
      hold on;
    
   n = [];
   e = [];
   n1 = [];
   e1 = [];
   n2 = [];
   e2 = [];
   n3 = [];
   e3 = [];
   
   for j=1:1:objs/8
      tn = sent(i,8*j-7);
      te = sent(i,8*j-6);
      tn1 = sent(i,8*j-5);
      te1 = sent(i,8*j-4);
      tn2 = sent(i,8*j-3);
      te2 = sent(i,8*j-2);
      tn3 = sent(i,8*j-1);
      te3 = sent(i,8*j);

      if tn ~= -1
         n = [n,tn];
         e = [e,te];
         n1 = [n1,tn1];
         e1 = [e1,te1];
         n2 = [n2,tn2];
         e2 = [e2,te2];
         n3 = [n3,tn3];
         e3 = [e3,te3];

         %    plot(te,tn,'.','MarkerEdgeColor',colors(j,:));
      end
   end
   
   plot(e,n,'ro');
   plot(e1,n1,'go');
   plot(e2,n2,'bo');
   h = plot(e3,n3,'yo');   
   set(h,'EraseMode', 'xor');
   %for cars moving across
   %axis([39900,399130,3781180,3781400]);
   %for cars moving towards
   axis([398970,399040,3781281,3781320]); 
   %for santa anita
   %axis([403400,403460,3777320,3777410]);
  frame = getframe(gca);
  aviobj = addframe(aviobj, frame);
   drawnow;
   hold off;
end

aviobj = close(aviobj);