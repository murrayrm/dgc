#include <unistd.h>
#include <iostream>
#include <sstream>
#include "dynPlannerModule.hh"
#include "DGCutils"
#include "interface_superCon_pln.hh"

#define MAX_DELTA_SIZE   	    100000
#define PLAN_TIME_TO_TAKE_SNAPSHOT  500000
#define CMAP_FILE                   "testmap"
#define NUM_ROWS                    280
#define NUM_COLS                    80
#define RES_ROWS                    0.4
#define RES_COLS                    0.4

CPlannerModule::CPlannerModule(int sn_key, bool bWaitForStateFill, bool bTakeSnapshots)
	: CSkynetContainer(SNplanner, sn_key),
	  CStateClient(bWaitForStateFill),
	  CSuperConClient("MOM"),
	  m_rddf("rddf.dat"),
	  m_bReceivedAtLeastOneDelta(false),
	  m_snapshotIndex(0),
	  m_bTakeSnapshots(bTakeSnapshots),
	  m_bSnapshotPlaybackMode(!bWaitForStateFill)
{
	readspecs();

	m_pMapDelta = new char[MAX_DELTA_SIZE];

	m_mapRequestSocket = m_skynet.get_send_sock(SNfullmaprequest);


	// **** Initialize the static map (constants in GlobalConstants.h) ****
#ifdef STATIC_MAP_FROM_FILE
	cout << __FILE__ << ":" << __LINE__ << ": Using pre-stored static map" << endl;
	m_map.initMap(0.0,0.0, NUM_ROWS,NUM_COLS, RES_ROWS,RES_COLS, 0);

	// If we're loading the static map from a file, we don't want to wait for the fusion mapper to
	// send deltas. The plan() method waits for this flag to be true before it enters the planning loop.
	m_bReceivedAtLeastOneDelta = true;
#else
	m_map.initMap(CONFIG_FILE_DEFAULT_MAP);
#endif


	// Add the speed limit layer to the map
	m_mapLayer = m_map.addLayer<double>(CONFIG_FILE_COST);


#ifdef STATIC_MAP_FROM_FILE
	// Load a pre-stored map
	m_map.loadLayer<double>(m_mapLayer, CMAP_FILE, true);
	double minval,maxval,nodataval;
	m_map.getMinMax<double>(m_mapLayer, &minval, &maxval, &nodataval);
	cout << __FILE__ << ":" << __LINE__ << ": min, max, nodata: " << minval << ", " << maxval << ", " << nodataval << endl;
#endif


	// **** Initialize the dynamic map ****
	double UTMNorthing = m_rddf.getWaypointNorthing(0);
	double UTMEasting  = m_rddf.getWaypointEasting(0);
	m_dynMap.initMap(UTMNorthing,UTMEasting, NUM_ROWS,NUM_COLS, RES_ROWS,RES_COLS, 0);


	DGCcreateMutex(&m_mapMutex);
	DGCcreateMutex(&m_movingObstMutex);
	DGCcreateMutex(&m_deltaReceivedMutex);
	DGCcreateCondition(&m_deltaReceivedCond);

	m_pPlanner = new CDynPlanner(&m_map, m_mapLayer, &m_dynMap, &m_rddf, false);
        //m_pPlanner = new CDynPlanner(&m_map, m_mapLayer, NULL     , &m_rddf, false);
}




CPlannerModule::~CPlannerModule() 
{
	delete m_pMapDelta;
	delete m_pPlanner;
	DGCdeleteMutex(&m_mapMutex);
	DGCdeleteMutex(&m_deltaReceivedMutex);
	DGCdeleteCondition(&m_deltaReceivedCond);
}




void CPlannerModule::getMapDeltasThread()
{
  // The skynet socket for receiving map deltas (from fusionmapper)
  int mapDeltaSocket = m_skynet.listen(SNfusiondeltamap, SNfusionmapper);
  if(mapDeltaSocket < 0)
    cerr << "CPlannerModule::getMapDeltasThread(): skynet listen returned error" << endl;
  
  while(true)
    {
      int deltasize;
      RecvMapdelta(mapDeltaSocket, m_pMapDelta, &deltasize);
      
      DGClockMutex(&m_mapMutex);
      m_map.applyDelta<double>(m_mapLayer, m_pMapDelta, deltasize);
      DGCunlockMutex(&m_mapMutex);
      //		cerr << "Applied mapdelta" << endl;
      
      // set the condition to signal that the first delta was received
      if(!m_bReceivedAtLeastOneDelta)
	DGCSetConditionTrue(m_bReceivedAtLeastOneDelta, m_deltaReceivedCond, m_deltaReceivedMutex);
    }
}




void CPlannerModule::movingObstacleThread(){
  cerr << "Entering the Moving Obstacle Thread!"<<endl;
  m_movingObstSocket = m_skynet.listen(SNmovingObstacle, MODmovingObstacle);
  movingObstacle newObstacle;
  int numreceived;
  while(true){
    numreceived = m_skynet.get_msg(m_movingObstSocket, &newObstacle, sizeof(movingObstacle), 0);
    if(numreceived>0){
      DGClockMutex(&m_movingObstMutex);
      m_dynMap.update(&newObstacle);
      DGCunlockMutex(&m_movingObstMutex);

      /****** debugging *********/
      /*
      unsigned long long DGCtime;
      DGCgettime(DGCtime);
      double Time = ( (double) DGCtime ) / 1.0e6;

      cerr << setprecision(10);
      cerr << "**********************************************************************************************************" << endl;
      cerr << " Corner position: (" << newObstacle.cornerPos[9].N << "," << newObstacle.cornerPos[9].E << ")" << endl; 
      cerr << " ObstacleID: " << newObstacle.ID << "; TimeStamp: " << newObstacle.timeStamp << endl;
      cerr << " Obstacle sides: (" << newObstacle.side1[9][0] << "," << newObstacle.side1[9][1] << "), ("  
	   << newObstacle.side2[9][0] << "," << newObstacle.side2[9][1] << ")" << endl;
      NEcoord eps(1,1);
      cerr << " Verifying changes to m_dynMap -- " << endl << "getCellData(" << (newObstacle.cornerPos[9] + eps).N << ", "
	   << (newObstacle.cornerPos[9] + eps).E << ", " << Time+0.01 << ") = "
	   << m_dynMap.getCellData(newObstacle.cornerPos[9] + eps, Time+0.01) << endl;
      */
      /************************/
    }
  }

}




void CPlannerModule::PlanningLoop(void) 
{
  int           trajSocket, trajSocketSeed, trajSocketInterm;
  int	        planresult;

  CTraj         prevSuccessfulTraj;
  bool          bPlanFromTraj = false;
  bool          bHaveValidPrevTraj = false;
  bool          bMadeGoodPlan;
  unsigned long long time1, time2;
  
  CTraj*        pPlannerTraj = m_pPlanner->getTraj();
  
  
  DGCgettime(time1);
  
  trajSocket       = m_skynet.get_send_sock(SNtraj);
  trajSocketSeed   = m_skynet.get_send_sock(SNtrajPlannerSeed);
  trajSocketInterm = m_skynet.get_send_sock(SNtrajPlannerInterm);


  cout << "Waiting for a delta map" << endl;
  
  // don't try to plan until at least a single map delta is received
  DGCWaitForConditionTrue(m_bReceivedAtLeastOneDelta, m_deltaReceivedCond, m_deltaReceivedMutex);   
  
  ofstream planfile("logs/pmplans.dat");
  ofstream seedfile("logs/pmseeds.dat");
  ofstream intermfile("logs/pminterms.dat");

  // clear old performance log file
  remove("logs/perflog.dat");
  
  while(true)
    {
	  readspecs();
	  UpdateState();
	  UpdateActuatorState();


	  // plan from veh state if we're not in run || (if we're in run && no plans
	  // came through yet).
	  // plan from traj if we're in run and at least one plan came through
	  
	  // if we're not in RUN, plan from veh state
	  if(m_actuatorState.m_estoppos != RUN)
	    bPlanFromTraj = false;
	  
	  if(DOPLOT)
	    snapshot();
	  
	  VehicleState lookaheadState = m_state;
	  int closestpoint =
	    bHaveValidPrevTraj ? prevSuccessfulTraj.getClosestPoint(m_state.Northing_rear(), m_state.Easting_rear())
	    : 0;
	  
	  unsigned long long timestamp;
	  unsigned long long speedThresholdHysterisisTimestamp;
	  DGCgettime(timestamp);
	  
	  // Compute present time
	  double Time = ( (double) timestamp ) / 1.0e6;

	  if(m_bSnapshotPlaybackMode)
	    {
	      DGClockMutex(&m_mapMutex);
	      planresult = m_pPlanner->plan(&m_state, Time, false, bHaveValidPrevTraj ? &prevSuccessfulTraj : NULL);
	      DGCunlockMutex(&m_mapMutex);
	      bMadeGoodPlan = (planresult == 0 || planresult == 4 || planresult == 9);
	    }
	  else if(NO_LOOKAHEAD 
		  ||
		  !bHaveValidPrevTraj
		  ||
		  !bPlanFromTraj
		  ||
		  (m_state.Speed2() < LOOKAHEAD_SPEED_THRESHOLD && 
		   (timestamp-speedThresholdHysterisisTimestamp)>LOOKAHEAD_SPEED_THRESHOLD_HYSTERISIS)
		  ||
		  hypot(prevSuccessfulTraj.getNorthing(closestpoint) - m_state.Northing_rear(), 
			prevSuccessfulTraj.getEasting(closestpoint) - m_state.Easting_rear()) > LOOKAHEAD_DIST_PROXIMITY
		  ||
		  fabs(hypot(prevSuccessfulTraj.getNorthingDiff(closestpoint,1), 
			     prevSuccessfulTraj.getEastingDiff(closestpoint, 1)) - m_state.Speed2()) > LOOKAHEAD_SPEED_PROXIMITY 
		  )
	    {

	      if(m_state.Speed2() < LOOKAHEAD_SPEED_THRESHOLD)
		speedThresholdHysterisisTimestamp = timestamp;
	      
	      cout << "WHOA!!! FROM STATE" << endl;
	      if(m_state.Speed2() < 1.0)
		{
		  m_state.YawRate = MIN_V / VEHICLE_WHEELBASE * tan(m_actuatorState.m_steerpos * VEHICLE_MAX_AVG_STEER);
		  cout << "using yawrate from actuator: " << m_state.YawRate << endl;
		}
	      DGClockMutex(&m_mapMutex);
	      planresult = m_pPlanner->plan(&m_state, Time, true, NULL);
	      bMadeGoodPlan = (planresult == 0 || planresult == 4 || planresult == 9);
	      
	      if(!bMadeGoodPlan)
		snapshot();
	      
	      DGCunlockMutex(&m_mapMutex);
	    }
	  else
	    {
	      cout << "WHOA!!! FROM TRAJ" << endl;
	      VehicleState lookaheadStateRear = m_state;

	      double lookaheadDist = PLANNING_LOOKAHEAD * m_state.Speed2();
	      
	      int lookaheadPoint = prevSuccessfulTraj.getPointAhead( closestpoint, lookaheadDist );
	      
	      /*******************************
	      lookaheadStateRear.Northing = prevSuccessfulTraj.getNorthingDiff(lookaheadPoint, 0);
	      lookaheadStateRear.Easting  = prevSuccessfulTraj.getEastingDiff (lookaheadPoint, 0);
	      
	      lookaheadStateRear.Vel_N    = prevSuccessfulTraj.getNorthingDiff(lookaheadPoint, 1);
	      lookaheadStateRear.Vel_E    = prevSuccessfulTraj.getEastingDiff (lookaheadPoint, 1);
	      lookaheadStateRear.Yaw      = atan2(lookaheadStateRear.Vel_E, lookaheadStateRear.Vel_N);
	      
	      lookaheadStateRear.Acc_N    = prevSuccessfulTraj.getNorthingDiff(lookaheadPoint, 2);
	      lookaheadStateRear.Acc_E    = prevSuccessfulTraj.getEastingDiff (lookaheadPoint, 2);
	      
	      lookaheadStateRear.YawRate  = lookaheadStateRear.YawRateEstimate();
	      *******************************/

	      DGClockMutex(&m_mapMutex);
	      planresult = m_pPlanner->plan(&lookaheadStateRear, Time, false, &prevSuccessfulTraj);
	      bMadeGoodPlan = (planresult == 0 || planresult == 4 || planresult == 9);
	      
	      if(!bMadeGoodPlan)
		{
		  m_state = lookaheadStateRear;
		  snapshot();
		}
	      
	      DGCunlockMutex(&m_mapMutex);
	      pPlannerTraj->prepend(&prevSuccessfulTraj,
				    max(0, closestpoint-MAX_POINTS_TO_PREPEND), lookaheadPoint-1);
	    }
	  



	  if(SEND_SEED_TRAJ)
	    SendTraj(trajSocketSeed,   m_pPlanner->getSeedTraj());
	  if(LOG_SEED_TRAJ)
	    {
	      m_pPlanner->getSeedTraj()->print(seedfile);
	      seedfile << endl;
	    }
	  

	  if(SEND_INTERM_TRAJ)
	    SendTraj(trajSocketInterm, m_pPlanner->getIntermTraj());
	  if(LOG_INTERM_TRAJ)
	    {
	      m_pPlanner->getIntermTraj()->print(intermfile);
	      intermfile << endl;
	    }
	  


	  if(bMadeGoodPlan)
	    {
	      cerr << "succeeded: " << planresult << endl;
	      prevSuccessfulTraj = *pPlannerTraj;
	      bHaveValidPrevTraj = true;
	      
	      if(SEND_FINAL_TRAJ)
		{
		  SendTraj(trajSocket, pPlannerTraj);
		  
		  scMessage( int( sc_interface::trajInfo ),
			     (int)sc_interface::pln_output_valid_traj,
			     pPlannerTraj->getMinSpeed());
		  
		}
	      
	      if(m_actuatorState.m_estoppos == RUN) {
		bPlanFromTraj = true;
	      }
	    }
	  else
	    {
	      if(bHaveValidPrevTraj) {

		scMessage(int( sc_interface::trajInfo ),
			  (int)sc_interface::pln_failed_deceleration_constraint,	
			  m_pPlanner->getMinSpeedOn(prevSuccessfulTraj));

	      }

	      if(SEND_FINAL_TRAJ && SEND_FAILURES)
		{
		  SendTraj(trajSocket, pPlannerTraj);
		}
	      cerr << "=====================================================================================================" << endl;
	      cerr << "failed: " << planresult << endl;
	    }
	  
	  if(LOG_FINAL_TRAJ)
	    {
	      pPlannerTraj->print(planfile);
	      planfile << endl;
	    }
	  

	  DGCgettime(time2);
	  if(time2 - time1 > PLAN_TIME_TO_TAKE_SNAPSHOT && bMadeGoodPlan)
	    {
	      snapshot();
	    }
	  
	  if(time2 - time1 < MIN_PLANNING_CYCLETIME_US)
	    usleep(MIN_PLANNING_CYCLETIME_US - (time2 - time1));
	  DGCgettime(time1);


       	  //return;

	}
}




void CPlannerModule::requestFullMap()
{
	bool bRequestMap = true;
	m_skynet.send_msg(m_mapRequestSocket,&bRequestMap, sizeof(bool) , 0);
}




void CPlannerModule::snapshot(void)
{
	if(m_bTakeSnapshots)
	{
		ostringstream mapName, stateName;
		mapName   << "map" << m_snapshotIndex;
		stateName << "st"  << m_snapshotIndex;

		m_map.saveLayer<double>(m_mapLayer, mapName.str().c_str(), true);
		ofstream outstate(stateName.str().c_str());
		outstate.write((char*)&m_state, sizeof(m_state));

		m_snapshotIndex++;
	}
}
