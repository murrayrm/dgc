#ifndef _DYNPLANNER_H_
#define _DYNPLANNER_H_

#include "VehicleState.hh"
#include "rddf.hh"
#include "CMap.hh"
#include "CDynMap.hh"
#include "traj.h"
#include "dynRefinementStage.h"
#include "ReactiveStage.h"
#include "CCPlanner.h"
#include "CVelocityPlanner.h"
#include "interface_superCon_pln.hh"


class CDynPlanner
{
	CDynRefinementStage  m_refinementStage; // soon to be defunct ;-)
	CReactiveStage	     m_reactiveStage; // defunct
	CCPlanner            m_spatialPlanner;
  CVelocityPlanner		 m_velocityPlanner; 
	

	ofstream	     m_interm;
	ofstream	     m_after;

	CMap*                m_pMap;
	CDynMap*             m_pDynMap;
	int                  m_layer;

public:

	/**
	   Constructor for the dynamic planner. Same as the old one, but with an extra argument for the
	   dynamic speed map.
	 */
	CDynPlanner(CMap *pMap, int mapLayerID, CDynMap *pDynMap, RDDF* pRDDF, bool USE_MAXSPEED = false, bool bGetVProfile = false);

	CDynPlanner(CMap *pMap, int mapLayerID, RDDF* pRDDF, bool USE_MAXSPEED = false, bool bGetVProfile = false);

	~CDynPlanner();

	/**
	   New plan() method. Takes an argument for the present time coordinate.
	 */
	int plan(VehicleState *pState, double Time, bool bIsStateFromAstate = true, CTraj* pPrevTraj = NULL);

	int plan(VehicleState *pState, bool bIsStateFromAstate = true, CTraj* pPrevTraj = NULL);

	double getVProfile(CTraj* pTraj);

	CTraj* getTraj(void);
	CTraj* getSeedTraj(void);
	CTraj* getIntermTraj(void);

	double getLength(void);
	double getMinSpeedOn(CTraj& traj);
       
};

#endif // _DYNPLANNER_H_
