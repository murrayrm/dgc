#ifndef _REFINEMENTSTAGE_H_
#define _REFINEMENTSTAGE_H_


#include "VehicleState.hh"
#include "rddf.hh"
#include "CMap.hh"
#include "CDynMap.hh"
#include "traj.h"
#include "defs.h"
#include "AliceConstants.h"

extern "C" void dgemv_(char*, int*, int*, double*, double*, int*, double*, int*, double*, double*, int*);
extern "C" void dgemm_(char*, char*, int*, int*, int*, double*, double*, int*, double*, int*, double*, double*, int*);
extern "C" void dgesv_(int*, int* , double *, int* , int *, double *, int*, int*);
extern "C" double ddot_(int*, double*, int*, double*, int*);
extern "C" void dgesvd_(char*, char*, int*, int*, double*, int*, double*, double*, int*, double*, int*, double*, int*, int* );
extern "C" double dscal_(int*, double*, double*, int*);

#define DO_BENCHMARK

class CDynRefinementStage
{
	RDDF*		m_pRDDF;
	CTraj*		m_pTraj;
	CMap*		m_pMap;        // The static map
	int		m_mapLayer;
	CDynMap*        m_pDynMap;     // The dynamic obstacle map

	// The order of the bounds is non-linear {init, traj, final}, linear {init,
	// traj, final}
	double*		m_solverLowerBounds;
	double*		m_solverUpperBounds;
	double*		m_PlannerLowerBounds;
	double*		m_PlannerUpperBounds;

	double *m_pSolverState;
	int inform;

	// Internal SNOPT crap
	char start[8];
	int SNOPTm, SNOPTn;
	int ne;
	int nName;
	int nnobj, nncon, mincw, miniw, minrw;
	int nout, nnjac;
	int iobj, ninf;
	double objadd;
	char prob[8*5];
	double *m_SNOPTmatrix;
	int *ha, *ka;
	double *rc;
	double *pi;
	int *hs, ns;
	double obj, sinf;
	char *cw;
	int *iw;
	double *rw;
	int lencw, leniw, lenrw;
	int PRINT_LEVEL;

	// Everything in the computation is shifted by this many units. Used to move
	// the absolute values of the computation to reasonable values
	double	m_initN, m_initE;
	double  m_initT;
	double	m_initTheta;
	double  m_initYawRate;
	double	m_initSpeed;
	double  m_initAcc;
	double  m_nodataUnpassableCutoffDist;
	int     m_pointsToNodataUnpassableCutoff;

	double	m_targetN, m_targetE;
	double	m_targetTheta;

	double*	m_pColl_theta;
	double*	m_pColl_dtheta;
	double*	m_pColl_ddtheta;
	double* m_pColl_speed;
	double* m_pColl_dspeed;
	double* m_pCollN;
	double* m_pCollE;
	double* m_pCollT;
	double*	m_pCollGradN;
	double*	m_pCollGradE;
	double*	m_pCollGradT;
	double* m_pCollVlimit;
	double* m_pCollDVlimitDN;
	double* m_pCollDVlimitDE;
	double* m_pCollDVlimitDT;
	double* m_pCollDVlimitDTheta;

	double  m_collSF;

	double**m_grad_solver;

	double*	m_valcoeffs, *m_d1coeffs, *m_d2coeffs;
	double*	m_valcoeffsfine, *m_d1coeffsfine, *m_d2coeffsfine;
	double*	m_valcoeffs_speed, *m_d1coeffs_speed;
	double*	m_valcoeffsfine_speed, *m_d1coeffsfine_speed;

	double*	m_dercoeffsmatrix;
	double* m_stateestimatematrix;
	double* m_speedestimatematrixfine;
	double* m_speedestimatematrix;

	double*	m_splinecoeffsTheta;

	// These are used to store the path on fine collocation spacing, and are
	// eventually converted to a CTraj trajectory.
	double* m_pOutputTheta;
	double* m_pOutputDTheta;
	double* m_pOutputSpeed;
	double* m_pOutputDSpeed;

	// these are used to store the theta and speed vectors for seeding. since
	// these two are seeded separately, we can share the memory space
	// They contain thetas/speeds on a fine collocation spacing.
	union
	{
		double*   m_pThetaVector;
		double*   m_pSpeedVector;
	};

	CTraj *m_pSeedTraj;

	/**
	   the nominal planning distance
	*/
	double PLANNING_TARGET_DIST;

	/**
	   the length of the SNOPT state vector
	*/
	int NPnumVariables;

	/**
	   numbers of constraints
	*/
	int	NUM_LIN_INIT_CONSTR, \
	        NUM_LIN_TRAJ_CONSTR, \
	        NUM_LIN_FINL_CONSTR, \
	        NUM_NLIN_INIT_CONSTR, \
	        NUM_NLIN_TRAJ_CONSTR, \
	        NUM_NLIN_FINL_CONSTR;

	string m_specsName;
	bool m_USE_MAXSPEED;

	/**
	   whether we want to actuallty run the solver or to just take the seed
	*/
	bool m_bOnlySeed;

	int  m_kernelWidth;
	int  m_failureCount;
	bool m_bDropKernel;

	/**
	   Transforms the internal representation of the solution trajectory to the CTraj form that
	   is actually passed on to trajFollower. This function is called from run().
	 */
	void outputTraj(CTraj* pTraj);

	/**
	   Computes the solver bounds
	*/
	void makeSolverBoundsArrays(void);

	/**
	   Seeds the planner state and computes the _planner_ bounds to be used:
	   - Calls SetUpPlannerBounds()
	   - Calls SeedFromTraj(...)
	   - Calls SeedSpeedFromState(...)
	 */
	bool SetUpPlannerBoundsAndSeedCoefficients(CTraj* pSeedTraj);

	/**
	   Computes the steering angle spline coefficients from the current solver state.
	   The coefficients are stored in m_splinecoeffsTheta.
	*/
	void getSplineCoeffsTheta(double* pSolverState);


	/**
	   Computes the N and E coordinates at the collocation points.
	   Also computes the corresponding gradients (with respect to what?).
	   There are to different versions of the code, depending on whether NEW_INTEGRATE_NE is defined
	   or not. I don't know what the difference is, but the do produce different results. Probably
	   safest to stick to the one which was default (with NEW_INTEGRATE_NE not defined).
	*/
	void integrateNE(int collIndex);
	void integrateNEfine(int collIndex, double& outN, double& outE);


	/**
	   Fills out m_pCollT data along the current solver state trajectory.
	   To computes the time at collocation point no. CollIndex+1, it finds the difference between
	   point no. CollIndex and CollIndex+1 and adds this to the time at CollIndex. This function
	   must thus be called sequentially, starting with CollIndex = 0.
	   
	   Should this function also compute the derivative wrt time? Is this m_pCollGradT? In
	   any case, this functionality is not yet implemented.
	   
	   Note: this function does not check that CollIndex < NUM_COLLOCATION_POINTS-1.
	*/
	void integrateT(int collIndex);


	/**
	   Allocates memory and performs some initialization. Called by the constructors.
	*/
	void initialize(char* pDercoeffsFile);

	/**
	   Contains the old constructor code, since that code now is used by
	   two different constructors. Don't wanna duplicate code.
	*/
	void construct(CMap *pMap, int mapLayerID, RDDF* pRDDF, bool USE_MAXSPEED, bool bGetVProfile);

	void SetCollocVars(void);
	void SetUpLinearConstr(void);
	void SetUpPlannerBounds(void);
	void CallSolver(void);

	/**
	   Seeds the spatial part (the first NUM_POINTS elements) of the m_pSolverState vector
	   using a least squares fit (I think) to the CTraj trajectory passed as an argument
	   to this function.
	*/
	bool SeedFromTraj(CTraj* pTraj, double dist = 0.0);
	void setTargetToDistAlongSpline(double distRatio);

	void NonLinearInitConstrFunc(void);
	void NonLinearTrajConstrFunc(int constr_index_snopt, int collIndex);
	void NonLinearFinlConstrFunc(void);


public:
	bool			m_bGetVProfile;

	/**
	   New version of run(). Sets m_initT and calls the old version, since the rest of
	   the code is identical.
	*/
	int run(VehicleState* pVehState, double initT, CTraj* pSeedTraj = NULL, bool bIsStateFromAstate = true);

	/**
	  Old version of run(). Doesn't take input on current time, so it sets m_initT = 0 + ASSUMED_PROCESSING_TIME.
	*/
	int run(VehicleState* pVehState, CTraj* pSeedTraj = NULL, bool bIsStateFromAstate = true);
	void checkDerivatives();

	CTraj* getTraj(void)
	{
		return m_pTraj;
	}

	double getObjective(void)
	{
		return m_obj;
	}

	void funcConstr(double* pSolverState);
	void funcCost  (double* pSolverState);
	double *m_pPrevSolverState;
	double*   m_pConstrData;
	double*   m_pConstrGradData;
	double    m_obj;
	double*   m_pCostGradData;

	/**
	   this function takes in a traj and performs a least-squares fit to make it C^2
	*/
	void MakeTrajC2(CTraj* pTraj);


	/** 
	    This function evaluates the spatial part of the current solver state
	    through the map to seed the temporal part with a least squares fit of the
	    map-based speeds. It does not take into account the time dependent speed
	    limits, since this would require that the speed profile is known already.
	    This function is similar to CDynRefinementStage::SeedFromTraj.
	*/
	void SeedSpeedFromState(void);


	/**
	   Constructor for the dynamic planner. Has an extra argument for the dynamic
	   obstacle map.
	*/
	CDynRefinementStage(CMap *pMap = NULL, int mapLayerID = 0, CDynMap *pDynMap = NULL, RDDF* pRDDF = NULL,
			    bool USE_MAXSPEED = false, bool bGetVProfile = false);

	// I keep the old constructor interface for now so that I won't yet have to change code
	// that calls this. It set m_pDynMap to NULL.
	CDynRefinementStage(CMap *pMap = NULL, int mapLayerID = 0, RDDF* pRDDF = NULL,
			    bool USE_MAXSPEED = false, bool bGetVProfile = false);
	~CDynRefinementStage();


	/**
	   Get the length of the trajectory (along the trajectory)
	*/
	double getLength(void)
	{
		return m_pSolverState[NPnumVariables-1];
	}


	CTraj* getSeedTraj(void);


	/**
	   Computes N, E and T (and their gradients) at the collocation points throughout the trajectory
	   determined by current solver state.
	*/
	void makeCollocationData(double* pSolverState);
};

#endif // _REFINEMENTSTAGE_H_
