#include <math.h>
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

#include "AliceConstants.h"
#include "CMapPlus.hh"
#include "dynRefinementStage.h"
#include "DGCutils"
#include "MapAccess.h"


/*
  Declarations of external function defined in the optimization software libraries (both SNOPT and MINOS)
*/
extern "C" int snoptm_(char *start, int *m, int *n, int *ne,
		       int *nname, int *nncon, int *nnobj, int *nnjac, 
		       int *iobj, double *objadd, char *prob,
		       int (*)(int *, int *, int *, int *, int *, double *, double *, double *,
			       double *, double *, int *, char *, int *, int *, int *, double *, int *, int), 
		       double *a, int *ha, int *ka, double *bl, double *
		       bu, char *names, int *hs, double *xs, double *pi, 
		       double *rc, int *inform__, int *mincw, int *miniw, 
		       int *minrw, int *ns, int *ninf, double *sinf, 
		       double *obj, char *cu, int *lencu, int *iu, int *
		       leniu, double *ru, int *lenru, char *cw, int *lencw, 
		       int *iw, int *leniw, double *rw, int *lenrw, int 
		       start_len, int prob_len, int names_len, int cu_len, int 
		       cw_len);
extern "C" int snopt_(char *, int *, int *, int *,
		      int *, int *, int *, int *, int *, 
		      double *, char *,
		      int (*)(int *, int *, int *, int *, double *x, double *, double *, int *,
			      char *, int *, int *, int *, double *, int *, int),
		      int (*)(int *, int *, double *, double *, double *, int *, char *, int *,
			      int *, int *, double *, int *, int),
		      double *, int *, 
		      int *, double *, double *, char *, int *, 
		      double *, double *, double *, int *, int *, 
		      int *, int *, int *, int *, double *, 
		      double *, char *, int *, int *, int *, double *,
		      int *, char *, int *, int *, int *, double *,
		      int *, int, int, int, int, int);
extern "C" int minoss_(char *, int *, int *, int 
		       *, int *, int *, int *, int *, int *, int 
		       *, double *, char *, double *, int *, int *, 
		       double *, double *, int *, int *, int *, 
		       double *, double *, double *, int *, int *, 
		       int *, int *, double *, double *, double *, 
		       int *, int, int);

extern "C" int snset_ (char *,    			 int *, int *, int *, char *, int *, int *, int *, double *, int *, int, int);
extern "C" int snseti_(char *,    int *, int *, int *, int *, char *, int *, int *, int *, double *, int *, int, int);
extern "C" int snsetr_(char *, double *, int *, int *, int *, char *, int *, int *, int *, double *, int *, int, int);

extern "C" int miopt_ (char *,    			 int *, int *, int *, int);

extern "C" int sninit_(int *, int *, char *, int *, int *, int *, double *, int *, int);
extern "C" int mispec_(int *, int *, int *, int *, int *);
extern "C" int mistart_(int*, int*, int*);

#ifdef USE_MINOS
extern "C" int matmod_(int *ncycle, int *nprob, bool *finish,
		       int *m, int *n, int *nb, int *ne, int *nka, 
		       int *ns, int *nscl, int *nname, double *a, int *
		       ha, int *ka, double *bl, double *bu, double *ascale, 
		       int *hs, int *name1, int *name2, double *x, 
		       double *pi, double *rc, double *z__, int *nwcore)
{ cerr << "planner: matmod called.... this shouldn't happen" << endl; return 0; }
#endif

#ifdef DO_BENCHMARK
unsigned long long costtime = 0;
unsigned long long constrtime = 0;
unsigned long long integrateNEtime = 0;
unsigned long long integrateTtime = 0;
unsigned long long timetotal = 0;
int numMapAccess = 0;
unsigned long long timeMapAccess = 0;
#endif

#define min(a,b) ((a)<(b) ? (a) : (b))


CDynRefinementStage* g_pRefinementStage;


/**
   The constraint function, passed to the numerical optimizer. This function calls a number of other
   functions that perform different parts of the computaions:
   - makeCollocationData()
   - funcConstr()
   - funcCost()

   I'm not sure why it calls funcCost(). It shouldn't be necessary, since that function computes the cost associated with a
   specific trajectory.
*/
#ifdef USE_MINOS
extern "C" int funcon_(int *mode, int *nncon, int *nnjac, 
		       int *nejac, double *pSolverState, double *fCon, double *gCon, int *nstate,
		       int *nprob, double *ru, int *lenru)
#else
int SNOPTfuncConstr(int *mode, int *nncon, int *nnjac, 
		    int *nejac, double *pSolverState, double *fCon, double *gCon, int *nstate,
		    char *cu, int *lencu, int *iu, int *leniu, double *ru, int *lenru, int cu_len)
#endif
{
#ifdef DO_BENCHMARK
	unsigned long long t1, t2;
	DGCgettime(t1);
#endif

	if(memcmp(g_pRefinementStage->m_pPrevSolverState, pSolverState, (*nnjac)*sizeof(pSolverState[0])) != 0)
	{
		memcpy(g_pRefinementStage->m_pPrevSolverState, pSolverState, (*nnjac)*sizeof(pSolverState[0]));
		g_pRefinementStage->makeCollocationData(pSolverState);
		g_pRefinementStage->funcConstr(pSolverState);
		g_pRefinementStage->funcCost(pSolverState);
	}

	if(*mode == 2 || *mode == 0)
		memcpy(fCon, g_pRefinementStage->m_pConstrData, (*nncon)*sizeof(fCon[0]));
	if(*mode == 2 || *mode == 1)
		memcpy(gCon, g_pRefinementStage->m_pConstrGradData, (*nejac)*sizeof(gCon[0]));

#ifdef DO_BENCHMARK
	DGCgettime(t2);
	constrtime += t2-t1;
#endif
	return 0;
}



/**
   The cost function, passed to the numerical optimizer. This function calls a number of other
   functions that perform different parts of the computaions:
   - makeCollocationData()
   - funcConstr()
   - funcCost()

   I'm not sure why it calls funcConstr(). It is possible that funcConstr() updates attributes that funcCost() needs
   for the cost computation.
*/
#ifdef USE_MINOS
extern "C" int funobj_(int *mode, int *nnobj,
		       double *pSolverState, double *fObj, double *gObj, int *nstate,
		       int *nprob, double *ru, int *lenru)
#else
int SNOPTfuncCost(int *mode, int *nnobj,
		  double *pSolverState, double *fObj, double *gObj, int *nstate,
		  char *cu, int *lencu, int *iu, int *leniu, double *ru, int *lenru, int cu_len)
#endif
{
#ifdef DO_BENCHMARK
	unsigned long long t1, t2;
	DGCgettime(t1);
#endif

	if(memcmp(g_pRefinementStage->m_pPrevSolverState, pSolverState, (*nnobj)*sizeof(pSolverState[0])) != 0)
	{
		memcpy(g_pRefinementStage->m_pPrevSolverState, pSolverState, (*nnobj)*sizeof(pSolverState[0]));
		g_pRefinementStage->makeCollocationData(pSolverState);
		g_pRefinementStage->funcConstr(pSolverState);
		g_pRefinementStage->funcCost(pSolverState);
	}

	if(*mode == 2 || *mode == 0)
		*fObj = g_pRefinementStage->m_obj;
	if(*mode == 2 || *mode == 1)
		memcpy(gObj, g_pRefinementStage->m_pCostGradData, (*nnobj)*sizeof(gObj[0]));

#ifdef DO_BENCHMARK
	DGCgettime(t2);
	costtime += t2-t1;
#endif

	return 0;
}




CDynRefinementStage::~CDynRefinementStage()
{
	int i;

	delete m_pTraj;

	// free memory
	delete[] m_SNOPTmatrix;
	delete[] ha;
	delete[] ka;
	delete[] hs;
	delete[] pi;
	delete[] rc;

	delete[] m_pSolverState;
	delete[] m_pPrevSolverState;
	delete[] m_solverLowerBounds;
	delete[] m_solverUpperBounds;

	delete[] m_splinecoeffsTheta;
	delete[] m_PlannerLowerBounds;
	delete[] m_PlannerUpperBounds;

	delete[] m_valcoeffs;
	delete[] m_d1coeffs;
	delete[] m_d2coeffs;
	delete[] m_valcoeffsfine;
	delete[] m_d1coeffsfine;
	delete[] m_d2coeffsfine;
	delete[] m_valcoeffs_speed;
	delete[] m_d1coeffs_speed;
	delete[] m_valcoeffsfine_speed;
	delete[] m_d1coeffsfine_speed;

	delete[] m_stateestimatematrix;
	delete[] m_speedestimatematrix;
	delete[] m_speedestimatematrixfine;

	delete[] m_pColl_theta;
	delete[] m_pColl_dtheta;
	delete[] m_pColl_ddtheta;
	delete[] m_pColl_speed;
	delete[] m_pColl_dspeed;
	delete[] m_pCollN;
	delete[] m_pCollE;
	delete[] m_pCollT;
	delete[] m_pCollGradN;
	delete[] m_pCollGradE;
	delete[] m_pCollGradT;
	delete[] m_pCollVlimit;
	delete[] m_pCollDVlimitDN;
	delete[] m_pCollDVlimitDE;
	delete[] m_pCollDVlimitDT;
	delete[] m_pCollDVlimitDTheta;

	delete[] m_pConstrData;
	delete[] m_pConstrGradData;
	delete[] m_pCostGradData;

	for(i=0;
	    i<NUM_NLIN_INIT_CONSTR +
	      NUM_NLIN_TRAJ_CONSTR +
	      NUM_NLIN_FINL_CONSTR;
	    i++)
	{
		delete[] m_grad_solver[i];
	}
	delete[] m_grad_solver;
	delete[] m_dercoeffsmatrix;

	delete[] m_pOutputTheta;
	delete[] m_pOutputDTheta;
	delete[] m_pOutputSpeed;
	delete[] m_pOutputDSpeed;

	delete[] m_pThetaVector;
	delete m_pSeedTraj;

	delete[] cw;
	delete[] iw;
	delete[] rw;
}




void CDynRefinementStage::outputTraj(CTraj* pTraj)
{
	int i;
	double NEout[6];
	double n,e;


	char notrans = 'N';
	int rows = NUM_COLLOCATION_POINTS_FINE;
	int cols = NUM_POINTS;
	double alpha = 1.0;
	double beta = 0.0;
	int incr = 1;

	// Use the solver state to theta matrices to transform the solver state vector into
	// yaw angles and their derivatives on a fine collocation spacing.
	dgemv_(&notrans, &rows, &cols, &alpha, m_valcoeffsfine, &rows,
				 m_pSolverState, &incr, &beta, m_pOutputTheta, &incr);
	dgemv_(&notrans, &rows, &cols, &alpha, m_d1coeffsfine, &rows,
				 m_pSolverState, &incr, &beta, m_pOutputDTheta, &incr);

	// Compute the speed values along the trajectory on a fine collocation spacing
	cols = NUM_SPEED_SEGMENTS;
	dgemv_(&notrans, &rows, &cols, &alpha, m_valcoeffsfine_speed, &rows,
				 m_pSolverState+NUM_POINTS, &incr, &beta, m_pOutputSpeed, &incr);
	dgemv_(&notrans, &rows, &cols, &alpha, m_d1coeffsfine_speed, &rows,
				 m_pSolverState+NUM_POINTS, &incr, &beta, m_pOutputDSpeed, &incr);


	n = e = 0.0;

	// Compute the spline coefficients corresponding to the steering angle spline
	// along the trajectory. These are needed by integrateNEfine(), which is called
	// further down in this function.
	getSplineCoeffsTheta(m_pSolverState);

	pTraj->startDataInput();

	// Loop through the collocation points (fine spacing) and output N, E and derivatives
	// to pTraj
	for(i=0; i<NUM_COLLOCATION_POINTS_FINE-1; i++)
	{
		m_pOutputTheta[i] += m_initTheta;
		m_pOutputSpeed[i] += m_initSpeed;

		double sin_m_pOutputTheta = sin(m_pOutputTheta[i]);
		double cos_m_pOutputTheta = cos(m_pOutputTheta[i]);

		NEout[0] = m_pSolverState[NPnumVariables-1]*n + m_initN;
		NEout[3] = m_pSolverState[NPnumVariables-1]*e + m_initE;
		NEout[1] = m_pOutputSpeed[i]*cos_m_pOutputTheta;
		NEout[4] = m_pOutputSpeed[i]*sin_m_pOutputTheta;
		NEout[2] = m_pOutputSpeed[i] / m_pSolverState[NPnumVariables-1] *
			( m_pOutputDSpeed[i] * cos_m_pOutputTheta - m_pOutputSpeed[i]*m_pOutputDTheta[i]*sin_m_pOutputTheta );
		NEout[5] = m_pOutputSpeed[i] / m_pSolverState[NPnumVariables-1] *
			( m_pOutputDSpeed[i] * sin_m_pOutputTheta + m_pOutputSpeed[i]*m_pOutputDTheta[i]*cos_m_pOutputTheta );
		pTraj->inputWithDiffs(NEout+0, NEout+3);

		// Integrate through to next point along trajectory on fine collocation spacing
		integrateNEfine(i, n, e);
	}
}




void CDynRefinementStage::MakeTrajC2(CTraj* pTraj)
{
	m_initTheta = atan2(pTraj->getEastingDiff(0, 1), pTraj->getNorthingDiff(0, 1));
	m_initSpeed = hypot(pTraj->getEastingDiff(0, 1), pTraj->getNorthingDiff(0, 1));
	m_initN = pTraj->getNorthingDiff(0, 0);
	m_initE = pTraj->getEastingDiff (0, 0);

	double *pThetaVector = new double[NUM_COLLOCATION_POINTS_FINE];
	double *pSpeedVector = new double[NUM_COLLOCATION_POINTS_FINE];
	double dist = pTraj->getLength();

	pTraj->getThetaVector(dist, NUM_COLLOCATION_POINTS_FINE, pThetaVector, m_initTheta);
	pTraj->getSpeedVector(dist, NUM_COLLOCATION_POINTS_FINE, pSpeedVector, m_initSpeed);

	char notrans = 'N';
	int rows = NUM_POINTS;
	int cols = NUM_COLLOCATION_POINTS_FINE;
	double alpha = 1.0;
	double beta = 0.0;
	int incr = 1;

	dgemv_(&notrans, &rows, &cols, &alpha, m_stateestimatematrix, &rows,
				 pThetaVector, &incr, &beta, m_pSolverState, &incr);

	rows = NUM_SPEED_SEGMENTS;
	dgemv_(&notrans, &rows, &cols, &alpha, m_speedestimatematrixfine, &rows,
				 pSpeedVector, &incr, &beta, m_pSolverState+NUM_POINTS, &incr);
	m_pSolverState[NPnumVariables-1] = dist;


	delete[] pThetaVector;
	delete[] pSpeedVector;

	outputTraj(pTraj);
}




CTraj* CDynRefinementStage::getSeedTraj(void)
{
	return m_pSeedTraj;
}




#define n								m_pCollN[collIndex]
#define e								m_pCollE[collIndex]
/**
   Tis function evaluates the spatial part of the current solver state
   through the map to seed the temporal part with a least squares fit of the
   map-based speeds. This function is similar to CDynRefinementStage::SeedFromTraj
*/
void CDynRefinementStage::SeedSpeedFromState(void)
{
	// first, compute theta(s) on a solver (not fine) collocation spacing.
	int collIndex;
	char notrans = 'N';
	int rows = NUM_COLLOCATION_POINTS;
	int cols = NUM_POINTS;
	double alpha = 1.0;
	double beta = 0.0;
	int incr = 1;

	// Store this theta(s) vector into m_pOutputTheta
	dgemv_(&notrans, &rows, &cols, &alpha, m_valcoeffs, &rows,
				 m_pSolverState, &incr, &beta, m_pOutputTheta, &incr);


	// Now integrate the trajectory to find the map speeds at each location
	collIndex = 0;
	n = 0.0;
	e = 0.0;
	for(collIndex=0; collIndex<NUM_COLLOCATION_POINTS; collIndex++)
	{
		double dummy;

		getContinuousMapValueDiffGrown(m_kernelWidth,
					       m_pMap, m_mapLayer, collIndex>m_pointsToNodataUnpassableCutoff,
					       m_initSpeed,
					       n*m_pSolverState[NPnumVariables-1] + m_initN,
					       e*m_pSolverState[NPnumVariables-1] + m_initE,
					       m_pOutputTheta[collIndex] + m_initTheta,
					       &m_pSpeedVector[collIndex], &dummy, &dummy, &dummy);

		m_pSpeedVector[collIndex] *= SPEED_SEED_SCALE_FACTOR;
		m_pSpeedVector[collIndex] -= (m_initSpeed + SPEED_SEED_SCALE_BIAS);

		if(collIndex != NUM_COLLOCATION_POINTS-1)
			integrateNE(collIndex);
	}

	// Now compute a least squares fit into the speed data
	notrans = 'N';
	rows = NUM_SPEED_SEGMENTS;
	cols = NUM_COLLOCATION_POINTS;
	alpha = 1.0;
	beta  = 0.0;
	incr = 1;

	dgemv_(&notrans, &rows, &cols, &alpha, m_speedestimatematrix, &rows,
				 m_pSpeedVector, &incr, &beta, m_pSolverState+NUM_POINTS, &incr);
}
#undef n
#undef e




bool CDynRefinementStage::SeedFromTraj(CTraj* pTraj, double dist)
{
	double trajlen = pTraj->getLength();

	// if the distance isn't specified, use the whole length of the traj
	// if the traj isn't long enough to accomodate the seed, return an error
	if(dist == 0.0)
		dist = trajlen;
	else if(trajlen < dist)
		return false;

	pTraj->getThetaVector(dist, NUM_COLLOCATION_POINTS_FINE, m_pThetaVector, m_initTheta);

	char notrans = 'N';
	int rows = NUM_POINTS;
	int cols = NUM_COLLOCATION_POINTS_FINE;
	double alpha = 1.0;
	double beta = 0.0;
	int incr = 1;

	dgemv_(&notrans, &rows, &cols, &alpha, m_stateestimatematrix, &rows,
	       m_pThetaVector, &incr, &beta, m_pSolverState, &incr);
	m_pSolverState[NPnumVariables-1] = dist;

	getSplineCoeffsTheta(m_pSolverState);

	setTargetToDistAlongSpline(1.0);

	m_pointsToNodataUnpassableCutoff = (int)
		(m_nodataUnpassableCutoffDist / dist * (double)(NUM_COLLOCATION_POINTS-1));
	cout << "m_pointsToNodataUnpassableCutoff: " << m_pointsToNodataUnpassableCutoff << endl;

	return true;
}

void CDynRefinementStage::setTargetToDistAlongSpline(double distRatio)
{
	int i;
	int finalPoint;

	finalPoint = min(lround(distRatio * (double)NUM_COLLOCATION_POINTS_FINE), NUM_COLLOCATION_POINTS_FINE-1);

	m_targetN = m_targetE = 0.0;
	for(i=0;
			i < finalPoint;
			i++)
	{
		integrateNEfine(i, m_targetN, m_targetE);
	}
	m_targetN *= m_pSolverState[NPnumVariables-1];
	m_targetE *= m_pSolverState[NPnumVariables-1];
	m_targetTheta =
		m_splinecoeffsTheta[3*(NUM_POINTS-2) + 0] + 
		m_splinecoeffsTheta[3*(NUM_POINTS-2) + 1] + 
		m_splinecoeffsTheta[3*(NUM_POINTS-2) + 2];
}



// New version of run
int CDynRefinementStage::run(VehicleState* pVehState, double initT, CTraj* pSeedTraj, bool bIsStateFromAstate)
{
  // Set time at planning instant. Does it depend on whether the state is from astate?
  // Note that we use the estimated time when the plan is ready
  m_initT = initT + ASSUMED_PROCESSING_TIME;

  return run(pVehState, pSeedTraj, bIsStateFromAstate);
}



// pVehState is still the state from the astate
int CDynRefinementStage::run(VehicleState* pVehState, CTraj* pSeedTraj, bool bIsStateFromAstate)
{
#ifdef DO_BENCHMARK
	unsigned long long t1, t2;
	DGCgettime(t1);
	costtime = 0;
	constrtime = 0;
	integrateNEtime = 0;
	integrateTtime = 0;
	timetotal = 0;
	numMapAccess = 0;
	timeMapAccess = 0;
#endif

	PRINT_LEVEL = 0;


	// Calculate the expected state at the beginning of the plan (m_initSpeed, etc.), depending
	// on whether the received current state is from astate.
	double speedAtRearAxle;
	if(bIsStateFromAstate)
	{
		speedAtRearAxle = hypot(pVehState->Vel_N + VEHICLE_WHEELBASE*pVehState->YawRate*sin(pVehState->Yaw), 
					pVehState->Vel_E - VEHICLE_WHEELBASE*pVehState->YawRate*cos(pVehState->Yaw));

		m_initSpeed	= fmax(MIN_V, speedAtRearAxle);
		m_initTheta	= pVehState->Yaw;
		m_initYawRate   = pVehState->YawRate;
		m_initN		= pVehState->Northing - VEHICLE_WHEELBASE*cos(pVehState->Yaw);
		m_initE		= pVehState->Easting  - VEHICLE_WHEELBASE*sin(pVehState->Yaw);

		// note: this is wrong. fucking front state
		m_initAcc       = (pVehState->Vel_N*pVehState->Acc_N + pVehState->Vel_E*pVehState->Acc_E) / speedAtRearAxle;
	}
	else
	{
		speedAtRearAxle = hypot(pVehState->Vel_N, pVehState->Vel_E);

		m_initSpeed	= fmax(MIN_V, speedAtRearAxle);
		m_initTheta	= pVehState->Yaw;
		m_initYawRate   = pVehState->YawRate;
		m_initN		= pVehState->Northing;
		m_initE		= pVehState->Easting;
		m_initAcc       = (pVehState->Vel_N*pVehState->Acc_N + pVehState->Vel_E*pVehState->Acc_E) / speedAtRearAxle;
	}



	double stoppingDistance;
	stoppingDistance = m_initSpeed*m_initSpeed / 2.0 / VEHICLE_MAX_DECEL;
	double desiredHorizon = fmin(fmax(stoppingDistance * PLANNING_HORIZON_TWEAK + m_initSpeed*ASSUMED_PROCESSING_TIME,
					  LEAST_TARGET_DIST), LARGEST_TARGET_DIST);
	if(PLANNING_TARGET_DIST < desiredHorizon)
		PLANNING_TARGET_DIST = desiredHorizon;
	else
		PLANNING_TARGET_DIST += PLANNING_TARGET_DIST_LAG * (desiredHorizon - PLANNING_TARGET_DIST);

	m_nodataUnpassableCutoffDist = fmax(stoppingDistance * UNPASSABLE_DISTANCE_TWEAK,
					    NODATA_UNPASSABLE_CUTOFF_DIST);



	PLANNING_TARGET_DIST += PLANNING_TARGET_DIST_LAG * (desiredHorizon - PLANNING_TARGET_DIST);

	static double laggedSpeed = 1000.0;
	if(m_initSpeed < laggedSpeed)
		laggedSpeed = m_initSpeed;
	else
		laggedSpeed += INITSPEED_LAG * (m_initSpeed - laggedSpeed);

	if(laggedSpeed < MAPSAMPLE_WIDTH_SPEED_12)
		m_kernelWidth = 1;
	else if(laggedSpeed < MAPSAMPLE_WIDTH_SPEED_23)
		m_kernelWidth = 2;
	else
		m_kernelWidth = 3;

	if(m_bDropKernel && m_kernelWidth>1)
	{
	  m_kernelWidth--;
	  m_bDropKernel=false;
	}
	cout << "kernel width: " << m_kernelWidth << endl;




	// if we couldn't set up the problem (probably because we were too close to
	// the end, return. Note that we're passing the astate state into this
	// function
	if(!SetUpPlannerBoundsAndSeedCoefficients(pSeedTraj))
		return -2;

	
	// make the bounds arrays for SNOPT
	makeSolverBoundsArrays();

	// zero out the previous solver state, since there is none
	memset(m_pPrevSolverState, 0, (NPnumVariables)*sizeof(m_pPrevSolverState[0]));


	// Call the numerical optimizer
	if(!m_bOnlySeed)
		CallSolver();
	else
		inform = 0;

	// 	if(inform == 0)
	{
		outputTraj(m_pTraj);
	}




	// mlarsson:
	// Save speed profiles
	ofstream file("../matlab/speed.m");
	cout << __FILE__ << ":" << __LINE__ << ": Speed profile (spline points)" << endl;
	file << "v_spline = [0 ";
	for (int ii=NUM_POINTS; ii<NPnumVariables-1; ii++) {
	  file << " " << m_pSolverState[ii];
	}
	file << "] + " << m_initSpeed << ";" << endl;

	cout << __FILE__ << ":" << __LINE__ << ": Speed profile (coll points)" << endl;
	file << "v_coll = [";
	for (int ii=0; ii<NUM_COLLOCATION_POINTS; ii++) {
	  file << " " << m_pColl_speed[ii];
	}
	file << "];" << endl;
	file << "s_spline = linspace(0,1,length(v_spline));" << endl;
	file << "s_coll = linspace(0,1,length(v_coll));" << endl;
	file << "figure; plot(s_spline,v_spline,'b', s_coll,v_coll,'r');" << endl;
	file.close();

	// mlarsson:
	// Save time coordinates throughout trajectory
	file.open("../matlab/time.m");
	cout << __FILE__ << ":" << __LINE__ << ": Time profile (coll points)" << endl;
	file << "t_coll = [";
	for (int ii = 0; ii<NUM_COLLOCATION_POINTS; ii++) {
	  file << " " << m_pCollT[ii];
	}
	file << "] + " << m_initT << ";" << endl;

	file << "s_coll = linspace(0,1,length(t_coll));" << endl;
	file << "figure; plot(s_coll,t_coll,'*');" << endl;
	file.close();


	// mlarsson:
	// Save speed limits throughout trajectory
	file.open("../matlab/vlimit.m");
	cout << __FILE__ << ":" << __LINE__ << ": Speed limits (coll points)" << endl;
	file << "vlimit_coll = [";
	for (int ii = 0; ii<NUM_COLLOCATION_POINTS; ii++) {
	  //NEcoord point(m_pCollN[ii]+m_initN, m_pCollE[ii]+m_initE);
	  //file << " " << m_pDynMap->getCellData(point, m_initT) * m_pMap->getDataUTM<double>(m_mapLayer,point.N, point.E);
	  file << " " << m_pCollVlimit[ii];
	}
	file << "];" << endl;

	file << "s_coll = linspace(0,1,length(vlimit_coll));" << endl;
	file << "figure; plot(s_coll,vlimit_coll,'*');" << endl;
	file.close();



#ifdef DO_BENCHMARK
	DGCgettime(t2);
	timetotal += t2-t1;

 	cout << setprecision(3);
	if(!m_bOnlySeed)
	{
		cout << "Time integrateNE: " << DGCtimetosec(integrateNEtime) << endl;
		cout << "Time integrateT: " << DGCtimetosec(integrateTtime) << endl;
		cout << "Time cost: " << DGCtimetosec(costtime) << endl;
		cout << "Time constraints: " << DGCtimetosec(constrtime) << endl;
		cout << "Time per MapAccess in us: " << DGCtimetosec(timeMapAccess)/double(numMapAccess)*1.0e6 << endl;
	}

	cout << __FILE__ << ":" << __LINE__ << ":" << endl;
	cout << "   Time total: " << DGCtimetosec(timetotal) << endl;

	// Write performance data to log file. The format is:
	// (planner result)  (total time)  (cost)  (constraints)  (integrateNE)
	//         (integrateT)  (map accesss time)  (nbr of map accesses)  \n
	ofstream logfile("logs/perflog.dat", ios::app);
	logfile << inform << " " << DGCtimetosec(timetotal) << " " << DGCtimetosec(costtime) << " "
		<< DGCtimetosec(constrtime) << " " << DGCtimetosec(integrateNEtime) << " "
		<< DGCtimetosec(integrateTtime) << " " << DGCtimetosec(timeMapAccess) << " " << numMapAccess << endl;
	logfile.close();

#endif

	if(inform == 0 ||
	   inform == 4 ||
	   inform == 9)
	{
		double minspeed = 1.0e8;
		for(int i=0; i<NUM_COLLOCATION_POINTS; i++) {
		  minspeed = fmin(minspeed, m_pCollVlimit[i]);
		}

		m_pTraj->setMinSpeed(minspeed);
		m_failureCount = 0;
		m_bDropKernel = false;
	}
	else
	{
		if(m_kernelWidth > 1 && ++m_failureCount > FAILURES_TO_DROP_KERNEL) {
		  m_bDropKernel = true;
		}
		m_bDropKernel = false;
	}
	return inform;
}




void CDynRefinementStage::checkDerivatives(void)
{
// 	ofstream derdat("derdat");
// 	derdat << setprecision(20);

// 	int i = 1;
// 	double del = 0.001;
// 	double swing = 1.0;

// 	m_pSolverState[i] -= swing;
// 	for(double j=-swing;j<swing;j+=del)
// 	{
// 		m_pSolverState[i] += del;

// 		makeCollocationData(m_pSolverState);
// 		funcConstr();
// 		funcCost();

// 		derdat << m_pSolverState[i] << ' ' << m_obj << ' ' << m_pCostGradData[i] << endl;
// 	}
// 	exit(1);







// 	int i;
// 	double obj;
// 	double* pCostGradData = new double[NPnumVariables];
// 	double* pCostGradComputed = new double[NPnumVariables];

// 	makeCollocationData(m_pSolverState);
// 	funcConstr();
// 	funcCost();

// 	obj = m_obj;
// 	memcpy(pCostGradData, m_pCostGradData, NPnumVariables*sizeof(pCostGradData[0]));

// 	cout << "Objective gradient check: given, computed, %err" << endl;
// 	cout.setf(ios::showpoint);
// 	cout.precision(4);
// 	for(i=0; i<NPnumVariables; i++)
// 	{
// 		cout.width(10);
// 		cout << pCostGradData[i];
// 	}
// 	cout << endl;

// 	for(i=0; i<NPnumVariables; i++)
// 	{
// 		m_pSolverState[i] += CHECKDERIVATIVES_DELTA;

// 		makeCollocationData(m_pSolverState);
// 		funcConstr();
// 		funcCost();

// 		pCostGradComputed[i] = (m_obj - obj) / CHECKDERIVATIVES_DELTA;
// 		cout.width(10);
// 		cout << pCostGradComputed[i];

// 		m_pSolverState[i] -= CHECKDERIVATIVES_DELTA;
// 	}
// 	cout << endl;

// 	for(i=0; i<NPnumVariables; i++)
// 	{
// 		cout.width(10);
// 		cout << fabs((pCostGradComputed[i] - pCostGradData[i]) / pCostGradData[i]);
// 	}
// 	cout << endl;

// 	delete[] pCostGradData;
// 	delete[] pCostGradComputed;
}

void CDynRefinementStage::CallSolver(void)
{
	int isumm = 0;
#ifdef USE_MINOS
	int ispecs = 0;
	mistart_(&PRINT_LEVEL, &isumm, &ispecs );
	mispec_(&ispecs, &PRINT_LEVEL, &isumm, &lenrw, &inform);
#else
	sninit_(&PRINT_LEVEL, &isumm,
					cw, &lencw, iw, &leniw, rw, &lenrw, 8);
#endif

	// parse the specs file
	ifstream specsfile("specsfile");
	string line;

	// do nothing if the specs file wasn't opened properly
	if(specsfile)
	{
		// search until the solver specs tag is found
		do
			getline(specsfile,line);
		while(specsfile && (line != m_specsName));

		getline(specsfile,line);
		while(specsfile && (line != ("END " + m_specsName)))
		{		
			if(line == "print6")
				PRINT_LEVEL = 6;
			else if(line == "print0")
				PRINT_LEVEL = 0;
			else
			{
#ifdef USE_MINOS
				char* pStr = (char*)line.c_str();
				miopt_(pStr, &PRINT_LEVEL, &isumm, &inform, strlen(pStr));
#else
				snset_((char*)line.c_str(), &PRINT_LEVEL, &isumm, &inform,
							 cw, &lencw, iw, &leniw, rw, &lenrw, line.length(), 8);
#endif
			}
			getline(specsfile,line);
		}
	}

	// zero out the top half (slacks) of solverstate. This might not be necessary
	memset(m_pSolverState + NPnumVariables, 0, (NPnumNonLinearConstr + NPnumLinearConstr)*sizeof(m_pSolverState[0]));
	memset(hs, 0, (SNOPTn+SNOPTm)*sizeof(hs[0]));
	memset(pi, 0, SNOPTm*sizeof(pi[0]));

#ifdef USE_MINOS
	int nb = SNOPTm + SNOPTn;
	int mincor;
	minoss_(start, &SNOPTm, &SNOPTn, &nb, &ne, &nName, &nncon, &nnobj, &nnjac, &iobj, &objadd, prob,
					m_SNOPTmatrix, ha, ka, m_solverLowerBounds, m_solverUpperBounds, NULL, NULL,
					hs, m_pSolverState, pi, rc,
					&inform, &minrw,
					&ns, &ninf, &sinf, &obj, rw, &lenrw, 4,8);
#else
	snopt_(start, &SNOPTm, &SNOPTn, &ne, &nName, &nncon, &nnobj, &nnjac, &iobj, &objadd,
				 prob, SNOPTfuncConstr, SNOPTfuncCost,
				 m_SNOPTmatrix, ha, ka, m_solverLowerBounds, m_solverUpperBounds, NULL, hs, m_pSolverState, pi, rc,
				 &inform, &mincw, &miniw, &minrw, &ns, &ninf, &sinf, &obj,
				 NULL, &lencw, NULL, NULL, rw, &lenrw,
				 cw, &lencw, iw, &leniw, rw, &lenrw,
				 8, 8, 8, 8, 8);
#endif

	// check for infeasibilities
	if((inform==0 || inform==4 || inform==9) && sinf>MAX_ALLOWABLE_INFEASIBILITY)
	{
	  inform = -5;
	}

// 	snoptm_(start, &SNOPTm, &SNOPTn, &ne, &nName, &nncon, &nnobj, &nnjac, &iobj, &objadd,
// 				 prob, SNOPTfuncConstrCost,
// 				 m_SNOPTmatrix, ha, ka, m_solverLowerBounds, m_solverUpperBounds, NULL, hs, m_pSolverState, pi, rc,
// 				 &inform, &mincw, &miniw, &minrw, &ns, &ninf, &sinf, &obj,
// 				 (char*)this, &lencw, NULL, NULL, rw, &lenrw,
// 				 cw, &lencw, iw, &leniw, rw, &lenrw,
// 				 8, 8, 8, 8, 8);


	// the following tests whether the gradients are proper
	// 	int iState = 20;
	// 	int iCon;
	// 	int mode = 2;
	// 	double fCon[NPnumNonLinearConstr];
	// 	double gCon[NPnumNonLinearConstr*NPnumVariables];
	// 	ofstream dataout("test.dat");
	//   m_pSolverState[0]=   -0.70204;
	//   m_pSolverState[1]=    0.64412;
	//   m_pSolverState[2]=   -6.06464;
	//   m_pSolverState[3]=    3.17813;
	//   m_pSolverState[4]=   -0.02959;
	//   m_pSolverState[5]=   -0.95691;
	//   m_pSolverState[6]=    1.80859;
	//   m_pSolverState[7]=    0.84552;
	//   m_pSolverState[8]=   -4.46146;
	//   m_pSolverState[9]=    5.67879;
	//   m_pSolverState[10]=    0.0;
	//   m_pSolverState[11]= -16.73304;
	//   m_pSolverState[12]=  -2.15412;
	//   m_pSolverState[13]=  11.57775;
	//   m_pSolverState[14]=   8.61134;
	//   m_pSolverState[15]=   6.07860;
	//   m_pSolverState[16]=   5.00339;
	//   m_pSolverState[17]=   1.33800;
	//   m_pSolverState[18]=  -2.05089;
	//   m_pSolverState[19]=   4.32064;
	//   m_pSolverState[20]=  77.68337;
	// 	for(int i=0; i<10; i++)
	// 		m_pSolverState[i]=0.0;

	// 	for(m_pSolverState[iState] = -0.5;
	// 			m_pSolverState[iState] < 0.5;
	// 			m_pSolverState[iState]+=0.05)
	// 	{
	// 		int nu = NPnumNonLinearConstr;
	// 		SNOPTfuncConstr(&mode, NULL, NULL,  NULL,
	// 										m_pSolverState, fCon, gCon,
	// 										NULL, NULL, NULL, NULL, &COLLOCATION_FACTOR, NULL, NULL, 0);

	// 		dataout << m_pSolverState[iState] << ' ';
	// 		for(iCon = 0; iCon<NPnumNonLinearConstr; iCon++)
	// 		{
	// 			dataout << fCon[iCon] << ' ' << gCon[MATRIX_INDEX(iCon, iState)] << ' ';
	// 		}
	// 		dataout << endl;
	// 	}
}
