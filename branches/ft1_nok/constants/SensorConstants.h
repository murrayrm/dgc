/*
** SensorConstants.h
** Created by: George Hines, 20 March 2005
** Last Modified by: George Hines, 23 March 2005
********************
** This file contains the coordinates of the sensors in the coordinate system of Alice
** It does not include the stereocameras; these read separate calibration files
********************
** Alice's vehicle coordinate system has its origin in the ground plane directly below the center of the front axle
** It is in the North-East-Down System, which means the following:
***** The x-axis points forward
***** The y-axis points starboard (right)
***** The z-axis points down (into the ground)
*/

#ifndef SENSORCONSTANTS_H
#define SENSORCONSTANTS_H

/* LADAR mounting parameters can be found in config files in the ladar
 * driver subdirectory (drivers/ladar/config/ladar/) */

/****************GPS******************/
#define X_GPS           -1.391     // [m]
#define Y_GPS           -0.317     // [m]
#define Z_GPS           -2.501     // [m]

                                   // These constants are necessary for correct operation of the GPS, and are here until a better place can be found for them
#define EARTH_RADIUS  6378137.     // [m]
#define ERROR_FALLOFF .75          // dimensionless
#define ERROR_WEIGHT  2.           // dimensionless
#define ANGLE_CUTOFF  M_PI/6.      // dimensionless
#define EPSILON2 0.00669437999     // First eccentricty of Earth, squared
#define GWGS0 9.7803267714         // Gravity at equator in WGS-84
#define GWGS1 0.00193185138639     // Gravity correction constant in WGS-84
#define EARTH_RATE 0.00007292115   // Angular velocity of earth

/*******************IMU********************/
#define X_IMU                -1.391     // [m]
#define Y_IMU                0.0     // [m]
#define Z_IMU                -2.501     // [m]

#define ROLL_IMU         0.0//-0.004        // roll angle [rad]; by definition to zero
#define PITCH_IMU        -0.066        // pitch angle [rad]; by definition to zero
#define YAW_IMU          0.0        // yaw angle [rad]; by definition

#define X_NOVATEL        -3.550    // x-position of the Novatel
#define Y_NOVATEL        0.0       // y-position of the Novatel
#define Z_NOVATEL        -2.514    // z-position of the Novatel

#endif
