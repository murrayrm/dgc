/* 
 * Desc: SensNet ladar blob
 * Date: 3 Dec 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#ifndef SENSNET_LADAR_H
#define SENSNET_LADAR_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <math.h>
#include "sensnet_types.h"

/** @file

@brief Ladar blob and some useful accessors.

*/

/// @brief Maximum image dimensions
#define SENSNET_LADAR_BLOB_MAX_POINTS 181
  

/// @brief Ladar scan data.
///
typedef struct sensnet_ladar_blob
{  
  /// Blob type (must be SENSNET_LADAR_BLOB)
  int blob_type;

  /// Sensor ID for originating sensor
  int sensor_id;

  /// Scan id
  int scanid;

  /// Image timestamp
  double timestamp;

  /// Vehicle state data.  Note that the state data may not be
  /// perfectly sync'ed with the image data, and that the image
  /// timestamp may differ slightly from the state timestamp.
  sensnet_state_t state;

  /// Sensor-to-vehicle transformation (homogeneous matrix)
  float sens2veh[4][4];

  /// Number of points
  int num_points;
  
  /// Range data
  float points[SENSNET_LADAR_BLOB_MAX_POINTS][2];
  
} sensnet_ladar_blob_t __attribute__((packed));


/// @brief Fast conversion from polar (bearing, range) to (x,y,z) in sensor frame
static  __inline__ \
void sensnet_ladar_blob_polar_to_sensor(sensnet_ladar_blob_t *self,
                                        float pa, float pr,
                                        float *px, float *py, float *pz)
{
  *px = pr * cos(pa);
  *py = pr * sin(pa);
  *pz = 0;  
  return;
}


/// @brief Fast conversion from polar (bearing, range) to (x,y,z) in vehicle frame
static  __inline__ \
void sensnet_ladar_blob_polar_to_vehicle(sensnet_ladar_blob_t *self,
                                         float pa, float pr,
                                         float *px, float *py, float *pz)
{
  float ax, ay, az;
  ax = pr * cos(pa);
  ay = pr * sin(pa);
  az = 0;

  *px = self->sens2veh[0][0]*ax + self->sens2veh[0][1]*ay + self->sens2veh[0][2]*az + self->sens2veh[0][3];
  *py = self->sens2veh[1][0]*ax + self->sens2veh[1][1]*ay + self->sens2veh[1][2]*az + self->sens2veh[1][3];
  *pz = self->sens2veh[2][0]*ax + self->sens2veh[2][1]*ay + self->sens2veh[2][2]*az + self->sens2veh[2][3];
  
  return;
}


#ifdef __cplusplus
}
#endif


#endif
