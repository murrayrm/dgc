@c -*- texinfo -*-

@node framework,traj,intro,Top
@chapter The Falcon Framework

@code{Falcon} evolved from a real-time control library into a general
purpose control library written to facilitate the transition from theory
and simulation to real-time application.  This desire to be both fast and
robust leads to conflicting code requirements.  In some cases, compromises
are made.  In others, two version of the routines exist, one with error 
detection and one without, exist.  All routines share some basic 
features, which are described in this chapter.

@section Function Defninition Convention
Each module in @code{Falcon} has a unique prefix of two to four characters
followed by an underscore.  Data structures are completely capitalized, and
do not necessarily share the same prefix as the functions.  For example, the
matrix computation module uses the prefix @code{mat_} and the data structure 
is a @code{MATRIX}.

The first argument to each function that operates on a data structure is
a pointer to the data structure.  Only constructor functions, functions
which return a new data structure, do not have this form.  This is,
essentially, an object based framework implemented in C.  The language C
was chosen for its portability and universal acceptance.  C++, which
allows methods on objects and class inheritance, is not as widely known.

In keeping with the object based framework, the programmer never directly
accesses the data structure.  Accessor functions are provided.  While this
is not enforced by the compiler, it is a rule that should be followed.
Direct modification of the data structure should be avoided.

@section Memory Management

Memory management in @code{Falcon} is simple.  All @code{malloc} calls
occur in functions that would not normally be called during a real-time
control loop.  The programmer is responsible for freeing all data
structures.  Each module provides commands for freeing its data structures.

All @code{malloc} calls are located in two functions for each module,
@code{*_create} and @code{*_resize}.  The @code{*_create} commands
create an object of a size that is likely to remain constant even as
individual instances of the object change.  The @code{*_resize} 
command changes the size of the object.  For example, @code{mat_create}
returns a new @code{MATRIX} structure, which has 0 rows and 0 columns.
@code{mat_resize} is used to set its size.  Many modules have a 
@code{*_init} command, which allocates the structure and sets its size.
Memory is freed by a call to @code{*_free}.

@section Return Status Values

Not all @code{Falcon} routines return status values, but those that do
follow a common convention.  A return value of 0 indicates success.  
Positive values indicate success, but with a warning.  Negative values
indicate an error.
Routines that return data structures return a pointer to 0 if an error
occurs.

@section File Conventions

All @code{falcon} modules load data from formatted ASCII files.  Some
modules also support some binary file types.  To allow compilation
on PC's running DOS, all @code{Falcon} files follow the 8.3 naming 
convention.  However, data files do not need to confrom to this standard.



