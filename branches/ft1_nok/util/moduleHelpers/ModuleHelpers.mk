MODULEHELPERS_PATH = $(DGC)/util/moduleHelpers

MODULEHELPERS_DEPEND = \
	$(MODULEHELPERS_PATH)/StateClient.cc \
	$(MODULEHELPERS_PATH)/MapdeltaTalker.cc \
	$(MODULEHELPERS_PATH)/StateClient.h \
	$(MODULEHELPERS_PATH)/SkynetContainer.h \
	$(MODULEHELPERS_PATH)/TrajTalker.h \
	$(MODULEHELPERS_PATH)/MapdeltaTalker.h \
	$(MODULEHELPERS_PATH)/Benchmark.hh \
	$(MODULEHELPERS_PATH)/Benchmark.cc \
	$(TRAJLIB) \
	$(SKYNETLIB) \
	$(DGCUTILS)
