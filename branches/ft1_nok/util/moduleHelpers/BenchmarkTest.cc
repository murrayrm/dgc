
#include "Benchmark.hh"


int main()
{
   Benchmark bench1;  //Lossless
  bench1.set_description("Lossless");
  bench1.begin();
  bench1.report();
  bench1.end();
  bench1.report();
  bench1.begin(); 
  sleep(1);
  cout << "just slept" << endl;
  bench1.report(); 
  bench1.begin(); 
  bench1.report(); 
  bench1.begin(); 
  bench1.report(); 
  bench1.begin(); 
  bench1.report(); 
  bench1.begin(); 
  bench1.report(); 

  bench1.end();
  cout << "This should end the long wait" << endl;
  bench1.report();
  bench1.end();
  bench1.end();
  bench1.begin();
  bench1.end();
  bench1.begin();
  bench1.end();
  bench1.begin();
  bench1.end();
  bench1.begin();
  bench1.end();
  bench1.begin();
  bench1.end();
  bench1.report();




 Benchmark bench2;  //lossy
 bench2.set_lossy();
  bench2.set_description("Lossy");
  bench2.begin();
  bench2.report();
  bench2.end();
  bench2.report();
  bench2.begin(); 
  sleep(2);
  cout << "just slept" << endl;
  bench2.report(); 
  bench2.begin(); 
  bench2.report(); 
  bench2.begin(); 
  bench2.report(); 
  bench2.begin(); 
  bench2.report(); 
  bench2.begin(); 
  bench2.report(); 

  bench2.end();
  cout << "This should end the long wait" << endl;
  bench2.report();
  bench2.end();
  bench2.end();
  bench2.begin();
  bench2.end();
  bench2.begin();
  bench2.end();
  bench2.begin();
  bench2.end();
  bench2.begin();
  bench2.end();
  bench2.begin();
  bench2.end();
  bench2.report();

  bench1.report();

cout << "done and quitting" << endl;
  return 0;
}
