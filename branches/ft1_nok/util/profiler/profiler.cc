#include"profiler.hh"

#ifdef DO_PROFILE

#include<iostream>
#include<fstream>
#include<iomanip>
#include<pthread.h>
#include<stdio.h>

#include<time.h>
#include<sys/times.h>

#include<map>
#include<string>
#include<vector>
#include<list>
#include<algorithm>
using namespace std;

/*
**
** Internal Profiling data
**
*/

//Length of profiling running averaging
#define PROFILE_AVG_LENGTH 10


/*
**
** The struct containing all data on a single profile
**
*/
static bool profiler_inited = false;
static pthread_mutex_t profiler_mutex;
static int profiler_autosave_interval;
static pthread_t profiler_thread;
static int profiler_next_id=0;
static double profiler_time=0.0;   //Total time spent by the profiler
static double profiler_resolution=1/(double)CLOCKS_PER_SEC;
static string profiler_cmdline="";
static string profiler_filename="";

struct ProfileData
{
  //Unique identifier for this profile
  int id;
  string name;
  pthread_t thread;     //The thread we were last running on (should be the same the whole time)

  //The relation to other profiles
  int parent_id;  //-1 if no parent found, -2 all parents tested unsuccefully
  int parent_ids_considered;    //Number of parent id:s considered (used to check for next parent)

  //Start time for current run, or -1 if not running
  clock_t start_time;

  //Current accumulated time for this run (ie pause - suspend), =0 if not suspended
  double acc_time;

  //Wrap around buffer of the latest times
  int head;      //Position in the buffer
  double buffer[PROFILE_AVG_LENGTH];

  //Last time
  double last_time;

  //Running average time
  double run_avg_time;

  //Total time
  double total_time;

  //Total number of runs
  int cnt;

  //Average time
  double avg_time;

  ProfileData()
    : id(profiler_next_id++), thread(0), parent_id(-1), parent_ids_considered(0),
      start_time(-1), acc_time(-1), head(0), last_time(0),
      run_avg_time(), total_time(0), cnt(0), avg_time(0)
  {
    for(int i=0; i<PROFILE_AVG_LENGTH; ++i)
      buffer[i]=0;
  }

  ProfileData(const ProfileData &pd)
    : id(pd.id), name(pd.name), thread(pd.thread), parent_id(pd.parent_id),
      parent_ids_considered(pd.parent_ids_considered),
      start_time(pd.start_time), acc_time(pd.acc_time),
      head(pd.head), last_time(pd.last_time), run_avg_time(pd.run_avg_time),
      total_time(pd.total_time), cnt(pd.cnt), avg_time(pd.avg_time)
  {
    for(int i=0; i<PROFILE_AVG_LENGTH; ++i)
      buffer[i]=pd.buffer[i];
  }

//Accessors
  int get_parent() const {
    if(parent_id<0)
      return -1;
    return parent_id;
  }

  double get_hz() const           { return 1/last_time; }
  double get_run_avg_hz() const   { return 1/run_avg_time; }
  double get_avg_hz() const       { return 1/avg_time; }
  double get_last_time() const    { return last_time; }
  double get_run_avg_time() const { return run_avg_time; }
  double get_avg_time() const     { return avg_time; }
  double get_total_time() const   { return total_time; }
  int get_count() const           { return cnt; }
};

struct ThreadData
{
  list<int> ids;       //List of all id:s currently active on this thread
  double last_time;   //Last registered time on this thread (for thread local clock())
};

static map<string, int> profiler_string2id;
static map<pthread_t, ThreadData> profiler_thread_data;
static vector<ProfileData> profiler_data;

void *auto_file_save_thread(void *);


/*
**
** Internal Profiling functions
**
*/
//Returns time in s
static double get_time(clock_t t1, clock_t t2)
{
  if(t2<t1) {   //Wrap around
    return (t2-(t1-0x7fffffff)) / (double)CLOCKS_PER_SEC;
  }
  return (t2-t1) / (double)CLOCKS_PER_SEC;
}
static double get_time(clock_t t1)
{
  return get_time(t1, clock());
}

static void assureInited()
{
  if(profiler_inited)
    return;

  if(pthread_mutex_init(&profiler_mutex, NULL) != 0) {
    cerr<<"Unable to create mutex"<<endl;
    return;
  }
  pthread_mutex_lock(&profiler_mutex);

  profiler_inited = true;
  profiler_autosave_interval=1;

  //Calculate true resolution
  clock_t t1, t2;
  t1=clock();
  while((t2=clock())==t1);
  profiler_resolution = get_time(t1,t2);

  //Get the profiler cmdline
  FILE *file = fopen("/proc/self/cmdline", "rb");
  int c;
  while( (c=fgetc(file)) != EOF) {
    if(c==0) {   //0 menas next argv
      if(profiler_filename.empty())
	profiler_filename = profiler_cmdline;
      c=32;
    }
    profiler_cmdline+=(char)c;
  }
  if(profiler_filename.empty())
    profiler_filename = profiler_cmdline;

  //Process the module name
  if(profiler_filename.find('/')!=string::npos)
    profiler_filename = profiler_filename.substr(profiler_filename.rfind('/')+1);

  char datetime[15];
  time_t t;

  time(&t);

  strftime(datetime, sizeof(datetime), "%y%m%d-%H%M%S", localtime(&t));
  profiler_filename = string("profiler.") + profiler_filename + "." + datetime + ".log";
  
  pthread_create(&profiler_thread, NULL, &auto_file_save_thread, 0);
  pthread_mutex_unlock(&profiler_mutex);
}

/*
**
** Profiler accessor functions
**
*/
static int string2id(const string &name)
{
  map<string, int>::iterator itr = profiler_string2id.find(name);
  if(itr==profiler_string2id.end())
    return -1;
  
  return itr->second;
}

static int add_profile(const string &name)
{
  ProfileData pd;
  pd.name = name;
  profiler_data.push_back(pd);

  profiler_string2id[name] = pd.id;

  if(pd.id!=(int)profiler_data.size()-1)
    cerr<<"Incorrect profiler id created"<<endl;

  return pd.id;
}

/*
**
** Control functions
**
*/

/*
**
** start
**
*/
void CProfile::start(const char *name)
{
  int t = clock();
  assureInited();

  pthread_mutex_lock(&profiler_mutex);

  int id = string2id(name);
  if(id==-1)
    id = add_profile(name);

  ProfileData &pd = profiler_data[id];

  if(pd.acc_time!=-1 || pd.start_time!=-1) {
    pthread_mutex_unlock(&profiler_mutex);
    cerr<<"Unable to starting a already running profile: "<<name<<endl;
    profiler_time += get_time(t);
    return;
  }

  //Add map between profile and thread, and check parenthood
  pd.thread = pthread_self();
  ThreadData &data = profiler_thread_data[ pd.thread ];

  if(pd.parent_id>=0) {     //Parent already assigned... make sure it's still valid
    int next_id=-2;
    list<int>::iterator itr;
    int i=0;
    for(itr = data.ids.begin(); itr!=data.ids.end(); ++itr, ++i) {
      if(*itr==pd.parent_id)
	break;

      if(i == pd.parent_ids_considered)
	next_id = *itr;
    }
    if(itr==data.ids.end()) {   //Parent not found, switch to other parent
      pd.parent_id = next_id;
      ++pd.parent_ids_considered;
    }
  } else if(pd.parent_id==-1) {  //No parent assigned so far, chose the first one
    int next_id=-2;
    list<int>::iterator itr;
    int i=0;
    for(itr = data.ids.begin(); itr!=data.ids.end(); ++itr, ++i) {
      if(i == pd.parent_ids_considered) {
	next_id = *itr;
	break;
      }
    }
  
    pd.parent_id = next_id;
    ++pd.parent_ids_considered;
  }

  //Add id
  data.ids.push_front( pd.id );
  data.last_time = clock()/ (double)CLOCKS_PER_SEC;

  //Initialize time
  pd.acc_time = 0;
  pd.start_time = clock();

  pthread_mutex_unlock(&profiler_mutex);

  profiler_time += get_time(t);
}


/*
**
** resume
**
*/

void CProfile::resume(const char *name)
{
  clock_t t = clock();

  assureInited();

  //get the profile
  pthread_mutex_lock(&profiler_mutex);
  int id = string2id(name);
  if(id==-1) {
    pthread_mutex_unlock(&profiler_mutex);
    cerr<<"Trying to resue none existing profile "<<name<<endl;
    profiler_time += get_time(t);
    return;
  }

  ProfileData &pd = profiler_data[id];

  if(pd.acc_time==-1) {
    pthread_mutex_unlock(&profiler_mutex);
    cerr<<"Trying to resume a none paused profile: "<<name<<endl;
    profiler_time += get_time(t);
    return;
  }

  pd.start_time = clock();
  pthread_mutex_unlock(&profiler_mutex);
  profiler_time += get_time(t);
}


/*
**
** pause
**
*/

void CProfile::pause(const char *name)
{
  clock_t t = clock();

  assureInited();

  //get the profile
  pthread_mutex_lock(&profiler_mutex);
  int id = string2id(name);
  if(id==-1) {
    pthread_mutex_unlock(&profiler_mutex);
    cerr<<"Trying to pause a none existing profile: "<<name<<endl;
    profiler_time += get_time(t);
    return;
  }

  ProfileData &pd = profiler_data[id];

  if(pd.start_time==-1) {
    pthread_mutex_unlock(&profiler_mutex);
    cerr<<"Trying to pause a none running profile: "<<name<<endl;
    profiler_time += get_time(t);
    return;
  }

  if(pd.acc_time==-1)
    pd.acc_time = 0;

  pd.acc_time += get_time(pd.start_time, t);
  pd.start_time=0;
  pthread_mutex_unlock(&profiler_mutex);
  profiler_time += get_time(t);
}


/*
**
** stop
**
*/

void CProfile::stop(const char *name)
{
  clock_t t = clock();

  assureInited();

  //get the profile
  pthread_mutex_lock(&profiler_mutex);
  int id = string2id(name);
  if(id==-1) {
    pthread_mutex_unlock(&profiler_mutex);
    cerr<<"Trying to stop a none existing profile: "<<name<<endl;
    profiler_time += get_time(t);
    return;
  }

  ProfileData &pd = profiler_data[id];

  if(pd.start_time==-1 && pd.avg_time==-1) {
    pthread_mutex_unlock(&profiler_mutex);
    cerr<<"Trying to stop a none running profile: "<<name<<endl;
    profiler_time += get_time(t);
    return;
  }

  pd.acc_time += get_time(pd.start_time, t);
  pd.start_time=-1;

  //Add this data to the table
  double s = pd.acc_time;   //Transform to s

  pd.run_avg_time -= pd.buffer[pd.head]/PROFILE_AVG_LENGTH;
  pd.buffer[pd.head] = s;
  ++pd.head;
  if(pd.head>=PROFILE_AVG_LENGTH)
    pd.head=0;
  if(pd.cnt>=PROFILE_AVG_LENGTH) {
    pd.run_avg_time += s/PROFILE_AVG_LENGTH;
  } else {
    pd.run_avg_time = (pd.run_avg_time*pd.cnt + s)/(pd.cnt+1);
  }

  pd.last_time = s;
  pd.total_time += s;
  pd.avg_time = (pd.avg_time*pd.cnt + s)/(pd.cnt+1);

  pd.cnt++;

  pd.acc_time=-1;   //Reset the timer

  //Add map between profile and thread, and check parenthood
  ThreadData &data = profiler_thread_data[ pthread_self() ];

  //Remove from the list, and make sure parent is still behind
  list<int>::iterator itr_self;
  for(itr_self = data.ids.begin(); itr_self!=data.ids.end(); ++itr_self) {
    if(*itr_self==pd.id)
      break;
  }

  if(itr_self == data.ids.end()) {
    cerr<<"Didn't find self in profile list"<<endl;
  } else {
    if(pd.parent_id>=0) {
      //Make sure parent is still there
      list<int>::iterator itr = itr_self;

      for(; itr!=data.ids.end(); ++itr) {
	if(*itr==pd.parent_id)
	  break;
      }
      if(itr==data.ids.end()) {   //Parent did not exist, mark as invalid
	pd.parent_id=-1;
      }
    }

    //Delete id
    data.ids.erase( itr_self );
  }

  data.last_time = clock()/ (double)CLOCKS_PER_SEC;

  pthread_mutex_unlock(&profiler_mutex);
  profiler_time += get_time(t);
}

/*
**
** Accessor functions
**
*/

/*
**
**Get the currently running freq and other stats
**
*/
double CProfile::get_hz(const char *name)
{
  assureInited();

  pthread_mutex_lock(&profiler_mutex);
  int id = string2id(name);
  if(id==-1) {
    pthread_mutex_unlock(&profiler_mutex);
    cerr<<"Trying to access a none existing profile: "<<name<<endl;
    return -1.0;
  }
  double d = profiler_data[id].get_hz();
  pthread_mutex_unlock(&profiler_mutex);

  return d;
}

double CProfile::get_run_avg_hz(const char *name)
{
  assureInited();

  pthread_mutex_lock(&profiler_mutex);
  int id = string2id(name);
  if(id==-1) {
    pthread_mutex_unlock(&profiler_mutex);
    cerr<<"Trying to access a none existing profile: "<<name<<endl;
    return -1.0;
  }
  double d = profiler_data[id].get_run_avg_hz();
  pthread_mutex_unlock(&profiler_mutex);

  return d;
}

double CProfile::get_avg_hz(const char *name)
{
  assureInited();

  pthread_mutex_lock(&profiler_mutex);
  int id = string2id(name);
  if(id==-1) {
    pthread_mutex_unlock(&profiler_mutex);
    cerr<<"Trying to access a none existing profile: "<<name<<endl;
    return -1.0;
  }
  double d = profiler_data[id].get_avg_hz();
  pthread_mutex_unlock(&profiler_mutex);

  return d;
}

//Returns last time in s
double CProfile::get_last_time(const char *name)
{
  assureInited();

  pthread_mutex_lock(&profiler_mutex);
  int id = string2id(name);
  if(id==-1) {
    pthread_mutex_unlock(&profiler_mutex);
    cerr<<"Trying to access a none existing profile: "<<name<<endl;
    return -1.0;
  }
  double d = profiler_data[id].get_last_time();
  pthread_mutex_unlock(&profiler_mutex);

  return d;
}

//Returns s
double CProfile::get_run_avg_time(const char *name)
{
  assureInited();

  pthread_mutex_lock(&profiler_mutex);
  int id = string2id(name);
  if(id==-1) {
    pthread_mutex_unlock(&profiler_mutex);
    cerr<<"Trying to access a none existing profile: "<<name<<endl;
    return -1.0;
  }
  double d = profiler_data[id].get_run_avg_time();
  pthread_mutex_unlock(&profiler_mutex);

  return d;
}

//Returns s
double CProfile::get_avg_time(const char *name)
{
  assureInited();

  pthread_mutex_lock(&profiler_mutex);
  int id = string2id(name);
  if(id==-1) {
    pthread_mutex_unlock(&profiler_mutex);
    cerr<<"Trying to access a none existing profile: "<<name<<endl;
    return -1.0;
  }
  double d = profiler_data[id].get_avg_time();
  pthread_mutex_unlock(&profiler_mutex);

  return d;
}

double CProfile::get_total_time(const char *name)
{
  assureInited();

  pthread_mutex_lock(&profiler_mutex);
  int id = string2id(name);
  if(id==-1) {
    pthread_mutex_unlock(&profiler_mutex);
    cerr<<"Trying to access a none existing profile: "<<name<<endl;
    return -1.0;
  }
  double d = profiler_data[id].get_total_time();
  pthread_mutex_unlock(&profiler_mutex);

  return d;
}

double CProfile::get_process_time()
{
  double t=0.0;
  for(map<pthread_t, ThreadData>::iterator itr_thread = profiler_thread_data.begin(); itr_thread != profiler_thread_data.end(); ++itr_thread) {
    t += itr_thread->second.last_time;
  }
  return t;

  //return clock()/(double)CLOCKS_PER_SEC;
}


double get_thread_time(pthread_t thread)
{
  return profiler_thread_data[ thread ].last_time;
  //return get_process_time();
}

/*
**
** Output functions
**
*/

void print_id(ostream &os, int id, int level, vector< vector<int> > &child_links, double parent_time)
{
  ProfileData &pd = profiler_data[id];

  for(int i=0; i<level; ++i)   //Fix indention
    os<<"  ";
  os<<setw(16)<<left<<pd.name;
  os<<setprecision(4)<<setw(5)<<right<<(pd.get_total_time() / parent_time * 100)<<"%";
  for(int i=22+2*level; i<30; ++i)   //Fix indention
    os<<" ";

  os<<setprecision(5)<<setw(8)<<right<<pd.get_avg_hz();
  os<<setw(16)<<pd.get_avg_time()*1000;
  os<<setw(8)<<pd.get_count();
  os<<setw(16)<<pd.get_total_time()<<endl;

  //Print children
  if(pd.get_total_time()!=0)
    parent_time = pd.get_total_time();
  for(vector<int>::iterator itr = child_links[id].begin(); itr!=child_links[id].end(); ++itr) {
    print_id(os, *itr, level+1, child_links, parent_time);
  }
}

//When calling this print, the mutex should already be locked!!!
void print(ostream &os)
{
  //Build child reference list from parent references
  vector<int> root;
  vector< vector<int> > child_links;

  child_links.resize( profiler_data.size() );
  for(vector<ProfileData>::iterator itr = profiler_data.begin(); itr!=profiler_data.end(); ++itr) {
    if(itr->parent_id>=0)
      child_links[ itr->parent_id ].push_back(itr->id);
    else
      root.push_back(itr->id);
  }

  os<<"Profiling data collected when running: "<<profiler_cmdline<<endl;
  os<<endl;
  
  //Print data
  //   0         1         2         3         4         5         6         7
  //   01234567890123456789012345678901234567890123456789012345678901234567890123456789
  //   |                             |       |               |       |               |
  os<<"Profile         % of parent         Hz       Time (ms)   Loops  Total Time (s)"<<endl;
  os<<"=============================================================================="<<endl;

  //Print profiles according to their relationship, group by thread
  double proc_time = CProfile::get_process_time();
  for(map<pthread_t, ThreadData>::iterator itr_thread = profiler_thread_data.begin(); itr_thread != profiler_thread_data.end(); ++itr_thread) {
    bool first=true;

    for(vector<int>::iterator itr = root.begin(); itr!=root.end(); ++itr) {
      if(profiler_data[*itr].thread!=itr_thread->first)
      	continue;
      if(first) {
	os<<"Thread "<<setw(9)<<left<<itr_thread->first;
	os<<setprecision(4)<<setw(5)<<right<<(itr_thread->second.last_time / proc_time * 100)<<"%";
	os<<"                                        ";
	os<<setw(16)<<itr_thread->second.last_time<<endl;
      }
      first=false;
      print_id(os, *itr, 1, child_links, itr_thread->second.last_time);
    }
  }
  os<<endl;
  os<<"profiler time consumtion: "<<profiler_time*1000<<" ms ("<<(profiler_time/CProfile::get_process_time()*100)<<"%)"<<endl;
  os<<"Resolution: "<<1/profiler_resolution<<" hz = "<<setprecision(5)<<1000*profiler_resolution<<" ms"<<endl;
}


void CProfile::print(const char *name)
{
  pthread_mutex_lock(&profiler_mutex);

  int id = string2id(name);
  if(id==-1) {
    pthread_mutex_unlock(&profiler_mutex);
    cerr<<"Trying to access a none existing profile: "<<name<<endl;
    return;
  }
  ProfileData &pd = profiler_data[id];

  pthread_mutex_unlock(&profiler_mutex);

  cout<<name<<" running at "<<pd.get_run_avg_hz()<<" hz, "<<pd.get_run_avg_time()<<" s/loop, total time "<<pd.get_total_time()<<" in "<<pd.get_count()<<endl;
}

void CProfile::print()
{
  pthread_mutex_lock(&profiler_mutex);
  ::print(cout);
  pthread_mutex_unlock(&profiler_mutex);
}

void CProfile::fprint(const char *filename)
{
  pthread_mutex_lock(&profiler_mutex);
  ofstream of(filename, ios_base::trunc | ios_base::out);
  ::print(of);
  pthread_mutex_unlock(&profiler_mutex);
}

/*
**
** Auto save functions
**
*/

//auto-save profile data interval in seconds, 0 to deactivate
void CProfile::auto_file_save(int interval)
{
  pthread_mutex_lock(&profiler_mutex);
  profiler_autosave_interval = interval;
  pthread_mutex_unlock(&profiler_mutex);
}

void *auto_file_save_thread(void*)
{
  int wait;
  
  while(1) {
    pthread_mutex_lock(&profiler_mutex);
    wait = profiler_autosave_interval;
    pthread_mutex_unlock(&profiler_mutex);

    if(wait==0)
      wait=1;
   
    usleep(wait*1000000);

    pthread_mutex_lock(&profiler_mutex);
    if(profiler_autosave_interval==0) {
      pthread_mutex_unlock(&profiler_mutex);
      continue;
    }

    clock_t t = clock();

    string tmpfilename = profiler_filename + "~";
    
    rename(profiler_filename.c_str(), tmpfilename.c_str());
    ofstream of(profiler_filename.c_str(), ios_base::trunc | ios_base::out);
    ::print(of);

    profiler_time += get_time(t);

    pthread_mutex_unlock(&profiler_mutex);
  }
  
  return NULL;
}


#endif // DO_PROFILE
