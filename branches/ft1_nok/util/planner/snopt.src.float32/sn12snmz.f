*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*
*     File  sn12snmz.f
*
*     SNOPT with merged user objective and constraint calls,
*
*     snoptm   snwrap
*     sntitl   snInit   snSpec   snMem
*     snset    snseti   snsetr   sngetc   sngeti   sngetr
*
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine snoptm( start, m, n, ne, nName,
     $                   nnCon, nnObj, nnJac,
     $                   iObj, ObjAdd, Prob,
     $                   userfg,
     $                   a, ha, ka, bl, bu, Names,
     $                   hs, xs, pi, rc, 
     $                   inform, mincw, miniw, minrw,
     $                   nS, nInf, sInf, Obj,
     $                   cu, lencu, iu, leniu, ru, lenru, 
     $                   cw, lencw, iw, leniw, rw, lenrw )

      implicit           real (a-h,o-z)
      external           userfg
      character*(*)      start
      character*8        Prob, Names(nName)
      integer            ha(ne), hs(n+m)
      integer            ka(n+1)
      real   a(ne), bl(n+m), bu(n+m)
      real   xs(n+m), pi(m), rc(n+m)

      character*8        cu(lencu), cw(lencw)
      integer            iu(leniu), iw(leniw)
      real   ru(lenru), rw(lenrw)

*     ------------------------------------------------------------------
*     snoptm  is a Fortran subroutine for constrained nonlinear
*     optimization.  The constraints take the form
*
*                            (   x  )
*                      bl <= (      ) <= bu,
*                            ( F(x) )
*
*     where bl and bu are constant lower and upper bounds.
*
*     o If all constraints are linear, F = A x for some sparse matrix A.
*
*     o If all constraints are nonlinear, F = fcon(x) for some vector
*       fcon of smooth functions.
*
*     o In general, there is a mixture of constraints of the form
*                      ( fcon(x1) +  A2 x2 ),
*                      (   A3 x1  +  A4 x2 )
*       where the nonlinear variables x1 must appear first as shown.
*
*     o fcon(x1) and (optionally) its partial derivatives A1(x) are set
*       in subroutine userfg (see below).
*
*     o The matrices A2, A3, A4 and the sparsity pattern of A1(x) are
*       entered column-wise in the arrays a, ha, ka (below).
*
*     o Internally, the constraints are converted into the form
*
*           fcon(x1) +  A2 x2  - s1      = 0,     bl <= ( x ) <= bu    
*             A3 x1  +  A4 x2       - s2 = 0            ( s )
*
*       where s = (s1,s2)  and the components of (x,s) are the
*       variables and slacks respectively.
*
*     ------------------------------------------------------------------
*     NOTE: Before calling snoptm, your calling program must call:
*     call snInit( iPrint, iSumm,
*    $             cw, lencw, iw, leniw, rw, lenrw )
*     This sets the default values of the optional parameters. You can
*     also alter the default values of iPrint and iSumm before snoptm 
*     is used.  iPrint = 0, etc, is OK.
*     ------------------------------------------------------------------
*
*     ON ENTRY:
*
*     start   specifies how a starting basis (and certain other items)
*             are to be obtained.
*             start = 'Cold' means that Crash should be used to choose
*                      an initial basis, unless a basis file is given
*                      via Old basis, Insert or Load in the Specs file.
*             start = 'Basis file' means the same (but is more
*                      meaningful in the latter case).
*             start = 'Warm' means that a basis is already defined in hs
*                      (probably from an earlier call).
*
*     m       is the number of slacks (i.e., general constraints). 
*             For LP, QP or LC  problems this means the number of rows
*             in the constraint matrix A.
*             m > 0.
*
*             For problems with no general constraints, set m = 1 and
*             impose the constraint that the sum of the variables 
*             must lie between plus and minus infinity. This gives
*             A one ``free row'' that will not alter the solution.
*
*     n       is the number of variables, excluding slacks.
*             For LP problems, this is the number of columns in A.
*             n > 0.
*
*     ne      is the number of nonzero entries in A (including the
*             Jacobian for any nonlinear constraints).
*             ne gt 0.
*
*     nName   is the number of column and row names provided in the
*             array  Names.  If nName = 1, there are NO names.
*             Generic names will be used in the printed solution.
*             Otherwise, nName = n+m and all names must be provided.
*
*     nnCon   is the number of nonlinear constraints.
*             nnCon ge 0.
*
*             a nonzero nnCon defines the row dimension of the 
*             constraint Jacobian A1(x) defined in subroutine userfg.
*
*     nnObj   is the number of nonlinear Objective variables.
*             nnObj ge 0.
*
*     nnJac   is the number of nonlinear Jacobian variables.
*             If nnCon = 0, nnJac = 0.
*             if nnCon > 0, nnJac > 0.
*
*             a nonzero nnJac defines the column dimension of the 
*             constraint Jacobian A1(x) defined in subroutine userfg.
*
*     iObj    says which row of A is a free row containing a linear
*             objective vector  c  (iObj = 0 if none).
*             iObj = 0  or  nnCon < iObj le m.
*
*     ObjAdd  is a constant that will be added to the objective.
*             Typically ObjAdd = 0.0d+0.
*
*     Prob    is an 8-character name for the problem, used in the
*             output.  A blank name can be assigned if necessary.
*
*     a(ne)   is the constraint matrix (Jacobian), stored column-wise.
*             Every element of a(*) must be assigned a value.  Elements
*             in the nonlinear part (see NOTE 2 below) can be any dummy
*             value (e.g., zero) since they are initialized by snoptm at
*             the first point that is feasible with respect to the
*             linear constraints.  The linear part of a(*) must contain
*             the constant Jacobian elements. 
*            
*     ha(ne)  is the list of row indices for each nonzero in a(*).
*
*     ka(n+1) is a set of pointers to the beginning of each column of
*             the constraint matrix within a(*) and ha(*).
*             Must have ka(1) = 1 and ka(n+1) = ne+1.
*
*  NOTES:  1. If the problem has a nonlinear objective,
*             the first nnObj columns of a and ha belong to the
*             nonlinear objective variables.
*             Subroutine userfg deals with these variables.
*          
*          2. If the problem has nonlinear constraints,
*             the first nnJac columns of a and ha belong to the
*             nonlinear Jacobian variables, and
*             the first nnCon rows of a and ha belong to the
*             nonlinear constraints.
*             Subroutine userfg deals with these variables and
*             constraints.
*          
*          3. If nnObj > 0 and nnJac > 0, the two sets of
*             nonlinear variables overlap.  The total number of
*             nonlinear variables is nnL = max( nnObj, nnJac ).
*          
*          4. The Jacobian forms the top left corner of a and ha.
*             If a Jacobian column j (1 le j le nnJac) contains
*             any entries a(k), ha(k) associated with nonlinear
*             constraints (1 le ha(k) le nnCon), those entries must
*             come before any other (linear) entries.
*          
*          5. The row indices ha(k) for a column may be in any order
*             (subject to Jacobian entries appearing first).
*             Subroutine userfg must define Jacobian entries in the
*             same order.
*          
*     bl(n+m) is the lower bounds on each variable (x,s).
*
*     bu(n+m) is the upper bounds on each variable (x,s).
*
*     Names(nName) is an character*8 array.
*             If nName =  1, Names is not used.  The printed solution
*             will use generic names for the columns and row.
*             If nName = n+m, Names(j) should contain an 8 character
*             name of the jth variable (j = 1, n+m).
*             If j = n+i, the jth variable is the ith row.
*
*     hs(n+m) sometimes contains a set of initial states for each
*             variable (x, s).  See the following NOTES.
*
*     xs(n+m) is a set of initial values for each variable (x, s).
*
*  NOTES:  1. If start = 'Cold' or 'Basis file' and a BASIS file
*             of some sort is to be input
*             (an OLD BASIS file, INSERT file or LOAD file),
*             hs and xs need not be set at all.
*
*          2. Otherwise, hs(1:n) must be defined for a cold start.
*             If nothing special is known about the problem, or if
*             there is no wish to provide special information,
*             you may set hs(j) = 0, xs(j) = 0.0d+0 for all j=1:n.
*             All variables will be eligible for the initial basis.
*        
*             Less trivially, to say that variable j will probably
*             be equal to one of its bounds,
*             set hs(j) = 4 and xs(j) = bl(j)
*             or  hs(j) = 5 and xs(j) = bu(j) as appropriate.
*        
*          3. For Cold starts with no basis file, a Crash procedure
*             is used to select an initial basis.  The initial basis
*             matrix will be triangular (ignoring certain small
*             entries in each column).
*             The values hs(j) = 0, 1, 2, 3, 4, 5 have the following
*             meaning:
*                
*             hs(j)    State of variable j during Crash
*        
*             0, 1, 3  Eligible for the basis.  3 is given preference.
*             2, 4, 5  Ignored.
*        
*             After Crash, hs(j) = 2 entries are made superbasic.
*             Other entries not selected for the basis are made
*             nonbasic at the value xs(j) if bl(j) <= xs(j) <= bu(j),
*             or at the value bl(j) or bu(j) closest to xs(j).
*
*          4. For Warm starts, all of hs(1:n+m) is assumed to be
*             set to the values 0, 1, 2 or 3 from some previous call.
*        
*     pi(m)   contains an estimate of the vector of Lagrange multipliers
*             (shadow prices) for the NONLINEAR constraints.  The first
*             nnCon components must be defined.  They will be used as
*             lambda in the subproblem objective function for the first
*             major iteration.  If nothing is known about lambda,
*             set pi(i) = 0.0d+0, i = 1 to nnCon.
*
*     nS      need not be specified for Cold starts,
*             but should retain its value from a previous call
*             when a Warm start is used.
*
*
*     ON EXIT:
*
*     hs(n+m) is the final state vector:
*
*                hs(j)    State of variable j    Normal value of xs(j)
*
*                  0      nonbasic               bl(j)
*                  1      nonbasic               bu(j)
*                  2      superbasic             Between bl(j) and bu(j)
*                  3      basic                  ditto
*
*             Very occasionally there may be nonbasic variables for
*             which xs(j) lies strictly between its bounds.
*             If nInf = 0, basic and superbasic variables may be outside
*             their bounds by as much as the Feasibility tolerance.
*             Note that if Scale is specified, the Feasibility tolerance
*             applies to the variables of the SCALED problem. 
*             In this case, the variables of the original problem may be
*             as much as 0.1 outside their bounds, but this is unlikely
*             unless the problem is very badly scaled.
*
*     xs(n+m) contains the final variables and slacks (x, s).
*
*     pi(m)   is the vector of Lagrange multipliers (shadow prices)
*             for the general constraints.
*
*     rc(n+m) is a vector of reduced costs: rc = g - (A -I)'pi, where g
*             is the gradient of the objective if xs is feasible
*             (or the gradient of the Phase-1 objective otherwise).
*             If nInf = 0, the last m entries are pi.
*
*     inform  says what happened; see the User's Guide.
*             A summary of possible values follows:
*
*             Inform   Meaning
*
*                0     Optimal solution found.
*                1     The problem is infeasible.
*                2     The problem is unbounded (or badly scaled).
*                3     Too many iterations.
*                5     The Superbasics limit is too small.
*                4     Final solution is feasible, but final optimality 
*                      could not quite be achieved,
*                6     Subroutine userfg requested termination by
*                      returning mode < 0.
*                7     Subroutine userfg seems to be giving incorrect
*                      gradients.
*                8     Subroutine userfg seems to be giving incorrect
*                      gradients.
*                9     The current point cannot be improved.
*               10     Numerical error in trying to satisfy the linear
*                      constraints (or the linearized nonlinear
*                      constraints).  The basis is very ill-conditioned.
*
*               20     Not enough storage for the basis factorization.
*               21     Error in basis package.
*               22     The basis is singular after several attempts to
*                      factorize it (and add slacks where necessary).
*
*               30     An OLD BASIS file had dimensions that did not
*                      match the current problem.
*               32     System error.  Wrong number of basic variables.
*
*               41     Not enough storage to hold SNOPT local variables.
*               42     Not enough char*8  storage to solve the problem.
*               43     Not enough integer storage to solve the problem.
*               44     Not enough real    storage to solve the problem.
*
*     mincw   says how much character storage is needed to solve the
*             problem.  If inform = 42, the work array cw(lencw) was 
*             too small.  snoptm may be called again with lencw suitably 
*             larger than mincw.
*
*     miniw   says how much integer storage is needed to solve the
*             problem.  If inform = 43, the work array iw(leniw) was too
*             small.  snoptm may be called again with leniw suitably 
*             larger than miniw.  (The bigger the better, since it is
*             not certain how much storage the basis factors need.)
*
*     minrw   says how much real storage is needed to solve the problem.
*             If inform = 44, the work array rw(lenrw) was too small.
*             (See the comments above for miniw.)
*
*     nS      is the final number of superbasics.
*
*     nInf    is the number of infeasibilities.
*
*     sInf    is the sum    of infeasibilities.
*
*     Obj     is the value of the nonlinear part of the objective.
*             If nInf = 0, Obj includes the nonlinear objective if any.
*             If nInf > 0, Obj is just the linear objective if any.
*
*     cu(lencu), iu(leniu), ru(lenru)  are character, integer and real
*             arrays of USER workspace.  These arrays are available to
*             pass data to the user-defined routine userfg.
*             If no workspace is required, you can either use dummy
*             arrays for cu, iu and ru, or use cw, iw and rw
*             (see below).
*
*     cw(lencw), iw(leniw), rw(lenrw)  are character*8, integer and real
*             arrays of workspace used by snoptm.
*             lencw  should be about at least 500.
*             leniw  should be about max( 500, 20(m+n) ) or larger.
*             lenrw  should be about max( 500, 40(m+n) ) or larger.
*
*     31-Oct 1998: First version based on snopt in SNOPT 5.3-4.
*     31-Oct 1998: Current version of snoptm.
*     ==================================================================
      integer            Htype
      parameter         (maxru     =   2)
      parameter         (maxiu     =   4)
      parameter         (maxcu     =   6)

      parameter         (iPrint    =  12)
      parameter         (iSumm     =  13)
      parameter         (neJac     =  20)
      parameter         (mName     =  51)

      external           snwrap
*     ------------------------------------------------------------------

      if (lencw .lt. 500 .or. leniw .lt. 500 .or. lenrw .lt. 500) then 
*        ---------------------------------------------------------------
*        Not enough workspace to do ANYTHING!
*        ---------------------------------------------------------------
         inform = 41
         mincw  = 500
         miniw  = 500
         minrw  = 500
         if (iw(iPrint) .gt. 0) write(iw(iPrint), 9000) 
         if (iw(iSumm ) .gt. 0) write(iw(iSumm ), 9000) 
         if (iw(iPrint) .le. 0  .and.  iw(iSumm)  .le. 0) then
            write(*, 9000)
         end if
         go to 999
      end if

*     Initialize timers and the standard input file.

      call s1time( 0, 0, iw, leniw, rw, lenrw  )
      call s1file( 'Standard input', iw, leniw )

      ierror     = 0

*     Load the iw array with various problem dimensions.

      iw( 15)    = m
      iw( 16)    = n
      iw( 17)    = ne
      iw( 21)    = nnCon
      iw( 23)    = nnObj
      iw( 22)    = nnJac
      iw(218)    = iObj

*     The obligatory call to snInit has already set the defaults.
*     Check that the optional parameters have sensible values.

      call s8dflt( 'Check optional parameters', 
     $             cw, lencw, iw, leniw, rw, lenrw )

*     ------------------------------------------------------------------
*     Print the options if iPrint > 0, Print level > 0 and lvlPrm > 0.
*     ------------------------------------------------------------------
      call s8dflt( 'Print the options', 
     $             cw, lencw, iw, leniw, rw, lenrw )

*     ------------------------------------------------------------------
*     Compute the storage requirements for snoptm  from the following
*     variables:
*         m,      n,    ne
*         maxR ,  maxS, nnL
*         nnObj,
*         neJac, nnCon, nnJac
*     All have to be known exactly before calling s8Mem.
*     The only one in doubt is neJac, the number of Jacobian elements.
*     Count them here.
*     ------------------------------------------------------------------
      iw(neJac) = 0
      if (nnCon .gt. 0) then
         last = ka(nnJac+1) - 1
         if (nnCon .eq. m) then
            iw(neJac) = last
         else
            do 100, k = 1, last
               i      = ha(k)
               if (i .le. nnCon) iw(neJac) = iw(neJac) + 1
  100       continue         
         end if
      end if

      iw(neJac) = max( 1, iw(neJac) )
 
*     Fetch the limits on snoptm real and integer workspace.
*     They are used to define the limits on LU workspace. 

      maxrw = iw(  3)          ! Extent of snoptm real    workspace
      maxiw = iw(  5)          ! Extent of snoptm integer workspace
      maxcw = iw(  7)          ! Extent of snoptm char*8  workspace

      mincw = iw(maxcu) + 1
      miniw = iw(maxiu) + 1
      minrw = iw(maxru) + 1

      maxR  = iw( 56)
      maxS  = iw( 57)

      call s8Mem ( ierror, iw(iPrint), iw(iSumm), 
     $             m, n, ne, iw(neJac),
     $             nnCon, nnJac, nnObj,
     $             maxR, maxS, 
     $             maxcw, maxiw, maxrw,
     $             lencw, leniw, lenrw,
     $             mincw, miniw, minrw, iw )
      if (ierror .ne. 0) then
         inform = ierror
         go to 999
      end if

*     ------------------------------------------------------------------
*     Copy the problem name into the work array.
*     ------------------------------------------------------------------
      cw(mName) = Prob

*     ------------------------------------------------------------------
*     Solve the problem.
*     Tell s8solv that we don't have an initial Hessian.
*     ------------------------------------------------------------------
      Htype  = - 1
      nb     = n + m
      nka    = n + 1
      call s8solv( start, Htype, 
     $             m, n, nb, ne, nka, nName, 
     $             iObj, ObjAdd, fObj, Objtru,
     $             nInf, sInf,
     $             snwrap, userfg, userfg,
     $             a, ha, ka, bl, bu, Names,
     $             hs, xs, pi, rc,
     $             inform, nMajor, nS, 
     $             cu, lencu, iu, leniu, ru, lenru, 
     $             cw, lencw, iw, leniw, rw, lenrw )

      Obj    = ObjTru

*     Print times for all clocks (if lvlTim > 0).

      call s1time( 0, 2, iw, leniw, rw, lenrw )

  999 return

 9000 format(  ' EXIT -- snoptm character, integer and real',
     $         ' work arrays each must have at least 500 elements')

*     end of snoptm
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine snwrap( modefg, ierror, nState, getCon, getObj,
     $                   m, n, neJac, nnL, 
     $                   nnCon, nnCon0, nnJac, nnObj, nnObj0, 
     $                   userfg, dummy,
     $                   ne, nka, ha, ka, 
     $                   fCon, fObj, gCon, gObj, x, 
     $                   cu, lencu, iu, leniu, ru, lenru, 
     $                   cw, lencw, iw, leniw, rw, lenrw )

      implicit           real (a-h,o-z)
      external           userfg, dummy
      logical            getCon, getObj

      integer            ha(ne)
      integer            ka(nka)

      real   fCon(nnCon0)
      real   gObj(nnObj0), gCon(neJac)
      real   x(nnL)

      character*8        cu(lencu), cw(lencw)
      integer            iu(leniu), iw(leniw)
      real   ru(lenru), rw(lenrw)

*     ==================================================================
*     snwrap  calls the user-written routine  userfg  to evaluate the
*     problem functions and possibly their gradients.
*
*     Argument  userfg  is called using modefg to control
*     the gradients as follows:
*
*     modefg        Task
*     ------        ----
*       2     Assign fCon, fObj and all known elements of gCon and gObj.
*       1     Assign all known elements of gCon and gObj.
*             (fObj and fCon are ignored).
*       0     Assign fObj, fCon.  (gCon and gObj are ignored).
*
*     Since objective and constraints are computed simultaneously,
*     the input variables  getCon  and  getObj are ignored. 
*
*     31-Oct 1998: First version based on snwrap in SNOPT 5.3-4.
*     18-Nov 1998: Current version of snwrap.
*     ==================================================================
      logical            scaled, gotgU
      parameter         (nfCon1    = 189)
      parameter         (nfCon2    = 190)
      parameter         (nfObj1    = 194)
      parameter         (nfObj2    = 195)
*     ------------------------------------------------------------------
      iPrint     = iw( 12)
      iSumm      = iw( 13)
      lvlScl     = iw( 75)
      lvlTim     = iw( 77)

      laScal     = iw(274)
      lxScl      = iw(275)
      lgConU     = iw(306)

      ierror     = 0
      scaled     = lvlScl .eq. 2
      mode       = modefg

*     ------------------------------------------------------------------
*     Unscale x.
*     ------------------------------------------------------------------
      if ( scaled ) then
         call scopy ( nnL, x         , 1, rw(lxScl), 1 )
         call ddscl ( nnL, rw(laScal), 1, x        , 1 )

*        If the Jacobian is known, any constant elements saved in gConU
*        must be copied into gCon.

         lvlDer     = iw( 71)
         if (nState .ne. 1) then
            nGotg2  = iw(187)
         end if

         gotgU = lvlDer .ge. 2  .and.  nGotg2 .lt. neJac

         if (gotgU  .and.  modefg .gt. 0) then
            call scopy ( neJac, rw(lgConU), 1, gCon, 1 )
         end if
      end if

*     ------------------------------------------------------------------
*     Compute the user-defined functions and derivatives.
*     ------------------------------------------------------------------
      if (lvlTim .ge. 2) call s1time( 4, 0, iw, leniw, rw, lenrw )
      call userfg( mode, nnObj, nnCon, nnJac, neJac,
     $             x, fObj, gObj, fCon, gCon, nState,
     $             cu, lencu, iu, leniu, ru, lenru )
      if (lvltim .ge. 2) call s1time(-4, 0, iw, leniw, rw, lenrw )

      iw(nfCon1) = iw(nfCon1) + 1
      iw(nfObj1) = iw(nfObj1) + 1
      if (modefg .gt. 0) then
         iw(nfCon2) = iw(nfCon2) + 1
         iw(nfObj2) = iw(nfObj2) + 1
      end if

*     ------------------------------------------------------------------
*     Scale  x and the derivatives.
*     ------------------------------------------------------------------
      if ( scaled ) then
         call scopy ( nnL  , rw(lxScl), 1, x, 1 )
         call dddiv ( nnCon, rw(laScal+n), 1, fCon, 1 )
         if (modefg .gt. 0) then
            call s8sclg( nnObj, rw(laScal), gObj, 
     $                   iw, leniw, rw, lenrw )
            call s8sclJ( nnCon, nnJac, neJac, n, ne, nka, 
     $                   rw(laScal), ha, ka, gCon,
     $                   iw, leniw, rw, lenrw )
         end if
      end if

      if (mode .lt. 0) then
*        ---------------------------------------------------------------
*        The user may be saying the function is undefined (mode = -1)
*        or may just want to stop                         (mode < -1).
*        ---------------------------------------------------------------
         if (mode .eq. -1) then
            ierror = -1
         else
            ierror =  6
            if (iPrint .gt. 0) write(iPrint, 9060) iw(nfCon1)
            if (iSumm  .gt. 0) write(iSumm , 9060) iw(nfCon1)
         end if
      end if

      return

 9060 format(  ' EXIT -- Termination requested after', i8,
     $         '  calls to the user-supplied routine.')

*     end of snwrap
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine sntitl( title )

      character*30       title

*     ==================================================================
*     sntitl sets the title for snoptm.
*     ==================================================================

      title  = 'S N O P T (m) 5.3-4 (Oct 1998)'
*---------------123456789|123456789|123456789|--------------------------

*     end of sntitl
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine snInit( iPrint, iSumm,
     $                   cw, lencw, iw, leniw, rw, lenrw )

      implicit           real (a-h,o-z)
      character*8        cw(lencw)
      integer            iw(leniw)
      real   rw(lenrw)

*     ==================================================================
*     snInit  is called by the user (not by snopt3) to do the following:
*     1. Open default files (Print, Summary).
*     2. Initialize title.
*     3. Set options to default values.
*
*     15 Nov 1991: First version.
*     14 Jul 1997: Thread-safe version.
*     02 Oct 1997: Character workspace added.
*     15 Feb 1998: Current version of snInit.
*     ==================================================================
      parameter         (maxru     =   2)
      parameter         (maxrw     =   3)
      parameter         (maxiu     =   4)
      parameter         (maxiw     =   5)
      parameter         (maxcu     =   6)
      parameter         (maxcw     =   7)

      parameter         (nnCon     =  21)
      parameter         (nnJac     =  22)
      parameter         (nnObj     =  23)

      parameter         (lvlTim    =  77)

      character*30       title
      character*30       dashes
      data               dashes /'=============================='/
*     ------------------------------------------------------------------

      if (lencw .lt. 500 .or. leniw .lt. 500 .or. lenrw .lt. 500) then 
*        ---------------------------------------------------------------
*        Not enough workspace to do ANYTHING!
*        ---------------------------------------------------------------
         if (iPrint .gt. 0) write(iPrint, 9000) 
         if (iSumm  .gt. 0) write(iSumm , 9000) 
         if (iPrint .le. 0  .and.  iSumm  .le. 0) then
            write(*, 9000)
         end if
         go to 999
      end if

      iw( 12)   = iPrint
      iw( 13)   = iSumm

      iw(maxcu) = 500
      iw(maxiu) = 500
      iw(maxru) = 500
      iw(maxcw) = lencw
      iw(maxiw) = leniw
      iw(maxrw) = lenrw

*     These dimensions need to be initialized for an MPS run.

      iw(nnCon) = 0
      iw(nnJac) = 0
      iw(nnObj) = 0

      call sntitl( title )
      call s1init( title, iw, leniw, rw, lenrw )

      if (iPrint .gt. 0) then
         write (iPrint, '(  9x, a )') ' ', dashes, title, dashes
      end if
      if (iSumm .gt. 0) then
         write (iSumm , '(  1x, a )') ' ', dashes, title, dashes
      end if

*     ------------------------------------------------------------------
*     Set the options to default values.
*     snopt  will check the options later and maybe print them.
*     ------------------------------------------------------------------
      call s3undf( cw, lencw, iw, leniw, rw, lenrw )

*     ------------------------------------------------------------------
*     Initialize some global values.
*     ------------------------------------------------------------------
      iw(lvlTim) = 3

  999 return

 9000 format(  ' EXIT from snInit --',
     $         ' character, integer and real work arrays',
     $         ' must each have at least 500 elements')

*     end of snInit
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine snSpec( iSpecs, inform, 
     $                   cw, lencw, iw, leniw, rw, lenrw )

      implicit           real (a-h,o-z)
      character*8        cw(lencw)
      integer            iw(leniw)
      real   rw(lenrw)

*     ==================================================================
*     snSpec  is called by the user to read the Specs file.
*
*     07-Feb 1998: First version.
*     07-Feb 1998: Current version of snSpec.
*     ==================================================================
      external           s3opt
*     ------------------------------------------------------------------
      iw( 11)   = iSpecs

      iPrint    = iw( 12)
      iSumm     = iw( 13)

      inform    = 0
      nCalls    = 1

*     ------------------------------------------------------------------
*     Read the Specs file.
*     snopt  will check the options later and maybe print them.
*     ------------------------------------------------------------------
      if (iSpecs .gt. 0) then
         call s3file( nCalls, iSpecs, s3opt,
     $                iPrint, iSumm, inform,
     $                cw, lencw, iw, leniw, rw, lenrw )
      end if

*     end of snSpec
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine snMem ( m, n, ne, neJac,
     $                   nnCon, nnJac, nnObj,
     $                   mincw, miniw, minrw,
     $                   cw, lencw, iw, leniw, rw, lenrw )

      implicit           real (a-h,o-z)
      character*8        cw(lencw)
      integer            iw(leniw)
      real   rw(lenrw)

*     ==================================================================
*     snMem   estimates the memory requirements for snopt,
*     using the values:
*        m    , n    , ne    neJac
*        nnObj, nnCon, nnJac     
*
*     These values are used to compute the minimum required storage:
*     mincw, miniw, minrw.
*
*     Note: 
*     1. All default parameters must be set before calling snMem,
*        since some values affect the amount of memory required.
*
*     2. The arrays rw and iw hold  constants and work-space addresses.
*        They must have dimension at least 500.
*
*     3. This version of snMem does not allow user accessible
*        partitions of cw, iw and rw.
*
*     29 Mar 1998: First version.
*     29 Mar 1998: Current version of snMem.
*     ==================================================================
      iPrint  = iw( 12)
      iSumm   = iw( 13)

      if (lencw .lt. 500 .or. leniw .lt. 500 .or. lenrw .lt. 500) then 
*        ---------------------------------------------------------------
*        Not enough workspace to do ANYTHING!
*        ---------------------------------------------------------------
         if (iPrint .gt. 0) write(iPrint, 9000) 
         if (iSumm  .gt. 0) write(iSumm , 9000) 
         if (iPrint .le. 0  .and.  iSumm  .le. 0) then
            write(*, 9000)
         end if
         go to 999
      end if

      inform  = 0

*     Error messages from s8Mem are suppressed.

      iPrinx  = 0
      iSummx  = 0

*     Assign fake values for lencw, leniw, lenrw.
*     This will force s8Mem to estimate the memory requirements.

      llenrw  = 500
      lleniw  = 500
      llencw  = 500

*     An obligatory call to snInit has `undefined' all options.
*     Check the user-defined values and assign undefined values.
*     s8dflt needs various problem dimensions in iw.

      iw( 15) = m
      iw( 16) = n
      iw( 17) = ne
      iw( 21) = nnCon
      iw( 22) = nnJac
      iw( 23) = nnObj

      call s8dflt( 'Check optional parameters', 
     $             cw, llencw, iw, lleniw, rw, llenrw )

      mincw   = 501
      miniw   = 501
      minrw   = 501

      maxcw   = lencw
      maxiw   = leniw
      maxrw   = lenrw

      maxR    = iw( 56)
      maxS    = iw( 57)

      call s8Mem ( inform, iPrinx, iSummx, 
     $             m, n, ne, neJac,
     $             nnCon, nnJac, nnObj,
     $             maxR, maxS, 
     $             maxcw, maxiw, maxrw,
     $             llencw, lleniw, llenrw,
     $             mincw, miniw, minrw, iw )

  999 return

 9000 format(  ' EXIT from snMem --',
     $         ' character, integer and real work arrays',
     $         ' must each have at least 500 elements')

*     end of snMem.
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine snset ( buffer, iPrint, iSumm, inform,
     $                   cw, lencw, iw, leniw, rw, lenrw )

      implicit           real (a-h,o-z)
      character*(*)      buffer

      character*8        cw(lencw)
      integer            iw(leniw)
      real   rw(lenrw)

*     ==================================================================
*     snset  decodes the option contained in  buffer.
*
*     The buffer is output to file iPrint, minus trailing blanks.
*     Error messages are output to files iPrint and iSumm.
*     Buffer is echoed to iPrint but normally not to iSumm.
*     It is echoed to iSumm before any error msg.
*
*     On entry,
*     iPrint is the print   file.  no output occurs if iPrint .le 0.
*     iSumm  is the Summary file.  no output occurs if iSumm  .le 0.
*     inform is the number of errors so far.
*
*     On exit,
*     inform is the number of errors so far.
*
*     27 Nov 1991: first version.
*     ==================================================================
      character*16       key
      character*8        cvalue
*     ------------------------------------------------------------------
      call s3opt ( .true., buffer, key, cvalue, ivalue, rvalue, 
     $             iPrint, iSumm, inform,
     $             cw, lencw, iw, leniw, rw, lenrw )

*     end of snset
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine snseti( buffer, ivalue, iPrint, iSumm, inform,
     $                   cw, lencw, iw, leniw, rw, lenrw )

      implicit           real (a-h,o-z)
      character*(*)      buffer
      integer            ivalue

      character*8        cw(lencw)
      integer            iw(leniw)
      real   rw(lenrw)

*     ==================================================================
*     snseti decodes the option contained in  buffer // ivalue.
*     The parameters other than ivalue are as in snset.
*
*     27 Nov 1991: first version of snseti.
*     20 Sep 1998: current version.
*     ==================================================================
      character*16       key
      character*72       buff72
      character*8        cvalue
*     ------------------------------------------------------------------
      write(key, '(i16)') ivalue
      lenbuf = len(buffer)
      buff72 = buffer
      buff72(lenbuf+1:lenbuf+16) = key
      ivalxx = ivalue
      call s3opt ( .true., buff72, key, cvalue, ivalxx, rvalue, 
     $             iPrint, iSumm, inform,
     $             cw, lencw, iw, leniw, rw, lenrw )

*     end of snseti
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine snsetr( buffer, rvalue, iPrint, iSumm, inform,
     $                   cw, lencw, iw, leniw, rw, lenrw )

      implicit           real (a-h,o-z)
      character*(*)      buffer
      real   rvalue

      character*8        cw(lencw)
      integer            iw(leniw)
      real   rw(lenrw)

*     ==================================================================
*     snsetr decodes the option contained in  buffer // rvalue.
*     The parameters other than rvalue are as in snset.
*
*     27 Nov 1991: first version of snsetr.
*     20 Sep 1998: current version.
*     ==================================================================
      character*16       key
      character*72       buff72
      character*8        cvalue
*     ------------------------------------------------------------------
      write(key, '(1p, e16.8)') rvalue
      lenbuf = len(buffer)
      buff72 = buffer
      buff72(lenbuf+1:lenbuf+16) = key
      rvalxx = rvalue
      call s3opt ( .true., buff72, key, cvalue, ivalue, rvalxx,
     $             iPrint, iSumm, inform,
     $             cw, lencw, iw, leniw, rw, lenrw )

*     end of snsetr
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine sngetc( buffer, cvalue,
     $                   inform, cw, lencw, iw, leniw, rw, lenrw )

      implicit           real (a-h,o-z)
      character*(*)      buffer
      character*8        cvalue

      character*8        cw(lencw)
      integer            iw(leniw)
      real   rw(lenrw)

*     ==================================================================
*     sngetc gets the value of the option contained in  buffer.
*     The parameters other than cvalue are as in snset.
*
*     17 May 1998: first version.
*     ==================================================================
      character*16       key
*     ------------------------------------------------------------------
      call s3opt ( .false., buffer, key, cvalue, ivalue, rvalue,
     $             0, 0, inform,
     $             cw, lencw, iw, leniw, rw, lenrw )

*     end of sngetc
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine sngeti( buffer, ivalue,
     $                   inform, cw, lencw, iw, leniw, rw, lenrw )

      implicit           real (a-h,o-z)
      character*(*)      buffer
      integer            ivalue

      character*8        cw(lencw)
      integer            iw(leniw)
      real   rw(lenrw)

*     ==================================================================
*     sngeti gets the value of the option contained in  buffer.
*     The parameters other than ivalue are as in snset.
*
*     17 May 1998: first version.
*     ==================================================================
      character*16       key
      character*8        cvalue
*     ------------------------------------------------------------------
      call s3opt ( .false., buffer, key, cvalue, ivalue, rvalue,
     $             0, 0, inform,
     $             cw, lencw, iw, leniw, rw, lenrw )

*     end of sngeti
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine sngetr( buffer, rvalue,
     $                   inform, cw, lencw, iw, leniw, rw, lenrw )

      implicit           real (a-h,o-z)
      character*(*)      buffer
      real   rvalue

      character*8        cw(lencw)
      integer            iw(leniw)
      real   rw(lenrw)

*     ==================================================================
*     sngetr gets the value of the option contained in  buffer.
*     The parameters other than rvalue are as in snset.
*
*     17 May 1998: first version.
*     ==================================================================
      character*16       key
      character*8        cvalue
*     ------------------------------------------------------------------
      call s3opt ( .false., buffer, key, cvalue, ivalue, rvalue,
     $             0, 0, inform,
     $             cw, lencw, iw, leniw, rw, lenrw )

*     end of snsetr
      end
