#ifndef ROAD_HH
#define ROAD_HH

//Defines a basic road type
struct RoadSegment
{
  double N;
  double E;
  double width;
  double speed;
} __attribute__ ((packed));

#define ROAD_MAX_LENGTH  50

struct Road
{
  int length;
  RoadSegment segment[ROAD_MAX_LENGTH];

  const RoadSegment &operator[](int i) const { return segment[i]; }
  RoadSegment &operator[](int i) { return segment[i]; }

  int data_size() const {
    return sizeof(Road) - (ROAD_MAX_LENGTH-length)*sizeof(RoadSegment);
  }
} __attribute__ ((packed));;

#endif
