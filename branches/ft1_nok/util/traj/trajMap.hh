#include <ostream>
#include <iostream>
#include "frames/frames.hh"
#include "CMap.hh"
using namespace std;
#include "traj.h"

#ifndef _TRAJMAP_H_
#define _TRAJMAP_H_

class CTrajMap: public CTraj
{
 public:
 
  CTrajMap(int order) : CTraj(order) {} ;
/*! Evaluates traj through a CMap layer and returns 1 if any point in the traj
      lies in a cell with goodness below the cutoff level.  Currently is only
      defined for a layer of doubles.  Default cutoff is 0.1 m/s */
  bool    isTrajObstructed(CMap * map, int layer, double cutoff = 0.1);
  
  /*! Evaluates traj through a CMap layer and returns the time it would take to
      complete the traj if at every point in the traj the vehicle traveled at
      the speed specified by the map cell that point falls within.  Only defined
      for a layer of doubles.  For trajs with known and constant spacing,
      specifing spacing will save calculations.*/
  double  evalCost(CMap * map, int layer, double spacing = -1.);
};

#endif
