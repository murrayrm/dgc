#include "StateQueue.h"
#include <queue>
#include <State.h>

StateQueue:: StateQueue()
{
  //Nothing to do here
}

void StateQueue::push(State* s)
{
  cout<<"starting push"<<endl;
  int size = Q.size();
  list<State*>::iterator pointer = Q.begin(); //Create a pointer to the front of the list.

  int pos = 1;  //Represents the element in the list [1, size] to which pointer points.

  if((size == 0) || (*s) < (*pointer) )
    {
      cout<<"pushing into front of list"<<endl;
      Q.push_front(s);
    }
  else
    {
      
      while((pos <= size) && (*(*pointer)) < s)  //We haven't gone past the end of the list, and s comes after pointer
	{
	  ++pointer; //increment the pointer
	  pos++;
	}
      
      if(pos > size)  //We ran off the end of the list
	{
	  cout<<"pushing into back of list"<<endl;
	  Q.push_back(s);
	}
      else
	{
	  cout<<"pushing into list in front of position "<<pos<<endl;
	  Q.insert(pointer, s);
	}
    }
}

State* StateQueue::peek()
{
  return Q.front();
}

State* StateQueue::peek_back()
{
  return Q.back();
}

State* StateQueue::pop()
{
  State* temp = peek();
  Q.pop_front();
  return temp;
}

State* StateQueue::pop_back()
{
  State* temp = peek_back();
  Q.pop_back();
  return temp;
}

void StateQueue::remove(State* s)
{
  Q.remove(s);
}

void StateQueue::clear()
{
  Q.clear();
}

void StateQueue::sort()
{
  StateCompare c;
  Q.sort<StateCompare>(c);
}

bool StateQueue::isEmpty()
{
  return Q.empty();
}

int StateQueue::size()
{
  return Q.size();
}

list<State*>::iterator StateQueue::getQueueIterator()
{
  return Q.begin();
}

void StateQueue::iterate()
{
 int size = Q.size();
  list<State*>::iterator pointer = Q.begin(); //Create a pointer to the front of the list.

  int pos = 1;  //Represents the element in the list [1, size] to which pointer points.

  while(pos <= size)  //We haven't gone past the end of the list
    {
      (*pointer) -> print();
      pointer++; //increment the pointer
      pos++;
    }
}


StateQueue::~StateQueue()
{
  //nothing to do here
}
