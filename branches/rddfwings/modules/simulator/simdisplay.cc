#include "Sim.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

#include <iostream>
using namespace std;

// Added for display 2/24/04 
// Changed to use new, platform-independent struct 1/6/05
VehicleState dispSS;
double d_decimaltime;
double d_speed;
double yaw_degrees;
double yawrate_degrees;

double steerCommand = 0.0;
double steerCommandRad = 0.0;
double steerCommandDeg = 0.0;
double steerActualRad = 0.0;
double steerActualDeg = 0.0;
double accelCommand = 0.0;
int Estop = 0;
int Trans = 0;

extern int QUIT_PRESSED;
extern int PAUSED;
extern int RESET_PRESSED;
extern vehicle_t* m_pVehicle;

extern SIM_DATUM d;

#include "vddtable.h"
int user_quit(long arg);
int pause_simulation(long arg);
int unpause_simulation(long arg);
int reset_simulation(long arg);

void asim::UpdateSparrowVariablesLoop() 
{
  while(true) 
  {
    // this is the same as in the arbiter display 
    dispSS = d.SS;
    m_input.Time = d_decimaltime = ((double)d.SS.Timestamp)/1000000.0;
    m_input.Speed = d_speed = d.SS.Speed2();
    m_input.YawDeg = yaw_degrees = d.SS.Yaw*180.0/M_PI;
    m_input.YawRateDeg = yawrate_degrees = d.SS.YawRate*180.0/M_PI;

    m_input.SteerCmd = steerCommand = d.steer_cmd; 
    m_input.SteerCmdRad = steerCommandRad = d.steer_cmd > 0.0 
            ? d.steer_cmd*VEHICLE_MAX_RIGHT_STEER
            : d.steer_cmd*VEHICLE_MAX_LEFT_STEER;
    m_input.SteerCmdDeg = steerCommandDeg = steerCommandRad*180.0/M_PI;
    m_input.SteerAngleRad = steerActualRad = d.phi;
    m_input.SteerAngleDeg = steerActualDeg = d.phi*180.0/M_PI;
    m_input.AccelCommand = accelCommand = d.accel_cmd;

    Estop = m_pVehicle->actuator_estop.position;
    Trans = d.trans_cmd;
    
    usleep(500000); // Wait a bit, because other threads will print some stuff out
  }
}

void asim::SparrowDisplayLoop() 
{
  dbg_all = 0;

  if (dd_open() < 0) exit(1);

  dd_bindkey('Q', user_quit);
  dd_bindkey('P', pause_simulation);
  dd_bindkey('U', unpause_simulation);
  dd_bindkey('R', reset_simulation);

  dd_usetbl(vddtable);

  usleep(500000); // Wait a bit, because other threads will print some stuff out
  dd_loop();	
  dd_close();
  QUIT_PRESSED = 1; // Following LADARMapper's lead
}

// TODO: Set the shutdown flag in the quit function
int user_quit(long arg)
{
  return DD_EXIT_LOOP;
}

// Pause the simulation
// Wrapper method to allow binding to sparrow keys
int pause_simulation(long arg)
{
  return pause_simulation();
}

// Unpause the simulation
// Wrapper method to allow binding to sparrow keys
int unpause_simulation(long arg)
{
  return unpause_simulation();
}

// Reset the simulation
// Wrapper method to allow binding to sparrow keys
int reset_simulation(long arg)
{
  return reset_simulation();
}
