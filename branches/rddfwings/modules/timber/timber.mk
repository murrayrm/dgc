TIMBER_PATH = $(DGC)/modules/timber

TIMBER_DEPEND_MODULES = $(SKYNETLIB) $(DGCUTILS)

TIMBER_DEPEND_SOURCES = \
	$(TIMBER_PATH)/CTimber.hh \
	$(TIMBER_PATH)/CTimber.cc \
	$(TIMBER_PATH)/CTimberClient.hh \
	$(TIMBER_PATH)/CTimberClient.cc \
	$(TIMBER_PATH)/CTimberTalker.hh \
	$(TIMBER_PATH)/CTimberTalker.cc \
	$(TIMBER_PATH)/TimberMain.cc \
        $(TIMBER_PATH)/TimberConfig.hh

TIMBER_DEPEND = \
	$(TIMBER_DEPEND_MODULES) \
	$(TIMBER_DEPEND_SOURCES)
