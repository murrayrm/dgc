#ifndef _MAPDELTATALKER_H_
#define _MAPDELTATALKER_H_

#include "pthread.h"
#include <iostream>
#include <unistd.h>
#include "SkynetContainer.h"
#include "DGCutils"
#include "CMap.hh"

class CMapdeltaTalker : virtual public CSkynetContainer {
public:
	CMapdeltaTalker();
	~CMapdeltaTalker();

	bool SendMapdelta(int mapdeltaSocket, CDeltaList* deltaList);
	bool RecvMapdelta(int mapdeltaSocket, char* pMapdelta, int* pSize);
};

#endif // _MAPDELTATALKER_H_
