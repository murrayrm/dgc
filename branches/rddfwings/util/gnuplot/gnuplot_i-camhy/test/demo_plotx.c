#include <math.h>
#include <stdio.h>
#include "gnuplot_i.c"
/*#include "gnuplot_i.h"*/
#include "nrutil.c"

int main(int argc, char *argv[]) {
  gnuplot_ctrl    *h ; /*Gnuplot-Control*/
  double *        d;
  double          e[50]; //static memory allocation
  int             i ;

  d=dvector(1,50); /* Dynamic memory allocation*/

  h = gnuplot_init() ; /*Init the Gnuplot-Window*/
  gnuplot_set_xlabel(h,"x"); /*Set label of x-Axes*/
  gnuplot_set_ylabel(h,"y"); /*Set label of y-Axes*/
  gnuplot_set_title(h,"gnuplot_plot_x - Demonstration"); /*Set Title*/
  gnuplot_set_grid(h); /* Turns grid on*/
  gnuplot_setstyle(h,"lines"); 
  /* Set style
     possible values:  
       - lines
       - points
       - linespoints
       - impulses
       - dots
       - steps
       - errorbars
       - boxes
       - boxeserrorbars 
  */
  /*Range:
  gnuplot_set_xrange(h,1.0,10.0); 
  gnuplot_set_yrange(h,1.0,10.0);
  */

  for (i=1 ; i<=50 ; i++) {
    d[i] = (double)(i*i) ;
  }
  for (i=0 ; i<50; i++) {
    e[i] = (double)(i*i*0.5);
  }

  /* Plotting of the first array: Start index 0, end index 49*/
  gnuplot_plot_x(h,e,50,"parabola");
  /* Plot the dynamic array: Start index 1, end index 50*/
  gnuplot_plotrange_x(h, d, 1, 50, "parabola nrutil") ;

  printf("press ENTER to continue\n");
  while (getchar()!='\n') {} /* Wait for keypress */

  gnuplot_close(h) ; /*Close the gnuplot-window*/
  free_dvector(d,1,50);
  return 1;
}
