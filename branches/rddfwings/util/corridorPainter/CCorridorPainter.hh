#ifndef __CCORRIDORPAINTER_HH__
#define __CCORRIDORPAINTER_HH__


#include <stdlib.h>
#include <iomanip>
#include "CMap.hh"
#include "rddf.hh"
#include "geometry/CLineSegment.hh"
#include "geometry/CRectangle.hh"

#define MAX_CORRIDOR_LAYERS 3

struct CCorridorPainterOptions {
  double optRDDFscaling;
  double optMaxSpeed;

	bool optOverwriteNegative;
  
  enum PAINT_CENTER_TRACKLINE_TYPE {
    DONT = 0,
    PERCENTAGE = 1,
    SHIFT = 2
  };

  PAINT_CENTER_TRACKLINE_TYPE paintCenterTrackline;
  
  enum WHAT_TO_ADJUST_TYPE {
    RDDF = 0,
    CENTER_TRACKLINE = 1
  };

  WHAT_TO_ADJUST_TYPE whatToAdjust;

  double adjustmentVal;
};

class CCorridorPainter {
public:
  CCorridorPainter(CMap* inputMap, RDDF* inputRDDF, RDDF* widerRDDF);
  ~CCorridorPainter();


//   int initPainter(CMap* inputMap,
// 		  int layerNum,
// 		  RDDF* inputRDDF,
// 		  int useDelta=0);

  void addLayer(int layerNum, CCorridorPainterOptions* painterOpts, RDDF* inputRDDF, int useDelta=0);

  void paintChanges(NEcoord rowBox[], NEcoord colBox[]);

	void paintData(NEcoord vehicleLoc, double yaw, double speed, double distanceAhead = 30.0, double distanceBehind = 5.0);

	void paintToStart(NEcoord vehicleLoc, double speed);

  //void setPainterOptions(CCorridorPainterOptions* painterOpts);

  void repaintAll();
		  //double RDDFscaling, double maxSpeed);
  
private:
  CMap* _inputMap;
  RDDF* _inputRDDF;
	RDDF* _widerRDDF;
  
  int _numLayers;

  int _layerNums[MAX_CORRIDOR_LAYERS];
  int _useDeltas[MAX_CORRIDOR_LAYERS];
  CCorridorPainterOptions* _painterOpts[MAX_CORRIDOR_LAYERS];
	RDDF* _inputRDDFs[MAX_CORRIDOR_LAYERS];

  int* _startingIndices;
  int* _endingIndices;
  int* _tracklineStartingIndices;
  int* _tracklineEndingIndices;
  
  int _prevBottomLeftRowMultiple;
  int _prevBottomLeftColMultiple;

  int _behindWaypointNum;
  int _aheadWaypointNum;


  int _lastUsedWayptNumberRowBox;
  int _lastUsedWayptNumberColBox;


	int _lastUsedWayptNum;

  void findStartingIndices(NEcoord bottomPoint, NEcoord middlePoint,
			   NEcoord topPoint,
			   int minRow, int maxRow,
			   int minCol, int maxCol);

  void findEndingIndices(NEcoord bottomPoint, NEcoord middlePoint,
			 NEcoord topPoint,
			 int minRow, int maxRow,
			 int minCol, int maxCol);

  void traceRay(NEcoord start, NEcoord end, int indexList[],
		int minRow, int maxRow,
		int minCol, int maxCol, int outOnWhichSide);


  void paintRectangle(CLineSegment trackline, int i, double speed[], 
		      int minRow, int maxRow,
											int minCol, int maxCol,
											int index);

  void paintCircle(CCircle circle, int i, double speed[],
		   int minRow, int maxRow,
									 int minCol, int maxCol, int index);

  void paintTrackline(int i, double speed,
		      int minRow, int maxRow,
		      int minCol, int maxCol,
		      int index);
		      

  int _startRow, _endRow;
};


#endif //__CCORRIDORPAINTER_HH__
