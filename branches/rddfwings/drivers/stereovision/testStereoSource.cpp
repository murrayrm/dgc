#include <stdio.h>
#include <stdlib.h>

#include <stereovision/stereoSource.hh>

int main(int argc, char *argv[]) {
  stereoSource mySource;
  mySource.init(0, "config/stereovision/CamID.ini.short", "temp", "bmp");

  for(int i=0; i<20; i++) {
    printf("Grab %d\n", i);
    mySource.grab();
    mySource.save();
  }

  mySource.stop();
  return 0;
}

