## fake fn
1;

  N = size(data, 2);

  temp = [];
  for i = 1:N
    temp = [temp max(sum( data{i}(:,start:finish)' ))];
  end

function y = logi(x)
  y = 1 ./ (1 + exp(-x));
end

function doplots(data, start, finish, norm)
  figure(1); 
  plotarea(data, start, finish, norm);
  figure(2);
  plotmax(data, start, finish, norm);
  figure(3);
  plotbump(data, start, finish, norm);
end

function plotarea(data, start, finish, norm)
  ## assume data is in a struct
  N = size(data, 2);
  
  first = 0;
  
  for i = 1:N
    temp = sum( data{i}(:,start:finish)' );
    if (first == 0)
      hold off;
      first = 1;
    else
      hold on;
    end
    plot(temp);
  end
  hold off;
  
end

function plotmax(data, start, finish, norm)
  ## assume data is in a struct
  N = size(data, 2);
  
  temp = [];
  for i = 1:N
    temp = [temp max(sum( data{i}(:,start:finish)' ))];
  end
  plot((5:5:40) / 2.23, temp / norm);
  title('max of summed area');

  t = 1:18;
  hold on;
  ##  plot(t, atan(.25*(t - 7)) * .35 + .4);
  plot(t, .6 * logi((t-8) / 2) + .2);
  hold off;
end

function plotbump(data, start, finish, norm)
  ## assume data is in a struct
  N = size(data, 2);
  
  temp = [];
  for i = 1:N
    temp = [temp (max(sum( data{i}(:,start:finish)' ))./norm) / (1.5658 * (.6 * logi((i*5/2.23-8) / 2) + .2))];
  end
  plot((5:5:40) / 2.23, temp);
  title('bumpiness based on max');
end

function plotstddev(data, start, finish, norm)
  ## assume data is in a struct
  N = size(data, 2);
  
  temp = [];
  for i = 1:N
    temp = [temp std(sum( data{i}(:,start:finish)' ))];
  end
  plot(temp / norm);
  hold off;
  title('std dev of summed area');
end
