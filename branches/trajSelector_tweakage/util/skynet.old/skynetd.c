#include <sys/time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include "skynetd.h"
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <memory.h>
#ifdef MACOSX
#include <sys/uio.h>
#endif

#define FPERR(args...) if(!to_fork) fprintf(stderr, ## args)

extern struct sn_module* mod_list_head;
extern sn_sock sock_list[NUM_SOCKS];
extern int chirp_modlist_sock;
extern char soc_filename[108];       
extern uint32_t myip;                 
extern int key;
extern short int num_rmsg;             
//extern rmultimsgi rmulmsgbufi;
extern struct rmultimsgi* rmultiinhead;
extern int debug_level;
extern int to_fork;
extern long long drop_rmultiin_last;

int accept_module_com(int s){
  struct sockaddr_un their_addr ;
  int sun_size = sizeof (their_addr);
  int new_s = accept(s, (struct sockaddr *)&their_addr, &sun_size);
  if (new_s == -1) {
    perror("accept");
    exit(1);
  } 
  if (new_s >= NUM_SOCKS){
    if (debug_level >= 3) dump_sock_list();
    fprintf(stderr, "new_s >= NUM_SOCKS.  Increase NUM_SOCKS.  new_s= %d\n", new_s);
    exit(1);
  }
  else {
    sock_list[new_s].present = 1;
    sock_list[new_s].type = module_com;
    sock_list[new_s].checktime = 0;
    assert(sock_list[new_s].data.module == NULL);
  }
  return new_s;
}

int handle_module_com(int s){
  /* first, detect closed sockets */
  modcom_hdr  hdr;
  int got = recv_quant(s, &hdr, sizeof(modcom_hdr), 0);
  if ( got <= 0 )
  {
    /* socket is closed */
    remove_module(s);
  }
  else
  {
    switch( hdr.action ){
      case sn_register:
          handle_register(s, &hdr); 
        break;
      case send_reliable:
          insert_reliable(s, &hdr);
        break;
      case get_lsock:
          mk_lsock(s, &hdr);
        break;
      default:
        FPERR("handle_module_com(): unhandled action.  Action= %u\n", hdr.action);
        exit(1);
        break;
    }
  }
  return 0;
}

int remove_module(int s)
{
  /* socket is closed */
  if (sock_list[s].data.module != NULL)
  {
    /* module is registered, unregister module */
    /* first, remove all large_socks associated with module */
    int i;
    struct sn_module* mod = sock_list[s].data.module;
    for(i=0; i < NUM_SOCKS; i++)
    {
      if (sock_list[i].data.module == mod && sock_list[i].type == large_msg)
      {
        remove_sock(i);
      }
    }
    if((*mod).next == NULL)
    {
      if (mod->prev == NULL)
      {
        /* only module in list */
        free(mod);
        mod_list_head = NULL;
      }
      else
      {
        /* last module in list */
        mod->prev->next = NULL;
        free(mod);
      }
    }
    else{
      if(mod->prev == NULL)
      {
        /*first module in list*/
        mod_list_head = mod->next;
        mod->next->prev = NULL;
        free(mod);
      }
      else{
        /* middle of the list */
        mod->next->prev = mod->prev;
        mod->prev->next = mod->next;
        free(mod);
      }
    }
  }
  remove_sock(s);
  return 0;
}

int mk_lsock(int s, modcom_hdr* hdr)
{
  /*make socketpair*/
  int sv[2];
  if  (socketpair(AF_UNIX, SOCK_STREAM /*SOCK_DGRAM*/, 0, sv) == -1)
  {
    perror("mk_lsock: socketpair()");
    exit(1);
  }
   
  /*put one socket in sock_list*/
  if (sv[0] >= NUM_SOCKS || sv[1] >= NUM_SOCKS)
  {
    if (debug_level >= 3) dump_sock_list();
    fprintf(stderr, "sv[0] or sv[1] >= NUM_SOCKS.  Increase NUM_SOCKS.  ");
    fprintf(stderr, "sv[0]=%d sv[1]=%d\n", sv[0], sv[1]);
    exit(1);
  }
  else
  {
    sock_list[sv[0]].present = 1;
    sock_list[sv[0]].type = large_msg;
    sn_msg tmp;
    recv(s, &tmp, sizeof(tmp), 0);
    sock_list[sv[0]].sn_msg_type = tmp; 
    sock_list[sv[0]].data.module = sock_list[s].data.module;
    //sock_list[sv[0]].data.sn_msg_type = msg_type;
    sock_list[sv[0]].checktime = 0;
  }
  
  /*make msghdr*/
  struct msghdr msg;
  struct iovec iov[1];
  
  union {
    struct cmsghdr cm;
    char control[CMSG_SPACE(sizeof(int))];
    } control_un;
  struct cmsghdr *cmptr;
  
  msg.msg_control = control_un.control;
  msg.msg_controllen = sizeof(control_un.control);

  cmptr = CMSG_FIRSTHDR(&msg);
  cmptr->cmsg_len = CMSG_LEN(sizeof(int));
  cmptr->cmsg_level = SOL_SOCKET;
  cmptr->cmsg_type = SCM_RIGHTS;
  *((int*) CMSG_DATA(cmptr)) = sv[1];

  msg.msg_name = NULL;
  msg.msg_namelen = 0;
  iov[0].iov_base = "";
  iov[0].iov_len = 1;
  msg.msg_iov = iov;
  msg.msg_iovlen = 1;
  msg.msg_flags = 0;

  /*send other socket*/
  int len = sendmsg(s, &msg, MSG_NOSIGNAL);
  if(len <= 0)
  {
    if (len == 0 || errno == EPIPE )
    { 
      FPERR("mk_lsock:sendmsg(): got EPIPE or other side shutdown\n");
      remove_module(s);
    }
    else if(len < 0)
    {
      perror("mk_lsock:sendmsg()");
      printf("%d\t%d\n", len, errno);
      exit(1);
    }
  }
  /*and close it*/
  close(sv[1]);

  return (0);
}

int remove_sock(int s)
{
  sock_list[s].present = 0;
  sock_list[s].data.module = NULL;
  sock_list[s].checktime = 0;
  shutdown(s, SHUT_RDWR);
  close(s);
  return 0;
}

void handle_register(int s, modcom_hdr* hdr)
{
  struct sn_module* mod = mod_list_head;
  struct sn_module* prevmod = NULL;
  if (mod_list_head != NULL)
  {
    while(!(mod == NULL || (prevmod != NULL && mod->id > prevmod->id + 1 ) || (prevmod == NULL && mod->id > (myip % 16777216)*256)))
    {
      prevmod = mod;
      mod = mod->next;
      //if (mod != NULL) printf("mod->id %x\n", mod->id);
      //printf("condition %x\n", ((get_my_ip() + 1) % 16777216) * 256);
    } //mod->id < ((get_my_ip() + 1) % 16777216) * 256);
  }
  struct sn_module* newmod = malloc(sizeof(struct sn_module));
  if (mod == NULL)
  {
    if (prevmod == NULL)
    {
      //printf("1\n");
      /* only module in list */
      newmod->prev = NULL;
      newmod->next = NULL;
      mod_list_head = newmod;
    }
    else
    {
      //printf("2\n");
      /* last module in list */
      newmod->next = NULL;
      newmod->prev = prevmod;
      newmod->prev->next = newmod;
    }
  }
  else
  {
    if (prevmod == NULL)
    {
      //printf("3\n");
      /* first module in list */
      newmod->next = mod;
      newmod->prev = NULL;
      mod->prev = newmod;
      mod_list_head = newmod;
    }
    else
    {
      //printf("4\n");
      /* middle of the list */
      newmod->next = mod;
      newmod->prev = prevmod;
      prevmod->next = newmod;
      mod->prev = newmod;
    }
  }
  if(newmod->prev != NULL && newmod->prev->id / 256 == (myip % 16777216))
  {
    newmod->id = newmod->prev->id + 1;
  }
  else newmod->id = (myip % 16777216) * 256;
  newmod->location = myip; 
  newmod->name = hdr->name;
  sock_list[s].data.module = newmod;
  sock_list[s].checktime = 0;
  int len = send_quant(s, &newmod->id, sizeof(newmod->id), MSG_NOSIGNAL);
  if(len <= 0)
  {
    if(len == 0 || errno == EPIPE)
    {
      FPERR("handle_register:send_quant() got EPIPE or other side shutdown\n");
      remove_module(s);
    }
    else if(len < 1)
    {
      perror("handle_register: send_quant");
      printf("%d\t%d\n", len, errno);
      exit(1);
    }
  }
}

int insert_reliable(int incoming, modcom_hdr* hdr)
{
  int i;
  //make socket for outgoing message
  int s;
  s = socket(AF_INET, SOCK_DGRAM, 0);
  struct sockaddr_in multi_addr;
  multi_addr.sin_family = AF_INET;
  multi_addr.sin_port = htons(dg_port);
  bzero(&multi_addr.sin_zero, 8);
  get_dg_addr(rmulti, &multi_addr.sin_addr, key);
  connect(s, (struct sockaddr*) &multi_addr, sizeof(struct sockaddr));
  if (s > NUM_SOCKS)
  {
    if (debug_level >= 3) dump_sock_list();
    fprintf(stderr, "s >= NUM_SOCKS.  Increase NUM_SOCKS.  s= %d\n", s);
    exit(1);
  }
  assert(sock_list[s].present == FALSE);
  sock_list[s].present = TRUE;
  sock_list[s].type = rmultiout;
  sock_list[s].checktime = 0;
  #define rmulmsgbufo sock_list[s].data.rmultimsgo
  rmulmsgbufo = malloc(sizeof(rmultimsgo));
  rmulmsgbufo->datalen = hdr->msglen; 
  rmulmsgbufo->data = malloc(rmulmsgbufo->datalen);
  rmulmsgbufo->msg_type = hdr->aux_data.msg_type;
  int got = recv_quant(incoming, rmulmsgbufo->data, rmulmsgbufo->datalen, 0);
  if (got <= 0)
  {
    if (got < 0 && errno != EPIPE)
    {
      perror("insert_reliable: recv_quant");
      exit(1);
    }
    else
    {
      FPERR("insert_reliable: got EPIPE or other side shut down\n");
      free(rmulmsgbufo->data);
      free(rmulmsgbufo);
      remove_sock(s);
    }
  }
  else
  {
    rmulmsgbufo->present = TRUE;
    for(i=0; i<NUM_RECIPS; i++) rmulmsgbufo->got_list[i] = TRUE;
    rmulmsgbufo->msg_id = (myip % 65536) * 65536 + (int)num_rmsg;
    num_rmsg++;
    /* no loopback cause we'll dispatch this locally before we send*/
    const u_char no =0;
    setsockopt(s, IPPROTO_IP, IP_MULTICAST_LOOP, &no, sizeof(no));
    /* now create the struture that dispatch_rmultiin will use */
    struct rmultimsgi* rmulmsgloop = malloc(sizeof(struct rmultimsgi));
    /* put new rmultimsgin at head of list */
    rmulmsgloop->next = rmultiinhead;
    rmulmsgloop->prev = NULL;
    if(rmulmsgloop->next) rmulmsgloop->next->prev = rmulmsgloop;
    rmultiinhead = rmulmsgloop;
    rmulmsgloop->present = 1;
    rmulmsgloop->data = rmulmsgbufo->data;
    rmulmsgloop->msg_type = rmulmsgbufo->msg_type;
    rmulmsgloop->datalen = rmulmsgbufo->datalen;
    rmulmsgloop->pkt_list = NULL;
    rmulmsgloop->num_pkts = 0;
    rmulmsgloop->msg_id = rmulmsgbufo->msg_id;
    SNgettime(&(rmulmsgloop->last_time));
    dispatch_rmultiin(rmulmsgloop);
    /* cannot call remove_rmultiin because rmulmsgloop->data is a pointer to
    * rmulmsgbufo->data .  calling remove_rmultiin would double free*/
    if(rmulmsgloop->next) 
    {
      rmultiinhead = rmulmsgloop->next;
      rmultiinhead->prev = NULL;
      if (rmultiinhead->next) rmultiinhead->next->prev = rmultiinhead;
    }
    else rmultiinhead = NULL;
    free(rmulmsgloop);
    handle_rmultiout(s);
  }
  return 0;
  #undef rmulmsgbufo 
}

int handle_rmultiout(int s)
{
  #define rmulmsgbufo sock_list[s].data.rmultimsgo
  int num_pkts = (rmulmsgbufo->datalen/MAX_PKT_SIZE) + ((rmulmsgbufo->datalen%MAX_PKT_SIZE) == 0 ? 0 : 1) + 1;
  int i;
  char buffer[MAX_PKT_SIZE + sizeof(rpkthdr)];
  double sleepytime = 0;
  for (i = 0; i < num_pkts; i++)
  {
    /* packet format
     * int msg_id
     * int pkt
     * int num_pkts
     * void* data
     *
     * last packet has only sn_msg
     */
    rpkthdr hdr;
    hdr.msg_id = htonl(rmulmsgbufo->msg_id);
    hdr.pkt = htonl(i);
    hdr.num_pkts = htonl(num_pkts);
    memcpy(buffer, &hdr, sizeof(hdr));
    /* memcpy(buffer, &rmulmsgbufo.msg_id, sizeof(rmulmsgbufo.msg_id));
    memcpy(buffer + sizeof(rmulmsgbufo.msg_id), &i, sizeof(i));
    memcpy(buffer + sizeof(rmulmsgbufo.msg_id) + sizeof(i), &num_pkts, 
      sizeof(num_pkts)); */
    size_t pkt_len;
    if (i == num_pkts - 1)
    {
      pkt_len = sizeof(sn_msg);
      int data = htonl(rmulmsgbufo->msg_type);
      memcpy(buffer + sizeof(hdr), (void*)&data, sizeof(sn_msg)); 
    }
    else if (i == num_pkts - 2) 
    {
      pkt_len = rmulmsgbufo->datalen - MAX_PKT_SIZE*i; 
      memcpy(buffer + sizeof(hdr), rmulmsgbufo->data + MAX_PKT_SIZE*i, pkt_len);
    }
    else //(i < num_pkts - 2)  
    {
      pkt_len = MAX_PKT_SIZE;
      memcpy(buffer + sizeof(hdr), rmulmsgbufo->data + MAX_PKT_SIZE*i, 
            MAX_PKT_SIZE);
    }
    send_quant(s, &buffer, sizeof(hdr) + pkt_len, 0); 
    /*memcpy(buffer + sizeof(rmulmsgbufo.msg_id) + sizeof(i) + sizeof(num_pkts),
      &rmulmsgbufo.data + MAX_PKT_SIZE*i, pkt_len);
    send_quant(s, &buffer, 3*sizeof(int) + pkt_len, 0); */
    
    if (debug_level >= 3) printf("sent packet %u\n", i);

#warning "Hack here regarding message timing. FIXME"
    //hack to reduce lost packets
    //rate limit output to 1000mbs divided by safety factor
    //TODO: replace this with a finer granularity call.  we may waste a lot of
    //time here
    //finer granularity not possible without real time scheduling.  Instead, sum
    sleepytime += 16 ; //8*MAX_PKT_SIZE/(1000*1024*1024)*1000000*10;
    //printf("sleepytime= %lu\n", (unsigned long)sleepytime);
    if(sleepytime > 200) 
    {
      //printf("sleeping %d\n", (unsigned long int) sleepytime);
      usleep((unsigned long int)sleepytime); 
      sleepytime = 0;
    }
  }  
  if (debug_level >= 2) 
    printf("handle_rmultiout(): just send msg_id: %d\tnum_pkts: %d\tmsg_type: %s\n", 
      rmulmsgbufo->msg_id, num_pkts, sn_msg_asString(rmulmsgbufo->msg_type));
  free(sock_list[s].data.rmultimsgo->data);
  free(sock_list[s].data.rmultimsgo);
  remove_sock(s);
  return 0;
  #undef rmulmsgbufo 
}

int handle_rmultiin(int s)
{
  /* get header */
  rpkthdr hdr;
  recv(s, &hdr, sizeof(hdr), MSG_PEEK);
  int msg_id = ntohl(hdr.msg_id);
  
  /* find this message in the list */
  /*TODO: make rmultibuf retain its value between calls. 
   *This will speed the module lookup to O(1) in the average case.*/
  
  /**/
  struct rmultimsgi* rmultibuf = rmultiinhead;
  while( rmultibuf!=NULL && rmultibuf->next!=NULL && rmultibuf->msg_id != msg_id)
  {
    rmultibuf = rmultibuf->next;
  }
  if (rmultibuf==NULL || (rmultibuf->next==NULL && rmultibuf->msg_id != msg_id)) 
    insert_rmultiin(s, &hdr, &rmultibuf);
  /* NOTE!!! insert_rmultiin can change rmultibuf */
  process_rmultiin(s, rmultibuf);
  return 0;
}

void drop_rmultiin()
{
  unsigned int dropped_bufcount = 0;
  struct rmultimsgi* rmultibuf = rmultiinhead;
  struct rmultimsgi* new_rmultibuf;
  long long now;
  SNgettime(&now);
  if(rmultibuf != NULL)
  do 
  {
    new_rmultibuf = rmultibuf->next;
    if(now - rmultibuf->last_time > RMULTIIN_TIMEOUT)
    {
      dropped_bufcount++;
      remove_rmultiin(rmultibuf);
    }
    rmultibuf = new_rmultibuf; 
  }while( rmultibuf!=NULL);
  if(debug_level >= 1 && dropped_bufcount > 0)
  {
    fprintf(stderr,"dropped %u incoming message buffers\n", dropped_bufcount);
  }
  drop_rmultiin_last = now;
}

int insert_rmultiin(int s, rpkthdr* hdr, struct rmultimsgi** rmultibuf_ptr)
{
  if (debug_level >= 2) printf("in insert_rmultiin\n");
  #define rmultibuf  (*rmultibuf_ptr)
  if(rmultibuf == NULL)
  {
    /* first message in list */
    rmultiinhead = malloc(sizeof(struct rmultimsgi));
    rmultiinhead->prev = NULL;
    rmultibuf = rmultiinhead;
  }
  else
  {
    rmultibuf->next = malloc(sizeof(struct rmultimsgi));
    rmultibuf->next->prev = rmultibuf;
    rmultibuf = rmultibuf->next;
  }
  rmultibuf->next = NULL;
  rmultibuf->present = TRUE;
  rmultibuf->num_pkts = ntohl(hdr->num_pkts);
  rmultibuf->msg_id = ntohl(hdr->msg_id);
  rmultibuf->datalen = rmultibuf->num_pkts * MAX_PKT_SIZE;
  rmultibuf->data = malloc(rmultibuf->datalen);
  rmultibuf->pkt_list = calloc(rmultibuf->num_pkts, sizeof(int));
  SNgettime(&(rmultibuf->last_time));
  /* Hack for speed.  Since FALSE == true, we can use bzero to init list */
  bzero((void*)rmultibuf->pkt_list, rmultibuf->num_pkts * sizeof(int));
  return 0;
  #undef rmultibuf
}  

int remove_rmultiin(struct rmultimsgi* rmultibuf)
{
  if(rmultibuf->prev == NULL)
  {
    if(rmultibuf->next == NULL)
    {
      /*only msg in list*/
      rmultiinhead = NULL;
    }
    else
    {
      /*first msg in list*/
      rmultiinhead = rmultibuf->next;
      rmultiinhead->prev = NULL;
    }
  }
  else
  {
    if(rmultibuf->next == NULL)
    {
      /*last msg in list*/
      rmultibuf->prev->next = NULL;
    }
    else
    {
      /*middle of the list*/
      rmultibuf->prev->next = rmultibuf->next;
      rmultibuf->next->prev = rmultibuf->prev;
    }
  }
  free(rmultibuf->data);
  free(rmultibuf->pkt_list);
  free(rmultibuf);
  return 0;
}

int process_rmultiin(int s, struct rmultimsgi* rmulmsgbufi)
{
  /* update rmultiin status */
  SNgettime(&(rmulmsgbufi->last_time));
  /* get header */
  unsigned int pkt, num_pkts, msg_id;
  char buffer[MAX_PKT_SIZE + sizeof(rpkthdr)];
  int datain;
  datain = recv(s, &buffer, MAX_PKT_SIZE + sizeof(rpkthdr), 0);
  rpkthdr* hdr = (rpkthdr*) &buffer;
  msg_id = ntohl(hdr->msg_id);
  pkt = ntohl(hdr->pkt);
  num_pkts = ntohl(hdr->num_pkts);
  
  if (debug_level >= 3) printf("process_rmultiin\tmsg_id: %d\tpkt: %d\tnum_pkts: %d\n", msg_id, pkt, num_pkts);

  /* get data */
  int pkt_len;
  if (pkt == num_pkts - 1)
  {
    pkt_len = sizeof(sn_msg);
    rmulmsgbufi->msg_type = (sn_msg)ntohl( *( (int*) (buffer + sizeof(rpkthdr))));
  }
  else 
  {
    if (pkt == num_pkts - 2) 
    {
      pkt_len = rmulmsgbufi->datalen - MAX_PKT_SIZE * (num_pkts - 2);
      rmulmsgbufi->datalen = MAX_PKT_SIZE * (num_pkts - 2) + datain - 
                             sizeof(rpkthdr);
    }
    else pkt_len = MAX_PKT_SIZE;
    memcpy(rmulmsgbufi->data + pkt*MAX_PKT_SIZE, buffer + sizeof(rpkthdr), pkt_len);
  }
  /* compute progress */
  if (rmulmsgbufi->pkt_list[pkt] == FALSE)
  {
    rmulmsgbufi->pkt_list[pkt] = TRUE;
    rmulmsgbufi->num_pkts--; 
  }
  if(rmulmsgbufi->num_pkts == 0)
  {
    if (debug_level >= 2) printf("processed rmultiin message %d\n", msg_id);
    dispatch_rmultiin(rmulmsgbufi);
    remove_rmultiin(rmulmsgbufi);
  }
  
  return 0;
}

int dispatch_rmultiin(struct rmultimsgi* rmulmsgbufi)
{
  if (debug_level >=2) printf("in dispatch_rmultiin\n");
  if (debug_level >=3) dump_sock_list();
  /* find the socket that goes with this type */
  int i=0;
  while(i < NUM_SOCKS  )
  {
    /*then send the data*/
    if(sock_list[i].sn_msg_type == rmulmsgbufi->msg_type && 
        sock_list[i].type == large_msg && sock_list[i].present)
    {
      /*send the length of the message, then send the message*/
      int datalen = htonl(rmulmsgbufi->datalen);
      size_t len = send(i, &datalen, sizeof(datalen), MSG_NOSIGNAL);
      if(len < 0 && errno != EPIPE)
      {
        perror("dispatch_rmultiin: send:");
        exit(1);
      }
      len=0;
      //TODO: eliminate while loop.  Instead, go back to main loop if not all
      //data is sent and put this socket in write_fds .  Forcing the program
      //to stay here until the send has completed can block too much
      while(len < rmulmsgbufi->datalen)
      {
        size_t sent = send(i, rmulmsgbufi->data + len, rmulmsgbufi->datalen - len, MSG_NOSIGNAL);

        if (sent <= 0)
        {
          if (sent == 0 || errno == EPIPE)
          {
            FPERR("dispatch_rmultiin: other side closed connection or got EPIPE\n");
            int j;
            for(j=0; sock_list[j].type != module_com || sock_list[j].present != 1 || sock_list[j].data.module != sock_list[i].data.module; j++);
            assert(sock_list[j].type == module_com && sock_list[j].present == 1 && sock_list[j].data.module == sock_list[i].data.module);
            remove_module(j);
          }
          else 
          {
            perror("dispatch_rmultiin:send");
            FPERR("send(%u, %p, %zx, 0)\n", i, rmulmsgbufi->data, rmulmsgbufi->datalen);
            if (debug_level >=3) dump_sock_list();
            exit(1);
          }
        }
        /*else if(len != rmulmsgbufi->datalen)
        {
          fprintf(stderr, "dispatch_rmultiin: sent != datalen .  ");
          fprintf(stderr, "sent= %u datalen= %u\n", sent, (uint32_t)rmulmsgbufi->datalen);
        }*/
        else len += sent;
      }
    }
    i++;
  }
  return 0;
}

void chirp_modlist()
{
#define modlistbufsiz 1024
  //printf("i'm chirping\n");
  char* buffer[modlistbufsiz];
  struct sn_module* module = mod_list_head;
  void* ptr = buffer;
  while(module != NULL)
  {
    assert((ptr - (void*)buffer) < (modlistbufsiz - sizeof(ml_elem))); 
    ((ml_elem*)ptr)->id = module->id;
    ((ml_elem*)ptr)->location = module->location;
    ((ml_elem*)ptr)->name = module->name;
    ptr += sizeof(ml_elem);
    module = module->next;

  }
  //now send this out multicast
  send_quant(chirp_modlist_sock, buffer, ptr - (void*)buffer, 0);
  set_checktime(chirp_modlist_sock, CHIRP_PERIOD);
  #undef modlistbufsiz
}

void set_checktime(int sock, unsigned int usec)
{
  unsigned long long now;
  SNgettime(&now);
  sock_list[sock].checktime = now + usec;
}

int dump_sock_list()
{
  /* TODO: dump checktime for each socket */
  int i;
  for(i=0; i < NUM_SOCKS; i++)
  {
    fprintf(stderr, "i = %d\t", i);
    fprintf(stderr, "present= %d\t", sock_list[i].present);
    switch(sock_list[i].type)
    {
      case global_var:
        fprintf(stderr, "type = global_var\t");
        break;
      case listening:
        fprintf(stderr, "type = listening\t");
        break;
      case module_com:
        fprintf(stderr, "type = module_com\t");
        fprintf(stderr, "module = %s\t", modulename_asString(sock_list[i].data.module->name));
        break;
      case rmulti_listen:
        fprintf(stderr, "type = rmulti_listen\t");
        break;
      case rmultiin:
        fprintf(stderr, "type = rmultiin\t");
        break;
      case rmultiout:
        fprintf(stderr, "type = rmultiout\t");
        break;
      case large_msg:
        fprintf(stderr, "type = large_msg\t");
        fprintf(stderr, "module = %s\t", modulename_asString(sock_list[i].data.module->name));
        fprintf(stderr, "sn_msg_type = %s\t", sn_msg_asString(sock_list[i].sn_msg_type));
        break;
      default:
        fprintf(stderr, "type = %s\t", sn_msg_asString(sock_list[i].sn_msg_type));
        break;
    }    
  fprintf(stderr, "\n");
  }
  return 0;
}
