#include "find_force.hh"

// double get_pedal(double v, double f, int gear)
// *********************************************
// Gets the proper pedal setting based on the current velocity
// current gear, and desired force.  First checks if the desired
// force is less than that applicable by using purely engine force, 
// if so, then use the brake
double get_pedal(double v, double f, int gear)
{
  double pedal;

  
  if (f < get_force(v, 0, gear)){
    //cout<<"get: "<<get_force(v, 0, gear)<<endl;
    pedal = get_brake(f);
  }else{
    pedal = get_throttle(v, f, gear);
  }
  
  return pedal;
}


// double get_throttle(double v, double f, int gear)
// ************************************************
// Inverse of get_force function.  This gives you the nominal
// throttle setting required to get a certain force output at the 
// wheels 
double get_throttle(double v, double f, int gear)
{
  double R_we, w_wheel, w_engine, w_f, Tf, Td, Te, th, th_a;

  // get  the proper ratio from the wheel velocity to the engine velocity
  switch(gear){
  case GEAR1:
    R_we =  R_TC * R_TRANS1 * R_DIFF * R_TRANSFER;
    break;
  case GEAR2:
    R_we =  R_TC * R_TRANS2 * R_DIFF * R_TRANSFER;
    break;
  case GEAR3:
    R_we =  R_TC * R_TRANS3 * R_DIFF * R_TRANSFER;
    break;
  case GEAR4:
    R_we =  R_TC * R_TRANS4 * R_DIFF * R_TRANSFER;
    break;
  case GEAR5:
    R_we =  R_TC * R_TRANS5 * R_DIFF * R_TRANSFER;
    break;
  }

  //cout<<"R_we: "<<R_we<<endl;
  
  // get the speeds of everything
  w_wheel = v / R_WHEEL;
  w_engine = w_wheel * R_we;
  w_f = fmax(w_engine, W_IDLE);

  //cout<<"w_wheel: "<<w_wheel<<" w_engine: "<<w_engine<<endl;

  //cout<<"w_f: "<< w_f  <<endl;

  // Get the torques and convert to newton-m from lb-ft
  Tf = FUDGE * (K_F1 + K_F2  * w_f + K_F3 * pow(w_f,2)) * LBFT2NM;
  Td = FUDGE * (K_F4 + K_F5  * w_f + K_F6 * pow(w_f,2)) * LBFT2NM;


  //cout<<"TF: "<<Tf<<" Td"<<Td<<endl;

  // get the desired engine torque
  Te = f / R_WHEEL / R_we;

  //cout<<"Te: "<<Te<<endl;

  // calculate the linear throttle requirement
  th_a = (Te - Td) / (Tf - Td);

  //cout<<"Th_a: "<<th_a<<endl;

  // now get the actual throttle
  th = .6*powf((log(1 - th_a) /  -K_T1) , (1 / K_T2));
 
  return fmin(th, 1);
}

// double get_force(double v, double th, int gear)
// **********************************************
// Calculates the resulting engine force from a throttle setting,
// velocity, and gear
double get_force(double v, double throttle, int gear)
{
  double R_we, w_wheel, w_engine, w_f, Tf, Td, Te;
  double T_wheel, F_eng, throt_act;
  // get  the proper ratio from the wheel velocity to the engine velocity
  switch(gear){
  case GEAR1:
    R_we =  R_TC * R_TRANS1 * R_DIFF * R_TRANSFER;
    break;
  case GEAR2:
    R_we =  R_TC * R_TRANS2 * R_DIFF * R_TRANSFER;
    break;
  case GEAR3:
    R_we =  R_TC * R_TRANS3 * R_DIFF * R_TRANSFER;
    break;
  case GEAR4:
    R_we =  R_TC * R_TRANS4 * R_DIFF * R_TRANSFER;
    break;
  case GEAR5:
    R_we =  R_TC * R_TRANS5 * R_DIFF * R_TRANSFER;
    break;
  }
  
  // get the speeds of everything
  w_wheel = v / R_WHEEL;
  w_engine = w_wheel * R_we;
  w_f = fmax(w_engine, W_IDLE);

  // get the nonlinear throttle mapping
  throt_act = 1 - exp( -K_T1 * pow(throttle,K_T2));
  
  // Get the torques and convert to newton-m from lb-ft
  Tf = FUDGE * (K_F1 + K_F2  * w_f + K_F3 * pow(w_f,2)) * LBFT2NM;
  Td = FUDGE * (K_F4 + K_F5  * w_f + K_F6 * pow(w_f,2)) * LBFT2NM;

  // get the resultant torque from throttle setting
  Te = Tf * throt_act + Td * (1 - throt_act); 

  // get the torque at the wheel
  T_wheel = Te * R_we; 
  
  // force is torque * wheel radius
  F_eng = T_wheel * R_WHEEL; 

  return F_eng;

}

// double get_brake(double f)
// *************************
// Get the brake setting needed for a certain force
double get_brake(double f)
{
  double brake, b_act;
 
  if (f >= 0){
    brake = 0;
  }else if (f < BF_MAX){
    brake = -1;
  }else{
    // get the linear brake setting
    b_act = f / BF_MAX;
    //cout<<"b_act: "<<b_act<<endl;

    // now get the actual brake
    brake = -powf((log(1 -  b_act) / - K_B1), (1 / K_B2));
    //cout<<"brake: "<<brake<<endl;
  }

  return brake;
}
    
    
