#include "CCostPainter.hh"

CCostPainter::CCostPainter ()
{
 _inputMap = NULL;
 _speedMap = NULL;

  _costLayerNum = 0;
  _elevLayerNum = 0;
  _corridorLayerNum = 0;
}


CCostPainter::~CCostPainter ()
{

}


int
CCostPainter::initPainter (CMap * inputMap, int costLayerNum,
                           int elevLayerNum, int corridorLayerNum,
                           int roadLayerNum, int fusedLayerNum ,  int nonBinary,
                           int growLayerNum, double radius,
                           int conservativeGrowing,
													 CMap* speedMap, int sampledCostLayerNum, int sampledCorridorLayerNum,
													 int fusedYetLayerNum, int superconLayerNum) {
  _inputMap = inputMap;
	_speedMap = speedMap;
  _nonBinary = nonBinary;
  _costLayerNum = costLayerNum;
  _elevLayerNum = elevLayerNum;
  _corridorLayerNum = corridorLayerNum;
  _growLayerNum = growLayerNum;
  _roadLayerNum = roadLayerNum;
  _fusedLayerNum = fusedLayerNum;
	_sampledCostLayerNum = sampledCostLayerNum;
	_sampledCorridorLayerNum = sampledCorridorLayerNum;
	_fusedYetLayerNum = fusedYetLayerNum;
	_superconLayerNum = superconLayerNum;

  _numRows = _inputMap->getNumRows ();
  _numCols = _inputMap->getNumCols ();

  _radius = radius;

  _conservativeGrowing = conservativeGrowing;

  _numMaskPts = 0;

  double rowRes = _inputMap->getResRows ();
  double colRes = _inputMap->getResCols ();

  int minRow, minCol, maxRow, maxCol;

  minRow = (int) (-radius / rowRes);
  minCol = (int) (-radius / colRes);
  maxRow = (int) (radius / rowRes);
  maxCol = (int) (radius / colRes);

  /*
     for(int row=minRow; row<maxRow; row++) {
     for(int col=minCol; col<maxCol; col++) {
     if(conservativeGrowing) {

     } else {
     if(
     }
     _numMaskPts++;
     }
     }
   */

  return 0;
}


#if 0
int CCostPainter::paintChanges (double UTMNorthing, double UTMEasting,
                             bool paintSurrounding )
{
  if (_inputMap->checkBoundsUTM (UTMNorthing, UTMEasting) == CMap::CM_OK)
  {
    double maxVelo, currentVelo;
    double maxGradient;
    double corridorReference, roadReference;
    int maxLocalRow, maxLocalCol;
    int minLocalRow, minLocalCol;
    int cellRow, cellCol;
    double distance;

    //First, set us some limits
    _inputMap->UTM2Win (UTMNorthing, UTMEasting, &cellRow, &cellCol);
    double resRows, resCols;
    resRows = _inputMap->getResRows();
    resCols = _inputMap->getResCols();

    if (cellRow < _numRows - 1) {
      maxLocalRow = cellRow + 1;
      if(paintSurrounding == 1){
	paintChanges(UTMNorthing+resRows, UTMEasting, 0);
      }
    } else {
      maxLocalRow = cellRow;
    }

    if (cellCol < _numCols - 1) {
      maxLocalCol = cellCol + 1;
      if(paintSurrounding ==1){
	paintChanges(UTMNorthing, UTMEasting + resCols, 0);
      }
    } else {
      maxLocalCol = cellCol;
    }

    if (cellRow > 0) {
      minLocalRow = cellRow - 1;
      if(paintSurrounding ==1){
	paintChanges(UTMNorthing-resRows, UTMEasting,  0);
      }
    } else {
      minLocalRow = cellRow;
    }

    if (cellCol > 0) {
      minLocalCol = cellCol - 1;
      if(paintSurrounding ==1){
	paintChanges(UTMNorthing, UTMEasting -resCols, 0);
      }
    } else {
      minLocalCol = cellCol;
    }
 
    
    int numAdjObs = 0;
    double currentElev, northElev, southElev, eastElev, westElev;
    double northGrad, southGrad, eastGrad, westGrad;
    currentElev =
      _inputMap->getDataWin < double >(_elevLayerNum, cellRow, cellCol);
    northElev =
      _inputMap->getDataWin < double >(_elevLayerNum, maxLocalRow, cellCol);
    southElev =
      _inputMap->getDataWin < double >(_elevLayerNum, minLocalRow, cellCol);
    eastElev =
      _inputMap->getDataWin < double >(_elevLayerNum, cellRow, maxLocalCol);
    westElev =
      _inputMap->getDataWin < double >(_elevLayerNum, cellRow, minLocalCol);
    double noData = _inputMap->getLayerNoDataVal < double >(_elevLayerNum);
    int gradTemp;
    double gradientNorthing, gradientEasting;
    CElevationFuser& mapData  = _inputMap->getDataWin<CElevationFuser>(_fusedLayerNum, cellRow, cellCol);
    double stdDev = mapData.getStdDev();
    double stdDevVelo;
    currentVelo = _inputMap->getDataWin < double >(_costLayerNum, cellRow, cellCol);
    corridorReference =
      _inputMap->getDataWin < double >(_corridorLayerNum, cellRow, cellCol);
    roadReference = 
      _inputMap->getDataWin < double >(_roadLayerNum, cellRow ,cellCol);
    bool northObs = false, southObs = false, eastObs=false, westObs = false;
    double stdDevArea = 0;
    
    double stdTemp;
    int stdCount = 0 ;
    //area stdDev here
    int i, j;
    //bound checking
    for(i=-2; i<3 ;i++){
      for(j=-2;j<3;j++){
        mapData  = _inputMap->getDataWin<CElevationFuser>(_fusedLayerNum, cellRow, cellCol);
	stdTemp = mapData.getStdDev();
	if (stdTemp >= .01){
	  stdCount++;
	  if( i !=0 || j!=0 ){
	    stdDevArea += stdTemp/pow((double)(pow(i,2.0)+pow(j,2.0)),.5)  ; 
	  }
	}
      }
    }
    stdDevArea = stdDevArea / stdCount;

    if(currentElev != noData){
      gradTemp = calcAreaGradient(cellRow, cellCol, gradientNorthing, gradientEasting);
      if ( _mapperOpts.optUseAreaGradient == 0){
	gradTemp = 0;
      }
      if (northElev != noData) {
	  if (gradTemp==1){
	    northGrad = fabs( gradientCorrection( (northElev - currentElev) / resRows,gradientNorthing,_mapperOpts.optAreaGradScaling));
	  } else{
	    northGrad = fabs ( northElev - currentElev)/resRows;
	  }
	} else {
	  northGrad = 0.0;
	}
      if (southElev != noData) {
	  if (gradTemp==1){
	    southGrad = fabs (gradientCorrection( (southElev - currentElev) / resRows,-gradientNorthing,_mapperOpts.optAreaGradScaling));
	  } else{
	    southGrad = fabs (southElev - currentElev) / resRows;
	  }
	} else {
	  southGrad = 0.0;
	}
      if (eastElev != noData) {
	  if (gradTemp==1){
	    eastGrad = fabs (gradientCorrection( (eastElev - currentElev) / resCols, gradientEasting, _mapperOpts.optAreaGradScaling));
	  } else{
	    eastGrad = fabs (eastElev - currentElev) / resCols;
	  }
	} else {
	  eastGrad = 0.0;
	}
      if (westElev != noData) {
	if (gradTemp==1){
	  westGrad = fabs (gradientCorrection( (westElev - currentElev) / resCols,-gradientEasting, _mapperOpts.optAreaGradScaling));
	} else{ 
	  westGrad = fabs (westElev - currentElev) / resCols;
	}
      } else {
	westGrad = 0.0;
      }
    
    // Calculate the maximum actual gradient between the current cell and the 
    // 4-neighboring cells with real elevation values
      if(northGrad >= _mapperOpts.optUnpassableObsHeight){
	numAdjObs++;
	northObs = true;
      }
      if(southGrad >= _mapperOpts.optUnpassableObsHeight){
	numAdjObs++;
	southObs = true;
      }
      if(eastGrad >= _mapperOpts.optUnpassableObsHeight){
	numAdjObs++;
	eastObs = true;
      }
      if(westGrad >= _mapperOpts.optUnpassableObsHeight){
	numAdjObs++;
	westObs = true;
      }

      double temp1, temp2;
      double unpassObs = _mapperOpts.optUnpassableObsHeight;
      double noDataVal = _inputMap->getLayerNoDataVal<double>(_elevLayerNum);


      if(numAdjObs == 1){
	if(northObs == true){
	  temp1 = _inputMap->getDataWin<double>(_elevLayerNum,cellRow +1, cellCol+1 );
	  temp2 = _inputMap->getDataWin<double>(_elevLayerNum,cellRow +1, cellCol-1 );
	  if(cellRow +1 < _numRows -1 && cellCol+1 < _numCols -1 && cellCol-1 >= 0 )
	  if(temp1 != noDataVal && temp2 != noDataVal){
	    
	    if(fabs(currentElev - temp2 ) < unpassObs && fabs(currentElev -temp2 ) < unpassObs){
	      northGrad = 0;
	    }
	  }
	}
	
     
     	if(southObs == true){
	  temp1 = _inputMap->getDataWin<double>(_elevLayerNum,cellRow -1, cellCol+1 );
	  temp2 = _inputMap->getDataWin<double>(_elevLayerNum,cellRow -1, cellCol-1 );
	  if(cellRow -1 >= 0 && cellCol+1 < _numCols -1 && cellCol-1 >= 0 )
	  if(temp1 != noDataVal && temp2 != noDataVal){
	    
	    if(fabs(currentElev - temp2 ) < unpassObs && fabs(currentElev -temp2 ) < unpassObs){
	      southGrad = 0;
	    }
	  }
	}
	

	if(eastObs == true){
	  temp1 = _inputMap->getDataWin<double>(_elevLayerNum,cellRow +1, cellCol+1 );
	  temp2 = _inputMap->getDataWin<double>(_elevLayerNum,cellRow -1, cellCol+1 );
	  if(cellCol +1 < _numRows -1 && cellRow+1 < _numCols -1 && cellRow-1 >= 0 )
	  if(temp1 != noDataVal && temp2 != noDataVal){
	    
	    if(fabs(currentElev - temp2 ) < unpassObs && fabs(currentElev -temp2 ) < unpassObs){
	      eastGrad = 0;
	    }
	  }
	}

	if(westObs == true){
	  temp1 = _inputMap->getDataWin<double>(_elevLayerNum,cellRow +1, cellCol-1 );
	  temp2 = _inputMap->getDataWin<double>(_elevLayerNum,cellRow -1, cellCol-1 );
	  if(cellCol -1 >= 0 && cellRow+1 < _numCols -1 && cellRow-1 >= 0 )
	  if(temp1 != noDataVal && temp2 != noDataVal){
	    
	    if(fabs(currentElev - temp2 ) < unpassObs && fabs(currentElev -temp2 ) < unpassObs){
	      westGrad = 0;
	    }
	  }
	}

    }
  
  

    maxGradient = fmax(northGrad, fmax(southGrad, fmax(eastGrad, westGrad)));
    if(_mapperOpts.optZeroGradSpeedAsRDDFPercentage == 0){
      if(_mapperOpts.optVeloGenerationAlg ==2){
	maxVelo = generateVeloFromGrad2(maxGradient, _mapperOpts.optZeroGradSpeed, _mapperOpts.optUnpassableObsHeight,_mapperOpts.optVeloTweak1,_mapperOpts.optVeloTweak2);
	stdDevVelo = generateVeloFromGrad2(stdDev*_mapperOpts.optStdDevScaling, _mapperOpts.optZeroGradSpeed, _mapperOpts.optUnpassableObsHeight, _mapperOpts.optVeloTweak1, _mapperOpts.optVeloTweak2);
      }
      else if(_mapperOpts.optVeloGenerationAlg ==3){
	maxVelo = generateVeloFromGrad3(maxGradient, _mapperOpts.optZeroGradSpeed, _mapperOpts.optUnpassableObsHeight,_mapperOpts.optVeloTweak1,_mapperOpts.optVeloTweak2);
	stdDevVelo = generateVeloFromGrad3(stdDev*_mapperOpts.optStdDevScaling, _mapperOpts.optZeroGradSpeed, _mapperOpts.optUnpassableObsHeight, _mapperOpts.optVeloTweak1, _mapperOpts.optVeloTweak2);
 }
      else{
	maxVelo = generateVeloFromGrad(maxGradient, _mapperOpts.optZeroGradSpeed, _mapperOpts.optUnpassableObsHeight);
	stdDevVelo = generateVeloFromGrad(stdDev*_mapperOpts.optStdDevScaling, _mapperOpts.optZeroGradSpeed, _mapperOpts.optUnpassableObsHeight);
      }
    } else{

      if(_mapperOpts.optVeloGenerationAlg ==2){
	maxVelo = generateVeloFromGrad2(maxGradient, _mapperOpts.optZeroGradSpeed*corridorReference, _mapperOpts.optUnpassableObsHeight,_mapperOpts.optVeloTweak1,_mapperOpts.optVeloTweak2);
	stdDevVelo = generateVeloFromGrad2(stdDev*_mapperOpts.optStdDevScaling, _mapperOpts.optZeroGradSpeed*corridorReference, _mapperOpts.optUnpassableObsHeight, _mapperOpts.optVeloTweak1, _mapperOpts.optVeloTweak2);
      }
      else if(_mapperOpts.optVeloGenerationAlg ==3){
       	maxVelo = generateVeloFromGrad3(maxGradient, _mapperOpts.optZeroGradSpeed*corridorReference, _mapperOpts.optUnpassableObsHeight,_mapperOpts.optVeloTweak1, _mapperOpts.optVeloTweak2);
     stdDevVelo = generateVeloFromGrad3(stdDev*_mapperOpts.optStdDevScaling, _mapperOpts.optZeroGradSpeed*corridorReference, _mapperOpts.optUnpassableObsHeight, _mapperOpts.optVeloTweak1, _mapperOpts.optVeloTweak2);
      }
      else{
	maxVelo = generateVeloFromGrad(maxGradient, _mapperOpts.optZeroGradSpeed*corridorReference, _mapperOpts.optUnpassableObsHeight);
	stdDevVelo = generateVeloFromGrad(stdDev*_mapperOpts.optStdDevScaling, _mapperOpts.optZeroGradSpeed*corridorReference, _mapperOpts.optUnpassableObsHeight);
      }
    }


        if (_nonBinary == 1)
        {
	  if(roadReference != _inputMap->getLayerNoDataVal<double>(_roadLayerNum)){
	    if(maxVelo <= roadReference){
	      maxVelo = maxVelo + (roadReference-(pow(maxVelo-roadReference/2,2))*(4/roadReference))*_mapperOpts.optRoadScaling;
	    }
	    else{
	      //prolly don't need to do anything
	    }
	  }
	}
        else
	  {
	    if (maxGradient > 0.3) {
	      maxVelo = _inputMap->getLayerNoDataVal<double>(_costLayerNum);
	    } else {
	      maxVelo = corridorReference;
	    }
	  }

	if(stdDevVelo != 0){//make sure we have data
	  if(maxVelo > .01 && maxVelo != noData) {  //make sure it's not an obstacle
	  maxVelo = maxVelo*(1-_mapperOpts.optStdDevWeight) +  stdDevVelo * _mapperOpts.optStdDevWeight;
	  }
	}
#warning "add a better fusion algorithm here";
	//   if(corridorReference != _inputMap->getLayerNoDataVal<double>(_corridorLayerNum) || corridorReference != _inputMap->getLayerOutsideMapVal<double>(_corridorLayerNum)){

    }
   else
     {
	 maxVelo = _mapperOpts.corridorOpts.optMaxSpeed;
     }
    


        if (currentVelo != maxVelo && maxVelo != _inputMap->getLayerNoDataVal < double >(_costLayerNum) &&
	    corridorReference != _inputMap->getLayerNoDataVal<double>(_corridorLayerNum))
	  
	  {
	    distance = pow((pow((cellRow-_numRows/2)*resRows ,2.0  )+pow((cellCol-_numCols/2)*resCols,2.0)),.5);
	    if(distance > _mapperOpts.optObstacleDistanceLimit ){
	      maxVelo = fmax( _mapperOpts.optDistantObstacleSpeed, maxVelo);
	    }

         _inputMap->setDataWin_Delta < double >(_costLayerNum, cellRow, cellCol,
						maxGradient);
						//fmax(fmin(fmin (maxVelo, corridorReference),_mapperOpts.optMaxSpeed),  _inputMap->getLayerNoDataVal<double>(_costLayerNum)));
       }
    
  }
  return 0;
}
#endif


double
CCostPainter::generateVeloFromGrad (double gradient, double zeroGradSpeed,
                                    double unpassableObsHeight)
{
  /*                    ^
   *    zeroGradSpeed ->+
   *                    |\
   *                    | \
   *                    |  \
   *             0.0001 +---+->
   *                        ^
   *                        |
   *                        unpassableGradient
   */
double temp = fabs(gradient);
  if (temp < unpassableObsHeight)
  {
    return (unpassableObsHeight - temp) * zeroGradSpeed / unpassableObsHeight;
  }
  else
  {
   return _inputMap->getLayerNoDataVal<double>(_costLayerNum);
  }
}

double CCostPainter::generateVeloFromGrad2(double gradient, double zeroGradSpeed, double unpassableObsHeight, double tweak1, double tweak2){
  double temp = fabs(gradient);
  
  if(temp < unpassableObsHeight)
    { //tweak1 like obsHeight
      if(temp <= tweak1){
	return ((tweak1 - temp)*zeroGradSpeed + temp*tweak2)/tweak1 ;
      }
      else{
	return (unpassableObsHeight - (temp-tweak1)*unpassableObsHeight/(unpassableObsHeight - tweak1))*tweak2 / unpassableObsHeight  ;
      }
    }
  else
    {
      return 0.0001;
    }
  
  
}
 
double CCostPainter::generateVeloFromGrad3(double gradient, double zeroGradSpeed, double unpassableObsHeight, double tweak1, double tweak2){
   double temp = fabs (gradient);
  if (temp < unpassableObsHeight){
   temp =  pow((unpassableObsHeight - pow(temp,tweak2)/pow(unpassableObsHeight,tweak2 - 1)) * zeroGradSpeed / unpassableObsHeight,tweak1)*pow(zeroGradSpeed,1.0-tweak1);
 
 
   // cout << tweak1 <<  " t1 t2 " << tweak1 << endl;
  //  cout << gradient << " gr  velo  " << temp  << endl;
  return temp;
  }
 else{
    return _inputMap->getLayerNoDataVal<double>(_costLayerNum);
  }
}
 
/*
int
CCostPainter::growChanges (double UTMNorthing, double UTMEasting)
{
  double rowRes = _inputMap->getResRows ();
  double colRes = _inputMap->getResCols ();

  int minRow, minCol, maxRow, maxCol;

  minRow = (int) (-1.0 * _radius / rowRes) - 1;
  minCol = (int) (-1.0 * _radius / colRes) - 1;
  maxRow = (int) (_radius / rowRes) + 1;
  maxCol = (int) (_radius / colRes) + 1;

  int origRow, origCol;

  int localRow, localCol;

  _inputMap->UTM2Win (UTMNorthing, UTMEasting, &origRow, &origCol);

  double blNorthing, blEasting, tlNorthing, tlEasting;
  double trNorthing, trEasting, brNorthing, brEasting;

  double blDist, tlDist, trDist, brDist;

  int *rowMask;
  int *colMask;

  rowMask = new int[(maxRow - minRow) * (maxCol - minCol)];
  colMask = new int[(maxRow - minRow) * (maxCol - minCol)];

  int numPoints = 0;

  double minCost = 999.0;

  //minCost = ((double)(rand()%100))/-100.0*6.0;

  double localCost;

  double noData = _inputMap->getLayerNoDataVal < double >(_costLayerNum);

  for (int row = minRow; row <= maxRow; row++)
  {
    for (int col = minCol; col <= maxCol; col++)
    {
      localRow = row + origRow;
      localCol = col + origCol;

      localCost =
        _inputMap->getDataWin < double >(_costLayerNum, localRow, localCol);

      if (localCost != _inputMap->getLayerOutsideMapVal <
          double >(_growLayerNum))
      {
        _inputMap->Win2UTM (localRow, localCol, &blNorthing, &blEasting);
        tlNorthing = blNorthing + rowRes;
        trNorthing = blNorthing + rowRes;
        brNorthing = blNorthing;

        brEasting = blEasting + colRes;
        trEasting = blEasting + colRes;
        tlEasting = blEasting;

        blDist =
          sqrt (pow (blNorthing - UTMNorthing, 2) +
                pow (blEasting - UTMEasting, 2));
        brDist =
          sqrt (pow (brNorthing - UTMNorthing, 2) +
                pow (brEasting - UTMEasting, 2));
        tlDist =
          sqrt (pow (tlNorthing - UTMNorthing, 2) +
                pow (tlEasting - UTMEasting, 2));
        trDist =
          sqrt (pow (trNorthing - UTMNorthing, 2) +
                pow (trEasting - UTMEasting, 2));

        if (_conservativeGrowing)
        {
          if (blDist < _radius || brDist < _radius ||
              tlDist < _radius || trDist < _radius)
          {
            rowMask[numPoints] = localRow;
            colMask[numPoints] = localCol;
            if (localCost < minCost && localCost != noData)
              minCost = localCost;
            numPoints++;
          }
        }
        else
        {
          if (blDist < _radius && brDist < _radius &&
              tlDist < _radius && trDist < _radius)
          {
            rowMask[numPoints] = localRow;
            colMask[numPoints] = localCol;
            if (localCost < minCost && localCost != noData)
              minCost = localCost;
            numPoints++;
          }

        }

      }
    }
  }

  for (int i = 0; i < numPoints; i++)
  {
    if (_inputMap->getDataWin <
        double >(_growLayerNum, rowMask[i], colMask[i]) > minCost)
    {
      _inputMap->setDataWin_Delta < double >(_growLayerNum, rowMask[i],
                                             colMask[i], minCost);
    }
  }
  return 1;
}
*/
/*
int
CCostPainter::growChanges (NEcoord exposedArea[])
{
  int minRow, maxRow, minCol, maxCol;

  _inputMap->UTM2Win (exposedArea[0].N, exposedArea[0].E, &minRow, &minCol);
  _inputMap->UTM2Win (exposedArea[2].N, exposedArea[2].E, &maxRow, &maxCol);

  // force the values into range
  minRow = max (minRow - 2, 0);
  minCol = max (minCol - 2, 0);
  maxRow = min (maxRow + 2, _inputMap->getNumRows () - 1);
  maxCol = min (maxCol + 2, _inputMap->getNumCols () - 1);

  double tempN, tempE;

  for (int row = minRow; row <= maxRow; row++)
  {
    for (int col = minCol; col <= maxCol; col++)
    {
      _inputMap->Win2UTM (row, col, &tempN, &tempE);
      growChanges (tempN, tempE);
    }
  }

  return 0;
}
*/
/*
int CCostPainter:: calcAreaGradient( double UTMNorthing, double UTMEasting, double & gradientNorthing, double & gradientEasting){

int areaDimension = 2;
int nCount, eCount;
int i;

double n1, n2;
double e1, e2;

 double resRows = _inputMap->getResRows();
 double resCols = _inputMap->getResCols();

 i = 1; 
 n1 =  _inputMap->getDataUTM<double>(_elevLayerNum, UTMNorthing + areaDimension , UTMEasting);
 while ( n1 == _inputMap->getLayerNoDataVal<double>(_elevLayerNum ) && i < areaDimension   ){
   n1 =  _inputMap->getDataUTM<double>(_elevLayerNum, UTMNorthing + areaDimension - i , UTMEasting);
   i++;
 }
 if(n1 ==  _inputMap->getLayerNoDataVal<double>(_elevLayerNum) ){
   return 0;  }
 
 nCount = areaDimension + 2 - i;
 
 i = 1; 
 n2 =  _inputMap->getDataUTM<double>(_elevLayerNum, UTMNorthing - areaDimension , UTMEasting);
 while ( n1 == _inputMap->getLayerNoDataVal<double>(_elevLayerNum ) && i < areaDimension   ){
   
   n2 =  _inputMap->getDataUTM<double>(_elevLayerNum, UTMNorthing + areaDimension + i , UTMEasting);
   i++;
 }
 nCount += areaDimension + 1 - i;
 if(2 == _inputMap->getLayerNoDataVal<double>(_elevLayerNum)){
   return 0;  }
 gradientNorthing = (n1-n2)/nCount/resRows;
 
 //take care of instance where a cell is surronded by no data
 
 i = 1; 
 e1 =  _inputMap->getDataUTM<double>(_elevLayerNum, UTMNorthing , UTMEasting + areaDimension);
 while ( e1 == _inputMap->getLayerNoDataVal<double>(_elevLayerNum ) && i < areaDimension   ){
   e1 =  _inputMap->getDataUTM<double>(_elevLayerNum, UTMNorthing + areaDimension - i , UTMEasting);
   i++;
 }
 
 if(e1 == _inputMap->getLayerNoDataVal<double>(_elevLayerNum)){
   return 0;  }
 eCount = areaDimension + 2 - i;
 
 i = 1; 
 e2 =  _inputMap->getDataUTM<double>(_elevLayerNum, UTMNorthing , UTMEasting + areaDimension);
 while ( e2 == _inputMap->getLayerNoDataVal<double>(_elevLayerNum ) && i < areaDimension   ){
   e2 =  _inputMap->getDataUTM<double>(_elevLayerNum, UTMNorthing + areaDimension + i , UTMEasting);
   i++;
 }
 
 if(e2 == _inputMap->getLayerNoDataVal<double>(_elevLayerNum)){
   return 0;  }
 eCount += areaDimension + 1 - i;
 gradientEasting = (e1-e2)/eCount/resCols;
 
return 1;

}
*/

/*
double CCostPainter::gradientCorrection(double gradient, double areaGradient, double gradScaling){
  gradient =   fabs(gradient - areaGradient*gradScaling); 
  return gradient;
}
*/
void CCostPainter::setMapperOpts(FusionMapperOptions *  mapperOpts)
{
  _mapperOpts =* mapperOpts;

  int kernelRowSize = _mapperOpts.kernelSize;
  int kernelColSize = kernelRowSize;
	double sigma = _mapperOpts.sigma;

	for(int r=0; r<kernelRowSize*2+1; r++) {
		for(int c=0; c<kernelColSize*2+1; c++) {
			//Make a simple gaussian kernel
			logKernel[c+ r*(kernelRowSize*2+1)] = exp(-(pow((double)(r-kernelRowSize),2.0)+pow((double)(c-kernelColSize),2.0))/(2.0*pow(sigma,2.0))) /
				(sigma*sqrt(2*M_PI));
		}
	}
}


int CCostPainter::repaintAll(bool paintSurrounding) {
  double UTMNorthing, UTMEasting;

  for(int row=0; row < _inputMap->getNumRows(); row++) {
    for(int col=0; col < _inputMap->getNumCols(); col++) {
      _inputMap->Win2UTM(row, col, &UTMNorthing, &UTMEasting);
      paintChanges(UTMNorthing, UTMEasting, paintSurrounding);
    }
  }

  return 1;
}


// int CCostPainter::uberRepaintAll(bool paintSurrounding) {
//   double UTMNorthing, UTMEasting;

//   for(int row=0; row < _inputMap->getNumRows(); row++) {
//     for(int col=0; col < _inputMap->getNumCols(); col++) {
//       _inputMap->Win2UTM(row, col, &UTMNorthing, &UTMEasting);
//       paintChanges(UTMNorthing, UTMEasting, paintSurrounding);
//     }
//   }

//   return 1;
// }


int CCostPainter::paintChanges(double UTMNorthing, double UTMEasting, bool paintSurrounding) {
	//Are we inside the map bounds?
  if (_inputMap->checkBoundsUTM (UTMNorthing, UTMEasting) != CMap::CM_OK)
    return 0;
	//Are we inside the RDDF?
  if(_inputMap->getDataUTM<double>(_corridorLayerNum, UTMNorthing, UTMEasting) ==
     _inputMap->getLayerNoDataVal<double>(_corridorLayerNum))
    return 0;
	//Do we have elevation data?
	if(_inputMap->getDataUTM<CElevationFuser>(_fusedLayerNum, UTMNorthing, UTMEasting) ==
		 _inputMap->getLayerNoDataVal<CElevationFuser>(_fusedLayerNum))
		return 0;

	//Initialize some variables for later use
  double finalSpeed = MAX_SPEED;
  double obstacleSpeed = MAX_SPEED;
  double speedNoDataVal = _inputMap->getLayerNoDataVal<double>(_costLayerNum);

  int currentRow, currentCol;
  int kernelRowSize, kernelColSize;
  _inputMap->UTM2Win(UTMNorthing, UTMEasting, &currentRow, &currentCol);
	kernelRowSize = _mapperOpts.kernelSize;
	kernelColSize = kernelRowSize;
	
	//Bound the kernel if we're close to the edge of the map
  int minRow, maxRow, minCol, maxCol;
  constrainRows(currentRow, kernelRowSize, minRow, maxRow);
  constrainCols(currentCol, kernelColSize, minCol, maxCol);
  double roadReference =  _inputMap->getDataWin < double >(_roadLayerNum, currentRow, currentCol);
  double roadNoDataSpeed = _inputMap->getLayerNoDataVal<double> (_roadLayerNum);
  double resRows = _inputMap->getResRows();
  double resCols = _inputMap->getResCols();

  double cellElev;
	CElevationFuser cellFused;
  CElevationFuser fusedNoDataVal = _inputMap->getLayerNoDataVal<CElevationFuser>(_fusedLayerNum);
	double otherN, otherE;
	double totalGrad = 0.0;
	int numSupportingCellsUsed = 0;
  CElevationFuser fusedElevData = _inputMap->getDataUTM<CElevationFuser>(_fusedLayerNum, UTMNorthing, UTMEasting);
	double currentCellElev = fusedElevData.getMeanElevation();

  if(fusedElevData.getStdDev() > _mapperOpts.stdDevThreshold) {
    obstacleSpeed = speedNoDataVal+0.0001;
  }

  for(int r = minRow; r<=maxRow; r++) {
    for(int c = minCol, i=0; c<=maxCol; c++, i++) {
      cellFused = _inputMap->getDataWin<CElevationFuser>(_fusedLayerNum, r, c);
      if(cellFused != fusedNoDataVal && !(r==currentRow && c==currentCol)) {
				totalGrad += fabs((currentCellElev - cellFused.getMeanElevation())*logKernel[c-currentCol + kernelColSize + (kernelRowSize*2+1)*(r-currentRow + kernelRowSize)]);
				numSupportingCellsUsed++;
				if(paintSurrounding) {
					_inputMap->Win2UTM(r,c,&otherN, &otherE);
					paintChanges(otherN, otherE, false);
				}
			}
    }
  }
	double avgGrad = totalGrad/((double)numSupportingCellsUsed);

	//We should use at least some number of cells before we think
	//we have enough information to assign this cell a speed
  if(numSupportingCellsUsed < _mapperOpts.minNumSupportingCells)
    return 0;


  if(_mapperOpts.optVeloGenerationAlg ==3){
    finalSpeed = generateVeloFromGrad3(fabs(avgGrad*100.0), _mapperOpts.optZeroGradSpeed, _mapperOpts.optUnpassableObsHeight,_mapperOpts.optVeloTweak1,_mapperOpts.optVeloTweak2);
  }
  else{
	finalSpeed = atan(fabs(avgGrad*100.0), _mapperOpts.optVeloTweak1, _mapperOpts.optZeroGradSpeed/(2.0*0.44704),
										-_mapperOpts.optZeroGradSpeed/(2.0*0.44704), _mapperOpts.optVeloTweak2, 0.44704);
  }

	//We want the smaller of speeds from our obstacle detector and our bumpiness detector
  finalSpeed = fmin(finalSpeed, obstacleSpeed);

  paintRoad(&finalSpeed,&roadReference,&_mapperOpts.optRoadScaling,&roadNoDataSpeed);


	//And we want the smaller of speeds from above and the RDDF
  finalSpeed = fmin(finalSpeed, _inputMap->getDataUTM<double>(_corridorLayerNum, UTMNorthing, UTMEasting));
	//And of course, we don't want a speed higher than our max speed
	finalSpeed = fmin(finalSpeed, _mapperOpts.optMaxSpeed);
	//And we want the larger of speeds from above and the min speed of the map
  finalSpeed = fmax(finalSpeed, speedNoDataVal + EPSILON_SPEED);

  //don't paint obstacles at a far distance
  if(_mapperOpts.optObstacleDistanceLimit < 200){  //no need to waste time computing stuff we won't use
    double distance = pow((pow((currentRow-_numRows/2)*resRows ,2.0  )+pow((currentCol-_numCols/2)*resCols,2.0)),.5);
    if(distance > _mapperOpts.optObstacleDistanceLimit ){
      finalSpeed = fmax( _mapperOpts.optDistantObstacleSpeed, finalSpeed);
    }
  }



	//Paint our speed
  _inputMap->setDataUTM_Delta<double>(_costLayerNum, UTMNorthing, UTMEasting, finalSpeed);

	//Now we do some interpolation
  constrainRows(currentRow, _mapperOpts.maxInterpolateCells, minRow, maxRow);
  constrainCols(currentCol, _mapperOpts.maxInterpolateCells, minCol, maxCol);

  int minLocalRow, maxLocalRow, minLocalCol, maxLocalCol;
  double localCellCost;
	CElevationFuser localCellFused;
  double localNorthing, localEasting;
	double otherCost;

  for(int r=minRow; r<=maxRow; r++) {
    for(int c=minCol; c<=maxCol; c++) {
      cellFused = _inputMap->getDataWin<CElevationFuser>(_fusedLayerNum, r, c);
			otherCost = _inputMap->getDataWin<double>(_costLayerNum, r, c);
      if(cellFused == fusedNoDataVal) {
				constrainRows(r, 1, minLocalRow, maxLocalRow);
				constrainCols(c, 1, minLocalCol, maxLocalCol);
				double localCost=0.0;
				int numSupporters=0;
				_inputMap->Win2UTM(r, c, &localNorthing, &localEasting);
				for(int localR=minLocalRow; localR<=maxLocalRow; localR++) {
					for(int localC=minLocalCol; localC<=maxLocalCol; localC++) {
						localCellCost = _inputMap->getDataWin<double>(_costLayerNum, localR, localC);
						localCellFused = _inputMap->getDataWin<CElevationFuser>(_fusedLayerNum, localR, localC);
						if(localCellFused != fusedNoDataVal) {
							localCost+=localCellCost;
							numSupporters++;
						}
					}
				}

				if(numSupporters >= _mapperOpts.minNumSupportingCellsForInterpolation) {
					_inputMap->setDataWin_Delta<double>(_costLayerNum, r, c, fmax(speedNoDataVal+EPSILON_SPEED, localCost/((double)numSupporters)));
				}
      }
    }
  }

	return 0;
}


int CCostPainter::paintChangesNotify(double UTMNorthing, double UTMEasting, CElevationFuser::STATUS forgetSurrounding) {
	int currentRow, currentCol, minRow, minCol, maxRow, maxCol;
	int minRow2, minCol2, maxRow2, maxCol2;
  int kernelRowSize = _mapperOpts.kernelSize;
  int kernelColSize = kernelRowSize;
	_inputMap->UTM2Win(UTMNorthing, UTMEasting, &currentRow, &currentCol);

	unsigned long long neighborsTimestamp = _inputMap->getDataWin<CElevationFuser>(_fusedLayerNum, currentRow, currentCol).getTimestamp();
	CElevationFuser::STATUS forgetMoreSurrounding;

  constrainRows(currentRow, kernelRowSize, minRow, maxRow);
  constrainCols(currentCol, kernelColSize, minCol, maxCol);
	for(int r=minRow; r	<= maxRow; r++) {
		for(int c=minCol; c<=maxCol; c++) {
			_inputMap->setDataWin_Delta<int>(_fusedYetLayerNum, r, c, 1);			
		}
	}

  constrainRows(currentRow, kernelRowSize*2, minRow, maxRow);
  constrainCols(currentCol, kernelColSize*2, minCol, maxCol);
	for(int r=minRow; r <= maxRow; r++) {
		for(int c=minCol; c <= maxCol; c++) {
			CElevationFuser& tempCell2 = _inputMap->getDataWin<CElevationFuser>(_fusedLayerNum, r, c);
			tempCell2.resetNoData(neighborsTimestamp);
		}
	}

	return 1;
}

int CCostPainter::paintNotifications() {
	//CDeltaList* deltaList = _inputMap->serializeDelta<int>(_fusedYetLayerNum, 0);

	double UTMNorthing=0.0, UTMEasting=0.0;

	int cellsize = _inputMap->getCurrentDeltaSize(_fusedYetLayerNum);
	//cout << "got " << cellsize << " different cells " << endl;
	for(int i=0; i<cellsize; i++) {
		int resultCell = _inputMap->getCurrentDeltaVal<int>(i, _fusedYetLayerNum, &UTMNorthing, &UTMEasting);				
		//cout << "Cell at " << UTMNorthing << ", " << UTMEasting << " = " << resultCell << endl;
		paintChanges(UTMNorthing, UTMEasting, false);
	}

	_inputMap->resetDelta<int>(_fusedYetLayerNum);

	return 1;
}


void CCostPainter::constrainRows(int currentRow, int numRows, int& minRow, int& maxRow) {
  constrainRange(currentRow, numRows, 0, _inputMap->getNumRows()-1, minRow, maxRow);
}

void CCostPainter::constrainCols(int currentCol, int numCols, int& minCol, int& maxCol) {
  constrainRange(currentCol, numCols, 0, _inputMap->getNumCols()-1, minCol, maxCol);
}

void CCostPainter::constrainRange(int start, int numDiff, int min, int max, int& resultMin, int& resultMax) {
  if(start - numDiff < min) {
    resultMin = min;
  } else {
    resultMin = start - numDiff;
  }

  if(start + numDiff > max) {
    resultMax = max;
  } else {
    resultMax = start + numDiff;
  }
}

  //Returns multipler*(a/(gradient+b)+c)
double CCostPainter::inverse(double gradient, double a, double b, double c, double multiplier) {
  return multiplier*(a/(gradient+b)+c);
}


//Returns multiplier*(m*gradient+b)
double CCostPainter::linear(double gradient, double m, double b, double multiplier) {
  return multiplier*(m*gradient+b);
}


//finalSpeed = 0.44704*(-15*2/M_PI*tanh(5.0*(fabs(avgGrad*100.0) - 0.5))+15.0);
double CCostPainter::atan(double val, double xCenter, double yCenter, double verticalHalfRange, double steepness, double multiplier) {
	return multiplier*(verticalHalfRange*2/M_PI*tanh(steepness*(val - xCenter)) + yCenter);
}

void CCostPainter::paintRoad(double * terrainSpeed, double * roadSpeed, double * roadScaling, double * noData){

  if(*roadSpeed != *noData){
    if(*terrainSpeed <= *roadSpeed){
      *terrainSpeed += (*roadSpeed-(pow(*terrainSpeed-*roadSpeed/2,2))*(4/ *roadSpeed))* *noData;
    }
    else{
      //prolly don't need to do anything
    }
  }

}
