function corridor = file2corridor(file)
% 
% function corridor = file2corridor(file)
%
% Changes: 
%   12-7-2004, Jason Yosinski, created (some code from plot_corridor.m)
%
% Function:
%   This function reads a *.bob format RDDF file and outputs the
%   corrosponding corridor matrix, to be used with such funcions as
%   path_from_corridor.m
%
% Input:
%   file = 'filename.fileextension'
%
% Output:
%   corridor = [x1 y1 width1 sl1;
%               x2 y2 width2 sl2;
%               .. .. ...... ...
%               xN yN widthN slN]
%       where width1 is the width of the corridor between (x1, y1) and 
%       (x2, y2).  sl1 is the speed limit imposed by the rddf on the first
%       corridor.  widthN and slN are irrelevant.
%
% Usage example:
%   corridor = file2corridor('example_corridor.bob');
%

% read in values from the file (deprecated)
%[type,eastings,northings,radii,vees] = ...
%	  textread(file, '%s%f%f%f%f', 'commentstyle', 'shell');

% if we change the first column (unused, anyhow) of the Bob format to a number 
% instead of a character, then we can just load the file directly (better, 
% for cross-compatibility with Octave)
data = load(file);
   
% the column format of data is [type, easting, northing, radius, speed]
corridor = data(:,2:5);
