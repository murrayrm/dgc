LATLONG_PATH = $(DGC)/util/latlong

LATLONG_DEPEND = \
	$(LATLONG_PATH)/LatLong.cpp \
	$(LATLONG_PATH)/LatLong.h \
	$(LATLONG_PATH)/contest-info.h \
	$(LATLONG_PATH)/convert.cpp \
	$(LATLONG_PATH)/ggis.c \
	$(LATLONG_PATH)/ggis.h \
	$(LATLONG_PATH)/latlon.dat \
	$(LATLONG_PATH)/test-latlon-to-utm.c \
	$(LATLONG_PATH)/test-utm-to-latlon.c
