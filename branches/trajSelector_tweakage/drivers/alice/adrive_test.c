// initial cut at driving software 12/26/04 ... a total hack

#include <stdio.h>
#include <iostream.h>
#include <math.h>
#include <sys/time.h>
#include <stdlib.h>
#include <curses.h>
#include <unistd.h>
#include <getopt.h>


#include "parker_steer.h"
#include "serial.h"
#include "brake.h"
#include "throttle.h"
#include "vehports.h"
#include "constants.h"

#include "sparrow/display.h"
#include "sparrow/dbglib.h"
#include "sparrow/flag.h"
#include "sparrow/keymap.h"

// FOr threading tully
#ifdef THREADING
#include "../adrive/adrive.h"
#endif //THEADING

char c;


//throttle inits
#define THROTTLE
int throttle_command(void);
int throttle_flag = 0;   //0 for unknown, 1 for enable, -1 for disable
pthread_t throttle_thread;
void *throttle_start(void *);
double throttle_position_send = 0;
double current_throttle_position;
//double throttle_position = 0;

//brake inits
#define BRAKE
int brake_command(void);
int brake_flag = 0; //0 for diable, 1 for enable
pthread_t brake_thread;
void *brake_start(void *);
double brake_position_send = 0;
double current_brake_position;
//double brake_position = 0;

//steering inits
#define STEER
int steer_command(void);
int steer_flag = 0;  //-1 for disable, 0 for unknown, 1 for opened
pthread_t steer_thread;
void *steer_start(void *);
double steer_position_send;
double steer_position = 0;
double steer_convert;
double steer_position_temp;

static char *short_options = "vbts";
static char *usage = "\
Usage: %s [-v] [options]\n\
 -b   disable braking system\n\
 -t   disable throttle system\n\
 -s   disable steering system\n\
";

int main(int argc, char **argv)
{
  int c;
  int error = 0;
  while ((c=getopt_long(argc, argv, short_options, NULL, NULL))!=EOF)
    switch(c)
    {
    case 'v': break;
    case 't': throttle_flag = -1; break;
    case 'b': brake_flag = -1; break;
    case 's': steer_flag = -1; break;
    default:  break;
    }
 

#ifdef THROTTLE
  if(throttle_flag == 0)
  {
    if(throttle_open(THROTTLE_SERIAL_PORT) != TRUE)
    {
      printf("Throttle open failed!!\n");
      error++;
      throttle_flag = 0;
    }
    else
    {
      throttle_flag = 1; //throttle opened and ready to go
      printf("Throttle OK\n");
    }
  }
#endif //THROTTLE

#ifdef BRAKE
  if(brake_flag == 0)
  {
    if(brake_open(BRAKE_SERIAL_PORT) != TRUE)
    {
      printf("Brake open failed\n");
      dbg_info("Brake open failed!\n");
      error++;
      brake_flag = 0;
    }
    else
    {
      brake_flag = 1;
      printf("Brake OK\n");
    }
  }
#endif //BRAKE

#ifdef STEER
  if(steer_flag == 0)
  {
    if(steer_open(STEER_SERIAL_PORT) != TRUE)
    {
      printf("Steer open failed, trying calibration\n");
      if(steer_calibrate() != 1)
      {
	printf("steer calibrate failed ... we are hosed\n");
	error++;
      }
      else
      {
	steer_flag = 1;
	printf("Steer OK\n");
      }
    }
    else
    {
      steer_flag = 1;
      printf("Steer_OK\n");
    }
  }
#endif

  printf("steer_flag = %d, brake_flag = %d, throttle_flag = %d\n",
	 steer_flag, brake_flag, throttle_flag);
  if(error > 0)
  {
    printf("Errors on startup, continue (y/n)?");
    if(getchar() != 'y') exit(1);
  }


#ifdef THROTTLE
  printf("starting throttle thread\n");
  pthread_create(&throttle_thread, NULL, throttle_start, (void *) NULL);
#endif //throttle


#ifdef BRAKE
  printf("starting brake thread\n");
  pthread_create(&brake_thread, NULL, brake_start, (void *) NULL);
#endif //brake


#ifdef STEER
  printf("starting steering thread\n");
  pthread_create(&steer_thread, NULL, steer_start, (void *) NULL);
#endif //steer


  while(1){
    printf("press t for throttle\n");
    printf("press b for brake\n");
    printf("press s for steer\n");
    printf("press c to calibrate steering\n");
    printf("press e to clear torque fault or re-enable motor\n"); 
    printf("press d to disable steering motor temporarily\n");
    printf("press q to quit program\n");

    if(throttle_flag != -1)
    {
      current_throttle_position = throttle_getposition();
      usleep(10000);
      printf("Throttle Position = %0.2f\n", current_throttle_position);
    }

    if(brake_flag != -1)
    {
      current_brake_position = brake_getposition();
      printf("Brake Position = %0.2f\n", current_brake_position);
    }

    if(steer_flag != -1)
    {
      steer_state_update(FALSE,FALSE);
      steer_position = steer_getposition();
      printf("Steering Position = %0.2f\n", steer_position);
    }

    c = getchar();
    switch(c)
    {
    case 't':
      system("clear");
      printf("enter value for throttle command (0-1): ");
      cin >> throttle_position_send;
      printf("Throttle set to %0.2f ... press enter to continue\n", throttle_position_send);
      throttle_setposition(throttle_position_send);
      system("clear");
      break;
    case 'b':
      system("clear");
      printf("enter value for brake command (0-1): ");
      cin >> brake_position_send;
      printf("Brake set to %0.2f ... press enter to continue\n", brake_position_send);
      brake_setposition(brake_position_send);
      system("clear");
      break;
    case 's':
      
      system("clear");
      printf("enter value for steer command (-1 to 1): ");
      cin >> steer_position_send;
      printf("Steering set to %0.2f ... press any key to continue\n", steer_position_send);
      steer_heading(steer_position_send);
      system("clear");
      break;
    case 'c':
      getchar();
      system("clear");
      printf("Calibrating steering ...\n");
      steer_calibrate();
      system("clear");
      break;
    case 'e':
      getchar();
      system("clear");
      printf("enabling drive ...\n");
      steer_exec_cmd(S_DRIVE1, TRUE);
      system("clear");
      break;
    case 'd':
      system("clear");
      printf("disabling drive ...\n");
      steer_disable();
      system("clear");
      break;
    case 'q':
      if(throttle_flag != -1)
      {
	throttle_zero();
	throttle_close();
      }
      exit(1);
      break;
    default: 
      system("clear");
      break;
    }
  }


} //end main

#ifdef THROTTLE
void *throttle_start(void *arg)
{
  //cout << "throttle thread test";
  //throttle_setposition(throttle_position_send);
}
#endif //throttle


#ifdef BRAKE
void *brake_start(void *arg)
{
  //cout << "brake thread test";
  //brake_setposition(brake_position_send);
}
#endif //brake


#ifdef STEER
void *steer_start(void *arg)
{
  //cout << "steer thread test";
  //steer_state_update(FALSE,FALSE);
  //steer_position = steer_getposition();
  //steer_heading(steer_position_send);
}
#endif //steer

#ifdef THROTTLE
int throttle_command(void){
  system("clear");
  printf("current throttle position: %0.2f\n", throttle_position_send);
  printf("please enter a throttle command (0 - 1) (q quits to menu): \n");
  cin>>throttle_position_send;
  if(throttle_position_send = 'q') return TRUE;
  else printf("throttle set to %0.2f\n",throttle_position_send);
  throttle_command();
}
#endif //throttle


#ifdef BRAKE
int brake_command(void){
  system("clear");
  printf("current brake position: %0.2f\n", brake_position_send);
  printf("please enter a brake command (0-1) (q quits to menu): \n");
  cin>>brake_position_send;
  if(brake_position_send == 'q') return TRUE;
  else printf("brake set to %0.2f\n", brake_position_send);
  brake_command();
}
#endif //brake


#ifdef STEER
int steer_command(void){
  system("clear");
  printf("current steering position: %0.2f\n", steer_position);
  printf("please enter a steering command (-1 to 1) (q quits to menu): \n");
  cin>>steer_position_temp;
  if(steer_position_temp == 'q') return TRUE;
  else{ 
    steer_position_send = steer_position_temp;
    printf("steering set to %0.2f\n ... press enter to continue", steer_position_send);
    getchar();
    getchar();
    steer_command();
  }
}
#endif //steer
