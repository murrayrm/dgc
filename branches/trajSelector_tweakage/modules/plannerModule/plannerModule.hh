#ifndef PLANNERMODULE_HH
#define PLANNERMODULE_HH

#include <pthread.h>
#include "StateClient.h"
#include "TrajTalker.h"
#include "MapdeltaTalker.h"
#include "CMapPlus.hh"
#include "rddf.hh"
#include "MapConstants.h"
#include "planner.h"

class CPlannerModule : public CStateClient, public CTrajTalker, public CMapdeltaTalker
{
	CPlanner*					m_pPlanner;
  CMapPlus				 	m_map;
  int								m_mapLayer;
  RDDF							m_rddf;
	char*							m_pMapDelta;

	/*! Whether at least one delta was received */
	bool							m_bReceivedAtLeastOneDelta;
	pthread_mutex_t		m_deltaReceivedMutex;
	pthread_cond_t		m_deltaReceivedCond;

	/*! The mutex to protect the map we're planning on */
	pthread_mutex_t		m_mapMutex;

	/*! The skynet socket for sending trajectories (to mode managements) */
	int								m_trajSocket;

	/*! The skynet socket for communication with fusionmapper to request the full
		map */
	int               m_mapRequestSocket;

	/*! The current index of the snapshot data. Circumstances that generate fsailed
		plans get saved to filenames with this index */
	int               m_snapshotIndex;

	/*! Whether snapshots get taken or not */
	bool              m_bClobberSnapshots;

	void snapshot(void);

public:
  CPlannerModule(int sn_key, bool bWaitForStateFill, bool bClobberSnapshots);
  ~CPlannerModule();

  /*! Method to run in separate thread.  This function runs the dd_loop() 
   *  Sparrow method to update the screen and take keyboard commands. */
  void SparrowDisplayLoop();

  /*! Method to update the variables that are to be displayed in the Sparrow
   *  interface.  This method does not run a loop. */
  void UpdateSparrowVariablesLoop();

	/*! This is the function that continually runs the planner in a loop */
	void PlanningLoop(void);

	/*! this is the function that continually reads map deltas and updates the
		map */
	void getMapDeltasThread();

	/*! This function is used to request the full map from fusionmapper */
	void requestFullMap();
};

#endif
