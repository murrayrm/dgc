/* ModuleStarter.cc: Start up and kill modules*/

#include "ModuleStarter.hh"
#include <getopt.h>
/* The name of this program. */
const char * program_name;

#define KEY_OPT 13
#define DEBUG_MODE 14
int OPTKEY = -1;;

/* Prints usage information for this program to STREAM (typically stdout 
   or stderr), and exit the program with EXIT_CODE. Does not return. */
void print_usage (FILE* stream, int exit_code)
{
  fprintf( stream, "Usage:  %s --key skynet_key [options]\n", program_name );
  fprintf( stream,
	   "  --debug, -d specifies debug mode, so don't daemonize\n"
           "  --help, -h        Display this message.\n" );      
  exit(exit_code);
}

char my_comp[50];
int my_comp_id;

int main(int argc, char** argv)
{  
  int sn_key;
  int ch;
  bool debug = false;
  /* A string listing valid short options letters. */
  const char* const short_options = "h";
  /* An array describing valid long options. */
  static struct option long_options[] = 
    {
      // first: long option (--option) string
      // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
      // third: if pointer, set variable to value of fourth argument
      //        if NULL, getopt_long returns fourth argument    
      {"debug",      0, NULL,           DEBUG_MODE},
      {"key",        1, NULL,           KEY_OPT}, 
      {"help",       0, NULL,              'h'},
      {NULL,         0, NULL,              0}
  };  
  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  program_name = argv[0];
  printf("\n");

  // Loop through and process all of the command-line input options.
  while((ch = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
  {
    switch(ch)
      {
      case DEBUG_MODE:
	debug = true;
	break;
      case KEY_OPT:
	OPTKEY = atoi(optarg);
	break;
      case 'h':
	/* User has requested usage information. Print it to standard
           output, and exit with exit code zero (normal 
           termination). */
        print_usage(stdout, 0);
	break;
      case '?': /* The user specified an invalid option. */
        /* Print usage information to standard error, and exit with exit
           code one (indicating abnormal termination). */
        print_usage(stderr, 1);
	break;
      case -1: /* Done with options. */
        break;
      }
  }
  
  if(OPTKEY == -1) 
    {      
      print_usage(stdout, 0);
      exit(2);
    }
  else
    {
      cout << "Key Passed as commandline: " << OPTKEY << endl;
      sn_key = OPTKEY;
    }

  if(!debug)
    {
      //begin daemonizing
      pid_t pid = fork();
      if (pid != 0)
	{
	  exit(0);       //in parent
	}
      //in child
    }
  //printf("Starting Skynet with SKYNET_KEY %s\n\n",sn_key); 
  cout << "Starting Skynet with SKYNET_KEY" << sn_key  << endl;
  Skynet = new  skynet(myself,sn_key);  
  gethostname(my_comp,50);
  my_comp_id =   verify_configuration();      
  if(my_comp_id == -1)
    {
      cout << " Computer was not found in the config file  " << CONFIG_FILE_NAME <<   "\n Have a good day. Bye." << endl;
      exit(2);
    }
  //printf("my module id: %d\n\n", Skynet->get_id());
  //initialize the sending socket
  msgSendSock = Skynet->get_send_sock(SNGuiMsg);
  listen();
  delete Skynet;
  return(0);
}

void listen()
  {
    int sock1 = Skynet->listen(myinput, other_mod);            //getting message via skynet
    while(1)
      {                                                   //server loop
	int CMDSIZE = sizeof(struct command);
	struct command cmd;
	Skynet->get_msg(sock1, &cmd, CMDSIZE, 0); 
	char* snModuleName = cmd.type;
	//printf("received, %d, %s for %d\n\n",my_comp_id,my_comp,cmd.compID);	
	cout << "Recieved Message for Computer: " << cmd.compID << ". My id is " << my_comp_id << endl;	
	if(cmd.compID == my_comp_id)
	  {
	    switch(cmd.action)
	      {	  
	      case 'K':
	      case 'k':
		//cout << "Kill Action Undefined" << endl;
		cout << "Message is: Kill " << cmd.type << endl; 
		stopModule(snModuleName);
		break;
	      case 'S':
	      case 's':
		cout << "Message is: Start " << cmd.type << endl; 
		cmd.PID = spawn(snModuleName);		
		myModules.push_back(cmd);
		strcpy(message, "It appears module started successfully. \n");
		sendToGui();		
		break;
	      default:
		cout << "Unknown Action: " << cmd.action << endl;
		break;
	      }	
	  }
	else
	  {
	    cout << " Not my module: " << cmd.type << endl;
	  }
      }
  }

pid_t spawn(char* snModuleName)
{
  pid_t pid;
  if((pid =fork()) < 0) 
    {
      perror("fork error");
      strcpy(message, "Fork Error\n"); 
      sendToGui();
    }
  else
    {
      if(pid == 0)
	{
	  // only for the child process
	  spawnModule(snModuleName);
	  cerr << "Forked Module Starter exiting ....." << flush << endl;
	  cerr << "Please send a kill command to remove zombie process" << endl;
	  cerr << endl << endl;
	  strcpy(message, "Fork Failed \n. Send kill to remove zombie \n \n");
	  sendToGui();
	  exit(2);
	}
    }
  return pid;
}
void spawnModule(char* snModuleName)
{
  if(getPath(snModuleName))
    { 
      cout << "Pausing for: " << execCmd.pause << "ms" << endl;
      usleep(execCmd.pause);
      cout << "Executing: " << execCmd.snModName <<" "  << execCmd.cmd << " " << execCmd.opts << flush << endl;
      char* opts = (char*)execCmd.opts.c_str();      
      cerr << "Cmd is " << execCmd.cmd.c_str() << "Opts is " << opts << endl;       
      //int status =execl(execCmd.cmd.c_str(),execCmd.cmd.c_str(), opts, (char *)NULL );
      int status =execv(my_Argv[0],my_Argv );
      cout << "ModuleStarter Error: Module did not spawn " << status << endl;
      cout << "This could be caused by \n 1. The executable is not " \
	" in the current working directory" \
	"\n 2. The executable exited due to wrong options" \
	"\n 3. The executable is expecting user input " << endl;
      strcpy(message, "ModuleStarter Error: Module did not spawn"
	     "This could be caused by \n 1. The executable is not " \
	     " in the current working directory" \
	     "\n 2. The executable exited due to wrong options" \
	     "\n 3. The executable is expecting user input \n");
      sendToGui();
    }
  else
    {
      cout << "Command not found. Check your " << CONFIG_FILE_NAME << endl;
      strcpy(message,"Command not found. Check remote config file \n");
      sendToGui();
    }
}

int verify_configuration()
{      //Read the config file and find out the id of the computer
  int i=-1;
  FILE* fp;
  int ID;
  char comp_name[50];
  fp = fopen(CONFIG_FILE_NAME,"r");
  if(fp == NULL)
    {
      cerr << "File Open Error. " << CONFIG_FILE_NAME << " not found. Make sure file is in same directory as executable" << endl;
    }
  else
    {
      while(EOF!=fscanf(fp,"%d",&ID))
	{
	  fscanf(fp,"%s",comp_name);
	  if(strcmp(comp_name,my_comp) == 0)
	    i = ID;
	}
      fclose(fp);
    }
  return i;
}

void stopModule(char* snModuleName)
{
  list<struct command>::iterator i;
  //pid_t killed;
  int found = -1;
  cout << "Killing : " << snModuleName << endl;
  for(i = myModules.begin(); i!=myModules.end(); ++i)
    {
      if(strcmp(i->type,snModuleName) == 0 )
	{
	  if(kill(i->PID, SIGKILL) == 0)
	    {
	      int status;
	      found = waitpid(i->PID, &status, 0); // clear status
	      myModules.erase(i); // remove killed module from list
	      cout << i->type << " Killed" << endl;
	      strcpy(message, i->type);
	      sendToGui();
	      strcpy(message, " Killed \n");
	      sendToGui();
	      break;
	    }
	  else
	    {
	      strcpy(message, "Kill Failed \n");
	      sendToGui();
	      cerr << "Kill Failed " << endl;
	    }
	} 
    }
  if(found == -1)
    {
      strcpy(message, "Stop Failed: Module not found \n");
      sendToGui();
      cerr << "Stop Failed: Module not found" << endl;  
    }
}

pair<string::size_type, string::size_type> getWord(string line)
{
  string::size_type begIdx = line.find_first_not_of(delim);
  string:: size_type endIdx;
  if(begIdx != string::npos)
    {
      endIdx = line.find_first_of(delim,begIdx);
      if(endIdx == string::npos)
	{
	  endIdx = line.length();
	}	
    }
  return pair<string::size_type, string::size_type>(begIdx, endIdx);
}

bool getPath(char* snModName)
{
  string line;
  //begin cplusplussifying io
  std::ifstream configfile(CONFIG_FILE_NAME);

  /**
   * i don't like this code, its ugly
   * wish we had adecent tokenizer in the stl
   */
  if (! configfile )
    { 
      cerr << "Error opening " <<  CONFIG_FILE_NAME << " file. \n Make sure its in the same directory as the executable" << endl; 
      strcpy(message,"ModuleStarter: Error opening config file \n");
      sendToGui();
      exit (1); 
    }
  pair<string::size_type, string::size_type> word;
  while (getline(configfile, line))
    {
      if((! line.empty()) && (line[0] != '#')) // # at beginning of line denotes comment
	{	 	
	  word = getWord(line);
	  if(line.substr(word.first,word.second) == preModule)
	    {
	  
	      line.erase(word.first, (word.second-word.first)); // get rid of already used word	      
	      word = getWord(line);

	      if(snModName == line.substr(word.first,(word.second-word.first)))
		{
		  execCmd.snModName = line.substr(word.first,(word.second-word.first));		  
		  line.erase(word.first, (word.second-word.first));
		  word = getWord(line);		  
		  //ignore computer settings
		  line.erase(word.first, (word.second-word.first));
		  word = getWord(line);
		  //ignore checked field
		  line.erase(word.first, (word.second-word.first));
		  word = getWord(line);
		  execCmd.pause = atoi(line.substr(word.first,(word.second-word.first)).c_str());
		  line.erase(word.first, (word.second-word.first));
		  strcpy(cmdLineVal, line.c_str());
		  //cout << " before parse " << cmdLineVal << endl << endl;
		  parsePath();
		  /**
		     word = getWord(line);		  
		  execCmd.cmd = line.substr(word.first,(word.second-word.first));
		  line.erase(word.first, (word.second-word.first));
		  string::size_type begIdx = line.find_first_of(delim);
		  string::size_type endIdx = line.find_first_not_of(delim);
		  line.erase(begIdx, endIdx-begIdx);
		  execCmd.opts = line;*/
		  configfile.close();
		  return true;
		}
	    }
	}      
    }
  configfile.close();
  cerr << "ModuleStarter: Could not parse config file correctly " << endl;
  strcpy(message,"ModuleStarter: Could not parse config file correctly \n");
  sendToGui();
  return false;
}

void sendToGui()
{
  if(Skynet->send_msg(msgSendSock,message, sizeof(message), 0) != sizeof(message))
    {
      cerr << "Error sending mesg: " << message << flush <<  endl; 
    }
}

void printem()
{
  int k = 0;
  while(my_Argv[k] != NULL)
    {      
      cout << k << ": " << my_Argv[k] << endl;
      k++;
    }
}

void parsePath()
{
  int k = 0;
  my_Argv[k] = strtok(cmdLineVal," ");
  do
    {
      if(*(my_Argv[k]) != ' ')
	{
	  k++;
	}
    }
  while((my_Argv[k] = strtok(NULL," ")) != NULL);
  my_Argv[k+1] = NULL;    
  printem();
}
