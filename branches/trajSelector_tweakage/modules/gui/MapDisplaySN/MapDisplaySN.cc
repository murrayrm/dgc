/**
 * MapDisplaySN.cc
 * Revision History:
 * 02/05/2005  hbarnor  Created
 */


#include "MapDisplaySN.hh"
#include "RoadPainter.hh"
#include "Road.hh"

#include <unistd.h>
#include <iostream>
#include <string>

#define MAX_DELTA_SIZE  10000000
#define THREAD_INTERVAL 50000

MapDisplaySN::MapDisplaySN(MapConfig * mapConfig, int sn_key, bool bWaitForStateFill, bool useHighResMaps, bool shiftAllMaps)
  : CSkynetContainer(SNMapDisplay,sn_key),
		CStateClient(bWaitForStateFill),
		m_mapConfig(mapConfig)
{
#ifdef RTP_VPROFILE_DISPLAY
	m_rtpPipe.open("./rtp");
#endif

	bShiftAllMaps = shiftAllMaps;

  m_mapConfig->set_state_struct(&m_state);
  m_mapConfig->set_actuator_state_struct(&m_actuatorState);

  //RDDFData& x = mapConfig->get_rddf_data(0);

	if(useHighResMaps) {
		numLayers = SNGUI_LAYER_TOTAL;
		layerNum = new int[SNGUI_LAYER_TOTAL];
		m_mapTypes = new sn_msg[SNGUI_LAYER_TOTAL];
		m_mapSources = new modulename[SNGUI_LAYER_TOTAL];

		myCMap.initMap(CONFIG_FILE_DEFAULT_MAP);
		layerNum[SNGUI_LAYER_FUSIONMAP_001] = myCMap.addLayer<double>(CONFIG_FILE_COST);
		m_mapTypes[SNGUI_LAYER_FUSIONMAP_001] = SNfusiondeltamap;
		m_mapSources[SNGUI_LAYER_FUSIONMAP_001] = SNfusionmapper;
		
		layerNum[SNGUI_LAYER_FUSIONMAP_002] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_MEAN_ELEV);
		m_mapTypes[SNGUI_LAYER_FUSIONMAP_002] = SNfusionElevDeltaMap;
		m_mapSources[SNGUI_LAYER_FUSIONMAP_002] = SNfusionmapper;
		
		layerNum[SNGUI_LAYER_LADAR_001] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_MEAN_ELEV);
		m_mapTypes[SNGUI_LAYER_LADAR_001] = SNladardeltamap;
		m_mapSources[SNGUI_LAYER_LADAR_001] = MODladarFeeder;      
		
		layerNum[SNGUI_LAYER_LADAR_002] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_STDDEV);
		m_mapTypes[SNGUI_LAYER_LADAR_002] = SNfusionDeltaMapStdDev;
		m_mapSources[SNGUI_LAYER_LADAR_002] = SNfusionmapper;
		
		layerNum[SNGUI_LAYER_LADAR_003] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_NUM);
		m_mapTypes[SNGUI_LAYER_LADAR_003] = SNladarDeltaMapNum;
		m_mapSources[SNGUI_LAYER_LADAR_003] = MODladarFeeder;
		
		layerNum[SNGUI_LAYER_STEREO_001] = myCMap.addLayer<double>(CONFIG_FILE_STEREO_MEAN_ELEV);
		m_mapTypes[SNGUI_LAYER_STEREO_001] = SNstereoDeltaMapMean;
		m_mapSources[SNGUI_LAYER_STEREO_001] = MODstereofeeder;
		
		layerNum[SNGUI_LAYER_STATIC_001] = myCMap.addLayer<double>(CONFIG_FILE_STATIC);
		m_mapTypes[SNGUI_LAYER_STATIC_001] = SNstaticdeltamap;
		m_mapSources[SNGUI_LAYER_STATIC_001] = MODstaticpainter;
		
		layerNum[SNGUI_LAYER_ROAD] = myCMap.addLayer<double>(CONFIG_FILE_ROAD);
		m_mapTypes[SNGUI_LAYER_ROAD] = SNroad2map;
		m_mapSources[SNGUI_LAYER_ROAD] = SNroadfinding;
	} else {
		numLayers = SNGUI_LOWRES_LAYER_TOTAL;
		layerNum = new int[SNGUI_LOWRES_LAYER_TOTAL];
		m_mapTypes = new sn_msg[SNGUI_LOWRES_LAYER_TOTAL];
		m_mapSources = new modulename[SNGUI_LOWRES_LAYER_TOTAL];

		myCMap.initMap(CONFIG_FILE_SPEED_MAP);

		layerNum[SNGUI_LOWRES_LAYER_FUSIONMAP_001] = myCMap.addLayer<double>(CONFIG_FILE_COST);
		m_mapTypes[SNGUI_LOWRES_LAYER_FUSIONMAP_001] = SNlowResSpeedDeltaMap;
		m_mapSources[SNGUI_LOWRES_LAYER_FUSIONMAP_001] = SNfusionmapper;
	}

  m_mapConfig->set_cmap(&myCMap, &m_mapdeltaMutex);



  static const sn_msg pathTypes[] =
    {SNtraj, SNtrajPlannerSeed, SNtrajPlannerInterm, SNtraj, SNtrajReverse};
  static const modulename pathSources[] =
    {ALLMODULES,SNplanner,SNplanner,SNplanner, SNtrajfollower};
  static const RGBA_COLOR pathColors[] = 
    {RGBA_COLOR(1.0, 0.0, 0.0),
     RGBA_COLOR(0.0, 0.5, 0.0),
     RGBA_COLOR(0.0, 1.0, 1.0),
     RGBA_COLOR(1.0, 0.0, 1.0),
     RGBA_COLOR(.1, 0.1, .1),
    };

  memcpy(m_pathTypes, pathTypes, sizeof(m_pathTypes));
  memcpy(m_pathSources, pathSources, sizeof(m_pathSources));
  memcpy(m_pathColors, pathColors, sizeof(m_pathColors));

  int i;
  // initialize paths
  // Set the color for our path history to re
  for(i=0; i<PATHIDX_NUMPATHS; i++)
	{
    paths[i].color = m_pathColors[i];
		DGCcreateMutex(&paths[i].pathMutex);
	}

  // memory for the map deltas
  for(i=0; i<SNGUI_LAYER_TOTAL; i++)
    m_pMapDelta[i] = new char[MAX_DELTA_SIZE];

  // set the paths
  m_mapConfig->set_paths( paths, PATHIDX_NUMPATHS );

	DGCcreateMutex(&m_mapdeltaMutex);
	DGCcreateMutex(&m_loggingMutex);

	m_ppLogs = new string*[NUM_MESSAGES_TO_LOG];
	for(i=0; i<NUM_MESSAGES_TO_LOG; i++)
		m_ppLogs[i] = NULL;

	m_logMessageIndex = 0;
	m_bLogBufferFull = false;
}

MapDisplaySN::~MapDisplaySN()
{
  int i;
  for(i=0; i<SNGUI_LAYER_TOTAL; i++)
    delete m_pMapDelta[i];

  for(i=0; i<PATHIDX_NUMPATHS; i++)
		DGCdeleteMutex(&paths[i].pathMutex);

	DGCdeleteMutex(&m_loggingMutex);
	DGCdeleteMutex(&m_mapdeltaMutex);

	for(i=0; i<NUM_MESSAGES_TO_LOG; i++)
		if(m_ppLogs[i] != NULL)
			delete m_ppLogs[i];
		
	delete[] m_ppLogs;
	
	delete m_writeLogSig;
}

void MapDisplaySN::init()
{
	printf("[%s:%d] Calling UpdateState() in init()... ", __FILE__, __LINE__);
	UpdateState(); // update the state
	printf(" ...done.");
	m_writeLogSig = new sigc::signal<void, ostream*>();
	m_writeLogSig->connect(sigc::mem_fun(this,&MapDisplaySN::writelogs));
	m_mapConfig->set_writeLogSig(m_writeLogSig);
	// center map display on vehicle
	myCMap.updateVehicleLoc(m_state.Northing,m_state.Easting);
	cout << "Skynet Gui: MapDisplay Module Init Finished" << endl;
}

// This thread reads in the map deltas as they come in. The display is only
// notified that something's changed in the getStateThread function
void MapDisplaySN::getMapDeltaThread(void* pArg)
{
  int layerIndex = (int)pArg;

  if(layerIndex==SNGUI_LAYER_ROAD) {
    getRoadThread();
    return;
  }

  sn_msg mapType = m_mapTypes[layerIndex];
  modulename mapSource = m_mapSources[layerIndex];

  int shift = 0;


  if(layerIndex == SNGUI_LAYER_FUSIONMAP_001 || bShiftAllMaps) {
    shift=1;
  }

  int mapDeltaSocket = m_skynet.listen(mapType, mapSource);
  
  if(mapDeltaSocket < 0)
    cerr << "MapDisplaySN::getMapDeltasThread(): skynet listen returned error" << endl;
  
  cerr << "MapDisplaySN::getMapDeltasThread(): running" << endl;
  while(true)
  {
		int deltasize;
		if(RecvMapdelta(mapDeltaSocket, m_pMapDelta[layerIndex], &deltasize))
    {
			unsigned long long timestamp;
			DGCgettime(timestamp);
			DGClockMutex(&m_loggingMutex);

			string **ppThismsg = &m_ppLogs[incrementLogMessageIndex()];
			if(*ppThismsg != NULL)
			{
				delete *ppThismsg;
			}
			*ppThismsg = new string;
			(*ppThismsg)->append((char*)&timestamp, sizeof(timestamp));
			(*ppThismsg)->append((char*)&mapType, sizeof(mapType));
			(*ppThismsg)->append((char*)&deltasize, sizeof(deltasize));
			(*ppThismsg)->append((char*)m_pMapDelta[layerIndex], deltasize);
			DGCunlockMutex(&m_loggingMutex);
      
      DGClockMutex(&m_mapdeltaMutex);
      myCMap.applyDelta<double>(layerNum[layerIndex], m_pMapDelta[layerIndex], deltasize, shift);
      DGCunlockMutex(&m_mapdeltaMutex);
    }
  }   
}

//Run from getMapDeltaThread to get the road data (which isn't sent as mapdelta
void MapDisplaySN::getRoadThread()
{
  int road_socket = m_skynet.listen(SNroad2map, SNroadfinding);

  bool first_road=true;
  int cur_road=0;
  Road road[2];
  CRoadPainter painter;

  printf("getRoadThread started\n");
  
  static int bytes=0;
  static int cnt=0;

  while(true) {
    //Read road data over skynet, make sure we always get the last transfer
    //so that we don't build up the buffert
    int received;
    do {
      received = m_skynet.get_msg(road_socket, &road[cur_road], sizeof(Road), 0);
      if(received < road[cur_road].data_size()) {
	printf("Error receiving road data\n");
	continue;
      }

      bytes +=received;
      ++cnt;
      //printf("Received %i messages, %i bytes, %i road points\n", cnt, bytes, road[cur_road].length);
    } while(m_skynet.is_msg(road_socket));

    if(received < road[cur_road].data_size())   //Invalid road data, get next package
      continue;

    //Paint the trajectory into the map
    DGClockMutex(&m_mapdeltaMutex);
    //myCMap.clearLayer(layerNum[SNGUI_LAYER_ROAD]);

    if(!first_road) {
      painter.paint(road[cur_road], road[1-cur_road], &myCMap, layerNum[SNGUI_LAYER_ROAD]);
    } else {
      painter.paint(road[cur_road], &myCMap, layerNum[SNGUI_LAYER_ROAD]);
    }
    
    first_road=false;
    cur_road = 1-cur_road;

    DGCunlockMutex(&m_mapdeltaMutex);
  }
}

// This thread reads in paths as they come in. The display is only notified that
// something's changed in the getStateThread function
void MapDisplaySN::getTrajThread(void* pArg)
{
  int pathIndex = (int)pArg;
  sn_msg pathType = m_pathTypes[pathIndex];
  modulename pathSource = m_pathSources[pathIndex];

  int trajSocket = m_skynet.listen(pathType, pathSource);

  while(true)
  {
		WaitForTrajData(trajSocket);

		unsigned long long timestamp;
		DGCgettime(timestamp);

		DGClockMutex(&m_loggingMutex);

		string **ppThismsg = &m_ppLogs[incrementLogMessageIndex()];
		if(*ppThismsg != NULL)
		{
			delete *ppThismsg;
		}
		*ppThismsg = new string;
		(*ppThismsg)->append((char*)&timestamp, sizeof(timestamp));
		(*ppThismsg)->append((char*)&pathType, sizeof(pathType));

		RecvTraj(trajSocket, &paths[pathIndex].points, &paths[pathIndex].pathMutex, *ppThismsg);

		DGCunlockMutex(&m_loggingMutex);

#ifdef RTP_VPROFILE_DISPLAY
		if(pathIndex == PATHIDX_PLANNED)
		{
			if(m_rtpPipe.good())
			{
				m_rtpPipe << "0" << endl;
				paths[pathIndex].points.printSpeedProfile(m_rtpPipe);
			}
		}
#endif


  }
}

void MapDisplaySN::getStateThread(void)
{
  while(true)
  {
    /* This sets m_state and m_actuatorState */
    UpdateState();
		UpdateActuatorState();

		unsigned long long timestamp;
		sn_msg skynettype;
		int msgsize;
		DGCgettime(timestamp);

		DGClockMutex(&m_loggingMutex);

		string **ppThismsg = &m_ppLogs[incrementLogMessageIndex()];
		if(*ppThismsg != NULL)
		{
			delete *ppThismsg;
		}
		*ppThismsg = new string;

		skynettype = SNstate;
		msgsize = sizeof(m_state);
		(*ppThismsg)->append((char*)&timestamp, sizeof(timestamp));
		(*ppThismsg)->append((char*)&skynettype, sizeof(skynettype));
		(*ppThismsg)->append((char*)&msgsize, sizeof(msgsize));
		(*ppThismsg)->append((char*)&m_state, msgsize);

		skynettype = SNactuatorstate;
		msgsize = sizeof(m_actuatorState);
		(*ppThismsg)->append((char*)&timestamp, sizeof(timestamp));
		(*ppThismsg)->append((char*)&skynettype, sizeof(skynettype));
		(*ppThismsg)->append((char*)&msgsize, sizeof(msgsize));
		(*ppThismsg)->append((char*)&m_actuatorState, msgsize);

		DGCunlockMutex(&m_loggingMutex);

    // Add our current position to the path history   
    // If our history has too many points in it, we need to shift them and add
    int num_points;
    if( (num_points = paths[PATHIDX_HISTORY].points.getNumPoints()) > TRAJ_MAX_LEN-2 )
    {
      paths[PATHIDX_HISTORY].points.shiftNoDiffs(1);
    }
    paths[PATHIDX_HISTORY].points.inputNoDiffs(m_state.Northing_rear(), m_state.Easting_rear());

    m_mapConfig->notify_update();      
    usleep(THREAD_INTERVAL);
  }
}

void MapDisplaySN::writelogs(ostream* pStream)
{
  printf("[%s:%d] writelogs has been called.\n", __FILE__, __LINE__);

  // if we've no data to write, return
  if(m_logMessageIndex == 0 && !m_bLogBufferFull)
  {
    printf("[%s:%d] No data to write.\n", __FILE__, __LINE__);
    return;
  }
  
  DGClockMutex(&m_loggingMutex);

	int oldMessageIndex = m_logMessageIndex;

	m_logMessageIndex = (m_bLogBufferFull ? m_logMessageIndex : 0);

	ofstream outfile;
	if(pStream == NULL)
	{
    printf("[%s:%d] Opening output file.\n", __FILE__, __LINE__);
		outfile.open("/tmp/guilog");
    if(!outfile) 
    {
      printf("[%s:%d] Couldn't open outfile.\n", __FILE__, __LINE__);
    }
		pStream = &outfile;
	}
		
	do
	{
		string *pThismsg = m_ppLogs[incrementLogMessageIndex()];

		if(pThismsg != NULL)
    {
			pStream->write(pThismsg->c_str(), pThismsg->length());
    }
    else
    {
      printf("[%s:%d] pThismsg == NULL (not writing)\n", __FILE__, __LINE__);
    }
	} while(m_logMessageIndex != oldMessageIndex);

	m_logMessageIndex = oldMessageIndex;
  printf("[%s:%d] Message index = %d\n", __FILE__, __LINE__, oldMessageIndex);

	DGCunlockMutex(&m_loggingMutex);
}
