//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_Ladar_GapTracker.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for ladar gap tracker class derived from particle filter class

UD_Ladar_GapTracker::UD_Ladar_GapTracker(UD_LadarSourceArray *ladarr, int num_particles, UD_Vector *meanstate, UD_Vector *covdiag) : UD_ParticleFilter(num_particles, meanstate, covdiag)
{
  // local names 
  
  ladarray = ladarr;

  // default values

  gapwidth = 4.0;

  show_particles = TRUE;

  initialize_prior();
}

//----------------------------------------------------------------------------

void UD_Ladar_GapTracker::iterate()
{
  // project "dangerous" ladar hit points to line defined by front axle of vehicle
  // along direction obtained from visual vanishing point

  ladarray->baseline_project(direction_error);

  // track center of least density in hit point projection -- the "gap"

  update();
}

//----------------------------------------------------------------------------

/// put particles in their a priori states, uniformly distributed over					
/// ladar angular range

void UD_Ladar_GapTracker::pf_samp_prior(UD_Vector *samp)
{
   samp->x[0] = ranged_uniform_UD_Random(-gapwidth, gapwidth);
}

//----------------------------------------------------------------------------

/// apply deterministic motion model to particles

void UD_Ladar_GapTracker::pf_dyn_state(UD_Vector *old_samp, UD_Vector *new_samp)
{
  copy_UD_Vector(old_samp, new_samp);
}

//----------------------------------------------------------------------------

/// random diffusion: add Gaussian noise to old state sample 

void UD_Ladar_GapTracker::pf_samp_state(UD_Vector *old_samp, UD_Vector *new_samp)
{
  sample_gaussian_diagonal_covariance(new_samp, old_samp, covdiagsqrt);
}

//----------------------------------------------------------------------------

/// evaluate likelihood of one particle given current image

double UD_Ladar_GapTracker::pf_condprob_zx(UD_Vector *samp)
{
  int i;
  float left_edge, right_edge, density, range_sigma, sigma;

  if (samp->x[0] < -gapwidth || samp->x[0] > gapwidth)
    return 0.0;

  left_edge = samp->x[0] - 0.5 * gapwidth;
  right_edge = samp->x[0] + 0.5 * gapwidth;
  range_sigma = 0.05;

  // density is proportional to how many dangerous hit point project to this
  // gap along VP-supplied direction, with more distant points given less weight

  density = ladarray->density_in_gap(left_edge, right_edge, range_sigma);

  // fewer hits -> higher likelihood; more hits -> smaller likelihood
 
  sigma = 0.1;
  
  return exp(-sigma * density);
}

//----------------------------------------------------------------------------

/// write current estimate of vanishing point POSITION (in image coordinates)
/// to file (terminal string is assumed to be newline)

void UD_Ladar_GapTracker::write(FILE *fp)
{
  //  write(fp, "\n");
}

//----------------------------------------------------------------------------

/// write current estimate of vanishing point POSITION (in image coordinates)
/// to file with an argument specifying terminal string (i.e., newline, 
/// comma-space, etc.)

void UD_Ladar_GapTracker::write(FILE *fp, char *terminal_string)
{
//   fprintf(fp, "%.2f, %.2f%s", vp_x(), vp_y(), terminal_string);
//   fflush(fp);
}

//----------------------------------------------------------------------------

/// draw road curvature tracker parameters, namely the POSITION of the
/// current road vanishing point.  assuming window dimensions are same as
/// candidate region

void UD_Ladar_GapTracker::draw()
{
  int i;

  // particles are yellow lines

  /*
  if (show_particles) {

    glColor3ub(255, 255, 0);
    glPointSize(1);

    glBegin(GL_POINTS);

    for (i = 0; i < num_samples; i++) 
      glVertex2f(sample[i]), win_h - candidate_vp_y(sample[i]));

    glEnd();
  }
  */

  // state is thick blue line

  /*
  glColor3ub(0, 0, 255);
  glPointSize(5);

  glBegin(GL_POINTS);

  glVertex2f(candidate_vp_x(), win_h - candidate_vp_y());

  glEnd();
  */
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
