//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_Aerial.hh"
	
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// horrible kludge to get GLUT callback functions into class

UD_Aerial *UD_Aerial::aerial = new UD_Aerial(NULL, NULL, "none", NULL);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void UD_Aerial::read_RDDF(char *filename)
{
  char ss[100];
  char tok[100];
  char *token;
  int numlines, cnt, curline;
  FILE *fp;  
  double Lat, Long;          
  double UTMNorthing, UTMEasting;
  int UTMZone;

  fp = fopen (filename,"r");
  //fp = fopen ("../stoddard_wells_data/raceday.rddf","r");
  if (!fp) {
    printf("no such RDDF %s\n", filename);
    exit(1);
  }

  numlines = 0;
  while (!feof(fp)) {
    fgets (ss,100,fp);
    numlines++;
  }
  numlines--;
  rewind (fp);

  rddf_num_waypoints = numlines;
  rddf = (double **) calloc(rddf_num_waypoints, sizeof(double *));
  
  token = tok;

  for (curline = 0; curline < rddf_num_waypoints; curline++) {

    rddf[curline] = (double *) calloc(RDDF_COLS + 1, sizeof(double));
 
    fgets(ss, 100, fp);
    token = strtok( ss,",");
    for (cnt = 0; cnt < RDDF_COLS; cnt++) {
      rddf[curline][cnt] = atof(token);
      token = strtok(NULL, ",");
    }

    // desert_loop_1500.rddf skips waypoint 913

    if (rddf[curline][RDDF_INDEX] != curline) {
      
      //      printf("waypoint index badness at %lf %i\n", rddf[curline][RDDF_INDEX], curline);
      //      exit(1);
    }

    // Convert Lat-Long from RDDF to UTM
    LLtoUTM(rddf[curline][RDDF_LAT], rddf[curline][RDDF_LONG], &UTMNorthing, &UTMEasting, &UTMZone);

    rddf[curline][RDDF_NORTHING] = UTMNorthing;
    rddf[curline][RDDF_EASTING] = UTMEasting;
    rddf[curline][RDDF_ZONE] = (double) UTMZone;
 
    // Convert width to meters from feet

    rddf[curline][RDDF_WIDTH] *= 0.305;

    // convert speed?

    // print what we've got

    //    printf("%i: n %lf e %lf z = %i, w %lf\n", curline, rddf[curline][RDDF_NORTHING], rddf[curline][RDDF_EASTING], (int) rddf[curline][RDDF_ZONE], rddf[curline][RDDF_WIDTH]);
  }

  tileZone = rddf[0][RDDF_ZONE];    // assume whole course is in same zone as first waypoint (doing this only
  // because Caltech state doesn't seem to include a zone, implying that one is assumed)

  fclose (fp);
}

//----------------------------------------------------------------------------

/// check whether suffix is ".rddf" or ".RDDF"

int UD_Aerial::is_RDDF(char *filename)
{
  if (!strcmp(".rddf", (char *) &filename[strlen(filename)-5]) || 
      !strcmp(".RDDF", (char *) &filename[strlen(filename)-5]))
    return TRUE;
  else
    return FALSE;
}

//----------------------------------------------------------------------------

/// fetch tiles from terraserver for this path

void UD_Aerial::rasterize_pathfile(char *filename)
{
  // if filename is RDDF, we've already read it into array rddf

  if (is_RDDF(filename)) {
//     printf(".rddf\n");
//     exit(1);
    rasterize_RDDF(TRUE, NULL);
  }

  // else if filename is ladar state log, open it, fetch tiles for all (easting, northing) coords., close file

  else {

    UD_StateSource_Scans *statesrc;
    int result;

    statesrc = new UD_StateSource_Scans(filename, NONE);
    do {
      result = statesrc->read();
      if (result) {
	// get octagon pattern to ensure "safety margin"
	getUTMImage(statesrc->northing, statesrc->easting);
	for (int i = 0; i < num_safety_pts; i++)
	  getUTMImage(statesrc->northing + safety_dy[i], statesrc->easting + safety_dx[i]);
	//	  printf("safety get %i: dx = %lf, dy = %lf\n", i, safety_dx[i], safety_dy[i]);
      }
    } while (result);
    
    exit(1);
  }
}

//----------------------------------------------------------------------------

void UD_Aerial::finish_construction(char *pathfilename, char *tilepathname, char *mode, UD_StateSource *ss)
{
  if (!strcmp(mode, "FETCH") || !strcmp(mode, "Fetch") || !strcmp(mode, "fetch"))
    run_mode = MODE_INTERNET_FETCH;
  else 
    run_mode = MODE_INTERNET_NOFETCH;

  strcpy(tilePath, tilepathname);
    
  if (is_RDDF(pathfilename))
    read_RDDF(pathfilename);
    
  if (run_mode == MODE_INTERNET_FETCH) {
      
    // check that we have necessary tile images and get them if we don't
      
    rasterize_pathfile(pathfilename);
      
    // getting terraserver tiles from web is a "standalone utility"
      
    exit(1);
  }
  else {
#ifdef SKYNET
    if (!ss)
      statesrc = new UD_StateSource_Skynet(NONE);
    else
      statesrc = ss;
#else
    if (!ss) 
      statesrc = new UD_StateSource_Scans(pathfilename, NONE);
    else 
      statesrc = ss;
#endif
  }
}

//----------------------------------------------------------------------------

/// constructor for aerial viewing class.  assuming 1 m / pixel
/// default path, tile location should be "../stoddard_wells_data/desert_loop_1500.rddf", 
/// "~cer/stoddard_wells_data/tiles", respectively 

UD_Aerial::UD_Aerial(char *pathfilename, char *tilepathname, char *mode, UD_StateSource *ss)
{
  int i;
  float theta, delta_theta;

  // local values

  // default values
  
  tileZone = 11;
  rddf = NULL;
  tilePath = NULL;
  drawblock = FALSE;
  blocksize = 51;
  block = (int **) calloc(blocksize, sizeof(int *));
  for (i = 0; i < blocksize; i++)
    block[i] = (int *) calloc(blocksize, sizeof(int));
  tilePath = (char *) calloc(MAXSTRLEN, sizeof(char));

  max_history_pts = 500;
  
  tileXMin = INT_MAX;
  tileXMax = -INT_MAX;
  tileYMin = INT_MAX;
  tileYMax = -INT_MAX;

  // body

  num_history_pts = cur_history_pt = 0;
  northing_history = (double *) calloc(max_history_pts, sizeof(double));
  easting_history = (double *) calloc(max_history_pts, sizeof(double));

  safetyMargin = TERRASERVER_TILE_SIDELENGTH; 

  num_safety_pts = 8;
  safety_dx = (double *) calloc(num_safety_pts, sizeof(double));
  safety_dy = (double *) calloc(num_safety_pts, sizeof(double));

  delta_theta = 2*PI/(float) num_safety_pts;
  for (i = 0, theta = 0; i < num_safety_pts; i++, theta += delta_theta) {
    safety_dx[i] = safetyMargin * cos(theta);
    safety_dy[i] = safetyMargin * sin(theta);
  }

  // if filename is RDDF

  if (!pathfilename || !tilepathname)
    return;
  else 
    finish_construction(pathfilename, tilepathname, mode, ss);
}

//----------------------------------------------------------------------------

// converts lat/long to UTM coords.  Equations from USGS Bulletin 1532 
// East Longitudes are positive, West longitudes are negative. 
// North latitudes are positive, South latitudes are negative
// Lat and Long are in decimal degrees
// Written by Chuck Gantz- chuck.gantz@globalstar.com

void UD_Aerial::LLtoUTM(const double Lat, const double Long, double *UTMNorthing, double *UTMEasting, int *ZoneNumber)
{
  double a = WGS_84_EQUATORIAL_RADIUS;
  double eccSquared = WGS_84_SQUARE_OF_ECCENTRICITY;
  double k0 = 0.9996;
  
  double LongOrigin;
  double eccPrimeSquared;
  double N, T, C, A, M;
  double LongTemp,LatRad,LongRad;
  double LongOriginRad;
  
  //Make sure the longitude is between -180.00 .. 179.9
  LongTemp = (Long+180.0)-(int)((Long+180)/360)*360.0-180.0; // -180.00 .. 179.9;
  
  LatRad = DEG2RAD(Lat);
  LongRad = DEG2RAD(LongTemp);
  
  *ZoneNumber = (int)((LongTemp + 180)/6) + 1;
  
  if( Lat >= 56.0 && Lat < 64.0 && LongTemp >= 3.0 && LongTemp < 12.0 )
    *ZoneNumber = 32;
  
  // Special zones for Svalbard
  if( Lat >= 72.0 && Lat < 84.0 ) 
    {
      if(      LongTemp >= 0.0  && LongTemp <  9.0 ) *ZoneNumber = 31;
      else if( LongTemp >= 9.0  && LongTemp < 21.0 ) *ZoneNumber = 33;
      else if( LongTemp >= 21.0 && LongTemp < 33.0 ) *ZoneNumber = 35;
      else if( LongTemp >= 33.0 && LongTemp < 42.0 ) *ZoneNumber = 37;
    }
  LongOrigin = (*ZoneNumber - 1)*6 - 180 + 3;  //+3 puts origin in middle of zone
  LongOriginRad = DEG2RAD(LongOrigin);
  
  eccPrimeSquared = (eccSquared)/(1-eccSquared);
  
  N = a/sqrt(1-eccSquared*sin(LatRad)*sin(LatRad));
  T = tan(LatRad)*tan(LatRad);
  C = eccPrimeSquared*cos(LatRad)*cos(LatRad);
  A = cos(LatRad)*(LongRad-LongOriginRad);
  
  M = a*((1	- eccSquared/4		- 3*eccSquared*eccSquared/64	- 5*eccSquared*eccSquared*eccSquared/256)*LatRad 
	 - (3*eccSquared/8	+ 3*eccSquared*eccSquared/32	+ 45*eccSquared*eccSquared*eccSquared/1024)*sin(2*LatRad)
	 + (15*eccSquared*eccSquared/256 + 45*eccSquared*eccSquared*eccSquared/1024)*sin(4*LatRad) 
	 - (35*eccSquared*eccSquared*eccSquared/3072)*sin(6*LatRad));
  
  *UTMEasting = (double)(k0*N*(A+(1-T+C)*A*A*A/6
			       + (5-18*T+T*T+72*C-58*eccPrimeSquared)*A*A*A*A*A/120)
			 + 500000.0);
  
  *UTMNorthing = (double)(k0*(M+N*tan(LatRad)*(A*A/2+(5-T+9*C+4*C*C)*A*A*A*A/24
					       + (61-58*T+T*T+600*C-330*eccPrimeSquared)*A*A*A*A*A*A/720)));
  if (Lat < 0)
    *UTMNorthing += 10000000.0; //10000000 meter offset for southern hemisphere
}

//----------------------------------------------------------------------------

// assuming all waypoints are in the same zone as the first waypoint

void UD_Aerial::draw_horizontal_line(int xleft, int xright, int y, int tilecoords, IplImage *mosaic)
{
  int x;

  for (x = xleft; x <= xright; x++) {

    if (tilecoords) {

      getTileImage(x, y);

      if (x < tileXMin)
	tileXMin = x;
      else if (x > tileXMax)
	tileXMax = x;
      if (y < tileYMin)
	tileYMin = y;
      else if (y > tileYMax)
	tileYMax = y;

      if (mosaic)
	UCHAR_IMXY(mosaic, tile2im_x(x), tile2im_y(y)) = 255;
    }
  }
}

//----------------------------------------------------------------------------

// assuming y - ymin is always within array bounds

void UD_Aerial::draw_table_point(int x, int y, int *left, int *right, int ymin)
{
  //  printf("x %i y %i, ymin %i\n", x, y, ymin);
  //  fflush(stdout);

  if (x < left[y - ymin])
    left[y - ymin] = x;
  if (x > right[y - ymin])
    right[y - ymin] = x;
}

//----------------------------------------------------------------------------

// lower-left corner of image is (0, 0)

// updates left, right arrays

void UD_Aerial::dda_table_line(double xa, double ya, double xb, double yb, int *left, int *right, int ymin)
{
  int xl, yl, xr, yr, dx, dy, xtemp, ytemp;
  double m, inv_m, x, y;

  // assign xl, yl to leftmost point (and xr, yr accordingly)

  if (xa < xb || (xa == xb && ya >= yb)) {
    xl = round(xa);
    yl = round(ya);
    xr = round(xb);
    yr = round(yb);
  }
  else if (xa > xb || (xa == xb && ya < yb)) {
    xl = round(xb);
    yl = round(yb);
    xr = round(xa);
    yr = round(ya);
  }

  dx = xr - xl;
  dy = yr - yl;
  
  if (dx) 
    m = (double) dy / (double) dx;

  // just a point

  if (!dx && !dy)
    draw_table_point(xl, yl, left, right, ymin);

  // vertical or negative slope line

  else if (!dx || m < 0) {

    // infinite and negative slope case is equivalent to rotating the 
    // coordinate axes 90 degs. clockwise and using the positive slope case.
    // this is accomplished via (x, y) -> (y, -x)

    // reverse left and right

    xtemp = xl;
    ytemp = yl;
    xl = yr;
    yl = -xr;
    xr = ytemp;
    yr = -xtemp;

    // negate and invert slope

    m = -(double) dx / (double) dy;

    // proceed as in the positive m case, except swap coordinates BACK when drawing: (x, y) -> (-y, x)

    if (m > 1) {      
      for (ytemp = yl, x = xl, inv_m = 1 / m; ytemp <= yr; ytemp++) {
	draw_table_point(-ytemp, round(x), left, right, ymin);
	x += inv_m;
      }
    }

    // increment x

    else if (m >= 0) {
      for (xtemp = xl, y = yl; xtemp <= xr; xtemp++) {
	draw_table_point(round(-y), xtemp, left, right, ymin);
	y += m;
      }
    }
  }

  // horizontal or positive slope line

  // increment y

  else if (m > 1) {
    for (ytemp = yl, x = xl, inv_m = 1 / m; ytemp <= yr; ytemp++) {
      draw_table_point(round(x), ytemp, left, right, ymin);
      x += inv_m;
    }
  }

  // increment x

  else if (m >= 0) {
    for (xtemp = xl, y = yl; xtemp <= xr; xtemp++) {
      draw_table_point(xtemp, round(y), left, right, ymin);
      y += m;
    }
  }

  // should never get this far
}

//----------------------------------------------------------------------------

// theta = 0 faces to right

void UD_Aerial::draw_sunpos(double x, double y, double baselength, double sunalt, double sunazi, int r, int g, int b)
{
  double dx, dy, altscale, b_2, h_3, h, theta;

  // position

  draw_circle(x, y, baselength, r, g, b);

  // direction

  theta = sunazi - 0.5 * PI;

  altscale = cos(sunalt);

  dx = altscale * baselength * cos(-theta);
  dy = altscale * baselength * sin(-theta);

  glBegin(GL_LINES);

  glColor3ub(r, g, b);

  glVertex2f(x, y);
  glVertex2f(x + dx, y + dy);

  glColor3ub(0, 0, 0);

  glVertex2f(x, y);
  glVertex2f(x - dx, y - dy);
  
  glEnd();

  // road direction

  /*
  theta = phi + rf->direction_error - 0.5 * PI;

  xprime = x + 0.5 * 10*baselength * cos(-theta);
  yprime = y + 0.5 * 10*baselength * sin(-theta);

  glColor3ub(255-r, 255-g, 255-b);

  glBegin(GL_LINES);

  glVertex2f(x, y);
  glVertex2f(xprime, yprime);
  
  glEnd();
  */
}

//----------------------------------------------------------------------------

// theta = 0 faces to right

void UD_Aerial::draw_arrowhead(double x, double y, double baselength, double phi, int r, int g, int b)
{
  double xprime, yprime, b_2, h_3, h, theta;

  // position

  draw_circle(x, y, 0.25 * baselength, r, g, b);

  // direction

  theta = phi - 0.5 * PI;

  xprime = x + 0.5 * baselength * cos(-theta);
  yprime = y + 0.5 * baselength * sin(-theta);

  glColor3ub(r, g, b);

  glBegin(GL_LINES);

  glVertex2f(x, y);
  glVertex2f(xprime, yprime);
  
  glEnd();

  // road direction

  /*
  theta = phi + rf->direction_error - 0.5 * PI;

  xprime = x + 0.5 * 10*baselength * cos(-theta);
  yprime = y + 0.5 * 10*baselength * sin(-theta);

  glColor3ub(255-r, 255-g, 255-b);

  glBegin(GL_LINES);

  glVertex2f(x, y);
  glVertex2f(xprime, yprime);
  
  glEnd();
  */
}

//----------------------------------------------------------------------------

void UD_Aerial::draw_string(char *s, int x, int y, int r, int g, int b) 
{
  int lines;
  char* p;
  float *f;

  f = (float *) calloc(4, sizeof(float));
  glGetFloatv(GL_CURRENT_RASTER_POSITION, f);

  glPushMatrix();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, glutGet(GLUT_WINDOW_WIDTH), 
	  0, glutGet(GLUT_WINDOW_HEIGHT), -1, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glColor3ub(0, 0, 0);
  glRasterPos2i(x+1, y-1);
  for(p = s, lines = 0; *p; p++) {
    if (*p == '\n') {
      lines++;
      glRasterPos2i(x+1, y-1-(lines*18));
    }
    //    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *p);
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *p);
  }
  glColor3ub(r, g, b);
  glRasterPos2i(x, y);
  for(p = s, lines = 0; *p; p++) {
    if (*p == '\n') {
      lines++;
      glRasterPos2i(x, y-(lines*18));
    }
    //    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *p);
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *p);
  }
  glRasterPos2i((int) f[0], (int) f[1]);
  free(f);
  glPopMatrix();
}

//----------------------------------------------------------------------------

void UD_Aerial::draw_circle(double x, double y, double radius, int r, int g, int b)
{
  double theta, delta_theta;

  delta_theta = 0.2;

  // circle perimeter

  glColor3ub(r, g, b);

  glBegin(GL_LINE_LOOP);

  for (theta = 0; theta < 2*PI; theta += delta_theta) 
    glVertex2f(x + radius * cos(theta), y + radius * sin(theta));

  glEnd();
}

//----------------------------------------------------------------------------

// fill it

void UD_Aerial::rasterize_circle(double xf, double yf, double radf, int tilecoords, IplImage *mosaic)
{
  double i, j;
  int x, y, r;

  if (!tilecoords) {
    //draw_circle(utm2win_x(xf), utm2win_y(yf), rf, 255, 255, 0);
    return;
  }

  xf = getTileNumber(xf);
  yf = getTileNumber(yf);
  radf = getTileNumber(radf);

  x = round(xf);
  y = round(yf);
  r = round(radf);

  draw_horizontal_line(x - r, x + r, y, tilecoords, mosaic);

  for (j = 1; j <= r; j++) {
    i = (int) round(sqrt(radf*radf - (double) (j*j))); 
    draw_horizontal_line(x - i, x + i, y + j, tilecoords, mosaic);
    draw_horizontal_line(x - i, x + i, y - j, tilecoords, mosaic);
  }
}

//----------------------------------------------------------------------------

// corners specified in clockwise order
// fill it

void UD_Aerial::rasterize_rectangle(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int tilecoords, IplImage *mosaic)
{
  double ymax, ymin, max1, max2, min1, min2;
  int ix1, ix2, ix3, ix4, iy1, iy2, iy3, iy4;
  int i, numrows;
  int *left, *right;

  // draw it in window

  if (!tilecoords) {
    glColor3ub(0, 0, 255);
    glBegin(GL_LINE_LOOP);
    glVertex2f(utm2win_x(x1), utm2win_y(y1));
    glVertex2f(utm2win_x(x2), utm2win_y(y2));
    glVertex2f(utm2win_x(x3), utm2win_y(y3));
    glVertex2f(utm2win_x(x4), utm2win_y(y4));
    glEnd();
    return;
  }

  x1 = getTileNumber(x1);
  y1 = getTileNumber(y1);
  x2 = getTileNumber(x2);
  y2 = getTileNumber(y2);
  x3 = getTileNumber(x3);
  y3 = getTileNumber(y3);
  x4 = getTileNumber(x4);
  y4 = getTileNumber(y4);

  // find max y, min y

  // make left & right array of max y - min y elements

  max1 = MAX2(round(y1), round(y2));
  max2 = MAX2(round(y3), round(y4));
  ymax = MAX2(max1, max2);
  
  min1 = MIN2(round(y1), round(y2));
  min2 = MIN2(round(y3), round(y4));
  ymin = MIN2(min1, min2);

  numrows = 1 + ymax - ymin;
  left = (int *) calloc(numrows, sizeof(int));
  right = (int *) calloc(numrows, sizeof(int));

  for (i = 0; i < numrows; i++) {
    left[i] = INT_MAX;
    right[i] = -INT_MAX;
  }

  // "draw" four edges of rect "into" left & right arrays

  dda_table_line(x1, y1, x2, y2, left, right, ymin);
  dda_table_line(x2, y2, x3, y3, left, right, ymin);
  dda_table_line(x3, y3, x4, y4, left, right, ymin);
  dda_table_line(x4, y4, x1, y1, left, right, ymin);

  for (i = 0; i < numrows; i++)
    draw_horizontal_line(left[i], right[i], i + ymin, tilecoords, mosaic);

  // free left and right arrays

  free(left);
  free(right);

}

//----------------------------------------------------------------------------

// see if we already have local copy

int UD_Aerial::haveTileImage(int tileX, int tileY)
{
   FILE *fp;
   char filename[256];

   // compose filename

   sprintf(filename, "%s/terra_Z=%i_X=%i_Y=%i.jpg", tilePath, tileZone, tileX, tileY);

   // try to open it

   fp = fopen(filename, "r");

   // handle fopen return values

   if (!fp)
      return 0;
   else {
      fclose(fp);
      return 1;
   }
}

//----------------------------------------------------------------------------

// of lower-left corner of tile...
// assuming 1 m / pixel resolution

void UD_Aerial::getTileUTMCoords(int tileX, int tileY, double *e, double *n)
{
  *e = TERRASERVER_TILE_SIDELENGTH * (double) tileX;
  *n = TERRASERVER_TILE_SIDELENGTH * (double) tileY;
}

//----------------------------------------------------------------------------

// assuming 1 m / pixel resolution

// note switch between x, y and northing, easting

void UD_Aerial::getUTMImage(double northing, double easting)
{
   // translate northing and easting into terraserver tile coordinates

  getTileImage(getTileNumber(easting), getTileNumber(northing));
}

//----------------------------------------------------------------------------

// note switch between x, y and northing, easting
// increasing tileX = west to east
// increasing tileY = south to north

int terra_requests = 0;

void UD_Aerial::getTileImage(int tileX, int tileY)
{
   char terraserver_request_string[256], change_name_string[256];

   //   printf("%i, %i\n", tileX, tileY);
   //return;

   // if we already have it, do nothing

   if (run_mode != MODE_INTERNET_FETCH || haveTileImage(tileX, tileY)) {
     //     printf("already have tile at Z=%i, X=%i, Y=%i in %s\n", tileZone, tileX, tileY, tilePath);
     return;
   }

   printf("making terra request %i\n", terra_requests++);

   /*

   // TEMPORARY--PUT A FAKE FILE WHERE THE TERRASERVER DOWNLOAD WOULD GO

   FILE *fp;

   sprintf(change_name_string, "%s/terra_Z=%i_X=%i_Y=%i.jpg", tilePath, tileZone, tileX, tileY);
   fp = fopen(change_name_string, "w");
   fprintf(fp, "%i\n", terra_requests-1);
   fclose(fp);
   
   return;
   */

   // form terraserver request string

   sprintf(terraserver_request_string, "wget \"http://terraserver-usa.com/tile.ashx?T=1&S=10&X=%i&Y=%i&Z=%i\"", 
           tileX, tileY, tileZone);

   // execute request

   system(terraserver_request_string);

   // change name of received file

   sprintf(change_name_string, "mv tile.ashx* %s/terra_Z=%i_X=%i_Y=%i.jpg", tilePath, tileZone, tileX, tileY);
   //   sprintf(change_name_string, "mv tile.ashx@T=1\\&S=10\\&X=%i\\&Y=%i\\&Z=%i %s/terra_Z=%i_X=%i_Y=%i.jpg", tileX, tileY, tileZone, tilePath, tileZone, tileX, tileY);
   printf("%s\n", change_name_string);
   //   exit(1);

   system(change_name_string);
   //   exit(1);
}

//----------------------------------------------------------------------------

// it's theoretically possible that one waypoint could be in UTM zone A
// and the next in zone B--ignoring this for now

// make RDDF path and tile path command-line options

// tilecoords = TRUE => tiles instead of pixels, and fill instead of outline
// FALSE implies the opposite on both counts

void UD_Aerial::rasterize_RDDF(int tilecoords, IplImage *mosaic)
{
  int i;
  double r, e1, n1, e2, n2, x1, y1, x2, y2, x3, y3, x4, y4, theta;

  // for each corridor...

  for (i = 0; i < rddf_num_waypoints - 1; i++) {

    e1 = rddf[i][RDDF_EASTING];
    n1 = rddf[i][RDDF_NORTHING];
    if (tilecoords)
      r = safetyMargin + 0.5 * rddf[i][RDDF_WIDTH];
    else
      r = rddf[i][RDDF_WIDTH];

    e2 = rddf[i + 1][RDDF_EASTING];
    n2 = rddf[i + 1][RDDF_NORTHING];

    // ...draw initial and terminal circles...

    rasterize_circle(e1, n1, r, tilecoords, mosaic);
    rasterize_circle(e2, n2, r, tilecoords, mosaic);

    // ...plus linking rectangle

    theta = atan2(n2 - n1, e2 - e1);
    x1 = e1 - r * sin(theta);
    y1 = n1 + r * cos(theta);

    x2 = e2 - r * sin(theta);
    y2 = n2 + r * cos(theta);

    x3 = e2 + r * sin(theta);
    y3 = n2 - r * cos(theta);

    x4 = e1 + r * sin(theta);
    y4 = n1 - r * cos(theta);

    rasterize_rectangle(x1, y1, x2, y2, x3, y3, x4, y4, tilecoords, mosaic); 
  }
}

//----------------------------------------------------------------------------

void UD_Aerial::setup_tile_textures()
{
  int i;

  //  glEnable(GL_TEXTURE_RECTANGLE_NV);

  tiles_per_side = 3;
  num_tiles = tiles_per_side * tiles_per_side;

  tileim = (IplImage **) calloc(num_tiles, sizeof(IplImage *));
  tiledom = (UD_DominantOrientations **) calloc(num_tiles, sizeof(UD_DominantOrientations *));
  for (i = 0; i < num_tiles; i++) {
    tileim[i] = NULL;
    tiledom[i] = NULL;
    //tiledom[i] = new UD_DominantOrientations(TERRASERVER_TILE_SIDELENGTH, TERRASERVER_TILE_SIDELENGTH, 9, 0.0, 180.0, 4.0);
    //    tiledom[i]->show = FALSE;
  }

  tilename = (GLuint *) calloc(num_tiles, sizeof(GLuint));
  glGenTextures(num_tiles, tilename);

  tilelut = (int *) calloc(num_tiles, sizeof(int));

  for (i = 0; i < num_tiles; i++) {

    glBindTexture(GL_TEXTURE_RECTANGLE_NV, tilename[i]);

    glTexParameterf(GL_TEXTURE_RECTANGLE_NV, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_RECTANGLE_NV, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_RECTANGLE_NV, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_RECTANGLE_NV, GL_TEXTURE_MIN_FILTER, GL_LINEAR);    
  }
}

//----------------------------------------------------------------------------

/// initialize aerial display functions, open window, etc.

void UD_Aerial::initialize_display(int x_position, int y_position)
{

  width = 1.5 * TERRASERVER_TILE_SIDELENGTH;
  height = 1.5 * TERRASERVER_TILE_SIDELENGTH;
  //width = 3 * TERRASERVER_TILE_SIDELENGTH;
  //height = 3 * TERRASERVER_TILE_SIDELENGTH;

  glutInitWindowSize(width, height);
  glutInitWindowPosition(x_position, y_position);
  window_id = glutCreateWindow("aerial");

  glutDisplayFunc(sdisplay); 
  glutMouseFunc(smouse); 
  glutKeyboardFunc(skeyboard);

  glPixelZoom(1, -1);
  glRasterPos2i(-1, 1);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, width-1, 0, height-1);

  setup_tile_textures();  
}

//----------------------------------------------------------------------------

// center tile is r = 0, c = 0 (i.e., there are negative rows and cols)

void UD_Aerial::load_grid_tile(int index, int lutval, int tileX, int tileY)
{
  int r, c;

  tilelut[index] = lutval;

  r = index / tiles_per_side - 1;  // i think the - 1 here assumes tiles_per_side = 3
  c = index % tiles_per_side - 1;

  sprintf(imfilename, "%s/terra_Z=%i_X=%i_Y=%i.jpg", tilePath, tileZone, tileX + c, tileY + r);
//   printf("loading %s\n", imfilename);
//   exit(1);
  tileim[index] = cvLoadImage(imfilename);

  if (tileim[index]) {

    //    tiledom[index]->compute(tileim[index]);

    glBindTexture(GL_TEXTURE_RECTANGLE_NV, tilename[lutval]);

    if (tiledom[index] && tiledom[index]->show)
      glTexImage2D(GL_TEXTURE_RECTANGLE_NV, 0, 1, tiledom[index]->gabor_max_response_intensity->width, tiledom[index]->gabor_max_response_intensity->height, 0,  
		   GL_LUMINANCE, GL_UNSIGNED_BYTE, tiledom[index]->gabor_max_response_intensity->imageData);
    else
      glTexImage2D(GL_TEXTURE_RECTANGLE_NV, 0, 3, tileim[index]->width, tileim[index]->height, 0, GL_RGB, 
		   GL_UNSIGNED_BYTE, tileim[index]->imageData);
  }
}

//----------------------------------------------------------------------------

void UD_Aerial::print_grid()
{	
  int r, c;

  // lut

  for (r = 1; r >= -1; r--) {
    for (c = -1; c <= 1; c++)
      printf("%i ", tilelut[rc2index(r, c)]);
    printf("\n");
  }
  printf("\n");

  // im

  for (r = 1; r >= -1; r--) {
    for (c = -1; c <= 1; c++)
      if (tileim[rc2index(r, c)])
	printf("1 ");
      else
	printf("0 ");
    printf("\n");
  }
  printf("\n");
}

//----------------------------------------------------------------------------

void UD_Aerial::scroll_tile_grid(int dX, int dY, int tileX, int tileY)
{
  int absdX, absdY, i, j, rc, oldlutval, index, srcindex, dstindex;
  UD_DominantOrientations *temp;

  //  printf("dX = %i, dY = %i\n", dX, dY);

  absdX = abs(dX);
  absdY = abs(dY);
	    
  // unified: for each row/column...

  for (j = 0; j < tiles_per_side; j++) {
	      
    rc = j - (tiles_per_side - 1) / 2;
	      
    // (1) unload tile in departing row/column 
	      
    index = rc2index(rc * absdX - dY, rc * absdY - dX);  
    if (tileim[index]) {
      cvReleaseImage(&tileim[index]);
      tileim[index] = NULL;
    }
    temp = tiledom[index];
    oldlutval = tilelut[index];
	  
    // (2) shift rest of column/row toward unoccupied spot
	      
    for (i = 0; i < tiles_per_side - 1; i++) {
      srcindex = rc2index(rc * absdX + i * dY, rc * absdY + i * dX);    
      dstindex = rc2index(rc * absdX + (i - 1) * dY, rc * absdY + (i - 1) * dX); 
      tileim[dstindex] = tileim[srcindex];
      tiledom[dstindex] = tiledom[srcindex];
      tilelut[dstindex] = tilelut[srcindex];
    }
	      
    // (3) load tile in arriving row/column
	      
    dstindex = rc2index(rc * absdX + dY, rc * absdY + dX);    
    tiledom[dstindex] = temp;
    load_grid_tile(dstindex, oldlutval, tileX, tileY);
  }
}

//----------------------------------------------------------------------------

// tile 0 is bottom-left, num_tiles - 1 is upper-right

void UD_Aerial::display()
{
  int tileX, tileY, i, j, k, deltaX, deltaY, dX, dY, X, Y, r, c;
  double s, s_2, x, y;
  char tilestring[MAXSTRLEN];

//   printf("aerial state is = %p\n", statesrc);
//   fflush(stdout);
//   printf("aerial state type = %i\n", statesrc->state_type);
//    fflush(stdout);
//    printf("%lf %lf\n", statesrc->easting, statesrc->northing);
//    fflush(stdout);

  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT);
  
  // seems to be returning northing = easting = 0 from first get_state() for some reason

  //  if (rf && rf->have_state && hypot(rf->northing, rf->easting) > 0.001) {
  if (hypot(statesrc->northing, statesrc->easting) > 0.001) {

    //   return;
 
    tileX = getTileNumber(statesrc->easting);
    tileY = getTileNumber(statesrc->northing);

    // check if we're leaving center tile (or getting very first tile)

    if (!currentTileInitialized || currentTileX != tileX || currentTileY != tileY) {

      // load first grid of tiles

      if (!currentTileInitialized) {

	currentTileInitialized = TRUE;
	deltaX = deltaY = 0;

	for (i = 0; i < num_tiles; i++) 
	  load_grid_tile(i, i, tileX, tileY);
      }

      // update which tile we're now in and what direction we moved to get here (if any)

      else {

	deltaX = tileX - currentTileX;
	deltaY = tileY - currentTileY; 

	//	printf("deltaX = %i, deltaY = %i\n", deltaX, deltaY);

	// when the state is fritzing and skipping around, it's possible to cross more than 
	// one tile border in one step.  so break big moves into multiple one-crossing moves

	// scroll horizontally

	X = currentTileX;
	Y = currentTileY;

	dX = dY = 0;
	if (deltaX)
	  dX = deltaX / abs(deltaX);


 	for (k = 0; k < abs(deltaX); k++) {
	  scroll_tile_grid(dX, dY, X + dX, Y + dY);
	  X += dX;
	  Y += dY;
	}

	// scroll vertically

	dX = dY = 0;
	if (deltaY)
	  dY = deltaY / abs(deltaY);

	for (k = 0; k < abs(deltaY); k++) {
	  scroll_tile_grid(dX, dY, X + dX, Y + dY);
	  X += dX;
	  Y += dY;
	}
      }
	
      currentTileX = tileX;
      currentTileY = tileY;
    }

    // figure out window offset of center tile

    getTileUTMCoords(currentTileX, currentTileY, &currentTileEasting, &currentTileNorthing);

    s = TERRASERVER_TILE_SIDELENGTH;
    s_2 = s / 2;
    x = 0.5 * (double) width + s_2 + currentTileEasting - statesrc->easting;
    y = 0.5 * (double) height + s_2 + currentTileNorthing - statesrc->northing;

    // render tiles

    for (i = 0; i < num_tiles; i++) {

      r = i / tiles_per_side - 1;
      c = i % tiles_per_side - 1;

      if (tileim[i]) {

	glColor3ub(255, 255, 255);
      
	glEnable(GL_TEXTURE_RECTANGLE_NV);

	glBindTexture(GL_TEXTURE_RECTANGLE_NV, tilename[tilelut[i]]);  
      
	glBegin(GL_QUADS);
	  glTexCoord2f(0, s); glVertex2f(x-s_2+c*s, y-s_2+r*s);
	  glTexCoord2f(0, 0); glVertex2f(x-s_2+c*s, y+s_2+r*s);
	  glTexCoord2f(s, 0); glVertex2f(x+s_2+c*s, y+s_2+r*s);
	  glTexCoord2f(s, s); glVertex2f(x+s_2+c*s, y-s_2+r*s);
	glEnd();

	glDisable(GL_TEXTURE_RECTANGLE_NV);

      }
      else {

	glColor3ub(255, 255, 255);
      
	glBegin(GL_QUADS);
	  glVertex2f(x-s_2+c*s, y-s_2+r*s);
	  glVertex2f(x-s_2+c*s, y+s_2+r*s);
	  glVertex2f(x+s_2+c*s, y+s_2+r*s);
	  glVertex2f(x+s_2+c*s, y-s_2+r*s);
        glEnd(); 
      }

      // grid lines

      glColor3ub(128, 128, 0);
      
      glBegin(GL_LINE_LOOP);
        glVertex2f(x-s_2+c*s, y-s_2+r*s);
	glVertex2f(x-s_2+c*s, y+s_2+r*s);
	glVertex2f(x+s_2+c*s, y+s_2+r*s);
	glVertex2f(x+s_2+c*s, y-s_2+r*s);
      glEnd(); 

      // grid tile numbers

      sprintf(tilestring, "%i, %i", tileX + c, tileY + r);
      draw_string(tilestring, x-s_2+c*s+5, y-s_2+r*s+5, 255, 255, 0);

    }

    // draw RDDF boundaries

    rasterize_RDDF(FALSE, NULL);

    // draw icon for sun angle

    draw_sunpos(width/2, height/2, 100, statesrc->sunalt, statesrc->sunazi, 255, 255, 0);

    // draw icon for vehicle position, heading

    draw_arrowhead(width/2, height/2, 20, statesrc->heading, 255, 0, 0);

    // draw aerial road tracer 

    glPointSize(1);

    //    glBegin(GL_POINTS);

//     glColor3ub(255, 255, 0);
// 
//     for (i = 0; i < rf->rtracer->num_samples; i++) 
//       glVertex2f(utm2win_x(rf->rtracer->sample[i]->x[0]), utm2win_y(rf->rtracer->sample[i]->x[1]));
// 
//     glColor3ub(255, 0, 255);

/*
    for (i = 0; i < rf->rtracer->path_length; i++) {
      glColor3ub(i * (255 / rf->rtracer->path_length), i * (255 / rf->rtracer->path_length), 0);
      for (j = 0; j < rf->rtracer->num_samples; j++) 
	glVertex2f(utm2win_x(rf->rtracer->path_x_samp[i][j]), utm2win_y(rf->rtracer->path_y_samp[i][j]));
    }

    glEnd();


    glBegin(GL_POINTS);

    if (drawblock) {
      for (j = 0; j < blocksize; j++) 
 	for (i = 0; i < blocksize; i++) {
	  glColor3ub(block[j][i], 0, 0);
	  glVertex2f(block_x+i-blocksize/2, height-(block_y+j-blocksize/2));
	}
      //      drawblock = FALSE;
    }

    glEnd();
*/

    // draw state history

    int count;

    glColor3ub(0, 255, 0);

//     glPointSize(2);
//     glBegin(GL_POINTS);

    glLineWidth(2);
    glBegin(GL_LINE_STRIP);

    glVertex2f(width/2, height/2);

    for (i = cur_history_pt - 1; i >= 0; i--) 
      glVertex2f(utm2win_x(easting_history[i]),
		 utm2win_y(northing_history[i]));

    if (num_history_pts == max_history_pts) {
      for (i = max_history_pts - 1; i > cur_history_pt; i--) 
	glVertex2f(utm2win_x(easting_history[i]),
		   utm2win_y(northing_history[i]));
    }

    glEnd();

    glLineWidth(1);
    glPointSize(1);

    northing_history[cur_history_pt] = statesrc->northing;
    easting_history[cur_history_pt] = statesrc->easting;

    // just filled buffer

    if (num_history_pts == max_history_pts - 1) {
      num_history_pts = max_history_pts;
      cur_history_pt = 0;
    }

    // buffer still has space

    else if (num_history_pts != max_history_pts) {
      num_history_pts++;
      cur_history_pt++;
    }

    // buffer is already full and we've wrapped around

    else {
      if (cur_history_pt < max_history_pts - 1)
	cur_history_pt++;
      else
	cur_history_pt = 0;
    }

    // iteration number
    
//     sprintf(tilestring, "%i", rf->cum_iterations);
//     draw_string(tilestring, 5, height - 15, 0, 0, 255);

  }

  // finish up

  glutSwapBuffers();
}

//----------------------------------------------------------------------------

int invert_flag = FALSE;

unsigned char *UD_Aerial::read_PPM(char *filename, int *width, int *height)
{
  int i, j, w, h;
  FILE *fp;
  int r, g, b, iscomment, t;
  unsigned char ur, ug, ub;
  char imtype[3];
  char str[80];
  char c;
  unsigned char *im;
  int bpp;
  float w_2, h_2, w_alpha, h_alpha, dist, alpha;

  // get size info

  fp = fopen(filename, "rb");
  if (!fp) {
    printf("read_PPM(): no such file");
    exit(1);
  }

  fscanf(fp, "%s\n", &imtype);

  // attempt to eat comments

  do {
    iscomment = FALSE;
    c = fgetc(fp);
    ungetc(c, fp);
    if (c == '#') {
      iscomment = TRUE;
      fgets (str, 79, fp);
    }
  } while (iscomment);

  // read image dimensions

  fscanf(fp, "%i %i\n255\n", &w, &h);    
  *width = w;
  *height = h;

  w_2 = w / 2;
  h_2 = h / 2;

  // allocate image

  bpp = 4;

  im = (unsigned char *) calloc(w * h * bpp, sizeof(unsigned char));

  // actually read it in

  // ascii

  if (!strcmp(imtype, "P3")) {

    //    printf("P3\n");

    for (j = 0, t = 0; j < h; j++) {
      for (i = 0; i < w; i++, t += bpp) {
	fscanf(fp, "%i %i %i ", &r, &g, &b);
	im[t] = (unsigned char) r;
	im[t+1] = (unsigned char) g;
	im[t+2] = (unsigned char) b;
	if (bpp == 4) {
	  alpha = 255.0;
	  im[t+3] = (unsigned char) alpha;
	}
      }
      fscanf(fp, "\n");
    }
  }

  // binary

  else if (!strcmp(imtype, "P6")) {

    //    printf("P6\n");

    for (j = 0, t = 0; j < h; j++) {
      for (i = 0; i < w; i++, t += bpp) {
	fscanf(fp, "%c%c%c", &ur, &ug, &ub);
	if (!invert_flag) {
	  im[t] = (unsigned char) ur;
	  im[t+1] = (unsigned char) ug;
	  im[t+2] = (unsigned char) ub;
	}
	else {
	  im[t] = (unsigned char) (255 - ur);
	  im[t+1] = (unsigned char) (255 - ug);
	  im[t+2] = (unsigned char) (255 - ub);
	}
	if (bpp == 4) {
	  alpha = 255.0;
	  im[t+3] = (unsigned char) alpha;
	}
      }
    }
  }

  // unknown

  else {
    printf("unrecognized ppm file type");
    exit(1);
  }

  // finish up

  fclose(fp);

  invert_flag = !invert_flag;

  return im;
}

void UD_Aerial::keyboard(unsigned char key, int x, int y)
{
  printf("aerial key\n");
}

//----------------------------------------------------------------------------

void UD_Aerial::mouse(int button, int state, int x, int y)
{
  int i, j, tilenum, pix_x, pix_y, intensity;
  double utm_x, utm_y;

  if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) {

    utm_x = win2utm_x(x);
    utm_y = win2utm_y(height-y-1);
    //utm_y = win2utm_y(y);

    tilenum = utm2tile(utm_x, utm_y);
    pix_x = utm2tilepix_x(utm_x);
    pix_y = 200-utm2tilepix_y(utm_y);

    if (!tileim[tilenum])
      printf("no tile image\n");
    else {

// 	  intensity = (int) UCHAR_R_IMXY(tileim[tilenum], pix_x, pix_y);
//     printf("x = %i, y = %i: I(%i, %i) = %i (%i, %i)\n", x, height-y-1, pix_x, pix_y, intensity, utm2tile_x(utm_x), utm2tile_y(utm_y));

      block_x = x;
      block_y = y;
      for (j = pix_y - blocksize/2; j <= pix_y + blocksize/2; j++) {
	for (i = pix_x - blocksize/2; i <= pix_x + blocksize/2; i++) {
	  block[j-pix_y+blocksize/2][i-pix_x+blocksize/2] = (int) UCHAR_R_IMXY(tileim[tilenum], i, j);
	}
      }
      drawblock = TRUE;
      glutPostRedisplay();
    }
  }

  /*
  if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
    for (i = 0; i < num_tiles; i++) {
      if (tiledom[i])
	tiledom[i]->show = !tiledom[i]->show;

      if (tileim[i]) {

	glBindTexture(GL_TEXTURE_RECTANGLE_NV, tilename[tilelut[i]]);

	if (tiledom[i] && tiledom[i]->show) {
	  //  tiledom[i]->compute(tileim[i]);
	  glTexImage2D(GL_TEXTURE_RECTANGLE_NV, 0, 1, tiledom[i]->gabor_max_response_intensity->width, tiledom[i]->gabor_max_response_intensity->height, 0,  
		       GL_LUMINANCE, GL_UNSIGNED_BYTE, tiledom[i]->gabor_max_response_intensity->imageData);
	}
	else
	  glTexImage2D(GL_TEXTURE_RECTANGLE_NV, 0, 3, tileim[i]->width, tileim[i]->height, 0, GL_RGB, 
		       GL_UNSIGNED_BYTE, tileim[i]->imageData);
      }
    }
    glutPostRedisplay();
  }
  */
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
