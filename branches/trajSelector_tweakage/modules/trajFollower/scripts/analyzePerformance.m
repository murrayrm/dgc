function analyzePerformance(fileName)

     data = load(fileName);

#for format of log file, see LOG_README in dgc/modules/trajFollower/scripts

     num = length(data)(1);

     data01=[];
     data12=[];
     data23=[];
     data34=[];
     data45=[];
     data56=[];
     data67=[];
     data78=[];
     data89=[];
     data910=[];
 
     lastCmd = 0;

    for i=1:1:num
       vel = data(i,12);
       if(vel >=.1 & vel < 1)
	 data01=[data01;data(i,:), lastCmd];
       elseif(vel >= 1 & vel < 2)
         data12=[data12; data(i,:), lastCmd];
       elseif(vel >= 2 & vel < 3)
         data23=[data23; data(i,:), lastCmd];
       elseif(vel >= 3 & vel < 4)
         data34=[data34; data(i,:), lastCmd];
       elseif(vel >= 4 & vel < 5)
         data45=[data45; data(i,:), lastCmd];
       elseif(vel >= 5 & vel < 6)
         data56=[data56; data(i,:), lastCmd];
       elseif(vel >= 6 & vel < 7)
         data67=[data67; data(i,:), lastCmd];
       elseif(vel >= 7 & vel < 8)
         data78=[data78; data(i,:), lastCmd];
       elseif(vel >= 8 & vel < 9)
         data89=[data89; data(i,:), lastCmd];
       elseif(vel >= 9 & vel < 10)
         data910=[data910; data(i,:), lastCmd];
       endif

       lastCmd = data(i,14);

    endfor



    if(!isempty(data01))
     out01=getStats(data01);
    else
     out01=[0,0,0,0,0,0,0];
    endif

    if(!isempty(data12))
     out12 = getStats(data12);
    else
     out12=[0,0,0,0,0,0,0];
    endif
    
    if(!isempty(data23))
     out23=getStats(data23);
    else
     out23=[0,0,0,0,0,0,0];
    endif
    
    if(!isempty(data34))
     out34=getStats(data34);
    else
     out34=[0,0,0,0,0,0,0];
    endif
    
    if(!isempty(data45))
     out45=getStats(data45);
    else
     out45=[0,0,0,0,0,0,0];
    endif
    
    if(!isempty(data56))
     out56=getStats(data56);
    else
     out56=[0,0,0,0,0,0,0];
    endif
    
    if(!isempty(data67))
     out67=getStats(data67);
    else
     out67=[0,0,0,0,0,0,0];
    endif
    
    if(!isempty(data78))
     out78=getStats(data78);
    else
     out78=[0,0,0,0,0,0,0];
    endif
    
    if(!isempty(data89))
     out89=getStats(data89);
    else
     out89=[0,0,0,0,0,0,0];
    endif
    
    if(!isempty(data910))
     out910=getStats(data910);
    else
     out910=[0,0,0,0,0,0,0];
    endif

    output = [out01; out12; out23; out34; out45; out56; out67; out78; out89; out910];

save -ascii output.temp output;

 file2=horzcat(fileName,"_stats");
     cmd=horzcat("mv output.temp ",file2);
   #  fflush(stdout)
     system(cmd);

#plot(abs(fft(data(:,3))))
#plot(abs(fft(data(:,14))))

#gset terminal pdf; gset output "FT_yerr.pdf"; plot(abs(fft(data(:,3))));
#gset terminal pdf; gset output "FT_cmd.pdf"; plot(abs(fft(data(:,14))));


end

