;----------------------------------------
; extra functions
;----------------------------------------

; insert date and time for notetaking stuff
(defun insert-date-time ()
   "Inserts human readable date and time string." (interactive)
     (insert (format-time-string "[%Y-%m-%d %l:%M:%S %P] ")))

(fset 'goto-other-buffer "\C-xb\C-m")

;(fset 'bugme-line
;   "bugme; \C-[OB\C-[OH")

(fset 'bugme-line
   "\C-[OHbugme; \C-[OB\C-[OH")

(fset 'unbugme-line
   "\C-[%bugme; \C-m\C-m.")

(fset 'bugme-insert-definition
   "#define bugme BUGME(__FILE__, __LINE__)\C-mvoid BUGME(string file, int line) {cout << \" [\" << file << \":\" << line << \"]\" << endl;}\C-m")



;----------------------------------------
; key bindings
;----------------------------------------

(global-set-key [(control b)] 'goto-other-buffer)
(global-set-key [backtab] 'dabbrev-expand)
(global-set-key [(control ?c) (?r)] 'insert-date-time)
(global-set-key [(control v)] 'other-window)
(global-set-key [(control meta b)] 'bugme-line)
(global-set-key [(control meta u)] 'unbugme-line)
(global-set-key [(control meta i)] 'bugme-insert-definition)



;----------------------------------------
; variables
;----------------------------------------

(set-variable 'tex-dvi-view-command "xdvi")



;----------------------------------------
; modes
;----------------------------------------

(autoload 'octave-mode "octave-mod" nil t)	
(setq auto-mode-alist (cons '("\\.m$". octave-mode) auto-mode-alist))

