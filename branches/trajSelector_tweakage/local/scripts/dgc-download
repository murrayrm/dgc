#!/bin/bash
#
# /usr/local/dgc-download - copy databases from gc to field server
# RMM, 24 Jul 04
#
# Usage: dgc-download [[-flags] user@remote]
#
# This script makes copies files from grand challenge to the field server
# using rsync.  It should be run after dgc-offline has been run on
# grand challenge.
#
# $Id$

DGC=/dgc
REMOTE=team@gc.caltech.edu
RSFLGS=-rav

# See if we were passed a remote path as an argument
# Use $* to allow optional flags as well
if ( [ "$1" ] ) then
  REMOTE=$*
else
  echo "Usage: dgc-download [flags] user@machine"
  exit 1;
fi
echo Using $REMOTE as remote source for download

# Shut down the MySQL server so that we can make copies of the databases
# Location varies between field server and GC
echo "Shutting down MySQL server"
if ( [ -e /etc/rc.d/rc.mysqld ] ) then
  /etc/rc.d/rc.mysqld stop
fi
if ( [ -e /etc/init.d/mysql ] ) then
  /etc/init.d/mysql stop
fi

#
# Now use rdist to copy over everything that we need
#
# Wiki: mysql database ('wikidb') + uploaded files (in images)
# Bugzilla: mysql database ('bugs')
# Subversion: DGC source code
# 

echo -e "\n== Downloading Subvesion =="
rsync $RSFLGS ${REMOTE}:$DGC/subversion $DGC

echo -e "\n== Downloading Wiki == "
rsync $RSFLGS ${REMOTE}:$DGC/wiki $DGC

echo -e "\n== Downloading Bugzilla =="
rsync $RSFLGS ${REMOTE}:$DGC/bugzilla $DGC

echo -e "\n== Downloading MySQL =="
rsync $RSFLGS ${REMOTE}:$DGC/mysql $DGC

echo -e "\n== Downlaoding documentation =="
rsync $RSFLGS ${REMOTE}:$DGC/www/doc $DGC/www
rsync $RSFLGS ${REMOTE}:$DGC/www/GrandChallenge $DGC/www

# Restart the MySQL server
echo -e "\n== Restarting MySQL server =="
if ( [ -e /etc/rc.d/rc.mysqld ] ) then
  /etc/rc.d/rc.mysqld start
fi
if ( [ -e /etc/init.d/mysql ] ) then
  /etc/init.d/mysql start
fi

sleep 1
