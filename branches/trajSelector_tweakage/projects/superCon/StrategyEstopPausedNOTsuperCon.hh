//SUPERCON STRATEGY TITLE: estop pause NOT (caused by) superCon (i.e. DARPA/adrive) - BLOCKING-ANYTIME STRATEGY
//HEADER FILE (.hh)
//DESCRIPTION: This strategy is essentially a 'holding-pen' for superCon's
//state machine when adrive/DARPA (via the remote e-stop controller) execute an
//estop PAUSE (e-stop disable is not considered, as under these circumstances
//our race is over anyway).  This strategy is entered when the position of
//the DARPA estop --> PAUSE, and the strategy is NOT LEFT UNTIL DARPA's
//estop --> RUN, note that while in this strategy NO moves to ANY other
//strategies are allowed (INCLUDING ALL OTHER ANYTIME STRATEGIES).
//Once this strategy is COMPLETE, it will transition to the NOMINAL strategy
//
//NOTE: as all scThrows (= scMessages - same thing) are cleared after the
//the SCstate table has been copied, they must be re-sent if they are to be
//acted on.  Also note that NO superCon actions will be taken during a DARPA
//estop, this may potentially be less efficient, however the lost time should
//not be significant, and it simplifies significantly how superCon would
//deal with adrive/DARPA estop pauses.

#include "Strategy.hh"
#include "sc_specs.h"

class CStrategyEstopPausedNOTsuperCon : public CStrategy
{

private:

  //# of the next stage to be executed - Stage #'s START at 1 (i.e. when
  //entering a new strategy nextStage = 1) - this is stored internally
  //by any strategy class instance so the information does not have to
  //be passed in stepForward(...)
  int nextStage;
  
  //latching bool instance to track whether to skip the operations for the
  //current stage -> value (true/false) updated by the entry operations.
  //NOTE: stage operations SKIPPED, iff skipStageOps == true.
  bool_latched skipStageOps;

public:

  /** CONSTRUCTORS **/
  CStrategyEstopPausedNOTsuperCon() : CStrategy() {}  

  /** DESTRUCTOR **/
  ~CStrategyEstopPausedNOTsuperCon() {}


  /** MUTATORS **/

  void stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface );
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

};
