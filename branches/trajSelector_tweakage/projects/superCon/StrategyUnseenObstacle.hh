//SUPERCON STRATEGY TITLE: Unseen-Obstacle - ANYTIME STRATEGY
//HEADER FILE (.hh)
//DESCRIPTION: This strategy updates the superCon layer in the map to reflect
//obstacles whose existence has been confirmed through investigation, i.e.
//Alice tried to drive forwards through an area, and her progress was
//impeded by an obstacle.  Note that this strategy is NOT responsible for
//identifying that there is an intraversible obstacle in front of Alice - 
//which is handled in the strategy-transition conditions (evaluated prior
//to executing any stepForward(...) method)

#include "Strategy.hh"
#include "sc_specs.h"

class CStrategyUnseenObstacle : public CStrategy
{

private:

  //# of the next stage to be executed - Stage #'s START at 1 (i.e. when
  //entering a new strategy nextStage = 1) - this is stored internally
  //by any strategy class instance so the information does not have to
  //be passed in stepForward(...)
  int nextStage;
  
  //latching bool instance to track whether to skip the operations for the
  //current stage -> value (true/false) updated by the entry operations.
  //NOTE: stage operations SKIPPED, iff skipStageOps == true.
  bool_latched skipStageOps;

public:
  
  /*** METHODS ***/

  /** CONSTRUCTORS **/
  CStrategyUnseenObstacle() : CStrategy() {}  

  /** DESTRUCTOR **/
  ~CStrategyUnseenObstacle() {}

  /** MUTATORS **/
  void stepForward(const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface);
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

};
