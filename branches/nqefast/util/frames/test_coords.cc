// test program for coordinate systems

#include <iostream>
#include "frames/frames.hh"

#include "MTA/Misc/Time/Timeval.hh"

int main()
{
  XYcoord coord(0, 0);  
  double scalar;

  char again = 'y'; // yes no for continue
  
  while(again == 'y')
  {  
    cout<<"Enter X coord: ";
    cin>>coord.X;
    cout<<"Enter Y coord: ";
    cin>>coord.Y;
    cout<<"Enter scalar multiplier: ";
    cin>>scalar;


    cout<<"X: "<<coord.X<<" Y: "<<coord.Y<<endl;
    cout<<"scalar: "<<scalar<<endl;
    cout<<"[X Y] + [X Y]: ["<<(coord+coord).X<<" "<<(coord+coord).Y<<"]"<<endl;
    cout<<"scalar * [X Y]: ["<<(coord*scalar).X<<" "<<(coord*scalar).Y<<"]"<<endl;

    cout<<"Again? (y/n)";
    cin>>again;
  }
      
  return 0;
}
