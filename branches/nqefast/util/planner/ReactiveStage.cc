// #define OUTNODES

#include "ReactiveStage.h"
#include "MapAccess.h"

#ifdef OUTNODES
#include <iomanip>
ofstream outnodes;
#endif

// This is intended to get the far corner on turns. pathgen will take the inside
// on turns, necessitating this factor. There should be a better way to do
// this...
#define BUILD_LAYER_EXTRA_WIDTH_FACTOR 1.5

CReactiveStage::CReactiveStage(CMap *pMap, int mapLayer, RDDF* pRDDF, bool USE_MAXSPEED)
	: m_trajgenpath("rddf.dat"), m_pMap(pMap), m_mapLayer(mapLayer), m_pRDDF(pRDDF)
{
	m_pLayers = new SLayer[MAX_NUM_LAYERS];
	m_pTraj   = new CTraj;

#ifdef OUTNODES
	outnodes.open("nodes");
	outnodes << setprecision(10);
#endif
}

CReactiveStage::~CReactiveStage()
{
	delete[] m_pLayers;
	delete   m_pTraj;
}

int CReactiveStage::run(VehicleState* pVehState, bool bIsStateFromAstate,
												CTraj* pPrevTraj)
{
	int layerIndex, nodeIndex;
	int numLayers;

	NEcoord pos;
	if(bIsStateFromAstate)
	{
		pos = NEcoord(pVehState->Northing_rear(), pVehState->Easting_rear());
		m_initSpeed = hypot(pVehState->Vel_N + VEHICLE_WHEELBASE*pVehState->YawRate*sin(pVehState->Yaw), 
												pVehState->Vel_E - VEHICLE_WHEELBASE*pVehState->YawRate*cos(pVehState->Yaw));
	}
	else
	{
		pos = NEcoord(pVehState->Northing, pVehState->Easting);
		m_initSpeed = hypot(pVehState->Vel_N, pVehState->Vel_E);
	}

	// get the trajgen traj. The reactive stage is based off of this traj
	m_trajgenpath.SeedFromLocation(pos.N, pos.E, pVehState->Yaw, &m_trajgentraj, PATHGENSEED_MERGEDIST);

	int    trajIndex = 0;
	double halfwidth;

	double sintheta;
	double costheta;
	NEcoord vecAtTrajindex;
	double  vecnorm;

	// compute the middle of the two keepout circles (based on maximum steering
	// angles at the start)
	NEcoord CenterOfLeftKeepout  = pos;
	NEcoord CenterOfRightKeepout = pos;
	double sinyaw = sin(pVehState->Yaw);
	double cosyaw = cos(pVehState->Yaw);
	CenterOfLeftKeepout .N -= VEHICLE_MIN_TURNING_RADIUS*sinyaw;
	CenterOfLeftKeepout .E += VEHICLE_MIN_TURNING_RADIUS*cosyaw;
	CenterOfRightKeepout.N += VEHICLE_MIN_TURNING_RADIUS*sinyaw;
	CenterOfRightKeepout.E -= VEHICLE_MIN_TURNING_RADIUS*cosyaw;

	double 	distIntoPlan = 0.0;

	double nodataUnpassableCutoffDist;
	double stoppingDistance;
	stoppingDistance = m_initSpeed*m_initSpeed / 2.0 / VEHICLE_MAX_DECEL;
	nodataUnpassableCutoffDist = fmax(stoppingDistance * UNPASSABLE_DISTANCE_TWEAK,
																		NODATA_UNPASSABLE_CUTOFF_DIST);

	// first build the first layer that just contains the starting point.
	// In this degenerate case, spacing has to be anything nonzero, so we use 1.0
	buildLayer(0, pos, 0.0, 1.0,  0.0, -1e8, 1e8, 1.0, false, NULL);

	// now build the rest of the layers in the dense section
	layerIndex=1;

	do
	{
		distIntoPlan += LAYER_SPACING1;

		double dummy;
		NEcoord prevpos = pos;

		// move up the trajgen path
		trajIndex = m_trajgentraj.getPointAhead(trajIndex, LAYER_SPACING1);
		pos.N = m_trajgentraj.getNorthing(trajIndex);
		pos.E = m_trajgentraj.getEasting (trajIndex);

		vecAtTrajindex.N = m_trajgentraj.getNorthing(trajIndex+1) - m_trajgentraj.getNorthing(trajIndex);
		vecAtTrajindex.E = m_trajgentraj.getEasting (trajIndex+1) - m_trajgentraj.getEasting (trajIndex);
		vecnorm = vecAtTrajindex.norm();
		sintheta = vecAtTrajindex.E/vecnorm;
		costheta = vecAtTrajindex.N/vecnorm;

		m_pRDDF->getPointAlongTrackLine(pos, 0.0, &dummy, &halfwidth);


		// Compute the limits on the points, based on initial yaw and maximum
		// steering angle
		double diffN;
		double diffE;
		double projLen;
		double projPerpLenSq;
		double halfChordSq;
		double maxr, maxl;

		diffN = CenterOfRightKeepout.N - pos.N;
		diffE = CenterOfRightKeepout.E - pos.E;
		projLen = diffN * ( sintheta) + diffE * (-costheta);
		projPerpLenSq = diffN*diffN + diffE*diffE - projLen*projLen;
		halfChordSq = VEHICLE_MIN_TURNING_RADIUS*VEHICLE_MIN_TURNING_RADIUS - projPerpLenSq;
		if(halfChordSq > 0.0)
			maxr = projLen - sqrt(halfChordSq);
		else
			maxr = 1e8;

		diffN = CenterOfLeftKeepout.N - pos.N;
		diffE = CenterOfLeftKeepout.E - pos.E;
		projLen = diffN * (-sintheta) + diffE * (costheta);
		projPerpLenSq = diffN*diffN + diffE*diffE - projLen*projLen;
		halfChordSq = VEHICLE_MIN_TURNING_RADIUS*VEHICLE_MIN_TURNING_RADIUS - projPerpLenSq;
		if(halfChordSq > 0.0)
			maxl = projLen - sqrt(halfChordSq);
		else
			maxl = 1e8;

		buildLayer(layerIndex, pos, sintheta, costheta, halfwidth*BUILD_LAYER_EXTRA_WIDTH_FACTOR, -maxl, maxr, SPACING_ACROSS_LAYER1,
							 nodataUnpassableCutoffDist < distIntoPlan, pPrevTraj);
		layerIndex++;
	} while(distIntoPlan < REACTIVE_DENSE_LENGTH);

	// now build the rest of the layers in the sparse section.
	// Note that this block of code is a copy of the one directly above
	do
	{
		distIntoPlan += LAYER_SPACING2;

		double dummy;
		NEcoord prevpos = pos;

		// move up the trajgen path
		trajIndex = m_trajgentraj.getPointAhead(trajIndex, LAYER_SPACING2);
		pos.N = m_trajgentraj.getNorthing(trajIndex);
		pos.E = m_trajgentraj.getEasting (trajIndex);

		vecAtTrajindex.N = m_trajgentraj.getNorthing(trajIndex+1) - m_trajgentraj.getNorthing(trajIndex);
		vecAtTrajindex.E = m_trajgentraj.getEasting (trajIndex+1) - m_trajgentraj.getEasting (trajIndex);
		vecnorm = vecAtTrajindex.norm();
		sintheta = vecAtTrajindex.E/vecnorm;
		costheta = vecAtTrajindex.N/vecnorm;

		// we're now far enough from the vehicle, so likely nothing is cut off with
		// the maximum steering angle. Thus 1e8
		m_pRDDF->getPointAlongTrackLine(pos, 0.0, &dummy, &halfwidth);
		buildLayer(layerIndex, pos, sintheta, costheta, halfwidth*BUILD_LAYER_EXTRA_WIDTH_FACTOR, -1e8, 1e8, SPACING_ACROSS_LAYER2,
							 nodataUnpassableCutoffDist < distIntoPlan, NULL);
		layerIndex++;
	} while(distIntoPlan < REACTIVE_LENGTH);

	numLayers = layerIndex-1;

	// now solve for the costs. Loop through the layers and for each layer, loop
	// through the nodes in the next layer.
	for(layerIndex=0; layerIndex<numLayers; layerIndex++)
	{
		for(nodeIndex=0; nodeIndex<m_pLayers[layerIndex+1].m_numNodes; nodeIndex++)
		{
			processLayerToPoint(layerIndex, nodeIndex);
		}
	}

	// find which of the nodes in the last layer have the highest cost
	// start out at the worst (longest time) possible cost
	double bestCost = 1.0e20;
	int    bestCostIndex = 0;
	double curCost;
	for(nodeIndex=0; nodeIndex<m_pLayers[numLayers].m_numNodes; nodeIndex++)
	{
		curCost = m_pLayers[numLayers].m_pNodes[nodeIndex].m_bestCost;

		if(curCost < bestCost)
		{
			bestCost      = curCost;
			bestCostIndex = nodeIndex;
		}
	}

	// now store the best route into a traj structure. We have to do this
	// backwards, and we need the result to contain evenly spaced points.

	// nodeIndex is the best node in the NEXT layer
	nodeIndex = bestCostIndex;
	SNode* pBestNodeNextLayer;

	// nodeIndexThisLayer is the best node in THIS layer. the nodeIndex node
	// points to this
	int nodeIndexThisLayer;
	SNode* pBestNodeThisLayer;

	// First compute the length of the result
	double length = 0.0;
	for(layerIndex = numLayers-1; layerIndex>=0; layerIndex--)
	{
		pBestNodeNextLayer = & m_pLayers[layerIndex+1].m_pNodes[nodeIndex];
		nodeIndexThisLayer = pBestNodeNextLayer->m_backPointer;
		pBestNodeThisLayer = & m_pLayers[layerIndex  ].m_pNodes[nodeIndexThisLayer];

		NEcoord vecdiff = (pBestNodeNextLayer->m_pos - pBestNodeThisLayer->m_pos);
		length += vecdiff.norm();

		nodeIndex = nodeIndexThisLayer;
	}

	double outputPointSpacing = length / (double)(REACTIVE_OUTPUT_POINTS - 1);

	// The following routine assumes that the output point spacing is less than
	// the segment length. Extra 2.0 is a safety factor since, I didn't think
	// about it throughly enough...
	if(2.0*outputPointSpacing > REACTIVE_LENGTH / (double)numLayers)
	{
		cerr << "PlannerReactive error: output point spacing too sparse for my stupid output routine..." << endl;
		return -3;
	}

	int pointIndex;

	// nodeIndex is the best node in the NEXT layer
	nodeIndex = bestCostIndex;

	m_pTraj->setNumPoints(REACTIVE_OUTPUT_POINTS);

	// start at the end
	layerIndex = numLayers-1;
	pBestNodeNextLayer = & m_pLayers[layerIndex+1].m_pNodes[nodeIndex];
	nodeIndexThisLayer = pBestNodeNextLayer->m_backPointer;
	pBestNodeThisLayer = & m_pLayers[layerIndex  ].m_pNodes[nodeIndexThisLayer];
	double posInSegment = 1.0;
	NEcoord curPoint = pBestNodeNextLayer->m_pos;
	NEcoord vecdiff = pBestNodeNextLayer->m_pos - pBestNodeThisLayer->m_pos;

	for(pointIndex = REACTIVE_OUTPUT_POINTS-1; pointIndex>=0; pointIndex--)
	{
		m_pTraj->setNorthing(pointIndex, curPoint.N);
		m_pTraj->setEasting (pointIndex, curPoint.E);

		// is the next point still in this segment or the next one?
		if(vecdiff.norm() * posInSegment >= outputPointSpacing || layerIndex==0)
		{
			posInSegment -= outputPointSpacing/vecdiff.norm();
			curPoint -= vecdiff*(outputPointSpacing/vecdiff.norm());
		}
		else
		{
			double leftInNextSegment = outputPointSpacing - vecdiff.norm() * posInSegment; // this is in meters
			layerIndex--;
			nodeIndex = nodeIndexThisLayer;
			pBestNodeNextLayer = & m_pLayers[layerIndex+1].m_pNodes[nodeIndex];
			nodeIndexThisLayer = pBestNodeNextLayer->m_backPointer;
			pBestNodeThisLayer = & m_pLayers[layerIndex  ].m_pNodes[nodeIndexThisLayer];
			vecdiff = pBestNodeNextLayer->m_pos - pBestNodeThisLayer->m_pos;

			posInSegment = 1.0 - leftInNextSegment/vecdiff.norm();
			curPoint = pBestNodeThisLayer->m_pos + vecdiff*posInSegment;			
		}
	}
	return 0;
}

// computes the connections from all the nodes in layer layerIndex to node
// nextNodeIndex in the next layer
void CReactiveStage::processLayerToPoint(int layerIndex, int nextNodeIndex)
{
	int thisNodeIndex;

	// start out at the worst (longest time) possible cost
	double bestCost = 1.0e20;
	int    bestCostIndex = 0;
	double curCost;

	// search all of the nodes in this layer to find the best way to the point in
	// the next layer. we find through which point in this layer that path
	// goes. Then we store this info into the node in the next layer
	for(thisNodeIndex=0; thisNodeIndex<m_pLayers[layerIndex].m_numNodes; thisNodeIndex++)
	{
		curCost =
			costBetweenNodes(m_pLayers[layerIndex]  .m_pNodes[thisNodeIndex],
											 m_pLayers[layerIndex+1].m_pNodes[nextNodeIndex],
											 m_pLayers[layerIndex].m_bPassableNodata)
			+ m_pLayers[layerIndex].m_pNodes[thisNodeIndex].m_bestCost;

		if(curCost < bestCost)
		{
			bestCost      = curCost;
			bestCostIndex = thisNodeIndex;
		}
	}

	m_pLayers[layerIndex+1].m_pNodes[nextNodeIndex].m_bestCost    = bestCost;
	m_pLayers[layerIndex+1].m_pNodes[nextNodeIndex].m_backPointer = bestCostIndex;
}

void CReactiveStage::buildLayer(int layer, NEcoord& pos, double sinTheta, double cosTheta,
																double halfwidth, double maxLeft, double maxRight,
																double spacingAcrossLayer, bool bPassableNodata,
																CTraj* pPrevTraj)
{
	int i;

	m_pLayers[layer].m_sinTheta        = sinTheta;
	m_pLayers[layer].m_cosTheta        = cosTheta;
	m_pLayers[layer].m_radLow          = -halfwidth;
	m_pLayers[layer].m_radHigh         =  halfwidth;
	m_pLayers[layer].m_layerMiddle     = pos;
	m_pLayers[layer].m_bPassableNodata = bPassableNodata;

	// this computes line intersections. To handle turns better. This is written up
	// on the second-to-last page in dima's notebook
	NEcoord v2(sinTheta, -cosTheta);
	for(i=1; i<layer; i++)
	{
		NEcoord delta = m_pLayers[layer].m_layerMiddle - m_pLayers[i].m_layerMiddle;

		// if these two layers are too far apart to interact, skip
		if(delta.norm() > halfwidth + fmax(-m_pLayers[i].m_radLow, m_pLayers[i].m_radHigh))
			continue;

		NEcoord v1(m_pLayers[i].m_sinTheta, -m_pLayers[i].m_cosTheta);
		double cosdiff = v1 * v2;

		double beta = delta * (v1*cosdiff - v2) / (1.0 - cosdiff*cosdiff);
		double alpha = delta*v1 + beta*cosdiff;

		// if the two layers are parallel, we can't cut off anything
		if(isnan(alpha) || isnan(beta))
			continue;

		// if intersecting, cut off
		if     (beta<0.0 && (m_pLayers[layer].m_radLow  - (beta +NODE_CUTOFF_THRESHOLD)) < 0.0 &&
						( (alpha<0.0 && (m_pLayers[i    ].m_radLow  - (alpha+NODE_CUTOFF_THRESHOLD)) < 0.0) ||
							(alpha>0.0 && (m_pLayers[i    ].m_radHigh - (alpha-NODE_CUTOFF_THRESHOLD)) > 0.0)
			      )
		       )
		{
			m_pLayers[layer].m_radLow = beta+NODE_CUTOFF_THRESHOLD;
		}
		else if(beta>0.0 && (m_pLayers[layer].m_radHigh - (beta -NODE_CUTOFF_THRESHOLD)) > 0.0 &&
						( (alpha>0.0 && (m_pLayers[i    ].m_radHigh - (alpha-NODE_CUTOFF_THRESHOLD)) > 0.0) ||
							(alpha<0.0 && (m_pLayers[i    ].m_radLow  - (alpha+NODE_CUTOFF_THRESHOLD)) < 0.0)
						)
					 )
		{
			m_pLayers[layer].m_radHigh = beta-NODE_CUTOFF_THRESHOLD;
		}
	}


	if(maxLeft < maxRight)
	{
		m_pLayers[layer].m_radLow  = fmax(m_pLayers[layer].m_radLow , maxLeft );
		m_pLayers[layer].m_radHigh = fmin(m_pLayers[layer].m_radHigh, maxRight);
	}
	else
		cerr << "planner firststage: maxLeft > maxRight. god damn herman" << endl;

	double r = m_pLayers[layer].m_radLow;
	int numsteps = (int)((m_pLayers[layer].m_radHigh - m_pLayers[layer].m_radLow) / spacingAcrossLayer);
	numsteps = min(MAX_POINTS_PER_LAYER, numsteps);
	numsteps = 2*((numsteps+1)/2); // make numsteps even to use the center point

	// delr will be INF if numsteps==0, but then delr wouldn't be used anyway
	double delr  = (m_pLayers[layer].m_radHigh - m_pLayers[layer].m_radLow) / (double) numsteps;

	for(i=0; i<=numsteps; i++)
	{
		double n,e;
		n = pos.N + r*sinTheta;
		e = pos.E - r*cosTheta;

		m_pLayers[layer].m_pNodes[i].m_pos = NEcoord(n,e);

		if(pPrevTraj != NULL)
		{
			int idx = pPrevTraj->getClosestPoint(n,e);
			double dn = pPrevTraj->getNorthing(idx) - n;
			double de = pPrevTraj->getEasting (idx) - e;
			m_pLayers[layer].m_pNodes[i].m_distToPrevSq = dn*dn + de*de;
		}
		else
			m_pLayers[layer].m_pNodes[i].m_distToPrevSq = 0.0;

#ifdef OUTNODES
		outnodes << pos.N+r*sinTheta << ' ' << pos.E-r*cosTheta << endl;
#endif
		r += delr;
	}

	m_pLayers[layer].m_numNodes = numsteps+1;
	if(numsteps+1 > MAX_NUM_NODES_PER_LAYER)
	{
		cerr << "PlannerReactive: too many nodes per layer; increase MAX_NUM_NODES_PER_LAYER" << endl;
		cerr << "m_numNodes = " << numsteps+1 << endl;
	}
}


// double CReactiveStage::costBetweenNodes(SNode& node1, SNode& node2)
// {
// 	NEcoord index = node1.m_pos;
// 	NEcoord diff  = node2.m_pos - node1.m_pos;
// 	double  len   = diff.norm();
// 	int numsteps  = lround(len);
// 	double  step  = len / (double)numsteps;

// 	double theta  = atan2(diff.E, diff.N);
// 	double  cost  = 0.0;

// 	diff = diff / len * step;

// 	for(int i=0; i<=numsteps; i++)
// 	{
// 		double f, dfdN, dfdE, dfdtheta;
// 		getContinuousMapValueDiffGrown(m_pMap, m_mapLayer,
// 																	 index.N + diff.N/2.0,
// 																	 index.E + diff.E/2.0,
// 																	 theta, &f, &dfdN, &dfdE, &dfdtheta);

// 		cost += 1.0 / f;
// 		index += diff;
// 	}
// 	cost *= len / (double)(numsteps + 1);

// 	return cost;
// }

double CReactiveStage::costBetweenNodes(SNode& node1, SNode& node2,
																				bool bPassableNodata)
{
	NEcoord diff  = node2.m_pos - node1.m_pos;
	double  len   = diff.norm();

	if(len < 1e-3)
		return 0.0;

	return 
		FIRSTSTAGE_PROXIMITY_TO_PREV_FACTOR*(node1.m_distToPrevSq + node2.m_distToPrevSq) +
		getTimeMapValueGrownNoDiff(m_pMap, m_mapLayer, bPassableNodata,
															 m_initSpeed,
															 node1.m_pos.N + diff.N/2.0,
															 node1.m_pos.E + diff.E/2.0,
															 diff.E/len, diff.N/len,
															 len);
}
