t = (1:100)';
x = (t > 50) - (t > 70) + 0.05;

r = 2;
k = [0 1 1 1 1];

fx = 1.0 ./ x;

g  = zeros(r,1);
gw = zeros(r,1);
for i=(r+1):(length(t)-r)
	g = [g; length(find(k)) / (k * fx((i-r) : (i+r)))];
	gw = [gw; (k * fx((i-r) : (i+r))) / (k * (fx((i-r) : (i+r)).*fx((i-r) : (i+r))))];
end;


g  = [g ; zeros(r,1)];
gw = [gw; zeros(r,1)];

plot(t,x, 'o-', t,g,'bo-', t,gw,'go-')
