/*
 *  Gazebo - Outdoor Multi-Robot Simulator
 *  Copyright (C) 2003  
 *     Nate Koenig & Andrew Howard
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
/* Desc: Model for a ClodBusterMod
 * Author: Pranav Srivastava
 * Date: 4 Sep 2003
 * CVS: $Id: ClodBuster.hh,v 1.8 2004/06/01 17:12:49 natepak Exp $
 */

#ifndef CLODBUSTERMOD_HH
#define CLODBUSTERMOD_HH

#include "Model.hh"

// Forward declarations
class Body;
class HingeJoint;
class Geom;
//class SphereGeom;
//class GeomData;

typedef struct _gz_position_t gz_position_t;


class ClodBusterMod  : public Model
{
  // Construct, destructor
  public: ClodBusterMod( World *world );
  public: virtual ~ClodBusterMod();

  // Load the model
  public: virtual int Load( WorldFile *file, WorldFileNode *node );

  // Initialize the model
  public: virtual int Init( WorldFile *file, WorldFileNode *node );

  // Finalize the model
  public: virtual int Fini();

  // Update the model state
  public: virtual void Update( double step );

  // Update the odometry
  private: void UpdateOdometry( double step );

  // Load ODE stuff
  private: int OdeLoad( WorldFile *file, WorldFileNode *node );

  // Initialize ODE
  private: int OdeInit( WorldFile *file, WorldFileNode *node );

  // Finalize ODE
  private: int OdeFini();

  // Initialize the external interface
  private: int IfaceInit();

  // Finalize the external interface
  private: int IfaceFini();

  // Get commands from the external interface
  private: void IfaceGetCmd();
  
  // Update the data in the external interface
  private: void IfacePutData();
  
  // Robot parameters
  private: double wheelSep, wheelDiam;
  
  // ODE components
  private: Body *body;
  private: Geom *bodyGeoms[4];
  private: Body *tires[4];
  private: Body *stubs[4];
  private: HingeJoint *wjoints[4];
  private: HingeJoint *sjoints[4];
 
 // External interface
 private: gz_position_t *iface;

  // Wheel speeds (left and right wheels are paired)
  private: double wheelSpeed[2];

  // Odometric pose estimate
  private: double odomPose[3];
  // Encoder counts
  private: double encoders[2];
  private: bool raw_encoder_position;
};

#endif
