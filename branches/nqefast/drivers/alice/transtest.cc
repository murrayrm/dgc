#include <iostream>

#include "igntrans.h"

using namespace std;

int main()
{
  int port;
  int input1;
  int input2;
  int input3;
  cout << "TRANSTEST Initializing transmission and ignition actuator.  What port would you like?" << endl;
  cin >> port;
  cout << endl << "TRANSTEST opening port " << port << endl;
  if (igntrans_open(port))
    cout << "TRANSTEST Sucessfully opened port " << port << endl;
  else 
    {
      cout << "TRANSTEST Failed to open port" << endl;
      return 1;
    }
  
  cout << "TRANSTEST Type a number for LED1 through 4 5 = trans 6 = ignition" << endl;
  cout << "Then select 1 to send 2 for recieve" << endl;
  cout << "Then if sending choose a postion" << endl;  
  while (1)
    {
      cout << "Choose element:" ;
      cin >> input1;
      cout << "send or recieve :";
      cin >> input2;
      cout << "choose position :";
      cin >> input3;
      
      if (input1 < 5)
	{
	  if(input2 == 1)
	    {
	      cout << "TRANSTEST let_setposition(" << input1 <<", " << input3 << ") returned " << led_setposition(input1, input3) << endl;
	    }
	  else
	    cout <<"TRANSTEST led_getpostion(" << input1 << ") returned " <<   led_getposition(input1) << endl;
	  
	}
      else if (input1 == 5)
	{
	  if(input2 == 1)
	    {
	      cout << "TRANSTEST trans_setposition(" << input3 << ") returned " << trans_setposition( input3) << endl;
	    }
	  else
	    cout << "TRANSTEST trans_getposition returned "<< trans_getposition() << endl;
	  
	}
      else if (input1 == 6)
	{
	  if(input2 == 1)
	    {
	      cout << "TRANSTEST ign_setposition(" << input3 << ") returned " << ign_setposition(input3) << endl;
	    }
	  else
	    cout << "TRANSTEST ign_getpostion returned " << ign_getposition() << endl;

	} 
    }
  return 0;
}
