
#ifndef LADAR_BUFFER_H
#define LADAR_BUFFER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <iostream>

#define LADAR_BUF_SIZE 100  /* in frames */
#define LADAR_BUF_FILL_FRAME 2
#define LADAR_BUF_FILL_MSG 1

using namespace std;

class CLadarDataBuffer { 
	private:
	char* frames[LADAR_BUF_SIZE];	
	int frame_size;
	int c_frame;
	int c_frame_offset;
	int last_frame_sent; 
	int num_frames() { return (c_frame - last_frame_sent - 1 + LADAR_BUF_SIZE) % LADAR_BUF_SIZE; }
	
	int _NextFrame(); 
	public:
	CLadarDataBuffer();
	~CLadarDataBuffer();
	
	void Init(int new_fsize);
	void DeInit();
	int FillMsg(char *msg, int size, FILE* log = NULL); 
	char *GetFrame();
	int IsFull();
	int IsEmpty();
	void Empty();
	void LogAllFrames(FILE *log);
};

#endif
