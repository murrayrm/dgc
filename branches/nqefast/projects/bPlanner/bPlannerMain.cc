#include <iostream>
#include "raid.hh"
#include "bPlanner.hh"
#include "bPlannerConfig.hh"

using namespace std;


void printHelp();

int main(int argc, char** argv)
{
  int skynet_key;
  char* p_skynet_key = getenv("SKYNET_KEY");
  if ( p_skynet_key == NULL )
    {
      cout << "You need to set a skynet key!" << endl;
      return 1;
    }
  else
    {
      skynet_key = atoi(p_skynet_key);
      cout << "Using skynet key: " << skynet_key << endl;
    }

  //  pd("got here yo!", rmsg::normal, 0);

  bPlanner plnObj(skynet_key);

  printHelp();
  return 0;
}



void printHelp()
{
  cout << "Help goes here" << endl;
}
