#!/bin/bash

# this should make stuff in the right order...i use this because 
# we don't have a master build yet
#
# jason
#
# run ./makegtk.sh to make the stuff for skynetgui


DGC=../..
SKYNET=$DGC/projects/skynet
MODULEHELPERS=$DGC/projects/ModuleHelpers
TRAJ=$DGC/projects/traj
TRAJCONTROLLER=$DGC/projects/skynet
SIMMODELS=$DGC/projects/simulator/models
SIMULATOR=$DGC/projects/simulator
MODEMAN=$DGC/projects/ModeMan
MODEMANMODULE=$DGC/projects/ModeManModule
PIDCONTROLLER=$DGC/projects/PID_Controller
TRAJFOLLOWER=$DGC/projects/sn_TrajFollower
RDDFPATHGEN=$DGC/projects/RddfPathGen



# check if clean argument is given
if [ $1 ]; then
  if [ $1=="clean" ]; then
      echo "-clean switch found!"
  else
      echo "Unrecognized switch!"
  fi
  echo "You must be in the /dgc/local/scripts directory when running this script!"
  echo "If this is the case, press enter to continue, otherwise, quit with Ctrl-C."
  read foo
  cd $DGC; make clean; make; make install; cd -;
  cd $SKYNET; make clean; make; make install; cd -;
  cd $MODULEHELPERS; make clean; make; make install; cd -;
  cd $TRAJ; make clean; make; make install; cd -;
  cd $TRAJCONTROLLER; make clean; make; make install; cd -;
  cd $SIMMODELS; make clean; make; cd -;
  cd $SIMULATOR; make clean; make; cd -;
  cd $MODEMAN; make clean; make; make install; cd -;
  cd $MODEMANMODULE; make clean; make; make install; cd -;
  cd $PIDCONTROLLER; make clean; make; make install; cd -;
  cd $TRAJFOLLOWER; make clean; make; make install; cd -;
  cd $RDDFPATHGEN; make clean; make; cd -;
  

else
  echo "You must be in the dgc/local/scripts directory when running this script!"
  echo "If this is the case, press enter to continue, otherwise, quit with Ctrl-C."
  read foo
  cd $DGC; make; make install; cd -;
  cd $SKYNET; make; make install; cd -;
  cd $MODULEHELPERS; make; make install; cd -;
  cd $TRAJ; make; make install; cd -;
  cd $TRAJCONTROLLER; make; make install; cd -;
  cd $SIMMODELS; make; cd -;
  cd $SIMULATOR; make; cd -;
  cd $MODEMAN; make; make install; cd -;
  cd $MODEMANMODULE; make; make install; cd -;
  cd $PIDCONTROLLER; make clean; make; make install; cd -;
  cd $TRAJFOLLOWER; make; make install; cd -;
  cd $RDDFPATHGEN; make; cd -;

fi

echo " "
echo " "
echo "script finished."


exit 0
