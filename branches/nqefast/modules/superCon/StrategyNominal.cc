/* SUPERCON STRATEGY TITLE: Nominal
 * SOURCE FILE (.cc)
 * See strategy header file for detailed documentation
 */

#include"StrategyNominal.hh"
#include "StrategyHelpers.hh"

bool CStrategyNominal::nominal_TransTerm_conditions( bool checkBool, CStrategyInterface *pStrategyInterface, const SCdiagnostic *pdiag, const char* StrategySpecificStr ) {
  
  //NOTE: The ORDER of the transition/termination methods is IMPORTANT! - this is
  //because if any transition/termination method performs its transition/termination
  //then it changes the record of the 'current strategy' being executed, and also
  //leaves() the current strategy, and enters() the new strategy.
  //
  //THE CURRENT-STRATEGY SHOULD BE CHANGED AT MOST *ONCE* BY THIS METHOD
  //(or any method within ONE stepForward() execution) - hence the structure
  //of the code below, *and* the methods are in DECREASING PRIORITY ORDER.

  if( !checkBool ) {
    checkBool = trans_EstopPausedNOTsuperCon( (*pdiag), StrategySpecificStr );
  }
  if( !checkBool ) {
    checkBool = trans_GPSreAcq( (*pdiag), StrategySpecificStr );
  }
  if( !checkBool ) {
    checkBool = trans_PlnFailedNoTraj( (*pdiag), StrategySpecificStr );
  }
  if( !checkBool ) {
    checkBool = trans_UnseenObstacle( (*pStrategyInterface), (*pdiag), StrategySpecificStr );
  }

  return checkBool;
}

bool CStrategyNominal::checkSlowAdvance( bool checkBool, const SCdiagnostic *pdiag, const char* StrategySpecificStr ) {

  if( !checkBool ) {
    checkBool = trans_SlowAdvance( (*pdiag), StrategySpecificStr );
  }

  return checkBool;
}



void CStrategyNominal::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface )
{
  skipStageOps.reset(); //resets to FALSE

  //Determine from the current state of the vehicle whether or not additional action is required in order
  //to put Alice into the nominal operating state so she can proceed

  if( m_pdiag->nominalOperation_InstCondition ) {
    //Jump to the all's-well stage, Alice is already in nominal operation state
    stgItr.setStage( s_nominal::alls_well );
    SuperConLog().log( STR, STRATEGY_STAGE_MSG, "StrategyNominal: In Nominal operating conditions (no changes)" );
    //speedcaps removed only ONCE per entry of alls_well
    m_pStrategyInterface->stage_modifyTrajFSpeedCap( remove_max_speedcap, 0.0 ); //double arg ignored when removing
    m_pStrategyInterface->stage_modifyTrajFSpeedCap( remove_min_speedcap, 0.0 ); //double arg ignored when removing
  }

  currentStageName = nominal_stage_names_asString( nominal_stage_names(stgItr.nextStage()) );

  switch( nominal_stage_names(stgItr.nextStage()) ) {

  case s_nominal::estop_pause:
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = nominal_TransTerm_conditions( skipStageOps, m_pStrategyInterface, m_pdiag, strprintf( "Nominal: %s", currentStageName.c_str() ) );           

      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->superConPaused ) {
	//already paused -> don't resend pause
	skipStageOps = true;
	stgItr.setStage( gear_drive );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Nominal: setup - Already superCon paused, don't re-send command" );
      }
      
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( !skipStageOps ) {
	
	m_pStrategyInterface->stage_superConEstop( sc_interface::estp_pause );	
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Nominal: stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;

  case s_nominal::gear_drive:
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = nominal_TransTerm_conditions( skipStageOps, m_pStrategyInterface, m_pdiag, strprintf( "Nominal: %s", currentStageName.c_str() ) );           
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->adriveGearDrive ) {
	//already in drive -> don't resend command
	skipStageOps = true;
	stgItr.setStage( trajF_forwards );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Nominal: setup - Already in drive, don't re-send command" );
      }

      if( m_pdiag->superConPaused == false ) {
	//NOT superCon paused - need to be to change gear, don't send gear change
	//command until Alice is paused
	SuperConLog().log( STR, WARNING_MSG, "Nominal: superCon e-stop pause not yet in place cannot change gear -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );	
      }

      if( m_pdiag->safeGearChangeConds == false ) {
	//conditions NOT yet safe for Alice to change gear -> don't send gear change command yet

	skipStageOps = true;	
	SuperConLog().log( STR, WARNING_MSG, "Nominal: alice not yet stationary (safeGearChangeConds == false), can't change gear -> will poll");
	checkForPersistLoop( this, persist_loop_waitstationary, stgItr.currentStageCount() );
      }

      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_changeGear( sc_interface::drive_gear );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Nominal: stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();	
      }
    }
    break;

  case s_nominal::trajF_forwards:
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = nominal_TransTerm_conditions( skipStageOps, m_pStrategyInterface, m_pdiag, strprintf( "Nominal: %s", currentStageName.c_str() ) );           
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->adriveGearDrive == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "Nominal - Alice not yet in drive gear can't change TF mode - will poll");
	checkForPersistLoop( this, persist_loop_gearChange, stgItr.currentStageCount() );
      }

      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_changeTFmode( tf_forwards, 0.0 ); //double arg not used in tf_forwards
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Nominal: stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();	
      }
    }
    break;

  case s_nominal::remove_speedcaps:
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = nominal_TransTerm_conditions( skipStageOps, m_pStrategyInterface, m_pdiag, strprintf( "Nominal: %s", currentStageName.c_str() ) );           
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->trajFForwards == false ) {
	SuperConLog().log( STR, WARNING_MSG, "Nominal: TrajFollower not yet in forwards mode - will poll");
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_trajfModeChange, stgItr.currentStageCount() );
      }      
      
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_modifyTrajFSpeedCap( remove_max_speedcap, 0.0 );
	m_pStrategyInterface->stage_modifyTrajFSpeedCap( remove_min_speedcap, 0.0 );	
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Nominal: stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();	
      }
    }
    break;

  case s_nominal::estop_run:
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = nominal_TransTerm_conditions( skipStageOps, m_pStrategyInterface, m_pdiag, strprintf( "Nominal: %s", currentStageName.c_str() ) );           
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */      
      
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_run);	
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Nominal: stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();	
      }
    }
    break;

  case s_nominal::alls_well:
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = nominal_TransTerm_conditions( skipStageOps, m_pStrategyInterface, m_pdiag, strprintf( "Nominal: %s", currentStageName.c_str() ) );           
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkSlowAdvance( skipStageOps, m_pdiag, strprintf( "Nominal: %s", currentStageName.c_str() ) );

      if( m_pdiag->superConRun == false ) {
	//superCon estop NOT in run -> wait, and then timeout

	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "Nominal: superCon e-stop run not yet in place -> will poll");
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );
      }

      /** BAD!! - THIS WILL NEVER BE TRUE**/
      //      if( m_pdiag->nominalOperation == false ) {
      //	
      //	skipStageOps = true;
      //	stgItr.setStage( s_nominal::estop_pause  );	
      //}

      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Nominal: stage - %s - no issues detected", currentStageName.c_str() );	
	//do NOT update stgItr.nextStage counter -> loop in alls_well until
	//a scenario that requires superCon action is identified
      }
    }
    break;

    /* END OF STRATEGY  */

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyNominal::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyNominal::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: Nominal - default stage reached!");
      
      //re-entering the nominal strategy (i.e. this strategy) will reset the stgItr.nextStage() counter, and reset the strategy internal
      //variables - the assumption is that this is the best option to attempt to clear the problem
    }
  }
}

//We are leaving this state, do clean up (NO NEW STATE TRANSFERS)
void CStrategyNominal::leave( CStrategyInterface *m_pStrategyInterface )
{
  skipStageOps.reset(); //resets to FALSE
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Nominal - Exiting" );
}

//We are entering this state, do initialisation (NO NEW STATE TRANSFERS)
void CStrategyNominal::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset();
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Nominal - Entering");
}

