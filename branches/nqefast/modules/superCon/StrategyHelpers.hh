//NOTE: due to the defines, this file might interfer w CDiagnostic and
//should therfor be included last of all include files and only in Strategy files!!!

#ifndef STRATEGY_HELPERS_HH
#define STRATEGY_HELPERS_HH


///////////////////////////////////////////////////////////////////////////////
//Helpers implemented through defines to get extra info on compile time
//These helpers must be called from within the derived CStrategy class

//Set the state, this effect will not be propagated until the next state update takes place
#define setDoubleState(state, val) { doSetDoubleState(&SCstate::state, val); }
#define setIntState(state, val) { doSetIntState(&SCstate::state, val); }

#endif
