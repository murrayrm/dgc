//SUPERCON STRATEGY TITLE: Slow Advance - SOMETIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategySlowAdvance.hh"
#include "StrategyHelpers.hh"

bool CStrategySlowAdvance::checkPlnFailedNoTraj( bool checkBool, const SCdiagnostic *pdiag, const char* StrategySpecificStr ) {
  
  if( !checkBool ) {
    checkBool = trans_PlnFailedNoTraj( (*pdiag), StrategySpecificStr );
  }
  return checkBool;
}

void CStrategySlowAdvance::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {
  
  skipStageOps.reset(); //resets to FALSE
  currentStageName = slowadvance_stage_names_asString( slowadvance_stage_names(stgItr.nextStage()) );

  switch( stgItr.nextStage() ) {
  
  case s_slowadvance::max_speedcap: //send speed-cap to trajF = high accuracy map speed (get better view of obstacle)
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "SlowAdvance: %s", currentStageName.c_str() ) );
      skipStageOps = checkPlnFailedNoTraj( skipStageOps, m_pdiag, strprintf( "SlowAdvance: %s", currentStageName.c_str() ) );     
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_modifyTrajFSpeedCap( add_max_speedcap, HIGH_ACCURACY_MAPS_SPEED_MS );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "SlowAdvance: stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;

  case s_slowadvance::slowadvance_wait: //wait until planner -> !NFP or Alice stops at zero-speed point on Traj
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "SlowAdvance: %s", currentStageName.c_str() ) );
      skipStageOps = checkPlnFailedNoTraj( skipStageOps, m_pdiag, strprintf( "SlowAdvance: %s", currentStageName.c_str() ) );      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->validAliceStop == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "SlowAdvance: Alice not yet stopped in front of obstacle - will poll");
	checkForPersistLoop( this, persist_loop_slowAdvanceApproach, stgItr.currentStageCount() );
      }
      
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	/** The "DAGGART-RIDGE HYDRA" - in situations like daggart ridge, we do NOT wish to transition
	 ** to LturnReverse FIRST, because we are likely to be in Mountain terrain - hence IFF the 
	 ** min-speed cell through which the current plan passes == the OUTSIDE RDDF VALUE will we 
	 ** transition to LturnReverse.  Otherwise we will transition to LoneRanger first **/
	m_pStrategyInterface->stage_modifyTrajFSpeedCap( remove_max_speedcap, 0 );
	
	if( m_pdiag->plnNFP_Terrain && !m_pdiag->plnNFP_SuperC ) {
	  //push-forward first with LoneRanger
	  transitionStrategy( StrategyLoneRanger, "SlowAdvance complete - stationary in front of intraversible obstacle [NOT OUTSIDE RDDF]" );	  
	}
	
	else if( m_pdiag->plnNFP_SuperC ) {
	  //reverse first with LturnReverse
	  transitionStrategy( StrategyLturnReverse, "SlowAdvance complete - stationary in front of intraversible obstacle [OUTSIDE RDDF]" );	  
	}

	else {
	  
	  transitionStrategy( StrategyNominal, "ERROR: SlowAdvance - reached stage with PLN !NFP through terrain or outside of RDDF --> NOMINAL" );	  	  
	  SuperConLog().log( STR, ERROR_MSG, "ERROR: SlowAdvance - reached stage %s with PLN !NFP through terrain or outside of RDDF --> NOMINAL", currentStageName.c_str() );
	}
	
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "SlowAdvance: stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }      
    break;
    
    /** END OF STRATEGY **/
    
  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategySlowAdvance::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategySlowAdvance::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: SlowAdvance - default stage reached!");
    }
  }
}


void CStrategySlowAdvance::leave( CStrategyInterface *m_pStrategyInterface )
{
  skipStageOps.reset(); //resets to FALSE
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "SlowAdvance - Exiting");
}

void CStrategySlowAdvance::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset();
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "SlowAdvance - Entering");
}
