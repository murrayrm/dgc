/**
 * DGCWidget.hh
 * Revision History:
 * 07/08/2005  hbarnor  Created
 * $Id: DGCWidget.hh 8530 2005-07-11 06:26:58Z hbarnor $
 */

#ifndef DGCWIDGET_HH
#define DGCWIDGET_HH

#include <glibmm/ustring.h>
#include <glibmm/dispatcher.h>
#include <iostream>

#define DGCLABEL 1
#define DGCBUTTON 2
#define DGCENTRY 3

using namespace std;
/**
 * Customized widget for our the Tab api. 
 * A widget class that adds a type-field. The type field is used to
 * determine the type of widget.. This is used mostly for error
 * correction. 
 */

class DGCWidget
{

public:
  /**
   * Constructor for the widet.
   * @param type - the type of widget 
   */
  DGCWidget(unsigned int type);
  /**
   * Destructor
   *
   */
  virtual ~DGCWidget();

  /**
   * Returns the type of the widget.
   * @return an int representing the type of widget.
   */
  unsigned int getType()
  {
    return m_type;
  };

  /**
   * Updates the text in the widget.
   * This method  updates the text in the widget. This function is
   * actually implemented in the sub-clases since its specific to the
   * widget. Always returns false for those that do not override it.
   */
  virtual void setText(Glib::ustring& newValue);

  /**
   * To DO 
   */
  virtual string getText();

  /**
   * Method to actually  set the text on the display.
   */
  virtual void displayNewValue();
protected:
  /**
   * The type of this widget.
   * An int representing the type of the widget. Used for error
   * checking mostly.
   */
  const unsigned int m_type;
  /**
   * Dispatcher, so that we can run the update display in a separate
   * thread and still be able to update the display.
   */
  Glib::Dispatcher m_updateDisplay;
  /**
   * String to hold value to be displayed.
   */
  Glib::ustring m_displayValue;
};

#endif
