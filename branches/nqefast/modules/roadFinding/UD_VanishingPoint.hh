//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_VanishingPoint
//----------------------------------------------------------------------------

#ifndef UD_VANISHING_DECS

//----------------------------------------------------------------------------

#define UD_VANISHING_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glut.h>

#include "opencv_utils.hh"

// my stuff 

#include "UD_Error.hh"
#include "UD_DominantOrientations.hh"

#ifdef HAVE_GEFORCE6

// Enabling a higher vote total per candidate with 16-bit floating point 
// blending

#include "glx.h"
#include "glext.h"
#include "glxtokens.h"

#endif

//-------------------------------------------------
// defines
//-------------------------------------------------

// uncomment this line if an Nvidia GeForce6-series
// graphics card with 16-bit floating point blending
// is installed
//#define HAVE_GEFORCE6

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))

// voter to candidate coordinate transformation

#define V2C_X(vx)        (candidate_scale * ((vx) - candidate_lateral_margin))
#define V2C_Y(vy)        (candidate_scale * ((vy) - candidate_top_margin))

// candidate to voter coordinate transformation

#define C2V_X(cx)        (inv_candidate_scale * (candidate_lateral_margin - (cx)))
#define C2V_Y(cy)        (inv_candidate_scale * (candidate_top_margin - (cy)))

#define ONE_OVER_256     0.00390625

#define MAX_ANGLE_DIFF   3.14159

//-------------------------------------------------
// class
//-------------------------------------------------

class UD_VanishingPoint
{
  
 public:

  // variables

  UD_DominantOrientations *dom;                        ///< dominant orientations object containing texture info which votes come from
  
  int show;                                            ///< draw?
    
  // how much to "shrink" borders of dominant orientation image to indicate
  // which voters don't vote because (a) they're too close to the image edge or 
  // (b) they're a priori unlikely to belong to the road
  
  // these numbers (in units of pixels) should all be >= 0

  int w, h;                                            ///< dimensions of source image
  int cw, ch;                                          ///< dimensions of candidate search region (candidate_image below)

  int voter_lateral_margin;                            ///< move sides of source image toward center by this much to get voter region sides
                                                       ///< (always positive number; units are pixels)
  int voter_top_margin;                                ///< move down top of source image by this much to get voter region top
  int voter_bottom_margin;                             ///< move up bottom of source image by this much to get voter region bottom
  float voter_sky_fraction;                            ///< vertical fraction (from top) of image to ignore votes from as assumed sky 
  
  IplImage *voter_ray_x, *voter_ray_y;                 ///< position of voter ray origin in candidate image coordinate system
  float *voter_ray_dx;                                 ///< x component of voter ray direction in candidate coord. system
  float *voter_ray_dy;                                 ///< y component of voter ray direction in candidate coord. system
  float voter_ray_length;                              ///< minimum ray length necessary to leave candidate image regardless of origin
  
  // limits of which candidates voters are voting for.  margin = 0 means use image borders, margin > 0 means shrink, 
  // margin < 0 means grow.  these are specified in scale of source image

  IplImage *candidate_image;                           ///< where the votes get tallied
  
  int candidate_lateral_margin;                        ///< move sides of source image by this much to get candidate region
                                                       ///< (+ = toward center, - = away from center; units are pixels)
  int candidate_top_margin;                            ///< move top of source image by this much to get candidate region top (+ = down)
  int candidate_bottom_margin;                         ///< move bottom of source image by this much to get candidate region bottom (+ = up)
  
  float candidate_scale;                               ///< how much "resolution" to use in candidate region.  2 => 2x2=4 pixels in size 
                                                       ///< of 1 source image pixel, 4 => 4x4=16, and so on (horiz. and vert. directions 
                                                       ///< scale equally)
  float inv_candidate_scale;                           ///< 1 / candidate_scale

  int compute_max_candidate;                           ///< find maximum vote-getter over entire candidate image? (not needed for tracking)  
  CvPoint pmax, pmin;                                  ///< (x, y) locations of maximum, minimum vote-getters, respectively

  int first_compute_call;                              ///< is this the first time we are attempting to hold an "election"?

  // functions

  UD_VanishingPoint(UD_DominantOrientations *, float, float);

  void draw_max();    
  void draw_voter_region();
  void initialize_opengl();

  void compute(IplImage *);                             
  void compute_linear_opengl(IplImage *);   

  void process_command_line_flags(int argc, char **argv);  

  // functions for converting between image coordinates and VP candidate region coordinates
 
  float im2can_x(float imx) { return imx - (float) candidate_lateral_margin; }   
  float im2can_y(float imy) { return imy - (float) candidate_top_margin; }   
  float can2im_x(float canx) { return canx + (float) candidate_lateral_margin; }   
  float can2im_y(float cany) { return cany + (float) candidate_top_margin; }   

#ifdef HAVE_GEFORCE6

  // variables

  Display *ogl_disp;
  GLXDrawable ogl_draw;
  GLXPbuffer ogl_pbuffer;
  GLXContext ogl_context, ogl_oldcontext;

  // functions

  void init_fp_blend();
  void enter_fp_blend();
  void exit_fp_blend();
  void fp_blend_test();

#endif

};

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
