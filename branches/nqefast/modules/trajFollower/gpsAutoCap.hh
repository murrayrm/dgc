#ifndef ASTATE_TEST_HH
#define ASTATE_TEST_HH

#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <fstream>

#include "VehicleState.hh"
#include "sn_msg.hh"
#include "DGCutils"
#include "StateClient.h"
#include "rddf.hh"
#include "GlobalConstants.h"
#include "ggis.h"

//GPS Capture frequency in Hz (frequency at which GPS autocap runs - note that you should check
//that the frequency selected does not lead to a density of waypoints > 2m (this makes TrajFollower
//unhappy
#define GPS_CAPTURE_FREQUENCY 0.5

class gpsAutoCap : public CStateClient {

  ofstream m_outputGPS;

  unsigned long long starttime;

  double timecalc;

public:
  gpsAutoCap(int);
  void Active();


};

#endif
