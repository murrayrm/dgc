#ifndef TRAJF_STATUS_STRUCT_HH
#define TRAJF_STATUS_STRUCT_HH

//FILE DESCRIPTION: This header file defines the trajFstatusStruct data structure sent in the skynet
//message type: SNtrajFstatus, the structure houses internal variables in trajFollower that need to
//be broadcast to the outside world (as other modules need to know their values) - including superCon

//SC --> TRAJF: TrajFollower execution mode commands/position
#define TRAJF_MODE_CMDS_LIST(_) \
  _( tf_forwards, = 0 ) \
  _( tf_reverse, )
DEFINE_ENUM(trajFmode, TRAJF_MODE_CMDS_LIST)

struct trajFstatusStruct {
  
  trajFmode currentMode;
  double currentVref;
  double smallestMAXspeedCap;
  double largestMINspeedCap;
  
};

#endif
