#Gnuplot plot file for creating the following plots:
#{planned paths; actual path}, {y-error against index}, {h-error against index}, {c-error against index},
#{y-error, h-error, c-error against index}

set key on outside below   
set terminal postscript color solid 
set format "%.3g"         
set output "planned_vs_actual.ps"
set title "Planned Vs Actual paths"
set xlabel "Northing, re-centred to zero (m)" 
set ylabel "Easting, re-centred to zero (m)"
#plot "state.dat" [3834100, 3834800] using 2:3 title "actual", "plans.dat" using  1:4 title "planned" with l
#plot [3834600:3834750] [442460:442575] "state.dat" using 2:3 title "actual", "default.traj" using 1:4 title "planned" with l
#plot [3834600:3834750] [442460:442575] "statev2.dat" using 2:3 title "actual", "default.traj" using 1:4 title "planned" with l
plot "state.dat" using 2:3 title "actual", "default.traj" using 1:4 title "planned" with l

set output "y-error.ps"
set title "y-error against point-index"
set xlabel "point index"
set ylabel "y-error, perpendicular distance from the path (m)"
plot "error.dat" using :1 title "y-error" with l
#plot "errorv2.dat" using :1 title "y-error" with l


set output "h-error_RAD.ps"
set title "theta (heading)-error against point-index (radians)"
set xlabel "point index"
set ylabel "theta-error, (desired-actual) (rad)"
plot "error.dat" using :2 title "theta-error" with l
#plot "errorv2.dat" using :2 title "theta-error" with l

set output "h-error_DEG.ps"
set title "theta (heading)-error against point-index (degrees)"
set xlabel "point index"
set ylabel "theta-error, (desired-actual) (degrees)"
plot "error.dat" using :((($2)/(2.0*3.14159))*360.0) title "theta-error" with l
#plot "errorv2.dat" using :((($2)/(2.0*3.14159))*360.0) title "theta-error" with l


set output "combination-error.ps"
set title "combination-error against point-index"
set xlabel "point index"
set ylabel "combination-error = alpha*y-error_tilda + (1-alpha)*beta*theta-error"
plot "error.dat" using :1 title "combination-error" with l
#plot "errorv2.dat" using :1 title "combination-error" with l


set output "speed-error.ps"
set title "Speed-error against point-index"
set xlabel "point index"
set ylabel "Speed (m/s)"
plot "velocity.dat" using :1 title "Planned speed", "velocity.dat" using :2 title "Actual speed" with l
#plot "velocityv2.dat" using :1 title "Planned speed", "velocity.dat" using :2 title "Actual speed" with l


set output "all-errors.ps"
set title "TrajFollower errors against point-index"
set xlabel "point index"
set ylabel "error measurement"
plot "error.dat" using :1 title "y-error", "error.dat" using :2 title "theta-error", "error.dat" using :3 title "c-error" with l

#pause -1