#ifndef __CMAPPLUS_HH__
#define __CMAPPLUS_HH__

#include <iomanip>
#include <fstream>
#include <stdio.h>
#include "CMap.hh"
#include <sys/time.h>
#include <iostream>
#include <sstream>

#define COMMENT_CHAR '%'

using namespace std;


enum {
  CMP_LOGGING_OFF,
  CMP_LOGGING_ON,
  CMP_LOGGING_PAUSED,

  CMP_LOGGING_NUM
};


enum {
  CMP_DISPLAY_OFF,
  CMP_DISPLAY_ON,
  CMP_DISPLAY_PAUSED,

  CMP_DISPLAY_NUM
};


enum {
  CMP_LOG_MSG_HEADER = 0,
  CMP_LOG_MSG_SHIFT = 1,
  CMP_LOG_MSG_CLEAR = 2,
  CMP_LOG_MSG_CHANGE = 3,

  CMP_LOG_MSG_NUM
};


class CMapPlus : public CMap {
public:
  CMapPlus();
  ~CMapPlus();

  int initLayerLog(int layerNum, char* layerFilename);
  int startLayerLog(int layerNum);
  int pauseLayerLog(int layerNum);
  int closeLayerLog(int layerNum);

  int setLayerLabel(int layerNum, const char* label);
  const char* getLayerLabel(int layerNum);

  template <class T>
  int saveLayer(int layerNum, const char* layerFilenamePrefix, bool bSaveAsPGM = false);

  template <class T>
  int loadLayer(int layerNum, const char* layerFilenamePrefix, bool bLoadAsPGM = false);

  template <class T>
  int addLayer(T noDataVal, T outsideMapVal, bool useDeltas = false,
	       const char* label = "None");
  template <class T>
  int addLayer(char* configFilename, bool useDeltas = false);

  template <class T>
  int setDataUTM(int layerNum, double UTMNorthing, double UTMEasting, T cellValue);
  template <class T>
  int setDataWin(int layerNum, int winRow, int winCol, T cellValue);

  int updateVehicleLoc(double UTMNorthing, double UTMEasting);

  int clearMap();
  int clearLayer(int layerNum);

  enum {
    CMP_TYPE_INT,
    CMP_TYPE_DBL,
    
    CMP_TYPE_NUM
  };
  

  template <class T>
  void getMinMax(int layerNum, double* minval, double* maxval, double* nodataval);


private:
  char** layerLogFilename;
  ofstream** layerLogFiles;
  int* layerLogStatus;

  string** layerLabels;

  int saveLayerShift(int layerNum);
  int saveLayerClear(int layerNum);
  template <class T>
  int saveLayerChange(int layerNum, int winRow, int winCol, T newValue);

  int setupLayer(int layerNum, bool useDeltas, const char* label);
};



template <class T>
int CMapPlus::addLayer(T noDataVal, T outsideMapVal, bool useDeltas, const char* label) {
  int layerNum;

  layerNum = CMap::addLayer<T>(noDataVal, outsideMapVal, useDeltas);

  setupLayer(layerNum, useDeltas, label);
  
  return layerNum;
}


template <class T>
int CMapPlus::addLayer(char* configFilename, bool useDeltas) {
  int layerNum;

  layerNum = CMap::addLayer<T>(configFilename, useDeltas);

  string label = "None";
  ifstream configFile;
  string fieldName;
  
  configFile.open(configFilename,ios::in);
  
  if(!configFile) {
    cout << __FILE__ <<"[" << __LINE__ << "]: " << "Unable to open file '"
	 << configFilename << "' - please check that it exists" << endl;
    exit(1);
    }
  
  while(getline(configFile, fieldName, ':')) {
    if(fieldName == "NoDataVal") {
      //Do nothing
    } else if(fieldName == "OutsideMapVal") {
      //Do nothing
    } else if(fieldName == "Label") {
      configFile >> label;
    } else {
      cout << __FILE__ << "[" << __LINE__ << "]: " 
	   << "While parsing config file " << configFilename 
	   << ", unknown parameter '" << fieldName << "'" << endl;
    }
    getline(configFile, fieldName);
  }  

  configFile.close();

  setupLayer(layerNum, useDeltas, label.c_str());

  return layerNum;
}


template <class T>
int CMapPlus::setDataUTM(int layerNum, double UTMNorthing, double UTMEasting, T cellValue) {
  int winRow, winCol;
  UTM2Win(UTMNorthing, UTMEasting, &winRow, &winCol);

  return setDataWin<T>(layerNum, winRow, winCol, cellValue);
}


template <class T>
int CMapPlus::setDataWin(int layerNum, int winRow, int winCol, T cellValue) {
  T& tempRef = CMap::getDataWin<T>(layerNum, winRow, winCol);
  if(tempRef != getLayerOutsideMapVal<T>(layerNum)) {
    tempRef = cellValue;

    saveLayerChange<T>(layerNum, winRow, winCol, cellValue);
  }

  return 0;
}


template <class T>
int CMapPlus::saveLayerChange(int layerNum, int winRow, int winCol, T newValue) {
  if(layerLogStatus[layerNum] == CMP_LOGGING_ON) {
    double timestamp;
    timeval time;
    gettimeofday(&time, NULL);
    timestamp = ((double) time.tv_sec) + (( (double) time.tv_usec) / 1000000.0); 
    *(layerLogFiles[layerNum]) << CMP_LOG_MSG_CHANGE << " " 
			       << layerNum << " " 
			       << fixed << timestamp << " "
			       << winRow << " " 
			       << winCol << " " 
			       << newValue << " "
			       << endl;
  }

  return 0;
}


template <class T>
int CMapPlus::loadLayer(int layerNum, const char* layerFilenamePrefix, bool bLoadAsPGM) {
  char headerFilename[256];
  char dataFilename[256];
  double timestamp, resRows, resCols, newUTMNorthing, newUTMEasting;
  int numRows, numCols;
  long int windowBottomLeftUTMNorthingRowResMultiple;
  long int windowBottomLeftUTMEastingColResMultiple;
	double minval, maxval, nodataval;

  sprintf(headerFilename, "%s.hdr", layerFilenamePrefix);
	if(bLoadAsPGM)
		sprintf(dataFilename, "%s.pgm", layerFilenamePrefix);
	else
		sprintf(dataFilename, "%s.dat", layerFilenamePrefix);

	ifstream headerstream(headerFilename);
  if(headerstream)
	{
		string line;
		getline(headerstream, line);
		getline(headerstream, line);
		headerstream >> timestamp >> numRows >> numCols >> resRows >> resCols
								 >> windowBottomLeftUTMNorthingRowResMultiple >> windowBottomLeftUTMEastingColResMultiple
			     >> minval >> maxval >> nodataval;

		if(numRows != getNumRows() || numCols != getNumCols() ||
			 resRows != getResRows() || resCols != getResCols())
		{
			cerr << "loaded cmap file doesn't match cmap it's being loaded into. whaaaaaaaaaaaaaaa" << endl;
			exit(-1);
		}
  }
	else
	{
		cerr << "CMapPlus::loadLayer(): Couldn't read header file" << endl;
		exit(1);
	}

  
	newUTMNorthing = (windowBottomLeftUTMNorthingRowResMultiple*2 + getNumRows()) * getResRows()/2.0;
	newUTMEasting  = (windowBottomLeftUTMEastingColResMultiple *2 + getNumCols()) * getResCols()/2.0;

	updateVehicleLoc(newUTMNorthing, newUTMEasting);

  ifstream dataFile(dataFilename);

	T cellValue;

	if(dataFile)
	{
		unsigned int pgmMaxval;
		if(bLoadAsPGM)
		{
			string line;
			do
				getline(dataFile,line);
			while(line[0] == '#');
			if(line != "P5")
			{
				cerr << "CMapPlus::loadLayer(): Invalid file format. Want P5 as PGM header" << endl;
				exit(1);
			}

			do
				getline(dataFile,line);
			while(line[0] == '#');
			int pgmWidth, pgmHeight;
			istringstream pgmDimensions(line);
			pgmDimensions >> pgmWidth >> pgmHeight;

			if(pgmWidth != getNumCols())
			{
				cerr << "CMapPlus::loadLayer(): PGM width is " << pgmWidth << ". Expected " << getNumCols() << endl;
				exit(1);
			}
			if(pgmHeight != getNumRows())
			{
				cerr << "CMapPlus::loadLayer(): PGM height is " << pgmHeight << ". Expected " << getNumRows() << endl;
				exit(1);
			}
			do
				getline(dataFile,line);
			while(line[0] == '#');

			istringstream pgmMaxvalStream(line);
			pgmMaxvalStream >> pgmMaxval;
		}

    for(int row=getNumRows()-1; row >= 0; row--)
		{
			for(int col=0; col < getNumCols(); col++)
			{
				if(bLoadAsPGM)
				{
					unsigned char data;
					dataFile.read((char*)&data, 1);
					cellValue = (T) ((double)data)/255.0 * (maxval - minval) + minval;
					if(cellValue != nodataval) {
					  setDataWin<T>(layerNum, row, col, cellValue);
					} else {
					  setDataWin<T>(layerNum, row, col, getLayerNoDataVal<T>(layerNum));
					}
				}
				else
				{
					dataFile >> cellValue; 
					setDataWin<T>(layerNum, row, col, cellValue);
				}
			}
		}
	}
	else
	{
		cerr << "CMapPlus::loadLayer(): Couldn't read data file" << endl;
		exit(1);
	}

  dataFile.close();

  return 0;
}

template <class T>
int CMapPlus::saveLayer(int layerNum, const char* layerFilenamePrefix, bool bSaveAsPGM) {
  char headerFilename[256];
  char dataFilename[256];
  //char datestamp[256];
  double timestamp;
  //time_t date;
  //tm* local;
  timeval time;
	double minval, maxval, nodataval;

  gettimeofday(&time, NULL);
  timestamp = ((double) time.tv_sec) + (( (double) time.tv_usec) / 1000000.0); 

  sprintf(headerFilename, "%s.hdr", layerFilenamePrefix);
  if(bSaveAsPGM)
		sprintf(dataFilename, "%s.pgm", layerFilenamePrefix);
	else
		sprintf(dataFilename, "%s.dat", layerFilenamePrefix);
  
  ofstream dataFile(dataFilename);
	getMinMax<T>(layerNum, &minval, &maxval, &nodataval);

	ofstream headerstream(headerFilename);
  if(headerstream)
	{
		headerstream << COMMENT_CHAR << " This is the header file for a instantaneously saved CMap layer" << endl;
		headerstream << COMMENT_CHAR << " timestamp       numRows numCols resRows resCols bottomLeftNorthingRowMult bottomLeftEastingColMuly minxval maxval nodataval" << endl;
		headerstream << timestamp << ' ' << getNumRows() << ' ' << getNumCols() << ' ' << getResRows() << ' ' << getResCols()
								 << ' ' << getWindowBottomLeftUTMNorthingRowResMultiple() << ' ' << getWindowBottomLeftUTMEastingColResMultiple()
			     << ' ' << minval << ' ' << maxval << ' ' << nodataval << endl;
	}

  if(dataFile)
	{
		if(bSaveAsPGM)
		{
			dataFile << "P5" << endl << 
				getNumCols() << ' ' << getNumRows() << endl << 
				255 << endl;
		}

    dataFile.precision(10);
    for(int row=getNumRows()-1; row >= 0; row--)
		{
      for(int col=0; col < getNumCols(); col++)
			{
				if(bSaveAsPGM)
				{
				  unsigned char data;
				  if(getDataWin<T>(layerNum, row, col) != getLayerNoDataVal<T>(layerNum)) {
				    data = lround(255.0 * (((double)getDataWin<T>(layerNum, row, col)) - minval) / (maxval - minval)) ;
				  } else {
				    data = lround(255.0 * (nodataval - minval) / (maxval - minval));
				  }
					dataFile.write((char*)&data, 1);
				}
				else
				{
					dataFile << getDataWin<T>(layerNum, row, col) << " ";
				}
      }
			if(!bSaveAsPGM)
				dataFile << endl;
    }
  }
  dataFile.close();

  return 0;
}

template <class T>
void CMapPlus::getMinMax(int layerNum, double* minval, double* maxval, double* nodataval)
{
	*minval =  1.0e10;
	*maxval = -1.0e10;

	T* data = &getDataMem<T>(layerNum,0,0);
	for(int i=0; i<_numRows*_numCols; i++)
	{
		T val = data[i];
		if(val != getLayerNoDataVal<T>(layerNum)) {
			*minval = fmin(*minval, val);
			*maxval = fmax(*maxval, val);
		}
	}

	if(getLayerNoDataVal<T>(layerNum) < *minval) {
	  if(*minval == *maxval || getLayerNoDataVal<T>(layerNum) > (*minval - (*maxval-*minval)/9.0)) {
	    *minval = getLayerNoDataVal<T>(layerNum);
	  } else {
	    *minval = *minval - (*maxval-*minval)/9.0;
	  }
	  *nodataval = *minval;
	} else if(getLayerNoDataVal<T>(layerNum) > *maxval) {
	  if(*minval == *maxval || getLayerNoDataVal<T>(layerNum) < (*maxval + (*maxval-*minval)/9.0)) {
	    *maxval = getLayerNoDataVal<T>(layerNum);
	  } else {
	    *maxval = *maxval + (*maxval - *minval)/9.0;
	  }
	  *nodataval = *maxval;
	} else {
	  *nodataval = getLayerNoDataVal<T>(layerNum);
	}

}


#endif //__CMAPPLUS__HH__
