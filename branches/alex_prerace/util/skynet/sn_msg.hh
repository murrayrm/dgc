#ifndef _SN_MSG_HH_
#define _SN_MSG_HH_

#include "sn_types.h"
#include <pthread.h>
#include <sp.h>
#include <string>

using namespace std;

#define MAX_SIZE_HOSTNAME 64
struct SSkynetID
{
  char       host[MAX_SIZE_HOSTNAME];
  modulename name;
  mailbox    unique;
	int        status;
} __attribute__((packed));

class skynet
{
	int               m_key;
	modulename        m_name;
	mailbox*          m_pMailboxes;
	string*           m_pGroupnames;
	int               m_numMailboxes;

	mailbox           m_sendMailbox;
	int               m_highestOpenMailboxIndex;

	pthread_mutex_t   m_mailboxMutex;

	int*              m_pStatus;

	mailbox           spreadConnect(void);
	void              makeGroupName(string* pGroupName, sn_msg type);

public:
	skynet(modulename myname, int key, int* pStatus=NULL);

	~skynet();                  //unregister

	int listen(sn_msg type, modulename somemodule);

	int get_send_sock(sn_msg type);

	/** sn_select(mboxidx) waits for data to be available on that mailbox */
	void sn_select(int mboxidx);

        /** check to see if more messages are available */
        bool is_msg(int mboxidx);
	
	size_t get_msg(int mboxidx, void *mybuf, size_t bufsize, int options = 0);
	size_t get_msg(int mboxidx, void *mybuf, size_t bufsize, int options,
								 pthread_mutex_t** ppMutex, bool bReleaseMutexWhenDone = true, int numMutices = 1);

	size_t send_msg(int type, void* msg, size_t msgsize, int options = 0);
	size_t send_msg(int type, void* msg, size_t msgsize, int options,
									pthread_mutex_t** ppMutex, bool bReleaseMutexWhenDone = true, int numMutices = 1);

	size_t send_msg(int type, const scatter* msgs, int options = 0);
	size_t send_msg(int type, const scatter* msgs, int options,
			pthread_mutex_t** ppMutex, bool bReleaseMutexWhenDone = true, int numMutices = 1);

	void   I_am_here(void);
};
#endif
