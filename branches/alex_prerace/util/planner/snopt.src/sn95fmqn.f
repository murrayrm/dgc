*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*
*     File  sn95fmqn.f.   Full memory BFGS routines.
*
*     s9FMH0   s9FMmp   s9FMsv   s9FMHx
*
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine s9FMH0( Htype, lenH, nnL, nQNmod, H0pre, H )

      implicit           double precision (a-h,o-z)
      integer            Htype
      double precision   H(lenH)

*     ==================================================================
*     s9FMH0 resets the approximate Hessian H to a diagonal matrix.
*     If H is already diagonal it is set to the identity.
*     On entry, the value of Htype is as follows:
*
*       Htype 
*       -----
*        -1      H undefined.
*         0      H is an approx. Hessian of the form defined by  lvlHes.
*         1      H is a diagonal matrix.
*         2      H is an identity matrix.
*
*     19 Jul 1995: First version of s9FMH0 written by PEG.
*     06 Sep 1998: Pre- and post-QP diagonal Hessian scaling added.
*     06 Sep 1998: Current version.
*     ==================================================================
      parameter         (zero = 0.0d+0)
*     ------------------------------------------------------------------

      if (Htype .eq. 0) then
*        ------------------------------------------------------------
*        Zero the off-diagonal elements H.
*        ------------------------------------------------------------
         l     = 1
         incr  = nnL
         nzero = nnL - 1

         do 200, k = 1, nnL-1
            call dload ( nzero, zero, H(l+1), 1 )
            l      = l     + incr
            incr   = incr  - 1
            nzero  = nzero - 1
  200    continue
         Htype = 1

      else
*        ------------------------------------------------------------
*        Set H0 to a multiple of the identity.
*        ------------------------------------------------------------
         l      = 1
         incr   = nnL
         nzero  = nnL - 1

         do 100, k = 1, nnL-1
            H(l)   = H0pre
            call dload ( nzero, zero, H(l+1), 1 )
            l      = l     + incr
            incr   = incr  - 1
            nzero  = nzero - 1
  100    continue
         H(l)  = H0pre
         Htype = 2

      end if

      nQNmod = 0

*     end of s9FMH0
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine s9FMmp( nnL, minrw, iw, leniw )

      implicit           double precision (a-h,o-z)
      integer            iw(leniw)

*     ==================================================================
*     s9FMmp computes the addresses of the quasi-Newton arrays.
*     These are saved and used for subsequent entries.
*
*     This version defines a full-memory BFGS form of H.
*
*     30 Dec 1991: First version of s9FMmp.
*     19 Jul 1997: Current version.
*     ==================================================================

      lenH    = nnL*(nnL + 1)/2
      lH      = minrw
      minrw   = lH    + lenH

      iw(371) = lH
      iw(372) = lenH

*     end of s9FMmp
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine s9FMup( Update, Htype, lenH, nnL, mQNmod, nQNmod, 
     $                   H0pre, H0scal,
     $                   ydx, dxHdx, Hdx, y, H )

      implicit           double precision (a-h,o-z)
      integer            Update, Htype
      double precision   Hdx(nnL), y(nnL), H(lenH)

*     ==================================================================
*     s9FMup does almost everything associated with the full-memory 
*     quasi-Newton update.
*     If defined, the self-scaling BFGS update parameter is saved.
*     It is needed to update the reduced Hessian when there are only
*     linear constraints.
*
*     19 Jul 1995: First version of s9FMup written by PEG.
*     06 Sep 1998: Pre- and post-QP diagonal Hessian scaling added.
*     06 Sep 1998: Current version.
*     ==================================================================
      parameter         (one = 1.0d+0)
*     ------------------------------------------------------------------

      ydxI   = one / ydx
      dxHdxI = one / dxHdx

      if (Update .gt. 1) then
         H0scal = ydx / dxHdx
         call dscal ( nnL, H0scal, Hdx, 1 )
         dxHdx  = dxHdx  * H0scal
         dxHdxI = dxHdxI / H0scal

         l      = 1
         incr   = nnL

         do 210, k = 1, nnL
            H(l)   = H(l)*H0scal
            l      = l    + incr
            incr   = incr - 1
  210    continue
      end if

      l   = 0
      do 230, i = 1, nnL
         c1  = Hdx(i)*dxHdxI
         c2  =   y(i)*  ydxI
         do 220, j = i, nnL
            l      = l + 1
            H(l)   = H(l) - c1*Hdx(j) + c2*y(j)
  220    continue
  230 continue

      if (nQNmod .ge. mQNmod) then

*        User wants to reset H to a diagonal.
*        Htype = 0 means ``Strip the off-diagonals.''

         call s9FMH0( Htype, lenH, nnL, nQNmod, H0pre, H )

         nQNmod = 0
      else
         nQNmod = nQNmod + 1
         Htype  = 0
      end if

*     end of s9FMup
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine s9FMHx( n, lenH, H, x, Hx )

      implicit           double precision (a-h,o-z)
      double precision   H(lenH), Hx(n), x(n)

*     ==================================================================
*     s9FMHx  computes the product Hx, where the symmetric part of H is
*     stored by rows in the one-dimensional array  H.  Note that
*     lenH is used to define the length of H,  and must
*     be at least n*(n + 1)/2. 
*
*     12 Jan 1996: First version of s9FMHx
*     12 Jan 1996: Current version.
*     ==================================================================
      parameter         (zero = 0.0d+0)
*     ------------------------------------------------------------------

      l = 0
      do 110, i = 1, n
         s     = zero
         do 100, j = i, n
            l     = l + 1
            s     = s + H(l)*x(j)
  100    continue
         Hx(i) = s
  110 continue
      
      l = 0
      do 210, j = 1, n-1
         xj  = x(j)
         l   = l + 1
         do 200, i = j+1, n
            l      = l + 1
            Hx(i)  = Hx(i) + H(l)*xj
  200    continue
  210 continue

*     end of s9FMHx
      end

