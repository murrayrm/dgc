*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*     
*     File  double.f 
*
*     s1eps    s1flmx   s1flmn
*
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      double precision   function s1eps ( )
      implicit           double precision (a-h,o-z)
*
*     Compute the machine precision.
*     IEEE Floating point double precision. 
*

      nbase  = 2
      ndigit = 53
      base   = nbase
      u      = base**(- ndigit)
      s1eps  = 2.0d+0*u

*     end of s1flmx
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      double precision   function s1flmx( )
      implicit           double precision (a-h,o-z)
*
*     IEEE Floating point double precision. 
*
      s1flmx = 1.7977d+307

*     end of s1flmx
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      double precision   function s1flmn( )
      implicit           double precision (a-h,o-z)
*
*     IEEE Floating point double precision. 
*
      s1flmn = 2.2251d-308

*     end of s1flmx
      end

