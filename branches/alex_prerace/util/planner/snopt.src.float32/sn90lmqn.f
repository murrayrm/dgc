*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*
*     File  sn90lmqn.f.   Limited-memory BFGS routines.
*
*     s9LMH0    s9LMHmp   s9LMsv   s9LMHx
*
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine s9LMH0( Htype, nnL, mQNmod, nQNmod,
     $                   H0pre,
     $                   H0, gdif, Hxdif, ytdx, dxtHdx )

      implicit           real (a-h,o-z)
      integer            Htype
      real   H0(nnL)
      real   gdif(nnL,mQNmod), Hxdif(nnL,mQNmod)
      real   ytdx(mQNmod), dxtHdx(mQNmod)

*     ==================================================================
*     s9LMH0 resets the approximate Hessian H to a diagonal matrix.
*     If H is already diagonal it is set to the identity.
*     On entry, the value of Htype is as follows:
*
*       Htype 
*       -----
*        -1      H undefined.
*         0      H is an approx. Hessian of the form defined by  lvlHes.
*         1      H is a diagonal matrix.
*         2      H is an identity matrix.
*
*     19 Jul 1995: First version of s9LMH0 written by PEG.
*     06 Sep 1998: Pre- and post-QP diagonal Hessian scaling added.
*     06 Sep 1998: Current version.
*     ==================================================================

      if (Htype .eq. 0) then
*        ------------------------------------------------------------
*        Reset H0 to the diagonal of the current H.
*        ------------------------------------------------------------
         do 110, k = 1,  min(mQNmod, nQNmod)
            do 100, i = 1, nnL
               H0(i) = H0(i) - Hxdif(i,k)*Hxdif(i,k)*dxtHdx(k)
     $                       +  gdif(i,k)* gdif(i,k)*  ytdx(k)
  100       continue
  110    continue
         Htype  = 1

      else
*        ------------------------------------------------------------
*        Set H0 to a multiple of the identity.
*        ------------------------------------------------------------
         call dload ( nnL, H0pre, H0, 1 )
         Htype  = 2

      end if

      nQNmod = 0

*     end of s9LMH0
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine s9LMmp( nnL, minrw, iw, leniw )

      implicit           real (a-h,o-z)
      integer            iw(leniw)

*     ==================================================================
*     s9LMmp computes the addresses of the quasi-Newton arrays.
*     These are saved and used for subsequent entries.
*
*     This version defines a limited-memory BFGS form of H.
*
*     30 Dec 1991: First version of s9LMmp.
*     19 Jul 1997: Current version.
*     ==================================================================

      mQNmod    = iw( 66)

*     ------------------------------------------------------------------
*     Compute the addresses of the limited-memory arrays.
*     These are saved and used for subsequent entries.
*     ------------------------------------------------------------------
      lH0       = minrw
      lgdSav    = lH0    + nnL
      lHdSav    = lgdSav + nnL*mQNmod
      lydx      = lHdSav + nnL*mQNmod
      ldxHdx    = lydx   +     mQNmod
      minrw     = ldxHdx +     mQNmod

      iw(381)   = lH0    
      iw(382)   = lgdSav 
      iw(383)   = lHdSav 
      iw(384)   = lydx   
      iw(385)   = ldxHdx  

*     end of s9LMmp
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine s9LMup( Update, Htype, nnL, mQNmod, nQNmod,
     $                   H0pre, H0scal, 
     $                   ydx, dxHdx, Hdx, y, H0,
     $                   gdif, Hxdif, ytdx, dxtHdx )

      implicit           real (a-h,o-z)
      integer            Update, Htype
      real   Hdx(nnL), y(nnL)
      real   ytdx(mQNmod), dxtHdx(mQNmod)
      real   H0(nnL)
      real   gdif(nnL,mQNmod), Hxdif(nnL,mQNmod)

*     ==================================================================
*     s9LMup does almost everything associated with the limited-memory 
*     quasi-Newton update.
*     If defined, the self-scaling BFGS update parameter is saved.
*     It is needed to update the reduced Hessian when there are only
*     linear constraints.
*
*     19 Jul 1995: First version of s9LMup written by PEG.
*     06 Sep 1998: Pre- and post-QP diagonal Hessian scaling added.
*     06 Sep 1998: Current version.
*     ==================================================================
      parameter         (one = 1.0d+0)
*     ------------------------------------------------------------------
      ydxI   = one / ydx
      dxHdxI = one / dxHdx

      if (Update .gt. 1) then
         H0scal = ydx / dxHdx
         call sscal ( nnL, H0scal, Hdx, 1 )
         dxHdx  = dxHdx  * H0scal
         dxHdxI = dxHdxI / H0scal
         call sscal ( nnL, H0scal, H0, 1 )

         if (nQNmod .gt. 0) then
            do 100, k = 1, nQNmod
               call sscal ( nnL, H0scal, Hxdif(1,k), 1 )
               call sscal ( nnL, H0scal,  gdif(1,k), 1 )
  100       continue
         end if
      end if

      if (nQNmod .ge. mQNmod) then
*        ---------------------------------------------------------------
*        Insufficient space for storing the new (Hdx,y).
*        Reset H0 to be the diagonal of the current H.
*        Discard any updates accumulated so far.
*        ---------------------------------------------------------------
         call s9LMH0( Htype, nnL, mQNmod, nQNmod,
     $                H0pre,
     $                H0, gdif, Hxdif, ytdx, dxtHdx )

*        If H0 was retained, include the latest update.

         if (Htype .ne. 2) then
            do 120, i = 1, nnL
               H0(i) = H0(i) - Hdx(i)*Hdx(i)*dxHdxI + y(i)*y(i)*ydxI
  120       continue
         end if

         nQNmod = 0

      else
*        ---------------------------------------------------------------
*        Space remains. Store Hdx and y.
*        ---------------------------------------------------------------
         nQNmod = nQNmod + 1
         call scopy ( nnL, Hdx, 1, Hxdif(1,nQNmod), 1 )
         call scopy ( nnL, y  , 1,  gdif(1,nQNmod), 1 )
         ytdx  (nQNmod) = ydxI
         dxtHdx(nQNmod) = dxHdxI
         Htype  = 0

      end if

*     end of s9LMup
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine s9LMHx( nnL, x, Hx, mQNmod, nQNmod,
     $                   H0, gdif, Hxdif, ytdx, dxtHdx )

      implicit           real (a-h,o-z)
      real   Hx(nnL), x(nnL)
      real   H0(nnL), ytdx(mQNmod), dxtHdx(mQNmod)
      real   gdif(nnL,mQNmod), Hxdif(nnL,mQNmod)

*     ==================================================================
*     s9LMHx does the work for s8Hx.
*
*     19 Jul 1995: First version of s9LMHx
*     15 Jan 1996: Current version
*     ==================================================================
      call scopy ( nnL,  x, 1, Hx, 1 )
      call ddscl ( nnL, H0, 1, Hx, 1 )

      if (nQNmod .gt. 0) then
         do 100, k = 1, nQNmod
            c1  = - sdot ( nnL, Hxdif(1,k), 1, x, 1 )*dxtHdx(k)
            call saxpy ( nnL, c1, Hxdif(1,k), 1, Hx, 1 )

            c2  =   sdot ( nnL,  gdif(1,k), 1, x, 1 )*  ytdx(k)
            call saxpy ( nnL, c2, gdif (1,k), 1, Hx, 1 )
  100    continue
      end if

*     end of s9LMHx
      end
