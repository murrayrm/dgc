#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

#include <stdlib.h>

#include "sn_msg.hh"
#include "DGCutils"
#include "herman.hh"

int main(int argc, char *argv[])
{
	ifstream logfile;
	istream* pLogstream = &logfile;
	if(argc != 3)
	{
		cerr << "usage: ./analyze tome rddf" << endl;
		return -1;
	}
	else
	{
		if(strcmp(argv[1], "-") == 0)
			pLogstream = &cin;
		else
			logfile.open(argv[1]);
	}
	if(!logfile)
	{
		cerr << "couldn't open tome" << endl;
		return -1;
	}

	herman rddf(argv[2]);
	if(!rddf.isParsed())
	{
		cerr << "couldn't parse rddf" << endl;
		return -1;
	}

	unsigned long long timestamp;
	sn_msg type;
	int size;
	VehicleState state;

	string line;

	while(true)
	{
		pLogstream->read((char*)&timestamp, sizeof(timestamp));
		pLogstream->read((char*)&type, sizeof(type));
		pLogstream->read((char*)&size, sizeof(size));
		if(type < 0 || type >= last_type)
		{
			cerr << "type too large; corrupt data;" << endl;
			return -1;
		}
		if(!(*pLogstream))
		{
			cerr << "reached end" << endl;
			return 0;
		}

		if(type == SNstate)
		{
			if(size != sizeof(VehicleState))
			{
				cerr << "not sizeof(VehicleState); corrupt. exiting" << endl;
				return -1;
			}
			pLogstream->read((char*)&state, sizeof(state));

			rddf.updateState(state);
			cout << rddf.getCurDistFromStart() << ' ' << state.Speed2() << endl;
		}
		else
		{
			pLogstream->seekg(size, ios_base::cur);
		}
	}
}
