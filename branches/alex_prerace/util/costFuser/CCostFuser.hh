#ifndef __CCOSTFUSER_HH__
#define __CCOSTFUSER_HH__

#include <stdlib.h>
#include <math.h>

#include "CMap.hh"
#include "CMapPlus.hh"
#include "MapConstants.h"
#include "CCorridorPainter.hh"
#include "frames/coords.hh"
#include "CElevationFuser.hh"

#define MAX_INPUT_COST_LAYERS 6

#define MIN_SPEED 0.1

struct CCostFuserOptions {
	double maxSpeed;

	int maxCellsToInterpolate;
	int minNumSupportingCellsForInterpolation;

	int paintRoadWithoutElevation;
};


class CCostFuser {
 public:

	CCostFuser(CMapPlus* outputMap, int layerNumFinalCost,
						 CMapPlus* rddfMap, int layerNumRDDF,
						 CMapPlus* changesMapA, CMapPlus* changesMapB,
						 CCostFuserOptions* options,
						 CCorridorPainterOptions* corridorOptions = NULL);
	~CCostFuser();

		enum STATUS {
		OK,
		ERROR
	};

	int addLayerElevCost(CMapPlus* inputMap, int layerNumElevCost, double relWeight,
											 CMapPlus* elevMap, int layerNumElev);
	STATUS addLayerRoad(CMapPlus* inputMap, int layerNumRoad);
	STATUS addLayerSuperCon(CMapPlus* inputMap, int layerNumSuperCon);

	unsigned long long markChangesCost(NEcoord point);
	//STATUS markChangesCost(NEcoord point, bool alreadyLocked);
	unsigned long long markChangesTerrain(int layerIndex, NEcoord point);

	unsigned long long fuseChangesCost(NEcoord newMapCenter);
 private:
	STATUS fuseChangesTerrain();
	STATUS interpolateChanges();

 public:
	STATUS markChangesCostAll();

	STATUS fuseCellTerrain(NEcoord point);
	STATUS fuseCellCost(NEcoord point);
	STATUS interpolateCell(NEcoord point);

	STATUS clearBuffers();

  void paintRoadCell(double* terrainSpeed, double* roadSpeed, double* roadScaling, double* noData);

	CCostFuserOptions* _options;

	int getNumCellsFused();

	int _numElevLayers;
	CMapPlus* _mapPtrsElevCost[MAX_INPUT_COST_LAYERS];
	CMapPlus* _mapPtrsElev[MAX_INPUT_COST_LAYERS];
	int _layerNumsElevCost[MAX_INPUT_COST_LAYERS];
	int _layerNumsElev[MAX_INPUT_COST_LAYERS];
	double _relWeights[MAX_INPUT_COST_LAYERS];

	int _layerNumRDDF;
	CMapPlus* _mapRDDF;

	int _layerNumRoad;
	CMapPlus* _mapRoad;
	CCorridorPainterOptions* _corridorOptions;

	int _layerNumSuperCon;
	CMapPlus* _mapSuperCon;

 	CMapPlus* _mapOutput;
 	int _layerNumCombinedElevCost;
 	int _layerNumOutputCost;

	CMapPlus* _mapChanges[2];
	int _layerNumElevChanges[2];
	int _layerNumCostChanges[2];
	int _layerNumInterpolationChanges[2];

	int _currentProcessingMapIndex;
	int _currentFillingMapIndex;

	pthread_rwlock_t _swapLock;
	pthread_rwlock_t _writeDeltaLock;

	int _numCellsFused;
};

#endif
