/********************
Haomiao Huang
3 x 3 matrix class
Modify for use to generate rotation matrices
********************/

#include "CMatrix.hh"
#include <iostream>
#include <cassert>

using namespace std;

// Constructors
CMatrix::CMatrix()  // create empty matrix
{
  int row, col;
  for (row = 0; row < rows; row++)
    for(col = 0; col < cols; col++)
      data[row][col]=0;
}

CMatrix::CMatrix(const CMatrix &matrix2) // copy constructor
{  
  int row, col;  // initialize counter variables

  for (row= 0; row < rows; row++)
    {
      for (col = 0; col < cols; col++)
	{
	  data[row][col]= matrix2.getelem(row, col); // copy elements into data
	}
    }
}

// Mutators
// sets the rowth colth element the matrix to value
void CMatrix::setelem(int row, int col, double value)  
{
  assert((row >= 0)&&(row < rows)); // check for negative and too large
  assert((col >= 0)&&(col < cols));
  data[row][col]= value; // set value
}

// Accessors
int CMatrix::getrows() const  // returns the number of rows
{
  return rows;
}

int CMatrix::getcols() const  // returns the number of columns
{
  return cols;
}

// returns the rowth, colth element of the matrix
double CMatrix::getelem(int row, int col) const  
{
  assert((row >= 0)&&(row < rows)); // check for negative and too large
  assert((col >= 0)&&(col < cols));
  return data[row][col];
}

// transpose()
// returns the matrix transpose (ijth element becomes jith element
CMatrix CMatrix::transpose() const
{
  CMatrix result;
  int row, col;

  for (row = 0; row < rows; row++)
    {
      for(col = 0; col < cols; col++)
	{
	  result.setelem(col, rows, data[row][col]);
	}
    }

  return result;
}

// operators
// compares with matrix2
bool CMatrix::operator==(const CMatrix &matrix2) const 
{
  int row, col;
  bool equal = true;
  
  // check if matrices are the same size
  if ((matrix2.getrows() == rows)&&(matrix2.getcols() == cols))
    {
      for (row= 0; row < rows; row++)
	{
	  for (col = 0; col < cols; col++)
	    {
	      if (data[row][col]!= matrix2.getelem(row, col))
		equal = false;  // if element is not equal return false
	    }
	}
    }
  else
    equal = false;

  return equal;
}

CMatrix CMatrix::operator+(const CMatrix& matrix2) const
{
  assert((matrix2.getrows() == rows)&&(matrix2.getcols() == cols));
  // only add if matrices are the same size
  CMatrix result; // create a copy of the current matrix
  result = *this;
  result += matrix2;  // add matrix2 to the copy
  return result;
}

CMatrix CMatrix::operator-(const CMatrix &matrix2) const
{
  assert((matrix2.getrows() == rows)&&(matrix2.getcols() == cols));
  // only add if matrices are the same size
  CMatrix result; // create a copy of the current matrix
  result = *this;
  result -= matrix2;  // subtract matrix2 from the copy
  return result;
}

CMatrix& CMatrix::operator+=(const CMatrix &matrix2)
{
  int row, col;

  for (row= 0; row < rows; row++)
    {
      for (col = 0; col < cols; col++)
	{
	  // add elements from matrix2
	  data[row][col]+= matrix2.getelem(row, col);  
	}
    }
  return *this;
}

CMatrix& CMatrix::operator-=(const CMatrix &matrix2)
{
  int row, col;
  
  for (row= 0; row < rows; row++)
    {
      for (col = 0; col < cols; col++)
	{
	  // subtract elements from matrix2
	  data[row][col]-= matrix2.getelem(row, col);
	}
    }

  return *this;
}

CMatrix CMatrix::operator*(const CMatrix &matrix2) const
{
  /* CMatrix multiplication:
  multiply ith row by jth column and sum results
  n x m * i x n = n x i */

  assert(cols == matrix2.getrows());
  // only multiply when you can

  CMatrix result;  // create a copy of the matrix
  result = *this;
  result *= matrix2;  // multiply by matrix 2 and return the result
  return result;
}

CMatrix& CMatrix::operator*=(const CMatrix &matrix2)
{
  /* CMatrix multiplication:
  multiply ith row by jth column and sum results
  n x m * i x n = n x i */
  
  int r2 = matrix2.getrows();
  int c2 = matrix2.getcols();
  int row, col, col2; 
  double entry=0;

  CMatrix result;
  
  // run through each column of matrix2
  for(col2 = 0; col2 < c2; col2++)
    {
      // multiply by each row of the original
      for (row = 0; row < rows; row++)
	{
	  // within each i x j multiply each entry
	  for(col = 0; col < cols; col++)
	    {
	      entry += data[row][col] * matrix2.getelem(col, col2);
	    }
	  result.setelem(row, col2, entry); // set value
	  entry = 0;
	}
    }

  *this = result;
  
  return *this;
}

bool CMatrix::operator!=(const CMatrix &matrix2) 
{
  return !(*this == matrix2); // check for not equal
}

CMatrix& CMatrix::operator=(const CMatrix &matrix2)
{
  int row, col;
  if (*this == matrix2)
    return *this;

  for (row= 0; row < rows; row++)
    {
      for (col = 0; col < cols; col++)
	{
	  data[row][col]= matrix2.getelem(row, col); // copy elements into data
	}
    }

  return *this;
}

// Destructors
CMatrix::~CMatrix()
{
}
