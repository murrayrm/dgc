#include "demo.h"

int main(){
    const char* msg = "Hello, world"; 
    
    int sock_fd;
    #if 0
    struct sockaddr_in my_addr;
    int sin_size = sizeof(struct sockaddr_in);
    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(PORT);
    my_addr.sin_addr.s_addr = INADDR_ANY;
    bzero(&my_addr.sin_zero, 8);    /*pad with 0 */
    #endif

    struct sockaddr_in multi_addr ;
    multi_addr.sin_family = AF_INET ;
    multi_addr.sin_port = htons(PORT);
    inet_aton(MADDR, &(multi_addr.sin_addr));
    bzero(&multi_addr.sin_zero, 8);


    sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
      if (sock_fd == -1) {
        perror("socket");
        exit(1);
      }
    
   #if 0 
    int bind_succeed =
        bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr));
    if (bind_succeed == -1) {
        perror("bind");
        exit(1);
    }
    #endif

    int connect_suceed =
        connect(sock_fd, (struct sockaddr*)&multi_addr, sizeof(struct sockaddr));
    if ( connect_suceed == -1 ) {
        perror("connect");
        exit(1);
    }
    while (1){
        int char_sent = send(sock_fd, msg, strlen(msg)+1, 0);
        sleep(1);
    }
}
