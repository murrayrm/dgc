//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_Linux_Time.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void UD_timestamp(long int *seconds, long int *useconds)
{
  static struct timeval tv;

  gettimeofday(&tv, NULL);
  *seconds = tv.tv_sec;
  *useconds = tv.tv_usec;
}

//----------------------------------------------------------------------------

double UD_timestamp()
{
  static struct timeval tv;

  gettimeofday(&tv, NULL);

  return (double) tv.tv_sec + 0.000001 * (double) tv.tv_usec;
}

//----------------------------------------------------------------------------

// current time, written in the following format: "Mar_14_2002_Thu_07_28_00PM"

char *UD_datetime_string()
{
  time_t ltime;
  struct tm *today;
  char *s;
  
  s = (char *) calloc(128, sizeof(char));

  time(&ltime);
  today = localtime(&ltime);

  strftime(s, 128, "%b_%d_%Y_%a_%I_%M_%S_%p", today);

  return s;
}

//----------------------------------------------------------------------------

// returns time in seconds

int UD_itime()
{
  return time(NULL);
}

//----------------------------------------------------------------------------

// returns time in seconds.milliseconds as a double 

double UD_time()
{
  long current;

#if defined(_WIN32)
  static struct timeb tb;
  ftime(&tb);
  current = tb.time * 1000 + tb.millitm;
#else
//   static struct timeval tv;
//   gettimeofday(&tv, NULL);
//   return  (double) tv.tv_sec + (double) tv.tv_usec / 1000;
  static struct tms tb;
  current = times(&tb);
#endif
  
  return (double) current;
}

//----------------------------------------------------------------------------

// number of seconds since last call

static double begin = 0;
static int calls = 0;

double UD_difftime()
{

  /*#if defined(_WIN32)
  static long begin = 0;
  static long finish, difference;

  static struct timeb tb;
  ftime(&tb);
  finish = tb.time * 1000 + tb.millitm;

  difference = finish - begin;
  begin = finish;
  
  return (double) difference / 1000.0;

#else
  */
  /*
  static long begin = 0;
  static long finish, difference;

  static struct timeval tv;
  gettimeofday(&tv, NULL);
  finish = (double) tv.tv_sec + (double) tv.tv_usec / 1000;

  static long begin = 0;
  */
  double finish, difference;

  struct timeval tv;
  gettimeofday(&tv, NULL);
  finish = (double) tv.tv_sec + (double) tv.tv_usec / 1000;
  //  printf("%lf %lf\n", finish, begin);

//   static struct tms tb;
//   finish = times(&tb);

  difference = finish - begin;
  //  printf("%li\n", difference);
  begin = finish;
  
  //  return (double) difference / 100.0;
  return difference / 100.0;

//#endif
  
}

//----------------------------------------------------------------------------

// in seconds

void UD_sleep(double time)
{
  usleep(1000000.0 * time);
  return;

  double start_time, diff_time;

  diff_time = 0.0;
  start_time = UD_difftime();
  //  printf("%lf\n", start_time);

  calls = 0;

  do {
    diff_time += UD_difftime();
    //    calls++;
    //    printf("s %lf/%lf %i\n", diff_time,time, calls);
    //    exit(1);
  } while (diff_time < time);
}

//----------------------------------------------------------------------------

// double mytime(struct timeval *tp)
// {
//   gettimeofday(tp, NULL); 
//   return ((double) tp->tv_sec + ((double) tp->tv_usec) / 1000000.0);
// }

//----------------------------------------------------------------------------

// sleep for t seconds (fractions OK)

// void mysleep(double t)
// {
//   struct timeval tp;
//   double tottime = 0.0;
//   double lasttime;
// 
//   lasttime = mytime(&tp);
//   do {
//     tottime += difftime(&tp, &lasttime);
//   } while (tottime < t);
// }

//----------------------------------------------------------------------------

// sleep for t microseconds (millionths of a second)

// void usleep_CRTime(unsigned long t)
// {
//   usleep(t);
// }

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------         
