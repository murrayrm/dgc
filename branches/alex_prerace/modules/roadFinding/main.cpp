//----------------------------------------------------------------------------
//  Road follower module
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//----------------------------------------------------------------------------

#include "decs.h"

#include<sys/stat.h>
#include<sys/types.h>


//----------------------------------------------------------------------------

UD_CameraCalibration *camcal          = NULL;
UD_ImageSource *imsrc                 = NULL;
UD_LadarCalibration *ladcal           = NULL;
UD_LadarSourceArray *ladarray         = NULL;
UD_DominantOrientations *dom          = NULL;
UD_VanishingPoint *vp                 = NULL;
UD_RoadFollower *rf                   = NULL;
UD_OnOff *oo                          = NULL;

UD_Reporter *reporter                 = NULL;
UD_Logger *live_logger                = NULL;

#ifdef SKYNET

//UD_LoggerTimber *timber_logger        = NULL;

#include "RFCommunications.hh"
#include "sparrowhawk.hh"
#include "vddroad.h"
#include "videoRecorder.hh"

#include <iostream>
#include <iomanip>

using namespace std;

RFCommunications *RFComm;

CVideoRecorder *pVideoRecorder;

struct ParseOptions {
  int snkey;
  bool playback;
  bool novideo;
  bool nodisp;
  bool logging;
  bool waitstate;
} options;

#endif

//----------------------------------------------------------------------------

int do_computations                   = TRUE;    // run algorithm? (vs. just displaying input)
int do_aerial                         = FALSE;   // show aerial display?
int do_alice                          = FALSE;   // are we connected to the vehicle?
int input_paused                      = FALSE;   // are we "frozen" on the current image?
int main_window_id;

//----------------------------------------------------------------------------

void display()
{
  // raw source

  if (!do_computations)
    glDrawPixels(imsrc->im->width, imsrc->im->height, GL_RGB, GL_UNSIGNED_BYTE, imsrc->im->imageData);

  // current image, tracker state, etc.

  else 
    rf->draw(imsrc);

  // ladar projection
  
//   if (ladsrc) 
//     ladsrc->draw(camcal);

  glutSwapBuffers();
}

//----------------------------------------------------------------------------

void keyboard(unsigned char key, int x, int y)
{
  // pause/unpause current image frame (just a freeze frame if live input--does not buffer)

  if (key == 'p')
    input_paused = !input_paused;
}

//----------------------------------------------------------------------------

void mouse(int button, int state, int x, int y)
{
  if (do_computations) {

    // cycle between raw image view, dominant orientation view, and vote function view
    
    if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) 
      rf->cycle_draw_mode_image();
    
    // cycle between overlaying nothing, tracker particles, or support rays
    
    else if (button == GLUT_RIGHT_BUTTON && state == GLUT_UP) 
      rf->cycle_draw_mode_overlay();
  }

  // save whatever the display is currently showing to file

  if (button == GLUT_MIDDLE_BUTTON && state == GLUT_UP) {
    glReadPixels(0, 0, imsrc->im->width, imsrc->im->height, GL_RGB, GL_UNSIGNED_BYTE, imsrc->im->imageData);
    sprintf(imsrc->frame_filename, "../road_data/saved_%05i.png", imsrc->relative_frame);
    cvConvertImage(imsrc->im, imsrc->im, CV_CVTIMG_SWAP_RB | CV_CVTIMG_FLIP);  // RGB->BGR	
    cvSaveImage(imsrc->frame_filename, imsrc->im);
  }
}

//----------------------------------------------------------------------------

void process_image()
{
  // calculate dominant orientations

  dom->compute(imsrc->im);
  
  // compute vote function 

  vp->compute(dom->gabor_max_response_index);
  
  // track vanishing point, centerline, make on/off road decision, etc.

  rf->run();
  
  // talk to vehicle controller
  
  if (do_alice) {
    //    rf->send_output_to_controller(camcal);
#ifdef SKYNET
    // printf("%f degs., %f m\n", RAD2DEG(rf->direction_error), rf->laser_offset);
    RFComm->RFSendRoad(rf->lookahead_z, rf->lookahead_x, rf->lookahead_radius, rf->num_lookahead_pts);
 #endif
  }
}

//----------------------------------------------------------------------------

void compute_display_loop()
{
  if (imsrc) {

    glutSetWindow(main_window_id);
    
    // start timing (of _iteration_, of which there may be multiple per image frame)

    reporter->start_timer(imsrc);

    // perform image processing for current image (frame 0 already "captured" by constructor)
    
    if (do_computations)
      process_image();

    // draw current image and algorithm output in camera window

    glutPostRedisplay();
  }

  // draw in overhead and aerial view windows 

  if (!ladarray->no_ladar()) {
    glutSetWindow(UD_Overhead::overhead->window_id);
    glutPostRedisplay();
  }

  if (do_aerial) {
    glutSetWindow(UD_Aerial::aerial->window_id);
    glutPostRedisplay();
  }

  // ready for new sensor data?

  if (!input_paused && (!do_computations || (rf && rf->iterate()))) {

    // handle logging

    if (live_logger)
      live_logger->write(imsrc, rf);                                      

#ifdef SKYNET
    //    if(timber_logger)
    //   timber_logger->write(imsrc, rf);                                      
#endif //SKYNET

    // capture 

    if (imsrc)
      imsrc->capture();    
//       else
//         UD_sleep(0.1);  // slow things down

    if (!ladarray->no_ladar()) 
      ladarray->capture(imsrc);     // syncs to image (ok with no imsrc)

    // something different should be done ifdef SKYNET

    if (do_aerial) {
#ifdef SKYNET
      UD_Aerial::aerial->statesrc->read();
#else
      if (imsrc && ladarray->no_ladar())
	UD_Aerial::aerial->statesrc->sync(imsrc->timestamp);  // sync to image if no ladar did it already
      else if (!imsrc)  
	UD_Aerial::aerial->statesrc->read();   // aerial must read state source itself
#endif
    }
  }

  // end timing

  if (imsrc)
    reporter->end_timer(imsrc);
}

//----------------------------------------------------------------------------

/// register callbacks and create one display window
/// imwidth = 0 or imheight = 0 indicates that there is no image source

void initialize_display(int imwidth, int imheight, int argc, char **argv)
{
  int x, y;
  
  x = 100;
  y = 100;

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  if (!ladarray->no_ladar()) {
    UD_Overhead::overhead->ladarray = ladarray;
    UD_Overhead::overhead->initialize_display(x + imwidth + 10, y);
  }
  if (do_aerial) 
    UD_Aerial::aerial->initialize_display(x + imwidth + UD_Overhead::overhead->width + 20, y);

  glutIdleFunc(compute_display_loop);

  if (!imwidth || !imheight)
    return;

  glutInitWindowSize(imwidth, imheight); 
  glutInitWindowPosition(x, y);
  main_window_id = glutCreateWindow("road");

  //  glutIdleFunc(compute_display_loop);
  
  glutDisplayFunc(display); 
  glutMouseFunc(mouse); 
  glutKeyboardFunc(keyboard);

  if (!do_computations) {
    glPixelZoom(1, -1);
    glRasterPos2i(-1, 1);
  }

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, imwidth-1, 0, imheight-1);
}


//----------------------------------------------------------------------------

void init_per_run(int argc, char **argv)
{
  if(argc>=2 && (strcasecmp(argv[1], "--help")==0 || strcasecmp(argv[1], "-help")==0)) {
    //Print help
    printf("USAGE: %s [options]\n", argv[0]);
    printf("General flags\n");
    printf("  --snkey <skynet_key>                      Sets the skynet key\n");
    printf("  --skynetkey <skynet_key>                  Sets the skynet key\n");
    printf("  --playback                                Starts in timber playback mode\n");
    printf("  --novideo                                 Starts without the video interface\n");
    printf("  --nodisp                                  Starts without the sparrow interface\n");
    printf("  --nowait                                  Starts without waiting for state data\n");
    printf("  --playback                                Starts in timber playback mode\n");
    printf("  -image_list <file> <start> <end> <width>  Load list of images from file\n");
    printf("                                            starting with image start and\n");
    printf("                                            running till image end.\n");
    printf("  -image_avi <file> <start> <end> <width>   Load video from file starting with\n");
    printf("                                            frame start and running till frame\n");
    printf("                                            <end>.\n");
    printf("  -image_live <cam_nr> <mode> <nframes> <w> Run from camera feed where mode is\n");
    printf("                                            one of:\n");
    printf("                                              MODE_640x480_MONO\n");
    printf("                                              MODE_640x480_YUV422\n");
    printf("                                              MODE_320x240_YUV422\n");
    printf("  -ladar_scans <file> <stamp> <diff>        Loads logged ladar scans from <file>\n");
    printf("                                            and loads a time stamp <stamp> file\n");
    printf("                                            for the images, finally sets maximal\n");
    printf("                                            allowed time discrepancy <diff> in\n");
    printf("                                            seconds between ladar and image\n");
    printf("  -ladar_skynet <diff>                      Runs ladars from alice, sets the\n");
    printf("                                            maximal allowed time discrepancy\n");
    printf("                                            <diff> in seconds between ladar\n");
    printf("                                            and image\n");
    printf("  -nocompute                                \n");
    printf("  -log <file> <interval>                    Start image loggin to <file>,\n");
    printf("                                            logging every log_interval image\n");
    printf("  -noalice                                  Run without communication with alice\n");
    printf("  -sw_cabladar                              Use calibration for cab mounted\n");
    printf("                                            ladar from Stoddard Wells\n");
    printf("  -sw_bumperladar                           Use calibration for bumper mounted\n");
    printf("                                            ladar from Stoddard Wells\n");
    printf("  -sw_rightcam                              Use calibration for right-hand\n");
    printf("                                            stereo camera from Stoddard Wells\n");
    printf("  -sw_leftcam                               Use calibration for left-hand\n");
    printf("                                            stereo camera from Stoddard Wells\n");
    /*
      printf("Printing and logging flags\n");
      printf("  -time <interval>                          Do timing\n");
      printf("  -printnum                                 Print image number\n");
      printf("Vanishing point flags\n");
      printf("  -sky <fraction>                           Set the fraction of the image that\n");
      printf("                                            should be considered sky\n");
      printf("  -computemax                               \n");
      printf("OnOff flags (?)\n");
      printf("  -klthresh <threshold>                     \n");
      printf("  -klthreshold <threshold>                  Same as above\n");
      printf("  -klmajority                               \n");
      printf("  -printonoff                               \n");
      printf("  -noonoff                                  \n");
      printf("RoadFollowing flags\n");
      printf("  -centerline                               Show centerline\n");
      printf("  -showsupp                                Show support\n");
      printf("  -thresh <threshold>                       Support angle difference threshold\n");
      printf("  -alpha <value>                            Support alpha\n");
      printf("Tracker flags\n");
      printf("  -deltax <val>                             \n");
      printf("  -deltay <val>                             \n");
      printf("  -doreset                                  \n");
      printf("  -iterations <val>                         Set number of iterations per image\n");
    */

    return;
  }

  //-------------------------------------------------
  // reporting (command-line flags can modify)
  //-------------------------------------------------

  reporter = new UD_Reporter();
  reporter->process_command_line_flags(argc, argv);   

  //-------------------------------------------------
  // ladar array stub
  //-------------------------------------------------

  ladarray = new UD_LadarSourceArray();

  //-------------------------------------------------
  // command-line flags (imsrc initialized after this)
  //-------------------------------------------------

  process_general_command_line_flags(argc, argv);

  //-------------------------------------------------
  // alice
  //-------------------------------------------------

#ifdef SKYNET
  printf("skynet!!\n");
  if (do_alice) {
    //Set up standard configuration

    do_computations=TRUE;

    //Make sure we have a skynet key
    if(options.snkey==-1) {
      char *RFSkyNetKey = getenv("SKYNET_KEY");
      if (RFSkyNetKey == NULL) {
	printf("SKYNET_KEY environment variable is not set!!\n");
	exit(1);
      } else {
	options.snkey = atoi(RFSkyNetKey);
      }
    }
    printf("skynet key = %i\n", options.snkey);
    RFComm = new RFCommunications(options.snkey, options.waitstate);
    printf("done constructing RFComm\n");

    
    if(options.playback) {  //Playback mode
      //center camera
      if(!camcal) {
	printf("Creating standard camera calibration\n");//
	//camcal = new UD_CameraCalibration(640, 480, DEG2RAD(64.8), 380.94, 379.83, 344.58, 243.72, 0.0, -2.089, -0.464, 0.0, DEG2RAD(-2));
	camcal = new UD_CameraCalibration(640, 480, DEG2RAD(94.3), 380.94, 379.83, 344.58, 243.72, 0.0, -2.089, -0.464, 0.0, DEG2RAD(-2));
      }
      
      if(!ladcal) {
	printf("Creating standard ladar calibration\n");
	ladcal = new UD_LadarCalibration(500, 50.0, -0.5, 0.0, -0.648, 0.965, 0.0);
	ladarray->add_ladar(new UD_LadarSource_Skynet(ladcal, 0.5));
      }

      printf("Creating timber source\n");
      imsrc = new UD_ImageSource_Timber(camcal, options.snkey);
      
    } else {   //Not in playback mode
      //Set standard settings

      //center camera
      if(!camcal) {   //No camera calibration, use standard
	printf("Creating standard camera calibration\n");
	//camcal = new UD_CameraCalibration(640, 480, DEG2RAD(32.4), 380.94, 379.83, 344.58, 243.72, 0.0, -2.089, -0.464, 0.0, DEG2RAD(-2));
	camcal = new UD_CameraCalibration(640, 480, DEG2RAD(94.3), 380.94, 379.83, 344.58, 243.72, 0.0, -2.089, -0.464, 0.0, DEG2RAD(-2));
      }

      if(!ladcal) {   //No ladar set, use standard
	printf("Creating standard ladar calibration\n");
	ladcal = new UD_LadarCalibration(500, 50.0, -0.5, 0.0, -0.648, 0.965, 0.0);
	ladarray->add_ladar(new UD_LadarSource_Skynet(ladcal, 0.5));
      }

      if(!imsrc) {   //No image source set, use the video recorder as default
	if(options.novideo) {
	  printf("Creating old image source\n");
	  imsrc = new UD_ImageSource_Live(camcal, 0, MODE_640x480_MONO, -1, 80);
	  //imsrc = new UD_ImageSource_Live(camcal, 0, MODE_640x480_YUV422, -1, 80);
	} else {
	  printf("Creating video recorder image source\n");
	  pVideoRecorder = new CVideoRecorder(options.snkey, true);
	  pVideoRecorder->setCamera(0);
	  imsrc = new UD_ImageSource_VideoRecorder(camcal, *pVideoRecorder, -1, 80);
	}
      }

      if(options.logging) {
	//Start logging      
	printf("Creating loggers\n");
	mkdir("/tmp/logs/roadFinding", 0755);
	live_logger = new UD_Logger("/tmp/logs/roadFinding/", 1, ladarray);
	
	//	timber_logger = new UD_LoggerTimber(options.snkey);
      }
    }
  }
 #endif

  //-------------------------------------------------
  // random number generation
  //-------------------------------------------------

  initialize_UD_Random();

#ifdef SKYNET
  SparrowHawk().rebind("YawRad", &camcal->yaw_angle);
  static double dummy1;
  SparrowHawk().rebind("YawDeg", &dummy1);
  SparrowHawk().set_setter("YawDeg", camcal, &UD_CameraCalibration::set_yaw_deg);
  SparrowHawk().set_getter("YawDeg", camcal, &UD_CameraCalibration::get_yaw_deg);
#endif

  //-------------------------------------------------
  // short circuit rest of init sequence?
  //-------------------------------------------------

  if (!imsrc || !do_computations) {
    if (!imsrc)
      initialize_display(0, 0, argc, argv);
    else
      initialize_display(imsrc->im->width, imsrc->im->height, argc, argv);

#ifdef SKYNET
    if(!options.nodisp)
      SparrowHawk().run();
#endif

    glutMainLoop();
    return;
  }

  //-------------------------------------------------
  // dominant orientation estimation
  //-------------------------------------------------

  dom = new UD_DominantOrientations(imsrc->im->width, imsrc->im->height, 36, 0.0, 180.0, 4.0);
  dom->process_command_line_flags(argc, argv);

  //-------------------------------------------------
  // vanishing point finding
  //-------------------------------------------------

  vp = new UD_VanishingPoint(dom, 0.2, 0.0);
  vp->process_command_line_flags(argc, argv);
  if (!ladarray->no_ladar()) 
    ladarray->set_win_deltas((float) -vp->candidate_lateral_margin, (float) -vp->candidate_top_margin); 
 
  //-------------------------------------------------
  // on vs. off road confidence
  //-------------------------------------------------

  if (imsrc->im->height == 60)
    //    oo = new UD_OnOff(imsrc, -0.05, 200, 0.5);
    oo = new UD_OnOff(imsrc, -0.1, 100, 1.0, 0.5);
  else
    oo = new UD_OnOff(imsrc, 0.1, 300, 1.0, 0.667);
  oo->process_command_line_flags(argc, argv);

  //-------------------------------------------------
  // tracking vanishing point
  //-------------------------------------------------

  rf = new UD_RoadFollower(vp, oo, ladarray);  
  rf->process_command_line_flags(argc, argv);
  rf->cal = camcal;

  //-------------------------------------------------
  // overhead and aerial views (constructed in 
  // respective source files)
  //-------------------------------------------------

  UD_Overhead::overhead->rf = rf;

  //-------------------------------------------------
  // start OpenGL GLUT
  //-------------------------------------------------

  initialize_display(vp->cw, vp->ch, argc, argv);

#ifdef SKYNET
  if(!options.nodisp)
    SparrowHawk().run();
#endif

  glutMainLoop();
}

//----------------------------------------------------------------------------

void process_general_command_line_flags(int argc, char **argv)
{
  int i, mode;
  UD_Aerial *tempaerial;

  //default is do alice
  do_alice = TRUE;
  bool force_alice=false;

  if (argc == 1) {
    printf("running with race parameters\n");
#ifndef SKYNET
    printf("skynet must be defined\n");
    exit(1);
#endif

    //If nothing is set, default settings will be loaded
    return;
  }

  for (i = 1; i < argc; i++) {

    // path to image file listing, start frame (by place in file list), end frame, target image width (180 recommended)

    if (!strcmp(argv[i],                             "-image_list")) {
      imsrc = new UD_ImageSource_Image(camcal, argv[i + 1], atoi(argv[i + 2]), atoi(argv[i + 3]), atoi(argv[i + 4]));
      do_alice = FALSE;

#ifdef SKYNET
      options.logging=false;
#endif

    // path to AVI file, start frame, end frame (-1 = last frame of movie), target image width (180 recommended)

    } else if (!strcmp(argv[i],                        "-image_avi")) {
      imsrc = new UD_ImageSource_AVI(camcal, argv[i + 1], atoi(argv[i + 2]), atoi(argv[i + 3]), atoi(argv[i + 4]));
      do_alice = FALSE;

#ifdef SKYNET
      options.logging=false;
#endif

      // camera number, mode, end frame (-1 = NONE), target image width (180 recommended)

    } else if (!strcmp(argv[i],                        "-image_live")) {
      if (!strcmp(argv[i + 2],      "MODE_640x480_MONO"))
	mode = MODE_640x480_MONO;
      else if (!strcmp(argv[i + 2], "MODE_640x480_YUV422"))
	mode = MODE_640x480_YUV422;
      else if (!strcmp(argv[i + 2], "MODE_320x240_YUV422"))
	mode = MODE_320x240_YUV422;
      else {
	printf("unsupported firewire camera mode\n");
	exit(1);
      }

      imsrc = new UD_ImageSource_Live(camcal, atoi(argv[i + 1]), mode, atoi(argv[i + 3]), atoi(argv[i + 4]));
      do_alice = FALSE;
    }

    // path to scans, path to image timestamp logs, maximum timestamp difference

    else if (!strcmp(argv[i],                        "-ladar_scans")) {  // use calibration for roof ladar at Stoddard Wells

      UD_LadarSource_Scans *ladsrc = new UD_LadarSource_Scans(argv[i + 1], ladcal, atof(argv[i + 2]));
      ladarray->add_ladar(ladsrc);
      do_alice = FALSE;
    }

    // insert timestamp info from Aug. 27 .log files into .lad files and
    // add 0 pitch entry (this is format all .lad files will be in
    // from Sept. 2 on

    else if (!strcmp(argv[i],                        "-fixlad")) {

      char *newname = (char *) calloc(1012, sizeof(char));
      char *sysstring = (char *) calloc(1012, sizeof(char));
      strcpy(newname, argv[i+1]);
      sprintf((char *) &newname[strlen(newname)-3], "old\0");

      FILE *testfp = fopen(newname, "r");
      if (testfp) {
	printf("already fixed\n");
	exit(1);
      }

      sprintf(sysstring, "mv %s %s", argv[i+1], newname);

//       printf("%s\n", sysstring);
//       exit(1);

      system(sysstring);

      FILE *oldfp, *logfp, *newfp;

      oldfp = fopen(newname, "r");
      sprintf((char *) &newname[strlen(newname)-3], "log\0");
      logfp = fopen(newname, "r");
      newfp = fopen(argv[i+1], "w");

      int result, tempint, num_rays, framenum, last_framenum, tempint1, tempint2, tempint3;
      float tempfloat, angle, range, tempfloat1, tempfloat2, tempfloat3, tempfloat4, tempfloat5;
      double timestamp, northing, easting, heading;
      long int secs, usecs;

      do {

	last_framenum = framenum;
	result = fscanf(oldfp, "%i %lf %lf %lf %i", &framenum, &northing, &easting, &heading, &num_rays);
	if (framenum > 1 && last_framenum == framenum)
	  exit(1);
	result = fscanf(logfp, "%u, %li, %lu, %f, %f, %f, %f, %f, %i, %i\n", &tempint1, &secs, &usecs, &tempfloat1, &tempfloat2, &tempfloat3, &tempfloat4, &tempfloat5, &tempint2, &tempint3);
	//	printf("%i, %li, %li, %f, %f, %f, %f, %f, %i, %i\n", tempint1, secs, usecs, tempfloat1, tempfloat2, tempfloat3, tempfloat4, tempfloat5, tempint2, tempint3);
	timestamp = (double) secs + 0.000001 * (double) usecs;
	printf("%i\n", framenum);
	fprintf(newfp, "%i %lf %lf %lf %lf 0.0 %i ", framenum, timestamp, northing, easting, heading, num_rays);
	for (int i = 0; i < num_rays; i++) {
	  fscanf(oldfp, "%f:%f ", &angle, &range);
	  fprintf(newfp, "%f:%f ", angle, range);
	}
	fscanf(oldfp, "\n");
	fprintf(newfp, "\n");
       
      } while (result);

      exit(1);

    }
    
    // converts .state files written by ladar on Aug. 04 to a form I can use

    else if (!strcmp(argv[i],                        "-fixraw")) {

      char *newname = (char *) calloc(1012, sizeof(char));
      char *sysstring = (char *) calloc(1012, sizeof(char));
      strcpy(newname, ladarray->ladsrc[0]->statesrc->filename);
      sprintf((char *) &newname[strlen(newname)], "2\0");
      sprintf(sysstring, "mv %s %s", ladarray->ladsrc[0]->statesrc->filename, newname);
      system(sysstring);

      FILE *stateout_fp = fopen(ladarray->ladsrc[0]->statesrc->filename, "w");
      fprintf(stateout_fp, "\%% this log has been fixed\n");

      do {
	ladarray->ladsrc[0]->capture();
	fprintf(stateout_fp, "%Li %Li 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0\n",  ladarray->ladsrc[0]->long_timestamp,  ladarray->ladsrc[0]->long_timestamp);
      } while (1);
      exit(1);

    }

    else if (!strcmp(argv[i],                        "-image_timestamps")) {
      if (imsrc)
	imsrc->setup_timestamp(argv[i + 1]);
      do_alice = FALSE;
    }

#ifdef SKYNET

    // bumper or roof needs to be specified here

    else if (!strcmp(argv[i],                        "-ladar_skynet")) {
      ladarray->add_ladar(new UD_LadarSource_Skynet(ladcal, atof(argv[i + 1])));
      do_alice = FALSE;

    } 
#endif

else if (!strcmp(argv[i],                        "-nocompute")) {
      do_computations = FALSE;

    // path, save image interval

    } else if (!strcmp(argv[i],                        "-log")) {
      live_logger = new UD_Logger(argv[i + 1], atoi(argv[i + 2]), ladarray);

    // ladar's mount point, FOV

    } else if (!strcmp(argv[i],                        "-sw_roofladar")) {   // use calibration for cab-mounted ladar at Stoddard Wells 
      //      ladcal = new UD_LadarCalibration(201, 50.0, -0.5, 0.0, -2.387, -0.477, -0.2);
      ladcal = new UD_LadarCalibration(181, 90.0, -1.0, 0.0, -2.387, -0.477, -0.2);

    } else if (!strcmp(argv[i],                        "-swjune_roofladar")) {   // use calibration for roof-mounted ladar at Stoddard Wells 
      ladcal = new UD_LadarCalibration(201, 50.0, -0.5, 0.0, -2.413, -0.477, -0.09032);

    } else if (!strcmp(argv[i],                        "-sw_bumperladar")) {   // use calibration for bumper-mounted ladar at Stoddard Wells 
      ladcal = new UD_LadarCalibration(201, 50.0, -0.5, 0.0, -0.648, 0.965, 0.0);

    } else if (!strcmp(argv[i],                        "-swaug_bumperladar")) {   // use calibration for bumper-mounted ladar at Stoddard Wells 
      ladcal = new UD_LadarCalibration(1024, 90.0, -1.0, 0.0, -0.648, 0.965, 0.0);

    } else if (!strcmp(argv[i],                        "-swaug21_bumperladar")) {   // use calibration for bumper-mounted ladar at Stoddard Wells 
      ladcal = new UD_LadarCalibration(181, 90.0, -1.0, 0.0, -0.648, 0.965, 0.0);

    // see here for information (e.g., FOV) on Caltech's stereo cameras: http://team.caltech.edu/members/

    } else if (!strcmp(argv[i],                        "-sw_rightcam")) {   // use calibration for right-hand stereo camera at Stoddard Wells 
      camcal = new UD_CameraCalibration(640, 480, DEG2RAD(94.3), 380.94, 379.83, 344.58, 243.72, 0.226, -2.088, -0.468, -0.1, 0.0);  // k1 = -0.266, k2 = 0.0877
    } else if (!strcmp(argv[i],                        "-sw_leftcam")) {   // use calibration for left-hand stereo camera at Stoddard Wells 
      camcal = new UD_CameraCalibration(640, 480, DEG2RAD(94.3), 380.94, 379.83, 344.58, 243.72, -0.248, -2.089, -0.464, -0.1, DEG2RAD(-2));
    
    } else if (!strcmp(argv[i],                        "-sw_centercam")) {   // use calibration for left-hand stereo camera at Stoddard Wells 
      camcal = new UD_CameraCalibration(640, 480, DEG2RAD(94.3), 380.94, 379.83, 344.58, 243.72, 0.0, -2.089, -0.464, -0.1, DEG2RAD(-4));
    
    } else if (!strcmp(argv[i],                        "-noalice")) {
      do_alice = FALSE;
    } else if (!strcmp(argv[i],                        "-alice")) {
      force_alice = true;

    } 

    // path to state source (RDDF or log), tile directory, and run mode ("fetch", "nofetch")

 else if (!strcmp(argv[i],                        "-aerial")) {
      do_aerial = TRUE;
      UD_StateSource *ss = NULL;
      if (!ladarray->no_ladar()) 
	ss = ladarray->ladsrc[0]->statesrc;
      tempaerial = new UD_Aerial(argv[i+1], argv[i+2], argv[i+3], ss);
      UD_Aerial::aerial->finish_construction(argv[i+1], argv[i+2], argv[i+3], ss);
#ifdef SKYNET
    } else if(!strcmp(argv[i], "-snkey") || !strcmp(argv[i], "--snkey") ||
	      !strcmp(argv[i], "-skynetkey") || !strcmp(argv[i], "--skynetkey")) {
      //Parse skynet key
      options.snkey = atoi(argv[i+1]);
      ++i;
    } else if(!strcmp(argv[i], "-playback") || !strcmp(argv[i], "--playback")) {
      options.playback = true;
    } else if(!strcmp(argv[i], "-novideo") || !strcmp(argv[i], "--novideo")) {
      options.novideo = true;
    } else if(!strcmp(argv[i], "-nodisp") || !strcmp(argv[i], "--nodisp")) {
      options.nodisp = true;
    } else if(!strcmp(argv[i], "-nowait") || !strcmp(argv[i], "--nowait")) {
      options.waitstate = false;
#endif
    }
  }

  if(force_alice)
    do_alice = TRUE;
  
  if (!imsrc) 
    do_computations = FALSE;
}

//----------------------------------------------------------------------------

int main(int argc, char **argv)
{
#ifdef SKYNET
  //Add all the sparrow display pages
  SparrowHawk().add_page(vddroad, "Main");

  options.playback = false;
  options.snkey=-1;
  options.novideo=true;
  //options.novideo=false;
  options.nodisp=true;
  options.logging=true;
  options.waitstate=true;
#endif

  init_per_run(argc, argv);

  return 1;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
