//SUPERCON STRATEGY TITLE: plannerModule failed to output a trajectory - ANYTIME STRATEGY (blocking for all strategies EXCEPT for EstopPausedNOTsuperCon
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyPlnFailedNoTraj.hh"
#include "StrategyHelpers.hh"

bool CStrategyPlnFailedNoTraj::checkForPlnOutputNewTraj( bool checkBool, const SCdiagnostic *pdiag ) {
  
  if( !checkBool ) {
    
    if( pdiag->trajReceivedFromPln == true && pdiag->pln_failed_deceleration_constraint == false ) {
      checkBool = true;
      transitionStrategy(StrategyNominal, "PlnFailedNoTraj: new traj received from planner -> NOMINAL");
      SuperConLog().log( STR, WARNING_MSG, "PlnFailedNoTraj: new traj received from planner -> NOMINAL" );
    }
  }
  return checkBool;
}

void CStrategyPlnFailedNoTraj::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE (this MUST remain here)
  currentStageName = plnfailed_stage_names_asString( plnfailed_stage_names(stgItr.nextStage()) );
  
  switch( stgItr.nextStage() ) {
    
  case s_plnfailed::set_tf_speedcaps: 
    //The planner failed to produce a plan in its last execution cycle, set
    //the trajFollower speed-caps so that Alice will stop as fast as possible
    //whilst following the current traj
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	skipStageOps = checkForPlnOutputNewTraj( skipStageOps, m_pdiag );
      }
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_modifyTrajFSpeedCap( add_max_speedcap, DBL_ZERO );
	m_pStrategyInterface->stage_modifyTrajFSpeedCap( remove_min_speedcap, DBL_ZERO );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "PlnFailedNoTraj - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;
    
  case s_plnfailed::clear_maps: 
    //Previously set the trajFollower speed-caps such that we should
    //stop as fast as possible (whilst still being safe), now clear
    //the maps, as the planner has not yet produced a new plan
    { 
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	skipStageOps = checkForPlnOutputNewTraj( skipStageOps, m_pdiag );
      }
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */      
      if( m_pdiag->aliceStationary == false ) {
	
	skipStageOps = true;	
	SuperConLog().log( STR, WARNING_MSG, "StrategyPlnFailedNoTraj: alice not yet stationary -> will poll");
	checkForPersistLoop( this, persist_loop_waitstationary, stgItr.currentStageCount() );
      }
            
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_clearMaps( sc_interface::clear_all_maps_everywhere );	
	//clear the trajFollower reversing buffer after clearing the maps
	m_pStrategyInterface->stage_changeTFmode( clear_tf_buffer, DBL_ZERO );	
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "PlnFailedNoTraj - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;

  case s_plnfailed::wait_new_traj: 
    //maps are now cleared, wait for planner to succeed, if it does
    //NOT, then push forwards along the previous trajectory sent
    //to trajFollower (using Lone-ranger)
    { 
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	skipStageOps = checkForPlnOutputNewTraj( skipStageOps, m_pdiag );
      }

      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */      
      
      if( m_pdiag->clear_maps_complete == false ) {
	
	skipStageOps = true;
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "StrategyPlnFailedNoTraj - confirmation of map-clearing NOT yet received from Fmapper -> will poll" );
	checkForPersistLoop( this, persist_loop_clearmap, stgItr.currentStageCount() );	
      }      

      if( m_pdiag->trajReceivedFromPln == false ) {
	
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "StrategyPlnFailedNoTraj planner NOT yet output a valid traj -> will poll");

	//timeout handled here as this reverts to the LturnReverse strategy
        if( stgItr.currentStageCount() > TIMEOUT_LOOP_WAIT_FOR_NEW_PLAN ) {
	  SuperConLog().log( STR, WARNING_MSG, "WARNING: StrategyPlnFailedNoTraj - wait for pln timed out -> LONE-RANGER" );
	  transitionStrategy( StrategyLoneRanger, "WARNING: StrategyPlnFailedNoTraj - wait for pln timed out -> LONE-RANGER" );
	}	
      }
      
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	transitionStrategy( StrategyNominal, "PlnFailedNoTraj - Strategy complete --> nominal" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "PlnFailedNoTraj - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;

    /* END OF STRATEGY */

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyPlnFailedNoTraj::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: PlnFailedNoTraj::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: PlnFailedNoTraj - default stage reached!");
    }
  }
}


void CStrategyPlnFailedNoTraj::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "PlnFailedNoTraj - Exiting");
}

void CStrategyPlnFailedNoTraj::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset(); //reset the stage iterator
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "PlnFailedNoTraj - Entering");
}
