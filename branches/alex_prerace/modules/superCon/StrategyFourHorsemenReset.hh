#ifndef STRATEGY_FOURHORSEMEN_HH
#define STRATEGY_FOURHORSEMEN_HH

//SUPERCON STRATEGY TITLE: Four-horsemen of the acpocalypse reset
//HEADER FILE (.hh)
//DESCRIPTION: Strategy to reset the complete system in the event
//of a sustained unidentified NFP scenario

#include "Strategy.hh"
#include "sc_specs.h"
#include "stage_iterator.hh"

namespace s_fourhorsemen {

#define ENDOFRDDF_STAGE_LIST(_) \
  _(estop_pause, = 1) /*first entry must = 1 (as this is referenced against NEXTstage */ \
  _(clear_all_maps, ) \
  _(trans_loneranger, )
DEFINE_ENUM(fourhorsemen_stage_names, ENDOFRDDF_STAGE_LIST)

}

using namespace std;
using namespace s_endofrddf;

class CStrategyFourHorsemenReseet : public CStrategy
{

/** METHODS **/
public:

  CStrategyFourHorsemenReseet() : CStrategy() {}  
  ~CStrategyFourHorsemenReseet() {}


  /** MUTATORS **/
  void stepForward(const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface);
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

};

#endif
