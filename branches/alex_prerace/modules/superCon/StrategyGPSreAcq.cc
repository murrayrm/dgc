//SUPERCON STRATEGY TITLE: GPS re-acquisition - ANYTIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyGPSreAcq.hh"
#include "StrategyHelpers.hh"

//Check for ANYTIME EXIT condition - used by astate to 'cancel' an 'active'
//GPSreAcq request.
bool CStrategyGPSreAcq::checkAstateMoveForwardNoClear( bool checkBool, const SCdiagnostic *pdiag, CStrategyInterface *pStrategyInterface ) {
  
  if( !checkBool ) {
    if( pdiag->astate_move_without_clear == true && pdiag->astNeedToIncludeGPSafterReAcq == false) {
      checkBool = true;
      transitionStrategy( StrategyNominal, "GPSreAcq: astate was not able to include the new GPS signal --> NOMINAL to proceed" );
      SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "GPSreAcq: astate was not able to include the new GPS signal --> NOMINAL to proceed" );
      
      if( pdiag->astate_jump_requires_TFrevBuffer_cleared ) {
	pStrategyInterface->stage_changeTFmode( clear_tf_buffer, DBL_ZERO );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - state-jump was big enough to require TF reverse-buffer clearing" );
      }            
      setDoubleState( astate_jump_size, 0.0 ); //reset astate jump size
      SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - astate_jump_size reset to zero" );      
    }
  }  
  return checkBool;
}

void CStrategyGPSreAcq::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE (this MUST remain here)
  currentStageName = gpsreacq_stage_names_asString( gpsreacq_stage_names(stgItr.nextStage()) );

  switch( stgItr.nextStage() ) {
    
  case s_gpsreacq::estop_pause: 
    //astate has requested superCon ready the system for re-inclusion of the
    //re-acquired GPS signal - hence superCon e-stop pause Alice
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAstateMoveForwardNoClear( skipStageOps, m_pdiag, m_pStrategyInterface );
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;
    
  case s_gpsreacq::confirm_pause_to_astate: 
    //confirm that Alice is paused, and stationary and then signal to
    //astate that it should include the GPS signal (as it is safe
    //for our state estimate to jump
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAstateMoveForwardNoClear( skipStageOps, m_pdiag, m_pStrategyInterface );
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->superConPaused == false ) {
	//NOT yet superCon paused - adrive has not yet processed the command
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "WARN: Alice not yet paused, cannot re-acq GPS - will poll");
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );
      }

      if( m_pdiag->aliceOBD2stationary == false ) {
	
	skipStageOps == true;
	SuperConLog().log( STR, WARNING_MSG, "WARN: Alice not yet stationary & braking (using OBD2) - will poll");
	checkForPersistLoop( this, persist_loop_waitstationary, stgItr.currentStageCount() );	
      }

      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_signalToAstate(sc_interface::ok_to_add_gps);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;    

  case s_gpsreacq::wait_astate_response: 
    //Sent confirmation of acceptable conditions for astate to allow jumping
    //while it re-acquires/incorporates GPS, wait for response from astate
    //detailing what action to take next based upon how successful it was
    //at re-incorporating the signal (specifically we are waiting for astate
    //to confirm that a map-clear IS required (or for astate to timeout)
    {

      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      //NO strategy conditional trans/term checks should be made at this
      //point, as the quality/accuracy of astate's output at this point is
      //highly uncertain
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAstateMoveForwardNoClear( skipStageOps, m_pdiag, m_pStrategyInterface );      

      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->astate_move_with_clear == false ) {
	//astate not yet determined what action to take
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "WARN: GPSreAcq - astate NOT yet confirmed cost-map clearing required - will poll");
	
	if( stgItr.currentStageCount() > TIMEOUT_LOOP_ASTATE_RESPONSE_THRESHOLD ) {
	  //timed-out waiting for astate, clear maps and -> NOMINAL (to proceed
	  //forwards)
	  m_pStrategyInterface->stage_signalToAstate( sc_interface::timed_out_waiting_for_astate );
	  SuperConLog().log( STR, WARNING_MSG, "WARNING: timed-out waiting for astate to respond: clear maps & -> NOMINAL" );
	  
	  m_pStrategyInterface->stage_changeTFmode( clear_tf_buffer, DBL_ZERO );
	  SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - timed-out on astate -> clear TF reverse buffer as its quality is unknown" );
	  
	  stgItr.setStage( s_gpsreacq::clear_cmap );
	}
      }
      
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	//proceed to the clear_cmap stage (this point reached after a move_with_clear message) AFTER
	//determining whether to clear the TF reverse buffer based upon the magnitude of the state jump
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - Stage - %s completed", currentStageName.c_str() );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - astate requested a map-clear" );
	stgItr.setStage( s_gpsreacq::clear_cmap );
      }            
    }
    break;

  case s_gpsreacq::clear_cmap: 
    //send a message to fusionMapper to clear the current cost-map as astate
    //has successfully completed a GPS re-acquisition jump
    {

      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      //NO strategy conditional trans/term checks should be made at this
      //point, as the quality/accuracy of astate's output at this point is
      //highly uncertain
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAstateMoveForwardNoClear( skipStageOps, m_pdiag, m_pStrategyInterface );      

      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	//signal to fusionMapper to clear the current map, as the GPS re-acq
	//is complete, and it is assumed that it resulted in a state jump
	m_pStrategyInterface->stage_clearMaps( sc_interface::clear_all_maps_everywhere );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - cost-map clearing action required" );
	
	//was the astate jump big enough to warrant clearing the trajFollower
	//reversing buffer?
	if( m_pdiag->astate_jump_requires_TFrevBuffer_cleared ) {
	  m_pStrategyInterface->stage_changeTFmode( clear_tf_buffer, DBL_ZERO );
	  SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - state-jump was big enough to require TF reverse-buffer clearing" );
	} else {
	  SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - state-jump was NOT big enough to warrant clearing TF buffer" );
	}
	
	setDoubleState( astate_jump_size, 0.0 ); //reset astate jump size
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - astate_jump_size reset to zero" );
	
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;

  case s_gpsreacq::trans_nominal: 
    //Sent command to clear map to fusionMapper - hence there should
    //NOT be any artifacts in the map resulting from state jumps
    //so return to NOMINAL
    { 
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      if( m_pdiag->clear_maps_complete == false ) {
	
	skipStageOps = true;
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - confirmation of map-clearing NOT yet received from Fmapper -> will poll" );
	checkForPersistLoop( this, persist_loop_clearmap, stgItr.currentStageCount() );	
      }
      
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	//un-pause Alice as the GPS re-acquisition process has been
	//completed --> NOMINAL
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - confirmation of map-clearing received from fmapper" );
	transitionStrategy( StrategyNominal, "GPSreAcq - Strategy complete --> nominal" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "GPSreAcq - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;


    /* END OF STRATEGY */

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyGPSreAcq::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyGPSreAcq::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: GPSreAcq - default stage reached!");
    }
  }
}


void CStrategyGPSreAcq::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "GPSreAcq - Exiting");
}

void CStrategyGPSreAcq::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset(); //reset the stage iterator
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "GPSreAcq - Entering");
}


