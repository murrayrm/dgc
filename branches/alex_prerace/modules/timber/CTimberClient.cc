#include "CTimberClient.hh"
#include <netinet/in.h>

using namespace std;


const std::string CTimberClient::RUN_DIR = "/var/dgc/run";


CTimberClient::CTimberClient(int module_enum)
{
  /** make some mutexes (before we can use our functions) */
  DGCcreateMutex(&timber_state_mutex);
  DGCcreateMutex(&module_name_mutex);
  DGCcreateMutex(&debug_level_mutex);
  DGCcreateMutex(&server_last_seen_mutex);
  DGCcreateMutex(&updated_dir_mutex);
  DGCcreateMutex(&local_logging_mutex);
  DGCcreateMutex(&playback_state_mutex);

  setDebugLevel(DEFAULT_DEBUG);
  printDebug("Begin CTimberClient Constructor", 2);
  /** 
   * Save the my PID, so that we can also log command and options in
   * timber logfile or directory . 
   */
  m_pid =  getpid();
  
  /** set up the map from module enum values to module names */
  map <int, string> MAPNAME;
  TIMBER_LOG_INFO(TIMBER_LOG_STRINGMAP)

  /** set all log levels to 0 (until we hear from the server) */
  TIMBER_LOG_INFO(INITIALIZE_LOG_LEVELS)

  module_enum_value = module_enum;
  module_name = new char[MAX_MODULENAME_LENGTH];
  setModuleName(MAPNAME[module_enum_value].c_str());

  log_directory_base = new char[MAX_PATH_LENGTH];
  string dir_string = getLogDirectoryBase();
  printDebug("getLogDirectoryBase returned " + dir_string, 1);
  strcpy(log_directory_base, dir_string.c_str());
  setLocalLogging(false);
  setUpdatedDir(false);

  setLastSeen(0); // we haven't seen the server yet

  session_timestamp = new char[MAX_TIMESTAMP_LENGTH];
  session_timestamp[0] = '\0';
  logging_enabled = false;

  playback_state.real_time_point = 12345;
  playback_state.playback_time_point = 12345;
  playback_state.playback_speed = 0;
  playback_state.playback_location = "";
  playback_state.new_playback_location = false;

  /** start the receive thread */
  DGCstartMemberFunctionThread(this, &CTimberClient::RecvThread);
  lastDirUsed = "";
  printDebug("End CTimberClient Constructor", 2);
}



CTimberClient::CTimberClient()
{
  cerr << endl
       << "CTimberClient::CTimberClient():  Default constructor called."
       << "This should never be the case... only use the one argument"
       << "constructor" << endl;
  assertThrow(false, "CTimberClient::CTimberClient():  Default constructor called.");
}



CTimberClient::~CTimberClient()
{
  /** delete the mutexes */
  DGCdeleteMutex(&timber_state_mutex);
  DGCdeleteMutex(&module_name_mutex);
  DGCdeleteMutex(&debug_level_mutex);
  DGCdeleteMutex(&server_last_seen_mutex);
  DGCdeleteMutex(&local_logging_mutex);
  DGCdeleteMutex(&updated_dir_mutex);
  DGCdeleteMutex(&playback_state_mutex);

  delete [] module_name;
  delete [] log_directory_base;
  delete [] session_timestamp;
}



string CTimberClient::getLogDir()
{
  string ret;

  /** verify that we have a server connection */
  if (!connectionStatus() && getLoggingEnabled())
    // we just lost the connection (it used to be on)
    // do the same thing as "got server off", but logging was on
    {
      lockState();
      cerr << "TIMBER ERROR: CONNECTION STATUS FAILED!!" << endl;
      logging_enabled = false;
      session_timestamp[0] = '\0';
      unlockState();
    }

  if (getLoggingEnabled())
    // directory already exists
    ret = getDirFromTimestamp();
  else
    {
      // logging is off
      lockState();
      char temp = session_timestamp[0];
      unlockState();
      if( temp == '\0')
	// give them a new fake directory
	{
	  //store fake timestamp
	  unsigned long long time;
	  DGCgettime(time);
	  unsigned long long the_time_round = (unsigned long long)DGCtimetollsec(time);
	  lockState();
	  session_timestamp[0] = '\0';
	  sprintf(session_timestamp, "%lli", the_time_round);
	  strcat(session_timestamp, FAKE_TIMESTAMP_MODIFIER);
	  unlockState();

	  createDirFromTimestamp();

	  ret = getDirFromTimestamp();
	}
      else
	{
	  ret = getDirFromTimestamp();
	}
    }

  // now we'll automatically add this folder.  this is a bit of a hack
  // as originally i had intended for people to create and manage their
  // own folders, but now timber does it all for them.
  addRelFolder(".");
  //  writeCommandLine(ret);
  return ret;
}



string CTimberClient::getPlaybackDir()
{
  string playback_dir = "";
  bool first_failure = true;
  bool ready = false;

  while ( !ready )
    {
      lockPlaybackState();
      playback_dir = playback_state.playback_location;
      unlockPlaybackState();
      
      if (playback_dir == "")
	{
	  
	  if (first_failure)
	    {
	      cout << "Waiting for Timber to tell us which directory to playback from...";
	      cout.flush();
	      first_failure = false;
	    }
	  else
	    {
	      cout << ".";
	      cout.flush();
	    }
	}
      else
	{
	  // make sure this directory exists
	  struct stat dir_stat;
	  stat(playback_dir.c_str(), &dir_stat);

	  if ( !S_ISDIR(dir_stat.st_mode) )
	    cout << "CTimberClient::getPlaybackDir():  Error:  \"" << playback_dir
		 << "\" does not exist!" << endl;
	  else
	    ready = true;
	}
      
      if (!ready)
	usleep(1000000);
      else
	break;
    } // end of while(true)
  if(!first_failure)
    cout << endl;

  // tack on the name of the module
  string modname(module_name);
  
  if (playback_dir[playback_dir.length()-1] != '/')
    playback_dir += "/";

  return playback_dir + modname + "/";
}



bool CTimberClient::checkNewDirAndReset()
{
  if (getUpdatedDir())
    {
      setUpdatedDir(false);
      return true;
    }
  else 
    return false;
}



bool CTimberClient::checkNewPlaybackDirAndReset()
{
  lockPlaybackState();
  bool ret = playback_state.new_playback_location;
  playback_state.new_playback_location = false;
  unlockPlaybackState();

  return ret;
}



bool CTimberClient::connectionStatus()
{
  unsigned long long time, ull_server_delay;
  DGCgettime(time);
  ull_server_delay = time - getLastSeen();
  double server_delay = DGCtimetosec(ull_server_delay, true);
  //  printDebug("server_delay is " + (string)string_server_delay, 2);

  return (server_delay < MAX_SERVER_DELAY);
}



string CTimberClient::getUser()
{
  char* user = new char[MESSAGE_LENGTH];
  user[0] = '\0';
  passwd* p_user_passwd = getpwuid(geteuid());
  strcat(user, p_user_passwd->pw_name);
  string ret(user);
  delete [] user;
  return ret;
}



string CTimberClient::getIPString()
{
  char* ip = new char[MESSAGE_LENGTH];
  ip[0] = '\0';


  // half-hacked
  char* iface;
#warning "hack... hard coded eth0... won't work for macs or wireless..."
  iface = "eth0";

  // blatantly copied from skynet_static something or other
  struct ifaddrs *myaddrs, *ifa;
  struct sockaddr_in *s4 = NULL;
  int status;
  
  status = getifaddrs(&myaddrs);
  if (status != 0)
    {
      perror("getifaddrs");
      exit(1);
    }
  
  for (ifa = myaddrs; ifa != NULL; ifa = ifa->ifa_next)
    {
      if ((ifa->ifa_addr != NULL) && ((ifa->ifa_flags & IFF_UP) != 0))
	{
	  if ( strcmp(ifa->ifa_name, iface) == 0)
	    {
	      s4 = (struct sockaddr_in *)(ifa->ifa_addr);
	    }
	}
    }
  if( s4 == NULL)
    {
      cout << "CTimberClient::storeMyIp eth0 not found.  Network unconfigured?\n" << endl;
      exit(1);
    }
  freeifaddrs(myaddrs);
  uint32_t ip_as_uint =  ntohl((s4->sin_addr).s_addr);

  //###  
  //  printf("%d.%d.%d.%d", ip_as_uint >> 24, (ip_as_uint >> 16) & 0xff, 
  //	 (ip_as_uint >> 8) & 0xff, ip_as_uint & 0xff);
  //  cout << endl;
  //###

  // they must have allocated it before
  sprintf(ip, "%d.%d.%d.%d", ip_as_uint >> 24, (ip_as_uint >> 16) & 0xff, 
	  (ip_as_uint >> 8) & 0xff, ip_as_uint & 0xff);

  string ret(ip);
  delete [] ip;
  return ret;
}



//
// DEPRACATED FUNCTION
/**
 * cat the directory name onto the end of the dest
 */
/*
void CTimberClient::catLogDirName(char* dest, bool fake_it)
{  
  // the folder MAY OR MAY NOT EXIST AT THIS POINT!
  double server_delay;
  unsigned long long time, ull_server_delay;
  DGCgettime(time);
  DGClockMutex(&server_last_seen_mutex);
  ull_server_delay = time - server_last_seen;
  DGCunlockMutex(&server_last_seen_mutex);
  server_delay = DGCtimetosec(ull_server_delay);
  if (server_delay > MAX_SERVER_DELAY) // haven't seen server for a while (or ever)
      cout << "NOT USING TIMBER!!!" << endl;

  strcat(dest, dgc_dir);
  strcat(dest, "/");
  strcat(dest, LOG_DIRECTORY);
  strcat(dest, "/");

  if (fake_it)
    {
      if (loggingEnabled())
	{
	  DGClockMutex(&session_timestamp_mutex);
	  strcat(dest, session_timestamp);   //must already exist
	  DGCunlockMutex(&session_timestamp_mutex);
	}
      else
	{
	  DGClockMutex(&session_timestamp_mutex);
	  long long int the_time_round = llround(DGCtimetosec(time));
	  session_timestamp[0] = '\0';
	  sprintf(session_timestamp, "%lli", the_time_round);
	  strcat(session_timestamp, FAKE_TIMESTAMP_MODIFIER);
	  strcat(dest, session_timestamp);
	  DGCunlockMutex(&session_timestamp_mutex);
	}
    }
  else
    {
      while (!loggingEnabled())
	{
	  cout << "Waiting for Timber to start log..." << endl;
	  usleep(1000000);
	}
      DGClockMutex(&session_timestamp_mutex);
      strcat(dest, session_timestamp);
      DGCunlockMutex(&session_timestamp_mutex);
    }

  strcat(dest, "/");
  DGClockMutex(&module_name_mutex);
  strcat(dest, module_name);
  DGCunlockMutex(&module_name_mutex);
  strcat(dest, "/");
}
*/



/** 
 * State flow hierarchy:
 * Get message -> update server_last_seen
 *
 * message is client message (sent by us): ignore
 * message is "server, logging on"
 *   (logging_enabled):
 *     (timestamp has changed): change to new timestamp, create new dir
 *     (timestamp is same): nothing
 *   (!logging_enabled): turn on logging, change session_timestamp, create dir
 * message is "server, logging off"
 *   (logging_enabled): clear session_timestamp, turn off logging
 *   (!logging_enabled): nothing
 *
 * there, thats' not so bad...
 */
void CTimberClient::RecvThread()
{
  printDebug("CTimberClient::RecvThread() has been called", 2);

  {
    ostringstream oss;
    oss << "SNtimberstring is " << SNtimberstring << " and ALLMODULES is " << ALLMODULES << endl;
    printDebug(oss.str(), 2);
  }
  
  int recvSocket = m_skynet.listen(SNtimberstring, ALLMODULES);
  char* message = new char[MESSAGE_LENGTH];
  int length = 0;


  while (true)
    {
      // get the message
      printDebug("trying to receive message...", 2);
      RecvTimberString(recvSocket, message);
      printDebug("(CTimberClient received \"" + (string)message + "\")", 2);
      
      // update server_last_seen
      lockLastSeen();
      DGCgettime(server_last_seen);
      unlockLastSeen();

      length = strlen(message);
      if (length >= TAGLENGTH)
	{
	  /****************
	   * First, check for logging message and process it
	   ****************/	  
	  lockState(); // state should be locked for this whole section!
	  if ( (strncmp(message, TAG_LOGTIMEON, TAGLENGTH) == 0) )
	    // got "server, log on"
	    {
	      istringstream rest_of_message(message + TAGLENGTH);
	      // cout << "rest of message is \"" << rest_of_message.str() << "\"" << endl;
	      string time_from_msg_string;
	      rest_of_message >> time_from_msg_string;
	      // char* time_from_msg = time_from_msg_string.c_str();

	      /** read in individual logging values */
	      int temp;
	      for (int i = 0; i < timber_types::LAST; i++)
		{
		  rest_of_message >> temp;
		  LEVEL_ARRAY_NAME[i] = temp;
		}
	      
	      if (logging_enabled)
		{
		  if (strcmp(time_from_msg_string.c_str(), session_timestamp) != 0)
		    // got a new timestamp, so start NEW RUN
		    {
		      strcpy(session_timestamp, time_from_msg_string.c_str());
		      createDirFromTimestamp();
		    }
		  //  delete time_from_msg; // do i need to delete this???
		}
	      else
		// logging is off, so turn it on
		{
		  //turn on, create dir, change timestamp
		  logging_enabled = true; // might be wrong...need to clear session_timestamp???
		  strcpy(session_timestamp, time_from_msg_string.c_str());
		  createDirFromTimestamp();
		  // delete time_from_msg;
		}
	    }

	  else if ( (strncmp(message, TAG_LOGOFF, TAGLENGTH) == 0) )
	    // got "server, log off"
	    {
	      if (logging_enabled)
		// turn logging off
		{
		  logging_enabled = false;
		  cerr << "TIMBER: TIMBER SERVER TURNED LOGGING OFF!!" << endl;
		  session_timestamp[0] = '\0';
		}
	    }
	  unlockState();
	  
	  /****************
	   * Second, check for playback message and process it
	   ****************/
	  if ( (strncmp(message, TAG_PLAYINFO, TAGLENGTH) == 0) )
	    {
	      lockPlaybackState();
	      string old_playback_location = playback_state.playback_location;
	      istringstream iss(message + TAGLENGTH);
	      iss >> playback_state.real_time_point
		  >> playback_state.playback_time_point
		  >> playback_state.playback_speed
		  >> playback_state.playback_location;
	      if (playback_state.playback_location != old_playback_location)
		playback_state.new_playback_location = true;
	      unlockPlaybackState();
	    }
	}
      else
	printDebug("Message was unusually short: \"" + (string)message
		   + "\"", 2);
    } // end of while (true)
     
  delete [] message;
}



/** prints out debug messages */
void CTimberClient::printDebug(string message, double req_level, bool newline)
{
  //  cout << "debug level is " << getDebugLevel() << " and required is " << req_level << endl;
  if (getDebugLevel() >= req_level)
    {
      cout << message;
      if (newline)
	cout << endl;
      else
	cout.flush();
    }
}



/** creates a new dir for logs
 * IMPORTANT:  the state mutex must be locked for the duration
 * of this function.
 */
void CTimberClient::createDirFromTimestamp()
{
  // testing stuff
  /*  cout << endl;
  cout << "*** time as a time_t is " << (time_t)atol(session_timestamp) << endl;
  cout << "*** time as a long is " << atol(session_timestamp) << endl;
  cout << "*** time as an unsigned long long is " << atol(session_timestamp) << endl;

  time_t thetime = (time_t)atol(session_timestamp);
  time_t* ptime = &thetime;
  //  tm* tmstruct = gmtime(ptime);
  tm* tmstruct = localtime(&thetime);
  //  tm* tmstruct = localtime( &((time_t)atol(session_timestamp)) );
  char * weekday[] = { "Sunday", "Monday",
                       "Tuesday", "Wednesday",
                       "Thursday", "Friday", "Saturday"};


  cout << "Day is " << weekday[tmstruct->tm_wday] << endl;
  cout << "complete time is " << string(asctime(tmstruct)) << endl; */
  // end testing stuff

  char* sys_command = new char[MAX_COMMAND_LENGTH];
  char* new_dir = new char[MAX_COMMAND_LENGTH];
  sys_command[0] = '\0';
  new_dir[0] = '\0';

  strcat(new_dir, " "); // add a space before the directory name
  string rest_of_path = getDirFromTimestamp(false);
  strcat(new_dir, rest_of_path.c_str());

  strcat(sys_command, CMD_MKDIR);
  strcat(sys_command, new_dir);
  int result = system(sys_command); // run command
  printDebug("Creating directory with command \""
	     + (string)sys_command + "\"", 1);

  if (result != 0)
    cerr << "*******************************************\n"
	 << "* Couldn't create directory using command:\n"
	 << "*   " << (string)sys_command << "\n"
	 << "* \n"
	 << "* Logging won't work!\n"
	 << "*******************************************" << endl;
  else
    setUpdatedDir(true);

  sys_command[0]='\0';
  strcat(sys_command, CMD_MKLINK);
  new_dir[0] = '\0';
  strcat(new_dir, " "); // add a space before the directory name
  rest_of_path = getDirFromTimestamp(false, true, false);
  strcat(new_dir, rest_of_path.c_str());
  strcat(sys_command, new_dir);
  strcat(sys_command, " ");
  strcat(sys_command, SYMLINK_NAME);
  printDebug("Creating symlink with command \""
	     + (string)sys_command + "\"", 1);
  result = system(sys_command);
  if (result != 0 && DONT_SCARE_PEOPLE == 0)
    cerr << "*******************************************\n"
	 << "* Couldn't create \"" << (string)SYMLINK_NAME "\" symlink using command:\n"
	 << "*   " << (string)sys_command << "\n"
	 << "* Return value was " << result << "\n"
	 << "* \n"
	 << "* Sorry :-D\n"
	 << "* Make sure you are running in bin!\n"
	 << "*******************************************" << endl;


  /** commented out sept 21 by jason in an attempt to make timber stop 
   * segfaulting everyone's modules */
#warning "this line makes everyone's stuff crash..."
  //writeCommandLine(new_dir);


  delete [] sys_command;
  delete [] new_dir;
}



string CTimberClient::getDirFromTimestamp(bool lock_state, bool with_base, bool with_modulename)
{
  char* ret_char = new char[MAX_COMMAND_LENGTH];
  ret_char[0] = '\0';

  if (lock_state)
    lockState();

  if (with_base) // whether or not we want to add the log_directory_base to our return
    strcat(ret_char, log_directory_base); // defined by constructor

  if (USE_UNIX_TIMESTAMP)
    strcat(ret_char, session_timestamp);
  else
    {
      time_t thetime = (time_t)atol(session_timestamp);
      tm* tmstruct = localtime(&thetime);
      char buffer[30];  // 30 sounds good
      //cout << "year is " << tmstruct->tm_year << endl;
      sprintf(buffer, "%04d", tmstruct->tm_year + 1900); strcat(ret_char, buffer);
      strcat(ret_char, "_");
      sprintf(buffer, "%02d", tmstruct->tm_mon + 1); strcat(ret_char, buffer);
      strcat(ret_char, "_");
      sprintf(buffer, "%02d", tmstruct->tm_mday); strcat(ret_char, buffer);
      strcat(ret_char, "/");
      sprintf(buffer, "%02d", tmstruct->tm_hour); strcat(ret_char, buffer);
      strcat(ret_char, "_");
      sprintf(buffer, "%02d", tmstruct->tm_min); strcat(ret_char, buffer);
      strcat(ret_char, "_");
      sprintf(buffer, "%02d", tmstruct->tm_sec); strcat(ret_char, buffer);
    }
  strcat(ret_char, "/");

  if (with_modulename)
    {
      strcat(ret_char, module_name);
      strcat(ret_char, "/");
    }

  if (lock_state)
    unlockState();

  string ret(ret_char);
  return ret;
}



/**
 * Attempts to read in the DGCDIRFILE in the current directory to get 
 * the path to the root (usually will be "/home/user/dgc")
 */
string CTimberClient::getLogDirectoryBase()
{
  printDebug("getLogDirectoryBase() called.", 2);
  string ret = "";

  if (RELATIVE_LOGGING)
    {
      ifstream file(DGCDIRFILE);
      if (file.is_open())
	{
	  string from_file;
	  file >> from_file;
	  printDebug("From file \"" + (string)DGCDIRFILE + "\", read \"" + from_file + "\"", 1);
	  ret = from_file;
	  if (ret[ret.length() - 1] != '/') // make sure last char is a "/"
	    ret += "/";
	  ret += (string)LOG_DIRECTORY + "/";
	  setLocalLogging(false); // this is good
	}
      else
	{
	  setLocalLogging(true); // this is discouraged

	  char* working_dir = new char[MAX_PATH_LENGTH];
	  working_dir[0] = '\0';
	  getcwd(working_dir, MAX_PATH_LENGTH);
	  ret = (string)working_dir;
	  ret += "/" + (string)LOG_DIRECTORY + "/";
	  delete [] working_dir;

	  cerr << "*******************************************\n"
	       << "* Couldn't open file:\n"
	       << "*   " << DGCDIRFILE << "\n"
	       << "* \n"
	       << "* Logs will be place in " << ret << "\n"
	       << "* instead of the proper place!\n"
	       << "* Remote might not work!\n"
	       << "* See the README for how to create this file.\n"
	       << "*******************************************" << endl;
	}

      if (file.is_open())
	file.close();
    }
  else
    {
      // use absolute logging directory (muuuchhh easier....)
      ret = (string)LOG_DIRECTORY;
      if (ret[ret.length() - 1] != '/') // make sure last char is a "/"
	ret += "/";
    }

  printDebug("getLogDirectoryBase() returning " + ret, 2);
  return ret;
}



void CTimberClient::addRelFolder(string rel_folder_name)
{
  printDebug("addRelFolder called with string \"" + rel_folder_name + "\"", 2);
  char* complete_path = new char[MAX_PATH_LENGTH];
  complete_path[0] = '\0';

  string user = getUser();   //get username and ip
  string ip = getIPString();

  strcat(complete_path, user.c_str());
  strcat(complete_path, "@");
  strcat(complete_path, ip.c_str());
  strcat(complete_path, ":");

  strcat(complete_path, log_directory_base);

  if (SPACE_BEFORE_TIMEDIR)
    strcat(complete_path, " ");

  string rest_of_path = getDirFromTimestamp(true, false); // DO lock state, DON'T add log_directory_base
  strcat(complete_path, rest_of_path.c_str()); // stick date, time, name on there

  if (rel_folder_name != ".") // don't do anything if they added the current dir
    strcat(complete_path, rel_folder_name.c_str());
  if (complete_path[strlen(complete_path) - 1] != '/')
    strcat(complete_path, "/"); // make sure it has the last "/" on it
  
  //create the complete message to send
  char* message = new char[MESSAGE_LENGTH];
  message[0] = '\0';
  strcat(message, TAG_ADDFOLDER);
  strcat(message, complete_path);

  SendTimberString(sendSocket, message);

  delete [] complete_path;
  delete [] message;
}



int CTimberClient::blockUntilTime(unsigned long long unsigned_goal_p)
{
  /**
   * New approach: caculate real time left to sleep; using 10 * MAX_PLAYBACK_SLEEP
   *   if scale is 0
   *
   * then check cases:
   *
   * destination_time is not in the future => return immediately (1)
   * destination_time is in the future
   *   req amount <= max amount => sleep that amount then return (0)
   *   req amount >  max amount => sleep for max then recheck
   *
   * Variable_p is playback time, variable_r is real time
   */ 

  unsigned long long unsigned_cur_r;
  long long goal_p, cur_r, cur_p, sleep_left_r;
  goal_p = (long long)unsigned_goal_p;

  while (true)
    {
      // get time as SIGNED long long (so that we can subtract and get a negative answer)
      DGCgettime(unsigned_cur_r);
      cur_r = (long long)unsigned_cur_r;

      // calculate the real time left to sleep
      lockPlaybackState();
      // get playback time (just for debug output)
      cur_p = (long long)playback_state.playback_time_point + (long long)
	(playback_state.playback_speed * (double)(cur_r - playback_state.real_time_point));

      if (playback_state.playback_speed == 0)
	sleep_left_r = 10 * MAX_PLAYBACK_SLEEP;
      else
	{
	  // calculate the SIGNED real time left to sleep
	  sleep_left_r = (long long)(((double)(goal_p - (long long)playback_state.playback_time_point)) / \
				     playback_state.playback_speed) 
	    + playback_state.real_time_point - cur_r;
	}
      unlockPlaybackState();

      {
	char* debug_out = new char[512];
	debug_out [0] = '\0';
	sprintf(debug_out, "\nCTimberClient::    || (     Goal_p       -     cur_p         =   sleep_left_p,   sleep_left_r, actual_sleep) \nblockUntilTime():  || (%14.6f - %14.6f = %14.6f, %14.6f, %f)", (double)goal_p/1e6, (double)cur_p/1e6,
		(double)(goal_p - cur_p)/1e6, (double)sleep_left_r/1e6, 
		(sleep_left_r < 0 ? 0 : 
		 (sleep_left_r > MAX_PLAYBACK_SLEEP ? MAX_PLAYBACK_SLEEP : sleep_left_r))/1e6 );;
	
	string str(debug_out);
	printDebug(str, .5);
	delete [] debug_out;
      }

      if (sleep_left_r < 0)
	/** we're already past the point (whether we're playing forward or backward), so 
	 * return a 1 */
	return 1;
      else
	{
	  /** we do need to sleep for some positive amount */
	  if (sleep_left_r > MAX_PLAYBACK_SLEEP)
	    {
	      /** sleep for only MAX_PLAYBACK_SLEEP, rather than the actual amount, and
	       * wake up in MAX_PLAYBACK_SLEEP usec, to check again */
	      DGCusleep((unsigned long)MAX_PLAYBACK_SLEEP);
	      continue;
	    }
	  else
	    {
	      /** sleep for the exact amount of time and return */
	      DGCusleep((unsigned long)sleep_left_r);
	      return 0;
	    }
	}
      
    } // end of while(true)

}



/** Accessors */
double CTimberClient::getDebugLevel()
{
  lockDebug();
  double ret = debug_level;
  unlockDebug();
  return ret;
}

string CTimberClient::getModuleName()
{
  lockName();
  string ret = module_name;
  unlockName();
  return ret;
}

bool CTimberClient::getLoggingEnabled()
{
  lockState();
  bool enab = logging_enabled;
  unlockState();
  return enab && connectionStatus() && enoughDiskSpace();
}

int CTimberClient::getMyLoggingLevel()
{
  lockState();
  int ret = LEVEL_ARRAY_NAME[module_enum_value];
  unlockState();
  return ret;
}



unsigned long long CTimberClient::getPlaybackTime(bool lock_state, unsigned long long real_time)
{
  if (real_time == 0)
    DGCgettime(real_time);
  if (lock_state)
    lockPlaybackState();
  unsigned long long ret = playback_state.playback_time_point
    + (unsigned long long)(((double)((long long)real_time - (long long)playback_state.real_time_point)) 
			   * playback_state.playback_speed);
  if (lock_state)
    unlockPlaybackState();
  {
    ostringstream oss;
    oss << " ps.ptp = " << playback_state.playback_time_point
	<< " rt = " << real_time
	<< " ps.rtp = " << playback_state.real_time_point
	<< " ps.ts = " << playback_state.playback_speed
	<< endl;
    printDebug("CTimberClient::getPlaybackTime info:  " + oss.str(), 3);
  }
  return ret;
}



double CTimberClient::getPlaybackSpeed(bool lock_state)
{
  if (lock_state)
    lockPlaybackState();
  double ret = playback_state.playback_speed;
  if (lock_state)
    unlockPlaybackState();
  return ret;
}



/** Mutators */
void CTimberClient::setDebugLevel(double level)
{
  lockDebug();
  debug_level = level;
  unlockDebug();
}


void CTimberClient::setModuleName(string des_name)
{
  lockName();
  module_name[0] = '\0';
  strcpy(module_name, des_name.c_str());
  unlockName();
}



/** Bunch of mutex lock'ers and unlock'ers */
void CTimberClient::lockState() { DGClockMutex(&timber_state_mutex); }
void CTimberClient::unlockState() { DGCunlockMutex(&timber_state_mutex); }
void CTimberClient::lockName() { DGClockMutex(&module_name_mutex); }
void CTimberClient::unlockName() { DGCunlockMutex(&module_name_mutex); }
void CTimberClient::lockDebug() { DGClockMutex(&debug_level_mutex); }
void CTimberClient::unlockDebug() { DGCunlockMutex(&debug_level_mutex); }
void CTimberClient::lockLastSeen() { DGClockMutex(&server_last_seen_mutex); }
void CTimberClient::unlockLastSeen() { DGCunlockMutex(&server_last_seen_mutex); }
void CTimberClient::lockLocalLogging() { DGClockMutex(&local_logging_mutex); }
void CTimberClient::unlockLocalLogging() { DGCunlockMutex(&local_logging_mutex); }
void CTimberClient::lockUpdatedDir() { DGClockMutex(&updated_dir_mutex); }
void CTimberClient::unlockUpdatedDir() { DGCunlockMutex(&updated_dir_mutex); }
void CTimberClient::lockPlaybackState() { DGClockMutex(&playback_state_mutex); }
void CTimberClient::unlockPlaybackState() { DGCunlockMutex(&playback_state_mutex); }



/** a few private accessors and mutators */
unsigned long long CTimberClient::getLastSeen()
{
  lockLastSeen();
  unsigned long long ret = server_last_seen;
  unlockLastSeen();
  return ret;
}

void CTimberClient::setLastSeen(unsigned long long des_time)
{
  lockLastSeen();
  server_last_seen = des_time;
  unlockLastSeen();
}

bool CTimberClient::getLocalLogging()
{
  lockLocalLogging();
  bool ret = local_logging;
  unlockLocalLogging();
  return ret;
}

void CTimberClient::setLocalLogging(bool value)
{
  lockLocalLogging();
  local_logging = value;
  unlockLocalLogging();
}

bool CTimberClient::getUpdatedDir()
{
  lockUpdatedDir();
  bool ret = updated_dir;
  unlockUpdatedDir();
  return ret;
}

void CTimberClient::setUpdatedDir(bool value)
{
  lockUpdatedDir();
  updated_dir= value;
  unlockUpdatedDir();
}

void CTimberClient::writeCommandLine(string logDir)
{
  if(lastDirUsed != logDir)
    {
      stringstream fileName;
      struct dirent **namelist;
      int n; 
      n = scandir(RUN_DIR.c_str(), &namelist, chfile, alphasort);
      if (n < 0 )
	{
	  perror("Unable to scandir for CTimberClient");
	}
      else
	{
	  n--;
	  for(;n>=0;n--)
	    {
	      //create file name
	      fileName.str("");
	      fileName.clear();
	      fileName << RUN_DIR.c_str() <<"/"<< namelist[n]->d_name;      
	      if(ParseConfig((fileName.str().c_str())))
		{
		  logCommand(logDir.c_str());
		  break;
		  //nothing to do in loop here 
		}
	    }
	  cout << "Done with loop" << endl;
	}
      free(namelist);
      //nothing yet
      lastDirUsed = logDir;
   }

}
//---------------------------------------------------------------------------
//check if a dir entry is a file ending on .pid and if so parse it 
//
int chfile(const struct dirent *entry) 
{
  struct stat f_stat;
  char full_fname[MAX_BUF_SIZE];

  sprintf(full_fname,"%s/%s",CTimberClient::RUN_DIR.c_str(),entry->d_name); 
  lstat(full_fname, &f_stat);
  
  if (S_ISDIR(f_stat.st_mode) == 0)
    {
      if ((strlen(full_fname)>strlen(LOG_EXT)) && (strcmp(full_fname+strlen(full_fname)-strlen(LOG_EXT),".pid") == 0))
	{
	  return 1;
	}
      else
	{
	  return 0;
	}
    }
  return 0;
}

//---------------------------------------------------------------------------
//Parse a simple 2 line config file containing pid on first line and 
//executable with cmd line options on the second line 
//
bool CTimberClient::ParseConfig(const char *fname) 
{
  FILE * log =fopen(fname, "r");
  if(log) // Always test file open
    {
      pid_t pid;
      
      //char *b_start;
      fscanf(log, "%d",&pid);
      fgets(mon_strbuf, MAX_BUF_SIZE, log); // read line 1
      fgets(mon_strbuf, MAX_BUF_SIZE, log); // read the command
      fgets(bindir_buf, PATH_MAX, log); // read the bin directory
      fgets(key_buf, MAX_BUF_SIZE, log); // read the skynet key
      fclose(log);
      // delete  \n in all these line
      mon_strbuf[strlen(mon_strbuf)-1] = '\0';
      bindir_buf[strlen(bindir_buf)-1] = '\0';
      key_buf[strlen(key_buf)-1] = '\0';
      cout << endl << endl << pid << endl << endl << m_pid << endl << endl;
      if (pid == m_pid)
	{
	  return true;
	}
    }
  return false;
  
}

int CTimberClient::logCommand(const char * dir)
{
  stringstream fileName;
  //create file name
  fileName.str("");
  fileName.clear();
  fileName << dir << "commandline.log" << endl;      
  //#if DEBUG
  cout << fileName.str().c_str() << endl;
  //#endif
  ofstream runFile(fileName.str().c_str());
  if (! runFile) // Always test file open
    {
      cerr << "Error opening output file" << endl;
      return -1;
    }
  runFile << "[" <<  mon_strbuf <<  "]: run from " << bindir_buf << " with $SKYNET_KEY=" << key_buf << flush<< endl;
  runFile.close();
  return 0;  
}



bool CTimberClient::enoughDiskSpace()
{
  struct statfs disk_info;
  statfs(LOG_DIRECTORY, & disk_info);
  
  return disk_info.f_bavail > MIN_BLOCKS;
}
