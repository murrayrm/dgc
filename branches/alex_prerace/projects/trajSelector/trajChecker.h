#include "trajSelector.h"

class trajChecker: public trajSelector
{

 public:

  trajChecker(int num, int sn_key, bool silent_mode);
  
  void check();
  
  ~trajChecker();
};
