// GPS control and communications code
// Jeremy Gillula Summer '03
#include "gps.hh"
#include <iostream>


#ifdef SHITTY_SERIAL

#include <string.h>
int gps_com;
int gps_bytesread = 0;

#else

#include <sstream>
#include <SerialStream.h>
using namespace LibSerial;

SerialStream serial_port;

#endif

using namespace std;

// returns a character string explaining the current gps nav mode
char* gps_mode(int nav_mode)
{
  char *message;  
  if(nav_mode & 128) { 
    switch(nav_mode & 3) {
    case 0:
      message = "Non-Differential\0";
      break;
    case 1:
      message =  "Code Based dGPS\0";
      break;
    case 2:
      message =  "RCP Navigation\0";
      break;
    case 3:
      message =  "RTK Navigation\0";
      break;
    default:
      message =  "Unknown\0";
      break;
    }
  } else {
    switch(nav_mode & 127) {
    case 1:
      message =  "Navigation Init - Too few measurements\0";
      break;
    case 2:
      message =  "Navigation Init - PDOP too high\0";
      break;
    case 3:
      message =  "Fast Navigation Failure - Jump too far\0";
      break;
    case 4:
      message =  "Fast Navigation Failure - Numerical error\0";
      break;
    case 5:
      message =  "Slow Navigation - Residuals too large\0";
      break;
    case 6:
      message =  "Navigation Init - New initialization\0";
      break;
    case 7:
      message =  "Slow Navigation - Too few measurements\0";
      break;
    case 8:
      message =  "Slow Navigation - PDOP too high\0";
      break;
    case 9:
      message =  "Fast Navigation - Too few measurements\0";
      break;
    case 10:
      message =  "Navigation First Pass - No Velocity\0";
      break;
    case 11:
      message =  "Slow Navigation - Jump too large\0";
      break;
    case 12:
      message =  "Navigation COCOM Limits Reached\0";
      break;
    case 13:
      message =  "dGPS Navigation Required - Not Available\0";
      break;
    case 14:
      message =  "Navigation Init - Numerical Error\0";
      break;
    case 15:
      message =  "Navigation Init - Navigation Fault\0";
      break;
    default:
      message =  "Unknown error.\0";
      break;
    }
  }


    return message;
}





// Default constructor for the gpsDataWrapper class
gpsDataWrapper::gpsDataWrapper() {}

// Default destructor for the gpsDataWrapper class
gpsDataWrapper::~gpsDataWrapper() {}

//Prints the data in a gpsDataWrapper object to the screen
void gpsDataWrapper::print_to_screen() {
  //Self-explantory data
  cout << "\nGPS Week: " << data.gps_week;
  cout << "\nMilliseconds in Week: " << data.ms_in_week;
  cout << "\nSatellites Used: " << data.sats_used;
 
  //Print 10 decimal places (arbitrary!) to show we have accuracy
  cout << setprecision(10);
  cout << "\nLatitude: " << data.lat;
  cout << "\nLongitude: " << data.lng;

  //Decode the nav_mode byte based on the NavCom Reference Manual
  //Look up message B1 for more info
  int nav_mode = data.nav_mode;
  cout << "\nNav Mode: ";

  if(nav_mode & 128) {
    cout << "Navigation Valid";
    
    if(nav_mode & 64) cout << "\n3D Navigation";
    if(nav_mode & 32) cout << "\ndGPS Used Tropo";
    if(nav_mode & 16) cout << "\nClock Stable";
    if(nav_mode & 8) cout << "\n1PPS Output Valid";
    if(nav_mode & 4) cout << "\nDual Frequency Navigation";
    
    cout << "\nMode: ";
    switch(nav_mode & 3) {
    case 0:
      cout << "Non-Differential";
      break;
    case 1:
      cout << "Code Based dGPS";
      break;
    case 2:
      cout << "RCP Navigation";
      break;
    case 3:
      cout << "RTK Navigation";
      break;
    default:
      cout << "Unknown";
      break;
    }
  } else {
    cout << "Navigation Invalid";
    switch(nav_mode & 127) {
    case 1:
      cout << "\nNavigation Init - Too few measurements";
      break;
    case 2:
      cout << "\nNavigation Init - PDOP too high";
      break;
    case 3:
      cout << "\nFast Navigation Failure - Jump too far";
      break;
    case 4:
      cout << "\nFast Navigation Failure - Numerical error";
      break;
    case 5:
      cout << "\nSlow Navigation - Residuals too large";
      break;
    case 6:
      cout << "\nNavigation Init - New initialization";
      break;
    case 7:
      cout << "\nSlow Navigation - Too few measurements";
      break;
    case 8:
      cout << "\nSlow Navigation - PDOP too high";
      break;
    case 9:
      cout << "\nFast Navigation - Too few measurements";
      break;
    case 10:
      cout << "\nNavigation First Pass - No Velocity";
      break;
    case 11:
      cout << "\nSlow Navigation - Jump too large";
      break;
    case 12:
      cout << "\nNavigation COCOM Limits Reached";
      break;
    case 13:
      cout << "\ndGPS Navigation Required - Not Available";
      break;
    case 14:
      cout << "\nNavigation Init - Numerical Error";
      break;
    case 15:
      cout << "\nNavigation Init - Navigation Fault";
      break;
    default:
      cout << "\nUnknown error.";
      break;
    }
  }

  //More self-explanatory data
  cout << "\nEllipsoidal Height: " << data.ellipsoidal_height;
  
  cout << "\nAltitude: " << data.altitude;
  
  cout << "\nVelocity North: " << data.vel_n;
  cout << "\nVelocity East: " << data.vel_e;
  cout << "\nVelocity Up: " << data.vel_u;
  
  cout << "\nPosition FOM: " << data.position_fom;
  cout << "\nGDOP: " << data.gdop;
  cout << "\nPDOP: " << data.pdop;
  cout << "\nHDOP: " << data.hdop;
  cout << "\nVDOP: " << data.vdop;
  cout << "\nTDOP: " << data.tdop;
  
  cout << "\nTFOM: " << data.tfom;
  
  cout << "\nMaximum dGPS Correction Age: " << data.max_dgps_corr_age;
  
  //Decode the configuration based on the NavCom GPS reference manual
  //Again, look up message B1 (PVT) for more info
  int dgps_config = data.dgps_config;
  cout << "\ndGPS Configuration: ";
  if(dgps_config && 1) cout << "\nIgnore RTCM Code Input";
  if(dgps_config && 2) cout << "\nIgnore RTCM RTK Input";
  if(dgps_config && 4) cout << "\nIgnore WCT Corrections";
  if(dgps_config && 8) cout << "\nIgnore RTG Corrections";
  if(dgps_config && 16) cout << "\nIgnore WAAS Corrections";
  if(dgps_config && 32) cout << "\nIgnore CMR Input";
  if(dgps_config && 64) cout << "\nIgnore RTK Input";

  //Decode the extended nav_mode based on the NavCom GPS reference manual
  //Again, look up message B1 (PVT) for more info
  int extended_nav_mode = data.extended_nav_mode;
  cout << "\nExtended Nav Mode: ";
  switch(extended_nav_mode) {
  case 0:
    cout << "\nNon-differential";
    break;
  case 1:
    cout << "\nCode (RTCM Type 1 and 9)";
    break;
  case 2:
    cout << "\nCode, Single Frequency (WAAS)";
    break;
  case 3:
    cout << "\nCode, Dual Frequency (WAAS)";
    break;
  case 4:
    cout << "\nCode, Single Frequency (WCT)";
    break;
  case 5:
    cout << "\nCode/RCP, Dual Frequency (WCT)";
    break;
  case 6:
    cout << "\nCode, Single Frequency (RTG)";
    break;
  case 10:
    cout << "\nCode (RTCM Type 1 and 9) Dual Frequency SV's only";
    break;
  case 11:
    cout << "\nCode/RCP Dual Frequency (RTG)";
    break;
  case 12:
    cout << "\nCode, Single Frequency (RTK NCT Corrections)";
    break;
  case 13:
    cout << "\nCode, Single Frequency (RTCM Type 18 and 19)";
    break;
  case 14:
    cout << "\nCode, Single Frequency (RTCM Type 21 and 22)";
    break;
  case 15:
    cout << "\nCode, Single Frequency (CMR)";
    break;
  case 16:
    cout << "\nCode, Dual Frequency (RTK NCT Corrections)";
    break;
  case 17:
    cout << "\nCode, Dual Frequency (RTCM Type 18 and 19)";
    break;
  case 18:
    cout << "\nCode, Dual Frequency (RTCM Type 21 and 22)";
    break;
  case 19:
    cout << "\nCode, Dual Frequency (CMR)";
    break;
  case 20:
    cout << "\nRTK (RTK NCT Corrections)";
    break;
  case 21:
    cout << "\nRTK (RTCM Type 18 and 19)";
    break;
  case 22:
    cout << "\nRTK (RTCM Type 21 and 22)";
    break;
  case 23:
    cout << "\nRTK (CMR)";
    break;
  default:
    cout << "\nUnknown.";
  }
  
  //The last of the self-explanatory data
  cout << "\nAntenna Height Adjustment: " << data.antenna_height_adj;
  
  cout << "\nAdjust Veritcal Height? " << data.adjust_v_height;
  cout << "\n";
  }

//This is exactly the same code as the above print_to_screen function, only out_file has replaced cout
void gpsDataWrapper::write_to_file(char *filename) {
  //Open the file for appending
  ofstream out_file(filename, ios::out | ios::app | ios::binary);
  //And then do the exact same stuff as above

  out_file << "\nGPS Week: " << data.gps_week;
  out_file << "\nMilliseconds in Week: " << data.ms_in_week;
  out_file << "\nSatellites Used: " << data.sats_used;
  
  out_file << setprecision(10);
  out_file << "\nLatitude: " << data.lat;
  out_file << "\nLongitude: " << data.lng;
  
  int nav_mode = data.nav_mode;
  out_file << "\nNav Mode: ";

  if(nav_mode & 128) {
    out_file << "Navigation Valid";
    
    if(nav_mode & 64) out_file << "\n3D Navigation";
    if(nav_mode & 32) out_file << "\ndGPS Used Tropo";
    if(nav_mode & 16) out_file << "\nClock Stable";
    if(nav_mode & 8) out_file << "\n1PPS Output Valid";
    if(nav_mode & 4) out_file << "\nDual Frequency Navigation";
    
    out_file << "\nMode: ";
    switch(nav_mode & 3) {
    case 0:
      out_file << "Non-Differential";
      break;
    case 1:
      out_file << "Code Based dGPS";
      break;
    case 2:
      out_file << "RCP Navigation";
      break;
    case 3:
      out_file << "RTK Navigation";
      break;
    default:
      out_file << "Unknown";
      break;
    }
  } else {
    out_file << "Navigation Invalid";
    switch(nav_mode & 127) {
    case 1:
      out_file << "\nNavigation Init - Too few measurements";
      break;
    case 2:
      out_file << "\nNavigation Init - PDOP too high";
      break;
    case 3:
      out_file << "\nFast Navigation Failure - Jump too far";
      break;
    case 4:
      out_file << "\nFast Navigation Failure - Numerical error";
      break;
    case 5:
      out_file << "\nSlow Navigation - Residuals too large";
      break;
    case 6:
      out_file << "\nNavigation Init - New initialization";
      break;
    case 7:
      out_file << "\nSlow Navigation - Too few measurements";
      break;
    case 8:
      out_file << "\nSlow Navigation - PDOP too high";
      break;
    case 9:
      out_file << "\nFast Navigation - Too few measurements";
      break;
    case 10:
      out_file << "\nNavigation First Pass - No Velocity";
      break;
    case 11:
      out_file << "\nSlow Navigation - Jump too large";
      break;
    case 12:
      out_file << "\nNavigation COCOM Limits Reached";
      break;
    case 13:
      out_file << "\ndGPS Navigation Required - Not Available";
      break;
    case 14:
      out_file << "\nNavigation Init - Numerical Error";
      break;
    case 15:
      out_file << "\nNavigation Init - Navigation Fault";
      break;
    default:
      out_file << "\nUnknown error.";
      break;
    }
  }

  out_file << "\nEllipsoidal Height: " << data.ellipsoidal_height;
  
  out_file << "\nAltitude: " << data.altitude;
  
  out_file << "\nVelocity North: " << data.vel_n;
  out_file << "\nVelocity East: " << data.vel_e;
  out_file << "\nVelocity Up: " << data.vel_u;
  
  out_file << "\nPosition FOM: " << data.position_fom;
  out_file << "\nGDOP: " << data.gdop;
  out_file << "\nPDOP: " << data.pdop;
  out_file << "\nHDOP: " << data.hdop;
  out_file << "\nVDOP: " << data.vdop;
  out_file << "\nTDOP: " << data.tdop;
  
  out_file << "\nTFOM: " << data.tfom;
  
  out_file << "\nMaximum dGPS Correction Age: " << data.max_dgps_corr_age;
  
  int dgps_config = data.dgps_config;
  out_file << "\ndGPS Configuration: ";
  if(dgps_config && 1) out_file << "\nIgnore RTCM Code Input";
  if(dgps_config && 2) out_file << "\nIgnore RTCM RTK Input";
  if(dgps_config && 4) out_file << "\nIgnore WCT Corrections";
  if(dgps_config && 8) out_file << "\nIgnore RTG Corrections";
  if(dgps_config && 16) out_file << "\nIgnore WAAS Corrections";
  if(dgps_config && 32) out_file << "\nIgnore CMR Input";
  if(dgps_config && 64) out_file << "\nIgnore RTK Input";

  int extended_nav_mode = data.extended_nav_mode;
  out_file << "\nExtended Nav Mode: ";
  switch(extended_nav_mode) {
  case 0:
    out_file << "\nNon-differential";
    break;
  case 1:
    out_file << "\nCode (RTCM Type 1 and 9)";
    break;
  case 2:
    out_file << "\nCode, Single Frequency (WAAS)";
    break;
  case 3:
    out_file << "\nCode, Dual Frequency (WAAS)";
    break;
  case 4:
    out_file << "\nCode, Single Frequency (WCT)";
    break;
  case 5:
    out_file << "\nCode/RCP, Dual Frequency (WCT)";
    break;
  case 6:
    out_file << "\nCode, Single Frequency (RTG)";
    break;
  case 10:
    out_file << "\nCode (RTCM Type 1 and 9) Dual Frequency SV's only";
    break;
  case 11:
    out_file << "\nCode/RCP Dual Frequency (RTG)";
    break;
  case 12:
    out_file << "\nCode, Single Frequency (RTK NCT Corrections)";
    break;
  case 13:
    out_file << "\nCode, Single Frequency (RTCM Type 18 and 19)";
    break;
  case 14:
    out_file << "\nCode, Single Frequency (RTCM Type 21 and 22)";
    break;
  case 15:
    out_file << "\nCode, Single Frequency (CMR)";
    break;
  case 16:
    out_file << "\nCode, Dual Frequency (RTK NCT Corrections)";
    break;
  case 17:
    out_file << "\nCode, Dual Frequency (RTCM Type 18 and 19)";
    break;
  case 18:
    out_file << "\nCode, Dual Frequency (RTCM Type 21 and 22)";
    break;
  case 19:
    out_file << "\nCode, Dual Frequency (CMR)";
    break;
  case 20:
    out_file << "\nRTK (RTK NCT Corrections)";
    break;
  case 21:
    out_file << "\nRTK (RTCM Type 18 and 19)";
    break;
  case 22:
    out_file << "\nRTK (RTCM Type 21 and 22)";
    break;
  case 23:
    out_file << "\nRTK (CMR)";
    break;
  default:
    out_file << "\nUnknown.";
  }
  
  out_file << "\nAntenna Height Adjustment: " << data.antenna_height_adj;
  
  out_file << "\nAdjust Veritcal Height? " << data.adjust_v_height;
  out_file << "\n";
}

//This function decodes a GPS PVT message in pvt_buffer, and updates the data accordingy
//The decoding follows the spec in the NavCom reference manual under message B1
int gpsDataWrapper::update_gps_data(char *pvt_buffer) {
  unsigned int stx, preamble1, preamble2, command_id, msg_length, cksum, etx, check_cksum;
  unsigned char *bffrptr, *msgptr;
  bffrptr=(unsigned char*)pvt_buffer;

  //First, get the STX, preambles, length, and command_id of the message
  stx=get_u08(bffrptr);
  bffrptr++;
  preamble1=get_u08(bffrptr);
  bffrptr++;
  preamble2=get_u08(bffrptr);
  bffrptr++;
  command_id=get_u08(bffrptr);
  bffrptr++;
  msg_length=get_u16(bffrptr);
  bffrptr+=2;
  msgptr=bffrptr;
  bffrptr+=msg_length-4;
  cksum=get_u08(bffrptr);
  bffrptr++;
  etx=get_u08(bffrptr);
  bffrptr++;
  
  check_cksum=gps_cksum(msgptr-3, msg_length-1);

  if(stx==2 && preamble1==153 && preamble2==102 && etx==3 && command_id==GPS_MSG_PVT) {
    double lat, lng;
    int lsb, sats_used, num_sats_used;
    unsigned int mask;

    //All this decoding happens as the manual says it should
    data.gps_week = get_u16(msgptr);
    msgptr+=2;
    data.ms_in_week = get_u32(msgptr);
    msgptr+=4;
    sats_used = get_u32(msgptr);
    num_sats_used = 0;
    for(unsigned int i=0; i<32; i++) {
      mask = 1 << i;
      if(mask & sats_used) num_sats_used++;
    }
    data.sats_used = num_sats_used;
    msgptr+=4;
    lat = ((double)get_s32(msgptr))/(2048*3600);
    msgptr+=4;
    lng = ((double)get_s32(msgptr))/(2048*3600);
    msgptr+=4;
    lsb = get_u08(msgptr);
    data.lat = lat + ((double)(lsb >> 4))/(32768*3600);
    data.lng = lng + ((double)(lsb & 15))/(32768*3600);
    msgptr+=1;
    data.nav_mode = (char)get_u08(msgptr);
    msgptr+=1;
    data.ellipsoidal_height = ((float)get_s32(msgptr))/1024;
    msgptr+=4;
    data.altitude = ((float)get_s32(msgptr))/1024;
    msgptr+=4;
    data.vel_n = ((float)get_s24(msgptr))/1024;
    msgptr+=3;
    data.vel_e = ((float)get_s24(msgptr))/1024;
    msgptr+=3;
    data.vel_u = ((float)get_s24(msgptr))/1024;
    msgptr+=3;
    data.position_fom = get_u08(msgptr);
    msgptr+=1;
    data.gdop = ((float)get_u08(msgptr))/10;
    msgptr+=1;
    data.pdop = ((float)get_u08(msgptr))/10;
    msgptr+=1;
    data.hdop = ((float)get_u08(msgptr))/10;
    msgptr+=1;
    data.vdop = ((float)get_u08(msgptr))/10;
    msgptr+=1;
    data.tdop = ((float)get_u08(msgptr))/10;
    msgptr+=1;
    data.tfom = get_u08(msgptr);
    msgptr+=1;
    data.max_dgps_corr_age = ((float)get_u16(msgptr))/10;
    msgptr+=2;
    data.dgps_config = (char)get_u08(msgptr);
    msgptr+=1;
    data.extended_nav_mode = (char)get_u08(msgptr);
    msgptr+=1;
    data.antenna_height_adj =  get_s16(msgptr);
    msgptr+=2;
    data.adjust_v_height = get_u08(msgptr);
    msgptr+=1;
    return 1;
  } else {
    printf("\nInvalid PVT Message.");
    return -1;
  }
}

// Initialize GPS Unit to output PVT messages at the desired rate
int gps_open(char* port)
{
#ifdef SHITTY_SERIAL
  int return_val=-1;
  if(serial_open(com, B19200)!=-1) {
    //If the serial port has been opened successfully...
    return_val=1;

    // set gps com port
    gps_com = com;

    // set the nav rate
    //  gps_set_nav_rate(rate);
  }
  return return_val;

#else

	serial_port.Open(port);
	serial_port.SetBaudRate( SerialStreamBuf::BAUD_19200 ) ;
	if ( ! serial_port.good() )
	{
		cerr << "gps_open() error: Could not set the baud rate." << endl ;
		return -1;
	}
	serial_port.SetCharSize( SerialStreamBuf::CHAR_SIZE_8) ;
	if ( ! serial_port.good() )
	{
		cerr << "gps_open() error: Could not set the char size." << endl ;
		return -1;
	}

  return 1;
#endif
}

//Sends commands to the GPS to clear it of all messages
int gps_clear() {
  unsigned char* buffer;
  char* buffer_start;
  int return_val;

  //Create the buffer and make sure it's big enough
  buffer_start=(char*)malloc(100);
  buffer=(unsigned char*)buffer_start;

  //First send a command to stop output of LBM messages
  //Message C8

  //Clears all messages
  buffer=make_u08(0x14, buffer);
  //Arbitrary byte needed for some reason, according to the GPS manual
  buffer=make_u08(0xFF, buffer);
  //Send the message
  gps_write(0xC8, buffer_start, ((char*)buffer-buffer_start));

  //Next send a command to stop output of other messages
  //Clears all messages
  buffer=make_u08(0x14, buffer);
  //0 Data Request Blocks
  buffer=make_u08(0, buffer);
  //Arbitrary 0x71 needed according to GPS manual
  buffer=make_u08(0x71, buffer);
  //Arbitrary 0 needed according to GPS manual
  buffer=make_u08(0, buffer);
  //Arbitrary 0 needed according to GPS manual
  buffer=make_u16(0, buffer);
  //Send the message
  return_val=gps_write(0x20, buffer_start, ((char*)buffer-buffer_start));

  free(buffer_start);

  return return_val;
}

// set_gps_nav_rate: 
// rate: 0 - leaves the rate as it is
//       1,2,5 - 1Hz,2Hz,5Hz 
//       10,25 - 10Hz,25Hz if available (ours currently can get up to 10)
int gps_set_nav_rate(int rate) {
  unsigned char *buffer;
  char *buffer_start;

	int num_SV = 4;

  //Make the buffer big enough for the messages
  buffer_start=(char*)malloc(100);
  buffer=(unsigned char*)buffer_start;

  //Use default settings
  buffer=make_u08(0x05, buffer);
  //Use best available observables (default)
  buffer=make_u08(0, buffer);
  //Min number of SVs for solution
  buffer=make_u08(num_SV, buffer);
  //Max PDOP for a solution (default 0x64)
  buffer=make_u08(0x64, buffer);
  //Elevation Mask (default: 7)
  buffer=make_u08(7, buffer);
  //Constraint Flags
  buffer=make_u08(0x06, buffer);
  //Constrained Height
  buffer=make_u32(0, buffer);  //Should be r32, but since it doesn't matter, u32 is fine
  //Differential Mode Control

  buffer = make_u16(32961, buffer); // use all default settings, reserved bits are written as 1
  //dGPS Data Too Old Time
  buffer=make_u08(60, buffer);
  //dGPS Station ID
  buffer=make_u16(0, buffer);
  //Nav Update Rate
  buffer=make_u08(rate, buffer);

  return gps_write(0x49, buffer_start, ((char*)buffer-buffer_start));
}


//Sets the message command_id to output at rate
//For information on specifying rates other than predefined constants, see gps.h
int gps_set_msg_rate(int command_id, int rate) {
  unsigned char* buffer;
  char* buffer_start;

  //Make the buffer big enough for the messages
  buffer_start=(char*)malloc(100);
  buffer=(unsigned char*)buffer_start;

  //This only applies to this message
  buffer=make_u08(0x04, buffer);       // use default settings
  //buffer=make_u08(0x01, buffer);       // store in NVram
  //There's only 1 data request block
  buffer=make_u08(1, buffer);
  //The data request block has this command_id
  buffer=make_u08(command_id, buffer);
  //We are using the control port
  buffer=make_u08(2, buffer);
  //Output the rate according to the format specified in the reference manual
  //Look for message 20
  if(rate>0) {
    buffer=make_u16(rate*10, buffer);
  } else{
    switch(rate) {
      case GPS_MSG_RT_STOP:
        rate=0;
        break;
      case GPS_MSG_RT_02HZ:
        rate=0x0001;
        break;
      case GPS_MSG_RT_05HZ:
        rate=0x0002;
        break;
      case GPS_MSG_RT_10HZ:
        rate=0x0003;
        break;
      case GPS_MSG_RT_25HZ:
        rate=0x0004;
        break;
      case GPS_MSG_RT_50HZ:
        rate=0x0005;
        break;
      case GPS_MSG_RT_OEXT:
        rate=0x2000;
        break;
      case GPS_MSG_RT_ONCH:
        rate=0x4000;
        break;
      default:
        printf("\nIncorrect message rate: %d", rate);
        return -1;
    }
    buffer=make_u16(rate, buffer);
  }
 
 //Finish the message according to the spec for message 20
  buffer=make_u08(0x71, buffer);
  buffer=make_u08(0, buffer);
  buffer=make_u16(0, buffer);
  for (int i=0; i < 10; i++) {
    printf("gps buffer: %x\n", *(buffer-10+i));
  }
  //Send it
  //return 0;
  return gps_write(0x20, buffer_start, ((char*)buffer-buffer_start));
}

//Closes the GPS's serial port
int gps_close()
{
#ifdef SHITTY_SERIAL
  return serial_close(gps_com);
#else
	serial_port.Close();
  return 1;
#endif
}
//Don't need to calibrate
int gps_calibrate() {
  return 0;
}

//how do we check if the gps is ok?
int gps_ok(){
  return 0;
}

//return some data, probably if it is ok, position, when it got it's last message, message rate, nav rate.  Is anything else useful?
int gps_status(int status_type, struct gps_statud_data *status){
  return 0;
}
//just stop getting data.  should save the rate somehow, so that enable works.  should also say that we're paused, so that the loop doesn't
//try to read from the serial port, and never return.
int gps_pause(){
  return  gps_clear();
}

//stop getting data.
int gps_disable(){
  return gps_pause();
}

//should start again.  needs to let the loop read from serial again, and start with saved mesage rate.
int gps_resume(){
  return 0;
}
int gps_enable(){
  return 0;
}

//Gets a GPS message and stores it in orig_buffer.  Should have another function that will return immediately if there is no data
//in the serial buffer, or if it is not the start of a gps message.  Can also wait between tries because data comes in very slowly.  

int gps_read(char* orig_buffer, int buf_length)
{
#ifdef SHITTY_SERIAL

  //Make sure the buffer is big enough...
  //cout << "in gps read" << endl;
  if(buf_length < 8) {
    printf("\nBuffer for storing GPS message too small.");
    return -1;
  } else {
    // Clear out the gps serial buffer.  Chances are there is a lot of 
    // data piled up in the receive queue that we do not want, rather
    // we want the NEXT valid PVT. 
    serial_clean_buffer(gps_com);



    //Then get some variables going
    char *msg_start, *buffer;
    int length, msg_length;

    //Just wait until you get a GPS message
    //This is known to hang the thread under certain conditions
    while(true) {
      usleep(10);
      buffer=orig_buffer;
      msg_start=buffer;
      //Keep reading until you get the first byte in any GPS message, 0x02
      length = serial_read_blocking(gps_com, buffer, 1);

      /* Keep track of the number of bytes read */
      if (length > 0) gps_bytesread += length;
      if( PRINT_DEBUG_MESSAGES ) {
        cout << "  Did a serial_read of length = " << length;
        cout << "  buffer[0] = \"" << buffer[0] << "\"" << endl;  
      }
      // What does this mean?  
      if(length > 0 && *buffer==(char)2) {
	buffer++;
	//If you've gotten an 0x02, chances are it's a GPS message
	//So read the next five bytes
	serial_read_blocking(gps_com, buffer, 5); gps_bytesread += 5;
        if( PRINT_DEBUG_MESSAGES )
          cout << "  Did a serial_read_blocking." << endl;  
	buffer+=5;
	//Now check and see if it's a valid GPS message
	if(gps_msg_valid(msg_start)) {
          if( PRINT_DEBUG_MESSAGES )
	    cout << "  We got a valid_gps_msg." << endl;  
          
          //If it is, figure extract its length
	  msg_length = get_u16((unsigned char*)(msg_start+4)) - 2;
	  //Make sure the buffer they gave you is big enough
	  if(msg_length <= 0 || msg_length+6 > buf_length) {
		  if(msg_length <= 0) {
			  printf("Got zero-length PVT message?");
		  } else {
			  printf("\nBuffer for storing GPS message too small.");
		  }
	    return -1;
	  } else {
	    //And then read the rest of the message
	    serial_read_blocking(gps_com, buffer, msg_length);
	    gps_bytesread += msg_length;
	    return 1;
	  }
	}
      } 
    } // end while()
  }

#else

  //Make sure the buffer is big enough...
  if(buf_length < 8)
	{
    cerr << "gps.cc: gps_read(): Buffer for storing GPS message too small." << endl;
    return -1;
	}

	// keep going until we get a valid message
	while(true)
	{
		// Clear out the gps serial buffer.  Chances are there is a lot of 
		// data piled up in the receive queue that we do not want, rather
		// we want the NEXT valid PVT. 
		serial_port.flush();
		serial_port.sync();

		char dummy[1024];
		serial_port.getline(dummy, 1024, '\x2');

		if (!serial_port) {
		  cerr << "Gps read timed out" << endl;
		  return -1;
		}

		orig_buffer[0] = '\x2';

		serial_port.read(orig_buffer+1, 5);

		if (!serial_port) {
		  cerr << "Gps read timed out" << endl;
		  return -1;
		}

		if(!gps_msg_valid(orig_buffer))
		{
			cerr << "gps.cc: gps_read(): invalid gps message" << endl;
			return 0;
		}

		int msg_length = get_u16((unsigned char*)&orig_buffer[4]) - 2;
		if(msg_length <= 0)
		{
			cerr << "gps.cc: gps_read(): Got zero-length PVT message?" << endl;
			return 0;
		}
		if(msg_length+6 > buf_length)
		{
			cerr << "gps.cc: gps_read(): Buffer for storing GPS message too small." << endl;
			return 0;
		}

		serial_port.read(orig_buffer+6, msg_length);

		if (!serial_port) {
		  cerr << "Gps read timed out" << endl;
		  return -1;
		}

		return 1;
	}
#endif
} // end gps_read()

int gps_read_pvt(char* orig_buffer, int buf_length) {
  int response;
  //cerr << "  started in get_gps_pvt_msg" << endl;  
#ifdef SHITTY_SERIAL
  serial_clean_buffer(gps_com);
#else
  serial_port.sync();
#endif

  do {
    //cout << "  In the do 1." << endl;  
    response = gps_read(orig_buffer, buf_length);
    //cout << "  In the do 2." << endl;  
  } while(gps_msg_type(orig_buffer) != GPS_MSG_PVT);
  //cout << "got pvt... supposedly" << endl;
  return response;
}

//Extracts the GPS command_id from the GPS message stored in buffer
//Follows the spec for GPS messages in the NavCom reference manual
int gps_msg_type(char * buffer) {
  return get_u08((unsigned char*)(buffer+3));
}

//Prints the GPS message stored in orig_buffer to the screen
//Only really useful for debugging since it prints in hex
int gps_print_msg(char *orig_buffer) {
  unsigned char *buffer;
  int length;

  buffer=(unsigned char*)orig_buffer;

  //Print the STX, preambles, command ID, and length
  printf("\n\nSTX: %.2X", *buffer);
  printf("\nPre1: %.2X", *(buffer+1));
  printf("\nPre2: %.2X", *(buffer+2));
  printf("\nID: %.2X", *(buffer+3));
  printf("\nLen: %.2X %.2X", *(buffer+4), *(buffer+5));
  //THen extract the length
  length = get_u16(buffer+4)-4;
  printf("\n");
  //Print out all the data in the message
  for(int i=0; i<length; i++) {
    printf("%.2X ", *(buffer+6+i));
  }
  //And finish up with the checksum and ETX
  printf("\nCKS: %.2X", *(buffer+length+6));
  printf("\nETX: %.2X", *(buffer+length+7));
	return 0;
}

//Writes a GPS message of command_id with data stored in orig_msgptr of length msg_length to the GPS unit
//The function follows the spec in the NavCom reference manual
int gps_write(int command_id, char *orig_msgptr, int msg_length) {
	unsigned int cksum;
	unsigned char *bffrptr;
	char *msgptr;
	int return_val=0;
     printf("length: %d\n", msg_length);fflush(stdout);
	//Make sure the buffer is big enough
	bffrptr=(unsigned char*)malloc(msg_length+8);
	msgptr=(char*)bffrptr;

	//STX
	bffrptr = make_u08(2, bffrptr);
	//Preamble1
	bffrptr = make_u08(153, bffrptr);
	//Preamble2
	bffrptr = make_u08(102, bffrptr);

	//Command ID
	bffrptr = make_u08(command_id, bffrptr);
	//Message Length
	bffrptr = make_u16(msg_length+4, bffrptr);

	//Message
	memcpy(bffrptr, orig_msgptr, msg_length);
	bffrptr+=msg_length;

	//Checksum
	cksum = gps_cksum(bffrptr-msg_length-3, msg_length+3);
	bffrptr = make_u08(cksum, bffrptr);

	//ETX
	bffrptr = make_u08(3, bffrptr);

#ifdef SHITTY_SERIAL

#ifndef SERIAL2
	//Write the message
	return_val = serial_write(gps_com, msgptr, msg_length+8, SerialBlock);
#else
	//Write the message
	return_val = serial_write(gps_com, msgptr, msg_length+8);
#endif

#else
	serial_port.write(msgptr, msg_length+8);
	return_val = msg_length+8;
#endif

	//Free the memory
	free(msgptr);

	return return_val;
}

//Checks to see if the string in orig_msg is a valid GPS message based on the spec in the reference
bool gps_msg_valid(char* orig_msg) {
	unsigned char* msgptr;

	msgptr=(unsigned char*)orig_msg;

	return get_u08(msgptr)==2 && get_u08(msgptr+1)==153 && get_u08(msgptr+2)==102;
}

bool gps_pvt_valid(char* orig_msg) {
  unsigned char* msgptr;

  //First check to make sure it's a valid GPS message in general
  if(gps_msg_valid(orig_msg)) {
    msgptr=(unsigned char*)orig_msg;
    
    //Than make sure that the nav valid flag is true
    //cout << "\nvalid? " << (get_u08(msgptr+25) & 128) << endl;
    //return true;
    return (get_u08(msgptr+25) & 128);
  }

  return false;
}

//Generates a checksum for a GPS message in buffer of length length based on the reference
unsigned int gps_cksum(unsigned char* buffer, int length) {
  unsigned int result=0;

  for(int i=0; i<length; i++) {
    result = result ^ *(buffer+i);
  }

  return result;
}

//The following functions use a bunch of bit-wise arithmetic to decode and encode
//integers of a specific bit-length
unsigned int get_u08(unsigned char* ptr) {
	return *ptr;
}


unsigned int get_u16(unsigned char* ptr) {
	int result=0;
	result = result | *ptr;
	result = result | (*(ptr+1) << 8);
	return result;
}

unsigned int get_u24(unsigned char* ptr) {
	int result=0;
	result = result | *ptr;
	result = result | (*(ptr+1) << 8);
	result = result | (*(ptr+2) << 16);
	return result;
}

unsigned int get_u32(unsigned char* ptr) {
	int result=0;
	result = result | *ptr;
	result = result | (*(ptr+1) << 8);
	result = result | (*(ptr+2) << 16);
	result = result | (*(ptr+3) << 24);
	return result;
}

int get_s08(unsigned char* ptr) {
	int result = *ptr & 127;
	if(*ptr >> 7) result-=128;
	return result;
}

int get_s16(unsigned char* ptr) {
	int result = *ptr + (*(ptr+1) << 8) & 32767;
	if(*(ptr+1) >> 7) result-=32768;
	return result;
}

int get_s24(unsigned char* ptr) {
	int result = *ptr + (*(ptr+1) << 8) + (*(ptr+2) << 16) & 8388607;
	if(*(ptr+2) >> 7) result-=8388608;
	return result;
}

int get_s32(unsigned char* ptr) {
	int result = *ptr + (*(ptr+1) << 8) + (*(ptr+2) << 16) + (*(ptr+3) << 24) & 2147483647;
	if(*(ptr+3) >> 7)
		result ^= 0x80000000;
	return result;
}

unsigned char* make_u08(unsigned int val, unsigned char* ptr) {
	*ptr= (unsigned char) val;
	return ptr+1;
}

unsigned char* make_u16(unsigned int val, unsigned char* ptr) {
	*(unsigned short*)ptr= (unsigned short) val;
	return ptr+2;
}

//unsigned char* make_u24(unsigned int val, unsigned char* ptr) {
//	*(unsigned int*)ptr=val;
//	return ptr+3;
//}

unsigned char* make_u32(unsigned int val, unsigned char* ptr) {
	*(unsigned int*)ptr=val;
	return ptr+4;
}


unsigned char* make_s08(int val, unsigned char* ptr) {
	*(char*)ptr= (char) val;
	return ptr+1;
}


unsigned char* make_s16(int val, unsigned char* ptr) {
	*(short*)ptr= (short) val;
	return ptr+2;
}

//unsigned char* make_s24(int val, unsigned char* ptr) {
//	*(int*)ptr=val;
//	return ptr+3;
//}

unsigned char* make_s32(int val, unsigned char* ptr) {
	*(int*)ptr=val;
	return ptr+4;
}
