#include "stereoSource.hh"
#include <math.h>
#include <iostream>
#include <string>
using namespace std;

stereoSource::stereoSource()
  : _pidControl(0.15,0,0,1.0) {
  DGCcreateMutex(&cameraMutex);
  _currentSourceType = SOURCE_UNASSIGNED;
  _pairIndex = -1;
  _exposureControlStatus = 0;
  //_exposureRefVal= 128.0;
  _exposureVal_left = -1.0;
  _exposureVal_right = -1.0;
  _pidControlStatus = 0;
  memset(_currentFilename, 0, 256);
  memset(_currentFileType, 0, 10);
 }


stereoSource::~stereoSource() {
  DGCdeleteMutex(&cameraMutex);
}


int stereoSource::init(int verboseLevel, char *camIDFilename, char *logFilename, char *logFileType, char * svsFilename) {
 int sub_window_x_off=0, sub_window_y_off=0, sub_window_width=0, sub_window_height=0;
  FILE *camIDFile = NULL;
  char leftCamID[256], rightCamID[256];
  char strFormatLeft[256], strModeLeft[256], strSpeedLeft[256], strFramerateLeft[256];
  char strFormatRight[256], strModeRight[256], strSpeedRight[256], strFramerateRight[256];
  int formatLeft, modeLeft, speedLeft, framerateLeft, colorLeft;
  int formatRight, modeRight, speedRight, framerateRight, colorRight;

  int numNodes;
  int i;
  char idstr[9];
  int numCameras;
  quadlet_t cam_ids[2];
  dc1394_camerainfo info[2];   
  nodeid_t * camera_nodes;
  raw1394handle_t * handle_ptr;
  dc1394bool_t has_white_balance;


  //Initialize some basic variables
  _verboseLevel = verboseLevel;
  _currentSourceType = SOURCE_CAMERAS;
  memcpy(_currentFilename, logFilename, strlen(logFilename));
  memcpy(_currentFileType, logFileType, strlen(logFileType));
  _pairIndex = 0;  
  
  //Initialize some camera variables from file
  camIDFile = fopen(camIDFilename, "r");
  if(camIDFile != NULL) {
    //Scan them from the file
    fscanf(camIDFile, "leftCamID: %s\nrightCamID: %s\nformatLeft: %s\nmodeLeft: %s\nspeedLeft: %s\nframerateLeft: %s\ncolorLeft: %d\nformatRight: %s\nmodeRight: %s\nspeedRight: %s\nframerateRight: %s\ncolorRight: %d\n",
	   leftCamID,
	   rightCamID,
	   strFormatLeft,
	   strModeLeft,
	   strSpeedLeft,
	   strFramerateLeft, 
	   &colorLeft, 
	   strFormatRight,
	   strModeRight,
	   strSpeedRight,
	   strFramerateRight,
	   &colorRight);

    FILE *svsFile = NULL;
    svsFile = fopen(svsFilename, "r");
    int a, b, c, d, e, f, g, h, i, j, k, l;
    if(svsFile!= NULL){
      fscanf(svsFile, "corrsize: %d\nthresh: %d\nndisp: %d\noffx: %d\nunique: %d\nwidth: %d\nheight: %d\nuse_sub_window: %d\nsub_window_x_offset: %d\nsub_window_y_offset: %d\nsub_window_width: %d\nsub_window_height: %d\nmulti: %d\nblobActive: %d\nblobTresh: %d\nblobDispTol: %d", &a, &b, &c, &d, &e, &f, &g, &h, &sub_window_x_off, &sub_window_y_off, &sub_window_width, &sub_window_height, &i, &j, &k, &l);
    }
    else{
      printf("couldn't find svsFile..\n");
    }
    
    FILE *expFile = NULL;
    expFile =fopen("./config/stereovision/ExpParams.ini", "r");
    if(expFile != NULL) {
      fscanf(expFile, "expRefVal: %lf", &_exposureRefVal);
    }
    else{
      printf("Couldn't find expFile \n");
      usleep(1000*1000*10);
    }

    //And set them
    if(strcmp(strFormatLeft, "FORMAT_SVGA_NONCOMPRESSED_1")==0) {
      formatLeft = FORMAT_SVGA_NONCOMPRESSED_1;
    } else if(strcmp(strFormatLeft, "FORMAT_VGA_NONCOMPRESSED")==0) {
      formatLeft = FORMAT_VGA_NONCOMPRESSED;
    } else {
      fprintf(stderr, "%s [%d]: Unknown formatLeft %s in the camera ID file!  Quitting...\n", __FILE__, __LINE__, strFormatLeft);
    }

    if(strcmp(strModeLeft, "MODE_1024x768_MONO")==0) {
      modeLeft = MODE_1024x768_MONO;
    } else if(strcmp(strModeLeft, "MODE_640x480_MONO")==0) {
      modeLeft = MODE_640x480_MONO;
    } else {
      fprintf(stderr, "%s [%d]: Unknown modeLeft %s in the camera ID file!  Quitting...\n", __FILE__, __LINE__, strModeLeft);
    }	   

    if(strcmp(strSpeedLeft, "SPEED_400")==0) {
      speedLeft = SPEED_400;
    } else {
      fprintf(stderr, "%s [%d]: Unknown speedLeft %s in the camera ID file!  Quitting...\n", __FILE__, __LINE__, strSpeedLeft);
    }

    if(strcmp(strFramerateLeft, "FRAMERATE_15")==0) {
      framerateLeft = FRAMERATE_15;
    } else if(strcmp(strFramerateLeft, "FRAMERATE_30")==0) {
      framerateLeft = FRAMERATE_30;
    } else {
      fprintf(stderr, "%s [%d]: Unknown framerateLeft %s in the camera ID file!  Quitting...\n", __FILE__, __LINE__, strFramerateLeft);
    }

    if(strcmp(strFormatRight, "FORMAT_SVGA_NONCOMPRESSED_1")==0) {
      formatRight = FORMAT_SVGA_NONCOMPRESSED_1;
    } else if(strcmp(strFormatRight, "FORMAT_VGA_NONCOMPRESSED")==0) {
      formatRight = FORMAT_VGA_NONCOMPRESSED;
    } else {
      fprintf(stderr, "%s [%d]: Unknown formatRight %s in the camera ID file!  Quitting...\n", __FILE__, __LINE__, strFormatRight);
    }

    if(strcmp(strModeRight, "MODE_1024x768_MONO")==0) {
      modeRight = MODE_1024x768_MONO;
    } else if(strcmp(strModeRight, "MODE_640x480_MONO")==0) {
      modeRight = MODE_640x480_MONO;
    } else {
      fprintf(stderr, "%s [%d]: Unknown modeRight %s in the camera ID file!  Quitting...\n", __FILE__, __LINE__, strModeRight);
    }	   

    if(strcmp(strSpeedRight, "SPEED_400")==0) {
      speedRight = SPEED_400;
    } else {
      fprintf(stderr, "%s [%d]: Unknown speedRight %s in the camera ID file!  Quitting...\n", __FILE__, __LINE__, strSpeedRight);
    }

    if(strcmp(strFramerateRight, "FRAMERATE_15")==0) {
      framerateRight = FRAMERATE_15;
    } else if(strcmp(strFramerateRight, "FRAMERATE_30")==0) {
      framerateRight = FRAMERATE_30;
    } else {
      fprintf(stderr, "%s [%d]: Unknown framerateRight %s in the camera ID file!  Quitting...\n", __FILE__, __LINE__, strFramerateRight);
    }
  } else {
    fprintf(stderr, "%s [%d]: Couldn't find camera ID file %s, quitting...\n", __FILE__, __LINE__, camIDFilename);
    exit(1);
    return stereoSource_NO_FILE;
  }    

  
  // Open ohci and asign handle to it
  handle_ptr = &handle;
  *handle_ptr = dc1394_create_handle(0);
  if (*handle_ptr==NULL)
  {
    fprintf( stderr, "Unable to aquire a raw1394 handle\n\n"
             "Please check \n"
	     "  - if the kernel modules `ieee1394',`raw1394' and `ohci1394' are loaded \n"
	     "  - if you have read/write access to /dev/raw1394\n\n");
    return(-1);
    }

  //  get the camera nodes
  numNodes = raw1394_get_nodecount(*handle_ptr);
  camera_nodes = dc1394_get_camera_nodes(*handle_ptr, &numCameras, 0);

  if(numCameras!=MAX_CAMERAS) {
    fprintf( stderr, "%d cameras found, not %d\n",numCameras, MAX_CAMERAS);
    dc1394_destroy_handle(*handle_ptr);
    return(-1);
  }

  for ( i = 0; i < 2; i++ ) {
    dc1394_get_camera_info(*handle_ptr, camera_nodes[i], &info[i]);
    cam_ids[i] = info[i].euid_64 & 0xffffffff;
    sprintf( idstr, "0x%08X",cam_ids[i] );

    if(strcmp(idstr, leftCamID)==0) {
      node2cam[i] = LEFT_CAM;
    } else if(strcmp(idstr, rightCamID)==0) {
      node2cam[i] = RIGHT_CAM;
    } else {
      fprintf(stderr, "%s [%d]: Camera %s not known as left (%s) or right (%s)\n", __FILE__, __LINE__, idstr, leftCamID, rightCamID);
      dc1394_destroy_handle(*handle_ptr);
      return stereoSource_UNKNOWN_ERROR;
    }

    dc1394_is_feature_present(handle, camera_nodes[i], FEATURE_WHITE_BALANCE, &has_white_balance);
    if(has_white_balance == DC1394_TRUE) dc1394_feature_on_off(handle, camera_nodes[i], FEATURE_WHITE_BALANCE, 0);
  }

  /*
  // setup capture
  for ( i = 0; i < numCameras; i++ ) {
    if ( dc1394_dma_setup_capture(*handle_ptr, camera_nodes[i],
				  i, // channel
				  formatLeft,
				  modeLeft,
				  speedLeft,
				  framerateLeft,
				  10,
				  1,
				  "/dev/video1394/0",
				  &cameras[i])!=DC1394_SUCCESS) {
	fprintf( stderr,"unable to setup camera-\n"
		 "check line %d of %s to make sure\n"
		 "that the video mode,framerate and format are\n"
		 "supported by your camera\n",
		 __LINE__,__FILE__);
	
	stop();
	return(-1);
      }
    dc1394_start_iso_transmission(handle, camera_nodes[i]);
  }
  */

    if ( dc1394_dma_setup_capture(*handle_ptr, camera_nodes[node2cam[LEFT_CAM]],
				  LEFT_CAM, // channel
				  formatLeft,
				  modeLeft,
				  speedLeft,
				  framerateLeft,
				  10,
				  1,
				  "/dev/video1394/0",
				  &cameras[node2cam[LEFT_CAM]])!=DC1394_SUCCESS) {
	fprintf( stderr,"unable to setup camera-\n"
		 "check line %d of %s to make sure\n"
		 "that the video mode,framerate and format are\n"
		 "supported by your camera\n",
		 __LINE__,__FILE__);
	
	stop();
	return(-1);
      }
    dc1394_start_iso_transmission(handle, camera_nodes[node2cam[LEFT_CAM]]);

    if ( dc1394_dma_setup_capture(*handle_ptr, camera_nodes[node2cam[RIGHT_CAM]],
				  RIGHT_CAM, // channel
				  formatRight,
				  modeRight,
				  speedRight,
				  framerateRight,
				  10,
				  1,
				  "/dev/video1394/0",
				  &cameras[node2cam[RIGHT_CAM]])!=DC1394_SUCCESS) {
	fprintf( stderr,"unable to setup camera-\n"
		 "check line %d of %s to make sure\n"
		 "that the video mode,framerate and format are\n"
		 "supported by your camera\n",
		 __LINE__,__FILE__);
	
	stop();
	return(-1);
      }
    dc1394_start_iso_transmission(handle, camera_nodes[node2cam[RIGHT_CAM]]);

    /*
    dc1394_feature_set allFeatures;
    dc1394_get_camera_feature_set(handle, camera_nodes[node2cam[LEFT_CAM]], &allFeatures);
    dc1394_print_feature_set(&allFeatures);
    */

    dc1394_auto_on_off(handle, camera_nodes[0], FEATURE_EXPOSURE, 1);
    dc1394_auto_on_off(handle, camera_nodes[1], FEATURE_EXPOSURE, 1);
    dc1394_auto_on_off(handle, camera_nodes[0], FEATURE_SHUTTER, 1);
    dc1394_auto_on_off(handle, camera_nodes[1], FEATURE_SHUTTER, 1);
    dc1394_auto_on_off(handle, camera_nodes[0], FEATURE_GAIN, 1);
    dc1394_auto_on_off(handle, camera_nodes[1], FEATURE_GAIN, 1);



  /*
  // set trigger mode - I'm not exactly sure what this does or if it is
  // strictly necessary
  for ( i = 0; i < numCameras; i++ ) {
    if( dc1394_set_trigger_mode(*handle_ptr, cameras[i].node, TRIGGER_MODE_0)
        != DC1394_SUCCESS)
    {
      fprintf( stderr, "unable to set camera %d trigger mode\n", i);
    }
  }
  */
  // having the cameras start sending data
  /*
  for ( i = 0; i < 2; i++ ) {
    if ( dc1394_start_iso_transmission(*handle_ptr,cameras[i].node)
	 !=DC1394_SUCCESS)
      {
	fprintf( stderr, "Unable to start camera %d iso transmission\n", i);
        stop();
	return(-1);
      }
  }
  */
  pairSize = cvSize(cameras[0].frame_width, cameras[0].frame_height);
  for(int i=0; i<MAX_CAMERAS; i++) {
    _images[i] = cvCreateImage(pairSize, IPL_DEPTH_8U, 1);
    _temp[i] = cvCreateImage(pairSize, IPL_DEPTH_8U, 1);
  }

  if(colorLeft) {
    _colorImages[node2cam[LEFT_CAM]] = cvCreateImage(pairSize, IPL_DEPTH_8U, 3);
  } else {
    _colorImages[node2cam[LEFT_CAM]] = NULL;
  }

  if(colorRight) {
    _colorImages[node2cam[RIGHT_CAM]] = cvCreateImage(pairSize, IPL_DEPTH_8U, 3);
  } else {
    _colorImages[node2cam[RIGHT_CAM]] = NULL;
  }
  if(sub_window_width!=0 && sub_window_height!=0){
    startExposureControl(cvRect(sub_window_x_off,sub_window_y_off,sub_window_width,sub_window_height));
  }
  return stereoSource_OK;
}

int stereoSource::init(int verboseLevel, int width, int height, char *baseFilename, char *baseFileType, int start) {
  _verboseLevel = verboseLevel;
  _currentSourceType = SOURCE_FILES;
  memcpy(_currentFilename, baseFilename, strlen(baseFilename));
  memcpy(_currentFileType, baseFileType, strlen(baseFileType));
  _pairIndex = start;

  pairSize = cvSize(width, height);
  for(int i=0; i<MAX_CAMERAS; i++) {
    _images[i] = cvCreateImage(pairSize, IPL_DEPTH_8U, 1);
    _temp[i] = cvCreateImage(pairSize, IPL_DEPTH_8U, 1);
  }

  node2cam[LEFT_CAM] = LEFT_CAM;
  node2cam[RIGHT_CAM] = RIGHT_CAM;

  return stereoSource_OK;
}


int stereoSource::stop() {
  switch(_currentSourceType) {
  case SOURCE_CAMERAS:
    int i;
    
    // stop data transmission
    for ( i = 0; i < 2; i++ ) {
      if(dc1394_dma_unlisten(handle, &cameras[i])!=DC1394_SUCCESS)
      //      if (dc1394_stop_iso_transmission(handle,cameras[i].node)!=DC1394_SUCCESS)
	{
	  fprintf( stderr, "Couldn't stop camera%d\n", i);
	}
    }
    nodeid_t * camera_nodes;
    int numCameras;
    camera_nodes = dc1394_get_camera_nodes(handle, &numCameras, 0);

    // close cameras
    for ( i = 0; i < 2; i++ ) {
      dc1394_dma_release_camera(handle,&cameras[i]);
      dc1394_stop_iso_transmission(handle, camera_nodes[i]);
    }
    dc1394_destroy_handle(handle);
    
    return stereoSource_OK;
    break;
  case SOURCE_FILES:
    return stereoSource_OK;
    break;
  default:
    fprintf(stderr, "%s [%d]: Unknown source type, couldn't close\n", __FILE__, __LINE__);
    return stereoSource_UNKNOWN_SOURCE;
    break;
  }

  return stereoSource_UNKNOWN_ERROR;
}

 
unsigned long long stereoSource::grab() {
  double exposureScale_left;
  double exposureScale_right;
  unsigned long long timestamp;

  switch(_currentSourceType) {
  case SOURCE_CAMERAS:
    DGClockMutex(&cameraMutex);
    if ( dc1394_dma_multi_capture(cameras,2)!=DC1394_SUCCESS)
      {
	fprintf( stderr, "Unable to capture frames\n" );
	stop();
	DGCunlockMutex(&cameraMutex);
	return(0);
      }
		DGCgettime(timestamp);

    for(int i=0; i<MAX_CAMERAS; i++) {
      cvSetData(_temp[i], (char *)cameras[i].capture_buffer, cameras[i].frame_width);
      if(_colorImages[i] == NULL) {
	cvReleaseImage(&_images[i]);
	_images[i] = cvCloneImage(_temp[i]);	
      } else {
	cvCvtColor(_temp[i], _colorImages[i], CV_BayerRG2RGB);
	cvCvtColor(_colorImages[i], _images[i], CV_RGB2GRAY);
      }

      dc1394_dma_done_with_buffer(&cameras[i]);
    }
    DGCunlockMutex(&cameraMutex);
    _pairIndex++;
              
    if(_exposureControlStatus) {
      exposureScale_left = getFeatureValExposure(LEFT_CAM) + 0.1*((_exposureRefVal - getAvgROIBrightness(LEFT_CAM))/_exposureRefVal); 
      //exposureScale_right = getFeatureValExposure(RIGHT_CAM) + 0.1*((_exposureRefVal - getAvgROIBrightness(RIGHT_CAM))/_exposureRefVal);
      
      if(exposureScale_left>1.0) exposureScale_left = 1.0;
      if(exposureScale_left<0.0) exposureScale_left = 0.0;
      //if(exposureScale_right>1.0) exposureScale_right = 1.0;
      //if(exposureScale_right<0.0) exposureScale_right = 0.0;
      _exposureVal_left = exposureScale_left;
      //_exposureVal_right = exposureScale_right;
      setFeatureValExposure(exposureScale_left, LEFT_CAM);
      //setFeatureValExposure(exposureScale_right, RIGHT_CAM);
    }
       
    return timestamp;
    break;

  case SOURCE_FILES:
    if(strcmp(_currentFilename, "")==0 || strcmp(_currentFileType, "")==0) {
      fprintf(stderr, "%s [%d]: Source is set to files, but filename or type is blank!\n", __FILE__, __LINE__);
      return 0;
    } else {
      timestamp = grab(_currentFilename, _currentFileType, _pairIndex);
      _pairIndex++;
      return timestamp;
    }
    break;
  default:
    fprintf(stderr, "%s [%d]: Unknown source type, couldn't grab\n", __FILE__, __LINE__);
    return 0;
    break;
  }

  return 0;
}


unsigned long long stereoSource::grab(char *baseFilename, char *baseFileType, int num) {
  char leftFilename[256], rightFilename[256];
  FILE *leftTestFile=NULL, *rightTestFile=NULL;

  sprintf(leftFilename, "%s%.5d-L.%s", baseFilename, num, baseFileType);
  sprintf(rightFilename, "%s%.5d-R.%s", baseFilename, num, baseFileType);
  _pairIndex = num;

  leftTestFile = fopen(leftFilename, "r");
  rightTestFile = fopen(rightFilename, "r");
  if(leftTestFile != NULL && rightTestFile != NULL) {
    fclose(leftTestFile);
    fclose(rightTestFile);
    
    for(int i=0; i<MAX_CAMERAS; i++) {
	cvReleaseImage(&_images[i]);
    }

    _images[node2cam[LEFT_CAM]] = cvLoadImage(leftFilename, -1);
    _images[node2cam[RIGHT_CAM]] = cvLoadImage(rightFilename, -1);

    if(_images[node2cam[LEFT_CAM]] == NULL || _images[node2cam[RIGHT_CAM]] == NULL) {
      fprintf(stderr, "%s [%d]: Error loading %s, %s into OpenCV object!\n", __FILE__, __LINE__, leftFilename, rightFilename);
      return 0;
    }
  } else {
    fprintf(stderr, "%s [%d]: Error finding %s or %s\n", __FILE__, __LINE__, leftFilename, rightFilename);
    return 0;
  }
  return 1;

}
  
 
int stereoSource::save() {
  if(_currentFilename==NULL || strcmp(_currentFileType, "")==0) {
    fprintf(stderr, "%s [%d]: Tried to save, but filename or type is blank!\n", __FILE__, __LINE__);
    exit(1);
    return stereoSource_UNKNOWN_ERROR;
  } else {
    return save(_currentFilename, _currentFileType, _pairIndex);
  }
}

int stereoSource::save(char *baseFilename, char *baseFileType, int num) {
  char leftFilename[256], rightFilename[256];
  char leftColorFilename[256], rightColorFilename[256]; 

   
  // This part is open-cv logging, which is bitmap
  
  sprintf(leftFilename, "%s%.5d-L.%s", baseFilename, num, baseFileType);
  sprintf(rightFilename, "%s%.5d-R.%s", baseFilename, num, baseFileType);
  cvSaveImage(leftFilename, _images[node2cam[LEFT_CAM]]);
  cvSaveImage(rightFilename, _images[node2cam[RIGHT_CAM]]);

  if(_colorImages[node2cam[LEFT_CAM]] != NULL) {
    sprintf(leftColorFilename, "%s%.5dColor-L.%s", baseFilename, num, baseFileType);
    cvSaveImage(leftColorFilename, _colorImages[node2cam[LEFT_CAM]]);
  }

  if(_colorImages[node2cam[RIGHT_CAM]] != NULL) {
    sprintf(rightColorFilename, "%s%.5dColor-R.%s", baseFilename, num, baseFileType);
    cvSaveImage(rightColorFilename, _colorImages[node2cam[RIGHT_CAM]]);
  }
  
  
  // Logging using Imagemagick - does currently not do color logging
  /*sprintf(leftFilename, "%s%.5d-L.gif", baseFilename, num);//, baseFileType);
  sprintf(rightFilename, "%s%.5d-R.gif", baseFilename, num);//, baseFileType); 
  Magick::Image leftImage(width(), height(), "I", Magick::CharPixel, left()); 
  Magick::Image rightImage(width(), height(), "I", Magick::CharPixel, right());
  leftImage.write(leftFilename);
  rightImage.write(rightFilename);*/
  
  // Color logging
  /*  if(_colorImages[node2cam[LEFT_CAM]] != NULL) {
    sprintf(leftColorFilename, "%s%.5dColor-L.gif", baseFilename, num);//, baseFileType);
    cvSaveImage(leftColorFilename, _colorImages[node2cam[LEFT_CAM]]);
  }
  
  if(_colorImages[node2cam[RIGHT_CAM]] != NULL) {
    sprintf(rightColorFilename, "%s%.5dColor-R.%s", baseFilename, num, baseFileType);
    cvSaveImage(rightColorFilename, _colorImages[node2cam[RIGHT_CAM]]);
    }*/

  return stereoSource_OK;
}


unsigned char* stereoSource::left() {
  return (unsigned char*)_images[node2cam[LEFT_CAM]]->imageData;
}


unsigned char* stereoSource::right() {
  return (unsigned char*)_images[node2cam[RIGHT_CAM]]->imageData;
}


stereoPair stereoSource::pair() {
  stereoPair returnPair;
  returnPair.left = left();
  returnPair.right = right();

  return returnPair;
}


int stereoSource::width() {
  return pairSize.width;
}


int stereoSource::height() {
  return pairSize.height;
}


int stereoSource::pairIndex() {
  return _pairIndex;
}


int stereoSource::resetPairIndex() {
  return _pairIndex = 0;
}


int stereoSource::setFeatureEnableAutoShutter(int status) {
  return 0;
}


int stereoSource::setFeatureEnableAutoExposure(int status) {
  nodeid_t * camera_nodes;
  int numCameras;
  DGClockMutex(&cameraMutex);
  
  camera_nodes = dc1394_get_camera_nodes(handle, &numCameras, 0);
  
  dc1394_auto_on_off(handle, camera_nodes[0], FEATURE_EXPOSURE, status);
  dc1394_auto_on_off(handle, camera_nodes[1], FEATURE_EXPOSURE, status);
  DGCunlockMutex(&cameraMutex);

  return 0;
}


int stereoSource::setFeatureEnableAutoGain(int status) {
  return stereoSource_NOT_IMPLEMENTED;
}


int stereoSource::setFeatureValShutter(double scale) {
  return stereoSource_NOT_IMPLEMENTED;
}


int stereoSource::setFeatureValExposure(double scale, int camera_nbr) {
  nodeid_t * camera_nodes;
  int numCameras;
  DGClockMutex(&cameraMutex);
  camera_nodes = dc1394_get_camera_nodes(handle, &numCameras, 0);

  unsigned int scale_min, scale_max, result_val;

  dc1394_get_min_value(handle, camera_nodes[camera_nbr], FEATURE_EXPOSURE, &scale_min);
  dc1394_get_max_value(handle, camera_nodes[camera_nbr], FEATURE_EXPOSURE, &scale_max);

  result_val = (unsigned int)(scale*((double)(scale_max-scale_min)) + (double)scale_min);

  dc1394_set_exposure(handle, camera_nodes[0], result_val);
  dc1394_set_exposure(handle, camera_nodes[1], result_val);
  
  DGCunlockMutex(&cameraMutex);

  return 0;
}


int stereoSource::setFeatureValGain(double scale) {
  return stereoSource_NOT_IMPLEMENTED;
}


double stereoSource::getFeatureValShutter() {
  return 0;
}


double stereoSource::getFeatureValExposure(int camera_nbr) {
  nodeid_t * camera_nodes;
  int numCameras;
  DGClockMutex(&cameraMutex);

  camera_nodes = dc1394_get_camera_nodes(handle, &numCameras, 0);

  unsigned int scale_min, scale_max, result_val;

  dc1394_get_min_value(handle, camera_nodes[camera_nbr], FEATURE_EXPOSURE, &scale_min);
  dc1394_get_max_value(handle, camera_nodes[camera_nbr], FEATURE_EXPOSURE, &scale_max);

  dc1394_get_exposure(handle, camera_nodes[camera_nbr], &result_val);
  DGCunlockMutex(&cameraMutex);

  return ((double)(result_val - scale_min))/((double)(scale_max-scale_min));
}


double stereoSource::getFeatureValGain() {
  return 0;
}


double stereoSource::getAvgBrightness(int camera, int x_start, int x_stop, int y_start, int y_stop) {
  //printf("%d, %d, %d, %d\n", x_start, y_start, x_stop-x_start, y_stop-y_start);
  cvSetImageROI(_images[camera], cvRect(x_start, y_start, (x_stop-x_start), y_stop-y_start));

  double mean = cvMean(_images[camera]);
  cvSetImageROI(_images[camera], cvRect(0,0,640,480));
  return mean;
  //return cvMean(_images[camera]);
}


double stereoSource::getAvgROIBrightness(int camera_nbr) {
  //return (getAvgBrightness(0, _imageROI.x, _imageROI.x+_imageROI.width, _imageROI.y, _imageROI.y+_imageROI.height)+
  //getAvgBrightness(1, _imageROI.x, _imageROI.x+_imageROI.width, _imageROI.y, _imageROI.y+_imageROI.height))/2.0;
  return (getAvgBrightness(camera_nbr, _imageROI.x, _imageROI.x+_imageROI.width, _imageROI.y, _imageROI.y+_imageROI.height));
}


int stereoSource::startExposureControl(CvRect imageROI) {
	if(currentSourceType()==SOURCE_CAMERAS) {
		_imageROI = imageROI;
		_exposureControlStatus = 1;
		return setFeatureEnableAutoExposure(0);
	}
	
	return stereoSource_OK;
}


int stereoSource::stopExposureControl() {
	if(currentSourceType()==SOURCE_CAMERAS) {
		_exposureControlStatus = 0;
		_exposureVal_left = -1.0;
		_exposureVal_right = -1.0;
		return setFeatureEnableAutoExposure(1);  
	}

	return stereoSource_OK;;
}

int stereoSource::setExposureControlGain(double gain){
  _exposureControlGain = gain;
  return 0;
}

int stereoSource::startPidControl(){
  _pidControlStatus = 1;
  return 0;
}

int stereoSource::setExposureControl(double kp, double kd, double ki, double sat){
  _pidControl.set_Kp(kp);
  _pidControl.set_Kd(kd);
  _pidControl.set_Ki(ki);
  _pidControl.reset();

  //  printf("KP %lf\n", _pidControl.get_Kp());
  return 0;
}


int stereoSource::currentSourceType() {
	return _currentSourceType;
}

double stereoSource::getExposureScaleValue(int camera_nbr){
  if(camera_nbr==LEFT_CAM){
    return _exposureVal_left;
  }
  return _exposureVal_right;
}
