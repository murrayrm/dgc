#include <iostream>
#include <sstream>
using namespace std;

#include <fcntl.h>
#include <stdlib.h>
#include <math.h>

#include "sn_msg.hh"
#include "DGCutils"

#define MAX_BUFFER_SIZE 1000000

struct SContext
{
	skynet*          pSkynet;
	int              handle;
	int*             sockets;
	char*            pBuffer;
	double           speed;
	int*             pPosition;
};

bool readHdr(int handle, sn_msg& msgtype,
						 int& msgsize, unsigned long long& timestamp)
{
	if(read(handle,(char*)&timestamp, sizeof(timestamp)) != sizeof(timestamp) ||
		 read(handle,(char*)&msgtype, sizeof(msgtype))     != sizeof(msgtype)   ||
		 read(handle,(char*)&msgsize, sizeof(msgsize))     != sizeof(msgsize))
	{
		cerr << "reached end of file" << endl;
		return false;
	}
	if(msgtype < 0 || msgtype >= last_type)
	{
		cerr << "msgtype too large; corrupt data;" << endl;
		return false;
	}

	return true;
}


void* play(void *pArg)
{
	int old;
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &old);
	pthread_setcanceltype (PTHREAD_CANCEL_ASYNCHRONOUS, &old);

	SContext *pContext = (SContext*)pArg;
	unsigned long long starttime;
	DGCgettime(starttime);

	unsigned long long timestampPlayStart = 0ULL;

	while(true)
	{
		sn_msg msgtype;
		int msgsize;
		unsigned long long timestamp;
		if(!readHdr(pContext->handle, msgtype, msgsize, timestamp))
		{
			// should delete the pDatabuffer, but whatever
			exit(0);
		}
		if(read(pContext->handle,(char*)pContext->pBuffer, msgsize) != msgsize)
		{
			cerr << "reached end of file" << endl;
			// should delete the pDatabuffer, but whatever
			exit(0);
		}

		if(timestampPlayStart == 0ULL)
			timestampPlayStart = timestamp;

		unsigned long long now;
		DGCgettime(now);

		long long diffsim, diffreal;
		diffsim  = timestamp- timestampPlayStart;
		diffreal = now      - starttime;
		diffreal = llround(pContext->speed * (double)diffreal);

		if(diffsim > diffreal)
			DGCusleep(diffsim - diffreal);

		int numsent = pContext->pSkynet->send_msg(pContext->sockets[msgtype],
																							pContext->pBuffer,
																							msgsize, 0);

		(*pContext->pPosition)++;

		if(numsent != msgsize)
		{
			cerr << "tried sending " << msgsize << " bytes; sent " << numsent << " bytes." << endl;
		}
	}
}

int main(int argc, char *argv[])
{
	char*                pDatabuffer;
	sn_msg               msgtype;
	int                  msgsize;
	unsigned long long   timestamp;
	int bytesSent;

	int sockets[last_type];
	int i;
	int position = 0;

	pDatabuffer = new char[MAX_BUFFER_SIZE];

	bool bPlayall = false;
	int handle;
	if(argc == 1)
	{
		handle = open("/tmp/guilog", O_RDONLY | O_LARGEFILE);
	}
	else
	{
		if(strcmp(argv[1], "-") == 0)
		{
			handle = STDIN_FILENO;
			bPlayall = true;
		}
		else
			handle = open(argv[1], O_RDONLY | O_LARGEFILE);
	}

	int sn_key;
	cerr << "Searching for skynet KEY " << endl;
	char* pSkynetkey = getenv("SKYNET_KEY");
	if( pSkynetkey == NULL )
	{
		cerr << "SKYNET_KEY environment variable isn't set" << endl;
		return -1;
	}
	else {
		sn_key = atoi(pSkynetkey);
		cout << "Construting skynet with key " << sn_key << endl;
	}
	skynet skynetobject(SNguilogplayer, sn_key);

	for(i=0; i<last_type; i++)
	{
		sockets[i] = skynetobject.get_send_sock((sn_msg)i);
	}

	pthread_t threadid;
	SContext context;

	string line;
	int numToPlayback;

	int lastPlayedBack = 1;
	bool doPlay;
	bool doContinuousPlay = false;

	while(true)
	{
		doPlay = true;

		// get number of messages to play (or ff)
		if(!bPlayall)
		{
			if(!doContinuousPlay)
			{
				cout << endl << "Position: " << position << endl;
				cout << "Enter the number of messages you want to playback. Precede with + to FF. s[v] plays back at speed v. 0 to quit" << endl;
			}

			cin.getline(pDatabuffer, MAX_BUFFER_SIZE);
			if(pDatabuffer[0] == '\0')
				numToPlayback = lastPlayedBack;
			else
			{
				if(pDatabuffer[0] == '+')
				{
					doPlay = false;
					istringstream linestream(pDatabuffer+1);
					linestream >> numToPlayback;
				}
				else if(pDatabuffer[0] == 's' && !doContinuousPlay)
				{
					istringstream linestream(pDatabuffer+1);
					linestream >> context.speed;
					context.pSkynet   = &skynetobject;
					context.handle    = handle;
					context.sockets   = sockets;
					context.pBuffer   = pDatabuffer;
					context.pPosition = &position;

					pthread_create(&threadid, NULL, &play, (void*)&context);
					cout << "continuous play at speed " << context.speed << ". p to stop." << endl;
					doContinuousPlay = true;
				}
				else if(pDatabuffer[0] == 'p')
				{
					doContinuousPlay = false;
					pthread_cancel(threadid);
					continue;
				}
				else
				{
					istringstream linestream(pDatabuffer);
					linestream >> numToPlayback;
					lastPlayedBack = numToPlayback;
				}
			}

			if(numToPlayback == 0)
				break;
		}
		else
			numToPlayback = 1000000000;

		if(doContinuousPlay)
			continue;

		position += numToPlayback;

		for(i = 0; i<numToPlayback; i++)
		{
			if(!readHdr(handle,msgtype,msgsize,timestamp))
			{
				delete pDatabuffer;
				return -1;
			}
			if(doPlay)
			{
				if(msgsize > MAX_BUFFER_SIZE)
				{
					cerr << "message too large: size = " << msgsize << ". skipping..." << endl;
					lseek(handle, msgsize, SEEK_CUR);
				}
				else
				{
					if(read(handle, pDatabuffer, msgsize) != msgsize)
					{
						cerr << "Reached end of log file" << endl;
						delete pDatabuffer;
						return 0;
					}
				}

				cout << "Timestamp: " << timestamp << ", msgtype: " << sn_msg_asString(msgtype) << endl;

				//			cerr << "about to send " << msgsize << " bytes" << endl;
				// 			if(msgtype == SNstate) cerr << "Northing = " << ((double*)pDatabuffer)[1] << endl;
				bytesSent = skynetobject.send_msg(sockets[msgtype], pDatabuffer, msgsize, 0);

				if(bytesSent != msgsize)
				{
					cerr << "Tried to send " << msgsize << " but sent " << bytesSent << " bytes." << endl;
					delete pDatabuffer;
					return -1;
				}
			}
			else
			{
				lseek(handle, msgsize, SEEK_CUR);
				//				cout << "Skipped: Timestamp: " << timestamp << ", msgtype: " << sn_msg_asString(msgtype) << endl;
			}
		}
	}

	delete pDatabuffer;
}
