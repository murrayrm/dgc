#include "MapAccess.h"
#include "specs.h"
#include "AliceConstants.h"

#define DO_BENCHMARK
#ifdef DO_BENCHMARK
extern int numMapAccess;
extern unsigned long long timeMapAccess;
#include <DGCutils>
#endif

extern "C" double dasum_(int*, double*, int*);


// #define FIRSTSTAGE_MAPACCESS_DONTMINIMIZE


#define MAX_NUM_CELLS_MAPSAMPLING 500


#define KERNLONGNUM(xsq)    (MAPSAMPLE_KERNLONG_T0 + (xsq)*(MAPSAMPLE_KERNLONG_T2 + MAPSAMPLE_KERNLONG_T4*(xsq)))
#define DKERNLONGNUM(x,xsq) ((x)*2.0* (MAPSAMPLE_KERNLONG_T2 + 2.0*MAPSAMPLE_KERNLONG_T4*(xsq)))
#define KERNLONGDEN(xsq)    (MAPSAMPLE_KERNLONG_B0 + (xsq)*(MAPSAMPLE_KERNLONG_B2 + (xsq)*(MAPSAMPLE_KERNLONG_B4 + MAPSAMPLE_KERNLONG_B6*(xsq))))
#define DKERNLONGDEN(x,xsq) ((x)*2.0*(MAPSAMPLE_KERNLONG_B2 + 2.0*(xsq)*(MAPSAMPLE_KERNLONG_B4 + 1.5*MAPSAMPLE_KERNLONG_B6*(xsq))))

#define KERNLATNUM1(xsq)    (MAPSAMPLE_KERNLAT_T0_1 + (xsq)*(MAPSAMPLE_KERNLAT_T2_1 + MAPSAMPLE_KERNLAT_T4_1*(xsq)))
#define DKERNLATNUM1(x,xsq) ((x)*2.0* (MAPSAMPLE_KERNLAT_T2_1 + 2.0*MAPSAMPLE_KERNLAT_T4_1*(xsq)))
#define KERNLATDEN1(xsq)    (MAPSAMPLE_KERNLAT_B0_1 + (xsq)*(MAPSAMPLE_KERNLAT_B2_1 + (xsq)*(MAPSAMPLE_KERNLAT_B4_1 + MAPSAMPLE_KERNLAT_B6_1*(xsq))))
#define DKERNLATDEN1(x,xsq) ((x)*2.0*(MAPSAMPLE_KERNLAT_B2_1 + 2.0*(xsq)*(MAPSAMPLE_KERNLAT_B4_1 + 1.5*MAPSAMPLE_KERNLAT_B6_1*(xsq))))

#define KERNLATNUM2(xsq)    (MAPSAMPLE_KERNLAT_T0_2 + (xsq)*(MAPSAMPLE_KERNLAT_T2_2 + MAPSAMPLE_KERNLAT_T4_2*(xsq)))
#define DKERNLATNUM2(x,xsq) ((x)*2.0* (MAPSAMPLE_KERNLAT_T2_2 + 2.0*MAPSAMPLE_KERNLAT_T4_2*(xsq)))
#define KERNLATDEN2(xsq)    (MAPSAMPLE_KERNLAT_B0_2 + (xsq)*(MAPSAMPLE_KERNLAT_B2_2 + (xsq)*(MAPSAMPLE_KERNLAT_B4_2 + MAPSAMPLE_KERNLAT_B6_2*(xsq))))
#define DKERNLATDEN2(x,xsq) ((x)*2.0*(MAPSAMPLE_KERNLAT_B2_2 + 2.0*(xsq)*(MAPSAMPLE_KERNLAT_B4_2 + 1.5*MAPSAMPLE_KERNLAT_B6_2*(xsq))))

#define KERNLATNUM3(xsq)    (MAPSAMPLE_KERNLAT_T0_3 + (xsq)*(MAPSAMPLE_KERNLAT_T2_3 + MAPSAMPLE_KERNLAT_T4_3*(xsq)))
#define DKERNLATNUM3(x,xsq) ((x)*2.0* (MAPSAMPLE_KERNLAT_T2_3 + 2.0*MAPSAMPLE_KERNLAT_T4_3*(xsq)))
#define KERNLATDEN3(xsq)    (MAPSAMPLE_KERNLAT_B0_3 + (xsq)*(MAPSAMPLE_KERNLAT_B2_3 + (xsq)*(MAPSAMPLE_KERNLAT_B4_3 + MAPSAMPLE_KERNLAT_B6_3*(xsq))))
#define DKERNLATDEN3(x,xsq) ((x)*2.0*(MAPSAMPLE_KERNLAT_B2_3 + 2.0*(xsq)*(MAPSAMPLE_KERNLAT_B4_3 + 1.5*MAPSAMPLE_KERNLAT_B6_3*(xsq))))

// ofstream outfile("dat");

double approxMin(double* f, double* dmin_df)
{
	double num = 1.0/f[0]      + 1.0/f[1];
	double den = 1.0/f[0]/f[0] + 1.0/f[1]/f[1];
	double val = num/den;

	dmin_df[0]   = (-1.0/f[0]/f[0] + 2.0/f[0]/f[0]/f[0] * val ) / den;
	dmin_df[1]   = (-1.0/f[1]/f[1] + 2.0/f[1]/f[1]/f[1] * val ) / den;

	return val;
}


#ifdef FIRSTSTAGE_MAPACCESS_DONTMINIMIZE
#error "This doesn't work well. don't know why. needs testing and debuggin"
// This function gets the traversal time from the grown map value. This function
// returns no derivatives and performs no smoothing or minimization
double getTimeMapValueGrownNoDiff(CMap* pMap, int mapLayer,
																	double Northing, double Easting,
																	double sin_yaw, double cos_yaw,
																	double len)
{
	int numcells;

	// the map values and their coords, in a coord system centered at the
	// requested position, re-oriented with arg(X)=yaw with Y = X +/- PI/2, scaled
	// to X,Y in meters
	static double* mapval = new double [MAX_NUM_CELLS_MAPSAMPLING];
	static double* x      = new double [MAX_NUM_CELLS_MAPSAMPLING];
	static double* y      = new double [MAX_NUM_CELLS_MAPSAMPLING];

	double radlat  = MAPSAMPLE_WIDTH/2.0;
	double radlong = len/2.0;

	pMap->getDataRectangle<double>(mapLayer,
																 Northing, Easting ,
																 cos_yaw, sin_yaw,
																 radlat, radlong, 
																 mapval, x, y, &numcells);
	if(numcells > MAX_NUM_CELLS_MAPSAMPLING)
	{
		cerr << "CPlannerStage::getContinuousMapValueDiffGrownNoDiff() used too many points" << endl;
		exit(1);
	}

	int incr1 = 1;
	return len/(dasum_(&numcells, mapval, &incr1)/(double)numcells);
}

#else

// This function gets the traversal time from the grown map value. This function
// returns no derivatives and performs no smoothing or minimization
double getTimeMapValueGrownNoDiff(CMap* pMap, int mapLayer, bool bPassableNodata,
																	double initSpeed,
																	double Northing, double Easting,
																	double sin_yaw, double cos_yaw,
																	double len)
{
	double traversalTime = 0.0;
	int numcells;

	// the map values and their coords, in a coord system centered at the
	// requested position, re-oriented with arg(X)=yaw with Y = X +/- PI/2, scaled
	// to X,Y in meters
	static double* mapval = new double [MAX_NUM_CELLS_MAPSAMPLING];
	static double* x      = new double [MAX_NUM_CELLS_MAPSAMPLING];
	static double* y      = new double [MAX_NUM_CELLS_MAPSAMPLING];

	double radlat  = MAPSAMPLE_WIDTH1/2.0;
	int numpieces  = lround(len / MAPSAMPLE_LENGTH);
	double radlong = len / (double)numpieces /2.0;

	Northing -= cos_yaw*(len/2.0 - radlong);
	Easting  -= sin_yaw*(len/2.0 - radlong);
	for(; numpieces>=0; numpieces--)
	{
		pMap->getDataRectangle<double>(mapLayer,
																	 Northing, Easting ,
																	 cos_yaw, sin_yaw,
																	 radlat, radlong, 
																	 mapval, x, y, &numcells);
		if(numcells > MAX_NUM_CELLS_MAPSAMPLING)
		{
			cerr << "CPlannerStage::getContinuousMapValueDiffGrownNoDiff() used too many points" << endl;
			exit(1);
		}
		double num = 0.0;
		double den = 0.0;

		double ysq;
		double kernlatnum;
		double kernlatden;
		double wy;
		double powvalnum;
		double powvalden;

		for(int i=0; i<numcells; i++)
		{
			double val = mapval[i];
			if(val < 0.0)
			{
				if(bPassableNodata)
					val = fmax(initSpeed*PASSABLE_NODATA_FACTOR, LOWEST_NODATA_DISTANT_SPEED);
				else
					val *= -1.0;
			}
			val += MAPSAMPLE_BIAS;

			ysq = y[i]*y[i];
			kernlatnum = KERNLATNUM1(ysq);
			kernlatden = KERNLATDEN1(ysq);
			wy = kernlatnum/kernlatden;

			powvalnum = wy        / val;
			powvalden = powvalnum / val;

			num += powvalnum;
			den += powvalden;
		}
		traversalTime += 1.0 / (num/den - MAPSAMPLE_BIAS);

		Northing += 2.0*cos_yaw*radlong;
		Easting  += 2.0*sin_yaw*radlong;
	}

	// traversalTime really is its reciprocal of speed, so below really is m * [s/m] = s
	return len*traversalTime;
}

#endif

void getContinuousMapValueDiffGrown(int kernelWidth,
																		CMap* pMap, int mapLayer, bool bPassableNodata,
																		double initSpeed,
																		double Northing, double Easting, double yaw,
																		double *fval, double *dfdN, double *dfdE, double *dfdyaw)
{
#ifdef DO_BENCHMARK
	unsigned long long timeMapAccess1, timeMapAccess2;
	DGCgettime(timeMapAccess1);
#endif

	// MAPACCESS_CENTER_FROM_REAR sets how far forward from the rear axle to move
	// the obstacle avoidance. To avoid obstacles at the front axle, this should
	// be set to VEHICLE_WHEELBASE. However, since the rear of the vehicle corners
	// tighter than the front, this is set closer to the rear
	union
	{
		int numcells;
		int i;
	};

	// the map values and their coords, in a coord system centered at the
	// requested position, re-oriented with arg(X)=yaw with Y = X +/- PI/2, scaled
	// to X,Y in meters
	static double* mapval = new double [MAX_NUM_CELLS_MAPSAMPLING];
	static double* x      = new double [MAX_NUM_CELLS_MAPSAMPLING];
	static double* y      = new double [MAX_NUM_CELLS_MAPSAMPLING];

	double radlat;
	if(kernelWidth == 1)
		radlat = MAPSAMPLE_WIDTH1/2.0;
	else if(kernelWidth == 2)
		radlat = MAPSAMPLE_WIDTH2/2.0;
	else
		radlat = MAPSAMPLE_WIDTH3/2.0;


	double radlong = MAPSAMPLE_LENGTH/2.0;
	double sin_yaw = sin(yaw);
	double cos_yaw = cos(yaw);

	double num;
	double den;
	double dnum_dN;
	double dden_dN;
	double dnum_dE;
	double dden_dE;
	double dnum_dyaw;
	double dden_dyaw;

	double filterCenterShift;

	num = 0.0;
	den = 0.0;
	dnum_dN   = 0.0;
	dden_dN   = 0.0;
	dnum_dE   = 0.0;
	dden_dE   = 0.0;
	dnum_dyaw = 0.0;
	dden_dyaw = 0.0;



	filterCenterShift = MAPACCESS_TO_REAR;

	pMap->getDataRectangle<double>(mapLayer,
																 Northing + filterCenterShift*cos_yaw,
																 Easting  + filterCenterShift*sin_yaw,
																 cos_yaw, sin_yaw,
																 radlat, radlong, 
																 mapval, x, y, &numcells);
	if(numcells > MAX_NUM_CELLS_MAPSAMPLING)
	{
		cerr << "CPlannerStage::getContinuousMapValueDiffGrown() used too many points" << endl;
		exit(1);
	}

	i--;
	for(; i>=0; i--)
	{
		double val = mapval[i];
		if(val < 0.0)
		{
			if(bPassableNodata)
				val = fmax(initSpeed*PASSABLE_NODATA_FACTOR, LOWEST_NODATA_DISTANT_SPEED);
			else
				val *= -1.0;
		}

		val += MAPSAMPLE_BIAS;
		double xval = x[i];
		double yval = y[i];

		double xsq = xval*xval;
		double kernlongden = KERNLONGDEN(xsq);
		double wx = KERNLONGNUM(xsq) / kernlongden;

		double ysq = yval*yval;

		double kernlatden, wy, wx_dy;

		if(kernelWidth == 1)
		{
			kernlatden = KERNLATDEN1(ysq);
			wy = KERNLATNUM1(ysq) / kernlatden;
			wx_dy = wx * (DKERNLATNUM1 (yval,ysq) - wy*DKERNLATDEN1 (yval,ysq)) / kernlatden ;
		}
		else if(kernelWidth == 2)
		{
			kernlatden = KERNLATDEN2(ysq);
			wy = KERNLATNUM2(ysq) / kernlatden;
			wx_dy = wx * (DKERNLATNUM2 (yval,ysq) - wy*DKERNLATDEN2 (yval,ysq)) / kernlatden ;
		}
		else
		{
			kernlatden = KERNLATDEN3(ysq);
			wy = KERNLATNUM3(ysq) / kernlatden;
			wx_dy = wx * (DKERNLATNUM3 (yval,ysq) - wy*DKERNLATDEN3 (yval,ysq)) / kernlatden ;
		}

 		double wy_dx = wy * (DKERNLONGNUM(xval,xsq) - wx*DKERNLONGDEN(xval,xsq)) / kernlongden;
		double powvalnum = wx*wy     / val;

		num += powvalnum;
		den += powvalnum / val;



// 		outfile << xval << ' ' << yval << ' ' << wx << ' ' << wy << ' ' << dx << ' ' << dy << ' ' << val << endl;


// [x; y] = [-cos -sin; -sin cos] [N-N0; E-E0]
		double del_dnum_dN   = (wx_dy*(-sin_yaw)                  + wy_dx*(-cos_yaw) ) / val;
		double del_dnum_dE   = (wx_dy*( cos_yaw)                  + wy_dx*(-sin_yaw) ) / val;
		double del_dnum_dyaw = (wx_dy*(xval + filterCenterShift ) + wy_dx*(-yval)    ) / val;
		dnum_dN   += del_dnum_dN;
		dnum_dE   += del_dnum_dE;
		dnum_dyaw += del_dnum_dyaw;
		dden_dN   += del_dnum_dN  /val;
		dden_dE   += del_dnum_dE  /val;
		dden_dyaw += del_dnum_dyaw/val;
	}



	filterCenterShift = MAPACCESS_TO_FRONT;
	pMap->getDataRectangle<double>(mapLayer,
																 Northing + filterCenterShift*cos_yaw,
																 Easting  + filterCenterShift*sin_yaw,
																 cos_yaw, sin_yaw,
																 radlat, radlong, 
																 mapval, x, y, &numcells);
	if(numcells > MAX_NUM_CELLS_MAPSAMPLING)
	{
		cerr << "CPlannerStage::getContinuousMapValueDiffGrown() used too many points" << endl;
		exit(1);
	}

	i--;
	for(; i>=0; i--)
	{
		double val = mapval[i];
		if(val < 0.0)
		{
			if(bPassableNodata)
				val = fmax(initSpeed*PASSABLE_NODATA_FACTOR, LOWEST_NODATA_DISTANT_SPEED);
			else
				val *= -1.0;
		}

		val += MAPSAMPLE_BIAS;
		double xval = x[i];
		double yval = y[i];

		double xsq = xval*xval;
		double kernlongden = KERNLONGDEN(xsq);
		double wx = KERNLONGNUM(xsq) / kernlongden;

		double ysq = yval*yval;

		double kernlatden, wy, wx_dy;

		if(kernelWidth == 1)
		{
			kernlatden = KERNLATDEN1(ysq);
			wy = KERNLATNUM1(ysq) / kernlatden;
			wx_dy = wx * (DKERNLATNUM1 (yval,ysq) - wy*DKERNLATDEN1 (yval,ysq)) / kernlatden ;
		}
		else if(kernelWidth == 2)
		{
			kernlatden = KERNLATDEN2(ysq);
			wy = KERNLATNUM2(ysq) / kernlatden;
			wx_dy = wx * (DKERNLATNUM2 (yval,ysq) - wy*DKERNLATDEN2 (yval,ysq)) / kernlatden ;
		}
		else
		{
			kernlatden = KERNLATDEN3(ysq);
			wy = KERNLATNUM3(ysq) / kernlatden;
			wx_dy = wx * (DKERNLATNUM3 (yval,ysq) - wy*DKERNLATDEN3 (yval,ysq)) / kernlatden ;
		}

 		double wy_dx = wy * (DKERNLONGNUM(xval,xsq) - wx*DKERNLONGDEN(xval,xsq)) / kernlongden;
		double powvalnum = wx*wy     / val;

		num += powvalnum;
		den += powvalnum / val;



// 		outfile << xval << ' ' << yval << ' ' << wx << ' ' << wy << ' ' << dx << ' ' << dy << ' ' << val << endl;


// [x; y] = [-cos -sin; -sin cos] [N-N0; E-E0]
		double del_dnum_dN   = (wx_dy*(-sin_yaw)                  + wy_dx*(-cos_yaw) ) / val;
		double del_dnum_dE   = (wx_dy*( cos_yaw)                  + wy_dx*(-sin_yaw) ) / val;
		double del_dnum_dyaw = (wx_dy*(xval + filterCenterShift ) + wy_dx*(-yval)    ) / val;
		dnum_dN   += del_dnum_dN;
		dnum_dE   += del_dnum_dE;
		dnum_dyaw += del_dnum_dyaw;
		dden_dN   += del_dnum_dN  /val;
		dden_dE   += del_dnum_dE  /val;
		dden_dyaw += del_dnum_dyaw/val;
	}
	if(kernelWidth == 1)
	{
		num += 1.0/MAPSAMPLE_MINSPEED1;
		den += 1.0/MAPSAMPLE_MINSPEED1/MAPSAMPLE_MINSPEED1;
	}
	else if(kernelWidth == 2)
	{
		num += 1.0/MAPSAMPLE_MINSPEED2;
		den += 1.0/MAPSAMPLE_MINSPEED2/MAPSAMPLE_MINSPEED2;
	}


	fval  [0] = num/den;
	dfdN  [0] = (dnum_dN   - dden_dN   * *fval ) / den;
	dfdE  [0] = (dnum_dE   - dden_dE   * *fval ) / den;
	dfdyaw[0] = (dnum_dyaw - dden_dyaw * *fval ) / den;

	*fval -= MAPSAMPLE_BIAS;


#ifdef DO_BENCHMARK
	DGCgettime(timeMapAccess2);
	timeMapAccess += timeMapAccess2-timeMapAccess1;
	numMapAccess++;
#endif
}

#if 0
#error "optimize the sin() and cos()"
void CPlannerStage::getContinuousMapValueDiffGrown(CMap* pMap, int mapLayer,
																									 double Northing, double Easting, double yaw,
																									 double *fval, double *dfdN, double *dfdE, double *dfdyaw)
{
	// this sets how far forward from the rear axle to move the obstacle
	// avoidance. To avoid obstacles at the front axle, this should be set to
	// VEHICLE_WHEELBASE. However, since the rear of the vehicle corners tighter
	// than the front, this is set to 0.0 for now. In the future, avoidance at
	// both sides will probably have to be implemented.
	const double forwardBias = 0.0;

	double f[OBSTACLE_AVOIDANCE_DIAM];
	double dfdNindividual[OBSTACLE_AVOIDANCE_DIAM];
	double dfdEindividual[OBSTACLE_AVOIDANCE_DIAM];
	double dmin_df[OBSTACLE_AVOIDANCE_DIAM];

	// r cycles through the radii away from the middle. pointindex cycles through
	// the point we're looking at. This is why the for loop below looks funny
	int r, pointindex = 0;

	getContinuousMapValueDiff(Northing + forwardBias*cos(yaw),
														Easting  + forwardBias*sin(yaw),
														&f[pointindex], &dfdNindividual[pointindex], &dfdEindividual[pointindex]);

	pointindex++;
	for(r=1; pointindex<OBSTACLE_AVOIDANCE_DIAM; r++)
	{
		getContinuousMapValueDiff(Northing + ((double)r)*OBSTACLE_AVOIDANCE_DELTA*sin(yaw) + forwardBias*cos(yaw),
															Easting  - ((double)r)*OBSTACLE_AVOIDANCE_DELTA*cos(yaw) + forwardBias*sin(yaw),
															&f[pointindex], &dfdNindividual[pointindex], &dfdEindividual[pointindex]);
		pointindex++;
		getContinuousMapValueDiff(Northing - ((double)r)*OBSTACLE_AVOIDANCE_DELTA*sin(yaw) + forwardBias*cos(yaw),
															Easting  + ((double)r)*OBSTACLE_AVOIDANCE_DELTA*cos(yaw) + forwardBias*sin(yaw),
															&f[pointindex], &dfdNindividual[pointindex], &dfdEindividual[pointindex]);
		pointindex++;
	}

	*fval = approxMin(f, dmin_df);

	*dfdN = 0.0;
	*dfdE = 0.0;
	for(pointindex=0; pointindex<OBSTACLE_AVOIDANCE_DIAM; pointindex++)
	{
		*dfdN += dmin_df[pointindex]*dfdNindividual[pointindex];
		*dfdE += dmin_df[pointindex]*dfdEindividual[pointindex];
	}


	pointindex = 1;
	*dfdyaw =
		dmin_df[0]*
		(dfdNindividual[0]* (-sin(yaw)*forwardBias) +
		 dfdEindividual[0]* ( cos(yaw)*forwardBias));

	for(r=1; pointindex<OBSTACLE_AVOIDANCE_DIAM; r++)
	{
		*dfdyaw +=
			dmin_df[pointindex  ]*
			(dfdNindividual[pointindex  ]* ( cos(yaw)*OBSTACLE_AVOIDANCE_DELTA * ((double)r) - sin(yaw)*forwardBias) +
			 dfdEindividual[pointindex  ]* ( sin(yaw)*OBSTACLE_AVOIDANCE_DELTA * ((double)r) + cos(yaw)*forwardBias));
		*dfdyaw +=
			dmin_df[pointindex+1]*
			(dfdNindividual[pointindex+1]* (-cos(yaw)*OBSTACLE_AVOIDANCE_DELTA * ((double)r) - sin(yaw)*forwardBias) +
			 dfdEindividual[pointindex+1]* (-sin(yaw)*OBSTACLE_AVOIDANCE_DELTA * ((double)r) + cos(yaw)*forwardBias));

		pointindex += 2;
	}
}


void CPlannerStage::getContinuousMapValueDiff(CMap* pMap, int mapLayer,
																							double UTMNorthing, double UTMEasting,
																							double *f, double *dfdN, double *dfdE)
{
	int nearestSWCornerRow, nearestSWCornerCol;
	int row, col, matrixidx;

	// Find the R/C coords of the cell whose SW corner is nearest to the given (N,E)
	pMap->UTM2Win(UTMNorthing + pMap->getResRows() / 2.0,
								UTMEasting  + pMap->getResCols() / 2.0,
								&nearestSWCornerRow, &nearestSWCornerCol);

	// middleCellN, middleCellE are the UTM coords of the corner nearest to UTMNorthing, UTMEasting
	// (rescaledN, rescaledE) is now [0,1] from the mid of the SW cell to the mid of the NE cell
	double middleCellN, middleCellE;
	pMap->Win2UTM(nearestSWCornerRow, nearestSWCornerCol, &middleCellN, &middleCellE);
	double rescaledN = ((UTMNorthing - middleCellN) + pMap->getResRows()/2.0) / pMap->getResRows();
	double rescaledE = ((UTMEasting  - middleCellE) + pMap->getResCols()/2.0) / pMap->getResCols();

#ifdef DO_BENCHMARK
	unsigned long long t1, t2;
	DGCgettime(t1);
#endif





#ifdef INTERPOLATION_GAUSSIAN

	*f    = 0.0;
 	*dfdN = 0.0;
 	*dfdE = 0.0;

	double delr, delc;
	double sumWeights                = 0.0;
	double sumWeightedMap            = 0.0;
	double sumMapDerivativesN        = 0.0;
	double sumDerivativesN           = 0.0;
	double sumMapDerivativesE        = 0.0;
	double sumDerivativesE           = 0.0;

 	for(col = -MAP_INTERP_RAD;
 			col < +MAP_INTERP_RAD;
 			col ++)
 	{
		delc = (double)(col+1) - rescaledE;

 		for(row = -MAP_INTERP_RAD;
 				row < +MAP_INTERP_RAD;
 				row ++)
 		{
			double w;
			double dwN, dwE;
			double m;
 			delr = (double)(row+1) - rescaledN;

 			w  = exp( MAP_INTERP_EXPCOEFF * (delr*delr + delc*delc) );
			dwN = -2.0*MAP_INTERP_EXPCOEFF * w * delr / pMap->getResRows();
			dwE = -2.0*MAP_INTERP_EXPCOEFF * w * delc / pMap->getResCols();
			m  = pMap->getDataWin<double>(mapLayer, row+nearestSWCornerRow, col+nearestSWCornerCol);

			sumWeights                += w;
 			sumWeightedMap            += w * m;
			sumMapDerivativesN        += m * dwN;
			sumDerivativesN           += dwN;
			sumMapDerivativesE        += m * dwE;
			sumDerivativesE           += dwE;
 		}
 	}

	*f    = sumWeightedMap/sumWeights;
	*dfdN = sumMapDerivativesN/sumWeights - sumWeightedMap*sumDerivativesN/sumWeights/sumWeights;
	*dfdE = sumMapDerivativesE/sumWeights - sumWeightedMap*sumDerivativesE/sumWeights/sumWeights;

#elif defined INTERPOLATION_SMOOTH
 	int i;


 	char notrans = 'N';
 	char trans   = 'T';
 	double alpha  =  1.0;
 	double beta   =  0.0;
	double minone = -1.0;
	double posone =  1.0;

 	int incr = 1;
 	int numcoeffs = 3;
 	int numpoints = MAP_INTERP_DIAM*MAP_INTERP_DIAM;

	// backup of WM is necessary because the svd routine destroys the contents
 	double WM[MAP_INTERP_DIAM*MAP_INTERP_DIAM * 3];
 	double WMbackup[MAP_INTERP_DIAM*MAP_INTERP_DIAM * 3];

	double windowW[MAP_INTERP_DIAM*MAP_INTERP_DIAM];
 	double delr, delc;

	double KN[MAP_INTERP_DIAM*MAP_INTERP_DIAM];
	double KE[MAP_INTERP_DIAM*MAP_INTERP_DIAM];

 	matrixidx = 0;
 	for(col = -MAP_INTERP_RAD;
 			col < +MAP_INTERP_RAD;
 			col ++)
 	{
		delc = (double)(col+1) - rescaledE;

 		for(row = -MAP_INTERP_RAD;
 				row < +MAP_INTERP_RAD;
 				row ++)
 		{
			double w;

 			delr = (double)(row+1) - rescaledN;
 			w = exp( MAP_INTERP_EXPCOEFF * (delr*delr + delc*delc) );
 			windowW [matrixidx] = w*pMap->getDataWin<double>(mapLayer, row+nearestSWCornerRow, col+nearestSWCornerCol);

 			WM[matrixidx + numpoints*0] = w;
 			WM[matrixidx + numpoints*1] = w*(double)row;
 			WM[matrixidx + numpoints*2] = w*(double)col;

			KN[matrixidx] = -4.0*MAP_INTERP_EXPCOEFF*delr / pMap->getResRows();
			KE[matrixidx] = -4.0*MAP_INTERP_EXPCOEFF*delc / pMap->getResCols();
 			matrixidx++;
 		}
 	}

	// make a backup of the WM matrix (because the svd routine destroys the data).
	memcpy(WMbackup, WM, MAP_INTERP_DIAM*MAP_INTERP_DIAM * 3*sizeof(double));

	double pinvWM[MAP_INTERP_DIAM*MAP_INTERP_DIAM * 3];
 	double coeffs[3];
	double coeffsdN[3];
	double coeffsdE[3];

	// Compute the SVD of the weighted M matrix
	char svdReturnWhatsImportant = 'S';
	double svdSigma[3];
	double svdU[MAP_INTERP_DIAM*MAP_INTERP_DIAM * 3];
	double svdVT[3*3];
	double svdWork[256];
	int    svdWorkSize = 256;
	int    svdInfo;
	dgesvd_(&svdReturnWhatsImportant, &svdReturnWhatsImportant, &numpoints, &numcoeffs, WM,
					&numpoints, svdSigma, svdU, &numpoints, svdVT, &numcoeffs,
					svdWork, &svdWorkSize, &svdInfo);

	// Now compute the pseudoinverse of WM: WM=U S VT -> WM+ = V inv(S) UT
	// where inv(S) is ST with the diagonal elements reciprocated.
	// inv(S) :
	svdSigma[0] = 1.0/svdSigma[0];
	svdSigma[1] = 1.0/svdSigma[1];
	svdSigma[2] = 1.0/svdSigma[2];
	// V*inv(S)
	dscal_(&numcoeffs, &svdSigma[0], svdVT  , &numcoeffs);
	dscal_(&numcoeffs, &svdSigma[1], svdVT+1, &numcoeffs);
	dscal_(&numcoeffs, &svdSigma[2], svdVT+2, &numcoeffs);
	// V*inv(S)*UT
	dgemm_(&trans, &trans, &numcoeffs, &numpoints, &numcoeffs, &alpha,
				 svdVT, &numcoeffs, svdU, &numpoints, &beta, pinvWM, &numcoeffs);


	// Now compute the solution vector into coeffs:
 	dgemv_(&notrans, &numcoeffs, &numpoints, &alpha, pinvWM, &numcoeffs,
 				 windowW, &incr, &beta, coeffs, &incr);

	// Store the output!
 	*f    = coeffs[0] + (rescaledN-1.0)*coeffs[1] + (rescaledE-1.0)*coeffs[2];


	// Now compute the derivatives:
	// First, compute the error: (windowW - WMx) -> windowW
	dgemv_(&notrans, &numpoints, &numcoeffs, &minone, WMbackup, &numpoints, coeffs, &incr,
				 &posone, windowW, &incr);
	// Then compute K*error
	for(i=0; i<numpoints; i++)
	{
		KN[i] *= windowW[i];
		KE[i] *= windowW[i];
	}
	// Now multiply the K*error to get the two derivative vectors (one for
	// easting, one for northing)
 	dgemv_(&notrans, &numcoeffs, &numpoints, &alpha, pinvWM, &numcoeffs,
 				 KN, &incr, &beta, coeffsdN, &incr);
 	dgemv_(&notrans, &numcoeffs, &numpoints, &alpha, pinvWM, &numcoeffs,
 				 KE, &incr, &beta, coeffsdE, &incr);
 	*dfdN = coeffsdN[0] + coeffs[1]/pMap->getResRows() + (rescaledN-1.0)*coeffsdN[1] + (rescaledE-1.0)*coeffsdN[2];
 	*dfdE = coeffsdE[0] + coeffs[2]/pMap->getResCols() + (rescaledE-1.0)*coeffsdE[2] + (rescaledN-1.0)*coeffsdE[1];;


#else


#if 0
	// At this point we sample the data in a window around (nearestSWCornerRow,
	// nearestSWCornerCol). Then we apply a gaussian blur to this window 4 times
	// to get the blurred value of the map at the 4 cells around our point (the
	// blur is done with a matrix multiplication). We then apply a bilinear
	// interpolation to compute the actual value of the map here. The
	// sampling/blurring (mostly sampling) of the map is a computational
	// bottleneck. We thus cache precomputed sampling/blurring results and use
	// those if we can.
	double& cachedBlurSW = pMap->getDataWin<double>(m_blurLayer, nearestSWCornerRow-1, nearestSWCornerCol-1);
	double& cachedBlurS  = pMap->getDataWin<double>(m_blurLayer, nearestSWCornerRow-1, nearestSWCornerCol  );
	double& cachedBlurW  = pMap->getDataWin<double>(m_blurLayer, nearestSWCornerRow  , nearestSWCornerCol-1);
	double& cachedBlur   = pMap->getDataWin<double>(m_blurLayer, nearestSWCornerRow  , nearestSWCornerCol  );

	double blurredwindow[4] = {0.0, 0.0, 0.0, 0.0};

	// if any one of the corners we need has invalid data, compute everything
	if(cachedBlurSW < 0.0 || cachedBlurS < 0.0 || cachedBlurW < 0.0 || cachedBlur < 0.0)
	{
		// zero out the blurring result
		memset(blurredwindow, 0, 4*sizeof(blurredwindow[0]));

		// these double loops can't be trivially optimized, since the map could wrap in memory (it's a torus).
		matrixidx = 0;
		for(col = nearestSWCornerCol-MAP_BLURINTERP_DIAM_CELLS/2;
				col < nearestSWCornerCol+MAP_BLURINTERP_DIAM_CELLS/2;
				col ++)
		{
			for(row = nearestSWCornerRow-MAP_BLURINTERP_DIAM_CELLS/2;
					row < nearestSWCornerRow+MAP_BLURINTERP_DIAM_CELLS/2;
					row ++)
			{
				double mapval = pMap->getDataWin<double>(mapLayer, row, col);
				blurredwindow[0] += mapval * m_mapblurcoeffs[matrixidx*4    ];
				blurredwindow[1] += mapval * m_mapblurcoeffs[matrixidx*4 + 1];
				blurredwindow[2] += mapval * m_mapblurcoeffs[matrixidx*4 + 2];
				blurredwindow[3] += mapval * m_mapblurcoeffs[matrixidx*4 + 3];
				matrixidx++;
			}
		}

		// if the cached values were NODATA, set them
		if(cachedBlurSW == -2.0) cachedBlurSW = blurredwindow[0];
		if(cachedBlurW  == -2.0) cachedBlurW  = blurredwindow[1];
		if(cachedBlurS  == -2.0) cachedBlurS  = blurredwindow[2];
		if(cachedBlur   == -2.0) cachedBlur   = blurredwindow[3];
	}
	else
	{
		// read in the cached values
		blurredwindow[0] = cachedBlurSW;
		blurredwindow[1] = cachedBlurW;
		blurredwindow[2] = cachedBlurS;
		blurredwindow[3] = cachedBlur;
	}


#else

	double blurredwindow[4] = {0.0, 0.0, 0.0, 0.0};

	blurredwindow[0] = pMap->getDataWin<double>(mapLayer, nearestSWCornerRow-1, nearestSWCornerCol-1);
	blurredwindow[1] = pMap->getDataWin<double>(mapLayer, nearestSWCornerRow-1, nearestSWCornerCol  );
	blurredwindow[2] = pMap->getDataWin<double>(mapLayer, nearestSWCornerRow  , nearestSWCornerCol-1);
	blurredwindow[3] = pMap->getDataWin<double>(mapLayer, nearestSWCornerRow  , nearestSWCornerCol  );

#endif

	*f =
		blurredwindow[0] * ((1.0 - rescaledN) * (1.0 - rescaledE)) +
		blurredwindow[1] * (rescaledN * (1.0 - rescaledE)) +
		blurredwindow[2] * ((1.0 - rescaledN) * rescaledE) +
		blurredwindow[3] * (rescaledN * rescaledE);

	*dfdN =
		(blurredwindow[0] * (rescaledE - 1.0) +
		 blurredwindow[1] * (1.0 - rescaledE) +
		 blurredwindow[2] * (-rescaledE) +
		 blurredwindow[3] * (rescaledE)) / pMap->getResRows();
	*dfdE =
		(blurredwindow[0] * (rescaledN - 1.0) +
		 blurredwindow[1] * (-rescaledN) +
		 blurredwindow[2] * (1.0 - rescaledN) +
		 blurredwindow[3] * (rescaledN)) / pMap->getResCols();

#endif

	if(m_USE_MAXSPEED)
	{
		if(f != NULL)
			*f = (*f) * (MAXSPEED - MIN_V) + MIN_V;
		if(dfdN != NULL)
		{
			*dfdN *= MAXSPEED - MIN_V;
			*dfdE *= MAXSPEED - MIN_V;
		}
	}
}

#endif

