#ifndef CRDDFPREP_HH
#define CRDDFPREP_HH

#include "sn_msg.hh"
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include "SkynetContainer.h"
#include "StateClient.h"
#include "raid.hh"
#include "rddf.hh"
#include "VehicleState.hh"

#define WAIT_CYCLES 10 /**< how many state iterations to wait for 
			* (past the normal waiting) */

using namespace std;


/**
 * CRddfPrep is the class which handles one-time startup duties... 
 * right now it just adds a point onto the RDDF file at our current location
 */
class CRddfPrep : public raid, public RDDF, virtual public CStateClient
{
public:
  /** Default constructor.  Reads in the RDDF specified by
   * the macro RDDF_FILE in GlobalConstants.h */
  CRddfPrep(int skynet_key);

  /** This contructore allows you to specify which RDDF file to use. */
  CRddfPrep(int skynet_key, char* rddf_file);

  /** Standard destructor */
  ~CRddfPrep();

  /**********************************************
   * RDDF functions
   **********************************************/

  /** returns whether or not the current rddf has had a point
   * added to the beginnign of it */
  bool checkRDDFHasBeenPrepended() { return is_startup_prepended_rddf; }

  void prependRDDFWithCurrentState();

private:
  /**
   * HELPER FUNCTIONS
   */

  /** Constructor helper.  Called by all CRddfPrep constructors */
  void CRddfPrepInit();


};

#endif  // CRDDFPREP_HH
