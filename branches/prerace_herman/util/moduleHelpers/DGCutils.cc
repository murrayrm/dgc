#include <time.h>
#include <errno.h>
#include <sys/time.h>
#include <pthread.h>
#include <iostream>

#include "DGCutils"

using namespace std;


void DGCgettime(unsigned long long& DGCtime)
{
	timeval tv;
	gettimeofday(&tv, NULL);

	DGCtime = (unsigned long long)tv.tv_usec + 1000000ULL * tv.tv_sec;
}

double DGCtimetosec(unsigned long long& DGCtime, bool lose_precision)
{
	if(DGCtime > 3600ULL*24ULL*1000000ULL && !lose_precision) // 1 day
	{
		cerr << "DO NOT use DGCtimetosec to convert system times to seconds (only differences). You fool." << endl;
		cerr << "Segfaulting. Use the core to find the bug." << endl;
// 		*((char*)0) = 145;
	}
	return ((double)DGCtime)/1.0e6;
}

long long DGCtimetollsec(const unsigned long long& DGCtime)
{
  return (long long)(DGCtime/1e6);
}


bool DGCcreateMutex(pthread_mutex_t* pMutex)
{
	if(pthread_mutex_init(pMutex, NULL) != 0)
	{
		cerr << "Couldn't create mutex" << endl;
		return false;
	}
	return true;
}

bool DGCdeleteMutex(pthread_mutex_t* pMutex)
{
	if(pthread_mutex_destroy(pMutex) != 0)
	{
		cerr << "Couldn't destroy mutex" << endl;
		return false;
	}
	return true;
}

bool DGClockMutex(pthread_mutex_t* pMutex)
{
	if(pthread_mutex_lock(pMutex) != 0)
	{
		cerr << "Couldn't lock mutex" << endl;
		return false;
	}
	return true;
}

bool DGCunlockMutex(pthread_mutex_t* pMutex)
{

	if(pthread_mutex_unlock(pMutex) != 0)
	{
		cerr << "Couldn't unlock mutex" << endl;
		return false;
	}
	return true;
}


bool DGCcreateRWLock(pthread_rwlock_t* pLock) {
	if(pthread_rwlock_init(pLock, NULL)!=0) {
		cerr << __FILE__ << "[" << __LINE__ << "]: " 
				 << "Couldn't create read-write lock" << endl;
		return false;
	}
	return true;
}


bool DGCdeleteRWLock(pthread_rwlock_t* pLock) {
	if(pthread_rwlock_destroy(pLock)!=0) {
		cerr << __FILE__ << "[" << __LINE__ << "]: " 
				 << "Couldn't destroy read-write lock" << endl;
		return false;
	}
	return true;
}


bool DGCreadlockRWLock(pthread_rwlock_t* pLock, unsigned long long* waitTime) {
	if(pthread_rwlock_rdlock(pLock)!=0) {
		cerr << __FILE__ << "[" << __LINE__ << "]: " 
				 << "Couldn't readlock read-write lock" << endl;
		return false;
	}
 
	return true;
}


bool DGCwritelockRWLock(pthread_rwlock_t* pLock, unsigned long long* waitTime) {
	if(pthread_rwlock_wrlock(pLock)!=0) {
		cerr << __FILE__ << "[" << __LINE__ << "]: " 
				 << "Couldn't writelock read-write lock" << endl;
		return false;
	}

	return true;
}


bool DGCunlockRWLock(pthread_rwlock_t* pLock) {
	if(pthread_rwlock_unlock(pLock)!=0) {
		cerr << __FILE__ << "[" << __LINE__ << "]: " 
				 << "Couldn't unlock read-write lock" << endl;
		return false;
	}
	return true;
}


bool DGCcreateCondition(pthread_cond_t* pCondition)
{
	if(pthread_cond_init(pCondition, NULL) != 0)
	{
		cerr << "Couldn't create condition" << endl;
		return false;
	}
	return true;
}

bool DGCcreateCondition(DGCcondition* dCondition) {
  if(!DGCcreateMutex(&(dCondition->pMutex)) ||
     !DGCcreateCondition(&(dCondition->pCond))) {
		cerr << __FILE__ << "[" << __LINE__ << "]: "
				 << "Couldn't create condition or mutex" << endl;
		return false;
	} 
	
	dCondition->bCond = false;

	return true;
}

bool DGCdeleteCondition(pthread_cond_t* pCondition)
{
	if(pthread_cond_destroy(pCondition) != 0)
	{
		cerr << "Couldn't delete condition" << endl;
		return false;
	}
	return true;
}

bool DGCdeleteCondition(DGCcondition* dCondition) {
  if(!DGCdeleteMutex(&(dCondition->pMutex)) ||
     !DGCdeleteCondition(&(dCondition->pCond))) {
		cerr << __FILE__ << "[" << __LINE__ << "]: "
				 << "Couldn't delete condition or mutex" << endl;
		return false;
	} 
	return true;
}

bool DGCWaitForConditionTrue(bool& bCond, pthread_cond_t& cond, pthread_mutex_t& mutex)
{
	DGClockMutex(&mutex);
  while(!bCond)
	{
		pthread_cond_wait(&cond, &mutex);
	}
	DGCunlockMutex(&mutex);

	return true;
}

bool DGCWaitForConditionTrue(DGCcondition& dCondition)
{
	DGClockMutex(&(dCondition.pMutex));
  while(!(dCondition.bCond))
	{
		pthread_cond_wait(&(dCondition.pCond), &(dCondition.pMutex));
	}
	DGCunlockMutex(&(dCondition.pMutex));

	return true;
}

bool DGCSetConditionTrue(bool& bCond, pthread_cond_t& cond, pthread_mutex_t& mutex)
{
	DGClockMutex(&mutex);
	bCond = true;
	pthread_cond_signal(&cond);
	DGCunlockMutex(&mutex);

	return true;
}

bool DGCSetConditionTrue(DGCcondition& dCondition)
{
	DGClockMutex(&(dCondition.pMutex));
	dCondition.bCond = true;
	pthread_cond_signal(&(dCondition.pCond));
	DGCunlockMutex(&(dCondition.pMutex));

	return true;
}

// Sleeps the specified amount of microseconds
void DGCusleep(unsigned long microseconds)
{
	timespec req, rem;
	int res;

	req.tv_sec  = microseconds / 1000000;
	req.tv_nsec = (microseconds - req.tv_sec*1000000)*1000;

	do
	{
		res = nanosleep(&req, &rem);
		memcpy(&req, &rem, sizeof(req));
	}
	while(res == -1 && errno == EINTR);
}
