#include "StateServer.hh"
#include "SuperConLog.hh"
#include "Diagnostic.hh"
#include "SuperCon.hh"

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>

#include "DGCutils"
#include "sn_msg.hh"

#include "sc_specs.h"

int           OPTKEY = -1;
int           TIMBERLOGLEV = -1;

/* The name of this program. */
const char * program_name;
char* logNamePrefix = NULL;

/* Prints usage information for this program to STREAM (typically stdout
   or stderr), and exit the program with EXIT_CODE. Does not return. */
void print_usage (FILE* stream, int exit_code)
{
  fprintf( stream, "Usage:  %s [options]\n", program_name );
  fprintf( stream,
	   "Welcome to superCon\n"
	   "\n"
	   "-------------------------- COMMAND-LINE OPTIONS ----------------------\n"
           "  --help, -h                  Display this message.\n"
	   "  --nodisp -n                 Turn OFF SparrowHawk display.\n"
	   "  --coutdisp                  cout the status tab to the display, using\n"
           "                              the specified sparrow-display logging level\n"
           "  --snkey [key]               Use designated skynet key.\n"
	   "  --silent, -s                Silent mode - do not *send* skynet msgs.\n"
	   "                                                                      \n"
	   "------------------------- TIMBER LOGGING OPTIONS ---------------------\n"
	   "  --log [logging_level]       Log through timber with the specified\n"
	   "                              (integer) logging_level\n"
	   "\n"
	   " LOGGING LEVELS (Timber logging levels): \n"
	   " 0 -> ERROR messages only (not recommended)\n"
	   " 1 -> WARNING messages [& above] \n"
	   " 2 -> Strategy transition level event messages [& above] \n"
	   " 3 -> Strategy stage level event messages [& above] \n"
	   " 4 -> SuperCon state & Diagnostic table update messages [& above] \n"
	   " 5 -> Output details of *all* skynet messages sent by superCon [& above]\n"
	   "      (this is NOT recommended for SparrowHawk log-level, OK for timber) \n"
	   " 6 -> Output DEBUG messages to SparrowHawk display \n"
	   "\n"
	   "NOTE: the SparrowHawk logging-tab log-level is set through the 'SPECS' tab\n"
	   "      although it uses the same logging levels as timber (see above) \n\n\n");
  exit(exit_code);
}

int main(int argc, char **argv) 
{
  bool usrSpef_silentOptionSelected = false;
  int usrSpef_timberLogging_level = (LAST_LOG_LEVEL_VALUE - 1); //defaults to logging every message

  readspecs();

  bool playback=false;
  int dispOpt = SPARROWHAWK_DISP; //default is sparrowHawk display
  
  /* Set the default arguments that won't need external access here. */
  
  /* Temporary character. */
  int ch;
  /* A string listing valid short options letters. */
  const char* const short_options = "hk:sl:";
  /* An array describing valid long options. */
  static struct option long_options[] = 
    {
      // first: long option (--option) string
      // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
      // third: if pointer, set variable to value of fourth argument
      //        if NULL, getopt_long returns fourth argument
      {"help",        0, NULL,              'h'},
      {"playback",    0, NULL,              'p'},
      {"snkey",       1, NULL,              'k'},
      {"silent",      0, NULL,              's'},
      {"log",         1, NULL,              'l'},
      {"nodisp",      0, NULL,              'n'},
      {"coutdisp",    0, NULL,              'c'},
      {NULL,          0, NULL,               0}
    };
  
  
  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  program_name = argv[0];
  printf("\n");
  
  // Loop through and process all of the command-line input options.
  while((ch = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
    {
      switch(ch)
	{
	case 'h':
	  /* User has requested usage information. Print it to standard
	     output, and exit with exit code zero (normal 
	     termination). */
	  print_usage(stdout, 0);
	  
	case 'k':
	  {
	    OPTKEY = atoi(optarg);
	  }
	  break;
	case 'p':
	  playback=true;
	  break;
	case 's':
	  {
	    cerr << "You selected SILENT MODE" << endl;
	    usrSpef_silentOptionSelected = true;
	  }
	  break;
	case 'l':
	  {
	    TIMBERLOGLEV = atoi(optarg);
	  }
	  break;
	case 'n':
	  {
	    cerr << "You selected the --nodisp option, SparrowHawk display will NOT open" << endl;
	    dispOpt = NO_DISP;
	  }
	  break;

	case 'c':
	  {
	    cerr << "You selected the --coutdisp option, displaying using cout to the terminal" << endl;
	    dispOpt = COUT_DISP;
	  }
	  break;

	case '?': /* The user specified an invalid option. */
	  /* Print usage information to standard error, and exit with exit
	     code one (indicating abnormal termination). */
	  print_usage(stderr, 1);
	  
	case -1: /* Done with options. */
	  {
	  }
	  break;
	}
    }

  // finish processing the command line

  if(TIMBERLOGLEV==-1) { //no timber logging level specified
    cerr << "you selected to NOT log with timber" << endl;
  } else {
   
    if( TIMBERLOGLEV < 0 || TIMBERLOGLEV >= LAST_LOG_LEVEL_VALUE ) {
      cerr << "ERROR: you are a muppet --> valid logging levels are 0 --> "<< (LAST_LOG_LEVEL_VALUE - 1)<<" try again (see --help for options)"<<endl;
    } else {
      usrSpef_timberLogging_level = TIMBERLOGLEV;
    }
  }


  int sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if(OPTKEY==-1)   //No key specified in the parameters
    {
      if( pSkynetkey == NULL )
	{
	  cerr << "SKYNET_KEY environment variable isn't set" << endl;
	} else {
	  sn_key = atoi(pSkynetkey); //convert char -> int
	}
    } else {
      sn_key = OPTKEY;
    }

  cerr << "Constructing skynet with KEY = " << sn_key << endl;

  SuperConLog( sn_key, usrSpef_timberLogging_level, dispOpt );
  cout << "SuperConLog singleton instance created with skynet-key = " << sn_key << " timber logging level = " << usrSpef_timberLogging_level << " and SparrowHawk log level = " << SPARROW_LOG_LEVEL << " Display: " << dispOpt <<endl;
  
  CStateServer stateServer(sn_key);
  //CDiagnostic diagnostic(sn_key, playback);
  CDiagnostic diagnostic( sn_key, usrSpef_silentOptionSelected, dispOpt );
  
  stateServer.activate();
  diagnostic.run();

  return 0;
} // end main()
