#include "ATimberBox.hh"

AtimberBox::AtimberBox(int skynetKey,  int * guistatus)
  : CSkynetContainer(SNadrive, skynetKey, guistatus) {
  
  timberMsgSocket = m_skynet.get_send_sock(SNguiToTimberMsg);
}


AtimberBox::~AtimberBox() {

}


 void AtimberBox::timberStart() {
   CTimber::GUI_MSG_TYPES msg = CTimber::START;
   m_skynet.send_msg(timberMsgSocket, &msg, sizeof(CTimber::GUI_MSG_TYPES));
 }


 void AtimberBox::timberStop() {
   CTimber::GUI_MSG_TYPES msg = CTimber::STOP;
   m_skynet.send_msg(timberMsgSocket, &msg, sizeof(CTimber::GUI_MSG_TYPES));
 }


AdriveEventLogger::AdriveEventLogger(char * logging_path){
    
  logfile.open (logging_path, fstream::out | fstream::app);
  event_enabled = false;
}

AdriveEventLogger::~ AdriveEventLogger(){
  logfile.close();  
}

int AdriveEventLogger::operator<<(string input_string){
  if (event_enabled)
    {
      DGCgettime(current_time);
      logfile << current_time << '\t' << input_string << endl;
      return 1;
    }
  else return -1;
}

int AdriveEventLogger::enable(){
  event_enabled = true;
  return 1;
}

int AdriveEventLogger::disable(){
  event_enabled = false;
  return 1;
}


AdriveEventLogger event("adrive.event");
AdriveEventLogger estop_log("adrive.estop");
