//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_StateSource
//----------------------------------------------------------------------------

#ifndef UD_STATESOURCE_DECS

//----------------------------------------------------------------------------

#define UD_STATESOURCE_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "UD_Linux_Time.hh"
#include "UD_SunPosition.hh"

#include <GL/glut.h>

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef NONE
#define NONE    -1         // convention when end frame number or iteration must be supplied
#endif


#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))

#define NO_RETURN -1.0

#define MAXSTRLEN  256

#define SCANS_LADAR_LOG   1
#define RAW_LADAR_LOG     2
#define UNNATURAL_LADAR_LOG     3
#define DAT_STATE_LOG     4
#define RDDF_STATE_LOG    5
#define LAD_STATE_LOG     6

//-------------------------------------------------
// classes
//-------------------------------------------------

class UD_StateSource
{
  
 public:

  // variables

  double timestamp;
  double northing, easting;
  double heading;
  double pitch;
  float sunalt, sunazi;
  double northing_acceleration, easting_acceleration;
  int state_num;
  double diff, max_diff;
  int state_type;
  int year, month, day, hours, minutes, seconds;
  char *filename;

  // functions

  UD_StateSource(double mdiff) {   state_type = 0; easting = northing = heading = 0; max_diff = mdiff; state_num = 0; }

  //  virtual int is_synced() = 0;
  virtual int sync(double) = 0;
  int is_synced() { return 1; }
  virtual int read() = 0;
  double myround(double);
  void get_start_date_and_time(char *);

};

//-------------------------------------------------

//#define SKYNET

#ifdef SKYNET

#include "RFCommunications.hh"
extern RFCommunications *RFComm;

#endif

class UD_StateSource_Skynet : public UD_StateSource
{
  
 public:

  // functions

  UD_StateSource_Skynet(double);

  void get(double *, double *, double *);

  int sync(double x) { return 1; }
  int is_synced() { return 1; }
  int read();

};


//-------------------------------------------------

class UD_StateSource_Scans : public UD_StateSource
{
  
 public:

  // variables

  FILE *state_fp;
  int scan_type;                       ///< format indicator for log file (it changed between March & June Stoddard Wells tests)
  int last_state_num, state_num;
  int frame_num;

  // functions

  UD_StateSource_Scans(char *, double);

  void get(double *, double *, double *);

  //  int is_synced() { printf("scans synced\n"); fflush(stdout); return (diff < max_diff); }
  int is_synced() { printf("scans synced\n"); fflush(stdout); return 1; }
  void eat_comments(FILE *);
  int read();
  int peek_read();
  int sync(double);
  void state_tell();
  void state_seek();

};

//-------------------------------------------------

// the state info in a .lad file

class UD_StateSource_Ladar : public UD_StateSource
{
  
 public:

  // variables

  // functions

  UD_StateSource_Ladar(char *);

  int is_synced() { return 1; }
  int read() { return 1; }
  int sync(double x ) { return 1; }

};

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
