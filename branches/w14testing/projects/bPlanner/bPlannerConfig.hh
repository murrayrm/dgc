#ifndef BPLANNER_CONFIG
#define BPLANNER_CONFIG

/**********************************
 * RAID
 **********************************/

#define DEFAULT_DEBUG      0

#define RAID_INFO(_) \
_(fncall, 0) \
_(normal, 0) \
_(error, 0)



/**********************************
 * HERMAN
 **********************************/

#define MAX_DIST_FROM_PATH_ASPECT_RATIO 2.0

#endif //BPLANNER_CONFIG
