#ifndef INTERFACE_SUPERCON_TRAJSELECT_HH
#define INTERFACE_SUPERCON_TRAJSELECT_HH

//FILE DESCRIPTION: Header file that contains the definitions of data structures that are
//used in direct communication between SuperCon and the trajSelector

namespace sc_interface {
 
/* Use the scMessage method */
//Used for trajSelector --> superCon (conditional) messages *using the scMessage method/message
//ONLY* - these enums should be used in the msgID field of the scMessage, ALSO
//the msgID field only takes an integer - HENCE you should RECAST one of the
//enums (type: scMsgID_SLT) - to an integer (you should recast whichever member
//of the enum details what you want to do - superCon will then perform the inverse
//recast statement so that the code on either side is cleaner - there are reasons
//why this has to be done)
#define TRAJSLT_SC_MSGIDS_LIST(_) \
  _( min_speed_in_current_traj, = 0 )
DEFINE_ENUM( scMsgID_SLT, TRAJSLT_SC_MSGIDS_LIST )

}

#endif
