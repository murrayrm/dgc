#include "StateServer.hh"


CStateServer::CStateServer(int sn_key)
  : CSkynetContainer(MODsupercon, sn_key)
{
  //Create mutex used to control access to heap
  DGCcreateMutex(&m_msg_mutex);
  DGCcreateCondition(&m_msg_event);
}

CStateServer::~CStateServer()
{
  DGCdeleteMutex(&m_msg_mutex);
  DGCdeleteCondition(&m_msg_event);
}

bool CStateServer::activate()
{
  //Start messagePump
  DGCstartMemberFunctionThread(this, &CStateServer::messagePump);

  //Register all skynet messages with their associated meta-state update functions
  registerSkynetMessage( SNstate, &CStateServer::sn_state, &CStateServer::ms_state );
  registerSkynetMessage( SNactuatorstate, &CStateServer::sn_actuatorstate, &CStateServer::ms_actuatorstate );
  registerSkynetMessage( SNdrivecmd, &CStateServer::sn_drivecmd, &CStateServer::ms_drivecmd );
  registerSkynetMessage( SNtrajFstatus, &CStateServer::sn_trajFstatus, &CStateServer::ms_trajFstatus );
  
  registerSkynetMessage(SNsuperconMessage, &CStateServer::sn_supercon);
  
  
  //Make sure all threads have time to go into their active loops
  usleep(10000);

  return true;
}

void CStateServer::registerSkynetMessage(sn_msg type, void (CStateServer::*threadProc)(), vector<MetaDataUpdateFunc> metadata)
{
  MessageType msg;
  
  msg.type = type;
  msg.metadata_links = metadata;
  m_msg_types.push_back(msg);
  
  DGCstartMemberFunctionThread(this, threadProc);
}


void CStateServer::pushMsg(SkynetMsg &msg)
{
  DGClockMutex(&m_msg_mutex);

  m_msg_heap.push_back(msg);
  push_heap(m_msg_heap.begin(), m_msg_heap.end());

  pthread_cond_signal(&m_msg_event);         //Signal that the heap is not empty
  DGCunlockMutex(&m_msg_mutex);
}

//Thread safe push onto the heap
void CStateServer::pushMsg(int priority, sn_msg type, void *pData)
{
  SkynetMsg msg;
  msg.priority = priority;
  msg.type = type;
  msg.pData = pData;
  pushMsg(msg);
}


CStateServer::SkynetMsg CStateServer::popMsg()
{
  pthread_mutex_lock(&m_msg_mutex);
  while(m_msg_heap.empty())
    pthread_cond_wait(&m_msg_event, &m_msg_mutex);
  
  //We have exclusive access to the heap
  pop_heap(m_msg_heap.begin(), m_msg_heap.end());
  SkynetMsg msg = m_msg_heap.back();
  m_msg_heap.pop_back();

  pthread_mutex_unlock(&m_msg_mutex);

  return msg;
}

void CStateServer::messagePump()
{
  cerr<<"CStateServer::messagePump - Entered message pump"<<endl;

  while(1) {    //Don't allow graceful termination
    SkynetMsg msg = popMsg();

    /*
    **
    ** Switch on message type  (i.e. message source)
    **
    */
    switch(msg.type) {   //Convert pData to correct type and deallocate when done

    case SNsuperconMessage:
      parseSuperConMessage( (supercon_msg_cmd*)msg.pData );
      msg.pData=0;
      break;
      
    case SNstate:
      /*
      **
      ** astate message
      **
      */
      {
	VehicleState *pState = (VehicleState*)msg.pData;
	
	m_SCstate.northing = pState->Northing;
	m_SCstate.easting = pState->Easting;
	m_SCstate.nVel = pState->Vel_N;
	m_SCstate.eVel = pState->Vel_E;
	m_SCstate.vehicleSpeed = pState->Speed2();
	m_SCstate.yaw = pState->Yaw;
	m_SCstate.pitch = pState->Pitch;
	
	m_SCstate.northingConfidence = pState->NorthConf;
	m_SCstate.eastingConfidence = pState->EastConf;
	m_SCstate.heightConfidence = pState->HeightConf;
	m_SCstate.rollConfidence = pState->RollConf;
	m_SCstate.pitchConfidence = pState->PitchConf;
	m_SCstate.yawConfidence = pState->YawConf;
	
	//Free memory
	delete pState;
	msg.pData=0;
      }
      break;
      /* end of msg. type: SNstate switch case */
      
    case SNactuatorstate:
      /*
      **
      ** actuator message
      **
      */
      {
	ActuatorState *pActuator = (ActuatorState*)msg.pData;
	
	m_SCstate.wheelSpeed = pActuator->m_VehicleWheelSpeed;
	m_SCstate.forceAppliedtoWheels = pActuator->m_WheelForce;
	m_SCstate.timeSinceEngineStart = pActuator->m_TimeSinceEngineStart;
	m_SCstate.transmissionPos = pActuator->m_transpos;
	m_SCstate.estopDARPAPos = pActuator->m_dstoppos;
	m_SCstate.estopSuperConPos = pActuator->m_cstoppos;
	m_SCstate.estopAdrivePos = pActuator->m_astoppos;
	m_SCstate.gasCmd = pActuator->m_gascmd;
	m_SCstate.brakeCmd = pActuator->m_brakecmd;
	m_SCstate.obd2operational = pActuator->m_obdiistatus;

	//Free memory
	delete pActuator;

	msg.pData=0;
    }
    break;
    /* end of msg. type: SNactuatorstate switch case */
    
    case SNdrivecmd:
      /*
      **
      ** drive command message
      **
      */
      {
	drivecmd_t *pDrivecmd = (drivecmd_t*)msg.pData;
	
	//Steer command
	if( pDrivecmd->my_actuator == steer && pDrivecmd->my_command_type == set_position ) {
	  m_SCstate.currentSteerCmd = pDrivecmd->number_arg;
	}
	
	//Acceleration (combined throttle-brake) command
	if( pDrivecmd->my_actuator == accel && pDrivecmd->my_command_type == set_position ) {
	  m_SCstate.currentAccelCmd = pDrivecmd->number_arg;
	}

      	//Free memory
	delete pDrivecmd;
	msg.pData=0;
      }
      break;
      /* end of msg. type: SNdrivecmd switch case */

    case SNtrajFstatus:
      /*
      **
      ** traj follower message
      **
      */
      {
	trajFstatusStruct *ptrajFstatusStruct = (trajFstatusStruct*)msg.pData;
	m_SCstate.currentSpeedRef = ptrajFstatusStruct->currentVref;
	m_SCstate.currentTFmode = ptrajFstatusStruct->currentMode;
	m_SCstate.trajFMAXspeedCap = ptrajFstatusStruct->smallestMAXspeedCap;
	m_SCstate.trajFMINspeedCap = ptrajFstatusStruct->largestMINspeedCap;
	
      	//Free memory
	delete ptrajFstatusStruct;
	msg.pData=0;
      }
      break;


    default:
      cerr<<"Unknown message"<<endl;
      break;
    }

    if(msg.pData) {
      //Memory not deallocated
      cerr<<"Memory not deallocated when handling message type:"<<msg.type<<endl;
    }

    //Update all necessary meta-states
    vector<MessageType>::iterator itr = find(m_msg_types.begin(), m_msg_types.end(), msg.type);
    if(itr!=m_msg_types.end()) {
      for(vector<MetaDataUpdateFunc>::iterator itr2 = itr->metadata_links.begin(); itr2!=itr->metadata_links.end(); ++itr2)
	(this->**itr2)();
    }
  }
}

/*
**
** Parse a single supercon message
**
*/
void CStateServer::parseSuperConMessage(supercon_msg_cmd *pMsg)
{
  /** scMessage from SUPERCON (deliberation thread) **/
  if(strncmp(pMsg->module_id, "SC", 2)==0) {
    /*
    **
    ** Internal message from supercon
    **
    */

    switch( (scMsgID_SC) pMsg->msg_id ) {
    case sc_interface::copy_scstate_table:    
      {
	//Update the state
	unsigned long long ts;
	DGCgettime(ts);
	m_SCstate.copyTimestamp = ts;
	
	//Copy state
	int npos=0;
	SCstate *pState;
	pthread_cond_t *pEvent;
	pthread_mutex_t *pMutex;
	bool *pFlag;
	sc_deserialize(pMsg, npos, pState);      //Get the supplied pointer
	sc_deserialize(pMsg, npos, pFlag);
	sc_deserialize(pMsg, npos, pMutex);
	sc_deserialize(pMsg, npos, pEvent);      //Get the supplied event
	
	DGClockMutex(pMutex);
	
	*pState = m_SCstate;
	
	*pFlag=true;
	pthread_cond_signal(pEvent);                //Signal the event
	
	m_SCstate.reset_scThrows();
	
	DGCunlockMutex(pMutex);
      }
      break;
      
    case sc_interface::update_double_scstate_value:
      {
	int npos=0;
	double SCstate::*pVar;
	double val;
	
	sc_deserialize(pMsg, npos, pVar);
	sc_deserialize(pMsg, npos, val);         //Value to set it to
	
	m_SCstate.*pVar = val;
      }
      break;
      
    case sc_interface::update_int_scstate_value:
      {
	int npos=0;
	int SCstate::*pVar;
	int val;
	
	sc_deserialize(pMsg, npos, pVar);
	sc_deserialize(pMsg, npos, val);         //Value to set it to
	
	m_SCstate.*pVar = val;
      }
      break;
      
    default:
      cerr<<"Unknown scMessage type "<<pMsg->type<<" from module "<<pMsg->module_id[0]<<pMsg->module_id[1]<<pMsg->module_id[2]<<endl;
      break;
    }
  } else if(strncmp(pMsg->module_id, "AST", 3)==0) {
    /*
    **
    ** scMessage from astate
    **
    */
    
    switch( (scMsgID_AST) pMsg->msg_id ) {
    case sc_interface::need_to_stop_GPS_reacquired:
      m_SCstate.astNeedToIncludeGPSafterReAcq = TRUE;
      break;
      
    case sc_interface::astate_happy_panda:
      m_SCstate.astHappyPanda = TRUE;
      break;
      
    default:
      cerr<<"Unknown scMessage type "<<pMsg->type<<" from module "<<pMsg->module_id[0]<<pMsg->module_id[1]<<pMsg->module_id[2]<<endl;
      break;
    }
  } else if(strncmp(pMsg->module_id, "TJF", 3)==0) {
    /*
    **
    ** scMessage from trajFollower
    **
    */

    switch( (scMsgID_TJF) pMsg->msg_id ) {
    case sc_interface::completed_reversing_action:
      m_SCstate.tfFinishedReversing = TRUE;
      break;
      
    default:
      cerr<<"Unknown scMessage type "<<pMsg->type<<" from module "<<pMsg->module_id[0]<<pMsg->module_id[1]<<pMsg->module_id[2]<<endl;
      break;
    }
  } else if(strncmp(pMsg->module_id, "SLT", 3)==0) {
    /*
    **
    ** scMessage from trajSelector
    **
    */

    switch( (scMsgID_SLT) pMsg->msg_id ) {
    case sc_interface::min_speed_in_current_traj:
      {
	int npos=0;
	double min_speed_on_traj;
	//Get the minimum speed [m/s] on the current trajectory
	sc_deserialize(pMsg, npos, min_speed_on_traj);
	
	m_SCstate.minCellSpeedOnCurrentTraj = min_speed_on_traj;
	SuperConLog().log( STR, WARNING_MSG, "SPEED ON TRAJ!! - %lf", min_speed_on_traj );
      }
      break;
      
    default:
      cerr<<"Unknown scMessage type "<<pMsg->type<<" from module "<<pMsg->module_id[0]<<pMsg->module_id[1]<<pMsg->module_id[2]<<endl;	      
      SuperConLog().log( SRV, ERROR_MSG, "Unknown scMessage type: %i  from module: %c%c%c", int(pMsg->type), pMsg->module_id[0], pMsg->module_id[1], pMsg->module_id[2] );
      break;
    }
    
  } else {
    /*
    **
    ** scMessage from unidentified host
    **
    */
    cerr<<"Supercon message received from unknown module "<<pMsg->module_id[0]<<pMsg->module_id[1]<<pMsg->module_id[2]<<endl;
    SuperConLog().log( SRV, ERROR_MSG, "SuperCon message received from unknown module: %c%c%c", pMsg->module_id[0], pMsg->module_id[1], pMsg->module_id[2] );
  }
  
  //Free memory
  delete pMsg;
}


/*
**
** Message receiving threads
**
*/

void CStateServer::sn_supercon()
{
  SuperConLog().log( SRV, SC_STATE_DIAGNOSTIC_TABLE_UPDATES, "sn_supercon started" );

  int socket = m_skynet.listen(SNsuperconMessage, ALLMODULES);
  supercon_msg_cmd *pMsg = new supercon_msg_cmd;

  while(true) {   //Don't allow graceful termination
    if(m_skynet.get_msg(socket, pMsg, sizeof(supercon_msg_cmd), 0) < (int)SUPERCON_MSG_CMD_CORE) {
      cerr<<"Incorrect number of bytes received in SNstate message"<<endl;
      continue;
    }
    if(pMsg->type != scMessage)    //If it was something else than a message, don't handle it
      continue;

    //cout<<"Received supercon message - id = "<<pMsg->msg_id<<"  size = "<<pMsg->user_bytes<<endl;

    //Send message
    int prior = 0;
    if(strncmp(pMsg->module_id, "SC", 2)==0) {
      //Received a supercon request
      if(pMsg->msg_id==0)   //Higher priority for request for state
	prior = 1;
      if(pMsg->msg_id==1)   //Make sure all state setttings requested by supercon (diagnostics - strategies) are propagated before a state update
	prior = 2;
    }

    pushMsg(prior, SNsuperconMessage, pMsg);

    //Allocate new buffer for next message
    pMsg = new supercon_msg_cmd;
  }
}


void CStateServer::sn_state()
{
  SuperConLog().log( SRV, SC_STATE_DIAGNOSTIC_TABLE_UPDATES, "sn_astate started" );

  int socket = m_skynet.listen(SNstate, ALLMODULES);
  VehicleState *pState = new VehicleState;
  
  while(true) {   //Don't allow graceful termination
    if(m_skynet.get_msg(socket, pState, sizeof(VehicleState), 0) != sizeof(VehicleState)) {
      cerr<<"Incorrect number of bytes received in SNstate message"<<endl;
      continue;
    }
    //Send message
    pushMsg(0, SNstate, pState);

    //Allocate new buffer for next message
    pState = new VehicleState;
  }
}


void CStateServer::sn_actuatorstate()
{
  SuperConLog().log( SRV, SC_STATE_DIAGNOSTIC_TABLE_UPDATES, "sn_adrive started" );

  int socket = m_skynet.listen(SNactuatorstate, ALLMODULES);
  ActuatorState *pActuator = new ActuatorState;
  
  while(true) {   //Don't allow graceful termination
    if(m_skynet.get_msg(socket, pActuator, sizeof(ActuatorState), 0) != sizeof(ActuatorState)) {
      cerr<<"Incorrect number of bytes received in SNactuatorstate message"<<endl;
      continue;
    }
    //Send message
    pushMsg(0, SNactuatorstate, pActuator);

    //Allocate new buffer for next message
    pActuator = new ActuatorState;
  }
}


void CStateServer::sn_drivecmd()
{
  SuperConLog().log( SRV, SC_STATE_DIAGNOSTIC_TABLE_UPDATES, "sn_trajFollower started" );

  int socket = m_skynet.listen(SNdrivecmd, ALLMODULES);
  drivecmd_t *pDrivecmd = new drivecmd_t;
  
  while(true) {   //Don't allow graceful termination
    if(m_skynet.get_msg(socket, pDrivecmd, sizeof(drivecmd_t), 0) != sizeof(drivecmd_t)) {
      cerr<<"Incorrect number of bytes received in SNdrivecmd message"<<endl;
      continue;
    }
    //Send message
    pushMsg(0, SNdrivecmd, pDrivecmd);

    //Allocate new buffer for next message
    pDrivecmd = new drivecmd_t;
  }
}


void CStateServer::sn_trajFstatus()
{
  SuperConLog().log( SRV, SC_STATE_DIAGNOSTIC_TABLE_UPDATES, "sn_trajFstatus started" );
  
  int socket = m_skynet.listen(SNtrajFstatus, ALLMODULES);
  trajFstatusStruct *ptrajFstatusStruct = new trajFstatusStruct;

  while(true) {   //Don't allow graceful termination
    if(m_skynet.get_msg(socket, ptrajFstatusStruct, sizeof(trajFstatusStruct), 0) != sizeof(trajFstatusStruct)) {
      cerr<<"Incorrect number of bytes received in SNtrajFstatus message"<<endl;
      continue;
    }
    //Send message
    pushMsg(0, SNtrajFstatus, ptrajFstatusStruct);

    //Allocate new buffer for next message
    ptrajFstatusStruct = new trajFstatusStruct;
  }
}



/*** META-STATE PROCESS DEFINITIONS  ***/

//Meta-variables to be updated when an SNstate message is received
void CStateServer::ms_state()
{
  m_SCstate.componentOfWeightDownHill = VEHICLE_MASS * GRAVITATIONAL_ACCELERATION_METERS_PER_SEC2 * sin(m_SCstate.pitch ); 
  m_SCstate.astateINVconfidence = hypot(m_SCstate.northingConfidence,m_SCstate.eastingConfidence );
}

//Meta-variables to be updated when an SNactuatorstate message is received
void CStateServer::ms_actuatorstate() {
  //not currently used
}

//Meta-variables to be updated when an SNdrivecmd message is received
void CStateServer::ms_drivecmd() {
  //not currently used
}

//Meta-variables to be updated when an SNtrajFstatus message is received
void CStateServer::ms_trajFstatus() {
  //not currently used
}


//////////////////////////////////////////////////////////////////////////////////////
//Operators needed for working with contained structures
bool operator==(const CStateServer::MessageType &msg, sn_msg type) {
  return msg.type==type;
}

bool operator<(const CStateServer::SkynetMsg &msg1, const CStateServer::SkynetMsg &msg2) {
  return msg1.priority<msg2.priority;
}

