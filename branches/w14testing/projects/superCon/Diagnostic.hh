#ifndef DIAGNOSTICS_HH
#define DIAGNOSTICS_HH

#include "SuperCon.hh"
#include "SuperConLog.hh"

#include "sc_specs.h"
#include "SuperConClient.hh"
#include "bool_counter.hh"

#include "Strategy.hh"
#include "StrategyInterface.hh"
#include "interface_superCon_adrive.hh"
#include "interface_superCon_astate.hh"
#include "interface_superCon_pln.hh"
#include "interface_superCon_superCon.hh"
#include "interface_superCon_trajF.hh"
#include "trajF_speedCap_cmd.hh"
#include "trajF_status_struct.hh"

#include "CTimberClient.hh"
#include "SkynetContainer.h"
#include "AliceConstants.h"
#include "GlobalConstants.h"

#include <pthread.h>
#include <fstream>
#include <vector>
#include <math.h>

using namespace std;
using namespace sc_interface;

class CStrategy;        //Forward declaration
class CStrategyInterface;

class CDiagnostic : virtual public CSkynetContainer, public CSuperConClient
{
public:

  //CDiagnostic(int skynet_key, bool playback=false);
  CDiagnostic( int skynet_key, bool silentOptionSelected, bool nodisp );

  ~CDiagnostic();

  //Active loop method (handles continued execution):
  //NOTE: assumes the state server is running
  void run();

protected:           //Internal functions related to strategies and rules
  void wait_for_state();
  void do_loging();
  void wait_loging();
  
  void init();       //Initialize all strategies etc.
  void updateDiagnostics(const SCstate &SCstate, SCdiagnostic &diag);

public:   //Interface functions (for CStrategy etc.)
  void transitionStrategy(SCStrategy to, const char *reason);
  void setDoubleState(double SCstate::*pState, double val);
  void setIntState(int SCstate::*pState, int val);
  string strategyName(SCStrategy id) const;

  void log(...) {}   //Dummy log function

protected:           //Internal helper functions
  void registerStrategy(SCStrategy id, const char *name, CStrategy *pStrategy);

  double get_debug_elapsed();
  void   do_debug_step();

private:
  bool runSparrowHawkDisplay; //true to start sparrow display

  bool m_SCstate_new;
  SCstate m_SCstate;
  pthread_mutex_t m_SCstate_mutex;        //Mutex locking the m_SCstate struct
  pthread_cond_t m_SCstate_event;
  SCdiagnostic m_diagnostics;
  
  SCStrategy m_current_strategy;        //Currently active strategy
  
  CStrategyInterface *m_pStrategyInterface; //Pointer to interface used by strategies

  //Log variables
  bool m_playback;
  bool m_logging_enabled;
  FILE *m_logfile_state;      //Log file for state data
  FILE *m_logfile_log;        //Log file for text logging

  struct {
    volatile bool is_step_forward;     //Are we stepping forward or running timper
    volatile int  do_steps;            //Number of steps left to go
    int do_steps_reset;                //Value of the "default" step count
    int cycles;
    unsigned long long start_time;
    unsigned long long cur_time;
    double playback_hz;
  } m_debug;                  //Data bound to the debuging

  //struct to hold list of all registered strategies
  struct Strategies
  {
    string name;
    CStrategy *pStrategy;
  };
  Strategies m_strategies[StrategyLast];
};


#endif
