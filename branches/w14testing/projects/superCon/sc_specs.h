#ifndef SC_SPECS_H
#define SC_SPECS_H

#include <math.h>

/** this should contain "_(type, name, init value)\" **/
#define SPECLIST(_) \
  /*** TOP-LEVEL SUPERCON DEFINES ***/ \
  _( double, SUPERCON_FREQ, 10.0 ) \
  /* (fewest log messages) --> LOG_ERROR = 0, LOG_WARNING = 1, LOG_INFORM = 2, LOG_ANNOY = 3 --> (most log messages) */ \
  _( int, SPARROW_LOG_LEVEL, 3 ) \
  /* Units = Meters/Second */ \
  _( double, TERRAIN_MAP_ZERO_SPEED, 0.15 ) \
  \
  \
  \
  /*** DEFINES USED BY DIAGNOSTIC RULES ***/ \
  _( double, WHEEL_SPIN_DETECTION_THRESHOLD_SPEED_MS, 5 ) \
  _( double, WHEEL_SPIN_WHEELS_FASTER_FACTOR, 10000.0 ) \
  /* Factor used to control the threshold used to determine whether sufficient force \
   * has been applied to move Alice up whatever incline she is currently on, the \
   * threshold is set to the force required to move Alice up a hill = (her current \
   * incline * this factor), NOTE: this should be >1 (% factor) \
   */ \
  _( double, ENGINE_FORCE_GREATER_FACTOR, 1.25 ) \
  /* THIS IS A HACK VALUE!! NEED A PROPER ONE! */ \
  _( double, ASTATE_CONFIDENCE_POOR_THRESHOLD, 0.4 ) \
  /* This is the slowest speed in M/S that the TrajFollower can maintain, speeds \
   * below this are assumed to be zero in some form  */ \
  /* Need these values to be > 1.0 (as this is the value used by trajFollower) */ \
  _( double, SLOWEST_MAINTAINABLE_ALICE_SPEED, 0.5 ) \
  _( double, SLOWEST_MAINTAINABLE_WHEEL_SPEED, 0.1 ) \
  /* In the case when OBD2 FAILS, this is accelCmd (hence -ve for brake, +ve for throttle (-1 -> +1) \
   * threshold that must be exceeded (in a +VE direction) before it is assumed that we have applied \
   * enough force to the wheels to move Alice's mass up a slope with angle = her current pitch */ \
  _( double, THROTTLE_THRESHOLD_FOR_ENGINE_FORCE_INCLINE_IF_OBD2_DEAD, 0.5 ) \
  \
  \
  /** REPETITION DEFINES USED IN TRANS_/TERM_ RULES **/ \
  _( int, REPEAT_TRUE_COUNT_NOMINALOPERATION, 3 ) \
  _( int, REPEAT_TRUE_COUNT_GPSREACQ, 3 ) \
  _( int, REPEAT_TRUE_COUNT_LTURNREVERSE, 3 ) \
  _( int, REPEAT_TRUE_COUNT_UNSEENOBSTACLE, 3 ) \
  _( int, REPEAT_TRUE_COUNT_SLOWADVANCE, 3 ) \
  _( int, REPEAT_TRUE_COUNT_WHEELSPIN, 3 ) \
  _( int, REPEAT_TRUE_COUNT_ESTOP_PAUSED_NOT_SUPERCON, 3 ) \
  _( int, REPEAT_TRUE_COUNT_PLN_NOT_NFP, 3 ) \
  \
  \
  \
  \
  /*** DEFINES USED IN STRATEGY CLASSES  ***/ \
  \
  /** Generic Strategy class **/ \
  /* Timeout loop thresholds are measured in SUPERCON EXECUTION CYCLES and are used during action 'time-out' checking */ \
  /* THESE ARE HACK VALUES - NEED PROPER ONES! */ \
  _( int, TIMEOUT_LOOP_ESTOP_THRESHOLD, 100 ) \
  _( int, TIMEOUT_LOOP_ASTATE_GPS_REINC_THRESHOLD, 100 ) \
  _( int, TIMEOUT_LOOP_GEARCHANGE, 100 ) \
  _( int, TIMEOUT_LOOP_TRAJFMODECHANGE, 100 ) \
  _( int, TIMEOUT_LOOP_TRAJF_REVERSE_FINISHED, 10000 ) \
  _( int, TIMEOUT_LOOP_SLOWADVANCE_APPROACH, 10000 ) \
  _( int, TIMEOUT_LOOP_UNSEENOBST_WAIT_FOR_STATIONARY, 50 ) \
  _( int, TIMEOUT_LOOP_WAIT_FOR_ALICE_STATIONARY, 100 ) \
  \
  /** GPSreAcq Strategy **/ \
  \
  /** LoneRanger Strategy **/ \
  /* Speed at which to attempt to drive through obstacles [METERS/SECOND] */ \
  _( double, LONE_RANGER_MIN_SPEED_MS, 2.0 ) \
  /* persistance count threshold for the number of consecutive plans made \
   * whilst in LoneRanger strategy which pass through superCon obstacles
   * (condition for transition to  */ \
  \
  /** LturnReverse Strategy **/ \
  /* Distance to reverse (along the path we came in on) for an L-turn */ \
  _( double, LTURN_REVERSE_DISTANCE_METERS, 15.0 ) \
  \
  /** Nominal Strategy **/ \
  \
  /** SlowAdvance Strategy **/ \
  /* Speed [meters/second] below which the maps can reasonably be considered \
   * to be 'high-quality' as sensor data gathered at slower speeds will be more \
   * heavily weighted than sensor data gathered at higher speeds, and reduced \
   * high frequency oscillations etc */ \
  _(double, HIGH_ACCURACY_MAPS_SPEED_MS, 3.0 ) \
  \
  /** UnseenObstacle Strategy **/ \
  /* This define should be ZERO if the center of the REAR-AXLE is being used, \
   * and if this is NOT the case then this define should be the offset in METERS \
   * (TOWARDS THE FRONT AXLE = +VE) from the center of the rear axle (assumes that \
   * vehicle origin is along the center-line (line joining the centers of both axles) \
   * of the vehicle) of the actual vehicle origin. */ \
  _( double, OFFSET_OF_VEHICLE_ORIGIN_FROM_REAR_AXLE, 2.75 ) \
  /* Number of points along the bumper for which map-delta updates are sent when an \
   * unseen-obstacle has been detected (should be even) */ \
  _( int, NUM_POINTS_TO_EVALUATE_ALONG_BUMPER, 42 ) \
  /* cost-value assigned to a cell in the superCon map layer to denote a superCon \
   * obstacle */ \
  _( double, SUPERCON_OBSTACLE_SPEED_MS, 0.001 )  \
  \
  /** WheelSpin Strategy **/ \
  \
  /** NOTsuperConEstopPause Strategy **/ \
  \
  /** Multiple Strategies **/

#define EXTERNDEFINESPEC(type, name, val) \
extern type name;

#define DEFINESPEC(type, name, val) \
type name = val;

#define EXTERNDEFINESPECS namespace specs { SPECLIST(EXTERNDEFINESPEC) }; using namespace specs
#define DEFINESPECS namespace specs { SPECLIST(DEFINESPEC)	}; using namespace specs


EXTERNDEFINESPECS;


extern void readspecs(void);

#endif //SC_SPECS_H
