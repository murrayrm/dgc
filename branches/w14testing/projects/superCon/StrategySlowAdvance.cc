//SUPERCON STRATEGY TITLE: Slow Advance - SOMETIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategySlowAdvance.hh"
#include "StrategyHelpers.hh"

void CStrategySlowAdvance::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {
  
  skipStageOps.reset(); //resets to FALSE
  currentStageName = slowadvance_stage_names_asString( slowadvance_stage_names(stgItr.nextStage()) );

  switch( stgItr.nextStage() ) {

  case s_slowadvance::max_speedcap: //send speed-cap to trajF = high accuracy map speed (get better view of obstacle)
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "SlowAdvance: %s", currentStageName.c_str() ) );
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_modifyTrajFSpeedCap( add_max_speedcap, HIGH_ACCURACY_MAPS_SPEED_MS );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "SlowAdvance: stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;

  case s_slowadvance::slowadvance_wait: //wait until planner -> !NFP or Alice stops at zero-speed point on Traj
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "SlowAdvance: %s", currentStageName.c_str() ) );
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->validAliceStop == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "SlowAdvance: Alice not yet stopped in front of obstacle - will poll");
	checkForPersistLoop( this, persist_loop_slowAdvanceApproach, stgItr.currentStageCount() );
      }
      
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	//remove the superCon speed-cap imposed at the start of the SlowAdvance
	//strategy, and transition to L-turn reverse strategy as Alice is currently
	//stationary at a point in the trajectory where the traj-speed goes to zero
	//(hence she should be in front of whatever obstacle is blocked her)
	m_pStrategyInterface->stage_modifyTrajFSpeedCap( remove_max_speedcap, 0 );
	transitionStrategy( StrategyLturnReverse, "SlowAdvance complete - stationary in front of intraversible obstacle" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "SlowAdvance complete - stationary in front of intraversible obstacle");
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "SlowAdvance: stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }      
    break;

    /** END OF STRATEGY **/
 
  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategySlowAdvance::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategySlowAdvance::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: SlowAdvance - default stage reached!");
    }
  }
}


void CStrategySlowAdvance::leave( CStrategyInterface *m_pStrategyInterface )
{
  skipStageOps.reset(); //resets to FALSE
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "SlowAdvance - Exiting");
}

void CStrategySlowAdvance::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset();
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "SlowAdvance - Entering");
}
