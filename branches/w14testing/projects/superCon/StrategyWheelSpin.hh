#ifndef STRATEGY_WHEELSPIN_HH
#define STRATEGY_WHEELSPIN_HH

//SUPERCON STRATEGY TITLE: Wheel-Spin - ANYTIME STRATEGY
//HEADER FILE (.hh)
//DESCRIPTION: 

#include "Strategy.hh"
#include "sc_specs.h"

namespace s_wheelspin {

#define WHEELSPIN_STAGE_LIST(_) \
  _(estop_pause, = 1) /*first entry must = 1 (as this is referenced against NEXTstage */ \
  _(trans_unseenobstacle, )
DEFINE_ENUM(wheelspin_stage_names, WHEELSPIN_STAGE_LIST)

}

using namespace std;
using namespace s_wheelspin;

class CStrategyWheelSpin : public CStrategy
{

/*** METHODS ***/
public:

  CStrategyWheelSpin() : CStrategy() {}  
  ~CStrategyWheelSpin() {}

  /** MUTATORS **/
  void stepForward(const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface);
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

private:
  //conditional test for transition to EstopPauseNOTsuperCon
  bool EstopPausedNOTsuperCon_trans_condition( bool checkBool, const SCdiagnostic *pdiag, const char* StrategySpecificStr );

};

#endif
