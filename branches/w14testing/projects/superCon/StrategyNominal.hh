#ifndef STRATEGY_NOMINAL_HH
#define STRATEGY_NOMINAL_HH

/* SUPERCON STRATEGY TITLE: Nominal
 * HEADER FILE (.hh)
 * DESCRIPTION: This strategy is the NOMINAL operation strategy, and the system
 * should transition to, and remain in, this strategy if none of the required
 * entry/continuation conditions for the other strategies are met.  Any route
 * that could be taken by the system by transitioning between different strategies
 * SHOULD return the system to the nominal state
 */

#include "Strategy.hh"
#include "sc_specs.h"

#include<iostream>

namespace s_nominal {

#define NOMINAL_STAGE_LIST(_) \
  _(estop_pause, = 1) /*first entry must = 1 (as this is referenced against NEXTstage */ \
  _(gear_drive, ) \
  _(trajF_forwards, ) \
  _(remove_speedcaps, ) \
  _(estop_run, ) \
  _(alls_well, ) /*This must be the last in the list - it is the nominal ops stage (loops)*/
DEFINE_ENUM(nominal_stage_names, NOMINAL_STAGE_LIST)

};

using namespace std;
using namespace s_nominal;

class CStrategyNominal : public CStrategy
{

/** METHODS **/
public:
  CStrategyNominal() : CStrategy() {}
  ~CStrategyNominal() {}

  //Step one step forward using the (new) supplied diagnostic rule results
  void stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface );
  //We are leaving this state, do clean up (NO NEW STATE TRANSFERS)
  void leave( CStrategyInterface *m_pStrategyInterface );
  //We are entering this state, do initialisation (NO NEW STATE TRANSFERS)
  void enter( CStrategyInterface *m_pStrategyInterface );

private:
  //Generic strategy conditions to be evaluated at the start of each stage
  //in the nominal strategy, NOTE: this does **NOT** check slowAdvance
  bool nominal_TransTerm_conditions( CStrategyInterface *pStrategyInterface, const SCdiagnostic *pdiag, const char* StrategySpecificStr );

  //check slowAdvance strategy - note that this is separate, as slowAdvance is NOT checked UNTIL we are in estop RUN
  bool checkSlowAdvance( const SCdiagnostic *pdiag, const char* StrategySpecificStr );

};

#endif
