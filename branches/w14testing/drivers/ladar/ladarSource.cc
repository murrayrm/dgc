#include "ladarSource.hh"

#define PFIXME printf("%s [%d] fixme\n", __FILE__, __LINE__);

ladarSource::ladarSource() { 
  _currentSourceType = SOURCE_UNASSIGNED;
  _scanIndex = 0;
  //_currentFilename = NULL;
  memset(_currentFilename, 0, 256);
  _loggingEnabled = 0;
  _loggingPaused = 0;
  _loggingOpts.logRaw = 1;
  _loggingOpts.logState = 1;
  _loggingOpts.logRanges = 0;
  _loggingOpts.logRelative = 0;
  _loggingOpts.logUTM = 0;
  _logFileState = NULL;  
  _logFileRelative = NULL;  
  _logFileRanges = NULL;  
  _logFileUTM = NULL;  

  _numPoints = 0;

  sprintf(_errorMessage, "No errors detected.");
}


ladarSource::~ladarSource() {
  stopLogging();
}


int ladarSource::init(int sourceType, char *ladarIDFilename, char *logFilename, int loggingEnabled, int optladar) {
  char filenameHeader[256];
  char filenameRaw[256];
  char filenameState[256];

  char tempBuffer[2048];

  _currentSourceType = sourceType;
  //int returnStatus = OK;
  if(strcmp(logFilename, "")!=0) {
    strcpy(_currentFilename, logFilename);
  }
  strcpy(_configFilename, ladarIDFilename);
  parseConfigFile(ladarIDFilename);
  
  //Different setup must occur depending on the source type we're using:
  switch(sourceType) {
  case SOURCE_LADAR:
    //If we're using an actual physical LADAR, then setup depends on the actual
    //LADAR type:
    switch(_currentLadarType) {
    case TYPE_SICK_LMS_221:   
      //If we're using a SICK LMS 221, keep trying to set it up until it works
			cerr << "One to three error messages are normal when starting the SICK LADAR - please be patient." << endl;
      while(_sickLadar.Setup(_configFilename)) {
	cerr << "Setup failed." << endl;
	sleep(1);
	sprintf(_errorMessage, "LADAR not responding - retrying!");
	_sickLadar.Reset();
      }      
      break;
      //Future Changes: Add new setup routines here
    case TYPE_RIEGL:
      	_riegl.Connect();
	_riegl.StartScan();
	break;
    default:
      PFIXME
      break;
    }
    
    break;

  case SOURCE_FILES:
    //If we're running off logged files, then for now we can get away with doing
    //one type of setup for every LADAR, if we make sure they all log using the
    //same format
    //First check to make sure the filename to run from is valid
    if(strcmp(_currentFilename, "")==0) {
      PFIXME;
      return UNKNOWN_ERROR;
    }
    //Setup the filenames of the specific individual files
    sprintf(filenameHeader, "%s%s", _currentFilename, ".header");
    sprintf(filenameRaw, "%s%s", _currentFilename, ".raw");
    sprintf(filenameState, "%s%s", _currentFilename, ".state");
    struct stat bufferHeader;
    struct stat bufferRaw;
    struct stat bufferState;
    //Check to see that the file exists
    if(!stat(filenameHeader, &bufferHeader) &&
       !stat(filenameRaw,    &bufferRaw) &&
       !stat(filenameState,  &bufferState)) {
      //Parse the header for configuration information
      parseConfigFile(filenameHeader);
      //Open the logged raw LADAR and state data
      _logFileState = fopen(filenameState, "r");
      fgets(tempBuffer, 2048, _logFileState);
    } else {
      sprintf(_errorMessage, "Log file did not exist");
      return UNKNOWN_ERROR;      
    }
    switch(_currentLadarType) {
    case TYPE_SICK_LMS_221:   
      _sickLadar.SetupFromFile(filenameHeader, filenameRaw);
      break;
    case TYPE_RIEGL:
      _riegl.Connect("dummy");
      _riegl.StartPlayback(filenameRaw);
      break;
    default:
      break;
    }
    break;    
  default:
    cerr << __FILE__ << " [" << __LINE__ << "]: Unknown source type, couldn't open!" << endl;
    return UNKNOWN_SOURCE;
    break;
  }

  //Check if logging was enabled and start it if necessary
  if(loggingEnabled) {
    startLogging(_loggingOpts);
  }

  return OK;
}


int ladarSource::stop() {
  return UNKNOWN_ERROR;
}

unsigned long long ladarSource::scan() {
  //Different scan functions must happen depending on the LADAR source we're using...
  switch(_currentSourceType) {
    case SOURCE_LADAR:
      //If we're using an actual physical LADAR
      switch(_currentLadarType) {
        case TYPE_SICK_LMS_221:
	  _numPoints =  _sickLadar.GetScanFrame(_ranges, _angles, &_latestScanTimestamp, _errorMessage);
          break;
	case TYPE_RIEGL:
	 _numPoints =  _riegl.GetScanFrame(NULL, _ranges, _angles, NULL, NULL, &_latestScanTimestamp);	 
	 break;
        default:
          break;
      }
      break;

    case SOURCE_FILES:
      cerr << "ladarSource::scan():     case SOURCE_FILES. We shouldn't be here. segfaulting" << endl;
      *(char*)0 = 1243;

      /*
      if(!feof(_logFileRaw)) {
              fscanf(_logFileRaw, "%llu ", &_latestScanTimestamp);
        fscanf(_logFileRaw, "%d ", &numScanPoints);
	for(int i=0; i<numScanPoints; i++) {
          fscanf(_logFileRaw, "%d ", &tempInt);
          if(tempInt >= MAX_RANGE_SICK_LMS_221) {
            //bad scan - do nothing
          } else {
            _ranges[numGoodPoints] = tempInt * _ladarUnits;
            _angles[numGoodPoints] = _scanAngleOffset + (i*_resolution*M_PI)/180.0;
            numGoodPoints++;
          }
          fscanf(_logFileRaw, "\n");
        }
      } else {
        //PFIXME;
	//printf("%s [%d] \n", __FILE__, __LINE__);
        return UNKNOWN_ERROR;
      }
      */
      //printf("%s [%d] \n", __FILE__, __LINE__);

      break;

  default:
    PFIXME
    break;
  }
  
  _scanIndex++;

  return _latestScanTimestamp;
}


unsigned long long ladarSource::scan(unsigned long long timestampThresh, int useLoggedState) {
   unsigned long long timestampActual = 0;
   unsigned long long timestampState;
   char tempBuffer[2048];

   switch(_currentLadarType) {
   case TYPE_SICK_LMS_221:
     timestampActual = _sickLadar.Scan(timestampThresh);
     if(timestampActual!=0)
       _numPoints =  _sickLadar.GetScanFrame(_ranges, _angles, &_latestScanTimestamp, _errorMessage);
     break;
   case TYPE_RIEGL:
     while (timestampActual < timestampThresh) 
       _numPoints =  _riegl.GetScanFrame(NULL, _ranges, _angles, NULL, NULL, &timestampActual);
     _latestScanTimestamp = timestampActual;
     break;
   default:
     PFIXME;
     break;
   }

   if(timestampActual==0) {
     sprintf(_errorMessage, "End of log file reached.");
   }

   _scanIndex++;

   if(useLoggedState) {
     fscanf(_logFileState, "%llu ", &timestampState);
     while(timestampActual != timestampState && !feof(_logFileState)) {
       fgets(tempBuffer, 2048, _logFileState);
       fscanf(_logFileState, "%llu ", &timestampState);      
     }
     fscanf(_logFileState, 
 	   "%llu %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
 	   &_currentState.Timestamp, 
 	   &_currentState.Northing, 
 	   &_currentState.Easting, 
 	   &_currentState.Altitude,
 	   &_currentState.Vel_N, 
 	   &_currentState.Vel_E, 
 	   &_currentState.Vel_D,
 	   &_currentState.Acc_N, 
 	   &_currentState.Acc_E, 
 	   &_currentState.Acc_D,
 	   &_currentState.Roll, 
 	   &_currentState.Pitch, 
 	   &_currentState.Yaw,
 	   &_currentState.RollRate, 
 	   &_currentState.PitchRate, 
 	   &_currentState.YawRate,
 	   &_currentState.RollAcc, 
 	   &_currentState.PitchAcc, 
 	   &_currentState.YawAcc);
   }

  return timestampActual;
}


int ladarSource::startLogging(ladarSourceLoggingOptions loggingOpts, const char *baseFilename) {
  char filenameHeader[256];
  char filenameRaw[256];
  char filenamePlain[256];
  char filenameState[256];
  char filenameRanges[256];
  char filenameRelative[256];
  char filenameUTM[256];

  //Check to make sure that the filename exists
  if(baseFilename!=NULL && strcmp(baseFilename, "")!=0) {
    strcpy(_currentFilename, baseFilename);
  }

  //If logging is already enabled, or if we're reading from files, then return
  //an error
  if(_loggingEnabled==1 || _currentSourceType==SOURCE_FILES) {
    PFIXME;
    return UNKNOWN_ERROR;
  } else {
    //Otherwise read the logging options
    _loggingOpts = loggingOpts;
      
    //Check to see if the filename exists
    if(_currentFilename==NULL || strcmp(_currentFilename, "")==0) {
      //the filename doesn't exist - make it
      time_t t = time(NULL);
      tm *local;
      local = localtime(&t);
      char datestamp[255];

      sprintf(datestamp, "%02d%02d%04d_%02d%02d%02d",
	      local->tm_mon+1, local->tm_mday, local->tm_year+1900,
	      local->tm_hour, local->tm_min, local->tm_sec);
      
      sprintf(_currentFilename, "logs/ladar_%s", datestamp);
    }
    
    sprintf(filenameHeader, "%s%s", _currentFilename, ".header");

    struct stat buffer;
    if(!stat(filenameHeader, &buffer)) {
      PFIXME;
      _loggingEnabled = 0;
      return UNKNOWN_ERROR;
    } else {
      sprintf(filenameRaw, "%s%s", _currentFilename, ".raw");
      sprintf(filenamePlain, "%s%s", _currentFilename, ".plain");
      sprintf(filenameState, "%s%s", _currentFilename, ".state");
      sprintf(filenameRanges, "%s%s", _currentFilename, ".ranges");
      sprintf(filenameRelative, "%s%s", _currentFilename, ".relative");
      sprintf(filenameUTM, "%s%s", _currentFilename, "UTM");
      
      FILE* logFileHeader = NULL;
      logFileHeader = fopen(filenameHeader, "w");
      if(logFileHeader==NULL) {
	fprintf(stderr, "%s [%d]: Error opening file '%s', quitting...\n", __FILE__, __LINE__, filenameHeader);
	exit(1);
      }

      fprintf(logFileHeader, "%% Header for ladar logs\n");
      fprintf(logFileHeader, "config_file: %s\n", _configFilename);
      fprintf(logFileHeader, "x: %lf\n", _ladarPosOffset.X);
      fprintf(logFileHeader, "y: %lf\n", _ladarPosOffset.Y);
      fprintf(logFileHeader, "z: %lf\n", _ladarPosOffset.Z);
      fprintf(logFileHeader, "pitch: %lf\n", _ladarAngleOffset.P);
      fprintf(logFileHeader, "roll: %lf\n", _ladarAngleOffset.R);
      fprintf(logFileHeader, "yaw: %lf\n", _ladarAngleOffset.Y);
      fprintf(logFileHeader, "type: %s\n", getLadarType(_currentLadarType));
      fprintf(logFileHeader, "scan_angle: %d\n", _scanAngle);
      fprintf(logFileHeader, "resolution: %lf\n", _resolution);
      fprintf(logFileHeader, "units: %lf\n", _ladarUnits);
      fprintf(logFileHeader, "tty: %s\n", _serialPort);
      fclose(logFileHeader);
      
      if(_loggingOpts.logRaw) {
	switch(_currentLadarType) {
	case TYPE_SICK_LMS_221:
	  _sickLadar.startLogging(filenameRaw);
	  break;  
	case TYPE_RIEGL:
	  _riegl.StartLogging(filenameRaw,filenamePlain);
	  break;
	default:
	  PFIXME;
	  break;
	}
	/*
	_logFileRaw = fopen(filenameRaw, "w");
	if(_logFileRaw==NULL) {
	  fprintf(stderr, "%s [%d]: Error opening file '%s', quitting...\n", __FILE__, __LINE__, filenameRaw);
	  exit(1);
	}
	fprintf(_logFileRaw, "%% Time [usec] | Scan Points\n");
	*/
      }

      if(_loggingOpts.logState) {
	_logFileState = fopen(filenameState, "w");
	if(_logFileState==NULL) {
	  fprintf(stderr, "%s [%d]: Error opening file '%s', quitting...\n", __FILE__, __LINE__, filenameState);
	  exit(1);
	}
	fprintf(_logFileState, "%% LADARTime [usec] | StateTime[usec] | Northing[m] | Easting [m] Altitude[m]");
	fprintf(_logFileState, " | Vel_N[m/s] | Vel_E[m/s] | Vel_U[m/s]" );
	fprintf(_logFileState, " | Acc_N[m/s^2] | Acc_E[m/s^2] | Acc_U[m/s^2]" );
	fprintf(_logFileState, " | Roll[rad] | Pitch[rad] | Yaw[rad]" );
	fprintf(_logFileState, " | RollRate[rad/s] | PitchRate[rad/s] | YawRate[rad/s]");
	fprintf(_logFileState, " | RollAcc[rad/s^2] | PitchAcc[rad/s^2] | YawAcc[rad/s^2]");
	fprintf(_logFileState, "\n");     
      }

      _loggingEnabled = 1;    
      return OK;
    }    
  }
    return UNKNOWN_ERROR;
}


int ladarSource::stopLogging() {
  if(_currentSourceType!=SOURCE_FILES) {

    /*
    if(_logFileRaw!=NULL) {
      fclose(_logFileRaw);
      _logFileRaw = NULL;
    }
    */
    switch(_currentLadarType) {
    case TYPE_SICK_LMS_221:
      if(_sickLadar.isLogging())
	_sickLadar.stopLogging();
      break;  
    case TYPE_RIEGL:
	_riegl.StopLogging();
      break;
    default:
      PFIXME;
      break;
    }

    if(_logFileState!=NULL) {
      fclose(_logFileState);
      _logFileState = NULL;
    }
    if(_logFileRanges!=NULL) {
      fclose(_logFileRanges);
      _logFileRanges = NULL;
    }
    if(_logFileRelative!=NULL) {
      fclose(_logFileRelative);
      _logFileRelative = NULL;
    }
    if(_logFileUTM!=NULL) {
      fclose(_logFileUTM);
      _logFileUTM = NULL;
    }

    _loggingEnabled = 0;
    _loggingPaused = 0;
  }

  return OK;
}


int ladarSource::resetScanIndex() {
  _scanIndex = 0;
  return UNKNOWN_ERROR;
}


int ladarSource::parseConfigFile(char* configFilename) {
  //Get info about our ladar from the ID file
  FILE* configFile = NULL;
  char fieldBuffer[512];
  char fieldName[256];
  char fieldValue[256];

  configFile = fopen(configFilename, "r");
  if(configFile != NULL) {
    while(!feof(configFile)) {
      fgets(fieldBuffer, 512, configFile);
      if(strncmp(fieldBuffer, "%", 1)!=0) {
	sscanf(fieldBuffer, "%s %s\n", fieldName, fieldValue);
	if(strcmp(fieldName, "type:")==0) {
	  _currentLadarType = getLadarType(fieldValue);
	} else if(strcmp(fieldName, "scan_angle:")==0) {
	  _scanAngle = atoi(fieldValue);
	} else if(strcmp(fieldName, "resolution:")==0) {
	  _resolution = atof(fieldValue);
	} else if(strcmp(fieldName, "tty:")==0) {
	  strcpy(_serialPort, fieldValue);
	} else if(strcmp(fieldName, "units:")==0) {
	  _ladarUnits = atof(fieldValue);
	} else if(strcmp(fieldName, "x:")==0) {
	  _ladarPosOffset.X = atof(fieldValue);
	} else if(strcmp(fieldName, "y:")==0) {
	  _ladarPosOffset.Y = atof(fieldValue);
	} else if(strcmp(fieldName, "z:")==0) {
	  _ladarPosOffset.Z = atof(fieldValue);
	} else if(strcmp(fieldName, "roll:")==0) {
	  _ladarAngleOffset.R = atof(fieldValue);
	} else if(strcmp(fieldName, "pitch:")==0) {
	  _ladarAngleOffset.P = atof(fieldValue);
	} else if(strcmp(fieldName, "yaw:")==0) {
	  _ladarAngleOffset.Y = atof(fieldValue);
	} else if(strcmp(fieldName, "config_file:")==0) {
	  //Don't need to do anything
	} else if(strcmp(fieldName, "rate:")==0) {
	  //Don't need to do anything yet
	} else if(strcmp(fieldName, "authority:")==0) {
	  //Don't do anything
	} else {
	  PFIXME;
	  printf("Unknown field '%s' with value '%s' in the configuration file %s\n", fieldName, fieldValue, configFilename);
	}
      }
    }
    _scanAngleOffset = ((180.0 - _scanAngle)/2.0) * M_PI/180.0;
    setLadarFrame(_ladarPosOffset, _ladarAngleOffset);

    return OK;
  } else {
    fprintf(stderr, "%s [%d]: Could not open LADAR configuration file: %s\n",
	    __FILE__, __LINE__, configFilename);
    return UNKNOWN_ERROR;
  }
      
  return UNKNOWN_ERROR;
}


int ladarSource::setLadarFrame(XYZcoord ladarPosOffset, RPYangle ladarAngleOffset) {
	_ladarPosOffset = ladarPosOffset;
	_ladarAngleOffset = ladarAngleOffset;

  _ladarFrame.initFrames(ladarPosOffset, 
			 ladarAngleOffset.P,
			 ladarAngleOffset.R,
			 ladarAngleOffset.Y);

  return OK;
}


int ladarSource::updateState(VehicleState state) {
  _currentState = state;
  
  _ladarFrame.updateState(NEDcoord(_currentState.Northing, _currentState.Easting, _currentState.Altitude), 
			  _currentState.Pitch, _currentState.Roll, _currentState.Yaw);
  
  if(_logFileState!=NULL) {
    fprintf(_logFileState, "%llu %llu %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
	    _latestScanTimestamp,
	    _currentState.Timestamp, 
	    _currentState.Northing, _currentState.Easting, _currentState.Altitude,
	    _currentState.Vel_N, _currentState.Vel_E, _currentState.Vel_D,
	    _currentState.Acc_N, _currentState.Acc_E, _currentState.Acc_D,
	    _currentState.Roll, _currentState.Pitch, _currentState.Yaw,
	    _currentState.RollRate, _currentState.PitchRate, _currentState.YawRate,
	    _currentState.RollAcc, _currentState.PitchAcc, _currentState.YawAcc);
    fflush(_logFileState);
  }

  return OK;
}

double ladarSource::range(int i) {
  return _ranges[i];
}


double ladarSource::angle(int i) {
 return _angles[i];
}

int    ladarSource::numPoints()
{
	return _numPoints;
}

XYZcoord ladarSource::ladarPoint(int i) {
  //printf("%d x: %f y: %f angle: %f range: %f\n", i,_ranges[i]*sin(_angles[i]),_ranges[i]*cos(_angles[i]), _angles[i], _ranges[i]);
  return XYZcoord(_ranges[i]*sin(_angles[i]), _ranges[i]*cos(_angles[i]), 0.0);
}


NEDcoord ladarSource::UTMPoint(int i) {
  return _ladarFrame.transformS2N(ladarPoint(i));
}



int ladarSource::toggleLogging() {
  _loggingPaused = !_loggingPaused;

  return !_loggingPaused;
}


int ladarSource::reset() {
	switch(_currentLadarType) {
	case TYPE_SICK_LMS_221:   
		_sickLadar.Reset();
      break;
	case TYPE_RIEGL:
		break;
	default:
		PFIXME;
		break;
	}
	return OK;
}

int ladarSource::getLadarType(char* ladarString) {
   if(strcmp(ladarString, "SICK_LMS_221")==0) 
      return TYPE_SICK_LMS_221;

   if (strcmp(ladarString, "RIEGL")==0) 
	return TYPE_RIEGL;  

   fprintf(stderr, "%s [%d]: Unknown LADAR type: %s\n", __FILE__, __LINE__, ladarString);
   return TYPE_UNKNOWN;
}


char* ladarSource::getLadarType(int ladarInt) {
  switch(ladarInt) {
  case TYPE_SICK_LMS_221:
    return "SICK_LMS_221";
    break;
  case TYPE_RIEGL:
    return "RIEGL";
    break;
  default:
    return NULL;
    break;
  }

  return NULL;
}
