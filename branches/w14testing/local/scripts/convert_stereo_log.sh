#!/bin/bash

# This script is used for conversion of stereo
# log data. It takes all the files in the
# current directory and converts them according
# to the command-line parameters.
#
# usage:
# convert_stereo_log <current format> <new format>

FILESINDIR=*
OLDFORMAT=$1
NEWFORMAT=$2
for file in $FILESINDIR
  do
  if [[ $file = *"$OLDFORMAT" ]]
      then
      NEWFILE=${file/$OLDFORMAT/$NEWFORMAT}
      echo converting $file to $NEWFILE...
      convert $file $NEWFILE
      if [[ -fs "$NEWFILE" ]]
	  then
	  echo deleting $file...
	  rm $file
      fi 
  fi
done