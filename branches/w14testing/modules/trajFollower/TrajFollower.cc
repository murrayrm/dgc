#include <unistd.h>

#include <sys/time.h>
#include <iostream>
#include <iomanip>
#include <math.h>
#include "trajFollowerTabSpecs.hh"
using namespace std;

#include "TrajFollower.hh"
#include "sn_msg.hh"
#include "adrive_skynet_interface_types.h"
#include "DGCutils"
#include "find_force.hh"

int QUIT_PRESSED = 0;

// These extern's relate to the '--' command line options for TrajFollower (see TrajFollowerMain.cc for set-up details)
extern int           NOSTATE;
extern int           NODISPLAY;
extern int           USEMODEMAN;       // Use ModeManModule
extern int           SIM;
extern unsigned long SLEEP_TIME;
extern int           A_HEADING_REAR;
extern int           A_HEADING_FRONT;
extern int           A_YAW;
extern int           Y_REAR;
extern int           Y_FRONT;
extern int           LATERAL_FF_OFF;
extern double        HACK_STEER_COMMAND;
extern bool          USE_HACK_STEER;
extern int           USE_DBS;


#define MIN_SPEED 0.01


/******************************************************************************/
/******************************************************************************/
TrajFollower::TrajFollower(int sn_key, char* pTrajfile, char* pLindzey) 
  : CSkynetContainer(SNtrajfollower, sn_key), CTimberClient(timber_types::trajFollower), CModuleTabClient(&m_input, &m_output), CSuperConClient("TJF")
{

  // getting start time
  DGCgettime(m_timeBegin);

  cout<<"Starting TrajFollower(...)"<<endl;

  DGCcreateMutex(&m_trajMutex);
  DGCcreateMutex(&m_speedCapMutex);
  DGCcreateMutex(&m_reverseMutex);
  DGCcreateMutex(&m_historyMutex);

  //initializing variables to make valgrind happy =)
  m_trajCount = 0;
  m_nActiveCount = 0;
  m_steer_cmd = 0.0;
  m_accel_cmd = 0.0;
  m_speedCapMax = 100.0;
  m_speedCapMin = -1.0;
  m_drivesocket = m_skynet.get_send_sock(SNdrivecmd);
  reverse = false;
  reverseDist = 0.0;
  historyDist = 0.0;
  distSinceLastAdd = 0.0;
  pointIndex = 0;
  sizeIndex = 0;

  cout<<"historyDist @ line 69: "<<historyDist<<endl;

  //setting up logs
  logs_enabled = getLoggingEnabled();
  if(logs_enabled)
    { 
      logs_location = getLogDir();
    } 
  lindzey_logs_location = string(pLindzey);
  logs_newDir = checkNewDirAndReset();
  

  //determining where we calculate errors from
  EYerrorside yerrorside = (Y_FRONT==1 ? YERR_FRONT : YERR_BACK);
  EAerrorside aerrorside = (A_HEADING_FRONT==1 ? AERR_FRONT : ( A_HEADING_REAR==1 ? AERR_BACK : AERR_YAW));

  //creating the pidControllers
  m_pPIDcontroller = new CPID_Controller(yerrorside, aerrorside, LATERAL_FF_OFF == 0, logs_location, lindzey_logs_location, false);
  reversecontroller = new CPID_Controller(YERR_BACK, aerrorside, LATERAL_FF_OFF == 0, logs_location, lindzey_logs_location, true);

  cout<<"FINISHED CONSTRUCTING CONTROLLERS"<<endl;

  // 3rd order traj. If we've been passed a static path, use it
  if(pTrajfile[0] == '\0')
    {
      m_pTraj   = new CTraj(3);
      cout<<"no static path"<<endl;
    }
  else
    {
      m_pTraj   = new CTraj(3, pTrajfile);
      //cout<<"ln81 TF.cc, creating new trajectory with file: "<<pTrajfile<<endl;
      //need to set path, cuz in main function only reset when
      //new traj is received.  		
      m_pPIDcontroller->SetNewPath(m_pTraj, &m_state);
    }

  cout<<"TrajFollower(...) Finished"<<endl;
}

TrajFollower::~TrajFollower() 
{
  delete m_pTraj;
  delete m_pPIDcontroller;
  delete reversecontroller;
}

/******************************************************************************/
/******************************************************************************/
void TrajFollower::Active() 
{

  // timers to make sure our control frequency is what we set (not
  // affected by how long it takes to compute each cycle)
  unsigned long long timestamp1, timestamp2;



  cout<< "Starting Active Function" <<endl;

  drivecmd_t my_command;
  // init the cmd to 0 to appease the memprofiler gods
  memset(&my_command, 0, sizeof(my_command)); 

  UpdateState();
  lastN = m_state.Northing;
  lastE = m_state.Easting;


  while(!QUIT_PRESSED) 
    {
      // time checkpoint at the start of the cycle
      DGCgettime(timestamp1);

      // increment module counter
      m_nActiveCount++;

      //check logging status
      logs_enabled = getLoggingEnabled(); 
      logs_newDir = checkNewDirAndReset();
      if(logs_newDir) {
	setupLogFiles();
      }

      // Get state from the simulator or astate
      // note that this sets m_state 
      UpdateActuatorState();
      if(reverse==false) UpdateReverse();

      //calculations for the sparrowHawk display
  DGCgettime(grr_timestamp);

  UpdateState(grr_timestamp,true);



      //Calculate the required vehicle speed
      m_Speed2 = m_state.Speed2();
      //Calculate angular values in degrees
      m_PitchDeg = ( ( m_state.Pitch / M_PI ) * 180 );
      m_RollDeg = ( ( m_state.Roll / M_PI ) * 180 );
      m_YawDeg = ( ( m_state.Yaw / M_PI ) * 180 );
      m_PitchRateDeg = ( ( m_state.PitchRate / M_PI ) * 180 );
      m_RollRateDeg = ( ( m_state.RollRate / M_PI ) * 180 );
      m_YawRateDeg = ( ( m_state.YawRate / M_PI ) * 180 );


      /** if our speed was too low, set it as if we're moving in the same direction
       * at the minimum speed WITH THE WHEELS POINTED STRAIGHT */
      if( m_state.Speed2() < MIN_SPEED )
	{
	  m_state.Vel_N = MIN_SPEED * cos(m_state.Yaw);
	  m_state.Vel_E = MIN_SPEED * sin(m_state.Yaw);
	}



      /* Compute the control inputs. */
      if(reverse == true)
	{
	  //	  cout<<"asking for reverse control inputs"<<endl;
	  DGClockMutex(&m_trajMutex);
	  at_end = reversecontroller->getControlVariables(&m_state, &m_actuatorState, &m_accel_cmd, &m_steer_cmd, logs_enabled, logs_location, logs_newDir);
      if(at_end==true) {
	cout<<"reached end of traj!"<<endl;
#warning "this may not be the only place that this is needed"
	scMessage((int)completed_reversing_action);
      }      

	  DGCunlockMutex(&m_trajMutex);
	}
      else
	{
	  DGClockMutex(&m_trajMutex);
	  DGClockMutex(&m_speedCapMutex);
	  m_pPIDcontroller->getControlVariables(&m_state, &m_actuatorState, &m_accel_cmd, &m_steer_cmd, logs_enabled, logs_location, logs_newDir,m_speedCapMin,m_speedCapMax);
	  DGCunlockMutex(&m_speedCapMutex);
	  DGCunlockMutex(&m_trajMutex);
	}


      /**apply DFE to proposed steering commands before shipping them off to adrive 
       * please note that pidController returns steering command in radians */
      double safe_steer_cmd;
#ifdef USE_DFE
      safe_steer_cmd = DFE(m_steer_cmd, m_actuatorState.m_steerpos, &m_state);
#else 
      safe_steer_cmd = m_steer_cmd;
#endif

      double steer_Norm, accel_Norm;

      //compute normalized steering commands		
      steer_Norm = safe_steer_cmd/VEHICLE_MAX_AVG_STEER;
      steer_Norm = fmax(fmin(steer_Norm, 1.0), -1.0);

#ifdef NO_ACC_FF
      //bounding accelNorm by -1 and 1
      accel_Norm = fmax(-1.0, fmin(m_accel_cmd, 1.0));
#else
      accel_Norm = get_pedal(m_state.Speed2(), m_accel_cmd*VEHICLE_MASS, 1);
#endif


      //for data collection purposes...
      if(USE_HACK_STEER == true)
	{
	  steer_Norm = HACK_STEER_COMMAND;
	  accel_Norm = 0;
	}


      //shipping commands off to adrive
      my_command.my_command_type = set_position;

      my_command.my_actuator = steer;
      my_command.number_arg = steer_Norm;
      m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);
      my_command.my_actuator = accel;
      my_command.number_arg = accel_Norm;
      m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);
      //for testing purposes only!!
      //my_command.my_actuator = trans;
      //if(reverse == true) my_command.number_arg = -1;
      //else my_command.number_arg = 1;
      //m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);

      //broadcasts trajFollower status
      statusComm();

      // time checkpoint at the end of the cycle
      DGCgettime(timestamp2);

      /** now sleep for SLEEP_TIME usec less the amount of time the
       * computation took */
      int delaytime = SLEEP_TIME - (timestamp2 - timestamp1);
      if(delaytime > 0)
	usleep(delaytime);


    } // end while(!QUIT_PRESSED) 

  cout<< "Finished Active state" <<endl;
}







double TrajFollower::DFE(double proposed_cmd, double curr_steerpos, VehicleState* state)
{
  double safe_cmd;

  safe_cmd = proposed_cmd;

  return safe_cmd;
}


/**  
 *function: DFE
 *inputs: desired steering command (on a scale of -1 to 1)
 *action: compares desired steering angle to curr angle and speed, and
 *   places hard boundaries on the angle and angle's rate of change
 *   as a function of speed
 *outputs: new, bounded steering command
 **/
#warning "the DFE function in PID_Controller needs attention!"

//double CPID_Controller::DFE(double cmd, double old)
//{
  /** I'd like this to take in the actual steering angle, but since I 
   * don't currently have that we have to use previously commanded one instead
   */

//  double speed,diff, maxCmd, maxDiff,newCmd;
//  speed = hypot(m_vehDN[FRONT], m_vehDE[FRONT]);

//  diff = cmd-old;

  //these will be the functions of speed that limit

	/*
	 *               a=v*theta_dot             //for circular motion with
	 *   ->  theta_dot=a/v                     //with a and v scalar
	 *
	 *       theta_dot=v*sin(phi)/l            //from bicycle model
	 *   ->        a/v=v*sin(phi)/l
	 *   ->      |phi|=arcsin(a*l/v^2)         //for a*l/v^2<=1
	 *   ->      |phi|~=a*l/v^2                //for small phi
	 */
	//if(speed == 0) speed = .001;	
	//maxCmd = asin(VEHICLE_MAX_LATERAL_ACCEL*VEHICLE_WHEELBASE/(speed*speed));

//  maxCmd = 1;

  //this number comes from Jeff, based on the physical limits of the actuator
  //I may want to change it later, if we think that turning the steering wheel 
  // to quickly could lead to unaceptable forces
//  maxDiff = .1;
//  newCmd = cmd;

//  if(diff > maxDiff) 
//  {
    //cout<<"diff too large"<<endl;
//    newCmd = maxDiff+old;
//  }
//  if(diff < -maxDiff) 
//  {
    //cout<<"diff too small"<<endl; 
//    newCmd = -maxDiff+old;
//  }

//  if(newCmd > maxCmd)
//  {
//    if( DEBUG_LEVEL > 1 ) 
//    { 
//      cout<<"new command too large"<<endl; 
//    }
//   newCmd = maxCmd;
//  }    

// if(newCmd < -maxCmd) 
//  {
//    if( DEBUG_LEVEL > 1 ) 
//    { 
//      cout<<"new command too small"<<endl;
//    }
//    newCmd = -maxCmd;
//  }

//  return newCmd;

//}



/******************************************************************************/
/******************************************************************************/
void TrajFollower::Comm()
{
  int trajSocket;
  //int bytesLeft;
  //int bytesReceived;
  //int bytesToReceive;
  //char *pInTraj;
  //char* m_pDataBuffer;
  //int i;

  /** determines what type of trajs to listen for */
#ifdef USE_SNRDDFTRAJ
  trajSocket = m_skynet.listen(SNRDDFtraj, SNRddfPathGen);
#elif USE_SNREACTIVETRAJ
  trajSocket = m_skynet.listen(SNreactiveTraj, SNreactive);
#elif USE_SNROADFINDINGTRAJ
  trajSocket = m_skynet.listen(SNroadFinding, SNroadfinding);
#elif USE_SNSTATICTRAJ
  trajSocket = m_skynet.listen(SNstaticTraj, SNstaticpainter);
#else
  trajSocket = m_skynet.listen(SNtraj, SNplanner);
#endif
  
  while(!QUIT_PRESSED)
    {
      RecvTraj(trajSocket, m_pTraj);
	
      /** commenting this out as we're logging plans WAY too quickly.
      ** will make this an option soon, this is just a quick fix
      m_pTraj->print(m_outputPlans);
      m_outputPlans << endl;
      m_outputPlanStarts << m_pTraj->getNorthing(0) << ' '
      << m_pTraj->getEasting(0) << endl;
      */

	  //printf("PID with default traj\n");
	  DGClockMutex(&m_trajMutex);
	  m_pPIDcontroller->SetNewPath(m_pTraj, &m_state);
	  //this broadcasts the history so it gets displayed on the gui
	  //ReverseTrajGen((2*MAXHISTORYSIZE));
	  DGCunlockMutex(&m_trajMutex);
      m_trajCount++;
    }
}


void TrajFollower::superconComm()
{
  int bytesReceived;
  int bytesToReceive;
  char* m_pDataBuffer;
  superConTrajFcmd* command;
  int reverseSocket; 
  //double distancetogo;
  //int endpoint;
  //RDDFData firstpoint;
  //CTraj traj;
  reverseSocket = m_skynet.listen(SNsuperconTrajfCmd, MODsupercon);
  //cout <<"init superconcomm on socket: " << reverseSocket << endl;
  bytesToReceive = sizeof(superConTrajFcmd);
  m_pDataBuffer = new char[bytesToReceive];

  while(!QUIT_PRESSED)
    {
      bytesReceived = m_skynet.get_msg(reverseSocket, m_pDataBuffer, bytesToReceive, 0);
      cout<<"received reverse message"<<endl;
      //if we were previously in reverse, we need to update our buffer
      if(reverse==true) Resume();
      command = (superConTrajFcmd*)m_pDataBuffer;
      //printf("Got a: %d\n", (int)*m_pDataBuffer);
      if(bytesReceived != bytesToReceive)
	{
	  cerr << "Trajfollower::superconComm(): skynet error" << endl;
	}
      else if((*command).commandType == tf_forwards) //go forward again;
	{
	  //printf("got a forward!\n");
	  DGClockMutex(&m_reverseMutex);
	  reverse = false;
	  //printf("reverse is: %d\n", reverse);
	  DGCunlockMutex(&m_reverseMutex);
	}
      else if(((*command).commandType == tf_reverse)&& ((*command).distanceToReverse > 0)) // we need to reverse
	{
	  //Do not allow mode-changes *unless* Alice is e-stop paused
	  if( m_actuatorState.m_estoppos == ESTP_PAUSE ) {
	    
	    //printf("got a backward: %d\n", (int)*m_pDataBuffer);
	    DGClockMutex(&m_reverseMutex);
	    reverseDist = (*command).distanceToReverse;
	    reverse = true;
	    //printf("reverse is: %d\n", reverse);
	    //TODO adjust gains
	    reversetraj = ReverseTrajGen(reverseDist);
	    reversecontroller->SetNewPath(&reversetraj, &m_state);
	    DGCunlockMutex(&m_reverseMutex);

	  } else {
	    cout<< "ERROR: Mode change request received when Alice was NOT e-stop paused"<<endl;;
	    cerr << "ERROR: Mode change request received when Alice was NOT e-stop paused" << endl;
	  }
	}
      else
	{
	  cerr << "Trajfollower::superconComm(): invalid supercon message" << endl;
	}
      //printf("looping superconComm\n");
    }
  delete m_pDataBuffer;
}


void TrajFollower::statusComm()
{
  trajFstatusStruct status;
  int superconSocket;
  superconSocket = m_skynet.get_send_sock(SNtrajFstatus);

#warning "WTF chris?!?!? there's some logic error here....ln 437"
  if(reverse == false)
    {
      status.currentMode = tf_forwards;
    }
  else
    {
      status.currentMode = tf_reverse;
    }
  
  DGClockMutex(&m_trajMutex);
  status.currentVref = m_pPIDcontroller->access_VRef();
  DGCunlockMutex(&m_trajMutex);
  
  DGClockMutex(&m_speedCapMutex);
  status.largestMINspeedCap = m_speedCapMin;
  status.smallestMAXspeedCap = m_speedCapMax;
  DGCunlockMutex(&m_speedCapMutex);
  
  if(m_skynet.send_msg(superconSocket, &status, sizeof(status), 0) != sizeof(status))
    {
      cerr << "TrajFollower::statusCom: error: msg size does not match send msg size" << endl;
    }
}


void TrajFollower::speedCapComm()
{
  int bytesReceived;
  int bytesToReceive;
  newSpeedCapCmd m_DataBuffer;
  newSpeedCapCmd* m_pDataBuffer = &m_DataBuffer;
  int speedCapSocket; 
  double* module_status;

  //second argument ignored
  speedCapSocket = m_skynet.listen(SNtrajFspeedCapCmd, MODsupercon);
  //cout <<"init speedcapcomm on socket: " << speedCapSocket << endl;
  bytesToReceive = sizeof(newSpeedCapCmd);
  //m_pDataBuffer = new newSpeedCapCmd;

  //for each module, I'll need to store two values: curr min/max speed limits
  int num_modules = (int)NUM_SENDING_MODULES;
  module_status = new double[2*num_modules];

  //initialize array to -1.0 to represent no command
  for(int bar=1; bar <= num_modules; bar++) {
    module_status[2*bar-2] = -1.0;
    module_status[2*bar-1] = 100.0;
  }

  while(!QUIT_PRESSED)
    {
      //      cout<<"waiting for message..."<<endl;
      bytesReceived = m_skynet.get_msg(speedCapSocket, m_pDataBuffer, bytesToReceive, 0);
      ofstream outfil("recvd");
      outfil.write((char*)m_pDataBuffer, 16);
      outfil.close();
      cout<<"got a speedcap message"<<endl;
      //cout<<"happiness! I got a speedCap msg!"<<endl;
      //cout<<"received from module: "<<m_pDataBuffer->m_speedCapSndModule<<endl;
      //cout<<"received action: "<<m_pDataBuffer->m_speedCapAction<<endl;
      //cout<<"requested setting: "<<m_pDataBuffer->m_speedCapArgument<<endl;
      if((!USE_DBS) && (m_pDataBuffer->m_speedCapSndModule == TF_MOD_DBS))
	{
	  continue;
	}
      if(bytesReceived <= 0)
	{
	  cerr << "Trajfollower::speedCapComm(): skynet error" << endl;
	}
      else if(m_pDataBuffer->m_speedCapAction == add_max_speedcap)
	{
	  //don't need mutex cuz no other threads will be accessing this array =)
	  //DGClockMutex(&m_speedCapMutex);
          double val = m_pDataBuffer->m_speedCapArgument;
	  //cout << "setting max to: "<< val << endl;
           module_status[2*((int)m_pDataBuffer->m_speedCapSndModule)+1] = val;
	  //DGCunlockMutex(&m_speedCapMutex);
	}

      else if(m_pDataBuffer->m_speedCapAction == add_min_speedcap)
	{
	  //DGClockMutex(&m_speedCapMutex);
double minval = m_pDataBuffer->m_speedCapArgument;
//	  cout<<"a setting min to: " << minval << endl;
	  module_status[2*((int)m_pDataBuffer->m_speedCapSndModule)] = minval;
	  //DGCunlockMutex(&m_speedCapMutex);
	}

      else if(m_pDataBuffer->m_speedCapAction == remove_max_speedcap)
	{
	  //	  cout<<"remove_max_speedcap"<<endl;
	  //DGClockMutex(&m_speedCapMutex);
	  module_status[2*((int)m_pDataBuffer->m_speedCapSndModule)+1] = 100.0;
	  //DGCunlockMutex(&m_speedCapMutex);
	}

      else if(m_pDataBuffer->m_speedCapAction == remove_min_speedcap)
	{
	  //	  cout<<"remove_min_speedcap"<<endl;
	  //DGClockMutex(&m_speedCapMutex);
	  module_status[2*((int)m_pDataBuffer->m_speedCapSndModule)] = -1.0;
	  //DGCunlockMutex(&m_speedCapMutex);
	}
      
      double max = 100;
      double min = -1;
      
      //DGClockMutex(&m_speedCapMutex);
      
      //cout<<" calculating min and max"<<endl;
      for(int foo = 0; foo < num_modules; foo++)
	{
	  
	  //find max of the min speed caps
	  if (module_status[2*foo] > min)
	    min = module_status[2*foo];
	  
	  //find min of the max speed caps
	  if(module_status[2*foo+1] < max)
	    max = module_status[2*foo+1];
	  
	}
      //cout<<"min and max: " << min << ' ' << max << endl;
      //DGCunlockMutex(&m_speedCapMutex)
      //sets speed limit to value computed again
      
      DGClockMutex(&m_speedCapMutex);
      //cout<<"unlocked the mutex"<<endl;
      m_speedCapMin = min;
      m_speedCapMax = max;
      
      DGCunlockMutex(&m_speedCapMutex);
    }
  delete m_pDataBuffer;

}    


void TrajFollower::setupLogFiles()
{
  //cout<<"Entered setupLogFiles"<<endl;
  if(1) {
    //cout<<"new directory, resetting outptu files"<<endl;
    logs_location = getLogDir();

    char testFileName[256];

    char testFilePath[256];
	  
    sprintf(testFileName, "test.dat");

    //cout<<"trying to make stateFilePath"<<endl;
    string temp;
    temp = logs_location + testFileName;
    strcpy(testFilePath, temp.c_str());

    //    cout<<"plansFilePath = "<<plansFilePath<<endl;

     
      m_outputTest << setprecision(20);

    sprintf(testFileName, "test.dat");

    temp = logs_location + testFileName;
    strcpy(testFilePath, temp.c_str());

       m_outputTest << setprecision(20);
	
  }
}



void::TrajFollower::UpdateReverse()
{
  //  cout<<"entering UpdateReverse fxn"<<endl;
  double distTraveled = hypot(m_state.Northing-lastN, m_state.Easting-lastE);
  //distSinceLastAdd += distTraveled;
  cout<<setprecision(20);
  //  cout<<"state according to UpdateState() (N,e): "<<m_state.Northing<<' '<<m_state.Easting<<endl;

  //if we haven't traveled far enough since last added a point, return
  if(distTraveled > HISTORYSPACING)
  {
    //cout<<"time to add a new point!"<<endl;
    //if we reach this point, we've traveled far enough to need to add a new point
    //lock mutex because both the active and the superConComm thread can change the history
    DGClockMutex(&m_historyMutex);

  historyDist = fmin(MAXHISTORYSIZE*HISTORYSPACING, fmax(0, historyDist + distTraveled));

  //cout<<"historyDist @ line 658: "<<historyDist<<endl;


    pointIndex++;
    sizeIndex = min(sizeIndex+1, MAXHISTORYSIZE);

    //cout<<"pointIndex, sizeIndex: "<<pointIndex<<' '<<sizeIndex<<endl;

    //  cout<<"last northing and easting coordinates: "<<lastN<<' '<<lastE<<endl;
    historyN[pointIndex%MAXHISTORYSIZE] = lastN;
    historyE[pointIndex%MAXHISTORYSIZE] = lastE;
    historyDiff[pointIndex%MAXHISTORYSIZE] = distTraveled;
    DGCunlockMutex(&m_historyMutex);

  lastN = m_state.Northing;
  lastE = m_state.Easting;

  //    distSinceLastAdd = 0;
  }
  historyDist = fmin(MAXHISTORYSIZE*HISTORYSPACING, fmax(0, historyDist));
}



void TrajFollower::Resume()
{
  cout<<"Entering RESUME function!!!"<<endl;
  /**need to find the closest point BEHIND us in the history
   * traverse the history from current point backwards until the 
   * distance from point i-1 to curr pos is larger than the 
   * distance from point i to curr pos */

  double currDist, currDiff, nextDist, currN, currE, nextN, nextE;

  currN = historyN[pointIndex % MAXHISTORYSIZE];
  currE = historyE[pointIndex % MAXHISTORYSIZE];

  //incrementing the pointer because we want to construct the new buffer starting with 
  //the point furthest away (i.e. pointIndex + 1) 
  currDist = hypot((m_state.Northing - currN),
		   (m_state.Easting - currE));
  //cout<<"currentDist in resumeForward returns: "<<currDist<<endl;
  pointIndex--; 
  sizeIndex--; 
  nextN = historyN[pointIndex % MAXHISTORYSIZE];
  nextE = historyE[pointIndex % MAXHISTORYSIZE];
  historyDist -= historyDiff[pointIndex % MAXHISTORYSIZE];
  cout<<"historyDist @ line 698: "<<historyDist<<endl;

  nextDist = hypot((m_state.Northing - nextN),
		   (m_state.Easting - nextE));

  /**while we haven't yet wrapped around to the point closest to current state 
   * keep adding points to the tempHistory buffer */
  while(nextDist < currDist && sizeIndex > 0)
    {

      pointIndex--; 
      sizeIndex--; 
      currN = nextN;
      currE = nextE;
      nextN = historyN[pointIndex % MAXHISTORYSIZE];
      nextE = historyE[pointIndex % MAXHISTORYSIZE];
      historyDist -= historyDiff[pointIndex % MAXHISTORYSIZE];
  cout<<"historyDist @ line 715: "<<historyDist<<endl;
      currDist = nextDist;
      nextDist = hypot((m_state.Northing - nextN),
	        	   (m_state.Easting - nextE));

      //      cout<<"currentDist in resumeForward returns: "<<currDist<<endl;

    }
  //makes the last point in our history be our current position
  cout<<"historyDist @ line 724: "<<historyDist<<endl;
  historyDist += hypot(m_state.Northing - historyN[(pointIndex - 1)%MAXHISTORYSIZE], m_state.Easting-historyE[(pointIndex-1)%MAXHISTORYSIZE]);
  historyDist = fmin(MAXHISTORYSIZE*HISTORYSPACING, fmax(0, historyDist));
  cout<<"historyDist @ line 726: "<<historyDist<<endl;
  historyN[pointIndex % MAXHISTORYSIZE] = m_state.Northing;
  historyE[pointIndex % MAXHISTORYSIZE] = m_state.Easting;
  sizeIndex = min(sizeIndex+1, MAXHISTORYSIZE);
}

CTraj TrajFollower::ReverseTrajGen(double distance)
{

  //cout<<"have called Reverse Traj Gen with distance: "<<distance<<endl;
  //This code was shamelessly cut and pasted from RDDFPathGen
#warning "don't allocate this on the stack! too big!!"
  CTraj ptraj;
  corridorstruct corridor_whole;
  vector<double> location(2);
  int bufferLength;
  int index;
  int reverseSocket;
  location[1]=m_state.Easting;
  location[0]=m_state.Northing;



  reverseSocket =  m_skynet.get_send_sock(SNtrajReverse);
  
  if(distance > historyDist)
    {
#warning "should throw an exception to superCon here"
      cout<<"asked to back up too far. "<<endl;
      cout<<"Distance requested = "<<distance<<";   Distance in history: "<<historyDist<<endl;
      distance = historyDist;
    }
  if(pointIndex < 2 )
    {
      cout << "can't backup less than 1 meter" << endl;
	  scMessage((int)completed_reversing_action);    
#warning "do i need to initialize ptraj in some way before returning it??"
      return ptraj;
    }

  // store data in corridor

  int i = pointIndex;
  corridor_whole.numPoints = 1;
  double tempDistance = historyDiff[i%MAXHISTORYSIZE];
  corridor_whole.n.push_back(historyN[i%MAXHISTORYSIZE]);
  corridor_whole.e.push_back(historyE[i%MAXHISTORYSIZE]);
  i--;

  /**this creates the corridor from which we will create the RDDF */
  while(tempDistance < distance)
    {
      //cout<<"in ln 742 while loop..."<<endl;
#warning "this should be stored in the reverseHistory data structure!!"
      tempDistance += historyDiff[i%MAXHISTORYSIZE];
      //cout<<"ln 747"<<endl;
      corridor_whole.e.push_back(historyE[i%MAXHISTORYSIZE]);
      corridor_whole.n.push_back(historyN[i%MAXHISTORYSIZE]);
      ///      cout<<"pushing back: "<<reverseHistory[(2*i+1)%(2*MAXHISTORYSIZE)]<<endl;
      //cout<<"ln 750"<<endl;
     
#warning "should get rid of these magic numbers"
      corridor_whole.width.push_back(5);
      corridor_whole.speedLimit.push_back(5);
      corridor_whole.numPoints += 1;
      //cout<<"ln 756"<<endl;
      i--;
      //cout<<"ln 757"<<endl;
    }
  //cout<<"printing corridor"<<endl;
  //printCorridor(corridor_whole);
  //cout<<"have exited while loop...now at ln 756"<<endl;
  pathstruct& path_whole_sparse = *(new pathstruct);
  path_whole_sparse = Path_From_Corridor(corridor_whole);
  //printPath(path_whole_sparse);
  //cout<<"have now generated path_whole_sparse: "<<sizeof(path_whole_sparse)<<endl;
  path_whole_sparse.currentPoint = 0; // we haven't gone anywhere yet, so we're still at the start of the path
  //cout<<"now at line 765..."<<endl;
  // get the chopped dense traj (the one we send over skynet), in path format
  pathstruct path_chopped_dense = DensifyAndChop(path_whole_sparse, location, 0, distance + VEHICLE_AXLE_DISTANCE);
  //pathstruct path_chopped_dense = path_whole_dense;  /**< send whole each time */
  //cout<<"have now geneated path_chopped_dense"<<endl;
  StorePath(ptraj, path_chopped_dense);
  //cout<<"have copied RDDF path into ptraj"<<endl;
  SendTraj(reverseSocket, &ptraj);
  //cout<<"have sent traj!"<<endl;
  //printf("ReverseTrajGen exiting\n");
  delete &path_whole_sparse;
  return ptraj;
}

/**
set a separate variable to mark how many good data values there are in the ring.
this is easier then deleting the ones that don't work.
********/
