#ifndef CTIMBERCLIENT_HH
#define CTIMBERCLIENT_HH

#include <iostream>
#include <iomanip>
#include "sn_msg.hh"
#include <sys/types.h>
#include <pwd.h>
#include <ifaddrs.h>
#include <unistd.h>
#include <string>
#include <fstream>
#ifndef MACOSX
#include <net/if.h>
#endif
#include <string.h>
#include <sstream>
#include "DGCutils"
#include "CTimberTalker.hh"
#include "SkynetContainer.h"
#include "TimberConfig.hh"
#include <math.h>
#include <time.h>
#include <sstream>
#include <map>

// for checking if a directory exists
#include <sys/stat.h>
#include <unistd.h>


#ifdef MACOSX
#define IFF_UP  1
#endif 

using namespace std;


class CTimberClient : public CTimberTalker
{
public:
  /** One argument constructor -
   * This is the only constructor that people should use.
   * This constructor sets up the necessary mutexes, sets the debug level
   * to the default one (in TimberConfig.hh), stores the module name (passed
   * as an argument), and sets up a few additional private variables
   * (the timestamp, the log directory base...).  In addition, it sets
   * loggingenabled to false, so that when timber starts up, logging must
   * be manually started.
   *
   * @param module_enum The enum associated with the module that is deriving
   *                    from CTimberClient. */
  CTimberClient(int module_enum);

  /** Default Constructor - 
   * Never call this constructor.  Only initialize CTimberClient with the
   * one argument constructor. */
  CTimberClient();

  /** Destructor - 
   * Standard destructor. */
  ~CTimberClient();
  
  
  
  /***************************************
   * Accessors
   ***************************************/

  /** Accesses for the name of the module that has derived from CTimberClient.
   *
   * @return The name of the module (which was at argument to the constructor) */
  string getModuleName();

  /** Accesses the current debug level - see documentation for setDebugLevel.
   *
   * @return The current debug level. */
  double getDebugLevel();

  /** Returns whether or not logging is currently enabled.  This will be true
   * if the person running the timber module has started logging.  If logging
   * has not yet been enabled, modules that derive from CTimberClient CAN STILL
   * LOG DATA (in the standard way, by calling getLogDir()), but the timestamp on
   * the directory where the logs are stored will not be synchronized, making it
   * harder to analyze logs from multiple modules.
   *
   * @return Whether logging is currently enabled. */
  bool getLoggingEnabled();

  /** Returns the module specific logging level, as set throught the timber
   * module
   * 
   * @return The individual module's logging level. */
  int getMyLoggingLevel();

  /** Returns the current playback time.
   *
   * @return The current playback time, as an unsigned long long (units are microseconds). */
  unsigned long long getPlaybackTime(bool lock_state = true, unsigned long long real_time = 0);

  /** Returns the playback speed.
   *
   * @return The playback speed, as a fraction of real time.  i.e. a value of .5 means
   *         we're playing back at half speed. */
  double getPlaybackSpeed(bool lock_state = true);



  /***************************************
   * Mutators
   ***************************************/

  /** Changes the debug level.
   * The debug level will be initialized to some default value (DEFAULT_DEBUG from
   * TimberConfig.hh).  Calls to printDebug() will cause debug information to be 
   * printed out if the current debug level is greater than or equal to the debug
   * level required for that debug call.  Currently, the debug levels used range
   * from 0-2:
   *  0: no debug info
   *  1: sparing amounts of debug info
   *  2: craploads of debug info
   *
   * @param level The desired debug level. */
  void setDebugLevel(double level);

  /** Changes the name of the module.
   *
   * @param des_name The new name of the module (which will be used for which
   *                 folder to log to) */
  void setModuleName(string des_name);



  /***************************************
   * Other functions
   ***************************************/

  /** Gets the location of the current logging directory.
   * Calling getLogDir() will create a timestamped directory (if it doesn't exist)
   * to log to.  If getLogDir() is called when logging is enabled, then it will return
   * a nicely timestamped directory for module owners to put their logs in.  It
   * will also communicate back to the timber module the location of this folder
   * (which computer it's on and the path on that computer to the directory), 
   * so that later we can collect all the logs (currently using the grab_logs.sh
   * script)
   *
   * @return A string that contains the directory to log to.  Ends with a `/' */
  string getLogDir();

  /** Gets the location of the current playback directory.
   * Will check to make sure the directory exists.  If it doesn't, it will spit
   * out an error message and keep checking for the existance of the directory.
   * 
   * @return A string that contains the directory to play back from.  Ends with a `/' */
  string getPlaybackDir();

  /** Check to see if the logging directory has changed.
   * checkNewDirAndReset() will return true the first time it is called after a
   * new logging directory is created.  This enables users to close their old files
   * and reopen new ones using the new directory.  checkNewDirAndReset() should be
   * called right AFTER getLogDir(), because getLogDir() is what resets the `new
   * directory' condition.  To recap, checkNewDirAndReset() will return true the
   * FIRST time it's called after a new directory is created.
   *
   * @return Whether or not this is the first time we've seen this new logging directory.
   */
  bool checkNewDirAndReset();

  /** Check to see if the playback directory has changed.
   * checkNewPlaybackDirAndReset() will return true the first time it is called after a
   * new playback directory is received from the timber module.  Works the same way
   * as checkNewDirAndReset()
   *
   * @return Whether or not this is the first time we've seen this new playback directory.
   */
  bool checkNewPlaybackDirAndReset();

  //  int addFile(char* p_file_name); // need directory info as well...
  void addRelFolder(string rel_folder_name); // need directory info as well...
  //  int storeMyIp(char* p_dest);
  
  /** Method that will not return until the current playback time has reached the
   * destination_time. 
   *
   * @param destination_time The time at which blockUntilTime() will return.
   * @return TBD.*/
  int blockUntilTime(unsigned long long destination_time);
  
private:
  /**
   * First, a little documentation on the strategy for managing
   * the different states that the Timber Client may find 
   * itself in.
   *
   * I'll let the complete state be defined by two variables:
   * char* session_timestamp;
   * bool logging_enabled;
   *
   * Other variables depend directly on these, for example,
   * logging_enabled implies that session_timestamp exists and is valid.
   * That and other relations are presented below:
   * 
   * (logging_enabled) => (strlen(session_timestamp) != 0)
   * (strlen(session_timestamp) != 0) => logging directory exists
   * constructor called  => (strlen(module_name ) != 0)
   * constructor called  => (log_directory_base exists and ends with "/")
   *
   * Anytime the state is changed in such a way that breaks part of it
   * (for fixing in subsequent steps), the timber_state_mutex must be locked,
   * so that when the state is available, it's always "verified" or proper
   * or something like that
   */
  /** command line logging */
  pid_t m_pid;
  void writeCommandLine(char * logDir);
  /** timber_state */
  void lockState();
  void unlockState();
  pthread_mutex_t timber_state_mutex;
  char* session_timestamp;
  bool logging_enabled;
  int LEVEL_ARRAY_NAME[timber_types::LAST];
  // end timber state

  void lockName();
  void unlockName();
  pthread_mutex_t module_name_mutex;
  char* module_name;
  int module_enum_value;

  void lockDebug();
  void unlockDebug();
  pthread_mutex_t debug_level_mutex;
  double debug_level;

  void lockLastSeen();
  void unlockLastSeen();
  unsigned long long getLastSeen();
  void setLastSeen(unsigned long long des_time);
  pthread_mutex_t server_last_seen_mutex;
  unsigned long long server_last_seen;

  void lockLocalLogging();
  void unlockLocalLogging();
  bool getLocalLogging();
  void setLocalLogging(bool value);
  pthread_mutex_t local_logging_mutex;
  bool local_logging;

  void lockUpdatedDir();
  void unlockUpdatedDir();
  bool getUpdatedDir();
  void setUpdatedDir(bool value);
  pthread_mutex_t updated_dir_mutex;
  bool updated_dir; /**< used for checkNewDirAndReset() */

  char* log_directory_base; /**< ends with a "/", doesn't change after constructor */

  void RecvThread();
  void printDebug(string message, double req_level, bool newline = true);
  void createDirFromTimestamp();
  string getDirFromTimestamp(bool lock_state = true, bool with_base = true,
			     bool with_modulename = true);
  string getLogDirectoryBase();
  string getUser();
  string getIPString();
  bool connectionStatus();
  void assertThrow(bool good, string message) const { if(!good) throw message; }
  
  /** A struct to keep track of everything related to logging playback.*/
  struct PlaybackState
  {
    /** real_time_point and playback_time_point are the two coordinates that
     * represent a point in the 2 space of real time and playback time.  For example
     * if real_time_point = 1030 and playback_time_point = 30, then at an actual time
     * of 1030, our playback time is 30.  In reality, these numbers would be much
     * bigger, because they're unsigned long longs, and an increment of one in
     * either number corrosponds to an increase of one microsecond.*/
    unsigned long long real_time_point;
    unsigned long long playback_time_point;  /**< see documenation for real_time_point */

    /** playback_speed is the relative speed of our playback time with respect to
     * actual (computer clock) time.  If playback_speed = .5 for instance, then we're
     * playing back data at half speed.*/
    double playback_speed;

    /** string containing the location of the playback directory being used */
    string playback_location;

    /** lets us know if this playback location is new (used for
     * checkNewPlaybackDirAndReset()) */
    bool new_playback_location;
  };

  /** Keeps track of everything related to logging playback.  Basically this is
   * just a wrapper for a point-slope representation of how the actual (computer)
   * time and the playback time are related. */
  PlaybackState playback_state;
  void lockPlaybackState();
  void unlockPlaybackState();
  pthread_mutex_t playback_state_mutex;

};


#endif  // CTIMBERCLIENT_HH
