#include <unistd.h>
#include <iostream>
#include <sstream>

#include "plannerModule.hh"
#include "DGCutils"

#define MAX_DELTA_SIZE							100000
#define PLAN_TIME_TO_TAKE_SNAPSHOT  500000

CPlannerModule::CPlannerModule(int sn_key, bool bWaitForStateFill, bool bTakeSnapshots)
	: CSkynetContainer(SNplanner, sn_key),
		CStateClient(bWaitForStateFill),
		m_rddf("rddf.dat"),
		m_bReceivedAtLeastOneDelta(false),
		m_snapshotIndex(0),
		m_bTakeSnapshots(bTakeSnapshots),
		m_bSnapshotPlaybackMode(!bWaitForStateFill)
{
	readspecs();

	m_pMapDelta = new char[MAX_DELTA_SIZE];

	m_mapRequestSocket = m_skynet.get_send_sock(SNfullmaprequest);

	// constants in GlobalConstants.h
	m_map.initMap(CONFIG_FILE_DEFAULT_MAP);

	// Add the speed limit layer to the map
	m_mapLayer = m_map.addLayer<double>(CONFIG_FILE_COST);

	DGCcreateMutex(&m_mapMutex);
	DGCcreateMutex(&m_deltaReceivedMutex);
	DGCcreateCondition(&m_deltaReceivedCond);

	m_pPlanner = new CPlanner(&m_map, m_mapLayer, &m_rddf, false);
}

CPlannerModule::~CPlannerModule() 
{
	delete m_pMapDelta;
	delete m_pPlanner;
	DGCdeleteMutex(&m_mapMutex);
	DGCdeleteMutex(&m_deltaReceivedMutex);
	DGCdeleteCondition(&m_deltaReceivedCond);
}

void CPlannerModule::getMapDeltasThread()
{
  // The skynet socket for receiving map deltas (from fusionmapper)
	int mapDeltaSocket = m_skynet.listen(SNfusiondeltamap, SNfusionmapper);
	if(mapDeltaSocket < 0)
		cerr << "CPlannerModule::getMapDeltasThread(): skynet listen returned error" << endl;

	while(true)
	{
		int deltasize;
		RecvMapdelta(mapDeltaSocket, m_pMapDelta, &deltasize);

 		DGClockMutex(&m_mapMutex);
		m_map.applyDelta<double>(m_mapLayer, m_pMapDelta, deltasize);
 		DGCunlockMutex(&m_mapMutex);
		//		cerr << "Applied mapdelta" << endl;

		// set the condition to signal that the first delta was received
		if(!m_bReceivedAtLeastOneDelta)
			DGCSetConditionTrue(m_bReceivedAtLeastOneDelta, m_deltaReceivedCond, m_deltaReceivedMutex);
	}
}

void CPlannerModule::PlanningLoop(void) 
{
	int						trajSocket, trajSocketSeed, trajSocketInterm;
	int						planresult;

	CTraj         prevSuccessfulTraj;
	bool          bSentTraj = false;

	bool          bMadeGoodPlan;
	unsigned long long time1, time2;

	CTraj*        pPlannerTraj = m_pPlanner->getTraj();


	DGCgettime(time1);

	trajSocket       = m_skynet.get_send_sock(SNtraj);
	trajSocketSeed   = m_skynet.get_send_sock(SNtrajPlannerSeed);
	trajSocketInterm = m_skynet.get_send_sock(SNtrajPlannerInterm);

	cout << "Waiting for a delta map" << endl;

	// don't try to plan until at least a single plan is received
	DGCWaitForConditionTrue(m_bReceivedAtLeastOneDelta, m_deltaReceivedCond, m_deltaReceivedMutex);

	ofstream planfile("logs/pmplans.dat");
	ofstream seedfile("logs/pmseeds.dat");
	ofstream intermfile("logs/pminterms.dat");

  while(true)
	{
		readspecs();
		UpdateState();

		if(DOPLOT)
			snapshot();

		VehicleState lookaheadState = m_state;
		int closestpoint = prevSuccessfulTraj.getClosestPoint(m_state.Northing_rear(), m_state.Easting_rear());


		if(m_bSnapshotPlaybackMode)
		{
			DGClockMutex(&m_mapMutex);
			planresult = m_pPlanner->plan(&m_state, false);
			DGCunlockMutex(&m_mapMutex);
			bMadeGoodPlan = (planresult == 0 || planresult == 4 || planresult == 9);
		}
		else if(NO_LOOKAHEAD ||
						!bSentTraj ||
						hypot(prevSuccessfulTraj.getNorthing(closestpoint) - m_state.Northing_rear(), prevSuccessfulTraj.getEasting(closestpoint) - m_state.Easting_rear()) > LOOKAHEAD_DIST_PROXIMITY ||
						fabs(hypot(prevSuccessfulTraj.getNorthingDiff(closestpoint,1), prevSuccessfulTraj.getEastingDiff(closestpoint, 1)) - m_state.Speed2()) > LOOKAHEAD_SPEED_PROXIMITY 
					 )
		{
			DGClockMutex(&m_mapMutex);
			planresult = m_pPlanner->plan(&m_state);
			bMadeGoodPlan = (planresult == 0 || planresult == 4 || planresult == 9);

			if(!bMadeGoodPlan)
				snapshot();

			DGCunlockMutex(&m_mapMutex);
		}
		else
		{
			VehicleState lookaheadStateRear = m_state;
			double lookaheadDist = PLANNING_LOOKAHEAD * m_state.Speed2();
      int lookaheadPoint = closestpoint + lround( lookaheadDist / prevSuccessfulTraj.getLength() * (double)prevSuccessfulTraj.getNumPoints());

			lookaheadPoint = min(lookaheadPoint, prevSuccessfulTraj.getNumPoints()-1);

			lookaheadStateRear.Northing = prevSuccessfulTraj.getNorthingDiff(lookaheadPoint, 0);
			lookaheadStateRear.Easting  = prevSuccessfulTraj.getEastingDiff (lookaheadPoint, 0);

			lookaheadStateRear.Vel_N    = prevSuccessfulTraj.getNorthingDiff(lookaheadPoint, 1);
			lookaheadStateRear.Vel_E    = prevSuccessfulTraj.getEastingDiff (lookaheadPoint, 1);
			lookaheadStateRear.Yaw      = atan2(lookaheadStateRear.Vel_E, lookaheadStateRear.Vel_N);

			lookaheadStateRear.Acc_N    = prevSuccessfulTraj.getNorthingDiff(lookaheadPoint, 2);
			lookaheadStateRear.Acc_E    = prevSuccessfulTraj.getEastingDiff (lookaheadPoint, 2);

			lookaheadStateRear.YawRate  = lookaheadStateRear.YawRateEstimate();

			DGClockMutex(&m_mapMutex);
			cout << "starting to plan" << endl;
			planresult = m_pPlanner->plan(&lookaheadStateRear, false);
			cout << "finished plan" << endl;
			bMadeGoodPlan = (planresult == 0 || planresult == 4 || planresult == 9);

			if(!bMadeGoodPlan)
			{
				m_state = lookaheadStateRear;
				snapshot();
			}

			DGCunlockMutex(&m_mapMutex);
			pPlannerTraj->prepend(&prevSuccessfulTraj,
														max(0, closestpoint-MAX_POINTS_TO_PREPEND), lookaheadPoint-1);
		}

		if(SEND_SEED_TRAJ)
			SendTraj(trajSocketSeed,   m_pPlanner->getSeedTraj());
		if(LOG_SEED_TRAJ)
		{
			m_pPlanner->getSeedTraj()->print(seedfile);
			seedfile << endl;
		}

		if(SEND_INTERM_TRAJ)
			SendTraj(trajSocketInterm, m_pPlanner->getIntermTraj());
		if(LOG_INTERM_TRAJ)
		{
			m_pPlanner->getIntermTraj()->print(intermfile);
			intermfile << endl;
		}

		if(bMadeGoodPlan)
		{
			cerr << "succeeded: " << planresult << endl;
			prevSuccessfulTraj = *pPlannerTraj;

			if(SEND_FINAL_TRAJ)
			{
				SendTraj(trajSocket, pPlannerTraj);
			}

			bSentTraj = true;
		}
		else
		{
			if(SEND_FINAL_TRAJ && SEND_FAILURES)
			{
				SendTraj(trajSocket, pPlannerTraj);
			}
			cerr << "failed: " << planresult << endl;

			if(SPEED_OPTIMIZE_FAILURES && bSentTraj)
				m_pPlanner->getVProfile(&prevSuccessfulTraj);
		}

		if(LOG_FINAL_TRAJ)
		{
			pPlannerTraj->print(planfile);
			planfile << endl;
		}

		DGCgettime(time2);
		if(time2 - time1 > PLAN_TIME_TO_TAKE_SNAPSHOT && bMadeGoodPlan)
		{
			snapshot();
		}

		if(time2 - time1 < MIN_PLANNING_CYCLETIME_US)
			usleep(MIN_PLANNING_CYCLETIME_US - (time2 - time1));
		DGCgettime(time1);
	}
}

void CPlannerModule::requestFullMap()
{
	bool bRequestMap = true;
	m_skynet.send_msg(m_mapRequestSocket,&bRequestMap, sizeof(bool) , 0);
}


void CPlannerModule::snapshot(void)
{
	if(m_bTakeSnapshots)
	{
		ostringstream mapName, stateName;
		mapName   << "map" << m_snapshotIndex;
		stateName << "st"  << m_snapshotIndex;

		m_map.saveLayer<double>(m_mapLayer, mapName.str().c_str(), true);
		ofstream outstate(stateName.str().c_str());
		outstate.write((char*)&m_state, sizeof(m_state));

		m_snapshotIndex++;
	}
}
