/**
 * main.cc
 * Revision History:
 * 01/21/2005  hbarnor  Created
 */

#include <getopt.h>
#include <assert.h>
#include <limits.h>

#include "MapDisplaySN/MapDisplaySN.hh"
#include "MapDisplay/MapConfig.hh"
#include "MainDisplay/GuiThread.h"
#include "MainDisplay/gui.h"

int NOSTATE   = 0;       // Don't update state if = 1.
int OPTKEY = -1;
int NOWAIT = 0;          // by the default, wait for state to fill
int HIGHRES = 1;
int SHIFT = 0;
int DEFCONFIG = 0; // use default configfile
/* The name of this program. */
const char * program_name;
char  configfile[PATH_MAX];
/* Prints usage information for this program to STREAM (typically stdout
   or stderr), and exit the program with EXIT_CODE. Does not return. */
void print_usage (FILE* stream, int exit_code)
{
  fprintf( stream, "Usage:  %s [options]\n", program_name );
  fprintf( stream,
           "  --nostate         Continue even if state data is not received.\n"
           "  --key -k          Use key .\n"
	   "  --config -c       Use specified configfile for starttab \n"
           "  --nowait          Do not wait for state to fill .\n"	
           "  --help, -h        Display this message.\n" 
	   "  --shift           Shift the gui's map for any map delta, not just those from fusionMapper\n"
	   "\n"
	   "Key-Bindings:       All are Ctrl+Something\n"
	   "  q, w              Switch focus to Colormap Center or Range spin boxes (respectively)\n"
	   "  e, r              Toggle auto-range and auto-center of colormap (respectively)\n"
	   "  o, p              Write gui log and find CMap (respectively)\n"
	   "  i                 Toggle drawing CMap\n"
	   "  a, s, d           Timber Playback: Resume, Pause, and Replay (respectively)\n"
	   "  f, g, h           Timber Logging: Start, Stop, and Restart (respectively)\n"
	   "  j, k              Clear current layer and all layers (respectively)\n"
	   "  l                 Switch focus to layer spin box (respectively)\n"
	   "  z, x, c, v, b, n  Toggle drawing grid, sensors, waypts, RDDF, paths, info (respectively)\n"
	   "\n");
  exit(exit_code);
}

int main(int argc, char *argv[])
{
  //initilalize config file
  strcpy(configfile,"modules.config");
  /* Set the default arguments that won't need external access here. */
 
  /* Temporary character. */
  int ch;
  /* A string listing valid short options letters. */
  const char* const short_options = "hk:c:";
  /* An array describing valid long options. */
  static struct option long_options[] = 
  {
    // first: long option (--option) string
    // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
    // third: if pointer, set variable to value of fourth argument
    //        if NULL, getopt_long returns fourth argument
    {"nostate",    no_argument,        &NOSTATE,          1},
    {"key",        required_argument,  NULL,              'k'},
    {"snkey",      required_argument,  NULL,              'k'},
    {"config",     required_argument,  NULL,              'c'},
    {"nowait",     no_argument,        &NOWAIT,           1},
    {"help",       no_argument,        NULL,              'h'},
    {"lowres",     no_argument,        &HIGHRES,          0},
    {"shift",      no_argument,        &SHIFT,            1},
    {0,            0,                  0,                 0}
  };

  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  program_name = argv[0];
  printf("\n");
  
  // Loop through and process all of the command-line input options.
  while((ch = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
  {
    switch(ch)
    {
    case 'h':
      /* User has requested usage information. Print it to standard
	 output, and exit with exit code zero (normal 
	 termination). */
    case '?': /* The user specified an invalid option. */
      /* Print usage information to standard error, and exit with exit
	 code one (indicating abnormal termination). */      
      print_usage(stdout, 0);
      break;    	
    case 'k':
      OPTKEY = atoi(optarg);
      break;
    case 'c':
      sprintf(configfile, "%s", optarg);
      break;
    case -1: /* Done with options. */
      break;
    }
  }
  int sn_key = 0;

	if(OPTKEY == -1)
	{
		cerr << "Searching for skynet KEY " << endl;	  
		char* pSkynetkey = getenv("SKYNET_KEY");
		if( pSkynetkey == NULL )
		{
			cerr << "SKYNET_KEY environment variable isn't set" << endl;
		}
		else
			sn_key = atoi(pSkynetkey);
	}
	else
	{
		sn_key = OPTKEY;
	}
	cerr << "Constructing skynet with KEY = " << sn_key << endl;
	cerr << "Starting skynet " << endl;
	
	// mapconfig stuff 
	GuiThread * myGui;
	MapConfig * mc = new MapConfig();

	// Set the RDDF in the MapConfig
	RDDF rddf("rddf.dat");
	RDDFVector rddf_vector = rddf.getTargetPoints();
	mc->set_rddf( rddf_vector );    
	MapDisplaySN mapDisplay(mc, sn_key, NOWAIT == 0, HIGHRES, SHIFT);
	mapDisplay.init();    
	//now run gui thread
	myGui  = new GuiThread(mc, sn_key);      
	myGui->getGui()->buildStartTab(configfile);
	myGui->getGui()->initGui();;      
	myGui->guithread_start_thread();
	cout << "Spawned GUI Thread " << endl;
	// start a thread for showing all of the traj's except for the history traj.
	// this one is handled inside getStateThread. feel free to make this cleaner
	for(int i=1; i<PATHIDX_NUMPATHS; i++)
	{
		DGCstartMemberFunctionThreadWithArg(&mapDisplay,&MapDisplaySN::getTrajThread, (void*)i);
	}

	for(int i=0; i<mapDisplay.numLayers; i++) 
	{
		DGCstartMemberFunctionThreadWithArg(&mapDisplay,&MapDisplaySN::getMapDeltaThread, (void*)i);
	}

	mapDisplay.getStateThread();
    
	delete mc;
	delete myGui;
	cerr << "Done .. Exiting ..." << endl;
	return 0;
}
