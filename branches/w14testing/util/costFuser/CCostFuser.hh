#ifndef __CCOSTFUSER_HH__
#define __CCOSTFUSER_HH__

#include <stdlib.h>
#include <math.h>

#include "CMap.hh"
#include "CMapPlus.hh"
#include "MapConstants.h"


#define MAX_INPUT_COST_LAYERS 6


struct CCostFuserOptions {
	double maxSpeed;

	int maxCellsToInterpolate;
	int minNumSupportingCellsForInterpolation;

	int paintRoadWithoutElevation;
};


class CCostFuser {
 public:

	CCostFuser(CMapPlus* outputMap, int layerNumFinalCost,
						 CMapPlus* rddfMap, int layerNumRDDF,
						 CCostFuserOptions* options);
	~CCostFuser();

		enum STATUS {
		OK,
		ERROR
	};

	int addLayerElevCost(CMapPlus* inputMap, int layerNumElevCost, double relWeight);
	STATUS addLayerRoad(CMapPlus* inputMap, int layerNumRoad);
	STATUS addLayerSuperCon(CMapPlus* inputMap, int layerNumSuperCon);

	STATUS markChangesCost(NEcoord point);
	STATUS markChangesTerrain(int layerIndex, NEcoord point);

	STATUS fuseChangesCost();
	STATUS fuseChangesTerrain();
	STATUS interpolateChanges();

	STATUS markChangesCostAll();

	STATUS fuseCellTerrain(NEcoord point);
	STATUS fuseCellCost(NEcoord point);
	STATUS interpolateCell(NEcoord point);

  void paintRoadCell(double* terrainSpeed, double* roadSpeed, double* roadScaling, double* noData);

	CCostFuserOptions* _options;

	int getNumCellsFused();

	int _numElevLayers;
	CMapPlus* _mapPtrsElevCost[MAX_INPUT_COST_LAYERS];
	int _layerNumsElevCost[MAX_INPUT_COST_LAYERS];
	double _relWeights[MAX_INPUT_COST_LAYERS];

	int _layerNumRDDF;
	CMapPlus* _mapRDDF;

	int _layerNumRoad;
	CMapPlus* _mapRoad;

	int _layerNumSuperCon;
	CMapPlus* _mapSuperCon;

	CMapPlus* _mapOutput;
	int _layerNumCombinedElevCost;
	int _layerNumOutputCost;
	int _layerNumElevChanges;
	int _layerNumCostChanges;
	int _layerNumInterpolationChanges;

	int _numCellsFused;
};

#endif
