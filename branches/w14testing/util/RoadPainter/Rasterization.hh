#ifndef RASTERIZATION_HH
#define RASTERIZATION_HH

//Declares the functionality of rasterization classes


#include<iostream>
#include<iomanip>

#include"profiler.hh"

//A limit on the maximal number of polygon edges
#define RASTERIZE_MAX_POLYGON_EDGES  20

//When defined outputs a bunch of debug messages
//#define PROFILER_DEBUG_ALL

struct Point
{
  double x;
  double y;

  Point() : x(0), y(0) {}
  Point(int xt, int yt) : x(xt), y(yt) {}
  Point(const Point &pt) : x(pt.x), y(pt.y) {}
  Point &operator=(const Point &pt) {
    if(this==&pt)
       return *this;
  
    x = pt.x;
     y = pt.y;
    return *this;
  }
};

//Polygon class
class Polygon
{
public:
  Polygon() : m_size(0) {}
  Polygon(int size) : m_size(size) {}
  Polygon(const Polygon &p) {
    for(int i=0; i<RASTERIZE_MAX_POLYGON_EDGES+1; ++i)
      m_pt[i] = p.m_pt[i];

    m_size = p.m_size;
  }
  void resize(int size) { m_size=size; }

  int size() const { return m_size; }

  Point &operator[](int i) { return m_pt[i]; }
  const Point &operator[](int i) const { return m_pt[i]; }

  //Make the polygon closed and ccw and w first point being bottom right
  bool make_ccw_closed(Polygon &out) const {
    //Check rotation of the polygon
    bool ccw=true;
    bool cw=true;
    
    for(int i=0; i<m_size-2; ++i) {
      if(turn_left(m_pt[i+1].x-m_pt[i].x,m_pt[i+1].y-m_pt[i].y, m_pt[i+2].x-m_pt[i+1].x,m_pt[i+2].y-m_pt[i+1].y)) {
	cw=false;
	//cerr<<"!cw : "<<i<<": "<<m_pt[i].x<<", "<<m_pt[i].y<<" -> "<<m_pt[i+1].x<<", "<<m_pt[i+1].y<<" -> "<<m_pt[i+2].x<<", "<<m_pt[i+2].y<<endl;
      }
      if(turn_right(m_pt[i+1].x-m_pt[i].x,m_pt[i+1].y-m_pt[i].y, m_pt[i+2].x-m_pt[i+1].x,m_pt[i+2].y-m_pt[i+1].y)) {
	ccw=false;
	//cerr<<"!ccw : "<<i<<": "<<m_pt[i].x<<", "<<m_pt[i].y<<" -> "<<m_pt[i+1].x<<", "<<m_pt[i+1].y<<" -> "<<m_pt[i+2].x<<", "<<m_pt[i+2].y<<endl;
      }
    }
    if(!cw && !ccw) {
      //Polygon is neither clock-wise or counter-clock-wise,
      //probably due to polygon not being convex
      #ifdef PROFILER_DEBUG_ALL
        cerr<<"Polygon is both clock-wise and counter clock-wise, ie not convex!\n"<<endl;
        cerr<<"  Poly:";
        for(int i=0; i<m_size; ++i)
	  cerr<<" ("<<setprecision(10)<<m_pt[i].x<<", "<<setprecision(10)<<m_pt[i].y<<")";
        cerr<<endl;
      #endif
      return false;
    }
    
    if(cw && ccw) {
      //Is both cw and ccw, must be a that all pixels are on a line
      //and then rasterization direction doesn't matter
      cw=false;
    }
    
    //Find bottom right point
    int br = 0;
    for(int i=1; i<m_size; ++i) {
      if(m_pt[i].y<m_pt[br].y) {
	br=i;
      } else if(m_pt[i].y==m_pt[br].y && m_pt[i].x>m_pt[br].x) {
	br=i;
      }
    }
    
    //Rotate polygon to begin at bottom right and be ccw
    out.resize( m_size + 1);

    int j=br;
    for(int i=0; i<out.size(); ++i) {
      out[i].x = m_pt[j].x;
      out[i].y = m_pt[j].y;
      
      if(ccw) {
	++j;
	if(j>=m_size)
	  j=0;
      } else {
	--j;
	if(j<0)
	  j=m_size-1;
      }
    }
    return true;
  }

protected:
  //Check which way a vector turns using the sign of the cross product
  bool turn_left(double x1, double y1, double x2, double y2) const {
    return (x1*y2)-(y1*x2) > 0;
  }
  bool turn_right(double x1, double y1, double x2, double y2) const {
    return (x1*y2)-(y1*x2) < 0;
  }

protected:
  Point m_pt[RASTERIZE_MAX_POLYGON_EDGES+1];

  int m_size;
};


//Converts a convex polygon to functor calles for every rasterized pixel
//The coordinate system is defined as follows
//     y        
//    /\           |
//     |           |
//     |           |
//     \-----> x   |

class CRasterization
{
 public:
  CRasterization() : m_line_segment_alloc(0), m_line_segment(0) {}
  ~CRasterization() {
    if(m_line_segment)
      delete []m_line_segment;
    m_line_segment = 0;
    m_line_segment_alloc = 0;
  }

  //Rasterize the supplied convex polygon and send all pixels to be drawn
  //to point_drawer functor.
  //The polyon should be supplied without a closing point
  //(i.e !(x[last]==x[first] && y[last]==y[first])  )
  //The x_unit and y_unit defines the unit size, ie the size of every pixel
  //to be written
  template<class T>
  bool rasterize(const Polygon &poly, double x_unit, double y_unit, T &point_drawer);

 protected:   //Helper functions
  //Perform the polygon -> scan line conversion
  //Input is a closed(!) polygon with the first point being the bottom right one
  void scan_line_convert()
  {
    CProfile pf("Scanline convert");

    int p;
    double y = m_poly[0].y;
    int line=0;

    //convert right sides
    for(p=0; p<m_poly.size()-1; ++p) {
      //Check if the line segment has turned down,
      //thus we want to paint the left side
      if(m_poly[p+1].y <= m_poly[p].y)
	break;

      //Slope of this line segment
      double k = (m_poly[p+1].x-m_poly[p].x) / (m_poly[p+1].y-m_poly[p].y);

      //Calculate x position at y for this line segment
      //Don't want to change y as it steps in m_y_unit steps
      double x = (y-m_poly[p].y) * k + m_poly[p].x;

      k*=m_y_unit;

      for(; y<=m_poly[p+1].y; y+=m_y_unit, x+=k, ++line) {
	m_line_segment[line].right = x;
        #ifdef PROFILER_DEBUG_ALL
	  cerr<<"  right"<<p<<" "<<x<<", "<<y<<endl;
	#endif
      }
    }

    //Step back one on y so we are on the same line as where we ended
    y-=m_y_unit;
    --line;

    while(m_poly[p+1].y == m_poly[p].y)    //Skip all straight lines
      ++p;

    //convert left sides
    for(; p<m_poly.size()-1; ++p) {
      if(m_poly[p+1].y==m_poly[p].y)   //Last line segment is straight
	break;

      //Slope of this line segment
      double k = (m_poly[p+1].x-m_poly[p].x) / (m_poly[p+1].y-m_poly[p].y);

      //Calculate x position at y for this line segment
      //Don't want to change y as it steps in m_y_unit steps
      double x = (y-m_poly[p].y) * k + m_poly[p].x;

      k*=m_y_unit;

      for(; y>=m_poly[p+1].y; y-=m_y_unit, x-=k, --line) {
	m_line_segment[line].left = x;
	#ifdef PROFILER_DEBUG_ALL
	  cerr<<"  left"<<p<<" "<<x<<", "<<y<<endl;
	#endif
      }
    }
  }

  //Pixelate the scan lines in
  template<class T>
  void pixelate(T &point_drawer);

 protected:   //Helper data types
  struct LineSegment {
    double left;
    double right;
  };

 protected:   //Member data
  //Number of line segments allocated
  int m_line_segment_alloc;
  //Array of line segments (no vector due to no need for dynamic reallocation)
  LineSegment *m_line_segment;
  //y coordinate of first item in m_line_segments
  double m_line_segment_base;
  //Number of line segments used - (y_max - y_min) / y_unit
  int m_line_segment_cnt;

  //The size of the pixels to rasterize too
  double m_x_unit;
  double m_y_unit;

  //The (preprocessed) closed polygon used to rasterize
  Polygon m_poly;
};

template<class T>
bool CRasterization::rasterize(const Polygon &poly, double x_unit, double y_unit, T &point_drawer)
{
  if(poly.size() > RASTERIZE_MAX_POLYGON_EDGES || poly.size()<3 || x_unit<=0 || y_unit<=0)
    return false;
  
  if(!poly.make_ccw_closed(m_poly)) {
    //cerr<<"wrong dir"<<endl;
    return false;
  }

  #ifdef PROFILER_DEBUG_ALL
    cerr<<"  Rasterizing poly:";
    for(int i=0; i<m_poly.size(); ++i)
      cerr<<" ("<<setprecision(10)<<m_poly[i].x<<", "<<setprecision(10)<<m_poly[i].y<<")";
    cerr<<endl;
  #endif

  m_x_unit = x_unit;
  m_y_unit = y_unit;

  //Get the maximal point and calculate number of line segments needed
  double max_y = m_poly[0].y;
  for(int i=1; i<m_poly.size(); ++i) {
    if(m_poly[i].y>max_y)
      max_y = m_poly[i].y;
  }
  m_line_segment_cnt = (int) ((max_y - m_poly[0].y) / m_y_unit + 1);
  if(m_line_segment_cnt > m_line_segment_alloc || !m_line_segment) {
    //Realloc to have space for all line segments
    if(m_line_segment)
      delete []m_line_segment;
    m_line_segment = new LineSegment[m_line_segment_cnt];
    m_line_segment_alloc = m_line_segment_cnt;	
  }

  //Reset the top line segment as it may not always be used
  //(due to rounding errors), left=1, right=0 will make no pixels paint
  m_line_segment[m_line_segment_cnt-1].left = 1;
  m_line_segment[m_line_segment_cnt-1].right = 0;

  m_line_segment_base = m_poly[0].y;

  //Reset scanlines
  for(int i=0; i<m_line_segment_cnt; ++i) {
    m_line_segment[i].left = 0;
    m_line_segment[i].right = 0;
  }

  scan_line_convert();
  pixelate(point_drawer);

  return true;
}

//Pixelate the scan lines in
template<class T>
void CRasterization::pixelate(T &point_drawer)
{
  double y = m_line_segment_base;

  CProfile p("pixelate");
  
  for(int i=0; i<m_line_segment_cnt; ++i, y+=m_y_unit) {
    double xstart = round(m_line_segment[i].left/m_x_unit)*m_x_unit;  //Discretize
    for(double x=xstart; x<m_line_segment[i].right+m_x_unit; x+=m_x_unit) {
      point_drawer(x,y);
    }
  }
}


//Converts a convex polygon to functor calles for every rasterized pixel
//Function takes two polygons, where the pixels pointer the ones
//in the set p1-p2 (i.e the ones in p1 that are not in p2)
//The coordinate system is defined as follows
//     y        
//    /\           |
//     |           |
//     |           |
//     \-----> x   |
/*
class CRasterizationDiff
{
 public:
  CRasterizationDiff() : m_line_segment_alloc(0), m_line_segment(0) {}
  ~CRasterizationDiff() {
    if(m_line_segment)
      delete []m_line_segment;
    m_line_segment = 0;
    m_line_segment_alloc = 0;
  }

  //Rasterize the supplied convex polygon and send all pixels to be drawn
  //to point_drawer functor.
  //The polyon should be supplied without a closing point
  //(i.e !(x[last]==x[first] && y[last]==y[first])  )
  //The x_unit and y_unit defines the unit size, ie the size of every pixel
  //to be written
  template<class T>
  bool rasterize(const Polygon &poly, const Polygon &clip, double x_unit, double y_unit, T &point_drawer);

 protected:   //Helper functions
  //Perform the polygon -> scan line conversion
  //Input is a closed(!) polygon with the first point being the bottom right one
  void scan_line_convert()
  {
    int p;
    double y = m_poly[0].y;
    int line=0;

    int c=0;

    //convert right sides
    for(p=0; p<m_poly.size()-1; ++p) {
      //Check if the line segment has turned down,
      //thus we want to paint the left side
      if(m_poly[p+1].y <= m_poly[p].y)
	break;
      
      //Slope of this line segment
      double k = (m_poly[p+1].x-m_poly[p].x) / (m_poly[p+1].y-m_poly[p].y);

      //Calculate x position at y for this line segment
      //Don't want to change y as it steps in m_y_unit steps
      double x = (y-m_poly[p].y) * k + m_poly[p].x;

      k*=m_y_unit;

      for(; y<=m_poly[p+1].y; y+=m_y_unit, x+=k, ++line) {
	m_line_segment[line].right = x;
      }
    }

    //Step back one on y so we are on the same line as where we ended
    y-=m_y_unit;
    --line;

    c*=2;

    //convert left sides
    for(; p<m_poly.size()-1; ++p) {
      //Slope of this line segment
      double k = (m_poly[p+1].x-m_poly[p].x) / (m_poly[p+1].y-m_poly[p].y);

      //Calculate x position at y for this line segment
      //Don't want to change y as it steps in m_y_unit steps
      double x = (y-m_poly[p].y) * k + m_poly[p].x;

      k*=m_y_unit;

      for(; y>=m_poly[p+1].y; y-=m_y_unit, x-=k, --line) {
	m_line_segment[line].left = x;
      }
    }
  }

  void scan_line_convert_clip()
  {
    int p;
    double y = m_clip[0].y;
    int line=0;

    int c=0;

    //Find first polygon that goes into the region
    for(p=0; p<m_clip.size()-1; ++p) {
      if(m_clip[p+1].y <= m_clip[p].y)  //Turned down
	break;
      if(m_clip[p+1].y < m_line_segment_base)
	continue;
    }

    //convert right sides
    for(; p<m_clip.size()-1; ++p) {
      //Check if the line segment has turned down,
      //thus we want to paint the left side
      if(m_clip[p+1].y <= m_clip[p].y)
	break;
      
      //Already out of the region
      if(y > m_line_segment_top)
	continue;
      
      //Slope of this line segment
      double k = (m_poly[p+1].x-m_poly[p].x) / (m_poly[p+1].y-m_poly[p].y);

      //Calculate x position at y for this line segment
      //Don't want to change y as it steps in m_y_unit steps
      double x = (y-m_poly[p].y) * k + m_poly[p].x;

      k*=m_y_unit;

      for(; y<=m_poly[p+1].y; y+=m_y_unit, x+=k, ++line) {
	m_line_segment[line].clip_right = x;
      }
    }

    //Step back one on y so we are on the same line as where we ended
    y-=m_y_unit;
    --line;

    c*=2;

    //Find first polygon that goes into the region
    for(p=0; p<m_clip.size()-1; ++p) {
      if(m_clip[p+1].y > y)
	continue;
    }

    //convert left sides
    for(; p<m_poly.size()-1; ++p) {
      //Already out of the region
      if(y < m_line_segment_base)
	break;

      //Slope of this line segment
      double k = (m_poly[p+1].x-m_poly[p].x) / (m_poly[p+1].y-m_poly[p].y);

      //Calculate x position at y for this line segment
      //Don't want to change y as it steps in m_y_unit steps
      double x = (y-m_poly[p].y) * k + m_poly[p].x;

      k*=m_y_unit;

      for(; y>=m_poly[p+1].y; y-=m_y_unit, x-=k, --line) {
	m_line_segment[line].clip_left = x;
      }
    }
  }

  //Pixelate the scan lines in
  template<class T>
  void pixelate(T &point_drawer);

 protected:   //Helper data types
  struct LineSegment {
    double left;
    double right;

    double clip_left;
    double clip_right;
  };

 protected:   //Member data
  //Number of line segments allocated
  int m_line_segment_alloc;
  //Array of line segments (no vector due to no need for dynamic reallocation)
  LineSegment *m_line_segment;
  //y coordinate of first item in m_line_segments
  double m_line_segment_base;
  double m_line_segment_top;
  //Number of line segments used - (y_max - y_min) / y_unit
  int m_line_segment_cnt;

  //The size of the pixels to rasterize too
  double m_x_unit;
  double m_y_unit;

  //The (preprocessed) closed polygon used to rasterize
  Polygon m_poly;
  Polygon m_clip;
};

template<class T>
bool CRasterization::rasterize(const Polygon &poly, const Polygon &clip, double x_unit, double y_unit, T &point_drawer)
{
  if(poly.size() > RASTERIZE_MAX_POLYGON_EDGES || poly.size()<3 || x_unit<=0 || y_unit<=0)
    return false;
  if(clip.size() > RASTERIZE_MAX_POLYGON_EDGES || clip.size()<3)
    return false;
  
  if(!poly.make_ccw_closed(m_poly))
    return false;
  if(!clip.make_ccw_closed(m_clip))
    return false;

  m_x_unit = x_unit;
  m_y_unit = y_unit;

  //Get the maximal point and calculate number of line segments needed
  double max_y = m_poly[0].y;
  for(int i=1; i<m_poly.size(); ++i) {
    if(m_poly[i].y>max_y)
      max_y = m_poly[i].y;
  }

  m_line_segment_cnt = (int) ((max_y - m_poly[0].y) / m_y_unit + 1);
  if(m_line_segment_cnt > m_line_segment_alloc || !m_line_segment) {
    //Realloc to have space for all line segments
    if(m_line_segment)
      delete []m_line_segment;
    m_line_segment = new LineSegment[m_line_segment_cnt];
    m_line_segment_alloc = m_line_segment_cnt;	
  }

  //Reset the top line segment as it may not always be used
  //(due to rounding errors), left=1, right=0 will make no pixels paint
  m_line_segment[m_line_segment_cnt-1].left = 1;
  m_line_segment[m_line_segment_cnt-1].right = 0;

  m_line_segment_base = m_poly[0].y;
  m_line_segment_top = m_line_segment_base + m_y_unit*(m_line_segment_cnt-1);

  //Reset scanlines
#warning Shouldn t need to reset the scanlines
  for(int i=0; i<m_line_segment_cnt; ++i) {
    m_line_segment[i].left = 1;
    m_line_segment[i].right = 0;
    m_line_segment[i].clip_left = 1;
    m_line_segment[i].clip_right = 0;
  }

  scan_line_convert();
  scan_line_convert_clip();
  pixelate(point_drawer);

  return true;
}

//Pixelate the scan lines in
template<class T>
void CRasterization::pixelate(T &point_drawer)
{
  double y = m_line_segment_base;
  
  for(int i=0; i<m_line_segment_cnt; ++i, y+=m_y_unit) {
    if(m_line_segment[i].clip_left < m_line_segment[i].left) {

      //clipping is on the left side of us
      double x = max(m_line_segment[i].clip_right, m_line_segment[i].left);
      for(; x<=m_line_segment[i].right; x+=m_x_unit) {
	point_drawer(x,y);
      }
    } else if(m_line_segment[i].clip_right > m_line_segment[i].right) {

      //clipping is on the right side of us
      double xr = min(m_line_segment[i].clip_left, m_line_segment[i].right);
      for(double x=m_line_segment[i].left; x<=xr; x+=m_x_unit) {
	point_drawer(x,y);
      }
    } else {

      //Clipping is inside
      for(double x=m_line_segment[i].left; x<=m_line_segment[i].clip_left; x+=m_x_unit) {
	point_drawer(x,y);
      }
      for(double x=m_line_segment[i].clip_right; x<=m_line_segment[i].right; x+=m_x_unit) {
	point_drawer(x,y);
      }
    }
  }
}

*/

#endif
