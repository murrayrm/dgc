#ifndef _STATECLIENT_H_
#define _STATECLIENT_H_

#include <pthread.h>
#include <unistd.h>
#include <iostream>
#include <fstream>

#include "SkynetContainer.h"
#include "VehicleState.hh"
#include "ActuatorState.hh"
#include "DGCutils"

#define STATE_BUFFER_SIZE 100

class CStateClient : virtual public CSkynetContainer
{
  /*! This is the state that is received from skynet. This is actually locked
    and received. UpdateState() copies this data into m_stateBuffer */
  VehicleState    m_receivedState;

	/* Same as above for the actuatorstate */
  ActuatorState   m_receivedActuatorstate;

  /*! The mutices to protect the data */
  pthread_mutex_t m_stateBufferMutex;
  pthread_mutex_t m_actuatorstateMutex;

  /*! The function which continually updates the state when any is received from
    the network */
  void getStateThread();

  /*! Same as above, but for the actuator state */
	void getActuatorStateThread();


  void state_Interpolate_Cubic(double,double,double,double,unsigned long long, unsigned long long, unsigned long long,double&,double&,double&);

  void state_Interpolate_Linear(double,double,double,double,unsigned long long, unsigned long long, unsigned long long,double&,double&,double&);

  VehicleState   state_buffer[STATE_BUFFER_SIZE];

  int state_index;

  /*! The mutex and condition necessary to block on state being filled. */
  pthread_mutex_t m_filledStateConditionMutex;
  pthread_cond_t  m_filledStateCondition;
  bool            m_bFilledState;

protected:
	/*! Whether we wait for the state buffer to fill or not */
	bool m_bWaitForStateFill;

  /*! The state itself. This is the copy of the state data that's coming
    in. This is visible from the derived classes */
  VehicleState m_state;

  /*! same as above */
  ActuatorState m_actuatorState;


  bool m_bRunThreads;

  DGCcondition condNewState;
	DGCcondition condNewActuatorState;

public:
  CStateClient(bool bWaitForStateFill = true);
  ~CStateClient();

  void UpdateState(void);
  void UpdateState(unsigned long long time_req, bool linear = false);

  void WaitForNewState(void);

	void WaitForNewActuatorState(void);

  void UpdateActuatorState(void);

  /** Returns an array of the last N values of the state buffer.
   * Used for DBS.
   *
   * @param N the number of VehicleState's to get
   * @param dest the buffer to put the state's in.  must allocate
   *             memory for this before calling GetLastNValues */
  void GetLastNValues(int N, VehicleState* dest);
};

#endif // _STATECLIENT_H_
