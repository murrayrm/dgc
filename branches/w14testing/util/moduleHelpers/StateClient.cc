#include "StateClient.h"

using namespace std;

CStateClient::CStateClient(bool bWaitForStateFill)
  : m_bWaitForStateFill(bWaitForStateFill)
{
  cerr << "In CStateClient()" << endl;

  // initialize the mutexes and the condition variable
  DGCcreateMutex(&m_actuatorstateMutex);
  DGCcreateMutex(&m_stateBufferMutex);
  DGCcreateMutex(&m_filledStateConditionMutex);
  DGCcreateCondition(&m_filledStateCondition);
  DGCcreateCondition(&condNewState);
	DGCcreateCondition(&condNewActuatorState);

  m_bFilledState = false;
  memset(state_buffer, 0, sizeof(state_buffer));
  memcpy(&m_state, &(state_buffer[0]), sizeof(m_state));
  state_index = -1;

  m_bRunThreads = true;

  DGCstartMemberFunctionThread(this, &CStateClient::getStateThread);
  DGCstartMemberFunctionThread(this, &CStateClient::getActuatorStateThread);

  if(m_bWaitForStateFill)
    {
      cout << "Waiting for state data to fill..." << endl;
      DGCWaitForConditionTrue(m_bFilledState, m_filledStateCondition, m_filledStateConditionMutex);
      cout << "State data filled!" << endl;
    }

}

CStateClient::~CStateClient()
{
  m_bRunThreads = false;
  sleep(1);
  DGCdeleteMutex(&m_stateBufferMutex);
  DGCdeleteMutex(&m_filledStateConditionMutex);
  DGCdeleteCondition(&m_filledStateCondition);
  DGCdeleteCondition(&condNewState);
	DGCdeleteCondition(&condNewActuatorState);
}

void CStateClient::UpdateState()
{
  DGClockMutex(&m_stateBufferMutex);
  memcpy(&m_state, &(state_buffer[state_index]), sizeof(m_state));
  DGCunlockMutex(&m_stateBufferMutex);
}

void CStateClient::WaitForNewState()
{
  DGCWaitForConditionTrue(condNewState);
  UpdateState();
  condNewState.bCond = false;
}

void CStateClient::WaitForNewActuatorState()
{
  DGCWaitForConditionTrue(condNewActuatorState);
  UpdateActuatorState();
  condNewActuatorState.bCond = false;
}


void CStateClient::UpdateState(unsigned long long time_req, bool linear)
{
  DGClockMutex(&m_stateBufferMutex);

  if(time_req > state_buffer[state_index].Timestamp)
    {
      memcpy(&m_state, &state_buffer[state_index], sizeof(m_state));
      DGCunlockMutex(&m_stateBufferMutex);
      return;
    }

  int index_furthest_back;
  int index_above;
  int index_below;
  if(m_bFilledState)
    index_furthest_back = (state_index + 1) % STATE_BUFFER_SIZE;
  else
    index_furthest_back = 0;

  index_below = state_index;
  do
    {
      // if we wrapped around, return the oldest state we have
      if(index_below == index_furthest_back)
	{
	  memcpy(&m_state, &state_buffer[index_furthest_back], sizeof(m_state));
	  DGCunlockMutex(&m_stateBufferMutex);
	  return;
	}
      index_below = (index_below + STATE_BUFFER_SIZE - 1) % STATE_BUFFER_SIZE;
    } while(time_req < state_buffer[index_below].Timestamp);

  index_above = (index_below + 1) % STATE_BUFFER_SIZE;

  VehicleState vl = state_buffer[index_below];
  VehicleState vu = state_buffer[index_above];

  DGCunlockMutex(&m_stateBufferMutex);

  //Start by copying most recent info into m_state

  memcpy(&m_state, &vu, sizeof(m_state));

  m_state.Timestamp = time_req;

	double resX, resXd, resXdd;
  state_Interpolate_Cubic(vl.Northing, vl.Vel_N, 
													vu.Northing, vu.Vel_N, 
													vl.Timestamp, vu.Timestamp, time_req,
													resX, resXd, resXdd);
	m_state.Northing = resX; 
	m_state.Vel_N    = resXd;
	m_state.Acc_N    = resXdd;

  state_Interpolate_Cubic(vl.Easting, vl.Vel_E, 
													vu.Easting, vu.Vel_E, 
													vl.Timestamp, vu.Timestamp, time_req,
													resX, resXd, resXdd);
	m_state.Easting  = resX; 
  m_state.Vel_E		 = resXd;
  m_state.Acc_E		 = resXdd;

  state_Interpolate_Cubic(vl.Altitude, vl.Vel_D, 
													vu.Altitude, vu.Vel_D, 
													vl.Timestamp, vu.Timestamp, time_req,
													resX, resXd, resXdd);
	m_state.Altitude = resX;
	m_state.Vel_D    = resXd;
	m_state.Acc_D    = resXdd;

  if (linear)
    {
      state_Interpolate_Linear(vl.Roll, vl.RollRate, 
															 vu.Roll, vu.RollRate, 
															 vl.Timestamp, vu.Timestamp, time_req,
															 resX, resXd, resXdd);
			m_state.Roll      = resX;
			m_state.RollRate  = resXd;
			m_state.RollAcc   = resXdd;

      state_Interpolate_Linear(vl.Pitch, vl.PitchRate, 
															 vu.Pitch, vu.PitchRate, 
															 vl.Timestamp, vu.Timestamp, time_req,
															 resX, resXd, resXdd);
			m_state.Pitch      = resX;
			m_state.PitchRate  = resXd;
			m_state.PitchAcc   = resXdd;

    }
  else
    {
      state_Interpolate_Cubic(vl.Roll, vl.RollRate, 
															vu.Roll, vu.RollRate, 
															vl.Timestamp, vu.Timestamp, time_req,
															resX, resXd, resXdd);
			m_state.Roll       = resX;
			m_state.RollRate   = resXd;
			m_state.RollAcc    = resXdd;

      state_Interpolate_Cubic(vl.Pitch, vl.PitchRate, 
															vu.Pitch, vu.PitchRate, 
															vl.Timestamp, vu.Timestamp, time_req,
															resX, resXd, resXdd);
			m_state.Pitch      = resX;
			m_state.PitchRate  = resXd;
			m_state.PitchAcc   = resXdd;
    }

  state_Interpolate_Cubic(vl.Yaw, vl.YawRate, 
													vu.Yaw, vu.YawRate, 
													vl.Timestamp, vu.Timestamp, time_req,
													resX, resXd, resXdd);
	m_state.Yaw       = resX;
	m_state.YawRate   = resXd;
	m_state.YawAcc    = resXdd;
}


void CStateClient::state_Interpolate_Cubic(double val1, double val1_dot, double val2, double val2_dot, unsigned long long time1, unsigned long long time2, unsigned long long timereq, double& interp, double& interp_dot, double& interp_dot_dot)
{
  // finding a cubic a + b*t + c*t^2 + d*t^3 where a=val1
  double scale = 1000000.0 / (double)(time2 - time1);
  double val2_shift = val2 - val1;
  double b = (val1_dot / scale);
  double c = -2.0*(val1_dot / scale) + 3.0*val2_shift - 1.0*(val2_dot / scale);
  double d =  1.0*(val1_dot / scale) - 2.0*val2_shift + 1.0*(val2_dot / scale);
  double t = (double)(timereq - time1) / (double)(time2 - time1);
  interp         = (t*(b + t*(c + t*d))) + val1;
  interp_dot     = (b + t*(2.0*c + t*3.0*d)) * scale;
  interp_dot_dot = (2.0*c + t*6.0*d) * scale * scale;
}

void CStateClient::state_Interpolate_Linear(double val1, double val1_dot, double val2, double val2_dot, unsigned long long time1, unsigned long long time2, unsigned long long timereq, double& interp, double& interp_dot, double& interp_dot_dot)
{
  // finding a + b*t where a=val1 THIS FUNCTION IS A HUGE HACK AND SHOULDN'T BE
  // USED. the derivatives are from the cubic version
  double scale = 1000000.0 / (double)(time2 - time1);
  double val2_shift = val2 - val1;
  double b = (val1_dot / scale);
  double c = -2.0*(val1_dot / scale) + 3.0*val2_shift - 1.0*(val2_dot / scale);
  double d =  1.0*(val1_dot / scale) - 2.0*val2_shift + 1.0*(val2_dot / scale);
  double t = (double)(timereq - time1) / (double)(time2 - time1);
  interp         = val2_shift * t + val1;
  interp_dot     = (b + t*(2.0*c + t*3.0*d)) * scale;
  interp_dot_dot = (2.0*c + t*6.0*d) * scale * scale;
}

void CStateClient::getStateThread()
{
  int statesocket = m_skynet.listen(SNstate, ALLMODULES);

  while(m_bRunThreads)
    {
      if(m_skynet.get_msg(statesocket, &m_receivedState, sizeof(m_receivedState), 0) != sizeof(m_receivedState))
	cerr << "Didn't receive the right number of bytes in the state structure" << endl;

      DGClockMutex(&m_stateBufferMutex);
      state_index++; 
      if (state_index == STATE_BUFFER_SIZE)
	{
	  state_index = 0;
	  if(!m_bFilledState)
	    DGCSetConditionTrue(m_bFilledState, m_filledStateCondition, m_filledStateConditionMutex);
	}

      memcpy(&(state_buffer[state_index]), &m_receivedState, sizeof(m_state));
      DGCunlockMutex(&m_stateBufferMutex);
      DGCSetConditionTrue(condNewState);
    }
}

void CStateClient::UpdateActuatorState()
{
  DGClockMutex(&m_actuatorstateMutex);
  memcpy(&m_actuatorState, &m_receivedActuatorstate, sizeof(m_actuatorState));
  DGCunlockMutex(&m_actuatorstateMutex);
}

void CStateClient::getActuatorStateThread()
{
  // listen for state from everybody.
  int actuatorstatesocket    = m_skynet.listen(SNactuatorstate, ALLMODULES);

  pthread_mutex_t *pActuatorstateMutex = &m_actuatorstateMutex;
  while(m_bRunThreads)
    {
      if(m_skynet.get_msg(actuatorstatesocket, &m_receivedActuatorstate, sizeof(m_receivedActuatorstate), 0, &pActuatorstateMutex) !=
	 sizeof(m_receivedActuatorstate))
	{
	  cerr << "Didn't receive the right number of bytes in the actuatorstate structure" << endl;
	}
      DGCSetConditionTrue(condNewActuatorState);
    }
}



void CStateClient::GetLastNValues(int N, VehicleState* dest)
{
  if(N>100)
    {
      cerr<<"This is not aloud,the state client only have room for a maximum of 100 values"<<endl;
      exit (1);
    }

  if( m_bWaitForStateFill==false)
    {
      cerr << "you have to wait for the state client to get full" << endl;
      exit (2);
    }

  if (state_index>=N-1)  
    {
      //one chunk
      memcpy(dest,&(state_buffer[state_index-N+1]),N*sizeof(VehicleState));
    }
  else
    {
      //two chunks
      memcpy(&(dest[(N-1)-state_index]),&(state_buffer[0]),(state_index+1)*sizeof(VehicleState));
      memcpy(&(dest[0]),&(state_buffer[100-N+1+state_index]),(N-(state_index+1))*sizeof(VehicleState));
    

    }
}
