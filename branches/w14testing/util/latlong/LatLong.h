#ifndef LATLONG_H
#define LATLONG_H

#include <cmath>
#include <iostream>

const double PI = 3.1415926535;

// I found this feet to meters conversion at
// http://www.vcgi.org/techres/standards/partii_section_b.pdf
// On 8/7/2003.  It should be replaced with any more accurate or confident
// conversion factor found.
const double FT_TO_M = 0.3048006096;
const double M_TO_FT = 3.280833333;


// These constants define the default elipsoid to represent the earth.  These
// are the parameters defining the WGS84 convention, as stated in J. C.
// Iliffe's "Datums and Map Projections for Remote Sensing, GIS, and
// Surveying," which is available in Caltech's Geo Library (2nd floor between
// N. and S. Mudd).  **This datum is the one specified by the GPS as the datum
// that defines its GPS output!**  The data of the radius is meters, whereas
// the flatness is a unitless quantity.
const double DEFAULT_A = 6378137;
const double DEFAULT_F = (1.0 / 298.257223563);
const double DEFAULT_E = sqrt((2*DEFAULT_F) - (DEFAULT_F*DEFAULT_F));

// These define constants for several other elispoids used in debugging.  The
// first set of constants should define the NAD83 convention, the primary datum
// for latitude and longitude used in the DOQ maps we obtained (NOTE: these
// maps already come with UTM data, so this datum and this class is not
// required to read from the maps.  The second set of constants is the
// antiquated Clark-1866 elipsoid.
const double NAD83_A = 20925646.325 * FT_TO_M;
const double NAD83_E = 0.0818191910428158;
const double CLARK_66_A = 6378206.4;
const double CLARK_66_E = 0.082272;

class LatLong {
 private:

  double _a;         // The semi-major axis of the elipsoid representing earth.
  double _b;         // The semi-minor axis of the elipsoid representing earth.
  double _e;         // The eccentricity of the elipsoid representing earth.
  double _f;         // The flattening of the elipsoid representing earth.

  double _lat;
  double _lon;
  
 public:
  LatLong(double lat, double lon, double a = DEFAULT_A, double e = DEFAULT_E);
  LatLong(const LatLong& ll);
  
  LatLong m_left(const double n = 1) const;
  LatLong m_up(const double n = 1) const;
  LatLong m_right(const double n = 1) const;
  LatLong m_down(const double n = 1) const;
  LatLong km_left(const double n = 1) const;
  LatLong km_up(const double n = 1) const;
  LatLong km_right(const double n = 1) const;
  LatLong km_down(const double n = 1) const;


  /*
  LatLong m_north(const int n = 1) const;
  LatLong m_east(const int n = 1) const;
  LatLong m_south(const int n = 1) const;
  LatLong m_west(const int n = 1) const;
  LatLong km_north(const int n = 1) const;
  LatLong km_east(const int n = 1) const;
  LatLong km_south(const int n = 1) const;
  LatLong km_west(const int n = 1) const;
  */

  double get_lat() const;
  double get_lon() const;
  double get_a() const;
  double get_b() const;
  double get_e() const;
  double get_f() const;
  void set_lat(const double lat);
  void set_lon(const double lon);
  void set_latlon(const double lat, const double lon);
  int get_zone() const;
  
  bool get_UTM(double* easting, double* northing, double* k = NULL) const;
  bool get_latlong(double* lat, double* lon);
  bool set_to_UTM(const double easting, const double northing, int zone);
}; 

#endif   // LATLONG_H
