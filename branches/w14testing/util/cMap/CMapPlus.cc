#include "CMapPlus.hh"


CMapPlus::CMapPlus() {
  layerLogFilename = (char**)malloc(0);
  layerLogFiles = (ofstream**)malloc(0);
  layerLogStatus = (int*)malloc(0);
  layerLabels = (string**)malloc(0);
}


CMapPlus::~CMapPlus() {

}


int CMapPlus::initLayerLog(int layerNum, char* layerFilename) {
  layerLogFilename[layerNum] = layerFilename;

  layerLogFiles[layerNum] = new ofstream;

  layerLogFiles[layerNum]->open(layerLogFilename[layerNum]);

  if(!*(layerLogFiles[layerNum])) {
    printf("ERROR OPENING FILE %s\n", layerLogFilename[layerNum]);
  }

  layerLogStatus[layerNum] = CMP_LOGGING_PAUSED;

  *(layerLogFiles[layerNum]) << COMMENT_CHAR << " This is a CMap Log!" << endl;
  *(layerLogFiles[layerNum]) << COMMENT_CHAR << " The log format:" << endl;
  *(layerLogFiles[layerNum]) << COMMENT_CHAR << " HEADERS: " << endl;
  *(layerLogFiles[layerNum]) << COMMENT_CHAR << " " << CMP_LOG_MSG_HEADER << " layerNum numRows numCols resRows resCols" << endl;
  *(layerLogFiles[layerNum]) << COMMENT_CHAR << " SHIFTS: " << endl;
  *(layerLogFiles[layerNum]) << COMMENT_CHAR << " " << CMP_LOG_MSG_SHIFT << " layerNum timestamp windowBottomLeftUTMNorthingRowResMultiple windowBottomLeftUTMEastingColResMultiple 0" << endl;
  *(layerLogFiles[layerNum]) << COMMENT_CHAR << " CLEARS: " << endl;
  *(layerLogFiles[layerNum]) << COMMENT_CHAR << " " << CMP_LOG_MSG_CLEAR << " layerNum timestamp 0 0 0" << endl;
  *(layerLogFiles[layerNum]) << COMMENT_CHAR << " CHANGES: " << endl;
  *(layerLogFiles[layerNum]) << COMMENT_CHAR << " " << CMP_LOG_MSG_CHANGE << " layerNum timestamp winRow winCol newValue" << endl;
  *(layerLogFiles[layerNum]) << COMMENT_CHAR << endl;
  *(layerLogFiles[layerNum]) << CMP_LOG_MSG_HEADER << " "
			     << layerNum << " "
			     << getNumRows() << " "
			     << getNumCols() << " "
			     << getResRows() << " "
			     << getResCols() << " "
			     << endl;

  return 0;
}


int CMapPlus::startLayerLog(int layerNum) {
  layerLogStatus[layerNum] = CMP_LOGGING_ON;

  return 0;
}


int CMapPlus::pauseLayerLog(int layerNum) {
  layerLogStatus[layerNum] = CMP_LOGGING_PAUSED;

  return 0;
}


int CMapPlus::closeLayerLog(int layerNum) {
  /*
  *(layerLogFiles[layerNum]) << "";
  */

  layerLogFiles[layerNum]->close();
  delete layerLogFiles[layerNum];
  layerLogFiles[layerNum] = NULL;
  layerLogStatus[layerNum] = CMP_LOGGING_OFF;

  return 0;
}


int CMapPlus::updateVehicleLoc(double UTMNorthing, double UTMEasting) {
  int returnVal;

  returnVal = CMap::updateVehicleLoc(UTMNorthing, UTMEasting);

  //printf("Shifted to %lf, %lf\n", UTMNorthing, UTMEasting);

  return returnVal;
}


int CMapPlus::clearMap() {
  int returnVal;

  returnVal = CMap::clearMap();

  return returnVal;
}


int CMapPlus::clearLayer(int layerNum) {
  int returnVal;

  returnVal = CMap::clearLayer(layerNum);

  return returnVal;
}


int CMapPlus::saveLayerShift(int layerNum) {
  if(layerLogStatus[layerNum] == CMP_LOGGING_ON) {
    double timestamp;
    timeval time;
    gettimeofday(&time, NULL);
    timestamp = ((double) time.tv_sec) + (( (double) time.tv_usec) / 1000000.0); 
    *(layerLogFiles[layerNum]) << CMP_LOG_MSG_SHIFT << " "
			       << layerNum << " "
			       << timestamp << " "
			       << getWindowBottomLeftUTMNorthingRowResMultiple() << " "
			       << getWindowBottomLeftUTMEastingColResMultiple() << " "
			       << 0 << " "
			       << endl;

  }

  return 0;
}


int CMapPlus::saveLayerClear(int layerNum) {
  if(layerLogStatus[layerNum] == CMP_LOGGING_ON) {
    double timestamp;
    timeval time;
    gettimeofday(&time, NULL);
    timestamp = ((double) time.tv_sec) + (( (double) time.tv_usec) / 1000000.0); 
    *(layerLogFiles[layerNum]) << CMP_LOG_MSG_CLEAR << " "
			       << layerNum << " "
			       << timestamp << " "
			       << 0 << " "
			       << 0 << " "
			       << 0 << " "
			       << endl;
  }

  return 0;
}

int CMapPlus::setLayerLabel(int layerNum, const char* label) {
  layerLabels[layerNum]->clear();
  layerLabels[layerNum]->append(label);
  
  return CM_OK;
}

const char* CMapPlus::getLayerLabel(int layerNum) {
	return layerLabels[layerNum]->c_str();
}


int CMapPlus::setupLayer(int layerNum, bool useDeltas, const char* label) {
  layerLogFilename = (char**)realloc(layerLogFilename, getNumLayers()*sizeof(char*));
  layerLogFiles = (ofstream**)realloc(layerLogFiles, getNumLayers()*sizeof(ofstream*));
  layerLogStatus = (int*)realloc(layerLogStatus, getNumLayers()*sizeof(int));
  layerLabels = (string**)realloc(layerLabels, getNumLayers()*sizeof(string*));
  if(layerLogFilename==NULL ||
     layerLogFiles == NULL ||
     layerLogStatus == NULL) {
    fprintf(stderr, "%s [%d]: Couldn't allocate memory!  Quitting...\n", __FILE__, __LINE__);
    exit(1);
  }
  
  if(useDeltas) {
    layerLabels[_deltaLayerID[layerNum]] = new string;
    string deltaName;
    deltaName+= label;
    deltaName+= "-Deltas";
    setLayerLabel(_deltaLayerID[layerNum], deltaName.c_str());
  } 
  layerLabels[layerNum] = new string;
  setLayerLabel(layerNum, label);
  layerLogStatus[layerNum] = CMP_LOGGING_OFF;

  return CM_OK;
}
