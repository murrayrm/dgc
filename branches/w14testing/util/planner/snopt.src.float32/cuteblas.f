*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*
*     File  subsblas.f
*
*     sasum    isamax
*
*     These correspond to members of the BLAS package (Basic Linear
*     Algebra Subprograms, Lawson et al. (1979), ACM TOMS 5, 3).
*     If possible, they should be replaced by authentic BLAS Level 1
*     routines tuned specifically to the machine being used.
*
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*** sasum thru isamax taken
*** from netlib, Thu May 16 21:00:13 EDT 1991 ***
*** Declarations of the form dx(1) changed to dx(*)

      real function sasum(n,dx,incx)
c
c     takes the sum of the absolute values.
c     jack dongarra, linpack, 3/11/78.
c
      real dx(*),dtemp
      integer i,incx,m,mp1,n,nincx
c
      sasum = 0.0d0
      dtemp = 0.0d0
      if(n.le.0)return
      if(incx.eq.1)go to 20
c
c        code for increment not equal to 1
c
      nincx = n*incx
      do 10 i = 1,nincx,incx
        dtemp = dtemp + dabs(dx(i))
   10 continue
      sasum = dtemp
      return
c
c        code for increment equal to 1
c
c
c        clean-up loop
c
   20 m = mod(n,6)
      if( m .eq. 0 ) go to 40
      do 30 i = 1,m
        dtemp = dtemp + dabs(dx(i))
   30 continue
      if( n .lt. 6 ) go to 60
   40 mp1 = m + 1
      do 50 i = mp1,n,6
        dtemp = dtemp + dabs(dx(i)) + dabs(dx(i + 1)) + dabs(dx(i + 2))
     *  + dabs(dx(i + 3)) + dabs(dx(i + 4)) + dabs(dx(i + 5))
   50 continue
   60 sasum = dtemp

*     end of sasum
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      integer function isamax(n,dx,incx)
c
c     finds the index of element having max. absolute value.
c     Jack Dongarra, linpack, 3/11/78.
c
      real dx(*),dmax
      integer i,incx,ix,n
c
      isamax = 0
      if( n .lt. 1 ) return
      isamax = 1
      if(n.eq.1)return
      if(incx.eq.1)go to 20
c
c        code for increment not equal to 1
c
      ix = 1
      dmax = dabs(dx(1))
      ix = ix + incx
      do 10 i = 2,n
         if(dabs(dx(ix)).le.dmax) go to 5
         isamax = i
         dmax = dabs(dx(ix))
    5    ix = ix + incx
   10 continue
      return
c
c        code for increment equal to 1
c
   20 dmax = dabs(dx(1))
      do 30 i = 2,n
         if(dabs(dx(i)).le.dmax) go to 30
         isamax = i
         dmax = dabs(dx(i))
   30 continue

*     end of isamax
      end

