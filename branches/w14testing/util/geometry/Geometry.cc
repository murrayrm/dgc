#include "Geometry.hh"

extern "C" void dgemv_(char*, int*, int*, double*, double*, int*, double*, int*, double*, double*, int*);
extern "C" void dgemm_(char*, char*, int*, int*, int*, double*, double*, int*, double*, int*, double*, double*, int*);
extern "C" double ddot_(int*, double*, int*, double*, int*);
extern "C" void daxpy_(int*, double*, double*, int*, double*, int*);
extern "C" void dgesv_(int*, int* , double *, int* , int *, double *, int*, int*);
extern "C" void dgesvd_(char*, char*, int*, int*, double*, int*, double*, double*, int*, double*, int*, double*, int*, int* );
extern "C" double dscal_(int*, double*, double*, int*);
extern "C" void dgels_(char*, int*, int*, int*, double*, int*, double*, int*, double*, int*, int*);
extern "C" double dnrm2_(int*, double*, int*);

void calculateWeightedPlaneFit(double weightedPointArray[], 
			       double weightVector[],
			       int numPoints,
			       double coeffs[]) {
  char notrans = 'N';
  double alpha  =  1.0;
  double beta   =  0.0;
  int incr = 1;
  int numCols = 3;

  static double* outputMatrix = NULL;
  static int outputMatrix_size = 0;

  if(outputMatrix_size < numPoints*3) {
    if(outputMatrix_size != 0)
      delete outputMatrix;
    outputMatrix = new double[numPoints*3];
    outputMatrix_size = 3*numPoints;
  }

  pseudoInv(weightedPointArray, numCols, numPoints, outputMatrix);

  // Now compute the solution vector into coeffs:
  dgemv_(&notrans, &numCols, &numPoints, &alpha, outputMatrix, &numCols,
	 weightVector, &incr, &beta, coeffs, &incr);
  
}

//This function is optimized for matrices that have more rows than columns
void pseudoInv(double inputMatrix[], int numCols, int numRows,
	       double outputMatrix[]) {
  char trans   = 'T';
  double alpha  =  1.0;
  double beta   =  0.0;  

  // Compute the SVD of the weighted M matrix
  char svdReturnWhatsImportantIntoInput = 'O';
  char svdReturnWhatsImportantIntoBuffer = 'S';
  assert(numCols == 3);
  double svdSigma[3];
  double svdVT[3*3];
  double svdWork[256];
  int svdWork_size = 256;
  assert(numRows + 3*3 < svdWork_size);
  int    svdInfo;
  dgesvd_(&svdReturnWhatsImportantIntoInput, &svdReturnWhatsImportantIntoBuffer, &numRows, &numCols, inputMatrix,
	  &numRows, svdSigma, NULL, &numRows, svdVT, &numCols,
	  svdWork, &svdWork_size, &svdInfo);

  // Now compute the pseudoinverse of inputMatrix: inputMatrix=U S VT -> inputMatrix+ = V inv(S) UT
  // where inv(S) is ST with the diagonal elements reciprocated.
  // inv(S) :
  svdSigma[0] = 1.0/svdSigma[0];
  svdSigma[1] = 1.0/svdSigma[1];
  svdSigma[2] = 1.0/svdSigma[2];
  // V*inv(S)
  dscal_(&numCols, &svdSigma[0], svdVT  , &numCols);
  dscal_(&numCols, &svdSigma[1], svdVT+1, &numCols);
  dscal_(&numCols, &svdSigma[2], svdVT+2, &numCols);
  // V*inv(S)*UT
  dgemm_(&trans, &trans, &numCols, &numRows, &numCols, &alpha,
	 svdVT, &numCols, inputMatrix, &numRows, &beta, outputMatrix, &numCols);
}


double calculateWeightedPlaneFitAndResidual(double pointArray[], 
					    double weightVector[],
					    int numPoints, 
					    double coeffs[]) {
  /*
  char notrans = 'N';
  double alpha  =  1.0;
  double beta   =  1.0;
  int numCols = 3;


  dgemv_(&notrans, &numCols, &numPoints, &alpha, outputMatrix, &numCols,
	 weightVector, &incr, &beta, coeffs, &incr);
  */
  char trans = 'T';
  char notrans = 'N';
  int numCols = 3;
  int numColsRHS = 1;

  double workArray[400];
  int workArray_size = 400;
  int info;
  int incr = 1;
  
  //cout << "numPoints was " << numPoints << endl;
  
  dgels_(&trans, &numCols, &numPoints, &numColsRHS, pointArray, &numCols, 
	 weightVector, &numPoints, workArray, &workArray_size, &info);

  memcpy((void*)coeffs, (void*)weightVector, 3*sizeof(double));
  int numwhatever = numPoints - numCols;

  double residual = dnrm2_(&numwhatever, weightVector+3, &incr);

  //cout << "Info was " << info << endl;

  //cout << "Residual was " << residual << endl;

  return residual;
}
