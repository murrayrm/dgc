/*!**
 * askynet_monitor.cc
 * This the file which contains the functions for communicating over skynet.  
 * Also in this file is the initialization function for the vehicle struct. */




#include <stdio.h>
#include <pthread.h>
#include <string.h>


#include "adrive.h"
#include "adrive_skynet_interface_types.h"
#include "askynet_monitor.hh"
#include "actuators.h"
#include "sn_msg.hh"
#include "ActuatorState.hh"

#include <iostream>
using namespace std;

#define BUFSIZE 1024



/* A list of the actuators */
char * actuator_names[NUM_ACTUATORS] = { "BRAKE",
                                         "GAS",
					 "STEER" };

/* A list of the functions to call to execute a command for each actuator*/
actuator_command_function actuator_command_functions[NUM_ACTUATORS] = { execute_brake_command,
                                                                        execute_gas_command,
									execute_steer_command};
/* A list of the functions to call to get the status of each actuator*/
actuator_status_function actuator_status_functions[NUM_ACTUATORS] = { execute_brake_status,
                                                                      execute_gas_status,
								      execute_steer_status};
/* A list of the functions to call to initialize each actuator*/
actuator_init_function actuator_init_functions[NUM_ACTUATORS] = { execute_brake_init,
                                                                  execute_gas_init,
								  execute_steer_init };

void parse_status_request(char buffer[BUFSIZE]);
void parse_command(char buffer[BUFSIZE]);
void sn_broadcast_position(int actuator);
void* actuatorStateBroadcastThread(void *pArg);

void execute_pause_condition(vehicle_t * new_vehicle, drivecmd_t my_command);
void execute_disable_condition(vehicle_t * new_vehicle, drivecmd_t my_command);
void execute_current_command(vehicle_t * new_vehicle, drivecmd_t  &my_command, skynet & Skynet, int sendsocket, drivecom_t &my_return);


/*********************** INITIALIZE VEHICLE ***************************/
/*!** This initializes the vehicle struct.  
 * Most importantly tHe function allocates the necessary  *
 *  memory for all the vehicle srructures.
 * It also sets the default conditions.,
 */
void init_vehicle_struct(vehicle_t & new_vehicle)
{
  int i;
  // initialize the vehicle to defaults 
  my_vehicle.supervisory_delay = SUPERVISORY_SLEEP_LENGTH;
  my_vehicle.super_count = 0;
  my_vehicle.skynet_key = 0;
  my_vehicle.shutdown = false;
  my_vehicle.protective_interlocks = false;
  my_vehicle.enable_ignition = false;
  my_vehicle.automatic_timber_logging = false;
  my_vehicle.run_superCon = false;
  my_vehicle.superCon_timeout = SUPERCON_DEFAULT_TIMEOUT;


  //  1.  Initialize Mutexes

  // Initialize acutator mutexes
  for(i=0; i<NUM_ACTUATORS; i++) 
  {
    actuator_t * p_actuator = & (new_vehicle.actuator[i]);

    pthread_mutex_init(&p_actuator->mutex, NULL);
    pthread_cond_init(&p_actuator->cond_flag, NULL);
    p_actuator->start_bit = 0;
    p_actuator->status_enabled  = false;
    p_actuator->command_enabled = false;
    p_actuator->status   = OFF;
    p_actuator->status_sleep_length= STATUS_SLEEP_LENGTH;
    p_actuator->command_sleep_length= ACTUATOR_COMMAND_SLEEP_TIME;
    p_actuator->command_timeout = DEFAULT_COMMAND_TIMEOUT;
    p_actuator->command  = 0;
    p_actuator->position = 0;
    p_actuator->velocity = 0;
    p_actuator->acceleration = 0;
    p_actuator->pressure = 0;
    p_actuator->error    = FALSE;      
    p_actuator->status_loop_counter = 0;
    p_actuator->command_loop_counter = 0;
    strcpy( p_actuator->name, actuator_names[i] );
      
    p_actuator->execute_command = actuator_command_functions[i];
    p_actuator->execute_status  = actuator_status_functions[i];
    p_actuator->execute_init    = actuator_init_functions[i];

  }
  
  //Initialize the steering calibration conditional.  
  pthread_cond_init(&steer_calibrated_flag, NULL);
         

  /* Intialize the OBDII actuator struct */
  pthread_mutex_init(&new_vehicle.actuator_obdii.mutex, NULL);
  pthread_cond_init(&new_vehicle.actuator_obdii.cond_flag, NULL);
    new_vehicle.actuator_obdii.start_bit = 0;
  new_vehicle.actuator_obdii.status_enabled = false;
  new_vehicle.actuator_obdii.command_enabled = false;
  new_vehicle.actuator_obdii.status_loop_counter = 0;
  new_vehicle.actuator_obdii.command_loop_counter = 0;
  new_vehicle.actuator_obdii.status = OFF;
  new_vehicle.actuator_obdii.poll_type_counter = 0;
  new_vehicle.actuator_obdii.status_sleep_length= STATUS_SLEEP_LENGTH;
  new_vehicle.actuator_obdii.command_sleep_length= ACTUATOR_COMMAND_SLEEP_TIME;
  new_vehicle.actuator_obdii.command_timeout = DEFAULT_COMMAND_TIMEOUT;
  new_vehicle.actuator_obdii.engineRPM = 0;
  new_vehicle.actuator_obdii.VehicleWheelSpeed = 0;
  new_vehicle.actuator_obdii.EngineCoolantTemp = 0;
  new_vehicle.actuator_obdii.WheelForce = 0;
  new_vehicle.actuator_obdii.GlowPlugLampTime = 0;
  new_vehicle.actuator_obdii.ThrottlePosition = 0;
  new_vehicle.actuator_obdii.CurrentGearRatio = 0;
  new_vehicle.actuator_obdii.execute_init    = execute_obdii_init;
  new_vehicle.actuator_obdii.execute_command = execute_obdii_command;
  new_vehicle.actuator_obdii.execute_status  = execute_obdii_status;
  strcpy( new_vehicle.actuator_obdii.name, "obdii" );

  /* Intialize the ESTOP actuator struct */
  pthread_mutex_init(&new_vehicle.actuator_estop.mutex, NULL);
  pthread_cond_init(&new_vehicle.actuator_estop.cond_flag, NULL);
  new_vehicle.actuator_estop.start_bit = 0;
  new_vehicle.actuator_estop.port = -1;
  new_vehicle.actuator_estop.status_enabled = false;
  new_vehicle.actuator_estop.command_enabled = false;
  new_vehicle.actuator_estop.status_loop_counter = 0;
  new_vehicle.actuator_estop.command_loop_counter = 0;
  new_vehicle.actuator_estop.status = 0;
  new_vehicle.actuator_estop.status_sleep_length= STATUS_SLEEP_LENGTH;
  new_vehicle.actuator_estop.command_sleep_length= ACTUATOR_COMMAND_SLEEP_TIME;
  new_vehicle.actuator_estop.command_timeout = DEFAULT_COMMAND_TIMEOUT;
  new_vehicle.actuator_estop.command = RUN; //THIS IS SO THAT WITHOUT superCon adrive will work as is.  If superCon is enabled this will be set to 1.  
  new_vehicle.actuator_estop.position = EPAUSE;
  new_vehicle.actuator_estop.astop = EPAUSE;
  new_vehicle.actuator_estop.dstop = EPAUSE;
  new_vehicle.actuator_estop.error = false;
  new_vehicle.actuator_estop.execute_init    = execute_estop_init;
  new_vehicle.actuator_estop.execute_command = execute_estop_command;
  new_vehicle.actuator_estop.execute_status  = execute_estop_status;
  strcpy( new_vehicle.actuator_obdii.name, "estop" );

  /* Intialize the TRANS actuator struct */
  pthread_mutex_init(&new_vehicle.actuator_trans.mutex, NULL);
  pthread_cond_init(&new_vehicle.actuator_trans.cond_flag, NULL);
  new_vehicle.actuator_trans.start_bit = 0;
  new_vehicle.actuator_estop.port = -1;
  new_vehicle.actuator_trans.status_enabled = false;
  new_vehicle.actuator_trans.command_enabled = false;
  new_vehicle.actuator_trans.status_loop_counter = 0;
  new_vehicle.actuator_trans.command_loop_counter = 0;
  new_vehicle.actuator_trans.status = 0;
  new_vehicle.actuator_trans.status_sleep_length= STATUS_SLEEP_LENGTH;
  new_vehicle.actuator_trans.command_sleep_length= ACTUATOR_COMMAND_SLEEP_TIME;
  new_vehicle.actuator_trans.command_timeout = DEFAULT_COMMAND_TIMEOUT;
  new_vehicle.actuator_trans.command = 0;
  new_vehicle.actuator_trans.position = 0;
  new_vehicle.actuator_trans.ignition_command = 1;
  new_vehicle.actuator_trans.ignition_position = 0;
  new_vehicle.actuator_trans.led1_command = 0;
  new_vehicle.actuator_trans.led1_position = 0;
  new_vehicle.actuator_trans.led2_command = 0;
  new_vehicle.actuator_trans.led2_position = 0;
  new_vehicle.actuator_trans.led3_command = 0;
  new_vehicle.actuator_trans.led3_position = 0;
  new_vehicle.actuator_trans.led4_command = 0;
  new_vehicle.actuator_trans.led4_position = 0;
  new_vehicle.actuator_trans.command = 0;
  new_vehicle.actuator_trans.error = false;
  new_vehicle.actuator_trans.execute_init    = execute_trans_init;
  new_vehicle.actuator_trans.execute_command = execute_trans_command;
  new_vehicle.actuator_trans.execute_status  = execute_trans_status;
  strcpy( new_vehicle.actuator_obdii.name, "trans" );

}   /* ENd of initilization of vehicle struct */


/****************************** skynet_main ********************************/
/***************************************************************************/

/*! This is the thread that will monitor skynet for adrive commands and 
 * cause them to execute.  It will pull the skynet key from the environment
 * variable.  It also spawns a thread to broadcast the actuator state periodically
 * to fill the CStateClient struct. */
void* skynet_main(void *pArg)
{
  struct skynet_main_args* pSNmainArgs = (struct skynet_main_args*)pArg;
  
  struct vehicle_t* pVehicle = pSNmainArgs->pVehicle;
  int& run_skynet_thread = *pSNmainArgs->pRun_skynet_thread;
  char * default_key;
  int sn_key;

  if ((default_key = getenv("SKYNET_KEY")) != NULL) {
    sn_key = atoi(default_key);
  } else {
    // Print an error message and continue
    fprintf(stderr, "NO SKYNET_KEY SPECIFIED; assuming 0\n");
    sn_key = 0;
  }
  pVehicle->skynet_key = sn_key;
  modulename myself = SNadrive;
  modulename other_mod = SNadrive_commander;
  skynet Skynet(myself, sn_key);
  
  // Create an AtimberBox instance with a pointer
  pVehicle->pAtimberBox = new AtimberBox::AtimberBox(sn_key, &pVehicle->gui_status);



  
  // spawn the thread that broadcasts the actoator state
  pSNmainArgs->pSkynet = (void*)&Skynet;
  pthread_t actuatorStateBroadcastThreadID;
  pthread_create( &actuatorStateBroadcastThreadID, NULL, &actuatorStateBroadcastThread, pArg);
  

  
  
  //printf("My module ID: %u\n My key is %d\n", Skynet.get_id(),sn_key);
  sn_msg myinput = SNdrivecmd;
  
  
  int socket_input = Skynet.listen(myinput, other_mod);
  drivecmd_t  my_command;
  int current_estop_position;

  // Initialize the return struct to fill.  
  drivecom_t  my_return;
  sn_msg myoutput = SNdrivecom;
  int socket_output = Skynet.get_send_sock(myoutput);
    

  /* This is the main loop that the skynet thread runs in.  
   * At this point it is effectively an infinite loop. */

  while (!my_vehicle.shutdown && run_skynet_thread == true)
    {
      // Listen for an incoming sknet message.  
      Skynet.get_msg(socket_input,&my_command, sizeof(my_command), 0);

      // Read the current estop position      
      current_estop_position = (int) pVehicle->actuator_estop.position;
      
      // Chose how to respond based on the estop postion
      if (current_estop_position == 0)
	{
	  execute_disable_condition(pVehicle, my_command);
	}
      else if (current_estop_position == 1)
	{
	  execute_pause_condition(pVehicle, my_command);
	}
      else if (current_estop_position == 2)
	{
	  execute_current_command(pVehicle, my_command, Skynet, socket_output, my_return);
	}
    }  
  pthread_exit(0); 
}  // End of skynet_main





/* 
 *
 *    Helper functions for skynet thread 
 * 
 */


/************************* ESTOP DISABLE *************************************/
/* In the case that we are in disable:
 *    ignore incoming commands
 *    set the brakes full on
 *    center the steering wheel
 *    turn off the throttle
 */
void execute_disable_condition(vehicle_t *pVehicle, drivecmd_t my_command)
{
stringstream dummy_string; 
// Check whether we're supposed to get out of disable.  
  if (my_command.my_actuator == estop)
    {
      //Record a skynet message
      pVehicle->actuator_estop.command_loop_counter++;
      if (my_command.my_command_type == set_position)
	{
	  pVehicle->actuator_estop.command = (int) my_command.number_arg; 
	  dummy_string.str() = "";
	  dummy_string << "SUPERCON SET " << (int) my_command.number_arg;
	  estop_log << dummy_string.str();
	}
    }
  //Set the brake to full on.  
  pVehicle->actuator[ACTUATOR_BRAKE].command = 1; 
  pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_BRAKE].cond_flag );
  
  //Set the gas to zero
  pVehicle->actuator[ACTUATOR_GAS].command = 0;
  pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_GAS].cond_flag );

  //Set the steering angle to zero
  pVehicle->actuator[ACTUATOR_STEER].command = 0; 
  pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_STEER].cond_flag );  
}


/********************** ESTOP PAUSE *********************************************/
/* In the case we are in pause:
 *   set the brake to BRAKE_PAUSE_POSITION
 *   set the gas to zero
 *   execute the current steering command 
 */
void execute_pause_condition(vehicle_t *pVehicle, drivecmd_t my_command)
{
  stringstream dummy_string;
  //Set the brake to BRAKE_PAUSE_POSITION in adrive.h
  pVehicle->actuator[ACTUATOR_BRAKE].command = pause_speed(); 
  pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_BRAKE].cond_flag );
  
  //Set the gas to zero
  pVehicle->actuator[ACTUATOR_GAS].command = 0;
  pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_GAS].cond_flag );
  
  // Pass through steer commands only
  switch (my_command.my_actuator){
  case steer: 
    // record that we recieved a skynet message to steering.  
    pVehicle->actuator[ACTUATOR_STEER].command_loop_counter ++;

    switch (my_command.my_command_type){
    case set_position:
       pVehicle->actuator[ACTUATOR_STEER].command = my_command.number_arg; 
       pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_STEER].cond_flag );
      break;
    case set_velocity:
       pVehicle->actuator[ACTUATOR_STEER].velocity = my_command.number_arg; 
       pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_STEER].cond_flag );
      break;
    case set_acceleration:
       pVehicle->actuator[ACTUATOR_STEER].acceleration = my_command.number_arg; 
       pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_STEER].cond_flag );
      break;
    }
    break;  
    // Ignore other actuators  
  case accel:
    // record that we recieved a skynet message to steering.  
    pVehicle->actuator[ACTUATOR_BRAKE].command_loop_counter ++;
    // record that we recieved a skynet message to steering.  
    pVehicle->actuator[ACTUATOR_GAS].command_loop_counter ++;
    break;
  case estop:
    //Record a skynet message
    pVehicle->actuator_estop.command_loop_counter++;
    // Check whether we're supposed to get out of pause
    if (my_command.my_command_type == set_position)
      {
	pVehicle->actuator_estop.command = (int) my_command.number_arg; 
	dummy_string.str() = "";
	dummy_string << "SUPERCON SET " << (int) my_command.number_arg;
	estop_log << dummy_string.str();
	
      }
    break;   
  case trans:
    //Record a skynet message
    pVehicle->actuator_trans.command_loop_counter++;
      
    //Update the transmission position
    if (my_command.my_command_type == set_position)
      {
	pVehicle->actuator_trans.command = (int) my_command.number_arg; 
	pthread_cond_broadcast( & pVehicle->actuator_trans.cond_flag );
	
      }
    break;   
  case gas:
    cerr << "Gas Messages outdated do not use" << endl;
    break;
  case brake:
    cerr << "Brake Messages outdeated do not use" << endl;
    break;
  }
}

/**************************** ESTOP RUN ******************************************/
/* In the case the estop is in run:
 *   execute the incoming command
 */
void execute_current_command (vehicle_t *pVehicle, drivecmd_t &my_command, skynet & Skynet, int sendsocket, drivecom_t &my_return) 
{
  stringstream dummy_string;
  // Parse the command and execute.  
  switch (my_command.my_actuator){
    // These cases came from adrive_skynet_interface_types.h
    // This structure assumes that the actions for each actuator
    // will be different.  If they are determined to be homogeneous, 
    // this case structure can be serialized.  
    
  case steer: 
    // record that we recieved a skynet message to steering.  
    pVehicle->actuator[ACTUATOR_STEER].command_loop_counter ++;
    
    //Do something with it.
    switch (my_command.my_command_type){
    case set_position:
      pVehicle->actuator[ACTUATOR_STEER].command = my_command.number_arg; 
      pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_STEER].cond_flag );
      break;      
    case set_acceleration:
      pVehicle->actuator[ACTUATOR_STEER].acceleration = my_command.number_arg; 
      //  execute_steer_acceleration(pVehicle->actuator[ACTUATOR_STEER].acceleration);
      //      pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_STEER].cond_flag );
      break;
    case set_velocity:
      pVehicle->actuator[ACTUATOR_STEER].velocity = my_command.number_arg; 
      //execute_steer_velocity(pVehicle->actuator[ACTUATOR_STEER].velocity);
      ///pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_STEER].cond_flag );
      break;
    }
    break;

    /* This is the case that should be used during the race.  It combines the accel and brake
     * preventing commanding brake and accel to be both commanded at once.  */
  case accel:
    // record that we recieved a skynet message to brake and gas.  
    pVehicle->actuator[ACTUATOR_BRAKE].command_loop_counter ++;
    pVehicle->actuator[ACTUATOR_GAS].command_loop_counter ++;
    
    //Do something with it.
    switch (my_command.my_command_type){
    case set_position:
      //Check the gas
      if(my_command.number_arg >= 0 && my_command.number_arg <= 1.0)
	// Check that the command is in the right range for gas and execute.  
	{
	  pVehicle->actuator[ACTUATOR_GAS].command = my_command.number_arg;
	}
      else //if (my_command.number_arg > -1 && my_command.number_arg <= 0)
	// if it is outside the range set gas to zero.  
	{
	  pVehicle->actuator[ACTUATOR_GAS].command = 0.0;
	}
      pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_GAS].cond_flag );
      
      
      //The brake
      if (my_command.number_arg > 0.0 && my_command.number_arg <= 1.0)
	// If the 
	{
	  pVehicle->actuator[ACTUATOR_BRAKE].command = 0.0; 
	}
      else if (my_command.number_arg >= -1.0 && my_command.number_arg <= 0.0)
	{
	  // Negate this when passed to the brake for it takes 0 to 1.
	  // This is so that the actuator takes 0 to 1 from the commanded 
	  // general accel. 
	  pVehicle->actuator[ACTUATOR_BRAKE].command = -1.0 * my_command.number_arg; 
	}
      pthread_cond_broadcast( & pVehicle->actuator[ACTUATOR_BRAKE].cond_flag );
      break;
    case set_acceleration:
      break;
    case set_velocity:
      break;
    }
    break;
    
  case estop:
    switch (my_command.my_command_type){
    case set_position:
      pVehicle->actuator_estop.command = (int) my_command.number_arg; 
      dummy_string.str() = "";
      dummy_string << "SUPERCON SET " << (int) my_command.number_arg;
      estop_log << dummy_string.str();
      
      pthread_cond_broadcast( & pVehicle->actuator_estop.cond_flag );
      break;
    case set_velocity:
      break;
    case set_acceleration:
      break;
    }
    break;
  case trans:
    //printf("got a trans");
    
    // record that we recieved a skynet message to trans.  
    pVehicle->actuator_trans.command_loop_counter ++;
    
    //Do something with it.
    switch (my_command.my_command_type){
    case set_position:
      pVehicle->actuator_trans.command = (int) my_command.number_arg; 
      pthread_cond_broadcast( & pVehicle->actuator_trans.cond_flag );
      break;      
    case set_velocity:
      break;
    case set_acceleration:
      break;
    }
    
    break;
  case gas:
    cerr << "Gas Messages outdated do not use" << endl;
    break;
  case brake:
    cerr << "Brake Messages outdeated do not use" << endl;
    break;
  }
}



/****************************** ACTUATOR STATE BROADCAST ***********************/
/* This is the thread to broadcast the actuator state. 
 * It loops sending out the data to fill the struct in 
 * CStateClient allowing easy access to actuator state 
 * to other modules.  
 * This function has two uses.  It both broadcasts over skynet as well as
 * logging data locally according to the TimberClient */

void* actuatorStateBroadcastThread(void *pArg)
{

  /***************************** BROADCAST **************************/
  struct skynet_main_args* pSNmainArgs = (struct skynet_main_args*)pArg;
  
  
  struct vehicle_t* pVehicle = pSNmainArgs->pVehicle;
  int& run_skynet_thread = *pSNmainArgs->pRun_skynet_thread;
  skynet* pSkynet = (skynet*)pSNmainArgs->pSkynet;

  ActuatorState state;
  AdriveTimberClient my_timber_client(pVehicle->skynet_key);

  int socket = pSkynet->get_send_sock(SNactuatorstate);
  string logging_location;
  bool logging_enabled;
  unsigned long long current_time;
  string testlog;
  fstream testlogfile;


 
  while (pVehicle->shutdown == false && run_skynet_thread == true)
    {
      // Fill in all the values
      state.m_steerstatus = pVehicle->actuator[ACTUATOR_STEER].status;
      state.m_steerpos = pVehicle->actuator[ACTUATOR_STEER].position;
      state.m_steercmd = pVehicle->actuator[ACTUATOR_STEER].command;
      state.m_steer_update_time = pVehicle->actuator[ACTUATOR_STEER].update_time;      

      state.m_gasstatus = pVehicle->actuator[ACTUATOR_GAS].status;
      state.m_gaspos = pVehicle->actuator[ACTUATOR_GAS].position;
      state.m_gascmd = pVehicle->actuator[ACTUATOR_GAS].command;
      state.m_gas_update_time = pVehicle->actuator[ACTUATOR_GAS].update_time;      
            
      state.m_brakestatus = pVehicle->actuator[ACTUATOR_BRAKE].status;
      state.m_brakepos = pVehicle->actuator[ACTUATOR_BRAKE].position;
      state.m_brakecmd = pVehicle->actuator[ACTUATOR_BRAKE].command;
      state.m_brakepressure = pVehicle->actuator[ACTUATOR_BRAKE].pressure;
      state.m_brake_update_time = pVehicle->actuator[ACTUATOR_BRAKE].update_time;      

      state.m_estopstatus = pVehicle->actuator_estop.status;
      state.m_estoppos = (int) pVehicle->actuator_estop.position;
      state.m_astoppos = (int) pVehicle->actuator_estop.astop;
      state.m_cstoppos = (int) pVehicle->actuator_estop.command;
      state.m_dstoppos = (int) pVehicle->actuator_estop.dstop;
      state.m_estop_update_time = pVehicle->actuator_estop.update_time;
      state.m_about_to_unpause = pVehicle->actuator_estop.about_to_unpause;      


      state.m_transstatus = pVehicle->actuator_trans.status;
      state.m_transpos = (int) pVehicle->actuator_trans.position;
      state.m_transcmd = (int) pVehicle->actuator_trans.command;
      state.m_trans_update_time = pVehicle->actuator_trans.update_time;      
      
      state.m_obdiistatus = pVehicle->actuator_obdii.status;
      state.m_engineRPM = pVehicle->actuator_obdii.engineRPM;
      state.m_TimeSinceEngineStart = pVehicle->actuator_obdii.TimeSinceEngineStart;
      state.m_VehicleWheelSpeed = pVehicle->actuator_obdii.VehicleWheelSpeed;
      state.m_EngineCoolantTemp = pVehicle->actuator_obdii.EngineCoolantTemp; 
      state.m_WheelForce = pVehicle->actuator_obdii.WheelForce;
      state.m_GlowPlugLampTime = pVehicle->actuator_obdii.GlowPlugLampTime;
      state.m_ThrottlePosition = pVehicle->actuator_obdii.ThrottlePosition;
      state.m_CurrentGearRatio = pVehicle->actuator_obdii.CurrentGearRatio;
      state.m_obdii_update_time = pVehicle->actuator_obdii.update_time;      





      if(pSkynet->send_msg(socket, &state, sizeof(state), 0) != sizeof(state))
	{
	  cerr << "actuatorStateBroadcastThread didn't send the right number of bytes!!!!!" << endl;
	}
      
      /********************************* TIMBER LOGGING *************************************/
      /* Each time we broadcast we will also record the data incase timber logging is enabled.*/

      logging_enabled = my_timber_client.getLoggingEnabled(); // get whether or not logging is enabled from timberclient
      if (logging_enabled)
	{
	  logging_location = my_timber_client.getLogDir(); // get the name of the logging directory specified as an absolute
	  // path.  getLogDir will create the directory as needed.
	  if (my_timber_client.checkNewDirAndReset())     // checkNewDirAndReset is useful if you want to output a certain file
	    // or files everytime a new `run' or `session' is started.
	    // checkNewDirAndReset will return true the first time it is called
	    // on a newly created directory, and false after that (until the next
	    // directory is created)
	    {
	      // First check if the file is open
	      if (testlogfile.is_open())
		{
		  // If so close it
		  testlogfile.close();
		}
	      // Set the new filename
	      testlog = logging_location + "/adrive.log";
	      // Open the file to output to
	      testlogfile.open(testlog.c_str(), fstream::out|fstream::app);
	    }
	  /* The file append code */ 
	  /* The data output is the time stamp and then each element of the actuator_state struct found 
	   * in DGC/constants/ActuatorState.hh tab delimited.*/
	  
	  DGCgettime(current_time);
	  testlogfile.precision(10);
	  testlogfile <<  current_time << '\t';
	  testlogfile <<  state.m_steerstatus << '\t' << state.m_steerpos << '\t' << state.m_steercmd << '\t' << state.m_steer_update_time << '\t';
	  testlogfile <<  state.m_gasstatus << '\t' << state.m_gaspos << '\t' << state.m_gascmd << '\t' << state.m_gas_update_time << '\t';
	  testlogfile <<  state.m_brakestatus << '\t' << state.m_brakepos << '\t' << state.m_brakecmd << '\t' << state.m_brakepressure <<'\t' << state.m_brake_update_time << '\t';
	  testlogfile <<  state.m_estopstatus << '\t' << state.m_estoppos << '\t'<< state.m_dstoppos << '\t'<< state.m_astoppos << '\t'<< state.m_cstoppos << '\t' << state.m_estop_update_time << '\t';
	  testlogfile <<  state.m_transstatus << '\t' << state.m_transpos << '\t' << state.m_transcmd << '\t' << state.m_trans_update_time << '\t';
	  testlogfile <<  state.m_obdiistatus << '\t' << state.m_engineRPM << '\t' << state.m_TimeSinceEngineStart << '\t' << state.m_VehicleWheelSpeed <<'\t';
	  testlogfile <<  state.m_EngineCoolantTemp << '\t' << state.m_WheelForce << '\t' << state.m_GlowPlugLampTime << '\t' << state.m_CurrentGearRatio <<'\t' << state.m_obdii_update_time << '\t';
	  testlogfile << endl;

	  
	  
	  
	}
      // Sleep length defined in adrive.h
      usleep(ACTUATOR_STATE_BROADCAST_INTERVAL);
    }
  
  return NULL;
}



