//-------------------------------------------------
// includes
//-------------------------------------------------

#include "cv.h"
#include "highgui.h"

//#include "conversions.h"   // just for YUV2RGB

//-------------------------------------------------
// defines
//-------------------------------------------------

// single-channel images

#define FLOAT_IMXY(im, x, y)  (((float*)(im->imageData + im->widthStep*y))[x])
#define UCHAR_IMXY(im, x, y)  (((unsigned char*)(im->imageData + im->widthStep*y))[x])

// 3-channel images (8 bits per channel)

#define UCHAR_B_IMXY(im, x, y)  (((unsigned char*)(im->imageData + im->widthStep*y))[3*x])
#define UCHAR_G_IMXY(im, x, y)  (((unsigned char*)(im->imageData + im->widthStep*y))[3*x+1])
#define UCHAR_R_IMXY(im, x, y)  (((unsigned char*)(im->imageData + im->widthStep*y))[3*x+2])

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------


    
