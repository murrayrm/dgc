/**
 * DGCWidget.cc
 * Revision History:
 * 07/08/2005  hbarnor  Created
 * $Id: DGCWidget.cc 8530 2005-07-11 06:26:58Z hbarnor $
 */

#include "DGCWidget.hh"

//#warning "KILL ME "
//#include <iostream>

DGCWidget::DGCWidget(unsigned int type)
  : m_type(type)
{
  m_updateDisplay.connect(sigc::mem_fun(this, &DGCWidget::displayNewValue));
}

DGCWidget::~DGCWidget()
{
  
}

void DGCWidget::setText(Glib::ustring& newValue)
{
  m_displayValue = newValue;
  m_updateDisplay();
  //cout << "Calling super const" << endl;
}

string DGCWidget::getText()
{
  return "DGCWidget";
}

void DGCWidget::displayNewValue()
{
  cout << " In DGCWidget" << endl;
}
