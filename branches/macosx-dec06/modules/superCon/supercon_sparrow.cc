/* Sparrow Display set-up */

#include <unistd.h>
#include <string.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

#include "StrategyInterface.hh"  //contains declaration of action strings (must be global for sparrow)
#include "SuperCon.hh"  //contains the definitions of the diagnostic & state tables
#include "Diagnostic.hh" //declarations of instances of SCstate & diagnostic tables to be output (global)

#include "superconmainTable.h"
#include "diagnosticTable.h"
#include "scstateTable.h"

int SparrowUpdateCount = 0;
int user_quit(long arg);


void CStrategyInterface::Init_SparrowDisplayTable() {

  //Initialise the tables used by the Sparrow display for the SuperCon
  //module - note that the initialisation of the table occurs here so that the
  //table can be initialised before the sparrow related threads are started
  //as otherwise a race condition can occur (see comment below in the
  //SparrowDisplayLoop() method for more information)
  if (dd_open() < 0) exit(1);
  dd_usetbl( superconmainTable );
  dd_usetbl( diagnosticTable );
  dd_usetbl( scstateTable );
}


void CStrategyInterface::UpdateSparrowVariablesLoop( const bool m_tweet_action_strings_updated ) 
{
  while(true) 
  {
    
    /* NOTE - the variables for ALL of the sparrow display screens used by
     * SuperCon are updated here */

    /* ADD NEW SPARROW DISPLAY ELEMENT UPDATES BELOW THIS POINT - REMEMBER THAT
     * DETAILS OF THE VARIABLES MUST ALSO BE GIVEN IN THE APPROPRIATE .dd FILE 
     */

    /* ADD SPARROW DISPLAY UPDATES FOR SC STATE SCREEN *NOT* IN SCSTATE TABLE BELOW THIS POINT */

    
    /* ADD SPARROW DISPLAY UPDATES FOR DIAGNOSTIC SCREEN *NOT* IN DIAGNOSTIC TABLE BELOW THIS POINT */


    /* ADD SPARROW DISPLAY UPDATES FOR SUPERCONMAIN SCREEN BELOW THIS POINT */

    //ONLY refresh the action strings on the sparrow display if the strings
    //have been updated (i.e. their value has changed)
    if(m_tweet_action_strings_updated == true) {
      
      //Update the current action string displayed on the SuperCon sparrow
      //display
      dd_refresh( STATUSSTRING );
      dd_refresh( HIST1ACTSTR );
      dd_refresh( HIST2ACTSTR );
      dd_refresh( HIST3ACTSTR );
      dd_refresh( HIST4ACTSTR );
      dd_refresh( HIST5ACTSTR );
 
      //strings have been refreshed - reset the bool indicator
      tweet_action_strings_updated = false;

    }

    /* ALL SPARROW DISPLAY ELEMENT UPDATES SHOULD BE MADE ABOVE THIS POINT */
    SparrowUpdateCount ++;  //KEEP!
    //hmm... keep this as sparrow display could otherwise
    //hog CPU power - frequency may need to be adjusted though
    usleep(100000); 
  }
}


void CStrategyInterface::SparrowDisplayLoop() 
{
  dbg_all = 0;
  if (dd_open() < 0) exit(1);

  //Removed the table initialisation of vddtable here, as it allows a race condition
  //to exist between two threads spawned by SuperCon (SparrowDisplayLoop
  // & UpdateSparrowVariablesLoop), essentially there are methods in the later
  //thread (UpdateSparrowVariablesLoop) which require the table to be initialised
  //*before* they are called - however as the threads run in parallel there is no
  //check to verify that this is the case (other than that the first string is
  //started first, which is what leads to a potential race condition).  Hence
  //the initialisation of the vddtable (sparrow table) has been moved to the
  //method Init_SparrowDisplayTable, which is called *before* either of the threads
  //are started - hence the table *must* exist prior to the threads being called.
  dd_bindkey('q', user_quit);

  usleep(250000); //another innocuous sparrow sleep command
  dd_loop();
  dd_close();

}

int user_quit(long arg)
{
  return DD_EXIT_LOOP;
}
