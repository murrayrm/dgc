@c -*- texinfo -*-

@node intro,framework,,Top
@chapter Introduction

@section Overview of the @code{Falcon} library

Falcon is a collection of programs and a library of C functions
intended to aid in the design and implementation of real-time control
systems on a variety of platforms.  In general, the same routines used
during simulation are also used for real-time control.  This common
interface speeds the transition from design to implementation.

Initially, the driving force behind @code{Falcon} was the need for tools to 
support @code{Sparrow}, a real-time control environment.  As 
@code{Falcon} progressed, its value as a generic control library
became appararent.   @code{Falcon} now strives to provide functions which
are platform independent, robust, and fast enough to be used in real-time
control.

@code{Falcon} is intended for use design, development, and implementation
of controllers in a research environment.  While the code is efficient,
it is not optimized for any particular platform.

The @code{Falcon} library is divided into three sections:

@table @b
@item Matrix based functions
  Many common applications require matrix manipulations.  @code{Falcon}
  provides a matrix computation module which is used by several other 
  modules, including the state space computation module and the linear 
  parameter varying control module.

@item Table based functions
  Both trajectories and lookup tables are frequently encountered in 
  control problems.  The trajectory tracking module allows the storage of
  complex trajectories in a compact form.  The lookup table module support
  tables of arbitrary dimension.

@item Matlab/Simulink interface
  Since design and analysis is often done in Matlab, an interface between
  @code{Falcon} and Matlab is provided.  An s-function interface to each 
  module is provided.  This allows both analysis and the real-time 
  implementation to share common data files.  As soon as a controller is
  designed, it is ready to be tested on the experiment.

@end table
@noindent
Each module is described in its own chapter.
A listing of all functions defined in the @code{Falcon} library is given in a
separate reference section.

@section Hardware requirements

@code{Falcon} will run on any computer with an ANSI-C compiler.

@section Accessing the @code{Falcon} library

The Falcon source tree is currently under development.  It will eventually
look like

@example
bin             executable programs
demo            source and executables for demo programs
doc             documentation files
include         header files
lib             library and support files
src             source code for the library 
testing         testing routines
@end example

@section Acknowledgements

Falcon would not have been written without the support of Richard
Murray, who provided a lab full of experiments which needed control
software.  Bob Behnken, Michiel van Nieuwstadt, and Sudipto Sur
provided valuable input and helped debug the routines by serving 
as Guinea pigs (@code{Falcon} food).
