#ifndef RECTANGLE_HH
#define RECTANGLE_HH

#include <math.h>
#include <iostream>

#include "frames/coords.hh"
#include "CCircle.hh"

using namespace std;

class CRectangle {
public:
  NEcoord _center;
  double _width;
  double _length;
  double _angle;

  CRectangle(NEcoord centerLineStart,
	     NEcoord centerLineEnd,
	     double width);

  //This assumes that the angle is 0 (that the sides of the
  //rectangle are aligned with Northing and Easting) and that the
  //corners are in the following order: SW, NW, NE, SE
  CRectangle(NEcoord corners[]);
  ~CRectangle();

  enum INTERSECTION_TYPE {
    INTERSECTION_NO=0,
    INTERSECTION_YES=1,
    INTERSECTION_CONTAINS=2,
    INTERSECTION_CONTAINED=3
  };

  INTERSECTION_TYPE checkIntersection(const CRectangle &other);

  INTERSECTION_TYPE checkIntersection(const CCircle &other);
};

#endif //RECTANGLE_HH
