#ifndef BENCHMARK_HH
#define BENCHMARK_HH
/* This is a class designed to keep track of repeated delays.
 * It is called each time a message is sent with begin() and then 
 * end() when the reply is recieved.  This was designed to keep 
 * track of serial port messages but can be used almost anywhere.
 * The timing comes from the cpu clock. */

/* Tully Foote  2/14/2006 */

#include <sys/time.h>
#include <iostream>
#include <math.h>
#include <string.h>
#include <fstream>
using namespace std;

class Benchmark {
private:

  struct time_node  // A node for a double linked list of unsigned long longs
  {
    unsigned long long value; // The value
    time_node *next;   // The pointer to the next value
    time_node *previous; // The last pointer

    time_node(unsigned long long value, time_node *next, time_node *previous)
      : value(value),next(next),previous(previous){}  // Inline constructor
  };

  time_node *first_start_time;
  time_node *last_start_time;
  time_node *first_delay;
  time_node *last_delay;
  //  int number_of_measurements; //a loose reference to
              //how many elements are in the delays list

  bool lossless;  //This is the flag whether the struct is lossless or lossy
                  // the default is lossless.
  string description;



  void destroy(time_node *, time_node *);  // Memory cleanup for the destructor
  unsigned long long ulltime();  //return the current time in ULL form
  bool print_ll(time_node*, fstream&); // Print out the values of a linked list

  bool lossless_begin();  //The begin for lossless
  bool latest_begin();    // The begin for lossy

  bool lossless_end();  // The end for lossless
  bool latest_end();    // THe end for lossy

  bool append_delay(unsigned long long);  // Append a delay value to the delay list

public:
  Benchmark();  //Constructor
  ~Benchmark();  //Destructor


  /* Mutators */
  bool begin();  // mark the start of a new packet
  bool end();  // record the delay since the last packet
  bool set_description(string);  // Set the description of the delay
  bool set_lossless();   // Set the mode to lossless
  bool set_lossy();      // Set the mode to lossy


  /* Accessors */
  double average_delay();  //return the average delay
  double std_dev();  // return the standard deviation of the delay
  int lost_packets();  //return the number of time started not returned


  void report();

};





#endif //BENCHMARK_HH
