%Takes a traj and gives plot of velocity and acceleration over time.
%Also plots acceleration over time broken into tangential and normal.
%Also plots the traj.
function plottraj(trajfile)
  traj=load(trajfile);
  l=size(traj)(1);
  d=sqrt((traj(2:l,1)-traj(1:(l-1),1)).^2+(traj(2:l,4)-traj(1:(l-1),4)).^2);
  v=sqrt(traj(:,2).^2+traj(:,5).^2);
  at=(traj(:,2).*traj(:,3)+traj(:,5).*traj(:,6))./v;
  an=(-traj(:,5).*traj(:,3)+traj(:,2).*traj(:,6))./v;
  a=sqrt(at.^2+an.^2);
  t(1)=0;
  for i=2:l
    if (v(i-1)==0)
      t(i)=t(i-1)+0.01;
    else
      t(i)=t(i-1)+d(i-1)/v(i-1);
    endif
  endfor
  figure;
  clearplot;
  plot(t,v,";Speed;",t,at,";Tangential Acceleration;")
  xlabel('Time (s)')
  ylabel('Speed (m/s) and Acceleration (m/s^2)')
  title(trajfile)
  grid on
  figure;
  clearplot;
  plot(t,a,";Acceleration;",t,at,";Tangential Acceleration;",t,an,";Normal Acceleration;")
  xlabel('Time (s)')
  ylabel('Acceleration (m/s^2)')
  title(trajfile)
  grid on
  figure;
  clearplot;
  plot(traj(:,4),traj(:,1))
  hold on
  text(traj(1,4),traj(1,1),'start')
  text(traj(end,4),traj(end,1),'end')
  xlabel('Easting (m)')
  ylabel('Northing (m)')
  title(trajfile)
  buffer=20; % spacing around traj plot
  axis([min(traj(:,4))-buffer max(traj(:,4))+buffer min(traj(:,1))-buffer max(traj(:,1))+buffer])
  axis('equal')
  grid on
