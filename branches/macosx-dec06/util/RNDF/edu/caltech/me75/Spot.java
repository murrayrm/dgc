package edu.caltech.me75;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Used to represent and parse a parking spot.  A parking spot consists of a
 * center waypoint and an entry waypoint (to enter the spot).
 * @author David Waylonis
 */
public class Spot {

	int[] id;
	double width;
	WayPoint inPoint;
	WayPoint checkPoint;
	
	/**
	 * Default constructor.  Takes in an ID to represent the spot.
	 * 
	 * @param id the spot ID
	 */
	public Spot(int[] id) {
		
		this.id = id;
	}
	
	/**
	 * Constructor with a string to represent the identity of the spot.
	 * 
	 * @param ident the string
	 */
	public Spot(String ident) {
		
		id = WayPoint.toIntRep(ident);
	}
	
	/**
	 * Read in and parse the spot representation.  Set the two waypoint values
	 * used to represent the spot to the corresponding waypoints.
	 * 
	 * @param br the buffered reader for the RNDF file
	 * @throws IOException if we are unable to read the file
	 */
	public void readSpot(BufferedReader br) throws IOException {
		  String line = br.readLine();
		  System.out.println(line);
		  String[] tokens = line.split("\\s");
		  inPoint = null;
		  checkPoint = null;
		  while(!line.startsWith("end_spot")) {
			  
		       System.out.println(line);
		       tokens = line.split("\\s");
		       if(line.startsWith("spot_width")) {
		    	   System.out.println("width: " + tokens[1]);
		    	   width = Double.parseDouble(tokens[1]);
		       }
		       else if(tokens[0].equals("checkpoint")) {
		       }
		       else if(!tokens[0].startsWith("/")) {
		    	   try {
		    		   if (inPoint == null) {
		    			   inPoint = new WayPoint(tokens[0], Double.parseDouble(tokens[1]),
		    					   Double.parseDouble(tokens[2]));
		    		   }
		    		   else if (checkPoint == null) {
		    			   checkPoint = new WayPoint(tokens[0], Double.parseDouble(tokens[1]),
		    					   Double.parseDouble(tokens[2]));
		    		   }
		    	   }
		    	   catch(Exception e) {}
		       }
		       line = br.readLine();
		  }
		  System.out.println(line);
	}
}
