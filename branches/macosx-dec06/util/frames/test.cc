// test program for frame transforms

#include <iostream>
#include "frames/frames.hh"
#include "../../constants/VehicleState.hh"

/*#include "vehlib/VState.hh"

#include "MTA/Misc/Time/Timeval.hh"*/

int main()
{
  double sp, sr, sy;  // sensor rpy relative to vehicle body frame
  XYZcoord sensor(0, 0, 0);  // sensor location relative to vehicle body frame
  VehicleState state; // vehicle state
  frames thesensor;
  XYZcoord point(0,0,0); // point to be transformed
  XYZcoord bpoint(0, 0, 0); // body point
  NEDcoord npoint(0,0,0); // nav point
  char again = 'y'; // yes no for continue
  
  // get sensor locations/attitudes relative to vehicle body frame
  cout<<"Enter sensor pitch:";
  cin>>sp;
  cout<<"Enter sensor roll:";
  cin>>sr;
  cout<<"Enter sensor yaw:";
  cin>>sy;
  cout<<"Enter sensor x offset:";
  cin>>sensor.X;
  cout<<"Enter sensor y offset:";
  cin>>sensor.Y;
  cout<<"Enter sensor z offset:";
  cin>>sensor.Z;
      
  // initialize the frame transform for the sensor frame to body
  thesensor.initFrames(sensor, sp, sr, sy);
  
  while(again == 'y')
    {    
      // get vehicle position and attitude
      cout<<"Enter vehicle pitch:";
      cin>>state.Pitch;
      cout<<"Enter vehicle roll:";
      cin>>state.Roll;
      cout<<"Enter vehicle yaw:";
      cin>>state.Yaw;
      cout<<"Enter vehicle northing:";
      cin>>state.Northing;
      cout<<"Enter vehicle easting:";
      cin>>state.Easting;
      cout<<"Enter vehicle altitude:";
      cin>>state.Altitude;
      
      // set the state that the frame transform uses
      NEDcoord pos(state.Northing, state.Easting, state.Altitude);
      thesensor.updateState(pos, state.Pitch, state.Roll, state.Yaw);

      // get the point that's being transformed
      cout<<"Enter x coordinate of point:";
      cin>>point.X;
      cout<<"Enter y coordinate of point:";
      cin>>point.Y;
      cout<<"Enter z coordinate of point:";
      cin>>point.Z;
      
      // transform the point to body frame
      bpoint = thesensor.transformS2B(point);
      
      cout<<"Body frame coordinates of point:"<<endl;
      cout<<"X: "<<bpoint.X<<" Y: "<<bpoint.Y<<" Z: "<< bpoint.Z<<endl;

      // transform the point to nav frame
      npoint = thesensor.transformS2N(point);

      cout<<"Nav frame coordinates of point:"<<endl;
      cout<<"N: "<<npoint.N<<" E: "<<npoint.E<<" D: "<< npoint.D<<endl;
      
      /*-----------------------------------------------------------*/
      
      cout<<endl<<"Inverting..."<<endl;

      // transform the ned point to body frame
      bpoint = thesensor.transformN2B(npoint);
      
      cout<<"Body frame coordinates of point:"<<endl;
      cout<<"X: "<<bpoint.X<<" Y: "<<bpoint.Y<<" Z: "<< bpoint.Z<<endl;

      // transform the point to sensor frame
      point = thesensor.transformN2S(npoint);

      cout<<"Sensor frame coordinates of point:"<<endl;
      cout<<"X: "<<point.X<<" Y: "<<point.Y<<" Z: "<< point.Z<<endl;

      /*-----------------------------------------------------------*/

      cout<<"Again? (y/n)";
      cin>>again;
    }
      
  return 0;
}
