function first_wp = plot_corridor_file( varargin );
% 
% function [first_wp] = plot_corridor_file( waypoint_file, [offset], [wprange] )
%
% Changes: 
%   02/02/04, Lars Cremean, created
%   12/07/2004, Jason Yosinski, added support for custom line styles,
%               commented out display of waypoint number
%   01/03/05, LBC, gutted to call plot_corridor_matrix()
%
% Function:
%   This function reads a Bob format waypoint specification and 
%   calls plot_corridor_matrix to display a 
%   plot of the associated corridor.
%
% Inputs: 
%   waypoint_file: string of BOB format waypoint file.
%   offset (optional): 2-vector of [Easting Northing] offset of first waypoint.
%                      if not specified, the first waypoint in the file is used.
%   wprange (optional): 2-vector of integers indicating index range of 
%                       waypoints to plot [minwp maxwp]
% 
% Outputs:
%   first_wp: Easting/Northing array of first_wp waypoint locations.
%
% Usage example:
%   wp = plot_corridor_file('waypoints.bob')
%   The waypoints file must be in Bob format!
%

% set the defaults for the optional arguments 

if( nargin < 1 )
  error('Need an argument (waypoint_file)');
end
if( nargin >= 1 )
  waypoint_file = varargin{1};

  % new-fangled, modularized version
  data = file2corridor( waypoint_file );
end
% if there's a non-empty second argument
if( nargin >= 2 && ~isempty(varargin{2}) )
  offset = varargin{2};
else
  offset = data(1,1:2);
end
if( nargin >= 3 )
  limits = varargin{3}
  if( prod(size(limits)) ~= 2 )
    error('Third argument should be a vector of integers that specify the waypoint limits.')
  end
else
  limits = [1; length(data)];
end
if( nargin >= 4 )
error('Too many arguments!');
end

% now call the plotting function
if( nargin >= 3 )
  first_wp = plot_corridor_matrix(data,offset,limits);
else
  first_wp = plot_corridor_matrix(data,offset);
end 
