T = 20;
dt = 1/T;
L = 10;

t = 0:dt:(L-1); # 0 dt 2dt... 1 ... 2 ... L-1

v = zeros(L,1); # 0           1     2     L-1
v = [1;2;3;10;9;8;1;1;1;1];
v += 0.05;

vt = 0*t;
vt(1:T:length(t)) = v;


res = 0*t;

n = 5;
k  = 15;
fw = 6.0;
l  = 1.5;
for i=1:length(t)
	dx = t(i) - (0:(L-1));
	w = (tanh(k*(dx+l)) - tanh(k*(dx-l))) / 2;

	res(i) = ( w * v.^(1-n) ) / ( w * v.^(-n) );
end

plot(t,vt, t, res)
