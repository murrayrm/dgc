#include <fstream>
#include <iomanip>
#include "spoofCom.hh"
using namespace std;

int statflag = 0;



SpoofCom::SpoofCom(int sn_key):CSkynetContainer(MODtrackMO, sn_key, &statflag)
{
  m_numobst = 0;
  m_obst = new spoofObstStruct[MAX_NUM_OBSTACLES];
}



SpoofCom::~SpoofCom()
{
  delete[] m_obst;
}




void SpoofCom::run() {
  _QUIT = 0;
  int m_send_socket = m_skynet.get_send_sock(SNmovingObstacle);

  while(!_QUIT){

    // Loop through all obstacles on the list
    for (int k = 0; k < m_numobst; k++) {
      movingObstacle obstacle;
      obstacle.ID = k + 1;
      obstacle.status = NEW;

      // Get the obstacle's timestamp:
      unsigned long long time;
      DGCgettime(time);

      cout << __FILE__ << ":" << __LINE__ << ": time = " << time << endl;

      obstacle.timeStamp = time - 1000000; // one second ago

      // Set velocity and side vectors
      obstacle.vel = m_obst[k].vel;
      obstacle.side1 = m_obst[k].side1;
      obstacle.side2 = m_obst[k].side2;

      // Compute the position one second ago:
      double deltaT = ( double(obstacle.timeStamp) - double(m_obst[k].timestamp) ) / 1.0e6;
      obstacle.cornerPos[0] = m_obst[k].startPos + obstacle.vel * deltaT;

      // Build "tracked" history
      for(int i = 1; i<NBR_OF_SAMPLES; i++){
	obstacle.cornerPos[i] = obstacle.cornerPos[i-1] + obstacle.vel * 0.1;
      }

      int msgSize = sizeof(obstacle);
      m_skynet.send_msg(m_send_socket, &obstacle, msgSize, 0);

      cout << endl << "****************************************" << endl;
      cout << setprecision(10);
      cout << "Sending new moving obstacle: " << endl;
      cout << "N     = " << obstacle.cornerPos[9].N << endl;
      cout << "E     = " << obstacle.cornerPos[9].E << endl;
      cout << "vel.N = " << obstacle.vel.N << endl;
      cout << "vel.E = " << obstacle.vel.E << endl;
      cout << "Side vectors   : (" << obstacle.side1.N << ", " << obstacle.side1.E << ")  ;  ("
	   << obstacle.side2.N << ", " << obstacle.side2.E << ")" << endl;
      cout << "Timestamp      : " << obstacle.timeStamp << endl;
      cout << "Obstacle ID    : " << obstacle.ID << endl;
      cout << "Obstacle status: " << obstacle.status << endl;

      // Wait a while before next iteration
      clock_t goal = 150000 + clock();
      while(goal > clock()) {}
    }
  }
}



void SpoofCom::loadFile(char *fname)
{
  ifstream file(fname);
  if (!file) {
    cout << "Unable to open the scenario file: " << fname << endl;
    cout << "Exiting." << endl;
    exit(1);
  }

  // Get the first line and read number of obstacles to be added
  file >> m_numobst;

  // loop through all obstacles
  for (int i = 0; i < m_numobst; i++) {
    spoofObstStruct newobst;
    // loop through the obstacle properties
    for (int j = 0; j < 8; j++) {
      double d;
      file >> d;
      *(&m_obst[i].startPos.N + j) = d;
    }
    DGCgettime(m_obst[i].timestamp);

    cout << endl << "****************************************" << endl;
    cout << setprecision(10);
    cout << "Sending new moving obstacle: " << endl;
    cout << "N     = " << m_obst[i].startPos.N << endl;
    cout << "E     = " << m_obst[i].startPos.E << endl;
    cout << "vel.N = " << m_obst[i].vel.N << endl;
    cout << "vel.E = " << m_obst[i].vel.E << endl;
    cout << "Side vectors   : (" << m_obst[i].side1.N << ", " << m_obst[i].side1.E << ")  ;  ("
	 << m_obst[i].side2.N << ", " << m_obst[i].side2.E << ")" << endl;
    cout << "Timestamp      : " << m_obst[i].timestamp << endl;
    cout << "Obstacle ID    : " << i+1 << endl;

  }


  file.close();
}





int main(int argc, char *argv[]) {

  if (argc < 2) {
    cout << "Usage: spoofCom scenario_file" << endl;
    return 1;
  }

  int sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if( pSkynetkey == NULL )
    {
      cerr << "SKYNET_KEY environment variable isn't set" << endl;
    }
  else
    sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;
  SpoofCom spoof(sn_key);

  spoof.loadFile(argv[1]);

  spoof.run();

  return 0;
}



