#ifndef _CCELEMENTARYPATH_H_
#define _CCELEMENTARYPATH_H_

#include "frames/coords.hh"
#include <vector>
#include "CVectorMap.h"
#include "traj.h"

#define		CSPATIALPLANNER_CUBEROOT_SHARPNESS  		0.4
#define		CSPATIALPLANNER_MAX_CURVATURE 					0.2
#define		CSPATIALPLANNER_SHARPNESS 		 		      (CSPATIALPLANNER_CUBEROOT_SHARPNESS*CSPATIALPLANNER_CUBEROOT_SHARPNESS*CSPATIALPLANNER_CUBEROOT_SHARPNESS)
#define 	CCELEMENTARYPATH_SECTION_LENGTH 	0.2
#define 	CSPATIALPLANNER_MAX_SEARCH_LENGTH				30.0
#define		CSPATIALPLANNER_ALICE_WIDTH							3.0

// 0.0
#define		CSPATIALPLANNER_SEGMENT_COST  					0.0
// 10
#define		CSPATIALPLANNER_CURVATURE_COST  				0.0 
// 1.0
#define		CSPATIALPLANNER_LENGTH_COST  					  1.0
// 1.0
#define		CSPATIALPLANNER_REVERSE_LENGTH_COST			1.0
// 0.0
#define		CSPATIALPLANNER_SHARPNESS_COST  				0.0
// 4.0
#define		CSPATIALPLANNER_MAX_CURVATURE_COST			4.0

class CCLandMark; // declare this here to avoid cyclic dependency with landmark

/** Basic data storage struct for CCElementaryPath. */
struct CPoint : public NEcoord
{
	double m_theta; 
	double m_curvature; 
	CPoint() {N=E=0.0; m_theta = 0.0; m_curvature = 0.0;};
};

/** The container class for CSpatialPlanner's path elements. Includes functionality to 
- generate symmetric, continuous curvature paths consisting of clothoids and an arc of circle
- find clothoid sharpness and length for an analytic solution between two symmetric postures
- rotate and translate from the origin and theta = 0, to start from a given posture. */ 
class CCElementaryPath : public vector<CPoint>
{
	public:
	
	/*------------------------ Methods --------------------------------*/
	
	/** Calculate the cost of traversing this path. */
	double cost();
	
	/** Generate a CTraj trajectory from the root node to here. */
	void getTraj(CTraj *t, int *i);
	
	/** Find sigma and l between these two symmetric postures. */
	bool findSigmaL(CCLandMark *p1, CCLandMark *p2); 
	
	/** Generate an elementary path with the current sigma and length values using trapezoidal integration. */
	void calculate(); 
	
	/** Rotate and move the path to align with m, and check it stays in the legal region. */
	bool startFrom(CCLandMark *start, CVectorMap *map);
	
	/** Used to check whether a path is still legal after an update to the map.  */
	bool isLegal(CVectorMap *map);
	
	/** Rotate and move the final point to align with m, check it stays in the legal region and return it in f. */
	bool finalPoint(CCLandMark *start, CVectorMap *map, NEcoord *f);
	
	/** Standard constructor. */
	CCElementaryPath();
	
	/*------------------------- Variables ------------------------------*/
	
	/** Is this path a reverse maneuvre? */
	bool m_reverse; 
	
	/** Sharpness of the clothoid */
	double m_sigma; 
	
	/** Length of the path. */
	double m_length; 
};

#endif
