% *************************
% makemap.m
% Creates and draws the test map used for preliminary dplanner testing.
% Saves the map and its header file as 'testmap.pgm' and
% 'testmap.hdr' resepectively.
% *************************

% Parameters:
% resRows = 0.2000;
% resCols = 0.2000;
resRows = 0.4000;
resCols = 0.4000;
% blNRowMul = 19171625;
% blEColMul = 2212600;
blNRowMul = 9585813;
blEColMul = 1106300;
minVal = 0.01;
maxVal = 10;
noDataVal = 0.08;


% Create map
multip = 255

testmap = ones(100/resRows, 20/resCols);

testmap = [ zeros(  6/resRows, 32/resCols)                                       ; ...
            zeros(100/resRows,  6/resCols) testmap zeros(100/resRows,  6/resCols); ...
            zeros(  6/resRows, 32/resCols)                                        ];

testmap = testmap * multip;
            

% testmap = 0;
% 
% testmap(26:425,26:125) = multip*ones(400,100);
% 
% triang = tril( multip*ones(150,150) );
% testmap(376:525,126:275) = triang;
% 
% triang = triu( multip*ones(100,100) );
% testmap(426:525,26:125) = triang;
% 
% triang = triu( multip*ones(75,75) );
% testmap(526:600,126:200) = triang;
% 
% triang = fliplr(triang);
% testmap(526:600,201:275) = triang;


% Draw map
image(testmap);


% Save map - must flip it upside down!
imwrite( flipud(testmap), 'testmap.pgm' );


% Create and save header
outid = fopen('testmap.hdr','w');
fprintf(outid,'%% This is the header file for a CMap layer created by mlarsson\n');
fprintf(outid,'%% timestamp       numRows numCols resRows resCols bottomLeftNorthingRowMult bottomLeftEastingColMult MinVal MaxVal NoDataVal\n');
fprintf(outid,'1105229144.400440 %d %d %f %f %d %d %f %f %f\n', ...
    size(testmap,1), size(testmap,2), resRows, resCols, blNRowMul, blEColMul, minVal, maxVal, noDataVal);
