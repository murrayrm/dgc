function map = merge_into_map(rows,cols,mapvalue,basemap)

if (nargin == 4)
    map = basemap;
else
    map = [];
end

for i=1:length(rows)
    map(rows(i),cols(i)) = mapvalue;
end