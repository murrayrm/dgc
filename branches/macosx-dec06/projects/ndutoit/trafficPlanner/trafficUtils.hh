#ifndef TRAFFICUTILS
#define TRAFFICUTILS

#include <iostream>
#include <string>
#include "rndf.hh"
#include <vector>
#include <list>
#include <math.h>
using namespace std;
//using namespace Mapper;

enum TrafficRules
{ STAY_IN_LANE, PASSING, U_TURN, TRAFFIC_LINES, DRIVE_IN_CORRECT_DIRECTION, STAY_ON_ROAD }; 
		
//struct Map
//{
//  Map()
//  {
//    // initialize the segment goals to zeros to appease the memory profilers
//    memset(this, 0, sizeof(*this));
//  }
//  void print()
//  {
//  }
//  double x1;
//  double y1;
//  double x2;
//  double y2;
//  double x3;
//  double y3;
//  double x4;
//  double y4;
//};
//
//struct Obstacle
//{
//  Obstacle()
//  {
//    // initialize the segment goals to zeros to appease the memory profilers
//    memset(this, 0, sizeof(*this));
//  }
//  void print()
//  {
//  }
//  double x1;
//  double y1;
//  double x2;
//  double y2;
//  double x3;
//  double y3;
//  double x4;
//  double y4;
//};

struct DPlannerStatus
{
  DPlannerStatus()
  {
    //initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
  void print()
  {
  }
  int goalID;
  bool status;
};

struct Constraint
{
	Constraint()
  {
    //initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
	double A1, A2, B, xrange1, xrange2, yrange1, yrange2;
};

struct ConstraintSet
{
	vector<Constraint> rightBoundaryConstraint, leftBoundaryConstraint, stopLineConstraint;
};
	
struct point 
{
	double x ;
    double y ;
};


#endif // TRAFFICUTILS
