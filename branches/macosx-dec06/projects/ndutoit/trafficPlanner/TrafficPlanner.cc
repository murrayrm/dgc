#include "TrafficPlanner.hh"
#include "DGCutils"

using namespace std;

#define MAX_DELTA_SIZE 100000
GisCoordLatLon latlon;
GisCoordUTM utm;

CTrafficPlanner::CTrafficPlanner(int sn_key, bool bWaitForStateFill, char* RNDFFileName)
  : CSkynetContainer(MODtrafficplanner, sn_key),
    CStateClient(bWaitForStateFill),
    m_bReceivedAtLeastOneDelta(false)
{
  utm.zone = 11;
  utm.letter = 'S';

  m_GloNavMapRequestSocket = m_skynet.get_send_sock(SNlocalGloNavMapRequest);
  m_GloNavMapSocket = m_skynet.listen(SNlocalGloNavMap, MODgloNavMapLib);
  /* DON'T FORGET TO CHANGE THE NAME OF SKYNET SOCKET */
  m_fullMapRequestSocket = m_skynet.get_send_sock(SNfullmaprequest);
  requestGloNavMap();
  m_rndfRevNum = 0;

  //m_rndf->print();
  /*
    m_rndf = new RNDF();
    if (!m_rndf->loadFile(RNDFFileName))
    {
    cerr << "Error:  Unable to load RNDF file " << RNDFFileName << ", exiting program" << endl;
    exit(1);
    }
    m_rndf->assignLaneDirection();
    m_rndfRevNum = 0;
  */

  localMapSocket = m_skynet.listen(SNtrafficLocalMap, MODmapping);
  //mapTalker = CLocalMapTalker(MODtrafficplanner, sn_key, false);
  m_bReceivedAtLeastOneLocalMap = false;
  
  m_bReceivedAtLeastOneDelta = false;
  segGoalsSocket = m_skynet.listen(SNsegGoals, MODmissionplanner);
  if(segGoalsSocket < 0)
    cerr << "TrafficPlanner: skynet listen returned error" << endl;

  DGCcreateMutex(&m_GloNavMapMutex);
  DGCcreateMutex(&m_LocalMapMutex);
  DGCcreateMutex(&m_LocalMapRecvMutex);
  //DGCcreateMutex(&m_ObstacleMutex);
  DGCcreateMutex(&m_CostMapMutex);
  DGCcreateMutex(&m_DPlannerStatusMutex);
  DGCcreateMutex(&m_SegGoalsMutex);
  DGCcreateMutex(&m_deltaReceivedMutex);
  DGCcreateCondition(&m_LocalMapRecvCond);
  DGCcreateCondition(&m_deltaReceivedCond);
}

CTrafficPlanner::~CTrafficPlanner() 
{
  delete m_rndf;
  
  //for (unsigned i=0; i < m_obstacle.size(); i++)
    //delete m_obstacle[i];
  	
  delete m_dplannerStatus;

  DGCdeleteMutex(&m_GloNavMapMutex);
  DGCdeleteMutex(&m_LocalMapMutex);
  //DGCdeleteMutex(&m_ObstacleMutex);
  DGCdeleteMutex(&m_CostMapMutex);
  DGCdeleteMutex(&m_DPlannerStatusMutex);
  DGCdeleteMutex(&m_SegGoalsMutex);
  DGCdeleteMutex(&m_deltaReceivedMutex);
  DGCdeleteCondition(&m_deltaReceivedCond);
}

void CTrafficPlanner::getMapDeltasThread()
{
  int mapLayer;
  char* elevationMapDelta = new char[MAX_DELTA_SIZE];

  // constants in GlobalConstants.h
  m_elevationMap.initMap(CONFIG_FILE_DEFAULT_MAP);

  // The skynet socket for receiving map deltas (from fusionmapper)
  int mapDeltaSocket = m_skynet.listen(SNdeltaElevationMap, MODmapping);
  if(mapDeltaSocket < 0)
    cerr << "CTrafficPlanner::getMapDeltasThread(): skynet listen returned error" << endl;

  while(true)
    {
      int deltasize;
      RecvMapdelta(mapDeltaSocket, elevationMapDelta, &deltasize);
      m_elevationMap.applyDelta<double>(mapLayer, elevationMapDelta, deltasize);

      // set the condition to signal that the first delta was received
      if(!m_bReceivedAtLeastOneDelta)
	DGCSetConditionTrue(m_bReceivedAtLeastOneDelta, m_deltaReceivedCond, m_deltaReceivedMutex);
    }
}

void CTrafficPlanner::getLocalMapThread()
{
  //vector<Mapper::Segment> segments;
  bool localMapReceived;
  Mapper::Map receivedLocalMap(localMapSegments);
  //Mapper::Map *map_p;

  int size;
	
  while(true)
    {
      //localMapReceived = mapTalker.RecvLocalMap(map_p, &size);
      localMapReceived = RecvLocalMap(localMapSocket, &receivedLocalMap, &size);
      if (localMapReceived)
    	{
	  DGClockMutex(&m_LocalMapMutex);
	  //m_localMap_test = *map_p;
	  m_localMap_test = receivedLocalMap;
	  DGCunlockMutex(&m_LocalMapMutex);
	  if (!m_bReceivedAtLeastOneLocalMap)
	    DGCSetConditionTrue(m_bReceivedAtLeastOneLocalMap, m_LocalMapRecvCond, m_LocalMapRecvMutex);
    	}
      //m_localMap_test.print();
    }

}

void CTrafficPlanner::getDPlannerStatusThread()
{
  // The skynet socket for receiving dplanner status
  //  int dplannerStatusSocket = m_skynet.listen(SNdplannerStatus, MODdynamicplanner);
  //  DPlannerStatus* dplannerStatus = new DPlannerStatus();
  //  if(dplannerStatusSocket < 0)
  //    cerr << "TrafficPlanner::getDPlannerStatusThread(): skynet listen returned error" << endl;

  //  while(true)
  //  {
  //    bool dPlannerStatusReceived = RecvDPlannerStatus(dplannerStatusSocket, dplannerStatus);
  //    /* YOU NEED TO FIGURE OUT WHAT TO DO HERE */
  //    if (dPlannerStatusReceived)
  //    {
  //      DGClockMutex(&m_DPlannerStatusMutex);
  //      m_dplannerStatus = dplannerStatus;
  //      DGCunlockMutex(&m_DPlannerStatusMutex);      
  //    }
  //  }
}

void CTrafficPlanner::getSegGoalsThread()
{
  SegGoals* segGoals = new SegGoals();
  while(true)
    {
  	
      //cout << "segGoalsReceived before = " << endl;
      bool segGoalsReceived = RecvSegGoals(segGoalsSocket, segGoals);
      //cout << "segGoalsReceived after = " << segGoalsReceived << endl;
      if (segGoalsReceived)
	{
	  DGClockMutex(&m_SegGoalsMutex);
	  m_segGoals.push_back(*segGoals);
	  DGCunlockMutex(&m_SegGoalsMutex);
	}
    }
}

void CTrafficPlanner::requestGloNavMap()
{

  int deltasize;
  RNDF* receivedRndf = new RNDF();

  bool bRequestMap = true;
  m_skynet.send_msg(m_GloNavMapRequestSocket,&bRequestMap, sizeof(bool) , 0);

  cout << "Waiting for Global Navigation Map" << endl;

  bool GloNavMapReceived = RecvGloNavMap(m_GloNavMapSocket, receivedRndf, &deltasize);
  
  //GloNavMapReceived = true;
  //************************************************************
  
  if (GloNavMapReceived)
    {
      cout << "Received a new GloNav map" << endl;    
    
      DGClockMutex(&m_GloNavMapMutex);
      m_rndf = receivedRndf;
      DGCunlockMutex(&m_GloNavMapMutex);
    }
}
	

void CTrafficPlanner::TPlanningLoop(void)
{
  /***************************************************************************/
  /* CONFIRM THAT WE HAVE JOIN THE SEGGOALS GROUP */
		
  SegGoalsStatus* segGoalsStatus = new SegGoalsStatus();
  segGoalsStatus->goalID = 0;
  segGoalsStatus->status = COMPLETED;

  int segGoalsStatusSocket = m_skynet.get_send_sock(SNtplannerStatus);
  int rddfSocket = m_skynet.get_send_sock(SNrddf);

  if(segGoalsStatusSocket < 0)
    cerr << "TrafficPlanner: skynet get_send_sock returned error" << endl;

  // Notify the mission planner that I'm joinning the group
  bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
  if (!statusSent)
    cout << "Error sending notification" << endl;
  else
    cout << "Successfully notified mission planner that I'm joining the group" <<  endl;
  
  // SHOULD NOW RECEIVE MAP FROM MAPPER
  generateMapFromRNDF(m_rndf, m_localMap);
  //cout << "printing m_localMap " << endl;
//  m_localMap.print();
//
//	int dummy;
//	cin >> dummy;

  //cout << "TPlanningLoop(): Waiting for elevation map" << endl;

	// quick hack: let tplanner sleep for a while to give mplanner a chance to start up
	sleep(3);

  // don't send goals until traffic planner starts listening
  //DGCWaitForConditionTrue(m_bReceivedAtLeastOneDelta, m_deltaReceivedCond, m_deltaReceivedMutex);
	
  //cout << "Waiting for local map ...";
  //DGCWaitForConditionTrue(m_bReceivedAtLeastOneLocalMap, m_LocalMapRecvCond, m_LocalMapRecvMutex);
  //cout << "done" << endl;

  /******************************************************************************/
  // OUTER LOOP - runs continuously
  bool outerLoopFlag = true;
  while(outerLoopFlag)
    {
      //usleep(500000);
				
      // Check that I have enough goals to plan with
      SegGoals currentSegGoals, nextSegGoals, nextNextSegGoals;
      DGClockMutex(&m_SegGoalsMutex);
      
      
	  	bool stopAtEndOfSegment;
	  	PlanningHorizon planningHorizon;
      unsigned numSegGoals = m_segGoals.size();
      if (numSegGoals > 0)
    	{
	  cout << endl;
	  cout << "Using new segment goal: ";
    		
	  currentSegGoals = m_segGoals.front();
      		
	  cout << "current goalID " << currentSegGoals.goalID << endl;
	  cout << endl;
	  m_segGoals.pop_front();
      		
	  /*************************************************************************/
	  /* Decide how far I need to plan */
	  // safe m_segGoals in temp variable to extract next and next-next segment goals info
	  stopAtEndOfSegment = currentSegGoals.stopAtExit;
	  list<SegGoals> segGoals_temp = m_segGoals; 	
	  switch (currentSegGoals.segment_type)
	    {
	    case ROAD_SEGMENT:
	      if (currentSegGoals.stopAtExit)
		{
		  //cout << "Need to plan in current segment only" << endl;
		  planningHorizon = CURRENT_SEGMENT;
		  stopAtEndOfSegment = true;
		} else
		  {
		    //cout << "Need to plan to next segment" << endl;
		    segGoals_temp.pop_front();
		    nextSegGoals = segGoals_temp.front();
		    if (nextSegGoals.segment_type == INTERSECTION)
		      {
			//cout << "Need to plan to the next next segment (intersection and road segment beyond)" << endl;
			segGoals_temp.pop_front();
			nextNextSegGoals = segGoals_temp.front();
			if (nextNextSegGoals.segment_type == END_OF_MISSION)
			  planningHorizon = NEXT_SEGMENT;
			else
			  planningHorizon = NEXT_NEXT_SEGMENT;
		      } 
		    else if (nextSegGoals.segment_type == PARKING_ZONE)
		      {
			//cout << "Need to plan to the beginning of the parking zone only" << endl;
			planningHorizon = CURRENT_SEGMENT;
			stopAtEndOfSegment = true;
		      } 
		    else if (nextSegGoals.segment_type == END_OF_MISSION)
		      {
			//cout << "Plan to next segment" << endl;
			planningHorizon = NEXT_SEGMENT;
		      }
		  }
	      break;
	    case INTERSECTION:
	      //cout << "Need to plan to the next segment (road segment)" << endl;
	      planningHorizon = NEXT_SEGMENT;
	      segGoals_temp.pop_front();
	      nextSegGoals = segGoals_temp.front();
	      break;
	    case PARKING_ZONE:
	      //cout << "Need to plan in the current segment only" << endl;
	      planningHorizon = CURRENT_SEGMENT;
	      stopAtEndOfSegment = true;
	      break;
	    case UTURN:
	      //cout << "Need to plan in the current segment only" << endl;
	      planningHorizon = CURRENT_SEGMENT;
	      break;
	    case END_OF_MISSION:
	      //cout << "Need to plan in the current segment only" << endl;
	      planningHorizon = CURRENT_SEGMENT;
	      stopAtEndOfSegment = true;
	      break;	
	    default:
	      //cout << "Something is likely wrong in segment type specification, or deciding how far I need to plan" << endl;
	      planningHorizon = CURRENT_SEGMENT; //bring vehicle to safe stop so that we can replan or something
	      stopAtEndOfSegment = true;
	    }         	
    	}
      DGCunlockMutex(&m_SegGoalsMutex);
    	
      if (numSegGoals > 0) // DONT START PLANNING BEFORE I HAVE SOME GOALS
	{
	  // Deal with general case first
	  if (!((currentSegGoals.entrySegmentID==0) && (currentSegGoals.entryLaneID==0) && (currentSegGoals.entryWaypointID==0)))
	    {
	    	
	      /*************************************************************************/
	      // INNER LOOP - planning loop
	      bool stayInLoopFlag = true;
	      while(stayInLoopFlag)
		{
		  double x_A,y_A, vx_A, vy_A;
		  /************************************************************************/
		  // READ ALICE POSITION    
		  UpdateState(); // from here I get m_state, which has fields m_state.Northing and m_state.Easting
		  x_A = m_state.Easting;  // position of the rear axle
		  y_A = m_state.Northing;
		  vx_A = m_state.Vel_E_rear();
		  vy_A = m_state.Vel_N_rear();
//		  cout << "Enter Alice position: x = ";
//		  cin >> x_A;
//		  cout << "Enter Alice position: y = ";
//		  cin >> y_A;			
//		  cout << "Enter Alice velocity: vx = ";
//		  cin >> vx_A;
//		  cout << "Enter Alice velocity: vy = ";
//		  cin >> vy_A;
//		  x_A = x_A + 396411.894; 
//		  y_A = y_A + 3778141.879; 				
					
	    		
		  // ************************************************************************
		  //Segment* rndfSegment = m_rndf->getSegment(segmentID); // this is in the RNDF class
		  double laneWidth = 12;					
		  //m_rndf->print();
			
		  // ***********************************************************************
		  // Convert elevation map to cost map
		  CMapPlus m_costMap;
		  DGClockMutex(&m_CostMapMutex);
		  elevation2Cost(m_elevationMap, m_costMap);
		  DGCunlockMutex(&m_CostMapMutex);
					
		  /************************************************************************/
		  /* EXTRACT TRAFFIC RULES */
		  vector<TrafficRules> trafficRules;
		  trafficRules = extractRules(currentSegGoals.segment_type);
					
		  /************************************************************************/
		  /* PARAMETER ESTIMATION */
		  // Estimate current lane
		  // Don't use this for now, but will become very important in the future
		  //int currentLane = 1;
		  
		  // This is a hack to account for limited sensing range
		  double dLimit = 100;
		  
		  // Need to estimate distance to the end of the segment (the exit point).
		  // This will depend either on the waypoint position, or in the case of a stopline, 
		  // the location of the stop line.
		  // When far away rely on GPS only and as we get closer, rely more on lane boundary data.
		  // from GPS
		  //cout << "ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER = " << ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER << endl;
		  //cout << "DIST_SWITCH_FROM_GPS_TO_SENSORS = " << DIST_SWITCH_FROM_GPS_TO_SENSORS << endl;
		  double dist2Exit_GPS, dist2Exit, dist2Exit_shifted;
		  double CSExitX, CSExitY, CSExitWayptX, CSExitWayptY;
		  double CSExitX_shifted, CSExitY_shifted;
		  Waypoint* CSExitWaypt = m_rndf->getWaypoint(currentSegGoals.exitSegmentID, currentSegGoals.exitLaneID, currentSegGoals.exitWaypointID);
		  CSExitWayptX = CSExitWaypt->getEasting();
		  CSExitWayptY = CSExitWaypt->getNorthing();
		  //int currentStopLineID = 0;		
		  //vector<Mapper::StopLine> LMCS_entryLane_stopLines;  
		  // When we have other info (such as checkpoint, or stopline information, switch to that info
		  if (currentSegGoals.segment_type == ROAD_SEGMENT)
		  {
				// Get rough idea of how far away we are
				dist2Exit_GPS = sqrt(pow(CSExitWayptX-x_A,2) + pow(CSExitWayptY-y_A,2));

		  	if ((stopAtEndOfSegment) && (dist2Exit_GPS < DIST_SWITCH_FROM_GPS_TO_SENSORS+ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER))
		  	{
//		  		// Get lane info from local map
//			    //vector<Mapper::Location> stopLineCoord;
//			    Mapper::Segment LMCS = m_localMap.getSegment(currentSegGoals.entrySegmentID);
//			    cout << "stopline segment = " << currentSegGoals.entrySegmentID << endl;
//			    Mapper::Lane LMCS_entryLane = LMCS.getLane(currentSegGoals.entryLaneID);
//			    cout << "stopline lane =  " << currentSegGoals.entryLaneID << endl;
//			    // Uncomment this when Dom has stoplines inplemented
//			    LMCS_entryLane_stopLines = LMCS_entryLane.getStopLines();
//			    cout << "LMCS_entryLane_stopLines.size() = " << LMCS_entryLane_stopLines.size() << endl;
//			    
//			    // Need to figure out which of the stoplines on the segment we are interested in now
//			    int closestStopLineCounter = 0;
//			    for (unsigned ii=0; ii<LMCS_entryLane_stopLines.size(); ii++)
//			    {
//			    	// Get x,y position on the stop line and compare to exit waypt location
//			    	double stopLine_x = LMCS_entryLane_stopLines[ii].getXLoc();
//			    	double stopLine_y = LMCS_entryLane_stopLines[ii].getYLoc();
// 						if (sqrt(pow(stopLine_x-CSExitWayptX,2)+pow(stopLine_y-CSExitWayptY,2))<MAX_GPS_SHIFT)
// 						{
// 							currentStopLineID = ii;
// 							closestStopLineCounter++;
// 						}
//			    }
//			    
//			    if (closestStopLineCounter > 1)
// 						cerr << "THERE ARE MORE THAN ONE STOPLINE CLOSE TO US!" << endl; // WHAT DO WE DO IN THIS CASE?
//					// SHOULD USE STOP LINE INFO HERE WHEN DOM HAS THAT UP AND RUNNING
//					cout << "closestStopLineCounter = " << closestStopLineCounter << endl;
//					cout << "currentStopLineID = " << currentStopLineID << endl;
//					cout << "Switching to stopline data now" << endl;
//					CSExitX = LMCS_entryLane_stopLines[currentStopLineID].getXLoc();	// make the exit pt x = stopline x
//			    CSExitY = LMCS_entryLane_stopLines[currentStopLineID].getYLoc();
//			    cout << "Switched!" << endl;
			    // FOR NOW USE WAYPOINT INFO
		  		CSExitX = CSExitWayptX;
		  		CSExitY = CSExitWayptY;			    
		  	}
		  	else	// we are far away, or dont have any other info 
		  	{
		  		CSExitX = CSExitWayptX;
		  		CSExitY = CSExitWayptY;
		  	} 
		  }
		  else if ((currentSegGoals.segment_type == INTERSECTION) || (currentSegGoals.segment_type == PARKING_ZONE))
		  {	
		  	// NOT SURE WHAT SENSORY DATA SHOULD BE USED TO UPDATE THIS
		  	CSExitX = CSExitWayptX;
		  	CSExitY = CSExitWayptY;
		  }
		  dist2Exit = sqrt(pow(CSExitX-x_A,2) + pow(CSExitY-y_A,2));
		 	printf("Exit point = (%3.6f,%3.6f)\n",CSExitX, CSExitY);  
		  /************************************************************************/
		  /* DECISION MAKING */
					
					
		  /************************************************************************/	
		  /* GENERATE CONSTRAINTS */
		  ConstraintSet constraintSet;
		  
		  if (currentSegGoals.segment_type == ROAD_SEGMENT)
		  {
		    Mapper::LaneBoundary::Divider currentLaneLBType, currentLaneRBType;
		    vector<Mapper::Location> currentLaneLBCoord, currentLaneRBCoord;
		   	DGClockMutex(&m_LocalMapMutex);
		    Mapper::Segment LMcurrentSegment = m_localMap.getSegment(currentSegGoals.entrySegmentID);
		   	DGCunlockMutex(&m_LocalMapMutex);
		    Mapper::Lane LMCS_entryLane = LMcurrentSegment.getLane(currentSegGoals.entryLaneID);
		    vector<Mapper::Location> LMCS_entryLane_LB = LMCS_entryLane.getLB();
		    vector<Mapper::Location> LMCS_entryLane_RB = LMCS_entryLane.getRB();
		    currentLaneLBType =  LMCS_entryLane.getLBType();
		    currentLaneRBType =  LMCS_entryLane.getRBType();
						
		    //for (unsigned ii=0;ii<LMCS_entryLane_LB.size();ii++)
		    //{
				//currentLaneLBCoord.push_back(LMCS_entryLane_LB[ii]);
		    //printf("LB [%d] = [%3.3f,%3.3f]\n", ii, LMCS_entryLane_LB[ii].x, LMCS_entryLane_LB[ii].y);
		    //} 
		    //for (unsigned ii=0;ii<LMCS_entryLane_RB.size();ii++)
		    //{
				//currentLaneRBCoord.push_back(LMCS_entryLane_RB[ii]);
				//printf("RB [%d] = [%3.3f,%3.3f]\n", ii, LMCS_entryLane_RB[ii].x, LMCS_entryLane_RB[ii].y);
		    //} 
		       								
		    // Need to only use information in the road segment up to the exit point
		    // find the point on the boundary that is closest to the exit waypoint and insert a point there
		    vector<double> x_RB, y_RB, x_LB, y_LB;
		    int ind_RBend_CP, ind_LBend_CP, ind_RBend_Ins, ind_LBend_Ins;
		    int ind_RBshift_CP, ind_LBshift_CP, ind_RBshift_Ins, ind_LBshift_Ins;
		    double x_temp, y_temp, disttemp;
		    Mapper::Location tmploc;
		    for (unsigned ii=0;ii<LMCS_entryLane_LB.size();ii++)
				{
				  x_LB.push_back(LMCS_entryLane_LB[ii].x);
				 	y_LB.push_back(LMCS_entryLane_LB[ii].y);
				}
		    disttemp = closestBoundaryPt(x_LB, y_LB, CSExitX, CSExitY, ind_LBend_CP, ind_LBend_Ins, x_temp, y_temp);
		    insertPointOnBoundary(x_LB, y_LB, x_temp, y_temp, ind_LBend_Ins);
	 			for (unsigned ii=0; ii<LMCS_entryLane_RB.size(); ii++)
				{
		  		x_RB.push_back(LMCS_entryLane_RB[ii].x);
		  		y_RB.push_back(LMCS_entryLane_RB[ii].y);
				}
	      disttemp = closestBoundaryPt(x_RB, y_RB, CSExitX, CSExitY, ind_RBend_CP, ind_RBend_Ins, x_temp, y_temp);
	      insertPointOnBoundary(x_RB, y_RB, x_temp, y_temp, ind_RBend_Ins);		 
	 			
	      // calculate how far I really am from the exit point (adjusting for Alice's length
	      // Use the lane boundary orientation at the exit point
	      double theta_ave, theta_LB, theta_RB;
	      // left boundary
	      if (ind_LBend_Ins>0)
	      	theta_LB = atan2(y_LB[ind_LBend_Ins-1] - y_LB[ind_LBend_Ins],x_LB[ind_LBend_Ins-1] - x_LB[ind_LBend_Ins]);
	      else
	      {
	      	cout << "Only one point on the left boundary - cannot calculate slope with which to offset the distance to exit" << endl;
	      	// In this case use the slope from the exit waypt to alice's current position
	      	theta_LB = atan2(y_A-y_LB[ind_LBend_Ins], x_A - x_LB[ind_LBend_Ins]);
	      }
	      // right boundary
	      if (ind_RBend_Ins>0)
	      	theta_RB = atan2(y_RB[ind_RBend_Ins-1] - y_RB[ind_RBend_Ins],x_RB[ind_RBend_Ins-1] - x_RB[ind_RBend_Ins]);
	      else
	      {
	      	cout << "Only one point on the right boundary - cannot calculate slope with which to offset the distance to exit" << endl;
	      	// In this case use the slope from the exit waypt to alice's current position
	      	theta_RB = atan2(y_A-y_RB[ind_RBend_Ins], x_A - x_RB[ind_RBend_Ins]);
	      }
	      // take the average of these angles
	      theta_ave = (theta_LB+theta_RB)/2;
	      // Shift back the segment exit pt by distance ALICE_DIST_REAR_AXLE_TO_FRONT_BUMBER
				CSExitX_shifted = CSExitX + ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER*cos(theta_ave);
				CSExitY_shifted = CSExitY + ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER*sin(theta_ave);	 
				//printf("Shifted exit point = (%3.6f,%3.6f)\n",CSExitX_shifted, CSExitY_shifted);  
				dist2Exit_shifted = sqrt(pow(CSExitX_shifted-x_A,2) + pow(CSExitY_shifted-y_A,2));	 			
	 			// Insert a point on the boundary at the off-set exit point
	 			disttemp = closestBoundaryPt(x_RB, y_RB, CSExitX_shifted, CSExitY_shifted, ind_RBshift_CP, ind_RBshift_Ins, x_temp, y_temp);
	      insertPointOnBoundary(x_RB, y_RB, x_temp, y_temp, ind_RBshift_Ins);
	 			disttemp = closestBoundaryPt(x_LB, y_LB, CSExitX_shifted, CSExitY_shifted, ind_LBshift_CP, ind_LBshift_Ins, x_temp, y_temp);
	      insertPointOnBoundary(x_LB, y_LB, x_temp, y_temp, ind_LBshift_Ins);
	     	
	     	// find the index of the point on the boundary closest to the exit point 			
	      disttemp = closestBoundaryPt(x_LB, y_LB, CSExitX, CSExitY, ind_LBend_CP, ind_LBend_Ins, x_temp, y_temp);
	      disttemp = closestBoundaryPt(x_RB, y_RB, CSExitX, CSExitY, ind_RBend_CP, ind_RBend_Ins, x_temp, y_temp);
	      
	 			for (int ii = 0; ii<=ind_LBend_CP; ii++)
				{
			  	tmploc.x = x_LB[ii];
			  	tmploc.y = y_LB[ii];
			  	currentLaneLBCoord.push_back(tmploc);
				} 
	      for (int ii = 0; ii<=ind_RBend_CP; ii++)
				{
		  		tmploc.x = x_RB[ii];
		  		tmploc.y = y_RB[ii];
		  		currentLaneRBCoord.push_back(tmploc);
				}
	      
	      //for (unsigned ii=0;ii<currentLaneLBCoord.size();ii++)
				//{
				//printf("LB [%d] = [%3.3f,%3.3f]\n", ii, currentLaneLBCoord[ii].x, currentLaneLBCoord[ii].y);
	  		//} 
	      //for (unsigned ii=0;ii<currentLaneRBCoord.size();ii++)
				//{
		  	//printf("RB [%d] = [%3.3f,%3.3f]\n", ii, currentLaneRBCoord[ii].x, currentLaneRBCoord[ii].y);
		  	//} 	
					      	      
	      constraintSet = laneFollowing(currentLaneLBCoord, currentLaneRBCoord, currentLaneLBType, currentLaneRBType, trafficRules, dLimit, dist2Exit, x_A, y_A);
	      
	 		}
	  	else if (currentSegGoals.segment_type == INTERSECTION)
	    {
	      cout << "INTERSECTION" << endl;
	      //Mapper::LaneBoundary::Divider intLBType, intRBType;
	      vector<Mapper::Location> intLaneLBCoord, intLaneRBCoord;
	      Mapper::Segment LM_intEntrySegment = m_localMap.getSegment(currentSegGoals.entrySegmentID);
	      Mapper::Segment LM_intExitSegment = m_localMap.getSegment(currentSegGoals.exitSegmentID);
	      Mapper::Lane LM_intEntryLane = LM_intEntrySegment.getLane(currentSegGoals.entryLaneID);
	      Mapper::Lane LM_intExitLane = LM_intExitSegment.getLane(currentSegGoals.exitLaneID);
	      vector<Mapper::Location> LM_intEntryLane_LB = LM_intEntryLane.getLB();
	      vector<Mapper::Location> LM_intEntryLane_RB = LM_intEntryLane.getRB();
	      vector<Mapper::Location> LM_intExitLane_LB = LM_intExitLane.getLB();
	      vector<Mapper::Location> LM_intExitLane_RB = LM_intExitLane.getRB();
	      //vector<Mapper::StopLine> LMCS_entryLane_stopLines = LM_intEntryLane.getStopLines();

	      // INTERSECTION ENTRY LANE
	      // find the point on the boundary that is closest to the entry waypoint and use this as 
	      // the starting point of the LB for the virtual lane inside the intersection
	      // Later need to use stop line info to update this
	      
				// wait for dom to implement stoplines before dealing with this	      
	      //double intEntryWayptX = LMCS_entryLane_stopLines[currentStopLineID].getXLoc();	// make the exit pt x = stopline x
	      //double intEntryWayptY = LMCS_entryLane_stopLines[currentStopLineID].getYLoc();
	      
	      Waypoint* intEntryWaypt = m_rndf->getWaypoint(currentSegGoals.entrySegmentID, currentSegGoals.entryLaneID, currentSegGoals.entryWaypointID);
	      double intEntryWayptX = intEntryWaypt->getEasting();
	      double intEntryWayptY = intEntryWaypt->getNorthing();
	      
	      vector<double> x_RB, y_RB, x_LB, y_LB;
	      int ind_RB_CP, ind_LB_CP, ind_RB_Ins, ind_LB_Ins;
	      int ind_inserted;
	      double x_temp, y_temp, disttemp;
	      Mapper::Location tmploc;
	      // left boudary
	      for (unsigned ii=0;ii<LM_intEntryLane_LB.size();ii++)
				{
			  	x_LB.push_back(LM_intEntryLane_LB[ii].x);
			  	y_LB.push_back(LM_intEntryLane_LB[ii].y);
				}
		    disttemp = closestBoundaryPt(x_LB,y_LB,intEntryWayptX, intEntryWayptY, ind_LB_CP, ind_LB_Ins, x_temp, y_temp);
		    insertPointOnBoundary(x_LB, y_LB, x_temp, y_temp, ind_LB_Ins);
		    // from this point, move back along the boundary by distance ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER and insert a point there
		    distanceAlongBoundary(x_LB, y_LB, ind_LB_Ins, ind_inserted, -ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER);
		    // for now just take the previous point
		    // make this the first point on the LB
		    tmploc.x = x_LB[ind_inserted]; tmploc.y = y_LB[ind_inserted];
		    // make this the first point on the LB		    
		    //disttemp = closestBoundaryPt(x_LB,y_LB,intEntryWayptX, intEntryWayptY, ind_LB_CP, ind_LB_Ins, x_temp, y_temp);
		    //tmploc.x = x_LB[ind_LB_CP-1]; tmploc.y = y_LB[ind_LB_CP-1];
		    intLaneLBCoord.push_back(tmploc);
		    //printf("first point on intersection left boundary = (%3.6f,%3.6f)\n",tmploc.x, tmploc.y);
		    // Make the point closest to the stopline the entry point the next point on the LB 
		    tmploc.x = x_temp; tmploc.y = y_temp;
		    intLaneLBCoord.push_back(tmploc);
		    //printf("second point on intersection left boundary = (%3.6f,%3.6f)\n",x_temp, y_temp);
		      
		    // Do the same for the RB
		    for (unsigned ii=0; ii<LM_intEntryLane_RB.size(); ii++)
				{
			  	x_RB.push_back(LM_intEntryLane_RB[ii].x);
			  	y_RB.push_back(LM_intEntryLane_RB[ii].y);
				}
		    disttemp = closestBoundaryPt(x_RB,y_RB,intEntryWayptX, intEntryWayptY, ind_RB_CP, ind_RB_Ins, x_temp, y_temp);
		    insertPointOnBoundary(x_RB, y_RB, x_temp, y_temp, ind_RB_Ins);
		    // from this point, move back along the boundary by distance ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER and insert a point there
		    distanceAlongBoundary(x_RB, y_RB, ind_RB_Ins, ind_inserted, -ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER);
		    // make this the first point on the RB
		    tmploc.x = x_RB[ind_inserted]; tmploc.y = y_RB[ind_inserted];
		    //disttemp = closestBoundaryPt(x_RB,y_RB,intEntryWayptX, intEntryWayptY, ind_RB_CP, ind_RB_Ins, x_temp, y_temp);
		    //tmploc.x = x_RB[ind_RB_CP-1]; tmploc.y = y_RB[ind_RB_CP-1];
		    intLaneRBCoord.push_back(tmploc);
		    //printf("first point on intersection right boundary = (%3.6f,%3.6f)\n",tmploc.x, tmploc.y);
		    // Make the point closest to the stopline the entry point the next point on the LB 
		    tmploc.x = x_temp; tmploc.y = y_temp;
		    intLaneRBCoord.push_back(tmploc);
		    //printf("second point on intersection right boundary = (%3.6f,%3.6f)\n",x_temp, y_temp);

		    // clear vectors
		    x_LB.clear(); y_LB.clear(); x_RB.clear(); y_RB.clear();
	
	      // INTERSECTION EXIT LANE
	      // find the point on the boundary that is closest to the entry waypoint and use this as 
		    // the starting point of the LB for the virtual lane inside the intersection
		    for (unsigned ii=0;ii<LM_intExitLane_LB.size();ii++)
				{
			  	x_LB.push_back(LM_intExitLane_LB[ii].x);
			  	y_LB.push_back(LM_intExitLane_LB[ii].y);
				}
		      
		    // Do the same for the RB
		    for (unsigned ii=0; ii<LM_intExitLane_RB.size(); ii++)
				{
			  	x_RB.push_back(LM_intExitLane_RB[ii].x);
			  	y_RB.push_back(LM_intExitLane_RB[ii].y);
				}
				
		    disttemp = closestBoundaryPt(x_LB,y_LB,CSExitX, CSExitY, ind_LB_CP, ind_LB_Ins, x_temp, y_temp);
		    tmploc.x = x_temp; tmploc.y = y_temp;
		    intLaneLBCoord.push_back(tmploc);
		    //insert these points on the boudary
		    insertPointOnBoundary(x_LB, y_LB, x_temp, y_temp, ind_LB_Ins);
		    // from this point, move along the boundary by distance ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER and insert a point there
		    distanceAlongBoundary(x_LB, y_LB, ind_LB_Ins, ind_inserted, ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER);
		    // make this the last point on the LB
		    tmploc.x = x_LB[ind_inserted]; tmploc.y = y_LB[ind_inserted];
		    intLaneLBCoord.push_back(tmploc);
		    
		    disttemp = closestBoundaryPt(x_RB,y_RB,CSExitX, CSExitY, ind_RB_CP, ind_RB_Ins, x_temp, y_temp);
		    tmploc.x = x_temp; tmploc.y = y_temp;
		    intLaneRBCoord.push_back(tmploc);
		    //insert these points on the boudary
		    insertPointOnBoundary(x_RB, y_RB, x_temp, y_temp, ind_RB_Ins);
		    // from this point, move along the boundary by distance ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER and insert a point there
		    distanceAlongBoundary(x_RB, y_RB, ind_RB_Ins, ind_inserted, ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER);
		    // make this the last point on the RB
		    tmploc.x = x_RB[ind_inserted]; tmploc.y = y_RB[ind_inserted];
		    intLaneRBCoord.push_back(tmploc);
			    
		    // Next I want to shift the exit point back by some distance to account for Alice's length
	     	// in the case of an intersection, I think we want to clear the intersection before switching to
	     	// roadsegment mode. 
				CSExitX_shifted = CSExitX;
				CSExitY_shifted = CSExitY;	 

				dist2Exit_shifted = sqrt(pow(CSExitX_shifted-x_A,2) + pow(CSExitY_shifted-y_A,2));

				// print the boundary points
//		    for (unsigned ii=0;ii<intLaneLBCoord.size();ii++)
//				{
//			  printf("LB [%d] = [%3.3f,%3.3f]\n", ii, intLaneLBCoord[ii].x, intLaneLBCoord[ii].y);
//			  } 
//		      for (unsigned ii=0;ii<intLaneRBCoord.size();ii++)
//				{
//			  printf("RB [%d] = [%3.3f,%3.3f]\n", ii, intLaneRBCoord[ii].x, intLaneRBCoord[ii].y);
//			  } 	
		      
		    constraintSet = intersectionNav(intLaneLBCoord, intLaneRBCoord, trafficRules, dLimit, x_A, y_A);
		    
		 }
		  
		  //cout << "rightBoundaryConstraint.size() = " << constraintSet.rightBoundaryConstraint.size() << endl;
		  //cout << "leftBoundaryConstraint.size() = " << constraintSet.leftBoundaryConstraint.size() << endl;
		  // print the boundary points
//		  for (unsigned ii=0;ii<constraintSet.leftBoundaryConstraint.size();ii++)
//		    {  
//		      printf("LBconstraints [%d] = [%3.3f,%3.3f] and [%3.6f, %3.6f]\n", ii, constraintSet.leftBoundaryConstraint[ii].xrange1, constraintSet.leftBoundaryConstraint[ii].yrange1, constraintSet.leftBoundaryConstraint[ii].xrange2, constraintSet.leftBoundaryConstraint[ii].yrange2);
//		    } 
//		  for (unsigned ii=0;ii<constraintSet.rightBoundaryConstraint.size();ii++)
//		    {
//		      printf("RBconstraints [%d] = [%3.3f,%3.3f] and [%3.6f, %3.6f]\n", ii, constraintSet.rightBoundaryConstraint[ii].xrange1, constraintSet.rightBoundaryConstraint[ii].yrange1, constraintSet.rightBoundaryConstraint[ii].xrange2, constraintSet.rightBoundaryConstraint[ii].yrange2);
//		    }	
		    
		  /************************************************************************/    
		  /* MERGE CONSTRAINTS */
					
					
		  /************************************************************************/    
		  /* POST-PROCESS CONSTRAINTS 											*/
		  //RDDF* newRddf = new RDDF("rddf.dat",true);
		  vector<double> rddfx, rddfy, rddfr;	// a clean slate to send to convRDDF
		  		  
		  RDDF* newRddf = new RDDF(NULL, true);		    		
		    		
		  bool rddfError;
		  rddfError = convRDDF(constraintSet.rightBoundaryConstraint, constraintSet.leftBoundaryConstraint, currentSegGoals, x_A, y_A, laneWidth, rddfx, rddfy, rddfr);
			//cout << "rddfx.size() = " << rddfx.size() << endl;
			
	  	//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  		//% convert into format to be sent to dplanner
  		//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  		//cout << "converting to rddf object" << endl;
  		RDDFData rddfData;
  		double previousNorthing, previousEasting;
  		int rddfNum=1;
      
      // Specify a velocity profile for 
  		//double rddfMaxSpeed = currentSegGoals.maxSpeedLimit;
  		double rddfMaxSpeed = 5;
  		double rddfSpeed, dist2Exit_RDDF, dist2Exit_shifted_RDDF;
  		double posStartToBrake = pow(rddfMaxSpeed,2)/(2*ALICE_DESIRED_DECELERATION); // The position where we want to start slowing down

			//cout << "posStartToBrake = " << posStartToBrake << endl;
			if(stopAtEndOfSegment)
				cout << "need to stop at the end of the segment" << endl;
			//cout << "dist2Exit = " << dist2Exit << endl;
			//cout << "dist2Exit_shifted = " << dist2Exit_shifted << endl;
		  for (int ii = 0; ii < (int)rddfx.size(); ii++)
    	{
	    	
      	if (ii==0)
				{
			  	// convert to rddf format
			  	utm.n = rddfy[ii];
			  	utm.e = rddfx[ii];
			  	gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);

					// Calculate speed using a ramp-down speed profile
					// This speed is dependent on our distance from the exit, whether
					// we want to stop, and Alice's dynamics
					dist2Exit_RDDF = sqrt(pow(CSExitX-rddfx[ii],2) + pow(CSExitY-rddfy[ii],2));
					//cout << "dist2Exit_RDDF = " << dist2Exit_RDDF << endl;
					dist2Exit_shifted_RDDF = sqrt(pow(CSExitX_shifted-rddfx[ii],2) + pow(CSExitY_shifted-rddfy[ii],2));
					//cout << "dist2Exit_shifted_RDDF = " << dist2Exit_shifted_RDDF << endl;
					
		  		if ((stopAtEndOfSegment) && (dist2Exit_shifted_RDDF<posStartToBrake) && (dist2Exit_RDDF>ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER))
		    		rddfSpeed = sqrt(2*ALICE_DESIRED_DECELERATION*dist2Exit_shifted_RDDF); //follow velocity profile
		    		
		    	else if ((stopAtEndOfSegment) && (dist2Exit_RDDF<ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER))
		    		rddfSpeed = 0; // we are techinically inside the intersection
		    		
		    	else
		    		rddfSpeed = rddfMaxSpeed; // we are away from the intersection
		    	  	
		    	//cout << "rddfSpeed = " << rddfSpeed << endl;
			
			  	rddfData.number = rddfNum;
			  	rddfData.Northing = rddfy[ii];
			  	rddfData.Easting = rddfx[ii];
			  	rddfData.maxSpeed = rddfSpeed;
			  	rddfData.offset = rddfr[ii];
			  	rddfData.radius = rddfr[ii];
			  	rddfData.latitude = latlon.latitude;
			  	rddfData.longitude = latlon.longitude;
		
					if (rddfx.size()==1)
						cerr << "ERROR convRDDF: only one data point in RDDFVector." << endl;
		
			  	newRddf->addDataPoint(rddfData);
			  	previousNorthing = utm.n;
			  	previousEasting = utm.e;
			  	rddfNum++;
				} 
		    else if (sqrt(pow(rddfy[ii]-previousNorthing,2)+pow(rddfx[ii]-previousEasting,2))>EPS)
				{
			  	// convert to rddf format
			  	utm.n = rddfy[ii];
			  	utm.e = rddfx[ii];
			  	gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
			  	
					// Calculate speed using a ramp-down speed profile
					// This speed is dependent on our distance from the exit, whether
					// we want to stop, and Alice's dynamics
					dist2Exit_RDDF = sqrt(pow(CSExitX-rddfx[ii],2) + pow(CSExitY-rddfy[ii],2));
					dist2Exit_shifted_RDDF = sqrt(pow(CSExitX_shifted-rddfx[ii],2) + pow(CSExitY_shifted-rddfy[ii],2));
		  		if ((stopAtEndOfSegment) && (dist2Exit_shifted_RDDF<posStartToBrake) && (dist2Exit_RDDF>ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER))
		    		rddfSpeed = sqrt(2*ALICE_DESIRED_DECELERATION*dist2Exit_shifted_RDDF); //follow velocity profile
		    	else if ((stopAtEndOfSegment) && (dist2Exit_RDDF<ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER))  
		    		rddfSpeed = 0; // we are techinically inside the intersection
		    	else
		    		rddfSpeed = rddfMaxSpeed; // we are away from the intersection
		    	  	
		    	//cout << "rddfSpeed = " << rddfSpeed << endl;
			
			  	rddfData.number = rddfNum;
			  	rddfData.Northing = rddfy[ii];
			  	rddfData.Easting = rddfx[ii];
			  	rddfData.maxSpeed = rddfSpeed;
			  	rddfData.offset = rddfr[ii];
			  	rddfData.radius = rddfr[ii];
			  	rddfData.latitude = latlon.latitude;
			  	rddfData.longitude = latlon.longitude;
		
			 	 	// Shift the Northing and Easting to those of the rear axle
					//	  double angle = atan2(rddf[ii][1]-previousNorthing, rddf[ii][0]-previousEasting);
					//	  double distBetweenDataPoints = sqrt(pow(rddf[ii][1]-previousNorthing,2) + pow(rddf[ii][0]-previousEasting,2));
					//	  rddfData.Northing = previousNorthing + (distBetweenDataPoints - VEHICLE_WHEELBASE)*sin(angle);
					//	  rddfData.Easting = previousEasting + (distBetweenDataPoints - VEHICLE_WHEELBASE)*cos(angle);
		
					//	  double dist2Exit = sqrt(pow(CSExitWayptX-rddfData.Easting,2) + pow(CSExitWayptY-rddfData.Northing,2));
			
					//	  if (currentSegGoals.stopAtExit && dist2Exit < posBrake)
					//	    rddfMaxSpeed = sqrt(2*ALICE_MAX_DECELERATION*dist2Exit);
		
				  newRddf->addDataPoint(rddfData);
			  	previousNorthing = utm.n;
			  	previousEasting = utm.e;
			  	rddfNum++;
				}
			} 
	
				    
		  if (rddfError)
		  {
		  	cout << "Error with rddf generation" << endl;
		      // NEED TO SEND mplanner FAILURE MESSAGE
		  }
		
		
		  /************************************************************************/    
		  /* SEND DATA TO DPLANNER */
	
		  SendRDDF(rddfSocket, newRddf);
		  // Print to screen
		  newRddf->print();
		  // Print to file
		  FILE * pFile;
		  pFile = fopen ("rddfFile.txt","w");
		  int numOfWayPts = newRddf->getNumTargetPoints(); 
		  RDDFVector rddfPoints = newRddf->getTargetPoints();
		  for (int ii=0; ii<numOfWayPts; ii++)
		    {
		      fprintf (pFile, "%i\t%f\t%3.9f\t%3.9f\t%3.9f\t%3.9f\t%f\t%f\t%f\t\n", 
			       rddfPoints[ii].number, rddfPoints[ii].distFromStart, 
			       rddfPoints[ii].Northing, rddfPoints[ii].Easting, 
			       rddfPoints[ii].longitude, rddfPoints[ii].latitude, 
			       rddfPoints[ii].maxSpeed, rddfPoints[ii].offset, 
			       rddfPoints[ii].radius);
		    }
		  fprintf(pFile, "\n");
		  fclose (pFile);
	    	
		  /************************************************************************/    
		  /* DECIDE WHEN SEGMENT COMPLETED */
		  // FOR NOW COMPARE POSITION WITH EXIT_WAYPT AND VELOCITY SHOULD BE SMALL AS WELL
		  // NEED TO WORK ON THIS SOME MORE
		  //if ((fabs(dist2ExitTest)<1) && ((sqrt(pow(vx_A,2)+pow(vy_A,2)))<1))
		  if (fabs(dist2Exit_shifted)<SEG_COMPLETE_DIST_TO_EXIT || 
		      pow(vx_A,2) + pow(vy_A,2) < STOP_SPEED_SQR && fabs(dist2Exit_shifted)<SEG_ALMOST_COMPLETE_DIST_TO_EXIT)
		    {
		      segGoalsStatus->status = COMPLETED;
		      stayInLoopFlag = false;
		      cout << "Completed!" << endl;
		  	  // Sleep more if we have a stop sign.
		      if (currentSegGoals.stopAtExit)
			  	sleep(5);
		    } //else
		  else
		    cout << "distance to exit = " << fabs(dist2Exit_shifted) << endl;
						
		  usleep(100000);
					
		} //WHILE STAY IN LOOP
					
	      /************************************************************************/    
	      /* SEND STATUS TO MPLANNER */
	      cout << "Sending status to mplanner...";
	      segGoalsStatus->goalID = currentSegGoals.goalID;
	      cout << "goal ID = " << segGoalsStatus->goalID << endl;
	      bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
	      if (!statusSent)
		cout << "Error sending status" << endl;
	      else
		cout << "Successfully sent status for goal " << segGoalsStatus->goalID << endl;
					
	    } else // THIS IS AT THE BEGINNING OR END OF A MISSION
	      {
		if (!(currentSegGoals.segment_type==END_OF_MISSION))
		  {
		    double x_A,y_A, vx_A, vy_A;
		    cout << "Segment 0!!" << endl;
		    bool stayInLoopFlag = true;
		    while(stayInLoopFlag)
		      {
			/************************************************************************/
			// READ ALICE POSITION    
			UpdateState(); // from here I get m_state, which has fields m_state.Northing and m_state.Easting
			x_A = m_state.Easting_front();
			y_A = m_state.Northing_front();
			vx_A = m_state.Vel_E_rear();
			vy_A = m_state.Vel_N_rear();
//			cout << "Enter Alice position: x = ";
//			cin >> x_A;
//			cout << "Enter Alice position: y = ";
//			cin >> y_A;			
//			cout << "Enter Alice velocity: vx = ";
//			cin >> vx_A;
//			cout << "Enter Alice velocity: vy = ";
//			cin >> vy_A;
//			x_A = x_A + 396411.894;
//			y_A = y_A + 3778141.879;	
								
			// Find the exit pt, or rather the entry point into the next segment
			Waypoint* CSEntryWaypt = m_rndf->getWaypoint(currentSegGoals.exitSegmentID, currentSegGoals.exitLaneID, currentSegGoals.exitWaypointID);
			double CSEntryX = CSEntryWaypt->getEasting();
			double CSEntryY = CSEntryWaypt->getNorthing();
			double CSEntryX_shifted, CSEntryY_shifted;
			double dist2Segment1 = sqrt(pow(x_A-CSEntryX,2)+pow(y_A-CSEntryY,2));
			
      double theta_ave = atan2(y_A-CSEntryY, x_A - CSEntryX);
      // Shift back the segment exit pt by distance ALICE_DIST_REAR_AXLE_TO_FRONT_BUMBER
			CSEntryX_shifted = CSEntryX + ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER*cos(theta_ave);
			CSEntryY_shifted = CSEntryY + ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER*sin(theta_ave);	 
			//printf("Shifted exit point = (%3.6f,%3.6f)\n",CSExitX_shifted, CSExitY_shifted);  
			double dist2Segment1_shifted = sqrt(pow(x_A-CSEntryX_shifted,2)+pow(y_A-CSEntryY_shifted,2));
			
			stopAtEndOfSegment = true;
			double rddfSpeed; 
			double rddfMaxSpeed = 8;
			double posStartToBrake = pow(rddfMaxSpeed,2)/(2*ALICE_DESIRED_DECELERATION); // The position where we want to start slowing down			
			
		  if ((stopAtEndOfSegment) && (dist2Segment1_shifted<posStartToBrake) && (dist2Segment1>ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER))
		    rddfSpeed = sqrt(2*ALICE_DESIRED_DECELERATION*dist2Segment1_shifted); //follow velocity profile
		  else if ((stopAtEndOfSegment) && (dist2Segment1<ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER))  
		 		rddfSpeed = 0; // we are techinically inside the intersection
		  else
	  		rddfSpeed = rddfMaxSpeed; // we are away from the intersection
			
			// Generate an rddf consisting of just these two points
			RDDF* newRddf = new RDDF(NULL, true);
			RDDFData rddfData;
						
			// Point 1: at Alice's current location
			utm.n = y_A;
			utm.e = x_A;
			gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
			rddfData.number = 1;
			rddfData.Northing = y_A;
			rddfData.Easting = x_A;
			rddfData.maxSpeed = rddfSpeed;
			rddfData.offset = dist2Segment1+LANEWIDTH;
			rddfData.radius = dist2Segment1+LANEWIDTH;
			rddfData.latitude = latlon.latitude;
			rddfData.longitude = latlon.longitude;
			newRddf->addDataPoint(rddfData);  		
		
			// Point 2: at entry point to Segment 1
			utm.n = CSEntryY;
			utm.e = CSEntryX;
			gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);\
			rddfData.number = 2;
			rddfData.Northing = CSEntryY;
			rddfData.Easting = CSEntryX;
			rddfData.maxSpeed = 0;
			rddfData.offset = LANEWIDTH;
			rddfData.radius = LANEWIDTH;
			rddfData.latitude = latlon.latitude;
			rddfData.longitude = latlon.longitude;
			newRddf->addDataPoint(rddfData); 
						    		
			SendRDDF(rddfSocket, newRddf);
			// Print to screen
			newRddf->print();
			// Print to file
			FILE * pFile;
			pFile = fopen ("rddfFile.txt","w");
			RDDFVector rddfPoints = newRddf->getTargetPoints();
			for (int ii=0; ii<2; ii++)
			  {
			    fprintf (pFile, "%i\t%f\t%3.9f\t%3.9f\t%3.9f\t%3.9f\t%f\t%f\t%f\t\n", 
				     rddfPoints[ii].number, rddfPoints[ii].distFromStart, 
				     rddfPoints[ii].Northing, rddfPoints[ii].Easting, 
				     rddfPoints[ii].longitude, rddfPoints[ii].latitude, 
				     rddfPoints[ii].maxSpeed, rddfPoints[ii].offset, 
				     rddfPoints[ii].radius);
			  }
			fprintf(pFile, "\n");
			fclose (pFile);
					   	
			// Check to see if we have completed this segment:
			cout << "dist2Segment1 = " << dist2Segment1 << endl;
			if (fabs(dist2Segment1)<2)
			  {
			    segGoalsStatus->status = COMPLETED;
			    stayInLoopFlag = false;
			    cout << "YES!" << endl;
			    /* SEND STATUS TO MPLANNER */
			    cout << "Sending status to mplanner...";
			    segGoalsStatus->goalID = currentSegGoals.goalID;
			    cout << "goal ID = " << segGoalsStatus->goalID << endl;
			    bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
			    if (!statusSent)
			      cout << "Error sending status" << endl;
			    else
			      cout << "Successfully sent status for goal " << segGoalsStatus->goalID << endl;
			  }
			else
			  cout << "not yet" << endl;
			usleep(100000);
		      } // WHILE STAY IN INNER LOOP (for segment 0)
		  } else // THIS IS THE END OF THE MISSION
		    {
		      cout << "END OF THE MISSION" << endl;
		      /* SEND STATUS TO MPLANNER */
		      cout << "letting mplanner.know..";
		      segGoalsStatus->goalID = currentSegGoals.goalID;
		      cout << "goal ID = " << segGoalsStatus->goalID << endl;
		      bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
		      if (!statusSent)
			cout << "Error sending status" << endl;
		      else
			cout << "Successfully signalled mplanner of end of mission " << segGoalsStatus->goalID << endl;
		      cout << "Exiting traffic planner" << endl;
		      outerLoopFlag = false;
		    }	
	      }  // END: if segment 0
			
	} // END: if (numOfSegGoals>0)		    	
      else 
    	{
	  cout << "No segment goals yet ...  still waiting." << endl;
	  outerLoopFlag = true;
    	}
      usleep(100000);
			
    }//WHILE STAY IN OUTER LOOP
}



void CTrafficPlanner::elevation2Cost(CMapPlus elevationMap, CMapPlus& costMap)
{
  costMap = elevationMap;
}

ConstraintSet CTrafficPlanner::laneFollowing(vector<Mapper::Location> laneLBCoord, vector<Mapper::Location> laneRBCoord, Mapper::LaneBoundary::Divider laneLBType, Mapper::LaneBoundary::Divider laneRBType, vector<TrafficRules> trafficRules, double dLimit, double dist2Exit, double x_A, double y_A) 
{
  ConstraintSet constraintSet;
  vector<double> x_RB;
  vector<double> y_RB;
  vector<double> x_LB;
  vector<double> y_LB;
  for (unsigned ii = 0; ii < laneLBCoord.size(); ii++)
    {
      x_LB.push_back(laneLBCoord[ii].x);
      y_LB.push_back(laneLBCoord[ii].y);
    }
  for (unsigned ii = 0; ii < laneRBCoord.size(); ii++)
    {
      x_RB.push_back(laneRBCoord[ii].x);
      y_RB.push_back(laneRBCoord[ii].y);
    }
  //cout << "reading in data..." << endl;
  //for (unsigned ii=0;ii<x_LB.size();ii++)
  //{
  //printf("LB (x,y) %d = (%3.6f,%3.6f)\n", ii, x_LB[ii], y_LB[ii]);
  //}
  // for (unsigned ii=0;ii<x_RB.size();ii++)
  //{
  //printf("RB (x,y) %d = (%3.6f,%3.6f)\n", ii, x_RB[ii], y_RB[ii]);
  //}

  // find points on boundary closest to me
  int ind_RB_CP, ind_LB_CP, ind_RB_Ins, ind_LB_Ins, index;
  double x_temp, y_temp;
  double distance = closestBoundaryPt(x_RB,y_RB,x_A,y_A, ind_RB_CP, ind_RB_Ins, x_temp, y_temp);
  // insert these points into boundary
  insertPointOnBoundary(x_RB, y_RB, x_temp, y_temp, ind_RB_Ins);
  // same for LB
  distance = closestBoundaryPt(x_LB,y_LB,x_A, y_A, ind_LB_CP, ind_LB_Ins, x_temp, y_temp);        
  insertPointOnBoundary(x_LB, y_LB, x_temp, y_temp, ind_LB_Ins);
  //cout << "after inserting closest boundary pt..." << endl;
  //for (unsigned ii=0;ii<x_LB.size();ii++)
  // {
  //printf("LB (x,y) %d = (%3.6f,%3.6f)\n", ii, x_LB[ii], y_LB[ii]);
  //}
  //for (unsigned ii=0;ii<x_RB.size();ii++)
  //{
  //printf("RB (x,y) %d = (%3.6f,%3.6f)\n", ii, x_RB[ii], y_RB[ii]);
  //}

  // insert a point a distance away from the point if we are far
  // enough away from the end of the segment.
  if (dLimit < dist2Exit)
    {
      distanceAlongBoundary(x_RB, y_RB, ind_RB_Ins, index, dLimit);
      distanceAlongBoundary(x_LB, y_LB, ind_LB_Ins, index, dLimit);    	
    }

  //cout << "after inserting a point at distance dLimit..." << endl;
  //for (unsigned ii=0;ii<x_LB.size();ii++)
  //{
  //printf("LB (x,y) %d = (%3.6f,%3.6f)\n", ii, x_LB[ii], y_LB[ii]);
  //}
  //for (unsigned ii=0;ii<x_RB.size();ii++)
  //{
  //printf("RB (x,y) %d = (%3.6f,%3.6f)\n", ii, x_RB[ii], y_RB[ii]);
  //}

  bool insert_flag = true;
  bool stay_on_road = false;
  bool traffic_lines = false;	
  // lane right boundary
  switch (laneRBType)
    {
    case ROAD_EDGE:
      for (unsigned ii = 0; ii<trafficRules.size(); ii++)
	{
	  if (trafficRules[ii] == STAY_ON_ROAD)
	    {
	      stay_on_road = true;
	      break;
	    }
	}
      if (stay_on_road)
	{
	  constraintSet.rightBoundaryConstraint = boundaryConstraints(x_RB, y_RB, x_LB, y_LB, x_A, y_A, dLimit, insert_flag);
	}
      break;
    case BROKEN_WHITE:
    case SOLID_WHITE:
    case DOUBLE_YELLOW:
      for (unsigned ii = 0; ii<trafficRules.size(); ii++)
	{
	  if (trafficRules[ii] == TRAFFIC_LINES)
	    {
	      traffic_lines = true;
	      break;
	    }
	}
            
      if (traffic_lines)
	{
	  constraintSet.rightBoundaryConstraint = boundaryConstraints(x_RB, y_RB, x_LB, y_LB, x_A, y_A, dLimit, insert_flag);
	}
                
      break;
    default:
      cerr << "ERROR -> Wrong Divider type" << endl;
    } // switch
        
  // lane left boundary
  stay_on_road = false;
  traffic_lines = false;
  switch (laneRBType)
    {
    case ROAD_EDGE:
      for (unsigned ii = 0; ii<trafficRules.size(); ii++)
	{
	  if (trafficRules[ii] == STAY_ON_ROAD)
	    {
	      stay_on_road = true;
	      break;
	    }
	}
      if (stay_on_road)
	{
	  constraintSet.leftBoundaryConstraint = boundaryConstraints(x_LB, y_LB, x_RB, y_RB, x_A, y_A, dLimit, insert_flag);
	}
      break;
    case BROKEN_WHITE:
    case SOLID_WHITE:
    case DOUBLE_YELLOW:
      for (unsigned ii = 0; ii<trafficRules.size(); ii++)
	{
	  if (trafficRules[ii] == TRAFFIC_LINES)
	    {
	      traffic_lines = true;
	      break;
	    }
	}
      if (traffic_lines)
	{
	  constraintSet.leftBoundaryConstraint = boundaryConstraints(x_LB, y_LB, x_RB, y_RB, x_A, y_A, dLimit, insert_flag);
	}
                
      break;
    default:
      cerr << "ERROR -> Wrong Divider type" << endl;
    } // switch

  return constraintSet;
		
}


ConstraintSet CTrafficPlanner::intersectionNav(vector<Mapper::Location> laneLBCoord, vector<Mapper::Location> laneRBCoord, vector<TrafficRules> trafficRules, double dLimit, double x_A, double y_A) 
{
  ConstraintSet constraintSet;
  vector<double> x_RB, y_RB, x_LB, y_LB;
  for (unsigned ii = 0; ii < laneLBCoord.size(); ii++)
    {
      x_LB.push_back(laneLBCoord[ii].x);
      y_LB.push_back(laneLBCoord[ii].y);
    }
  for (unsigned ii = 0; ii < laneRBCoord.size(); ii++)
    {
      x_RB.push_back(laneRBCoord[ii].x);
      y_RB.push_back(laneRBCoord[ii].y);
    }

  // for (unsigned ii=0;ii<x_LB.size();ii++)
    //{
      //  printf("LB (x,y) %d = (%3.6f,%3.6f)\n", ii, x_LB[ii], y_LB[ii]);
      //}
  //for (unsigned ii=0;ii<x_RB.size();ii++)
    //{
      // printf("RB (x,y) %d = (%3.6f,%3.6f)\n", ii, x_RB[ii], y_RB[ii]);
      // }

  bool insert_flag = false;
  constraintSet.rightBoundaryConstraint = boundaryConstraints(x_RB, y_RB, x_LB, y_LB, x_RB[0], y_RB[0], dLimit, insert_flag);
  constraintSet.leftBoundaryConstraint = boundaryConstraints(x_LB, y_LB, x_RB, y_RB, x_LB[0], y_LB[0], dLimit, insert_flag);
  return constraintSet;
}

vector<TrafficRules> CTrafficPlanner::extractRules(SegmentType segmentType)
{
  vector<TrafficRules> trafficRules;
  switch (segmentType)
    {
      /* {STAY_IN_LANE, PASSING, U_TURN, TRAFFIC_LINES, DRIVE_IN_CORRECT_DIRECTION, STAY_ON_ROAD}*/
    case ROAD_SEGMENT:
      trafficRules.push_back(STAY_IN_LANE);
      trafficRules.push_back(PASSING); 
      trafficRules.push_back(TRAFFIC_LINES);
      trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
      trafficRules.push_back(STAY_ON_ROAD);
      break;
      // NEED TO GO THROUGH THESE AND DEFINE LATER  
    case PARKING_ZONE:
      trafficRules.push_back(STAY_IN_LANE);
      trafficRules.push_back(PASSING); 
      trafficRules.push_back(TRAFFIC_LINES);
      trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
      trafficRules.push_back(STAY_ON_ROAD);
      break;  
    case INTERSECTION:
      trafficRules.push_back(STAY_IN_LANE);
      trafficRules.push_back(PASSING); 
      trafficRules.push_back(TRAFFIC_LINES);
      trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
      trafficRules.push_back(STAY_ON_ROAD);
      break;  
    case PREZONE:
      trafficRules.push_back(STAY_IN_LANE);
      trafficRules.push_back(PASSING); 
      trafficRules.push_back(TRAFFIC_LINES);
      trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
      trafficRules.push_back(STAY_ON_ROAD);
      break;  
    case UTURN:
      trafficRules.push_back(STAY_IN_LANE);
      trafficRules.push_back(PASSING); 
      trafficRules.push_back(TRAFFIC_LINES);
      trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
      trafficRules.push_back(STAY_ON_ROAD);
      break;  
    case PAUSE:
      trafficRules.push_back(STAY_IN_LANE);
      trafficRules.push_back(PASSING); 
      trafficRules.push_back(TRAFFIC_LINES);
      trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
      trafficRules.push_back(STAY_ON_ROAD);
      break;  
    case END_OF_MISSION:
      trafficRules.push_back(STAY_IN_LANE);
      trafficRules.push_back(PASSING); 
      trafficRules.push_back(TRAFFIC_LINES);
      trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
      trafficRules.push_back(STAY_ON_ROAD);
      break;  
    default:
      trafficRules.push_back(STAY_IN_LANE);
      trafficRules.push_back(PASSING); 
      trafficRules.push_back(TRAFFIC_LINES);
      trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
      trafficRules.push_back(STAY_ON_ROAD);
    }
	
  // print the traffic rules to the screen to check
  //for (unsigned ii=0;ii<trafficRules.size();ii++)
  //{
  //	cout << "Traffic Rule " << ii << " is " << trafficRules[ii]<<endl;
  //}
	
  return trafficRules;
}

double CTrafficPlanner::closestBoundaryPt(vector<double> xB,vector<double> yB, double x, double y, int& ind_CP, int& ind_Insert, double& x_Pt, double& y_Pt)
{
  // function that finds index of point on boundary (xB,yB) that is closest 
  // to specified point x,y and then calculates the actual closest pt on the boundary
	
  int lengthxB = xB.size();
  double distance = BIGNUMBER;
;
  // closest point on Boundary
  for (int ii = 0; ii<lengthxB; ii++)
    {
      double distance_temp = sqrt(pow(x-xB[ii],2)+pow(y-yB[ii],2));
      if (distance_temp<distance)
	{
	  distance=distance_temp;
	  ind_CP=ii;
	}
    }
	
  // Do a least-squares line fit to number of points around this point
  // for now use 3 points

  double slope, intersection;	
  double x_toFit[3], y_toFit[3];
  int sizeOfXToFit;
  if (ind_CP > 0 && ind_CP+1 < lengthxB)
    {
      sizeOfXToFit = 3;
      x_toFit[0] = xB[ind_CP-1];
      x_toFit[1] = xB[ind_CP];
      x_toFit[2] = xB[ind_CP+1];
      y_toFit[0] = yB[ind_CP-1];
      y_toFit[1] = yB[ind_CP];
      y_toFit[2] = yB[ind_CP+1];
      lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
    }
  else if (ind_CP == 0 && ind_CP+1 < lengthxB)
    {
      sizeOfXToFit = 2;
      x_toFit[0] = xB[ind_CP];
      x_toFit[1] = xB[ind_CP+1];
      y_toFit[0] = yB[ind_CP];
      y_toFit[1] = yB[ind_CP+1];
      lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
    }
  else if (ind_CP > 0 && ind_CP+1 == lengthxB)
    {
      sizeOfXToFit = 2;
      x_toFit[0] = xB[ind_CP-1];
      x_toFit[1] = xB[ind_CP];
      y_toFit[0] = yB[ind_CP-1];
      y_toFit[1] = yB[ind_CP];
      lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
    }
  else
    {
      x_Pt = xB[ind_CP];
      y_Pt = yB[ind_CP];
      ind_Insert = ind_CP;
      return distance;
    }
  // then find best approximation to the point on the line
  // transform into coordinate frame at begin pt of line x-axis along line
  //double theta = atan(slope);
  double theta = atan2(y_toFit[sizeOfXToFit-1]-y_toFit[0], x_toFit[sizeOfXToFit-1]-x_toFit[0]); 
  double R11 = cos(theta);
  double R12 = -sin(theta);
  double R21 = sin(theta);
  double R22 = cos(theta);
  double d_vec1 = xB[ind_CP];
  double d_vec2 = yB[ind_CP];
  double posXYPt_local1 = x;
  double posXYPt_local2 = y;    
  double posXYPt_line1 = R11*(posXYPt_local1-d_vec1)+R21*(posXYPt_local2-d_vec2);
  // double posXYPt_line2 = R12*(posXYPt_local1-d_vec1)+R22*(posXYPt_local2-d_vec2); 
  // find projection of point onto x-axis, which is aligned with line
  double posBDPt_line1 = posXYPt_line1;
  double posBDPt_line2 = 0;
  //    cout << "posBDPt_line1 = " << posBDPt_line1 << endl; 
  // transform new point back to local frame
  double posBDPt_local1 = d_vec1 + R11*posBDPt_line1 + R12*posBDPt_line2;
  double posBDPt_local2 = d_vec2 + R21*posBDPt_line1 + R22*posBDPt_line2;
  x_Pt = posBDPt_local1;
  y_Pt = posBDPt_local2;
  // if the point lies on the line (pos x-axis), then give index of next point
  if (posBDPt_line1>0)
    ind_Insert = ind_CP+1;
  else
    { 
      ind_Insert = ind_CP;
      //ind_CP = ind_CP+1;
    }
  
  distance = sqrt(pow(x_Pt-x,2)+pow(y_Pt-y,2));
  return distance;
}

void CTrafficPlanner::lineFit(double x_toFit[],double y_toFit[],int sizeOfXToFit,double& slope,double& intersection)
{
  // do a least squares fit - assumed only error in y-direction
  double sumX=0, sumX2=0, sumY=0, sumXY=0;
	
  for (int ii=0;ii<sizeOfXToFit;ii++)
    {
      sumX += x_toFit[ii];
      sumY += y_toFit[ii];
      sumX2 += x_toFit[ii]*x_toFit[ii];
      sumXY += x_toFit[ii]*y_toFit[ii];		
    }
	
	
  intersection = (sumY*sumX2-sumX*sumXY)/(sizeOfXToFit*sumX2-sumX*sumX);
  slope = (sizeOfXToFit*sumXY-sumX*sumY)/(sizeOfXToFit*sumX2-sumX*sumX);
}

void CTrafficPlanner::insertPointOnBoundary(vector<double>& xB,vector<double>& yB,double x, double y,int index)
{
  // check if the points are too close to each other
  if (sqrt(pow(xB[index]-x,2)+pow(yB[index]-y,2)) > 2*EPS)
    {
      xB.insert(xB.begin()+index,x);
      yB.insert(yB.begin()+index,y);
    }
}

void CTrafficPlanner::distanceAlongBoundary(vector<double>& xB, vector<double>& yB, int index_start, int& index_inserted, double distance)
{
	
	double dAlongBoundary = 0;
	int lengthxB = (int)xB.size();
	if(distance>0)
	{
	  for (int ii=index_start; ii+1<lengthxB; ii++)
	  {
	  	dAlongBoundary = dAlongBoundary + sqrt(pow(xB[ii]-xB[ii+1],2)+pow(yB[ii]-yB[ii+1],2));
	    
      if (dAlongBoundary>distance)
			{
	  		// find a point on the line segment at distance
			  double slope, intersection;	        
			  double x_toFit[3], y_toFit[3];
			  int sizeOfXToFit;
			  if (ii > 0)
			    {
			      sizeOfXToFit = 3;
			      x_toFit[0] = xB[ii-1];
			      x_toFit[1] = xB[ii];
			      x_toFit[2] = xB[ii+1];
			      y_toFit[0] = yB[ii-1];
			      y_toFit[1] = yB[ii];
			      y_toFit[2] = yB[ii+1];	            
			      lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
			    }
			  else if (ii == 0)
			    {
			      sizeOfXToFit = 2;
			      x_toFit[0] = xB[ii];
			      x_toFit[1] = xB[ii+1];
			      y_toFit[0] = yB[ii];
			      y_toFit[1] = yB[ii+1];	            
			      lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
			    }
			  // in coordinate frame centered at (xB(ii),yB(ii)), with x-axis aligned with the line, specify
			  // distance along x-axis and then transform into local frame
			  //double theta = atan(slope);
			  double theta = atan2(y_toFit[sizeOfXToFit-1]-y_toFit[0], x_toFit[sizeOfXToFit-1]-x_toFit[0]);
			  double R11 = cos(theta);
			  double R12 = -sin(theta);
			  double R21 = sin(theta);
			  double R22 = cos(theta);
			  double d_vec1 = xB[ii];
			  double d_vec2 = yB[ii];
			  double posXY_line1 = sqrt(pow(xB[ii]-xB[ii+1],2)+pow(yB[ii]-yB[ii+1],2))-(dAlongBoundary-distance);
			  double posXY_line2 = 0;    
			  // transform new point back to local frame
			  double posXY_local1 = d_vec1 + R11*posXY_line1 + R12*posXY_line2;
			  double posXY_local2 = d_vec2 + R21*posXY_line1 + R22*posXY_line2;
			  if (sqrt(pow(xB[ii]-posXY_local1,2)+pow(yB[ii]-posXY_local2,2)) > 2*EPS)
			  {
			    index_inserted = ii+1;
			    insertPointOnBoundary(xB, yB, posXY_local1, posXY_local2, index_inserted);
			    break;
			  }
			}
		}
	}
	else 
	{
	  for (int ii=index_start; ii==1; ii--)
	  {
	  	dAlongBoundary = dAlongBoundary + sqrt(pow(xB[ii-1]-xB[ii],2)+pow(yB[ii-1]-yB[ii],2));
	    
      if (dAlongBoundary>fabs(distance))
			{
	  		// find a point on the line segment at distance
			  double slope, intersection;	        
			  double x_toFit[3], y_toFit[3];
			  int sizeOfXToFit;
			  if (ii+1 < lengthxB)
			    {
			      sizeOfXToFit = 3;
			      x_toFit[0] = xB[ii+1];
			      x_toFit[1] = xB[ii];
			      x_toFit[2] = xB[ii-1];
			      y_toFit[0] = yB[ii+1];
			      y_toFit[1] = yB[ii];
			      y_toFit[2] = yB[ii-1];	            
			      lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
			    }
			  else if (ii+1 == lengthxB)
			    {
			      sizeOfXToFit = 2;
			      x_toFit[0] = xB[ii];
			      x_toFit[1] = xB[ii-1];
			      y_toFit[0] = yB[ii];
			      y_toFit[1] = yB[ii-1];	            
			      lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
			    }
			  // in coordinate frame centered at (xB(ii),yB(ii)), with x-axis aligned with the line, specify
			  // distance along x-axis and then transform into local frame
			  //double theta = atan(slope);
			  double theta = atan2(y_toFit[sizeOfXToFit-1]-y_toFit[0], x_toFit[sizeOfXToFit-1]-x_toFit[0]);
			  double R11 = cos(theta);
			  double R12 = -sin(theta);
			  double R21 = sin(theta);
			  double R22 = cos(theta);
			  double d_vec1 = xB[ii];
			  double d_vec2 = yB[ii];
			  double posXY_line1 = sqrt(pow(xB[ii]-xB[ii-1],2)+pow(yB[ii]-yB[ii-1],2))-(dAlongBoundary-fabs(distance));
			  double posXY_line2 = 0;    
			  // transform new point back to local frame
			  double posXY_local1 = d_vec1 + R11*posXY_line1 + R12*posXY_line2;
			  double posXY_local2 = d_vec2 + R21*posXY_line1 + R22*posXY_line2;
			  if (sqrt(pow(xB[ii]-posXY_local1,2)+pow(yB[ii]-posXY_local2,2)) > 2*EPS)
			  {
			    index_inserted = ii-1;
			    insertPointOnBoundary(xB, yB, posXY_local1, posXY_local2, index_inserted);
			    break;
			  }
			  else
			  	index_inserted = ii;
			}
		}
		
	}
}

vector<Constraint> CTrafficPlanner::boundaryConstraints(vector<double> xB1, vector<double> yB1, vector<double> xB2, vector<double> yB2, double x, double y, double distance, bool insert_flag)
{
  vector<Constraint> constraints;
  if (xB1.size() <= 1)
    {
      cerr << "Not enough points in boundary (in boundaryConstraints function)." << endl; 
      return constraints;
    }
  // closest point on B1
  double x_temp_B1, y_temp_B1;
  double x_temp_B2, y_temp_B2;	
  int ind_B1_Ins, ind_B2_Ins, ind_B1_CP, ind_B2_CP;
  double dist;
    if (xB1.size()>1)
      {
	dist = closestBoundaryPt(xB1, yB1, x, y, ind_B1_CP, ind_B1_Ins, x_temp_B1, y_temp_B1);
      }
    else
    {
      ind_B1_Ins = 0;
      ind_B1_CP = 0;
      x_temp_B1 = xB1[ind_B1_CP];
      y_temp_B1 = yB1[ind_B1_CP];
    }
  // closest point on B2
  if (xB2.size()>1)
    {
      dist = closestBoundaryPt(xB2, yB2, x, y, ind_B2_CP, ind_B2_Ins, x_temp_B2, y_temp_B2);
    }
  else
    {
      ind_B2_Ins = 0;
      ind_B2_CP = 0;
      x_temp_B2 = xB2[ind_B2_CP];
      y_temp_B2 = yB2[ind_B2_CP];
    }
  // flag that says whether or not I need to insert these points into the
  // boundary so that these points are at position index
  int ind_B1, ind_B2;
  if (insert_flag)
    {
      insertPointOnBoundary(xB1,yB1,x_temp_B1,y_temp_B1,ind_B1_Ins);
      insertPointOnBoundary(xB2,yB2,x_temp_B2,y_temp_B2,ind_B2_Ins);
      ind_B1 = ind_B1_Ins;
      ind_B2 = ind_B2_Ins;
    }
  else
    {
      ind_B1 = ind_B1_CP;
      ind_B2 = ind_B2_CP;
    }

	
  // Generate constraints for the all the points on the boudary in
  // the next X meters along the boundary
  double dAlongBoundary = 0;
  double slope, intersection;
  if (ind_B1+1 == (int)xB1.size())
    {
      ind_B1--;
    }
  for (int ii = ind_B1; ii+1<(int)xB1.size(); ii++)
    {
      Constraint constraint;
      // check that denominator is ~= 0
      if (xB1[ii+1]-xB1[ii] != 0)
	{
	  slope=(yB1[ii+1]-yB1[ii])/(xB1[ii+1]-xB1[ii]);
	}
      else
	{
	  slope = ((yB1[ii+1]-yB1[ii])/fabs(yB1[ii+1]-yB1[ii]))*BIGNUMBER;
	}
      
      intersection=yB1[ii]-slope*xB1[ii];
      // Check which side the closest point on the left boundary is
      // for the line segment on the right boundary closest to you
      double value = yB2[ind_B2] - slope*xB2[ind_B2] - intersection;
      if (value > 0)
	{
	  // I also want to be on this side -> above line
	  // segment, but need to specify as Ax-B<=0
	  constraint.A1 = slope;
	  constraint.A2 = -1; 
	  constraint.B = -intersection;
	}
      else if (value < 0)
	{
	  constraint.A1 = -slope;
	  constraint.A2 = 1;
	  constraint.B = intersection;
	}
      else
	{
	  cerr<<"ERROR in boundary constraint generation function "<<endl;
	}
      constraint.xrange1 = xB1[ii];
      constraint.xrange2 = xB1[ii+1];
      constraint.yrange1 = yB1[ii];
      constraint.yrange2 = yB1[ii+1];	
      constraints.push_back(constraint); 
      dAlongBoundary += sqrt(pow(xB1[ii]-xB1[ii+1],2)+pow(yB1[ii]-yB1[ii+1],2));
      //	    cout << "dAlongBoundary = " << dAlongBoundary << endl;
      if (dAlongBoundary - distance > 0)
	{
	  return constraints;
	}
    }
  return constraints;
}

//bool CTrafficPlanner::convRDDF( vector<Constraint> line1, vector<Constraint> line2, SegGoals currentSegGoals, double x_A, double y_A, double laneWidth, RDDF* newRddf) 
bool CTrafficPlanner::convRDDF( vector<Constraint> line1, vector<Constraint> line2, SegGoals currentSegGoals, double x_A, double y_A, double laneWidth, vector<double>& rddfx, vector<double>& rddfy, vector<double>& rddfr)
{  
  int i, j, k, l1, l2 ;

  //%%%%%%%%%%%%%%%%%%%%%%%%
  //% FIND CLOSEST SEGMENTS
  //%%%%%%%%%%%%%%%%%%%%%%%%
  bool error = false ;
  //double apos[] = {x_A, y_A};
  int numline1seg = line1.size();
  //cout << "numline1seg = " << numline1seg << endl;
  int numline2seg = line2.size();
  //cout << "numline2seg = " << numline2seg << endl;
  double l1p[numline1seg+1][2], l2p[numline2seg+1][2] ;
  for( i = 0 ; i < numline1seg ; i++ ) {
    l1p[i][0] = line1[i].xrange1 ;
    l1p[i][1] = line1[i].yrange1 ;
    //printf("line1[%d] (x1,y1) = (%3.6f,%3.6f)\n", i, l1p[i][0], l1p[i][1]);
  }
  l1p[numline1seg][0] = line1[numline1seg-1].xrange2 ;
  l1p[numline1seg][1] = line1[numline1seg-1].yrange2 ;
  //printf("line1[%d] (x2,y2) = (%3.6f,%3.6f)\n", i, l1p[numline1seg][0], l1p[numline1seg][1]);
  
  int numline1pts = numline1seg + 1 ;

  for( i = 0; i < numline2seg ; i++ ) {
    l2p[i][0] = line2[i].xrange1 ;
    l2p[i][1] = line2[i].yrange1 ;
    //printf("line2[%d] (x1,y1) = (%3.6f,%3.6f)\n", i, l2p[i][0], l2p[i][1]);
  }
  l2p[numline2seg][0] = line2[numline2seg-1].xrange2 ;
  l2p[numline2seg][1] = line2[numline2seg-1].yrange2 ;
  //printf("line2[%d] (x1,y1) = (%3.6f,%3.6f)\n", numline2seg, l2p[numline2seg][0], l2p[numline2seg][1]);
  int numline2pts = numline2seg + 1 ;
  //cout << "numline2pts = " << numline2pts << endl;
  // Check that end points of two lines are not the same (full lane obstacle)
  if( (l1p[numline1seg][0] == l2p[numline2seg][0]) && (l1p[numline1seg][1] == l2p[numline2seg][1]) )
    {
      error = true ;
      cerr << "ERROR convRDDF: (l1p[numline1seg][0] == l2p[numline2seg][0]) && (l1p[numline1seg][1] == l2p[numline2seg][1])" << endl;
    }

  double mpt[2] = {0, 0} ;
  double r = 0 ;

  // Line 1
  //cout << "line 1" << endl;
  double min1, min2, dist, xdiff, ydiff ;
  int min1num, min2num;
  bool skip = false ;
  double a[2], b[2], maga, magb, adotb, th, check[4], check1[4], check2[4] ;
  point cpt_, cpt1_, cpt2_, mpt_ ;
  double cpt[2], pnt[2] ;
  double rddfln1[numline1pts][3], rddfln2[numline2pts][3] ;
  for( i = 0 ; i < numline1pts ; i++ ) {
    min1 = BIGNUMBER ;
    min2 = BIGNUMBER ;
    min1num = 0 ;
    min2num = 0 ;
    for( j = 0; j < numline2pts; j++ ) {
      xdiff = l1p[i][0] - l2p[j][0] ;
      ydiff = l1p[i][1] - l2p[j][1] ;
      dist = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
      if( dist < min1 ) {
	if( dist < min2 ) {
	  min1 = min2 ;
	  min1num = min2num ;
	  min2 = dist ;
	  min2num = j ;
	} else {
	  min1 = dist ;
	  min1num = j ;
	}
      }
    }
    skip = false ;
    if( (i == 0) || (i == (numline1pts-1))) {
      if( i == 0 ) {
	a[0] = l1p[0][0] - l2p[0][0] ;
	a[1] = l1p[0][1] - l2p[0][1] ;
	b[0] = l2p[1][0] - l2p[0][0] ;
	b[1] = l2p[1][1] - l2p[0][1] ;
	maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
	magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
	adotb = a[0] * b[0] + a[1] * b[1] ;
      } else {
	a[0] = l1p[numline1seg][0] - l2p[numline2seg][0] ;
	a[1] = l1p[numline1seg][1] - l2p[numline2seg][1] ;
	b[0] = l2p[numline2seg-1][0] - l2p[numline2seg][0] ;
	b[1] = l2p[numline2seg-1][1] - l2p[numline2seg][1] ;
	maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
	magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
	adotb = a[0] * b[0] + a[1] * b[1] ;
      }
      th = acos( adotb/(maga * magb) ) ;
      if( th > (PI / 2 + EPS) ) 
	skip = true ;
    }
    if( skip ) {
      mpt[0] = 0 ;
      mpt[1] = 0 ;
      r = 0 ;
    } else {
      pnt[0] = l1p[i][0] ;
      pnt[1] = l1p[i][1] ;
      if( (min1num + 1 == min2num) || (min2num + 1 == min1num)) {
	//fprintf( stderr, "min1num = %i, minnum2 = %i\n" , min1num, min2num) ;
	if( min1num < min2num ) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 )
	      check[k] = l2p[min1num][k] ;
	    else
	      check[k] = l2p[min1num+1][k-2];
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check[k] = l2p[min2num][k] ;
	    else
	      check[k] = l2p[min2num+1][k-2] ;
	  }
	}
	cpt_ = closept( check, pnt ) ;
	cpt[0] = cpt_.x ;
	cpt[1] = cpt_.y ;
	mpt_ = midpt( cpt, pnt ) ;
	mpt[0] = mpt_.x ;
	mpt[1] = mpt_.y ;
	r = radius( mpt, pnt, cpt ) ;
      }  else if( (min1num + 2 == min2num) || (min2num + 2 == min1num)) {
	if( min1num < min2num ) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if ( k < 2 ) {
	      check1[k] = l2p[min1num][k] ;
	      check2[k] = l2p[min2num-1][k] ;
	    } else {
	      check1[k] = l2p[min1num+1][k-2] ;
	      check2[k] = l2p[min2num][k-2] ;
	    }
	  }
	} else { 
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) {
	      check1[k] = l2p[min2num][k] ;
	      check2[k] = l2p[min1num-1][k] ;
	    } else {
	      check1[k] = l2p[min2num+1][k-2] ;
	      check2[k] = l2p[min1num][k-2] ;
	    }
	  }
	}
	cpt1_ = closept(check1, pnt) ;
	cpt2_ = closept(check2, pnt) ;
	double xdiff1 = cpt1_.x - pnt[0] ;
	double ydiff1 = cpt1_.y - pnt[1] ;
	double xdiff2 = cpt2_.x - pnt[0];
	double ydiff2 = cpt2_.y - pnt[1] ;
	double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
	double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
	if( dist1 < dist2 ) {
	  cpt[0] = cpt1_.x ;
	  cpt[1] = cpt1_.y ;
	} else {
	  cpt[0] = cpt2_.x ;
	  cpt[1] = cpt2_.y ;
	}
	mpt_ = midpt( cpt, pnt ) ;
	mpt[0] = mpt_.x ;
	mpt[1] = mpt_.y ;
	r = radius( mpt, pnt, cpt ) ;
      } else {
	if( min1num < (numline2pts-1) ) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check1[k] = l2p[min1num][k] ;
	    else
	      check1[k] = l2p[min1num+1][k-2] ;
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 )
	      check1[k] = l2p[min1num-1][k] ;
	    else 
	      check1[k] = l2p[min1num][k-2] ;
	  }
	}
	if( min2num < (numline2pts-1) ) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check2[k] = l2p[min2num][k] ;
	    else
	      check2[k] = l2p[min2num+1][k-2] ;
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 )
	      check2[k] = l2p[min2num-1][k] ;
	    else 
	      check2[k] = l2p[min2num][k-2] ;
	  }
	}
	cpt1_ = closept(check1, pnt) ;
	cpt2_ = closept(check2, pnt) ;
	double xdiff1 = cpt1_.x - pnt[0] ;
	double ydiff1 = cpt1_.y - pnt[1] ;
	double xdiff2 = cpt2_.x - pnt[0];
	double ydiff2 = cpt2_.y - pnt[1] ;
	double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
	double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
	if( dist1 < dist2 ){
	  cpt[0] = cpt1_.x ;
	  cpt[1] = cpt1_.y ;
	} else {
	  cpt[0] = cpt2_.x ;
	  cpt[1] = cpt2_.y ;
	}
	mpt_ = midpt( cpt, pnt ) ;
	mpt[0] = mpt_.x ;
	mpt[1] = mpt_.y ;
	r = radius( mpt, pnt, cpt ) ;
      }
    }
    if( r > laneWidth / 2 )
      r = laneWidth / 2 ;

    rddfln1[i][0] = mpt[0] ;
    rddfln1[i][1] = mpt[1] ;
    rddfln1[i][2] = r ;
    //printf("rddfln1[%d] (x,y) = (%3.6f,%3.6f)\n", i, rddfln1[i][0], rddfln1[i][1]);
  }

  // Line 2
  //cout << "line 2" << endl;
  for( i = 0 ; i < numline2pts ; i++ ) {
    min1 = BIGNUMBER ;
    min2 = BIGNUMBER ;
    min1num = 0 ;
    min2num = 0 ;
    for( j = 0 ; j < numline1pts ; j++ ) {
      xdiff = l2p[i][0] - l1p[j][0] ;
      ydiff = l2p[i][1] - l1p[j][1] ;
      dist = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
      if( dist < min1 ) {
	if( dist < min2 ) {
	  min1 = min2 ;
	  min1num = min2num ;
	  min2 = dist ;
	  min2num = j ;
	} else {
	  min1 = dist ;
	  min1num = j ;
	}
      }
    }
    skip = false ;
    if( (i == 0) || (i == numline2seg) ) {
      if( i == 0 ) { 
	a[0] = l2p[0][0] - l1p[0][0] ;
	a[1] = l2p[0][1] - l1p[0][1] ;
	b[0] = l1p[1][0] - l1p[0][0] ;
	b[1] = l1p[1][1] - l1p[0][1] ;
	maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
	magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
	adotb = a[0] * b[0] + a[1] * b[1] ;
      } else {
	a[0] = l2p[numline2pts-1][0] - l1p[numline1pts-1][0] ;
	a[1] = l2p[numline2pts-1][1] - l1p[numline1pts-1][1] ;
	b[0] = l1p[numline1pts-2][0] - l1p[numline1pts-1][0] ;
	b[1] = l1p[numline1pts-2][1] - l1p[numline1pts-1][1] ;
	maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
	magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
	adotb = a[0] * b[0] + a[1] * b[1] ;
      }
      th = acos( adotb/(maga * magb) ) ;
      if( th > (PI / 2 + EPS) ) 
	skip = true ;

    }
    if( skip ) {
      mpt[0] = 0 ;
      mpt[1] = 0 ;
      r = 0 ;
    } else {
      pnt[0] = l2p[i][0] ;
      pnt[1] = l2p[i][1] ;
      //    fprintf( stderr, "min1 = %i, min2 = %i\n", min1num, min2num ) ;
      if( (min1num + 1 == min2num) || (min2num + 1 == min1num) ) {
	if( min1num < min2num ) {
	  for( k = 0 ; k < 4 ; k++ ){
	    if( k < 2 )
	      check[k] = l1p[min1num][k] ;
	    else
	      check[k] = l1p[min1num+1][k-2];
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 )
	      check[k] = l1p[min2num][k] ;
	    else
	      check[k] = l1p[min2num+1][k-2];
	  }
	}
	cpt_ = closept( check, pnt ) ;
	cpt[0] = cpt_.x ;
	cpt[1] = cpt_.y ;
	//       fprintf( stderr, "1: cpt[%i] = %lf, %lf\n", i, cpt[0], cpt[1] ) ;
	mpt_ = midpt( cpt, pnt ) ;
	mpt[0] = mpt_.x ;
	mpt[1] = mpt_.y ;
	r = radius( mpt, pnt, cpt ) ;
      } else if( (min1num + 2 == min2num) || (min2num + 2 == min1num)) {
	if( min1num < min2num) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if ( k < 2 ) {
	      check1[k] = l1p[min1num][k] ;
	      check2[k] = l1p[min2num-1][k] ;
	    } else {
	      check1[k] = l1p[min1num+1][k-2] ;
	      check2[k] = l1p[min2num][k-2] ;
	    }
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if ( k < 2 ) {
	      check1[k] = l1p[min2num][k] ;
	      check2[k] = l1p[min1num-1][k] ;
	    } else {
	      check1[k] = l1p[min2num+1][k-2] ;
	      check2[k] = l1p[min1num][k-2] ;
	    }
	  }
	}
	cpt1_ = closept(check1, pnt) ;
	cpt2_ = closept(check2, pnt) ;
	double xdiff1 = cpt1_.x - pnt[0] ;
	double ydiff1 = cpt1_.y - pnt[1] ;
	double xdiff2 = cpt2_.x - pnt[0];
	double ydiff2 = cpt2_.y - pnt[1] ;
	double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
	double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
	if( dist1 < dist2 ) {
	  cpt[0] = cpt1_.x ;
	  cpt[1] = cpt1_.y ;
	} else {
	  cpt[0] = cpt2_.x ;
	  cpt[1] = cpt2_.y ;
	}
	//       fprintf( stderr, "2: cpt[%i] = %lf, %lf\n", i, cpt[0], cpt[1] ) ;
	mpt_ = midpt( cpt, pnt ) ;
	mpt[0] = mpt_.x ;
	mpt[1] = mpt_.y ;
	r = radius( mpt, pnt, cpt ) ;
      } else {
	if( min1num < numline1seg ) {
	  for( k = 0 ; k < 4 ; k++ ){
	    if( k < 2 ) 
	      check1[k] = l1p[min1num][k] ;
	    else
	      check1[k] = l1p[min1num+1][k-2] ;
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check1[k] = l1p[min1num-1][k] ;
	    else
	      check1[k] = l1p[min1num][k-2] ;
	  }
	}
	if( min2num < numline1seg ) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check2[k] = l1p[min2num][k] ;
	    else
	      check2[k] = l1p[min2num+1][k-2] ;
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check2[k] = l1p[min2num-1][k] ;
	    else
	      check2[k] = l1p[min2num][k-2] ;
	  }
	}
	cpt1_ = closept(check1, pnt) ;
	cpt2_ = closept(check2, pnt) ;
	double xdiff1 = cpt1_.x - pnt[0] ;
	double ydiff1 = cpt1_.y - pnt[1] ;
	double xdiff2 = cpt2_.x - pnt[0];
	double ydiff2 = cpt2_.y - pnt[1] ;
	double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
	double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
	if( dist1 < dist2 ) {
	  cpt[0] = cpt1_.x ;
	  cpt[1] = cpt1_.y ;
	} else {
	  cpt[0] = cpt2_.x ;
	  cpt[1] = cpt2_.y ;
	}
	//       fprintf( stderr, "3: cpt[%i] = %lf, %lf\n", i, cpt[0], cpt[1] ) ;
	mpt_ = midpt( cpt, pnt ) ;
	mpt[0] = mpt_.x ;
	mpt[1] = mpt_.y ;
	r = radius( mpt, pnt, cpt ) ;
      }
    }
    if( r > laneWidth / 2)
      r = laneWidth / 2 ;

    rddfln2[i][0] = mpt[0] ;
    rddfln2[i][1] = mpt[1] ;
    rddfln2[i][2] = r ;
    
    //printf("rddfln2[%d] (x,y) = (%3.6f,%3.6f)\n", i, rddfln2[i][0], rddfln2[i][1]);
  }

  //%%%%%%%%%%%%%%%%%%%%%%%%
  //% SORT RDDF POINTS
  //%%%%%%%%%%%%%%%%%%%%%%%%
  //cout << "sort rddf points" << endl;
  l1 = 0 ;
  l2 = 0 ;
  // int count // It is now passed as an argument to the function
  //count = 0 ;
  double xdiff1, ydiff1, dist1, xdiff2, ydiff2, dist2 ;
  //int nlp = numline1pts + numline2pts ;
  //cout << "numline1pts = " << numline1pts << endl;
  //cout << "numline2pts = " << numline2pts << endl;
  //cout << "nlp = " << nlp << endl;
  //double rddf[nlp][3];	// this is now passed as an argument to the function
  while( (l1 < numline1pts) || (l2 < numline2pts) ) 
    {
      //cout << "here now 1" << endl;
      if( (rddfln1[l1][0] == 0) && (rddfln1[l1][1] == 0) && (rddfln1[l1][2] == 0))
        l1++;
      if( (rddfln2[l2][0] == 0) && (rddfln2[l2][1] == 0) && (rddfln2[l2][2] == 0))
        l2++ ;
        
      if( l1 < numline1pts ) 
	{
	  xdiff1 = rddfln1[l1][0] - rddfln1[0][0] ;
	  ydiff1 = rddfln1[l1][1] - rddfln1[0][1] ;
	  dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
	} 
      //else
      //dist1 = -1 ;
      //cout << "here now 2" << endl;
      if( l2 < numline2pts ) 
	{
	  xdiff2 = rddfln2[l2][0] - rddfln2[0][0] ;
	  ydiff2 = rddfln2[l2][1] - rddfln2[0][1] ;
	  dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
	} 
      //else
      //dist2 = -1 ;
      //cout << "here now 3" << endl;
      //if( dist1 < dist2 || dist2 < 0 ) 
      if( (l1 < numline1pts) && (dist1 <= dist2) || (l2 >= numline2pts) )
	{
//	  rddf[count][0] = rddfln1[l1][0];
//	  rddf[count][1] = rddfln1[l1][1];
//	  rddf[count][2] = rddfln1[l1][2];
		rddfx.push_back(rddfln1[l1][0]);
		rddfy.push_back(rddfln1[l1][1]);
		rddfr.push_back(rddfln1[l1][2]);
	  //printf("rddf[%d] from rddfln1 (x,y) = (%3.6f,%3.6f), (%3.6f,%3.6f)\n", count, rddf[count][0], rddf[count][1], rddfln1[l1][0], rddfln1[l1][1]);
	  l1++ ;
	  //count++ ;
	  
	} 
      else if ( l2 < numline2pts )
	{
//	  rddf[count][0] = rddfln2[l2][0];
//	  rddf[count][1] = rddfln2[l2][1];
//	  rddf[count][2] = rddfln2[l2][2];
		rddfx.push_back(rddfln2[l2][0]);
		rddfy.push_back(rddfln2[l2][1]);
		rddfr.push_back(rddfln2[l2][2]);
	  //printf("rddf[%d] from rddfln2 (x,y) = (%3.6f,%3.6f)\n", count, rddf[count][0], rddf[count][1]);
	  l2++ ;
	  //count++ ;

	}

    } 
  
  //cout << "RDDF points count = " << count << endl;


  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  //% CHECK FOR CORRIDOR FEASIBILITY
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  //cout << "checking feasibility" << endl;
  //cout << "rddfr.size() = " << rddfr.size() << endl;
  for( unsigned ii = 0 ; ii < rddfr.size() ; ii++ ) 
  {
    //cout << "radius [" << ii << "] = " << rddfr[ii] << endl;
    if( rddfr[ii] < VEHICLE_WIDTH / 2 )
    {
			error = true ;
			cerr << "ERROR convRDDF: rddfr[ii] < VEHICLE_WIDTH / 2" << endl;
    }
    //int dummy;
    //cin >> dummy;
  }
	//cout << "exiting convRDDF loop" << endl;
  return(error) ;
}

point CTrafficPlanner::closept( double lnseg[4], double pt[2] ) 
{ 
  double min[2], max[2], a[2], b1[2], b2[2] ;
  double maga, magb, adotb1, adotb2, minth, maxth, distmin, distln, distmax ;

  min[0] = lnseg[0] ;
  min[1] = lnseg[1] ;
  max[0] = lnseg[2] ;
  max[1] = lnseg[3] ;

  a[0] = pt[0] - min[0] ;
  a[1] = pt[1] - min[1] ;
  b1[0] = max[0] - min[0] ;
  b1[1] = max[1] - min[1] ;
  maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
  magb = sqrt( b1[0] * b1[0] + b1[1] * b1[1] ) ;
  adotb1 = a[0] * b1[0] + a[1] * b1[1] ;
  minth = acos(adotb1/(maga*magb)) ;
  distmin = maga ;
  distln = magb ;

  a[0] = pt[0] - max[0] ;
  a[1] = pt[1] - max[1] ;
  b2[0] = min[0] - max[0] ;
  b2[1] = min[1] - max[1] ;
  maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
  adotb2 = a[0] * b2[0] + a[1] * b2[1] ;
  maxth = acos(adotb2/(maga*magb)) ;
  distmax = maga ;

  double mdist1, a1, mdist2, a2, check, apt1[2], apt2[2] ;
  point cpt ;
  if( (minth <= PI/2) && (maxth <= PI/2 ) && (minth > 0) && (maxth > 0)) {
    mdist1 = distmin * sin(minth) ;
    a1 = mdist1 / tan(minth) ;
    mdist2 = distmax * sin(maxth) ;
    a2 = mdist2 / tan(maxth) ;
    check = fabs( a1 + a2 - distln ) ;
    apt1[0] = ( a1 / distln ) * b1[0] + min[0] ;
    apt1[1] = ( a1 / distln ) * b1[1] + min[1] ;
    apt2[0] = ( a2 / distln ) * b2[0] + max[0] ;
    apt2[1] = ( a2 / distln ) * b2[1] + max[1] ;
    if( check < 0.1 ){ 
      cpt.x = ( apt1[0] + apt2[0] ) / 2 ;
      cpt.y = ( apt1[1] + apt2[1] ) / 2 ;
    } else {
      if( mdist1 < mdist2 ) {
	cpt.x = apt1[0] ;
	cpt.y = apt1[1] ;
      } else { 
	cpt.x = apt2[0] ;
	cpt.y = apt2[1] ;
      }
    }
  } else {
    if( distmin < distmax ) {
      cpt.x = min[0] ;
      cpt.y = min[1] ;
    } else {
      cpt.x = max[0] ;
      cpt.y = max[1] ;
    }
  }
  return(cpt) ;
}

point CTrafficPlanner::midpt( double pt1[2], double pt2[2] ) 
{ 
  point mpt ;

  mpt.x = (pt1[0] + pt2[0]) / 2 ;
  mpt.y = (pt1[1] + pt2[1]) / 2 ;

  return(mpt) ;
}

double CTrafficPlanner::radius( double mpt[2], double pt1[2], double pt2[2] ) 
{
  double xdiff, ydiff, dist1, dist2, r ;

  xdiff = (mpt[0] - pt1[0]) ;
  ydiff = (mpt[1] - pt1[1]) ;
  dist1 = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
  xdiff = (mpt[0] - pt2[0]) ;
  ydiff = (mpt[1] - pt2[1]) ;
  dist2 = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
  if( dist1 <= dist2 )
    r = dist1 ;
  else
    r = dist2 ;

  return(r) ;
}

void CTrafficPlanner::generateMapFromRNDF(RNDF* RNDFObj, Mapper::Map& map)
{
  int numwpnts;
  //Waypoint * wpnt; 
  //std::GPSPoint::Waypoint * wpnt;
    
  vector<Mapper::Segment> segments;
   
  //For every segment in the RNDF
  for (int ii=1; ii <= RNDFObj->getNumOfSegments();ii++)
  {
    
    //Make a new segment
    vector<Mapper::Lane> seg_lanes;
      
    //Get the segment
    //std::Segment * rseg = RNDFObj->getSegment(ii);
      
    //For every lane in the segment
    //for (int jj=0;jj<rseg->getNumberofLanes();jj++)
    for (int jj=1; jj<=(RNDFObj->getSegment(ii))->getNumOfLanes(); jj++)
    {
      //std::Lane * rlane = rseg->getLane(jj);
      //numwpnts = rlane->getNumberofWaypoinse();
      numwpnts = (( RNDFObj->getSegment(ii))->getLane(jj))->getNumOfWaypoints();
      vector<Mapper::Location> centerline;
      
      for (int nn=1; nn<=numwpnts; nn++)
	{ 
	  Mapper::Location loc = { ((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn)->getEasting(),
				   ((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn)->getNorthing() };
	              
	  centerline.push_back (loc);        
	  
//	  //check to see if the waypoint is a stopline
//	  if ((((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn))->isStopSign())
//	    {
//	      //Mapper::StopLine stopLine = Mapper::StopLine("", ((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn)->getEasting(), 
//				//		   ((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn)->getNorthing() );
//				Mapper::StopLine stopLine = Mapper::StopLine("", loc.x, loc.y);
//	      lane.addStopLine(stopLine);
//	      printf("Stopline loc = (%3.6f, %3.6f)\n",stopLine.getXLoc(), stopLine.getYLoc());
//	    } 
	   
	}
      
      //Make a new lane object
      Mapper::Lane lane = Mapper::Lane(jj, centerline);
      
      // Add stoplines to the lanes as necessary
      for (int nn=1; nn<=numwpnts; nn++)
	{ 
	  //check to see if the waypoint is a stopline
	  if ((((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn))->isStopSign())
	    {
	      Mapper::StopLine stopLine = Mapper::StopLine("", ((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn)->getEasting(), 
						   ((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn)->getNorthing() + 1);
				//Mapper::StopLine stopLine = Mapper::StopLine("", loc.x, loc.y);
	      lane.addStopLine(stopLine);
	      //cout << "segmentID = " << ii << endl;
	      //cout << "laneID = " << lane.getID() << endl;
	      //vector<Mapper::StopLine> stoplines_temp = lane.getStopLines();
	      //cout << "stoplines_temp.size() = " << stoplines_temp.size() << endl;
	      //printf("Stopline loc = (%3.6f, %3.6f)\n",stopLine.getXLoc(), stopLine.getYLoc());
	      //printf("Stopline loc = (%3.6f, %3.6f)\n",((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn)->getEasting(), ((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn)->getNorthing());
	    } 
  }
      
		  
      //put all of the lanes in a segment 
      seg_lanes.push_back(lane);
      
      if (jj==1)
	{ //again, compensating for 1-indexing
	  Mapper::Lane lane = Mapper::Lane(jj, centerline);
	  seg_lanes.push_back(lane);       
    	}
        
    }
        
    //push that segment onto the vector of segments
    Mapper::Segment seg = Mapper::Segment(ii,seg_lanes);
    //put that segment on the vector of segments 
    segments.push_back(seg);
        
    if (ii==1)
    { //compensating for 1-indexing
      Mapper::Segment seg = Mapper::Segment(ii,seg_lanes);
      segments.push_back(seg);
    }
    //Mapper::Lane lanes_temp = seg.getLane(1); 
    //vector<Mapper::StopLine> stoplines_temp2 = lanes_temp.getStopLines();
   	//cout << "stoplines_temp2.size() = " << stoplines_temp2.size() << endl;
     
  }
  map = Mapper::Map(segments);
  //Mapper::Segment seg_temp = map.getSegment(1);
  //Mapper::Lane lanes_temp2 = seg_temp.getLane(1); 
  //vector<Mapper::StopLine> stoplines_temp3 = lanes_temp2.getStopLines();
  //cout << "stoplines_temp3.size() = " << stoplines_temp3.size() << endl;
  
}
