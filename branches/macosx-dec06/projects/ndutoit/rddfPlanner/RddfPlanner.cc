/*
 * RddfPlanner.cc - simple planner using RDDF centerline
 *
 * RMM, 26 Nov 06
 *
 * This planner reads in an RDDF across skynet and sends out a
 * trajectory corresponding to the centerline of the RDDF corridor.
 *
 * This module was modified from rddfPathGen and it still has some
 * relics of that code.  
 *
 */

#define TRYTIMBER

#include "RddfPlanner.hh"
#include <string>

/* SparrowHawk files, including displays */
#include "sparrowhawk.hh"
#include "maindisp.h"

using namespace std;

//Include the header file for the refinement stage of the planner, required in order to use
//Dima's make traj C2 function (used to convert the trajs output by RDDFPathGen to C2 just
//before they are output from RDDFPathGen
#include "RefinementStage.h"

//RDDF& rddf = *(new RDDF)


int NOSKYNET = 0;                /**< for command line argument */
int WRITE_COMPLETE_DENSE = 0;    /**< for command line argument */
int WRITE_COMPLETE_SPARSE = 1;   /**< for command line argument */
int WRITE_EVERY_TRAJ = 0;        /**< for command line argument */
int WAIT_STATE = true;           /**< for command line argument */
int NOMM = 0;                    /**< for command line argument; 
				    not using ModeManModule */
int C2 = 0;                      /**< whether or not to C2-ify the trajs
				  * 0 = none
				  * 1 = planner c2 function
				  * 2 = traj.feaziblize */
int C2SPECIFIED = 0;             /**< whether or not the C2 or noC2 option was specified */
/** for command line arguemnt */
int TIMBER_DEBUG_LEVEL = DEFAULT_DEBUG;


RddfPlanner::RddfPlanner(int skynetKey) : CSkynetContainer(SNRddfPathGen, skynetKey), CStateClient(WAIT_STATE), CTimberClient(timber_types::rddfPathGen), CRDDFTalker(true)
{
  setDebugLevel(TIMBER_DEBUG_LEVEL);
  // do something here if you want
}



RddfPlanner::~RddfPlanner()
{
  // Do we need to destruct anything??
  cout << "RddfPlanner destructor has been called." << endl;
}

void RddfPlanner::ActiveLoop()
{
  /**
   * this is the main function which is run.  execution should be trapped
   * somewhere in this funciton during operation.
   */
  printf("[%s:%d] Entering RddfPlanner::ActiveLoop()\n", __FILE__, __LINE__);

  // INITIALIZATION
  // set up message counter
  int message_counter = 0;

  // echo SKYNET_KEY
  cout << "\n\nCurrent SKYNET_KEY: ";
  cout.flush();
  system("echo $SKYNET_KEY");

  /*
   * Set up network connection if we are running using skynet
   */
  int trajSocket;
  int skynetguiSocket;

  if(!NOSKYNET)
    {
      if (NOMM)
		trajSocket = m_skynet.get_send_sock(SNtraj);
      else 
	  {
	  skynetguiSocket = m_skynet.get_send_sock(SNtraj);
	  trajSocket = m_skynet.get_send_sock(SNRDDFtraj);
	  }
    }
  
  // generate blank trajectory to be filled in by planner
  CTraj* ptraj;
  ptraj = new CTraj(3);  // order 3 (by default)

  // Figure out how long to sleep for at the end of the loop
  int usleep_time = 1000000/FREQUENCY; // set frequency in RddfPlanner.hh
  
  //Declare the refinement stage for the planner required when using Dimas make traj
  //C2 function
  CRefinementStage refstage;

  // some variables used for logging
  bool logging_enabled;
  string logging_location;
  int logging_level;
  
  while (true) {
    // Main execution loop.  
    // This is where we're trapped during normal program execution
    message_counter++;

    /* Get the RDDF via RDDFTalker */
    cout << "rddfPlanner: waiting for RDDF" << endl;
    WaitForRDDF(); UpdateRDDF();
    RDDF rddf = m_newRddf;
    cout << "rddfPlanner: received RDDF with " << rddf.getNumTargetPoints()
	 << " points" << endl;
      
    rddf.print();
      
    // extract the info from waypoints and store in corridor
    int N = rddf.getNumTargetPoints();   // number of points
    RDDFVector& waypoints = *(new RDDFVector);
    waypoints = rddf.getTargetPoints();

    // store data in corridor
    corridorstruct corridor_whole;
    corridor_whole.numPoints = N;
    for(int i = 0; i < N; i++) {
	      // shouldn't get any segfaults here, but who knows...
	      corridor_whole.e.push_back(waypoints[i].Easting);
	      corridor_whole.n.push_back(waypoints[i].Northing);
	      //shrink the corridor width to account for vehicle width, tracking error
	      corridor_whole.width.push_back(waypoints[i].radius-RDDF_VEHWIDTH_EFF);
	      corridor_whole.speedLimit.push_back(waypoints[i].maxSpeed);
	      
	      if (waypoints[i].radius-RDDF_VEHWIDTH_EFF < .01)
			cerr << "CORRIDOR WIDTH IS SMALLER THAN RDDF_VEHWIDTH_EFF!" << endl;
	
	      //###
	      //printf("waypoints[ %d ].Easting is %f\n", i, waypoints[i].Easting);
	      //printf("waypoints[ %d ].Northing is %f\n", i, waypoints[i].Northing);
	      //###
	    }
	  cout << "done (read " << N << " points)." << endl;
	  
	  // generate and store whole path (sparse path, so max of 4 path points
	  // per corridor point)
	  cout << "Generating complete (sparse) path... "; cout.flush();
	  //  pathstruct path_whole_sparse = (pathstruct)Path_From_Corridor(corridor_whole);
	  pathstruct& path_whole_sparse = *(new pathstruct);
	  path_whole_sparse = Path_From_Corridor(corridor_whole);
	  path_whole_sparse.currentPoint = 0; // we haven't went anywhere yet, so we're still at the start of the path

	  /* create an 'rddf' which contains the path */
	  /* This is legacy herman code; may not be quite right */
	  RDDF& path_whole_sparse_rddf = *(new RDDF((char *) NULL));
	  RDDFData temp_point;
	  cout << (int)path_whole_sparse.numPoints << endl;
	  for (int i = 0; i < (int)path_whole_sparse.numPoints; i++)
	    {
	      temp_point.number = i;
	      temp_point.distFromStart = (i==0) ? 0 :
		  path_whole_sparse_rddf.getWaypointDistFromStart(i-1) +
		  hypot(path_whole_sparse.n[i] - path_whole_sparse.n[i-1],
		  path_whole_sparse.e[i] - path_whole_sparse.e[i-1]);
	      temp_point.Northing = path_whole_sparse.n[i];
	      temp_point.Easting = path_whole_sparse.e[i];
	      temp_point.offset = path_whole_sparse.corWidth[i];
	      temp_point.radius = path_whole_sparse.corWidth[i];
	      path_whole_sparse_rddf.addDataPoint(temp_point);

	      /* Get the speed from the waypoints */
	      /* No sure this is really right */
	      temp_point.maxSpeed = waypoints[i].maxSpeed;

	    }
	  cout << "done (sparse path has " << path_whole_sparse.numPoints
	       << " points, sparse path \'rddf\' has " << path_whole_sparse_rddf.getNumTargetPoints()
	       << " points)." << endl;

	  cout << "-----\n";
	  path_whole_sparse_rddf.print();
	  cout << "-----\n";
      
      // timber logging section (complete dense and sparse traj part)
      logging_enabled = getLoggingEnabled();
      if (logging_enabled)
	{
	  logging_location = getLogDir();
	  logging_level = getMyLoggingLevel();
 	  if (checkNewDirAndReset())
	    {
	      if (WRITE_COMPLETE_DENSE && logging_level > 0)
		{
		  string file_location = logging_location + (string)TRAJOUTPUT;
		  char* file = new char[256];
		  strcpy(file, file_location.c_str());
		  
		  cout << "Writing whole dense traj to file (may take a while; file"
		       << " is " << file_location << ")... ";
		  cout.flush();
		  
		  pathstruct path_whole_dense = DensifyPath(path_whole_sparse);
		  WritePathToFile(path_whole_dense, file);  // TRAJOUTPUT is defined in RddfPlanner.hh
		  delete [] file;
		  
		  cout << "done." << endl;
		}

	      if (WRITE_COMPLETE_SPARSE && logging_level > 0)
		{
		  string file_location = logging_location + (string)SPARSE_PATH_FILE;
		  char* file = new char[256];
		  strcpy(file, file_location.c_str());

		  cout << "Writing sparse path to file " << file_location << " ... "; cout.flush();
		  
		  WritePathToFile(path_whole_sparse, file);
		  cout << "done." << endl;
		}
	    }
	} // end of logging section
      
      // grab where we are and store it as location, so we only have to access astate
      // once per time through the loop
      vector<double> location(2);
      GetLocation(location);

      pathstruct path_chopped_dense = DensifyAndChop(path_whole_sparse, location,
						     CHOPBEHIND, CHOPAHEAD, -1);
      //pathstruct path_chopped_dense = path_whole_dense;  /**< send whole each time */

      
      StorePath(*ptraj, path_chopped_dense);

      if(SEND_TRAJ && !NOSKYNET)	  {
	if (C2 == 1) {
	  // Use Dima's function to make the output traj C2
	  // This also sets the speed 
	  refstage.MakeTrajC2(ptraj);
	} else if (C2 == 2)
	  ptraj->feasiblize(RDDF_MAXLATERAL, RDDF_MAXACCEL,
			    RDDF_MAXDECEL, CURVATURE_AVERAGING_DISTANCE);

#ifdef DEBUG
	cout << "----" << endl;
	ptraj->print();
	cout << "----" << endl;
#endif

	SendTraj(trajSocket, ptraj);
	// Also send to skynetgui
	if (!NOMM) SendTraj(skynetguiSocket, ptraj);
      }
      
      // timber logging section (chopped dense traj's)
      if (logging_enabled && logging_level >= 2)
	  {
	  	//### save the last sent chopped path
	  	char* buffer= new char[256];
	  	string file_location = logging_location + (string)CHOPPED_OUTPUT_BASE;
	  	strcpy(buffer, file_location.c_str());
 
	 	char* msg_num = new char[256];
	  	sprintf(msg_num, "_%04d", message_counter);
	  	strcat(buffer, msg_num);
 	  
	  	//open file and write it
	  	ofstream output_file;
	  	output_file.open(buffer, ios::trunc | ios::out);
	  	if(!output_file.is_open())
	    	cout << "\n ! Unable to open file " << (string)buffer << " , no data will be written!" << endl;
	  	else
	    	ptraj->print(output_file);
	  	output_file.close();

	  	delete [] msg_num;
	  	delete [] buffer;
	  }

      // Sleep until it is time to replan
      usleep(usleep_time);

      // Delete data structures that we are done with
      delete &path_whole_sparse;
      delete &path_whole_sparse_rddf;
      delete &waypoints;
    }
  delete ptraj;
}



void RddfPlanner::GetLocation(vector<double> & location)
{
  UpdateState(); 
  location[0] = m_state.Northing;
  location[1] = m_state.Easting;
}



void RddfPlanner::ReceiveDataThread()
{
  cout << "RddfPlanner is unable to receive data at this time." << endl;
}



int main(int argc, char** argv)
{
  // stick the whole thing in a try loop, so we can catch errors
  try {
    for (int i = 1; i < argc; ++i)
      {
	string arg(argv[i]);
      
	if (arg == "h" || arg == "-h" || arg == "--help" || arg == "help")
	  {
	    /* User has requested usage information. Print it to standard
	       output, and exit with exit code zero. */
	    cout << "| Command line options:\n"
		 << "| (you must type command line options just as they appear in this help\n"
		 << "| screen; do not preceed them with some arbitrary number of -'s.\n"
		 << "===============================================================\n"
		 << "  h,-h,help,--help    Prints this.\n"
		 << "  k, noskynet         Doesn't send any skynet messages.  We still need skynet\n"
		 << "                         to run, because we need state.\n"
		 << "  d, writedense       Writes the dense traj to a file.\n"
		 << "  s, nosparse         Skips writing the sparse traj to a file.\n"
		 << "  e, everytraj        Writes every traj sent over skynet to a file (named sequentially).\n"
		 << "  w, nowrite          Skips writing the dense and sparse traj to a file.\n"
		 << "  nw, nowait          Doesn't wait for the state buffer to fill (though it will still use\n"
		 << "                         state once it is filled).\n"
		 << "  nomm                Don't use ModeManModule.\n"
		 << "  c, c2               Make all traj's c2 by running them through an approximation\n"
		 << "                         function in the planner.\n"
		 << "  f, feasiblize       Run the trajs through the traj.feaziblize function to make\n"
		 << "                         the feedforward term smooth.\n"
		 << "  n, noc2             Do not perform any approximations.\n"
		 << "  d0                  Set debug level to 0 for timber operations.\n"
		 << "  d1                  Set debug level to 0 for timber operations.\n"
		 << "  d2                  Set debug level to 0 for timber operations.\n"
		 << "  \n"
		 << "| Additional info:\n"
		 << "==============================================================\n"
		 << "  \n"
		 << "  Users must explicitly specify either c2 (c) or noc2 (n) on the command line.  This is\n"
		 << "  required so that there isn't any confusion as to what kind of traj's we're sending.\n"
		 << "  C2 traj's will be smoother (continuous second derivative), but the actual path may\n"
		 << "  jump around.  Non-c2 traj's will not move around, but they have a second derivative\n"
		 << "  which is only piecewise continuous.\n"
		 << "  \n"
		 << "  Also, the options writedense and c2 are mutually exclusive, because the approximation\n"
		 << "  function currently used would never work for the whole corridor.\n"
		 << "  \n"
		 << "  Nothing will be written to any log file unless logging has been enabled via timber!\n"
		 << "  \n"
		 << "| Status:\n"
		 << "==============================================================\n"
		 << "  Current RDDF:  ";
	    cout.flush();
	    system("readlink ../../util/RDDF/rddf.dat");
	    cout << "Current SKYNET_KEY:  ";
	    cout.flush();
	    system("echo $SKYNET_KEY");
	    cout << endl;
	    return 0;
	  }
	else if (arg == "d" || arg == "writedense")
	  {
	    cout << "WRITING DENSE TRAJ TO FILE!" << endl;
	    WRITE_COMPLETE_DENSE = 1;
	  }
	else if (arg == "s" || arg == "nosparse")
	  {
	    cout << "NOT WRITING SPARSE TRAJ TO FILE!" << endl;
	    WRITE_COMPLETE_SPARSE = 0;
	  }
	else if (arg == "e" || arg == "everytraj")
	  {
	    cout << "WRITING EVERY TRAJ TO FILE!" << endl;
	    WRITE_EVERY_TRAJ = 1;
	  }
	else if (arg == "w" || arg == "nowrite")
	  {
	    cout << "NOT WRITING DENSE TRAJ TO FILE!" << endl;
	    cout << "NOT WRITING SPARSE TRAJ TO FILE!" << endl;
	    cout << "NOT WRITING EVERY TRAJ TO FILE!" << endl;
	    WRITE_COMPLETE_DENSE = 0;
	    WRITE_COMPLETE_SPARSE = 0;
	    WRITE_EVERY_TRAJ = 0;
	  }
	else if (arg == "nw" || arg == "nowait")
	  {
	    cout << "NOT WAITING FOR STATE TO FILL!" << endl;
	    WAIT_STATE = false;
	  }
	else if (arg == "k" || arg == "noskynet")
	  {
	    cout << "NOT SENDING SKYNET MESSAGES!" << endl;
	    NOSKYNET = 1;
	  }
	else if (arg == "nomm")
	  {
	    cout << "RUNNING WITHOUT MODEMANMODULE!" << endl;
	    NOMM = 1;
	  }
	else if (arg == "c" || arg == "c2")
	  {
	    cout << "TRAJ'S WILL BE C2 (THROUGH PLANNER FUNCTION)!" << endl;
	    C2 = 1;
	    C2SPECIFIED = 1;
	  }
	else if (arg == "f" || arg == "feasiblize")
	  {
	    cout << "TRAJ'S WILL BE C2 (THROUGH FEASIBLIZE)!" << endl;
	    C2 = 2;
	    C2SPECIFIED = 1;
	  }
	else if (arg == "n" || arg == "noc2")
	  {
	    cout << "TRAJ'S WILL NOT BE C2!" << endl;
	    C2 = 0;
	    C2SPECIFIED = 1;
	  }
	else if (arg == "d0")
	  {
	    cout << "TIMBER DEBUG LEVEL SET TO 0" << endl;
	    TIMBER_DEBUG_LEVEL = 0;
	  }
	else if (arg == "d1")
	  {
	    cout << "TIMBER DEBUG LEVEL SET TO 1" << endl;
	    TIMBER_DEBUG_LEVEL = 1;
	  }
	else if (arg == "d2")
	  {
	    cout << "TIMBER DEBUG LEVEL SET TO 2" << endl;
	    TIMBER_DEBUG_LEVEL = 2;
	  }
	else
	  cout << "UNKNOWN SWITCH: \"" << arg << "\"" << endl;
      }

    // Check the validity of the command line options.
    // two things must be checked... they must have specified either c2 or noc2, and
    // they can't specify c2 and writedense
    if (!C2SPECIFIED)
      {
	ostringstream oss;
	oss << __FILE__ << ", " <<__LINE__ << ": You must specify one of {c, c2, n, noc2, f, feasiblize} on"
	    << " the command line!";
	throw (oss.str());
      }
    if (C2 && WRITE_COMPLETE_DENSE)
      {
	ostringstream oss;
	oss << __FILE__ << ", " << __LINE__ << ": C2 and WRITE_COMPLETE_DENSE are incompatible options:"
	    << "  remove one of them!";
	throw (oss.str());
      }

    //Setup skynet
    int intSkynetKey = 0;

    char* ptrSkynetKey = getenv("SKYNET_KEY");
  
    if(ptrSkynetKey == NULL)
      {
	cout << "Unable to get skynet key!" << endl;
	return 1;
      }
    else
      {
	//cout << "Got to RddfPlanner int main" << endl;
	intSkynetKey = atoi(ptrSkynetKey);
	RddfPlanner RddfPlannerObj(intSkynetKey);

	//cout << "Entering ActiveLoop." << endl;
	RddfPlannerObj.ActiveLoop();
      }
    
  } // end of try {
  catch (std::string message)
    {
      cout << "Caught exception, error was \"" << message << "\"" << endl;
      return 1;
    }
  
  return 0;
}
