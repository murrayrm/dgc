#ifndef RNDF_HH_
#define RNDF_HH_
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <vector>
//using namespace std;

//namespace std //Doodle
//{
class doodles
{
public:
  doodles();
  int getKanoodles();
private:
  int kanoodles;
};
//}

namespace std
{


/*! GPSPoint class. The GPS point class represents a generic GPS point.
 *  Waypoints and perimeter points are subclasses of GPS points.
 *  All GPS points have a longitude and latitude. GPS points can be designated
 *  as entry entry points and/or exit points.
 *  All perimeter points has laneID = 0
 *  \brief The waypoint class represents a waypoint provided by the RNDF file.
 */
class GPSPoint
{
public:
/*! All GPS points have a longitude and latitude. GPS points can be designated
 *  as entry points and/or exit points. */
  GPSPoint(int segmentID, int laneID, int waypointID, double northing, double easting);
  virtual ~GPSPoint();
  
/*! Returns the segment ID of THIS. */
  int getSegmentID();
  
/*! Returns the lane ID of THIS. */
  int getLaneID();

/*! Returns the waypoint ID of THIS. */
  int getWaypointID();
  

/*! Returns the latitude of THIS.
  double getLatitude();
  
  ! Returns the longitude of THIS.
  double getLongitude();
*/
  
/*! Returns the northing of THIS. */
  double getNorthing();
  
/*! Returns the easting of THIS. */
  double getEasting();
  
/*! Returns the vector of pointers to entry GPS points. */
  vector<GPSPoint*> getEntryPoints();
  
/*! Sets THIS as an entry GPS point. */
  void setEntry();
  
/*! Sets THIS as an exit GPS point and adds entryGPSPoint to the array
 *  entryPoints. */
  void setExit(GPSPoint* entryGPSPoint);
  
/*! Sets the waypointID of THIS. */
  void setWaypointID(int);
  
/*! Returns TRUE iff THIS is an entry GPS point. */
  bool isEntry();
  
/*! Returns TRUE iff THIS is an exit GPS point. */
  bool isExit();

/*! Prints the segmentID, laneID, and waypointID of THIS. */
  void print();
  
protected:
/*  Segments are identified using the form 'M'. The Nth lane of segment M has
 *  identifier 'M.N'. The waypoints of each lane are similarly identified 
 *  such that the Pth waypoint of lane M.N is 'M.N.P'.*/
  int segmentID, laneID, waypointID;

/*  GPS latitude and logitude fields are floats with six decimal places 
 *  and represent points in the WGS84 coordinate system.
  double latitude, longitude;
*/
  double northing, easting;
  
/*  GPS points can be designated as an entry and/or  an exit point. */
  bool entry, exit;
  
/*  Exit GPS points have related entry GPS points. */
  vector<GPSPoint*> entryPoints;
};



// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

/*! Waypoint class. The Waypoint class represents a waypoint provided by the
 * RNDF file. All waypoints have a segment ID, lane ID, waypoint ID, longitude,
 * and latitude. Waypoints are designated as a checkpoint, entry waypoint,
 * exit waypoint, or a stop sign.
 * \brief The waypoint class represents a waypoint provided by the RNDF file.
 */
class Waypoint : public GPSPoint
{
public:
/*! All waypoints have a segment ID, lane ID, waypoint ID, longitude, and
 * latitude. Initially, each waypoint is not designated as a checkpoint,
 * entry waypoint, exit waypoint, nor a stop sign. */
	Waypoint(int segmentID, int laneID, int waypointID,
    double northing, double easting);
	virtual ~Waypoint();

/*! If this waypoint is also a checkpoint, returns the checkpoint ID. */
  int getCheckpointID();
  
/*! Sets THIS as a checkpoint and sets the checkpoint ID. */
  void setCheckpointID(int checkpointID);
  
/*! Sets THIS as a stop sign. */
  void setStopSign();
  
/*! Returns TRUE iff THIS is a checkpoint. */
  bool isCheckpoint();
  
/*! Returns TRUE iff THIS is a stop sign. */
  bool isStopSign();
  
protected:
  int checkpointID;
  
/*! Waypoints can be designated as a checkpoint, stop sign, entry waypoint, 
 *  or exit waypoint. */  
  bool checkpoint, stopSign;
};



// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

/*! PerimeterPoint class. The PerimeterPoint class represents a perimeterpoint
 * provided by the RNDF file. All perimeterpoints have a zone ID perimeterpoint
 * ID, longitude, and latitude. Perimeterpoints are designated as an entry 
 * perimeterpoint, exit perimeterpoint, or neither.
 * \brief The perimeterpoint class represents a perimeterpoint provided by the RNDF.
 */
class PerimeterPoint: public GPSPoint
{
public:
/*! All perimeterpoints have a zone ID, perimeterpoint ID, longitude, and
 *  latitude. Initially, each perimeterpoint is not designated as a entry
 *  perimeterpoint nor as an exit perimeterpoint. All perimeterpoints have
 *  a "laneID" equal to 0. */
	PerimeterPoint(int zoneID, int zero, int perimeterPointID, 
    double northing, double easting);
	virtual ~PerimeterPoint();

/*! Returns the zone ID of THIS. */
  int getZoneID();

/*! Returns the perimeterpoint ID of THIS. */
  int getPerimeterPointID();
  
private:
/*  Zones are identified using the form 'M'. The perimeterpoints of each zone
 *  are identified such that the Pth perimeterpoint of zone M is 'M.0.P'.*/
  int zoneID, perimeterPointID;
};



// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

/*! ParkingSpot class. The ParkingSpot class represents a parking spot provided
 *  by the RNDF file. All parking spots have a zone ID, spotID, and two waypoints.
 *  Parking spots may have an optional spot width.
 * \brief The ParkingSpot class represents a parking spot provided by the RNDF.
 */
class ParkingSpot
{
public:
/*! All parking spots have a zoneID or spotID. */
	ParkingSpot(int zoneID, int spotID);
	virtual ~ParkingSpot();
  
/*! Sets the spot width of THIS. */
  void setSpotWidth(int spotWidth);
  
/*! Sets waypointID of THIS to WAYPOINT. */
  void setWaypoint(int waypointID, Waypoint* waypoint);
  
/*! Returns the zoneID of THIS. */
  int getZoneID();
  
/*! Returns the spotID of THIS. */
  int getSpotID();
  
/*! Returns the spot width of THIS. */
  int getSpotWidth();
  
/*! Returns a pointer to a Waypoint with the waypointID passed in as an argument. */
  Waypoint* getWaypoint(int waypointID);
  
private:
  int zoneID, spotID, spotWidth;
  Waypoint* waypoint1;
  Waypoint* waypoint2;
};



// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

/*! Lane class. The Lane class represents a lane provided by the RNDF file.
 * All lanes have a segment ID, lane ID, and number of waypoints. Lanes may
 * have optional information including lane width and boundaries.
 * \brief The Lane class represents a lane provided by the RNDF file.
 */
class Lane
{
public:
/*! All lanes have a segment ID, lane ID, and number of waypoints. */
	Lane(int, int, int);
	virtual ~Lane();

/*! Returns the segment ID of THIS. */
  int getSegmentID();
  
/*! Returns the lane ID of THIS. */
  int getLaneID();
  
/*! Returns the number of waypoints contained in THIS. */
  int getNumOfWaypoints();
  
/*! Returns the lane width of THIS. */
  int getLaneWidth();
  
/*! Returns the left boundary of THIS. */
  string getLeftBoundary();
  
/*! Returns the right boundary of THIS. */
  string getRightBoundary();

/*! Returns the direction of THIS */
  int getDirection();
  
/*! Sets the lane width of THIS. */
  void setLaneWidth(int);
  
/*! Sets the left boundary of THIS. */
  void setLeftBoundary(string);
  
/*! Sets the right boundary of THIS. */
  void setRightBoundary(string);
  
/*! Sets the number of waypoints of THIS. */
  void setNumOfWaypoints(int);

/*! Sets the direction of THIS */
  void setDirection(int);
  
/*! Returns a pointer to a Waypoint with the waypoint ID passed in as an
 *  argument. */
  Waypoint* getWaypoint(int);
  
/*! Adds a waypoint to the array of waypoints contained in THIS. */
  void addWaypoint(Waypoint*);
  
private:
  int segmentID, laneID, numOfWaypoints, laneWidth;
  int direction;
  string leftBoundary, rightBoundary;
  
/*! Each lane contains an array of waypoints of size numOfWaypoints. */
  vector<Waypoint*> waypoints;
};



// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

/*! Segment class. The Segment class represents a segment provided by the RNDF
 *  file. Each segment is characterized by its ID, the number of lanes in it,
 *  and optionally a segment name.
 * \brief The segment class represents a segment provided by the RNDF file.
 */
class Segment
{
public:
/*! Each segment is characterized by its ID and the number of lanes in it. */
	Segment(int, int);
  virtual ~Segment();
  
/*! Returns the segment ID of THIS. */
  int getSegmentID();
  
/*! Returns the number of lanes that THIS contains. */
  int getNumOfLanes();

/*! Returns the segment name of THIS. */
  string getSegmentName();
  
/*! Sets the segment name of THIS. */
  void setSegmentName(string);
  
/*! Returns the minimum speed of THIS. */
  int getMinSpeed();
  
/*! Returns the maximum speed of THIS. */
  int getMaxSpeed();
  
/*! Returns the minimum and maximum speed limit of THIS. */
  void setSpeedLimits(int, int);
  
/*! Returns a pointer to a Lane with the lane ID passed in as an argument. */
  Lane* getLane(int);
  
/*! Adds a lane to the array of lanes contained in THIS. */
  void addLane(Lane*);
  
private:
/* Segments are identified using the form 'M'. All segments have a finite 
 * number of lanes in them. */
  int segmentID, numOfLanes;
  
/* Optionally, a segment may also have a segment name or street name. */
  string segmentName;
  
/* Each segment contains an array of lanes of size numOfLanes. */
  vector<Lane*> lanes;
  
/* Each segment has a minimum and maximum speed limit. */
  int minSpeed, maxSpeed;
};



// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

/*! Zone class. The Zone class represents a zone provided by the RNDF file.
 * All zones have a zone ID, a fixed number of parking spots, and a minimum
 * and maximum speed limit. Zones have a fixed perimeter characterized by
 * a fixed number of perimeter points. Zones may have an optional name.
 * \brief The Zone class represents a zone provided by the RNDF file.
 */
class Zone
{
public:
/*! All Zones have a zone ID and a fixed number of parking spots. */
	Zone(int zoneID, int numOfSpots);
	virtual ~Zone();

/*! Sets the zone name of THIS. */
  void setZoneName(string zoneName);
  
/*! Sets the minimum and maximum speed limit of THIS. */
  void setSpeedLimits(int minSpeed, int maxSpeed);
  
/*! Adds a perimeter point to the array of perimeter points contained in THIS. */
  void addPerimeterPoint(PerimeterPoint* perimeterPoint);
  
/*! Adds a parking spot to the array of parking spots contained in THIS. */
  void addParkingSpot(ParkingSpot* parkingSpot);

/*! Returns the zone ID of THIS. */
  int getZoneID();
  
/*! Returns the number of parking spots contained in THIS. */
  int getNumOfSpots();
  
/*! Returns the number of perimeter points contained in THIS. */
  int getNumOfPerimeterPoints();
  
/*! Returns the zone name of THIS. */
  string getZoneName();
  
/*! Returns the minimum speed of THIS. */
  int getMinSpeed();
  
/*! Returns the maximum speed of THIS. */
  int getMaxSpeed();
 
/*! Returns a pointer to a perimeter point with the perimeterPointID passed in
 *  as an argument. */
  PerimeterPoint* getPerimeterPoint(int perimeterPointID);
  
/*! Returns a pointer to a parking spot with the spotID passed in as an argument. */
  ParkingSpot* getParkingSpot(int spotID);
  
private:
/* Zones are identified with an integer > 0. All zones have a finite 
 * number of parkings spots in them. Zones also have a minimum and
 * maximum speed allowed. */
  int zoneID, numOfSpots, minSpeed, maxSpeed;
  
/* Optionally, a zone may also have a zone name. */
  string zoneName;
  
/* Each zone contains a zone perimeter. */
  vector<PerimeterPoint*> perimeter;
  
/* Each zone contains an array of parking spots of size numOfSpots. */
  vector<ParkingSpot*> spots;
};




// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
/*! RNDF class. The RNDF class represents a RNDF and MDF, route network
 * definition file and Mission Data File provided by DARPA. RNDF and MDF 
 * files are loaded using the loadFile(char* fileName) method. Segments and 
 * Zones are contained in the RNDF class. 
 * \brief The RNDF class represents a RNDF and MDF provided by DARPA.
 */
class RNDF
{
  public:
/*! All RNDFs initially contain no information. */
    RNDF();
  	virtual ~RNDF();

/*! Returns a pointer to a Segment with the segmentID passed in as an argument. */
    Segment* getSegment(int segmentID);
    
/*! Returns a pointer to a Lane with the segmentID and laneID passed in as 
 * arguments. */    
    Lane* getLane(int segmentID, int laneID);
    
/*! Returns a pointer to a Waypoint with the segmentOrZoneID, laneID and
 *  waypointID passed in as arguments. */  
    Waypoint* getWaypoint(int segmentOrZoneID, int laneID, int waypointID);
    
/*! Returns a pointer to a Zone with the zoneID passed in as an argument. */
    Zone* getZone(int zoneID);
    
/*! Returns a pointer to a PerimeterPoint with the zoneID and perimeterPointID
 *  passed in as arguments. */
    PerimeterPoint* getPerimeterPoint(int zoneID, int perimeterPointID);
    
/*! Returns a pointer to a ParkingSpot with the zoneID and spotID passed in as 
 *  arguments. */
    ParkingSpot* getSpot(int zoneID, int spotID);
    
/*! Returns the number of segments contained in THIS. */
    int getNumOfSegments();
    
/*! Returns the number of zones contained in THIS. */
    int getNumOfZones();

/*! Get the waypoint with the specified checkpointID */
    Waypoint* getCheckpoint(int);
        
/*! Insert a waypoint right before the given waypoint (waypointID). */
    void addExtraWaypoint(int, int, int, double, double);

/*! Loads a RNDF or MDF. */
    bool loadFile(char* fileName);

/* Assigns lane direction */
    void assignLaneDirection();
    
  private:
    int numOfSegments, numOfZones;
    vector<Segment*> segments;
    vector<Zone*> zones;
    vector<Waypoint*> ckpts;           // The vector of waypoints that are checkpoints
    
    void parseSegment(ifstream*);
    void parseLane(ifstream*, int);
    void parseWaypoint(ifstream*, int, int);
    void parseZone(ifstream*);
    void parsePerimeter(ifstream*, int);
    void parseSpot(ifstream*, int);
    void parseExit(ifstream*);
    void parseCheckpoint(ifstream*);
    void parseSpeedLimit(ifstream*);
    
    void setSegmentsAndZones(ifstream*);
    
    void addSegment(int, int);
    void setSegmentName(int, string);
    void setSpeedLimits(int, int, int);
    
    void addLane(int, int, int);
    void setLaneWidth(int, int, int);
    void setLeftBoundary(int, int, string);
    void setRightBoundary(int, int, string);
    
    void addWaypoint(int, int, int, double, double);
    void setCheckpoint(int, int, int, int);
    void setStopSign(int, int, int);
    void setExit(int, int, int, int, int, int);
    
    void addZone(int, int);
    void setZoneName(int, string);
    void addPerimeterPoint(int, int, double, double);
    
    void addSpot(int, int);
    void setSpotWidth(int, int, int);
    void addSpotWaypoint(int, int, int, double, double);
};

}

#endif /*RNDF_HH_*/


