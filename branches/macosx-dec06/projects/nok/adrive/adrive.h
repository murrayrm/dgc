/*!**
 *  Adrive mutex version header.  
 *  This defines the structures for adrive 
 *  Including vehicle, ports, actuators, and errors.  
 *  ALso all constants are in here.  
 *  TUlly Foote
 *  12/28/04
 */



#ifndef _ADRIVE_MUTEX
#define _ADRIVE_MUTEX
#include <iostream>
#include <fstream>
#include <pthread.h>
#include <sstream>

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <fstream>
#include <sys/time.h>
#include <time.h>
#include <errno.h>

#include "DGCutils"
#include "parker_steer.h"
#include "sn_msg.hh"
#include "actuators.h"
#include "brake.h"
#include "throttle.h"

#include "OBDIIDriver.hpp"
#include "ATimberBox.hh"



/******  COnstants for adrive componants *******/

/* Define status values. */
#define ON 1
#define OFF 0
#define ERROR -1
#define ENABLED 1
#define DISABLED 0
#define ERROR_HAPPENED 5
#define NO_ERROR true
#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

//Values use usleep
#define DARPA_PAUSE_TIMEOUT 5000000
#define LOGGING_DELAY 10000  //100Hz
#define ACTUATOR_COMMAND_SLEEP_TIME 100000  //10Hz
#define DEFAULT_COMMAND_TIMEOUT 1 // 1 second
#define MAX_COMMAND_WAIT 1  // One second
#define SUPERVISORY_SLEEP_LENGTH 1000000 //Needs to be longer than STATUS_SLEEP_LENGTH 
#define SUPERCON_DEFAULT_TIMEOUT 1000000 //How long to wait between supervisory commands
#define STATUS_SLEEP_LENGTH 250000 /* 4Hz 
				    * THIS IS NOW ONLY THE DEFAULT
				    * IT IS SET IN adrive.config 
				    * and adjustable in sparrow
				    * 
				    * I've set this to 4hz for now.  THe goal is 
				    * 10 Hz.  
				    * We've had problems upping this, I thimnk 
				    * that we might have satureated the serial 
				    * port.  
				    *
				    * Previous changes
				    * Set at 1 Hz right now.  
				    * 10 Hz made things terrible at S.A.
				    * I've tested it at 20Hz in the shop
				    * However I do not have any way to test for 
				    * message/packet loss.  So I'm going to back  
				    * off from that tested value of 50000us of sleep. */
#define ACTUATOR_STATE_BROADCAST_INTERVAL 30000 // send the actuator state at ~30Hz
#define ENGINE_ON_RPM 600 // The RPM at which the engine is considered running
#define COLD_START_TEMP 140 // The temperature below which a wait for the glow plugs is required
#define COLD_START_WAIT_TIME 5  // The number of seconds to wait before starting for a cold start
#define BLIND_STARTER_MOTOR_TIME 4 // The number of seconds to crank if we dont' have OBDII
#define BLIND_SHIFTING_WAIT_TIME 10 // Number of seconds to wait before shifting if OBDII is invalid
#define UNKNOWN_ENGINE_CONDITION_RESTART_TIMEOUT 300  // Number of seconds between retries of starting engine if OBDII is off.  
#define DISABLE_TIME_UNTIL_ENGINE_OFF 5 // How long to sleep before turning off the engine after entering disable.  
#define BRAKE_PAUSE_POSITION .7  // How strongly the brake will turn on when put into pause.  
#define SPEED_STOPPED_THRESHOLD .1 //Below what speed we are considered stopped.  
#define OBDII_ERROR_TIMEOUT 200000  // usec to sleep if OBDII fails so we don't loop superfasts

typedef int error_t;
typedef int (*actuator_command_function)(double position);
typedef int (*actuator_status_function)();
typedef void (*actuator_init_function)();
typedef int (*actuator_obdii_command_function)(double command);
typedef int (*actuator_obdii_status_function)();
typedef void (*actuator_obdii_init_function)();
typedef int (*actuator_estop_command_function)(double command);
typedef int (*actuator_estop_status_function)();
typedef void (*actuator_estop_init_function)();



/*! An enumeration of all the actuators associated with adrive. */
enum ACTUATOR_TYPE {
  ACTUATOR_BRAKE, ACTUATOR_GAS, ACTUATOR_STEER, NUM_ACTUATORS
};

/*! An enumeration of estop states. */
enum ESTOP_TYPES {
  ESTOP_DISABLE, ESTOP_PAUSE, ESTOP_RUN
};

/*! An enumeration of engine conditions */
enum ENGINE_CONDITIONS {
  ENGINE_OFF,     // Engine is switched off
  ENGINE_STOPPED, // Engine is in run but has stalled or hasn't started
  ENGINE_RUNNING, // THis is nominal
  ENGINE_UNKNOWN  // OBDII isn't reporting we don't know the status of the engine
};


/*! The generic actuator type.  
 *  Valid for throttle, brake, steering, and transmission 3/10/05. 
 * It has a little extra unused data, but optimization has not been an issue. */
struct actuator_t
{
  pthread_mutex_t mutex;
  
  pthread_once_t start_bit;
  pthread_cond_t cond_flag;
  
  /*! The port number of the actuator. */
  int port;
  /*! A boolean of whether the actuator status thread is enabled. */
  int status_enabled;
  /* HOw long the status loop sleeps in microseconds*/
  int status_sleep_length;
  /* HOw long the command loop sleeps in microseconds*/
  int command_sleep_length;
  /* How long before the command loop times out in seconds*/
  int command_timeout;
  /*! A boolean of whether the actuator status thread is enabled. */
  int command_enabled;
  /*! A counter updated by the status loop*/
  int status_loop_counter;
  /*! The time at which the actuator was last updated */
  unsigned long long update_time;
  /*! A counter updated by the command loop*/
  int command_loop_counter;
  /*! A boolean current status of the actuator */
  int status;
  /*! The current commanded position.  Values dependant on actuator. */
  double command;
  /*! The current actuator position.  Values dependant on actuator. */
  double position;
  /* The actuator velocity */
  double velocity;
  /* The actuator acceleration */
  double acceleration;
  /*! The current actuator independant feedback.  Values dependant on actuator. */
  double pressure;
  /*! The actuator error struct */
  error_t error;
  /*! The name of the actutor, unused*/
  char name[100];
  /*! The address of the initialization function */
  actuator_init_function execute_init;
  /*! The address of the command function */
  actuator_command_function execute_command;
  /*! The address of the status update function */
  actuator_status_function execute_status;
  /*! The pthread handle for the command thread. */
  pthread_t pthread_command_handle;
  /*! The pthread handle for the status thread. */
  pthread_t pthread_status_handle;
}; 

struct estop_t
{
  pthread_mutex_t mutex;
  
  pthread_once_t start_bit;
  pthread_cond_t cond_flag;
  
  /*! The port number of the actuator. */
  int port;
  /*! A boolean of whether the actuator status thread is enabled. */
  int status_enabled;
  /*! A boolean of whether the actuator status thread is enabled. */
  int command_enabled;
  /*! An unused counter */
  int status_loop_counter;
  /*! An unused counter */
  int command_loop_counter;
  /*! The time at which the actuator was last updated */
  unsigned long long update_time;
  /*! A boolean current status of the actuator */
  int status;
  /* HOw long the status loop sleeps */
  int status_sleep_length;
  /* HOw long the command loop sleeps */
  int command_sleep_length;
  /* How long before the command loop times out in seconds*/
  int command_timeout;
  /*! The current commanded position.  Values dependant on actuator. */
  int command;
  /*! The current actuator position.  Values dependant on actuator. */
  int position;
  /* The actuator disable positon */
  int astop;
  /* The reason for setting astop to EPAUSE */
  int astop_err;
  /* the darpa command */
  int dstop;
  /* Warning that we're about to go to run */
  int about_to_unpause;

  /*! The actuator error struct */
  error_t error;
  /*! The name of the actutor, unused*/
  char name[100];
  /*! The address of the initialization function */
  actuator_init_function execute_init;
  /*! The address of the command function */
  actuator_command_function execute_command;
  /*! The address of the status update function */
  actuator_status_function execute_status;
  /*! The pthread handle for the command thread. */
  pthread_t pthread_command_handle;
  /*! The pthread handle for the status thread. */
  pthread_t pthread_status_handle;
}; 

struct obdii_t 
{
  pthread_mutex_t mutex;
  
  pthread_once_t start_bit;
  pthread_cond_t cond_flag;

  /*! The port number of the actuator. */
  int port;
  /*! A boolean of whether the actuator status thread is enabled. */
  int status_enabled;
  /*! A boolean of whether the actuator status thread is enabled. */
  int command_enabled;
  /*! A counter for the adrive keep alive monitering*/
  int status_loop_counter;
  /*! The number of times obdii speed is polled before other fields*/
  int speed_priority;
  /* A counter that determines what type of OBDII information is polled.
   * Usually OBDII only polls for speed, except every speed_priority
   *  polls when it fills in more data such as trans position.  */
  int poll_type_counter;
  /*! An unused counter */
  int command_loop_counter;
  /*! The time at which the actuator was last updated */
  unsigned long long update_time;
  /*! A boolean current status of the actuator */
  int status;
  /* HOw long the status loop sleeps */
  int status_sleep_length;
  /* HOw long the command loop sleeps */
  int command_sleep_length;
  /* How long before the command loop times out in seconds*/
  int command_timeout;
  /* The actuator's name */
  char name[100];


  /*! The class that contains all the function calls. */
  OBDIIDriver my_obdii_driver;
  /*! The RPM of the engine */
  double engineRPM;
  /*! The time since the engine started in seconds */
  int TimeSinceEngineStart;
  /*! Vehicle speed m/s */
  double VehicleWheelSpeed;
  /*! The temperature of the engine coolant. */
  double EngineCoolantTemp;
  /*! The engine torque in foot pounds */
  double WheelForce;
  /*! The time the glow plug has been on??? in seconds */
  int GlowPlugLampTime;
  /*! The position of the accelerator petal UNITS?? */
  double ThrottlePosition;
  /*! The current gear ratio */
  double CurrentGearRatio;
  /*! Battery Voltate */
  double BatteryVoltage;
  /*! The address of the initialization function */
  actuator_obdii_init_function  execute_init;
  /*! The address of the command function */
  actuator_obdii_command_function execute_command;
  /*! The address of the status update function */
  actuator_obdii_status_function execute_status;
  /*! The pthread handle for the status thread. */
  pthread_t pthread_status_handle;
};

struct trans_t
{
  pthread_mutex_t mutex;
  
  pthread_once_t start_bit;
  pthread_cond_t cond_flag;
  
  /*! The port number of the actuator. */
  int port;
  /*! A boolean of whether the actuator status thread is enabled. */
  int status_enabled;
  /*! A boolean of whether the actuator status thread is enabled. */
  int command_enabled;
  /*! An unused counter */
  int status_loop_counter;
  /*! An unused counter */
  int command_loop_counter;
  /*! The time at which the actuator was last updated */
  unsigned long long update_time;
  /*! A boolean current status of the actuator */
  int status;
  /* HOw long the status loop sleeps */
  int status_sleep_length;
  /* HOw long the command loop sleeps */
  int command_sleep_length;
  /* How long before the command loop times out in seconds*/
  int command_timeout;
  /*! The current commanded position.  Values dependant on actuator. */
  int command;
  /*! The current actuator position.  Values dependant on actuator. */
  int position;
  /* The ignition command */
  int ignition_command;
  /* The ignition position */
  int ignition_position;
  /* Position and commands for leds */
  int led1_command;
  int led2_command;
  int led3_command;
  int led4_command;
  int led1_position;
  int led2_position;
  int led3_position;
  int led4_position;

  /*! The actuator error struct */
  error_t error;
  /*! The name of the actutor, unused*/
  char name[100];
  /*! The address of the initialization function */
  actuator_init_function execute_init;
  /*! The address of the command function */
  actuator_command_function execute_command;
  /*! The address of the status update function */
  actuator_status_function execute_status;
  /*! The pthread handle for the command thread. */
  pthread_t pthread_command_handle;
  /*! The pthread handle for the status thread. */
  pthread_t pthread_status_handle;
}; 



/** The vehicle struct.  This contains all the adrive data. */
struct vehicle_t {
  struct actuator_t actuator[NUM_ACTUATORS];
  struct trans_t actuator_trans;
  struct obdii_t actuator_obdii;
  struct estop_t actuator_estop;
  //THe delay for the supervisory thread
  int supervisory_delay;
  // The loop counter for the supervisory thread. 
  int super_count;
  // The skynet key that adrive is to communicate on.
  int skynet_key;
  // Whether or not adrive will automatically pause if it hasn't recieved a command
  // before the timeout value for each command
  int protective_interlocks;
  // A flag for whether adrive will automatically start and stop timber
  int automatic_timber_logging;
  // A flag that in theory will shutdown adrive cleanly when set
  int shutdown;
  //A pointer to an instance of AtimberBox which sends commands to timber
  // to start and stop logging. 
  AtimberBox * pAtimberBox;
  // Whether or not to initialize superCon 
  int run_superCon;
  // how long between supercon messages to timeout
  int superCon_timeout;
  // Whether to use the ignition actuator
  int enable_ignition;
  // A status flag to pass back to the gui
  int gui_status;
};

/* A function to initialize the vehicle struct */
// moved to askynet_monitor.hh
//void init_vehicle_struct(vehicle_t & new_vehicle);

/* the global vehicle*/
extern struct vehicle_t my_vehicle;

// Global mutexes and conds
/* The mutex conditional that allows the simple steering calibration hack */
extern pthread_cond_t steer_calibrated_flag;


/*! A function to update the overall estop position based on 
 * the current inputs */
int updateEstopPosition(vehicle_t *pVehicle);

void* logging_main (void* arg);
void read_config_file(vehicle_t & my_vehicle);
void* sparrow_main (void* arg);
void* actuator_status_thread_function(void* arg);
void* actuator_command_thread_function(void* arg);
void* actuator_obdii_status_thread_function(void * arg);
void* actuator_estop_status_thread_function(void * arg);
void* actuator_trans_status_thread_function(void * arg);
void* actuator_trans_command_thread_function(void * arg);
void* supervisory_thread (void* arg);

int execute_adrive_pause(int error_type);
int remove_adrive_pause();
int set_pause();
int set_disable();
double pause_speed();

#endif // __ADRIVE_H__
	  
