/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */


#ifndef MISSIONPLANNER_HH
#define MISSIONPLANNER_HH

#include <pthread.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <list>
#include <math.h>
#include "StateClient.h"
#include "estop.h"
#include "DGCutils"
#include "sparrowhawk.hh"
#include "RNDF.hh"
#include "Obstacle.hh"
#include "Graph.hh"
#include "missionUtils.hh"
#include "SegGoalsTalker.hh"
#include "GloNavMapTalker.hh"
#include "GlobalConstants.h"
#define LEAST_NUM_SEGGOALS_STORED 8
#define LEAST_NUM_SEGGOALS_TPLANNER_STORED 3
#define NUM_SEGGOALS_DISPLAYED 6


/**
 * MissionPlanner class.
 */
class CMissionPlanner : public CStateClient, public CGloNavMapTalker, public CSegGoalsTalker
{
  int m_snKey;                     // Should be set in MissionPlannerMain and never changed.
  bool m_nosparrow;                // Should be set in MissionPlannerMain and never changed.
  int m_usingGloNavMapLib;		   // Should be set in MissionPlannerMain and never changed.

  RNDF* m_rndf;
  int m_rndfRevNum;                // RNDF revision#
  list<Edge*> m_edgesRemoved;     // All the edges that have been removed from the RNDF graph

  vector<Waypoint*> m_checkpointSequence;
  int* m_checkpointGoalID;  // m_checkpointGoalID[i] is the goalID for crossing m_checkpointSequence[i]
  int m_nextCheckpointIndex;

  Graph* m_rndfGraph;

  list<SegGoals> m_segGoalsSeq;

  list<SegGoals> m_executingGoals;  // The goals that are currently stored in the traffic planner. 
                                    // The first element of this list is the goal that I believe the traffic planner is executing.

  int m_nextGoalID;                 // The ID of the next goal to be added to m_segGoalsSeq

  SegGoalsStatus m_segGoalsStatus;  // TPlanner status used in the planning loop to determine if we need to replan
  SegGoalsStatus* m_receivedSegGoalsStatus;  // Actual TPlanner status

  vector<Obstacle*> m_obstacles;     // The list of all the obstacles.


  // Whether at least one RNDF object was received
  bool m_bReceivedAtLeastOneGloNavMap;
  pthread_mutex_t m_GloNavMapReceivedMutex;
  pthread_cond_t m_GloNavMapReceivedCond;


  // Whether traffic planner starts listening
  bool m_bReceivedAtLeastOneSegGoalsStatus;
  pthread_mutex_t m_FirstSegGoalsStatusReceivedMutex;
  pthread_cond_t m_FirstSegGoalsStatusReceivedCond;

  // The mutex to protect the RNDF we're planning on
  pthread_mutex_t m_GloNavMapMutex;

  // The mutex to protect the segGoalsSeq
  pthread_mutex_t m_SegGoalsSeqMutex;

  // The mutex to protect the status of segGoals
  pthread_mutex_t m_SegGoalsStatusMutex;

  // The mutex to protect the index of the next checkpoint
  pthread_mutex_t m_NextCheckpointIndexMutex;

  // The mutex to protect the list of executing goals
  pthread_mutex_t m_ExecGoalsMutex;

  // The mutex to protect the list of obstacles
  pthread_mutex_t m_ObstaclesMutex;

  // The skynet socket for sending segment-level goals to tplanner
  int m_segGoalsSocket;
  // The skynet socket for communication with Global Navigation Map to request the RNDF object
  int m_GloNavMapRequestSocket;
  // The skynet socket for communication with Global Navigation Map to get the RNDF object
  int m_GloNavMapSocket;

  // SparrowHawk display
  CSparrowHawk *shp;

  // Sparrow variables
  int sparrowNextGoalIDs[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextSegments1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextLanes1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextWaypoints1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextSegments2[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextLanes2[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextWaypoints2[NUM_SEGGOALS_DISPLAYED];
  double sparrowMinSpeed, sparrowMaxSpeed;
  bool sparrowIllegalPassingAllowed, sparrowStopAtExit, sparrowIsExitCheckpoint;
  int sparrowNextCheckpointID, sparrowNextCheckpointGoalID;
  int sparrowNextCheckpointSegmentID, sparrowNextCheckpointLaneID, sparrowNextCheckpointWaypointID;

  // Obstacle inserted by sparrow
  int obstacleID;
  int obstacleSegmentID;
  int obstacleLaneID;
  int obstacleWaypointID;


public:
  /*! Contstructor */
  CMissionPlanner(int skynetKey, bool bWaitForStateFill, bool nosparrow, char* RNDFFileName, char* MDFFileName, bool usingGloNavMapLib);
  /*! Standard destructor */
  ~CMissionPlanner();

  /*! Method to run in separate thread.  This function runs the dd_loop() 
   *  Sparrow method to update the screen and take keyboard commands. */
  void SparrowDisplayLoop();

  /*! Method to update the variables that are to be displayed in the Sparrow
   *  interface. */
  void UpdateSparrowVariablesLoop();

  void sparrowInsertObstacle();

  /*! This is the function that continually runs the planner in a loop */
  void MPlanningLoop(void);

  /*! this is the function that continually reads tplanner status and update the executing goalID*/
  void getTPlannerStatusThread();

  /*! this is the function that continually transmit segment goals to tplanner and  update the 
      executing goalID*/
  void sendSegGoalsThread();

  /*! This function is used to request the full map from World Map */
  void requestGloNavMap();

private:
  void initializeSegGoals();
  vector<SegGoals> planFromCurrentPosition(GPSPoint*, int);
  vector<SegGoals> planNextMission(int);
  vector<SegGoals> addUturn(GPSPoint*, GPSPoint*, Vertex*);
  void addEndOfMission(vector<SegGoals>&);
  void resetGoals();
  int getObstacleIndex(int);
  vector<Obstacle*> getAllObstaclesOnSegment(int);
  bool loadMDFFile(char*, RNDF*);
  void parseCheckpoint(ifstream*, RNDF*);
  void parseSpeedLimit(ifstream*, RNDF*);
  void setSpeedLimits(int, double, double, RNDF*);
  void printMissionOnSparrow(list<SegGoals> segGoals, int startIndex);
};

#endif  // MISSIONPLANNER_HH
