/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#ifndef VERTEX_HH_
#define VERTEX_HH_
#define INFINITE_COST_TO_COME 1.0E9f
#include "Edge.hh"
#include <vector>
#include <iostream>
using namespace std;

/*! Vertex class. Represents a vertex of a graph. A vertex contains a
 *  segmentID, laneID, and waypointID. A vertex may contain one or more
 *  edges.
 * \brief The Vertex class used in the Graph class.
 */
class Vertex
{
public:
/*! All vertices have a segmentID, laneID, and waypointID. */
	Vertex(int segmentID, int laneID, int waypointID);
	virtual ~Vertex();
  
/*! Returns the segmentID of THIS. */
  int getSegmentID();
  
/*! Returns the laneID of THIS. */
  int getLaneID();
  
/*! Returns the waypointID of THIS. */
  int getWaypointID();
  
/*! Returns the vector of edges contained in THIS. */
  vector<Edge*> getEdges();
  
/*! Adds a pointer to an edge to the vector of edges. */
  void addEdge(Edge* edge);

/*! Removes a pointer an edge from the vector of edges. */
  bool removeEdge(Edge* edge);

/*! For graph search, set the cost to come to THIS. */
  void setCostToCome(float cost);

/*! For graph search, get the cost to come to THIS. */ 
  float getCostToCome();

/*! For graph search, set the vertex that is to be visited before THIS. */
  void setPreviousVertex(Vertex* previous);

/*! For graph search, get the vertex that is to be visited before THIS. */
  Vertex* getPreviousVertex();

/*! Prints the segmentID, laneID, and waypointID of THIS. */
  void print();
  
private:
  int segmentID, laneID, waypointID;
  vector<Edge*> edges;
  /* The following variables are used for graph search */
  float costToCome;
  Vertex* previousVertex;
};

#endif /*VERTEX_HH_*/
