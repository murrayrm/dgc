#ifndef STRATEGY_INTERSECTION_HH
#define STRATEGY_INTERSECTION_HH

//SUPERCON STRATEGY TITLE: Intersection - SOMETIME STRATEGY
//HEADER FILE (.hh)
//DESCRIPTION: 
//Alice has been sent a signal telling her that she is at an intersection.
//As such, she is now on the lookout for a traffic jam, which is defined as 
//occuring after no progress through the intersection has been made by anyone
//for ten seconds.

#include "Strategy.hh"
#include "sc_specs.h"

namespace s_intersection {

#define INTERSECTION_STAGE_LIST(_) \
  _(wait_in_line, = 1) /*first entry must = 1 (as this is referenced against NEXTstage */  \
  _(wait_your_turn, ) 
DEFINE_ENUM(instersection_stage_names, INTERSECTION_STAGE_LIST)

}

using namespace std;
using namespace s_trafficjam;

class CStrategyTrafficJam : public CStrategy
{

public:

  /** CONSTRUCTORS **/
  CStrategyTrafficJam() : CStrategy() {}  

  /** DESTRUCTOR **/
  ~CStrategyTrafficJam() {}


  /** MUTATORS **/

  void stepForward(const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface);
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

};

#endif
