//SUPERCON STRATEGY TITLE: Intersection - SOMETIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyIntersection.hh"
#include "StrategyHelpers.hh"

void CStrategyIntersection::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE
  currentStageName = roadblock_stage_names_asString( roadblock_stage_names(stgItr.nextStage()) );
  
  switch( stgItr.nextStage() ) {
  case s_intersection::wait_in_line:
    //superCon will start counting to 10, but will NOT pause Alice unless
    //she is at the edge of the intersection (nose on the stop line). This will
    //allow her to follow vehicles in front of her in line, but not pass
    //through the intersection when it isn't her turn.
    {

      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfMission( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      if( m_pdiag->intersectionProgress == true )
      //This loop resets the count for observing a traffic jam each time someone
      //moves through the intersection or we move up in the queue.
      //DEFINE intersectionProgress!!!
      //Lives in Diagnostic.cc, SuperCon.hh (needs to be done)
      //Needs: interface_superCon_tplanner.hh (new file), StateServer.cc (new section)
      {
        stgItr.reset();
        SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Intersection - Resetting" );
      }

      if( stgItr.currentStageCount() > TIMEOUT_LOOP_WAIT_TEN_SECS ) {
	transitionStrategy( StrategyTrafficJam, "Intersection - no progress for 10 s --> Traffic Jam");
	SuperConLog().log( STR, WARNING_MSG, "StrategyIntersection - timed out --> traffic jam" );
	stgItr.incrStage();
      }
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->atStopLine == false) {
	//atStopLine needs definition. Will probably be a message from Tplanner.
	skipStageOps == true
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(sc_interface::estop_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Intersection - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_intersection::wait_your_turn:
    //Should be at intersection in e-stop pause mode waiting for our turn to go
    //through the intersection. If more than ten seconds goes by without anyone
    //driving past us, or without us moving up in queue, then we need to say
    //that this scenario is a traffic jam and call that module.
    { 
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Road Block: %s", currentStageName.c_str() ) ); 

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfMission( (*m_pdiag), currentStageName.c_str() );
      }
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT*/

      if( m_pdiag->intersectionProgress == true )
      //This loop resets the count for observing a traffic jam each time someone
      //moves through the intersection or we move up in the queue.
      //DEFINE intersectionProgress!!!
      //Lives in Diagnostic.cc, SuperCon.hh (needs to be done)
      //Needs: interface_superCon_tplanner.hh (new file), StateServer.cc (new section)
      {
        stgItr.reset();
        SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Intersection - Resetting" );
      }

      if( stgItr.currentStageCount() > TIMEOUT_LOOP_WAIT_TEN_SECS ) {
	transitionStrategy( StrategyTrafficJam, "Intersection - no progress for 10 s --> Traffic Jam");
	SuperConLog().log( STR, WARNING_MSG, "StrategyIntersection - timed out --> traffic jam" );
	stgItr.incrStage();
      }
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->superConPaused == false ) {
	
	skipStageOps = true;
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "StrategyIntersection - Alice not yet e-stop paused -> will poll" );
      }
      
      if( m_pdiag->goThroughIntersection == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "StrategyIntersection - Alice not yet cleared to go by TrafficPlanner -> will poll");
      }
      
      if( m_pdiag->trajRecievedFromPln == false) {
        skipStageOps = true;
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "StrategyIntersection - NOT recieving traj from TrajPlanner");
      }
      
      /* ALL conditional checks should be completed ABOVE this point */       	  
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if (skipStageOps == false) {
	transitionStrategy( StrategyNominal , "Intersection - Strategy complete --> nominal" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Intersection - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }      
    }
  break;  
  /** END OF STRATEGY **/
    
  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyIntersection::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyIntersection::stepForward - default case in nextStep switch() reached" );
      transitionStrategy(StrategyNominal, "ERROR: Intersection - default stage reached!" );
    }
  }
}


void CStrategyIntersection::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Intersection - Exiting" );
}

void CStrategyIntersection::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset();
  skipStageOps.reset(); //resets to FALSE 
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Intersection - Entering" );
}
