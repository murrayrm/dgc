/*
 *  Alice_Nominal.h
 *  DPlannerModule
 *
 *  Created by Melvin E. Flores on 11/21/06.
 *  Control and Dynamical Systems
 *	California Institute of Technology
 *
 *  Copyright 2006. All rights reserved.
 *
 */

#ifndef ALICE_NOMINAL_H
#define ALICE_NOMINAL_H

#include "OTG_Interfaces.h" 
#include "OTG_Utilities.h" 

/* Flat outputs: */
#define z1			 z[0]
#define z1d			 z[1]
#define z1dd		 z[2]
#define z1ddd		 z[3]
#define z2			 z[4]
#define z2d			 z[5]
#define z2dd		 z[6]
#define z2ddd		 z[7]
#define z3			 z[8]

/* States: */
#define x1			 x[0]
#define x2			 x[1]
#define x3			 x[2]
#define x4			 x[3]

/* Inputs: */
#define u1			 u[0]
#define u2			 u[1]

/* Parameters: */
#define p1			 Parameters[0]
#define p2			 Parameters[1]
#define p3			 Parameters[2]
#define p4			 Parameters[3]
#define p5			 Parameters[4]
#define p6			 Parameters[5]
#define p7			 Parameters[6]
#define p8			 Parameters[7]
#define p9			 Parameters[8]
#define p10			 Parameters[9]
#define p11			 Parameters[10]

class CAlice_Nominal: public IOCProblem
{
	public:
		 CAlice_Nominal(void);
		~CAlice_Nominal(void);

		virtual void GetTimeInterval(TimeInterval* const timeInterval);
		virtual void GetOCPSizes(OCPSizes* const ocpSizes);
		virtual void GetNURBSData(const NURBSDataType* const dataType, const int* const NofFlatOutputs, int* const Data);
		virtual void GetBreakPoints(const TimeInterval* const timeInterval, const int* const NofFlatOutputs, const int* const NofBreakPoints, double** const BreakPoints);

		virtual void GetCostCollocPoints(const TimeInterval* const timeInterval, const int* const NofCostCollocPoints, double* const CostCollocPoints);
		virtual void GetConstraintCollocPoints(const TimeInterval* const timeInterval, const int* const NofConstraintCollocPoints, double* const ConstraintCollocPoints);

		virtual void GetOCPParameters(const int* const i, const double* const t, const FunctionType* const functionType, const int* const NofParameters, double* const Parameters);
		virtual void RecoverSystem(const double* const tau, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, double* const t, const int* const NofStates, double* const x, const int* const NofInputs, double* const u);

		virtual void TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt);
		virtual void FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf);

		virtual void GetLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A);
		virtual void InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli);
		virtual void TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt);
		virtual void FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf);

		virtual void GetLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const NofConstraints, double* const LUBounds);

	private:

		int m_i;
		int m_j;
		
		int m_NofParameters;
		double* m_Parameters;
};

#endif /* ALICE_NOMINAL_H */

