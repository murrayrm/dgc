#ifndef VIEWPORT_H_
#define VIEWPORT_H_

#include <SDL/SDL.h>
#include "Color.h"
#include "Vector.h"

namespace TrafSim
{
	
enum ViewportEventType   { EMPTY, MOUSEMOTION, MOUSEBUTTONDOWN, MOUSEBUTTONUP, KEYDOWN, KEYUP, REPAINT, QUIT };
enum ViewportEventButton { LEFT = 1, MIDDLE, RIGHT }; ///< Refers to mouse buttons

/**
 * You get ViewportEvents by calling Viewport::pollEvents. Events corresponding to each member
 * of the ViewportEventType enumeration are passed from the window manager to the client program
 * using this struct.
 * 
 * The meanings of the event types are as follows:
 * 
 * EMPTY 	        -- there is no event that requires the client program's attention
 * 
 * MOUSEMOTION      -- the mouse has been moved within the viewport's client area.
 * 					   pos contains the new location of the mouse, in world coordinates.
 * 
 * MOUSEBUTTONDOWN  -- the user has clicked (but not yet released) one of the mouse buttons.
 * 					   button contains one of the ViewportEventButton values.
 * 
 * MOUSEBUTTONUP    -- the user has released a previously clicked mouse button.
 * 					   button contains one of the ViewportEventButton values.
 * 
 * KEYDOWN			-- the user has pressed (but not yet released) a keyboard key.
 * 					   button contains one of the SDLKey values.
 * 
 * KEYUP			-- the user has released a previously pressed keyboard key.
 * 					   button contains one of the SDLKey values.
 * 
 * REPAINT			-- some event has occurred that requires the viewport's client area to be
 * 					   repainted. Any necessary drawing calls should be made again, and then
 * 					   Viewport::repaint() should be called. (it's safe to ignore this event in
 * 					   a real-time application).
 * 
 * QUIT				-- an event has occurred that caused the viewport to close.
 */
struct ViewportEvent
{
	/// Dispatch on this member to handle specific events
	ViewportEventType type; 
	
	/// Either a mouse button (one of ViewportEventButton enum) of a key on the keyboard.
	/// The latter is represented by the SDLKey enumeration, the contents of which
	/// can probably be found here: http://www.libsdl.org/cgi/docwiki.cgi/SDLKey
	unsigned int button; 
	
	/// In the case of a mouse event, this is the location of the mouse pointer 
	/// *in OpenGL world coordinates*. That means this value depends on the pan/zoom
	/// and size of the viewport that generated the event.
	Vector pos;
};

/**
 * This class encapsulates the functionality of a window that displays some portion of a 2D
 * OpenGL rendering environment. Only one such window is meant to be opened at a time. 
 * Open a window with a call to Viewport::open(). Subsequent calls to Viewport::open() from any
 * viewport class will only reset the viewport to its default configuration; it will not open
 * a new viewport.
 * 
 * Once a viewport is open, the client program should call Viewport::pollEvents() until it returns
 * "false", make any desired calls to the draw() functions of other libtrafsim classes, and
 * then call Viewport::repaint() to update the view.
 */ 
class Viewport
{
public:

	/* ******** CONSTRUCTORS AND DESTRUCTORS ******** */
	
	/// No API calls are made in the constructor; only some private members are initialized.
	Viewport();
	
	/// The destructor automatically calls Viewport::close() to make sure that the window is
	/// not left dangling after the Viewport object is destroyed.
	~Viewport();
	
	/* ******** INITIALIZATION AND TERMINATION ******** */
	
	/// Creates a viewport window. This is the first function to be called on a Viewport.
	/// Note that multiple calls to open() will only reset the parameters of the first
	/// window rather than opening a new one.
	/// \param width The horizontal size of the window, in pixels
	/// \param height The vertical size of the window, in pixels
	/// \param caption The text to appear on the window's title bar
	/// \return True on success, False on failure
	bool open(unsigned int width, unsigned int height, std::string const& caption);
	
	/// Closes a viewport window, unloads the windowing API. It's OK to call this function
	/// more than once. However, it is not safe to call this function from a thread other than
	/// the one the call to Viewport::open() was made from.
	/// \return True on success, False on failure
	bool close();
	
	/* ******** DATA MEMBER ACCESS FUNCTIONS ******** */
	
	/// \return The horizontal size of the viewport window, in pixels 
	unsigned int getScreenWidth() const;
	/// \return The vertical size of the viewport window, in pixels 
	unsigned int getScreenHeight() const;
	/// \return The dimensions of the visible area in the viewport. The units are understood to be meters. 
	Vector const& getPhysicalSize() const;
	/// \return The position in world coordinates of the center of the viewport. The units are understood to be meters.
	Vector const& getPhysicalPosition() const;
	/// \return Whether or not the user is allowed to manipulate the viewport using the mouse
	bool isPanZoomEnabled() const;
	
	/* ******** PROPERTY SETTING FUNCTIONS ******** */
	
	/// Changes the on-screen dimensions of the viewport window. Also scales the physical dimensions accordingly,
	/// so that there is no aspect ratio distortion. It is not safe to call this function from a different thread
	/// than the one the viewport was opened in. This call will also fail if the window is not currently open.
	/// \param width The new horizontal size of the window, in pixels
	/// \param height The new vertical size of the window, in pixels
	/// \return True on success, False on failure
	bool resize(unsigned int width, unsigned int height);
	/// Sets the viewport to allow the user to manipulate the view using the mouse
	void enablePanZoom();
	/// Sets the viewport to prevent the user from manipulating the view using the mouse
	void disablePanZoom();
	/// Changes the "zoom" setting of the viewport. It is not safe to call this function from a different thread
	/// than the one the viewport was opened in. This call will also fail if the window is not currently open.
	/// \param size The new dimensions of the section of the world visible through the viewport window.
	/// \return True on success, False on failure
	bool setPhysicalSize(Vector const& size);
	/// Changes the "pan" setting of the viewport. It is not safe to call this function from a different thread
	/// than the one the viewport was opened in. This call will also fail if the window is not currently open.
	/// \param position The new world coordinates of the center of the viewport window.
	/// \return True on success, False on failure
	bool setPhysicalPosition(Vector const& position);
	/// Changes the color of the viewport. It is not safe to call this function from a different thread
	/// than the one the viewport was opened in. This call will also fail if the window is not currently open.
	/// \return True on success, False on failure
	bool setBackgroundColor(Color const& c);
	
	/* ******** MAIN WINDOWING API ******** */
	
	/**
	 * This function should be called on a regular basis to process windowing events. The window will not
	 * function *at all* if this function isn't called. If an event comes through that may concern the client
	 * program, it will be passed through to the viewportEvent argument. Otherwise, the viewportEvent's type
	 * will be set to EMPTY. The correct idiom for using this function is:
	 *
	 * ViewportEvent event;
	 * while (pollEvents(event)) { ... process events ... }
	 *
	 * It is not safe to call this function from a different thread than the one the viewport was opened in.
	 * This call will also fail if the window is not currently open.
	 *
	 * \param viewportEvent An event structure to hold the function's output
	 * \return True if an event was processed, False if there are no more events
	 */
	bool pollEvents(ViewportEvent& viewportEvent);
	
	/// The results of any calls to libtrafsim's drawing functions will not appear until this function is called.
	/// Furthermore, the 'work screen' is cleared once it is rendered to the real window, so all objects will 
	/// have to be rendered again before the next call to repaint(). It is not safe to call this function from a 
	/// different thread than the one the viewport was opened in. This call will also fail if the window is not currently open.
	/// \return True on success, False on failure
	bool repaint();
	
private:
	enum PanZoomMode { NONE, PAN, ZOOM };

	// the following five functions take care of pan/zoom functionality
	void _handleMouseMotion(float xrel, float yrel);   
	void _handleMouseButtonDown(unsigned int button, unsigned int x, unsigned int y); 
	void _handleMouseButtonUp(unsigned int button, unsigned int x, unsigned int y);
	void _handleKeyUp(SDLKey key);
	void _handleKeyDown(SDLKey key);
	
	// maps between pixel-based coordinates reported by SDL and OpenGL world coordinates
	// that are passed to the client application
	Vector _translateCoords(int x, int y);

	SDL_Surface* screen;
	unsigned int screenWidth;	// in pixels
	unsigned int screenHeight;  // in pixels

	const Vector defaultPhysicalSize; 
	const Vector defaultPhysicalPos;	
	Vector physicalSize;  // in OpenGL coords (meters)
	Vector physicalPos;	  // in OpenGL coords (meters)

	PanZoomMode panZoomMode;

	bool panZoomEnabled;  // is the user allowed to pan and zoom with the mouse?
	bool altDown;		  // only pan/zoom when the alt key is down
	bool closed; // internal flag
};

}

#endif /*VIEWPORT_H_*/
