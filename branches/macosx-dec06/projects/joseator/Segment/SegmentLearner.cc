/*****************************************************************
 *  Project    : Learning and adaptation, SURF 2006               *
 *  File       : SegmentLearner.cc                                *
 *  Description:       *
 *  Author     : Jose Torres                                      *
 *  Modified   : 7-27-06                                          *
 *****************************************************************/      

#include "SegmentLearner.hh"

// Constructor
SegmentLearner::SegmentLearner(int skynet_key) :
  CSkynetContainer(SNastate, skynet_key)
{
  number = 1;
  entrance_A = NULL;
  entrance_B = NULL;
}



//destructor
SegmentLearner::~SegmentLearner()  
{
 
}



void SegmentLearner::addNode(double roadEdge_a, double roadEdge_b)
{
  // cout<<fixed <<showpoint <<setprecision(6);
  
  SegmentNode* newNode = new SegmentNode;
  newNode->roadEdge_A = roadEdge_a;
  newNode->roadEdge_B = roadEdge_b;
  
  if(entrance_A == NULL && entrance_B == NULL)		/*! adds node if not in "Location" list already*/
    {
      entrance_A = newNode;
      entrance_B = newNode;
      newNode->next = NULL;
      newNode->previous = NULL;
    }
  else
    {
      newNode->next = NULL;
      newNode->previous = entrance_B;
      entrance_B->next = newNode;
      entrance_B = newNode;
    }    

} //end addNode



bool SegmentLearner::checkForTransition(ofstream &outData)
{
  Transition newSegment;
  //Set up socket for segment transition
  int changedRoad = m_skynet.listen(SNsegmentTransition, ALLMODULES);
  if (changedRoad < 0)
    {
      cerr << "TraversedPath::TraversedPath(): skynet listen returned error" << endl;
    }
  
  
  if(m_skynet.get_msg(changedRoad, &newSegment, sizeof(newSegment), 0) != sizeof(newSegment))
    {
      cout << "Didn't receive the right number of bytes in the state structure" << endl;
      return false;
    }
  else
    {
      number = newSegment.segment;
      return newSegment.segmentTransition;  
    }
} //end checkForTransition



void SegmentLearner::active() 
{
  Road my_command; 
  ofstream  outData;
  int num;
  //Set up socket for road data
  int checkPointSock = m_skynet.listen(SNrndfCheckpoint, ALLMODULES);
  cout<< checkPointSock <<endl;
  if (checkPointSock < 0)
    {
      cerr << "TraversedPath::TraversedPath(): skynet listen returned error" << endl;
    }
  
  
  
  while(true)
    {
      num = number;
      char name[200];
      int x;
      cout<< "opening new file for new segment"<<endl;
      x = sprintf(name, "segmentFiles/%d_segment", num);
      //cout<< "opened new file " <<name <<endl;
      outData.open(name);
      if(!outData) cout<<"ERROR, unable to open file" <<name <<endl;
      outData<<fixed <<showpoint <<setprecision(6);
  
      while(1)
	{
	  // cout<<"about to attempt to get edge of road" <<endl;
	  if(m_skynet.get_msg(checkPointSock, &my_command, sizeof(my_command), 0) != sizeof(my_command))
	    {
	      cout << "Didn't receive the right number of bytes in the state structure" << endl;
	    }
	  cout<< "received data " << my_command.edge_a <<" " <<my_command.edge_b<<endl;
	  outData<<my_command.edge_a <<" " <<my_command.edge_b <<endl;
	  if(checkForTransition(outData))
	    {
	      //cout<< "transition into new segment" <<endl;
	      outData.close();
	      //cout<< "closed file" <<endl;
	      break;
	    }
	  // DGCusleep(1000000);
	} //end while
    }     
  
} //end active



int main() 
{  
  int sn_key = 0;  
  char* pSkynetkey = getenv("SKYNET_KEY");
  cout<<"got skynet key"<<endl;
  if ( pSkynetkey == NULL)
    {
      cerr << "SKYNET_KEY environment variable isn't set." << endl;
    } 
  else
    {
      sn_key = atoi(pSkynetkey);
    }
  cerr << "Constructing skynet with KEY = " << sn_key << endl;
  SegmentLearner course(sn_key);
  cout<<"about to call active"<<endl;  
  course.active();
  
  return 0;
} //end main    
