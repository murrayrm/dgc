function playRawData(filename, start, stop);

raw = load(filename);



[row,col] = size(raw);
timings = raw(:,1);
ranges = raw(:,3:col);
[row,col] = size(ranges)

%the no-data value is 8191...
for i=1:row
    for j=1:col
        if ranges(i,j)==8191 
            ranges(i,j)=0;
        end
    end
end


temp = [1:col];
angles = 2*pi*temp/360;

temp = ones(row,1);
ang = temp*angles;

x = cos(ang).*ranges;
y = sin(ang).*ranges;

figure();
for j=start:stop
    pause(.01);
    plot(x(j,:), y(j,:),'k.');
    axis([-6000,6000,0,4000]);
    drawnow
end