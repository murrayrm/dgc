#include <iostream>
#include <cassert>
#include "Environment.hh"
using namespace std;

  ///////////
  // Setup //
  ///////////

SimulationEnvironment::SimulationEnvironment(char* rndf_filename, 
					     int skynetKey,
					     char* sim_filename, 
					     char* lanes_filename) :
  Environment(rndf_filename), CSkynetContainer(SNstateSim, skynetKey) {

  cout<<"** Simulation env constructor **"<<endl;

  this->sim_filename = sim_filename;
  this->lanes_filename = lanes_filename;

}

SimulationEnvironment::~SimulationEnvironment()
{
  cout<<"** Simulation env destructor **"<<endl;
  
  delete[] sim_filename;
  delete[] lanes_filename;
}

void SimulationEnvironment::setup()
{

  cout <<endl<< "Simulation Environment setup. " << endl;

  // access astateSim
  loadSimFile(sim_filename);
  UpdateStateSim();

  double vn, ve;
  cout<<"Alice's velocity (n e): ";
  cin>>vn>>ve;

  // Alice's vehicle ID is always 0
  ourState = new State(0, m_state_sim.ne_coord(), NEcoord(vn,ve));
  ourState->setMode(0); 

  // load the RNDF
  loadRNDF();

  // get the next goal
  setGoal();

  // get lane boundaries
  setLaneBoundaries();

  // determine which lane we're in
  calcLane(*ourState);

  // now that we have our state, figure out right/left lanes
  calcRightLanes();

  int input;
  cout<<"Number of vehicles: ";
  cin>>input;

  for(int i=0; i<input; i++) {
    cout<<endl<<"Adding "<<i+1;
    if (i+1==1) cout<<"st ";
    else if (i+1==2) cout<<"nd ";
    else if (i+1==3) cout<<"rd ";
    else cout<<"th ";
    cout<<"vehicle."<<endl;
    addVehicle();
  }
  
  ourState->setMode(1);
  cout <<endl<< endl<<"Set to normal driving mode." <<endl;
  
  cout <<endl<< "Environment setup complete."<< endl;
}


void SimulationEnvironment::loadRNDF()
{
  rndf.loadFile(rndf_filename);
  cout<<"Loaded "<<rndf_filename<<endl<<endl;

  // TODO: load current segment from a file
  //currSegment = rndf.getSegment(input);

  // initialize the laneBoundaries array
  laneBoundaries = new vector<NEcoord>[currSegment->getNumOfLanes()];

}

void SimulationEnvironment::setLaneBoundaries()
{
  // TODO: load lane boundaries from a file
}

void SimulationEnvironment::update()
{

  // don't need to update the lanes since they're loaded all
  // at once every time we run setupSimulation()

  // update our state
  updateState();

  // check the goal status
  if (atGoal())
    setGoal();

  // update the other vehicles
  updateVehicles();

}

void SimulationEnvironment::updateState()
{
  UpdateStateSim();  
  ourState->setPos(m_state_sim.ne_coord());
  
  double vn, ve;
  cout<<"New velocity (n e): ";
  cin>>vn>>ve;
  ourState->setVel(vn,ve);

  calcLane(*ourState);
}

void SimulationEnvironment::updateVehicles()
{
  // TODO: get vehicle status from a file, using astateSim
}

void SimulationEnvironment::addVehicle()
{
  // TODO: fill this in with something
}


void SimulationEnvironment::setGoal()
{
  // TODO: load current goal from a file
}
