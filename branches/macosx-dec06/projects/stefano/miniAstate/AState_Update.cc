/* AState_Update.cc (miniAstate)
 *
 * Astate class function to update the state estimate
 *
 * Stefano Di Cairano, NOV-06
 *
 * 
 */


#include "AState.hh"
#include "Matrix.hh"
#include "sparrow/display.h"
#include "sparrow/flag.h"

#define FREQUENCY_DIVISOR 8
#define square(x) ((x)*(x))

extern unsigned short setStateMode(unsigned short);
GisCoordLatLon latsForVehState;
GisCoordUTM utmForVehState;

void AState::updateStateThread()
{
  unsigned long long stateTime = 0;
  unsigned long long apxTime = 0;
  rawApx apxEstimate;
  int newState = 0;
  int count = 0;   //Counter that increments every loop (used to limit broadcast rate)
  
  
  while (!quitPressed)
  {
    
    newState = 0;
    DGClockMutex(&m_ApxDataMutex);
    
    if (apxBufferReadInd > apxBufferLastInd)
    { 
      DGCSetConditionTrue(apxBufferFree);

      apxBufferLastInd++;

      int ind = apxBufferLastInd % APX_DATA_BUFFER_SIZE;

      
      apxEstimate=apxBuffer[ind];

      apxTime = apxBuffer[ind].time;

      if(stateTime < apxTime)
      {
        stateTime = apxTime;
      }

      newState = 1;
      apxCount++;

    }
    
    if( (DEBUG_LEVEL>4) && (apxActive == 0) ) //I build this for test
    { 
      usleep(5000);
      apxEstimate.time = 100;
      apxEstimate.data.latitude = 100;
      apxEstimate.data.longitude = 100;
      apxEstimate.data.altitude = 100;
      apxEstimate.data.heading = 100;
      apxEstimate.data.northVelocity = 100;
      apxEstimate.data.eastVelocity = 100;
      apxEstimate.data.downVelocity = 100;
      apxEstimate.data.longitudinalAcc = 100;
      apxEstimate.data.transverseAcc = 100;
      apxEstimate.data.downAcc = 100;
      apxEstimate.data.roll = 100;
      apxEstimate.data.pitch = 100;
      apxEstimate.data.heading = 100;
      apxEstimate.data.rollRate = 100;
      apxEstimate.data.pitchRate = 100;
      apxEstimate.data.yawRate = 100;
      apxEstimate.data.alignStatus = NO_SOLUTION;
      newState=1;
    }
    
    
    DGCunlockMutex(&m_ApxDataMutex);

    if (newState == 0)
    {
      newData.bCond = false;
      DGCWaitForConditionTrue(newData);
    }
    else
    { 
      latsForVehState.latitude = apxEstimate.data.latitude;
      latsForVehState.longitude = apxEstimate.data.longitude;
      gis_coord_latlon_to_utm(&latsForVehState, &utmForVehState, GIS_GEODETIC_MODEL_WGS_84);
      
      DGClockMutex(&m_VehicleStateMutex);
      _vehicleState.Timestamp = apxEstimate.time;
      _vehicleState.Northing = utmForVehState.n; 
      _vehicleState.Easting = utmForVehState.e;
      _vehicleState.Altitude = -apxEstimate.data.altitude; //POS-LV gives positive altitude
      _vehicleState.GPS_Northing = _vehicleState.Northing ;
      _vehicleState.GPS_Easting = _vehicleState.Easting;
      _vehicleState.raw_YawRate = apxEstimate.data.heading;
      _vehicleState.Vel_N = apxEstimate.data.northVelocity;
      _vehicleState.Vel_E = apxEstimate.data.eastVelocity;
      _vehicleState.Vel_D = apxEstimate.data.downVelocity;
      _vehicleState.Acc_N = 0; //we get accelerations w.r.t. reference frame. need to transform them
      _vehicleState.Acc_E = 0;
      _vehicleState.Acc_D = 0;
      _vehicleState.RollAcc = 0; //we don't get these
      _vehicleState.PitchAcc = 0;
      _vehicleState.YawAcc = 0;
      _vehicleState.RollRate = apxEstimate.data.rollRate*M_PI/180;
      _vehicleState.PitchRate = apxEstimate.data.pitchRate*M_PI/180;
      _vehicleState.YawRate = apxEstimate.data.yawRate*M_PI/180;
      _vehicleState.Roll = apxEstimate.data.roll*M_PI/180;
      _vehicleState.Pitch =  apxEstimate.data.pitch*M_PI/180;
      _vehicleState.Yaw =  apxEstimate.data.heading*M_PI/180;

      // Update local pose
      if (!this->haveInitPose)
      {
        this->haveInitPose = true;
        this->initE = _vehicleState.Easting;
        this->initN = _vehicleState.Northing;
      }
      _vehicleState.localX = _vehicleState.Northing - this->initN;
      _vehicleState.localY = _vehicleState.Easting - this->initE;
      _vehicleState.localZ = _vehicleState.Altitude;
      _vehicleState.localRoll = _vehicleState.Roll;
      _vehicleState.localPitch = _vehicleState.Pitch;
      _vehicleState.localYaw = _vehicleState.Yaw;
      _vehicleState.vehicleVelX =   cos(_vehicleState.Yaw) * _vehicleState.Vel_N;
      _vehicleState.vehicleVelX +=  sin(_vehicleState.Yaw) * _vehicleState.Vel_E;
      _vehicleState.vehicleVelY =  -sin(_vehicleState.Yaw) * _vehicleState.Vel_N;
      _vehicleState.vehicleVelY +=  cos(_vehicleState.Yaw) * _vehicleState.Vel_E;
      _vehicleState.vehicleVelRoll = _vehicleState.RollRate;
      _vehicleState.vehicleVelPitch = _vehicleState.PitchRate;
      _vehicleState.vehicleVelYaw = _vehicleState.YawRate;
      
      //We don't get confidences yet

      _vehicleState.NorthConf = 1;
      _vehicleState.EastConf = 1;
      _vehicleState.HeightConf = 1;
      _vehicleState.RollConf = 1;
      _vehicleState.PitchConf = 1;
      _vehicleState.YawConf = 1;
      apxStatus = apxEstimate.data.alignStatus;
      DGCunlockMutex(&m_VehicleStateMutex);


      stateMode = setStateMode((unsigned short)apxEstimate.data.alignStatus);
      
      if (count++ % FREQUENCY_DIVISOR == 0)
      {	
	      broadcast();
      }
      if (_options.logRaw==1)
      {
        stateLogStream.write((char*)&_vehicleState, sizeof(_vehicleState));
        if(apxBufferReadInd % 1300 == 1)
        {
          apxLogStream.flush();
        }      	
      }
    }
  }
}
/*
Matrix nav2earth(double yaw, double pitch, double roll)
{
  Matrix R(3,3);
#ifdef UNUSED
  double ca=cos(yaw);
  double cb=cos(pitch);
  double cg=cos(roll);
  double sa=sin(yaw);
  double sb=sin(pitch);
  double sg=sin(roll);

  R[]={(ca*cb*cg-sa*sg), (-ca*cb*sg-sa*cg), (ca*sb), (sa*cb*cg+ca*sg), (-ca*cb*sg+ca*cg), (sa*sb), (-sb*cg), (sb*sg), (cb)};  
 call.\n" ) ;
perror( NULL ) ;#endif  

  return(R);
}
*/
/*! this function converts the apxStatus into AState statemode.
 * preinit correspond to the initial solution
 * init correspond to a large error (coarse leveling or heading error > 15 )
 * nav is for full navigation or heading error < 15 deg (fine alignement)
 * oh_shit is for a missing initial solution/ system errors
 * */ 
unsigned short setStateMode(unsigned short status)
{
  unsigned short mode = OH_SHIT;
/*	switch(apxStatus)
	{
      case 0 : stateMode = NAV;
	     //  cout << stateMode << "= I am in NAV" << endl;
               break;
	  case 1 : stateMode = NAV;
	      // cout << stateMode << "= I am in NAV" << endl;
               break;
	  case 2 : stateMode = INIT;
	     //  cout << stateMode << "= I am in INIT" << endl;
               break;
	  case 3 : stateMode = INIT;
	     //  cout << stateMode << "= I am in INIT" << endl;
               break;
	  case 4 : stateMode = INIT;
	     //  cout << stateMode << "= I am in INIT" << endl;
               break;
	  case 5 : stateMode = INIT;
	      // cout << stateMode << "= I am in INIT" << endl;
               break;
	  case 6 : stateMode = INIT;
	      // cout << stateMode << "= I am in INIT" << endl;
               break;
	  case 7 : stateMode = PREINIT;
	     //  cout << stateMode << "= I am in PREINIT" << endl;
               break;
	  case 8 : stateMode = OH_SHIT;
	     //  cout << stateMode << "= I am in SHIT" << endl;
               break;
      default : cerr << "invalid apxStatus code received: " << apxStatus << endl;
	}*/
  if(status<8)
  {
	  mode = PREINIT;
  }
  if(status<7)
  {
	  mode = INIT;
  }
if(status<1)
  {
	  mode = NAV;
  }
	return(mode);
}
