/* AState_Main.cc 
 * 
 * Main function of the minimal Astate with replay capabilities
 * 
 *        Stefano Di Cairano, DEC-06
 * 
 */
 
#include <getopt.h>
#include "AState.hh"
#include <stdlib.h>
#include "DGCutils"

void print_usage() {
  cout << "Usage: astate [options]" << endl;
  cout << '\t' << "--help, -h" << '\t' << "Display this message" << endl;
  cout << '\t' << "--noapx, -a" << '\t' << "Disable Applanix (what are you gonna do??)" << endl;
  cout << '\t' << "--now, -!" << '\t' << "Run without warning massage at start" << endl;
  cout << '\t' << "--nosparrow, --nosp, -x" << '\t' << "Disable sparrow" << endl;
  cout << '\t' << "--verbose, -v" << '\t' << "Enable verbose mode" << endl;
  cout << '\t' << "--snkey, -s <snkey>" << '\t' << "Use skynetkey <snkey> (defaults to $SKYNET_KEY)" << endl;
  cout << '\t' << "--log, -l " << '\t' << "Log raw data to /tmp/log/astate_raw/<timestamp>" << endl;
  cout << '\t' << "--replay, -r <logfile>" << '\t' << "Replay raw data from <logfile>" << endl;
  cout << '\t' << "-p" << '\t' <<  "Start astate in timber playback mode" << endl;
}

const char* const short_options = "higonakvs:lr:f:pdxwc!";

static struct option long_options[] =
  {
    //first: long option (--option) string
    //second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
    //third: if pointer, set variable to value of fourth argument
    //	 if NULL, getopt_long returns fourth argument
    {"help",	0,	NULL,	'h'},
    {"noapx",	0,	NULL,	'a'},
    {"a",       0,      NULL,   'a'},
    {"verbose",	0,	NULL,	'v'},
    {"snkey",	1,	NULL,	's'},
    {"s",	1,	NULL,	's'},
    {"log",	0,	NULL,	'l'},
    {"l",	0,	NULL,	'l'},
    {"replay",	1,	NULL,	'r'},
    {"r",	1,	NULL,	'r'},
    {"fast",	1,	NULL,	'f'},
    {"f",	1,	NULL,	'f'},
    {"p",   0,	NULL,   'p'},
    {"nosparrow", 0, NULL,  'x'},
    {"x",   0,      NULL,   'x'},
    {"nosp", 0,     NULL,   'x'},
    {"now", 0,      NULL,   '!'},
    {0,0,0,0}
  };

AState *ss;

astateOpts inputAstateOpts;

int main(int argc, char **argv)
{
  // Set this so we know the difference between a key set in the 
  // command line and an uninitialized key.
  int sn_key = -1;
  int now = 0;

  // Temporary variables for command-line parsing
  int options_index;
  int ch;

  // Set default options for the astateOpts struct.
  inputAstateOpts.useApplanix = 1;
  inputAstateOpts.useVerbose = 0;
  inputAstateOpts.logRaw = 0;
  inputAstateOpts.useReplay = 0;
  inputAstateOpts.timberPlayback = 0;
  inputAstateOpts.useSparrow = 1;
  inputAstateOpts.fastMode = 0;

  // Parse command-line options
  while((ch = getopt_long(argc, argv, short_options, long_options, &options_index)) != -1)
    {
      switch(ch)
	{
	case 'h':
	  print_usage();
	  return 0;
	  break;
	case 'a':
	  inputAstateOpts.useApplanix = 0;
	  break;
	case 'v':
	  inputAstateOpts.useVerbose = 1;
	  break;
	case 's':
	  sn_key = atoi(optarg);
	  break;
	case 'l':
	  inputAstateOpts.logRaw = 1;
	  break;
	case 'r':
	  inputAstateOpts.useReplay = 1;
	  strncpy(inputAstateOpts.replayFile, optarg, 99);
	  break;
	case 'f':
	  inputAstateOpts.useReplay = 1;
	  inputAstateOpts.fastMode = 1;
	  strncpy(inputAstateOpts.replayFile, optarg, 99);
	  break;
	case 'p':
	  inputAstateOpts.timberPlayback = 1;
	  break;
	case 'x':
	  inputAstateOpts.useSparrow = 0;
	  break;
	case '!':
	  now = 1;
	  break;
	default:
	  break;
	}
    }
	
  if (inputAstateOpts.useReplay == 1 && inputAstateOpts.logRaw == 1) {
    cerr << "Don't log a replay file." << endl;
    return 0;
  }

  if (!(inputAstateOpts.useReplay || inputAstateOpts.timberPlayback || now)) {
    cerr << "Warning: AState should be started from a dead stop for proper orientation." << endl;
    cerr << "Hit enter to continue once the vehicle is stopped." << endl;
    cerr << "It will take approximately 30 seconds for state to converge." << endl;
    cerr << "DO NOT MOVE DURING THAT TIME!" << endl;
    getchar();
  }

  char* pSkynetkey = getenv("SKYNET_KEY");
  if (sn_key == -1) {
    if ( pSkynetkey == NULL )
      {
	cerr << "SKYNET_KEY environment variable isn't set." << endl;
      } else {
	sn_key = atoi(pSkynetkey);
      }
  }
	
  cerr << "Constructing skynet with KEY = " << sn_key << endl;

  //Create AState server:
  ss = new AState(sn_key, inputAstateOpts);

  ss->active();

  delete ss;

  return 0;
}
