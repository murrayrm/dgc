#include "AState.hh"
// SDC Nov-06

//MetaState was solely responsible for Keeping track of which sensors are enabled / active.
//For now it is almost useless but it is kept for future upgrades of Applanix runtime reconfig

using namespace std;

void AState::metaStateThread() 
{

  // give time for the heartbeats to start
  sleep(1);

  while(!quitPressed) 
  {
    if(DEBUG_LEVEL>4)
    {
      cout << "This is the METASTATE thread working" << endl;
      sleep(1);
    }
    DGClockMutex(&m_MetaStateMutex);        
    DGClockMutex(&m_HeartbeatMutex);
    if(_metaState.apxEnabled)
    {
      if (_heartbeat.apxActive)
      {
	    _metaState.apxActive = 1;
	    _metaState.apxStatus = _heartbeat.apxStatus;
      }
      else
      {
	    _metaState.apxActive = 0;
	    _metaState.apxStatus = NO_SOLUTION;
      }
    }

    DGCunlockMutex(&m_HeartbeatMutex);
    DGCunlockMutex(&m_MetaStateMutex);


    _heartbeat.apxActive = false;

    DGCunlockMutex(&m_HeartbeatMutex);
    DGCunlockMutex(&m_MetaStateMutex);
         
    sleep(1);
  }
}
