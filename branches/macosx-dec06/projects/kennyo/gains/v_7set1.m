

V_0 = 7;   % speed of the vehicle- meters/sec

% loop gain
K = .005;
% frequency at which the derivative term rolls off
T_r = 10;
% derivative gain
T_d = 7;
% integral gain
T_i = 30;

run run_gains

save v_7set1.mat
