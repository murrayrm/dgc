% constants
l = 3;
v = 15;

% derivative rolloff frequency
T = 10;

% gains
k_d = 0.1;
k_i = 2.5e-4;
k_p = 0.01;

k_t = 0;


% run it
sim('alice');

% plots
figure(1); plot(tout, yout, tout, rout.signals.values);