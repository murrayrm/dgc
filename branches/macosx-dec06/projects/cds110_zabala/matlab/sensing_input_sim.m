% Spoofs inputs from sensing

clear all;

% CONSTANTS
max_vision_dist = 15;   % for use in determining whether we use GPS or vision
                                    % i.e. we start using vision when we
                                    % are this many meters away

% Load the traj file
traj = dlmread('../rosebowl_straight_matlab.traj');

% compute longitudinal distance (add up segments of traj file)
for (i = 1:size(traj,1)-1)
    
    %time
    segments(i,1) = traj(i,1);
    % length of segment
    segments(i,2) = sqrt( (traj(i,4)-traj(i+1,4))^2 + ...
        (traj(i,5)-traj(i+1,5))^2 );
    
end


% compute total longitudinal distance at each timestep
for (i = (size(segments,1)-1):-1:1)
    segments(i,2) = segments(i,2) + segments(i+1,2);
end

%figure;
plot(segments(:,1),segments(:,2));

% save segments to mat file for use in SIMULINK model
save('distance.mat','segments');

% store total time for running path
total_time = floor(segments(size(segments,1),1));

% store initial error
init_error = segments(1,2)

% store seed for random number generator
seed = random('Normal',0,10);

% run simulation
sim sensing_inputs;

% save the noisy vision data in the traj file
p = vision_data.signals.values;
p = p';
y = interpft(p,size(traj,1));
t = 1:size(traj,1);
%plot(t,y);

traj(:,13) = y';
traj(1:size(traj,1)-1,14) = segments(:,2);

% move up the stop line so we can be sure the follow program isn't
% interfering with our controller
traj(1:400,13)  = traj(67:466,13);
traj(401:466,13) = 0;
traj(1:400,14)  = traj(67:466,14);
traj(401:466,14) = 0;

traj(:,5) = traj(:,5) + 15;

traj_size = [size(traj,1) 13];
dlmwrite('../rosebowl_straight_vision_15e.traj',traj_size,' ');
dlmwrite('../rosebowl_straight_vision.traj_15e',traj,'delimiter',' ','coffset',1,'precision',12,'-append');
%dlmwrite('../rosebowl_straight_vision.traj',traj_size,' ');
%dlmwrite('../rosebowl_straight_vision.traj',traj,'delimiter',' ','coffset',1,'precision',12,'-append');
