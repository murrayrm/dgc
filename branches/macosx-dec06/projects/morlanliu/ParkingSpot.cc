#include "ParkingSpot.hh"
using namespace std;

ParkingSpot::ParkingSpot(int zoneID, int spotID)
{
  this->zoneID = zoneID;
  this->spotID = spotID;
  this->spotWidth = 0;
  this->waypoint1 = NULL;
  this->waypoint2 = NULL;
}

ParkingSpot::~ParkingSpot()
{
  delete waypoint1;
  delete waypoint2;
}

void ParkingSpot::setSpotWidth(int spotWidth)
{
  this->spotWidth = spotWidth;
}

void ParkingSpot::setWaypoint(int waypointID, Waypoint* waypoint)
{
  if(waypointID == 1)
    this->waypoint1 = waypoint;
  else if(waypointID == 2)
    this->waypoint2 = waypoint;
  else
    cout << "Not a valid waypoint ID. Waypoint ID must be 1 or 2." << endl;
}

int ParkingSpot::getZoneID()
{
  return this->zoneID;
}

int ParkingSpot::getSpotID()
{
  return this->spotID;
}

int ParkingSpot::getSpotWidth()
{
  return this->spotWidth;
}

Waypoint* ParkingSpot::getWaypoint(int waypointID)
{
  if(waypointID == 1)
    return this->waypoint1;
  else if(waypointID == 2)
    return this->waypoint2;
  else
    return NULL;
}
