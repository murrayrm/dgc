#ifndef PERIMETERPOINT_HH_
#define PERIMETERPOINT_HH_
#include <vector>
#include "GPSPoint.hh"
#include "Waypoint.hh"
#include <iostream>
using namespace std;

/*! PerimeterPoint class. The PerimeterPoint class represents a perimeterpoint
 * provided by the RNDF file. All perimeterpoints have a zone ID perimeterpoint
 * ID, longitude, and latitude. Perimeterpoints are designated as an entry 
 * perimeterpoint, exit perimeterpoint, or neither.
 * \brief The perimeterpoint class represents a perimeterpoint provided by the RNDF.
 */
class PerimeterPoint: public GPSPoint
{
public:
/*! All perimeterpoints have a zone ID, perimeterpoint ID, longitude, and
 *  latitude. Initially, each perimeterpoint is not designated as a entry
 *  perimeterpoint nor as an exit perimeterpoint. All perimeterpoints have
 *  a "laneID" equal to 0. */
	PerimeterPoint(int zoneID, int zero, int perimeterPointID, 
    double latitude, double longitude);
	virtual ~PerimeterPoint();

/*! Returns the zone ID of THIS. */
  int getZoneID();

/*! Returns the perimeterpoint ID of THIS. */
  int getPerimeterPointID();
  
private:
/*  Zones are identified using the form 'M'. The perimeterpoints of each zone
 *  are identified such that the Pth perimeterpoint of zone M is 'M.0.P'.*/
  int zoneID, perimeterPointID;
};

#endif
