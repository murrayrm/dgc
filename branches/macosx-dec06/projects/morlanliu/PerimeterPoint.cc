#include "PerimeterPoint.hh"
using namespace std;

PerimeterPoint::PerimeterPoint(int m, int n, int p, double lat, double lon) : GPSPoint(m, n, p, lat, lon)
{
}

PerimeterPoint::~PerimeterPoint()
{
}

int PerimeterPoint::getZoneID()
{
  return this->zoneID;
}

int PerimeterPoint::getPerimeterPointID()
{
  return this->perimeterPointID;
}
