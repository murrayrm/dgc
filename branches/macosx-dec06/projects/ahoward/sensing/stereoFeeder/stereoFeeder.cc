
/* 
 * Desc: Stereo feeder module using JPLV
 * Date: 09 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
  
#if USE_FB
#include <vis-tools/frame_buffer.h>
#endif

#include <jplv/jplv_stereo.h>

#include <sparrow/display.h>

#include <pose3.h>
#include <sn_msg.hh>
#include <sn_types.h>
#include <VehicleState.hh>
#include <sensnet.h>
#include <sensnet_stereo.h>

#include "bb_camera.h"
#include "stereo_log.h"
#include "stereo_feeder_opt.h"



/// @brief Stereo feeder class
class StereoFeeder
{
  public:   

  /// Default constructor
  StereoFeeder();

  /// Default destructor
  ~StereoFeeder();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);

  /// Parse the config file
  int parseConfigFile(const char *configDir);

  /// Initialize feeder for simulated capture
  int initSim(const char *configDir);
  
  /// Finalize feeder for simulated capture
  int finiSim();

  /// Capture a simulated frame 
  int captureSim();

  /// Initialize feeder for live capture
  int initLive(const char *configDir);
  
  /// Finalize feeder for live capture
  int finiLive();

  /// Capture a frame 
  int captureLive();
  
  /// Initialize feeder for log replay
  int initReplay(const char *logDir);

  /// Finalize feeder for log replay
  int finiReplay();

  /// Capture a frame from the log
  int captureReplay();

  /// Get the predicted vehicle state
  int getState(double timestamp);

  /// Initialize everything except capture
  int init();

  /// Finalize everything except capture
  int fini();
  
  /// Process a frame
  int process();

  // Run fake stereo using ground-plane constraint
  int calcPlaneDisp();
  
  /// Publish data over sensnet
  int writeSensnet();

  // Display current image to framebuffer
  int displayImage();

  // Write the current images to a file
  int writeFile();
  
  public:
  
  /// Initialize log for writing
  int logOpenWrite(const char *configDir);

  /// Initialize log for reading
  int logOpenRead(const char *logDir);

  /// Finalize log
  int logClose();

  /// Write the current images to the log
  int logWrite();

  /// Read the current scan from the log (used during playback)
  int logRead();

  public:

  /// Sparrow callback; occurs in sparrow thread
  static int onUserQuit(long);

  /// Sparrow callbacks; occurs in sparrow thread
  static int onUserPause(long);

  /// Sparrow callbacks; occurs in sparrow thread
  static int onUserDisplay(long);
    
  /// Sparrow callbacks; occurs in sparrow thread
  static int onUserLog(long);

  /// Sparrow callbacks; occurs in sparrow thread
  static int onUserExport(long);
  
  /// Sparrow thread
  pthread_t spThread;

  /// Mutex for thead sync
  pthread_mutex_t spMutex;

  public:

  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon; int skynetKey;

  // Our module id (SkyNet)
  modulename moduleId;
  char *moduleName;

  // Our sensor id (SensNet)
  sensnet_id_t sensorId;
  char *sensorName;

  // What mode are we in?
  enum {modeLive, modeReplay, modeSim} mode;

  // Should we quit?
  bool quit;

  // Should we pause?
  bool pause;

  // Is logging enabled
  bool logging;

  // Id of the camera
  int cameraId;
  
  // Camera interface
  bb_camera_t *camera;

  // Buffer for raw image capture
  jplv_image_t *capImage;

  // Log file (reading or writing, depending on mode)
  stereo_log_t *log;

  // Pyramid level for downsampling input image
  int imageLevel;
  
  // Raw image dimensions
  int imageCols, imageRows, imageChannels;

  // Camera models
  jplv_cmod_t leftModel, rightModel;

  // Sensor-to-vehicle transform
  float sens2veh[4][4];
  
  // Calibration stats (display only).
  // Sensor position (x,y,z) and rotation (roll,pitch,yaw).
  vec3_t sensPos, sensRot;

  // Stereo context
  jplv_stereo_t *stereo;
  
  // SensNet context
  sensnet_t *sensnet;
  
  // Current frame id
  int frameId;

  // Current frame time
  double frameTime;

  // Current state data
  sensnet_state_t state;
  
  // Current export frame id
  int exportId;

  // Start time for computing stats
  double startTime;
  
  // Capture stats
  int capCount;
  double capTime, capRate, capPeriod;

  // Difference between capture time and state time
  double stateDeltaTime;

  // Measured average exposure
  int imageExposure;

  // Stereo stats
  int stereoCount;
  double stereoTime, stereoRate, stereoPeriod;
  float stereoDispFrac;

  // Logging stats
  int logCount, logSize;

  // Enable the frame buffer display
  bool enableDisplay;
  
#if USE_FB
  // Console frame buffer
  struct frame_buffer_t *fb;
#endif
};


// Pointer to the one instance of this class.  Sparrow
// needs this in order to work correctly.
static StereoFeeder *self;


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stdout, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
StereoFeeder::StereoFeeder()
{
  memset(this, 0, sizeof(*this));

  this->frameId = -1;

  return;
}


// Default destructor
StereoFeeder::~StereoFeeder()
{
  return;
}


// Parse the command line
int StereoFeeder::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleName = this->options.module_id_arg;
  this->moduleId = modulenamefromString(this->moduleName);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->moduleName);
  
  // Fill out sensor id
  this->sensorName = this->options.sensor_id_arg;
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);
  
  return 0;
}


// Parse the config file
int StereoFeeder::parseConfigFile(const char *configDir)
{  
  // Load options from the configuration file
  char filename[256];
  snprintf(filename, sizeof(filename), "%s/%s.CFG", configDir, this->sensorName);
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", filename);

  // Parse transform
  float px, py, pz;
  float rx, ry, rz;
  sscanf(this->options.sens_pos_arg, "%f, %f, %f", &px, &py, &pz);
  sscanf(this->options.sens_rot_arg, "%f, %f, %f", &rx, &ry, &rz);

  pose3_t pose;
  pose.pos = vec3_set(px, py, pz);
  pose.rot = quat_from_rpy(rx, ry, rz);
  pose3_to_mat44f(pose, this->sens2veh);
  
  // Record euler angles for display only
  this->sensPos = vec3_set(px, py, pz);
  this->sensRot.x = rx * 180/M_PI;
  this->sensRot.y = ry * 180/M_PI;
  this->sensRot.z = rz * 180/M_PI;

  // Fill out the camera id
  this->cameraId = atoi(this->options.camera_arg);
  if (this->cameraId <= 0)
    return ERROR("invalid camera id: %s", this->options.camera_arg);

  return 0;
}


// Initialize feeder for simulated capture
int StereoFeeder::initSim(const char *configDir)
{
  char filename[256];

  // Load configuration file
  if (this->parseConfigFile(configDir) != 0)
    return -1;

  // Record key image dimensions
  this->imageLevel = this->options.level_arg;  
  this->imageCols = 512 >> this->imageLevel; // MAGIC
  this->imageRows = 384 >> this->imageLevel; // MAGIC
  this->imageChannels = 1;

  MSG("image %dx%dx%d", this->imageCols, this->imageRows, this->imageChannels);
      
  // Load camera model
  snprintf(filename, sizeof(filename), "%s/%d-left.cahvor", configDir, this->cameraId);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->leftModel, filename) != 0)
    return ERROR("unable to load camera model %s : %s", filename, jplv_error_str());

  // Load camera model
  snprintf(filename, sizeof(filename), "%s/%d-right.cahvor", configDir, this->cameraId);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->rightModel, filename) != 0)
    return ERROR("unable to load camera model %s : %s", filename, jplv_error_str());

  // Initialize logging
  if (this->options.enable_log_flag)
    if (this->logOpenWrite(configDir) != 0)
      return -1;
  
  this->mode = modeSim;
  
  return 0;
}


// Finalize feeder for simulated capture
int StereoFeeder::finiSim()
{    
  return 0;
}


// Capture a simulated frame 
int StereoFeeder::captureSim()
{
  double timestamp;
  jplv_image_t *rectImage, *dispImage;
  uint8_t *rpix;
  uint16_t *dpix;
  float cx, cy, sx, sy, baseline;
  pose3_t pose;
  float mv[4][4];
  double ml[4][4];
  int pi, pj;
  int qi, qj;
  float px, py, pz;
  float vx, vy, vz;
  double qx, qy;

  // Simulate 15Hz
  usleep(75000);
  
  timestamp = dgc_gettimeofday();
  
  // Get the current state data
  if (this->getState(timestamp) != 0)
    return -1;

  // Get pointers to the rectified image buffers
  rectImage = jplv_stereo_get_image(self->stereo,
                                    JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_LEFT);
  dispImage = jplv_stereo_get_image(self->stereo,
                                    JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);

  // Camera model
  jplv_cmod_t model;
  jplv_stereo_get_cmod_rect(this->stereo, JPLV_STEREO_CAMERA_LEFT, &model);    
  cx = model.ext.cahv.hc;
  cy = model.ext.cahv.vc;
  sx = model.ext.cahv.hs;
  sy = model.ext.cahv.vs;
  baseline = jplv_stereo_get_baseline(this->stereo);

  // Camera-to-vehicle transform
  assert(sizeof(mv) == sizeof(this->sens2veh));
  memcpy(mv, this->sens2veh, sizeof(mv));

  // Vehicle-to-local transform
  pose.pos = vec3_set(this->state.pose_local[0],
                      this->state.pose_local[1],
                      this->state.pose_local[2]);
  pose.rot = quat_from_rpy(this->state.pose_local[3],
                           this->state.pose_local[4],
                           this->state.pose_local[5]);
  pose3_to_mat44d(pose, ml);

  rpix = (uint8_t*) jplv_image_pixel(rectImage, 0, 0);
  dpix = (uint16_t*) jplv_image_pixel(dispImage, 0, 0);
  for (pj = 0; pj < rectImage->rows; pj++)
  {
    for (pi = 0; pi < rectImage->cols; pi++)
    {
      // Compute the ray/z-plane intersection in camera coordinates;
      // the matrix m specifies the camera pose relative to the plane.
      px = (pi - cx) / sx;
      py = (pj - cy) / sy;
      pz = -(mv[2][3] - VEHICLE_TIRE_RADIUS) / (mv[2][0] * px + mv[2][1] * py + mv[2][2]);
      px = px * pz;
      py = py * pz;

      // Compute disparity
      dpix[0] = (uint16_t) (unsigned int) (int) (sx / pz * JPLV_STEREO_DISP_SCALE * baseline);
      
      // Convert to vehicle coordinates
      vx = mv[0][0] * px + mv[0][1] * py + mv[0][2] * pz + mv[0][3];
      vy = mv[1][0] * px + mv[1][1] * py + mv[1][2] * pz + mv[1][3];
      vz = mv[2][0] * px + mv[2][1] * py + mv[2][2] * pz + mv[2][3];

      // Convert to local coordinates
      qx = ml[0][0] * vx + ml[0][1] * vy + ml[0][2] * vz + ml[0][3];
      qy = ml[1][0] * vx + ml[1][1] * vy + ml[1][2] * vz + ml[1][3];

      // Checker board
      if (false)
      {
        qi = (int) ((qx + 100000) / 1.00);
        qj = (int) ((qy + 100000) / 1.00);
        rpix[0] = 0x80 + ((qi + qj) % 2) * 0x7F;
      }

      // Lines every 20m
      if (true)
      {
        rpix[0] = 0x20;
        if (fmod(qx + 1e6, 20) < 0.12)
          rpix[0] = 0xD0;
        if (fmod(qy + 1e6, 20) < 0.12)
          rpix[0] = 0xD0;
      }

      rpix += rectImage->channels;
      dpix += 1;
    }
  }
  
  this->frameId += 1;
  this->frameTime = timestamp;

  // Record state latency
  this->stateDeltaTime = this->state.timestamp - this->frameTime;

  return 0;
}


// Initialize feeder for live capture
int StereoFeeder::initLive(const char *configDir)
{
  char filename[256];

  // Load configuration file
  if (this->parseConfigFile(configDir) != 0)
    return -1;
  
  // Initialize the camera
  this->camera = bb_camera_alloc(this->options.camera_arg);
  if (!this->camera)
    return ERROR("unable to connect to camera %s", this->options.camera_arg);
  if (bb_camera_init(this->camera, this->options.camera_config_arg) != 0)
    return -1;

  // Record key image dimensions
  this->imageLevel = this->options.level_arg;  
  this->imageCols = this->camera->image.width >> this->imageLevel;
  this->imageRows = this->camera->image.height >> this->imageLevel;
  this->imageChannels = 1; // HACK this->camera->image.channels;

  MSG("image %dx%dx%d", this->imageCols, this->imageRows, this->imageChannels);
      
  // Load camera model
  snprintf(filename, sizeof(filename), "%s/%d-left.cahvor", configDir, this->cameraId);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->leftModel, filename) != 0)
    return ERROR("unable to load camera model %s : %s", filename, jplv_error_str());

  // Load camera model
  snprintf(filename, sizeof(filename), "%s/%d-right.cahvor", configDir, this->cameraId);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->rightModel, filename) != 0)
    return ERROR("unable to load camera model %s : %s", filename, jplv_error_str());

  // Create buffer for raw image capture
  this->capImage = jplv_image_alloc(this->imageCols << this->imageLevel,
                                    this->imageRows << this->imageLevel,
                                    this->imageChannels, 16, 0, NULL);

  // Initialize logging
  if (this->options.enable_log_flag)
    if (this->logOpenWrite(configDir) != 0)
      return -1;
  
  this->mode = modeLive;
  
  return 0;
}


// Finalize feeder for live capture
int StereoFeeder::finiLive()
{
  if (this->log)
    this->logClose();
  
  bb_camera_fini(this->camera);
  bb_camera_free(this->camera);
  this->camera = NULL;

  jplv_image_free(this->capImage);
    
  return 0;
}


// Capture a frame 
int StereoFeeder::captureLive()
{
  int exp;
  jplv_image_t *leftImage, *rightImage;
  
  // Get pointers to the raw image buffers
  leftImage = jplv_stereo_get_image(self->stereo,
                                    JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
  rightImage = jplv_stereo_get_image(self->stereo,
                                     JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_RIGHT);
    
  // Read images off the camera
  bb_camera_capture(this->camera, this->capImage);

  // Demultiplex the image and measure overall image exposure
  bb_camera_demux_mono(this->capImage, this->imageLevel, &exp, leftImage, rightImage);
  
  //MSG("%d %d", this->frameId, exp);

  // Set measured exposure
  bb_camera_auto_gain(this->camera, 127, exp);
  
  this->frameId += 1;

  // HACK BROKEN this->frameTime = this->camera->timestamp;
  //this->frameTime = dgc_gettimeofday();
  this->frameTime = dgc_gettimeofday();

  // Get the matching state data
  if (this->getState(this->frameTime) != 0)
    return -1;

  // Record state latency
  this->stateDeltaTime = this->state.timestamp - this->frameTime;

  // Update stats
  this->imageExposure = exp;

  return 0;
}


// Initialize feeder for log replay
int StereoFeeder::initReplay(const char *logDir)
{
  // REMOVE?
  // Load configuration file from log directory
  if (this->parseConfigFile(logDir) != 0)
    return -1;

  // Open log file for reading
  if (this->logOpenRead(logDir) != 0)
    return -1;

  MSG("image %dx%dx%d", this->imageCols, this->imageRows, this->imageChannels);
  
  this->mode = modeReplay;
  
  return 0;
}


// Finalize feeder for log replay
int StereoFeeder::finiReplay()
{
  this->logClose();
  
  return 0;
}


// Capture a frame from the log
int StereoFeeder::captureReplay()
{
  // TESTING
  usleep(100000);

  if (this->logRead() != 0)
    return -1;

  // Record state latency
  this->stateDeltaTime = this->state.timestamp - this->frameTime;

  return 0;
}


// Initialize everything except image capture
int StereoFeeder::init()
{
  if (true)
  {    
    // Create stereo context
    this->stereo = jplv_stereo_alloc(this->imageCols, this->imageRows, this->imageChannels);
    if (!this->stereo)
      return ERROR("unable to allocate stereo context: JPLV %s", jplv_error_str());
    
    // Re-scale the models if ncessary
    jplv_cmod_scale_dims(&this->leftModel, this->imageCols, this->imageRows);
    jplv_cmod_scale_dims(&this->rightModel, this->imageCols, this->imageRows);

    // Set the model in the stereo context
    jplv_stereo_set_cmod(this->stereo, JPLV_STEREO_CAMERA_LEFT, &this->leftModel);
    jplv_stereo_set_cmod(this->stereo, JPLV_STEREO_CAMERA_RIGHT, &this->rightModel);

    // Set defaults
    jplv_stereo_set_disparity(this->stereo, 0, 40, 7, 7);

    // Prepare for action
    jplv_stereo_prepare(this->stereo);
  }
    
  if (true)
  {
    // Initialize SensNet
    this->sensnet = sensnet_alloc();
    if (sensnet_connect(this->sensnet,
                        this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
      return ERROR("unable to connect to sensnet");

    // Subscribe to vehicle state messages
    if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState), 100) != 0)
      return ERROR("unable to join state group");
  }

#if USE_FB
  if (this->options.enable_fb_given)
  {
    struct fb_var_screeninfo vinfo;

    // Initialize the frame buffer
    this->fb = frame_buffer_open("/dev/fb0");
    if (this->fb == NULL)
      return ERROR("unable to open frame buffer");

    // Check the screen dimensions
    frame_buffer_get_vinfo(this->fb, &vinfo);
    if (vinfo.bits_per_pixel != 32)
      return ERROR("frame buffer must be 32bpp, not %dbpp", vinfo.bits_per_pixel);
  }
#endif

  if (!this->options.disable_sparrow_flag)
  {
    // This looks weird, but we include the display table here so that
    // the pointers are initialized correctly.  The table is allocated
    // statically, so we can only have one instance of this class.
#include "stereoFeederSp.h"

    // Initialize CLI
    if (dd_open() < 0)
      return ERROR("unable to open display");
    dd_usetbl(display);

    // Bind keys
    dd_bindkey('q', onUserQuit);
    dd_bindkey('Q', onUserQuit);
    dd_bindkey('p', onUserPause);
    dd_bindkey('P', onUserPause);
    dd_bindkey('l', onUserLog);
    dd_bindkey('L', onUserLog);
    dd_bindkey('d', onUserDisplay);
    dd_bindkey('D', onUserDisplay);
    dd_bindkey('e', onUserExport);
    dd_bindkey('E', onUserExport);
    
    // Kick off thread for console interface
    pthread_mutex_init(&this->spMutex, NULL);
    pthread_create(&this->spThread, NULL, (void* (*) (void*)) dd_loop, this);
  }

  return 0;
}


// Finalize everything except capture
int StereoFeeder::fini()
{
  // Clean up the CLI
  if (!this->options.disable_sparrow_flag)
  {
    pthread_join(this->spThread, NULL);
    pthread_mutex_destroy(&this->spMutex);
    dd_close();
  }
  
  // Clean up SensNet
  if (sensnet_disconnect(this->sensnet) != 0)
    return ERROR("unable to connect to sensnet");
  sensnet_free(this->sensnet);

  // Clean up stereo
  jplv_stereo_free(this->stereo);
  
  return 0;
}


// Get the predicted vehicle state
int StereoFeeder::getState(double timestamp)
{
  int blob_id, blob_len;
  VehicleState state;

  memset(&this->state, 0, sizeof(this->state));
      
  // Get the state newest data in the cache
  if (sensnet_peek(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, &blob_id, &blob_len) != 0)
    return -1;
  if (blob_id >= 0)
  {
    if (sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR,
                     SNstate, blob_id, sizeof(state), &state) != 0)
      return 0; // TODO something
  }
  
  // TODO: do prediction by comparing timestamps
  this->state.timestamp = (double) state.Timestamp / 1e6;

  // Set local pose
  this->state.pose_local[0] = state.localX;
  this->state.pose_local[1] = state.localY;
  this->state.pose_local[2] = state.localZ;
  this->state.pose_local[3] = state.localRoll;
  this->state.pose_local[4] = state.localPitch;
  this->state.pose_local[5] = state.localYaw;

  // Set global pose
  this->state.pose_global[0] = state.Northing;
  this->state.pose_global[1] = state.Easting;
  this->state.pose_global[2] = state.Altitude;
  this->state.pose_global[3] = state.Roll;
  this->state.pose_global[4] = state.Pitch;
  this->state.pose_global[5] = state.Yaw;

  // Set velocities
  this->state.vel_vehicle[0] = state.vehicleVelX;
  this->state.vel_vehicle[1] = state.vehicleVelY;
  this->state.vel_vehicle[2] = state.vehicleVelZ;
  this->state.vel_vehicle[3] = state.vehicleVelRoll;
  this->state.vel_vehicle[4] = state.vehicleVelPitch;
  this->state.vel_vehicle[5] = state.vehicleVelYaw;

  //MSG("state %.3f %.3f %.3f", (double) state.Timestamp * 1e-6, state.Northing, state.Easting);
  
  return 0;
}


// Process a frame
int StereoFeeder::process()
{
  double elapsedTime;

  elapsedTime = dgc_gettimeofday();
  
  if (this->mode != modeSim)
  {
    // Run regular stereo pipeline
    jplv_stereo_rectify(this->stereo);  
    jplv_stereo_laplace(this->stereo);

    if (this->options.force_plane_flag)
    {
      // Run fake stereo using ground-plane constraint
      this->calcPlaneDisp();
    }
    else
    {
      // Run real stereo
      jplv_stereo_disparity(this->stereo);
    }
  }
  
  jplv_stereo_range(this->stereo);  
  jplv_stereo_diag(this->stereo);

  // Compute elapsed time for update
  elapsedTime = dgc_gettimeofday() - elapsedTime;
  
  // Compute some diagnostics
  this->stereoCount += 1;
  this->stereoPeriod = 1000.0 * elapsedTime;
  this->stereoRate = 1.0 / (dgc_gettimeofday() - this->stereoTime);
  this->stereoTime = dgc_gettimeofday();
  this->stereoDispFrac = this->stereo->diag_num_valid;
  this->stereoDispFrac /= (this->stereo->image_cols * this->stereo->image_rows);

  return 0;
}


// Run fake stereo using ground-plane constraint
int StereoFeeder::calcPlaneDisp()
{
  jplv_image_t *dispImage;
  uint16_t *dpix;
  int pi, pj;
  float px, py, pz;
  float cx, cy, sx, sy, baseline;
  float mv[4][4];

  // Disparity image
  dispImage = jplv_stereo_get_image(self->stereo,
                                    JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);

  // Camera model
  jplv_cmod_t model;
  jplv_stereo_get_cmod_rect(this->stereo, JPLV_STEREO_CAMERA_LEFT, &model);    
  cx = model.ext.cahv.hc;
  cy = model.ext.cahv.vc;
  sx = model.ext.cahv.hs;
  sy = model.ext.cahv.vs;    
  baseline = jplv_stereo_get_baseline(this->stereo);

  // Camera-to-vehicle transform
  assert(sizeof(mv) == sizeof(this->sens2veh));
  memcpy(mv, this->sens2veh, sizeof(mv));

  // TODO: consider robot attitude
  
  dpix = (uint16_t*) jplv_image_pixel(dispImage, 0, 0);
  for (pj = 0; pj < dispImage->rows; pj++)
  {
    for (pi = 0; pi < dispImage->cols; pi++)
    {
      // Compute the ray/z-plane intersection in camera coordinates;
      // the matrix m specifies the camera pose relative to the plane.
      px = (pi - cx) / sx;
      py = (pj - cy) / sy;
      pz = -(mv[2][3] - VEHICLE_TIRE_RADIUS) / (mv[2][0] * px + mv[2][1] * py + mv[2][2]);

      // Compute disparity
      dpix[0] = (uint16_t) (unsigned int) (int) (sx / pz * JPLV_STEREO_DISP_SCALE * baseline);
      
      dpix += 1;
    }
  }

  return 0;
}


// Display image to framebuffer
int StereoFeeder::displayImage()
{
#if USE_FB
  jplv_image_t *image;
  struct fb_var_screeninfo vinfo;

  // Get the screen dimensions
  frame_buffer_get_vinfo(this->fb, &vinfo);

  // Draw raw left on upper right corner
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
  frame_buffer_draw(this->fb, vinfo.xres - image->cols, 0, image->cols, image->rows,
                    image->channels, image->bits/8, image->data);

  /*
  // Draw diagnostics on upper right corner
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_DIAG, JPLV_STEREO_CAMERA_LEFT);
  frame_buffer_draw(this->fb,
                    vinfo.xres - image->cols, 0, image->cols, image->rows,
                    image->channels, image->bits/8, image->data);
  */

#endif
  return 0;
}


// Write the current images to a file
int StereoFeeder::writeFile()
{
  jplv_image_t *image;
  char filename[256];
  
  image = jplv_stereo_get_image(this->stereo, JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
  snprintf(filename, sizeof(filename),
           "%s_%04d_LEFT.pnm", this->sensorName, this->exportId);
  if (jplv_image_write_pnm(image, filename, NULL) != 0)
    return ERROR("unable to write image: %s", jplv_error_str());

  image = jplv_stereo_get_image(this->stereo, JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_RIGHT);
  snprintf(filename, sizeof(filename),
           "%s_%04d_RIGHT.pnm", this->sensorName, this->exportId);
  if (jplv_image_write_pnm(image, filename, NULL) != 0)
    return ERROR("unable to write image: %s", jplv_error_str());
  this->exportId++;

  return 0;
}


// Publish data
int StereoFeeder::writeSensnet()
{
  pose3_t pose;
  sensnet_stereo_blob_t blob;
  jplv_image_t *image;
  
  blob.blob_type = SENSNET_STEREO_BLOB;
  blob.sensor_id = this->sensorId;
  blob.frameid = this->frameId;
  blob.timestamp = this->frameTime;
  blob.state = this->state;

  blob.cols = this->stereo->image_cols;
  blob.rows = this->stereo->image_rows;

  // Get the rectified model and copy to blob
  jplv_cmod_t model;
  jplv_stereo_get_cmod_rect(this->stereo, JPLV_STEREO_CAMERA_LEFT, &model);    
  blob.cx = model.ext.cahv.hc;
  blob.cy = model.ext.cahv.vc;
  blob.sx = model.ext.cahv.hs;
  blob.sy = model.ext.cahv.vs;    
  blob.disp_scale = JPLV_STEREO_DISP_SCALE;
  blob.baseline = jplv_stereo_get_baseline(this->stereo);
  assert(blob.baseline > 0);

  // Copy camera transform
  assert(sizeof(this->sens2veh) == sizeof(blob.sens2veh));
  memcpy(blob.sens2veh, this->sens2veh, sizeof(this->sens2veh));

  // Vehicle to local transform
  pose.pos = vec3_set(blob.state.pose_local[0],
                      blob.state.pose_local[1],
                      blob.state.pose_local[2]);
  pose.rot = quat_from_rpy(blob.state.pose_local[3],
                           blob.state.pose_local[4],
                           blob.state.pose_local[5]);  
  pose3_to_mat44f(pose, blob.veh2loc);  

  // Copy rectified data
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_LEFT);
  blob.color_channels = image->channels;
  blob.color_size = image->data_size;
  assert(sizeof(blob.color_data) >= (size_t) image->data_size);
  memcpy(blob.color_data, image->data, image->data_size);
  
  // Copy disparity data
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);
  blob.disp_size = image->data_size;
  assert(sizeof(blob.disp_data) >= (size_t) image->data_size);
  memcpy(blob.disp_data, image->data, image->data_size);

  // Send rectified color image
  if (sensnet_write(this->sensnet, this->sensorId, SENSNET_STEREO_BLOB,
                    this->frameId, sizeof(blob), &blob) != 0)
    return ERROR("unable to write blob");

  return 0;
}


// Initialize logging for writing
int StereoFeeder::logOpenWrite(const char *configDir)
{
  time_t t;
  char timestamp[64];
  char logDir[256];
  char cmd[256];
  stereo_log_header_t header;

  // Construct log name
  t = time(NULL);
  strftime(timestamp, sizeof(timestamp), "%F-%a-%H-%M-%S", localtime(&t));
  snprintf(logDir, sizeof(logDir), "%s/%s-%s",
           this->options.log_path_arg, timestamp, this->sensorName);
    
  // Create log file
  this->log = stereo_log_alloc();
  assert(this->log);

  // Construct log header
  memset(&header, 0, sizeof(header));
  header.camera_id = this->cameraId;
  header.cols = this->imageCols;
  header.rows = this->imageRows;
  header.channels = this->imageChannels;

  // Open log file for writing
  if (stereo_log_open_write(this->log, logDir, &header) != 0)
    return ERROR("unable to open log: %s", logDir);

  // Copy configuration files
  snprintf(cmd, sizeof(cmd), "cp %s/%d-*.cahvor %s", configDir, this->cameraId, logDir);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "cp %s/%s.CFG %s", configDir, this->sensorName, logDir);
  system(cmd);

  return 0;
}


// Initialize log for reading
int StereoFeeder::logOpenRead(const char *logDir)
{
  char filename[256];
  stereo_log_header_t header;

  // Open log file for reading
  this->log = stereo_log_alloc();
  assert(this->log);
  if (stereo_log_open_read(this->log, logDir, &header) != 0)
    return ERROR("unable to open log: %s", logDir);

  MSG("image %dx%dx%d", header.cols, header.rows, header.channels);

  this->imageLevel = 0;  
  this->imageCols = header.cols;
  this->imageRows = header.rows;
  this->imageChannels = header.channels;

  // Fille out the camera id
  this->cameraId = header.camera_id;

  // Load camera model from log dir
  snprintf(filename, sizeof(filename), "%s/%d-left.cahvor", logDir, this->cameraId);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->leftModel, filename) != 0)
    return ERROR("unable to load camera model %s : %s", filename, jplv_error_str());
  
  // Load camera model from log dir
  snprintf(filename, sizeof(filename), "%s/%d-right.cahvor", logDir, this->cameraId);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->rightModel, filename) != 0)
    return ERROR("unable to load camera model %s : %s", filename, jplv_error_str());

  return 0;
}


// Finalize log
int StereoFeeder::logClose()
{
  if (!this->log)
    return 0;

  stereo_log_close(this->log);
  stereo_log_free(this->log);
  this->log = NULL;
  
  return 0;
}


// Write the current images to the log
int StereoFeeder::logWrite()
{
  stereo_log_tag_t tag;
  jplv_image_t *leftImage, *rightImage;
  
  if (!this->log)
    return 0;

  // Construct the image tag
  tag.frameid = this->frameId;
  tag.timestamp = this->frameTime;

  tag.state.timestamp = this->state.timestamp;
  memcpy(tag.state.pose_local, this->state.pose_local, sizeof(tag.state.pose_local));
  memcpy(tag.state.pose_global, this->state.pose_global, sizeof(tag.state.pose_global));
  memcpy(tag.state.vel_vehicle, this->state.vel_vehicle, sizeof(tag.state.vel_vehicle));
  memcpy(tag.sens2veh, this->sens2veh, sizeof(tag.sens2veh));
  
  // Get current raw images
  leftImage = jplv_stereo_get_image(this->stereo,
                                    JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
  rightImage = jplv_stereo_get_image(this->stereo,
                                     JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_RIGHT);

  // Write to log
  if (stereo_log_write(this->log, &tag,
                       leftImage->data_size, leftImage->data, rightImage->data) != 0)
    return ERROR("unable to write to log");

  // Update stats
  this->logCount++;
  this->logSize = this->logCount * leftImage->data_size * 2 / 1024 / 1024;
  
  return 0;
}


// Read current frame from log
int StereoFeeder::logRead()
{
  stereo_log_tag_t tag;
  jplv_image_t *leftImage, *rightImage;
  
  // Get pointers to the raw image buffers
  leftImage = jplv_stereo_get_image(self->stereo,
                                    JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
  rightImage = jplv_stereo_get_image(self->stereo,
                                     JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_RIGHT);

  // Read image data from log
  if (stereo_log_read(this->log, &tag, leftImage->data_size,
                          leftImage->data, rightImage->data) != 0)
    return ERROR("unable to read log");

  this->frameId = tag.frameid;
  this->frameTime = tag.timestamp;

  this->state.timestamp = tag.state.timestamp;
  memcpy(this->state.pose_local, tag.state.pose_local, sizeof(this->state.pose_local));
  memcpy(this->state.pose_global, tag.state.pose_global, sizeof(this->state.pose_global));
  memcpy(this->state.vel_vehicle, tag.state.vel_vehicle, sizeof(this->state.vel_vehicle));
  memcpy(this->sens2veh, tag.sens2veh, sizeof(this->sens2veh));
  
  return 0;
}


// Handle user events; occurs in sparrow thread
int StereoFeeder::onUserQuit(long)
{
  assert(self);
  self->quit = true;
  return DD_EXIT_LOOP;
}


// Handle user events; occurs in sparrow thread
int StereoFeeder::onUserPause(long)
{
  assert(self);
  self->pause = !self->pause;
  return 0;
}


// Handle user events; occurs in sparrow thread
int StereoFeeder::onUserDisplay(long)
{
  assert(self); 
  self->enableDisplay = !self->enableDisplay;  
  return 0;
}


// Handle user events; occurs in sparrow thread
int StereoFeeder::onUserLog(long)
{
  assert(self);
  self->logging = !self->logging;
  return 0;
}


// Handle user events; occurs in sparrow thread
int StereoFeeder::onUserExport(long)
{
  assert(self);
  pthread_mutex_lock(&self->spMutex);
  self->writeFile();
  pthread_mutex_unlock(&self->spMutex);
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  int status;

  // Create feeder
  self = new StereoFeeder();
  assert(self);
 
  // Parse command line options
  if (self->parseCmdLine(argc, argv) != 0)
    return -1;

  if (self->options.sim_flag)
  {
    // Initialize for simulated capture
    if (self->initSim(".") != 0)
      return -1;
  }
  else if (self->options.replay_given)
  {
    // Initialize for replay
    if (self->initReplay(self->options.replay_arg) != 0)
      return -1;
  }
  else
  {
    // Initialize for live capture
    if (self->initLive(".") != 0)
      return -1;
  }

  // Initialize everything
  if (self->init() != 0)
    return -1;

  self->startTime = dgc_gettimeofday();
  
  // Start processing
  while (!self->quit)
  {
    double nextTime;
    
    // Capture incoming frame directly into the stereo buffers
    if (self->mode == StereoFeeder::modeSim)
    {
      if (self->captureSim() != 0)
        break;
    }
    else if (self->mode == StereoFeeder::modeReplay)
    {
      if (self->captureReplay() != 0)
        break;
    }
    else
    {
      if (self->captureLive() != 0)
        break;
    }

    // Display current image on framebuffer
    if (self->options.enable_fb_flag && self->enableDisplay)
      self->displayImage();

    // Compute some diagnostics
    self->capCount += 1;
    self->capTime = dgc_gettimeofday() - self->startTime;
    self->capRate = (float) self->capCount / self->capTime;
    self->capPeriod = 1000.0 / self->capRate;
    
    // If paused, or if we are running too fast, give up our time
    // slice.
    if (self->pause || dgc_gettimeofday() < nextTime)
    {
      usleep(0);
      continue;
    }
    nextTime = dgc_gettimeofday() + 1.0/self->options.rate_arg;
    
    // Capture and process one frame
    pthread_mutex_lock(&self->spMutex);
    status = self->process();
    pthread_mutex_unlock(&self->spMutex);
    if (status != 0)
      break;

    // Publish data
    if (self->writeSensnet() != 0)
      break;
        
    // Write to the log
    if (self->options.enable_log_flag && (self->logging || self->options.disable_sparrow_flag))
      self->logWrite();
  }
  
  // Clean up
  self->fini();
  if (self->mode == StereoFeeder::modeSim)
    self->finiSim();
  else if (self->mode == StereoFeeder::modeReplay)
    self->finiReplay();
  else
    self->finiLive();
  delete self;

  MSG("program exited cleanly");
  
  return 0;
}
