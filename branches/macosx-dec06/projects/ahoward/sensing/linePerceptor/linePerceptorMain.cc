
/* 
 * Desc: Line perceptor run-time 
 * Date: 26 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

// Sensnet support
#include "sensnet.h"
#include "sensnet_stereo.h"
#include "skynet_roadline.h"

// Skynet support
// REMOVE #include "sn_msg.hh"
#include "sn_types.h"

// TODO: move this to sn_msg.hh
// REMOVE typedef skynet Skynet;

// Sparrow CLI support
#include "sparrow/display.h"

// Cmd-line handling
#include "linePerceptorOpt.h"


// TESTING
class LinePerceptor
{
  // TODO
};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stdout, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Application control
static bool quit;
//static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

// Sensnet control
static sensnet_t *sensnet;
static sensnet_stereo_blob_t blob;

// Sparrow thread control
static pthread_t sparrowThread;

// Sparrow CLI data
static bool spPaused;
static char spSpreadDaemon[80];
static int spSpreadKey;
static char spModuleName[80];
static int spFrameId;
static double spLatency;
static double spRunCycles, spRunTime, spRunPeriod, spRunFreq;
static bool spFake;
static int spNumFakes;


// Handle quit button
int spUserQuit(long arg)
{
  ::quit = true;
  return DD_EXIT_LOOP;
}


// Handle pause button
int spUserPause(long arg)
{
  ::spPaused = !::spPaused;
  return 0;
}


// Handle fake stop button
int spUserFake(long arg)
{
  ::spFake = true;  
  return 0;
}


// This looks weird, but we include the display table here so that
// the callbacks are initialized correctly.  
#include "linePerceptorSp.h"


// Main program thread
int main(int argc, char **argv)
{
  char *spreadDaemon;
  int spreadKey, sensorId;
  modulename moduleId;
  gengetopt_args_info options;
  // REMOVE Skynet *skynet;
  skynet_roadline_msg_t msg;
  int blobType, blobId, oldBlobId, blobLen;
  
  LinePerceptor *percept;
  
  // Load cmd-line options
  if (cmdline_parser(argc, argv, &options) < 0)
    return -1;

  // Fill out the spread name
  if (options.spread_daemon_given)
    spreadDaemon = options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the spread key
  if (options.spread_key_given)
    spreadKey = options.spread_key_arg;
  else if (getenv("SPREAD_KEY"))
    spreadKey = atoi(getenv("SPREAD_KEY"));
  else
    spreadKey = 0;
  
  // Fill out module id
  moduleId = modulenamefromString(options.module_id_arg);
  if (moduleId <= 0)
    return ERROR("invalid module id: %s", options.module_id_arg);
  
  // Fill out sensor id
  sensorId = sensnet_id_from_name(options.sensor_id_arg);
  if (sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", options.sensor_id_arg);
  
  // Initialize sensnet
  sensnet = sensnet_alloc();
  assert(sensnet);
  if (sensnet_connect(sensnet, spreadDaemon, spreadKey, moduleId) != 0)
    return -1;
  
  // Join stereo group
  if (sensnet_join(sensnet, sensorId,
                       SENSNET_STEREO_BLOB, sizeof(sensnet_stereo_blob_t), 5) != 0)
    return -1;

  // Initialize skynet
  // REMOVE skynet = new Skynet((modulename) moduleId, spreadKey);
  //assert(skynet);
  
  // Create perceptor
  percept = new LinePerceptor();
  assert(percept);

  // Initialize the CLI
  if (dd_open() < 0)
    return ERROR("unable to open display");
  dd_usetbl(display);

  // Initialize key-bindings
  dd_bindkey('q', spUserQuit);
  dd_bindkey('Q', spUserQuit);
  dd_bindkey('p', spUserPause);
  dd_bindkey('P', spUserPause);
  dd_bindkey('f', spUserFake);
  dd_bindkey('F', spUserFake);

  // Initialize CLI data
  strncpy(::spSpreadDaemon, spreadDaemon, sizeof(::spSpreadDaemon));
  ::spSpreadKey = spreadKey;
  strncpy(::spModuleName, modulename_asString(moduleId), sizeof(::spModuleName));
  
  // Kick off CLI thread
  pthread_create(&::sparrowThread, NULL, (void* (*) (void*)) dd_loop, NULL);

  blobType = SENSNET_STEREO_BLOB;
  blobId = -1;
  blobLen = -1;
  oldBlobId = -1;

  while (!::quit)
  {
    double runTime;

    // Did we read any data?  If not give up now.
    if (::spPaused || sensorId == SENSNET_NULL_SENSOR)
    {
      usleep(0);
      continue;
    }

    // Read the current blob
    if (sensnet_peek(sensnet, sensorId, blobType, &blobId, &blobLen) != 0)
      break;

    // Do we have new data?
    if (blobId < 0 || blobId == oldBlobId)
    {
      usleep(0);
      continue;
    }
    
    // Read the current blob
    if (sensnet_read(sensnet, sensorId, blobType, blobId, blobLen, &blob) != 0)
      break;
    oldBlobId = blobId;

    // Update CLI
    ::spLatency = dgc_gettimeofday() - blob.timestamp;
    
    // Prepare the roadline message
    msg.msg_type = SNroadLine;
    msg.frameid = blob.frameid;
    msg.timestamp = blob.timestamp;
    msg.state = blob.state;
    msg.num_lines = 0;

    // Process the current blob
    // TODO
    runTime = -dgc_gettimeofday();
    usleep(200000);
    runTime += dgc_gettimeofday();

    // Use some fake data
    if (::spFake)
    {
      // MAGIC
      msg.lines[0].a[0] = +5;
      msg.lines[0].a[1] = -2;
      msg.lines[0].a[2] = 0.5;
      msg.lines[0].b[0] = +5;
      msg.lines[0].b[1] = +2;
      msg.lines[0].b[2] = 0.5;
      msg.num_lines = 1;
      ::spFake = false;
      ::spNumFakes += 1;
    }
    
    // Send line data
    if (msg.num_lines > 0)
    {
      // REMOVE skynet->send_msg(msg.msg_type, &msg, sizeof(msg));
      sensnet_write(sensnet, SENSNET_SKYNET_SENSOR, msg.msg_type, 0, sizeof(msg), &msg);
    }
    
    // Update CLI
    ::spRunTime += runTime;
    ::spRunCycles += 1;
    ::spRunFreq = ::spRunCycles / ::spRunTime;
    ::spRunPeriod = ::spRunTime / ::spRunCycles;
    ::spFrameId = blobId; 
  }

  // Clean up display
  pthread_join(sparrowThread, NULL);
  dd_close();
  
  // Clean up perceptor
  delete percept;

  // Clean up sensnet
  sensnet_leave(sensnet, sensorId, SENSNET_STEREO_BLOB);
  sensnet_free(sensnet);

  // Clean up skynet
  // REMOVE delete skynet;

  MSG("exited cleanly");
  
  return 0;
}



