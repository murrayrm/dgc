/*	// get perpendicular angle. (-90 is out of the set, but +90 is inside)
	float rectAngle;
	if (angle1 > 0) {
		rectAngle = angle1-90;
	} else {
		rectAngle = angle1+90;
	}
	
	// cheat. make it be different from 90 so that we can use simple equations. eheh
	if (rectAngle==90) rectAngle -= 0.01;

	// define y=mx+b parameters for line1, line2 and the line that intersects line1 and line2
	float m1,m2,m;
	float b1,b2,b;
	float xi;

	// works better avoid vertical situations
	m1 = tan(angle1);
	b1 = start1.y-m1*start1.x;

	m2 = tan(angle2);
	b2 = start2.y-m2*start2.x;

	m = tan(rectAngle);

	// What we want:
	// Starting and ending points of matching segments of (line1 and line2) on line1
	float x1s, x1e;
	float y1s, y1e;	
	// Starting and ending points of matching segments of (line1 and line2) on line2
	float x2s, x2e;
	float y2s, y2e;	

	// Either the segment of line1 is shorter on the left side and a perpendicular line passing on its left corner intersects the segment of line2,
	x1s = start1.x;
	y1s = start1.y;
	b = start1.y-m*start1.x;
	xi = (b2 - b)/(m-m2);
	x2s = xi;
	y2s = m2*xi+b2;
	// OR maybe the segment of line2 is shorter on the left side and a perpendicular line passing on its left corner intersects the segment of line1,
	if (xi<start2.x || xi>end2.x) {

		//	strange lines that might brake all of this are on the middle of the perspective: \/ (unlikely to happen) /\ (likely to happen)
		if (fabs(angle1-angle2) > 90) {
			// lets cheat here ... basically swap the points
			x2s = end2.x;
			y2s = end2.y;
		} else {
			x2s = start2.x;
			y2s = start2.y;
		}
		
		b = y2s-m*x2s;
		xi = (b1 - b)/(m-m1);
		x1s = xi;
		y1s = m1*xi+b1;
		// OR both segments line1 and line2 do not overlap at all.
		if (xi<start1.x || xi > end1.x) return 0;
	}

	// Simillarly, ...
	// Either the segment of line1 is shorter on the right side and a perpendicular line passing on its right corner intersects the segment of line2,
	x1e = end1.x;
	y1e = end1.y;
	b = end1.y-m*end1.x;
	xi = (b2 - b)/(m-m2);
	x2e = xi;
	y2e = m2*xi+b2;
	// OR maybe the segment of line2 is shorter on the right side and a perpendicular line passing on its right corner intersects the segment of line1.
	if (xi<start2.x || xi>end2.x) {

		//	strange lines that might brake all of this are on the middle of the perspective: \/ (unlikely to happen) /\ (likely to happen)
		if (fabs(angle1-angle2) > 90) {
			// lets cheat here ... basically swap the points
			x2s = start2.x;
			y2s = start2.y;
		} else {
			x2s = end2.x;
			y2s = end2.y;
		}
		
		x2e = end2.x;
		y2e = end2.y;
		b = end2.y-m*end2.x;
		xi = (b1 - b)/(m-m1);
		x1e = xi;
		y1e = m1*xi+b1;
		// OR nothing! Eheheh...
	}
		
	// maximum distance at overlapping extremes

	// distance between starts
	float dss = distance(x1s,y1s,x2s,y2s);
	// distance between ends
	float dee = distance(x1e,y1e,x2e,y2e);
	// distance between start of 1 and end of 2
	float dse = distance(x1s,y1s,x2e,y2e);
	// distance between end of 1 and start of 2
	float des = distance(x1e,y1e,x2s,y2s);

	if ( dss < dse) {
		if ( (dss > dee ? dss : dee) > distanceTol) return 0;
	} else {
		if ( (dse > des ? dse : des) > distanceTol) return 0;		
	}

	// overlapping length
	if (distance(x1s,y1s,x1e,y1e) < alignmentTol) {
		return 0;
	}
*/
